#include "..\common.hpp"
#include "..\LODObject.h"
#include "..\ObjToolAnimation.h"
#include "..\GlobalFunctions.h"
#include ".\gdlodobject.h"
#include <malloc.h>
#include "..\Edges.h"
#include "..\ObjToolTopology.h"
#include "..\ObjToolAnimation.h"
#include "..\ObjToolClipboard.h"
#include "..\ObjToolCheck.h"
#include "..\ObjToolsMatLib.h"
#include "..\ObjToolProxy.h"
#include "..\ObjToolSections.h"
#include <io.h>

#define Category "O2ScriptLib::ObjectData"


using namespace ObjektivLib;

DEFINE_FAST_ALLOCATOR(GdObjectData)
DEFINE_FAST_ALLOCATOR(GdFaceT)

GdObjectData::GdObjectData(void)
{
_owner=new LODObjectRef;
_object=_owner->_ref->Active();
}

GdObjectData::GdObjectData(ObjektivLib::ObjectData *object, LODObjectRef *owner): 
_object(object),_owner(owner)
{

}

#define GET_OBJECTDATA(oper) (static_cast<GdObjectData *>(oper.GetData())->_object)
#define GET_FACE(oper) FaceT(static_cast<GdFaceT *>(oper.GetData())->_object,static_cast<GdFaceT *>(oper.GetData())->_face)


GdObjectData::GdObjectData(const GdObjectData &other):_object(other._object),_owner(other._owner)
{

}
RString GdObjectData::GetText() const
{
  char buff[50];
  sprintf(buff,"%d/%d/%d",_object->NPoints(),_object->NFaces(),_object->NAnimations());
  return RString(buff);
}

bool GdObjectData::IsEqualTo(const GameData *data) const
{
  const GdObjectData *Odata=dynamic_cast<const GdObjectData *>(data);
  return (Odata!=NULL && Odata->_object==_object);
}

RString GdFaceT::GetText() const
{
    ObjektivLib::FaceT fc(_object,_face);
  char buff[256];
  _snprintf(buff,sizeof(buff),"%s,%s",Pathname::GetNameFromPath(fc.GetTexture()),
    Pathname::GetNameFromPath(fc.GetMaterial()));
  return RString(buff);
}

bool GdFaceT::IsEqualTo(const GameData *data)const
{
  const GdFaceT *face=dynamic_cast<const GdFaceT *>(data);
  return (face!=NULL && face->_object==_object && face->_face==_face);
}


static GameValue countFaces(const GameState *gs, GameValuePar oper1)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  return GameValue((float)obj->NFaces());
}

static GameValue countPoints(const GameState *gs, GameValuePar oper1)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  return GameValue((float)obj->NPoints());
}

static GameValue countAnimations(const GameState *gs, GameValuePar oper1)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  return GameValue((float)obj->NAnimations());
}

static GameValue setProperty(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  const GameArrayType &array=oper2;
  if (array.Size()<2) return GameValue(false); 
  RString prop=array[0].GetType()==GameString?array[0]:array[0].GetText();
  if (array[1].GetNil()) obj->DeleteNamedProp(prop);
  else
  {
    RString val=array[1].GetType()==GameString?array[1]:array[1].GetText();
    obj->SetNamedProp(prop,val);
  }
  return GameValue(true);
}

static GameValue getProperty(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  RString name=oper2;  
  const char *prop=obj->GetNamedProp(name);
  if (prop==NULL) return GameValue();
  else
    return GameValue((GameStringType)prop);
}


 static GameValue getProperties(const GameState *gs, GameValuePar oper1)
 {
   ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
   GameArrayType newarr;
   int i;
   for (i=0;i<MAX_NAMED_PROP;i++)
   {
     if (obj->GetNamedProp(i))
     {
        newarr.Append()=obj->GetNamedProp(i);
        newarr.Append()=obj->GetNamedPropValue(i);
     }
   }
   return newarr;
 }

static GameValue setPoint(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
   ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  if (oper2.GetType()!=GameArray) {gs->SetError(EvalType);return GameValue(false);}
  const GameArrayType &array=oper2;
  if (array.Size()!=2) {gs->SetError(EvalType);return GameValue(false);}
  if (array[0].GetType()!=GameScalar) {gs->SetError(EvalType);return GameValue(false);}
  if (array[1].GetType()!=GameArray)  {gs->SetError(EvalType);return GameValue(false);}
  const GameArrayType &vector=array[1];
  if (vector.Size()<3) {gs->SetError(EvalType);return GameValue(false);}
  int index=toInt(array[0]);
  if (index<0 || index>=obj->NPoints()) return GameValue(false);
  PosT &ps=obj->Point(index);
  ps.SetPoint(VecT(vector[0],vector[1],vector[2]));
  return GameValue(true);
}

static GameValue addPoint(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  if (oper2.GetType()!=GameArray) {gs->SetError(EvalType);return GameValue();}
  const GameArrayType &vector=oper2;
  if (vector.Size()<3) {gs->SetError(EvalType);return GameValue();}
  int index=obj->ReservePoints(1);
  if (index<0 || index>=obj->NPoints()) return GameValue();
  PosT &ps=obj->Point(index);
  ps.SetPoint(VecT(vector[0],vector[1],vector[2]));
  return GameValue((float)index);
}

static GameValue getPoint(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
 {
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  int index=toInt(oper2);
  if (index<0 || index>=obj->NPoints()) return GameValue();
  PosT &ps=obj->Point(index);
  GameArrayType array;
  array.Access(2);
  array[0]=ps[0];
  array[1]=ps[1];
  array[2]=ps[2];
  return array;
}

static GameValue forEachPoint(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
 {
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1); 
  if (oper2.GetType()!=GameArray) {gs->SetError(EvalType);return GameValue(false);}
  const GameArrayType &array=oper2;  
  if (array.Size()<2) {gs->SetError(EvalType);return GameValue(false);}
  if (!(array[0].GetType() & GameCode)) {gs->SetError(EvalType);return GameValue(false);}
  if (!(array[1].GetType() & GameCode))  {gs->SetError(EvalType);return GameValue(false);}
  GameValue testcode=array[0];
  GameValue opercode=array[1];
  GameVarSpace space(gs->GetContext());
  space.VarLocal("_x");
  space.VarLocal("_this");
  space.VarSet("_this",oper1,true);
  gs->BeginContext(&space);
  if (array.Size()==3 && (GameBoolType)array[2]==true)
  {
    for (int i=obj->NPoints()-1;i>=0;i--)
    {
      space.VarSet("_x",GameValue((float)i));
      GameValue res=gs->EvaluateMultiple((RString)testcode);
      if ((bool)res)
      {
        res=gs->EvaluateMultiple((RString)opercode);
#if USE_PRECOMPILATION==0
        if (space._blockExit) {gs->EndContext();return res;}
#endif
      }
    }    
  }
  else
  {
    for (int i=0;i<obj->NPoints();i++)
    {
      space.VarSet("_x",GameValue((float)i));
      GameValue res=gs->EvaluateMultiple((RString)testcode);
      if ((bool)res)
      {
        res=gs->EvaluateMultiple((RString)opercode);
#if USE_PRECOMPILATION==0
        if (space._blockExit) {gs->EndContext();return res;}
#endif
      }
    }    
  }
  gs->EndContext();
  return GameValue();
}

static GameValue forEachFace(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1); 
  if (oper2.GetType()!=GameArray) {gs->SetError(EvalType);return GameValue(false);}
  const GameArrayType &array=oper2;  
  if (array.Size()<2) {gs->SetError(EvalType);return GameValue(false);}
  if (!(array[0].GetType() & GameCode)) {gs->SetError(EvalType);return GameValue(false);}
  if (!(array[1].GetType() & GameCode))  {gs->SetError(EvalType);return GameValue(false);}
  GameValue testcode=array[0];
  GameValue opercode=array[1];
  GameVarSpace space(gs->GetContext());
  space.VarLocal("_x");
  space.VarLocal("_this");
  space.VarSet("_this",oper1,true);
  gs->BeginContext(&space);
  if (array.Size()==3 && (GameBoolType)array[2]==true)
  {
    for (int i=obj->NFaces()-1;i>=0;i--)
    {
      space.VarSet("_x",GameValue((float)i));
      GameValue res=gs->EvaluateMultiple((RString)testcode);
      if ((bool)res)
      {
        res=gs->EvaluateMultiple((RString)opercode);
#if USE_PRECOMPILATION==0
        if (space._blockExit) {gs->EndContext();return res;}
#endif
      }
    }    
  }
  else
  {
    for (int i=0;i<obj->NFaces();i++)
    {
      space.VarSet("_x",GameValue((float)i));
      GameValue res=gs->EvaluateMultiple((RString)testcode);
      if ((bool)res)
      {
        res=gs->EvaluateMultiple((RString)opercode);
#if USE_PRECOMPILATION==0
        if (space._blockExit) {gs->EndContext();return res;}
#endif
      }
    }    
  }
  gs->EndContext();
  return GameValue();
}

static GameValue getFace(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1); 
  int index=toInt(oper2);
  if (index<0 || index>=obj->NFaces()) return GameValue();
  GdFaceT *fc=new GdFaceT(obj,(static_cast<GdObjectData *>(oper1.GetData()))->_owner,index);
  return GameValue(fc);
}

static GameValue getFaceVertices(const GameState *gs, GameValuePar oper1)
{
  FaceT fc=GET_FACE(oper1);
  GameArrayType array;
  for (int i=0;i<fc.N();i++) array.Append()=GameValue((float)fc.GetPoint(i));
  return array;
}

static GameValue newFace(const GameState *gs, GameValuePar oper1)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1); 
  return GameValue((float)obj->ReserveFaces(1));
}

static GameValue setFaceVertices (const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  FaceT fc=GET_FACE(oper1);
  const GameArrayType &arr=oper2;
  if (arr.Size()<3 || arr.Size()>4)  return GameValue();
  else
  {
    fc.SetN(arr.Size());
    for (int i=0;i<arr.Size();i++)
    {
      int index=toInt(arr[i]);
      if (index<0 || index>=fc.GetObject()->NPoints())
      {
        gs->SetError(EvalDim);
        return GameValue();
      }
      else
        fc.SetPoint(i,index);
    }
  }
return oper1;
}

static GameValue setFaceTexture (const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  FaceT fc=GET_FACE(oper1);
  GameStringType str=oper2;
  fc.SetTexture(str);
  return oper1;
}

static GameValue setFaceMaterial (const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  FaceT fc=GET_FACE(oper1);
  GameStringType str=oper2;
  fc.SetMaterial(str);
  return oper1;
}

static GameValue getFaceTexture (const GameState *gs, GameValuePar oper1)
{
  FaceT fc=GET_FACE(oper1);
  return GameValue((GameStringType)fc.GetTexture());
}

static GameValue getFaceMaterial (const GameState *gs, GameValuePar oper1)
{
  FaceT fc=GET_FACE(oper1);
  return GameValue((GameStringType)fc.GetMaterial());
}

static GameValue setFaceUVSet (const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  FaceT fc=GET_FACE(oper1);
  const GameArrayType &arr=oper2;
  int size=arr.Size();
  int pos=0;
  for (int i=0;i<fc.N();i++)
  {
    if (pos<size)     
      fc.SetU(i,arr[pos++]);
    else
      fc.SetU(i,0);
    if (pos<size)
      fc.SetV(i,arr[pos++]);
    else 
      fc.SetV(i,0);
  }
return oper1;
}

static GameValue getFaceUVSet (const GameState *gs, GameValuePar oper1)
{
  FaceT fc=GET_FACE(oper1);
  GameArrayType array;
  for (int i=0;i<fc.N();i++) 
  {
    array.Append()=GameValue((float)fc.GetU(i));
    array.Append()=GameValue((float)fc.GetV(i));
  }
  return array;
}

static GameValue getAnimVertex(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  const GameArrayType &arr=oper2;
  if (arr.Size()!=2 || arr[0].GetType()!=GameScalar || arr[1].GetType()!=GameScalar) 
  {
    gs->SetError(EvalType);
    return GameValue();
  }
  int frame=toInt(arr[0]);
  int index=toInt(arr[1]);
  if (index<0 || index>=obj->NPoints() || frame<0 || frame>=obj->NAnimations())
  {
    gs->SetError(EvalForeignError,"Index or frame is out of range");
    return GameValue();
  }
  AnimationPhase &anim=*obj->GetAnimation(frame);
  VecT &vx=anim[index];
  GameArrayType vector;
  vector.Append()=vx[0];
  vector.Append()=vx[1];
  vector.Append()=vx[2];
  return vector;
}

static GameValue setAnimVertex(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  const GameArrayType &arr=oper2;
  if (arr.Size()!=3 || arr[0].GetType()!=GameScalar || arr[1].GetType()!=GameScalar || arr[2].GetType()!=GameArray) 
  {
    gs->SetError(EvalType);
    return GameValue();
  }
  int frame=toInt(arr[0]);
  int index=toInt(arr[1]);
  const GameArrayType &vector=arr[2];
  if (index<0 || index>=obj->NPoints() || frame<0 || frame>=obj->NAnimations())
  {
    gs->SetError(EvalForeignError,"Index or frame is out of range");
    return GameValue();
  }  
  AnimationPhase &anim=*obj->GetAnimation(frame);
  anim[index]=VecT((float)vector[0],(float)vector[1],(float)vector[2]);
  return oper1;
}

static GameValue timeFromFrame(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  int frame=toInt(oper2);
  if (frame<0 || frame>=obj->NAnimations()) 
  {
    gs->SetError(EvalForeignError,"frame is out of range");
    return GameValue();
  }  
  return obj->GetAnimation(frame)->GetTime();
}

static GameValue frameFromTime(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  float  time=oper2;
  int frame=obj->NearestAnimationIndex(time);
  if (frame==-1) return GameValue();
  return (float)frame;
}

static GameValue setFrameTime(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  const GameArrayType &arr=oper2;
  if (arr.Size()!=2 || arr[0].GetType()!=GameScalar || arr[1].GetType()!=GameScalar) 
  {
    gs->SetError(EvalType);
    return GameValue();
  }
  int frame=toInt(arr[0]);
  float time=arr[1];
  if (frame<0 || frame>=obj->NAnimations()) 
  {
    gs->SetError(EvalForeignError,"frame is out of range");
    return GameValue();
  }  
  obj->GetAnimation(frame)->SetTime(time);
  obj->SortAnimations();
  return oper1;
}

static GameValue addAnimFrame(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  float  time=oper2;
  int nanim=obj->NAnimations();
  AnimationPhase phs(obj);
  phs.SetTime(time);
  phs.Validate();
  for (int i=0;i<obj->NPoints();i++) phs[i]=obj->Point(i);
  if (obj->AddAnimation(phs)==false) 
    return GameValue();
  obj->SortAnimations();
  int index=obj->AnimationIndex(time);
  return (float)index;
}

static GameValue deleteAnimFrame(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  int frame=toInt(oper2);
  if (frame<0 || frame>=obj->NAnimations()) 
  {
    gs->SetError(EvalForeignError,"frame is out of range");
    return GameValue();
  }  
  return obj->DeleteAnimation(obj->GetAnimation(frame)->GetTime());  
}

static GameValue setActiveFrame(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  int frame=toInt(oper2);
  if (frame<-1 || frame>=obj->NAnimations()) 
  {
    gs->SetError(EvalForeignError,"frame is out of range");
    return GameValue();
  }  
  obj->UseAnimation(frame);
  return oper1;
}


static GameValue deleteAllFrames(const GameState *gs, GameValuePar oper1)
{
    ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
    obj->DeleteAllAnimations();
    return oper1;
}

static GameValue updateActiveFrame(const GameState *gs, GameValuePar oper1)
{
    ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
    int curanim=obj->CurrentAnimation();
    if (curanim==-1)
    {
      gs->SetError(EvalForeignError,"No active frame");
      return GameValue();
    }
    obj->RedefineAnimation(curanim);
    return oper1;
}

static GameValue currentFrame(const GameState *gs, GameValuePar oper1)
{
    ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
    int curanim=obj->CurrentAnimation();
    if (curanim==-1)
    {
      return GameValue();
    }
    return (float)curanim;
}



static GameValue copyFrame(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  const GameArrayType &arr=oper2;
  if (arr.Size()!=2 || arr[0].GetType()!=GameScalar || arr[1].GetType()!=GameScalar) 
  {
    gs->SetError(EvalType);
    return GameValue();
  }
  int trgframe=toInt(arr[0]);
  int srcframe=toInt(arr[1]);
  if (trgframe<0 || trgframe>=obj->NAnimations() || srcframe<0 || srcframe>=obj->NAnimations())
  {
    gs->SetError(EvalForeignError,"One of indices is out of range");
    return GameValue();
  }  
  AnimationPhase &src=*(obj->GetAnimation(srcframe));
  AnimationPhase &trg=*(obj->GetAnimation(trgframe));
  trg=src;
  return oper1;    
}


static GameValue copyObject(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *trg=GET_OBJECTDATA(oper1);
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper2);
  if (trg!=src) *trg=*src;
  return oper1;
}

static GameValue normAnimation(const GameState *gs, GameValuePar oper1)
{
    ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
    ObjToolAnimation &anim=obj->GetTool<ObjToolAnimation>();
    anim.AutoTimeAnimations(true);
    return oper1;
  
}

static GameValue ownerOf(const GameState *gs, GameValuePar oper1)
{
  const GdObjectData *Odata=static_cast<const GdObjectData *>(oper1.GetData());
  return GameValue(new GdLODObject(Odata->_owner));
}

static GameValue constructCopy(const GameState *gs, GameValuePar oper1)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  Ref<LODObjectRef> lodobj=new LODObjectRef();
  lodobj->_ref->AddLevel(*src,1.0f);
  lodobj->_ref->DeleteLevel(0);
  return GameValue(new GdObjectData(lodobj->_ref->Active(),lodobj));
}


static GameValue getMassPoint(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  int index=toInt(oper2);
  if (index<0 || index>=src->NPoints())
  {
    gs->SetError(EvalForeignError,"Index out of range");
    return GameValue();
  }
  double mass=src->GetPointMass(index);
  return (float)mass;
}

static GameValue setMassPoint(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  const GameArrayType& arr=oper2;
  if (arr[0].GetType()!=GameScalar || arr[1].GetType()!=GameScalar)
  {
    gs->SetError(EvalType);
    return GameValue();
  }
  int index=toInt(arr[0]);
  float mass=arr[1];
  if (index<0 || index>=src->NPoints())
  {
    gs->SetError(EvalForeignError,"Index out of range");
    return GameValue();
  }
  src->SetPointMass(index,mass);
  return oper1;
}

static void GetMaskRange(const GameState *gs, GameValuePar oper, unsigned long &mask, unsigned long &shift)
{
  mask=0;
  shift=0;
  if (oper.GetType()==GameScalar)
  {
    int pos=toInt(oper);
    mask=1<<pos;
    shift=pos;
  }
  else if (oper.GetType()==GameArray)
  {
    const GameArrayType &arr=oper;
    if (arr.Size()!=2)
    {
      gs->SetError(EvalType);
      return;
    }
    if (arr[0].GetType()!=GameScalar && arr[1].GetType()!=GameScalar)
    {
      gs->SetError(EvalType);
      return;
    }
    int from=toInt(arr[0]);
    int to=toInt(arr[1]);
    for (int i=from; i<=to;i++)
    {
      mask<<=1;
      mask|=1;
    }
    shift=from;
    mask<<=shift;
  }
  else 
    gs->SetError(EvalType);
}

static GameValue setFaceFlags(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  FaceT fc=GET_FACE(oper1);
  const GameArrayType &arr=oper2;
  if (arr.Size()!=2 || arr[1].GetType()!=GameScalar) {gs->SetError(EvalType);return GameValue();}
  unsigned long mask,shift,val;
  GetMaskRange(gs,arr[0],mask,shift);
  val=toLargeInt(arr[1]);
  val<<=shift;
  val&=mask;
  fc.SetFlags(mask,val);
  return oper1;
}

static GameValue getFaceFlags(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  FaceT fc=GET_FACE(oper1);
  unsigned long mask,shift,val;
  GetMaskRange(gs,oper2,mask,shift);
  val=fc.GetFlags();
  val=(val & mask)>>shift;
  return (float)val;
}

static GameValue setPointFlags(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  const GameArrayType &arr=oper2;
  if (arr.Size()!=3 || arr[0].GetType()!=GameScalar || arr[2].GetType()!=GameScalar ) {gs->SetError(EvalType);return GameValue();}
  unsigned long mask,shift,val;
  int index=toInt(arr[0]);
  if (index<0 || index>=src->NPoints())
  {
    gs->SetError(EvalForeignError,"Index out of range");
    return GameValue();
  }
  GetMaskRange(gs,arr[1],mask,shift);
  val=toLargeInt(arr[2]);
  val<<=shift;
  val&=mask;
  PosT &ps=src->Point(index);
  ps.flags=(ps.flags & ~mask) | val;
  return oper1;

}

static GameValue getPointFlags(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  const GameArrayType &arr=oper2;
  if (arr.Size()!=2 || arr[0].GetType()!=GameScalar) {gs->SetError(EvalType);return GameValue();}
  unsigned long mask,shift,val;
  int index=toInt(arr[0]);
  if (index<0 || index>=src->NPoints())
  {
    gs->SetError(EvalForeignError,"Index out of range");
    return GameValue();
  }
  GetMaskRange(gs,arr[1],mask,shift);
  PosT &ps=src->Point(index);
  val=ps.flags & mask;
  val>>=shift;
  return (float)val;
}

static GameValue getPointFlagsStr(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  int index=toInt(oper2);
  if (index<0 || index>=src->NPoints())
  {
    gs->SetError(EvalForeignError,"Index out of range");
    return GameValue();
  }
  unsigned long flags=src->Point(index).flags;
  RString res;
  sprintf(res.CreateBuffer(9),"%08X",flags);
  return res;  
}

static GameValue getFaceFlagsStr(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  int index=toInt(oper2);
  if (index<0 || index>=src->NFaces())
  {
    gs->SetError(EvalForeignError,"Index out of range");
    return GameValue();
  }
  FaceT fc(src,index);
  unsigned long flags=fc.GetFlags();
  RString res;
  sprintf(res.CreateBuffer(9),"%08X",flags);
  return res;  
}

static GameValue massCenterOf(const GameState *gs, GameValuePar oper1)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  VecT vc=src->GetMassCentre(false);
  GameArrayType arr;
  arr.Append()=vc[0];
  arr.Append()=vc[1];
  arr.Append()=vc[2];
  return arr;
}

static GameValue callRuntime(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  RString name=oper2;
  name.Lower();  
  if (name=="facegraph") 
  {
    CEdges result(src->NPoints());
    CEdges nocross(src->NPoints());
    src->BuildFaceGraph(result,nocross);
    GameArrayType res;
    for (int i=0;i<src->NPoints();i++)
    {
      GameArrayType sub;
      for (int j=0;;j++)
      {
        int where=result.GetEdge(i,j);
        if (where<0) break;
        sub.Append()=(float)where;
      }
      res.Append()=sub;
    }
    return res;
  }
  else if (name=="hidesel") src->HideSelection();
  else if (name=="unhidesel") src->UnhideSelection();
  else if (name=="locksel") src->LockSelection();
  else if (name=="unlocksel") src->UnlockSelection();
  else if (name=="recalcnorms") src->RecalcNormals(true);
  else if (name=="sortanims") src->SortAnimations();
  else if (name=="squarizesel") src->Squarize();
  else if (name=="triangulatesel") src->Triangulate();
  else if (name=="selhidden") src->UseHidden();
  else if (name=="checkclosed") src->GetTool<ObjToolTopology>().CheckClosedTopology();
  else if (name=="closetopo") src->GetTool<ObjToolTopology>().CloseTopology();
  else if (name=="removecompo") src->GetTool<ObjToolTopology>().RemoveComponents();
  else if (name=="createcompo") 
  {
    Selection sel(src);
    src->GetTool<ObjToolProxy>().GetAllProxies(sel);
    src->GetTool<ObjToolTopology>().CreateComponents(&sel);
  }
  else if (name=="compoconvexhull") src->GetTool<ObjToolTopology>().ComponentConvexHull();
  else if (name=="checkconvexcompo") src->GetTool<ObjToolTopology>().CheckConvexComponents();
  else if (name=="checkconvexity") src->GetTool<ObjToolTopology>().CheckConvexity(false);
  else if (strncmp(name,"autosharp ",10)==0) 
  {
    float num;
    sscanf((const char *)name+10,"%f",&num);
    src->GetTool<ObjToolTopology>().AutoSharpEdges(num*3.14159265f/180.0f);
  }
  else if (name=="makesharp")  src->GetTool<ObjToolTopology>().MakeEdgesSharp();  
  else if (name=="mirroranim") src->GetTool<ObjToolAnimation>().MirrorAnimation();
  else if (name=="halfrateanim") src->GetTool<ObjToolAnimation>().HalfRateAnimation();
  else if (name=="texlist" || name=="matlist" || name=="proxylist") 
  {
    BTree<ObjMatLibItem> container;
    if (name=="proxylist")
    {
      bool setpp=true;
      if (ObjToolProxy::ProxyPrefix==NULL) ObjToolProxy::ProxyPrefix="proxy:";else setpp=false;
      src->GetTool<ObjToolProxy>().EnumProxies(container);
      if (setpp) ObjToolProxy::ProxyPrefix=NULL;
    }
    else
      src->GetTool<ObjToolMatLib>().ReadMatLib(container,name=="texlist"?ObjToolMatLib::ReadTextures:ObjToolMatLib::ReadMaterials);
    AutoArray<RString> list;
    ObjToolMatLib::ConvContainerToArray(container,list);
    GameArrayType res;
    for (int i=0;i<list.Size();i++) res.Append()=list[i];
    return res;
  }
  else if (name=="cropsel") src->GetTool<ObjToolClipboard>().ExtractSelection();
  else if (name=="optimize") src->GetTool<ObjToolCheck>().Optimize();
  else if (name=="isolatedpts") src->GetTool<ObjToolCheck>().IsolatedPoints();
  else if (name=="chkmapping") src->GetTool<ObjToolCheck>().CheckMapping();
  else if (name=="chkstvects") src->GetTool<ObjToolCheck>().CheckSTVectors();
  else if (name=="chkfaces") src->GetTool<ObjToolCheck>().CheckFaces();
  else if (name=="repairfaces") src->GetTool<ObjToolCheck>().RepairDegeneratedFaces();
  else if (name=="degfaces") src->GetTool<ObjToolCheck>().CheckDegeneratedFaces();
  else if (name=="smoothgroups") 
  {
    unsigned long *grps=new unsigned long[src->NFaces()];
    src->GetTool<ObjToolTopology>().BuildSmoothGroups(grps);
    GameArrayType arr;
    arr.Resize(src->NFaces());
    for (int i=0;i<src->NFaces();i++) arr[i]=GameValue((float)grps[i]);
    delete [] grps;
    return arr;
  }
  else if (name=="countsections")
  {
    BTree<ObjToolSections::ObjToolSectionInfo> sinfo;
    src->GetTool<ObjToolSections>().CalculateSections(sinfo);
    return (float)(sinfo.Size());
  }
  else if (name=="getsections")
  {
    BTree<ObjToolSections::ObjToolSectionInfo> sinfo;
    src->GetTool<ObjToolSections>().CalculateSections(sinfo);
    ObjToolSections::ObjToolSectionInfo **sectarr=new ObjToolSections::ObjToolSectionInfo *[sinfo.Size()];
    int count=ObjToolSections::BuildSectionArray(sinfo,sectarr);
    GameArrayType arr;
    for (int i=0;i<count;i++)
    {
      GameArrayType subarr;
      char buff[10];
      subarr.Append()=RString(sectarr[i]->ptrTexture);
      subarr.Append()=RString(sectarr[i]->ptrMaterial);
      subarr.Append()=RString(_itoa(sectarr[i]->flags,buff,16));
      subarr.Append()=(float)sectarr[i]->count;
      subarr.Append()=sectarr[i]->IsAlpha;
      subarr.Append()=(float)sectarr[i]->alphaId;
      arr.Append()=subarr;
    }
    delete [] sectarr;
    return arr;
  }
  else 
  {
    gs->SetError(EvalForeignError,"Run-time routine has not been found.");
  }
  return oper1;
}


static GameValue sharpEdges(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  const GameArrayType &arr=oper2;
  for (int i=0;i<arr.Size();i+=2)
  {
    int a=toInt(arr[i]);
    int b=toInt(arr[i+1]);
    src->AddSharpEdge(a,b);
  }
  return oper1;
}

static GameValue smoothEdges(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  const GameArrayType &arr=oper2;
  for (int i=0;i<arr.Size();i+=2)
  {
    int a=toInt(arr[i]);
    int b=toInt(arr[i+1]);
    src->RemoveSharpEdge(a,b);
  }
  return oper1;
}

static GameValue TestSharpEdges(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  const GameArrayType &arr=oper2;
  CEdges egs(src->NPoints());
  for (int i=0,a=0,b=0;src->EnumEdges(i,a,b);) egs.SetEdge(a,b);
  int cntm=0,cnts=0;
  for (int i=0;i<arr.Size();i+=2)
  {
    int a=toInt(arr[i]);
    int b=toInt(arr[i+1]);
    if (egs.IsEdge(a,b)) cnts++;
    else cntm++;
  }
  return (float)((cntm==0)-(cnts==0));
}

static GameValue mergeObject(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *trg=GET_OBJECTDATA(oper1);
  ObjektivLib::ObjectData *src;
  RString selName;
  if (oper2.GetType()==GameArray)
  {
    const GameArrayType &arr=oper2;
    if (arr.Size()!=2 || arr[0].GetType()!=EvalType_ObjectData || arr[1].GetType()!=GameString)
    {
      gs->SetError(EvalType);
      return GameValue();
    }
    src=GET_OBJECTDATA(arr[0]);
    selName=arr[1];
  }
  else
    src=GET_OBJECTDATA(oper2);
  trg->Merge(*src,selName.GetLength()==0?NULL:selName.Data());
  return oper1;
}

static GameValue exportRTM(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  ObjToolAnimation &anim=src->GetTool<ObjToolAnimation>();
  RString fname=oper2;  
  float xStep,zStep;
  if (_access(fname,0)==0 && _access(fname,02)!=0) return false;
  if (_access(fname,0)==0 && remove(fname)!=0) return false;
  const char *step=anim.GetNamedProp("Step");
  if (step==NULL) step=anim.GetNamedProp("ZStep");
  if (step) zStep=(float)atof(step);else zStep=0;
  step=anim.GetNamedProp("XStep");
  if (step) xStep=(float)atof(step);else xStep=0;
  anim.ExportAnimation(fname,zStep,xStep);
  return (_access(fname,0)==0);  
}

static GameValue importRTM(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  ObjToolAnimation &anim=src->GetTool<ObjToolAnimation>();
  RString fname=oper2;  
  anim.AutoAnimation(fname);
  return anim.NAnimations()>1;
}

static GameValue CopyFunct( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  ObjektivLib::ObjectData *trg=GET_OBJECTDATA(oper2);
  *src=*trg;
  return oper1;
}

static GameValue selectionToProxy( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const GameArrayType &params=oper2;
  if (params.Size()!=3 || params[0].GetType()!=GameString || params[1].GetType()!=GameString || params[2].GetType()!=GameArray) {state->SetError(EvalType);return GameValue();}
  const GameArrayType &pin=params[2];
  if (pin.Size()!=3 || pin[0].GetType()!=GameScalar || pin[1].GetType()!=GameScalar || pin[2].GetType()!=GameScalar) {state->SetError(EvalType);return GameValue();}
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  VecT vpin(pin[0],pin[1],pin[2]);
  GdLODObject *gdlodout=new GdLODObject();
  LODObject *lodout=gdlodout->GetObject()->_ref;
  ObjektivLib::ObjectData *obj=lodout->Active();
  *obj=*src;  
  NamedSelection *named=const_cast<NamedSelection *>(obj->GetNamedSel((RString)params[0]));
  if (named==NULL) {state->SetError(EvalForeignError, "Selection was not found");delete gdlodout; return GameValue();}
  obj->GetTool<ObjToolClipboard>().ExtractSelection(named);
  for (int i=0;i<obj->NPoints();i++)
  {
    PosT &ps=obj->Point(i);
    ps-=vpin;
  }
  src->SelectionDelete(named);
  src->DeleteNamedSel((RString)params[0]);
  src->GetTool<ObjToolProxy>().CreateProxy((RString)params[1],vpin);
  return GameValue(new GdObjectData(obj,gdlodout->GetObject()));
}

static GameValue createProxy(const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const GameArrayType &params=oper2;
  if (params.Size()!=2 || params[0].GetType()!=GameString || params[1].GetType()!=GameArray) {state->SetError(EvalType);return GameValue();}
  const GameArrayType &pin=params[1];
  if (pin.Size()!=3 || pin[0].GetType()!=GameScalar || pin[1].GetType()!=GameScalar || pin[2].GetType()!=GameScalar) {state->SetError(EvalType);return GameValue();}
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  VecT vpin(pin[0],pin[1],pin[2]);
  src->GetTool<ObjToolProxy>().CreateProxy((RString)params[0],vpin);
  return oper1;

}


static GameValue isUVSet(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  int id=toLargeInt(oper2);
  if (id<0) return false;
  return src->GetUVSet(id)!=0;
}

static GameValue setActiveUVSet(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  int id=toLargeInt(oper2);
  if (id<-1) return false;
  bool res=src->SetActiveUVSet(id);  
  if (res==false)
  {
    id=src->AddUVSet(id)->GetIndex();
    return src->SetActiveUVSet(id);
  }
  else
    return true;
}

static GameValue deleteUVSet(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  int id=toLargeInt(oper2);
  if (id<0) return false;
  return src->DeleteUvSet(id);
}

static GameValue changeIdOfActiveUVSet(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  int id=toLargeInt(oper2);
  if (id<0) return false;
  ObjUVSet *current=src->GetActiveUVSet();
  if (current->GetIndex()==0) return false;
  ObjUVSet *newset=src->GetUVSet(id);
  if (current==newset) return true;
  if (newset) newset->SetIndex(current->GetIndex());
  current->SetIndex(id);
  return true;
}

class FunctorFillArrayWithUVSetIDs
{
  mutable GameArrayType &_result;
public:
  FunctorFillArrayWithUVSetIDs(GameArrayType &result):_result(result) {}
  bool operator()(const ObjUVSet *item) const
  {
    _result.Append()=(float)(item->GetIndex());
    return false;
  }
};

static GameValue getUVSetList(const GameState *state, GameValuePar oper1)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  GameArrayType result;
  src->EnumUVSets(FunctorFillArrayWithUVSetIDs(result));
  return result;
}

static GameValue getActiveUVSet(const GameState *state, GameValuePar oper1)
{
  ObjektivLib::ObjectData *src=GET_OBJECTDATA(oper1);
  return (float)(src->GetActiveUVSet()->GetIndex());
}


#define OPERATORS_DEFAULT(XX, Category) \
  XX(GameBool, "setProperty",function, setProperty,EvalType_ObjectData, GameArray, "object","[property,value]", "Sets property in object. Property is representes as array with two field (second argument can be anyType): [name,value]. If value is nil, property is deleted", "_object setProperty [\"XStep\",0.520]", "true, if no error", "", "", Category) \
  XX(GameString, "getProperty",function, getProperty,EvalType_ObjectData, GameString, "object","property", "Gets property of object.", "_object getProperty \"XStep\";", "for.ex: \"0.520\"", "", "", Category) \
  XX(GameBool,"setPoint",function, setPoint,EvalType_ObjectData, GameArray, "object","[index,vector]","Sets point in object. ","_mesh setPoint [_index,[_x,_y,_z]];","true, if success","","",Category)\
  XX(GameArray,"getPoint",function, getPoint,EvalType_ObjectData, GameScalar, "object","index","Gets point in object. ","_vector=_mesh getPoint _index;","vector or NIL if error","","",Category)\
  XX(GameScalar, "addPoint",function, addPoint, EvalType_ObjectData, GameArray, "object", "vector","Creates new points and returns its index.","_mesh addPoint [0,0,0]","","","",Category)\
  XX(GameAny, "forEachPoint", function,forEachPoint, EvalType_ObjectData, GameArray, "object", "[condition,operation,back]",\
      "Call 'operation' for each point in object. 'condition' string that is evaluated for each point. If 'condition' returns true, 'operation' is evaluated. 'back' is optional (default false). If true, enumeration is processed from end to begin",\
      "_mesh forEachPoint [\"_this isPointSelected _x\",\"_vector=_this getPoint _x;_this setPoint [_x,[_vector @ 0,_vector @ 1, _vector @ 2+0.5]]\"];nil","","","",Category)\
  XX(GameAny, "forEachFace", function,forEachFace, EvalType_ObjectData, GameArray, "object", "[condition,operation,back]",\
      "Call 'operation' for each face in object. 'condition' string that is evaluated for each face. If 'condition' returns true, 'operation' is evaluated. 'back' is optional (default false). If true, enumeration is processed from end to begin",\
      "same kind as forEachPoint","","","",Category) \
  XX(EvalType_FaceT,"face",function,getFace,EvalType_ObjectData,GameScalar,"object","index","Returns face indexed by index.","_object face 10 setTexture \"example.paa\";","","","",Category) \
  XX(EvalType_FaceT,"setVertices",function,setFaceVertices,EvalType_FaceT, GameArray,"face","array","Sets new vertices to face. Array contains new vertices.","_object face 10 setVertices [1,8,15,11]","true if operation succesed","","",Category) \
  XX(EvalType_FaceT,"setTexture",function,setFaceTexture,EvalType_FaceT, GameString,"face","file","Sets texture to the face.","_face setTexture \"example.paa\"","","","",Category) \
  XX(EvalType_FaceT,"setMaterial",function,setFaceMaterial,EvalType_FaceT, GameString,"face","file","Sets material to the face.","_face setMaterial \"example.rvmat\"","","","",Category) \
  XX(EvalType_FaceT,"setUVSet",function,setFaceUVSet,EvalType_FaceT, GameArray,"face","set","Assign new uv coordinates to face. If array is to short, missing values is assumed as zero","_face setUVSet [0,0, 1,0, 1,1, 1,0];","","","",Category) \
  XX(EvalType_ObjectData,":=",function,copyObject,EvalType_ObjectData, EvalType_ObjectData,"trg","src","Copy object to another. Content of trg is replaced by copy of src.","_b=getActiveObject newLODObject;_b:=_a; //_b is copy of _a","","","",Category) \
  XX(EvalType_ObjectData,"mergeObject",function,mergeObject,EvalType_ObjectData, EvalType_ObjectData,"trg","src","Merges one object (src) into another (trg). Works same way as File->Merge in Objectiv menu.","","","","",Category) \
  XX(EvalType_ObjectData,"mergeObject",function,mergeObject,EvalType_ObjectData, GameArray,"trg","[src,selName]","Merges one object (src) into another (trg). You can specify selection name(selName), that will contain new object.Works same way as File->Merge in Objectiv menu.","","","","",Category) \
  \
  XX(GameArray,"getAnimPoint",function,getAnimVertex,EvalType_ObjectData, GameArray,"mesh","[frame,index]","Gets vertex in animation frame as array [x,y,z]","_vertex=_mesh getAnimPoint [1,10];","vertex with index 10 in frame 1","","",Category) \
  XX(EvalType_ObjectData,"setAnimPoint",function,setAnimVertex,EvalType_ObjectData, GameArray,"mesh","[frame,index,vertex]","Sets vertex in animation frame. Vertex is array [x,y,z]","_mesh setAnimPoint [1,10,[2,3,4]];","","","",Category) \
  XX(GameScalar,"FrameFromTime",function,frameFromTime,EvalType_ObjectData, GameScalar,"mesh","time","Retruns frame index of nearest time","_frame = _mesh frameFromTime 0.5","frame index nearest to time 0.5","","",Category) \
  XX(GameScalar,"TimeFromFrame",function,timeFromFrame,EvalType_ObjectData, GameScalar,"mesh","frame","Retusn time of specified frame","_time = _mesh timeFromFrame 10","time of frame 10","","",Category) \
  XX(EvalType_ObjectData,"setFrameTime",function,setFrameTime,EvalType_ObjectData, GameArray,"mesh","[frame,time]","sets time of specified frame","_mesh setFrameTime [10,0.5];","","","",Category) \
  XX(GameScalar,"addAnimFrame",function,addAnimFrame,EvalType_ObjectData, GameScalar,"mesh","time","Creates new animation frame and assign it time. Returns index of created frame. It reindexes frames, so update all your variables that contain frame index.","_newframe=_mesh addAnimFrame 0.5","","","",Category) \
  XX(GameScalar,"deleteAnimFrame",function,deleteAnimFrame,EvalType_ObjectData, GameScalar,"mesh","frame","Deletes frame specified by index. Returns new count of animations in mesh. It reindexes frames, so update all your variables that contain frame index","_mesh deleteAnimFrame 5;","","","",Category) \
  XX(EvalType_ObjectData,"setActiveFrame",function,setActiveFrame,EvalType_ObjectData, GameScalar,"mesh","frame","Sets active frame. Active frame is mapped into vertices in model, so you can use setPoint and getPoint to manipulate with vertices in frame. Changes is not reflected in animation until"\
  "active frame is changed or saveActiveFrame is called or object is saved. Do not try change active frame using setAnimVertex, all changes may be discarded. If you want change active frame, use -1 as frame number to unset active frame. But changes made using setPoint after may be discarded","_mesh setActiveFrame 1;","","","",Category) \
  XX(EvalType_ObjectData,"copyFrame",function,copyFrame,EvalType_ObjectData, GameArray,"mesh","[trgframe,srcframe]","Copies srcframe frame into trgframe frame.","_mesh copyFrame [2,1]; //copy frame 1 into frame 2","","","",Category) \
  XX(GameScalar,"getPointMass",function,getMassPoint,EvalType_ObjectData, GameScalar,"mesh","index","Gets vertex's mass. Mass is valid only for Gemometry Level","_mesh getMassPoint 10;","","","",Category) \
  XX(EvalType_ObjectData,"setPointMass",function,setMassPoint,EvalType_ObjectData, GameArray,"mesh","[index,value]","Sets vertex's mass. Mass is valid only for Gemometry Level","_mesh setMassPoint [10,1200];","","","",Category) \
  XX(EvalType_FaceT,"setFaceFlags",function,setFaceFlags,EvalType_FaceT, GameArray,"face","[index/range,value]","Sets face flag(s). First argument can be number of flags, or range of flags in form [l,h]. Second argument is value. Use predefined constants to set meanful flags","_face setFaceFlags [FLAG_FACE_USERVALUE,10];","","","",Category) \
  XX(GameScalar,"getFaceFlags",function,getFaceFlags,EvalType_FaceT, GameArray,"face","[range]","Gets face flags. Argument is range of flags in form [l,h]. Use predefined constants to get meanful flags","_face getFaceFlags FLAG_FACE_USERVALUE;","","","",Category) \
  XX(GameScalar,"getFaceFlags",function,getFaceFlags,EvalType_FaceT, GameScalar,"face","index","Gets face flag. Argument is flag's index. Use predefined constants to get meanful flags","","","","",Category) \
  XX(EvalType_ObjectData,"setPointFlags",function,setPointFlags,EvalType_ObjectData, GameArray,"mesh","[point_index,flag_index/range,value]","Sets point flag(s). Use predefined constants to get meanful flags","","","","",Category) \
  XX(GameScalar,"getPointFlags",function,getPointFlags,EvalType_ObjectData, GameArray,"mesh","[point_index,flag_index/range]","Gets point flag(s). Use predefined constants to get meanful flags","","","","",Category) \
  XX(GameString,"getPointFlagsStr",function,getPointFlagsStr,EvalType_ObjectData,GameScalar,"mesh","index","Gets code-string represents flags of point.","","","","",Category) \
  XX(GameString,"getFaceFlagsStr",function,getFaceFlagsStr,EvalType_ObjectData, GameScalar,"mesh","index","Gets code-string represents flags of face.","","","","",Category) \
  XX(GameAny,"callRuntime",function,callRuntime,EvalType_ObjectData, GameString,"mesh","name","Call a O2 runtime procedure. `name` specified name of procedure","","","","",Category) \
  XX(EvalType_ObjectData,"sharpEdges",function,sharpEdges,EvalType_ObjectData, GameArray,"mesh","[a,b,a,b,a,b ...]","Sets edges sharp","","","","",Category) \
  XX(EvalType_ObjectData,"smoothEdges",function,smoothEdges,EvalType_ObjectData, GameArray,"mesh","[a,b,a,b,a,b ...]","Sets edges smooth","","","","",Category) \
  XX(GameScalar,"testSharpEdges",function,TestSharpEdges,EvalType_ObjectData, GameArray,"mesh","[a,b,a,b,a,b ...]","Test edges to sharpness. Result is -1, 0 or 1. Result -1: all edges has been smooth. Result 1 all edges has been sharp. Result 0 some edges has been smooth and some edges has been sharp","","","","",Category) \
  \
  XX(GameBool,"exportRTM",function,exportRTM,EvalType_ObjectData, GameString,"mesh","filename","Exports RTM from animation. Returns true, when success. Function excepted normalized animation and properties step and XStep defined","","","","",Category) \
  XX(GameBool,"importRTM",function,importRTM,EvalType_ObjectData, GameString,"mesh","filename","Imports RTM to animation. Returns true, when success.","","","","",Category) \
  XX(EvalType_ObjectData,":=",nula, CopyFunct, EvalType_ObjectData, EvalType_ObjectData,"object","object", "Copies content of second object to first object", "this:=_object", "", "", "", Category) \
  XX(EvalType_ObjectData,"selectionToProxy",function, selectionToProxy, EvalType_ObjectData,GameArray,"mesh","[selection,proxyname,pin]","Function extracts selection, and replaces it with proxy. Specify name of selection, and proxyname without \"proxy:\" word. Pin is vector [x,y,z] and specifies location of new proxy.<p>Note: Object is not saved, script may use ownerOf to get owning LODObject, and call save itself</p>","_newobj=_mesh selectionToProxy [\"hlava\",\"\\data\\hlava\\hlava.p3d\",[0,0,0]]","","","",Category) \
  XX(EvalType_ObjectData,"createProxy",function, createProxy, EvalType_ObjectData,GameArray,"mesh","[proxyname,pin]","Function creates new proxy at pin","","","","",Category) \
  XX(GameBool,"isUVSet",function, isUVSet, EvalType_ObjectData,GameScalar,"mesh","UVSet_id","Tests existence of an UVSet. ","if (_mesh isUVSet 2)","(Tests, if UVSet with id 2 exists in mesh)","","",Category"::UVSets") \
  XX(GameBool,"setActiveUVSet",function, setActiveUVSet, EvalType_ObjectData,GameScalar,"mesh","UVSet_id","Activates UVSet. If UVSet not exists, creates new. If you specify -1 as ID, new UVSet is created with unique ID. Use getActiveUVSet to get thid ID","","true - no error","","",Category"::UVSets") \
  XX(GameBool,"deleteUVSet",function, deleteUVSet, EvalType_ObjectData,GameScalar,"mesh","UVSet_id","Deletes specified UVSet","","true - no error","","",Category"::UVSets") \
  XX(GameBool,"changeIdOfActiveUVSet",function, changeIdOfActiveUVSet, EvalType_ObjectData,GameScalar,"mesh","UVSet_id","Changes ID of active UVSet. If ID already exists in mesh, IDs are exchanged","","true - no error","","",Category"::UVSets") \
  



#define FUNCTIONS_DEFAULT(XX, Category) \
  XX(GameScalar, "countFaces", countFaces, EvalType_ObjectData, "object", "Counts  faces in object", "_nfaces=countFaces _object", "count of faces in object", "", "", Category) \
  XX(GameScalar, "countPoints", countPoints, EvalType_ObjectData, "object", "Counts points in object", "_npoints=countPoints _object", "count of points in objects", "", "", Category) \
  XX(GameScalar, "countAnimations", countAnimations, EvalType_ObjectData, "object", "Counts animations in object", "_nanims=countAnimations _object", "count of animations in objects", "", "", Category) \
  XX(GameArray, "properties", getProperties, EvalType_ObjectData, "object", "Returns array of properties. Array is organised into pairs, each pair contain property name and its value. If you want to get count of array, divide the result twice","_numprop=count properties _object /2","","","",Category)\
  XX(GameArray,"getVertices",getFaceVertices,EvalType_FaceT,"face","Returns array of vertices at face.","{..code _x...} forEach getVertices (_object face 10) ","","","",Category) \
  XX(GameScalar,"addFace",newFace,EvalType_ObjectData,"object","Returns index of new face","_nwfc=face addFace _object;","","","",Category) \
  XX(GameString,"getTexture",getFaceTexture,EvalType_FaceT,"face","Returns current face texture","_texture=getTexture _face;","","","",Category) \
  XX(GameString,"getMaterial",getFaceMaterial,EvalType_FaceT,"face","Returns current face material","_material=getMaterial _face;","","","",Category) \
  XX(GameArray,"getUVSet",getFaceUVSet ,EvalType_FaceT,"face","Returns array of UV coordinates of current stage in order U,V,U,V..etc","_uvset=getUVSet _face;","","","",Category) \
  XX(EvalType_ObjectData,"deleteAllFrames",deleteAllFrames,EvalType_ObjectData,"mesh","Deletes all frames in object.","deleteAllFrames _mesh;","","","",Category) \
  XX(EvalType_ObjectData,"updateActiveFrame",updateActiveFrame,EvalType_ObjectData, "mesh","Updates animation. All changes made using setPoint is copied to vertices in active animation.","_mesh setActiveFrame 1;_mesh setPoint [1,[1,2,3]];updateActiveFrame _mesh;","","","",Category) \
  XX(GameScalar,"getActiveFrame",currentFrame,EvalType_ObjectData, "mesh","Returns index of animation currently mapped to vertices. Returns nil, if no animation is mapped","_mesh setActiveFrame 1;getActiveFrame _mesh","1","","",Category) \
  XX(EvalType_ObjectData,"normAnimation",normAnimation,EvalType_ObjectData, "mesh","Normalizes animation. Animation will start at time 0 and ends at time 1","","","","",Category) \
  XX(EvalType_LODObject,"ownerOf",ownerOf,EvalType_ObjectData, "mesh","Returns owner LODObject of this object","","","","",Category) \
  XX(EvalType_ObjectData,"+",constructCopy,EvalType_ObjectData, "mesh","Creates copy of object. It constructs copy with new owner","","","","",Category) \
  XX(GameArray,"massCenterOf",massCenterOf,EvalType_ObjectData, "mesh","Calculates center of mass","","","","",Category) \
  XX(GameArray,"getUVSetList",getUVSetList ,EvalType_ObjectData,"object","Enumerates available UV sets in objects. Result is array of indexes","_uvset=getUVSetList _obj;","","","",Category"::UVSets") \
  XX(GameScalar,"getActiveUVSet",getActiveUVSet,EvalType_ObjectData,"object","Returns ID of currently active UVSet","_active=getActiveUVSet _obj;","","","",Category"::UVSets") \


static GameOperator BinaryFuncts[]=
{
  OPERATORS_DEFAULT(REGISTER_OPERATOR, Category)
};

static GameFunction UnaryFuncts[]=
{
  FUNCTIONS_DEFAULT(REGISTER_FUNCTION, Category)
};


static GameData *CreateObject(ParamArchive *ar) {return new GdObjectData;}
static GameData *CreateFace(ParamArchive *ar) {return new GdFaceT;}

TYPES_OBJECT_DATA(DEFINE_TYPE, Category)

#if DOCUMENT_COMREF
static ComRefFunc DefaultComRefFunc[] =
{
/*  NULARS_DEFAULT(COMREF_NULAR, Category)*/
  FUNCTIONS_DEFAULT(COMREF_FUNCTION, Category)
  OPERATORS_DEFAULT(COMREF_OPERATOR, Category)
};

static ComRefType DefaultComRefType[] =
{
  TYPES_DEFAULT(COMREF_TYPE, Category)
};
#endif


void GdObjectData::RegisterToGameState(GameState *gState)
{
  GameState &state = *gState; // helper to make macro works
  TYPES_OBJECT_DATA(REGISTER_TYPE, Category)

  gState->NewOperators(BinaryFuncts,lenof(BinaryFuncts));
  gState->NewFunctions(UnaryFuncts,lenof(UnaryFuncts));
  /*gState->NewNularOps(NularyFuncts,lenof(NularyFuncts));*/
#if DOCUMENT_COMREF
  gState->AddComRefFunctions(DefaultComRefFunc,lenof(DefaultComRefFunc));
  gState->AddComRefTypes(DefaultComRefType,lenof(DefaultComRefType));
#endif
}
