#pragma once
#include <El\Evaluator\express.hpp>
#include <es\types\pointers.hpp>
#include "O2TypeID.h"
#include <El\Pathname\Pathname.h>

#define TYPES_OBJECT_DATA(XX, Category) \
  XX("ObjectData",EvalType_ObjectData,CreateObject,"@ObjectData","ObjectData","This type represents one level of P3D.",Category)\
  XX("FaceT",EvalType_FaceT,CreateFace,"@FaceT","FaceT","This type represents one face in object.",Category)\

TYPES_OBJECT_DATA(DECLARE_TYPE, Category)


class LODObjectRef: public RefCount
{
public:

  ObjektivLib::LODObject *_ref;  
  bool _delete;
  LODObjectRef()
  {
    _ref=new ObjektivLib::LODObject();
    _delete=true;
  };
  LODObjectRef(ObjektivLib::LODObject *ref)
  {
    _ref=ref;
    _delete=false;
  }
  ~LODObjectRef()
  {
    if (_delete) delete _ref;
  }
  LODObjectRef(const LODObjectRef &other)
  {
      _ref=new ObjektivLib::LODObject(*other._ref);
    _delete=true;
  }
};


class GdObjectData :  public GameData
{
public:
    ObjektivLib::ObjectData *_object;
  Ref<LODObjectRef> _owner;

  GdObjectData(void);
  GdObjectData(ObjektivLib::ObjectData *object, LODObjectRef *owner);
  GdObjectData(const GdObjectData &other);

  const GameType &GetType() const {return EvalType_ObjectData;}
  RString GetText() const;

  bool IsEqualTo(const GameData *data) const;
  const char *GetTypeName() const {return "ObjectData";}
  GameData *Clone() const {return new GdObjectData (*this);}

  USE_FAST_ALLOCATOR;

  static void RegisterToGameState(GameState *gState);
};




class GdFaceT :  public GameData
{
public:
  ObjektivLib::ObjectData *_object;
  Ref<LODObjectRef> _owner;
  int _face;

  GdFaceT () {}
  GdFaceT (ObjektivLib::ObjectData *object, LODObjectRef *owner, int face):
    _object(object),_owner(owner),_face(face) {}
  GdFaceT (const GdFaceT  &other):
    _object(other._object),_owner(other._owner),_face(other._face){}

  const GameType &GetType() const {return EvalType_FaceT;}
  RString GetText() const;

  bool IsEqualTo(const GameData *data) const;
  const char *GetTypeName() const {return "FaceT";}
  GameData *Clone() const {return new GdFaceT  (*this);}

  USE_FAST_ALLOCATOR;

  static void RegisterToGameState(GameState *gState);
};


