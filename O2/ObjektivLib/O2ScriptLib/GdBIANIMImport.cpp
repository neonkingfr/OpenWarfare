#include "..\common.hpp"
#include "..\LODObject.h"
#include "gdLODObject.h"
#include "gdBIANIMImport.h"
#include "../BIANMImport.h"


static RString lastImportError;
#define Category "O2Scripts::ImportBIAnim"

static GameValue BiAnimImport(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  using namespace ObjektivLib;
  const GameArrayType &arr=oper2;
  if (arr.Size()<3) return RString("Less parameters");
  RString name=arr[0];
  float scale=arr[1];
  int invert=toInt((float)arr[2]);
  GdObjectData *obj=static_cast<GdObjectData *>(oper1.GetData());
  RString err;

  CBIANMImport anm;
  ifstream infile(name,ios::in|ios::binary);
  if (!infile) {err="Load Unsuccessful";return RString(err);}

  int errline;
  int error=anm.ParseSkeleton(infile,&errline);
  if (!error)
  {
    anm.ImportBones(*(obj->_object),scale,(invert & 1)!=0,(invert & 2)!=0, (invert & 4)!=0);
    AnimationPhase tmp(obj->_object);
    tmp.Validate();
    tmp.SetTime(-0.5f);    
    anm.LoadAnimationFrame(*(obj->_object),tmp,scale,(invert & 1)!=0,(invert & 2)!=0, (invert & 4)!=0);    
    obj->_object->AddAnimation(tmp);
    float timpos=0;    
    while ((error=anm.ParseNextFrame(infile,&errline))==0)
    {
      tmp.SetTime(timpos);
      anm.LoadAnimationFrame(*(obj->_object),tmp,scale,(invert & 1)!=0,(invert & 2)!=0, (invert & 4)!=0);    
      obj->_object->AddAnimation(tmp);
      timpos+=1.0f;
    }
    if (error==-12) error=0;
    obj->_object->UseAnimation(0);
  }
  if (error)
  {
    RString s;
    _snprintf(s.CreateBuffer(5000),5000,"%s(%d): %s",name.Data(),errline,anm.GetError(error));
    s;
  }
  return RString("");
}
 

#define OPERATORS_DEFAULT(XX, Category) \
  XX(GameString, "ImportBIAnim",function, BiAnimImport, EvalType_ObjectData ,GameArray, "level","[\"filename\",_scale,_invert]", "","","","","",Category) \



static GameOperator UnaryFuncts[]=
{
  OPERATORS_DEFAULT(REGISTER_OPERATOR, Category)
};


#if DOCUMENT_COMREF
static ComRefFunc DefaultComRefFunc[] =
{
  OPERATORS_DEFAULT(COMREF_OPERATOR, Category)
};
#endif

void GdBIANIMImport::RegisterToGameState(GameState *gState)
{
  gState->NewOperators(UnaryFuncts,lenof(UnaryFuncts));
#if DOCUMENT_COMREF
  gState->AddComRefFunctions(DefaultComRefFunc,lenof(DefaultComRefFunc));
#endif
}
