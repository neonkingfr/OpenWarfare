#ifndef __O2LIB_USE_IEXTERNALVIEWER__H__
#define __O2LIB_USE_IEXTERNALVIEWER__H__

#ifndef _WINDEF_
//compatibility definitions

typedef unsigned long DWORD;
typedef DWORD   COLORREF;
typedef DWORD   *LPCOLORREF;

#endif
                  \
namespace ObjektivLib {

class LODObject;

class IExternalViewer
{
public:
  enum ErrorEnum
  {
    ErrOK=0,                ///< Operation completted succesfully
    ErrCreateProcessFail=1, ///< Unable to start viewer, create process failed
    ErrBusy=2,              ///< Update process is busy, try it later
    ErrNotInicialized=3,    ///< Viewer is not inicialized, use Start to inicialize viewer
    ErrAttachFailed=4,      ///< Attach Failed, no viewer found
    ErrNoConfig=6,          ///< Call SetViewerSettings
    ErrTimeout=7,           ///< Various situation. Waiting for critical situation timeouted
    ErrFailed=8,             ///< Operation Failed, or condition not TRUE
    ErrInitFailed=9,        ///< Init failed, cannot duplicate handles
    ErrLost=10              ///< Aplication has been lost, call Restart
  };
  virtual ErrorEnum Start()=0;
  virtual ErrorEnum Stop()=0;
  virtual ErrorEnum Restart()=0;
  virtual ErrorEnum Attach()=0;
  virtual ErrorEnum Detach()=0;
  virtual ErrorEnum FinalDetach()=0;

  virtual ErrorEnum Update(LODObject *lod, const char *p3dname=NULL, DWORD waittime=0)=0;
  virtual ErrorEnum StartScript(const char *scriptname)=0;
  virtual ErrorEnum IsReady()=0;

  virtual ErrorEnum SwitchToViewer()=0;
  virtual ErrorEnum HasFocus()=0;
  virtual ErrorEnum SetWeather(float overcast, float fog)=0;
  virtual ErrorEnum GetWeather(float &overcast, float &fog)=0;
  virtual ErrorEnum RedrawViewer()=0;
  virtual ErrorEnum TimeOfDay(int advance_insec, DWORD *cur_insec)=0;
  virtual ErrorEnum GetLightColors(COLORREF &ambient, COLORREF &diffuse)=0;
  virtual ErrorEnum SetMaterial(const char *matName, void *paramFileData, int size)=0;
  virtual ErrorEnum SetRTM(const char *rtmName)=0;

  struct EnvInfoStruct
  {
    enum ValueID
    {
      valGetInfo=0,
      valSetInfo=1,
      valOvercast=2,
      valFog=3,
      valEyeAccom=4,
      valTime=5
    };
    HANDLE unblock;
    int version;
    float overcast;
    float fog;
    float eyeAccom;
    unsigned long time;
    float ambient[3];
    float diffuse[3];
  };

  virtual ErrorEnum SetEnvironment(const EnvInfoStruct &env)=0;
  virtual ErrorEnum GetEnvironment(EnvInfoStruct &env) const=0;
  virtual ErrorEnum SetEnvironmentValue(EnvInfoStruct::ValueID valueId, float f)=0;
  virtual ErrorEnum SetEnvironmentValue(EnvInfoStruct::ValueID valueId, long l)=0;
  virtual ErrorEnum SetViewerControler(const char *controlerName, float value, float *minval=0, float *maxval=0)=0;

};
                  }
#endif