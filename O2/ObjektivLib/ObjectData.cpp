
// keyframe animations
// (C) Ondrej Spanel, Suma

#include "Common.hpp"
#include "data3d.h"

#include <El/BTree/BTree.h>
#include "ObjectData.h"
#include "Edges.h"

#include "GlobalFunctions.h"
#include "ObjToolClipboard.h"


///*********** objAnim.cpp ***************

namespace ObjektivLib {


void PointAttrib<Vector3>::Clear( int i )
{
  _attrib[i]=VZero;
}

int ObjectData::NAnimations() const
{
  return _phase.Size();
}

//--------------------------------------------------

AnimationPhase *ObjectData::GetAnimation( int i ) const
{
  return _phase[i];
}

//--------------------------------------------------

int ObjectData::NearestAnimationIndex( float time ) const
{
  int i,iMin=-1;
  float minDiff=1e10;
  for( i=0; i<_phase.Size(); i++ )
  {
    float diff=fabs(_phase[i]->GetTime()-time);
    if( minDiff>diff ) minDiff=diff,iMin=i;
  }
  return iMin;
}

//--------------------------------------------------

int ObjectData::AnimationIndex( float time ) const
{
  int i,iMin=0;
  float minDiff=1e10;
  for( i=0; i<_phase.Size(); i++ )
  {
    float diff=fabs(_phase[i]->GetTime()-time);
    if( minDiff>diff ) minDiff=diff,iMin=i;
  }
  if( minDiff<1e-4 ) return iMin;
  return -1;
  //return iMin;
}

//--------------------------------------------------

bool ObjectData::AddAnimation( const AnimationPhase &src )
{
  if( AnimationIndex(src.GetTime())>=0 ) return false;
  AnimationPhase *phase=new AnimationPhase(src);
  phase->Validate();
  _phase.Add(phase);
  SetDirty();
  return true;
}

//--------------------------------------------------

#if 1
static int CmpPhaseBin( const void *p0, const void *p1 )
{
  const Ref<AnimationPhase> phase0=*(const Ref<AnimationPhase> *)p0;
  const Ref<AnimationPhase> phase1=*(const Ref<AnimationPhase> *)p1;
  if( phase0->GetTime()>phase1->GetTime() ) return +1;
  if( phase0->GetTime()<phase1->GetTime() ) return -1;
  return 0;
}

//--------------------------------------------------

#else
static int CmpPhase
(
 const Ref<AnimationPhase> &p0,
 const Ref<AnimationPhase> &p1
 )
{
  if( p0->GetTime()>p1->GetTime() ) return +1;
  if( p0->GetTime()<p1->GetTime() ) return -1;
  return 0;
}

//--------------------------------------------------

#endif

bool ObjectData::SortAnimations()
{
  bool changed=false;
  for( int i=0; i<_phase.Size()-1; i++ )
  {
    if( CmpPhaseBin(&_phase[i],&_phase[i+1])<0 ) changed=true;
    //if( CmpPhase(_phase[i],_phase[i+1])<0 ) changed=true;
  }
  if( changed )
  {
    float current=0;
    if( _autoSaveAnimation>=0 )
    {
      current=_phase[_autoSaveAnimation]->GetTime();
    }
    //qsort(_phase.Data(),_phase.Size(),sizeof(*_phase.Data()),CmpPhase);
    _phase.QSortBin(CmpPhaseBin);
    if( _autoSaveAnimation>=0 )
    {
      _autoSaveAnimation=AnimationIndex(current);
      current=_phase[_autoSaveAnimation]->GetTime();
    }
  }
  return changed;
}

//--------------------------------------------------


//--------------------------------------------------

//--------------------------------------------------


//--------------------------------------------------

bool ObjectData::RenameAnimation( float oldTime, float newTime )
{
  if( AnimationIndex(newTime)>=0 ) return false;
  int index=AnimationIndex(oldTime);
  if( index<0 ) return false;
  _phase[index]->SetTime(newTime);
  SetDirty();
  return true;
}

//--------------------------------------------------

bool ObjectData::DeleteAnimation( float time )
{
  int index=AnimationIndex(time);
  if( index<0 ) return false;
  _phase.Delete(index);
  if( _autoSaveAnimation==index ) _autoSaveAnimation=-1;
  if( _autoSaveAnimation>index ) _autoSaveAnimation--;
  SetDirty();
  return true;
}

//--------------------------------------------------

void ObjectData::DeleteAllAnimations()
{
  UseAnimation(-1);
  _phase.Clear();
}

//--------------------------------------------------

void ObjectData::RedefineAnimation( AnimationPhase &anim )
{
  int i;
  anim.Validate();
  for( i=0; i<anim.N(); i++ )
  {
    anim[i]=Point(i);
  }
}

//--------------------------------------------------

void ObjectData::RedefineAnimation( int i )
{
  SetDirty();
  RedefineAnimation(*_phase[i]);
  _autoSaveAnimation=i;
}

//--------------------------------------------------

void ObjectData::UseAnimation( int index )
{
  if( _autoSaveAnimation>=0 )
  {
    RedefineAnimation(*_phase[_autoSaveAnimation]);
  }
  if( index>=0 )
  {
    AnimationPhase &anim=*_phase[index];
    UseAnimation(anim);
  }
  _autoSaveAnimation=index;
}

//--------------------------------------------------

void ObjectData::UseAnimation( AnimationPhase &anim )
{
  for( int i=0; i<anim.N(); i++ )
  {
    if( anim[i]!=Point(i) )
    {
      if( _autoSaveAnimation<0 ) SetDirty();
      Point(i).SetPoint(anim[i]);
    }
  }
}

//--------------------------------------------------


//--------------------------------------------------

//--------------------------------------------------

//--------------------------------------------------
//--------------------------------------------------

RString ObjectData::Description() const
{
  if( _autoSaveAnimation<0 ) return "";
  char buff[200];
  snprintf(buff,sizeof(buff),", %d:%.6f",_autoSaveAnimation,_phase[_autoSaveAnimation]->GetTime());
  return RString(buff);
}

//--------------------------------------------------

///*********** objMass.cpp ***************

void PointAttrib<float>::Clear( int i )
{
  _attrib[i]=0;
}

//--------------------------------------------------

void ObjectData::SetSelectionMass( double mass, bool constTotal )
{
  int i;
  double total=0;
  double rest=0;
  double change=0;
  int nSel=0,nRest=0;
  _mass.Validate(NPoints());
  for( i=0; i<NPoints(); i++ )
  {
    if( PointSelected(i) ) total+=_mass[i],nSel++;
    else rest+=_mass[i],nRest++;
  }
  if( nSel<=0 ) return;
  if( total<1e-3 )
  {
    double vertexMass=mass/nSel;
    for( i=0; i<NPoints(); i++ ) if( PointSelected(i) ) _mass[i]=(float)vertexMass;
  }
  else
  {
    float massRatio=(float)(mass/total);
    for( i=0; i<NPoints(); i++ ) if( PointSelected(i) )
    {
      double newMass=massRatio*_mass[i];
      change+=newMass-_mass[i];
      _mass[i]=(float)newMass;
    }
  }
  if( constTotal && nRest>0 )
  { // leave total mass unchanged
    float restRatio=(float)((rest-change)/rest);
    if( restRatio>0 && _finite(restRatio))
    {
      for( i=0; i<NPoints(); i++ ) if( !PointSelected(i) )
      {
        double newMass=restRatio*_mass[i];
        //change+=newMass-_mass[i];
        _mass[i]=(float)newMass;
      }
    }
  }
  SetDirty();
}

//--------------------------------------------------

double ObjectData::GetSelectionMass() const
{
  double total=0;
  for( int i=0; i<_mass.N(); i++ ) if( PointSelected(i) ) total+=_mass[i];
  return total;
}

//--------------------------------------------------

double ObjectData::GetTotalMass() const
{
  double total=0;
  for( int i=0; i<_mass.N(); i++ ) total+=_mass[i];
  return total;
}

//--------------------------------------------------

double ObjectData::GetMaxMass() const
{
  double total=0;
  for( int i=0; i<_mass.N(); i++ ) if (total<_mass[i]) total=_mass[i];
  return total;
}

//--------------------------------------------------

VecT ObjectData::GetMassCentre(bool selection)
{
  double max=selection?GetSelectionMass():GetTotalMass();
  VecT ps(0,0,0);
  for (int i=0;i<_mass.N();i++)
    if (!selection || PointSelected(i))
    {
      PosT &p=Point(i);
      ps+=p*_mass[i]/(Coord)max;
    }
    return ps;
}

//--------------------------------------------------

const char *ObjectData::GetNamedProp( const char *name ) const
{
  for( int i=0; i<MAX_NAMED_PROP; i++ )
  {
    if( _namedProp[i] )
    {
      if( !_strcmpi(_namedProp[i]->Name(),name) ) return _namedProp[i]->Value();
    }
  }
  return NULL;
}

//--------------------------------------------------

bool ObjectData::SetNamedProp( const char *name, const char *value )
{
  int i;
  for( i=0; i<MAX_NAMED_PROP; i++ )
  {
    if( _namedProp[i] )
    {
      if( !_strcmpi(_namedProp[i]->Name(),name) )
      {
        _namedProp[i]->SetValue(value);
        SetDirty();
        return true;
      }
    }
  }
  for( i=0; i<MAX_NAMED_PROP; i++ )
  {
    if( !_namedProp[i] )
    {
      _namedProp[i]=new NamedProperty(name,value);
      SetDirty();
      return true;
    }
  }
  return false;
}

//--------------------------------------------------

const char *ObjectData::GetNamedProp( int i ) const
{
  if( !_namedProp[i] ) return NULL;
  return _namedProp[i]->Name();
}

const char *ObjectData::GetNamedPropValue( int i ) const
{
  if( !_namedProp[i] ) return NULL;
  return _namedProp[i]->Value();
}
//--------------------------------------------------

bool ObjectData::DeleteNamedProp( const char *name )
{
  int i;
  for( i=0; i<MAX_NAMED_PROP; i++ )
  {
    if( _namedProp[i] )
    {
      if( !_strcmpi(_namedProp[i]->Name(),name) )
      {
        delete _namedProp[i];
        _namedProp[i]=NULL;
        SetDirty();
        return true;
      }
    }
  }
  return false;
}

//--------------------------------------------------

bool ObjectData::RenameNamedProp( const char *name, const char *newname )
{
  if( GetNamedProp(newname) ) return false;
  int i;
  for( i=0; i<MAX_NAMED_PROP; i++ )
  {
    if( _namedProp[i] )
    {
      if( !_strcmpi(_namedProp[i]->Name(),name) )
      {
        _namedProp[i]->SetName(newname);
        SetDirty();
        return true;
      }
    }
  }
  return false;
}

//--------------------------------------------------

///*********** ObjObje ***************

void ObjectData::DoConstruct()
{
  _points.Clear();
  _normals.Clear();
  _faces.Clear();

  _sel.Clear();
  _hide.Clear();
  _lock.Clear();
  _mass.Clear();
  _sharpEdge.Clear();

  _dirty=false;

  _autoSaveAnimation=-1;

  int i;
  for( i=0; i<MAX_NAMED_SEL; i++ ) _namedSel[i]=NULL;
  for( i=0; i<MAX_NAMED_PROP; i++ ) _namedProp[i]=NULL;
  _phase.Clear();
  _activeUVSet=AddUVSet(0);
  if (_activeUVSet==0) _activeUVSet=GetUVSet(0);
}

void ObjectData::DoDestruct()
{
  int i;
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( _namedSel[i] ) delete _namedSel[i],_namedSel[i]=NULL;
  }
  for( i=0; i<MAX_NAMED_PROP; i++ )
  {
    if( _namedProp[i] ) delete _namedProp[i],_namedProp[i]=NULL;
  }
}

// copy constructor
void ObjectData::DoCopy( const ObjectData &src )
{
  _points=src._points;
  _faces=src._faces;
  _normals=src._normals;

  _sel=src._sel;
  _sel.SetOwner(this);

  _hide=src._hide;
  _hide.SetOwner(this);

  _lock=src._lock;
  _lock.SetOwner(this);

  _mass=src._mass;
  _mass.SetObject(this);

  _sharpEdge=src._sharpEdge;

  _dirty=src._dirty;

  int i;
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( src._namedSel[i] ) 
    {
      // Create new named selection according to source object
      _namedSel[i]=new NamedSelection(*src._namedSel[i], src._namedSel[i]->Name());
      if( _namedSel[i] ) _namedSel[i]->SetOwner(this);
    }
    else _namedSel[i]=NULL;
  }
  for( i=0; i<MAX_NAMED_PROP; i++ )
  {
    if( src._namedProp[i] ) 
    {
      _namedProp[i]=new NamedProperty(*src._namedProp[i]);
    }
    else _namedProp[i]=NULL;
  }
  for( i=0; i<src._phase.Size(); i++ )
  {
    _phase.Add(new AnimationPhase(*src._phase[i],this));
  }
  _autoSaveAnimation=src._autoSaveAnimation;
  _uvsets.Clear();
  for (int i=0;i<src._uvsets.Size();i++)
  {
    _uvsets.Append()=new ObjUVSet(*(src._uvsets[i]),this);
    if (src._activeUVSet==src._uvsets[i])
      _activeUVSet=_uvsets[i];
  }

  // copy metadata
  for (int i = 0; i < src._metadata.Size(); ++i)
    _metadata.Add(new AnimationMetadata(src._metadata[i]->name, src._metadata[i]->value));
  for (int i = 0; i < src._keyStone.Size(); ++i)
    _keyStone.Add(new AnimationKeyStone(src._keyStone[i]->time, src._keyStone[i]->key, src._keyStone[i]->value));
}


void ObjectData::DoCopy( const ObjectData &src, int animationPhase )
{
  DoCopy(src);
  // use data from animationPhase
  if( animationPhase>=0 )
  {
    UseAnimation(animationPhase);
    _phase.Clear();
    _autoSaveAnimation=-1;
  }
}

PosT *ObjectData::NewPoint()
{
  int index=ReservePoints(1);
  return &_points[index];
}

VecT *ObjectData::NewNormal()
{
  int index=_normals.Add(Vector3(0,1,0));
  return &_normals[index];
}

int ObjectData::NewFace()
{
  int index=ReserveFaces(1);
  return index;
}

int ObjectData::ReservePoints( int count )
{
  int i,j;

  int oldPoints=_points.Size();
  int newPoints=oldPoints+count;
  _points.Resize(newPoints);
  for( j=oldPoints; j<newPoints; j++ )
  {
    _points[j][0]=0;
    _points[j][1]=0;
    _points[j][2]=0;
    _points[j].flags=0;
  }
  _sel.ValidatePoints();
  _lock.ValidatePoints();
  _hide.ValidatePoints();
  _mass.Validate();
  for( i=0; i<NAnimations(); i++ ) _phase[i]->Validate();
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( _namedSel[i] ) _namedSel[i]->ValidatePoints();
  }
  return oldPoints;
}

int ObjectData::ReserveFaces( int count )
{
  int i,j;
  int oldFaces=_faces.Size();
  int newFaces=oldFaces+count;
  _faces.Resize(newFaces);
  for( j=oldFaces; j<newFaces; j++ )
  {
    _faces[j].n=0;
    _faces[j].flags=0;
    for (int k=0;k<4;k++) memset(_faces[j].vs+k,0,sizeof(_faces[j].vs[0]));
  }
  _sel.ValidateFaces(newFaces);
  _lock.ValidateFaces(newFaces);
  _hide.ValidateFaces(newFaces);
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( _namedSel[i] ) _namedSel[i]->ValidateFaces(newFaces);
  }
  return oldFaces;
}




void ObjectData::Triangulate( bool allFaces )
{
  int nFaces=NFaces();
  for( int i=0; i<nFaces; i++ ) if( allFaces || FaceSelected(i) )
  {
    FaceT face(this,i);
    if( face.N()<=3 ) continue;
    FaceT nFace(this,NewFace()); // may change pointers into face
    face.CopyFaceTo(nFace);
    nFace.CopyPointInfo(0,face,0);
    nFace.CopyPointInfo(1,face,2);
    nFace.CopyPointInfo(2,face,3);
    face.SetN(3);
    nFace.SetN(3);
    SetDirty();
    // original face is selected - new face should be selected too
    int iFace=nFace.GetFaceIndex();
    if( FaceSelected(i) )		  
      FaceSelect(iFace);		  

    // ADDED by BREDY - BEGIN
    // adjust all named selections - original face is selected - new face should be selected too
    for (int p=0;p<MAX_NAMED_SEL;p++)
    {
      Selection *sel=(Selection *)GetNamedSel(p);
      if (sel && sel->FaceSelected(i)) sel->FaceSelect(iFace);

    }
    // END
  }
}

void ObjectData::Triangulate(TriangulateMode mode, Selection *sel) {  
    if (sel == 0) sel = &_sel;
    AutoArray<int> faces = sel->GetSelectedFaces();
    ProgressBar<int> pb(faces.Size());
    for( int i=0; i<faces.Size(); i++ ) 
    {
        pb.AdvanceNext(1);
        FaceT face(this,faces[i]);
        if( face.N()<=3 ) continue;

        bool reversed;
        if (mode == tmNormal) reversed = false;
        else if (mode == tmReversed) reversed = true;
        else {
            Vector3 n1 = face.CalculateNormalAtPointAdjusted(1);
            float d1 = -face.GetPointVector<Vector3>(1).DotProduct(n1);
            float d2 = -face.GetPointVector<Vector3>(3).DotProduct(n1);
            reversed = d1 < d2 == (mode == tmConvex);
        }

        FaceT nFace(this,NewFace()); // may change pointers into face
        face.CopyFaceTo(nFace);

        if (reversed) {

            nFace.CopyPointInfo(0,face,1);
            nFace.CopyPointInfo(1,face,2);
            nFace.CopyPointInfo(2,face,3);
            face.CopyPointInfo(2,face,3);
        } else {
            nFace.CopyPointInfo(0,face,0);
            nFace.CopyPointInfo(1,face,2);
            nFace.CopyPointInfo(2,face,3);
        }
        face.SetN(3);
        nFace.SetN(3);
        SetDirty();
        // original face is selected - new face should be selected too
        int iFace=nFace.GetFaceIndex();
        if( FaceSelected(faces[i]) )		  
            FaceSelect(iFace);		  

        // ADDED by BREDY - BEGIN
        // adjust all named selections - original face is selected - new face should be selected too
        for (int p=0;p<MAX_NAMED_SEL;p++)
        {
            Selection *sel=(Selection *)GetNamedSel(p);
            if (sel && sel->FaceSelected(faces[i])) sel->FaceSelect(iFace);

        }
        // END
        // adjust current selection
        sel->FaceSelect(iFace);
    }

}

void ObjectData::Squarize( bool allFaces )
{
  // restore square faces from triangles
  int i,j;
  for( i=NFaces(); --i>=0; )  if( allFaces || FaceSelected(i) )
    for( j=i; --j>=0; )  if( allFaces || FaceSelected(j) )
    {
      int k;
      //if( smoothing[i]!=smoothing[j] ) continue;
      FaceT iFace(this,i);
      FaceT jFace(this,j);
      if( iFace.N()!=3 ) continue;
      if( jFace.N()!=3 ) continue;
      // merge two triangle into one rectangle
      // there must be two common vertices
      // each triangle should contain one unique vertex
      int iC[2],jC[2];
      int cs=0;
      for( k=0; k<3; k++ )
      {
        int atInJ=jFace.PointAt(iFace.GetPoint(k));
        if( atInJ>=0 )
        {
          jC[cs]=atInJ;
          iC[cs]=k;
          cs++;
          if( cs>=2 ) break;
        }
      }
      if( cs<2 ) continue; // no two common points
      // check fo coplanarity of the faces using CalculateNormal() function
      Vector3 iNormal=iFace.CalculateNormal();
      Vector3 jNormal=jFace.CalculateNormal();

      // if plane is planar, both normals should be same
      // calculate cos(fi) - angle between normals
      double cosFi=iNormal*jNormal;
      if( cosFi<0.999 )
      {
        //double cosMin1=cosFi-1;
        continue; // 0.999 is cos(2.5 degree)
      }

      // following conditions are true:
      // iC[0]<iC[1], iC[0]<=1

      // check for some condition we need to be true
      if( jC[0]==jC[1] ) continue;
      // the points must be identical to merge
      if( iFace.GetNormal(iC[0])!=jFace.GetNormal(jC[0]) ) continue;
      if( iFace.GetNormal(iC[1])!=jFace.GetNormal(jC[1]) ) continue;
      if( fabs(iFace.GetU(iC[0])-jFace.GetU(jC[0]))>0.001 ) continue;
      if( fabs(iFace.GetV(iC[1])-jFace.GetV(jC[1]))>0.001 ) continue;
      // rotate faces so that iC[0],jC[0] is 0
      while( iC[0]>0 )
      {
        iFace.Shift1To0();
        iC[0]=ModuloCounting(iC[0]+2,3);
        iC[1]=ModuloCounting(iC[1]+2,3);
      }
      while( jC[0]>0 )
      {
        jFace.Shift1To0();
        jC[0]=ModuloCounting(jC[0]+2,3);
        jC[1]=ModuloCounting(jC[1]+2,3);
      }
      // four possible situations: iC[1],jC[1]==1 or 2
      // only two of them enable concatenation
      if( iC[1]==jC[1] ) continue; // different orientation
      // from j take point which is neither jC[0] nor jC[1]
      int jUnique=0;
      for( k=0; k<3; k++ ) if( jC[0]!=k &&  jC[1]!=k ) jUnique=k;
      FaceT nFace=iFace;
      if( iC[1]==1 )
      {
        // insert unique j point between iC[0] and iC[1]
        nFace.CopyPointInfo(3,iFace,2);
        nFace.CopyPointInfo(2,iFace,1);
        nFace.CopyPointInfo(1,jFace,jUnique);
      }
      else // iC[1]==2
      {
        nFace.CopyPointInfo(3,jFace,jUnique);        
      }
      nFace.SetN(4);
      // check if result is convex
      // note: concave would have some angle>0
      if( nFace.IsConvex() )
      {
        nFace.CopyFaceTo(jFace);
        DeleteFace(i);
        break; // break j loop - i face is already matched
      }
    }
    if( NNormals()<=0 ) NewNormal();
    //RecalcNormals();
}

typedef struct
{
  char magic[4]; // "SS3D"
  int sizeVert,sizeFace,sizeNorm,sizeEdge;
} SetHead;

int ObjectData::SaveData( ostream &f, bool final, int version )
{
  DataHeaderEx head;
  if (version==0)
  {
    strncpy(head.magic,"SP3X",4);
    head.version=0x099;
  }
  else
  {
    strncpy(head.magic,"P3DM",4);
    head.version=0x100;
  }
  head.nPos=NPoints();
  head.nNorm=NNormals();
  head.nFace=NFaces();
  head.flags=0;
  head.headSize=sizeof(head);
  f.write((char *)&head,sizeof(head));

  int i;
  for( i=0; i<NPoints(); i++ )
  {
    DataPointEx point;
    point=Point(i);
    f.write((char *)&point,sizeof(point));
  }
  for( i=0; i<NNormals(); i++ )
  {
    DataNormal norm;
    norm=Normal(i);
    f.write((char *)&norm,sizeof(norm));
  }
  ObjUVSet *setZero=GetUVSet(0);
  for( i=0; i<NFaces(); i++ )
  {
    DataFaceEx face=Face(i);
    if (version==0)
    {
      DataFaceExOldVersion fold;
      fold.flags=face.flags;
      strncpy(fold.texture,face.vTexture,32);
      fold.n=face.n;
      for (int j=0;j<fold.n;j++)
      {
        fold.vs[j].point=face.vs[j].point;
        fold.vs[j].normal=face.vs[j].normal;
        fold.vs[j].mapU=setZero->GetUV(i,j).u;
        fold.vs[j].mapV=setZero->GetUV(i,j).v;
      }
      f.write((char *)&fold,sizeof(fold));
    }
    else
    {
      f.write((char *)&face.n,sizeof(face.n));
      for (int v=0;v<MAX_DATA_POLY;v++)
      {      
        f.write((char *)(face.vs+v),sizeof(face.vs[v]));
        f.write((char *)&(setZero->GetUV(i,v)),sizeof(ObjUVSet_Item));
      }
      f.write((char *)&face.flags,sizeof(face.flags));
      f.write(face.vTexture,face.vTexture.GetLength()+1);
      f.write(face.vMaterial,face.vMaterial.GetLength()+1);
    }
  }
  if( f.fail() ) return -1;
  return 0;
}


void ObjectData::SaveTag( ostream &f, const char *tag, int size , bool nolongtag)
{
  if (nolongtag)
  {
    char tagBuf[64];
    strncpy(tagBuf,tag,sizeof(tagBuf));
    f.write(tagBuf,sizeof(tagBuf));
    f.write((char *)&size,sizeof(size));
  }
  else
  {
    f.put('\x01');
    while (*tag) {f.put(*tag++);}
    f.put(*tag);
    f.write((char *)&size,sizeof(size));
  }
}

RString ObjectData::LoadTag( istream &f, int &size )
{ // load sized tag  
  RString out;
  int zn=f.get();
  if (zn=='\x01')
  {
    char smallbuff[256];
    char *end=smallbuff+255;
    char *cur=smallbuff;
    int remain=64;
    zn=f.get();
    while (zn>0) 
    {
      *cur++=(char)zn;
      if (cur==end) 
      {
        *cur=0;
        out=out+smallbuff;
        cur=smallbuff;
      }
      zn=f.get();
    }
    *cur=0;
    out=out+smallbuff;
  }
  else
  {
    f.putback((char)zn);
    char tagBuf[64+1];
    f.read(tagBuf,64);
    tagBuf[64]=0;
    out=tagBuf;
  }
  f.read((char *)&size,sizeof(size));
  return out;
}

bool ObjectData::FindTag( istream &f, const char *name, int &size )
{
  int tagSize;
  for(;;)
  {
    RString tag=LoadTag(f,tagSize);
    if( f.fail() || f.eof() ) return false;
    if( !_strcmpi(tag,name) )
    {
      size=tagSize;
      return true;
    }
    else f.seekg(tagSize,ios::cur);
  }
}

static RString LoadOldTag( istream &f )
{ // load old tag
  char tagBuf[64+1];
  f.read(tagBuf,64);
  tagBuf[64]=0;
  return tagBuf;
}

int ObjectData::SaveSetup( ostream &f, bool final, bool mass, bool nolongtags )
{
  f.write("TAGG",4);

  int selSize=NPoints()*sizeof(bool)+NFaces()*sizeof(bool);

  int edgeSize=_sharpEdge.Size()*sizeof(int)*2;
  // consider skip sharp edges in finalized object
  //if( !final )
  if (edgeSize!=0) 
  {
    SaveTag(f,"#SharpEdges#",edgeSize,nolongtags);
    f.write((char *)_sharpEdge.Data(),edgeSize);
  }

  if( !_sel.IsEmpty() && !final )
  {
    SaveTag(f,"#Selected#",selSize,nolongtags);
    _sel.Save(f);
  }

  int i;
  for( i=0; i<MAX_NAMED_SEL; i++ ) if( _namedSel[i] && !_namedSel[i]->IsEmpty())
  {
    if( final && _namedSel[i]->Name()[0]=='.' ) continue;
    if( final && _namedSel[i]->Name()[0]=='-' ) continue;
    if(_namedSel[i]->Name()[0]=='!' ) continue;
    SaveTag(f,_namedSel[i]->Name(),selSize,nolongtags);
    _namedSel[i]->Save(f);
  }
  for( i=0; i<MAX_NAMED_PROP; i++ ) if( _namedProp[i] )
  {
    if( final && _namedProp[i]->Name()[0]=='.' ) continue;
    if( final && _namedProp[i]->Name()[0]=='-' ) continue;
    if(_namedProp[i]->Name()[0]=='!' ) continue;
    SaveTag(f,"#Property#",sizeof(*_namedProp[i]),nolongtags);
    f.write((char *)_namedProp[i],sizeof(*_namedProp[i]));
  }

  if( f.fail() ) return -1;

  if( !_lock.IsEmpty()  && !final )
  {
    SaveTag(f,"#Lock#",selSize,nolongtags);
    _lock.Save(f);
  }

  if( !_hide.IsEmpty()  && !final )
  {
    SaveTag(f,"#Hide#",selSize,nolongtags);
    _hide.Save(f);
  }
  /*#ifndef FULLVER

  SaveTag(f,"#MaterialIndex#",16);
  f.write((char *)&prot_componumber,sizeof(prot_componumber));
  f.write((char *)&prot_magicnumber,sizeof(prot_magicnumber));
  f.write((char *)&prot_unlockcode,sizeof(prot_unlockcode));
  f.write((char *)&prot_requestcode,sizeof(prot_requestcode));
  #endif*/

  if( mass )
  {
    int massSize=NPoints()*sizeof(float);
    SaveTag(f,"#Mass#",massSize,nolongtags);
    _mass.Save(f);
  }

  for( i=0; i<NAnimations(); i++ )
  {
    float time=_phase[i]->GetTime();
    int taggSize=sizeof(time)+sizeof(Vector3)*NPoints();
    SaveTag(f,"#Animation#",taggSize,nolongtags);
    f.write((char *)&time,sizeof(time));
    _phase[i]->Save(f);
  }

  int uvSetTagSize=-1;
  for (i=0;i<_uvsets.Size();i++)
  {
    if (uvSetTagSize==-1)
      uvSetTagSize=_uvsets[i]->CalculateSaveSize();
    SaveTag(f,"#UVSet#",uvSetTagSize,nolongtags);
    _uvsets[i]->Save(f);
  }

  SaveTag(f,"#EndOfFile#",0,nolongtags);

  if( f.fail() ) return -1;
  return 0;
}

int ObjectData::SaveBinary( ostream &f, int version, bool final, bool mass )
{
  int ret=-1;
  if( _autoSaveAnimation>=0 ) RedefineAnimation(_autoSaveAnimation);
  ret=SaveData(f,final,version);
  if( ret==0 ) ret=SaveSetup(f,final,mass,version==0);
  // do not clear dirty - save may be export or save for object viewer ...
  return ret;
}

int ObjectData::Save(const Pathname &filename, bool final, bool mass )
{
  // save in binary Poseidon format
  RecalcNormals();
  RString ext=filename.GetExtension();
  ofstream f(filename,ios::binary|ios::out);
  return SaveBinary(f,final,mass);
}

int ObjectData::LoadData( istream &f )
{
  DoDestruct();
  DoConstruct();

  DataHeaderEx head;
  bool extended=true;
  f.read((char *)&head,sizeof(head));
  int version=0;
  if (strncmp(head.magic,"P3DM",4)==0) 
  {
    version=1;
    //	  if (head.version!=0x100) return -1; //invalid version
  }
  if( strncmp(head.magic,"SP3D",4)==0 && version==0 )
  {
    extended=false;
    DataHeader oHead;
    f.seekg((size_t)f.tellg()-sizeof(head),ios_base::beg);
    f.read((char *)&oHead,sizeof(oHead));
    if( strncmp(head.magic,"SP3D",4)  ) return -1; // file input error 
    head.nPos=oHead.nPos;
    head.nNorm=oHead.nNorm;
    head.nFace=oHead.nFace;
    head.version=0;
    head.headSize=sizeof(oHead);
  }
  else
  {
    f.seekg(head.headSize-sizeof(head),ios::cur);		
  }
  _points.Realloc(head.nPos);
  _points.Resize(head.nPos);
  _normals.Realloc(head.nNorm);
  _normals.Resize(head.nNorm);
  _faces.Realloc(head.nFace);
  _faces.Resize(head.nFace);

  int i;
  for( i=0; i<NPoints(); i++ )
  {
    if( !extended )
    {
      DataPoint point;
      f.read((char *)&point,sizeof(point));
      if( f.fail() ) break;
      //Assert( point.Size()>0 );
      Point(i).SetPoint(point);
      Point(i).flags=0;
    }
    else
    {
      DataPointEx point;
      f.read((char *)&point,sizeof(point));
      if( f.fail() ) break;
      Point(i)=point;
      //Assert( point.Size()>0 );
    }
  }
  for( i=0; i<NNormals(); i++ )
  {
    DataNormal norm;
    f.read((char *)&norm,sizeof(norm));
    if( f.fail() ) break;
    Normal(i)=norm;
  }  
  ObjUVSet *zeroSet=GetUVSet(0);
  _activeUVSet=zeroSet;
  for( i=0; i<NFaces(); i++ )
  {
    DataFaceEx face;
    if( !extended )
    {
      DataFace oFace;
      f.read((char *)&oFace,sizeof(oFace));
      if( f.fail() ) break;
      (DataFace)face=oFace;
      face.flags=0;
    }
    else
    {
      if (version==1)
      {
        f.read((char *)&face.n,sizeof(face.n));
        for (int v=0;v<MAX_DATA_POLY;v++)
        {      
          f.read((char *)(face.vs+v),sizeof(face.vs[v]));
          ObjUVSet_Item uv;
          f.read((char *)&uv,sizeof(ObjUVSet_Item));
          zeroSet->SetUV(i,v,uv);
        }
        f.read((char *)&face.flags,sizeof(face.flags));
        char buff[9000];
        f.getline(buff,9000,0);_strlwr(buff);face.vTexture=buff;
        f.getline(buff,9000,0);_strlwr(buff);face.vMaterial=buff;
        Face(i) = face;
      }
      else
      {
        DataFaceExOldVersion fold;
        f.read((char *)&fold,sizeof(fold));
        if( f.fail() ) break;

        FaceT nwfc(this,i);
        nwfc.SetTexture(fold.texture);
        nwfc.SetN(fold.n);
        nwfc.SetFlags(fold.flags);
        for (int i=0;i<fold.n;i++)
        {
          nwfc.SetPoint(i,fold.vs[i].point);
          nwfc.SetNormal(i,fold.vs[i].normal);
          nwfc.SetUV(i,fold.vs[i].mapU,fold.vs[i].mapV);
        }
      }
    }
  }
  if( f.fail() ) return -1;
  return 0;
}

int ObjectData::LoadSetup( istream &f )
{
  char tagMagic[4];
  f.read(tagMagic,sizeof(tagMagic));
  if( strncmp(tagMagic,"TAGG",4) )
  {
    if( f.fail() || f.eof() ) return true; // no setup available
    SetHead setHead;
    f.seekg(-4,ios::cur);
    f.read((char *)&setHead,sizeof(setHead));
    if // check if it is old (non-tagged) format
      (
      f.fail()
      || strncmp(setHead.magic,"SS3D",4)
      )
    {
      return -1;
    }
    _sel.Load(f,setHead.sizeVert,setHead.sizeFace,setHead.sizeNorm);
    int nSEdges=setHead.sizeEdge/sizeof(SharpEdge);
    _sharpEdge.Resize(nSEdges);
    f.read((char *)_sharpEdge.Data(),setHead.sizeEdge);

    _mass.Clear(); // default mass

    int i;
    for( i=0; ;)
    {
      RString nameSel=LoadOldTag(f);
      if( f.eof() || f.fail() ) return 0;
      if( nameSel[0]=='$' ) // some tag
      {  // old tags - no size informations
        if( !_strcmpi(nameSel,"$Lock$") )
        {
          _lock.Load(f,setHead.sizeVert,setHead.sizeFace,setHead.sizeNorm);
        }
        else if( !_strcmpi(nameSel,"$Hide$") )
        {
          _hide.Load(f,setHead.sizeVert,setHead.sizeFace,setHead.sizeNorm);
        }
        else if( !_strcmpi(nameSel,"$Mass$" ) ) _mass.Load(f,NPoints());
        else if( !_strcmpi(nameSel,"$EndOfFile$" ) ) return 0;
      }
      else
      { // no tags - it must be selection
        if( i<MAX_NAMED_SEL )
        {
          _namedSel[i]=new NamedSelection(this,nameSel);
          _namedSel[i]->Load(f,setHead.sizeVert,setHead.sizeFace,setHead.sizeNorm);
          i++;
        }
      }
    } // end of non-tagged read
  }
  else
  { // start tagged read
    // tagged format - we can skip any unsupported tags
    bool UvSetsLoaded=false;
    int i;
    int sizeVert=NPoints()*sizeof(byte);
    int sizeFace=NFaces()*sizeof(bool);
    for( i=0; ;)
    {
      int tagSize;
      RString nameSel=LoadTag(f,tagSize);
      if( f.eof() || f.fail() ) return -1;
      if( !_strcmpi(nameSel,"#Selected#") )
      {
        //WVERIFY( sizeVert+sizeFace==tagSize );
        _sel.Load(f,sizeVert,sizeFace,0);
      }
      else if( !_strcmpi(nameSel,"#SharpEdges#") )
      {
        int nSEdges=tagSize/(sizeof(SharpEdge));
        if (nSEdges) 
        {
            _sharpEdge.Resize(nSEdges);
            f.read((char *)_sharpEdge.Data(),tagSize);
            // normalize:
            for( int i=0; i<_sharpEdge.Size(); i++ )
            {
              SharpEdge &edge=_sharpEdge[i];
              if( edge[0]>edge[1] ) __swap(edge[0],edge[1]);
            }
            SortSharpEdges();
        }
      }
      else if( !_strcmpi(nameSel,"#Lock#") )
      {
        //WVERIFY( sizeVert+sizeFace==tagSize );
        _lock.Load(f,sizeVert,sizeFace,0);
      }
      else if( !_strcmpi(nameSel,"#Hide#") )
      {
        //WVERIFY( sizeVert+sizeFace==tagSize );
        _hide.Load(f,sizeVert,sizeFace,0);
      }
      else if( !_strcmpi(nameSel,"#Mass#" ) )
      {
        //WVERIFY(nPoints*sizeof(float)== tagSize );
        _mass.Load(f,NPoints());
      }
      else if( !_strcmpi(nameSel,"#Property#" ) )
      {
        NamedProperty prop("","");
        //WVERIFY(sizeof(prop)== tagSize );
        f.read((char *)&prop,sizeof(prop));
        SetNamedProp(prop.Name(),prop.Value());
      }
      else if( !_strcmpi(nameSel,"#Animation#" ) )
      {
        AnimationPhase anim(this);
        float time;
        f.read((char *)&time,sizeof(time));
        anim.SetTime(time);
        anim.Load(f,NPoints());
        AddAnimation(anim);
      }
      else if( !_strcmpi(nameSel,"#UVSet#" ) )
      {
        if (!UvSetsLoaded)
        {
          _uvsets.Clear();
          UvSetsLoaded=true;
          _activeUVSet=0;
        }
        ObjUVSet *set=AddUVSet(0x7FFFFFFF); //Any large number to avoid searching best id
        set->Load(f);
        if (!_activeUVSet) _activeUVSet=set;
      }
      else if( !_strcmpi(nameSel,"#EndOfFile#" ) )
      {
        f.seekg(tagSize,ios::cur);
        LoadFinished();
        return 0;
      }
      else if( nameSel.GetLength()==0 || nameSel[0]!='#' )
      { // no special tag - it must be selection
        //WVERIFY( sizeVert+sizeFace==tagSize );
        if( i<MAX_NAMED_SEL )
        {
          _namedSel[i]=new NamedSelection(this,nameSel);
          _namedSel[i]->Load(f,sizeVert,sizeFace,0);
          i++;
        }
      }
      else // some unsupported tag
      {
        f.seekg(tagSize,ios::cur);
        if( f.fail() ) return -1;
      }
      if( f.fail() ) return -1;
    }
  }

}

int ObjectData::LoadFinished()
{
  if( _phase.Size()>0 )
  {
    int index=AnimationIndex(0.0);
    if( index<0 ) index=0;
    UseAnimation(index);
  }
  return 0;
}

int ObjectData::LoadBinary( istream &f )
{
  // load binary Poseidon format
  int ret=-1;

  if( LoadData(f)<0 ) return -1;

  if( LoadSetup(f)<0 )
  {
    return -1;
  }

  _dirty=false;
  return 0;
}

int ObjectData::LoadBinary( const Pathname &filename )
{
  ifstream f;
  f.open(filename,ios::binary|ios::in);
  if( f.fail() ) return -1;
  return LoadBinary(f);
}

int ObjectData::Load( const Pathname &filename )
{
  return LoadBinary(filename);
}

bool ObjectData::EnumEdges(int &enm, int &from, int &to) const
{  
  if (enm>=_sharpEdge.Size()) return false;
  const SharpEdge &cur=_sharpEdge[enm];
  enm++;
  from=cur[0];
  to=cur[1];
  return true;
}

//************ objtopo.cpp ******************
















































void ObjectData::SelectHalfSpace( const FaceT &face )
{
  int i;
  for( i=0; i<NPoints(); i++ )
  {
    Vector3 pt=Point(i);
    PointSelect(i,face.PointInHalfSpace(pt));
  }
  for( i=0; i<NFaces(); i++ )
  {
    const FaceT iFace(this,i);
    FaceSelect(i,face.FaceInHalfSpace(iFace));
  }
}



// Changed by BREDY - Better implementation
bool ObjectData::SelectionSplit(Selection *sel)
{
  if (sel == 0) sel = &_sel;
  int i;
  int pcnt=0;
  int nmax=NPoints();
  int ncur=nmax;
  int *parray=new int[nmax];    //Alloc temporary array
  for (i=0;i<nmax;i++) parray[i]=i;
  for (i=0;i<NFaces();i++)
  {
    FaceT fc(this,i);
    if (!sel->FaceSelected(i))
      for (int j=0;j<fc.N();j++)
      {        
        int pt=fc.GetPoint(j);
        if (sel->PointSelected(pt) && parray[pt]==pt)
          parray[pt]=nmax++;      //Prepare array for adding new points
      }
  }    
  if (nmax==NPoints()) return false;
  ReservePoints(nmax-NPoints());    //Reserve new points
  for (i=0;i<ncur;i++) if (parray[i]!=i)    //Split points
  {
    PosT &ps=Point(parray[i]);
    ps=Point(i);
    for (int j=0;j<NAnimations();j++)       //Split points in animations
    {
      AnimationPhase *phs=GetAnimation(j);      
      (*phs)[parray[i]]=(*phs)[i];
    }
    sel->PointSelect(i,false);
    sel->PointSelect(parray[i],true);
  }
  for (i=0;i<MAX_NAMED_SEL;i++) if (_namedSel[i])
  {
    for (int j=0;j<ncur;j++) if (parray[j]!=j)
    {
      _namedSel[i]->SetPointWeight(parray[j],_namedSel[i]->PointWeight(j));
    }
  }
  for (i=0;i<NFaces();i++) if (sel->FaceSelected(i)) //Rebuild faces
  {
    FaceT fc(this,i);
    for (int j=0;j<fc.N();j++)
      fc.SetPoint(j,parray[fc.GetPoint(j)]);
  } 
  delete [] parray;      //delete temporary array;
  return true;
}


/*

bool ObjectData::SelectionSplit()
{
// unselect any points that are not used in any selected face
{
Temp<bool> selectedByFace(NPoints());
int i;
for( i=0; i<NPoints(); i++ ) selectedByFace[i]=false;
for( i=0; i<NFaces(); i++ ) if( FaceSelected(i) )
{
const FaceT &face=Face(i);
for( int v=0; v<face.N(); v++ ) selectedByFace[face.GetPoint(v)]=true;
}
for( i=0; i<NPoints(); i++ ) if( !selectedByFace[i] ) PointSelect(i,false);
}

// save current selection to a temporary object
SRef<ObjectData> temp=new ObjectData(*this);
temp->ExtractSelection();

Temp<bool> wasUsed(NPoints());
Temp<bool> isUsed(NPoints());
int i;
for( i=0; i<NPoints(); i++ ) wasUsed[i]=isUsed[i]=false;
// scan which points were used
for( i=0; i<NFaces(); i++ )
{
const FaceT &face=Face(i);
for( int v=0; v<face.N(); v++ ) wasUsed[face.GetPoint(v)]=true;
}
// remove all selected faces
for( i=NFaces(); --i>=0; ) if( FaceSelected(i) )
{
// scanned and convexFaces array ned be changed to reflect deletion
DeleteFace(i);
}
// scan which points are now used
for( i=0; i<NFaces(); i++ )
{
const FaceT &face=Face(i);
for( int v=0; v<face.N(); v++ ) isUsed[face.GetPoint(v)]=true;
}
// remove all points that are no longer used (but was before)
for( i=NPoints(); --i>=0; ) if( wasUsed[i] && !isUsed[i] )
{
// convexPoints array need be changed to reflect deletion
DeletePoint(i);
}
// paste in the saved selection
Merge(*temp);
// recalculate normals
RecalcNormals();
return true;
}



*/



























void ObjectData::BackfaceCull( const Vector3 &pin )
{
  // check all faces and mark backfaced
  ClearSelection();
  // select all faces
  for( int f=0; f<NFaces(); f++ )
  {
    bool culled=true;
    const FaceT face(this,f);
    // calculate face normal
    Vector3 normal=face.CalculateNormal();
    // check backface for all vertices
    Vector3 direction=((Vector3)Point(face.GetPoint(0))-pin).Normalized();
    if( normal*direction>-0.05 ) culled=false;
    /**/
    for( int p=0; p<NAnimations() && culled; p++ )
    {
      AnimationPhase &phase=*GetAnimation(p);
      Vector3 normal=face.CalculateNormal(phase);
      Vector3 direction=(phase[face.GetPoint(0)]-pin).Normalized();
      if( normal*direction>-0.05 ) culled=false;
    }
    /**/
    FaceSelect(f,culled);
  }
  SelectPointsFromFaces();
}


//*** objSelect ********/


void ObjectData::DeletePoint( int index )
{
  // point indices are important in selections and edges
  _sel.DeletePoint(index);
  int i;

  // delete in all selections
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( _namedSel[i] ) _namedSel[i]->DeletePoint(index);
  }
  _lock.DeletePoint(index);
  _hide.DeletePoint(index);
  _mass.DeletePoint(index);

  // delete in all animations
  for( i=0; i<NAnimations(); i++ )
  {
    AnimationPhase &anim=*GetAnimation(i);
    anim.DeletePoint(index);
  }


  for( int j=_sharpEdge.Size(); --j>=0; )
  {
    if( _sharpEdge[j][0]==index || _sharpEdge[j][1]==index )
    {
      _sharpEdge.Delete(j);
    }
    else
    {
      if( _sharpEdge[j][0]>index ) _sharpEdge[j][0]--;
      if( _sharpEdge[j][1]>index ) _sharpEdge[j][1]--;
    }
  }

  // scan all faces, delete all containing point index
  for( i=NFaces(); --i>=0; )
  {
    FaceT face(this,i);
    if( face.ContainsPoint(index) ) DeleteFace(i);
    else
    {
      for( int j=0; j<face.N(); j++ )
      {
        if( face.GetPoint(j)>index ) face.SetPoint(j,face.GetPoint(j)-1);
      }
    }
  }
  _points.Delete(index);
  // update point attributes
  _sel.ValidatePoints();
  _lock.ValidatePoints();
  _hide.ValidatePoints();
  _mass.Validate();
  for( i=0; i<NAnimations(); i++ ) _phase[i]->Validate();
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( _namedSel[i] ) _namedSel[i]->ValidatePoints();
  }
}

//--------------------------------------------------

void ObjectData::DeleteNormal( int index )
{
  // normal indices are important in selections
  int i;
  // scan all faces, delete all containing normal index
  for( i=NFaces(); --i>=0; )
  {
    FaceT face(this,i);
    if( face.ContainsNormal(index) ) DeleteFace(i);
    else
    {
      for( int j=0; j<face.N(); j++ )
      {
        if( face.GetNormal(j)>index ) face.SetNormal(j,face.GetNormal(j)-1);
      }
    }
  }
  _normals.Delete(index);
}

//--------------------------------------------------

void ObjectData::DeleteFace( int index )
{
  // face indices are important only in selections
  int i;
  _sel.DeleteFace(index);
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( _namedSel[i] ) _namedSel[i]->DeleteFace(index);
  }
  _lock.DeleteFace(index);
  _hide.DeleteFace(index);
  for (i=0;i<_uvsets.Size();i++) _uvsets[i]->DeleteFace(index);
  // delete face
  _faces.Delete(index);
  // update point attributes
  _sel.ValidateFaces();
  _lock.ValidateFaces();
  _hide.ValidateFaces();
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( _namedSel[i] ) _namedSel[i]->ValidateFaces();
  }
}

//--------------------------------------------------

void ObjectData::ReplacePoint( int newIndex, int oldIndex )
{
  // point indices are important in selections and edges
  for( int j=_sharpEdge.Size(); --j>=0; )
  {
    if( _sharpEdge[j][0]==oldIndex ) _sharpEdge[j][0]=newIndex;
    if( _sharpEdge[j][1]==oldIndex ) _sharpEdge[j][1]=newIndex;
  }

  SortSharpEdges();

  // scan all faces, replace all containing point index
  int i;
  for( i=NFaces(); --i>=0; )
  {
    FaceT face(this,i);
    bool degraded=false;
    for( int j=0; j<face.N(); j++ )
    {
      if( face.GetPoint(j)==newIndex ) degraded=true;
    }
    if( !degraded )
    {
      for( int j=0; j<face.N(); j++ )
      {
        if( face.GetPoint(j)==oldIndex ) face.SetPoint(j,newIndex);
      }
    }
    else
    {
      // point is already contained in face - we will only remove it
      int k, j;
      for(j=0,k=0; j<face.N(); j++ )
      {
        if( face.GetPoint(j)!=oldIndex ) face.SetPoint(k++,face.GetPoint(j));
      }
      face.SetN(k);
      if( face.N()<3 ) DeleteFace(i);
    }
  }
  // what happens in animations and selections?
  // this should be handled by merge
}

//--------------------------------------------------

void ObjectData::PermuteVertices( const int *permutation )
{
  // point indices are important in selections and edges
  for( int j=_sharpEdge.Size(); --j>=0; )
  {
    _sharpEdge[j][0]=permutation[_sharpEdge[j][0]];
    _sharpEdge[j][1]=permutation[_sharpEdge[j][1]];
    if( _sharpEdge[j][0]>_sharpEdge[j][1] )
    {
      __swap(_sharpEdge[j][0],_sharpEdge[j][1]);
    }
  }

  SortSharpEdges();

  // scan all faces, replace all containing point index
  int i;
  for( i=NFaces(); --i>=0; )
  {
    FaceT face(this,i);
    for( int j=0; j<face.N(); j++ )
    {
      face.SetPoint(j,permutation[face.GetPoint(j)]);
    }
  }

  // permute selections
  _sel.PermuteVertices(permutation);
  _hide.PermuteVertices(permutation);
  _lock.PermuteVertices(permutation);
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( _namedSel[i] ) _namedSel[i]->PermuteVertices(permutation);
  }

  // permute animations
  for( i=0; i<_phase.Size(); i++ )
  {
    _phase[i]->PermuteVertices(permutation);
  }

  // permute per-vertex attributes
  _mass.PermuteVertices(permutation);

  // permute object data
  Temp<PosT> newPos(NPoints());
  for( i=0; i<NPoints(); i++ ) newPos[i]=Point(i);
  for( i=0; i<NPoints(); i++ )
  {
    Point(permutation[i])=newPos[i];
  }

}

//--------------------------------------------------

int ObjectData::FindNormal( const VecT &norm, double maxDist )
{
  double maxDist2=maxDist*maxDist;
  int ret=-1;
  for( int i=0; i<NNormals(); i++ )
  {
    Vector3 df=norm-Normal(i);
    double dist2=df.SquareSize();
    if( dist2<maxDist2 ) maxDist2=dist2,ret=i;
  }
  return ret;
}

//--------------------------------------------------

int ObjectData::FindPoint( const VecT &point, double maxDist )
{
  double maxDist2=maxDist*maxDist;
  int ret=-1;
  for( int i=0; i<NPoints(); i++ )
  {
    Vector3 df=point-Point(i);
    double dist2=df.SquareSize();
    if( dist2<maxDist2 ) maxDist2=dist2,ret=i;
  }
  return ret;
}

//--------------------------------------------------

int ObjectData::FindOrientedEdge( int a, int b ) const
{
  // return index of face containing points a,b
  for( int i=0; i<NFaces(); i++ )
  {
    const FaceT face(this,i);
    if( face.ContainsEdge(a,b) ) return i;
  }
  return -1;
}

//--------------------------------------------------

int ObjectData::FindOrientedEdge( int a, int b, bool *searchFaces ) const
{
  for( int i=0; i<NFaces(); i++ ) if( searchFaces[i] )
  {
    const FaceT face(this,i);
    if( face.ContainsEdge(a,b) ) return i;
  }
  return -1;
}

//--------------------------------------------------

int ObjectData::FindEdge( int a, int b ) const
{
  // return index of face containing points a,b
  for( int i=0; i<NFaces(); i++ )
  {
    const FaceT face(this,i);
    if( face.ContainsEdge(a,b) ) return i;
    if( face.ContainsEdge(b,a) ) return i;
  }
  return -1;
}

//--------------------------------------------------

int ObjectData::AddNormal( const VecT &norm )
{
  int index;
/*  // if given normal does not exist, create it
  int index=FindNormal(norm,1.0/16);
  if( index>=0 ) return index;*/

  //NOTE AddNormal doesn't check normal existence, it is caller job.

  index=NNormals();
  *NewNormal()=norm;
  return index;
}

//--------------------------------------------------

int ObjectData::SingleSelectedFace() const
{
  // if one and only one face is selected, return its index
  int i;
  int count=0;
  int select=-1;
  for( i=0; i<NFaces(); i++ )
  {
    if( FaceSelected(i) ) select=i,count++;
  }
  if( count!=1 ) return -1;
  return select;
}

//--------------------------------------------------

int ObjectData::FirstSelectedFace() const
{
  int i;
  for( i=0; i<NFaces(); i++ )
  {
    if( FaceSelected(i) ) return i;
  }
  return -1;
}

//--------------------------------------------------

int ObjectData::CountSelectedFaces() const
{
  int i;
  int count=0;
  for( i=0; i<NFaces(); i++ )
  {
    if( FaceSelected(i) ) count++;
  }
  return count;
}

//--------------------------------------------------

int ObjectData::SingleSelectedPoint() const
{
  int i;
  int count=0;
  int select=-1;
  for( i=0; i<NPoints(); i++ )
  {
    if( PointSelected(i) ) select=i,count++;
  }
  if( count!=1 ) return -1;
  return select;
}

//--------------------------------------------------

int ObjectData::FirstSelectedPoint() const
{
  int i;
  for( i=0; i<NPoints(); i++ )
  {
    if( PointSelected(i) ) return i;
  }
  return -1;
}

//--------------------------------------------------

int ObjectData::CountSelectedPoints() const
{
  int i;
  int count=0;
  for( i=0; i<NPoints(); i++ )
  {
    if( PointSelected(i) ) count++;
  }
  return count;
}

//--------------------------------------------------

inline int CompareF( const SharpEdge *edge0, const SharpEdge *edge1 )
{
  int ret=(*edge0)[0]-(*edge1)[0];
  if( ret ) return ret;
  return (*edge0)[1]-(*edge1)[1];
}

//--------------------------------------------------

template <class Type>
int FullSearch( const Type *base, int num, const Type &key )
{
  // robust non-binary implementation
  const Type *data=base;
  int i=num;
  while( i>0 )
  {
    if( !CompareF(data,&key) ) return num-i;
    data++;
    i--;
  }
  return -1;
}

//--------------------------------------------------

#if 0
template <class Type>
int BSearch( const Type *base, int num, const Type &key )
{
  return FullSearch(base,num,key);
}

//--------------------------------------------------

#else
template <class Type>
int BSearch( const Type *base, int num, const Type &key )
{
  const Type *lo = base;
  const Type *hi = base + (num - 1);
  const Type *mid;
  unsigned int half;
  int result;

  while (lo <= hi)
  {

    half = num / 2;
    if (half!=0)
    {
      mid = lo + (num & 1 ? half : (half - 1));

      result = CompareF(&key,mid);
      if (!result)
        return(mid-base);
      else if (result < 0)
      {
        hi = mid - 1;
        num = num & 1 ? half : half-1;
      }
      else    
      {
        lo = mid + 1;
        num = half;
      }
    }
    else if( num ) return CompareF(lo,&key) ? -1 : lo-base; // it should be here
    else break;
  }

  return -1;
}

//--------------------------------------------------

#endif
int ObjectData::FindSharpEdge( int a, int b ) const
{
  if( a>b ) __swap(a,b);
  const SharpEdge *data=_sharpEdge.Data();
  int size=_sharpEdge.Size();
  SharpEdge key;
  key[0]=a;
  key[1]=b;
  // find a usign binary search
  int index=BSearch(data,size,key);
#if 0
  if( index<0 )
  {
    index=FullSearch(data,size,key);
    if( index>=0 )
    {
      Fail("Edge found late.");
    }
  }
#endif
  return index;

}

//--------------------------------------------------

#include <Es/Algorithms/qsort.hpp>

void ObjectData::SortSharpEdges()
{
  QSort(_sharpEdge.Data(),_sharpEdge.Size(),CompareF);
  // optimize sharp edge list
  for( int i=1; i<_sharpEdge.Size(); )
  {
    const SharpEdge &curr=_sharpEdge[i-1];
    const SharpEdge &next=_sharpEdge[i];
    if( !CompareF(&curr,&next) ) _sharpEdge.Delete(i);
    else i++;
  }
}

//--------------------------------------------------

void ObjectData::RemoveSharpEdge( int a, int b )
{
  int index=FindSharpEdge(a,b);
  if( index>=0 ) RemoveSharpEdge(index);
}

//--------------------------------------------------

void ObjectData::AddSharpEdge( int a, int b )
{
  int index=FindSharpEdge(a,b);
  if( index<0 )
  {
    if( a>b ) __swap(a,b);
    SharpEdge edge;
    edge[0]=a;
    edge[1]=b;
    int i;
    for( i=0; i<_sharpEdge.Size(); i++ )
    {
      SharpEdge &cur=_sharpEdge[i];
      if( CompareF(&cur,&edge)>=0 ) break;
    }
    //_sharpEdge.Add(edge);
    _sharpEdge.Insert(i,edge);
  }
}

//--------------------------------------------------

void ObjectData::RemoveSharpEdge( int index )
{
  _sharpEdge.Delete(index);
}

//--------------------------------------------------

void ObjectData::RemoveAllSharpEdges()
{
  if( _sharpEdge.Size()>0 )
  {
    _sharpEdge.Clear();
  }
}

//--------------------------------------------------

void ObjectData::SelectFacesFromPoints( int vertex, SelMode mode )
{
  // test all faces adajcent to vertex and select them as appropriate
  int i;
  for( i=0; i<NFaces(); i++ )
  {
    const FaceT face(this,i);
    if( vertex<0 || face.ContainsPoint(vertex) )
    {
      bool allSel=true;
      for( int j=0; j<face.N(); j++ )
      {
        allSel=allSel && PointSelected(face.GetPoint(j));
      }
      if( mode==SelAdd && !allSel ) continue; 
      if( mode==SelSub && allSel ) continue; 
      FaceSelect(i,allSel);
    }
  }
}

//--------------------------------------------------

void ObjectData::SelectPointsFromSelectedFaces( int faceIndex, SelMode mode )
{
  int i;
  for( i=0; i<NFaces(); i++ )
    if( faceIndex<0 || faceIndex==i )
      if( FaceSelected(i) )
      {
        const FaceT face(this,i);
        for( int j=0; j<face.N(); j++ )
        {
          PointSelect(face.GetPoint(j),true);
        }
      }
}

//--------------------------------------------------

void ObjectData::UnselectPointsFromUnselectedFaces( int faceIndex, SelMode mode )
{
  int i;
  for( i=0; i<NFaces(); i++ )
    if( faceIndex<0 || faceIndex==i )
      if( !FaceSelected(i) )
      {
        const FaceT face(this,i);
        for( int j=0; j<face.N(); j++ )
        {
          PointSelect(face.GetPoint(j),false);
        }
      }
}

//--------------------------------------------------

void ObjectData::SelectPointsFromFaces( int faceIndex, SelMode mode )
{
  // test all points contained in face and select them as appropriate
  // could be much faster for faceIndex>=0, but this way it is easier
  // two stages: one selects all points of selected faces
  // second unselect all points of unselected faces
  // order of stages can vary depending on mode
  switch( mode )
  {
  case SelAdd:
    SelectPointsFromSelectedFaces(faceIndex,mode);
    break;
  case SelSub:
    UnselectPointsFromUnselectedFaces(faceIndex,mode);
    break;
  default: // SelSet
    UnselectPointsFromUnselectedFaces(faceIndex,mode);
    SelectPointsFromSelectedFaces(faceIndex,mode);
    break;
  }
}

//--------------------------------------------------

void ObjectData::PromoteComponent( bool *visited, CEdges &edges, int point, bool state )
{
  int i;
  for( i=0; i<NPoints(); i++ ) if( !visited[i] )
  {
    if( edges(i,point) )
    {
      visited[i]=true;
      PointSelect(i,state);
      PromoteComponent(visited,edges,i,state);
    }
  }
}

//--------------------------------------------------

void ObjectData::SelectComponent( int point, bool state )
{
  Temp<bool> visited(NPoints());
  if( !visited ) return;
  int i;
  for( i=0; i<NPoints(); i++ ) visited[i]=false;
  visited[point]=true;
  SRef<CEdges> edges=new CEdges(NPoints());
  if( !edges ) return;
  for( i=0; i<NFaces(); i++ )
  {
    const FaceT face(this,i);
    int j;
    for( j=0; j<face.N(); j++ )
    {
      int i0=face.GetPoint(j);
      int i1=face.GetPoint((j+1)%face.N());
      edges->SetEdge(i0,i1);
    }
  }
  PointSelect(point,state);
  PromoteComponent(visited,*edges,point,state);
}

//--------------------------------------------------

bool ObjectData::SelectionDelete(Selection *sel)
{
  int i,j,eshp,eshf;

  if (sel==NULL) sel=&_sel;
  sel->Validate();
  //find faces, that will be also deleted with points
  for (i=0;i<NFaces();i++) if (!sel->FaceSelected(i))
  {
    FaceT fc(*this,i);
    for (j=0;j<fc.N();j++) if (sel->PointSelected(fc.GetPoint(j)))
    {
      sel->FaceSelect(i);
      break;
    }
  }

  for (i=0;i<_uvsets.Size();i++) _uvsets[i]->DeleteSelected(*sel);

  Selection *selList[MAX_NAMED_SEL+3];
  int nsel;
  for (nsel=0,i=0;i<MAX_NAMED_SEL;i++) if (_namedSel[i]) selList[nsel++]=_namedSel[i];
  if (sel!=&_hide) selList[nsel++]=&_hide;
  if (sel!=&_lock) selList[nsel++]=&_lock;
  if (sel!=&_sel) selList[nsel++]=&_sel;
  int *fshift=new int[NFaces()];
  int *pshift=new int[NPoints()];
  eshf=0;
  eshp=0;
  for (i=0;i<NFaces();i++) if (!sel->FaceSelected(i)) fshift[eshf++]=i;
  for (i=0;i<NPoints();i++) if (!sel->PointSelected(i)) pshift[i]=eshp++;else pshift[i]=-1;
  int cp=0,cm=eshf+eshp+_sharpEdge.Size();
  ProgressBar<int> progress(cm);
  if (_mass.N()>=NPoints())
  {
    for (i=0;i<NPoints() && pshift[i]==i;i++);
    for (;i<NPoints();i++) if (!sel->PointSelected(i))
      _mass[pshift[i]]=_mass[i];
  }

  for (i=0;i<eshf;i++)
  {
    if (fshift[i]!=i)
    {
/*      FaceT fc(this,fshift[i] );
      fc.CopyFaceTo(i);*/
      _faces[i]=_faces[fshift[i]];
      for (j=0;j<nsel;j++)      
        selList[j]->faceSel[i]=selList[j]->faceSel[fshift[i]];        
      progress.SetPos(cp++);
    }
    for (j=0;j<_faces[i].n;j++)
    {
      int &pt=_faces[i].vs[j].point;
      pt=pshift[pt];
      ASSERT(pt>=0);
    }
  }
  for (i=0;i<NPoints() && pshift[i]==i;i++,cp++);
  for (;i<NPoints();i++) if (!sel->PointSelected(i))
  {
    ASSERT(pshift[i]>=0);
    _points[pshift[i]]=_points[i];
    for (j=0;j<nsel;j++)
    {
      selList[j]->vertSel[pshift[i]]=selList[j]->vertSel[i];
    }
    for (j=0;j<NAnimations();j++)
    {
      AnimationPhase &phs=*GetAnimation(j);
      phs[pshift[i]]=phs[i];
    } 
   progress.SetPos(cp++);
  }  
 
  for (i=0;i<_sharpEdge.Size();i++)
  {
    progress.SetPos(cp++);
    if ((_sharpEdge[i].edge[0]=pshift[_sharpEdge[i].edge[0]])==-1) 
       {_sharpEdge.Delete(i);i--;}
    else
      if ((_sharpEdge[i].edge[1]=pshift[_sharpEdge[i].edge[1]])==-1)
       {_sharpEdge.Delete(i);i--;}
  }
  _faces.Resize(eshf);
  _points.Resize(eshp);
  _mass.Validate();
  for (j=0;j<nsel;j++) selList[j]->Validate();
  for (j=0;j<NAnimations();j++) GetAnimation(j)->Validate();


  delete [] fshift;
  delete [] pshift;

  if (sel==&_sel) ClearSelection();

  /*

  int i;
  bool del=false;
  // go backward - indexes become invalid after deletion
  // delete all selected faces - no point removal
  for( i=NFaces(); --i>=0; ) if( sel->FaceSelected(i) )
  {
    DeleteFace(i);
    del=true;
  }
  // delete all selected points with associated faces
  for( i=NPoints(); --i>=0; ) if( sel->PointSelected(i) )
  {
    DeletePoint(i);
    del=true;
  }
  if( del )
  {
    //RecalcNormals();
    //ClearSelection();
    SetDirty();
  }
  */
  SetDirty();
  return true;
}

//--------------------------------------------------

bool ObjectData::SelectionDeleteFaces(Selection *sel)
{

  Selection cpy=sel?*sel:_sel;
  int i;
  for (i=0;i<NPoints();i++) cpy.PointSelect(i,false);
  SelectionDelete(&cpy);

  return true;
}

//--------------------------------------------------

void ObjectData::ClearSelection()
{
  _sel.Clear();
}

//--------------------------------------------------

//--------------------------------------------------

bool ObjectData::Merge( const ObjectData &src,const char *createSel,bool selections)
{
  int i,j;
  // merged data will be selected
  ClearSelection();
  // copy points
  // allocate new points
  int pIndex=ReservePoints(src.NPoints());
  int fIndex=ReserveFaces(src.NFaces());
  int nIndex=_normals.Size();
  _normals.Resize(nIndex+src.NNormals());

  memcpy(_points.Data()+pIndex,src._points.Data(),src.NPoints()*sizeof(PosT));
  // copy points to all animation phases
  for( i=0; i<NAnimations(); i++ )
  {
    AnimationPhase &phase=*GetAnimation(i);
    phase.Validate(NPoints());
    for( j=0; j<src.NPoints(); j++ )
    {
      phase[pIndex+j]=src.Point(j);
    }
  }
  // copy normals


  if( src.NNormals()>0 )
  {
    memcpy(&_normals[nIndex],src._normals.Data(),src.NNormals()*sizeof(VecT));
  }
  for( i=0; i<src.NFaces(); i++ )
  {    
    FaceT face(this);
    face=FaceT(src,i);
    for( j=0; j<face.N(); j++ )
    {
      face.SetPoint(j,face.GetPoint(j)+pIndex);
      face.SetNormal(j,face.GetNormal(j)+nIndex);
    }
    face.CopyFaceTo(fIndex+i);
  }
  // import mass information
  if( src._mass.N()>0 )
  {
    _mass.Validate();
    for( i=0; i<src._mass.N(); i++ ) _mass[pIndex+i]=src._mass[i];
  }
  // copy and re-index edges
  int oldEdges=_sharpEdge.Size();
  _sharpEdge.Resize(_sharpEdge.Size()+src._sharpEdge.Size());
  for( i=0; i<src._sharpEdge.Size(); i++ )
  {
    _sharpEdge[oldEdges+i][0]=src._sharpEdge[i][0]+pIndex;
    _sharpEdge[oldEdges+i][1]=src._sharpEdge[i][1]+pIndex;
  }


  if (selections)
  {
    // define all non-empty named selections from src
    int si;
    for( si=0; si<MAX_NAMED_SEL; si++ ) if( src._namedSel[si] )
    {
      RString temp;
      const NamedSelection &srcSel=*src._namedSel[si];
      if( srcSel.IsEmpty() ) continue;
      //For special format name.index, we always create new selection with new index
      const char *srcSelName=srcSel.Name();
      const char *index=strrchr(srcSelName,'.');
      int iindex;
      char tmp;
      if (index && sscanf(index+1,"%d%c",&iindex,&tmp)==1)
      {
        char *buff=temp.CreateBuffer(strlen(srcSelName)+10);
        strncpy(buff,srcSelName,(index-srcSelName)+1);
        char *num=buff+(index-srcSelName)+1;
        sprintf(num,"%03d",iindex);
        while (GetNamedSel(buff)!=NULL  && !GetNamedSel(buff)->IsEmpty())
        {
          iindex++;
          sprintf(num,"%03d",iindex);
        }
        srcSelName=buff;
      }
      // define sel with sel
      // use old named selection in target (if any)
      if( !UseNamedSel(srcSelName) ) _sel.Clear();
      // add any points selected in source
      for( i=0; i<src.NPoints(); i++ )
      {
        _sel.SetPointWeight(pIndex+i,srcSel.PointWeight(i));
      }
      for( i=0; i<src.NFaces(); i++ )
      {
        _sel.FaceSelect(fIndex+i,srcSel.FaceSelected(i));
      }
      // save to target
      SaveNamedSel(srcSelName);
    }    
  }

  for (int i=0;i<MAX_NAMED_PROP;i++)
    if (src._namedProp[i]!=NULL)
      SetNamedProp(src._namedProp[i]->Name(),src._namedProp[i]->Value());
      
  // set selection
  _sel.Clear();
  for( i=0; i<src.NPoints(); i++ ) _sel.PointSelect(pIndex+i,true);
  for( i=0; i<src.NFaces(); i++ ) _sel.FaceSelect(fIndex+i,true);
  if (createSel!=NULL) SaveNamedSel(createSel);
  SetDirty();
  return true;
}

struct ODataMergeInfo
{
   long Xcell;
   long Ycell;
   long Zcell;
   int flags;
   int point1;
   int point2;
   float distance;

   ODataMergeInfo(long xcell, long ycell, long zcell, int flags):
   Xcell(xcell),Ycell(ycell),Zcell(zcell),flags(flags) {}
   ODataMergeInfo() {}
   int Compare(const ODataMergeInfo &other) const
   {
     int i=(flags>other.flags)-(flags<other.flags);
     if (i==0) i=(Xcell>other.Xcell)-(Xcell<other.Xcell);
     if (i==0) i=(Ycell>other.Ycell)-(Ycell<other.Ycell);
     if (i==0) i=(Zcell>other.Zcell)-(Zcell<other.Zcell);
     return i;
   }
   bool operator==(const ODataMergeInfo &other) const {return Compare(other)==0;}
   bool operator>=(const ODataMergeInfo &other) const {return Compare(other)>=0;}
   bool operator<=(const ODataMergeInfo &other) const {return Compare(other)<=0;}
   bool operator!=(const ODataMergeInfo &other) const {return Compare(other)!=0;}
   bool operator>(const ODataMergeInfo &other) const {return Compare(other)>0;}
   bool operator<(const ODataMergeInfo &other) const {return Compare(other)<0;}
   int operator=(int i) {return i;}
};

///New version of merge points
bool ObjectData::MergePoints( double limit, bool selected, Selection *sel)
{  
  if (sel==0) sel=&_sel;
  bool changeTotal=false;
  bool again=true;
  int *remapIndices=new int[NPoints()];
  SRef<int> remapAutofree(remapIndices);

  {
    float minx=FLT_MAX,miny=FLT_MAX,minz=FLT_MAX;
    float maxx=-FLT_MAX,maxy=-FLT_MAX,maxz=-FLT_MAX;
    for (int i=0;i<NPoints();i++) 
    {
      remapIndices[i]=i;
      PosT &point=Point(i);
      if (minx>point.X()) minx=point.X();
      if (miny>point.Y()) miny=point.Y();
      if (miny>point.Z()) minz=point.Z();
      if (maxx<point.X()) maxx=point.X();
      if (maxy<point.Y()) maxy=point.Y();
      if (maxy<point.Z()) maxz=point.Z();
    }
   float maxlimit=(float)sqrt((minx-maxx)*(minx-maxx)+(miny-maxy)*(miny-maxy)+(minz-maxz)*(minz-maxz))*1.5f;
   if (maxlimit<limit) limit=maxlimit;      
  }

  int divider=1;
  ProgressBar<int> pb(selected?sel->NPoints():NPoints());
  while (again || divider>1)
  {        
    BTree<ODataMergeInfo> dbstore;
    double dlimit=limit/divider;
    again=false;
    int cntprocessed=0;
    for (int i=0;i<NPoints();i++) if ((!selected || sel->PointSelected(i)) && remapIndices[i]==i)
    {
      cntprocessed++;
      PosT &point=Point(i);
      ODataMergeInfo ptnfo;
      ptnfo.Xcell=toLargeInt((float)floor(point.X()/dlimit));   //calculate cell coordinates for points
      ptnfo.Ycell=toLargeInt((float)floor(point.Y()/dlimit));
      ptnfo.Zcell=toLargeInt((float)floor(point.Z()/dlimit));
      ptnfo.flags=point.flags;    //test all neighbor cells for existence of an point
      {
        ODataMergeInfo *best=0;
        ODataMergeInfo *fnd;
        ODataMergeInfo test;
        static int testOrder[27][3]=
        {
          {0,0,0}, {0,1,0},{0,-1,0},{1,0,0},{-1,0,0},{0,0,1},{0,0,-1},        
          {0,1,1}, {0,-1,1},{1,0,1},{-1,0,1},
          {0,1,-1},{0,-1,-1},{1,0,-1},{-1,0,-1},          
          {-1,1,0},{1,1,0}, {-1,-1,0}, {1,-1,0},     
          {-1,1,1},{1,1,1}, {-1,-1,1},{1,-1,1}, {1,-1,-1},{-1,1,-1}, {1,1,-1},{-1,-1,-1}
        };
        float bestdist=FLT_MAX;
        test.flags=ptnfo.flags;
        for (int x=0;x<27;x++)
        {
          test.Xcell=ptnfo.Xcell+testOrder[x][0];
          test.Ycell=ptnfo.Ycell+testOrder[x][1];
          test.Zcell=ptnfo.Zcell+testOrder[x][2];
          fnd=dbstore.Find(test);
          if (fnd)
          {
            PosT &scpt=Point(fnd->point1);
            float distance=scpt.Distance2(point);
            if (distance<limit*limit && distance<fnd->distance)
            {
              if (bestdist>distance)
              {
                bestdist=distance;
                best=fnd;
              }
            }
          }
        }
        if (best==0)
        {
          fnd=dbstore.Find(ptnfo);
          if (fnd==0) 
          {
            ptnfo.distance=FLT_MAX;
            ptnfo.point2=-1;
            ptnfo.point1=i;
            dbstore.Add(ptnfo);
          }          
        }
        else
        {
          best->point2=i;
          best->distance=bestdist;
        }
      }    
    }
    BTreeIterator<ODataMergeInfo> iter(dbstore);
    ODataMergeInfo *item;
    int cycles=0;
    while (item=iter.Next()) 
    {
      cycles++;
      if (item->point2!=-1)
      {
        float iMass=0,jMass=0;
        float sumMass=0;
        int i=item->point1;
        int j=item->point2;
        Vector3 pointI=Point(i);
        Vector3 pointJ=Point(j);
        if( i<_mass.N() && j<_mass.N() )
        {
          iMass=_mass[i];
          jMass=_mass[j];
          sumMass=iMass+jMass;
          _mass[i]=sumMass;
        }
        if( sumMass<=0 ) iMass=jMass=1,sumMass=2;
        Vector3 average=(pointI*iMass+pointJ*jMass)/sumMass;
        Point(i).SetPoint(average);
        Point(i).flags=item->flags; // merge flags
        // calculate average for all animation phases
        for( int p=0; p<NAnimations(); p++ )
        {
          AnimationPhase &anim=*GetAnimation(p);
          anim[i]=(anim[i]*iMass+anim[j]*jMass)/sumMass;
        }
        // be clever as for selections
        // merged point should be in all selections any of mergers is
        for( int s=0; s<MAX_NAMED_SEL; s++ )
        {
          NamedSelection *sel=_namedSel[s];
          if( sel )
          {
            sel->SetPointWeight
              (
              i,
              (sel->PointWeight(i)+sel->PointWeight(j))*0.5f
              );
          }
        }
        if( _lock.PointSelected(j) ) _lock.PointSelect(i);
        if( !_hide.PointSelected(j) ) _hide.PointSelect(i,false);
        remapIndices[j]=i;      
        again=true;
      }
    }
    if (cycles<(cntprocessed>>3)) divider++;
    else if (divider>1) divider--;
//    LogF("CellSize: %f, Processed: %d, merged %d, next divider %d",dlimit, cntprocessed, cycles, divider);
    pb.SetPos(pb.GetMax()-(cntprocessed-cycles));
  }
  Selection toDel(this);
  // now, we have array remapIndices filed by indices to merged points.
  // solve transition
  for (int i=0;i<NPoints();i++)
  {
    while (remapIndices[remapIndices[i]]!=remapIndices[i]) remapIndices[i]=remapIndices[remapIndices[i]];
    if (remapIndices[i]!=i) toDel.PointSelect(i);
  }
  // remap faces
  for (int i=0;i<NFaces();i++)
  {
    FaceT fc(this,i);
    for (int j=0;j<fc.N();j++) fc.SetPoint(j,remapIndices[fc.GetPoint(j)]);
    if (fc.RepairDegenerated()==false) 
      toDel.FaceSelect(i);
  }
  for( int j=_sharpEdge.Size(); --j>=0; )
  {
    _sharpEdge[j][0]=remapIndices[_sharpEdge[j][0]];
    _sharpEdge[j][1]=remapIndices[_sharpEdge[j][1]];
  }

  SelectionDelete(&toDel);
  return true;
}

/*bool ObjectData::MergePoints( double limit, bool selected, Selection *sel)
{
  // merge all selected points that are nearer than limit
  ProgressBar<int> progress(NPoints() * (NPoints()  + 1) / 2);
  int i,j;
  bool changeTotal=false;
  if (sel==NULL) sel=&_sel;
  
  for( i=0; i<NPoints(); i++ ) if( !selected || sel->PointSelected(i) )
  {
    for( j=0; j<i && j < NPoints(); j++ ) if( !selected || sel->PointSelected(j) )
    {
      int flagsI=Point(i).flags;
      int flagsJ=Point(j).flags;
      Vector3 pointI=Point(i);
      Vector3 pointJ=Point(j);
      if( flagsI==flagsJ ) // merge only points with same flags
        if( (pointI-pointJ).SquareSize()<=limit*limit )
        {
          // if there is mass, use weighed average and sum mass
          float iMass=0,jMass=0;
          float sumMass=0;
          if( i<_mass.N() && j<_mass.N() )
          {
            iMass=_mass[i];
            jMass=_mass[j];
            sumMass=iMass+jMass;
            _mass[i]=sumMass;
          }
          if( sumMass<=0 ) iMass=jMass=1,sumMass=2;
          Vector3 average=(pointI*iMass+pointJ*jMass)/sumMass;
          Point(i).SetPoint(average);
          Point(i).flags=flagsJ|flagsI; // merge flags
          // calculate average for all animation phases
          for( int p=0; p<NAnimations(); p++ )
          {
            AnimationPhase &anim=*GetAnimation(p);
            anim[i]=(anim[i]*iMass+anim[j]*jMass)/sumMass;
          }
          // be clever as for selections
          // merged point should be in all selections any of mergers is
          for( int s=0; s<MAX_NAMED_SEL; s++ )
          {
            NamedSelection *sel=_namedSel[s];
            if( sel )
            {
              sel->SetPointWeight
                (
                i,
                (sel->PointWeight(i)+sel->PointWeight(j))*0.5f
                );
            }
          }
          if( _lock.PointSelected(j) ) _lock.PointSelect(i);
          if( !_hide.PointSelected(j) ) _hide.PointSelect(i,false);

          ReplacePoint(i,j);
          DeletePoint(j);    
          j--;
          i--;
          _dirty=true;
          changeTotal = true;
        }

        progress.SetPos( i * NPoints() + j);
    }
  }
  if( changeTotal ) SortSharpEdges();
  return changeTotal;
}
*/
//--------------------------------------------------

// named selection

bool ObjectData::SaveNamedSel( const char *name, const Selection *sel)
{
  if (sel==NULL) sel=&_sel;
  int i;
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( _namedSel[i] && !strcmp(_namedSel[i]->Name(),name) )
    {
      *_namedSel[i]=NamedSelection(*sel,name);
      return false;
    }
  }
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( !_namedSel[i] )
    {
      // free slot - save
      _namedSel[i]=new NamedSelection(*sel,name);
      return true;
    }
  }
  return false;
}

//--------------------------------------------------

bool ObjectData::UseNamedSel( const char *name, SelMode mode )
{
  int i;
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( _namedSel[i] && !strcmp(_namedSel[i]->Name(),name) )
    {
      const NamedSelection *nam=_namedSel[i];
      UseSelection(nam,mode);
      return true;
    }
  }
  return false;
}

bool ObjectData::UseSelection(const Selection *sel, SelMode mode )
{
  switch( mode )
  {
  default: // case SelSet:
    _sel=*sel;
    break;
  case SelAdd:
    _sel+=*sel;
    break;
  case SelSub:
    _sel-=*sel;
    break;
  }
  return true;
}

//--------------------------------------------------

bool ObjectData::DeleteNamedSel( const char *name )
{
  int i;
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( _namedSel[i] && !strcmp(_namedSel[i]->Name(),name) )
    {
      delete _namedSel[i],_namedSel[i]=NULL;
      return true;
    }
  }
  return false;
}

//--------------------------------------------------

bool ObjectData::RenameNamedSel( const char *name, const char *newname )
{
  int i;
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( _namedSel[i] && !strcmp(_namedSel[i]->Name(),newname) )
    {
      return false;
    }
  }
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( _namedSel[i] && !strcmp(_namedSel[i]->Name(),name) )
    {
      _namedSel[i]->SetName(newname);
      return true;
    }
  }
  return false;
}

//--------------------------------------------------

int ObjectData::FindNamedSel( const char *name ) const
{
  int i;
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( _namedSel[i] && !strcmp(_namedSel[i]->Name(),name) )
    {
      return i;
    }
  }
  return -1;
}
int ObjectData::FindNamedSelNoCaseSensitive( const char *name ) const
{
  int i;
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( _namedSel[i] && !stricmp(_namedSel[i]->Name(),name) )
    {
      return i;
    }
  }
  return -1;
}

//--------------------------------------------------

const char *ObjectData::NamedSel( int i ) const
{
  if( i<0 || i>=MAX_NAMED_SEL ) return NULL;
  if( !_namedSel[i] ) return NULL;
  return _namedSel[i]->Name();
}

//--------------------------------------------------

const NamedSelection *ObjectData::GetNamedSel( int i ) const
{
  return _namedSel[i];
}

//--------------------------------------------------

NamedSelection *ObjectData::GetNamedSel( int i )
{
  return _namedSel[i];
}

//--------------------------------------------------

const NamedSelection *ObjectData::GetNamedSel( const char *name ) const
{
  int i=FindNamedSel(name);
  if( i>=0 ) return _namedSel[i];
  return NULL;
}

//--------------------------------------------------

VecT GetTwoSidedNormal(FaceT& fc, ObjectData *obj, int point)
{
  int a=point-1;
  int b=point;
  int c=point+1;
  if (a<0) a+=fc.N();
  if (c>=fc.N()) c-=fc.N();
  PosT& ps1=obj->Point(fc.GetPoint(a));
  PosT& ps2=obj->Point(fc.GetPoint(b));
  PosT& ps3=obj->Point(fc.GetPoint(c));
  return (ps1+ps3)*0.5f-ps2;
  /*  VecT v2

  CopyVektor(v1,mxVector3(ps2[0],ps2[1],ps2[2]));
  v2=(ps1+ps3)*0.5
  CopyVektor(v2,mxVector3(
  (ps1[0]+ps3[0])*0.5f,(ps1[1]+ps3[1])*0.5f,(ps1[2]+ps3[2])*0.5f));
  RozdilVektoru(v1,v2);
  return v1;*/
}

NamedSelection *ObjectData::GetNamedSel( const char *name )
{
  int i=FindNamedSel(name);
  if( i>=0 ) return _namedSel[i];
  return NULL;
}

//--------------------------------------------------


static bool FaceTestLightingCallback(const FaceT &fc, void *context)
{
  int flags=fc.GetFlags() & FACE_LIGHT_MASK;
  if (flags==FACE_BOTHSIDESLIGHT||flags==FACE_SKYLIGHT||flags==FACE_FLATLIGHT) return false;
  return true;
}

/*static void BuildGraph(ObjectData *obj, CEdges& egs, CEdges &nocross)
{
/*
THASHTABLE tbl;
Hash_Init2(&tbl,10001,sizeof(SGraphItem),0,8,0);
for (int i=0;i<obj->NFaces();i++)
{
FaceT fc(obj,i);
int flags=fc.GetFlags() & FACE_LIGHT_MASK;
if (flags==FACE_BOTHSIDESLIGHT||flags==FACE_SKYLIGHT||flags==FACE_FLATLIGHT) continue;
for (int j=0;j<fc.N();j++)
{
int spoj=GetFaceEdge(&tbl,fc,nocross,i,j);
if (spoj!=-1) 
{
egs.SetEdge(i,spoj);
egs.SetEdge(spoj,i);
}
}
}
Hash_Done(&tbl);
}*/

static VecT GetSkyNormal(FaceT& fc, ObjectData *obj, int point)
{
  PosT& ps1=obj->Point(fc.GetPoint(point));
  VecT v1=ps1;
  v1.Normalize();
  return v1;
}

static void ObtoceniVertexu(ObjectData *obj, int face, int vertex, CEdges& graph, CEdges& stats)
{
  int a=0;
  int p;
  stats.SetEdge(face,vertex);
  while ((p=graph.GetEdge(face,a++))!=-1)
  {
    FaceT fc(obj,p);
    int j;
    for (j=0;j<fc.N();j++) if (fc.GetPoint(j)==vertex) break;
    if (j!=fc.N())
    {
      int b=0;
      int q;
      while ((q=stats.GetEdge(p,b++))!=-1)
        if (q==vertex) break;
      if (q==-1)ObtoceniVertexu(obj,p,vertex,graph,stats);
      fc.SetNormal(j,obj->NNormals());
    }
  }
}


static bool IsFaceValid(FaceT &fc)
{
  if (fc.N()==3)
    if (fc.GetPoint(0)==fc.GetPoint(1) ||
      fc.GetPoint(1)==fc.GetPoint(2) ||
      fc.GetPoint(2)==fc.GetPoint(0))
      return false;
    else if (fc.N()==4)
      if (fc.GetPoint(0)==fc.GetPoint(1) ||
        fc.GetPoint(1)==fc.GetPoint(2) ||
        fc.GetPoint(2)==fc.GetPoint(3) ||
        fc.GetPoint(3)==fc.GetPoint(0) ||
        fc.GetPoint(0)==fc.GetPoint(2) ||
        fc.GetPoint(1)==fc.GetPoint(3))
        return false;
      else return false;
      return true;
}

static void PrepareNormals(ObjectData *obj, CEdges& nocross)
{
  CEdges graph;
  graph.SetVertices(obj->NFaces());
  obj->BuildFaceGraph(graph,nocross,FaceTestLightingCallback);  
  CEdges facestats;
  facestats.SetVertices(__max(obj->NFaces(),obj->NPoints()));
  int fcnt=obj->NFaces();
  int i;
  int *reserved=new int[obj->NPoints()]; //pole rezervovanych normal pro zamknute normaly
  memset(reserved,0xff,obj->NPoints()*sizeof(int));
  bool markinvalid=false;
  bool askmark=false;
  ProgressBar<int> progress(fcnt);
  for (i=0;i<fcnt;i++)
  {
    progress.SetNextPos(i+1);
    FaceT fc(obj,i);
    if (!IsFaceValid(fc))
    {
      for (int j=0;j<fc.N();j++) fc.SetNormal(j,0);
      continue;
    }
    VecT vc;
    int flags=fc.GetFlags() & FACE_LIGHT_MASK;
    if (flags==FACE_BOTHSIDESLIGHT)
    {
      for (int j=0;j<fc.N();j++)
      {
        fc.SetNormal(j,obj->NNormals());
        VecT *norm=obj->NewNormal();
        vc=GetTwoSidedNormal(fc,obj,j);
        *norm=vc;
        /*        (*norm)[0]=vc[XVAL];
        (*norm)[1]=vc[YVAL];
        (*norm)[2]=vc[ZVAL];*/
      }
    }
    else if (flags==FACE_SKYLIGHT)
    {
      for (int j=0;j<fc.N();j++)
      {
        fc.SetNormal(j,obj->NNormals());
        VecT *norm=obj->NewNormal();
        vc=GetSkyNormal(fc,obj,j);
        *norm=vc;
      }
    }
    else if (flags==FACE_FLATLIGHT)
    {
      int idx=obj->NNormals();
      VecT *norm=obj->NewNormal();
      vc=fc.CalculateNormal();
      *norm=vc;
      for (int j=0;j<fc.N();j++) fc.SetNormal(j,idx);		
    }
    else	
      for (int j=0;j<fc.N();j++) 
      {
        int vx=fc.GetPoint(j);
        if (obj->Point(vx).flags & POINT_SPECIAL_LOCKNORMAL)
        {
          if (reserved[vx]!=-1) fc.SetNormal(j,reserved[vx]);
          else
          {
            fc.SetNormal(j,reserved[vx]=obj->NNormals()); //pouze rezervuje jednu normalu pro tento
            VecT *vc=obj->NewNormal();        //vertex;
            (*vc)[0]=(*vc)[1]=(*vc)[2]=0.0f;
          }
        }
        else
        {
          int a=0;
          int p;
          while ((p=facestats.GetEdge(i,a++))>=0)
            if (p==vx) break;
          if (p==-1)
          {
            ObtoceniVertexu(obj,i,vx,graph,facestats);
            fc.SetNormal(j,obj->NNormals());		  
            VecT *vc=obj->NewNormal();
            (*vc)[0]=(*vc)[1]=(*vc)[2]=0.0f;
          }
        }
      } 
  }
  delete [] reserved;
}

static void CalcNormals(ObjectData *obj, VecT *locked_normals)
{
  //  bool nosmart=!config->GetBool(IDS_CFGSMARTNORMALS);
  for (int i=0;i<obj->NFaces();i++)
  {
    FaceT fc(obj,i);
    int flags=fc.GetFlags() & FACE_LIGHT_MASK;
    if (flags==FACE_BOTHSIDESLIGHT||flags==FACE_SKYLIGHT||flags==FACE_FLATLIGHT) continue;
    int aloneNormal=-1;
    for (int j=0;j<fc.N();j++)
    {      
      VecT v=fc.CalculateNormalAtPointAdjusted(j);
      //ASSERT(v.SquareSize()>0);
      if (v[0]==FLT_MAX) v[0]=0;
      if (v[1]==FLT_MAX) v[1]=0;
      if (v[2]==FLT_MAX) v[2]=0;
      if (flags == FACE_REVERSELIGHT)
      {
        v[0]=-v[0];
        v[1]=-v[1];
        v[2]=-v[2];
      }

      VecT &nv=obj->Normal(fc.GetNormal(j));
      if (obj->Point(fc.GetPoint(j)).flags & POINT_SPECIAL_LOCKNORMAL)
      {
        nv=locked_normals[fc.GetPoint(j)];  //vyzvedni ulozenou hodnotu normaly pro tento vektor
      }
      else
      {
        VecT res=nv+v;
        if (res.SquareSize()<1e-12 && (nv[0]!=0 || nv[1]!=0 || nv[2]!=0))
        {       
          if (aloneNormal==-1) aloneNormal=obj->AddNormal(v);
          fc.SetNormal(j,aloneNormal);
        }
        else
        {
          nv[0]+=v[0];      //scitej normaly face
          nv[1]+=v[1];
          nv[2]+=v[2];
        }
      }
    }
  }  
  for (int j=0;j<obj->NNormals();j++)
  {
    VecT &nv=obj->Normal(j);
    nv.Normalize(); 
    //ASSERT(nv.SquareSize()>0.1);
  }
}


static void ClearNormalInfo(ObjectData *obj)
{
  int p=obj->NFaces();
  for (int i=0;i<p;i++)
  {
    FaceT fc(obj,i);
    for (int j=0;j<fc.N();j++)
      fc.SetNormal(j,0);
  }
}

static void ReadLockedNormals(ObjectData *obj, VecT *locked_normals)
{
  int fcnt=obj->NFaces(); //pocet facu
  for (int i=0;i<fcnt;i++)
  {
    FaceT fc(obj,i);
    for (int j=0;j<fc.N();j++)
    {
      PosT &vx=obj->Point(fc.GetPoint(j));
      if (vx.flags & POINT_SPECIAL_LOCKNORMAL) //zamknuta normala
      {
        locked_normals[fc.GetPoint(j)]=obj->Normal(fc.GetNormal(j)); //uloz hodnotu normaly
      }
    }
  }
}

//--------------------------------------------------

//Zamknute normaly se zpracovavaji ve 3 krocich
/* 

1] sejmou hodnoty normal do pole, ke kazdemu vertexu jedna normala (ReadLockedNormals)
2] pri novem rozdelovani normal (PrepareNormals) se pro zamknute normaly vyhradi 
ke kazdemu vertexu jedna normala
3] Pri vypoctu normal (CalcNormals) se pro zamknute normaly nastavi puvodni 
hodnota podle pole

*/


void ObjectData::RecalcNormals(bool noprogress)
{
  VecT *locked_normals=new VecT[NPoints()];
  memset(locked_normals,0,sizeof(VecT)*NPoints());


  CEdges egs;
  LoadSharpEdges(egs);
  ReadLockedNormals(this, locked_normals);
  ResetNormals();
  ClearNormalInfo(this);
  PrepareNormals(this,egs);
  CalcNormals(this,locked_normals);
  delete [] locked_normals;
}


void ObjectData::BuildFaceGraph(CEdges &result, const CEdges &nocross, bool (*FaceTestCallback)(const FaceT &fc, void *context),void *context) const
{
  CEdges vxlist(NPoints());   //seznam hran bod -> face
  int i;

  for (i=0;i<NFaces();i++) {
      FaceT fc(this,i);
      if (FaceTestCallback==NULL || FaceTestCallback(fc,context)) {
        for (int j=0;j<fc.N();j++)
        {
          vxlist.SetEdge(fc.GetPoint(j),i);
        }
      }
  }
  //na konci vxlist obsahuje pro kazdy bod seznam indexu facu, ktere s nim inciduji

  result.Clear();


  for (i=0;i<NFaces();i++) //pro vsechny facy
  {
    FaceT fc(this,i);
    for (int j=0;j<fc.N();j++)  //pro vsechny jeho vrcholy
    {
      int pt=fc.GetPoint(j);    //tento bod budou mit vsechny facy spolecny
      int nxf;
      for (int k=0;(nxf=vxlist.GetEdge(pt,k))!=-1;k++) if (nxf!=i) //hledej vsechny incidujici facy
      {
        FaceT fc2(this,nxf);              //hledej dalsi spolecny bod
        int nxfp;
        for (nxfp=0;fc2.GetPoint(nxfp)!=pt;nxfp++);        
        int j1=j+1>=fc.N()?0:j+1;     //na aktualni face to musi byt bod +1
        int nxfp1=(nxfp==0?fc2.N():nxfp)-1;         //na nalezene to musi byt bod -1
        int pt2=fc2.GetPoint(nxfp1);           //nasli jsme nejaky bod, ale jestli jej inciduje druha face??
        if (pt2==fc.GetPoint(j1) && !nocross.IsEdge(pt,pt2)) 
          result.SetEdge(i,nxf); //v pripade ze je nalezena spolecna hrana
        //tj hrana tak ze jeden face ma a, b a druhy b, a, pak se zde vytvori spojeni mezi temito facy
      }
    }
  }
}

void ObjectData::CleanEmptySelections()
{
  for (int i=0;i<MAX_NAMED_SEL;i++)
  {
    if (_namedSel[i] && _namedSel[i]->IsEmpty()) 
    {
      delete _namedSel[i];
      _namedSel[i]=NULL;
    }
  }
}

void ObjectData::LoadSharpEdges(CEdges &edges) const
{
  edges.SetVertices(NPoints());
  int a,b;
  for (int i=0;EnumEdges(i,a,b);)
    edges.SetEdge(a,b);
}


void ObjectData::SaveSharpEdges(const CEdges &edges, bool merge)
{
  if (!merge) 
  {
    this->RemoveAllSharpEdges();
    for (int i=0,cnt=NPoints();i<cnt;i++)
    {
      for (int j=0,k;(k=edges.GetEdge(i,j))!=-1;j++)
      {
        if (k<i) 
        {
          if (!edges.IsEdgeInDirection(k,i))
            _sharpEdge.Append()=SharpEdge(k,i);
        }
        else if (i>k)
            _sharpEdge.Append()=SharpEdge(i,k);     
      }
    }
  }
  else
  {
    for (int i=0,cnt=NPoints();i<cnt;i++)
    {
      for (int j=0,k;(k=edges.GetEdge(i,j))!=-1;j++)
      {
        AddSharpEdge(i,k);
      }
    }
  }
}


int ObjectData::GetMetadataCount() const
{
  return _metadata.Size();
}
const AnimationMetadata *ObjectData::GetMetadata(int index) const
{
  Assert(index >= 0 && index < _metadata.Size());
  if (index >= 0 && index < _metadata.Size())
    return _metadata[index];
  return NULL;
}
const AnimationMetadata *ObjectData::GetMetadata(const RString &name) const
{
  int index = FindMetadata(name);
  if (index == -1)
    return NULL;
  else
    return _metadata[index];
}

void ObjectData::AddMetadata(const AnimationMetadata &data)
{
  _metadata.Add(new AnimationMetadata(data));
}
void ObjectData::SetMetadata(const AnimationMetadata &data)
{
  int index = FindMetadata(data.name);
  if (index == -1)
    _metadata.Add(new AnimationMetadata(data));
  else
    _metadata[index]->value = data.value;
}
void ObjectData::DeleteMetadata(int index)
{
  Assert(index >= 0 && index < _metadata.Size());
  if (index >= 0 && index < _metadata.Size())
    return _metadata.Delete(index);
}
int ObjectData::FindMetadata(const RString &name) const
{
  for (int i = 0; i < _metadata.Size(); ++i)
    if (_metadata[i]->name == name)
      return i;
  return -1;
}
void ObjectData::ClearAllMetadata()
{
  _metadata.Clear();
}


int ObjectData::GetKeyStoneCount() const
{
  return _keyStone.Size();
}
AnimationKeyStone *ObjectData::GetKeyStone(int index) const
{
  Assert(index >= 0 && index < _keyStone.Size());
  if (index >= 0 && index < _keyStone.Size())
    return _keyStone[index];
  return NULL;
}
void ObjectData::AddKeyStone(const AnimationKeyStone &data)
{
  _keyStone.Add(new AnimationKeyStone(data));
}
void ObjectData::AddKeyStoneSorted(const AnimationKeyStone &data)
{
  int index = 0;
  // Find index for insert sorted by time
  for (; index < _keyStone.Size(); ++index)
    if (_keyStone[index]->time > data.time)
      break;
  _keyStone.Insert(index, new AnimationKeyStone(data));
}
void ObjectData::DeleteKeyStone(int index)
{
  Assert(index >= 0 && index < _keyStone.Size());
  if (index >= 0 && index < _keyStone.Size())
    return _keyStone.Delete(index);
}
void ObjectData::ClearAllKeyStones()
{
  _keyStone.Clear();
}


class FunctorUVSetCompare
{
public:
    int operator () (const SRef<ObjUVSet> *a, const SRef<ObjUVSet> *b) const
    {
      ObjUVSet *aa=a->GetRef();
      ObjUVSet *bb=b->GetRef();
      if (aa->GetIndex()>bb->GetIndex()) return 1;
      if (aa->GetIndex()<bb->GetIndex()) return -1;
      return 0;
    }
};

void ObjectData::SortUVSets()
{
  QSort<>(_uvsets,FunctorUVSetCompare());
}

}