// Edges.cpp: implementation of the CEdges class.
//
//////////////////////////////////////////////////////////////////////

#include "Common.hpp"
#include "ObjectData.h"
#include "Edges.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

namespace ObjektivLib {


CEdges::CEdges(int npoints)
  {
  _edgelist=NULL;
  _nvertx=0;
  _levels=0;
  if (npoints) SetVertices(npoints);
  }

//--------------------------------------------------

CEdges::~CEdges()
  {
  SetVertices(0);
  }

//--------------------------------------------------

bool CEdges::TestSharp(int a, int b) const
  {
  if (a>=_nvertx) return false;
  for (int i=0;i<_levels;i++)
    if (_edgelist[i][a]==b) return true;
  else if (_edgelist[i][a]==-1) return false;
  return false;
  }

//--------------------------------------------------

bool CEdges::ExpandLevel()
  {
  int *level=new int[_nvertx];
  if (level==NULL) return false;
  memset(level,0xFF,sizeof(int)*_nvertx);
  int **nl=new int *[_levels+1];
  if (nl==NULL)
    {
    delete []level;
    return false;
    }
  memcpy(nl,_edgelist,sizeof(int *)*_levels);
  nl[_levels]=level;
  _levels++;
  delete [] _edgelist;
  _edgelist=nl;
  return true;
  }

//--------------------------------------------------

void CEdges::SetEdge(int a, int b)
  {
  int i;
  if (a>=_nvertx) return;
  for (i=0;i<_levels;i++)
    if (_edgelist[i][a]==b) return;
  else if (_edgelist[i][a]==-1) 
    {
    _edgelist[i][a]=b;
    if (i+1<_levels) _edgelist[i+1][a]=-1;
    return;
    }
  if (ExpandLevel()) _edgelist[i][a]=b;
  }

//--------------------------------------------------

void CEdges::SetVertices(int cnt,int mode)
  {
  if (cnt==_nvertx)
    {
    if (mode==1) 
      {Clear();return;}
    if (mode==2) 
      {return;}
    }
  for (int i=0;i<_levels;i++)
    delete [] _edgelist[i];
  delete _edgelist;
  _edgelist=NULL;
  _levels=0;
  _nvertx=cnt;
  }

//--------------------------------------------------

void CEdges::RemoveEdge(int a, int b)
  {
  if (!TestSharp(a,b))
    {
    a^=b;
    b^=a;
    a^=b;
    if (!TestSharp(a,b)) return;
    }
  int i=0,j=0;
  for (;i<_levels && _edgelist[i][a]>=0;i++)
    {
    if (_edgelist[i][a]==b) continue;
    if (i!=j) 
      {_edgelist[j][a]=_edgelist[i][a];}	
    j++;
    }
  if (i!=j && j<_levels) _edgelist[j][a]=-1;
  }

//--------------------------------------------------

void CEdges::Clear()
  {
  if (_edgelist==NULL) return;
  memset(_edgelist[0],0xFF,sizeof(int)*_nvertx);
  }

//--------------------------------------------------

int CEdges::GetEdge(int idx,int i) const
  {
  if (idx>=_nvertx || i>=_levels) return -1;
  else return _edgelist[i][idx];
  }

//--------------------------------------------------


}