#include <Windows.h>
#include <malloc.h>
#include <string.h>
#include <stdio.h>
#include <el/pathname/pathname.h>
#include ".\massprocess.h"

bool MassProcess::ProcessFolders(const char *folder, const char *mask, bool recursive)
  {
  size_t foldlen=strlen(folder);
  char *join=(char *)alloca(strlen(folder)+strlen(mask)+10);
  if (folder[foldlen-1]!='\\') sprintf(join,"%s\\%s",folder,mask);
  else sprintf(join,"%s%s",folder,mask);
  WIN32_FIND_DATA wfind;
  HANDLE h=FindFirstFile(join,&wfind);
  Pathname outname=join;
  while (h!=INVALID_HANDLE_VALUE)
    {    
    if (strcmp(wfind.cFileName,".") && strcmp(wfind.cFileName,".."))
      {
      outname.SetFilename(wfind.cFileName);     
      if (!(wfind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
        {
		_fileInfo=&wfind;
        if (ProcessFile(outname)==false) return false;
		_fileInfo=NULL;
        }
      }
    if (FindNextFile(h,&wfind)==FALSE) 
      {
      FindClose(h);
      h=INVALID_HANDLE_VALUE;
      }
    }
  if (recursive)
    {
    if (folder[foldlen-1]!='\\') sprintf(join,"%s\\*.*",folder,mask);
    else sprintf(join,"%s*.*",folder,mask);
    h=FindFirstFile(join,&wfind);
    while (h!=INVALID_HANDLE_VALUE)
      {    
      if (strcmp(wfind.cFileName,".") && strcmp(wfind.cFileName,".."))
        {
        outname.SetFilename(wfind.cFileName);     
        if (wfind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
          {
  		 _fileInfo=&wfind;		  
  		  if (BeforeEnterFolder(outname)==true)
			{
			bool cont=ProcessFolders(outname,mask,recursive);
			_fileInfo=&wfind;		  
			AfterExitFolder(outname);
			_fileInfo=NULL;
			if (cont==false) return false;
			}
          }
        }
      if (FindNextFile(h,&wfind)==FALSE) 
        {
        FindClose(h);
        h=INVALID_HANDLE_VALUE;
        }
      }
    }
  return true;
  }

const void *MassProcess::GetCurFileSpecificInfo(size_t infosize)
  {
  if (infosize==sizeof(WIN32_FIND_DATA)) return _fileInfo;
  else return NULL;
  }