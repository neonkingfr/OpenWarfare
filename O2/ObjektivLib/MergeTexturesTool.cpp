#include "common.hpp"
#include <el/ParamFile/paramFile.hpp>
#include <El/BTree/Btree.h>
#include ".\mergetexturestool.h"
#include "ObjectData.h"
#include "ObjToolsMatLib.h"
#include "LODObject.h"
#include <io.h>

namespace ObjektivLib {


LSError MergeTexturesTool::LoadPtm(const char *ptmText, size_t size)
{
  QIStrStream inbuff(ptmText,size);
  LSError err=_colorPtm.Parse(inbuff);
  if (err==LSOK) return LoadMergeInfo();
  return err;
}


LSError MergeTexturesTool::LoadPtm(const char *filename)
{
  LSError err=_colorPtm.Parse(filename);
  if (err==LSOK) return LoadMergeInfo();
  return err;
}

namespace Functors
{
  class LoadMergeInfo
  {
    mutable BTree<MergeTexturesTool::MergeInfo> &_mergeInfo;
    int _mainsizex;
    int _mainsizey;
    RString _targetTex;
  public:
    LoadMergeInfo(BTree<MergeTexturesTool::MergeInfo> &mergeInfo,int mainsizex, int mainsizey, RString targetTex):
        _mergeInfo(mergeInfo),
          _mainsizex(mainsizex),
          _mainsizey(mainsizey),
          _targetTex(targetTex)
        {

        }

    bool operator()(const ParamEntryVal &val) const
    {
      MergeTexturesTool::MergeInfo nfo(_targetTex,val,_mainsizex,_mainsizey);
      _mergeInfo.Add(nfo);
      return false;
    }
  };

  class LoadOneSetInfo
  {
    mutable BTree<MergeTexturesTool::MergeInfo> &_mergeInfo;
    mutable LSError &state;
  public:
    LoadOneSetInfo(BTree<MergeTexturesTool::MergeInfo> &mergeInfo,LSError &state):_mergeInfo(mergeInfo),state(state) {}
    bool operator()(const ParamEntryVal &val) const
    {      
      if (val.IsClass())
      {      
      int mainsizex,mainsizey;
      RString maintxt;

      ParamEntryPtr entry=val.FindEntry("w");
      if (entry.NotNull() && entry->IsIntValue()) 
      {
        mainsizex=entry->GetInt();
        entry=val.FindEntry("h");
        if (entry.NotNull() && entry->IsIntValue()) 
        {
          mainsizey=entry->GetInt();
          entry=val.FindEntry("file");
          if (entry.NotNull() && entry->IsTextValue()) 
          {
            maintxt=entry->GetValue();
            entry=val.FindEntry("Items");
            if (entry.NotNull() && entry->IsClass())
            {
              ParamClassPtr items=entry->GetClassInterface();
              items->ForEachEntry(Functors::LoadMergeInfo(_mergeInfo,mainsizex,mainsizey,maintxt));
              state=LSOK;
              return false;
            }
          }
        }
      }
      state=LSStructure;
      return true;
      }    
     return false;
    }
  };
}

LSError MergeTexturesTool::LoadMergeInfo()
{
  ParamEntryPtr entry=_colorPtm.FindEntry("InfTexMerge");
  if (entry.NotNull() && entry->IsClass())
  {
    ParamClassPtr mainCls=entry->GetClassInterface();
    LSError state=LSStructure;
    mainCls->ForEachEntry(Functors::LoadOneSetInfo(_mergeInfo,state));
    return state;
  }
  return LSStructure;
}


static void CalculateTransform(float x, float y, float sizex, float sizey, int angle, float mainszx, float mainszy,float transform[3][2])
{  
  float a,b,c,d;
  ASSERT(sizex!=0 && sizey!=0);
  switch (angle)
  {
  case 0:a=sizex;b=0;c=0;d=sizey;break;
  case 1:y+=sizex-1;a=0;b=-sizex;c=sizey;d=0;break;
  case 2:x+=sizex-1;y+=sizey-1;a=-sizex;d=-sizey;c=b=0;break;
  case 3:x+=sizey-1;a=0;b=sizex;c=-sizey;d=0;break;
  default:a=b=c=d=0;
  }
  float mx=1/mainszx;
  float my=1/mainszy;
  transform[0][0]=a*mx;
  transform[0][1]=b*my;
  transform[1][0]=c*mx;
  transform[1][1]=d*my;
  transform[2][0]=x*mx;
  transform[2][1]=y*my;
}


MergeTexturesTool::MergeInfo::MergeInfo(RString targetTex, const ParamEntryVal &texInfo, int mainsizex, int mainsizey):_targetTex(targetTex)
{
  float trn[3][2];
  int x,y,sizex,sizey,scale;
  int angle;
  RString name;

  ParamEntryPtr entry;
  entry=texInfo.FindEntry("x");
  if (entry.NotNull() && entry->IsIntValue()) x=entry->GetInt();else x=0;
  entry=texInfo.FindEntry("y");
  if (entry.NotNull() && entry->IsIntValue()) y=entry->GetInt();else y=0;
  entry=texInfo.FindEntry("w");
  if (entry.NotNull() && entry->IsIntValue()) sizex=entry->GetInt();else sizex=0;
  entry=texInfo.FindEntry("h");
  if (entry.NotNull() && entry->IsIntValue()) sizey=entry->GetInt();else sizey=0;
  entry=texInfo.FindEntry("scale");
  if (entry.NotNull() && entry->IsIntValue()) scale=entry->GetInt();else scale=0;
  entry=texInfo.FindEntry("angle");
  if (entry.NotNull() && entry->IsIntValue()) angle=entry->GetInt();else angle=0;
  entry=texInfo.FindEntry("name");
  if (entry.NotNull() && entry->IsTextValue()) name=entry->GetValue();else name="";

  float powfact=pow(2.0f,scale);
  sizex=toInt(sizex*powfact);
  sizey=toInt(sizey*powfact);
  CalculateTransform((float)x,(float)y,(float)sizex,(float)sizey,angle,(float)mainsizex,(float)mainsizey,trn);

  _sourceTex=name;

  _transform=Matrix3P(trn[0][0],trn[1][0],trn[2][0],
                      trn[0][1],trn[1][1],trn[2][1],
                      0,0,0);
  _matchFace = false;
  _matchRvmat = false;

}

bool MergeTexturesTool::MergeTextures(ObjectData *obj)
{
  bool success=false;
  for (int i=0;i<obj->NFaces();i++)  
  {
    FaceT fc(obj,i);
    RString tex=fc.GetTexture();
    MergeInfo *fnd=_mergeInfo.Find(MergeInfo(tex));
    if (fnd)
    {
      success=true;
      fnd->_matchFace = true;
      fc.SetTexture(fnd->GetTargetTexture());
      for (int j=0;j<fc.N();j++)
      {
        ObjUVSet_Item uv=fc.GetUV(j);
        {
          Vector3P vx=fnd->GetTransform()*Vector3(uv.u,uv.v,1);
          fc.SetUV(j,vx[0],vx[1]);
        }
      }
    }
  }
  return success;
}

struct SMatFileConvert
{
  RString _srcMat;
  RString _trgMat;
  SRef<ParamFile> _matData;
  bool joined;

  int CompareWith(const SMatFileConvert &other) const
  {
    return _stricmp(_srcMat,other._srcMat);
  }
  
  bool operator==(const SMatFileConvert &other) const {return CompareWith(other)==0;}
  bool operator>=(const SMatFileConvert &other) const {return CompareWith(other)>=0;}
  bool operator<=(const SMatFileConvert &other) const {return CompareWith(other)<=0;}
  bool operator!=(const SMatFileConvert &other) const {return CompareWith(other)!=0;}
  bool operator>(const SMatFileConvert &other) const {return CompareWith(other)>0;}
  bool operator<(const SMatFileConvert &other) const {return CompareWith(other)<0;}

  SMatFileConvert(const char *search=0):_srcMat(search),joined(false) {}
  SMatFileConvert(RString srcMat):_srcMat(srcMat),joined(false) {}

};

static bool CompareMaterials(const ParamEntry *e1, const ParamEntry *e2)
{
  if (_stricmp(e1->GetName(),"uvTransform")==0) return true;
  if (e1->IsClass() && e2->IsClass())
    {
      if (e1->GetEntryCount()==e2->GetEntryCount())
      {        
        for (int i=0,cnt=e1->GetEntryCount();i<cnt;i++)
        {
          const ParamEntry *cmp1=e1->GetEntry(i).GetPointer();
          const ParamEntry *cmp2=e2->FindEntry(cmp1->GetName()).GetPointer();
          if (cmp2)
          {
            if (CompareMaterials(cmp1,cmp2)==false) return false;
          }
          else
            return false;
        }
        return true;
      }
  }
  else if (e1->IsArray() && e2->IsArray())
    {
      if (e1->GetSize()==e2->GetSize())
      {
        const ParamEntry &arr1=*e1;
        const ParamEntry &arr2=*e2;
        for (int i=0;i<e1->GetSize();i++)
        {
          if (arr1[i].IsTextValue()==true && arr2[i].IsTextValue()==true)
          {
            if (_stricmp(arr1[i].GetValue(),arr2[i].GetValue())) return false;
          }
          else if (arr1[i].IsFloatValue()==true && arr2[i].IsFloatValue()==true)
          {
            float a1=arr1[i];
            float a2=arr2[i];
            if (fabs(a1-a2)>0.000001) return false;
          }
          else if (arr1[i].IsIntValue()==true && arr2[i].IsIntValue()==true)
          {
            int a1=arr1[i];
            int a2=arr2[i];
            if (a1!=a2) return false;
          }
          else if (arr1[i].IsArrayValue()==true && arr2[i].IsArrayValue()==true)
          {   
            //TODO: Compare arrays
            if (arr1[i].GetItemCount()!=arr2[i].GetItemCount()) return false;
          }
          else
            return false;
        }
        return true;
      }
    }  
  else if (e1->IsFloatValue() && e2->IsFloatValue())
  {
    float a1=*e1;
    float a2=*e2;
    if (fabs(a1-a2)>0.000001) return false;
    return true;
  }
  else if (e1->IsIntValue() && e2->IsIntValue())
  {
    int a1=*e1;
    int a2=*e2;
    if (a1!=a2) return false;
    return true;
  }
  else if (e1->IsTextValue() && e2->IsTextValue())
  {    
      if (_stricmp(e1->GetValue().Data(),e2->GetValue().Data())!=0) return false;   
      return true;
  }
  return false;
}

/*
static RString CalculateCommonName(RString name1, RString name2, const char *basePath)
{
   int nlen1=name1.GetLength();
   int nlen2=name2.GetLength();
   RString res;
   char *out=res.CreateBuffer(__max(nlen1,nlen2)+10);
   const char *stop1=Pathname::GetExtensionFromPath(name1);
   const char *stop2=Pathname::GetExtensionFromPath(name2);
   const char *ptr1=name1;
   const char *ptr2=name2;
   while (ptr1!=stop1 && ptr2!=stop2)
   {
      if (toupper(*ptr1)!=toupper(*ptr2)) break;
      *out++=*ptr1;
      ptr1++;
      ptr2++;
   }
   *out=0;
   RString tryname=RString(basePath,res+stop1);
   if (_access(tryname,0)!=-1)
   {
     char subbuf[20];
     for (int i=0;_access(tryname,0)!=-1;i++)     
       tryname=RString(basePath,res+"_"+_itoa(i,subbuf,10)+"_merged"+stop1);     
     res=res+"_"+subbuf+"_merged"+stop1;
   }
   else
     res=res+"_merged"+stop1;
   return res;
}

*/
static RString UniqueName(RString name, const char *basePath)
{
  const char *ext=Pathname::GetExtensionFromPath(name);
  RString res=name.Mid(0,ext-name.Data());  
  RString tryname=RString(basePath,res+ext);
  if (_access(tryname,0)!=-1)
  {
    char subbuf[20];
    for (int i=0;_access(tryname,0)!=-1;i++)     
      tryname=RString(basePath,res+"_"+_itoa(i,subbuf,10)+ext);     
    res=res+"_"+subbuf+ext;
  }
  else 
    res=name;
  return res;
}
/*
static void OptimizeMaterials(BTree<SMatFileConvert> &list, const char *basePath)
{
  BTreeIterator<SMatFileConvert> iter1(list);
  SMatFileConvert *item1;
  while ((item1=iter1.Next())!=0) if (!item1->joined)
  {
    BTreeIterator<SMatFileConvert> iter2(list);
    SMatFileConvert *item2;
    iter2.BeginFrom(*item1);
    if (iter2.Next())
    {
        while ((item2=iter2.Next())!=0)
        {
          if (CompareMaterials(item1->_matData,item2->_matData))
          {
            item2->joined=true;
            item2->_trgMat=item1->_trgMat=CalculateCommonName(item1->_trgMat,item2->_trgMat, basePath);
          }
        }
    }
  }
}

static void SaveMaterials(BTree<SMatFileConvert> &list, const char *basePath)
{
  BTreeIterator<SMatFileConvert> iter1(list);
  SMatFileConvert *item1;
  while ((item1=iter1.Next())!=0) if (!item1->joined)
  {
    item1->_matData->Save(RString(basePath,item1->_trgMat));
  }
}
*/
bool MergeTexturesTool::CacheMaterials(const char *search,int basePathCut) const
{
  WIN32_FIND_DATA find;
  HANDLE fh;
  fh=FindFirstFile(search,&find);
  bool succ=false;
  Pathname tmp=search;
  if (fh!=INVALID_HANDLE_VALUE) 
  {
    do 
    {
      tmp.SetFilename(find.cFileName);
      if (strlen(tmp)>(unsigned)basePathCut)
      {      
        if (_rvmatCache.Find(RVMatCache(tmp.GetFullPath()+basePathCut))==0)
        {        
          ParamFile *pf=new ParamFile;
          if (pf->Parse(tmp)==LSOK)
          {
            _rvmatCache.Add(RVMatCache(tmp.GetFullPath()+basePathCut,pf));
            succ=true;
          }
        }
      }
    }
    while (FindNextFile(fh,&find));
    FindClose(fh);
  }
  return succ;
}

RString MergeTexturesTool::ShareMaterialFromCache(RString name, const ParamFile *rvmatData) const
{
  BTreeIterator<RVMatCache> cacheIt(_rvmatCache);
  RVMatCache *item;
  while ((item=cacheIt.Next())!=0)
  {
    if (CompareMaterials(item->_matData,rvmatData))
      return item->_file;
  }
  return "";
}


RString MergeTexturesTool::ShareMaterial(RString name, const ParamFile *rvmatData, const char *basePath) const
{
  RString newname=ShareMaterialFromCache(name,rvmatData);
  if (newname=="")
  {  
    Pathname searchPath(basePath+name);
    searchPath.SetFiletitle("*");    
    if (CacheMaterials(searchPath,strlen(basePath))) 
      newname=ShareMaterialFromCache(name,rvmatData);
  }
  return newname;
}


bool MergeTexturesTool::MergeMaterialsUsingTextures(ObjectData *obj, const char *basePath)
{
  BTree<SMatFileConvert> matList;
  ObjToolMatLib &matLib=obj->GetTool<ObjToolMatLib>();  
  {  
    BTree<ObjMatLibItem> pomocna;
    matLib.ReadMatLib(pomocna,ObjToolMatLib::ReadMaterials);
    BTreeIterator<ObjMatLibItem> pomiter(pomocna);
    ObjMatLibItem *pomitem;
    while ((pomitem=pomiter.Next())!=0) if (pomitem->name[0]!=0) 
    {
      SMatFileConvert nfo(pomitem->name);
      const char *ext=Pathname::GetExtensionFromPath(pomitem->name);
      nfo._matData=new ParamFile;      
      if (LoadAndMergeMaterialStruct(nfo._srcMat,nfo._matData,basePath,nfo._trgMat)==LSOK)
      {
        RString sharename=ShareMaterial(nfo._trgMat,nfo._matData,basePath);
        if (sharename=="") 
        {
          sharename=UniqueName(nfo._trgMat,basePath);
          nfo._matData->Save(RString(basePath+sharename));
        }
        nfo._trgMat=sharename;
        matList.Add(nfo);  
      }
    }
  }
  bool success=false;
  for (int i=0;i<obj->NFaces();i++)  
  {
    FaceT fc(obj,i);
    RString mat=fc.GetMaterial();
    SMatFileConvert *fnd=matList.Find(SMatFileConvert(mat));
    if (fnd)
    {
      success=true;
      fc.SetMaterial(fnd->_trgMat);
    }
  }
  return success;
}

static RString CropSuffix(const RString &src) {
    const char *c =  strrchr(src.Data(),'_');
    if (c == 0) return src;
    const char *d = c+1;
    while (*d && isalpha(*d)) d++;
    if (d == c || *d != 0) return src;
    return RString(src,c - src.Data());
}

static RString GetSuffix(const RString &src, const RString &cropped) {
    return src.Mid(cropped.GetLength());    
}

namespace Functors
{
  class ProcessStages
  {
    BTree<MergeTexturesTool::MergeInfo> &mergeInfo;
    bool &success;
    mutable RString &targetData;
  public:
    ProcessStages(BTree<MergeTexturesTool::MergeInfo> &mergeInfo,bool &success,RString &targetData)
      :mergeInfo(mergeInfo),success(success),targetData(targetData) {}
    bool operator()(ParamEntryVal val) const
    {
      if (_strnicmp(val.GetName(),"Stage",5)==0 && val.IsClass())
      {
        ParamEntryPtr entry=val.FindEntry("texture");
        if (entry.NotNull() && *entry->GetValue().Data()!='#' && *entry->GetValue().Data()!=0)
        {
          RString src=entry->GetValue().Data();
          const char *ext=Pathname::GetExtensionFromPath(src);
          RString name=src.Mid(0,ext-src.Data());
          int nlen=name.GetLength();
          RString cname = CropSuffix(name);
          RString suffix = GetSuffix(name,cname);

          BTreeIterator<MergeTexturesTool::MergeInfo> fnditer(mergeInfo);
          fnditer.BeginFrom(MergeTexturesTool::MergeInfo(cname));
          MergeTexturesTool::MergeInfo *found=fnditer.Next();
          while (found)
          {
            RString nm=found->GetSourceTexture();
            RString cnm = CropSuffix(nm.Mid(0,Pathname::GetExtensionFromPath(nm)-nm.Data()));
            if (_stricmp(cnm,cname)!=0) 
              found=0;
            else
            {
              if (GetSuffix(nm,cnm).GetLength()!=0)
                    break;
              found=fnditer.Next();
            }
          }
          if (found)
          {
            RString nwname=found->GetTargetTexture();
            targetData=nwname.Mid(0,Pathname::GetNameFromPath(nwname)-nwname.Data());
            RString ext=Pathname::GetExtensionFromPath(nwname);
            nwname=nwname.Mid(0,nwname.GetLength()-ext.GetLength());
            if (nwname.GetLength()>3 && nwname[nwname.GetLength()-3]=='_')
              nwname=nwname.Mid(0,nwname.GetLength()-3);
            nwname=nwname+suffix+ext;
            success=true;
            entry->SetValue(nwname);
            found->_matchRvmat = true;
          }
        }
      }
      return false;
    }
  };
}

LSError MergeTexturesTool::LoadAndMergeMaterialStruct(RString src, ParamFile *data, const char *basePath, RString &trg)
{
  LSError err=data->Parse(RString(basePath,src));
  if (err!=LSOK) return err;

  bool success=false;
  RString targetPath;
  data->ForEachEntry(Functors::ProcessStages(_mergeInfo,success,targetPath));
  if (!success) return LSNoEntry;
    
  trg=targetPath+"Me_"+Pathname::GetNameFromPath(src);
  return LSOK;
}

bool MergeTexturesTool::MergeTextures(LODObject *lod)
{
  bool succ=false;
  for (int i=0;i<lod->NLevels();i++)
  {
    ObjectData *obj=lod->Level(i);
    succ|=MergeTextures(obj);
  }
  return succ;
}
bool MergeTexturesTool::MergeMaterialsUsingTextures(LODObject *lod, const char *basePath)
{
  bool succ=false;
  for (int i=0;i<lod->NLevels();i++)
  {
    ObjectData *obj=lod->Level(i);
    succ|=MergeMaterialsUsingTextures(obj,basePath);
  }
  return succ;
}

struct CO2NO
{
  RString _colorMapa;
  RString _normalMapa;

  CO2NO(const char *colorMapa=0,const char *normalMapa=0):_colorMapa(colorMapa),_normalMapa(normalMapa) {}
  CO2NO(RString colorMapa,RString normalMapa=RString()):_colorMapa(colorMapa),_normalMapa(normalMapa) {}

  int CompareWith(const CO2NO &other) const
  {
    return _stricmp(_colorMapa,other._colorMapa);
  }
  
  bool operator==(const CO2NO &other) const {return CompareWith(other)==0;}
  bool operator>=(const CO2NO &other) const {return CompareWith(other)>=0;}
  bool operator<=(const CO2NO &other) const {return CompareWith(other)<=0;}
  bool operator!=(const CO2NO &other) const {return CompareWith(other)!=0;}
  bool operator>(const CO2NO &other) const {return CompareWith(other)>0;}
  bool operator<(const CO2NO &other) const {return CompareWith(other)<0;}

};

/*
LSError MergeTexturesTool::SaveNOPtm(LODObject *lodobj, const char *sourcePtm, const char *targetPtm, const char *basePath)
{
  BTree<CO2NO> co2no;
  
  for (int i=0;i<lodobj->NLevels();i++)
  {
    ObjectData *obj=lodobj->Level(i);
    for (int j=0;j<obj->NFaces();j++)
    {
      FaceT fc(obj,j);
      RString tex=fc.GetTexture();
      RString mat=fc.GetMaterial();
      if (tex[0]!=0)
      {
        CO2NO *find=co2no.Find(CO2NO(tex));
        if (find==0)
        {
          RString matname=RString(basePath,mat);
          ParamFile pf;
          if (pf.Parse(matname)==LSOK)
          {

          }
        }
      }
    }
  }

}
*/

}