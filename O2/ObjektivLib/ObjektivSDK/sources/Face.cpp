#include "stdafx.h"
#include "../interfaces/Adapter.h"
#include "../interfaces/OxygeneSDK.h"
#include "../../FaceT.h"
#include "../interfaces/Mesh.h"

namespace OxygeneSDK {

    Face Face::Add(Mesh &obj) {return Face(obj,obj.NewFace());}
    Face Face::Bind(Mesh &obj, int faceIndex) {return Face(obj,faceIndex);}

    ///Creates access class to given face in given mesh
    Face::Face(Mesh &obj,int faceIndex):Super((ObjektivLib::ObjectData *)obj,faceIndex) {}
    ///Creates an anonymous face not included into the mesh (but refers it)
    Face::Face(Mesh &obj):Super((ObjektivLib::ObjectData *)obj) {}
    ///Creates copy. 
    Face::Face(const Face &other):Super(other) {}
    ///Converts FaceT to Face
    Face::Face(Super &other):Super() {BindTo(other);}


    const char *Face::GetTexture(int stage) {
        return FaceT::GetTexture().Data();
    }
    const char *Face::GetMaterial(int stage) {
        return FaceT::GetMaterial().Data();
    }
    Mesh Face::GetObject() {return FaceT::GetObject();}
    const Mesh Face::GetObject() const {return FaceT::GetObject();}

    Vertex Face::GetVertex(int vs) const {return FaceT::GetPointVector<ObjektivLib::PosT>(vs);}
    Vector Face::GetNornalVector(int vs) const {return FaceT::GetNormalVector<ObjektivLib::VecT>(vs);}
}