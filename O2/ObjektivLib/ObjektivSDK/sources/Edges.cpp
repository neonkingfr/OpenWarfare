#include "stdafx.h"
#include "../interfaces/Adapter.h"
#include "../interfaces/OxygeneSDK.h"
#include "../interfaces/Edges.h"

namespace OxygeneSDK {

    Edges::Edges(int items):Super(new SmallObject<ObjektivLib::CEdges>(items),owner) {}
    int Edges::GetEdge(int idx,int i) const {return object->GetEdge(idx,i);}
    void Edges::Clear() {return object->Clear();}
    void Edges::RemoveEdge(int va, int vb) {return object->RemoveEdge(va,vb);}
    void Edges::SetVertices(int cnt, int mode) {return object->SetVertices(cnt,mode);}
    void Edges::SetEdge(int a, int b) {return object->SetEdge(a,b);}
    bool Edges::IsEdge(int va,int vb) const {return object->IsEdge(va,vb);}
    bool Edges::IsEdgeInDirection(int va,int vb) const {return object->IsEdgeInDirection(va,vb);}
    int Edges::GetN() const {return object->GetN();}
}









