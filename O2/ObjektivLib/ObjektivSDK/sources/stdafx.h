// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows XP or later.
#define WINVER 0x0501		// Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 6.0 or later.
#define _WIN32_IE 0x0600	// Change this to the appropriate value to target other versions of IE.
#endif

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
// Windows Header Files:
//#include <windows.h>
#define DLLEXTERN __declspec(dllexport)
#include <el/Pathname/Pathname.h>
#include "../../LODObject.h"
#include "../../FaceT.h"
#include "../../Edges.h"
#define DebugPrint5 LogF
#include "SmallObject.h"

class MTLocker
{
    CRITICAL_SECTION csect;
public:
    MTLocker() {InitializeCriticalSection(&csect);}
    ~MTLocker() {DeleteCriticalSection(&csect);}
    void Lock() {EnterCriticalSection(&csect);}
    void Unlock() {LeaveCriticalSection(&csect);}
};

typedef LightSpeed::SmallObjectAllocator<
        LightSpeed::SmallObjectAlloc::ExceptionErrorHandling, 
        MTLocker> SmallAllocator;
extern  SmallAllocator smallAllc;

namespace OxygeneSDK {

    template<class T>
    class SmallObject: public T
    {
    public:
      ///operator new replacement
      void *operator new(size_t sz)
      {
        return smallAllc.Alloc(sz);
      }
      ///operator delete replacement
      void operator delete(void *ptr, size_t sz)
      {
        return smallAllc.Free(ptr,sz);
      }
      SmallObject() {}

      template<class A>
      SmallObject(A a):T(a) {}
      template<class A, class B>
      SmallObject(A a, B b):T(a,b) {}
      template<class A, class B, class C>
      SmallObject(A a, B b, C c):T(a,b,c) {}
    };

}
//#include "../interfaces/OxygeneSDK.h"



// TODO: reference additional headers your program requires here
