#pragma once
#include "stdafx.h"
#include "../interfaces/Adapter.h"
#include "../interfaces/OxygeneSDK.h"







namespace OxygeneSDK {


    template<class Object>
    Adapter<Object>::Adapter(void)
        :object(new SmallObject<Object>),isref(false) {

    }

    template<class Object>
    Adapter<Object>::~Adapter() {
        if (!isref) delete static_cast<SmallObject<Object> *>(object);
    }

    template<class Object>
    Adapter<Object>::Adapter(const Object *object)
        :object(const_cast<Object *>(object)),isref(true) {}

    template<class Object>
    Adapter<Object>::Adapter(Object *object, Owner owner)
        :object(object),isref(false) {}
    
    template<class Object>
    Adapter<Object>::Adapter(const Adapter<Object> &other)
        :object(new SmallObject<Object>(*other.object)),isref(false) {}
    
    template<class Object>
    Adapter<Object> &Adapter<Object>::operator=(const Adapter &other) {
        if (other.object != object) {
            *object = *other.object;
        }
        return *this;
    }

    template<class Object>
    Object *Adapter<Object>::Unlink() const {
        isref = true;
        return object;
    }



}

