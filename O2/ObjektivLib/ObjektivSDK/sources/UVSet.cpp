#include "stdafx.h"
#include "../interfaces/Adapter.h"
#include "../interfaces/OxygeneSDK.h"
#include "../interfaces/UVSet.h"

namespace OxygeneSDK {

    VectorUV::VectorUV(const ObjektivLib::ObjUVSet_Item &uv):u(uv.u),v(uv.v) {}

    UVSet::UVSet(Mesh &mesh, int id):Super(new SmallObject<ObjektivLib::ObjUVSet>(mesh, id)) {}
    UVSet::UVSet(const UVSet &other, Mesh &newOwner):Super(new SmallObject<ObjektivLib::ObjUVSet>(other, newOwner)) {}
    
    Mesh UVSet::GetOwner() const {return object->GetOwner();}
    int UVSet::GetIndex() const {return object->GetIndex();}
    void UVSet::SetIndex(int index) {return object->SetIndex(index);}
    void UVSet::Validate() {return object->Validate();}
    void UVSet::SetUV(int face, int vs, float u, float v) {object->SetUV(face,vs,ObjektivLib::ObjUVSet_Item(u,v));}
    void UVSet::SetUV(int face, int vs, const VectorUV &uv) {object->SetUV(face,vs,ObjektivLib::ObjUVSet_Item(uv.u,uv.v));}

    VectorUV UVSet::GetUV(int face, int vs) const {return object->GetUV(face,vs);}


}
