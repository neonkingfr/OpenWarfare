#include "stdafx.h"
#include "../interfaces/SdkCommon.h"          
#include "../interfaces/OxygeneSdk.h"          
#include "version.h"


LightSpeed::SmallObjectAllocator<
        LightSpeed::SmallObjectAlloc::ExceptionErrorHandling, 
        MTLocker> smallAllc;


namespace OxygeneSDK {
    
    float GetSDKVersion() {return (float)(SDK_VERSION);}

}