#include "stdafx.h"
#include "../interfaces/SdkCommon.h"          
#include "../interfaces/OxygeneSdk.h"          
#include "../interfaces/LODMesh.h"          
#include "../interfaces/Mesh.h"

namespace OxygeneSDK {
    using namespace ObjektivLib;

    static bool LdCallback( istream &f, void *context ) {
        return reinterpret_cast<ILODMeshLoadCallback *>(context)->LoadCallBack(f);
    }
    static bool SvCallback( ostream &f, void *context ) {
        return reinterpret_cast<ILODMeshSaveCallback *>(context)->SaveCallBack(f);
    }

    LODMesh::LODMesh(Mesh &firstlod, float resolution): Super(new SmallObject<LODObject>(firstlod.Unlink(),resolution),owner) {}
    int LODMesh::GetActiveLevel() const {return object->ActiveLevel();}
    Mesh LODMesh::GetActiveMesh() const {return Mesh(object->Active());}
    Mesh LODMesh::GetLevel(int level) const {return Mesh(object->Level(level));}
    void LODMesh::SelectLevel( int level ) {return object->SelectLevel(level);}
    int LODMesh::NLevels() const {return object->NLevels();}
    int LODMesh::FindLevel( float resolution ) const {return object->FindLevel(resolution);}
    int LODMesh::FindLevelExact( float resolution ) const {return object->FindLevelExact(resolution);}
    float LODMesh::GetResolution( int level ) const {return object->Resolution(level);}
    bool LODMesh::ChangeResolution( float oldRes, float newRes ) {return object->ChangeResolution(oldRes,newRes);}
    int LODMesh::SetResolution( int level, float newRes ) {return object->SetResolution(level,newRes);}
    bool LODMesh::DeleteLevel( int level ) {return object->DeleteLevel(level);}
    int LODMesh::AddLevel(Mesh mesh, float resolution ) {return object->AddLevel(mesh,resolution);}
    Mesh LODMesh::UnlinkLevel( int level ) {return Mesh(object->DetachLevel(level),Mesh::owner);}
    int LODMesh::LinkLevel(Mesh &mesh, float resolution ) {return object->InsertLevel(mesh.Unlink(),resolution);}
    bool LODMesh::Load(const char *filename, ILODMeshLoadCallback *callback) {
        return object->Load(Pathname(filename),callback?LdCallback:0,callback) == 0;
    }
    bool LODMesh::Load(std::istream &f, ILODMeshLoadCallback *callback) {
        return object->Load(f,callback?LdCallback:0,callback) == 0;
    }
    bool LODMesh::Save(const char *filename, ILODMeshSaveCallback *callback) const {
        return object->Save(Pathname(filename),OBJDATA_LATESTVERSION, false  ,callback?SvCallback:0,callback) == 0;
    }
    bool LODMesh::Save(std::ostream &f, ILODMeshSaveCallback *callback ) const {
        return object->Save(f,OBJDATA_LATESTVERSION, false) == 0 
            && (callback == 0 || SvCallback(f, callback));
    }
    bool LODMesh::Merge(const LODMesh &src , bool createlods, const char *createSel, bool fillLods) {
        return object->Merge(src,createlods, createSel, fillLods) == 0;
    }
    Vector LODMesh::CenterAll() {
        return Vector(object->CenterAll());
    }
    void LODMesh::GetAABB(Vector& minP, Vector& maxP) {
      Vector3 minP_, maxP_;
      object->GetAABB(minP_, maxP_);
      minP = Vector(minP_);
      maxP = Vector(maxP_);
    }


}