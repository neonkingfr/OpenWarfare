#include "stdafx.h"
#include "../interfaces/SdkCommon.h"          
#include "../interfaces/Vector.h"    
#include "Adapter.cpp"

namespace OxygeneSDK {

    template class DLLEXTERN  Adapter<Vector3>;
    
    Vertex::Vertex(const ObjektivLib::DataPointEx *x):Super(x) {}
    Vertex::Vertex(ObjektivLib::DataPointEx *x, Super::Owner owner):Super(x,owner) {}
    Vertex::Vertex(const ObjektivLib::DataPointEx &other):Super(other) {}



    Vector::Vector():Super(new SmallObject<Vector3>(0,0,0), owner) {}
    Vector::Vector(float x, float y, float z):Super(new SmallObject<Vector3>(x,y,z), owner) {};
    float Vector::X() const {return object->X();}
    float Vector::Y() const {return object->Y();}
    float Vector::Z() const {return object->Z();}
    Vector::VectorAccess *Vector::operator->() {return reinterpret_cast<VectorAccess *>(&(*object)[0]);}
    const Vector::VectorAccess *Vector::operator->() const {return reinterpret_cast<const VectorAccess *>(&(*object)[0]);}
    float &Vector::operator[](int idx) {return object->operator[](idx);}
    float Vector::operator[](int idx) const {return object->operator[](idx);}

    Vertex::Vertex():Super(new SmallObject<ObjektivLib::DataPointEx>()) {
        static_cast<ObjektivLib::DataPointEx *>(object)->SetPoint(Vector3(0,0,0));
        static_cast<ObjektivLib::DataPointEx *>(object)->flags = 0;
    }
    Vertex::Vertex(const Vector &vx, int flags ):Super(new SmallObject<ObjektivLib::DataPointEx>()) {
        static_cast<ObjektivLib::DataPointEx *>(object)->SetPoint(vx);
        static_cast<ObjektivLib::DataPointEx *>(object)->flags = flags;
    }

    Vertex::Vertex(float x, float y , float z , int flags ):Super(new SmallObject<ObjektivLib::DataPointEx>()) {
        static_cast<ObjektivLib::DataPointEx *>(object)->SetPoint(Vector3(x,y,z));
        static_cast<ObjektivLib::DataPointEx *>(object)->flags = flags;
    }


    int Vertex::GetFlags() const {
        return static_cast<ObjektivLib::DataPointEx *>(object)->flags;
    }
    Vertex::VertexAccess *Vertex::operator->() {return static_cast<VertexAccess *>(Super::operator ->());}
    const Vertex::VertexAccess *Vertex::operator->() const {return static_cast<const VertexAccess *>(Super::operator ->());}

    Vertex::operator const ObjektivLib::DataPointEx &() const {
        return *static_cast<const ObjektivLib::DataPointEx *>(object);
    }
    Vertex::operator ObjektivLib::DataPointEx &() {
        return *static_cast<ObjektivLib::DataPointEx *>(object);
    }
}
