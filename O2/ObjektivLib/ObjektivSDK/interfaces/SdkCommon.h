#pragma once

#ifndef _MATH3DP_HPP
#define Vector3 Vector3P
#endif

namespace ObjektivLib {    

    class ObjectData;
    class LODObject;
    class DataPointEx;
    class Selection;
    class NamedProperty;
    class ObjUVSet;
    struct ObjUVSet_Item;
    class CEdges;
    class AnimationPhase;
    class NamedSelection;

}

#ifndef DOXYGEN
#ifndef DLLEXTERN
#define DLLEXTERN __declspec(dllimport)
#endif
#else
#define DLLEXTERN
#endif

namespace OxygeneSDK {

    float DLLEXTERN GetSDKVersion();

}

/**GLOBAL SYMBOLS  */
class Vector3;