#pragma once

#pragma warning(disable: 4661)

#include "SdkCommon.h"
#include "Adapter.h"


namespace OxygeneSDK {  


    ///Standard vector used in OxygeneSDK
    class DLLEXTERN Vector: public Adapter<Vector3> {

        typedef Adapter<Vector3> Super;
    public:
        ///helps to access each coordinate
        struct VectorAccess{

            float x;
            float y;
            float z;
        };

        ///constructs adapter
        Vector(const Vector3 *x):Super(x) {}
        ///constructs adapter
        Vector(Vector3 *x,Super::Owner owner):Super(x, owner) {}
        ///constructs vector from reference to an vector in DLL
        Vector(const Vector3 &other):Super(&other) {}
        ///constructs new vector inited to 0,0,0
        Vector();
        ///constructs specified vector
        Vector(float x, float y = 0.0f, float z = 0.0f);
        
        ///assignment
        Vector &operator = ( const Vector& src) {
            Super::operator=(src);return *this;
        }

        ///returns X coordinate
        float X() const;
        ///returns Y coordinate
        float Y() const;
        ///returns Z coordinate
        float Z() const;
    
        VectorAccess *operator->();
        const VectorAccess *operator->() const;
        float &operator[](int idx);
        float operator[](int idx) const;
    };

    class DLLEXTERN Vertex: public Vector {

        typedef Vector Super ;
    public:        
        struct VertexAccess: public Super::VectorAccess{

            int flags;
        };

        Vertex(const ObjektivLib::DataPointEx *x);
        Vertex(ObjektivLib::DataPointEx *x, Super::Owner owner);
        Vertex(const ObjektivLib::DataPointEx &other);
        Vertex();
        Vertex(const Vector &vx, int flags = 0);
        Vertex(float x, float y = 0, float z = 0, int flags = 0);

          ///assignment
        Vertex &operator = ( const Vertex& src) {
            Super::operator=(src);return *this;
        }
        Vertex &operator = ( const Vector& src) {
            Super::operator=(src);return *this;
        }

        int GetFlags() const;

        VertexAccess *operator->();
        const VertexAccess *operator->() const;

        operator const ObjektivLib::DataPointEx &() const;
        operator ObjektivLib::DataPointEx &();


    };
}