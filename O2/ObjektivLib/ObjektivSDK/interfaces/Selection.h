#pragma once

#pragma warning(disable: 4661)

#include "SdkCommon.h"
#include "Adapter.h"
#include <vector>

#include "Mesh.h"



namespace OxygeneSDK {  

    class Mesh;

    class DLLEXTERN Selection: public Adapter<ObjektivLib::Selection> {
        typedef Adapter<ObjektivLib::Selection> Super;

        Selection(const Selection &src, const Selection &src2, 
            void (Selection::*oper)(const Selection &src)): Super(src) {
            (this->*oper)(src2);
        }
        Selection(const Selection &src, void (Selection::*oper)()): Super(src) {            
            (this->*oper)();
        }

        void SelectFacesFromPointsTouchModeXXX() {SelectFacesFromPointsTouchMode();}
    public:        

        Selection(const ObjektivLib::Selection *x):Super(x) {}
        Selection(ObjektivLib::Selection *x,Super::Owner owner):Super(x, owner) {}
        Selection( Mesh &object);
        Selection(const Selection &src, Mesh &object);

        void operator += ( const Selection &src );
        void operator -= ( const Selection &src );
        void operator &= ( const Selection &src );
        void operator |= ( const Selection &src );
        void Invert();
        float PointWeight( int i ) const ;
        void SetPointWeight( int i, float w );
        bool PointSelected( int i ) const ;
        void PointSelect( int i, bool sel=true );
        bool FaceSelected( int i ) const;
        void FaceSelect( int i, bool sel=true );
        void Clear();
        bool IsEmpty() const;
        int NPoints() const;
        int NFaces() const;
        void SelectPointsFromFaces();
        void SelectFacesFromPoints();
        bool SelectFacesFromPointsTouchMode();
        void SelectConnectedFaces();
        void SetObject(Mesh &object);
        const Mesh GetObject() const;

        void Validate();
        Vector GetCenter() const;
        std::pair<Vector, Vector> GetBoundingBox() const;

        void SelectAll();

        std::pair<int, int>  GetSelectedPointsRange() const;
        std::pair<int, int>  GetSelectedFacesRange() const;
    

        Selection operator + ( const Selection &src ) const {
            return Selection(*this,src,&Selection::operator+=);
        }            
        Selection operator - ( const Selection &src ) const {
            return Selection(*this,src,&Selection::operator-=);
        }
        Selection operator & ( const Selection &src ) const {
            return Selection(*this,src,&Selection::operator&=);
        }
        Selection operator | ( const Selection &src ) const {
            return Selection(*this,src,&Selection::operator|=);
        }

        Selection operator ~ () const {return Selection(*this,&Selection::Invert);}

        Selection GetSelectPointsFromFaces() const {
            return Selection(*this,&Selection::SelectPointsFromFaces);
        }
        Selection GetSelectFacesFromPoints() const {
            return Selection(*this,&Selection::SelectFacesFromPoints);
        }
        Selection GetSelectFacesFromPointsTouchMode() const {
            return Selection(*this,&Selection::SelectFacesFromPointsTouchModeXXX);
        }
        Selection GetSelectConnectedFaces() const {
            return Selection(*this,&Selection::SelectConnectedFaces);
        }
        Selection GetSelectAll() {
            return Selection(*this,&Selection::SelectAll);
        }



    };

    class DLLEXTERN NamedSelection: public Selection {

         typedef Selection Super;
    public:
        NamedSelection(const ObjektivLib::NamedSelection *x);
        NamedSelection(ObjektivLib::NamedSelection  *x,Super::Owner owner);
        NamedSelection(const Selection &other, const char *name):Super(other) {
            SetName(name);
        }
        NamedSelection(Mesh &mesh, const char *name):Super(mesh) {
            SetName(name);
        }

        const char *Name() const;
        void SetName(const char *name);


    };

    template<>
    inline Adapter<ObjektivLib::Selection>::Adapter();
}