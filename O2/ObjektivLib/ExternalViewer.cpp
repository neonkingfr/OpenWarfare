#include "common.hpp"
#include ".\externalviewer.h"
#include "ObjectData.h"
#include "LODObject.h"
#include <process.h>
#include <malloc.h>
#include "ObjToolProxy.h"
#include <Tlhelp32.h>


#define NOTIFIERTOPIC "Instance:%X"
#define NOTIFIERCLASS "ExternalViewerNotifierClass@Objektiv2"

#define USER_UPDATEMSG WM_APP+1222

#define USER_RELOAD ( WM_APP+0 )
#define USER_CLOSE ( WM_APP+1 )
#define USER_FILE_HANDLE ( WM_APP+2 )
#define USER_MUTEX_HANDLE ( WM_APP+3 )
#define USER_WINDOW_HANDLE ( WM_APP+4 )
#define USER_ANIM_PHASE ( WM_APP+5 )
#define USER_RUN_SCRIPT ( WM_APP+6 )
#define USER_PREVIEW_DIALOG ( WM_APP+7 )
#define USER_LIGHT_AMBIENT_R ( WM_APP+8 )
#define USER_LIGHT_AMBIENT_G ( WM_APP+9 )
#define USER_LIGHT_AMBIENT_B ( WM_APP+10 )
#define USER_LIGHT_DIFFUSE_R ( WM_APP+11 )
#define USER_LIGHT_DIFFUSE_G ( WM_APP+12 )
#define USER_LIGHT_DIFFUSE_B ( WM_APP+13 )
#define USER_LIGHT_REQUEST   ( WM_APP+14 )
#define USER_TIME_REQUEST    ( WM_APP+15 )
#define USER_WEATHER     ( WM_APP+16 )
#define USER_ADVANCE_TIME     ( WM_APP+17 )
#define USER_LIGHT_REQUEST_SHORT   ( WM_APP+18 )
#define USER_RELOAD_2 (WM_APP+20)
#define USER_RELOAD_CHANGED_FILES (WM_APP+19)
#define USER_RELOAD_MATERIAL (WM_APP+21)
#define USER_RELOAD_DONE (WM_APP+22)
#define USER_USE_RTM (WM_APP+23)
#define USER_ENVIRONMENT_INFO (WM_APP+24)
#define USER_ANIMATION_CONTROL (WM_APP+25)

void LogF(const char *str,...);

namespace ObjektivLib {


struct UserReloadMaterialStruct
{
  HWND notify; ///<handle to signal Objektiv2, that data has been taken over
  unsigned long matNameOffset; ///<offset in datablock containing material name
  unsigned long dataOffset; ///<offset in datablock containing material data
  size_t dataSize; ///<size of material data;
  char data[1]; ///<datablock
};

  FILE *_LogF;



struct UserReloadSharedMemStruct
{
  HWND notify;
  float LodBias;
  float animPhase;
  size_t datasize;
  char data[1];
};

struct UserAnimationControlStruct
{
  HANDLE unblock;
  int version;
  float value;
  float minval;
  float maxval;
  char controlName[1];
};


#define HANDLE_LOST(op) do {ErrorEnum tryres=HandleLostOp();if (tryres==ErrOK) return op ;else return tryres;} while (0)


ExternalViewer::ExternalViewer(void)
{
  _sharedlocker=NULL;
  _sharedmem=NULL;
  _viewer=NULL;
  _thread=NULL;
  _notifier=NULL;
  _window=NULL;
  _working=NULL;
  _config=NULL;
  _shareMemorySize=0;
  LogF("(ExtViewer) External viewer init");
}

ExternalViewer::~ExternalViewer(void)
{
  FinalDetach();
  LogF("(ExtViewer) External viewer done");
}


static unsigned int __stdcall StaticWorkingThread(void *ptr)
{
  ExternalViewer *viewer=(ExternalViewer *)ptr;    
  LogF("(ExtViewer) StaticWorkingThread started");
  viewer->WorkingThread();
  return 0;
}



static LRESULT DummyWinProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  if (msg==USER_UPDATEMSG) 
  {
    LogF("(ExtViewer) Received update message");
    ExternalViewer *self=(ExternalViewer *)GetWindowLong(hWnd,GWL_USERDATA);
    return self->OnUpdateObjectMessage(*(const LODObject *)lParam,(const char *)wParam);
  }
  else if (msg==USER_RELOAD_DONE)
  {
    ExternalViewer *self=(ExternalViewer *)GetWindowLong(hWnd,GWL_USERDATA);  
    LogF("(ExtViewer) Viewer notifies, that object is updated");
  }
  else if (msg>=WM_APP)
  {
    LogF("(ExtViewer) Received buldozer message, %d %08X %08X",msg,wParam,lParam);
    ExternalViewer *self=(ExternalViewer *)GetWindowLong(hWnd,GWL_USERDATA);  
    self->_msg.message=msg;
    self->_msg.wParam=wParam;
    self->_msg.lParam=lParam;
    self->_msg.time=GetMessageTime();
    SetEvent(self->_waitMsg);
    WaitForSingleObject(self->_waitMsgAck,1000);
  }
  return DefWindowProc(hWnd,msg,wParam,lParam);
}

void ExternalViewer::RunWorkingThread()
{
  LogF("(ExtViewer) RunWorkingThread startded");
  WNDCLASS cls;
  if (GetClassInfo(GetModuleHandle(NULL),NOTIFIERCLASS,&cls)==FALSE)
  {
    memset(&cls,0,sizeof(cls));
    cls.lpfnWndProc=(WNDPROC)DummyWinProc;
    cls.lpszClassName=NOTIFIERCLASS;
    RegisterClass(&cls);
  }
  _thread=(HANDLE)_beginthreadex(NULL,0,StaticWorkingThread,this,0,(unsigned int*)&_threadID);      
  Assert(_thread!=NULL);
  while (PostThreadMessage(_threadID,WM_APP+1000,0,0)==FALSE) 
    Sleep(10);
  LogF("(ExtViewer) RunWorkingThread finished");
}

bool ExternalViewer::OnUpdateObjectMessage(const LODObject &lod,const char *modelname)
{
  if (!IsWindow(_window)) 
  {
    return false;
  }
  LogF("(ExtViewer) Waiting for buffer release");
  WaitForSingleObject(_working,INFINITE);      
  LogF("(ExtViewer) Starting update");
  LODObject lodcopy;      
  PrepareForUpdate(lod,lodcopy);
  AfterModelPrepared(*lod.Active(),*lodcopy.Active());
  if (InSendMessage()) ReplyMessage(TRUE);
  if (_config->hNotifyStatus) PostMessage(_config->hNotifyStatus,_config->msgNotifyStatus,2,0);
  SaveObject(lodcopy,(_config->displayFlags & _config->NoModelConfig)?0:modelname);
  ReleaseMutex(_working);
  LogF("(ExtViewer) Update complette");
  if (_config->hNotifyStatus) 
  {
    WaitForInputIdle(_viewer,10000);
    PostMessage(_config->hNotifyStatus,_config->msgNotifyStatus,0,0);    
  }
  return true;
}

void ExternalViewer::WorkingThread()
{
  _waitMsg=CreateEvent(NULL,FALSE,FALSE,NULL);
  _waitMsgAck=CreateEvent(NULL,FALSE,FALSE,NULL);
  char buff[256];
  sprintf(buff,NOTIFIERTOPIC,_window);
  _notifier=CreateWindow(NOTIFIERCLASS,buff,0,0,0,0,0,0,0,GetModuleHandle(NULL),0);
  SetWindowLong(_notifier,GWL_USERDATA,(LONG)this);
  MSG msg;
  LogF("(ExtViewer) Working thread waiting for requests");
  while (GetMessageA(&msg,0,0,0))
  {
    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }
  DestroyWindow(_notifier);
  CloseHandle(_waitMsg);
  CloseHandle(_waitMsgAck);
  LogF("(ExtViewer) Working thread cleanup");
}

static HWND FindViewerWindowFast()
{
  return ::FindWindow("Arma 3","Buldozer");
}

static BOOL CALLBACK ViewerWindowEnum(HWND hwnd,  LPARAM lParam)
{
  HWND *seek=(HWND *)lParam;
  HWND wnd=CreateWindow("STATIC","Viewer Search...",WS_VISIBLE|WS_OVERLAPPEDWINDOW|SS_CENTER|SS_CENTERIMAGE,100,100,200,50,NULL,NULL,GetModuleHandle(NULL),NULL);
  UpdateWindow(wnd);
  PostMessage(hwnd,USER_LIGHT_REQUEST_SHORT,0,(LPARAM)wnd);
  MSG msg;
  do
  {
    msg.hwnd=0;
    msg.message=0;
    if (MsgWaitForMultipleObjects(0,0,FALSE,1000,QS_ALLPOSTMESSAGE)==WAIT_TIMEOUT) break;
    if (PeekMessage(&msg,wnd,0,0,PM_REMOVE))
      if (msg.message==USER_LIGHT_REQUEST_SHORT) break;
      else
        DispatchMessage(&msg);
  }
  while (true);  
  DestroyWindow(wnd);
  if (msg.hwnd==wnd) {*seek=hwnd;return false;}
  return true;
}


static bool FindFilenameInModuleList(DWORD processId, const char *modname)
{
  HANDLE modlist=CreateToolhelp32Snapshot(TH32CS_SNAPMODULE,processId);
  MODULEENTRY32 mentry;
  int lastChr = strlen(modname);
  mentry.dwSize=sizeof(mentry);
  if (Module32First(modlist,&mentry))
    do
    {      
      mentry.szExePath[lastChr]='\0';
      if (_stricmp(mentry.szExePath,modname)==0) 
      {
        CloseHandle(modlist);
        return true;
      }
    }
    while (Module32Next(modlist,&mentry));
  return false;
}

static HWND FindViewerWindowFromProcessName(const char *name,HANDLE snapshot, DWORD parentId = NULL)
{
  PROCESSENTRY32 pentry;
  pentry.dwSize=sizeof(pentry);
  if (Process32First(snapshot,&pentry))  
    do
    {      
      if (FindFilenameInModuleList(pentry.th32ProcessID,name)) //we found process
      {
        THREADENTRY32 tentry;
        tentry.dwSize=sizeof(tentry);
        if (Thread32First(snapshot,&tentry))
          do
          {
            if (tentry.th32OwnerProcessID==pentry.th32ProcessID)
              {
                if ((parentId==0)||(parentId=pentry.th32ParentProcessID))
                {
                  HWND seeker=NULL;
                  EnumThreadWindows(tentry.th32ThreadID,ViewerWindowEnum,(LPARAM)&seeker);
                  if(seeker) return seeker;
                }
              }
          }
          while (Thread32Next(snapshot,&tentry));
      }
    }
    while (Process32Next(snapshot,&pentry));
  return NULL;
}
static HWND stFindViewerWindow(const char *cmdLine, DWORD parentId = NULL)
{
  HANDLE snapshot=::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS|TH32CS_SNAPTHREAD,0);
  if (*cmdLine!=0)
  {
    char *buff=(char *)alloca(strlen(cmdLine)+1);
    for (int i=(strrchr(cmdLine,'\\')-cmdLine);;i++)
    {
      if (isspace(cmdLine[i])||(cmdLine[i]=='.') || cmdLine[i]==0)
      {
        strncpy(buff,cmdLine,i);
        buff[i]=0;
        HWND hWnd=FindViewerWindowFromProcessName(buff,snapshot,parentId);
        if (hWnd) 
        {
          CloseHandle(snapshot);
          return hWnd;
        }              
        if (cmdLine[i]==0) break;
      }
    }
  }
  CloseHandle(snapshot);
  return FindViewerWindowFast();
}

static void GetExeDir(char *buff)
{
  char *c;
  if (buff[0]=='"')
  {
    c=strchr(buff+1,'"');
    if (c!=NULL) 
    {
      *c=0;
      strcpy(buff,buff+1);
    }
    else
    {
      c=strchr(buff,32);
      if (c) *c=0;
    }
  }
  else
  {
    c=strchr(buff,32);
    if (c) *c=0;
  }
  c=strrchr(buff,'\\');
  if (c!=NULL) c[1]=0; else buff[0]=0;
}

static void GetExeName(char *buff)
{
  char *c;
  if (buff[0]=='"')
  {
    c=strchr(buff+1,'"');
    if (c!=NULL) 
    {
      *c=0;
      strcpy(buff,buff+1);
    }
  }
  c=strrchr(buff,'\\');
  c=strchr(c,' ');
  if (c!=NULL) c[1]=0;
}

static BOOL CALLBACK ViewerWindowSeeker(HWND hwnd,  LPARAM lParam)
{
  ExternalViewer *self=(ExternalViewer *)lParam;
  self->_window=hwnd;
  return FALSE;
}

bool IsSoftWrap(char *_name,size_t size)
{
  char name[_MAX_PATH];
  strcpy_s(name,_MAX_PATH,_name); 
  int lastchr = strlen(name);
  for (int i=lastchr;i>0;i--)
  {
   if ((name[i]=='.')||(name[i]==' ')||(name[i]==0))
   {
     name[i]=0;
     strcat_s(name,_MAX_PATH,".locked");

     if (Pathname::TestFile(name))
     {
       strcpy_s(_name,size,name);
       return true;
     }
   }
  }
return false;
}

IExternalViewer::ErrorEnum ExternalViewer::Start()
{
  HWND active=GetForegroundWindow();
  if (_config==NULL) return ErrNoConfig;
  if ((~_config->displayFlags & _config->DontShareViewer) && Attach()==ErrOK) return ErrOK;
  char *exeDir=strcpy((char *)alloca(strlen(_config->startupLine)+1),_config->startupLine);
  GetExeDir(exeDir);
  if (_config->startupFolder.GetLength()!=0) exeDir=_config->startupFolder.MutableData();

  STARTUPINFO sInfo;
  PROCESS_INFORMATION pInfo;
  memset(&sInfo,0,sizeof(sInfo));
  sInfo.cb=sizeof(sInfo); 
  if (_config->consoleInput && _config->consoleOutput)
  {
    sInfo.dwFlags|=STARTF_USESTDHANDLES;
    sInfo.hStdInput=_config->consoleInput;
    sInfo.hStdOutput=_config->consoleOutput;
    sInfo.hStdError=_config->consoleError?_config->consoleError:_config->consoleOutput;
  }

  LogF("(ExtViewer) Starting buldozer %s",_config->startupLine.Data());
  if (::CreateProcess(NULL,const_cast<char *>(_config->startupLine.Data()),NULL,NULL,
    TRUE,NORMAL_PRIORITY_CLASS,NULL,exeDir,&sInfo,&pInfo)==TRUE)
  {
    HWND wnd=NULL;  
    char exeName[_MAX_PATH];
    strcpy(exeName,_config->buldozerName);
    if (IsSoftWrap(exeName,_MAX_PATH))
    {
      CloseHandle(pInfo.hThread);
      CloseHandle(pInfo.hProcess);
      LogF("(ExtViewer) Trying connect buldozer created by SoftWrap.");
      for (int i=0;i<10;i++)
      {
       if ((wnd=stFindViewerWindow(exeName,pInfo.dwProcessId))==0) 
       {
         LogF("(ExtViewer) Waiting for init buldozer.");
         Sleep(500);
       }
       else break;
      }
      if (wnd==0)
      {
        LogF("(ExtViewer) Wait timeout.");
        return ErrTimeout;
      }
      DWORD processID;
      GetWindowThreadProcessId(wnd,&processID);
      _viewer=OpenProcess(SYNCHRONIZE|PROCESS_DUP_HANDLE|PROCESS_QUERY_INFORMATION|PROCESS_TERMINATE|PROCESS_SET_INFORMATION,TRUE,processID);
      if (_viewer==NULL) return ErrAttachFailed;
    }
    else
    {
      while (wnd==NULL)
      {
        LogF("(ExtViewer) Waiting for init buldozer.");
        if (WaitForInputIdle(pInfo.hProcess,_config->init_timeout)!=0) 
        {
          LogF("(ExtViewer) Wait timeout.");
          SetPriorityClass(pInfo.hProcess,BELOW_NORMAL_PRIORITY_CLASS);
          CloseHandle(pInfo.hThread);
          CloseHandle(pInfo.hProcess);
          return ErrTimeout;
        }
        EnumThreadWindows(pInfo.dwThreadId,ViewerWindowEnum,(LPARAM)&wnd);
        if (wnd==NULL)
        {
          if (WaitForSingleObject(pInfo.hThread,2000)!=WAIT_TIMEOUT)
          {
            LogF("(ExtViewer) Wait timeout.");
            SetPriorityClass(pInfo.hProcess,BELOW_NORMAL_PRIORITY_CLASS);
            CloseHandle(pInfo.hThread);
            CloseHandle(pInfo.hProcess);
            return ErrAttachFailed;
          }
        }
      }
      _viewer=pInfo.hProcess;
      SetPriorityClass(pInfo.hProcess,BELOW_NORMAL_PRIORITY_CLASS);
      CloseHandle(pInfo.hThread);
    }
    _window=wnd;  
    LogF("(ExtViewer) Buldozer init succes, finish init.");
    return CommonInit();
  }
   LogF("(ExtViewer) Buldozer init failed.");
  return ErrCreateProcessFail;


}

IExternalViewer::ErrorEnum ExternalViewer::Attach()
{
  _window=stFindViewerWindow(_config->buldozerName);
  if (_window==NULL) return ErrAttachFailed;
  DWORD processID;
  GetWindowThreadProcessId(_window,&processID);
  _viewer=OpenProcess(SYNCHRONIZE|PROCESS_DUP_HANDLE|PROCESS_QUERY_INFORMATION|PROCESS_TERMINATE|PROCESS_SET_INFORMATION,TRUE,processID);
  if (_viewer==NULL) return ErrAttachFailed;
  LogF("(ExtViewer) Waiting for viewers response.");
  if (WaitForInputIdle(_viewer,20000)!=0) 
  {
    LogF("(ExtViewer) timeout.");
    CloseHandle(_viewer);_viewer=NULL;
    return ErrTimeout;
  }
  LogF("(ExtViewer) Attach successed.");
  return CommonInit();
}

void ExternalViewer::CleanUp()
{
  LogF("(ExtViewer) Starting cleanup.");
  if (_thread) 
  {
    PostThreadMessage(_threadID,WM_QUIT,0,0);
    WaitForSingleObject(_thread,INFINITE);
    CloseHandle(_thread);_thread=NULL;
  }
  LogF("(ExtViewer) Waiting for release memory.");
  WaitForSingleObject(_sharedlocker,10000);
  LogF("(ExtViewer) Releasing objects and memory.");
  CloseHandle(_sharedmem);_sharedmem=NULL;
  ReleaseMutex(_sharedlocker);
  CloseHandle(_sharedlocker);_sharedlocker=NULL;
  CloseHandle(_viewer);_viewer=NULL;
  CloseHandle(_thread);_thread=NULL;
  _window=NULL;
  LogF("(ExtViewer) Finish cleanup.");
}

IExternalViewer::ErrorEnum ExternalViewer::Detach()
{
  if (_thread==NULL) return ErrNotInicialized;
  LogF("(ExtViewer) Detach.");
  CleanUp();
  return ErrOK;
}

IExternalViewer::ErrorEnum ExternalViewer::CommonInit()
{
  if (~_config->displayFlags & _config->DirectMode)
  {
    _working=CreateMutex(NULL,FALSE,NULL);  
    RunWorkingThread();
  }
  else
  {
    _working=NULL;
    _thread=NULL;
  }
  return ErrOK;
}

IExternalViewer::ErrorEnum ExternalViewer::Stop()
{
  if (_window==NULL) return ErrNotInicialized;
  LogF("(ExtViewer) Stop.");
  PostMessage(_window,USER_CLOSE,0,0);
  CleanUp();
  return ErrOK;
}

IExternalViewer::ErrorEnum ExternalViewer::Restart()
{
  if (_window!=NULL) 
    {
    HANDLE waithandle;
    LogF("(ExtViewer) Restart requests stop current viewer");
    DuplicateHandle(GetCurrentProcess(),_viewer,GetCurrentProcess(),&waithandle,0,FALSE,DUPLICATE_SAME_ACCESS);
    Stop();
    WaitForSingleObject(waithandle,20000);
    CloseHandle(waithandle);
    }  
  return Start();
}

IExternalViewer::ErrorEnum ExternalViewer::Update(LODObject *lod, const char *p3dname, DWORD waittime)
{  
  if (_window==NULL) return ErrNotInicialized;
  if (_thread!=NULL)
  {
    if (_window==NULL) return ErrNotInicialized;
    DWORD status=WaitForSingleObject(_working,waittime);
    if (status==WAIT_TIMEOUT) return ErrBusy;
    if (status!=WAIT_OBJECT_0) return ErrFailed;
    ReleaseMutex(_working);    
    LogF("(ExtViewer) Requesting working thread for update.");
    if (_config->hNotifyStatus) SendNotifyMessage(_config->hNotifyStatus,_config->msgNotifyStatus,1,0);
    int result=SendMessage(_notifier,USER_UPDATEMSG,(WPARAM)p3dname,(LPARAM)lod);
    if (result==FALSE) HANDLE_LOST(Update(lod,p3dname,waittime));      
  }
  else
  {
    if (_window==NULL) return ErrNotInicialized;
    if (!IsWindow(_window)) HANDLE_LOST(Update(lod,p3dname,waittime));            
    DWORD status=WaitForSingleObject(_sharedlocker,waittime);
    if (status==WAIT_TIMEOUT) return ErrBusy;
    if (status!=WAIT_OBJECT_0) return ErrFailed;
    SaveObject(*lod,p3dname);
    ReleaseMutex(_sharedlocker);
  }
  return ErrOK;  
}


IExternalViewer::ErrorEnum ExternalViewer::FinalDetach()
{
  LogF("(ExtViewer) FinalDetach.");
  char buff[256];

  if (_window==NULL) return ErrNotInicialized;

  PostThreadMessage(_threadID,WM_QUIT,0,0);
  WaitForSingleObject(_thread,10000);
  sprintf(buff,NOTIFIERTOPIC,_window);
  HWND hWnd=FindWindow(NOTIFIERCLASS,buff);
  if (hWnd==NULL) Stop();else Detach();
  return ErrOK;     
}

IExternalViewer::ErrorEnum ExternalViewer::StartScript(const char *scriptname)
{
  if (_window==NULL) return ErrNotInicialized;

  bool out;  
  if (!IsWindow(_window)) HANDLE_LOST(StartScript(scriptname));
  ATOM atm=GlobalAddAtom(scriptname);
  PostMessage(_window, USER_RUN_SCRIPT, 0,(LPARAM)atm);
  for (int i=0;i<20;i++)
  {
    out=GlobalFindAtom(scriptname)!=atm;
    if (out) break;
    Sleep(100);
  }
  GlobalDeleteAtom(atm);
  return out?ErrOK:ErrFailed;
}

IExternalViewer::ErrorEnum ExternalViewer::IsReady()
{
  if (_window==NULL) return ErrNotInicialized;
  if (!IsWindow(_window)) HANDLE_LOST(IsReady());
  DWORD status=WaitForSingleObject(_working,0);
  if (status==WAIT_TIMEOUT) return ErrBusy;
  if (status!=WAIT_OBJECT_0) return ErrFailed;
  ReleaseMutex(_working);
  return ErrOK;
}

IExternalViewer::ErrorEnum ExternalViewer::SwitchToViewer()
{
  if (_window==NULL) return ErrNotInicialized;
  if (!IsWindow(_window)) HANDLE_LOST(SwitchToViewer());
  ::SetForegroundWindow(_window);
  ::ShowWindow(_window,SW_RESTORE);
  return ErrOK;
}

IExternalViewer::ErrorEnum ExternalViewer::HasFocus()
{
  if (_window==NULL) return ErrNotInicialized;
  if (!IsWindow(_window)) HANDLE_LOST(HasFocus());
  if (GetForegroundWindow()==_window) return ErrOK;
  return ErrFailed;
}


void ExternalViewer::PrepareForUpdate(const LODObject &lod, LODObject &output)
{
  bool selStateNeed=(_config->displayFlags & ExternalViewerConfig::InvertedSelection)==0;
  LogF("(ExtViewer) Preparing for update - head");
  const ObjectData &odata=*lod.Active();
  ObjectData &result=*output.Active();  
  if (_config->displayFlags & ExternalViewerConfig::MultipleLODs)
    output.SetResolution(output.ActiveLevel(),lod.Resolution(lod.ActiveLevel()));
  int *remap=(int *)alloca((odata.NPoints()+1)*sizeof(int));  
  int *remapfaces=(int *)alloca((odata.NFaces()+1)*sizeof(int));  
  bool *killfaces=(bool *)alloca(odata.NFaces());
  memset(remap,0xFF,sizeof(int)*(odata.NPoints()+1));
  memset(remapfaces,0xFF,sizeof(int)*(odata.NFaces()+1));
  memset(killfaces,0,odata.NFaces());
  LogF("(ExtViewer) Preparing for update - crop phase");


  int nextindex=0;
  int i;
  const Selection *hidden=odata.GetHidden();
  const Selection *selected=odata.GetSelection();
  bool isselected=selected->NFaces()>0;
  if (_config->selMode==ExternalViewerConfig::SelectDisplay && isselected)       
  {for(int i=0;i<odata.NFaces();i++) if (!selected->FaceSelected(i)==selStateNeed) killfaces[i]=true;}
  if ((_config->displayFlags & ExternalViewerConfig::HideHidden) && !hidden->IsEmpty())
  {for(int i=0;i<odata.NFaces();i++) if (hidden->FaceSelected(i)==selStateNeed) killfaces[i]=true;}  
  LogF("(ExtViewer) Preparing for update - proxy creating phase");
  
  int proxycnt=0;
  if (_config->displayFlags & ExternalViewerConfig::HideProxies) 
  {
    const char *prox=ObjToolProxy::ProxyPrefix;
    int plen=strlen(prox);
    if (prox!=NULL)
    {
      int i;
      for (i=0;i<MAX_NAMED_SEL;i++) 
      {
        const NamedSelection *sel=odata.GetNamedSel(i);
        if (sel!=NULL && strncmp(sel->Name(),prox,plen)==0)
        {
          int face=odata.GetTool<ObjToolProxy>().FindProxyFace(sel->Name());
          killfaces[face]=1;
        }
      }
    }
  }
  LogF("(ExtViewer) Preparing for update - remaping phase");

  
  for (i=0;i<odata.NFaces();i++) if (!killfaces[i])
  {
    FaceT fc(odata,i);
    FaceT newfc;
    newfc.CreateIn(&result);
    fc.CopyFaceTo(newfc);
    bool hidv=false;
    remapfaces[i]=newfc.GetFaceIndex();

    float mapU[]={0,0,1,1};
    float mapV[]={0,1,1,0};
    for (int j=0;j<fc.N();j++) 
    {
      int oldpt=fc.GetPoint(j);
      if (oldpt>=odata.NPoints()) oldpt=0;
      if (remap[oldpt]==-1) 
      {
        remap[oldpt]=result.NPoints();          
        PosT &pt=*result.NewPoint();
        pt=odata.Point(oldpt);        
        hidv|=(pt.flags & POINT_SPECIAL_HIDDEN)!=0;
        if (_config->selMode==ExternalViewerConfig::SelectLight)
        {            
          if (!selected->PointSelected(oldpt)==selStateNeed)
          {pt.flags = (pt.flags & ~(POINT_LIGHT_MASK | POINT_USER_MASK)) | POINT_AMBIENT;}
          else
          {pt.flags = (pt.flags & ~(POINT_LIGHT_MASK | POINT_USER_MASK)) | POINT_NOLIGHT;}    
        }
        pt.flags &= ~POINT_LAND_MASK;
      }
      newfc.SetPoint(j,remap[newfc.GetPoint(j)]);
      if (_config->displayFlags & _config->WireFrame)
      {
        newfc.SetTexture(_config->wframeTex);
        newfc.SetUV(j,mapU[j],mapV[j]);
        newfc.SetMaterial("");
      }
    }    
    if (_config->displayFlags & ExternalViewerConfig::HideTextures) newfc.SetTexture("#(rgb,8,8,3)color(0.5,0.5,0.5,1)");
    else if (_config->displayFlags & ExternalViewerConfig::RemoveUnsupportedTexs)
    {      
      const char *name=newfc.GetTexture();
      const char *ext=Pathname::GetExtensionFromPath(name);
      if (_stricmp(ext,".paa")!=0 && _stricmp(ext,".tga")!=0 && _stricmp(ext,".pac")!=0 && name[0]!='#') newfc.SetTexture("");
    }
    if (_config->displayFlags & ExternalViewerConfig::HideMaterials) newfc.SetMaterial("");
    else if (_config->displayFlags & ExternalViewerConfig::RemoveUnsupportedTexs)
    {      
      const char *ext=Pathname::GetExtensionFromPath(newfc.GetMaterial());
      if (_stricmp(ext,".rvmat")!=0) newfc.SetMaterial("");
    }
    if (isselected && !selected->FaceSelected(i)==selStateNeed)
      if (_config->selMode==ExternalViewerConfig::SelectTexture) newfc.SetTexture(_config->selectTex);
      else if (_config->selMode==ExternalViewerConfig::SelectMaterial) newfc.SetMaterial(_config->selectMat);    
      else if (_config->selMode==ExternalViewerConfig::SelectWFrame) 
      {
        newfc.SetTexture(_config->wframeTex);
        newfc.SetMaterial(NULL);
        for (int i=0;i<newfc.N();i++) newfc.SetUV(i,mapU[i],mapV[i]);
      }
   if (newfc.GetTexture().GetLength()==0 &&  newfc.GetMaterial().GetLength()==0)
    newfc.SetTexture("#(rgb,8,8,3)color(0.5,0.5,0.5,1)");

  }

  Selection isolated(const_cast<ObjectData *>(&odata));
  for (int i=0,cnt=odata.NFaces();i<cnt;i++) isolated.FaceSelect(i,true);
  isolated.SelectPointsFromFaces();
  for (int i=0,cnt=odata.NPoints();i<cnt;i++) if (!isolated.PointSelected(i) && remap[i]==-1)
  {
    remap[i]=result.NPoints();
    PosT &pt=*result.NewPoint();
    pt=odata.Point(i);
  }

  LogF("(ExtViewer) Preparing for update - selection remaping phase");
  for (i=0;i<MAX_NAMED_SEL;i++) 
  {
    const NamedSelection *nsel=odata.GetNamedSel(i);
    if (nsel!=NULL && nsel->Name()[0]!='.' && nsel->Name()[0]!='-')
    {
      Selection newsel(&result);
      for (int i=0;i<odata.NPoints();i++)
        if (remap[i]!=-1) newsel.PointSelect(remap[i],nsel->PointSelected(i));
      for (int i=0;i<odata.NFaces();i++)
        if (remapfaces[i]!=-1) newsel.FaceSelect(remapfaces[i],nsel->FaceSelected(i));
      result.SaveNamedSel(nsel->Name(),&newsel);
    }
  }
  LogF("(ExtViewer) Preparing for update - normalizing normals");
  if (odata.NNormals()==0)
    result.RecalcNormals(true);
  else
    for (i=0;i<odata.NNormals();i++)
    {
      VecT &v=*result.NewNormal();
      v=odata.Normal(i);
    }
  LogF("(ExtViewer) Preparing for update - animations remap");
  if (_config->freezeAnim!=-1 && _config->freezeAnim<odata.NAnimations())
    {
      const AnimationPhase &phs=*odata.GetAnimation(_config->freezeAnim);
      for (int i=0;i<odata.NPoints();i++) if (remap[i]!=-1)      
        result.Point(remap[i]).SetPoint(phs[i]);
    }
    else
    {
      for (int i=0;i<odata.NAnimations();i++)
      {
        const AnimationPhase &phs=*odata.GetAnimation(i);
        AnimationPhase newadd(&result);
        newadd.SetTime(phs.GetTime());
        newadd.Validate();
        for (int j=0;j<odata.NPoints();j++) if (remap[j]!=-1)
          newadd[remap[j]]=phs[j];
        result.AddAnimation(newadd);
      }
      int curanim=odata.CurrentAnimation();
      if (curanim>=0)
      {
        result.RedefineAnimation(curanim);
        result.UseAnimation(curanim);
      }
    }
    VecT ObjectCenter=VecT(0,0,0);
    VecT lower=ObjectCenter,higher=ObjectCenter;    
    if ( _config->groundPos==_config->GPCenter||
        _config->groundPos==_config->GPLowest
        )
    {
      if (result.NPoints()>0)
      {
        higher=lower=result.Point(0);
      }      
      for (i=0;i<result.NPoints();i++)
      {
        VecT &test=result.Point(i);
        if (lower[0]>test[0]) lower[0]=test[0];
        if (lower[1]>test[1]) lower[1]=test[1];
        if (lower[2]>test[2]) lower[2]=test[2];
        if (higher[0]<test[0]) higher[0]=test[0];
        if (higher[1]<test[1]) higher[1]=test[1];
        if (higher[2]<test[2]) higher[2]=test[2];
      }
    ObjectCenter=VecT((lower[0]+higher[0])*0.5f,(lower[1]+higher[1])*0.5f,(lower[2]+higher[2])*0.5f);
    }
    if ((_config->displayFlags & _config->ShowMarkers) && (_config->markerObj.GetLength()!=0))
    {
      LogF("(ExtViewer) Preparing for update - markers");
      const Selection *hid=odata.GetHidden();
      for (int i=0,cnt=odata.NPoints();i<cnt;i++) if (!isolated.PointSelected(i) && !hid->PointSelected(i))
      {
        VecT v=odata.Point(i);        
        result.GetTool<ObjToolProxy>().CreateProxy(_config->markerObj,v,0.1f);
      }
    }
    if (_config->groundPos!=_config->GPDisabled && _config->groundObj.GetLength()!=0)
    {
      float pin[3]={0,0,0};
      switch (_config->groundPos)
      {
      case _config->GPDefault:
        {
          int lc=lod.FindLevelExact(LOD_LANDCONTACT);
          if (lc!=-1)
          {
            ObjectData *landContact=lod.Level(lc);
            if (landContact->NPoints()>0)
              pin[1]=landContact->Point(0)[1];
            for (int i=1,cnt=landContact->NPoints();i<cnt;i++)
            {
              const VecT &pt=landContact->Point(i);
              if (pin[1]>pt[1]) pin[1]=pt[1];
            }
          }
        }break;
      case _config->GPZero:break;
      case _config->GPCenter:
        pin[0]=ObjectCenter[0];
        pin[1]=ObjectCenter[1];
        pin[2]=ObjectCenter[2];
        break;
      case _config->GPLowest:pin[1]=lower[1];break;
      }
      result.GetTool<ObjToolProxy>().CreateProxy(_config->groundObj,pin,0.1f);
    }
  LogF("(ExtViewer) Preparing for update - handling multiple lods");
  if (_config->displayFlags & ExternalViewerConfig::MultipleLODs)
  {
      for (i=0;i<lod.NLevels();i++) if (i!=lod.ActiveLevel())
      {
        int lev=output.AddLevel(ObjectData(),lod.Resolution(i));
        *output.Level(lev)=*lod.Level(i);
      }
  }
  else 
  {
    if (_config->displayFlags & ExternalViewerConfig::ShowShadow)
    {
      int shd=lod.FindLevel(LOD_SHADOW_MIN);     
      if (shd>0 && shd!=lod.ActiveLevel())
      {
        float rres=lod.Resolution(shd);
        if (rres>=LOD_SHADOW_MIN  && rres<LOD_SHADOW_MAX)
        {          
          int lev=output.AddLevel(ObjectData(),rres);
          *output.Level(lev)=*lod.Level(shd);
        }
      }
    }
    if (lod.Resolution(lod.ActiveLevel())!=LOD_MEMORY)
    {
      int mem=lod.FindLevelExact(LOD_MEMORY);
      if (mem!=-1)    
        output.AddLevel(*lod.Level(mem),lod.Resolution(mem));    
    }
  }
  if (_config->centerMode!=_config->Autocenter)
  {  
    LogF("(ExtViewer) Preparing for update - centering object");

    VecT ObjectCenter=VecT(0,0,0);
    VecT lower=ObjectCenter,higher=ObjectCenter;    
    for (int i=0;i<output.NLevels();i++)
    {
      ObjectData *odata=output.Level(i);
      for (int j=0;j<odata->NPoints();j++)
      {
        VecT &test=odata->Point(j);
        if (lower[0]>test[0]) lower[0]=test[0];
        if (lower[1]>test[1]) lower[1]=test[1];
        if (lower[2]>test[2]) lower[2]=test[2];
        if (higher[0]<test[0]) higher[0]=test[0];
        if (higher[1]<test[1]) higher[1]=test[1];
        if (higher[2]<test[2]) higher[2]=test[2];
      }
    }
    ObjectCenter=VecT((lower[0]+higher[0])*0.5f,(lower[1]+higher[1])*0.5f,(lower[2]+higher[2])*0.5f);
    ObjectData *result=output.Level(0);
    VecT pin=_config->pin;
    if (_config->centerMode==ExternalViewerConfig::CenterToSelection)
      pin=selected->GetCenter();    
    VecT diff=pin-ObjectCenter;
    VecT newpt;
    if (diff[0]<0) newpt[0]=lower[0]+diff[0]*2; else newpt[0]=higher[0]+diff[0]*2;
    if (diff[1]<0) newpt[1]=lower[1]+diff[1]*2; else newpt[1]=higher[1]+diff[1]*2;
    if (diff[2]<0) newpt[2]=lower[2]+diff[2]*2; else newpt[2]=higher[2]+diff[2]*2;
    result->NewPoint()->SetPoint(newpt);
  }
  LogF("(ExtViewer) Preparing for update - all done");
if (remap[odata.NPoints()]!=-1) MessageBox(NULL,"overflow",NULL,NULL);
/*Pathname pthname("t:\\temp.p3d");
output.Save(pthname,0x9999,false,NULL,NULL);*/
}

bool ExternalViewer::TryToSaveObject(const LODObject &object, const char *modelname)
{
  if (modelname==NULL) modelname="";
  UserReloadSharedMemStruct *data
    =reinterpret_cast<UserReloadSharedMemStruct  *>
    (MapViewOfFile(_sharedmem,FILE_MAP_ALL_ACCESS,0,0,0));
  int maxdatasize=_shareMemorySize-sizeof(*data);
  LogF("(ExtViewer) Trying save object, memory reserved %d",maxdatasize);
  ostrstream outstream(data->data,maxdatasize);
  object.Save(outstream,OBJDATA_LATESTVERSION,true);
  _objectSize=outstream.tellp();
  data->datasize=_objectSize;
  outstream.write(modelname,strlen(modelname)+1);
  LogF("(ExtViewer) Trying save object, memory used %d",_objectSize);
  UnmapViewOfFile(data);
  if (!outstream) return false;  
  return true;
}

void ExternalViewer::SaveObject(const LODObject &lod, const char *modelname)
{
  LogF("(ExtViewer) Saving object for update");
  SECURITY_ATTRIBUTES security;
  security.nLength=sizeof(SECURITY_ATTRIBUTES);
  security.lpSecurityDescriptor=NULL;
  security.bInheritHandle=true;
  if (_sharedmem==NULL) 
  {
    LogF("(ExtViewer) Shared memory first init");
    _shareMemorySize=1024*1024;
    _sharedmem=CreateFileMapping(NULL,&security,PAGE_READWRITE,0,_shareMemorySize,NULL);
    if (_sharedmem==NULL) return;
    _sharedlocker=CreateMutex(&security,FALSE,NULL);
  }
  LogF("(ExtViewer) Requesting shared memory lock");
  if (WaitForSingleObject(_sharedlocker,10000)!=WAIT_OBJECT_0) return;
  while (TryToSaveObject(lod,modelname)==false)
  {
    LogF("(ExtViewer) Expanding memory");
    _shareMemorySize*=2;
    CloseHandle(_sharedmem);
    _sharedmem=CreateFileMapping(NULL,&security,PAGE_READWRITE,0,_shareMemorySize,NULL);
    if (_sharedmem==NULL) return;
  }
  LogF("(ExtViewer) Preparing handles");
  HANDLE dup_mem,dup_mutex;
  UserReloadSharedMemStruct *data
    =reinterpret_cast<UserReloadSharedMemStruct  *>
    (MapViewOfFile(_sharedmem,FILE_MAP_ALL_ACCESS,0,0,sizeof(UserReloadSharedMemStruct)));  
  if (lod.Active()->NAnimations() && lod.Active()->CurrentAnimation()>=0)
    data->animPhase=lod.Active()->GetAnimation(lod.Active()->CurrentAnimation())->GetTime();
  else
    data->animPhase=0.0f;
  data->LodBias=_config->lodBias;
  data->notify=_notifier;
  UnmapViewOfFile((LPCVOID *)data);
  DuplicateHandle(GetCurrentProcess(),_sharedmem,_viewer,&dup_mem,0,TRUE,DUPLICATE_SAME_ACCESS);
  DuplicateHandle(GetCurrentProcess(),_sharedlocker,_viewer,&dup_mutex,0,TRUE,DUPLICATE_SAME_ACCESS);
  LogF("(ExtViewer) Sending update");
  PostMessage(_window,USER_RELOAD_2 ,(WPARAM)dup_mutex,(LPARAM)dup_mem);
  ReleaseMutex(_sharedlocker);
  MSG msg;
  WaitForMessage(msg,5000,USER_RELOAD_2);
  LogF("(ExtViewer) Update finish");
  if (_config->displayFlags & _config->PopupViewer)
  {
    SetWindowPos(_window,HWND_TOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOSENDCHANGING|SWP_NOACTIVATE|SWP_DRAWFRAME);
    SetWindowPos(_window,HWND_NOTOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_NOSENDCHANGING|SWP_NOACTIVATE|SWP_DRAWFRAME);
  }
}

IExternalViewer::ErrorEnum ExternalViewer::SetWeather(float overcast, float fog)
{
  if (_window==NULL) return ErrNotInicialized;
  if (!IsWindow(_window)) HANDLE_LOST(SetWeather(overcast, fog));
  PostMessage(_window,USER_WEATHER,*(WPARAM *)&overcast,*(LPARAM *)&fog);
  return ErrOK;
}

IExternalViewer::ErrorEnum ExternalViewer::GetWeather(float &overcast, float &fog)
{  
  MSG msg;
  if (_window==NULL) return ErrNotInicialized;
  if (!IsWindow(_window)) HANDLE_LOST(GetWeather(overcast, fog));  
  PostMessage(_window,USER_WEATHER,0xFFFFFFFF,(LPARAM)_notifier);
  if (WaitForMessage(msg,1000,USER_WEATHER)==ErrTimeout) return ErrTimeout;
  overcast=*(float *)&_msg.wParam;
  fog=*(float *)&_msg.lParam;
  SetEvent(_waitMsg);
  return ErrOK;
}

IExternalViewer::ErrorEnum ExternalViewer::RedrawViewer()
{
  LogF("(ExtViewer) RedrawViewer");
  MSG msg;
  if (_window==NULL) return ErrNotInicialized;
  if (!IsWindow(_window)) HANDLE_LOST(IsReady());
  PostMessage(_window,USER_RELOAD_CHANGED_FILES,0,(LPARAM)_notifier);
  if (WaitForMessage(msg,1000,USER_RELOAD_CHANGED_FILES)==ErrTimeout) return ErrTimeout;
  return ErrOK;
}

IExternalViewer::ErrorEnum ExternalViewer::WaitForMessage(MSG &msg, DWORD timeout , UINT message)
{
  LogF("(ExtViewer) Waiting for message %d thread %d, message thread %d",message,GetCurrentThreadId(),_threadID);
  if (GetCurrentThreadId()==_threadID)
  {
    do
    {
    if (MsgWaitForMultipleObjects(0,NULL,FALSE,timeout,QS_ALLPOSTMESSAGE)==WAIT_TIMEOUT) return ErrTimeout;
      while (PeekMessage(&msg,NULL,0,0,PM_REMOVE)==FALSE)
      {
        if (MsgWaitForMultipleObjects(0,NULL,FALSE,timeout,QS_ALLPOSTMESSAGE)==WAIT_TIMEOUT) return ErrTimeout;
      }
      if (msg.message==WM_QUIT) 
      {
        PostQuitMessage(msg.wParam);
        return ErrFailed;
      }
      DispatchMessage(&msg);
    }
    while (message!=0 && msg.message!=message);
    LogF("(ExtViewer) Message arrived %d",msg.message);
    return ErrOK;
  }
  else
  {
    if (WaitForSingleObject(_waitMsg,timeout)!=WAIT_OBJECT_0) return ErrTimeout;  
    if (message!=0)
      while (_msg.message!=message)
      {
        SetEvent(_waitMsgAck);
        if (WaitForSingleObject(_waitMsg,timeout)!=WAIT_OBJECT_0) return ErrTimeout;  
      }
      msg=_msg;
      SetEvent(_waitMsgAck);
      LogF("(ExtViewer) Message arrived %d",msg.message);
      return ErrOK;
  }
}

IExternalViewer::ErrorEnum ExternalViewer::TimeOfDay(int advance_insec, DWORD *cur_insec)
{
  MSG msg;
  if (_window==NULL) return ErrNotInicialized;
  if (!IsWindow(_window)) HANDLE_LOST(TimeOfDay(advance_insec, cur_insec));  
  PostMessage(_window,USER_ADVANCE_TIME,(WPARAM)advance_insec,(LPARAM)_notifier);
  if (WaitForMessage(msg,1000,USER_ADVANCE_TIME)==ErrTimeout) return ErrTimeout;
  if (cur_insec) *cur_insec=msg.lParam;
  return ErrOK;
}

IExternalViewer::ErrorEnum ExternalViewer::GetLightColors(COLORREF &ambient, COLORREF &diffuse)
{
  MSG msg;
  if (_window==NULL) return ErrNotInicialized;
  if (!IsWindow(_window)) HANDLE_LOST(GetLightColors(ambient, diffuse));  
  PostMessage(_window,USER_LIGHT_REQUEST_SHORT,0,(LPARAM)_notifier);
  if (WaitForMessage(msg,1000,USER_LIGHT_REQUEST_SHORT)==ErrTimeout) return ErrTimeout;
  ambient=msg.wParam;
  diffuse=msg.lParam;
  return ErrOK;
}

/*IExternalViewer::ErrorEnum ExternalViewer::ReloadAll()
{
if (_window==NULL) return ErrNotInicialized;
if (!IsWindow(_window)) HANDLE_LOST(IsReady());  
PostMessage(_window,USER_RESET_VIEWER,0,0); 
return ErrOK;
}*/

IExternalViewer::ErrorEnum ExternalViewer::SetMaterial(const char *matName, void *paramFileData, int size)
{
  if (_window==NULL) return ErrNotInicialized;
  if (!IsWindow(_window)) HANDLE_LOST(SetMaterial(matName, paramFileData, size));  
  int matNameLen=strlen(matName)+1;
  int totalMemNeed=matNameLen+size+sizeof(UserReloadMaterialStruct);
  SECURITY_ATTRIBUTES security;
  memset(&security,0,sizeof(security));
  security.nLength=sizeof(security);
  security.bInheritHandle=TRUE;
  HANDLE sharedmem=CreateFileMapping(NULL,&security,PAGE_READWRITE,0,totalMemNeed,NULL);
  if (sharedmem==NULL) return ErrFailed;
  HANDLE mutex=CreateMutex(&security,TRUE,NULL);
  HANDLE dup_mutex,dup_mem;
  if (DuplicateHandle(GetCurrentProcess(),mutex,_viewer,&dup_mutex,0,TRUE,DUPLICATE_SAME_ACCESS) &&
    DuplicateHandle(GetCurrentProcess(),sharedmem,_viewer,&dup_mem,0,TRUE,DUPLICATE_SAME_ACCESS))
  {
    MSG msg;
    PostMessage(_window,USER_RELOAD_MATERIAL,(WPARAM)dup_mutex,(LPARAM)dup_mem);
    UserReloadMaterialStruct *matinfo=reinterpret_cast<UserReloadMaterialStruct *>(::MapViewOfFile(sharedmem,FILE_MAP_WRITE,0,0,0));
    matinfo->notify=_notifier;
    matinfo->matNameOffset=size;
    matinfo->dataOffset=0;
    matinfo->dataSize=size;
    strcpy(matinfo->data+matinfo->matNameOffset,matName);
    memcpy(matinfo->data+matinfo->dataOffset,paramFileData,size);
    UnmapViewOfFile((void *)matinfo);
    ReleaseMutex(mutex);
    CloseHandle(mutex);
    CloseHandle(sharedmem);
    if (WaitForMessage(msg,1000,USER_RELOAD_MATERIAL)==ErrTimeout) return ErrTimeout;
    if (msg.wParam==0) return ErrFailed;
    return ErrOK;
  }
  return ErrFailed;
}

IExternalViewer::ErrorEnum ExternalViewer::SetRTM(const char *rtmName)
{
  if (_window==NULL) return ErrNotInicialized;

  bool out;
  if (!IsWindow(_window)) HANDLE_LOST(SetRTM(rtmName));
  ATOM atm=rtmName?GlobalAddAtom(rtmName):0;
  PostMessage(_window, USER_USE_RTM , 0,(LPARAM)atm);
  if (atm!=0)
  {
    for (int i=0;i<20;i++)
    {
      out=GlobalFindAtom(rtmName)!=atm;
      if (out) break;
      Sleep(100);
    }
  GlobalDeleteAtom(atm);
  }
  else
    out=true;
  return out?ErrOK:ErrFailed;
}

IExternalViewer::ErrorEnum ExternalViewer::SetEnvironment(const EnvInfoStruct &env)
{
  if (_window==NULL) return ErrNotInicialized;
  if (!IsWindow(_window)) HANDLE_LOST(SetEnvironment(env));  

  ErrorEnum stat=ErrOK;
  HANDLE hShare=CreateFileMapping(0,0,PAGE_READWRITE,0,sizeof(EnvInfoStruct),0);
  if (hShare==0) return ErrFailed;
  HANDLE waitE=CreateEvent(0,0,0,0);
  EnvInfoStruct *data=reinterpret_cast<EnvInfoStruct *>(MapViewOfFile(hShare,FILE_MAP_WRITE,0,0,0));
  if (data) 
  {
    *data=env;
    data->version=1;
    DuplicateHandle(GetCurrentProcess(),waitE,_viewer,&data->unblock,0,FALSE,DUPLICATE_SAME_ACCESS);
    UnmapViewOfFile(data);
    HANDLE send;
    DuplicateHandle(GetCurrentProcess(),hShare,_viewer,&send,0,FALSE,DUPLICATE_SAME_ACCESS);
    PostMessage(_window,USER_ENVIRONMENT_INFO,EnvInfoStruct::valSetInfo,(LPARAM)send);    
  }  
  else
    stat=ErrFailed;
  CloseHandle(hShare);
  if (stat==ErrOK)
      if (WaitForSingleObject(waitE,1000)==WAIT_TIMEOUT) stat=ErrTimeout;
  CloseHandle(waitE);
  return stat;
}

IExternalViewer::ErrorEnum ExternalViewer::GetEnvironment(EnvInfoStruct &env) const
{
  ErrorEnum stat=ErrOK;
  if (_window==NULL) return ErrNotInicialized;
  if (!IsWindow(_window)) HANDLE_LOST(GetEnvironment(env));  

  HANDLE hShare=CreateFileMapping(0,0,PAGE_READWRITE,0,sizeof(EnvInfoStruct),0);
  if (hShare==0) return ErrFailed;
  HANDLE waitE=CreateEvent(0,0,0,0);
  EnvInfoStruct *data=reinterpret_cast<EnvInfoStruct *>(MapViewOfFile(hShare,FILE_MAP_WRITE,0,0,0));
  if (data) 
  {
    DuplicateHandle(GetCurrentProcess(),waitE,_viewer,&data->unblock,0,FALSE,DUPLICATE_SAME_ACCESS);
    UnmapViewOfFile(data);
    HANDLE send;
    DuplicateHandle(GetCurrentProcess(),hShare,_viewer,&send,0,FALSE,DUPLICATE_SAME_ACCESS);
    PostMessage(_window,USER_ENVIRONMENT_INFO,EnvInfoStruct::valGetInfo,(LPARAM)send);    
  }
  else stat=ErrFailed;
  if (stat==ErrOK)
    if (WaitForSingleObject(waitE,1000)==WAIT_TIMEOUT) stat=ErrTimeout;
    else
    {  
      data=reinterpret_cast<EnvInfoStruct *>(MapViewOfFile(hShare,FILE_MAP_READ,0,0,0));
      if (data)
      {
        env=*data;
        UnmapViewOfFile(data);
      }
      else stat=ErrFailed;
    }
  CloseHandle(hShare);
  CloseHandle(waitE);
  return stat;
}

IExternalViewer::ErrorEnum ExternalViewer::SetEnvironmentValue(EnvInfoStruct::ValueID value, float f)
{
  if (_window==NULL) return ErrNotInicialized;
  if (!IsWindow(_window)) HANDLE_LOST(SetEnvironmentValue(value,f));  

  PostMessage(_window,USER_ENVIRONMENT_INFO,value,* reinterpret_cast<LPARAM *>(&f));
  return ErrOK;
}

IExternalViewer::ErrorEnum ExternalViewer::SetEnvironmentValue(EnvInfoStruct::ValueID value, long l)
{
  if (_window==NULL) return ErrNotInicialized;
  if (!IsWindow(_window)) HANDLE_LOST(SetEnvironmentValue(value,l));  

  PostMessage(_window,USER_ENVIRONMENT_INFO,value,* reinterpret_cast<LPARAM *>(&l));
  return ErrOK;
}

IExternalViewer::ErrorEnum ExternalViewer::SetViewerControler(const char *controlerName, float value, float *minval, float *maxval)
{
  ErrorEnum stat=ErrOK;
  if (_window==NULL) return ErrNotInicialized;
  if (!IsWindow(_window)) HANDLE_LOST(SetViewerControler(controlerName,value,minval,maxval));

  HANDLE hShare=CreateFileMapping(0,0,PAGE_READWRITE,0,sizeof(UserAnimationControlStruct)+strlen(controlerName),0);
  if (hShare==0) return ErrFailed;
  HANDLE waitE=CreateEvent(0,0,0,0);
  UserAnimationControlStruct *data=reinterpret_cast<UserAnimationControlStruct *>(MapViewOfFile(hShare,FILE_MAP_WRITE,0,0,0));
  if (data) 
  {
    strcpy(data->controlName,controlerName);
    data->value=value;
    DuplicateHandle(GetCurrentProcess(),waitE,_viewer,&data->unblock,0,FALSE,DUPLICATE_SAME_ACCESS);
    UnmapViewOfFile(data);
    HANDLE send;
    DuplicateHandle(GetCurrentProcess(),hShare,_viewer,&send,0,FALSE,DUPLICATE_SAME_ACCESS);
    PostMessage(_window,USER_ANIMATION_CONTROL,0,(LPARAM)send);    
  }
  else
    stat=ErrFailed;
  if (stat==ErrOK && (minval || maxval))
    if (WaitForSingleObject(waitE,10000)==WAIT_TIMEOUT) stat=ErrTimeout;
    else
    {  
      data=reinterpret_cast<UserAnimationControlStruct *>(MapViewOfFile(hShare,FILE_MAP_READ,0,0,0));
      if (data)
      {
        if (minval) *minval=data->minval;
        if (maxval) *maxval=data->maxval;
        UnmapViewOfFile(data);
      }
      else stat=ErrFailed;
    }
  CloseHandle(hShare);
  CloseHandle(waitE);
  return stat;
}

}