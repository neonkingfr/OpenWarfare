// P3DReference.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "MassProcess.h"
#include "../ObjToolsMatLib.h"

class P3DReference:public MassProcess
  {
  BTree<ObjMatLibItem> matlist;
  public:
    bool ProcessFile(const char *filename);
    void ProcessMaterials(const char *root);
  };




int main(int argc, char* argv[])
{
bool recursive=false;
const char *folder;
const char *root;
if (argc<3)
  {
  puts("Parameters: [/s] folder root\n\n/s          process recursive\nfolder      folder to process, ex: X:\\data\nroot        Root folder of project, ex: X:\\");
  return -1;
  }
if (stricmp(argv[1], "/s")==0)
  {
  if (argc<4)
    {
    puts("Missing parameter");
    return -1;
    }
  recursive=true;
  folder=argv[2];
  root=argv[3];
  }
else
  {
  folder=argv[1];
  root=argv[2];
  }
P3DReference mass;
mass.ProcessFolders(folder,"*.p3d",recursive);
mass.ProcessMaterials(root);
return 0;
}


bool P3DReference::ProcessFile(const char *filename)
  {
  LODObject lod;
  if (lod.Load(filename,NULL,NULL)!=0)
    fprintf(stderr,"Error opening: %s\n",filename);
  else
    {
    BTree<ObjMatLibItem> texmat;
    for (int i=0;i<lod.NLevels();i++)
      {
      ObjToolMatLib &matlib=lod.Level(i)->GetTool<ObjToolMatLib>();
      matlib.ReadMatLib(texmat,ObjToolMatLib::ReadTextures);
      matlib.ReadMatLib(texmat,ObjToolMatLib::ReadMaterials);
      matlib.ReadMatLib(matlist,ObjToolMatLib::ReadMaterials);
      }
    if (texmat.Smallest()!=NULL)
      {
      BTreeIterator<ObjMatLibItem> texmatit(texmat);
      texmatit.BeginFrom(*texmat.Smallest());
      ObjMatLibItem *item;
      while (item=texmatit.Next())
        {
        if (item->name.GetLength()!=0)
          printf("P3D: %s --> REF: %s\n",filename,item->name.Data());
        }
      }
    else
        printf("P3D: %s --> ## No Reference ##\n",filename);
    }
  return true;
  }

void P3DReference::ProcessMaterials(const char *root)
  {
  Pathname rootpath(root);
  if (matlist.Smallest()!=NULL)
    {
    BTreeIterator<ObjMatLibItem> matlistit(matlist);
    matlistit.BeginFrom(*matlist.Smallest());
    ObjMatLibItem *item;
    while (item=matlistit.Next())
      {
      if (item->name.GetLength()!=0)
        {
        BTree<RString> files;
        Pathname matpath(item->name.Data(),rootpath);
        ObjToolMatLib::ExploreMaterial(matpath.IsNull()?item->name.Data():matpath.GetFullPath(),files);
        if(files.Size())
          {
          BTreeIterator<RString> filesiter(files);
          filesiter.BeginFrom(*files.Smallest());
          RString *file;
          while (file=filesiter.Next())
            {
            printf("RVMAT: %s --> REF: %s\n",item->name.Data(),file->Data());
            }
          }
        }
      }
    }
  }