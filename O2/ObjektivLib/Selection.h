#ifndef _TRADITIONAL_SELECTION_H_
#define _TRADITIONAL_SELECTION_H_
#pragma once

             \


namespace ObjektivLib {

    class ObjectData;


class Selection
  {
  friend class ObjectData;
  
  private:
    
    const ObjectData * _object;
    
    //byte vert[OMaxPoints];
    //bool norm[OMaxPoints];
    //bool face[OMaxFaces];
    AutoArray<unsigned char> vertSel;
    AutoArray<bool> faceSel;
    
    void ValidatePoints( int nPoints=-1 ) const;
    void ValidateFaces( int nFaces=-1 ) const;
    
  public:
    Selection( const ObjectData *object )
      {
      _object=object;
      Validate();
      }
    Selection( const Selection &src );
    Selection( const Selection &src, ObjectData *object );
    
    void operator += ( const Selection &src );
    void operator -= ( const Selection &src );
    void operator &= ( const Selection &src );
    void operator |= ( const Selection &src );

    bool operator==(const Selection &other) const
    {
      return vertSel==other.vertSel && faceSel==other.faceSel && _object==other._object;
    }

    bool operator!=(const Selection &other) const
    {
      return vertSel!=other.vertSel || faceSel!=other.faceSel || _object!=other._object;
    }

    Selection& operator ~ ();
    
    float PointWeight( int i ) const 
      {return (float)(vertSel[i]*(1.0/255));}
    void SetPointWeight( int i, float w )
      {
      int iw=toInt(w*255);
      if( iw<0 ) iw=0;else if( iw>255 ) iw=255;
      vertSel[i]=iw;
      }
    
    bool PointSelected( int i ) const 
      {return vertSel[i]!=0;}
    void PointSelect( int i, bool sel=true )
      {
      Validate();
      vertSel[i]=sel ? 255 : 0;
      }
    
    bool FaceSelected( int i ) const 
      {return faceSel[i];}
    void FaceSelect( int i, bool sel=true )
      {
      Validate();
      faceSel[i]=sel;
      }
    
    void Clear();
    void ClearFaces();
    void ClearPoints();
    bool IsEmpty() const;
    
    void DeletePoint( int index );
    //void DeleteNormal( int index );
    void DeleteFace( int index );
    
    void Save( std::ostream &f );
    void Load( std::istream &f, int sizeVert, int sizeFace, int sizeNorm );
    
    void PermuteFaces( const int *permutation );
    void PermuteVertices( const int *permutation );
    
    int NPoints() const;
    int NFaces() const;

    ///selects all points used by selected faces
    void SelectPointsFromFaces();
    ///selects each face which all points are selected
    void SelectFacesFromPoints();
    ///selects each face which has at least one point selected
    bool SelectFacesFromPointsTouchMode();
    ///extends selection to whole connected mesh
    void SelectConnectedFaces();

    
    void SetOwner( ObjectData *object )
      {
      _object=object;
      Validate();
      } 
    const ObjectData *GetOwner() const
      {return _object;}
    
    void Validate() const;
    VecT GetCenter() const;
    std::pair<VecT, VecT> GetBoundingBox() const;

    void SelectAll();

    std::pair<int, int>  GetSelectedPointsRange() const;
    std::pair<int, int>  GetSelectedFacesRange() const;
    AutoArray<int> GetSelectedPoints() const;
    AutoArray<int> GetSelectedFaces() const;
    ///Selects points specified by array
    /**@param pts array of point indices to select
       @note Function will not unselect previously selected points. 
       You need to call Clear to handle this
       */
    void SelectPoints(const Array<int> &pts);
    ///Selects faces specified by array
    /**@param pts array of face indices to select
       @note Function will not unselect previously selected faces. 
       You need to call Clear to handle this
       */
    void SelectFaces(const Array<int> &fcs);
  };

  }

/*class RegisteredSelection : public Selection
{
public:

  RegisteredSelection( ObjectData *object ) : Selection(object) 
  {
    if (_object)
      _object->RegisterSelection(this);    
  };

  RegisteredSelection( const Selection &src) : Selection(src)
  {
    if (_object)
      _object->RegisterSelection(this);    
  };

  RegisteredSelection( const Selection &src, ObjectData *object ) : Selection(src, object)
  {
    if (_object)
      _object->RegisterSelection(this);    
  };

  ~RegisteredSelection()

};*/

#endif`