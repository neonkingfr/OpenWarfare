#include "common.hpp"
#include "ObjToolAnimation.h"
#include <Es/Common/mathND.hpp>
#include "El\QStream\qbStream.hpp"
#include "El\QStream\serializeBin.hpp"
#include "El\Math\mathStore.hpp"
#include "El\ParamFile\paramFile.hpp"

namespace ObjektivLib {


void ObjToolAnimation::HalfRateAnimation()
  {
  // reduce rate to half
  // keep first and last frame
  int p,nPositive=0;
  for( p=1; p<_phase.Size()-1; )
    {
    // skip p
    p++;
    // delete animation p+1
    _phase.Delete(p);
    }
  SortAnimations();
  }

static const float MinPositive=-1e-3f;

void ObjToolAnimation::MirrorAnimation()
  {
  // ignore phases with negative time
  int p,nPositive=0;
  for( p=0; p<NAnimations(); p++ )
    {
    if( GetAnimation(p)->GetTime()>=MinPositive ) nPositive++;
    }
  int nPhase=0;
  float invN=1.0f/nPositive;
  for( p=0; p<NAnimations(); p++ )
    {
    AnimationPhase *anim=GetAnimation(p);
    float time=anim->GetTime();
    if( time<MinPositive ) continue;
    float autoTime=nPhase*invN;
    nPhase++;
    anim->SetTime(1-invN-autoTime);
    }
  SortAnimations();
  }

void ObjToolAnimation::SoftenAnimation(float valueX, float valueY, float valueZ, bool actualVsAverage, float timeRange, bool interpolate, bool looped)
    {
    // save current data
    int current=_autoSaveAnimation;
    if (current<0 && actualVsAverage) return;
    float actTime = current>=0 ? _phase[current]->GetTime() : 0;
    UseAnimation(-1);
    // soften animation (all phases)
    int p,i;
    float averageCoef=1.0f/_phase.Size();
    float invTimeRange = ( timeRange>0 && interpolate ) ? 1.0f/timeRange : 0;
    for( i=0; i<NPoints(); i++ ) if( PointSelected(i) )
      {
      float weight=_sel.PointWeight(i);
      Vector3 average=VZero;
      if (actualVsAverage)
        {
        average=(*_phase[current])[i];
        }
      else
        {
        for( p=0; p<_phase.Size(); p++ )
          {
          const AnimationPhase &phase=(*_phase[p]);
          // ignore phases with negative time (special)
          if( phase.GetTime()<MinPositive ) continue;
          average+=phase[i];
          }
        average*=averageCoef;
        }
      float invSoftValueX=(1-valueX)*weight;
      float invSoftValueY=(1-valueY)*weight;
      float invSoftValueZ=(1-valueZ)*weight;
      for( p=0; p<_phase.Size(); p++ )
        {
        AnimationPhase &phase=(*_phase[p]);
        float time = phase.GetTime();
        if (time<MinPositive) continue;
        
        float timeDist=fabs(time-actTime);
        if( looped )
          {
          if( timeDist>0.5 ) timeDist=1-timeDist;
          if( timeDist<0 ) timeDist=0;
          }
        if( timeDist>timeRange ) continue;
        float factor=1-timeDist*invTimeRange;
        float factorX = factor*invSoftValueX;
        float factorY = factor*invSoftValueY;
        float factorZ = factor*invSoftValueZ;
        phase[i][0]=phase[i][0]*(1-factorX)+average[0]*factorX;
        phase[i][1]=phase[i][1]*(1-factorY)+average[1]*factorY;
        phase[i][2]=phase[i][2]*(1-factorZ)+average[2]*factorZ;
        }
      }
    // restore current data
    UseAnimation(current);
    }

static void FindCoordSpace(CoordSpace &coord, const ObjectData *source, const NamedSelection &sel, const AnimationPhase &beg)
    {
    int i,j,k,l;
    // create a list of all selected points
    AutoArray<int> selPoints;
    for( i=0; i<source->NPoints(); i++ ) if( sel.PointSelected(i) )
      {
      selPoints.Add(i);
      }
    int nSel=selPoints.Size();
    //if( nSel<1 ) return MIdentity; // no point
    // find four selected points
    // they should define coordinate space
    // i.e for any (i,j,k) min(begP[i]-begP[j])*(begP[k]-begP[j]) should be maximal
    float minMax=1e10;
    int iMax=-1,jMax=-1,kMax=-1,lMax=-1;
    for( i=0; i<nSel; i++ )
      for( j=0; j<nSel; j++ ) if( j!=i )
        for( k=0; k<nSel; k++ ) if( k!=i && k!=j )
          for( l=0; l<nSel; l++ ) if( l!=i && l!=j && l!=k )
            {
            // all possible combinations are tested here
            Vector3 jMi=(beg[selPoints[j]]-beg[selPoints[i]]).Normalized();
            Vector3 kMi=(beg[selPoints[k]]-beg[selPoints[i]]).Normalized();
            Vector3 lMi=(beg[selPoints[l]]-beg[selPoints[i]]).Normalized();
            //float jMiSize2=jMi.SquareSize();
            //float kMiSize2=kMi.SquareSize();
            //float lMiSize2=lMi.SquareSize();
            float jk=jMi*kMi;
            float jl=jMi*lMi;
            float kl=kMi*lMi;
            float maxCosA=floatMax(jk,floatMax(jl,kl));
            //float minSP=fabs(jk*jl*kl)/(jMiSize2*kMiSize2*lMiSize2);
            Vector3 test=kMi.CrossProduct(lMi).Normalized();
            float testf=1.0f-fabs(test.DotProduct(jMi));
            maxCosA+=testf;
            if( _finite(testf) && minMax>maxCosA && maxCosA>0.0f )
              {
              minMax=maxCosA;
              iMax=selPoints[i],jMax=selPoints[j],kMax=selPoints[k],lMax=selPoints[l];
              // if we have some good solution, we need not to search for the best
              if( minMax<0.1 ) 
                goto Solution;
              }
            }
    if( iMax<0 || minMax>0.8 )
      {
      // not enough points to define whole transformation matrix
      coord.pt[0]=coord.pt[1]=coord.pt[2]=coord.pt[3]=-1;
      ObjData_NoCoordinatePointsMessage(sel.Name());
      }
    else
      {
      Solution:
      coord.pt[0]=iMax;
      coord.pt[1]=jMax;
      coord.pt[2]=kMax;
      coord.pt[3]=lMax;
      }    
    }

static Matrix4P CalcTransform(const CoordSpace &coord, const ObjectData *source, const AnimationPhase &beg, const AnimationPhase &end)
    {
    if( coord.pt[0]<0 )
      {
      // not enough points to define whole transformation matrix
      //return Matrix4(MTranslation,end[coord.pt[0]]-beg[coord.pt[0]]);
      return MIdentity;
      }
    
    Vector3 pBeg[4],pEnd[4];
    int i,j;
    for( i=0; i<4; i++ )
      {
      pBeg[i]=beg[coord.pt[i]];
      pEnd[i]=end[coord.pt[i]];
      }
    // create a lin. equation system matrix
    // we search for a matrix M (3x4) (rows * columns):
    // M*pBeg[i]=pEnd[i]
    Matrix<12,12> left;
    Vector<12> right;
    left.InitZero();
    // M(i,j) corresponds to solution(i*4+j) (column index)
    for( int k=0; k<4; k++ )
      {
      // insert condition below into the system
      // M*pBeg[i]=pEnd[i]
      for( i=0; i<3; i++ )
        {
        int row=k*3+i;
        Assert( row<12 );
        for( j=0; j<3; j++ )
          {
          int col=i*4+j;
          Assert( col<12 );
          left(row,col)=pBeg[k][j];
          }
        left(row,i*4+3)=1;
        right[row]=pEnd[k][i];
        }
      }
    Vector<12> solution;
    // solve lin. equation system
    Matrix<12,12> invLeft;
    bool regular=invLeft.InitInversed(left);
    Assert( regular );
    Multiply(solution,invLeft,right);
    #if _DEBUG
      {
      // verify solution
      Vector<12> verify;
      Multiply(verify,left,solution);
      for( int i=0; i<12; i++ )
        {
        if( fabs(verify[i]-right[i])>1e-5 )
          {
          char line[256];
          sprintf(line,"%10s ","!Solution");
          for( int j=0; j<12; j++ )
            {
            sprintf(line+strlen(line),"%6.3f ",solution[j]);
            }
          Log(line);
          break;
          }
        }
      }
    #endif
    // fill matrix with the solution
    Matrix4P transform(MIdentity);
    Vector3P translate(VZero);
    for( i=0; i<3; i++ )
      {
      for( j=0; j<3; j++ ) transform(i,j)=solution[i*4+j];
      translate[i]=solution[i*4+3];
      }
    transform.SetPosition(translate);
    return transform;
    }


  
struct PointWeight
  {
  int selIndex;
  float weight;
 ClassIsSimple(PointWeight);
  };

//--------------------------------------------------


class PointWeights: public AutoArray<PointWeight>
  {
  public:
    void AddWeight( int selIndex, float weight );
  };

//--------------------------------------------------

void PointWeights::AddWeight( int selIndex, float weight )
  {
  // note: we are sure each selection is added just once
  PointWeight w;
  w.selIndex=selIndex;
  w.weight=weight;
  Add(w);
  }

//--------------------------------------------------

#define MagicLength 8
static const char RTMMagicMDAT[MagicLength+1]="RTM_MDAT"; // Metadata
static const char RTMMagic100[MagicLength+1]="RTM_0100";
static const char RTMMagic101[MagicLength+1]="RTM_0101";
static const char BMTRMagic[4+1]="BMTR";

#define Put(o,v) o.write((const char *)&v,sizeof(v))
#define Get(o,v) o.read((char *)&v,sizeof(v))

static ostream &operator << ( ostream &o, const Vector3P &v )
  {
  o << v.X() << ',' << v.Y() << ',' << v.Z() << ',';
  return o;
  }

//--------------------------------------------------

int ObjToolAnimation::FindCoordSpaces(CoordSpace coord[MAX_NAMED_SEL], const AnimationPhase &src0) const
        {
        int ns;
        // find neutral coordinate spaces
        int nsel=0;
        for( ns=0; ns<MAX_NAMED_SEL; ns++ ) if( _namedSel[ns] )
          {
          if( *_namedSel[ns]->Name()=='*' || *_namedSel[ns]->Name()=='-' || *_namedSel[ns]->Name()=='.' || *_namedSel[ns]->Name()=='!') continue;
          const NamedSelection *srcSel=_namedSel[ns];
          // scan source for some coordinate space points
          FindCoordSpace(coord[ns],this,*srcSel,src0);
          nsel++;
          // weighting information is assigned real time
          }
        return nsel;
        }

//--------------------------------------------------

void ObjToolAnimation::ExportAnimation( const char *file, float zStep, float xStep ) const
  {
  float yStep=0;
  bool binary=true;
  const char *ext=strrchr(file,'.');
  if( ext && !_strcmpi(ext,".txt") ) binary=false;
  ofstream o;
  if( NAnimations()<2 ) return; // nothing to save

  if( binary )
  {
    o.open(file,ios::binary|ios::out);
  }
  else
  {
    o.open(file,ios::out);
  }

  bool someMetadata = _metadata.Size() > 0 || _keyStone.Size() > 0;
  if (someMetadata && binary)
  {
    unsigned char strSize;
    const char *str;
    float time;
    int count;

    o.write(RTMMagicMDAT,MagicLength);

    // Save metadata
    count = _metadata.Size();
    Put(o, count);
    for (int i = 0; i < count; ++i)
    {
      AnimationMetadata *metadata = _metadata[i];
      // name
      strSize = metadata->name.GetLength();
      Put(o, strSize);
      str = metadata->name.Data();
      o.write(str, strSize);
      // value
      strSize = metadata->value.GetLength();
      Put(o, strSize);
      str = metadata->value.Data();
      o.write(str, strSize);
    }

    // Save keystones
    count = _keyStone.Size();
    Put(o, count);
    for (int i = 0; i < count; ++i)
    {
      AnimationKeyStone *keystone = _keyStone[i];
      // time
      time = keystone->time;
      Put(o, time);
      // key
      strSize = keystone->key.GetLength();
      Put(o, strSize);
      if (strSize > 0)
      {
        str = keystone->key.Data();
        o.write(str, strSize);
      }
      // value
      strSize = keystone->value.GetLength();
      Put(o, strSize);
      if (strSize > 0)
      {
        str = keystone->value.Data();
        o.write(str, strSize);
      }
    }
  }

  if( binary )
  {
    o.write(RTMMagic101,MagicLength);
    Put(o,xStep);
    Put(o,yStep);
    Put(o,zStep);
  }
  else
  {
    o << "XStep length=" << xStep << '\n';
    o << "YStep length=" << xStep << '\n';
    o << "ZStep length=" << zStep << '\n';
  }
  const AnimationPhase &src0=*_phase[0];
  // for all named selections of source find points that can be used as coordinate space
  CoordSpace coord[MAX_NAMED_SEL];
  
  int nsel=FindCoordSpaces(coord,src0);
  if( binary )
    {
    int na=NAnimations()-1; // first is ignored (linking)
    Put(o,na);
    Put(o,nsel);
    }
  
  int ns;
  for( ns=0; ns<MAX_NAMED_SEL; ns++ ) if( _namedSel[ns] )
    {
    if( *_namedSel[ns]->Name()=='*' || *_namedSel[ns]->Name()=='-' || *_namedSel[ns]->Name()=='.' || *_namedSel[ns]->Name()=='!') continue;
    const NamedSelection &aSel=*_namedSel[ns];
    if( binary )
      {
      char name[32];
      strcpy_s(name,32,aSel.Name());
      // save all transformed selection names
      o.write(name,sizeof(name));
      }
    else
      {
      o << aSel.Name() << '\n';
      }
    }
  ProgressBar<int> pb(NAnimations());
  for( int p=1; p<NAnimations(); p++ )
    {
    pb.AdvanceNext(1);
    AnimationPhase &srcP=*_phase[p];
    // create transformations for all named selections
    if( binary )
      {
      float time=srcP.GetTime();
      Put(o,time);
      }
    else
      {
      o << "Key "<< srcP.GetTime() << '\n';
      }
    int ns;
    for( ns=0; ns<MAX_NAMED_SEL; ns++ ) if( _namedSel[ns] )
      {
      if( *_namedSel[ns]->Name()=='*' || *_namedSel[ns]->Name()=='-' || *_namedSel[ns]->Name()=='.' || *_namedSel[ns]->Name()=='!') continue;
      const NamedSelection &aSel=*_namedSel[ns];
      const char *nsName=aSel.Name();
      // scan for corresponding named selection in source
      // create transformation matrix from neutral to current position
      // from any three points we can generate transformation matrix
      Matrix4P transform=CalcTransform(coord[ns],this,src0,srcP);
      // save transforms together with selection names
      if( binary )
        {
        char name[32];
        strcpy_s(name,32,nsName);
        o.write(name,sizeof(name));
        Put(o,transform);
        }
      else
        {
        o << nsName << "={";
        // save orientation
        o << transform.DirectionAside();
        o << transform.DirectionUp();
        o << transform.Direction();
        // save position
        o << transform.Position();
        o << "};\n";
        }
      }
    
    }
  }

//--------------------------------------------------

void ObjToolAnimation::ExportAnimationLooped( const char *file ,const char *stepsel) const
  {
  SRef<ObjectData> temp=new ObjectData(*this);
  ObjToolAnimation &temptool=temp->GetTool<ObjToolAnimation>();
  float zStepLength;
  float xStepLength;
  if (stepsel!=NULL && stepsel[0]!=0 && temptool.GetNamedSel(stepsel)!=NULL)
  {
    { // if there is step removed, insert it back
    const char *step=temptool.GetNamedProp("Step");
    float length=0;
    if( step )
      {
      length=(float)atof(step);
      temptool.RemoveZStep(-length);
      }
    }
    { // if there is step removed, insert it back
    const char *xStep=temptool.GetNamedProp("XStep");
    float length=0;
    if( xStep )
      {
      length=(float)atof(xStep);
      temptool.RemoveXStep(-length);
      }
    }
    zStepLength=temptool.StepLength(stepsel);
    xStepLength=temptool.XStepLength(stepsel);
    temptool.RemoveZStep(zStepLength);
    temptool.RemoveXStep(xStepLength);
  }
  else 
  {
    const char *step=temptool.GetNamedProp("XStep");
    if( step ) xStepLength=(float)atof(step);else xStepLength=0;
    step=temptool.GetNamedProp("Step");
    if( step ) zStepLength=(float)atof(step);else zStepLength=0;
  }
  temptool.ExportAnimation(file,zStepLength,xStepLength);
  }

//--------------------------------------------------

void ObjToolAnimation::AutoAnimation( const char *file,const char * animationSkeleton)
  {
  ifstream in;
  in.open(file,ios::binary|ios::in);
  float xStep=0,yStep=0,zStep=0;
  int nAnim=0,nSel=0;
  char magic[MagicLength+1];
  in.read(magic,MagicLength);
  magic[MagicLength]=0;

  if( _phase.Size()>0 && _autoSaveAnimation!=0 )
  {
    // reset current data to neutral position
    UseAnimation(*_phase[0]);
  }
  // delete all animation phases
  if( _phase.Size()>0 ) SetDirty();
  _phase.Clear();
  _autoSaveAnimation=-1;

  // Delete metadata and keystones
  ClearAllMetadata();
  ClearAllKeyStones();

  // Read metadata
  if( !strcmp(RTMMagicMDAT,magic) )
  {
    int count;
    unsigned char strSize;
    char name[256];
    char val[256];
    float time;

    // Read metadata
    Get(in, count);
    for (int i = 0; i < count; ++i)
    {
      Get(in, strSize);
      in.read(name, strSize);
      name[strSize] = 0;
      Get(in, strSize);
      in.read(val, strSize);
      val[strSize] = 0;
      AnimationMetadata metadata;
      metadata.name = name;
      metadata.value = val;
      AddMetadata(metadata);
    }

    // Read keystones
    Get(in, count);
    for (int i = 0; i < count; ++i)
    {
      AnimationKeyStone keystone;
      // time
      Get(in, time);
      keystone.time = time;
      // key
      Get(in, strSize);
      if (strSize > 0)
        in.read(val, strSize);
      val[strSize] = 0;
      keystone.key = val;
      // value
      Get(in, strSize);
      if (strSize > 0)
        in.read(val, strSize);
      val[strSize] = 0;
      keystone.value = val;
      AddKeyStone(keystone);
    }

    // Read next magic tag
    in.read(magic,MagicLength);
    magic[MagicLength]=0;
  }


  if( !strcmp(RTMMagic100,magic) )
    {
    // file format error
    // compare with magic value
    Get(in,zStep); // get basic characeristics
    Get(in,nAnim);
    Get(in,nSel);
    }
  else if( !strcmp(RTMMagic101,magic) )
    {
    Get(in,xStep); // get basic characeristics
    Get(in,yStep);
    Get(in,zStep);
    Get(in,nAnim);
    Get(in,nSel);
    }
  else if( !strncmp(BMTRMagic,magic,4))
  {
    in.close();


    char * animationCfgName = strdup(animationSkeleton);
    char * skeletonName="";
    char * mark = strchr(animationCfgName,'[');
    if (mark)
    {
      *mark = '\0';
      skeletonName = mark+1;
      char * mark = strchr(skeletonName,']');
      if (mark) *mark = '\0';
    }
    ParamFile fileWithSkeleton;
    ParamEntryPtr skeletonBones;
    fileWithSkeleton.Parse(animationCfgName);
    ParamEntryPtr cfgSkeletons =  fileWithSkeleton.FindEntry("CfgSkeletons");
    if (cfgSkeletons.NotNull() && cfgSkeletons->IsClass())
    {
      ParamEntryPtr OFP2_ManSkeleton =  cfgSkeletons->FindEntry(skeletonName);
      if (OFP2_ManSkeleton.NotNull() && OFP2_ManSkeleton->IsClass())
      {
        skeletonBones =  OFP2_ManSkeleton->FindEntry("skeletonBones");
        //isarray
        //todo load inherited
      }
    }


    QIFStreamB inqif;
    inqif.AutoOpen(file);
    if (inqif.fail() || inqif.rest()<=0) return; //RptF("Animation %s not found or empty",file);
    SerializeBinStream f(&inqif);

    if (!f.Version('RTMB')) return;

    const int actVersion = 3;
    const int minVersion = 2;
    if (!f.Version(minVersion, actVersion))
    {
      f.SetError(SerializeBinStream::EBadVersion);
      return;
    }
    if (f.GetVersion() >= 3) f.UseLZOCompression();

    bool _reversed; //! animation should be reversed during loading
    Vector3 _step; // movement per single animation cycle
    int _nPhases; //!< number of keyframes in full animation
    int _preloadCount; //!< animation should be always loaded
    /// what file version (0 = non-streamed, 1 = streamed, 2 = binarized streamed)
    char _version;
    /// compressed arrays are using LZO compression
    bool _lzoCompression;
    /// which bones are used in this animation
    AutoArray<RString> _selections; //originaly RStringB
    /// time of each keyframe (0..1)
    AutoArray<float> _phaseTimes;

    f.Transfer(_reversed);

    f.Transfer(_step[0]);
    f.Transfer(_step[1]);
    f.Transfer(_step[2]);

    char buff[50];
    snprintf(buff,sizeof(buff),"%f",_step[2]);
    SetNamedProp("xStep",buff);
    snprintf(buff,sizeof(buff),"%f",_step[0]);
    SetNamedProp("step",buff);
    snprintf(buff,sizeof(buff),"%f",_step[1]);
    SetNamedProp("yStep",buff);

    f.Transfer(_nPhases);
    f.Transfer(_preloadCount);
    int nAnim;

    {
      _version = f.GetVersion();
      nAnim = f.LoadInt();
      _lzoCompression = f.IsUsingLZOCompression();
    }
    f.TransferBasicArray(_selections); 

    FindArray<int> transformed;
    // load all transformed selection names
    for(int ns=0; ns<_selections.Size(); ns++ )
    {
      int index=FindNamedSelNoCaseSensitive(_selections[ns]);
      if( index>=0 ) transformed.Add(index);
    }
    // create weighting information for all points
    Temp<PointWeights> weights(NPoints());
    for(int ns=0; ns<MAX_NAMED_SEL; ns++ ) if( _namedSel[ns] )
    {
      // use only selections that have corresponding transformed selection
      if( transformed.Find(ns)<0 ) continue;
      const NamedSelection &aSel=*_namedSel[ns];
      for( int i=0; i<NPoints(); i++ ) if( aSel.PointSelected(i) )
      {
        // start with weights taken from selections
        weights[i].AddWeight(ns,aSel.PointWeight(i));
      }
    }
    // normalize weights
    for( int i=0; i<NPoints(); i++ ) if( weights[i].Size()>0 )
    {
      PointWeights &wg=weights[i];
      float sum=0;
      int w;
      for( w=0; w<wg.Size(); w++ ) sum+=wg[w].weight;
      if( sum==0 )
      {
        float avg=1.0f/wg.Size();
        for( w=0; w<wg.Size(); w++ ) wg[w].weight=avg;
      }
      else
      {
        float coef=1/sum;
        for( w=0; w<wg.Size(); w++ ) wg[w].weight*=coef;
      }
    }

    f.TransferBinaryArray(_phaseTimes);

    AutoArray<AutoArray<Matrix4Quat16b>> _buffer;
    if (_nPhases > 0)
    {
      _buffer.Realloc(_nPhases);
      _buffer.Resize(_nPhases);
      for (int i=0; i<_nPhases; i++)
      {
        f.TransferBinaryArray(_buffer[i]);
      }
    }
    AnimationPhase phase0(this);
    phase0.Redefine(this);
    phase0.SetTime(-0.5);
    //RedefineAnimation(phase0);
    AddAnimation(phase0);

    // some index tables for speed up loading
    int _size = _selections.Size();
    int *indexes = new int[_size];
    int *parents = new int[_size];


    for(int i=0; i<_size; i++ )
    {
      int index=FindNamedSelNoCaseSensitive(_selections[i]);
      indexes[i] = index;
      parents[i] = -1;

      if (skeletonBones.NotNull())
      {
	      RStringB parent =_selections[i]; 
	      ASSERT(skeletonBones->GetSize()%2 ==0);
	      for (int t=0; t<skeletonBones->GetSize();t+=2)
	      {
	        if (!stricmp((*skeletonBones)[t].GetValue(),parent))
	        {
	          parents[i] = FindNamedSelNoCaseSensitive((*skeletonBones)[t+1].GetValue());
	          break;
	        }
	      }  
	    }
    }

    for( int p=0; p<_nPhases /*nAnim*/; p++ )
    {
      float time = _phaseTimes[p];
//      Get(in,time); // get basic characeristics
      AnimationPhase phase=phase0;
      // load keyframe time
      phase.SetTime(time);
      // fill animation with neutral position
      //RedefineAnimation(phase);
      int ns,s;
      Matrix4 transforms[MAX_NAMED_SEL];
      bool transformsDone[MAX_NAMED_SEL];
      //for( ns=0; ns<MAX_NAMED_SEL; ns++ ) transforms[ns].InitIdentity();
      for( ns=0; ns<MAX_NAMED_SEL; ns++ ) transforms[ns].SetZero(),transformsDone[ns] = false;
      // load all transformations, check corresponding named selections  
      for( s=0; s<_selections.Size(); s++ )
      {
        Matrix4P transform;     

        transform = _buffer[p][s];

        int index = indexes[s];
        if( index<0 ) continue; // no corresponding selection     

        transforms[index]=(transform);
      }
      bool foundAny = true;
      while (foundAny)
      {
        foundAny = false;
        for( s=0; s<_selections.Size(); s++ )
        {
          int index = indexes[s];
          if( index<0 ) continue; // no corresponding selection

          if (transformsDone[index]) continue;
          int indexParent = parents[s];

          if (transformsDone[indexParent] || indexParent<0)
          {
            if (indexParent>-1) 
            {
              transforms[index] = transforms[indexParent] * transforms[index];
            }
            foundAny = true;
            transformsDone[index] = true;
          };          
        };
      };
      if (_reversed)
      {
        static const Matrix4P _reversedMatrix = Matrix4P(-1.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,0.0f,-1.0f,0.0f);
	      for(int index=0; index<MAX_NAMED_SEL; index++ )
	      {
	          transforms[index] =_reversedMatrix * transforms[index] * _reversedMatrix;
	      }
      }


      // apply transformations to current animation phasis
      for( int i=0; i<NPoints(); i++ )
      {
        const PointWeights &wg=weights[i];
        if( wg.Size()<=0 ) continue; // not animated
        Vector3 &pos=phase[i];
        // do compromises between weighted transformations
        Vector3 res=VZero;
        for( int w=0; w<wg.Size(); w++ )
        {
          const PointWeight &pw=wg[w];
          res+=transforms[pw.selIndex]*pos*pw.weight;
        }
        pos=res;
      }
      AddAnimation(phase);
    }

    if( _phase.Size()>0 )
    {
      _autoSaveAnimation=0;
      UseAnimation(*_phase[_autoSaveAnimation]);
    }
    inqif.close();
    return;
  }
  else
  {
     //AfxMessageBox(IDS_BADIMPORT,MB_OK|MB_ICONSTOP);
     in.close();
     return;
  }
  // for each phasis of source create a new phasis in object
  int ns;
  
  FindArray<int> transformed;
  // load all transformed selection names
  for( ns=0; ns<nSel; ns++ )
    {
    char name[32];
    in.read(name,sizeof(name));
    int index=FindNamedSel(name);
    if( index>=0 ) transformed.Add(index);
    }
  
  // create weighting information for all points
  Temp<PointWeights> weights(NPoints());
  for( ns=0; ns<MAX_NAMED_SEL; ns++ ) if( _namedSel[ns] )
    {
    // use only selections that have corresponding transformed selection
    if( transformed.Find(ns)<0 ) continue;
    const NamedSelection &aSel=*_namedSel[ns];
    for( int i=0; i<NPoints(); i++ ) if( aSel.PointSelected(i) )
      {
      // start with weights taken from selections
      weights[i].AddWeight(ns,aSel.PointWeight(i));
      }
    }
  // normalize weights
  for( int i=0; i<NPoints(); i++ ) if( weights[i].Size()>0 )
    {
    PointWeights &wg=weights[i];
    float sum=0;
    int w;
    for( w=0; w<wg.Size(); w++ ) sum+=wg[w].weight;
    if( sum==0 )
      {
      float avg=1.0f/wg.Size();
      for( w=0; w<wg.Size(); w++ ) wg[w].weight=avg;
      }
    else
      {
      float coef=1/sum;
      for( w=0; w<wg.Size(); w++ ) wg[w].weight*=coef;
      }
    }
  
  AnimationPhase phase0(this);
  phase0.Redefine(this);
  phase0.SetTime(-0.5);
  //RedefineAnimation(phase0);
  AddAnimation(phase0);
  for( int p=0; p<nAnim; p++ )
    {
    float time;
    Get(in,time); // get basic characeristics
    AnimationPhase phase=phase0;
    // load keyframe time
    phase.SetTime(time);
    // fill animation with neutral position
    //RedefineAnimation(phase);
    int ns,s;
    Matrix4 transforms[MAX_NAMED_SEL];
    //for( ns=0; ns<MAX_NAMED_SEL; ns++ ) transforms[ns].InitIdentity();
    for( ns=0; ns<MAX_NAMED_SEL; ns++ ) transforms[ns].SetZero();
    // load all transformations, check corresponding named selections
    for( s=0; s<nSel; s++ )
      {
      char name[32];
      Matrix4P transform;
      in.read(name,sizeof(name));
      Get(in,transform);
      int index=FindNamedSel(name);
      if( index<0 ) continue; // no corresponding selection
      transforms[index]=transform;
      }
    
    // apply transformations to current animation phasis
    for( int i=0; i<NPoints(); i++ )
      {
      const PointWeights &wg=weights[i];
      if( wg.Size()<=0 ) continue; // not animated
      Vector3 &pos=phase[i];
      // do compromises between weighted transformations
      Vector3 res=VZero;
      for( int w=0; w<wg.Size(); w++ )
        {
        const PointWeight &pw=wg[w];
        res+=transforms[pw.selIndex]*pos*pw.weight;
        }
      pos=res;
      }
    AddAnimation(phase);
    }
  char buff[50];
  snprintf(buff,sizeof(buff),"%f",zStep);
  SetNamedProp("step",buff);
 snprintf(buff,sizeof(buff),"%f",xStep);
  SetNamedProp("xStep",buff);
  
  // select first animation phasis
  if( _phase.Size()>0 )
    {
    _autoSaveAnimation=0;
    UseAnimation(*_phase[_autoSaveAnimation]);
    }
  in.close();
  }

//--------------------------------------------------

Vector3 ObjToolAnimation::GetStep(const char *selection) const
  {
  // measure step length
  // there are two methods:
  // 1st - measure half step length(in time 0.5)
  int startIndex=NearestAnimationIndex(0);
  int halfIndex=NearestAnimationIndex(0.5);
  int endIndex=NearestAnimationIndex(1.0);
  if( startIndex<0 ) return VZero;
  if( startIndex==halfIndex && startIndex==endIndex ) return VZero;
  const AnimationPhase *start=GetAnimation(startIndex);
  const AnimationPhase *half=GetAnimation(halfIndex);
  const AnimationPhase *end=GetAnimation(endIndex);
  float startTime=start->GetTime();
  float halfTime=half->GetTime();
  float endTime=end->GetTime();
  // check position of selection
/*  static const char com1[]="teziste";
  static const char com2[]="bricho";*/
  const NamedSelection *comSel=GetNamedSel(selection);
  if( !comSel ) return VZero;
  // calculate com of comSel
  int i;
  Vector3 startCom=VZero,halfCom=VZero,endCom=VZero;
  int nCom=0;
  //comSel->ValidatePoints(-1);
  for( i=0; i<NPoints(); i++ ) if( comSel->PointSelected(i) )
    {
    startCom+=(*start)[i];
    halfCom+=(*half)[i];
    endCom+=(*end)[i];
    nCom++;
    }
  if( nCom<=0 ) return VZero;
  startCom/=(Coord)nCom;
  halfCom/=(Coord)nCom;
  endCom/=(Coord)nCom;
  // assume x,y step is zero
  if( (endTime-startTime)>0.99 )
    {
    // full step available
    return (endCom-startCom)*(1/(endTime-startTime));
    }
  else
    {
    // half step calculation
    return (halfCom-startCom)*(1/(halfTime-startTime));
    }
  }

//--------------------------------------------------

void ObjToolAnimation::RemoveStep( Vector3 stepLength )
  {
  int current=_autoSaveAnimation;
  // reset current data to neutral position
  UseAnimation(-1);
  int i;
  int index=0;
  for( i=0; i<NAnimations(); i++ )
    {
    AnimationPhase *phase=GetAnimation(i);
    phase->Validate();
    if( phase->GetTime()<MinPositive ) continue;
    Vector3 offset=stepLength*phase->GetTime();
    for( int v=0; v<NPoints(); v++ )
      {
      (*phase)[v]-=offset;
      }
    }
  // select first animation phasis
  UseAnimation(current);
  }

//--------------------------------------------------

void ObjToolAnimation::NormalizeCenter( bool normX, bool normZ ,const char *centername)
  {
  // step should be already removed
  // move all animation phases so that COM is at Z-coordinate 0
  int current=_autoSaveAnimation;
  // reset current data to neutral position
  UseAnimation(-1);
  int i;
  int index=0;
  
  int startIndex=NearestAnimationIndex(0);
  if( startIndex<0 ) return;
  const AnimationPhase *start=GetAnimation(startIndex);
/*  static const char com1[]="teziste";
  static const char com2[]="bricho";*/
  const NamedSelection *comSel=GetNamedSel(centername);
  if( !comSel ) return;
  // calculate com of comSel
  Vector3 startCom=VZero;
  int nCom=0;
  for( i=0; i<NPoints(); i++ ) if( comSel->PointSelected(i) )
    {
    startCom+=(*start)[i];
    nCom++;
    }
  if( nCom<=0 ) return;
  startCom/=(Coord)nCom;
  for( i=0; i<NAnimations(); i++ )
    {
    AnimationPhase *phase=GetAnimation(i);
    if( phase->GetTime()<MinPositive ) continue;
    for( int v=0; v<NPoints(); v++ )
      {
      if (normZ) (*phase)[v][2]-=startCom[2];
      if (normX) (*phase)[v][0]-=startCom[0];
      }
    }
  // select first animation phasis
  UseAnimation(current);
  
  }


static void NoCoordinatePointsMessage(const char *selection_name)
  {
  char no_coordinate_points_not_defined=false;
  assert(no_coordinate_points_not_defined);
  }

ObjData_NoCoordinatePointsMessage_Function ObjData_NoCoordinatePointsMessage=NoCoordinatePointsMessage;



void ObjToolAnimation::AutoTimeAnimations( bool last1 )
  {
  // ignore phases with negative time
  int p,nPositive=0;
  for( p=0; p<NAnimations(); p++ )
    {
    if( GetAnimation(p)->GetTime()>=MinPositive ) nPositive++;
    }
  int nPhase=0;
  float invN=last1 ? 1.0f/(nPositive-1) : 1.0f/nPositive;
  for( p=0; p<NAnimations(); p++ )
    {
    AnimationPhase *anim=GetAnimation(p);
    float time=anim->GetTime();
    if( time<MinPositive ) continue;
    float autoTime=nPhase*invN;
    nPhase++;
    anim->SetTime(autoTime);
    }
  }


}