#pragma once
#include "IExternalViewer.h"

namespace ObjektivLib {


class LODObject;
class ObjectData;

struct ExternalViewerConfig
{
  RString selectTex;    //texture for selection
  RString selectMat;    //material for selection
  RString wframeTex;    //texture for wireframe
  RString startupLine;  //startup line for viewer
  RString buldozerName;  //startup name of viewer  
  RString startupFolder; //startup folder
  float lodBias;        //lodBias for multiple LODs
  int freezeAnim;       //animation to freeze
  enum SelEnum {SelectTexture,SelectMaterial,SelectLight,SelectNone,SelectDisplay,SelectWFrame};
  enum CenterEnum {Autocenter, CenterToPin, CenterToSelection};
  enum FlagsEnum 
  {
    HideTextures=1,
    HideMaterials=2,
    HideProxies=4,
    HideHidden=8,
    MultipleLODs=16,
    RemoveUnsupportedTexs=32, ///<Replaces unsupported textures and materials by empty
    DirectMode=128,      ///<Forces no processing, copies model directly
    DontShareViewer=256,     ///<Disables viewer sharing, creates new viewer instance instead
    NoAutoRecreate=512,    ///<Disables recreate, if viewer is lost
    ShowShadow=1024,
    ShowMarkers=2048,
    PopupViewer=4096,
    WireFrame=16384,
    NoModelConfig=32768,
    InvertedSelection=65536

  };
  RString groundObj;    //ground object
  enum GroundPosFlags
  {
    GPDefault=0, GPZero=1,GPCenter=2,GPLowest=3,GPDisabled=4
  };
  GroundPosFlags groundPos;
  float shadowDefLevel;   
  SelEnum selMode;
  CenterEnum centerMode;
  Vector3 pin;

  RString markerObj;

  unsigned long displayFlags;
  unsigned long maxMemory;  ///<set maximum memory to alocate sharing  

  HANDLE consoleInput;    //must be both set, or NULL
  HANDLE consoleOutput;
  HANDLE consoleError;

  HWND hNotifyStatus;     //handle to notify status change
  UINT msgNotifyStatus;   //message assigned to notify state

  DWORD init_timeout;

  ExternalViewerConfig():freezeAnim(-1),selMode(SelectTexture),centerMode(CenterToPin),displayFlags(0),lodBias(0.0f),
  shadowDefLevel(0),consoleInput(0),consoleOutput(0),consoleError(0),hNotifyStatus(0),msgNotifyStatus(0),
  init_timeout(45000) {}
};


class ExternalViewer: public IExternalViewer
{
  HANDLE _sharedlocker;     ///<mutex to exclusive acces to shared memory
  HANDLE _sharedmem;        ///<shared memory handle
  //  HANDLE _dup_sharedlocker;     ///<mutex to exclusive acces to shared memory
  //  HANDLE _dup_sharedmem;        ///<shared memory handle
  HANDLE _viewer;           ///<handle to viewer process
  HANDLE _thread;           ///<handle to updating thread
  DWORD _threadID;          ///<ID of working thread;
  HWND _notifier;           ///<handle to notifier window
  HWND _window;             ///<handle to viewer window

  HANDLE _working;           ///<handle to mutex locked during working thread works

  DWORD _shareMemorySize;     ///<memory size for sharing  
  DWORD _objectSize;

  MSG _msg;
  HANDLE _waitMsg;
  HANDLE _waitMsgAck;

  ExternalViewerConfig *_config;


public:

  ExternalViewer(void);
  ~ExternalViewer(void);

  ExternalViewerConfig *SetViewerSettings(ExternalViewerConfig *cfg) {ExternalViewerConfig *old=_config;_config=cfg;return old;}
  ExternalViewerConfig *GetViewerSettings() {return _config;}

  ErrorEnum Start();
  ErrorEnum Stop();
  ErrorEnum Restart();
  ErrorEnum Attach();
  ErrorEnum Detach();        
  ErrorEnum FinalDetach();

  ErrorEnum Update(LODObject *lod, const char *p3dname=NULL, DWORD waittime=0);
  ErrorEnum StartScript(const char *scriptname);
  ErrorEnum IsReady(); ///<returns status as errorcode ErrOK = Ready, ErrBusy = Running Busy, ErrNotInicialized = offline

  ErrorEnum SwitchToViewer();
  ErrorEnum HasFocus();
  ErrorEnum SetWeather(float overcast, float fog);
  ErrorEnum GetWeather(float &overcast, float &fog);
  ErrorEnum RedrawViewer();
  ErrorEnum TimeOfDay(int advance_insec, DWORD *cur_insec);
  ErrorEnum GetLightColors(COLORREF &ambient, COLORREF &diffuse);
  ErrorEnum SetMaterial(const char *matName, void *paramFileData, int size);
  ErrorEnum SetRTM(const char *rtmName);
  ErrorEnum SetEnvironment(const EnvInfoStruct &env);
  ErrorEnum GetEnvironment(EnvInfoStruct &env) const;
  ErrorEnum SetEnvironmentValue(EnvInfoStruct::ValueID valueId, float f);
  ErrorEnum SetEnvironmentValue(EnvInfoStruct::ValueID valueId, long l);
  ErrorEnum SetViewerControler(const char *controlerName, float value, float *minval=0, float *maxval=0);

  HWND GetWindow() {return _window;}

protected:
  void RunWorkingThread();
  void WorkingThread();
  friend unsigned int __stdcall StaticWorkingThread(void *ptr);     
  friend BOOL CALLBACK ViewerWindowSeeker(HWND hwnd,  LPARAM lParam);
  ErrorEnum CommonInit();
  void CleanUp();

  // Worker functions  
  void PrepareForUpdate(const LODObject &lod, LODObject &output);
  bool TryToSaveObject(const LODObject &object, const char *modelname);
  void SaveObject(const LODObject &lod, const char *modelname);
  bool OnUpdateObjectMessage(const LODObject &lod,const char *modelname);
  friend LRESULT DummyWinProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
  ErrorEnum HandleLostOp()
  {
    LogF("(ExtViewer) Viewer window has lost, trying reconnect");
    if (_config->displayFlags & (_config->NoAutoRecreate | _config->DontShareViewer)) return ErrLost;
    Stop();
    if (Attach()!=ErrOK) return ErrAttachFailed;
    return ErrOK;
  }
  ErrorEnum HandleLostOp() const
  {
    return const_cast<ExternalViewer *>(this)->HandleLostOp();
  }

  ErrorEnum WaitForMessage(MSG &msg, DWORD timeout , UINT message=0); 


public: //Overrides
  virtual bool AfterModelPrepared(const ObjectData &source, ObjectData &target)
  {return true;}

};

}