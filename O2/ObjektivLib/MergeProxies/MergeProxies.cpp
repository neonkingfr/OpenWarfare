// MergeProxies.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ProxyMerge.h"
#include "ProxiesFromTxt.hpp"
#include "..\ObjToolProxy.h"
#include "..\ObjToolJoinProxy.h"
#include "..\ObjToolCheck.h"
#include "..\BiFXTImport.h"

typedef AutoArray<AutoArray<RStringB> > StringArray2D;


bool FillMergeListAll(const Pathname& src, StringArray2D& mergeList)
{
  LODObject proxyObject;

  if (proxyObject.Load(Pathname(src),NULL,NULL) != 0) 
    return false;

  // colect proxies
  ObjectData * proxyLod = proxyObject.Active();

  int nProxies = proxyLod->GetTool<ObjToolProxy>().EnumProxies(NULL);
  NamedSelection ** list = new NamedSelection*[nProxies];
  proxyLod->GetTool<ObjToolProxy>().EnumProxies(list);

  mergeList.Append();
  for(int i = 0; i < nProxies; i++)
  {
    NamedSelection* sel = list[i];
    mergeList[0].Add(sel->Name());
  }
  return true;
}



bool FillMergeListStatic(StringArray2D& mergeList)
{
  const char * mergeList0[] =
  {"proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x31994_ID12x26853_ID15x31994\\F108_V56_ID2x31994_ID12x26853_ID15x31994.80",
  "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x31998_ID12x26855_ID15x31998\\F108_V56_ID2x31998_ID12x26855_ID15x31998.60",
  "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x33486_ID12x27611_ID15x33486\\F108_V56_ID2x33486_ID12x27611_ID15x33486.62",
  "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F30_V18_ID2x8580_ID12x11258_ID15x8580\\F30_V18_ID2x8580_ID12x11258_ID15x8580.79",
  "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F32_V18_ID2x7542_ID12x14008_ID15x7542\\F32_V18_ID2x7542_ID12x14008_ID15x7542.59",
  "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F40_V22_ID2x8055_ID12x14708_ID15x8055\\F40_V22_ID2x8055_ID12x14708_ID15x8055.55",
  "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F40_V22_ID2x8055_ID12x14708_ID15x8055\\F40_V22_ID2x8055_ID12x14708_ID15x8055.65",
  "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F42_V24_ID2x9647_ID12x14448_ID15x9647\\F42_V24_ID2x9647_ID12x14448_ID15x9647.78",
  ""
  };

  const char * mergeList1[] = {
    "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x24147_ID2x24147_ID12x19644\\F100_V52_ID1x24147_ID2x24147_ID12x19644.144",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x31990_ID12x26851_ID15x31990\\F108_V56_ID2x31990_ID12x26851_ID15x31990.03",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x31997_ID12x26855_ID15x31997\\F108_V56_ID2x31997_ID12x26855_ID15x31997.68",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F126_V65_ID1x52272_ID2x56413_ID12x29055_ID15x4141\\F126_V65_ID1x52272_ID2x56413_ID12x29055_ID15x4141.124",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F130_V67_ID1x51205_ID2x55348_ID12x29682_ID15x4143\\F130_V67_ID1x51205_ID2x55348_ID12x29682_ID15x4143.73",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F30_V18_ID2x8578_ID12x11258_ID15x8578\\F30_V18_ID2x8578_ID12x11258_ID15x8578.67",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F44_V24_ID2x10932_ID12x15028_ID15x10932\\F44_V24_ID2x10932_ID12x15028_ID15x10932.74",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F96_V50_ID2x29615_ID12x24085_ID15x29615\\F96_V50_ID2x29615_ID12x24085_ID15x29615.70",
      ""
  };

  const char * mergeList2[] = {
    "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F102_V53_ID1x30345_ID2x30345_ID12x23200\\F102_V53_ID1x30345_ID2x30345_ID12x23200.136",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F106_V55_ID1x17070_ID2x28885_ID11x13623_ID12x26902\\F106_V55_ID1x17070_ID2x28885_ID11x13623_ID12x26902.10",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID1x31994_ID2x31994_ID12x26853\\F108_V56_ID1x31994_ID2x31994_ID12x26853.02",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID1x31994_ID2x31994_ID12x26853\\F108_V56_ID1x31994_ID2x31994_ID12x26853.29",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F110_V57_ID1x19211_ID2x32441_ID11x15265_ID12x22993\\F110_V57_ID1x19211_ID2x32441_ID11x15265_ID12x22993.142",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F114_V59_ID1x32845_ID2x46692_ID11x15337_ID12x30281_ID15x2499\\F114_V59_ID1x32845_ID2x46692_ID11x15337_ID12x30281_ID15x2499.37",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F122_V63_ID1x1423_ID2x32586_ID11x41491_ID12x28357\\F122_V63_ID1x1423_ID2x32586_ID11x41491_ID12x28357.31",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F122_V63_ID1x42948_ID2x32586_ID12x28359\\F122_V63_ID1x42948_ID2x32586_ID12x28359.39",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F40_V22_ID2x10935_ID12x15028_ID15x10935\\F40_V22_ID2x10935_ID12x15028_ID15x10935.35",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F88_V46_ID1x46411_ID2x57580_ID11x19518_ID12x24615\\F88_V46_ID1x46411_ID2x57580_ID11x19518_ID12x24615.120",
      ""
  };

  const char * mergeList3[] = {
    "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x24147_ID2x24147_ID12x19644\\F100_V52_ID1x24147_ID2x24147_ID12x19644.129",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x24147_ID2x24147_ID12x19644\\F100_V52_ID1x24147_ID2x24147_ID12x19644.132",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID1x16909_ID2x28724_ID11x13622_ID12x23330\\F108_V56_ID1x16909_ID2x28724_ID11x13622_ID12x23330.140",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID1x30570_ID2x31994_ID11x1423_ID12x26853\\F108_V56_ID1x30570_ID2x31994_ID11x1423_ID12x26853.05",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID1x31994_ID2x31994_ID12x26853\\F108_V56_ID1x31994_ID2x31994_ID12x26853.30",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F112_V58_ID1x18303_ID2x31533_ID11x15265_ID12x27190\\F112_V58_ID1x18303_ID2x31533_ID11x15265_ID12x27190.07",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F122_V63_ID1x1423_ID2x32586_ID11x41491_ID12x28356\\F122_V63_ID1x1423_ID2x32586_ID11x41491_ID12x28356.33",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F122_V63_ID1x42948_ID2x32586_ID12x28359\\F122_V63_ID1x42948_ID2x32586_ID12x28359.40",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F154_V79_ID1x47403_ID2x65245_ID11x15328_ID12x38938_ID15x6678\\F154_V79_ID1x47403_ID2x65245_ID11x15328_ID12x38938_ID15x6678.56",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F92_V48_ID1x34584_ID2x45936_ID11x19532_ID12x21409\\F92_V48_ID1x34584_ID2x45936_ID11x19532_ID12x21409.122",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F98_V51_ID1x31413_ID2x31413_ID12x22574\\F98_V51_ID1x31413_ID2x31413_ID12x22574.139",
      ""
  };

  const char * mergeList4[] = {
    "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x22723_ID2x24147_ID11x1423_ID12x19644\\F100_V52_ID1x22723_ID2x24147_ID11x1423_ID12x19644.128",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x24146_ID2x24146_ID12x19644\\F100_V52_ID1x24146_ID2x24146_ID12x19644.152",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x24147_ID2x24147_ID12x19644\\F100_V52_ID1x24147_ID2x24147_ID12x19644.145",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x29982_ID2x29982_ID12x18549\\F100_V52_ID1x29982_ID2x29982_ID12x18549.51",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F20_V14_ID2x4142_ID12x4157\\F20_V14_ID2x4142_ID12x4157.107",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F30_V18_ID2x4142_ID11x2202_ID12x4157\\F30_V18_ID2x4142_ID11x2202_ID12x4157.103",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F36_V20_ID1x7397_ID2x7397_ID12x7333\\F36_V20_ID1x7397_ID2x7397_ID12x7333.47",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F58_V32_ID2x29394_ID12x14896\\F58_V32_ID2x29394_ID12x14896.106",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F60_V32_ID2x52728_ID12x22818\\F60_V32_ID2x52728_ID12x22818.25",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F76_V41_ID2x60127_ID12x17118\\F76_V41_ID2x60127_ID12x17118.22",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F90_V48_ID2x29394_ID11x14447_ID12x13846\\F90_V48_ID2x29394_ID11x14447_ID12x13846.102",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F96_V50_ID1x52111_ID2x52111_ID12x21978\\F96_V50_ID1x52111_ID2x52111_ID12x21978.117",
      ""
  };

  const char * mergeList5[] = {
    "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x24147_ID2x24147_ID12x19644\\F100_V52_ID1x24147_ID2x24147_ID12x19644.148",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x24147_ID2x24147_ID12x19644\\F100_V52_ID1x24147_ID2x24147_ID12x19644.149",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x30092_ID2x30092_ID12x18619\\F100_V52_ID1x30092_ID2x30092_ID12x18619.50",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F28_V16_ID1x4916_ID2x4916_ID12x5956\\F28_V16_ID1x4916_ID2x4916_ID12x5956.44",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F28_V16_ID2x14654_ID12x7161\\F28_V16_ID2x14654_ID12x7161.95",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F30_V18_ID2x4097_ID12x6564\\F30_V18_ID2x4097_ID12x6564.109",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F58_V31_ID2x50149_ID12x17437\\F58_V31_ID2x50149_ID12x17437.99",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F84_V44_ID2x37568_ID12x27990\\F84_V44_ID2x37568_ID12x27990.113",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F84_V44_ID2x37569_ID12x27990\\F84_V44_ID2x37569_ID12x27990.110",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F88_V46_ID2x55083_ID12x23731\\F88_V46_ID2x55083_ID12x23731.90",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F96_V50_ID1x52111_ID2x52111_ID12x21978\\F96_V50_ID1x52111_ID2x52111_ID12x21978.119",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x29982_ID2x29982_ID12x18549\\F100_V52_ID1x29982_ID2x29982_ID12x18549.53",
      ""
  };


  const char * mergeList6[] = {
    "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x22723_ID2x24147_ID11x1423_ID12x19644\\F100_V52_ID1x22723_ID2x24147_ID11x1423_ID12x19644.133",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F102_V53_ID1x30344_ID2x30344_ID12x23200\\F102_V53_ID1x30344_ID2x30344_ID12x23200.137",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F106_V55_ID1x17070_ID2x28885_ID11x13623_ID12x26902\\F106_V55_ID1x17070_ID2x28885_ID11x13623_ID12x26902.09",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID1x31994_ID2x31994_ID12x26853\\F108_V56_ID1x31994_ID2x31994_ID12x26853.04",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID1x31994_ID2x31994_ID12x26853\\F108_V56_ID1x31994_ID2x31994_ID12x26853.27",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F110_V57_ID1x19211_ID2x32441_ID11x15265_ID12x22993\\F110_V57_ID1x19211_ID2x32441_ID11x15265_ID12x22993.143",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F114_V59_ID1x32844_ID2x46692_ID11x15337_ID12x30281_ID15x2499\\F114_V59_ID1x32844_ID2x46692_ID11x15337_ID12x30281_ID15x2499.38",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F122_V63_ID1x1423_ID2x32586_ID11x41491_ID12x28357\\F122_V63_ID1x1423_ID2x32586_ID11x41491_ID12x28357.32",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F122_V63_ID1x42948_ID2x32586_ID12x28359\\F122_V63_ID1x42948_ID2x32586_ID12x28359.42",
      ""
  };

  const char * mergeList7[] = {
    "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x24147_ID2x24147_ID12x19644\\F100_V52_ID1x24147_ID2x24147_ID12x19644.130",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x24147_ID2x24147_ID12x19644\\F100_V52_ID1x24147_ID2x24147_ID12x19644.150",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x24147_ID2x24147_ID12x19644\\F100_V52_ID1x24147_ID2x24147_ID12x19644.153",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x31253_ID2x31253_ID12x19002\\F100_V52_ID1x31253_ID2x31253_ID12x19002.135",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F36_V20_ID1x7397_ID2x7397_ID12x7333\\F36_V20_ID1x7397_ID2x7397_ID12x7333.48",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F88_V46_ID1x46411_ID2x57580_ID11x19518_ID12x24615\\F88_V46_ID1x46411_ID2x57580_ID11x19518_ID12x24615.121",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F96_V50_ID1x52111_ID2x52111_ID12x21978\\F96_V50_ID1x52111_ID2x52111_ID12x21978.116",
      ""
  };

  const char * mergeList8[] = {
    "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x31994_ID12x26853_ID15x31994\\F108_V56_ID2x31994_ID12x26853_ID15x31994.81",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x31997_ID12x26855_ID15x31997\\F108_V56_ID2x31997_ID12x26855_ID15x31997.69",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F30_V18_ID2x8578_ID12x11258_ID15x8578\\F30_V18_ID2x8578_ID12x11258_ID15x8578.66",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F30_V18_ID2x8580_ID12x11258_ID15x8580\\F30_V18_ID2x8580_ID12x11258_ID15x8580.76",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F40_V22_ID2x10935_ID12x15028_ID15x10935\\F40_V22_ID2x10935_ID12x15028_ID15x10935.36",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F42_V24_ID2x9647_ID12x14448_ID15x9647\\F42_V24_ID2x9647_ID12x14448_ID15x9647.77",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F44_V24_ID2x10932_ID12x15028_ID15x10932\\F44_V24_ID2x10932_ID12x15028_ID15x10932.75",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F96_V50_ID2x29615_ID12x24085_ID15x29615\\F96_V50_ID2x29615_ID12x24085_ID15x29615.71",
      ""
  };

  const char * mergeList9[] = {
    "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x24147_ID2x24147_ID12x19644\\F100_V52_ID1x24147_ID2x24147_ID12x19644.126",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x24147_ID2x24147_ID12x19644\\F100_V52_ID1x24147_ID2x24147_ID12x19644.147",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID1x16909_ID2x28724_ID11x13622_ID12x23330\\F108_V56_ID1x16909_ID2x28724_ID11x13622_ID12x23330.141",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID1x30570_ID2x31994_ID11x1423_ID12x26853\\F108_V56_ID1x30570_ID2x31994_ID11x1423_ID12x26853.06",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID1x31994_ID2x31994_ID12x26853\\F108_V56_ID1x31994_ID2x31994_ID12x26853.28",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F112_V58_ID1x18304_ID2x31533_ID11x15265_ID12x27190\\F112_V58_ID1x18304_ID2x31533_ID11x15265_ID12x27190.08",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F122_V63_ID1x1423_ID2x32586_ID11x41491_ID12x28356\\F122_V63_ID1x1423_ID2x32586_ID11x41491_ID12x28356.34",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F122_V63_ID1x42948_ID2x32586_ID12x28359\\F122_V63_ID1x42948_ID2x32586_ID12x28359.41",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F154_V79_ID1x47404_ID2x65244_ID11x15326_ID12x38937_ID15x6678\\F154_V79_ID1x47404_ID2x65244_ID11x15326_ID12x38937_ID15x6678.57",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F40_V22_ID2x8055_ID12x14708_ID15x8055\\F40_V22_ID2x8055_ID12x14708_ID15x8055.54",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F92_V48_ID1x34584_ID2x45937_ID11x19532_ID12x21409\\F92_V48_ID1x34584_ID2x45937_ID11x19532_ID12x21409.123",
      ""
  };

  const char * mergeList10[] = {
    "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x24147_ID2x24147_ID12x19644\\F100_V52_ID1x24147_ID2x24147_ID12x19644.131",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x24147_ID2x24147_ID12x19644\\F100_V52_ID1x24147_ID2x24147_ID12x19644.151",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x31994_ID12x26853_ID15x31994\\F108_V56_ID2x31994_ID12x26853_ID15x31994.01",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x31994_ID12x26853_ID15x31994\\F108_V56_ID2x31994_ID12x26853_ID15x31994.61",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x33494_ID12x27612_ID15x33494\\F108_V56_ID2x33494_ID12x27612_ID15x33494.63",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F126_V65_ID1x52272_ID2x56413_ID12x29055_ID15x4141\\F126_V65_ID1x52272_ID2x56413_ID12x29055_ID15x4141.125",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F130_V67_ID1x51205_ID2x55348_ID12x29682_ID15x4143\\F130_V67_ID1x51205_ID2x55348_ID12x29682_ID15x4143.72",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F32_V18_ID2x7542_ID12x14008_ID15x7542\\F32_V18_ID2x7542_ID12x14008_ID15x7542.58",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F40_V22_ID2x8055_ID12x14708_ID15x8055\\F40_V22_ID2x8055_ID12x14708_ID15x8055.64",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F98_V51_ID1x31413_ID2x31413_ID12x22574\\F98_V51_ID1x31413_ID2x31413_ID12x22574.138",
      ""
  };

  const char * mergeList11[] = {
    "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x24147_ID2x24147_ID12x19644\\F100_V52_ID1x24147_ID2x24147_ID12x19644.127",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x24147_ID2x24147_ID12x19644\\F100_V52_ID1x24147_ID2x24147_ID12x19644.146",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x30092_ID2x30092_ID12x18619\\F100_V52_ID1x30092_ID2x30092_ID12x18619.52",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID1x31252_ID2x31252_ID12x19002\\F100_V52_ID1x31252_ID2x31252_ID12x19002.134",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F104_V54_ID2x56894_ID12x26506\\F104_V54_ID2x56894_ID12x26506.21",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F28_V16_ID1x4916_ID2x4916_ID12x5956\\F28_V16_ID1x4916_ID2x4916_ID12x5956.46",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F30_V18_ID2x4097_ID12x6564\\F30_V18_ID2x4097_ID12x6564.108",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F56_V30_ID2x49860_ID12x20775\\F56_V30_ID2x49860_ID12x20775.26",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F84_V44_ID2x37569_ID12x27990\\F84_V44_ID2x37569_ID12x27990.111",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F84_V44_ID2x37569_ID12x27990\\F84_V44_ID2x37569_ID12x27990.112",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F96_V50_ID1x52111_ID2x52111_ID12x21979\\F96_V50_ID1x52111_ID2x52111_ID12x21979.114",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F96_V50_ID1x52111_ID2x52111_ID12x21979\\F96_V50_ID1x52111_ID2x52111_ID12x21979.118",
      ""
  };

  const char * mergeList12[] = {
    "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F102_V53_ID2x61300_ID12x23434\\F102_V53_ID2x61300_ID12x23434.18",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x63988_ID12x26853\\F108_V56_ID2x63988_ID12x26853.16",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x63988_ID12x26853\\F108_V56_ID2x63988_ID12x26853.88",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x63989_ID12x26853\\F108_V56_ID2x63989_ID12x26853.86",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F38_V22_ID2x8282_ID12x8508\\F38_V22_ID2x8282_ID12x8508.84",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F38_V22_ID2x8282_ID12x8508\\F38_V22_ID2x8282_ID12x8508.85",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F62_V34_ID2x44168_ID12x11136\\F62_V34_ID2x44168_ID12x11136.91",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F96_V50_ID2x63368_ID12x23944\\F96_V50_ID2x63368_ID12x23944.20",
      ""
  };

  const char * mergeList13[] = {	
    "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x63988_ID12x26853\\F108_V56_ID2x63988_ID12x26853.13",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F36_V20_ID2x20684_ID12x8414\\F36_V20_ID2x20684_ID12x8414.96",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F58_V31_ID2x54781_ID12x18780\\F58_V31_ID2x54781_ID12x18780.23",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F60_V32_ID2x52728_ID12x22818\\F60_V32_ID2x52728_ID12x22818.14",
      ""
  };

  const char * mergeList14[] = {
    "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F20_V14_ID2x4142_ID12x4157\\F20_V14_ID2x4142_ID12x4157.105",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F30_V18_ID2x4142_ID11x2202_ID12x4157\\F30_V18_ID2x4142_ID11x2202_ID12x4157.101",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F58_V32_ID2x29394_ID12x14896\\F58_V32_ID2x29394_ID12x14896.104",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F62_V34_ID2x44167_ID12x11136\\F62_V34_ID2x44167_ID12x11136.92",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F72_V39_ID2x62485_ID12x14290\\F72_V39_ID2x62485_ID12x14290.93",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F90_V48_ID2x29394_ID11x14447_ID12x13846\\F90_V48_ID2x29394_ID11x14447_ID12x13846.100",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F96_V50_ID1x52111_ID2x52111_ID12x21979\\F96_V50_ID1x52111_ID2x52111_ID12x21979.115",
      ""
  };

  const char * mergeList15[] = {
    "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F100_V52_ID2x61011_ID12x26772\\F100_V52_ID2x61011_ID12x26772.17",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x63988_ID12x26853\\F108_V56_ID2x63988_ID12x26853.87",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x63988_ID12x26853\\F108_V56_ID2x63988_ID12x26853.89",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x63989_ID12x26853\\F108_V56_ID2x63989_ID12x26853.12",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F38_V22_ID2x8282_ID12x8508\\F38_V22_ID2x8282_ID12x8508.82",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F38_V22_ID2x8282_ID12x8508\\F38_V22_ID2x8282_ID12x8508.83",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F60_V32_ID2x52728_ID12x22818\\F60_V32_ID2x52728_ID12x22818.11",
      ""
  };

  const char * mergeList16[] = {
    "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F108_V56_ID2x63988_ID12x26853\\F108_V56_ID2x63988_ID12x26853.15",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F28_V16_ID2x14654_ID12x7161\\F28_V16_ID2x14654_ID12x7161.94",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F36_V20_ID2x20684_ID12x8414\\F36_V20_ID2x20684_ID12x8414.97",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F58_V31_ID2x54781_ID12x18780\\F58_V31_ID2x54781_ID12x18780.98",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F62_V33_ID2x53017_ID12x19480\\F62_V33_ID2x53017_ID12x19480.24",
      "proxy:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\WallsTemp\\F86_V45_ID2x59489_ID12x20658\\F86_V45_ID2x59489_ID12x20658.19",
      ""
  };

  const char ** mergeListTemp[] = {mergeList0,mergeList1,mergeList2,mergeList3,mergeList4,mergeList5,
    mergeList6,mergeList7,mergeList8,mergeList9,mergeList10,mergeList11,mergeList12,mergeList13,
    mergeList14,mergeList15,mergeList16};  

  for (int i=0;i<lenof(mergeListTemp);i++)
  {    
    int j=0;
    AutoArray<RStringB>& list = mergeList.Append();
    while (strlen(mergeListTemp[i][j]) >0)
    {        
      list.Add(RStringB(mergeListTemp[i][j]));
      j++;
    }
  }

  return true;
}

bool FillMergeListAutomatic(const Pathname& src, StringArray2D& mergeList)
{
 // Divider divider;
 // divider.LoadProxyObject(src);
  //return divider(mergeList);
  return false;
}

bool FillMergeListAccordingArg(int argc, _TCHAR* argv[], const Pathname& src, StringArray2D& mergeList)
{
  //return FillMergeListStatic(mergeList);
  //return FillMergeListAutomatic(src, mergeList);
  return FillMergeListAll(src, mergeList);
}


int _tmain(int argc, _TCHAR* argv[])
{
#define FROM_TXT 0
#if FROM_TXT  
  LODObject in;
  in.ChangeResolution(0,1);
  CBiFXTImport::Import("P:\\ofp2\\Structures\\Buildings\\Europe\\Houses\\Brickhouses\\Data\\WallsTemp\\bohemia_02.fxt", &in);
  in.Save(Pathname("P:\\ofp2\\Structures\\Buildings\\Europe\\Houses\\Brickhouses\\BrickB06\\BrickB06_all.p3d"), 
    9999,false,NULL,NULL);  

  return 0;
#endif

#define ALL_TO_1 0
#if ALL_TO_1  
  LODObject in;
  in.Load(Pathname("P:\\ofp2\\Structures\\Buildings\\Europe\\Houses\\Brickhouses\\BrickB06\\BrickB06_all.p3d"),NULL,NULL);
  ObjectData * proxyLod = in.Active();

  int nProxies = proxyLod->GetTool<ObjToolProxy>().EnumProxies(NULL);
  NamedSelection ** list = new NamedSelection*[nProxies];
  proxyLod->GetTool<ObjToolProxy>().EnumProxies(list);

  Matrix4 resTrans;
  proxyLod->GetTool<ObjToolJoinProxy>().CreateModelFromProxies(Pathname("P:\\ofp2\\Structures\\Buildings\\Europe\\Houses\\Brickhouses\\BrickB06\\BrickB06_tmp.p3d"), 
    list, nProxies, resTrans, false);

 return 0;
#endif

#define TO_PIECES 1
#if TO_PIECES
 LODObject in;
 in.Load(Pathname("P:\\ofp2\\Structures\\Buildings\\Europe\\Houses\\Brickhouses\\BrickB06\\BrickB06_tmp.p3d"),NULL,NULL);

 int subPartsLoc = in.FindLevelExact(LOD_SUB_PARTS);
 ObjectData * proxyLod = in.Level(subPartsLoc);

 proxyLod->GetTool<ObjToolJoinProxy>().ReplaceGroupsByProxies("-sbp.","P:\\"); 

 in.Save(Pathname("P:\\ofp2\\Structures\\Buildings\\Europe\\Houses\\Brickhouses\\BrickB06\\BrickB06_cnv.p3d"),
   9999,false,NULL,NULL);  

 return 0;

#endif
#define MAKE_SEL 0
#if MAKE_SEL
 LODObject in;
 in.Load(Pathname("P:\\ofp2\\Structures\\Buildings\\Europe\\Houses\\Brickhouses\\BrickB06\\BrickB06.p3d"),NULL,NULL);

 int subPartsLoc = in.FindLevelExact(LOD_SUB_PARTS);
 ObjectData * proxyLod = in.Level(subPartsLoc);

 // Select All
 Selection sel(proxyLod);
 for(int i = 0; i < proxyLod->NFaces(); i++)
   sel.FaceSelect(i);

 sel.SelectPointsFromFaces();

 proxyLod->UseSelection(&sel);

 /// create selections
 proxyLod->GetTool<ObjToolJoinProxy>().CreateProxiesGroup("-sbp.\\ofp2\\Structures\\Buildings\\Europe\\Houses\\Brickhouses\\BrickB06\\merged",5,17);

 in.Save(Pathname("P:\\ofp2\\Structures\\Buildings\\Europe\\Houses\\Brickhouses\\BrickB06\\BrickB06_tmp.p3d"),
   9999,false,NULL,NULL); 

 return 0;

#endif

/*

  ProxyMerge mrg("P:\\");
  Pathname src="P:\\ofp2\\Structures\\Buildings\\Europe\\Houses\\Brickhouses\\BrickB06\\BrickB06_tmp.p3d";
  Pathname trg=src;  

  StringArray2D mergeList;

  if (!FillMergeListAccordingArg(argc, argv, src, mergeList))
    return 0;

  trg.SetFiletitle(RString(src.GetTitle(),"_cnv"));
  if (mrg.LoadProxyObject(src)==false)
  {
    puts("Unable open proxy object");return -1;
  }

  for (int i=0;i<mergeList.Size();i++)
  {
    char mergedName[1024];
    sprintf(mergedName,"p:\\OFP2\\Structures\\Buildings\\Europe\\Houses\\BrickHouses\\Data\\Merged\\bohemia_01\\merged%d.p3d", i);
    mrg.SetMergedName(mergedName);
    
    AutoArray<RStringB>& list = mergeList[i];
    for (int j=0;j<list.Size();j++)    
    {        
      mrg.MergeProxy(list[j]);      
    }

    //mrg.CombineParts();
    mrg.SaveMergedObject();
  }

  mrg.SaveProxyObject(trg);*/
  return 0;
}

