#include "StdAfx.h"
#include ".\proxymerge.h"
#include "..\ObjToolProxy.h"
#include "..\ObjToolCheck.h"
#include "..\ObjToolTopology.h"

#include <el/math/math3d.hpp>

ProxyMerge::ProxyMerge(const char *projectRoot):_projectRoot(projectRoot),_curObject(NULL)
{
  _firstProxyFace=-1;
  _nMergedProxies = 0; 
}

ProxyMerge::~ProxyMerge(void)
{
  delete _curObject;
}


bool ProxyMerge::LoadProxyObject(const char *name)
{
  if (_proxyObject.Load(Pathname(name),NULL,NULL)!=0) return false;
  return true;
}


bool ProxyMerge::MergeProxy(const char *proxyName)
{
  if (_curObject == NULL)
  {
    _curObject = new LODObject; 
  }

  ObjectData *proxyLod=_proxyObject.Active();
  LODObject lodProxy;
  Matrix4P proxyTrn;

  int proxyFace=proxyLod->GetTool<ObjToolProxy>().FindProxyFace(proxyName);
  if (proxyFace==-1) return false;

  Pathname proxyPath(proxyName+strlen("proxy:"),Pathname(_projectRoot));
  proxyPath.SetExtension(".p3d");
  if (lodProxy.Load(proxyPath,NULL,NULL)!=0) return false; 
  
  FaceT proxy(proxyLod,proxyFace);
  ObjToolProxy::GetProxyTransform(proxy,proxyTrn);
  
  if (_firstProxyFace==-1)
  {
    _firstProxyFace=proxyFace;
    //_firstProxyTransform = M4Identity;
    _firstProxyTransform.SetInvertGeneral(proxyTrn);
    proxyTrn.SetIdentity();
    NamedSelection *sel=proxyLod->GetNamedSel(proxyName);
    Pathname mn(_mergedName);    
    sel->SetName(RString("proxy:\\")+mn.GetDirectory()+mn.GetTitle()+".1");
  }
  else
  {
    proxyLod->DeleteFace(proxyFace);
    proxyTrn=_firstProxyTransform*proxyTrn;
  }
  for (int i=0;i<lodProxy.NLevels();i++)
  {
    ObjectData *objProxy=lodProxy.Level(i);
    int targetLevel=_curObject->FindLevelExact(lodProxy.Resolution(i));
    if (targetLevel==-1) targetLevel=_curObject->AddLevel(ObjectData(),lodProxy.Resolution(i));
    ObjectData *objTarget=_curObject->Level(targetLevel);

    for (int j=0;j<objProxy->NPoints();j++)
    {
      PosT &pos=objProxy->Point(j);
      pos.SetPoint(proxyTrn.FastTransform(pos));
    }

    objTarget->Merge(*objProxy,false);
  }

  /// add proxy into break lod
  int targetLevel=_curObject->FindLevelExact(LOD_SUB_PARTS);
  if (targetLevel==-1) targetLevel=_curObject->AddLevel(ObjectData(), LOD_SUB_PARTS); // Lod for proxies
  ObjectData *objTarget=_curObject->Level(targetLevel);
  objTarget->GetTool<ObjToolProxy>().CreateProxy(proxyName+strlen("proxy:"), proxyTrn);

  _nMergedProxies ++;

  return true;
}

void ProxyMerge::CombineParts()
{
  if (_curObject = NULL)
    return;

  for (int i=0;i<_curObject->NLevels();i++)
  {
    ObjectData *cur=_curObject->Level(i);
    cur->MergePoints(0.0001,false);
  }
}

bool ProxyMerge::SaveProxyObject(const char *name)
{
 ObjectData *proxyLod=_proxyObject.Active();    
 proxyLod->GetTool<ObjToolCheck>().IsolatedPoints();
 proxyLod->SelectionDelete();
 return _proxyObject.Save(Pathname(name),9999,false,NULL,NULL)!=0;
}

bool ProxyMerge::SaveMergedObject()
{
  if (_curObject == NULL)
    return true;

  

  // delete level  0
 //_curObject->DeleteLevel(0);

 // No shadow
 { 
  /*_curObject->SelectLevel(_curObject->FindLevelExact(1.0f));
  _curObject->Active()->SetNamedProp("LODNoShadow","1");
  _curObject->SelectLevel(_curObject->FindLevelExact(2.0f));
  _curObject->Active()->SetNamedProp("LODNoShadow","1");
  _curObject->SelectLevel(_curObject->FindLevelExact(3.0f));
  _curObject->Active()->SetNamedProp("LODNoShadow","1");*/
 }
  

 // find components in geometry lod
 /*int geomLODId = _curObject->FindLevelExact(LOD_GEOMETRY);
 ASSERT(geomLODId != -1);
 if (geomLODId != -1)
 { 
   //_curObject->DeleteLevel(geomLODId); // no geometry level
   //_curObject->AddLevel(ObjectData(), LOD_GEOMETRY); // no geometry level
   _curObject->SelectLevel(geomLODId);
   ObjectData * proxyLod = _curObject->Active();

   // delete all named selections staring with cc
   for(int i = 0; i < MAX_NAMED_SEL; i++)
   {
     const char * sel = proxyLod->NamedSel(i);
     if (sel != NULL && strncmp("cc_", sel,3) == 0)
       proxyLod->DeleteNamedSel(sel);
   }

   proxyLod->GetTool<ObjToolTopology>().CreateComponents();

   /// set mass  
   float mass = _nMergedProxies * 1000.0f / proxyLod->NPoints();
   for(int i = 0; i < proxyLod->NPoints(); i++)
   {
     proxyLod->SetPointMass(i, mass);
   }
 }
*/
 if ( _curObject->Save(Pathname(_mergedName),9999,false,NULL,NULL)==0) 
 {
   delete _curObject;
   _curObject = NULL;
   _firstProxyFace=-1;
   _nMergedProxies = 0;
   return true;
 }
 return false;
}
