#include "common.hpp"
#include "GlobalFunctions.h"
#include <malloc.h>
#include "LODObject.h"
#include "ObjectData.h"
#include "ObjToolTopology.h"
#include "ObjToolSections.h"
#include ".\bitxtimport.h"
#include "SpecialLod.hpp"


namespace ObjektivLib {

void CBiTXTImport::ReadNum()
{
  bool tecka=false;
  int pos=0;
  int i=_rdstream.get();
  while (i!=EOF && pos<sizeof(_text)-1 && (isdigit(i) || i=='.' || i=='E' || i=='e' || i=='+' || i=='-'))
  {
    _text[pos++]=(char)i;
    if (i=='.' || i=='E'|| i=='e') tecka=true;
    i=_rdstream.get();
  }
  _rdstream.putback((char)i);
  _text[pos]=0;
  if (tecka)
  {
    int succ=sscanf(_text,"%f",&_fValue);
    if (succ==0)
    {SetError(ErrInvalidNumber);}
    else
      _cmdType=Float;
  }
  else
  {
    int succ=sscanf(_text,"%d",&_iValue);
    if (succ==0)
    {SetError(ErrInvalidNumber);}
    else
      _cmdType=Integer;
  }  
}

void CBiTXTImport::ReadString()
{
  int pos=0;
  int i=_rdstream.get();
  while (i!=EOF && i!='"' && pos<sizeof(_text)-1) 
  {
    _text[pos++]=(char)i;
    i=_rdstream.get();
  }
  /*  if (pos==sizeof(_text)-1)    
  while (i!=EOF && i!='"' && pos<sizeof(_text)) 
  i=_rdstream.get();       */
  _text[pos]=0;
  if (i!='"') 
  {
    SetError(ErrMissingQuotes);
  }
  _cmdType=Text;
}

struct CBiTXTImport_CmdAssign
{
  CBiTXTImport::SectionAndCommands type;
  char *text;
};

static CBiTXTImport_CmdAssign cmdassign[]=
{ {CBiTXTImport::SectHeader,":header"},
{CBiTXTImport::SectMaterial,":material"},      
{CBiTXTImport::SectLod,":lod"},
{CBiTXTImport::SectObject,":object"},
{CBiTXTImport::SectPoints,":points"},
{CBiTXTImport::SectFace,":face"},
{CBiTXTImport::SectEdges,":edges"},
{CBiTXTImport::SectSelection,":selection"},
{CBiTXTImport::SectEnd,":end"},
{CBiTXTImport::cmdVersion,"version"},
{CBiTXTImport::cmdSharp,"sharp"},
{CBiTXTImport::cmdAngle,"angle"},
{CBiTXTImport::cmdIndex,"index"},
{CBiTXTImport::cmdNormal,"normal"},
{CBiTXTImport::cmdUV,"uv"},
{CBiTXTImport::cmdSG,"sg"},
{CBiTXTImport::cmdTexture,"texture"},
{CBiTXTImport::cmdMaterial,"material"},
{CBiTXTImport::stEdges,"edges"},
{CBiTXTImport::stNormals,"normals"},
{CBiTXTImport::SectMass,":mass"},
{CBiTXTImport::cmdIndexBase,"index-base"},
{CBiTXTImport::cmdChannels,"uvchannels"}
};


void CBiTXTImport::ReadCommand()
{
  int pos=0;
  int i=_rdstream.get();
  while (i!=EOF && !isspace(i) && pos<sizeof(_text)-1) 
  {
    _text[pos++]=(char)i;
    i=_rdstream.get();
  }
  _text[pos]=0;
  for (int i=0;i<lenof(cmdassign);i++)
  {
    if (_stricmp(_text,cmdassign[i].text)==0)
    {
      _command=cmdassign[i].type;
      return;
    }
  }
  SetError(ErrUnknownCommand);
}

void CBiTXTImport::ReadNext()
{
  if (IsError()) return;
  int i;
  i=_rdstream.get();
  while (isspace(i)) i=_rdstream.get();
  if (i==EOF)
  {
    _cmdType=FileEof;
    return;
  }
  if (i=='-' || i=='+' || i=='.' || isdigit(i))
  {
    _rdstream.putback((char)i);
    ReadNum();
    return;
  }
  if (i=='"') 
  {
    ReadString();
    return;
  }
  _cmdType=Command;
  if (i==':') 
  {
    _cmdType=Section;
  }
  else if (!isalpha(i)) 
  {SetError(ErrUnknownToken);}
  _rdstream.putback((char)i);
  ReadCommand();
}


void CBiTXTImport::OpenFile(const char *filename)
{
  _rdstream.open(filename,ios::in);
  if (!_rdstream)
  {
    SetError(ErrFileOpenError);
    return;
  }
  _rdstream.seekg(0,ios_base::end);
  if (!_rdstream) 
  {
    _progress=NULL;
    _rdstream.clear(0);
  }
  else
  {
    _progress->SetMax(_rdstream.tellg());
    _rdstream.seekg(0);
  }
}

void CBiTXTImport::ReadHeaderSection()
{
  if (_cmdType==Command && _command==cmdVersion)
  {
    ReadNext();
    if (_cmdType==Float)
    {
      _version=_fValue;
      if (_version!=1.0) {
        SetError(ErrUnknownVersion);return;
      }
      ReadNext();
      if (_cmdType==Command && _command==cmdSharp)
      {
        ReadNext();
        if (_cmdType==Command && (_command==cmdAngle || _command==cmdSG || _command==stEdges || _command==stNormals))
        {
          _sharp=_command;
          ReadNext();
          if (_sharp==cmdAngle)            
              if (_cmdType==Float)
              {
                _angle=_fValue;
                ReadNext();
              }
              else
                SetError(ErrExceptingFloatNumber);
          if (_cmdType==Command && _command==cmdIndexBase)
          {
            ReadNext();
            if (_cmdType==Integer)
            {
              _indexBase=_iValue;
              ReadNext();
            }
            else
              SetError(ErrExceptingIntegerNumber);
          }
        }
        else
          SetError(ErrExceptingSharpValue);
      }
      else
        SetError(ErrExceptingSharp);
    }
    else
      SetError(ErrExceptingFloatNumber);
  }
  else
    SetError(ErrExceptingVersion);
}



void CBiTXTImport::ReadMaterial()
{
  SetError(ErrUnimplemented);
}

Vector3 CBiTXTImport::ReadVector()
{
  float x,y,z;
  if (_cmdType==Float)
  {
    x=_fValue;
    ReadNext();
    if (_cmdType==Float)
    {
      z=_fValue;
      ReadNext();
      if (_cmdType==Float)
      {
        y=_fValue;
        ReadNext();
        return Vector3(x,y,z);
      }
      else SetError(ErrExceptingFloatNumber);
    }
    else SetError(ErrExceptingFloatNumber);
  }
  else SetError(ErrExceptingFloatNumber);
  return Vector3();
}

Vector3 CBiTXTImport::ReadUV()
{
  float x,y;
  if (_cmdType==Float)
  {
    x=_fValue;
    ReadNext();
    if (_cmdType==Float)
    {
      y=1.0f-_fValue;
      ReadNext();
      return Vector3(x,y,0);        
    }
    else SetError(ErrExceptingFloatNumber);
  }
  else SetError(ErrExceptingFloatNumber);
  return Vector3();
}

void CBiTXTImport::ReadPoints(ObjectData *obj)
{
  while (_cmdType==Float)
  {
    Vector3 vx=ReadVector();
    if (IsError()) return;
    vx*=0.001f;
    PosT *pos=obj->NewPoint();    
    pos->SetPoint(vx);
  }
}

void CBiTXTImport::ReadNormals(ObjectData *obj, int fcindex)
{
  FaceT fc;
  Vector3 vx=ReadVector();
  if (IsError()) return;
  for (int i=fcindex;i<obj->NFaces();i++)
  {
    FaceT fc(obj,i);
    int strpos=1;
    if (i!=fcindex) 
    {
      FaceT fcl(obj,i-1);
      fc.SetNormal(strpos,fcl.GetNormal(fcl.N()-1));
      strpos++;
    }
    for (;strpos<fc.N();strpos++)
    {
      Vector3 vz=ReadVector();
      if (IsError()) return;
      fc.SetNormal(strpos,obj->AddNormal(vz));
    }
    fc.SetNormal(0,obj->AddNormal(vx));
  }  
}

void CBiTXTImport::ReadUVS(ObjectData *obj, int fcindex)    
{
  FaceT fc;
  Vector3 vx=ReadUV();
  if (IsError()) return;
  for (int i=fcindex;i<obj->NFaces();i++)
  {
    FaceT fc(obj,i);
    int strpos=1;
    if (i!=fcindex) 
    {
      FaceT fcl(obj,i-1);
      fc.SetU(strpos,fcl.GetU(fcl.N()-1));
      fc.SetV(strpos,fcl.GetV(fcl.N()-1));
      strpos++;
    }
    for (;strpos<fc.N();strpos++)
    {
      Vector3 vz=ReadUV();
      if (IsError()) return;
      fc.SetUV(strpos,vz[0],vz[1]);
    }
    fc.SetUV(0,vx[0],vx[1]);
  }  
}



void CBiTXTImport::ReadFace(ObjectData *obj, bool oldStyle)
{
  if (_cmdType==Command && _command==cmdIndex)
  {
    ReadNext();
    int vxpts[256];
    int vxcnt=0;

    while (_cmdType==Integer)
    {
      if (vxcnt==256) 
      {
        SetError(ErrTooHighCountOfVertices);
        return;
      }
      if (_iValue<1 || _iValue>obj->NPoints()) { SetError(ErrIntegerOutOfRange);return;}
      vxpts[vxcnt++]=_iValue-_indexBase;
      ReadNext();
    }
    if (vxcnt<3) 
    {
      SetError(ErrTooLowCountOfVertices);
      return;
    }
    int fcindex=obj->NFaces();
    for (int i=1;i+1<vxcnt;i+=2)
    {
      FaceT fc;
      fc.CreateIn(obj);
      fc.SetN(i+2>=vxcnt?3:4);
      fc.SetPoint(0,vxpts[0]);
      fc.SetPoint(1,vxpts[i]);
      fc.SetPoint(2,vxpts[i+1]);
      if (fc.N()==4) fc.SetPoint(3,vxpts[i+2]);
    }
    while (_cmdType==Command)
    {
      switch (_command)
      {
      case cmdNormal:
        ReadNext();
        ReadNormals(obj,fcindex);
        break;
      case cmdUV:
        ReadNext();
        if (!oldStyle)
        {
          if (_cmdType==Integer)
          {
            int i=_iValue;
            if (obj->SetActiveUVSet(i)==false)
            {
              SetError(ErrIntegerOutOfRange);
              return;
            }
            ReadNext();
          }
          else
          {
            SetError(ErrExceptingIntegerNumber);
            return;
          }          
        }
        ReadUVS(obj,fcindex);
        break;
      case cmdTexture:
        {
          ReadNext();
          if (_cmdType==Text) 
          {
            for (int i=fcindex;i<obj->NFaces();i++)
            {
              FaceT fc(obj,i);
              fc.SetTexture(_text);
            }
          }
          else 
          {SetError(ErrExceptingString);return;}
          ReadNext();
        }
        break;
      case cmdMaterial:
        {
          ReadNext();
          if (_cmdType==Text) 
          {
            for (int i=fcindex;i<obj->NFaces();i++)
            {
              FaceT fc(obj,i);
              fc.SetMaterial(_text);
            }
          }
          else
          {SetError(ErrExceptingString);return;}
          ReadNext();
        }
        break;
      case cmdSG:
        {
          ReadNext();
          if (_cmdType==Integer) 
          {
            for (int i=fcindex;i<obj->NFaces();i++)
            {
              FaceT fc(obj,i);
              fc.SetFlags(_iValue);
            }
          }
          else
          {SetError(ErrExceptingIntegerNumber);return;}
          ReadNext();
        }
        break;
      default: return;
      }
    }
  }
}

void CBiTXTImport::ReadEdges(ObjectData *obj)
{
  while (_cmdType==Integer)
  {
    int a=_iValue;
    if (a<0 || a>=obj->NPoints()) { SetError(ErrIntegerOutOfRange);return;}
    ReadNext();
    if (_cmdType==Integer)
    {
      int b=_iValue;
      if (b<0 || b>=obj->NPoints()) { SetError(ErrIntegerOutOfRange);return;}
      obj->AddSharpEdge(a,b);
      ReadNext();
    }
    else
      SetError(ErrExceptingIntegerNumber);
  }  
}

void CBiTXTImport::ReadSelection(ObjectData *obj)
{
  if (_cmdType==Text)
  {
    obj->SaveNamedSel(_text);
    NamedSelection *sel=obj->GetNamedSel(_text);
    ReadNext();
    while (_cmdType==Integer)
    {
      int a=_iValue-_indexBase;
      if (a<0 || a>=obj->NPoints()) 
      {
        SetError(ErrIntegerOutOfRange);        
        return;
      }
      ReadNext();
      if (_cmdType==Float)
      {
        if (_fValue<0.0f || _fValue>1.0f) {SetError(ErrFloatOutOfRange);return;}
        sel->SetPointWeight(a,_fValue);
      }
      else if (_cmdType==Integer)
      {
        if (_iValue<0 || _iValue>100) {SetError(ErrIntegerOutOfRange);return;}
        sel->SetPointWeight(a,_iValue*0.01f);
      }
      else {SetError(ErrExceptingFloatNumber);return;}
      ReadNext();
    }   
    sel->SelectFacesFromPoints();
  }
  else
    SetError(ErrExceptingString);
}

void CBiTXTImport::ReadMass(ObjectData *obj)
{
  for (int i=0;i<obj->NPoints();i++)
  {
    if (_cmdType==Float)
    {
      obj->SetPointMass(i,_fValue);
      ReadNext();
    }
  }
}


void CBiTXTImport::ReadUnknownObject(ObjectData *obj)
{
  bool oldstyle=true;
  if (_cmdType==Command && _command==cmdChannels)
  {
    ReadNext();
    while (_cmdType==Integer)
    {
      obj->AddUVSet(_iValue);
      ReadNext();
    }
    oldstyle=false;
  }
  if (_cmdType==Section && _command==SectPoints)
  {
    if (_progress) _progress->SetPos(_rdstream.tellg());
    ReadNext();
    ReadPoints(obj);
    while (true)
    {
      if (_cmdType==Section)
      {
        switch (_command)
        {
        case SectFace:
          ReadNext();
          ReadFace(obj,oldstyle);
          break;
        case SectEdges:
          ReadNext();
          ReadEdges(obj);
          break;
        case SectSelection:
          ReadNext();
          ReadSelection(obj);
          break;
        case SectMass:
          ReadNext();
          ReadMass(obj);
        default: 
          {
            if (_sharp==cmdSG)
            {
              DWORD *sg=new DWORD [obj->NFaces()];
              for (int i=0;i<obj->NFaces();i++)
              {
                FaceT fc(obj,i);
                sg[i]=fc.GetFlags();
                fc.SetFlags(0);
              }
              obj->GetTool<ObjToolTopology>().ReadSmoothGroups(sg, false);
              delete [] sg;
            }
            else if (_sharp==stNormals)
              obj->GetTool<ObjToolTopology>().DetectEdgesFromNormals();
            else if (_sharp==cmdAngle)
              obj->GetTool<ObjToolTopology>().AutoSharpEdges(_angle);            
            return;
          }
        }
      }
      else 
      {SetError(ErrExceptingSection);break;}
    }
  }
  else
    SetError(ErrExceptingPointsSection);
}

void CBiTXTImport::ReadSpecifiedObject(ObjectData *obj)
{
  const char *objname="";
repeat:
  if (_cmdType==Text)     
  {
    objname=strcpy((char *)alloca(strlen(_text)+1),_text);
    ReadNext();
  }
  ObjectData object;
  ReadUnknownObject(&object);
  if (!IsError())
  {    
    int p=obj->NPoints();
    int q=obj->NFaces();
    if (objname[0])
    {
      object.ClearSelection();
      for (int i=0;i<object.NPoints();i++) object.PointSelect(i);
      object.SelectFacesFromPoints();
      object.SaveNamedSel(objname);
      object.ClearSelection();
    }
    obj->Merge(object);
    if (_cmdType==Section && _command==SectObject)
    {
      ReadNext();
      goto repeat;
    }
  }  
  if (_sortFaces)
    obj->GetTool<ObjToolSections>().SortTextures();
}


void CBiTXTImport::ReadLods(LODObject *lod)
{
  float level;
  if (_cmdType==Float)    
    level=_fValue;
  else if (_cmdType==Integer)
    level=(float)_iValue;
  else if (_cmdType==Text)
    level=LODNameToResol(RString (_text));
  else 
  {
    SetError(ErrExceptingLodSpecifier);
    return;
  }
  ReadNext();
  if (_cmdType==Section && _command==SectObject)
  {
    ObjectData object;
    ReadNext();
    ReadSpecifiedObject(&object);
    if (!IsError())
    {
      int l=lod->FindLevelExact(level);      
      if (l!=-1) *lod->Level(l)=object;
      else lod->AddLevel(object,level);
    }        
  }
  else 
    SetError(ErrExceptingObject);
}

void CBiTXTImport::ReadAfterHeader(LODObject *lod)
{
  while (_cmdType==Section && _command==SectMaterial)
  {
    ReadNext();
    ReadMaterial();
  }
  while (_cmdType==Section && _command==SectLod) 
  {
    ReadNext();
    ReadLods(lod);
  }
  while (_cmdType==Section && _command==SectObject)
  {
    ReadNext();
    ReadSpecifiedObject(lod->Active());
  }
  while (_cmdType==Section && _command==SectPoints)
  {
    ReadNext();
    ReadUnknownObject(lod->Active());
    lod->Active()->GetTool<ObjToolSections>().SortTextures();
  }    
}


void CBiTXTImport::ReadHeader(LODObject *lod)
{
  ReadNext();
  if (_cmdType==Section && _command==SectHeader)
  {
    ReadNext();
    if (!IsError()) ReadHeaderSection();
    if (!IsError()) ReadAfterHeader(lod);
    return;
  }
  SetError(ErrMissingHeader);
}

void CBiTXTImport::Import(const char *filename, LODObject *lod)
{
  ProgressBar<int> progress(0);
  _progress=&progress;
  _indexBase=1;
  OpenFile(filename);
  if (IsError()) return;
  ReadHeader(lod);
  if (IsError()) return;
  if (_cmdType!=Section || _command!=SectEnd)
    if (_cmdType==FileEof)      
      SetError(ErrUnexceptedEnd);
    else
      SetError(ErrStructureRuleBreak);
  else
  {
    int p=lod->FindLevelExact(0);
    if (p!=-1)
    {
      if (lod->Level(p)->NPoints()==0 && lod->Level(p)->NFaces()==0)
        lod->DeleteLevel(p);
    }
  }
}
std::pair<int,int> CBiTXTImport::GetErrorLine()
{
  int line=1,col=1;
  _rdstream.clear(0);
  long pos=_rdstream.tellg();
  _rdstream.seekg(0);
  while (_rdstream.tellg()<pos && !(!_rdstream))
  {
    int i=_rdstream.get();
    while (i>=0 && i!='\n') i=_rdstream.get();
    if (i==EOF) break;
    col++;
    if (i=='\n') {col=1;line++;}
  }
  std::pair<int,int> pt;
  pt.first=col;
  pt.second=line;
  return pt;
}


void CBiTXTImport::FormatError(char *buff,int size)
{
  std::pair<int,int> pt=GetErrorLine();
  const char *chyba;
  switch (GetError())
  {
  case ErrMissingHeader: chyba="Missing or invalid header section. File format is unknown";break;
  case ErrUnknownVersion: chyba="Unsupported file version";break;
  case ErrUnknownToken: chyba="Unknown token found";break;
  case ErrMissingQuotes: chyba="Missing quotes";break;
  case ErrUnknownCommand: chyba="Unknown command";break;
  case ErrFileOpenError: chyba="File open error";break;
  case ErrInvalidNumber: chyba="Invalid number";break;
  case ErrExceptingFloatNumber: chyba="Excepting float number";break;
  case ErrExceptingAngle: chyba="Excepting 'ANGLE' command";break;
  case ErrExceptingSharpValue: chyba="Excepting one of keywords: SG, NORMALS, EDGES, ANGLE";break;
  case ErrExceptingSharp: chyba="Excepting 'SHARP' command";break;
  case ErrExceptingVersion: chyba="Excepting 'VERSION' command";break;
  case ErrUnimplemented: chyba="Command or section not is currently implemnted";break;
  case ErrExceptingLodSpecifier: chyba="Excepting float number or integer number or string with name of special LOD";break;
  case ErrExceptingObject: chyba="Excepting ':OBJECT' section";break;
  case ErrExceptingSection: chyba="Excepting section";break;
  case ErrExceptingPointsSection: chyba="Excepting ':POINTS' section";break;
  case ErrTooLowCountOfVertices: chyba="Too less vertices";break;
  case ErrTooHighCountOfVertices: chyba="Too many vertices";break;
  case ErrExceptingIntegerNumber: chyba="Excepting integer number";break;
  case ErrIntegerOutOfRange: chyba="Integer out of range";break;
  case ErrFloatOutOfRange: chyba="Float out of range";break;
  case ErrExceptingString: chyba="Excepting string";break;
  case ErrUnexceptedEnd: chyba="Unexcepted end of file";break;
  case ErrStructureRuleBreak: chyba="Structure rulebreak, token is unexcepted here";break;
  default: chyba="An error (Error description is missing)";break;
  }
  _snprintf(buff,size,"%s at line %d character %d", chyba,pt.second,pt.first);
}

bool CBiTXTExport::Export(const char *filename, LODObject *lod,ExportMode mode)
{
  _mode=mode;
  _wrstream.open(filename,ios::out|ios::trunc);
  if (!_wrstream) return false;

  WriteHeader(lod);
  WriteLods(lod);
  WriteCommand(SectEnd);
  return !(!_wrstream);
}

void CBiTXTExport::WriteNum(float value)
{
  char buff[256];
  sprintf(buff,"%g",value);
  if (strchr(buff,'.')==NULL && strchr(buff,'e')==NULL && strchr(buff,'E')==NULL) strcat(buff,".0");
  _wrstream<<buff<<" ";
}

void CBiTXTExport::WriteNum(int value)
{
  _wrstream<<value<<" ";
}


void CBiTXTExport::WriteString(const char *text)
{
  _wrstream<<"\""<<text<<"\" ";
}

void CBiTXTExport::WriteCommand(SectionAndCommands cmd)
{
  for (int i=0;i<lenof(cmdassign);i++)
  {
    if (cmdassign[i].type==cmd)
    {
      _wrstream<<cmdassign[i].text<<" ";
      return;
    }
  }  
 ASSERT(false);
}

void CBiTXTExport::Eoln()
{
  _wrstream<<"\n";
}


void CBiTXTExport::WriteHeader(LODObject *lod)
{
  WriteCommand(SectHeader);Eoln();
  WriteCommand(cmdVersion);WriteNum(1.0f);Eoln();
  WriteCommand(cmdSharp);
  switch (_mode)
  {
  case ExpNormals: WriteCommand(stNormals);Eoln();break;
  case ExpSG: WriteCommand(cmdSG);Eoln();break;
  case ExpEdges: WriteCommand(stEdges);Eoln();break;
  default: ASSERT(false);break;
  }
  Eoln();
}

void CBiTXTExport::WriteLods(LODObject *lod)
{
  int celk=0;
  int cur=0;
  for (int i=0;i<lod->NLevels();i++) celk=celk+lod->Level(i)->NPoints()+lod->Level(i)->NFaces();
  ProgressBar<int> progress(celk);
  for (int i=0;i<lod->NLevels();i++)
  {
    cur+=lod->Level(i)->NFaces()+lod->Level(i)->NPoints();
    progress.SetNextPos(cur);
    if (_mode==ExpNormals)  lod->Level(i)->RecalcNormals(true);
    WriteCommand(SectLod);WriteNum(lod->Resolution(i));Eoln();
    WriteLevel(lod->Level(i));  
    if (ISMASS(lod->Resolution(i))) WriteMass(lod->Level(i));
  }  
}

void CBiTXTExport::WriteLevel(ObjectData *obj)
{
  int i;
  unsigned long *groups;
  if (_mode==ExpSG)
  {
    groups=new unsigned long[obj->NFaces()];
    obj->GetTool<ObjToolTopology>().BuildSmoothGroups(groups);
  }
  WriteCommand(SectObject);Eoln();
  WritePoints(obj);Eoln();
  for (i=0;i<obj->NFaces();i++) WriteFace(obj,groups,i);
  if (_mode==ExpEdges) WriteEdges(obj);Eoln();
  for (i=0;i<MAX_NAMED_SEL;i++) WriteSelection(obj,i);Eoln();
  if (_mode==ExpSG) delete [] groups;
}

void CBiTXTExport::WritePoints(ObjectData *obj)
{
  WriteCommand(SectPoints);Eoln();
  for(int i=0;i<obj->NPoints();i++) 
  {
    Vector3 vx=obj->Point(i);
    vx[0]*=1000.0f;
    vx[1]*=1000.0f;
    vx[2]*=1000.0f;
    WriteVector(vx);  Eoln();
  }
}

void CBiTXTExport::WriteFace(ObjectData *obj,unsigned long *groups, int index)
{
  int i;
  WriteCommand(SectFace);Eoln();
  WriteCommand(cmdIndex);
  FaceT fc(obj,index);
  for (i=0;i<fc.N();i++) WriteNum(fc.GetPoint(i)+1);Eoln();
  if (_mode==ExpNormals)
  {
    WriteCommand(cmdNormal);
    for (i=0;i<fc.N();i++) WriteVector(obj->Normal(fc.GetNormal(i)));        
    Eoln();
  }
  for (i=0;i<fc.N();i++) if (fc.GetU(i)!=0.0f || fc.GetV(i)!=0.0f)
  {
    WriteCommand(cmdUV);
    for (i=0;i<fc.N();i++) WriteUV(fc.GetU(i),fc.GetV(i));
    Eoln();
    break;
  }
  if (_mode==ExpSG)
  {
    WriteCommand(cmdSG);
    _wrstream<<groups[index];
    Eoln();
  }
  if (fc.GetTexture().GetLength())
  {
    WriteCommand(cmdTexture);
    WriteString(fc.GetTexture());Eoln();
  }
  if (fc.GetMaterial().GetLength())
  {
    WriteCommand(cmdMaterial);
    WriteString(fc.GetMaterial());Eoln();
  };
}

void CBiTXTExport::WriteUV(float u, float v)
{
    WriteNum(u);
    WriteNum(1.0f-v);
}

void CBiTXTExport::WriteEdges(ObjectData *obj)
{
  WriteCommand(SectEdges); Eoln();
  int a,b,i;
  for (i=0;obj->EnumEdges(i,a,b);)
  {
    WriteNum(a);
    WriteNum(b);
  }
  Eoln();
}

void CBiTXTExport::WriteSelection(ObjectData *obj, int index)
{
  const NamedSelection *sel=obj->GetNamedSel(index);
  if (sel==NULL) return;
  WriteCommand(SectSelection);
  WriteString(sel->Name());Eoln();
  for (int i=0;i<obj->NPoints();i++) if (sel->PointSelected(i))
  {
    WriteNum(i+1);
    WriteNum(sel->PointWeight(i));
    Eoln();
  }    
}

void CBiTXTExport::WriteMass(ObjectData *obj)
{  
  WriteCommand(SectMass);
  Eoln();
  for (int i=0;i<obj->NPoints();i++) WriteNum((float)obj->GetPointMass(i));
  Eoln();
}


void CBiTXTExport::WriteVector(Vector3 &vx)
{
    WriteNum(vx[0]);
    WriteNum(vx[2]);
    WriteNum(vx[1]);
}

}