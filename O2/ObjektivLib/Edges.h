// Edges.h: interface for the CEdges class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EDGES_H__B80FCF89_B645_47F4_B9BD_1A7FD89FDF52__INCLUDED_)
#define AFX_EDGES_H__B80FCF89_B645_47F4_B9BD_1A7FD89FDF52__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

namespace ObjektivLib {


class ObjectData;

class CEdges  
  {
  int **_edgelist;
  int _nvertx;
  int _levels;
  public:
    int GetEdge(int idx,int i) const;
    void Clear();
    void RemoveEdge(int va, int vb);
    void SetVertices(int cnt, int mode=1);
    void SetEdge(int a, int b);
    bool IsEdge(int va,int vb) const
      {return TestSharp(va,vb) || TestSharp(vb,va);}
    bool IsEdgeInDirection(int va,int vb) const
      {return TestSharp(va,vb);}
    CEdges(int npoints=0);    
    bool operator()(int a, int b) {return IsEdge(a,b);}
    virtual ~CEdges();
    int GetN() {return _nvertx;}
    
  protected:
    bool ExpandLevel();
    bool TestSharp(int a, int b) const;
  };


}
//--------------------------------------------------

#endif // !defined(AFX_EDGES_H__B80FCF89_B645_47F4_B9BD_1A7FD89FDF52__INCLUDED_)

