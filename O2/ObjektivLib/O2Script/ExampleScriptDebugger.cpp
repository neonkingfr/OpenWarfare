#include "StdAfx.h"
#include ".\examplescriptdebugger.h"
#include <conio.h>

void ExampleScriptDebugger::LoadSourcePart()
{
  const char *p=_context._expression+_context._pc;
  while (*p>1 && *p<33) *p++;
  strncpy(srcpart,p,70);
  char *c=srcpart+70;
  *c=0;
  c=strchr(srcpart,'\n');
  if (c) *c=0;
}

void ExampleScriptDebugger::OnTrace()
{
  LoadSourcePart();
  printf("\nBreakpoint line %d: %s\n ",GetCurLine(),srcpart);
  ShowMenu();
}

void ExampleScriptDebugger::OnVariableBreakpoint(const char *name, const GameValue &val)
{
  LoadSourcePart();
  printf("Variable changed at line %d\n"
         "%s=%s\n"
         "%s...\n",GetCurLine(),name,val.GetText().Data(),srcpart);
  ShowMenu();
}

void ExampleScriptDebugger::ShowMenu()
{
  DumpVariables(true);
  do
  {
    puts("\b<Enter>-Trace next, <Space>-Trace over, <Esc> - Step Out\n<V>ariable breakpoint, <D>umpVars, <R>un, <T>erm,<W>Watch");  
    char c=toupper(getch());
    printf("%c\n",c);
    switch (c)
    {
    case 13: SetTraceMode(traceBreakNext);return;
    case 32: SetTraceMode(traceStepOver);return;
    case 'R': SetTraceMode(traceRun);return;
    case 'T': SetTraceMode(traceTerminate);return;
    case 'V': ToggleVarBreakpoint();break;
    case 'D': DumpVariables(true);break;
    case 'W': WatchVariable();break;  
    case 27: SetTraceMode(traceStepOut);return;
    }
  }
  while (true);
}

void ExampleScriptDebugger::DumpVariables(bool all)
{
  if (all) 
  {
    for (int i=_context._curContext;i>=0;i--) if (_context._contextStack[i])
      DumpVariables(GetLocalVariables(i));
  }
  else
    DumpVariables(GetLocalVariables());
  DumpVariables(_context._script);
  puts("");
}

bool ExampleScriptDebugger::PrintVariable::operator()(GameVariable &variable,VarBankType *container)
{
  printf("%s='%s' ",variable.GetName().Data(),variable.GetValueText().Data());
  return false;
}

static void Func(GameVariable &variable, MapStringToClass< GameVariable, AutoArray< GameVariable, MemAllocD>, MapClassTraits< GameVariable> > *,void *)
  {
  if (variable._value.GetNil())
    printf("%s=nil ",variable.GetName().Data());
  else
  {
    RString result=variable._value.GetText();
    char buff[80];
    strncpy(buff,result,75);
    strcpy(buff+75,"...");
    char *c=strchr(buff,'\r');
    if (c==NULL) c=strchr(buff,'\n');
    if (c!=NULL) strcpy(c,"...");
    printf("%s='%s'(%s) ",variable.GetName().Data(),buff,variable._value.GetData()->GetTypeName());
  }
  }

void ExampleScriptDebugger::DumpVariables(GameVarSpace *var)
{ 
  if (var)
  {
    PrintVariable pr;
    var->_vars.ForEach(Func,NULL);
    printf("| ");
  }
}

void ExampleScriptDebugger::WatchVariable()
{
  char buff[256];
  printf("variable name?:");
  scanf("%255s",&buff);
  if (buff[0] && buff[0]!=32)
  {
    for (int i=_context._curContext;i>=0;i--) if (GetLocalVariables(i))
    {
      GameValue val;
      if (GetLocalVariables(i)->VarGet(buff,val))
        printf("Level: %d %s='%s'\n",i,buff,val.GetText().Data());
    }
    GameValue gval=_context._script->VarGet(buff);
    printf("Global %s='%s'\n",buff,gval.GetText().Data());
  }
}
bool ExampleScriptDebugger::PrintBreakpoint::operator()(const RString &name) const
{
  printf("%s ",name.Data());
  return false;
}


void ExampleScriptDebugger::ToggleVarBreakpoint()
{
  PrintBreakpoint br;
  printf("Active breakpoints:");
  ForEachVarBreakpoint(br);
   char buff[256];
  printf("\nVariable name? ('-' cancel):");
  scanf("%255s",&buff);
  if (buff[0] && buff[0]!='-')
    if (IsVarBreakpoint(buff))
      UnsetVarBreakpoint(buff);
    else
      SetVarBreakpoint(buff);
}

bool ExampleScriptDebugger::OnError(GameState &script,const char *error)
{
  GetContext(script);
  LoadSourcePart();
  printf("Error reported: %s at line %d: %s\n",error,GetCurLine(),srcpart);
  return true;
}
bool ExampleScriptDebugger::OnConsoleOutput(GameState &script,const char *text)
{
  printf("Output: %s\n",text);
  return true;
}

void ExampleScriptDebugger::TestBreak()
  {
    if (kbhit())
    {
      int c=getch();
      if (c==27) 
      {
        SetTraceMode(traceBreakNext);
        puts("<break>");
      }
    }
  } 