#include "StdAfx.h"
#include ".\scriptloader.h"

ScriptLoader::ScriptLoader(void)
{
}

ScriptLoader::~ScriptLoader(void)
{
}


QIStream *ScriptLoader::OnEnterInclude(const char *filename)
{
  ScriptStream *stream=new ScriptStream();
  stream->restoreScriptName=curScriptName;
  curScriptName=Pathname(filename,curScriptName);
  if (curScriptName.IsNull()) curScriptName=filename;
  stream->open(curScriptName);
  if (stream->fail()) 
  {
    curScriptName=stream->restoreScriptName;
    delete stream;
    return NULL;
  }  
  return stream;
}

void ScriptLoader::OnExitInclude(QIStream *str)
{
  ScriptStream *stream=static_cast<ScriptStream *>(str);
  curScriptName=stream->restoreScriptName;  
}
