// O2Script.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <El/elementpch.hpp>
#include <El/Evaluator/express.hpp>
#include <El/Evaluator/evaluatorExpress.hpp>
#include <Es/files/fileContainer.hpp>
#include "..\LODObject.h"
#include "ExampleScriptDebugger.h"
#include <conio.h>

#include "ScriptLoader.h"
#include "MassProcess.h"
#include "conio.h"
#include "..\UIDebugger\DllDebuggingSession.h"
#include "..\O2ScriptLib\O2ScriptClass.h"
#include <strstream>

#include "El\Stringtable\stringtable.hpp"

using namespace std;

#define HELP \
  "O2Script [switches] [scriptName] [parameters]\n"\
  "\n"\
  "switches:\n"\
  "<none>     if none switches specified, runs script \"scriptName\" and \n"\
  "           passes \"parameters\" as string to this\n"\
  "-a         passes arguments into a array of strings (creates empty array if there is no parameters)\n"\
  "-f         \"parameters\" field is list of files or wild cards\n"\
  "-s         process wild cards recursive\n"\
  "-l         parameters is read from file, line by line\n"\
  "-p         parameters is read from the input stream. Example of usage\n"\
  "           dir *.p3d /b | O2Script -p script.bio2s\n"\
  "-c         reads input stream and run text as script. Input data is \n"\
  "           terminated by zero. This is support for runParalel command\n"\
  "-q         suppress any system messages\n"\
  "-d         disables debugger\n"\
  "-w         wait for key after all jobs finished.\n"\
  "\n"\
  "scriptName Name of file containing the script to run. This field is ignored\n"\
  "           when flag -c is specified\n"\
  "\n"\
  "parameters One on more parameters. Parameters can be passed directly to the\n"\
  "           script, or in case that -f,-s,-l is specified, parameters are\n"\
  "           filenames."


class UnlGameState: public GameState
{
public:
  virtual int MaxIterations() const {return 0;};
};

enum SwitchesCodes 
  {switchFiles=1,
  switchSubdir=2, 
  switchList=4,
  switchPipe=8,
  switchClient=16,
  switchQuiet=32,
  switchDisDebug=64,
  switchToArray=128,
  switchWaitKey=256
  };


static unsigned long ProcessSwitches(int &argp, int argc, const char *argv[])
{
  unsigned long sw=0;
  while (argp<argc && (argv[argp][0]=='-' || argv[argp][0]=='/'))
  {
    int chr=0;
    while (argv[argp][chr]=='-' || argv[argp][chr]=='/')
    {
      chr++;
      switch (argv[argp][chr])
      {
      case 'f': sw|=switchFiles;break;
      case 's': sw|=switchSubdir;break;
      case 'l': sw|=switchList;break;
      case 'p': sw|=switchPipe;break;
      case 'c': sw|=switchClient;break;
      case 'q': sw|=switchQuiet;break;
      case 'd': sw|=switchDisDebug;break;
      case 'a': sw|=switchToArray;break;
      case 'w': sw|=switchWaitKey;break;
      };
      chr++;
    }
  argp++;
  }
return sw;
}

struct ScriptExeInfo
{
  O2ScriptClass *_script;
  ScriptDebuggerBase *_debugger;
  GameState *_gState;
  bool quietMode;
  bool arrayMode;
  GameValue result;    
  bool ExecuteScript(const char *parameters="");
  bool ExecuteScript(const char *argv[], int argc);
  bool PostExecute(GameValue result);
};


class MassRunScript: public MassProcess
{
  ScriptExeInfo &_exeinfo;
public:
  MassRunScript(ScriptExeInfo &exeinfo):_exeinfo(exeinfo) {}
  bool ProcessFile(const char *filename);
};

bool ScriptExeInfo::PostExecute(GameValue val)
{
  bool res=val;
  result=val;
  if (res)
  {
    if (!quietMode) fputs("true result returned, stopping processing\n",stderr);
    return false;
  }
  if (_debugger && _debugger->GetTraceMode()==_debugger->traceTerminate)
  {
    if (!quietMode) fputs("Terminated: stopping processing\n",stderr);
    return false;
  }
  if (!quietMode)  fprintf(stderr,"<result>%s</result>\n",result.GetText().Data());
  return true;
}

bool ScriptExeInfo::ExecuteScript(const char *parameters)
{
  if (arrayMode) return ExecuteScript(&parameters, 1);
  else
  {
    if (!quietMode)  fprintf(stderr,"Executing script with \"%s\"\n",parameters);  
    _gState->VarSet("this",RString(parameters),true,false);
   _script->_blockExit=false;
    return PostExecute(_script->RunScript(*_gState));
  }
}

bool ScriptExeInfo::ExecuteScript(const char *argv[], int argc)
{
  GameArrayType arr;
  for (int i=0;i<argc;i++) arr.Append()=argv[i];
  GameValue thisval=arr;
  if (!quietMode)  fprintf(stderr,"Executing script with \"%s\"\n",thisval.GetText().Data());  
  _gState->VarSet("this",thisval,true,false);
  _script->_blockExit=false;
  return PostExecute(_script->RunScript(*_gState));
}


static void ProcessListOfFiles( ScriptExeInfo &exeinfo, int &argp, int argc, const char *argv[], bool recursive)
{
  while (argp<argc)
  {
    MassRunScript ms(exeinfo);
    Pathname pth=argv[argp++];
    if (ms.ProcessFolders(pth.GetDirectoryWithDrive(),pth.GetFilename(),recursive)==false) break;    
  }
}

static bool ProcessInputFileFile(ScriptExeInfo &exeinfo, istream &in)
{
  bool result=true;
  char buff[1024];
  RString line;
  bool eoln;
  while (!in.eof())
  {
    eoln=false;
    line="";
    while (!eoln)
    {
      in.get(buff,sizeof(buff));
      if (!in) break;
      if (buff[0]==0) break;
      line=line+buff;
      eoln=in.peek()=='\n';
      if (eoln) in.get();
    }
    if (!eoln) break;
    if (line[0]!=0)
    {
      if (exeinfo.ExecuteScript(line)==false) {result=false;break;}
    }
  }
  return result;
}

static bool ProcessInputFile( ScriptExeInfo &exeinfo, const char *filename)
{
  bool result=true;
  ifstream in(filename,ios::in);
  if (!in)
  {
    fprintf(stderr,"Unable to open file: %s\n",filename);
    return true;
  }
  ProcessInputFileFile(exeinfo,in);
  return result;
}


static bool ProcessInputFiles( ScriptExeInfo &exeinfo, int &argp, int argc, const char *argv[])
{
  while (argp<argc)
  {
    if (ProcessInputFile(exeinfo,argv[argp++])==false) return false;
  }
 return true;
}

bool MassRunScript::ProcessFile(const char *filename)
{
  return _exeinfo.ExecuteScript(filename);
}


static bool ReadScriptFromInput(O2ScriptClass &o2scr)
{
  ostrstream out;
  int i=cin.get();
  while (i!=EOF && i!=0) {out.put((char )i);i=cin.get();}
  out.put(0);
  o2scr.LoadScriptDirect(out.str());
  out.freeze(false);
  return true;
}

static bool ReadParametersFromInput(ScriptExeInfo &exeinfo)
{
  return ProcessInputFileFile(exeinfo,cin);  
}
/*
int WinMain(HINSTANCE hInst, HINSTANCE prev,const char *cmdline, int showflag)
{
  return _tmain(__argc, __wargv);
}
*/
extern RString GLanguage;
int _tmain(int argc, const char* argv[])
{

	  TCHAR szEXEPath[2048];
	  GetModuleFileName(NULL, szEXEPath, 2048 );

    // cesta k setup.csv
    int nRes = lstrlen(szEXEPath);
    while(szEXEPath[nRes-1] != _T('.'))
      nRes--;
    lstrcpy(szEXEPath+nRes,_T("csv"));

    GLanguage = "English";
    FILE *testf = fopen(szEXEPath,"rb");
    if (testf!=NULL)
    {
      bool result = LoadStringtable ("global", szEXEPath);
      LogF("result %d",result);
    }

    UnlGameState gState;
    O2ScriptClass scriptcls;
    scriptcls.PrepareGameState(gState);
    scriptcls.SetLibraryPaths("u:\\tools\\objektiv2\\dist");    


    if (argc<2) 
    {
      puts(HELP);
      getchar();
      return -1;
    }

    int argp=1;
    unsigned long switches=ProcessSwitches(argp,argc,argv);    
    bool succread=false;
    if (switches & switchClient)
    {
      succread=ReadScriptFromInput(scriptcls);      
    }
    else
    {
      if (argp>=argc) {puts(HELP);return -1;}
      succread=scriptcls.LoadScript(argv[argp++]);
    }
    if (!succread)
    {
      puts(scriptcls.GetPreprocError());
      return -1;
    }
    
    DllDebuggerProvider dbgprovider;
#if _DEBUG
    dbgprovider.CreateProvider("UIDebugger_d.dll");
#else
    dbgprovider.CreateProvider("UIDebugger.dll");
#endif
    DllDebuggingSession session(dbgprovider);
    ScriptDebuggerBase *debugger=session;
    gState.AttachDebugger(debugger);
    ScriptExeInfo exeinfo;
    exeinfo._debugger=debugger;
    exeinfo._gState=&gState;
    exeinfo._script=&scriptcls;
    exeinfo.quietMode=(switches & switchQuiet)!=0;
    exeinfo.arrayMode=(switches & switchToArray)!=0;

    if (switches & switchFiles || switches & switchSubdir)
    {
      ProcessListOfFiles(exeinfo,argp,argc,argv,(switches & switchSubdir)!=0);
    }
    else if (switches & switchList)
    {
      ProcessInputFiles(exeinfo,argp,argc,argv);
    }
    else if (switches & switchPipe)
    {
      ReadParametersFromInput(exeinfo);
    }
    else if (switches & switchToArray)
    {
      exeinfo.ExecuteScript(argv+argp,argc-argp);
    }
    else 
    {
      RString list;
      while (argp<argc)
      {
        list=list+argv[argp]+(argp+1==argc?"":" ");
        argp++;
      }
      exeinfo.ExecuteScript(list);
    }

  if (switches & switchWaitKey) 
  {
    fputs("Press any key...",stderr);
    getche();
  }
  return toInt(exeinfo.result);
}

