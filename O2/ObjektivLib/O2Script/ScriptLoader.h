#pragma once
#include <el\preprocc\preproc.h>
#include <el\pathname\pathname.h>

class ScriptStream: public QIFStream
{
public:
  Pathname restoreScriptName;
};

class ScriptLoader :public Preproc
{
  
public:
  Pathname curScriptName;

  ScriptLoader(void);
  ~ScriptLoader(void);  

  virtual QIStream *OnEnterInclude(const char *filename);
  virtual void OnExitInclude(QIStream *stream);


};
