
 #pragma once
#include <el\evaluator\scriptdebuggerbase.h>

class ExampleScriptDebugger :  public ScriptDebuggerBase
{
  char srcpart[71];
  class PrintVariable
  {
  public: bool operator()(GameVariable &variable,VarBankType *container);
  };
  class PrintBreakpoint
  {
  public: bool operator()(const RString &name) const;
  };
public:
  void LoadSourcePart();
  virtual void OnTrace();
  virtual void OnVariableBreakpoint(const char *name, const GameValue &val);
  void ShowMenu();
  void DumpVariables(bool all=false);
  void DumpVariables(GameVarSpace *var);
  void ToggleVarBreakpoint();
  void WatchVariable();
  virtual bool OnConsoleOutput(GameState &script,const char *text);
  virtual bool OnError(GameState &script,const char *error);
  virtual void TestBreak();
};
