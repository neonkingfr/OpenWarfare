// ObjektivLib.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <malloc.h>
#include <windows.h>

static void ShowLODReport(const Pathname &filename, LODObject &lod)
{
  printf("Object name: %s\n",filename.GetFilename());
  printf("Number of levels: %d\n",lod.NLevels());
  printf("LODindex Resolution NPoints NFaces NAnimations\n");
  for (int i=0;i<lod.NLevels();i++)
  {
    printf("%8d.%10.3g %7d %6d %7d\n",i,lod.Resolution(i),lod.Level(i)->NPoints(),lod.Level(i)->NFaces(),lod.Level(i)->NAnimations());
  }
}
/*
int main(int argc, char* argv[])
{
LODObject lod;
Pathname objname("P:\\OFP2\\Vehicles\\Land\\Wheeled\\HMMWV\\HMMWVTOW\\HMMWVTOW.p3d");
if (lod.Load(objname,NULL,NULL)==-1)
{
printf("LoadError %s\n",objname);
return -1;
}
ShowLODReport(objname,lod);
return 0;
}

*/


void TestFile(const char *path,const char *file)
{
  char *pathname=(char *)alloca(strlen(path)+strlen(file)+20);
  sprintf(pathname,"%s\\%s",path,file);
  LODObject lod;
  lod.Load(pathname,NULL,NULL);
  bool has=false;
  for (int i=0;i<lod.NLevels();i++)
    if (lod.Resolution(i)>=LOD_SHADOW_MIN && lod.Resolution(i)<=LOD_SHADOW_MAX)
    {
      printf("ShadowVolume Level: %g (%s)\n",lod.Resolution(i)-LOD_SHADOW_MIN,pathname);
      has=true;
    }
    if (has==false)
      printf("No Shadow Volume in %s\n", pathname);
}

int main(int argc, char *argv[])
{
  if (argc<2) 
  {
    puts("parametrem je adresar, treba x:\\data3d");
  }
  char *wild=strcpy((char *)alloca(strlen(argv[1])+20),argv[1]);
  strcat(wild,"\\*.p3d");
  printf("Searching %s\n",wild);
  WIN32_FIND_DATA wfind;
  HANDLE h=FindFirstFile(wild,&wfind);
  while (h)
  {
    TestFile(argv[1],wfind.cFileName);
    if (FindNextFile(h,&wfind)==FALSE) 
    {
      FindClose(h);
      h=NULL;
    }
  }
}