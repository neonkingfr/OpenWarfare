#pragma once

namespace ObjektivLib {


class ObjectData;
class LODObject;

namespace Functors
{
  class LoadMergeInfo;
  class LoadOneSetInfo;
  class ProcessStages;
}

class MergeTexturesTool
{
  ParamFile _colorPtm;   

  friend Functors::LoadMergeInfo;
  friend Functors::LoadOneSetInfo;
  friend Functors::ProcessStages;

  class MergeInfo
  {
    RString _sourceTex;
    RString _targetTex;
    Matrix3 _transform;
    
  public:
    MergeInfo(RString targetTex, const ParamEntryVal &texInfo, int mainsizex, int mainsizey);
    MergeInfo(RString search):_sourceTex(search) {}
    MergeInfo(const char *search=0):_sourceTex(search) {}

    const RString &GetTargetTexture() const {return _targetTex;}
    const RString &GetSourceTexture() const {return _sourceTex;}
    const Matrix3 &GetTransform() const {return _transform;}

  int CompareWith(const MergeInfo &other) const
  {
    return _stricmp(_sourceTex,other._sourceTex);
  }
  
  bool operator==(const MergeInfo &other) const {return CompareWith(other)==0;}
  bool operator>=(const MergeInfo &other) const {return CompareWith(other)>=0;}
  bool operator<=(const MergeInfo &other) const {return CompareWith(other)<=0;}
  bool operator!=(const MergeInfo &other) const {return CompareWith(other)!=0;}
  bool operator>(const MergeInfo &other) const {return CompareWith(other)>0;}
  bool operator<(const MergeInfo &other) const {return CompareWith(other)<0;}


    mutable bool _matchFace;
    mutable bool _matchRvmat;


  };

  struct RVMatCache
  {
    RString _file;
    SRef<ParamFile> _matData;

    RVMatCache(RString name, ParamFile *pf=0):_file(name),_matData(pf) {}
    RVMatCache(const char *name=0):_file(name) {}

  int CompareWith(const RVMatCache &other) const
  {
    return _stricmp(_file,other._file);
  }
  
  bool operator==(const RVMatCache &other) const {return CompareWith(other)==0;}
  bool operator>=(const RVMatCache &other) const {return CompareWith(other)>=0;}
  bool operator<=(const RVMatCache &other) const {return CompareWith(other)<=0;}
  bool operator!=(const RVMatCache &other) const {return CompareWith(other)!=0;}
  bool operator>(const RVMatCache &other) const {return CompareWith(other)>0;}
  bool operator<(const RVMatCache &other) const {return CompareWith(other)<0;}
  };

  BTree<MergeInfo> _mergeInfo;
  mutable BTree<RVMatCache> _rvmatCache;

  LSError LoadMergeInfo();
  LSError LoadAndMergeMaterialStruct(RString src, ParamFile *data, const char *basePath, RString &trg);

  RString ShareMaterialFromCache(RString name, const ParamFile *rvmatData) const;
  RString ShareMaterial(RString name, const ParamFile *rvmatData, const char *basePath) const;
  bool CacheMaterials(const char *search, int basePathCut=0) const;

public:

  LSError LoadPtm(const char *ptmText, size_t size);
  LSError LoadPtm(const char *filename);


//  static LSError SaveNOPtm(LODObject *lodobj, const char *sourcePtm, const char *targetPtm, const char *basePath);


  bool MergeTextures(ObjectData *obj);
  bool MergeMaterialsUsingTextures(ObjectData *obj, const char *basePath);
  
  bool MergeTextures(LODObject *obj);
  bool MergeMaterialsUsingTextures(LODObject *obj, const char *basePath);

  template<class Fn>
  void MatchState(Fn functor) {
      BTreeIterator<MergeInfo> iter(_mergeInfo);  

      MergeInfo *itm ;
      while ((itm = iter.Next())!=0) 
         functor(itm->GetSourceTexture().Data(),itm->_matchFace, itm->_matchRvmat);
  }


  ///Exports PTM created after MergeMaterialsUsingTextures
  /**
  This PTM is useful to create merged normalmaps in TexMerge
  */

};

}