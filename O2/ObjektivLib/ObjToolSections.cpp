#include "common.hpp"
#include ".\objtoolsections.h"

namespace ObjektivLib {


int ObjToolSections::ObjToolSectionInfo::compare(const ObjToolSectionInfo& other) const
{
  int d;
  if (IsAlpha!=other.IsAlpha)       //alfy jsou na konci
    if (other.IsAlpha) return -1;
    else return 1;
  if (IsAlpha)                  //pokud jsou obe alfy
  {
    d=other.alphaId-alphaId;    //patri do stejne sekce jen kdyz maji stejne alphaId
    if (d) return d;
  }
  d=other.ptrMaterial-ptrMaterial;  //porovnej materialy
  if (d) return d;
  d=other.ptrTexture-ptrTexture;  //porovnej textury
  if (d) return d;
  d=other.flags-flags; //porovnej flagy
  if (d) return d;        
  return 0;
}
  
int ObjToolSections::ObjToolSectionInfo::operator=(int zero)
{
  ASSERT(zero==0);    
  return 0;
}

void ObjToolSections::CalculateSections(BTree<ObjToolSections::ObjToolSectionInfo> &list, bool selonly)
{
  int cnt=NFaces();
  const char *lastTex=NULL;
  const char *lastMat=NULL;
  unsigned long lastFlags=0;
  int lastAlphaId=-1;
  for (int i=0;i<cnt;i++) if (!selonly || FaceSelected(i))
  {
    ObjToolSectionInfo nfo;
    FaceT fc(*this,i);

    nfo.ptrMaterial=fc.GetMaterial();
    nfo.ptrTexture=fc.GetTexture();
    const char *texName=Pathname::GetNameFromPath(nfo.ptrTexture);
    int len=strlen(texName);
    nfo.IsAlpha=(len>7 && (strncmp(texName+len-7,"_ca",3)==0)) || (len>3 && _stricmp(texName+len-4,".paa")==0);
    nfo.flags=fc.GetFlags() & ~FACE_COLORIZE_MASK;
    if (lastTex==nfo.ptrTexture && lastMat==nfo.ptrMaterial && lastFlags==nfo.flags)    
      nfo.alphaId=lastAlphaId;
    else
      nfo.alphaId=lastAlphaId=i;
    lastTex=nfo.ptrTexture;
    lastMat=nfo.ptrMaterial;
    lastFlags=nfo.flags;
    nfo.count=1;
    ObjToolSectionInfo *fnd=list.Find(nfo);
    if (fnd) fnd->count++; else list.Add(nfo);    
  }
}

int ObjToolSections::BuildSectionArray(BTree<ObjToolSections::ObjToolSectionInfo> &list, ObjToolSections::ObjToolSectionInfo **array)
{
  ObjToolSectionInfo *temp=list.Smallest();
  int index=0;
  if (temp)
  {
    BTreeIterator<ObjToolSectionInfo> iter(list);
    iter.BeginFrom(*temp);
    while ((temp=iter.Next())!=NULL)
    {
      array[index++]=temp;
    }
  }
  return index;
}

void *ObjToolSections::ObjToolSectionInfo::PackSectionInfo(void *preData, int presize)
{
  int size=strlen(ptrTexture)+1+strlen(ptrMaterial)+1+sizeof(unsigned long)+sizeof(bool)+presize+sizeof(int)+sizeof(int);
  char *block=(char *)malloc(size);
  ostrstream buff(block,size);
  if (presize) buff.write((char *)preData,presize);
  buff.write(ptrTexture,strlen(ptrTexture)+1);
  buff.write(ptrMaterial,strlen(ptrMaterial)+1);
  buff.write((char *)&flags,sizeof(flags));
  buff.write((char *)&IsAlpha,sizeof(IsAlpha));
  buff.write((char *)&alphaId,sizeof(alphaId));
  buff.write((char *)&count,sizeof(count));
  return (void *)block;
}

void ObjToolSections::ObjToolSectionInfo::UnpackSectionInfo(const void *packed, int preDataSize)
{
  const char *block=(const char *)packed;
  block+=preDataSize;
  ptrTexture=block;
  block=strchr(block,0)+1;
  ptrMaterial=block;
  block=strchr(block,0)+1;
  flags=*(unsigned long *)block;
  block+=sizeof(unsigned long);
  IsAlpha=*(bool *)block;
  block+=sizeof(bool);
  alphaId=*(int *)block;  
  block+=sizeof(int);
  count=*(int *)block;
}

void ObjToolSections::SelectSection(void *packedSectionInfo, int preDataSize, bool addsel, Selection *out)
{
  ObjToolSectionInfo nfo;
  nfo.UnpackSectionInfo(packedSectionInfo,preDataSize);
  SelectSection(nfo,addsel,out);
}

void ObjToolSections::SelectSection(const ObjToolSections::ObjToolSectionInfo &nfo, bool addsel, Selection *out)
{
  if (!addsel) 
    if (out)
      out->Clear();
    else
      ClearSelection();
  if (nfo.IsAlpha)
  {
    if (nfo.alphaId>=NFaces()) return;
    int pos=nfo.alphaId;    
    FaceT fc(*this,pos);
    while (pos<NFaces() && _stricmp(fc.GetMaterial(),nfo.ptrMaterial)==0 &&
       _stricmp(fc.GetTexture(),nfo.ptrTexture)==0 && 
       (fc.GetFlags() & ~FACE_COLORIZE_MASK)==nfo.flags)
    {
      if (out)
        {
        out->FaceSelect(pos);
        for (int j=0;j<fc.N();j++) out->PointSelect(fc.GetPoint(j));
        }
      else
        {
        FaceSelect(pos);
        for (int j=0;j<fc.N();j++) PointSelect(fc.GetPoint(j));
        }
       pos++;
       fc.BindTo(this,pos);
    }
  }
  else for (int i=0;i<NFaces();i++) if (!addsel || FaceSelected(i)==false)
  {
    FaceT fc(*this,i);
    if (_stricmp(fc.GetMaterial(),nfo.ptrMaterial)==0 && 
        _stricmp(fc.GetTexture(),nfo.ptrTexture)==0 && 
        (fc.GetFlags() & ~FACE_COLORIZE_MASK)==nfo.flags)
    {
      if (out)
        {
        out->FaceSelect(i);
        for (int j=0;j<fc.N();j++) out->PointSelect(fc.GetPoint(j));
        }
      else
        {
        FaceSelect(i);
        for (int j=0;j<fc.N();j++) PointSelect(fc.GetPoint(j));
        }
    }
  }
}

static ObjectData *sort_obj;

static int CompareFaces(const void *left, const void *right)
{
  int ileft=*(const int *)left;
  int iright=*(const int *)right;
  FaceT fleft(sort_obj,ileft);
  FaceT fright(sort_obj,iright);
  const char *texleft=fleft.GetTexture();
  const char *texright=fright.GetTexture();
  int texll=strlen(texleft);
  int texrl=strlen(texright);
  if (texll>6 && texrl>6)
  {
    int res=-_stricmp(texleft+texll-6,texright+texrl-6);
    if (res!=0) return res;
  }
  return _stricmp(texleft,texright);
}

void ObjToolSections::SortTextures()
{
  sort_obj=this;
  SRef<int> index=new int[NFaces()];
  for (int i=0;i<NFaces();i++) index[i]=i;
  qsort(index,NFaces(),sizeof(int),CompareFaces);
  AutoArray<FaceT> temp;
  temp.Access(NFaces()-1);
  for (int i=0;i<NFaces();i++)  
  {
    FaceT fc(this,index[i]);
    temp[i].CreateUnbound(this);
    fc.CopyFaceTo(temp[i]);
  }
  for (int i=0;i<NFaces();i++)  
  {
    FaceT fc(this,i);
      temp[i].CopyFaceTo(fc);
  }
  for (int j=0;j<MAX_NAMED_SEL;j++) 
  {
    NamedSelection *sel=const_cast<NamedSelection *>(GetNamedSel(j));
    if (sel)
    {
      NamedSelection temp(*sel);
      for (int i=0;i<NFaces();i++)
        temp.FaceSelect(i,sel->FaceSelected(index[i]));
      *sel=temp;
    }
  }
}

}