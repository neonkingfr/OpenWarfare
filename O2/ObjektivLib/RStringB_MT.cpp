#include "common.hpp"
#include <el/MultiThread/CriticalSection.h>
#include ".\rstringb_mt.h"
using namespace MultiThread;

#undef RStringB

static CriticalSection glock;

template<class T>
static const T &InitLocked(const T &ref)
{
  glock.Lock();
  return ref;
}

RStringB_MT::RStringB_MT( RString str ):RStringB(InitLocked(str)) 
{
  glock.Unlock();
}

RStringB_MT::RStringB_MT( RStringS  str ):RStringB(InitLocked(str)) 
{
  glock.Unlock();
}

RStringB_MT::RStringB_MT( const char *str ):RStringB(InitLocked(str)) 
{
  glock.Unlock();
}

RStringB_MT::RStringB_MT( const RStringB_MT &str ):RStringB(InitLocked(str)) 
{
  glock.Unlock();
}

RStringB_MT &RStringB_MT::operator=( const RStringB_MT &src )
{
  glock.Lock();
  RStringB::operator =(src);
  glock.Unlock();
  return *this;
}

bool RStringB_MT::operator == ( const RStringB_MT &with ) const
{
glock.Lock();
bool res=RStringB::operator==(with);
glock.Unlock();
return res;
}
bool RStringB_MT::operator != ( const RStringB_MT &with ) const
{
  glock.Lock();
  bool res=RStringB::operator!=(with);
  glock.Unlock();
  return res;
}

RStringB_MT_DestructUnlock::~RStringB_MT_DestructUnlock()
{
  glock.Unlock();
}

RStringB_MT::~RStringB_MT()
{
  glock.Lock();
}


RStringB_MT &RStringB_MT::operator=( const RString &src )
{
  glock.Lock();
  RStringB::operator =(src);
  glock.Unlock();
  return *this;
}

RStringB_MT &RStringB_MT::operator=( const char *src )
{
  glock.Lock();
  RStringB::operator =(src);
  glock.Unlock();
  return *this;
}
