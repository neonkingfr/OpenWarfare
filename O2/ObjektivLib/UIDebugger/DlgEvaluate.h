#pragma once
#include "afxwin.h"


// DlgEvaluate dialog

#define DBGN_VARIABLECHANGE (WM_APP+105)

class DlgEvaluate : public CDialog
{
	DECLARE_DYNAMIC(DlgEvaluate)

public:
	DlgEvaluate(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgEvaluate();

// Dialog Data
	enum { IDD = IDD_EVALUATE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CEdit wExpression;
  CEdit wVariable;
  CEdit wResult;
  GameState *vGamestate;
  GameVarSpace *vVarSpace;
  CWnd *wNotifyVarChange;
  ScriptDebuggerBase *dbg;

  virtual BOOL OnInitDialog();
protected:
  virtual void OnOK();
public:
  void DialogRules(void);
  afx_msg void OnEnChangeScript();
  CString vExpress;
  CString vVariable;
protected:
  virtual void OnCancel();
public:
  afx_msg void OnBnClickedStop();
  CButton wStop;
  CButton wEvaluate;
};
