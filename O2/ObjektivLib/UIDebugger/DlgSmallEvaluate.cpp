// DlgSmallEvaluate.cpp : implementation file
//

#include "stdafx.h"
#include "UIDebugger.h"
#include "DlgSmallEvaluate.h"
#include ".\dlgsmallevaluate.h"


// DlgSmallEvaluate dialog

IMPLEMENT_DYNAMIC(DlgSmallEvaluate, CDialog)
DlgSmallEvaluate::DlgSmallEvaluate(CWnd* pParent /*=NULL*/)
	: CDialog(DlgSmallEvaluate::IDD, pParent)
    , cmdline(_T(""))
    , varname(_T(""))
{
}

DlgSmallEvaluate::~DlgSmallEvaluate()
{
}

void DlgSmallEvaluate::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_EDIT1, cmdline);
}


BEGIN_MESSAGE_MAP(DlgSmallEvaluate, CDialog)
END_MESSAGE_MAP()


// DlgSmallEvaluate message handlers

BOOL DlgSmallEvaluate::OnInitDialog()
{
  CDialog::OnInitDialog();

  CString title;
  GetWindowText(title);
  title=title+" "+varname;
  SetWindowText(title);

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}
