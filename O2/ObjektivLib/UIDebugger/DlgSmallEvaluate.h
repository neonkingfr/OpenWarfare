#pragma once
#include "afxwin.h"


// DlgSmallEvaluate dialog

class DlgSmallEvaluate : public CDialog
{
	DECLARE_DYNAMIC(DlgSmallEvaluate)

public:
	DlgSmallEvaluate(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgSmallEvaluate();

// Dialog Data
	enum { IDD = IDD_SMALLEVALUATE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CString cmdline;
  CString varname;
  virtual BOOL OnInitDialog();
};
