// DlgDebugger.cpp : implementation file
//

#include "stdafx.h"
#include "UIDebugger.h"
#include "DlgDebugger.h"
#include ".\dlgdebugger.h"
#include "DebuggerSession.h"
#include "DlgSmallEvaluate.h"
#include "../../bredy.libs/archive/archive.h"
#include "../../bredy.libs/archive/ArchiveStreamRegister.h"
#include "ArchiveExtensions.h"


// DlgDebugger dialog

IMPLEMENT_DYNAMIC(DlgDebugger, CDialog)
DlgDebugger::DlgDebugger(CDebuggerSession  *dbgobject, CWnd* pParent /*=NULL*/)
	: CDialog(DlgDebugger::IDD, pParent),_dbgobject(dbgobject)
{
  _breakSign=false;
  _inDebugger=false;
  _termSign=false;
  _splitx=0.5f;
  _splity=0.5f;
  _nonstoprun=false;  
  _runToMode=false;
  _BTWTTtimer=0;
  _cmenutimer=0;
  _cmenudisable=FALSE;
  _hotkeysdisable=false;
  _enrouteError=NULL;
}

DlgDebugger::~DlgDebugger()
{
}

void DlgDebugger::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_CONTEXTTAB, wContextTab);
  DDX_Control(pDX, IDC_SOURCEVIEW, wSourceView);
  DDX_Control(pDX, IDC_WATCH, wWatch);
  DDX_Control(pDX, IDC_AUTOWATCH, wAutowatch);
  DDX_Control(pDX, IDC_HSHIFTBAR, wHShiftBar);
  DDX_Control(pDX, IDC_VSHIFTBAR, wVShiftBar);
}


BEGIN_MESSAGE_MAP(DlgDebugger, CDialog)
  ON_WM_SIZE()
  ON_MESSAGE(DBGM_CONSOLEOUTPUT,OnConsoleOutput)
  ON_MESSAGE(DBGM_QUITDEBUGGER,OnQuitDebugger)
  ON_MESSAGE(DBGM_ONERROR,OnScriptError)
  ON_MESSAGE(DBGM_ONVARCHANGE,OnVarBreak)
  ON_MESSAGE(DBGM_ONTRACE,OnTrace)
  ON_MESSAGE(DBGN_VARIABLECHANGE,OnEvaluateVariableChange)
  ON_MESSAGE(DBGM_TRAYMESSAGE ,OnTrayMessage)
  ON_MESSAGE(DBGM_CANCELBREAK,OnCancelBreak)
  ON_MESSAGE(DBGM_FORCEBREAK,OnForceBreak)
  ON_COMMAND(ID_DEBUGGING_RUN, OnDebuggingRun)
  ON_COMMAND(ID_DEBUGGER_STEPINTO, OnDebuggerStepinto)
  ON_COMMAND(ID_DEBUGGER_STEPOVER, OnDebuggerStepover)
  ON_COMMAND(ID_DEBUGGING_BREAK, OnDebuggingBreak)
  ON_COMMAND(ID_DEBUGGING_STEPOUT, OnDebuggingStepout)
  ON_COMMAND(ID_DEBUGGING_TERMINATE, OnDebuggingTerminate)
  ON_NOTIFY(TCN_SELCHANGE, IDC_CONTEXTTAB, OnTcnSelchangeContexttab)
  ON_NOTIFY(NM_DBLCLK, IDC_AUTOWATCH, OnNMDblclkAutowatch)
  ON_COMMAND(ID_WATCH_SETVARIABLEVALUE, OnWatchSetvariablevalue)
  ON_NOTIFY(LVN_ENDLABELEDIT, IDC_WATCH, OnLvnEndlabeleditWatch)
  ON_NOTIFY(NM_DBLCLK, IDC_WATCH, OnNMDblclkWatch)
  ON_WM_INITMENUPOPUP()
  ON_UPDATE_COMMAND_UI(ID_DEBUGGING_BREAK, OnUpdateDebuggingCommand)
  ON_UPDATE_COMMAND_UI(ID_DEBUGGER_STEPINTO, OnUpdateDebuggingCommand)
  ON_UPDATE_COMMAND_UI(ID_DEBUGGER_STEPOVER, OnUpdateDebuggingCommand)
  ON_UPDATE_COMMAND_UI(ID_DEBUGGING_BREAK, OnUpdateDebuggingCommand)
  ON_UPDATE_COMMAND_UI(ID_DEBUGGING_STEPOUT, OnUpdateDebuggingCommand)
  ON_UPDATE_COMMAND_UI(ID_DEBUGGING_TERMINATE, OnUpdateDebuggingCommand)
  ON_UPDATE_COMMAND_UI(ID_DEBUGGING_RUN, OnUpdateDebuggingCommand)
  ON_UPDATE_COMMAND_UI(ID_DEBUGGING_RUNTO, OnUpdateDebuggingCommand)
  ON_UPDATE_COMMAND_UI(ID_DEBUGGING_RUNNON, OnUpdateDebuggingCommand)
  ON_UPDATE_COMMAND_UI(ID_WATCH_SETVARIABLEVALUE, OnUpdateWatchSetvariablevalue)
  ON_COMMAND(ID_WATCH_ADDWATCH, OnWatchAddwatch)
  ON_UPDATE_COMMAND_UI(ID_WATCH_ADDWATCH, OnUpdateWatchAddwatch)
  ON_COMMAND(ID_WATCH_EDITWATCH, OnWatchEditwatch)
  ON_UPDATE_COMMAND_UI(ID_WATCH_EDITWATCH, OnUpdateWatchEditwatch)
  ON_COMMAND(ID_WATCH_DELETEWATCH, OnWatchDeletewatch)
  ON_UPDATE_COMMAND_UI(ID_WATCH_DELETEWATCH, OnUpdateWatchDeletewatch)
  ON_STN_CLICKED(IDC_HSHIFTBAR, OnStnClickedHshiftbar)
  ON_STN_CLICKED(IDC_VSHIFTBAR, OnStnClickedVshiftbar)
  ON_WM_LBUTTONUP()
  ON_WM_MOUSEMOVE()
  ON_COMMAND(ID_DEBUGGING_RUNNON, OnDebuggingRunnon)
  ON_COMMAND(ID_WATCH_WATCHBREAKPOINTS, OnWatchWatchbreakpoints)
  ON_NOTIFY(NM_RDBLCLK, IDC_AUTOWATCH, OnNMRdblclkAutowatch)
  ON_NOTIFY(NM_RDBLCLK, IDC_WATCH, OnNMRdblclkWatch)
  ON_COMMAND(ID_DEBUGGING_RUNTO, OnDebuggingRunto)
  ON_WM_TIMER()
  ON_COMMAND(ID_DEBUGGER_SETSOURCEPOSITION, OnDebuggerSetsourceposition)
  ON_WM_CONTEXTMENU()
  ON_UPDATE_COMMAND_UI(ID_DEBUGGER_SETSOURCEPOSITION, OnUpdateDebuggerSetsourceposition)
  ON_NOTIFY(LVN_BEGINLABELEDIT, IDC_WATCH, OnLvnBeginlabeleditWatch)
  ON_WM_DESTROY()
  ON_COMMAND(ID_WATCH_DETECTUNDECLARED, OnWatchDetectundeclared)
  ON_UPDATE_COMMAND_UI(ID_WATCH_DETECTUNDECLARED, OnUpdateWatchDetectundeclared)
END_MESSAGE_MAP()


// DlgDebugger message handlers

void DlgDebugger::OnSize(UINT nType, int cx, int cy)
{
  CDialog::OnSize(nType, cx, cy);
  RecalcLayout();
}

void DlgDebugger::RecalcLayout(void)
{
CRect wrect,butt;
GetClientRect(&wrect);
GetDlgItem(ID_DEBUGGING_BREAK)->GetWindowRect(&butt);
CSize bsz=butt.Size();
wrect.top=bsz.cy+20;
int midy=toInt((wrect.bottom-wrect.top)*_splity+wrect.top);
int midx=toInt(wrect.left+(wrect.right-wrect.left)*_splitx);
CRect viewrc(5,wrect.top+25,wrect.right-5,midy-5);
CRect watchrc(5,midy+5,midx-5,wrect.bottom-5);
CRect awatchrc(midx+5,midy+5,wrect.right-5,wrect.bottom-5);
HDWP hdwp=BeginDeferWindowPos(8);
DeferWindowPos(hdwp,wContextTab,NULL,wrect.left,wrect.top,wrect.Size().cx,wrect.Size().cy,SWP_NOZORDER);
DeferWindowPos(hdwp,wSourceView,NULL,viewrc.left,viewrc.top,viewrc.Size().cx,viewrc.Size().cy,SWP_NOZORDER);
DeferWindowPos(hdwp,wWatch,NULL,watchrc.left,watchrc.top,watchrc.Size().cx,watchrc.Size().cy,SWP_NOZORDER);
DeferWindowPos(hdwp,wAutowatch,NULL,awatchrc.left,awatchrc.top,awatchrc.Size().cx,awatchrc.Size().cy,SWP_NOZORDER);
DeferWindowPos(hdwp,wVShiftBar,NULL,midx-3,awatchrc.top,6,awatchrc.Size().cy,SWP_NOZORDER);
DeferWindowPos(hdwp,wHShiftBar,NULL,viewrc.left,midy-3,viewrc.Size().cx,6,SWP_NOZORDER);
EndDeferWindowPos(hdwp);
}

BOOL DlgDebugger::OnInitDialog()
{
  CDialog::OnInitDialog();

  RecalcLayout();  
  wWatch.InsertColumn(0,StrRes[IDS_WATCHTITLE],LVCFMT_LEFT,75,0);
  wWatch.InsertColumn(1,StrRes[IDS_WATCHTYPE],LVCFMT_LEFT,75,1);
  wWatch.InsertColumn(2,"",LVCFMT_LEFT,300,2);
  wWatch.SetImageList(&theApp._watchico,LVSIL_SMALL);
  wWatch.SetImageList(&theApp._watchstate,LVSIL_STATE);
  ListView_SetExtendedListViewStyleEx(wWatch,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES ,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
  wAutowatch.InsertColumn(0,StrRes[IDS_AUTOWATCHTITLE],LVCFMT_LEFT,75,0);
  wAutowatch.InsertColumn(1,StrRes[IDS_WATCHTYPE],LVCFMT_LEFT,75,1);
  wAutowatch.InsertColumn(2,"",LVCFMT_LEFT,300,2);
  wAutowatch.SetImageList(&theApp._watchico,LVSIL_SMALL);
  ListView_SetExtendedListViewStyleEx(wAutowatch,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES ,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
  if (fixfont.m_hObject==NULL)
    fixfont.CreatePointFont(12,"Courier");
  wSourceView.SetFont(&fixfont,TRUE);
  wContextTab.InsertItem(0,StrRes[IDS_OUTPUTTITLE]);
  MinimizeTray();
  DebuggerStateChange(false);
  _hotkeys=::LoadAccelerators(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDR_ACCELERATOR1));
 

  return FALSE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}


static bool IsLarge(const GameArrayType &arr, int level)
{
  if (level>10) return true;
  if (arr.Size()>100) return true;
  for (int i=0;i<arr.Size();i++)
    if (arr[i].GetType()==GameArray)
    {
      const GameArrayType &arr2=arr[i];
      if (IsLarge(arr2,level+1)) return true;
    }
  return false;
}

static RString GetValueStr(const GameValue &val)
{
  if (val.GetType()==GameArray)
  {
    const GameArrayType &arr=val;
    if (IsLarge(arr,0)) return "[<large array>]";
    return val.GetText();
  }
  else
    return val.GetText();
}

void DlgDebugger::ConsoleOutput(const char *text)
{  
  SendMessage(DBGM_CONSOLEOUTPUT,0,(LPARAM)text);
}

LRESULT DlgDebugger::OnConsoleOutput(WPARAM wParam, LPARAM lParam)
{
  if (_curTab!=0)
  {
    wContextTab.SetCurSel(_curTab=0);
    UpdateSourceView();
  }
  const char *text=(const char *)lParam;
  char *textcopy=strcpy((char *)alloca(strlen(text)+1),text);
  if (GetQueueStatus(QS_ALLINPUT)) AfxPumpMessage();
  if (InSendMessage() )  ReplyMessage(0);
  int sz=wSourceView.GetWindowTextLength()+strlen(textcopy);
  if (sz>30000)  
  {
    wSourceView.SetSel(0,3000);
    wSourceView.ReplaceSel("");
  }  
  int len=wSourceView.GetWindowTextLength();
  wSourceView.SetSel(len,len);
  wSourceView.ReplaceSel(textcopy);
  wSourceView.ReplaceSel("\r\n");
  wSourceView.GetWindowText(_dbgOutput);
 return 0;
}

LRESULT DlgDebugger::OnQuitDebugger(WPARAM wParam, LPARAM lParam)
{
  SaveState();
  DestroyWindow();
  PostQuitMessage(0);
  ::Shell_NotifyIcon(NIM_DELETE,&_trayicon);
  return 0;
}

void DlgDebugger::SwitchToDebugger(UINT msg, WPARAM wParam, LPARAM lParam)
{  
  if (_termSign) 
  {
    _dbgobject->SetTraceMode(_dbgobject->traceTerminate);
    _termSign=false;
  }
  else
  {
    _debuggerLock.Lock();  
    _waitingThread=GetCurrentThreadId();
    SendMessage(msg,wParam,lParam);
    _debuggerWait.Lock();
    _debuggerWait.Unlock();
    _debuggerLock.Unlock();
  }
}

void DlgDebugger::ReleaseProgram()
{
  _breakSign=false;
  _termSign=false;
  DebuggerStateChange(false);
  _BTWTTtimer=SetTimer(1000,1000,NULL);
  _debuggerWait.SetEvent();
}

LRESULT DlgDebugger::OnScriptError(WPARAM wParam, LPARAM lParam)
{
  if (!_inDebugger) 
    DebuggerStateChange(true);
  CString errorinfo;
  AfxFormatString1(errorinfo,IDS_DEBUGSCRIPTERROR,(const char *)lParam);
  if (_enrouteError)
  {
    *_enrouteError=errorinfo;
    _dbgobject->SetTraceMode(_dbgobject->traceTerminate);
  }
  else
    AfxMessageBox(errorinfo,MB_ICONSTOP);
  return 0;
}

#define ENABLE_DLG_CONTROL(id,enable) GetDlgItem(id)->EnableWindow(enable)

void DlgDebugger::DebuggerStateChange(bool state)
{
  _inDebugger=state;
  ENABLE_DLG_CONTROL(ID_DEBUGGER_STEPINTO,state);
  ENABLE_DLG_CONTROL(ID_DEBUGGER_STEPOVER,state);
  ENABLE_DLG_CONTROL(ID_DEBUGGING_STEPOUT,state);
  ENABLE_DLG_CONTROL(ID_DEBUGGING_RUNTO,state);
  ENABLE_DLG_CONTROL(ID_DEBUGGING_RUN,state);
  ENABLE_DLG_CONTROL(ID_DEBUGGING_RUNNON,state);
  ENABLE_DLG_CONTROL(ID_DEBUGGING_BREAK,!state);
  wWatch.EnableWindow(state);
  wAutowatch.EnableWindow(state);  
  if (state)
  {
    if (!IsWindowVisible()) RestoreTray();   
    SetWindowPos(&wndTopMost,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
    SetWindowPos(&wndNoTopMost,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
    UpdateTabs();    
    Invalidate(true);
    _runToMode=false;
    if (_BTWTTtimer)
    {
      KillTimer(_BTWTTtimer);
      _BTWTTtimer=0;
    }
    if (_dlgbreakwait.GetSafeHwnd())
      _dlgbreakwait.DestroyWindow();
  }  
}
void DlgDebugger::OnDebuggingRun()
{
  if (_inDebugger) _dbgobject->SetTraceMode(_dbgobject->traceRun);
  ReleaseProgram();
  MinimizeTray();
}

void DlgDebugger::OnDebuggerStepinto()
{
  _dbgobject->SetTraceMode(_dbgobject->traceBreakNext);
  ReleaseProgram();
}

void DlgDebugger::OnDebuggerStepover()
{
  _dbgobject->SetTraceMode(_dbgobject->traceStepOver);
  ReleaseProgram();
}

void DlgDebugger::OnDebuggingBreak()
{
  BreakScript();
}

void DlgDebugger::OnDebuggingStepout()
{
  _dbgobject->SetTraceMode(_dbgobject->traceStepOut);
  ReleaseProgram();
}

void DlgDebugger::OnDebuggingTerminate()
{ 
  if (_inDebugger)  
  {
     _dbgobject->SetTraceMode(_dbgobject->traceTerminate);
    ReleaseProgram();
  }
  else
  {
    TerminateScript();
  }
}
void DlgDebugger::OnOK()
{
 
}

void DlgDebugger::OnCancel()
{
  if (_inDebugger)  
    OnDebuggingRun();
  else
    MinimizeTray();
}

void DlgDebugger::UpdateTabs(void)
{
  int needTabs=_dbgobject->_context._curContext;
  int curTabs=wContextTab.GetItemCount()-1;
  if (curTabs<needTabs)
  {
    for (int i=curTabs;i<needTabs;i++)
    {
      int pos=wContextTab.InsertItem(1,"",0);      
    }
  }
  else
  {
    while (curTabs>needTabs)
    {
      wContextTab.DeleteItem(1);
      curTabs--;
    }
  }
  wContextTab.SetCurSel(1);
  _curTab=1;
  _maxTab=needTabs+1;
  for (int i=1;i<_maxTab;i++)
  {
    GameVarSpace *spc=_dbgobject->GetLocalVariables(_maxTab-i);
    RString name=spc->_scopeName;
    int scplvl=1;
    while  (name[0]==0 && spc) 
    {
      spc=const_cast<GameVarSpace *>(spc->_parent);
      if (spc) name=spc->_scopeName;
      scplvl++;
    }
    char title[50];
    if (name[0])    
      snprintf(title,50,"(%d) %s",scplvl,name.Data());         
    else          
      sprintf(title,StrRes[IDS_LEVELTITLE],_maxTab-i);    
    TCITEM item;
    item.mask=TCIF_TEXT;
    item.pszText=title;    
    wContextTab.SetItem(i,&item);
  }  
  UpdateSourceView();
}

void DlgDebugger::OnTcnSelchangeContexttab(NMHDR *pNMHDR, LRESULT *pResult)
{
  _curTab=wContextTab.GetCurSel();
  UpdateSourceView();
}

static int NLCorrectPos(const char *testtxt, int index)
{
  int newindex=index;
  for (int i=0;i<index && testtxt[i];i++)
    if (testtxt[i]=='\n') newindex++;
  return newindex;
}

static void NLCorrectText(const char *testtxt, char *outtext)
{
  while (*testtxt)
  {
    if (*testtxt=='\n') *outtext++='\r';
    *outtext++=*testtxt++;
  }
  *outtext++=*testtxt++;
}

void DlgDebugger::UpdateSourceView(void)
{
  if (_curTab==0)
  {
    wSourceView.SetWindowText(_dbgOutput);
    wSourceView.SetSel(_dbgOutput.GetLength(),_dbgOutput.GetLength());
  }
  else
  {
    if (_inDebugger)
    {
      const char *pos0=_dbgobject->GetCurrentExpression(_maxTab-_curTab);
      const char *pos=_dbgobject->GetCurrentPosition(_maxTab-_curTab);
      if (pos==NULL) pos=pos0;
      if (pos0==NULL)
      {
        wSourceView.SetWindowText(StrRes[IDS_UNABLETOGETSOURCEPOSITION]);
      }
      else
      {
        int newstrsize=NLCorrectPos(pos0,strlen(pos0))+10;
        int index=pos-pos0; //index=NLCorrectPos(pos0,index);
        char *newstr=new char[newstrsize];

        NLCorrectText(pos0,newstr);
        wSourceView.SetWindowText(newstr);
        delete [] newstr;

        while (isspace(*pos)) 
        {
          if (*pos++=='\n') return;        
        }
        int line=wSourceView.LineFromChar(index);
        int lineend=wSourceView.LineIndex(line+1);
        if (lineend==-1) lineend=wSourceView.GetWindowTextLength();
        wSourceView.SetSel(index,lineend);
        CHARFORMAT2 cf;
        cf.cbSize=sizeof(cf);
        cf.dwMask=CFM_BOLD|CFM_UNDERLINE;
        cf.dwEffects=CFE_BOLD|CFE_UNDERLINE;
        wSourceView.SetSelectionCharFormat(cf);
        
        int fline=wSourceView.GetFirstVisibleLine();
        if (fline>=line) wSourceView.LineScroll(-4);
  /*      CRect rc;
        wSourceView.GetClientRect(&rc);
        int lline=wSourceView.LineFromChar(wSourceView.CharFromPos(CPoint(rc.left,rc.bottom)));
        wSourceView.LineScroll(line-fline-(lline-fline)/2);*/
      }
    }
    else
    {
      wSourceView.SetWindowText(StrRes[IDS_LEVELNOTAVAILABLE]);
    }
  UpdateAutowatchVariableList();
  UpdateWatchVariableList();
  }
}

static bool IsValidVar(const char *name)
{
  if (isalpha(*name) || *name=='_')
  {
    name++;
    while (*name)
    {
      if (!(isalpha(*name) || isdigit(*name) || *name=='_')) return false;
      name++;
    }
    return true;
  }
  return false;
}

LRESULT DlgDebugger::OnTrace(WPARAM wParam, LPARAM lParam)
{
  DebuggerStateChange(true);
  if (GetRunNonstop()) ReleaseProgram();
  return 0;
}

struct VarInfo
{
  RString varname;
  int level;
  bool expanded;
  GameVarSpace *space;
};

TypeIsSimple(VarInfo);

template <>
void BTreeUnsetItem(CString &item)
  {
  item="";
  }

static const int valTextMaxLen = 300;
class FillVarArrayFunctor
{
public:
  BTree<CString> *varused;
  const VarBankType *vars;
  CListCtrl *list;
  DlgDebugger *self;
  bool operator()(const GameVariable &variable,const VarBankType *container)
  {
    CString varname=(const char *)variable._name;
    CString *found=varused->Find(varname);
    if (found==NULL)
    {
      CString valtext=(const char *)GetValueStr(variable._value);
      if (valtext.GetLength()>valTextMaxLen) valtext=valtext.Mid(0,valTextMaxLen-3)+"...";
      int ls=list->InsertItem(0,varname,0);
      list->SetItemText(ls,1,self->GetTypeName(variable._value.GetType()));
      list->SetItemText(ls,2,valtext);
      list->SetItemData(ls,variable._value.GetType());
      list->SetItem(ls,0,LVIF_IMAGE,NULL,variable._value.GetType()==GameArray?1:0,0,0,0,0);
      varused->Add(varname);
    }
    return false;
  }
};

class GameValueCString:public CString
{
public:
  GameValue value;
  GameValueCString operator=(const char *text){CString::operator =(text);return *this;}
};

template <>
void BTreeUnsetItem(GameValueCString &item)
  {
  item="";
  }


static bool CheckIsArrayDef(const char *varname,CString& subvarname)
{
  const char *p=varname;
  const char *subend;
  while (*p)
  {
    if (*p=='\\')
    {
      subend=p;
      while (*p && *p=='\\' || isdigit(*p)) *p++;
      if (*p==0)
      {
        int size=subend-varname;
        char *buff=subvarname.GetBuffer(size);
        strncpy(buff,varname,size);
        subvarname.ReleaseBuffer(size);
        return true;
      }
    }
    p++;
  }
subvarname=varname;
return false;
}



static GameValue ExtractFromArrayDef(const char *varname,int namepartsz, GameValue value)
{
  varname+=namepartsz;
  while (*varname)
  {
    if (*varname=='\\') varname++;
    else if (isdigit(*varname))
    {
      if (value.GetType()!=GameArray) return value;
      int index=atoi(varname);      
      const GameArrayType &arr=value;
      if (index<0 || index>=arr.Size()) return GameValue();
      value=arr[index];
      while (isdigit(*varname)) varname++;
    }
    else return GameValue();  
  }
return value;
}


static bool EvaluateWatch(const char *varname,const GameVarSpace *space, GameState *state, BTree<GameValueCString> *optim_var_list, GameValue &value)
{    
    bool exists=space->VarGet(varname,value);
    if (!exists && state->GetGlobalVariables())
      exists=state->GetGlobalVariables()->VarGet(varname,value);
    if (!exists)
    {
      GameValueCString subvarname,*found;
      bool isarray=CheckIsArrayDef(varname,subvarname);
      found=optim_var_list?optim_var_list->Find(subvarname):NULL;
      if (found==NULL)
      {    
        found=&subvarname;
        GameVarSpace context(const_cast<GameVarSpace *>(space));
        state->BeginContext(&context);
        found->value=state->Evaluate(subvarname);
        state->EndContext();
        if (optim_var_list) optim_var_list->Add(*found);
      }
      if (isarray)
      {
        value=ExtractFromArrayDef(varname,subvarname.GetLength(),found->value);
      }
      else
        value=found->value;
      if (value.GetNil()) return false;
    }
  return true;
}



void DlgDebugger::UpdateAutowatchVariableList(void)
{
  if (_curTab==0 || !_inDebugger) return;
  int level=_maxTab-_curTab;
  const GameVarSpace *space=_dbgobject->GetLocalVariables(level);
  BTree<CString> usedVars;

  int cnt=wAutowatch.GetItemCount();
  CString varname;
  CString valtext;
  for (int i=0;i<cnt;i++)
  {
    CString gameError;
    _enrouteError=&gameError;
    varname=wAutowatch.GetItemText(i,0);
    GameValue value;
    bool exists=EvaluateWatch(varname,space,_dbgobject->_context._script,NULL,value);
    if (!exists)
    {
      wAutowatch.DeleteItem(i);
      cnt--;
      i--;
    }
    else
    {
      valtext=GetValueStr(value);
      if (valtext.GetLength()>valTextMaxLen) valtext=valtext.Mid(0,valTextMaxLen-3)+"...";
      wAutowatch.SetItemText(i,2,valtext);
      wAutowatch.SetItemText(i,1,value.GetNil()?"":GetTypeName(value.GetData()->GetType()));
// TODO: scripting types are not longer identified by a number
/*
      if (wAutowatch.GetItemData(i)!=value.GetType())
      {
        wAutowatch.SetItem(i,0,LVIF_IMAGE,NULL,value.GetType()==GameArray?1:0,0,0,0,0);
        wAutowatch.SetItemData(i,value.GetType());
      }
*/
      usedVars.Add(varname);
    }
    _enrouteError=0;
  }
  while (space)
  {
    FillVarArrayFunctor filvar;
    filvar.self=this;
    filvar.list=&wAutowatch;
    filvar.vars=&space->_vars;
    filvar.varused=&usedVars;
    filvar.vars->ForEachF(filvar);
    space=space->_parent;
  }
  if (_dbgobject->_context._script && _dbgobject->_context._script->GetGlobalVariables())
  {    
    FillVarArrayFunctor filvar;
    filvar.self=this;
    filvar.list=&wAutowatch;
    // TODO: different access to global variable space
    filvar.vars = &_dbgobject->_context._script->GetGlobalVariables()->GetVariables();
    filvar.varused=&usedVars;
    filvar.vars->ForEachF(filvar);
  }
}

static void CloseUpItem(CListCtrl &list, CString varname, int index)
{
  int cnt=list.GetItemCount();
  varname+="\\";
  CString subvarname;
  for (int i=index+1;i<cnt;i++)
  {
     subvarname=list.GetItemText(i,0);
     if (strncmp(subvarname,varname,varname.GetLength())==0)
     {
       list.DeleteItem(i);
       i--;
       cnt--;
     }
     else
       break;    
  }
}


void DlgDebugger::UpdateWatchVariableList(void)
{
  if (_curTab==0 || !_inDebugger) return;
  int level=_maxTab-_curTab;  
  const GameVarSpace *space=_dbgobject->GetLocalVariables(level);
  BTree<GameValueCString> optim_var_list;
  
  int cnt=wWatch.GetItemCount();
  CString varname;
  CString valtext;
  bool wasSpace=false;
  int i;
  for (i=0;i<cnt;i++)
  {
    varname=wWatch.GetItemText(i,0);
    if (varname=="")  
    {
      wasSpace=true;
       wWatch.SetItemState(i,INDEXTOSTATEIMAGEMASK(1),LVIS_STATEIMAGEMASK);
    }
    else
    {
      GameValue value;
      CString gameError;
      _enrouteError=&gameError;
      bool exists=EvaluateWatch(varname,space,_dbgobject->_context._script,&optim_var_list,value);
      if (exists)  
      {
        valtext=GetValueStr(value);
        if (valtext.GetLength()>128) valtext=valtext.Mid(0,128);
        wWatch.SetItemText(i,2,valtext);
        wWatch.SetItemText(i,1,value.GetNil()?"":GetTypeName(value.GetData()->GetType()));      
// TODO: scripting types are not longer identified by a number
/*
        if (wWatch.GetItemData(i)!=value.GetType())
        {
          wWatch.SetItem(i,0,LVIF_IMAGE,NULL,value.GetType()==GameArray?1:0,0,0,0,0);
          wWatch.SetItemData(i,value.GetType());
          CloseUpItem(wWatch,varname,i);
          cnt=wWatch.GetItemCount();
        }      
*/
      }
      else
      {
        wWatch.SetItemText(i,1,"");
        wWatch.SetItemText(i,2,gameError.GetLength()==0?StrRes[IDS_VARUNAVAILABLE]:"<"+gameError+">");
      }
      _enrouteError=NULL;
      if (_dbgobject->IsVarBreakpoint(varname))
        wWatch.SetItemState(i,INDEXTOSTATEIMAGEMASK(2),LVIS_STATEIMAGEMASK);
      else
        wWatch.SetItemState(i,INDEXTOSTATEIMAGEMASK(1),LVIS_STATEIMAGEMASK);
    }
  }
  if (!wasSpace)
  {
    wWatch.InsertItem(i,"",0);
  }
}


void DlgDebugger::OnWatchSetvariablevalue()
{  
  if (GetFocus()==&wWatch || GetFocus()==&wAutowatch)
  {
    CListCtrl &list=*(CListCtrl *)GetFocus();
    int i=list.GetNextItem(-1,LVNI_FOCUSED);
    if (i!=-1)    
      evalDlg.vVariable=list.GetItemText(i,0);    
  }
  evalDlg.vGamestate=_dbgobject->_context._script;
  int level=_maxTab-_curTab;
  evalDlg.vVarSpace=_dbgobject->GetLocalVariables(level);
  evalDlg.wNotifyVarChange=this;
  evalDlg.dbg=_dbgobject;
  evalDlg.DoModal();
}

LRESULT DlgDebugger::OnEvaluateVariableChange(WPARAM wParam, LPARAM lParam)
{
  UpdateAutowatchVariableList();
  UpdateWatchVariableList();
  return 0;
}

void DlgDebugger::OnLvnEndlabeleditWatch(NMHDR *pNMHDR, LRESULT *pResult)
{
  NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
  _hotkeysdisable=FALSE;
  if (pDispInfo->item.pszText==NULL) return;
  CString oldvar=wWatch.GetItemText(pDispInfo->item.iItem,0);
  if (oldvar.GetLength()!=0) CloseUpItem(wWatch,oldvar,pDispInfo->item.iItem);
  if (!(*pResult = pDispInfo->item.pszText[0]!=0))
  {
    wWatch.DeleteItem(pDispInfo->item.iItem);
  }
  PostMessage(DBGN_VARIABLECHANGE,0,0);  
}

void DlgDebugger::HandleDoubleclickOnList(CListCtrl &list)
{
  if (_curTab==0) return;
  int i=list.GetNextItem(-1,LVNI_FOCUSED);
  CString varname=list.GetItemText(i,0);
  int level=_maxTab-_curTab;
  GameVarSpace *space=_dbgobject->GetLocalVariables(level);
  GameValue value;
  EvaluateWatch(varname,space,_dbgobject->_context._script,NULL,value);
  if (value.GetType()==GameArray)
  {
    LVITEM item;
    item.mask=LVIF_IMAGE|LVIF_INDENT;
    item.iItem=i;
    item.iSubItem=0;
//    if ((item.mask & LVIF_INDENT)==0) item.iIndent=0;
    list.GetItem(&item);    
    if (item.iImage==2)
    {
      CloseUpItem(list,varname,i);
      list.SetItem(i,0,LVIF_IMAGE,NULL,1,0,0,0,0);
    }
    else
    {
      const GameArrayType &arr=value;    
      CString newname;
      int pos=i;
      for (i=0;i<arr.Size();i++)
      {      
        newname.Format("%s\\%d",varname,i);
        LVITEM itm;
        itm.mask=LVIF_TEXT|LVIF_INDENT;
        itm.cchTextMax=0;
        itm.iImage=0;
        itm.iIndent=item.iIndent+1;
        itm.iSubItem=0;
        itm.pszText=newname.LockBuffer();
        itm.iItem=pos+i+1;
        list.InsertItem(&itm);
        newname.UnlockBuffer();
      }
    list.SetItem(pos,0,LVIF_IMAGE,NULL,2,0,0,0,0);
    }
  }
  else if (varname.Trim().GetLength()!=0)
  {
    DlgSmallEvaluate dlg;
    GameValue val;
    dlg.varname=varname;
    dlg.cmdline=GetValueStr(value); 

    do
    {
      if (dlg.DoModal()==IDOK && dlg.cmdline.Trim().GetLength()!=0)
      {
        GameVarSpace space(_dbgobject->_context._script->GetContext());
        _dbgobject->_context._script->BeginContext(&space);
        val=_dbgobject->_context._script->Evaluate(dlg.cmdline);
        _dbgobject->_context._script->EndContext();        
        if (val.GetNil())
          AfxMessageBox(IDS_ERRORINEXPRESSION,MB_ICONSTOP);
      }
      else
        return;
    }
    while (val.GetNil());
    int p=varname.ReverseFind('\\');
    if (p!=-1)
    {
       if (EvaluateWatch(varname.Mid(0,p),space,_dbgobject->_context._script,NULL,value))
       {
         CString cmd;
         cmd=varname.Mid(0,p)+" set ["+((LPCTSTR)varname+p+1)+","+dlg.cmdline+"]";
        GameVarSpace space(_dbgobject->_context._script->GetContext());
        _dbgobject->_context._script->BeginContext(&space);
        _dbgobject->_context._script->Evaluate(cmd);
        _dbgobject->_context._script->EndContext();        
       }
    }
    else
      space->VarSet(varname,val,false);
  }
UpdateAutowatchVariableList();
UpdateWatchVariableList();
}

void DlgDebugger::OnNMDblclkWatch(NMHDR *pNMHDR, LRESULT *pResult)
{
  HandleDoubleclickOnList(wWatch);  
  *pResult = 0;
}

void DlgDebugger::OnNMDblclkAutowatch(NMHDR *pNMHDR, LRESULT *pResult)
{
  HandleDoubleclickOnList(wAutowatch);
  *pResult = 0;
}

void DlgDebugger::MinimizeTray(void)
{
  _trayicon.cbSize=sizeof(_trayicon);
  _trayicon.hWnd=*this;
  _trayicon.uID=0;
  _trayicon.uFlags=NIF_ICON|NIF_MESSAGE|NIF_TIP;
  _trayicon.uCallbackMessage=DBGM_TRAYMESSAGE ;
  _trayicon.hIcon=theApp.LoadIcon(IDR_TRAYICON);
  strncpy(_trayicon.szTip,StrRes[IDS_TRAYTEXT],sizeof(_trayicon.szTip));
  if (::Shell_NotifyIcon(NIM_MODIFY,&_trayicon)==FALSE)
    ::Shell_NotifyIcon(NIM_ADD,&_trayicon);
  ShowWindow(SW_HIDE);
}

void DlgDebugger::RestoreTray(void)
{
  ::Shell_NotifyIcon(NIM_DELETE,&_trayicon);
  ShowWindow(SW_SHOW);
  if (IsIconic()) OpenIcon();
}

LRESULT DlgDebugger::OnTrayMessage(WPARAM wParam, LPARAM lParam)
{
  if (lParam==WM_LBUTTONDBLCLK)
  {
    OnDebuggingBreak();
    RestoreTray();
  }
  return 0;
}

void DlgDebugger::OnInitMenuPopup(CMenu* pMenu, UINT nIndex, BOOL bSysMenu)
  {
	if (bSysMenu)
		return;     // don't support system menu

    ASSERT(pMenu != NULL);
	
	// check the enabled state of various menu items

	CCmdUI state;
	state.m_pMenu = pMenu;
	ASSERT(state.m_pOther == NULL);
	ASSERT(state.m_pParentMenu == NULL);

	// determine if menu is popup in top-level menu and set m_pOther to
	//  it if so (m_pParentMenu == NULL indicates that it is secondary popup)
	HMENU hParentMenu;
	if (AfxGetThreadState()->m_hTrackingMenu == pMenu->m_hMenu)
		state.m_pParentMenu = pMenu;    // parent == child for tracking popup
	else if ((hParentMenu = ::GetMenu(m_hWnd)) != NULL)
	{
		CWnd* pParent = GetTopLevelParent();
			// child windows don't have menus -- need to go to the top!
		if (pParent != NULL &&
			(hParentMenu = ::GetMenu(pParent->m_hWnd)) != NULL)
		{
			int nIndexMax = ::GetMenuItemCount(hParentMenu);
			for (int nIndex = 0; nIndex < nIndexMax; nIndex++)
			{
				if (::GetSubMenu(hParentMenu, nIndex) == pMenu->m_hMenu)
				{
					// when popup is found, m_pParentMenu is containing menu
					state.m_pParentMenu = CMenu::FromHandle(hParentMenu);
					break;
				}
			}
		}
	}

	state.m_nIndexMax = pMenu->GetMenuItemCount();
	for (state.m_nIndex = 0; state.m_nIndex < state.m_nIndexMax;
	  state.m_nIndex++)
	{
		state.m_nID = pMenu->GetMenuItemID(state.m_nIndex);
		if (state.m_nID == 0)
			continue; // menu separator or invalid cmd - ignore it

		ASSERT(state.m_pOther == NULL);
		ASSERT(state.m_pMenu != NULL);
		if (state.m_nID == (UINT)-1)
		{
			// possibly a popup menu, route to first item of that popup
			state.m_pSubMenu = pMenu->GetSubMenu(state.m_nIndex);
			if (state.m_pSubMenu == NULL ||
				(state.m_nID = state.m_pSubMenu->GetMenuItemID(0)) == 0 ||
				state.m_nID == (UINT)-1)
			{
				continue;       // first item of popup can't be routed to
			}
			state.DoUpdate(this, FALSE);    // popups are never auto disabled
		}
		else
		{
			// normal menu item
			// Auto enable/disable if frame window has 'm_bAutoMenuEnable'
			//    set and command is _not_ a system command.
			state.m_pSubMenu = NULL;
			state.DoUpdate(this, true);
		}

		// adjust for menu deletions and additions
		UINT nCount = pMenu->GetMenuItemCount();
		if (nCount < state.m_nIndexMax)
		{
			state.m_nIndex -= (state.m_nIndexMax - nCount);
			while (state.m_nIndex < nCount &&
				pMenu->GetMenuItemID(state.m_nIndex) == state.m_nID)
			{
				state.m_nIndex++;
			}
		}
		state.m_nIndexMax = nCount;
	}
  }

void DlgDebugger::OnUpdateDebuggingCommand(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(GetDlgItem(pCmdUI->m_nID)->IsWindowEnabled());
}

void DlgDebugger::OnUpdateWatchSetvariablevalue(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_inDebugger && _curTab!=0);
}

void DlgDebugger::OnWatchAddwatch()
{
  wWatch.SetFocus();
  wWatch.EditLabel(wWatch.GetItemCount()-1);
}

void DlgDebugger::OnUpdateWatchAddwatch(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(wWatch.IsWindowEnabled());
}

void DlgDebugger::OnWatchEditwatch()
{
  wWatch.SetFocus();
  int cursel=wWatch.GetNextItem(-1,LVNI_FOCUSED);
  if (cursel!=-1)
    wWatch.EditLabel(cursel);
}

void DlgDebugger::OnUpdateWatchEditwatch(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(wWatch.IsWindowEnabled() && wWatch.GetNextItem(-1,LVNI_FOCUSED)!=-1);
}

void DlgDebugger::OnWatchDeletewatch()
{
  int cursel=wWatch.GetNextItem(-1,LVNI_SELECTED);
  while (cursel!=-1)
  {
    CString varname=wWatch.GetItemText(cursel,0);
    if (varname.GetLength()!=0)
    {
      CloseUpItem(wWatch,varname,cursel);
    }
    wWatch.DeleteItem(cursel);
    cursel=wWatch.GetNextItem(-1,LVNI_SELECTED);
  }
  UpdateWatchVariableList();
}

void DlgDebugger::OnUpdateWatchDeletewatch(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(wWatch.IsWindowEnabled() && wWatch.GetNextItem(-1,LVNI_SELECTED)!=-1);
}

void DlgDebugger::OnStnClickedHshiftbar()
{
SetCapture();
_moveVert=false;
ShowCursor(FALSE);
}

void DlgDebugger::OnStnClickedVshiftbar()
{
SetCapture();
_moveVert=true;
ShowCursor(FALSE);
}

void DlgDebugger::OnLButtonUp(UINT nFlags, CPoint point)
{
  if (GetCapture())
  {
    ReleaseCapture();
    ShowCursor(TRUE);
  }
}

void DlgDebugger::OnMouseMove(UINT nFlags, CPoint point)
{
 if (GetCapture())
 {
   ClientToScreen(&point);
   if (_moveVert)
   {
     CRect rc;
     wSourceView.GetClientRect(&rc);
     wSourceView.ScreenToClient(&point);
     _splitx=(float)point.x/(float)rc.right;
     if (_splitx<0.05f) _splitx=0.05f;
     if (_splitx>0.95f) _splitx=0.95f;
   }
   else
   {
     CRect rc;
     wContextTab.GetClientRect(&rc);
     wContextTab.ScreenToClient(&point);
     _splity=(float)point.y/(float)rc.bottom;
     if (_splity<0.05f) _splity=0.05f;
     if (_splity>0.95f) _splity=0.95f;
   }
   RecalcLayout();
 }else 

  CDialog::OnMouseMove(nFlags, point);
}

void DlgDebugger::OnDebuggingRunnon()
{
  _dbgobject->SetTraceMode(_dbgobject->traceRun);
  _nonstoprun=true;
  ReleaseProgram();
  MinimizeTray();
}

void DlgDebugger::OnWatchWatchbreakpoints()
{
  wWatch.SetFocus();
  bool nothingsel=true;
  POSITION cursel=wWatch.GetFirstSelectedItemPosition();
  while (cursel)
  {
    int index=wWatch.GetNextSelectedItem(cursel);
    CString varname=wWatch.GetItemText(index,0);
    if (_dbgobject->IsVarBreakpoint(varname))
      _dbgobject->UnsetVarBreakpoint(varname);
    else
      _dbgobject->SetVarBreakpoint(varname);
    UpdateWatchVariableList();
    nothingsel=false;
  }
  if (nothingsel)
    AfxMessageBox(IDS_WATCHBREAKPOINT);

}

void DlgDebugger::OnNMRdblclkAutowatch(NMHDR *pNMHDR, LRESULT *pResult)
{
  wAutowatch.SetFocus();
  bool nothingsel=true;
  POSITION cursel=wAutowatch.GetFirstSelectedItemPosition();
  while (cursel)
  {
    int index=wAutowatch.GetNextSelectedItem(cursel);
    CString varname=wAutowatch.GetItemText(index,0);
    int p=wWatch.InsertItem(wWatch.GetItemCount()-1,varname,0);
    wWatch.EnsureVisible(p,FALSE);    
  }
 UpdateWatchVariableList();
}

BOOL DlgDebugger::PreTranslateMessage(MSG* pMsg)
{
  
  if (!_hotkeysdisable && ::TranslateAccelerator(*this,_hotkeys,pMsg))
    return TRUE;
  if (pMsg->message==WM_RBUTTONDBLCLK)
  {
    KillTimer(_cmenutimer);
    _cmenutimer=0;
    _cmenudisable=TRUE;
    if (pMsg->hwnd==wSourceView)
    {
      CString seltext=wSourceView.GetSelText().Trim();
      if (seltext.Find('\r')==-1 && seltext.Find('\n')==-1 && seltext.GetLength())
      {
        int p=wWatch.InsertItem(wWatch.GetItemCount()-1,seltext,0);
        wWatch.EnsureVisible(p,FALSE);    
        UpdateWatchVariableList();
      }
      return TRUE;
    }
  }

  return CDialog::PreTranslateMessage(pMsg);
}

void DlgDebugger::OnNMRdblclkWatch(NMHDR *pNMHDR, LRESULT *pResult)
{
  OnWatchWatchbreakpoints();
  *pResult = 0;
}

LRESULT DlgDebugger::OnVarBreak(WPARAM wParam, LPARAM lParam)
{
  DebuggerStateChange(true);
  const char *varname=(const char *)wParam;
  const GameValue *val=(const GameValue *)lParam;
  CString text;
  AfxFormatString2(text,IDS_VARIABLECHANGEMSG,varname,GetValueStr(*val));
  if (_nonstoprun)
  {
    ConsoleOutput(text);
    ReleaseProgram();
  }
  else
    AfxMessageBox(text,MB_ICONSTOP);
  return 1;
}

const char *DlgDebugger::FindBestBreakPosition(const char *text,const char *begin)
{
  while (text>begin)
  {
    if (*text==';' || *text=='{')
    {
      text++;
      while (isspace(*text)) text++;
      return text;
    }
    --text;
  }
  while (isspace(*text)) text++;
  return text;
}

const char *DlgDebugger::FindBlockBegin(const char *text,const char *begin)
{
  bool instr=false;
  int level=0;
  while (text>begin)
  {
    if (*text=='{')
    {
      if (level) level--;
      else
        return text+1;
    }
    else if (*text=='}')
      level++;
    else if (*text=='"')
      instr=!instr;
    else if (*text=='\n')
      instr=false;
    --text;
  }
  return text;
}

const char *DlgDebugger::FindBlockEnd(const char *text)
{
  bool instr=false;
  int level=0;
  while (*text)
  {
    if (*text=='}')
    {
      if (level) level--;
      else
        return text;
    }
    else if (*text=='{')
      level++;
    else if (*text=='"')
      instr=!instr;
    else if (*text=='\n')
      instr=false;
    ++text;
  }
  return text;
}

#include <el/crc/crc.hpp>
bool DlgDebugger::CheckBreakpoint(BreakPoint &breakpt)
{  
  if (breakpt.expPos!=_dbgobject->_context._expression)
  {
    breakpt.checked=false;
    breakpt.expPos=_dbgobject->_context._expression;
    int len=strlen(_dbgobject->_context._expression);
    if (len>=breakpt.blockChksumChars)
    {
      CRCCalculator crccalc;
      crccalc.Reset();
      crccalc.Add(breakpt.expPos,breakpt.blockChksumChars);
      if (crccalc.GetResult()==breakpt.blockChksum)
      {
        breakpt.checked=true;
      }    
    }    
  }
 if (breakpt.checked==false) return false;
 if (_dbgobject->_context._pc==breakpt.offset) 
 {
   breakpt.hstate=true;
   return true;
 }
 if ((_dbgobject->_context._pc>breakpt.offset)!=breakpt.hstate)
 {
   if (breakpt.hstate==false) 
   {
     breakpt.hstate=true;
     return true;
   }
   else
     breakpt.hstate=false;
 }
 return false;
  
}
 
void DlgDebugger::OnDebuggingRunto()
{
  if (_curTab!=0)
  {
    int level=_maxTab-_curTab;
    _runToMode=true;
    long sel1,sel2;
    wSourceView.GetSel(sel1,sel2);
    const char *exprs=_dbgobject->GetCurrentExpression(level);
    const char *blockbeg=FindBlockBegin(exprs+sel1,exprs);
    const char *blockend=FindBlockEnd(blockbeg);
    const char *blockoffset=FindBestBreakPosition(exprs+sel1,blockbeg);
    int len=blockend-blockbeg;
    CRCCalculator crccalc;
    crccalc.Reset();
    crccalc.Add(blockbeg,len);
    _runToBreakpoint.blockChksum=crccalc.GetResult();
    _runToBreakpoint.blockChksumChars=len;
    _runToBreakpoint.checked=false;
    _runToBreakpoint.expPos=NULL;
    _runToBreakpoint.offset=blockoffset-blockbeg;
    _runToBreakpoint.scopePos=NULL;
    _dbgobject->SetTraceMode(_dbgobject->traceRun);
    ReleaseProgram();

  }
}
bool DlgDebugger::GetTestBreakpoints()
{
    if (_runToMode)
    {
      _dbgobject->GetContext(*_dbgobject->_context._script);
      return CheckBreakpoint(_runToBreakpoint);
    }
    return false;
  }


void DlgDebugger::SaveState(void)
{
  ArchiveStreamRegisterOut out(HKEY_CURRENT_USER,"SOFTWARE\\Bohemia Interactive Studio\\UIScriptDebugger","settings");
  ArchiveSimpleBinary arch(out);
  SerializeState(arch);  
}

void DlgDebugger::LoadState(void)
{
  ArchiveStreamRegisterIn in(HKEY_CURRENT_USER,"SOFTWARE\\Bohemia Interactive Studio\\UIScriptDebugger","settings");
  if (!in.IsError())
  {
    ArchiveSimpleBinary arch(in);
    SerializeState(arch);
    RecalcLayout();
  }
}

void DlgDebugger::SerializeState(IArchive &arch)
{
  int cnt=wWatch.GetItemCount();
  CString str;
  arch(cnt);
  for (int i=0;i<cnt;i++)
  {
    if (+arch) str=wWatch.GetItemText(i,0);
    arch(str);
    if (-arch) wWatch.InsertItem(i,str,0);
  }
  WINDOWPLACEMENT wp;
  wp.length=sizeof(wp);
  if (+arch) GetWindowPlacement(&wp);
  arch.Binary(&wp,sizeof(wp));
  if (-arch) 
  {
    wp.showCmd=SW_HIDE;
    SetWindowPlacement(&wp);
    MinimizeTray();
  }
  arch(_splitx);
  arch(_splity);
  arch(evalDlg.vExpress);
  arch(evalDlg.vVariable);  
  for (int i=0;i<3;i++)
  {
    wWatch.SetColumnWidth(i,arch.RValue(wWatch.GetColumnWidth(i)));
    wAutowatch.SetColumnWidth(i,arch.RValue(wAutowatch.GetColumnWidth(i)));  
  }
  _dbgobject->EnableDetectUndeclared(arch.RValue(_dbgobject->IsDetectUndeclaredEnabled()));
}

static BOOL CALLBACK BringThreadWindows(HWND hwnd, LPARAM lParam)
{
  SetForegroundWindow(hwnd);
  return TRUE;
}

void DlgDebugger::OnTimer(UINT nIDEvent)
{
  if (_BTWTTtimer==nIDEvent)
  {
    KillTimer(_BTWTTtimer);
    _BTWTTtimer=0;
    EnumThreadWindows(_waitingThread,BringThreadWindows,0);
  }
  if (_cmenutimer==nIDEvent)
  {
    KillTimer(_cmenutimer);
    _cmenutimer=0;
    CMenu mnu;
    mnu.LoadMenu(IDR_DEBUGMENU);
    CMenu *sub;
    if (_cmenuw==wSourceView) 
    {
      CPoint srcpt=_cmenupt;
      wSourceView.ScreenToClient(&srcpt);
      int chid=wSourceView.CharFromPos(srcpt);
      wSourceView.SetSel(chid,chid);
      sub=mnu.GetSubMenu(0);
    }
    else if (_cmenuw==wWatch) sub=mnu.GetSubMenu(1);
    else return;
    sub->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON|TPM_LEFTBUTTON,_cmenupt.x,_cmenupt.y,this);
  }
  CDialog::OnTimer(nIDEvent);
}

void DlgDebugger::OnDebuggerSetsourceposition()
{
  if (_curTab!=0)
  {
    int level=_maxTab-_curTab;
    _runToMode=true;
    long sel1,sel2;
    wSourceView.GetSel(sel1,sel2);
    const char *exprs=_dbgobject->GetCurrentExpression(level);
    const char *start=FindBestBreakPosition(exprs+sel1,exprs);
    _dbgobject->SetLevelCurrentPosition(level,start);
    UpdateSourceView();
  }

}

void DlgDebugger::OnContextMenu(CWnd* pWnd, CPoint point)
{
  if (_cmenudisable)
  {
    _cmenudisable=FALSE;
  }
  else
  {
  _cmenutimer=SetTimer(20,500,NULL);
  _cmenuw=*pWnd;
  _cmenupt=point;
  }
}

void DlgDebugger::OnUpdateDebuggerSetsourceposition(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_inDebugger && _curTab!=0);
}


void DlgDebugger::OnLvnBeginlabeleditWatch(NMHDR *pNMHDR, LRESULT *pResult)
{
  NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
  _hotkeysdisable=true;
  *pResult = 0;
}

RString DlgDebugger::GetTypeName(GameType typd)
  {
    return _dbgobject->_context._script->GetInternalTypeName(typd);
  }

LRESULT DlgDebugger::OnCancelBreak(WPARAM wParam, LPARAM lParam)
{ 
  _breakSign=false;
  _termSign=false;
  if (_dlgbreakwait.GetSafeHwnd()!=NULL)
    _dlgbreakwait.DestroyWindow();
return 0;
}
LRESULT DlgDebugger::OnForceBreak(WPARAM wParam, LPARAM lParam)
{
  if (_dlgbreakwait.GetSafeHwnd()!=NULL)
    _dlgbreakwait.DestroyWindow();
  __try
  {
  _dbgobject->GetContext(*_dbgobject->_context._script);
  DebuggerStateChange(true);  
  }
  __except(1)
  {
    AfxMessageBox(IDS_CONTEXTGETFAILED);
  }
  return 0;
}


void DlgDebugger::OnDestroy()
{
  SaveState();
  CDialog::OnDestroy();
}

void DlgDebugger::OnWatchDetectundeclared()
{
  _dbgobject->EnableDetectUndeclared(!_dbgobject->IsDetectUndeclaredEnabled());
}

void DlgDebugger::OnUpdateWatchDetectundeclared(CCmdUI *pCmdUI)
{
  pCmdUI->SetCheck(_dbgobject->IsDetectUndeclaredEnabled());
}
