// DebuggerSession.cpp : implementation file
//

#include "stdafx.h"
#include "UIDebugger.h"
#include "DebuggerSession.h"
#include "resource.h"


// CDebuggerSession

#pragma warning (disable: 4355)
CDebuggerSession::CDebuggerSession():_dbgwin(this),_owner(GetDesktopWindow())
{
}

CDebuggerSession::~CDebuggerSession()
{
}

BOOL CDebuggerSession::InitInstance()
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  _dbgwin.Create(IDD_DEBUGGER,CWnd::FromHandle(_owner));
  _dbgwin.LoadState();
  SetEvent(runevent);
  CloseHandle(runevent);
  CString str;
  _dbgwin.GetWindowText(str);
  if (_title.GetLength()!=0)
  {
    str=_title+" - "+str;
    _dbgwin.SetWindowText(str);
  }
  return TRUE;
}

int CDebuggerSession::ExitInstance()
{
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CDebuggerSession, CWinThread)
END_MESSAGE_MAP()




// CDebuggerSession message handlers
void CDebuggerSession::OnTrace()
{
  if (GetCurrentThreadId()==this->m_nThreadID) return;
  _dbgwin.SwitchToDebugger(DBGM_ONTRACE,0,0);
}
void CDebuggerSession::OnVariableBreakpoint(const char *name, const GameValue &val)
{
  if (GetCurrentThreadId()==this->m_nThreadID) return;
  _dbgwin.SwitchToDebugger(DBGM_ONVARCHANGE,(WPARAM)name,(LPARAM)&val);
}

bool CDebuggerSession::OnConsoleOutput(GameState &script,const char *text)
{
 AFX_MANAGE_STATE(AfxGetStaticModuleState());
  if (GetCurrentThreadId()==this->m_nThreadID)   
    _dbgwin.OnConsoleOutput(0,(LPARAM)text);  
  else
    _dbgwin.ConsoleOutput(text);
 return true;
}

void CDebuggerSession::QuitDebugger()
{
  if (GetCurrentThreadId()==this->m_nThreadID)
  {
    _dbgwin.SendMessage(DBGM_QUITDEBUGGER,0,0);    
  }
  else
  {
    int timeout=5;
    _dbgwin.PostMessage(DBGM_QUITDEBUGGER,0,0);
    while (WaitForSingleObject(this->m_hThread,1000)==WAIT_TIMEOUT && timeout--)
      if (_dbgwin.GetSafeHwnd()) 
        _dbgwin.PostMessage(DBGM_QUITDEBUGGER,0,0);
      else
        PostThreadMessage(WM_QUIT,0,0);
    if (timeout==0) 
    {
      AfxMessageBox(IDS_DEBUGGERHALTED,MB_OK);
      TerminateThread(this->m_hThread,0);
    }  
  }
}

bool CDebuggerSession::OnError(GameState &script,const char *error)
{
  IScriptDebugger::ContextInfo saveContext=_context;
  GetContext(script);
  SetTraceMode(traceRun);
  if (GetCurrentThreadId()==this->m_nThreadID)
    _dbgwin.SendMessage(DBGM_ONERROR,0,(LPARAM)error);
  else
   _dbgwin.SwitchToDebugger(DBGM_ONERROR,0,(LPARAM)error);
  _context=saveContext;
  return true;
}

