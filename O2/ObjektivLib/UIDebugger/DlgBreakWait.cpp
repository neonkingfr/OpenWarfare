// DlgBreakWait.cpp : implementation file
//

#include "stdafx.h"
#include "UIDebugger.h"
#include "DlgBreakWait.h"
#include ".\dlgbreakwait.h"


// DlgBreakWait dialog

IMPLEMENT_DYNAMIC(DlgBreakWait, CDialog)
DlgBreakWait::DlgBreakWait(CWnd* pParent /*=NULL*/)
	: CDialog(DlgBreakWait::IDD, pParent)
{
}

DlgBreakWait::~DlgBreakWait()
{
}

void DlgBreakWait::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(DlgBreakWait, CDialog)
  ON_BN_CLICKED(IDC_TERMINATE, OnBnClickedTerminate)
END_MESSAGE_MAP()


// DlgBreakWait message handlers

void DlgBreakWait::OnCancel()
{
  notify->PostMessage(DBGM_CANCELBREAK,0,0);
}

void DlgBreakWait::OnOK()
{
  if (AfxMessageBox(IDS_FORCEWARN,MB_YESNO)==IDYES)
    notify->PostMessage(DBGM_FORCEBREAK,0,0);
}

void DlgBreakWait::OnBnClickedTerminate()
{
  if (AfxMessageBox(IDC_KILLWARN,MB_YESNO)==IDYES)
  {
    HANDLE thr=OpenThread(THREAD_TERMINATE,FALSE,this->lastThread);
    TerminateThread(thr,-1);
    CloseHandle(thr);
    PostQuitMessage(0);
  }
}
