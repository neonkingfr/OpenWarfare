#pragma once

template <>
void ArchiveVariableExchange(IArchive &arch,CString &data)
{
  if (+arch)
  {
    char *c=const_cast<char *>((LPCTSTR)data);
    arch(c);
  }
  else
  {
    char *c=NULL;
    arch(c);
    data=c;
    delete [] c;
  }  
}