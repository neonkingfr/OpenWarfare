#pragma once


// DlgBreakWait dialog

#define DBGM_CANCELBREAK (WM_APP+200)
#define DBGM_FORCEBREAK (WM_APP+201)

class DlgBreakWait : public CDialog
{
	DECLARE_DYNAMIC(DlgBreakWait)

public:
	DlgBreakWait(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgBreakWait();

// Dialog Data
	enum { IDD = IDD_BREAKINPROGRESS };
    CWnd *notify;
    DWORD lastThread;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
    virtual void OnCancel();
    virtual void OnOK();
public:
  afx_msg void OnBnClickedTerminate();
};
