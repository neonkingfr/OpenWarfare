#include <Es/Common/win.h>
#include ".\dlldebuggingsession.h"

DllDebuggerProvider::DllDebuggerProvider()
{
_debugmod=NULL;
}

DllDebuggerProvider::~DllDebuggerProvider()
{
if (IsLoaded()) 
{
  DoneDebuggerMemoryManagment();
  FreeLibrary(_debugmod);
}
_debugmod=NULL;
}

static void *local_malloc(size_t sz, DllDebugMallocFreeOp op)
{
    switch (op)
    {
    case DDMallocFree: return malloc(sz);
    case DDNewDelete: return operator new(sz);
    case DDNewDeleteArr: return operator new[](sz);
    }
    return 0;
}

static void local_free(void *what, DllDebugMallocFreeOp op)
{
    switch (op)
    {
    case DDMallocFree: return free(what);
    case DDNewDelete: return operator delete(what);
    case DDNewDeleteArr: return operator delete[](what);
    }
}


bool DllDebuggerProvider::CreateProvider(const char *providerDll)
{
  _debugmod=LoadLibrary(providerDll);
  if (_debugmod==NULL) return false;
  type_dlldebugger_SetupDebuggerMemoryManager SetupDebuggerMemoryManager=(type_dlldebugger_SetupDebuggerMemoryManager)GetProcAddress(_debugmod,"SetupDebuggerMemoryManager");
  InitDebugger=(type_dlldebugger_InitDebugger)GetProcAddress(_debugmod,"InitDebugger");
  DoneDebugger=(type_dlldebugger_DoneDebugger)GetProcAddress(_debugmod,"DoneDebugger");
  InitDebugger2=(type_dlldebugger_InitDebugger2)GetProcAddress(_debugmod,"InitDebugger2");
  DoneDebuggerMemoryManagment=(type_dlldebugger_DoneDebuggerMemoryManagment )GetProcAddress(_debugmod,"DoneDebuggerMemoryManagment");

  if (DoneDebugger==NULL || InitDebugger==NULL ||   SetupDebuggerMemoryManager==NULL || DoneDebuggerMemoryManagment==NULL)
  {
    FreeLibrary(_debugmod);
    _debugmod=NULL;
    return false;
  }
  SetupDebuggerMemoryManager(&local_malloc,&local_free);
  return true;
}


DllDebuggingSession::DllDebuggingSession(DllDebuggerProvider &provider):_provider(provider)
{
  if (provider.IsLoaded())  
    _debugger=provider.InitDebugger();
  else
    _debugger=NULL;
}

DllDebuggingSession::DllDebuggingSession(DllDebuggerProvider &provider, HWND owner, const char *title):_provider(provider)
{
  if (provider.IsLoaded())  
    _debugger=provider.InitDebugger2(owner,title);
  else
    _debugger=NULL;
}

DllDebuggingSession::~DllDebuggingSession(void)
{
  if (_debugger!=NULL && _provider.IsLoaded())
    _provider.DoneDebugger(_debugger);
}


