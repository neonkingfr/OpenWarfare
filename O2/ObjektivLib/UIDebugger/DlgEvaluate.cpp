// DlgEvaluate.cpp : implementation file
//

#include "stdafx.h"
#include "UIDebugger.h"
#include "DlgEvaluate.h"
#include ".\dlgevaluate.h"


// DlgEvaluate dialog

IMPLEMENT_DYNAMIC(DlgEvaluate, CDialog)
DlgEvaluate::DlgEvaluate(CWnd* pParent /*=NULL*/)
	: CDialog(DlgEvaluate::IDD, pParent)
    , vExpress(_T(""))
    , vVariable(_T(""))
{
}

DlgEvaluate::~DlgEvaluate()
{
}

void DlgEvaluate::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_SCRIPT, wExpression);
  DDX_Control(pDX, IDC_VARIABLE, wVariable);
  DDX_Control(pDX, IDC_RESULT, wResult);
  DDX_Text(pDX, IDC_SCRIPT, vExpress);
  DDX_Text(pDX, IDC_VARIABLE, vVariable);
  DDX_Control(pDX, IDC_STOP, wStop);
  DDX_Control(pDX, IDOK, wEvaluate);
}


BEGIN_MESSAGE_MAP(DlgEvaluate, CDialog)
  ON_EN_CHANGE(IDC_SCRIPT, OnEnChangeScript)
  ON_BN_CLICKED(IDC_STOP, OnBnClickedStop)
END_MESSAGE_MAP()


// DlgEvaluate message handlers

BOOL DlgEvaluate::OnInitDialog()
{
  CDialog::OnInitDialog();

  DialogRules();

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgEvaluate::OnOK()
{

  CString express; wExpression.GetWindowText(express);
  CString var; wVariable.GetWindowText(var);
  var=var.Trim();
  express=express.Trim();
  if (express.GetLength()==0) return ;
  
  wEvaluate.ShowWindow(SW_HIDE);
  wStop.ShowWindow(SW_SHOW);

  GameVarSpace space(vGamestate->GetContext());
  vGamestate->BeginContext(&space);
  GameValue res=vGamestate->EvaluateMultiple(express);
  vGamestate->EndContext();
  
  if (var.GetLength()!=0) 
  {
    vVarSpace->VarSet(var,res);  
    wNotifyVarChange->PostMessage(DBGN_VARIABLECHANGE,0,0);
  }
  wResult.SetWindowText(res.GetText());

  wEvaluate.ShowWindow(SW_SHOW);
  wStop.ShowWindow(SW_HIDE);
  dbg->SetTraceMode(dbg->traceRun);
}

void DlgEvaluate::DialogRules(void)
{
   CWnd *ok=GetDlgItem(IDOK);
   ok->EnableWindow(wExpression.GetWindowTextLength()!=0);
}

void DlgEvaluate::OnEnChangeScript()
{
  DialogRules();
}

void DlgEvaluate::OnCancel()
{
  UpdateData();  
  dbg->SetTraceMode(dbg->traceTerminate);
  EndDialog(0);
}

void DlgEvaluate::OnBnClickedStop()
{
  dbg->SetTraceMode(dbg->traceTerminate);
}
