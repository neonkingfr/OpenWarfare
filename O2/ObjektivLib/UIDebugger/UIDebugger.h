// UIDebugger.h : main header file for the UIDebugger DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

// CUIDebuggerApp
// See UIDebugger.cpp for the implementation of this class
//

class CUIDebuggerApp : public CWinApp
{
    

public:
	CUIDebuggerApp();

    CImageList _watchico;
    CImageList _watchstate;
    

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
    virtual int ExitInstance();
};

extern CUIDebuggerApp theApp;