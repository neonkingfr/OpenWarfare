#pragma once

#ifndef _ANIMATION_PHASE_H
#define _ANIMATION_PHASE_H


#include <el/math/math3d.hpp>
#include "PointAttrib.h"

namespace ObjektivLib {

class ObjectData;


class AnimationPhase: public PointAttrib<Vector3>
{
  float _time;

public:
  AnimationPhase(  ObjectData *object )
    :PointAttrib<Vector3>(object),_time(0)
  {}
  AnimationPhase(  const AnimationPhase &src )
    :PointAttrib<Vector3>(src),_time(src._time)
  {}

  AnimationPhase(  const AnimationPhase &src, ObjectData *object )
    :PointAttrib<Vector3>(src,object),_time(src._time)
  {}

  void SetTime( float time ) 
  {_time=time;}
  float GetTime() const  
  {return _time;}

  void Redefine( ObjectData *data ); // import external data
};

}
#endif
