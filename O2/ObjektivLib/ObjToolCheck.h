#ifndef __OBJ_TOOL_CHECK_CLASS_HEADER_
#define __OBJ_TOOL_CHECK_CLASS_HEADER_

#pragma once


#include "ObjectData.h"

namespace ObjektivLib {


class ObjToolCheck: public ObjectData
  {
  public:
  void CheckFaces();
  void IsolatedPoints();
  void CheckMapping();
  void CheckSTVectors();
  void Optimize();

  void RepairDegeneratedFaces();
  void CheckDegeneratedFaces();
  };


}
#endif