#pragma once
#include "objectdata.h"
#include <El/BTree/Btree.h>

namespace ObjektivLib {


struct ObjMatLibItem
  {
  bool status;
  RString name;
  bool operator==(const ObjMatLibItem& other) const {return _stricmp(name,other.name)==0;}
  bool operator>=(const ObjMatLibItem& other) const {return _stricmp(name,other.name)>=0;}
  bool operator<=(const ObjMatLibItem& other) const {return _stricmp(name,other.name)<=0;}
  bool operator!=(const ObjMatLibItem& other) const {return _stricmp(name,other.name)!=0;}
  bool operator>(const ObjMatLibItem& other) const {return _stricmp(name,other.name)>0;}
  bool operator<(const ObjMatLibItem& other) const {return _stricmp(name,other.name)<0;}  
  ObjMatLibItem():status(false) {}
  ObjMatLibItem(const RString name):name(name),status(true) {}
  };


class ObjToolMatLib :  public ObjectData
  {
  public:
    enum ReadMode {ReadTextures,ReadMaterials};
    enum SearchMode {SearchTextures, SearchMaterials, SearchBoth};
    enum ConvArrayMode {InclExist=1,InclNonExist=2};

    void ReadMatLib(BTree<ObjMatLibItem>& container, ReadMode mode) const;
    void RenameTexOrMat(const char *oldname, const char *newname, SearchMode smode);
    
    void CreateSelection(const char *name, SearchMode smode, Selection &newsel) const;

    static void CheckFilesExists(BTree<ObjMatLibItem>& container,const char *basepath);
    static void ConvContainerToArray(const BTree<ObjMatLibItem>& container, AutoArray<RString>& outarray, int convmode=InclExist);

    void SetTexOrMat(const char *texOrMat, ReadMode mode, const Selection *sel=NULL); //map material ot texture to curremt selection (or specified by sel)

    static void ExploreMaterial(const char *matName, BTree<RStringI> &container);
  };

}

template<>
void BTreeUnsetItem(ObjektivLib::ObjMatLibItem &item);
