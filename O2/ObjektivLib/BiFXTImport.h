#pragma once

namespace ObjektivLib {

class LODObject;

class CBiFXTImport
{    
public:  
  static bool Import(const char * _txtName, LODObject * into);
};

}