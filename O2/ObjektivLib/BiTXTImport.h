#pragma once

#include <el/math/math3d.hpp>
#include <el/ProgressBar/ProgressBar.h>
#include <fstream>

namespace ObjektivLib {

class CBiTXTCommon
{
public:
  enum SectionAndCommands {SectHeader,SectMaterial,SectLod,SectObject,SectPoints,SectFace,SectEdges,SectSelection,SectEnd,
    cmdVersion,cmdSharp,cmdAngle,cmdIndex,cmdNormal,cmdUV,cmdTexture,cmdMaterial,cmdSG,
    stEdges,stNormals,SectMass,cmdIndexBase,cmdChannels};
};

class CBiTXTImport: public CBiTXTCommon
  {
  public:
  enum CmdType {Section,Command,Integer,Float,Text,FileEof,NewLine,Error};
  enum ErrorType {ErrInvalidNumber,ErrUnknownToken,ErrMissingQuotes,ErrUnknownCommand,ErrFileOpenError,ErrMissingHeader,
            ErrExceptingFloatNumber,ErrExceptingAngle,ErrExceptingSharpValue,ErrExceptingSharp,ErrExceptingVersion,
            ErrUnimplemented,ErrExceptingLodSpecifier,ErrExceptingObject,ErrExceptingSection,ErrExceptingPointsSection,
            ErrTooLowCountOfVertices,ErrTooHighCountOfVertices,ErrExceptingIntegerNumber,ErrIntegerOutOfRange,
            ErrFloatOutOfRange,ErrExceptingString,ErrUnexceptedEnd,ErrUnknownVersion,ErrStructureRuleBreak};

  private:

  CmdType _cmdType;  
  SectionAndCommands  _command;
  SectionAndCommands  _sharp;
  ErrorType _errType;
  float _angle;
  float _version;
  int _indexBase;

  std::ifstream _rdstream;
  ProgressBar<int> *_progress;
  char _text[1024];
  float _fValue;
  int _iValue;

  bool _sortFaces;
  
  void ReadNum();
  void ReadString();
  void ReadNext();
  void ReadCommand();
  
  void ReadHeader(LODObject *lod);
  void ReadHeaderSection();
  void ReadAfterHeader(LODObject *lod);

  void ReadLods(LODObject *lod);
  void ReadMaterial();
  void ReadSpecifiedObject(ObjectData *obj);
  void ReadUnknownObject(ObjectData *obj);

  Vector3 ReadVector();
  Vector3 ReadUV();
  void ReadPoints(ObjectData *obj);
  void ReadFace(ObjectData *obj, bool oldStyle);
  void ReadEdges(ObjectData *obj);
  void ReadNormals(ObjectData *obj, int fcindex);
  void ReadUVS(ObjectData *obj, int fcindex);
  void ReadSelection(ObjectData *obj);

  void SetError(ErrorType err)
    {if (_cmdType!=Error) {_cmdType=Error;_errType=err;}}

  void OpenFile(const char *filename);

  public:
    void Import(const char *filename, LODObject *lod);
    bool IsError() {return _cmdType==Error;}
  ErrorType GetError() {return _errType;}
  std::pair<int,int> GetErrorLine();
    void FormatError(char *buff,int size);
    void ReadMass(ObjectData *obj);
    virtual ~CBiTXTImport() {}
    CBiTXTImport():_sortFaces(true) {}
    virtual float LODNameToResol(const char *name) = 0; //must be defined - converts name to level ID
    virtual RStringB LODResolToName(float resol) const = 0; //must be defined - converts level ID to name
    void EnableSortFaces(bool enable) {_sortFaces=enable;}
    bool IsSortFacesEnabled() const {return _sortFaces;}
  };


class CBiTXTExport:  public CBiTXTCommon
{
    std::ofstream _wrstream;
  int _mode;
  void WriteNum(float value);
  void WriteNum(int value);
  void WriteString(const char *text);
  void WriteCommand(SectionAndCommands cmd);
  void Eoln();

  void WriteHeader(LODObject *lod);
  void WriteLods(LODObject *lod);
  void WriteLevel(ObjectData *obj);
  void WriteVector(Vector3 &vx);
  void WriteUV(float u, float v);
  void WritePoints(ObjectData *obj);
  void WriteFace(ObjectData *obj, unsigned long *groups, int index);
  void WriteEdges(ObjectData *obj);
  void WriteSelection(ObjectData *obj,int index);
  void WriteMass(ObjectData *obj);
public:
  enum ExportMode {ExpNormals=1,ExpSG=2,ExpEdges=3};
  bool Export(const char *filename, LODObject *lod,ExportMode mode);
};
}