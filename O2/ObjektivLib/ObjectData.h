#ifndef __TRADITIONAL_OBJECT_DATA_H_
#define __TRADITIONAL_OBJECT_DATA_H_

#pragma once

#include <stdio.h>
#include <fstream>
/*#include <Es/essencePch.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Types/pointers.hpp>
#include <El/Pathname/Pathname.h>*/
#include "data3d.h"
#include "FaceT.h"
#include "Selection.h"
#include "NamedObject.h"
#include "NamedProperty.h"
#include "AnimationPhase.h"
#include "ObjUVSet.h"

class Pathname;

namespace ObjektivLib {

using namespace std;


class NamedSelection: public NamedObject<Selection> {
public:
    typedef NamedObject<Selection> Base;
    NamedSelection( ObjectData *object, const char *name ):Base(object,name) {}
    NamedSelection( const Selection &sel, const char *name ):Base(sel,name) {}
};

#define CTAssert(value) do {char dummy[(value) ? 1 : -1];(void)dummy;} while (false)


#define MAX_NAMED_SEL 2048
#define MAX_NAMED_PROP 128

#define OBJDATA_LATESTVERSION 1

class CEdges;


struct SharpEdge
  {
  int edge[2];
  int &operator[] ( int i ) 
    {return edge[i];}
  int operator[] ( int i ) const 
    {return edge[i];}
  ClassIsSimple(SharpEdge);
  SharpEdge(int a,int b) {edge[0]=a;edge[1]=b;}
  SharpEdge() {}
  };

typedef enum 
  {SelSet,SelAdd,SelSub,SelAnd} SelMode;


class FaceTraditional: public DataFaceEx
  {
  public:
    
    FaceTraditional()  {}
    FaceTraditional( const DataFaceEx &data )
      :DataFaceEx(data)
        {}

    ClassIsMovable(FaceTraditional);

  };


struct AnimationKeyStone : public RefCount
{
  AnimationKeyStone() {};
  AnimationKeyStone(float _time, const RString &_key, const RString &_value): 
    time(_time), key(_key), value(_value) {};
  float time;
  RString key;
  RString value;
};
ClassIsGeneric(AnimationKeyStone);


struct AnimationMetadata : public RefCount
{
  AnimationMetadata() {};
  AnimationMetadata(const RString &_name, const RString &_value): name(_name), value(_value) {};
  RString name;
  RString value;
};
ClassIsGeneric(AnimationMetadata);


#pragma warning (disable: 4355)

class ObjectData
  {
  protected:
    friend class Selection;
    friend class LODObject;
    
    // object data
    //PosT points[OMaxPoints];
    //VecT normals[OMaxPoints];
    //FaceT faces[OMaxFaces];
    //int nPoints,nNormals,nFaces;
    AutoArray<PosT> _points;
    AutoArray<VecT> _normals;
    AutoArray<FaceTraditional> _faces;
    AutoArray<SRef<ObjUVSet> > _uvsets;
    
    bool _dirty; // some data was changed and not saved
    
    // object setup (selection, edges...)
    Selection _sel;
    Selection _hide,_lock; // hidden, locked vertices
    NamedSelection *_namedSel[MAX_NAMED_SEL];
    NamedProperty *_namedProp[MAX_NAMED_PROP];
    
    PointAttrib<float> _mass;
    AutoArray< Ref<AnimationPhase> > _phase;

    AutoArray< Ref<AnimationKeyStone> > _keyStone;
    AutoArray< Ref<AnimationMetadata> > _metadata;

    AutoArray<SharpEdge> _sharpEdge;
    
    
    int _autoSaveAnimation; // currently edited data is taken from some animation
    ObjUVSet *_activeUVSet;
    
    
  private:
    // initializers and deinitilizers
    void DoConstruct(); // constructor
//    void DoConstructEmpty(); // constructor
    void DoCopy( const ObjectData &src ); // copy constructor
    void DoCopy( const ObjectData &src, int animationPhase ); // copy constructor
    void DoDestruct(); // destructor
    
  public:
    // cannonical interface
    ObjectData()
      :_sel(this),_hide(this),_lock(this),
    _mass(this)
      {
      DoConstruct();
      }
    ~ObjectData()
      {DoDestruct();}
    
  public:
    ObjectData( const ObjectData &src )
      :_sel(this),_hide(this),_lock(this),
    _mass(this)
      {
      DoCopy(src);
      }
    ObjectData( const ObjectData &src, int animationPhase )
      :_sel(this),_hide(this),_lock(this),
    _mass(this)
      {
      DoCopy(src,animationPhase);
      }
    void operator = ( const ObjectData &src )
      {
      DoDestruct();
      DoCopy(src);
      }
    ///Merges one object into another
    /**
      @param src source object
      @param createSel if specified, merges object as selection named createSel
      @param selections if true, source selections is also merged.
      */
    bool Merge( const ObjectData &src ,const char *createSel=NULL, bool selections=true);
    
    // _access to data 
    void SetDirty()
      {_dirty=true;}
    void ClearDirty()
      {_dirty=false;}
    bool Dirty()
      {return _dirty;}
    
    
    int NPoints() const 
      {return _points.Size();}
    const PosT &Point( int i ) const 
      {return _points[i];}
    PosT &Point( int i ) 
      {return _points[i];}
    
    int NNormals() const 
      {return _normals.Size();}
    const VecT &Normal( int i ) const 
      {return _normals[i];}
    VecT &Normal( int i ) 
      {return _normals[i];}
    void ResetNormals() 
      {_normals.Clear();}
    
    int NFaces() const 
      {return _faces.Size();}
    const FaceTraditional &Face( int i ) const 
      {return _faces[i];}
    FaceTraditional &Face( int i ) 
      {return _faces[i];}
    
    bool SaveNamedSel( const char *name , const Selection *sel=NULL);
    bool UseNamedSel( const char *name, SelMode mode=SelSet );
    bool DeleteNamedSel( const char *name );
    bool RenameNamedSel( const char *name, const char *newname );
    const char *NamedSel( int i ) const;

    bool UseSelection(const Selection *sel, SelMode mode=SelSet );
    
    int FindNamedSel( const char *name ) const;
    int FindNamedSelNoCaseSensitive( const char *name ) const;
    
    const NamedSelection *GetNamedSel( int i ) const;
    const NamedSelection *GetNamedSel( const char *name ) const;
    NamedSelection *GetNamedSel( int i );
    NamedSelection *GetNamedSel( const char *name );
    
    
    const char *GetNamedProp( const char *name ) const; // NULL if not existing
    bool SetNamedProp( const char *name, const char *value );
    bool DeleteNamedProp( const char *name );
    bool RenameNamedProp( const char *name, const char *newname );
    
    const char *GetNamedProp( int i ) const;
    const char *GetNamedPropValue( int i ) const;
    //int NNamedProps() const;
    
    
    int NAnimations() const;
    AnimationPhase *GetAnimation( int i ) const;
    int AnimationIndex( float time ) const;
    int NearestAnimationIndex( float time ) const;
    bool AddAnimation( const AnimationPhase &src );
    bool RenameAnimation( float oldTime, float newTime );
    bool DeleteAnimation( float time );
    void DeleteAllAnimations();
    bool SortAnimations(); // retain current animation
    
    int CurrentAnimation() const 
      {return _autoSaveAnimation;}
    
    void RedefineAnimation( AnimationPhase &anim );
    void UseAnimation( AnimationPhase &anim ); 
       
    void RedefineAnimation( int i );
    void UseAnimation( int i );
    
    RString Description() const;
    
    const Selection *GetSelection() const 
      {return &_sel;}
    const Selection *GetHidden() const 
      {return &_hide;}
    const Selection *GetLocked() const 
      {return &_lock;}
    
    void UseLocked() 
      {_sel=_lock;}
    void UseHidden() 
      {_sel=_hide;}
    
    bool PointSelected( int i ) const 
      {return _sel.PointSelected(i);}
    bool FaceSelected( int i ) const 
      {return _sel.FaceSelected(i);}
    void PointSelect( int i, bool sel=true )
      {_sel.PointSelect(i,sel);}
    void FaceSelect( int i, bool sel=true )
      {_sel.FaceSelect(i,sel);}
    
    int ReservePoints( int count );
    int ReserveFaces( int count );
    
    PosT *NewPoint();
    VecT *NewNormal();
    int NewFace();
    
    int AddNormal( const VecT &norm );
    int FindNormal( const VecT &norm, double maxDist=0.01 );
    int FindEdge( int a, int b ) const;
    int FindOrientedEdge( int a, int b ) const;
    int FindOrientedEdge( int a, int b, bool *searchFaces ) const;
    int FindPoint(const VecT &point, double maxDist=0.00001 );
    
    void DeletePoint( int index );
    void DeleteFace( int index );
    void DeleteNormal( int index );
    void ReplacePoint( int newIndex, int oldIndex );
    
    void PermuteVertices( const int *permutation );

    // Animation metadata
    int GetMetadataCount() const;
    const AnimationMetadata *GetMetadata(int index) const;
    const AnimationMetadata *GetMetadata(const RString &name) const;
    void AddMetadata(const AnimationMetadata &data);
    // Set existing metadata or add new
    void SetMetadata(const AnimationMetadata &data);
    // Delete metadata at index position
    void DeleteMetadata(int index);
    // Return index of metadata or -1 if not found
    int FindMetadata(const RString &name) const;
    void ClearAllMetadata();

// Animation keystones
    int GetKeyStoneCount() const;
    AnimationKeyStone *GetKeyStone(int index) const;
    // Add new keystone to end position
    void AddKeyStone(const AnimationKeyStone &data);
    // Add new keystone sorted by time
    void AddKeyStoneSorted(const AnimationKeyStone &data);
    // Delete keystone at index position
    void DeleteKeyStone(int index);
    void ClearAllKeyStones();

  public:
    // normals recalculation
    void RecalcNormals(bool noprogress=false); // set normals as dirty
    
    // face optimization
    void BackfaceCull( const Vector3 &pin );
    // result in current selection
    bool SelectionSplit(Selection *sel = 0);
   
    void Triangulate( bool allFaces=false );
    void Squarize( bool allFaces=false );

    enum TriangulateMode {
        tmNormal,
        tmReversed,
        tmConvex,
        tmConcave
    };
    void Triangulate(TriangulateMode mode, Selection *sel = 0);
    
  protected:
        
    int LoadBinary(const Pathname &filename );        
    
  public: // public I/O routines
    // binary format stream operation
//    void Optimize(); // remove any unnecessary faces, ....
    
    int SaveData( ostream &f, bool final, int version );
    int SaveSetup( ostream &f, bool final, bool mass, bool nolongtags);
    int LoadData( istream &f );
    int LoadSetup( istream &f );
    
    int LoadFinished();
    
    int LoadBinary( istream &f );
    int SaveBinary( ostream &f, int version, bool final=false, bool mass=true );
    
      
    // autodetect format and load
    int Load(const Pathname &filename );
    // save in new (binary) format
    int Save(const Pathname &filename, bool final=false, bool mass=true );
    
    // selection operations
  public:
    
    void ClearSelection();
    
    int SingleSelectedFace() const;
    int FirstSelectedFace() const;
    int CountSelectedFaces() const;
    
    int SingleSelectedPoint() const;
    int FirstSelectedPoint() const;
    int CountSelectedPoints() const;
    
    // sharp/smoothed edge operations
    int FindSharpEdge( int a, int b ) const;
    void SortSharpEdges();
    void RemoveSharpEdge( int a, int b );
    void RemoveSharpEdge( int index );
    void RemoveAllSharpEdges();
    void AddSharpEdge( int a, int b );
    bool EnumEdges(int &enm, int &from, int &to) const;
    ///Loads sharp edges into CEdges structure
    void LoadSharpEdges(CEdges &edges) const;
    ///Saves sharp edges from CEdges structure
    void SaveSharpEdges(const CEdges &edges, bool merge=false);
    
    // autoselection
    void SelectFacesFromPoints( int vertex=-1, SelMode mode=SelSet );
    void SelectPointsFromSelectedFaces( int faceIndex, SelMode mode );
    void UnselectPointsFromUnselectedFaces( int faceIndex, SelMode mode );
    void SelectPointsFromFaces( int face=-1, SelMode mode=SelSet );
    
    void SelectHalfSpace( const FaceT &face );

    void CleanEmptySelections();

    ///Builds face grafh
    /** Builds graph, where points are faces, and edges are incidence between faces.
    @param result Receives result graph of incidence
    @param nocross List of edges in object, that cannot be included into incidence
    @param FaceTestCallback callback function to exclude some faces. If callback returns false, face is excluded
    @param context user defined context passed into callback function*/
    void BuildFaceGraph(CEdges &result,const  CEdges &nocross, bool (*FaceTestCallback)(const FaceT &fc, void *context)=NULL,void *context=NULL) const;
    
  protected:
    //void PromoteComponent( bool *visited, int point, bool state );
    void PromoteComponent( bool *visited, CEdges &edges, int point, bool state );
    
  public:
    void SelectComponent( int point, bool state=true );
    
    void HideSelection()  
      {_hide+=_sel;}
    void UnhideSelection() 
      {_hide-=_sel;}
    void LockSelection() 
      {_lock+=_sel;}
    void UnlockSelection() 
      {_lock-=_sel;}
    
    void SetPointMass( int i, double mass ) 
      {_mass[i]=(float)mass;}
    double GetPointMass( int i ) const 
      {return i<_mass.N()?_mass[i]:0;}
    
    void SetSelectionMass( double mass, bool constTotal=false );
    double GetSelectionMass() const;
    double GetTotalMass() const;
    double GetMaxMass() const;
    VecT GetMassCentre(bool selection);
    
    // animation weighting operations
    
    
    bool SelectionDelete(Selection *sel=NULL);
    bool SelectionDeleteFaces(Selection *sel=NULL);
    
    // different operations on selection
    bool MergePoints( double limit, bool selected, Selection *sel=NULL );
    
    
    // general helper function export
    
    static void SaveTag( ostream &f, const char *tag, int size , bool nolongtag);
    static RString LoadTag( istream &f, int &size );
    static bool FindTag( istream &f, const char *name, int &size );

    template <class ToolClass>
    ToolClass& GetTool()
      {
      CTAssert(sizeof(ToolClass)==sizeof(*this));
      return static_cast<ToolClass &>(*this);
      }

    template <class ToolClass>
    const ToolClass& GetTool() const
      {
      CTAssert(sizeof(ToolClass)==sizeof(*this));
      return static_cast<const ToolClass &>(*this);
      }

      ///UVSets...

   ObjUVSet *GetUVSet(int uvsetid)
   {
     for (int i=0;i<_uvsets.Size();i++) if (_uvsets[i]->GetIndex()==uvsetid) return _uvsets[i];
     return 0;
   }

   const ObjUVSet *GetUVSet(int uvsetid) const
   {
     return const_cast<ObjectData *>(this)->GetUVSet(uvsetid);
   }

   int GetUVSetCount() const
   {
     return _uvsets.Size();
   }

   bool SetActiveUVSet(int uvsetid)
      {
        ObjUVSet *ss=GetUVSet(uvsetid);
        if (ss==0) return false;
        _activeUVSet=ss;
        return true;
      }
  ObjUVSet *AddUVSet(int uvsetid=-1)
  {
    if (uvsetid==-1) 
    {
      uvsetid=0;
      ObjUVSet *ret;
      do {ret=AddUVSet(uvsetid++); }  while (ret==0);
      return ret;
    }
    else
    {
      for (int i=0;i<_uvsets.Size();i++)
        if (_uvsets[i]->GetIndex()==uvsetid) return 0;
      ObjUVSet *nw=new ObjUVSet(this,uvsetid);
      _uvsets.Append()=nw;
      return nw;      
    }
  }

  ObjUVSet *GetActiveUVSet() { return _activeUVSet;}
  const ObjUVSet *GetActiveUVSet() const { return _activeUVSet;}

  template<class Functor>
  bool EnumUVSets(Functor &functor)
  {
    return _uvsets.ForEachF(functor);
  }
  template<class Functor>
  bool EnumUVSets(Functor &functor) const
  {
    return _uvsets.ForEachF(functor);
  }

  bool DeleteUvSet(int id)
  {
    if (id==0) return false;
    for (int i=0;i<_uvsets.Size();i++) 
      if (_uvsets[i]->GetIndex()==id)
      {
        if (_uvsets[i]==_activeUVSet) _activeUVSet=GetUVSet(0);
        _uvsets.Delete(i);
        return true;
      }
    return false;
  }

  void SortUVSets();

};


  inline bool TextureIsAlpha( const char *name ) {return true;} ///zatim docasne reseni!!!!!!!!!!!
/*
{
  static RString lastTest;
  static bool lastResult;
  if( !_strcmpi(lastTest,name) ) return lastResult;
  lastTest=name;
  lastResult=!_strcmpi(NajdiPExt(NajdiNazev(name)),".paa");
  if( lastResult ) return lastResult;
  // make full path
  // get 
  //WFilePath dest;
  //dest.SetDrive(_viewer.GetDrive());
  //dest.SetDirectory(_viewer.GetDirectory()+"data\\");
  
  
  InitPal2Pac();
  DWORD argb=0xffffffff;
  if (TextureColorARGB)
  {
    argb=::TextureColorARGB(name);
  }
  int a=(argb>>24)&0xff;
  lastResult=( a<0xe0 );
  return lastResult;
}
*/
//  void RecalcNormals2(ObjectData *obj,bool noprogress);

#define TRADITIONAL_OBJECTDATA_DEFINED 

}

#include "PointAttrib.tli"

#endif