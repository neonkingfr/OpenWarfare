#include "Common.hpp"
#include "LODObject.h"
#include "GlobalFunctions.h"
#include <fstream>

namespace ObjektivLib {

using namespace std;

// we assume new never returns NULL

void LODObject::DoConstruct(ObjectData *firstlod, float resolution)
  {
  _obj[0]=firstlod?new ObjectData(*firstlod):new ObjectData();
  _resolutions[0]=resolution;
  _n=1;
  _active=0;
  _dirty=false;
  }

//--------------------------------------------------

void LODObject::DoConstructEmpty()
  {
  _n=0;
  _dirty=false;
  _active=0;
  }

//--------------------------------------------------

void LODObject::DoConstruct( const LODObject &src )
  {
  int i;
  for(i=0; i<src._n; i++ )
    {
    _obj[i]=new ObjectData(*src._obj[i]);
    _resolutions[i]=src._resolutions[i];
    }
  _active=src._active;
  _n=i;
  _dirty=false;
  }

//--------------------------------------------------

void LODObject::DoDestruct()
  {
  for( int i=0; i<_n; i++ ) delete _obj[i],_obj[i]=NULL;
  _n=0;
  }

//--------------------------------------------------

bool LODObject::RemoveLevel( int level )
  {
  if( _n<=1 ) return false;
  if( _obj[level] ) return false;
  for( int i=level+1; i<_n; i++ ) _obj[i-1]=_obj[i],_resolutions[i-1]=_resolutions[i];
  _n--;
  _dirty=true;
  return true;
  }

//--------------------------------------------------

ObjectData *LODObject::DetachLevel(int level) //remove level and returns pointer
  {
  if( _n<=1 ) return NULL;
  ObjectData *ret=_obj[level];
  _obj[level]=NULL;
  if (RemoveLevel(level)==false) return NULL;
  return ret;
  }

//--------------------------------------------------

int LODObject::InsertLevel( ObjectData *obj, float resolution )
  {
  if( _n>=MAX_LOD_LEVELS ) return -1;
  int i,j;
  for( i=0; i<_n; i++ ) if( _resolutions[i]>resolution ) break;
  // insert to position i
  for( j=_n; j>i; j-- ) _obj[j]=_obj[j-1],_resolutions[j]=_resolutions[j-1];
  _obj[i]=obj;
  _resolutions[i]=resolution;
  _n++;
  _dirty=true;
  if( _active>i ) _active++;
  return i;
  }

//--------------------------------------------------

bool LODObject::DeleteLevel( int level )
  {
  if( _n<=1 ) return false;
  delete _obj[level];
  _obj[level]=NULL;
  if( _active>=level && _active>0 ) _active--;
  return RemoveLevel(level);
  }

//--------------------------------------------------

int LODObject::AddLevel( const ObjectData &obj, float resolution )
  {
  if( _n>=MAX_LOD_LEVELS ) return -1;
  // levels are sorted by increasing resolution
  ObjectData *ins=new ObjectData(obj);
  return InsertLevel(ins,resolution);
  }

//--------------------------------------------------

int LODObject::FindLevel( float resolution ) const
  {
  float minDif=1e20f;
  int minI=-1;
  for( int i=0; i<_n; i++ )
    {
    float dif=fabs(resolution-_resolutions[i]);
    if( minDif>dif ) minDif=dif,minI=i;
    }
  return minI;
  }

//--------------------------------------------------
// bxbx fix: epsilon 0.001 is not able recognize shadowwolume_0 and shadowwolume_10

int LODObject::FindLevelExact( float resolution ) const
  {
  float minDif=fabs(resolution)*0.00001f;
  int minI=-1;
  for( int i=0; i<_n; i++ )
    {
    float dif=fabs(resolution-_resolutions[i]);
    if( minDif>=dif ) minDif=dif,minI=i;
    }
  return minI;
  }



//--------------------------------------------------

int LODObject::SetResolution( int level, float newRes )
  {
  _dirty=true;
  if( _n>1 )
    {
    ObjectData *levelData=_obj[level];
    // delete on old position
    bool isActive=( level==_active );
    _obj[level]=NULL;
    bool res;
    res=RemoveLevel(level); // always true
    //WVERIFY( res );
    int newLevel=InsertLevel(levelData,newRes); // always true
    //WVERIFY( newLevel>=0 );
    if( isActive ) _active=newLevel;
    return newLevel;
    }
  else
    {
    //WVERIFY( level==0 );
    _resolutions[level]=newRes;
    return level;
    }
  }

//--------------------------------------------------

bool LODObject::ChangeResolution( float oldRes, float newRes )
  {
  int level=FindLevel(oldRes);
  if( level<0 ) return false;
  // change ordering
  return SetResolution(level,newRes)>=0;
  }

//--------------------------------------------------

//#include "release\resource.h"



//--------------------------------------------------

void LODObject::ClearDirty()
  {
  for( int i=0; i<_n; i++ )
    {
    _obj[i]->ClearDirty();
    }
  _dirty=false;
  }

//--------------------------------------------------

void LODObject::SetDirty()
  {
  _dirty=true;
  }

//--------------------------------------------------

bool LODObject::Dirty()
  {
  for( int i=0; i<_n; i++ )
    {
    if( _obj[i]->Dirty() ) return true;
    }
  return _dirty;
  }

//--------------------------------------------------


//--------------------------------------------------

int LODObject::Save( ostream &f, int version, bool final ) const
  {
  //f.write("NLOD",4);
  f.write("MLOD",4);
  int ver=0x101;
  f.write((char *)&ver,sizeof(ver)); // save format version number
  int n=NLevels();
  f.write((char *)&n,sizeof(n));
  for( int i=0; i<NLevels(); i++ )
    {
    int ret=0;
    // object should be followed by resolution info
    float resol=Resolution(i);
    //bool saveMass=( i==0 );
    // new objects have mass stored in geometry level 
    bool saveMass=(resol==LOD_GEOMETRY || resol==-LOD_GEOMETRY 
      || resol == LOD_GEOMETRY_PHYS || resol == -LOD_GEOMETRY_PHYS
      || resol == LOD_GEOMETRY_PHYS_OLD || resol == -LOD_GEOMETRY_PHYS_OLD
      );
    if( _obj[i]->SaveBinary(f,version,final,saveMass)<0 ) return -1;
    f.write((char *)&resol,sizeof(resol));
    if( f.fail() ) return -1;
    }
  return 0;
  }

//--------------------------------------------------

int LODObject::Save(const Pathname& filename, int version, bool final, SaveCallBackSt callback, void *context ) const
  {

  ObjectData *obj = this->Active();
  const char *binarized = obj->GetNamedProp("-binarized");
  if (binarized && _stricmp(binarized,"1")==0)
      return -1;

  RString ext=filename.GetExtension();
  if( ext[0]=='.' && ext[1]=='a' )
    {
//    return _obj[0]->SaveGenAsc(filename);
    }
  ofstream f;
  f.open(filename,ios::out|ios::binary|ios::trunc);
  if (f.fail()) return -1;
  if( Save(f,version,final)<0 ) return -1;
  // after everything is saved, save Objektiv configuration
  if( callback )
    {
    if( !callback(f,context) ) return -1;
    }
  // do not clear dirty - save may be export or save for object viewer ...
  //_dirty=false;
  #if 0 // all setup files have been removed
  // remove old setup file
  filename.SetExtension(".set3d");
  remove(filename);
  #endif
  return 0;
  }

//--------------------------------------------------

int LODObject::Load(const Pathname& filename, LoadCallBackSt callback, void *context )
  {
    if (filename.IsNull()) return -1;
/*  if( !_strcmpi(filename.GetExtension(),".3ds") )
    {
    ObjectData *obj=new ObjectData();
    if( obj->Load3DS(filename)<0 ) goto Error;
    AddLevel(*obj,0.0f);
    delete obj;
    }
  else if( _strcmpi(filename.GetExtension(),OBJ_EXT) )
    {
    // only p3d format can handle LOD levels
    // other formats read only one level
    ObjectData *obj=new ObjectData();
    if( obj->Load(filename)<0 ) goto Error;
    AddLevel(*obj,0.0f);
    delete obj;
    }
  else*/
    {
    ifstream f;
    f.open(filename,ios::in|ios::binary);
    if( f.fail() ) return -1;
    return Load(f,callback,context);
    }
}

int LODObject::Load(istream &f, LoadCallBackSt callback, void *context ) {

    DoDestruct();
    DoConstructEmpty();

    char magic[4];
    int ver=-1; // version number
    f.read(magic,sizeof(magic));
    if( f.fail() || f.eof() ) return -1;
    int n=MAX_LOD_LEVELS;
    bool isLOD=false;
    if( !strncmp(magic,"NLOD",sizeof(magic)) )
      {
      f.read((char *)&n,sizeof(n));
      isLOD=true;
      ver=0; // no version in NLOD format
      }
    else if( !strncmp(magic,"MLOD",sizeof(magic)) )
      {
      // MLOD format contains version number
      f.read((char *)&ver,sizeof(ver));
      if (ver==9999) ver=1;
      isLOD=true;
      f.read((char *)&n,sizeof(n));
      }
    else if ( strncmp(magic,"ODOL",sizeof(magic))==0)
    {
        return LoadOdol(f);
    }
    else 
        return -1;
    ProgressBar<int> progress(n);
    for( int i=0; i<n; i++ )
      {
      progress.SetNextPos(i+1);
      int ret=0;
      ObjectData *obj=new ObjectData();
      // object should be followed by resolution info
      float resol;
      streampos before=f.tellg();
      if( obj->LoadBinary(f)<0 )
        {
        delete obj;
        if( i==0 ) goto Error;
        f.seekg(before);
        break; // last LOD already loaded
        }
      f.read((char *)&resol,sizeof(resol));
      if( f.fail() || f.eof() ) resol=0;      
      AddLevel(*obj,resol);
      delete obj;
      if( f.fail() || f.eof() ) break;
      }
    if( !isLOD && _n==1 && ver<0 )
      {
/*      // try to load setup from old .set3d file
      ifstream setup;
      Pathname pfilename=filename;
      pfilename.SetExtension(".set3d");
      setup.open(pfilename,ios::in|ios::binary);
      if( !f.fail() && !f.eof() )
        {
        _obj[0]->LoadSetup(f);
        }
        */
          return -1;
      }
    if( callback )
      {
      if( !callback(f,context) ) return -1;
      }
    // if there is mass info in LOD 0, convert it to geometry level
    int gIndex=FindLevelExact(LOD_GEOMETRY);
    if( gIndex>0 && ver==0 )
      { // geometry exists and is different from LOD 0
      bool oldMass=false,newMass=false;
      ObjectData *lod0=Level(0);
      ObjectData *lodG=Level(gIndex);
      if( lodG ) lodG->_mass.Validate(lodG->NPoints());
      int i,j;
      for( i=0; i<lod0->NPoints(); i++ )
        {
        if( lod0->_mass[i]>0 ) oldMass=true;
        }
      if( oldMass )
        {
        for( i=0; i<lodG->NPoints(); i++ )
          {
          lodG->_mass[i]=0;
          }
        // some old mass defined
        // move old mass to new mass and delete old mass information
        // distibute mass information of all lod0 vertices
        for( i=0; i<lodG->NPoints(); i++ )
          {
          if( lodG->_mass[i]>0 ) newMass=true;
          }
        for( i=0; i<lod0->NPoints(); i++ )
          {
          double sumInvDist2=0;
          const Vector3 &pos0=lod0->Point(i);
          for( j=0; j<lodG->NPoints(); j++ )
            {
            const Vector3 &posG=lodG->Point(j);
            float dist2=(posG-pos0).SquareSize();
            //float dist2=(posG-pos0).Size();
            if( dist2<=1e-3 )
              {
              // move whole mass to nearest point
              lodG->_mass[j]+=lod0->_mass[i];
              goto Distibuted;
              }
            sumInvDist2+=1/dist2;
            }
          double mass0divSumInvDist2;
          mass0divSumInvDist2=lod0->_mass[i]/sumInvDist2;
          float sumMass;
          sumMass=0;
          for( j=0; j<lodG->NPoints(); j++ )
            {
            const Vector3 &posG=lodG->Point(j);
            float invDist2=1/(posG-pos0).SquareSize();
            //float invDist2=1/(posG-pos0).Size();
            // distribute between points by distance
            lodG->_mass[j]+=(float)(invDist2*mass0divSumInvDist2);
            sumMass+=(float)(invDist2*mass0divSumInvDist2);
            }
          Distibuted:
          lod0->_mass[i]=0;
          }
        // reset mass in all lods but geometry
        for( j=1; j<NLevels(); j++ ) if( j!=gIndex )
          {
          ObjectData *lodJ=Level(j);
          for( i=0; i<lodJ->_mass.N(); i++ )
            {
            lodJ->_mass[i]=0;
            }
          }
        }
      
      }
    
  ClearDirty();
  // after everything is loaded, load Objektiv configuration
  return 0;
  Error:
  DoDestruct();
  DoConstruct();
  return -1;
  }

//--------------------------------------------------

bool LODObject::Merge( const LODObject &src , bool createlods, const char *createSel, bool fillLods)
  {
  if (createlods)
    {
    for (int i=0;i<src.NLevels();i++)
      {
      float level=src.Resolution(i);
      int index=FindLevelExact(level);
      if (index==-1) 
        {
          
          if (fillLods )
          {
            int fromIndx = FindLevel(level);
            if (_resolutions[fromIndx] < 999)
            {
              AddLevel(*_obj[fromIndx],level);
            }
            else
            {
              ObjectData obj;
              AddLevel(obj,level);
            }           
          }
          else
          {
            ObjectData obj;
            AddLevel(obj,level);
          }        
        }
      }
    }
  
  // merge all LOD levels
  for( int i=0; i<_n; i++ )
    {
    // find nearest LOD level of src
      int index;
      if (_resolutions[i] < 999)
        index=src.FindLevel(_resolutions[i]);
      else 
        index=src.FindLevelExact(_resolutions[i]);

    if( index>=0 )
      {
      if( !_obj[i]->Merge(*src._obj[index],createSel) ) return false;
      }
    }
  return true;
  }

//--------------------------------------------------

Vector3 LODObject::CenterAll()
  {
  Vector3 minP, maxP;
  GetAABB(minP, maxP);
  Vector3 offset=(minP+maxP)*0.5;
  for( int level=0; level<_n; level++ )
    {
    ObjectData *obj=_obj[level];
    for( int i=0; i<obj->NPoints(); i++ )
      {
      obj->Point(i)-=offset;
      // change also also all animations
      for( int a=0; a<obj->NAnimations(); a++ )
        {
        AnimationPhase *phase=obj->GetAnimation(a);
        (*phase)[i]-=offset;
        }
      }
    }
    return offset;
  }

//--------------------------------------------------

void LODObject::GetAABB(Vector3& minPoint, Vector3& maxPoint)
{
  Vector3 minP(+1e10,+1e10,+1e10);
  Vector3 maxP(-1e10,-1e10,-1e10);
  for( int level=0; level<_n; level++ )
  {
    ObjectData *obj=_obj[level];
    for( int i=0; i<obj->NPoints(); i++ )
    {
      Vector3 pos=obj->Point(i);
      CheckMinMax(minP,maxP,pos);
      // scan also all animations
    }
  }
  minPoint = minP;
  maxPoint = maxP;
}

//--------------------------------------------------

#pragma pack(1)
    struct Header {
        int special;
        float boundingSphere;
        float geometrySphere;
        int remarks;
        int andHints;
        int orHints;
        Vector3 aimingCenter;
        unsigned __int32 color;
        unsigned __int32 colorTop;
        float viewDensity;
        Vector3 minMax[2];
        Vector3 boundingCenter;
        Vector3 geometryCenter;
        Vector3 centerOfMass;
        Matrix3 invInertia;
        bool autoCenter;
        bool lockAutoCenter;
        bool canOcclude;
        bool canBeOccluded;
        bool allowAnimation;
    };
#pragma pack()

int LODObject::LoadOdol(istream &f)
{
    int version = 0;
    f.read(reinterpret_cast<char *>(&version),sizeof(version));
    int nres = 0;
    f.read(reinterpret_cast<char *>(&nres),sizeof(nres));

    AutoArray<float> reslist;
    reslist.Resize(nres);
    f.read(reinterpret_cast<char *>(reslist.Data()),sizeof(float)*nres);


    Header hdr;
    f.read(reinterpret_cast<char *>(&hdr),sizeof(hdr));

    ObjectData obj;
    obj.ReservePoints(8);
    for (int i = 0; i < 8; i++)
    {
        PosT &ps = obj.Point(i);
        ps[0] = hdr.minMax[i & 1][0];
        ps[1] = hdr.minMax[(i>>1) & 1][1];
        ps[2] = hdr.minMax[(i>>2) & 1][2];
        ps.SetPoint(ps + hdr.boundingCenter);
    }

    int faces[][4] = 
        {
            {0,1,3,2},
            {0,2,6,4},
            {3,1,5,7},
            {7,5,4,6}, 
            {1,0,4,5},
            {2,3,7,6}
        };

   for (int i = 0 ; i<6; i++)
   {
       FaceT fc;
       fc.CreateIn(&obj);
       for (int j = 0; j<4; j++)
           fc.SetPoint(j,faces[i][j]);
       fc.SetN(4);
   }

   Selection sel(&obj);
   sel.SelectAll();
   obj.SaveNamedSel("Binarized model, cannot import - loaded fake for some calculations",&sel);

   char buff[50];

   obj.SetNamedProp("autocenter",hdr.autoCenter?"1":"0");
   obj.SetNamedProp("-lockAutoCenter",hdr.lockAutoCenter?"1":"0");
   obj.SetNamedProp("-canOcclude",hdr.canOcclude?"1":"0");
   obj.SetNamedProp("-canBeOccluded",hdr.canBeOccluded?"1":"0");
   obj.SetNamedProp("-allowAnimation",hdr.allowAnimation?"1":"0");
   sprintf(buff,"%g",hdr.boundingSphere);
   obj.SetNamedProp("-spehere",buff);
   sprintf(buff,"%g",hdr.viewDensity);
   obj.SetNamedProp("-viewDensity",buff);
   obj.SetNamedProp("-binarized","1");

   obj.RecalcNormals();

   for (int i = 0; i<reslist.Size(); i++)
   {
       AddLevel(obj,reslist[i]);
   }

   return 0;
}

}