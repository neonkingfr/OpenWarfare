
#pragma once

namespace ObjektivLib {


class ObjectData;

template <class AType>
class PointAttrib: public RefCount
  {
  friend class ObjectData;
  
  protected:
    
    ObjectData *_object;
    
    AutoArray<AType> _attrib;
    
  public:
    PointAttrib( ObjectData *object )
      {
      _object=object;
      }
    PointAttrib( const PointAttrib &src );
    PointAttrib( const PointAttrib &src, ObjectData *object );
    
    AType &operator [] ( int i )
      {
      return _attrib[i];
      }
    const AType &operator [] ( int i ) const
      {
      return _attrib[i];
      }
    
    void Validate( int i=-1 );
    
    void Clear();
    void Clear( int i );
    
    void DeletePoint( int index );
    
    void PermuteVertices( const int *permutation );
    
    void Save( std::ostream &f );
    void Load( std::istream &f, int nVert );
    
    int N() const;
    
    void SetObject( ObjectData *object ) 
      {_object=object;}
  };

}