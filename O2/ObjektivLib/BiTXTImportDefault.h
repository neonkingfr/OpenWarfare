#pragma once

#include "BiTXTImport.h"

namespace ObjektivLib {

class CBiTXTImportDefault:public CBiTXTImport
{
public:
  virtual float LODNameToResol(const char *name);
  virtual RStringB LODResolToName(float resol) const;
};

}