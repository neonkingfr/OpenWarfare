#ifndef __OBJ_TOOL_SECTIONS_CLASS_HEADER_
#define __OBJ_TOOL_SECTIONS_CLASS_HEADER_
#include "objectdata.h"
#include <el/BTree/BTree.h>




#define DEFINE_COMPARE_OPERATOR(oper)  bool operator oper (const ObjToolSectionInfo& other) const {return compare(other) oper 0;}

namespace ObjektivLib {


class ObjToolSections :  public ObjectData
{
public:
    struct ObjToolSectionInfo
    {
      const char *ptrTexture;   
      const char *ptrMaterial;
      bool IsAlpha;
      int alphaId;
      unsigned long flags;
      int count;

      int compare(const ObjToolSectionInfo& other) const;
      DEFINE_COMPARE_OPERATOR(==)
      DEFINE_COMPARE_OPERATOR(>)
      DEFINE_COMPARE_OPERATOR(<)
      DEFINE_COMPARE_OPERATOR(>=)
      DEFINE_COMPARE_OPERATOR(<=)
      DEFINE_COMPARE_OPERATOR(!=)

      int operator=(int zero);
      void *PackSectionInfo(void *preData=NULL, int size=0);
      void UnpackSectionInfo(const void *packed, int preDataSize);
    };

    void CalculateSections(BTree<ObjToolSectionInfo> &list, bool selonly=false);
    /**
    @param list List of sections created by CalculateSections.
    @param array Allocated array in size list.Size() pointers.
    @return count of items, same value asi list.Size();
    */
    static int BuildSectionArray(BTree<ObjToolSectionInfo> &list, ObjToolSectionInfo **array);
    void SelectSection(void *packedSectionInfo, int preDataSize, bool addsel, Selection *out=NULL);
    void SelectSection(const ObjToolSectionInfo &nfo, bool addsel, Selection *out=NULL);

    void SortTextures();

};

}
#undef DEFINE_COMPARE_OPERATOR


#endif