#pragma once


#ifndef __RSTRINGB_MT_HPP__
#define __RSTRINGB_MT_HPP__

#ifdef RStringB
#undef RStringB
#endif

#include <es/strings/rstring.hpp>
class RStringB_MT_DestructUnlock
{
public:
  RStringB_MT_DestructUnlock() {}
  ~RStringB_MT_DestructUnlock();
};

class RStringB_MT :public RStringB_MT_DestructUnlock, public RStringB
{
public:
public:
  RStringB_MT() {}
  RStringB_MT( RString str );
  RStringB_MT( RStringS str );
  RStringB_MT( const char *str );

  RStringB_MT( const RStringB_MT &src );
  RStringB_MT &operator=( const RStringB_MT &src );
  RStringB_MT &operator=( const char *src );
  RStringB_MT &operator=( const RString &src );
  ~RStringB_MT();

  bool operator == ( const RStringB_MT &with ) const;
  bool operator != ( const RStringB_MT &with ) const;

  ClassIsMovable(RStringB_MT);
};

#define RStringB RStringB_MT
#endif