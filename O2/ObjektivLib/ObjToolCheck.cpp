#include "common.hpp"
#include "ObjToolCheck.h"

namespace ObjektivLib {


void ObjToolCheck::CheckFaces()
  {
  // check all faces for planarity
  // stop on:
  // non-planar geometry    
  // non-linear texture coordinates
  int i;
  for( i=0; i<NFaces(); i++ )
    {
    FaceT face(this,i);
    if( face.N()==3 )
      {
      FaceSelect(i,false);
      continue;
      }
    if( face.N()<3 )
      {
      FaceSelect(i,true);
      continue;
      }
    // 4 point face - calculate plane normal and test for planarity
    const PosT &p0=Point(face.GetPoint(0));
    const PosT&p1=Point(face.GetPoint(1));
    const PosT &p2=Point(face.GetPoint(2));
    const PosT &p3=Point(face.GetPoint(3));
    Vector3 p10=(Vector3)p1-(Vector3)p0;
    Vector3 p20=(Vector3)p2-(Vector3)p0;
    Vector3 p13=(Vector3)p1-(Vector3)p3;
    Vector3 p23=(Vector3)p2-(Vector3)p3;
    Vector3 normal0=p10.CrossProduct(p20);
    Vector3 normal3=p13.CrossProduct(p23);
    // if plane is planar, both normals should be same
    // calculate cos(fi) - angle between normals
    double cosFi=normal0*normal3/(normal0.Size()*normal3.Size());
    if( cosFi<0.996 ) // 0.996 is cos(5 degree)
      {
      FaceSelect(i,true);
      continue;
      }
    FaceSelect(i,false);
    }
  SelectPointsFromFaces();
  }

struct UVPair
  {
  float u,v;
  };

bool GenerateST(Vector3Par anormal, Vector3Par apos, Vector3Par bpos, Vector3Par cpos,
                const UVPair &at, const UVPair &bt, const UVPair &ct,
                Vector3 &outS, Vector3 &outT)
{
  float k = bt.u - at.u;
  float l = bt.v - at.v;
  float m = ct.u - at.u;
  float n = ct.v - at.v;
  Vector3 v1 = bpos - apos;
  Vector3 v2 = cpos - apos;
  float kn = k * n;
  float lm = l * m;
  float d = lm - kn;

  // Test the vectors to be lineary dependent  
  if (fabs(d) < 1e-8)
  {
    // First try s to be (1,0,0) vector
    Vector3 s(1, 0, 0);
    float t = s * anormal;
    s = s - anormal * t;
    if (fabs(s.SquareSize()) < FLT_MIN)
    {
      s.Normalize();
    }
    else
    {
      // (1,0,0) failed, so (0,1,0) must work
      s = Vector3(0, 1, 0);
      t = s * anormal;
      s = (s - anormal * t).Normalized();
    }

    // Write out some reasonable values
    outS = s;
    outT = anormal.CrossProduct(s);

    // The vectors in the texture are lineary dependent
    return false;
  }

  // U to S
  outS = (-n * v1 * d + l * v2 * d).Normalized();
/*
  if (d > 0.0f)
  {
    outS = (-n * v1 + l * v2).Normalized();
  }
  else
  {
    outS = (n * v1 - l * v2).Normalized();
  }
*/

  // V to T
  outT = (m * v1 * d - k * v2 * d).Normalized();
/*
  if (d < 0.0f)
  {
    outT = (-m * v1 + k * v2).Normalized();
  }
  else
  {
    outT = (m * v1 - k * v2).Normalized();
  }
*/
  // Normalize the outS and outT using the anormal
  // The reference vector is the normal - the outS and outT must be orthogonal to it

  // Get the middleST vector and make it orthogonal to normal
  Vector3 middleST = (outS + outT) * 0.5f;
  float t = middleST * anormal;
  middleST = middleST - anormal * t;
  if (fabs(middleST.SquareSize()) < FLT_MIN)
  {
    // The middle ST is lineary dependent on normal
    return false;
  }
  middleST.Normalize();

  // Get vector orthogonal to the middleST
  Vector3 orthoMiddleST = anormal.CrossProduct(middleST);

  if (orthoMiddleST.DotProduct(outS) > 0.0f)
  {
    // Get outS and outT from the middle of the middleST and orthoModdleST
    outS = (orthoMiddleST + middleST).Normalized();
    outT = (-orthoMiddleST + middleST).Normalized();
  }
  else
  {
    // Get outS and outT from the middle of the middleST and orthoModdleST
    outS = (-orthoMiddleST + middleST).Normalized();
    outT = (orthoMiddleST + middleST).Normalized();
  }

  return true;
}

void ObjToolCheck::CheckSTVectors()
  {
  for (int i=0;i<NFaces();i++)
    {
    FaceT fc(this,i);
    if (fc.N()==3)
      {
      PosT p1=Point(fc.GetPoint(0));
      PosT p2=Point(fc.GetPoint(1));
      PosT p3=Point(fc.GetPoint(2));
      VecT anorm=Normal(fc.GetNormal(0));
      UVPair at,bt,ct;
      at.u=fc.GetU(0);at.v=fc.GetV(0);
      bt.u=fc.GetU(1);bt.v=fc.GetV(1);
      ct.u=fc.GetU(2);ct.v=fc.GetV(2);
      Vector3 out1,out2;
      bool res=GenerateST(anorm,p1,p2,p3,at,bt,ct,out1,out2);
      if (!res) FaceSelect(i);
      continue;
      }
    else
      {
      int a=2;
      int b=3;
      int c;
      for (c=0;c<4;c++)
        {
        PosT p1=Point(fc.GetPoint(a));
        PosT p2=Point(fc.GetPoint(b));
        PosT p3=Point(fc.GetPoint(c));
        VecT anorm=Normal(fc.GetNormal(0));
        UVPair at,bt,ct;
        at.u=fc.GetU(a);at.v=fc.GetV(a);
        bt.u=fc.GetU(b);bt.v=fc.GetV(b);
        ct.u=fc.GetU(c);ct.v=fc.GetV(c);
        Vector3 out1,out2;
        bool res=GenerateST(anorm,p1,p2,p3,at,bt,ct,out1,out2);
        if (!res) 
          {
          FaceSelect(i);
          break;
          }
        a=b;
        b=c;
        }   
      if (c!=4) continue;
      }
    FaceSelect(i,false);
    }
  SelectPointsFromFaces();
  }


//--------------------------------------------------

void ObjToolCheck::IsolatedPoints()
  {
  ClearSelection();
  int i;
  for( i=0; i<NFaces(); i++ ) FaceSelect(i,true);
  SelectPointsFromFaces();
  
  // invert selection
  for( i=0; i<NPoints(); i++ ) PointSelect(i,!PointSelected(i));
  //for( i=0; i<NFaces(); i++ ) FaceSelect(i,!FaceSelected(i));
  for( i=0; i<NFaces(); i++ ) FaceSelect(i,false);
  }

//--------------------------------------------------

#define UV_COS_LIMIT 0.999 // cos(2 degree)

void ObjToolCheck::CheckMapping()
  {
  int i;
  for( i=0; i<NFaces(); i++ )
    {
    FaceT face(this,i);
    if( face.N()<=3 )
      {
      FaceSelect(i,false);
      continue;
      }
    // check if u,v are linear
    // all calculations are performed in planar space defined by face normal
    Vector3 normal=face.CalculateNormal();
    double sqrtArea=sqrt(face.CalculateArea());
    Vector3 p0=Point(face.GetPoint(0));
    Vector3 p1=Point(face.GetPoint(1));
    Vector3 p2=Point(face.GetPoint(2));
    Vector3 p3=Point(face.GetPoint(3));
    // normal defines direction up
    // direction forward is defined by (p1-p0)
    Vector3 dir=p1-p0;
    dir.Normalize();
    Vector3 aside=dir.CrossProduct(normal);
    // aside is lineX, normal is lineY, dir is lineZ
    // convert to linear space
    // y is assumed to be same for all vertices (face should be planar)
    Vector3 l0(p0*aside,0,p0*dir);
    Vector3 l1(p1*aside,0,p1*dir);
    Vector3 l2(p2*aside,0,p2*dir);
    Vector3 l3(p3*aside,0,p3*dir);
    Vector3 normal0,normal3;
    // check planarity for u
    l0[1]=(Coord)(face.GetU(0)*sqrtArea);
    l1[1]=(Coord)(face.GetU(1)*sqrtArea);
    l2[1]=(Coord)(face.GetU(2)*sqrtArea);
    l3[1]=(Coord)(face.GetU(3)*sqrtArea);
    normal0=(l1-l0).CrossProduct(l2-l0);
    normal3=(l1-l3).CrossProduct(l2-l3);
    // if plane is planar, both normals should be same
    // calculate cos(fi) - angle between normals
    double cosFi=normal0*normal3/(normal0.Size()*normal3.Size());
    if( cosFi<UV_COS_LIMIT )
      {
      FaceSelect(i,true);
      continue;
      }
    // check planarity for v
    l0[1]=(Coord)(face.GetV(0)*sqrtArea);
    l1[1]=(Coord)(face.GetV(1)*sqrtArea);
    l2[1]=(Coord)(face.GetV(2)*sqrtArea);
    l3[1]=(Coord)(face.GetV(3)*sqrtArea);
    normal0=(l1-l0).CrossProduct(l2-l0);
    normal3=(l1-l3).CrossProduct(l2-l3);
    // if plane is planar, both normals should be same
    // calculate cos(fi) - angle between normals
    cosFi=normal0*normal3/(normal0.Size()*normal3.Size());
    if( cosFi<UV_COS_LIMIT )
      {
      FaceSelect(i,true);
      continue;
      }
    FaceSelect(i,false);
    }
  SelectPointsFromFaces();
  }



struct SortFaceT
{
  FaceTraditional *_face;
  int _index;
  float _minY; // order by y
  bool _toDelete;
  SortFaceT():_toDelete(false)
  {}
  SortFaceT(FaceTraditional &face, int index, float minY )
    :_face(&face),_index(index),_toDelete(false),_minY(minY)
    {
    }
  ClassIsSimpleZeroed(SortFaceT);
};



static int CmpFaceVert( const void *f0, const void *f1 )
{
  // alpha faces should be drawn last
  int diff;
  int j;
  const FaceTraditional *d0=((const SortFaceT *)f0)->_face;
  const FaceTraditional *d1=((const SortFaceT *)f1)->_face;
  
  diff=d0->n-d1->n;
  if( diff ) return diff;
  for( j=0; j<d0->n; j++ )
  {
    diff=d0->vs[j].point-d1->vs[j].point;
    if( diff ) return diff;
  }
  return 0;
}

static int CmpFaceText( const void *f0, const void *f1 )
{
  const SortFaceT *d0=(const SortFaceT *)f0;
  const SortFaceT *d1=(const SortFaceT *)f1;
  
  float diff=d1->_minY-d0->_minY;
  if( diff<0 ) return -1;
  if( diff>0 ) return +1;
  // do not sort alpha textures
  if( d1->_minY<-1e5 ) return 0;
  // first go merged textures, than non-merged
  int d;
  int d0merged = (d0->_face->flags&FACE_DISABLE_TEXMERGE)==0;
  int d1merged = (d1->_face->flags&FACE_DISABLE_TEXMERGE)==0;
  d = d0merged - d1merged;
  if (d) return d;
  
  // check texture name
  // different textures may have same minY
  d = _stricmp(d0->_face->vTexture,d1->_face->vTexture);
  if (d) return d;
  // the texture is same - use vertex indices
  return CmpFaceVert(f0,f1);
}

/**/
static int CmpFaceIndex( const void *f0, const void *f1 )
{
  const SortFaceT *d0=(const SortFaceT *)f0;
  const SortFaceT *d1=(const SortFaceT *)f1;
  int diff=d0->_index-d1->_index;
  return diff;
}

/**/

static void BubbleSort( void *data, int nElem, int sizeElem, int compare( const void *e1, const void *e2 ) )
{
  bool change;
  Temp<char> buf(sizeElem);
  do
  {
    char *e1=(char *)data;
    char *e2=e1+sizeElem;
    change=false;
    for( int i=1; i<nElem; i++ )
    {
      if( compare(e1,e2)>0 )
      {
        memcpy(buf,e1,sizeElem);
        memcpy(e1,e2,sizeElem);
        memcpy(e2,buf,sizeElem);
        change=true;
      }
      e1+=sizeElem;
      e2+=sizeElem;
    }
  } while( change );
}

struct SortVertex
{
  //ObjectData *obj;
  Vector3 p;
  int index;
  ClassIsSimpleZeroed(SortVertex);
};


static int CmpSortVertex( const void *vv1, const void *vv2 )
{
  const SortVertex &v1=*(const SortVertex *)vv1;
  const SortVertex &v2=*(const SortVertex *)vv2;
  // sort by y
  float dif;
  const Vector3 &p1=v1.p;
  const Vector3 &p2=v2.p;
  dif=p1.Y()-p2.Y();
  if( dif<0 ) return +1;if( dif>0 ) return -1;
  dif=p1.X()-p2.X();
  if( dif<0 ) return +1;if( dif>0 ) return -1;
  dif=p1.Z()-p2.Z();
  if( dif<0 ) return +1;if( dif>0 ) return -1;
  return 0;
}

struct SortTexture
{
  const char *name;
  float minY;
  ClassIsSimple(SortTexture);
};


class PreSortTextures: public AutoArray<SortTexture>
{
  public:
    void NewFace( ObjectData *obj, const FaceT &face );
    int FindTexture( const char *name );
    float MinY( const char *name );
};

int PreSortTextures::FindTexture( const char *name )
{
  for( int i=0; i<Size(); i++ )
  {
    if( !_strcmpi(name,(*this)[i].name) ) return i;
  }
  return -1;
}

typedef DWORD __stdcall TextureColorARGBT( const char *s );


static TextureColorARGBT *TextureColorARGB;


void PreSortTextures::NewFace( ObjectData *obj, const FaceT &face )
{
  const char *name=face.GetTexture();
  float minY=1e10;
  for( int i=0; i<face.N(); i++ )
  {
    float y=obj->Point(face.GetPoint(i)).Y();
    if( minY>y ) minY=y;
  }
  int index=FindTexture(name);
  if( index>=0 )
  {
    // alpha can be marked once
    if( (*this)[index].minY>minY ) (*this)[index].minY=minY;
  }
  else
  {
    bool alpha=TextureIsAlpha(name);
    if( alpha ) minY=-1e10; // draw last
    index=Add();
    (*this)[index].minY=minY;
    (*this)[index].name=name;
  }
}

float PreSortTextures::MinY( const char *name )
{
  int index=FindTexture(name);
  if( index<0 ) return 0;
  return (*this)[index].minY;
}

void ObjToolCheck::Optimize()
{
  // remove any unnecessary faces, ....
  RecalcNormals(false);  
  int i;
  
  // remove all czech characters from all names
/*  int i;
  for( i=0; i<NFaces(); i++ )
  {
    FaceT &face=Face(i);
    //if( 
    char buf[1024];
    if( DeCzech(buf,face.vTexture) )
    {
      WFilePath fullOld,fullNew;
      fullOld=DataRoot+face.vTexture;
      fullNew=DataRoot+buf;
      MoveFile(fullOld,fullNew);
      face.vTexture=buf;
    }
  }*/
  
  // remove any identical points
  MergePoints(0,false);
  
  // sort all vertex indices
  {
    // sort vertices so that first is topmost
    Temp<SortVertex> sortVertices(NPoints());
    for( i=0; i<NPoints(); i++ )
    {
      sortVertices[i].index=i;
      sortVertices[i].p=Point(i);
    }
    
    //QSort((SortVertex *)sortVertices,NPoints(),CmpSortVertex);
    qsort((SortVertex *)sortVertices,NPoints(),sizeof(*sortVertices),CmpSortVertex);
    Temp<int> permutVertices(NPoints());
    for( i=0; i<NPoints(); i++ )
    {
      permutVertices[sortVertices[i].index]=i;
      //permutVertices[i]=sortVertices[i].index;
    }
    PermuteVertices(permutVertices);
  }
  
  // sort all faces so that first vertex index is the lowest one
  for( i=0; i<NFaces(); i++ )
  {
    FaceT face(this,i);
    int minP=INT_MAX,minJ=0;
    int j;
    for( j=0; j<face.N(); j++ )
    {
      if( face.GetPoint(j)<minP ) minP=face.GetPoint(j),minJ=j;
    }
    FaceT temp=face;
    for( j=0; j<face.N(); j++ )
    {
      int src=ModuloCounting(j+minJ,face.N());
      face.CopyPointInfo(j,temp,src);
    }
  }
  
  // pre-sort textures
  PreSortTextures sortTextures;
  for( i=0; i<NFaces(); i++ ) sortTextures.NewFace(this,FaceT(this,i));
  
  // sort faces by texture name
  Temp<SortFaceT> sortFace(NFaces());
  for( i=0; i<NFaces(); i++ )
  {
    sortFace[i]=SortFaceT(Face(i),i,sortTextures.MinY(Face(i).vTexture));
  }
  // use quick sort - result is not stored anywhere
  qsort(sortFace,NFaces(),sizeof(*sortFace),CmpFaceVert);
  // remove any duplicate faces
  /**/
  for( i=0; i<NFaces(); i++ )
  {
    SortFaceT &sFace=sortFace[i];
    FaceTraditional &face=*sFace._face;
    if( face.n<3 ) sFace._toDelete=true;
  }
  for( i=1; i<NFaces(); i++ )
  {
    SortFaceT &face=sortFace[i];
    SortFaceT &prev=sortFace[i-1];
    if( !CmpFaceVert(&face,&prev) )
    {
      face._toDelete=true;
    }
  }
  
  // sort faces to delete to end
  qsort(sortFace,NFaces(),sizeof(*sortFace),CmpFaceIndex);
  
  for( i=NFaces(); --i>=0; )
  {
    //WASSERT( sortFace[i]._index==i );
    if( sortFace[i]._toDelete )
    {
      DeleteFace(sortFace[i]._index);
    }
  }
  /**/
  
  // prepare for sorting again
  for( i=0; i<NFaces(); i++ )
  {
    sortFace[i]=SortFaceT(Face(i),i,sortTextures.MinY(Face(i).vTexture));
  }
  //qsort(sortFace,nFaces,sizeof(*sortFace),CmpFaceVert);
  //BubbleSort(sortFace,nFaces,sizeof(*sortFace),CmpFaceVert);
  // sort faces by texture name
  BubbleSort(sortFace,NFaces(),sizeof(*sortFace),CmpFaceText);
  // remove any invalid sharp edges
  for( i=_sharpEdge.Size(); --i>=0; )
  {
    int a=_sharpEdge[i][0],b=_sharpEdge[i][1];
    if( a==b || a<0 || a>=NPoints() || b<0 || b>=NPoints() ) RemoveSharpEdge(i);
  }
  // remove any double sharp edges
  for( i=_sharpEdge.Size(); --i>=1; )
  {
    int a1=_sharpEdge[i][0],b1=_sharpEdge[i][1];
    int a0=_sharpEdge[i-1][0],b0=_sharpEdge[i-1][1];
    if( a1==a0 && b1==b0 ) RemoveSharpEdge(i);
  }
  
  
  {
    // sort results in inverse permutation
    // create inverse of inverse permutation
    Temp<int> permut(NFaces());
    for( i=0; i<NFaces(); i++ )
    {
      permut[sortFace[i]._index]=i;
    }
    // apply permutation on all face indices
    _sel.PermuteFaces(permut);
    _lock.PermuteFaces(permut);
    _hide.PermuteFaces(permut);
    for( i=0; i<MAX_NAMED_SEL; i++ )
    {
      if( _namedSel[i] ) _namedSel[i]->PermuteFaces(permut);
    }
    // permute faces
    Temp<FaceT> save(NFaces());
    //for( i=0; i<nFaces; i++ ) save[i]=*sortFace[i]._face;
    for( i=0; i<NFaces(); i++ ) save[permut[i]]=FaceT(this,i);
    for( i=0; i<NFaces(); i++ ) save[i].CopyFaceTo(i);
  }
  
for( i=0; i<NNormals(); i++ ) 
  {  
  VecT &src=Normal(i);
  if (!_finite(src[0]) || _isnan(src[0]) ||
      !_finite(src[1]) || _isnan(src[1]) ||
      !_finite(src[2]) || _isnan(src[2])) 
    src[0]=src[1]=src[2]=0;  
  }
int warn=0;
for( i=0; i<NFaces(); i++ ) 
  {  
  FaceT fc(this,i);
  for (int j=0;j<fc.N();j++)
    if (!(fabs(fc.GetU(j))<1e5) || !(fabs(fc.GetV(j))<1e5) ||
      !_finite(fc.GetU(j)) || !_finite(fc.GetV(j)))
      {
      fc.SetUV(j,0,0);
      warn++;
      }
  }
  
}


void ObjToolCheck::RepairDegeneratedFaces()
{
  Selection todel(this);
  for (int i=0;i<NFaces();i++)
  {
    FaceT fc(this,i);
    if (!fc.Check(true,false,0.0001f,false))
      todel.FaceSelect(i);
  }
  SelectionDelete(&todel);
}

void ObjToolCheck::CheckDegeneratedFaces()
{
  ClearSelection();
  for (int i=0;i<NFaces();i++)
  {
    FaceT fc(this,i);
    if (!fc.Check(false,false,0.0001f,false))
      FaceSelect(i);
  } 
  SelectPointsFromFaces();
}

}