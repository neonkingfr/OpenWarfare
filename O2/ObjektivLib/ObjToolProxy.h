#pragma once
#include "objectdata.h"

#include "ObjToolsMatLib.h"

namespace ObjektivLib {


class ObjToolProxy :  public ObjectData
  {
  public:
    bool CreateProxy(const char *filename, const float *pin=NULL,const Matrix3 * rot = NULL);
    bool CreateProxy(const char *filename, Matrix4Val trans);
    bool CreateProxy(const char *filename, const float *pin, float size);
    bool SelectionToProxy(const char *filename, const Selection *sel=NULL);
    
    ///Enumerates proxies;
    /**
    @param list array reserved for pointers to NamedObject contains selection of proxy. 
            If NULL specified, return value will contain count of proxies
    @param filename name of proxy to search. If NULL specified, all proxies are included
    @param partial true means, proxies, that name partially matches the filename will also included
    @return count of proxies matched.
    */
    int EnumProxies(NamedSelection **list=NULL, const char *filename=NULL, bool partial=true) const;
    
    ///Gets filename from proxyname;
    /** trg must have the atleast same size as source! */
    static void GetProxyFilename(const char *src, char *trg);
    ///Converts filename into proxyname
    /**
    @param filename filename to convert
    @param proxyname buffer reserved for proxyname
    @param index index of proxy 0-99
    @param size size of reserved buffer
    @return count characters user in buffer. if zero, an error was occured. If bigger then size, buffer was too small
    */
    static int FilenameToProxyname(const char *filename, char *proxyname, int index, int size);
    
    ///Enumerates proxies into BTree container from ObjMatLibItem.
    /** It can be later user for mass file manipulation
    */
    void EnumProxies(BTree<ObjMatLibItem>& container, const char *filename=NULL, bool partial=true, const char *basepath=0) const;


    static const char *ProxyPrefix;

    ///Returns proxy face belongs to proxy. -1 if not found.
    int FindProxyFace(const char *proxyName) const;
    ///Returns proxy face belongs to proxy. -1 if not found.
    int FindProxyFace(const NamedSelection *proxy) const;

    ///Calculates proxy transformation
    static void GetProxyTransform(FaceT& fc, Matrix4 &trans, bool noscale=true);

    ///Shows all proxies in selection
    /** This function only mark proxies shown (hidden). Client must test flag to detect visible proxy and draw it by own
    @param show status for proxy. Function marks only proxies, not other faces
    @param sel selection, use null for current selection
    */
    void ShowProxyContent(bool show, Selection *sel=0);

    const NamedSelection *GetShowProxySelection();

    const NamedSelection *GetProxySelection(FaceT &fc, bool shownonly=true) const;

    Pathname GetProxyPathname(const char *proxyName, const char *projectRoot);

    void GetAllProxies(Selection &sel);

  };

}