// ParalelAgent.h: interface for the CParalelAgent class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PARALELAGENT_H__33DF1330_B327_47BB_AD4B_31D76B5A0FAC__INCLUDED_)
#define AFX_PARALELAGENT_H__33DF1330_B327_47BB_AD4B_31D76B5A0FAC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define SPEAGENT_MAXPROCESSES 8

class CParalelAgent  
  {
  struct PINFO
    {
    SOCKADDR_IN owner;
    char fname[MAX_PATH];
    HANDLE process; 
    };
  
  PINFO pinfo[SPEAGENT_MAXPROCESSES];
  
  public:
    void TerminateProcess(const char *fname, SOCKADDR_IN &sin);
    void CreateProcess(const char *fname, const char *args, SOCKADDR_IN &sin);
    CParalelAgent();
    virtual ~CParalelAgent();
    
  };

//--------------------------------------------------

#endif // !defined(AFX_PARALELAGENT_H__33DF1330_B327_47BB_AD4B_31D76B5A0FAC__INCLUDED_)
