// ParalelAgent.cpp: implementation of the CParalelAgent class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ParalelAgent.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CParalelAgent::CParalelAgent()
  {
  for (int i=0;i<SPEAGENT_MAXPROCESSES;i++) pinfo[i].process=NULL;
  }

//--------------------------------------------------

CParalelAgent::~CParalelAgent()
  {
  
  }

//--------------------------------------------------

void CParalelAgent::CreateProcess(const char *fname, const char *args, SOCKADDR_IN &sin)
  {
  for (int i=0;i<SPEAGENT_MAXPROCESSES;i++) if (pinfo[i].process==NULL)
    {
    char *p=inet_ntoa(sin.sin_addr);
    char *buff=(char *)alloca(strlen(args)+strlen(fname)+50);
    char *c;
    sprintf(buff,"%s ",fname);
    c=strchr(buff,0);
    sprintf(c,args,p);
    char *directory=(char *)alloca(strlen(buff));
    c=strrchr(directory,'//');
    *c=0;	
    STARTUPINFO nfo;
    nfo.cb=sizeof(nfo);
    nfo.dwFlags=STARTF_USESHOWWINDOW;
    nfo.wShowWindow=SW_HIDE;
    PROCESS_INFORMATION pi;
    if (::CreateProcess(fname,buff,NULL,NULL,FALSE,CREATE_DEFAULT_ERROR_MODE|IDLE_PRIORITY_CLASS,NULL,directory,&nfo,&pi))
      {
      pinfo[i].process=pi.hProcess;
      strncpy(pinfo[i].fname,fname,sizeof(pinfo[i].fname));
      pinfo[i].owner=sin;
      }
    break;
    }
  }

//--------------------------------------------------

void CParalelAgent::TerminateProcess(const char *fname, SOCKADDR_IN &sin)
  {
  HANDLE fwait[SPEAGENT_MAXPROCESSES];
  int ps[SPEAGENT_MAXPROCESSES];
  int cnt=0;
  for (int i=0;i<SPEAGENT_MAXPROCESSES;i++) if (pinfo[i].process!=NULL && strncmp(fname,pinfo[i].fname,sizeof(pinfo[i].fname))==0 && memcmp(&sin.sin_addr,&pinfo[i].owner.sin_addr,sizeof(in_addr))==0)
    {		
    ps[cnt]=i;
    fwait[cnt]=pinfo[i].process;
    cnt++;
    }
  if (cnt)
    {
    WaitForMultipleObjects(cnt,fwait,TRUE,2000);
    for (int i=0;i<cnt;i++)
      {
      if (WaitForSingleObject(fwait[i],0)==WAIT_TIMEOUT) ::TerminateProcess(fwait[i],255);
      CloseHandle(fwait[i]);
      pinfo[ps[i]].process=NULL;
      }
    }
  }

//--------------------------------------------------

