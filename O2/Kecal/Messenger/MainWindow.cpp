// MainWindow.cpp: implementation of the CMainWindow class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MainWindow.h"
#include "Application.h"
#include "resource.h"
#include "ChatWindow.h"
#include "TemaDlg.h"
#include "NotifyMsgDlg.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define MAINWINDOW "MainWindow"

CMainWindow::CMainWindow()
  {
  framebgr=NULL;  
  listfont=NULL;
  _captureWindow=NULL;
  _showIPs=false;
  quicksearch[0]=0;
  }










CMainWindow::~CMainWindow()
  {
  DeleteObject(framebgr);
  DeleteObject(listfont);
  }










HBITMAP LoadBitmapFromFile(const char *bitmap)
  {
  return (HBITMAP)LoadImage(AfxGetInstanceHandle(),bitmap,IMAGE_BITMAP,0,0,LR_DEFAULTCOLOR|LR_LOADFROMFILE);
  }










LRESULT CMainWindow::OnInitDialog()
  {
  MINMAXINFO nfo;
  GetMinMaxInfo(nfo);
  state=GetDlgItem(IDC_STAV);
  list=GetDlgItem(IDC_LIST);  
  nfo.ptMinTrackSize.y=100;
  nfo.ptMinTrackSize.x=120;
  SetMinMaxInfo(nfo);
  HMENU mnu=LoadMenu(theApp->hInst,MAKEINTRESOURCE(IDR_MAINMENU));
  HMENU sss=GetSubMenu(mnu,0);
  int cnt=GetMenuItemCount(sss);
  int i;
  HMENU sub=NULL;
  for (i=0;i<cnt && sub==NULL;i++) sub=GetSubMenu(sss,i);
  if (sub)
    {
    cnt=GetMenuItemCount(sub);
    for (i=0;i<cnt;i++)
      {
      char s[256];
      GetMenuString(sub,i,s,ArrLength(s),MF_BYPOSITION);
      SendMessage(state,CB_ADDSTRING,0,(LPARAM)s);
      SendMessage(state,CB_SETITEMDATA,i,GetMenuItemID(sub,i)-ID_MAIN_MJSTAV_NEJSEM);
      }
    }
  DestroyMenu(mnu);
  LoadUsersAndGroups();
  ReloadBgPic();
  
  CDialogClass::OnInitDialog();
  
  HDC dc=GetDC(*this);
  framebgrdc=CreateCompatibleDC(dc);
  ReleaseDC(*this,dc);
  
  WINDOWPLACEMENT wp;
  HKEY key=OpenProfileKey();
    {
    ctArchiveInReg arch(key,MAINWINDOW);
    if (!(!arch))
      {
      arch.serial(&wp,sizeof(wp));
      wp.showCmd=SW_HIDE;
      SetWindowPlacement(*this,&wp);
      }
    }
  CheckWindowVisibility(*this);
  return TRUE;
  }










LRESULT CMainWindow::OnMeasureItem(UINT idCtl, LPMEASUREITEMSTRUCT lpmis)
  {
  lpmis->itemHeight=18;
  return TRUE;
  }










static void DrawPartOfBitmap(HDC windc, HDC bitmapdc, RECT rc, int shift)
  {
  BITMAP bmp;
  GetObject(GetCurrentObject(bitmapdc,OBJ_BITMAP),sizeof(bmp),&bmp);
  RECT srcrc=rc;  
  RECT saverc=rc;
  opakuj:
  srcrc.left=rc.left%bmp.bmWidth;
  srcrc.top=(rc.top+shift*(rc.bottom-rc.top))%bmp.bmHeight;
  srcrc.right=srcrc.left+rc.right-rc.left;
  srcrc.bottom=srcrc.top+rc.bottom-rc.top;
  BitBlt(windc,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,bitmapdc,srcrc.left,srcrc.top,SRCCOPY);  
  if (srcrc.right>bmp.bmWidth)
    {
    rc.left+=bmp.bmWidth-srcrc.left;
    goto opakuj;
    }
  rc=saverc;
  if (srcrc.bottom>bmp.bmHeight)
    {	
    rc.top+=bmp.bmHeight-srcrc.top;
    saverc=rc;
    goto opakuj;
    }
  }










LRESULT CMainWindow::DlgProc(UINT msg, WPARAM wParam, LPARAM lParam)
  {
  switch (msg)
    {
    case WM_MEASUREITEM: return OnMeasureItem(wParam,(LPMEASUREITEMSTRUCT)lParam);
    case WM_CTLCOLORLISTBOX: 
    if ((HWND) lParam==list)
      {
      if (framebgr)
        {
        HDC hdcLB = (HDC) wParam;
        RECT rc;
        RECT rc2;
        int cnt=SendMessage(list,LB_GETCOUNT,0,0);
        GetClientRect(list,&rc);
        if (cnt==0) rc2=rc;
        else
          {
          SendMessage(list,LB_GETITEMRECT,cnt-1,(LPARAM)&rc2);
          rc2.top=rc2.bottom;
          rc2.bottom=rc.bottom;
          }
        if (rc2.bottom>rc2.top)
          {
          RECT rc3;
          GetClipBox(hdcLB,&rc);
          IntersectRect(&rc3,&rc,&rc2);
          HBITMAP old=(HBITMAP)SelectObject(framebgrdc,framebgr);
          DrawPartOfBitmap(hdcLB,framebgrdc,rc3,0);
          SelectObject(framebgrdc,old);
          }
        return (LONG)GetStockObject(HOLLOW_BRUSH);
        }	  
      }
    return 0;
    
    case WM_DRAWITEM: 
    if (wParam==IDC_STAV) return OnDrawStates(wParam, (LPDRAWITEMSTRUCT)lParam);
    if (wParam==IDC_LIST) return OnDrawList(wParam, (LPDRAWITEMSTRUCT)lParam);
    break;
    case WM_CONTEXTMENU: return OnContextMenu((HWND)wParam,LOWORD(lParam),HIWORD(lParam));
    case WM_EXITSIZEMOVE: 
      {
      LRESULT l=CDialogClass::DlgProc(msg,wParam,lParam);
      SavePosition(*this,MAINWINDOW);
      return l;
      }
    case WM_MENUCHAR:
      {
      if (LOWORD(wParam)==13 && HIWORD(wParam) == MF_SYSMENU)
        {
        RECT rc;
        SendMessage(list,LB_GETITEMRECT,ListBox_GetCurSel(list),(LPARAM)&rc);
        ClientToScreen(list,(LPPOINT)&rc.left);
        ClientToScreen(list,(LPPOINT)&rc.right);
        PostMessage(*this,WM_CONTEXTMENU,(WPARAM)list,MAKELPARAM((rc.left+rc.right)>>1,(rc.top+rc.bottom)>>1));
        SetWindowLong(*this,DWL_MSGRESULT,MNC_CLOSE);
        return MNC_CLOSE;
        }
      return 0;
      }
    case WM_CHARTOITEM:
    if (LOWORD(wParam)==13) 
    {if (HandleCapture(true)) OpenChannel();}
    else
    if (LOWORD(wParam)==32)
      {
      int id=ListBox_GetCurSel(list)-theApp->users.Size()-1;
      if (id>=0 && id<theApp->groups.Size()) 
        {
        theApp->groups[id].sign=!theApp->groups[id].sign;
        InvalidateRect(list,NULL,TRUE);
        }
      }
    else
    {
      char chr=(char)LOWORD(wParam);
      int pos=HIWORD(wParam);
      HWND listbox=(HWND)lParam;
      if (strlen(quicksearch)<30)
      {
        char str[2]={chr,0};
        strcat(quicksearch,str);
        int index=theApp->users.QuickSearchUser(quicksearch);
        return index;
      }
    }
    
    return ListBox_GetCurSel(list);
    case WM_ACTIVATE:
      quicksearch[0]=0;
      return 0;
    case WM_LBUTTONUP:
      {
      if (_dragCnt && GetCapture()==*this)
        {
        POINT pt;pt.x=GET_X_LPARAM(lParam);pt.y=GET_Y_LPARAM(lParam);
        ClientToScreen(*this,&pt);
        ReleaseCapture();
        _dragCnt=0;
        theApp->listlock=false;
        CChatWindow *wnd=theApp->chatlist.GetDropTarget(pt);
        if (wnd)
          if (wnd->peerchannel)
            if (MessageBox(*this,StrRes[IDS_DROPPEER],appname,MB_YESNO|MB_ICONQUESTION)==IDYES)
              {
              int us=theApp->users.FindUserByID(wnd->chan.majorid,wnd->chan.minorid);
              if (us!=-1) SendMessage(list,LB_SETSEL,TRUE,us);
              OpenChannel();
              }
        else
          return 1;
        else
          OpenChannel(wnd);
        }
      return 1;
      }
    case WM_MOUSEMOVE:
    if (_dragCnt && GetCapture()==*this)
      {
      POINT pt;pt.x=GET_X_LPARAM(lParam);pt.y=GET_Y_LPARAM(lParam);
      ClientToScreen(*this,&pt);
      theApp->chatlist.OnDragSetCursor(pt);
      }
    return 1;
    case KM_CAPTUREUSERSELECT:
      {
        _captureWindow=(HWND)lParam;
        return 1;
      }
    default: return CDialogClass::DlgProc(msg,wParam,lParam);
    }
  return FALSE;
  }










LRESULT CMainWindow::OnDrawStates(int idCtl, LPDRAWITEMSTRUCT lpdis)
  {
  if (lpdis->itemID==-1) return TRUE;
  HBRUSH bgr;
  if (lpdis->itemState & ODS_SELECTED) 
    {
    bgr=CreateSolidBrush(GetSysColor(COLOR_HIGHLIGHT));
    SetTextColor(lpdis->hDC,GetSysColor(COLOR_HIGHLIGHTTEXT));
    }
  else
    {
    bgr=CreateSolidBrush(GetSysColor(COLOR_WINDOW));
    SetTextColor(lpdis->hDC,GetSysColor(COLOR_WINDOWTEXT));
    }
  SetBkMode(lpdis->hDC,TRANSPARENT);
  FillRect(lpdis->hDC,&(lpdis->rcItem),bgr);
  HICON icn=(HICON)LoadImage(theApp->hInst,MAKEINTRESOURCE(IDI_OFFLINE+lpdis->itemData),IMAGE_ICON,16,16,LR_SHARED);
  DrawIconEx(lpdis->hDC,lpdis->rcItem.left+1,lpdis->rcItem.top,icn,16,16,0,NULL,DI_NORMAL);
  RECT rc=lpdis->rcItem;
  rc.left+=20;
  char *s=(char *)alloca(SendMessage(lpdis->hwndItem,CB_GETLBTEXTLEN,lpdis->itemID,0)+1);
  SendMessage(lpdis->hwndItem,CB_GETLBTEXT,lpdis->itemID,(LPARAM)s);
  DrawText(lpdis->hDC,s,-1,&rc,DT_NOPREFIX|DT_END_ELLIPSIS|DT_LEFT|DT_VCENTER);
  DeleteObject(bgr);
  return 1;
  }










LRESULT CMainWindow::OnDrawList(int idCtl, LPDRAWITEMSTRUCT lpdis)
  {
  int id=lpdis->itemID;
  if (id==-1) return TRUE;
  if (framebgr)
    {
    HBITMAP old=(HBITMAP)SelectObject(framebgrdc,framebgr);
    DrawPartOfBitmap(lpdis->hDC,framebgrdc,lpdis->rcItem,SendMessage(lpdis->hwndItem,LB_GETTOPINDEX,0,0));
    SelectObject(framebgrdc,old);
    }
  if (lpdis->itemState & ODS_SELECTED && id!=theApp->users.Size()) 
    {
    if (!theApp->config.skin_transparent)
      {
      SetBkColor(lpdis->hDC,GetSysColor(COLOR_HIGHLIGHT));
      ExtTextOut(lpdis->hDC,0,0,ETO_OPAQUE,&lpdis->rcItem,0,NULL,NULL);
      SetTextColor(lpdis->hDC,GetSysColor(COLOR_HIGHLIGHTTEXT));
      }
    else
      {
      SetTextColor(lpdis->hDC,theApp->config.skin_color==CLR_DEFAULT?GetSysColor(COLOR_WINDOWTEXT):theApp->config.skin_color);
      HPEN pen=CreatePen(PS_SOLID,1,GetSysColor(COLOR_HIGHLIGHT));
      HPEN oldpen=(HPEN)SelectObject(lpdis->hDC,pen);
      for (int i=lpdis->rcItem.left-lpdis->rcItem.bottom+lpdis->rcItem.top;i<lpdis->rcItem.right;i+=2)
        {MoveToEx(lpdis->hDC,i,lpdis->rcItem.top,NULL);LineTo(lpdis->hDC,i+lpdis->rcItem.bottom-lpdis->rcItem.top,lpdis->rcItem.bottom);}
      SelectObject(lpdis->hDC,oldpen);
      DeleteObject(pen);
      }
    }
  else
    {
    SetTextColor(lpdis->hDC,theApp->config.skin_color==CLR_DEFAULT?GetSysColor(COLOR_WINDOWTEXT):theApp->config.skin_color);
    if (!framebgr)
      {
      SetBkColor(lpdis->hDC,GetSysColor(COLOR_WINDOW));
      ExtTextOut(lpdis->hDC,0,0,ETO_OPAQUE,&lpdis->rcItem,0,NULL,NULL);
      }
    }  
  SetBkMode(lpdis->hDC,TRANSPARENT);
  SelectObject(lpdis->hDC,GetStockObject(NULL_PEN));
  HFONT oldfont;
  if (listfont) oldfont=(HFONT)SelectObject(lpdis->hDC,listfont);
  RECT rc=lpdis->rcItem;
  if (id<theApp->users.Size())
    {
    HICON icn=(HICON)LoadImage(theApp->hInst,MAKEINTRESOURCE(IDI_OFFLINE+theApp->users[id].state),IMAGE_ICON,16,16,LR_SHARED);
    DrawIconEx(lpdis->hDC,lpdis->rcItem.left+4,lpdis->rcItem.top+1,icn,16,16,0,NULL,DI_NORMAL);
    rc.left+=25;
    if (_showIPs)
    {
      HPEN pen=CreatePen(PS_SOLID,1,GetSysColor(COLOR_BTNSHADOW));
      HPEN pold=(HPEN)SelectObject(lpdis->hDC,pen);
      RECT rc2=rc;
      rc.right=((rc2.left+rc2.right)>>1)-2;
      rc2.left=((rc2.left+rc2.right)>>1)+2;
      char *ipnum=inet_ntoa(theApp->users[id].lastknownip.sin_addr);
      DrawText(lpdis->hDC,theApp->users[id].username,-1,&rc,DT_NOPREFIX|DT_END_ELLIPSIS|DT_LEFT|DT_VCENTER|DT_SINGLELINE);	
      DrawText(lpdis->hDC,ipnum,-1,&rc2,DT_NOPREFIX|DT_END_ELLIPSIS|DT_LEFT|DT_VCENTER|DT_SINGLELINE);	
      MoveToEx(lpdis->hDC,rc.right+2,rc.top,NULL);
      LineTo(lpdis->hDC,rc.right+2,rc.bottom);
      SelectObject(lpdis->hDC,pold);
      DeleteObject(pen);
    }
    else
      DrawText(lpdis->hDC,theApp->users[id].username,-1,&rc,DT_NOPREFIX|DT_END_ELLIPSIS|DT_LEFT|DT_VCENTER|DT_SINGLELINE);	
    if (theApp->users[id].offlinemsg && theApp->users[id].offlinemsg[0])
      {
      icn=(HICON)LoadImage(theApp->hInst,MAKEINTRESOURCE(IDI_NOTIFYMSG),IMAGE_ICON,16,16,LR_SHARED);
      DrawIconEx(lpdis->hDC,lpdis->rcItem.right-20,lpdis->rcItem.top+1,icn,16,16,0,NULL,DI_NORMAL);
      }
    }
  else 
    {
    id-=theApp->users.Size();
    if (id==0)
      {
      HPEN pen=CreatePen(PS_SOLID,3,GetSysColor(COLOR_BTNSHADOW));
      HPEN pold=(HPEN)SelectObject(lpdis->hDC,pen);
      MoveToEx(lpdis->hDC,rc.left+2,(rc.top+rc.bottom)>>1,NULL);
      LineTo(lpdis->hDC,rc.right-2,(rc.top+rc.bottom)>>1);
      SelectObject(lpdis->hDC,pold);
      DeleteObject(pen);
      }
    else 
      {
      HPEN pen=CreatePen(PS_SOLID,1,GetSysColor(COLOR_BTNSHADOW));
      HPEN pold=(HPEN)SelectObject(lpdis->hDC,pen);
      id--;
      HICON icn;
      if (theApp->groups[id].privat) 
        icn=(HICON)LoadImage(theApp->hInst,MAKEINTRESOURCE(IDI_PRIVATEGRP),IMAGE_ICON,16,16,LR_SHARED);
      else
        icn=(HICON)LoadImage(theApp->hInst,MAKEINTRESOURCE(theApp->groups[id].sign?IDI_CHECKIN:IDI_CHECKOUT),IMAGE_ICON,16,16,LR_SHARED);
      DrawIconEx(lpdis->hDC,lpdis->rcItem.left+4,lpdis->rcItem.top+1,icn,16,16,0,NULL,DI_NORMAL);
      rc.left+=25;
      RECT rc2=rc;
      rc.right=((rc2.left+rc2.right)>>1)-2;
      rc2.left=((rc2.left+rc2.right)>>1)+2;
      DrawText(lpdis->hDC,theApp->groups[id].groupname,-1,&rc,DT_NOPREFIX|DT_END_ELLIPSIS|DT_LEFT|DT_VCENTER|DT_SINGLELINE);	
      DrawText(lpdis->hDC,theApp->groups[id].owner,-1,&rc2,DT_NOPREFIX|DT_END_ELLIPSIS|DT_LEFT|DT_VCENTER|DT_SINGLELINE);	
      MoveToEx(lpdis->hDC,rc.right+2,rc.top,NULL);
      LineTo(lpdis->hDC,rc.right+2,rc.bottom);
      SelectObject(lpdis->hDC,pold);
      DeleteObject(pen);
      }
    
    }
  if (listfont) SelectObject(lpdis->hDC,oldfont);
  return 1;
  }










void CMainWindow::LoadUsersAndGroups()
  {
  int p=SendMessage(list,LB_GETTOPINDEX,0,0);
  int ucount=theApp->users.Size();
  int gcount=theApp->groups.Size();
  SendMessage(list,LB_SETCOUNT,ucount+gcount+1,0);
  InvalidateRect(list,NULL,TRUE);
  SendMessage(list,LB_SETTOPINDEX,p,0);
  }










LRESULT CMainWindow::OnCancel()
  {
  ShowWindow(*this,SW_HIDE);
  return 1;
  }










void CMainWindow::SetState(EUserState st)
  {
  int cnt=SendMessage(state,CB_GETCOUNT,0,0);
  int i;
  for (i=0;i<cnt;i++)
    if (SendMessage(state,CB_GETITEMDATA,i,0)==(LRESULT)st)
      {
      SendMessage(state,CB_SETCURSEL,i,0);
      break;
      }
  }










LRESULT CMainWindow::OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt)
  {
  switch (wID)
    {
    case IDC_STAV:if (wNotifyCode==CBN_SELCHANGE) theApp->SetMyState((EUserState)SendMessage(state,CB_GETITEMDATA,SendMessage(state,CB_GETCURSEL,0,0),0));return 1;
    case IDC_VOLBY: 
      {	  
      RECT rc;
      POINT pt;
      GetWindowRect(GetDlgItem(IDC_VOLBY),&rc);	  
      pt.x=rc.left;
      pt.y=rc.bottom;
      HMONITOR mon=MonitorFromPoint(pt,MONITOR_DEFAULTTONEAREST);
      bool top=false;
      if (mon)
        {
        MONITORINFO mi;
        mi.cbSize=sizeof(mi);
        GetMonitorInfo(mon,&mi);
        if (pt.y+200>mi.rcWork.bottom) 
          {pt.y=rc.top;top=true;}		
        }
      theApp->ProcessMenu(pt,top);return 1;
      }
    case IDC_LIST: if (wNotifyCode==LBN_SELCHANGE)
      {
      POINT pt;
      GetCursorPos(&pt);
      int it=LBItemFromPt(list,pt,FALSE);
      ScreenToClient(list,&pt);
      if (SendMessage(list,LB_GETSEL,it,0)>0 && pt.x<20) 
        {
        it-=theApp->users.Size()+1;
        if (it>=0) 
          {
          theApp->groups[it].sign=!theApp->groups[it].sign;
          theApp->SaveGroups();
          InvalidateRect(list,NULL,FALSE);
          }
        }
      CheckPrivateGroup(it-theApp->users.Size()-1);
      HandleCapture(false);
      }
    if (wNotifyCode==LBN_DBLCLK)
      {
      CheckPrivateGroup(ListBox_GetCurSel(list)-theApp->users.Size()-1);
      if (HandleCapture(true)) OpenChannel();
      }
    return 1;
    default: CDialogClass::OnCommand(wID,wNotifyCode,clt );
    }
  return 0;
  }










LRESULT CMainWindow::OnSize(int cx, int cy, int rx, int ry, int flags)
  {
  HDWP dwp=BeginDeferWindowPos(5);
  MoveWindowRel(list,0,0,rx,ry,dwp);
  MoveWindowRel(state,0,ry,rx,0,dwp);
  MoveWindowRel(GetDlgItem(IDC_VOLBY),0,ry,0,0,dwp);
  EndDeferWindowPos(dwp);
  InvalidateRect(list,NULL,FALSE);
  return 1;
  }










LRESULT CMainWindow::OnContextMenu(HWND hWnd, short xpos, short ypos)
  {
  if (list==hWnd)
    {
    POINT p;
    POINT sc;
    p.x=xpos;
    p.y=ypos;
    sc=p;
    ScreenToClient(list,&p);
    SendMessage(list,WM_LBUTTONDOWN,MK_LBUTTON,MAKELPARAM(p.x,p.y));
    SendMessage(list,WM_LBUTTONUP,0,MAKELPARAM(p.x,p.y));
    bool nosel=SendMessage(list,LB_GETSELCOUNT,0,0)==0;
    int sel=LBItemFromPt(list,sc,FALSE);	
    if (sel<0) nosel=true;
    CheckPrivateGroup(sel-theApp->users.Size()-1);
    HMENU mnu=LoadMenu(theApp->hInst,MAKEINTRESOURCE(IDR_MAINMENU));
    if (sel>theApp->users.Size() || nosel)
      {
      int item=sel-theApp->users.Size()-1;
      HMENU grp=GetSubMenu(mnu,1);
      if (nosel)
        {
        EnableMenuItem(grp,ID_GROUPS_POSLATZPRVU,MF_GRAYED);
        EnableMenuItem(grp,ID_GROUPS_SLEDOVATSKUPINU,MF_GRAYED);
        EnableMenuItem(grp,ID_GROUPS_PEJMENOVATSKUPINU,MF_GRAYED);
        EnableMenuItem(grp,ID_GROUPS_VYMAZATSKUPINU,MF_GRAYED);
        }
      else
        {
        if (theApp->groups[item].sign) CheckMenuItem(mnu,ID_GROUPS_SLEDOVATSKUPINU,MF_CHECKED);
        //		if (stricmp(theApp->config.myname,theApp->groups[item].owner)) EnableMenuItem(grp,ID_GROUPS_PEJMENOVATSKUPINU,MF_GRAYED);
        }
      int cmd=TrackPopupMenu(grp,TPM_RETURNCMD|TPM_NONOTIFY,xpos,ypos,0,*this,NULL);
      switch (cmd)
        {
        case ID_GROUPS_NOVSKUPINA: theApp->AddGroup();break;
        case ID_GROUPS_VYMAZATSKUPINU: theApp->DeleteGroup(item);break;
        case ID_GROUPS_PEJMENOVATSKUPINU: theApp->RenameGroup(item);break;
        case ID_GROUPS_SLEDOVATSKUPINU: theApp->groups[item].sign=!theApp->groups[item].sign;InvalidateRect(list,NULL,FALSE);break;
        case ID_GROUPS_POSLATZPRVU: OpenChannel();break;
        case ID_GROUPS_OBNOVIT: theApp->SendCommand(NULL,COMM_RELOADGROUPS);break;
        }
      }
    else if (sel<theApp->users.Size())
      {
      HMENU grp=GetSubMenu(mnu,2);
      if (nosel)
        {
        EnableMenuItem(grp,ID_USERS_POSLATZPRVU,MF_GRAYED);
        EnableMenuItem(grp,ID_USERS_VYMAZATUIVATELE,MF_GRAYED);
        EnableMenuItem(grp,ID_USERS_HISTORIE,MF_GRAYED);
        EnableMenuItem(grp,ID_UNBIND_IDENTITY,MF_GRAYED);
        }
      else
        if (theApp->users[sel].email==0 || theApp->users[sel].email[0]==0)
          EnableMenuItem(grp,ID_UNBIND_IDENTITY,MF_GRAYED);
      if (_showIPs)
        CheckMenuItem(grp,ID_USERS_ZOBRAZIPADRESY,MF_CHECKED);
      int cmd=TrackPopupMenu(grp,TPM_RETURNCMD|TPM_NONOTIFY,xpos,ypos,0,*this,NULL);
      switch (cmd)
        {
        case ID_USERS_VYMAZATUIVATELE:
        theApp->LockLists();
        if (MessageBox(*this,StrRes[IDS_DELETEUSER],appname,MB_OKCANCEL|MB_ICONINFORMATION)==IDOK)
          {			
          theApp->users.Delete(sel);
          theApp->SaveUsers();
          this->LoadUsersAndGroups();
          }
        theApp->UnlockLists();
        break;
        case ID_UNBIND_IDENTITY: theApp->users[sel].SetEmail("");theApp->SaveUsers();break;
        case ID_USERS_UPOZOROVACZPRVA:SetSelectionNotifyMsg();break;
        case ID_USERS_POSLATZPRVU:OpenChannel();break;
        case ID_USERS_OBNOVIT: theApp->SendCommand(NULL,COMM_REFRESH);break;
        case ID_USERS_ZOBRAZIPADRESY: _showIPs=!_showIPs;InvalidateRect(list,NULL,TRUE);break;
        case ID_USERS_HISTORIE: CHistoryWindow::OpenHistory(*this,ChannelID(theApp->users[sel].majid,theApp->users[sel].minid));break;
        }	
      }
    DestroyMenu(mnu);
    }
  return 1;
  }

void CMainWindow::UpdateList()
  {
  InvalidateRect(list,NULL,FALSE);
  CheckWindowVisibility(*this);
  }

void CMainWindow::OpenChannel(CChatWindow *towindow)
  {  
  if (theApp->mystate==UST_Offline)
    {
    MessageBox(*this,StrRes[IDS_NESMISBYTOFFLINE],appname,MB_OK|MB_ICONSTOP);
    return;
    }
  bool deaf=true;
  CTemaDlg dlg;
  theApp->LockLists();
    {
    int *users=(int *)alloca((theApp->users.Size()+1)*sizeof(int));
    int *groups=(int *)alloca((theApp->groups.Size()+1)*sizeof(int));
    int *p;
    int i,j;
    p=users;
    for (i=0;i<theApp->users.Size();i++)
      if (SendMessage(list,LB_GETSEL,i,0)) 
        {
        *p++=i;
        if (theApp->users[i].state!=UST_Deaf && theApp->users[i].state!=UST_Offline) deaf=false;
        }
    *p=-1;
    i++;
    p=groups;
    for (j=0;j<theApp->groups.Size();j++)
      if (SendMessage(list,LB_GETSEL,i+j,0)) 
        {*p++=j;deaf=false;}
    *p=-1;
    if (deaf)
      {
      if ((users[1]!=-1 || groups[0]!=-1) && MessageBox(*this,StrRes[IDS_USERDEAF],appname,MB_YESNO)==IDYES ) 
        {
        SetSelectionNotifyMsg();
        theApp->UnlockLists();
        return;
        }
      }
    if (towindow)
      {
      towindow->InviteUsersGroups(users,groups);
      int *p;
      char buff[256];
      for (p=users;*p!=-1;p++)
        {
        sprintf(buff,StrRes[IDS_ADDUSERCHAT],theApp->users[*p].username);
        towindow->SystemMessage(buff);
        }
      for (p=groups;*p!=-1;p++)
        {
        sprintf(buff,StrRes[IDS_ADDGROUPCHAT],theApp->groups[*p].groupname);
        towindow->SystemMessage(buff);
        }
      }
    else
      {
      CChatWindowList &wlist=theApp->chatlist;
      wlist.CreateChannel(users,groups);	
      }
    //	theApp->SendCommand(3,COMM_OPENCHAT,&id,users,groups,dlg.GetTopic());
    }
  theApp->UnlockLists();
  }










/*void CMainWindow::SavePosition()
  {
  WINDOWPLACEMENT wp;
  wp.length=sizeof(wp);
  GetWindowPlacement(*this,&wp);
  HKEY key=OpenProfileKey();
    {
    ctArchiveOutReg arch(key,MAINWINDOW);
    arch.serial(&wp,sizeof(wp));
    }
  RegCloseKey(key);
  }*/

//--------------------------------------------------

void CMainWindow::CheckPrivateGroup(int id)
  {
  if (id>=0)
    if (theApp->groups[id].privat)
      {
      AutoArray<ChannelID> &uslst=theApp->groups[id].poeple;
      SendMessage(list,WM_SETREDRAW,FALSE,0);
      for (int i=0;i<uslst.Size();i++)
        {
        ChannelID &id=uslst[i];
        int pos=theApp->users.FindUserByID(id.majorid,id.minorid);
        if (pos!=-1) SendMessage(list,LB_SETSEL,1,pos);
        }
      SendMessage(list,WM_SETREDRAW,TRUE,0);
      SendMessage(list,LB_SETSEL,1,theApp->users.Size()+1+id);
      }
  }










LRESULT CMainWindow::OnOK()
  {
  CheckPrivateGroup(ListBox_GetCurSel(list)-theApp->users.Size()-1);
  OpenChannel();
  return 0;
  }










void CMainWindow::SetSelectionNotifyMsg()
  {
  int anchor=SendMessage(list,LB_GETANCHORINDEX,0,0);
  if (anchor==-1) return;
  if (anchor>=theApp->users.Size()) return;
  CNotifyMsgDlg dlg;
  dlg.vMessage=theApp->users[anchor].offlinemsg;
  int p=dlg.DoModal(IDD_NOTIFYMSG,*this);
  if (p==IDOK)
    {
    int end=SendMessage(list,LB_GETCOUNT,0,0);
    for (int i=anchor;i<theApp->users.Size();i++)
      {
      if (SendMessage(list,LB_GETSEL,i,0))
        {
        theApp->users[i].SetNotifyMsg(dlg.oMessage);
        }
      }
    }
  theApp->SaveUsers();
  InvalidateRect(list,NULL,TRUE);
  }










BOOL CMainWindow::IsSubclassMsg(MSG *msg)
  {
  if (msg->hwnd==list)
    {
    if (msg->message==WM_MOUSEMOVE)
      if (msg->wParam & MK_LBUTTON)
        {
        _dragCnt++;
        ReleaseCapture();
        if (_dragCnt>1)
          {
          int pt=::LBItemFromPt(list,msg->pt,FALSE);
          if (pt!=-1) SendMessage(list,LB_SETSEL,TRUE,pt);          
          CheckPrivateGroup(pt-theApp->users.Size()-1);
          SetCursor(::LoadCursor(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDC_DRAGARROW)));
          SetCapture(*this);
          theApp->listlock=true;
          }
        return TRUE;
        }
    else
      {
      _dragCnt=0;
      return FALSE;
      }
    }
    if (msg->message==WM_KEYDOWN)
    {
      if (msg->wParam==VK_UP||
          msg->wParam==VK_DOWN||
          msg->wParam==VK_LEFT||
          msg->wParam==VK_RIGHT||
          msg->wParam==VK_RETURN||
          msg->wParam==VK_SPACE) quicksearch[0]=0;
      if (msg->wParam==VK_BACK && quicksearch[0])
      {
        quicksearch[strlen(quicksearch)-1]=0;
        ListBox_SetCurSel(list,theApp->users.QuickSearchUser(quicksearch));
      }
    }    
    if (IsDialogMessage(*this,msg)) return TRUE;
    return FALSE;
  }










void CMainWindow::ReloadBgPic()
  {
  DeleteObject(framebgr);
  framebgr=NULL;
  DeleteObject(listfont);
  listfont=NULL;
  if (theApp->config.skin_image[0])
    framebgr=LoadBitmapFromFile(theApp->config.skin_image);
  InvalidateRect(list,NULL,TRUE);
  if (theApp->config.skin_font[0])
    {
    LOGFONT lg;
    memset(&lg,0,sizeof(lg));
    strcpy(lg.lfFaceName,theApp->config.skin_font);
    lg.lfHeight=-theApp->config.skin_fontsize;
    lg.lfWeight=theApp->config.skin_fontbold?FW_BOLD:FW_NORMAL;
    lg.lfItalic=theApp->config.skin_fontitalic;
    lg.lfCharSet=EASTEUROPE_CHARSET;
    listfont=CreateFontIndirect(&lg);
    }
  }




bool CMainWindow::HandleCapture(bool select)
{
  if (_captureWindow)
  {
    int i=ListBox_GetCurSel(list);
    if (i>=theApp->users.Size()) return true;
    SendMessage(_captureWindow,KM_USERSELECTED,select,(LPARAM)&(theApp->users[i]));
    return false;
  }
  return true;
}





void CMainWindow::OpenDump()
  {
  
  }










