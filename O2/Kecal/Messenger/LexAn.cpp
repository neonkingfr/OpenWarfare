// LexAn.cpp: implementation of the CLexAn class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LexAn.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLexAn::CLexAn()
  {
  buffer=NULL;
  curptr="";
  }

//--------------------------------------------------

CLexAn::~CLexAn()
  {
  free(buffer);
  }

//--------------------------------------------------

void CLexAn::SetBuffer(const char *b)
  {
  free(buffer);
  buffer=strdup(b);
  curptr=buffer;
  }

//--------------------------------------------------

int CLexAn::Read()
  {
  while (*curptr<33 && *curptr!=0) curptr++;
  charpos=curptr-buffer;
  if (*curptr==0) return lastrd=LEX_EOF;
  if (isdigit(*curptr))
    {
    intdata=0;
    while (isdigit(*curptr))
      {
      intdata=intdata*10+((*curptr)-'0');
      curptr++;
      }
    if (*curptr=='\t') return lastrd=LEX_NUMBER;
    while (curptr>buffer && *curptr!='\t') curptr--;
    if (*curptr=='\t') curptr++;
    }
  char *c=strchr(curptr,'\t');
  strdata=curptr;
  if (c==NULL) curptr=strchr(curptr,0);
  else 
    {
    *c=0;
    curptr=c+1;
    }
  return lastrd=LEX_STRING;
  }

//--------------------------------------------------

