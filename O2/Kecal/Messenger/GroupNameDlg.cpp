// GroupNameDlg.cpp: implementation of the CGroupNameDlg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Application.h"
#include "GroupNameDlg.h"
#include "resource.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGroupNameDlg::CGroupNameDlg()
  {
  grpname="";
  result=NULL;
  privat=true;
  editidx=-1;
  logauto=false;
  soundonly=false;
  }

//--------------------------------------------------

CGroupNameDlg::~CGroupNameDlg()
  {
  free(result);
  }

//--------------------------------------------------

LRESULT CGroupNameDlg::OnInitDialog()
  {
  int i;
  free(result);
  SetDlgItemText(IDC_JMENO,grpname);
  strncpy(soundname,sound?sound:"",sizeof(soundname));
  soundname[sizeof(soundname)-1]=0;  
  SetSoundName(soundname);
  users=GetDlgItem(IDC_USERS);
  for (i=0;i<theApp->users.Size();i++)	
    SendMessage(users,LB_ADDSTRING,0,(LPARAM)(theApp->users[i].username));
  if (privat)
    {
    for (i=0;i<groupcontent.Size();i++)
      {
      ChannelID id=groupcontent[i];
      int pos=theApp->users.FindUserByID(id.majorid,id.minorid);
      if (pos!=-1) 
        {
        const char *name=theApp->users[pos].username;
        int i=SendMessage(users,LB_FINDSTRING,-1,(LPARAM)name);
        if (i!=LB_ERR) SendMessage(users,LB_SETSEL,TRUE,i);
        }
      }
    CheckDlgButton(*this,IDC_PRIVATEGROUP,BST_CHECKED);
    }
  else
    if (logauto) CheckDlgButton(*this,IDC_LOGAUTO,BST_CHECKED);
  
  if (soundonly && !privat) 
    {
    EnableWindow(GetDlgItem(IDC_JMENO),FALSE);
    EnableWindow(GetDlgItem(IDC_PRIVATEGROUP),FALSE);
    EnableWindow(GetDlgItem(IDC_LOGAUTO),FALSE);
    }
  
  DialogRules();
  return CDialogClass::OnInitDialog();
  }

//--------------------------------------------------

LRESULT CGroupNameDlg::OnOK()
  {
  groupresult.Clear();
  result=GetDlgItemText(IDC_JMENO);  
  if (stricmp(result,"debugconsole")==0)
    {
    free(result);
    result=NULL;
    theApp->OpenConsole();
    return 0;
    }
  int pos=theApp->groups.FindGroup(result);  
  if (pos!=-1 && pos!=editidx)
    {
    MessageBox(*this,StrRes[IDS_GROUPALREADYEXISTS],appname,MB_OK|MB_ICONINFORMATION);
    free(result);
    result=NULL;
    return 1;
    }
  if (IsDlgButtonChecked(*this,IDC_PRIVATEGROUP)==BST_CHECKED)
    {
    privat=true;
    int cnt=SendMessage(users,LB_GETCOUNT,0,0);
    for (int i=0;i<cnt;i++)
      {
      if (SendMessage(users,LB_GETSEL,i,0)>0)
        {
        char buff[256];
        if (SendMessage(users,LB_GETTEXTLEN,i,0)<255)
          {
          SendMessage(users,LB_GETTEXT,i,(LPARAM)buff);
          int pos=theApp->users.FindUser(buff);
          if (pos!=-1)
            groupresult.Add(ChannelID(theApp->users[pos].majid,theApp->users[pos].minid));
          }
        }
      }
    }
  else
    privat=false;
  logauto=IsDlgButtonChecked(*this,IDC_LOGAUTO)==BST_CHECKED;
  free(sound);
  sound=soundname[0]?strdup(soundname):NULL;
  
  return CDialogClass::OnOK();
  }

//--------------------------------------------------

LRESULT CGroupNameDlg::OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt)
  {
  if (wID==IDC_JMENO || wID==IDC_PRIVATEGROUP) DialogRules();
  if (wID==IDC_CLEAR)
    {
    int cnt=SendMessage(users,LB_GETCOUNT,0,0);
    for (int i=0;i<cnt;i++) SendMessage(users,LB_SETSEL,0,i);
    }
  if (wID==IDC_BROWSESOUND)
    {
    if (AskForSound(soundname,sizeof(soundname),*this))  SetSoundName(soundname);
    }
  if (wID==IDC_BROWSECLEAR)
    {
    soundname[0]=0;
    SetSoundName(soundname);
    }
  return CDialogClass::OnCommand(wID,wNotifyCode,clt);
  }

//--------------------------------------------------

void CGroupNameDlg::DialogRules()
  {
  EnableWindow(GetDlgItem(IDOK),GetWindowTextLength(GetDlgItem(IDC_JMENO))!=0);
  BOOL p=IsDlgButtonChecked(*this,IDC_PRIVATEGROUP)==BST_CHECKED;
  EnableWindow(GetDlgItem(IDC_USERS),p);
  EnableWindow(GetDlgItem(IDC_LOGAUTO),!p && !soundonly);
  }

//--------------------------------------------------

void CGroupNameDlg::SetSoundName(const char *name)
  {
  const char *p=strrchr(name,'\\');
  if (p==NULL) p=(char *)name;else p++;
  SetDlgItemText(IDC_BROWSESOUND,p);
  }

//--------------------------------------------------

