// LanguageDialog.cpp: implementation of the CLanguageDialog class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"
#include "LanguageDialog.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

LCID languages[]=
  {
  MAKELCID(MAKELANGID(LANG_ENGLISH,SUBLANG_DEFAULT),SORT_DEFAULT),
  MAKELCID(MAKELANGID(LANG_CZECH,SUBLANG_DEFAULT),SORT_DEFAULT)
  };

CLanguageDialog::CLanguageDialog()
  {
  
  }

CLanguageDialog::~CLanguageDialog()
  {

  }



LRESULT  CLanguageDialog::OnInitDialog()
  {
  int cnt=sizeof(languages)/sizeof(languages[0]);
  char buff[256];
  for (int i=0;i<cnt;i++)
	{
	GetLocaleInfo(languages[i],LOCALE_SLANGUAGE,buff,sizeof(buff));
	int p=ListBox_AddString(GetDlgItem(IDC_LANGLIST),buff);
	ListBox_SetItemData(GetDlgItem(IDC_LANGLIST),p,languages[i]);	
	}
  EnableWindow(GetDlgItem(IDOK),FALSE);
  return 0;
  }


LRESULT CLanguageDialog::OnOK()
  {
  HWND list=GetDlgItem(IDC_LANGLIST);
  int p=ListBox_GetCurSel(list);
  lang=ListBox_GetItemData(list,p);
  return CDialogClass::OnOK();
  }

LRESULT CLanguageDialog::OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt)
  {
  if (wID==IDC_LANGLIST) EnableWindow(GetDlgItem(IDOK),TRUE);
  return CDialogClass::OnCommand(wID,wNotifyCode,clt);
  }