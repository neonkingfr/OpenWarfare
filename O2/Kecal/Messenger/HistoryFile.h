// HistoryFile.h: interface for the CHistoryFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HISTORYFILE_H__D8B96F29_185D_4BF8_9053_E69D4C26A755__INCLUDED_)
#define AFX_HISTORYFILE_H__D8B96F29_185D_4BF8_9053_E69D4C26A755__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


using namespace std;

struct ChannelID;
class CHistoryFile  
  {
  ofstream &out;
  public:
    void AddToHistory(const char *from, const char *text);
    bool CreateHistory(const char *topic, const char *from, ChannelID &chan);
    CHistoryFile();
    virtual ~CHistoryFile();
    void MiniLog(int index);
  };










#endif // !defined(AFX_HISTORYFILE_H__D8B96F29_185D_4BF8_9053_E69D4C26A755__INCLUDED_)
