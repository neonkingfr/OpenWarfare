// UserList.h: interface for the CUserList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_USERLIST_H__526C8F1D_1690_4863_9BDE_3B6519B69FF8__INCLUDED_)
#define AFX_USERLIST_H__526C8F1D_1690_4863_9BDE_3B6519B69FF8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define USERMAXTIMEOUT 240 //cas po ktere se povazuje uzivatel za offline - v sekundach
#define USERREFRESH 60
#define GETUSERLISTTIMEOUT 60 //cas po kterem je poslan prikaz na GETUSERLIST - v sekundach
#define USERREPLYTIMEOUT 10 //cas po kterem je uzivatel povazovan za offline hned po odeslan zpravy.

#define USERINFOVERSION 0x102

#define UST_MAXSTATES 7
enum EUserState 
  {UST_Offline=0,UST_Online=1, UST_Away=2, UST_Invisible=3, UST_Deaf=4, UST_Working=5, UST_Gaming=6, UST_Deleted=99};

//--------------------------------------------------

inline ctArchive &operator>>=(ctArchive &arch, EUserState &state)
  {arch.serial(&state,sizeof(state));return arch;}

//--------------------------------------------------

struct SUserInfo
  {
  char *username; //jmeno uzivatele   
  char *email;  //e-mail uživatele
  DWORD majid;
  DWORD minid;
  EUserState state; //current user state  
  DWORD timeout;  //cas, kdy vyprsi timeout a uzivatel bude povazovan za offline
  DWORD vanish_date;  //cas, kdy uzivatel vyprsi z kontaktlistu, pokud se neprihlasi
  char *offlinemsg; //zprava zaslana uzivateli v okamziku, kdy se objevi online
  SOCKADDR_IN lastknownip; //posledni znama IP adresa uzivatele
  int version;
  
  SUserInfo() 
    {username=NULL;timeout=0;state=UST_Offline;offlinemsg=NULL;email=NULL;vanish_date=0;
    memset(&lastknownip,0,sizeof(lastknownip));}
  void SetName(const char *name) 
    {free(username);username=strdup(name);}
  void SetEmail(const char *name)
    {free(email);email=strdup(name);}
  void SetNotifyMsg(const char *msg) 
    {if (offlinemsg!=msg) {free(offlinemsg);offlinemsg=strdup(msg);}}
  void SetLongTimeout(DWORD curtime) 
    {timeout=curtime+USERMAXTIMEOUT*1000;}
  void SetShortTimout(DWORD curtime) 
    {timeout=curtime+USERREPLYTIMEOUT*1000;}
  bool IsTimout(DWORD curtime) 
    {return timeout<curtime;}
  void SetVanishDate()
    {
    SYSTEMTIME stim;
    GetSystemTime(&stim);
    stim.wMonth+=3;
    if (stim.wMonth>12) 
    {
      stim.wMonth-=12;
      stim.wYear+=1;
    }
    vanish_date=MAKELPARAM(MAKEWORD(stim.wDay,stim.wMonth),stim.wYear);
    }
  operator const char *() 
    {return username;}
  ~SUserInfo() 
    {free(username);free(offlinemsg);free(email);}
  const char *operator=(const char *other) 
    {SetName(other);SetLongTimeout(GetTickCount());return other;}
  void Serialize(ctArchive &arch);
  friend ctArchive& operator>>=(ctArchive& arch, SUserInfo &nfo);
  bool SetUserState(EUserState st);
  bool operator>(const SUserInfo &other) const 
    {
    int sw1=SortWeight(),sw2=other.SortWeight();
    if (sw1==sw2) return stricmp(username,other.username)>0;else return sw1<sw2;
    }
  bool operator<(const SUserInfo &other) const 
    {
    int sw1=SortWeight(),sw2=other.SortWeight();
    if (sw1==sw2) return stricmp(username,other.username)<0;else return sw1>sw2;
    }
  public:
    int SortWeight() const;
  };

//--------------------------------------------------

TypeIsMovable(SUserInfo);

class IUserListStateChange
{
public:
  virtual void UserStateChanged(int id)=0;
};

class CUserList:public AutoArray<SUserInfo>
  {  
  public:
    int FindUserByID(DWORD majid, DWORD minid);
    int FindByIdentity(const char *identity);
    bool RemoveDeleted();
    int FindUser(const char *logname);
    bool CheckTimeout(IUserListStateChange *notify=0);
    void CheckVanish();
    int QuickSearchUser(const char *logname);
  };

//--------------------------------------------------

#endif // !defined(AFX_USERLIST_H__526C8F1D_1690_4863_9BDE_3B6519B69FF8__INCLUDED_)
