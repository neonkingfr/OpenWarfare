// TemaDlg.cpp: implementation of the CTemaDlg class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TemaDlg.h"
#include "resource.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTemaDlg::CTemaDlg()
  {
  topic=NULL;
  }

//--------------------------------------------------

CTemaDlg::~CTemaDlg()
  {
  free(topic);
  }

//--------------------------------------------------

LRESULT CTemaDlg::OnOK()
  {
  topic=GetDlgItemText(IDC_MESSAGE);
  return CDialogClass::OnOK();
  }

//--------------------------------------------------

void CTemaDlg::DialogRules()
  {
  EnableWindow(GetDlgItem(IDOK),GetWindowTextLength(GetDlgItem(IDC_MESSAGE))!=0);
  }

//--------------------------------------------------

LRESULT CTemaDlg::OnInitDialog()
  {
  DialogRules();
  return CDialogClass::OnInitDialog();
  }

//--------------------------------------------------

LRESULT CTemaDlg::OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt)
  {
  if (wID==IDC_MESSAGE) DialogRules();
  return CDialogClass::OnCommand(wID,wNotifyCode,clt);
  }

//--------------------------------------------------

