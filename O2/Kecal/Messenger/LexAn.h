// LexAn.h: interface for the CLexAn class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LEXAN_H__A72AB05C_0CCC_40DD_8EB6_BBD3A8411494__INCLUDED_)
#define AFX_LEXAN_H__A72AB05C_0CCC_40DD_8EB6_BBD3A8411494__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define LEX_NUMBER 1 
#define LEX_STRING 2
#define LEX_EOF 3
#define LEX_ERR 4

class CLexAn  
  {
  char *buffer;
  char *strdata;
  int intdata;
  char *curptr;
  int lastrd;
  int charpos;
  public:
    int Read();
    int GetNumber() 
      {return intdata;}
    char *GetString() 
      {return strdata;}
    void SetBuffer(const char *b);
    char *GetRemainBuffer() 
      {return curptr;}
    int GetLastRead() 
      {return lastrd;}
    int GetLastCharPos() 
      {return charpos;}
    CLexAn();
    virtual ~CLexAn();
  };

//--------------------------------------------------

#endif // !defined(AFX_LEXAN_H__A72AB05C_0CCC_40DD_8EB6_BBD3A8411494__INCLUDED_)
