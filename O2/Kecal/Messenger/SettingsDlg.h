// SettingsDlg.h: interface for the CSettingsDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SETTINGSDLG_H__F5595188_A214_4727_AFAF_D2EDFA46E852__INCLUDED_)
#define AFX_SETTINGSDLG_H__F5595188_A214_4727_AFAF_D2EDFA46E852__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DialogClass.h"

#include "SConfig.h"

class CSettingsDlg : public CDialogClass  
  {
  HWND wOk;
  char imagename[MAX_PATH*4];
  char soundname[MAX_PATH*4];
  COLORREF fontcolor;
  public:
	LRESULT DlgProc(UINT msg, WPARAM wParam, LPARAM lParam);
    void DialogRules();
    SConfig *cfg;
    bool langrestart;
    char *oldname;
    LRESULT OnOK();
    LRESULT OnInitDialog();
    CSettingsDlg();
    virtual ~CSettingsDlg();
    LRESULT OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt);
    
  private:
    void SetButtonName(int idc,const char *name);
  };

//--------------------------------------------------

#endif // !defined(AFX_SETTINGSDLG_H__F5595188_A214_4727_AFAF_D2EDFA46E852__INCLUDED_)
