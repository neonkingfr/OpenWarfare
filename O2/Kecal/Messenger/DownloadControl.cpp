// DownloadControl.cpp: implementation of the CDownloadControl class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DownloadControl.h"
#include "application.h"
#include "resource.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDownloadControl::CDownloadControl()
  {
  buffer=0;
  blocklist=0;
  fname=0;
  }

//--------------------------------------------------

CDownloadControl::~CDownloadControl()
  {
  free(buffer);
  free(blocklist);
  free(fname);
  }

//--------------------------------------------------

void CDownloadControl::PrepareDownload(int fsize, int blocks)
  {
  if (buffer) return;
  buffer=(char *)malloc(DOWNLOAD_BLOCKSIZE*blocks);
  blocklist=(bool *)malloc(blocks);
  memset(blocklist,0,blocks);
  bufsize=blocks;
  blockremain=blocks;
  filesize=fsize;
  char path[512];
  GetModuleFileName(theApp->hInst,path,512);
  char *c=strrchr(path,'\\')+1;
  ChannelID id;
  GenerateUID(id.majorid,id.minorid);
  id.print(c,500-strlen(c));
  strcat(c,".dll");
  fname=strdup(path);
  }

//--------------------------------------------------

bool CDownloadControl::ReceiveBlock(int blockid, char *data)
  {
  if (buffer==0) return false;
  if (blockremain==0) return true;
  if (blockid>=0 && blockid<bufsize)
    {
    if (blocklist[blockid]==false)
      {
      memcpy(buffer+blockid*DOWNLOAD_BLOCKSIZE,data,DOWNLOAD_BLOCKSIZE);
      blocklist[blockid]=true;
      blockremain--;
      if (blockremain==0)
        {
        HANDLE f=CreateFile(fname,GENERIC_WRITE,FILE_SHARE_READ,NULL,CREATE_ALWAYS,FILE_FLAG_SEQUENTIAL_SCAN,NULL);
        if (f==NULL) 
          {
          MessageBox(NULL,StrRes[IDS_UNABLETOSAVEVERSION],appname,MB_OK|MB_ICONEXCLAMATION);
          return true;
          }
        DWORD written;
        WriteFile(f,buffer,filesize,&written,NULL);
        CloseHandle(f);
        if (written!=(DWORD)filesize)
          {
          MessageBox(NULL,StrRes[IDS_UNABLETOSAVEVERSION],appname,MB_OK|MB_ICONEXCLAMATION);
          DeleteFile(fname);
          return true;
          }
        return true;
        }
      }
    }
  return false;
  }

//--------------------------------------------------

void CDownloadControl::DropBuffers()
  {
  free(buffer);
  free(blocklist);
  free(fname);
  buffer=0;
  blocklist=0;
  fname=0;
  blockremain=0;
  }

//--------------------------------------------------

