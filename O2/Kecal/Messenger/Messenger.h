
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the MESSENGER_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// MESSENGER_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef MESSENGER_EXPORTS
#define MESSENGER_API __declspec(dllexport)
#else
#define MESSENGER_API __declspec(dllimport)
#endif

// This class is exported from the Messenger.dll
class MESSENGER_API CMessenger 
  {
  public:
    CMessenger(void);
    // TODO: add your methods here.
  };

//--------------------------------------------------

extern MESSENGER_API int nMessenger;

MESSENGER_API int fnMessenger(void);

