// DownloadControl.h: interface for the CDownloadControl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DOWNLOADCONTROL_H__BA6F6090_F06D_4281_B96C_6C122BE2793B__INCLUDED_)
#define AFX_DOWNLOADCONTROL_H__BA6F6090_F06D_4281_B96C_6C122BE2793B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define DOWNLOAD_BLOCKSIZE 512

class CDownloadControl  
  {
  char *buffer;
  bool *blocklist;
  char *fname;
  int bufsize;
  int blockremain;
  int filesize;
  public:
    void DropBuffers();
    bool ReceiveBlock(int blockid, char *data);
    void PrepareDownload(int fsize, int blocks);
    CDownloadControl();
    virtual ~CDownloadControl();
    
  };

//--------------------------------------------------

#endif // !defined(AFX_DOWNLOADCONTROL_H__BA6F6090_F06D_4281_B96C_6C122BE2793B__INCLUDED_)
