// About.cpp: implementation of the CAbout class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "About.h"
#include "resource.h"
#include "version.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAbout::CAbout()
  {
  
  }

//--------------------------------------------------

CAbout::~CAbout()
  {
  
  }

//--------------------------------------------------

LRESULT CAbout::OnInitDialog()
  {
  SetDlgItemInt(IDC_VERSION,VERSION_NR,FALSE);
  CDialogClass::OnInitDialog();
  return 0;
  }

//--------------------------------------------------

