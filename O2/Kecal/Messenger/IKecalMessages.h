#pragma once

struct KecalMsgHeader
{
  const char *name; ///<Name of kecal's identity
  EUserState state; ///<state of sender
  ChannelID uid;  ///kecal's unique id;

  KecalMsgHeader(),name(0) {}
  KecalMsgHeader(const char *name, EUserState state, ChannelID &uid):
  name(strdup(name)),state(state),uid(uid) {}
  ~KecalMsgHeader() {free(name);}
  }
};


struct KecalUserExtraInfo
{
  const char *realName;
  BITMAPINFOHEADER
};



class IKecalMessages
{
public:

  virtual ~IKecalMessages(void) {}

  virtual void SetHeader(const KecalMsgHeader &header);
  virtual const KecalMsgHeader &GetHeader() const;

  virtual void IdleMsg()=0;
  virtual void Refresh()=0;
  virtual void Groups(const Array<const char *> &groups, const Array<bool> privateMarks)=0;
  virtual void OpenChat(ChannelID chanId, const char *topic, const Array<const char *> &users, const Array<const char *> &groups)=0;
  virtual void RequestChannelName(ChannelID chanId)=0;
  virtual void ChannelName(ChannelID chanId, const char *name)=0;
  virtual void ChannelMessage(ChannelID chanId, const char *text)=0;
  virtual void DownloadRequest(const char *fromUser)=0;
  virtual void DownloadInfo(size_t size, size_t blocks)=0;
  virtual void DownloadData(size_t blockid, void *blockdata, size_t blocksize)=0;
  virtual void PeerMessage(ChannelID userid, const char *text)=0;
  virtual void PeerMessageAck(int messageId)=0;
  virtual void PeerNotify(int notifyId, ChannelID targetUser)=0;

  ///new messages
  virtual void Request


};
