// WaveOut.h: interface for the CWaveOut class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WAVEOUT_H__19B48C2C_F113_4507_8D49_F9253223968B__INCLUDED_)
#define AFX_WAVEOUT_H__19B48C2C_F113_4507_8D49_F9253223968B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WaveInOut.h"

class CWaveOut : public CWaveInOut  
{
	HWAVEOUT handle;
public:
	void Close();
	MMRESULT Open(HINSTANCE hInst, HWND hMainWnd, UINT uDeviceID, LPWAVEFORMATEX pwfx);

	virtual bool BufferDrop(char *buff, int size)=0;
    virtual void OnBufferDrop(WAVEHDR *hdr);
	virtual MMRESULT Reset();
	virtual MMRESULT Start();
	virtual MMRESULT Stop();
	virtual MMRESULT Pause();
	virtual MMRESULT Resume();
	virtual void ReuseLeftBuffers();	
	CWaveOut();
	virtual ~CWaveOut();

};

#endif // !defined(AFX_WAVEOUT_H__19B48C2C_F113_4507_8D49_F9253223968B__INCLUDED_)
