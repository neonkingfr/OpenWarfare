// GroupNameDlg.h: interface for the CGroupNameDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GROUPNAMEDLG_H__41A9F16D_93B9_4B93_A85B_C4A60045BA38__INCLUDED_)
#define AFX_GROUPNAMEDLG_H__41A9F16D_93B9_4B93_A85B_C4A60045BA38__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DialogClass.h"

class CGroupNameDlg : public CDialogClass  
  {
  HWND users;
  char soundname[MAX_PATH*4];
  
  void SetSoundName(const char *name);
  public:
    void DialogRules();
    LRESULT OnOK();
    LRESULT OnInitDialog();
    char *grpname;
    char *result;
    char *sound;
    AutoArray<ChannelID> groupcontent;
    AutoArray<ChannelID> groupresult;
    bool privat;
    bool logauto;
    int editidx;
    
    bool soundonly;
    
    CGroupNameDlg();
    virtual ~CGroupNameDlg();
    virtual LRESULT OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt);
    
    
  };

//--------------------------------------------------

#endif // !defined(AFX_GROUPNAMEDLG_H__41A9F16D_93B9_4B93_A85B_C4A60045BA38__INCLUDED_)
