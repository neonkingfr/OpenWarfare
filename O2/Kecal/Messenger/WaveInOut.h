// WaveInOut.h: interface for the CWaveInOut class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WAVEINOUT_H__B29A40CA_277F_484C_953E_814A0BFF3B28__INCLUDED_)
#define AFX_WAVEINOUT_H__B29A40CA_277F_484C_953E_814A0BFF3B28__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define WAVEBUF_GRANUALITY 2000
class CWaveInOut  
  {
protected:
  HWND notify;  
  char *buffer;
  WAVEHDR *freelist;
  int bufsize;
  int position;

  void FreeHeaders();
  void AllocHeaders();
  void ReleaseHeader(WAVEHDR *hdr);
  WAVEHDR * GetHeader();
  BOOL Create(HINSTANCE hIns, HWND parent);
  void Release();
  HWND GetCallbackHandle() {return notify;}

public:
	virtual bool BufferDrop(char *buff, int size)=0;
	virtual void OnOpen() {}
	virtual void OnClose() {}
    virtual void OnBufferDrop(WAVEHDR *hdr)=0;
	virtual MMRESULT Reset()=0;
	virtual MMRESULT Start()=0;
	virtual MMRESULT Stop()=0;
	virtual MMRESULT Pause()=0;
	virtual MMRESULT Resume()=0;
	virtual void ReuseLeftBuffers()=0;
	virtual bool InProgress() {return notify!=NULL;}
	void AllocBuffer(int maxsize);
	
	CWaveInOut();
	virtual ~CWaveInOut();
  
  };

#endif // !defined(AFX_WAVEINOUT_H__B29A40CA_277F_484C_953E_814A0BFF3B28__INCLUDED_)
