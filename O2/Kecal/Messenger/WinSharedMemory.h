#pragma once

#include "ISharedMemory.h"
#include <Windows.h>


class WinSharedMemory;

class WinS_HWNDTarget:public ISharedMemoryTarget
{
  friend WinSharedMemory;
  HWND _hWnd;
public:
  WinS_HWNDTarget(HWND hWnd): _hWnd(hWnd) {}
  virtual unsigned long GetTargetType() const;
  virtual void *GetTarget() const {return NULL;}
  static const WinS_HWNDTarget *IsMyTarget(const ISharedMemoryTarget &);
};

class WinS_Message: public ISharedMemoryTarget
  {
  friend WinSharedMemory;
public:
  WPARAM wParam;
  LPARAM lParam;
  WinS_Message(WPARAM wParam, LPARAM lParam):wParam(wParam),lParam(lParam) {}
  virtual unsigned long GetTargetType() const;
  virtual void *GetTarget() const {return NULL;}
  static const WinS_Message*IsMyTarget(const ISharedMemoryTarget &);



  };

class WinSharedMemory :public ISharedMemory
{
  HANDLE _lock;
  HANDLE _memory;
  size_t _memsize;
  size_t _lockoffset;
  size_t _locksize;
  bool _islocked;
  void *_lockptr;
  bool _sharelockenabled;
  WinS_HWNDTarget _target;
  HANDLE _file;

  bool SafeLock(DWORD timeout=INFINITE) const;

public:
  /// Constructs shared memory using file mappings
  /** @param enablegloballocks if true, function sends handle of mutex to other applications
  to handle global locks. This will disables wUserData in Share function.
  Use false only if you constructs instance to use SendPackage function only.
  It enables wUserData in function SendPackage
  @param file, handle to file, that will used during shared memory construction. If NULL, swapfile is used.
  */
  
  WinSharedMemory(bool enablegloballocks=true, HANDLE _file=NULL);
  
  /// Synchronizes content with shared memory of another application
  /** wParam and lParam is parameters received from other side. This parameters
  is filled by ShareWith function during synchronizing contents. After function successfully 
  returns  shared memory is inicialized, and holds the same content as other side. All
  changes made in this memory is projected to other side.
  */
  bool Synchronize(WPARAM wParam, LPARAM lParam);
  

  virtual ~WinSharedMemory(void);

  virtual bool Allocate(size_t size);

  virtual void *Lock(size_t offset=0, size_t size=0, bool exclusive=true, DWORD waittime=0xFFFFFFFF);
  virtual bool Unlock();
  virtual bool SetTarget(const ISharedMemoryTarget &target);
  virtual const ISharedMemoryTarget &GetCurrentTarget() const;
  virtual bool Share(unsigned int messageId, unsigned long userData=0) const ;
  virtual size_t Size()const;
  virtual bool IsLocked(size_t *offset=NULL, size_t *size=NULL) const;
  virtual bool ShareSimulateCopy() const  {return false;}
  virtual bool SendPackage( unsigned int messageId, const void *data, size_t size, unsigned long wUserData=0);
  virtual bool Synchronize(const ISharedMemoryTarget &message);
  virtual void Free();
  virtual bool IsBusy() const;

  
};
