// WaveInOut.cpp: implementation of the CWaveInOut class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "WaveInOut.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWaveInOut::CWaveInOut()
  {
  buffer=NULL;
  bufsize=0;
  freelist=NULL;
  notify=NULL;
  }

CWaveInOut::~CWaveInOut()
  {
  FreeHeaders();
  free(buffer);
  if (notify) Release();
  }

void CWaveInOut::AllocBuffer(int maxsize)
  {  
  FreeHeaders();  
  free(buffer);
  bufsize=maxsize;
  bufsize=((bufsize+WAVEBUF_GRANUALITY-1)/WAVEBUF_GRANUALITY)*WAVEBUF_GRANUALITY;
  buffer=(char *)malloc(bufsize);
  position=0;  
  AllocHeaders();
  }

void CWaveInOut::FreeHeaders()
  {
  while (freelist)
	{
	WAVEHDR *x=freelist;
	freelist=freelist->lpNext;
	free(x);
	}
  }

void CWaveInOut::AllocHeaders()
  {
  int i;
  for (i=0;i<bufsize;i+=WAVEBUF_GRANUALITY)
	{
	WAVEHDR *x=(WAVEHDR *)malloc(sizeof(WAVEHDR));
	x->lpData=buffer+i;
	x->dwBufferLength=WAVEBUF_GRANUALITY;
	x->dwFlags=0;
	x->dwLoops=0;
	x->dwUser=0;
	x->reserved=0;
	x->lpNext=freelist;
	freelist=x;
	}
  }

WAVEHDR *CWaveInOut::GetHeader()
  {
  WAVEHDR *p=freelist;
  if (p==NULL) return NULL; //no header left
  freelist=freelist->lpNext;
  p->lpNext=NULL;
  return p;
  }

void CWaveInOut::ReleaseHeader(WAVEHDR *hdr)
  {
  hdr->lpNext=freelist;
  freelist=hdr;
  }

static LRESULT CALLBACK NotifyWindowProc(HWND hWnd, UINT msg,  WPARAM wParam, LPARAM lParam)
  {
  CWaveInOut *self=(CWaveInOut *)GetWindowLong(hWnd,GWL_USERDATA);
  switch (msg)
	{
	case MM_WOM_OPEN:
	case MM_WIM_OPEN:self->OnOpen();break;
	case MM_WOM_CLOSE:
	case MM_WIM_CLOSE: self->OnClose();break;
	case MM_WIM_DATA:
	case MM_WOM_DONE: self->OnBufferDrop((WAVEHDR *)lParam);break;
	default: return DefWindowProc(hWnd,msg,wParam,lParam);
	}
  return 0;  
  }

BOOL CWaveInOut::Create(HINSTANCE hIns, HWND parent)
  {
  static WNDCLASS wclass=
	{0,&NotifyWindowProc,0,0,0,0,0,0,0,"BredyWaveInOutNotificationClass"};

  if (wclass.hInstance==NULL)
	{
	wclass.hInstance=hIns;
	RegisterClass(&wclass);
	}
  notify=CreateWindow("BredyWaveInOutNotificationClass","WaveInOutNotificationSink",0,0,0,0,0,parent,NULL,hIns,NULL);
  if (notify==NULL) return FALSE;
  SetWindowLong(notify,GWL_USERDATA,(LONG)this);
  return TRUE;
  }

void CWaveInOut::Release()
  {
  DestroyWindow(notify);
  notify=NULL;
  }
