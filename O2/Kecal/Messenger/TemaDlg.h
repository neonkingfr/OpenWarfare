// TemaDlg.h: interface for the CTemaDlg class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEMADLG_H__33892088_353A_4E6D_B795_056FB2F0D3BC__INCLUDED_)
#define AFX_TEMADLG_H__33892088_353A_4E6D_B795_056FB2F0D3BC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DialogClass.h"

class CTemaDlg : public CDialogClass  
  {
  char *topic;
  public:	
    virtual LRESULT OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt);
    LRESULT OnInitDialog();
    LRESULT OnOK();
    CTemaDlg();
    virtual ~CTemaDlg();
    
    const char *GetTopic() 
      {return topic;}
    
  protected:
    void DialogRules();
  };

//--------------------------------------------------

#endif // !defined(AFX_TEMADLG_H__33892088_353A_4E6D_B795_056FB2F0D3BC__INCLUDED_)
