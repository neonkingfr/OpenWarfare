// stdafx.cpp : source file that includes just the standard includes
//	BisMessenger3.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

// TODO: reference any additional headers you need in STDAFX.H
// and not in this file

void MoveWindowRel(HWND hwnd, int xm, int ym, int xsm, int ysm,HDWP dwp)
  {
  RECT rc;
  GetWindowRect(hwnd,&rc);
  rc.left+=xm;
  rc.right+=xm;
  rc.top+=ym;
  rc.bottom+=ym;
  rc.right+=xsm;
  rc.bottom+=ysm;
  HWND parent=GetParent(hwnd);
  HWND owner=GetWindow(hwnd,GW_OWNER);
  if (parent && owner!=parent) 
    {
    ScreenToClient(parent,((POINT *)&rc));
    ScreenToClient(parent,((POINT *)&rc)+1);
    }
  DeferWindowPos(dwp,hwnd,NULL,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,SWP_NOZORDER);
  }










HKEY OpenKey(HKEY base, const char *path)
  {
  char *buff=(char *)alloca(1+strlen(path));
  strcpy(buff,path);
  HKEY last=base;
  HKEY cur;
  DWORD temp;
  char *c=buff;
  do
    {
    c=strchr(buff,'\\');
    if (c!=NULL) *c=0;
    int res=RegCreateKeyEx(last,buff,0,"",REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&cur,&temp);
    if (last!=base) RegCloseKey(last);
    last=cur;
    if (res!=ERROR_SUCCESS) return NULL;
    buff=c+1;
    }
  while (c);
  return cur;
  }










HKEY OpenProfileKey()
  {
  return OpenKey(HKEY_CURRENT_USER,"Software\\Bohemia Interactive Studio\\Kecal");
  }










//path=NULL remove
void RunAtStartup(const char *name, const char *path)
  {
  HKEY key=OpenKey(HKEY_CURRENT_USER,"Software\\Microsoft\\Windows\\CurrentVersion\\Run");
  if (path)
    RegSetValueEx(key,name,0,REG_SZ,(CONST BYTE *)path,strlen(path)+1);
  else 
    RegDeleteValue(key,name);
  }










void GenerateUID(DWORD &majid, DWORD &minid)
  {
  SYSTEMTIME syst;
  GetSystemTime(&syst);
  majid=syst.wYear+syst.wMonth*4000+syst.wDay*13*4000+syst.wHour*13*32*4000+syst.wMinute*13*32*24*4000;
  minid=syst.wSecond+syst.wMilliseconds*60+3600*GetTickCount();
  }










DWORD CALLBACK RichEdit_EditStreamCallback(
                                           DWORD dwCookie, // application-defined value
                                           LPBYTE pbBuff,  // pointer to a buffer
                                           LONG cb,        // number of bytes to read or write
                                           LONG *pcb       // pointer to number of bytes transferred
                                           )
    {
    istrstream *in=(istrstream *)dwCookie;
    in->read((char *)pbBuff,cb);
    *pcb=in->gcount();
    return 0;
    }










void RichEdit_ReplaceSel(HWND control,const char *text)
  {
  istrstream in(const_cast<char *>(text));
  EDITSTREAM estr;
  estr.dwCookie=(DWORD)(&in);
  estr.dwError=0;
  estr.pfnCallback=RichEdit_EditStreamCallback;
  SendMessage(control,EM_STREAMIN,SF_TEXT|SFF_SELECTION,(LPARAM)(&estr));
  }










void RichEdit_SetCurrentFormat(HWND control,DWORD mask, DWORD values,DWORD color, char *face, int size)
  {
  CHARFORMAT2 chf;
  chf.cbSize=sizeof(chf);
  chf.dwMask=mask;
  chf.dwEffects=values;
  chf.crTextColor=color;
  if (face) _snprintf(chf.szFaceName,LF_FACESIZE,"%s",face);
  chf.yHeight=size;
  SendMessage(control,EM_SETCHARFORMAT,SCF_SELECTION,(LPARAM)(&chf));
  }










void SavePosition(HWND hWnd, const char *filename)
  {
  WINDOWPLACEMENT wp;
  wp.length=sizeof(wp);
  GetWindowPlacement(hWnd,&wp);
  HKEY key=OpenProfileKey();
    {
    ctArchiveOutReg arch(key,filename);
    arch.serial(&wp,sizeof(wp));
    }
  RegCloseKey(key);
  }










bool LoadPosition(HWND hWnd, const char *filename, WINDOWPLACEMENT *wpp)
  {
  WINDOWPLACEMENT wp;
  HKEY key=OpenProfileKey();
    {
    ctArchiveInReg arch(key,filename);
    RegCloseKey(key);
    if (!(!arch))
      {
      arch.serial(&wp,sizeof(wp));
      if (wpp==NULL)
        SetWindowPlacement(hWnd,&wp);
      else
        *wpp=wp;
      return true;
      }
    }
  return false;
  }










void CheckWindowVisibility(HWND hWnd)
  {
  RECT rc;
  GetWindowRect(hWnd,&rc);
  HMONITOR mon=MonitorFromRect(&rc,MONITOR_DEFAULTTONEAREST);
  MONITORINFO nfo;
  nfo.cbSize=sizeof(nfo);
  GetMonitorInfo(mon,&nfo);
  int shiftx=0,shifty=0;
  if (rc.left+80>nfo.rcWork.right) shiftx=nfo.rcWork.right-rc.right;
  if (rc.right-80<nfo.rcWork.left) shiftx=nfo.rcWork.left-rc.left;
  if (rc.top+80>nfo.rcWork.bottom) shifty=nfo.rcWork.bottom-rc.bottom;
  if (rc.bottom-80<nfo.rcWork.top) shifty=nfo.rcWork.top-rc.top;
  rc.right+=shiftx;
  rc.left+=shiftx;
  rc.bottom+=shifty;
  rc.top+=shifty;
  MoveWindow(hWnd,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,TRUE);
  }



