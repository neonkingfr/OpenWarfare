// ACMStream.h: interface for the ACMStream class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ACMSTREAM_H__17095ED6_FD96_4472_B066_871614D732CA__INCLUDED_)
#define AFX_ACMSTREAM_H__17095ED6_FD96_4472_B066_871614D732CA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define ACMMAXBUFF 65536
class ACMStream  
{
  unsigned char buffer[ACMMAXBUFF];
  HACMSTREAM drv;
  int nextload;

public:
	void Close();
	MMRESULT ConvertData(unsigned char *out, DWORD maxsize, DWORD &used, DWORD flags);
	void LoadData(const char *data, int size);
	MMRESULT Open(LPWAVEFORMATEX src,LPWAVEFORMATEX trg);

	ACMStream();
	virtual ~ACMStream();

};

#endif // !defined(AFX_ACMSTREAM_H__17095ED6_FD96_4472_B066_871614D732CA__INCLUDED_)
