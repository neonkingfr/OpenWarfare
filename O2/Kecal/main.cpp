#include <windows.h>
#include "Messenger/NetEngine.h"
#include "StringRes.h"
#include "loader.h"


#define KM_RAISEUP (WM_APP+2) 
#define KM_TEXT (WM_APP+7) //LPARAM a WPARAM 

typedef int (*tt_GetVersion)();
typedef int (*tt_Start)(HINSTANCE Instance);

HMODULE LoadLatestVersion(HINSTANCE hInstance)
  {
  DWORD minver=0;
  char pathname[512];
  char found[512]="";
  char findstring[512];
  
  GetModuleFileName((HMODULE)hInstance,findstring,512);
  char *c=strrchr(findstring,'\\')+1;
  strcpy(c,"*.dll");
  strcpy(pathname,findstring);
  c=pathname+(c-findstring);
  WIN32_FIND_DATA fnd;
  HANDLE f=FindFirstFile(findstring,&fnd);
  if (f)
    {
    for (BOOL succ=TRUE;succ;succ=FindNextFile(f,&fnd))
      {
      tt_GetVersion GetVersion;
      strcpy(c,fnd.cFileName);
      HMODULE ll=LoadLibrary(pathname);
      if (ll)
        {
        GetVersion=(tt_GetVersion)GetProcAddress(ll,"GetModuleVersion");
        if (GetVersion) 
          {
          DWORD z=GetVersion();
          if (z>minver) 
            {
            strcpy(found,pathname);
            minver=z;
            }
          }
        FreeLibrary(ll);
        }
      }
    FindClose(f);
    }
  if (found[0])
    {
    HANDLE f=FindFirstFile(findstring,&fnd);
    if (f)
      {
      for (BOOL succ=TRUE;succ;succ=FindNextFile(f,&fnd))
        {
        strcpy(c,fnd.cFileName);
        if (stricmp(pathname,found)) DeleteFile(pathname);
        }
      FindClose(f);
      }
    return LoadLibrary(found);
    }
  return NULL;
  }

//--------------------------------------------------
typedef struct tagMYREC
{
  char  user[200];
  char  message[20000];        
} MYREC;


char seps[]   = " ";
char *token, *next_token;


int __stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
  {
  //display help      
  if ((!strcmp(lpCmdLine,"/?")) || (!strcmp(lpCmdLine,"?")))  
  {
    MessageBox(NULL,"Usage (other kecal instance must run): kecal.exe user text\n\n  user ..who to send to\n  text ..text to send","Kecal",MB_OK);
    return 1;
  };

  //WM_COPYDATA msg uses this structure
  MYREC data;
  data.user[0] = '\0';
  data.message[0] ='\0';
  
  //retrieve command line arguments
  token = strtok_s( lpCmdLine, seps, &next_token );    
  int i=0;
  while( token != NULL )
  { 
    if (i==0) {strcpy_s(data.user,199,token);}
    else
      if (i==1) {strcpy_s(data.message,19999,token);}
      else
    {
      strcat_s(data.message,19999," ");
      strcat_s(data.message,19999,token);
    }    
    token = strtok_s( NULL, seps, &next_token ); // C4996
    i++;
  };

  if (stricmp(lpCmdLine,"/NEWINSTANCE"))
  {    
    HWND self=FindWindow(SOCKCLASSNAME,NULL);
    if (self) 
    {
      if (!_strcmpi(data.user,"")) return 1;
      if (!_strcmpi(data.user,"NOREPLY")) return 1;
      if (!_strcmpi(data.user,"UNKNOWN")) return 1;
      COPYDATASTRUCT MyCDS;      
      MyCDS.cbData = sizeof(data);
      MyCDS.lpData = (LPVOID)(&data);
      
      bool ret;
  //    DWORD err;
      if (strlen(data.user)>0)
      {
        ret = SendMessage(self,WM_COPYDATA,0,(LPARAM)&MyCDS);
      }
      else
      {
        ret = PostMessage(self,KM_RAISEUP,0,WM_RBUTTONUP);  
      }

      
      
      
      //PostMessage(self,KM_TEXT,(WPARAM)"vilem_2",(LPARAM)"Tuto 123456789");  
      return 1;
      }
    }
  CStringRes StrRes;
  StrRes.SetInstance(hInstance);
  tt_Start Start;
  HMODULE dll;
  int res;
  do
    {
    dll=LoadLatestVersion(hInstance);
    if (dll==NULL)
      {
      MessageBox(NULL,StrRes[IDS_UNABLETOFINDDLL],NULL,MB_OK|MB_ICONSTOP);
      return 255;
      }
    Start=(tt_Start)GetProcAddress(dll,"Start");
    if (Start==NULL)
      {
      MessageBox(NULL,StrRes[IDS_UNABLETOCREATELINK],NULL,MB_OK|MB_ICONSTOP);
      return 255;
      }
    res=Start((HINSTANCE)dll);
    FreeLibrary(dll);
    }
  while (res==1);
  return 0;
  }

//--------------------------------------------------

