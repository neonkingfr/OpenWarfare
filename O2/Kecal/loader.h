//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by loader.rc
//

#pragma warning(disable: 4996)

#define IDS_UNABLETOFINDDLL             1
#define IDS_UNABLETOCREATELINK          2
#define IDI_MAIN                        100

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
