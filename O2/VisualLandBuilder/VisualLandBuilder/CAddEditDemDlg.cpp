//-----------------------------------------------------------------------------

#include "stdafx.h"
#include ".\VisualLandBuilder.h"
#include ".\CAddEditDemDlg.h"

#include "..\..\LandBuilder\HighMapLoaders\include\EtHelperFunctions.h"

//-----------------------------------------------------------------------------

IMPLEMENT_DYNAMIC( CAddEditDemDlg, CDialog )

//-----------------------------------------------------------------------------

CAddEditDemDlg::CAddEditDemDlg( CWnd* parent /*=NULL*/ )
: CDialog( CAddEditDemDlg::IDD, parent )
, m_modified( false )
, m_visitorMap( NULL )
{
}

//-----------------------------------------------------------------------------

CAddEditDemDlg::~CAddEditDemDlg()
{
}

//-----------------------------------------------------------------------------

void CAddEditDemDlg::DoDataExchange( CDataExchange* dX )
{
  CDialog::DoDataExchange( dX );
  DDX_Control( dX, IDC_DEMFILENAME,       m_fileNameCtrl );
  DDX_Control( dX, IDC_DEMPARAMETERSLIST, m_parametersListCtrl );
  DDX_Control( dX, IDC_DEMTYPECOMBO,      m_typeListCtrl );
  DDX_Control( dX, IDC_DEMDESCRIPTION,    m_demDescriptionCtrl );
}

//-----------------------------------------------------------------------------

BEGIN_MESSAGE_MAP( CAddEditDemDlg, CDialog )
	ON_NOTIFY( NM_CLICK,         IDC_DEMPARAMETERSLIST, &CAddEditDemDlg::OnNMClickParametersList )
	ON_NOTIFY( LVN_ITEMACTIVATE, IDC_DEMPARAMETERSLIST, &CAddEditDemDlg::OnLvnItemActivateParametersList )
	ON_NOTIFY( LVN_KEYDOWN,      IDC_DEMPARAMETERSLIST, &CAddEditDemDlg::OnLvnKeydownParametersList )
	ON_BN_CLICKED( IDC_DEMEDIT,      &CAddEditDemDlg::OnBnClickedEdit )
	ON_BN_CLICKED( IDOK,             &CAddEditDemDlg::OnBnClickedOk )
	ON_BN_CLICKED( IDC_DEMBROWSEBTN, &CAddEditDemDlg::OnBnClickedBrowse )
	ON_BN_CLICKED( IDC_MAPDATA,      &CAddEditDemDlg::OnBnClickedMapData )
	ON_CBN_SELCHANGE( IDC_DEMTYPECOMBO, &CAddEditDemDlg::OnCbnSelChangeDemTypeCombo )
END_MESSAGE_MAP()

//-----------------------------------------------------------------------------

BOOL CAddEditDemDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// sets the type list control 
	for ( int i = LBDT_START_ENUM + 1; i < LBDT_END_ENUM; ++i )
	{
		m_typeListCtrl.AddString( LBDemTypesAsString[i].c_str() );
	}

	if ( m_dem.GetType() != LBDT_START_ENUM )
	{
		m_typeListCtrl.SetCurSel( m_dem.GetType() - 1 );
		GetDlgItem( IDC_DEMBROWSEBTN )->EnableWindow( TRUE );
	}
	else
	{
		GetDlgItem( IDC_DEMBROWSEBTN )->EnableWindow( FALSE );
	}


  if ( m_visitorMap )
  {
		GetDlgItem( IDC_MAPDATA )->EnableWindow( TRUE );
  }
  else
  {
		GetDlgItem( IDC_MAPDATA )->EnableWindow( FALSE );
  }

	// sets the object list control column header
	m_parametersListCtrl.InsertColumn( 0, string( HEAD_NAME ).c_str(), LVCFMT_LEFT );
	m_parametersListCtrl.SetColumnWidth( 0, 120 );
	m_parametersListCtrl.InsertColumn( 1, string( HEAD_VALUE ).c_str(), LVCFMT_RIGHT );
	m_parametersListCtrl.SetColumnWidth( 1, 120 );

	// sets the object list control style
	m_parametersListCtrl.SetExtendedStyle( LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT );

	// sets the in-place editing object for the list 
	m_parametersListCtrl.SetEditType( ET_EDIT );

	// sets the name control
	m_fileNameCtrl.SetWindowTextA( m_dem.GetFileName().c_str() );

	// sets the list control
	UpdateParametersListCtrl();

	return (TRUE);
}

//-----------------------------------------------------------------------------

void CAddEditDemDlg::UpdateParametersListCtrl()
{
	bool firstTime = false;

	INT_PTR listCount = m_dem.GetParametersList().GetCount();
	int listCtrlCount = m_parametersListCtrl.GetItemCount();

	// if the counts of item are different it means that this is the first 
	// time we enter this function
  if ( listCount != listCtrlCount ) { firstTime = true; }

	INT_PTR demParamCount = m_dem.GetParametersList().GetCount();
	for ( int i = 0; i < demParamCount; ++i )
	{
		LBParameter* item = m_dem.GetParametersList().GetAt( i );
		if ( firstTime )
		{
			// sets first column
			m_parametersListCtrl.InsertItem( i, item->GetName().c_str() );
		}
		// if the parameter has no default, show it bolded
		if ( !item->GetHasDefault() )
		{
			m_parametersListCtrl.SetItemStyle( i, 0, LIS_BOLD, true );
		}

		// modify the second column only if the data are changed
		string newValue = item->GetValue();
		CString oldValue = m_parametersListCtrl.GetItemText( i, 1 );

		if ( newValue != string( oldValue ) )
		{
			// sets second column
			m_parametersListCtrl.SetItem( i, 1, LVIF_TEXT, newValue.c_str(), 0, 0, 0, 0 );
			
			// if data are changed update m_modified
      if ( !firstTime ) { m_modified = true; }
		}
	}

	// if the list is empty, disables it
	if ( listCount == 0 )
	{
		m_parametersListCtrl.EnableWindow( FALSE );
	}

	// now check if the ok button can be enabled
	CheckOkButtonEnabling();
}

//-----------------------------------------------------------------------------

bool CAddEditDemDlg::ValidateParamCtrlInput( CString data, LBParameter* par )
{
	switch ( par->GetType() )
	{
	case LBPT_INTEGER:
		{
			if ( !IsInteger( string( data ), true ) )
			{
				AfxGetMainWnd()->MessageBox( string( MSG_MUSTBEINTEGER ).c_str(), string( MSG_ERROR ).c_str(), 
                                     MB_OK | MB_ICONSTOP);
				return (false);
			}
			else
			{
				return (true);
			}
		}
	case LBPT_FLOAT:
		{
			if ( !IsFloatingPoint( string( data ), true ) )
			{
				AfxGetMainWnd()->MessageBox( string( MSG_MUSTBEFLOATING ).c_str(), string( MSG_ERROR ).c_str(), 
                                     MB_OK | MB_ICONSTOP);
				return (false);
			}
			else
			{
				return (true);
			}
		}
	case LBPT_BOOL:
		{
			if ( !IsBool( string( data ), true ) )
			{
				AfxGetMainWnd()->MessageBox( string( MSG_MUSTBEZEROORONE ).c_str(), string( MSG_ERROR ).c_str(), 
                                     MB_OK | MB_ICONSTOP);
				return (false);
			}
			else
			{
				return (true);
			}
		}
	case LBPT_STRING: 
    {
      return (true);
    }
	default:
		AfxGetMainWnd()->MessageBox( string( MSG_DATANOTRECOGNIZED ).c_str(), string( MSG_ERROR ).c_str(), 
                                 MB_OK | MB_ICONSTOP );
		return (false);
	}
}

//-----------------------------------------------------------------------------

void CAddEditDemDlg::CheckOkButtonEnabling()
{
	if ( m_typeListCtrl.GetCurSel() == CB_ERR )
	{
		GetDlgItem( IDOK )->EnableWindow( FALSE );
		return;
	}

	CString data;
	m_fileNameCtrl.GetWindowTextA( data );

	if ( data.GetLength() == 0 )
	{
		GetDlgItem( IDOK )->EnableWindow( FALSE );
		return;
	}

	if ( !m_dem.GetParametersList().Validate() )
	{
		GetDlgItem( IDOK )->EnableWindow( FALSE );
		return;
	}

	GetDlgItem( IDOK )->EnableWindow( TRUE );
}

//-----------------------------------------------------------------------------

void CAddEditDemDlg::SetDem( LBDem* dem )
{
	// sets the working copy of the data
	m_dem = *dem;

	// sets the pointer to the original data
	m_oldDem = dem;
}

//-----------------------------------------------------------------------------

void CAddEditDemDlg::SetVisitorMap( const LBVisitorMap* visitorMap )
{
  m_visitorMap = visitorMap;
}

//-----------------------------------------------------------------------------

void CAddEditDemDlg::OnNMClickParametersList( NMHDR* nmhdr, LRESULT* result )
{
	// gets the index of the selected item in the list
	int selItem = m_parametersListCtrl.GetNextItem( -1, LVNI_SELECTED );

	// enable/disable change button
	if ( selItem != -1 )
	{
		LPNMLVKEYDOWN lVKeyDow = reinterpret_cast< LPNMLVKEYDOWN >( nmhdr );
    if ( lVKeyDow->wVKey == VK_UP )   { selItem--; }
    if ( lVKeyDow->wVKey == VK_DOWN ) { selItem++; }
    if ( selItem < 0 ) { selItem = 0; }
    if ( selItem > m_parametersListCtrl.GetItemCount() - 1 ) { selItem = m_parametersListCtrl.GetItemCount() - 1; }

		// something is selected
		GetDlgItem( IDC_DEMEDIT )->EnableWindow( TRUE );
		if ( m_parametersListCtrl.GetValidationPassed() )
		{
			m_demDescriptionCtrl.SetWindowTextA( m_dem.GetParametersList().GetAt( selItem )->GetDescription().c_str() );
		}
	}
	else
	{
		// no selection
		GetDlgItem( IDC_DEMEDIT )->EnableWindow( FALSE );
		m_demDescriptionCtrl.SetWindowTextA( "" );
	}

	*result = 0;
}

//-----------------------------------------------------------------------------

void CAddEditDemDlg::OnLvnItemActivateParametersList( NMHDR* nmhdr, LRESULT* result )
{
	OnBnClickedEdit();

	*result = 0;
}

//-----------------------------------------------------------------------------

void CAddEditDemDlg::OnLvnKeydownParametersList( NMHDR* nmhdr, LRESULT* result )
{
	OnNMClickParametersList( nmhdr, result );
	
	*result = 0;
}

//-----------------------------------------------------------------------------

void CAddEditDemDlg::OnBnClickedEdit()
{
	// gets the index of the selected item in the list
	int selItem = m_parametersListCtrl.GetNextItem( -1, LVNI_SELECTED );

	// sets a copy of the parameter
	LBParameter* par = new LBParameter( m_dem.GetParametersList().GetAt( selItem ) );

	// passes the copy to the dialog
	m_editParameterDlg.SetParameter( par );
	if ( m_editParameterDlg.DoModal() == IDOK )
	{
		m_dem.GetParametersList().SetAt( selItem, par );
		UpdateParametersListCtrl();
		m_modified = true;
	}
	else
	{
		delete par;
	}
}

//-----------------------------------------------------------------------------

void CAddEditDemDlg::OnBnClickedOk()
{
	// update data if modified
	if ( m_modified )
	{
		// gets data from dialog
		int type = m_typeListCtrl.GetCurSel() + 1;
		m_dem.SetType( (LBDemTypes)type );

		CString data;
		m_fileNameCtrl.GetWindowTextA( data );
		m_dem.SetFileName( data );

		// updates data
		*m_oldDem = m_dem;
	}
	OnOK();
}

//-----------------------------------------------------------------------------

void CAddEditDemDlg::OnBnClickedBrowse()
{
	string strFilter = LBDemFileFilters[m_typeListCtrl.GetCurSel() + 1];
	string strExt    = LBDemFileExt[m_typeListCtrl.GetCurSel() + 1];
	string strFile   = m_dem.GetFileName();

	DWORD flags = OFN_HIDEREADONLY | OFN_FILEMUSTEXIST | OFN_EXTENSIONDIFFERENT;
	CFileDialog fileDlg( TRUE, strExt.c_str(), strFile.c_str(), flags, strFilter.c_str(), this );

	if ( fileDlg.DoModal() == IDOK )
	{
		m_dem.SetFileName( fileDlg.GetPathName() );
		m_fileNameCtrl.SetWindowTextA( fileDlg.GetPathName() );
		UpdateParametersListCtrl();
		m_modified = true;
	}
}

//-----------------------------------------------------------------------------

void CAddEditDemDlg::OnBnClickedMapData()
{
	INT_PTR demParamCount = m_dem.GetParametersList().GetCount();
	for ( int i = 0; i < demParamCount; ++i )
	{
		LBParameter* item = m_dem.GetParametersList().GetAt( i );
		if ( item->GetName() == "dstLeft" )
    {
      item->SetValue( m_visitorMap->GetMapLeftAsString() );
    }
		else if ( item->GetName() == "dstBottom" )
    {
      item->SetValue( m_visitorMap->GetMapBottomAsString() );
    }
		else if ( item->GetName() == "dstRight" )
    {
      item->SetValue( m_visitorMap->GetMapRightAsString() );
    }
		else if ( item->GetName() == "dstTop" )
    {
      item->SetValue( m_visitorMap->GetMapTopAsString() );
    }
  }

	UpdateParametersListCtrl();
	m_modified = true;
}

//-----------------------------------------------------------------------------

void CAddEditDemDlg::OnCbnSelChangeDemTypeCombo()
{
	m_dem.SetFileName( string( "" ) );
	m_fileNameCtrl.SetWindowTextA( "" );
	GetDlgItem( IDC_DEMBROWSEBTN )->EnableWindow( TRUE );
	UpdateParametersListCtrl();
	m_modified = true;
}

//-----------------------------------------------------------------------------

BOOL CAddEditDemDlg::PreTranslateMessage( MSG* msg )
{
	if ( msg->message == MY_ENDEDITMSG )
	{
		// gets the index of the selected item in the list
		int selItem = static_cast< int >( msg->wParam );

		LBParameter* par = m_dem.GetParametersList().GetAt( selItem );
		CString data = m_parametersListCtrl.GetItemText( selItem, 1 );

		if ( ValidateParamCtrlInput( data, par ) )
		{
			par->SetValue( data );
			m_dem.GetParametersList().SetAt( selItem, par );
			m_parametersListCtrl.SetValidationPassed( true );
			UpdateParametersListCtrl();
			m_modified = true;
		}
		else
		{
			m_parametersListCtrl.SetValidationPassed( false );
			m_parametersListCtrl.SetItemText( selItem, 1, par->GetValue().c_str() );
			m_parametersListCtrl.SetItemState( selItem, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED );
			m_parametersListCtrl.SetFocus();
		}
	}

	return (CDialog::PreTranslateMessage( msg ));
}

//-----------------------------------------------------------------------------
