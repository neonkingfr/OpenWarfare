#pragma once
#include "afxcmn.h"

#include ".\LBModuleList.h"
#include ".\ExtendedControls\CListCtrlEx.h"

#define MY_PAINTMSG   0x1112

// CTabObjectColorDlg dialog

class CTabObjectColorListDlg : public CDialog
{
	DECLARE_DYNAMIC(CTabObjectColorListDlg)

public:
	CTabObjectColorListDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTabObjectColorListDlg();

// Dialog Data
	enum { IDD = IDD_TABOBJECTCOLOR };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	// the list ctrl
	CListCtrlEx m_ObjColorList;
	CImageList imageList;

	// pointer to the current module list
	LBModuleList* m_pModuleList;

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //
private:
	virtual BOOL OnInitDialog(void);

// -------------------------------------------------------------------------- //
// DIALOG EVENT HANDLERS                                                      //
// -------------------------------------------------------------------------- //
	afx_msg void OnSize(UINT nType, int cx, int cy);

// -------------------------------------------------------------------------- //
// INTERFACE                                                                  //
// -------------------------------------------------------------------------- //
public:
	void SetList(LBModuleList* moduleList, bool showAll);

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
private:
	bool AlreadyInList(string name);

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
private:
	// this function is used to intercept the end label editing in
	// the list ctrl
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
