#pragma once

#include <string>

#include ".\LBDirectoryItem.h"

using std::string;

class LBDirectoryList : public CObject
{
	DECLARE_SERIAL(LBDirectoryList)

	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// the version
	int m_Version;
	// the list of parameters
	CTypedPtrList<CObList, LBDirectoryItem*> m_List;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************************************************************** //
	// Constructors                                                   //
	// ************************************************************** //

	// Default constructor
	LBDirectoryList();

	// copy constructor
	LBDirectoryList(const LBDirectoryList& other);

	// ************************************************************** //
	// Destructor                                                     //
	// ************************************************************** //
	virtual ~LBDirectoryList();

	// ************************************************************** //
	// Assignement                                                    //
	// ************************************************************** //
	LBDirectoryList& operator = (const LBDirectoryList& other);

	// ************************************************************** //
	// Member variables getters                                       //
	// ************************************************************** //

	// GetList()
	// returns the list
	CTypedPtrList<CObList, LBDirectoryItem*>& GetList();

	// ************************************************************** //
	// Member variables setters                                       //
	// ************************************************************** //

	// SetList()
	// sets the list with the given one
	void SetList(CTypedPtrList<CObList, LBDirectoryItem*>& newList);

	// ************************************************************** //
	// Member variables manipulators                                  //
	// ************************************************************** //
	
	// Append()
	// adds a copy of the the given dem at the end of this list
	void Append(const LBDirectoryItem& directory);
	void Append(LBDirectoryItem* directory);

	// GetAt()
	// returns the dem of this list at the given index 
	LBDirectoryItem* GetAt(int index);

	// SetAt()
	// sets the dem of this list at the given index with the given dem
	void SetAt(int index, LBDirectoryItem* directory);

	// RemoveAt()
	// removes the dem at the given index from this list
	void RemoveAt(int index);

	// RemoveAll()
	// removes all the dems from this list
	void RemoveAll();

	// ************************************************************** //
	// Interface                                                      //
	// ************************************************************** //

	// GetCount()
	// returns the number of items in this list
	INT_PTR GetCount() const;

	// IsEmpty()
	// returns true if the list is empty
	BOOL IsEmpty() const;

	// Validate()
	// returns the result of data validation of the directory of this list
	LBErrorTypes Validate() const;

	// GetWorkingFolder()
	// returns the working folder
	string GetWorkingFolder() const;

// ******************************************
// Removed connection to LB1
// 08.04.2009
// ******************************************
/*
	// GetLandBuilderPath()
	// returns the path to LandBuilder.exe
	string GetLandBuilderPath() const;
*/

	// GetLandBuilder2Path()
	// returns the path to LandBuilder2.exe
	string GetLandBuilder2Path() const;

	// ************************************************************** //
	// Serialization                                                  //
	// ************************************************************** //
	virtual void Serialize(CArchive& ar);
};
