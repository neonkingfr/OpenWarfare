#pragma once

#include <string>

#include ".\LBError.h"

using std::string;

// *********************** //
// Enum for resource types //
// *********************** //
typedef enum
{
	LBDI_FILE,
	LBDI_FOLDER
} LBDirectoryItemTypes;

class LBDirectoryItem : public CObject
{
	DECLARE_SERIAL(LBDirectoryItem)

	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// the version
	int m_Version;
	// the resource name
	string m_Resource;
	// the resource pathname
	// (path+file+ext in case of file / only path in case of folder)
	string m_ResourcePathName;
	// the resource type
	LBDirectoryItemTypes m_ResourceType;
	// the resource filter
	// (to be used in the CFileDialog call)
	string m_ResourceFilter;
	// the resource file fixed name
	// (when a resource MUST have this file name)
	string m_ResourceFileName;
	// the resource file fixed extension
	// (when a resource MUST have this extension)
	string m_ResourceExtension;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************************************************************** //
	// Constructors                                                   //
	// ************************************************************** //

	// Default constructor
	LBDirectoryItem();

	// Constructs this directory item with the given parameters
	LBDirectoryItem(const string& resource, const string& resourcePathName, 
		            LBDirectoryItemTypes m_ResourceType,
					const string& resourceFilter, 
					const string& resourceFileName, 
					const string& resourceExtension);

	LBDirectoryItem(LBDirectoryItem* other);

	// copy constructor
	LBDirectoryItem(const LBDirectoryItem& other);

	// ************************************************************** //
	// Assignement                                                    //
	// ************************************************************** //
	LBDirectoryItem& operator = (const LBDirectoryItem& other);

	// ************************************************************** //
	// Member variables getters                                       //
	// ************************************************************** //
	
	// GetResource()
	// returns the resource of this directory item
	string GetResource() const;

	// GetResourcePathName()
	// returns the resource path name of this directory item
	string GetResourcePathName() const;

	// GetResourceType()
	// returns the resource type of this directory item
	LBDirectoryItemTypes GetResourceType() const;

	// GetResourceFilter()
	// returns the resource filter of this directory item
	string GetResourceFilter() const;

	// GetResourceFileName()
	// returns the resource file name of this directory item
	string GetResourceFileName() const;

	// GetResourceExtension()
	// returns the resource extension of this directory item
	string GetResourceExtension() const;

	// ************************************************************** //
	// Member variables setters                                       //
	// ************************************************************** //
	
	// SetResource()
	// sets the resource of this directory item with the given one
	void SetResource(const string& newResource);

	// SetResource()
	// sets the resource of this directory item with the given one
	void SetResource(const CString& newResource);

	// SetResourcePathName()
	// sets the resource path name of this directory item with the given one
	void SetResourcePathName(const string& newResourcePathName);

	// SetResourcePathName()
	// sets the resource path name of this directory item with the given one
	void SetResourcePathName(const CString& newResourcePathName);

	// SetResourceType()
	// sets the resource type of this directory item
	void SetResourceType(LBDirectoryItemTypes newResourceType);

	// SetResourceFilter()
	// sets the resource filter of this directory item with the given one
	void SetResourceFilter(const string& newResourceFilter);

	// SetResourceFilter()
	// sets the resource filter of this directory item with the given one
	void SetResourceFilter(const CString& newResourceFilter);

	// SetResourceFileName()
	// sets the resource file name of this directory item with the given one
	void SetResourceFileName(const string& newResourceFileName);

	// SetResourceFileName()
	// sets the resource file name of this directory item with the given one
	void SetResourceFileName(const CString& newResourceFileName);

	// SetResourceExtension()
	// sets the resource extension of this directory item with the given one
	void SetResourceExtension(const string& newResourceExtension);

	// SetResourceExtension()
	// sets the resource extension of this directory item with the given one
	void SetResourceExtension(const CString& newResourceExtension);

	// ************************************************************** //
	// Interface                                                      //
	// ************************************************************** //
	
	// Validate()
	// returns the result from dem validation
	// LBET_NOERROR if the dem can be exported to LandBuilder
	LBErrorTypes Validate();

	// ************************************************************** //
	// Serialization                                                  //
	// ************************************************************** //
	virtual void Serialize(CArchive& ar);
};