// CEditParameterDlg.cpp : implementation file
//

#include "stdafx.h"
#include ".\LBGlobalVariables.h"
#include ".\VisualLandBuilder.h"
#include ".\CEditParameterDlg.h"

#include "..\..\LandBuilder\HighMapLoaders\include\EtHelperFunctions.h"

// CEditParameterDlg dialog

IMPLEMENT_DYNAMIC(CEditParameterDlg, CDialog)

// -------------------------------------------------------------------------- //

CEditParameterDlg::CEditParameterDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEditParameterDlg::IDD, pParent)
	, m_Modified(false)
{
}

// -------------------------------------------------------------------------- //

CEditParameterDlg::~CEditParameterDlg()
{
}

// -------------------------------------------------------------------------- //

void CEditParameterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_NAME, m_NameCtrl);
	DDX_Control(pDX, IDC_VALUE, m_ValueCtrl);
}

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CEditParameterDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CEditParameterDlg::OnBnClickedOk)
	ON_EN_CHANGE(IDC_VALUE, &CEditParameterDlg::OnEnChangeValue)
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //

BOOL CEditParameterDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();

	// sets the name control
	m_NameCtrl.SetWindowTextA(m_Parameter.GetName().c_str());

	// sets the value control
	m_ValueCtrl.SetWindowTextA(m_Parameter.GetValue().c_str());

	return TRUE;
}

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

void CEditParameterDlg::SetParameter(LBParameter* parameter)
{
	// sets the working copy of the data
	m_Parameter = *parameter;

	// sets the pointer to the original data
	m_pOldParameter = parameter;
}

// -------------------------------------------------------------------------- //

bool CEditParameterDlg::Validate()
{
	CString data;
	m_ValueCtrl.GetWindowTextA(data);

	switch(m_Parameter.GetType())
	{
	case LBPT_INTEGER:
		{
			if(!IsInteger(string(data), false))
			{
				AfxGetMainWnd()->MessageBox(string(MSG_MUSTBEINTEGER).c_str(), string(MSG_ERROR).c_str(), MB_OK | MB_ICONSTOP);
				return false;
			}
			else
			{
				return true;
			}
		}
	case LBPT_FLOAT:
		{
			if(!IsFloatingPoint(string(data), false))
			{
				AfxGetMainWnd()->MessageBox(string(MSG_MUSTBEFLOATING).c_str(), string(MSG_ERROR).c_str(), MB_OK | MB_ICONSTOP);
				return false;
			}
			else
			{
				return true;
			}
		}
	case LBPT_BOOL:
		{
			if(!IsBool(string(data), false))
			{
				AfxGetMainWnd()->MessageBox(string(MSG_MUSTBEZEROORONE).c_str(), string(MSG_ERROR).c_str(), MB_OK | MB_ICONSTOP);
				return false;
			}
			else
			{
				return true;
			}
		}
	case LBPT_STRING: return true;
	default:
		AfxGetMainWnd()->MessageBox(string(MSG_DATANOTRECOGNIZED).c_str(), string(MSG_ERROR).c_str(), MB_OK | MB_ICONSTOP);
		return false;
	}
}

// -------------------------------------------------------------------------- //
// EDIT HANDLERS                                                              //
// -------------------------------------------------------------------------- //

void CEditParameterDlg::OnEnChangeValue()
{
	m_Modified = true;
}

// -------------------------------------------------------------------------- //
// BUTTONS HANDLERS                                                           //
// -------------------------------------------------------------------------- //

void CEditParameterDlg::OnBnClickedOk()
{
	// update data if modified
	if(m_Modified)
	{
		if(Validate())
		{
			// gets data from dialog
			CString data;
			m_ValueCtrl.GetWindowTextA(data);
			m_Parameter.SetValue(data);

			// updates data
			*m_pOldParameter = m_Parameter;
	
			OnOK();
		}
		else
		{
			m_ValueCtrl.SetFocus();
		}
	}
	else
	{
		OnOK();
	}
}
