//-----------------------------------------------------------------------------

#include "stdafx.h"

#include ".\LBModule.h"
#include ".\LBHelperFunctions.h"

#include "..\..\LandBuilder\plugins\GenericPlacers\AdvancedOnPlacePlacer.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\AdvancedRandomPlacer.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\AdvancedRandomPlacerSlope.h"
#include "..\..\LandBuilder\plugins\GenericPlacers\BestFitPlacer.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\BiasedRandomPlacer.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\BiasedRandomPlacerSlope.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\BoundarySubSquaresFramePlacer.h"
//#include "..\..\LandBuilder\plugins\BuildingPlacers\DummyBuildingPlacer.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\FramedForestPlacer.h"
#include "..\..\LandBuilder\plugins\Transformations\GeoProjection.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\InnerSubSquaresFramePlacer.h"
#include "..\..\LandBuilder\plugins\GenericPlacers\OnPlacePlacer.h"
#include "..\..\LandBuilder\plugins\BuildingPlacers\NYDummyBuildingPlacer.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\RandomOnPathPlacer.h"
#include "..\..\LandBuilder\plugins\GenericPlacers\RegularOnPathPlacer.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\SimpleRandomPlacer.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\SimpleRandomPlacerSlope.h"
#include "..\..\LandBuilder\plugins\RandomPlacers\SubSquaresRandomPlacer.h"
#include "..\..\LandBuilder\plugins\Transformations\Transform2D.h"
#include "..\..\LandBuilder\Shapes\ShapeHelpers.h"

#include ".\LBDbValuesFunctor.h"
#include ".\LBRectangle.h"

//-----------------------------------------------------------------------------

// AdvancedOnPlacePlacer typedefs
typedef LandBuildInt::Modules::AdvancedOnPlacePlacer::ShapeInput AOPP_ShapeInput;

// AdvancedRandomPlacer typedefs
typedef LandBuildInt::Modules::AdvancedRandomPlacer::ShapeInput  ARP_ShapeInput;
typedef LandBuildInt::Modules::AdvancedRandomPlacer::ObjectInput ARP_ObjectInput;
typedef AutoArray< ARP_ObjectInput >                             ARP_ObjectsIn;

// AdvancedRandomPlacerSlope typedefs
typedef LandBuildInt::Modules::AdvancedRandomPlacerSlope::ShapeInput  ARPS_ShapeInput;
typedef LandBuildInt::MapInput                                        ARPS_MapInput;
typedef LandBuildInt::Modules::AdvancedRandomPlacerSlope::ObjectInput ARPS_ObjectInput;
typedef AutoArray< ARPS_ObjectInput >                                 ARPS_ObjectsIn;

// BestFitPlacer typedefs
typedef LandBuildInt::Modules::BestFitPlacer::ShapeInput  BFP_ShapeInput;
typedef LandBuildInt::Modules::BestFitPlacer::ObjectInput BFP_ObjectInput;
typedef AutoArray< BFP_ObjectInput >                      BFP_ObjectsIn;

// BiasedRandomPlacer typedefs
typedef LandBuildInt::Modules::BiasedRandomPlacer::ShapeInput  BRP_ShapeInput;
typedef LandBuildInt::Modules::BiasedRandomPlacer::ObjectInput BRP_ObjectInput;
typedef AutoArray< BRP_ObjectInput >                           BRP_ObjectsIn;

// BiasedRandomPlacerSlope typedefs
typedef LandBuildInt::Modules::BiasedRandomPlacerSlope::ShapeInput  BRPS_ShapeInput;
typedef LandBuildInt::MapInput                                      BRPS_MapInput;
typedef LandBuildInt::Modules::BiasedRandomPlacerSlope::ObjectInput BRPS_ObjectInput;
typedef AutoArray< BRPS_ObjectInput >                               BRPS_ObjectsIn;

// BoundarySubSquaresFramePlacer typedefs
typedef LandBuildInt::Modules::BoundarySubSquaresFramePlacer::ShapeInput  BSSFP_ShapeInput;
typedef LandBuildInt::Modules::BoundarySubSquaresFramePlacer::ObjectInput BSSFP_ObjectInput;
typedef AutoArray< BSSFP_ObjectInput >                                    BSSFP_ObjectsIn;

//// DummyBuildingPlacer typedefs
//typedef LandBuildInt::Modules::DummyBuildingPlacer::GlobalInput DBP_GlobalInput;
//typedef LandBuildInt::Modules::DummyBuildingPlacer::DbfInput    DBP_DbfInput;

// FramedForestPlacer typedefs
typedef LandBuildInt::Modules::FramedForestPlacer::ShapeInput  FFP_ShapeInput;
typedef LandBuildInt::Modules::FramedForestPlacer::ObjectInput FFP_ObjectInput;
typedef AutoArray< FFP_ObjectInput >                           FFP_ObjectsIn;

//// GeoProjection typedefs
//typedef LandBuildInt::Modules::GeoProjection::GlobalInput GP_GlobalInput;

// InnerSubSquaresFramePlacer typedefs
typedef LandBuildInt::Modules::InnerSubSquaresFramePlacer::ShapeInput  ISSFP_ShapeInput;
typedef LandBuildInt::Modules::InnerSubSquaresFramePlacer::ObjectInput ISSFP_ObjectInput;
typedef AutoArray< ISSFP_ObjectInput >                                 ISSFP_ObjectsIn;

// OnPlacePlacer typedefs
typedef LandBuildInt::Modules::OnPlacePlacer::ShapeInput OPP_ShapeInput;

// NYDummyBuildingPlacer typedefs
typedef LandBuildInt::Modules::NYDummyBuildingPlacer::ShapeInput NYDBP_ShapeInput;

// RandomOnPathPlacer typedefs
typedef LandBuildInt::Modules::RandomOnPathPlacer::ShapeInput  ROPP_ShapeInput;
typedef LandBuildInt::Modules::RandomOnPathPlacer::ObjectInput ROPP_ObjectInput;
typedef AutoArray< ROPP_ObjectInput >                          ROPP_ObjectsIn;

// RegularOnPathPlacer typedefs
typedef LandBuildInt::Modules::RegularOnPathPlacer::ShapeInput  ReOPP_ShapeInput;
typedef LandBuildInt::Modules::RegularOnPathPlacer::ObjectInput ReOPP_ObjectInput;
typedef AutoArray< ReOPP_ObjectInput >                          ReOPP_ObjectsIn;

// SimpleRandomPlacer typedefs
typedef LandBuildInt::Modules::SimpleRandomPlacer::ShapeInput  SRP_ShapeInput;
typedef LandBuildInt::Modules::SimpleRandomPlacer::ObjectInput SRP_ObjectInput;

// SimpleRandomPlacerSlope typedefs
typedef LandBuildInt::Modules::SimpleRandomPlacerSlope::ShapeInput  SRPS_ShapeInput;
typedef LandBuildInt::MapInput                                      SRPS_MapInput;
typedef LandBuildInt::Modules::SimpleRandomPlacerSlope::ObjectInput SRPS_ObjectInput;

// SubSquaresRandomPlacer typedefs
typedef LandBuildInt::Modules::SubSquaresRandomPlacer::ShapeInput  SSRP_ShapeInput;
typedef LandBuildInt::Modules::SubSquaresRandomPlacer::ObjectInput SSRP_ObjectInput;
typedef AutoArray< SSRP_ObjectInput >                              SSRP_ObjectsIn;

// Transform2D typedefs
typedef LandBuildInt::Modules::Transform2D::ShapeInput T2D_ShapeInput;

//-----------------------------------------------------------------------------

IMPLEMENT_SERIAL( LBModule, CObject, 1 )

//-----------------------------------------------------------------------------

LBModule::LBModule() 
: CObject()
, m_version( VERSION_NUMBER )
, m_active( true )
, m_drawObjects( NULL )
{
}

//-----------------------------------------------------------------------------

LBModule::LBModule( const string& lbName, const string& vlbName,
                    int lbCompatibility,
                    LBModuleTypes type, const string& description, 
                    LBModuleUseObjectTypes useObjects, 
                    const LBParameterList& objParametersList,
                    const LBParameterList& globalParametersList,
                    const LBParameterList& hiddenGlobalParametersList,
                    const LBParameterList& dbfParametersList,
                    bool useShapefile, DWORD allowedShapeTypes, bool acceptMultipartShapes,
                    const string& shapeFileName,
                    LBModuleUseDemTypes useDems, bool exportsTerrain, bool hasPreview,
                    bool hasExtension, const string& lbExtensionName, const string& notes ) 
: CObject()
, m_version( VERSION_NUMBER )
, m_active( true )
, m_lbName( lbName )
, m_vlbName( vlbName )
, m_landBuilderCompatibility( lbCompatibility )
, m_type( type )
, m_description( description )
, m_useObjects( useObjects )
, m_useShapefile( useShapefile )
, m_allowedShapeTypes( allowedShapeTypes )
, m_acceptMultipartShapes( acceptMultipartShapes )
, m_shapeFileName( shapeFileName )
, m_useDems( useDems )
, m_exportsTerrain( exportsTerrain )
, m_objParametersList( objParametersList )
, m_globalParametersList( globalParametersList )
, m_hiddenGlobalParametersList( hiddenGlobalParametersList )
, m_dbfParametersList( dbfParametersList )
, m_drawObjects( NULL )
, m_hasPreview( hasPreview )
, m_hasExtension( hasExtension )
, m_lbExtensionName( lbExtensionName )
, m_notes( notes )
{
}

//-----------------------------------------------------------------------------

LBModule::LBModule( LBModule* other ) 
: m_version( other->m_version )
, m_active( other->m_active )
, m_lbName( other->m_lbName )
, m_landBuilderCompatibility( other->m_landBuilderCompatibility )
, m_vlbName( other->m_vlbName )
, m_type( other->m_type )
, m_description( other->m_description )
, m_useObjects( other->m_useObjects )
, m_useShapefile( other->m_useShapefile )
, m_allowedShapeTypes( other->m_allowedShapeTypes )
, m_acceptMultipartShapes( other->m_acceptMultipartShapes )
, m_shapeFileName( other->m_shapeFileName )
, m_useDems( other->m_useDems )
, m_exportsTerrain( other->m_exportsTerrain )
, m_objParametersList( other->m_objParametersList )
, m_globalParametersList( other->m_globalParametersList )
, m_hiddenGlobalParametersList( other->m_hiddenGlobalParametersList )
, m_dbfParametersList( other->m_dbfParametersList )
, m_objectsList( other->m_objectsList )
, m_demsList( other->m_demsList )
, m_drawObjects( other->m_drawObjects )
, m_hasPreview( other->m_hasPreview )
, m_hasExtension( other->m_hasExtension )
, m_lbExtensionName( other->m_lbExtensionName )
, m_notes( other->m_notes )
{
}

//-----------------------------------------------------------------------------

LBModule::LBModule( const LBModule& other ) 
: m_version( other.m_version )
, m_active( other.m_active )
, m_lbName( other.m_lbName )
, m_landBuilderCompatibility( other.m_landBuilderCompatibility )
, m_vlbName( other.m_vlbName )
, m_type( other.m_type )
, m_description( other.m_description )
, m_useObjects( other.m_useObjects )
, m_useShapefile( other.m_useShapefile )
, m_allowedShapeTypes( other.m_allowedShapeTypes )
, m_acceptMultipartShapes( other.m_acceptMultipartShapes )
, m_shapeFileName( other.m_shapeFileName )
, m_useDems( other.m_useDems )
, m_exportsTerrain( other.m_exportsTerrain )
, m_objParametersList( other.m_objParametersList )
, m_globalParametersList( other.m_globalParametersList )
, m_hiddenGlobalParametersList( other.m_hiddenGlobalParametersList )
, m_dbfParametersList( other.m_dbfParametersList )
, m_objectsList( other.m_objectsList )
, m_demsList( other.m_demsList )
, m_drawObjects( other.m_drawObjects )
, m_hasPreview( other.m_hasPreview )
, m_hasExtension( other.m_hasExtension )
, m_lbExtensionName( other.m_lbExtensionName )
, m_notes( other.m_notes )
{
}

//-----------------------------------------------------------------------------

LBModule::~LBModule()
{
}

//-----------------------------------------------------------------------------

LBModule& LBModule::operator = ( const LBModule& other )
{
  m_version                    = other.m_version;
  m_active                     = other.m_active;
  m_lbName                     = other.m_lbName;
  m_vlbName                    = other.m_vlbName;
  m_landBuilderCompatibility   = other.m_landBuilderCompatibility;
  m_type                       = other.m_type;
  m_description                = other.m_description;
  m_useObjects                 = other.m_useObjects;
  m_useShapefile               = other.m_useShapefile;
  m_allowedShapeTypes          = other.m_allowedShapeTypes;
  m_acceptMultipartShapes      = other.m_acceptMultipartShapes;
  m_shapeFileName              = other.m_shapeFileName;
  m_useDems                    = other.m_useDems;
  m_exportsTerrain             = other.m_exportsTerrain;
  m_objParametersList          = other.m_objParametersList;
  m_globalParametersList       = other.m_globalParametersList;
  m_hiddenGlobalParametersList = other.m_hiddenGlobalParametersList;
  m_dbfParametersList          = other.m_dbfParametersList;
  m_objectsList                = other.m_objectsList;
  m_demsList                   = other.m_demsList;
  m_drawObjects                = other.m_drawObjects;
  m_hasPreview                 = other.m_hasPreview;
  m_hasExtension               = other.m_hasExtension;
  m_lbExtensionName            = other.m_lbExtensionName;
  m_notes                      = other.m_notes;

  return (*this);
}

//-----------------------------------------------------------------------------

const string& LBModule::GetLBName() const
{
  return (m_lbName);
}

//-----------------------------------------------------------------------------

const string& LBModule::GetVLBName() const
{
  return (m_vlbName);
}

//-----------------------------------------------------------------------------

LBModuleTypes LBModule::GetType() const
{
  return (m_type);
}

//-----------------------------------------------------------------------------

string LBModule::GetDescription() const
{
  return (m_description);
}

//-----------------------------------------------------------------------------

LBModuleUseObjectTypes LBModule::GetUseObjects() const
{
  return (m_useObjects);
}

//-----------------------------------------------------------------------------

bool LBModule::GetUseShapefile() const
{
  return (m_useShapefile);
}

//-----------------------------------------------------------------------------

DWORD LBModule::GetAllowedShapeTypes() const
{
  return (m_allowedShapeTypes);
}

//-----------------------------------------------------------------------------

bool LBModule::GetAcceptMultipartShapes() const
{
  return (m_acceptMultipartShapes);
}

//-----------------------------------------------------------------------------

const string& LBModule::GetShapeFileName() const
{
  return (m_shapeFileName);
}

//-----------------------------------------------------------------------------

LBParameterList& LBModule::GetObjParametersList() 
{
  return (m_objParametersList);
}

//-----------------------------------------------------------------------------

LBObjectList& LBModule::GetObjectsList()
{
  return (m_objectsList);
}

//-----------------------------------------------------------------------------

LBParameterList& LBModule::GetGlobalParametersList() 
{
  return (m_globalParametersList);
}

//-----------------------------------------------------------------------------

LBParameterList& LBModule::GetHiddenGlobalParametersList() 
{
  return (m_hiddenGlobalParametersList);
}

//-----------------------------------------------------------------------------

LBParameterList& LBModule::GetDbfParametersList() 
{
  return (m_dbfParametersList);
}

//-----------------------------------------------------------------------------

LBModuleUseDemTypes LBModule::GetUseDems() const
{
  return (m_useDems);
}

//-----------------------------------------------------------------------------

bool LBModule::GetExportsTerrain() const
{
  return (m_exportsTerrain);
}

//-----------------------------------------------------------------------------

LBDemList& LBModule::GetDemsList()
{
  return (m_demsList);
}

//-----------------------------------------------------------------------------

bool LBModule::IsActive() const
{
  return (m_active);
}

//-----------------------------------------------------------------------------

bool LBModule::GetHasPreview() const
{
  return (m_hasPreview);
}

//-----------------------------------------------------------------------------

bool LBModule::GetHasExtension() const
{
  return (m_hasExtension);
}

//-----------------------------------------------------------------------------

const string& LBModule::GetLBExtensionName() const
{
  return (m_lbExtensionName);
}

//-----------------------------------------------------------------------------

const string& LBModule::GetNotes() const
{
  return (m_notes);
}

//-----------------------------------------------------------------------------

void LBModule::SetLBName( const string& lbName )
{
  m_lbName = lbName;
}

//-----------------------------------------------------------------------------

void LBModule::SetVLBName( const string& vlbName )
{
  m_vlbName = vlbName;
}

//-----------------------------------------------------------------------------

void LBModule::SetType( LBModuleTypes type )
{
  m_type = type;
}

//-----------------------------------------------------------------------------

void LBModule::SetDescription( const string& description )
{
  m_description = description;
}

//-----------------------------------------------------------------------------

void LBModule::SetUseObjects( LBModuleUseObjectTypes useObjects )
{
  m_useObjects = useObjects;
}

//-----------------------------------------------------------------------------

void LBModule::SetUseShapefile( bool useShapefile )
{
  m_useShapefile = useShapefile;
}

//-----------------------------------------------------------------------------

void LBModule::GetAllowedShapeTypes( DWORD allowedShapeTypes )
{
  m_allowedShapeTypes = allowedShapeTypes;
}

//-----------------------------------------------------------------------------

void LBModule::SetShapeFileName( const string& shapeFileName )
{
  m_shapeFileName = shapeFileName;
}

//-----------------------------------------------------------------------------

void LBModule::SetAcceptMultipartShapes( bool acceptMultipartShapes )
{
  m_acceptMultipartShapes = acceptMultipartShapes;
}

//-----------------------------------------------------------------------------

void LBModule::SetShapeFileName( const CString& shapeFileName )
{
  m_shapeFileName = shapeFileName.GetString();
}

//-----------------------------------------------------------------------------

void LBModule::SetObjParametersList( const LBParameterList& objParametersList )
{
  m_objParametersList = objParametersList;
}

//-----------------------------------------------------------------------------

void LBModule::SetObjectsList( const LBObjectList& objectsList )
{
  m_objectsList = objectsList;
}

//-----------------------------------------------------------------------------

void LBModule::SetGlobalParametersList( const LBParameterList& globalParametersList )
{
  m_globalParametersList = globalParametersList;
}

//-----------------------------------------------------------------------------

void LBModule::SetHiddenGlobalParametersList( const LBParameterList& hiddenGlobalParametersList )
{
  m_hiddenGlobalParametersList = hiddenGlobalParametersList;
}

//-----------------------------------------------------------------------------

void LBModule::SetDbfParametersList( const LBParameterList& dbfParametersList )
{
  m_dbfParametersList = dbfParametersList;
}

//-----------------------------------------------------------------------------

void LBModule::SetUseDems( LBModuleUseDemTypes useDems )
{
  m_useDems = useDems;
}

//-----------------------------------------------------------------------------

void LBModule::SetExportsTerrain( bool exportsTerrain )
{
  m_exportsTerrain = exportsTerrain;
}

//-----------------------------------------------------------------------------

void LBModule::SetDemsList( const LBDemList& demsList )
{
  m_demsList = demsList;
}

//-----------------------------------------------------------------------------

void LBModule::SetActive( bool active )
{
  m_active = active;
}

//-----------------------------------------------------------------------------

void LBModule::SetHasPreview( bool hasPreview )
{
  m_hasPreview = hasPreview;
}

//-----------------------------------------------------------------------------

void LBModule::SetHasExtension( bool hasExtension )
{
  m_hasExtension = hasExtension;
}

//-----------------------------------------------------------------------------

void LBModule::SetLBExtensionName( const string& lbExtensionName )
{
  m_lbExtensionName = lbExtensionName;
}

//-----------------------------------------------------------------------------

void LBModule::SetNotes( const string& notes )
{
  m_notes = notes;
}

//-----------------------------------------------------------------------------

void LBModule::AddObject( LBObject* object )
{
  m_objectsList.Append( object );
}

//-----------------------------------------------------------------------------

void LBModule::SetObject( int index, LBObject* object )
{
  m_objectsList.SetAt( index, object );
}

//-----------------------------------------------------------------------------

void LBModule::RemoveObject( int index )
{
  m_objectsList.RemoveAt( index );
}

//-----------------------------------------------------------------------------

void LBModule::RemoveAllObjects()
{
  m_objectsList.RemoveAll();
}

//-----------------------------------------------------------------------------

void LBModule::SetGlobalParameter( int index, LBParameter* parameter )
{
  m_globalParametersList.SetAt( index, parameter );
}

//-----------------------------------------------------------------------------

void LBModule::RemoveAllGlobalParameters()
{
  m_globalParametersList.RemoveAll();
}

//-----------------------------------------------------------------------------

void LBModule::ClearAllGlobalParameters()
{
  m_globalParametersList.Clear();
}

//-----------------------------------------------------------------------------

void LBModule::SetHiddenGlobalParameter( int index, LBParameter* parameter )
{
  m_hiddenGlobalParametersList.SetAt( index, parameter );
}

//-----------------------------------------------------------------------------

void LBModule::RemoveAllHiddenGlobalParameters()
{
  m_hiddenGlobalParametersList.RemoveAll();
}

//-----------------------------------------------------------------------------

void LBModule::ClearAllHiddenGlobalParameters()
{
  m_hiddenGlobalParametersList.Clear();
}

//-----------------------------------------------------------------------------

void LBModule::SetDbfParameter( int index, LBParameter* parameter )
{
  m_dbfParametersList.SetAt( index, parameter );
}

//-----------------------------------------------------------------------------

void LBModule::RemoveAllDbfParameters()
{
  m_dbfParametersList.RemoveAll();
}

//-----------------------------------------------------------------------------

void LBModule::ClearAllDbfParameters()
{
  m_dbfParametersList.Clear();
}

//-----------------------------------------------------------------------------

void LBModule::AddDem( LBDem* dem )
{
  m_demsList.Append( dem );
}

//-----------------------------------------------------------------------------

void LBModule::SetDem(int index, LBDem* dem )
{
  m_demsList.SetAt( index, dem );
}

//-----------------------------------------------------------------------------

void LBModule::RemoveDem( int index )
{
  m_demsList.RemoveAt( index );
}

//-----------------------------------------------------------------------------

void LBModule::RemoveAllDems()
{
  m_demsList.RemoveAll();
}

//-----------------------------------------------------------------------------

LBErrorTypes LBModule::Validate( LBDirectoryList& directoryList )
{
  // if shapefile searches for file
  if ( m_useShapefile )
  {
    // checks if shapefile file exists
    if ( GetFileAttributes( m_shapeFileName.c_str() ) == INVALID_FILE_ATTRIBUTES )
    {
      return (LBET_SHAPEFILEFILENOTFOUND);
    }

    if ( m_dbfParametersList.GetCount() )
    {
      // checks if dbf file exists
      string dbfFile = m_shapeFileName.substr( 0, m_shapeFileName.size() - 3 ) + "dbf";
      if ( GetFileAttributes( dbfFile.c_str() ) == INVALID_FILE_ATTRIBUTES )
      {
        return (LBET_DBFFILENOTFOUND);
      }
    }
  }

  // if dem searches for file
  if ( m_useDems != LBMUD_NONE )
  {
    LBErrorTypes res = m_demsList.Validate();
    if ( res != LBET_NOERROR ) { return (res); }
  }

  // if objects verifies
  if ( m_useObjects != LBMUO_NONE )
  {
    LBErrorTypes res = m_objectsList.Validate();
    if ( res != LBET_NOERROR ) { return (res); }
  }

  // verifies global parameters
  if ( !m_globalParametersList.Validate() ) { return (LBET_MISSINGGLOBALPARAMETER); }

  // verifies dbf parameters
  if ( !m_dbfParametersList.Validate() ) { return (LBET_MISSINGDBFPARAMETER); }

  return (LBET_NOERROR);
}

//-----------------------------------------------------------------------------

bool LBModule::SaveTaskConfig( const string& fileName, const string& workingFolder )
{
  string cfgFileName   = workingFolder + fileName + ".cfg";
  string cfgFileNameEx = fileName + "_ext.shp";

  CStdioFile f;

  if ( !f.Open( cfgFileName.c_str(), CFile::modeCreate | CFile::modeWrite ) )
  {
    return (false);
  }

  string line = "class TaskConfig\n";
  f.WriteString( line.c_str() );
  line = "{\n";
  f.WriteString( line.c_str() );
  line = "  module = \"" + m_lbName + "\";\n";
  f.WriteString( line.c_str() );
  line = "  class Parameters\n";
  f.WriteString( line.c_str() );
  line = "  {\n";
  f.WriteString( line.c_str() );

  INT_PTR objCount = static_cast< int >( m_objectsList.GetCount() );
  if ( objCount != 0 )
  {
    for ( int i = 0; i < objCount; ++i )
    {
      LBObject* item = m_objectsList.GetAt( i );

      if ( item->GetName() != OBJ_NAME_FROMDBF )
      {
        if ( m_useObjects == LBMUO_MULTIPLE )
        {
          char buf[81];
          sprintf_s(buf, 80, "    object%d", (i + 1));
          line = string(buf);
        }
        else
        {
          line = "    object";
        }
        line += (" = \"" + item->GetName() + "\";\n");
        f.WriteString( line.c_str() );

        INT_PTR objParCount = item->GetParametersList().GetCount();
        for ( int j = 0; j < objParCount; ++j )
        {
          LBParameter* parItem = item->GetParametersList().GetAt( j );
          if ( parItem->GetValue() != "" )
          {
            line = ("    " + parItem->GetName());
            if ( m_useObjects == LBMUO_MULTIPLE )
            {
              char buf[81];
              sprintf_s( buf, 80, "%d", (i + 1) );
              line += string( buf );
            }
            line += (" = \"" + parItem->GetValue() + "\";\n");
            f.WriteString( line.c_str() );
          }
        }
      }
      else
      {
        INT_PTR objParCount = item->GetParametersList().GetCount();
        int objectIndex = -1;
        if ( objParCount > 0 ) 
        {
          LBParameter* parItem = item->GetParametersList().GetAt( 0 );
          string strValue = parItem->GetValue();
          objectIndex = atoi( strValue.c_str() );
        }

        if ( m_useObjects == LBMUO_MULTIPLE )
        {
          char buf[81];
          sprintf_s( buf, 80, "    object%d = \"object%d\";\n", (i + 1), objectIndex );
          line = string( buf );
        }
        else
        {
          line = "    object = \"object\";\n";
        }
        f.WriteString( line.c_str() );
        
        for ( int j = 0; j < objParCount; ++j )
        {
          LBParameter* parItem = item->GetParametersList().GetAt( j );
          if ( parItem->GetValue() != "" )
          {
            line = ("    " + parItem->GetName());
            if ( m_useObjects == LBMUO_MULTIPLE )
            {
              char buf[81];
              sprintf_s( buf, 80, "%d", (i + 1) );
              line += string( buf );
              line += " = \"" + parItem->GetName();
              sprintf_s( buf, 80, "%d", objectIndex );
              line += string( buf );
              line += "\";\n";
            }
            else
            {
              line += (" = \"" + parItem->GetName() + "\";\n");
            }
            f.WriteString( line.c_str() );
          }
        }
      }
    }
  }

  INT_PTR demCount = m_demsList.GetCount();
  if ( demCount != 0 )
  {
    if ( m_useDems == LBMUD_MULTIPLE )
    {
      for ( int i = 0; i < demCount; ++i )
      {
        LBDem* item = m_demsList.GetAt( i );
        char buf[81];
        sprintf_s( buf, 80, "    demType%d", (i + 1) );
        line = string( buf );
        line += (" = \"" + item->GetTypeAsString() + "\";\n");
        f.WriteString( line.c_str() );
        sprintf_s( buf, 80, "    filename%d", (i + 1) );
        line = string( buf );
        line += (" = \"" + workingFolder + GetFileNameFromPath( item->GetFileName() ) + "\";\n");
        f.WriteString( line.c_str() );

        INT_PTR demParamCount = item->GetParametersList().GetCount();
        for ( int j = 0; j < demParamCount; ++j )
        {
          LBParameter* par = item->GetParametersList().GetAt( j );
          if ( par->GetValue() != "" )
          {
            line = ("    " + par->GetName());
            char buf[81];
            sprintf_s( buf, 80, "%d", (i + 1) );
            line += string( buf );
            line += (" = \"" + par->GetValue() + "\";\n");
            f.WriteString( line.c_str() );
          }				
        }
      }
    }
    else
    {
      LBDem* item = m_demsList.GetAt( 0 );
      line = "    demType = \"" + item->GetTypeAsString() + "\";\n";
      f.WriteString( line.c_str() );
      line = "    filename = \"" + workingFolder + GetFileNameFromPath( item->GetFileName() ) + "\";\n";
      f.WriteString( line.c_str() );

      INT_PTR demParamCount = item->GetParametersList().GetCount();
      for ( int j = 0; j < demParamCount; ++j )
      {
        LBParameter* par = item->GetParametersList().GetAt( j );
        if ( par->GetValue() != "" )
        {
          line = ("    " + par->GetName()) + " = \"" + par->GetValue() + "\";\n";
          f.WriteString( line.c_str() );
        }				
      }
    }
  }

  INT_PTR gloParamCount = m_globalParametersList.GetCount();
  for ( int i = 0; i < gloParamCount; ++i )
  {
    LBParameter* item = m_globalParametersList.GetAt( i );
    if ( !item->IsExtension() )
    {
      if ( item->GetValue() != "" )
      {
        line = "    " + item->GetName() + " = \"" + item->GetValue() + "\";\n";
        f.WriteString( line.c_str() );
      }
    }
  }

  INT_PTR hiddenGloParamCount = m_hiddenGlobalParametersList.GetCount();
  for ( int i = 0; i < hiddenGloParamCount; ++i )
  {
    LBParameter* item = m_hiddenGlobalParametersList.GetAt( i );
    if ( !item->IsExtension() )
    {
      if ( item->GetValue() != "" )
      {
        line = "    " + item->GetName() + " = \"" + item->GetValue() + "\";\n";
        f.WriteString( line.c_str() );
      }
    }
  }

  if ( GetHasExtension() )
  {
    line = "    OutShapefile = \"" + cfgFileNameEx + "\";\n";
    f.WriteString( line.c_str() );
  }

  line = "  };\n";
  f.WriteString( line.c_str() );

  line = "  columnTransl[] =\n";
  f.WriteString( line.c_str() );
  line = "  {\n";
  f.WriteString( line.c_str() );

  INT_PTR dbfParamCount = m_dbfParametersList.GetCount();
  if ( dbfParamCount != 0 )
  {
    // first detects last parameters who has a valid value
    // this will be printed without comma
    int lastValid = -1;
    for ( int i = 0; i < dbfParamCount; ++i )
    {
      LBParameter* item = m_dbfParametersList.GetAt( i );
      if ( !item->IsExtension() )
      {
        if ( item->GetValue() != "" ) { lastValid = i; }
      }
    }

    for ( int i = 0; i < dbfParamCount; ++i )
    {
      LBParameter* item = m_dbfParametersList.GetAt( i );
      if ( !item->IsExtension() )
      {
        if ( item->GetValue() != "" )
        {
          line = "    \"" + item->GetValue() + "->" + item->GetName() + "\"";
          if ( i != lastValid ) { line += ","; }
          line += "\n";
          f.WriteString( line.c_str() );
        }
      }
    }
  }

  line = "  };\n";
  f.WriteString( line.c_str() );
  line = "};\n";
  f.WriteString( line.c_str() );

  f.Close();
  return (true);
}

//-----------------------------------------------------------------------------

bool LBModule::SaveExtensionTaskConfig( const string& fileName2, const string& workingFolder )
{
  string cfgFileNameEx = (workingFolder + fileName2 + "_ext.cfg");

  CStdioFile f;

  if ( !f.Open( cfgFileNameEx.c_str(), CFile::modeCreate | CFile::modeWrite ) )
  {
    return (false);
  }

  string line = "class TaskConfig\n";
  f.WriteString( line.c_str());
  line = "{\n";
  f.WriteString( line.c_str() );
  line = "  module = \"" + m_lbExtensionName + "\";\n";
  f.WriteString( line.c_str() );
  line = "  class Parameters\n";
  f.WriteString( line.c_str() );
  line = "  {\n";
  f.WriteString( line.c_str() );

  INT_PTR gloParamCount = m_globalParametersList.GetCount();
  for ( int i = 0; i < gloParamCount; ++i )
  {
    LBParameter* item = m_globalParametersList.GetAt( i );
    if ( item->IsExtension() )
    {
      if ( item->GetValue() != "" )
      {
        line = "    " + item->GetName() + " = \"" + item->GetValue() + "\";\n";
        f.WriteString( line.c_str() );
      }
    }
  }

  line = "  };\n";
  f.WriteString( line.c_str() );

  line = "  columnTransl[] =\n";
  f.WriteString( line.c_str() );
  line = "  {\n";
  f.WriteString( line.c_str() );

  INT_PTR dbfParamCount = m_dbfParametersList.GetCount();
  if ( dbfParamCount != 0 )
  {
    // first detects last parameters who has a valid value
    // this will be printed without comma
    int lastValid = -1;
    for ( int i = 0; i < dbfParamCount; ++i )
    {
      LBParameter* item = m_dbfParametersList.GetAt( i );
      if ( item->IsExtension() )
      {
        if ( item->GetValue() != "" ) { lastValid = i; }
      }
    }

    for ( int i = 0; i < dbfParamCount; ++i )
    {
      LBParameter* item = m_dbfParametersList.GetAt( i );
      if ( item->IsExtension() )
      {
        if ( item->GetValue() != "" )
        {
          line = "    \"" + item->GetValue() + "->" + item->GetName() + "\"";
          if ( i != lastValid ) { line += ","; }
          line += "\n";
          f.WriteString( line.c_str() );
        }
      }
    }
  }

  line = "  };\n";
  f.WriteString( line.c_str() );
  line = "};\n";
  f.WriteString( line.c_str() );

  f.Close();
  return (true);
}

//-----------------------------------------------------------------------------

void LBModule::Preview( Shapes::ShapeList& shapeList, Shapes::ShapeDatabase& shDB, CPaintDC* dc, CRect& rect, 
                        DrawParams drawParams, bool calculate )
{
  if ( m_vlbName == MODULE_VLBNAME_NONE ) { return; }

  if ( m_drawObjects.Size() == 0 || calculate ) { CreatePreview( shapeList, shDB ); }

  CPen*   oldPen   = dc->GetCurrentPen();
  CBrush* oldBrush = dc->GetCurrentBrush();

  size_t objCount = m_drawObjects.Size();
  for ( size_t i = 0; i < objCount; ++i )
  {
    int x = static_cast< int >( drawParams.picMedX + (m_drawObjects[i].position.x - drawParams.viewMedX) / drawParams.ratio );
    int y = static_cast< int >( drawParams.picMedY + (drawParams.viewMedY - m_drawObjects[i].position.y) / drawParams.ratio );

    LBRectangle rRect( rect.left + 1, rect.bottom - 1, rect.right - 1, rect.top + 1 );

    if ( rRect.Contains( x, y ) )
    {		
      double dimX;
      double dimY;
      CBrush fillBrush;
      CPen   borderPen;
      if ( m_objectsList.GetCount() > 0 )
      {
        fillBrush.CreateSolidBrush( PREV_COLORS[m_objectsList.GetAt( m_drawObjects[i].type )->GetPvFillColor()] );
        borderPen.CreatePen( PS_SOLID, 1, PREV_COLORS[m_objectsList.GetAt( m_drawObjects[i].type )->GetPvBorderColor()] );
        dimX = m_drawObjects[i].length * m_drawObjects[i].scaleX;
        dimY = m_drawObjects[i].width  * m_drawObjects[i].scaleY;
      }
      else
      {
        fillBrush.CreateSolidBrush( PREV_COLORS[COLOR_LIGHTGREY] );
        borderPen.CreatePen( PS_SOLID, 1, PREV_COLORS[COLOR_GREY] );
        dimX = m_drawObjects[i].length * m_drawObjects[i].scaleX;
        dimY = m_drawObjects[i].width  * m_drawObjects[i].scaleY;
      }

      dc->SelectObject( &borderPen );
      dc->SelectObject( &fillBrush );

      double angle = m_drawObjects[i].rotation;
      double x0    = m_drawObjects[i].position.x;
      double y0    = m_drawObjects[i].position.y;

      Shapes::DVertex v[4];

      v[0].x = x0 + (-dimX * 0.5) * EtMathD::Cos( angle ) - (dimY * 0.5)  * EtMathD::Sin( angle );
      v[0].y = y0 + (-dimX * 0.5) * EtMathD::Sin( angle ) + (dimY * 0.5)  * EtMathD::Cos( angle );
      v[1].x = x0 + (-dimX * 0.5) * EtMathD::Cos( angle ) - (-dimY * 0.5) * EtMathD::Sin( angle );
      v[1].y = y0 + (-dimX * 0.5) * EtMathD::Sin( angle ) + (-dimY * 0.5) * EtMathD::Cos( angle );
      v[2].x = x0 + (dimX * 0.5)  * EtMathD::Cos( angle ) - (-dimY * 0.5) * EtMathD::Sin( angle );
      v[2].y = y0 + (dimX * 0.5)  * EtMathD::Sin( angle ) + (-dimY * 0.5) * EtMathD::Cos( angle );
      v[3].x = x0 + (dimX * 0.5)  * EtMathD::Cos( angle ) - (dimY * 0.5)  * EtMathD::Sin( angle );
      v[3].y = y0 + (dimX * 0.5)  * EtMathD::Sin( angle ) + (dimY * 0.5)  * EtMathD::Cos( angle );

      CPoint pts[4];
      bool inside = true;
      for ( int j = 0; j < 4; ++j )
      {
        pts[j].x = static_cast< int >( drawParams.picMedX + (v[j].x - drawParams.viewMedX) / drawParams.ratio );
        pts[j].y = static_cast< int >( drawParams.picMedY + (drawParams.viewMedY - v[j].y) / drawParams.ratio );
        if ( !rRect.Contains( pts[j].x, pts[j].y ) )
        {
          inside = false;
          break;
        }
      }

      if ( inside ) { dc->Polygon( pts, 4 ); }
    }
  }

  dc->SelectObject( oldPen );
  dc->SelectObject( oldBrush );
}

//-----------------------------------------------------------------------------

bool LBModule::IsAllowedShapeList( Shapes::ShapeList& shapeList )
{
  if ( shapeList.Size() == 0 ) { return (false); }

  Shapes::IShape* shape = shapeList.GetShape( 0 );
  RString type = GetShapeType( shape );

  DWORD flag;
  if ( type == SHAPE_NAME_POINT )      { flag = AST_POINT; }
  if ( type == SHAPE_NAME_MULTIPOINT ) { flag = AST_MULTIPOINT; }
  if ( type == SHAPE_NAME_POLYLINE )   { flag = AST_POLYLINE; }
  if ( type == SHAPE_NAME_POLYGON )    { flag = AST_POLYGON; }

  return ((m_allowedShapeTypes & flag) == flag);
}

//-----------------------------------------------------------------------------

void LBModule::CreatePreview( Shapes::ShapeList& shapeList, Shapes::ShapeDatabase& shDB )
{
  m_drawObjects.Clear();

  if ( m_vlbName == MODULE_VLBNAME_ADVANCEDONPLACEPLACER )
  {
    // sets shape parameters
    AOPP_ShapeInput shapeIn;

    // gets dbf parameters values
    LBDbValuesFunctor functor;
    shDB.ForEachField( functor );

    int shapesCount = shapeList.Size();
    for ( int i = 0; i < shapesCount; ++i )
    {
      // object
      string value = m_globalParametersList.GetValue( PARNAME_OBJECT );
      if ( value != "" )
      {
        shapeIn.m_name = value.c_str();
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_OBJECT );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_name = dValue.c_str();
        }
      }

      // dirCorrection
      shapeIn.m_dirCorrection = 0.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_DIRCORRECTION );
      if ( value != "" )
      {
        shapeIn.m_dirCorrection = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_DIRCORRECTION );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_dirCorrection = atof( dValue.c_str() );
        }
      }

      Shapes::IShape* shape = shapeList.GetShape( i );
      LandBuildInt::Modules::AdvancedOnPlacePlacer aopp;
      ModuleObjectOutputArray* objects = aopp.RunAsPreview( shape, shapeIn );
      m_drawObjects.Append( *objects );
    }

    return;
  }
  else if ( m_vlbName == MODULE_VLBNAME_ADVANCEDRANDOMPLACER )
  {
    // sets shape parameters
    ARP_ShapeInput shapeIn;
    // sets objects parameters
    ARP_ObjectsIn vlbObjectsIn;

    // gets objects whose data have been inputted in VLB
    INT_PTR objCount = m_objectsList.GetCount();
    for ( int i = 0; i < objCount; ++i )
    {
      LBObject* object = m_objectsList.GetAt( i );
      if ( object->GetName() != OBJ_NAME_FROMDBF )
      {
        ARP_ObjectInput obj = object->AdvancedRandomPlacerObjectInput();
        vlbObjectsIn.Add( obj );
      }
    }

    // gets dbf parameters values
    LBDbValuesFunctor functor;
    shDB.ForEachField( functor );

    int shapesCount = shapeList.Size();
    for ( int i = 0; i < shapesCount; ++i )
    {
      ARP_ObjectsIn dbfObjectsIn;
      ARP_ObjectsIn objectsIn;

      // gets objects whose data are in the dbf
      INT_PTR objCount = m_objectsList.GetCount();
      for ( int j = 0; j < objCount; ++j )
      {
        LBObject* object = m_objectsList.GetAt( j );
        if ( object->GetName() == OBJ_NAME_FROMDBF )
        {
          ARP_ObjectInput obj = object->AdvancedRandomPlacerObjectInput( i, functor );
          dbfObjectsIn.Add( obj );
        }
      }
      
      // merges all objects
      int vlbObjectsInCount = vlbObjectsIn.Size();
      for ( int j = 0; j < vlbObjectsInCount; ++j )
      {
        objectsIn.Add( vlbObjectsIn[j] );
      }
      int dbfObjectsInCount = dbfObjectsIn.Size();
      for ( int j = 0; j < dbfObjectsInCount; ++j )
      {
        objectsIn.Add( dbfObjectsIn[j] );
      }

      // randomSeed
      string value = m_globalParametersList.GetValue( PARNAME_RANDOMSEED );
      if ( value != "" )
      {
        shapeIn.m_randomSeed = static_cast< size_t >( atoi( value.c_str() ) );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_RANDOMSEED );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_randomSeed = static_cast< size_t >( atoi( dValue.c_str() ) );
        }
      }

      // hectareDensity
      shapeIn.m_hectareDensity = 100.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_HECTAREDENSITY );
      if ( value != "" )
      {
        shapeIn.m_hectareDensity = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_HECTAREDENSITY );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_hectareDensity = atof( dValue.c_str() );
        }
      }

      LandBuildInt::Modules::AdvancedRandomPlacer arp;
      Shapes::IShape* shape = shapeList.GetShape( i );			
      ModuleObjectOutputArray* objects = arp.RunAsPreview( shape, shapeIn, objectsIn );
      m_drawObjects.Append( *objects );
    }

    return;
  }
  else if ( m_vlbName == MODULE_VLBNAME_ADVANCEDRANDOMPLACERSLOPE )
  {
    // sets shape parameters
    ARPS_ShapeInput shapeIn;
    // sets objects parameters
    ARPS_ObjectsIn vlbObjectsIn;

    // gets objects whose data have been inputted in VLB
    INT_PTR objCount = m_objectsList.GetCount();
    for ( int i = 0; i < objCount; ++i )
    {
      LBObject* object = m_objectsList.GetAt( i );
      if ( object->GetName() != OBJ_NAME_FROMDBF )
      {
        ARPS_ObjectInput obj = object->AdvancedRandomPlacerSlopeObjectInput();
        vlbObjectsIn.Add( obj );
      }
    }

    // sets maps parameters
    ARPS_MapInput mapIn = m_demsList.GetAt( 0 )->AdvancedRandomPlacerSlopeMapInput();

    // gets dbf parameters values
    LBDbValuesFunctor functor;
    shDB.ForEachField( functor );

    int shapesCount = shapeList.Size();
    for ( int i = 0; i < shapesCount; ++i )
    {
      ARPS_ObjectsIn dbfObjectsIn;
      ARPS_ObjectsIn objectsIn;

      // gets objects whose data are in the dbf
      INT_PTR objCount = m_objectsList.GetCount();
      for ( int j = 0; j < objCount; ++j )
      {
        LBObject* object = m_objectsList.GetAt( j );
        if ( object->GetName() == OBJ_NAME_FROMDBF )
        {
          ARPS_ObjectInput obj = object->AdvancedRandomPlacerSlopeObjectInput( i, functor );
          dbfObjectsIn.Add( obj );
        }
      }

      // merges all objects
      int vlbObjectsInCount = vlbObjectsIn.Size();
      for ( int j = 0; j < vlbObjectsInCount; ++j )
      {
        objectsIn.Add( vlbObjectsIn[j] );
      }
      int dbfObjectsInCount = dbfObjectsIn.Size();
      for ( int j = 0; j < dbfObjectsInCount; ++j )
      {
        objectsIn.Add( dbfObjectsIn[j] );
      }

      // randomSeed
      string value = m_globalParametersList.GetValue( PARNAME_RANDOMSEED );
      if ( value != "" )
      {
        shapeIn.m_randomSeed = static_cast< size_t >( atoi( value.c_str() ) );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_RANDOMSEED );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_randomSeed = static_cast< size_t >( atoi( dValue.c_str() ) );
        }
      }

      // hectareDensity
      shapeIn.m_hectareDensity = 100.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_HECTAREDENSITY );
      if ( value != "" )
      {
        shapeIn.m_hectareDensity = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_HECTAREDENSITY );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_hectareDensity = atof( dValue.c_str() );
        }
      }

      LandBuildInt::Modules::AdvancedRandomPlacerSlope arps;
      Shapes::IShape* shape = shapeList.GetShape( i );			
      ModuleObjectOutputArray* objects = arps.RunAsPreview( shape, shapeIn, mapIn, objectsIn );
      m_drawObjects.Append( *objects );
    }

    return;
  }
  else if ( m_vlbName == MODULE_LBNAME_BESTFITPLACER )
  {
    // sets shape parameters
    BFP_ShapeInput shapeIn;
    // sets objects parameters
    BFP_ObjectsIn vlbObjectsIn;

    // gets objects whose data have been inputted in VLB
    INT_PTR objCount = m_objectsList.GetCount();
    for ( int i = 0; i < objCount; ++i )
    {
      LBObject* object = m_objectsList.GetAt( i );
      if ( object->GetName() != OBJ_NAME_FROMDBF )
      {
        BFP_ObjectInput obj = object->BestFitPlacerObjectInput();
        vlbObjectsIn.Add( obj );
      }
    }

    // gets dbf parameters values
    LBDbValuesFunctor functor;
    shDB.ForEachField( functor );

    int shapesCount = shapeList.Size();
    for ( int i = 0; i < shapesCount; ++i )
    {
      BFP_ObjectsIn dbfObjectsIn;
      BFP_ObjectsIn objectsIn;

      // gets objects whose data are in the dbf
      INT_PTR objCount = m_objectsList.GetCount();
      for ( int j = 0; j < objCount; ++j )
      {
        LBObject* object = m_objectsList.GetAt( j );
        if ( object->GetName() == OBJ_NAME_FROMDBF )
        {
          BFP_ObjectInput obj = object->BestFitPlacerObjectInput( i, functor );
          dbfObjectsIn.Add( obj );
        }
      }

      // merges all objects
      int vlbObjectsInCount = vlbObjectsIn.Size();
      for ( int j = 0; j < vlbObjectsInCount; ++j )
      {
        objectsIn.Add( vlbObjectsIn[j] );
      }
      int dbfObjectsInCount = dbfObjectsIn.Size();
      for ( int j = 0; j < dbfObjectsInCount; ++j )
      {
        objectsIn.Add( dbfObjectsIn[j] );
      }

      // dirCorrection
      shapeIn.m_dirCorrection = 0.0; // default value
      string value = m_globalParametersList.GetValue( PARNAME_DIRCORRECTION );
      if ( value != "" )
      {
        shapeIn.m_dirCorrection = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_DIRCORRECTION );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_dirCorrection = atof( dValue.c_str() );
        }
      }

      // desiredHeight
      value = m_globalParametersList.GetValue( PARNAME_DESIREDHEIGHT );
      if ( value != "" )
      {
        shapeIn.m_desiredHeight = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_DESIREDHEIGHT );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_desiredHeight = atof( dValue.c_str() );
        }
      }

      Shapes::IShape* shape = shapeList.GetShape( i );			
      LandBuildInt::Modules::BestFitPlacer bfp;
      ModuleObjectOutputArray* objects = bfp.RunAsPreview( shape, shapeIn, objectsIn );
      m_drawObjects.Append( *objects );
    }

    return;
  }
  else if ( m_vlbName == MODULE_VLBNAME_BIASEDRANDOMPLACER )
  {
    // sets shape parameters
    BRP_ShapeInput shapeIn;
    // sets objects parameters
    BRP_ObjectsIn vlbObjectsIn;

    // gets objects whose data have been inputted in VLB
    INT_PTR objCount = m_objectsList.GetCount();
    for ( int i = 0; i < objCount; ++i )
    {
      LBObject* object = m_objectsList.GetAt( i );
      if ( object->GetName() != OBJ_NAME_FROMDBF )
      {
        BRP_ObjectInput obj = object->BiasedRandomPlacerObjectInput();
        vlbObjectsIn.Add( obj );
      }
    }

    // gets dbf parameters values
    LBDbValuesFunctor functor;
    shDB.ForEachField( functor );

    int shapesCount = shapeList.Size();
    for ( int i = 0; i < shapesCount; ++i )
    {
      BRP_ObjectsIn dbfObjectsIn;
      BRP_ObjectsIn objectsIn;

      // gets objects whose data are in the dbf
      INT_PTR objCount = m_objectsList.GetCount();
      for ( int j = 0; j < objCount; ++j )
      {
        LBObject* object = m_objectsList.GetAt( j );
        if ( object->GetName() == OBJ_NAME_FROMDBF )
        {
          BRP_ObjectInput obj = object->BiasedRandomPlacerObjectInput( i, functor );
          dbfObjectsIn.Add( obj );
        }
      }

      // merges all objects
      int vlbObjectsInCount = vlbObjectsIn.Size();
      for ( int j = 0; j < vlbObjectsInCount; ++j )
      {
        objectsIn.Add( vlbObjectsIn[j] );
      }
      int dbfObjectsInCount = dbfObjectsIn.Size();
      for ( int j = 0; j < dbfObjectsInCount; ++j )
      {
        objectsIn.Add( dbfObjectsIn[j] );
      }

      // smallAtBoundary
      shapeIn.m_smallAtBoundary = true; // default value
      string value = m_globalParametersList.GetValue( PARNAME_SMALLATBOUNDARY );
      if ( value != "" )
      {
        int iValue = static_cast< int >( atoi( value.c_str() ) );
        if ( iValue == 0 )
        {
          shapeIn.m_smallAtBoundary = false;
        }
        else
        {
          shapeIn.m_smallAtBoundary = true;
        }
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_SMALLATBOUNDARY );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          int iValue = static_cast< int >( atoi( dValue.c_str() ) );
          if ( iValue == 0 )
          {
            shapeIn.m_smallAtBoundary = false;
          }
          else
          {
            shapeIn.m_smallAtBoundary = true;
          }
        }
      }

      // randomSeed
      value = m_globalParametersList.GetValue( PARNAME_RANDOMSEED );
      if ( value != "" )
      {
        shapeIn.m_randomSeed = static_cast< size_t >( atoi( value.c_str() ) );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_RANDOMSEED );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_randomSeed = static_cast< size_t >( atoi( dValue.c_str() ) );
        }
      }

      // hectareDensity
      shapeIn.m_hectareDensity = 100.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_HECTAREDENSITY );
      if ( value != "" )
      {
        shapeIn.m_hectareDensity = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_HECTAREDENSITY );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_hectareDensity = atof( dValue.c_str() );
        }
      }

      // aggregationCoeff
      shapeIn.m_clusterCoeff = 1.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_HECTAREDENSITY );
      if ( value != "" )
      {
        shapeIn.m_hectareDensity = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_AGGREGATIONCOEFF );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_clusterCoeff = atof( dValue.c_str() );
        }
      }

      // parentCount
      shapeIn.m_parentCount = 1; // default value
      value = m_globalParametersList.GetValue( PARNAME_PARENTCOUNT );
      if ( value != "" )
      {
        shapeIn.m_parentCount = atoi( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_PARENTCOUNT );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_parentCount = atoi( dValue.c_str() );
        }
      }

      Shapes::IShape* shape = shapeList.GetShape( i );			
      LandBuildInt::Modules::BiasedRandomPlacer brp;
      ModuleObjectOutputArray* objects = brp.RunAsPreview( shape, shapeIn, objectsIn );
      m_drawObjects.Append( *objects );
    }

    return;
  }
  else if ( m_vlbName == MODULE_VLBNAME_BIASEDRANDOMPLACERSLOPE )
  {
    // sets shape parameters
    BRPS_ShapeInput shapeIn;
    // sets objects parameters
    BRPS_ObjectsIn vlbObjectsIn;

    // gets objects whose data have been inputted in VLB
    INT_PTR objCount = m_objectsList.GetCount();
    for ( int i = 0; i < objCount; ++i )
    {
      LBObject* object = m_objectsList.GetAt( i );
      if ( object->GetName() != OBJ_NAME_FROMDBF )
      {
        BRPS_ObjectInput obj = object->BiasedRandomPlacerSlopeObjectInput();
        vlbObjectsIn.Add( obj );
      }
    }

    // sets maps parameters
    BRPS_MapInput mapIn = m_demsList.GetAt( 0 )->BiasedRandomPlacerSlopeMapInput();

    // gets dbf parameters values
    LBDbValuesFunctor functor;
    shDB.ForEachField( functor );

    int shapesCount = shapeList.Size();
    for ( int i = 0; i < shapesCount; ++i )
    {
      BRPS_ObjectsIn dbfObjectsIn;
      BRPS_ObjectsIn objectsIn;

      // gets objects whose data are in the dbf
      INT_PTR objCount = m_objectsList.GetCount();
      for ( int j = 0; j < objCount; ++j )
      {
        LBObject* object = m_objectsList.GetAt( j );
        if ( object->GetName() == OBJ_NAME_FROMDBF )
        {
          BRPS_ObjectInput obj = object->BiasedRandomPlacerSlopeObjectInput( i, functor );
          dbfObjectsIn.Add( obj );
        }
      }

      // merges all objects
      int vlbObjectsInCount = vlbObjectsIn.Size();
      for ( int j = 0; j < vlbObjectsInCount; ++j )
      {
        objectsIn.Add( vlbObjectsIn[j] );
      }
      int dbfObjectsInCount = dbfObjectsIn.Size();
      for ( int j = 0; j < dbfObjectsInCount; ++j )
      {
        objectsIn.Add( dbfObjectsIn[j] );
      }

      // smallAtBoundary
      shapeIn.m_smallAtBoundary = true; // default value
      string value = m_globalParametersList.GetValue( PARNAME_SMALLATBOUNDARY );
      if ( value != "" )
      {
        int iValue = static_cast< int >( atoi( value.c_str() ) );
        if ( iValue == 0 )
        {
          shapeIn.m_smallAtBoundary = false;
        }
        else
        {
          shapeIn.m_smallAtBoundary = true;
        }
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_SMALLATBOUNDARY );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          int iValue = static_cast< int >( atoi( dValue.c_str() ) );
          if ( iValue == 0 )
          {
            shapeIn.m_smallAtBoundary = false;
          }
          else
          {
            shapeIn.m_smallAtBoundary = true;
          }
        }
      }

      // randomSeed
      value = m_globalParametersList.GetValue( PARNAME_RANDOMSEED );
      if ( value != "" )
      {
        shapeIn.m_randomSeed = static_cast< size_t >( atoi( value.c_str() ) );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_RANDOMSEED );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_randomSeed = static_cast< size_t >( atoi( dValue.c_str() ) );
        }
      }

      // hectareDensity
      shapeIn.m_hectareDensity = 100.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_HECTAREDENSITY );
      if ( value != "" )
      {
        shapeIn.m_hectareDensity = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_HECTAREDENSITY );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_hectareDensity = atof( dValue.c_str() );
        }
      }

      // aggregationCoeff
      shapeIn.m_clusterCoeff = 1.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_AGGREGATIONCOEFF );
      if ( value != "" )
      {
        shapeIn.m_clusterCoeff = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_AGGREGATIONCOEFF );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_clusterCoeff = atof( dValue.c_str() );
        }
      }

      // parentCount
      shapeIn.m_parentCount = 1; // default value
      value = m_globalParametersList.GetValue( PARNAME_PARENTCOUNT );
      if ( value != "" )
      {
        shapeIn.m_parentCount = atoi( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_PARENTCOUNT );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_parentCount = atoi( dValue.c_str() );
        }
      }

      Shapes::IShape* shape = shapeList.GetShape( i );			
      LandBuildInt::Modules::BiasedRandomPlacerSlope brps;
      ModuleObjectOutputArray* objects = brps.RunAsPreview( shape, shapeIn, mapIn, objectsIn );
      m_drawObjects.Append( *objects );
    }

    return;
  }
  else if ( m_vlbName == MODULE_VLBNAME_BOUNDARYSUBSQUARESFRAMEPLACER )
  {
    // sets shape parameters
    BSSFP_ShapeInput shapeIn;
    // sets objects parameters
    BSSFP_ObjectsIn vlbObjectsIn;

    // gets objects whose data have been inputted in VLB
    INT_PTR objCount = m_objectsList.GetCount();
    for ( int i = 0; i < objCount; ++i )
    {
      LBObject* object = m_objectsList.GetAt( i );
      if ( object->GetName() != OBJ_NAME_FROMDBF )
      {
        BSSFP_ObjectInput obj = object->BoundarySubSquaresFramePlacerObjectInput();
        vlbObjectsIn.Add( obj );
      }
    }

    // gets dbf parameters values
    LBDbValuesFunctor functor;
    shDB.ForEachField( functor );

    int shapesCount = shapeList.Size();
    for ( int i = 0; i < shapesCount; ++i )
    {
      BSSFP_ObjectsIn dbfObjectsIn;
      BSSFP_ObjectsIn objectsIn;

      // gets objects whose data are in the dbf
      INT_PTR objCount = m_objectsList.GetCount();
      for ( int j = 0; j < objCount; ++j )
      {
        LBObject* object = m_objectsList.GetAt( j );
        if ( object->GetName() == OBJ_NAME_FROMDBF )
        {
          BSSFP_ObjectInput obj = object->BoundarySubSquaresFramePlacerObjectInput( i, functor );
          dbfObjectsIn.Add( obj );
        }
      }
      
      // merges all objects
      int vlbObjectsInCount = vlbObjectsIn.Size();
      for ( int j = 0; j < vlbObjectsInCount; ++j )
      {
        objectsIn.Add( vlbObjectsIn[j] );
      }
      int dbfObjectsInCount = dbfObjectsIn.Size();
      for ( int j = 0; j < dbfObjectsInCount; ++j )
      {
        objectsIn.Add( dbfObjectsIn[j] );
      }

      // randomSeed
      string value = m_globalParametersList.GetValue( PARNAME_RANDOMSEED );
      if ( value != "" )
      {
        shapeIn.m_randomSeed = static_cast< size_t >( atoi( value.c_str() ) );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_RANDOMSEED );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_randomSeed = static_cast< size_t >( atoi( dValue.c_str() ) );
        }
      }

      // subSquaresSize
      shapeIn.m_subSquaresSize = 10.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_SUBSQUARESSIZE );
      if ( value != "" )
      {
        shapeIn.m_subSquaresSize = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_SUBSQUARESSIZE );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_subSquaresSize = atof( dValue.c_str() );
        }
      }

      // maxNoise
      shapeIn.m_maxNoise = 5.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_MAXNOISE );
      if ( value != "" )
      {
        shapeIn.m_maxNoise = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_MAXNOISE );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_maxNoise = atof( dValue.c_str() );
        }
      }

      // mindist
      shapeIn.m_minDistance = 0.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_MINDISTANCE );
      if ( value != "" )
      {
        shapeIn.m_minDistance = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_MINDISTANCE );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_minDistance = atof( dValue.c_str() );
        }
      }

      LandBuildInt::Modules::BoundarySubSquaresFramePlacer bssfp;
      Shapes::IShape* shape = shapeList.GetShape( i );			
      ModuleObjectOutputArray* objects = bssfp.RunAsPreview( shape, shapeIn, objectsIn );
      m_drawObjects.Append( *objects );
    }

    return;
  }
//  else if ( m_vlbName == MODULE_VLBNAME_DUMMYBUILDINGPLACER )
//  {
//    // sets global parameters
//    DBP_GlobalInput globalIn;
//    // sets dbf parameters
//    DBP_DbfInput dbfIn;
//
//    // gets dbf parameters values
//    LBDbValuesFunctor functor;
//    shDB.ForEachField( functor );
//
//    int shapesCount = shapeList.Size();
//    for ( int i = 0; i < shapesCount; ++i )
//    {
//      string value = m_globalParametersList.GetValue( PARNAME_MODELCIVIL );
//      if ( value != "" )
//      {
//        globalIn.m_modelCivil = RString( value.c_str() );
//      }
//      else
//      {
//        string field = m_dbfParametersList.GetValue( PARNAME_MODELCIVIL );
//        if ( field != "" )
//        {
//          string dValue = functor.GetValue( i, field );
//          globalIn.m_modelCivil = RString( dValue.c_str() );
//        }
//      }
//
//      value = m_globalParametersList.GetValue( PARNAME_MODELINDUSTRIAL );
//      if ( value != "" )
//      {
//        globalIn.m_modelIndustrial = RString( value.c_str() );
//      }
//      else
//      {
//        string field = m_dbfParametersList.GetValue( PARNAME_MODELINDUSTRIAL );
//        if ( field != "" )
//        {
//          string dValue = functor.GetValue( i, field );
//          globalIn.m_modelIndustrial = RString( dValue.c_str() );
//        }
//      }
//
//      value = m_globalParametersList.GetValue( PARNAME_MODELMILITARY );
//      if ( value != "" )
//      {
//        globalIn.m_modelMilitary = RString( value.c_str() );
//      }
//      else
//      {
//        string field = m_dbfParametersList.GetValue( PARNAME_MODELMILITARY );
//        if ( field != "" )
//        {
//          string dValue = functor.GetValue( i, field );
//          globalIn.m_modelMilitary = RString( dValue.c_str() );
//        }
//      }
//
//      value = m_globalParametersList.GetValue( PARNAME_MAXNUMFLOORSCIVIL );
//      if ( value != "" )
//      {
//        globalIn.m_maxNumFloorsCivil = atoi( value.c_str() );
//      }
//      else
//      {
//        string field = m_dbfParametersList.GetValue( PARNAME_MAXNUMFLOORSCIVIL );
//        if ( field != "" )
//        {
//          string dValue = functor.GetValue( i, field );
//          globalIn.m_maxNumFloorsCivil = atoi( dValue.c_str() );
//        }
//      }
//
//      value = m_globalParametersList.GetValue( PARNAME_MAXNUMFLOORSINDUSTRIAL );
//      if ( value != "" )
//      {
//        globalIn.m_maxNumFloorsIndustrial = atoi( value.c_str() );
//      }
//      else
//      {
//        string field = m_dbfParametersList.GetValue( PARNAME_MAXNUMFLOORSINDUSTRIAL );
//        if ( field != "" )
//        {
//          string dValue = functor.GetValue( i, field );
//          globalIn.m_maxNumFloorsIndustrial = atoi( dValue.c_str() );
//        }
//      }
//
//      value = m_globalParametersList.GetValue( PARNAME_MAXNUMFLOORSMILITARY );
//      if ( value != "" )
//      {
//        globalIn.m_maxNumFloorsMilitary = atoi( value.c_str() );
//      }
//      else
//      {
//        string field = m_dbfParametersList.GetValue( PARNAME_MAXNUMFLOORSMILITARY );
//        if ( field != "" )
//        {
//          string dValue = functor.GetValue( i, field );
//          globalIn.m_maxNumFloorsMilitary = atoi( dValue.c_str() );
//        }
//      }
//
//      value = m_globalParametersList.GetValue( PARNAME_HEIGHTFLOORSCIVIL );
//      if ( value != "" )
//      {
//        globalIn.m_heightFloorsCivil = atof( value.c_str() );
//      }
//      else
//      {
//        string field = m_dbfParametersList.GetValue( PARNAME_HEIGHTFLOORSCIVIL );
//        if ( field != "" )
//        {
//          string dValue = functor.GetValue( i, field );
//          globalIn.m_heightFloorsCivil = atof( dValue.c_str() );
//        }
//      }
//
//      value = m_globalParametersList.GetValue( PARNAME_HEIGHTFLOORSINDUSTRIAL );
//      if ( value != "" )
//      {
//        globalIn.m_heightFloorsIndustrial = atof( value.c_str() );
//      }
//      else
//      {
//        string field = m_dbfParametersList.GetValue( PARNAME_HEIGHTFLOORSINDUSTRIAL );
//        if ( field != "" )
//        {
//          string dValue = functor.GetValue( i, field );
//          globalIn.m_heightFloorsIndustrial = atof( dValue.c_str() );
//        }
//      }
//
//      value = m_globalParametersList.GetValue( PARNAME_HEIGHTFLOORSMILITARY );
//      if ( value != "" )
//      {
//        globalIn.m_heightFloorsMilitary = atof( value.c_str() );
//      }
//      else
//      {
//        string field = m_dbfParametersList.GetValue( PARNAME_HEIGHTFLOORSMILITARY );
//        if ( field != "" )
//        {
//          string dValue = functor.GetValue( i, field );
//          globalIn.m_heightFloorsMilitary = atof( dValue.c_str() );
//        }
//      }
//
//      value = m_globalParametersList.GetValue( PARNAME_BUILDINGTYPE );
//      if ( value != "" )
//      {
//        dbfIn.m_buildingType = (LandBuildInt::Modules::DummyBuildingPlacer::BuildingType)atoi( value.c_str() );
//      }
//      else
//      {
//        string field = m_dbfParametersList.GetValue( PARNAME_BUILDINGTYPE );
//        if ( field != "" )
//        {
//          string dValue = functor.GetValue( i, field );
//          dbfIn.m_buildingType = (LandBuildInt::Modules::DummyBuildingPlacer::BuildingType)atoi( dValue.c_str() );
//        }
//      }
//
//      value = m_globalParametersList.GetValue( PARNAME_NUMOFFLOOR );
//      if ( value != "" )
//      {
//        dbfIn.m_numOfFloors = atoi( value.c_str() );
//      }
//      else
//      {
//        string field = m_dbfParametersList.GetValue( PARNAME_NUMOFFLOOR );
//        if ( field != "" )
//        {
//          string dValue = functor.GetValue( i, field );
//          dbfIn.m_numOfFloors = atoi( dValue.c_str() );
//        }
//      }
//
//      Shapes::IShape* shape = shapeList.GetShape( i );
//      LandBuildInt::Modules::DummyBuildingPlacer m_Dbp;
//      ModuleObjectOutputArray* objects = m_Dbp.RunAsPreview( shape, globalIn, dbfIn );
//      m_drawObjects.Append( *objects );
//    }
//
//    return;
//  }
  else if ( m_vlbName == MODULE_VLBNAME_FRAMEDFORESTPLACER )
  {
    // sets shape parameters
    FFP_ShapeInput shapeIn;
    // sets objects parameters
    FFP_ObjectsIn vlbObjectsIn;

    // gets objects whose data have been inputted in VLB
    INT_PTR objCount = m_objectsList.GetCount();
    for ( int i = 0; i < objCount; ++i )
    {
      LBObject* object = m_objectsList.GetAt( i );
      if ( object->GetName() != OBJ_NAME_FROMDBF )
      {
        FFP_ObjectInput obj = object->FramedForestPlacerObjectInput();
        vlbObjectsIn.Add( obj );
      }
    }

    // gets dbf parameters values
    LBDbValuesFunctor functor;
    shDB.ForEachField( functor );

    int shapesCount = shapeList.Size();
    for ( int i = 0; i < shapesCount; ++i )
    {
      FFP_ObjectsIn dbfObjectsIn;
      FFP_ObjectsIn objectsIn;

      // gets objects whose data are in the dbf
      INT_PTR objCount = m_objectsList.GetCount();
      for ( int j = 0; j < objCount; ++j )
      {
        LBObject* object = m_objectsList.GetAt( j );
        if ( object->GetName() == OBJ_NAME_FROMDBF )
        {
          FFP_ObjectInput obj = object->FramedForestPlacerObjectInput( i, functor );
          dbfObjectsIn.Add( obj );
        }
      }
      
      // merges all objects
      int vlbObjectsInCount = vlbObjectsIn.Size();
      for ( int j = 0; j < vlbObjectsInCount; ++j )
      {
        objectsIn.Add( vlbObjectsIn[j] );
      }
      int dbfObjectsInCount = dbfObjectsIn.Size();
      for ( int j = 0; j < dbfObjectsInCount; ++j )
      {
        objectsIn.Add( dbfObjectsIn[j] );
      }

      // randomSeed
      string value = m_globalParametersList.GetValue( PARNAME_RANDOMSEED );
      if ( value != "" )
      {
        shapeIn.m_randomSeed = static_cast< size_t >( atoi( value.c_str() ) );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_RANDOMSEED );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_randomSeed = static_cast< size_t >( atoi( dValue.c_str() ) );
        }
      }

      // mainDirection
      value = m_globalParametersList.GetValue( PARNAME_MAINDIRECTION );
      if ( value != "" )
      {
        shapeIn.m_mainDirection = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_MAINDIRECTION );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_mainDirection = atof( dValue.c_str() );
        }
      }

      // subSquaresSize
      shapeIn.m_subSquaresSize = 10.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_SUBSQUARESSIZE );
      if ( value != "" )
      {
        shapeIn.m_subSquaresSize = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_SUBSQUARESSIZE );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_subSquaresSize = atof( dValue.c_str() );
        }
      }

      // maxNoise
      shapeIn.m_maxNoise = 5.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_MAXNOISE );
      if ( value != "" )
      {
        shapeIn.m_maxNoise = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_MAXNOISE );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_maxNoise = atof( dValue.c_str() );
        }
      }

      // mindist
      shapeIn.m_minDistance = 0.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_MINDISTANCE );
      if ( value != "" )
      {
        shapeIn.m_minDistance = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_MINDISTANCE );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_minDistance = atof( dValue.c_str() );
        }
      }

      // numBoundaryObjects
      shapeIn.m_numBoundaryObjects = 1; // default value
      value = m_globalParametersList.GetValue( PARNAME_NUMBOUNDARYOBJECTS );
      if ( value != "" )
      {
        shapeIn.m_numBoundaryObjects = atoi( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_NUMBOUNDARYOBJECTS );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_numBoundaryObjects = atoi( dValue.c_str() );
        }
      }

      LandBuildInt::Modules::FramedForestPlacer ffp;
      Shapes::IShape* shape = shapeList.GetShape( i );			
      ModuleObjectOutputArray* objects = ffp.RunAsPreview( shape, shapeIn, objectsIn );
      m_drawObjects.Append( *objects );
    }
    return;
  }
  else if ( m_vlbName == MODULE_VLBNAME_INNERSUBSQUARESFRAMEPLACER )
  {
    // sets shape parameters
    ISSFP_ShapeInput shapeIn;
    // sets objects parameters
    ISSFP_ObjectsIn vlbObjectsIn;

    // gets objects whose data have been inputted in VLB
    INT_PTR objCount = m_objectsList.GetCount();
    for ( int i = 0; i < objCount; ++i )
    {
      LBObject* object = m_objectsList.GetAt( i );
      if ( object->GetName() != OBJ_NAME_FROMDBF )
      {
        ISSFP_ObjectInput obj = object->InnerSubSquaresFramePlacerObjectInput();
        vlbObjectsIn.Add( obj );
      }
    }

    // gets dbf parameters values
    LBDbValuesFunctor functor;
    shDB.ForEachField( functor );

    int shapesCount = shapeList.Size();
    for ( int i = 0; i < shapesCount; ++i )
    {
      ISSFP_ObjectsIn dbfObjectsIn;
      ISSFP_ObjectsIn objectsIn;

      // gets objects whose data are in the dbf
      INT_PTR objCount = m_objectsList.GetCount();
      for ( int j = 0; j < objCount; ++j )
      {
        LBObject* object = m_objectsList.GetAt( j );
        if ( object->GetName() == OBJ_NAME_FROMDBF )
        {
          ISSFP_ObjectInput obj = object->InnerSubSquaresFramePlacerObjectInput( i, functor );
          dbfObjectsIn.Add( obj );
        }
      }
      
      // merges all objects
      int vlbObjectsInCount = vlbObjectsIn.Size();
      for ( int j = 0; j < vlbObjectsInCount; ++j )
      {
        objectsIn.Add( vlbObjectsIn[j] );
      }
      int dbfObjectsInCount = dbfObjectsIn.Size();
      for ( int j = 0; j < dbfObjectsInCount; ++j )
      {
        objectsIn.Add( dbfObjectsIn[j] );
      }

      // randomSeed
      string value = m_globalParametersList.GetValue( PARNAME_RANDOMSEED );
      if ( value != "" )
      {
        shapeIn.m_randomSeed = static_cast< size_t >( atoi( value.c_str() ) );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_RANDOMSEED );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_randomSeed = static_cast< size_t >( atoi( dValue.c_str() ) );
        }
      }

      // subSquaresSize
      shapeIn.m_subSquaresSize = 10.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_SUBSQUARESSIZE );
      if ( value != "" )
      {
        shapeIn.m_subSquaresSize = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_SUBSQUARESSIZE );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_subSquaresSize = atof( dValue.c_str() );
        }
      }

      // maxNoise
      shapeIn.m_maxNoise = 5.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_MAXNOISE );
      if ( value != "" )
      {
        shapeIn.m_maxNoise = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_MAXNOISE );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_maxNoise = atof( dValue.c_str() );
        }
      }

      LandBuildInt::Modules::InnerSubSquaresFramePlacer issfp;
      Shapes::IShape* shape = shapeList.GetShape( i );			
      ModuleObjectOutputArray* objects = issfp.RunAsPreview( shape, shapeIn, objectsIn );
      m_drawObjects.Append( *objects );
    }
    return;
  }
  else if ( m_vlbName == MODULE_VLBNAME_NYDUMMYBUILDINGPLACER )
  {
    // sets shape parameters
    NYDBP_ShapeInput shapeIn;

    // gets dbf parameters values
    LBDbValuesFunctor functor;
    shDB.ForEachField( functor );

    int shapesCount = shapeList.Size();
    for ( int i = 0; i < shapesCount; ++i )
    {
      // randomSeed
      string value = m_globalParametersList.GetValue( PARNAME_RANDOMSEED );
      if ( value != "" )
      {
        shapeIn.m_randomSeed = static_cast< size_t >( atoi( value.c_str() ) );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_RANDOMSEED );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_randomSeed = static_cast< size_t >( atoi( dValue.c_str() ) );
        }
      }

      // buildingMinHeight
      value = m_globalParametersList.GetValue( PARNAME_BUILDINGMINHEIGHT );
      if ( value != "" )
      {
        shapeIn.m_buildingMinHeight = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_BUILDINGMINHEIGHT );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_buildingMinHeight = atof( dValue.c_str() );
        }
      }

      // buildingMaxHeight
      value = m_globalParametersList.GetValue( PARNAME_BUILDINGMAXHEIGHT );
      if ( value != "" )
      {
        shapeIn.m_buildingMaxHeight = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_BUILDINGMAXHEIGHT );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_buildingMaxHeight = atof( dValue.c_str() );
        }
      }

      // modelNameBase
      value = m_globalParametersList.GetValue( PARNAME_MODELNAMEBASE );
      if ( value != "" )
      {
        shapeIn.m_modelNameBase = RString( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_MODELNAMEBASE );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_modelNameBase = RString( dValue.c_str() );
        }
      }

      // modelVariants
      value = m_globalParametersList.GetValue( PARNAME_MODELVARIANTS );
      if ( value != "" )
      {
        shapeIn.m_modelVariants = atoi( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_MODELVARIANTS );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_modelVariants = atoi( dValue.c_str() );
        }
      }

      // mainDirection
      value = m_globalParametersList.GetValue( PARNAME_MAINDIRECTION );
      if ( value != "" )
      {
        shapeIn.m_mainDirection = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_MAINDIRECTION );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_mainDirection = atof( dValue.c_str() );
        }
      }

      // roadMinWidth
      value = m_globalParametersList.GetValue( PARNAME_ROADMINWIDTH );
      if ( value != "" )
      {
        shapeIn.m_roadMinWidth = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_ROADMINWIDTH );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_roadMinWidth = atof( dValue.c_str() );
        }
      }

      // roadMaxWidth
      value = m_globalParametersList.GetValue( PARNAME_ROADMAXWIDTH );
      if ( value != "" )
      {
        shapeIn.m_roadMaxWidth = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_ROADMAXWIDTH );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_roadMaxWidth = atof( dValue.c_str() );
        }
      }

      // blockMinSize
      value = m_globalParametersList.GetValue( PARNAME_BLOCKMINSIZE );
      if ( value != "" )
      {
        shapeIn.m_blockMinSize = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_BLOCKMINSIZE );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_blockMinSize = atof( dValue.c_str() );
        }
      }

      // blockMaxSize
      value = m_globalParametersList.GetValue( PARNAME_BLOCKMAXSIZE );
      if ( value != "" )
      {
        shapeIn.m_blockMaxSize = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_BLOCKMAXSIZE );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_blockMaxSize = atof( dValue.c_str() );
        }
      }

      // blockMinSubdivisions
      value = m_globalParametersList.GetValue( PARNAME_BLOCKMINSUBD );
      if ( value != "" )
      {
        shapeIn.m_blockMinSubdivisions = atoi( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_BLOCKMINSUBD );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_blockMinSubdivisions = atoi( dValue.c_str() );
        }
      }

      // blockMaxSubdivisions
      value = m_globalParametersList.GetValue( PARNAME_BLOCKMAXSUBD );
      if ( value != "" )
      {
        shapeIn.m_blockMaxSubdivisions = atoi( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_BLOCKMAXSUBD );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_blockMaxSubdivisions = atoi( dValue.c_str() );
        }
      }

      Shapes::IShape* shape = shapeList.GetShape( i );
      LandBuildInt::Modules::NYDummyBuildingPlacer nyDbp;
      ModuleObjectOutputArray* objects = nyDbp.RunAsPreview( shape, shapeIn );
      m_drawObjects.Append( *objects );
    }
    return;
  }
  else if ( m_vlbName == MODULE_VLBNAME_ONPLACEPLACER )
  {
    // sets shape parameters
    OPP_ShapeInput shapeIn;

    // gets dbf parameters values
    LBDbValuesFunctor functor;
    shDB.ForEachField( functor );

    int shapesCount = shapeList.Size();
    for ( int i = 0; i < shapesCount; ++i )
    {
      // object
      string value = m_globalParametersList.GetValue( PARNAME_OBJECT );
      if ( value != "" )
      {
        shapeIn.m_name = value.c_str();
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_OBJECT );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_name = dValue.c_str();
        }
      }

      // orientation
      value = m_globalParametersList.GetValue( PARNAME_ORIENTATION );
      if ( value != "" )
      {
        shapeIn.m_orientation = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_ORIENTATION );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_orientation = atof( dValue.c_str() );
        }
      }

      // scaleX
      shapeIn.m_scaleX = 1.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_SCALEX );
      if ( value != "" )
      {
        shapeIn.m_scaleX = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_SCALEX );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_scaleX = atof( dValue.c_str() );
        }
      }

      // scale Y
      shapeIn.m_scaleY = 1.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_SCALEY );
      if ( value != "" )
      {
        shapeIn.m_scaleY = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_SCALEY );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_scaleY = atof( dValue.c_str() );
        }
      }
      
      // scaleZ
      shapeIn.m_scaleZ = 1.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_SCALEZ );
      if ( value != "" )
      {
        shapeIn.m_scaleZ = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_SCALEZ );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_scaleZ = atof( dValue.c_str() );
        }
      }

      Shapes::IShape* shape = shapeList.GetShape( i );
      LandBuildInt::Modules::OnPlacePlacer opp;
      ModuleObjectOutputArray* objects = opp.RunAsPreview( shape, shapeIn );
      m_drawObjects.Append( *objects );
    }
    return;
  }
  else if ( m_vlbName == MODULE_VLBNAME_RANDOMONPATHPLACER )
  {
    // sets shape parameters
    ROPP_ShapeInput shapeIn;
    // sets objects parameters
    ROPP_ObjectsIn vlbObjectsIn;

    // gets objects whose data have been inputted in VLB
    INT_PTR objCount = m_objectsList.GetCount();
    for ( int i = 0; i < objCount; ++i )
    {
      LBObject* object = m_objectsList.GetAt( i );
      if ( object->GetName() != OBJ_NAME_FROMDBF )
      {
        ROPP_ObjectInput obj = object->RandomOnPathPlacerObjectInput();
        vlbObjectsIn.Add( obj );
      }
    }

    // gets dbf parameters values
    LBDbValuesFunctor functor;
    shDB.ForEachField( functor );

    int shapesCount = shapeList.Size();
    for ( int i = 0; i < shapesCount; ++i )
    {
      ROPP_ObjectsIn dbfObjectsIn;
      ROPP_ObjectsIn objectsIn;

      // gets objects whose data are in the dbf
      INT_PTR objCount = m_objectsList.GetCount();
      for ( int j = 0; j < objCount; ++j )
      {
        LBObject* object = m_objectsList.GetAt( j );
        if ( object->GetName() == OBJ_NAME_FROMDBF )
        {
          ROPP_ObjectInput obj = object->RandomOnPathPlacerObjectInput( i, functor );
          dbfObjectsIn.Add( obj );
        }
      }

      // merges all objects
      int vlbObjectsInCount = vlbObjectsIn.Size();
      for ( int j = 0; j < vlbObjectsInCount; ++j )
      {
        objectsIn.Add( vlbObjectsIn[j] );
      }
      int dbfObjectsInCount = dbfObjectsIn.Size();
      for ( int j = 0; j < dbfObjectsInCount; ++j )
      {
        objectsIn.Add( dbfObjectsIn[j] );
      }

      // maxDistance
      shapeIn.m_maxDistance = 10.0; // default value
      string value = m_globalParametersList.GetValue( PARNAME_MAXDISTANCE );
      if ( value != "" )
      {
        shapeIn.m_maxDistance = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_MAXDISTANCE );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_maxDistance = atof( dValue.c_str() );
        }
      }

      // randomSeed
      value = m_globalParametersList.GetValue( PARNAME_RANDOMSEED );
      if ( value != "" )
      {
        shapeIn.m_randomSeed = static_cast< size_t >( atoi( value.c_str() ) );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_RANDOMSEED);
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_randomSeed = static_cast< size_t >( atoi( dValue.c_str() ) );
        }
      }

      // linearDensity
      shapeIn.m_linearDensity = 10.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_LINEARDENSITY );
      if ( value != "" )
      {
        shapeIn.m_linearDensity = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_LINEARDENSITY );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_linearDensity = atof( dValue.c_str() );
        }
      }

      // sets shape geo parameters
      Shapes::IShape* shape = shapeList.GetShape( i );			
      LandBuildInt::Modules::RandomOnPathPlacer ropp;
      ModuleObjectOutputArray* objects = ropp.RunAsPreview( shape, shapeIn, objectsIn );
      m_drawObjects.Append( *objects );
    }
    return;
  }
  else if ( m_vlbName == MODULE_VLBNAME_REGULARONPATHPLACER )
  {
    // sets shape parameters
    ReOPP_ShapeInput shapeIn;
    // sets objects parameters
    ReOPP_ObjectsIn vlbObjectsIn;

    // gets objects whose data have been inputted in VLB
    INT_PTR objCount = m_objectsList.GetCount();
    for (int i = 0; i < objCount; ++i)
    {
      LBObject* object = m_objectsList.GetAt( i );
      if ( object->GetName() != OBJ_NAME_FROMDBF )
      {
        ReOPP_ObjectInput obj = object->RegularOnPathPlacerObjectInput();
        vlbObjectsIn.Add( obj );
      }
    }

    // gets dbf parameters values
    LBDbValuesFunctor functor;
    shDB.ForEachField( functor );

    int shapesCount = shapeList.Size();
    for ( int i = 0; i < shapesCount; ++i )
    {
      ReOPP_ObjectsIn dbfObjectsIn;
      ReOPP_ObjectsIn objectsIn;

      // gets objects whose data are in the dbf
      INT_PTR objCount = m_objectsList.GetCount();
      for ( int j = 0; j < objCount; ++j )
      {
        LBObject* object = m_objectsList.GetAt( j );
        if ( object->GetName() == OBJ_NAME_FROMDBF )
        {
          ReOPP_ObjectInput obj = object->RegularOnPathPlacerObjectInput( i, functor );
          dbfObjectsIn.Add( obj );
        }
      }

      // merges all objects
      int vlbObjectsInCount = vlbObjectsIn.Size();
      for ( int j = 0; j < vlbObjectsInCount; ++j )
      {
        objectsIn.Add( vlbObjectsIn[j] );
      }
      int dbfObjectsInCount = dbfObjectsIn.Size();
      for ( int j = 0; j < dbfObjectsInCount; ++j )
      {
        objectsIn.Add( dbfObjectsIn[j] );
      }

      // randomSeed
      string value = m_globalParametersList.GetValue( PARNAME_RANDOMSEED );
      if (value != "")
      {
        shapeIn.m_randomSeed = static_cast< size_t >( atoi( value.c_str() ) );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_RANDOMSEED );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_randomSeed = static_cast< size_t >( atoi( dValue.c_str() ) );
        }
      }

      // offset
      value = m_globalParametersList.GetValue( PARNAME_OFFSET );
      if ( value != "" )
      {
        shapeIn.m_offset = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_OFFSET );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_offset = atof( dValue.c_str() );
        }
      }

      // offsetmaxNoise
      value = m_globalParametersList.GetValue( PARNAME_OFFSETMAXNOISE );
      if ( value != "" )
      {
        shapeIn.m_offsetMaxNoise = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_OFFSETMAXNOISE );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_offsetMaxNoise = atof( dValue.c_str() );
        }
      }

      // step
      value = m_globalParametersList.GetValue( PARNAME_STEP );
      if ( value != "" )
      {
        shapeIn.m_step = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_STEP );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_step = atof( dValue.c_str() );
        }
      }

      // stepMaxNoise
      value = m_globalParametersList.GetValue( PARNAME_STEPMAXNOISE );
      if ( value != "" )
      {
        shapeIn.m_stepMaxNoise = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_STEPMAXNOISE );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_stepMaxNoise = atof( dValue.c_str() );
        }
      }

      // firstStep
      value = m_globalParametersList.GetValue( PARNAME_FIRSTSTEP );
      if ( value != "" )
      {
        shapeIn.m_firstStep = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_FIRSTSTEP );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_firstStep = atof( dValue.c_str() );
        }
      }

      // sets shape geo parameters
      Shapes::IShape* shape = shapeList.GetShape(i);			
      LandBuildInt::Modules::RegularOnPathPlacer reopp;
      ModuleObjectOutputArray* objects = reopp.RunAsPreview( shape, shapeIn, objectsIn );
      m_drawObjects.Append( *objects );
    }
    return;
  }
  else if ( m_vlbName == MODULE_VLBNAME_SIMPLERANDOMPLACER )
  {
    // sets shape parameters
    SRP_ShapeInput shapeIn;
    // sets objects parameters
    SRP_ObjectInput objectIn;

    // gets objects whose data have been inputted in VLB
    LBObject* object = m_objectsList.GetAt( 0 );
    if ( object->GetName() != OBJ_NAME_FROMDBF )
    {
      objectIn = object->SimpleRandomPlacerObjectInput();
    }

    // gets dbf parameters values
    LBDbValuesFunctor functor;
    shDB.ForEachField( functor );

    int shapesCount = shapeList.Size();
    for ( int i = 0; i < shapesCount; ++i )
    {
      // gets objects whose data are in the dbf
      if ( object->GetName() == OBJ_NAME_FROMDBF )
      {
        objectIn = object->SimpleRandomPlacerObjectInput( i, functor );
      }

      // randomSeed
      string value = m_globalParametersList.GetValue( PARNAME_RANDOMSEED );
      if ( value != "" )
      {
        shapeIn.m_randomSeed = static_cast< size_t >( atoi( value.c_str() ) );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_RANDOMSEED );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_randomSeed = static_cast< size_t >( atoi( dValue.c_str() ) );
        }
      }

      // hectareDensity
      shapeIn.m_hectareDensity = 100.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_HECTAREDENSITY );
      if ( value != "" )
      {
        shapeIn.m_hectareDensity = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_HECTAREDENSITY );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_hectareDensity = atof( dValue.c_str() );
        }
      }

      LandBuildInt::Modules::SimpleRandomPlacer srp;
      Shapes::IShape* shape = shapeList.GetShape( i );			
      ModuleObjectOutputArray* objects = srp.RunAsPreview( shape, shapeIn, objectIn );
      m_drawObjects.Append( *objects );
    }
    return;
  }
  else if ( m_vlbName == MODULE_VLBNAME_SIMPLERANDOMPLACERSLOPE )
  {
    // sets shape parameters
    SRPS_ShapeInput shapeIn;
    // sets objects parameters
    SRPS_ObjectInput objectIn;

    // gets objects whose data have been inputted in VLB
    LBObject* object = m_objectsList.GetAt( 0 );
    if ( object->GetName() != OBJ_NAME_FROMDBF )
    {
      objectIn = object->SimpleRandomPlacerSlopeObjectInput();
    }

    // sets maps parameters
    SRPS_MapInput mapIn = m_demsList.GetAt( 0 )->SimpleRandomPlacerSlopeMapInput();

    // gets dbf parameters values
    LBDbValuesFunctor functor;
    shDB.ForEachField( functor );

    int shapesCount = shapeList.Size();
    for ( int i = 0; i < shapesCount; ++i )
    {
      // gets objects whose data are in the dbf
      LBObject* object = m_objectsList.GetAt( 0 );
      if ( object->GetName() == OBJ_NAME_FROMDBF )
      {
        objectIn = object->SimpleRandomPlacerSlopeObjectInput( i, functor );
      }

      // randomSeed
      string value = m_globalParametersList.GetValue( PARNAME_RANDOMSEED );
      if ( value != "" )
      {
        shapeIn.m_randomSeed = static_cast< size_t >( atoi( value.c_str() ) );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_RANDOMSEED );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_randomSeed = static_cast< size_t >( atoi( dValue.c_str() ) );
        }
      }

      // hectareDensity
      shapeIn.m_hectareDensity = 100.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_HECTAREDENSITY );
      if ( value != "" )
      {
        shapeIn.m_hectareDensity = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_HECTAREDENSITY );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_hectareDensity = atof( dValue.c_str() );
        }
      }

    LandBuildInt::Modules::SimpleRandomPlacerSlope srps;
      Shapes::IShape* shape = shapeList.GetShape( i );			
      ModuleObjectOutputArray* objects = srps.RunAsPreview( shape, shapeIn, mapIn, objectIn );
      m_drawObjects.Append( *objects );
    }
    return;
  }
  else if( m_vlbName == MODULE_VLBNAME_SUBSQUARESRANDOMPLACER )
  {
    // sets shape parameters
    SSRP_ShapeInput shapeIn;
    // sets objects parameters
    SSRP_ObjectsIn vlbObjectsIn;

    // gets objects whose data have been inputted in VLB
    INT_PTR objCount = m_objectsList.GetCount();
    for ( int i = 0; i < objCount; ++i )
    {
      LBObject* object = m_objectsList.GetAt( i );
      if ( object->GetName() != OBJ_NAME_FROMDBF )
      {
        SSRP_ObjectInput obj = object->SubSquaresRandomPlacerObjectInput();
        vlbObjectsIn.Add( obj );
      }
    }

    // gets dbf parameters values
    LBDbValuesFunctor functor;
    shDB.ForEachField( functor );

    int shapesCount = shapeList.Size();
    for ( int i = 0; i < shapesCount; ++i )
    {
      SSRP_ObjectsIn dbfObjectsIn;
      SSRP_ObjectsIn objectsIn;

      // gets objects whose data are in the dbf
      INT_PTR objCount = m_objectsList.GetCount();
      for ( int j = 0; j < objCount; ++j )
      {
        LBObject* object = m_objectsList.GetAt( j );
        if ( object->GetName() == OBJ_NAME_FROMDBF )
        {
          SSRP_ObjectInput obj = object->SubSquaresRandomPlacerObjectInput( i, functor );
          dbfObjectsIn.Add( obj );
        }
      }
      
      // merges all objects
      int vlbObjectsInCount = vlbObjectsIn.Size();
      for ( int j = 0; j < vlbObjectsInCount; ++j )
      {
        objectsIn.Add( vlbObjectsIn[j] );
      }
      int dbfObjectsInCount = dbfObjectsIn.Size();
      for ( int j = 0; j < dbfObjectsInCount; ++j )
      {
        objectsIn.Add( dbfObjectsIn[j] );
      }

      // randomSeed
      string value = m_globalParametersList.GetValue( PARNAME_RANDOMSEED );
      if ( value != "" )
      {
        shapeIn.m_randomSeed = static_cast< size_t >( atoi( value.c_str() ) );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_RANDOMSEED );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_randomSeed = static_cast< size_t >( atoi( dValue.c_str() ) );
        }
      }

      // subSquaresSize
      shapeIn.m_subSquaresSize = 10.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_SUBSQUARESSIZE );
      if ( value != "" )
      {
        shapeIn.m_subSquaresSize = atof(value.c_str());
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_SUBSQUARESSIZE );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_subSquaresSize = atof( dValue.c_str() );
        }
      }

      // maxNoise
      shapeIn.m_maxNoise = 5.0; // default value
      value = m_globalParametersList.GetValue( PARNAME_MAXNOISE );
      if ( value != "" )
      {
        shapeIn.m_maxNoise = atof( value.c_str() );
      }
      else
      {
        string field = m_dbfParametersList.GetValue( PARNAME_MAXNOISE );
        if ( field != "" )
        {
          string dValue = functor.GetValue( i, field );
          shapeIn.m_maxNoise = atof( dValue.c_str() );
        }
      }

      LandBuildInt::Modules::SubSquaresRandomPlacer ssrp;
      Shapes::IShape* shape = shapeList.GetShape( i );			
      ModuleObjectOutputArray* objects = ssrp.RunAsPreview( shape, shapeIn, objectsIn );
      m_drawObjects.Append( *objects );
    }
    return;
  }
}

//-----------------------------------------------------------------------------

string LBModule::ToString()
{
  string output;

  if ( m_useShapefile ) 
  {
    output += ("Shapefile: " + m_shapeFileName);
  }

  if ( m_useDems != LBMUD_NONE )
  {
    if ( output.length() > 0 ) { output += " / "; }
    output += "Dem files: ";
    INT_PTR demCount = m_demsList.GetCount();
    for ( int i = 0; i < demCount; ++i )
    {
      LBDem* item = m_demsList.GetAt( i );
      if ( i > 0 ) { output += ", "; }
      output += item->GetFileName();
    }
  }

  if ( m_useObjects != LBMUO_NONE )
  {
    if ( output.size() != 0 ) { output += " / "; }
    output += "Objects: ";

    INT_PTR objCount = m_objectsList.GetCount();
    for ( int i = 0; i < objCount; ++i )
    {
      LBObject* item = m_objectsList.GetAt( i );
      if ( i > 0 ) { output += ", "; }
      output += item->GetName();
    }
  }

  return (output);
}

//-----------------------------------------------------------------------------

void LBModule::Serialize( CArchive& ar )
{
  CString lbName;
  CString vlbName;
  UINT    type;
  CString description;
  UINT    useObjects;
  UINT    useDems;
  CString shapeFileName;
  CString lbExtensionName;
  CString notes;

  CObject::Serialize( ar );

  m_objParametersList.Serialize( ar );
  m_objectsList.Serialize( ar );
  m_globalParametersList.Serialize( ar );
  m_hiddenGlobalParametersList.Serialize( ar );
  m_dbfParametersList.Serialize( ar );
  m_demsList.Serialize( ar );

  if ( ar.IsStoring() )
  {
    lbName          = CString( m_lbName.c_str() );
    vlbName         = CString( m_vlbName.c_str() );
    type            = UINT( m_type );
    description     = CString( m_description.c_str());
    useObjects      = m_useObjects;
    useDems         = m_useDems;
    shapeFileName   = CString( m_shapeFileName.c_str() );
    lbExtensionName = CString( m_lbExtensionName.c_str() );
    notes           = CString( m_notes.c_str() );
    ar << m_version;
    ar << lbName;
    ar << vlbName;
    ar << m_landBuilderCompatibility;
    ar << type;
    ar << description;
    ar << useObjects;
    ar << m_useShapefile;
    ar << m_allowedShapeTypes;
    ar << m_acceptMultipartShapes;
    ar << shapeFileName;
    ar << useDems;
    ar << m_exportsTerrain;
    ar << m_active;
    ar << m_hasPreview;
    ar << m_hasExtension;
    ar << lbExtensionName;
    ar << notes;
  }
  else
  {
    ar >> m_version;
    ar >> lbName;
    ar >> vlbName;
    ar >> m_landBuilderCompatibility;
    ar >> type;
    ar >> description;
    ar >> useObjects;
    ar >> m_useShapefile;
    ar >> m_allowedShapeTypes;
    ar >> m_acceptMultipartShapes;
    ar >> shapeFileName;
    ar >> useDems;
    ar >> m_exportsTerrain;
    ar >> m_active;
    ar >> m_hasPreview;
    ar >> m_hasExtension;
    ar >> lbExtensionName;
    ar >> notes;
    m_lbName          = lbName;
    m_vlbName         = vlbName;
    m_type            = static_cast< LBModuleTypes >( type );
    m_description     = description;
    m_useObjects      = static_cast< LBModuleUseObjectTypes >( useObjects );
    m_useDems         = static_cast< LBModuleUseDemTypes >( useDems );
    m_shapeFileName   = shapeFileName;
    m_lbExtensionName = lbExtensionName;
    m_notes           = notes;
  }
}

//-----------------------------------------------------------------------------

LBModule LBModule::GetPredefinedModule( LBPredefinedModules moduleCode )
{
  // the temp module object list of parameters
  LBParameterList objParametersList;
  // the temp module list of global parameters
  LBParameterList globalParametersList;
  // the temp module list of hidden global parameters
  LBParameterList hiddenGlobalParametersList;
  // the temp module list of dbf parameters
  LBParameterList dbfParametersList;
  
  switch ( moduleCode )
  {
  case LBPM_ADVANCED_ON_PLACE_PLACER:
    {
      dbfParametersList.Append( new LBParameter( PARNAME_OBJECT       , "", PARDESC_OBJECT       , LBPT_STRING, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_DIRCORRECTION, "", PARDESC_DIRCORRECTION, LBPT_FLOAT , true , true, false ) );
      return (LBModule( MODULE_LBNAME_ADVANCEDONPLACEPLACER, MODULE_VLBNAME_ADVANCEDONPLACEPLACER, LBCT_1 | LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_ADVANCEDONPLACEPLACER,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON, true, "", LBMUD_NONE, false, true, false, "", "" ));
    }
  case LBPM_ADVANCED_RANDOM_PLACER:
    {
      objParametersList.Append( new LBParameter( PARNAME_MINHEIGHT  , "", PARDESC_MINHEIGHT2 , LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MAXHEIGHT  , "", PARDESC_MAXHEIGHT2 , LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MINDISTANCE, "", PARDESC_MINDISTANCE, LBPT_FLOAT  , true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_PROB       , "", PARDESC_PROB       , LBPT_INTEGER, true, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_RANDOMSEED, "", PARDESC_RANDOMSEED, LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_HECTAREDENSITY, "", PARDESC_HECTAREDENSITY, LBPT_STRING, true, true, false ) );
      return (LBModule( MODULE_LBNAME_ADVANCEDRANDOMPLACER, MODULE_VLBNAME_ADVANCEDRANDOMPLACER, LBCT_1 | LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_ADVANCEDRANDOMPLACER,
                        LBMUO_MULTIPLE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON, true, "", LBMUD_NONE, false, true, false, "", "" ));
    }
  case LBPM_ADVANCED_RANDOM_PLACER_SLOPE:
    {
      objParametersList.Append( new LBParameter( PARNAME_MINHEIGHT  , "", PARDESC_MINHEIGHT2 , LBPT_INTEGER, true , false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MAXHEIGHT  , "", PARDESC_MAXHEIGHT2 , LBPT_INTEGER, true , false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MINDISTANCE, "", PARDESC_MINDISTANCE, LBPT_FLOAT  , true , false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MINSLOPE   , "", PARDESC_MINSLOPE   , LBPT_FLOAT  , false, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MAXSLOPE   , "", PARDESC_MAXSLOPE   , LBPT_FLOAT  , false, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_PROB       , "", PARDESC_PROB       , LBPT_INTEGER, true , false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_RANDOMSEED, "", PARDESC_RANDOMSEED, LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_HECTAREDENSITY, "", PARDESC_HECTAREDENSITY, LBPT_STRING, true, true, false ) );
      return (LBModule( MODULE_LBNAME_ADVANCEDRANDOMPLACERSLOPE, MODULE_VLBNAME_ADVANCEDRANDOMPLACERSLOPE, LBCT_1 | LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_ADVANCEDRANDOMPLACERSLOPE,
                        LBMUO_MULTIPLE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON, true, "", LBMUD_SINGLE, false, true, false, "", "" ));
    }
  case LBPM_BEST_FIT_PLACER:
    {
      objParametersList.Append( new LBParameter( PARNAME_LENGTH, "", PARDESC_LENGTH, LBPT_FLOAT, false, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_WIDTH , "", PARDESC_WIDTH3, LBPT_FLOAT, false, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_HEIGHT, "", PARDESC_HEIGHT, LBPT_FLOAT, false, false, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_DIRCORRECTION, "", PARDESC_DIRCORRECTION, LBPT_FLOAT , true , true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_DESIREDHEIGHT, "", PARDESC_DESIREDHEIGHT, LBPT_FLOAT , false, true, false ) );
      return (LBModule( MODULE_LBNAME_BESTFITPLACER, MODULE_VLBNAME_BESTFITPLACER, LBCT_1 | LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_BESTFITPLACER,
                        LBMUO_MULTIPLE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON, true, "", LBMUD_NONE, false, true, false, "", "" ));
    }
  case LBPM_BIASED_RANDOM_PLACER:
    {
      objParametersList.Append( new LBParameter( PARNAME_PROB       , "", PARDESC_PROB       , LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MINHEIGHT  , "", PARDESC_MINHEIGHT2 , LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MAXHEIGHT  , "", PARDESC_MAXHEIGHT2 , LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MINDISTANCE, "", PARDESC_MINDISTANCE, LBPT_FLOAT  , true, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_SMALLATBOUNDARY, "", PARDESC_SMALLATBOUNDARY, LBPT_BOOL   , true , true, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_RANDOMSEED     , "", PARDESC_RANDOMSEED     , LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_HECTAREDENSITY  , "", PARDESC_HECTAREDENSITY  , LBPT_STRING, true, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_AGGREGATIONCOEFF, "", PARDESC_AGGREGATIONCOEFF, LBPT_STRING, true, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_PARENTCOUNT, "", PARDESC_PARENTCOUNT2, LBPT_INTEGER, true, true, false ) );
      return (LBModule( MODULE_LBNAME_BIASEDRANDOMPLACER, MODULE_VLBNAME_BIASEDRANDOMPLACER, LBCT_1 | LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_BIASEDRANDOMPLACER,
                        LBMUO_MULTIPLE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON, true, "", LBMUD_NONE, false, true, false, "", "" ));
    }
  case LBPM_BIASED_RANDOM_PLACER_SLOPE:
    {
      objParametersList.Append( new LBParameter( PARNAME_PROB       , "", PARDESC_PROB       , LBPT_INTEGER, true , false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MINHEIGHT  , "", PARDESC_MINHEIGHT2 , LBPT_INTEGER, true , false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MAXHEIGHT  , "", PARDESC_MAXHEIGHT2 , LBPT_INTEGER, true , false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MINDISTANCE, "", PARDESC_MINDISTANCE, LBPT_FLOAT  , true , false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MINSLOPE   , "", PARDESC_MINSLOPE   , LBPT_FLOAT  , false, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MAXSLOPE   , "", PARDESC_MAXSLOPE   , LBPT_FLOAT  , false, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_SMALLATBOUNDARY, "", PARDESC_SMALLATBOUNDARY, LBPT_BOOL   , true , true, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_RANDOMSEED     , "", PARDESC_RANDOMSEED     , LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_HECTAREDENSITY  , "", PARDESC_HECTAREDENSITY  , LBPT_STRING, true, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_AGGREGATIONCOEFF, "", PARDESC_AGGREGATIONCOEFF, LBPT_STRING, true, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_PARENTCOUNT, "", PARDESC_PARENTCOUNT2, LBPT_INTEGER, true, true, false ) );
      return (LBModule( MODULE_LBNAME_BIASEDRANDOMPLACERSLOPE, MODULE_VLBNAME_BIASEDRANDOMPLACERSLOPE, LBCT_1 | LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_BIASEDRANDOMPLACERSLOPE,
                        LBMUO_MULTIPLE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON, true, "", LBMUD_SINGLE, false, true, false, "", "" ));
    }
  case LBPM_BOUNDARY_SUB_SQUARES_FRAME_PLACER:
    {
      objParametersList.Append( new LBParameter( PARNAME_MINHEIGHT, "", PARDESC_MINHEIGHT2, LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MAXHEIGHT, "", PARDESC_MAXHEIGHT2, LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_PROB     , "", PARDESC_PROB      , LBPT_INTEGER, true, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_RANDOMSEED , "", PARDESC_RANDOMSEED    , LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_SUBSQUARESSIZE, "", PARDESC_SUBSQUARESSIZE, LBPT_FLOAT  , true , true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_MAXNOISE   , "", PARDESC_MAXNOISE    , LBPT_FLOAT, true, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_MINDISTANCE, "", PARDESC_MINDISTANCE2, LBPT_FLOAT, true, true, false ) );
      return (LBModule( MODULE_LBNAME_BOUNDARYSUBSQUARESFRAMEPLACER, MODULE_VLBNAME_BOUNDARYSUBSQUARESFRAMEPLACER, LBCT_1 | LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_BOUNDARYSUBSQUARESFRAMEPLACER,
                        LBMUO_MULTIPLE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON, true, "", LBMUD_NONE, false, true, false, "", "" ));
    }
  case LBPM_BUILDING_CREATOR:
    {
      globalParametersList.Append( new LBParameter( PARNAME_MODELSPATH, "", PARDESC_MODELSPATH, LBPT_STRING , false, false, false ) );
//			globalParametersList.Append( new LBParameter( PARNAME_MATCHTOL     , "", PARDESC_MATCHTOL     , LBPT_FLOAT, false, true, false ) );
//			globalParametersList.Append( new LBParameter( PARNAME_VERTEXTOL    , "", PARDESC_VERTEXTOL    , LBPT_FLOAT, false, true, false ) );
//			globalParametersList.Append( new LBParameter( PARNAME_RECTCORNERTOL, "", PARDESC_RECTCORNERTOL, LBPT_FLOAT, false, true, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_RANDOMSEED, "", PARDESC_RANDOMSEED,  LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_NUMOFFLOOR,   "", PARDESC_NUMOFFLOOR,   LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_MAXNUMFLOORS, "", PARDESC_MAXNUMFLOORS, LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_FLOORHEIGHT,  "", PARDESC_FLOORHEIGHT,  LBPT_FLOAT,   false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_ROOFTYPE,     "", PARDESC_ROOFTYPE,     LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_RECTIFY,      "", PARDESC_RECTIFY,      LBPT_BOOL,    false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_TEXFILE,      "", PARDESC_TEXFILE,      LBPT_STRING,  false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_ROOFRED,      "", PARDESC_ROOFRED,      LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_ROOFGREEN,    "", PARDESC_ROOFGREEN,    LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_ROOFBLUE,     "", PARDESC_ROOFBLUE,     LBPT_INTEGER, false, true, false ) );
      return (LBModule( MODULE_LBNAME_BUILDINGCREATOR, MODULE_VLBNAME_BUILDINGCREATOR, LBCT_1 | LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_BUILDINGCREATOR,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON, false, "", LBMUD_NONE, false, false, false, "", "" ));
    }
  case LBPM_CROSSROAD_PLACER:
    {
      globalParametersList.Append( new LBParameter( PARNAME_XSIZE         , "", PARDESC_XSIZE         , LBPT_FLOAT , false, true, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_XPADDING      , "", PARDESC_XPADDING      , LBPT_FLOAT , false, true, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_XPALETTEFILE  , "", PARDESC_XPALETTEFILE  , LBPT_STRING, false, true, false ) );
      return (LBModule( MODULE_LBNAME_CROSSROADPLACER, MODULE_VLBNAME_CROSSROADPLACER, LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_CROSSROADPLACER,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POINT, false, "", LBMUD_NONE, false, false, true, MODULE_LBNAME_CROSSROADPLACEREXT, "" ));
    }
  case LBPM_CS_SMOOTH_TERRAIN_ALONG_PATH:
    {
      hiddenGlobalParametersList.Append( new LBParameter( PARNAME_LONGSMOOTHTYPE, "ConstantSlope", "", LBPT_STRING, true, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_NAMEPREFIX,  "", PARDESC_NAMEPREFIX2, LBPT_STRING, true,  false, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_WIDTH, "", PARDESC_WIDTH2, LBPT_FLOAT, false, true, false ) );
      return (LBModule( MODULE_LBNAME_CSSMOOTHTERRAINALONGPATH, MODULE_VLBNAME_CSSMOOTHTERRAINALONGPATH, LBCT_1 | LBCT_2, 
                        LBMT_TERRAIN_MODELER, MODULE_DESC_CSSMOOTHTERRAINALONGPATH,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYLINE, true, "", LBMUD_SINGLE, true, false, false, "", "" ));
    }
  case LBPM_DELETE_ALONG_PATH:
    {
      globalParametersList.Append( new LBParameter( PARNAME_WIDTH, "", PARDESC_WIDTH, LBPT_FLOAT, false, true, false ) );
      return (LBModule( MODULE_LBNAME_DELETEALONGPATH, MODULE_VLBNAME_DELETEALONGPATH, LBCT_1 | LBCT_2,
                        LBMT_DELETER_FROM_SHAPEFILE, MODULE_DESC_DELETEALONGPATH,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYLINE, true, "", LBMUD_NONE, false, false, false, "", "" ));
    }
  case LBPM_DELETE_IN_POLYGON:
    {
      return (LBModule( MODULE_LBNAME_DELETEINPOLYGON, MODULE_VLBNAME_DELETEINPOLYGON, LBCT_1 | LBCT_2,
                        LBMT_DELETER_FROM_SHAPEFILE, MODULE_DESC_DELETEINPOLYGON,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON, true, "", LBMUD_NONE, false, false, false, "", "" ));
    }
//  case LBPM_DUMMY_BUILDING_PLACER:
//    {
//      globalParametersList.Append( new LBParameter( PARNAME_MAXNUMFLOORSCIVIL     , "", PARDESC_MAXNUMFLOORSCIVIL     , LBPT_INTEGER, true , true, false ) );
//      globalParametersList.Append( new LBParameter( PARNAME_MAXNUMFLOORSINDUSTRIAL, "", PARDESC_MAXNUMFLOORSINDUSTRIAL, LBPT_INTEGER, true , true, false ) );
//      globalParametersList.Append( new LBParameter( PARNAME_MAXNUMFLOORSMILITARY  , "", PARDESC_MAXNUMFLOORSMILITARY  , LBPT_INTEGER, true , true, false ) );
//      globalParametersList.Append( new LBParameter( PARNAME_HEIGHTFLOORSCIVIL     , "", PARDESC_HEIGHTFLOORSCIVIL     , LBPT_INTEGER, true , true, false ) );
//      globalParametersList.Append( new LBParameter( PARNAME_HEIGHTFLOORSINDUSTRIAL, "", PARDESC_HEIGHTFLOORSINDUSTRIAL, LBPT_INTEGER, true , true, false ) );
//      globalParametersList.Append( new LBParameter( PARNAME_HEIGHTFLOORSMILITARY  , "", PARDESC_HEIGHTFLOORSMILITARY  , LBPT_INTEGER, true , true, false ) );
//      globalParametersList.Append( new LBParameter( PARNAME_MODELCIVIL            , "", PARDESC_MODELBUILDING         , LBPT_STRING , false, true, false ) );
//      globalParametersList.Append( new LBParameter( PARNAME_MODELINDUSTRIAL       , "", PARDESC_MODELBUILDING         , LBPT_STRING , false, true, false ) );
//      globalParametersList.Append( new LBParameter( PARNAME_MODELMILITARY         , "", PARDESC_MODELBUILDING         , LBPT_STRING , false, true, false ) );
//      dbfParametersList.Append( new LBParameter( PARNAME_BUILDINGTYPE, "", PARDESC_BUILDINGTYPE, LBPT_STRING, false, true, false ) );
//      dbfParametersList.Append( new LBParameter( PARNAME_NUMOFFLOOR  , "", PARDESC_NUMOFFLOOR  , LBPT_STRING, false, true, false ) );
//      return (LBModule( MODULE_LBNAME_DUMMYBUILDINGPLACER, MODULE_VLBNAME_DUMMYBUILDINGPLACER, LBCT_1 | LBCT_2,
//                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_DUMMYBUILDINGPLACER,
//                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
//                        true, AST_POLYGON, true, "", LBMUD_NONE, false, true, false, "", "" ));
//    }
  case LBPM_EDIFICI:
    {
      globalParametersList.Append( new LBParameter( PARNAME_LIBRARYFILE     , "", PARDESC_LIBRARYFILE     , LBPT_STRING , false, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_MODELSPATH      , "", PARDESC_MODELSPATH      , LBPT_STRING , false, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_RANDOMSEED      , "", PARDESC_RANDOMSEED      , LBPT_INTEGER, true , false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_SIMILARITYFACTOR, "", PARDESC_SIMILARITYFACTOR, LBPT_FLOAT  , true , false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_NAMEPREFIX      , "", PARDESC_NAMEPREFIX      , LBPT_STRING , true, false, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_TYPE          , "", PARDESC_TYPE          , LBPT_STRING , false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_NUMFLOORS     , "", PARDESC_NUMFLOORS     , LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_ENTERABLE     , "", PARDESC_ENTERABLE     , LBPT_BOOL   , false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_RECTIFYCORNERS, "", PARDESC_RECTIFYCORNERS, LBPT_BOOL   , false, true, false ) );
      return LBModule( MODULE_LBNAME_EDIFICI, MODULE_VLBNAME_EDIFICI, LBCT_2,
               LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_EDIFICI,
               LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
               true, AST_POLYGON, true, "", LBMUD_NONE, false, true, false, "", "");
    }
  case LBPM_FA_SMOOTH_TERRAIN_ALONG_PATH:
    {
      globalParametersList.Append( new LBParameter( PARNAME_LONGSMOOTHPAR, "", PARDESC_LONGSMOOTHPAR, LBPT_FLOAT , false, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_NAMEPREFIX,  "", PARDESC_NAMEPREFIX2, LBPT_STRING, true,  false, false ) );
      hiddenGlobalParametersList.Append( new LBParameter( PARNAME_LONGSMOOTHTYPE, "FixedAbsolute", "", LBPT_STRING, true, false, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_WIDTH, "", PARDESC_WIDTH2, LBPT_FLOAT, false, true, false ) );
      return (LBModule( MODULE_LBNAME_FASMOOTHTERRAINALONGPATH, MODULE_VLBNAME_FASMOOTHTERRAINALONGPATH, LBCT_1 | LBCT_2, 
                        LBMT_TERRAIN_MODELER, MODULE_DESC_FASMOOTHTERRAINALONGPATH,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYLINE, true, "", LBMUD_SINGLE, true, false, false, "", "" ));
    }
  case LBPM_FRAMED_FOREST_PLACER:
    {
      objParametersList.Append( new LBParameter( PARNAME_MINHEIGHT, "", PARDESC_MINHEIGHT2, LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MAXHEIGHT, "", PARDESC_MAXHEIGHT2, LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_PROB     , "", PARDESC_PROB      , LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_INNER    , "", PARDESC_INNER     , LBPT_BOOL   , true, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_RANDOMSEED, "", PARDESC_RANDOMSEED, LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_MAINDIRECTION     , "", PARDESC_MAINDIRECTION2    , LBPT_FLOAT  , false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_SUBSQUARESSIZE    , "", PARDESC_SUBSQUARESSIZE    , LBPT_FLOAT  , true, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_MAXNOISE          , "", PARDESC_MAXNOISE          , LBPT_FLOAT  , true, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_MINDISTANCE       , "", PARDESC_MINDISTANCE3      , LBPT_FLOAT  , true, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_NUMBOUNDARYOBJECTS, "", PARDESC_NUMBOUNDARYOBJECTS, LBPT_INTEGER, true, true, false ) );
      return (LBModule( MODULE_LBNAME_FRAMEDFORESTPLACER, MODULE_VLBNAME_FRAMEDFORESTPLACER, LBCT_1 | LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_FRAMEDFORESTPLACER,
                        LBMUO_MULTIPLE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON, true, "", LBMUD_NONE, false, true, false, "", "" ));
    }
  case LBPM_FR_SMOOTH_TERRAIN_ALONG_PATH:
    {
      globalParametersList.Append( new LBParameter( PARNAME_LONGSMOOTHPAR, "", PARDESC_LONGSMOOTHPAR2, LBPT_FLOAT , false, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_NAMEPREFIX,  "", PARDESC_NAMEPREFIX2, LBPT_STRING, true,  false, false ) );
      hiddenGlobalParametersList.Append( new LBParameter( PARNAME_LONGSMOOTHTYPE, "FixedRelative", "", LBPT_STRING, true, false, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_WIDTH, "", PARDESC_WIDTH2, LBPT_FLOAT, false, true, false ) );
      return (LBModule( MODULE_LBNAME_FRSMOOTHTERRAINALONGPATH, MODULE_VLBNAME_FRSMOOTHTERRAINALONGPATH, LBCT_1 | LBCT_2, 
                        LBMT_TERRAIN_MODELER, MODULE_DESC_FRSMOOTHTERRAINALONGPATH,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYLINE, true, "", LBMUD_SINGLE, true, false, false, "", "" ));
    }
//  case LBPM_GEO_PROJECTION:
//    {
//      globalParametersList.Append( new LBParameter( PARNAME_TRANSFTYPE, "", PARDESC_TRANSFTYPE, LBPT_STRING, false, false, false ) );
//      return (LBModule( MODULE_LBNAME_GEOPROJECTION, MODULE_VLBNAME_GEOPROJECTION, LBCT_1 | LBCT_2,
//                        LBMT_SHAPE_MODELER, MODULE_DESC_GEOPROJECTION,
//                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
//                        false, AST_NONE, true, "", LBMUD_NONE, false, false, false, "", "" ));
//    }
//  case LBPM_HIGHMAP_LOADER:
//    return (LBModule( MODULE_LBNAME_HIGHMAPLOADER, MODULE_VLBNAME_HIGHMAPLOADER, LBCT_1 | LBCT_2,
//                      LBMT_TERRAIN_MODELER, MODULE_DESC_HIGHMAPLOADER,
//                      LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
//                      false, AST_NONE, true, "", LBMUD_MULTIPLE, true, false, false, "", "" ));
//  case LBPM_HIGHMAP_TEST:
//    {
//      globalParametersList.Append( new LBParameter( PARNAME_AMPLITUDEX, "", PARDESC_AMPLITUDEX, LBPT_FLOAT, false, true, false ) );
//      globalParametersList.Append( new LBParameter( PARNAME_PERIODX   , "", PARDESC_PERIODX   , LBPT_FLOAT, false, true, false ) );
//      globalParametersList.Append( new LBParameter( PARNAME_AMPLITUDEY, "", PARDESC_AMPLITUDEY, LBPT_FLOAT, false, true, false ) );
//      globalParametersList.Append( new LBParameter( PARNAME_PERIODY   , "", PARDESC_PERIODY   , LBPT_FLOAT, false, true, false ) );
//      return (LBModule( MODULE_LBNAME_HIGHMAPTEST, MODULE_VLBNAME_HIGHMAPTEST, LBCT_1 | LBCT_2,
//                        LBMT_TERRAIN_MODELER, MODULE_DESC_HIGHMAPTEST,
//                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
//                        false, AST_NONE, true, "", LBMUD_NONE, true, false, false, "", "" ));
//    }
  case LBPM_INNER_SUB_SQUARES_FRAME_PLACER:
    {
      objParametersList.Append( new LBParameter( PARNAME_MINHEIGHT, "", PARDESC_MINHEIGHT2, LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MAXHEIGHT, "", PARDESC_MAXHEIGHT2, LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_PROB     , "", PARDESC_PROB      , LBPT_INTEGER, true, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_RANDOMSEED, "", PARDESC_RANDOMSEED, LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_SUBSQUARESSIZE, "", PARDESC_SUBSQUARESSIZE, LBPT_FLOAT, true, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_MAXNOISE      , "", PARDESC_MAXNOISE      , LBPT_FLOAT, true, true, false ) );
      return (LBModule( MODULE_LBNAME_INNERSUBSQUARESFRAMEPLACER, MODULE_VLBNAME_INNERSUBSQUARESFRAMEPLACER, LBCT_1 | LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_INNERSUBSQUARESFRAMEPLACER,
                        LBMUO_MULTIPLE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON, true, "", LBMUD_NONE, false, true, false, "", "" ));
    }
  case LBPM_M_SMOOTH_TERRAIN_ALONG_PATH:
    {
      globalParametersList.Append( new LBParameter( PARNAME_LONGSMOOTHPAR, "", PARDESC_LONGSMOOTHPAR3, LBPT_FLOAT , false, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_NAMEPREFIX,  "", PARDESC_NAMEPREFIX2, LBPT_STRING, true,  false, false ) );
      hiddenGlobalParametersList.Append( new LBParameter( PARNAME_LONGSMOOTHTYPE, "Monotone", "", LBPT_STRING, true, false, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_WIDTH, "", PARDESC_WIDTH2, LBPT_FLOAT, false, true, false ) );
      return (LBModule( MODULE_LBNAME_MSMOOTHTERRAINALONGPATH, MODULE_VLBNAME_MSMOOTHTERRAINALONGPATH, LBCT_1 | LBCT_2,
                        LBMT_TERRAIN_MODELER, MODULE_DESC_MSMOOTHTERRAINALONGPATH,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYLINE, true, "", LBMUD_SINGLE, true, false, false, "", "" ));
    }
  case LBPM_MA_SMOOTH_TERRAIN_ALONG_PATH:
    {
      globalParametersList.Append( new LBParameter( PARNAME_LONGSMOOTHPAR, "", PARDESC_LONGSMOOTHPAR4, LBPT_FLOAT , false, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_NAMEPREFIX,  "", PARDESC_NAMEPREFIX2, LBPT_STRING, true,  false, false ) );
      hiddenGlobalParametersList.Append( new LBParameter( PARNAME_LONGSMOOTHTYPE, "MovingAverage", "", LBPT_STRING, true, false, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_WIDTH, "", PARDESC_WIDTH2, LBPT_FLOAT, false, true, false ) );
      return (LBModule( MODULE_LBNAME_MASMOOTHTERRAINALONGPATH, MODULE_VLBNAME_MASMOOTHTERRAINALONGPATH, LBCT_1 | LBCT_2, 
                        LBMT_TERRAIN_MODELER, MODULE_DESC_MASMOOTHTERRAINALONGPATH,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYLINE, true, "", LBMUD_SINGLE, true, false, false, "", "" ));
    }
  case LBPM_MS_SMOOTH_TERRAIN_ALONG_PATH:
    {
      globalParametersList.Append( new LBParameter( PARNAME_LONGSMOOTHPAR, "", PARDESC_LONGSMOOTHPAR3, LBPT_FLOAT , false, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_NAMEPREFIX,  "", PARDESC_NAMEPREFIX2, LBPT_STRING, true,  false, false ) );
      hiddenGlobalParametersList.Append( new LBParameter( PARNAME_LONGSMOOTHTYPE, "MaxSlope", "", LBPT_STRING, true, false, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_WIDTH, "", PARDESC_WIDTH2, LBPT_FLOAT, false, true, false ) );
      return (LBModule( MODULE_LBNAME_MSSMOOTHTERRAINALONGPATH, MODULE_VLBNAME_MSSMOOTHTERRAINALONGPATH, LBCT_1 | LBCT_2, 
                        LBMT_TERRAIN_MODELER, MODULE_DESC_MSSMOOTHTERRAINALONGPATH,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYLINE, true, "", LBMUD_SINGLE, true, false, false, "", "" ));
    }
//  case LBPM_NONE:
//    return (LBModule( MODULE_LBNAME_NONE, MODULE_VLBNAME_NONE, LBCT_1 | LBCT_2,
//                      LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_NONE,
//                      LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
//                      true, AST_POLYGON, true, "", LBMUD_NONE, false, true, false, "", "" ));
  case LBPM_NY_DUMMY_BUILDING_PLACER:
    {
      globalParametersList.Append( new LBParameter( PARNAME_RANDOMSEED       , "", PARDESC_RANDOMSEED       , LBPT_INTEGER, false, true, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_BUILDINGMINHEIGHT, "", PARDESC_BUILDINGMINHEIGHT, LBPT_FLOAT  , false, true, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_BUILDINGMAXHEIGHT, "", PARDESC_BUILDINGMAXHEIGHT, LBPT_FLOAT  , false, true, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_MODELNAMEBASE    , "", PARDESC_MODELNAMEBASE    , LBPT_STRING , false, true, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_MODELVARIANTS    , "", PARDESC_MODELVARIANTS    , LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_MAINDIRECTION, "", PARDESC_MAINDIRECTION, LBPT_FLOAT  , false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_ROADMINWIDTH , "", PARDESC_ROADMINWIDTH , LBPT_FLOAT  , false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_ROADMAXWIDTH , "", PARDESC_ROADMAXWIDTH , LBPT_FLOAT  , false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_BLOCKMINSIZE , "", PARDESC_BLOCKMINSIZE , LBPT_FLOAT  , false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_BLOCKMAXSIZE , "", PARDESC_BLOCKMAXSIZE , LBPT_FLOAT  , false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_BLOCKMINSUBD , "", PARDESC_BLOCKMINSUBD , LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_BLOCKMAXSUBD , "", PARDESC_BLOCKMAXSUBD , LBPT_INTEGER, false, true, false ) );
      return (LBModule( MODULE_LBNAME_NYDUMMYBUILDINGPLACER, MODULE_VLBNAME_NYDUMMYBUILDINGPLACER, LBCT_1 | LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_NYDUMMYBUILDINGPLACER,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON, true, "", LBMUD_NONE, false, true, false, "", "" ));
    }
  case LBPM_ON_PLACE_PLACER:
    {
      dbfParametersList.Append( new LBParameter( PARNAME_OBJECT     , "", PARDESC_OBJECT     , LBPT_STRING, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_ORIENTATION, "", PARDESC_ORIENTATION, LBPT_FLOAT , false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_SCALEX     , "", PARDESC_SCALEX     , LBPT_FLOAT , true , true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_SCALEY     , "", PARDESC_SCALEY     , LBPT_FLOAT , true , true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_SCALEZ     , "", PARDESC_SCALEZ     , LBPT_FLOAT , true , true, false ) );
      return (LBModule( MODULE_LBNAME_ONPLACEPLACER, MODULE_VLBNAME_ONPLACEPLACER, LBCT_1 | LBCT_2, 
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_ONPLACEPLACER,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POINT, true, "", LBMUD_NONE, false, true, false, "", "" ));
    }
  case LBPM_RANDOM_ON_PATH_PLACER:
    {
      objParametersList.Append( new LBParameter( PARNAME_PROB     , "", PARDESC_PROB      , LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MINHEIGHT, "", PARDESC_MINHEIGHT3, LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MAXHEIGHT, "", PARDESC_MAXHEIGHT3, LBPT_INTEGER, true, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_MAXDISTANCE, "", PARDESC_MAXDISTANCE, LBPT_FLOAT  , true , true, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_RANDOMSEED , "", PARDESC_RANDOMSEED , LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_LINEARDENSITY, "", PARDESC_LINEARDENSITY, LBPT_STRING, true, true, false ) );
      return (LBModule( MODULE_LBNAME_RANDOMONPATHPLACER, MODULE_VLBNAME_RANDOMONPATHPLACER, LBCT_1 | LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_RANDOMONPATHPLACER,
                        LBMUO_MULTIPLE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON | AST_POLYLINE, true, "", LBMUD_NONE, false, true, false, "", "" ));
    }
  case LBPM_REGULAR_ON_PATH_PLACER:
    {
      objParametersList.Append( new LBParameter( PARNAME_MINHEIGHT    , "", PARDESC_MINHEIGHT3   , LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MAXHEIGHT    , "", PARDESC_MAXHEIGHT3   , LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MATCHORIENT  , "", PARDESC_MATCHORIENT  , LBPT_BOOL   , true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_DIRCORRECTION, "", PARDESC_DIRCORRECTION, LBPT_FLOAT  , true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_PLACERIGHT   , "", PARDESC_PLACERIGHT   , LBPT_BOOL   , true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_PLACELEFT    , "", PARDESC_PLACELEFT    , LBPT_BOOL   , true, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_RANDOMSEED , "", PARDESC_RANDOMSEED , LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_OFFSET        , "", PARDESC_OFFSET        , LBPT_FLOAT, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_OFFSETMAXNOISE, "", PARDESC_OFFSETMAXNOISE, LBPT_FLOAT, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_STEP          , "", PARDESC_STEP2         , LBPT_FLOAT, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_STEPMAXNOISE  , "", PARDESC_STEPMAXNOISE  , LBPT_FLOAT, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_FIRSTSTEP     , "", PARDESC_FIRSTSTEP     , LBPT_FLOAT, false, true, false ) );
      return (LBModule( MODULE_LBNAME_REGULARONPATHPLACER, MODULE_VLBNAME_REGULARONPATHPLACER, LBCT_1 | LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_REGULARONPATHPLACER,
                        LBMUO_SINGLE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON | AST_POLYLINE, true, "", LBMUD_NONE, false, true, false, "", "" ));
    }
  case LBPM_RIVERS:
    {
      globalParametersList.Append( new LBParameter( PARNAME_NOWATERELEV, "", PARDESC_NOWATERELEV, LBPT_FLOAT , false, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_WATERDEPTH,  "", PARDESC_WATERDEPTH,  LBPT_FLOAT , false, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_NAMEPREFIX,  "", PARDESC_NAMEPREFIX2, LBPT_STRING, true,  false, false ) );
      return (LBModule( MODULE_LBNAME_RIVERS, MODULE_VLBNAME_RIVERS, LBCT_2, 
                        LBMT_TERRAIN_MODELER, MODULE_DESC_RIVERS,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON, false, "", LBMUD_SINGLE, true, false, false, "", "" ));
    }
  case LBPM_ROADS_GENERATOR:
    {
      globalParametersList.Append( new LBParameter( PARNAME_MODELSPATH, "", PARDESC_MODELSPATH, LBPT_STRING , false, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_MASKSIZE  , "", PARDESC_MASKSIZE  , LBPT_INTEGER, true , false, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_WIDTH  , "", PARDESC_WIDTH  , LBPT_STRING , false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_LEVEL  , "", PARDESC_LEVEL  , LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_TEXFILE, "", PARDESC_TEXFILE, LBPT_STRING , false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_MASKCOL, "", PARDESC_MASKCOL, LBPT_INTEGER, true , true, false ) );
      return (LBModule( MODULE_LBNAME_ROADSGENERATOR, MODULE_VLBNAME_ROADSGENERATOR, LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_ROADSGENERATOR,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYLINE, false, "", LBMUD_NONE, false, false, false, "", "" ));
    }
  case LBPM_ROAD_PLACER:
    {
      globalParametersList.Append( new LBParameter( PARNAME_PARTCOUNT   , "", PARDESC_PARTCOUNT   , LBPT_INTEGER, false, true, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_STEP        , "", PARDESC_STEP        , LBPT_FLOAT  , false, true, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_ROADTYPE    , "", PARDESC_ROADTYPE    , LBPT_STRING , false, true, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_PARTCOUNT2  , "", PARDESC_PARTCOUNT2  , LBPT_INTEGER, false, true, true ) );
      globalParametersList.Append( new LBParameter( PARNAME_PARTNAMEBASE, "", PARDESC_PARTNAMEBASE, LBPT_STRING , false, true, true ) );
      dbfParametersList.Append( new LBParameter( PARNAME_WIDTH         , "", PARDESC_WIDTH, LBPT_STRING, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_XFORCEDTEXTURE, "", PARDESC_XFORCEDTEXTURE, LBPT_STRING, false, true, false ) );
      return (LBModule( MODULE_LBNAME_ROADPLACER, MODULE_VLBNAME_ROADPLACER, LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_ROADPLACER,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYLINE, false, "", LBMUD_NONE, false, false, true, MODULE_LBNAME_ROADPLACEREXT, "" ));
    }
// for the moment scripts are not implemented
//	case LBPM_SCRIPT_MODULE:
//		return LBModule(MODULE_LBNAME_SCRIPTMODULE, MODULE_VLBNAME_SCRIPTMODULE, LBCT_1 | LBCT_2, LBMT_SCRIPT,
//			            MODULE_DESC_SCRIPTMODULE, LBMUO_NONE, objParametersList, globalParametersList, dbfParametersList,
//						false, AST_NONE, true, "", LBMUD_NONE, false, false, false, "", "");
  case LBPM_SIMPLE_RANDOM_PLACER:
    {
      globalParametersList.Append( new LBParameter( PARNAME_RANDOMSEED, "", PARDESC_RANDOMSEED, LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_HECTAREDENSITY, "", PARDESC_HECTAREDENSITY, LBPT_STRING, true, true, false ) );
      return (LBModule( MODULE_LBNAME_SIMPLERANDOMPLACER, MODULE_VLBNAME_SIMPLERANDOMPLACER, LBCT_1 | LBCT_2, 
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_SIMPLERANDOMPLACER,
                        LBMUO_SINGLE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON, true, "", LBMUD_NONE, false, true, false, "", "" ));
    }
  case LBPM_SIMPLE_RANDOM_PLACER_SLOPE:
    {
      objParametersList.Append( new LBParameter( PARNAME_MINSLOPE, "", PARDESC_MINSLOPE, LBPT_FLOAT, false, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MAXSLOPE, "", PARDESC_MAXSLOPE, LBPT_FLOAT, false, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_RANDOMSEED, "", PARDESC_RANDOMSEED, LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_HECTAREDENSITY, "", PARDESC_HECTAREDENSITY, LBPT_STRING, true, true, false ) );
      return (LBModule( MODULE_LBNAME_SIMPLERANDOMPLACERSLOPE, MODULE_VLBNAME_SIMPLERANDOMPLACERSLOPE, LBCT_1 | LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_SIMPLERANDOMPLACERSLOPE,
                        LBMUO_SINGLE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON, true, "", LBMUD_SINGLE, false, true, false, "", "" ));
    }
/*
    no more needed, replaced by specialized modules

  case LBPM_SMOOTH_TERRAIN_ALONG_PATH:
    {
      dbfParametersList.Append(new LBParameter(PARNAME_WIDTH, "", PARDESC_WIDTH2, LBPT_FLOAT, false, true, false));
      return LBModule(MODULE_LBNAME_SMOOTHTERRAINALONGPATH, MODULE_VLBNAME_SMOOTHTERRAINALONGPATH, LBCT_1 | LBCT_2, 
              LBMT_TERRAIN_MODELER, MODULE_DESC_SMOOTHTERRAINALONGPATH,
              LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
              true, AST_POLYLINE, true, "", LBMUD_SINGLE, true, false, false, "", "");
    }
*/
  case LBPM_STRADE:
    {
      globalParametersList.Append( new LBParameter( PARNAME_LIBRARYFILE, "", PARDESC_LIBRARYFILE, LBPT_STRING , false, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_MODELSPATH , "", PARDESC_MODELSPATH , LBPT_STRING , false, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_RANDOMSEED , "", PARDESC_RANDOMSEED , LBPT_INTEGER, true, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_CROSSTYPE  , "", PARDESC_CROSSTYPE  , LBPT_INTEGER, true, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_USESPLINES , "", PARDESC_USESPLINES , LBPT_BOOL   , true, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_DXFEXPORT  , "", PARDESC_DXFEXPORT  , LBPT_INTEGER, true , false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_VISITORVER , "", PARDESC_VISITORVER , LBPT_STRING , false, false, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_TYPE          , "", PARDESC_TYPE  , LBPT_STRING, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_WIDTH         , "", PARDESC_WIDTH , LBPT_FLOAT , false, true, false ) );
      return LBModule( MODULE_LBNAME_STRADE, MODULE_VLBNAME_STRADE, LBCT_2, LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_STRADE,
                       LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                       true, AST_POLYLINE, true, "", LBMUD_NONE, false, true, false, "", "" );
    }
  case LBPM_SUB_SQUARES_RANDOM_PLACER:
    {
      objParametersList.Append( new LBParameter( PARNAME_MINHEIGHT, "", PARDESC_MINHEIGHT2, LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_MAXHEIGHT, "", PARDESC_MAXHEIGHT2, LBPT_INTEGER, true, false, false ) );
      objParametersList.Append( new LBParameter( PARNAME_PROB     , "", PARDESC_PROB      , LBPT_INTEGER, true, false, false ) );
      globalParametersList.Append( new LBParameter( PARNAME_RANDOMSEED, "", PARDESC_RANDOMSEED, LBPT_INTEGER, false, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_SUBSQUARESSIZE, "", PARDESC_SUBSQUARESSIZE, LBPT_FLOAT, true, true, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_MAXNOISE      , "", PARDESC_MAXNOISE      , LBPT_FLOAT, true, true, false ) );
      return (LBModule( MODULE_LBNAME_SUBSQUARESRANDOMPLACER, MODULE_VLBNAME_SUBSQUARESRANDOMPLACER, LBCT_1 | LBCT_2,
                        LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_SUBSQUARESRANDOMPLACER,
                        LBMUO_MULTIPLE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POLYGON, true, "", LBMUD_NONE, false, true, false, "", "" ));
    }
  case LBPM_TRANSFORM_2D:
    {
      globalParametersList.Append( new LBParameter( PARNAME_TRANSFTYPE, "", PARDESC_TRANSFTYPE, LBPT_STRING, false, false, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_X   , "", PARDESC_X   , LBPT_STRING, false, false, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_Y   , "", PARDESC_Y   , LBPT_STRING, false, false, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_USED, "", PARDESC_USED, LBPT_STRING, false, false, false ) );
      dbfParametersList.Append( new LBParameter( PARNAME_LAST, "", PARDESC_LAST, LBPT_STRING, false, false, false ) );
      return (LBModule( MODULE_LBNAME_TRANSFORM2D, MODULE_VLBNAME_TRANSFORM2D, LBCT_1 | LBCT_2,
                        LBMT_SHAPE_MODELER, MODULE_DESC_TRANSFORM2D,
                        LBMUO_NONE, objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                        true, AST_POINT, true, "", LBMUD_NONE, false, false, false, "", "" ));
    }
  default:
    return (LBModule( MODULE_LBNAME_NONE, MODULE_VLBNAME_NONE, LBCT_1 | LBCT_2,
                      LBMT_PLACER_FROM_SHAPEFILE, MODULE_DESC_NONE, LBMUO_NONE, 
                      objParametersList, globalParametersList, hiddenGlobalParametersList, dbfParametersList,
                      true, AST_POLYGON, true, "", LBMUD_NONE, false, true, false, "", "" ));
  }
}

//-----------------------------------------------------------------------------
