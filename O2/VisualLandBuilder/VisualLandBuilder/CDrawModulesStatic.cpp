#include "stdafx.h"

#include "limits.h"

#include "resource.h"

#include ".\CPreviewDlg.h"
#include ".\CDrawModulesStatic.h"

#include "..\..\LandBuilder\LandBuilder2\IShapeExtra.h"
#include "..\..\LandBuilder\Shapes\ShapeHelpers.h"

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CDrawModulesStatic, CStatic)
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_SETCURSOR()
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //

CDrawModulesStatic::CDrawModulesStatic() : CStatic()
{
	m_ModuleList           = NULL;
	m_ShapeOrganizerList   = NULL;
	m_ChangedSelectedShape = false;
	m_Panning              = false;
	m_PanningProgressing   = false;
	m_HandClose = ::LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_HAND_CLOSE));
	m_HandOpen  = ::LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_HAND_OPEN));
	m_ZoomingWindow            = false;
	m_ZoomingWindowProgressing = false;
	m_ZoomWindow = ::LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_ZOOM_WINDOW));
}

// -------------------------------------------------------------------------- //
// INTERFACE                                                                  //
// -------------------------------------------------------------------------- //

void CDrawModulesStatic::SetList(LBModuleList* moduleList, AutoArray<pShapeOrganizer>* shapeOrganizerList, bool showAll, bool showModulePreview)
{
	m_ModuleList         = moduleList;
	m_ShapeOrganizerList = shapeOrganizerList;
	m_ShowAll            = showAll;
	m_ShowModulePreview  = showModulePreview;

	m_OldSelectedModule  = -1;
	m_OldSelectedShape   = -1;
	m_SelectedModule     = -1;
	m_SelectedShape      = -1;

	m_DrawParams.zoomFactor = 1.0;

	// set the variable to true to trigger module calculation
	m_CalculateModules   = true;

	SetWorldExtents();
	SetViewExtents(false);
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::SetSelectedShape(int moduleIndex, int shapeIndex)
{
	m_OldSelectedModule    = m_SelectedModule;
	m_OldSelectedShape     = m_SelectedShape;
	m_SelectedModule       = moduleIndex;
	m_SelectedShape        = shapeIndex;
	m_ChangedSelectedShape = true;
	this->Invalidate();
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::ZoomWindow()
{
	m_ZoomingWindow = !m_ZoomingWindow;
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::Pan()
{
	m_Panning = !m_Panning;
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::ZoomPlus()
{
	m_DrawParams.zoomFactor *= 1.2;
	SetViewExtents(false);
	Invalidate();
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::ZoomMinus()
{
	m_DrawParams.zoomFactor /= 1.2;
	if(m_DrawParams.zoomFactor < 1.0) m_DrawParams.zoomFactor = 1.0;
	SetViewExtents(false);
	Invalidate();
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::ZoomExt()
{
	m_DrawParams.zoomFactor = 1.0;
	m_ViewExtents = m_WorldExtents;
	SetViewExtents(false);
	Invalidate();
}

// -------------------------------------------------------------------------- //

double CDrawModulesStatic::GetZoomFactor()
{
	return m_DrawParams.zoomFactor;
}

// -------------------------------------------------------------------------- //

bool CDrawModulesStatic::IsPanning() const
{
	return m_Panning;
}

// -------------------------------------------------------------------------- //

bool CDrawModulesStatic::IsPanningProgressing() const
{
	return m_PanningProgressing;
}

// -------------------------------------------------------------------------- //

bool CDrawModulesStatic::IsZoomingWindow() const
{
	return m_ZoomingWindow;
}

// -------------------------------------------------------------------------- //

bool CDrawModulesStatic::IsZoomingWindowProgressing() const
{
	return m_ZoomingWindowProgressing;
}

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

void CDrawModulesStatic::SetWorldExtents()
{
	if(!m_ModuleList)
	{
		m_WorldExtents.Zero();
	}
	else
	{
		double left   = FLT_MAX;
		double right  = FLT_MAX;
		double top    = FLT_MAX;
		double bottom = FLT_MAX;

		INT_PTR moduleCount = m_ModuleList->GetCount();
		for(int i = 0; i < moduleCount; ++i)
		{
			LBModule* module = m_ModuleList->GetAt(i);

			if(module->GetHasPreview() || m_ShowAll)
			{
				if(module->IsActive() || m_ShowAll)
				{
					if(module->GetUseShapefile())
					{
						Shapes::ShapeList& shapeList = (*m_ShapeOrganizerList)[i]->GetShapeList();
						int shapesCount = shapeList.Size();
						for(int j = 0; j < shapesCount; ++j)
						{
							Shapes::IShape* shape = shapeList.GetShape(j);	
							int verticesCount = shape->GetVertexCount();
							for(int k = 0; k < verticesCount; ++k)
							{
								const Shapes::DVertex& v = shape->GetVertex(k);
								if(left   == FLT_MAX || left   > v.x) left   = v.x;
								if(top    == FLT_MAX || top    < v.y) top    = v.y;
								if(right  == FLT_MAX || right  < v.x) right  = v.x;
								if(bottom == FLT_MAX || bottom > v.y) bottom = v.y;
							}		
						}
					}
				}
			}
		}
		m_WorldExtents.Set(left, top, right, bottom);
		m_ViewExtents = m_WorldExtents;
	}
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::SetViewExtents(bool moveViewCenter)
{
	CRect rect;
	GetClientRect(rect);

	double clientWidth  = rect.Width() - 4;
	double clientHeight = rect.Height() - 4;
	double worldWidth  = m_WorldExtents.Width();
	double worldHeight = m_WorldExtents.Height();

	if(clientWidth <= 0.0 || clientHeight <= 0.0)
	{
		m_ViewExtents.Zero();
		return;
	}

	if(m_ViewExtents.IsZero())
	{
		m_ViewExtents = m_WorldExtents;
	}

	if(!moveViewCenter)
	{
		double ratioW = worldWidth / clientWidth;
		double ratioH = worldHeight / clientHeight;
		if(ratioW > ratioH)
		{
			m_DrawParams.ratio = ratioW / m_DrawParams.zoomFactor;
		}
		else
		{
			m_DrawParams.ratio = ratioH / m_DrawParams.zoomFactor;
		}

		Shapes::DVertex viewBarycenter = m_ViewExtents.GetBarycenter();

		m_ViewExtents.Set(viewBarycenter.x - m_DrawParams.ratio * clientWidth * 0.5, 
						  viewBarycenter.y + m_DrawParams.ratio * clientHeight * 0.5, 
						  viewBarycenter.x + m_DrawParams.ratio * clientWidth * 0.5, 
						  viewBarycenter.y - m_DrawParams.ratio * clientHeight * 0.5);
	}
	else
	{
		Shapes::DVertex viewBarycenter = m_ViewExtents.GetBarycenter();
		double viewNewWidth  = rect.Width() * m_DrawParams.ratio;
		double viewNewHeight = rect.Height() * m_DrawParams.ratio;
		m_ViewExtents.Set(viewBarycenter.x - viewNewWidth * 0.5, 
			              viewBarycenter.y + viewNewHeight * 0.5, 
						  viewBarycenter.x + viewNewWidth * 0.5, 
						  viewBarycenter.y - viewNewHeight * 0.5);
	}
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::DrawShapes(CPaintDC* dc, CRect& rect)
{
	if(m_ModuleList)
	{
		if(m_ViewExtents.Width() > 0 && m_ViewExtents.Height() > 0)
		{
			INT_PTR moduleCount = m_ModuleList->GetCount();
			for(int i = 0; i < moduleCount; ++i)
			{
				LBModule* module = m_ModuleList->GetAt(i);

				if(module->GetHasPreview() || m_ShowAll)
				{
					if(module->IsActive() || m_ShowAll)
					{
						if(module->GetUseShapefile())
						{
							Shapes::ShapeList& shapeList = (*m_ShapeOrganizerList)[i]->GetShapeList();
							int shapesCount = shapeList.Size();
							for(int j = 0; j < shapesCount; ++j)
							{
								Shapes::IShape* shape = shapeList.GetShape(j);
								RString type = GetShapeType(shape);
								if(type == SHAPE_NAME_POINT || type == SHAPE_NAME_MULTIPOINT)
								{
									DrawPoints(dc, rect, shape);
								}
								else if(type == SHAPE_NAME_POLYLINE)
								{
									DrawPolyline(dc, rect, shape);
								}
								else
								{
									DrawPolygon(dc, rect, shape);
								}
							}
						}
					}
				}
			}
		}
	}
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::DrawSelectedShape(CPaintDC* dc, CRect& rect, bool highlight)
{
	int selModule;
	int selShape;

	CPen redPen;
	CPen* pOldPen = NULL;

	if(highlight)
	{
		redPen.CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
		pOldPen = dc->SelectObject(&redPen);
		selModule = m_SelectedModule;
		selShape  = m_SelectedShape;	
	}
	else
	{
		selModule = m_OldSelectedModule;
		selShape  = m_OldSelectedShape;	
	}

	if(m_ModuleList)
	{
		if(m_ViewExtents.Width() > 0 && m_ViewExtents.Height() > 0)
		{
			if(selModule != -1 && selShape != -1)
			{
				Shapes::ShapeList& shapeList = (*m_ShapeOrganizerList)[selModule]->GetShapeList();
				Shapes::IShape* shape = shapeList.GetShape(selShape);
				RString type = GetShapeType(shape);
				if(type == SHAPE_NAME_POINT || type == SHAPE_NAME_MULTIPOINT)
				{
					DrawPoints(dc, rect, shape);
				}
				else if(type == SHAPE_NAME_POLYLINE)
				{
					DrawPolyline(dc, rect, shape);
				}
				else
				{
					DrawPolygon(dc, rect, shape);
				}
			}
		}
	}

	if(highlight)
	{
		dc->SelectObject(pOldPen);
		redPen.DeleteObject();
	}
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::DrawPolygon(CPaintDC* dc, CRect& rect, Shapes::IShape* shape)
{
	int partCount = shape->GetPartCount();
	int start = 0;
	for(int i = 0; i < partCount; ++i)
	{
		int verticesCount = shape->GetPartSize(i);

		for(int j = start; j < (start + verticesCount - 1); ++j)
		{
			const Shapes::DVertex& v1 = shape->GetVertex(j);
			const Shapes::DVertex& v2 = shape->GetVertex(j + 1);
			DrawSegment(dc, rect, v1, v2);		
		}
		const Shapes::DVertex& v1 = shape->GetVertex(start + verticesCount - 1);
		const Shapes::DVertex& v2 = shape->GetVertex(start);
		DrawSegment(dc, rect, v1, v2);		

		start += verticesCount;
	}
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::DrawPolyline(CPaintDC* dc, CRect& rect, Shapes::IShape* shape)
{
	int partCount = shape->GetPartCount();
	int start = 0;
	for(int i = 0; i < partCount; ++i)
	{
		int verticesCount = shape->GetPartSize(i);

		for(int j = start; j < (start + verticesCount - 1); ++j)
		{
			const Shapes::DVertex& v1 = shape->GetVertex(j);
			const Shapes::DVertex& v2 = shape->GetVertex(j + 1);
			DrawSegment(dc, rect, v1, v2);
		}
		start += verticesCount;
	}
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::DrawPoints(CPaintDC* dc, CRect& rect, Shapes::IShape* shape)
{
	int verticesCount = shape->GetVertexCount();
	for(int k = 0; k < verticesCount; ++k)
	{
		const Shapes::DVertex& v = shape->GetVertex(k);
		if(m_ViewExtents.Contains(v))
		{
			int x = static_cast<int>(m_DrawParams.picMedX + (v.x - m_DrawParams.viewMedX) / m_DrawParams.ratio);
			int y = static_cast<int>(m_DrawParams.picMedY + (m_DrawParams.viewMedY - v.y) / m_DrawParams.ratio);
			dc->Arc(x - 2, y + 2, x + 2, y - 2, x + 2, y, x + 2, y);
		}
	}
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::DrawSegment(CPaintDC* dc, CRect& rect, const Shapes::DVertex& v1, const Shapes::DVertex& v2)
{
	int x1 = static_cast<int>(m_DrawParams.picMedX + (v1.x - m_DrawParams.viewMedX) / m_DrawParams.ratio);
	int y1 = static_cast<int>(m_DrawParams.picMedY + (m_DrawParams.viewMedY - v1.y) / m_DrawParams.ratio);
	int x2 = static_cast<int>(m_DrawParams.picMedX + (v2.x - m_DrawParams.viewMedX) / m_DrawParams.ratio);
	int y2 = static_cast<int>(m_DrawParams.picMedY + (m_DrawParams.viewMedY - v2.y) / m_DrawParams.ratio);

	Shapes::DVertex vv1(x1, y1);
	Shapes::DVertex vv2(x2, y2);

	LBRectangle rRect(rect.left + 1, rect.bottom - 1, rect.right - 1, rect.top + 1);

	rRect.ClipSegment(&vv1, &vv2);
	
	if(!vv1.EqualXY(vv2))
	{
		dc->MoveTo(vv1.x, vv1.y);
		dc->LineTo(vv2.x, vv2.y);
	}
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::DrawModulesPreview(CPaintDC* dc, CRect& rect, bool calculate)
{
	BeginWaitCursor();

	INT_PTR moduleCount = m_ModuleList->GetCount();
	{
		for(int i = 0; i < moduleCount; ++i)
		{
			LBModule* module = m_ModuleList->GetAt(i);

			if(module->GetHasPreview())
			{
				if(module->GetUseShapefile())
				{
					if(module->IsActive() || m_ShowAll)
					{
						Shapes::ShapeList& shapeList = (*m_ShapeOrganizerList)[i]->GetShapeList();
						Shapes::ShapeDatabase& shDB = (*m_ShapeOrganizerList)[i]->GetShapeDatabase();
						reinterpret_cast<CPreviewDlg*>(GetParent())->ShowApplyingModuleMessage(module);
						module->Preview(shapeList, shDB, dc, rect, m_DrawParams, calculate);
					}
				}
			}
		}
		reinterpret_cast<CPreviewDlg*>(GetParent())->ShowApplyingModuleMessage(NULL);
	}

	EndWaitCursor();
}

// -------------------------------------------------------------------------- //
// EVENT HANDLERS                                                             //
// -------------------------------------------------------------------------- //

void CDrawModulesStatic::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	CRect rect;
	GetClientRect(rect);

	if(!m_ChangedSelectedShape)
	{
		dc.FillSolidRect(rect, RGB(255, 255, 255));
	}

	Shapes::DVertex viewBarycenter = m_ViewExtents.GetBarycenter();

	m_DrawParams.picMedX  = rect.right * 0.5;
	m_DrawParams.picMedY  = rect.bottom * 0.5;
	m_DrawParams.viewMedX = viewBarycenter.x;
	m_DrawParams.viewMedY = viewBarycenter.y;

	if(!m_ChangedSelectedShape)
	{
		if(m_ShowModulePreview)
		{
			if(!m_PanningProgressing)
			{
				DrawModulesPreview(&dc, rect, m_CalculateModules);
			}
		}

		DrawShapes(&dc, rect);
	}
	DrawSelectedShape(&dc, rect, false);
	DrawSelectedShape(&dc, rect, true);

	// set the variable to false to untrigger module calculation
	m_CalculateModules = false;
	m_ChangedSelectedShape = false;
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::OnSize(UINT nType, int cx, int cy)
{
	CStatic::OnSize(nType, cx, cy);

	if(m_DrawParams.zoomFactor == 1.0)
	{
		SetViewExtents(false);
	}
	else
	{
		CRect rect;
		GetClientRect(rect);
		
		if(rect.Width() * m_DrawParams.ratio > m_WorldExtents.Width() &&
		   rect.Height() * m_DrawParams.ratio > m_WorldExtents.Height())
		{
			SetViewExtents(false);
		}
		else
		{
			SetViewExtents(true);
		}
	}
}

// -------------------------------------------------------------------------- //
// MOUSE EVENTS HANDLERS                                                      //
// -------------------------------------------------------------------------- //

void CDrawModulesStatic::OnLButtonDown(UINT nFlags, CPoint point)
{
	if(m_Panning)
	{
		::SetCursor(m_HandClose);
		SetCapture();
		m_PanStartPoint = point;
		m_PanStartPoint += CPoint(-20, -20);
		m_PanningProgressing = true;
		m_PanStartViewExtents = m_ViewExtents;
	}
	if(m_ZoomingWindow)
	{
		SetCapture();
		m_ZoomWindowStartPoint  = point;
		m_ZoomWindowStartPoint  += CPoint(-20, -20);
		m_ZoomWindowOldEndPoint = point;
		m_ZoomWindowOldEndPoint += CPoint(-20, -20);
		m_ZoomingWindowProgressing = true;
	}

	CStatic::OnLButtonDown(nFlags, point);
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::OnLButtonUp(UINT nFlags, CPoint point)
{
	if(m_Panning)
	{
		if(m_PanningProgressing)
		{
			if(GetCapture() == this) ReleaseCapture();
			m_PanningProgressing = false;
			Invalidate();
		}
	}

	if(m_ZoomingWindow)
	{
		if(m_ZoomingWindowProgressing)
		{
			if(GetCapture() == this) ReleaseCapture();

			int picDx = point.x - m_ZoomWindowStartPoint.x;
			int picDy = point.y - m_ZoomWindowStartPoint.y;

			if(picDx != 0 && picDy != 0)
			{
				CRect rect;
				GetClientRect(rect);

				double ratioX = static_cast<double>(picDx) / static_cast<double>(rect.Width());
				double ratioY = static_cast<double>(picDy) / static_cast<double>(rect.Height());
				double ratio = max(ratioX, ratioY);
				m_DrawParams.zoomFactor /= ratio;

				double picMedX = m_ZoomWindowStartPoint.x + picDx * 0.5;
				double picMedY = m_ZoomWindowStartPoint.y + picDy * 0.5;

				double viewMedX = (picMedX - m_DrawParams.picMedX) * m_DrawParams.ratio + m_DrawParams.viewMedX;
				double viewMedY = (m_DrawParams.picMedY - picMedY) * m_DrawParams.ratio + m_DrawParams.viewMedY;

				double viewDx = picDx * m_DrawParams.ratio;
				double viewDy = picDy * m_DrawParams.ratio;

				m_ViewExtents.Set(viewMedX - viewDx * 0.5, viewMedY + viewDy * 0.5,
					              viewMedX + viewDx * 0.5, viewMedY - viewDy * 0.5);
				SetViewExtents(false);
			}

			Invalidate();
			// simulates the pression on Zoom Window button
			reinterpret_cast<CPreviewDlg*>(GetParent())->OnZoomWindow();
		}
	}

	CStatic::OnLButtonUp(nFlags, point);
}

// -------------------------------------------------------------------------- //

void CDrawModulesStatic::OnMouseMove(UINT nFlags, CPoint point)
{
	if((nFlags & MK_LBUTTON) == MK_LBUTTON)
	{
		if(GetCapture() == this)
		{
			if(m_Panning)
			{
				if(m_PanningProgressing)
				{
					double dx = (point.x - m_PanStartPoint.x) * m_DrawParams.ratio;
					double dy = (point.y - m_PanStartPoint.y) * m_DrawParams.ratio;

					double newLeft   = m_PanStartViewExtents.Left() - dx;
					double newRight  = m_PanStartViewExtents.Right() - dx;
					double newTop    = m_PanStartViewExtents.Top() + dy;
					double newBottom = m_PanStartViewExtents.Bottom() + dy;

					if(newLeft < m_WorldExtents.Left())
					{
						double diff = m_WorldExtents.Left() - newLeft;
						newLeft  += diff;
						newRight += diff;
					}
					if(newRight > m_WorldExtents.Right())
					{
						double diff = newRight - m_WorldExtents.Right();
						newLeft  -= diff;
						newRight -= diff;
					}
					if(newBottom < m_WorldExtents.Bottom())
					{
						double diff = m_WorldExtents.Bottom() - newBottom;
						newBottom += diff;
						newTop    += diff;
					}
					if(newTop > m_WorldExtents.Top())
					{
						double diff = newTop - m_WorldExtents.Top();
						newBottom -= diff;
						newTop    -= diff;
					}

					if(newLeft != m_ViewExtents.Left() || 
					   newTop != m_ViewExtents.Top() ||
					   newRight != m_ViewExtents.Right() ||
					   newBottom != m_ViewExtents.Bottom())
					{
						m_ViewExtents.Set(newLeft, newTop, newRight, newBottom);

						Invalidate();
						UpdateWindow();
					}
				}
			}
			if(m_ZoomingWindow)
			{
				if(m_ZoomingWindowProgressing)
				{
					CClientDC dc(this);
					CPen pen;
					CPen* oldPen;
					CBrush* oldBrush;
					int mode;
					{
						pen.CreatePen(PS_DOT, 1, RGB(0, 0, 0));
						oldPen = dc.SelectObject(&pen);
						oldBrush = (CBrush*)dc.SelectStockObject(NULL_BRUSH);
						mode = dc.SetROP2(R2_XORPEN);
						dc.Rectangle(m_ZoomWindowStartPoint.x, m_ZoomWindowStartPoint.y, 
							         m_ZoomWindowOldEndPoint.x, m_ZoomWindowOldEndPoint.y);
						m_ZoomWindowOldEndPoint = point;
						dc.Rectangle(m_ZoomWindowStartPoint.x, m_ZoomWindowStartPoint.y, 
							         m_ZoomWindowOldEndPoint.x, m_ZoomWindowOldEndPoint.y);
						dc.SetROP2(mode);
						dc.SelectObject(oldPen);
						dc.SelectObject(oldBrush);
					}
				}
			}
		}
	}
	CStatic::OnMouseMove(nFlags, point);
}

// -------------------------------------------------------------------------- //
// CURSOR HANDLERS                                                            //
// -------------------------------------------------------------------------- //

BOOL CDrawModulesStatic::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	if(m_Panning)
	{
		::SetCursor(m_HandOpen);
		return TRUE;
	}
	if(m_ZoomingWindow)
	{
		::SetCursor(m_ZoomWindow);
		return TRUE;
	}
	return CStatic::OnSetCursor(pWnd, nHitTest, message);
}

