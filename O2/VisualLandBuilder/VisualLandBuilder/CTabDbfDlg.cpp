// CTabDbfDlg.cpp : implementation file
//

#include "stdafx.h"

#include ".\VisualLandBuilder.h"
#include ".\CTabDbfDlg.h"
#include ".\CAddEditModuleDlg.h"
#include "..\..\LandBuilder\Shapes\ShapeOrganizer.h"
#include ".\LBDbFieldsFunctor.h"

// CTabDbfDlg dialog

IMPLEMENT_DYNAMIC(CTabDbfDlg, CDialog)

// -------------------------------------------------------------------------- //

CTabDbfDlg::CTabDbfDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTabDbfDlg::IDD, pParent)
	, m_Modified(false)
{
}

// -------------------------------------------------------------------------- //

CTabDbfDlg::~CTabDbfDlg()
{
}

// -------------------------------------------------------------------------- //

void CTabDbfDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TABDBFPARLIST, m_DbfParListCtrl);
	DDX_Control(pDX, IDC_TABDBFDESCRIPTION, m_TabDbfDescriptionCtrl);
}

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CTabDbfDlg, CDialog)
	ON_NOTIFY(NM_CLICK, IDC_TABDBFPARLIST, &CTabDbfDlg::OnNMClickTabDbfParList)
	ON_NOTIFY(LVN_KEYDOWN, IDC_TABDBFPARLIST, &CTabDbfDlg::OnLvnKeydownTabDbfParList)
	ON_BN_CLICKED(IDC_TABDBFCLEARBTN, &CTabDbfDlg::OnBnClickedTabDbfClearBtn)
	ON_BN_CLICKED(IDC_TABDBFTOGLOBALBTN, &CTabDbfDlg::OnBnClickedTabDbfToGlobalBtn)
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //

BOOL CTabDbfDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();

	// sets the object list column header
	m_DbfParListCtrl.InsertColumn(0, string(HEAD_PARAMETER).c_str(), LVCFMT_LEFT);
	m_DbfParListCtrl.SetColumnWidth(0, 160);
	m_DbfParListCtrl.InsertColumn(1, string(HEAD_DBFFIELD).c_str(), LVCFMT_LEFT);
	m_DbfParListCtrl.SetColumnWidth(1, 160);

	// sets the object list style
	m_DbfParListCtrl.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	// sets the in-place editing object for the list 
	m_DbfParListCtrl.SetEditType(ET_COMBO);

	return TRUE;
}

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //

void CTabDbfDlg::UpdateState()
{
	// gets the index of the selected item in the list if any
	int selItem = m_DbfParListCtrl.GetNextItem(-1, LVNI_SELECTED);

	// clears the list items
	m_DbfParListCtrl.DeleteAllItems();

	// enables/disables controls if there are global parameters
	if(m_pModule->GetDbfParametersList().IsEmpty())
	{
		GetDlgItem(IDC_TABDBFGROUP)->EnableWindow(FALSE);
		GetDlgItem(IDC_TABDBFPARLIST)->EnableWindow(FALSE);
		GetDlgItem(IDC_TABDBFMANDATORY)->EnableWindow(FALSE);
		GetDlgItem(IDC_TABDBFDESCRGROUP)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_TABDBFGROUP)->EnableWindow(TRUE);
		GetDlgItem(IDC_TABDBFPARLIST)->EnableWindow(TRUE);
		GetDlgItem(IDC_TABDBFMANDATORY)->EnableWindow(TRUE);
		GetDlgItem(IDC_TABDBFDESCRGROUP)->EnableWindow(TRUE);
	}

	// updates the items
	INT_PTR dbfParamCount = m_pModule->GetDbfParametersList().GetCount();
	for(int i = 0; i < dbfParamCount; ++i)
	{
		LBParameter* item = m_pModule->GetDbfParametersList().GetAt(i);
		m_DbfParListCtrl.InsertItem(i, item->GetName().c_str());
		// if the parameter has no default, show it bolded
		if(!item->GetHasDefault())
		{
			m_DbfParListCtrl.SetItemStyle(i, 0, LIS_BOLD, true);
		}
		m_DbfParListCtrl.SetItem(i, 1, LVIF_TEXT, item->GetValue().c_str(), 0, 0, 0, 0);
	}

	if(selItem != -1)
	{
		m_DbfParListCtrl.EnsureVisible(selItem, FALSE);
		m_DbfParListCtrl.SetItemState(selItem, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
	}

	m_Modified = true;

	// disables toglobal button
	GetDlgItem(IDC_TABDBFTOGLOBALBTN)->EnableWindow(FALSE);

	// enables/disables clear and to button
	if(!m_pModule->GetDbfParametersList().IsEmpty() && HasData())
	{
		GetDlgItem(IDC_TABDBFCLEARBTN)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_TABDBFCLEARBTN)->EnableWindow(FALSE);
	}

	// clears the description
	m_TabDbfDescriptionCtrl.SetWindowTextA("");

	// setups the editing combo data
	m_DbfParListCtrl.SetComboItems(m_ItemListCombo);

	// disables the module list if there is at least one object
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateModuleListCtrlState();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateClearAllDataBtnState();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->CheckOkButtonEnabling();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateDbfTabImage();
}

// -------------------------------------------------------------------------- //

bool CTabDbfDlg::GetModified() const
{
	return m_Modified;
}

// -------------------------------------------------------------------------- //

bool CTabDbfDlg::HasData() const
{
	return m_pModule->GetDbfParametersList().HasSomeValue();
}

// -------------------------------------------------------------------------- //

bool CTabDbfDlg::Validate() const
{
	return m_pModule->GetDbfParametersList().Validate();
}

// -------------------------------------------------------------------------- //

void CTabDbfDlg::SetModule(LBModule* module)
{
	m_pModule = module;
	// loads fields from dbf file
	GetDbfFields();
}

// -------------------------------------------------------------------------- //

BOOL CTabDbfDlg::GetClearButtonState() const
{
	return GetDlgItem(IDC_TABDBFCLEARBTN)->IsWindowEnabled();	
}

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

void CTabDbfDlg::GetDbfFields()
{
	// setups the editing combo data
	string name = m_pModule->GetShapeFileName();
	if(name != "" && !(GetFileAttributes(name.c_str()) == INVALID_FILE_ATTRIBUTES))
	{
		BeginWaitCursor();

		// gets fields from dbf file
		Shapes::ShapeDatabase& shDB = reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->GetShapeDatabase();
		LBDbFieldsFunctor functor;
		shDB.ForEachField(functor);
		m_ItemListCombo = functor.GetFieldsList();
		m_DbfParListCtrl.SetComboItems(m_ItemListCombo);

		EndWaitCursor();
	}
	else
	{
		m_pModule->ClearAllDbfParameters();
		m_ItemListCombo.clear();
	}
}

// -------------------------------------------------------------------------- //

int CTabDbfDlg::GetDbfFieldsCount()
{
	return static_cast<int>(m_ItemListCombo.size());
}

// -------------------------------------------------------------------------- //

bool CTabDbfDlg::DataMatchFields()
{
	INT_PTR dbfParamCount = m_pModule->GetDbfParametersList().GetCount();
	int dbfFieldsCount = static_cast<int>(m_ItemListCombo.size());
	for(int i = 0; i < dbfParamCount; ++i)
	{
		string data = m_pModule->GetDbfParametersList().GetAt(i)->GetValue();
		bool found = false;
		for(int j = 0; j < dbfFieldsCount; ++j)
		{
			if(data == m_ItemListCombo[j])
			{
				found = true;
				break;
			}
		}
		if(m_pModule->GetDbfParametersList().GetAt(i)->GetHasDefault() && data =="")
		{
			found = true;
		}
		if(!found)
		{
			return false;
		}
	}
	return true;
}

// -------------------------------------------------------------------------- //
// OBJECT LIST EVENT HANDLERS                                                 //
// -------------------------------------------------------------------------- //

void CTabDbfDlg::OnNMClickTabDbfParList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// gets the index of the selected item in the list
	int selItem = m_DbfParListCtrl.GetNextItem(-1, LVNI_SELECTED);

	// enables/disables the to global button
	if(selItem != -1)
	{
		// something is selected
		if(m_pModule->GetDbfParametersList().GetAt(selItem)->IsMovable())
		{
			GetDlgItem(IDC_TABDBFTOGLOBALBTN)->EnableWindow(TRUE);
		}
		else
		{
			GetDlgItem(IDC_TABDBFTOGLOBALBTN)->EnableWindow(FALSE);
		}
		m_TabDbfDescriptionCtrl.SetWindowTextA(m_pModule->GetDbfParametersList().GetAt(selItem)->GetDescription().c_str());
	}
	else
	{
		// no selection
		GetDlgItem(IDC_TABDBFTOGLOBALBTN)->EnableWindow(FALSE);
		m_TabDbfDescriptionCtrl.SetWindowTextA("");
	}

	*pResult = 0;
}

// -------------------------------------------------------------------------- //

void CTabDbfDlg::OnLvnKeydownTabDbfParList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);

	OnNMClickTabDbfParList(pNMHDR, pResult);

	*pResult = 0;
}

// -------------------------------------------------------------------------- //
// OBJECT LIST BUTTONS HANDLERS                                               //
// -------------------------------------------------------------------------- //

void CTabDbfDlg::OnBnClickedTabDbfClearBtn()
{
	string text = MSG_CONFIRMCLEARDBFPAR;
	if(MessageBox(text.c_str(), "", MB_YESNO | MB_ICONQUESTION) == IDYES)
	{
		m_pModule->ClearAllDbfParameters();
		UpdateState();
	}
}

// -------------------------------------------------------------------------- //

void CTabDbfDlg::OnBnClickedTabDbfToGlobalBtn()
{
	// gets the index of the selected item in the list
	int selItem = m_DbfParListCtrl.GetNextItem(-1, LVNI_SELECTED);

	LBParameter* par = m_pModule->GetDbfParametersList().GetAt(selItem);

	// move the parameter from one list to the other
	par->ClearValue();
	// passes as value to create a copy of the data
	m_pModule->GetGlobalParametersList().Append(*par);
	m_pModule->GetDbfParametersList().RemoveAt(selItem);
	UpdateState();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateGlobalTabDialog();
}

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

BOOL CTabDbfDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == MY_ENDEDITMSG)
	{
		INT_PTR dbfParamCount = m_pModule->GetDbfParametersList().GetCount();
		for(int i = 0; i < dbfParamCount; ++i)
		{
			CString cData = m_DbfParListCtrl.GetItemText(i, 1);
			string data = cData;
			m_pModule->GetDbfParametersList().GetAt(i)->SetValue(data);
		}
		UpdateState();
	}
	// this is needed to prevent the dialog to hide when ENTER or ESC is pressed
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}


