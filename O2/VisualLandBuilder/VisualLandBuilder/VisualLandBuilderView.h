//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

class CVisualLandBuilderView : public CListView
{
protected: // create from serialization only
	CVisualLandBuilderView();

	DECLARE_DYNCREATE( CVisualLandBuilderView )

public:
	CVisualLandBuilderDoc* GetDocument() const;

public:

public:
	virtual BOOL PreCreateWindow( CREATESTRUCT& cs );

protected:
	virtual void OnInitialUpdate(); // called first time after construct

// Implementation
public:
	virtual ~CVisualLandBuilderView();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump( CDumpContext& dc ) const;
#endif

private:
	// this variable is used to correctly trigger
	// the click on the check boxes of the list
	bool m_updateFromCheck;

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

protected:
	virtual void OnUpdate( CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/ );

private:
	// module menu
	afx_msg void OnUpdateLandBuilderPreview( CCmdUI* cmdUI );
	afx_msg void OnUpdateLandBuilderRunLandBuilder2( CCmdUI* cmdUI );
	afx_msg void OnUpdateModuleEditModule( CCmdUI* cmdUI );
	afx_msg void OnUpdateModuleRemoveModule( CCmdUI* cmdUI );
	afx_msg void OnUpdateModuleMoveDownModule( CCmdUI* cmdUI );
	afx_msg void OnUpdateModuleMoveUpModule( CCmdUI* cmdUI );
	afx_msg void OnUpdateModuleRemoveAllModules( CCmdUI* cmdUI );
	afx_msg void OnUpdateModuleCheckModule( CCmdUI* cmdUI );
	afx_msg void OnUpdateModuleCheckAllModules( CCmdUI* cmdUI );
	afx_msg void OnUpdateModuleUncheckAllModules( CCmdUI* cmdUI );
	afx_msg void OnUpdateModuleDuplicateModule( CCmdUI* cmdUI );

private:
	afx_msg void OnNMClick( NMHDR* nmhdr, LRESULT* result );
	afx_msg void OnLvnItemActivate( NMHDR* nmhdr, LRESULT* result );
	afx_msg void OnLvnKeydown( NMHDR* nmhdr, LRESULT* result );
	afx_msg void OnLvnItemChanged( NMHDR* nmhdr, LRESULT* result );
};

//-----------------------------------------------------------------------------

#ifndef _DEBUG  // debug version in VisualLandBuilderView.cpp
inline CVisualLandBuilderDoc* CVisualLandBuilderView::GetDocument() const
{ 
  return (reinterpret_cast< CVisualLandBuilderDoc* >(m_pDocument)); 
}
#endif

//-----------------------------------------------------------------------------
