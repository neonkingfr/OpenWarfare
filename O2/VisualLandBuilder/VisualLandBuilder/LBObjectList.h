#pragma once

#include <string>

#include ".\LBObject.h"

using std::string;

class LBObjectList : public CObject
{
	DECLARE_SERIAL(LBObjectList)

	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// the version
	int m_Version;
	// the list of parameters
	CTypedPtrList<CObList, LBObject*> m_List;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************************************************************** //
	// Constructors                                                   //
	// ************************************************************** //

	// Default constructor
	LBObjectList();

	// copy constructor
	LBObjectList(const LBObjectList& other);

	// ************************************************************** //
	// Destructor                                                     //
	// ************************************************************** //
	virtual ~LBObjectList();

	// ************************************************************** //
	// Assignement                                                    //
	// ************************************************************** //
	LBObjectList& operator = (const LBObjectList& other);

	// ************************************************************** //
	// Member variables getters                                       //
	// ************************************************************** //

	// GetList()
	// returns the list
	CTypedPtrList<CObList, LBObject*>& GetList();

	// ************************************************************** //
	// Member variables setters                                       //
	// ************************************************************** //

	// SetList()
	// sets the list with the given one
	void SetList(CTypedPtrList<CObList, LBObject*>& newList);

	// ************************************************************** //
	// Member variables manipulators                                  //
	// ************************************************************** //
	
	// Append()
	// adds a copy of the the given object at the end of this list
	void Append(const LBObject& object);
	void Append(LBObject* object);

	// GetAt()
	// returns the object of this list at the given index 
	LBObject* GetAt(int index);

	// SetAt()
	// sets the object of this list at the given index with the given object
	void SetAt(int index, LBObject* object);

	// RemoveAt()
	// removes the object at the given index from this list
	void RemoveAt(int index);

	// RemoveAll()
	// removes all the objects from this list
	void RemoveAll();

	// ************************************************************** //
	// Interface                                                      //
	// ************************************************************** //

	// GetCount()
	// returns the number of items in this list
	INT_PTR GetCount();

	// IsEmpty()
	// returns true if the list is empty
	BOOL IsEmpty();

	// Validate()
	// returns the result of data validation of the objects of this list
	LBErrorTypes Validate();
	// GetFirstFreeDbfIndex()
	// returns the first free index for data retriewed from dbf
	int GetFirstFreeDbfIndex();

	// ************************************************************** //
	// Serialization                                                  //
	// ************************************************************** //
	virtual void Serialize(CArchive& ar);
};
