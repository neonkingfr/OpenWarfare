//-----------------------------------------------------------------------------

#include "stdafx.h"
#include "VisualLandBuilder.h"
#include "CRunLandBuilder2Dlg.h"

//-----------------------------------------------------------------------------

IMPLEMENT_DYNAMIC( CRunLandBuilder2Dlg, CDialog )

//-----------------------------------------------------------------------------

CRunLandBuilder2Dlg::CRunLandBuilder2Dlg( CWnd* parent )
: CDialog( CRunLandBuilder2Dlg::IDD, parent )
{
}

//-----------------------------------------------------------------------------

CRunLandBuilder2Dlg::~CRunLandBuilder2Dlg()
{
}

//-----------------------------------------------------------------------------

void CRunLandBuilder2Dlg::DoDataExchange( CDataExchange* dx )
{
	CDialog::DoDataExchange( dx );
	DDX_Control( dx, IDC_EDIT1, m_runNameCtrl );
}

//-----------------------------------------------------------------------------

BEGIN_MESSAGE_MAP( CRunLandBuilder2Dlg, CDialog )
	ON_BN_CLICKED( IDOK, &CRunLandBuilder2Dlg::OnBnClickedOk )
	ON_EN_CHANGE( IDC_RUNLBEDIT, &CRunLandBuilder2Dlg::OnEnChangeRunLbEdit )
END_MESSAGE_MAP()

//-----------------------------------------------------------------------------
// DIALOG HANDLERS
//-----------------------------------------------------------------------------

BOOL CRunLandBuilder2Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_runNameCtrl.SetWindowTextA( m_name.c_str() );

	return (TRUE);
}

//-----------------------------------------------------------------------------
// HELPER FUNCTIONS
//-----------------------------------------------------------------------------

void CRunLandBuilder2Dlg::SetName( string* name )
{
	// sets the working copy of the data
	m_name = *name;

	// sets the pointer to the original data
	m_oldName = name;
}

//-----------------------------------------------------------------------------

bool CRunLandBuilder2Dlg::Validate()
{
  size_t size = m_name.size();
  if ( size == 0 ) { return (false); }

  for ( size_t i = 0; i < size; ++i )
  {
    if ( m_name[i] == ' ' ) { return (false); }
  }

	return (true);
}

//-----------------------------------------------------------------------------
// BUTTONS HANDLERS
//-----------------------------------------------------------------------------

void CRunLandBuilder2Dlg::OnBnClickedOk()
{
	if ( Validate() )
	{
		// updates data
		*m_oldName = m_name;

  	OnOK();
  }
}

//-----------------------------------------------------------------------------
// EDIT HANDLERS
//-----------------------------------------------------------------------------

void CRunLandBuilder2Dlg::OnEnChangeRunLbEdit()
{
	CString data;
	m_runNameCtrl.GetWindowText( data );

	m_name = data;
}

//-----------------------------------------------------------------------------
