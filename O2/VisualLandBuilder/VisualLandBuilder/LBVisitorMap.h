//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>

#include ".\LBGlobalVariables.h"
#include ".\LBParameterList.h"
#include ".\LBParameter.h"
#include ".\LBError.h"

//-----------------------------------------------------------------------------

using std::string;

//-----------------------------------------------------------------------------

class LBVisitorMap : CObject
{
	DECLARE_SERIAL( LBVisitorMap )

protected:
	// the version
	int m_version;

	// the map list of parameters
	LBParameterList m_parametersList;

public:
	// Default constructor
	LBVisitorMap();

	// copy constructor
	LBVisitorMap( const LBVisitorMap& other );

	virtual ~LBVisitorMap();

	LBVisitorMap& operator = ( const LBVisitorMap& other );
	
	// GetParameterList()
	// returns the parameter list of this map
	LBParameterList& GetParametersList();
	
	// SetParameterList()
	// sets the parameter list of this map with the given one
	void SetParameterList( const LBParameterList& parametersList );

	// SetParameter()
	// sets the parameter of this map at the given index with
	// the given parameter 
	void SetParameter( int index, LBParameter* parameter );

	// Reset()
	// resets this map
	void Reset();

	// SetDefault()
	// sets the default visitor map data
	void SetDefault();

	// Validate()
	// returns the result from map validation
	// LBET_NOERROR if the map can be exported to LandBuilder
	LBErrorTypes Validate();

	double GetMapLeft() const;
	double GetMapTop() const;
	double GetMapRight() const;
	double GetMapBottom() const;
	double GetMapWidth() const;
	double GetMapHeight() const;
	double GetGridX() const;
	double GetGridY() const;
	int    GetVisitorVersion() const;

	string GetMapLeftAsString() const;
	string GetMapTopAsString() const;
	string GetMapRightAsString() const;
	string GetMapBottomAsString() const;
	string GetMapWidthAsString() const;
	string GetMapHeightAsString() const;
	string GetGridXAsString() const;
	string GetGridYAsString() const;
	string GetVisitorVersionAsString() const;

	virtual void Serialize( CArchive& ar );
};

//-----------------------------------------------------------------------------
