#pragma once

#include ".\LBModule.h"
#include ".\CDrawShapesStatic.h"
#include ".\CPreviewDlg.h"

#include "afxwin.h"

#include ".\External\ColorStatic\ColorStaticST.h"

// CTabShapefileDlg dialog

class CTabShapefileDlg : public CDialog
{
	DECLARE_DYNAMIC(CTabShapefileDlg)

public:
	CTabShapefileDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTabShapefileDlg();

// Dialog Data
	enum { IDD = IDD_TABSHAPEFILE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

// -------------------------------------------------------------------------- //

private:
	// the Shape name edit ctrl
	CEdit m_ShapeNameTextCtrl;
	// the shape picture
	CDrawShapesStatic m_ShapePictureCtrl;

	// controls for multipart shapes
	CColorStaticST m_MultipartShapesLbl;
	CColorStaticST m_MultipartShapesText;

	string m_SelectedShapeType;
	int    m_ShapesCount;
	int    m_MultipartShapesCount;

	// pointer to the current module
	LBModule* m_pModule;

	// will be set to true if the data are modified
	bool m_Modified;

	// the dialogs
	CPreviewDlg m_PreviewDlg;

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //
private:
	virtual BOOL OnInitDialog(void);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //
public:
	// updates the state of the dialog sets m_Modified if data have been changed
	void UpdateState();
	// returns true if data have been modified
	bool GetModified() const;
	// return true if there are data already inputted
	bool HasData() const;
	// sets the module
	void SetModule(LBModule* module);
	// returns the state of the clear button (true if enabled)
	BOOL GetClearButtonState() const;
	// set the color of the multipart shapes ctrl
	void SetMultipartShapesCtrlColor(bool error);

// -------------------------------------------------------------------------- //
// PICTURE HANDLERS                                                           //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnStnClickedShapePicture();

// -------------------------------------------------------------------------- //
// BUTTONS HANDLERS                                                           //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnBnClickedShapeBrowseBtn();
public:
	afx_msg void OnBnClickedTabShapeClearBtn();

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
private:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
