#pragma once

#include "..\..\LandBuilder\Shapes\Vertex.h"

class LBRectangle
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	double m_Left;
	double m_Top;
	double m_Right;
	double m_Bottom;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************************************************************** //
	// Constructors                                                   //
	// ************************************************************** //

	// Default constructor
	LBRectangle();

	// Constructs this parameter item with the given parameters
	LBRectangle(double left, double top, double right, double bottom);

	// copy constructor
	LBRectangle(const LBRectangle& other);

	// ************************************************************** //
	// Assignement                                                    //
	// ************************************************************** //
	LBRectangle& operator = (const LBRectangle& other);

	// ************************************************************** //
	// Member variables getters                                       //
	// ************************************************************** //

	// GetLeft()
	// returns the left edge of this rectangle
	double GetLeft() const;
	// Left()
	// returns the left edge of this rectangle
	double Left() const;
	// GetTop()
	// returns the top edge of this rectangle
	double GetTop() const;
	// Top()
	// returns the top edge of this rectangle
	double Top() const;
	// GetRight()
	// returns the right edge of this rectangle
	double GetRight() const;
	// Right()
	// returns the right edge of this rectangle
	double Right() const;
	// GetBottom()
	// returns the bottom edge of this rectangle
	double GetBottom() const;
	// Bottom()
	// returns the bottom edge of this rectangle
	double Bottom() const;

	// ************************************************************** //
	// Member variables setters                                       //
	// ************************************************************** //

	// Set()
	// sets the edges of this rectangle with the given values
	void Set(double newLeft, double newTop, double newRight, double newBottom);
	// SetLeft()
	// sets the left edge of this rectangle with the given value
	void SetLeft(double newLeft);
	// Left()
	// sets the left edge of this rectangle with the given value
	void Left(double newLeft);
	// SetTop()
	// sets the top edge of this rectangle with the given value
	void SetTop(double newTop);
	// Top()
	// sets the top edge of this rectangle with the given value
	void Top(double newTop);
	// SetRight()
	// sets the top right of this rectangle with the given value
	void SetRight(double newRight);
	// Right()
	// sets the top right of this rectangle with the given value
	void Right(double newRight);
	// SetBottom()
	// sets the top bottom of this rectangle with the given value
	void SetBottom(double newBottom);
	// Bottom()
	// sets the top bottom of this rectangle with the given value
	void Bottom(double newBottom);

	// ************************************************************** //
	// Member variables manipulators                                  //
	// ************************************************************** //

	// Zero()
	// sets to zero all data members of this rectangle
	void Zero();
	// Scale()
	// scales this rectangle, using the barycenter as fixed point and using
	// the given values in x and y directions
	void Scale(double scaleX, double scaleY);

	// ************************************************************** //
	// Rectangle properties                                           //
	// ************************************************************** //
	
	// IsZero()
	// returns true if all data members of this rectangle are zero
	bool IsZero() const;
	// Width()
	// returns the width of this rectangle
	double Width() const;
	// Height()
	// returns the height of this rectangle
	double Height() const;
	// AspectRatio()
	// returns the ratio height/width of this rectangle
	// returns -1.0 if width = 0
	double AspectRatio() const;
	// Contains()
	// returns true if the point having the given coordinates is inside this
	// rectangle (edge are considered inside)
	bool Contains(double x, double y) const;
	bool Contains(const Shapes::DVertex& v1) const;
	// GetBarycenter()
	// returns the barycenter of this rectangle
	Shapes::DVertex GetBarycenter() const;
	// ClipSegment()
	// clips the segment with the given ends 
	// when returns, gives the part of the segment contained in this rectangle
	// if the segment is completely extern to this rectangle when returns the 2
	// vertices coincide
	void ClipSegment(Shapes::DVertex* v1, Shapes::DVertex* v2);
	// IntersectionPoint()
	// returns the intersection point between this rectangle and the segment with the given ends
	// the first vertex must be inside the rectangle
	Shapes::DVertex* IntersectionPoint(Shapes::DVertex* v1, Shapes::DVertex* v2);
};
