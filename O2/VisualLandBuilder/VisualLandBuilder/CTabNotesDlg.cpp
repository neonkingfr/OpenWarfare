// CTabNotesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "VisualLandBuilder.h"
#include "CTabNotesDlg.h"

#include ".\CAddEditModuleDlg.h"

// CTabNotesDlg dialog

IMPLEMENT_DYNAMIC(CTabNotesDlg, CDialog)

// -------------------------------------------------------------------------- //

CTabNotesDlg::CTabNotesDlg(CWnd* pParent /*=NULL*/)
: CDialog(CTabNotesDlg::IDD, pParent)
, m_Modified(false)
{
}

// -------------------------------------------------------------------------- //

CTabNotesDlg::~CTabNotesDlg()
{
}

// -------------------------------------------------------------------------- //

void CTabNotesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TABNOTESEDIT, m_EditCtrl);
}

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CTabNotesDlg, CDialog)
	ON_BN_CLICKED(IDC_TABNOTESCLEARBTN, &CTabNotesDlg::OnBnClickedTabNotesClearBtn)
	ON_EN_CHANGE(IDC_TABNOTESEDIT, &CTabNotesDlg::OnEnChangeTabNotesEdit)
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //

BOOL CTabNotesDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();
	
	return TRUE;
}

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //

void CTabNotesDlg::UpdateState()
{
	// enables/disables the clear btn
	string notes = m_pModule->GetNotes();

	if(notes.size() == 0)
	{
		GetDlgItem(IDC_TABNOTESCLEARBTN)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_TABNOTESCLEARBTN)->EnableWindow(TRUE);
	}

	m_EditCtrl.SetWindowTextA(notes.c_str());

	// disables the module list if there is at least one object
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateModuleListCtrlState();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateClearAllDataBtnState();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->CheckOkButtonEnabling();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateNotesTabImage();
}

// -------------------------------------------------------------------------- //

bool CTabNotesDlg::GetModified() const
{
	return m_Modified;
}

// -------------------------------------------------------------------------- //

void CTabNotesDlg::SetModule(LBModule* module)
{
	m_pModule = module;
}

// -------------------------------------------------------------------------- //

BOOL CTabNotesDlg::GetClearButtonState() const
{
	return GetDlgItem(IDC_TABNOTESCLEARBTN)->IsWindowEnabled();	
}

// -------------------------------------------------------------------------- //

bool CTabNotesDlg::HasData() const
{
	return m_pModule->GetNotes().size() > 0;
}

// -------------------------------------------------------------------------- //
// EDIT CONTROL EVENT HANDLERS                                                //
// -------------------------------------------------------------------------- //

void CTabNotesDlg::OnEnChangeTabNotesEdit()
{
	CString notes;
	m_EditCtrl.GetWindowTextA(notes);
	m_pModule->SetNotes(string(notes));
	m_Modified = true;

	if(notes.GetLength() == 0)
	{
		GetDlgItem(IDC_TABNOTESCLEARBTN)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_TABNOTESCLEARBTN)->EnableWindow(TRUE);
	}
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateClearAllDataBtnState();
}

// -------------------------------------------------------------------------- //
// BUTTONS HANDLERS                                                           //
// -------------------------------------------------------------------------- //

void CTabNotesDlg::OnBnClickedTabNotesClearBtn()
{
	string text = MSG_CONFIRMCLEARNOTES;
	if(MessageBox(text.c_str(), "", MB_YESNO | MB_ICONQUESTION) == IDYES)
	{
		m_pModule->SetNotes("");
		m_Modified = true;
		UpdateState();
	}
}

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

BOOL CTabNotesDlg::PreTranslateMessage(MSG* pMsg)
{
	// this is needed to prevent the dialog to hide when ENTER or ESC is pressed
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

// -------------------------------------------------------------------------- //
