#include "stdafx.h"

#include ".\CDrawShapesStatic.h"
#include "..\..\LandBuilder\LandBuilder2\IShapeExtra.h"
#include "..\..\LandBuilder\Shapes\ShapeHelpers.h"

BEGIN_MESSAGE_MAP( CDrawShapesStatic, CStatic )
	ON_WM_PAINT()
END_MESSAGE_MAP()

CDrawShapesStatic::CDrawShapesStatic() : CStatic()
{
	m_ShapeList = NULL;
}

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //

void CDrawShapesStatic::SetShapeList( Shapes::ShapeList* shapeList )
{
	m_ShapeList = shapeList;
}

// -------------------------------------------------------------------------- //
// EVENT HANDLERS                                                             //
// -------------------------------------------------------------------------- //

void CDrawShapesStatic::OnPaint()
{
	CPaintDC dc( this ); // device context for painting

	CRect rect;
	GetClientRect( rect );

	dc.FillSolidRect( rect, RGB( 255, 255, 255 ) );

	m_DrawParams.zoomFactor = 1.0;

	if ( m_ShapeList )
	{
		SetWorldExtents();
		SetViewExtents();

		Shapes::DVertex viewBarycenter = m_ViewExtents.GetBarycenter();

		m_DrawParams.picMedX  = rect.right * 0.5;
		m_DrawParams.picMedY  = rect.bottom * 0.5;
		m_DrawParams.viewMedX = viewBarycenter.x;
		m_DrawParams.viewMedY = viewBarycenter.y;

		DrawShapes( &dc, rect );
	}
}

// -------------------------------------------------------------------------- //

void CDrawShapesStatic::DrawShapes( CPaintDC* dc, CRect& rect )
{
	int shapesCount = m_ShapeList->Size();
	for ( int j = 0; j < shapesCount; ++j )
	{
		Shapes::IShape* shape = m_ShapeList->GetShape( j );
		RString type = GetShapeType( shape );
		if ( type == SHAPE_NAME_POINT || type == SHAPE_NAME_MULTIPOINT )
		{
			DrawPoints( dc, rect, shape );
		}
		else if ( type == SHAPE_NAME_POLYLINE )
		{
			DrawPolyline( dc, rect, shape );
		}
		else
		{
			DrawPolygon( dc, rect, shape );
		}
	}
}

// -------------------------------------------------------------------------- //

void CDrawShapesStatic::DrawPoints( CPaintDC* dc, CRect& rect, Shapes::IShape* shape )
{
	int verticesCount = shape->GetVertexCount();
	for(int k = 0; k < verticesCount; ++k)
	{
		const Shapes::DVertex& v = shape->GetVertex(k);
		if(m_ViewExtents.Contains(v))
		{
			int x = static_cast<int>(m_DrawParams.picMedX + (v.x - m_DrawParams.viewMedX) / m_DrawParams.ratio);
			int y = static_cast<int>(m_DrawParams.picMedY + (m_DrawParams.viewMedY - v.y) / m_DrawParams.ratio);
			dc->Arc(x - 2, y + 2, x + 2, y - 2, x + 2, y, x + 2, y);
		}
	}
}

// -------------------------------------------------------------------------- //

void CDrawShapesStatic::DrawPolygon(CPaintDC* dc, CRect& rect, Shapes::IShape* shape)
{
	int partCount = shape->GetPartCount();
	int start = 0;
	for(int i = 0; i < partCount; ++i)
	{
		int verticesCount = shape->GetPartSize(i);

		for(int j = start; j < (start + verticesCount - 1); ++j)
		{
			const Shapes::DVertex& v1 = shape->GetVertex(j);
			const Shapes::DVertex& v2 = shape->GetVertex(j + 1);
			DrawSegment(dc, rect, v1, v2);		
		}
		const Shapes::DVertex& v1 = shape->GetVertex(start + verticesCount - 1);
		const Shapes::DVertex& v2 = shape->GetVertex(start);
		DrawSegment(dc, rect, v1, v2);		

		start += verticesCount;
	}
}

// -------------------------------------------------------------------------- //

void CDrawShapesStatic::DrawPolyline(CPaintDC* dc, CRect& rect, Shapes::IShape* shape)
{
	int partCount = shape->GetPartCount();
	int start = 0;
	for(int i = 0; i < partCount; ++i)
	{
		int verticesCount = shape->GetPartSize(i);

		for(int j = start; j < (start + verticesCount - 1); ++j)
		{
			const Shapes::DVertex& v1 = shape->GetVertex(j);
			const Shapes::DVertex& v2 = shape->GetVertex(j + 1);
			DrawSegment(dc, rect, v1, v2);
		}
		start += verticesCount;
	}
}

// -------------------------------------------------------------------------- //

void CDrawShapesStatic::DrawSegment(CPaintDC* dc, CRect& rect, const Shapes::DVertex& v1, const Shapes::DVertex& v2)
{
	int x1 = static_cast<int>(m_DrawParams.picMedX + (v1.x - m_DrawParams.viewMedX) / m_DrawParams.ratio);
	int y1 = static_cast<int>(m_DrawParams.picMedY + (m_DrawParams.viewMedY - v1.y) / m_DrawParams.ratio);
	int x2 = static_cast<int>(m_DrawParams.picMedX + (v2.x - m_DrawParams.viewMedX) / m_DrawParams.ratio);
	int y2 = static_cast<int>(m_DrawParams.picMedY + (m_DrawParams.viewMedY - v2.y) / m_DrawParams.ratio);

	Shapes::DVertex vv1(x1, y1);
	Shapes::DVertex vv2(x2, y2);

	LBRectangle rRect(rect.left + 1, rect.bottom - 1, rect.right - 1, rect.top + 1);

	rRect.ClipSegment(&vv1, &vv2);
	
	if(!vv1.EqualXY(vv2))
	{
		dc->MoveTo(vv1.x, vv1.y);
		dc->LineTo(vv2.x, vv2.y);
	}
}

// -------------------------------------------------------------------------- //

void CDrawShapesStatic::SetWorldExtents()
{
	double left   = FLT_MAX;
	double right  = FLT_MAX;
	double top    = FLT_MAX;
	double bottom = FLT_MAX;

	int shapesCount = m_ShapeList->Size();
	for(int j = 0; j < shapesCount; ++j)
	{
		Shapes::IShape* shape = m_ShapeList->GetShape(j);	
		int verticesCount = shape->GetVertexCount();
		for(int k = 0; k < verticesCount; ++k)
		{
			const Shapes::DVertex& v = shape->GetVertex(k);
			if(left   == FLT_MAX || left   > v.x) left   = v.x;
			if(top    == FLT_MAX || top    < v.y) top    = v.y;
			if(right  == FLT_MAX || right  < v.x) right  = v.x;
			if(bottom == FLT_MAX || bottom > v.y) bottom = v.y;
		}		
	}

	m_WorldExtents.Set(left, top, right, bottom);
	m_ViewExtents = m_WorldExtents;
}

// -------------------------------------------------------------------------- //

void CDrawShapesStatic::SetViewExtents()
{
	CRect rect;
	GetClientRect(rect);

	double clientWidth  = rect.Width() - 4;
	double clientHeight = rect.Height() - 4;
	double worldWidth  = m_WorldExtents.Width();
	double worldHeight = m_WorldExtents.Height();

	if(clientWidth <= 0.0 || clientHeight <= 0.0)
	{
		m_ViewExtents.Zero();
		return;
	}

	if(m_ViewExtents.IsZero())
	{
		m_ViewExtents = m_WorldExtents;
	}

	double ratioW = worldWidth / clientWidth;
	double ratioH = worldHeight / clientHeight;
	if(ratioW > ratioH)
	{
		m_DrawParams.ratio = ratioW / m_DrawParams.zoomFactor;
	}
	else
	{
		m_DrawParams.ratio = ratioH / m_DrawParams.zoomFactor;
	}

	Shapes::DVertex viewBarycenter = m_ViewExtents.GetBarycenter();

	m_ViewExtents.Set(viewBarycenter.x - m_DrawParams.ratio * clientWidth * 0.5, 
					  viewBarycenter.y + m_DrawParams.ratio * clientHeight * 0.5, 
					  viewBarycenter.x + m_DrawParams.ratio * clientWidth * 0.5, 
					  viewBarycenter.y - m_DrawParams.ratio * clientHeight * 0.5);
}

