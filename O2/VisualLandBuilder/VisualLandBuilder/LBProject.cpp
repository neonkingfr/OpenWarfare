//-----------------------------------------------------------------------------

#include "stdafx.h"

#include ".\LBProject.h"
#include ".\LBHelperFunctions.h"

//-----------------------------------------------------------------------------

IMPLEMENT_SERIAL( LBProject, CObject, 1 )

//-----------------------------------------------------------------------------

LBProject::LBProject() 
: CObject()
, m_version( VERSION_NUMBER )
{
}

//-----------------------------------------------------------------------------

LBProject::LBProject( const LBProject& other ) 
: m_version( other.m_version )
, m_outputName( other.m_outputName )
, m_visitorMap( other.m_visitorMap )
, m_moduleList( other.m_moduleList )
, m_options( other.m_options )
{
}

//-----------------------------------------------------------------------------

LBProject::~LBProject()
{
}

//-----------------------------------------------------------------------------

LBProject& LBProject::operator = ( const LBProject& other )
{
	m_version       = other.m_version;
	m_outputName    = other.m_outputName;
	m_visitorMap    = other.m_visitorMap;
	m_moduleList    = other.m_moduleList;
	m_options       = other.m_options;

	return (*this);
}

//-----------------------------------------------------------------------------

int LBProject::GetVersion() const
{
	return (m_version);
}

//-----------------------------------------------------------------------------

string LBProject::GetOutputName() const
{
	return (m_outputName);
}

//-----------------------------------------------------------------------------

LBVisitorMap& LBProject::GetVisitorMap()
{
	return (m_visitorMap);
}

//-----------------------------------------------------------------------------

const LBVisitorMap& LBProject::GetVisitorMap() const
{
	return (m_visitorMap);
}

//-----------------------------------------------------------------------------

LBOptions& LBProject::GetOptions()
{
	return (m_options);
}

//-----------------------------------------------------------------------------

LBDirectoryList& LBProject::GetDirectoryList()
{
	return (m_options.GetDirectoryList());
}

//-----------------------------------------------------------------------------

LBModuleList& LBProject::GetModuleList()
{
	return (m_moduleList);
}

//-----------------------------------------------------------------------------

void LBProject::SetVersion( int version )
{
	m_version = version;
}

//-----------------------------------------------------------------------------

void LBProject::SetOutputName( const string& name )
{
	m_outputName = name;
}

//-----------------------------------------------------------------------------

void LBProject::SetVisitorMap( const LBVisitorMap& visitorMap )
{
	m_visitorMap = visitorMap;
}

//-----------------------------------------------------------------------------

void LBProject::SetOptions( const LBOptions& options )
{
	m_options = options;
}

//-----------------------------------------------------------------------------

void LBProject::SetDirectoryList( const LBDirectoryList& directoryList )
{
	m_options.SetDirectoryList( directoryList );
}

//-----------------------------------------------------------------------------

void LBProject::SetModuleList( const LBModuleList& moduleList )
{
	m_moduleList = moduleList;
}

//-----------------------------------------------------------------------------

void LBProject::AddModule( LBModule* module )
{
	m_moduleList.Append( module );
}

//-----------------------------------------------------------------------------

void LBProject::SetModule( int index, LBModule* module )
{
	m_moduleList.SetAt( index, module );
}

//-----------------------------------------------------------------------------

void LBProject::RemoveModule( int index )
{
	m_moduleList.RemoveAt( index );
}

//-----------------------------------------------------------------------------

void LBProject::RemoveAllModules()
{
	m_moduleList.RemoveAll();
}

//-----------------------------------------------------------------------------

void LBProject::SetDefaultVisitorMap()
{
	m_visitorMap.SetDefault();
}

//-----------------------------------------------------------------------------

void LBProject::SetDefaultDirectoryList( CTabDirectoriesDlg& dlg )
{
	// the default specification for the directory list
	// is coded in the CDirectoriesDlg
	LBDirectoryList& dirList = m_options.GetDirectoryList();
	dlg.SetDefaultDirectoryList( &dirList );
}

//-----------------------------------------------------------------------------

void LBProject::Reset()
{
	// clears visitor map
	m_visitorMap.Reset();
	// clears directory list
	m_options.GetDirectoryList().RemoveAll();
	// clears module list
	m_moduleList.RemoveAll();
}

//-----------------------------------------------------------------------------

LBError LBProject::ValidateModules()
{
	LBError err;
	INT_PTR modCount = m_moduleList.GetCount();
	int activeCounter = 0;
	for ( int i = 0; i < modCount; ++i )
	{
		LBModule* item = m_moduleList.GetAt( i );
		// checks only active modules
		if ( item->IsActive() )
		{
			err.SetType( static_cast< LBErrorTypes >( item->Validate( m_options.GetDirectoryList() ) ) );
			if ( err.GetType() != LBET_NOERROR )
			{
				err.SetModuleIndex( i + 1 );
				return (err);
			}
			activeCounter++;
		}
	}

	if ( activeCounter == 0 )
	// no active modules
	{
		err.SetType( LBET_NOACTIVEMODULES );
		return (err);		
	}
	
	err.SetType( LBET_NOERROR );
	return (err);
}

//-----------------------------------------------------------------------------

bool LBProject::SaveMainConfig( const string& fileName, const string& lbVersion )
{
	CStdioFile f;

	if ( !f.Open( fileName.c_str(), CFile::modeCreate | CFile::modeWrite ) )
	{
		return (false);
	}

	// gets the name from the file we are writing
	m_outputName = f.GetFileTitle();
	size_t pos = m_outputName.find( '.' );
	if ( pos != string::npos )
	{
		m_outputName = m_outputName.substr( 0, pos );
	}

	string line = "class LandBuildConfig\n";
	f.WriteString( line.c_str() );
	line = "{\n";
	f.WriteString( line.c_str() );
	line = "  class MapArea\n";
	f.WriteString( line.c_str() );
	line = "  {\n";
	f.WriteString( line.c_str() );

	// searches if there is any module exporting terrain
	bool exportsTerrain = m_moduleList.ExportsTerrain();

  line = "    left = " + m_visitorMap.GetMapLeftAsString() + ";\n";
	f.WriteString( line.c_str() );
  line = "    top = " + m_visitorMap.GetMapTopAsString() + ";\n";
	f.WriteString( line.c_str() );
  line = "    right = " + m_visitorMap.GetMapRightAsString() + ";\n";
	f.WriteString( line.c_str() );
  line = "    bottom = " + m_visitorMap.GetMapBottomAsString() + ";\n";
	f.WriteString( line.c_str() );
  line = "    visitor = " + m_visitorMap.GetVisitorVersionAsString() + ";\n";
	f.WriteString( line.c_str() );

//	INT_PTR visParamCount = m_visitorMap.GetParametersList().GetCount();
//	for ( int i = 0; i < visParamCount; ++i )
//	{
//		LBParameter* item = m_visitorMap.GetParametersList().GetAt( i );
//		if ( (item->GetName() == MAPAREA_LEFT) ||
//         (item->GetName() == MAPAREA_TOP) ||
//         (item->GetName() == MAPAREA_RIGHT) ||
//         (item->GetName() == MAPAREA_BOTTOM) )
//		{
//			string name = item->GetName();
//			int pointPos = (int)name.find_last_of( '.' );
//			name = name.substr( pointPos + 1 );
//
//			line = "    " + name + " = " + item->GetValue() + ";\n";
//			f.WriteString( line.c_str() );
//		}
//	}

	line = "  };\n";
	f.WriteString( line.c_str() );

//	// i'm still not sure if this class must be written in the cfg file
//	// of if it needed only when there are no other transformations
//	if ( LBVersion == "LB2" )
//	{
//		line = "  class ShapeArea\n";
//		f.WriteString( line.c_str() );
//		line = "  {\n";
//		f.WriteString( line.c_str() );
//		line = "    left = \"auto\";\n";
//		f.WriteString( line.c_str() );
//		line = "    top = \"auto\";\n";
//		f.WriteString( line.c_str() );
//		line = "    right = \"auto\";\n";
//		f.WriteString( line.c_str() );
//		line = "    bottom = \"auto\";\n";
//		f.WriteString( line.c_str() );
//		line = "  };\n";
//		f.WriteString( line.c_str() );
//	}

	if ( exportsTerrain )
	{
		line = "  class HighMapGrid\n";
		f.WriteString( line.c_str() );
		line = "  {\n";
		f.WriteString( line.c_str() );

    line = "    x = " + m_visitorMap.GetGridXAsString() + ";\n";
	  f.WriteString( line.c_str() );
    line = "    y = " + m_visitorMap.GetGridYAsString() + ";\n";
	  f.WriteString( line.c_str() );

//		INT_PTR visParamCount = m_visitorMap.GetParametersList().GetCount();
//		for ( int i = 0; i < visParamCount; ++i )
//		{
//			LBParameter* item = m_visitorMap.GetParametersList().GetAt( i );
//			if ( (item->GetName() == HIGHMAPGRID_X) ||
//			     (item->GetName() == HIGHMAPGRID_Y) )
//			{
//				string name = item->GetName();
//				int pointPos = (int)name.find_last_of( '.' );
//				name = name.substr( pointPos + 1 );
//				line = "    " + name + " = " + item->GetValue() + ";\n";
//				f.WriteString( line.c_str() );
//			}
//		}
	
		line = "  };\n";
		f.WriteString( line.c_str() );
	}

	line = "  TaskList[] =\n";
	f.WriteString( line.c_str() );
	line = "  {\n";
	f.WriteString( line.c_str() );

	// first detects last active module
	// this will be printed without comma
	int lastActive = m_moduleList.GetLastActiveModuleIndex();

	INT_PTR modCount = m_moduleList.GetCount(); 
	for ( int i = 0; i < modCount; ++i )
	{
		LBModule* item = m_moduleList.GetAt( i );
		if ( item->IsActive() )
		{
			char buf[81];
			sprintf_s( buf, 80, "module_%d", i + 1 );
			line = "    \"" + string( buf ) + "@";
			if ( item->GetUseShapefile() )
			{
				string shName = GetFileNameFromPath( item->GetShapeFileName() );
				shName = shName.substr( 0, shName.size() - 4 );
				line += (shName + "@" + shName);
			}
			line += "\"";

			if ( item->GetHasExtension() )
			{
				// first, completes and writes the regular module line 
				line += ",\n";
				f.WriteString( line.c_str() );
		
				// then, writes the line for the extension
				char buf[81];
				sprintf_s( buf, 80, "module_%d_ext", i + 1 );
				line = "    \"" + string( buf ) + "\"";
			}

      if ( i != lastActive ) { line += ","; }
			line += "\n";
			f.WriteString( line.c_str() );
		}
	}

	line = "  };\n";
	f.WriteString( line.c_str() );
	line = "};\n";
	f.WriteString( line.c_str() );

	f.Close();
	return (true);
}

//-----------------------------------------------------------------------------

bool LBProject::SaveTaskConfigs()
{
	string path = m_options.GetDirectoryList().GetWorkingFolder();

  if ( path.size() == 0 ) { return (false); }

	INT_PTR modCount = m_moduleList.GetCount();
	for ( int i = 0; i < modCount; ++i )
	{
		LBModule* item = m_moduleList.GetAt( i );
		if ( item->IsActive() )
		{
			char buf[81];
			sprintf_s( buf, 80, "module_%d", i + 1 );
			string filename = string( buf );
			bool res = item->SaveTaskConfig( filename, path );
      if ( !res ) { return (res); }

			if ( item->GetHasExtension() )
			{
				bool res = item->SaveExtensionTaskConfig( filename, path );
        if ( !res ) { return (res); }
			}
		}	
	}
	return (true);
}

//-----------------------------------------------------------------------------

void LBProject::Serialize( CArchive& ar )
{
	CString name;

	CObject::Serialize( ar );

	m_visitorMap.Serialize( ar );
	m_options.Serialize( ar );
	m_moduleList.Serialize( ar );

	if ( ar.IsStoring() )
	{
		// gets the name from the file we are writing
		CFile* f = ar.GetFile();
		m_outputName = f->GetFileTitle();
		size_t pos = m_outputName.find( '.' );
		if ( pos != string::npos )
		{
			m_outputName = m_outputName.substr( 0, pos );
		}

		name = CString( m_outputName.c_str() );
		ar << m_version;
		ar << name;
	}
	else
	{
		ar >> m_version;
		ar >> name;
		m_outputName = name;
	}
}

//-----------------------------------------------------------------------------
