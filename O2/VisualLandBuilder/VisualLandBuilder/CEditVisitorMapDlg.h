//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include ".\LBVisitorMap.h"
#include ".\ExtendedControls\CListCtrlEx.h"
#include ".\CEditParameterDlg.h"
#include "afxwin.h"

//-----------------------------------------------------------------------------

class CEditVisitorMapDlg : public CDialog
{
	DECLARE_DYNAMIC( CEditVisitorMapDlg )

public:
	CEditVisitorMapDlg( CWnd* parent = NULL );   // standard constructor
	virtual ~CEditVisitorMapDlg();

// Dialog Data
	enum 
  { 
    IDD = IDD_EDITVISITORMAP 
  };

protected:
	virtual void DoDataExchange( CDataExchange* dx );    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	// the list control
	CListCtrlEx m_parameterListCtrl;
	// the description control
	CStatic m_visMapDescriptionCtrl;
	// will be set to true if the data are modified
	bool m_modified;

	// DATA
	// the pointer to the original data
	LBVisitorMap* m_oldVisitorMap;
	// the working copy of the data
	LBVisitorMap m_visitorMap;

	// the dialog for edit parameters
	CEditParameterDlg m_editParameterDlg;

private:
	virtual BOOL OnInitDialog();

public:
	// updates the list control and sets m_modified if data have been changed
	void UpdateParameterListCtrl();
	// sets the data and the pointer to the original ones
	//  !! must be called before the DoModal function of this dialog !!
	void SetVisitorMap( LBVisitorMap* visitorMap );
	// ValidateParamCtrlInput()
	// verify the inputs in the parameters ctrl
	bool ValidateParamCtrlInput( const CString& data, const LBParameter* par );

private:
	afx_msg void OnLvnItemActivateVisMapParameters( NMHDR* nmhdr, LRESULT* result );
	afx_msg void OnNMClickVisMapParameters( NMHDR* nmhdr, LRESULT* result  );
	afx_msg void OnLvnKeydownVisMapParameters( NMHDR* nmhdr, LRESULT* result );

private:
	afx_msg void OnBnClickedVisMapEditBtn();

public:
	afx_msg void OnBnClickedOk();

private:
	// this function is used to intercept the end label editing in
	// the list ctrl
	virtual BOOL PreTranslateMessage( MSG* msg );
};

//-----------------------------------------------------------------------------
