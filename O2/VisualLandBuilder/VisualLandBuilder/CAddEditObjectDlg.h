#pragma once
#include "afxcmn.h"

#include ".\LBObject.h"
#include ".\LBModule.h"
#include ".\ExtendedControls\CListCtrlEx.h"
#include ".\CEditParameterDlg.h"
#include "afxwin.h"

// CAddEditObjectDlg dialog

class CAddEditObjectDlg : public CDialog
{
	DECLARE_DYNAMIC(CAddEditObjectDlg)

public:
	CAddEditObjectDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAddEditObjectDlg();

// Dialog Data
	enum { IDD = IDD_ADDEDITOBJECT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
private:
	// the name control
	CEdit m_NameCtrl;
	// the parameters list control
	CListCtrlEx m_ParametersListCtrl;
	// the dialog for add/edit parameter
	CEditParameterDlg m_EditParameterDlg;
	// the description ctrl
	CStatic m_DescriptionCtrl;
	// will be set to true if the data are modified
	bool m_Modified;

	// DATA
	// the pointer to the original data
	LBObject* m_pOldObject;
	// the working copy of the data
	LBObject m_Object;
	// the pointer to the current module
	LBModule* m_pModule;

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //
private:
	virtual BOOL OnInitDialog(void);

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
private:
	// UpdateParametersListCtrl()
	// updates the list control and sets m_Modified if data have been changed
	void UpdateParametersListCtrl();
	// ValidateParamCtrlInput()
	// verify the inputs in the parameters ctrl
	bool ValidateParamCtrlInput(CString data, LBParameter* par);
	// CheckOkButtonEnabling()
	// checks if the ok button can be enabled
	void CheckOkButtonEnabling();
public:
	// SetObject()
	// sets the working object and the pointer to the original one
	//  !! must be called before the DoModal function of this dialog !!
	void SetObject(LBObject* object);
	// SetModule()
	// sets the pointer to the current module
	//  !! must be called before the DoModal function of this dialog !!
	void SetModule(LBModule* module);

// -------------------------------------------------------------------------- //
// EDIT HANDLERS                                                              //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnEnChangeName();

// -------------------------------------------------------------------------- //
// LIST HANDLERS                                                              //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnNMClickParametersList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemActivateParametersList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnKeydownParametersList(NMHDR *pNMHDR, LRESULT *pResult);

// -------------------------------------------------------------------------- //
// BUTTONS HANDLERS                                                              //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnBnClickedEdit();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedObjectFromDbf();
	afx_msg void OnBnClickedMinusBtn();
	afx_msg void OnBnClickedPlusBtn();

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
private:
	// this function is used to intercept the end label editing in
	// the list ctrl
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
