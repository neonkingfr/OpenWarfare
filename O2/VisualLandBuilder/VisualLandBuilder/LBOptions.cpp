#include "stdafx.h"

#include ".\LBOptions.h"
#include ".\LBHelperFunctions.h"

IMPLEMENT_SERIAL(LBOptions, CObject, 1)

// ************************************************************** //
// Constructors                                                   //
// ************************************************************** //

LBOptions::LBOptions() 
: CObject()
, m_Version(VERSION_NUMBER)
{
}

// -------------------------------------------------------------------------- //

LBOptions::LBOptions(const LBOptions& other) 
: m_Version(other.m_Version)
, m_DirectoryList(other.m_DirectoryList)
{
}

// ************************************************************** //
// Destructor                                                     //
// ************************************************************** //

LBOptions::~LBOptions()
{
}

// ************************************************************** //
// Assignement                                                    //
// ************************************************************** //

LBOptions& LBOptions::operator = (const LBOptions& other)
{
	m_Version       = other.m_Version;
	m_DirectoryList = other.m_DirectoryList;

	return *this;
}

// ************************************************************** //
// Member variables getters                                       //
// ************************************************************** //

LBDirectoryList& LBOptions::GetDirectoryList()
{
	return m_DirectoryList;
}

// ************************************************************** //
// Member variables setters                                       //
// ************************************************************** //

void LBOptions::SetDirectoryList(const LBDirectoryList& directoryList)
{
	m_DirectoryList = directoryList;
}

// ************************************************************** //
// Member variables manipulators                                  //
// ************************************************************** //

void LBOptions::SetDefaultDirectoryList(CTabDirectoriesDlg& dlg)
{
	// the default specification for the directory list
	// is coded in the CDirectoriesDlg
	dlg.SetDefaultDirectoryList(&m_DirectoryList);
}

// ************************************************************** //
// Serialization                                                  //
// ************************************************************** //

void LBOptions::Serialize(CArchive& ar)
{
	CObject::Serialize(ar);

	m_DirectoryList.Serialize(ar);

	if(ar.IsStoring())
	{
		ar << m_Version;
	}
	else
	{
		ar >> m_Version;
	}
}
