//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include ".\LBModule.h"

#include ".\CAddEditDemDlg.h"

#include "afxwin.h"
#include "afxcmn.h"

//-----------------------------------------------------------------------------

class CTabDemsDlg : public CDialog
{
	DECLARE_DYNAMIC( CTabDemsDlg )

public:
	CTabDemsDlg( CWnd* parent = NULL );   // standard constructor
	virtual ~CTabDemsDlg();

// Dialog Data
	enum 
  { 
    IDD = IDD_TABDEMS 
  };

protected:
	virtual void DoDataExchange( CDataExchange* dX );    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	// the Dems list ctrl
	CListCtrl m_demsListCtrl;

	// pointer to the current module
	LBModule* m_module;

	// will be set to true if the data are modified
	bool m_modified;

	// the dialogs
	CAddEditDemDlg m_addEditDemDlg;

	// the working copy of the map data
	const LBVisitorMap* m_visitorMap;

private:
	virtual BOOL OnInitDialog();

public:
	// updates the state (enabled/disabled) of this dialog
	void UpdateState();
	// returns true if data have been modified
	bool GetModified() const;
	// return the list control item count
	int GetListItemCount() const;
	// sets the module
	void SetModule( LBModule* module );

	void SetVisitorMap( const LBVisitorMap* visitorMap );

	// returns the state of the clear button (true if enabled)
	BOOL GetClearButtonState() const;

private:
	afx_msg void OnLvnItemActivateTabDemsList( NMHDR* nmhdr, LRESULT* result );
	afx_msg void OnLvnKeydownTabDemsList( NMHDR* nmhdr, LRESULT* result );
	afx_msg void OnNMClickTabDemsList( NMHDR* nmhdr, LRESULT* result );

private:
	afx_msg void OnBnClickedTabDemsAddBtn();
	afx_msg void OnBnClickedTabDemsEditBtn();
	afx_msg void OnBnClickedTabDemsRemoveBtn();

public:
	afx_msg void OnBnClickedTabDemsClearBtn();

private:
	virtual BOOL PreTranslateMessage( MSG* msg );
};

//-----------------------------------------------------------------------------
