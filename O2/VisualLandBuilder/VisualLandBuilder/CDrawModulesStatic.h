#pragma once

#include ".\LBRectangle.h"

#include ".\LBModuleList.h"
#include "..\..\LandBuilder\Shapes\ShapeOrganizer.h"

class CDrawModulesStatic : public CStatic
{
public:
	CDrawModulesStatic();

	DECLARE_MESSAGE_MAP()

private:
	// the rectangle used to draw
	LBRectangle m_WorldExtents;
	LBRectangle m_ViewExtents;

	// variables to scale drawing
	DrawParams m_DrawParams;

private:
	// the list of modules to draw
	LBModuleList* m_ModuleList;
	// the list of associated ShapeOrganizer 
	AutoArray<pShapeOrganizer>* m_ShapeOrganizerList;

	// filters
	bool m_ShowAll;
	bool m_ShowModulePreview;
	// the index to know the selected shape
	int  m_OldSelectedModule;
	int  m_OldSelectedShape;
	int  m_SelectedModule;
	int  m_SelectedShape;
	bool m_ChangedSelectedShape;
	// to trigger the module calculation
	bool m_CalculateModules;
	// pan variables
	bool m_Panning;
	bool m_PanningProgressing;
	LBRectangle m_PanStartViewExtents;
	CPoint m_PanStartPoint;
	HCURSOR m_HandOpen;
	HCURSOR m_HandClose;
	// zoom window variables
	bool m_ZoomingWindow;
	bool m_ZoomingWindowProgressing;
	CPoint m_ZoomWindowStartPoint;
	CPoint m_ZoomWindowOldEndPoint;
	HCURSOR m_ZoomWindow;

// -------------------------------------------------------------------------- //
// INTERFACE                                                                  //
// -------------------------------------------------------------------------- //
public:
	// SetList()
	// sets the module list to be drawn
	void SetList(LBModuleList* moduleList, AutoArray<pShapeOrganizer>* shapeOrganizerList, bool showAll, bool showModulePreview);
	// SetSelectedShape()
	// sets the selected shape
	void SetSelectedShape(int moduleIndex, int shapeIndex);
	// ZoomWindow()
	// starts/ends zoom window
	void ZoomWindow();
	// Pan()
	// starts/ends pan
	void Pan();
	// ZoomPlus()
	// increases the zoom factor
	void ZoomPlus();
	// ZoomMinus()
	// decreases the zoom factor
	void ZoomMinus();
	// ZoomExt()
	// sets the zoom factor to 1.0
	void ZoomExt();
	// GetZoomFactor()
	// returns the current zoom factor
	double GetZoomFactor();
	// IsPanning()
	// returns true if the pan button has been pressed once
	bool IsPanning() const;
	// IsPanningProgressing()
	// returns true if we are panning
	bool IsPanningProgressing() const;
	// IsZoomingWindow()
	// returns true if the zoom window button has been pressed once
	bool IsZoomingWindow() const;
	// IsZoomingWindowProgressing()
	// returns true if we are zooming window
	bool IsZoomingWindowProgressing() const;

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
private:
	// SetWorldExtents()
	// calculates the bounding rectangle in world coordinate
	void SetWorldExtents();
	// SetViewExtents()
	// calculates the view rectangle (in world coordinate) starting from
	// the given rectangle and the picture dimensions
	void SetViewExtents(bool moveViewCenter);

	// drawing functions
	void DrawShapes(CPaintDC* dc, CRect& rect);
	void DrawSelectedShape(CPaintDC* dc, CRect& rect, bool highlight);
	void DrawPolygon(CPaintDC* dc, CRect& rect, Shapes::IShape* shape);
	void DrawPolyline(CPaintDC* dc, CRect& rect, Shapes::IShape* shape);
	void DrawPoints(CPaintDC* dc, CRect& rect, Shapes::IShape* shape);
	void DrawSegment(CPaintDC* dc, CRect& rect, const Shapes::DVertex& v1, const Shapes::DVertex& v2);
	void DrawModulesPreview(CPaintDC* dc, CRect& rect, bool calculate);

// -------------------------------------------------------------------------- //
// EVENT HANDLERS                                                             //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);

// -------------------------------------------------------------------------- //
// MOUSE EVENTS HANDLERS                                                      //
// -------------------------------------------------------------------------- //
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

// -------------------------------------------------------------------------- //
// CURSOR HANDLERS                                                            //
// -------------------------------------------------------------------------- //
public:
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
};