#pragma once
#include "afxwin.h"

#include ".\LBParameter.h"

// CEditParameterDlg dialog

class CEditParameterDlg : public CDialog
{
	DECLARE_DYNAMIC(CEditParameterDlg)

public:
	CEditParameterDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CEditParameterDlg();

// Dialog Data
	enum { IDD = IDD_EDITPARAMETER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
private:
	// the name control
	CStatic m_NameCtrl;
	// the value control
	CEdit m_ValueCtrl;
	// will be set to true if the data are modified
	bool m_Modified;

	// DATA
	// the pointer to the original data
	LBParameter* m_pOldParameter;
	// the working copy of the data
	LBParameter m_Parameter;

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //
private:
	virtual BOOL OnInitDialog(void);

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
public:
	// SetParameter()
	// sets the working parameter and the pointer to the original one
	//  !! must be called before the DoModal function of this dialog !!
	void SetParameter(LBParameter* parameter);
private:
	// Validate()
	// verify the data entered in the dialog
	// returns true if all data are ok
	bool Validate();

// -------------------------------------------------------------------------- //
// EDIT HANDLERS                                                              //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnEnChangeValue();

// -------------------------------------------------------------------------- //
// BUTTONS HANDLERS                                                           //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnBnClickedOk();
};
