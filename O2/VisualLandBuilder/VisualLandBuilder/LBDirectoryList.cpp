#include "stdafx.h"

#include ".\LBDirectoryList.h"
#include ".\LBGlobalVariables.h"

IMPLEMENT_SERIAL(LBDirectoryList, CObject, 1)

// ************************************************************** //
// Constructors                                                   //
// ************************************************************** //

LBDirectoryList::LBDirectoryList() 
: CObject()
, m_Version(VERSION_NUMBER)
{
}

// -------------------------------------------------------------------------- //

LBDirectoryList::LBDirectoryList(const LBDirectoryList& other)
: m_Version(other.m_Version)
{
	// copies the new data
	POSITION pos = other.m_List.GetHeadPosition();
	while(pos)
	{
		LBDirectoryItem* item = new LBDirectoryItem(other.m_List.GetNext(pos));
		m_List.AddTail(item);
	}
}

// ************************************************************** //
// Destructor                                                     //
// ************************************************************** //

LBDirectoryList::~LBDirectoryList()
{
	RemoveAll();
}

// ************************************************************** //
// Assignement                                                    //
// ************************************************************** //

LBDirectoryList& LBDirectoryList::operator = (const LBDirectoryList& other)
{
	m_Version = other.m_Version;

	// clears the current list
	RemoveAll();

	// copies the new data
	POSITION pos = other.m_List.GetHeadPosition();
	while(pos)
	{
		LBDirectoryItem* item = new LBDirectoryItem(other.m_List.GetNext(pos));
		m_List.AddTail(item);
	}

	return *this;
}

// ************************************************************** //
// Member variables getters                                       //
// ************************************************************** //

CTypedPtrList<CObList, LBDirectoryItem*>& LBDirectoryList::GetList()
{
	return m_List;
}

// ************************************************************** //
// Member variables setters                                       //
// ************************************************************** //

void LBDirectoryList::SetList(CTypedPtrList<CObList, LBDirectoryItem*>& newList)
{
	// clears the current list
	RemoveAll();

	// copies the new data
	POSITION pos = newList.GetHeadPosition();
	while(pos)
	{
		LBDirectoryItem* item = new LBDirectoryItem(newList.GetNext(pos));
		m_List.AddTail(item);
	}
}

// ************************************************************** //
// Member variables manipulators                                  //
// ************************************************************** //

void LBDirectoryList::Append(const LBDirectoryItem& directory)
{
	LBDirectoryItem* item = new LBDirectoryItem(directory);
	m_List.AddTail(item);
}

// -------------------------------------------------------------------------- //

void LBDirectoryList::Append(LBDirectoryItem* directory)
{
	m_List.AddTail(directory);
}

// -------------------------------------------------------------------------- //

LBDirectoryItem* LBDirectoryList::GetAt(int index)
{
	return m_List.GetAt(m_List.FindIndex(index));
}

// -------------------------------------------------------------------------- //

void LBDirectoryList::SetAt(int index, LBDirectoryItem* directory)
{
	POSITION pos = m_List.FindIndex(index);
	LBDirectoryItem* oldDirectory = m_List.GetAt(pos);
	m_List.SetAt(pos, new LBDirectoryItem(directory));
	delete directory;
	delete oldDirectory;
}

// -------------------------------------------------------------------------- //

void LBDirectoryList::RemoveAt(int index)
{
	CTypedPtrList<CObList, LBDirectoryItem*> tempList;

	// sets the copy of the list
	// discarding the item to remove
	POSITION pos = m_List.GetHeadPosition();
	int counter = 0;
	while(pos)
	{
		LBDirectoryItem* item = m_List.GetNext(pos);
		if(counter != index)
		{
			LBDirectoryItem* copyItem = new LBDirectoryItem(item);
			tempList.AddTail(copyItem);
		}
		counter++;
	}

	// sets the list to be the temp list
	SetList(tempList);

	// clears the temp list
	pos = tempList.GetHeadPosition();
	while(pos)
	{
		delete tempList.GetNext(pos);
	}
	tempList.RemoveAll();
}

// -------------------------------------------------------------------------- //

void LBDirectoryList::RemoveAll()
{
	// removes all items from the list
	POSITION pos = m_List.GetHeadPosition();
	while(pos)
	{
		delete m_List.GetNext(pos);
	}
	m_List.RemoveAll();
}

// ************************************************************** //
// Interface                                                      //
// ************************************************************** //

INT_PTR LBDirectoryList::GetCount() const
{
	return m_List.GetCount();
}

// -------------------------------------------------------------------------- //

BOOL LBDirectoryList::IsEmpty() const
{
	return m_List.IsEmpty();
}

// -------------------------------------------------------------------------- //

LBErrorTypes LBDirectoryList::Validate() const
{
	POSITION pos = m_List.GetHeadPosition();
	while(pos)
	{
		LBDirectoryItem* item = m_List.GetNext(pos);
		LBErrorTypes res = item->Validate();
		if(res != LBET_NOERROR)
		{
			return res;
		}
	}
	return LBET_NOERROR;
}

// -------------------------------------------------------------------------- //

string LBDirectoryList::GetWorkingFolder() const
{
	POSITION pos = m_List.GetHeadPosition();
	while(pos)
	{
		LBDirectoryItem* item = m_List.GetNext(pos);
		if(item->GetResource() == RESNAME_WORKINGFOLDER)
		{
			return item->GetResourcePathName();
		}
	}
	return "";
}

// -------------------------------------------------------------------------- //

// ******************************************
// Removed connection to LB1
// 08.04.2009
// ******************************************
/*
string LBDirectoryList::GetLandBuilderPath() const
{
	POSITION pos = m_List.GetHeadPosition();
	while(pos)
	{
		LBDirectoryItem* item = m_List.GetNext(pos);
		if(item->GetResource() == RESNAME_LANDBUILDER)
		{
			return item->GetResourcePathName();
		}
	}
	return "";
}
*/

// -------------------------------------------------------------------------- //

string LBDirectoryList::GetLandBuilder2Path() const
{
	POSITION pos = m_List.GetHeadPosition();
	while(pos)
	{
		LBDirectoryItem* item = m_List.GetNext(pos);
		if(item->GetResource() == RESNAME_LANDBUILDER2)
		{
			return item->GetResourcePathName();
		}
	}
	return "";
}

// ************************************************************** //
// Serialization                                                  //
// ************************************************************** //

void LBDirectoryList::Serialize(CArchive& ar)
{
	CObject::Serialize(ar);

	m_List.Serialize(ar);

	if(ar.IsStoring())
	{
		ar << m_Version;
	}
	else
	{
		ar >> m_Version;
	}
}

// -------------------------------------------------------------------------- //
