#pragma once
#include "afxcmn.h"

#include <string>
#include <vector>

#include ".\LBModule.h"
#include ".\ExtendedControls\CListCtrlEx.h"

#include "afxwin.h"

using std::string;
using std::vector;

// CTabDbfDlg dialog

class CTabDbfDlg : public CDialog
{
	DECLARE_DYNAMIC(CTabDbfDlg)

public:
	CTabDbfDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTabDbfDlg();

// Dialog Data
	enum { IDD = IDD_TABDBF };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

// -------------------------------------------------------------------------- //

private:
	// the parameters list control
	CListCtrlEx m_DbfParListCtrl;
	// the description ctrl
	CStatic m_TabDbfDescriptionCtrl;

	// the list for filling the editing combo
	vector<string> m_ItemListCombo;

	// pointer to the current module
	LBModule* m_pModule;

	// will be set to true if the data are modified
	bool m_Modified;

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //
private:
	virtual BOOL OnInitDialog(void);

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //
public:
	// UpdateState()
	// updates the dialog state and sets m_Modified if data have been changed
	void UpdateState();
	// GetModified()
	// returns true if data have been modified
	bool GetModified() const;
	// HasData()
	// return true if there are data already inputted
	bool HasData() const;
	// Validate()
	// return true if there are all the data needed
	bool Validate() const;
	// SetModule()
	// sets the module
	void SetModule(LBModule* module);
	// GetClearButtonState()
	// returns the state of the clear button (true if enabled)
	BOOL GetClearButtonState() const;

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
public:
	// GetDbfFields()
	// loads fields from dbf file
	void GetDbfFields();
	// GetDbfFieldsCount
	// returns the number of fields in the dbf
	int GetDbfFieldsCount();
	// DataMatchFields()
	// returns true if all the data match the dbf fields
	bool DataMatchFields();

// -------------------------------------------------------------------------- //
// OBJECT LIST EVENT HANDLERS                                                 //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnNMClickTabDbfParList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnKeydownTabDbfParList(NMHDR *pNMHDR, LRESULT *pResult);

// -------------------------------------------------------------------------- //
// OBJECT LIST BUTTONS HANDLERS                                               //
// -------------------------------------------------------------------------- //
public:
	afx_msg void OnBnClickedTabDbfClearBtn();
private:
	afx_msg void OnBnClickedTabDbfToGlobalBtn();

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
private:
	// this function is used to intercept the end label editing in
	// the list ctrl
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
