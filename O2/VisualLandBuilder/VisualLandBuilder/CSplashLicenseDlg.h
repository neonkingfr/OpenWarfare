#pragma once
#include "afxwin.h"

// CSplashLicense dialog

class CSplashLicenseDlg : public CDialog
{
	DECLARE_DYNAMIC(CSplashLicenseDlg)

public:
	CSplashLicenseDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSplashLicenseDlg();

// Dialog Data
	enum { IDD = IDD_SPLASH_LICENSE };

public:
	static CSplashLicenseDlg* S_SplashLicenseDlg;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	CRichEditCtrl m_License_Text;

public:
	virtual BOOL OnInitDialog(void);

private:
	CFont   m_CStaticFont;
	CStatic m_CStatic;
public:
	CButton m_Agreed;

	afx_msg void OnBnClickedCheckAgree();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
};
