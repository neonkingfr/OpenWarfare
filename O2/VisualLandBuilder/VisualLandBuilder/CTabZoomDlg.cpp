// CTabZoomDlg.cpp : implementation file
//

#include "stdafx.h"
#include ".\VisualLandBuilder.h"
#include ".\CTabZoomDlg.h"
#include ".\CPreviewDlg.h"

// CTabZoomDlg dialog

IMPLEMENT_DYNAMIC(CTabZoomDlg, CDialog)

// -------------------------------------------------------------------------- //

CTabZoomDlg::CTabZoomDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTabZoomDlg::IDD, pParent)
{
}

// -------------------------------------------------------------------------- //

CTabZoomDlg::~CTabZoomDlg()
{
}

// -------------------------------------------------------------------------- //

void CTabZoomDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CTabZoomDlg, CDialog)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_PAN, &CTabZoomDlg::OnBnClickedPan)
	ON_BN_CLICKED(IDC_ZOOMWIN, &CTabZoomDlg::OnBnClickedZoomWin)
	ON_BN_CLICKED(IDC_ZOOMPLUS, &CTabZoomDlg::OnBnClickedZoomPlus)
	ON_BN_CLICKED(IDC_ZOOMMINUS, &CTabZoomDlg::OnBnClickedZoomMinus)
	ON_BN_CLICKED(IDC_ZOOMEXT, &CTabZoomDlg::OnBnClickedZoomExt)
	// support for XP style bitmap buttons
	ON_WM_THEMECHANGED()
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //

BOOL CTabZoomDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();

	m_PanLabel.SubclassDlgItem(IDC_PANTEXT, this);
	m_ZoomWindowLabel.SubclassDlgItem(IDC_ZOOMWINTEXT, this);

	// support for XP style bitmap buttons
	m_Theme.Init(m_hWnd);
	m_PanBtn.Load(IDC_PAN, this, &m_Theme);
	m_ZoomWinBtn.Load(IDC_ZOOMWIN, this, &m_Theme);
	m_ZoomPlusBtn.Load(IDC_ZOOMPLUS, this, &m_Theme);
	m_ZoomMinusBtn.Load(IDC_ZOOMMINUS, this, &m_Theme);
	m_ZoomExtBtn.Load(IDC_ZOOMEXT, this, &m_Theme);

	SetButtonsState(1.0, false, false);

	return TRUE;
}

// -------------------------------------------------------------------------- //

void CTabZoomDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	CRect dlgRect;
	GetClientRect(dlgRect);

	CWnd* group           = GetDlgItem(IDC_ZOOMGROUP);
	CWnd* panBtn          = GetDlgItem(IDC_PAN);
	CWnd* zoomWinBtn      = GetDlgItem(IDC_ZOOMWIN);
	CWnd* zoomPlusBtn     = GetDlgItem(IDC_ZOOMPLUS);
	CWnd* zoomMinusBtn    = GetDlgItem(IDC_ZOOMMINUS);
	CWnd* zoomExtBtn      = GetDlgItem(IDC_ZOOMEXT);

	CWnd* panTxt          = GetDlgItem(IDC_PANTEXT);
	CWnd* zoomWinTxt      = GetDlgItem(IDC_ZOOMWINTEXT);
	CWnd* zoomPlusTxt     = GetDlgItem(IDC_ZOOMPLUSTEXT);
	CWnd* zoomMinusTxt    = GetDlgItem(IDC_ZOOMMINUSTEXT);
	CWnd* zoomExtTxt      = GetDlgItem(IDC_ZOOMEXTTEXT);

	if(group)
	{
		int groupLeft   = dlgRect.left + 1;
		int groupTop    = dlgRect.top + 1;
		int groupRight  = dlgRect.right - 1;
		int groupBottom = dlgRect.bottom - 1;
		int groupWidth  = groupRight - groupLeft;
		int groupHeight = groupBottom - groupTop;
		group->MoveWindow(groupLeft, groupTop, groupWidth, groupHeight, TRUE);		

		// buttons
		panBtn->MoveWindow(groupLeft + 5, groupTop + 10, 36, 36, TRUE);		
		zoomWinBtn->MoveWindow(groupLeft + 5, groupTop + 48, 36, 36, TRUE);		
		zoomPlusBtn->MoveWindow(groupLeft + 5, groupTop + 86, 36, 36, TRUE);		
		zoomMinusBtn->MoveWindow(groupLeft + 5, groupTop + 124, 36, 36, TRUE);		
		zoomExtBtn->MoveWindow(groupLeft + 5, groupTop + 162, 36, 36, TRUE);		

		// text
		panTxt->MoveWindow(groupLeft + 50, groupTop + 21, 100, 12, TRUE);		
		zoomWinTxt->MoveWindow(groupLeft + 50, groupTop + 59, 100, 12, TRUE);		
		zoomPlusTxt->MoveWindow(groupLeft + 50, groupTop + 97, 100, 12, TRUE);		
		zoomMinusTxt->MoveWindow(groupLeft + 50, groupTop + 135, 100, 12, TRUE);		
		zoomExtTxt->MoveWindow(groupLeft + 50, groupTop + 173, 100, 12, TRUE);		
	}
}

// -------------------------------------------------------------------------- //

void CTabZoomDlg::OnBnClickedPan()
{
	reinterpret_cast<CPreviewDlg*>(GetParent()->GetParent())->OnPan();
}

// -------------------------------------------------------------------------- //

void CTabZoomDlg::OnBnClickedZoomWin()
{
	reinterpret_cast<CPreviewDlg*>(GetParent()->GetParent())->OnZoomWindow();
}

// -------------------------------------------------------------------------- //

void CTabZoomDlg::OnBnClickedZoomPlus()
{
	reinterpret_cast<CPreviewDlg*>(GetParent()->GetParent())->OnZoomPlus();
}

// -------------------------------------------------------------------------- //

void CTabZoomDlg::OnBnClickedZoomMinus()
{
	reinterpret_cast<CPreviewDlg*>(GetParent()->GetParent())->OnZoomMinus();
}

// -------------------------------------------------------------------------- //

void CTabZoomDlg::OnBnClickedZoomExt()
{
	reinterpret_cast<CPreviewDlg*>(GetParent()->GetParent())->OnZoomExt();
}

// -------------------------------------------------------------------------- //

void CTabZoomDlg::SetButtonsState(double zoomFactor, bool isPanning, bool isZoomingWindow)
{
	m_PanLabel.SetTextColor(RGB(0, 0, 0));
	m_ZoomWindowLabel.SetTextColor(RGB(0, 0, 0));
	GetDlgItem(IDC_PAN)->EnableWindow(TRUE);
	GetDlgItem(IDC_ZOOMWIN)->EnableWindow(TRUE);
	GetDlgItem(IDC_ZOOMPLUS)->EnableWindow(TRUE);
	GetDlgItem(IDC_ZOOMMINUS)->EnableWindow(TRUE);
	GetDlgItem(IDC_ZOOMEXT)->EnableWindow(TRUE);
	GetDlgItem(IDC_PANTEXT)->EnableWindow(TRUE);
	GetDlgItem(IDC_ZOOMWINTEXT)->EnableWindow(TRUE);
	GetDlgItem(IDC_ZOOMPLUSTEXT)->EnableWindow(TRUE);
	GetDlgItem(IDC_ZOOMMINUSTEXT)->EnableWindow(TRUE);
	GetDlgItem(IDC_ZOOMEXTTEXT)->EnableWindow(TRUE);

	if(isPanning)
	{
		m_PanLabel.SetTextColor(RGB(255, 0, 0));
		GetDlgItem(IDC_ZOOMWIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMPLUS)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMMINUS)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMEXT)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMWINTEXT)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMPLUSTEXT)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMMINUSTEXT)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMEXTTEXT)->EnableWindow(FALSE);
		return;
	}

	if(isZoomingWindow)
	{
		m_ZoomWindowLabel.SetTextColor(RGB(255, 0, 0));
		GetDlgItem(IDC_PAN)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMPLUS)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMMINUS)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMEXT)->EnableWindow(FALSE);
		GetDlgItem(IDC_PANTEXT)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMPLUSTEXT)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMMINUSTEXT)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMEXTTEXT)->EnableWindow(FALSE);
		return;
	}

	if(zoomFactor == 1.0)
	{
		GetDlgItem(IDC_PAN)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMMINUS)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMEXT)->EnableWindow(FALSE);
		GetDlgItem(IDC_PANTEXT)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMMINUSTEXT)->EnableWindow(FALSE);
		GetDlgItem(IDC_ZOOMEXTTEXT)->EnableWindow(FALSE);
		return;
	}
}

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

BOOL CTabZoomDlg::PreTranslateMessage(MSG* pMsg)
{
	// this is needed to prevent the dialog to hide when ENTER or ESC is pressed
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

// -------------------------------------------------------------------------- //

LRESULT CTabZoomDlg::OnThemeChanged()
{
	m_Theme.ThemeChanged(m_hWnd);

	return 1;
}