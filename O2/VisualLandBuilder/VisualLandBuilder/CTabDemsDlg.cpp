//-----------------------------------------------------------------------------

#include "stdafx.h"
#include ".\VisualLandBuilder.h"
#include ".\CTabDemsDlg.h"

#include ".\CAddEditModuleDlg.h"

//-----------------------------------------------------------------------------

IMPLEMENT_DYNAMIC( CTabDemsDlg, CDialog )

//-----------------------------------------------------------------------------

CTabDemsDlg::CTabDemsDlg( CWnd* parent /*=NULL*/ )
: CDialog( CTabDemsDlg::IDD, parent )
, m_modified( false )
{
}

//-----------------------------------------------------------------------------

CTabDemsDlg::~CTabDemsDlg()
{
}

//-----------------------------------------------------------------------------

void CTabDemsDlg::DoDataExchange( CDataExchange* dX )
{
	CDialog::DoDataExchange( dX );
	DDX_Control( dX, IDC_TABDEMSLIST, m_demsListCtrl );
}

//-----------------------------------------------------------------------------

BEGIN_MESSAGE_MAP( CTabDemsDlg, CDialog )
	ON_BN_CLICKED( IDC_TABDEMSADDBTN,    &CTabDemsDlg::OnBnClickedTabDemsAddBtn )
	ON_BN_CLICKED( IDC_TABDEMSEDITBTN,   &CTabDemsDlg::OnBnClickedTabDemsEditBtn )
	ON_BN_CLICKED( IDC_TABDEMSREMOVEBTN, &CTabDemsDlg::OnBnClickedTabDemsRemoveBtn )
	ON_BN_CLICKED( IDC_TABDEMSCLEARBTN,  &CTabDemsDlg::OnBnClickedTabDemsClearBtn )
	ON_NOTIFY( LVN_ITEMACTIVATE, IDC_TABDEMSLIST, &CTabDemsDlg::OnLvnItemActivateTabDemsList )
	ON_NOTIFY( LVN_KEYDOWN,      IDC_TABDEMSLIST, &CTabDemsDlg::OnLvnKeydownTabDemsList )
	ON_NOTIFY( NM_CLICK,         IDC_TABDEMSLIST, &CTabDemsDlg::OnNMClickTabDemsList )
END_MESSAGE_MAP()

//-----------------------------------------------------------------------------

BOOL CTabDemsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// sets the object list column header
	m_demsListCtrl.InsertColumn( 0, string( HEAD_FILETYPE ).c_str(), LVCFMT_LEFT );
	m_demsListCtrl.SetColumnWidth( 0, 90 );
	m_demsListCtrl.InsertColumn( 1, string( HEAD_FILENAME ).c_str(), LVCFMT_LEFT );
	m_demsListCtrl.SetColumnWidth( 1, 100 );
	m_demsListCtrl.InsertColumn( 2, string( HEAD_SRCLEFT ).c_str(), LVCFMT_LEFT );
	m_demsListCtrl.SetColumnWidth( 2, 70 );
	m_demsListCtrl.InsertColumn( 3, string( HEAD_SRCTOP ).c_str(), LVCFMT_LEFT );
	m_demsListCtrl.SetColumnWidth( 3, 70 );
	m_demsListCtrl.InsertColumn( 4, string( HEAD_SRCRIGHT ).c_str(), LVCFMT_LEFT );
	m_demsListCtrl.SetColumnWidth( 4, 70 );
	m_demsListCtrl.InsertColumn( 5, string( HEAD_SRCBOTTOM ).c_str(), LVCFMT_LEFT );
	m_demsListCtrl.SetColumnWidth( 5, 70 );
	m_demsListCtrl.InsertColumn( 6, string( HEAD_DESTLEFT ).c_str(), LVCFMT_LEFT );
	m_demsListCtrl.SetColumnWidth( 6, 70 );
	m_demsListCtrl.InsertColumn( 7, string( HEAD_DESTTOP ).c_str(), LVCFMT_LEFT );
	m_demsListCtrl.SetColumnWidth( 7, 70 );
	m_demsListCtrl.InsertColumn( 8, string( HEAD_DESTRIGHT ).c_str(), LVCFMT_LEFT );
	m_demsListCtrl.SetColumnWidth( 8, 70 );
	m_demsListCtrl.InsertColumn( 9, string( HEAD_DESTBOTTOM ).c_str(), LVCFMT_LEFT );
	m_demsListCtrl.SetColumnWidth( 9, 70 );

	// sets the object list style
	m_demsListCtrl.SetExtendedStyle( LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT );

	return (TRUE);
}

//-----------------------------------------------------------------------------

void CTabDemsDlg::UpdateState()
{
	// clears the list items
	if ( m_demsListCtrl.GetItemCount() != 0 )
	{
		m_demsListCtrl.DeleteAllItems();
	}

	// enables/disables controls if there are dems
	if ( m_module->GetUseDems() != LBMUD_NONE )
	{
		GetDlgItem( IDC_TABDEMSGROUP )->EnableWindow( TRUE );
		GetDlgItem( IDC_TABDEMSADDBTN )->EnableWindow( TRUE );
		GetDlgItem( IDC_TABDEMSLIST )->EnableWindow( TRUE );

		// updates the items
		INT_PTR demCount = m_module->GetDemsList().GetCount();
		for ( int i = 0; i < demCount; ++i )
		{
			LBDem* item = m_module->GetDemsList().GetAt( i );
			m_demsListCtrl.InsertItem( i, item->GetTypeAsString().c_str() );
			m_demsListCtrl.SetItem( i, 1, LVIF_TEXT, item->GetFileName().c_str(), 0, 0, 0, 0 );
		
			LBParameterList& demParamList = item->GetParametersList();
			INT_PTR demParamCount = demParamList.GetCount();
			for ( int j = 0; j < demParamCount; ++j )
			{
				string value = demParamList.GetAt( j )->GetValue();
				m_demsListCtrl.SetItem( i, j + 2, LVIF_TEXT, value.c_str(), 0, 0, 0, 0 );
			}
		}
		m_modified = true;

		// enables/disables add button
		if ( ((m_module->GetUseDems() == LBMUD_SINGLE) && (m_demsListCtrl.GetItemCount() == 1)) ||
			   (m_module->GetUseDems() == LBMUD_NONE) )
		{
			GetDlgItem( IDC_TABDEMSADDBTN )->EnableWindow( FALSE );
		}
		else
		{
			GetDlgItem( IDC_TABDEMSADDBTN )->EnableWindow( TRUE );
		}

		// enables/disables clear button
		if ( m_demsListCtrl.GetItemCount() == 0 )
		{
			GetDlgItem( IDC_TABDEMSCLEARBTN )->EnableWindow( FALSE );
		}
		else
		{
			GetDlgItem( IDC_TABDEMSCLEARBTN )->EnableWindow( TRUE );
		}

		// disables edit and remove buttons 
		GetDlgItem( IDC_TABDEMSEDITBTN )->EnableWindow( FALSE );
		GetDlgItem( IDC_TABDEMSREMOVEBTN )->EnableWindow( FALSE );
	}
	else
	{
		GetDlgItem( IDC_TABDEMSGROUP )->EnableWindow( FALSE );
		GetDlgItem( IDC_TABDEMSLIST )->EnableWindow( FALSE );
		GetDlgItem( IDC_TABDEMSADDBTN )->EnableWindow( FALSE );
		GetDlgItem( IDC_TABDEMSEDITBTN )->EnableWindow( FALSE );
		GetDlgItem( IDC_TABDEMSREMOVEBTN )->EnableWindow( FALSE );
		GetDlgItem( IDC_TABDEMSCLEARBTN )->EnableWindow( FALSE );
	}

	// disables the module list if there is at least one object
	reinterpret_cast< CAddEditModuleDlg* >( GetParent()->GetParent() )->UpdateModuleListCtrlState();
	reinterpret_cast< CAddEditModuleDlg* >( GetParent()->GetParent() )->UpdateClearAllDataBtnState();
	reinterpret_cast< CAddEditModuleDlg* >( GetParent()->GetParent() )->CheckOkButtonEnabling();
	reinterpret_cast< CAddEditModuleDlg* >( GetParent()->GetParent() )->UpdateDemTabImage();
}

//-----------------------------------------------------------------------------

bool CTabDemsDlg::GetModified() const
{
	return (m_modified);
}

//-----------------------------------------------------------------------------

void CTabDemsDlg::SetModule( LBModule* module )
{
	m_module = module;
}

//-----------------------------------------------------------------------------

void CTabDemsDlg::SetVisitorMap( const LBVisitorMap* visitorMap )
{
  m_visitorMap = visitorMap;
}

//-----------------------------------------------------------------------------

int CTabDemsDlg::GetListItemCount() const
{
	return (m_demsListCtrl.GetItemCount());
}

//-----------------------------------------------------------------------------

BOOL CTabDemsDlg::GetClearButtonState() const
{
	return (GetDlgItem( IDC_TABDEMSCLEARBTN )->IsWindowEnabled());	
}

//-----------------------------------------------------------------------------

void CTabDemsDlg::OnLvnItemActivateTabDemsList( NMHDR* nmhdr, LRESULT* result )
{
//	LPNMITEMACTIVATE nmia = reinterpret_cast< LPNMITEMACTIVATE >( nmhdr );

	OnBnClickedTabDemsEditBtn();

	*result = 0;
}

//-----------------------------------------------------------------------------

void CTabDemsDlg::OnLvnKeydownTabDemsList( NMHDR* nmhdr, LRESULT* result )
{
//	LPNMLVKEYDOWN lVKeyDow = reinterpret_cast< LPNMLVKEYDOWN >( nmhdr );

	OnNMClickTabDemsList( nmhdr, result);

	*result = 0;
}

//-----------------------------------------------------------------------------

void CTabDemsDlg::OnNMClickTabDemsList( NMHDR* nmhdr, LRESULT* result )
{
	// gets the index of the selected item in the list
	int selItem = m_demsListCtrl.GetNextItem( -1, LVNI_SELECTED );

	// enables/disables edit and remove button
	if ( selItem != -1 )
	{
		// something is selected
		GetDlgItem( IDC_TABDEMSEDITBTN )->EnableWindow( TRUE );
		GetDlgItem( IDC_TABDEMSREMOVEBTN )->EnableWindow( TRUE );
	}
	else
	{
		// no selection
		GetDlgItem( IDC_TABDEMSEDITBTN )->EnableWindow( FALSE );
		GetDlgItem( IDC_TABDEMSREMOVEBTN )->EnableWindow( FALSE );
	}

	*result = 0;
}

//-----------------------------------------------------------------------------

void CTabDemsDlg::OnBnClickedTabDemsAddBtn()
{
	// creates a new object
	LBDem* lbD = new LBDem();

	lbD->SetDefaultParameterList();

	// passes the dem to the dialog
	m_addEditDemDlg.SetDem( lbD );
  m_addEditDemDlg.SetVisitorMap( m_visitorMap );
	if ( m_addEditDemDlg.DoModal() == IDOK )
	{
		// adds the new object to the list
		m_module->AddDem( lbD );
		UpdateState();
	}
	else
	{
		delete lbD;
	}
}

//-----------------------------------------------------------------------------

void CTabDemsDlg::OnBnClickedTabDemsEditBtn()
{
	// gets the index of the selected item in the list
	int selItem = m_demsListCtrl.GetNextItem( -1, LVNI_SELECTED );

	// sets a copy of the dem
	LBDem* lbD = new LBDem( m_module->GetDemsList().GetAt( selItem ) );	

	m_addEditDemDlg.SetDem( lbD );
	if ( m_addEditDemDlg.DoModal() == IDOK )
	{
		m_module->SetDem( selItem, lbD );
		UpdateState();
	}
	else
	{
		delete lbD;
	}
}

//-----------------------------------------------------------------------------

void CTabDemsDlg::OnBnClickedTabDemsRemoveBtn()
{
	// gets the index of the selected item in the list
	int selItem = m_demsListCtrl.GetNextItem( -1, LVNI_SELECTED );

	LBDem* lbD = m_module->GetDemsList().GetAt( selItem );	

	string text = MSG_CONFIRMREMOVEDEMFILE;
	if ( MessageBox( text.c_str(), "", MB_YESNO | MB_ICONQUESTION ) == IDYES )
	{
		m_module->RemoveDem( selItem );
		UpdateState();
	}
}

//-----------------------------------------------------------------------------

void CTabDemsDlg::OnBnClickedTabDemsClearBtn()
{
	string text = MSG_CONFIRMREMOVEALLDEMS;
	if ( MessageBox( text.c_str(), "", MB_YESNO | MB_ICONQUESTION ) == IDYES )
	{
		m_module->RemoveAllDems();
		UpdateState();
	}
}

//-----------------------------------------------------------------------------

BOOL CTabDemsDlg::PreTranslateMessage( MSG* msg )
{
	// this is needed to prevent the dialog to hide when ENTER or ESC is pressed
	if ( msg->message == WM_KEYDOWN )
	{
		if ( msg->wParam == VK_RETURN || msg->wParam == VK_ESCAPE )
		{
			return (TRUE);
		}
	}
	return (CDialog::PreTranslateMessage( msg ));
}

//-----------------------------------------------------------------------------
