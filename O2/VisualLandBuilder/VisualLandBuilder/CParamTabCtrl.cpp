//-----------------------------------------------------------------------------

#include "stdafx.h"

#include ".\CParamTabCtrl.h"

//-----------------------------------------------------------------------------

BEGIN_MESSAGE_MAP( CParamTabCtrl, CTabCtrl )
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

//-----------------------------------------------------------------------------

CParamTabCtrl::CParamTabCtrl()
{
	m_tabPages[0] = new CTabObjectDlg();
	m_tabPages[1] = new CTabGlobalDlg();
	m_tabPages[2] = new CTabDbfDlg();
	m_tabPages[3] = new CTabShapefileDlg();
	m_tabPages[4] = new CTabDemsDlg();
	m_tabPages[5] = new CTabNotesDlg();

	m_numberOfPages = 6;


	// sets the image list for the param tabs
	COLORREF windowBk = RGB( 192, 192, 192 );
	m_imageList.Create( IDB_TABSTATES, 16, 0, windowBk );
}

//-----------------------------------------------------------------------------

CParamTabCtrl::~CParamTabCtrl()
{
	for ( int i = 0; i < m_numberOfPages; ++i )
	{
		delete m_tabPages[i];
	}
}

//-----------------------------------------------------------------------------

void CParamTabCtrl::Init()
{
	SetImageList( &m_imageList );

	m_currentTab = 0;

	m_tabPages[0]->Create( IDD_TABOBJECT,    this );
	m_tabPages[1]->Create( IDD_TABGLOBAL,    this );
	m_tabPages[2]->Create( IDD_TABDBF,       this );
	m_tabPages[3]->Create( IDD_TABSHAPEFILE, this );
	m_tabPages[4]->Create( IDD_TABDEMS,      this );
	m_tabPages[5]->Create( IDD_TABNOTES,     this );

	m_tabPages[0]->ShowWindow( SW_SHOW );
	m_tabPages[1]->ShowWindow( SW_HIDE );
	m_tabPages[2]->ShowWindow( SW_HIDE );
	m_tabPages[3]->ShowWindow( SW_HIDE );
	m_tabPages[4]->ShowWindow( SW_HIDE );
	m_tabPages[5]->ShowWindow( SW_HIDE );

	SetRectangle();
}

//-----------------------------------------------------------------------------

void CParamTabCtrl::SetRectangle()
{
	CRect tabRect, itemRect;

	GetClientRect( &tabRect );

	int nX = tabRect.left + 2;
	int nY = tabRect.top + 23;
	int nXc = tabRect.Width() - 6;
	int nYc = tabRect.Height() - 27;

	for ( int i = 0; i < m_numberOfPages; ++i )
	{
		if ( i == m_currentTab )
		{
			m_tabPages[i]->SetWindowPos( &wndTop, nX, nY, nXc, nYc, SWP_SHOWWINDOW );
		}
		else
		{
			m_tabPages[i]->SetWindowPos( &wndTop, nX, nY, nXc, nYc, SWP_HIDEWINDOW );
		}
	}
}

//-----------------------------------------------------------------------------

CDialog* CParamTabCtrl::GetDialog( UINT index ) const
{
	switch ( index )
	{
	case IDD_TABOBJECT:
		return m_tabPages[0];
	case IDD_TABGLOBAL:
		return m_tabPages[1];
	case IDD_TABDBF:
		return m_tabPages[2];
	case IDD_TABSHAPEFILE:
		return m_tabPages[3];
	case IDD_TABDEMS:
		return m_tabPages[4];
	case IDD_TABNOTES:
		return m_tabPages[5];
	default:
		return NULL;
	}
}

//-----------------------------------------------------------------------------

void CParamTabCtrl::OnLButtonDown( UINT nFlags, CPoint point ) 
{
	CTabCtrl::OnLButtonDown( nFlags, point );

	if ( m_currentTab != GetCurFocus() )
	{
		m_tabPages[m_currentTab]->ShowWindow( SW_HIDE );
		m_currentTab = GetCurFocus();
		m_tabPages[m_currentTab]->ShowWindow( SW_SHOW );
		m_tabPages[m_currentTab]->SetFocus();
	}
}

//-----------------------------------------------------------------------------

BOOL CParamTabCtrl::PreTranslateMessage( MSG* msg )
{
	if ( msg->message == WM_KEYDOWN )
	{
		if ( msg->wParam == VK_LEFT || msg->wParam == VK_RIGHT )
		{
			m_tabPages[m_currentTab]->ShowWindow( SW_HIDE );
			if ( msg->wParam == VK_LEFT )
			{
				if ( m_currentTab > 0 )
				{
					m_currentTab--;
				}
			}
			if ( msg->wParam == VK_RIGHT )
			{
				if ( m_currentTab < m_numberOfPages - 1 )
				{
					m_currentTab++;
				}
			}
			m_tabPages[m_currentTab]->ShowWindow( SW_SHOW );
		}
	}

	return (CTabCtrl::PreTranslateMessage( msg ));
}

//-----------------------------------------------------------------------------
