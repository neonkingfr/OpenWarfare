#pragma once
#include "afxwin.h"

#include <string>

using std::string;

// CRunLandBuilderDlg dialog

class CRunLandBuilderDlg : public CDialog
{
	DECLARE_DYNAMIC(CRunLandBuilderDlg)

public:
	CRunLandBuilderDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CRunLandBuilderDlg();

// Dialog Data
	enum { IDD = IDD_RUNLANDBUILDER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()


public:
	// the name ctrl
	CEdit m_RunNameCtrl;

	// DATA
	// the pointer to the original data
	string* m_pOldName;
	// the working copy of the data
	string m_Name;

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //

	BOOL OnInitDialog(void);

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
public:
	// SetName()
	// sets the working name and the pointer to the original one
	//  !! must be called before the DoModal function of this dialog !!
	void SetName(string* name);

	// Validate()
	// validates dialogs data
	// returns true if all ok
	bool Validate();

// -------------------------------------------------------------------------- //
// BUTTONS HANDLERS                                                           //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnBnClickedOk();
// -------------------------------------------------------------------------- //
// EDIT HANDLERS                                                              //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnEnChangeRunLbEdit();
};
