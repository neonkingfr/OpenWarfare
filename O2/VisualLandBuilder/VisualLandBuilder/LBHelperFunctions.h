#pragma once

#include <string>

using std::string;

// returns the file name from the given path
string GetFileNameFromPath(string path);

