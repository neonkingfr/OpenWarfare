#include "stdafx.h"

#include ".\LBDbFieldsFunctor.h"

// -------------------------------------------------------------------------- //

LBDbFieldsFunctor::LBDbFieldsFunctor()
{
}

// -------------------------------------------------------------------------- //

bool LBDbFieldsFunctor::operator ()(unsigned int shapeId, RStringI paramName, RStringI value) const
{
	string data = string(paramName.Data());

	// discard field beginning with tilde
	if(data[0] == '~') return false;
	if(!AlreadyInList(data))
	{
		m_FieldList.push_back(data);
		return false;
	}
	else
	{
		return true;
	}
}

// -------------------------------------------------------------------------- //

vector<string>& LBDbFieldsFunctor::GetFieldsList() const
{
	return m_FieldList;
}

// -------------------------------------------------------------------------- //

size_t LBDbFieldsFunctor::GetCount() const
{
	return m_FieldList.size();
}

// -------------------------------------------------------------------------- //

bool LBDbFieldsFunctor::AlreadyInList(string data)  const
{
	for(unsigned int i = 0; i < m_FieldList.size(); ++i)
	{
		if(data == m_FieldList[i]) return true;
	}
	return false;
}