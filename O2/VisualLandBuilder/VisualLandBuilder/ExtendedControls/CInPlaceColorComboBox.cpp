#include "stdafx.h"
#include ".\CInPlaceColorComboBox.h"

CInPlaceColorComboBox::CInPlaceColorComboBox(int iItem, int iSubItem, vector<COLORREF>& plstItems, UINT selection)
{
	m_iItem    = iItem;
	m_iSubItem = iSubItem;

	for(UINT i = 0; i < plstItems.size(); ++i)
	{
		m_ColorList.push_back(plstItems[i]);
	}

	m_Selection = selection;
	m_bESC = FALSE;
}

// -------------------------------------------------------------------------- //

CInPlaceColorComboBox::~CInPlaceColorComboBox()
{
}

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CInPlaceColorComboBox, CComboBox)
	ON_WM_CREATE()
	ON_WM_KILLFOCUS()
	ON_WM_CHAR()
	ON_WM_NCDESTROY()
	ON_CONTROL_REFLECT(CBN_CLOSEUP, OnCloseUp)
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //

int CInPlaceColorComboBox::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CComboBox::OnCreate(lpCreateStruct) == -1) return -1;

	// Set the proper font
	CFont* font = GetParent()->GetFont();
	SetFont(font);

	size_t colorCount = m_ColorList.size();
	for(UINT i = 0; i < colorCount; ++i)
	{
		CString str;
		str.Format("%d", i);
		int index = AddString(LPCTSTR(str));
		if(index != LB_ERR && index != LB_ERRSPACE) SetItemData(i, i);
	}
	
	SetCurSel(m_Selection);
	SetFocus();
	return 0;
}

// -------------------------------------------------------------------------- //

void CInPlaceColorComboBox::PreSubclassWindow()
{
	m_ItemHeight        = 18; // default item height
	m_ItemHBkColor      = GetSysColor(COLOR_HIGHLIGHT);
	m_ItemNormalBkColor = GetSysColor(COLOR_WINDOW);

	CComboBox::PreSubclassWindow();
}

// -------------------------------------------------------------------------- //

BOOL CInPlaceColorComboBox::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			::TranslateMessage(pMsg);
			::DispatchMessage(pMsg);
			return TRUE;				// DO NOT process further
		}
	}

	return CComboBox::PreTranslateMessage(pMsg);
}

// -------------------------------------------------------------------------- //

void CInPlaceColorComboBox::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	lpMeasureItemStruct->itemHeight = m_ItemHeight;
}

// -------------------------------------------------------------------------- //

void CInPlaceColorComboBox::OnKillFocus(CWnd* pNewWnd)
{
	CComboBox::OnKillFocus(pNewWnd);

	CString str;
	str.Format("%d", GetCurSel());

	// Send Notification to parent of ListView ctrl
	LV_DISPINFO dispinfo;
	dispinfo.hdr.hwndFrom = GetParent()->m_hWnd;
	dispinfo.hdr.idFrom = GetDlgCtrlID();
	dispinfo.hdr.code = LVN_ENDLABELEDIT;

	dispinfo.item.mask = LVIF_TEXT;
	dispinfo.item.iItem = m_iItem;
	dispinfo.item.iSubItem = m_iSubItem;
	dispinfo.item.pszText = m_bESC ? NULL : LPTSTR((LPCTSTR)str);
	dispinfo.item.cchTextMax = str.GetLength();

	GetParent()->GetParent()->SendMessage(WM_NOTIFY, GetParent()->GetDlgCtrlID(), (LPARAM)&dispinfo);

	PostMessage(WM_CLOSE);
}

// -------------------------------------------------------------------------- //

void CInPlaceColorComboBox::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if(nChar == VK_ESCAPE || nChar == VK_RETURN)
	{
		if(nChar == VK_ESCAPE) m_bESC = TRUE;
		GetParent()->SetFocus();
		return;
	}

	CComboBox::OnChar(nChar, nRepCnt, nFlags);
}

// -------------------------------------------------------------------------- //

void CInPlaceColorComboBox::OnNcDestroy()
{
	CComboBox::OnNcDestroy();

	delete this;
}

// -------------------------------------------------------------------------- //

void CInPlaceColorComboBox::OnCloseUp()
{
	GetParent()->SetFocus();
}

// -------------------------------------------------------------------------- //

void CInPlaceColorComboBox::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	CRect rItemRect(lpDrawItemStruct->rcItem);
	CRect rBlockRect(rItemRect);

	CDC dc;
	if(!dc.Attach(lpDrawItemStruct->hDC))
	{
		return;
	}

	int iState = lpDrawItemStruct->itemState;

	if(iState & ODS_SELECTED)
	{
		dc.FillSolidRect(&rBlockRect, m_ItemHBkColor);
	}
	else
	{
		dc.FillSolidRect(&rBlockRect, m_ItemNormalBkColor);
	}

	if(iState & ODS_FOCUS)
	{
		dc.DrawFocusRect(&rItemRect);
	}

	rBlockRect.DeflateRect(CSize(2, 2));

	int iItem = lpDrawItemStruct->itemID;

	CString strColor;
	if(iItem != -1)
	{
		COLORREF crColour = m_ColorList[GetItemData(iItem)];
		dc.FillSolidRect(&rBlockRect, crColour);				
		CBrush brFrameBrush;
		brFrameBrush.CreateStockObject(BLACK_BRUSH);
		dc.FrameRect(&rBlockRect, &brFrameBrush);
	}

	dc.Detach();
}
