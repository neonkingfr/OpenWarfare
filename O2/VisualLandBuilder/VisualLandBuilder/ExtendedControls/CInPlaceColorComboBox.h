#pragma once

#include <string>
#include <vector>

using std::string;
using std::vector;

class CInPlaceColorComboBox : public CComboBox
{
// Construction
public:
	CInPlaceColorComboBox(int iItem, int iSubItem, vector<COLORREF>& plstItems, UINT selection);

// Overrides
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

// Destructor
public:
	virtual ~CInPlaceColorComboBox();

// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnNcDestroy();
	afx_msg void OnCloseUp();

	DECLARE_MESSAGE_MAP()

private:
	int              m_iItem;
	int              m_iSubItem;
	vector<COLORREF> m_ColorList;
	short            m_ItemHeight;
	COLORREF         m_ItemNormalBkColor;
	COLORREF         m_ItemHBkColor;
	UINT             m_Selection;
	BOOL             m_bESC;				// To indicate whether ESC key was pressed

protected:
	virtual void PreSubclassWindow();

public:
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
};
