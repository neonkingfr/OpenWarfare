#include <string>
#include <vector>

using std::string;
using std::vector;

class LBDbFieldsFunctor
{
	mutable vector<string> m_FieldList;

public:
	LBDbFieldsFunctor();
	bool operator ()(unsigned int shapeId, RStringI paramName, RStringI value) const;
	vector<string>& GetFieldsList() const;
	size_t GetCount() const;
private:
	bool AlreadyInList(string data) const;
};