#pragma once

#include ".\External\TransparentStatic\TransparentStatic.h"
#include "afxwin.h"

// CSplashDlg dialog

class CSplashDlg : public CDialog
{
	DECLARE_DYNAMIC(CSplashDlg)

public:
	CSplashDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSplashDlg();

// Dialog Data
	enum { IDD = IDD_SPLASH };

public:
	static CSplashDlg* m_SplashDlg;

private:
	// the version static ctrl
	CTransparentStatic m_VersionStaticCtrl;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	static void ShowSplashScreen(CWnd* pParentWnd);
private:
	void HideSplashScreen();
private:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
public:
	virtual BOOL OnInitDialog();
};
