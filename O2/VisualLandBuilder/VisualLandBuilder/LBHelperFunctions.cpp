#include "stdafx.h"

#include ".\LBHelperFunctions.h"

// -------------------------------------------------------------------------- //

string GetFileNameFromPath(string path)
{
	size_t pos = path.find_last_of('\\');
	int start;
	if(pos == string::npos)
	{
		start = 0;
	}
	else
	{
		start = (int)pos + 1;
	}
	return path.substr(start);
}

// -------------------------------------------------------------------------- //
