#pragma once

#include <string>

#include ".\LBDirectoryList.h" 
#include ".\CTabDirectoriesDlg.h"

using std::string;

class LBOptions : public CObject
{
	DECLARE_SERIAL(LBOptions)

	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// the version
	int m_Version;

	// the list of Land Builder resource directories
	LBDirectoryList m_DirectoryList;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************************************************************** //
	// Constructors                                                   //
	// ************************************************************** //

	// Default constructor
	LBOptions();

	// copy constructor
	LBOptions(const LBOptions& other);

	// ************************************************************** //
	// Destructor                                                     //
	// ************************************************************** //
	virtual ~LBOptions();

	// ************************************************************** //
	// Assignement                                                    //
	// ************************************************************** //
	LBOptions& operator = (const LBOptions& other);

	// ************************************************************** //
	// Member variables getters                                       //
	// ************************************************************** //

	// GetDirectoryList()
	// returns the directory list of this project
	LBDirectoryList& GetDirectoryList();

	// ************************************************************** //
	// Member variables setters                                       //
	// ************************************************************** //

	// SetDirectoryList()
	// sets the directory list of this project with the given one
	void SetDirectoryList(const LBDirectoryList& directoryList);

	// ************************************************************** //
	// Member variables manipulators                                  //
	// ************************************************************** //

	// SetDefaultDirectoryList()
	// sets the default directory list
	void SetDefaultDirectoryList(CTabDirectoriesDlg& dlg);

	// ************************************************************** //
	// Serialization                                                  //
	// ************************************************************** //
	virtual void Serialize(CArchive& ar);
};
