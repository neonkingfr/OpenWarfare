#include "stdafx.h"
#include "theme.h"

/************************************************************
**
**	Simple Theme class for CHoverBitmapButton
**
**	You could use the WTL CTheme class instead.
**
**	by Rail Jon Rogut Feb 2003
**
*************************************************************/

CTheme::CTheme()
{
	m_bXPTheme = FALSE;
	m_bLibLoaded = FALSE;

	m_bThemeExists = FALSE;

	m_hModThemes = NULL;
	m_hTheme = NULL;

	m_hWnd = NULL;

	zOpenThemeData = NULL;
	zDrawThemeBackground = NULL;
	zCloseThemeData = NULL;
	zDrawThemeText = NULL;
	zGetThemeBackgroundContentRect = NULL;
	zDrawThemeEdge = NULL;
	zDrawThemeIcon = NULL;
}

CTheme::~CTheme()
{
	if (m_hModThemes)
		::FreeLibrary(m_hModThemes);
}

void CTheme::Init(HWND hWnd)
{
	Init();
	OpenTheme(hWnd);
}

void CTheme::Init()
{
	m_bXPTheme = GetAppearance();

	if (!m_bXPTheme)
		{
		return;
		}

	m_hModThemes = LoadLibrary(_T("UXTHEME.DLL"));

	if(m_hModThemes)
	{
		zOpenThemeData = (PFNOPENTHEMEDATA)GetProcAddress(m_hModThemes, _T("OpenThemeData"));
		zDrawThemeBackground = (PFNDRAWTHEMEBACKGROUND)GetProcAddress(m_hModThemes, _T("DrawThemeBackground"));
		zCloseThemeData = (PFNCLOSETHEMEDATA)GetProcAddress(m_hModThemes, _T("CloseThemeData"));
		zDrawThemeText = (PFNDRAWTHEMETEXT)GetProcAddress(m_hModThemes, _T("DrawThemeText"));
		zGetThemeBackgroundContentRect = (PFNGETTHEMEBACKGROUNDCONTENTRECT)GetProcAddress(m_hModThemes, _T("GetThemeBackgroundContentRect"));
		zDrawThemeEdge = (PFNDRAWTHEMEEDGE)GetProcAddress(m_hModThemes, _T("DrawThemeEdge"));
		zDrawThemeIcon = (PFNDRAWTHEMEICON)GetProcAddress(m_hModThemes, _T("DrawThemeIcon"));

		if(zOpenThemeData && zDrawThemeBackground && zCloseThemeData 
									&& zDrawThemeText && zGetThemeBackgroundContentRect 
									&& zDrawThemeEdge && zDrawThemeIcon)
			{
			m_bLibLoaded = TRUE;			
			}
		else
			{
			::FreeLibrary(m_hModThemes);
			m_hModThemes = NULL;
			}
	}
}

void	CTheme::ReInit(void)
{
	if (m_hModThemes)
		::FreeLibrary(m_hModThemes);

	m_hModThemes = NULL;
	m_bLibLoaded = FALSE;
	m_bXPTheme = FALSE;
	m_bThemeExists = FALSE;
}

void	CTheme::ThemeChanged(HWND hWnd)
{
	if (m_bLibLoaded)
		{
		if (m_hTheme)
			{
			zCloseThemeData(m_hTheme);
			ReInit();
			}
		}

	Init();

	OpenTheme(hWnd);
}

void	CTheme::OpenTheme(HWND hWnd)
{
	if (m_bLibLoaded)
		{
		ASSERT(hWnd);

		m_hWnd = hWnd;

		m_hTheme = zOpenThemeData(m_hWnd, L"Button");

		if (m_hTheme)
			m_bThemeExists = TRUE;
		}
}

void	CTheme::DrawThemeBackground(HDC dc, RECT *pRect, int iPartID, int iStateID, RECT *pClipRect)
{
	CRect r = *pRect;

	r.InflateRect(1, 1);

	if (m_hTheme)
		{
		zDrawThemeBackground(m_hTheme, dc, iPartID, iStateID, &r, pClipRect);
		}
}

void	CTheme::DrawThemeEdge(HDC dc, RECT *pRect, UINT uEdge, UINT uFlags, int iPartID, int iStateID, RECT *pClipRect)
{
	if (m_hTheme)
		{
		zDrawThemeEdge(m_hTheme, dc, iPartID, iStateID, pRect, uEdge, uFlags, pClipRect);
		}
}

BOOL	CTheme::GetAppearance(void)
{
	// For XP - Detect if the Window Style is Classic or XP

	OSVERSIONINFO osvi;

	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	GetVersionEx(&osvi);

	if (osvi.dwMajorVersion < 5)	// Earlier than XP
		return FALSE;

	/////////////////////////////////////////////////////

	HKEY	hKey;
	
	CString szSubKey = _T("Control Panel\\Appearance");
	CString	szCurrent = _T("Current");

	DWORD	dwSize = 200;

	unsigned char * pBuffer = new unsigned char[dwSize];

	memset(pBuffer, 0, dwSize);

	if (RegOpenKeyEx(HKEY_CURRENT_USER, (LPCSTR)szSubKey, 0L, KEY_READ, &hKey) != ERROR_SUCCESS)
		{
		// Can't find it

		delete []pBuffer;

		return FALSE;
		}

	RegQueryValueEx(hKey, szCurrent, NULL, NULL, pBuffer, &dwSize); 

	RegCloseKey(hKey);

	szCurrent = pBuffer;

	delete []pBuffer;

	if (szCurrent == _T("Windows Standard"))
		return FALSE;

	return TRUE;
}
