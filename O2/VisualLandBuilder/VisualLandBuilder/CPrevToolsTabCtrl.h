#pragma once
#include "resource.h"

#include ".\CTabZoomDlg.h"
#include ".\CTabShapeListDlg.h"
#include ".\CTabObjectColorListDlg.h"

// ****************** //
// Enum for tab items //
// ****************** //
typedef enum
{
	PV_TAB_ZOOM,
	PV_TAB_SHAPES,
	PV_TAB_OBJCOLORS
} LBPreViewTabItems;

class CPrevToolsTabCtrl : public CTabCtrl
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
	CDialog* m_TabPages[3];
	int m_CurrentTab;
	int m_NumberOfPages;
	CImageList imageList;

public:
	// ************************************************************** //
	// Constructors                                                   //
	// ************************************************************** //

	// Default constructor
	CPrevToolsTabCtrl();

	// ************************************************************** //
	// Destructor                                                     //
	// ************************************************************** //
	virtual ~CPrevToolsTabCtrl();

	// ************************************************************** //
	// helper functions                                               //
	// ************************************************************** //
public:
	void Init();
private:
	void SetRectangle();
public:
	CDialog* GetDialog(UINT index) const;

// ************************************************************** //
// Message handlers                                               //
// ************************************************************** //
private:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg void OnSize(UINT nType, int cx, int cy);
};