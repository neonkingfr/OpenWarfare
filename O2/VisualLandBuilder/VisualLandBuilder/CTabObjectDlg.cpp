// CTabObjectDlg.cpp : implementation file
//

#include "stdafx.h"
#include ".\VisualLandBuilder.h"
#include ".\CTabObjectDlg.h"

#include ".\CAddEditModuleDlg.h"

// CTabObjectDlg dialog

IMPLEMENT_DYNAMIC(CTabObjectDlg, CDialog)

// -------------------------------------------------------------------------- //

CTabObjectDlg::CTabObjectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTabObjectDlg::IDD, pParent)
	, m_Modified(false)
{
}

// -------------------------------------------------------------------------- //

CTabObjectDlg::~CTabObjectDlg()
{
}

// -------------------------------------------------------------------------- //

void CTabObjectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TABOBJECTLIST, m_ObjectsListCtrl);
}

// -------------------------------------------------------------------------- //

BEGIN_MESSAGE_MAP(CTabObjectDlg, CDialog)
	ON_NOTIFY(NM_CLICK, IDC_TABOBJECTLIST, &CTabObjectDlg::OnNMClickTabObjectList)
	ON_BN_CLICKED(IDC_TABOBJADDBTN, &CTabObjectDlg::OnBnClickedTabObjAddBtn)
	ON_BN_CLICKED(IDC_TABOBJEDITBTN, &CTabObjectDlg::OnBnClickedTabObjEditBtn)
	ON_BN_CLICKED(IDC_TABOBJREMOVEBTN, &CTabObjectDlg::OnBnClickedTabObjRemoveBtn)
	ON_BN_CLICKED(IDC_TABOBJCLEARBTN, &CTabObjectDlg::OnBnClickedTabObjClearBtn)
	ON_NOTIFY(LVN_ITEMACTIVATE, IDC_TABOBJECTLIST, &CTabObjectDlg::OnLvnItemActivateTabObjectList)
	ON_NOTIFY(LVN_KEYDOWN, IDC_TABOBJECTLIST, &CTabObjectDlg::OnLvnKeydownTabObjectList)
END_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //

BOOL CTabObjectDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();

	// sets the object list column header
	m_ObjectsListCtrl.InsertColumn(0, string(HEAD_OBJECTNAME).c_str(), LVCFMT_LEFT);
	m_ObjectsListCtrl.SetColumnWidth(0, 110);

	// sets the object list style
	m_ObjectsListCtrl.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);

	return TRUE;
}

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //

void CTabObjectDlg::UpdateState(bool header)
{
	// clears the list items
	if(m_ObjectsListCtrl.GetItemCount() != 0)
	{
		m_ObjectsListCtrl.DeleteAllItems();
	}

	// updates the column header
	if(header)
	{
		// clears the list columns
		int columnCount = m_ObjectsListCtrl.GetHeaderCtrl()->GetItemCount();
		for(int i = 1; i < columnCount; ++i)
		{
			m_ObjectsListCtrl.DeleteColumn(1);
		}

		// re-populate the list columns
		switch(m_pModule->GetUseObjects())
		{
		case LBMUO_NONE:
			break;
		case LBMUO_SINGLE:
		case LBMUO_MULTIPLE:
			{
				INT_PTR objParamCount = m_pModule->GetObjParametersList().GetCount();
				for(int i = 0; i < objParamCount; ++i)
				{
					LBParameter* item = m_pModule->GetObjParametersList().GetAt(i);
					string colHeader = item->GetName();
					m_ObjectsListCtrl.InsertColumn(i + 1, colHeader.c_str(), LVCFMT_LEFT);
					m_ObjectsListCtrl.SetColumnWidth(i + 1, 80);
				}
			}
			break;
		default:
			break;
		}

		// enables/disables controls if there are objects
		switch(m_pModule->GetUseObjects())
		{
		case LBMUO_NONE:
			GetDlgItem(IDC_TABOBJGROUP)->EnableWindow(FALSE);
			GetDlgItem(IDC_TABOBJECTLIST)->EnableWindow(FALSE);
			break;
		case LBMUO_SINGLE:
		case LBMUO_MULTIPLE:
			GetDlgItem(IDC_TABOBJGROUP)->EnableWindow(TRUE);
			GetDlgItem(IDC_TABOBJECTLIST)->EnableWindow(TRUE);
			break;
		default:
			GetDlgItem(IDC_TABOBJGROUP)->EnableWindow(FALSE);
			GetDlgItem(IDC_TABOBJECTLIST)->EnableWindow(FALSE);
			break;
		}
	}

	// updates the items
	INT_PTR objCount = m_pModule->GetObjectsList().GetCount();
	for(int i = 0; i < objCount; ++i)
	{
		LBObject* item = m_pModule->GetObjectsList().GetAt(i);
		m_ObjectsListCtrl.InsertItem(i, item->GetName().c_str());
		if(item->GetName() == OBJ_NAME_FROMDBF)
		{
			m_ObjectsListCtrl.SetRowTxtColor(i, RGB(0, 128, 0));
			m_ObjectsListCtrl.SetRowStyle(i, LIS_TXTCOLOR);
		}
		else
		{
			m_ObjectsListCtrl.SetRowTxtColor(i, RGB(0, 0, 0));
			m_ObjectsListCtrl.SetRowStyle(i, LIS_TXTCOLOR);
		}

		INT_PTR objParamCount = item->GetParametersList().GetCount();
		for(int j = 0; j < objParamCount; ++j)
		{
			LBParameter* param = item->GetParametersList().GetAt(j);
			m_ObjectsListCtrl.SetItem(i, j + 1, LVIF_TEXT, param->GetValue().c_str(), 0, 0, 0, 0);
		}
	}
	m_Modified = true;

	// enables/disables add button
	if(((m_pModule->GetUseObjects() == LBMUO_SINGLE) && (m_ObjectsListCtrl.GetItemCount() == 1)) ||
		(m_pModule->GetUseObjects() == LBMUO_NONE))
	{
		GetDlgItem(IDC_TABOBJADDBTN)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_TABOBJADDBTN)->EnableWindow(TRUE);
	}

	// enables/disables clear button
	if(m_ObjectsListCtrl.GetItemCount() == 0)
	{
		GetDlgItem(IDC_TABOBJCLEARBTN)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_TABOBJCLEARBTN)->EnableWindow(TRUE);
	}

	// disables edit and remove buttons 
	GetDlgItem(IDC_TABOBJEDITBTN)->EnableWindow(FALSE);
	GetDlgItem(IDC_TABOBJREMOVEBTN)->EnableWindow(FALSE);

	// disables the module list if there is at least one object
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateModuleListCtrlState();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateClearAllDataBtnState();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->CheckOkButtonEnabling();
	reinterpret_cast<CAddEditModuleDlg*>(GetParent()->GetParent())->UpdateObjectTabImage();
}

// -------------------------------------------------------------------------- //

bool CTabObjectDlg::GetModified() const
{
	return m_Modified;
}

// -------------------------------------------------------------------------- //
	
int CTabObjectDlg::GetListItemCount() const
{
	return m_ObjectsListCtrl.GetItemCount();
}

// -------------------------------------------------------------------------- //

void CTabObjectDlg::SetModule(LBModule* module)
{
	m_pModule = module;
}

// -------------------------------------------------------------------------- //

BOOL CTabObjectDlg::GetClearButtonState() const
{
	return GetDlgItem(IDC_TABOBJCLEARBTN)->IsWindowEnabled();	
}

// -------------------------------------------------------------------------- //
// OBJECT LIST EVENT HANDLERS                                                 //
// -------------------------------------------------------------------------- //

void CTabObjectDlg::OnNMClickTabObjectList(NMHDR *pNMHDR, LRESULT *pResult)
{
	// gets the index of the selected item in the list
	int selItem = m_ObjectsListCtrl.GetNextItem(-1, LVNI_SELECTED);

	// enables/disables edit and remove button
	if(selItem != -1)
	{
		// something is selected
		GetDlgItem(IDC_TABOBJEDITBTN)->EnableWindow(TRUE);
		GetDlgItem(IDC_TABOBJREMOVEBTN)->EnableWindow(TRUE);
	}
	else
	{
		// no selection
		GetDlgItem(IDC_TABOBJEDITBTN)->EnableWindow(FALSE);
		GetDlgItem(IDC_TABOBJREMOVEBTN)->EnableWindow(FALSE);
	}

	*pResult = 0;
}

// -------------------------------------------------------------------------- //

void CTabObjectDlg::OnLvnItemActivateTabObjectList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	OnBnClickedTabObjEditBtn();

	*pResult = 0;
}

// -------------------------------------------------------------------------- //

void CTabObjectDlg::OnLvnKeydownTabObjectList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);

	OnNMClickTabObjectList(pNMHDR, pResult);

	*pResult = 0;
}

// -------------------------------------------------------------------------- //
// OBJECT LIST BUTTONS HANDLERS                                               //
// -------------------------------------------------------------------------- //

void CTabObjectDlg::OnBnClickedTabObjAddBtn()
{
	// creates a new object
	LBObject* lbO = new LBObject();

	lbO->SetParameterList(m_pModule->GetObjParametersList());

	// passes the object to the dialog
	m_AddEditObjectDlg.SetObject(lbO);
	m_AddEditObjectDlg.SetModule(m_pModule);
	if(m_AddEditObjectDlg.DoModal() == IDOK)
	{
		// adds the new object to the list
		m_pModule->AddObject(lbO);
		UpdateState(false);
	}
	else
	{
		delete lbO;
	}
}

// -------------------------------------------------------------------------- //

void CTabObjectDlg::OnBnClickedTabObjEditBtn()
{
	// gets the index of the selected item in the list
	int selItem = m_ObjectsListCtrl.GetNextItem(-1, LVNI_SELECTED);

	LBObject* lbO = m_pModule->GetObjectsList().GetAt(selItem);	

	m_AddEditObjectDlg.SetObject(lbO);
	if(m_AddEditObjectDlg.DoModal() == IDOK)
	{
		m_pModule->SetObject(selItem, lbO);
		UpdateState(false);
	}
}

// -------------------------------------------------------------------------- //

void CTabObjectDlg::OnBnClickedTabObjRemoveBtn()
{
	// gets the index of the selected item in the list
	int selItem = m_ObjectsListCtrl.GetNextItem(-1, LVNI_SELECTED);

	LBObject* lbO = m_pModule->GetObjectsList().GetAt(selItem);	

	string text = MSG_CONFIRMREMOVEOBJECT;
	if(MessageBox(text.c_str(), "", MB_YESNO | MB_ICONQUESTION) == IDYES)
	{
		m_pModule->RemoveObject(selItem);
		UpdateState(false);
	}
}

// -------------------------------------------------------------------------- //

void CTabObjectDlg::OnBnClickedTabObjClearBtn()
{
	string text = MSG_CONFIRMREMOVEALLOBJS;
	if(MessageBox(text.c_str(), "", MB_YESNO | MB_ICONQUESTION) == IDYES)
	{
		m_pModule->RemoveAllObjects();
		UpdateState(false);
	}
}

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //

BOOL CTabObjectDlg::PreTranslateMessage(MSG* pMsg)
{
	// this is needed to prevent the dialog to hide when ENTER or ESC is pressed
	if(pMsg->message == WM_KEYDOWN)
	{
		if(pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

// -------------------------------------------------------------------------- //
