//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include ".\CTabObjectDlg.h"
#include ".\CTabGlobalDlg.h"
#include ".\CTabDbfDlg.h"
#include ".\CTabShapefileDlg.h"
#include ".\CTabDemsDlg.h"
#include ".\CTabNotesDlg.h"

//-----------------------------------------------------------------------------

// ****************** //
// Enum for tab items //
// ****************** //
typedef enum
{
	TAB_OBJECTS,
	TAB_GLOBALS,
	TAB_DBFS,
	TAB_SHAPES,
	TAB_DEMS,
	TAB_NOTES
} LBTabItems;

//-----------------------------------------------------------------------------

// ******************* //
// Enum for tab states //
// ******************* //
typedef enum
{
	TS_NODATA,
	TS_DATAOK,
	TS_MISSINGDATA,
	TS_WRONGDATA
} LBTabStates;

//-----------------------------------------------------------------------------

class CParamTabCtrl : public CTabCtrl
{
protected:
	CDialog*   m_tabPages[6];
	int        m_currentTab;
	int        m_numberOfPages;
	CImageList m_imageList;

public:
	// Default constructor
	CParamTabCtrl();

	virtual ~CParamTabCtrl();

public:
	void Init();

private:
	void SetRectangle();

public:
	CDialog* GetDialog( UINT index ) const;

private:
	afx_msg void OnLButtonDown( UINT nFlags, CPoint point );

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL PreTranslateMessage( MSG* msg );
};

//-----------------------------------------------------------------------------
