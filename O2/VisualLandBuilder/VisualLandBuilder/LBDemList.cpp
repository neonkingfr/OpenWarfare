#include "stdafx.h"

#include ".\LBDemList.h"

IMPLEMENT_SERIAL(LBDemList, CObject, 1)

// ************************************************************** //
// Constructors                                                   //
// ************************************************************** //

LBDemList::LBDemList() 
: CObject()
, m_Version(VERSION_NUMBER)
{
}

// -------------------------------------------------------------------------- //

LBDemList::LBDemList(const LBDemList& other)
: m_Version(other.m_Version)
{
	// copies the new data
	POSITION pos = other.m_List.GetHeadPosition();
	while(pos)
	{
		LBDem* item = new LBDem(other.m_List.GetNext(pos));
		m_List.AddTail(item);
	}
}

// ************************************************************** //
// Destructor                                                     //
// ************************************************************** //

LBDemList::~LBDemList()
{
	RemoveAll();
}

// ************************************************************** //
// Assignement                                                    //
// ************************************************************** //

LBDemList& LBDemList::operator = (const LBDemList& other)
{
	m_Version = other.m_Version;

	// clears the current list
	RemoveAll();

	// copies the new data
	POSITION pos = other.m_List.GetHeadPosition();
	while(pos)
	{
		LBDem* item = new LBDem(other.m_List.GetNext(pos));
		m_List.AddTail(item);
	}

	return *this;
}

// ************************************************************** //
// Member variables getters                                       //
// ************************************************************** //

CTypedPtrList<CObList, LBDem*>& LBDemList::GetList()
{
	return m_List;
}

// ************************************************************** //
// Member variables setters                                       //
// ************************************************************** //

void LBDemList::SetList(CTypedPtrList<CObList, LBDem*>& newList)
{
	// clears the current list
	RemoveAll();

	// copies the new data
	POSITION pos = newList.GetHeadPosition();
	while(pos)
	{
		LBDem* item = new LBDem(newList.GetNext(pos));
		m_List.AddTail(item);
	}
}

// ************************************************************** //
// Member variables manipulators                                  //
// ************************************************************** //

void LBDemList::Append(const LBDem& dem)
{
	LBDem* item = new LBDem(dem);
	m_List.AddTail(item);
}

// -------------------------------------------------------------------------- //

void LBDemList::Append(LBDem* dem)
{
	m_List.AddTail(dem);
}

// -------------------------------------------------------------------------- //

LBDem* LBDemList::GetAt(int index)
{
	return m_List.GetAt(m_List.FindIndex(index));
}

// -------------------------------------------------------------------------- //

void LBDemList::SetAt(int index, LBDem* dem)
{
	POSITION pos = m_List.FindIndex(index);
	LBDem* oldDem = m_List.GetAt(pos);
	m_List.SetAt(pos, new LBDem(dem));
	delete dem;
	delete oldDem;
}

// -------------------------------------------------------------------------- //

void LBDemList::RemoveAt(int index)
{
	CTypedPtrList<CObList, LBDem*> tempList;

	// sets the copy of the list
	// discarding the item to remove
	POSITION pos = m_List.GetHeadPosition();
	int counter = 0;
	while(pos)
	{
		LBDem* item = m_List.GetNext(pos);
		if(counter != index)
		{
			LBDem* copyItem = new LBDem(item);
			tempList.AddTail(copyItem);
		}
		counter++;
	}

	// sets the list to be the temp list
	SetList(tempList);

	// clears the temp list
	pos = tempList.GetHeadPosition();
	while(pos)
	{
		delete tempList.GetNext(pos);
	}
	tempList.RemoveAll();
}

// -------------------------------------------------------------------------- //

void LBDemList::RemoveAll()
{
	// removes all items from the list
	POSITION pos = m_List.GetHeadPosition();
	while(pos)
	{
		delete m_List.GetNext(pos);
	}
	m_List.RemoveAll();
}

// ************************************************************** //
// Interface                                                      //
// ************************************************************** //

INT_PTR LBDemList::GetCount()
{
	return m_List.GetCount();
}

// -------------------------------------------------------------------------- //

BOOL LBDemList::IsEmpty()
{
	return m_List.IsEmpty();
}

// -------------------------------------------------------------------------- //

LBErrorTypes LBDemList::Validate()
{
	POSITION pos = m_List.GetHeadPosition();
	while(pos)
	{
		LBDem* item = m_List.GetNext(pos);
		LBErrorTypes res = item->Validate();
		if(res != LBET_NOERROR)
		{
			return res;
		}
	}
	return LBET_NOERROR;
}

// ************************************************************** //
// Serialization                                                  //
// ************************************************************** //

void LBDemList::Serialize(CArchive& ar)
{
	CObject::Serialize(ar);

	m_List.Serialize(ar);

	if(ar.IsStoring())
	{
		ar << m_Version;
	}
	else
	{
		ar >> m_Version;
	}
}

// -------------------------------------------------------------------------- //
