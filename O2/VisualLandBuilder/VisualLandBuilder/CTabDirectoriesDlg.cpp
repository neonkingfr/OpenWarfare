// CDirectoriesDlg.cpp : implementation file
//

//-----------------------------------------------------------------------------

#include "stdafx.h"
#include ".\VisualLandBuilder.h"
#include ".\CTabDirectoriesDlg.h"
#include ".\External\FolderDialog\FolderDlg.h"

//-----------------------------------------------------------------------------

IMPLEMENT_DYNAMIC( CTabDirectoriesDlg, CDialog )

//-----------------------------------------------------------------------------

CTabDirectoriesDlg::CTabDirectoriesDlg( CWnd* parent /*=NULL*/ )
: CDialog( CTabDirectoriesDlg::IDD, parent )
, m_modified( false )
{
}

//-----------------------------------------------------------------------------

CTabDirectoriesDlg::~CTabDirectoriesDlg()
{
}

//-----------------------------------------------------------------------------

void CTabDirectoriesDlg::DoDataExchange( CDataExchange* dx )
{
	CDialog::DoDataExchange( dx );
	DDX_Control( dx, IDC_DIRECTORYLIST, m_directoryListCtrl );
}

//-----------------------------------------------------------------------------

BEGIN_MESSAGE_MAP( CTabDirectoriesDlg, CDialog )
	ON_NOTIFY( NM_CLICK, IDC_DIRECTORYLIST, &CTabDirectoriesDlg::OnNMClickDirectoryList )
	ON_BN_CLICKED( IDCHANGEBTN,        &CTabDirectoriesDlg::OnBnClickedChangeBtn )
	ON_BN_CLICKED( IDDEFAULTBTN,       &CTabDirectoriesDlg::OnBnClickedDefaultBtn )
	ON_BN_CLICKED( IDC_SAVEDEFAULTBTN, &CTabDirectoriesDlg::OnBnClickedSaveDefaultBtn )
	ON_NOTIFY( LVN_ITEMACTIVATE, IDC_DIRECTORYLIST, &CTabDirectoriesDlg::OnLvnItemActivateDirectoryList )
	ON_NOTIFY( LVN_KEYDOWN,      IDC_DIRECTORYLIST, &CTabDirectoriesDlg::OnLvnKeydownDirectoryList )
END_MESSAGE_MAP()

//-----------------------------------------------------------------------------

BOOL CTabDirectoriesDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// sets the list column header
	m_directoryListCtrl.InsertColumn( 0, string( HEAD_RESOURCE ).c_str(), LVCFMT_LEFT );
	m_directoryListCtrl.SetColumnWidth( 0, 90);
	m_directoryListCtrl.InsertColumn( 1, string( HEAD_PATH ).c_str(), LVCFMT_LEFT );
	m_directoryListCtrl.SetColumnWidth( 1, 260 );

	// sets the list style
	m_directoryListCtrl.SetExtendedStyle( LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT );

	return (TRUE);
}

//-----------------------------------------------------------------------------

void CTabDirectoriesDlg::UpdateDirectoryListCtrl()
{
	INT_PTR dirCount  = m_directoryList->GetCount();

  m_directoryListCtrl.DeleteAllItems();

	for ( int i = 0; i < dirCount; ++i )
	{
		LBDirectoryItem* item = m_directoryList->GetAt( i );
		m_directoryListCtrl.InsertItem( i, item->GetResource().c_str() );
	
		// modify the second column only if the data are changed
		string newPath = item->GetResourcePathName();
    m_directoryListCtrl.SetItem( i, 1, LVIF_TEXT, newPath.c_str(), 0, 0, 0, 0 );
	}
}

//-----------------------------------------------------------------------------

void CTabDirectoriesDlg::SetDirectoryList( LBDirectoryList* inputList )
{
	// sets the pointer to the original data
	m_directoryList = inputList;
}

//-----------------------------------------------------------------------------

void CTabDirectoriesDlg::SetDefaultDirectoryList( LBDirectoryList* list )
{
	list->RemoveAll();

	string resnameWorkingFolder = RESNAME_WORKINGFOLDER;

  string resnameLandBuilder2  = RESNAME_LANDBUILDER2;

#ifdef __ONLY_FOR_VBS__
	string defaultPathWorkingFolder = PATH_WORKINGFOLDER_VBS;
#else
	string defaultPathWorkingFolder = PATH_WORKINGFOLDER;
#endif

#ifdef __ONLY_FOR_VBS__
	string defaultPathLandBuilder2  = PATH_LANDBUILDER2_VBS;
#else
	string defaultPathLandBuilder2  = PATH_LANDBUILDER2;
#endif

	CString pathWorkingFolder = theApp.GetProfileString( _T( "Paths" ), resnameWorkingFolder.c_str(), defaultPathWorkingFolder.c_str() );
	CString pathLandBuilder2  = theApp.GetProfileString( _T( "Paths" ), resnameLandBuilder2.c_str(), defaultPathLandBuilder2.c_str() );

	list->Append( new LBDirectoryItem( resnameWorkingFolder, string( pathWorkingFolder ), LBDI_FOLDER, FILTER_WORKINGFOLDER, FILE_WORKINGFOLDER, EXT_WORKINGFOLDER ) );
  list->Append( new LBDirectoryItem( resnameLandBuilder2, string( pathLandBuilder2 ), LBDI_FILE, FILTER_LANDBUILDER2, FILE_LANDBUILDER2, EXT_LANDBUILDER2 ) );
}

//-----------------------------------------------------------------------------

void CTabDirectoriesDlg::OnNMClickDirectoryList( NMHDR* nmhdr, LRESULT* result )
{
	// gets the index of the selected item in the list
	int selItem = m_directoryListCtrl.GetNextItem( -1, LVNI_SELECTED );

	// enable/disable change button
	if ( selItem != -1 )
	{
		// something is selected
		GetDlgItem( IDCHANGEBTN )->EnableWindow( TRUE );
	}
	else
	{
		// no selection
		GetDlgItem( IDCHANGEBTN )->EnableWindow( FALSE );
	}

	*result = 0;
}

//-----------------------------------------------------------------------------

void CTabDirectoriesDlg::OnLvnItemActivateDirectoryList( NMHDR* nmhdr, LRESULT* result )
{
	OnBnClickedChangeBtn();

	*result = 0;
}

//-----------------------------------------------------------------------------

void CTabDirectoriesDlg::OnLvnKeydownDirectoryList( NMHDR* nmhdr, LRESULT* result )
{
	OnNMClickDirectoryList( nmhdr, result );

	*result = 0;
}

//-----------------------------------------------------------------------------

void CTabDirectoriesDlg::OnBnClickedDefaultBtn()
{
	SetDefaultDirectoryList( m_directoryList );
	UpdateDirectoryListCtrl();
}

//-----------------------------------------------------------------------------

void CTabDirectoriesDlg::OnBnClickedSaveDefaultBtn()
{
	INT_PTR dirCount  = m_directoryList->GetCount();
	for ( int i = 0; i < dirCount; ++i )
	{
		LBDirectoryItem* item = m_directoryList->GetAt( i );
		string resName = item->GetResource();
		string path = item->GetResourcePathName();

		bool res = theApp.WriteProfileString( _T( "Paths" ), resName.c_str(), path.c_str() );
		if ( !res )
		{
			AfxMessageBox( "Failed to write to registry", MB_OK | MB_ICONSTOP );
		}
	}
}

//-----------------------------------------------------------------------------

void CTabDirectoriesDlg::OnBnClickedChangeBtn()
{
	// gets the index of the selected item in the list control
	int selItem = m_directoryListCtrl.GetNextItem( -1, LVNI_SELECTED );
	// gets the corresponding item in the working directory list 
	LBDirectoryItem* item = m_directoryList->GetAt( selItem );

	string strFilter = item->GetResourceFilter();
	string strExt    = item->GetResourceExtension();
	string strFile   = item->GetResourcePathName();

	if ( item->GetResourceType() == LBDI_FILE )
	{
		// dialogs for files
		bool update = false;

		DWORD flags = OFN_HIDEREADONLY | OFN_FILEMUSTEXIST | OFN_EXTENSIONDIFFERENT;
		CFileDialog fileDlg( TRUE, strExt.c_str(), strFile.c_str(), flags, strFilter.c_str(), this );
		if ( fileDlg.DoModal() == IDOK )
		{
			if ( item->GetResourceType() == LBDI_FILE )
			{
				if ( item->GetResourceFileName() == string( fileDlg.GetFileTitle().MakeLower() ) )
				{
					if ( strExt == string( fileDlg.GetFileExt().MakeLower() ) )
					{
						update = true;
					}
				}
			}
			else
			{
				update = true;
			}
			// checkings
			if ( update )
			{
				CString path = fileDlg.GetPathName().MakeLower();
				LBDirectoryItem* item = m_directoryList->GetAt( selItem );
				item->SetResourcePathName( path );
				UpdateDirectoryListCtrl();
			}
			else
			{
				AfxGetMainWnd()->MessageBox( string( MSG_INVALIDSELECTION).c_str(), string( MSG_ERROR ).c_str(), MB_OK );
			}
		}
	}
	else
	{
    if ( strFile == "" ) { strFile = "c:\\"; }
		// dialogs for folders
		CFolderDialog folderDlg( "Select Folder", strFile.c_str(), this, BIF_RETURNONLYFSDIRS | BIF_USENEWUI );
		if ( folderDlg.DoModal() == IDOK )
		{
			CString path = CString( folderDlg.GetFolderPath() ).MakeLower();
			if ( path.Right( 1 ) != _T( "\\" ) )
			{
				path += "\\";
			}
			LBDirectoryItem* item = m_directoryList->GetAt( selItem );
			item->SetResourcePathName( path );
			UpdateDirectoryListCtrl();
		}
	}
}

//-----------------------------------------------------------------------------

BOOL CTabDirectoriesDlg::PreTranslateMessage( MSG* msg )
{
	// this is needed to prevent the dialog to hide when ENTER or ESC is pressed
	if ( msg->message == WM_KEYDOWN )
	{
		if ( msg->wParam == VK_RETURN || msg->wParam == VK_ESCAPE )
		{
			return (TRUE);
		}
	}
	return (CDialog::PreTranslateMessage( msg ));
}

//-----------------------------------------------------------------------------
