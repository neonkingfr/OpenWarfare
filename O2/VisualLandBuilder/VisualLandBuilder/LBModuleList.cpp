#include "stdafx.h"

#include ".\LBModuleList.h"

IMPLEMENT_SERIAL(LBModuleList, CObject, 1)

// ************************************************************** //
// Constructors                                                   //
// ************************************************************** //

LBModuleList::LBModuleList() 
: CObject()
, m_Version(VERSION_NUMBER)
{
}

// -------------------------------------------------------------------------- //

LBModuleList::LBModuleList(const LBModuleList& other)
: m_Version(other.m_Version)
{
	// copies the new data
	POSITION pos = other.m_List.GetHeadPosition();
	while(pos)
	{
		LBModule* item = new LBModule(other.m_List.GetNext(pos));
		m_List.AddTail(item);
	}
}

// ************************************************************** //
// Destructor                                                     //
// ************************************************************** //

LBModuleList::~LBModuleList()
{
	RemoveAll();
}

// ************************************************************** //
// Assignement                                                    //
// ************************************************************** //

LBModuleList& LBModuleList::operator = (const LBModuleList& other)
{
	m_Version = other.m_Version;

	// clears the current list
	RemoveAll();

	// copies the new data
	POSITION pos = other.m_List.GetHeadPosition();
	while(pos)
	{
		LBModule* item = new LBModule(other.m_List.GetNext(pos));
		m_List.AddTail(item);
	}

	return *this;
}

// ************************************************************** //
// Member variables getters                                       //
// ************************************************************** //

CTypedPtrList<CObList, LBModule*>& LBModuleList::GetList()
{
	return m_List;
}

// ************************************************************** //
// Member variables setters                                       //
// ************************************************************** //

void LBModuleList::SetList(CTypedPtrList<CObList, LBModule*>& newList)
{
	// clears the current list
	RemoveAll();

	// copies the new data
	POSITION pos = newList.GetHeadPosition();
	while(pos)
	{
		LBModule* item = new LBModule(newList.GetNext(pos));
		m_List.AddTail(item);
	}
}

// ************************************************************** //
// Member variables manipulators                                  //
// ************************************************************** //

void LBModuleList::Append(const LBModule& module)
{
	LBModule* item = new LBModule(module);
	m_List.AddTail(item);
}

// -------------------------------------------------------------------------- //

void LBModuleList::Append(LBModule* module)
{
	m_List.AddTail(module);
}

// -------------------------------------------------------------------------- //

LBModule* LBModuleList::GetAt(int index)
{
	return m_List.GetAt(m_List.FindIndex(index));
}

// -------------------------------------------------------------------------- //

void LBModuleList::SetAt(int index, LBModule* module)
{
	POSITION pos = m_List.FindIndex(index);
//	LBModule* oldModule = m_List.GetAt(pos);
	m_List.SetAt(pos, new LBModule(module));
	delete module;
}

// -------------------------------------------------------------------------- //

void LBModuleList::RemoveAt(int index)
{
	CTypedPtrList<CObList, LBModule*> tempList;

	// sets the copy of the list
	// discarding the item to remove
	POSITION pos = m_List.GetHeadPosition();
	int counter = 0;
	while(pos)
	{
		LBModule* item = m_List.GetNext(pos);
		if(counter != index)
		{
			LBModule* copyItem = new LBModule(item);
			tempList.AddTail(copyItem);
		}
		counter++;
	}

	// sets the list to be the temp list
	SetList(tempList);

	// clears the temp list
	pos = tempList.GetHeadPosition();
	while(pos)
	{
		delete tempList.GetNext(pos);
	}
	tempList.RemoveAll();
}

// -------------------------------------------------------------------------- //

void LBModuleList::RemoveAll()
{
	// removes all items from the list
	POSITION pos = m_List.GetHeadPosition();
	while(pos)
	{
		delete m_List.GetNext(pos);
	}
	m_List.RemoveAll();
}

// ************************************************************** //
// Interface                                                      //
// ************************************************************** //

INT_PTR LBModuleList::GetCount() const
{
	return m_List.GetCount();
}

// -------------------------------------------------------------------------- //

BOOL LBModuleList::IsEmpty() const
{
	return m_List.IsEmpty();
}

// -------------------------------------------------------------------------- //

bool LBModuleList::UsesDems() const
{
	// searches if there is any active module using dems
	POSITION pos = m_List.GetHeadPosition();
	while(pos)
	{
		LBModule* item = m_List.GetNext(pos);
		if(item->IsActive() && item->GetUseDems() != LBMUD_NONE)
		{
			return true;
		}
	}
	return false;
}

// -------------------------------------------------------------------------- //

bool LBModuleList::UsesShapefiles() const
{
	// searches if there is any active module using shapefiles
	POSITION pos = m_List.GetHeadPosition();
	while(pos)
	{
		LBModule* item = m_List.GetNext(pos);
		if(item->IsActive() && item->GetUseShapefile())
		{
			return true;
		}
	}
	return false;
}

// -------------------------------------------------------------------------- //

bool LBModuleList::ExportsTerrain() const
{
	// searches if there is any active module using dems
	POSITION pos = m_List.GetHeadPosition();
	while(pos)
	{
		LBModule* item = m_List.GetNext(pos);
		if(item->IsActive() && item->GetExportsTerrain())
		{
			return true;
		}
	}
	return false;
}

// -------------------------------------------------------------------------- //

LBModule* LBModuleList::GetActiveModule(int position) const
{
	POSITION pos = m_List.GetHeadPosition();
	int counter = 1;
	while(pos)
	{
		LBModule* item = m_List.GetNext(pos);
		if(item->IsActive())
		{
			if(counter == position) return item;
			counter ++;
		}
	}

	return NULL;
}

// -------------------------------------------------------------------------- //

int LBModuleList::GetLastActiveModuleIndex() const
{
	int last = -1;
	// searches for the last active module 
	POSITION pos = m_List.GetHeadPosition();
	int counter = 0;
	while(pos)
	{
		LBModule* item = m_List.GetNext(pos);
		if(item->IsActive())
		{
			last = counter;
		}
		counter++;
	}
	return last;
}

// -------------------------------------------------------------------------- //

int LBModuleList::GetActiveModuleCount(string moduleName) const
{
	POSITION pos = m_List.GetHeadPosition();
	int counter = 0;
	while(pos)
	{
		LBModule* item = m_List.GetNext(pos);
		if(item->IsActive() && item->GetVLBName() == moduleName)
		{
			counter++;
		}
	}
	return counter;
}

// -------------------------------------------------------------------------- //

int LBModuleList::GetActiveModuleUsingShapefileCount() const
{
	POSITION pos = m_List.GetHeadPosition();
	int counter = 0;
	while(pos)
	{
		LBModule* item = m_List.GetNext(pos);
		if(item->IsActive() && item->GetUseShapefile())
		{
			counter++;
		}
	}
	return counter;
}

// -------------------------------------------------------------------------- //

int LBModuleList::GetActiveModulesCount() const
{
	POSITION pos = m_List.GetHeadPosition();
	int counter = 0;
	while(pos)
	{
		LBModule* item = m_List.GetNext(pos);
		if(item->IsActive())
		{
			counter++;
		}
	}
	return counter;
}

// ************************************************************** //
// Serialization                                                  //
// ************************************************************** //

void LBModuleList::Serialize(CArchive& ar)
{
	CObject::Serialize(ar);

	m_List.Serialize(ar);

	if(ar.IsStoring())
	{
		ar << m_Version;
	}
	else
	{
		ar >> m_Version;
	}
}

// -------------------------------------------------------------------------- //
