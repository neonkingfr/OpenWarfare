#include "stdafx.h"

#include ".\LBObject.h"
#include ".\LBGlobalVariables.h"

IMPLEMENT_SERIAL(LBObject, CObject, 1)

// ************************************************************** //
// Constructors                                                   //
// ************************************************************** //

LBObject::LBObject() 
: CObject()
, m_Version(VERSION_NUMBER)
{
	m_PvFillColor   = COLOR_GREEN;
	m_PvBorderColor = COLOR_DARKGREEN;
}

// -------------------------------------------------------------------------- //

LBObject::LBObject(const string& name) 
: CObject()
, m_Version(VERSION_NUMBER)
, m_Name(name)
{
	m_PvFillColor   = COLOR_GREEN;
	m_PvBorderColor = COLOR_DARKGREEN;
}

// -------------------------------------------------------------------------- //

LBObject::LBObject(const string& name, 
				   const LBParameterList& parametersList) 
: CObject()
, m_Version(VERSION_NUMBER)
, m_Name(name)
, m_PvFillColor(COLOR_GREEN)
, m_PvBorderColor(COLOR_DARKGREEN)
, m_ParametersList(parametersList)
{
}

// -------------------------------------------------------------------------- //

LBObject::LBObject(LBObject* other) 
: m_Version(other->m_Version)
, m_Name(other->m_Name)
, m_PvFillColor(other->m_PvFillColor)
, m_PvBorderColor(other->m_PvBorderColor)
, m_ParametersList(other->m_ParametersList)
{
}

// -------------------------------------------------------------------------- //

LBObject::LBObject(const LBObject& other) 
: m_Version(other.m_Version)
, m_Name(other.m_Name)
, m_PvFillColor(other.m_PvFillColor)
, m_PvBorderColor(other.m_PvBorderColor)
, m_ParametersList(other.m_ParametersList)
{
}

// ************************************************************** //
// Destructor                                                     //
// ************************************************************** //

LBObject::~LBObject()
{
}

// ************************************************************** //
// Assignement                                                    //
// ************************************************************** //
LBObject& LBObject::operator = (const LBObject& other)
{
	m_Version        = other.m_Version;
	m_Name           = other.m_Name;
	m_PvFillColor    = other.m_PvFillColor;
	m_PvBorderColor  = other.m_PvBorderColor;
	m_ParametersList = other.m_ParametersList;

	return *this;
}

// ************************************************************** //
// Member variables getters                                       //
// ************************************************************** //
	
string LBObject::GetName() const 
{ 
	return m_Name; 
}

// -------------------------------------------------------------------------- //

LBParameterList& LBObject::GetParametersList() 
{
	return m_ParametersList;
}

// -------------------------------------------------------------------------- //

LBPreviewColors LBObject::GetPvFillColor() const
{
	return m_PvFillColor;
}

// -------------------------------------------------------------------------- //

string LBObject::GetPvFillColorAsString() const
{
	char buf[80];
	_itoa_s(m_PvFillColor, buf, 80, 10);
	return string(buf);
}

// -------------------------------------------------------------------------- //

LBPreviewColors LBObject::GetPvBorderColor() const
{
	return m_PvBorderColor;
}

// -------------------------------------------------------------------------- //

string LBObject::GetPvBorderColorAsString() const 
{
	char buf[80];
	_itoa_s(m_PvBorderColor, buf, 80, 10);
	return string(buf);
}

// ************************************************************** //
// Member variables setters                                       //
// ************************************************************** //

void LBObject::SetName(const string& newName) 
{ 
	m_Name = newName; 
}

// -------------------------------------------------------------------------- //

void LBObject::SetName(const CString& newName) 
{ 
	m_Name = newName; 
}

// -------------------------------------------------------------------------- //

void LBObject::SetParameterList(const LBParameterList& parametersList)
{
	m_ParametersList = parametersList;
}

// -------------------------------------------------------------------------- //

void LBObject::SetPvFillColor(LBPreviewColors newPvFillColor)
{
	m_PvFillColor = newPvFillColor;
}

// -------------------------------------------------------------------------- //

void LBObject::SetPvBorderColor(LBPreviewColors newPvBorderColor)
{
	m_PvBorderColor = newPvBorderColor;
}

// ************************************************************** //
// Interface                                                      //
// ************************************************************** //

LBErrorTypes LBObject::Validate()
{
	// checks if all parameters have been set to a value
	if(!m_ParametersList.Validate())
	{
		return LBET_MISSINGOBJECTPARAMETER;
	}

	return LBET_NOERROR;
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::AdvancedRandomPlacer::ObjectInput LBObject::AdvancedRandomPlacerObjectInput()
{
	if(m_Name != OBJ_NAME_FROMDBF)
	{
		RString name = m_Name.c_str();

		double prob = 100.0;   // defalt value
		string strProb = m_ParametersList.GetValue(PARNAME_PROB);
		if(strProb != "")
		{
			prob = atof(strProb.c_str());
		}

		double minHeight = 100.0;   // defalt value
		string strMinH = m_ParametersList.GetValue(PARNAME_MINHEIGHT);
		if(strMinH != "")
		{
			minHeight = atof(strMinH.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		string strMaxH = m_ParametersList.GetValue(PARNAME_MAXHEIGHT);
		if(strMaxH != "")
		{
			maxHeight = atof(strMaxH.c_str());
		}

		double minDistance = 5.0;   // defalt value
		string strMinDistance = m_ParametersList.GetValue(PARNAME_MINDISTANCE);
		if(strMinDistance != "")
		{
			minDistance = atof(strMinDistance.c_str());
		}

		return LandBuildInt::Modules::AdvancedRandomPlacer::ObjectInput(name, prob, minHeight, maxHeight, minDistance);
	}
	else
	{
		return LandBuildInt::Modules::AdvancedRandomPlacer::ObjectInput("", 0, 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::AdvancedRandomPlacer::ObjectInput LBObject::AdvancedRandomPlacerObjectInput(int shapeIndex, LBDbValuesFunctor& functor)
{
	if(m_Name == OBJ_NAME_FROMDBF)
	{
		// in this case all parameters contain the object index 
		string strIndex       = m_ParametersList.GetValue(PARNAME_PROB);
		string strObject      = PARNAME_OBJECT + strIndex;
		string strProb        = PARNAME_PROB + strIndex;
		string strMinHeight   = PARNAME_MINHEIGHT + strIndex;
		string strMaxHeight   = PARNAME_MAXHEIGHT + strIndex;
		string strMinDistance = PARNAME_MINDISTANCE + strIndex;

		string value = functor.GetValue(shapeIndex, strObject);
		RString name = value.c_str();

		double prob = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strProb);
		if(value != "")
		{
			prob = atof(value.c_str());
		}

		double minHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMinHeight);
		if(value != "")
		{
			minHeight = atof(value.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMaxHeight);
		if(value != "")
		{
			maxHeight = atof(value.c_str());
		}

		double minDistance = 5.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMinDistance);
		if(value != "")
		{
			minDistance = atof(value.c_str());
		}

		return LandBuildInt::Modules::AdvancedRandomPlacer::ObjectInput(name, prob, minHeight, maxHeight, minDistance);
	}
	else
	{
		return LandBuildInt::Modules::AdvancedRandomPlacer::ObjectInput("", 0, 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::AdvancedRandomPlacerSlope::ObjectInput LBObject::AdvancedRandomPlacerSlopeObjectInput()
{
	if(m_Name != OBJ_NAME_FROMDBF)
	{
		RString name = m_Name.c_str();

		double prob = 100.0;   // defalt value
		string strProb = m_ParametersList.GetValue(PARNAME_PROB);
		if(strProb != "")
		{
			prob = atof(strProb.c_str());
		}

		double minHeight = 100.0;   // defalt value
		string strMinH = m_ParametersList.GetValue(PARNAME_MINHEIGHT);
		if(strMinH != "")
		{
			minHeight = atof(strMinH.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		string strMaxH = m_ParametersList.GetValue(PARNAME_MAXHEIGHT);
		if(strMaxH != "")
		{
			maxHeight = atof(strMaxH.c_str());
		}

		double minDistance = 5.0;   // defalt value
		string strMinDistance = m_ParametersList.GetValue(PARNAME_MINDISTANCE);
		if(strMinDistance != "")
		{
			minDistance = atof(strMinDistance.c_str());
		}

		double minSlope;
		string strMinS = m_ParametersList.GetValue(PARNAME_MINSLOPE);
		if(strMinS != "")
		{
			minSlope = atof(strMinS.c_str());
		}

		double maxSlope;
		string strMaxS = m_ParametersList.GetValue(PARNAME_MAXSLOPE);
		if(strMaxS != "")
		{
			maxSlope = atof(strMaxS.c_str());
		}

		return LandBuildInt::Modules::AdvancedRandomPlacerSlope::ObjectInput(name, prob, minHeight, maxHeight, minDistance, minSlope, maxSlope);
	}
	else
	{
		return LandBuildInt::Modules::AdvancedRandomPlacerSlope::ObjectInput("", 0, 0, 0, 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::AdvancedRandomPlacerSlope::ObjectInput LBObject::AdvancedRandomPlacerSlopeObjectInput(int shapeIndex, LBDbValuesFunctor& functor)
{
	if (m_Name == OBJ_NAME_FROMDBF)
	{
		// in this case all parameters contain the object index 
		string strIndex       = m_ParametersList.GetValue(PARNAME_PROB);
		string strObject      = PARNAME_OBJECT + strIndex;
		string strProb        = PARNAME_PROB + strIndex;
		string strMinHeight   = PARNAME_MINHEIGHT + strIndex;
		string strMaxHeight   = PARNAME_MAXHEIGHT + strIndex;
		string strMinDistance = PARNAME_MINDISTANCE + strIndex;
		string strMinSlope    = PARNAME_MINSLOPE + strIndex;
		string strMaxSlope    = PARNAME_MAXSLOPE + strIndex;

		string value = functor.GetValue(shapeIndex, strObject);
		RString name = value.c_str();

		double prob = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strProb);
		if (value != "")
		{
			prob = atof(value.c_str());
		}

		double minHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMinHeight);
		if (value != "")
		{
			minHeight = atof(value.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMaxHeight);
		if (value != "")
		{
			maxHeight = atof(value.c_str());
		}

		double minDistance = 5.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMinDistance);
		if (value != "")
		{
			minDistance = atof(value.c_str());
		}

		double minSlope;
		value = functor.GetValue(shapeIndex, strMinSlope);
		if (value != "")
		{
			minSlope = atof(value.c_str());
		}

		double maxSlope;
		value = functor.GetValue(shapeIndex, strMaxSlope);
		if (value != "")
		{
			maxSlope = atof(value.c_str());
		}

		return LandBuildInt::Modules::AdvancedRandomPlacerSlope::ObjectInput(name, prob, minHeight, maxHeight, minDistance, minSlope, maxSlope);
	}
	else
	{
		return LandBuildInt::Modules::AdvancedRandomPlacerSlope::ObjectInput("", 0, 0, 0, 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::RandomOnPathPlacer::ObjectInput LBObject::RandomOnPathPlacerObjectInput()
{
	if (m_Name != OBJ_NAME_FROMDBF)
	{
		RString name = m_Name.c_str();

		double minHeight = 100.0;   // defalt value
		string strMinH = m_ParametersList.GetValue(PARNAME_MINHEIGHT);
		if (strMinH != "")
		{
			minHeight = atof(strMinH.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		string strMaxH = m_ParametersList.GetValue(PARNAME_MAXHEIGHT);
		if (strMaxH != "")
		{
			maxHeight = atof(strMaxH.c_str());
		}

		double prob = 100.0;   // defalt value
		string strProb = m_ParametersList.GetValue(PARNAME_PROB);
		if (strProb != "")
		{
			prob = atof(strProb.c_str());
		}

		return LandBuildInt::Modules::RandomOnPathPlacer::ObjectInput(name, minHeight, maxHeight, prob);
	}
	else
	{
		return LandBuildInt::Modules::RandomOnPathPlacer::ObjectInput("", 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::RandomOnPathPlacer::ObjectInput LBObject::RandomOnPathPlacerObjectInput(int shapeIndex, LBDbValuesFunctor& functor)
{
	if (m_Name == OBJ_NAME_FROMDBF)
	{
		// in this case all parameters contain the object index 
		string strIndex     = m_ParametersList.GetValue(PARNAME_PROB);
		string strObject    = PARNAME_OBJECT + strIndex;
		string strProb      = PARNAME_PROB + strIndex;
		string strMinHeight = PARNAME_MINHEIGHT + strIndex;
		string strMaxHeight = PARNAME_MAXHEIGHT + strIndex;

		string value = functor.GetValue(shapeIndex, strObject);
		RString name = value.c_str();

		double minHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMinHeight);
		if (value != "")
		{
			minHeight = atof(value.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMaxHeight);
		if (value != "")
		{
			maxHeight = atof(value.c_str());
		}

		double prob = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strProb);
		if (value != "")
		{
			prob = atof(value.c_str());
		}

		return LandBuildInt::Modules::RandomOnPathPlacer::ObjectInput(name, minHeight, maxHeight, prob);
	}
	else
	{
		return LandBuildInt::Modules::RandomOnPathPlacer::ObjectInput("", 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::RegularOnPathPlacer::ObjectInput LBObject::RegularOnPathPlacerObjectInput()
{
	if (m_Name != OBJ_NAME_FROMDBF)
	{
		RString name = m_Name.c_str();

		double minHeight = 100.0;   // defalt value
		string strMinH = m_ParametersList.GetValue(PARNAME_MINHEIGHT);
		if (strMinH != "")
		{
			minHeight = atof(strMinH.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		string strMaxH = m_ParametersList.GetValue(PARNAME_MAXHEIGHT);
		if (strMaxH != "")
		{
			maxHeight = atof(strMaxH.c_str());
		}

		bool matchOrient = true;   // defalt value
		string strMatchOrient = m_ParametersList.GetValue(PARNAME_MATCHORIENT);
		if (strMatchOrient != "")
		{
			int matchOrientInt = atoi(strMatchOrient.c_str());
			if (matchOrientInt == 0)
			{
				matchOrient = false;
			}
		}

		double dirCorrection = 0.0;   // defalt value
		string strDirC = m_ParametersList.GetValue(PARNAME_DIRCORRECTION);
		if (strDirC != "")
		{
			dirCorrection = atof(strDirC.c_str());
		}

		bool placeRight = true;   // defalt value
		string strPlaceRight = m_ParametersList.GetValue(PARNAME_PLACERIGHT);
		if (strPlaceRight != "")
		{
			int placeRightInt = atoi(strPlaceRight.c_str());
			if (placeRightInt == 0)
			{
				placeRight = false;
			}
		}

		bool placeLeft = true;   // defalt value
		string strPlaceLeft = m_ParametersList.GetValue(PARNAME_PLACELEFT);
		if (strPlaceLeft != "")
		{
			int placeLeftInt = atoi(strPlaceLeft.c_str());
			if (placeLeftInt == 0)
			{
				placeLeft = false;
			}
		}

		return LandBuildInt::Modules::RegularOnPathPlacer::ObjectInput(name, minHeight, maxHeight, matchOrient, dirCorrection, placeRight, placeLeft);
	}
	else
	{
		return LandBuildInt::Modules::RegularOnPathPlacer::ObjectInput("", 0, 0, 0, 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::RegularOnPathPlacer::ObjectInput LBObject::RegularOnPathPlacerObjectInput(int shapeIndex, LBDbValuesFunctor& functor)
{
	if (m_Name == OBJ_NAME_FROMDBF)
	{
		// in this case all parameters contain the object index 
		string strIndex         = m_ParametersList.GetValue(PARNAME_PROB);
		string strObject        = PARNAME_OBJECT + strIndex;
		string strMinHeight     = PARNAME_MINHEIGHT + strIndex;
		string strMaxHeight     = PARNAME_MAXHEIGHT + strIndex;
		string strMatchOrient   = PARNAME_MATCHORIENT + strIndex;
		string strDirCorrection = PARNAME_DIRCORRECTION + strIndex;
		string strPlaceRight    = PARNAME_PLACERIGHT + strIndex;
		string strPlaceLeft     = PARNAME_PLACELEFT+ strIndex;

		string value = functor.GetValue(shapeIndex, strObject);
		RString name = value.c_str();

		double minHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMinHeight);
		if (value != "")
		{
			minHeight = atof(value.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMaxHeight);
		if (value != "")
		{
			maxHeight = atof(value.c_str());
		}

		bool matchOrient = true;   // defalt value
		value = functor.GetValue(shapeIndex, strMatchOrient);
		if (value != "")
		{
			int matchOrientInt = atoi(value.c_str());
			if (matchOrientInt == 0)
			{
				matchOrient = false;
			}
		}

		double dirCorrection = 0.0;   // defalt value
		value = functor.GetValue(shapeIndex, strDirCorrection);
		if (value != "")
		{
			dirCorrection = atof(value.c_str());
		}

		bool placeRight = true;   // defalt value
		value = functor.GetValue(shapeIndex, strPlaceRight);
		if (value != "")
		{
			int placeRightInt = atoi(value.c_str());
			if (placeRightInt == 0)
			{
				placeRight = false;
			}
		}

		bool placeLeft = true;   // defalt value
		value = functor.GetValue(shapeIndex, strPlaceLeft);
		if (value != "")
		{
			int placeLeftInt = atoi(value.c_str());
			if (placeLeftInt == 0)
			{
				placeLeft = false;
			}
		}

		return LandBuildInt::Modules::RegularOnPathPlacer::ObjectInput(name, minHeight, maxHeight, matchOrient, dirCorrection, placeRight, placeLeft);
	}
	else
	{
		return LandBuildInt::Modules::RegularOnPathPlacer::ObjectInput("", 0, 0, 0, 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::BiasedRandomPlacer::ObjectInput LBObject::BiasedRandomPlacerObjectInput()
{
	if (m_Name != OBJ_NAME_FROMDBF)
	{
		RString name = m_Name.c_str();

		double minHeight = 100.0;   // defalt value
		string strMinH = m_ParametersList.GetValue(PARNAME_MINHEIGHT);
		if (strMinH != "")
		{
			minHeight = atof(strMinH.c_str());
		}
		
		double maxHeight = 100.0;   // defalt value
		string strMaxH = m_ParametersList.GetValue(PARNAME_MAXHEIGHT);
		if (strMaxH != "")
		{
			maxHeight = atof(strMaxH.c_str());
		}
		
		double prob = 100.0;   // defalt value
		string strProb = m_ParametersList.GetValue(PARNAME_PROB);
		if (strProb != "")
		{
			prob = atof(strProb.c_str());
		}

		double minDistance = 5.0;   // defalt value
		string strMinDistance = m_ParametersList.GetValue(PARNAME_MINDISTANCE);
		if (strMinDistance != "")
		{
			minDistance = atof(strMinDistance.c_str());
		}

		return LandBuildInt::Modules::BiasedRandomPlacer::ObjectInput(name, minHeight, maxHeight, prob, minDistance);
	}
	else
	{
		return LandBuildInt::Modules::BiasedRandomPlacer::ObjectInput("", 0, 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::BiasedRandomPlacer::ObjectInput LBObject::BiasedRandomPlacerObjectInput(int shapeIndex, LBDbValuesFunctor& functor)
{
	if (m_Name == OBJ_NAME_FROMDBF)
	{
		// in this case all parameters contain the object index 
		string strIndex       = m_ParametersList.GetValue(PARNAME_PROB);
		string strObject      = PARNAME_OBJECT + strIndex;
		string strProb        = PARNAME_PROB + strIndex;
		string strMinHeight   = PARNAME_MINHEIGHT + strIndex;
		string strMaxHeight   = PARNAME_MAXHEIGHT + strIndex;
		string strMinDistance = PARNAME_MINDISTANCE + strIndex;

		string value = functor.GetValue(shapeIndex, strObject);
		RString name = value.c_str();

		double minHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMinHeight);
		if (value != "")
		{
			minHeight = atof(value.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMaxHeight);
		if (value != "")
		{
			maxHeight = atof(value.c_str());
		}

		double prob = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strProb);
		if (value != "")
		{
			prob = atof(value.c_str());
		}

		double minDistance = 5.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMinDistance);
		if (value != "")
		{
			minDistance = atof(value.c_str());
		}

		return LandBuildInt::Modules::BiasedRandomPlacer::ObjectInput(name, minHeight, maxHeight, prob, minDistance);
	}
	else
	{
		return LandBuildInt::Modules::BiasedRandomPlacer::ObjectInput("", 0, 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::BiasedRandomPlacerSlope::ObjectInput LBObject::BiasedRandomPlacerSlopeObjectInput()
{
	if(m_Name != OBJ_NAME_FROMDBF)
	{
		RString name = m_Name.c_str();

		double minHeight = 100.0;   // defalt value
		string strMinH = m_ParametersList.GetValue(PARNAME_MINHEIGHT);
		if(strMinH != "")
		{
			minHeight = atof(strMinH.c_str());
		}
		
		double maxHeight = 100.0;   // defalt value
		string strMaxH = m_ParametersList.GetValue(PARNAME_MAXHEIGHT);
		if(strMaxH != "")
		{
			maxHeight = atof(strMaxH.c_str());
		}
		
		double prob = 100.0;   // defalt value
		string strProb = m_ParametersList.GetValue(PARNAME_PROB);
		if(strProb != "")
		{
			prob = atof(strProb.c_str());
		}

		double minDistance = 100.0;   // defalt value
		string strMinDistance = m_ParametersList.GetValue(PARNAME_MINDISTANCE);
		if(strMinDistance != "")
		{
			minDistance = atof(strMinDistance.c_str());
		}

		double minSlope;
		string strMinS = m_ParametersList.GetValue(PARNAME_MINSLOPE);
		if(strMinS != "")
		{
			minSlope = atof(strMinS.c_str());
		}
		double maxSlope;
		string strMaxS = m_ParametersList.GetValue(PARNAME_MAXSLOPE);
		if(strMaxS != "")
		{
			maxSlope = atof(strMaxS.c_str());
		}

		return LandBuildInt::Modules::BiasedRandomPlacerSlope::ObjectInput(name, minHeight, maxHeight, prob, minDistance, minSlope, maxSlope);
	}
	else
	{
		return LandBuildInt::Modules::BiasedRandomPlacerSlope::ObjectInput("", 0, 0, 0, 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::BiasedRandomPlacerSlope::ObjectInput LBObject::BiasedRandomPlacerSlopeObjectInput(int shapeIndex, LBDbValuesFunctor& functor)
{
	if(m_Name == OBJ_NAME_FROMDBF)
	{
		// in this case all parameters contain the object index 
		string strIndex       = m_ParametersList.GetValue(PARNAME_PROB);
		string strObject      = PARNAME_OBJECT + strIndex;
		string strProb        = PARNAME_PROB + strIndex;
		string strMinHeight   = PARNAME_MINHEIGHT + strIndex;
		string strMaxHeight   = PARNAME_MAXHEIGHT + strIndex;
		string strMinDistance = PARNAME_MINDISTANCE + strIndex;
		string strMinSlope    = PARNAME_MINSLOPE + strIndex;
		string strMaxSlope    = PARNAME_MAXSLOPE + strIndex;

		string value = functor.GetValue(shapeIndex, strObject);
		RString name = value.c_str();

		double minHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMinHeight);
		if(value != "")
		{
			minHeight = atof(value.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMaxHeight);
		if(value != "")
		{
			maxHeight = atof(value.c_str());
		}

		double prob = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strProb);
		if(value != "")
		{
			prob = atof(value.c_str());
		}

		double minDistance = 5.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMinDistance);
		if(value != "")
		{
			minDistance = atof(value.c_str());
		}

		double minSlope;
		value = functor.GetValue(shapeIndex, strMinSlope);
		if(value != "")
		{
			minSlope = atof(value.c_str());
		}

		double maxSlope;
		value = functor.GetValue(shapeIndex, strMaxSlope);
		if(value != "")
		{
			maxSlope = atof(value.c_str());
		}

		return LandBuildInt::Modules::BiasedRandomPlacerSlope::ObjectInput(name, minHeight, maxHeight, prob, minDistance, minSlope, maxSlope);
	}
	else
	{
		return LandBuildInt::Modules::BiasedRandomPlacerSlope::ObjectInput("", 0, 0, 0, 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::SimpleRandomPlacer::ObjectInput LBObject::SimpleRandomPlacerObjectInput()
{
	if(m_Name != OBJ_NAME_FROMDBF)
	{
		RString name = m_Name.c_str();

		return LandBuildInt::Modules::SimpleRandomPlacer::ObjectInput(name);
	}
	else
	{
		return LandBuildInt::Modules::SimpleRandomPlacer::ObjectInput("");
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::SimpleRandomPlacer::ObjectInput LBObject::SimpleRandomPlacerObjectInput(int shapeIndex, LBDbValuesFunctor& functor)
{
	if(m_Name == OBJ_NAME_FROMDBF)
	{
		string strObject = PARNAME_OBJECT;

		string value = functor.GetValue(shapeIndex, strObject);
		RString name = value.c_str();

		return LandBuildInt::Modules::SimpleRandomPlacer::ObjectInput(name);
	}
	else
	{
		return LandBuildInt::Modules::SimpleRandomPlacer::ObjectInput("");
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::SimpleRandomPlacerSlope::ObjectInput LBObject::SimpleRandomPlacerSlopeObjectInput()
{
	if(m_Name != OBJ_NAME_FROMDBF)
	{
		RString name = m_Name.c_str();
		double minSlope;
		string strMinS = m_ParametersList.GetValue(PARNAME_MINSLOPE);
		if(strMinS != "")
		{
			minSlope = atof(strMinS.c_str());
		}
		double maxSlope;
		string strMaxS = m_ParametersList.GetValue(PARNAME_MAXSLOPE);
		if(strMaxS != "")
		{
			maxSlope = atof(strMaxS.c_str());
		}

		return LandBuildInt::Modules::SimpleRandomPlacerSlope::ObjectInput(name, minSlope, maxSlope);
	}
	else
	{
		return LandBuildInt::Modules::SimpleRandomPlacerSlope::ObjectInput("", 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::SimpleRandomPlacerSlope::ObjectInput LBObject::SimpleRandomPlacerSlopeObjectInput(int shapeIndex, LBDbValuesFunctor& functor)
{
	if(m_Name == OBJ_NAME_FROMDBF)
	{
		string strObject   = PARNAME_OBJECT;
		string strMinSlope = PARNAME_MINSLOPE;
		string strMaxSlope = PARNAME_MAXSLOPE;

		string value = functor.GetValue(shapeIndex, strObject);
		RString name = value.c_str();

		double minSlope;
		value = functor.GetValue(shapeIndex, strMinSlope);
		if(value != "")
		{
			minSlope = atof(value.c_str());
		}
		double maxSlope;
		value = functor.GetValue(shapeIndex, strMaxSlope);
		if(value != "")
		{
			maxSlope = atof(value.c_str());
		}

		return LandBuildInt::Modules::SimpleRandomPlacerSlope::ObjectInput(name, minSlope, maxSlope);
	}
	else
	{
		return LandBuildInt::Modules::SimpleRandomPlacerSlope::ObjectInput("", 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::SubSquaresRandomPlacer::ObjectInput LBObject::SubSquaresRandomPlacerObjectInput()
{
	if(m_Name != OBJ_NAME_FROMDBF)
	{
		RString name = m_Name.c_str();

		double prob = 100.0;   // defalt value
		string strProb = m_ParametersList.GetValue(PARNAME_PROB);
		if(strProb != "")
		{
			prob = atof(strProb.c_str());
		}

		double minHeight = 100.0;   // defalt value
		string strMinH = m_ParametersList.GetValue(PARNAME_MINHEIGHT);
		if(strMinH != "")
		{
			minHeight = atof(strMinH.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		string strMaxH = m_ParametersList.GetValue(PARNAME_MAXHEIGHT);
		if(strMaxH != "")
		{
			maxHeight = atof(strMaxH.c_str());
		}

		return LandBuildInt::Modules::SubSquaresRandomPlacer::ObjectInput(name, prob, minHeight, maxHeight);
	}
	else
	{
		return LandBuildInt::Modules::SubSquaresRandomPlacer::ObjectInput("", 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::SubSquaresRandomPlacer::ObjectInput LBObject::SubSquaresRandomPlacerObjectInput(int shapeIndex, LBDbValuesFunctor& functor)
{
	if(m_Name == OBJ_NAME_FROMDBF)
	{
		// in this case all parameters contain the object index 
		string strIndex       = m_ParametersList.GetValue(PARNAME_PROB);
		string strObject      = PARNAME_OBJECT + strIndex;
		string strProb        = PARNAME_PROB + strIndex;
		string strMinHeight   = PARNAME_MINHEIGHT + strIndex;
		string strMaxHeight   = PARNAME_MAXHEIGHT + strIndex;

		string value = functor.GetValue(shapeIndex, strObject);
		RString name = value.c_str();

		double prob = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strProb);
		if(value != "")
		{
			prob = atof(value.c_str());
		}

		double minHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMinHeight);
		if(value != "")
		{
			minHeight = atof(value.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMaxHeight);
		if(value != "")
		{
			maxHeight = atof(value.c_str());
		}

		return LandBuildInt::Modules::SubSquaresRandomPlacer::ObjectInput(name, prob, minHeight, maxHeight);
	}
	else
	{
		return LandBuildInt::Modules::SubSquaresRandomPlacer::ObjectInput("", 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::InnerSubSquaresFramePlacer::ObjectInput LBObject::InnerSubSquaresFramePlacerObjectInput()
{
	if(m_Name != OBJ_NAME_FROMDBF)
	{
		RString name = m_Name.c_str();

		double prob = 100.0;   // defalt value
		string strProb = m_ParametersList.GetValue(PARNAME_PROB);
		if(strProb != "")
		{
			prob = atof(strProb.c_str());
		}

		double minHeight = 100.0;   // defalt value
		string strMinH = m_ParametersList.GetValue(PARNAME_MINHEIGHT);
		if(strMinH != "")
		{
			minHeight = atof(strMinH.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		string strMaxH = m_ParametersList.GetValue(PARNAME_MAXHEIGHT);
		if(strMaxH != "")
		{
			maxHeight = atof(strMaxH.c_str());
		}

		return LandBuildInt::Modules::InnerSubSquaresFramePlacer::ObjectInput(name, prob, minHeight, maxHeight);
	}
	else
	{
		return LandBuildInt::Modules::InnerSubSquaresFramePlacer::ObjectInput("", 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::InnerSubSquaresFramePlacer::ObjectInput LBObject::InnerSubSquaresFramePlacerObjectInput(int shapeIndex, LBDbValuesFunctor& functor)
{
	if(m_Name == OBJ_NAME_FROMDBF)
	{
		// in this case all parameters contain the object index 
		string strIndex       = m_ParametersList.GetValue(PARNAME_PROB);
		string strObject      = PARNAME_OBJECT + strIndex;
		string strProb        = PARNAME_PROB + strIndex;
		string strMinHeight   = PARNAME_MINHEIGHT + strIndex;
		string strMaxHeight   = PARNAME_MAXHEIGHT + strIndex;

		string value = functor.GetValue(shapeIndex, strObject);
		RString name = value.c_str();

		double prob = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strProb);
		if(value != "")
		{
			prob = atof(value.c_str());
		}

		double minHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMinHeight);
		if(value != "")
		{
			minHeight = atof(value.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMaxHeight);
		if(value != "")
		{
			maxHeight = atof(value.c_str());
		}

		return LandBuildInt::Modules::InnerSubSquaresFramePlacer::ObjectInput(name, prob, minHeight, maxHeight);
	}
	else
	{
		return LandBuildInt::Modules::InnerSubSquaresFramePlacer::ObjectInput("", 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::BoundarySubSquaresFramePlacer::ObjectInput LBObject::BoundarySubSquaresFramePlacerObjectInput()
{
	if(m_Name != OBJ_NAME_FROMDBF)
	{
		RString name = m_Name.c_str();

		double prob = 100.0;   // defalt value
		string strProb = m_ParametersList.GetValue(PARNAME_PROB);
		if(strProb != "")
		{
			prob = atof(strProb.c_str());
		}

		double minHeight = 100.0;   // defalt value
		string strMinH = m_ParametersList.GetValue(PARNAME_MINHEIGHT);
		if(strMinH != "")
		{
			minHeight = atof(strMinH.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		string strMaxH = m_ParametersList.GetValue(PARNAME_MAXHEIGHT);
		if(strMaxH != "")
		{
			maxHeight = atof(strMaxH.c_str());
		}

		return LandBuildInt::Modules::BoundarySubSquaresFramePlacer::ObjectInput(name, prob, minHeight, maxHeight);
	}
	else
	{
		return LandBuildInt::Modules::BoundarySubSquaresFramePlacer::ObjectInput("", 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::BoundarySubSquaresFramePlacer::ObjectInput LBObject::BoundarySubSquaresFramePlacerObjectInput(int shapeIndex, LBDbValuesFunctor& functor)
{
	if(m_Name == OBJ_NAME_FROMDBF)
	{
		// in this case all parameters contain the object index 
		string strIndex       = m_ParametersList.GetValue(PARNAME_PROB);
		string strObject      = PARNAME_OBJECT + strIndex;
		string strProb        = PARNAME_PROB + strIndex;
		string strMinHeight   = PARNAME_MINHEIGHT + strIndex;
		string strMaxHeight   = PARNAME_MAXHEIGHT + strIndex;

		string value = functor.GetValue(shapeIndex, strObject);
		RString name = value.c_str();

		double prob = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strProb);
		if(value != "")
		{
			prob = atof(value.c_str());
		}

		double minHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMinHeight);
		if(value != "")
		{
			minHeight = atof(value.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMaxHeight);
		if(value != "")
		{
			maxHeight = atof(value.c_str());
		}

		return LandBuildInt::Modules::BoundarySubSquaresFramePlacer::ObjectInput(name, prob, minHeight, maxHeight);
	}
	else
	{
		return LandBuildInt::Modules::BoundarySubSquaresFramePlacer::ObjectInput("", 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::FramedForestPlacer::ObjectInput LBObject::FramedForestPlacerObjectInput()
{
	if(m_Name != OBJ_NAME_FROMDBF)
	{
		RString name = m_Name.c_str();

		double prob = 100.0;   // defalt value
		string strProb = m_ParametersList.GetValue(PARNAME_PROB);
		if (strProb != "")
		{
			prob = atof(strProb.c_str());
		}

		double minHeight = 100.0;   // defalt value
		string strMinH = m_ParametersList.GetValue(PARNAME_MINHEIGHT);
		if (strMinH != "")
		{
			minHeight = atof(strMinH.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		string strMaxH = m_ParametersList.GetValue(PARNAME_MAXHEIGHT);
		if (strMaxH != "")
		{
			maxHeight = atof(strMaxH.c_str());
		}

		bool inner = true;   // defalt value
		string strInner = m_ParametersList.GetValue(PARNAME_INNER);
		if (strInner != "")
		{
			int innerInt = atoi(strInner.c_str());
			if (innerInt == 0)
			{
				inner = false;
			}
		}

		return LandBuildInt::Modules::FramedForestPlacer::ObjectInput(name, prob, minHeight, maxHeight, inner);
	}
	else
	{
		return LandBuildInt::Modules::FramedForestPlacer::ObjectInput("", 0, 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::FramedForestPlacer::ObjectInput LBObject::FramedForestPlacerObjectInput(int shapeIndex, LBDbValuesFunctor& functor)
{
	if(m_Name == OBJ_NAME_FROMDBF)
	{
		// in this case all parameters contain the object index 
		string strIndex     = m_ParametersList.GetValue(PARNAME_PROB);
		string strObject    = PARNAME_OBJECT + strIndex;
		string strProb      = PARNAME_PROB + strIndex;
		string strMinHeight = PARNAME_MINHEIGHT + strIndex;
		string strMaxHeight = PARNAME_MAXHEIGHT + strIndex;
		string strInner     = PARNAME_INNER + strIndex;

		string value = functor.GetValue(shapeIndex, strObject);
		RString name = value.c_str();

		double prob = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strProb);
		if (value != "")
		{
			prob = atof(value.c_str());
		}

		double minHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMinHeight);
		if (value != "")
		{
			minHeight = atof(value.c_str());
		}

		double maxHeight = 100.0;   // defalt value
		value = functor.GetValue(shapeIndex, strMaxHeight);
		if (value != "")
		{
			maxHeight = atof(value.c_str());
		}

		bool inner = true;   // defalt value
		value = functor.GetValue(shapeIndex, strInner);
		if (value != "")
		{
			int innerInt = atoi(value.c_str());
			if (innerInt == 0)
			{
				inner = false;
			}
		}

		return LandBuildInt::Modules::FramedForestPlacer::ObjectInput(name, prob, minHeight, maxHeight, inner);
	}
	else
	{
		return LandBuildInt::Modules::FramedForestPlacer::ObjectInput("", 0, 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::BestFitPlacer::ObjectInput LBObject::BestFitPlacerObjectInput()
{
	if(m_Name != OBJ_NAME_FROMDBF)
	{
		RString name = m_Name.c_str();

		double length;
		string strL = m_ParametersList.GetValue(PARNAME_LENGTH);
		if(strL != "")
		{
			length = atof(strL.c_str());
		}

		double width;
		string strW = m_ParametersList.GetValue(PARNAME_WIDTH);
		if(strW != "")
		{
			width = atof(strW.c_str());
		}

		double height;
		string strH = m_ParametersList.GetValue(PARNAME_HEIGHT);
		if(strH != "")
		{
			height = atof(strH.c_str());
		}

		return LandBuildInt::Modules::BestFitPlacer::ObjectInput(name, length, width, height);
	}
	else
	{
		return LandBuildInt::Modules::BestFitPlacer::ObjectInput("", 0, 0, 0);
	}
}

// -------------------------------------------------------------------------- //

LandBuildInt::Modules::BestFitPlacer::ObjectInput LBObject::BestFitPlacerObjectInput(int shapeIndex, LBDbValuesFunctor& functor)
{
	if(m_Name == OBJ_NAME_FROMDBF)
	{
		// in this case all parameters contain the object index 
		string strIndex  = m_ParametersList.GetValue(PARNAME_PROB);
		string strObject = PARNAME_OBJECT + strIndex;
		string strLength = PARNAME_LENGTH + strIndex;
		string strWidth  = PARNAME_WIDTH + strIndex;
		string strHeight = PARNAME_HEIGHT + strIndex;

		string value = functor.GetValue(shapeIndex, strObject);
		RString name = value.c_str();

		double length;
		value = functor.GetValue(shapeIndex, strLength);
		if(value != "")
		{
			length = atof(value.c_str());
		}

		double width;
		value = functor.GetValue(shapeIndex, strWidth);
		if(value != "")
		{
			width = atof(value.c_str());
		}

		double height;
		value = functor.GetValue(shapeIndex, strHeight);
		if(value != "")
		{
			height = atof(value.c_str());
		}

		return LandBuildInt::Modules::BestFitPlacer::ObjectInput(name, length, width, height);
	}
	else
	{
		return LandBuildInt::Modules::BestFitPlacer::ObjectInput("", 0, 0, 0);
	}
}

// ************************************************************** //
// Serialization                                                  //
// ************************************************************** //

void LBObject::Serialize(CArchive& ar)
{
	CString name;
	UINT    fillColor;
	UINT    borderColor;

	CObject::Serialize(ar);

	m_ParametersList.Serialize(ar);

	if(ar.IsStoring())
	{
		name        = CString(m_Name.c_str());
		fillColor   = UINT(m_PvFillColor);
		borderColor = UINT(m_PvBorderColor);
		ar << m_Version;
		ar << name;
		ar << fillColor;
		ar << borderColor;
	}
	else
	{
		ar >> m_Version;
		ar >> name;
		ar >> fillColor;
		ar >> borderColor;
		m_Name          = name;
		m_PvFillColor   = static_cast<LBPreviewColors>(fillColor); 
		m_PvBorderColor = static_cast<LBPreviewColors>(borderColor); 
	}
}
