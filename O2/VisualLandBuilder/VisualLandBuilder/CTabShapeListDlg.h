#pragma once
#include "afxcmn.h"

#include ".\LBModuleList.h"
#include "..\..\LandBuilder\Shapes\ShapeOrganizer.h"

// CTabShapeListDlg dialog

class CTabShapeListDlg : public CDialog
{
	DECLARE_DYNAMIC(CTabShapeListDlg)

public:
	CTabShapeListDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTabShapeListDlg();

// Dialog Data
	enum { IDD = IDD_TABPREVSHAPELIST };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	// the list
	CTreeCtrl m_ShapeList;

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //

private:
	afx_msg void OnSize(UINT nType, int cx, int cy);

// -------------------------------------------------------------------------- //
// INTERFACE                                                                  //
// -------------------------------------------------------------------------- //
public:
	void SetList(LBModuleList* moduleList, AutoArray<pShapeOrganizer>* shapeOrganizerList, bool showAll);

// -------------------------------------------------------------------------- //
// LIST HANDLERS                                                              //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnNMClickShapeList(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg void OnTvnSelChangedShapeList(NMHDR *pNMHDR, LRESULT *pResult);

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
private:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
