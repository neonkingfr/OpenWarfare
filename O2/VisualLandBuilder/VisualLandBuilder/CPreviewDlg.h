#pragma once
#include "afxwin.h"

#include ".\LBModuleList.h"
#include "..\..\LandBuilder\Shapes\ShapeOrganizer.h"

#include ".\CDrawModulesStatic.h"
#include ".\CPrevToolsTabCtrl.h"

#include ".\CTabZoomDlg.h"
#include ".\CTabShapeListDlg.h"
#include ".\CTabObjectColorListDlg.h"

#include "afxcmn.h"

// CPreviewDlg dialog

class CPreviewDlg : public CDialog
{
	DECLARE_DYNAMIC(CPreviewDlg)

public:
	CPreviewDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPreviewDlg();

// Dialog Data
	enum { IDD = IDD_PREVIEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

private:
	virtual BOOL OnInitDialog(void);

private:
	// the picture ctrl
	CDrawModulesStatic m_PictureCtrl;
	// the cancel btn
	CButton m_CancelBtn;
	// the tools tab
	CPrevToolsTabCtrl m_ToolTab;

	// the child dialogs
	CTabZoomDlg*            m_ZooDlg;
	CTabShapeListDlg*       m_ShaDlg;
	CTabObjectColorListDlg* m_ColDlg;

	// filters
	bool m_ShowAll;
	bool m_ShowModulePreview;

	// pan variables
	bool m_Panning;
	// zoom window variables
	bool m_ZoomingWindow;

	// DATA
	// the pointer to the original data
	LBModuleList* m_ModuleList;

	// the list of associated ShapeOrganizer 
	AutoArray<pShapeOrganizer> m_ShapeOrganizerList;

	DECLARE_MESSAGE_MAP()

// -------------------------------------------------------------------------- //
// INTERFACE FUNCTIONS                                                        //
// -------------------------------------------------------------------------- //
public:
	// SetModuleList()
	// sets the module list to be drawn with the given one
	// if the showAll parameter is true, draw also inactive modules
	// !!! must be called before the DoModal
	void SetModuleList(LBModuleList* moduleList, bool showAll, bool showModulePreview);
	// SetSelectedShape()
	// sets the selected shape
	void SetSelectedShape(int moduleIndex, int shapeIndex);

// -------------------------------------------------------------------------- //
// DIALOG HANDLERS                                                            //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnSize(UINT nType, int cx, int cy);

// -------------------------------------------------------------------------- //
// HELPER FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
public:
	// ShowApplyingModuleMessage()
	// shows a text message while calculating module preview
	void ShowApplyingModuleMessage(LBModule* module);

// -------------------------------------------------------------------------- //
// TRICKY FUNCTIONS                                                           //
// -------------------------------------------------------------------------- //
private:
	// this function is used to intercept the repaint message triggered
	// when the user change colors for the objects
	virtual BOOL PreTranslateMessage(MSG* pMsg);

// -------------------------------------------------------------------------- //
// SUB DIALOG EVENTS HANDLERS                                                 //
// -------------------------------------------------------------------------- //
public:
	void OnPan();
	void OnZoomWindow();
	void OnZoomPlus();
	void OnZoomMinus();
	void OnZoomExt();

// -------------------------------------------------------------------------- //
// MOUSE EVENTS HANDLERS                                                      //
// -------------------------------------------------------------------------- //
private:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

// -------------------------------------------------------------------------- //
// CURSOR HANDLERS                                                            //
// -------------------------------------------------------------------------- //
private:
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
};
