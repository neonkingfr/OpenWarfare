#ifdef _MSC_VER
#pragma once
#endif

#ifndef _MATHDEFS_HPP
#define _MATHDEFS_HPP

// common KNI/P declarations

// diferent enum placeholders for different constructors

enum _noInit {NoInit};
//enum _vZero {VZero};
enum _vRotate {VRotate};
enum _vFastTransform {VFastTransform};
enum _vFastTransformA {VFastTransformA};
enum _vPerspective {VPerspective};
enum _vMultiply {VMultiply};
enum _vMultiplyLeft {VMultiplyLeft};

//enum _mZero {MZero};
//enum _mIdentity {MIdentity};
enum _mTranslation {MTranslation};
enum _mTilda {MTilda};
enum _mRotationX {MRotationX};
enum _mRotationY {MRotationY};
enum _mRotationZ {MRotationZ};
enum _mRotationAxis {MRotationAxis};
enum _mScale {MScale};
enum _mPerspective {MPerspective};
enum _mDirection {MDirection};
enum _mUpAndDirection {MUpAndDirection};
enum _mView {MView};
enum _mMultiply {MMultiply};
enum _mInverseRotation {MInverseRotation};
enum _mInverseScaled {MInverseScaled};
enum _mInverseGeneral {MInverseGeneral};
enum _mNormalTransform {MNormalTransform};

typedef float Coord;
#define COORD_MIN ( -FLT_MAX )
#define COORD_MAX ( +FLT_MAX )
#define coord(x) (float)(x)
	
#define H_PI ( 3.14159265358979323846f )
#define H_SQRT2 ( 1.4142135624f )
#define H_INVSQRT2 ( 0.70710678119f )

#endif
