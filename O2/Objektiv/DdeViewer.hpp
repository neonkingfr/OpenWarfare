#ifndef _DDEVIEWER_HPP
#define _DDEVIEWER_HPP

#include "objlod.hpp"

bool StartViewer
  (
  HWND hWnd, LODObject *obj, const char *exePath, float lodBias, bool twoMon
  );
bool UpdateViewer( HWND hWnd, LODObject *obj, float lodBias );
void CloseViewer( HWND hWnd );

#endif

