// checkVer.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "checkVer.h"
#include <El/ParamFile/ParamFile.hpp>
#include <io.h>


CStringRes StrRes;
#define APPNAME (StrRes[IDS_APPNAME])

// Forward declarations of functions included in this code module:

FILETIME GetFileDateTime(const char *filename)
{
  FILETIME res;
  res.dwHighDateTime=0;
  res.dwLowDateTime=0;
  HANDLE hFile=CreateFile(filename,0,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,0,NULL);
  if (hFile!=NULL) 
  {    
    GetFileTime(hFile,NULL,NULL,&res);
    CloseHandle(hFile);
  }
  return res;
}

bool CompareFilesToUpdate(const char *local, const char *remote)
{
  FILETIME localtm=GetFileDateTime(local);
  FILETIME remotetm=GetFileDateTime(remote);
  return  (localtm.dwHighDateTime<remotetm.dwHighDateTime ||
    localtm.dwHighDateTime==remotetm.dwHighDateTime && 
    localtm.dwLowDateTime<remotetm.dwLowDateTime);
}


int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
  StrRes.SetInstance(hInstance);
  
  if (lpCmdLine[0]==0)
  {
    MessageBox(NULL,StrRes[IDS_NOPARAMETER],APPNAME,MB_OK);
    return -1;
  }
  ParamFile info;
  LSError error=info.Parse(lpCmdLine);
  if (error!=LSOK)
  {    
    char *buff=(char *)alloca(strlen(lpCmdLine)+strlen(StrRes[IDS_PARSEERROR])+100);
    sprintf(buff,StrRes[IDS_PARSEERROR],lpCmdLine,error);
    MessageBox(NULL,buff,APPNAME,MB_OK);
    return -1;
  }
  ParamEntryPtr checkentry=info.FindEntry("Check");
  if (checkentry.NotNull() && checkentry->IsClass())
  {
    int cnt=checkentry->GetEntryCount();
    for (int i=0;i<cnt;i++)
    {
      ParamEntryVal subentry=checkentry->GetEntry(i);
      if (subentry.IsClass())
      {
        RString localfile,remotefile,distribuce;
        ConstParamEntryPtr p=subentry.FindEntry("LocalFile");
        if (p.NotNull()) localfile=p->GetValue();
        p=subentry.FindEntry("RemoteFile");
        if (p.NotNull()) remotefile=p->GetValue();
        p=subentry.FindEntry("DistCmd");
        if (p.NotNull()) distribuce=p->GetValue();
        if (localfile.GetLength()!=0)
        {
          bool needupdate=false;
          if (_access(localfile,0)!=0) needupdate=true;
          if (remotefile.GetLength()!=0 &&_access(remotefile,0)==0)
          {
            if (!needupdate) needupdate=CompareFilesToUpdate(localfile,remotefile);
            if (needupdate)
            {
              const char *message=distribuce.GetLength()?StrRes[IDS_UPDATEAUTO]:StrRes[IDS_UPDATEMAN];
              char *buff=(char *)alloca(strlen(localfile)+strlen(message)+100);
              sprintf(buff,message,localfile.Data());
              int id=MessageBox(NULL,buff,APPNAME,distribuce.GetLength()?MB_YESNO:MB_OK);
              if (id==IDYES)
                system(distribuce);
            }
          }
        }
        else
         MessageBox(NULL,StrRes[IDS_INIERROR_NOLOCALNAME],APPNAME,MB_OK);
      }
      else
        MessageBox(NULL, StrRes[IDS_INIERROR_EXCEPTINGCLASS],APPNAME,MB_OK);
    }
  }
  else
    MessageBox(NULL, StrRes[IDS_INIERROR_NOCHECKENTRY],APPNAME,MB_OK);
  ParamEntryPtr runentry=info.FindEntry("Run");
  if (runentry.NotNull())
  {
    STARTUPINFO nfo;
    PROCESS_INFORMATION pinf;
    memset(&nfo,0,sizeof(nfo));
    nfo.cb=sizeof(nfo);
    nfo.dwFlags=STARTF_USESHOWWINDOW;
    nfo.wShowWindow=nCmdShow;
    RString torun=runentry->GetValue();    
    BOOL pp=CreateProcess(NULL,const_cast<char *>(torun.Data()),NULL,NULL,FALSE,0,NULL,NULL,&nfo,&pinf);
    if (pp==FALSE)
    {
      MessageBox(NULL,StrRes[IDS_SPAWNERROR],APPNAME,MB_OK);
    }
    else
    {
      CloseHandle(pinf.hProcess);
      CloseHandle(pinf.hThread);
    }
  }
  return 0;
}

