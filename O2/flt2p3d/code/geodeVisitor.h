
#pragma once

#include "CommonIncludes.h"

namespace OsgP3DObject
{
// collects geodes from scene sub-graph attached to 'this'
class geodeVisitor : public osg::NodeVisitor { 

public:
	geodeVisitor():
	  osg::NodeVisitor(osg::NodeVisitor::TRAVERSE_ALL_CHILDREN) 
	  {
		  _addToGeodelist = true;
	  }

	  ~geodeVisitor() { _geodelist.clear();}

	  // one apply for each type of Node that might be a user transform
	  //   virtual void apply(osgAction::ActionHeader& ah);
	  // virtual void apply(osg::Drawable& dr);
	  virtual void apply(osg::Geode& geode) {
		  if (_addToGeodelist)
		  {
			  _geodelist.push_back(&geode);
		  }
	  }
	  //    virtual void apply(osg::Billboard& geode);
	  virtual void apply(osg::Group& gp){
		  // check that we are in the lowest lod


			  const char* className = gp.className();
			  string lodName = "LOD";
			  if(lodName == className)
			  {	
				  const osg::LOD & lod = dynamic_cast<const osg::LOD &>(gp);
				  const osg::LOD::RangeList& rangeList = lod.getRangeList();
				  osg::LOD::RangeList::const_iterator iter = rangeList.begin();
				  for(;iter != rangeList.end() ; iter++)
				  {
					  if (iter->first == 0)
					  {
						_addToGeodelist = true;
						break;
					  }
					  else
					  {
						  _addToGeodelist = false;
					  }
				  }

			  }




				traverse(gp);    // must continue subgraph traversal.
		  
	  }
	  //   virtual void apply(osg::Switch& sw);
	  // virtual void apply(osg::Transform& transform);
	  std::vector<const osg::Geode *> getGeodes() {return _geodelist;}
protected:

	typedef std::vector<const osg::Geode *>    Geodelist;
	Geodelist  _geodelist;
	bool _addToGeodelist;
};

} // namespace OsgP3DObject