                        #pragma once

#pragma warning(disable: 4661)

#include "SdkCommon.h"
#include "Adapter.h"
#include <vector>

#include "Mesh.h"



namespace OxygeneSDK {  

    class DLLEXTERN Frame: public Adapter<ObjektivLib::AnimationPhase>
    {
        typedef Adapter<ObjektivLib::AnimationPhase> Super;
    public:
        Frame(const ObjektivLib::AnimationPhase *x):Super(x) {}
        Frame(ObjektivLib::AnimationPhase *x,Super::Owner owner):Super(x, owner) {}
        Frame(Mesh &object );
        Frame(const Frame& other, Mesh &object );

        void SetTime( float time );
        float GetTime() const;
        void Redefine(Mesh &data ); // import external data

        const Vector operator[](int i) const;
        void set(int i, const Vector &vx);
        void Validate();
    };


     template<>
     inline Adapter<ObjektivLib::AnimationPhase>::Adapter();
}

