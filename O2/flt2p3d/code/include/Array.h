#pragma once

#pragma warning(disable: 4661)

#include "SdkCommon.h"
#include "Adapter.h"
#include <vector>

namespace OxygeneSDK {

    template<class T>
    class DLLEXTERN Array: public Adapter<std::vector<T> > {
        typedef Adapter<std::vector<T> > Super;

    public:

        Array(const std::vector<T> *other):Super(other) {}
        Array(std::vector<T> *other, Owner owner):Super(other, owner) {}
        Array(const std::vector<T> &other) {Super::operator=(Array(&other));}
        Array() {}
        Array(const Array &other):Super(other) {}        

        const T &operator[](unsigned int index) const {
            return object->at(index);
        }
        std::size_t Size() const{
            return object->size();
        }
    };

}