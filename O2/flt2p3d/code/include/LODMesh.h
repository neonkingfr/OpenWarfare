#pragma once

#pragma warning(disable: 4661)

#include "SdkCommon.h"
#include "Adapter.h"
#include "Vector.h"
#include "Mesh.h"

namespace OxygeneSDK {  
 
    class ILODMeshLoadCallback {
    public:
        virtual bool LoadCallBack( std::istream &f ) = 0;
    };

    class ILODMeshSaveCallback {
    public:
        virtual bool SaveCallBack( std::ostream &f ) = 0;
    };

    class DLLEXTERN LODMesh: public Adapter<ObjektivLib::LODObject>
    {
        typedef Adapter<ObjektivLib::LODObject> Super;
    public:
        LODMesh(const ObjektivLib::LODObject *x):Super(x) {}
        LODMesh(ObjektivLib::LODObject *x,Super::Owner owner):Super(x, owner) {}
        
        LODMesh &operator = ( const LODMesh& src) {
            Super::operator=(src);return *this;
        }

        LODMesh() {}
        LODMesh(Mesh &firstlod, float resolution=0.0f);
       
        ///Retrieves index of active level
        int GetActiveLevel() const;
        ///Retrieves active mesh
        Mesh GetActiveMesh() const;        
        ///Retrieves mesh at specified level index
        Mesh GetLevel(int level) const;   
        
        ///Changes active level
        /**
        @param level index of mesh. use FindLevel to convert resolution to the index
        */
        void SelectLevel( int level ); 
        ///Retrieves count of levels
        int NLevels() const;
       
        ///Finds nearest level index
        /**
            @param resolution resolution to search
            @return index of mesh with nearest resolution
        */
        int FindLevel( float resolution ) const;
        
        ///Finds exact level
        /**
           @param resolution resolution to search
           @return index of mesh with specified resolution, 
            or -1 if mesh with specified resolution not exists
        */
        int FindLevelExact( float resolution ) const;
        ///Retrieves resolution of the mesh
        float GetResolution( int level ) const;
        ///Changes resolution of mesh specified by its current resolution
        bool ChangeResolution( float oldRes, float newRes );
        ///Sets resolution at level
        int SetResolution( int level, float newRes );
    
        ///Removes level        
        bool DeleteLevel( int level );
        ///Adds new level to the LODMesh
        /**
         @param mesh source mesh, it will used as template of that level. 
         Function adds copy of the mesh, so later changes in the source object
         will not affect added level.
         @param resolution resolution
         @return index of the new level
         */
        int AddLevel(Mesh mesh, float resolution );
        ///Removes level and returns it as object
        /**
         @param level index of level, which's mesh will be unlinked
         @return mesh of that level
         @note Function removes specified level, but allows to caller retrieve
         mesh associated with the level.
         */
        Mesh UnlinkLevel( int level );

        ///Adds and links mesh into LODMesh object
        /**
            @param mesh mesh that will be linked into the object. Later changes
            in source mesh WILL affect linked mesh in the object
            @param resolution resolution
         @return index of the new level
            */
        int LinkLevel(Mesh &mesh, float resolution );


       
        ///Replaces content of object with data readed from the file
        /**
        @param filename name of P3D file
        @param callback Interface with function that will be called to read user 
                specific data associated with the p3d
        @retval true succes
        @retval false fail
        */
        bool Load(const char *filename, ILODMeshLoadCallback *callback = 0 );
        ///Replaces content of object with data readed from the file
        /**
        @param f std stream with prepared data
        @param callback Interface with function that will be called to read user 
                specific data associated with the p3d
        @retval true succes
        @retval false fail
        */
        bool Load(std::istream &f, ILODMeshLoadCallback *callback = 0);
        ///Stores content of object into the external file
        /**
        @param filename name of P3D file
        @param callback Interface with function that will be called to write user 
                specific data associated with the p3d
        @retval true succes
        @retval false fail
        */
        bool Save(const char *filename,   ILODMeshSaveCallback *callback = 0  ) const;
        ///Stores content of object int the external stream
        /**
        @param f std stream
        @param callback Interface with function that will be called to write user 
                specific data associated with the p3d
        @retval true succes
        @retval false fail
        */
        bool Save(std::ostream &f, ILODMeshSaveCallback *callback = 0 ) const;
        ///Merges one LODMesh with another
        /**
        Processes all resolutions and merges meshes with the same resolution
        @param src source LODMesh
        @param createlods specify true, if you wish to enable creation of the new levels. 
                        If this flag is false, function will not merge levels that
                        is not exists in target object, until fillLods is specified
        @param createSel if specified, function will creates selection with this name 
                        for all faces and points added to the levels
        @param fillLods for not existing levels function will use nearest unused levels
        @retval true succes
        @retval false fail
        */
        bool Merge(const LODMesh &src , bool createlods, const char *createSel=NULL, bool fillLods = false);
        
        ///Centers all objects and returns coordinates of the previous center        
        Vector CenterAll();
    };
}