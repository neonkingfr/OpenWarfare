#ifndef __TRADITIONAL_INTERFACE_FACE_T_OBJECTIV_2_
#define __TRADITIONAL_INTERFACE_FACE_T_OBJECTIV_2_


//#include "../optima/optima2mfc.h"
//#include "data3d.h"

#ifndef _RSTRING_HPP
class RString;
#endif

namespace ObjektivLib {


class IArchive;

class ObjectData;
class FaceHelper;
struct ObjUVSet_Item;
#ifndef ClassIsMovable
#define  ClassIsMovable(x)
#endif

#ifndef DLLEXTERN
#define DLLEXTERN
#endif


class DLLEXTERN FaceT
  {
    FaceHelper *_helper;
    ObjectData *_obj;
    int _faceIndex;
    ObjUVSet_Item *FindUVSetItem(int stage, int vs, bool set);
    const ObjUVSet_Item *FindUVSetItem(int stage, int vs) const;

  public:

    
    FaceT(ObjectData *obj,int faceIndex):_obj(obj),_faceIndex(faceIndex),_helper(NULL) {}
    FaceT(ObjectData &obj,int faceIndex):_obj(&obj),_faceIndex(faceIndex),_helper(NULL) {}
    FaceT(const ObjectData *obj,int faceIndex):_obj(const_cast<ObjectData *>(obj)),_faceIndex(faceIndex),_helper(NULL) {}
    FaceT(const ObjectData &obj,int faceIndex):_obj(const_cast<ObjectData *>(&obj)),_faceIndex(faceIndex),_helper(NULL) {}
    FaceT(ObjectData *obj):_faceIndex(-1),_helper(0) {CreateUnbound(obj);}
    FaceT(ObjectData &obj):_faceIndex(-1),_helper(0) {CreateUnbound(&obj);}
    FaceT(const ObjectData *obj):_faceIndex(-1),_helper(0) {CreateUnbound(const_cast<ObjectData *>(obj));}
    FaceT(const ObjectData &obj):_faceIndex(-1),_helper(0) {CreateUnbound(const_cast<ObjectData *>(&obj));}
    FaceT(const FaceT &other);
    FaceT():_obj(NULL),_faceIndex(-1),_helper(NULL) {}///<if you are using this constructor, remember to call SetObject soon!
    
    FaceT &operator=(const FaceT &other);
    ~FaceT() {Release();}

    void SetObject(ObjectData *obj) {_obj=obj;}
    void CreateUnbound(ObjectData *obj); ///<Creates an anonymous face
    void CreateIn(ObjectData *obj); ///<Creates new face in object
    void BindTo(ObjectData *obj, int index); ///<Binds face from object. Current face is unbend
    void BindTo(const FaceT &other); ///<Binds face from object. Current face is unbend
	void Release();  ///<releases current face, and object stays unbended
	bool IsValid() const {return (_helper!=NULL || _faceIndex!=-1) && _obj!=NULL;}

//* Direct manipulating with face

    void SetTexture(RString name,int stage=-1);
    void SetMaterial(RString name, int stage=-1);
    void SetTexture(const char *name, int stage=-1);
    void SetMaterial(const char *name, int stage=-1);
    void SetN(int n) {SetVxCount(n);}
    void SetVxCount(int n);
    void SetPoint(int vs, int pt);
    void SetNormal(int vs, int nr);
    void SetU(int vs, float tu, int stage=-1);
    void SetV(int vs, float tv, int stage=-1);
    void SetUV(int vs, float tu, float tv, int stage=-1);    
    void SetFlags(unsigned long flags);
    unsigned long SetFlags(unsigned long zerobits, unsigned long invertbits);

    const RString &GetTexture(int stage=-1) const ;
    const RString &GetMaterial(int stage=-1) const ;
    int N() const  {return GetVxCount();}
    int GetVxCount() const ;
    int GetPoint(int vs) const ;
    int GetNormal(int vs) const ;
    float GetU(int vs,  int stage=-1) const ;
    float GetV(int vs, int stage=-1) const ;
    ObjUVSet_Item GetUV(int vs, int stage=-1) const ;
    unsigned long GetFlags() const ;
    

    void CopyFaceTo(int index) const; //copies face into another index in object
    void CopyFaceTo(FaceT &other) const; //copies face into another index in object, target acts as reference
    int GetFaceIndex() const {return _faceIndex;}

    int GetPointInfoSize() const ;   //gets total size for structure to save one point information
    void SavePointInfo(int point, void *nfostruct) const; //saves structure for one point
    void LoadPointInfo(int point, const void *nfostruct); //loads structure for one point

    void CopyPointInfo(int trgpt, int srcpt);
    void CopyPointInfo(int trgpt, FaceT &other, int srcpt);

    void Serialize(IArchive &arch);
    ObjectData *GetObject() {return _obj;}
    const ObjectData *GetObject() const {return _obj;}


// Other
    template<class T1>
    const T1 &GetPointVector(int vs) const
      {return _obj->Point(GetPoint(vs));}

    template<class T1>
        T1 &GetPointVector(int vs)
      {return _obj->Point(GetPoint(vs));}

    template<class T1>
    const T1 &GetNormalVector(int vs) const
      {return _obj->Point(GetNormal(vs));}

    template<class T1>
    T1 &GetNormalVector(int vs)
      {return _obj->Normal(GetNormal(vs));}
  
    bool ContainsPoint( int vertex ) const;
    bool IsNeighbourgh( const FaceT &face ) const;
    int ContainsPoints( const bool *vertices ) const;
    bool ContainsEdge( int v1, int v2 ) const;
    bool GetEdge(const FaceT &other, int &v1,int &v2) const;
    bool GetEdgeIndices(const FaceT &other, int &i1a,int &i1b, int &i2a, int& i2b) const;
    int PointAt( int vertex ) const;
    bool ContainsNormal( int vertex ) const;
    void Reverse();
    void Cross();
    void Shift0To1();
    void Shift1To0();
    Vector3 CalculateNormal( ) const;
    Vector3 CalculateNormal(  const class AnimationPhase &phase ) const;
    Vector3 CalculateRawNormal(  ) const;
    Vector3 CalculateNormalAtPointAdjusted(int vs) const;
    double CalculateArea(  ) const;
    double CalculatePerimeter( ) const;
    bool IsConvex(  ) const;
    
    bool PointInHalfSpace
      (
         const Vector3 &p
          ) const;
    bool FaceInHalfSpace
      (
         const FaceT &face
          ) const;
    

    ///Calculates point in half space
    /**
     * @retval 1 point is at side in direction of face normal
     * @retval 0 point is lying on the face (there is very small tolerance)
     * @retval -1 point is at the other side
     */

    static int PointInHalfSpaceEx(const Vector3 &normal, float d, const Vector3 &p);

    
    void AutoUncross(  );
    void AutoReverse(  );

    enum FaceToFaceLocation
    {
      fAbove, //first face is above second
      fBelow, //first face is below second
      fIntermediate, //first face is intermediate with second (some points are above and some points are below)
      fAboveBoth, //first face is above and also second face is above first
      fBelowBoth, //first face is below and also second face is below first
      fCross, //faces are crossed. 
    };

    ///gets location of face relative to plane
    FaceToFaceLocation FaceLocation(const Vector3 &normal, float d) const;

    ///gets location of face relative to second face 
    /**
     * It will take normal and location of second face 
     */
    FaceToFaceLocation FaceLocation(const FaceT &other) const;
    
    ///gets location of face relative to seconds face and vica versa
    /**
     * @retval fAbove - first face is complettly above second  
     * @retval fBelow - first face is cimplettly below second
     * @retval fAboveBoth - faces are forwared to each other
     * @retval fBelowBoth - faces are reverse to each other
     * @retval fCross - faces are crossed.
     */
    FaceToFaceLocation FaceLocation2(const FaceT &other) const;

    static bool PointInHalfSpace( const Vector3 &normal, float d, const Vector3 &p );

    ///Function test face for degeneration
    /**
    @param repair TRUE: Face can be repaired during check. FALSE: Face will be
                  checked only and function returns false, if face is not valid.
    
    @param removeInvalid TRUE: Invalid face will be removed from object. This
                  option is ignored, if face is not attached to object. 

    @param ptDistance Default value (-1) forces check point indexes only. Defining
                  any positive distance, function will check, if points have 
                  defined distance. Don't forget to check isolated points.

    @param reportRepaired Normally, function returns true, if all check passed or
                 face can be repaired. If this parameter is true, function
                 returns false even if face has been successfully repaired.

    @return function return false, when face cannot be repaired. When reportRepaired
                paramater is true, function returns false, when face was invalid, and 
                has been repaired. In other cases, function returns true.
    */
    bool Check(bool repair=true, bool removeInvalid=false, float ptDistance=-1, bool reportRepaired=false);

    ///Function tests edge between the faces for mapping
    /**
    @param other Other face.
    @return function returns true, if both faces shares U,V coordinates, so mapping continues from one face to
      other. Function returns false, if faces has different mapping on edge, or if faces has no common edge.
    */
    bool HasSameMappingOnEdge(FaceT &other);
    bool ContinueMappingOnEdge(FaceT &other);

    bool RepairDegenerated();

    bool SetActiveStage(int stage);
    int GetActiveStage() const;

    ///returns true, if face is mapped. Not mapped face has U,V the same
    bool HasSomeMapping() const;
    void ClearMapping();
                                                 
    ///tests uv coordinates of two faces whether they are in collision
    /**
     Function is useful to detect, whether one face overlaps another in UV space
     @retval true, there is collision
     @retval false no collision
     @note collision points must lay inside of face, not at border on the face
     */
    bool IsUVCollision(const FaceT &other) const;

    ///returns true, when uv coordinate is inside of face in current uvset
    bool IsUVCoordInside(const ObjUVSet_Item &uv) const;
    
    ///Calculates angle at the point.
    /**
     angle is counted around normal axe. It uses right hand rule rotation. Angle zero
     means that both vectors from that point are same.
     */
    float CalcAngleAtPoint(int vs) const;
    ///Works simular to CalcAngleAtPoint, but result is returned as sin,cos
    /**
    @param vs index of point
    @return returns sinus and cosinus of the angle. To retrieve angle, use atan2(first,second);
    */
    std::pair<float,float> CalcAngleAtPointRaw(int vs) const;


    ClassIsMovable(FaceT);

  private:
    void swap_pts(int pt1,int pt2);

    

  };

}


#endif
