

#pragma once

#include "CommonIncludes.h"

namespace OsgP3DObject
{
	class ReaderWriterP3D : public osgDB::ReaderWriter
	{
		//WriteResult convert(const osg::Node *osgNode);
	
	protected:
		mutable std::string _implicitPath;
		mutable std::string _dataDir;
		mutable std::string _materialPrefix;

	public:

		ReaderWriterP3D();

		virtual const char* className() const { return "P3D object Writer"; }
		virtual bool acceptsExtension(const std::string& extension) const { return osgDB::equalCaseInsensitive(extension,"p3d"); }

		virtual ReadResult readNode(const std::string& file, const osgDB::ReaderWriter::Options* options) const;
		virtual WriteResult writeNode(const osg::Node& node,const std::string& fileName, const osgDB::ReaderWriter::Options* options) const;
		virtual WriteResult writeNode( const osg::Node& node, std::ostream& fOut, const Options* options ) const;
	};



} // namespace OsgP3DObject
