
#include <Windows.h>
#include <Dbghelp.h>
#include "ReaderWriterP3D.h"
#include "geodeVisitor.h"
#include "OsgGeodeToP3D.h"
//#include "OgreProgramSerializer.h"
namespace OsgP3DObject
{

	// ----------------------------------------------------------------------------------------------------------------------------------------------

	// now register with Registry to instantiate the above
	// reader/writer.
	osgDB::RegisterReaderWriterProxy<ReaderWriterP3D> g_readerWriter_P3D_Proxy;

// ----------------------------------------------------------------------------------------------------------------------------------------------

ReaderWriterP3D::ReaderWriterP3D()
{
	supportsExtension("p3d","P3D file format");
}

// ----------------------------------------------------------------------------------------------------------------------------------------------

ReaderWriterP3D::ReadResult ReaderWriterP3D::readNode(const std::string& file, const osgDB::ReaderWriter::Options* options) const
{
	// TODO
	return ReadResult::FILE_NOT_HANDLED;
}

// ----------------------------------------------------------------------------
ReaderWriterP3D::WriteResult ReaderWriterP3D::writeNode(const osg::Node& node,const std::string& fileName, const osgDB::ReaderWriter::Options* options) const
{
  std::cout<< "----------------Start convert-----------------" <<std::endl;
	if ( fileName.empty() )
	{
		osg::notify( osg::FATAL ) << "p3dexp: writeNode: empty file name" << std::endl;
		return WriteResult::FILE_NOT_HANDLED;
	}

	std::string ext = osgDB::getLowerCaseFileExtension( fileName );
	_materialPrefix = osgDB::getSimpleFileName(fileName);
	_materialPrefix = osgDB::getNameLessExtension(_materialPrefix);
	if ( !acceptsExtension(ext) )
		return WriteResult::FILE_NOT_HANDLED;

	// Get and save the implicit path name (in case a path wasn't specified in Options).
	std::string filePath = osgDB::getFilePath( fileName );

	// replace "/" to "\\"
	for (size_t i=0; i<filePath.size(); ++i)
		if (filePath[i]=='/')
			filePath[i]='\\';

	if (filePath.size() && filePath[filePath.size()-1]!='\\')
		filePath += '\\';

	cout<< "path: " << filePath<<endl;
	MakeSureDirectoryPathExists(filePath.c_str());

	// create data dir
	std::string dataDir = filePath + "data\\";
	if (dataDir.size() && dataDir[0]=='\\')
		dataDir.erase(0,1);
	MakeSureDirectoryPathExists(dataDir.c_str());

	_implicitPath = filePath;
	_dataDir = dataDir;

	std::ofstream fOut;
	fOut.open( fileName.c_str(), std::ios::out | std::ios::binary );
	if ( fOut.fail())
	{
		osg::notify( osg::FATAL ) << "p3dexp: Failed to open output stream." << std::endl;
		return WriteResult::ERROR_IN_WRITING_FILE;
	}

	WriteResult wr = WriteResult::FILE_NOT_HANDLED;
	wr = writeNode( node, fOut, options );
	fOut.close();

	return wr;
}

// ----------------------------------------------------------------------------
ReaderWriterP3D::WriteResult ReaderWriterP3D::writeNode( const osg::Node& node, std::ostream& fOut, const Options* options ) const
{
	// optiony zatim neresim.. 
	/*
	// Convert Options to FltOptions.
	ExportOptions* fltOpt = new ExportOptions( options );
	fltOpt->parseOptionsString();

	// If user didn't specify a temp dir, use the output directory
	//   that was implicit in the output file name.
	if (fltOpt->getTempDir().empty())
		fltOpt->setTempDir( _implicitPath );
	if (!fltOpt->getTempDir().empty())
	{*/
		// If the temp directory doesn't already exist, make it.
		
	/*
		if (!osgDB::makeDirectory(_implicitPath))
		{
			osg::notify( osg::FATAL ) << "p3dexp: Error creating temp dir: " << _implicitPath << std::endl;
			return WriteResult::ERROR_IN_WRITING_FILE;
		}
	*/
		//}

	/*
	flt::DataOutputStream dos( fOut.rdbuf(), fltOpt->getValidateOnly() );
	flt::FltExportVisitor fnv( &dos, fltOpt );

	// Hm. 'node' is const, but in order to write out this scene graph,
	//   must use Node::accept() which requires 'node' to be non-const.
	//   Pretty much requires casting away const.
	osg::Node* nodeNonConst = const_cast<osg::Node*>( &node );
	if (!nodeNonConst)
		return WriteResult::ERROR_IN_WRITING_FILE;
	nodeNonConst->accept( fnv );
	fnv.complete( node );
	
	return fltOpt->getWriteResult();
	*/
  std::cout<< "-----------------Load driver------------------" <<std::endl;
	OsgGeodeToP3D convP3d;
	convP3d.SetWorkingDir(_dataDir);
	return convP3d.convert(&node,fOut,_materialPrefix)? WriteResult::FILE_SAVED : WriteResult::ERROR_IN_WRITING_FILE;
}

} // namespace