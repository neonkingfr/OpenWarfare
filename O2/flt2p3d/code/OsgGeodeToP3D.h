

#pragma once

#include "CommonIncludes.h"

namespace OsgP3DObject
{
	typedef signed int int32;
	typedef signed short int16;
	typedef signed char int8;
	typedef unsigned int uint32;
	typedef unsigned short uint16;
	typedef unsigned char uint8;


	struct Face
	{
		unsigned long a;
		unsigned long b;
		unsigned long c;
		const osg::StateSet * state;
	};
	typedef vector<Face> FaceList;
	typedef vector<osg::Vec3> VertexList;
	typedef vector<osg::Vec2> UVList;	
	

	class OsgGeodeToP3D
	{
	protected:

		std::string _workingDir;
		map<const osg::StateSet*,string> _stateMap;
		map<const osg::StateSet*,unsigned int> _stateCount;
		map<const osg::StateSet*,string> _textureMap;
		map<const osg::StateSet*,string> _materialMap;

		/// Compute and return face
		Face getFace(unsigned long a, unsigned long b, unsigned long c, unsigned long offset, const osg::PrimitiveSet *pset, const osg::StateSet * state)
		{
			Face f;
			f.a = pset->index(a) + offset; 
			f.b = pset->index(b) + offset; 
			f.c = pset->index(c) + offset;
			f.state = state;

			if (_stateCount.find(state) == _stateCount.end())
			{
				_stateCount[state] = 3;
			}
			else
			{
				_stateCount[state] = _stateCount[state] + 3;
			}
			

			return f; 

		}

		/// Translate axis
		osg::Vec3 convertVertexAxis(const osg::Vec3 & osgVec)
		{
			osg::Vec3 result;
			result[0] = osgVec.x();
			result[1] = osgVec.z();
			result[2] = -osgVec.y();
			return result;
		}		

		/// Fill internal structures with faces from osg
		void getFacesFromGeometry(const osg::Geometry * i_osgGeometry, FaceList & io_faceList, VertexList & io_vertexList, const unsigned int const_i_offset, const osg::StateSet * state);

		/// Get first texture filename from osg material
		std::string convertMaterial(const osg::StateSet* i_osgState, const std::string &i_materialName);
		/// Create tga file from rgb(rgb won't be erased)
		std::string convertRgbToTga(const std::string &filename);
		/// Create rvmat file with materials
		void convertOsgMaterial(const osg::Material *io_pMaterial,const std::string &materialName);
		/// Compute nearest 2^ size
		uint32 OsgGeodeToP3D::PowerOf2(uint32 v);
	public:

		// TODO: maybe in future we will need more material attributes
		// than texture(eg. color, shiness, lighting,...)
		/// Convert osgNode into P3D and save it
		bool convert(const osg::Node *osgNode, std::ostream &os,const std::string &materialPrefix);

		void SetWorkingDir(const std::string &workdir) 
		{ 
			_workingDir = workdir; 
			if (_workingDir.size() && _workingDir[_workingDir.size()-1]!='\\')
				_workingDir += '\\';
		}
	};



} // namespace
