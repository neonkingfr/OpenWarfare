
#include <sstream>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <Windows.h>
#include <Dbghelp.h>
#include "OsgGeodeToP3D.h"
#include "geodeVisitor.h"
#include "Mesh.h"
#include "LODMesh.h"
#include "sgi.h"

#include <osgDB/WriteFile>
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

typedef signed int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;

struct TGAHeader
{
	uint8 mpData[18];
	uint16 Get16(uint32 o){return mpData[o]|(mpData[o+1]<<8);}
	void Set16(uint32 o,uint16 v){mpData[o]=v&0xff;mpData[o+1]=v>>8;}
	uint8 IDLen(void) {return mpData[0];}
	uint8 ColMapType(void){return mpData[1];}
	uint8 ImageType(void){return mpData[2];};
	uint16 ColMapOffset(void){return Get16(3);}
	uint16 ColMapLen(void){return Get16(5);}
	uint8 ColMapBits(void){return mpData[7];}
	uint16 ImageOX(void){return Get16(8);}
	uint16 ImageOY(void){return Get16(10);}
	uint16 ImageW(void){return Get16(12);}
	uint16 ImageH(void){return Get16(14);}
	uint8 ImageBits(void){return mpData[16];}
	uint8 ImageDesc(void){return mpData[17];}
	void IDLen(uint8 v) {mpData[0]=v;}
	void ColMapType(uint8 v){mpData[1]=v;}
	void ImageType(uint8 v){mpData[2]=v;};
	void ColMapOffset(uint16 v){Set16(3,v);}
	void ColMapLen(uint16 v){Set16(5,v);}
	void ColMapBits(uint8 v){mpData[7]=v;}
	void ImageOX(uint16 v){Set16(8,v);}
	void ImageOY(uint16 v){Set16(10,v);}
	void ImageW(uint16 v){Set16(12,v);}
	void ImageH(uint16 v){Set16(14,v);}
	void ImageBits(uint8 v){mpData[16]=v;}
	void ImageDesc(uint8 v){mpData[17]=v;}
	uint8 *pData(void){return mpData;}
	TGAHeader(uint8 vIDLen, uint8 vColMapType, uint8 vImageType,
		uint16 vColMapOffset, int16 vColMapLen, uint8 vColMapBits,
		uint16 vImageOX, uint16 vImageOY, uint16 vImageW, uint16 vImageH,
		uint8 vImageBits, uint8 vImageDesc)
	{
		IDLen(vIDLen);ColMapType(vColMapType);ImageType(vImageType);
		ColMapOffset(vColMapOffset);ColMapLen(vColMapLen);ColMapBits(vColMapBits);
		ImageOX(vImageOX);ImageOY(vImageOY);ImageW(vImageW);ImageH(vImageH);
		ImageBits(vImageBits);ImageDesc(vImageDesc);
	}
	TGAHeader(){};
};

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

long Target;

#define TARGET_NONE 0
#define TARGET_TGA  1
#define TARGET_SGI  2
#define TARGET_RGBA 3
#define TARGET_RGB  4

class iColour {
	// Integer colour representation (used for storage)
public:
	unsigned char b, g, r, a;
};

class Bitmap {
	// Bitmaps in B-G-R-A format, as in 32 bit TARGA
public:
	unsigned long uMax, vMax;
	iColour *Pixel;
	int Create(unsigned long u, unsigned long v);
	int Destroy(void);
	int LoadTGA(const char *Name);
	int SaveTGA(const char *Name);
	int LoadRGBA(char *Name);
	int SaveRGBA(char *Name);
	int LoadRGB(char *Name);
	int SaveRGB(char *Name);
};

int Bitmap::Create(unsigned long u, unsigned long v) {
	uMax = u;
	vMax = v;

	Pixel = new iColour[uMax * vMax];
	if(Pixel == NULL) {
		printf("Bitmap::Create: Can't allocate %li bytes of memory for new bitmap (%li x %li)\n", 4 * uMax * vMax, uMax, vMax);
		return 0;
	}

	memset(Pixel, 0, 4 * uMax * vMax);

	return 1;

}

int Bitmap::Destroy(void) {
	uMax = 0;
	vMax = 0;
	delete Pixel;
	return 1;
}

int Bitmap::LoadTGA(const char *Name) {
	FILE *Handle;
	unsigned char Header[18];

	Handle = fopen(Name, "rb");
	if(Handle == NULL) {
		printf("Bitmap::LoadTGA: Can't open %s\n", Name);
		return 0;
	}

	fseek(Handle, 0, 0);
	fread(Header, 1, 18, Handle);

	if(Header[2] != 2) {
		printf("Bitmap::LoadTGA: %s is not a valid TARGA file\n", Name);
		return 0;
	}

	if(Header[16] != 32) {
		printf("Bitmap::LoadTGA: %s is a %i bit file.\n    Only 32 bit images (24 bit colour + 8 bit alpha channel) are supported.\n    Please convert this file\n", Name, Header[16]);
		exit(EXIT_FAILURE);
	}

	uMax = ((unsigned long) Header[13] << 8) + Header[12];
	vMax = ((unsigned long) Header[15] << 8) + Header[14];

	Pixel = new iColour[uMax * vMax];
	if(Pixel == NULL) {
		printf("Bitmap::LoadTGA: Can't allocate %li bytes of memory for %s (%li x %li)\n", 4 * uMax * vMax, Name, uMax, vMax);
		return 0;
	}

	fseek(Handle, 18, 0);
	fread(Pixel, 4, uMax*vMax, Handle);
	fclose(Handle);

	printf("Bitmap::LoadTGA: %s (%li x %li) successfully loaded\n", Name, uMax, vMax);

	return 1;
}

int Bitmap::SaveTGA(const char *Name) {
	FILE *Handle;
	unsigned char Header[18];

	Header[ 0] = 0;
	Header[ 1] = 0;
	Header[ 2] = 2;     // Uncompressed, uninteresting
	Header[ 3] = 0;
	Header[ 4] = 0;
	Header[ 5] = 0;
	Header[ 6] = 0;
	Header[ 7] = 0;
	Header[ 8] = 0;
	Header[ 9] = 0;
	Header[10] = 0;
	Header[11] = 0;
	Header[12] = (unsigned char) uMax;  // Dimensions
	Header[13] = (unsigned char) ((unsigned long) uMax >> 8);
	Header[14] = (unsigned char) vMax;
	Header[15] = (unsigned char) ((unsigned long) vMax >> 8);
	Header[16] = 32;    // Bits per pixel
	Header[17] = 8;

	Handle = fopen(Name, "wb");
	if(Handle == NULL) {
		printf("Bitmap::SaveTGA: Can't open %s\n", Name);
		return 0;
	}
	fseek(Handle, 0, 0);
	fwrite(Header, 1, 18, Handle);
	fseek(Handle, 18, 0);
	fwrite(Pixel, 4, uMax * vMax, Handle);
	fclose(Handle);

	printf("Bitmap::SaveTGA: %s (%li x %li) successfully saved\n", Name, uMax, vMax);

	return 1;
}

int Bitmap::LoadRGBA(char *Name) {

	// As storage and file layout differ, we have to use a different method.

	FILE *Handle;
	unsigned char Header[512];
	unsigned char *Buffer;
	unsigned long i, size;

	Handle = fopen(Name, "rb");
	if(Handle == NULL) {
		printf("Bitmap::LoadRGBA: Can't open file %s\n", Name);
		return 0;
	}

	fseek(Handle, 0, 0);
	fread(Header, 1, 512, Handle);

	if( (((unsigned short) Header[0] << 8) + Header[1] )!= 474 ) {
		printf("Bitmap::LoadRGBA: %s is not an SGI image file\n", Name);
		return 0;
	}

	if(Header[2] != 0) {
		printf("Bitmap::LoadRGBA: %s is RLE (compressed).    \nOnly uncompressed images are supported.\n    Please convert this file\n", Name);
		return 0;
	}

	if(Header[3] != 1) {
		printf("Bitmap::LoadRGBA: %s contains more than one byte per colour channel per pixel\n", Name);
		return 0;
	}

	if(Header[11] != 4) {
		printf("Bitmap::LoadRGBA: %s is not a four-channel (RGBA) image\n", Name);
		return 0;
	}

	uMax = ((unsigned long) Header[6] << 8) + Header[7];
	vMax = ((unsigned long) Header[8] << 8) + Header[9];

	Pixel  = new iColour[uMax * vMax];
	Buffer = new unsigned char[uMax * vMax * 4];

	if(Pixel == NULL) {
		printf("LoadRGBA: Can't allocate %li bytes of memory for %s (%li x %li)\n", 4 * uMax * vMax, Name, uMax, vMax);
		return 0;
	}
	if(Buffer == NULL) {
		printf("LoadRGBA: Can't allocate %li bytes of memory to unpack %s (%li x %li)\n", 4 * uMax * vMax, Name, uMax, vMax);
		return 0;
	}

	fseek(Handle, 512, 0);
	fread(Buffer, 4, uMax*vMax, Handle);
	fclose(Handle);

	// Unfortunately, RGBA and TGA images store colour channels according to
	// different conventions, so we have to unpack:

	size = uMax*vMax;

	i = 0;
	do {
		Pixel[i].r = Buffer[i];
		Pixel[i].g = Buffer[i + size];
		Pixel[i].b = Buffer[i + (size * 2)];
		Pixel[i].a = Buffer[i + (size * 3)];
	} while(++i < size);

	delete Buffer;

	printf("Bitmap::LoadRGBA: %s (%li x %li) successfully loaded\n", Name, uMax, vMax);

	return 1;
}

int Bitmap::SaveRGBA(char *Name) {
	FILE *Handle;
	unsigned char Header[512];
	unsigned char *Buffer;
	unsigned long i, size;

	memset(Header, 0, 512);

	Header[0] = 474 >> 8;   	// Magic number
	Header[1] = 474 & 255; 	// ...
	Header[2] = 0; 		// Verbatim
	Header[3] = 1; 		// BPC
	Header[4] = 0;		// Dimension
	Header[5] = 3;		// ...
	Header[6] = (unsigned char) ((unsigned long) uMax >> 8);	// X size
	Header[7] = (unsigned char) ((unsigned long) uMax & 255);	// ...
	Header[8] = (unsigned char) ((unsigned long) vMax >> 8);	// Y size
	Header[9] = (unsigned char) ((unsigned long) vMax & 255);	// ...
	Header[10] = 0;		// Z Size (4 channel RGBA)
	Header[11] = 4;		// ...
	// 4 byte pixel min 00
	Header[16] = 0;		// 4 byte pixel max FF
	Header[17] = 0;		// ...
	Header[18] = 0;		// ...
	Header[19] = 255;		// ...
	// And the rest is irrelevant...

	Handle = fopen(Name, "wb");
	if(Handle == NULL) {
		printf("Bitmap::SaveRGBA: Can't open %s\n", Name);
		return 0;
	}
	fseek(Handle, 0, 0);
	fwrite(Header, 1, 512, Handle);

	// Unfortunately, RGBA and TGA images store colour channels according to
	// different conventions, so we have to unpack:

	Buffer = new unsigned char[4*uMax*vMax];

	if(Buffer == NULL) {
		printf("LoadRGBA: Can't allocate %li bytes of memory to pack %s (%li x %li)\n", 4 * uMax * vMax, Name, uMax, vMax);
		return 0;
	}

	size = uMax*vMax;
	i = 0;
	do {
		Buffer[i] = Pixel[i].r;
		Buffer[i + size] = Pixel[i].g;
		Buffer[i + (size * 2)] = Pixel[i].b;
		Buffer[i + (size * 3)] = Pixel[i].a;
	} while(++i < size);

	// Save new data

	fseek(Handle, 512, 0);
	fwrite(Buffer, 4, uMax*vMax, Handle);
	fclose(Handle);

	delete Buffer;

	printf("Bitmap::SaveRGBA: %s (%li x %li) successfully saved\n", Name, uMax, vMax);

	return 1;
}

int Bitmap::LoadRGB(char *Name) {

	// As storage and file layout differ, we have to use a different method.

	FILE *Handle;
	unsigned char Header[512];
	unsigned char *Buffer;
	unsigned long i, size;

	Handle = fopen(Name, "rb");
	if(Handle == NULL) {
		printf("Bitmap::LoadRGB: Can't open file %s\n", Name);
		return 0;
	}

	fseek(Handle, 0, 0);
	fread(Header, 1, 512, Handle);

	if( (((unsigned short) Header[0] << 8) + Header[1] )!= 474 ) {
		printf("Bitmap::LoadRGB: %s is not an SGI image file\n", Name);
		return 0;
	}

	if(Header[2] != 0) {
		printf("Bitmap::LoadRGB: %s is RLE (compressed).    \nOnly uncompressed images are supported.\n    Please convert this file\n", Name);
		return 0;
	}

	if(Header[3] != 1) {
		printf("Bitmap::LoadRGB: %s contains more than one byte per colour channel per pixel\n", Name);
		return 0;
	}

	if(Header[11] != 3) {
		printf("Bitmap::LoadRGB: %s is not a three-channel (RGB) image\n", Name);
		return 0;
	}

	uMax = ((unsigned long) Header[6] << 8) + Header[7];
	vMax = ((unsigned long) Header[8] << 8) + Header[9];

	Pixel  = new iColour[uMax * vMax];
	Buffer = new unsigned char[uMax * vMax * 3];

	if(Pixel == NULL) {
		printf("LoadRGB: Can't allocate %li bytes of memory for %s (%li x %li)\n", 4 * uMax * vMax, Name, uMax, vMax);
		return 0;
	}
	if(Buffer == NULL) {
		printf("LoadRGB: Can't allocate %li bytes of memory to unpack %s (%li x %li)\n", 3 * uMax * vMax, Name, uMax, vMax);
		return 0;
	}

	fseek(Handle, 512, 0);
	fread(Buffer, 3, uMax*vMax, Handle);
	fclose(Handle);

	// Unfortunately, RGBA and TGA images store colour channels according to
	// different conventions, so we have to unpack:

	size = uMax*vMax;

	i = 0;
	do {
		Pixel[i].r = Buffer[i];
		Pixel[i].g = Buffer[i + size];
		Pixel[i].b = Buffer[i + (size * 2)];
		Pixel[i].a = 0;						// Dummy alpha
	} while(++i < size);

	delete Buffer;

	printf("Bitmap::LoadRGB: %s (%li x %li) successfully loaded\n", Name, uMax, vMax);

	return 1;
}

int Bitmap::SaveRGB(char *Name) {
	FILE *Handle;
	unsigned char Header[512];
	unsigned char *Buffer;
	unsigned long i, size;

	memset(Header, 0, 512);

	Header[0] = 474 >> 8;   	// Magic number
	Header[1] = 474 & 255; 		// ...
	Header[2] = 0; 		// Verbatim
	Header[3] = 1; 		// BPC
	Header[4] = 0;		// Dimension
	Header[5] = 3;		// ...
	Header[6] = (unsigned char) ((unsigned long) uMax >> 8);	// X size
	Header[7] = (unsigned char) ((unsigned long) uMax & 255);	// ...
	Header[8] = (unsigned char) ((unsigned long) vMax >> 8);	// Y size
	Header[9] = (unsigned char) ((unsigned long) vMax & 255);	// ...
	Header[10] = 0;		// Z Size (3 channel RGB)
	Header[11] = 3;		// ...
	// 4 byte pixel min 00
	Header[16] = 0;		// 4 byte pixel max FF
	Header[17] = 0;		// ...
	Header[18] = 0;		// ...
	Header[19] = 255;	// ...
	// And the rest is irrelevant...

	Handle = fopen(Name, "wb");
	if(Handle == NULL) {
		printf("Bitmap::SaveRGB: Can't open %s\n", Name);
		return 0;
	}
	fseek(Handle, 0, 0);
	fwrite(Header, 1, 512, Handle);

	// Unfortunately, RGBA and TGA images store colour channels according to
	// different conventions, so we have to unpack:

	Buffer = new unsigned char[3*uMax*vMax];

	if(Buffer == NULL) {
		printf("LoadRGB: Can't allocate %li bytes of memory to pack %s (%li x %li)\n", 3 * uMax * vMax, Name, uMax, vMax);
		return 0;
	}

	size = uMax*vMax;
	i = 0;
	do {
		Buffer[i] = Pixel[i].r;
		Buffer[i + size] = Pixel[i].g;
		Buffer[i + (size * 2)] = Pixel[i].b;
	} while(++i < size);

	// Save new data

	fseek(Handle, 512, 0);
	fwrite(Buffer, 3, uMax*vMax, Handle);
	fclose(Handle);

	delete Buffer;

	printf("Bitmap::SaveRGB: %s (%li x %li) successfully saved\n", Name, uMax, vMax);

	return 1;
}

int TypeImage(char *Name) {
	FILE *Handle;
	unsigned char Header[12];

	Handle = fopen(Name, "rb");
	if(Handle == NULL) {
		printf("Can't open %s - ignoring\n", Name);
		return 0;
	}

	fseek(Handle, 0, 0);
	fread(Header, 1, 12, Handle);
	fclose(Handle);

	switch(Header[0]) {
		case 0:
			// TGA
			return 1;
		case 1:
			// SGI
			switch(Header[11]) {
		case 3:
			// RGB
			return 2;
		case 4:
			// RGBA
			return 3;
			}
		default:
			printf("%s is not a convertable image\n", Name);
			return 0;
	}

}

char *MakeName(char *Name, long t) {
	long k, l;
	char *Write;

	k = 0;
	l = 0;
	while(Name[k] != 0) {
		if(Name[k] == '.') { 
			l++ ;
		}
		k++;
	}
	if(l > 0) {
		// We have an extension, so prune it off
		while(Name[k] != '.') {
			k--;
		}
		// We are on the spot (literally)
		Name[k] = 0;

		Write = new char[k + 5];

		if(Write == NULL) {
			printf("Out of memory\n");
			exit(EXIT_FAILURE);
		}

		switch(t) {
			case TARGET_TGA:
				sprintf(Write, "%s.tga", Name);
				break;
			case TARGET_SGI:
				sprintf(Write, "%s.sgi", Name);
				break;
		}

		Name[k] = '.';

	}
	else {
		// No extension, so add one

		Write = new char[k + 5];

		if(Write == NULL) {
			printf("Out of memory\n");
			exit(EXIT_FAILURE);
		}

		switch(t) {
			case TARGET_TGA:
				sprintf(Write, "%s.tga", Name);
				break;
			case TARGET_SGI:
				sprintf(Write, "%s.sgi", Name);
				break;
		}

		Name[k] = '.';

	}

	return Write;

}
/*
void main(int argc, char *argv[], char *envp[]) {
long i, j;
char *Write;

Bitmap Image;

printf("\n    Converts TARGA and SGI images (c) Antony Searle 1998\n\n");

if(argc == 1) {
// We have no input: give help
printf("Syntax: Convert [options] [file1] [file2]...\n\n");
printf("Options:\n");
printf("    NONE    Defaults to changing all TGA to (24 bit) SGI, and all SGI to TGA\n");
printf("    -tga    Changes all SGI images to TGA                                   \n");
printf("    -sgi    Changes all TGA images to (24 bit) SGI images                   \n");
printf("    -rgb    Changes all images to (24 bit) SGI images                       \n");
printf("    -rgba   Changes all images to (32 bit) SGI images                       \n\n");

printf("Default behaviour will cause the loss of any TGA alpha channels\n");
printf("Files with incorrect extensions will be copied to their correct\n    extensions (.tga, .sgi)\n\n");

return;

}

Target = TARGET_NONE;

i = 1;
while((argv[i])[0] == '-') {
// We have an option, not a file
switch((argv[i])[1]) {
case 't':
Target = TARGET_TGA;
printf("Converting all files to TARGA (.tga)\n");
break;
case 's':
Target = TARGET_SGI;
printf("Converting all TARGA files to (24 bit) SGI (.sgi)\n");
break;
case 'r':
switch((argv[i])[4]) {
case 0:
Target = TARGET_RGB;
printf("Converting all files to (24 bit) SGI (.sgi)\n");
break;
case 'a':
Target = TARGET_RGBA;
printf("Converting all files to (32 bit) SGI (.sgi)\n");
break;
default:
printf("Unknown option %s\n", argv[i]);
return;
}
break;
default:
printf("Unknown option [%s]\n", argv[i]);
return;
}
if(++i == argc) {
printf("No files\n");
return;
}
}

// Have parsed options: Now, we go to work on the images.

do {

// First, determine the type of the file:
j = TypeImage(argv[i]);

if(j != 0) {

// We have a recognised format

switch(Target) {
case TARGET_NONE:
// Take SGI->TGA, TGA->RGB
switch(j) {
case 1:
//TGA->RGB
Write = MakeName(argv[i], TARGET_SGI);
if(Image.LoadTGA(argv[i])) { Image.SaveRGB(Write); Image.Destroy(); }
delete Write;
break;
case 2:
//RGB->TGA
Write = MakeName(argv[i], TARGET_TGA);
if(Image.LoadRGB(argv[i])) { Image.SaveTGA(Write); Image.Destroy(); }
delete Write;
break;
case 3:
//RGBA->TGA
Write = MakeName(argv[i], TARGET_TGA);
if(Image.LoadRGBA(argv[i])) { Image.SaveTGA(Write); Image.Destroy(); }
delete Write;
break;
}
break;
case TARGET_TGA:
// Take SGI->TGA
switch(j) {
case 1:
//TGA->TGA
Write = MakeName(argv[i], TARGET_TGA);
if(Image.LoadTGA(argv[i])) { Image.SaveTGA(Write); Image.Destroy(); }
delete Write;
break;
case 2:
//RGB->TGA
Write = MakeName(argv[i], TARGET_TGA);
if(Image.LoadRGB(argv[i])) { Image.SaveTGA(Write); Image.Destroy(); }
delete Write;
break;
case 3:
//RGBA->TGA
Write = MakeName(argv[i], TARGET_TGA);
if(Image.LoadRGBA(argv[i])) { Image.SaveTGA(Write); Image.Destroy(); }
delete Write;
break;
}
break;
case TARGET_SGI:
// Take TGA->RGB
switch(j) {
case 1:
//TGA->RGB
Write = MakeName(argv[i], TARGET_SGI);
if(Image.LoadTGA(argv[i])) { Image.SaveRGB(Write); Image.Destroy(); }
delete Write;
break;
case 2:
//RGB->RGB
Write = MakeName(argv[i], TARGET_SGI);
if(Image.LoadRGB(argv[i])) { Image.SaveRGB(Write); Image.Destroy(); }
delete Write;
break;
case 3:
//RGBA->RGBA
Write = MakeName(argv[i], TARGET_SGI);
if(Image.LoadRGBA(argv[i])) { Image.SaveRGBA(Write); Image.Destroy(); }
delete Write;
break;
}
break;
case TARGET_RGB:
// Take TGA->RGB, RGBA->RGB
switch(j) {
case 1:
//TGA->RGB
Write = MakeName(argv[i], TARGET_SGI);
if(Image.LoadTGA(argv[i])) { Image.SaveRGB(Write); Image.Destroy(); }
delete Write;
break;
case 2:
//RGB->RGB
Write = MakeName(argv[i], TARGET_SGI);
if(Image.LoadRGB(argv[i])) { Image.SaveRGB(Write); Image.Destroy(); }
delete Write;
break;
case 3:
//RGBA->RGB
Write = MakeName(argv[i], TARGET_SGI);
if(Image.LoadRGBA(argv[i])) { Image.SaveRGB(Write); Image.Destroy(); }
delete Write;
break;
}
break;
case TARGET_RGBA:
// Take TGA->RGBA, RGB->RGBA
switch(j) {
case 1:
//TGA->RGBA
Write = MakeName(argv[i], TARGET_SGI);
if(Image.LoadTGA(argv[i])) { Image.SaveRGBA(Write); Image.Destroy(); }
delete Write;
break;
case 2:
//RGB->RGBA
Write = MakeName(argv[i], TARGET_SGI);
if(Image.LoadRGB(argv[i])) { Image.SaveRGBA(Write); Image.Destroy(); }
delete Write;
break;
case 3:
//RGBA->RGB
Write = MakeName(argv[i], TARGET_SGI);
if(Image.LoadRGBA(argv[i])) { Image.SaveRGBA(Write); Image.Destroy(); }
delete Write;
break;
}
break;
}

}

} while(++i < argc);

} 
*/




void TestLoadSGI()
{
	Bitmap image;
	char *inName = "c:/blum2.sgi";
	char *outName = "c:/blum.tga";
	if (image.LoadRGB(inName) )
	{
		image.SaveTGA(outName); 
		image.Destroy(); 
	}

};

/*
* Local functions...
*/

static int	getlong(FILE *);
static int	getshort(FILE *);
static int	putlong(long, FILE *);
static int	putshort(unsigned short, FILE *);
static int	read_rle8(FILE *, unsigned short *, int);
static int	read_rle16(FILE *, unsigned short *, int);
static int	write_rle8(FILE *, unsigned short *, int);
static int	write_rle16(FILE *, unsigned short *, int);


/*
* 'sgiClose()' - Close an SGI image file.
*/

int
sgiClose(sgi_t *sgip)	/* I - SGI image */
{
	int	i;		/* Return status */
	long	*offset;	/* Looping var for offset table */


	if (sgip == NULL)
		return (-1);

	if (sgip->mode == SGI_WRITE && sgip->comp != SGI_COMP_NONE)
	{
		/*
		* Write the scanline offset table to the file...
		*/

		fseek(sgip->file, 512, SEEK_SET);

		for (i = sgip->ysize * sgip->zsize, offset = sgip->table[0];
			i > 0;
			i --, offset ++)
			if (putlong(offset[0], sgip->file) < 0)
				return (-1);

		for (i = sgip->ysize * sgip->zsize, offset = sgip->length[0];
			i > 0;
			i --, offset ++)
			if (putlong(offset[0], sgip->file) < 0)
				return (-1);
	};

	if (sgip->table != NULL)
	{
		free(sgip->table[0]);
		free(sgip->table);
	};

	if (sgip->length != NULL)
	{
		free(sgip->length[0]);
		free(sgip->length);
	};

	if (sgip->comp == SGI_COMP_ARLE)
		free(sgip->arle_row);

	i = fclose(sgip->file);
	free(sgip);

	return (i);
}


/*
* 'sgiGetRow()' - Get a row of image data from a file.
*/

int
sgiGetRow(sgi_t          *sgip,	/* I - SGI image */
		  unsigned short *row,	/* O - Row to read */
		  int            y,	/* I - Line to read */
		  int            z)	/* I - Channel to read */
{
	int	x;		/* X coordinate */
	long	offset;		/* File offset */


	if (sgip == NULL ||
		row == NULL ||
		y < 0 || y >= sgip->ysize ||
		z < 0 || z >= sgip->zsize)
		return (-1);

	switch (sgip->comp)
	{
	case SGI_COMP_NONE :
		/*
		* Seek to the image row - optimize buffering by only seeking if
		* necessary...
		*/

		offset = 512 + (y + z * sgip->ysize) * sgip->xsize * sgip->bpp;
		if (offset != ftell(sgip->file))
			fseek(sgip->file, offset, SEEK_SET);

		if (sgip->bpp == 1)
		{
			for (x = sgip->xsize; x > 0; x --, row ++)
				*row = getc(sgip->file);
		}
		else
		{
			for (x = sgip->xsize; x > 0; x --, row ++)
				*row = getshort(sgip->file);
		};
		break;

	case SGI_COMP_RLE :
		offset = sgip->table[z][y];
		if (offset != ftell(sgip->file))
			fseek(sgip->file, offset, SEEK_SET);

		if (sgip->bpp == 1)
			return (read_rle8(sgip->file, row, sgip->xsize));
		else
			return (read_rle16(sgip->file, row, sgip->xsize));
		break;
	};

	return (0);
}


/*
* 'sgiOpen()' - Open an SGI image file for reading or writing.
*/

sgi_t *
sgiOpen(const char *filename,	/* I - File to open */
		int  mode,	/* I - Open mode (SGI_READ or SGI_WRITE) */
		int  comp,	/* I - Type of compression */
		int  bpp,	/* I - Bytes per pixel */
		int  xsize,	/* I - Width of image in pixels */
		int  ysize,	/* I - Height of image in pixels */
		int  zsize)	/* I - Number of channels */
{
	sgi_t	*sgip;		/* New SGI image file */
	FILE	*file;		/* Image file pointer */


	if (mode == SGI_READ)
		file = fopen(filename, "rb");
	else
		file = fopen(filename, "wb+");

	if (file == NULL)
		return (NULL);

	if ((sgip = sgiOpenFile(file, mode, comp, bpp, xsize, ysize, zsize)) == NULL)
		fclose(file);

	return (sgip);
}


/*
* 'sgiOpenFile()' - Open an SGI image file for reading or writing.
*/

sgi_t *
sgiOpenFile(FILE *file,	/* I - File to open */
			int  mode,	/* I - Open mode (SGI_READ or SGI_WRITE) */
			int  comp,	/* I - Type of compression */
			int  bpp,	/* I - Bytes per pixel */
			int  xsize,	/* I - Width of image in pixels */
			int  ysize,	/* I - Height of image in pixels */
			int  zsize)	/* I - Number of channels */
{
	int	i, j;		/* Looping var */
	char	name[80];	/* Name of file in image header */
	short	magic;		/* Magic number */
	sgi_t	*sgip;		/* New image pointer */


	if ((sgip = (sgi_t*)calloc(sizeof(sgi_t), 1)) == NULL)
		return (NULL);

	sgip->file = file;

	switch (mode)
	{
	case SGI_READ :
		sgip->mode = SGI_READ;

		magic = getshort(sgip->file);
		if (magic != SGI_MAGIC)
		{
			free(sgip);
			return (NULL);
		};

		sgip->comp  = getc(sgip->file);
		sgip->bpp   = getc(sgip->file);
		getshort(sgip->file);		/* Dimensions */
		sgip->xsize = getshort(sgip->file);
		sgip->ysize = getshort(sgip->file);
		sgip->zsize = getshort(sgip->file);
		getlong(sgip->file);		/* Minimum pixel */
		getlong(sgip->file);		/* Maximum pixel */

		if (sgip->comp)
		{
			/*
			* This file is compressed; read the scanline tables...
			*/

			fseek(sgip->file, 512, SEEK_SET);

			sgip->table    = (long**)calloc(sgip->zsize, sizeof(long *));
			sgip->table[0] = (long*)calloc(sgip->ysize * sgip->zsize, sizeof(long));
			for (i = 1; i < sgip->zsize; i ++)
				sgip->table[i] = sgip->table[0] + i * sgip->ysize;

			for (i = 0; i < sgip->zsize; i ++)
				for (j = 0; j < sgip->ysize; j ++)
					sgip->table[i][j] = getlong(sgip->file);
		};
		break;

	case SGI_WRITE :
		if (xsize < 1 ||
			ysize < 1 ||
			zsize < 1 ||
			bpp < 1 || bpp > 2 ||
			comp < SGI_COMP_NONE || comp > SGI_COMP_ARLE)
		{
			free(sgip);
			return (NULL);
		};

		sgip->mode = SGI_WRITE;

		putshort(SGI_MAGIC, sgip->file);
		putc((sgip->comp = comp) != 0, sgip->file);
		putc(sgip->bpp = bpp, sgip->file);
		putshort(3, sgip->file);		/* Dimensions */
		putshort(sgip->xsize = xsize, sgip->file);
		putshort(sgip->ysize = ysize, sgip->file);
		putshort(sgip->zsize = zsize, sgip->file);
		if (bpp == 1)
		{
			putlong(0, sgip->file);	/* Minimum pixel */
			putlong(255, sgip->file);	/* Maximum pixel */
		}
		else
		{
			putlong(-32768, sgip->file);	/* Minimum pixel */
			putlong(32767, sgip->file);	/* Maximum pixel */
		};
		putlong(0, sgip->file);		/* Reserved */

		memset(name, 0, sizeof(name));
		fwrite(name, sizeof(name), 1, sgip->file);

		for (i = 0; i < 102; i ++)
			putlong(0, sgip->file);

		switch (comp)
		{
		case SGI_COMP_NONE : /* No compression */
			/*
			* This file is uncompressed.  To avoid problems with sparse files,
			* we need to write blank pixels for the entire image...
			*/

			if (bpp == 1)
			{
				for (i = xsize * ysize * zsize; i > 0; i --)
					putc(0, sgip->file);
			}
			else
			{
				for (i = xsize * ysize * zsize; i > 0; i --)
					putshort(0, sgip->file);
			};
			break;

		case SGI_COMP_ARLE : /* Aggressive RLE */
			sgip->arle_row    = (unsigned short *)calloc(xsize, sizeof(unsigned short));
			sgip->arle_offset = 0;

		case SGI_COMP_RLE : /* Run-Length Encoding */
			/*
			* This file is compressed; write the (blank) scanline tables...
			*/

			for (i = 2 * ysize * zsize; i > 0; i --)
				putlong(0, sgip->file);

			sgip->firstrow = ftell(sgip->file);
			sgip->nextrow  = ftell(sgip->file);
			sgip->table    = (long**)calloc(sgip->zsize, sizeof(long *));
			sgip->table[0] = (long*)calloc(sgip->ysize * sgip->zsize, sizeof(long));
			for (i = 1; i < sgip->zsize; i ++)
				sgip->table[i] = sgip->table[0] + i * sgip->ysize;
			sgip->length    = (long**)calloc(sgip->zsize, sizeof(long *));
			sgip->length[0] = (long*)calloc(sgip->ysize * sgip->zsize, sizeof(long));
			for (i = 1; i < sgip->zsize; i ++)
				sgip->length[i] = sgip->length[0] + i * sgip->ysize;
			break;
		};
		break;

	default :
		free(sgip);
		return (NULL);
	};

	return (sgip);
}


/*
* 'sgiPutRow()' - Put a row of image data to a file.
*/

int
sgiPutRow(sgi_t          *sgip,	/* I - SGI image */
		  unsigned short *row,	/* I - Row to write */
		  int            y,	/* I - Line to write */
		  int            z)	/* I - Channel to write */
{
	int	x;		/* X coordinate */
	long	offset;		/* File offset */


	if (sgip == NULL ||
		row == NULL ||
		y < 0 || y >= sgip->ysize ||
		z < 0 || z >= sgip->zsize)
		return (-1);

	switch (sgip->comp)
	{
	case SGI_COMP_NONE :
		/*
		* Seek to the image row - optimize buffering by only seeking if
		* necessary...
		*/

		offset = 512 + (y + z * sgip->ysize) * sgip->xsize * sgip->bpp;
		if (offset != ftell(sgip->file))
			fseek(sgip->file, offset, SEEK_SET);

		if (sgip->bpp == 1)
		{
			for (x = sgip->xsize; x > 0; x --, row ++)
				putc(*row, sgip->file);
		}
		else
		{
			for (x = sgip->xsize; x > 0; x --, row ++)
				putshort(*row, sgip->file);
		};
		break;

	case SGI_COMP_ARLE :
		if (sgip->table[z][y] != 0)
			return (-1);

		/*
		* First check the last row written...
		*/

		if (sgip->arle_offset > 0)
		{
			for (x = 0; x < sgip->xsize; x ++)
				if (row[x] != sgip->arle_row[x])
					break;

			if (x == sgip->xsize)
			{
				sgip->table[z][y]  = sgip->arle_offset;
				sgip->length[z][y] = sgip->arle_length;
				return (0);
			};
		};

		/*
		* If that didn't match, search all the previous rows...
		*/

		fseek(sgip->file, sgip->firstrow, SEEK_SET);

		if (sgip->bpp == 1)
		{
			do
			{
				sgip->arle_offset = ftell(sgip->file);
				if ((sgip->arle_length = read_rle8(sgip->file, sgip->arle_row, sgip->xsize)) < 0)
				{
					x = 0;
					break;
				};

				for (x = 0; x < sgip->xsize; x ++)
					if (row[x] != sgip->arle_row[x])
						break;
			}
			while (x < sgip->xsize);
		}
		else
		{
			do
			{
				sgip->arle_offset = ftell(sgip->file);
				if ((sgip->arle_length = read_rle16(sgip->file, sgip->arle_row, sgip->xsize)) < 0)
				{
					x = 0;
					break;
				};

				for (x = 0; x < sgip->xsize; x ++)
					if (row[x] != sgip->arle_row[x])
						break;
			}
			while (x < sgip->xsize);
		};

		if (x == sgip->xsize)
		{
			sgip->table[z][y]  = sgip->arle_offset;
			sgip->length[z][y] = sgip->arle_length;
			return (0);
		}
		else
			fseek(sgip->file, 0, SEEK_END);	/* Clear EOF */

	case SGI_COMP_RLE :
		if (sgip->table[z][y] != 0)
			return (-1);

		offset = sgip->table[z][y] = sgip->nextrow;

		if (offset != ftell(sgip->file))
			fseek(sgip->file, offset, SEEK_SET);

		if (sgip->bpp == 1)
			x = write_rle8(sgip->file, row, sgip->xsize);
		else
			x = write_rle16(sgip->file, row, sgip->xsize);

		if (sgip->comp == SGI_COMP_ARLE)
		{
			sgip->arle_offset = offset;
			sgip->arle_length = x;
			memcpy(sgip->arle_row, row, sgip->xsize * sizeof(short));
		};

		sgip->nextrow      = ftell(sgip->file);
		sgip->length[z][y] = x;

		return (x);
	};

	return (0);
}


/*
* 'getlong()' - Get a 32-bit big-endian integer.
*/

static int
getlong(FILE *fp)	/* I - File to read from */
{
	unsigned char	b[4];


	fread(b, 4, 1, fp);
	return ((b[0] << 24) | (b[1] << 16) | (b[2] << 8) | b[3]);
}


/*
* 'getshort()' - Get a 16-bit big-endian integer.
*/

static int
getshort(FILE *fp)	/* I - File to read from */
{
	unsigned char	b[2];


	fread(b, 2, 1, fp);
	return ((b[0] << 8) | b[1]);
}


/*
* 'putlong()' - Put a 32-bit big-endian integer.
*/

static int
putlong(long n,		/* I - Long to write */
		FILE *fp)	/* I - File to write to */
{
	if (putc(n >> 24, fp) == EOF)
		return (EOF);
	if (putc(n >> 16, fp) == EOF)
		return (EOF);
	if (putc(n >> 8, fp) == EOF)
		return (EOF);
	if (putc(n, fp) == EOF)
		return (EOF);
	else
		return (0);
}


/*
* 'putshort()' - Put a 16-bit big-endian integer.
*/

static int
putshort(unsigned short n,	/* I - Short to write */
		 FILE           *fp)	/* I - File to write to */
{
	if (putc(n >> 8, fp) == EOF)
		return (EOF);
	if (putc(n, fp) == EOF)
		return (EOF);
	else
		return (0);
}


/*
* 'read_rle8()' - Read 8-bit RLE data.
*/

static int
read_rle8(FILE           *fp,	/* I - File to read from */
		  unsigned short *row,	/* O - Data */
		  int            xsize)	/* I - Width of data in pixels */
{
	int	i,		/* Looping var */
		ch,		/* Current character */
		count,		/* RLE count */
		length;		/* Number of bytes read... */


	length = 0;

	while (xsize > 0)
	{
		if ((ch = getc(fp)) == EOF)
			return (-1);
		length ++;

		count = ch & 127;
		if (count == 0)
			break;

		if (ch & 128)
		{
			for (i = 0; i < count; i ++, row ++, xsize --, length ++)
				*row = getc(fp);
		}
		else
		{
			ch = getc(fp);
			length ++;
			for (i = 0; i < count; i ++, row ++, xsize --)
				*row = ch;
		};
	};

	return (xsize > 0 ? -1 : length);
}


/*
* 'read_rle16()' - Read 16-bit RLE data.
*/

static int
read_rle16(FILE           *fp,	/* I - File to read from */
		   unsigned short *row,	/* O - Data */
		   int            xsize)/* I - Width of data in pixels */
{
	int	i,		/* Looping var */
		ch,		/* Current character */
		count,		/* RLE count */
		length;		/* Number of bytes read... */


	length = 0;

	while (xsize > 0)
	{
		if ((ch = getshort(fp)) == EOF)
			return (-1);
		length ++;

		count = ch & 127;
		if (count == 0)
			break;

		if (ch & 128)
		{
			for (i = 0; i < count; i ++, row ++, xsize --, length ++)
				*row = getshort(fp);
		}
		else
		{
			ch = getshort(fp);
			length ++;
			for (i = 0; i < count; i ++, row ++, xsize --)
				*row = ch;
		};
	};

	return (xsize > 0 ? -1 : length * 2);
}


/*
* 'write_rle8()' - Write 8-bit RLE data.
*/

static int
write_rle8(FILE           *fp,	/* I - File to write to */
		   unsigned short *row,	/* I - Data */
		   int            xsize)/* I - Width of data in pixels */
{
	int			length,	/* Length of output line */
		count,	/* Number of repeated/non-repeated pixels */
		i,	/* Looping var */
		x;	/* Looping var */
	unsigned short	*start,	/* Start of sequence */
		repeat;	/* Repeated pixel */


	for (x = xsize, length = 0; x > 0;)
	{
		start = row;
		row   += 2;
		x     -= 2;

		while (x > 0 && (row[-2] != row[-1] || row[-1] != row[0]))
		{
			row ++;
			x --;
		};

		row -= 2;
		x   += 2;

		count = row - start;
		while (count > 0)
		{
			i     = count > 126 ? 126 : count;
			count -= i;

			if (putc(128 | i, fp) == EOF)
				return (-1);
			length ++;

			while (i > 0)
			{
				if (putc(*start, fp) == EOF)
					return (-1);
				start ++;
				i --;
				length ++;
			};
		};

		if (x <= 0)
			break;

		start  = row;
		repeat = row[0];

		row ++;
		x --;

		while (x > 0 && *row == repeat)
		{
			row ++;
			x --;
		};

		count = row - start;
		while (count > 0)
		{
			i     = count > 126 ? 126 : count;
			count -= i;

			if (putc(i, fp) == EOF)
				return (-1);
			length ++;

			if (putc(repeat, fp) == EOF)
				return (-1);
			length ++;
		};
	};

	length ++;

	if (putc(0, fp) == EOF)
		return (-1);
	else
		return (length);
}


/*
* 'write_rle16()' - Write 16-bit RLE data.
*/

static int
write_rle16(FILE           *fp,	/* I - File to write to */
			unsigned short *row,/* I - Data */
			int            xsize)/* I - Width of data in pixels */
{
	int			length,	/* Length of output line */
		count,	/* Number of repeated/non-repeated pixels */
		i,	/* Looping var */
		x;	/* Looping var */
	unsigned short	*start,	/* Start of sequence */
		repeat;	/* Repeated pixel */


	for (x = xsize, length = 0; x > 0;)
	{
		start = row;
		row   += 2;
		x     -= 2;

		while (x > 0 && (row[-2] != row[-1] || row[-1] != row[0]))
		{
			row ++;
			x --;
		};

		row -= 2;
		x   += 2;

		count = row - start;
		while (count > 0)
		{
			i     = count > 126 ? 126 : count;
			count -= i;

			if (putshort(128 | i, fp) == EOF)
				return (-1);
			length ++;

			while (i > 0)
			{
				if (putshort(*start, fp) == EOF)
					return (-1);
				start ++;
				i --;
				length ++;
			};
		};

		if (x <= 0)
			break;

		start  = row;
		repeat = row[0];

		row ++;
		x --;

		while (x > 0 && *row == repeat)
		{
			row ++;
			x --;
		};

		count = row - start;
		while (count > 0)
		{
			i     = count > 126 ? 126 : count;
			count -= i;

			if (putshort(i, fp) == EOF)
				return (-1);
			length ++;

			if (putshort(repeat, fp) == EOF)
				return (-1);
			length ++;
		};
	};

	length ++;

	if (putshort(0, fp) == EOF)
		return (-1);
	else
		return (2 * length);
}


/*
* End of "$Id: sgilib.c,v 1.5 1998/04/23 17:40:49 mike Exp mike $".
*/

/*
typedef unsigned int uint;

void Resize(uint32 *ps,uint32 sw,uint32 sh,uint32 *pd,uint32 dw,uint32 dh)
{
uint32 i,j,y;
y=0;
uint32 dx=(sw<<16)/dw;
uint32 dy=(sh<<16)/dh;
for(j=0;j<dh;j++)
{
uint32 x=0;
uint32 *r=&ps[(y>>16)*sw];
for(i=0;i<dw;i++)
{
pd[i]=r[x>>16];
x+=dx;
}
pd+=dw;
y+=dy;
}
}
*/

bool LoadSGIAndStoreTGA(const std::string &filename,std::string &outFilename)
{
	sgi_t *sgi;
	
	//	char *outName = "c:/blum.tga";
	std::string outName = osgDB::getNameLessExtension(filename)+".tga";
	outFilename = outName;

	sgi = sgiOpen(filename.c_str(), SGI_READ, 0, 0, 0, 0, 0);
	if (!sgi)
	{	  
		return false;
	}
	
	Bitmap image;
	unsigned long xsize = sgi->xsize;
	unsigned long ysize = sgi->ysize;
	unsigned long zsize = sgi->zsize;
	image.Create(xsize, ysize);

	iColour *pixels = image.Pixel;

	cout<< filename<<" bits size:" << zsize*8 <<endl;
	unsigned short *row = new unsigned short[xsize];

	for (unsigned int y = 0; y <ysize; ++y)
		for (unsigned int z = 0; z < zsize; ++z)
		{
			sgiGetRow(sgi, row, y, z);

			iColour *pixrow = &pixels[(ysize-y-1)*xsize];
			for (unsigned int x = 0; x < xsize; ++x)
			{
				iColour &pix = pixrow[x];
				if (z == 0)
					pix.r = row[x];
				else if (z == 1)
					pix.g = row[x];
				else if (z == 2)
					pix.b = row[x];
				else if (z == 3)
					pix.a = row[x];
			}
		}

		// fill grayscale
		if(zsize==1)
			for (unsigned int i=0;i<xsize*ysize;++i)
				pixels[i].g = pixels[i].b = pixels[i].r;

		// fill alpha
		if(zsize<4)
			for (unsigned int i=0;i<xsize*ysize;++i)
				pixels[i].a = 0xff;

		delete row;

		image.SaveTGA(outName.c_str()); 
		image.Destroy(); 


		sgiClose(sgi);
		return true;
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------
bool ReadTGA32(const char *pFilename,uint8 *&pImage,uint32 &ImageWidth,uint32 &ImageHeight,
				uint32 &ImageDesc,uint32 &ImageHasAlpha, TGAHeader &TGAHdr)
{
	FILE *f;

	f=fopen(pFilename,"rb");
	if(!f)
	{
		printf("File cannot be accessed.\n");
		return false;
	}

	fread(TGAHdr.pData(),1,18,f);

	if(((TGAHdr.ImageType()!=2)&&(TGAHdr.ImageType()!=10))||((TGAHdr.ImageBits()!=32)&&(TGAHdr.ImageBits()!=24)))
	{
		fclose(f);
		printf("Only 24 or 32 bit TGA files can be processed. (TGA Image Type:%d Bits:%d)\n",TGAHdr.ImageType(),TGAHdr.ImageBits());
		return false;
	}
	ImageWidth=TGAHdr.ImageW();ImageHeight=TGAHdr.ImageH();ImageDesc=TGAHdr.ImageDesc();ImageHasAlpha=(TGAHdr.ImageBits()==32);
	pImage=new uint8 [ImageWidth*(ImageHeight+1)*4];
	fread(pImage,1,TGAHdr.IDLen(),f);
	if(TGAHdr.ImageType()==2)
		fread(pImage,1,ImageWidth*ImageHeight*(TGAHdr.ImageBits()>>3),f);
	else
	{
		uint32 n,i,j=0;
		n=TGAHdr.ImageBits()>>3;
		uint32 l=(ImageWidth*ImageHeight*n);
		while(j<l)
		{
			uint8 b,r;
			fread(&b,1,1,f);
			r=(b&0x7f);
			if(b&0x80)
			{
				fread(&pImage[j],1,n,f);
				j+=n;
				for(i=0;i<(r*n);i++)
					pImage[j+i]=pImage[j+i-n];
			}
			else
			{
				r++;
				fread(&pImage[j],1,r*n,f);
			}
			j+=(r*n);
		}
	}
	fclose(f);

	/*
	// fill alpha
	if(TGAHdr.ImageBits()==32)
	{
		for (unsigned int i=0;i<ImageWidth*ImageHeight;++i)
			pImage[i*4+3]=0xff;
	}*/

	return true;
}

//-----------------------------------------------------------------------------
void WriteTGA32(const char *pFilename,uint32 *pImage,uint32 ImageWidth,uint32 ImageHeight,
				uint32 ImageDesc,TGAHeader &TGAHdr)
{
	/*
	TGAHeader TGAHdr(
	14, 0, 2,
	0, 0, 0,
	0, 0, ImageWidth, ImageHeight,
	32, (ImageDesc&0xf0)|8
	);*/

	FILE *f;
	f=fopen(pFilename,"wb");
	if(!f)
	{
		printf("File cannot be written.\n");
		return;
	}
	TGAHdr.ImageW(ImageWidth);
	TGAHdr.ImageH(ImageHeight);
	fwrite(TGAHdr.pData(),18,1,f);
	fwrite("carda",0,1,f);
	fwrite(pImage,4,ImageWidth*ImageHeight,f);
	fclose(f);
}


void Resize(uint32 *ps,uint32 sw,uint32 sh,uint32 *pd,uint32 dw,uint32 dh)
{
	uint32 i,j,y;
	y=0;
	uint32 dx=(sw<<16)/dw;
	uint32 dy=(sh<<16)/dh;
	for(j=0;j<dh;j++)
	{
		uint32 x=0;
		uint32 *r=&ps[(y>>16)*sw];
		for(i=0;i<dw;i++)
		{
			pd[i]=r[x>>16];
			x+=dx;
		}
		pd+=dw;
		y+=dy;
	}
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


using namespace OxygeneSDK;

static const std::string mesh_name = "p3dMesh";

// ---------------------------------------------------------------------------

namespace OsgP3DObject
{

	// ---------------------------------------------------------------------------
	void OsgGeodeToP3D::getFacesFromGeometry(const osg::Geometry * i_osgGeometry, 
		FaceList & io_faceList, VertexList & io_vertexList, const unsigned int i_offset, 
		const osg::StateSet * state)
	{
		unsigned int i=0;
		for (unsigned pri=0; pri<i_osgGeometry->getNumPrimitiveSets(); ++pri) 
		{
			const osg::PrimitiveSet *pset = i_osgGeometry->getPrimitiveSet(pri);

			unsigned N = pset->getNumIndices();

			switch (pset->getMode()) 
			{

			case osg::PrimitiveSet::TRIANGLES:
				{
					for (i=0; i<N; i+=3) 
					{
						Face triangleFace = getFace(i, i+1, i+2, i_offset, pset, state);
						io_faceList.push_back(triangleFace);						
					}

					cout<< "triangles" <<endl;
					break;
				}

			case osg::PrimitiveSet::QUADS:
				for (i=0; i<N; i+=4) 
				{
					io_faceList.push_back(getFace( i, i+1, i+2, i_offset, pset, state));
					io_faceList.push_back(getFace( i+2, i+3, i, i_offset, pset, state));
				}
				cout<< "quads" <<endl;
				break;

			case osg::PrimitiveSet::TRIANGLE_STRIP:
				if (pset->getType() == osg::PrimitiveSet::DrawArrayLengthsPrimitiveType) 
				{
					osg::DrawArrayLengths *dal = (osg::DrawArrayLengths *)(pset);
					unsigned j = 0;
					for (osg::DrawArrayLengths::const_iterator pi=dal->begin(); pi!=dal->end(); ++pi) 
					{
						unsigned iN = static_cast<unsigned>(*pi-2);
						for (i=0; i<iN; ++i, ++j) 
						{
							if ((i%2) == 0) 
							{
								io_faceList.push_back(getFace(j, j+1, j+2, i_offset, pset, state));
							} else 
							{
								io_faceList.push_back(getFace(j+1, j, j+2, i_offset, pset, state));
							}
						}
						j += 2;
					}
				} else 
				{
					for (i=0; i<N-2; ++i) 
					{
						if ((i%2) == 0) 
						{
							io_faceList.push_back(getFace(i, i+1, i+2, i_offset, pset, state));
						} else {
							io_faceList.push_back(getFace(i+1, i, i+2, i_offset, pset, state));

						}
					}
				}
				cout<< "trianglestrip" <<endl;
				break;

			case osg::PrimitiveSet::TRIANGLE_FAN:
				if (pset->getType() == osg::PrimitiveSet::DrawArrayLengthsPrimitiveType) 
				{
					osg::DrawArrayLengths *dal = (osg::DrawArrayLengths *)(pset);
					unsigned j = 0;
					for (osg::DrawArrayLengths::const_iterator pi=dal->begin(); pi!=dal->end(); ++pi) 
					{
						unsigned iN = static_cast<unsigned>(*pi-2);
						for (i=0; i<iN; ++i) 
						{
							io_faceList.push_back(getFace(0, j+1, j+2, i_offset, pset, state));
						}
						j += 2;

					}
					cout<< "0,j+1,j+2" <<endl;
				} else {
					for (i=0; i<N-2; ++i) 
					{
						io_faceList.push_back(getFace(0, i+1, i+2, i_offset, pset, state));
		
					}
				}
				cout<< "trianglefan" <<endl;
				break;

			case osg::PrimitiveSet::POINTS:
			case osg::PrimitiveSet::LINES:
			case osg::PrimitiveSet::LINE_STRIP:
			case osg::PrimitiveSet::LINE_LOOP:
				break;

			default: osg::notify(osg::WARN) << "Warning: TangentSpaceGenerator: unknown primitive mode " << pset->getMode() << "\n";

			}
		}

		const osg::Vec3Array* vertexArray = (osg::Vec3Array*)(i_osgGeometry->getVertexArray());
		if (vertexArray)
		{				
		  unsigned int numberOfVertexs = ((osg::Vec3Array*)i_osgGeometry->getVertexArray())->size();
		  for( i = 0; i < numberOfVertexs; i++)
		  {
			  io_vertexList.push_back(convertVertexAxis((*vertexArray)[i]));
		  }
    }
	}


	// ---------------------------------------------------------------------------
	bool OsgGeodeToP3D::convert(const osg::Node *osgNode, std::ostream &os,const std::string &materialPrefix)
	{		
		bool bFirst = true;
		unsigned int numberOfVertexs = 0;
		unsigned int numberOfTex = 0;
		geodeVisitor vs; // this collects geodes.
		osg::Node *nd=(osg::Node *)(osgNode);
		nd->accept(vs); // this parses the tree to find Geodes
		std::vector<const osg::Geode *> glist=vs.getGeodes();


		unsigned int meshCounter = 0;

		FaceList faceList;
		VertexList vertexList;

		unsigned int matCounter = 0 ;
		unsigned int emptyMatCounter = 0 ;
		unsigned int subMeshCounter = 0;

		printf("OxygeneSDK version %g\n",OxygeneSDK::GetSDKVersion());
		printf("Generating toroid\n");
		OxygeneSDK::LODMesh ldmesh;

		for (unsigned int i = 0; i < glist.size(); i++)
		{
			const osg::Geode * currentGeode = glist[i];

			const unsigned int iNumDrawables = currentGeode->getNumDrawables();



			for (unsigned int j = 0; j < iNumDrawables; j++)
			{
				const osg::Geometry* geometry = currentGeode->getDrawable(j)->asGeometry();

				if (geometry) {


					const osg::StateSet* theState = currentGeode->getStateSet();
					if (NULL == theState)
					{
						theState = geometry->getStateSet();
					}

					//if (NULL != theState)
					{
						getFacesFromGeometry(geometry, faceList, vertexList, numberOfVertexs, theState);
					}

					string matName;
					if(_stateMap.find(theState) != _stateMap.end())	
						matName = _stateMap[theState];
					else
					{
						std::ostringstream os;
						os<<matCounter;
						matName = materialPrefix + "_" + os.str()+"m";
						matCounter++;
						// first - the material
						if (theState) 
						{
							std::string textureFile = convertMaterial(theState, matName/*, io_programList*/);
							std::cout<<"textureFile: ["<< textureFile <<"]"<<std::endl;
							textureFile = convertRgbToTga(textureFile);
							if (textureFile.empty())
								++emptyMatCounter;

							textureFile = osgDB::getNameLessExtension(osgDB::getSimpleFileName(textureFile))+".tga";
							//io_matSer.queueForExport(ogreMaterial);
							_stateMap[theState] = matName;
							_textureMap[theState] = textureFile;
							cout<<"material name: "<<matName;
							cout<<"texture filename: "<<textureFile<<endl;
						}
					}

					osg::Geometry::PrimitiveSetList::const_iterator pItr;
					for(pItr = geometry->getPrimitiveSetList().begin(); pItr != geometry->getPrimitiveSetList().end(); ++pItr) 
					{

						const osg::PrimitiveSet* primitiveset = pItr->get();

						/*
						OsgPrimitiveToOgreSubMesh SubMeshCov(_omp);
						string subMeshName = i_meshName + "_" + Ogre::StringConverter::toString(subMeshCounter);
						Ogre::SubMesh*  ogreSubMesh = SubMeshCov.convert(primitiveset, subMeshName, ogreMesh, numberOfVertexs);
						if (ogreSubMesh)
						{
						subMeshCounter++;
						ogreSubMesh->setMaterialName(matName);
						}
						*/

					}
					osg::Vec3Array* verts = 
					  (osg::Vec3Array*)(geometry->getVertexArray());
					
					if (verts)
					  numberOfVertexs += verts->size();

					if (NULL != theState)
					{
						if (numberOfTex < theState->getTextureAttributeList().size())
							numberOfTex = theState->getTextureAttributeList().size();
					}


				}
			}
		}


		// SOME THINGS with tex coords
		int countVerts=0;
		UVList vecUV;
		for (unsigned int i = 0; i < glist.size(); i++)
		{
			const osg::Geode * currentGeode = glist[i];
			const unsigned int iNumDrawables = currentGeode->getNumDrawables();

			for (unsigned int j = 0; j < iNumDrawables; j++)
			{
				const osg::Geometry* geometry = currentGeode->getDrawable(j)->asGeometry();


				const osg::StateSet* theState = currentGeode->getStateSet();


				if (geometry) {
					if (NULL==theState)
					{
						theState = geometry->getStateSet();
					}

					const osg::Geometry* geometry = currentGeode->getDrawable(j)->asGeometry();
					// now get the vertexes
					const osg::Vec3Array* vertexArray = (osg::Vec3Array*)(geometry->getVertexArray());
					//const osg::Vec3Array* normalArray = (osg::Vec3Array*)(geometry->getNormalArray());
					numberOfVertexs = vertexArray ? vertexArray->size() : 0;

					unsigned int geometryNumberOfTex = 0;

					if (NULL != theState)
					{
						geometryNumberOfTex = theState->getTextureAttributeList().size();
					}

					cout<< "numberOfVertexs: "<<numberOfVertexs<<endl;
					if (numberOfVertexs > 0)
					{
						// copy texCoords
						if (numberOfTex > 0)
						{
							for(unsigned int k1 = 0; k1 < numberOfTex ; k1++)
							{
								const osg::Vec2Array* texCoordArray = (osg::Vec2Array*)(geometry->getTexCoordArray(k1));


								for (unsigned int k2 = 0 ; k2 < numberOfVertexs ; k2++ )
								{
									if (k1 < geometryNumberOfTex)
									{
										float u = (*texCoordArray)[k2].x();
										float v = (*texCoordArray)[k2].y();
										//cout<<k2+countVerts<< "- U:"<<(*texCoordArray)[k2].x()<<" : ";
										//cout<< "V:"<<(*texCoordArray)[k2].y()<<endl;
										vecUV.push_back(osg::Vec2(u,v));
										++countVerts;
									}
									else
									{
										vecUV.push_back(osg::Vec2(0,0));
										++countVerts;

									}
								}								
							}							
						}

					}




				}
			}


		}
		cout<< "countOfUV: "<<countVerts<<endl;
		cout<< "final verts in list: " <<vertexList.size()<<endl;

		Mesh mm;
		// add vertices into p3d
		for(VertexList::iterator it=vertexList.begin();it!=vertexList.end();++it)
		{
			OxygeneSDK::Vector vx;
			vx->x = (*it).x();
			vx->y = (*it).y();
			vx->z = -(*it).z();
			mm.NewPoint() = vx;		
		}


		bool doUV = (matCounter!=emptyMatCounter);

    // MOD: 8.7. 2009 - changed CCW to CW and 
    // flipped Z axis

		// TODO: add CW/CCW option into arguments
		// add face(CCW)
		for(FaceList::iterator it=faceList.begin();it!=faceList.end();++it)
		{
			std::string textureFile = _textureMap[(*it).state];
			OxygeneSDK::Face fc = OxygeneSDK::Face::Add(mm);
			fc.SetN(3);

			fc.SetPoint(0,(*it).a);//c
			if (doUV)
				fc.SetUV(0,vecUV[(*it).a].x(),vecUV[(*it).a].y());
			
			fc.SetPoint(1,(*it).b);//b
			if (doUV)
				fc.SetUV(1,vecUV[(*it).b].x(),vecUV[(*it).b].y());
			
			fc.SetPoint(2,(*it).c);//a
			if (doUV)
				fc.SetUV(2,vecUV[(*it).c].x(),vecUV[(*it).c].y());

			// modify path to material and texture
			// from P:/muhehe/japapa.mat
			// to   muhehe/japapa.mat
			std::string wktDir=_workingDir;
			if (wktDir.size()>3)
			{
				if (wktDir[1]==':')
					wktDir.erase(0,3);
			}

			//"data\\"+
			textureFile = wktDir+textureFile;
			fc.SetTexture(textureFile.c_str());	

			std::string materialFile = _stateMap[(*it).state] + ".rvmat";
			materialFile = wktDir+materialFile;
			
///////////////////////////////////////////////////////////////
///////////      HACKED for german presenation.. disable setting rvmats to p3d      
			//fc.SetMaterial(materialFile.c_str());
///////////////////////////////////////////////////////////////			
		}


		ldmesh.LinkLevel(mm,1);
		ldmesh.DeleteLevel(0);
		printf("Exporting \n");
		ldmesh.Save(os);

		return true;
	}

	// ---------------------------------------------------------------------------
	uint32 OsgGeodeToP3D::PowerOf2(uint32 v)
	{
		uint32 MinPower=0,MaxPower=30;
		uint32 i;
		if(v<(1<<MinPower)) return MinPower;
		for(i=MinPower;i<MaxPower;i++)
			if(v>=(1<<i)) if(v<(1<<(i+1))) return i+1;
		return MaxPower;
	}

	// ---------------------------------------------------------------------------
	std::string OsgGeodeToP3D::convertRgbToTga(const std::string &filename)
	{
		std::string outFile;
		// create TGA
		if (!LoadSGIAndStoreTGA(filename,outFile))
		{
			cout<< "p3dexp: Error creating tga from: "<<filename<<std::endl;
    
///////////////////////////////////////////////////////////////////////////////
/////////   HACKED BY CARDA: we should return empty string, because
//                           SourceTexture isn't rgba

      if (filename.empty())
        return "";
        
      std::string outName = osgDB::getNameLessExtension(filename)+".tga";
      outName = _workingDir+osgDB::getSimpleFileName(outName);
      
      std::string fileFrom = filename;
      for (size_t i=0; i<fileFrom.size(); ++i)
        if (fileFrom[i]=='\\')
          fileFrom[i]='/';      

      // we have to use original file with original extension
      std::string fileTo = _workingDir+osgDB::getSimpleFileName(filename);
      for (size_t i=0; i<fileTo.size(); ++i)
        if (fileTo[i]=='\\')
          fileTo[i]='/';      

      //CopyFile(fileFrom.c_str(),fileTo.c_str(),false);
			return outName;
///////////////////////////////////////////////////////////////////////////////			
		}

		// Load them
		uint8 *pImage;
		uint32 ImageWidth,ImageHeight,ImageDesc,ImageHasAlpha;
		TGAHeader TGAHdr;
		if(!ReadTGA32(outFile.c_str(),pImage,ImageWidth,ImageHeight,ImageDesc,ImageHasAlpha,TGAHdr))
		{
			cout<< "p3dexp: Error loading tga: "<<outFile<<std::endl;
			return "";
		}

		// resize temp TGA to ^2
		cout<< "width: " << ImageWidth<<" resized to: ";
		int ResizeW = 1<<PowerOf2(ImageWidth);
		cout<<ResizeW<<endl;
		cout<< "heigh: " << ImageHeight<<" resized to: ";
		int ResizeH = 1<<PowerOf2(ImageHeight);		
		cout<<ResizeH<<endl;
		uint32 *d=new uint32 [ResizeW*(ResizeH+1)];
		Resize((uint32 *)pImage,ImageWidth,ImageHeight,d,ResizeW,ResizeH);
		delete [] pImage;
		pImage=(uint8 *)d;
		ImageWidth=ResizeW;ImageHeight=ResizeH;

		std::string fileToDel = outFile;
		for (size_t i=0; i<fileToDel.size(); ++i)
			if (fileToDel[i]=='\\')
				fileToDel[i]='/';

		DeleteFile(fileToDel.c_str());

		//ofstream errOff(std::string(_workingDir+"out.err").c_str(),std::ios::out);
		//errOff<<outFile<<endl;
		//errOff.close();

		std::string textureFile = _workingDir+osgDB::getSimpleFileName(outFile);
		// write TGA
		WriteTGA32(textureFile.c_str(),(uint32 *)pImage,ImageWidth,ImageHeight,ImageDesc,TGAHdr);
		
		cout<<"path to material: "<< outFile <<endl;
		return outFile;
	}

	// ---------------------------------------------------------------------------
	void OsgGeodeToP3D::convertOsgMaterial(const osg::Material *io_pMaterial, const std::string &materialName)
	{
	//////////////////////////////////////////////////////////////////////////
	//////////// HACKED: disabled for German presentation
	  return;
	//////////////////////////////////////////////////////////////////////////
	
		const osg::Vec4& Diffuse	= io_pMaterial->getDiffuse(osg::Material::FRONT_AND_BACK);
		const osg::Vec4& Ambient	= io_pMaterial->getAmbient(osg::Material::FRONT_AND_BACK);
		const osg::Vec4& Emissive	= io_pMaterial->getEmission(osg::Material::FRONT_AND_BACK);
		const osg::Vec4& Specular	= io_pMaterial->getSpecular(osg::Material::FRONT_AND_BACK);
		const float shininess	= io_pMaterial->getShininess(osg::Material::FRONT_AND_BACK);
		
		std::string materialFile = _workingDir+materialName+".rvmat";
		// create rvmat file;
		ofstream os(materialFile.c_str(),std::ios::out);
		/*
		ambient[]={1.000000,1.000000,1.000000,1.000000};
		diffuse[]={1.000000,0.000000,0.000000,1.000000};
		forcedDiffuse[]={0.000000,0.000000,0.000000,0.000000};
		emmisive[]={1.000000,1.000000,1.000000,1.000000};
		specular[]={1.000000,1.000000,1.000000,1.000000};
		*/
		// ambient
		cout<<"material: "<<materialFile<<endl;
		os<<"ambient[]={"<<Ambient.x()<<",";
		os<<Ambient.y()<<",";
		os<<Ambient.z()<<",";
		os<<Ambient.w()<<"};"<<endl;		
		// diffuse
		os<<"diffuse[]={"<<Diffuse.x()<<",";
		os<<Diffuse.y()<<",";
		os<<Diffuse.z()<<",";
		os<<Diffuse.w()<<"};"<<endl;
		os<<"forcedDiffuse[]={0.000000,0.000000,0.000000,0.000000};"<<endl;
		// emissive
		os<<"emmisive[]={"<<Emissive.x()<<",";
		os<<Emissive.y()<<",";
		os<<Emissive.z()<<",";
		os<<Emissive.w()<<"};"<<endl;;
		// specular
		os<<"specular[]={"<<Specular.x()<<",";
		os<<Specular.y()<<",";
		os<<Specular.z()<<",";
		os<<Specular.w()<<"};"<<endl;
		os<<"specularPower="<<shininess<<";"<<endl;
		os.close();
		cout<<"-----------------"<<endl;
	}

	// ---------------------------------------------------------------------------
	std::string OsgGeodeToP3D::convertMaterial(const osg::StateSet* i_osgState,
		const std::string &i_materialName)
	{
	  std::cout<< "ConvertMaterial: " << i_materialName<<std::endl;
		// osg::Material
		const osg::StateSet::RefAttributePair* pRAP;
		pRAP = i_osgState->getAttributePair(osg::StateAttribute::MATERIAL);
		if (NULL != pRAP)
		{
			const osg::Material *pMaterial = dynamic_cast<const osg::Material*>(pRAP->first.get());
			if (NULL != pMaterial)
			{
				cout<< "CREATING MATERIAL"<<endl;
				// diffusion/light/shiness..
				convertOsgMaterial(pMaterial,i_materialName);
			}
		}
      
		osg::StateSet::TextureAttributeList tal = i_osgState->getTextureAttributeList();

		for(unsigned int k = 0 ; k < tal.size(); k++)
		{
			osg::Texture2D* texture = (osg::Texture2D*)(i_osgState->getTextureAttribute(k,osg::StateAttribute::TEXTURE));

			if (NULL != texture)
			{
				if (NULL != texture->getImage())
				{
				  std::string file = texture->getImage()->getFileName();
				  std::string ext = osgDB::getFileExtension(file);
				  if (ext!="rgb" && ext!="rgba")				    
				  {				  				  
				    // TODO: try to create tga from rawData
				    osg::Image* img = texture->getImage();
				    //osgDB::ReaderWriter::writeImage(*img,"c:/_osgTests/some.tga");				    
            file = osgDB::getNameLessExtension(file)+".tga";
            file = _workingDir+osgDB::getSimpleFileName(file);
  //////////////////////////////////////////////////////////////////////////                        
            int width  = img->s();
            int height = img->t();
            TGAHeader TGAHdr(
              0, 0, 2,
              0, 0, 0,
              0, 0, width, height,
              32, 8/*img desc*/
              );

            char* outBuf = new char[width*height*4];
            int offsetSrc = 0;
            for (int y=0; y<height; ++y)
              for (int x=0; x<width; ++x)
            { 
              int offsetDst = (height-y-1)*width+x;
              outBuf[offsetDst*4+0] = img->data()[offsetSrc*3+2];
              outBuf[offsetDst*4+1] = img->data()[offsetSrc*3+1];
              outBuf[offsetDst*4+2] = img->data()[offsetSrc*3+0];
              outBuf[offsetDst*4+3] = 0xff;
              ++offsetSrc;
            }

            // resize to power of 2
            // resize temp TGA to ^2
            cout<< "width: " << width<<" resized to: ";
            int ResizeW = 1<<PowerOf2(width);
            cout<<ResizeW<<endl;
            cout<< "heigh: " << height<<" resized to: ";
            int ResizeH = 1<<PowerOf2(height);		
            cout<<ResizeH<<endl;
            uint32* d=new uint32 [ResizeW*(ResizeH+1)];
            Resize((uint32 *)outBuf,width,height,d,ResizeW,ResizeH);
            delete[] outBuf;
            outBuf=(char*)d;
            width=ResizeW;
            height=ResizeH;

            WriteTGA32 (file.c_str(),(uint32*)outBuf,width,height,0,TGAHdr);
            delete[] outBuf;
  //////////////////////////////////////////////////////////////////////////				  
  				  
				    // doesn't work and don't know why :-)
				    // bool result = osgDB::writeImageFile(*img,file);				    
          }				
					// only first texture
					return file;
				}
			}
		}

		//ogreMaterial->setLightingEnabled(false);
		//curPass->setSceneBlending(Ogre::SBF_SOURCE_ALPHA, Ogre::SBF_ONE_MINUS_SOURCE_ALPHA);
		//curPass->setAlphaRejectSettings(Ogre::CMPF_GREATER, 254);


		const std::string ntf = "";
		return ntf;
	}


	//=============================================================================
	void CreateTempPath()
	{
		/*
		// create temp
		std::string path = m_tempDir + "\\" + m_pboDirectory+"\\";
		wxString wxPath = path;
		wxPath.Replace("/","\\");
		path = wxPath.c_str();
		*/
		std::string path = "fds";
		MakeSureDirectoryPathExists(path.c_str());

		/*
		// copy config_def.xml
		// from /bin
		char buf[256];
		taGetAplicationDir(buf);

		std::string defXml = path + TEST_CONFIG_DEF;
		std::string fromXml = std::string(buf) + "\\"+DEF_DIR+"\\" + TEST_CONFIG_DEF;
		CopyFile(fromXml.c_str(),defXml.c_str(),false);	
		*/
	}
} // namespace

