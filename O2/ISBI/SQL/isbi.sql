-- phpMyAdmin SQL Dump
-- version 2.6.1-rc2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jan 17, 2005 at 06:09 PM
-- Server version: 4.1.9
-- PHP Version: 5.0.3
-- 
-- Database: `isbi`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `article`
-- 

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `ID` int(10) unsigned NOT NULL auto_increment,
  `HistCreated` datetime NOT NULL default '0000-00-00 00:00:00',
  `HistExpired` datetime NOT NULL default '9999-12-30 00:00:00',
  `bindUser` int(10) unsigned NOT NULL default '0',
  `bindParent` int(10) unsigned NOT NULL default '0',
  `bindThread` int(10) unsigned NOT NULL default '0',
  `title` varchar(255) NOT NULL default '',
  `lastChange` datetime default '0000-00-00 00:00:00',
  PRIMARY KEY  (`ID`,`HistCreated`),
  KEY `bindUser` (`bindUser`),
  KEY `bindParent` (`bindParent`),
  KEY `bindThread` (`bindThread`),
  KEY `HistExpired` (`HistExpired`),
  FULLTEXT KEY `title` (`title`)
) TYPE=MyISAM PACK_KEYS=0 COMMENT='Table of articles';

-- --------------------------------------------------------

-- 
-- Table structure for table `articlestatus`
-- 

DROP TABLE IF EXISTS `articlestatus`;
CREATE TABLE IF NOT EXISTS `articlestatus` (
  `bindUser` int(10) unsigned NOT NULL default '0',
  `bindArticle` int(10) unsigned NOT NULL default '0',
  `Status` set('unread','important','favorites') NOT NULL default 'unread',
  PRIMARY KEY  (`bindUser`,`bindArticle`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Table structure for table `content`
-- 

DROP TABLE IF EXISTS `content`;
CREATE TABLE IF NOT EXISTS `content` (
  `ID` int(10) unsigned NOT NULL default '0',
  `bindArticle` int(10) unsigned NOT NULL default '0',
  `Order` smallint(6) NOT NULL default '0',
  `Text` text NOT NULL,
  `Filename` varchar(255) NOT NULL default '',
  `Data` mediumblob NOT NULL,
  `Mode` enum('normal','forceImage','forceLink','forceIframe','hidden') NOT NULL default 'normal',
  `HistCreated` datetime NOT NULL default '0000-00-00 00:00:00',
  `HistExpired` datetime NOT NULL default '9999-12-30 00:00:00',
  PRIMARY KEY  (`ID`,`HistCreated`),
  KEY `bindArticle` (`bindArticle`),
  KEY `HistExpired` (`HistExpired`),
  FULLTEXT KEY `Text` (`Text`,`Filename`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Table structure for table `group`
-- 

DROP TABLE IF EXISTS `group`;
CREATE TABLE IF NOT EXISTS `group` (
  `ID` int(10) unsigned NOT NULL auto_increment,
  `HistCreated` datetime NOT NULL default '0000-00-00 00:00:00',
  `HistExpired` datetime NOT NULL default '9999-12-31 00:00:00',
  `Name` varchar(255) NOT NULL default '',
  `order` varchar(255) NOT NULL default '',
  `bindUser` int(10) unsigned NOT NULL default '0',
  `expireDays` int(10) unsigned default NULL,
  `flags` set('nohistory','readonly','folder') NOT NULL default '',
  PRIMARY KEY  (`ID`,`HistCreated`),
  KEY `order` (`order`),
  KEY `HistExpired` (`HistExpired`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Table structure for table `groupthread`
-- 

DROP TABLE IF EXISTS `groupthread`;
CREATE TABLE IF NOT EXISTS `groupthread` (
  `bindGroup` int(10) unsigned NOT NULL default '0',
  `bindThread` int(10) unsigned NOT NULL default '0',
  `HistCreated` datetime NOT NULL default '0000-00-00 00:00:00',
  `HistExpired` datetime NOT NULL default '9999-12-30 00:00:00',
  PRIMARY KEY  (`bindGroup`,`bindThread`,`HistCreated`),
  KEY `HistExpired` (`HistExpired`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Table structure for table `privatemessages`
-- 

DROP TABLE IF EXISTS `privatemessages`;
CREATE TABLE IF NOT EXISTS `privatemessages` (
  `Sender` int(10) unsigned NOT NULL default '0',
  `Recipient` int(10) unsigned NOT NULL default '0',
  `ID` int(10) unsigned NOT NULL default '0',
  `bindThread` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`ID`),
  KEY `Sender` (`Sender`),
  KEY `Recipient` (`Recipient`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Table structure for table `property`
-- 

DROP TABLE IF EXISTS `property`;
CREATE TABLE IF NOT EXISTS `property` (
  `bindThread` int(10) unsigned NOT NULL default '0',
  `Name` varchar(32) NOT NULL default '',
  `Value` varchar(255) NOT NULL default '',
  `HistCreated` datetime NOT NULL default '0000-00-00 00:00:00',
  `HistExpired` datetime NOT NULL default '9999-12-31 00:00:00',
  PRIMARY KEY  (`bindThread`,`Name`,`HistCreated`),
  KEY `HistExpired` (`HistExpired`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Table structure for table `signedgroups`
-- 

DROP TABLE IF EXISTS `signedgroups`;
CREATE TABLE IF NOT EXISTS `signedgroups` (
  `bindUser` int(10) unsigned NOT NULL default '0',
  `bindGroup` int(10) unsigned NOT NULL default '0',
  `Mode` enum('all','limited','unread') NOT NULL default 'all',
  PRIMARY KEY  (`bindUser`,`bindGroup`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Table structure for table `task`
-- 

DROP TABLE IF EXISTS `task`;
CREATE TABLE IF NOT EXISTS `task` (
  `bindUser` int(10) unsigned NOT NULL default '0',
  `bindThread` int(10) unsigned NOT NULL default '0',
  `HistCreated` datetime NOT NULL default '0000-00-00 00:00:00',
  `HistExpired` datetime NOT NULL default '9999-12-31 00:00:00',
  `bindOwner` int(10) unsigned NOT NULL default '0',
  `Status` enum('assigned','working','paused','done','postponed','rejected','waiting') NOT NULL default 'assigned',
  `Deadline` date default NULL,
  `Priority` enum('Very critical!!!','Critical!!','Very High!','High','Normal','Low','Very Low','An Idea') NOT NULL default 'Normal',
  PRIMARY KEY  (`bindUser`,`bindThread`,`HistCreated`),
  KEY `HistExpired` (`HistExpired`),
  KEY `HistExpired_2` (`HistExpired`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Table structure for table `thread`
-- 

DROP TABLE IF EXISTS `thread`;
CREATE TABLE IF NOT EXISTS `thread` (
  `bindArticle` int(10) unsigned NOT NULL default '0',
  `lastPost` datetime NOT NULL default '0000-00-00 00:00:00',
  `Flags` set('nohistory','readonly') NOT NULL default '',
  PRIMARY KEY  (`bindArticle`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Table structure for table `user`
-- 

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(10) unsigned NOT NULL auto_increment,
  `Login` varchar(50) NOT NULL default '',
  `Password` varchar(50) NOT NULL default '',
  `Email` varchar(255) NOT NULL default '',
  `Name` varchar(50) NOT NULL default '',
  `HistCreated` datetime NOT NULL default '0000-00-00 00:00:00',
  `HistExpired` datetime NOT NULL default '9999-12-31 00:00:00',
  `Rights` set('users','groups','threads','reply','properties','privatemsg','task') NOT NULL default 'groups,threads,reply,properties,privatemsg,task',
  PRIMARY KEY  (`ID`),
  KEY `Login` (`Login`),
  KEY `HistExpired` (`HistExpired`)
) TYPE=MyISAM PACK_KEYS=0;

-- --------------------------------------------------------

-- 
-- Table structure for table `userinfo`
-- 

DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE IF NOT EXISTS `userinfo` (
  `bindUser` int(10) unsigned NOT NULL default '0',
  `Position` varchar(32) NOT NULL default '',
  `Description` text NOT NULL,
  `WebPage` varbinary(255) NOT NULL default '',
  `Image` blob NOT NULL,
  `pageLen` tinyint(4) unsigned NOT NULL default '255',
  PRIMARY KEY  (`bindUser`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Table structure for table `view`
-- 

DROP TABLE IF EXISTS `view`;
CREATE TABLE IF NOT EXISTS `view` (
  `bindUser` int(10) unsigned NOT NULL default '0',
  `Name` varchar(32) NOT NULL default '',
  `Table` enum('article','thread','task') NOT NULL default 'article',
  `Conditions` varchar(255) NOT NULL default '',
  `Note` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`bindUser`,`Name`)
) TYPE=MyISAM;
