<?php
require_once "globalVars.php";
require_once "common.lib.php";
if (!isset($in_page)) $in_page="frames";
if ($in_page!="") require_once "page_".$in_page.".php";
if (!function_exists("OnDatabaseError"))
  {
  function OnDatabaseError($query,$file,$line)
    {
    echo "<h3>Database error</h3>
    <table>
    <tr><th>Query</th><td>$query</td></tr>
    <tr><th>File</th><td>$file</td></tr>
    <tr><th>Line</th><td>$line</td></tr>
    <tr><th>Reason</th><td>".mysql_error()."</td></tr>
    </table>";
    die();   
    }
  }
require_once "dbConnect.php";
require_once "login.php";
require_once "lang/english.lang.php";
header("Content-Type: text/html; charset=utf-8"); 
if (function_exists("BeforePageStart")) BeforePageStart();
echo "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n";
if (function_exists("BeforeHeader"))  BeforeHeader();
echo "<html><head><title>ISBI - Bohemia Interactive Information System</title><style>";
if (function_exists("PageStyle"))  Pagestyle();
echo " @import url(\"default.css\");";
echo "</style></head>";
if (function_exists("AfterHeader")) AfterHeader();
echo "<body>";
if (function_exists("Content")) Content();
echo "</body>";
if (function_exists("BeforeFooter")) BeforeFooter();
echo "</html>";
if (function_exists("AfterFooter")) AfterFooter();
?>

