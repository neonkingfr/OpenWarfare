<?php

function BeforePageStart()
{
  global $StrRes,$userinfo,$in_dlg,$message,$auth_user;
  if (isset($in_dlg))
    {
    mysql_query("INSERT `userinfo`(`bindUser`,`Position`,`Description`,`WebPage`) 
      VALUES ('$userinfo[ID]','$in_dlg[position]','$in_dlg[description]','$in_dlg[webPage]')")
      or mysql_query("UPDATE `userinfo`
                          SET `bindUser`='$userinfo[ID]',
                              `Position`='$in_dlg[position]',
                              `Description`='$in_dlg[description]',
                              `WebPage`='$in_dlg[webPage]'
                              WHERE `bindUser`='$userinfo[ID]'")
      or OnDatabaseError("",__FILE__,__LINE__);
    mysql_query("UPDATE `user` SET `Name`='$in_dlg[name]',`Email`='$in_dlg[email]' WHERE `ID`='$userinfo[ID]'")
      or OnDatabaseError("",__FILE__,__LINE__);
    if ($in_dlg["pwd"]!='********')
      {
      if ($in_dlg["pwd"]!=$in_dlg["pwd2"])
        $message=$StrRes["PasswordsNotSame"];
      else
      {
      mysql_query("UPDATE `user` SET `Password`=PASSWORD('$auth_user|$in_dlg[pwd]') WHERE `ID`='$userinfo[ID]'")
        or OnDatabaseError("",__FILE__,__LINE__);            
      $message=$StrRes["PasswordChanged"];
      }
      }
    } 
  if (isset($_FILES['image']) && $_FILES['image']['name']!="")
  {
    if ($_FILES['image']['size']==0 || $_FILES['image']['size']>64000)
      $message=$StrRes["FileTooLong"];
    else if ($_FILES['image']['type']!='image/jpeg')
      $message=$StrRes["FileMustBeJpeg"];
    else
    {
      $fd=fopen($_FILES['image']['tmp_name'],"r");
      $img=fread($fd,filesize($_FILES['image']['tmp_name']));
      fclose($fd);
      $img=mysql_escape_string($img);
      mysql_query("UPDATE `userinfo` SET `image`='$img' WHERE `bindUser`='$userinfo[ID]'")
        or OnDatabaseError("",__FILE__,__LINE__);                 
    }
  }
}
function Content()
{
 global $StrRes,$userinfo,$in_dlg,$strRegNewUserPasswordRetype,$strRegNewUserPassword,$message;
 if (!isset($in_dlg))
 {
    $query=new MyQuery("SELECT * FROM `user` LEFT JOIN `userinfo` ON `ID`=`bindUser` WHERE  `ID`='$userinfo[ID]'");
    $query->Next();
    $in_dlg=Array(
      "name"=> addSlashes($query->data["Name"]),
      "email"=> addSlashes($query->data["Email"]),
      "position"=> addSlashes($query->data["Position"]),
      "description"=> addSlashes($query->data["Description"]),
      "webPage"=> addSlashes($query->data["WebPage"]),
      "pageLen"=> addSlashes($query->data["pageLen"])
      );   
 }
  echo "
    <form method=\"post\" action=\"?page=setup\" enctype=\"multipart/form-data\" >
    <input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"65000\">
    <table class=\"dlg\">
    <caption class=\"dlg\">$StrRes[SetupPage]</caption>
    <tr>
    <th class=\"dlg\">$StrRes[YourName]</th>
    <td class=\"dlg\"><input type=\"text\" name=\"dlg[name]\" value=\"".HTMLChars($in_dlg["name"])."\" class=\"dlg\"></td>
    </tr>
    <tr>
    <th class=\"dlg\">$StrRes[YourEmail]</th>
    <td class=\"dlg\"><input type=\"text\" name=\"dlg[email]\" value=\"".HTMLChars($in_dlg["email"])."\" class=\"dlg\"></td>
    </tr>
    <tr>
    <th class=\"dlg\">$StrRes[YourPosition]</th>
    <td class=\"dlg\"><input type=\"text\" name=\"dlg[position]\" value=\"".HTMLChars($in_dlg["position"])."\" class=\"dlg\"></td>
    </tr>
    <tr>
    <th class=\"dlg\">$StrRes[YourDescription]</th>
    <td class=\"dlg\"><textarea name=\"dlg[description]\" rows=5 cols=40  class=\"dlg\">".HTMLChars($in_dlg["description"])."</textarea></td>
    </tr>
    <tr>
    <th class=\"dlg\">$StrRes[YourWebPage]</th>
    <td class=\"dlg\"><input type=\"text\" name=\"dlg[webPage]\" value=\"".HTMLChars($in_dlg["webPage"])."\" class=\"dlg\"></td>
    </tr>
    <tr>
    <th class=\"dlg\">$StrRes[YourImage]</th>
    <td class=\"dlg\"><input name=\"image\" type=\"file\" class=\"dlg\"></td>
    </tr>
    <tr>
    <th class=\"dlg\">$strRegNewUserPassword</th>
    <td class=\"dlg\"><input type=\"password\" name=\"dlg[pwd]\" value=\"********\" class=\"dlg\"></td>
    </tr>
    <tr>
    <th class=\"dlg\">$strRegNewUserPasswordRetype</th>
    <td class=\"dlg\"><input type=\"password\" name=\"dlg[pwd2]\" value=\"********\" class=\"dlg\"></td>
    </tr>
    <tr>
    <th class=\"dlg\">&nbsp;<span class=\"error\">$message</span></th>
    <td class=\"dlg\"><input type=\"submit\" value=\"$StrRes[Save]\" class=\"dlgbutton\"></td>
    </tr>
    </table>
    </form>
    <div align=\"center\"><a href=\"?page=showuser&amp;id=$userinfo[ID]\">$StrRes[ShowMyProfile]</a></div>
  ";
}
?>
