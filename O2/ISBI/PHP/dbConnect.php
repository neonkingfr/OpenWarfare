<?php
require_once "config/config.php";

$dbhandle=mysql_connect($cfg_dbserver,$cfg_dblogin,$cfg_dbpassword) or die(OnDatabaseError(__FILE__,__LINE__));
mysql_select_db($cfg_dbname) or die(OnDatabaseError("mysql_connect",__FILE__,__LINE__));

function OnExitDB()
{
  global $dbhandle;
  mysql_close($dbhandle);
}

register_shutdown_function("OnExitDB");

class MyQuery
{
private $resource;
public $row;
 
 
  function __construct($query,$file=__FILE__, $line=__LINE__)
  {
    $this->resource=mysql_query($query) or OnDatabaseError($query,$file,$line);
  }
  
  function __destruct() 
  {
    @mysql_free_result($this->resource);
  }
  
  function Next()
  {
    $row=mysql_fetch_assoc($this->resource);
    if ($row)
    {
      $this->data=$row;
      return true;
    }
    else
      return false;
  }
  
 
};

?>
