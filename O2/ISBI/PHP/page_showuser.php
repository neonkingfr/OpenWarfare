<?php
function BeforePageStart()
{
  global $in_id,$in_img;
  if (isset($in_img))
  {
    $query=new MyQuery("SELECT `Image` FROM `userinfo` WHERE  `bindUser`='$in_id'");
    $query->Next();   
    header('Content-type: image/jpeg') ;  
    echo $query->data["Image"];
    exit;
  }
}

function Content()
{
global $StrRes,$in_id;
$query=new MyQuery("SELECT Name,Email,Position,WebPage,Description,Login FROM `user` LEFT JOIN `userinfo` ON `ID`=`bindUser` WHERE  `ID`='$in_id'");
$query->Next();
$data=$query->data;
  echo "<table class=\"dlg\">
       <caption class=\"dlg\">$StrRes[ProfileUser]: $data[Login]</caption>
       <tr>
       <td>
       <img src=\"?page=showuser&amp;id=$in_id&amp;img=1\" alt=\"\" title=\"$data[Login]\" align=\"left\" vspace=20 hspace=20>
       <br><br>
       <table class=\"dlg\">
       <tr>
       <th class=\"dlg\">$StrRes[UserName]</th>
       <td class=\"dlg\">$data[Name]</td>
       </tr>
       <tr>
       <th class=\"dlg\">$StrRes[Email]</th>
       <td class=\"dlg\"><a href=\"mailto:$data[Email]\">$data[Email]</a></td>
       </tr>
       <tr>
       <th class=\"dlg\">$StrRes[Position]</th>
       <td class=\"dlg\">$data[Position]</td>
       </tr>
       <tr>
       <th class=\"dlg\">$StrRes[PersonalWebPage]</th>
       <td class=\"dlg\"><a href=\"$data[WebPage]\" target=\"_blank\">$data[WebPage]</a></td>
       </tr>
       </table>
       <h3>$StrRes[UserDescription]</h3>
       <p align=\"justify\">$data[Description]</p>
       </td>
       </tr>
       </table> 
  ";
}
?>
