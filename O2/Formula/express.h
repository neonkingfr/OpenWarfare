#ifndef expression_h
#define expression_h

#define NVar ( 'z'-'a'+1 )

typedef enum
{
  EvalOK,
  EvalGen, // generic error
  EvalExpo, // exponent out of range or invalid
  EvalNum, // invalid number
  EvalVar, // undefined variable
  EvalDivZero, // zero divisor
  EvalTg90, // tg 90
  EvalOpenB, // missing (
  EvalCloseB, // missing )
  EvalOper, // unknown operator
} EvalError;

const char *Evaluate( const char *source, EvalError &error, int index=NVar );

#endif
