#include <wpch.hpp>

#include <math.h>
#include <float.h>
#include <ctype.h>
#include <string.h>
#include <stdio.h>

#include "express.h"
#include <locale.h>


typedef WBool Flag;

#define False FALSE
#define True TRUE

#define lenof(arr) ( sizeof(arr)/sizeof(*arr) )

#define M_PI 3.1415926536

static int Ind; /* ktera promenna se pocita */
// muzeme pouzivat slozky pole vysledky od 0 do Ind-1
static const char *Pos,*Pos0;
static EvalError ErrF;

static void vynech( void )
{
  while( (*Pos==' ') ) Pos++;
}

#define NVyr NVar

static double Vysledky[NVyr];
static Flag Done[NVyr];

static void Chyba( EvalError error=EvalGen )
{
  // chyba na pozici Pos
  ErrF=error;
}

static double sejmid( void )
{
  const char *Po1;
  const char *Po2;
  const struct lconv *Tecka;
  double cisl;
  int i, Fg;
  
  Tecka=localeconv();
  Po1=Pos;
  cisl=0;
  Fg=0;
  while( *Pos>='0' && *Pos<='9' )
  {
    Fg=1;
    cisl*=10;
    cisl+= (double) *Pos-'0';
    Pos++;
  }
  if( *Pos=='.' || *Pos==',' || *Pos==*Tecka->decimal_point )
  {
    i=0;
    Pos++;
    while( *Pos>='0' && *Pos<='9' )
    {
      Fg=1;
      i++;
      cisl+=(*Pos - '0')/pow(10,i);
      Pos++;
    }
  }
  if( Fg && (*Pos=='e' || *Pos=='E') )
  {
    int c=0, zn;
    
    Pos++;
    zn=1;
    switch (*Pos)
    {
      case '-':
      zn=-1;
      Pos++;
      break;
      case '+':
      Pos++;
      break;
    }
    Po2=Pos;
    while( *Pos>='0' && *Pos<='9' )
    {
      c*=10;
      c+= *Pos-'0';
      Pos++;
    }
    if( Po2==Pos )
    {
      Chyba(EvalExpo);
      return 0;
    }
    if( c>DBL_MAX_10_EXP || c<DBL_MIN_10_EXP)
    {
      Chyba(EvalExpo);
      return 0;
    }
    cisl=cisl*pow(10,c*zn);
  }
  if( Po1==Pos ) Chyba(EvalNum);
  return cisl;
}

static double Const( void )
{
  double vrt;
  vynech();
  if( isalpha(Pos[0]) && !isalpha(Pos[1])) /* promenna */
  {
    char l=toupper(Pos[0]);
    int v=l-'A';
    if( v<Ind && Done[v] )
    {
      Pos++;
      return Vysledky[v];
    }
    Chyba(EvalVar);
    vrt=0;
  }
  else if( Pos[0]=='$' ) /* hex */
  {
    Flag Fg=False;
    vrt=0;
    Pos++;
    while( isxdigit(*Pos) )
    {
      Fg=True;
      vrt*=16;
      if( isdigit(*Pos) ) vrt+=*Pos-'0';
      else vrt+=toupper(*Pos)-('A'-10);
      Pos++;
    }
    if( !Fg ) Chyba(EvalNum);
    return vrt;
  }
  else vrt=sejmid(); 
  return vrt;
}

#define SL 64

typedef double (* ProcO2) (double O1, double O2);
typedef double (* ProcO1) (double O1);

static double Soucet( double Oper1, double Oper2 )
{
  return Oper1+Oper2;
}

static double Rozdil( double Oper1, double Oper2 )
{
  return Oper1-Oper2;
}

static double Soucin( double Oper1, double Oper2 )
{
  return Oper1*Oper2;
}

static double Podil( double Oper1, double Oper2 )
{
  if( Oper2==0 )
  {
    Chyba(EvalDivZero);
    return 0;
  }
  return Oper1/Oper2;
}

static double Zbytek( double Oper1, double Oper2 )
{
  if( Oper2==0 )
  {
    Chyba(EvalDivZero);
    return 0;
  }
  return fmod(Oper1,Oper2);
}

static double Na( double Oper1, double Oper2 )
{
  return pow(Oper1, Oper2);
}

typedef struct
{
  char *Op;
  int Prio;
  ProcO2 proc;
} Oper2;

enum 
{ nula, soucet, soucin, unar, mocnina, zavorky };

static Oper2 O2[]=
{
  { "^", mocnina, Na},
  { "*", soucin, Soucin},
  { "/", soucin, Podil},
  { "%", soucin, Zbytek},
  { "mod", soucin, Zbytek},
  { "+", soucet, Soucet},
  { "-", soucet, Rozdil}
};

static double Plus( double Operator1 )
{
  return Operator1;
}

static double Minus( double Operator1 )
{
  return -Operator1;
}

static double Stupne( double Operator1 )
{
  return 2*M_PI*Operator1/360;
}

static double Radian( double Operator1 )
{
  return Operator1*360/(2*M_PI);
}

static double Sinus( double Operator1 )
{
  if( fmod(Operator1, 180)==0.0 ) return 0;
  return sin(Stupne(Operator1));
}

static double Cosinus( double Operator1 )
{
  if( fabs(fmod(Operator1, 180))==90.0 ) return 0;
  return cos(Stupne(Operator1));
}

static double Tangens( double Operator1 )
{
  double zbtk;
  
  if ( Operator1==0 ) return 0;
  zbtk=fmod(Operator1, 180);
  if( zbtk==45 ) return 1;
  if( zbtk==90 )
  {
    Chyba(EvalTg90);
    return 0;
  }
  return tan(Stupne(Operator1));
}

static double ASinus( double Operator1 )
{
  return Radian(asin(Operator1));
}

static double ACosinus( double Operator1 )
{
  return Radian(acos(Operator1));
}

static double ATangens( double Operator1 )
{
  return Radian(atan(Operator1));
}

static double Log( double Operator1 )
{
  return log10(Operator1);
}

static double Ln( double Operator1 )
{
  return log(Operator1);
}

static double Exp( double Operator1 )
{
  return exp(Operator1);
}

static double Odmocnina( double Operator1 )
{
  return sqrt(Operator1);
}

typedef struct
{
  char *Op;
  ProcO1 proc;
} Oper1;

static Oper1 O1[]=
{
  { "sin", Sinus },
  { "cos", Cosinus },
  { "tg", Tangens },
  { "tan", Tangens },
  { "asin", ASinus },
  { "acos", ACosinus },
  { "atg", ATangens },
  { "atan", ATangens },
  { "deg", Radian },
  { "rad", Stupne },
  { "log", Log },
  { "ln", Ln },
  { "exp", Exp },
  { "sqrt", Odmocnina },
  { "+", Plus},
  { "-", Minus}
};

static double S[SL];
static int UB[SL];
static int Pr[SL];
static int SOp[SL];
static int SP, PP;

static void VyhCast( int Prio )
{
  while( (SP>0) && (Prio<=Pr[SP-1])  )
  {
    if( UB[SP-1]==1 ) S[SP-1]=O1[SOp[SP-1]].proc( S[SP] );
    else S[SP-1]=O2[SOp[SP-1]].proc( S[SP-1], S[SP] );
    SP--;
    if( ErrF ) return;
  }
}

static int CMP( const char *Ps, const char *Ps1 )
{
  return strncmp(Ps,Ps1,strlen(Ps1));
}

double Vyhod( void )
{
  int Op1,Op2;
  PP=0;
  SP=0;
  ErrF=EvalOK;
  for(;;)
  {
    Oper1 *Uk1;
    Oper2 *Uk2;
    vynech();
    if( *Pos=='(' )
    {
      Pos++;
      PP+=zavorky;
      continue;
    }
    for( Uk1=O1, Op1=0; Op1<(int) lenof(O1) && CMP(Pos,Uk1->Op); Op1++, Uk1++ );
    if( Op1==(int)lenof(O1) )
    {
      S[SP]=Const();
      if( ErrF ) return 0;
      CekejBinar:
      vynech();
      switch( *Pos )
      {
        case ')':
        Pos++;
        PP-=zavorky;
        if( PP<0 )
        {
          Chyba(EvalOpenB);
          return 0;
        }
        goto CekejBinar;
        case 0:
        if( PP!=0 )
        {
          Chyba(EvalCloseB);
          return 0;
        } 
        VyhCast( 0 );
        return S[SP]; 
        default:
        for( Uk2=O2, Op2=0; Op2<(int) lenof(O2) && CMP(Pos,Uk2->Op); Op2++, Uk2++ );
        if( Op2==(int) lenof(O2) )
        {
          Chyba(EvalOper);
          return 0;
        }   
        Pos+=strlen(Uk2->Op);
        VyhCast( Uk2->Prio+PP );
        if( ErrF ) return 0;
        SOp[SP]=Op2;
        UB[SP]=2;
        Pr[SP]=O2[Op2].Prio+PP;
        SP++;
        break;
      } /* switch */
    }/*binar atp.*/
    else /*un�rn� oper�tor*/
    {
      Pos+=strlen(Uk1->Op);
      SOp[SP]=Op1;
      UB[SP]=1;
      Pr[SP]=unar+PP;
      SP++;
    } /*uloz unar*/
  } /*while*/
}

const char *Evaluate( const char *source, EvalError &error, int index )
{
  static char Buf[256];
  ErrF=EvalOK;
  Pos0=Pos=source;
  Ind=index;
  double result=Vyhod();
  if( Ind<NVar )
  {
    Vysledky[Ind]=result;
    Done[Ind]=true;
  }
  if( ErrF )
  {
    char *b;
    const char *p;
    for( p=Pos0,b=Buf; p<Pos; p++,b++ )
    {
      *b=*p;
    }
    *b=0;
  }
  else sprintf(Buf,"%.11g",result);
  error=ErrF;
  return Buf;
}

