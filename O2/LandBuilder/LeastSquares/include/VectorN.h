#ifndef VECTORN_H
#define VECTORN_H

#include <assert.h>

#include "..\..\HighMapLoaders\include\EtMath.h"

template <class Real>
class VectorN
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// -------------------- //
	// the vector dimension //
	// -------------------- //
    unsigned int m_DimensionsCount;

	// ------------------- //
	// the vector elements //
	// ------------------- //
	Real* m_Elements;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //
	VectorN(unsigned int dimensions = 0);
	VectorN(unsigned int dimensions, const Real* elements);

	VectorN(const VectorN<Real>& other);

	// ********** //
	// Destructor //
	// ********** //
    ~VectorN();

	// ********* //
	// Interface //
	// ********* //

    // assignment
    VectorN<Real>& operator = (const VectorN<Real>& other);

    // comparison
    bool operator == (const VectorN<Real>& other) const;
    bool operator != (const VectorN<Real>& other) const;

    // arithmetic operations
    VectorN<Real> operator + (const VectorN<Real>& other) const;
    VectorN<Real> operator - (const VectorN<Real>& other) const;
    Real          operator * (const VectorN<Real>& other) const;
    VectorN<Real> operator * (Real scalar) const;
    VectorN<Real> operator / (Real scalar) const;

	VectorN<Real> operator - () const;

    // arithmetic updates
    VectorN<Real>& operator += (const VectorN<Real>& other);
    VectorN<Real>& operator -= (const VectorN<Real>& other);
    VectorN<Real>& operator *= (Real scalar);
    VectorN<Real>& operator /= (Real scalar);

    operator const Real* () const;
    operator Real* ();

	Real GetAt(unsigned int index) const;
	void SetAt(unsigned int index, Real value);

	void SetDimensions(unsigned int dimensions);
    void SetVector(unsigned int dimensions, const Real* elements);

	unsigned int GetDimensions() const;

    Real Length() const;
    Real SquaredLength() const;
    Real Dot(const VectorN<Real>& other) const;
    VectorN<Real> Normalize() const;
    VectorN<Real>& NormalizeInPlace();

	// ************************************************************** //
	// Helper functions                                               //
	// ************************************************************** //
protected:	
    void Deallocate();

    // support for comparisons
    int CompareArrays(const VectorN<Real>& other) const;
};

template <class Real>
VectorN<Real> operator * (Real scalar, const VectorN<Real>& v);

#include "..\src\VectorN.inl"

#endif
