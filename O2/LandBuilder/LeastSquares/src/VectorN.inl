// ************************************************************************** //
// Constructors                                                               //
// ************************************************************************** //

template <class Real>
VectorN<Real>::VectorN(unsigned int dimensions)
{
	m_Elements = NULL;
	SetDimensions(dimensions);
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real>::VectorN(unsigned int dimensions, const Real* elements)
{
	m_Elements = NULL;
	SetVector(dimensions, elements);
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real>::VectorN(const VectorN<Real>& other)
{
    m_DimensionsCount = other.m_DimensionsCount;
    if (m_DimensionsCount > 0)
    {
        m_Elements = new Real[m_DimensionsCount];
        size_t size = m_DimensionsCount * sizeof(Real);
		// WARNING - TO DO -
		// no check here
        memcpy_s(m_Elements, size, other.m_Elements, size);
    }
    else
    {
        m_Elements = NULL;
    }
}

// ************************************************************************** //
// Destructor                                                                 //
// ************************************************************************** //

template <class Real>
VectorN<Real>::~VectorN()
{
	Deallocate();
}

// ************************************************************************** //
// Interface                                                                  //
// ************************************************************************** //

template <class Real>
VectorN<Real>& VectorN<Real>::operator = (const VectorN<Real>& other)
{
    if (other.m_DimensionsCount > 0)
    {
        if (m_DimensionsCount != other.m_DimensionsCount)
        {
            Deallocate();

            m_DimensionsCount = other.m_DimensionsCount;
            m_Elements        = new Real[m_DimensionsCount];
        }

		size_t size = m_DimensionsCount * sizeof(Real);
		// WARNING - TO DO -
		// no check here
        memcpy_s(m_Elements, size, other.m_Elements, size);
    }
    else
    {
        Deallocate();
        m_DimensionsCount = 0;
        m_Elements        = NULL;
    }
    return *this;
}

// -------------------------------------------------------------------------- //

template <class Real>
bool VectorN<Real>::operator == (const VectorN<Real>& other) const
{
    return CompareElements(other) == 0;
}

// -------------------------------------------------------------------------- //

template <class Real>
bool VectorN<Real>::operator != (const VectorN<Real>& other) const
{
    return CompareElements(other) != 0;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real> VectorN<Real>::operator + (const VectorN<Real>& other) const
{
    VectorN<Real> v(m_DimensionsCount);

	if(m_DimensionsCount == other.m_DimensionsCount)
	{
		for(unsigned int i = 0; i < m_DimensionsCount; ++i)
		{
			v.m_Elements[i] = m_Elements[i] + other.m_Elements[i];
		}
	}
    return v;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real> VectorN<Real>::operator - (const VectorN<Real>& other) const
{
    VectorN<Real> v(m_DimensionsCount);

	if(m_DimensionsCount == other.m_DimensionsCount)
	{
		for(unsigned int i = 0; i < m_DimensionsCount; ++i)
		{
			v.m_Elements[i] = m_Elements[i] - other.m_Elements[i];
		}
	}
    return v;
}

// -------------------------------------------------------------------------- //

template <class Real>
Real VectorN<Real>::operator * (const VectorN<Real>& other) const
{
	return Dot(other);
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real> VectorN<Real>::operator * (Real scalar) const
{
    VectorN<Real> v(m_DimensionsCount);

	for(unsigned int i = 0; i < m_DimensionsCount; ++i)
	{
		v.m_Elements[i] = scalar * m_Elements[i];
	}
    return v;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real> VectorN<Real>::operator / (Real scalar) const
{
    VectorN<Real> v(m_DimensionsCount);

    if (scalar != static_cast<Real>(0.0))
    {
        Real invScalar = (static_cast<Real>(1.0)) / scalar;
        for(unsigned int i = 0; i < m_DimensionsCount; ++i)
        {
            v.m_Elements[i] = invScalar * m_Elements[i];
        }
    }
    else
    {
        for(unsigned int i = 0; i < m_DimensionsCount; ++i)
        {
            v.m_Elements[i] = EtMath<Real>::MAX_REAL;
        }
    }
    return v;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real> VectorN<Real>::operator - () const
{
    VectorN<Real> v(m_DimensionsCount);

	for(unsigned int i = 0; i < m_DimensionsCount; ++i)
	{
		v.m_Elements[i] = -m_Elements[i];
	}
    return v;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real>& VectorN<Real>::operator += (const VectorN<Real>& other)
{
	if(m_DimensionsCount == other.m_DimensionsCount)
	{
		for(unsigned int i = 0; i < m_DimensionsCount; ++i)
		{
			m_Elements[i] += other.m_Elements[i];
		}
	}
    return *this;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real>& VectorN<Real>::operator -= (const VectorN<Real>& other)
{
	if(m_DimensionsCount == other.m_DimensionsCount)
	{
		for(unsigned int i = 0; i < m_DimensionsCount; ++i)
		{
			m_Elements[i] -= other.m_Elements[i];
		}
	}
    return *this;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real>& VectorN<Real>::operator *= (Real scalar)
{
	for(unsigned int i = 0; i < m_DimensionsCount; ++i)
	{
		m_Elements[i] -= scalar;
	}
    return *this;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real>& VectorN<Real>::operator /= (Real scalar)
{
    if (scalar != static_cast<Real>(0.0))
    {
        Real invScalar = (static_cast<Real>(1.0)) / scalar;
        for(unsigned int i = 0; i < m_DimensionsCount; ++i)
        {
            m_Elements[i] *= invScalar;
        }
    }
    else
    {
        for(unsigned int i = 0; i < m_DimensionsCount; ++i)
        {
            m_Elements[i] = EtMath<Real>::MAX_REAL;
        }
    }
    return *this;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real>::operator const Real* () const
{
    return m_Elements;
}

//----------------------------------------------------------------------------

template <class Real>
VectorN<Real>::operator Real* ()
{
    return m_Elements;
}

//----------------------------------------------------------------------------

template <class Real>
Real VectorN<Real>::GetAt(unsigned int index) const
{
	if(index <= m_DimensionsCount)
	{
		return m_Elements[index];
	}
	else
	{
		return EtMath<Real>::MAX_REAL;
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
void VectorN<Real>::SetAt(unsigned int index, Real value)
{
	if(index <= m_DimensionsCount)
	{
		m_Elements[index] = value;
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
void VectorN<Real>::SetDimensions(unsigned int dimensions)
{
	Deallocate();

	if (dimensions > 0)
    {
        m_DimensionsCount = dimensions;
        m_Elements        = new Real[dimensions];

        memset(m_Elements, 0, dimensions * sizeof(Real));
    }
    else
    {
        m_DimensionsCount = 0;
        m_Elements        = NULL;
    }
}

// -------------------------------------------------------------------------- //

template <class Real>
void VectorN<Real>::SetVector(unsigned int dimensions, const Real* elements)
{
	Deallocate();

    if(dimensions > 0)
    {
        m_DimensionsCount = dimensions;
        m_Elements        = new Real[dimensions];

        size_t size = dimensions * sizeof(Real);
		// WARNING - TO DO -
		// no check here
        memcpy_s(m_Elements, size, elements, size);
    }
    else
    {
        m_DimensionsCount = 0;
        m_Elements        = NULL;
    }
}

// -------------------------------------------------------------------------- //

template <class Real>
unsigned int VectorN<Real>::GetDimensions() const
{
	return m_DimensionsCount;
}

// -------------------------------------------------------------------------- //

template <class Real>
Real VectorN<Real>::Length() const
{
	return EtMath<Real>::Sqrt(SquaredLength());
}

// -------------------------------------------------------------------------- //

template <class Real>
Real VectorN<Real>::SquaredLength() const
{
    Real sqLen = static_cast<Real>(0.0);
    for(unsigned int i = 0; i < m_DimensionsCount; ++i)
    {
        sqLen += m_Elements[i] * m_Elements[i];
    }
    return sqLen;
}

// -------------------------------------------------------------------------- //

template <class Real>
Real VectorN<Real>::Dot(const VectorN<Real>& other) const
{
    Real dot = static_cast<Real>(0.0);
    for(unsigned int i = 0; i < m_DimensionsCount; ++i)
    {
        dot += m_Elements[i] * other.m_Elements[i];
    }
    return dot;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real> VectorN<Real>::Normalize() const
{
    VectorN<Real> v(m_DimensionsCount);

	Real len = Length();

    if(len > EtMath<Real>::ZERO_TOLERANCE)
    {
        Real invLen = (static_cast<Real>(1.0)) / len;
        for(unsigned int i = 0; i < m_DimensionsCount; ++i)
        {
            v.m_Elements[i] = m_Elements[i] * invLen;
        }
    }
    else
    {
        for(unsigned int i = 0; i < m_DimensionsCount; ++i)
        {
            v.m_Elements[i] = static_cast<Real>(0.0);
        }
    }

    return v;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real>& VectorN<Real>::NormalizeInPlace()
{
	Real len = Length();

    if(len > EtMath<Real>::ZERO_TOLERANCE)
    {
        Real invLen = (static_cast<Real>(1.0)) / len;
        for(unsigned int i = 0; i < m_DimensionsCount; ++i)
        {
            m_Elements[i] *= invLen;
        }
    }
    else
    {
        for(unsigned int i = 0; i < m_DimensionsCount; ++i)
        {
            m_Elements[i] = static_cast<Real>(0.0);
        }
    }

    return *this;
}

// ************************************************************************** //
// Helper functions                                                           //
// ************************************************************************** //

template <class Real>
void VectorN<Real>::Deallocate()
{
	if(m_Elements) 
	{
		delete[] m_Elements;
		m_Elements = NULL;
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
int VectorN<Real>::CompareArrays(const VectorN<Real>& other) const
{
    return memcmp(m_Elements, other.m_Elements, m_DimensionsCount * sizeof(Real));
}

// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real> operator * (Real scalar, const VectorN<Real>& v)
{
	unsigned int dimensionsCount = v.GetDimensions();

	VectorN<Real> vOut(dimensionsCount);

    for(unsigned int i = 0; i < dimensionsCount; ++i)
    {
        vOut[i] = scalar * v[i];
    }
    return vOut;
}
