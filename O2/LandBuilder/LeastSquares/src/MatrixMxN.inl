// ************************************************************************** //
// Constructors                                                               //
// ************************************************************************** //

template <class Real>
MatrixMxN<Real>::MatrixMxN(unsigned int rowsCount, unsigned int columnsCount)
{
	m_Elements = 0;
	m_Rows     = 0;
	SetSize(rowsCount, columnsCount);
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real>::MatrixMxN(unsigned int rowsCount, unsigned int columnsCount, const Real* elements)
{
	m_Elements = 0;
	m_Rows     = 0;
	SetMatrix(rowsCount, columnsCount, elements);
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real>::MatrixMxN(unsigned int rowsCount, unsigned int columnsCount, const Real** rows)
{
	m_Elements = 0;
	m_Rows     = 0;
	SetMatrix(rowsCount, columnsCount, rows);
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real>::MatrixMxN(const MatrixMxN<Real>& other)
{
    m_RowsCount     = 0;
    m_ColumnsCount  = 0;
    m_ElementsCount = 0;
    m_Elements      = 0;
    m_Rows          = 0;

    *this = other;
}

// ************************************************************************** //
// Destructor                                                                 //
// ************************************************************************** //

template <class Real>
MatrixMxN<Real>::~MatrixMxN()
{
	Deallocate();
}

// ************************************************************************** //
// Interface                                                                  //
// ************************************************************************** //

template <class Real>
MatrixMxN<Real>& MatrixMxN<Real>::operator = (const MatrixMxN<Real>& other)
{
    if(other.m_ElementsCount > 0)
    {
        if(m_RowsCount != other.m_RowsCount || m_ColumnsCount != other.m_ColumnsCount)
        {
            Deallocate();

            m_RowsCount     = other.m_RowsCount;
            m_ColumnsCount  = other.m_ColumnsCount;
            m_ElementsCount = other.m_ElementsCount;

			Allocate(false);
		}

        for(unsigned int row = 0; row < m_RowsCount; ++row)
        {
            for(unsigned int col = 0; col < m_ColumnsCount; ++col)
            {
                m_Rows[row][col] = other.m_Rows[row][col];
            }
        }
    }
    else
    {
        Deallocate();
        m_RowsCount     = 0;
        m_ColumnsCount  = 0;
        m_ElementsCount = 0;
        m_Elements      = 0;
        m_Rows          = 0;
    }

    return *this;
}

// -------------------------------------------------------------------------- //

template <class Real>
bool MatrixMxN<Real>::operator == (const MatrixMxN<Real>& other) const
{
    return CompareElements(other) == 0;
}

// -------------------------------------------------------------------------- //

template <class Real>
bool MatrixMxN<Real>::operator != (const MatrixMxN<Real>& other) const
{
    return CompareElements(other) != 0;
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real> MatrixMxN<Real>::operator + (const MatrixMxN<Real>& other) const
{
    MatrixMxN<Real> m(m_RowsCount, m_ColumnsCount);

    if(m_RowsCount == other.m_RowsCount && m_ColumnsCount == other.m_ColumnsCount)
	{
		for(unsigned int i = 0; i < m_ElementCount; ++i)
		{
			m.m_Elements[i] = m_Elements[i] + other.m_Elements[i];
		}
	}
    return m;
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real> MatrixMxN<Real>::operator - (const MatrixMxN<Real>& other) const
{
    MatrixMxN<Real> m(m_RowsCount, m_ColumnsCount);

    if(m_RowsCount == other.m_RowsCount && m_ColumnsCount == other.m_ColumnsCount)
	{
		for(unsigned int i = 0; i < m_ElementCount; ++i)
		{
			m.m_Elements[i] = m_Elements[i] - other.m_Elements[i];
		}
	}
    return m;
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real> MatrixMxN<Real>::operator * (const MatrixMxN<Real>& other) const
{    
	MatrixMxN<Real> m(m_RowsCount, other.m_ColumnsCount);

    if(m_ColumnsCount == other.m_RowsCount)
	{
		for(unsigned int row = 0; row < m.m_RowsCount; ++row)
		{
			for(unsigned int col = 0; col < m.m_ColumnsCount; ++col)
			{
				for(unsigned int i = 0; i < m_ColumnsCount; ++i)
				{
					m.m_Rows[row][col] += m_Rows[row][i] * other.m_Rows[i][col];
				}
			}
		}
	}
    return m;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real> MatrixMxN<Real>::operator * (const VectorN<Real>& v) const
{
    VectorN<Real> vOut(m_RowsCount);

	if(v.GetDimensions() == m_ColumnsCount)
	{
		for(unsigned int row = 0; row < m_RowsCount; ++row)
		{
			for (unsigned int col = 0; col < m_ColumnsCount; ++col)
			{
				vOut.SetAt(row, vOut.GetAt(row)+ m_Rows[row][col] * v.GetAt(col));
			}        
		}
	}
    return vOut;
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real> MatrixMxN<Real>::operator * (Real scalar) const
{
    MatrixMxN<Real> m(m_RowsCount, m_ColumnsCount);

    for(unsigned int i = 0; i < m_ElementsCount; ++i)
    {
        m.m_Elements[i] = scalar * m_Elements[i];
    }
    return m;
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real> MatrixMxN<Real>::operator / (Real scalar) const
{
    MatrixMxN<Real> m(m_RowsCount, m_ColumnsCount);

    if (scalar != static_cast<Real>(0.0))
    {
        Real invScalar = (static_cast<Real>(1.0)) / scalar;
        for(unsigned int i = 0; i < m_ElementsCount; ++i)
        {
            m.m_Elements[i] = invScalar * m_Elements[i];
        }
    }
    else
    {
        for(unsigned int i = 0; i < m_ElementsCount; ++i)
        {
            m.m_Elements[i] = EtMath<Real>::MAX_REAL;
        }
    }
    return m;
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real> MatrixMxN<Real>::operator - () const
{
    MatrixMxN<Real> m(m_RowsCount, m_ColumnsCount);

    for(unsigned int i = 0; i < m_ElementsCount; ++i)
    {
        m.m_Elements[i] = -m_Elements[i];
    }
    return m;
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real>& MatrixMxN<Real>::operator += (const MatrixMxN<Real>& other)
{
    if(m_RowsCount == other.m_RowsCount && m_ColumnsCount == other.m_ColumnsCount)
	{
		for(unsigned int i = 0; i < m_ElementsCount; ++i)
		{
			m_Elements[i] += other.m_Elements[i];
		}
	}
    return *this;
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real>& MatrixMxN<Real>::operator -= (const MatrixMxN<Real>& other)
{
    if(m_RowsCount == other.m_RowsCount && m_ColumnsCount == other.m_ColumnsCount)
	{
		for(unsigned int i = 0; i < m_ElementsCount; ++i)
		{
			m_Elements[i] -= other.m_Elements[i];
		}
	}
    return *this;
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real>& MatrixMxN<Real>::operator *= (Real scalar)
{
	for(unsigned int i = 0; i < m_ElementsCount; ++i)
	{
		m_Elements[i] *= scalar;
	}
    return *this;
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real>& MatrixMxN<Real>::operator /= (Real scalar)
{
    if (scalar != static_cast<Real>(0.0))
    {
        Real invScalar = (static_cast<Real>(1.0)) / scalar;
        for(unsigned int i = 0; i < m_ElementsCount; ++i)
        {
            m_Elements[i] *= invScalar;
        }
    }
    else
    {
        for(unsigned int i = 0; i < m_ElementsCount; ++i)
        {
            m_Elements[i] = EtMath<Real>::MAX_REAL;
        }
    }
    return *this;
}

// -------------------------------------------------------------------------- //

template <class Real>
unsigned int MatrixMxN<Real>::GetRowsCount() const
{
	return m_RowsCount;
}

// -------------------------------------------------------------------------- //

template <class Real>
unsigned int MatrixMxN<Real>::GetColumnsCount() const
{
	return m_ColumnsCount;
}

// -------------------------------------------------------------------------- //

template <class Real>
unsigned int MatrixMxN<Real>::GetElementsCount() const
{
	return m_ElementsCount;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real> MatrixMxN<Real>::GetRow(unsigned int row) const
{
    VectorN<Real> v(m_ColumnsCount);

	if(0 <= row && row < m_RowsCount)
	{
		for(unsigned int col = 0; col < m_ColumnsCount; ++col)
		{
			v[col] = m_Rows[row][col];
		}
	}

    return v;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real> MatrixMxN<Real>::GetColumn(unsigned int column) const
{
    VectorN<Real> v(m_RowsCount);

	if(0 <= column && column < m_ColumnsCount)
	{
		for(unsigned int row = 0; row < m_RowsCount; ++row)
		{
			v[row] = m_Rows[row][col];
		}
	}

    return v;
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real>::operator const Real* () const
{
	return m_Elements;
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real>::operator Real* ()
{
	return m_Elements;
}

// -------------------------------------------------------------------------- //

template <class Real>
const Real* MatrixMxN<Real>::operator [] (unsigned int row) const
{
    if(0 <= row && row < m_RowsCount)
	{
	    return m_Rows[row];
	}
	else
	{
		return NULL;
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
Real* MatrixMxN<Real>::operator [] (unsigned int row)
{
    if(0 <= row && row < m_RowsCount)
	{
	    return m_Rows[row];
	}
	else
	{
		return NULL;
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
Real MatrixMxN<Real>::operator () (unsigned int row, unsigned int column) const
{
    if(0 <= row && row < m_RowsCount && 0 <= column && column < m_ColumnsCount)
	{
	    return m_Rows[row][column];
	}
	else
	{
		return EtMath<Real>::MAX_REAL;
	}
}

// -------------------------------------------------------------------------- //
/*
template <class Real>
Real& MatrixMxN<Real>::operator() (unsigned int row, unsigned int column)
{
    if(0 <= row && row < m_RowsCount && 0 <= column && column < m_ColumnsCount)
	{
	    return m_Rows[row][column];
	}
	else
	{
		return EtMath<Real>::MAX_REAL;
	}
}
*/
// -------------------------------------------------------------------------- //

template <class Real>
void MatrixMxN<Real>::SetSize(unsigned int rowsCount, unsigned int columnsCount)
{
	Deallocate();

	if(rowsCount > 0 && columnsCount > 0)
	{
		m_RowsCount     = rowsCount;
		m_ColumnsCount  = columnsCount;
		m_ElementsCount = rowsCount * columnsCount;

		Allocate(true);
	}
	else
	{
		m_RowsCount     = 0;
		m_ColumnsCount  = 0;
		m_ElementsCount = 0;
		m_Elements      = 0;
		m_Rows          = 0;
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
void MatrixMxN<Real>::SetMatrix(unsigned int rowsCount, unsigned int columnsCount, const Real* elements)
{
    Deallocate();

    if (rowsCount > 0 && columnsCount > 0)
    {
        m_RowsCount     = rowsCount;
        m_ColumnsCount  = columnsCount;
        m_ElementsCount = rowsCount * columnsCount;

        Allocate(false);

        size_t size = m_ElementsCount * sizeof(Real);
		// WARNING - TO DO -
		// no check here
        memcpy_s(m_Elements, size, elements, size);

		// NB the link to m_RowFirst is done inside Allocate()
    }
    else
    {
        m_RowsCount     = 0;
        m_ColumnsCount  = 0;
        m_ElementsCount = 0;
        m_Elements      = 0;
        m_Rows          = 0;
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
void MatrixMxN<Real>::SetMatrix(unsigned int rowsCount, unsigned int columnsCount, const Real** rows)
{
    Deallocate();

    if (rowsCount > 0 && columnsCount > 0)
    {
        m_RowsCount     = rowsCount;
        m_ColumnsCount  = columnsCount;
        m_ElementsCount = rowsCount * columnsCount;

        Allocate(false);

        for(unsigned int row = 0; row < m_RowsCount; ++row)
        {
            for(unsigned int col = 0; col < m_ColumnsCount; ++col)
            {
                m_Rows[row][col] = rows[row][col];
            }
        }
    }
    else
    {
        m_RowsCount     = 0;
        m_ColumnsCount  = 0;
        m_ElementsCount = 0;
        m_Elements      = 0;
        m_Rows          = 0;
    }
}

// -------------------------------------------------------------------------- //

template <class Real>
Real MatrixMxN<Real>::GetAt(unsigned int row, unsigned int column) const
{
	if((0 <= row && row < m_RowsCount) && (0 <= column && column < m_ColumnsCount))
	{
		return m_Rows[row][column];
	}
	else
	{
		return EtMath<Real>::MAX_REAL;
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
void MatrixMxN<Real>::SetAt(unsigned int row, unsigned int column, Real value)
{
	if((0 <= row && row < m_RowsCount) && (0 <= column && column < m_ColumnsCount))
	{
		m_Rows[row][column] = value;
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
void MatrixMxN<Real>::SetRow(unsigned int row, const VectorN<Real>& v)
{
	if((0 <= row && row < m_RowsCount) && (v.GetDimensions() == m_ColumnsCount))
	{
		for(unsigned int col = 0; col < m_ColumnsCount; ++col)
		{
			m_Rows[row][col] = v[col];
		}
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
void MatrixMxN<Real>::SetColumn(unsigned int column, const VectorN<Real>& v)
{
	if((0 <= column && column < m_ColumnsCount) && (v.GetDimensions() == m_RowsCount))
	{
		for(unsigned int row = 0; row < m_RowsCount; ++row)
		{
			m_Rows[row][column] = v[row];
		}
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
void MatrixMxN<Real>::SwapRows(unsigned int row1, unsigned int row2)
{
    if(0 <= row1 && row1 < m_RowsCount && 0 <= row2 && row2 < m_RowsCount)
	{
		Real* temp = m_Rows[row1];

		m_Rows[row1] = m_Rows[row2];
		m_Rows[row2] = temp;
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
bool MatrixMxN<Real>::IsSquare() const
{
	return (m_RowsCount == m_ColumnsCount);
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real> MatrixMxN<Real>::Transpose() const
{
    MatrixMxN<Real> m(m_ColumnsCount, m_RowsCount);

	for(unsigned int row = 0; row < m_RowsCount; ++row)
    {
        for(unsigned int col = 0; col < m_ColumnsCount; ++col)
        {
            m.m_Rows[col][row] = m_Rows[row][col];
        }
    }
    return m;
}

// -------------------------------------------------------------------------- //

template <class Real>
bool MatrixMxN<Real>::Inverse(MatrixMxN<Real>& out) const
{
	if(m_RowsCount == 0 || m_ColumnsCount == 0) return false;
	if(m_RowsCount != m_ColumnsCount) return false;

	out = *this;
	unsigned int size = out.m_RowsCount;

	int* colIndex = new int[size];
	int* rowIndex = new int[size];
	bool* pivoted = new bool[size];
	memset(pivoted, 0, size * sizeof(bool));

	unsigned int row = 0;
	unsigned int col = 0;

	for(unsigned int i0 = 0; i0 < size; ++i0)
	{
		Real max = static_cast<Real>(0.0);
		for(unsigned int i1 = 0; i1 < size; ++i1)
		{
			if(!pivoted[i1])
			{
				for(unsigned int i2 = 0; i2 < size; ++i2)
				{
					if(!pivoted[i2])
					{
						Real a = EtMath<Real>::Fabs(out[i1][i2]);
						if(a > max)
						{
							max = a;
							row = i1;
							col = i2;
						}
					}
				}
			}
		}

		if(max == static_cast<Real>(0.0))
		{
			delete[] colIndex;
			delete[] rowIndex;
			delete[] pivoted;
			return false;
		}

		pivoted[col] = true;

		if(row != col)
		{
			out.SwapRows(row, col);
		}

		rowIndex[i0] = row;
		colIndex[i0] = col;

		Real inv = static_cast<Real>(1.0) / out[col][col];
		out[col][col] = static_cast<Real>(1.0);

		for(unsigned int i1 = 0; i1 < size; ++i1)
		{
			out[col][i1] *= inv;
		}

		for(unsigned int i1 = 0; i1 < size; ++i1)
		{
			if(i1 != col)
			{
				Real temp = out[i1][col];
				out[i1][col] = static_cast<Real>(0.0);
				for(unsigned int i2 = 0; i2 < size; ++i2)
				{
					out[i1][i2] -= out[col][i2] * temp;
				}
			}
		}
	}

	// here using int to avoid problems with underflows
	for(int i0 = size - 1; i0 >= 0; --i0)
	{
		if(rowIndex[i0] != colIndex[i0])
		{
			for(unsigned int i1 = 0; i1 < size; ++i1)
			{
				Real temp = out[i1][rowIndex[i0]];
				out[i1][rowIndex[i0]] = out[i1][colIndex[i0]];
				out[i1][colIndex[i0]] = temp;
			}
		}
	}

	delete[] rowIndex;
	delete[] colIndex;
	delete[] pivoted;
	return true;
}

// -------------------------------------------------------------------------- //

template <class Real>
bool MatrixMxN<Real>::SetUnitary()
{
	if(m_RowsCount == 0 || m_ColumnsCount == 0) return false;
	if(m_RowsCount != m_ColumnsCount) return false;

	for(unsigned int row = 0; row < m_RowsCount; ++row)
    {
		m_Rows[row][row] = static_cast<Real>(1.0);
    }
	return true;
}

// ************************************************************************** //
// Helper functions                                                           //
// ************************************************************************** //

template <class Real>
void MatrixMxN<Real>::Allocate(bool setToZero)
{
    m_Elements = new Real[m_ElementsCount];

    if (setToZero)
    {
        memset(m_Elements, 0, m_ElementsCount * sizeof(Real));
    }

    m_Rows = new Real*[m_RowsCount];
    for(unsigned int row = 0; row < m_RowsCount; ++row)
    {
        m_Rows[row] = &m_Elements[row * m_ColumnsCount];
    }
}

// -------------------------------------------------------------------------- //

template <class Real>
void MatrixMxN<Real>::Deallocate()
{
	if(m_Elements) delete[] m_Elements;
	if(m_Rows)     delete[] m_Rows;
}

// -------------------------------------------------------------------------- //

template <class Real>
int MatrixMxN<Real>::CompareElements(const MatrixMxN<Real>& other) const
{
    return memcmp(m_Elements, other.m_Elements, m_ElementsCount * sizeof(Real));
}

// -------------------------------------------------------------------------- //
// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real> operator * (Real scalar, const MatrixMxN<Real>& m)
{
    MatrixMxN<Real> mOut(m.GetRows(), m.GetColumns());

	const Real* tempIn = m;
    Real* tempOut = mOut;

	unsigned int elementsCount = m.GetElementsCount();

	for(unsigned int i = 0; i < elementsCount; ++i)
    {
        tempOut[i] = scalar * tempIn[i];
    }
    return mOut;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real> operator * (const VectorN<Real>& v, const MatrixMxN<Real>& m)
{
	unsigned int rowsCount    = m.GetRowsCount();
	unsigned int columnsCount = m.GetColumnsCount();

	VectorN<Real> vOut(columnsCount);

	if(v.GetDimensions() == rowsCount)
	{
		Real* temp = vOut;
		for(unsigned int col = 0; col < columnsCount; ++col)
		{
			for(unsigned int row = 0; row < rowsCount; ++row)
			{
				temp[col] += v[row] * m[row][col];
			}
		}
	}
    return vOut;
}
