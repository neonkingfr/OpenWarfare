// ************************************************************************** //
// Constructors                                                               //
// ************************************************************************** //

template <class Real>
LeastSquaresSolver<Real>::LeastSquaresSolver()
{
}

// ************************************************************************** //
// Interface                                                                  //
// ************************************************************************** //

template <class Real>
bool LeastSquaresSolver<Real>::SetCoefficientsMatrix(const MatrixMxN<Real>& matA)
{
	if(matA.GetRowsCount() < matA.GetColumnsCount()) return false;

	m_MatA = matA;

	unsigned int size = m_MatA.GetRowsCount();

	// clears and resizes the matrix of weights
	m_MatW.SetSize(size, size);
	// sets the matrix of weights to be the unitary matrix
	m_MatW.SetUnitary();
	return true;
}

// -------------------------------------------------------------------------- //

template <class Real>
bool LeastSquaresSolver<Real>::SetWeightsMatrix(const MatrixMxN<Real>& matW)
{
	if(!matW.IsSquare()) return false;
	if(m_MatA.GetRowsCount() != matW.GetRowsCount()) return false;

	m_MatW = matW;
	return true;
}

// -------------------------------------------------------------------------- //

template <class Real>
bool LeastSquaresSolver<Real>::SetKnownsVector(const VectorN<Real>& vecL)
{
	if(m_MatA.GetRowsCount() != vecL.GetDimensions()) return false;

	m_VecL = vecL;
	return true;
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real> LeastSquaresSolver<Real>::GetCoefficientsMatrix() const
{
	return m_MatA;
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real> LeastSquaresSolver<Real>::GetWeigthsMatrix() const
{
	return m_MatW;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real> LeastSquaresSolver<Real>::GetKnownsVector() const
{
	return m_VecL;
}

// -------------------------------------------------------------------------- //

template <class Real>
MatrixMxN<Real> LeastSquaresSolver<Real>::GetCovariancesMatrix() const
{
	return m_MatH;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real> LeastSquaresSolver<Real>::GetSolutionsVector() const
{
	return m_VecX;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real> LeastSquaresSolver<Real>::GetResidualsVector() const
{
	return m_VecV;
}

// -------------------------------------------------------------------------- //

template <class Real>
Real LeastSquaresSolver<Real>::GetResidualsSquaredSigma() const
{
	return m_SqSigma;
}

// -------------------------------------------------------------------------- //

template <class Real>
VectorN<Real> LeastSquaresSolver<Real>::GetSigmasVector() const
{
	return m_VecSigmas;
}

// -------------------------------------------------------------------------- //

template <class Real>
bool LeastSquaresSolver<Real>::Solve()
{
	MatrixMxN<Real> at     = m_MatA.Transpose();
	MatrixMxN<Real> at_w   = at * m_MatW; 
	MatrixMxN<Real> at_w_a = at_w * m_MatA; 

	if(!at_w_a.Inverse(m_MatH)) return false;

	VectorN<Real> at_w_l = at_w * m_VecL;

	// vector of solutions
	m_VecX = m_MatH * at_w_l;

	VectorN<Real> ax = m_MatA * m_VecX;

	// vector of residuals of solutions
	m_VecV = ax - m_VecL;

	unsigned int rowsCountA = m_MatA.GetRowsCount();
	unsigned int colsCountA = m_MatA.GetColumnsCount();

	unsigned int den = rowsCountA - colsCountA;

	if(den != 0)
	{
		m_SqSigma = ((m_VecV * m_MatW) * m_VecV) / den;
		unsigned int rowsCountH = m_MatH.GetRowsCount();
		m_VecSigmas.SetDimensions(rowsCountH);
		for(unsigned int i = 0; i < rowsCountH; ++i)
		{
			// this must be verified (should contain weigths)
			m_VecSigmas.SetAt(i, EtMath<Real>::Sqrt(m_MatH[i][i] * m_SqSigma));
		}
	}
	else
	{
		m_SqSigma = static_cast<Real>(0.0);
		m_VecSigmas.SetDimensions(0);
	}

	return true;
}

// -------------------------------------------------------------------------- //
