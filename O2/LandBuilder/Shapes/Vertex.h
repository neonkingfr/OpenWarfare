//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

namespace Shapes
{

//-----------------------------------------------------------------------------

	struct DVector
	{
		double x, y, z;
    
		DVector( double x = 0.0, double y = 0.0, double z = DBL_MIN ) 
		: x( x )
		, y( y )
		, z( z ) 
		{
		}    

		DVector( const DVector& v2, const DVector& v1 )
		{
			x = v2.x - v1.x;
			y = v2.y - v1.y;
			z = v2.z - v1.z;
		}

		bool IsZUsed() 
		{
			return (z != DBL_MIN);
		}
		
		ClassIsSimpleZeroed( DVector );

		void SetMaximum( const DVector& other )
		{
      if ( other.x > x ) { x = other.x; }
      if ( other.y > y ) { y = other.y; }
      if ( other.z > z ) { z = other.z; }
		}

		void SetMinimum( const DVector& other )
		{
      if ( other.x < x ) { x = other.x; }
      if ( other.y < y ) { y = other.y; }
      if ( other.z < z ) { z = other.z; }
		}

		bool EqualXY( const DVector& other ) const
		{
			return (other.x == x && other.y == y);
		}

		double DotXY( const DVector& other ) const
		{
			return (other.x * x + other.y * y);
		}

		// right hand rule
		DVector Cross( const DVector& other ) const
		{
		    return (DVector( y * other.z - z * other.y, 
                         z * other.x - x * other.z, 
                         x * other.y - y * other.x ));
		}

		double SizeXY2() const
		{
			return (x * x + y * y);
		}

		double SizeXY() const
		{
			return (sqrt( SizeXY2() ));
		}

		double SizeXYZ() const
		{
			return (sqrt(x * x + y * y + z * z));
		}

		double DistanceXY2( const DVector& other ) const
		{
			return (((*this) - (other)).SizeXY2());
		}

		double DistanceXY( const DVector& other ) const
		{
			return (((*this) - (other)).SizeXY());
		}

		DVector operator - ( const DVector& other ) const
		{
			return (DVector( x - other.x, 
                       y - other.y, 
                       z - other.z ));
		}

		DVector operator + ( const DVector& other ) const
		{
			return (DVector( x + other.x, 
                       y + other.y, 
                       z + other.z ));
		}

		DVector operator * ( double scalar ) const
		{
			return (DVector( x * scalar, 
                       y * scalar, 
                       z * scalar ));
		}

		DVector operator / ( double scalar ) const
		{
			return (operator * (1.0 / scalar));
		}

		DVector& operator += ( const DVector& other ) 
		{
			x += other.x;
			y += other.y;
			z += other.z;
			return (*this);
		}

		DVector& operator -= ( const DVector& other ) 
		{
			x -= other.x;
			y -= other.y;
			z -= other.z;
			return (*this);
		}

		DVector& operator *= ( double scalar ) 
		{
			x *= scalar;
			y *= scalar;
			z *= scalar;
			return (*this);
		}

		DVector& operator /= ( double scalar ) 
		{
			if ( scalar != 0.0 )
			{
				double invScalar = 1.0 / scalar;
				x *= invScalar;
				y *= invScalar;
				z *= invScalar;
			}
			else
			{
				x = DBL_MAX;
				y = DBL_MAX;
				z = DBL_MAX;
			}
			return (*this);
		}

		DVector& NormalizeXYInPlace()
		{
			*this /= SizeXY();
			return (*this);
		}

		DVector PerpXYCW() const
		{
			return (DVector( y, -x ));
		}

		DVector PerpXYCCW() const
		{
			return (DVector( -y, x ));
		}

		double DirectionXY() const
		{
			double PI = 4.0 * atan( 1.0 );

			if ( x == 0.0 )
			{
				if ( y > 0.0 ) 
				{
					return (PI * 0.5);
				}
				else if ( y < 0.0 )
				{
					return (PI * 3.0 * 0.5);
				}
				else
				{
					return (0.0);
				}
			}
			else
			{
				if ( y == 0.0 )
				{
					if ( x > 0.0 )
					{
						return (0.0);
					}
					else
					{
						return (PI);
					}
				}
				else
				{
					double angle = atan( fabs( y / x ) );
					if ( x < 0 && y > 0 )
					{
						return (PI - angle);
					}
					if ( x < 0 && y < 0 )
					{
						return (angle + PI);
					}
					if ( x > 0 && y < 0 )
					{
						return (2.0 * PI - angle);
					}
					return (angle);
				}
			}
		}

		double DirectionYX() const
		{
			double PI = 4.0 * atan( 1.0 );

			if ( x == 0.0 )
			{
				if ( y > 0.0 ) 
				{
					return (0.0);
				}
				else if ( y < 0.0 )
				{
					return (2.0 * PI);
				}
				else
				{
					return (0.0);
				}
			}
			else
			{
				if ( y == 0.0 )
				{
					if ( x > 0.0 )
					{
						return (PI * 0.5);
					}
					else
					{
						return (3.0 * PI * 0.5);
					}
				}
				else
				{
					double angle = atan( fabs( x / y ) );
					if ( x < 0 && y > 0 )
					{
						return (2.0 * PI - angle);
					}
					if ( x < 0 && y < 0 )
					{
						return (angle + PI);
					}
					if ( x > 0 && y < 0 )
					{
						return (PI - angle);
					}
					return (angle);
				}
			}
		}

		Shapes::DVector GetSecondEndXY( const Shapes::DVector& firstEnd )
		{
			double angle  = DirectionXY();
			double length = SizeXY();
			double tempX  = firstEnd.x + length * cos( angle ); 
			double tempY  = firstEnd.y + length * sin( angle );
			return (Shapes::DVector( tempX, tempY ));
		}

		void RotateXY( double angle )
		{
			double tempX = x;
			double tempY = y;
			x = tempX * cos( angle ) - tempY * sin( angle ); 
			y = tempX * sin( angle ) + tempY * cos( angle );
		}
	};

//-----------------------------------------------------------------------------

	struct DVertex : public DVector
	{
		double m;

		DVertex( double x = 0.0, double y = 0.0, double z = DBL_MIN, double m = DBL_MIN ) 
		: DVector( x, y, z )
		, m( m ) 
		{
		}

		DVertex( const DVector& vx, double m = DBL_MIN ) 
		: DVector( vx )
		, m( m ) 
		{
		}

		bool IsMUsed() 
		{
			return (m != DBL_MIN);
		}

		ClassIsSimpleZeroed( DVertex );

		bool IsInvalidVertex() const;
		static const DVertex& GetInvalidVertex();

		void SetMaximum( const DVertex& other )
		{
      if ( other.m > m ) { m = other.m; }
			DVector::SetMaximum( other );
		}

		void SetMinimum( const DVertex& other )
		{
      if ( other.m < m ) { m = other.m; }
			DVector::SetMinimum( other );
		}
	};

//-----------------------------------------------------------------------------

	class VertexArray
	{
	protected:
		AutoArray< DVertex >       m_v;
		AutoArray< unsigned char > m_flags;

		size_t                     m_nextFree;

	public:
		enum Flags
		{
			fUsed = 1
		};

	protected:
		size_t GetNextFreeAtPos( size_t pos ) const;
		size_t GetPrevFreeAtPos( size_t pos ) const;
		void SetNextFreeAtPos( size_t pos, size_t nextFree );
		size_t AllocVertex();
		void FreeVertex( size_t pos );

	public:
		size_t Size() const 
		{
			return (this ? (unsigned)m_v.Size() : 0);
		}    

		VertexArray() 
		: m_nextFree( 0 ) 
		{
		}

		VertexArray( size_t dimension ) 
    : m_v( dimension )
    , m_flags( dimension )
		, m_nextFree( 0 ) 
		{
		}

		size_t AddVertex( const DVertex& vx = DVertex() );
		bool DeleteVertex( size_t id );
		const DVertex& GetVertex( size_t pos ) const;
		bool SetVertex( size_t pos, const DVertex& vx );

		const DVertex& operator [] ( size_t id ) const
		{
			return (GetVertex( id ));
		}

		bool SetAt( size_t id, const DVertex& vx ) 
		{
			return (SetVertex( id, vx ));
		}
    
		template < class VertexArrayType >
		class IterT
		{
			friend class VertexArray;

			size_t           m_vxPos;
			VertexArrayType* m_outer;

			IterT( VertexArrayType* arr, size_t vxPos ) 
      : m_vxPos( vxPos )
      , m_outer( arr ) 
      {
      }

		public:
			operator bool () 
			{
				return (m_vxPos != -1);
			}

			IterT& operator ++ () 
			{
				m_outer->NextVertex( m_vxPos );
				return (*this);
			}

			IterT& operator -- () 
			{
				m_outer->PrevVertex( m_vxPos );
				return (*this);
			}

			IterT operator ++ ( int ) 
			{
				int saveVx = m_vxPos;
				m_outer->NextVertex( m_vxPos );
				return (IterT< VertexArrayType >( m_outer, saveVx ));
			}

			IterT operator -- ( int ) 
			{
				int saveVx = m_vxPos;
				m_outer->PrevVertex( m_vxPos );
				return (IterT< VertexArrayType >( m_outer, saveVx ));
			}

			const DVertex& GetVertex() const 
			{
				return (m_outer->GetVertex( m_vxPos ));
			}

			bool SetVertex( const DVertex& vx )
			{
				return (m_outer->SetVertex( m_vxPos, vx ));
			}

			bool DeleteVertex()
			{
				return (m_outer->DeleteVertex( m_vxPos ));
			}

			int Index() const
			{
				return (m_vxPos);
			}
		};

		typedef IterT< VertexArray >       Iter;
		typedef IterT< const VertexArray > IterC;

		friend class IterT< VertexArray >;
		friend class IterT< const VertexArray >;

		Iter First();
		IterC First() const;
		Iter Last();
		IterC Last() const;

	protected:
		void NextVertex( size_t& vxPos ) const;
		void PrevVertex( size_t& vxPos ) const;
	};

//-----------------------------------------------------------------------------

	struct DBox
	{
		DVertex lo;
		DVertex hi;

	public:
		DBox( const DVertex& lo, const DVertex& hi ) 
    : lo( lo ), hi( hi ) 
		{
		}

		DBox() 
		{
		}

		bool PtInsideXY( const DVector& pt )const
		{
			return (lo.x <= pt.x && hi.x > pt.y && lo.y <= pt.y && hi.y > pt.y);
		}

		bool IntersectionXY( const DBox& box ) const
		{
      if ( box.lo.x < hi.x && box.hi.x > lo.x && box.lo.y < hi.y && box.hi.y > lo.y ) { return (true); }
			return (false);
		}
	};

//-----------------------------------------------------------------------------

} // namespace Shapes
