//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

namespace Shapes
{

//-----------------------------------------------------------------------------

	class ShapeDatabase;

//-----------------------------------------------------------------------------

	class IDBColumnNameConvert
	{
	public:
		virtual const char* TranslateColumnName( const char* name ) const = 0;
	};

//-----------------------------------------------------------------------------

	class DBFReader
	{
	public:

#pragma pack( 1 )

		struct FieldInfo
		{
			char fieldName[11];
			char fieldType;
			unsigned long disp;
			unsigned char length;
			unsigned char decimals;
			unsigned char flags;
			unsigned long nextAutoincrement;
			unsigned char step;
			unsigned long res5;
			unsigned long res6;
			ClassIsBinaryZeroed( FieldInfo );
		};

		struct DBFHeader
		{
			unsigned char version;
			unsigned char lastupdate_yy;
			unsigned char lastupdate_mm;
			unsigned char lastupdate_dd;
			unsigned long records;
			unsigned short headersz;
			unsigned short reclen;
			unsigned short zeroes;
			char incomplette_transaction;
			char encription;
			char multiuser_reservation[12];
			char index;
			unsigned char language;
			char reserved[2];
		};

#pragma pack()

	protected:
		DBFHeader                m_header;
		AutoArray< FieldInfo >   m_fieldInfo;
		AutoArray< const char* > m_fieldTranslated;

	public:
		enum ReaderError
		{
			errOk = 0,
			errFileOpenError,
			errHeaderReadError,
			errErrorReadingData
		};

		ReaderError ReadDBF( std::istream& input, ShapeDatabase& database, const Array< size_t >& shapeIndexes, IDBColumnNameConvert* t = NULL );    
		ReaderError ReadDBF( const char* fname, ShapeDatabase& database, const Array< size_t >& shapeIndexes, IDBColumnNameConvert* t = NULL );
	};
}

//-----------------------------------------------------------------------------

TypeIsSimple( const char* );

//-----------------------------------------------------------------------------
