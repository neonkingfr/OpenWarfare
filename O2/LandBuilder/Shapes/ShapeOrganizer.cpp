//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include ".\ShapeFileReader.h"
#include ".\DBFReader.h"
#include ".\shapeorganizer.h"

//-----------------------------------------------------------------------------

namespace Shapes
{

//-----------------------------------------------------------------------------

	using namespace ExceptReg;

//-----------------------------------------------------------------------------

	ShapeOrganizer::ShapeOrganizer( IShapeFactory* factory ) 
	: m_factory( factory )
	, m_nextcpcountervalue( 1 )
	{
	}

//-----------------------------------------------------------------------------

	ShapeOrganizer::~ShapeOrganizer()
	{
	}

//-----------------------------------------------------------------------------

	const char* ShapeOrganizer::s_algorithmFieldName = "~Algorithm";
	const char* ShapeOrganizer::s_cpFieldName        = "~ConstParams";

//-----------------------------------------------------------------------------

	size_t ShapeOrganizer::AllocateNewConstantParamsID( const char* algorithm )
	{
		m_cp.SetField( m_nextcpcountervalue, s_algorithmFieldName, algorithm );
		return (m_nextcpcountervalue++);
	}

//-----------------------------------------------------------------------------

	void ShapeOrganizer::DeleteConstantParams( size_t ID )
	{
		m_cp.DeleteShape( ID );
	}

//-----------------------------------------------------------------------------

	void ShapeOrganizer::SetConstantParam( size_t cpID, const char* name, const char* value )
	{
		m_cp.SetField( cpID, name, value );
	}

//-----------------------------------------------------------------------------

	const char* ShapeOrganizer::GetConstantParam( size_t cpID, const char* name ) const
	{
		return (m_cp.GetField( cpID, name ));
	}

//-----------------------------------------------------------------------------

	bool ShapeOrganizer::SetShapeCP( size_t shapeID, size_t constantParamsID )
	{
		char buff[50];
		sprintf( buff, "%X", constantParamsID );
		return (m_shapeData.SetField( shapeID, s_cpFieldName, buff ));
	}

//-----------------------------------------------------------------------------

	const char* ShapeOrganizer::GetShapeParameter( size_t shapeID, const char* fieldname ) const
	{
		return (m_shapeData.GetField( shapeID, fieldname ));
	}

//-----------------------------------------------------------------------------

	size_t ShapeOrganizer::GetShapeCP( size_t shapeID ) const
	{
		const char* c = m_shapeData.GetField( shapeID, s_cpFieldName );
    if ( !c ) { return (-1); }
		size_t cpindex;
    if ( sscanf( c, "%X", &cpindex ) != 1 ) { return (-1); }
		return (cpindex);
	}

//-----------------------------------------------------------------------------

	const char* ShapeOrganizer::GetShapeConstParameter( size_t ShapeID, const char* fieldname ) const
	{
		return (GetConstantParam( GetShapeCP( ShapeID ), fieldname ));
	}

//-----------------------------------------------------------------------------

	size_t ShapeOrganizer::AddShape( IShape* shape, size_t constantParamsID )
	{
		int id = m_listOfShapes.AddShape( shape );
    if ( id != -1 ) { SetShapeCP( id, constantParamsID ); }
		return (id);
	}

//-----------------------------------------------------------------------------

	bool ShapeOrganizer::DeleteShape( size_t ID )
	{
		m_listOfShapes.DeleteShape( ID );
		return (m_shapeData.DeleteShape( ID ));
	}

//-----------------------------------------------------------------------------

	void ShapeOrganizer::DeleteUnusedCP( bool report )
	{
		BTree< size_t > usedCP;
		for ( int curCP = -1; (curCP = m_shapeData.GetNextUsedShapeID( curCP )) != -1; )
		{
			size_t cpid = GetShapeCP( curCP );
			usedCP.Add( cpid );
		}

		for ( int curCP = -1; (curCP = m_cp.GetNextUsedShapeID( curCP )) != -1;)
		{
			if ( usedCP.Find( curCP ) == 0 )
			{
				if ( !report || RegExcpt( UnusedCP( curCP, this ) ) != ExceptionRegister::fbIgnore )
				{
					m_cp.DeleteShape( curCP );
				}
			}
		}
	}

//-----------------------------------------------------------------------------

	bool ShapeOrganizer::ReadShapeFile( const Pathname& name, 
                                      size_t constantParamsID, 
                                      IDBColumnNameConvert* dbconvert, 
                                      const Pathname& forceDBChange,
                                      AutoArray< size_t >* shapeIndexes )
	{
		class MyShapeFileReader : public ShapeFileReader
		{
			IShapeFactory* m_factory;

		public:
			MyShapeFileReader( IShapeFactory* factory ) 
			: m_factory( factory ) 
			{
			}

			virtual IShape* CreatePoint( const ShapePoint& origShape )
			{
				return (m_factory ? m_factory->CreatePoint( origShape ) : ShapeFileReader::CreatePoint( origShape ));
			}

			virtual IShape* CreateMultipoint( const ShapeMultiPoint& origShape )
			{
				return (m_factory ? m_factory->CreateMultipoint( origShape ) : ShapeFileReader::CreateMultipoint( origShape ));
			}

			virtual IShape* CreatePolyLine( const ShapePoly& origShape )
			{
				return (m_factory ? m_factory->CreatePolyLine( origShape ) : ShapeFileReader::CreatePolyLine( origShape ));
			}

			virtual IShape* CreatePolygon( const ShapePoly& origShape )
			{
				return (m_factory ? m_factory->CreatePolygon( origShape ) : ShapeFileReader::CreatePolygon( origShape ));
			}
		};

		MyShapeFileReader rdr( m_factory );
		AutoArray< size_t > shpidx;
    if ( !shapeIndexes ) { shapeIndexes = &shpidx; }
		ShapeFileReader::ShapeReaderError err = rdr.ReadShapeFile( name, m_vxList, m_listOfShapes, shapeIndexes );
		if ( err ) 
		{
			RegExcpt( ShapeLoaderError( err, name ) );
		}
		else
		{
			Pathname dbfname = forceDBChange;
			if ( dbfname.IsNull() )
			{
				dbfname = name;
				dbfname.SetExtension( ".dbf" );
			}
			DBFReader dbfreader;
			DBFReader::ReaderError derr = dbfreader.ReadDBF( dbfname, m_shapeData, *shapeIndexes, dbconvert );
			if ( derr ) 
			{
				RegExcpt( DatabaseLoaderError( err, name ) );
			}
			else
			{
				for ( int i = 0, cnt = shapeIndexes->Size(); i < cnt; ++i )
				{
					SetShapeCP( (*shapeIndexes)[i], constantParamsID );
				}
				return (true);
			}
		}

		for ( int i = 0, cnt = shapeIndexes->Size(); i < cnt; ++i )
		{
			DeleteShape( (*shapeIndexes)[i] );
		}

		return (false);
	}
  
//-----------------------------------------------------------------------------
	
	DBox ShapeOrganizer::CalcBoundingBox() const
	{  
		DBox res( DVertex( 0.0, 0.0 ), DVertex( 0.0, 0.0 ) );
		VertexArray::IterC iter = m_vxList.First();
		if ( iter )
		{
			res.lo = res.hi = iter.GetVertex();
			++iter;
			while ( iter )
			{
				const DVertex& x = iter.GetVertex();
				res.lo.SetMinimum( x );
				res.hi.SetMaximum( x );
				++iter;
			}
		}
		return (res);
	}

//-----------------------------------------------------------------------------

	void ShapeOrganizer::TransformVertices( const Matrix4& a )
	{
		for ( VertexArray::Iter iter = m_vxList.First(); iter; ++iter )
		{
			const DVertex& vx = iter.GetVertex();
			DVertex nx;
			nx.x = a( 0, 0 ) * vx.x + a( 0, 1 ) * vx.y + a( 0, 2 ) * vx.z + a( 0, 3 );
			nx.y = a( 1, 0 ) * vx.x + a( 1, 1 ) * vx.y + a( 1, 2 ) * vx.z + a( 1, 3 );
			nx.z = a( 2, 0 ) * vx.x + a( 2, 1 ) * vx.y + a( 2, 2 ) * vx.z + a( 2, 3 );
			iter.SetVertex( nx );
		}
	}

//-----------------------------------------------------------------------------

} // namespace Shapes
