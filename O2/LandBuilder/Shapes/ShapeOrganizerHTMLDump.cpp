#include "StdAfx.h"
#include ".\shapeorganizerhtmldump.h"
#include <fstream>
#include <windows.h>
#include "ShapePoint.h"
#include "ShapeMultiPoint.h"
#include "ShapePoly.h"
#include "ShapePolyLine.h"
#include "ShapePolygon.h"

#define PREVIEWWIDTH 400
#define PREVIEWHEIGHT 300

namespace Shapes
{
  using namespace std;

  class IDrawInterface
  {
  public:
    static const unsigned int IFCUID=11223;
    virtual void Draw(HDC canvas,const DBox &trns)=0;
  };

  static void htmlspecialchars(ostream &out, const char *text)
  {
    while (*text)
    {
      if (*text=='<') out<<"&lt;";
      else if (*text=='>') out<<"&gt;";
      else if (*text=='&') out<<"&amp;";
      else if (*text=='"') out<<"&quot;";
      else out<<*text;
      text++;
    }
  }

  class DumpDBFields
  {
    mutable ostream &out;
  public:
    DumpDBFields(ostream &out):out(out) {}
    bool operator()(unsigned int shapeId, const char *param, const char *value) const
    {
      out<<"<tr><th>";
      htmlspecialchars(out,param);
      out<<"</th><td>";
      htmlspecialchars(out,value);
      out<<"</td></tr>";
      return false;
    }

  };


  static void DrawObjectPreview(const Pathname& refName, ostream &out, IDrawInterface *obj, DBox box)
  {
    if (box.hi.x==box.lo.x)
    {
      box.hi.x*=1.1f;
      box.lo.x*=0.9f;
    }
    if (box.hi.y==box.lo.y)
    {
      box.hi.y*=1.1f;
      box.lo.y*=0.9f;      
    }

    DBox trns;
    trns.hi.x=PREVIEWWIDTH/(box.hi.x-box.lo.x);
    trns.hi.y=PREVIEWHEIGHT/(box.hi.y-box.lo.y);
    trns.lo.x=-box.lo.x*trns.hi.x;
    trns.lo.y=-box.lo.y*trns.hi.y;
    HWND desktop=GetDesktopWindow();
    HDC dcref=GetDC(desktop);
    Pathname rfname;
    char buffer[256];
    sprintf(buffer,"%X.emf",obj);
    rfname=refName;
    rfname.SetFiletitle(refName.GetFilename());
    rfname.SetExtension(".data");
    rfname.SetDirectory(rfname);
    rfname.SetFilename(buffer);

    RECT bound={0,0,PREVIEWWIDTH,PREVIEWHEIGHT};

    int iWidthMM = GetDeviceCaps(dcref, HORZSIZE); 
    int iHeightMM = GetDeviceCaps(dcref, VERTSIZE); 
    int iWidthPels = GetDeviceCaps(dcref, HORZRES); 
    int iHeightPels = GetDeviceCaps(dcref, VERTRES); 

    bound.left = (bound.left * iWidthMM * 100)/iWidthPels; 
    bound.top = (bound.top * iHeightMM * 100)/iHeightPels; 
    bound.right = (bound.right * iWidthMM * 100)/iWidthPels; 
    bound.bottom = (bound.bottom * iHeightMM * 100)/iHeightPels; 

    CreateDirectory(rfname.GetDirectoryWithDriveWLBS(),NULL);
    HDC dc=CreateEnhMetaFile(dcref,rfname,&bound,"\0\0\0");
    ReleaseDC(desktop,dcref);
    if (dc==0) return;

    SelectObject(dc,GetStockObject(NULL_PEN));
    SelectObject(dc,GetStockObject(WHITE_BRUSH));    
    Rectangle(dc,0,0,PREVIEWWIDTH,PREVIEWHEIGHT);

    obj->Draw(dc,trns);

    SelectObject(dc,GetStockObject(NULL_PEN));
    SelectObject(dc,GetStockObject(BLACK_BRUSH));
    Rectangle(dc,PREVIEWWIDTH/2-5,PREVIEWHEIGHT/2-5,PREVIEWWIDTH/2+5,PREVIEWHEIGHT/2+5);
    HENHMETAFILE metafile=CloseEnhMetaFile(dc);
    DeleteEnhMetaFile(metafile);

    out<<"<img src=\""<<rfname.GetNameFromPath(rfname.GetDirectoryWithDriveWLBS())<<"/"<<rfname.GetFilename()<<"\" width=400 height=300>";
  }

  IShape *ClipTest(const IShape *orig, const DVector &a, const DVector &b)
  {
    double la=-(b.y-a.y);
    double lb=b.x-a.x;
    double lc=-(la*a.x+lb*a.y);    

    return orig->Clip(la,lb,lc);
  }

  IShape *CropTest(const IShape *orig, const DVector &a, const DVector &b)
  {
    double left=a.x+(b.x-a.x)/3;
    double top=a.y+(b.y-a.y)/3;
    double right=a.x+2*(b.x-a.x)/3;
    double bottom=a.y+2*(b.y-a.y)/3;

    return orig->Crop(left,top,right,bottom);
  }

  void ShapeOrganizerHTMLDump::DumpToHTML(const ShapeOrganizer &db, const Pathname &filename)
  {
    ofstream out(filename,ios::out);
    out<<"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n";
    out<<"<html>\r\n";
    out<<"<head>\r\n";
    out<<"<title>DUMP</title>\r\n";
    out<<"<style>\r\n";
    out<<".inner th,.inner td {border-right: 1px solid gray; border-bottom: 1px solid gray; min-width: 100px;}\r\n";
    out<<".id {background-color: #FFFED0;}\r\n";
    out<<".db {background-color: #FFD0D0;}\r\n";
    out<<".cp {background-color: #E0D0FF;}\r\n";
    out<<".preview {background-color: #D0F0FF;}\r\n";
    out<<"body {background-color: #FFFFFF; color: #0;}\r\n";
    out<<"</style>\r\n";
    out<<"</head>\r\n";
    out<<"<body>\r\n";

    out<<"<table border=\"1\" class=\"outer\" width=\"100%\">\r\n";    
    out<<"<tr><th class=\"id\">ShapeID</th><th class=\"db\">DB Parameters</th><th class=\"cp\">Config</th><th class=\"preview\">Preview</th></tr>\r\n";
    for (ShapeList::IterC iter=db.FirstShape();iter;++iter)
    {
      unsigned int id=iter.GetShapeId();
      unsigned int cp=db.GetShapeCP(id);       
      out<<"<tr>";
      out<<"<td class=\"id\">"<<id<<"</td>";
      out<<"<td class=\"db\"><table class=\"inner\" width=\"100%\">";
      db.GetShapeDatabase().ForEachField(DumpDBFields(out),id);
      out<<"</table></td>";
      out<<"<td class=\"cp\"  width=\"100%\"><table>";
      db.GetConstantParamDB().ForEachField(DumpDBFields(out),cp);
      out<<"</table></td>";
      out<<"<td>";
      IShape *shape=iter.GetShape();
      DBox box=shape->CalcBoundingBox();
//      IShape *clipped=ClipTest(shape,box.lo,box.hi);
      IShape *clipped=CropTest(shape,box.lo,box.hi);
      if (clipped)
      {      
        IDrawInterface *drawObj=clipped->GetInterface<IDrawInterface>();
        if (drawObj) DrawObjectPreview(filename,out,drawObj,iter.GetShape()->CalcBoundingBox());
/*        IDrawInterface *drawObj=shape->GetInterface<IDrawInterface>();
        if (drawObj) DrawObjectPreview(filename,out,drawObj,iter.GetShape()->CalcBoundingBox());
 //       bool inside=clipped->PtInside(DVector((box.lo.x+box.hi.x)*0.5f,(box.lo.y+box.hi.y)*0.5f));
//        DVector nearestInside=shape->NearestInside(DVector((box.lo.x+box.hi.x)*0.5f,(box.lo.y+box.hi.y)*0.5f));
/*        if (inside)
        {
          out<<"X";
        }*/
      }
      out<<"</td></tr>\r\n";
    }
    out<<"</table>\r\n";
    out<<"</body>\r\n";
    out<<"</html>\r\n";
  }

  static int toLargeInt(double f)
  {
    return ::toLargeInt((float)f);
  }

  IShape *ShapeOrganizerHTMLDump::CreatePoint(const ShapePoint &origShape)
  {
    class ShapePointDraw: public ShapePoint, public IDrawInterface
    {
    public:
      ShapePointDraw(const ShapePoint &other):ShapePoint(other) {}
      virtual void Draw(HDC dc,const DBox &trns)
      {
        const DVector &v=GetVertex(0);
        SetPixel(dc,toLargeInt(v.x*trns.hi.x+trns.lo.x),toLargeInt(v.y*trns.hi.y+trns.lo.y),0);
      }
      virtual void *GetInterfacePtr(unsigned int ifcuid)
      {
        return ifcuid==IDrawInterface::IFCUID?static_cast<IDrawInterface *>(this):ShapePoint::GetInterfacePtr(ifcuid);
      }
      virtual ShapePointDraw *NewInstance(const ShapePoint &other) const
      {
        return new ShapePointDraw(other);
      }
    };
    return new ShapePointDraw(origShape);
  }
  IShape *ShapeOrganizerHTMLDump::CreateMultipoint(const ShapeMultiPoint &origShape)
  {
    class ShapePointDraw: public ShapeMultiPoint, public IDrawInterface
    {
    public:
      ShapePointDraw(const ShapeMultiPoint &other):ShapeMultiPoint(other) {}
      virtual void Draw(HDC dc,const DBox &trns)
      {
        for (unsigned int i=0,cnt=GetVertexCount();i<cnt;i++)
        {        
          const DVector &v=GetVertex(i);
          SetPixel(dc,toLargeInt(v.x*trns.hi.x+trns.lo.x),toLargeInt(v.y*trns.hi.y+trns.lo.y),0);
        }
      }
      virtual void *GetInterfacePtr(unsigned int ifcuid)
      {
        return ifcuid==IDrawInterface::IFCUID?static_cast<IDrawInterface *>(this):ShapeMultiPoint::GetInterfacePtr(ifcuid);
      }
      virtual ShapePointDraw *NewInstance(const ShapeMultiPoint &other) const
      {
        return new ShapePointDraw (other);
      }
    };
    return new ShapePointDraw(origShape);
  }
  IShape *ShapeOrganizerHTMLDump::CreatePolyLine(const ShapePoly &origShape)
  {
    class ShapePolyDraw: public ShapePolyLine, public IDrawInterface
    {
    public:
      ShapePolyDraw(const ShapePoly &other):ShapePolyLine(other) {}
      virtual void Draw(HDC dc,const DBox &trns)
      {
        for (unsigned int i=0,cnt=GetPartCount();i<cnt;i++)
        {        
          unsigned int partstart=GetPartIndex(i);
          unsigned int partsz=i+1==cnt?GetVertexCount():GetPartIndex(i+1)-partstart;
          SelectObject(dc,GetStockObject(BLACK_PEN));
          for (unsigned int j=0,cnt=partsz;j<cnt;j++)
          {
            const DVector &v=GetVertex(j+partstart);
            if (j==0) MoveToEx(dc,toLargeInt(v.x*trns.hi.x+trns.lo.x),toLargeInt(v.y*trns.hi.y+trns.lo.y),0);
            else LineTo(dc,toLargeInt(v.x*trns.hi.x+trns.lo.x),toLargeInt(v.y*trns.hi.y+trns.lo.y));
          }
        }
      }
      virtual void *GetInterfacePtr(unsigned int ifcuid)
      {
        return ifcuid==IDrawInterface::IFCUID?static_cast<IDrawInterface *>(this):ShapePoly::GetInterfacePtr(ifcuid);
      }
      virtual ShapePolyDraw *NewInstance(const ShapePolyLine &other) const
      {
        return new ShapePolyDraw(other);
      }
      virtual ShapePolyDraw *NewInstance(const ShapePoly &other) const
      {
        return new ShapePolyDraw(other);
      }
    };
    return new ShapePolyDraw(origShape);
  }

  IShape *ShapeOrganizerHTMLDump::CreatePolygon(const ShapePoly &origShape)
  {
    class ShapePolyDraw: public ShapePolygon, public IDrawInterface
    {
    public:
      ShapePolyDraw(const ShapePoly &other):ShapePolygon(other) {}
      virtual void Draw(HDC dc,const DBox &trns)
      {
        POINT *ptlist=(POINT *)alloca(sizeof(POINT)*GetVertexCount());
        LOGBRUSH lg;
        lg.lbColor=RGB(0,200,200);
        lg.lbStyle=BS_SOLID;
        lg.lbHatch=0;
        HBRUSH fl=CreateBrushIndirect(&lg);
        HBRUSH obr=(HBRUSH)SelectObject(dc,fl);
        SelectObject(dc,GetStockObject(BLACK_PEN));
        for (unsigned int i=0,cnt=GetPartCount();i<cnt;i++)
        {        
          unsigned int partstart=GetPartIndex(i);
          unsigned int partsz=(i+1==cnt?GetVertexCount():GetPartIndex(i+1))-partstart;
          for (unsigned int j=0,cnt=partsz;j<cnt;j++)
          {
            const DVector &v=GetVertex(j+partstart);
            ptlist[j].x=toLargeInt(v.x*trns.hi.x+trns.lo.x);
            ptlist[j].y=toLargeInt(v.y*trns.hi.y+trns.lo.y);
          }
          Polygon(dc,ptlist,partsz);
        }
        SelectObject(dc,obr);
        DeleteObject(fl);
      }
      virtual void *GetInterfacePtr(unsigned int ifcuid)
      {
        return ifcuid==IDrawInterface::IFCUID?static_cast<IDrawInterface *>(this):ShapePoly::GetInterfacePtr(ifcuid);
      }
      virtual ShapePolyDraw *NewInstance(const ShapePolygon &other) const
      {
        return new ShapePolyDraw(other);
      }
      virtual ShapePolyDraw *NewInstance(const ShapePoly &other) const
      {
        return new ShapePolyDraw(other);
      }
    };
    return new ShapePolyDraw(origShape);
  }

}