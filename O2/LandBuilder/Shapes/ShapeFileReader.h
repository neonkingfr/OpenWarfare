//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

namespace Shapes
{

//-----------------------------------------------------------------------------

	class VertexArray;
	class ShapeList;
	class IShape;
	class ShapePoint;
	class ShapeMultiPoint;
	class ShapePoly;

//-----------------------------------------------------------------------------

	class ShapeFileReader
	{
	public:
		struct ShapeHeader
		{
			unsigned long m_fileCode;
			unsigned long m_fileLength;
			unsigned long m_version;
			unsigned long m_shapeType;
			double m_Xmin;
      double m_Ymin;
      double m_Xmax;
      double m_Ymax;
      double m_Zmin;
      double m_Zmax;
      double m_Mmin;
      double m_Mmax;
		};

	protected:
		ShapeHeader m_header;
    
	public:
		enum ShapeReaderError
		{
			sheOk                  =0,
			sheReadError           =1,
			sheUnexceptedEndOfFile =2,
			sheOpenError           =3,
			sheFileTagMismach      =4,
			sheVersionNotSupported =5
		};

		ShapeReaderError ReadShapeFile( std::istream& input, VertexArray& vxArray, ShapeList& shapeList, 
                                    AutoArray< size_t >* indexList = NULL );

    const ShapeHeader& GetShapeFileHeader() const 
		{
			return (m_header);
		}
		ShapeReaderError ReadShapeFile( const _TCHAR* filename, VertexArray& vxArray, ShapeList& shapeList, 
                                    AutoArray< size_t >* indexList = NULL );

	public:
		virtual IShape* CreatePoint( const ShapePoint& origShape );
		virtual IShape* CreateMultipoint( const ShapeMultiPoint& origShape );
		virtual IShape* CreatePolyLine( const ShapePoly& origShape );
		virtual IShape* CreatePolygon( const ShapePoly& origShape );
	};

//-----------------------------------------------------------------------------

} // namespace Shapes
