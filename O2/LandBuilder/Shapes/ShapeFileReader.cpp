//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "ShapePoint.h"
#include "ShapeMultiPoint.h"
#include "ShapePoly.h"
#include "ShapePolygon.h"
#include "ShapePolyLine.h"
#include "Vertex.h"
#include "ShapeList.h"
#include ".\shapefilereader.h"
#include <fstream>

//-----------------------------------------------------------------------------

namespace Shapes
{

//-----------------------------------------------------------------------------

  using namespace std;

//-----------------------------------------------------------------------------

  template < class T >
  bool ReadBig( istream& input, T& t )
  {
    char* c = reinterpret_cast< char* >( &t );
    for ( int i = sizeof( t ) - 1; i >= 0; --i )
    {
      input.read( c + i, 1 );
      if ( input.gcount() != 1 ) { return (false); }
    }
    return (true);
  }

//-----------------------------------------------------------------------------

  template < class T >
  bool ReadLittle( istream& input, T& t )
  {
    char* c = reinterpret_cast< char* >( &t );
    input.read( c, sizeof( t ) );
    if ( input.gcount() != sizeof( t ) ) { return (false); }
    return (true);
  }

//-----------------------------------------------------------------------------

  static ShapeFileReader::ShapeReaderError ReadShapeHeader( istream& input, ShapeFileReader::ShapeHeader& hdr )
  {
    char unused[5 * 4];
    if ( !ReadBig( input, hdr.m_fileCode ) ) { return (ShapeFileReader::sheReadError); }
    input.read( unused, sizeof( unused ) );
    if ( !ReadBig( input, hdr.m_fileLength ) ) { return (ShapeFileReader::sheReadError); }
    if ( !ReadLittle( input, hdr.m_version ) ) { return (ShapeFileReader::sheReadError); }
    if ( !ReadLittle( input, hdr.m_shapeType ) ) { return (ShapeFileReader::sheReadError); }
    if ( !ReadLittle( input, hdr.m_Xmin ) ) { return (ShapeFileReader::sheReadError); }
    if ( !ReadLittle( input, hdr.m_Ymin ) ) { return (ShapeFileReader::sheReadError); }
    if ( !ReadLittle( input, hdr.m_Xmax ) ) { return (ShapeFileReader::sheReadError); }
    if ( !ReadLittle( input, hdr.m_Ymax ) ) { return (ShapeFileReader::sheReadError); }
    if ( !ReadLittle( input, hdr.m_Zmin ) ) { return (ShapeFileReader::sheReadError); }
    if ( !ReadLittle( input, hdr.m_Zmax ) ) { return (ShapeFileReader::sheReadError); }
    if ( !ReadLittle( input, hdr.m_Mmin ) ) { return (ShapeFileReader::sheReadError); }
    if ( !ReadLittle( input, hdr.m_Mmax ) ) { return (ShapeFileReader::sheReadError); }  
    return (ShapeFileReader::sheOk);  
  }

//-----------------------------------------------------------------------------

  struct RecordHeader
  {
    unsigned long m_recordNumber;
    unsigned long m_contentLength;
    unsigned long m_shapeType;
  };

//-----------------------------------------------------------------------------

  static bool ReadRecordHeader( istream& input, RecordHeader& hdr )
  {
    if ( !ReadBig( input, hdr.m_recordNumber ) ) { return (false); }
    if ( !ReadBig( input, hdr.m_contentLength ) ) { return (false); }
    if ( !ReadLittle( input, hdr.m_shapeType ) ) { return (false); }
    return (true);
  }

//-----------------------------------------------------------------------------

  static size_t AddVertex( VertexArray& vxArray, double x, double y, AutoArray< size_t >* vxIndexes = NULL )
  {
    size_t res = vxArray.AddVertex( DVertex( x, y ) );
    if ( vxIndexes ) { vxIndexes->Add( res ); }
    return (res);
  }

//-----------------------------------------------------------------------------

  static ShapeFileReader::ShapeReaderError ReadPoint( istream& input, VertexArray& vxArray, 
                                                      ShapePoint& pt, AutoArray< size_t >* vxIndexes = NULL )
  {
    double x,y;
    if ( !ReadLittle( input, x ) ) { return (ShapeFileReader::sheReadError); }
    if ( !ReadLittle( input, y ) ) { return (ShapeFileReader::sheReadError); }
    size_t id = AddVertex( vxArray, x, y, vxIndexes );
    pt = ShapePoint( id, &vxArray );
    return (ShapeFileReader::sheOk);
  }

//-----------------------------------------------------------------------------

  static ShapeFileReader::ShapeReaderError ReadMOrZ( istream& input, VertexArray& vxArray, 
                                                     const Array< size_t >& vxIndexes, 
                                                     bool readZ, bool readRange )
  {
    if ( readRange )
    {
      double range[2];
      if ( !ReadLittle( input, range ) ) { return (ShapeFileReader::sheReadError); }
    }

    if ( readZ ) 
    {
      for ( int i = 0, cnt = vxIndexes.Size(); i < cnt; ++i ) 
      {
        if ( vxIndexes[i] != -1 )
        {
          DVertex vect = vxArray[vxIndexes[i]];
          if ( !ReadLittle( input, vect.z ) ) { return (ShapeFileReader::sheReadError); }
          vxArray.SetAt( vxIndexes[i], vect );
        }
        else
        {
          double unused;
          if ( !ReadLittle( input, unused ) ) { return (ShapeFileReader::sheReadError); }
        }
      }
    }
    else
    {
      for ( int i = 0, cnt = vxIndexes.Size(); i < cnt; ++i ) 
      {
        if ( vxIndexes[i] != -1 )
        {
          DVertex vect = vxArray[vxIndexes[i]];
          if ( !ReadLittle( input, vect.m ) ) { return (ShapeFileReader::sheReadError); }
          vxArray.SetAt( vxIndexes[i], vect );
        }
        else
        {
          double unused;
          if ( !ReadLittle( input, unused ) ) { return (ShapeFileReader::sheReadError); }
        }
      }
    }

    return (ShapeFileReader::sheOk);
  }

//-----------------------------------------------------------------------------

  static ShapeFileReader::ShapeReaderError ReadPointM( istream& input, VertexArray& vxArray, ShapePoint& pt )
  {
    AutoArray< size_t > vxIndexes;
    ShapeFileReader::ShapeReaderError err = ReadPoint( input, vxArray, pt, &vxIndexes );
    if ( err ) { return (err); }
    err=ReadMOrZ( input, vxArray, vxIndexes, false, false );
    return (err);
  }

//-----------------------------------------------------------------------------

  static ShapeFileReader::ShapeReaderError ReadPointZ( istream &input, VertexArray& vxArray, ShapePoint& pt )
  {
    AutoArray< size_t > vxIndexes;
    ShapeFileReader::ShapeReaderError err = ReadPoint( input, vxArray, pt, &vxIndexes );
    if ( err ) { return (err); }
    err = ReadMOrZ( input, vxArray, vxIndexes, true, false );
    if ( err ) { return (err); }
    err = ReadMOrZ( input, vxArray, vxIndexes, false, false );
    return (err);
  }

//-----------------------------------------------------------------------------

  static ShapeFileReader::ShapeReaderError ReadMultipoint( istream &input, VertexArray &vxArray, 
                                                           ShapeMultiPoint& mp, 
                                                           AutoArray< size_t >* vxIndexes = NULL )
  {
    double bb[4];
    if ( !ReadLittle( input, bb ) ) { return (ShapeFileReader::sheReadError); }
    unsigned long count;
    if ( !ReadLittle( input, count ) ) { return (ShapeFileReader::sheReadError); }
    AutoArray< size_t > vxlist;
    for ( unsigned long i = 0; i < count; ++i )
    {
      double x, y;
      if ( !ReadLittle( input, x) ) { return (ShapeFileReader::sheReadError); }
      if ( !ReadLittle( input, y) ) { return (ShapeFileReader::sheReadError); }
      vxlist.Add( AddVertex( vxArray, x, y, vxIndexes ) );
    }
    mp = ShapeMultiPoint( vxlist, &vxArray );
    return (ShapeFileReader::sheOk);
  }

//-----------------------------------------------------------------------------

  static ShapeFileReader::ShapeReaderError ReadMultipointM( istream& input, VertexArray& vxArray, 
                                                            ShapeMultiPoint& pt )
  {
    AutoArray< size_t > vxIndexes;
    ShapeFileReader::ShapeReaderError err = ReadMultipoint( input, vxArray, pt, &vxIndexes );
    if ( err ) { return (err); }
    err = ReadMOrZ( input, vxArray, vxIndexes, false, true );
    return (err);
  }

//-----------------------------------------------------------------------------

  static ShapeFileReader::ShapeReaderError ReadMultipointZ( istream& input, VertexArray& vxArray, ShapeMultiPoint& pt )
  {
    AutoArray< size_t > vxIndexes;
    ShapeFileReader::ShapeReaderError err = ReadMultipoint( input, vxArray, pt, &vxIndexes );
    if ( err ) { return (err); }
    err = ReadMOrZ( input, vxArray, vxIndexes, true, true );
    if ( err ) { return (err); }
    err = ReadMOrZ( input, vxArray, vxIndexes, false, true );
    return (err);
  }

//-----------------------------------------------------------------------------

  static ShapeFileReader::ShapeReaderError ReadPolyLine( istream &input, VertexArray& vxArray, ShapePoly& pl, 
                                                         AutoArray< size_t >* vxIndexes = NULL )
  {
    double box[4];
    if ( !ReadLittle( input, box ) ) { return (ShapeFileReader::sheReadError); }
    unsigned long numParts;
    unsigned long numPoints;
    AutoArray< size_t > vxlist;
    AutoArray< size_t > partlist;
    if ( !ReadLittle( input, numParts ) )  { return (ShapeFileReader::sheReadError); }
    if ( !ReadLittle( input, numPoints ) ) { return (ShapeFileReader::sheReadError); }
    partlist.Resize( numParts );
    vxlist.Resize( numPoints );
    for ( size_t i = 0; i < numParts; ++i )
    {
      unsigned long v;
      if ( !ReadLittle( input, v ) ) { return (ShapeFileReader::sheReadError); }
      partlist[i] = v;
    }
    for ( size_t i = 0; i < numPoints; ++i )
    {
      double x, y;
      if ( !ReadLittle( input, x ) ) { return (ShapeFileReader::sheReadError); }
      if ( !ReadLittle( input, y ) ) { return (ShapeFileReader::sheReadError); }
      vxlist[i] = AddVertex( vxArray, x, y, vxIndexes );
    }
    pl = ShapePoly( vxlist, partlist, &vxArray );
    return (ShapeFileReader::sheOk);
  }

//-----------------------------------------------------------------------------

  static ShapeFileReader::ShapeReaderError ReadPolyLineM( istream& input, VertexArray& vxArray, ShapePoly& pl )
  {
    AutoArray< size_t > vxIndexes;
    ShapeFileReader::ShapeReaderError err = ReadPolyLine( input, vxArray, pl, &vxIndexes );
    if ( err ) { return (err); }
    err=ReadMOrZ( input, vxArray, vxIndexes, false, true );
    return (err);
  }

//-----------------------------------------------------------------------------

  static ShapeFileReader::ShapeReaderError ReadPolyLineZ( istream& input, VertexArray& vxArray, ShapePoly& pl )
  {
    AutoArray< size_t > vxIndexes;
    ShapeFileReader::ShapeReaderError err = ReadPolyLine( input, vxArray, pl, &vxIndexes );
    if ( err ) { return (err); }
    err = ReadMOrZ( input, vxArray, vxIndexes, true, true );
    if ( err ) { return (err); }
    err = ReadMOrZ( input, vxArray, vxIndexes, false, true );
    return (err);
  }

//-----------------------------------------------------------------------------

  static ShapeFileReader::ShapeReaderError ReadPolygon( istream &input, VertexArray& vxArray, ShapePoly& pl, 
                                                        AutoArray< size_t >* vxIndexes = NULL )
  {
    double box[4];
    if ( !ReadLittle( input, box ) ) { return (ShapeFileReader::sheReadError); }
    unsigned long numParts;
    unsigned long numPoints;
    AutoArray< size_t > vxlist;
    AutoArray< size_t > partlist;
    if ( !ReadLittle( input, numParts ) )  { return (ShapeFileReader::sheReadError); }
    if ( !ReadLittle( input, numPoints ) ) { return (ShapeFileReader::sheReadError); }
    partlist.Resize( numParts );
    vxlist.Resize( numPoints - numParts );
    for ( size_t i = 0; i < numParts; ++i )
    {
      unsigned long v;
      if ( !ReadLittle( input, v ) ) { return (ShapeFileReader::sheReadError); }
      partlist[i] = v;
    }
    size_t nextPart = 1;
    for ( size_t i = 0; i < numPoints; ++i )
    {
      double x,y;
      if ( !ReadLittle( input, x ) ) { return (ShapeFileReader::sheReadError); }
      if ( !ReadLittle( input, y ) ) { return (ShapeFileReader::sheReadError); }

      //remove last point in the part, it must be the same as first.
      if ( (nextPart >= numParts && i + 1 == numPoints) || (nextPart < numParts && i + 1 == partlist[nextPart]) )     
      {
        nextPart++;    
        if ( vxIndexes ) { vxIndexes->Add( -1 ); }
      }
      else
      {
        vxlist[i - nextPart + 1] = AddVertex( vxArray, x, y, vxIndexes );
      }
    }
    //fix part indexes
    for ( size_t i = 0; i < numParts; ++i ) 
    {
      partlist[i] = partlist[i] - i;
    }
    pl = ShapePoly( vxlist, partlist, &vxArray );
    return (ShapeFileReader::sheOk);
  }

//-----------------------------------------------------------------------------

  static ShapeFileReader::ShapeReaderError ReadPolygonM( istream& input, VertexArray& vxArray, ShapePoly& pl )
  {
    AutoArray< size_t > vxIndexes;
    ShapeFileReader::ShapeReaderError err = ReadPolyLine( input, vxArray, pl, &vxIndexes );
    if ( err ) { return (err); }
    err = ReadMOrZ( input, vxArray, vxIndexes, false, true );
    return (err);
  }

//-----------------------------------------------------------------------------

  static ShapeFileReader::ShapeReaderError ReadPolygonZ( istream& input, VertexArray& vxArray, ShapePoly& pl )
  {
    AutoArray< size_t > vxIndexes;
    ShapeFileReader::ShapeReaderError err = ReadPolyLine( input, vxArray, pl, &vxIndexes );
    if ( err ) { return (err); }
    err = ReadMOrZ( input, vxArray, vxIndexes, true, true );
    if ( err ) { return (err); }
    err = ReadMOrZ( input, vxArray, vxIndexes, false, true );
    return (err);
  }

//-----------------------------------------------------------------------------

  ShapeFileReader::ShapeReaderError ShapeFileReader::ReadShapeFile( istream& input, VertexArray& vxArray, ShapeList& shapeList, 
                                                                    AutoArray< size_t >* indexList )
  {
    ShapeFileReader::ShapeReaderError res;

    res = ReadShapeHeader( input, m_header );
    if ( res ) { return (res); }
    if ( m_header.m_fileCode != 9994 ) { return (sheFileTagMismach); }
    if ( m_header.m_version != 1000 ) { return (sheVersionNotSupported); }

    unsigned long curSize = 100;  
    while ( curSize < m_header.m_fileLength * 2 )
    {
      RecordHeader rhdr;
      if ( !ReadRecordHeader( input, rhdr ) ) { return (sheUnexceptedEndOfFile); }

      IShape* shape = NULL;

      switch ( rhdr.m_shapeType )
      {
      case 0:break; //null shape
      case 1:
        {
          ShapePoint sh;
          res = ReadPoint( input, vxArray, sh );        
          if ( res ) { return (res); }
          shape = CreatePoint( sh );
        }
        break;
      case 3:
        {
          ShapePoly sh;
          res = ReadPolyLine( input, vxArray, sh );        
          if ( res ) { return (res); }
          shape = CreatePolyLine( sh );
        }
        break;
      case 5:
        {
          ShapePoly sh;
          res = ReadPolygon( input, vxArray, sh );
          if ( res ) { return (res); }
          shape = CreatePolygon( sh );
        }
        break;
      case 8:
        {
          ShapeMultiPoint sh;
          res = ReadMultipoint( input, vxArray, sh );
          if ( res ) { return (res); }
          shape = CreateMultipoint( sh );
        }
        break;
      case 11:
        {
          ShapePoint sh;
          res = ReadPointZ( input, vxArray, sh );        
          if ( res ) { return (res); }
          shape = CreatePoint( sh );
        }
        break;
      case 13:
        {
          ShapePoly sh;
          res = ReadPolyLineZ( input, vxArray, sh );        
          if ( res ) { return (res); }
          shape = CreatePolyLine( sh );
        }
        break;
      case 15:
        {
          ShapePoly sh;
          res = ReadPolygonZ( input, vxArray, sh );
          if ( res ) { return (res); }
          shape = CreatePolygon( sh );
        }
        break;
      case 18:
        {
          ShapeMultiPoint sh;
          res = ReadMultipointZ( input, vxArray, sh );
          if ( res ) { return (res); }
          shape = CreateMultipoint( sh );
        }
        break;
      case 21:
        {
          ShapePoint sh;
          res = ReadPointM( input, vxArray, sh );        
          if ( res ) { return (res); }
          shape = CreatePoint( sh );
        }
        break;
      case 23:
        {
          ShapePoly sh;
          res = ReadPolyLineM( input, vxArray, sh );        
          if ( res ) { return (res); }
          shape = CreatePolyLine( sh );
        }
        break;
      case 25:
        {
          ShapePoly sh;
          res = ReadPolygonM( input, vxArray, sh );
          if ( res ) { return (res); }
          shape = CreatePolygon( sh );
        }
        break;
      case 28:
        {
          ShapeMultiPoint sh;
          res = ReadMultipointM( input, vxArray, sh );
          if ( res ) { return (res); }
          shape = CreateMultipoint( sh );
        }
        break;
      }

      if ( !shape ) ///shape has not been created. SkipIt
      {
        unsigned long skipOffset = rhdr.m_contentLength * 2 - 4;
        if ( skipOffset )
        {
          AutoArray< char > buff;
          buff.Resize( skipOffset );
          input.read( buff.Data(), skipOffset );
          if ( !input ) { return (sheReadError); }
          if ( input.gcount() != skipOffset ) { return (sheUnexceptedEndOfFile); }
          curSize += (skipOffset + 12);
        }  
      }
      else
      {
        unsigned long skipOffset = rhdr.m_contentLength * 2;
        unsigned long recordNumber = rhdr.m_recordNumber - 1;
        unsigned long shapeId = shapeList.AddShape( PShape( shape ) );
        if ( indexList )
        {
          while ( (unsigned)indexList->Size() < recordNumber ) 
          {
            indexList->Add( -1 );
          }
          indexList->Add( shapeId );
        }   
        curSize += (skipOffset + 8);
      }    
    }

    return (sheOk);
  }

//-----------------------------------------------------------------------------

  IShape* ShapeFileReader::CreatePoint( const ShapePoint& origShape )
  {
    return (new ShapePoint( origShape ));
  }

//-----------------------------------------------------------------------------

  IShape* ShapeFileReader::CreateMultipoint( const ShapeMultiPoint& origShape )
  {
    return (new ShapeMultiPoint( origShape ));
  }

//-----------------------------------------------------------------------------

  IShape* ShapeFileReader::CreatePolyLine( const ShapePoly& origShape )
  {
    return (new ShapePolyLine( origShape ));
  }

//-----------------------------------------------------------------------------

  IShape* ShapeFileReader::CreatePolygon( const ShapePoly& origShape )
  {
    return (new ShapePolygon( origShape ));
  }

//-----------------------------------------------------------------------------

  ShapeFileReader::ShapeReaderError ShapeFileReader::ReadShapeFile( const _TCHAR* filename, VertexArray& vxArray, ShapeList& shapeList, 
                                                                    AutoArray< size_t >* indexList )
  {
    ifstream in( filename,ios::in | ios::binary );
    if ( !in ) { return (sheOpenError); }
    return (ReadShapeFile( in, vxArray, shapeList, indexList ));
  }

//-----------------------------------------------------------------------------

} // namespace Shapes
