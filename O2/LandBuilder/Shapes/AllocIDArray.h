#pragma once

#include "LargeAutoArray.h"
#include <Es/Common/langExt.hpp>

// -------------------------------------------------------------------------- //

typedef InitVal<unsigned long, 0> InitUnsignedZeroed;

TypeIsMovable(InitUnsignedZeroed)

// -------------------------------------------------------------------------- //

template<class T, int blockSz>
class AllocIDArray : public LargeAutoArray<T, blockSz>
{
	int _firstUnused;
	AutoArray<InitUnsignedZeroed> _unusedMask;

	// -------------------------------------------------------------------------- //

	static int GetBitMaskPos(int pos) 
	{
		return pos >> 5;
	}

	// -------------------------------------------------------------------------- //

	static unsigned long GetBitMask(int pos) 
	{
		return 1 << (pos & 31);
	}

	// -------------------------------------------------------------------------- //

	int FindNextUnused(int from)
	{
		int total = _unusedMask.Size() * 32;
		for (int i = from; i < total; i++)
		{
			if ((i & 31) == 0 && _unusedMask[GetBitMaskPos(i)] == 0)
			{
				i += 31;
				continue;
			}
			else
			{
				if (_unusedMask[GetBitMaskPos(i)] & GetBitMask(i))
				{
					return i;
				}
			}
		}
		return -1;
	}

	// -------------------------------------------------------------------------- //
public:
	AllocIDArray(void)
	: _firstUnused(-1)  
	{
	}

	// -------------------------------------------------------------------------- //

	int Add(const T& item)
	{
		if (_firstUnused == -1)
		{
			return LargeAutoArray<T, blockSz>::Add(item);
		}
		else
		{
			(*this)[_firstUnused] = item;
			int res = _firstUnused;
			_unusedMask[GetBitMaskPos(_firstUnused)] &= ~GetBitMask(_firstUnused);
			_firstUnused = FindNextUnused(_firstUnused);
			return res;
		}
	}

	// -------------------------------------------------------------------------- //
	
	bool Delete(int pos)
	{
		if (pos >= 0 && pos + 1 < Size())
		{
			(*this)[pos] = T();
			_unusedMask.Access(GetBitMaskPos(pos));
			_unusedMask[GetBitMaskPos(pos)] |= GetBitMask(pos);
			if (pos < _firstUnused || _firstUnused == -1) _firstUnused = pos;
			return true;
		}
		else if (pos + 1 == Size())
		{
			LargeAutoArray<T, blockSz>::Delete(pos, 1);
			_unusedMask.Resize(pos / 32 + 1);
			return true;
		}
		else
		{
			return false;
		}
	}

	// -------------------------------------------------------------------------- //

	bool IsIdValid(int id) const
	{
		if (id < 0) return false;
		if (id >= Size()) return false;
		if (GetBitMaskPos(id) >= _unusedMask.Size()) return true;
		if (_unusedMask[GetBitMaskPos(id)] & GetBitMask(id)) return false;
		return true;
	}
};