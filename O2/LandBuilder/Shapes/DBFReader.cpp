//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "ShapeDatabase.h"
#include ".\dbfreader.h"
#include <fstream>

//-----------------------------------------------------------------------------

namespace Shapes
{

//-----------------------------------------------------------------------------

	using namespace std;

//-----------------------------------------------------------------------------

	DBFReader::ReaderError DBFReader::ReadDBF( std::istream& input, ShapeDatabase& database, 
                                             const Array< size_t >& shapeIndexes, IDBColumnNameConvert* t )
	{
		input.read( (char*)&m_header, sizeof( m_header ) );
    if ( !input || input.gcount() != sizeof( m_header ) ) { return (errHeaderReadError); }

		m_fieldInfo.Clear();
		unsigned char mark;
		input.read( (char*)&mark, 1 );
    if ( !input || input.gcount() != 1 ) { return (errHeaderReadError); }

    while ( mark != 0x0D )
		{
			FieldInfo finfo;
			finfo.fieldName[0] = mark;
			input.read( (char*)&finfo + 1, sizeof( finfo ) - 1 );
      if ( !input || input.gcount() != sizeof( finfo ) - 1 ) { return (errHeaderReadError); }
			input.read( (char*)&mark, 1 );
      if ( !input || input.gcount() != 1 ) { return (errHeaderReadError); }
			finfo.fieldName[10] = 0;
			int id = m_fieldInfo.Size();
			m_fieldInfo.Add( finfo );
			if ( t ) 
			{
				m_fieldTranslated.Add( t->TranslateColumnName( m_fieldInfo[id].fieldName ) );
			}
			else 
			{
				m_fieldTranslated.Add( m_fieldInfo[id].fieldName );
			}
		}
		input.seekg( m_header.headersz );
		int totalRecords = __min( m_header.records, (unsigned)shapeIndexes.Size() );
		char* buffer = (char*)alloca( __max( m_header.reclen, 255 ) + 1 );
		int indexpos = 0;
		for ( int i = 0; i < totalRecords; ++i )
		{
			char delmark;
			input.read( &delmark, 1 );
      if ( !input || input.gcount() != 1 ) { return (errErrorReadingData); }
			if ( delmark == ' ' )
			{      
				for ( int j = 0, cnt = m_fieldInfo.Size(); j < cnt; ++j )
				{
					input.read( buffer, m_fieldInfo[j].length );
          if ( !input || input.gcount() != m_fieldInfo[j].length ) { return (errErrorReadingData); }
					buffer[m_fieldInfo[j].length] = 0;
					switch ( m_fieldInfo[j].fieldType )
					{
					case 'B':
					case 'M':
					case 'G': buffer[0] = 0; break; //NO VALUE, this type is not supported
					case '@': buffer[0] = 0; break; //NO VALUE, this type is not supported
					case '+':
					case '|':
						{
							long* l = reinterpret_cast< long* >( buffer );
              if ( *l & 0x80000000 ) { *l = -(*l & 0x7FFFFFFF); }
							_ltoa( *l, buffer, 10 );
						}
						break;
					case 'O':
						{
							double* l = reinterpret_cast< double* >( buffer );
							sprintf( buffer, "%g", *l );
						}
						break;
					}
          while ( buffer[0] == ' ' ) { strcpy( buffer, buffer + 1 ); }
					int l = strlen( buffer );
          while ( l > 0 && buffer[l - 1] == ' ' ) { buffer[--l] = 0; }

					size_t shapeId = shapeIndexes[indexpos];
					if ( shapeId != -1 )
					{
						database.SetField( shapeId, m_fieldTranslated[j], buffer );
					}
				}
				indexpos++;
			}
		}
		return (errOk);  
	}

//-----------------------------------------------------------------------------

	DBFReader::ReaderError DBFReader::ReadDBF( const char* fname, ShapeDatabase& database, 
                                             const Array< size_t >& shapeIndexes, IDBColumnNameConvert* t )
	{
		ifstream input( fname, ios::in | ios::binary );
    if ( !input ) { return (errFileOpenError); }
		return (ReadDBF( input, database, shapeIndexes, t ));
	}

//-----------------------------------------------------------------------------

} // namespace Shapes