//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include ".\ShapeHelpers.h"

//-----------------------------------------------------------------------------

RString GetShapeType( const Shapes::IShape* shape )
{
	const Shapes::ShapePolygon* shapePolygon = dynamic_cast< const Shapes::ShapePolygon* >( shape );
	if ( shapePolygon ) { return (SHAPE_NAME_POLYGON); }

	const Shapes::ShapePolyLine* shapePolyline = dynamic_cast< const Shapes::ShapePolyLine* >( shape );
  if ( shapePolyline ) { return (SHAPE_NAME_POLYLINE); }

	const Shapes::ShapePoint* shapePoint = dynamic_cast< const Shapes::ShapePoint* >( shape );
	if ( shapePoint ) { return (SHAPE_NAME_POINT); }

	const Shapes::ShapeMultiPoint* shapeMultiPoint = dynamic_cast< const Shapes::ShapeMultiPoint* >( shape );
	if ( shapeMultiPoint ) { return (SHAPE_NAME_MULTIPOINT); }

	return ("");
}

//-----------------------------------------------------------------------------

double GetShapePerimeter( const Shapes::IShape* shape )
{
	RString type = GetShapeType( shape );
  if ( type == SHAPE_NAME_POINT )      { return (0.0); }
  if ( type == SHAPE_NAME_MULTIPOINT ) { return (0.0); }

	int vCount = shape->GetVertexCount();

	double perimeter = 0.0f;

	for ( int i = 0; i < (vCount - 1); ++i )
	{
		const Shapes::DVertex& v1 = shape->GetVertex( i );
		const Shapes::DVertex& v2 = shape->GetVertex( i + 1 );
		perimeter += sqrt( v1.DistanceXY2( v2 ) );
	}

	if ( type )
	{
		if ( type == SHAPE_NAME_POLYGON )
		{
			// closes the polygon
			const Shapes::DVertex& v1 = shape->GetVertex( 0 );
			const Shapes::DVertex& v2 = shape->GetVertex( vCount - 1 );
			perimeter += sqrt( v1.DistanceXY2( v2 ) );
		}
	}
	return (perimeter);
}

//-----------------------------------------------------------------------------

double GetShapeArea( const Shapes::IShape* shape )
{
	RString type = GetShapeType( shape );
	if ( type == SHAPE_NAME_POLYGON )
	{
		// gets the number of vertices
		int numVertices = shape->GetVertexCount();
		
		double area = 0.0;

		// calculates area (gauss algorithm)
		for ( int i = 0, j = (numVertices - 1); i < numVertices; j = i++ )
		{
			const Shapes::DVertex& v1 = shape->GetVertex( j );
			const Shapes::DVertex& v2 = shape->GetVertex( i );

			area += v1.x * v2.y - v1.y * v2.x;
		}
		area /= 2.0;

		area = (area > 0.0) ? area : -area;

		return (area);
	}
	else
	{
		return (0.0);
	}
}

//-----------------------------------------------------------------------------

Shapes::DVector NearestToEdge( const Shapes::IShape* shape, const Shapes::DVector& vx )
{
	RString type = GetShapeType( shape );
	if( type == SHAPE_NAME_POLYGON )
	{
		const Shapes::ShapePolygon* shapePolygon = dynamic_cast< const Shapes::ShapePolygon* >( shape );		
		return (shapePolygon->NearestToEdge( vx, true ));
	}
	else if(type == SHAPE_NAME_POLYLINE)
	{
		const Shapes::ShapePolyLine* shapePolyline = dynamic_cast< const Shapes::ShapePolyLine* >( shape );		
		return (shapePolyline->NearestToEdge( vx, false ));
	}
	return (Shapes::DVector( 0.0, 0.0, 0.0 ));
}

//-----------------------------------------------------------------------------

double RandomInRangeF( double min, double max )
{
	return (min + (static_cast< double >( rand() ) / static_cast< double >( RAND_MAX )) * (max - min));
}

//-----------------------------------------------------------------------------

int RandomInRangeI( int min, int max )
{
	return (min + static_cast< double >( RandomInRangeF( min, max ) ));
}

//-----------------------------------------------------------------------------

Shapes::DVector GetPointOnShapeAt( const Shapes::IShape* shape, double longCoordinate )
{
	Shapes::DVector vNull( 0.0, 0.0 );

	RString type = GetShapeType( shape );
  if ( type == SHAPE_NAME_POINT )      { return (vNull); }
  if ( type == SHAPE_NAME_MULTIPOINT ) { return (vNull); }

	size_t numVertices = shape->GetVertexCount();
  if ( numVertices == 0 ) { return (vNull); }

	double shapeLength = GetShapePerimeter( shape );

  if ( (longCoordinate < 0.0) || (shapeLength < longCoordinate) ) { return (vNull); }

	if ( longCoordinate == 0.0 )
	{
		Shapes::DVertex v = shape->GetVertex( 0 );
		return (Shapes::DVector( v.x, v.y ));
	}

	if ( longCoordinate == shapeLength )
	{
		if ( type == SHAPE_NAME_POLYLINE )
		{
			Shapes::DVertex v = shape->GetVertex( numVertices - 1 );
			return (Shapes::DVector( v.x, v.y ));
		}
		else if ( type == SHAPE_NAME_POLYGON )
		{
			Shapes::DVertex v = shape->GetVertex( 0 );
			return (Shapes::DVector( v.x, v.y ));
		}
	}

	double tempLength = 0.0;
	for ( size_t i = 1; i < numVertices; ++i )
	{
		Shapes::DVertex v0 = shape->GetVertex( i - 1 );
		Shapes::DVertex v1 = shape->GetVertex( i );
		Shapes::DVector v01( v1, v0 );

		double d = v01.SizeXY();
		if ( tempLength + d > longCoordinate )
		{
			double pos = longCoordinate - tempLength;
			double angle = v01.DirectionXY();
			double x = v0.x + pos * cos( angle );
			double y = v0.y + pos * sin( angle );
			return (Shapes::DVector( x, y ));
		}
		tempLength += d;
	}

	// we arrive here only if the shape is a polygon
	Shapes::DVertex v0 = shape->GetVertex( numVertices - 1 );
	Shapes::DVertex v1 = shape->GetVertex( 0 );
	Shapes::DVector v01( v1, v0 );
	double pos = longCoordinate - tempLength;
	double angle = v01.DirectionXY();
	double x = v0.x + pos * cos( angle );
	double y = v0.y + pos * sin( angle );
	return (Shapes::DVector( x, y ));
}

//-----------------------------------------------------------------------------
