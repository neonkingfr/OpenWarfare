#include "StdAfx.h"
#include ".\processor.h"
#include "../Shapes/ShapeNull.h"

namespace LandBuildInt
{
	Processor::Processor(IModuleFactory *modFactory):_core(modFactory)
	{
	}

	Processor::~Processor(void)
	{
	}

	FloatRect Processor::LoadRectangle(ParamClassPtr cls)
	{
		FloatRect res(0, 0, 0, 0);
		ParamEntryPtr entry;
		entry = cls->FindEntry("left");
		if (entry.NotNull()) res.left = entry->operator float();

		entry = cls->FindEntry("top");
		if (entry.NotNull()) res.top = entry->operator float();

		entry = cls->FindEntry("right");
		if (entry.NotNull()) res.right = entry->operator float();

		entry = cls->FindEntry("bottom");
		if (entry.NotNull()) res.bottom = entry->operator float();

		return res;
	}

	void Processor::LoadTranslationTable(ParamEntryPtr entry, int type)
	{
		int sz = entry->GetSize();
		for (int i = 0; i < sz; i++)
		{
			RString val = entry->operator [](i).operator RStringB();
			char* c = val.MutableData();
			char* d = strstr(c, "->");
			if (d != 0)
			{
				*d = 0;
				d += 2;
				_core.AddResource(type, c, d);
			}
		}
	}

	bool Processor::LoadProject(const char* configName)
	{
		printf("Processing project: %s\n", configName);

		Pathname::SetCWD(Pathname(configName));

		LSError err = _config.Parse(configName);
		if (err != LSOK)
		{
			printf("Parse error: %d\n", err);
			return false;
		}
		ParamEntryPtr entry;
		entry = _config.FindEntry("LandBuildConfig");
		if (entry.NotNull() && entry->IsClass())
		{
			ParamClassPtr cfgClass = entry->GetClassInterface();
			entry = cfgClass->FindEntry("MapArea");
			if (entry.NotNull() && entry->IsClass())
			{
				FloatRect rect = LoadRectangle(entry->GetClassInterface());
				_core.SetMapArea(rect);

				FloatRect shapeArea(0, 0, 0, 0);
				entry = cfgClass->FindEntry("ShapeArea");
				if (entry.NotNull() && entry->IsClass())
				{
					shapeArea = LoadRectangle(entry->GetClassInterface());
				}

				entry = cfgClass->FindEntry("HighMapGrid");
				if (entry.NotNull() && entry->IsClass())
				{
					ParamEntryPtr x, y;
					x = entry->FindEntry("x");
					y = entry->FindEntry("y");
					if (y.IsNull()) y = x;
					if (x.IsNull()) x = y;
					if (x.NotNull() && y.NotNull())
					{
						_core.SetOFPSegmentSize(*x, *y);
					}

				}

				entry = cfgClass->FindEntry("TaskList");
				if (entry.NotNull() && entry->IsArray())
				{
					for (int i = 0; i < entry->GetSize(); i++)
					{
						RStringB val = entry->operator [](i);
						char* splt = const_cast<char*>(strchr(val.MutableData(), '@'));
						Pathname taskPath(configName);
						Pathname shapePath(configName);
						Pathname dbfName(PathNull);
						if (splt == 0)
						{
							taskPath.SetFiletitle(val);
							shapePath.SetFiletitle(val);
						}
						else
						{
							*splt = 0;
							taskPath.SetFiletitle(val);
							splt++;
							if (*splt) 
							{
								char* splt2 = const_cast<char*>(strchr(splt, '@'));
								if (splt2)
								{
									*splt2 = 0;
									splt2++;
									dbfName = configName;
									dbfName.SetFiletitle(splt2);
									dbfName.SetExtension(".dbf");
								}
								shapePath.SetFiletitle(splt);
							}
							else 
							{
								shapePath.SetNull();
							}
						}
						if (!shapePath.IsNull()) shapePath.SetExtension(".shp");
						LoadTask(taskPath,shapePath);
					}
				}

				entry = cfgClass->FindEntry("resourceTransl");
				if (entry.NotNull() && entry->IsArray()) LoadTranslationTable(entry, 0);

				if (shapeArea.left == 0 && shapeArea.right == 0 || 
					shapeArea.top == 0 && shapeArea.bottom == 0)
				{
					Shapes::DBox bbox = _core.GetShapeOrganizer().CalcBoundingBox();
					if (shapeArea.left == 0) 
					{
						shapeArea.left  = (float)bbox.lo.x;
						shapeArea.right = (float)bbox.hi.x;
					}
					if (shapeArea.top == 0) 
					{
						shapeArea.top    = (float)bbox.lo.y;
						shapeArea.bottom = (float)bbox.hi.y;
					}
				}
				Matrix4 shapeTransform = Matrix4P(MTranslation, Vector3(rect.left, rect.top, 0)) *
										 Matrix4P(MScale, (rect.right - rect.left) / (shapeArea.right - shapeArea.left), (rect.bottom - rect.top) / (shapeArea.bottom - shapeArea.top), 1) *
										 Matrix4P(MTranslation, Vector3(-shapeArea.left, -shapeArea.top, 0));
				_core.GetShapeOrganizer().TransformVertices(shapeTransform);
				return true;
			}
			else
			{
				puts("Missing MapArea class");
			}
		} 
		else
		{
			puts("Missing LandBuildConfig class");
		}
		return false;
	}

	namespace Functors
	{
		class LoadConstantParams
		{
			unsigned int cid;
			mutable Shapes::ShapeOrganizer &org;
		public:
			LoadConstantParams(Shapes::ShapeOrganizer& org, unsigned int cid) : org(org), cid(cid) {}

			bool operator()(const ParamEntryVal& entry) const
			{
				org.SetConstantParam(cid, entry.GetName(), entry.GetValue());
				return false;    
			}
		};
	};

	void Processor::LoadTask(const Pathname& configName, const Pathname& shapeName, const Pathname& dbfName)
	{
		printf("loading: %s\n", configName.GetFilename());
		ParamFile taskCfg;
		LSError err = taskCfg.Parse(configName);
		if (err != LSOK)
		{
			printf("!Parse error: %d\n", err);
			return;
		}
		Shapes::ShapeOrganizer& org = _core.GetShapeOrganizer();
		ParamEntryPtr entry = taskCfg.FindEntry("TaskConfig");
		if (entry.NotNull() && entry->IsClass())
		{
			ParamClassPtr tcfg = entry->GetClassInterface();
			entry = tcfg->FindEntry("module");
			if (entry.NotNull() && entry->IsTextValue())
			{
				RStringB modname = entry->GetValue();
				unsigned int cid = org.AllocateNewConstantParamsID(modname);
				entry = tcfg->FindEntry("parameters")        ;
				if (entry.NotNull() && entry->IsClass())
				{
					ParamClassPtr params = entry->GetClassInterface();
					params->ForEachEntry(Functors::LoadConstantParams(org, cid));
				}
				entry = tcfg->FindEntry("columnTransl");
				if (entry.NotNull() && entry->IsArray())
				{
					LoadTranslationTable(entry, 1);
				}
				if (shapeName.IsNull())
				{
					org.AddShape(new Shapes::ShapeNull(0), cid);
				}
				else
				{
					if (org.ReadShapeFile(shapeName, cid, &_core, dbfName))
					{            
					}
					else
					{
						printf("!Unable to load shape: %s\r\n",configName.GetFullPath());
					}
				}
			}
			else
			{
				puts("!missing 'module' entry");
			}
		}
		else
		{
			puts("!Each config for shape file must have TaskConfig class");
		}
		_core.FlushColumnTranslateTable();
	}
}