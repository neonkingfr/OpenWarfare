#pragma once

//-----------------------------------------------------------------------------

#include "IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>
#include "..\Shapes\ShapeHelpers.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{
		class OnPlacePlacer : public IBuildModule
		{
		public:
			struct DbfInput
			{
				RString name;
				double orientation;
				double scaleX;
				double scaleY;
				double scaleZ;
			};

		public:
			virtual void Run( IMainCommands* );
			ModuleObjectOutputArray* RunAsPreview( const Shapes::IShape* pShape, DbfInput& dbfIn );

			class Exception: public ExceptReg::IException
			{
				unsigned int code;
				const char *text;

			public:
				Exception( unsigned int code, const char* text ) 
        : code( code )
        , text( text ) 
        {
        }

				virtual unsigned int GetCode() const 
        { 
          return (code); 
        }

				virtual const _TCHAR* GetDesc() const 
        { 
          return (text); 
        }

				virtual const _TCHAR* GetModule() const 
        { 
          return ("OnPlacePlacer"); 
        }

				virtual const _TCHAR* GetType() const 
        { 
          return ("OnPlacePlacerException"); 
        }
			};

		private:
			const Shapes::IShape* m_pCurrentShape;   
			DbfInput              m_dbfIn;

			ModuleObjectOutputArray m_objectsOut;

			bool GetDbfParameters( IMainCommands* pCmdIfc );
			void CreateObjects();
			void ExportCreatedObjects( IMainCommands* pCmdIfc );
			void WriteReport();
		};
	}
}

//-----------------------------------------------------------------------------
