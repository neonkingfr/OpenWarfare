#pragma once
#include "ibuildmodule.h"
#include <el/Evaluator/express.hpp>

class O2ScriptClass;


namespace LandBuildInt
{
  namespace Modules
  {


    class ScriptModule :  public IBuildModule, public GameState
    {
/*
#if USE_PRECOMPILATION
      GameCodeType _code;      
#else
      RString _code;
#endif
*/
      RString _code;
      
      IMainCommands *_cmdIfc;

    public:
      bool LoadScript(const char *script);
      virtual void Run(IMainCommands *cmdIfc);



      class ErrorRep: public ExceptReg::IException
      {
        RString exceptionText;
        unsigned int code;

      public:
        ErrorRep(const char *text, unsigned int code):exceptionText(text),code(code) {}
        virtual unsigned int GetCode() const {return code;}
        virtual const _TCHAR *GetDesc() const {return exceptionText;}
        virtual const _TCHAR *GetModule() const {return "ScriptModule";}
        virtual const _TCHAR *GetType() const {return "ScriptModule error";}
      };

      static ScriptModule &Instance(GameState &gs) {return static_cast<ScriptModule &>(gs);}
      static const ScriptModule &Instance(const GameState &gs) {return static_cast<const ScriptModule &>(gs);}

      static IMainCommands *CmdIfc(GameState &gs) {return Instance(gs)._cmdIfc;}
      static IMainCommands *CmdIfc(const GameState &gs) {return Instance(const_cast<GameState &>(gs))._cmdIfc;}
      
      ProgressBar<float> *_progress;
  
    };

  }
}
