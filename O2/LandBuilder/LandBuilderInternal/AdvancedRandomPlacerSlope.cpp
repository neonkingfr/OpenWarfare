#include "StdAfx.h"
#include <algorithm>
#include ".\AdvancedRandomPlacerSlope.h"

namespace LandBuildInt
{
	namespace Modules
	{
		// --------------------------------------------------------------------------------

		AdvancedRandomPlacerSlope::AdvancedRandomPlacerSlope()
		{
			m_MapLoaded = false;
		}

		// --------------------------------------------------------------------------------

		void AdvancedRandomPlacerSlope::Run(IMainCommands* cmdIfc)
		{
			printf("Started a new shape                   \n");
			printf("--------------------------------------\n");

			// gets shape and its properties
			m_pCurrentShape = cmdIfc->GetActiveShape();      

			// initializes const variables
			sDTED2        = "DTED2";
			sUSGSDEM      = "USGSDEM";
			sARCINFOASCII = "ARCINFOASCII";
			sXYZ          = "XYZ";

			// gets shape geometric data
			if (GetShapeGeoData())
			{
				// gets parameters from calling cfg file
				if (GetGlobalParameters(cmdIfc))
				{
					// gets dbf parameters from calling cfg file
					if (GetDbfParameters(cmdIfc))
					{
						// gets objects parameters from calling cfg file
						if(GetObjectsParameters(cmdIfc))
						{
							// normalizes probabilities
							if (NormalizeProbs())
							{
								if (!m_MapLoaded) // to read the map only once
								{
									// gets maps parameters from calling cfg file
									if(!GetMapParameters(cmdIfc)) return;
									// load maps
									LoadHighMap();
								}

								// initializes random seed
								srand(m_GlobalIn.randomSeed);

								// calculates the number of individual to be generated
								// (this value is referred to the bounding box of the shape
								//  and assume a uniform distribution - random generated values
								//  will be filtered to obtain the same density inside the shape,
								//  discarding the ones which will fall out of the shape itself)
								m_NumberOfIndividuals = static_cast<int>(m_ShpGeo.areaBBHectares * m_DbfIn.hectareDensity);

								// sets the population
								CreateObjects(false);

								// export objects
								ExportCreatedObjects(cmdIfc);

								// writes the report
								WriteReport();
							}
						}
					}
				}
			}       
		}

		// --------------------------------------------------------------------//

		ModuleObjectOutputArray* AdvancedRandomPlacerSlope::RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn, MapInput& mapIn,
																		 AutoArray<AdvancedRandomPlacerSlope::ObjectInput, MemAllocStack<AdvancedRandomPlacerSlope::ObjectInput, 32> >& objectsIn)
		{
			m_pCurrentShape = shape;
			m_GlobalIn      = globalIn;
			m_DbfIn         = dbfIn;
			m_MapIn         = mapIn;
			m_ObjectsIn     = objectsIn;

			if(m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();

			// initializes const variables
			sDTED2        = "DTED2";
			sUSGSDEM      = "USGSDEM";
			sARCINFOASCII = "ARCINFOASCII";
			sXYZ          = "XYZ";

			// gets shape geometric data
			if (GetShapeGeoData())
			{
				// normalizes probabilities
				if (NormalizeProbs())
				{
					if (!m_MapLoaded) // to read the map only once
					{
						// load maps
						LoadHighMap();
					}
					else
					{
						m_MapIn.hmData = m_MapData;
					}

					// initializes random seed
					srand(m_GlobalIn.randomSeed);

					// calculates the number of individual to be generated
					// (this value is referred to the bounding box of the shape
					//  and assume a uniform distribution - random generated values
					//  will be filtered to obtain the same density inside the shape,
					//  discarding the ones which will fall out of the shape itself)
					m_NumberOfIndividuals = static_cast<int>(m_ShpGeo.areaBBHectares * m_DbfIn.hectareDensity);

					// sets the population
					CreateObjects(true);

					return &m_ObjectsOut;
				}
				else
				{
					return NULL;
				}
			}
			else
			{
				return NULL;
			}
		}

		// --------------------------------------------------------------------//

		bool AdvancedRandomPlacerSlope::GetShapeGeoData()
		{
			Shapes::DBox bBox = m_pCurrentShape->CalcBoundingBox();

			m_ShpGeo.minX = bBox.lo.x;
			m_ShpGeo.minY = bBox.lo.y;
			m_ShpGeo.maxX = bBox.hi.x;
			m_ShpGeo.maxY = bBox.hi.y;

			m_ShpGeo.area         = GetShapeArea(m_pCurrentShape);
			m_ShpGeo.areaHectares = m_ShpGeo.area / 10000.0;

			m_ShpGeo.widthBB        = m_ShpGeo.maxX - m_ShpGeo.minX;
			m_ShpGeo.heightBB       = m_ShpGeo.maxY - m_ShpGeo.minY;
			m_ShpGeo.areaBB         = m_ShpGeo.widthBB * m_ShpGeo.heightBB;
			m_ShpGeo.areaBBHectares = m_ShpGeo.areaBB / 10000.0;

			return true;
		}

		// --------------------------------------------------------------------------------

		bool AdvancedRandomPlacerSlope::GetGlobalParameters(IMainCommands* cmdIfc)
		{
			const char* x = cmdIfc->QueryValue("randomSeed", false);
			if (x != 0) 
			{
				if (IsInteger(string(x), false))
				{
					m_GlobalIn.randomSeed = static_cast<unsigned int>(atoi(x));
				}
				else
				{
					ExceptReg::RegExcpt(Exception(11, ">>> Bad data for randomSeed <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(12, ">>> Missing randomSeed value <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool AdvancedRandomPlacerSlope::GetDbfParameters(IMainCommands* cmdIfc)
		{
			m_DbfIn.hectareDensity = 100.0; // default value
			const char* x = cmdIfc->QueryValue("hectareDensity", false);
			if (x != 0) 
			{
				if (IsFloatingPoint(string(x), false))
				{
					m_DbfIn.hectareDensity = atof(x);
				}
				else
				{
					printf(">>> ------------------------------------------------------- <<<\n");
					printf(">>> Bad data for hectareDensity - using default value (100) <<<\n");
					printf(">>> ------------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> -------------------------------------------------------- <<<\n");
				printf(">>> Missing hectareDensity value - using default value (100) <<<\n");
				printf(">>> -------------------------------------------------------- <<<\n");
			}
			if (m_DbfIn.hectareDensity < 0.01)
			{
				ExceptReg::RegExcpt(Exception(21, ">>> hectareDensity is too low (less then 0.01) <<<"));
				return false;
			}
			return true;
		}

		// --------------------------------------------------------------------------------

		bool AdvancedRandomPlacerSlope::GetObjectsParameters(IMainCommands* cmdIfc)
		{
			// resets arrays
			if (m_ObjectsIn.Size() != 0)
			{
				m_ObjectsIn.Clear();
				m_ObjectsOut.Clear();
			}

			int counter = 1;
			do 
			{
				char object[50], minheight[50], maxheight[50], mindist[50], minslope[50], maxslope[50], prob[50];
				sprintf_s(object,    "object%d",    counter);
				sprintf_s(minheight, "minheight%d", counter);
				sprintf_s(maxheight, "maxheight%d", counter);
				sprintf_s(mindist,   "mindist%d",   counter);
				sprintf_s(minslope,  "minslope%d",  counter);
				sprintf_s(maxslope,  "maxslope%d",  counter);
				sprintf_s(prob,      "prob%d",      counter);

				ObjectInput nfo;

				// object type
				const char* x = cmdIfc->QueryValue(object, false);
				if (x == 0) break;
				nfo.m_Name = x;
        
				// object min height
				x = cmdIfc->QueryValue(minheight, false);
				nfo.m_MinHeight = 100.0; // default value
				if (x != 0)
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_MinHeight = atof(x);
					}
					else
					{
						printf(">>> -------------------------------------------------- <<<\n");
						printf(">>> Bad data for minheight - using default value (100) <<<\n");
						printf(">>> -------------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> --------------------------------------------------- <<<\n");
					printf(">>> Missing minheight value - using default value (100) <<<\n");
					printf(">>> --------------------------------------------------- <<<\n");
				}

				// object max height
				x = cmdIfc->QueryValue(maxheight, false);
				nfo.m_MaxHeight = 100.0; // default value
				if (x != 0)
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_MaxHeight = atof(x);
					}
					else
					{
						printf(">>> -------------------------------------------------- <<<\n");
						printf(">>> Bad data for maxheight - using default value (100) <<<\n");
						printf(">>> -------------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> --------------------------------------------------- <<<\n");
					printf(">>> Missing maxheight value - using default value (100) <<<\n");
					printf(">>> --------------------------------------------------- <<<\n");
				}
        
				// object min distance
				x = cmdIfc->QueryValue(mindist, false);
				nfo.m_MinDistance = 5.0; // default value
				if (x != 0) 
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_MinDistance = atof(x);
					}
					else
					{
						printf(">>> ------------------------------------------------ <<<\n");
						printf(">>> Bad data for mindist - using default value (5.0) <<<\n");
						printf(">>> ------------------------------------------------ <<<\n");
					}
				}
				else
				{
					printf(">>> ----------------------------------------------------- <<<\n");
					printf(">>> Missing value for mindist - using default value (5.0) <<<\n");
					printf(">>> ----------------------------------------------------- <<<\n");
				}

				// object min slope
				x = cmdIfc->QueryValue(minslope, false);
				if (x != 0)
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_MinSlope = atof(x);
					}
					else
					{
						ExceptReg::RegExcpt(Exception(35, ">>> Bad data for minslope value <<<"));
						return false;
					}
				}
				else
				{
					ExceptReg::RegExcpt(Exception(36, ">>> Missing minslope value <<<"));
					return false;
				}
				
				// object max slope
				x = cmdIfc->QueryValue(maxslope, false);
				if (x != 0)
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_MaxSlope = atof(x);
					}
					else
					{
						ExceptReg::RegExcpt(Exception(37, ">>> Bad data for maxslope value <<<"));
						return false;
					}
				}
				else
				{
					ExceptReg::RegExcpt(Exception(38, ">>> Missing maxslope value <<<"));
					return false;
				}

				// object prob
				nfo.m_Prob = 100.0; // default value
				x = cmdIfc->QueryValue(prob, false);
				if (x != 0)
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_Prob = atof(x);
					}
					else
					{
						printf(">>> --------------------------------------------- <<<\n");
						printf(">>> Bad data for prob - using default value (100) <<<\n");
						printf(">>> --------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> -------------------------------------------------- <<<\n");
					printf(">>> Missing value for prob - using default value (100) <<<\n");
					printf(">>> -------------------------------------------------- <<<\n");
				}

				nfo.m_Counter = 0;

				m_ObjectsIn.Add(nfo);
				counter++;
			} 
			while (true); 
			return true;
		}

		// --------------------------------------------------------------------------------

		bool AdvancedRandomPlacerSlope::GetMapParameters(IMainCommands* cmdIfc)
		{
			// gets demType from cfg file
			const char* tmp = cmdIfc->QueryValue("demType", false);
			if (tmp == 0) 
			{
				ExceptReg::RegExcpt(Exception(51, ">>> Missing Dem\'s type <<<"));
				return false;
			}

			m_MapIn.m_DemType = tmp;

			// checks demType
			if ((m_MapIn.m_DemType != sDTED2) && (m_MapIn.m_DemType != sUSGSDEM) && (m_MapIn.m_DemType != sARCINFOASCII) && (m_MapIn.m_DemType != sXYZ))
			{
				ExceptReg::RegExcpt(Exception(52, ">>> Dem type not implemented <<<"));
				return false;
			}

			// gets filename from cfg file
			tmp = cmdIfc->QueryValue("filename", false);
			if (tmp == 0)
			{
				ExceptReg::RegExcpt(Exception(53, ">>> Missing Dem\'s filename <<<"));
				return false;
			}

			m_MapIn.m_FileName = tmp;

			// checks file extension
			if (m_MapIn.m_DemType == sDTED2) 
			{
				string tmpFileName = m_MapIn.m_FileName;
				transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

				if (!(tmpFileName.find(".DT2") == tmpFileName.length() - 4))
				{
					ExceptReg::RegExcpt(Exception(54, ">>> The file is not a DTED2 <<<"));
					return false;
				}
			}
			if (m_MapIn.m_DemType == sUSGSDEM) 
			{
				string tmpFileName = m_MapIn.m_FileName;
				transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

				if (!(tmpFileName.find(".DEM") == tmpFileName.length() - 4))
				{
					ExceptReg::RegExcpt(Exception(55, ">>> The file is not a USGS DEM <<<"));
					return false;
				}
			}
			if (m_MapIn.m_DemType == sARCINFOASCII) 
			{
				string tmpFileName = m_MapIn.m_FileName;
				transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

				if ((!(tmpFileName.find(".GRD") == tmpFileName.length() - 4)) && (!(tmpFileName.find(".ASC") == tmpFileName.length() - 4)))
				{
					ExceptReg::RegExcpt(Exception(56, ">>> The file is not a Arc/Info ASCII Grid <<<"));
					return false;
				}
			}
			if (m_MapIn.m_DemType == sXYZ) 
			{
				string tmpFileName = m_MapIn.m_FileName;
				transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

				if (!(tmpFileName.find(".XYZ") == tmpFileName.length() - 4))
				{
					ExceptReg::RegExcpt(Exception(56, ">>> The file is not a XYZ <<<"));
					return false;
				}
			}

			// gets srcLeft from cfg file
			tmp = cmdIfc->QueryValue("srcLeft", false);
			if (tmp != 0)
			{
				if (IsInteger(string(tmp), false))
				{
					m_MapIn.m_SrcLeft = atoi(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(61, ">>> Bad data for srcLeft <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(62, ">>> Missing srcLeft value <<<"));
				return false;
			}

			// gets srcRight from cfg file
			tmp = cmdIfc->QueryValue("srcRight", false);
			if (tmp != 0)
			{
				if (IsInteger(string(tmp), false))
				{
					m_MapIn.m_SrcRight = atoi(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(63, ">>> Bad data for srcRight <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(64, ">>> Missing srcRight value <<<"));
				return false;
			}

			// gets srcTop from cfg file
			tmp = cmdIfc->QueryValue("srcTop", false);
			if (tmp != 0)
			{
				if (IsInteger(string(tmp), false))
				{
					m_MapIn.m_SrcTop = atoi(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(65, ">>> Bad data for srcTop <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(66, ">>> Missing srcTop value <<<"));
				return false;
			}

			// gets srcBottom from cfg file
			tmp = cmdIfc->QueryValue("srcBottom", false);
			if (tmp != 0)
			{
				if (IsInteger(string(tmp), false))
				{
					m_MapIn.m_SrcBottom = atoi(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(67, ">>> Bad data for srcBottom <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(68, ">>> Missing srcBottom value <<<"));
				return false;
			}

			// gets dstLeft from cfg file
			tmp = cmdIfc->QueryValue("dstLeft", false);
			if (tmp != 0)
			{
				if (IsFloatingPoint(string(tmp), false))
				{
					m_MapIn.m_DstLeft = atof(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(69, ">>> Bad data for dstLeft <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(70, ">>> Missing dstLeft value <<<"));
				return false;
			}

			// gets dstRight from cfg file
			tmp = cmdIfc->QueryValue("dstRight", false);
			if (tmp != 0)
			{
				if (IsFloatingPoint(string(tmp), false))
				{
					m_MapIn.m_DstRight = atof(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(71, ">>> Bad data for dstRight <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(72, ">>> Missing dstRight value <<<"));
				return false;
			}

			// gets dstTop from cfg file
			tmp = cmdIfc->QueryValue("dstTop", false);
			if (tmp != 0)
			{
				if (IsFloatingPoint(string(tmp), false))
				{
					m_MapIn.m_DstTop = atof(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(73, ">>> Bad data for dstTop <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(74, ">>> Missing dstTop value <<<"));
				return false;
			}

			// gets dstBottom from cfg file
			tmp = cmdIfc->QueryValue("dstBottom", false);
			if (tmp != 0)
			{
				if (IsFloatingPoint(string(tmp), false))
				{
					m_MapIn.m_DstBottom = atof(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(75, ">>> Bad data for dstBottom <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(76, ">>> Missing dstBottom value <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool AdvancedRandomPlacerSlope::LoadHighMap()
		{
			if (m_MapIn.m_DemType == sDTED2)
			{
				if (!(m_MapData.loadFromDT2(m_MapIn.m_FileName)))
				{
					ExceptReg::RegExcpt(Exception(81, ">>> Error while loading DTED2 file <<<"));
					return false;
				}
			}
			if (m_MapIn.m_DemType == sUSGSDEM)
			{
				if (!(m_MapData.loadFromUSGSDEM(m_MapIn.m_FileName)))
				{
					ExceptReg::RegExcpt(Exception(82, ">>> Error while loading USGS DEM file <<<"));
					return false;
				}
				else
				{
					// if the elevation units in the DEM are feet, converts to METERS
					if (m_MapData.getWorldUnitsW() == EtRectElevationGrid<double, short>::euFEET)
					{
						m_MapData.convertWorldUnitsW(EtRectElevationGrid<double, short>::euMETERS);
					}
				}
			}
			if (m_MapIn.m_DemType == sARCINFOASCII)
			{
				if (!(m_MapData.loadFromARCINFOASCIIGrid(m_MapIn.m_FileName)))
				{
					ExceptReg::RegExcpt(Exception(83, ">>> Error while loading Arc/Info Ascii Grid file <<<"));
					return false;
				}
			}
			if (m_MapIn.m_DemType == sXYZ)
			{
				if (!(m_MapData.loadFromXYZ(m_MapIn.m_FileName)))
				{
					ExceptReg::RegExcpt(Exception(84, ">>> Error while loading xyz file <<<"));
					return false;
				}
			}
			m_MapIn.hmData = m_MapData;

			m_MapIn.hmData.offsetOriginWorldU(-m_MapIn.hmData.getOriginWorldU());
			m_MapIn.hmData.offsetOriginWorldV(-m_MapIn.hmData.getOriginWorldV());

			m_MapLoaded = true;

			return true;
		}

		// --------------------------------------------------------------------//

		bool AdvancedRandomPlacerSlope::NormalizeProbs()
		{
			double total = 0.0;

			int objInCount = m_ObjectsIn.Size();
			for (int i = 0; i < objInCount; ++i)
			{
				total += m_ObjectsIn[i].m_Prob;
			}

			if (total == 0.0) 
			{
				ExceptReg::RegExcpt(Exception(91, ">>> Total probability equal to zero <<<"));
				return false;
			}

			double invTotal = 1 / total;
			for (int i = 0; i < objInCount; ++i)
			{
				m_ObjectsIn[i].m_NormProb = m_ObjectsIn[i].m_Prob * invTotal;
			}
			return true;
		}

		// --------------------------------------------------------------------//

		bool AdvancedRandomPlacerSlope::PtCloseToObject(const Shapes::DVector& v) const
		{
			int objOutCount = m_ObjectsOut.Size();
			for (int j = 0; j < objOutCount; ++j)
			{
				double distSq = v.DistanceXY2(m_ObjectsOut[j].position);

				// is close to already existing objects ?
				double minDistance = m_ObjectsIn[m_ObjectsOut[j].type].m_MinDistance;
				double minDistanceSq = minDistance * minDistance; 
				if (distSq < minDistanceSq)
				{
					return true;
				}
			}
			return false;
		}

		// --------------------------------------------------------------------//

		void AdvancedRandomPlacerSlope::CreateObjects(bool preview)
		{
			ProgressBar<int> pb(m_NumberOfIndividuals);
			
			for (int i = 0; i < m_NumberOfIndividuals; ++i)
			{
				if (!preview) pb.AdvanceNext(1);

				// random position
				double x0 = RandomInRangeF(0, m_ShpGeo.widthBB);
				double y0 = RandomInRangeF(0, m_ShpGeo.heightBB);
				Shapes::DVector v0 = Shapes::DVector(x0 + m_ShpGeo.minX, y0 + m_ShpGeo.minY);

				// the random point is inside the current shape ?
				if (m_pCurrentShape->PtInside(v0))
				{
					if (!PtCloseToObject(v0))
					{
						// random type
						int type = GetRandomType();

						double slope = m_MapIn.hmData.getWorldSlopeAt(v0.x, v0.y, EtRectElevationGrid<double, short>::imARMA) * 100;

						if ((slope >= m_ObjectsIn[type].m_MinSlope) && (slope <= m_ObjectsIn[type].m_MaxSlope))
						{
							ModuleObjectOutput out;

							// sets data
							out.position = v0;
							out.type     = type;
							out.rotation = RandomInRangeF(0, EtMathD::TWO_PI);

							double minScale = m_ObjectsIn[type].m_MinHeight;
							double maxScale = m_ObjectsIn[type].m_MaxHeight;

							out.scaleX = out.scaleY = out.scaleZ = RandomInRangeF(minScale, maxScale) / 100.0;

							// used only by VLB for preview
							out.length   = 10.0;
							out.width    = 10.0;
							out.height   = 10.0;

							// adds to array
							m_ObjectsOut.Add(out);

							// update type counter
							m_ObjectsIn[type].m_Counter++;
						}
					}
				}
			}
		}

		// --------------------------------------------------------------------//

		int AdvancedRandomPlacerSlope::GetRandomType()
		{
			double p = (double)rand() / ((double)RAND_MAX + 1);

			bool found = false;
			int index  = 0;
			double curProb = m_ObjectsIn[index].m_NormProb;
			while (!found)
			{
				if (p < curProb)
				{
					found = true;
				}
				else
				{
					index++;
					curProb += m_ObjectsIn[index].m_NormProb;
				}
			}
			return index;
		}

		// --------------------------------------------------------------------//

		void AdvancedRandomPlacerSlope::ExportCreatedObjects(IMainCommands* cmdIfc)
		{
			int objOutCount = m_ObjectsOut.Size();
			for (int i = 0; i < objOutCount; ++i)
			{
				RString name      = m_ObjectsIn[m_ObjectsOut[i].type].m_Name;
				Vector3 position  = Vector3(static_cast<Coord>(m_ObjectsOut[i].position.x), 0, static_cast<Coord>(m_ObjectsOut[i].position.y));

				Matrix4 mTranslation = Matrix4(MTranslation, position);
				Matrix4 mRotation    = Matrix4(MRotationY, static_cast<Coord>(m_ObjectsOut[i].rotation));
				Matrix4 mScaling     = Matrix4(MScale, static_cast<Coord>(m_ObjectsOut[i].scaleX), static_cast<Coord>(m_ObjectsOut[i].scaleZ), static_cast<Coord>(m_ObjectsOut[i].scaleY));

				Matrix4 transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject(transform, name, 0);
			}
		}

		// --------------------------------------------------------------------//

		void AdvancedRandomPlacerSlope::WriteReport()
		{
			printf("Created                  \n");
			int objInCount = m_ObjectsIn.Size();
			for (int i = 0; i < objInCount; ++i)
			{
				printf("%d", m_ObjectsIn[i].m_Counter);
				printf(" : ");
				printf(m_ObjectsIn[i].m_Name);
				printf("                           \n");
			}
			printf("--------------------------------------\n");
		}
	}
}