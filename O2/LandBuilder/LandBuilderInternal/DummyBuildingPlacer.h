#pragma once

#include <string>

#include "IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>
#include "..\HighMapLoaders\include\EtMath.h"

#include "..\Shapes\ShapeHelpers.h"

using std::string;

namespace LandBuildInt
{
	namespace Modules
	{
		class DummyBuildingPlacer : public IBuildModule
		{
		public:
			// enumerator for building type
			enum BuildingType { btNULL, btCIVIL, btINDUSTRIAL, btMILITARY, btNUM };

		public:
			// enumerator for building shape
			enum BuildingShape { bsRECTANGLE, bsLSHAPE, bsNUM };

			// L-Shape can be vectorialized in several ways, depending from
			// the choice of the first vertex and of the direction of rotation (CW or CCW)
			// To let the module recognize which is the configuration of the current
			// L-Shape we have to take in account of all possible variants.
			// The method choiced is to start from the first edge (determined by the first
			// two vertices) and then follow the perimeter of the shape. When we arrive 
			// to a corner, we change edge and we can turn or to the right (R) or to the left (L).
			// writing the sequences of turns we obtain an unique string of Ls and Rs
			// identifying the current configuration.
			// known the configuration, we will be able to determine the direction of every
			// edge, and to know which edge we have to be cutted to obtain the 2 rectangles
			// forming the L-Shape

			// enum defining the possible L-Shape configuration
			enum LShapeConfiguration { lscRLLLLL, lscLRLLLL, lscLLRLLL, lscLLLRLL, lscLLLLRL, lscLLLLLR, 
				                       lscLRRRRR, lscRLRRRR, lscRRLRRR, lscRRRLRR, lscRRRRLR, lscRRRRRL,
			                           lscNUM};

			// enum defining the direction of rotation of vertices
			enum ShapeVerticesRotationDir { vrdCW, vrdCCW };

		public:
			struct GlobalInput
			{
				RString modelCivil;
				RString modelIndustrial;
				RString modelMilitary;
				double  heightFloorsCivil;
				double  heightFloorsIndustrial;
				double  heightFloorsMilitary;
				int     maxNumFloorsCivil;
				int     maxNumFloorsIndustrial;
				int     maxNumFloorsMilitary;
			};

			struct DbfInput
			{
				BuildingType buildingType;
				int          numOfFloors;
			};

		private:
			class Edge
			{
			private:
				Shapes::DVertex* m_V1;
				Shapes::DVertex* m_V2;

			public:
				// constructors
				Edge() 
				{
					m_V1 = 0;
					m_V2 = 0;
				}

				Edge(Shapes::DVertex* v1, Shapes::DVertex* v2) : m_V1(v1), m_V2(v2) {}

				// copy constructor
				Edge(const Edge& other)
				{
					m_V1 = other.m_V1;
					m_V2 = other.m_V2;
				}

				// sets the given vertex with the given coordinates
				// vertex must be 1 or 2
				void SetVertex(int vertex, double x, double y)
				{
					switch(vertex)
					{
					case 1:
						m_V1->x = x;
						m_V1->y = y;
						break;
					case 2:
						m_V2->x = x;
						m_V2->y = y;
						break;
					}
				}

				// return the x coordinate of the given vertex
				// vertex must be 1 or 2
				double GetVertexX(int vertex)
				{
					switch(vertex)
					{
					case 1:
						return m_V1->x;
					case 2:
						return m_V2->x;
					}
				}

				// return the y coordinate of the given vertex
				// vertex must be 1 or 2
				double GetVertexY(int vertex)
				{
					switch(vertex)
					{
					case 1:
						return m_V1->y;
					case 2:
						return m_V2->y;
					}
				}

				// return the mid point of the edge
				Shapes::DVertex MidPoint()
				{
					return (Shapes::DVertex((m_V1->x + m_V2->x) / 2, (m_V1->y + m_V2->y) / 2));
				}

				// return the angle of direction of this edge in the plane XY
				// from the x axis (in radians)
				double DirectionXY()
				{
					return EtMathD::SegmentDirection_XY_CCW_X(m_V1->x, m_V1->y, m_V2->x, m_V2->y);
				}

				// return the length of this edge
				double Length()
				{
					return m_V1->DistanceXY(*m_V2);
				}

				ClassIsMovable(Edge); 
			};

		public:
			DummyBuildingPlacer();

			virtual void Run(IMainCommands* cmdIfc);
			ModuleObjectOutputArray* RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn);

			class Exception : public ExceptReg::IException
			{
				unsigned int code;
				const char* text;

			public:
				Exception(unsigned int code, const char* text) 
				: code(code)
				, text(text) 
				{
				}

				virtual unsigned int GetCode() const 
				{ 
					return code; 
				}

				virtual const _TCHAR* GetDesc() const 
				{ 
					return text; 
				}

				virtual const _TCHAR* GetModule() const 
				{ 
					return "DummyBuildingPlacer"; 
				}

				virtual const _TCHAR* GetType() const 
				{ 
					return "DummyBuildingPlacerException"; 
				}
			};

		private:
			AutoArray<Shapes::DVertex, MemAllocStack<Shapes::DVertex, 32> > m_Vertices;
			AutoArray<Edge, MemAllocStack<Edge, 32> >                       m_Edges;

			ModuleObjectOutputArray m_ObjectsOut;

			string                   m_LShapeOddEdgeSigns[12];
			string                   m_LShapeEvenEdgeSigns[12];
			string                   m_StrLShapeConfiguration[12];

			const Shapes::IShape*    m_pCurrentShape; 
			int                      m_csVertexCount;
			GlobalInput              m_GlobalIn;
			DbfInput                 m_DbfIn;
			double                   m_MeanDirectionU;
			double                   m_MeanDirectionV;
			LShapeConfiguration      m_LShapeConfiguration;
			BuildingShape            m_BuildingShape;

			bool            GetDbfParameters(IMainCommands* cmdIfc);
			bool            GetGlobalParameters(IMainCommands* cmdIfc);
			bool            SetGeometry();
			void            CalculateMeanUVDirections();
			void            OrthogonalizeUV();
			void            ReshapeVertices();
			void            RotateAboutPoint(const Shapes::DVertex point, double angle);
			void            OrthogonalizeShape();
			void            CreateObjects();
			void            ExportCreatedObjects(IMainCommands* cmdIfc);
		};
	}
}