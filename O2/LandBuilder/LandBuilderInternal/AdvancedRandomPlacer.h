#pragma once

#include "IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>

#include "..\Shapes\ShapeHelpers.h"

namespace LandBuildInt
{
	namespace Modules
	{
		class AdvancedRandomPlacer : public IBuildModule
		{
		public:
			struct GlobalInput
			{
				unsigned int randomSeed;
			};

			struct DbfInput
			{
				double hectareDensity;
			};

			struct ObjectInput
			{
				RString m_Name;
				double  m_Prob;
				double  m_NormProb;
				double  m_MinHeight;
				double  m_MaxHeight;
				double  m_MinDistance;
				int     m_Counter;

				ObjectInput() 
				{
				}

				ObjectInput(RString name, double prob, double minHeight, double maxHeight, double minDistance)
				: m_Name(name)
				, m_Prob(prob)
				, m_MinHeight(minHeight)
				, m_MaxHeight(maxHeight)
				, m_MinDistance(minDistance)
				, m_Counter(0)
				{
				}

				ClassIsMovable(ObjectInput);
			};

		private:
			struct ShapeGeo
			{
				double minX;
				double maxX;
				double minY;
				double maxY;
				double widthBB;        // bounding box width
				double heightBB;       // bounding box height
				double area;           // area of the shape (polygon)
				double areaHectares;   // area of the shape in hectares
				double areaBB;         // bounding box area
				double areaBBHectares; // bounding box area in hectares
			};

		public:
			virtual void Run(IMainCommands*);
			ModuleObjectOutputArray* RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn, 
												  AutoArray<AdvancedRandomPlacer::ObjectInput, MemAllocStack<AdvancedRandomPlacer::ObjectInput, 32> >& objectsIn);

			class Exception : public ExceptReg::IException
			{
				unsigned int code;
				const char* text;

			public:
				Exception(unsigned int code, const char* text) 
				: code(code)
				, text(text) 
				{
				}

				virtual unsigned int GetCode() const 
				{ 
					return code; 
				}

				virtual const _TCHAR* GetDesc() const 
				{ 
					return text; 
				}

				virtual const _TCHAR* GetModule() const 
				{ 
					return "AdvancedRandomPlacer"; 
				}

				virtual const _TCHAR* GetType() const 
				{ 
					return "AdvancedRandomPlacerException"; 
				}
			};

		private:
			const Shapes::IShape* m_pCurrentShape;      

			ShapeGeo m_ShpGeo;
			int      m_NumberOfIndividuals;

			AutoArray<ObjectInput, MemAllocStack<ObjectInput, 32> > m_ObjectsIn;
			ModuleObjectOutputArray                                 m_ObjectsOut;

			GlobalInput m_GlobalIn;
			DbfInput    m_DbfIn;

			bool GetShapeGeoData();
			bool GetGlobalParameters(IMainCommands* cmdIfc);
			bool GetDbfParameters(IMainCommands* cmdIfc);
			bool GetObjectsParameters(IMainCommands* cmdIfc);

			bool NormalizeProbs();
			bool PtCloseToObject(const Shapes::DVector& v) const;

			void CreateObjects(bool preview);
			int  GetRandomType();
			void ExportCreatedObjects(IMainCommands* cmdIfc);
			void WriteReport();
		};
	}
}
