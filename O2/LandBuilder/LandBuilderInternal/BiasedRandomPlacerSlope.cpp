#include "StdAfx.h"
#include <algorithm>
#include ".\BiasedRandomPlacerSlope.h"
#include ".\IShapeExtra.h"

#include "..\HighMapLoaders\include\EtMath.h"

namespace LandBuildInt
{
	namespace Modules
	{
		// --------------------------------------------------------------------------------

		BiasedRandomPlacerSlope::BiasedRandomPlacerSlope()
		{
			m_MapLoaded = false;
		}

		// --------------------------------------------------------------------//

		void BiasedRandomPlacerSlope::Run(IMainCommands *cmdIfc)
		{
			printf("Started a new shape                   \n");
			printf("--------------------------------------\n");

			// initializes const variables
			sDTED2        = "DTED2";
			sUSGSDEM      = "USGSDEM";
			sARCINFOASCII = "ARCINFOASCII";
			sXYZ          = "XYZ";

			// gets shape and its properties
			m_pCurrentShape = cmdIfc->GetActiveShape();      
			
			// gets shape geometric data
			if (GetShapeGeoData())
			{
				// gets parameters from calling cfg file
				if (GetGlobalParameters(cmdIfc))
				{
					// gets dbf parameters from calling cfg file
					if (GetDbfParameters(cmdIfc))
					{
						// gets objects parameters from calling cfg file
						if (GetObjectsParameters(cmdIfc))
						{
							// normalizes probabilities
							if (NormalizeProbs())
							{
								if (!m_MapLoaded) // to read the map only once
								{
									// gets maps parameters from calling cfg file
									if(!GetMapParameters(cmdIfc)) return;
									// load maps
									LoadHighMap();
								}

								// initializes random seed
								srand(m_GlobalIn.randomSeed);

								// for each object type put at least one parent
								// who will be the start for all the other objects
								// of the same type
								SetParents();

								// calculates the number of individual to be generated
								// (this value is referred to the bounding box of the shape
								//  and assume a uniform distribution - random generated values
								//  will be filtered to obtain the same density inside the shape,
								//  discarding the ones which will fall out of the shape itself)
								m_NumberOfIndividuals = static_cast<int>(m_ShpGeo.areaBBHectares * m_DbfIn.hectareDensity);

								// sets the population
								CreateObjects(false);

								// export objects
								ExportCreatedObjects(cmdIfc);

								// writes the report
								WriteReport();
							}
						}
					}
				}
			}
		}

		// --------------------------------------------------------------------//

		ModuleObjectOutputArray* BiasedRandomPlacerSlope::RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn, MapInput& mapIn,
																	   AutoArray<BiasedRandomPlacerSlope::ObjectInput, MemAllocStack<BiasedRandomPlacerSlope::ObjectInput, 32> >& objectsIn)
		{
			m_pCurrentShape = shape;
			m_GlobalIn      = globalIn;
			m_DbfIn         = dbfIn;
			m_MapIn         = mapIn;
			m_ObjectsIn     = objectsIn;

			int objInCount = m_ObjectsIn.Size();
			for (int i = 0; i < objInCount; ++i)
			{
				m_ObjectsIn[i].m_MaxDistance = m_ObjectsIn[i].m_MinDistance * 9.0 * m_DbfIn.clusterCoeff; 
			}

			if (m_ObjectsOutExtra.Size() != 0) m_ObjectsOutExtra.Clear();

			// initializes const variables
			sDTED2        = "DTED2";
			sUSGSDEM      = "USGSDEM";
			sARCINFOASCII = "ARCINFOASCII";
			sXYZ          = "XYZ";

			// gets shape geometric data
			if (GetShapeGeoData())
			{
				// normalizes probabilities
				if (NormalizeProbs())
				{
					if (!m_MapLoaded) // to read the map only once
					{
						// load maps
						LoadHighMap();
					}
					else
					{
						m_MapIn.hmData = m_MapData;
					}

					// initializes random seed
					srand(m_GlobalIn.randomSeed);

					// for each object type put at least one parent
					// who will be the start for all the other objects
					// of the same type
					SetParents();

					// calculates the number of individual to be generated
					// (this value is referred to the bounding box of the shape
					//  and assume a uniform distribution - random generated values
					//  will be filtered to obtain the same density inside the shape,
					//  discarding the ones which will fall out of the shape itself)
					m_NumberOfIndividuals = static_cast<int>(m_ShpGeo.areaBBHectares * m_DbfIn.hectareDensity);

					// sets the population
					CreateObjects(true);

					m_ObjectsOut.Clear();

					// extract the data
					int objOutExCount = m_ObjectsOutExtra.Size();
					for (int i = 0; i < objOutExCount; ++i)
					{
						m_ObjectsOut.Add(m_ObjectsOutExtra[i].data);
					}
					return &m_ObjectsOut;
				}
				else
				{
					return NULL;
				}
			}
			else
			{
				return NULL;
			}
		}

		// --------------------------------------------------------------------//

		bool BiasedRandomPlacerSlope::GetShapeGeoData()
		{
			Shapes::DBox bBox = m_pCurrentShape->CalcBoundingBox();

			m_ShpGeo.minX = bBox.lo.x;
			m_ShpGeo.minY = bBox.lo.y;
			m_ShpGeo.maxX = bBox.hi.x;
			m_ShpGeo.maxY = bBox.hi.y;

			m_ShpGeo.area         = GetShapeArea(m_pCurrentShape);
			m_ShpGeo.areaHectares = m_ShpGeo.area / 10000.0;

			m_ShpGeo.widthBB        = m_ShpGeo.maxX - m_ShpGeo.minX;
			m_ShpGeo.heightBB       = m_ShpGeo.maxY - m_ShpGeo.minY;
			m_ShpGeo.areaBB         = m_ShpGeo.widthBB * m_ShpGeo.heightBB;
			m_ShpGeo.areaBBHectares = m_ShpGeo.areaBB / 10000.0;

			return true;
		}

		// --------------------------------------------------------------------//

		bool BiasedRandomPlacerSlope::GetGlobalParameters(IMainCommands *cmdIfc)
		{
			m_GlobalIn.smallAtBoundary = true; // default value
			const char* x = cmdIfc->QueryValue("smallAtBoundary", false);
			if (x != 0) 
			{
				if (IsBool(string(x), false))
				{
					int i = static_cast<int>(atoi(x));
					if (i == 0)
					{
						m_GlobalIn.smallAtBoundary = false;
					}
				}
				else
				{
					printf(">>> ------------------------------------------------------ <<<\n");
					printf(">>> Bad data for smallAtBoundary - using default value (1) <<<\n");
					printf(">>> ------------------------------------------------------ <<<\n");
				}
			}
			else
			{
				printf(">>> ----------------------------------------------------------- <<<\n");
				printf(">>> Missing value for smallAtBoundary - using default value (1) <<<\n");
				printf(">>> ----------------------------------------------------------- <<<\n");
			}

			x = cmdIfc->QueryValue("randomSeed", false);
			if (x != 0) 
			{
				if (IsInteger(string(x), false))
				{
					m_GlobalIn.randomSeed = static_cast<unsigned int>(atoi(x));
				}
				else
				{
					ExceptReg::RegExcpt(Exception(11, ">>> Bad data for randomSeed <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(12, ">>> Missing value for randomSeed <<<"));
				return false;
			}

			return true;
		};

		// --------------------------------------------------------------------//

		bool BiasedRandomPlacerSlope::GetDbfParameters(IMainCommands *cmdIfc)
		{
			// gets parameters from calling cfg file
			m_DbfIn.hectareDensity = 100.0; // default value
			const char* x = cmdIfc->QueryValue("hectareDensity", false);
			if (x != 0) 
			{
				if (IsFloatingPoint(string(x), false))
				{
					m_DbfIn.hectareDensity = atof(x);
				}
				else
				{
					printf(">>> ------------------------------------------------------- <<<\n");
					printf(">>> Bad data for hectareDensity - using default value (100) <<<\n");
					printf(">>> ------------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> ------------------------------------------------------------ <<<\n");
				printf(">>> Missing value for hectareDensity - using default value (100) <<<\n");
				printf(">>> ------------------------------------------------------------ <<<\n");
			}
			if (m_DbfIn.hectareDensity < 0.01)
			{
				ExceptReg::RegExcpt(Exception(21, "Density is too low (less then 0.01)"));
				return false;
			}

			m_DbfIn.clusterCoeff = 1.0; // default value
			x = cmdIfc->QueryValue("aggregationCoeff", false);
			if (x != 0) 
			{
				if (IsFloatingPoint(string(x), false))
				{
					m_DbfIn.clusterCoeff = atof(x);
				}
				else
				{
					printf(">>> ----------------------------------------------------- <<<\n");
					printf(">>> Bad data for clusterCoeff - using default value (1.0) <<<\n");
					printf(">>> ----------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> ---------------------------------------------------------- <<<\n");
				printf(">>> Missing value for clusterCoeff - using default value (1.0) <<<\n");
				printf(">>> ---------------------------------------------------------- <<<\n");
			}
			if (m_DbfIn.clusterCoeff < 0.0)
			{
				ExceptReg::RegExcpt(Exception(22, ">>> Coefficient must be positive <<<"));
				return false;
			}

			m_DbfIn.parentCount = 1; // default value
			x = cmdIfc->QueryValue("parentCount", false);
			if (x != 0) 
			{
				if (IsInteger(string(x), false))
				{
					m_DbfIn.parentCount = atoi(x);
					if(m_DbfIn.parentCount < 1) m_DbfIn.parentCount = 1;
				}
				else
				{
					printf(">>> -------------------------------------------------- <<<\n");
					printf(">>> Bad data for parentCount - using default value (1) <<<\n");
					printf(">>> -------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> ------------------------------------------------------- <<<\n");
				printf(">>> Missing value for parentCount - using default value (1) <<<\n");
				printf(">>> ------------------------------------------------------- <<<\n");
			}

			return true;
		}

		// --------------------------------------------------------------------//

		bool BiasedRandomPlacerSlope::GetObjectsParameters(IMainCommands *cmdIfc)
		{
			// resets arrays
			if (m_ObjectsIn.Size() != 0)
			{
				m_ObjectsIn.Clear();
				m_ObjectsOutExtra.Clear();
			}

			// gets objects' data from calling cfg file
			int counter = 1;
			do 
			{
				char object[50], minheight[50], maxheight[50], prob[50], mindist[50],minslope[50], maxslope[50];
				sprintf_s(object     , "object%d"     , counter);
				sprintf_s(minheight  , "minheight%d"  , counter);
				sprintf_s(maxheight  , "maxheight%d"  , counter);
				sprintf_s(prob       , "prob%d"       , counter);
				sprintf_s(mindist    , "mindist%d"    , counter);
				sprintf_s(minslope   , "minslope%d"   ,  counter);
				sprintf_s(maxslope   , "maxslope%d"   ,  counter);

				ObjectInput nfo;

				// object type
				const char* x = cmdIfc->QueryValue(object, false);
				if (x == 0) break;
				nfo.m_Name = x;

				// object min height
				x = cmdIfc->QueryValue(minheight, false);
				nfo.m_MinHeight = 100.0; // default value
				if (x != 0) 
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_MinHeight = atof(x);
					}
					else
					{
						printf(">>> -------------------------------------------------- <<<\n");
						printf(">>> Bad data for minheight - using default value (100) <<<\n");
						printf(">>> -------------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> ------------------------------------------------------- <<<\n");
					printf(">>> Missing value for minheight - using default value (100) <<<\n");
					printf(">>> ------------------------------------------------------- <<<\n");
				}

				// object max height
				x = cmdIfc->QueryValue(maxheight, false);
				nfo.m_MaxHeight = 100.0; // default value
				if (x != 0) 
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_MaxHeight = atof(x);
					}
					else
					{
						printf(">>> -------------------------------------------------- <<<\n");
						printf(">>> Bad data for maxheight - using default value (100) <<<\n");
						printf(">>> -------------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> ------------------------------------------------------- <<<\n");
					printf(">>> Missing value for maxheight - using default value (100) <<<\n");
					printf(">>> ------------------------------------------------------- <<<\n");
				}

				// object min slope
				x = cmdIfc->QueryValue(minslope, false);
				if (x != 0)
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_MinSlope = atof(x);
					}
					else
					{
						ExceptReg::RegExcpt(Exception(31, ">>> Bad data for minslope <<<"));
						return false;
					}
				}
				else
				{
					ExceptReg::RegExcpt(Exception(32, ">>> Missing minslope value <<<"));
					return false;
				}
				
				// object max slope
				x = cmdIfc->QueryValue(maxslope, false);
				if (x != 0)
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_MaxSlope = atof(x);
					}
					else
					{
						ExceptReg::RegExcpt(Exception(33, ">>> Bad data for maxslope <<<"));
						return false;
					}
				}
				else
				{
					ExceptReg::RegExcpt(Exception(34, ">>> Missing maxslope value <<<"));
					return false;
				}

				// object probability
				nfo.m_Prob = 100.0; // default value
				x = cmdIfc->QueryValue(prob, false);
				if (x != 0) 
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_Prob = atof(x);
					}
					else
					{
						printf(">>> --------------------------------------------- <<<\n");
						printf(">>> Bad data for prob - using default value (100) <<<\n");
						printf(">>> --------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> -------------------------------------------------- <<<\n");
					printf(">>> Missing value for prob - using default value (100) <<<\n");
					printf(">>> -------------------------------------------------- <<<\n");
				}

				// object min distance
				x = cmdIfc->QueryValue(mindist, false);
				nfo.m_MinDistance = 5.0; // default value
				if (x != 0) 
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_MinDistance = atof(x);
					}
					else
					{
						printf(">>> ------------------------------------------------ <<<\n");
						printf(">>> Bad data for mindist - using default value (5.0) <<<\n");
						printf(">>> ------------------------------------------------ <<<\n");
					}
				}
				else
				{
					printf(">>> ----------------------------------------------------- <<<\n");
					printf(">>> Missing value for mindist - using default value (5.0) <<<\n");
					printf(">>> ----------------------------------------------------- <<<\n");
				}

				// object counter
				nfo.m_Counter = 0;

				// object max distance
				nfo.m_MaxDistance = nfo.m_MinDistance * 9.0 * m_DbfIn.clusterCoeff; 

				m_ObjectsIn.Add(nfo);
				counter++;
			} 
			while(true);   
			return true;
		}

		// --------------------------------------------------------------------------------

		bool BiasedRandomPlacerSlope::GetMapParameters(IMainCommands* cmdIfc)
		{
			// gets demType from cfg file
			const char* tmp = cmdIfc->QueryValue("demType", false);
			if (tmp == 0) 
			{
				ExceptReg::RegExcpt(Exception(51, ">>> Missing Dem\'s type <<<"));
				return false;
			}

			m_MapIn.m_DemType = tmp;

			// checks demType
			if ((m_MapIn.m_DemType != sDTED2) && (m_MapIn.m_DemType != sUSGSDEM) && (m_MapIn.m_DemType != sARCINFOASCII) && (m_MapIn.m_DemType != sXYZ))
			{
				ExceptReg::RegExcpt(Exception(52, ">>> Dem type not implemented <<<"));
				return false;
			}

			// gets filename from cfg file
			tmp = cmdIfc->QueryValue("filename", false);
			if (tmp == 0)
			{
				ExceptReg::RegExcpt(Exception(53, ">>> Missing Dem\'s filename <<<"));
				return false;
			}

			m_MapIn.m_FileName = tmp;

			// checks file extension
			if (m_MapIn.m_DemType == sDTED2) 
			{
				string tmpFileName = m_MapIn.m_FileName;
				transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

				if (!(tmpFileName.find(".DT2") == tmpFileName.length() - 4))
				{
					ExceptReg::RegExcpt(Exception(54, ">>> The file is not a DTED2 <<<"));
					return false;
				}
			}
			if (m_MapIn.m_DemType == sUSGSDEM) 
			{
				string tmpFileName = m_MapIn.m_FileName;
				transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

				if (!(tmpFileName.find(".DEM") == tmpFileName.length() - 4))
				{
					ExceptReg::RegExcpt(Exception(55, ">>> The file is not a USGS DEM <<<"));
					return false;
				}
			}
			if (m_MapIn.m_DemType == sARCINFOASCII) 
			{
				string tmpFileName = m_MapIn.m_FileName;
				transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

				if ((!(tmpFileName.find(".GRD") == tmpFileName.length() - 4)) && (!(tmpFileName.find(".ASC") == tmpFileName.length() - 4)))
				{
					ExceptReg::RegExcpt(Exception(56, ">>> The file is not a Arc/Info ASCII Grid <<<"));
					return false;
				}
			}
			if (m_MapIn.m_DemType == sXYZ) 
			{
				string tmpFileName = m_MapIn.m_FileName;
				transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

				if (!(tmpFileName.find(".XYZ") == tmpFileName.length() - 4))
				{
					ExceptReg::RegExcpt(Exception(56, ">>> The file is not a XYZ <<<"));
					return false;
				}
			}

			// gets srcLeft from cfg file
			tmp = cmdIfc->QueryValue("srcLeft", false);
			if (tmp != 0)
			{
				if (IsInteger(string(tmp), false))
				{
					m_MapIn.m_SrcLeft = atoi(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(61, ">>> Bad data for srcLeft <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(62, ">>> Missing srcLeft value <<<"));
				return false;
			}

			// gets srcRight from cfg file
			tmp = cmdIfc->QueryValue("srcRight", false);
			if (tmp != 0)
			{
				if (IsInteger(string(tmp), false))
				{
					m_MapIn.m_SrcRight = atoi(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(63, ">>> Bad data for srcRight <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(64, ">>> Missing srcRight value <<<"));
				return false;
			}

			// gets srcTop from cfg file
			tmp = cmdIfc->QueryValue("srcTop", false);
			if (tmp != 0)
			{
				if (IsInteger(string(tmp), false))
				{
					m_MapIn.m_SrcTop = atoi(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(65, ">>> Bad data for srcTop <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(66, ">>> Missing srcTop value <<<"));
				return false;
			}

			// gets srcBottom from cfg file
			tmp = cmdIfc->QueryValue("srcBottom", false);
			if (tmp != 0)
			{
				if (IsInteger(string(tmp), false))
				{
					m_MapIn.m_SrcBottom = atoi(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(67, ">>> Bad data for srcBottom <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(68, ">>> Missing srcBottom value <<<"));
				return false;
			}

			// gets dstLeft from cfg file
			tmp = cmdIfc->QueryValue("dstLeft", false);
			if (tmp != 0)
			{
				if (IsFloatingPoint(string(tmp), false))
				{
					m_MapIn.m_DstLeft = atof(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(69, ">>> Bad data for dstLeft <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(70, ">>> Missing dstLeft value <<<"));
				return false;
			}

			// gets dstRight from cfg file
			tmp = cmdIfc->QueryValue("dstRight", false);
			if (tmp != 0)
			{
				if (IsFloatingPoint(string(tmp), false))
				{
					m_MapIn.m_DstRight = atof(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(71, ">>> Bad data for dstRight <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(72, ">>> Missing dstRight value <<<"));
				return false;
			}

			// gets dstTop from cfg file
			tmp = cmdIfc->QueryValue("dstTop", false);
			if (tmp != 0)
			{
				if (IsFloatingPoint(string(tmp), false))
				{
					m_MapIn.m_DstTop = atof(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(73, ">>> Bad data for dstTop <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(74, ">>> Missing dstTop value <<<"));
				return false;
			}

			// gets dstBottom from cfg file
			tmp = cmdIfc->QueryValue("dstBottom", false);
			if (tmp != 0)
			{
				if (IsFloatingPoint(string(tmp), false))
				{
					m_MapIn.m_DstBottom = atof(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(75, ">>> Bad data for dstBottom <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(76, ">>> Missing dstBottom value <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool BiasedRandomPlacerSlope::LoadHighMap()
		{
			if (m_MapIn.m_DemType == sDTED2)
			{
				if (!(m_MapData.loadFromDT2(m_MapIn.m_FileName)))
				{
					ExceptReg::RegExcpt(Exception(81, ">>> Error while loading DTED2 file <<<"));
					return false;
				}
			}
			if (m_MapIn.m_DemType == sUSGSDEM)
			{
				if (!(m_MapData.loadFromUSGSDEM(m_MapIn.m_FileName)))
				{
					ExceptReg::RegExcpt(Exception(82, ">>> Error while loading USGS DEM file <<<"));
					return false;
				}
				else
				{
					// if the elevation units in the DEM are feet, converts to METERS
					if(m_MapData.getWorldUnitsW() == EtRectElevationGrid<double, short>::euFEET)
					{
						m_MapData.convertWorldUnitsW(EtRectElevationGrid<double, short>::euMETERS);
					}
				}
			}
			if (m_MapIn.m_DemType == sARCINFOASCII)
			{
				if (!(m_MapData.loadFromARCINFOASCIIGrid(m_MapIn.m_FileName)))
				{
					ExceptReg::RegExcpt(Exception(83, ">>> Error while loading Arc/Info Ascii Grid file <<<"));
					return false;
				}
			}
			if (m_MapIn.m_DemType == sXYZ)
			{
				if (!(m_MapData.loadFromXYZ(m_MapIn.m_FileName)))
				{
					ExceptReg::RegExcpt(Exception(84, ">>> Error while loading xyz file <<<"));
					return false;
				}
			}
			m_MapIn.hmData = m_MapData;

			m_MapIn.hmData.offsetOriginWorldU(-m_MapIn.hmData.getOriginWorldU());
			m_MapIn.hmData.offsetOriginWorldV(-m_MapIn.hmData.getOriginWorldV());

			m_MapLoaded = true;

			return true;
		}

		// --------------------------------------------------------------------//

/*
		// this is the old value for min distance
		// when the algorithm used global parameter for distance
		void BiasedRandomPlacerSlope::SetDistances()
		{
			m_MaxDistance   = (sqrt(10000.0 / m_GlobalIn.hectareDensity) / 2.0) * 9.0 * m_GlobalIn.clusterCoeff; 
		}
*/

		// --------------------------------------------------------------------//

		bool BiasedRandomPlacerSlope::NormalizeProbs()
		{
			double total = 0.0;

			int objInCount = m_ObjectsIn.Size();
			for (int i = 0; i < objInCount; ++i)
			{
				total += m_ObjectsIn[i].m_Prob;
			}

			if (total == 0.0) 
			{
				ExceptReg::RegExcpt(Exception(91, ">>> Total probability equal to zero <<<"));
				return false;
			}

			double invTotal = 1 / total;
			for (int i = 0; i < objInCount; ++i)
			{
				m_ObjectsIn[i].m_NormProb = m_ObjectsIn[i].m_Prob * invTotal;
			}
			return true;
		}

		// --------------------------------------------------------------------//

		void BiasedRandomPlacerSlope::SetParents()
		{
			// add one object for every species in the config file
			int objInCount = m_ObjectsIn.Size();
			for (int i = 0; i < objInCount; ++i)
			{
				Shapes::DVector v0;

				bool valid = false;

				while (!valid)
				{
					// random position
					double x0 = RandomInRangeF(0, m_ShpGeo.widthBB);
					double y0 = RandomInRangeF(0, m_ShpGeo.heightBB);
					v0 = Shapes::DVector(x0 + m_ShpGeo.minX, y0 + m_ShpGeo.minY);

					// the random point is inside the current shape ?
					if (m_pCurrentShape->PtInside(v0))
					{
						// is close to already created objects ?
						valid = !(PtCloseToObject(v0));
					}
				}

				// if all ok, add to array
				AddObjectOutExtra(v0, i, 0);
			}

			// adds more parents for every species if specified
			for (int i = 0; i < objInCount; ++i)
			{
				// set the parent to first created object of this species
				ObjectOutputExtra* parent = &m_ObjectsOutExtra[i];

//				int estimatedNumberOfParents = m_DbfIn.parentCount;
				int estimatedNumberOfParents = m_DbfIn.parentCount * m_ShpGeo.areaBBHectares;

				while (m_ObjectsIn[i].m_Counter < estimatedNumberOfParents)
				{
					Shapes::DVector v0;

					bool valid = false;

					while (!valid)
					{
						// random position
						double x0 = RandomInRangeF(0, m_ShpGeo.widthBB);
						double y0 = RandomInRangeF(0, m_ShpGeo.heightBB);
						v0 = Shapes::DVector(x0 + m_ShpGeo.minX, y0 + m_ShpGeo.minY);

						// the random point is inside the current shape ?
						if (m_pCurrentShape->PtInside(v0))
						{
							// is close to already created objects ?
							valid = !(PtCloseToObject(v0));
						}
					}

					// if all ok, add to array
					AddObjectOutExtra(v0, i, parent);

					// update parent
					parent = &m_ObjectsOutExtra[m_ObjectsOutExtra.Size() - 1];
				}
			}
		}

		// --------------------------------------------------------------------//

		bool BiasedRandomPlacerSlope::PtCloseToObject(const Shapes::DVector& v) const
		{
			int objOutExCount = m_ObjectsOutExtra.Size();
			for (int j = 0; j < objOutExCount; ++j)
			{
				double distSq = v.DistanceXY2(m_ObjectsOutExtra[j].data.position);

				// is close to already existing objects ?
				double minDistanceSq = m_ObjectsOutExtra[j].minDistance * m_ObjectsOutExtra[j].minDistance; 
				if (distSq < minDistanceSq)
				{
					return true;
				}
			}
			return false;
		}

		// --------------------------------------------------------------------//

		void BiasedRandomPlacerSlope::AddObjectOutExtra(const Shapes::DVector& v, int type, ObjectOutputExtra* parent)
		{
			ObjectOutputExtra out;

			// sets data
			out.data.position   = v;
			out.data.type       = type;
			out.data.rotation   = RandomInRangeF(0, EtMathD::TWO_PI);

			Shapes::DVector n = NearestToEdge(m_pCurrentShape, v);

			out.minDistance = m_ObjectsIn[type].m_MinDistance;
			out.maxDistance = m_ObjectsIn[type].m_MaxDistance;

			double distSq = v.DistanceXY2(n);
			if (distSq < 4.0 * (out.minDistance * out.minDistance))
			{
				out.atBoundary = true;
			}
			else
			{
				out.atBoundary = false;
			}

			double minScale = m_ObjectsIn[type].m_MinHeight;
			double maxScale = m_ObjectsIn[type].m_MaxHeight;

			if (m_GlobalIn.smallAtBoundary && out.atBoundary)
			{
				out.data.scaleX = 
				out.data.scaleY = 
				out.data.scaleZ = minScale / 100.0;
			}
			else
			{
				out.data.scaleX = 
				out.data.scaleY = 
				out.data.scaleZ = RandomInRangeF(minScale, maxScale) / 100.0;
			}

			// update parenthood
			if (parent != 0)
			{
				out.childIndex = parent->childIndex;
				parent->childIndex = m_ObjectsOutExtra.Size();
			}
			else
			{
				out.childIndex = m_ObjectsOutExtra.Size();
			}

			// used only by VLB for preview
			out.data.length = 10.0;
			out.data.width  = 10.0;
			out.data.height = 10.0;

			// adds to array
			m_ObjectsOutExtra.Add(out);

			// update type counter
			m_ObjectsIn[type].m_Counter++;
		}

		// --------------------------------------------------------------------//

		int BiasedRandomPlacerSlope::GetRandomType()
		{
			double p = (double)rand() / ((double)RAND_MAX + 1);

			bool found = false;
			int index  = 0;
			double curProb = m_ObjectsIn[index].m_NormProb;
			while (!found)
			{
				if (p < curProb)
				{
					found = true;
				}
				else
				{
					index++;
					curProb += m_ObjectsIn[index].m_NormProb;
				}
			}
			return index;
		}

		// --------------------------------------------------------------------//

		BiasedRandomPlacerSlope::ObjectOutputExtra* BiasedRandomPlacerSlope::GetRandomParent(int type)
		{
			int randomPosIndex = static_cast<int>(RandomInRangeF(0, static_cast<double>(m_ObjectsIn[type].m_Counter)));

			int parentIndex = type;
			// searches for parent
			for (int i = 0; i < randomPosIndex; ++i)
			{
				parentIndex = m_ObjectsOutExtra[parentIndex].childIndex;
			}

			return &m_ObjectsOutExtra[parentIndex];
		}

		// --------------------------------------------------------------------//

		void BiasedRandomPlacerSlope::CreateObjects(bool preview)
		{
			ProgressBar<int> pb(m_NumberOfIndividuals);
			
			for (int i = 0; i < m_NumberOfIndividuals; ++i)
			{
				if (!preview) pb.AdvanceNext(1);

				// random position
				double x0 = RandomInRangeF(0, m_ShpGeo.widthBB);
				double y0 = RandomInRangeF(0, m_ShpGeo.heightBB);
				Shapes::DVector v0 = Shapes::DVector(x0 + m_ShpGeo.minX, y0 + m_ShpGeo.minY);

				// the random point is inside the current shape ?
				if (m_pCurrentShape->PtInside(v0))
				{
					// random type
					int type = GetRandomType();

					int iterNum = 0;
					int iterMax = 2 * m_ObjectsIn[type].m_Counter;
					bool valid = false;
					while (!valid)
					{
						iterNum++;

						if (iterNum < iterMax)
						{
							// gets the parent
							ObjectOutputExtra* parent = GetRandomParent(type);

							// random polar coordinates from parent
 							double dist  = RandomInRangeF(parent->minDistance, parent->maxDistance);
							double angle = RandomInRangeF(0, EtMathD::TWO_PI);

							// calculates new random position
							x0 = parent->data.position.x + dist * cos(angle);
    						y0 = parent->data.position.y + dist * sin(angle);
							v0 = Shapes::DVector(x0, y0);

							// the new random point is inside the current shape ?
							if (m_pCurrentShape->PtInside(v0))
							{
								valid = !(PtCloseToObject(v0));

								if (valid)
								{
									double slope = m_MapIn.hmData.getWorldSlopeAt(v0.x, v0.y, EtRectElevationGrid<double, short>::imARMA) * 100;

									if ((slope >= m_ObjectsIn[type].m_MinSlope) && (slope <= m_ObjectsIn[type].m_MaxSlope))
									{
										// if all ok, add to array											
										AddObjectOutExtra(v0, type, parent);
									}
								}
							}
						}
						else
						{
							valid = true;
						}
					}
				}
			}
		}

		// --------------------------------------------------------------------//

		void BiasedRandomPlacerSlope::ExportCreatedObjects(IMainCommands* cmdIfc)
		{
			int objOutExCount = m_ObjectsOutExtra.Size();
			for (int i = 0; i < objOutExCount; ++i)
			{
				RString name      = m_ObjectsIn[m_ObjectsOutExtra[i].data.type].m_Name;
				Vector3 position  = Vector3(static_cast<Coord>(m_ObjectsOutExtra[i].data.position.x), 0, static_cast<Coord>(m_ObjectsOutExtra[i].data.position.y));

				Matrix4 mTranslation = Matrix4(MTranslation, position);
				Matrix4 mRotation    = Matrix4(MRotationY, static_cast<Coord>(m_ObjectsOutExtra[i].data.rotation));
				Matrix4 mScaling     = Matrix4(MScale, static_cast<Coord>(m_ObjectsOutExtra[i].data.scaleX), static_cast<Coord>(m_ObjectsOutExtra[i].data.scaleZ), static_cast<Coord>(m_ObjectsOutExtra[i].data.scaleY));

				Matrix4 transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject(transform, name, 0);
			}
		}

		// --------------------------------------------------------------------//

		void BiasedRandomPlacerSlope::WriteReport()
		{
			printf("Created                  \n");
			int objInCount = m_ObjectsIn.Size();
			for (int i = 0; i < objInCount; ++i)
			{
				printf("%d", m_ObjectsIn[i].m_Counter);
				printf(" : ");
				printf(m_ObjectsIn[i].m_Name);
				printf("                           \n");
			}
			printf("--------------------------------------\n");
		}
	}
}
