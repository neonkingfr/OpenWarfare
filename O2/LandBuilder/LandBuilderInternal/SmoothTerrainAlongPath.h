#pragma once

#include <vector>

#include ".\IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>

#include "..\HighMapLoaders\include\EtRectElevationGrid.h"
#include "..\HighMapLoaders\include\EtRectangle.h"

using std::vector;

namespace LandBuildInt
{
	namespace Modules
	{
		const double cNODATA = -32757.0;

		class SmoothTerrainAlongPath : public IBuildModule
		{
		public:
			struct GlobalInput
			{
				string longSmoothType;
				double longSmoothPar;
			};

			struct DbfInput
			{
				double width;
			};

			struct MapInput
			{
				string       m_DemType;
				string       m_FileName;
				unsigned int m_SrcLeft;
				unsigned int m_SrcTop;
				unsigned int m_SrcRight;
				unsigned int m_SrcBottom;
				double       m_DstLeft;
				double       m_DstTop;
				double       m_DstRight;
				double       m_DstBottom;

				EtRectElevationGrid<double, int> hmData;

				MapInput() 
				{
				}

				MapInput(string demType, string fileName, unsigned int srcLeft, unsigned int srcTop, 
					     unsigned int srcRight, unsigned int srcBottom, double dstLeft, double dstTop,
						 double dstRight, double dstBottom)
				: m_DemType(demType)
				, m_FileName(fileName)
				, m_SrcLeft(srcLeft)
				, m_SrcTop(srcTop)
				, m_SrcRight(srcRight)
				, m_SrcBottom(srcBottom)
				, m_DstLeft(dstLeft)
				, m_DstTop(dstTop)
				, m_DstRight(dstRight)
				, m_DstBottom(dstBottom)
				{
				}

				ClassIsMovable(MapInput); 
			};

			class DemSampler : public IHeightMapSampler
			{
				MapInput& m_Source;

			public:
				DemSampler(MapInput& source) 
				: m_Source(source)
				{
				}

				virtual float GetHeight(const HeightMapLocInfo& heightInfo) const
				{
					// select the source owner of the heightInfo point
					// and get the height from it
					if((heightInfo.x >= m_Source.m_DstLeft) &&
					   (heightInfo.x <= m_Source.m_DstRight) && 
					   (heightInfo.y >= m_Source.m_DstBottom) && 
					   (heightInfo.y <= m_Source.m_DstTop))
					{
						double srcWLeft   = m_Source.hmData.getWorldUAt(m_Source.m_SrcLeft);
						double srcWBottom = m_Source.hmData.getWorldVAt(m_Source.m_SrcBottom);
						double srcWRight  = m_Source.hmData.getWorldUAt(m_Source.m_SrcRight);
						double srcWTop    = m_Source.hmData.getWorldVAt(m_Source.m_SrcTop);

						double srcSizeX = srcWRight - srcWLeft;
						double srcSizeY = srcWTop - srcWBottom;
						double dstSizeX = m_Source.m_DstRight - m_Source.m_DstLeft;
						double dstSizeY = m_Source.m_DstTop - m_Source.m_DstBottom;
						
						double newX = srcWLeft + (heightInfo.x - m_Source.m_DstLeft) * srcSizeX / dstSizeX;
						double newY = srcWBottom + (heightInfo.y - m_Source.m_DstBottom) * srcSizeY / dstSizeY;

						return static_cast<float>(m_Source.hmData.getWorldWAt(newX, newY, EtRectElevationGrid<double, int>::imBILINEAR));
					}

					// if we arrive here something went wrong
					return 0;
				}
			};

			class FixedVertex
			{
				int m_X;
				int m_Y;
				double m_Z;
				unsigned int m_Count;

			public:
				FixedVertex(int x, int y, double z)
				: m_X(x)
				, m_Y(y)
				, m_Z(z)
				, m_Count(1)
				{
				}

				int X() const
				{
					return m_X;
				}

				int Y() const
				{
					return m_Y;
				}

				double Z() const
				{
					return m_Z;
				}

				void Z(double z)
				{
					m_Z = z;
				}

				void AverageZ()
				{
					m_Z /= m_Count;
					m_Count = 1;
				}

				unsigned int Count()
				{
					return m_Count;
				}

				void IncrementCount(double z)
				{
					m_Z += z;
					m_Count++;
				}
			};

			class FixedVerticesList
			{
				vector<FixedVertex> m_List;

			public:
				FixedVerticesList()
				{
				}

				void AddVertex(const FixedVertex& inV)
				{
					int res = Contains(inV.X(), inV.Y()); 
					if (res != -1)
					{
						m_List[res].IncrementCount(inV.Z());
					}
					else
					{
						m_List.push_back(inV);
					}
				}

				FixedVertex& GetVertex(unsigned int index)
				{
					return m_List[index];
				}

				double GetZAt(int x, int y)
				{
					size_t verticesCount = m_List.size();
					for (unsigned int i = 0; i < verticesCount; ++i)
					{
						const FixedVertex& v = m_List[i];
						if (v.X() == x && v.Y() == y)
						{
							return v.Z();
						}
					}
					return cNODATA;
				}

				int Contains(int x, int y)
				{
					size_t verticesCount = m_List.size();
					for (unsigned int i = 0; i < verticesCount; ++i)
					{
						const FixedVertex& v = m_List[i];
						if (v.X() == x && v.Y() == y)
						{
							return i;
						}
					}
					return -1;
				}

				size_t Size() const
				{
					return m_List.size();
				}
			};

		public:
			SmoothTerrainAlongPath();

			virtual void Run(IMainCommands*);
			virtual void OnEndPass(IMainCommands* cmdIfc);

			class Exception: public ExceptReg::IException
			{
				unsigned int code;
				const char* text;

			public:
				Exception(unsigned int code, const char* text) 
				: code(code)
				, text(text) 
				{
				}

				virtual unsigned int GetCode() const 
				{ 
					return code; 
				}

				virtual const _TCHAR* GetDesc() const 
				{ 
					return text; 
				}

				virtual const _TCHAR* GetModule() const 
				{ 
					return "SimpleTerrainSmoother"; 
				}

				virtual const _TCHAR* GetType() const 
				{ 
					return "SimpleTerrainSmootherException"; 
				}
			};

		private:
			string sDTED2;
			string sUSGSDEM;
			string sARCINFOASCII;
			string sXYZ;

			bool m_FirstRun;
			bool m_AllRoadsLoaded;
			unsigned int m_ShapesCounter;

			double m_MapOriginWorldU;
			double m_MapOriginWorldV;

			FixedVerticesList m_UnderFixedList;

			const Shapes::IShape* m_pCurrentShape;      

			GlobalInput m_GlobalIn;
			DbfInput    m_DbfIn;
			MapInput    m_MapIn;

			EtRectElevationGrid<double, int> m_MapData;
			AutoArray<Shapes::DVector, MemAllocStack<Shapes::DVector, 32> > m_ShapeProfile;

			bool GetGlobalParameters(IMainCommands* cmdIfc);
			bool GetDbfParameters(IMainCommands* cmdIfc);
			bool GetMapParameters(IMainCommands* cmdIfc);
			bool LoadHighMap();
			void SmoothProfile();
			void ProfileAsShape();
			void ProfilePointAtSteps(double step);
			void GetProfileElevations();
			void SmoothWithMovingAverage();
			void SmoothWithMaxSlope();
			void SmoothWithMonotone();
			void SmoothWithConstantSlope();
			void SmoothWithFixedAbsolute();
			void SmoothWithFixedRelative();
			void SmoothTerrainUnderPath();
			void SmoothSurroundingTerrain();
			void SaveTerrain();
			EtRectangle<double> SegmentBoundingBox(const Shapes::DVertex& v0, const Shapes::DVertex& v1);
		};
	}
}