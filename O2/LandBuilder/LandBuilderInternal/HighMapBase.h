#pragma once

#include "ILandBuildCmds.h"

namespace LandBuildInt
{
	class HeightMapBase : public IHeightMap
	{
	public:
		struct SamplerModule
		{
			const IHeightMapSampler* _sampler;
			float _resolution;
			FloatRect _area;

			// -------------------------------------------------------------------------- //

			SamplerModule() 
			: _sampler(0)
			, _resolution(FLT_MAX) 
			{
			}
      
			// -------------------------------------------------------------------------- //

			SamplerModule(const IHeightMapSampler* sampler, float resolution, FloatRect area) 
			: _sampler(sampler)
			, _resolution(resolution)
			, _area(area) 
			{
			}

			// -------------------------------------------------------------------------- //

			SamplerModule(const SamplerModule& other) 
			: _area(other._area)
			, _resolution(other._resolution)
			, _sampler(other._sampler)
			{
				const_cast<SamplerModule&>(other)._sampler = 0;
			}

			// -------------------------------------------------------------------------- //

			SamplerModule& operator = (const SamplerModule& other)
			{
				this->~SamplerModule();
				new (this) SamplerModule(other);
				return *this;
			}

			// -------------------------------------------------------------------------- //

			~SamplerModule()
			{
				if (_sampler) _sampler->Release();
			}

			// -------------------------------------------------------------------------- //

			ClassIsMovable(SamplerModule)
		};

	protected:
		AutoArray<SamplerModule> _samplers;
		FloatRect _mapArea;
		float _minRes;

	public:
		HeightMapBase(void);
		~HeightMapBase(void);
  
		virtual FloatRect GetMapArea();
		void SetMapArea(const FloatRect& mapSize);
		virtual void AddSamplerModule(const IHeightMapSampler* sampler, float resolution = 1, const FloatRect* area = 0);
		virtual float GetHeight(float x, float y) const;

		// -------------------------------------------------------------------------- //

		float GetRecomendedResolution() const 
		{
			return _minRes;
		}
	};
}