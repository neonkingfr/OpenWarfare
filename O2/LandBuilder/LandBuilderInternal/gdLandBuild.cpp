#include "StdAfx.h"
#include <El/Evaluator/express.hpp>
#include <El/Evaluator/Addons/Matrix/gdMatrix.h>
#include ".\gdlandbuild.h"
#include "ScriptModule.h"
#include "IShapeExtra.h"
using namespace std;
using namespace LandBuildInt;
using namespace LandBuildInt::Modules;

#define Category "LandBuilder"

static GameValue placeObject( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  unsigned int ID=mod->GetObjectMap()->PlaceObject(*static_cast<GdMatrix *>(oper2.GetData()),(RString)oper1,0);
  return (float)ID;
}

static GameValue setTag( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  IObjectMap *omap=mod->GetObjectMap();
  unsigned int ID=toLargeInt(oper1);
  unsigned int TAG=toLargeInt(oper2);
  if (omap->ReplaceObject(ID,omap->GetObjectLocation(ID),omap->GetObjectType(ID),TAG)==false) return GameValue();
  return oper1;
}

static GameValue setLocation( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  IObjectMap *omap=mod->GetObjectMap();
  unsigned int ID=toLargeInt(oper1);
  GdMatrix *mx=static_cast<GdMatrix *>(oper2.GetData());
  if (omap->ReplaceObject(ID,*mx,omap->GetObjectType(ID),omap->GetObjectTag(ID))==false) return GameValue();
  return oper1;
}

static GameValue setObjectType( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  IObjectMap *omap=mod->GetObjectMap();
  unsigned int ID=toLargeInt(oper1);
  RString type=oper2;
  if (omap->ReplaceObject(ID,omap->GetObjectLocation(ID),type,omap->GetObjectTag(ID))==false) return GameValue();
  return oper1;
}

static GameValue forEachObject( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  IObjectMap *omap=mod->GetObjectMap();
  int id=-1;
  unsigned int TAG=toLargeInt(oper2);
  GameVarSpace local(state->GetContext());
  while (omap->EnumObjects(id))
  {
    if (TAG==0 || omap->GetObjectTag(id)==TAG)
    {    
      local.VarSet("_x",(float)id,true);
      state->BeginContext(&local);
#if USE_PRECOMPILATION
      GameDataCode *code = static_cast<GameDataCode *>(oper1.GetData());
      state->Evaluate(code->GetString(), code->GetCode());
#else
      state->EvaluateMultiple((RString)oper1);
      if (local._blockExit) break;
#endif
      state->EndContext();
    }
  }
  return GameValue();
}

static GameValue forEachShape( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const Shapes::IShape *mod=IGdShape::Shape(oper2.GetData());
  GameVarSpace local(state->GetContext());
  GameArrayType coords;
  coords.Resize(4);
  for (int i=0,cnt=mod->GetVertexCount();i<cnt;i++)
  {
    const Shapes::DVertex &vx=mod->GetVertex(i);
    coords[0]=(float)vx.x;
    coords[1]=(float)vx.y;
    coords[2]=(float)vx.z;
    coords[3]=(float)vx.m;
    local.VarSet("_x",coords,true);
    state->BeginContext(&local);
#if USE_PRECOMPILATION
      GameDataCode *code = static_cast<GameDataCode *>(oper1.GetData());
      state->Evaluate(code->GetString(), code->GetCode());
#else
      state->EvaluateMultiple((RString)oper1);
      if (local._blockExit) break;
#endif    
      state->EndContext();
  }
  return GameValue();
}

static GameValue shapeSelectVertex( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const Shapes::IShape *shape=IGdShape::Shape(oper1.GetData());
  if (shape==0) 
  {
    state->SetError(EvalForeignError,"Shape is empty or null");
    return GameValue();
  }
  int pos=toLargeInt(oper2);
  if (pos<0 || pos>=(signed)shape->GetVertexCount()) 
  {
    state->SetError(EvalForeignError,"Out of shape vertices");
    return GameValue();
  }
  const Shapes::DVertex &vx=shape->GetVertex(pos);
  GameArrayType res;
  res.Realloc(4);
  res.Add((float)vx.x);
  res.Add((float)vx.y);
  res.Add((float)vx.z);
  res.Add((float)vx.m);
  return res;
}

static GameValue shapeBBoxPart( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const Shapes::IShape *shape=IGdShape::Shape(oper2.GetData());
  if (shape==0) 
  {
    state->SetError(EvalForeignError,"Shape is empty or null");
    return GameValue();
  }
  int pos=toLargeInt(oper1);
  if (pos<0 || pos>=(signed)shape->GetPartCount()) 
  {
    state->SetError(EvalForeignError,"Invalid part index");
    return GameValue();
  }

  Shapes::DBox dbox=shape->CalcPartBoundingBox(pos);
  GameArrayType res;
  res.Resize(8);
  res.Add((float)dbox.lo.x);
  res.Add((float)dbox.lo.y);
  res.Add((float)dbox.hi.x);
  res.Add((float)dbox.hi.y);
  res.Add((float)dbox.lo.z);
  res.Add((float)dbox.hi.z);
  res.Add((float)dbox.lo.m);
  res.Add((float)dbox.hi.m);
  return res;
}

static GameValue shapePointInside( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const Shapes::IShape *shape=IGdShape::Shape(oper1.GetData());
  if (shape==0) 
  {
    state->SetError(EvalForeignError,"Shape is empty or null");
    return GameValue();
  }
  const GameArrayType &arr=oper2;
  if (arr.Size()!=2 || arr[0].GetType()!=GameScalar || arr[1].GetType()!=GameScalar)
  {
    state->SetError(EvalType);
    return GameValue();
  }
  float x=arr[0];
  float y=arr[1];
  return shape->PtInside(Shapes::DVector(x,y));
}

static GameValue shapeNearestInside(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  const Shapes::IShape *shape=IGdShape::Shape(oper1.GetData());
  if (shape==0) 
  {
    state->SetError(EvalForeignError,"Shape is empty or null");
    return GameValue();
  }
  const GameArrayType &arr=oper2;
  if (arr.Size()!=2 || arr[0].GetType()!=GameScalar || arr[1].GetType()!=GameScalar)
  {
    state->SetError(EvalType);
    return GameValue();
  }
  float x=arr[0];
  float y=arr[1];
  Shapes::DVector vx=shape->NearestInside(Shapes::DVector(x,y));
  GameArrayType res;
  res.Resize(2);
  res.Add((float)vx.x);
  res.Add((float)vx.y);
  return res;
}

static GameValue shapeNearestEdge(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  const Shapes::IShape *shape=IGdShape::Shape(oper1.GetData());
  if (shape==0) 
  {
    state->SetError(EvalForeignError,"Shape is empty or null");
    return GameValue();
  }
  const IShapeExtra *shapex=shape->GetInterface<IShapeExtra>();
  if (shapex==0)
  {
    return shapeNearestInside(state,oper1,oper2);
  }

  const GameArrayType &arr=oper2;
  if (arr.Size()!=2 || arr[0].GetType()!=GameScalar || arr[1].GetType()!=GameScalar)
  {
    state->SetError(EvalType);
    return GameValue();
  }

  float x=arr[0];
  float y=arr[1];
  Shapes::DVector vx=shapex->NearestEdge(Shapes::DVector(x,y));
  GameArrayType res;
  res.Resize(2);
  res.Add((float)vx.x);
  res.Add((float)vx.y);
  return res;
}

static GameValue queryParam(const GameState *state, GameValuePar oper1)
{
  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  return RString(mod->QueryValue((RString)oper1,false));
}

static GameValue queryConstParam(const GameState *state, GameValuePar oper1)
{
  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  return RString(mod->QueryValue((RString)oper1,true));
}

static GameValue convertAlias(const GameState *state, GameValuePar oper1)
{
  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  return RString(mod->ConvertName((RString)oper1));
}

static GameValue getObjectType(const GameState *state, GameValuePar oper1)
{
  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  IObjectMap *objm=mod->GetObjectMap();
  return RString(objm->GetObjectType(toLargeInt(oper1)));  
}

static GameValue getLocation(const GameState *state, GameValuePar oper1)
{
  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  IObjectMap *objm=mod->GetObjectMap();
  return GameValue(new GdMatrix(objm->GetObjectLocation(toLargeInt(oper1))));  
}

static GameValue getTag(const GameState *state, GameValuePar oper1)
{
  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  IObjectMap *objm=mod->GetObjectMap();
  return (float)(objm->GetObjectTag(toLargeInt(oper1)));  
}

static GameValue findNearest(const GameState *state, GameValuePar oper1)
{
  const GameArrayType &arr=oper1;
  if (arr.Size()<2)
  {
    state->SetError(EvalForeignError,"Too less arguments");
    return GameValue();
  }

  if (arr[0].GetType()!=GameScalar || arr[1].GetType()!=GameScalar)
  {
    state->SetError(EvalType);
    return GameValue();
  }

  float x=arr[0];
  float y=arr[1];
  unsigned int tag;
  Array<unsigned int> skip(0);

  if (arr.Size()>2)
  {
    if (arr[2].GetType()!=GameScalar) 
    {
      state->SetError(EvalType);
      return GameValue();
    }
    else
    {
      tag=toLargeInt(arr[2]);
    }
  }
  else
    tag=0;

  if (arr.Size()>3)
  {
    if (arr[3].GetType()!=GameArray)
    {
      state->SetError(EvalType);
      return GameValue();
    }
    else
    {
      const GameArrayType &lst=arr[3];
      unsigned int *ids=(unsigned int *)alloca(sizeof(unsigned int)*lst.Size());
      for (int i=0;i<lst.Size();i++)
        if (lst[i].GetType()!=GameScalar)
        {
          state->SetError(EvalType);
          return GameValue();
        }
        else
          ids[i]=toLargeInt(lst[i]);
      skip=Array<unsigned int>(ids,lst.Size());
    }
  }

  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  IObjectMap *objm=mod->GetObjectMap();
  unsigned int res=objm->FindNearestObject(x,y,tag,skip);
  if (res==-1) return GameValue();
  return (float)res;
}

static GameValue objectsInAreaRect(const GameState *state, GameValuePar oper1)
{
  const GameArrayType &arr=oper1;
  if (arr.Size()!=4 && arr.Size()!=5|| arr[0].GetType()!=GameScalar
    || arr[1].GetType()!=GameScalar|| arr[2].GetType()!=GameScalar
    || arr[3].GetType()!=GameScalar)
  {
    state->SetError(EvalType);
    return GameValue();
  }

  unsigned int tag=0;
  if (arr.Size()==5)
    if (arr[4].GetType()==GameScalar)
      tag=toLargeInt(arr[4]);
    else
    {
      state->SetError(EvalType);
      return GameValue();
    }
   
  FloatRect frect(arr[0],arr[1],arr[2],arr[3]);

  class Callback: public IEnumObjectCallback
  {
    mutable GameArrayType &arr;
  public:
    Callback(GameArrayType &arr):arr(arr) {}
    virtual int operator()(unsigned int objId) const
    {
      arr.Add((float)objId);
      return 0;
    }
  };

  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  IObjectMap *objm=mod->GetObjectMap();
  GameArrayType res;
  objm->ObjectsInArea(frect,tag,Callback(res));
  return res;
}

static GameValue objectsInAreaShape(const GameState *state, GameValuePar oper1)
{
  const Shapes::IShape *shape=IGdShape::Shape(oper1.GetData());

  class Callback: public IEnumObjectCallback
  {
    mutable GameArrayType &arr;
  public:
    Callback(GameArrayType &arr):arr(arr) {}
    virtual int operator()(unsigned int objId) const
    {
      arr.Add((float)objId);
      return 0;
    }
  };

  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  IObjectMap *objm=mod->GetObjectMap();
  GameArrayType res;

  objm->ObjectsInArea(*shape,0,Callback(res));
  return res;
}

static GameValue deleteObject(const GameState *state, GameValuePar oper1)
{
  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  IObjectMap *objm=mod->GetObjectMap();
  return objm->DeleteObject(toLargeInt(oper1));
}

static GameValue countVertices(const GameState *state, GameValuePar oper1)
{
  const Shapes::IShape *shape=IGdShape::Shape(oper1.GetData());
  return (float)shape->GetVertexCount();
}

static GameValue shapeParts(const GameState *state, GameValuePar oper1)
{
  const Shapes::IShape *shape=IGdShape::Shape(oper1.GetData());
  int count=shape->GetPartCount();
  GameArrayType res;
  for (int i=0;i<count;i++) res.Add((float)shape->GetPartIndex(i));
  return res;
}
static GameValue getShapeType(const GameState *state, GameValuePar oper1)
{
  const Shapes::IShape *shape=IGdShape::Shape(oper1.GetData());
  const IShapeExtra *ex=shape->GetInterface<IShapeExtra>();
  if (ex==0) 
  {
    return RString("unknown");
  }
  else
  {
    return RString(ex->GetShapeTypeName());
  }
}

static GameValue shapeBBox(const GameState *state, GameValuePar oper1)
{
  const Shapes::IShape *shape=IGdShape::Shape(oper1.GetData());
  Shapes::DBox dbox=shape->CalcBoundingBox();
  GameArrayType res;
  res.Realloc(8);
  res.Add((float)dbox.lo.x);
  res.Add((float)dbox.lo.y);
  res.Add((float)dbox.hi.x);
  res.Add((float)dbox.hi.y);
  res.Add((float)dbox.lo.z);
  res.Add((float)dbox.hi.z);
  res.Add((float)dbox.lo.m);
  res.Add((float)dbox.hi.m);
  return res;
}

static GameValue getActiveShape(const GameState *state)
{
  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  const Shapes::IShape *shape=mod->GetActiveShape();
  if (shape==0)
  {
    state->SetError(EvalForeignError,"Internal: No active shape");
    return GameValue();
  }
  return GameValue(new GdConstShape(shape));
}

static GameValue getMapArea(const GameState *state)
{
  IMainCommands *mod=ScriptModule::CmdIfc(*state);
  FloatRect mapArea=mod->GetObjectMap()->GetMapArea();
  GameArrayType res;
  res.Realloc(4);
  res.Add(mapArea.left);
  res.Add(mapArea.top);
  res.Add(mapArea.right);
  res.Add(mapArea.bottom);
  return res;
}

static GameValue shpPoint(const GameState *state)
{
  return RString(SHAPE_NAME_POINT);
}

static GameValue shpMultiPoint(const GameState *state)
{
  return RString(SHAPE_NAME_MULTIPOINT);
}

static GameValue shpPolyLine(const GameState *state)
{
  return RString(SHAPE_NAME_POLYLINE);
}

static GameValue shpPolygon(const GameState *state)
{
  return RString(SHAPE_NAME_POLYGON);
}

static GameData *CreateShapeType(ParamArchive *ar)
{
  return 0;
}

static GameValue shapeClip(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  const Shapes::IShape *shape=IGdShape::Shape(oper1.GetData());
  const GameArrayType &arr=oper2;
  if (arr.Size()!=3 || arr[0].GetType()!=GameScalar || arr[1].GetType()!=GameScalar ||arr[2].GetType()!=GameScalar)
  {
    state->SetError(EvalType);
    return GameValue();
  }
  float a=arr[0];
  float b=arr[1];
  float c=arr[2];
  GdTempShape *res=new GdTempShape();
  Shapes::IShape *clp=shape->Clip(a,b,c,res->GetVertexArray());
  if (clp==0)
  {
    delete res;
    return GameValue();
  }
  res->SetShape(clp);
  return GameValue(res);
}


static GameValue shapeCrop(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  const Shapes::IShape *shape=IGdShape::Shape(oper1.GetData());
  const GameArrayType &arr=oper2;
  if (arr.Size()!=4 || arr[0].GetType()!=GameScalar || arr[1].GetType()!=GameScalar ||arr[2].GetType()!=GameScalar||arr[3].GetType()!=GameScalar)
  {
    state->SetError(EvalType);
    return GameValue();
  }
  float left=arr[0];
  float top=arr[1];
  float right=arr[2];
  float bottom=arr[2];
  GdTempShape *res=new GdTempShape();
  Shapes::IShape *clp=shape->Crop(left,top,right,bottom,res->GetVertexArray());
  if (clp==0)
  {
    delete res;
    return GameValue();
  }
  res->SetShape(clp);
  return GameValue(res);
}

static GameValue reportProgress(const GameState *state, GameValuePar oper1)
{
  const ScriptModule &cmod=ScriptModule::Instance(*state);
  ScriptModule &mod=const_cast<ScriptModule &>(cmod);
  if (mod._progress)
    mod._progress->SetPos((float)oper1);
  return GameValue();
}


#define OPERATORS_DEFAULT(XX, Category) \
  XX(GameScalar, "placeObjectAt",function, placeObject, GameString, EvalType_Matrix, "object","position", "Places a new object at position defined by matrix.", "", "ID of new object. Use ID to access instance of the object", "", "", Category) \
  XX(GameScalar, "setTag",function, setTag, GameScalar, GameScalar, "objectId","tagId", "Sets object tag", "", "Left argument. It allows to create chains.", "", "", Category) \
  XX(GameScalar, "setLocation",function, setLocation, GameScalar, EvalType_Matrix, "objectId","location", "Sets object location", "", "Left argument. It allows to create chains.", "", "", Category) \
  XX(GameScalar, "setObjectType",function, setObjectType, GameScalar, GameString, "objectId","type", "Sets object type (type is string contains name of object, or template name. ", "", "Left argument. It allows to create chains.", "", "", Category) \
  XX(GameNothing, "forEachObject",function, forEachObject, GameCode, GameScalar, "code","tag filter", "Process code for each object. If tag filter is not equal zero, it processes object with specified tag", "", "", "", "", Category) \
  \
  XX(GameArray, "select",function, shapeSelectVertex, GdShapeTypeId, GameScalar, "shape","index", "Returns vertex at given position [x,y,z,m]", "", "", "", "", Category) \
  XX(GameArray, "@",mocnina, shapeSelectVertex, GdShapeTypeId, GameScalar, "shape","index", "Returns vertex at given position [x,y,z,m]", "", "", "", "", Category) \
  XX(GameArray, "shapeBBox",function, shapeBBoxPart, GameScalar, GdShapeTypeId, "partIndex","shape", "Returns shape part bounding box. Result is returned as [left,top,right,bottom, minZ, maxZ, minM,maxM]. Note: function is slow. It is faster to store result into a variable and use it.","", "", "", "", Category) \
  XX(GameBool, "shapePointInside",function, shapePointInside,  GdShapeTypeId, GameArray,"shape","[x,y]", "Returns true, when give point is inside the shape.","", "", "", "", Category) \
  XX(GameArray, "shapeNearestInside",function, shapeNearestInside,  GdShapeTypeId, GameArray,"shape","[x,y]", "Returns position that is nearest inside point to the given point. If given points is inside, returns that point unchanged. Note: Returned point needn't pass PointInside test, because rounding error problem. ","", "", "", "", Category) \
  XX(GameArray, "shapeNearestEdge",function, shapeNearestEdge,  GdShapeTypeId, GameArray,"shape","[x,y]", "Returns nearest position that lies on edge. On polygon type, it is edge of the polygon, regardless whether point lies inside or outside of polygon. On other type of shape, it returns the same value as shapeNearestInside . ","", "", "", "", Category) \
  XX(GdShapeTypeId, "clip",function, shapeClip,  GdShapeTypeId, GameArray,"shape","[a,b,c]", "Clips the shape by bisector. Clipped shape is returned as result, original shape remains untouched. Bisector is defined as equation ax+by+c=0. Function can return NIL in case empty shape result (because shape cannot be empty)","_clipped=_shape clip [1,1,0]", "", "", "", Category) \
  XX(GdShapeTypeId, "crop",function, shapeCrop,  GdShapeTypeId, GameArray,"shape","[minX,minY,maxX,maxY]", "Crops the shape by specified rectangle. Result shape is whole inside of rectangle. Cropped shape is returned as result, original shape remains untouched. Function can return NIL in case empty shape result (because shape cannot be empty)","_clipped=_shape crop [-1,-1,2,3]", "", "", "", Category) \
  XX(GameNothing, "forEach",function, forEachShape, GameCode, GdShapeTypeId, "code","shape", "Process code for each point in the shape.", "", "", "", "", Category) \


#define FUNCTIONS_DEFAULT(XX, Category) \
  XX(GameString, "queryParam", queryParam, GameString, "name", "Returns value from database under defined name. If field doesn't exists, it is taken from constant parameters. If there is no constant parameter, it returns NIL", "queryParam \"density\"", "\"5\" (returned as string)", "", "", Category) \
  XX(GameString, "queryConstParam", queryConstParam, GameString, "name", "Returns value from constant parameters. If there is no constant parameter, it returns NIL", "queryConstParam \"density\"", "\"7\" (returned as string)", "", "", Category) \
  XX(GameString, "convertAlias", convertAlias, GameString, "alias", "Assumes that given name is alias and search resource table to search associated object name. Function returns the same value, if there is no conversion found.", "convertAlias \"tree\"", "\"picea_abies.p3d\"", "", "", Category) \
  XX(GameString, "getObjectType",getObjectType, GameScalar, "objectId","Gets object type (type is string contains name of object, or template name. ", "", "", "", "", Category) \
  XX(EvalType_Matrix, "getLocation",getLocation, GameScalar, "objectId","Gets object location", "", "", "", "", Category) \
  XX(GameScalar, "getTag",getTag, GameScalar, "objectId","Gets object tag", "", "", "", "", Category) \
  XX(GameScalar, "findNearest",findNearest, GameArray, "[x,y,<tag,<[skip ids]>>]","Finds nearest object to given coordinates. Optionally, you can specify target tag. You can also specify objects, that should be skipped, even if they nearest. Use Tag=0 for disabling tag filtering if you want to use \"skip\" array.", "findNearest [100,200,0,[1,4,5]]", "Nearest object at 100,200, no tag filtering, skip objects 1,4 and 5", "", "", Category) \
  XX(GameArray, "objectsInArea",objectsInAreaRect, GameArray, "[left,top, right,bottom,tag]","Finds object in specified rectangle. Optionally, you can specify target tag.  Use Tag=0 to disable tag filtering.", "", "", "", "", Category) \
  XX(GameArray, "objectsInArea",objectsInAreaShape, GdShapeTypeId, "shape","Finds object in specified shape.", "", "", "", "", Category) \
  XX(GameBool, "deleteObject",deleteObject, GameScalar, "objectId","Deletes object specified by ID. Released ID can be used for new object in future", "", "", "", "", Category) \
  \
  XX(GameScalar, "count",countVertices, GdShapeTypeId, "shape","Returns count of vertices of given shape", "", "", "", "", Category) \
  XX(GameArray, "shapeParts",shapeParts, GdShapeTypeId, "shape","Returns array of shape parts ", "", "", "", "", Category) \
  XX(GameString, "shapeType",getShapeType, GdShapeTypeId, "shape","Returns shape type. Shape type is returned as string. Valid results are 'point','multipoint','polyline','polygon'. But it is more safe to use predefined nulars, for example shpPoint, shpMultiPoint, shpPolyLine, shpPolygon", "", "", "", "", Category) \
  XX(GameArray, "shapeBBox",shapeBBox, GdShapeTypeId, "shape","Returns shape bounding box. Result is returned as [left,top,right,bottom,minZ,maxZ,minM,maxM]. Note: function is slow, it is faster to store result into the variable and use it.", "", "", "", "", Category) \
  \
  XX(GameNothing, "reportProgress",reportProgress, GameScalar, "p","Reports current working progress. Value is between 0 and 1 (0.76 = 76%). LandBuilder uses this value to calculate progress for whole task", "", "", "", "", Category) \


#define NULARS_DEFAULT(XX, Category) \
  XX(GdShapeTypeId, "activeShape", getActiveShape, "Returns active shape. It is recommended to store result (it is faster) instead accessing active shape through activeShape repeatedly.", "", "", "", "", Category) \
  XX(GameArray, "mapArea",getMapArea, "Returns [left,top,right,bottom] dimensions of map area", "", "", "", "", Category) \
  XX(GameString, "shpPoint",shpPoint, "Name of point type shape", "", "", "", "", Category) \
  XX(GameString, "shpMultiPoint",shpMultiPoint, "Name of multipoint type shape", "", "", "", "", Category) \
  XX(GameString, "shpPolyLine",shpPolyLine, "Name of polyline type shape", "", "", "", "", Category) \
  XX(GameString, "shpPolygon",shpPolygon, "Name of polygon type shape", "", "", "", "", Category) \


static GameOperator BinaryFuncts[]=
{
  OPERATORS_DEFAULT(REGISTER_OPERATOR, Category)
};

static GameFunction UnaryFuncts[]=
{
  FUNCTIONS_DEFAULT(REGISTER_FUNCTION, Category)
};

static GameNular NularyFuncts[]=
{
  NULARS_DEFAULT(REGISTER_NULAR, Category)
};

TYPES_LANDBUILDER(DEFINE_TYPE, "LandBuilder")

#if DOCUMENT_COMREF
static ComRefFunc DefaultComRefFunc[] =
{
    NULARS_DEFAULT(COMREF_NULAR, Category)
  FUNCTIONS_DEFAULT(COMREF_FUNCTION, Category)
    OPERATORS_DEFAULT(COMREF_OPERATOR, Category)
};

static ComRefType DefaultComRefType[] =
{
 TYPES_DEFAULT(COMREF_TYPE, Category)
 // TYPES_DEFAULT_COMB(COMREF_TYPE, "Default")
};
#endif

void GdLandBuild::RegisterToGameState(GameState *gState)
{
  GameState &state = *gState;  // helper to make macro works
  TYPES_LANDBUILDER(REGISTER_TYPE, LandBuilder)

  gState->NewOperators(BinaryFuncts,lenof(BinaryFuncts));
  gState->NewFunctions(UnaryFuncts,lenof(UnaryFuncts));
  gState->NewNularOps(NularyFuncts,lenof(NularyFuncts));
#if DOCUMENT_COMREF
  gState->AddComRefFunctions(DefaultComRefFunc,lenof(DefaultComRefFunc));
    gState->AddComRefTypes(DefaultComRefType,lenof(DefaultComRefType));
#endif
}


RString IGdShape::GetText() const
{
  RString res="[";
  const Shapes::IShape *shape=GetShapeRef();
  for (int i=0,count=shape->GetVertexCount();i<count;i++)
  {
    if (i!=0) res=res+",";
    char buff[256];    
    const Shapes::DVertex &vx=shape->GetVertex(i);
    sprintf(buff,"\t[%g,%g,%g,%g]\n",vx.x,vx.y,vx.z,vx.m);
    res=res+buff;
  }
  res=res+"]";
  return res;
}

bool IGdShape::IsEqualTo(const GameData *data) const 
{
  const IGdShape *other=dynamic_cast<const IGdShape *>(data);
  if (other)
  {
    return other->GetShapeRef()==GetShapeRef();
  }
  return false;
}
