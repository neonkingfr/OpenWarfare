#include "StdAfx.h"
#include ".\Transform2D.h"

#include "..\HighMapLoaders\include\EtHelperFunctions.h"

namespace LandBuildInt
{
	namespace Modules
	{
		void Transform2D::Run(IMainCommands* cmdIfc)
		{
			// gets shape and its properties
			m_pCurrentShape = cmdIfc->GetActiveShape(); 

			// gets parameters from calling cfg file
			if(GetGlobalParameters(cmdIfc))
			{
				// gets parameters from calling cfg file
				if(GetDbfParameters(cmdIfc))
				{
					// updates the homologous point list
					UpdateHomoPoints();

					// if all points have been loaded
					if(m_DbfIn.last)
					{
						Solve();
					}
				}
			}
		}

		// --------------------------------------------------------------------//

		bool Transform2D::GetGlobalParameters(IMainCommands* cmdIfc)
		{
			// gets parameters from calling cfg file
			const char* x = cmdIfc->QueryValue("transfType", false);
			if (x != 0)
			{
				if(strcmp(x, "RIGID") == 0)
				{
					m_GlobalIn.type = T2_RIGID;
				}
				else if(strcmp(x, "CONFORM") == 0)
				{
					m_GlobalIn.type = T2_CONFORM;
				}
				else if(strcmp(x, "AFFINE") == 0)
				{
					m_GlobalIn.type = T2_AFFINE;
				}
				else if(strcmp(x, "AFFINEEX") == 0)
				{
					m_GlobalIn.type = T2_AFFINEEX;
				}
				else if(strcmp(x, "HOMOGRAPHIC") == 0)
				{
					m_GlobalIn.type = T2_HOMOGRAPHIC;
				}
				else if(strcmp(x, "BILINEAR") == 0)
				{
					m_GlobalIn.type = T2_BILINEAR;
				}
				else if(strcmp(x, "POLYNOMIAL2") == 0)
				{
					m_GlobalIn.type = T2_POLYNOMIAL2;
				}
				else if(strcmp(x, "BESTFIT") == 0)
				{
					m_GlobalIn.type = T2_BESTFIT;
				}
				else 
				{
					ExceptReg::RegExcpt(Exception(11, ">>> Unknown transfType <<<"));
					return false;
				}
			}
			else
			{        
				ExceptReg::RegExcpt(Exception(12, ">>> No transfType specified <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------//

		bool Transform2D::GetDbfParameters(IMainCommands* cmdIfc)
		{
			// gets parameters from calling cfg file
			const char* x = cmdIfc->QueryValue("x", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.x = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(21, ">>> Bad data for x <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(22, ">>> Missing x value <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("y", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.y = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(23, ">>> Bad data for y <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(24, ">>> Missing y value <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("used", false);
			if (x != 0) 
			{
				if(IsBool(string(x), false))
				{
					int i = static_cast<int>(atoi(x));
					if (i == 0)
					{
						m_DbfIn.used = false;
					}
					else
					{
						m_DbfIn.used = true;
					}
				}
				else
				{
					ExceptReg::RegExcpt(Exception(25, ">>> Bad data for used <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(26, ">>> Missing used value <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("last", false);
			if (x != 0) 
			{
				if(IsBool(string(x), false))
				{
					int i = static_cast<int>(atoi(x));
					if (i == 0)
					{
						m_DbfIn.last = false;
					}
					else
					{
						m_DbfIn.last = true;
					}
				}
				else
				{
					ExceptReg::RegExcpt(Exception(27, ">>> Bad data for last <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(28, ">>> Missing last value <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------//

		void Transform2D::UpdateHomoPoints()
		{
			if(m_DbfIn.used)
			{
				Shapes::DVector p = m_pCurrentShape->GetVertex(0);

				Point2<double> p1(p.x, p.y);
				Point2<double> p2(m_DbfIn.x, m_DbfIn.y);
				HomologousPoint2<double> hp(p1, p2);
				m_HomoPoints.AddPoint(hp);
			}
		}

		// --------------------------------------------------------------------//

		bool Transform2D::Solve()
		{
			Transform2Manager<double> trans;
			trans.SetHomoPointsList(m_HomoPoints);

			if(trans.ChooseTransformation(m_GlobalIn.type))
			{
				printf(">>> ------------------------------------------------------ <<<\n");
				printf(">>> Applying transformation: ");
				printf(trans.GetTransformationName().c_str());
				printf(" <<<\n");
				printf(">>> ------------------------------------------------------ <<<\n");

		        Shapes::VertexArray* vxarray = const_cast<Shapes::VertexArray*>(m_pCurrentShape->GetVertexArray());

				vector<Point2<double> > pointsIn;

				for(Shapes::VertexArray::Iter iter = vxarray->First(); iter; ++iter)
				{
					const Shapes::DVertex& vx = iter.GetVertex();
					Point2<double> p(vx.x, vx.y);
					pointsIn.push_back(p);
				}

				vector<Point2<double> > pointsOut = trans.ApplyTransformation(pointsIn);

				int counter = 0;
				for(Shapes::VertexArray::Iter iter = vxarray->First(); iter; ++iter)
				{
					const Shapes::DVertex& vx = iter.GetVertex();
					Shapes::DVertex nx(pointsOut[counter].X(), pointsOut[counter].Y());
					iter.SetVertex(nx);
					counter++;
				}

				return true;
			}
			else
			{
				printf(">>> ------------------------------------------------------ <<<\n");
				printf(">>> Error while applying transformation: ");
				printf(trans.GetTransformationName().c_str());
				printf(" <<<\n");
				printf(">>> ------------------------------------------------------ <<<\n");
				return false;
			}
		}
	}
}

