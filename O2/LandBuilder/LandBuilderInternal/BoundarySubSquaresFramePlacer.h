#pragma once

#include "IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>

#include "..\Shapes\ShapeHelpers.h"

namespace LandBuildInt
{
	namespace Modules
	{
		class BoundarySubSquaresFramePlacer : public IBuildModule
		{
		public:
			struct GlobalInput
			{
				unsigned int randomSeed;
			};

			struct DbfInput
			{
				double subSquaresSize;
				double maxNoise;
				double minDistance;
			};

			struct ObjectInput
			{
				RString m_Name;
				double  m_Prob;
				double  m_NormProb;
				double  m_MinHeight;
				double  m_MaxHeight;
				int     m_Counter;

				ObjectInput() {}
				ObjectInput(RString name, double prob, double minHeight, double maxHeight)
				{
					m_Name        = name;
					m_Prob        = prob;
					m_MinHeight   = minHeight;
					m_MaxHeight   = maxHeight;
					m_Counter     = 0;
				}

				ClassIsMovable(ObjectInput);
			};

		public:
			virtual void Run(IMainCommands*);
			ModuleObjectOutputArray* RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn, 
												  AutoArray<BoundarySubSquaresFramePlacer::ObjectInput, MemAllocStack<BoundarySubSquaresFramePlacer::ObjectInput, 32> >& objectsIn);

			class Exception : public ExceptReg::IException
			{
				unsigned int code;
				const char* text;

			public:
				Exception(unsigned int code, const char* text) : code(code), text(text) {}
				virtual unsigned int GetCode() const { return code; }
				virtual const _TCHAR* GetDesc() const { return text; }
				virtual const _TCHAR* GetModule() const { return "BoundarySubSquaresFramePlacer"; }
				virtual const _TCHAR* GetType() const { return "BoundarySubSquaresFramePlacerException"; }
			};

		private:
			const Shapes::IShape* m_pCurrentShape;      

			AutoArray<ObjectInput, MemAllocStack<ObjectInput, 32> > m_ObjectsIn;
			ModuleObjectOutputArray                                 m_ObjectsOut;

			GlobalInput m_GlobalIn;
			DbfInput    m_DbfIn;

			bool GetGlobalParameters(IMainCommands* cmdIfc);
			bool GetDbfParameters(IMainCommands* cmdIfc);
			bool GetObjectsParameters(IMainCommands* cmdIfc);

			bool NormalizeProbs();

			void CreateObjects(bool preview);
			int  GetRandomType();
			void ExportCreatedObjects(IMainCommands* cmdIfc);
			void WriteReport();
		};
	}
}
