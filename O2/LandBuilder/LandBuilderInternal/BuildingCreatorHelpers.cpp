#include "StdAfx.h"
#include ".\BuildingCreatorHelpers.h"

#include "..\Shapes\ShapeHelpers.h"

#include <projects/ObjektivLib/ObjToolTopology.h>

namespace BC_Help
{
	// -------------------------------------------------------------------------- //
	// Dxf2006Writer
	// -------------------------------------------------------------------------- //

	Dxf2006Writer::Dxf2006Writer()
	{
	}

	// -------------------------------------------------------------------------- //

	void Dxf2006Writer::Set(const string& filename)
	{
		m_File.open(filename.c_str(), std::ios::out | std::ios::trunc);
		if (!m_File.fail()) 
		{
			WriteEntitiesHead();
		}
		else
		{
			// TODO: some error message
		}
	}

	// -------------------------------------------------------------------------- //

	Dxf2006Writer::~Dxf2006Writer()
	{
		if (!m_File.fail()) 
		{
			WriteEntitiesTail();
			WriteFileTail();
			m_File.close();
		}
	}

	// -------------------------------------------------------------------------- //

	void Dxf2006Writer::WriteEntitiesHead()
	{
		if (!m_File.fail()) 
		{
			string out = "  0\nSECTION\n  2\nENTITIES";
			m_File << out << endl;
		}
	}

	// -------------------------------------------------------------------------- //

	void Dxf2006Writer::WriteEntitiesTail()
	{
		if (!m_File.fail()) 
		{
			string out = "  0\nENDSEC";
			m_File << out << endl;
		}
	}

	// -------------------------------------------------------------------------- //

	void Dxf2006Writer::WriteFileTail()
	{
		if (!m_File.fail()) 
		{
			string out = "  0\nEOF";
			m_File << out << endl;
		}
	}

	// -------------------------------------------------------------------------- //

	void Dxf2006Writer::WritePolyline(const vector<Shapes::DVertex>& polyline, 
									  const string& layer, bool close, 
									  double startingWidth, 
									  double endingWidth)
	{
		if (!m_File.fail()) 
		{
			unsigned int size = polyline.size();

			if (size > 1)
			{
				char buffer[80];
				string out = "  0\nPOLYLINE\n100\nAcDbEntity\n  8\n";
				out += (layer + "\n 66\n     1\n100\nAcDb2dPolyline\n 10\n0.0\n 20\n0.0\n 30\n0.0\n");
				if (close)
				{
					out += " 70\n1\n";
				}
				else
				{
					out += " 70\n0\n";
				}
				_gcvt_s(buffer, 80, startingWidth, 8);
				out += (" 40\n" + string(buffer) +"\n");
				_gcvt_s(buffer, 80, endingWidth, 8);
				out += (" 41\n" + string(buffer) +"\n");
				for (unsigned int i = 0; i < size; ++i)
				{
					out += "  0\nVERTEX\n100\nAcDbVertex\n  8\n";
					out += (layer + "\n100\nAcDb2dVertex\n 10\n");
					_gcvt_s(buffer, 80, polyline[i].x, 8);
					out += (string(buffer) + "\n 20\n");
					_gcvt_s(buffer, 80, polyline[i].y, 8);
					out += (string(buffer) + "\n 30\n0.0\n");
				}
				out += "  0\nSEQEND\n  8\n";
				out += layer;
				m_File << out << endl;
			}
		}
	}

	// -------------------------------------------------------------------------- //
	// Line
	// -------------------------------------------------------------------------- //

	Line::Line(const Shapes::DVertex& v1, const Shapes::DVertex& v2)
	: m_V1(v1)
	, m_V2(v2)
	{
		CalculateCoefficients();
	}

	// -------------------------------------------------------------------------- //

	bool Line::Intersection(const Line& other, Shapes::DVertex& intPoint) const
	{
		double den = m_B * other.m_A - m_A * other.m_B;
		if (abs(den) < EPSILON) 
		{
			intPoint.x = DBL_MAX;
			intPoint.y = DBL_MAX;
			return false;
		}

		double numX = m_C * other.m_B - m_B * other.m_C;
		double numY = m_A * other.m_C - m_C * other.m_A;

		intPoint.x = numX / den;
		intPoint.y = numY / den;

		return true;
	}

	// -------------------------------------------------------------------------- //

	void Line::CalculateCoefficients()
	{
		double dx = m_V2.x - m_V1.x;
		double dy = m_V2.y - m_V1.y;

		if (abs(dx) < EPSILON)
		{
			if (abs(dy) < EPSILON)
			{
				m_A = 0.0;
				m_B = 0.0;
				m_C = 0.0;
			}
			else
			{
				m_A = 1.0;
				m_B = 0.0;
				m_C = -m_V1.x;
			}
		}
		else if (abs(dy) < EPSILON)
		{
			m_A = 0.0;
			m_B = 1.0;
			m_C = -m_V1.y;
		}
		else
		{
			m_A = 1.0 / dx;
			m_B = -1.0 / dy;
			m_C = - m_V1.x / dx + m_V1.y / dy;
		}
	}

	// -------------------------------------------------------------------------- //
	// BoundingBox
	// -------------------------------------------------------------------------- //

	BoundingBox::BoundingBox()
	: m_Width(DBL_MAX)
	, m_Height(DBL_MAX)
	, m_Orientation(0.0)
	, m_BaseSegmentIndex(-1)
	{
	}

	// -------------------------------------------------------------------------- //

	double BoundingBox::GetWidth() const
	{
		return m_Width;
	}
	
	// -------------------------------------------------------------------------- //

	double BoundingBox::GetHeight() const
	{
		return m_Height;
	}

	// -------------------------------------------------------------------------- //

	double BoundingBox::GetOrientation() const
	{
		return m_Orientation;
	}

	// -------------------------------------------------------------------------- //

	int BoundingBox::GetBaseSegmentIndex() const
	{
		return m_BaseSegmentIndex;
	}

	// -------------------------------------------------------------------------- //

	void BoundingBox::Reset()
	{
		m_Width            = DBL_MAX;
		m_Height           = DBL_MAX;
		m_Orientation      = 0.0;
		m_BaseSegmentIndex = -1;
	}

	// -------------------------------------------------------------------------- //

	void BoundingBox::CalculateFromVertices(const vector<Shapes::DVertex>& vertices)
	{
		unsigned int cnt = vertices.size();
		double minArea = DBL_MAX;

		Shapes::DVertex p0 = vertices[0];

		for (unsigned int i = 0, j = (cnt - 1); i < cnt; j = i++)
		{
			const Shapes::DVertex& pJ = vertices[j];
			const Shapes::DVertex& pI = vertices[i];

			Shapes::DVector vJI(pI, pJ);
			vJI.NormalizeXYInPlace();

			Shapes::DVector vPerp = vJI.PerpXYCCW();

			double w0 = 0.0;
			double w1 = 0.0;
			double h0 = 0.0;
			double h1 = 0.0;

			for (unsigned int k = 1; k < cnt; ++k)
			{
				const Shapes::DVertex& pK = vertices[k];
				Shapes::DVector vK0(pK, p0);

				double testW = vJI.DotXY(vK0);
				if (testW < w0)
				{
					w0 = testW;
				}
				else if (testW > w1)
				{
					w1 = testW;
				}

				double testH = vPerp.DotXY(vK0);
				if (testH < h0)
				{
					h0 = testH;
				}
				else if (testH > h1)
				{
					h1 = testH;
				}
			}

			double tempWidth = w1 - w0;
			double tempHeight  = h1 - h0; 
			double area = tempWidth * tempHeight;
			if (area < minArea)
			{
				minArea            = area;
				m_Width            = tempWidth;
				m_Height           = tempHeight;
				m_Orientation      = vJI.DirectionXY(); 
				m_BaseSegmentIndex = j;
			}		
		}
	}

	// -------------------------------------------------------------------------- //
	// Footprint
	// -------------------------------------------------------------------------- //

	Footprint::Footprint()
	: m_TurningSequence("")
	{
	}

	// -------------------------------------------------------------------------- //

	Footprint::Footprint(const vector<Shapes::DVertex>& vertices, 
						 double delVertexTol, double rectCornerTol, bool rectify)
	: m_Vertices(vertices)
	, m_TurningSequence("")
	, m_DelVertexTol(delVertexTol / 180 * PI)
	, m_RectCornerTol(rectCornerTol / 180 * PI)
	{
		RemoveDuplicatedPoints();
		SetVerticesOrderToCCW();
		SetLongestEdgeAsFirst();
		DeleteMidSegmentVertices();
		if (rectify)
		{
			RectifyCorners();
		}
	}

	// -------------------------------------------------------------------------- //

	const vector<Shapes::DVertex>& Footprint::GetVertices() const
	{
		return m_Vertices;
	}

	// -------------------------------------------------------------------------- //

	vector<Shapes::DVertex>& Footprint::GetVertices()
	{
		return m_Vertices;
	}

	// -------------------------------------------------------------------------- //

	Shapes::DVertex& Footprint::GetVertex(unsigned int index)
	{
		assert(index < m_Vertices.size());
		return m_Vertices[index];
	}

	// -------------------------------------------------------------------------- //

	const Shapes::DVertex& Footprint::GetVertex(unsigned int index) const
	{
		assert(index < m_Vertices.size());
		return m_Vertices[index];
	}

	// -------------------------------------------------------------------------- //

	Shapes::DVertex& Footprint::operator [] (unsigned int index)
	{
		assert(index < m_Vertices.size());
		return m_Vertices[index];
	}

	// -------------------------------------------------------------------------- //

	const Shapes::DVertex& Footprint::operator [] (unsigned int index) const
	{
		assert(index < m_Vertices.size());
		return m_Vertices[index];
	}

	// -------------------------------------------------------------------------- //

	unsigned int Footprint::GetVerticesCount() const
	{
		return m_Vertices.size();
	}

	// -------------------------------------------------------------------------- //

	vector<double> Footprint::GetTurningFunction()
	{
		if (m_TurningFunction.size() == 0)
		{
			CalculateTurningFunction();
		}

		return m_TurningFunction;
	}

	// -------------------------------------------------------------------------- //

	string Footprint::GetTurningSequence()
	{
		if (m_TurningSequence == "")
		{
			CalculateTurningSequence();
		}

		return m_TurningSequence;
	}

	// -------------------------------------------------------------------------- //

	void Footprint::RemoveDuplicatedPoints()
	{
		double proximityTol = 0.5; // half meter

		vector<unsigned int> toBeDeleted;

		unsigned int cnt = m_Vertices.size();
		for (unsigned int i = 0, j = (cnt - 1); i < cnt; j = i++)
		{
			const Shapes::DVertex& prevV = m_Vertices[j];
			const Shapes::DVertex& curV  = m_Vertices[i];

			if (prevV.DistanceXY(curV) < proximityTol)
			{
				toBeDeleted.push_back(i);
			}
		}

		if (toBeDeleted.size() > 0)
		{
			for (int i = toBeDeleted.size() - 1; i >= 0; --i)
			{
				m_Vertices.erase(m_Vertices.begin() + toBeDeleted[i]);
			}

			SetLongestEdgeAsFirst();
			m_BoundingBox.Reset();
			m_TurningFunction.clear();
			m_TurningSequence = "";
		}
	}

	// -------------------------------------------------------------------------- //

	void Footprint::SetVerticesOrderToCCW()
	{
		if (SignedArea() < 0.0)
		{
			ReverseVerticesOrder();
		}
	}

	// -------------------------------------------------------------------------- //

	void Footprint::DeleteMidSegmentVertices()
	{
		if (m_TurningFunction.size() == 0)
		{
			CalculateTurningFunction();
		}

		vector<unsigned int> toBeDeleted;

		double diff; 
		for (unsigned int i = 0, cnt = m_Vertices.size(); i < cnt; ++i)
		{
			if (i == 0)
			{
				diff = m_TurningFunction[i] - m_TurningFunction[cnt - 1]; 
			}
			else
			{
				diff = m_TurningFunction[i] - m_TurningFunction[i - 1]; 
			}

			if (abs(diff) < m_DelVertexTol)
			{
				toBeDeleted.push_back(i);
			}
		}

		if (toBeDeleted.size() > 0)
		{
			for (int i = toBeDeleted.size() - 1; i >= 0; --i)
			{
				m_Vertices.erase(m_Vertices.begin() + toBeDeleted[i]);
			}

			SetLongestEdgeAsFirst();
			m_BoundingBox.Reset();
			m_TurningFunction.clear();
			m_TurningSequence = "";
		}
	}

	// -------------------------------------------------------------------------- //

	void Footprint::RectifyCorners()
	{
		unsigned int cnt = m_Vertices.size();

		// we cannot rectify a triangle
		if (cnt < 4) return;

		double min90  = HALF_PI - m_RectCornerTol;
		double max90  = HALF_PI + m_RectCornerTol;
		double min270 = 3.0 * HALF_PI - m_RectCornerTol;
		double max270 = 3.0 * HALF_PI + m_RectCornerTol;

		unsigned int iPrev;
		unsigned int iCur;
		unsigned int iNext;
		unsigned int iNextNext;

		bool modified = false;

		for (unsigned int i = 0; i < cnt; ++i)
		{
			if (i == 0)
			{
				iPrev     = cnt - 1;
				iCur      = i;
				iNext     = i + 1;
				iNextNext = i + 2;
			}
			else if (i == (cnt - 1))
			{
				iPrev     = i - 1;
				iCur      = i;
				iNext     = 0;
				iNextNext = 1;
			}
			else if (i == (cnt - 2))
			{
				iPrev     = i - 1;
				iCur      = i;
				iNext     = i + 1;
				iNextNext = 0;
			}
			else
			{
				iPrev     = i - 1;
				iCur      = i;
				iNext     = i + 1;
				iNextNext = i + 2;
			}

			Shapes::DVertex vPrev(m_Vertices[iPrev]);
			Shapes::DVertex vCur(m_Vertices[iCur]);
			Shapes::DVertex vNext(m_Vertices[iNext]);
			Shapes::DVertex vNextNext(m_Vertices[iNextNext]);

			Shapes::DVector vecPrev = Shapes::DVector(vCur, vPrev);
			Shapes::DVector vecCur  = Shapes::DVector(vNext, vCur);

			double dirP = vecPrev.DirectionXY();
			double dirC = vecCur.DirectionXY();

			double diffCP = dirC - dirP;

			if (diffCP < 0.0) diffCP += TWO_PI;

			double diff = 0.0;

			if (min90 < diffCP && diffCP < max90)
			{
				diff = diffCP - HALF_PI;
			}
			else if (min270 < diffCP && diffCP < max270)
			{
				diff = diffCP - 3.0 * HALF_PI;
			}

			if (diff != 0.0)
			{
				Shapes::DVertex midPoint(0.5 * (vCur.x + vNext.x), 0.5 * (vCur.y + vNext.y));
				vecCur.RotateXY(-diff);

				Line linePrev(vPrev, vCur);
				Line lineCur(midPoint, vecCur.GetSecondEndXY(midPoint));
				Line lineNext(vNext, vNextNext);

				Shapes::DVertex newCur;
				Shapes::DVertex newNext;

				if (lineCur.Intersection(linePrev, newCur))
				{
					if (lineCur.Intersection(lineNext, newNext))
					{
						if (newCur.DistanceXY(vCur) < newCur.DistanceXY(vPrev))
						{
							if (newNext.DistanceXY(vNext) < newNext.DistanceXY(vNextNext))
							{
								m_Vertices[iCur]  = newCur;
								m_Vertices[iNext] = newNext;
								modified = true;
							}
						}
					}
				}
			}
		}

		if (modified)
		{
			m_BoundingBox.Reset();
			m_TurningFunction.clear();
			m_TurningSequence = "";
		}
	}

	// -------------------------------------------------------------------------- //

	double Footprint::GetMinBoundingBoxOrientation()
	{
		if (m_BoundingBox.GetWidth() == DBL_MAX)
		{
			m_BoundingBox.CalculateFromVertices(m_Vertices);
		}
		return m_BoundingBox.GetOrientation();
	}

	// -------------------------------------------------------------------------- //

	void Footprint::ResetBoundingBox()
	{
		m_BoundingBox.Reset();
	}

	// -------------------------------------------------------------------------- //

	double Footprint::SignedArea() const
	{
		double area = 0.0;
		unsigned int cnt = m_Vertices.size();
		// calculates area (gauss algorithm)
		for (unsigned int i = 0, j = (cnt - 1); i < cnt; j = i++)
		{
			Shapes::DVertex v1 = m_Vertices[j];
			Shapes::DVertex v2 = m_Vertices[i];

			area += v1.x * v2.y - v1.y * v2.x;
		}
		return (0.5 * area);
	}

	// -------------------------------------------------------------------------- //

	void Footprint::ReverseVerticesOrder()
	{
		vector<Shapes::DVertex> tempList;

		int size = m_Vertices.size();
		for (int i = size - 1; i >= 0; --i)
		{
			tempList.push_back(m_Vertices[i]);
		}

		m_Vertices = tempList;
		m_BoundingBox.Reset();
		m_TurningFunction.clear();
		m_TurningSequence = "";
	}

	// -------------------------------------------------------------------------- //

	void Footprint::SetLongestEdgeAsFirst()
	{
		double maxLen      = 0.0;
		unsigned int index = 0;

		unsigned int cnt = m_Vertices.size();

		for (unsigned int i = 0, j = (cnt - 1); i < cnt; j = i++)
		{
			Shapes::DVertex v1 = m_Vertices[j];
			Shapes::DVertex v2 = m_Vertices[i];

			double len = v1.DistanceXY(v2);
			if (len > maxLen)
			{
				maxLen = len;
				index = j;
			}
		}

		if (index != 0)
		{
			vector<Shapes::DVertex> tempList;

			for (unsigned int i = index; i < cnt; ++i)
			{
				tempList.push_back(m_Vertices[i]);
			}
			for (unsigned int i = 0; i < index; ++i)
			{
				tempList.push_back(m_Vertices[i]);
			}

			m_Vertices = tempList;
			m_BoundingBox.Reset();
			m_TurningFunction.clear();
			m_TurningSequence = "";
		}
	}

	// -------------------------------------------------------------------------- //

	void Footprint::CalculateTurningFunction()
	{
		m_TurningFunction.clear();

		if (m_Vertices.size() == 0) return;

		unsigned int cnt = m_Vertices.size();

		for (unsigned int i = 0; i < (cnt - 1); ++i)
		{
			const Shapes::DVertex& v0 = m_Vertices[i];
			const Shapes::DVertex& v1 = m_Vertices[i + 1];
			Shapes::DVector v(v1, v0);
			m_TurningFunction.push_back(v.DirectionXY());
		}
		const Shapes::DVertex& v0 = m_Vertices[cnt - 1];
		const Shapes::DVertex& v1 = m_Vertices[0];
		Shapes::DVector v(v1, v0);
		m_TurningFunction.push_back(v.DirectionXY());
	}

	// -------------------------------------------------------------------------- //

	void Footprint::CalculateTurningSequence()
	{
		m_TurningSequence = "";
		if (m_Vertices.size() < 3) return;

		for (unsigned int i = 0, cnt = m_Vertices.size(); i < cnt; ++i)
		{
			Shapes::DVector v0;
			Shapes::DVector v1;

			if (i == 0)
			{
				v0 = Shapes::DVector(m_Vertices[0], m_Vertices[cnt - 1]);
				v1 = Shapes::DVector(m_Vertices[1], m_Vertices[0]);
			}
			else if (i == cnt - 1)
			{
				v0 = Shapes::DVector(m_Vertices[cnt - 1], m_Vertices[cnt - 2]);
				v1 = Shapes::DVector(m_Vertices[0], m_Vertices[cnt - 1]);
			}
			else
			{
				v0 = Shapes::DVector(m_Vertices[i], m_Vertices[i - 1]);
				v1 = Shapes::DVector(m_Vertices[i + 1], m_Vertices[i]);
			}

			Shapes::DVector cross = v0.Cross(v1);

			if (cross.z == 0.0)
			{
				m_TurningSequence += "S"; // straight
			}
			else if (cross.z > 0.0)
			{
				m_TurningSequence += "L"; // left turn
			}
			else
			{
				m_TurningSequence += "R"; // right turn
			}
		}
	}

	// -------------------------------------------------------------------------- //

	void Footprint::RotateAboutVertex(const Shapes::DVertex& v, double angle)
	{
		if (angle != 0.0)
		{
			double x;
			double y;
			for (unsigned int i = 0, cnt = m_Vertices.size(); i < cnt; ++i)
			{
				x = m_Vertices[i].x - v.x;
				y = m_Vertices[i].y - v.y;
				m_Vertices[i].x = v.x + x * cos(angle) + y * sin(angle);
				m_Vertices[i].y = v.y - x * sin(angle) + y * cos(angle);
			}
		}
	}

	// -------------------------------------------------------------------------- //

	Shapes::DVertex Footprint::GetBarycenter() const
	{
		Shapes::DVertex barycenter(0.0, 0.0, 0.0);

		for (unsigned int i = 0, cnt = m_Vertices.size(); i < cnt; ++i)
		{
			barycenter += m_Vertices[i];
		}
		barycenter /= m_Vertices.size();
		return barycenter;
	}

	// -------------------------------------------------------------------------- //
	
	Shapes::DVertex Footprint::GetMinPoint() const
	{
		Shapes::DVertex minV = m_Vertices[0];

		for (unsigned int i = 1, cnt = m_Vertices.size(); i < cnt; ++i)
		{
			if (minV.x > m_Vertices[i].x)
			{
				minV.x = m_Vertices[i].x;
			}
			if (minV.y > m_Vertices[i].y)
			{
				minV.y = m_Vertices[i].y;
			}
			if (minV.z > m_Vertices[i].z)
			{
				minV.z = m_Vertices[i].z;
			}
		}

		return minV;
	}

	// -------------------------------------------------------------------------- //

	double Footprint::GetMinBoundingBoxWidth()
	{
		if (m_BoundingBox.GetWidth() == DBL_MAX)
		{
			m_BoundingBox.CalculateFromVertices(m_Vertices);
		}
		return m_BoundingBox.GetWidth();
	}

	// -------------------------------------------------------------------------- //

	double Footprint::GetMinBoundingBoxHeight()
	{
		if (m_BoundingBox.GetWidth() == DBL_MAX)
		{
			m_BoundingBox.CalculateFromVertices(m_Vertices);
		}
		return m_BoundingBox.GetHeight();
	}

	// -------------------------------------------------------------------------- //
	// Helper functions
	// -------------------------------------------------------------------------- //

	double InternalAngleAt(const vector<Shapes::DVertex>& polygon, unsigned int index, bool left)
	{
		unsigned int cnt = polygon.size();

		Shapes::DVector vecPrev;
		Shapes::DVector vecNext;

		if (index == 0)
		{
			vecPrev = Shapes::DVector(polygon[cnt - 1], polygon[0]);
			vecNext = Shapes::DVector(polygon[1], polygon[0]);
		}
		else if (index == cnt - 1)
		{
			vecPrev = Shapes::DVector(polygon[cnt - 2], polygon[cnt - 1]);
			vecNext = Shapes::DVector(polygon[0], polygon[cnt - 1]);
		}
		else
		{
			vecPrev = Shapes::DVector(polygon[index - 1], polygon[index]);
			vecNext = Shapes::DVector(polygon[index + 1], polygon[index]);
		}

		double angle = vecNext.DirectionXY() - vecPrev.DirectionXY();

		if (angle < 0.0) angle += TWO_PI;

		if (left)
		{
			angle = TWO_PI - angle;
		}

		return angle;
	}

	// -------------------------------------------------------------------------- //

	bool Offset(vector<Shapes::DVertex>& polygon, double offset, bool left)
	{
		unsigned int cnt = polygon.size();

		vector<Shapes::DVertex> newPolygon(polygon);

		for (unsigned int i = 0; i < cnt; ++i)
		{
			double angle = InternalAngleAt(polygon, i, left);

			if (angle == 0.0 || angle == TWO_PI)
			{
				polygon.clear();
				return false;
			}

			// TODO: we miss the check of the offset value to see if it is
			//       valid (if it does not give a point deletion)

			double sin_HalfA = sin(angle / 2.0);

			Shapes::DVector vecPrev;
			Shapes::DVector vecNext;

			if (i == 0)
			{
				vecPrev = Shapes::DVector(polygon[0], polygon[cnt - 1]);
				vecNext = Shapes::DVector(polygon[1], polygon[0]);
			}
			else if (i == cnt - 1)
			{
				vecPrev = Shapes::DVector(polygon[cnt - 1], polygon[cnt - 2]);
				vecNext = Shapes::DVector(polygon[0], polygon[cnt - 1]);
			}
			else
			{
				vecPrev = Shapes::DVector(polygon[i], polygon[i - 1]);
				vecNext = Shapes::DVector(polygon[i + 1], polygon[i]);
			}

			Shapes::DVector vecPerpPrev;
			Shapes::DVector vecPerpNext;

			if (left)
			{
				vecPerpPrev = vecPrev.PerpXYCCW();
				vecPerpNext = vecNext.PerpXYCCW();
			}
			else
			{
				vecPerpPrev = vecPrev.PerpXYCW();
				vecPerpNext = vecNext.PerpXYCW();
			}

			vecPerpPrev.NormalizeXYInPlace();
			vecPerpNext.NormalizeXYInPlace();
			Shapes::DVector vecMean = (vecPerpPrev + vecPerpNext) / 2.0;
			vecMean.NormalizeXYInPlace();
			vecMean *= (offset * 1.0 / sin_HalfA);

			newPolygon[i] = vecMean.GetSecondEndXY(polygon[i]);
		}

		polygon = newPolygon;

		return true;
	}

	// -------------------------------------------------------------------------- //
	// Texture
	// -------------------------------------------------------------------------- //
		
	Texture::Texture(RString pathname, RString filename, unsigned int size)
	: m_Pathname(pathname)
	, m_Filename(filename)
	, m_Size(size)
	{
	}

	// -------------------------------------------------------------------------- //

	Texture::~Texture()
	{
	}

	// -------------------------------------------------------------------------- //

	RString Texture::GetPathname() const
	{
		return m_Pathname;
	}

	// -------------------------------------------------------------------------- //

	RString Texture::GetFilename() const
	{
		return m_Filename;
	}

	// -------------------------------------------------------------------------- //

	unsigned int Texture::GetSize() const
	{
		return m_Size;
	}

	// -------------------------------------------------------------------------- //
	// ModelP3D
	// -------------------------------------------------------------------------- //

	BuildingModelP3D::BuildingModelP3D()
	{
	}

	// -------------------------------------------------------------------------- //

	BuildingModelP3D::BuildingModelP3D(RString filename, const Footprint& footprint, 
									   unsigned int numOfFloors, double floorHeight,
									   unsigned int roofType, RString modelPath, 
									   RString texturesPath, const vector<Texture>& textures)
	{
		Create(filename, footprint, numOfFloors, floorHeight, roofType, modelPath,
			   texturesPath, textures);
	}

	// -------------------------------------------------------------------------- //

	void BuildingModelP3D::Create(RString filename, const Footprint& footprint, 
								  unsigned int numOfFloors, double floorHeight,
								  unsigned int roofType, RString modelPath, 
								  RString texturesPath, const vector<Texture>& textures)
	{
		m_Filename     = filename;
		m_Footprint    = footprint;
		m_NumOfFloors  = numOfFloors;
		m_FloorHeight  = floorHeight;
		m_RoofType     = roofType;
		m_ModelPath    = modelPath;
		m_TexturesPath = texturesPath;
		m_Textures     = textures;

		PREDEF_PROC_TEXT_COUNT = 6;
		ProcTextures.push_back("#(argb,8,8,3)color(0.45,0.45,0.45,1.0,CO)");
		ProcTextures.push_back("#(argb,8,8,3)color(0.55,0.55,0.55,1.0,CO)"); 
		ProcTextures.push_back("#(argb,8,8,3)color(0.62,0.58,0.49,1.0,CO)");
		ProcTextures.push_back("#(argb,8,8,3)color(0.87,0.81,0.65,1.0,CO)"); 
		ProcTextures.push_back("#(argb,8,8,3)color(0.66,0.60,0.39,1.0,CO)");
		ProcTextures.push_back("#(argb,8,8,3)color(0.65,0.69,0.73,1.0,CO)");
		ProcTextures.push_back("#(argb,8,8,3)color(0.49,0.45,0.40,1.0,CO)");
		ProcTextures.push_back("#(argb,8,8,3)color(0.44,0.30,0.25,1.0,CO)"); 
		ProcTextures.push_back("#(argb,8,8,3)color(0.59,0.60,0.74,1.0,CO)");
		ProcTextures.push_back("#(argb,8,8,3)color(0.16,0.20,0.56,1.0,CO)"); 
		ProcTextures.push_back("#(argb,8,8,3)color(0.38,0.51,0.45,1.0,CO)");
		ProcTextures.push_back("#(argb,8,8,3)color(0.39,0.42,0.37,1.0,CO)");

		BaseProcTexture = "#(argb,8,8,3)color(1.0,1.0,1.0,1.0,CO)";
		RoofProcTexture = "#(argb,8,8,3)color(0.65,0.39,0.31,1.0,CO)";

		if (m_NumOfFloors < 1) m_NumOfFloors = 1;
		if (m_FloorHeight < 2.0) m_FloorHeight = 2.0;

		SetOrigin();
		OrientateAsMinBoundingBox();
		CorrectOrigin();
	}

	// -------------------------------------------------------------------------- //

	Shapes::DVertex BuildingModelP3D::GetPosition() const
	{
		return m_Position;
	}

	// -------------------------------------------------------------------------- //

	double BuildingModelP3D::GetOrientation() const
	{
		return m_Orientation;
	}

	// -------------------------------------------------------------------------- //

	void BuildingModelP3D::ExportToP3DFile()
	{
		LODObject lodObj;

		ObjectData* obj = new ObjectData;
		if (m_Textures.size() > 0)
		{
			GenerateLOD(*obj, true);
		}
		else
		{
			GenerateLOD(*obj, true, true);
		}

		lodObj.InsertLevel(obj, 1.0);

		lodObj.DeleteLevel(0);

		obj = new ObjectData;
		GenerateGeometryLOD(*obj);
		obj->SetNamedProp("class", "house");
		obj->SetNamedProp("map", "house");
		obj->SetNamedProp("dammage", "building");
		obj->SetNamedProp("sbsource", "shadowvolume");
		obj->SetNamedProp("prefershadowvolume", "0");
		lodObj.InsertLevel(obj, LOD_GEOMETRY);

		obj = new ObjectData;
		GenerateViewGeometryLOD(*obj);
		lodObj.InsertLevel(obj, LOD_VIEWGEO);

		obj = new ObjectData;
		GenerateFireGeometryLOD(*obj);
		lodObj.InsertLevel(obj, LOD_FIREGEO);

		obj = new ObjectData;
		GenerateRoadwayLOD(*obj);
		lodObj.InsertLevel(obj, LOD_ROADWAY);

		obj = new ObjectData;
		GenerateShadowLOD(*obj);
		lodObj.InsertLevel(obj, LOD_GET_SHADOW(0.0));

		Pathname path = m_ModelPath + m_Filename + ".p3d";
		if (lodObj.Save(path, OBJDATA_LATESTVERSION, false, 0, 0) != 0) 
		{
			throw new std::exception("Unable to save output file (p3d)");
		}
	}

	// -------------------------------------------------------------------------- //

	const vector<CG_Polygon2>& BuildingModelP3D::GetPartitions() const
	{
		return m_Partitions;
	}

	// -------------------------------------------------------------------------- //

	void BuildingModelP3D::GenerateLOD(ObjectData& obj, bool textureLOD, 
									   bool genProceduralTexture)
	{
		int counter = -1;
		unsigned int cnt = m_Footprint.GetVerticesCount();
		for (unsigned int i = 0; i <= m_NumOfFloors + 1; ++i)
		{
			for (unsigned int j = 0; j < cnt; ++j)
			{
				obj.NewPoint()->SetPoint(Vector3((Coord)(m_Footprint[j].x), 
												 (Coord)(counter * m_FloorHeight), 
												 (Coord)(m_Footprint[j].y)));
			}
			counter++;
		}

		vector<Shapes::DVertex> polygonRoof1(m_Footprint.GetVertices());
		if (m_RoofType == 1)
		{
			if (Offset(polygonRoof1, 1.0, true))
			{
				for (unsigned int j = 0; j < cnt; ++j)
				{
					obj.NewPoint()->SetPoint(Vector3((Coord)(polygonRoof1[j].x), 
													 (Coord)((m_NumOfFloors + 1) * m_FloorHeight), 
													 (Coord)(polygonRoof1[j].y)));
				}
			}
			else
			{
				m_RoofType = 0;
			}
		}

		AutoArray<int, MemAllocStack<int, 128> > indices;

		ObjToolTopology& topology = obj.GetTool<ObjToolTopology>();

		// bottom (reversed order)
		for (int j = cnt - 1; j >= 0; --j)
		{
			indices.Add(j);
		}
		topology.TessellatePolygon(indices.Size(), indices.Data());

		// facades and inclined roofs
		unsigned int shift = 0;
		if (m_RoofType == 1) shift = 1;

		for (unsigned int i = 0; i <= m_NumOfFloors + shift; ++i)
		{
			for (unsigned int j = 0; j < cnt; ++j)
			{
				FaceT fc1;
				fc1.CreateIn(&obj);
				fc1.SetN(3);

				if (j != cnt - 1)
				{
					fc1.SetPoint(0, i * cnt + j);
					fc1.SetPoint(1, i * cnt + j + 1);
					fc1.SetPoint(2, (i + 1) * cnt + j);

					obj.FaceSelect(fc1.GetFaceIndex());

					FaceT fc2;
					fc2.CreateIn(&obj);
					fc2.SetN(3);

					fc2.SetPoint(0, i * cnt + j + 1);
					fc2.SetPoint(1, (i + 1) * cnt + j + 1);
					fc2.SetPoint(2, (i + 1) * cnt + j);

					obj.FaceSelect(fc2.GetFaceIndex());
				}
				else
				{
					fc1.SetPoint(0, i * cnt + j);
					fc1.SetPoint(1, i * cnt);
					fc1.SetPoint(2, (i + 1) * cnt + j);

					obj.FaceSelect(fc1.GetFaceIndex());

					FaceT fc2;
					fc2.CreateIn(&obj);
					fc2.SetN(3);

					fc2.SetPoint(0, i * cnt);
					fc2.SetPoint(1, (i + 1) * cnt);
					fc2.SetPoint(2, (i + 1) * cnt + j);

					obj.FaceSelect(fc2.GetFaceIndex());
				}
			}
		}

		// top
		switch (m_RoofType)
		{
		case 0:
			{
				indices.Clear();
				for (unsigned int j = 0; j < cnt; ++j)
				{
					indices.Add((m_NumOfFloors + 1) * cnt + j);
				}
				topology.TessellatePolygon(indices.Size(), indices.Data());
			}
			break;
		case 1:
			{
				indices.Clear();
				for (unsigned int j = 0; j < cnt; ++j)
				{
					indices.Add((m_NumOfFloors + 2) * cnt + j);
				}
				topology.TessellatePolygon(indices.Size(), indices.Data());
			}
			break;
		default:
			{
				indices.Clear();
				for (unsigned int j = 0; j < cnt; ++j)
				{
					indices.Add((m_NumOfFloors + 1) * cnt + j);
				}
				topology.TessellatePolygon(indices.Size(), indices.Data());
			}
		}

		int randTexture = (int)RandomInRangeF(1, PREDEF_PROC_TEXT_COUNT) - 1;

		// masses
		for (unsigned int i = 0, pCnt = obj.NPoints(); i < pCnt; ++i)
		{
			obj.SetPointMass(i, 1250.0f);
		}

		// sharp edges and textures
		unsigned int floorCounter = 0;
		unsigned int faceCounter = 0;
		for (unsigned int i = 0, fCnt = obj.NFaces(); i < fCnt; ++i)
		{
			FaceT fc(obj, i);
			unsigned int vCnt = fc.GetVxCount();
			for (unsigned j = 0, k = vCnt - 1; j < vCnt; k = j++)
			{
				int v1 = fc.GetPoint(k);
				int v2 = fc.GetPoint(j);
				obj.AddSharpEdge(v1, v2);
			}

			if (textureLOD)
			{
				if (genProceduralTexture)
				{
					if (i < cnt - 2)
					{
						// bottom 
						RString procText = BaseProcTexture.c_str();
						fc.SetTexture(procText);
					}
					else if (cnt - 2 <= i && i < cnt - 2 + 2 * (m_NumOfFloors + 1) * cnt)
					{
						// facades 
						faceCounter++;
						if (faceCounter > 2 * cnt)
						{
							faceCounter = 1;
							floorCounter++;
						}

						RString procText;
						if (floorCounter % 2 == 0)
						{
							procText = ProcTextures[randTexture * 2].c_str();
						}
						else
						{
							procText = ProcTextures[randTexture * 2 + 1].c_str();
						}
						fc.SetTexture(procText);
					}
					else
					{
						// top 
						RString procText = RoofProcTexture.c_str();
						fc.SetTexture(procText);
					}
				}
				else
				{
					if (i < cnt - 2)
					{
						// bottom 
						RString procText = BaseProcTexture.c_str();
						fc.SetTexture(procText);
					}
					else if (cnt - 2 <= i && i < cnt - 2 + 2 * (m_NumOfFloors + 1) * cnt)
					{
						RString path;
						path = m_TexturesPath + m_Textures[0].GetFilename() + ".tga";
						int posDrive = path.Find(":");
						path = path.Substring(posDrive + 1, path.GetLength());
						if (path[0] == '\\')
						{
							path = path.Substring(1, path.GetLength());
						}

						const VecT& vec0 = fc.GetPointVector<VecT>(0);
						const VecT& vec1 = fc.GetPointVector<VecT>(1);
						const VecT& vec2 = fc.GetPointVector<VecT>(2);
						int i0 = fc.GetPoint(0);
						int i1 = fc.GetPoint(1);
						int i2 = fc.GetPoint(2);

						float u0, v0;
						float u1, v1;
						float u2, v2;
						float ratio;

						if (vec0.Y() == vec1.Y())
						{
							ratio = 1.0f + floor(vec0.Distance(vec1) / fabs(vec0.Y() - vec2.Y()));

							int iDiff = i0 - i1;
							if (abs(iDiff) == 1)
							{
								if (iDiff > 0)
								{
									u0 = 1.0f;
									u1 = 0.0f;
								}
								else
								{
									u0 = 0.0f;
									u1 = 1.0f;
								}
							}
							else
							{
								if (iDiff > 0)
								{
									u0 = 0.0f;
									u1 = 1.0f;
								}
								else
								{
									u0 = 1.0f;
									u1 = 0.0f;
								}
							}

							if (vec2.Distance(vec0) < vec2.Distance(vec1))
							{
								u2 = u0;
							}
							else
							{
								u2 = u1;
							}

							if (vec0.Y() < vec2.Y())
							{
								v0 = 1.0f;
								v1 = 1.0f;
								v2 = 0.0f;
							}
							else
							{
								v0 = 0.0f;
								v1 = 0.0f;
								v2 = 1.0f;
							}
						}
						else if (vec0.Y() == vec2.Y())
						{
							ratio = 1.0f + floor(vec0.Distance(vec2) / fabs(vec0.Y() - vec1.Y()));

							int iDiff = i0 - i2;
							if (abs(iDiff) == 1)
							{
								if (iDiff > 0)
								{
									u0 = 1.0f;
									u2 = 0.0f;
								}
								else
								{
									u0 = 0.0f;
									u2 = 1.0f;
								}
							}
							else
							{
								if (iDiff > 0)
								{
									u0 = 0.0f;
									u2 = 1.0f;
								}
								else
								{
									u0 = 1.0f;
									u2 = 0.0f;
								}
							}

							if (vec1.Distance(vec0) < vec1.Distance(vec2))
							{
								u1 = u0;
							}
							else
							{
								u1 = u2;
							}

							if (vec0.Y() < vec1.Y())
							{
								v0 = 1.0f;
								v2 = 1.0f;
								v1 = 0.0f;
							}
							else
							{
								v0 = 0.0f;
								v2 = 0.0f;
								v1 = 1.0f;
							}
						}
						else
						{
							ratio = 1.0f + floor(vec1.Distance(vec2) / fabs(vec1.Y() - vec0.Y()));

							int iDiff = i1 - i2;
							if (abs(iDiff) == 1)
							{
								if (iDiff > 0)
								{
									u1 = 1.0f;
									u2 = 0.0f;
								}
								else
								{
									u1 = 0.0f;
									u2 = 1.0f;
								}
							}
							else
							{
								if (iDiff > 0)
								{
									u1 = 0.0f;
									u2 = 1.0f;
								}
								else
								{
									u1 = 1.0f;
									u2 = 0.0f;
								}
							}

							if (vec0.Distance(vec1) < vec0.Distance(vec2))
							{
								u0 = u1;
							}
							else
							{
								u0 = u2;
							}

							if (vec1.Y() < vec0.Y())
							{
								v1 = 1.0f;
								v2 = 1.0f;
								v0 = 0.0f;
							}
							else
							{
								v1 = 0.0f;
								v2 = 0.0f;
								v0 = 1.0f;
							}
						}

						if (u0 == 1.0f) u0 *= ratio;
						if (u1 == 1.0f) u1 *= ratio;
						if (u2 == 1.0f) u2 *= ratio;

						fc.SetUV(0, u0, v0);
						fc.SetUV(1, u1, v1);
						fc.SetUV(2, u2, v2);

						fc.SetTexture(path);
					}
					else
					{
						// top 
						RString procText = RoofProcTexture.c_str();
						fc.SetTexture(procText);
					}
				}
			}
		}

		GenerateMapping(obj);

		obj.RecalcNormals();
	}

	// -------------------------------------------------------------------------- //

	void BuildingModelP3D::GenerateGeometryLOD(ObjectData& obj)
	{
		GenerateLOD(obj);
		GenerateComponents(obj);
	}

	// -------------------------------------------------------------------------- //

	void BuildingModelP3D::GenerateViewGeometryLOD(ObjectData& obj)
	{
		GenerateLOD(obj);
		GenerateComponents(obj);
	}

	// -------------------------------------------------------------------------- //

	void BuildingModelP3D::GenerateFireGeometryLOD(ObjectData& obj)
	{
		GenerateLOD(obj);
		GenerateComponents(obj);
	}

	// -------------------------------------------------------------------------- //

	void BuildingModelP3D::GenerateRoadwayLOD(ObjectData& obj)
	{
		AutoArray<int, MemAllocStack<int, 128> > indices;

		ObjToolTopology& topology = obj.GetTool<ObjToolTopology>();

		// top
		switch (m_RoofType)
		{
		case 0:
			{
				for (unsigned int j = 0, cnt = m_Footprint.GetVerticesCount(); j < cnt; ++j)
				{
					obj.NewPoint()->SetPoint(Vector3((Coord)(m_Footprint[j].x), 
													 (Coord)(m_NumOfFloors * m_FloorHeight), 
													 (Coord)(m_Footprint[j].y)));
					indices.Add(j);

				}
				topology.TessellatePolygon(indices.Size(), indices.Data());
			}
			break;
		case 1:
			{
				for (unsigned int j = 0, cnt = m_Footprint.GetVerticesCount(); j < cnt; ++j)
				{
					obj.NewPoint()->SetPoint(Vector3((Coord)(m_Footprint[j].x), 
													 (Coord)(m_NumOfFloors * m_FloorHeight), 
													 (Coord)(m_Footprint[j].y)));
					indices.Add(j);

				}

				vector<Shapes::DVertex> polygon = m_Footprint.GetVertices();
				if (Offset(polygon, 1.0, true))
				{
					for (unsigned int j = 0, cnt = polygon.size(); j < cnt; ++j)
					{
						obj.NewPoint()->SetPoint(Vector3((Coord)(polygon[j].x), 
														 (Coord)((m_NumOfFloors + 1) * m_FloorHeight), 
														 (Coord)(polygon[j].y)));
					}
				}

				for (unsigned int j = 0, cnt = polygon.size(); j < cnt; ++j)
				{
					FaceT fc1;
					fc1.CreateIn(&obj);
					fc1.SetN(3);

					if (j != cnt - 1)
					{
						fc1.SetPoint(0, j);
						fc1.SetPoint(1, j + 1);
						fc1.SetPoint(2, cnt + j);

						obj.FaceSelect(fc1.GetFaceIndex());

						FaceT fc2;
						fc2.CreateIn(&obj);
						fc2.SetN(3);

						fc2.SetPoint(0, j + 1);
						fc2.SetPoint(1, cnt + j + 1);
						fc2.SetPoint(2, cnt + j);

						obj.FaceSelect(fc2.GetFaceIndex());
					}
					else
					{
						fc1.SetPoint(0, j);
						fc1.SetPoint(1, 0);
						fc1.SetPoint(2, cnt + j);

						obj.FaceSelect(fc1.GetFaceIndex());

						FaceT fc2;
						fc2.CreateIn(&obj);
						fc2.SetN(3);

						fc2.SetPoint(0, 0);
						fc2.SetPoint(1, cnt);
						fc2.SetPoint(2, cnt + j);

						obj.FaceSelect(fc2.GetFaceIndex());
					}
				}
			}
			break;
		default:
			{
				for (unsigned int j = 0, cnt = m_Footprint.GetVerticesCount(); j < cnt; ++j)
				{
					obj.NewPoint()->SetPoint(Vector3((Coord)(m_Footprint[j].x), 
													 (Coord)(m_NumOfFloors * m_FloorHeight), 
													 (Coord)(m_Footprint[j].y)));
					indices.Add(j);

				}
				topology.TessellatePolygon(indices.Size(), indices.Data());
			}
		}

		obj.RecalcNormals();
	}

	// -------------------------------------------------------------------------- //

	void BuildingModelP3D::GenerateShadowLOD(ObjectData& obj)
	{
		GenerateLOD(obj);
	}

	// -------------------------------------------------------------------------- //

	void BuildingModelP3D::GenerateMapping(ObjectData& obj)
	{
	}

	// -------------------------------------------------------------------------- //

	void BuildingModelP3D::SetOrigin()
	{
		m_Position = m_Footprint.GetBarycenter();

		for (unsigned int i = 0, cnt = m_Footprint.GetVerticesCount(); i < cnt; ++i)
		{
			m_Footprint[i].x -= m_Position.x;
			m_Footprint[i].y -= m_Position.y;
		}
		m_Footprint.ResetBoundingBox();
	}

	// -------------------------------------------------------------------------- //

	void BuildingModelP3D::OrientateAsMinBoundingBox()
	{
		m_Orientation = m_Footprint.GetMinBoundingBoxOrientation();

		if (m_Orientation != 0.0)
		{
			// we are rotating about the origin
			double x;
			double y;
			for (unsigned int i = 0, cnt = m_Footprint.GetVerticesCount(); i < cnt; ++i)
			{
				x = m_Footprint[i].x;
				y = m_Footprint[i].y;
				m_Footprint[i].x = x * cos(m_Orientation) + y * sin(m_Orientation);
				m_Footprint[i].y = - x * sin(m_Orientation) + y * cos(m_Orientation);
			}
		}
	}

	// -------------------------------------------------------------------------- //

	void BuildingModelP3D::CorrectOrigin()
	{
		Shapes::DVertex minP = m_Footprint.GetMinPoint();

		double halfBBWidth  = 0.5 * m_Footprint.GetMinBoundingBoxWidth();
		double halfBBHeight = 0.5 * m_Footprint.GetMinBoundingBoxHeight();

		double dx = halfBBWidth + minP.x;
		double dy = halfBBHeight + minP.y;

		for (unsigned int i = 0, cnt = m_Footprint.GetVerticesCount(); i < cnt; ++i)
		{
			m_Footprint[i].x -= dx;
			m_Footprint[i].y -= dy;
		}

		double rotDx = dx * cos(m_Orientation) - dy * sin(m_Orientation);
		double rotDy = dx * sin(m_Orientation) + dy * cos(m_Orientation);

		m_Position.x += rotDx;
		m_Position.y += rotDy;
	}

	// -------------------------------------------------------------------------- //

	void BuildingModelP3D::GenerateComponents(ObjectData& obj)
	{
/*
		// old code
		{
			Selection sel(&obj);
			for (int i = 0; i < obj.NPoints(); ++i)
			{
				sel.PointSelect(i);
			}

			for (int i = 0; i < obj.NFaces(); ++i)
			{
				sel.FaceSelect(i);
			}

			obj.SaveNamedSel("Component01", &sel);
		}
*/
		// new code
		{
			CG_Polygon2 poly;
			for (unsigned int i = 0, cnt = m_Footprint.GetVerticesCount(); i < cnt; ++i)
			{
				poly.AddVertex(m_Footprint[i].x, m_Footprint[i].y);
			}

			m_Partitions = poly.PartitionHertelMehlhorn(PolyTri_SimpleEarClipping);

			for (size_t i = 0, cnt1 = m_Partitions.size(); i < cnt1; ++i)
			{
				Selection sel(&obj);

				for (unsigned int j = 0, cnt2 = m_Partitions[i].VerticesCount(); j < cnt2; ++j)
				{
					for (int k = 0, cnt3 = m_Footprint.GetVerticesCount(); k < cnt3; ++k)
					{
						CG_Point2 point(m_Footprint[k].x, m_Footprint[k].y);

						if (m_Partitions[i].FindVertex(point))
						{
							size_t shift = 0;
							if (m_RoofType == 1) shift = 1;
							for (unsigned int m = 0; m <= m_NumOfFloors + 1 + shift; ++m)
							{
								sel.PointSelect(k + m * cnt3);
							}
						}
					}
				}

				char chCounter[4];
				sprintf_s(chCounter, "%d", i + 1);
				string strCounter = chCounter;
				
				while (strCounter.length() < 2)
				{
					strCounter = "0" + strCounter;
				}

				string component = "Component" + strCounter;

				obj.SaveNamedSel(component.c_str(), &sel);
			}
		}
	}

	// -------------------------------------------------------------------------- //
	// Building
	// -------------------------------------------------------------------------- //

	Building::Building()
	{
	}

	// -------------------------------------------------------------------------- //

	Building::Building(const Footprint& footprint, unsigned int numOfFloors, 
					   double floorHeight, unsigned int roofType)
	: m_Footprint(footprint)
	, m_NumOfFloors(numOfFloors)
	, m_FloorHeight(floorHeight)
	, m_RoofType(roofType)
	{
		m_FPTurningFunction = m_Footprint.GetTurningFunction();
		m_FPTurningSequence = m_Footprint.GetTurningSequence();
	}

	// -------------------------------------------------------------------------- //

	Footprint& Building::GetFootprint()
	{
		return m_Footprint;
	}

	// -------------------------------------------------------------------------- //

	const Footprint& Building::GetFootprint() const
	{
		return m_Footprint;
	}

	// -------------------------------------------------------------------------- //

	Shapes::DVertex Building::GetPosition() const
	{
		return m_Position;
	}

	// -------------------------------------------------------------------------- //

	double Building::GetOrientation() const
	{
		return m_Orientation;
	}

	// -------------------------------------------------------------------------- //

	const BuildingModelP3D& Building::GetModelP3D() const
	{
		return m_ModelP3D;
	}

	// -------------------------------------------------------------------------- //

	void Building::GenerateP3DModels(IMainCommands* cmdIfc, RString modelsPath, 
									 RString texturesPath, const vector<Texture>& textures)
	{
		Shapes::DVertex mPosition = m_Footprint.GetBarycenter();

		char buffer[80];
		sprintf(buffer, "building_%d_%d", (int)mPosition.x, (int)mPosition.y);
		RString modelName(buffer);

		m_ModelP3D.Create(modelName, m_Footprint, m_NumOfFloors, m_FloorHeight, m_RoofType, modelsPath, texturesPath, textures);
		m_ModelP3D.ExportToP3DFile();

		mPosition = m_ModelP3D.GetPosition();
		Vector3 position  = Vector3(static_cast<Coord>(mPosition.x), 0, static_cast<Coord>(mPosition.y));

		Coord   rotation = static_cast<Coord>(m_ModelP3D.GetOrientation());
		Matrix4 mTranslation = Matrix4(MTranslation, position);
		Matrix4 mRotation    = Matrix4(MRotationY, rotation);

		Matrix4 transform = mTranslation * mRotation;
		cmdIfc->GetObjectMap()->PlaceObject(transform, modelName, 0);			
	}

	// -------------------------------------------------------------------------- //
}