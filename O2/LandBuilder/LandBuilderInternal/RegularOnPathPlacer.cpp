#include "StdAfx.h"
#include ".\RegularOnPathPlacer.h"
#include "..\HighMapLoaders\include\EtHelperFunctions.h"
#include "..\HighMapLoaders\include\EtMath.h"

// -------------------------------------------------------------------------- //

namespace LandBuildInt
{
	namespace Modules
	{
		// --------------------------------------------------------------------//

		RegularOnPathPlacer::RegularOnPathPlacer()
		{
		}

		// --------------------------------------------------------------------//

		RegularOnPathPlacer::~RegularOnPathPlacer()
		{
		}

		// --------------------------------------------------------------------//

		void RegularOnPathPlacer::Run(IMainCommands* cmdIfc)
		{
			printf("Started a new shape                   \n");
			printf("--------------------------------------\n");

			// gets shape and its properties
			m_pCurrentShape = cmdIfc->GetActiveShape();

			// gets global parameters from calling cfg file
			if(GetGlobalParameters(cmdIfc))
			{
				// gets dbf parameters from calling cfg file
				if(GetDbfParameters(cmdIfc))
				{
					// gets objects parameters from calling cfg file
					if(GetObjectsParameters(cmdIfc))
					{
						// initializes random seed
						srand(m_GlobalIn.randomSeed);

						// creates the objects
						CreateObjects(false);

						// export objects
						ExportCreatedObjects(cmdIfc);
					}
				}
			}
		}

		// --------------------------------------------------------------------//

		ModuleObjectOutputArray* RegularOnPathPlacer::RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn,
																   AutoArray<RegularOnPathPlacer::ObjectInput, MemAllocStack<RegularOnPathPlacer::ObjectInput, 32> >& objectsIn)
		{
			m_pCurrentShape = shape;
			m_GlobalIn      = globalIn;
			m_DbfIn         = dbfIn;
			m_ObjectsIn     = objectsIn;

			if(m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();

			// initializes random seed
			srand(m_GlobalIn.randomSeed);

			// creates the objects
			CreateObjects(true);

			return &m_ObjectsOut;
		}

		// --------------------------------------------------------------------//

		bool RegularOnPathPlacer::GetGlobalParameters(IMainCommands* cmdIfc)
		{
			const char* x = cmdIfc->QueryValue("randomSeed", false);
			if (x != 0) 
			{
				if(IsInteger(string(x), false))
				{
					m_GlobalIn.randomSeed = static_cast<unsigned int>(atoi(x));
				}
				else
				{
					ExceptReg::RegExcpt(Exception(11, ">>> Bad data for randomSeed <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(12, ">>> Missing value for randomSeed <<<"));
				return false;
			}
			return true;
		}

		// --------------------------------------------------------------------//

		bool RegularOnPathPlacer::GetDbfParameters(IMainCommands* cmdIfc)
		{
			const char* x = cmdIfc->QueryValue("offset", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.offset = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(21, ">>> Bad data for offset <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(22, ">>> Missing value for offset <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("offsetMaxNoise", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.offsetMaxNoise = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(23, ">>> Bad data for offsetMaxNoise <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(24, ">>> Missing value for offsetMaxNoise <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("step", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.step = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(25, ">>> Bad data for step <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(26, ">>> Missing value for step <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("stepMaxNoise", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.stepMaxNoise = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(27, ">>> Bad data for stepMaxNoise <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(28, ">>> Missing value for stepMaxNoise <<<"));
				return false;
			}

			x = cmdIfc->QueryValue("firstStep", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.firstStep = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(29, ">>> Bad data for firstStep <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(30, ">>> Missing value for firstStep <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------//

		bool RegularOnPathPlacer::GetObjectsParameters(IMainCommands* cmdIfc)
		{
			// resets arrays
			if(m_ObjectsIn.Size() != 0)
			{
				m_ObjectsIn.Clear();
				m_ObjectsOut.Clear();
			}

			// gets objects' data from calling cfg file
			string object        = "object";
			string minheight     = "minheight";
			string maxheight     = "maxheight";
			string matchorient   = "matchOrient";
			string dircorrection = "dirCorrection";
			string placeright    = "placeRight";
			string placeleft     = "placeLeft";

			ObjectInput nfo;

			// object type
			const char* x = cmdIfc->QueryValue(object.c_str(), false);
			if (x == 0) return false;
			nfo.m_Name = x;

			// object min height
			nfo.m_MinHeight = 100.0; // default value
			x = cmdIfc->QueryValue(minheight.c_str(), false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					nfo.m_MinHeight = atof(x);
				}
				else
				{
					printf(">>> -------------------------------------------------- <<<\n");
					printf(">>> Bad data for minheight - using default value (100) <<<\n");
					printf(">>> -------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> ------------------------------------------------------- <<<\n");
				printf(">>> Missing value for minheight - using default value (100) <<<\n");
				printf(">>> ------------------------------------------------------- <<<\n");
			}

			// object max height
			nfo.m_MaxHeight = 100.0; // default value
			x = cmdIfc->QueryValue(maxheight.c_str(), false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					nfo.m_MaxHeight = atof(x);
				}
				else
				{
					printf(">>> -------------------------------------------------- <<<\n");
					printf(">>> Bad data for maxheight - using default value (100) <<<\n");
					printf(">>> -------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> ------------------------------------------------------- <<<\n");
				printf(">>> Missing value for maxheight - using default value (100) <<<\n");
				printf(">>> ------------------------------------------------------- <<<\n");
			}

			// object match orientation
			nfo.m_MatchOrient = true; // default value
			x = cmdIfc->QueryValue(matchorient.c_str(), false);
			if (x != 0) 
			{
				if(IsBool(string(x), false))
				{
					int i = static_cast<int>(atoi(x));
					if (i == 0)
					{
						nfo.m_MatchOrient = false;
					}
				}
				else
				{
					printf(">>> -------------------------------------------------- <<<\n");
					printf(">>> Bad data for matchOrient - using default value (1) <<<\n");
					printf(">>> -------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> ------------------------------------------------------- <<<\n");
				printf(">>> Missing value for matchOrient - using default value (1) <<<\n");
				printf(">>> ------------------------------------------------------- <<<\n");
			}

			// object direction correction
			nfo.m_DirCorrection = true; // default value
			x = cmdIfc->QueryValue(dircorrection.c_str(), false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					nfo.m_DirCorrection = atof(x);
				}
				else
				{
					printf(">>> ------------------------------------------------------ <<<\n");
					printf(">>> Bad data for dirCorrection - using default value (0.0) <<<\n");
					printf(">>> ------------------------------------------------------ <<<\n");
				}
			}
			else
			{
				printf(">>> ----------------------------------------------------------- <<<\n");
				printf(">>> Missing value for dirCorrection - using default value (0.0) <<<\n");
				printf(">>> ----------------------------------------------------------- <<<\n");
			}

			// object place right
			x = cmdIfc->QueryValue(placeright.c_str(), false);
			nfo.m_PlaceRight = true; // default value
			if (x != 0) 
			{
				if(IsBool(string(x), false))
				{
					int i = static_cast<int>(atoi(x));
					if (i == 0)
					{
						nfo.m_PlaceRight = false;
					}
				}
				else
				{
					printf(">>> ------------------------------------------------- <<<\n");
					printf(">>> Bad data for placeRight - using default value (1) <<<\n");
					printf(">>> ------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> ------------------------------------------------------ <<<\n");
				printf(">>> Missing value for placeRight - using default value (1) <<<\n");
				printf(">>> ------------------------------------------------------ <<<\n");
			}

			// object place left
			x = cmdIfc->QueryValue(placeleft.c_str(), false);
			nfo.m_PlaceLeft = true; // default value
			if (x != 0) 
			{
				if(IsBool(string(x), false))
				{
					int i = static_cast<int>(atoi(x));
					if (i == 0)
					{
						nfo.m_PlaceLeft = false;
					}
				}
				else
				{
					printf(">>> ------------------------------------------------ <<<\n");
					printf(">>> Bad data for placeLeft - using default value (1) <<<\n");
					printf(">>> ------------------------------------------------ <<<\n");
				}
			}
			else
			{
				printf(">>> ------------------------------------------------------ <<<\n");
				printf(">>> Missing value for placeRight - using default value (1) <<<\n");
				printf(">>> ------------------------------------------------------ <<<\n");
			}

			m_ObjectsIn.Add(nfo);

			return true;
		}

		// --------------------------------------------------------------------//

		void RegularOnPathPlacer::CreateObjects(bool preview)
		{
			double perimeter = GetShapePerimeter(m_pCurrentShape);
			for (double s = m_DbfIn.firstStep; s <= perimeter; s += m_DbfIn.step)
			{
				// random noise along step
				double sNoiseR = RandomInRangeF(-m_DbfIn.stepMaxNoise, m_DbfIn.stepMaxNoise);
				double sNoiseL = RandomInRangeF(-m_DbfIn.stepMaxNoise, m_DbfIn.stepMaxNoise);

				double correctedSR = s + sNoiseR;
				double correctedSL = s + sNoiseL;

				if (correctedSR < 0.0)       correctedSR = 0.0;
				if (correctedSR > perimeter) correctedSR = perimeter;
				if (correctedSL < 0.0)       correctedSL = 0.0;
				if (correctedSL > perimeter) correctedSL = perimeter;

				// random noise along offset
				double oNoiseR = RandomInRangeF(-m_DbfIn.offsetMaxNoise, m_DbfIn.offsetMaxNoise);
				double oNoiseL = RandomInRangeF(-m_DbfIn.offsetMaxNoise, m_DbfIn.offsetMaxNoise);

				Shapes::DVertex curV1;
				Shapes::DVertex curV2;

				if (m_ObjectsIn[0].m_PlaceRight)
				{
					Shapes::DVertex curPosR = ConvertToXY(correctedSR, curV1, curV2);
					Shapes::DVector curV12(curV2, curV1);
					curV12.NormalizeXYInPlace();
					Shapes::DVector curR = curV12.PerpXYCW();
					curR *= (m_DbfIn.offset + oNoiseR);
					Shapes::DVertex posR = curR.GetSecondEndXY(curPosR);

					// new object
					ModuleObjectOutput out;

					// sets objects out data
					// type
					out.type     = 0;
					// position
					out.position = Shapes::DVertex(posR.x, posR.y);
					// rotation
					if (m_ObjectsIn[0].m_MatchOrient)
					{
						out.rotation = curV12.DirectionXY() + m_ObjectsIn[0].m_DirCorrection * EtMathD::PI / 180.0;
					}
					else
					{
						out.rotation = RandomInRangeF(0, EtMathD::TWO_PI);
					}

					// scale
					double minScale = m_ObjectsIn[0].m_MinHeight;
					double maxScale = m_ObjectsIn[0].m_MaxHeight;
					double scale = RandomInRangeF(minScale, maxScale);
					out.scaleX = out.scaleY = out.scaleZ = scale / 100.0;

					// used only by VLB for preview
					out.length = 10.0;
					out.width  = 10.0;
					out.height = 10.0;

					// adds to array
					m_ObjectsOut.Add(out);
				}

				if (m_ObjectsIn[0].m_PlaceLeft)
				{
					Shapes::DVertex curPosL = ConvertToXY(correctedSL, curV1, curV2);
					Shapes::DVector curV12(curV2, curV1);
					curV12.NormalizeXYInPlace();
					Shapes::DVector curL = curV12.PerpXYCCW();
					curL *= (m_DbfIn.offset + oNoiseL);
					Shapes::DVertex posL = curL.GetSecondEndXY(curPosL);

					// new object
					ModuleObjectOutput out;

					// sets objects out data
					// type
					out.type     = 0;
					// position
					out.position = Shapes::DVertex(posL.x, posL.y);
					// rotation
					if (m_ObjectsIn[0].m_MatchOrient)
					{
						out.rotation = curV12.DirectionXY() + m_ObjectsIn[0].m_DirCorrection * EtMathD::PI / 180.0;
					}
					else
					{
						out.rotation = RandomInRangeF(0, EtMathD::TWO_PI);
					}
					// scale
					double minScale = m_ObjectsIn[0].m_MinHeight;
					double maxScale = m_ObjectsIn[0].m_MaxHeight;
					double scale = RandomInRangeF(minScale, maxScale);
					out.scaleX = out.scaleY = out.scaleZ = scale / 100.0;

					// used only by VLB for preview
					out.length = 10.0;
					out.width  = 10.0;
					out.height = 10.0;

					// adds to array
					m_ObjectsOut.Add(out);
				}
			}
		}

		// --------------------------------------------------------------------//

		void RegularOnPathPlacer::ExportCreatedObjects(IMainCommands* cmdIfc)
		{
			int objOutCount = m_ObjectsOut.Size();
			for(int i = 0; i < objOutCount; ++i)
			{
				RString name      = m_ObjectsIn[m_ObjectsOut[i].type].m_Name;
				Vector3 position  = Vector3(static_cast<Coord>(m_ObjectsOut[i].position.x), 0, static_cast<Coord>(m_ObjectsOut[i].position.y));

				Matrix4 mTranslation = Matrix4(MTranslation, position);
				Matrix4 mRotation    = Matrix4(MRotationY, static_cast<Coord>(m_ObjectsOut[i].rotation));
				Matrix4 mScaling     = Matrix4(MScale, static_cast<Coord>(m_ObjectsOut[i].scaleX), static_cast<Coord>(m_ObjectsOut[i].scaleZ), static_cast<Coord>(m_ObjectsOut[i].scaleY));

				Matrix4 transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject(transform, name, 0);
			}
		}

		// --------------------------------------------------------------------//

		Shapes::DVertex RegularOnPathPlacer::ConvertToXY(double s, Shapes::DVertex& v1, Shapes::DVertex& v2)
		{
			double dist;

			int vCount = m_pCurrentShape->GetVertexCount();

			int index = -1;
			double tempPos = 0.0;

			for (int i = 0; i < (vCount - 1); i++)
			{
				v1 = m_pCurrentShape->GetVertex(i);
				v2 = m_pCurrentShape->GetVertex(i + 1);
				dist = v1.DistanceXY(v2);
				tempPos += dist;
				if (s < tempPos)
				{
					index = i;
					break;
				}
			}

			if (index == -1)
			{
				v1 = m_pCurrentShape->GetVertex(vCount - 1);
				v2 = m_pCurrentShape->GetVertex(0);
				dist = v1.DistanceXY(v2);
				tempPos += dist;
			}
			else
			{
				v1 = m_pCurrentShape->GetVertex(index);
				v2 = m_pCurrentShape->GetVertex(index + 1);
			}

			dist = v1.DistanceXY(v2);			
			Shapes::DVector diff = v1 - v2;

			double localS = tempPos - s;
			double delta = 0.0;

			if (dist != 0.0)
			{
				delta = localS / dist;
			}

			double newX = v2.x + diff.x * delta;
			double newY = v2.y + diff.y * delta;

			return Shapes::DVertex(newX, newY); 
		}

		// --------------------------------------------------------------------//
	}
}

