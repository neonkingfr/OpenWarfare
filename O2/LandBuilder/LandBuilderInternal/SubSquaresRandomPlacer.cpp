#include "StdAfx.h"
#include ".\SubSquaresRandomPlacer.h"

#include "..\HighMapLoaders\include\EtMath.h"
#include "..\HighMapLoaders\include\EtHelperFunctions.h"

namespace LandBuildInt
{
	namespace Modules
	{
		// --------------------------------------------------------------------------------

		void SubSquaresRandomPlacer::Run(IMainCommands *cmdIfc)
		{
			printf("Started a new shape                   \n");
			printf("--------------------------------------\n");

			// gets shape and its properties
			m_pCurrentShape = cmdIfc->GetActiveShape();      

			// gets parameters from calling cfg file
			if(GetGlobalParameters(cmdIfc))
			{
				// gets dbf parameters from calling cfg file
				if(GetDbfParameters(cmdIfc))
				{
					// gets objects parameters from calling cfg file
					if(GetObjectsParameters(cmdIfc))
					{
						// normalizes probabilities
						if(NormalizeProbs())
						{
							// initializes random seed
							srand(m_GlobalIn.randomSeed);

							// sets the population
							CreateObjects(false);

							// export objects
							ExportCreatedObjects(cmdIfc);

							// writes the report
							WriteReport();
						}													
					}
				}
			}       
		}

		// --------------------------------------------------------------------//

		ModuleObjectOutputArray* SubSquaresRandomPlacer::RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn, 
																      AutoArray<SubSquaresRandomPlacer::ObjectInput, MemAllocStack<SubSquaresRandomPlacer::ObjectInput, 32> >& objectsIn)
		{
			m_pCurrentShape = shape;
			m_GlobalIn      = globalIn;
			m_DbfIn         = dbfIn;
			m_ObjectsIn     = objectsIn;

			if(m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();

			// normalizes probabilities
			if(NormalizeProbs())
			{
				// initializes random seed
				srand(m_GlobalIn.randomSeed);

				// sets the population
				CreateObjects(true);

				return &m_ObjectsOut;
			}
			else
			{
				return NULL;
			}
		}

		// --------------------------------------------------------------------------------

		bool SubSquaresRandomPlacer::GetGlobalParameters(IMainCommands* cmdIfc)
		{
			const char* x = cmdIfc->QueryValue("randomSeed", false);
			if (x != 0) 
			{
				if(IsInteger(string(x), false))
				{
					m_GlobalIn.randomSeed = static_cast<unsigned int>(atoi(x));
				}
				else
				{
					ExceptReg::RegExcpt(Exception(11, ">>> Bad data for randomSeed <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(12, ">>> Missing randomSeed value <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool SubSquaresRandomPlacer::GetDbfParameters(IMainCommands* cmdIfc)
		{
			m_DbfIn.subSquaresSize = 10.0; // default value
			const char* x = cmdIfc->QueryValue("subSquaresSize", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.subSquaresSize = atof(x);
				}
				else
				{
					printf(">>> -------------------------------------------------------- <<<\n");
					printf(">>> Bad data for subSquaresSize - using default value (10.0) <<<\n");
					printf(">>> -------------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> --------------------------------------------------------- <<<\n");
				printf(">>> Missing subSquaresSize value - using default value (10.0) <<<\n");
				printf(">>> --------------------------------------------------------- <<<\n");
			}
			if (m_DbfIn.subSquaresSize < 1.0)
			{
				ExceptReg::RegExcpt(Exception(21, ">>> subSquaresSize is too low (less then 1.0) <<<"));
				return false;
			}

			m_DbfIn.maxNoise = 5.0; // default value
			x = cmdIfc->QueryValue("maxNoise", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.maxNoise = atof(x);
				}
				else
				{
					printf(">>> ------------------------------------------------- <<<\n");
					printf(">>> Bad data for maxNoise - using default value (5.0) <<<\n");
					printf(">>> ------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> -------------------------------------------------- <<<\n");
				printf(">>> Missing maxNoise value - using default value (5.0) <<<\n");
				printf(">>> -------------------------------------------------- <<<\n");
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool SubSquaresRandomPlacer::GetObjectsParameters(IMainCommands* cmdIfc)
		{
			// resets arrays
			if(m_ObjectsIn.Size() != 0)
			{
				m_ObjectsIn.Clear();
				m_ObjectsOut.Clear();
			}

			int counter = 1;
			do 
			{
				char object[50], minheight[50], maxheight[50], prob[50];
				sprintf_s(object,    "object%d",    counter);
				sprintf_s(minheight, "minheight%d", counter);
				sprintf_s(maxheight, "maxheight%d", counter);
				sprintf_s(prob,      "prob%d",      counter);

				ObjectInput nfo;

				// object type
				const char* x = cmdIfc->QueryValue(object, false);
				if (x == 0) break;
				nfo.m_Name = x;
        
				// object min height
				nfo.m_MinHeight = 100.0; // default value
				x = cmdIfc->QueryValue(minheight, false);
				if (x != 0)
				{
					if(IsFloatingPoint(string(x), false))
					{
						nfo.m_MinHeight = atof(x);
					}
					else
					{
						printf(">>> ---------------------------------------------------- <<<\n");
						printf(">>> Bad data for minheight - using default value (100.0) <<<\n");
						printf(">>> ---------------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> ----------------------------------------------------- <<<\n");
					printf(">>> Missing minheight value - using default value (100.0) <<<\n");
					printf(">>> ----------------------------------------------------- <<<\n");
				}

				// object max height
				nfo.m_MaxHeight = 100.0; // default value
				x = cmdIfc->QueryValue(maxheight, false);
				if (x != 0)
				{
					if(IsFloatingPoint(string(x), false))
					{
						nfo.m_MaxHeight = atof(x);
					}
					else
					{
						printf(">>> ---------------------------------------------------- <<<\n");
						printf(">>> Bad data for maxheight - using default value (100.0) <<<\n");
						printf(">>> ---------------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> ----------------------------------------------------- <<<\n");
					printf(">>> Missing maxheight value - using default value (100.0) <<<\n");
					printf(">>> ----------------------------------------------------- <<<\n");
				}
        
				// object prob
				nfo.m_Prob = 100.0; // default value
				x = cmdIfc->QueryValue(prob, false);
				if (x != 0)
				{
					if(IsFloatingPoint(string(x), false))
					{
						nfo.m_Prob = atof(x);
					}
					else
					{
						printf(">>> ----------------------------------------------- <<<\n");
						printf(">>> Bad data for prob - using default value (100.0) <<<\n");
						printf(">>> ----------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> ------------------------------------------------ <<<\n");
					printf(">>> Missing prob value - using default value (100.0) <<<\n");
					printf(">>> ------------------------------------------------ <<<\n");
				}

				nfo.m_Counter = 0;

				m_ObjectsIn.Add(nfo);
				counter++;
			} 
			while(true); 
			return true;
		}

		// --------------------------------------------------------------------//

		bool SubSquaresRandomPlacer::NormalizeProbs()
		{
			double total = 0.0;

			int objInCount = m_ObjectsIn.Size();
			for(int i = 0; i < objInCount; ++i)
			{
				total += m_ObjectsIn[i].m_Prob;
			}

			if(total == 0.0) 
			{
				ExceptReg::RegExcpt(Exception(41, ">>> Total probability equal to zero <<<"));
				return false;
			}

			double invTotal = 1 / total;
			for(int i = 0; i < objInCount; ++i)
			{
				m_ObjectsIn[i].m_NormProb = m_ObjectsIn[i].m_Prob * invTotal;
			}

			return true;
		}

		// --------------------------------------------------------------------//

		void SubSquaresRandomPlacer::CreateObjects(bool preview)
		{
			Shapes::DBox bounding = m_pCurrentShape->CalcBoundingBox();
			ProgressBar<int> pb(toLargeInt((float)((bounding.hi.y - bounding.lo.y) / m_DbfIn.subSquaresSize)));

			for (double y = bounding.lo.y + m_DbfIn.subSquaresSize * 0.5; y < bounding.hi.y; y += m_DbfIn.subSquaresSize) 
			{
				if(!preview) pb.AdvanceNext(1);

				Shapes::IShape* tmp = m_pCurrentShape->Clip(0, 1, -(y - m_DbfIn.subSquaresSize * 0.5));
				if (tmp == 0) continue;
				Shapes::IShape* clp = tmp->Clip(0, -1, (y + m_DbfIn.subSquaresSize * 0.5));
				delete tmp;
				if (clp == 0) continue;

				Shapes::DBox leftright = clp->CalcBoundingBox();

				double nextTest = bounding.lo.x;
				for (double x = (floor(leftright.lo.x / m_DbfIn.subSquaresSize) * m_DbfIn.subSquaresSize) + m_DbfIn.subSquaresSize * 0.5; x < leftright.hi.x; x += m_DbfIn.subSquaresSize)
				{
					double randDist = RandomInRangeF(0, m_DbfIn.maxNoise);
					double angle    = RandomInRangeF(0, EtMathD::TWO_PI);

					double x0 = x + randDist * EtMathD::Cos(angle);
					double y0 = y + randDist * EtMathD::Sin(angle);
					Shapes::DVector v0 = Shapes::DVector(x0, y0);

					if (clp->PtInside(v0))
					{
						// random type
						int type = GetRandomType();

						ModuleObjectOutput out;

						// sets data
						out.position = v0;
						out.type     = type;
						out.rotation = RandomInRangeF(0, EtMathD::TWO_PI);

						double minScale = m_ObjectsIn[type].m_MinHeight;
						double maxScale = m_ObjectsIn[type].m_MaxHeight;

						// used only by VLB for preview
						out.length = 10.0;
						out.width  = 10.0;
						out.height = 10.0;

						out.scaleX = out.scaleY = out.scaleZ = RandomInRangeF(minScale, maxScale) / 100.0;

						// adds to array
						m_ObjectsOut.Add(out);

						// update type counter
						m_ObjectsIn[type].m_Counter++;
					}
				}
				delete clp;
			}
		}

		// --------------------------------------------------------------------//

		int SubSquaresRandomPlacer::GetRandomType()
		{
			double p = (double)rand() / ((double)RAND_MAX + 1);

			bool found = false;
			int index  = 0;
			double curProb = m_ObjectsIn[index].m_NormProb;
			while(!found)
			{
				if (p < curProb)
				{
					found = true;
				}
				else
				{
					index++;
					curProb += m_ObjectsIn[index].m_NormProb;
				}
			}
			return index;
		}

		// --------------------------------------------------------------------//

		void SubSquaresRandomPlacer::ExportCreatedObjects(IMainCommands* cmdIfc)
		{
			int objOutCount = m_ObjectsOut.Size();
			for(int i = 0; i < objOutCount; ++i)
			{
				RString name      = m_ObjectsIn[m_ObjectsOut[i].type].m_Name;
				Vector3 position  = Vector3(static_cast<Coord>(m_ObjectsOut[i].position.x), 0, static_cast<Coord>(m_ObjectsOut[i].position.y));

				Matrix4 mTranslation = Matrix4(MTranslation, position);
				Matrix4 mRotation    = Matrix4(MRotationY, static_cast<Coord>(m_ObjectsOut[i].rotation));
				Matrix4 mScaling     = Matrix4(MScale, static_cast<Coord>(m_ObjectsOut[i].scaleX), static_cast<Coord>(m_ObjectsOut[i].scaleZ), static_cast<Coord>(m_ObjectsOut[i].scaleY));

				Matrix4 transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject(transform, name, 0);
			}
		}

		// --------------------------------------------------------------------//

		void SubSquaresRandomPlacer::WriteReport()
		{
			printf("Created                  \n");
			int objInCount = m_ObjectsIn.Size();
			for(int i = 0; i < objInCount; ++i)
			{
				printf("%d", m_ObjectsIn[i].m_Counter);
				printf(" : ");
				printf(m_ObjectsIn[i].m_Name);
				printf("                           \n");
			}
			printf("--------------------------------------\n");
		}
	}
}