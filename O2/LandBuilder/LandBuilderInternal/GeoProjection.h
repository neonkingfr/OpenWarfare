#pragma once

#include ".\IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>

#include "..\Projections\include\ProjTransformation.h"

namespace LandBuildInt
{
	namespace Modules
	{
		class GeoProjection : public IBuildModule
		{
		public:
			struct GlobalInput
			{
				PredefinedProjTransformationCodes type;
			};

		public:
			virtual void Run(IMainCommands*);

			class Exception: public ExceptReg::IException
			{
				unsigned int code;
				const char* text;

			public:
				Exception(unsigned int code,const char* text) : code(code), text(text) {}
				virtual unsigned int GetCode() const { return code; }
				virtual const _TCHAR* GetDesc() const { return text; }
				virtual const _TCHAR* GetModule() const { return "GeoProjection"; }
				virtual const _TCHAR* GetType() const { return "GeoProjectionException"; }
			};

		private:
			const Shapes::IShape* m_pCurrentShape;   
			GlobalInput           m_GlobalIn;

			bool GetGlobalParameters(IMainCommands* cmdIfc);
			bool Solve(IMainCommands* cmdIfc);
		};
	}
}
