#include "StdAfx.h"
#include "Core.h"
#include <iomanip>

namespace LandBuildInt
{
	Core::Core(IModuleFactory* factory)
	: _curShape(_shapes.FirstShape())
	, _modFactory(factory)
	, _ofpMapSegmentSizeX(0)
	, _ofpMapSegmentSizeY(0)
	{
	}

	Core::~Core(void)
	{
	}

	const char* Core::QueryValue(const char* name, bool forceConstant) const
	{
		if (forceConstant)
		{
			return _shapes.GetShapeConstParameter(_curShape.GetShapeId(), name);
		}
		else
		{
			const char* res = _shapes.GetShapeParameter(_curShape.GetShapeId(), name);
			if (res == 0) return QueryValue(name, true);
			return res;
		}    
	}

	const Shapes::IShape* Core::GetActiveShape() const
	{
		return _curShape.GetShape();
	}

	const char* Core::ConvertName(const char* alias) const
	{
		RscItem* it = _rscList.Find(RscItem(0, alias, RString()));
		if (it) 
		{
			return it->_replace;
		}
		else
		{
			return alias;
		}
	}

	const Shapes::IShape* Core::GetObjectShape(const char* objectName) const
	{
		return 0;
	}
  
	Shapes::IShape* Core::CreateObjectShapeTransformed(const char* objectName, const Matrix4& trns) const
	{
		return 0;
	}

	void Core::DestroyShape(Shapes::IShape* shape)
	{
		delete shape;
	}

	unsigned int Core::PlaceObject(const Matrix4& location, const char* objectName, unsigned int tag)
	{
		return _objects.Add(LandBuildInt::ObjectDesc(objectName, location, 0));
	}

	const Matrix4& Core::GetObjectLocation(unsigned int objId)
	{
		return _objects[objId].GetPositionMatrix();
	}
  
	const char* Core::GetObjectType(unsigned int objId)
	{
		return _objects[objId].GetName();
	}

	unsigned int Core::GetObjectTag(unsigned int objId)
	{
		return _objects[objId].GetTag();
	}

	unsigned int Core::FindNearestObject(float x, float y, unsigned int tag, const Array<unsigned int>& ignoreIDs)
	{
		const ObjectDesc* obj = _objects.Nearest(x, y, tag, ignoreIDs);
		return _objects.GetObjectID(obj);
	}

	bool Core::EnumObjects(int& objId)
	{
		do
		{
			objId++;
			if (objId < 0 || objId >= _objects.Size()) 
			{
				objId = -1;
				return false;
			}
		}
		while (!_objects.IsValidObjectID(objId));
		return true;
	}

	int Core::ObjectsInArea(const FloatRect& area, unsigned int tag, const IEnumObjectCallback& callback)
	{
		SRef<AutoArray<const ObjectDesc*> > objs = _objects.ObjectsInBox(Shapes::DBox(Shapes::DVertex(area.left, area.top), Shapes::DVertex(area.right, area.bottom)));
		for (int i = 0; i < objs->Size(); i++) 
		{
			int res = callback(_objects.GetObjectID(objs->operator[](i)));
			if (res != 0) return res;
		}
		return 0;
	}

	int Core::ObjectsInArea(const Shapes::IShape& shape, unsigned int tag, const IEnumObjectCallback& callback)
	{
		SRef<AutoArray<const ObjectDesc*> > objs = _objects.ObjectsInBox(shape.CalcBoundingBox());
		for (int i = 0; i < objs->Size(); i++) 
		{
			const Vector3& vx = objs->operator [](i)->GetPositionVector();
			if (shape.PtInside(Shapes::DVector(vx.X(), vx.Y())))
			{
				int res = callback(_objects.GetObjectID(objs->operator[](i)));
				if (res != 0) return res;
			}
		}
		return 0;
	}

	bool Core::DeleteObject(unsigned int objId)
	{
		return _objects.Delete(objId);
	}

	void Core::AddResource(int type, RString origin, RString replacement)
	{
		RscItem* it = _rscList.Find(RscItem(type, origin,RString()));
		if (it)
		{
			if (replacement.IsEmpty())
			{
				_rscList.Remove(*it);
			}
			else
			{
				it->_replace = replacement;
			}
		}
		else
		{
			_rscList.Add(RscItem(type, origin, replacement));
		}
	}

	IBuildModule* Core::GetModule(const char* name)
	{
		ModuleItem lookup(name, 0);
		ModuleItem* fnd = _modList.Find(lookup);
		if (fnd) 
		{
			return fnd->_module;
		}
		else
		{
			IBuildModule* m = _modFactory->GetModule(name);
			lookup._module = m;
			_modList.Add(lookup);
			return m;
		}
	}

	void Core::Run()
	{
/*    
		char rotchrs[] = "|/-\\";
		int cnt = 0;
*/
		int numpoints = 0;
		for (_curShape = _shapes.FirstShape(); _curShape; ++_curShape) 
		{
			numpoints += _curShape.GetShape()->GetVertexCount();
		}

		ProgressBar<int> pb(numpoints);

		// the following lines are part of the hack to let LandBuilder
		// behave like LandBuilder2 - calling the OnEndPass method
		// of each module after the last shape in the module has been
		// processed

		// start
		_curShape = _shapes.FirstShape();
		const char* name = _shapes.GetShapeConstParameter(_curShape.GetShapeId(), _shapes.algorithmFieldName);
		IBuildModule* prevMod = GetModule(name);
		// end

		for (_curShape = _shapes.FirstShape(); _curShape; ++_curShape)
		{
			const char* name = _shapes.GetShapeConstParameter(_curShape.GetShapeId(), _shapes.algorithmFieldName);
			IBuildModule* mod = GetModule(name);
			pb.AdvanceNext(_curShape.GetShape()->GetVertexCount());

			if (mod == 0)
			{
				printf("Unknown module name: %s - skipping\r\n", name);
			}
			else
			{
				mod->Run(this);

				// the following lines are part of the hack to let LandBuilder
				// behave like LandBuilder2 - calling the OnEndPass method
				// of each module after the last shape in the module has been
				// processed

				// start
				if (prevMod != mod)
				{
					prevMod->OnEndPass(this);
				}
				// end
			}

			// the following lines are part of the hack to let LandBuilder
			// behave like LandBuilder2 - calling the OnEndPass method
			// of each module after the last shape in the module has been
			// processed

			// start
			prevMod = mod;
			// end
		}

		// the following lines are part of the hack to let LandBuilder
		// behave like LandBuilder2 - calling the OnEndPass method
		// of each module after the last shape in the module has been
		// processed

		// start
		prevMod->OnEndPass(this);
		// end
	}

	bool Core::ExportHeightMap(std::ostream &out)
	{
		printf("Exporting height map\n");
		int minx = toInt(floor(_mapArea.left / _ofpMapSegmentSizeX));
		int miny = toInt(floor(_mapArea.top / _ofpMapSegmentSizeY));
		int maxx = toInt(ceil(_mapArea.right / _ofpMapSegmentSizeX));
		int maxy = toInt(ceil(_mapArea.bottom / _ofpMapSegmentSizeY));
		
		out << "heightmap\n";
		out << minx << " " << miny << " " << maxx << " " << maxy << " " << _ofpMapSegmentSizeX << " " << _ofpMapSegmentSizeY << "\n";
		
		ProgressBar<int> pb(maxx - minx);
		for (int y = miny; y <= maxy; ++y)
		{
			pb.AdvanceNext(1);
			for (int x = minx; x <= maxx; ++x)
			{
				float fx = x * _ofpMapSegmentSizeX;
				float fy = y * _ofpMapSegmentSizeY;
				float h = _highMap.GetHeight(fx, fy);
				out << h << " ";
			}
			out << "\n";
		}
		out << "end heightmap\n";
		printf("Finished ... (height map)\n");
		return !(!out);
	}

	bool Core::SaveResult(const char* target)
	{
		using namespace std;
		ofstream out(target, ios::out|ios::trunc);
		if (!out) return false;
		out << setiosflags( ios::scientific );
		if (_ofpMapSegmentSizeX > 0.0001 && _ofpMapSegmentSizeX > 0.0001)
		{
			ProgressBar<int> pb(2);
			pb.AdvanceNext(1);
			if (!ExportHeightMap(out)) return false;
			pb.AdvanceNext(1);
			if (!SaveResult(out)) return false;
			return true;
		}
		else
		{
			return SaveResult(out);
		}
	}

	bool Core::SaveResult(std::ostream& out)
	{
		ProgressBar<int> pb(_objects.Size());
		using namespace std;
		out << "objects\n";
		for(int i = 0; i < _objects.Size(); i++)
		{
			if (_objects.IsValidObjectID(i))
			{
				pb.AdvanceNext(1);
				const ObjectDesc& obj = _objects.operator [](i);
				const Matrix4& pos = obj.GetPositionMatrix();
				const char* name = obj.GetName();
				out << pos(0,0) << "\t" << pos(1,0) << "\t" << pos(2,0) << "\t";
				out << pos(0,1) << "\t" << pos(1,1) << "\t" << pos(2,1) << "\t";
				out << pos(0,2) << "\t" << pos(1,2) << "\t" << pos(2,2) << "\t";
				out << pos(0,3) << "\t" << pos(1,3) << "\t" << pos(2,3) << "\t";
				out << obj.GetTag() << "\t";
				out << name << "\n";
			}
		}
		out << "end objects\n";
		return !(!out);
	}

	const char* Core::TranslateColumnName(const char* name) const
	{    
		RscItem* it=_rscList.Find(RscItem(1, name, RString()));
		if (it) 
		{
			return it->_replace;
		}
		else
		{
			return name;  
		}
	}

	void Core::FlushColumnTranslateTable()
	{
		do
		{
			BTreeIterator<RscItem,BTreeCompare> iter(_rscList);
			iter.BeginFrom(RscItem(1, "", ""));
			RscItem* res = iter.Next();
			if (res == 0 || res->_type != 1) return;
			_rscList.Remove(*res);
		}
		while (true);
	}

	bool Core::ReplaceObject(unsigned int objId, const Matrix4& location, const char* objectName, unsigned int tag)
	{
		return _objects.Replace(objId, ObjectDesc(objectName, location, tag));
	}

	void* Core::GetInterfacePtr(unsigned int ifcuid) 
	{
		switch (ifcuid)
		{
			case IObjectMap::IFCUID: return static_cast<IObjectMap*>(this); break;
			case IHeightMap::IFCUID: return static_cast<IHeightMap*>(&_highMap); break;
			default: return IMainCommands::GetInterfacePtr(ifcuid);
		}
	}
}