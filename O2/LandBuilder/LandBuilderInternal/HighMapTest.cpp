#include "StdAfx.h"
#include ".\highmaptest.h"


#define PI 3.1415926535f
namespace LandBuildInt
{
	namespace Modules
	{
		void HeightMapTest::Run(IMainCommands *cmdIfc)
		{
			float ampx, perix, ampy, periy;

			const char *tmp = cmdIfc->QueryValue("amplitudex");
			if (tmp == 0 || sscanf(tmp, "%f", &ampx) != 1)
			{
				ExceptReg::RegExcpt(Exception(1, "missing 'amplitudex'"));
				return;
			}

			tmp = cmdIfc->QueryValue("amplitudey");
			if (tmp == 0 || sscanf(tmp, "%f", &ampy) != 1)
			{
				ExceptReg::RegExcpt(Exception(1, "missing 'amplitudey'"));
				return;
			}
			tmp = cmdIfc->QueryValue("periodx");
			if (tmp == 0 || sscanf(tmp, "%f", &perix) != 1)
			{
				ExceptReg::RegExcpt(Exception(1, "missing 'periodx'"));
				return;
			}
			tmp = cmdIfc->QueryValue("periody");
			if (tmp == 0 || sscanf(tmp, "%f", &periy) != 1)
			{
				ExceptReg::RegExcpt(Exception(1, "missing 'periody'"));
				return;
			}

			class Sampler: public IHeightMapSampler
			{
				float ampx, perix, ampy, periy;
			public:
				Sampler(float ampx, float perix, float ampy, float periy):
					ampx(ampx), ampy(ampy), perix(perix), periy(periy) 
				{
				}

				virtual float GetHeight(const HeightMapLocInfo &heightInfo) const
				{
					return sin(heightInfo.x / perix * 2 * PI) * ampx + sin(heightInfo.y / periy * 2 * PI) * ampy + (ampx + ampy);
				}
			};

			cmdIfc->GetHeightMap()->AddSamplerModule(new Sampler(ampx, perix, ampy, periy));
		}
	}
}