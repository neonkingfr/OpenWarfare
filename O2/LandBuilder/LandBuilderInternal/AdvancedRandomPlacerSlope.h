#pragma once

#include <string>

#include ".\IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>

#include "..\Shapes\ShapeHelpers.h"
#include "..\HighMapLoaders\include\EtRectElevationGrid.h"

using std::string;

namespace LandBuildInt
{
	namespace Modules
	{
		class AdvancedRandomPlacerSlope : public IBuildModule
		{
		public:
			struct GlobalInput
			{
				unsigned int randomSeed;
			};

			struct DbfInput
			{
				double hectareDensity;
			};

			struct ObjectInput
			{
				RString m_Name;
				double  m_Prob;
				double  m_NormProb;
				double  m_MinHeight;
				double  m_MaxHeight;
				double  m_MinDistance;
				double  m_MinSlope;
				double  m_MaxSlope;
				int     m_Counter;

				ObjectInput() {}
				ObjectInput(RString name, double prob, double minHeight, 
					        double maxHeight, double minDistance, double minSlope, double maxSlope)
				{
					m_Name        = name;
					m_Prob        = prob;
					m_MinHeight   = minHeight;
					m_MaxHeight   = maxHeight;
					m_MinDistance = minDistance;
					m_MinSlope    = minSlope;
					m_MaxSlope    = maxSlope;
					m_Counter     = 0;
				}

				ClassIsMovable(ObjectInput);
			};

			struct MapInput
			{
				string       m_DemType;
				string       m_FileName;
				unsigned int m_SrcLeft;
				unsigned int m_SrcTop;
				unsigned int m_SrcRight;
				unsigned int m_SrcBottom;
				double       m_DstLeft;
				double       m_DstTop;
				double       m_DstRight;
				double       m_DstBottom;

				EtRectElevationGrid<double, short> hmData;

				MapInput() 
				{
				}

				MapInput(string demType, string fileName, unsigned int srcLeft, unsigned int srcTop, 
					     unsigned int srcRight, unsigned int srcBottom, double dstLeft, double dstTop,
						 double dstRight, double dstBottom)
				{
					m_DemType   = demType;
					m_FileName  = fileName;
					m_SrcLeft   = srcLeft;
					m_SrcTop    = srcTop;
					m_SrcRight  = srcRight;
					m_SrcBottom = srcBottom;
					m_DstLeft   = dstLeft;
					m_DstTop    = dstTop;
					m_DstRight  = dstRight;
					m_DstBottom = dstBottom;
				}

				ClassIsMovable(MapInput); 
			};

		private:
			struct ShapeGeo
			{
				double minX;
				double maxX;
				double minY;
				double maxY;
				double widthBB;        // bounding box width
				double heightBB;       // bounding box height
				double area;           // area of the shape (polygon)
				double areaHectares;   // area of the shape in hectares
				double areaBB;         // bounding box area
				double areaBBHectares; // bounding box area in hectares
			};

		public:
			AdvancedRandomPlacerSlope();

			virtual void Run(IMainCommands*);
			ModuleObjectOutputArray* RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn, MapInput& mapIn,
												  AutoArray<AdvancedRandomPlacerSlope::ObjectInput, MemAllocStack<AdvancedRandomPlacerSlope::ObjectInput, 32> >& objectsIn);

			class Exception: public ExceptReg::IException
			{
				unsigned int code;
				const char* text;

			public:
				Exception(unsigned int code, const char* text) : code(code), text(text) {}
				virtual unsigned int GetCode() const { return code; }
				virtual const _TCHAR* GetDesc() const { return text; }
				virtual const _TCHAR* GetModule() const { return "AdvancedRandomPlacerSlope"; }
				virtual const _TCHAR* GetType() const { return "AdvancedRandomPlacerSlopeException"; }
			};

		private:
			string sDTED2;
			string sUSGSDEM;
			string sARCINFOASCII;
			string sXYZ;

			bool   m_MapLoaded;

			const Shapes::IShape* m_pCurrentShape;      

			ShapeGeo m_ShpGeo;
			int      m_NumberOfIndividuals;

			AutoArray<ObjectInput, MemAllocStack<ObjectInput, 32> > m_ObjectsIn;
			ModuleObjectOutputArray                                 m_ObjectsOut;

			GlobalInput m_GlobalIn;
			DbfInput    m_DbfIn;
			MapInput    m_MapIn;

			EtRectElevationGrid<double, short> m_MapData;

			bool GetShapeGeoData();
			bool GetGlobalParameters(IMainCommands* cmdIfc);
			bool GetDbfParameters(IMainCommands* cmdIfc);
			bool GetObjectsParameters(IMainCommands* cmdIfc);
			bool GetMapParameters(IMainCommands* cmdIfc);
			bool LoadHighMap();

			bool NormalizeProbs();
			bool PtCloseToObject(const Shapes::DVector& v) const;

			void CreateObjects(bool preview);
			int  GetRandomType();
			void ExportCreatedObjects(IMainCommands* cmdIfc);
			void WriteReport();
		};
	}
}
