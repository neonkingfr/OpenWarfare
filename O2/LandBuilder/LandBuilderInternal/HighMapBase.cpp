#include "StdAfx.h"
#include ".\highmapbase.h"

namespace LandBuildInt
{
	HeightMapBase::HeightMapBase(void) 
	: _minRes(FLT_MAX / 10)
	{
	}

	// -------------------------------------------------------------------------- //

	HeightMapBase::~HeightMapBase(void)
	{
	}

	// -------------------------------------------------------------------------- //

	void HeightMapBase::SetMapArea(const FloatRect& mapSize)
	{
		_mapArea = mapSize;
	}

	// -------------------------------------------------------------------------- //

	FloatRect HeightMapBase::GetMapArea()
	{
		return _mapArea;
	}

	// -------------------------------------------------------------------------- //

	void HeightMapBase::AddSamplerModule(const IHeightMapSampler* sampler, float resolution, const FloatRect* area)
	{
		FloatRect zr(0, 0, 0, 0);
		if (area == 0) area = &zr;

		_samplers.Add(SamplerModule(sampler, resolution, *area));
		if (resolution < _minRes) _minRes = resolution;
	}

	// -------------------------------------------------------------------------- //

	float HeightMapBase::GetHeight(float x, float y) const
	{
		HeightMapLocInfoC locInfo(x, y, 0);
		for (int i = 0; i < _samplers.Size(); ++i)
		{
			const SamplerModule& mod = _samplers[i];
			if ((x >= mod._area.left && x <= mod._area.right && 
				 y >= mod._area.bottom && y <= mod._area.top) || 
				(mod._area.left == 0 && mod._area.top == 0 &&  mod._area.right == 0 && mod._area.bottom == 0))
			{
				locInfo.alt_m = mod._sampler->GetHeight(locInfo);
			}
		}    
		return locInfo.alt_m;
	}
}