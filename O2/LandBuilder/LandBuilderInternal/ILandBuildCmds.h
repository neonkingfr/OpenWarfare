#pragma once

#include <el/Interfaces/IMultiInterfaceBase.hpp>
//#include "../Shapes/iShape.h"

// -------------------------------------------------------------------------- //

namespace Shapes
{
	class IShape;
    struct DVertex;
    class VertexArray;
}

// -------------------------------------------------------------------------- //

namespace LandBuilder2
{
    class IProjectTask;
	class LBRoadDefinition;
	class LBRoadPath;
}

// -------------------------------------------------------------------------- //

namespace LandBuildInt
{
	class IHeightMap;
	class IObjectMap;

	// -------------------------------------------------------------------------- //

	struct FloatRect
	{
		float left, top, right, bottom;
	public:
		FloatRect() 
		{
		}
		
		// -------------------------------------------------------------------------- //

		FloatRect(float left, float top, float right, float bottom)
		: left(left)
		, top(top)
		, right(right)
		, bottom(bottom) 
		{
		}

		// -------------------------------------------------------------------------- //

		float Width() const
		{
			return (right - left);
		}

		// -------------------------------------------------------------------------- //

		float Height() const
		{
			return (bottom - top);
		}
	};

	// -------------------------------------------------------------------------- //

	struct HeightMapLocInfo
	{
		float x;      ///<x coordinate
		float y;      ///<y coordinate
		float alt_m;  ///<altitude at coordinate
		float xn;     ///<x coord of the normal at coordinate
		float yn;     ///<y coord of the normal at coordinate
		float zn;     ///<z coord of the normal at coordinate
	};

	// -------------------------------------------------------------------------- //

	///Construction of HeightMapLocInfo
	/**
	* You can use this structure instead HeightMapLocInfo, if you prefer inline construction of the structure.
	*/
	struct HeightMapLocInfoC : public HeightMapLocInfo
	{
		HeightMapLocInfoC(float x, float y, float alt, float xn = 0, float yn = 0, float zn = 0)
		{
			this->x     = x;
			this->y     = y;
			this->alt_m = alt;
			this->xn    = xn;
			this->yn    = yn;
			this->zn    = zn;
		}
	};

	// -------------------------------------------------------------------------- //

	///Sampler is the basic part of high map interface
	/**
	* The high map is generated through samplers. Sampler is module, that should calculate height
	* in given coordinates. When high map is generated, sampler is called for each position in
	* map and ask modules for high on given coordinates.
	*
	* To define high map, you have to register sampler into the IHeightMap interface. Samplers
	* are registered in order. Later samplers can modify terrain generaded by previous samplers. 
	* 
	*/
	class IHeightMapSampler : public IMultiInterfaceBase
	{
	public:
		///Starts sampler for given position
		/**
		* Function called for given position. Read position from the structure.
		* Function should return new height. 
		* @param heightInfo current height at position. 
		* @return new height
		* @note sampler doesn't need to support normal vectors. In this case, xn..zn contains zeroes
		*/
		virtual float GetHeight(const HeightMapLocInfo& heightInfo) const = 0;
		
		// -------------------------------------------------------------------------- //

		///Called by HeightMap class in destructor
		virtual void Release() const 
		{
			delete this;
		}
		
		// -------------------------------------------------------------------------- //

		///Virtual destructor
		virtual ~IHeightMapSampler() 
		{
		}
	};

	// -------------------------------------------------------------------------- //

	class IHeightMap : public IMultiInterfaceBase
	{
	public:
        DECLARE_IFCUIDA('H','g','t','M','a','p');
		///Returns active map area in meters
		virtual FloatRect GetMapArea() = 0;

		///Starts land sampler
		/**
		* Function calls IHigMapSampler for each position at land. Sampler can choose positions in order or
		* random. It depends on high map representation and optimalization
		* @param sampler instance of client sampler
		* @param resolution recommended resolution in meters. This parameter is hint for sampler. High values
		* can cause that sampler will not detect small details in the high map. Low values causes longer sampling.
		* Advanced samplers can dynamically change this value during sampling to optimize its work and find out 
		* maximum detail.
		* @param area defines area, where sampler should run. If 0 passed, sampler will use entire map.
		*/
		virtual void AddSamplerModule(const IHeightMapSampler* sampler, float resolution = 1, const FloatRect* area = 0) = 0;

		///Retrieves height on given point
		/**
		* @param x x-position  
		* @param y y-position
		* @return height in meters
		*
		* @note This is not single lookup function. Function must process call samplers that operates at
		* given point and recalculate the height
		*/
		virtual float GetHeight(float x, float y) const = 0;
        
		virtual ~IHeightMap() 
		{
		}
	};
    
	// -------------------------------------------------------------------------- //

	class IEnumObjectCallback
	{
	public:
		virtual int operator()(unsigned int objId) const = 0;
	};

	// -------------------------------------------------------------------------- //

	class IObjectMap : public IMultiInterfaceBase
	{
	public:
        DECLARE_IFCUIDA('O','b','j','M','a','p');
		///Places object into object map
		/**
		  @param location Matrix specifies object location, orientation, etc. Hight coordinates are 
		   relative to terain height

		  @param objectName name of object at position

		  @param tag user defined tag. It useful to create groups of object. You can use tag when searching an
			object or objects. Use tag equal zero means "no group", this object is not in group, and can be find
			only with no filtering.
		*/
		virtual unsigned int PlaceObject(const Matrix4& location, const char* objectName, unsigned int tag) = 0;

	    virtual const Matrix4& GetObjectLocation(unsigned int objId) = 0;

		virtual const char* GetObjectType(unsigned int objId) = 0;
    
	    virtual unsigned int GetObjectTag(unsigned int objId) = 0;

		virtual unsigned int FindNearestObject(float x, float y, unsigned int tag, const Array<unsigned int>& ignoreIDs) = 0;

		virtual Shapes::IShape* CreateObjectShape(unsigned int objId) = 0;

		 ///Enumerates objects
		/**
		On the start, set objId to -1. Function returns true, if objId has been changed to next object id.
		When function returns false, all the objects has been enumerated.
		*/
		virtual bool EnumObjects(int& objId) = 0;

		///
		virtual int ObjectsInArea(const FloatRect& area, unsigned int tag, const IEnumObjectCallback& callback) = 0;

	    virtual int ObjectsInArea(const Shapes::IShape& shape, unsigned int tag, const IEnumObjectCallback& callback) = 0;

		virtual bool DeleteObject(unsigned int objId) = 0;

		virtual bool ReplaceObject(unsigned int objId, const Matrix4& location, const char* objectName, unsigned int tag) = 0;

		///Returns active map area in meters
		virtual FloatRect GetMapArea() const = 0;
	};

	// -------------------------------------------------------------------------- //

	class IRoadMap : public IMultiInterfaceBase
	{
	public:
        DECLARE_IFCUIDA('R','o','a','M','a','p');

		virtual void AddRoadDefinition(const LandBuilder2::LBRoadDefinition& roadDefinition) = 0;
		virtual void AddRoadPath(const LandBuilder2::LBRoadPath& roadPath) = 0;
	};

	// -------------------------------------------------------------------------- //

	class IGlobVertexArray : public IMultiInterfaceBase
    {
	public:
        DECLARE_IFCUIDA('G','V','x','A','r','r');

        virtual unsigned int AllocVertex(const Shapes::DVertex& vertex) = 0;
        virtual bool FreeVertex(unsigned int index) = 0;
        virtual const Shapes::VertexArray* GetVertexArray() const = 0;
        virtual bool SetVertex(unsigned int index, const Shapes::DVertex& vertex) = 0;
    };

	// -------------------------------------------------------------------------- //

	class IMainCommands : public IMultiInterfaceBase
	{
	public:
        DECLARE_IFCUIDA('M','a','i','n','C','m');
		///queries value of database.
		/**
		@param name variable name
		@param forceConstant true to force get value from constant params. Normally if variable exists in database
		it taken from it. Otherwise, it takes from constant params. Set this to true to take variable from constant
		params.
		@retval pointer Pointer to value text
		@retval NULL Variable was not found (forward exception is raised)
		*/
		virtual const char* QueryValue(const char* name, bool forceConstant = false) const = 0;

		// -------------------------------------------------------------------------- //

		///retrieves pointer to IShape interface of active shape
		/**
		  Module can use active shape to find borders of active area.
		  @return function should always return a valid pointer. 
		*/
		virtual const Shapes::IShape* GetActiveShape() const = 0;

		// -------------------------------------------------------------------------- //

		///retrieves pointer to IHeightMap interface
        IHeightMap* GetHeightMap() 
		{
			return GetInterface<IHeightMap>();
		}

		// -------------------------------------------------------------------------- //

		///retrieves pointer to IHeightMap interface
		const IHeightMap* GetHeightMap() const  
		{
			return GetInterface<IHeightMap>();
		}

		// -------------------------------------------------------------------------- //

		///retrieves pointer to IObjectMap interface
		IObjectMap* GetObjectMap() 
		{
			return GetInterface<IObjectMap>();
		}

		// -------------------------------------------------------------------------- //

		///retrieves pointer to IObjectMap interface
		const IObjectMap* GetObjectMap() const 
		{
			return GetInterface<IObjectMap>();
		}

		// -------------------------------------------------------------------------- //

		///retrieves pointer to IRoadMap interface
		IRoadMap* GetRoadMap() 
		{
			return GetInterface<IRoadMap>();
		}

		// -------------------------------------------------------------------------- //

		///retrieves pointer to IRoadMap interface
		const IRoadMap* GetRoadMap() const 
		{
			return GetInterface<IRoadMap>();
		}

		// -------------------------------------------------------------------------- //

		///Converts alias name to real object name
		/**
		  Typical module doesn't work with real objects, but with aliases. Real object names are
		  passed using the config.
		  @param alias name of object alias
		  @retval pointer real object name
		  @retval 0 unknown alias (forward exception is raised)
		*/
		virtual const char* ConvertName(const char* alias) const = 0;

		// -------------------------------------------------------------------------- //

		virtual const Shapes::IShape* GetObjectShape(const char* objectName) const = 0;

		// -------------------------------------------------------------------------- //

		///Creates shape object that represents transformed object
		/**
		  @return pointer to created shape object. When you don't need shape anymore, destroy it by DestroyShape function.
		*/
		virtual Shapes::IShape* CreateObjectShapeTransformed(const char* objectName, const Matrix4& trns) const = 0;

		// -------------------------------------------------------------------------- //

        ///Retrieves informations about active task
        /**
         Because one module can be used with multiple tasks, this function returns
         pointer to instance, that requests the module. You can use this pointer to
         optain additional informations about the task         
         */
        virtual const LandBuilder2::IProjectTask* GetCurrentTask() const 
		{
			return 0;
		}

		// -------------------------------------------------------------------------- //

        ///Enqueues task into queue
        /**
          Any module can create the task and enqueue the task for seconds pass.
          @param task newly created instance of task. Instance will be destroyed during
            processing. Don't enqueu
          @param highpriority when true, task is enqueued at the top of queue. Othewise it
            is enqueued at the bottom of queue. 
          @retval true success
          @retval false failed, not supported
          @note Enqueueing task during OnBeginPass with highpriority causes, that
            tasks will be processed before any other tasks. With lowpriority causes, that
            task will be processed on after any other task. Enqueuing task during OnEndPass
            will cause start new pass.          
         */
        virtual bool EnqueueTask(LandBuilder2::IProjectTask* task, bool highpriority) const 
		{
			return false;
		}
	};
};

namespace LandBuilder2
{
	using namespace LandBuildInt;
}
