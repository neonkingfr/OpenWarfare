// LandBuilderInternal.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Processor.h"
#include "ModuleFactory.h"
#include <Windows.h>

#include "IShapeExtra.h"
#include "../Shapes/ShapePoint.h"
#include "../Shapes/ShapeMultiPoint.h"
#include "../Shapes/ShapePolygon.h"
#include "../Shapes/ShapePolyLine.h"

template<class T>
class MyShapeTemplateClass: public T, public IShapeExtra
{
public:
	MyShapeTemplateClass(const T& other) 
	: T(other) 
	{
	}
	
	virtual void* GetInterfacePtr(unsigned int IFCUID)
	{
		if (IFCUID == IShapeExtra::IFCUID) 
		{
			return static_cast<IShapeExtra*>(this);
		}
		else 
		{
			return T::GetInterfacePtr(IFCUID);
		}
	}

	virtual const char* GetShapeTypeName() const;
	virtual Shapes::DVector NearestEdge(const Shapes::DVector& vx) const;

	virtual MyShapeTemplateClass<T>* NewInstance(const T& other) const 
	{
		return new MyShapeTemplateClass<T>(other);
	}
};

template<class T> 
const char* MyShapeTemplateClass<T>::GetShapeTypeName() const 
{
	return 0;
}

template<> 
const char* MyShapeTemplateClass<Shapes::ShapePoint>::GetShapeTypeName() const 
{
	return SHAPE_NAME_POINT;
}

template<> 
const char* MyShapeTemplateClass<Shapes::ShapeMultiPoint>::GetShapeTypeName() const 
{
	return SHAPE_NAME_MULTIPOINT;
}

template<>
const char* MyShapeTemplateClass<Shapes::ShapePolyLine>::GetShapeTypeName() const 
{
	return SHAPE_NAME_POLYLINE;
}

template<>
const char* MyShapeTemplateClass<Shapes::ShapePolygon>::GetShapeTypeName() const 
{
	return SHAPE_NAME_POLYGON;
}

template<class T>
Shapes::DVector MyShapeTemplateClass<T>::NearestEdge(const Shapes::DVector& vx) const 
 {
	 return T::NearestInside(vx);
}

template<>
Shapes::DVector MyShapeTemplateClass<Shapes::ShapePolygon>::NearestEdge(const Shapes::DVector& vx) const 
{
	return Shapes::ShapePolygon::NearestToEdge(vx, true);
}

class LandBuildShapeFactory : public Shapes::IShapeFactory
{
public:
	virtual Shapes::IShape* CreatePoint(const Shapes::ShapePoint& origShape)
	{
		return new MyShapeTemplateClass<Shapes::ShapePoint>(origShape);
	}

	virtual Shapes::IShape* CreateMultipoint(const Shapes::ShapeMultiPoint& origShape)
	{
		return new MyShapeTemplateClass<Shapes::ShapeMultiPoint>(origShape);
	}

	virtual Shapes::IShape* CreatePolyLine(const Shapes::ShapePoly& origShape)
	{
		return new MyShapeTemplateClass<Shapes::ShapePolyLine>(origShape);
	}

	virtual Shapes::IShape* CreatePolygon(const Shapes::ShapePoly& origShape)
	{
		return new MyShapeTemplateClass<Shapes::ShapePolygon>(origShape);
	}
};

#define MAXREMAINTIME (999 * 3600)

class SimpleProgress : public ProgressBarFunctions
{
  DWORD lastUpdate;
  int cnt;
  DWORD sumEndTime;
  float lastMarkup;
  DWORD lastMarkupTime;
  DWORD startTime;
public:
  SimpleProgress() {startTime=lastUpdate=GetTickCount();cnt=0;lastMarkupTime=lastUpdate;lastMarkup=0;sumEndTime=300000;}  
  virtual bool ManualUpdate() {return true;}
  virtual void Update()
  {
    char rot[]="\\|/-";
    DWORD curTime=GetTickCount();
    if (curTime-lastUpdate>100)
    {
      lastUpdate=curTime;
      float result=ReadResult();
      if (result<lastMarkup && result<0.01) 
      {
        lastMarkup=0;
        lastMarkupTime=startTime=GetTickCount();        
        sumEndTime=300000;
      }
      cnt=(cnt+1)&0x3;
      

      DWORD curRelTime=curTime-lastMarkupTime; 
      DWORD relMarkup=lastMarkupTime-startTime;
      float estimateProcessed=lastMarkup+(float)curRelTime/(sumEndTime/2-relMarkup)*(1-lastMarkup);
      if (estimateProcessed>=1.0f) estimateProcessed=0.99999f;
      float diff=(1-result)/(1-estimateProcessed);

      if (diff<0.8f || diff>1.2f)
      {
        DWORD processed=curTime-lastMarkupTime;
        float estimation=processed/(result-lastMarkup)*(1-result);
        float endTime=curTime+estimation-startTime;
      /*  float endTime2=(curTime-startTime)/result;
        float fact=result*2.0f-1.0f;
        fact=fabs(fact);
        DWORD endTimeF=toLargeInt(endTime*(fact)+endTime2*(1-fact));*/
        DWORD endTimeF=toLargeInt(endTime);
        lastMarkup=result;
        lastMarkupTime=curTime;
        sumEndTime=(sumEndTime-sumEndTime/2)+endTimeF;

      }      
      DWORD remainTime=(sumEndTime/2-curTime+startTime)/1000;
      if (remainTime>MAXREMAINTIME) remainTime=0;
      
      printf("%5.2f%%, %c  (%02d:%02d:%02d)  \r",result*100.0f,rot[cnt],remainTime/3600,(remainTime/60)%60,(remainTime%60));

    }
  }
};

int _tmain(int argc, _TCHAR* argv[])
{
	using namespace LandBuildInt;
	LandBuildShapeFactory shapeFactory;
	LandBuildInt::ModuleFactory modFactory;

	if (argc < 2)
	{
		puts("\n"
			 "\n"
			 "LandBuilder\n"
			 "Copyright 2007 Bohemia Interactive. All rights reserved.\n"
			 "--------------------------------------------------------\n"
			 "\n"
			 "\n"
			 "Usage: LandBuilder <project pathname> [<result file>]\n\n"
			 "<project pathname> path and filename to configuration file that defines all\n"
			 "                   required resources, shapes, and other files and definitions\n\n"
			 "<result file>      Result will be placed into this file. This file can be\n "
			 "                   imported into Visitor. If missing, project pathname\n"
			 "                   with different extension will be used.");
		return -1;
	}

	puts("\n"
		 "\n"
		 "LandBuilder\n"
		 "Copyright 2007 Bohemia Interactive. All rights reserved.\n"
		 "--------------------------------------------------------\n"
		 "\n"
		 "\n");

	Processor procs(&modFactory);
	procs.SetShapeFactory(&shapeFactory);

	SimpleProgress progressBar;
	ProgressBarFunctions::SelectGlobalProgressBarHandler(&progressBar);

	if (procs.LoadProject(argv[1]) == false)
	{
		puts("Error reported... exiting...");
		return -2;
	}

	Pathname resultName;
	if (argc > 2) 
	{
		resultName = argv[2];
	}
	else
	{
		resultName = argv[1];
		if (_stricmp(resultName.GetExtension(), ".lbt") == 0)
		{
			resultName.SetExtension(".ltx");
		}
		else
		{
			resultName.SetExtension(".lbt");
		}
	}

	puts("Processing...");

	procs.ProcessProject();

	printf("Saving result to: %s\n", resultName.GetFullPath());
	if (procs.SaveResult(resultName) == false)
	{
		puts("Unable to save result...");
		return -3;
	}
	return 0;
}