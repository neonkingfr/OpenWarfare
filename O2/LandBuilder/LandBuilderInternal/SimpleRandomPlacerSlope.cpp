#include "StdAfx.h"
#include <algorithm>
#include ".\simplerandomplacerslope.h"

namespace LandBuildInt
{
	namespace Modules
	{
		SimpleRandomPlacerSlope::SimpleRandomPlacerSlope()
		{
			m_MapLoaded = false;
		}

		// --------------------------------------------------------------------------------

		void SimpleRandomPlacerSlope::Run(IMainCommands *cmdIfc)
		{
			printf("Started a new shape                   \n");
			printf("--------------------------------------\n");

			// gets shape and its properties
			m_pCurrentShape = cmdIfc->GetActiveShape();      

			// initializes const variables
			sDTED2        = "DTED2";
			sUSGSDEM      = "USGSDEM";
			sARCINFOASCII = "ARCINFOASCII";
			sXYZ          = "XYZ";

			// gets shape geometric data
			if(GetShapeGeoData())
			{
				// gets parameters from calling cfg file
				if(GetGlobalParameters(cmdIfc))
				{
					// gets dbf parameters from calling cfg file
					if(GetDbfParameters(cmdIfc))
					{
						// gets objects parameters from calling cfg file
						if(GetObjectsParameters(cmdIfc))
						{
							if(!m_MapLoaded) // to read the map only once
							{
								// gets maps parameters from calling cfg file
								if(!GetMapParameters(cmdIfc)) return;
								// load maps
								LoadHighMap();
							}

							// initializes random seed
							srand(m_GlobalIn.randomSeed);

							// calculates the number of individual to be generated
							// (this value is referred to the bounding box of the shape
							//  and assume a uniform distribution - random generated values
							//  will be filtered to obtain the same density inside the shape,
							//  discarding the ones which will fall out of the shape itself)
							m_NumberOfIndividuals = static_cast<int>(m_ShpGeo.areaBBHectares * m_DbfIn.hectareDensity);

							// sets the population
							CreateObjects(false);

							// export objects
							ExportCreatedObjects(cmdIfc);

							// writes the report
							WriteReport();
						}
					}
				}
			}       
		}

		// --------------------------------------------------------------------//

		ModuleObjectOutputArray* SimpleRandomPlacerSlope::RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn, MapInput& mapIn,
																       SimpleRandomPlacerSlope::ObjectInput& objectsIn)
		{
			m_pCurrentShape = shape;
			m_GlobalIn      = globalIn;
			m_DbfIn         = dbfIn;
			m_MapIn         = mapIn;
			m_ObjectsIn     = objectsIn;

			if(m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();

			// initializes const variables
			sDTED2        = "DTED2";
			sUSGSDEM      = "USGSDEM";
			sARCINFOASCII = "ARCINFOASCII";
			sXYZ          = "XYZ";

			// gets shape geometric data
			if(GetShapeGeoData())
			{
				if(!m_MapLoaded) // to read the map only once
				{
					// load maps
					LoadHighMap();
				}
				else
				{
					m_MapIn.hmData = m_MapData;
				}

				// initializes random seed
				srand(m_GlobalIn.randomSeed);

				// calculates the number of individual to be generated
				// (this value is referred to the bounding box of the shape
				//  and assume a uniform distribution - random generated values
				//  will be filtered to obtain the same density inside the shape,
				//  discarding the ones which will fall out of the shape itself)
				m_NumberOfIndividuals = static_cast<int>(m_ShpGeo.areaBBHectares * m_DbfIn.hectareDensity);

				// sets the population
				CreateObjects(true);

				return &m_ObjectsOut;
			}
			else
			{
				return NULL;
			}
		}

		// --------------------------------------------------------------------//

		bool SimpleRandomPlacerSlope::GetShapeGeoData()
		{
			Shapes::DBox bBox = m_pCurrentShape->CalcBoundingBox();

			m_ShpGeo.minX = bBox.lo.x;
			m_ShpGeo.minY = bBox.lo.y;
			m_ShpGeo.maxX = bBox.hi.x;
			m_ShpGeo.maxY = bBox.hi.y;

			m_ShpGeo.area         = GetShapeArea(m_pCurrentShape);
			m_ShpGeo.areaHectares = m_ShpGeo.area / 10000.0;

			m_ShpGeo.widthBB        = m_ShpGeo.maxX - m_ShpGeo.minX;
			m_ShpGeo.heightBB       = m_ShpGeo.maxY - m_ShpGeo.minY;
			m_ShpGeo.areaBB         = m_ShpGeo.widthBB * m_ShpGeo.heightBB;
			m_ShpGeo.areaBBHectares = m_ShpGeo.areaBB / 10000.0;

			return true;
		}

		// --------------------------------------------------------------------------------

		bool SimpleRandomPlacerSlope::GetGlobalParameters(IMainCommands* cmdIfc)
		{
			const char* x = cmdIfc->QueryValue("randomSeed", false);
			if (x != 0) 
			{
				if(IsInteger(string(x), false))
				{
					m_GlobalIn.randomSeed = static_cast<unsigned int>(atoi(x));
				}
				else
				{
					ExceptReg::RegExcpt(Exception(11, ">>> Bad data for randomSeed <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(12, ">>> Missing value for randomSeed <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool SimpleRandomPlacerSlope::GetDbfParameters(IMainCommands* cmdIfc)
		{
			m_DbfIn.hectareDensity = 100.0; // default value
			const char* x = cmdIfc->QueryValue("hectareDensity", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.hectareDensity = atof(x);
				}
				else
				{
					printf(">>> ------------------------------------------------------- <<<\n");
					printf(">>> Bad data for hectareDensity - using default value (100) <<<\n");
					printf(">>> ------------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> ------------------------------------------------------------ <<<\n");
				printf(">>> Missing value for hectareDensity - using default value (100) <<<\n");
				printf(">>> ------------------------------------------------------------ <<<\n");
			}
			if (m_DbfIn.hectareDensity < 0.01)
			{
				ExceptReg::RegExcpt(Exception(21, ">>> Density is too low (less then 0.01) <<<"));
				return false;
			}
			return true;
		}

		// --------------------------------------------------------------------------------

		bool SimpleRandomPlacerSlope::GetObjectsParameters(IMainCommands* cmdIfc)
		{
			// resets arrays
			if(m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();

			char object[50]   = "object";
			char minslope[50] = "minslope";
			char maxslope[50] = "maxslope";

			// object type
			const char* x = cmdIfc->QueryValue(object, false);
			if (x == 0) return false;
			m_ObjectsIn.m_Name = x;

			// object min slope
			x = cmdIfc->QueryValue(minslope, false);
			if (x != 0)
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_ObjectsIn.m_MinSlope = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(31, ">>> Bad data for minslope <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(32, ">>> Missing minslope value <<<"));
				return false;
			}
			
			// object max slope
			x = cmdIfc->QueryValue(maxslope, false);
			if (x != 0)
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_ObjectsIn.m_MaxSlope = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(33, ">>> Bad data for maxslope <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(34, ">>> Missing maxslope value <<<"));
				return false;
			}

			m_ObjectsIn.m_Counter = 0;

			return true;
		}

		// --------------------------------------------------------------------------------

		bool SimpleRandomPlacerSlope::GetMapParameters(IMainCommands* cmdIfc)
		{
			// gets demType from cfg file
			const char* tmp = cmdIfc->QueryValue("demType", false);
			if (tmp == 0) 
			{
				ExceptReg::RegExcpt(Exception(51, ">>> Missing Dem\'s type <<<"));
				return false;
			}

			m_MapIn.m_DemType = tmp;

			// checks demType
			if((m_MapIn.m_DemType != sDTED2) && (m_MapIn.m_DemType != sUSGSDEM) && (m_MapIn.m_DemType != sARCINFOASCII) && (m_MapIn.m_DemType != sXYZ))
			{
				ExceptReg::RegExcpt(Exception(52, ">>> Dem type not implemented <<<"));
				return false;
			}

			// gets filename from cfg file
			tmp = cmdIfc->QueryValue("filename", false);
			if (tmp == 0)
			{
				ExceptReg::RegExcpt(Exception(53, ">>> Missing Dem\'s filename <<<"));
				return false;
			}

			m_MapIn.m_FileName = tmp;

			// checks file extension
			if(m_MapIn.m_DemType == sDTED2) 
			{
				string tmpFileName = m_MapIn.m_FileName;
				transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

				if(!(tmpFileName.find(".DT2") == tmpFileName.length() - 4))
				{
					ExceptReg::RegExcpt(Exception(54, ">>> The file is not a DTED2 <<<"));
					return false;
				}
			}
			if(m_MapIn.m_DemType == sUSGSDEM) 
			{
				string tmpFileName = m_MapIn.m_FileName;
				transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

				if(!(tmpFileName.find(".DEM") == tmpFileName.length() - 4))
				{
					ExceptReg::RegExcpt(Exception(55, ">>> The file is not a USGS DEM <<<"));
					return false;
				}
			}
			if(m_MapIn.m_DemType == sARCINFOASCII) 
			{
				string tmpFileName = m_MapIn.m_FileName;
				transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

				if((!(tmpFileName.find(".GRD") == tmpFileName.length() - 4)) && (!(tmpFileName.find(".ASC") == tmpFileName.length() - 4)))
				{
					ExceptReg::RegExcpt(Exception(56, ">>> The file is not a Arc/Info ASCII Grid <<<"));
					return false;
				}
			}
			if(m_MapIn.m_DemType == sXYZ) 
			{
				string tmpFileName = m_MapIn.m_FileName;
				transform(tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper);

				if(!(tmpFileName.find(".XYZ") == tmpFileName.length() - 4))
				{
					ExceptReg::RegExcpt(Exception(56, ">>> The file is not a XYZ <<<"));
					return false;
				}
			}

			// gets srcLeft from cfg file
			tmp = cmdIfc->QueryValue("srcLeft", false);
			if (tmp != 0)
			{
				if(IsInteger(string(tmp), false))
				{
					m_MapIn.m_SrcLeft = atoi(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(61, ">>> Bad data for srcLeft <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(62, ">>> Missing srcLeft value <<<"));
				return false;
			}

			// gets srcRight from cfg file
			tmp = cmdIfc->QueryValue("srcRight", false);
			if (tmp != 0)
			{
				if(IsInteger(string(tmp), false))
				{
					m_MapIn.m_SrcRight = atoi(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(63, ">>> Bad data for srcRight <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(64, ">>> Missing srcRight value <<<"));
				return false;
			}

			// gets srcTop from cfg file
			tmp = cmdIfc->QueryValue("srcTop", false);
			if (tmp != 0)
			{
				if(IsInteger(string(tmp), false))
				{
					m_MapIn.m_SrcTop = atoi(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(65, ">>> Bad data for srcTop <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(66, ">>> Missing srcTop value <<<"));
				return false;
			}

			// gets srcBottom from cfg file
			tmp = cmdIfc->QueryValue("srcBottom", false);
			if (tmp != 0)
			{
				if(IsInteger(string(tmp), false))
				{
					m_MapIn.m_SrcBottom = atoi(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(67, ">>> Bad data for srcBottom <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(68, ">>> Missing srcBottom value <<<"));
				return false;
			}

			// gets dstLeft from cfg file
			tmp = cmdIfc->QueryValue("dstLeft", false);
			if (tmp != 0)
			{
				if(IsFloatingPoint(string(tmp), false))
				{
					m_MapIn.m_DstLeft = atof(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(69, ">>> Bad data for dstLeft <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(70, ">>> Missing dstLeft value <<<"));
				return false;
			}

			// gets dstRight from cfg file
			tmp = cmdIfc->QueryValue("dstRight", false);
			if (tmp != 0)
			{
				if(IsFloatingPoint(string(tmp), false))
				{
					m_MapIn.m_DstRight = atof(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(71, ">>> Bad data for dstRight <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(72, ">>> Missing dstRight value <<<"));
				return false;
			}

			// gets dstTop from cfg file
			tmp = cmdIfc->QueryValue("dstTop", false);
			if (tmp != 0)
			{
				if(IsFloatingPoint(string(tmp), false))
				{
					m_MapIn.m_DstTop = atof(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(73, ">>> Bad data for dstTop <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(74, ">>> Missing dstTop value <<<"));
				return false;
			}

			// gets dstBottom from cfg file
			tmp = cmdIfc->QueryValue("dstBottom", false);
			if (tmp != 0)
			{
				if(IsFloatingPoint(string(tmp), false))
				{
					m_MapIn.m_DstBottom = atof(tmp);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(75, ">>> Bad data for dstBottom <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(76, ">>> Missing dstBottom value <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool SimpleRandomPlacerSlope::LoadHighMap()
		{
			if(m_MapIn.m_DemType == sDTED2)
			{
				if(!(m_MapData.loadFromDT2(m_MapIn.m_FileName)))
				{
					ExceptReg::RegExcpt(Exception(81, ">>> Error while loading DTED2 file <<<"));
					return false;
				}
			}
			if(m_MapIn.m_DemType == sUSGSDEM)
			{
				if(!(m_MapData.loadFromUSGSDEM(m_MapIn.m_FileName)))
				{
					ExceptReg::RegExcpt(Exception(82, ">>> Error while loading USGS DEM file <<<"));
					return false;
				}
				else
				{
					// if the elevation units in the DEM are feet, converts to METERS
					if(m_MapData.getWorldUnitsW() == EtRectElevationGrid<double, short>::euFEET)
					{
						m_MapData.convertWorldUnitsW(EtRectElevationGrid<double, short>::euMETERS);
					}
				}
			}
			if(m_MapIn.m_DemType == sARCINFOASCII)
			{
				if(!(m_MapData.loadFromARCINFOASCIIGrid(m_MapIn.m_FileName)))
				{
					ExceptReg::RegExcpt(Exception(83, ">>> Error while loading Arc/Info Ascii Grid file <<<"));
					return false;
				}
			}
			if(m_MapIn.m_DemType == sXYZ)
			{
				if(!(m_MapData.loadFromXYZ(m_MapIn.m_FileName)))
				{
					ExceptReg::RegExcpt(Exception(84, ">>> Error while loading xyz file <<<"));
					return false;
				}
			}
			m_MapIn.hmData = m_MapData;

			m_MapIn.hmData.offsetOriginWorldU(-m_MapIn.hmData.getOriginWorldU());
			m_MapIn.hmData.offsetOriginWorldV(-m_MapIn.hmData.getOriginWorldV());

			m_MapLoaded = true;

			return true;
		}

		// --------------------------------------------------------------------//

		void SimpleRandomPlacerSlope::CreateObjects(bool preview)
		{
			ProgressBar<int> pb(m_NumberOfIndividuals);
			
			for(int i = 0; i < m_NumberOfIndividuals; ++i)
			{
				if(!preview) pb.AdvanceNext(1);

				// random position
				double x0 = RandomInRangeF(0, m_ShpGeo.widthBB);
				double y0 = RandomInRangeF(0, m_ShpGeo.heightBB);
				Shapes::DVector v0 = Shapes::DVector(x0 + m_ShpGeo.minX, y0 + m_ShpGeo.minY);

				// the random point is inside the current shape ?
				if (m_pCurrentShape->PtInside(v0))
				{
					double slope = m_MapIn.hmData.getWorldSlopeAt(v0.x, v0.y, EtRectElevationGrid<double, short>::imARMA) * 100;

					if((slope >= m_ObjectsIn.m_MinSlope) && (slope <= m_ObjectsIn.m_MaxSlope))
					{
						ModuleObjectOutput out;

						// sets data
						out.position = v0;
						out.type     = 0;
						out.rotation = RandomInRangeF(0, EtMathD::TWO_PI);
						out.scaleX = out.scaleY = out.scaleZ = 1.0;

						// used only by VLB for preview
						out.length = 10.0;
						out.width  = 10.0;
						out.height = 10.0;

						// adds to array
						m_ObjectsOut.Add(out);

						// update type counter
						m_ObjectsIn.m_Counter++;
					}
				}
			}
		}

		// --------------------------------------------------------------------//

		void SimpleRandomPlacerSlope::ExportCreatedObjects(IMainCommands* cmdIfc)
		{
			int objOutCount = m_ObjectsOut.Size();
			for(int i = 0; i < objOutCount; ++i)
			{
				RString name      = m_ObjectsIn.m_Name;
				Vector3 position  = Vector3(static_cast<Coord>(m_ObjectsOut[i].position.x), 0, static_cast<Coord>(m_ObjectsOut[i].position.y));

				Matrix4 mTranslation = Matrix4(MTranslation, position);
				Matrix4 mRotation    = Matrix4(MRotationY, static_cast<Coord>(m_ObjectsOut[i].rotation));
				Matrix4 mScaling     = Matrix4(MScale, static_cast<Coord>(m_ObjectsOut[i].scaleX), static_cast<Coord>(m_ObjectsOut[i].scaleZ), static_cast<Coord>(m_ObjectsOut[i].scaleY));

				Matrix4 transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject(transform, name, 0);
			}
		}

		// --------------------------------------------------------------------//

		void SimpleRandomPlacerSlope::WriteReport()
		{
			printf("Created                  \n");
			printf("%d", m_ObjectsIn.m_Counter);
			printf(" : ");
			printf(m_ObjectsIn.m_Name);
			printf("                           \n");
			printf("--------------------------------------\n");
		}
	}
}