#pragma once
#include "core.h"

namespace LandBuildInt
{
	class ModuleFactory : public IModuleFactory
	{
	public:
		virtual IBuildModule* GetModule(const char* name);
	};
}