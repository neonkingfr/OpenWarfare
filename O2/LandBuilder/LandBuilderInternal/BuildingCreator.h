#pragma once

// -------------------------------------------------------------------------- //

#include ".\IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>
#include ".\BuildingCreatorHelpers.h"

// -------------------------------------------------------------------------- //

using namespace BC_Help;

// -------------------------------------------------------------------------- //

namespace LandBuildInt
{
	namespace Modules
	{
		class BuildingCreator : public IBuildModule
		{
		public:
			struct GlobalInput
			{
				RString modelsPath;
				RString texturesPath;
				unsigned int randomSeed;

				double matchTol;
				double vertexTol;
				double rectCornerTol;
			};

			struct DbfInput
			{
				unsigned int numOfFloors;
				double       floorHeight;
				unsigned int roofType;
				bool         rectify;
				RString      texFile;
			};

			class Exception : public ExceptReg::IException
			{
				unsigned int code;
				const char* text;

			public:
				Exception(unsigned int code, const char* text) 
				: code(code)
				, text(text) 
				{
				}

				virtual unsigned int GetCode() const 
				{ 
					return code; 
				}

				virtual const _TCHAR* GetDesc() const 
				{ 
					return text; 
				}

				virtual const _TCHAR* GetModule() const 
				{ 
					return "BuildingCreator"; 
				}

				virtual const _TCHAR* GetType() const 
				{ 
					return "BuildingCreatorException"; 
				}
			};

		public:
			BuildingCreator();
			~BuildingCreator();

			virtual void Run(IMainCommands* cmdIfc);

		private:
			bool m_FirstRun;

			const Shapes::IShape* m_pCurrentShape; 
			GlobalInput           m_GlobalIn;
			DbfInput              m_DbfIn;

			Dxf2006Writer m_DxfWriter;

			Building m_Building;

			size_t m_ShapeCounter;

			bool GetGlobalParameters(IMainCommands* cmdIfc);
			bool GetDbfParameters(IMainCommands* cmdIfc);
			void ExportDxf();
		};
	}
}
