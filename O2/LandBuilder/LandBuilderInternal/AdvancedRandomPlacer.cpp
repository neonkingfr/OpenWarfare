#include "StdAfx.h"
#include ".\AdvancedRandomPlacer.h"

#include "..\HighMapLoaders\include\EtMath.h"
#include "..\HighMapLoaders\include\EtHelperFunctions.h"

namespace LandBuildInt
{
	namespace Modules
	{
		// --------------------------------------------------------------------------------

		void AdvancedRandomPlacer::Run(IMainCommands *cmdIfc)
		{
			printf("Started a new shape                   \n");
			printf("--------------------------------------\n");

			// gets shape and its properties
			m_pCurrentShape = cmdIfc->GetActiveShape();      

			// gets shape geometric data
			if (GetShapeGeoData())
			{
				// gets parameters from calling cfg file
				if (GetGlobalParameters(cmdIfc))
				{
					// gets dbf parameters from calling cfg file
					if (GetDbfParameters(cmdIfc))
					{
						// gets objects parameters from calling cfg file
						if(GetObjectsParameters(cmdIfc))
						{
							// normalizes probabilities
							if (NormalizeProbs())
							{
								// initializes random seed
								srand(m_GlobalIn.randomSeed);

								// calculates the number of individual to be generated
								// (this value is referred to the bounding box of the shape
								//  and assume a uniform distribution - random generated values
								//  will be filtered to obtain the same density inside the shape,
								//  discarding the ones which will fall out of the shape itself)
								m_NumberOfIndividuals = static_cast<int>(m_ShpGeo.areaBBHectares * m_DbfIn.hectareDensity);

								// sets the population
								CreateObjects(false);

								// export objects
								ExportCreatedObjects(cmdIfc);

								// writes the report
								WriteReport();
							}													
						}
					}
				}
			}       
		}

		// --------------------------------------------------------------------//

		ModuleObjectOutputArray* AdvancedRandomPlacer::RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn, 
																    AutoArray<AdvancedRandomPlacer::ObjectInput, MemAllocStack<AdvancedRandomPlacer::ObjectInput, 32> >& objectsIn)
		{
			m_pCurrentShape = shape;
			m_GlobalIn      = globalIn;
			m_DbfIn         = dbfIn;
			m_ObjectsIn     = objectsIn;

			if (m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();

			// gets shape geometric data
			if (GetShapeGeoData())
			{
				// normalizes probabilities
				if (NormalizeProbs())
				{
					// initializes random seed
					srand(m_GlobalIn.randomSeed);

					// calculates the number of individual to be generated
					// (this value is referred to the bounding box of the shape
					//  and assume a uniform distribution - random generated values
					//  will be filtered to obtain the same density inside the shape,
					//  discarding the ones which will fall out of the shape itself)
					m_NumberOfIndividuals = static_cast<int>(m_ShpGeo.areaBBHectares * m_DbfIn.hectareDensity);

					// sets the population
					CreateObjects(true);

					return &m_ObjectsOut;
				}
				else
				{
					return NULL;
				}
			}
			else
			{
				return NULL;
			}
		}

		// --------------------------------------------------------------------//

		bool AdvancedRandomPlacer::GetShapeGeoData()
		{
			Shapes::DBox bBox = m_pCurrentShape->CalcBoundingBox();

			m_ShpGeo.minX = bBox.lo.x;
			m_ShpGeo.minY = bBox.lo.y;
			m_ShpGeo.maxX = bBox.hi.x;
			m_ShpGeo.maxY = bBox.hi.y;

			m_ShpGeo.area         = GetShapeArea(m_pCurrentShape);
			m_ShpGeo.areaHectares = m_ShpGeo.area / 10000.0;

			m_ShpGeo.widthBB        = m_ShpGeo.maxX - m_ShpGeo.minX;
			m_ShpGeo.heightBB       = m_ShpGeo.maxY - m_ShpGeo.minY;
			m_ShpGeo.areaBB         = m_ShpGeo.widthBB * m_ShpGeo.heightBB;
			m_ShpGeo.areaBBHectares = m_ShpGeo.areaBB / 10000.0;

			return true;
		}

		// --------------------------------------------------------------------------------

		bool AdvancedRandomPlacer::GetGlobalParameters(IMainCommands* cmdIfc)
		{
			const char* x = cmdIfc->QueryValue("randomSeed", false);
			if (x != 0) 
			{
				if (IsInteger(string(x), false))
				{
					m_GlobalIn.randomSeed = static_cast<unsigned int>(atoi(x));
				}
				else
				{
					ExceptReg::RegExcpt(Exception(11, ">>> Bad data for randomSeed <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(12, ">>> Missing randomSeed value <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool AdvancedRandomPlacer::GetDbfParameters(IMainCommands* cmdIfc)
		{
			m_DbfIn.hectareDensity = 100.0; // default value
			const char* x = cmdIfc->QueryValue("hectareDensity", false);
			if (x != 0) 
			{
				if (IsFloatingPoint(string(x), false))
				{
					m_DbfIn.hectareDensity = atof(x);
				}
				else
				{
					printf(">>> ------------------------------------------------------- <<<\n");
					printf(">>> Bad data for hectareDensity - using default value (100) <<<\n");
					printf(">>> ------------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> -------------------------------------------------------- <<<\n");
				printf(">>> Missing hectareDensity value - using default value (100) <<<\n");
				printf(">>> -------------------------------------------------------- <<<\n");
			}
			if (m_DbfIn.hectareDensity < 0.01)
			{
				ExceptReg::RegExcpt(Exception(21, ">>> hectareDensity is too low (less then 0.01) <<<"));
				return false;
			}
			return true;
		}

		// --------------------------------------------------------------------------------

		bool AdvancedRandomPlacer::GetObjectsParameters(IMainCommands* cmdIfc)
		{
			// resets arrays
			if (m_ObjectsIn.Size() != 0)
			{
				m_ObjectsIn.Clear();
				m_ObjectsOut.Clear();
			}

			int counter = 1;
			do 
			{
				char object[50], minheight[50], maxheight[50], mindist[50], prob[50];
				sprintf_s(object,    "object%d",    counter);
				sprintf_s(minheight, "minheight%d", counter);
				sprintf_s(maxheight, "maxheight%d", counter);
				sprintf_s(mindist,   "mindist%d",   counter);
				sprintf_s(prob,      "prob%d",      counter);

				ObjectInput nfo;

				// object type
				const char* x = cmdIfc->QueryValue(object, false);
				if (x == 0) break;
				nfo.m_Name = x;
        
				// object min height
				nfo.m_MinHeight = 100.0; // default value
				x = cmdIfc->QueryValue(minheight, false);
				if (x != 0)
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_MinHeight = atof(x);
					}
					else
					{
						printf(">>> ---------------------------------------------------- <<<\n");
						printf(">>> Bad data for minheight - using default value (100.0) <<<\n");
						printf(">>> ---------------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> ----------------------------------------------------- <<<\n");
					printf(">>> Missing minheight value - using default value (100.0) <<<\n");
					printf(">>> ----------------------------------------------------- <<<\n");
				}

				// object max height
				nfo.m_MaxHeight = 100.0; // default value
				x = cmdIfc->QueryValue(maxheight, false);
				if (x != 0)
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_MaxHeight = atof(x);
					}
					else
					{
						printf(">>> ---------------------------------------------------- <<<\n");
						printf(">>> Bad data for maxheight - using default value (100.0) <<<\n");
						printf(">>> ---------------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> ----------------------------------------------------- <<<\n");
					printf(">>> Missing maxheight value - using default value (100.0) <<<\n");
					printf(">>> ----------------------------------------------------- <<<\n");
				}
        
				// object min distance
				nfo.m_MinDistance = 5.0; // default value
				x = cmdIfc->QueryValue(mindist, false);
				if (x != 0) 
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_MinDistance = atof(x);
					}
					else
					{
						printf(">>> ------------------------------------------------ <<<\n");
						printf(">>> Bad data for mindist - using default value (5.0) <<<\n");
						printf(">>> ------------------------------------------------ <<<\n");
					}
				}
				else
				{
					printf(">>> ----------------------------------------------------- <<<\n");
					printf(">>> Missing value for mindist - using default value (5.0) <<<\n");
					printf(">>> ----------------------------------------------------- <<<\n");
				}

				// object prob
				nfo.m_Prob = 100.0; // default value
				x = cmdIfc->QueryValue(prob, false);
				if (x != 0)
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_Prob = atof(x);
					}
					else
					{
						printf(">>> ----------------------------------------------- <<<\n");
						printf(">>> Bad data for prob - using default value (100.0) <<<\n");
						printf(">>> ----------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> ------------------------------------------------ <<<\n");
					printf(">>> Missing prob value - using default value (100.0) <<<\n");
					printf(">>> ------------------------------------------------ <<<\n");
				}

				nfo.m_Counter = 0;

				m_ObjectsIn.Add(nfo);
				counter++;
			} 
			while(true); 
			return true;
		}

		// --------------------------------------------------------------------//

		bool AdvancedRandomPlacer::NormalizeProbs()
		{
			double total = 0.0;

			int objInCount = m_ObjectsIn.Size();
			for (int i = 0; i < objInCount; ++i)
			{
				total += m_ObjectsIn[i].m_Prob;
			}

			if (total == 0.0) 
			{
				ExceptReg::RegExcpt(Exception(41, ">>> Total probability equal to zero <<<"));
				return false;
			}

			double invTotal = 1 / total;
			for (int i = 0; i < objInCount; ++i)
			{
				m_ObjectsIn[i].m_NormProb = m_ObjectsIn[i].m_Prob * invTotal;
			}

			return true;
		}

		// --------------------------------------------------------------------//

		bool AdvancedRandomPlacer::PtCloseToObject(const Shapes::DVector& v) const
		{
			int objOutCount = m_ObjectsOut.Size();
			for (int j = 0; j < objOutCount; ++j)
			{
				double distSq = v.DistanceXY2(m_ObjectsOut[j].position);

				// is close to already existing objects ?
				double minDistance = m_ObjectsIn[m_ObjectsOut[j].type].m_MinDistance;
				double minDistanceSq = minDistance * minDistance; 
				if (distSq < minDistanceSq)
				{
					return true;
				}
			}
			return false;
		}

		// --------------------------------------------------------------------//

		void AdvancedRandomPlacer::CreateObjects(bool preview)
		{
			ProgressBar<int> pb(m_NumberOfIndividuals);
			
			for (int i = 0; i < m_NumberOfIndividuals; ++i)
			{
				if (!preview) pb.AdvanceNext(1);

				// random position
				double x0 = RandomInRangeF(0, m_ShpGeo.widthBB);
				double y0 = RandomInRangeF(0, m_ShpGeo.heightBB);
				Shapes::DVector v0 = Shapes::DVector(x0 + m_ShpGeo.minX, y0 + m_ShpGeo.minY);

				// the random point is inside the current shape ?
				if (m_pCurrentShape->PtInside(v0))
				{
					if (!PtCloseToObject(v0))
					{
						// random type
						int type = GetRandomType();

						ModuleObjectOutput out;

						// sets data
						out.position = v0;
						out.type     = type;
						out.rotation = RandomInRangeF(0, EtMathD::TWO_PI);

						double minScale = m_ObjectsIn[type].m_MinHeight;
						double maxScale = m_ObjectsIn[type].m_MaxHeight;

						out.scaleX = out.scaleY = out.scaleZ = RandomInRangeF(minScale, maxScale) / 100.0;

						// used only by VLB for preview
						out.length   = 10.0;
						out.width    = 10.0;
						out.height   = 10.0;

						// adds to array
						m_ObjectsOut.Add(out);

						// update type counter
						m_ObjectsIn[type].m_Counter++;
					}
				}
			}
		}

		// --------------------------------------------------------------------//

		int AdvancedRandomPlacer::GetRandomType()
		{
			double p = (double)rand() / ((double)RAND_MAX + 1);

			bool found = false;
			int index  = 0;
			double curProb = m_ObjectsIn[index].m_NormProb;
			while (!found)
			{
				if (p < curProb)
				{
					found = true;
				}
				else
				{
					index++;
					curProb += m_ObjectsIn[index].m_NormProb;
				}
			}
			return index;
		}

		// --------------------------------------------------------------------//

		void AdvancedRandomPlacer::ExportCreatedObjects(IMainCommands* cmdIfc)
		{
			int objOutCount = m_ObjectsOut.Size();
			for (int i = 0; i < objOutCount; ++i)
			{
				RString name      = m_ObjectsIn[m_ObjectsOut[i].type].m_Name;
				Vector3 position  = Vector3(static_cast<Coord>(m_ObjectsOut[i].position.x), 0, static_cast<Coord>(m_ObjectsOut[i].position.y));

				Matrix4 mTranslation = Matrix4(MTranslation, position);
				Matrix4 mRotation    = Matrix4(MRotationY, static_cast<Coord>(m_ObjectsOut[i].rotation));
				Matrix4 mScaling     = Matrix4(MScale, static_cast<Coord>(m_ObjectsOut[i].scaleX), static_cast<Coord>(m_ObjectsOut[i].scaleZ), static_cast<Coord>(m_ObjectsOut[i].scaleY));

				Matrix4 transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject(transform, name, 0);
			}
		}

		// --------------------------------------------------------------------//

		void AdvancedRandomPlacer::WriteReport()
		{
			printf("Created                  \n");
			int objInCount = m_ObjectsIn.Size();
			for (int i = 0; i < objInCount; ++i)
			{
				printf("%d", m_ObjectsIn[i].m_Counter);
				printf(" : ");
				printf(m_ObjectsIn[i].m_Name);
				printf("                           \n");
			}
			printf("--------------------------------------\n");
		}
	}
}