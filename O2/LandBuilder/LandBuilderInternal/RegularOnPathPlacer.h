#pragma once

// -------------------------------------------------------------------------- //

#include ".\IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>
#include "..\Shapes\ShapeHelpers.h"

// -------------------------------------------------------------------------- //

namespace LandBuildInt
{
	namespace Modules
	{
		class RegularOnPathPlacer : public IBuildModule
		{
		public:
			struct GlobalInput
			{
				unsigned int randomSeed;
			};

			struct DbfInput
			{
				double offset;
				double offsetMaxNoise;
				double step;
				double stepMaxNoise;
				double firstStep;
			};

			struct ObjectInput
			{
				RString m_Name;
				double  m_MinHeight;
				double  m_MaxHeight;
				bool    m_MatchOrient;
				double  m_DirCorrection;
				bool    m_PlaceRight;
				bool    m_PlaceLeft;

				ObjectInput() 
				{
				}

				ObjectInput(RString name, double minHeight, double maxHeight, 
							bool matchOrient, double dirCorrection, bool placeRight, bool placeLeft)
				: m_Name(name)
				, m_MinHeight(minHeight)
				, m_MaxHeight(maxHeight)
				, m_MatchOrient(matchOrient)
				, m_DirCorrection(dirCorrection)
				, m_PlaceRight(placeRight)
				, m_PlaceLeft(placeLeft)
				{
				}

				ClassIsMovable(ObjectInput); 
			};

		public:
			RegularOnPathPlacer();
			~RegularOnPathPlacer();

			virtual void Run(IMainCommands* cmdIfc);
			ModuleObjectOutputArray* RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn,
												  AutoArray<RegularOnPathPlacer::ObjectInput, MemAllocStack<RegularOnPathPlacer::ObjectInput, 32> >& objectsIn);

			class Exception: public ExceptReg::IException
			{
				unsigned int code;
				const char*  text;

			public:
				Exception(unsigned int code, const char* text) 
				: code(code)
				, text(text) 
				{
				}

				virtual unsigned int GetCode() const 
				{ 
					return code; 
				}

				virtual const _TCHAR* GetDesc() const 
				{ 
					return text; 
				}

				virtual const _TCHAR* GetModule() const 
				{ 
					return "RegularOnPathPlacer"; 
				}

				virtual const _TCHAR* GetType() const 
				{ 
					return "RegularOnPathPlacerException"; 
				}
			};

		private:
			AutoArray<ObjectInput, MemAllocStack<ObjectInput, 32> > m_ObjectsIn;
			ModuleObjectOutputArray                                 m_ObjectsOut;

			GlobalInput           m_GlobalIn;
			DbfInput              m_DbfIn;
			const Shapes::IShape* m_pCurrentShape;      

			bool GetGlobalParameters(IMainCommands* cmdIfc);
			bool GetDbfParameters(IMainCommands* cmdIfc);
			bool GetObjectsParameters(IMainCommands* cmdIfc);
			void CreateObjects(bool preview);
			void ExportCreatedObjects(IMainCommands* cmdIfc);

			Shapes::DVertex ConvertToXY(double s, Shapes::DVertex& v1, Shapes::DVertex& v2);
		};
	}
}
