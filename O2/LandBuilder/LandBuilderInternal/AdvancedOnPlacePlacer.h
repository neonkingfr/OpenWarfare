#pragma once

#include ".\IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>
#include "..\Shapes\ShapeHelpers.h"

namespace LandBuildInt
{
	namespace Modules
	{
		class AdvancedOnPlacePlacer : public IBuildModule
		{
		public:
			struct DbfInput
			{
				RString name;
				double dirCorrection;
			};

		public:
			virtual void Run(IMainCommands*);
			ModuleObjectOutputArray* RunAsPreview(const Shapes::IShape* shape, DbfInput& dbfIn);

			class Exception : public ExceptReg::IException
			{
				unsigned int code;
				const char* text;

			public:
				Exception(unsigned int code, const char* text) 
				: code(code)
				, text(text) 
				{
				}

				virtual unsigned int GetCode() const 
				{ 
					return code; 
				}

				virtual const _TCHAR* GetDesc() const 
				{ 
					return text; 
				}

				virtual const _TCHAR* GetModule() const 
				{ 
					return "AdvancedOnPlacePlacer"; 
				}

				virtual const _TCHAR* GetType() const 
				{ 
					return "AdvancedOnPlacePlacerException"; 
				}
			};

		private:
			const Shapes::IShape* m_pCurrentShape;   
			DbfInput              m_DbfIn;

			double m_Orientation;

			ModuleObjectOutputArray m_ObjectsOut;

			bool GetDbfParameters(IMainCommands* cmdIfc);
			void SetGeometry();
			void CreateObjects();
			void ExportCreatedObjects(IMainCommands* cmdIfc);
			void WriteReport();
		};
	}
}