#pragma once

#include ".\IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>

#include "..\Transform2D\include\Transform2Manager.h"

namespace LandBuildInt
{
	namespace Modules
	{
		class Transform2D : public IBuildModule
		{
		public:
			struct GlobalInput
			{
				Transformations2 type;
			};

			struct DbfInput
			{
				double x;
				double y;
				bool   used; 
				bool   last;
			};

		public:
			virtual void Run(IMainCommands*);

			class Exception: public ExceptReg::IException
			{
				unsigned int code;
				const char* text;

			public:
				Exception(unsigned int code,const char* text) : code(code), text(text) {}
				virtual unsigned int GetCode() const { return code; }
				virtual const _TCHAR* GetDesc() const { return text; }
				virtual const _TCHAR* GetModule() const { return "Transform2D"; }
				virtual const _TCHAR* GetType() const { return "Transform2DException"; }
			};

		private:
			HomologousPoint2List<double> m_HomoPoints;

		private:
			const Shapes::IShape* m_pCurrentShape;   
			GlobalInput           m_GlobalIn;
			DbfInput              m_DbfIn;

			bool GetGlobalParameters(IMainCommands* cmdIfc);
			bool GetDbfParameters(IMainCommands* cmdIfc);
			void UpdateHomoPoints();
			bool Solve();
		};
	}
}
