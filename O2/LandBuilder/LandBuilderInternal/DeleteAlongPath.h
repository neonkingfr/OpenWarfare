//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include ".\IBuildModule.h"
#include "..\Shapes\IShape.h"
#include <el/MultiThread/ExceptionRegister.h>

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{
		class DeleteAlongPath : public IBuildModule
		{
		public:
			struct DbfInput
			{
				double width;
			};

		public:
			virtual void Run( IMainCommands* );

			class Exception: public ExceptReg::IException
			{
				unsigned int code;
				const char* text;

			public:
				Exception( unsigned int code, const char* text ) 
        : code( code )
        , text( text ) 
        {
        }

				virtual unsigned int GetCode() const 
        { 
          return (code); 
        }

				virtual const _TCHAR* GetDesc() const 
        { 
          return (text); 
        }

				virtual const _TCHAR* GetModule() const 
        { 
          return ("DeleteAlongPath"); 
        }

				virtual const _TCHAR* GetType() const 
        { 
          return ("DeleteAlongPathException"); 
        }
			};

		private:
			const Shapes::IShape* m_pCurrentShape;   
			DbfInput              m_dbfIn;
			int                   m_deletionCounter;

			bool GetDbfParameters( IMainCommands* pCmdIfc );
			void DeleteObjects( IMainCommands* pCmdIfc );
			void WriteReport();
		};
	}
}

//-----------------------------------------------------------------------------
