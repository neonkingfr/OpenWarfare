#pragma once

#include "ILandBuildCmds.h"

namespace LandBuildInt
{
	class ModuleException : public ExceptReg::IException
	{
		unsigned int code;
		const char* text;    

	public:
		ModuleException(unsigned int code, const char* text) 
		: code(code)
		, text(text) 
		{
		}

		// -------------------------------------------------------------------------- //

		virtual unsigned int GetCode() const 
		{ 
			return code; 
		}

		// -------------------------------------------------------------------------- //

		virtual const _TCHAR* GetDesc() const 
		{ 
			return text; 
		}

		// -------------------------------------------------------------------------- //

		virtual const _TCHAR* GetModule() const 
		{ 
			return "Module"; 
		}

		// -------------------------------------------------------------------------- //

		virtual const _TCHAR* GetType() const 
		{ 
			return "ModuleException"; 
		}
	};

	// -------------------------------------------------------------------------- //

#define DECLAREMODULEEXCEPTION(ModuleName) class Exception: public ModuleException {\
public:\
  Exception(unsigned int code, const char* text) : ModuleException(code,text) {}\
  virtual const _TCHAR* GetModule() const { return ModuleName; }\
  virtual const _TCHAR* GetModuleType() const { return ModuleName "Exception"; }\
};

	// -------------------------------------------------------------------------- //

	class IBuildModule
	{
	public:
        ///called on begin of pass during multipass processing        
        virtual void OnBeginPass(IMainCommands* cmdIfc) 
		{
		}

		// -------------------------------------------------------------------------- //
        ///called on begin of group
        virtual void OnBeginGroup(IMainCommands* cmdIfc) 
		{
		}

		// -------------------------------------------------------------------------- //
        ///called on end of group
        virtual void OnEndGroup(IMainCommands* cmdIfc) 
		{
		}

		// -------------------------------------------------------------------------- //
        ///called on end of pass        
        virtual void OnEndPass(IMainCommands* cmdIfc) 
		{
		}

		// -------------------------------------------------------------------------- //
        ///called for every shape
		virtual void Run(IMainCommands* cmdIfc) = 0;

		// -------------------------------------------------------------------------- //
		///called for destruction
        virtual void Release() 
		{
			delete this;
		}
          
		// -------------------------------------------------------------------------- //
        virtual ~IBuildModule() 
		{
		}
	};
}
