#include "StdAfx.h"
#include ".\OnPlacePlacer.h"

#include "..\HighMapLoaders\include\EtMath.h"
#include "..\HighMapLoaders\include\EtHelperFunctions.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{
		// -------------------------------------------------------------------------- //

		void OnPlacePlacer::Run( IMainCommands* pCmdIfc )
		{
			// resets arrays
      if ( m_objectsOut.Size() != 0 ) { m_objectsOut.Clear(); }

			// gets shape and its properties
			m_pCurrentShape = pCmdIfc->GetActiveShape();    

			// gets parameters from calling cfg file
			if ( GetDbfParameters( pCmdIfc ) )
			{
				// sets the output objects
				CreateObjects();

				// export objects
				ExportCreatedObjects( pCmdIfc );

				// writes the report
				WriteReport();
			}
		}

		// --------------------------------------------------------------------//

		ModuleObjectOutputArray* OnPlacePlacer::RunAsPreview( const Shapes::IShape* pShape, DbfInput& dbfIn )
		{
			m_pCurrentShape = pShape;
			m_dbfIn         = dbfIn;

			// resets arrays
      if ( m_objectsOut.Size() != 0 ) { m_objectsOut.Clear(); }

			// sets the output objects
			CreateObjects();

			return (&m_objectsOut);
		}

		// --------------------------------------------------------------------//

		bool OnPlacePlacer::GetDbfParameters( IMainCommands* pCmdIfc )
		{
			// gets parameters from calling cfg file
			const char* x = pCmdIfc->QueryValue( "orientation", false );
			if ( x != 0 )
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_dbfIn.orientation = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 11, ">>> Bad data for orientation <<<" ) );
					return false;
				}
			}
			else
			{        
				ExceptReg::RegExcpt( Exception( 12, ">>> Missing orientation value <<<" ) );
				return false;
			}

			x = pCmdIfc->QueryValue( "object", false );
			if ( x != 0 )
			{
				m_dbfIn.name = x;
			}
			else
			{        
				ExceptReg::RegExcpt( Exception( 13, ">>> No object specified <<<" ) );
				return false;
			}

			m_dbfIn.scaleX = 1.0; // default value
			x = pCmdIfc->QueryValue( "scaleX", false );
			if ( x != 0 )
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_dbfIn.scaleX = atof( x );
				}
				else
				{
					printf( ">>> ----------------------------------------------- <<<\n" );
					printf( ">>> Bad data for scaleX - using default value (1.0) <<<\n" );
					printf( ">>> ----------------------------------------------- <<<\n" );
				}
			}
			else
			{        
				printf( ">>> ------------------------------------------------ <<<\n" );
				printf( ">>> Missing scaleX value - using default value (1.0) <<<\n" );
				printf( ">>> ------------------------------------------------ <<<\n" );
			}

			m_dbfIn.scaleY = 1.0; // default value
			x = pCmdIfc->QueryValue( "scaleY", false );
			if ( x != 0 )
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_dbfIn.scaleY = atof( x );
				}
				else
				{
					printf( ">>> ----------------------------------------------- <<<\n" );
					printf( ">>> Bad data for scaleY - using default value (1.0) <<<\n" );
					printf( ">>> ----------------------------------------------- <<<\n" );
				}
			}
			else
			{        
				printf( ">>> ------------------------------------------------ <<<\n" );
				printf( ">>> Missing scaleY value - using default value (1.0) <<<\n" );
				printf( ">>> ------------------------------------------------ <<<\n" );
			}

			m_dbfIn.scaleZ = 1.0; // default value
			x = pCmdIfc->QueryValue( "scaleZ", false );
			if ( x != 0 )
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_dbfIn.scaleZ = atof( x );
				}
				else
				{
					printf( ">>> ----------------------------------------------- <<<\n" );
					printf( ">>> Bad data for scaleZ - using default value (1.0) <<<\n" );
					printf( ">>> ----------------------------------------------- <<<\n" );
				}
			}
			else
			{        
				printf( ">>> ------------------------------------------------ <<<\n" );
				printf( ">>> Missing scaleZ value - using default value (1.0) <<<\n" );
				printf( ">>> ------------------------------------------------ <<<\n" );
			}

			return (true);
		}

		// --------------------------------------------------------------------//

		void OnPlacePlacer::CreateObjects()
		{
			int vCount = m_pCurrentShape->GetVertexCount();
			for ( int i = 0; i < vCount; ++i )
			{
				ModuleObjectOutput out;

				// sets object out data
				out.position = m_pCurrentShape->GetVertex( i );
        // WARNING: the minus sign is an hack
				out.rotation = -EtMathD::DegToRad( m_dbfIn.orientation );
				out.scaleX   = m_dbfIn.scaleX;
				out.scaleY   = m_dbfIn.scaleY;
				out.scaleZ   = m_dbfIn.scaleZ;

				// used only by VLB for preview
				out.length = 10.0;
				out.width  = 10.0;
				out.height = 10.0;

				// adds to array
				m_objectsOut.Add( out );
			}
		}

		// --------------------------------------------------------------------//

		void OnPlacePlacer::ExportCreatedObjects( IMainCommands* pCmdIfc )
		{
			int objOutCount = m_objectsOut.Size();
			for ( int i = 0; i < objOutCount; ++i )
			{
				RString name      = m_dbfIn.name;
				Vector3 position  = Vector3( static_cast< Coord >( m_objectsOut[i].position.x ), 0, static_cast< Coord >( m_objectsOut[i].position.y ) );

				Matrix4 mTranslation = Matrix4( MTranslation, position );
				Matrix4 mRotation    = Matrix4( MRotationY, static_cast< Coord >( m_objectsOut[i].rotation ) );
				Matrix4 mScaling     = Matrix4( MScale, static_cast< Coord >( m_objectsOut[i].scaleX ), static_cast< Coord >( m_objectsOut[i].scaleZ ), static_cast< Coord >( m_objectsOut[i].scaleY ) );

				Matrix4 transform = mTranslation * mRotation * mScaling;
				pCmdIfc->GetObjectMap()->PlaceObject( transform, name, 0 );
			}
		}

		// --------------------------------------------------------------------//

		void OnPlacePlacer::WriteReport()
		{
			printf( "Created                    \n" );
			printf( "%d", m_objectsOut.Size() );
			printf( " : " );
			printf( m_dbfIn.name );
			printf( "                           \n" );
			printf( "--------------------------------------\n" );
		}
	}
}
