#pragma once

// -------------------------------------------------------------------------- //

#include ".\IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>
#include "..\Shapes\ShapeHelpers.h"

// -------------------------------------------------------------------------- //

namespace LandBuildInt
{
	namespace Modules
	{
		class RandomOnPathPlacer : public IBuildModule
		{
		public:
			struct GlobalInput
			{
				double       maxDistance;
				unsigned int randomSeed;
			};

			struct DbfInput
			{
				double linearDensity;
			};

			struct ObjectInput
			{
				RString m_Name;
				double  m_MinHeight;
				double  m_MaxHeight;
				double  m_Prob;
				double  m_NormProb;
				int     m_Counter;

				ObjectInput() {}
				ObjectInput(RString name, double minHeight, double maxHeight, double prob)
				{
					m_Name      = name;
					m_MinHeight = minHeight;
					m_MaxHeight = maxHeight;
					m_Prob      = prob;
					m_Counter   = 0;
				}

				ClassIsMovable(ObjectInput); 
			};

		private:
			struct ShapeGeo
			{
				double perimeter;
			};

		public:
			virtual void Run(IMainCommands* cmdIfc);
			ModuleObjectOutputArray* RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn,
												  AutoArray<RandomOnPathPlacer::ObjectInput, MemAllocStack<RandomOnPathPlacer::ObjectInput, 32> >& objectsIn);

			class Exception: public ExceptReg::IException
			{
				unsigned int code;
				const char*  text;

			public:
				Exception(unsigned int code, const char* text) 
				: code(code)
				, text(text) 
				{
				}

				virtual unsigned int GetCode() const 
				{ 
					return code; 
				}

				virtual const _TCHAR* GetDesc() const 
				{ 
					return text; 
				}

				virtual const _TCHAR* GetModule() const 
				{ 
					return "RandomOnPathPlacer"; 
				}

				virtual const _TCHAR* GetType() const 
				{ 
					return "RandomOnPathPlacerException"; 
				}
			};

		private:
			AutoArray<ObjectInput, MemAllocStack<ObjectInput, 32> > m_ObjectsIn;
			ModuleObjectOutputArray                                 m_ObjectsOut;

			GlobalInput           m_GlobalIn;
			DbfInput              m_DbfIn;
			const Shapes::IShape* m_pCurrentShape;      
			ShapeGeo              m_ShpGeo;
			int                   m_NumberOfIndividuals;

			bool GetShapeGeoData();
			bool GetGlobalParameters(IMainCommands* cmdIfc);
			bool GetDbfParameters(IMainCommands* cmdIfc);
			bool GetObjectsParameters(IMainCommands* cmdIfc);

			bool NormalizeProbs();
			int  GetRandomType();
			void CreateObjects(bool preview);
			void ExportCreatedObjects(IMainCommands* cmdIfc);
			void WriteReport();

			Shapes::DVertex ConvertToXY(double s);
		};
	}
}