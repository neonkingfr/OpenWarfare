#pragma once

#include "IBuildModule.h"
#include <el/MultiThread/ExceptionRegister.h>

namespace LandBuildInt
{
	namespace Modules
	{
		struct MyPoint;
		class RoadPlacer: public IBuildModule
		{
		public:
			DECLAREMODULEEXCEPTION("RoadPlacer");

			RoadPlacer(void);
			virtual ~RoadPlacer(void);

			virtual void Run(IMainCommands*);

		protected:
			void RoadPlacer::PlaceRoad(IMainCommands* land,
									   const Array<MyPoint>& points, 
									   int cluster, 
									   const char* partNameBase, 
									   double width, 
									   const Array<float>& straight,
									   double straightTolerance,
									   const Array<float>& alternative);
		};
	}
}
