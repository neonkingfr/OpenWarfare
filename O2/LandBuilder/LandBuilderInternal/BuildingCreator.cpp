#include "StdAfx.h"
#include <string>

#include ".\BuildingCreator.h"

#include "..\Shapes\IShape.h"
#include ".\IShapeExtra.h"
#include "..\HighMapLoaders\include\EtHelperFunctions.h"

// --------------------------------------------------------------------------------

using std::string;

// --------------------------------------------------------------------------------

static const bool EXPORT_DXF = true;

// --------------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{
		BuildingCreator::BuildingCreator()
		: m_FirstRun(true)
		, m_ShapeCounter(0)
		{
		}

		// --------------------------------------------------------------------------------

		BuildingCreator::~BuildingCreator()
		{
		}

		// --------------------------------------------------------------------------------

		void BuildingCreator::Run(IMainCommands* cmdIfc)
		{
			// gets shape and its properties
			m_pCurrentShape = const_cast<Shapes::IShape*>(cmdIfc->GetActiveShape());

			m_ShapeCounter++;
			printf("Working on shape: %d\n", m_ShapeCounter);

			// checks for multipart polygons
			if (m_pCurrentShape->GetPartCount() > 1)
			{
				ExceptReg::RegExcpt(Exception(11, ">>> BuildingCreator module accepts only shapefiles containing non-multipart polygons <<<"));
				return;
			}

			// checks for known shapes
			const IShapeExtra* extra = m_pCurrentShape->GetInterface<IShapeExtra>();
			if (extra == 0) 
			{
				ExceptReg::RegExcpt(Exception(12, ">>> Found unknown shape in shapefile. Cannot get type of that shape <<<"));
				return;
			}

			// checks for polylines only
			RString shapeType = extra->GetShapeTypeName();
			if (shapeType != RString(SHAPE_NAME_POLYGON)) 
			{
				ExceptReg::RegExcpt(Exception(11, ">>> BuildingCreator module accepts only shapefiles containing non-multipart polygons <<<"));
				return;
			}

			// here we get the global data
			if (m_FirstRun)
			{
				// gets parameters from calling cfg file
				if(!GetGlobalParameters(cmdIfc))
				{
					return;
				}
				Pathname::CreateFolder(m_GlobalIn.texturesPath);

				RString dxfFileName = m_GlobalIn.texturesPath + "Buildings.dxf";
				m_DxfWriter.Set(dxfFileName.Data());

				// initializes random seed
				srand(m_GlobalIn.randomSeed);

				m_FirstRun = false;
			}

			// skips polylgons with less than 3 vertices
			if (m_pCurrentShape->GetVertexCount() > 2)
			{
				if(!GetDbfParameters(cmdIfc))
				{
					return;
				}

				// here vertexTol is in degrees
				vector<Shapes::DVertex> fpVertices;
				for (unsigned int i = 0, cnt = m_pCurrentShape->GetVertexCount(); i < cnt; ++i)
				{
					fpVertices.push_back(m_pCurrentShape->GetVertex(i));
				}

				Footprint footprint(fpVertices, m_GlobalIn.vertexTol, m_GlobalIn.rectCornerTol, m_DbfIn.rectify);

				vector<Texture> textures;
				if (footprint.GetVerticesCount() > 2)
				{
					if (!(m_DbfIn.texFile == "none"))
					{
						Texture texture(m_GlobalIn.texturesPath, m_DbfIn.texFile, 0);
						textures.push_back(texture);
					}
					m_Building = Building(footprint, m_DbfIn.numOfFloors, m_DbfIn.floorHeight, m_DbfIn.roofType);
					m_Building.GenerateP3DModels(cmdIfc, m_GlobalIn.modelsPath, m_GlobalIn.texturesPath, textures);
				}
			}

			if (EXPORT_DXF)
			{
				ExportDxf();
			}
		}

		// --------------------------------------------------------------------------------

		bool BuildingCreator::GetGlobalParameters(IMainCommands* cmdIfc)
		{
			// gets parameters from calling cfg file
			const char* x = cmdIfc->QueryValue("modelsPath", false);
			if (x != 0) 
			{
				m_GlobalIn.modelsPath = x;
			}
			else
			{
				ExceptReg::RegExcpt(Exception(21, ">>> Missing modelsPath value <<<"));
				return false;
			}

			if (m_GlobalIn.modelsPath[m_GlobalIn.modelsPath.GetLength() - 1] != '\\')
			{
				m_GlobalIn.modelsPath = m_GlobalIn.modelsPath + "\\";
			}

			// TODO: add check for valid path

			m_GlobalIn.texturesPath = m_GlobalIn.modelsPath + "Data\\";
/*
			x = cmdIfc->QueryValue("matchTol", false);
			if (x != 0) 
			{
				if (IsFloatingPoint(string(x), false))
				{
					m_GlobalIn.matchTol = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(22, ">>> Bad data for matchTol <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(23, ">>> Missing matchTol value <<<"));
				return false;
			}
*/
/*
			x = cmdIfc->QueryValue("vertexTol", false);
			if (x != 0) 
			{
				if (IsFloatingPoint(string(x), false))
				{
					m_GlobalIn.vertexTol = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(24, ">>> Bad data for vertexTol <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(25, ">>> Bad data for vertexTol <<<"));
				return false;
			}
*/
			m_GlobalIn.vertexTol = 10; // using 5 degrees

/*
			x = cmdIfc->QueryValue("rectCornerTol", false);
			if (x != 0) 
			{
				if (IsFloatingPoint(string(x), false))
				{
					m_GlobalIn.rectCornerTol = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(26, ">>> Bad data for rectCornerTol <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(27, ">>> Bad data for rectCornerTol <<<"));
				return false;
			}
*/
			m_GlobalIn.rectCornerTol = 10; // using 10 degrees

			x = cmdIfc->QueryValue("randomSeed", false);
			if (x != 0) 
			{
				if (IsInteger(string(x), false))
				{
					m_GlobalIn.randomSeed = static_cast<unsigned int>(atoi(x));
				}
				else
				{
					ExceptReg::RegExcpt(Exception(28, ">>> Bad data for randomSeed <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(29, ">>> Missing randomSeed value <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool BuildingCreator::GetDbfParameters(IMainCommands* cmdIfc)
		{
			// gets number of floors
			const char* x = cmdIfc->QueryValue("numOfFloor", false);
			if (x != 0) 
			{
				if(IsInteger(string(x), false))
				{
					m_DbfIn.numOfFloors = atoi(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(31, ">>> Bad data for numOfFloor <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(32, ">>> Missing numOfFloor value <<<"));
				return false;
			}

			// gets floors' height
			x = cmdIfc->QueryValue("floorHeight", false);
			if (x != 0) 
			{
				if(IsFloatingPoint(string(x), false))
				{
					m_DbfIn.floorHeight = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(33, ">>> Bad data for floorHeight <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(34, ">>> Mising floorHeight value <<<"));
				return false;
			}

			// gets roof type
			x = cmdIfc->QueryValue("roofType", false);
			if (x != 0) 
			{
				if(IsInteger(string(x), false))
				{
					m_DbfIn.roofType = atoi(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(35, ">>> Bad data for roofType <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(36, ">>> Missing roofType value <<<"));
				return false;
			}

			if (m_DbfIn.roofType > 1)
			{
				m_DbfIn.roofType = 0;
				printf(">>> ------------------------------------------------------- <<<\n");
				printf(">>> roofType non supported - using default value (0 - None) <<<\n");
				printf(">>> ------------------------------------------------------- <<<\n");
			}

			// gets rectify
			x = cmdIfc->QueryValue("rectify", false);
			if (x != 0) 
			{
				if(IsBool(string(x), false))
				{
					int i = static_cast<int>(atoi(x));
					if (i == 0)
					{
						m_DbfIn.rectify = false;
					}
					else
					{
						m_DbfIn.rectify = true;
					}
				}
				else
				{
					ExceptReg::RegExcpt(Exception(37, ">>> Bad data for rectify <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(38, ">>> Missing rectify value <<<"));
				return false;
			}

			// gets texture file
			x = cmdIfc->QueryValue("texFile", false);
			if (x != 0) 
			{
				m_DbfIn.texFile = x;

				m_DbfIn.texFile.Lower();
				int pos = m_DbfIn.texFile.GetLength() - 3;
				
				if (pos < 0)
				{
					m_DbfIn.texFile = "none";
				}

				RString endName = m_DbfIn.texFile.Substring(pos, m_DbfIn.texFile.GetLength());
				if (endName == "_co")
				{
					// this is because of problems with operator !=
				}
				else
				{
					m_DbfIn.texFile = "none";
				}
			}
			else
			{
				m_DbfIn.texFile = "none";
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		void BuildingCreator::ExportDxf()
		{
			if (m_GlobalIn.modelsPath.GetLength() == 0) return;

			Footprint fp = m_Building.GetFootprint();
			m_DxfWriter.WritePolyline(fp.GetVertices(), "1", true);
			const BuildingModelP3D& modelP3D = m_Building.GetModelP3D();
			const vector<CG_Polygon2>& partitions = modelP3D.GetPartitions();
			Shapes::DVertex pos = modelP3D.GetPosition();
			double angle = modelP3D.GetOrientation();
			for (size_t i = 0, cntI = partitions.size(); i < cntI; ++i)
			{
				vector<Shapes::DVertex> polygon;
				const CG_Polygon2& part = partitions[i];
				for (size_t j = 0, cntJ = part.VerticesCount(); j < cntJ; ++j)
				{
					const CG_Polygon2Vertex& ver = part.GetVertex(j);
					double x = pos.x + ver.X() * cos(angle) - ver.Y() * sin(angle);
					double y = pos.y + ver.X() * sin(angle) + ver.Y() * cos(angle);
					polygon.push_back(Shapes::DVertex(x, y));
				}
				m_DxfWriter.WritePolyline(polygon, "tri", true);
			}
		}

		// --------------------------------------------------------------------------------
	}
}