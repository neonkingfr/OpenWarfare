#include "StdAfx.h"
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include ".\scriptmodule.h"
#include <el/Evaluator/Addons/ScriptLoader.h>
#include <el/Evaluator/Addons/Dialogs/dialogs.h>
#include <el/Evaluator/Addons/StringsArrays/StringsArrays.h>
#include <el/Evaluator/Addons/Matrix/gdMatrix.h>
#include <el/Evaluator/Addons/Streams/gdIOStream.h>
#include <el/Evaluator/Addons/PreprocCmd/PreprocCmd.h>
#include <el/Evaluator/Addons/SourceSafe/gdSourceSafe.h>
//#include <projects/ObjektivLib/UIDebugger/DllDebuggingSession.h>
#include "gdLandBuild.h"

/*static DllDebuggerProvider GDebugProvider;
SRef<DllDebuggingSession> GDebugSession;*/

static void RegisterToGameState(GameState *gs)
{
  GdStringsArrays::RegisterToGameState(gs);
  GdMatrix::RegisterToGameState(gs);
  GdDialogs::RegisterToGameState(gs);
//TODO: Why  GdIOStreamV?  GdIOStreamV::RegisterToGameState(gs); 
//TODO: Why  GdIOStream?  GdIOStream::RegisterToGameState(gs);
  GdSourceSafe::RegisterToGameState(gs);
  GdPreprocCmd::RegisterToGameState(gs);
  GdLandBuild::RegisterToGameState(gs);
}


namespace LandBuildInt
{
  namespace Modules
  {

    bool  ScriptModule::LoadScript(const char *script)
    {

      Pathname exePath=Pathname::GetExePath();
      Pathname curDir=Pathname::GetCWD();

      ScriptLoaderClass scriptProcess;
      RString libList;
      sprintf(libList.CreateBuffer(strlen(exePath)+strlen(curDir)+2),"%s;%s",curDir.GetDirectoryWithDriveWLBS(),exePath.GetDirectoryWithDriveWLBS());
      scriptProcess.SetLibraryPaths(libList);

      bool res=scriptProcess.LoadScript(script);
      if (res==false)
      {
        ExceptReg::RegExcpt(ErrorRep(scriptProcess.GetPreprocError(),scriptProcess.GetPreprocErrorCode()));
        return false;
      }


      RegisterToGameState(this);

/*      if (GDebugSession.IsNull())
      {
        Pathname dllSearch=Pathname::GetExePath();
        dllSearch.SetFilename("UIDebugger.DLL");
        if (dllSearch.BottomTopSearchExist())
        {
          GDebugProvider.CreateProvider(dllSearch);
          GDebugSession=new DllDebuggingSession(GDebugProvider);
        }
      }

      if (GDebugSession.NotNull()) AttachDebugger(GDebugSession->operator ScriptDebuggerBase*());
*/
/*#if DOCUMENT_COMREF
      if (!gState.ComRefDocument("P:\\doc\\Objektiv2\\Manuals\\ComRef\\data\\comref.xml"))
        puts("Unable to generate COMREF");
      if (!gState.GenerateUltraeditWordFile("p:\\doc\\WORDFILE.TXT","BIS O2Script","BIO2S INC",true))
        puts("Unable to generate WORDFILE");
      exit(0);

#endif*/
    
      _code=scriptProcess.GetScript();

      return true;
    }

    void ScriptModule::Run(IMainCommands *cmdIfc)
    {
      ProgressBar<float> pb(1);
      _progress=&pb;
      GameVarSpace spc(GetContext());
      BeginContext(&spc);
      _cmdIfc=cmdIfc;
      EvaluateMultiple(_code);
      EndContext();
      _progress=0;
    }
  }
}

#if DOCUMENT_COMREF

void main()
{
  GameState gs;
  RegisterToGameState(&gs);

  if (!gs.ComRefDocument("P:\\doc\\LandBuilder\\ComRef\\data\\comref.xml"))
    puts("Unable to generate COMREF");
/*  if (!gState.GenerateUltraeditWordFile("p:\\doc\\WORDFILE.TXT","BIS O2Script","BIO2S INC",true))
    puts("Unable to generate WORDFILE");*/


}





#endif