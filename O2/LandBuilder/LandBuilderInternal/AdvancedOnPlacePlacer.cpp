#include "StdAfx.h"
#include ".\AdvancedOnPlacePlacer.h"

#include "..\HighMapLoaders\include\EtMath.h"
#include "..\HighMapLoaders\include\EtHelperFunctions.h"

namespace LandBuildInt
{
	namespace Modules
	{
		// --------------------------------------------------------------------//

		void AdvancedOnPlacePlacer::Run(IMainCommands* cmdIfc)
		{
			// resets arrays
			if (m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();

			// gets shape and its properties
			m_pCurrentShape = cmdIfc->GetActiveShape();    

			// gets parameters from calling cfg file
			if (GetDbfParameters(cmdIfc))
			{
				SetGeometry();

				// sets the output objects
				CreateObjects();

				// export objects
				ExportCreatedObjects(cmdIfc);

				// writes the report
				WriteReport();
			}
		}

		// --------------------------------------------------------------------//

		ModuleObjectOutputArray* AdvancedOnPlacePlacer::RunAsPreview(const Shapes::IShape* shape, DbfInput& dbfIn)
		{
			m_pCurrentShape = shape;
			m_DbfIn         = dbfIn;

			// resets arrays
			if (m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();

			SetGeometry();

			// sets the output objects
			CreateObjects();

			return &m_ObjectsOut;
		}

		// --------------------------------------------------------------------//

		bool AdvancedOnPlacePlacer::GetDbfParameters(IMainCommands* cmdIfc)
		{
			// gets parameters from calling cfg file
			const char* x = cmdIfc->QueryValue("object", false);
			if (x != 0)
			{
				m_DbfIn.name = x;
			}
			else
			{        
				ExceptReg::RegExcpt(Exception(11, ">>> No object specified <<<"));
				return false;
			}

			m_DbfIn.dirCorrection = 0.0; // default value
			x = cmdIfc->QueryValue("dirCorrection", false);
			if (x != 0) 
			{
				if (IsFloatingPoint(string(x), false))
				{
					m_DbfIn.dirCorrection = atof(x);
				}
				else
				{
					printf(">>> ------------------------------------------------------ <<<\n");
					printf(">>> Bad data for dirCorrection - using default value (0.0) <<<\n");
					printf(">>> ------------------------------------------------------ <<<\n");
				}
			}
			else
			{
				printf(">>> ------------------------------------------------------- <<<\n");
				printf(">>> Missing dirCorrection value - using default value (0.0) <<<\n");
				printf(">>> ------------------------------------------------------- <<<\n");
			}

			return true;
		}

		// --------------------------------------------------------------------//

		void AdvancedOnPlacePlacer::SetGeometry()
		{
			Shapes::DVertex barycenter = GetShapeBarycenter(m_pCurrentShape);

			int vCount = m_pCurrentShape->GetVertexCount();
			
			// inertia moment about y axis
			double Iyy = 0.0;
			// inertia moment about x axis
			double Ixx = 0.0;
			// mixed inertia moment 
			double Ixy = 0.0;
			for (int i = 0; i < vCount; ++i)
			{
				double dx = m_pCurrentShape->GetVertex(i).x - barycenter.x;
				double dy = m_pCurrentShape->GetVertex(i).y - barycenter.y;
				Iyy += (dx * dx);
				Ixx += (dy * dy);
				Ixy += (dx * dy);
			}

			if (Ixx == Iyy)
			{
				m_Orientation = 0.0;
			}
			else
			{
				double t = -Ixy / (Ixx - Iyy);
				double twoAlpha = EtMathD::Atan(t);
				m_Orientation = twoAlpha * 0.5;
			}
		}

		// --------------------------------------------------------------------//

		void AdvancedOnPlacePlacer::CreateObjects()
		{
			ModuleObjectOutput out;

			// sets object out data
			out.position = GetShapeBarycenter(m_pCurrentShape);
			out.rotation = m_Orientation + EtMathD::DegToRad(m_DbfIn.dirCorrection);
			out.scaleX = out.scaleY = out.scaleZ = 1.0;

			// used only by VLB for preview
			out.length = 10.0;
			out.width  = 10.0;
			out.height = 10.0;

			// adds to array
			m_ObjectsOut.Add(out);
		}

		// --------------------------------------------------------------------//

		void AdvancedOnPlacePlacer::ExportCreatedObjects(IMainCommands *cmdIfc)
		{
			int objOutCount = m_ObjectsOut.Size();
			for (int i = 0; i < objOutCount; ++i)
			{
				RString name      = m_DbfIn.name;
				Vector3 position  = Vector3(static_cast<Coord>(m_ObjectsOut[i].position.x), 0, static_cast<Coord>(m_ObjectsOut[i].position.y));

				Matrix4 mTranslation = Matrix4(MTranslation, position);
				Matrix4 mRotation    = Matrix4(MRotationY, static_cast<Coord>(m_ObjectsOut[i].rotation));

				Matrix4 transform = mTranslation * mRotation;
				cmdIfc->GetObjectMap()->PlaceObject(transform, name, 0);
			}
		}

		// --------------------------------------------------------------------//

		void AdvancedOnPlacePlacer::WriteReport()
		{
			int objOutCount = m_ObjectsOut.Size();
			printf("Created %d new objects\n", objOutCount);
			printf("--------------------------------------\n");
		}
	}
}