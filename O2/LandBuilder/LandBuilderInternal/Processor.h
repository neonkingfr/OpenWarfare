#pragma once

#include "Core.h"
#include <el/ParamFile/paramFile.hpp>

namespace LandBuildInt
{
	class Processor
	{
		ParamFile _config;
		Core _core;
	protected:
		FloatRect LoadRectangle(ParamClassPtr entry);
		void LoadTranslationTable(ParamEntryPtr entry, int type);

	public:
		Processor(IModuleFactory* modFactory);
		~Processor(void);

		bool LoadProject(const char* configName);

		void LoadTask(const Pathname& configName, const Pathname& shapeName, const Pathname& dbfName = Pathname(PathNull));

		bool SaveResult(const char* resultName) 
		{
			return _core.SaveResult(resultName);
		}

		bool ProcessProject()
		{
			_core.Run();
			return true;
		}

		void SetShapeFactory(Shapes::IShapeFactory* factory)
		{
			_core.SetShapeFactory(factory);
		}
	};
}