#include "StdAfx.h"
#include ".\FramedForestPlacer.h"

#include "..\HighMapLoaders\include\EtMath.h"
#include "..\HighMapLoaders\include\EtHelperFunctions.h"

namespace LandBuildInt
{
	namespace Modules
	{
		// --------------------------------------------------------------------------------

		void FramedForestPlacer::Run(IMainCommands* cmdIfc)
		{
			printf("Started a new shape                   \n");
			printf("--------------------------------------\n");

			// gets shape and its properties
			m_pCurrentShape = const_cast<Shapes::IShape*>(cmdIfc->GetActiveShape());      

			// gets parameters from calling cfg file
			if (GetGlobalParameters(cmdIfc))
			{
				// gets dbf parameters from calling cfg file
				if (GetDbfParameters(cmdIfc))
				{
					// gets objects parameters from calling cfg file
					if (GetObjectsParameters(cmdIfc))
					{
						// normalizes probabilities for inner objects
						if (NormalizeProbs(true))
						{
							// normalizes probabilities for outer objects
							if (NormalizeProbs(false))
							{
								// initializes random seed
								srand(m_GlobalIn.randomSeed);

								// sets the population
								CreateObjects(false);

								// export objects
								ExportCreatedObjects(cmdIfc);

								// writes the report
								WriteReport();
							}
						}													
					}
				}
			}       
		}

		// --------------------------------------------------------------------//

		ModuleObjectOutputArray* FramedForestPlacer::RunAsPreview(const Shapes::IShape* shape, GlobalInput& globalIn, DbfInput& dbfIn, 
																  AutoArray<FramedForestPlacer::ObjectInput, MemAllocStack<FramedForestPlacer::ObjectInput, 32> >& objectsIn)
		{
			m_pCurrentShape = const_cast<Shapes::IShape*>(shape);
			m_GlobalIn      = globalIn;
			m_DbfIn         = dbfIn;
			m_ObjectsIn     = objectsIn;

			if (m_ObjectsOut.Size() != 0) m_ObjectsOut.Clear();
			m_InnerCounter = 0;
			m_OuterCounter = 0;

			unsigned int objInCount = m_ObjectsIn.Size();
			for (unsigned int i = 0; i < objInCount; ++i)
			{
				if (m_ObjectsIn[i].m_Inner)
				{
					m_InnerCounter++;
				}
				else
				{
					m_OuterCounter++;
				}
			}

			if (m_InnerCounter == 0 && m_OuterCounter == 0)
			{
				return false;
			}

			// normalizes probabilities for inner objects
			if (NormalizeProbs(true))
			{
				// normalizes probabilities for outer objects
				if (NormalizeProbs(false))
				{
					// initializes random seed
					srand(m_GlobalIn.randomSeed);

					// sets the population
					CreateObjects(true);

					return &m_ObjectsOut;
				}
				else
				{
					return NULL;
				}
			}
			else
			{
				return NULL;
			}
		}

		// --------------------------------------------------------------------------------

		bool FramedForestPlacer::GetGlobalParameters(IMainCommands* cmdIfc)
		{
			const char* x = cmdIfc->QueryValue("randomSeed", false);
			if (x != 0) 
			{
				if (IsInteger(string(x), false))
				{
					m_GlobalIn.randomSeed = static_cast<unsigned int>(atoi(x));
				}
				else
				{
					ExceptReg::RegExcpt(Exception(11, ">>> Bad data for randomSeed <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(12, ">>> Missing randomSeed value <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool FramedForestPlacer::GetDbfParameters(IMainCommands* cmdIfc)
		{
			const char* x = cmdIfc->QueryValue("mainDirection", false);
			if (x != 0) 
			{
				if (IsFloatingPoint(string(x), false))
				{
					m_DbfIn.mainDirection = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt(Exception(21, ">>> Bad data for mainDirection <<<"));
					return false;
				}
			}
			else
			{
				ExceptReg::RegExcpt(Exception(22, ">>> Missing mainDirection value <<<"));
				return false;
			}

			m_DbfIn.subSquaresSize = 10.0; // default value
			x = cmdIfc->QueryValue("subSquaresSize", false);
			if (x != 0) 
			{
				if (IsFloatingPoint(string(x), false))
				{
					m_DbfIn.subSquaresSize = atof(x);
				}
				else
				{
					printf(">>> -------------------------------------------------------- <<<\n");
					printf(">>> Bad data for subSquaresSize - using default value (10.0) <<<\n");
					printf(">>> -------------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> --------------------------------------------------------- <<<\n");
				printf(">>> Missing subSquaresSize value - using default value (10.0) <<<\n");
				printf(">>> --------------------------------------------------------- <<<\n");
			}
			if (m_DbfIn.subSquaresSize < 1.0)
			{
				ExceptReg::RegExcpt(Exception(21, ">>> subSquaresSize is too low (less then 1.0) <<<"));
				return false;
			}

			m_DbfIn.maxNoise = 5.0; // default value
			x = cmdIfc->QueryValue("maxNoise", false);
			if (x != 0) 
			{
				if (IsFloatingPoint(string(x), false))
				{
					m_DbfIn.maxNoise = atof(x);
				}
				else
				{
					printf(">>> ------------------------------------------------- <<<\n");
					printf(">>> Bad data for maxNoise - using default value (5.0) <<<\n");
					printf(">>> ------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> -------------------------------------------------- <<<\n");
				printf(">>> Missing maxNoise value - using default value (5.0) <<<\n");
				printf(">>> -------------------------------------------------- <<<\n");
			}

			m_DbfIn.minDistance = 0.0; // default value
			x = cmdIfc->QueryValue("mindist", false);
			if (x != 0) 
			{
				if (IsFloatingPoint(string(x), false))
				{
					m_DbfIn.minDistance = atof(x);
				}
				else
				{
					printf(">>> ------------------------------------------------ <<<\n");
					printf(">>> Bad data for mindist - using default value (0.0) <<<\n");
					printf(">>> ------------------------------------------------ <<<\n");
				}
			}
			else
			{
				printf(">>> ------------------------------------------------- <<<\n");
				printf(">>> Missing mindist value - using default value (0.0) <<<\n");
				printf(">>> ------------------------------------------------- <<<\n");
			}

			m_DbfIn.numBoundaryObjects = 1; // default value
			x = cmdIfc->QueryValue("numBoundaryObjects", false);
			if (x != 0) 
			{
				if (IsInteger(string(x), false))
				{
					m_DbfIn.numBoundaryObjects = atoi(x);
				}
				else
				{
					printf(">>> --------------------------------------------------------- <<<\n");
					printf(">>> Bad data for numBoundaryObjects - using default value (1) <<<\n");
					printf(">>> --------------------------------------------------------- <<<\n");
				}
			}
			else
			{
				printf(">>> ---------------------------------------------------------- <<<\n");
				printf(">>> Missing numBoundaryObjects value - using default value (1) <<<\n");
				printf(">>> ---------------------------------------------------------- <<<\n");
			}

			return true;
		}

		// --------------------------------------------------------------------------------

		bool FramedForestPlacer::GetObjectsParameters(IMainCommands* cmdIfc)
		{
			// resets arrays
			if (m_ObjectsIn.Size() != 0)
			{
				m_ObjectsIn.Clear();
				m_ObjectsOut.Clear();
			}

			m_InnerCounter = 0;
			m_OuterCounter = 0;

			int counter = 1;
			do 
			{
				char object[50], minheight[50], maxheight[50], prob[50], inner[50];
				sprintf_s(object,    "object%d",    counter);
				sprintf_s(minheight, "minheight%d", counter);
				sprintf_s(maxheight, "maxheight%d", counter);
				sprintf_s(prob,      "prob%d",      counter);
				sprintf_s(inner,     "inner%d",     counter);

				ObjectInput nfo;

				// object type
				const char* x = cmdIfc->QueryValue(object, false);
				if (x == 0) break;
				nfo.m_Name = x;
        
				// object min height
				nfo.m_MinHeight = 100.0; // default value
				x = cmdIfc->QueryValue(minheight, false);
				if (x != 0)
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_MinHeight = atof(x);
					}
					else
					{
						printf(">>> ---------------------------------------------------- <<<\n");
						printf(">>> Bad data for minheight - using default value (100.0) <<<\n");
						printf(">>> ---------------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> ----------------------------------------------------- <<<\n");
					printf(">>> Missing minheight value - using default value (100.0) <<<\n");
					printf(">>> ----------------------------------------------------- <<<\n");
				}

				// object max height
				nfo.m_MaxHeight = 100.0; // default value
				x = cmdIfc->QueryValue(maxheight, false);
				if (x != 0)
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_MaxHeight = atof(x);
					}
					else
					{
						printf(">>> ---------------------------------------------------- <<<\n");
						printf(">>> Bad data for maxheight - using default value (100.0) <<<\n");
						printf(">>> ---------------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> ----------------------------------------------------- <<<\n");
					printf(">>> Missing maxheight value - using default value (100.0) <<<\n");
					printf(">>> ----------------------------------------------------- <<<\n");
				}
        
				// object prob
				nfo.m_Prob = 100.0; // default value
				x = cmdIfc->QueryValue(prob, false);
				if (x != 0)
				{
					if (IsFloatingPoint(string(x), false))
					{
						nfo.m_Prob = atof(x);
					}
					else
					{
						printf(">>> ----------------------------------------------- <<<\n");
						printf(">>> Bad data for prob - using default value (100.0) <<<\n");
						printf(">>> ----------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> ------------------------------------------------ <<<\n");
					printf(">>> Missing prob value - using default value (100.0) <<<\n");
					printf(">>> ------------------------------------------------ <<<\n");
				}

				// object inner
				nfo.m_Inner = true; // default value
				x = cmdIfc->QueryValue(inner, false);
				if (x != 0) 
				{
					if (IsBool(string(x), false))
					{
						int i = static_cast<int>(atoi(x));
						if (i == 0)
						{
							nfo.m_Inner = false;
						}
					}
					else
					{
						printf(">>> -------------------------------------------- <<<\n");
						printf(">>> Bad data for inner - using default value (1) <<<\n");
						printf(">>> -------------------------------------------- <<<\n");
					}
				}
				else
				{
					printf(">>> --------------------------------------------- <<<\n");
					printf(">>> Missing inner value - using default value (1) <<<\n");
					printf(">>> --------------------------------------------- <<<\n");
				}

				nfo.m_Counter = 0;

				if (nfo.m_Inner)
				{
					m_InnerCounter++;
				}
				else
				{
					m_OuterCounter++;
				}

				m_ObjectsIn.Add(nfo);

				counter++;
			} 
			while(true); 

			if (m_InnerCounter == 0 && m_OuterCounter == 0)
			{
				ExceptReg::RegExcpt(Exception(31, ">>> No objects. <<<"));
				return false;
			}

			return true;
		}

		// --------------------------------------------------------------------//

		bool FramedForestPlacer::NormalizeProbs(bool inner)
		{
			double total = 0.0;

			int objInCount = m_ObjectsIn.Size();
			for (int i = 0; i < objInCount; ++i)
			{
				if ((inner && m_ObjectsIn[i].m_Inner) || (!inner && !m_ObjectsIn[i].m_Inner))
				{
					total += m_ObjectsIn[i].m_Prob;
				}
			}

/*
			if(total == 0.0) 
			{
				if (inner)
				{
					ExceptReg::RegExcpt(Exception(41, ">>> Total inner probability equal to zero <<<"));
					return false;
				}
				else
				{
					ExceptReg::RegExcpt(Exception(42, ">>> Total outer probability equal to zero <<<"));
				}
			}
*/

			if(total != 0.0) 
			{
				double invTotal = 1 / total;
				for (int i = 0; i < objInCount; ++i)
				{
					if ((inner && m_ObjectsIn[i].m_Inner) || (!inner && !m_ObjectsIn[i].m_Inner))
					{
						m_ObjectsIn[i].m_NormProb = m_ObjectsIn[i].m_Prob * invTotal;
					}
				}
			}

			return true;
		}

		// --------------------------------------------------------------------//

		double FramedForestPlacer::SquareDistanceFromPolygon(Shapes::DVector vLB, Shapes::DVector vRB, Shapes::DVector vRT, Shapes::DVector vLT)
		{
			// corners' distances from polygon
			Shapes::DVector nLB = NearestToEdge(m_pCurrentShape, vLB);
			double distanceLB = vLB.DistanceXY(nLB);
			Shapes::DVector nRB = NearestToEdge(m_pCurrentShape, vRB);
			double distanceRB = vRB.DistanceXY(nRB);
			Shapes::DVector nRT = NearestToEdge(m_pCurrentShape, vRT);
			double distanceRT = vRT.DistanceXY(nRT);
			Shapes::DVector nLT = NearestToEdge(m_pCurrentShape, vLT);
			double distanceLT = vLT.DistanceXY(nLT);

			// calculates min distance 
			double minDist = distanceLB;
			if (minDist > distanceRB) minDist = distanceRB;
			if (minDist > distanceRT) minDist = distanceRT;
			if (minDist > distanceLT) minDist = distanceLT;

			return minDist;
		}

		// --------------------------------------------------------------------//

		void FramedForestPlacer::CreateObjects(bool preview)
		{
			double angle = m_DbfIn.mainDirection / 180.0 * EtMathD::PI;
			m_pCurrentShape->RotateAboutBarycenter(-angle);

			Shapes::DBox bounding = m_pCurrentShape->CalcBoundingBox();
			ProgressBar<int> pb(toLargeInt((float)((bounding.hi.y - bounding.lo.y) / m_DbfIn.subSquaresSize)));

			for (double y = bounding.lo.y + m_DbfIn.subSquaresSize * 0.5; y < bounding.hi.y; y += m_DbfIn.subSquaresSize) 
			{
				if (!preview) pb.AdvanceNext(1);

				Shapes::IShape* tmp = m_pCurrentShape->Clip(0, 1, -(y - m_DbfIn.subSquaresSize * 0.5));
				if (tmp == 0) continue;
				Shapes::IShape* clp = tmp->Clip(0, -1, (y + m_DbfIn.subSquaresSize * 0.5));
				delete tmp;
				if (clp == 0) continue;

				Shapes::DBox leftright = clp->CalcBoundingBox();

				double nextTest = bounding.lo.x;
				for (double x = (floor(leftright.lo.x / m_DbfIn.subSquaresSize) * m_DbfIn.subSquaresSize) + m_DbfIn.subSquaresSize * 0.5; x < leftright.hi.x; x += m_DbfIn.subSquaresSize)
				{
					// square corners
					double xLB = x - m_DbfIn.subSquaresSize * 0.5;
					double yLB = y - m_DbfIn.subSquaresSize * 0.5;
					Shapes::DVector vLB = Shapes::DVector(xLB, yLB);
					double xRB = x + m_DbfIn.subSquaresSize * 0.5;
					double yRB = y - m_DbfIn.subSquaresSize * 0.5;
					Shapes::DVector vRB = Shapes::DVector(xRB, yRB);
					double xLT = x - m_DbfIn.subSquaresSize * 0.5;
					double yLT = y + m_DbfIn.subSquaresSize * 0.5;
					Shapes::DVector vLT = Shapes::DVector(xLT, yLT);
					double xRT = x + m_DbfIn.subSquaresSize * 0.5;
					double yRT = y + m_DbfIn.subSquaresSize * 0.5;
					Shapes::DVector vRT = Shapes::DVector(xRT, yRT);

					// we can have 2 cases: 
					// - a square completely inside the polygon and with min distance from
					//   the polygon greater than or equal to the given mindistance
					// - all other squares with at least one corner inside the poligon
					if (m_pCurrentShape->PtInside(vLB) && m_pCurrentShape->PtInside(vRB) &&
					    m_pCurrentShape->PtInside(vLT) && m_pCurrentShape->PtInside(vRT) &&
					    SquareDistanceFromPolygon(vLB, vRB, vRT, vLT) >= m_DbfIn.minDistance)
					{
						CreateInnerObjects(x, y, clp);
					}
					else if (m_pCurrentShape->PtInside(vLB) || m_pCurrentShape->PtInside(vRB) ||
							 m_pCurrentShape->PtInside(vLT) || m_pCurrentShape->PtInside(vRT))
					{
						CreateOuterObjects(x, y);
					}
				}
				delete clp;
			}

			m_pCurrentShape->RotateAboutBarycenter(angle);
			RotateCreatedObjects(angle);
		}

		// --------------------------------------------------------------------//

		void FramedForestPlacer::CreateInnerObjects(double x, double y, Shapes::IShape* clp)
		{
			if (m_InnerCounter == 0) return;

			double randDist = RandomInRangeF(0, m_DbfIn.maxNoise);
			double angle    = RandomInRangeF(0, EtMathD::TWO_PI);

			double x0 = x + randDist * EtMathD::Cos(angle);
			double y0 = y + randDist * EtMathD::Sin(angle);
			Shapes::DVector v0 = Shapes::DVector(x0, y0);

			if (clp->PtInside(v0))
			{
				// random type
				int type = GetRandomType(true);

				ModuleObjectOutput out;

				// sets data
				out.position = v0;
				out.type     = type;
				int randOrient = floor(RandomInRangeF(0, 4));
				out.rotation   = randOrient * 0.5 * EtMathD::PI;

				double minScale = m_ObjectsIn[type].m_MinHeight;
				double maxScale = m_ObjectsIn[type].m_MaxHeight;

				out.scaleX = out.scaleY = out.scaleZ = RandomInRangeF(minScale, maxScale) / 100.0;

				// used only by VLB for preview
				out.length = 10.0;
				out.width  = 10.0;
				out.height = 10.0;

				// adds to array
				m_ObjectsOut.Add(out);

				// update type counter
				m_ObjectsIn[type].m_Counter++;
			}
		}

		// --------------------------------------------------------------------//

		void FramedForestPlacer::CreateOuterObjects(double x, double y)
		{
			if (m_OuterCounter == 0) return;

			for (unsigned int i = 0; i < m_DbfIn.numBoundaryObjects; ++i)
			{
				double dX = RandomInRangeF(0, m_DbfIn.subSquaresSize);
				double dY = RandomInRangeF(0, m_DbfIn.subSquaresSize);

				double x0 = x - m_DbfIn.subSquaresSize * 0.5 + dX;
				double y0 = y - m_DbfIn.subSquaresSize * 0.5 + dY;
				Shapes::DVector v0 = Shapes::DVector(x0, y0);

				if (m_pCurrentShape->PtInside(v0))
				{
					// random type
					int type = GetRandomType(false);

					ModuleObjectOutput out;

					// sets data
					out.position = v0;
					out.type     = type;
					out.rotation = RandomInRangeF(0, EtMathD::TWO_PI);

					double minScale = m_ObjectsIn[type].m_MinHeight;
					double maxScale = m_ObjectsIn[type].m_MaxHeight;

					out.scaleX = out.scaleY = out.scaleZ = RandomInRangeF(minScale, maxScale) / 100.0;

					// used only by VLB for preview
					out.length = 10.0;
					out.width  = 10.0;
					out.height = 10.0;

					// adds to array
					m_ObjectsOut.Add(out);

					// update type counter
					m_ObjectsIn[type].m_Counter++;
				}
			}
		}

		// --------------------------------------------------------------------//

		int FramedForestPlacer::GetRandomType(bool inner)
		{
			double p = (double)rand() / ((double)RAND_MAX + 1);

			bool found = false;

			// searches first object matching inner/outer
			int index  = 0;
			while (m_ObjectsIn[index].m_Inner != inner)
			{
				index++;
			}

			double curProb = m_ObjectsIn[index].m_NormProb;
			while (!found)
			{
				if (p < curProb)
				{
					found = true;
				}
				else
				{
					index++;
					// increments only if objects matches inner/outer
					if (m_ObjectsIn[index].m_Inner == inner)
					{
						curProb += m_ObjectsIn[index].m_NormProb;
					}
				}
			}
			return index;
		}

		// --------------------------------------------------------------------//

		void FramedForestPlacer::RotateCreatedObjects(double angle)
		{
			Shapes::DVertex shapeBarycenter = m_pCurrentShape->GetBarycenter();
			int objOutCount = m_ObjectsOut.Size();
			for (int i = 0; i < objOutCount; ++i)
			{
				Shapes::DVector v = m_ObjectsOut[i].position;
				double dx = v.x - shapeBarycenter.x;
				double dy = v.y - shapeBarycenter.y;

				v.x = shapeBarycenter.x + dx * EtMathD::Cos(angle) - dy * EtMathD::Sin(angle);
				v.y = shapeBarycenter.y + dx * EtMathD::Sin(angle) + dy * EtMathD::Cos(angle);
				m_ObjectsOut[i].position = v;
				m_ObjectsOut[i].rotation += angle;
			}			
		}

		// --------------------------------------------------------------------//

		void FramedForestPlacer::ExportCreatedObjects(IMainCommands* cmdIfc)
		{
			int objOutCount = m_ObjectsOut.Size();
			for (int i = 0; i < objOutCount; ++i)
			{
				RString name = m_ObjectsIn[m_ObjectsOut[i].type].m_Name;

				Vector3 position  = Vector3(static_cast<Coord>(m_ObjectsOut[i].position.x), 0, static_cast<Coord>(m_ObjectsOut[i].position.y));

				Matrix4 mTranslation = Matrix4(MTranslation, position);
				Matrix4 mRotation    = Matrix4(MRotationY, static_cast<Coord>(m_ObjectsOut[i].rotation));
				Matrix4 mScaling     = Matrix4(MScale, static_cast<Coord>(m_ObjectsOut[i].scaleX), static_cast<Coord>(m_ObjectsOut[i].scaleZ), static_cast<Coord>(m_ObjectsOut[i].scaleY));

				Matrix4 transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject(transform, name, 0);
			}
		}

		// --------------------------------------------------------------------//

		void FramedForestPlacer::WriteReport()
		{
			printf("Created                  \n");
			int objInCount = m_ObjectsIn.Size();
			for (int i = 0; i < objInCount; ++i)
			{
				printf("%d", m_ObjectsIn[i].m_Counter);
				printf(" : ");
				printf(m_ObjectsIn[i].m_Name);
				printf("                           \n");
			}
			printf("--------------------------------------\n");
		}
	}
}