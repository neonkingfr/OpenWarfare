// *********************************************************************** //
// HomologousPoint2                                                        //
// *********************************************************************** //
// Class definying an homologous point with two dimensions.                //
// Homologous points are points whose coordinates are known in two         //
// different coordinate systems.                                           //
// ----------------------------------------------------------------------- //
// Real can be float or double.                                            //
// ----------------------------------------------------------------------- //
//                                                                         //
//                                                                         //
//                                                                         //
//                                                                         //
//                                                                         //
//                                                                         //
// *********************************************************************** //

#ifndef HOMOLOGOUSPOINT2_H
#define HOMOLOGOUSPOINT2_H

#include <utility>

#include ".\Point2.h"

using std::pair;

template <class Real>
class HomologousPoint2
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// ----------------------------------------------------- //
	// The pair of points representing this point in the two //
	// reference coordinate systems.                         //
	// ----------------------------------------------------- //
    pair<Point2<Real>, Point2<Real> > m_XY12;

	// ----------------------------------------------------- //
	// True or false in dependence of the validity of this   //
	// point as homologous point.                            //
	// Example of invalid points are the central points in a //
	// list of three or more points aligned or               //
	// quasi-aligned.                                        //
	// By default this point is a valid one. It is up to the //
	// code who tests the validity of this point to set its  //
	// value to false if needed.                             //
	// ----------------------------------------------------- //
    bool m_Valid;

	// ----------------------------------------------------- //
	// This variable is intended to give to the user the     //
	// chances to choice which points to use from a list of  //
	// homologous points. May happen that the user wants to  //
	// use only one subset of these points, so he can do it  //
	// by setting this value to true or false.               //
	// By default this point is an active one. It is up to   //
	// the code who tests the validity of this point to set  //
	// its value to false if needed.                         //
	// ----------------------------------------------------- //
	bool m_Active;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //
	HomologousPoint2(const Point2<Real>& pSys1 = NULL, const Point2<Real>& pSys2 = NULL, 
					 bool valid = true, bool active = true);

	// ************************ //
	// Member variables getters //
	// ************************ //

	// IsValid()
	// Returns the validity value of this point
	bool IsValid() const;

	// IsActive()
	// Returns the activation value of this point
	bool IsActive() const;

	// ********* //
	// Interface //
	// ********* //

	// GetXYSystem1()
	// Returns this point as it is in the first
	// reference coordinate systems.
	Point2<Real> GetXYSystem1() const;

	// GetXYSystem2()
	// Returns this point as it is in the second
	// reference coordinate systems.
	Point2<Real> GetXYSystem2() const;

	// Validate()
	// Sets the validity of this point with the given value
	void Validate(bool valid);

	// Activate()
	// Sets the activation of this point with the given value
	void Activate(bool active);

};

#include "..\src\HomologousPoint2.inl"

#endif