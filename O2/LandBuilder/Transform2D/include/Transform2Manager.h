// *********************************************************************** //
// Transform2Manager                                                       //
// *********************************************************************** //
// Class to manage two dimensional transformations.                        //
// ----------------------------------------------------------------------- //
// Real can be float or double.                                            //
// *********************************************************************** //

#ifndef TRANSFORM2MANAGER_H
#define TRANSFORM2MANAGER_H

#include <string>
#include <vector>

#include ".\HomologousPoint2List.h"
#include ".\Transform2.h"
#include ".\Transform2Rigid.h"
#include ".\Transform2Conform.h"
#include ".\Transform2Affine.h"
#include ".\Transform2AffineEx.h"
#include ".\Transform2Homographic.h"
#include ".\Transform2Bilinear.h"
#include ".\Transform2Polynomial2.h"

using std::string;
using std::vector;

// ************************************************************** //
// Enumerator of possible transformations                         //
// ************************************************************** //
typedef enum
{
	T2_NONE,
	T2_RIGID,
	T2_CONFORM,
	T2_AFFINE,
	T2_AFFINEEX,
	T2_HOMOGRAPHIC,
	T2_BILINEAR,
	T2_POLYNOMIAL2,
	T2_BESTFIT,
	T2_WIDEST
} Transformations2;

template <class Real>
class Transform2Manager
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// ----------------------------- //
	// the list of homologous points //
	// ----------------------------- //
	HomologousPoint2List<Real> m_HomoPointsList;

	// -------------------------- //
	// the list of control points //
	// -------------------------- //
	HomologousPoint2List<Real> m_CtrlPointsList;

	// ----------------------------- //
	// pointer to the transformation //
	// ----------------------------- //
	Transform2<Real>* m_Transformation;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //
	Transform2Manager();

	// ********** //
	// Destructor //
	// ********** //
	~Transform2Manager();

	// ************************ //
	// Member variables setters //
	// ************************ //

	// SetHomoPointsList()
	// Sets the list of homologous points of this transformation using a 
	// copy of the given list.
	void SetHomoPointsList(const HomologousPoint2List<Real>& homoPointsList);

	// SetCtrlPointsList()
	// Sets the list of control points of this transformation using a 
	// copy of the given list.
	void SetCtrlPointsList(const HomologousPoint2List<Real>& ctrlPointsList);

	// ********* //
	// Interface //
	// ********* //

	// GetTransformationName()
	// Returns the name of the current transformation.
	string GetTransformationName() const;

	// GetParameters()
	// Returns the parameters of current transformation.
	const vector<Real>& GetParameters() const;

	// ChooseTransformation()
	// Calculates the parameters of this transformation using
	// the list of homologous points as input for the least square method.
	// If the given Transformations2 is one of the following:
	// T2_RIGID
	// T2_CONFORM
	// T2_AFFINE
	// T2_AFFINEEX
	// T2_HOMOGRAPHIC
	// T2_BILINEAR
	// T2_POLYNOMIAL2
	// the parameter will be the ones of the given transformation.
	// If the given Transformations2 is:
	// T2_BESTFIT
	// all transformations will be tested and will be taken the parameters of 
	// the one giving the minimum differences on reference points and control points 
	// transformed coordinates.
	// If the given Transformations2 is:
	// T2_WIDEST
	// the parameters will be the ones of the first transformation that can be applied
	// in dependence of the reference points in the order:
	// 1 - T2_POLYNOMIAL
	// 2 - T2_BILINEAR
	// 3 - T2_HOMOGRAPHIC
	// 4 - T2_AFFINEEX
	// 5 - T2_AFFINE
	// 6 - T2_CONFORM
	// 7 - T2_RIGID
	// Returns true if successfull (if we have the parameters to apply the transformation).
	bool ChooseTransformation(Transformations2 transformation);

	// ApplyTranformation()
	// Applies the transformation the the given list of points and returns
	// the list of transformed points.
	vector<Point2<Real> > ApplyTransformation(vector<Point2<Real> > pointsIn);

	// **************** //
	// Helper functions //
	// **************** //
protected:
	// Clear()
	// Clears the transformation pointer;
	void Clear();

	// BestFit()
	// Tries all the available transformations and choose the one whose differences
	// on transformed coordinates are the smallest.
	void BestFit();

	// Widest()
	// Returns a pointer to the first transformation applicable, depending on the 
	// given homologous points, in the order:
	// 1 - POLYNOMIAL2
	// 2 - BILINEAR
	// 3 - HOMOGRAPHIC
	// 4 - AFFINEEX
	// 5 - AFFINE
	// 6 - CONFORM
	// 7 - RIGID
	// 8 - NONE
	void Widest();

	// VerifyPoints()
	// Verifies the homologous and the control points and returns the sum of all
	// the squared distances between calculated and given points.
	Real VerifyPoints();
};

#include "..\src\Transform2Manager.inl"

#endif
