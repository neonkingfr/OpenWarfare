// *********************************************************************** //
// Point2                                                                  //
// *********************************************************************** //
// Class definying an point with two dimensions.                           //
// ----------------------------------------------------------------------- //
// Real can be float or double.                                            //
// ----------------------------------------------------------------------- //
//                                                                         //
//                                                                         //
//                                                                         //
//                                                                         //
//                                                                         //
//                                                                         //
// *********************************************************************** //

#ifndef POINT2_H
#define POINT2_H

template <class Real>
class Point2
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// ----------------------------- //
	// The coordinates of this point //
	// ----------------------------- //
    union
	{
		struct
		{
			Real m_X;
			Real m_Y;
		};
		Real m_XY[2];
	};

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //
	Point2(Real x = static_cast<Real>(0.0), Real y = static_cast<Real>(0.0));

	// ************************ //
	// Member variables getters //
	// ************************ //
	
	// X()
	// Returns the x coordinate of this point
	Real X() const;

	// Y()
	// Returns the y coordinate of this point
	Real Y() const;

	// ************************ //
	// Member variables setters //
	// ************************ //
	
	// X()
	// Sets the x coordinate of this point
	void X(Real x);

	// Y()
	// Sets the y coordinate of this point
	void Y(Real y);

	// ********* //
	// Interface //
	// ********* //

	// TranslateX()
	// Translates the x coordinate of this point 
	// with the given value
	void TranslateX(Real valueX);	

	// TranslateY()
	// Translates the y coordinate of this point 
	// with the given value
	void TranslateY(Real valueY);	

	// SquaredDistance()
	// Returns the squared distance between this point
	// and the given one.
	Real SquaredDistance(const Point2<Real>& other) const;
};

// DistancePoint2Line2()
// Returns the distance from the given point pE to the line connecting
// the given points p1 and p2
template <class Real>
Real DistancePoint2Line2(const Point2<Real>& p1, const Point2<Real>& p2, const Point2<Real>& pE);

#include "..\src\Point2.inl"

#endif