// *********************************************************************** //
// Transform2                                                              //
// *********************************************************************** //
// Base abstract class for two dimensional transformations.                //
// ----------------------------------------------------------------------- //
// The parameters of the transformation can be obtained passing to this    //
// class a list of homologous points (points whose coordinate are known in //
// both systems). In this case the calculated parameters will give the     //
// transformation from system 1 to system 2 (system 1 = first point in the //
// pair defining the homologous points).                                   //
// ----------------------------------------------------------------------- //
// Real can be float or double.                                            //
// *********************************************************************** //

#ifndef TRANSFORM2_H
#define TRANSFORM2_H

#include "vector"
#include "string"

#include ".\HomologousPoint2List.h"
#include "..\..\LeastSquares\include\LeastSquaresSolver.h"

using std::vector;
using std::string;

template <class Real>
class Transform2
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// ------------------------------- //
	// the name of this transformation //
	// ------------------------------- //
	string m_Name;

	// ----------------------------- //
	// the list of homologous points //
	// ----------------------------- //
	HomologousPoint2List<Real> m_HomoPointsList;

	// --------------------------------------------------------- //
	// the minimum number of valid homologous points required by //
	// the transormation                                         //
	// --------------------------------------------------------- //
	// When there are more than two points, these must be        //
	// non-aligned.                                              //
	// --------------------------------------------------------- //
    unsigned int m_HomoPointsMinCount;

	// ------------------------ //
	// the number of parameters //
	// ------------------------ //
    unsigned int m_ParametersCount;

	// ------------------------ //
	// the number of incognites //
	// ------------------------ //
    unsigned int m_IncognitesCount;

	// ---------------------- //
	// the list of parameters //
	// ---------------------- //
    vector<Real> m_Parameters;

	// -------------------------- //
	// The matrix of coefficients //
	// -------------------------- //
	MatrixMxN<Real> m_MatA;

	// ------------------------------- //
	// The matrix of equations weights //
	// ------------------------------- //
	MatrixMxN<Real> m_MatW;

	// ------------------------- //
	// The vector of known terms //
	// ------------------------- //
	VectorN<Real> m_VecL;

	// ------------------------------ //
	// The least squares class solver //
	// ------------------------------ //
	LeastSquaresSolver<Real> m_LSSolver;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //
	Transform2();

	// ************************ //
	// Member variables getters //
	// ************************ //

	// GetName()
	// Returns the name of this transformation.
	string GetName() const;

	// ************************ //
	// Member variables setters //
	// ************************ //

	// SetHomoPointsList()
	// Sets the list of homologous points of this transformation using a 
	// copy of the given list.
	void SetHomoPointsList(const HomologousPoint2List<Real>& homoPointsList);

	// ********* //
	// Interface //
	// ********* //

	// CanProceed()
	// Returns true if this transformation has enough valid homologous points
	// to proceed to parameters calculation.
	bool CanProceed();

	// GetParameters()
	// Returns the parameters of this transformation.
	const vector<Real>& GetParameters() const;

	// GetSigmaSquared()
	// Returns the sigma squared of the parameters of this transformation.
	Real GetSigmaSquared() const;

	// DoParametersCalculation()
	// Calculates the parameters of this transformation using
	// the list of homologous points as input for the least square method.
	// Returns true if successfull.
	// Assumes that the CanProceed() method has been already called with success.
	// The obtained parameters will transform points from system 1 to system 2.
	bool DoParametersCalculation();

	// TransformPoint()
	// Transforms the given point using this transformation.
	virtual Point2<Real> TransformPoint(Point2<Real> p) = 0;

	// **************** //
	// Helper functions //
	// **************** //
protected:
	// SetCoefficientsMatrix()
	// Sets the matrix of the coefficients to be used by the least squares
	// method, using the homologous points list.
	// Returns true if successfull.
	virtual bool SetCoefficientsMatrix() = 0;

	// SetKnownsVector()
	// Sets the vector of the known terms to be used by the least squares
	// method, using the homologous points list.
	// Returns true if successfull.
	virtual bool SetKnownsVector() = 0;

	// SetWeightsMatrix()
	// Sets the matrix of the equations' weights to be used by the least squares
	// method, using the homologous points list.
	// Returns true if successfull.
	virtual bool SetWeightsMatrix() = 0;

	// ConvertParameters()
	// Converts the parameters from the form returned by the least squares solver
	// to the one tipical of this transformation.
	// Returns true if successfull.
	virtual bool ConvertParameters() = 0;
};

#include "..\src\Transform2.inl"

#endif