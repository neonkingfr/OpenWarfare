// ************************************************************************** //
// Constructors                                                               //
// ************************************************************************** //

template <class Real>
Transform2Homographic<Real>::Transform2Homographic() : Transform2<Real>()
{
	m_Name               = "Homographic";
	m_HomoPointsMinCount = 4;
	m_ParametersCount    = 8;
	m_IncognitesCount    = 8;
}

// ************************************************************************** //
// Interface                                                                  //
// ************************************************************************** //

template <class Real>
Point2<Real> Transform2Homographic<Real>::TransformPoint(Point2<Real> p)
{
	Point2<Real> transP;
	
	if(m_Parameters.size() > 0)
	{
		transP.X((m_Parameters[0] * p.X() + m_Parameters[1] * p.Y() + m_Parameters[2]) / (m_Parameters[6] * p.X() + m_Parameters[7] * p.Y() + static_cast<Real>(1.0))); 
		transP.Y((m_Parameters[3] * p.X() + m_Parameters[4] * p.Y() + m_Parameters[5]) / (m_Parameters[6] * p.X() + m_Parameters[7] * p.Y() + static_cast<Real>(1.0))); 
	}

	return transP;
}

// ************************************************************************** //
// Helper functions                                                           //
// ************************************************************************** //

template <class Real>
bool Transform2Homographic<Real>::SetCoefficientsMatrix()
{
	m_MatA.SetSize(2 * m_HomoPointsList.ActiveAndValidCount(), m_IncognitesCount);

	vector<HomologousPoint2<Real> > avPoints = m_HomoPointsList.GetActiveAndValidPoints();

	size_t pointsCount = avPoints.size();
	for(unsigned int i = 0; i < pointsCount; ++i)
	{
		Point2<Real> p1 = avPoints[i].GetXYSystem1();
		Point2<Real> p2 = avPoints[i].GetXYSystem2();

		m_MatA.SetAt(0 + (i * 2), 0, p1.X());
		m_MatA.SetAt(0 + (i * 2), 1, p1.Y());
		m_MatA.SetAt(0 + (i * 2), 2, static_cast<Real>(1.0));
		m_MatA.SetAt(0 + (i * 2), 6, -(p1.X() * p2.X()));
		m_MatA.SetAt(0 + (i * 2), 7, -(p1.Y() * p2.X()));

		m_MatA.SetAt(1 + (i * 2), 3, p1.X());
		m_MatA.SetAt(1 + (i * 2), 4, p1.Y());
		m_MatA.SetAt(1 + (i * 2), 5, static_cast<Real>(1.0));
		m_MatA.SetAt(1 + (i * 2), 6, -(p1.X() * p2.Y()));
		m_MatA.SetAt(1 + (i * 2), 7, -(p1.Y() * p2.Y()));
	}

	return m_LSSolver.SetCoefficientsMatrix(m_MatA);
}

// -------------------------------------------------------------------------- //

template <class Real>
bool Transform2Homographic<Real>::SetKnownsVector()
{
	m_VecL.SetDimensions(2 * m_HomoPointsList.ActiveAndValidCount());

	vector<HomologousPoint2<Real> > avPoints = m_HomoPointsList.GetActiveAndValidPoints();

	size_t pointsCount = avPoints.size();
	for(unsigned int i = 0; i < pointsCount; ++i)
	{
		m_VecL.SetAt(0 + (i * 2), avPoints[i].GetXYSystem2().X());
		m_VecL.SetAt(1 + (i * 2), avPoints[i].GetXYSystem2().Y());
	}

	return m_LSSolver.SetKnownsVector(m_VecL);
}

// -------------------------------------------------------------------------- //

template <class Real>
bool Transform2Homographic<Real>::SetWeightsMatrix()
{
	// the current implementation uses the same weight for every
	// equation, so the weights matrix is just the unitary matrix

	unsigned int size = 2 * m_HomoPointsList.ActiveAndValidCount();
	m_MatW.SetSize(size, size);
	if(!m_MatW.SetUnitary()) return false;

	return m_LSSolver.SetWeightsMatrix(m_MatW);
}

// -------------------------------------------------------------------------- //

template <class Real>
bool Transform2Homographic<Real>::ConvertParameters()
{
	m_Parameters.clear();

	VectorN<Real> vecX = m_LSSolver.GetSolutionsVector();

	Real p0 = vecX[0];
	m_Parameters.push_back(p0);
	Real p1 = vecX[1];
	m_Parameters.push_back(p1);
	Real p2 = vecX[2];
	m_Parameters.push_back(p2);
	Real p3 = vecX[3];
	m_Parameters.push_back(p3);
	Real p4 = vecX[4];
	m_Parameters.push_back(p4);
	Real p5 = vecX[5];
	m_Parameters.push_back(p5);
	Real p6 = vecX[6];
	m_Parameters.push_back(p6);
	Real p7 = vecX[7];
	m_Parameters.push_back(p7);

	return true;
}