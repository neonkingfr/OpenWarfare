// ************************************************************************** //
// Constructors                                                               //
// ************************************************************************** //

template <class Real>
Transform2<Real>::Transform2()
{
}

// ************************************************************************** //
// Member variables getters                                                   //
// ************************************************************************** //

template <class Real>
string Transform2<Real>::GetName() const
{
	return m_Name;
}

// ************************************************************************** //
// Member variables setters                                                   //
// ************************************************************************** //

template <class Real>
void Transform2<Real>::SetHomoPointsList(const HomologousPoint2List<Real>& homoPointList)
{
	m_HomoPointsList = homoPointList;
}

// ************************************************************************** //
// Interface                                                                  //
// ************************************************************************** //

template <class Real>
bool Transform2<Real>::CanProceed()
{
	// the number of active and valid homologous points must be greater then 
	// or equal to the minimum number of point required
	if(m_HomoPointsList.ActiveAndValidCount() < m_HomoPointsMinCount) return false;
	return true;
}

// -------------------------------------------------------------------------- //

template <class Real>
const vector<Real>& Transform2<Real>::GetParameters() const
{
	return m_Parameters;
}

// -------------------------------------------------------------------------- //

template <class Real>
Real Transform2<Real>::GetSigmaSquared() const
{
	return m_LSSolver.GetResidualsSquaredSigma();
}

// -------------------------------------------------------------------------- //

template <class Real>
bool Transform2<Real>::DoParametersCalculation()
{
	if(SetCoefficientsMatrix() && SetWeightsMatrix() && SetKnownsVector() && 
	   m_LSSolver.Solve() && ConvertParameters())
	{
		return true;
	}
	else
	{
		return false;
	}
}
