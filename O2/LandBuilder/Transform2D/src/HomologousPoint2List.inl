// ************************************************************************** //
// Constructors                                                               //
// ************************************************************************** //

template <class Real>
HomologousPoint2List<Real>::HomologousPoint2List()
{
	m_ActiveCount         = NULL;
	m_ValidCount          = NULL;
	m_ActiveAndValidCount = NULL;
	m_AlignementChecked   = false;
}

// ************************************************************************** //
// Destructor                                                                 //
// ************************************************************************** //
template <class Real>
HomologousPoint2List<Real>::~HomologousPoint2List()
{
	Clear();
}

// ************************************************************************** //
// Assignement                                                                //
// ************************************************************************** //

template <class Real>
HomologousPoint2List<Real>& HomologousPoint2List<Real>::operator = (const HomologousPoint2List<Real>& other)
{
	// empties the current list
	Clear();

	// copies items from the other list
	size_t pointsCount = other.Count();
	for(unsigned int i = 0; i < pointsCount; ++i)
	{
		m_Points.push_back(other.m_Points[i]);
	}

	// Doesn't points lists, if needed they will be recalculated.
	// Same for the active count, valid count, active and valid count
	// and for the alignement check

	return *this;
}

// ************************************************************************** //
// Member variables getters                                                   //
// ************************************************************************** //

template <class Real>
vector<HomologousPoint2<Real> > HomologousPoint2List<Real>::GetPoints() const
{
	return m_Points;
}

// -------------------------------------------------------------------------- //

template <class Real>
vector<HomologousPoint2<Real> > HomologousPoint2List<Real>::GetActiveAndValidPoints() 
{
	// calculates only if needed
	if(m_ActiveAndValidPoints.size() == 0) CalculateActiveAndValidPoints();
	return m_ActiveAndValidPoints;
}

// ************************************************************************** //
// Interface                                                                  //
// ************************************************************************** //

template <class Real>
void HomologousPoint2List<Real>::AddPoint(HomologousPoint2<Real> point)
{
	// appends point
	m_Points.push_back(point);
	// clears active count (adding a point may change it)
	ClearActiveCount();
	// clears valid count (adding a point may change it)
	ClearValidCount();
	// clears active and valid count (adding a point may change it)
	ClearActiveAndValidCount();
	// reset alignement check variable
	m_AlignementChecked = false;
}

// -------------------------------------------------------------------------- //

template <class Real>
bool HomologousPoint2List<Real>::RemovePoint(unsigned int index)
{
	if(index >= m_Points.size()) return false;

	// removes point at index
	m_Points.erase(m_Points.begin() + index);
	// clears active count (removing a point may change it)
	ClearActiveCount();
	// clears valid count (removing a point may change it)
	ClearValidCount();
	// clears active and valid count (removing a point may change it)
	ClearActiveAndValidCount();
	// reset alignement check variable
	m_AlignementChecked = false;

	return true;
}

// -------------------------------------------------------------------------- //

template <class Real>
HomologousPoint2<Real>* HomologousPoint2List<Real>::GetPoint(unsigned int index)
{
	if(index >= m_Points.size()) return NULL;

	return &m_Points[index];
}

// -------------------------------------------------------------------------- //

template <class Real>
void HomologousPoint2List<Real>::Clear()
{
	// clears points list
	m_Points.clear();
	// clears active count
	ClearActiveCount();
	// clears valid count
	ClearValidCount();
	// clears active and valid count
	ClearActiveAndValidCount();
	// reset alignement check variable
	m_AlignementChecked = false;
}

// -------------------------------------------------------------------------- //

template <class Real>
unsigned int HomologousPoint2List<Real>::Count() const
{
	return static_cast<unsigned int>(m_Points.size());
}

// -------------------------------------------------------------------------- //

template <class Real>
unsigned int HomologousPoint2List<Real>::ActiveCount()
{
	// calculates only if needed
	if(!m_ActiveCount) CalculateActiveCount();
	return *m_ActiveCount;
}

// -------------------------------------------------------------------------- //

template <class Real>
unsigned int HomologousPoint2List<Real>::ValidCount()
{
	// calculates only if needed
	if(!m_ValidCount) CalculateValidCount();
	return *m_ValidCount;
}

// -------------------------------------------------------------------------- //

template <class Real>
unsigned int HomologousPoint2List<Real>::ActiveAndValidCount()
{
	// calculates only if needed
	if(!m_ActiveAndValidCount) CalculateActiveAndValidCount();
	return *m_ActiveAndValidCount;
}

// ************************************************************************** //
// Helper functions                                                           //
// ************************************************************************** //

template <class Real>
void HomologousPoint2List<Real>::CalculateActiveAndValidPoints()
{
	size_t pointsCount = m_Points.size(); 
	if(pointsCount > 0)
	{
		for(unsigned int i = 0; i < pointsCount; ++i)
		{
			if(m_Points[i].IsActive() && m_Points[i].IsValid())
			{
				m_ActiveAndValidPoints.push_back(m_Points[i]);
			}		
		}
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
void HomologousPoint2List<Real>::CalculateActiveCount()
{
	unsigned int pointsCount = m_Points.size(); 
	unsigned int counter = 0;
	if(pointsCount > 0)
	{
		for(unsigned int i = 0; i < pointsCount; ++i)
		{
			if(m_Points[i].IsActive()) counter++;
		}
		m_ActiveCount = new unsigned int(counter);
		return;
	}
	m_ActiveCount = new unsigned int(0);
}

// -------------------------------------------------------------------------- //

template <class Real>
void HomologousPoint2List<Real>::CalculateValidCount()
{
	if(!m_AlignementChecked) CheckAlignement();

	unsigned int pointsCount = m_Points.size(); 
	unsigned int counter = 0;
	if(pointsCount > 0)
	{
		for(unsigned int i = 0; i < pointsCount; ++i)
		{
			if(m_Points[i].IsValid()) counter++;
		}
		m_ValidCount = new unsigned int(counter);
		return;
	}
	m_ValidCount = new unsigned int(0);
}

// -------------------------------------------------------------------------- //

template <class Real>
void HomologousPoint2List<Real>::CalculateActiveAndValidCount()
{
	size_t pointsCount = m_Points.size(); 
	unsigned int counter = 0;
	if(pointsCount > 0)
	{
		for(unsigned int i = 0; i < pointsCount; ++i)
		{
			if(m_Points[i].IsActive() && m_Points[i].IsValid()) counter++;
		}
		m_ActiveAndValidCount = new unsigned int(counter);
		return;
	}
	m_ActiveAndValidCount = new unsigned int(0);
}

// -------------------------------------------------------------------------- //

template <class Real>
void HomologousPoint2List<Real>::ClearActiveCount()
{
	if(m_ActiveCount) 
	{
		delete m_ActiveCount;
		m_ActiveCount = NULL;
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
void HomologousPoint2List<Real>::ClearValidCount()
{
	if(m_ValidCount) 
	{
		delete m_ValidCount;
		m_ValidCount = NULL;

		// revalidates all points
		size_t pointsCount = m_Points.size(); 
		if(pointsCount > 0)
		{
			for(unsigned int i = 0; i < pointsCount; ++i)
			{
				m_Points[i].Validate(true);
			}
		}
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
void HomologousPoint2List<Real>::ClearActiveAndValidCount()
{
	if(m_ActiveAndValidCount) 
	{
		delete m_ActiveAndValidCount;
		m_ActiveAndValidCount = NULL;
	}
}

// -------------------------------------------------------------------------- //

template <class Real>
void HomologousPoint2List<Real>::CheckAlignement()
{
	size_t pointsCount = m_Points.size(); 

	if(pointsCount > 1)
	{
		// the alignement check is done in this way:
		// 1) searches for the two farest points in system 1

		Real maxDist = static_cast<Real>(0.0);
		unsigned int p1Index;
		unsigned int p2Index;
		for(unsigned int i = 0; i < pointsCount - 1; ++i)
		{
			for(unsigned int j = i + 1; j < pointsCount; ++j)
			{
				if(m_Points[i].IsActive() && m_Points[j].IsActive())
				{
					Real dist = m_Points[i].GetXYSystem1().SquaredDistance(m_Points[j].GetXYSystem1());
					if(dist > maxDist)
					{
						maxDist = dist;
						p1Index = i;
						p2Index = j;
					}
				}
			}
		}

		if(maxDist == static_cast<Real>(0.0)) return;

		maxDist = EtMath<Real>::Sqrt(maxDist);

		// 2) then checks all the other points to see which is their distance
		//    from the line connecting the two points found and stores the greatest
		//    distance

		Real maxDist2 = 0.0;
		Point2<Real> p1 = m_Points[p1Index].GetXYSystem1();
		Point2<Real> p2 = m_Points[p2Index].GetXYSystem1();
		for(unsigned int i = 0; i < pointsCount; ++i)
		{
			if(i != p1Index && i != p2Index)
			{
				Real dist = DistancePoint2Line2(p1, p2, m_Points[i].GetXYSystem1());
				if(dist > maxDist2)
				{
					maxDist2 = dist;
				}
			}
		}

		// 3) finally checks the newly found distance with the former. If the first
		//    is a small percent of the second, then the points are aligned or
		//    quasi-aligned (at the moment we use 10 percent)

		if(maxDist2 / maxDist < static_cast<Real>(0.1))
		{
			// points are aligned or quasi aligned, so invalidates
			// all the points, but p1 and p2
			for(unsigned int i = 0; i < pointsCount; ++i)
			{
				if(i != p1Index && i != p2Index)
				{
					m_Points[i].Validate(false);
				}
			}
		}
	}

	// now the alignement has been checked
	m_AlignementChecked = true;
}
