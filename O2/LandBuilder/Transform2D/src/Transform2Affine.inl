// ************************************************************************** //
// Constructors                                                               //
// ************************************************************************** //

template <class Real>
Transform2Affine<Real>::Transform2Affine() : Transform2<Real>()
{
	m_Name               = "Affine";
	m_HomoPointsMinCount = 3;
	m_ParametersCount    = 5;
	m_IncognitesCount    = 6;
}

// ************************************************************************** //
// Interface                                                                  //
// ************************************************************************** //

template <class Real>
Point2<Real> Transform2Affine<Real>::TransformPoint(Point2<Real> p)
{
	Point2<Real> transP;
	
	if(m_Parameters.size() > 0)
	{
		transP.X(m_Parameters[0] + m_Parameters[3] * p.X() * EtMath<Real>::Cos(m_Parameters[2]) - m_Parameters[4] * p.Y() * EtMath<Real>::Sin(m_Parameters[2])); 
		transP.Y(m_Parameters[1] + m_Parameters[3] * p.X() * EtMath<Real>::Sin(m_Parameters[2]) + m_Parameters[4] * p.Y() * EtMath<Real>::Cos(m_Parameters[2])); 
	}

	return transP;
}

// ************************************************************************** //
// Helper functions                                                           //
// ************************************************************************** //

template <class Real>
bool Transform2Affine<Real>::SetCoefficientsMatrix()
{
	m_MatA.SetSize(2 + 2 * m_HomoPointsList.ActiveAndValidCount(), m_IncognitesCount);

	vector<HomologousPoint2<Real> > avPoints = m_HomoPointsList.GetActiveAndValidPoints();

	m_MatA.SetAt(0, 0, static_cast<Real>(0.0));
	m_MatA.SetAt(0, 1, static_cast<Real>(0.0));
	m_MatA.SetAt(0, 2, static_cast<Real>(1.0));
	m_MatA.SetAt(0, 3, static_cast<Real>(0.0));
	m_MatA.SetAt(0, 4, static_cast<Real>(0.0));
	m_MatA.SetAt(0, 5, static_cast<Real>(-1.0));

	m_MatA.SetAt(1, 0, static_cast<Real>(0.0));
	m_MatA.SetAt(1, 1, static_cast<Real>(0.0));
	m_MatA.SetAt(1, 2, static_cast<Real>(0.0));
	m_MatA.SetAt(1, 3, static_cast<Real>(1.0));
	m_MatA.SetAt(1, 4, static_cast<Real>(1.0));
	m_MatA.SetAt(1, 5, static_cast<Real>(0.0));

	size_t pointsCount = avPoints.size();
	for(unsigned int i = 0; i < pointsCount; ++i)
	{
		m_MatA.SetAt(2 + (i * 2), 0, static_cast<Real>(1.0));
		m_MatA.SetAt(2 + (i * 2), 1, static_cast<Real>(0.0));
		m_MatA.SetAt(2 + (i * 2), 2, avPoints[i].GetXYSystem1().X());
		m_MatA.SetAt(2 + (i * 2), 3, avPoints[i].GetXYSystem1().Y());
		m_MatA.SetAt(2 + (i * 2), 4, static_cast<Real>(0.0));
		m_MatA.SetAt(2 + (i * 2), 5, static_cast<Real>(0.0));

		m_MatA.SetAt(3 + (i * 2), 0, static_cast<Real>(0.0));
		m_MatA.SetAt(3 + (i * 2), 1, static_cast<Real>(1.0));
		m_MatA.SetAt(3 + (i * 2), 2, static_cast<Real>(0.0));
		m_MatA.SetAt(3 + (i * 2), 3, static_cast<Real>(0.0));
		m_MatA.SetAt(3 + (i * 2), 4, avPoints[i].GetXYSystem1().X());
		m_MatA.SetAt(3 + (i * 2), 5, avPoints[i].GetXYSystem1().Y());
	}

	return m_LSSolver.SetCoefficientsMatrix(m_MatA);
}

// -------------------------------------------------------------------------- //

template <class Real>
bool Transform2Affine<Real>::SetKnownsVector()
{
	m_VecL.SetDimensions(2 + 2 * m_HomoPointsList.ActiveAndValidCount());

	vector<HomologousPoint2<Real> > avPoints = m_HomoPointsList.GetActiveAndValidPoints();

	m_VecL.SetAt(0, static_cast<Real>(0.0));
	m_VecL.SetAt(1, static_cast<Real>(0.0));

	size_t pointsCount = avPoints.size();
	for(unsigned int i = 0; i < pointsCount; ++i)
	{
		m_VecL.SetAt(2 + (i * 2), avPoints[i].GetXYSystem2().X());
		m_VecL.SetAt(3 + (i * 2), avPoints[i].GetXYSystem2().Y());
	}

	return m_LSSolver.SetKnownsVector(m_VecL);
}

// -------------------------------------------------------------------------- //

template <class Real>
bool Transform2Affine<Real>::SetWeightsMatrix()
{
	// the current implementation uses the same weight for every
	// equation, so the weights matrix is just the unitary matrix

	unsigned int size = 2 + 2 * m_HomoPointsList.ActiveAndValidCount();
	m_MatW.SetSize(size, size);
	if(!m_MatW.SetUnitary()) return false;

	return m_LSSolver.SetWeightsMatrix(m_MatW);
}

// -------------------------------------------------------------------------- //

template <class Real>
bool Transform2Affine<Real>::ConvertParameters()
{
	m_Parameters.clear();

	VectorN<Real> vecX = m_LSSolver.GetSolutionsVector();

	Real p0 = vecX[0]; // x0
	m_Parameters.push_back(p0);
	Real p1 = vecX[1]; // y0
	m_Parameters.push_back(p1);
	Real x2 = vecX[2]; // sx * cos(a)
	Real x3 = vecX[3]; // -(sy * sin(a))
	Real x4 = vecX[4]; // sx * sin(a)
	Real x5 = vecX[5]; // sy * cos(a)

	Real sx = EtMath<Real>::Sqrt(x2 * x2 + x4 * x4); // sx
	Real sy = EtMath<Real>::Sqrt(x3 * x3 + x5 * x5); // sy

	if(sx == static_cast<Real>(0.0)) return false;
	if(sy == static_cast<Real>(0.0)) return false;

	Real invSx = static_cast<Real>(1.0) / sx;
	Real invSy = static_cast<Real>(1.0) / sy;

	x2 *= invSx; // cos(a)
	x3 *= invSy; // -sin(a)
	x4 *= invSx; // sin(a)
	x5 *= invSy; // cos(a)

	Real cosa = (x2 + x5) * static_cast<Real>(0.5); // cos(a)
	Real sina = (-x3 + x4) * static_cast<Real>(0.5); // sin(a)

	Real sign = (sina >= 0) ? static_cast<Real>(1.0) : static_cast<Real>(-1.0);

	Real a1 = sign * EtMath<Real>::ACos(cosa);
	Real a2 = EtMath<Real>::ASin(sina);

	Real a  = (a1 + a2) * static_cast<Real>(0.5); // a

	Real p2 = a;
	m_Parameters.push_back(p2);
	Real p3 = sx;
	m_Parameters.push_back(p3);
	Real p4 = sy;
	m_Parameters.push_back(p4);

	return true;
}

