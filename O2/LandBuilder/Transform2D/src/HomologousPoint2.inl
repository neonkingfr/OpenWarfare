// ************************************************************************** //
// Constructors                                                               //
// ************************************************************************** //

template <class Real>
HomologousPoint2<Real>::HomologousPoint2(const Point2<Real>& pSys1, const Point2<Real>& pSys2, 
										 bool valid, bool active)
: m_XY12(pSys1, pSys2), m_Valid(valid), m_Active(active)
{
}

// ************************************************************************** //
// Member variables getters                                                   //
// ************************************************************************** //

template <class Real>
bool HomologousPoint2<Real>::IsValid() const
{
	return m_Valid;
}

// -------------------------------------------------------------------------- //

template <class Real>
bool HomologousPoint2<Real>::IsActive() const
{
	return m_Active;
}

// ************************************************************************** //
// Interface                                                                  //
// ************************************************************************** //

template <class Real>
Point2<Real> HomologousPoint2<Real>::GetXYSystem1() const
{
	return m_XY12.first;
}

// -------------------------------------------------------------------------- //

template <class Real>
Point2<Real> HomologousPoint2<Real>::GetXYSystem2() const
{
	return m_XY12.second;
}

// -------------------------------------------------------------------------- //

template <class Real>
void HomologousPoint2<Real>::Validate(bool valid)
{
	m_Valid = valid;
}

// -------------------------------------------------------------------------- //

template <class Real>
void HomologousPoint2<Real>::Activate(bool active)
{
	m_Active = active;
}
