// ************************************************************************** //
// Constructors                                                               //
// ************************************************************************** //

template <class Real>
Transform2Polynomial2<Real>::Transform2Polynomial2() : Transform2<Real>()
{
	m_Name               = "Polynomial2";
	m_HomoPointsMinCount = 6;
	m_ParametersCount    = 12;
	m_IncognitesCount    = 12;
}

// ************************************************************************** //
// Interface                                                                  //
// ************************************************************************** //

template <class Real>
Point2<Real> Transform2Polynomial2<Real>::TransformPoint(Point2<Real> p)
{
	Point2<Real> transP;
	
	if(m_Parameters.size() > 0)
	{
		transP.X(m_Parameters[0] + m_Parameters[1] * p.X() + m_Parameters[2] * p.Y() + m_Parameters[3] * p.X() * p.Y() + m_Parameters[4] * p.X() * p.X() + m_Parameters[5] * p.Y() * p.Y()); 
		transP.Y(m_Parameters[6] + m_Parameters[7] * p.X() + m_Parameters[8] * p.Y() + m_Parameters[9] * p.X() * p.Y() + m_Parameters[10] * p.X() * p.X() + m_Parameters[11] * p.Y() * p.Y()); 
	}

	return transP;
}

// ************************************************************************** //
// Helper functions                                                           //
// ************************************************************************** //

template <class Real>
bool Transform2Polynomial2<Real>::SetCoefficientsMatrix()
{
	m_MatA.SetSize(2 * m_HomoPointsList.ActiveAndValidCount(), m_IncognitesCount);

	vector<HomologousPoint2<Real> > avPoints = m_HomoPointsList.GetActiveAndValidPoints();

	size_t pointsCount = avPoints.size();
	for(unsigned int i = 0; i < pointsCount; ++i)
	{
		m_MatA.SetAt(0 + (i * 2),  0, static_cast<Real>(1.0));
		m_MatA.SetAt(0 + (i * 2),  1, avPoints[i].GetXYSystem1().X());
		m_MatA.SetAt(0 + (i * 2),  2, avPoints[i].GetXYSystem1().Y());
		m_MatA.SetAt(0 + (i * 2),  3, avPoints[i].GetXYSystem1().X() * avPoints[i].GetXYSystem1().Y());
		m_MatA.SetAt(0 + (i * 2),  4, avPoints[i].GetXYSystem1().X() * avPoints[i].GetXYSystem1().X());
		m_MatA.SetAt(0 + (i * 2),  5, avPoints[i].GetXYSystem1().Y() * avPoints[i].GetXYSystem1().Y());
		m_MatA.SetAt(0 + (i * 2),  6, static_cast<Real>(0.0));
		m_MatA.SetAt(0 + (i * 2),  7, static_cast<Real>(0.0));
		m_MatA.SetAt(0 + (i * 2),  8, static_cast<Real>(0.0));
		m_MatA.SetAt(0 + (i * 2),  9, static_cast<Real>(0.0));
		m_MatA.SetAt(0 + (i * 2), 10, static_cast<Real>(0.0));
		m_MatA.SetAt(0 + (i * 2), 11, static_cast<Real>(0.0));

		m_MatA.SetAt(1 + (i * 2),  0, static_cast<Real>(0.0));
		m_MatA.SetAt(1 + (i * 2),  1, static_cast<Real>(0.0));
		m_MatA.SetAt(1 + (i * 2),  2, static_cast<Real>(0.0));
		m_MatA.SetAt(1 + (i * 2),  3, static_cast<Real>(0.0));
		m_MatA.SetAt(1 + (i * 2),  4, static_cast<Real>(0.0));
		m_MatA.SetAt(1 + (i * 2),  5, static_cast<Real>(0.0));
		m_MatA.SetAt(1 + (i * 2),  6, static_cast<Real>(1.0));
		m_MatA.SetAt(1 + (i * 2),  7, avPoints[i].GetXYSystem1().X());
		m_MatA.SetAt(1 + (i * 2),  8, avPoints[i].GetXYSystem1().Y());
		m_MatA.SetAt(1 + (i * 2),  9, avPoints[i].GetXYSystem1().X() * avPoints[i].GetXYSystem1().Y());
		m_MatA.SetAt(1 + (i * 2), 10, avPoints[i].GetXYSystem1().X() * avPoints[i].GetXYSystem1().X());
		m_MatA.SetAt(1 + (i * 2), 11, avPoints[i].GetXYSystem1().Y() * avPoints[i].GetXYSystem1().Y());
	}

	return m_LSSolver.SetCoefficientsMatrix(m_MatA);
}

// -------------------------------------------------------------------------- //

template <class Real>
bool Transform2Polynomial2<Real>::SetKnownsVector()
{
	m_VecL.SetDimensions(2 * m_HomoPointsList.ActiveAndValidCount());

	vector<HomologousPoint2<Real> > avPoints = m_HomoPointsList.GetActiveAndValidPoints();

	size_t pointsCount = avPoints.size();
	for(unsigned int i = 0; i < pointsCount; ++i)
	{
		m_VecL.SetAt(0 + (i * 2), avPoints[i].GetXYSystem2().X());
		m_VecL.SetAt(1 + (i * 2), avPoints[i].GetXYSystem2().Y());
	}

	return m_LSSolver.SetKnownsVector(m_VecL);
}

// -------------------------------------------------------------------------- //

template <class Real>
bool Transform2Polynomial2<Real>::SetWeightsMatrix()
{
	// the current implementation uses the same weight for every
	// equation, so the weights matrix is just the unitary matrix

	unsigned int size = 2 * m_HomoPointsList.ActiveAndValidCount();
	m_MatW.SetSize(size, size);
	if(!m_MatW.SetUnitary()) return false;

	return m_LSSolver.SetWeightsMatrix(m_MatW);
}

// -------------------------------------------------------------------------- //

template <class Real>
bool Transform2Polynomial2<Real>::ConvertParameters()
{
	m_Parameters.clear();

	VectorN<Real> vecX = m_LSSolver.GetSolutionsVector();

	Real p0 = vecX[0];
	m_Parameters.push_back(p0);
	Real p1 = vecX[1];
	m_Parameters.push_back(p1);
	Real p2 = vecX[2];
	m_Parameters.push_back(p2);
	Real p3 = vecX[3];
	m_Parameters.push_back(p3);
	Real p4 = vecX[4];
	m_Parameters.push_back(p4);
	Real p5 = vecX[5];
	m_Parameters.push_back(p5);
	Real p6 = vecX[6];
	m_Parameters.push_back(p6);
	Real p7 = vecX[7];
	m_Parameters.push_back(p7);
	Real p8 = vecX[8];
	m_Parameters.push_back(p8);
	Real p9 = vecX[9];
	m_Parameters.push_back(p9);
	Real p10 = vecX[10];
	m_Parameters.push_back(p10);
	Real p11 = vecX[11];
	m_Parameters.push_back(p11);

	return true;
}