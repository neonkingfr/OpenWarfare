//-----------------------------------------------------------------------------
// File: DXF2007Importer.cpp
//
// Desc: Exporter of shape data
//
// Auth: phoboss
//
// Copyright (c) 2000-2008 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#include "DXF2007Importer.h"

//-----------------------------------------------------------------------------

CDXF2007Importer::CDXF2007Importer()
{
}

//-----------------------------------------------------------------------------

CDXF2007Importer::CDXF2007Importer( const string& filename )
{
	Filename( filename );
}

//-----------------------------------------------------------------------------

CDXF2007Importer::~CDXF2007Importer()
{
	if ( !m_in.fail() )
	{
		m_in.close();
	}
}

//-----------------------------------------------------------------------------

void CDXF2007Importer::Filename( const string& filename )
{
	if ( m_in.is_open() )
	{
		m_in.close();
	}

	m_in.open( filename.c_str(), std::ios::in );
	if ( m_in.fail() )
	{
//		wxGetApp().GetErrHandler()->Raise( DCERR_MAJOR, wxString("DXF Importer"),
//										   "Failed to open .dxf file" );
	}
}

//-----------------------------------------------------------------------------

bool CDXF2007Importer::Read()
{
	m_in.seekg( 0 );

	string line;
	while ( !m_in.eof() )
	{
		if ( ReadLine( line ) )
		{
			if ( line == "LWPOLYLINE" )
			{
				if ( !ReadLwPolyline() ) 
				{ 
					return (false); 
				}
			}
		}
		else
		{
			return (false);
		}
	}

	return (true);
}

//-----------------------------------------------------------------------------

const vector< CG_Polygon2< double > >& CDXF2007Importer::GetPolygons() const
{
	return (m_polygonList);
}

//-----------------------------------------------------------------------------

bool CDXF2007Importer::ReadLwPolyline()
{
	bool complete = false;
	bool closed = false;
	size_t verticesCount = 0;
	streamoff pos;
	string line;

	while ( !complete )
	{
		pos = m_in.tellg();
		if ( ReadLine( line ) )
		{
			if ( line == "90" )
			{
				if ( ReadLine( line ) )
				{
					// TODO: check for integer
					verticesCount = atoi( line.c_str() );
				}
				else
				{
					return (false);
				}
			}
			else if ( line == "70" )
			{
				if ( ReadLine( line ) )
				{
					// TODO: check for integer
					int isClosed = atoi( line.c_str() );
					if ( isClosed == 1 )
					{
						closed = true;
					}
				}
				else
				{
					return (false);
				}
			}
			else if ( line == "10" )
			{
				complete = true;
			}
		}
		else
		{
			return (false);
		}
	}

	if ( verticesCount == 0 ) 
	{ 
		return (false); 
	}

	if ( closed )
	{
		return (ReadLwPolylineAsPolygon( pos, verticesCount ));
	}
	else
	{
		return (ReadLwPolylineAsPolyline( pos, verticesCount ));
	}
}

//-----------------------------------------------------------------------------

bool CDXF2007Importer::ReadLwPolylineAsPolygon( streamoff start, size_t verticesCount )
{
	m_in.seekg( start, ios_base::beg );

	CG_Polygon2< double > poly;

	string line;
	for ( size_t i = 0; i < verticesCount; ++i )
	{
		double x, y;

		if ( ReadLine( line ) )
		{
			if ( line != "10" ) { return (false); }
		}
		else
		{
			return (false);
		}

		if ( ReadLine( line ) )
		{
			// TODO: check for floating point
			x = atof( line.c_str() );
		}
		else
		{
			return (false);
		}

		if ( ReadLine( line ) )
		{
			if ( line != "20" ) { return (false); }
		}
		else
		{
			return (false);
		}

		if ( ReadLine( line ) )
		{
			// TODO: check for floating point
			y = atof( line.c_str() );
		}
		else
		{
			return (false);
		}

		poly.AppendVertex( x, y );
	}

	m_polygonList.push_back( poly );

	return (true);
}

//-----------------------------------------------------------------------------

bool CDXF2007Importer::ReadLwPolylineAsPolyline( streamoff start, size_t verticesCount )
{
	m_in.seekg( start, ios_base::beg );

	string line;
	for ( size_t i = 0; i < verticesCount; ++i )
	{
		double x, y;

		if ( ReadLine( line ) )
		{
			if ( line != "10" ) { return (false); }
		}
		else
		{
			return (false);
		}

		if ( ReadLine( line ) )
		{
			// TODO: check for floating point
			x = atof( line.c_str() );
		}
		else
		{
			return (false);
		}

		if ( ReadLine( line ) )
		{
			if ( line != "20" ) { return (false); }
		}
		else
		{
			return (false);
		}

		if ( ReadLine( line ) )
		{
			// TODO: check for floating point
			y = atof( line.c_str() );
		}
		else
		{
			return (false);
		}
	}

	return (true);
}

//-----------------------------------------------------------------------------

bool CDXF2007Importer::ReadLine( string& line )
{
	if ( m_in.eof() ) { return (false); }

	getline( m_in, line );
	if ( line.length() > 0 )
	{
		string::size_type s = line.find_first_not_of( " " );
		string::size_type e = line.find_last_not_of( " " );
		line = line.substr( s, e - s + 1 );
	}

	return (true);
}
