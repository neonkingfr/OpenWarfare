//-----------------------------------------------------------------------------
// File: DXFExporter.cpp
//
// Desc: Exporter of shape data (source)
//
// Auth: phoboss
//
// Copyright (c) 2000-2008 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

//#include "stdafx.h"
//#include "../../Common/Dummy_app.h"
//#include "../../Geometry/Vertex.h"
//#include "../../Geometry/PolylineArea.h"
#include "DXF2007Exporter.h"

//-----------------------------------------------------------------------------

CDXF2007Exporter::CDXF2007Exporter()
{
}

//-----------------------------------------------------------------------------

CDXF2007Exporter::CDXF2007Exporter( const string& filename )
{
	Filename( filename );
}

//-----------------------------------------------------------------------------

CDXF2007Exporter::~CDXF2007Exporter()
{
	if ( !m_out.fail() )
	{
		WriteEntitiesTail();
		WriteFileTail();

		m_out.close();
	}
}

//-----------------------------------------------------------------------------

void CDXF2007Exporter::Filename( const string& filename )
{
	if ( m_out.is_open() )
	{
		m_out.close();
	}

	m_out.open( filename.c_str(), std::ios::out | std::ios::trunc );
	if ( !m_out.fail() )
	{
		WriteEntitiesHead();
	}
	else
	{
//		wxGetApp().GetErrHandler()->Raise( DCERR_MAJOR, wxString("DXF Exporter"),
//										   "Failed to open .dxf file" );
	}
}

//-----------------------------------------------------------------------------

void CDXF2007Exporter::Filename( const char* filename )
{
	Filename( string( filename ) );
}

//-----------------------------------------------------------------------------

void CDXF2007Exporter::WriteEntitiesHead()
{
	if ( !m_out.fail() )
	{
		string out = " 0\nSECTION\n 2\nENTITIES";
		m_out << out << endl;
	}
}

//-----------------------------------------------------------------------------

void CDXF2007Exporter::WriteEntitiesTail()
{
	if ( !m_out.fail() )
	{
		string out = " 0\nENDSEC";
		m_out << out << endl;
	}
}

//-----------------------------------------------------------------------------

void CDXF2007Exporter::WriteFileTail()
{
	if ( !m_out.fail() ) 
	{
		string out = " 0\nEOF";
		m_out << out << endl;
	}
}

//-----------------------------------------------------------------------------

//void CDXF2007Exporter::WritePolyline( CPolylineArea* pPolyline, const wxString& layer,
//								 bool bClose, double dStartingWidth, double dEndingWidth )
//{
//	if( !m_out.fail() )
//	{
//		unsigned int size = pPolyline->m_nCoreVerticesCount;
//
//		if( size > 1 )
//		{
//			char buffer[80];
//			std::string out = " 0\nPOLYLINE\n100\nAcDbEntity\n 8\n";
//			out += (layer + "\n 66\n 1\n100\nAcDb2dPolyline\n 10\n0.0\n 20\n0.0\n30\n0.0\n");
//			if( bClose )
//			{
//				out += " 70\n1\n";
//			}
//			else
//			{
//				out += " 70\n0\n";
//			}
//			_gcvt_s( buffer, 80, dStartingWidth, 8 );
//			out += (" 40\n" + std::string( buffer ) +"\n");
//
//			_gcvt_s( buffer, 80, dEndingWidth, 8 );
//			out += (" 41\n" + std::string( buffer ) +"\n");
//
//			for( unsigned int i = 0; i < size; ++i )
//			{
//				out += " 0\nVERTEX\n100\nAcDbVertex\n 8\n";
//				out += (layer + "\n100\nAcDb2dVertex\n 10\n");
//
//				_gcvt_s( buffer, 80, (double)pPolyline->m_pVertices[i].GetCenter().pos.x, 8 );
//				out += (std::string( buffer ) + "\n 20\n");
//
//				_gcvt_s( buffer, 80, (double)pPolyline->m_pVertices[i].GetCenter().pos.y, 8 );
//				out += (std::string( buffer ) + "\n 30\n0.0\n");
//			}
//			out += " 0\nSEQEND\n 8\n";
//			out += layer;
//			m_out << out << endl;
//		}
//	}
//}

//-----------------------------------------------------------------------------

void CDXF2007Exporter::WritePolygon( const Polygon2& poly, const string& layer,
                                     Float startingWidth, Float endingWidth )
{
	if ( !m_out.fail() )
	{
		size_t size = poly.VerticesCount();

		if ( size > 2 )
		{
			char buffer[80];
			std::string out = " 0\nPOLYLINE\n100\nAcDbEntity\n 8\n";
			out += (layer + "\n 66\n     1\n100\nAcDb2dPolyline\n 10\n0.0\n 20\n0.0\n30\n0.0\n");
			out += " 70\n1\n";

			_gcvt_s( buffer, 80, startingWidth, 16 );
			out += ( " 40\n" + string( buffer ) + "\n" );

			_gcvt_s( buffer, 80, endingWidth, 16 );
			out += ( " 41\n" + std::string( buffer ) + "\n" );

			for ( size_t i = 0; i < size; ++i )
			{
				out += " 0\nVERTEX\n100\nAcDbVertex\n 8\n";
				out += (layer + "\n100\nAcDb2dVertex\n 10\n");

				_gcvt_s( buffer, 80, poly.Vertex( i ).GetX(), 16 );
				out += ( std::string( buffer ) + "\n 20\n" );

				_gcvt_s( buffer, 80, poly.Vertex( i ).GetY(), 16 );
				out += ( std::string( buffer ) + "\n 30\n0.0\n" );
			}
			out += " 0\nSEQEND\n 8\n";
			out += layer;
			m_out << out << endl;
		}
		else
		{
			// TODO: log message skipped polygon
		}
	}
}

//-----------------------------------------------------------------------------

void CDXF2007Exporter::WritePolyline( const Polyline2& poly, const string& layer,
                                      Float startingWidth, Float endingWidth )
{
	if ( !m_out.fail() )
	{
		size_t size = poly.VerticesCount();

		if ( size > 1 )
		{
			char buffer[80];
			std::string out = " 0\nPOLYLINE\n100\nAcDbEntity\n 8\n";
			out += (layer + "\n 66\n     1\n100\nAcDb2dPolyline\n 10\n0.0\n 20\n0.0\n30\n0.0\n");
			out += " 70\n0\n";

			_gcvt_s( buffer, 80, startingWidth, 16 );
			out += ( " 40\n" + string( buffer ) + "\n" );

			_gcvt_s( buffer, 80, endingWidth, 16 );
			out += ( " 41\n" + std::string( buffer ) + "\n" );

			for ( size_t i = 0; i < size; ++i )
			{
				out += " 0\nVERTEX\n100\nAcDbVertex\n 8\n";
				out += (layer + "\n100\nAcDb2dVertex\n 10\n");

				_gcvt_s( buffer, 80, poly.Vertex( i ).GetX(), 16 );
				out += ( std::string( buffer ) + "\n 20\n" );

				_gcvt_s( buffer, 80, poly.Vertex( i ).GetY(), 16 );
				out += ( std::string( buffer ) + "\n 30\n0.0\n" );
			}
			out += " 0\nSEQEND\n 8\n";
			out += layer;
			m_out << out << endl;
		}
		else
		{
			// TODO: log message skipped polygon
		}
	}
}

//-----------------------------------------------------------------------------

void CDXF2007Exporter::WriteCircle( const Point2& center, Float radius,
                                    const string& layer )
{
	if ( !m_out.fail() ) 
	{
		char buffer[80];
		string out = "  0\nCIRCLE\n100\nAcDbEntity\n  8\n";
		out += ( layer + "\n100\nAcDbCircle\n 10\n" );
		_gcvt_s( buffer, 80, center.GetX(), 16 );
		out += ( string(buffer) + "\n 20\n" );
		_gcvt_s( buffer, 80, center.GetY(), 16 );
		out += ( string(buffer) + "\n 40\n" );
		_gcvt_s( buffer, 80, radius, 16 );
		out += string( buffer );
		m_out << out << endl;
	}
}

//-----------------------------------------------------------------------------
