#include "..\include\StdAfx.h"
#include "..\include\EtString.h"

// --------------------------------------------------------------------------------

string EtString::LTrim(const string& s)
{
	// if the string is empty return
	if(s.length() == 0)
	{
		return s;
	}

	// if the first character is not a space or a tab return the string
	if((s[0] != ' ') && (s[0] != '\t'))
	{
		return s;
	}

	unsigned int pos = 0;
	while ((pos < s.length()) && ((s[pos] == ' ') || (s[pos] == '\t')))
	{
		pos++;
	}

	if(pos == s.length())
	{
		// the string is empty (only spaces or tabs)
		return "";
	}
	else
	{
		return s.substr(pos);
	}
}

// --------------------------------------------------------------------------------

string EtString::RTrim(const string& s)
{
	// if the string is empty return
	if(s.length() == 0)
	{
		return s;
	}

	// if the last character is not a space or a tab return the string
	if((s[s.length() - 1] != ' ') && (s[s.length() - 1] != '\t'))
	{
		return s;
	}

	size_t pos = s.length() - 1;
	while ((pos >= 0) && ((s[pos] == ' ') || (s[pos] == '\t')))
	{
		pos--;
	}

	if(pos == -1)
	{
		// the string is empty (only spaces or tabs)
		return "";
	}
	else
	{
		return s.substr(0, pos);
	}

}

// --------------------------------------------------------------------------------

string EtString::RemoveFromStart(const string& s, const string& toBeRemoved)
{
	if(s.find(toBeRemoved, 0) == 0)
	{
		return s.substr(toBeRemoved.size());
	}
	else
	{
		return s;
	}
}
