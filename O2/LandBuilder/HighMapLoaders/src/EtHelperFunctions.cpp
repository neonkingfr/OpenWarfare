#include "..\include\StdAfx.h"
#include "..\include\EtHelperFunctions.h"
#include "..\include\EtString.h"

// -------------------------------------------------------------------------- //

bool IsInteger(const string& strValue, bool acceptEmptyString)
{
	string temp = EtString::LTrim(strValue);
	temp = EtString::RTrim(temp);

	size_t len = temp.size();

	if(len == 0) 
	{
		if(acceptEmptyString)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	string comp = VALIDINTEGER;
	for(unsigned int i = 0; i < len; ++i)
	{
		if(comp.find(temp[i]) == string::npos)
		{
			return false;
		}
	}
	return true;
}

// -------------------------------------------------------------------------- //

bool IsFloatingPoint( const string& strValue, bool acceptEmptyString )
{
	string temp = EtString::LTrim( strValue );
	temp = EtString::RTrim( temp );

	size_t len = temp.size();

	if ( len == 0 ) 
	{
		if ( acceptEmptyString )
		{
			return (true);
		}
		else
		{
			return (false);
		}
	}

	string comp = VALIDFLOAT;
	for ( size_t i = 0; i < len; ++i )
	{
		if ( comp.find( temp[i] ) == string::npos )
		{
			return (false);
		}
	}
	return (true);
}

// -------------------------------------------------------------------------- //

bool IsBool(const string& strValue, bool acceptEmptyString)
{
	string temp = EtString::LTrim(strValue);
	temp = EtString::RTrim(temp);

	size_t len = temp.size();

	if(len == 0) 
	{
		if(acceptEmptyString)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	if((temp == "0") || (temp == "1"))
	{
		return true;
	}
	else
	{
		return false;
	}
}

// -------------------------------------------------------------------------- //
