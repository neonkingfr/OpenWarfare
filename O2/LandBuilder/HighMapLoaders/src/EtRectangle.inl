// ************************************************************************** //
// Default constructor                                                        //
// ************************************************************************** //

template <class Real>
EtRectangle<Real>::EtRectangle()
{
}

// ************************************************************************** //
// Other constructors                                                         //
// ************************************************************************** //

template <class Real>
EtRectangle<Real>::EtRectangle(Real left, Real top, Real right, Real bottom)
: m_Left(left)
, m_Top(top)
, m_Right(right)
, m_Bottom(bottom)
{
}

// ************************************************************************** //
// Copy constructor                                                           //
// ************************************************************************** //

template <class Real>
EtRectangle<Real>::EtRectangle(const EtRectangle<Real>& other)
: m_Left(other.m_Left)
, m_Top(other.m_Top)
, m_Right(other.m_Right)
, m_Bottom(other.m_Bottom)
{
}

// ************************************************************************** //
// Assignement                                                                //
// ************************************************************************** //

template <class Real>
EtRectangle<Real>& EtRectangle<Real>::operator = (const EtRectangle<Real>& other)
{
	m_Left   = other.m_Left;
	m_Top    = other.m_Top;
	m_Right  = other.m_Right;
	m_Bottom = other.m_Bottom;

	return *this;
}

// ************************************************************************** //
// Members getters                                                            //
// ************************************************************************** //

template <class Real>
Real EtRectangle<Real>::GetLeft() const
{
	return m_Left;
}

// -------------------------------------------------------------------------- //

template <class Real>
Real EtRectangle<Real>::GetRight() const
{
	return m_Right;
}

// -------------------------------------------------------------------------- //

template <class Real>
Real EtRectangle<Real>::GetTop() const
{
	return m_Top;
}

// -------------------------------------------------------------------------- //

template <class Real>
Real EtRectangle<Real>::GetBottom() const
{
	return m_Bottom;
}

// ************************************************************************** //
// Rectangle properties                                                       //
// ************************************************************************** //

template <class Real>
Real EtRectangle<Real>::Width() const
{
	return (right - left);
}

// -------------------------------------------------------------------------- //

template <class Real>
Real EtRectangle<Real>::Height() const
{
	return (top - bottom);
}

// -------------------------------------------------------------------------- //

template <class Real>
Real EtRectangle<Real>::Area() const
{
	return (Width() * Heigth());
}

// -------------------------------------------------------------------------- //

template <class Real>
bool EtRectangle<Real>::Contains(Real x, Real y) const
{
	if (left <= x && x <= right)
	{
		if (bottom <= y && y <= top)
		{
			return true;
		}
	}
	return false;
}