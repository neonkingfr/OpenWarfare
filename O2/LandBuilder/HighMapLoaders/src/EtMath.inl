//----------------------------------------------------------------------------

// returns the absolute value of the given value
template <class Real>
Real EtMath<Real>::Fabs(Real value)
{
	return static_cast<Real>(fabs(static_cast<double>(value)));
}

//----------------------------------------------------------------------------

// returns the atan of the given angle (in radians)
template <class Real>
Real EtMath<Real>::Tan(Real angleRAD)
{
	return static_cast<Real>(tan(static_cast<double>(angleRAD)));
}

//----------------------------------------------------------------------------

// returns the atan of the given angle (in radians)
template <class Real>
Real EtMath<Real>::Atan(Real angleRAD)
{
	return static_cast<Real>(atan(static_cast<double>(angleRAD)));
}

// --------------------------------------------------------------------------------

// returns the sin of the given angle (in radians)
template <class Real>
Real EtMath<Real>::Sin(Real angleRAD)
{
    return static_cast<Real>(sin(static_cast<double>(angleRAD)));
}

// --------------------------------------------------------------------------------

// returns the cos of the given angle (in radians)
template <class Real>
Real EtMath<Real>::Cos(Real angleRAD)
{
    return static_cast<Real>(cos(static_cast<double>(angleRAD)));
}

// --------------------------------------------------------------------------------

// returns the ArcSin of the given value
template <class Real>
Real EtMath<Real>::ASin(Real value)
{
    if (-(static_cast<Real>(1.0)) < value)
    {
        if (value < static_cast<Real>(1.0))
        {
            return static_cast<Real>(asin(static_cast<double>(value)));
        }
        else
        {
            return (PI / static_cast<Real>(2.0));
        }
    }
    else
    {
        return -(PI / static_cast<Real>(2.0));
    }
}

// --------------------------------------------------------------------------------

// returns the ArcCos of the given value
template <class Real>
Real EtMath<Real>::ACos(Real value)
{
    if (-(static_cast<Real>(1.0)) < value)
    {
        if (value < static_cast<Real>(1.0))
        {
            return static_cast<Real>(acos(static_cast<double>(value)));
        }
        else
        {
            return static_cast<Real>(0.0);
        }
    }
    else
    {
        return PI;
    }
}

// --------------------------------------------------------------------------------

// returns the squared root of the given value
template <class Real>
Real EtMath<Real>::Sqrt(Real value)
{
    return static_cast<Real>(sqrt(static_cast<double>(value)));
}

// --------------------------------------------------------------------------------

// returns the given base elevated at the given exponent
template <class Real>
Real EtMath<Real>::Pow(Real base, Real exponent)
{
	return static_cast<Real>(pow(static_cast<double>(base), static_cast<double>(exponent)));
}

// --------------------------------------------------------------------------------

// return the angle (in radians) in the plane XY between the segment 
// whose ending point are specified by the given coordinates 
// (p1 = starting point, p2 = ending point) and the x axis in counterclockwise direction
template <class Real>
Real EtMath<Real>::SegmentDirection_XY_CCW_X(Real p1X, Real p1Y, Real p2X, Real p2Y)
{
	Real dx = p2X - p1X; 
	Real dy = p2Y - p1Y; 

	if(dy == static_cast<Real>(0.0))
	{
		if(dx >= static_cast<Real>(0.0))
		{
			return static_cast<Real>(0.0);
		}
		else
		{
			return PI;
		}
	}

	if(dx == static_cast<Real>(0.0))
	{
		if(dy > static_cast<Real>(0.0))
		{
			return (PI / static_cast<Real>(2.0));
		}
		else
		{
			return (static_cast<Real>(3.0 / 2.0) * PI);
		}
	}

	Real angle = Atan(Fabs(dy / dx));

	if(dx > static_cast<Real>(0.0))
	{
		if(dy > static_cast<Real>(0.0))
		{
			return angle;
		}
		else
		{
			return (static_cast<Real>(2.0) * PI - angle);
		}
	}
	else
	{
		if(dy > static_cast<Real>(0.0))
		{
			return (PI - angle);
		}
		else
		{
			return (PI + angle);
		}
	}
}

// --------------------------------------------------------------------------------

// returns the given angle (in radians) converted in the 1st period
// [0..2 * PI]
template <class Real>
Real EtMath<Real>::ToFirstPeriod(Real angle)
{
	while(angle > static_cast<Real>(2.0) * PI)
	{
		angle -= static_cast<Real>(2.0) * PI;
	}
	while(angle < static_cast<Real>(0.0))
	{
		angle += static_cast<Real>(2.0) * PI;
	}
	return angle;
}

// --------------------------------------------------------------------------------

// returns the bilinear interpolation of the given parameters
// f00 value of function in 0,0
// f10 value of function in 1,0
// f01 value of function in 0,1
// f11 value of function in 1,1
// x and y MUST be in the range [0..1]
template < class Real >
Real EtMath< Real >::InterpolateBilinear( Real f00, Real f10, Real f01, Real f11, Real x, Real y )
{
  Real one = static_cast< Real >( 1 );
	return (f00 * (one - x) * (one - y) + f10 * x * (one - y) + f01 * (one - x) * y + f11 * x * y);
}

// --------------------------------------------------------------------------------

// converts the angle given in radians to degree
template <class Real>
Real EtMath<Real>::RadToDeg(Real angle)
{
	return angle * static_cast<Real>(180.0) / PI;
}

// --------------------------------------------------------------------------------

// converts the angle given in degrees to radians
template < class Real >
Real EtMath< Real >::DegToRad( Real angle )
{
	return angle * PI / static_cast< Real >( 180.0 );
}
