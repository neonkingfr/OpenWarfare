#include "..\include\StdAfx.h"
#include "..\include\EtMath.h"
#include <math.h>
#include <float.h>

// --------------------------------------------------------------------------------
// float specialization
// --------------------------------------------------------------------------------
template<> const float EtMath<float>::PI = static_cast<float>(4.0 * atan(1.0));
template<> const float EtMath<float>::TWO_PI = 2.0f * PI;
template<> const float EtMath<float>::ZERO_TOLERANCE = 1e-06f;
template<> const float EtMath<float>::MAX_REAL = FLT_MAX;

template<> const float EtMath<float>::ZERO = 0.0f;
template<> const float EtMath<float>::ONE  = 1.0f;
template<> const float EtMath<float>::TEN  = 10.0f;

// --------------------------------------------------------------------------------
// double specialization
// --------------------------------------------------------------------------------
template<> const double EtMath<double>::PI = 4.0 * atan(1.0);
template<> const double EtMath<double>::TWO_PI = 2.0 * PI;
template<> const double EtMath<double>::ZERO_TOLERANCE = 1e-08;
template<> const double EtMath<double>::MAX_REAL = DBL_MAX;

template<> const double EtMath<double>::ZERO = 0.0;
template<> const double EtMath<double>::ONE  = 1.0;
template<> const double EtMath<double>::TEN  = 10.0;
