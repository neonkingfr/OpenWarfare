//----------------------------------------------------------------------------

// returns the factorial of the given integer number
template <class Integer>
Integer EtMathInt<Integer>::Factorial(Integer value)
{
	if(value <= static_cast<Integer>(0)) return static_cast<Integer>(1);

	Integer a = static_cast<Integer>(1);

	for(int i = 1; i <= value; ++i)
	{
		a *= i;
	}
	return a;
}

//----------------------------------------------------------------------------

// returns the double factorial of the given integer number
template <class Integer>
Integer EtMathInt<Integer>::DoubleFactorial(Integer value)
{
	if(value <= static_cast<Integer>(0)) return static_cast<Integer>(1);
	if(value == static_cast<Integer>(1)) return static_cast<Integer>(1);

	Integer a;

	if((value % 2) == 0)
	// value is even
	{
		a = static_cast<Integer>(2);
	}
	else
	// value is odd
	{
		a = static_cast<Integer>(1);
	}

	for(int i = a + 2; i <= value; i += 2)
	{
		a *= i;
	}

	return a;
}

//----------------------------------------------------------------------------

// returns the power of 2 with the given integer exponent
template <class Integer>
Integer EtMathInt<Integer>::PowerOfTwo(Integer value)
{
	if(value <= static_cast<Integer>(0)) return static_cast<Integer>(1);

	Integer a = static_cast<Integer>(2);

	for(int i = 2; i <= value; ++i)
	{
		a *= 2;
	}
	return a;
}