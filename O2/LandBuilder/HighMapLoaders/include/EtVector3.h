// ************************************************************************** //
// file          : ETLibs\Math\EtVector3.h                                    //
//                                                                            //
// class         : EtVector3                                                  //
// ************************************************************************** //
// description   : Three dimensional vector in cartesian coordinates.         //
//                 The vector is specyfied by its three components.           //
//                                                                            //
// ************************************************************************** //
// Author        : Enrico Turri                                               //
//                                                                            //
// since         : 13.01.2007                                                 //
// last revision : 21.01.2007                                                 //
// ************************************************************************** //
// known bugs    :                                                            //
// ************************************************************************** //
#ifndef ETVECTOR3_H
#define ETVECTOR3_H

#include <iostream>

using std::ostream;

template <class Real> class EtPoint3;
template <class Real> class EtMatrix3x3;

template <class Real>
class EtVector3
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// ---------------------------- //
	// the components of the vector //
	// ---------------------------- //
	union 
	{
		struct 
		{
			Real m_X;
			Real m_Y;
			Real m_Z;
		};
		Real m_XYZ[3];
	};

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //

	// ------------------- //
	// Default constructor //
	// ------------------- //
	EtVector3();

	// ------------------------- //
	// Constructs a new vector   //
	// with the given parameters //
	// ------------------------- //
	EtVector3(Real x, Real y, Real z);

	EtVector3(const Real xyz[3]);

	// ------------------------------- //
	// Constructs the vector (p2 - p1) //
	// ------------------------------- //
	EtVector3(const EtPoint3<Real>& p1, const EtPoint3<Real>& p2);

	// ---------------- //
	// Copy constructor //
	// ---------------- //
	EtVector3(const EtVector3<Real>& other);

	// *********** //
	// Assignement //
	// *********** //
	EtVector3<Real>& operator = (const EtVector3<Real>& other);

	// ********** //
	// Comparison //
	// ********** //
	bool operator == (const EtVector3<Real>& other) const;
	bool operator != (const EtVector3<Real>& other) const;

	// ********************* //
	// Arithmetic operations //
	// ********************* //
	EtVector3<Real> operator + (const EtVector3<Real>& other) const;
	EtVector3<Real> operator - (const EtVector3<Real>& other) const;
	EtVector3<Real> operator * (const EtMatrix3x3<Real>& matrix) const;
	EtVector3<Real> operator * (Real scalar) const;
	// ---------------------------------------------------------- //
	// returns the dot product between this and the given vectors //
	// ---------------------------------------------------------- //
	Real operator * (const EtVector3<Real>& other) const;
	EtVector3<Real> operator / (Real scalar) const;

	// ****************** //
	// Arithmetic updates //
	// ****************** //
	EtVector3<Real>& operator += (const EtVector3<Real>& other);
	EtVector3<Real>& operator -= (const EtVector3<Real>& other);
	EtVector3<Real>& operator *= (Real scalar);
	EtVector3<Real>& operator /= (Real scalar);

	// ********************* //
	// Negation              //
	// ********************* //
	EtVector3<Real> operator - () const;

	// ***************** //
	// Vector properties //
	// ***************** //

	// --------------------------------- //
	// Length                            //
	// returns the length of this vector //
	// --------------------------------- //
	Real Length() const;

	// --------------------------------- //
	// Size                              //
	// returns the length of this vector //
	// --------------------------------- //
	Real Size() const;

	// --------------------------------- //
	// Magnitude                         //
	// returns the length of this vector //
	// --------------------------------- //
	Real Magnitude() const;

	// ----------------------------------------- //
	// SquaredLength                             //
	// returns the squared length of this vector //
	// ----------------------------------------- //
	Real SquaredLength() const;

	// ***************** //
	// Vector operations //
	// ***************** //

	// ---------------------- //
	// Normalize              //
	// normalizes this vector //
	// ---------------------- //
	void Normalize();

	// ---------------------------------------------------------- //
	// Dot                                                        //
	// returns the dot product between this and the given vectors //
	// ---------------------------------------------------------- //
	Real Dot(const EtVector3<Real>& other) const;

	// ------------------------------------------------------------ //
	// Cross                                                        //
	// returns the cross product between this and the given vectors //
	// ------------------------------------------------------------ //
	// uses right hand rule                                         //
	// ------------------------------------------------------------ //
	EtVector3<Real> Cross(const EtVector3<Real>& other) const;

	// -------------------------------------------------- //
	// ProjectInto                                        //
	// returns the vector obtained projecting this vector //
	// into the given vector                              //
	// -------------------------------------------------- //
	EtVector3<Real> ProjectInto(const EtVector3<Real>& other) const;

	// ---------------------------------------------------- //
	// ProjectIntoPerpTo                                    //
	// returns the vector obtained projecting this vector   //
	// into the perpendicular direction to the given vector //
	// ---------------------------------------------------- //
	EtVector3<Real> ProjectIntoPerpTo(const EtVector3<Real>& other) const;

	// ************************ //
	// Member variables getters //
	// ************************ //

	// ************************************** //
	// GetX                                   //
	// returns the X component of this vector //
	// ************************************** //
	Real GetX() const;		

	// ************************************** //
	// X                                      //
	// returns the X component of this vector //
	// ************************************** //
	Real X() const;		

	// ************************************** //
	// GetY                                   //
	// returns the Y component of this vector //
	// ************************************** //
	Real GetY() const;		

	// ************************************** //
	// Y                                      //
	// returns the Y component of this vector //
	// ************************************** //
	Real Y() const;		

	// ************************************** //
	// GetZ                                   //
	// returns the Z component of this vector //
	// ************************************** //
	Real GetZ() const;		

	// ************************************** //
	// Z                                      //
	// returns the Z component of this vector //
	// ************************************** //
	Real Z() const;		

	// ************************ //
	// Member variables setters //
	// ************************ //

	// ************************************** //
	// Set                                    //
	// sets the components of this vector     //
	// ************************************** //
	void Set(Real newX, Real newY, Real newZ);		

	// ************************************** //
	// SetX                                   //
	// sets the X component of this vector //
	// ************************************** //
	void SetX(Real newX);		

	// ************************************** //
	// X                                      //
	// sets the X component of this vector //
	// ************************************** //
	void X(Real newX);		

	// ************************************** //
	// SetY                                   //
	// sets the Y component of this vector //
	// ************************************** //
	void SetY(Real newY);		

	// ************************************** //
	// Y                                      //
	// sets the Y component of this vector //
	// ************************************** //
	void Y(Real newY);		

	// ************************************** //
	// SetZ                                   //
	// sets the Z component of this vector //
	// ************************************** //
	void SetZ(Real newZ);		

	// ************************************** //
	// Z                                      //
	// sets the Z component of this vector //
	// ************************************** //
	void Z(Real newZ);		
};

// ******************************** //
// Overloaded arithmetic operations //
// ******************************** //
template <class Real>
EtVector3<Real> operator * (Real scalar, const EtVector3<Real>& v);

// ************************ //
// Serialization and debug  //
// ************************ //
template <class Real>
ostream& operator << (ostream& stream, const EtVector3<Real>& v);

#include "..\src\EtVector3.inl"

typedef EtVector3<float>  EtVector3F;
typedef EtVector3<double> EtVector3D;

#endif