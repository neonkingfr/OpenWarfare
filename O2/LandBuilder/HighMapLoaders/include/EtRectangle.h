#ifndef ETRECTANGLE_H
#define ETRECTANGLE_H

template <class Real>
class EtRectangle
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// -------------------------- //
	// the edges of the rectangle //
	// -------------------------- //
	Real m_Left;
	Real m_Top;
	Real m_Right;
	Real m_Bottom;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ************ //
	// Constructors //
	// ************ //

	// ------------------- //
	// Default constructor //
	// ------------------- //
	EtRectangle();

	// -------------------------- //
	// Constructs a new rectangle //
	// with the given parameters  //
	// -------------------------- //
	EtRectangle(Real left, Real top, Real right, Real bottom);

	// ---------------- //
	// Copy constructor //
	// ---------------- //
	EtRectangle(const EtRectangle<Real>& other);

	// *********** //
	// Assignement //
	// *********** //
	EtRectangle<Real>& operator = (const EtRectangle<Real>& other);

	// *************** //
	// Members getters //
	// *************** //

	// --------------------------------------- //
	// GetLeft()                               //
	// returns the left edge of this rectangle //
	// --------------------------------------- //
	Real GetLeft() const;

	// ---------------------------------------- //
	// GetRight()                               //
	// returns the right edge of this rectangle //
	// ---------------------------------------- //
	Real GetRight() const;

	// -------------------------------------- //
	// GetTop()                               //
	// returns the top edge of this rectangle //
	// -------------------------------------- //
	Real GetTop() const;

	// ----------------------------------------- //
	// GetBottom()                               //
	// returns the bottom edge of this rectangle //
	// ----------------------------------------- //
	Real GetBottom() const;

	// ******************** //
	// Rectangle properties //
	// ******************** //

	// ----------------------------------- //
	// Width()                             //
	// returns the width of this rectangle //
	// ----------------------------------- //
	Real Width() const;

	// ------------------------------------ //
	// Height()                             //
	// returns the height of this rectangle //
	// ------------------------------------ //
	Real Height() const;

	// ---------------------------------- //
	// Area()                             //
	// returns the area of this rectangle //
	// ---------------------------------- //
	Real Area() const;

	// --------------------------------------- //
	// Contains()                              //
	// returns true if this rectangle contains //
	// the given point                         //
	// --------------------------------------- //
	bool Contains(Real x, Real y) const;
};

#include "..\src\EtRectangle.inl"

#endif