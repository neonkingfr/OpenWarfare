#ifndef ETUNITSCONVERTER_H
#define ETUNITSCONVERTER_H

template <class Real>
class EtUnitsConverter
{
public:
	static const Real ANG_DEG_TO_SEC;
	static const Real ANG_DEG_TO_RAD;
	static const Real ANG_MIN_TO_SEC;
	static const Real ANG_RAD_TO_DEG;
	static const Real ANG_RAD_TO_SEC;
	static const Real ANG_SEC_TO_RAD;
	static const Real LEN_FEET_TO_METERS;
	static const Real LEN_METERS_TO_FEET;
};

typedef EtUnitsConverter<float>  EtUnitsConverterF;
typedef EtUnitsConverter<double> EtUnitsConverterD;

#endif