#ifndef ETMATH_H
#define ETMATH_H

#include <complex>
#include <cmath>

template <class Real>
class EtMath
{
public:
	static const Real PI;
	static const Real TWO_PI;
    static const Real ZERO_TOLERANCE;
	static const Real MAX_REAL;

	static const Real ZERO;
	static const Real ONE;
	static const Real TEN;

	// returns the absolute value of the given value
	static Real Fabs(Real value);
	// returns the atan of the given angle (in radians)
    static Real Tan(Real angleRAD);
	// returns the tan of the given angle (in radians)
    static Real Atan(Real angleRAD);
	// returns the sin of the given angle (in radians)
	static Real Sin(Real angleRAD);
	// returns the cos of the given angle (in radians)
    static Real Cos(Real angleRAD);
	// returns the ArcSin of the given value
	static Real ASin(Real value);
	// returns the ArcCos of the given value
	static Real ACos(Real value);

	// returns the squared root of the given value
	static Real Sqrt(Real value);

	// returns the given base elevated at the given exponent
	static Real Pow(Real base, Real exponent);

	// return the angle (in radians) in the plane XY between the segment 
	// whose ending point are specified by the given coordinates 
	// (p1 = starting point, p2 = ending point) and the x axis in counterclockwise direction
	static Real SegmentDirection_XY_CCW_X(Real p1X, Real p1Y, Real p2X, Real p2Y);

	// returns the given angle (in radians) converted in the 1st period
	// [0..2 * PI]
	static Real ToFirstPeriod(Real angle);

	// Returns the bilinear interpolation of the function having:
	// f00 value of function in 0,0
	// f10 value of function in 1,0
	// f01 value of function in 0,1
	// f11 value of function in 1,1
	// in the point (x, y)
	// x and y MUST be in the range [0..1]
	static Real InterpolateBilinear( Real f00, Real f10, Real f01, Real f11, Real x, Real y );

	// converts the angle given in radians to degree
	static Real RadToDeg(Real angle);

	// converts the angle given in degrees to radians
	static Real DegToRad(Real angle);
};

#include "..\src\EtMath.inl"

typedef EtMath<float>  EtMathF;
typedef EtMath<double> EtMathD;

#endif