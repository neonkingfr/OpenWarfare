#ifndef ETHELPERFUNCTIONS_H
#define ETHELPERFUNCTIONS_H

#include <string>

using std::string;

#define VALIDINTEGER "0123456789+-"
#define VALIDFLOAT   "0132456789+-.eE"

// returns true if the given string contain an integer
bool IsInteger(const string& strValue, bool acceptEmptyString);

// returns true if the given string contain a floating point number (float or double)
bool IsFloatingPoint(const string& strValue, bool acceptEmptyString);

// returns true if the given string contain 0 or 1
bool IsBool(const string& strValue, bool acceptEmptyString);

#endif