// -------------------------------------------------------
// class EtRectElevationGrid
//
// represents a rectanglar grid where in every node is
// specified an elevation
// 
// the grid is assumed to have its origin (u = 0, v = 0, w = 0) in the SW corner
// u increases from left to right 
// v increases from bottom to top
// w increases in the up direction
// the grid can be on every surface: plane, geoid, and so on
// -------------------------------------------------------

#ifndef ETRECTELEVATIONGRID_H
#define ETRECTELEVATIONGRID_H

#include <string>

using std::string;

#define NO_ELEVATION -32767

template <class Real, class Integer>
class EtRectElevationGrid
{
public:
	// enumerators for units
	enum SurfaceUnits   
	{ 
		suNULL, 
		suRADIANS, 
		suFEET, 
		suMETERS, 
		suSECONDS 
	};
	enum ElevationUnits 
	{ 
		euNULL, 
		euFEET, 
		euMETERS 
	};
	// enumerator for resampling methods
	enum ResamplingMethods 
	{ 
		rmLINEAR 
	};
	// enumerator for interpolation methods
	enum InterpolationMethods 
	{ 
		imBILINEAR,
		imARMA
	};

protected:

	// ----------------------------------------------
	// INTERNAL DATA
	// ----------------------------------------------

	// number of u nodes
	size_t m_SizeU;
	// number of v nodes
	size_t m_SizeV;
	// number of w nodes
	size_t m_SizeW;

	// the array containing the elevation data
	Integer* m_W;

	// ----------------------------------------------
	// WORLD DATA
	// ----------------------------------------------

	// u resolution (internode distance in m_WorldUnitsU)
	Real m_ResolutionU;
	// v resolution (internode distance in m_WorldUnitsV)
	Real m_ResolutionV;
	// w resolution (internode distance in m_WorldUnitsW)
	Real m_ResolutionW;

	// the u World units
	SurfaceUnits m_WorldUnitsU;
	// the v World units
	SurfaceUnits m_WorldUnitsV;
	// the w World units
	ElevationUnits m_WorldUnitsW;

	// the World u coordinate of origin
	Real m_OriginWorldU;
	// the World v coordinate of origin
	Real m_OriginWorldV;
	// the World w coordinate of origin
	Real m_OriginWorldW;

public:
	// ----------------------------------------------
	// CONSTRUCTORS
	// ----------------------------------------------

	// constructs an empty grid
	EtRectElevationGrid();

	// constructs an empty grid with the specified u and v sizes
	EtRectElevationGrid( unsigned int sizeU, unsigned int sizeV );

	// ----------------------------------------------
	// COPY CONSTRUCTOR
	// ----------------------------------------------

	EtRectElevationGrid( const EtRectElevationGrid< Real, Integer >& other );

	// ----------------------------------------------
	// DESTRUCTOR
	// ----------------------------------------------

	~EtRectElevationGrid();

	// ----------------------------------------------
	// ASSIGNEMENT
	// ----------------------------------------------

	EtRectElevationGrid< Real, Integer >& operator = ( const EtRectElevationGrid< Real, Integer >& other );

	// ----------------------------------------------
	// INTERNAL DATA METHODS
	// ----------------------------------------------

	// sets the u and v grid sizes
	// if the grid is not empty deletes all old data
	// return false if the memory allocation fails
	// if fillW is true the elevation array is filled with the NO_ELEVATION value
	bool setUVSize( size_t extU, size_t extV, bool fillW = true );
	// swaps u and v coordinates of this grid
	// if worldToo is true swaps also World origin, units and resolutions u and v
	void swapUV( bool worldToo );
	// mirrors the ws on u coordinate
	void mirrorU();
	// mirrors the ws on v coordinate
	void mirrorV();

	// returns the size (number of nodes) in u
	size_t getSizeU() const;

  // returns the size (number of nodes) in v
	size_t getSizeV() const;

  // returns the size (number of nodes) in w
	size_t getSizeW() const;

	// returns the w at node (u, v) of the grid
	Integer getWAt( size_t u, size_t v ) const;

  // sets the w at node (u, v) of the grid
	// no resampling of the elevation array is done in this method
	// so the method normalizeW() should be called after the call to 
	// this method
	void setWAt( size_t u, size_t v, Integer newElevation );

	// sets the w at node (u, v) of the grid 
	// no resampling of the elevation array is done in this method
	// so the method normalizeW() should be called after the call to 
	// this method
	void setWorldWAt( size_t u, size_t v, Real worldElevation );

  // sets all the ws to the same value
	void setAllWToSingleValue( Integer value );

  // sets all the ws to the same value
	void setAllWorldWToSingleValue( Real value );

//protected:
public:
	// returns the first non NO_ELEVATION w
	// assumes that the ws are normalized
	// returns NO_ELEVATION only if all the w are NO_ELEVATION
	Integer getFirstValidW() const;
	// returns the min valid w (always equal to 0)
	// assumes that the ws are normalized
	// returns NO_ELEVATION only if all the w are NO_ELEVATION
	Integer getMinValidW() const;
	// returns the max valid w (always equal to (m_SizeW - 1))
	// assumes that the ws are normalized
	// returns NO_ELEVATION only if all the w are NO_ELEVATION
	Integer getMaxValidW() const;

	// normalize all the ws to be in the range [0..(m_SizeW -1)]
	// updates also origin World w
	void normalizeW();

	// offsets the ws of the sub grid of this grid whose
	// SW and NE corners have the specified coordinates
	// by the specified value
	void offsetW( const size_t swCornerU, const size_t swCornerV, const size_t neCornerU, const size_t neCornerV, const Integer value ) const;
	// adds the ws of the specified grid to the ones of this grid
	// in the area where the 2 grids overlap
	// the values offsetU and offsetV allow to place the origin
	// of the specified grid in the desired position with respect to the
	// origin of this grid
	// no addition take places where there are NO_ELEVATION values in either grid
	void addW( const int offsetU, const int offsetV, const EtRectElevationGrid& other );

	// ----------------------------------------------
	// WORLD DATA METHODS
	// ----------------------------------------------

public:
	// returns the World units of u coordinate
	int getWorldUnitsU() const;
	// returns the World units of v coordinate
	int getWorldUnitsV() const;
	// returns the World units of w coordinate
	int getWorldUnitsW() const;

	// returns the World units of u coordinate as string
	string getWorldUnitsUAsString() const;
	// returns the World units of v coordinate as string
	string getWorldUnitsVAsString() const;
	// returns the World units of w coordinate as string
	string getWorldUnitsWAsString() const;

	// sets the World units of u coordinate
	// only setting (no conversion)
	void setWorldUnitsU( const SurfaceUnits newWorldUnitsU );
	// sets the World units of v coordinate
	// only setting (no conversion)
	void setWorldUnitsV( const SurfaceUnits newWorldUnitsV );
	// sets the World units of w coordinate
	// only setting (no conversion)
	void setWorldUnitsW( const ElevationUnits newWorldUnitsW );

	// converts the World units of u coordinate
	// to the specified units
	// currently implemented:
	// suMETERS <-> suFEET
	// suRADIANS <-> suSECONDS
	void convertWorldUnitsU( const SurfaceUnits newWorldUnitsU );
	// converts the World units of v coordinate
	// to the specified units
	// currently implemented:
	// suMETERS <-> suFEET
	// suRADIANS <-> suSECONDS
	void convertWorldUnitsV( const SurfaceUnits newWorldUnitsV );
	// converts the World units of w coordinate
	// to the specified units
	// currently implemented:
	// euMETERS <-> euFEET
	void convertWorldUnitsW( const ElevationUnits newWorldUnitsW );

	// returns the resolution of u coordinate
	Real getResolutionU() const;
	// returns the resolution of v coordinate
	Real getResolutionV() const;
	// returns the resolution of w coordinate
	Real getResolutionW() const;

	// sets the resolution of u coordinate
	void setResolutionU( const Real newResolutionU );
	// sets the resolution of v coordinate
	void setResolutionV( const Real newResolutionV );
	// sets the resolution of w coordinate
	void setResolutionW( const Real newResolutionW );

	// returns the World u coordinate of origin
	Real getOriginWorldU() const;
	// returns the World v coordinate of origin
	Real getOriginWorldV() const;
	// returns the World w coordinate of origin
	Real getOriginWorldW() const;

	// sets the World u coordinate of origin
	void setOriginWorldU( const Real newU );
	// sets the World v coordinate of origin
	void setOriginWorldV( const Real newV );
	// sets the World w coordinate of origin
	void setOriginWorldW( const Real newW );
	// sets the World origin
	void setOriginWorldUVW( const Real newU, const Real newV, const Real newW );

	// offsets the World u coordinate of origin with the specified value
	void offsetOriginWorldU( const Real offsetU );
	// offsets the World v coordinate of origin with the specified value
	void offsetOriginWorldV( const Real offsetV );
	// offsets the World w coordinate of origin with the specified value
	void offsetOriginWorldW( const Real offsetW );
	// offsets the World origin with the specified values
	void offsetOriginWorldUVW( const Real offsetU, const Real offsetV, const Real offsetW );

	// returns the World u at the specified u on grid
	Real getWorldUAt( size_t u ) const;

  // returns the World v at the specified v on grid
	Real getWorldVAt( size_t v ) const;

  // returns the World w at the specified w on grid
	Real getWorldWAt( size_t w ) const;

  // returns the World w at node (u, v) of the grid
	Real getWorldWAt( size_t u, size_t v ) const;

  // returns the World w at the point (worldU, worldV) of the grid
	Real getWorldWAt( Real worldU, Real worldV, InterpolationMethods method, bool clamp = false ) const;

	// returns the World slope at the point (worldU, worldV) of the grid
	// 1.0 (= 100%) corresponds to an angle of 45 degrees
	Real getWorldSlopeAt( const Real worldU, const Real worldV, const InterpolationMethods method ) const;

	// returns the minimum World u on this grid
	Real getMinWorldU() const;
	// returns the maximum World u on this grid
	Real getMaxWorldU() const;
	// returns the minimum World v on this grid
	Real getMinWorldV() const;
	// returns the maximum World v on this grid
	Real getMaxWorldV() const;
	// returns the minimum World w on this grid
	Real getMinWorldW() const;
	// returns the maximum World w on this grid
	Real getMaxWorldW() const;

	// returns the u size of this grid in World units
	Real getWorldSizeU() const;
	// returns the v size of this grid in World units
	Real getWorldSizeV() const;
	// returns the w size of this grid in World units
	Real getWorldSizeW() const;

public:
	// ----------------------------------------------
	// OPERATIONS ON GRID
	// ----------------------------------------------
	// returns the sub grid of this grid whose
	// SW and NE corners have the specified coordinates
	EtRectElevationGrid<Real, Integer> subGrid( size_t swCornerU, size_t swCornerV, size_t neCornerU, size_t neCornerV ) const;

	// returns the sub grid of this grid whose
	// SW and NE corners have the specified World coordinates
	EtRectElevationGrid< Real, Integer > subGrid( Real swCornerWorldU, Real swCornerWorldV, Real neCornerWorldU, Real neCornerWorldV ) const;

  // returns the grid obtained by merging this grid to the specified one
	// placing the second to the right of the first
	// all world quantities of the new grid are inherited from this grid
	// even if the second has different ones
	// if offsetV is specified the specified grid will be shifted in V direction
	EtRectElevationGrid<Real, Integer> mergeRight(const EtRectElevationGrid<Real, Integer>& other, const int offsetV = 0) const;
	// returns the grid obtained by merging this grid to the specified one
	// placing the second to the left of the first
	// all world quantities of the new grid are inherited from this grid
	// even if the second has different ones
	// if offsetV is specified the specified grid will be shifted in V direction
	EtRectElevationGrid<Real, Integer> mergeLeft(const EtRectElevationGrid<Real, Integer>& other, const int offsetV = 0) const;
	// returns the grid obtained by merging this grid to the specified one
	// placing the second to the top of the first
	// all world quantities of the new grid are inherited from this grid
	// even if the second has different ones
	// if offsetU is specified the specified grid will be shifted in U direction
	EtRectElevationGrid<Real, Integer> mergeTop(const EtRectElevationGrid<Real, Integer>& other, const int offsetU = 0) const;
	// returns the grid obtained by merging this grid to the specified one
	// placing the second to the bottom of the first
	// all world quantities of the new grid are inherited from this grid
	// even if the second has different ones
	// if offsetU is specified the specified grid will be shifted in U direction
	EtRectElevationGrid<Real, Integer> mergeBottom(const EtRectElevationGrid<Real, Integer>& other, const int offsetU = 0) const;

	// returns a grid obtained by resampling the u of this grid
	EtRectElevationGrid<Real, Integer> resampleU(const ResamplingMethods method, const Real newResolutionU);
	// returns a grid obtained by resampling the v of this grid
	EtRectElevationGrid<Real, Integer> resampleV(const ResamplingMethods method, const Real newResolutionV);
	// returns a grid obtained by resampling the u and v of this grid
	EtRectElevationGrid<Real, Integer> resampleUV(const ResamplingMethods methodU, const ResamplingMethods methodV, const Real newResolutionU, const Real newResolutionV);

private:
	// returns a grid obtained by linear resampling the u of this grid
	EtRectElevationGrid<Real, Integer> linearResampleU(const Real newResolutionU);
	// returns a grid obtained by linear resampling the v of this grid
	EtRectElevationGrid<Real, Integer> linearResampleV(const Real newResolutionV);
	// returns a grid obtained by linear resampling the u and v of this grid
	EtRectElevationGrid<Real, Integer> linearResampleUV(const Real newResolutionU, const Real newResolutionV);

public:
	// ----------------------------------------------
	// LOADERS
	// ----------------------------------------------
	// loads data from DTED *.dt2 files
	// returns true if successfull
	bool loadFromDT2(char* fileName);
	bool loadFromDT2(string fileName);
	// loads data from USGS DEM *.dem files
	// returns true if successfull
	bool loadFromUSGSDEM(char* fileName);
	bool loadFromUSGSDEM(string fileName);
	// loads data from Arc/Info ASCII Grid *.asc or *.grd files
	// returns true if successfull
	bool loadFromARCINFOASCIIGrid( char* fileName );
	bool loadFromARCINFOASCIIGrid( const string& fileName );
	// loads data from *.xyz files
	// returns true if successfull
	bool loadFromXYZ(char* fileName);
	bool loadFromXYZ(string fileName);

	// ----------------------------------------------
	// SAVERS
	// ----------------------------------------------
	// saves data to *.xyz files
	// returns true if successfull
	bool saveToXYZ(char* fileName);
	bool saveToXYZ(string fileName);
	// saves World data to *.xyz files
	// returns true if successfull
	bool saveAsWorldToXYZ(char* fileName);
	bool saveAsWorldToXYZ(string fileName);
	// saves World data to Arc/Info ASCII Grid *.asc or *.grd files
	// returns true if successfull
	bool saveAsWorldToARCINFOASCIIGrid( char* filename );
	bool saveAsWorldToARCINFOASCIIGrid( const string& filename );

	// saves data of the subGrid of this grid with the specified
	// SW and NE corners to *.xyz files
	// returns true if successfull
	bool saveSubGridToXYZ(char* fileName, const unsigned int swCornerU, const unsigned int swCornerV, const unsigned int neCornerU, const unsigned int neCornerV);
	bool saveSubGridToXYZ(string fileName, const unsigned int swCornerU, const unsigned int swCornerV, const unsigned int neCornerU, const unsigned int neCornerV);
	// saves World data of the subGrid of this grid with the specified
	// SW and NE corners to *.xyz files
	// returns true if successfull
	bool saveSubGridAsWorldToXYZ(char* fileName, const unsigned int swCornerU, const unsigned int swCornerV, const unsigned int neCornerU, const unsigned int neCornerV);
	bool saveSubGridAsWorldToXYZ(string fileName, const unsigned int swCornerU, const unsigned int swCornerV, const unsigned int neCornerU, const unsigned int neCornerV);

protected:
	// ----------------------------------------------
	// HELPERS
	// ----------------------------------------------
	// set all World units to meters
	void setDefaultWorldUnits();
	// clears all data
	void clear();
	// return true if the grid is empty (cleared)
	bool empty() const;
	// allocs memory for the elevation array 
	// return true if the memory allocation is successfull
	// if fillW is true the grid is filled with the NO_ELEVATION value
	bool allocW(bool fillW = true);
	// returns the index in the array of the node at (u, v)
	size_t w_Index( size_t u, size_t v) const;

	// converts Surface Units to string for more meaningful output
	string surfaceUnitsAsString(const SurfaceUnits units) const;
	// converts Elevation Units to string for more meaningful output
	string elevationUnitsAsString(const ElevationUnits units) const;
};

#include "..\src\EtRectElevationGrid.inl"

// Helper for DT2 loader: 
// converts from DDDMMSSH (degrees-minute-seconds-hemisphere) to Real
// the returned value is in arc-seconds
template <class Real>
Real DDDMMSSH_ToSeconds(char angle[9]);

#endif