//-----------------------------------------------------------------------------
// File: CG_HoledPolygon2.inl
//
// Desc: A two dimensional polygon with holes
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < typename Real >
CG_HoledPolygon2< Real >::CG_HoledPolygon2()
: CG_Polygon2< Real >()
{
	m_type = PolyType_HOLED;
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_HoledPolygon2< Real >::CG_HoledPolygon2( const CG_Polygon2< Real >& polygon )
: CG_Polygon2< Real >( polygon )
{
	m_type = PolyType_HOLED;
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_HoledPolygon2< Real >::CG_HoledPolygon2( const CG_HoledPolygon2< Real >& other )
: CG_Polygon2< Real >( other )
, m_holes( other.m_holes )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_HoledPolygon2< Real >::~CG_HoledPolygon2()
{
	CG_Polygon2< Real >::~CG_Polygon2();
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_HoledPolygon2< Real >& CG_HoledPolygon2< Real >::operator = ( const CG_HoledPolygon2< Real >& other )
{
	CG_Polygon2< Real >::operator = ( other );
	m_holes = other.m_holes;

	return (*this);
}

//-----------------------------------------------------------------------------

template < typename Real >
const vector< CG_Polygon2< Real > >& CG_HoledPolygon2< Real >::GetHoles() const
{
	return (m_holes);
}

//-----------------------------------------------------------------------------

template < typename Real >
vector< CG_Polygon2< Real > >& CG_HoledPolygon2< Real >::GetHoles()
{
	return (m_holes);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_HoledPolygon2< Real >::Translate( Real dx, Real dy )
{
	CG_Polygon2< Real >::Translate( dx, dy );
	for ( size_t i = 0, cntI = m_holes.size(); i < cntI; ++i )
	{
		m_holes[i].Translate( dx, dy );
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_HoledPolygon2< Real >::Translate( const CG_Vector2< Real >& displacement )
{
	CG_Polygon2< Real >::Translate( displacement );
	for ( size_t i = 0, cntI = m_holes.size(); i < cntI; ++i )
	{
		m_holes[i].Translate( displacement );
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_HoledPolygon2< Real >::Rotate( const CG_Point2< Real >& center, Real angle )
{
	CG_Polygon2< Real >::Rotate( center, angle );
	for ( size_t i = 0, cntI = m_holes.size(); i < cntI; ++i )
	{
		m_holes[i].Rotate( center, angle );
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_HoledPolygon2< Real >::RotateAroundBarycenter( Real angle )
{
	CG_Polygon2< Real >::RotateAroundBarycenter( angle );
	CG_Point2< Real > center = Barycenter();
	for ( size_t i = 0, cntI = m_holes.size(); i < cntI; ++i )
	{
		m_holes[i].Rotate( center, angle );
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_HoledPolygon2< Real >::Triangulate( const ETriangulationMethod& method )
{
	CG_Polygon2< Real >::Triangulate( PolyTri_TRIANGLE16 );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_HoledPolygon2< Real >::Partition( const ETriangulationMethod& method )
{
	CG_Polygon2< Real >::Partition( PolyTri_TRIANGLE16 );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_HoledPolygon2< Real >::Rectify()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_HoledPolygon2< Real >::AddHole( const CG_Polygon2< Real >& hole )
{
	if ( CG_Polygon2< Real >::Contains( hole, true ) )
	{
		m_holes.push_back( hole );
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
size_t CG_HoledPolygon2< Real >::HolesCount() const
{
	return (m_holes.size());
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_HoledPolygon2< Real >::Contains( const CG_Point2< Real >& point, bool boundaries )
{
	if ( !CG_Polygon2< Real >::Contains( point, boundaries ) ) { return (false); }

	for ( size_t i = 0, cntI = m_holes.size(); i < cntI; ++i )
	{
		if ( m_holes[i].Contains( point, boundaries ) ) { return (false); }
	}

	return (true);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_HoledPolygon2< Real >::Contains( const CG_Polygon2< Real >& polygon, bool boundaries )
{
	if ( !CG_Polygon2< Real >::Contains( polygon, boundaries ) ) { return (false); }

	for ( size_t i = 0, cntI = m_holes.size(); i < cntI; ++i )
	{
		if ( m_holes[i].Contains( polygon, boundaries ) ) { return (false); }
	}

	return (true);
}

//-----------------------------------------------------------------------------
