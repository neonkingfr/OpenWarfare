//-----------------------------------------------------------------------------
// File: CG_Rectangle2.inl
//
// Desc: A two dimensional rectangle
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < typename Real >
CG_Rectangle2< Real >::CG_Rectangle2()
: m_center( static_cast< Real >( 0 ), static_cast< Real >( 0 ) )
, m_width( static_cast< Real >( 0 ) )
, m_height( static_cast< Real >( 0 ) )
, m_orientation( static_cast< Real >( 0 ) )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Rectangle2< Real >::CG_Rectangle2( const CG_Rectangle2< Real >& other )
: m_center( other.m_center )
, m_height( other.m_height )
, m_width( other.m_width )
, m_orientation( other.m_orientation )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Rectangle2< Real >::~CG_Rectangle2()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
const CG_Point2< Real >& CG_Rectangle2< Real >::GetCenter() const
{
	return (m_center);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Rectangle2< Real >::GetWidth() const
{
	return (m_width);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Rectangle2< Real >::GetHeight() const
{
	return (m_height);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Rectangle2< Real >::GetOrientation() const
{
	return (m_orientation);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Rectangle2< Real >::SetCenter( const CG_Point2< Real >& center )
{
	m_center = center;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Rectangle2< Real >::SetHeight( Real height )
{
	m_height = height;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Rectangle2< Real >::SetWidth( Real width )
{
	m_width = width;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Rectangle2< Real >::SetOrientation( Real orientation )
{
	m_orientation = orientation;
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Rectangle2< Real >::Area() const
{
	return (m_width * m_height);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Rectangle2< Real >::AspectRatio() const
{
	if ( m_width != static_cast< Real >( 0 ) )
	{
		return (m_height / m_width);
	}
	else
	{
		return (CG_Consts< Real >::MAX_REAL);
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Rectangle2< Real >::CornerLT() const
{
	return (Corner( true, true ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Rectangle2< Real >::CornerLB() const
{
	return (Corner( true, false ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Rectangle2< Real >::CornerRT() const
{
	return (Corner( false, true ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Rectangle2< Real >::CornerRB() const
{
	return (Corner( false, false ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Rectangle2< Real >::Corner( bool left, bool top ) const
{
	Real w = static_cast< Real >( 0.5 ) * m_width;
	Real h = static_cast< Real >( 0.5 ) * m_height;

	if ( left )
	{
		w *= static_cast< Real >( -1 );
	}
	if ( !top )
	{
		h *= static_cast< Real >( -1 );
	}

	CG_Point2< Real > retP( m_center.GetX() + w, m_center.GetY() + h );

	if ( m_orientation != static_cast< Real >( 0 ) )
	{
		retP.Rotate( m_center, m_orientation );	
	}

	return (retP);
}

//-----------------------------------------------------------------------------
