//-----------------------------------------------------------------------------
// File: CG_Point3.inl
//
// Desc: A three dimensional point
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < typename Real >
CG_Point3< Real >::CG_Point3( Real x, Real y, Real z )
: m_x( x )
, m_y( y )
, m_z( z )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point3< Real >::CG_Point3( const CG_Point3< Real >& other )
: m_x( other.m_x )
, m_y( other.m_y )
, m_z( other.m_z )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point3< Real >::~CG_Point3()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point3< Real >& CG_Point3< Real >::operator = ( const CG_Point3< Real >& other )
{
	m_x = other.m_x;
	m_y = other.m_y;
	m_z = other.m_z;

	return (*this);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Point3< Real >::Equals( const CG_Point3< Real >& other, Real tolerance ) const
{
	if ( this == &other ) { return (true); }

	Real dx = abs( other.m_x - m_x );
	Real dy = abs( other.m_y - m_y );
	Real dz = abs( other.m_z - m_z );

	return (dx <= tolerance && dy <= tolerance && dz <= tolerance);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Point3< Real >::GetX() const
{
	return (m_x);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Point3< Real >::GetY() const
{
	return (m_y);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Point3< Real >::GetZ() const
{
	return (m_z);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Point3< Real >::SetX( Real x )
{
	m_x = x;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Point3< Real >::SetY( Real y )
{
	m_y = y;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Point3< Real >::SetZ( Real z )
{
	m_z = z;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Point3< Real >::Translate( Real dx, Real dy, Real dz )
{
	m_x += dx;
	m_y += dy;
	m_z += dz;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Point3< Real >::Rotate( const CG_Vector3< Real >& axis, Real angle,
							    const CG_Point3< Real >& center )
{
	CG_Matrix3x3< Real > rotMat;
	rotMat.SetFromAxisAngle( axis, angle );
	CG_Point3< Real > transfCenter = -rotMat * center;
	transfCenter.Translate( center.GetX(), center.GetY(), center.GetZ() );
	CG_Point3< Real > transfThis = rotMat * (*this);
	(*this) = transfThis.Translated( transfCenter.GetX(), transfCenter.GetY(), 
									 transfCenter.GetZ() );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Point3< Real >::Rotate( const CG_Matrix3x3< Real >& matrix )
{
	(*this) = matrix * (*this);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Point3< Real >::SquaredDistance( const CG_Point3< Real >& other ) const
{
	Real dx = other.m_x - m_x;
	Real dy = other.m_y - m_y;
	Real dz = other.m_z - m_z;
	return (dx * dx + dy * dy + dz * dz);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Point3< Real >::Distance( const CG_Point3< Real >& other ) const
{
	return (sqrt( SquaredDistance( other ) ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point3< Real > CG_Point3< Real >::Translated( Real dx, Real dy, Real dz )
{
	CG_Point3< Real > retP( *this );
	retP.Translate( dx, dy, dz );
	return (retP);
}

//-----------------------------------------------------------------------------

template < typename Real >
std::ostream& operator << ( std::ostream& o, const CG_Point3< Real >& p )
{
	o << "(" << p.GetX() << ", " << p.GetY() << ", " << p.GetZ() <<  ")";
    return (o);
}

//-----------------------------------------------------------------------------
