//-----------------------------------------------------------------------------
// File: CG_Rectangle2.h
//
// Desc: A two dimensional rectangle
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// This class represents a non axis-aligned rectangle in two dimensions 
// (euclidean plane XY).
//-----------------------------------------------------------------------------

#ifndef CG_RECTANGLE2_H
#define CG_RECTANGLE2_H

//-----------------------------------------------------------------------------

#include "CG_Point2.h"

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Rectangle2
{
	CG_Point2< Real > m_center;

	Real m_width;
	Real m_height;
	Real m_orientation;

public:
	CG_Rectangle2();
	CG_Rectangle2( const CG_Rectangle2< Real >& other );
	~CG_Rectangle2();

	// Getters
	// {
	// Returns the center of this rectangle.
	const CG_Point2< Real >& GetCenter() const;

	// Returns the width of this rectangle.
	Real GetWidth() const;

	// Returns the height of this rectangle.
	Real GetHeight() const;

	// Returns the orientation of this rectangle. (the angle between the
	// world's X axis and the basis of this rectangle) 
	Real GetOrientation() const;
	// }

	// Setters
	// {
	void SetCenter( const CG_Point2< Real >& center );
	void SetHeight( Real height );
	void SetWidth( Real width );
	void SetOrientation( Real orientation );
	// }

	// Bounding box properties
	// {
	// Returns the area of this rectangle.
	Real Area() const;

	// Returns the aspect ratio (heigth/width) of this rectangle.
	Real AspectRatio() const;

	// Returns the left top corner of this rectangle.
	CG_Point2< Real > CornerLT() const;

	// Returns the left bottom corner of this rectangle.
	CG_Point2< Real > CornerLB() const;

	// Returns the right top corner of this rectangle.
	CG_Point2< Real > CornerRT() const;

	// Returns the right bottom corner of this rectangle.
	CG_Point2< Real > CornerRB() const;
	// }

private:
	CG_Point2< Real > Corner( bool left, bool top ) const;
};

//-----------------------------------------------------------------------------

#include "CG_Rectangle2.inl"

//-----------------------------------------------------------------------------

#endif