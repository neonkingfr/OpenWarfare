//-----------------------------------------------------------------------------
// File: TPriorityQueue.h
//
// Desc: a templated priority queue
//
// The template classes must have the following methods:
// class T1:
// - T1( const T1& other );
// class T2:
// - bool operator > ( const T2& other );
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#ifndef TPRIORITYQUEUE_H
#define TPRIORITYQUEUE_H

//-----------------------------------------------------------------------------

#include <vector>

//-----------------------------------------------------------------------------

using std::vector;

//-----------------------------------------------------------------------------

template < class T1, typename Real >
class TPriorityQueue
{
protected:
	class Item
	{
		T1 m_data;
		Real m_priority;

	public:
		/**
		\name Constructor
		*/
		//@{
		Item( const T1& data, const Real& priority );
		//@}

		/**
		\name Getters
		*/
		//@{
		const T1& GetData() const;
		T1& GetData();
		const Real& GetPriority() const;
		//@}

		/**
		\name Setters
		*/
		//@{
		void SetData( const T1& data );
		void SetPriority( const Real& priority );
		//@}
	};

	vector< Item > m_items;

public:
	/**
	\name Constructors
	*/
	//@{
	TPriorityQueue();
	TPriorityQueue( const TPriorityQueue< T1, Real >& other );
	//@}

	/**
	\name Destructor
	*/
	//@{
	virtual ~TPriorityQueue();
	//@}

	/*!
	Assignement
	*/
	//@{
	TPriorityQueue< T1, Real >& operator = ( const TPriorityQueue< T1, Real >& other );
	//@}

	/*!
	Items access
	*/
	//@{
	/*!
	Returns the item of this priority queue at the given index.
	\param index the index of the desired item of this list.
	\return the item of this priority queue at the given index.
	*/
	const T1& operator [] ( size_t index ) const;
	T1& operator [] ( size_t index );
	//@}

	/*!
	Tests if this priority queue is empty.
	\return true if this queue is empty.
	*/
	bool Empty() const;

	/*!
	Removes the first item of this priority queue from the top position.
	*/
	void Pop();

	/*!
	Adds an item to the priority queue based on the given priority.
	\param item the item to add to this queue.
	\param priority the priority of the item.
	\param front if true the given item will be placed before any other item having the same priority.
	*/
	void Push( const T1& item, const Real& priority, bool front = false );

	/*!
	Returns the number of items in this priority queue.
	\return the number of items in this priority queue.
	*/
	size_t Size() const;

	/*!
	Returns a const reference to the first item of this priority queue.
	\return a const reference to the first item of this priority queue.
	*/
	const T1& Top() const;

	/*!
	Removes the item of this priority queue at the given index.
	\param index the index of the item to remove.
	*/
	void Remove( size_t index );

	/*!
	Clears this priority.
	*/
  void Clear();

private:
	void _CopyFrom( const TPriorityQueue< T1, Real >& other );
};

//-----------------------------------------------------------------------------

#include "TPriorityQueue.inl"

//-----------------------------------------------------------------------------

#endif