//-----------------------------------------------------------------------------
// File: CG_Triangulation.inl
//
// Desc: A triangulation interface for the triangulation results with a meta data.
//
// Auth: Daniel Musil
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < typename Real >
CG_Triangulation< Real >::CG_Triangulation()
: m_done( false )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Triangulation< Real >::~CG_Triangulation()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Triangulation< Real >::IsDone()
{
	return m_done;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Triangulation< Real >::Clear()
{
	m_done = false;
	m_triangles.clear();
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Triangulation< Real >::AppendTriangle( const CG_Triangle2< Real >& triangle )
{
   m_triangles.push_back( triangle );
}

//-----------------------------------------------------------------------------

template < typename Real >
const vector< CG_Triangle2< Real > >& CG_Triangulation< Real >::GetTrianglesList() const
{
	return m_triangles;
}

//-----------------------------------------------------------------------------
