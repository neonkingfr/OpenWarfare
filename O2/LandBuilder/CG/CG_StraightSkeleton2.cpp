//-----------------------------------------------------------------------------
// File: CG_StraightSkeleton2.cpp
//
// Desc: the straight skeleton of a two dimensional polygon
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "CG_StraightSkeleton2.h"

//-----------------------------------------------------------------------------
