//-----------------------------------------------------------------------------
// File: CG_Vector3.inl
//
// Desc: A three dimensional vector
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real >::CG_Vector3()
: m_x( static_cast< Real >( 0 ) )
, m_y( static_cast< Real >( 0 ) )
, m_z( static_cast< Real >( 0 ) )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real >::CG_Vector3( Real x, Real y, Real z )
: m_x( x )
, m_y( y )
, m_z( z )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real >::CG_Vector3( const CG_Point3< Real >& head, 
							    const CG_Point3< Real >& tail )
{
	m_x = head.GetX() - tail.GetX();
	m_y = head.GetY() - tail.GetY();
	m_z = head.GetZ() - tail.GetZ();
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real >::CG_Vector3( const CG_Vector3< Real >& other )
: m_x( other.m_x )
, m_y( other.m_y )
, m_z( other.m_z )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real >::~CG_Vector3()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real >& CG_Vector3< Real >::operator = ( const CG_Vector3< Real >& other )
{
	m_x = other.m_x;
	m_y = other.m_y;
	m_z = other.m_z;

	return (*this);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Vector3< Real >::Equals( const CG_Vector3< Real >& other, Real tolerance ) const
{
	if ( this == &other ) { return (true); }

	Real dx = abs( other.m_x - m_x );
	Real dy = abs( other.m_y - m_y );
	Real dz = abs( other.m_z - m_z );

	return (dx <= tolerance && dy <= tolerance && dz <= tolerance);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real >& CG_Vector3< Real >::operator += ( const CG_Vector3< Real >& other )
{
	m_x += other.m_x;
	m_y += other.m_y;
	m_z += other.m_z;

	return (*this);
}

//-----------------------------------------------------------------------------
template < typename Real >
CG_Vector3< Real >& CG_Vector3< Real >::operator -= ( const CG_Vector3< Real >& other )
{
	m_x -= other.m_x;
	m_y -= other.m_y;
	m_z -= other.m_z;

	return (*this);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real >& CG_Vector3< Real >::operator *= ( const Real scalar )
{
	m_x *= scalar;
	m_y *= scalar;
	m_z *= scalar;

	return (*this);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real > CG_Vector3< Real >::operator + ( const CG_Vector3< Real >& other ) const
{
	CG_Vector3< Real > ret( *this );
	ret += other;
	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real > CG_Vector3< Real >::operator - ( const CG_Vector3< Real >& other ) const
{
	CG_Vector3< Real > ret( *this );
	ret -= other;
	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real > CG_Vector3< Real >::operator * ( const Real scalar ) const
{
	CG_Vector3< Real > ret( *this );
	ret *= scalar;
	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Vector3< Real >::GetX() const
{
	return (m_x);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Vector3< Real >::GetY() const
{
	return (m_y);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Vector3< Real >::GetZ() const
{
	return (m_z);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Vector3< Real >::SetX( Real x )
{
	m_x = x;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Vector3< Real >::SetY( Real y )
{
	m_y = y;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Vector3< Real >::SetZ( Real z )
{
	m_z = z;
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Vector3< Real >::SquaredLength() const
{
	return (m_x * m_x + m_y * m_y + m_z * m_z);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Vector3< Real >::Length() const
{
	return (sqrt( SquaredLength() ));
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Vector3< Real >::Normalize()
{
	Real len = Length();
	if ( len != static_cast< Real >( 0 ) )
	{
		Real inv = static_cast< Real >( 1 ) / len;
		m_x *= inv;
		m_y *= inv;
		m_z *= inv;
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real > CG_Vector3< Real >::operator - () const
{
	return (CG_Vector3< Real >( -m_x, -m_y, -m_z ));
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Vector3< Real >::Dot( const CG_Vector3< Real >& other ) const
{
	return (m_x * other.m_x + m_y * other.m_y + m_z * other.m_z);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real > CG_Vector3< Real >::Cross( const CG_Vector3< Real >& other ) const
{
	return (CG_Vector3< Real >( m_y * other.m_z - m_z * other.m_y,
								m_z * other.m_x - m_x * other.m_z,
								m_x * other.m_y - m_y * other.m_x ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real > CG_Vector3< Real >::Normalized() const
{
	CG_Vector3< Real > retVec( *this );
	retVec.Normalize();
	return (retVec);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point3< Real > CG_Vector3< Real >::Head( const CG_Point3< Real >& tail ) const
{
	CG_Point3< Real > ret( tail );
	ret.Translate( m_x, m_y, m_z );
	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point3< Real > CG_Vector3< Real >::Tail( const CG_Point3< Real >& head ) const
{
	CG_Point3< Real > ret( head );
	ret.Translate( -m_x, -m_y, -m_z );
	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Vector3< Real >::Rotate( const CG_Vector3< Real >& axis, Real angle )
{
	CG_Matrix3x3< Real > rotMat;
	rotMat.SetFromAxisAngle( axis, angle );
	CG_Point3< Real > head = Head();
	CG_Point3< Real > transfHead = rotMat * head;
	m_x = transfHead.GetX();
	m_y = transfHead.GetY();
	m_z = transfHead.GetZ();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real > operator * ( const Real scalar, const CG_Vector3< Real >& v )
{
	CG_Vector3< Real > ret( v );
	ret *= scalar;
	return (ret);
}

//-----------------------------------------------------------------------------
