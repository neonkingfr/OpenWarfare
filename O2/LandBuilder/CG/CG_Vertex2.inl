//-----------------------------------------------------------------------------
// File: CG_Vertex2.inl
//
// Desc: A two dimensional vertex
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < typename Real >
CG_Vertex2< Real >::CG_Vertex2()
: CG_Point2< Real >()
, m_id( -1 )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vertex2< Real >::CG_Vertex2( Real x, Real y )
: CG_Point2< Real >( x, y )
, m_id( -1 )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vertex2< Real >::CG_Vertex2( Real x, Real y, int id )
: CG_Point2< Real >( x, y )
, m_id( id )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vertex2< Real >::CG_Vertex2( const CG_Vertex2< Real >& other )
: CG_Point2< Real >( other.m_x, other.m_y )
, m_id( other.m_id )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vertex2< Real >::~CG_Vertex2()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
int CG_Vertex2< Real >::GetID() const
{
	return (m_id);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Vertex2< Real >::SetID( int id )
{
	m_id = id;
}

//-----------------------------------------------------------------------------
