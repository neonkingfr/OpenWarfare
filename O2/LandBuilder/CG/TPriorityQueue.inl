//-----------------------------------------------------------------------------
// File: TPriorityQueue.inl
//
// Desc: a templated priority queue
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < class T1, typename Real >
TPriorityQueue< T1, Real >::Item::Item( const T1& data, const Real& priority )
: m_data( data )
, m_priority( priority )
{
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
const T1& TPriorityQueue< T1, Real >::Item::GetData() const
{
	return (m_data);
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
T1& TPriorityQueue< T1, Real >::Item::GetData()
{
	return (m_data);
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
const Real& TPriorityQueue< T1, Real >::Item::GetPriority() const
{
	return (m_priority);
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
void TPriorityQueue< T1, Real >::Item::SetData( const T1& data )
{
	m_data = data;
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
void TPriorityQueue< T1, Real >::Item::SetPriority( const Real& priority )
{
	m_priority = priority;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template < class T1, typename Real >
TPriorityQueue< T1, Real >::TPriorityQueue()
{
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
TPriorityQueue< T1, Real >::TPriorityQueue( const TPriorityQueue< T1, Real >& other )
{
	_CopyFrom( other );
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
TPriorityQueue< T1, Real >::~TPriorityQueue()
{
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
TPriorityQueue< T1, Real >& TPriorityQueue< T1, Real >::operator = ( const TPriorityQueue< T1, Real >& other )
{
	_CopyFrom( other );
	return (*this);
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
const T1& TPriorityQueue< T1, Real >::operator [] ( size_t index ) const
{
	assert( index < m_items.size() );
	return (m_items[index].GetData());
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
T1& TPriorityQueue< T1, Real >::operator [] ( size_t index )
{
	assert( index < m_items.size() );
	return (m_items[index].GetData());
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
bool TPriorityQueue< T1, Real >::Empty() const
{
	return (m_items.size() == 0);
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
void TPriorityQueue< T1, Real >::Pop()
{
	m_items.erase( m_items.begin() );
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
void TPriorityQueue< T1, Real >::Push( const T1& item, const Real& priority, bool front )
{
	size_t size = m_items.size();

	if ( size > 0 )
	{
    Real diff;
		for ( size_t i = 0; i < size; ++i )
		{
      diff = priority - m_items[i].GetPriority();

      if ( diff > CG_Consts< Real >::ZERO_TOLERANCE )
			{
				m_items.insert( m_items.begin() + i, Item( item, priority ) );
				return;
			}

      if ( front && abs( diff ) <= CG_Consts< Real >::ZERO_TOLERANCE )
			{
				m_items.insert( m_items.begin() + i, Item( item, priority ) );
				return;
			}
		}
	}

	m_items.push_back( Item( item, priority ) );
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
size_t TPriorityQueue< T1, Real >::Size() const
{
	return (m_items.size());
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
const T1& TPriorityQueue< T1, Real >::Top() const
{
	assert( m_items.size() > 0 );
	return (m_items[0].GetData());
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
void TPriorityQueue< T1, Real >::Remove( size_t index )
{
	assert( index < m_items.size() );
	m_items.erase( m_items.begin() + index );
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
void TPriorityQueue< T1, Real >::Clear()
{
  m_items.clear();
}

//-----------------------------------------------------------------------------

template < class T1, typename Real >
void TPriorityQueue< T1, Real >::_CopyFrom( const TPriorityQueue< T1, Real >& other )
{
	m_items = other.m_items;
}

//-----------------------------------------------------------------------------
