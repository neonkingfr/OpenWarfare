//-----------------------------------------------------------------------------
// File: CG_Segment2.h
//
// Desc: A two dimensional segment
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// This class represents a segment in two dimensions (euclidean plane XY).
//-----------------------------------------------------------------------------

#ifndef CG_SEGMENT2_H
#define CG_SEGMENT2_H

//-----------------------------------------------------------------------------

#include "CG_Point2.h"
#include "CG_Line2.h"
#include "CG_Vector2.h"

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Segment2 
{
protected:
  CG_Point2< Real > m_from;
  CG_Point2< Real > m_to;

public:
  CG_Segment2();
  CG_Segment2( const CG_Point2< Real >& from, const CG_Point2< Real >& to );
  CG_Segment2( const CG_Segment2< Real >& other );

  virtual ~CG_Segment2();

  // Getters
  // {
  const CG_Point2< Real >& GetFrom() const;
  const CG_Point2< Real >& GetTo() const;
  // }

  // Setters
  // {
  void SetFrom( const CG_Point2< Real >& from );
  void SetTo( const CG_Point2< Real >& to );
  // }

  // Comparison
  // {
  bool Equals( const CG_Segment2< Real >& other,
               Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;
  bool IsInverseOf( const CG_Segment2< Real >& other,
                    Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;
  bool EqualsOrInverseOf( const CG_Segment2< Real >& other,
                          Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;
  // }

  // Segment manipulators
  // {
  // Rotates this segment around the given center by the 
  // given angle in radians (positive CCW).
  void Rotate( const CG_Point2< Real >& center, Real angle );

  // Rotates this segment around its mid point by the 
  // given angle in radians (positive CCW).
  void RotateAroundMidpoint( Real angle );

	// Translates this segment using the given displacements.
	void Translate( Real dx, Real dy );
	void Translate( const CG_Vector2< Real >& displacement );
  // }

  // Segment properties
  // {
  Real SquaredLength() const;
  Real Length() const;

  // Returns the direction (angle between the world's X axis and this 
  // segment, oriented as from->to) in radians (positive CCW)
  Real Direction() const;

  // Returns the closest point on this segment to the given point.
  CG_Point2< Real > ClosestPointTo( const CG_Point2< Real >& point, 
                                    Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

  // Returns the unit tangent vector to this segment.
  CG_Vector2< Real> UnitTangentVector() const;

  // Returns the unit normal vector to this segment.
  // It is always directed toward the left.
  CG_Vector2< Real> UnitNormalVector() const;

  // Returns the midpoint of this segment
  CG_Point2< Real > MidPoint() const;

  // Returns true if the given point is strictly in the semiplane at
  // the left of the line containing this segment for an observer placed
  // in m_From and looking at m_To
  bool HasAtLeft( const CG_Point2< Real >& point,
                  Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

  // Returns true if the given point is in the semiplane at
  // the left of the line containing this segment for an observer placed
  // in m_From and looking at m_To or if it lies on the line
  bool HasAtLeftOrOn( const CG_Point2< Real >& point, 
                      Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

  // Returns true if the given point lies on the line containing this segment 
  bool IsCollinear( const CG_Point2< Real >& point, 
                    Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

  // Returns the distance of the given point from the line containing this segment
  Real DistanceFromContainingLine( const CG_Point2< Real >& point ) const;
  // }

	// Containment
	// {
  // Returns true if this segment contains the point with the given coordinates.
  // If boundary is true the segment's endpoints are tested for containment too.
  bool Contains( Real x, Real y, bool boundary, 
                 Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

  // Returns true if this segment contains the given point.
  // If boundary is true the segment's endpoints are tested for containment too.
  bool Contains( const CG_Point2< Real >& point, bool boundary, 
                 Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

  // Returns true if this segment contains the given segment.
  // If boundary is true the segment's endpoints are tested for containment too.
  bool Contains( const CG_Segment2< Real >& other, bool boundary, 
                 Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;
  // }

	// Intersection
	// {
  // Returns true if this segment and the given one share a point interior
  // to both (if one endpoint of one of the segments lies on the other segment
  // the result is false).
  bool IntersectsProperly( const CG_Segment2< Real >& other, 
                           Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

  // Returns true if this segment and the given one intersect
  // (if one endpoint of one of the segments lies on the other segment
  // the result is true).
  bool Intersects( const CG_Segment2< Real >& other, bool boundaries, 
                   Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

  // Returns the intersection point between this segment and the given one 
  // or the point at infinite if no intersection exists
  // If boundary is true the segment's endpoints are tested too.
  CG_Point2< Real > Intersection( const CG_Segment2< Real >& other, bool boundary,
                                  Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const; 
  // }

  // Operations with segment
  // {
  // Returns a segment who is the inverse of this segment
  CG_Segment2< Real > Inverted() const;

  // Returns a segment who is obtained from the offset of this segment by
  // the given quantity
  // Negative value of the offset are toward the left of this segment
  CG_Segment2< Real > Offset( Real offset ) const;

  // Returns a vector having the from point as tail and the to point as head
  CG_Vector2< Real > ToVector() const;
  // }
};

//-----------------------------------------------------------------------------

#include "CG_Segment2.inl"

//-----------------------------------------------------------------------------

#endif