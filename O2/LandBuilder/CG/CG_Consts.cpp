//-----------------------------------------------------------------------------
// File: CG_Consts.cpp
//
// Desc: Mathematical constants
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include <math.h>
#include <float.h>

//-----------------------------------------------------------------------------

#include "CG_Consts.h"

//-----------------------------------------------------------------------------

template<> const float CG_Consts< float >::PI         = (float)(4.0 * atan( 1.0 ));
template<> const float CG_Consts< float >::TWO_PI     = 2.0f * CG_Consts< float >::PI;
template<> const float CG_Consts< float >::HALF_PI    = 0.5f * CG_Consts< float >::PI;
template<> const float CG_Consts< float >::INV_PI     = 1.0f / CG_Consts< float >::PI;
template<> const float CG_Consts< float >::INV_TWO_PI = 1.0f / CG_Consts< float >::TWO_PI;

template<> const float CG_Consts< float >::DEG_TO_RAD = CG_Consts< float >::PI / 180.0f;
template<> const float CG_Consts< float >::RAD_TO_DEG = 180.0f / CG_Consts< float >::PI;

template<> const float CG_Consts< float >::MAX_REAL = FLT_MAX;

template<> const float CG_Consts< float >::EPSILON        = FLT_EPSILON;
template<> const float CG_Consts< float >::ZERO_TOLERANCE = 1e-06f;

template<> const float CG_Consts< float >::ZERO = 0.0f;
template<> const float CG_Consts< float >::HALF = 0.5f;
template<> const float CG_Consts< float >::ONE  = 1.0f;
template<> const float CG_Consts< float >::TWO  = 2.0f;

//-----------------------------------------------------------------------------

template<> const double CG_Consts< double >::PI         = 4.0 * atan( 1.0 );
template<> const double CG_Consts< double >::TWO_PI     = 2.0 * CG_Consts< double >::PI;
template<> const double CG_Consts< double >::HALF_PI    = 0.5 * CG_Consts< double >::PI;
template<> const double CG_Consts< double >::INV_PI     = 1.0 / CG_Consts< double >::PI;
template<> const double CG_Consts< double >::INV_TWO_PI = 1.0 / CG_Consts< double >::TWO_PI;

template<> const double CG_Consts< double >::DEG_TO_RAD = CG_Consts< double >::PI / 180.0;
template<> const double CG_Consts< double >::RAD_TO_DEG = 180.0 / CG_Consts< double >::PI;

template<> const double CG_Consts< double >::MAX_REAL = DBL_MAX;

template<> const double CG_Consts< double >::EPSILON        = DBL_EPSILON;
template<> const double CG_Consts< double >::ZERO_TOLERANCE = 1e-07;

template<> const double CG_Consts< double >::ZERO = 0.0;
template<> const double CG_Consts< double >::HALF = 0.5;
template<> const double CG_Consts< double >::ONE  = 1.0;
template<> const double CG_Consts< double >::TWO  = 2.0;

//-----------------------------------------------------------------------------
