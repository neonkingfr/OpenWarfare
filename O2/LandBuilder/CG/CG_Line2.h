//-----------------------------------------------------------------------------
// File: CG_Line2.h
//
// Desc: A two dimensional line
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// This class represents a line in two dimensions (euclidean plane XY).
//-----------------------------------------------------------------------------

#ifndef CG_LINE2_H
#define CG_LINE2_H

//-----------------------------------------------------------------------------

#include "CG_Point2.h"

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Line2
{
	Real m_a;
	Real m_b;
	Real m_c;

public:
	CG_Line2();
	CG_Line2( const CG_Point2< Real >& p1, const CG_Point2< Real >& p2 );
	CG_Line2( const CG_Line2< Real >& other );

	// Assignement
	CG_Line2< Real >& operator = ( const CG_Line2< Real >& other );

	// Comparison
	bool Equals( const CG_Line2< Real >& other,
               Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

	// Getters
	// {
	Real GetA() const;
	Real GetB() const;
	Real GetC() const;
	// }

	// Line properties
	// {
	// Returns the direction (angle between the world's X axis and this 
	// line) in radians (positive CCW)
	Real Direction() const;
	// }

	// Returns the distance of the given point from this line.
	Real Distance( const CG_Point2< Real >& point );

private:
	void _Initialize( const CG_Point2< Real >& p1, const CG_Point2< Real >& p2 );
};

//-----------------------------------------------------------------------------

// Returns true if the given lines intersect.
// The intersection point is returned in the parameter intersection.
template < typename Real >
bool Intersect( const CG_Line2< Real >& line1, const CG_Line2< Real >& line2,
                CG_Point2< Real >& intersection );

//-----------------------------------------------------------------------------

#include "CG_Line2.inl"

//-----------------------------------------------------------------------------

#endif

