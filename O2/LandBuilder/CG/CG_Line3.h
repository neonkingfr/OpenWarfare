//-----------------------------------------------------------------------------
// File: CG_Line3.h
//
// Desc: A three dimensional line
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// This class represents a line in three dimensions (euclidean space XYZ).
//-----------------------------------------------------------------------------

#ifndef CG_LINE3_H
#define CG_LINE3_H

//-----------------------------------------------------------------------------

#include "CG_Point3.h"

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Line3
{
	CG_Point3< Real >  m_base;
	CG_Vector3< Real > m_direction;

public:
	CG_Line3();
	CG_Line3( const CG_Point3< Real >& base, const CG_Vector3< Real >& direction );
	CG_Line3( const CG_Line3< Real >& other );
	~CG_Line3();

	// Line properties
	// {
	CG_Point3< Real > PointAt( Real distanceFromBase ) const;
	// }

	// Operations with lines
	// {
	Real Distance( const CG_Point3< Real >& point ) const;
	// }
};

//-----------------------------------------------------------------------------

#include "CG_Line3.inl"

//-----------------------------------------------------------------------------

#endif

