///-----------------------------------------------------------------------------
// File: CG_Polyline2.inl
//
// Desc: A two dimensional polyline
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include <algorithm>

//-----------------------------------------------------------------------------

template < typename Real >
CG_Polyline2< Real >::CG_Polyline2()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Polyline2< Real >::CG_Polyline2( const CG_Polyline2< Real >& other )
: m_vertices( other.m_vertices )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Polyline2< Real >::~CG_Polyline2()
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Polyline2< Real >& CG_Polyline2< Real >::operator = ( const CG_Polyline2< Real >& other )
{
	m_vertices = other.m_vertices;

	return (*this);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polyline2< Real >::Equals( const CG_Polyline2< Real >& other ) const
{
	if ( this == &other ) { return (true); }

	if ( m_vertices.size() != other.m_vertices.size() ) { return (false); }

	for ( size_t i = 0, cntI = m_vertices.size(); i < cntI; ++i )
	{
		if ( !m_vertices[i].Equals( other.m_vertices[i], CG_Consts< Real >::EPSILON ) ) { return (false); }
	}

	return (true);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polyline2< Real >::EqualsReverse( const CG_Polyline2< Real >& other ) const
{
	if ( m_vertices.size() != other.m_vertices.size() ) { return (false); }

	for ( size_t i = 0, cntI = m_vertices.size(); i < cntI; ++i )
	{
		if ( !m_vertices[i].Equals( other.m_vertices[cntI - 1 - i], CG_Consts< Real >::EPSILON ) ) { return (false); }
	}

	return (true);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polyline2< Real >::Translate( Real dx, Real dy )
{
	for ( size_t i = 0, cntI = m_vertices.size(); i < cntI; ++i )
	{
		m_vertices[i].Translate( dx, dy );
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polyline2< Real >::Translate( const CG_Vector2< Real >& displacement )
{
	Translate( displacement.GetX(), displacement.GetY() );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polyline2< Real >::Rotate( const CG_Point2< Real >& center, Real angle )
{
	for ( size_t i = 0, cntI = m_vertices.size(); i < cntI; ++i )
	{
		m_vertices[i].Rotate( center, angle );
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polyline2< Real >::AppendVertex( Real x, Real y )
{
	AppendVertex( CG_Point2< Real >( x, y ) );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polyline2< Real >::AppendVertex( const CG_Point2< Real >& vertex )
{
	m_vertices.push_back( vertex );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polyline2< Real >::AppendVertices( const CG_Polyline2< Real >& other )
{
	if ( other.VerticesCount() > 0 )
	{
		Real tolerance = static_cast< Real >( 0.001 );

		// checks if other first vertex does not coincide with this last vertex
		if ( !other.FirstVertex().Equals( LastVertex(), tolerance ) )
		{
			AppendVertex( other.FirstVertex() );
		}

		for ( size_t i = 1, cntI = other.VerticesCount(); i < cntI; ++i )
		{
			AppendVertex( other.Vertex( i ) );
		}
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polyline2< Real >::InsertVertex( size_t index, Real x, Real y )
{
	InsertVertex( index, CG_Point2< Real >( x, y ) ); 
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polyline2< Real >::InsertVertex( size_t index, const CG_Point2< Real >& vertex )
{
	assert( index < m_vertices.size() );
	m_vertices.insert( m_vertices.begin() + index, vertex );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polyline2< Real >::MoveVertexTo( size_t index, Real x, Real y )
{
	MoveVertexTo( index, CG_Point2< Real >( x, y ) );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polyline2< Real >::MoveVertexTo( size_t index, const CG_Point2< Real >& vertex )
{
	assert( index < m_vertices.size() );
	CG_Point& p = m_vertices[index];
	p.SetX( x );
	p.SetY( y );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polyline2< Real >::RemoveVertex( size_t index )
{
	assert( index < m_vertices.size() );
	m_vertices.erase( m_vertices.begin() + index );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polyline2< Real >::RemoveAllVertices()
{
	m_vertices.clear();
}

//-----------------------------------------------------------------------------

template < typename Real >
size_t CG_Polyline2< Real >::RemoveConsecutiveDuplicatedVertices( Real tolerance )
{
	size_t counter = 0;

	if ( m_vertices.size() > 1 )
	{
		size_t i = 0;
		while ( i < m_vertices.size() - 1 )
		{
			if ( m_vertices[i].Equals( m_vertices[i + 1], tolerance ) )
			{
				m_vertices.erase( m_vertices.begin() + (i + 1) );
				++counter;
				--i;
			}
			++i;
		}
	}

	return (counter);
}

//-----------------------------------------------------------------------------

template < typename Real >
size_t CG_Polyline2< Real >::RemoveConsecutiveAlignedVertices( Real tolerance )
{
	size_t counter = 0;

	if ( m_vertices.size() > 2 )
	{
		size_t i = 1;
		while ( i < m_vertices.size() - 1 )
		{
			if ( Collinear( m_vertices[i - 1], m_vertices[i], m_vertices[i + 1], tolerance ) )
			{
				m_vertices.erase( m_vertices.begin() + i );
				++counter;
				--i;
			}
			++i;
		}
	}

	return (counter);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Polyline2< Real >::ReverseVerticesOrder()
{
	reverse( m_vertices.begin(), m_vertices.end() );
}

//-----------------------------------------------------------------------------

template < typename Real >
size_t CG_Polyline2< Real >::StraightenWherePossible( Real tolerance, Real threshold, bool fixedEndpoints )
{
  size_t removedVertices = 0;

  size_t size     = m_vertices.size();
  size_t calcSize = size;
  size_t minSize  = fixedEndpoints ? 5 : 3;
  if ( calcSize < minSize ) { return (removedVertices); }

  size_t currStartV = fixedEndpoints ? 3 : 2;
  size_t lastV      = fixedEndpoints ? (size - 2) : (size - 1);

  while ( currStartV <= lastV )
  {
    CG_Polyline2< Real > temp;
    temp.AppendVertex( m_vertices[currStartV - 2] );
    temp.AppendVertex( m_vertices[currStartV - 1] );
    size_t currEndV = currStartV;

    bool alreadyOnStraight = false;
    bool proceedReducing   = false;
    CG_Segment2< Real > prevSeg;
    CG_Segment2< Real > currSeg;
    while ( currEndV <= lastV )
    {
      prevSeg = currSeg;
      temp.AppendVertex( m_vertices[currEndV] );
      bool isStraight = temp.IsApproximatelyStraight( tolerance, threshold, currSeg );
      bool currIsLast = false;

      if ( isStraight && !alreadyOnStraight ) 
      { 
        alreadyOnStraight = true; 
        if ( currEndV == lastV )
        {
          proceedReducing = true;
          currIsLast      = true;
        }
      }
      else if ( !isStraight && alreadyOnStraight ) 
      { 
        proceedReducing = true;
      }
      else if ( isStraight && alreadyOnStraight && (currEndV == lastV) ) 
      { 
        proceedReducing = true;
        currIsLast      = true;
      }

      if ( proceedReducing )
      {
        size_t start = currStartV - 2;
        size_t end   = currIsLast ? currEndV : (currEndV - 1);
        CG_Segment2< Real > seg = currIsLast ? currSeg : prevSeg;

        size_t iterRemovedVertices = 0;
        for ( size_t i = start; i <= end; ++i )
        {
          m_vertices.erase( m_vertices.begin() + start );
          ++iterRemovedVertices;
        }

        m_vertices.insert( m_vertices.begin() + start, seg.GetTo() );
        m_vertices.insert( m_vertices.begin() + start, seg.GetFrom() );
        iterRemovedVertices -= 2;
        removedVertices += iterRemovedVertices;

        if ( currIsLast ) 
        { 
          return (removedVertices); 
        }
        
        size     = m_vertices.size();
        calcSize = size - end + iterRemovedVertices + 1;
        if ( calcSize < minSize ) 
        { 
          return (removedVertices); 
        }
        currStartV = end - iterRemovedVertices;
        lastV = fixedEndpoints ? (size - 2) : (size - 1);
        currEndV = lastV;
      }

      ++currEndV;
    }

    ++currStartV;
  }

  return (removedVertices);
}

//-----------------------------------------------------------------------------

template < typename Real >
size_t CG_Polyline2< Real >::VerticesCount() const
{
	return (m_vertices.size());
}

//-----------------------------------------------------------------------------

template < typename Real >
const CG_Point2< Real >& CG_Polyline2< Real >::Vertex( size_t index ) const
{
	assert( index < m_vertices.size() );
	return (m_vertices[index]);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real >& CG_Polyline2< Real >::Vertex( size_t index )
{
	assert( index < m_vertices.size() );
	return (m_vertices[index]);
}

//-----------------------------------------------------------------------------

template < typename Real >
const CG_Point2< Real >& CG_Polyline2< Real >::FirstVertex() const
{
	return (m_vertices[0]);
}

//-----------------------------------------------------------------------------

template < typename Real >
const CG_Point2< Real >& CG_Polyline2< Real >::LastVertex() const
{
	return (m_vertices[m_vertices.size() - 1]);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Segment2< Real > CG_Polyline2< Real >::Segment( size_t index ) const
{
	assert( index < m_vertices.size() - 1 );
	return (CG_Segment2< Real >( m_vertices[index], m_vertices[index + 1] ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Segment2< Real > CG_Polyline2< Real >::FirstSegment() const
{
	assert( m_vertices.size() > 1 );
	return (CG_Segment2< Real >( m_vertices[0], m_vertices[1] ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Segment2< Real > CG_Polyline2< Real >::LastSegment() const
{
	assert( m_vertices.size() > 1 );
	size_t size = m_vertices.size();
	return (CG_Segment2< Real >( m_vertices[size - 2], m_vertices[size - 1] ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Segment2< Real > CG_Polyline2< Real >::Chord() const
{
	return (CG_Segment2< Real >( FirstVertex(), LastVertex() ));
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Polyline2< Real >::Length() const
{
	Real length = static_cast< Real >( 0 );
	if ( m_vertices.size() > 1 )
	{
		for ( size_t i = 0, cntI = m_vertices.size() - 1; i < cntI; ++i )
		{
			length += m_vertices[i].Distance( m_vertices[i + 1] );
		}
	}

	return (length);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polyline2< Real >::HasConsecutiveOverlappingSegments( Real tolerance ) const
{
	if ( m_vertices.size() < 3 ) { return (false); }

	for ( size_t i = 1, cntI = m_vertices.size() - 1; i < cntI; ++i )
	{
		// We use the property that two parallel unitary vectors, pointing
		// in opposite directions, have a dot product equal to -1.
		CG_Vector2< Real > seg1( m_vertices[i], m_vertices[i - 1]); 
		seg1.Normalize();
		CG_Vector2< Real > seg2( m_vertices[i + 1], m_vertices[i]); 
		seg2.Normalize();
		if ( seg1.Dot( seg2 ) < (tolerance - static_cast< Real >( 1 )) ) 
		{ 
			return (true); 
		}
	}

	return (false);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Rectangle2< Real > CG_Polyline2< Real >::MinimumBoundingBox() const
{
	CG_Rectangle2< Real > ret;

	// TODO: we need the convex hull of the vertices,
	// then we can apply the same algorithm as for polygons

	// then: calculates the center of the bounding rectangle
	if ( m_vertices.size() > 0 )
	{
		// at the moment i did not found a faster way to calculate the
		// bounding box center...

		// first we move to the reference system where the bounding box
		// is axis aligned and in this system we calculate the coordinates 
		// of the bounding box center

		CG_Point2< Real > barycenter = Barycenter();

		Real minX = std::numeric_limits< Real >::infinity();
		Real minY = std::numeric_limits< Real >::infinity();
		Real maxX = -std::numeric_limits< Real >::infinity();
		Real maxY = -std::numeric_limits< Real >::infinity();

		for ( size_t i = 0, cntI = m_vertices.size(); i < cntI; ++i )
		{
			CG_Point2< Real > p = m_vertices[i].Rotated( barycenter, -ret.GetDirection() );
			Real x = p.GetX();
			Real y = p.GetY();
			minX = std::min( minX, x );
			minY = std::min( minY, y );
			maxX = std::max( maxX, x );
			maxY = std::max( maxY, y );
		}

		Real bbcXaa = static_cast< Real >( 0.5 ) * ( minX + maxX );
		Real bbcYaa = static_cast< Real >( 0.5 ) * ( minY + maxY );

		CG_Point2< Real > bbC( bbcXaa, bbcYaa );

		// now we have to calculate the coordinate of the center in our
		// reference system
		bbC.Rotate( barycenter, ret.GetDirection() );

		// done :)
		ret.SetCenter( bbC );
	}

	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Rectangle2< Real > CG_Polyline2< Real >::AxisAlignedBoundingBox() const
{
	CG_Rectangle2< Real > ret;

	Real minX = CG_Consts< Real >::MAX_REAL;
	Real minY = CG_Consts< Real >::MAX_REAL;
	Real maxX = -CG_Consts< Real >::MAX_REAL;
	Real maxY = -CG_Consts< Real >::MAX_REAL;

	Real x, y;
	for ( size_t = 0, cntI = m_vertices.size(); i < cntI; ++i )
	{
		x = m_vertices[i].GetX();
		y = m_vertices[i].GetY();
		minX = min( minX, x );
		minY = min( minY, y );
		maxX = max( maxX, x );
		maxY = max( maxY, y );
	}

	ret.SetWidth( maxX - minX );
	ret.SetHeight( maxY - minY );
	ret.SetDirection( static_cast< Real >( 0 ) );
	Real midX = static_cast< Real >( 0.5 ) * (maxX + minX);
	Real midY = static_cast< Real >( 0.5 ) * (maxY + minY);
	ret.SetCenter( CG_Point2< Real >( midX, midY ) );

	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Polyline2< Real >::Barycenter() const
{
	Real x = static_cast< Real >( 0 );
	Real y = static_cast< Real >( 0 );

	if ( m_vertices.size() > 0 )
	{
		for ( size_t i = 0, cntI = m_vertices.size(); i < cntI; ++i )
		{
			x += m_vertices[i].GetX();
			y += m_vertices[i].GetY();
		}

		Real invSize = static_cast< Real >( 1 ) / m_vertices.size();
		x *= invSize;
		y *= invSize;
	}

	return (CG_Point2< Real >( x, y ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Polyline2< Real >::ClosestPointTo( const CG_Point2< Real >& point,
                                                        Real tolerance, 
                                                        CG_Vector2< Real >* tangent,
                                                        CG_Vector2< Real >* normal ) const
{
	CG_Point2< Real > ret;

	if ( m_vertices.size() > 0 )
	{
		if ( m_vertices.size() == 1 )
		{
			ret = m_vertices[0];
			if ( tangent )
			{
				tangent->SetX( static_cast< Real >( 0 ) );
				tangent->SetY( static_cast< Real >( 0 ) );
			}
			if ( normal )
			{
				normal->SetX( static_cast< Real >( 0 ) );
				normal->SetY( static_cast< Real >( 0 ) );
			}
		}
		else
		{
			// for speed, we use squared distances

			Real minDist = std::numeric_limits< Real >::infinity();
			for ( size_t i = 0, cntI = m_vertices.size() - 1; i < cntI; ++i )
			{
				CG_Segment2< Real > seg( m_vertices[i], m_vertices[i + 1] );
				CG_Point2< Real > closest = seg.ClosestPointTo( point, tolerance );
				Real dist = point.SquaredDistance( closest );
				if ( minDist > dist )
				{
					minDist = dist;
					ret = closest;

					if ( tangent || normal ) // if both NULL, skips
					{
						int neighSegIndex = -1;
						if ( closest.Equals( m_vertices[i], tolerance ) )
						{
							// closest coincides with the first vertex of 
							// the current segment.
							// there is a neighbor only if this vertex is
							// not the first vertex of the polyline.
							if ( i != 0 ) { neighSegIndex = i - 1; }
						}
						else if ( closest.Equals( m_vertices[i + 1], tolerance ) )
						{
							// closest coincides with the second vertex of 
							// the current segment.
							// there is a neighbor only if this vertex is
							// not the last vertex of the polyline.
							if ( i != cntI - 1 ) { neighSegIndex = i + 1; }
						}

						if ( neighSegIndex == -1 )
						{
							// non shared vertex
							if ( tangent ) { *tangent = seg.UnitTangentVector(); }
							if ( normal )  { *normal  = seg.UnitNormalVector(); }
						}
						else
						{
							// shared vertex
							CG_Segment2< Real > neighSeg( m_vertices[neighSegIndex], m_vertices[neighSegIndex + 1] );
							if ( normal )  { *normal  = (static_cast< Real >( 0.5 ) * (seg.UnitNormalVector() + neighSeg.UnitNormalVector())).Normalized(); }
							if ( tangent ) { *tangent = (static_cast< Real >( 0.5 ) * (seg.UnitNormalVector() + neighSeg.UnitNormalVector())).Normalized().PerpCW(); }
						}
					}
				}
			}
		}
	}

	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Polyline2< Real >::MinX() const
{
	Real minX = CG_Consts< Real >::MAX_REAL;

	for ( size_t = 0, cntI = m_vertices.size(); i < cntI; ++i )
	{
		minX = min( minX, m_vertices[i].GetX() );
	}

	return (minX);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Polyline2< Real >::MaxX() const
{
	Real maxX = -CG_Consts< Real >::MAX_REAL;

	for ( size_t = 0, cntI = m_vertices.size(); i < cntI; ++i )
	{
		maxX = max( maxX, m_vertices[i].GetX() );
	}

	return (maxX);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Polyline2< Real >::MinY() const
{
	Real minY = CG_Consts< Real >::MAX_REAL;

	for ( size_t = 0, cntI = m_vertices.size(); i < cntI; ++i )
	{
		minY = min( minY, m_vertices[i].GetY() );
	}

	return (minY);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Polyline2< Real >::MaxY() const
{
	Real maxY = -CG_Consts< Real >::MAX_REAL;

	for ( size_t = 0, cntI = m_vertices.size(); i < cntI; ++i )
	{
		maxY = max( maxY, m_vertices[i].GetY() );
	}

	return (maxY);
}

//-----------------------------------------------------------------------------

template < typename Real >
int CG_Polyline2< Real >::SegmentIndexAt( Real length ) const
{
	if ( length < static_cast< Real >( 0 ) ) { return (-1); }

	Real currLength = static_cast< Real >( 0 );
	for ( size_t i = 0, cntI = m_vertices.size() - 1; i < cntI; ++i )
	{
		currLength += m_vertices[i].Distance( m_vertices[i + 1] );
		if ( length <= currLength )
		{
			return (static_cast< int >( i ));
		}
	}

	return (m_vertices.size() - 1); 
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Polyline2< Real >::PointAt( Real length ) const
{
	if ( length < static_cast< Real >( 0 ) ) { return (FirstVertex()); }

	Real currLength = static_cast< Real >( 0 );
	Real prevLength = static_cast< Real >( 0 );
	Real dist;
	for ( size_t i = 0, cntI = m_vertices.size() - 1; i < cntI; ++i )
	{
		dist = m_vertices[i].Distance( m_vertices[i + 1] );
		currLength += dist;
		if ( length <= currLength )
		{
			CG_Vector2< Real > v( m_vertices[i + 1], m_vertices[i] );
			v.Normalize();
			v *= (length - prevLength);
			return (v.Head( m_vertices[i] ));
		}
		prevLength += dist;
	}

	return (LastVertex()); 
}

//-----------------------------------------------------------------------------

template < typename Real >
vector< CG_Polyline2< Real > > CG_Polyline2< Real >::SplitInTwoHalves( Real tolerance ) const
{
	return (Split( static_cast< Real >( 0.5 ) * Length(), tolerance ));
}

//-----------------------------------------------------------------------------

template < typename Real >
vector< CG_Polyline2< Real > > CG_Polyline2< Real >::SplitOnLongestSegment( Real tolerance ) const
{
	// first searches the longest segment
	int index = LongestSegmentIndex();
	assert( index != -1 );

	size_t longestSegIndex = static_cast< size_t >( index );

	// calculates the coordinate of the cut
	Real cutLength = static_cast< Real >( 0 );
	for ( size_t i = 0; i < longestSegIndex; ++i )
	{
		cutLength += m_vertices[i].Distance( m_vertices[i + 1] );
	}	
	cutLength += static_cast< Real >( 0.5 ) * m_vertices[longestSegIndex].Distance( m_vertices[longestSegIndex + 1] );

	return (Split( cutLength, tolerance ) );
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Polyline2< Real > CG_Polyline2< Real >::SubPart( Real startLength, Real endLength, 
                                                    bool entireSegments ) const
{
	assert( startLength < endLength );

	CG_Polyline2< Real > ret;

	int startSegIndex = SegmentIndexAt( startLength );
	int endSegIndex   = SegmentIndexAt( endLength );

	if ( startSegIndex < 0 ) { startSegIndex = 0; }
	if ( endSegIndex < 0 )   { endSegIndex = 0; }
	if ( startSegIndex == (m_vertices.size() - 1) ) { startSegIndex = m_vertices.size() - 2; }
	if ( endSegIndex == (m_vertices.size() - 1) )   { endSegIndex = m_vertices.size() - 2; }

	// first vertex
	if ( entireSegments )
	{
		ret.AppendVertex( m_vertices[startSegIndex] );
	}
	else
	{
		ret.AppendVertex( PointAt( startLength ) );
	}

	// mid vertices
	for ( size_t i = static_cast< size_t >( startSegIndex + 1 ); i <= static_cast< size_t >( endSegIndex ); ++i )
	{
		ret.AppendVertex( m_vertices[i] );
	}

	// last vertex
	if ( entireSegments )
	{
		ret.AppendVertex( m_vertices[static_cast< size_t >( endSegIndex + 1 )] );
	}
	else
	{
		ret.AppendVertex( PointAt( endLength ) );
	}

	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Polyline2< Real >::LengthAt( const CG_Point2< Real >& point, Real tolerance ) const
{
	Real length = static_cast< Real >( 0 );

	for ( size_t i = 0, cntI = m_vertices.size() - 1; i < cntI; ++i )
	{
		CG_Segment2< Real > seg( m_vertices[i], m_vertices[i + 1] );
		if ( seg.Contains( point, true, tolerance ) )
		{
			CG_Vector2< Real > vec( point, m_vertices[i] );
			length += vec.Length();
			return (length);
		}
		else
		{
			length += seg.Length();
		}
	}

	return (static_cast< Real >( 0 ));
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Polyline2< Real >::LengthAtClosestPointTo( const CG_Point2< Real >& point,
                                                   Real tolerance ) const
{
	CG_Point2< Real > closestPoint = ClosestPointTo( point, tolerance, NULL, NULL );
	return (LengthAt( closestPoint, tolerance ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Polyline2< Real >::MidPoint() const
{
	return (PointAt( static_cast< Real >( 0.5 ) * Length() ));
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Point2< Real > CG_Polyline2< Real >::MidPointOfLongestSegment() const
{
	// first searches the longest segment
	int index = LongestSegmentIndex();
	assert( index != -1 );

	CG_Segment2< Real > seg = Segment( static_cast< size_t >( index ) );
	return (seg.MidPoint());
}

//-----------------------------------------------------------------------------

namespace CGPOLYLINE2PRIVATE
{
  typedef enum
  {
    UNKNOWN,
    INSIDE,
    OUTSIDE,
    INTERSECTION_IN,
    INTERSECTION_OUT
  } ClipVertexType;

  template < typename Real >
  class ClipVertex : public CG_Point2< Real >
  {
    string         m_name;
    ClipVertexType m_type;

  public:
		ClipVertex( Real x, Real y, const string& name, ClipVertexType type )
    : CG_Point2< Real >( x, y )
    , m_name( name )
    , m_type( type )
    {
    }

		ClipVertex( const ClipVertex& other )
    : CG_Point2< Real >( other )
    , m_name( other.m_name )
    , m_type( other.m_type )
    {
    }

		virtual ~ClipVertex()
    {
    }

    const string& GetName() const
    {
      return (m_name);
    }

    ClipVertexType GetType() const
    {
      return (m_type);
    }

    void SetType( ClipVertexType type )
    {
      m_type = type;
    }
  };

  template < typename Real >
  class ClipVertexList
  {
    vector< ClipVertex< Real > > m_list;
    size_t                       m_current;

  public:
    ClipVertexList()
    : m_current( 0 )
    {
    }

    void Append( const ClipVertex< Real >& vertex )
    {
      m_list.push_back( vertex );
    }

    void InsertAfter( const string& vertexName, const ClipVertex< Real >& vertex )
    {
      int index = FindByName( vertexName );
      if ( index != -1 ) 
      {
        if ( index < (int)(m_list.size() - 1) )
        {
          const ClipVertex< Real >& pointAtIndex = m_list[index];
          Real dist1 = pointAtIndex.Distance( vertex );
          while ( index < (int)(m_list.size() - 1) && m_list[index + 1].GetName()[0] == 'I' )
          {
            Real dist2 = pointAtIndex.Distance( m_list[index + 1] );
            if ( dist2 > dist1 ) 
            { 
              ++index; 
            }
            else
            {
              break;
            }
          }
          m_list.insert( m_list.begin() + index + 1, vertex ); 
        }
        else
        {
          m_list.push_back( vertex );
        }
      }
    }

    size_t Size() const
    {
      return (m_list.size());
    }

    int FindByName( const string& vertexName ) const
    {
      for ( size_t i = 0, cntI = m_list.size(); i < cntI; ++i )
      {
        if ( vertexName == m_list[i].GetName() ) { return ((int)i); }
      }

      return (-1);
    }

    bool AdvanceCurrent()
    {
      if ( m_current == m_list.size() - 1 )
      {
        return (false);
      }
      else
      {
        ++m_current;
        return (true);
      }
    }

    void ResetCurrent()
    {
      m_current = 0;
    }

    ClipVertexType GetCurrentType() const
    {
      return (m_list[m_current].GetType());
    }

    void SetCurrentType( ClipVertexType type )
    {
      m_list[m_current].SetType( type );
    }

    CG_Point2< Real > GetCurrentPosition() const
    {
      return (m_list[m_current]);
    }
  };
}

//-----------------------------------------------------------------------------

template < typename Real >
vector< CG_Polyline2< Real > > CG_Polyline2< Real >::Clip( const CG_Polygon2< Real >& clipPolygon ) const
{
  // first we make a working copy of the clipping polygon 
  CG_Polygon2< Real > tempClip( clipPolygon );

  vector< CG_Polyline2< Real > > polylines;

  size_t sizeThis = m_vertices.size();
  size_t sizeClip = tempClip.VerticesCount();

  if ( sizeThis < 2 || sizeClip < 3 ) { return (polylines); }

  // the clipping polygon completely contains this polyline
  if ( tempClip.Contains( *this, true ) )
  {
    polylines.push_back( *this );
    return (polylines); 
  }

  // this polyline and the clipping polygon don't intersect
  if ( !tempClip.IntersectsProperly( *this ) ) { return (polylines); }

  // we create a lists containing the polyline's vertices
  CGPOLYLINE2PRIVATE::ClipVertexList< Real > listThis;
  for ( size_t i = 0; i < sizeThis; ++i )
  {
    const CG_Point2< Real >& point = m_vertices[i];
    char buff[20];
    sprintf_s( buff, "%d", i );
    string name( buff );
    CGPOLYLINE2PRIVATE::ClipVertexType type;
    if ( tempClip.Contains( point, true ) )
    {
      type = CGPOLYLINE2PRIVATE::INSIDE; 
    }
    else
    {
      type = CGPOLYLINE2PRIVATE::OUTSIDE; 
    }

    listThis.Append( CGPOLYLINE2PRIVATE::ClipVertex< Real >( point.GetX(), point.GetY(), name, type ) ); 
  }

  // step one is to identify all of the intersection points between this polyline and the 
  // clipping polygon and add them to the list

  size_t intCounter = 0;
  for ( size_t i = 0; i < sizeThis - 1; ++i )
  {
    CG_Segment2< Real > segmThis = Segment( i );

    for ( size_t j = 0; j < sizeClip; ++j )
    {
      CG_Segment2< Real > edgeClip = tempClip.Edge( j );

      if ( segmThis.IntersectsProperly( edgeClip ) )
      {
        // calculates intersection point
        CG_Point2< Real > intPoint = segmThis.Intersection( edgeClip, false );

        // sets the point name
        char buffCounter[20];
        sprintf_s( buffCounter, "I%d", (++intCounter) );
        string intName( buffCounter );

        CGPOLYLINE2PRIVATE::ClipVertex< Real > intVertex( intPoint.GetX(), intPoint.GetY(), intName, CGPOLYLINE2PRIVATE::UNKNOWN ); 

        // adds the point to the lists
        char buffThis[20];
        sprintf_s( buffThis, "%d", i );
        string nameThis( buffThis );
        listThis.InsertAfter( nameThis, intVertex );
      }
    }
  }

  // now we identify the type of each intersection point
  CGPOLYLINE2PRIVATE::ClipVertexType typePrev = listThis.GetCurrentType();

  while ( listThis.AdvanceCurrent() )
  {
    CGPOLYLINE2PRIVATE::ClipVertexType typeCurr = listThis.GetCurrentType();
    
    if ( typeCurr != typePrev )
    {
      if ( typeCurr == CGPOLYLINE2PRIVATE::UNKNOWN )
      {
        if ( typePrev == CGPOLYLINE2PRIVATE::INSIDE )
        {
          listThis.SetCurrentType( CGPOLYLINE2PRIVATE::INTERSECTION_OUT );
          typePrev = CGPOLYLINE2PRIVATE::OUTSIDE;
        }
        else if ( typePrev == CGPOLYLINE2PRIVATE::OUTSIDE )
        {
          listThis.SetCurrentType( CGPOLYLINE2PRIVATE::INTERSECTION_IN );
          typePrev = CGPOLYLINE2PRIVATE::INSIDE;
        }
      }
    }
  }

  // finally we use the list to identify the resulting polylines
  listThis.ResetCurrent();
  CG_Polyline2< Real > polyline;
  if ( listThis.GetCurrentType() == CGPOLYLINE2PRIVATE::INSIDE )
  {
    polyline.AppendVertex( listThis.GetCurrentPosition() );
  }

  while ( listThis.AdvanceCurrent() )
  {
    CGPOLYLINE2PRIVATE::ClipVertexType typeCurr = listThis.GetCurrentType();

    if ( typeCurr == CGPOLYLINE2PRIVATE::INSIDE )
    {
      polyline.AppendVertex( listThis.GetCurrentPosition() );
    }
    else if ( typeCurr == CGPOLYLINE2PRIVATE::OUTSIDE )
    {
      if ( polyline.VerticesCount() > 1 ) 
      { 
        polylines.push_back( polyline ); 
        polyline.RemoveAllVertices();
      }
    }
    else if ( typeCurr == CGPOLYLINE2PRIVATE::INTERSECTION_IN )
    {
      polyline.AppendVertex( listThis.GetCurrentPosition() );
    }
    else if ( typeCurr == CGPOLYLINE2PRIVATE::INTERSECTION_OUT )
    {
      polyline.AppendVertex( listThis.GetCurrentPosition() );
      polylines.push_back( polyline );
      polyline.RemoveAllVertices();
    }
  }

  if ( polyline.VerticesCount() > 1 ) { polylines.push_back( polyline ); }

  return (polylines);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Polyline2< Real >::IsApproximatelyStraight( Real tolerance, Real threshold, CG_Segment2< Real >& approximatingSegment ) const
{
  size_t size = m_vertices.size();

  if ( size < 2 )  { return (false); }
  if ( size == 2 ) { return (true); }

  // this algorithm is a modification of the one that can been taken from http://www.geometrictools.com
  // we use baricentric coordinates to try to avoid for overflows
  // {
	CG_Point2< Real > center = Barycenter();
  Real cX = center.GetX();
  Real cY = center.GetY();

  Real sumX  = CG_Consts< Real >::ZERO;
  Real sumY  = CG_Consts< Real >::ZERO;
  Real sumXX = CG_Consts< Real >::ZERO;
  Real sumXY = CG_Consts< Real >::ZERO;

  for ( size_t i = 0; i < size; ++i )
  {
    Real x = m_vertices[i].GetX() - cX; 
    Real y = m_vertices[i].GetY() - cY; 
    sumX += x;
    sumY += y;
    sumXX += (x * x);
    sumXY += (x * y);
  }

  Real aafA[2][2] = { { sumXX, sumX },
                      { sumX, static_cast< Real >( size ) } };

  Real afB[2] = { sumXY, sumY };

  Real det = aafA[0][0] * aafA[1][1] - aafA[0][1] * aafA[1][0];

  // is the approximating line vertical ?
  bool isVertical = (abs( det ) < CG_Consts< Real >::ZERO_TOLERANCE) ? true : false;

  // approximating line
  CG_Line2< Real > line;
  if ( !isVertical )
  {
    Real invDet = CG_Consts< Real >::ONE / det;
    Real rfA = (aafA[1][1] * afB[0] - aafA[0][1] * afB[1]) * invDet;
    Real rfB = (aafA[0][0] * afB[1] - aafA[1][0] * afB[0]) * invDet;
    line = CG_Line2< Real >( CG_Point2< Real >( CG_Consts< Real >::ZERO, rfB ),
                             CG_Point2< Real >( CG_Consts< Real >::ONE, rfA + rfB ) );
  }
  else
  {
    line = CG_Line2< Real >( CG_Point2< Real >( cX, CG_Consts< Real >::ZERO ),
                             CG_Point2< Real >( cX, CG_Consts< Real >::ONE ) );
  }
  // }

  // calculates the approximating segment
  CG_Vector2< Real > lineDir( line.Direction() );
  CG_Vector2< Real > lineNorm = lineDir.PerpCCW();

  CG_Point2< Real > startPoint( m_vertices[0].GetX() - cX, m_vertices[0].GetY() - cY );
  CG_Point2< Real > endPoint( m_vertices[size - 1].GetX() - cX, m_vertices[size - 1].GetY() - cY );

  CG_Line2< Real > lineV1( startPoint, lineNorm.Head( startPoint ) );
  CG_Line2< Real > lineV2( endPoint, lineNorm.Head( endPoint ) );

  CG_Point2< Real > v1;
  CG_Point2< Real > v2;
  bool res1 = Intersect( line, lineV1, v1 );
  bool res2 = Intersect( line, lineV2, v2 );

  assert( res1 && res2 );
  if ( !res1 || !res2 )
  {
    approximatingSegment = CG_Segment2< Real >( CG_Consts< Real >::MAX_REAL, CG_Consts< Real >::MAX_REAL );
    return (false);
  }

  approximatingSegment = CG_Segment2< Real >( v1, v2 );
  approximatingSegment.Translate( cX, cY );

	CG_Segment2< Real > longestSegment = Segment( static_cast< size_t >( LongestSegmentIndex() ) );
  Float maxDist = threshold * longestSegment.Length();

  for ( size_t i = 0; i < size; ++i )
  {
    CG_Point2< Real > p( m_vertices[i].GetX() - cX, m_vertices[i].GetY() - cY );
    Float dist = line.Distance( p );
    if ( dist > maxDist ) 
    { 
      return (false); 
    }
  }

  return (true);
}

//-----------------------------------------------------------------------------

template < typename Real >
vector< CG_Polyline2< Real > > CG_Polyline2< Real >::Split( Real length, Real tolerance ) const
{
	vector< CG_Polyline2< Real > > ret;
	CG_Polyline2< Real > poly1;
	CG_Polyline2< Real > poly2;

	int splitSegIndex = SegmentIndexAt( length );
	if ( splitSegIndex < 0 ) { splitSegIndex = 0; }
	if ( splitSegIndex == (m_vertices.size() - 1) ) { splitSegIndex = m_vertices.size() - 2; }

	CG_Point2< Real > splitPoint = PointAt( length );

	// sets poly1 vertices
	for ( size_t i = 0, cntI = static_cast< size_t >( splitSegIndex ); i <= cntI; ++i )
	{
		poly1.AppendVertex( m_vertices[i] );
	}
	if ( !splitPoint.Equals( poly1.LastVertex(), tolerance ) )
	{
		poly1.AppendVertex( splitPoint );
	}

	// sets poly2 vertices
	if ( !splitPoint.Equals( m_vertices[static_cast< size_t >( splitSegIndex ) + 1], tolerance ) )
	{
		poly2.AppendVertex( splitPoint );
	}
	for ( size_t i = static_cast< size_t >( splitSegIndex ) + 1, cntI = m_vertices.size(); i < cntI; ++i )
	{
		poly2.AppendVertex( m_vertices[i] );
	}

	ret.push_back( poly1 );
	ret.push_back( poly2 );

	return (ret);
}

//-----------------------------------------------------------------------------

template < typename Real >
int CG_Polyline2< Real >::LongestSegmentIndex() const
{
	int longestSegIndex = -1; 

	if ( m_vertices.size() > 1 )
	{
		Real maxLength = static_cast< Real >( 0 );
		Real currLength;
		for ( size_t i = 0, cntI = m_vertices.size() - 1; i < cntI; ++i )
		{
			currLength = m_vertices[i].SquaredDistance( m_vertices[i + 1] );
			if ( maxLength < currLength )
			{
				maxLength = currLength;
				longestSegIndex = static_cast< int >( i );
			}
		}
	}

	return (longestSegIndex);
}

//-----------------------------------------------------------------------------
