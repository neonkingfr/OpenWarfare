//-----------------------------------------------------------------------------
// File: CG_Circle2.inl
//
// Desc: A two dimensional circle
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < typename Real >
CG_Circle2< Real >::CG_Circle2()
: m_radius( static_cast< Real >( 0 ) )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Circle2< Real >::CG_Circle2( const CG_Point2< Real >& center, Real radius )
: m_center( center )
, m_radius( radius )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Circle2< Real >::CG_Circle2( Real centerX, Real centerY, Real radius )
: m_center( Point2< Real >( centerX, centerY ) )
, m_radius( radius )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Circle2< Real >::CG_Circle2( const CG_Circle2< Real >& other )
: m_center( other.m_center )
, m_radius( other.m_radius )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
const CG_Point2< Real >& CG_Circle2< Real >::GetCenter() const
{
	return (m_center);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Circle2< Real >::GetRadius() const
{
	return (m_radius);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Circle2< Real >::SetCenter( const CG_Point2< Real >& center )
{
	m_center = center;
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Circle2< Real >::SetCenter( Real centerX, Real centerY )
{
	m_center.SetX( centerX );
	m_center.SetY( centerY );
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Circle2< Real >::SetRadius( Real radius )
{
	m_radius = radius;
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Circle2< Real >::Contains( const CG_Point2< Real >& point, bool boundary, Real tolerance ) const
{
	// for speed uses squared distance
	Real sqDist = m_center.SquaredDistance( point );

	Real threshold = m_radius;
	if ( boundary )
	{
		threshold += tolerance;
	}
	else
	{
		threshold -= tolerance;
	}
	Real sqThreshold = threshold * threshold;

	return (sqDist <= sqThreshold);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Circle2< Real >::Area() const
{
	return (m_radius * m_radius * CG_Consts< Real >::PI );
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Circle2< Real >::Perimeter() const
{
	return (static_cast< Real >( 2 ) * m_radius * CG_Consts< Real >::PI);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool Intersect( const CG_Circle2< Real >& circle, const CG_Segment2< Real >& segment, 
                CG_Point2< Real >& intersection, Real tolerance )
{
	const CG_Point2< Real >& center = circle.GetCenter();
	Real radius = circle.GetRadius();
	Real maxRadius = radius + tolerance;

	Real distFrom = segment.GetFrom().Distance( center );
	Real distTo   = segment.GetTo().Distance( center );

	bool insideFrom = (distFrom - maxRadius) > static_cast< Real >( 0 ) ? false : true;
	bool insideTo   = (distTo - maxRadius) > static_cast< Real >( 0 ) ? false : true;

	if ( (insideFrom && insideTo) )
	{
		// the segment is completely contained in the circle
		intersection = CG_Point2< Real >( CG_Consts< Real >::MAX_REAL, CG_Consts< Real >::MAX_REAL );
		return (false);
	}
	else if ( (insideFrom && insideTo) || (!insideFrom && !insideTo) )
	{
		// the segment's endpoints are both out of the circle
		// TODO: in this case there could be 2 intersections 
		intersection = CG_Point2< Real >( CG_Consts< Real >::MAX_REAL, CG_Consts< Real >::MAX_REAL );
		return (false);
	}
	else
	{
		CG_Point2< Real > p1;
		CG_Point2< Real > p2;
		Real dist;

		if ( distFrom < distTo )
		{
			p1 = segment.GetFrom();
			p2 = segment.GetTo();
			dist = (radius - distFrom) / (distTo - distFrom);
		}
		else
		{
			p1 = segment.GetTo();
			p2 = segment.GetFrom();
			dist = (radius - distTo) / (distFrom - distTo);
		}

		CG_Vector2< Real > v( p2, p1 );
		v *= dist;

		intersection = v.Head( p1 );
		return (true);
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
bool FirstIntersect( const CG_Circle2< Real >& circle, const CG_Polyline2< Real >& polyline, 
                     CG_Point2< Real >& intersection, int& segIndex, Real tolerance )
{
	for ( size_t i = 0, cntI = polyline.VerticesCount() - 1; i < cntI; ++i )
	{
		if ( Intersect( circle, polyline.Segment( i ), intersection, tolerance ) )
		{
			segIndex = i;
			return (true);
		}
	}

	segIndex = -1;
	intersection = CG_Point2< Real >( CG_Consts< Real >::MAX_REAL, CG_Consts< Real >::MAX_REAL );
	return (false);
}


//-----------------------------------------------------------------------------
