//-----------------------------------------------------------------------------
// File: CG_Vector3.h
//
// Desc: A three dimensional vector
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// This class represents a vector in three dimensions (euclidean space XYZ).
//-----------------------------------------------------------------------------

#ifndef CG_VECTOR3_H
#define CG_VECTOR3_H

//-----------------------------------------------------------------------------

#include "CG_Consts.h"

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Point3;

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Vector3 
{
	Real m_x;
	Real m_y;
	Real m_z;

public:
	CG_Vector3();
	CG_Vector3( Real x, Real y, Real z );
	// Construct the vector (head - tail)
	CG_Vector3( const CG_Point3< Real >& head, 
				const CG_Point3< Real >& tail = CG_Point3< Real >( static_cast< Real >( 0 ), static_cast< Real >( 0 ), static_cast< Real >( 0 ) ));
	CG_Vector3( const CG_Vector3< Real >& other );
	~CG_Vector3();

	// Assignement
	CG_Vector3< Real >& operator = ( const CG_Vector3< Real >& other );

	// Comparison
	bool Equals( const CG_Vector3< Real >& other, Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

	// Arithmetic updates
	CG_Vector3< Real >& operator += ( const CG_Vector3< Real >& other );
	CG_Vector3< Real >& operator -= ( const CG_Vector3< Real >& other );
	CG_Vector3< Real >& operator *= ( const Real scalar );

	// Arithmetic operations
	CG_Vector3< Real > operator + ( const CG_Vector3< Real >& other ) const;
	CG_Vector3< Real > operator - ( const CG_Vector3< Real >& other ) const;
	CG_Vector3< Real > operator * ( const Real scalar ) const;

	// Getters
	// {
	Real GetX() const;
	Real GetY() const;
	Real GetZ() const;
	// }

	// Setters
	// {
	void SetX( Real x );
	void SetY( Real y );
	void SetZ( Real z );
	// }

	// Vector properties
	// {
	Real SquaredLength() const;
	Real Length() const;
	// }

	// Vector manipulators
	// {
	// Normalizes this vector
	void Normalize();
	// }

	// Operations with vectors
	// {
	CG_Vector3< Real > operator - () const;

	// Returns the dot product between this vector and the given one.
	Real Dot( const CG_Vector3< Real >& other ) const;

	// Returns the cross product between this vector and the given one.
	CG_Vector3< Real > Cross( const CG_Vector3< Real >& other ) const;

	// Returns a normalized copy of this vector 
	CG_Vector3< Real > Normalized() const;

	// returns the head of this vector when applied to the given point
	CG_Point3< Real > Head( const CG_Point3< Real >& tail = CG_Point3< Real >() ) const;

	// returns the tail (the application point) of this vector when 
	// the head is in the given point
	CG_Point3< Real > Tail( const CG_Point3< Real >& head ) const;

	// Rotates CCW this vector around the given axis by the given angle in radians
	void Rotate( const CG_Vector3< Real >& axis, Real angle );
	// }
};

//-----------------------------------------------------------------------------

template < typename Real >
CG_Vector3< Real > operator * ( const Real scalar, const CG_Vector3< Real >& v );

//-----------------------------------------------------------------------------

#include "CG_Vector3.inl"

//-----------------------------------------------------------------------------

#endif