//-----------------------------------------------------------------------------
// File: CG_Triangulation.h
//
// Desc: A triangulation container for the triangulation results with a meta data.
//
// Auth: Daniel Musil 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#ifndef CG_TRIANGULATION_H
#define CG_TRIANGULATION_H

//-----------------------------------------------------------------------------

#include <vector>
#include "CG_Triangle2.h"

//-----------------------------------------------------------------------------

using std::vector;

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Triangulation
{
public:
	CG_Triangulation();
	virtual ~CG_Triangulation();

	// Returns true if the triangulation has been already processed
	virtual bool IsDone();

	// Discards any triangulation and clear the structures
	virtual void Clear();

   // Returns a list of the triangles as a result of the triangulation
	virtual const vector< CG_Triangle2< Real > >& GetTrianglesList() const;

	// Returns a list of all the points
	virtual vector< CG_Point2< Real > > GetPointsList() const = 0;

	// Return number of the segments after the triangulation
	virtual long GetNumberOfSegments() const = 0;

	// Appends a triangle to the triangulation results
	virtual void AppendTriangle( const CG_Triangle2< Real >& triangle );

protected:
	// True if the triangulation has been already processed
	bool m_done;

	// A result of the triangulation
	vector< CG_Triangle2< Real > > m_triangles;
};

//-----------------------------------------------------------------------------

#include "CG_Triangulation.inl"

//-----------------------------------------------------------------------------

#endif
