//-----------------------------------------------------------------------------
// File: TCircularList.inl
//
// Desc: a templated circular list
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < class T >
TCircularList< T >::Item::Item( const T& data )
: m_prev( NULL )
, m_next( NULL )
, m_data( data )
{
}

//-----------------------------------------------------------------------------

template < class T >
typename TCircularList< T >::Item* TCircularList< T >::Item::GetPrev()
{
	return (m_prev);
}

//-----------------------------------------------------------------------------

template < class T >
const typename TCircularList< T >::Item* TCircularList< T >::Item::GetPrev() const
{
	return (m_prev);
}

//-----------------------------------------------------------------------------

template < class T >
typename TCircularList< T >::Item* TCircularList< T >::Item::GetNext()
{
	return (m_next);
}

//-----------------------------------------------------------------------------

template < class T >
const typename TCircularList< T >::Item* TCircularList< T >::Item::GetNext() const
{
	return (m_next);
}

//-----------------------------------------------------------------------------

template < class T >
T& TCircularList< T >::Item::GetData()
{
	return (m_data);
}

//-----------------------------------------------------------------------------

template < class T >
const T& TCircularList< T >::Item::GetData() const
{
	return (m_data);
}

//-----------------------------------------------------------------------------

template < class T >
void TCircularList< T >::Item::SetPrev( Item* prev )
{
	m_prev = prev;
}

//-----------------------------------------------------------------------------

template < class T >
void TCircularList< T >::Item::SetNext( Item* next )
{
	m_next = next;
}

//-----------------------------------------------------------------------------

template < class T >
void TCircularList< T >::Item::SetData( const T& data )
{
	m_data = data;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

template < class T >
TCircularList< T >::TCircularList()
: m_head( NULL )
, m_itemsCount( 0 )
{
}

//-----------------------------------------------------------------------------

template < class T >
TCircularList< T >::TCircularList( const TCircularList< T >& other )
: m_head( NULL )
, m_itemsCount( 0 )
{
	_CopyFrom( other );
}

//-----------------------------------------------------------------------------

template < class T >
TCircularList< T >::~TCircularList()
{
	RemoveAll();
}

//-----------------------------------------------------------------------------

template < class T >
TCircularList< T >& TCircularList< T >::operator = ( const TCircularList< T >& other )
{
	_CopyFrom( other );
	return (*this);
}

//-----------------------------------------------------------------------------

template < class T >
const T& TCircularList< T >::operator [] ( int index ) const
{
	return (_GetAt( index )->GetData());
}

//-----------------------------------------------------------------------------

template < class T >
T& TCircularList< T >::operator [] ( int index )
{
	return (_GetAt( index )->GetData());
}

//-----------------------------------------------------------------------------

template < class T >
const T& TCircularList< T >::operator [] ( size_t index ) const
{
	return (_GetAt( static_cast< int >( index ) )->GetData());
}

//-----------------------------------------------------------------------------

template < class T >
T& TCircularList< T >::operator [] ( size_t index )
{
	return (_GetAt( static_cast< int >( index ) )->GetData());
}

//-----------------------------------------------------------------------------

template < class T >
T& TCircularList< T >::Append( const T& item )
{
	_Append( item );
	return (_GetAt( static_cast< int >( m_itemsCount ) - 1 )->GetData());
}

//-----------------------------------------------------------------------------

template < class T >
const T& TCircularList< T >::Append( const T& item ) const
{
	_Append( item );
	return (_GetAt( static_cast< int >( m_itemsCount ) - 1 )->GetData());
}

//-----------------------------------------------------------------------------

template < class T >
void TCircularList< T >::RemoveAll()
{
	if ( m_head )
	{
		while ( m_head )
		{
			Remove( 0 );
		}
	}
}

//-----------------------------------------------------------------------------

template < class T >
void TCircularList< T >::Insert( int index, const T& item )
{
	if ( m_head )
	{
		Item* newItem = new Item( item );
		Item* curr    = _GetAt( index );
		Item* next    = curr->GetNext();
		newItem->SetNext( next );
		newItem->SetPrev( curr );
		curr->SetNext( newItem );
		next->SetPrev( newItem );
		++m_itemsCount;
	}
}

//-----------------------------------------------------------------------------

template < class T >
void TCircularList< T >::Remove( int index )
{
	if ( m_head )
	{
		Item* curr = _GetAt( index );

		if ( m_itemsCount > 1 )
		{
			Item* prev = curr->GetPrev();
			Item* next = curr->GetNext();

			prev->SetNext( next );
			next->SetPrev( prev );

			if ( curr == m_head ) { m_head = prev; }
		}

		delete curr;
		curr = NULL;
		--m_itemsCount;

		if ( m_itemsCount == 0 ) { m_head = NULL; }
	}	
}

//-----------------------------------------------------------------------------

template < class T >
void TCircularList< T >::Reverse()
{
	if ( m_head )
	{
		Item* curr = m_head;

		do
		{
			Item* temp = curr->GetNext();
			curr->SetNext( curr->GetPrev() );
			curr->SetPrev( temp );

			curr = curr->GetNext();
		}
		while ( curr != m_head );
	}
}

//-----------------------------------------------------------------------------

template < class T >
template < typename Real >
size_t TCircularList< T >::RemoveConsecutiveDuplicated( Real tolerance )
{
	size_t counter = 0;

	if ( m_head )
	{
		if ( m_itemsCount > 1 )
		{
			Item* curr = m_head;
			Item* next = NULL;

			do
			{
				next = curr->GetNext();

				while ( curr->GetData().Equals( next->GetData(), tolerance ) )
				{
					curr->SetNext( next->GetNext() );
					next->GetNext()->SetPrev( curr );

					if ( next == m_head ) { m_head = curr; }

					delete next;
					next = NULL;
					--m_itemsCount;
					++counter;

					next = curr->GetNext();
				}
			
				curr = curr->GetNext();
			}
			while ( curr != m_head );
		}
	}

	return (counter);
}

//-----------------------------------------------------------------------------

template < class T >
void TCircularList< T >::Slide( int count )
{
	if ( m_head )
	{
		Item* curr = m_head;

		if ( count > 0 )
		{
			for ( int i = 0; i < count; ++i )
			{
				curr = curr->GetNext();
			}
		}
		else
		{
			for ( int i = 0, cntI = abs( count ); i < cntI; ++i )
			{
				curr = curr->GetPrev();
			}
		}

		m_head = curr;
	}
}

//-----------------------------------------------------------------------------

template < class T >
size_t TCircularList< T >::Size() const
{
	return (m_itemsCount);
}

//-----------------------------------------------------------------------------

template < class T >
T* TCircularList< T >::Find( const T& item ) const
{
	if ( m_head )
	{
		Item* curr = m_head;

		do
		{
			T& data = curr->GetData();
			if ( data.Equals( item ) ) { return (&data); }
			curr = curr->GetNext();
		}
		while ( curr != m_head );
	}

	return (NULL);
}

//-----------------------------------------------------------------------------

template < class T >
int TCircularList< T >::FindIndex( const T& item ) const
{
	if ( m_head )
	{
		Item* curr = m_head;
		int counter = -1;

		do
		{
			++counter;
			if ( curr->GetData().Equals( item ) ) { return (counter); }
			curr = curr->GetNext();
		}
		while ( curr != m_head );
	}
	return (-1);
}

//-----------------------------------------------------------------------------

template < class T >
void TCircularList< T >::_CopyFrom( const TCircularList< T >& other )
{
	RemoveAll();
	for ( size_t i = 0; i < other.m_itemsCount; ++i )
	{
		Append( other[i] );
	}
}

//-----------------------------------------------------------------------------

template < class T >
size_t TCircularList< T >::_Index( int index ) const
{
	if ( 0 <= index && index < static_cast< int >( m_itemsCount ) ) { return (static_cast< size_t >( index ) ); }

	if ( index < 0 )
	{
		size_t i = m_itemsCount - abs( index ) % m_itemsCount;
		if ( i == m_itemsCount )
		{
			return (0);
		}
		else
		{
			return (i);
		}
	}
	else
	{
		return (index % m_itemsCount);
	}
}

//-----------------------------------------------------------------------------

template < class T >
typename TCircularList< T >::Item* TCircularList< T >::_GetAt( int index )
{
	assert( m_head );

	size_t itemIndex = _Index( index );

	Item* curr = m_head;
	for ( size_t i = 0; i < itemIndex; ++i )
	{
		curr = curr->GetNext();
	}

	return (curr);
}

//-----------------------------------------------------------------------------

template < class T >
const typename TCircularList< T >::Item* TCircularList< T >::_GetAt( int index ) const
{
	assert( m_head );

	size_t itemIndex = _Index( index );

	Item* curr = m_head;
	for ( size_t i = 0; i < itemIndex; ++i )
	{
		curr = curr->GetNext();
	}

	return (curr);
}

//-----------------------------------------------------------------------------

template < class T >
void TCircularList< T >::_Append( const T& item )
{
	Item* newItem = new Item( item );

	if ( m_head )
	{
		newItem->SetNext( m_head );
		newItem->SetPrev( m_head->GetPrev() );
		m_head->SetPrev( newItem );
		newItem->GetPrev()->SetNext( newItem );
	}
	else
	{
		m_head = newItem;
		m_head->SetPrev( newItem );
		m_head->SetNext( newItem );
	}

	++m_itemsCount;
}

//-----------------------------------------------------------------------------
