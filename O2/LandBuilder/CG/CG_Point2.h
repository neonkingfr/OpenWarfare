//-----------------------------------------------------------------------------
// File: CG_Point2.h
//
// Desc: A two dimensional point
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// This class represents a point in two dimensions (euclidean plane XY).
//-----------------------------------------------------------------------------

#ifndef CG_POINT2_H
#define CG_POINT2_H

//-----------------------------------------------------------------------------

#include <ostream>

//-----------------------------------------------------------------------------

#include "CG_Consts.h"

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Point2 
{
protected:
	Real m_x;
	Real m_y;

public:
	CG_Point2( Real x = CG_Consts< Real >::ZERO, Real y = CG_Consts< Real >::ZERO );
	CG_Point2( const CG_Point2< Real >& other );
	virtual ~CG_Point2();

	// Assignement
	CG_Point2< Real >& operator = ( const CG_Point2< Real >& other );

	// Comparison
	bool Equals( const CG_Point2< Real >& other, 
               Real tolerance = CG_Consts< Real >::ZERO_TOLERANCE ) const;

	// Getters
	// {
	Real GetX() const;
	Real GetY() const;
	// }

	// Setters
	// {
	void SetX( Real x );
	void SetY( Real y );
	void Set( const CG_Point2< Real >& other );
	// }

	// Point manipulators
	// {
	// translates this point using the given displacements
	void Translate( Real dx, Real dy );

	// rotates CCW this point around the given center by the 
	// given angle in radians
	void Rotate( const CG_Point2< Real >& center, Real angle );
	// }

	// Operations with points
	// {
	Real SquaredDistance( const CG_Point2< Real >& other ) const;
	Real Distance( const CG_Point2< Real >& other ) const;

	// returns a copy of this point translated by the given displacements
	CG_Point2< Real > Translated( Real dx, Real dy ) const;

	// returns a copy of this point rotated CCW around the given center by the 
	// given angle in radians
	CG_Point2< Real > Rotated( const CG_Point2< Real >& center, Real angle ) const;
	// }
};

//-----------------------------------------------------------------------------

template < typename Real >
std::ostream& operator << ( std::ostream& o, const CG_Point2< Real >& p );

//-----------------------------------------------------------------------------

// returns true if the three given points are collinear 
template < typename Real >
bool Collinear( const CG_Point2< Real >& p1, const CG_Point2< Real >& p2,
                const CG_Point2< Real >& p3, Real tolerance );

//-----------------------------------------------------------------------------

#include "CG_Point2.inl"

//-----------------------------------------------------------------------------

#endif