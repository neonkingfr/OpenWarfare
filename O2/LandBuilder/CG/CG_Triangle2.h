//-----------------------------------------------------------------------------
// File: CG_Triangle2.h
//
// Desc: A two dimensional triangle
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// This class represents a triangle in two dimensions (euclidean plane XY).
//-----------------------------------------------------------------------------

#ifndef CG_TRIANGLE2_H
#define CG_TRIANGLE2_H

//-----------------------------------------------------------------------------

#include <assert.h>

//-----------------------------------------------------------------------------

#include "CG_Point2.h"
#include "CG_Rectangle2.h"

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Segment2; 

template < typename Real >
class CG_Triangle2 
{
	CG_Point2< Real > m_vertices[3];

public:
	CG_Triangle2();
	CG_Triangle2( const CG_Point2< Real >& v1, const CG_Point2< Real >& v2,
                const CG_Point2< Real >& v3 );
	~CG_Triangle2();

	// Triangle properties
	// {
	// Returns the signed area of this triangle
	Real SignedArea() const;

	// Returns the area of this triangle
	Real Area() const;

	// Returns the perimeter of this triangle
	Real Perimeter() const;

	// Returns the vertex of this triangle at the given index.
	const CG_Point2< Real >& Vertex( size_t index ) const;
	CG_Point2< Real >& Vertex( size_t index );

	// Returns the incenter (the point where the three angle bisectors intersect) 
	// of this triangle.
	CG_Point2< Real > InCenter() const;

	// Returns the edge opposite to the vertex of this triangle at the given index.
	CG_Segment2< Real > EdgeOppositeTo( size_t index ) const;

	// Returns the number of vertices of this polygon.
	size_t VerticesCount() const;

	// Returns the axis aligned rectangle containing this triangle.
	CG_Rectangle2< Real > AxisAlignedBoundingBox() const;

	// Returns the extreme values of the coordinates of the vertices
	// of this triangle.
	Real MinX() const;
	Real MaxX() const;
	Real MinY() const;
	Real MaxY() const;
  // }

	// Triangle manipulators
	// {
	// Sets the vertex of this triangle at the given index with the given vertex
	void SetVertex( size_t index, const CG_Point2< Real >& vertex );

	// Translates this triangle using the given displacements
	void Translate( Real dx, Real dy );

	// Rotates CCW this triangle around the given center by the 
	// given angle in radians
	void Rotate( const CG_Point2< Real >& center, Real angle );

	// Scales of hte given amout this triangle using the given center as origin
	void Scale( const CG_Point2< Real >& center, Real scaleFactor );
	// }

  // Containment
	// {
	// Returns true if this triangle contains the given point
	bool Contains( Real x, Real y, bool boundaries ) const;

  // Returns true if this triangle contains the given point
	bool Contains( const CG_Point2< Real >& point, bool boundaries )  const;
	// }
};

//-----------------------------------------------------------------------------

template < typename Real >
std::ostream& operator << ( std::ostream& o, const CG_Triangle2< Real >& t );

//-----------------------------------------------------------------------------

#include "CG_Triangle2.inl"

//-----------------------------------------------------------------------------

#endif