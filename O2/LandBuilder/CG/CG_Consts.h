//-----------------------------------------------------------------------------
// File: CG_Consts.h
//
// Desc: Mathematical constants
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#ifndef CG_CONSTS_H
#define CG_CONSTS_H

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Consts
{
public:
	static const Real PI;
	static const Real TWO_PI;
	static const Real HALF_PI;
	static const Real INV_PI;
	static const Real INV_TWO_PI;

	static const Real DEG_TO_RAD;
	static const Real RAD_TO_DEG;

  static const Real MAX_REAL;

  static const Real EPSILON;
  static const Real ZERO_TOLERANCE;

	static const Real ZERO;
	static const Real HALF;
	static const Real ONE;
	static const Real TWO;
};

//-----------------------------------------------------------------------------

#include "CG_Consts.inl"

//-----------------------------------------------------------------------------

#endif
