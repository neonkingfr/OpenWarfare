//-----------------------------------------------------------------------------
// File: CG_Line2.inl
//
// Desc: A two dimensional line
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

#include "CG_Consts.h"

//-----------------------------------------------------------------------------

template < typename Real >
CG_Line2< Real >::CG_Line2()
: m_a( CG_Consts< Real >::ZERO )
, m_b( CG_Consts< Real >::ZERO )
, m_c( CG_Consts< Real >::ZERO )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Line2< Real >::CG_Line2( const CG_Point2< Real >& p1, const CG_Point2< Real >& p2 )
{
	assert( !p1.Equals( p2 ) );
	_Initialize( p1, p2 );
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Line2< Real >::CG_Line2( const CG_Line2< Real >& other )
: m_a( other.m_a )
, m_b( other.m_b )
, m_c( other.m_c )
{
}

//-----------------------------------------------------------------------------

template < typename Real >
CG_Line2< Real >& CG_Line2< Real >::operator = ( const CG_Line2< Real >& other )
{
	m_a = other.m_a;
	m_b = other.m_b;
	m_c = other.m_c;
	return (*this);
}

//-----------------------------------------------------------------------------

template < typename Real >
bool CG_Line2< Real >::Equals( const CG_Line2< Real >& other, Real tolerance ) const
{
	if ( this == &other ) { return (true); }

	if ( abs( other.m_a - m_a ) > tolerance ) { return (false); }
	if ( abs( other.m_b - m_b ) > tolerance ) { return (false); }
	if ( abs( other.m_c - m_c ) > tolerance ) { return (false); }

	return (true);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Line2< Real >::GetA() const
{
	return (m_a);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Line2< Real >::GetB() const
{
	return (m_b);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Line2< Real >::GetC() const
{
	return (m_c);
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Line2< Real >::Direction() const
{
	if ( m_b == CG_Consts< Real >::ZERO )
	{
		return (CG_Consts< Real >::HALF_PI);
	}
	else if ( m_a == CG_Consts< Real >::ZERO )
	{
		return (CG_Consts< Real >::ZERO);
	}
	else
	{
		return (atan( -m_a / m_b ));
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
Real CG_Line2< Real >::Distance( const CG_Point2< Real >& point )
{
	Real num = abs( m_a * point.GetX() + m_b * point.GetY() + m_c );
	Real den = sqrt( m_a * m_a + m_b * m_b );
	return (num / den);
}

//-----------------------------------------------------------------------------

template < typename Real >
void CG_Line2< Real >::_Initialize( const CG_Point2< Real >& p1, const CG_Point2< Real >& p2 )
{
	Real epsilon = CG_Consts< Real >::ZERO_TOLERANCE;

	Real dx = p2.GetX() - p1.GetX();
	Real dy = p2.GetY() - p1.GetY();

	if ( abs( dx ) < epsilon )
	{
		if ( abs( dy ) < epsilon )
		{
			m_a = CG_Consts< Real >::ZERO;
			m_b = CG_Consts< Real >::ZERO;
			m_c = CG_Consts< Real >::ZERO;
		}
		else
		{
			m_a = CG_Consts< Real >::ONE;
			m_b = CG_Consts< Real >::ZERO;
			m_c = -p1.GetX();
		}
	}
	else if ( abs( dy ) < epsilon )
	{
		m_a = CG_Consts< Real >::ZERO;
		m_b = CG_Consts< Real >::ONE;
		m_c = -p1.GetY();
	}
	else
	{
		m_a = CG_Consts< Real >::ONE / dx;
		m_b = -CG_Consts< Real >::ONE / dy;
		m_c = -p1.GetX() / dx + p1.GetY() / dy;
	}
}

//-----------------------------------------------------------------------------

template < typename Real >
bool Intersect( const CG_Line2< Real >& line1, const CG_Line2< Real >& line2,
                CG_Point2< Real >& intersection )
{
	Real den = line1.GetB() * line2.GetA() - line1.GetA() * line2.GetB();
	if ( abs( den ) < CG_Consts< Real >::ZERO_TOLERANCE ) 
	{
		intersection.SetX( CG_Consts< Real >::MAX_REAL );
		intersection.SetY( CG_Consts< Real >::MAX_REAL );
		return (false);
	}

	Real numX = line1.GetC() * line2.GetB() - line1.GetB() * line2.GetC();
	Real numY = line1.GetA() * line2.GetC() - line1.GetC() * line2.GetA();

	intersection.SetX( numX / den );
	intersection.SetY( numY / den );

	return (true);
}

//-----------------------------------------------------------------------------
