//-----------------------------------------------------------------------------
// File: TCircularList.h
//
// Desc: a templated circular list
//
// The template class must have the following methods:
// - T( const T& other );
// - bool Equals( const T& other, Real tolerance ) const;
//
// The current implementation allows for duplicated items.
// 
// 
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#ifndef TCIRCULARLIST_H
#define TCIRCULARLIST_H

//-----------------------------------------------------------------------------

template < class T >
class TCircularList
{
protected:
	class Item
	{
		T     m_data;
		Item* m_prev;
		Item* m_next;

	public:
		/**
		\name Constructor
		*/
		//@{
		Item( const T& data );
		//@}

		/**
		\name Getters
		*/
		//@{
		Item* GetPrev();
		const Item* GetPrev() const;
		Item* GetNext();
		const Item* GetNext() const;
		T& GetData();
		const T& GetData() const;
		//@}

		/**
		\name Setters
		*/
		//@{
		void SetPrev( Item* prev );
		void SetNext( Item* next );
		void SetData( const T& data );
		//@}
	};

	// The head of the circular list
	Item* m_head;

	// The number of items
	size_t m_itemsCount;

public:
	/**
	\name Constructors
	*/
	//@{
	TCircularList();
	TCircularList( const TCircularList< T >& other );
	//@}

	/**
	\name Destructor
	*/
	//@{
	virtual ~TCircularList();
	//@}

	/*!
	Assignement
	*/
	//@{
	TCircularList< T >& operator = ( const TCircularList< T >& other );
	//@}

	/*!
	Items access
	*/
	//@{
	/*!
	Returns the item of this list at the given index.
	\param index the index of the desired item of this list (can be greater
	             then the number of items).
	\return the item of this vector at the given index.
	*/
	const T& operator [] ( int index ) const;
	T& operator [] ( int index );
	const T& operator [] ( size_t index ) const;
	T& operator [] ( size_t index );
	//@}

	/**
	\name List Manipulators
	*/
	//@{
	/*!
	Appends the given item to this list.
	\param item the item to append to this list.
	\return the item added to this list.
	*/
	T& Append( const T& item );
	const T& Append( const T& item ) const;

	/*!
	Clears this list.
	*/
	void RemoveAll();

	/*!
	Inserts the given item into this list at the given index position.
	\param index the position into the list where to insert the new item.
	\param item the item to insert.
	*/
	void Insert( int index, const T& item );

	/*!
	Removes the item at the given index position.
	\param index the position of the item to remove.
	*/
	void Remove( int index );

	/*!
	Reverses the order of the item of this list.
	*/
	void Reverse();

	/*!
	Removes consecutive duplicated items (only the first of them will 
	be left).
	\return the number of removed items.
	*/
	template < typename Real >
	size_t RemoveConsecutiveDuplicated( Real tolerance );

	/*!
	Slides the head of the circular list the given number of positions.
	\param count the number of positions to slide.
	*/
	void Slide( int count );
	//@}

	/**
	\name List Properties
	*/
	//@{
	/*!
	Returns the number of items of this list.
	\return the number of items of this list.
	*/
	size_t Size() const;

	/*!
	Returns a pointer to the item of this list which coincide with
	the given one or NULL if no match is found
	\param item the item to search for.
	\return a pointer to the item of this list which coincide with the given 
	        one or NULL if none.
	*/
	T* Find( const T& item ) const;

	/*!
	Returns the index of the item of this list which coincide with
	the given one or -1 if no match is found
	\param item the item to search for.
	\return the index of the item of this list which coincide with the given 
	        one or -1 if none.
	*/
	int FindIndex( const T& item ) const;
	//@}

private:
	void _CopyFrom( const TCircularList< T >& other );
	size_t _Index( int index ) const;
	Item* _GetAt( int index );
	const Item* _GetAt( int index ) const;
	void _Append( const T& item );
};

//-----------------------------------------------------------------------------

#include "TCircularList.inl"

//-----------------------------------------------------------------------------

#endif
