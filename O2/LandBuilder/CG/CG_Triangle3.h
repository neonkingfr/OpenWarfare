//-----------------------------------------------------------------------------
// File: CG_Triangle3.h
//
// Desc: A three dimensional triangle
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// This class represents a triangle in three dimensions (euclidean space XYZ).
//-----------------------------------------------------------------------------

#ifndef CG_TRIANGLE3_H
#define CG_TRIANGLE3_H

//-----------------------------------------------------------------------------

#include "CG_Point3.h"
#include "CG_Vector3.h"

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Triangle3 
{
protected:
	CG_Point3< Real > m_vertices[3];

public:
	CG_Triangle3();
	CG_Triangle3( const CG_Point3< Real >& v1, const CG_Point3< Real >& v2,
				  const CG_Point3< Real >& v3 );
	CG_Triangle3( const CG_Triangle3< Real >& other );
	virtual ~CG_Triangle3();

	// Triangle manipulators
	// {
	// Translates this triangle using the given displacements.
	void Translate( Real dx, Real dy, Real dz );
	void Translate( const CG_Vector3< Real >& displacement );

	// rotates CCW this triangle around the given axis passing through the given
	// center by the given angle in radians
	void Rotate( const CG_Vector3< Real >& axis, Real angle, 
				 const CG_Point3< Real >& center = CG_Point3< Real >( static_cast< Real >( 0 ), static_cast< Real >( 0 ) ) );

	// Rotates this triangle using the given rotation matrix
	void Rotate( const CG_Matrix3x3< Real >& matrix );
	// }

	// Triangle properties
	// {
	// Returns the vertex of this triangle at the given index.
	const CG_Point3< Real >& Vertex( size_t index ) const;

	// Returns the unit vector normal to the plane of this triangle
	CG_Vector3< Real > Normal() const;

	// Returns the perimeter of this triangle
	Real Perimeter() const;
	// }
};

//-----------------------------------------------------------------------------

#include "CG_Triangle3.inl"

//-----------------------------------------------------------------------------

#endif