//-----------------------------------------------------------------------------
// File: CG_Polyline2.h
//
// Desc: A two dimensional polyline
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// This class represents a polyline in two dimensions (euclidean plane XY)
//-----------------------------------------------------------------------------

#ifndef CG_POLYLINE_H
#define CG_POLYLINE_H

//-----------------------------------------------------------------------------

#include <vector>
#include <string>

//-----------------------------------------------------------------------------

#include "CG_Segment2.h"
#include "CG_Rectangle2.h"

//-----------------------------------------------------------------------------

using std::vector;
using std::string;

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Polygon2;

//-----------------------------------------------------------------------------

template < typename Real >
class CG_Polyline2
{
	vector< CG_Point2< Real > > m_vertices;

public:
	CG_Polyline2();
	CG_Polyline2( const CG_Polyline2< Real >& other );
	~CG_Polyline2();

	// Assignement.
	CG_Polyline2< Real >& operator = ( const CG_Polyline2< Real >& other );

	// Comparison
	bool Equals( const CG_Polyline2< Real >& other ) const;
	bool EqualsReverse( const CG_Polyline2< Real >& other ) const;

	// Polyline manipulators
	// {
	// Translates this polyline using the given displacements.
	void Translate( Real dx, Real dy );
	void Translate( const CG_Vector2< Real >& displacement );

	// Rotates this polyline around the given center by the 
	// given angle in radians (positive CCW).
	void Rotate( const CG_Point2< Real >& center, Real angle );
	// }

	// Vertices manipulators
	// {
	// Appends the given vertex.
	void AppendVertex( Real x, Real y );
	void AppendVertex( const CG_Point2< Real >& vertex );

	// Appends the vertices of the given polyline.
	// Skips the first vertex if it coincides with this polyline last vertex.
	void AppendVertices( const CG_Polyline2< Real >& other );

	// Inserts the given vertex in the given position.
	void InsertVertex( size_t index, Real x, Real y );
	void InsertVertex( size_t index, const CG_Point2< Real >& vertex );

	// Move the specified vertex to the given location.
	void MoveVertexTo( size_t index, Real x, Real y );
	void MoveVertexTo( size_t index, const CG_Point2< Real >& vertex );

	// Removes the vertex at the specified position.
	void RemoveVertex( size_t index );

	// Removes all vertices.
	void RemoveAllVertices();

	// Removes consecutive duplicated vertices (only the first of 
	// them will be left).
	// Returns the number of removed vertices.
	size_t RemoveConsecutiveDuplicatedVertices( Real tolerance );

	// Tries to remove consecutive aligned vertices.
	// The check is done using three consecutive vertices at a time
	// and removing the central one.
	// Returns the number of removed vertices.
	size_t RemoveConsecutiveAlignedVertices( Real tolerance );

	// Reverses the current vertices' order.
	void ReverseVerticesOrder();

	// Tries to remove vertices by searching approximately straight regions
  // and substituting them with single segments.
  // The parameter threshold is used to check for the quasi-straight condition. 
  // It is a percent value = distance of a point from the approximating
  // segment / length of the segment.
  // If the parameter fixedEndpoints is set to true, the endpoints of this polyline
  // will be not considered during the computation and will be left in their original
  // position.
	// Returns the number of removed vertices.
  size_t StraightenWherePossible( Real tolerance, Real threshold, bool fixedEndpoints );
	// }

	// Polyline properties
	// {
	// Returns the number of vertices of this polyline.
	size_t VerticesCount() const;

	// Returns the vertex of this polyline at the given index.
	const CG_Point2< Real >& Vertex( size_t index ) const;
	CG_Point2< Real >& Vertex( size_t index );

	// Returns the first vertex of this polyline.
	const CG_Point2< Real >& FirstVertex() const;

	// Returns the last vertex of this polyline.
	const CG_Point2< Real >& LastVertex() const;

	// Returns the first segment of this polyline.
	CG_Segment2< Real > FirstSegment() const;

	// Returns the last segment of this polyline.
	CG_Segment2< Real > LastSegment() const;

	// Returns the segment with the given index
	CG_Segment2< Real > Segment( size_t index ) const;

	// Returns the segment connecting the first and the last vertices of this polyline
	CG_Segment2< Real > Chord() const;

	// Returns the length of this polyline.
	Real Length() const;

	// Returns true if there are any two consecutive segments
	// overlapping.
	bool HasConsecutiveOverlappingSegments( Real tolerance ) const;

	// Returns the minimum rectangle containing this polyline.
	CG_Rectangle2< Real > MinimumBoundingBox() const;

	// Returns the axis aligned rectangle containing this polyline.
	CG_Rectangle2< Real > AxisAlignedBoundingBox() const;

	// Returns the barycenter of the vertices of this polyline.
	CG_Point2< Real > Barycenter() const;

	// Returns the closest point on this polyline to the given test point.
	// If pTangent and pNormal are not NULL, after return, they will 
	// contain the tangent and the normal unit vectors to the polyline in
	// the closest point.
	// If closest point coincide with a vertex of this polyline, the
	// tangent and normal vectors are the average of the vectors relative
	// to the two segments sharing the vertex. 
	// The normal is always rotated CCW with respect to the tangent.
	CG_Point2< Real > ClosestPointTo( const CG_Point2< Real >& point, Real tolerance,
                                    CG_Vector2< Real >* tangent, 
                                    CG_Vector2< Real >* normal ) const;

	Real MinX() const;
	Real MaxX() const;
	Real MinY() const;
	Real MaxY() const;

	// Returns the index of the segment containing the given coordinate
	// measured along this polyline
	// Returns -1 if the given parameter is negative
	// Returns (num segments) if the given parameter is greater than
	// the length of this polyline
	int SegmentIndexAt( Real length ) const;

	// Returns the world coordinates of the point at the given coordinate
	// measured along this polyline
	// Returns the first vertex of this polyline if the given parameter is negative
	// Returns the last vertex of this polyline if the given parameter is greater
	// than the length of this polyline
	CG_Point2< Real > PointAt( Real length ) const;
	// }

	// Operations with polyline
	// {
	// Splits this polyline in two new polylines each length/2 long.
	vector< CG_Polyline2< Real > > SplitInTwoHalves( Real tolerance ) const;

	// Splits this polyline in two new polylines cutting in the middle of the longest 
	// segment of this polyline.
	vector< CG_Polyline2< Real > > SplitOnLongestSegment( Real tolerance ) const;
	
	// Returns the sub-polyline of this polyline having the starting and
	// ending points at the given coordinates along this polyline.
	// If entireSegment is true, the segments containing the endpoints are
	// not cutted at the given coordinates.
	CG_Polyline2< Real > SubPart( Real startLength, Real endLength, bool entireSegments = true ) const;

	// Returns the length of the polyline from its starting point to the given one
	// Return zero if the given point does not belong to the polyline
	Real LengthAt( const CG_Point2< Real >& point, Real tolerance ) const;

	// Returns the length of the polyline from its starting point to the point closest
	// to the given one
	Real LengthAtClosestPointTo( const CG_Point2< Real >& point, Real tolerance ) const;

	// Returns the mid point of this polyline
	CG_Point2< Real > MidPoint() const;

	// Returns the mid point of the longest segment of this polyline
	CG_Point2< Real > MidPointOfLongestSegment() const;

  // Returns the list of polylines obtained by clipping this polyline with the given polygon.
  // The returned polylines are completely contained into the clipping polygon.
  // The list will be empty if this polyline is completely outside the clipping polygon.
  // The list will contain only one polyline (a copy of this) if this polyline is completely
  // inside the clipping polygon.
  // The list will contain one or more polylines if this polyline is partly contained into the
  // clipping polygon.
  vector< CG_Polyline2< Real > > Clip( const CG_Polygon2< Real >& clipPolygon ) const;

  // Returns true if this polyline is approximately straight.
  // The parameter threshold is used to check for the quasi-straight condition. 
  // It is a percent value = distance of a point from the approximating
  // segment / length of the segment.
  // The parameter approximatingSegment will contain the approximating segment when the method
  // returns true.
  bool IsApproximatelyStraight( Real tolerance, Real threshold, CG_Segment2< Real >& approximatingSegment ) const;
  // }

private:
	// Splits this polyline in two parts at the given coordinate measured along
	// the polyline.
	vector< CG_Polyline2< Real > > Split( Real length, Real tolerance ) const;

	// Returns the index of the longest segment of this polyline
	int LongestSegmentIndex() const;
};

//-----------------------------------------------------------------------------

#include "CG_Polyline2.inl"

//-----------------------------------------------------------------------------

#endif