//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <vector>

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\landbuilder2\MapInput.h"

#include "..\..\HighMapLoaders\include\EtRectElevationGrid.h"
#include "..\..\HighMapLoaders\include\EtRectangle.h"

#include "..\..\Shapes\Vertex.h"

//-----------------------------------------------------------------------------

using std::vector;

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
  namespace Modules
  {

//-----------------------------------------------------------------------------

    const double cNODATA = -32757.0;

    class SmoothTerrainAlongPath : public IBuildModule
    {
    public:
      struct ShapeInput
      {
        double m_width;
      };

      class FixedVertex
      {
        int    m_x;
        int    m_y;
        double m_z;
        size_t m_count;

      public:
        FixedVertex( int x, int y, double z )
        : m_x( x )
        , m_y( y )
        , m_z( z )
        , m_count( 1 )
        {
        }

        int GetX() const
        {
          return (m_x);
        }

        int GetY() const
        {
          return (m_y);
        }

        double GetZ() const
        {
          return (m_z);
        }

        void SetZ( double z )
        {
          m_z = z;
        }

        void AverageZ()
        {
          m_z /= m_count;
          m_count = 1;
        }

        size_t GetCount()
        {
          return (m_count);
        }

        void IncrementCount( double z )
        {
          m_z += z;
          m_count++;
        }
      };

      class FixedVerticesList
      {
        vector< FixedVertex > m_list;

      public:
        FixedVerticesList()
        {
        }

        void AddVertex( const FixedVertex& inV )
        {
          int res = Contains( inV.GetX(), inV.GetY() ); 
          if ( res != -1 )
          {
            m_list[res].IncrementCount( inV.GetZ() );
          }
          else
          {
            m_list.push_back( inV );
          }
        }

        FixedVertex& GetVertex( size_t index )
        {
          return (m_list[index]);
        }

        const FixedVertex& GetVertex( size_t index ) const
        {
          return (m_list[index]);
        }

        double GetZAt( int x, int y )
        {
          size_t verticesCount = m_list.size();
          for ( size_t i = 0; i < verticesCount; ++i )
          {
            const FixedVertex& v = m_list[i];
            if ( v.GetX() == x && v.GetY() == y )
            {
              return (v.GetZ());
            }
          }
          return (cNODATA);
        }

        int Contains( int x, int y )
        {
          size_t verticesCount = m_list.size();
          for ( size_t i = 0; i < verticesCount; ++i )
          {
            const FixedVertex& v = m_list[i];
            if ( v.GetX() == x && v.GetY() == y )
            {
              return (i);
            }
          }
          return -(1);
        }

        size_t Size() const
        {
          return (m_list.size());
        }
      };

    public:
      SmoothTerrainAlongPath();

      virtual ~SmoothTerrainAlongPath()
      {
      }

      virtual void Run( IMainCommands* cmdIfc );
      virtual void OnEndPass( IMainCommands* cmdIfc );

      DECLAREMODULEEXCEPTION( "SmoothTerrainAlongPath" );

    private:
      string m_version;

      bool   m_firstRun;
      bool   m_canProceed;
      size_t m_shapeCounter;

      RString m_filePrefix;

      string m_longSmoothType;
      double m_longSmoothPar;

      FixedVerticesList m_underFixedList;

      const Shapes::IShape* m_currentShape;      

      ShapeInput m_shapeIn;
      MapInput   m_mapIn;

      EtRectElevationGrid< double, int > m_mapData;
      AutoArray< Shapes::DVector >       m_shapeProfile;

      bool GetGlobalParameters( IMainCommands* cmdIfc );
      bool GetShapeParameters( IMainCommands* cmdIfc );
      bool GetMapParameters( IMainCommands* cmdIfc );
      bool LoadHighMap();
      void SmoothProfile();
      void ProfileAsShape();
      void ProfilePointAtSteps( double step );
      void GetProfileElevations();
      void SmoothWithMovingAverage();
      void SmoothWithMaxSlope();
      void SmoothWithMonotone();
      void SmoothWithConstantSlope();
      void SmoothWithFixedAbsolute();
      void SmoothWithFixedRelative();
      void SmoothTerrainUnderPath();
      void SmoothSurroundingTerrain();
      void SaveTerrain();
      EtRectangle< double > SegmentBoundingBox( const Shapes::DVertex& v0, const Shapes::DVertex& v1 );
    };

//-----------------------------------------------------------------------------

  } // namespace Modules
} // namespace LandBuildInt