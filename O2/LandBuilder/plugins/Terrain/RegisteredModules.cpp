#include "StdAfx.h"
#include "ModuleRegistrator.h"

namespace LandBuilder2
{
	using namespace LandBuildInt;
}

//source contains list of registered modules. 
/**Don't add new modules here. Instead, register module using the registrator 
at place of module's definition
*/
#include "CountourLines.h"
#include "HighMapTest.h"
#include "HighMapLoader.h"
#include "SmoothTerrainAlongPath.h"

namespace LandBuilder2
{
	RegisterModule< Modules::CountourLines >          _register_TE_001( "CountourLines" );
	RegisterModule< Modules::HeightMapTest >          _register_TE_002( "HighMapTest" );
	RegisterModule< Modules::HeightMapLoader >        _register_TE_003( "HighMapLoader" );
	RegisterModule< Modules::SmoothTerrainAlongPath > _register_TE_004( "SmoothTerrainAlongPath" );
}