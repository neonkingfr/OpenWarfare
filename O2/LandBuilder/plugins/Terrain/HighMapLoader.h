//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>
#include <vector>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\MapInput.h"

#include "..\..\HighMapLoaders\include\EtRectElevationGrid.h"

//-----------------------------------------------------------------------------

using std::string;
using std::vector;

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
  namespace Modules
  {

//-----------------------------------------------------------------------------

    class HeightMapLoader : public IBuildModule
    {
    public:
      DECLAREMODULEEXCEPTION( "HeightMapLoader" );

      virtual ~HeightMapLoader()
      {
      }

      virtual void Run( IMainCommands* cmdIfc );

    private:
      vector< MapInput > m_sources;

      bool GetSourcesParameters( IMainCommands* cmdIfc );
      bool LoadSourcesHighMaps();
    };

//-----------------------------------------------------------------------------

  } // namespace Modules
} // namespace LandBuildInt