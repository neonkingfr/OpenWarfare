// --------------------------------------------------------------------------------

#include "StdAfx.h"
#include <string>

#include "..\..\Shapes\IShape.h"
#include "..\..\LandBuilder2\IShapeExtra.h"
#include "..\..\HighMapLoaders\include\EtHelperFunctions.h"

#include ".\BuildingCreator.h"

// --------------------------------------------------------------------------------

using std::string;

// --------------------------------------------------------------------------------

// static const bool EXPORT_DXF = true;

// --------------------------------------------------------------------------------

namespace LandBuildInt
{

	namespace Modules
	{

// --------------------------------------------------------------------------------

		BuildingCreator::BuildingCreator()
		: m_firstRun( true )
		, m_shapeCounter( 0 )
    , m_modelsPath( "" )
    , m_texturesPath( "" )
    , m_version( "1.1.2" )
		{
		}

// --------------------------------------------------------------------------------

		void BuildingCreator::Run( IMainCommands* cmdIfc )
		{
      RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
      if ( taskName != m_currTaskName )
      {
        m_shapeCounter = 0;
        m_firstRun     = true;
        m_modelsPath   = "";
        m_texturesPath = "";
        m_currTaskName = taskName;
      }

			++m_shapeCounter;

			// gets shape and its properties
			m_currentShape = cmdIfc->GetActiveShape();  

      if ( !m_currentShape ) { return; }

      LOGF( Info ) << "                                                           ";
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "BuildingCreator " << m_version << " - Started shape n. " << m_shapeCounter;
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "Parameters:";

			if ( m_firstRun )
			{
			  // gets parameters from calling cfg file
			  const char* x = cmdIfc->QueryValue( "modelsPath", true );
			  if ( x ) 
			  {				  
          m_modelsPath = x;
			  }
			  else
			  {
				  ExceptReg::RegExcpt( Exception( 21, ">>> Missing modelsPath value <<<" ) );
				  return;
			  }

			  if ( m_modelsPath[m_modelsPath.GetLength() - 1] != '\\' )
			  {
				  m_modelsPath = m_modelsPath + "\\";
			  }

			  // TODO: add check for valid path

			  m_texturesPath = m_modelsPath + "Data\\";

				Pathname::CreateFolder( m_texturesPath );

//				RString dxfFileName = m_texturesPath + "Buildings.dxf";
//				m_dxfWriter.Set( dxfFileName.Data() );

			  m_vertexTol = 10; // using 5 degrees
			  m_rectCornerTol = 10; // using 10 degrees

        m_firstRun = false;
      }

      // checks for multipart polygons
			if ( m_currentShape->GetPartCount() > 1 )
			{
        LOGF( Warn ) << "Skipped multipart shape: " << m_shapeCounter;
			}
			else
			{
				// checks for known shapes
				const IShapeExtra* extra = m_currentShape->GetInterface< IShapeExtra >();
				if ( !extra ) 
				{
					ExceptReg::RegExcpt( Exception( 12, ">>> Found unknown shape in shapefile. Cannot get type of that shape <<<" ) );
					return;
				}

				// checks for polylines only
				RString shapeType = extra->GetShapeTypeName();
				if ( shapeType != RString( SHAPE_NAME_POLYGON ) ) 
				{
					ExceptReg::RegExcpt( Exception( 11, ">>> BuildingCreator module accepts only shapefiles containing non-multipart polygons <<<" ) );
					return;
				}

				// skips polygons with less than 3 vertices
				if ( m_currentShape->GetVertexCount() > 2 )
				{
					if ( GetShapeParameters( cmdIfc ) ) 
          { 
					  // here vertexTol is in degrees
					  vector< Shapes::DVertex > vertices;
					  for ( size_t i = 0, cnt = m_currentShape->GetVertexCount(); i < cnt; ++i )
					  {
						  vertices.push_back( m_currentShape->GetVertex( i ) );
					  }

					  Footprint footprint( vertices, m_vertexTol, m_rectCornerTol, m_shapeIn.m_rectify );

					  vector< Texture > textures;
					  if ( footprint.GetVerticesCount() > 2 )
					  {
						  if ( !(m_shapeIn.m_texFile == "none") )
						  {
							  Texture texture( m_texturesPath, m_shapeIn.m_texFile, 0 );
							  textures.push_back( texture );
						  }
						  Building building = Building( footprint, m_shapeIn.m_numOfFloors, m_shapeIn.m_maxNumFloors, m_shapeIn.m_floorHeight, m_shapeIn.m_roofType, (double)m_shapeIn.m_roofRed / 255.0, (double)m_shapeIn.m_roofGreen / 255.0, (double)m_shapeIn.m_roofBlue / 255.0 );
						  building.GenerateP3DModel( cmdIfc, m_modelsPath, m_texturesPath, textures, m_modelsLibrary );
					  }
          }
				}

//				if ( EXPORT_DXF )
//				{
//					ExportDxf();
//				}
			}
		}

// --------------------------------------------------------------------------------

		bool BuildingCreator::GetShapeParameters( IMainCommands* cmdIfc )
		{
      // randomSeed (isMovable)
			const char* x = cmdIfc->QueryValue( "randomSeed", true );
      if ( !x ) { x = cmdIfc->QueryValue( "randomSeed", false ); }

      if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_randomSeed = static_cast< size_t >( atoi( x ) );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 28, ">>> Bad data for randomSeed <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 29, ">>> Missing randomSeed value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "randomSeed: " << m_shapeIn.m_randomSeed;

			// numOfFloor (isMovable)
      x = cmdIfc->QueryValue( "numOfFloor", true );
			if ( !x ) { x = cmdIfc->QueryValue( "numOfFloor", false ); }

			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_numOfFloors = atoi( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 31, ">>> Bad data for numOfFloor <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 32, ">>> Missing numOfFloor value <<<" ) );
				return (false);
			}

      if ( m_shapeIn.m_numOfFloors < 0 )
      {
        ExceptReg::RegExcpt( Exception( 33, ">>> Bad data for numOfFloor value: it must be a number >= 0 <<<" ) );
				return (false);
      }

      LOGF( Info ) << "numOfFloor: " << m_shapeIn.m_numOfFloors;

			// maxNumFloors (isMovable && hasDefault)
			m_shapeIn.m_maxNumFloors = 3; // default value
      x = cmdIfc->QueryValue( "maxNumFloors", true );
			if ( !x ) { x = cmdIfc->QueryValue( "maxNumFloors", false ); }

			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_maxNumFloors = atoi( x );
				}
				else
				{
          LOGF( Warn ) << ">>> --------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for maxNumFloors - using default value (3) <<<";
          LOGF( Warn ) << ">>> --------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ---------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing maxNumFloors value - using default value (3) <<<";
        LOGF( Warn ) << ">>> ---------------------------------------------------- <<<";
			}

      if ( m_shapeIn.m_maxNumFloors < 1 )
      {
        m_shapeIn.m_maxNumFloors = 3;
        LOGF( Warn ) << ">>> ----------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Wrong data for maxNumFloors - using default value (3) <<<";
        LOGF( Warn ) << ">>> ----------------------------------------------------- <<<";
      }

      LOGF( Info ) << "maxNumFloors: " << m_shapeIn.m_maxNumFloors;

			// floorHeight (isMovable)
      x = cmdIfc->QueryValue( "floorHeight", true );
			if ( !x ) { x = cmdIfc->QueryValue( "floorHeight", false ); }

      if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_floorHeight = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 33, ">>> Bad data for floorHeight <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 34, ">>> Missing floorHeight value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "floorHeight: " << m_shapeIn.m_floorHeight;

			// roofRed (isMovable)
      x = cmdIfc->QueryValue( "roofRed", true );
			if ( !x ) { x = cmdIfc->QueryValue( "roofRed", false ); }

			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_roofRed = atoi( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 41, ">>> Bad data for roofRed <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 42, ">>> Missing roofRed value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "roofRed: " << m_shapeIn.m_roofRed;

			// roofGreen (isMovable)
      x = cmdIfc->QueryValue( "roofGreen", true );
			if ( !x ) { x = cmdIfc->QueryValue( "roofGreen", false ); }

			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_roofGreen = atoi( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 43, ">>> Bad data for roofGreen <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 44, ">>> Missing roofGreen value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "roofGreen: " << m_shapeIn.m_roofGreen;

			// roofBlue (isMovable)
      x = cmdIfc->QueryValue( "roofBlue", true );
			if ( !x ) { x = cmdIfc->QueryValue( "roofBlue", false ); }

			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_roofBlue = atoi( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 45, ">>> Bad data for roofBlue <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 46, ">>> Missing roofBlue value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "roofBlue: " << m_shapeIn.m_roofBlue;

			// roofType (isMovable)
      x = cmdIfc->QueryValue( "roofType", true );
			if ( !x ) { x = cmdIfc->QueryValue( "roofType", false ); }

			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_roofType = atoi( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 35, ">>> Bad data for roofType <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 36, ">>> Missing roofType value <<<" ) );
				return (false);
			}

			if ( m_shapeIn.m_roofType > 1 )
			{
				m_shapeIn.m_roofType = 0;
        LOGF( Info ) << ">>> ------------------------------------------------------- <<<";
        LOGF( Info ) << ">>> roofType non supported - using default value (0 - None) <<<";
        LOGF( Info ) << ">>> ------------------------------------------------------- <<<";
			}

      LOGF( Info ) << "roofType: " << m_shapeIn.m_roofType;

			// rectify (isMovable)
      x = cmdIfc->QueryValue( "rectify", true );
			if ( !x ) { x = cmdIfc->QueryValue( "rectify", false ); }

      if ( x ) 
			{
				if ( IsBool( string( x ), false ) )
				{
					int i = static_cast< int >( atoi( x ) );
					if ( i == 0 )
					{
						m_shapeIn.m_rectify = false;
					}
					else
					{
						m_shapeIn.m_rectify = true;
					}
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 37, ">>> Bad data for rectify <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 38, ">>> Missing rectify value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "rectify: " << m_shapeIn.m_rectify;

			// texFile (isMovable)
      x = cmdIfc->QueryValue( "texFile", true );
			if ( !x ) { x = cmdIfc->QueryValue( "texFile", false ); }

			if ( x ) 
			{
				m_shapeIn.m_texFile = x;

				m_shapeIn.m_texFile.Lower();
				int pos = m_shapeIn.m_texFile.GetLength() - 3;
				
				if ( pos < 0 )
				{
					m_shapeIn.m_texFile = "none";
				}
			}
			else
			{
				m_shapeIn.m_texFile = "none";
			}

      LOGF( Info ) << "texFile: " << m_shapeIn.m_texFile;

			return (true);
		}

// --------------------------------------------------------------------------------

//		void BuildingCreator::ExportDxf()
//		{
//			if ( m_modelsPath.GetLength() == 0 ) { return; }
//
//			Footprint fp = m_building.GetFootprint();
//			m_dxfWriter.WritePolyline( fp.GetVertices(), "1", true );
//			const BuildingModelP3D& modelP3D = m_building.GetModelP3D();
//			const vector< CGPolygon2 >& partitions = modelP3D.GetPartitions();
//			Shapes::DVertex pos = modelP3D.GetPosition();
//			double angle = modelP3D.GetOrientation();
//			for ( size_t i = 0, cntI = partitions.size(); i < cntI; ++i )
//			{
//				vector< Shapes::DVertex > polygon;
//				const CGPolygon2& part = partitions[i];
//				for ( size_t j = 0, cntJ = part.VerticesCount(); j < cntJ; ++j )
//				{
//					const CGPoint2& ver = part.Vertex( j );
//					double x = pos.x + ver.GetX() * cos( angle ) - ver.GetY() * sin( angle );
//					double y = pos.y + ver.GetX() * sin( angle ) + ver.GetY() * cos( angle );
//					polygon.push_back( Shapes::DVertex( x, y ) );
//				}
//				m_dxfWriter.WritePolyline( polygon, "tri", true );
//			}
//		}

// --------------------------------------------------------------------------------
	} // namespace Modules
} // namespace LandBuildInt