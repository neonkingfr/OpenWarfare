//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\CG\CG_Polygon2.h"

#include ".\BuildingCreatorHelpers.h"

//-----------------------------------------------------------------------------

using namespace BC_Help;

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		class BuildingCreator : public IBuildModule
		{
      typedef CG_Point2< double >   CGPoint2;
      typedef CG_Polygon2< double > CGPolygon2;

		public:
			struct ShapeInput
			{
				size_t  m_randomSeed;
				size_t  m_numOfFloors;
				size_t  m_maxNumFloors;
				double  m_floorHeight;
				size_t  m_roofType;
				bool    m_rectify;
				RString m_texFile;
				size_t  m_roofRed;
				size_t  m_roofGreen;
				size_t  m_roofBlue;
			};

      DECLAREMODULEEXCEPTION( "BuildingCreator" )

		public:
			BuildingCreator();
			virtual ~BuildingCreator()
      {
      }

			virtual void Run( IMainCommands* cmdIfc );

		private:
			bool m_firstRun;
      string m_version;

		  RString m_modelsPath;
		  RString m_texturesPath;

			double m_vertexTol;
			double m_rectCornerTol;

			const Shapes::IShape* m_currentShape; 
			ShapeInput            m_shapeIn;

//			Dxf2006Writer m_dxfWriter;

			size_t m_shapeCounter;

			vector< BuildingModelP3D > m_modelsLibrary;

			bool GetShapeParameters( IMainCommands* cmdIfc );

//			void ExportDxf();
		};

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt
