//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include ".\dummybuildingplacer.h"

#include "..\..\HighMapLoaders\include\EtHelperFunctions.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		DummyBuildingPlacer::DummyBuildingPlacer()
    : m_version( "1.1.0" )
		{
			// defining the possible L-Shape configuration in string form
			m_strLShapeConfiguration[0]  = "RLLLLL";
			m_strLShapeConfiguration[1]  = "LRLLLL";
			m_strLShapeConfiguration[2]  = "LLRLLL";
			m_strLShapeConfiguration[3]  = "LLLRLL";
			m_strLShapeConfiguration[4]  = "LLLLRL";
			m_strLShapeConfiguration[5]  = "LLLLLR";
			m_strLShapeConfiguration[6]  = "LRRRRR";
			m_strLShapeConfiguration[7]  = "RLRRRR";
			m_strLShapeConfiguration[8]  = "RRLRRR";
			m_strLShapeConfiguration[9]  = "RRRLRR";
			m_strLShapeConfiguration[10] = "RRRRLR";
			m_strLShapeConfiguration[11] = "RRRRRL";

			// array defining the sign of the direction of odd edges for every possible 
			// L-Shape configuration 
			m_lShapeOddEdgeSigns[0]  = "++-";
			m_lShapeOddEdgeSigns[1]  = "++-";
			m_lShapeOddEdgeSigns[2]  = "+--";
			m_lShapeOddEdgeSigns[3]  = "+--";
			m_lShapeOddEdgeSigns[4]  = "+-+";
			m_lShapeOddEdgeSigns[5]  = "+-+";
			m_lShapeOddEdgeSigns[6]  = "++-";
			m_lShapeOddEdgeSigns[7]  = "++-";
			m_lShapeOddEdgeSigns[8]  = "+--";
			m_lShapeOddEdgeSigns[9]  = "+--";
			m_lShapeOddEdgeSigns[10] = "+-+";
			m_lShapeOddEdgeSigns[11] = "+-+";

			// array defining the sign of the direction of even edges for every possible 
			// L-Shape configuration 
			m_lShapeEvenEdgeSigns[0]  = "+-+";
			m_lShapeEvenEdgeSigns[1]  = "++-";
			m_lShapeEvenEdgeSigns[2]  = "++-";
			m_lShapeEvenEdgeSigns[3]  = "+--";
			m_lShapeEvenEdgeSigns[4]  = "+--";
			m_lShapeEvenEdgeSigns[5]  = "+-+";
			m_lShapeEvenEdgeSigns[6]  = "+-+";
			m_lShapeEvenEdgeSigns[7]  = "++-";
			m_lShapeEvenEdgeSigns[8]  = "++-";
			m_lShapeEvenEdgeSigns[9]  = "+--";
			m_lShapeEvenEdgeSigns[10] = "+--";
			m_lShapeEvenEdgeSigns[11] = "+-+";
		}

//-----------------------------------------------------------------------------

		void DummyBuildingPlacer::Run( IMainCommands* cmdIfc )
		{
      LOGF( Info ) << "";
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "DummyBuildingPlacer " << m_version << " - Started a new shape ";
      LOGF( Info ) << "===========================================================";

			// gets shape and its properties
			m_currentShape = cmdIfc->GetActiveShape(); 

      if ( !m_currentShape ) { return; }

			// gets parameters from calling cfg file
			if ( GetGlobalParameters( cmdIfc ) )
			{
				// gets dbf parameters from calling cfg file
				if ( GetDbfParameters( cmdIfc ) )
				{
					// the following operations are needed to correct digitalization errors
					// (we want real rectangle and L-shapes frames)

					// sets the geometry of the shape
					if ( SetGeometry() )
					{
						// calculates mean UV directions
						CalculateMeanUVDirections();

						// orthogonalizes the UV directions
						OrthogonalizeUV();

						// reshape the shape to be a regular polygon
						// (removes digitalization errors)
						ReshapeVertices();

						// creates objects to export
						CreateObjects();

						// export the shape
						ExportCreatedObjects( cmdIfc );
					}
					else
					// skips the shape if SetGeometry returns false
					{
            LOGF( Warn ) << "Shape skipped                  ";
					}
				}
			}
		}

//-----------------------------------------------------------------------------

		ModuleObjectOutputArray* DummyBuildingPlacer::RunAsPreview( const Shapes::IShape* shape, 
                                                                GlobalInput& globalIn, 
                                                                DbfInput& dbfIn )
		{
			m_currentShape  = shape;
			m_globalIn      = globalIn;
			m_dbfIn         = dbfIn;

      if ( !m_currentShape ) { return (NULL); }

      if ( m_objectsOut.Size() != 0 ) { m_objectsOut.Clear(); }

			// the following operations are needed to correct digitalization errors
			// (we want real rectangle and L-shapes frames)

			// sets the geometry of the shape
			if ( SetGeometry() )
			{
				// calculates mean UV directions
				CalculateMeanUVDirections();

				// orthogonalizes the UV directions
				OrthogonalizeUV();

				// reshape the shape to be a regular polygon
				// (removes digitalization errors)
				ReshapeVertices();

				// creates objects to export
				CreateObjects();

				return (&m_objectsOut);
			}
			else
			// skips the shape if SetGeometry returns false
			{
				return (NULL);
			}
		}

//-----------------------------------------------------------------------------

		bool DummyBuildingPlacer::GetGlobalParameters( IMainCommands* cmdIfc )
		{
			// gets parameters from calling cfg file
			const char* x = cmdIfc->QueryValue( "modelCivil", false );
			if ( x ) 
			{
				m_globalIn.m_modelCivil = x;
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 11, ">>> Missing modelCivil value <<<" ) );
				return (false);
			}

			x = cmdIfc->QueryValue( "modelIndustrial", false );
			if ( x ) 
			{
				m_globalIn.m_modelIndustrial = x;
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 12, ">>> Missing modelIndustrial value <<<" ) );
				return (false);
			}

			x = cmdIfc->QueryValue( "modelMilitary", false );
			if ( x ) 
			{
				m_globalIn.m_modelMilitary = x;
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 13, ">>> Missing modelMilitary value <<<" ) );
				return (false);
			}

			m_globalIn.m_maxNumFloorsCivil = 3; // default value
			x = cmdIfc->QueryValue( "maxNumFloorsCivil", false );
			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_globalIn.m_maxNumFloorsCivil = atoi( x );
				}
				else
				{
          LOGF( Warn ) << ">>> -------------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for maxNumFloorsCivil - using default value (3) <<<";
          LOGF( Warn ) << ">>> -------------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> --------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing maxNumFloorsCivil value - using default value (3) <<<";
        LOGF( Warn ) << ">>> --------------------------------------------------------- <<<";
			}
			if ( m_globalIn.m_maxNumFloorsCivil < 1 )
			{
				ExceptReg::RegExcpt( Exception( 14, " >>> maxNumFloorsCivil MUST be equal or greater than 1 <<<" ) );
				return (false);
			}

			m_globalIn.m_maxNumFloorsIndustrial = 2; // default value
			x = cmdIfc->QueryValue( "maxNumFloorsIndustrial", false );
			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_globalIn.m_maxNumFloorsIndustrial = atoi( x );
				}
				else
				{
          LOGF( Warn ) << ">>> ------------------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for maxNumFloorsIndustrial - using default value (2) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> -------------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing maxNumFloorsIndustrial value - using default value (2) <<<";
        LOGF( Warn ) << ">>> -------------------------------------------------------------- <<<";
			}
			if ( m_globalIn.m_maxNumFloorsIndustrial < 1 )
			{
				ExceptReg::RegExcpt( Exception( 15, ">>> maxNumFloorsIndustrial MUST be equal or greater than 1 <<<" ) );
				return (false);
			}

			m_globalIn.m_maxNumFloorsMilitary = 1; // default value
			x = cmdIfc->QueryValue( "maxNumFloorsMilitary", false );
			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_globalIn.m_maxNumFloorsMilitary = atoi( x );
				}
				else
				{
          LOGF( Warn ) << ">>> ----------------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for maxNumFloorsMilitary - using default value (1) <<<";
          LOGF( Warn ) << ">>> ----------------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ------------------------------------------------------------ <<<";
        LOGF( Warn ) << ">>> Missing maxNumFloorsMilitary value - using default value (1) <<<";
        LOGF( Warn ) << ">>> ------------------------------------------------------------ <<<";
			}
			if ( m_globalIn.m_maxNumFloorsMilitary < 1 )
			{
				ExceptReg::RegExcpt( Exception( 16, ">>> maxNumFloorsMilitary MUST be equal or greater than 1 <<<" ) );
				return (false);
			}

			m_globalIn.m_heightFloorsCivil = 3.0; // default value
			x = cmdIfc->QueryValue( "heightFloorsCivil", false );
			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_globalIn.m_heightFloorsCivil = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> ---------------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for heightFloorsCivil - using default value (3.0) <<<";
          LOGF( Warn ) << ">>> ---------------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ----------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing heightFloorsCivil value - using default value (3.0) <<<";
        LOGF( Warn ) << ">>> ----------------------------------------------------------- <<<";
			}
			if ( m_globalIn.m_heightFloorsCivil < 3.0 )
			{
        LOGF( Warn ) << ">>> --------------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Value for heightFloorsCivil too low - using default value (3.0) <<<";
        LOGF( Warn ) << ">>> --------------------------------------------------------------- <<<";
				m_globalIn.m_heightFloorsCivil = 3.0;
			}

			m_globalIn.m_heightFloorsIndustrial = 3.0; // default value
			x = cmdIfc->QueryValue( "heightFloorsIndustrial", false );
			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_globalIn.m_heightFloorsIndustrial = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> --------------------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for heightFloorsIndustrial - using default value (3.0) <<<";
          LOGF( Warn ) << ">>> --------------------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ---------------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing heightFloorsIndustrial value - using default value (3.0) <<<";
        LOGF( Warn ) << ">>> ---------------------------------------------------------------- <<<";
			}
			if ( m_globalIn.m_heightFloorsIndustrial < 3.0 )
			{
        LOGF( Warn ) << ">>> -------------------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Value for heightFloorsIndustrial too low - using default value (3.0) <<<";
        LOGF( Warn ) << ">>> -------------------------------------------------------------------- <<<";
				m_globalIn.m_heightFloorsIndustrial = 3.0;
			}

			m_globalIn.m_heightFloorsMilitary = 3.0; // default value
			x = cmdIfc->QueryValue( "heightFloorsMilitary", false );
			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_globalIn.m_heightFloorsMilitary = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> ------------------------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for heightFloorsMilitary - using default value (3.0) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------------------- <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> -------------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing heightFloorsMilitary value - using default value (3.0) <<<";
        LOGF( Warn ) << ">>> -------------------------------------------------------------- <<<";
			}
			if ( m_globalIn.m_heightFloorsMilitary < 3.0 )
			{
        LOGF( Warn ) << ">>> ------------------------------------------------------------------ <<<";
        LOGF( Warn ) << ">>> Value for heightFloorsMilitary too low - using default value (3.0) <<<";
        LOGF( Warn ) << ">>> ------------------------------------------------------------------ <<<";
				m_globalIn.m_heightFloorsMilitary = 3.0;
			}

			return (true);
		}

//-----------------------------------------------------------------------------

		bool DummyBuildingPlacer::GetDbfParameters( IMainCommands* cmdIfc )
		{
			// gets shape type
			m_dbfIn.m_buildingType = btNULL;
			const char* x = cmdIfc->QueryValue( "buildingType", false );
			if ( x ) 
			{
				if ( string( x ) == "CIVIL" )      
        {
          m_dbfIn.m_buildingType = btCIVIL;
        }
				else if ( string( x ) == "INDUSTRIAL" ) 
        {
          m_dbfIn.m_buildingType = btINDUSTRIAL;
        }
				else if ( string( x ) == "MILITARY" )   
        {
          m_dbfIn.m_buildingType = btMILITARY;
        }
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 21, ">>> Missing buildingType value <<<" ) );
				return (false);
			}
			
			// checks if existing shape type
			if ( m_dbfIn.m_buildingType == btNULL )
			{
				ExceptReg::RegExcpt( Exception( 22, ">>> buildingType not implemented <<<" ) );
				return (false);
			}

			// gets number of floor
			// 0 means random
			x = cmdIfc->QueryValue( "numOfFloor", false );
			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_dbfIn.m_numOfFloors = atoi( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 23, ">>> Bad data for numOfFloor <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 24, ">>> Missing numOfFloor value <<<" ) );
				return (false);
			}

			// checks if positive
			if ( m_dbfIn.m_numOfFloors < 0 )
			{
				ExceptReg::RegExcpt( Exception( 25, ">>> numOfFloor MUST be a positive value <<<" ) );
				return (false);
			}

			return (true);
		}

//-----------------------------------------------------------------------------

		// returns false if the shape has no 4 or 6 vertices or if it has 6 vertices
		// but has not a recognizable L-shape
		bool DummyBuildingPlacer::SetGeometry()
		{
			// gets the number of vertices
			m_csVertexCount = m_currentShape->GetVertexCount();

			// checks vertex count
			if ( (m_csVertexCount != 4) && (m_csVertexCount != 6) )
			{
				return (false);
			}

			// sets the shape of the shape
			if ( m_csVertexCount == 4 )
			{
				m_buildingShape = bsRECTANGLE;
			}
			else
			{
				m_buildingShape = bsLSHAPE;
			}

			// sets the vertices array
			if ( m_vertices.Size() != 0 )
			{
				m_vertices.Clear();
			}

			for ( int i = 0; i < m_csVertexCount; ++i )
			{
				Shapes::DVertex v = m_currentShape->GetVertex( i );
				m_vertices.Add( v );
			}

			// sets the edges array
			if ( m_edges.Size() != 0 )
			{
				m_edges.Clear();
			}

			for ( int i = 0; i < (m_csVertexCount - 1); ++i )
			{
				Edge e( &m_vertices[i], &m_vertices[i + 1] );
				m_edges.Add( e );
			}
			Edge e( &m_vertices[m_csVertexCount - 1], &m_vertices[0] );
			m_edges.Add( e );

			// determines the configuration if L-Shape
			if ( m_buildingShape == bsLSHAPE )
			{
				string tmpConfiguration = "";

				// to determinate if the sequence of corners
				for (int i = 0; i < m_csVertexCount; ++i )
				{
					int i1 = i;
					int i2 = i + 1;
					int i3 = i + 2;
          if ( i2 >= m_csVertexCount ) { i2 -= m_csVertexCount; }
          if ( i3 >= m_csVertexCount ) { i3 -= m_csVertexCount; }

					Shapes::DVector v1( m_vertices[i2].x - m_vertices[i1].x, m_vertices[i2].y - m_vertices[i1].y );
					Shapes::DVector v2( m_vertices[i3].x - m_vertices[i1].x, m_vertices[i3].y - m_vertices[i1].y );

					Shapes::DVector cross = v1.Cross( v2 );

					if ( cross.z > 0 )
					{
						tmpConfiguration += "L";
					}
					else
					{
						tmpConfiguration += "R";
					}
				}

				// assigns configuration
				m_lShapeConfiguration = lscNUM;
				for ( int i = 0; i < lscNUM; ++i )
				{
					if ( tmpConfiguration == m_strLShapeConfiguration[i] )
					{
						m_lShapeConfiguration = static_cast< LShapeConfiguration >( i );
					}
				}

				// checks configuration
				if (m_lShapeConfiguration == lscNUM )
				{
					return (false);
				}
			}
			return (true);
		}

//-----------------------------------------------------------------------------

		void DummyBuildingPlacer::CalculateMeanUVDirections()
		{
			double tempU = 0.0;
			double tempV = 0.0;

			// U direction is determined by the odd edges (starting at 0)
			// V direction is determined by the even edges (starting at 1)
			// in case of rectangle we have two couples of edges, in each couple
			// the two edges point in opposite directions whatever if the order of
			// vertices is CW or CCW
			// in case of L-shape the orientation of the edges depends on the order
			// in which the vertices were inputted during vectorialization, if CW
			// or CCW, and on the choice of the first vertex, so we have to check
			// all possible case

			// odd edges
			int counter = 0;
			double firstDir;
			for ( int i = 0; i < m_csVertexCount; i += 2 )
			{
				double tempDir = m_edges[i].DirectionXY();

				// if L-Shape
				if ( m_buildingShape == bsLSHAPE )
				{
					if ( m_lShapeOddEdgeSigns[m_lShapeConfiguration][counter] == '-' )
					{
						tempDir += EtMathD::PI;
						if ( tempDir > EtMathD::TWO_PI )
						{
							tempDir -= EtMathD::TWO_PI;
						}
					}
				}
				// if rectangle
				else
				{
					if ( counter == 1 )
					{
						tempDir += EtMathD::PI;
						if ( tempDir > EtMathD::TWO_PI )
						{
							tempDir -= EtMathD::TWO_PI;
						}
					}
				}

				// store first direction to check for problems around the 0.0
				// when adding directions
				if ( counter == 0 )
				{
					firstDir = tempDir;
				}
				// checks for problems around the 0.0 (if the product of sins is negative
				// then we have a problem - reasoning in degrees instead of radians:
				// this means that we are tring to add something like 1 + 359, while the
				// correct should be 1 + (-1) if then we want the average direction (in the
				// first case we will obtain 180 which is not correct))
				//
				// this implementation doesn't intercept and correct the case:
				// 1st dir = 0.0
				// 2nd * 3rd < 0.0
				else
				{
					double c1 = EtMathD::Cos( firstDir );
					double s1 = EtMathD::Sin( firstDir );
					double s2 = EtMathD::Sin( tempDir );
					if ( (c1 > 0) && ((s1 * s2) < 0.0) )
					{
						if ( s1 >= 0.0 )
						{
							tempDir -= EtMathD::TWO_PI;
						}
						else
						{
							tempDir += EtMathD::TWO_PI;
						}
					}
				}

				tempU += tempDir;
				counter++;
			}
			m_meanDirectionU = tempU / counter;
			if ( m_meanDirectionU > EtMathD::TWO_PI )
			{
				m_meanDirectionU -= EtMathD::TWO_PI;
			}
			if ( m_meanDirectionU < 0.0 )
			{
				m_meanDirectionU += EtMathD::TWO_PI;
			}

			// even edges
			counter = 0;
			for ( int i = 1; i < m_csVertexCount; i += 2 )
			{
				double tempDir = m_edges[i].DirectionXY();

				// if L-Shape
				if ( m_buildingShape == bsLSHAPE )
				{
					if ( m_lShapeEvenEdgeSigns[m_lShapeConfiguration][counter] == '-' )
					{
						tempDir += EtMathD::PI;
						if ( tempDir > EtMathD::TWO_PI )
						{
							tempDir -= EtMathD::TWO_PI;
						}
					}
				}
				// if rectangle
				else
				{
					if ( counter == 1 )
					{
						tempDir += EtMathD::PI;
						if ( tempDir > EtMathD::TWO_PI )
						{
							tempDir -= EtMathD::TWO_PI;
						}
					}
				}

				// store first direction to check for problems around the 0.0
				// when adding directions
				if ( counter == 0 )
				{
					firstDir = tempDir;
				}
				// checks for problems around the 0.0 (if the product of sins is negative
				// then we have a problem - reasoning in degrees instead of radians:
				// this means that we are tring to add something like 1 + 359, while the
				// correct should be 1 + (-1) if then we want the average direction (in the
				// first case we will obtain 180 which is not correct))
				//
				// this implementation doesn't intercept and correct the case:
				// 1st dir = 0.0
				// 2nd * 3rd < 0.0
				// that can happen with L-Shape
				else
				{
					double c1 = EtMathD::Cos( firstDir );
					double s1 = EtMathD::Sin( firstDir );
					double s2 = EtMathD::Sin( tempDir );
					if ( (c1 > 0) && ((s1 * s2) < 0.0) )
					{
						if ( s1 >= 0.0 )
						{
							tempDir -= EtMathD::TWO_PI;
						}
						else
						{
							tempDir += EtMathD::TWO_PI;
						}
					}
				}

				tempV += tempDir;
				counter++;
			}
			m_meanDirectionV = tempV / counter;
			if ( m_meanDirectionV > EtMathD::TWO_PI )
			{
				m_meanDirectionV -= EtMathD::TWO_PI;
			}
			if ( m_meanDirectionV < 0.0 )
			{
				m_meanDirectionV += EtMathD::TWO_PI;
			}
		}

//-----------------------------------------------------------------------------

		void DummyBuildingPlacer::OrthogonalizeUV()
		{
			// unitary vectors on the UV directions
			Shapes::DVector vectorU( EtMathD::Cos( m_meanDirectionU ), EtMathD::Sin( m_meanDirectionU ) );
			Shapes::DVector vectorV( EtMathD::Cos( m_meanDirectionV ), EtMathD::Sin( m_meanDirectionV ) );

			// dot product between vectors
			double dot = vectorU.DotXY( vectorV );

			// cross product between vectors
			Shapes::DVector cross = vectorU.Cross( vectorV );
			double angle = EtMathD::ACos( dot );

      if ( angle > EtMathD::PI ) { angle = EtMathD::TWO_PI - angle; }
			double correctAngle = EtMathD::Fabs( (EtMathD::PI * 0.5 - angle) * 0.5 );

			// angle smaller than 90 degrees
			if ( dot > EtMathD::ZERO_TOLERANCE )
			{
				if ( cross.z > 0.0 )
				{
					m_meanDirectionU -= correctAngle; 
					m_meanDirectionV += correctAngle;
				}
				else
				{
					m_meanDirectionU += correctAngle; 
					m_meanDirectionV -= correctAngle;
				}
			}

			// angle greater than 90 degrees
			if ( dot < EtMathD::ZERO_TOLERANCE )
			{
				if ( cross.z > 0.0 )
				{
					m_meanDirectionU += correctAngle; 
					m_meanDirectionV -= correctAngle;
				}
				else
				{
					m_meanDirectionU -= correctAngle; 
					m_meanDirectionV += correctAngle;
				}
			}
		}

//-----------------------------------------------------------------------------

		void DummyBuildingPlacer::ReshapeVertices()
		{
			// gets barycenter of shape
			Shapes::DVertex barycenter = m_currentShape->Barycenter();

			// aligns U axis to the X axis
			RotateAboutPoint( barycenter, -m_meanDirectionU );

			// Replace vertices to remove digitalizatin errors
			OrthogonalizeShape();

			// restores previous alignement
			RotateAboutPoint( barycenter, m_meanDirectionU );
		}

//-----------------------------------------------------------------------------

		void DummyBuildingPlacer::RotateAboutPoint( const Shapes::DVertex point, double angle )
		{
			for ( int i = 0; i < m_vertices.Size(); ++i )
			{
				double dx = m_vertices[i].x - point.x;
				double dy = m_vertices[i].y - point.y;

				m_vertices[i].x = point.x + (dx * EtMathD::Cos( angle ) - dy * EtMathD::Sin( angle ));
				m_vertices[i].y = point.y + (dx * EtMathD::Sin( angle ) + dy * EtMathD::Cos( angle ));
			}
		}

//-----------------------------------------------------------------------------

		void DummyBuildingPlacer::OrthogonalizeShape()
		{
			AutoArray< Shapes::DVertex > midPoints;

			// stores edges midPoints
			for ( int i = 0, cnt = m_edges.Size(); i < cnt; ++i )
			{
				midPoints.Add( m_edges[i].MidPoint() );
			}

			// odd edges
			for ( int i = 0, cnt = m_edges.Size(); i < cnt; i += 2 )
			{
				m_edges[i].SetVertex( 1, m_edges[i].GetVertexX( 1 ), midPoints[i].y );
				m_edges[i].SetVertex( 2, m_edges[i].GetVertexX( 2 ), midPoints[i].y );
			}

			// even edges
			for ( int i = 1, cnt = m_edges.Size(); i < cnt; i += 2 )
			{
				m_edges[i].SetVertex( 1, midPoints[i].x, m_edges[i].GetVertexY( 1 ) );
				m_edges[i].SetVertex( 2, midPoints[i].x, m_edges[i].GetVertexY( 2 ) );
			}
		}

//-----------------------------------------------------------------------------

		void DummyBuildingPlacer::CreateObjects()
		{
			// clears array
			m_objectsOut.Clear();

			// calculate building height
			double floorHeight;
			switch ( m_dbfIn.m_buildingType )
			{
      default:
			case btCIVIL:
				floorHeight = m_globalIn.m_heightFloorsCivil;
        break;
			case btINDUSTRIAL:
				floorHeight = m_globalIn.m_heightFloorsIndustrial;
        break;
			case btMILITARY:
				floorHeight = m_globalIn.m_heightFloorsMilitary;
        break;
			}

			int numFloor;
			if ( m_dbfIn.m_numOfFloors == 0 )
			{
				// random num floor
				int max;
				switch ( m_dbfIn.m_buildingType )
				{
        default:
				case btCIVIL:
					max = m_globalIn.m_maxNumFloorsCivil;
          break;
				case btINDUSTRIAL:
					max = m_globalIn.m_maxNumFloorsIndustrial;
          break;
				case btMILITARY:
					max = m_globalIn.m_maxNumFloorsMilitary;
          break;
				}

				numFloor = 1 + static_cast< int >((rand() / RAND_MAX) * max);
			}
			else
			{
				numFloor = m_dbfIn.m_numOfFloors;
			}

			double height = floorHeight * numFloor;

			// if L-Shape
			if ( m_buildingShape == bsLSHAPE )
			{
				// the subdivision in 2 rectangles depends on the vertices
				// order

				// new objects
				ModuleObjectOutput out1;
				ModuleObjectOutput out2;

				int edge_1_1_1, edge_1_1_2, edge_1_2_1, edge_1_2_2;
				int edge_2_1_1, edge_2_1_2, edge_2_2_1, edge_2_2_2;
				int diag_1_1, diag_1_2;
				int diag_2_1, diag_2_2;

				switch ( m_lShapeConfiguration )
				{
				case lscRLLLLL:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 0;
						edge_1_1_2 = 1;
						// edge 2 rectangle 1
						edge_1_2_1 = 5;
						edge_1_2_2 = 0;
						// diagonal rectangle 1
						diag_1_1 = 5;
						diag_1_2 = 1;
						// edge 1 rectangle 2
						edge_2_1_1 = 2;
						edge_2_1_2 = 3;
						// edge 2 rectangle 2
						edge_2_2_1 = 4;
						edge_2_2_2 = 3;
						// diagonal rectangle 2
						diag_2_1 = 2;
						diag_2_2 = 4;
					}
					break;
				case lscLRLLLL:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 0;
						edge_1_1_2 = 1;
						// edge 2 rectangle 1
						edge_1_2_1 = 1;
						edge_1_2_2 = 2;
						// diagonal rectangle 1
						diag_1_1 = 0;
						diag_1_2 = 2;
						// edge 1 rectangle 2
						edge_2_1_1 = 5;
						edge_2_1_2 = 4;
						// edge 2 rectangle 2
						edge_2_2_1 = 3;
						edge_2_2_2 = 4;
						// diagonal rectangle 2
						diag_2_1 = 3;
						diag_2_2 = 5;
					}
					break;
				case lscLLRLLL:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 3;
						edge_1_1_2 = 2;
						// edge 2 rectangle 1
						edge_1_2_1 = 1;
						edge_1_2_2 = 2;
						// diagonal rectangle 1
						diag_1_1 = 1;
						diag_1_2 = 3;
						// edge 1 rectangle 2
						edge_2_1_1 = 5;
						edge_2_1_2 = 4;
						// edge 2 rectangle 2
						edge_2_2_1 = 0;
						edge_2_2_2 = 5;
						// diagonal rectangle 2
						diag_2_1 = 0;
						diag_2_2 = 4;
					}
					break;
				case lscLLLRLL:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 3;
						edge_1_1_2 = 2;
						// edge 2 rectangle 1
						edge_1_2_1 = 4;
						edge_1_2_2 = 3;
						// diagonal rectangle 1
						diag_1_1 = 2;
						diag_1_2 = 4;
						// edge 1 rectangle 2
						edge_2_1_1 = 0;
						edge_2_1_2 = 1;
						// edge 2 rectangle 2
						edge_2_2_1 = 0;
						edge_2_2_2 = 5;
						// diagonal rectangle 2
						diag_2_1 = 1;
						diag_2_2 = 5;
					}
					break;
				case lscLLLLRL:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 4;
						edge_1_1_2 = 5;
						// edge 2 rectangle 1
						edge_1_2_1 = 4;
						edge_1_2_2 = 3;
						// diagonal rectangle 1
						diag_1_1 = 3;
						diag_1_2 = 5;
						// edge 1 rectangle 2
						edge_2_1_1 = 0;
						edge_2_1_2 = 1;
						// edge 2 rectangle 2
						edge_2_2_1 = 1;
						edge_2_2_2 = 2;
						// diagonal rectangle 2
						diag_2_1 = 0;
						diag_2_2 = 2;
					}
					break;
				case lscLLLLLR:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 4;
						edge_1_1_2 = 5;
						// edge 2 rectangle 1
						edge_1_2_1 = 5;
						edge_1_2_2 = 0;
						// diagonal rectangle 1
						diag_1_1 = 4;
						diag_1_2 = 0;
						// edge 1 rectangle 2
						edge_2_1_1 = 3;
						edge_2_1_2 = 2;
						// edge 2 rectangle 2
						edge_2_2_1 = 1;
						edge_2_2_2 = 2;
						// diagonal rectangle 2
						diag_2_1 = 1;
						diag_2_2 = 3;
					}
					break;
				case lscLRRRRR:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 2;
						edge_1_1_2 = 3;
						// edge 2 rectangle 1
						edge_1_2_1 = 1;
						edge_1_2_2 = 2;
						// diagonal rectangle 1
						diag_1_1 = 1;
						diag_1_2 = 3;
						// edge 1 rectangle 2
						edge_2_1_1 = 5;
						edge_2_1_2 = 4;
						// edge 2 rectangle 2
						edge_2_2_1 = 5;
						edge_2_2_2 = 0;
						// diagonal rectangle 2
						diag_2_1 = 0;
						diag_2_2 = 4;
					}
					break;
				case lscRLRRRR:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 2;
						edge_1_1_2 = 3;
						// edge 2 rectangle 1
						edge_1_2_1 = 3;
						edge_1_2_2 = 4;
						// diagonal rectangle 1
						diag_1_1 = 2;
						diag_1_2 = 4;
						// edge 1 rectangle 2
						edge_2_1_1 = 0;
						edge_2_1_2 = 1;
						// edge 2 rectangle 2
						edge_2_2_1 = 0;
						edge_2_2_2 = 5;
						// diagonal rectangle 2
						diag_2_1 = 1;
						diag_2_2 = 5;
					}
					break;
				case lscRRLRRR:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 5;
						edge_1_1_2 = 4;
						// edge 2 rectangle 1
						edge_1_2_1 = 3;
						edge_1_2_2 = 4;
						// diagonal rectangle 1
						diag_1_1 = 3;
						diag_1_2 = 5;
						// edge 1 rectangle 2
						edge_2_1_1 = 0;
						edge_2_1_2 = 1;
						// edge 2 rectangle 2
						edge_2_2_1 = 1;
						edge_2_2_2 = 2;
						// diagonal rectangle 2
						diag_2_1 = 0;
						diag_2_2 = 2;
					}
					break;
				case lscRRRLRR:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 5;
						edge_1_1_2 = 4;
						// edge 2 rectangle 1
						edge_1_2_1 = 0;
						edge_1_2_2 = 5;
						// diagonal rectangle 1
						diag_1_1 = 0;
						diag_1_2 = 4;
						// edge 1 rectangle 2
						edge_2_1_1 = 3;
						edge_2_1_2 = 2;
						// edge 2 rectangle 2
						edge_2_2_1 = 1;
						edge_2_2_2 = 2;
						// diagonal rectangle 2
						diag_2_1 = 1;
						diag_2_2 = 3;
					}
					break;
				case lscRRRRLR:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 0;
						edge_1_1_2 = 1;
						// edge 2 rectangle 1
						edge_1_2_1 = 0;
						edge_1_2_2 = 5;
						// diagonal rectangle 1
						diag_1_1 = 1;
						diag_1_2 = 5;
						// edge 1 rectangle 2
						edge_2_1_1 = 3;
						edge_2_1_2 = 2;
						// edge 2 rectangle 2
						edge_2_2_1 = 4;
						edge_2_2_2 = 3;
						// diagonal rectangle 2
						diag_2_1 = 2;
						diag_2_2 = 4;
					}
					break;
				case lscRRRRRL:
					{
						// edge 1 rectangle 1
						edge_1_1_1 = 0;
						edge_1_1_2 = 1;
						// edge 2 rectangle 1
						edge_1_2_1 = 1;
						edge_1_2_2 = 2;
						// diagonal rectangle 1
						diag_1_1 = 0;
						diag_1_2 = 2;
						// edge 1 rectangle 2
						edge_2_1_1 = 4;
						edge_2_1_2 = 5;
						// edge 2 rectangle 2
						edge_2_2_1 = 4;
						edge_2_2_2 = 3;
						// diagonal rectangle 2
						diag_2_1 = 3;
						diag_2_2 = 5;
					}
					break;
				}
				Edge edge11( &m_vertices[edge_1_1_1], &m_vertices[edge_1_1_2] );
				Edge edge12( &m_vertices[edge_1_2_1], &m_vertices[edge_1_2_2] );
				Edge edge21( &m_vertices[edge_2_1_1], &m_vertices[edge_2_1_2] );
				Edge edge22( &m_vertices[edge_2_2_1], &m_vertices[edge_2_2_2] );
				Edge diag1( &m_vertices[diag_1_1], &m_vertices[diag_1_2] );
				Edge diag2( &m_vertices[diag_2_1], &m_vertices[diag_2_2] );

				out1.position = diag1.MidPoint();
				out1.rotation = m_meanDirectionU;
				out1.scaleX = edge11.Length();
				out1.scaleY = edge12.Length();
				out1.scaleZ = height;

				out2.position = diag2.MidPoint();
				out2.rotation = m_meanDirectionU;
				out2.scaleX = edge21.Length();
				out2.scaleY = edge22.Length();
				out2.scaleZ = height;

				// used only by VLB for preview
				out1.length = 1.0;
				out1.width  = 1.0;
				out1.height = 1.0;
				out2.length = 1.0;
				out2.width  = 1.0;
				out2.height = 1.0;

				// adds to array
				m_objectsOut.Add( out1 );
				m_objectsOut.Add( out2 );

			}
			// if rectangle
			else
			{
				// new object
				ModuleObjectOutput out;

				out.position = m_currentShape->Barycenter();
				out.rotation = m_meanDirectionU;
				out.scaleX = m_vertices[0].DistanceXY( m_vertices[1] );
				out.scaleY = m_vertices[1].DistanceXY( m_vertices[2] );
				out.scaleZ = height;

				// used only by VLB for preview
				out.length = 1.0;
				out.width  = 1.0;
				out.height = 1.0;

				// adds to array
				m_objectsOut.Add( out );
			}
		}

//-----------------------------------------------------------------------------

		void DummyBuildingPlacer::ExportCreatedObjects(IMainCommands* cmdIfc)
		{
			for ( int i = 0; i < m_objectsOut.Size(); i++ )
			{
				RString name;
				switch ( m_dbfIn.m_buildingType )
				{
        default:
				case btCIVIL:
					name = m_globalIn.m_modelCivil;
          break;
				case btINDUSTRIAL:
					name = m_globalIn.m_modelIndustrial;
          break;
				case btMILITARY:
					name = m_globalIn.m_modelMilitary;
          break;
				}

				Vector3D position = Vector3D( m_objectsOut[i].position.x, 
                                      0.0, 
                                      m_objectsOut[i].position.y );

				Matrix4D mTranslation = Matrix4D( MTranslation, position );
				Matrix4D mRotation    = Matrix4D( MRotationY, m_objectsOut[i].rotation );
				Matrix4D mScaling     = Matrix4D( MScale, m_objectsOut[i].scaleX, 
                                                  m_objectsOut[i].scaleZ, 
                                                  m_objectsOut[i].scaleY );

				Matrix4D transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject( transform, name, 0 );
			}
		}

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt