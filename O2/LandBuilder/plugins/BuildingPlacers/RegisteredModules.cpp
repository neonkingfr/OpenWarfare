#include "StdAfx.h"
#include "ModuleRegistrator.h"

namespace LandBuilder2
{
	using namespace LandBuildInt;
}

//source contains list of registered modules. 
/**Don't add new modules here. Instead, register module using the registrator 
at place of module's definition
*/
#include "DummyBuildingPlacer.h"
#include "NYDummyBuildingPlacer.h"
#include "BuildingCreator.h"

namespace LandBuilder2
{
	RegisterModule<Modules::DummyBuildingPlacer>   _register_BP_001("DummyBuildingPlacer");
	RegisterModule<Modules::NYDummyBuildingPlacer> _register_BP_002("NYDummyBuildingPlacer");
	RegisterModule<Modules::BuildingCreator>       _register_BP_003("BuildingCreator");
}