//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\Shapes\ShapeHelpers.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		class NYDummyBuildingPlacer : public IBuildModule
		{
		  class Road
		  {
		  public:
			  double m_coord;
			  double m_width;

			  Road() 
			  {
			  }

			  Road( double coord, double width ) 
			  : m_coord( coord )
			  , m_width( width ) 
			  {
			  }

			  ClassIsMovable( Road );
		  };

		  class Block : public Shapes::DBox
		  {
		  public:
			  Block() 
			  : Shapes::DBox() 
			  {
			  }

			  Block( const Shapes::DVertex& lo, const Shapes::DVertex& hi ) 
			  : Shapes::DBox( lo, hi ) 
			  {
			  }

			  ClassIsMovable( Block );
		  };

		public:
			struct ShapeInput
			{
				size_t  m_randomSeed;
				double  m_buildingMinHeight;
				double  m_buildingMaxHeight;
				RString m_modelNameBase;
				int     m_modelVariants;
				double  m_mainDirection;
				double  m_roadMinWidth;
				double  m_roadMaxWidth;
				double  m_blockMinSize;
				double  m_blockMaxSize;
				int     m_blockMinSubdivisions;
				int     m_blockMaxSubdivisions;
			};

		public:
			NYDummyBuildingPlacer();

			virtual ~NYDummyBuildingPlacer()
      {
      }

			virtual void Run( IMainCommands* cmdIfc );
			ModuleObjectOutputArray* RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn );

      DECLAREMODULEEXCEPTION( "NYDummyBuildingPlacer" )

		private:
      string m_version;
      size_t m_shapeCounter;

			const Shapes::IShape* m_currentShape; 
			ShapeInput            m_shapeIn;

			Shapes::DBox m_orientedBBox;

			AutoArray< Road >  m_hRoads;
			AutoArray< Road >  m_vRoads;
			AutoArray< Block > m_blocks;

			ModuleObjectOutputArray m_objectsOut;
			ModuleObjectOutputArray m_tempObjectsOut;

			bool GetShapeParameters( IMainCommands* cmdIfc );
			void CalculateOrientedBBox();
			void CalculateRoadNetwork();
			void CalculateBlocks();
			void CreateObjects();
			void RotateObjectsAboutShapeBarycenter();
			void DeleteOutsideObjects();
			void ExportCreatedObjects( IMainCommands* cmdIfc );
			void WriteReport();
		};

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt