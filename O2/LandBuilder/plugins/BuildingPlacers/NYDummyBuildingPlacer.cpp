//-----------------------------------------------------------------------------

#include "StdAfx.h"

#include "..\..\HighMapLoaders\include\EtMath.h"
#include "..\..\HighMapLoaders\include\EtHelperFunctions.h"

#include ".\nydummybuildingplacer.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		NYDummyBuildingPlacer::NYDummyBuildingPlacer()
    : m_version ( "1.1.1" )
    , m_shapeCounter( 0 )
		{
		}

//-----------------------------------------------------------------------------

		void NYDummyBuildingPlacer::Run( IMainCommands* cmdIfc )
		{
      RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
      if ( taskName != m_currTaskName )
      {
        m_shapeCounter = 0;
        m_currTaskName = taskName;
      }

      ++m_shapeCounter;

			// gets shape and its properties
			m_currentShape = cmdIfc->GetActiveShape(); 

      if ( !m_currentShape ) { return; }

      LOGF( Info ) << "                                                           ";
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "NYDummyBuildingPlacer " << m_version << " - Started shape n. " << m_shapeCounter;
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "Parameters:";

			// gets shape parameters from calling cfg file
			if ( GetShapeParameters( cmdIfc ) )
			{
				// initializes random seed
				srand( m_shapeIn.m_randomSeed );

				CalculateOrientedBBox();

				CalculateRoadNetwork();

				CalculateBlocks();

				// creates objects to export
				CreateObjects();

				RotateObjectsAboutShapeBarycenter();

				DeleteOutsideObjects();

				// export the objects
				ExportCreatedObjects( cmdIfc );

				// writes the report
				WriteReport();
			}
		}

//-----------------------------------------------------------------------------

		ModuleObjectOutputArray* NYDummyBuildingPlacer::RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn )
		{
			m_currentShape = const_cast< Shapes::IShape* >( shape );
			m_shapeIn      = shapeIn;

      if ( !m_currentShape ) { return (NULL); }

      if ( m_tempObjectsOut.Size() != 0 ) { m_tempObjectsOut.Clear(); }
      if ( m_objectsOut.Size() != 0 )     { m_objectsOut.Clear(); }

			// initializes random seed
			srand( m_shapeIn.m_randomSeed );

			CalculateOrientedBBox();

			CalculateRoadNetwork();

			CalculateBlocks();

			// creates objects to export
			CreateObjects();

			RotateObjectsAboutShapeBarycenter();

			DeleteOutsideObjects();

			return (&m_objectsOut);
		}

//-----------------------------------------------------------------------------

		bool NYDummyBuildingPlacer::GetShapeParameters( IMainCommands* cmdIfc )
		{
      // randomSeed (isMovable)
			const char* x = cmdIfc->QueryValue( "randomSeed", true );
			if ( !x ) { x = cmdIfc->QueryValue( "randomSeed", false ); }

			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_randomSeed = static_cast< size_t >( atoi( x ) );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 11, ">>> Bad data for randomSeed <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 12, ">>> Missing randomSeed value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "randomSeed: " << m_shapeIn.m_randomSeed;

      // buildingMinHeight (isMovable)
      x = cmdIfc->QueryValue( "buildingMinHeight", true );
			if ( !x ) { x = cmdIfc->QueryValue( "buildingMinHeight", false ); }

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_buildingMinHeight = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 13, ">>> Bad data for buildingMinHeight <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 14, ">>> Missing buildingMinHeight value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "buildingMinHeight: " << m_shapeIn.m_buildingMinHeight;

      // buildingMaxHeight (isMovable)
      x = cmdIfc->QueryValue( "buildingMaxHeight", true );
			if ( !x ) { x = cmdIfc->QueryValue( "buildingMaxHeight", false ); }

      if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_buildingMaxHeight = atof(x);
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 15, ">>> Bad data for buildingMaxHeight <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 16, ">>> Missing buildingMaxSize value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "buildingMaxHeight: " << m_shapeIn.m_buildingMaxHeight;

      // modelNameBase (isMovable)
      x = cmdIfc->QueryValue( "modelNameBase", true );
			if ( !x ) { x = cmdIfc->QueryValue( "modelNameBase", false ); }

      if ( x ) 
			{
				m_shapeIn.m_modelNameBase = x;
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 17, ">>> Missing modelNameBase value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "modelNameBase: " << m_shapeIn.m_modelNameBase;

      // modelVariants (isMovable)
      x = cmdIfc->QueryValue( "modelVariants", true );
			if ( !x ) { x = cmdIfc->QueryValue( "modelVariants", false ); }

			if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_modelVariants = atoi( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 18, ">>> Bad data for modelVariants <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception(19, ">>> Missing modelVariants value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "modelVariants: " << m_shapeIn.m_modelVariants;

      // mainDirection (isMovable)
      x = cmdIfc->QueryValue( "mainDirection", true );
			if ( !x ) { x = cmdIfc->QueryValue( "mainDirection", false ); }

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_mainDirection = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 21, ">>> Bad data for mainDirection <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 22, ">>> Missing mainDirection value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "mainDirection: " << m_shapeIn.m_mainDirection;

      // roadMinWidth (isMovable)
      x = cmdIfc->QueryValue( "roadMinWidth", true );
			if ( !x ) { x = cmdIfc->QueryValue( "roadMinWidth", false ); }

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_roadMinWidth = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 23, ">>> Bad data for roadMinWidth <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 24, ">>> Missing roadMinWidth value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "roadMinWidth: " << m_shapeIn.m_roadMinWidth;

      // roadMaxWidth (isMovable)
      x = cmdIfc->QueryValue( "roadMaxWidth", true );
			if ( !x ) { x = cmdIfc->QueryValue( "roadMaxWidth", false ); }

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_roadMaxWidth = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 25, ">>> Bad data for roadMaxWidth <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 26, ">>> Missing roadMaxWidth value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "roadMaxWidth: " << m_shapeIn.m_roadMaxWidth;

      // blockMinSize (isMovable)
      x = cmdIfc->QueryValue( "blockMinSize", true );
			if ( !x ) { x = cmdIfc->QueryValue( "blockMinSize", false ); }

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_blockMinSize = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 27, ">>> Bad data for blockMinSize <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 28, ">>> Missing blockMinSize value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "blockMinSize: " << m_shapeIn.m_blockMinSize;

      // blockMaxSize (isMovable)
      x = cmdIfc->QueryValue( "blockMaxSize", true );
			if ( !x ) { x = cmdIfc->QueryValue( "blockMaxSize", false ); }

      if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_blockMaxSize = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 29, ">>> Bad data for blockMaxSize <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 30, ">>> Missing blockMaxSize value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "blockMaxSize: " << m_shapeIn.m_blockMaxSize;

      // blockMinSubdivisions (isMovable)
      x = cmdIfc->QueryValue( "blockMinSubdivisions", true );
			if ( !x ) { x = cmdIfc->QueryValue( "blockMinSubdivisions", false ); }

      if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_blockMinSubdivisions = atoi( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 31, ">>> Bad data for blockMinSubdivisions <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 32, ">>> Missing blockMinSubdivisions value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "blockMinSubdivisions: " << m_shapeIn.m_blockMinSubdivisions;

      // blockMaxSubdivisions (isMovable)
      x = cmdIfc->QueryValue( "blockMaxSubdivisions", true );
			if ( !x ) { x = cmdIfc->QueryValue( "blockMaxSubdivisions", false ); }

      if ( x ) 
			{
				if ( IsInteger( string( x ), false ) )
				{
					m_shapeIn.m_blockMaxSubdivisions = atoi( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 33, ">>> Bad data for blockMaxSubdivisions <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 34, ">>> Missing blockMaxSubdivisions value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "blockMaxSubdivisions: " << m_shapeIn.m_blockMaxSubdivisions;

			return (true);
		}

//-----------------------------------------------------------------------------

		void NYDummyBuildingPlacer::CalculateOrientedBBox()
		{
			m_orientedBBox = m_currentShape->CalcBoundingBox();
		}

//-----------------------------------------------------------------------------

		void NYDummyBuildingPlacer::CalculateRoadNetwork()
		{
			m_vRoads.Clear();
			m_hRoads.Clear();

			double minX = m_orientedBBox.lo.x;
			double minY = m_orientedBBox.lo.y;
			double maxX = m_orientedBBox.hi.x;
			double maxY = m_orientedBBox.hi.y;

			// vertical roads
			double currentX = minX - (maxX - minX) * 0.5;
			double width = RandomInRangeF( m_shapeIn.m_roadMinWidth, m_shapeIn.m_roadMaxWidth );
			m_vRoads.Add( Road( currentX, width ) );
	
			int counter = 0;
			while ( currentX < maxX + (maxX - minX) * 0.5 )
			{
				double width = RandomInRangeF( m_shapeIn.m_roadMinWidth, m_shapeIn.m_roadMaxWidth );
				double block = RandomInRangeF( m_shapeIn.m_blockMinSize, m_shapeIn.m_blockMaxSize );

				currentX += m_vRoads[counter].m_width * 0.5 + block + width * 0.5;

				m_vRoads.Add( Road( currentX, width ) );
				counter++;
			}

			// horizontal roads
			double currentY = minY - (maxY - minY) * 0.5;
			width = RandomInRangeF( m_shapeIn.m_roadMinWidth, m_shapeIn.m_roadMaxWidth );
			m_hRoads.Add( Road( currentY, width ) );

			counter = 0;
			while ( currentY < maxY + (maxY - minY) * 0.5 )
			{
				double width = RandomInRangeF( m_shapeIn.m_roadMinWidth, m_shapeIn.m_roadMaxWidth );
				double block = RandomInRangeF( m_shapeIn.m_blockMinSize, m_shapeIn.m_blockMaxSize );

				currentY += m_hRoads[counter].m_width * 0.5 + block + width * 0.5;

				m_hRoads.Add( Road( currentY, width ) );
				counter++;
			}
		}

//-----------------------------------------------------------------------------

		void NYDummyBuildingPlacer::CalculateBlocks()
		{
			m_blocks.Clear();

			int hRoadsCount = m_hRoads.Size();
			int vRoadsCount = m_vRoads.Size();

			for ( int i = 0; i < hRoadsCount - 1; ++i )
			{
				for ( int j = 0; j < vRoadsCount - 1; ++j )
				{
					double minX = m_vRoads[j].m_coord + m_vRoads[j].m_width * 0.5;
					double maxX = m_vRoads[j + 1].m_coord - m_vRoads[j + 1].m_width * 0.5;
					double minY = m_hRoads[i].m_coord + m_hRoads[i].m_width * 0.5;
					double maxY = m_hRoads[i + 1].m_coord - m_hRoads[i + 1].m_width * 0.5;
					Shapes::DVertex vLo( minX, minY );
					Shapes::DVertex vHi( maxX, maxY );

					Block block( vLo, vHi );

					m_blocks.Add( block );
				}
			}
		}

//-----------------------------------------------------------------------------

		void NYDummyBuildingPlacer::CreateObjects()
		{
      if ( m_tempObjectsOut.Size() != 0 ) { m_tempObjectsOut.Clear(); }
      if ( m_objectsOut.Size() != 0 )     { m_objectsOut.Clear(); }

			int blocksCount = m_blocks.Size();
			for ( int i = 0; i < blocksCount; ++i )
			{
				Block block = m_blocks[i];
				double blockWSize = block.hi.x - block.lo.x;
				double blockHSize = block.hi.y - block.lo.y;

				int cellsW = (int)floor( RandomInRangeF( m_shapeIn.m_blockMinSubdivisions, m_shapeIn.m_blockMaxSubdivisions + 1 ) );
				int cellsH = (int)floor( RandomInRangeF( m_shapeIn.m_blockMinSubdivisions, m_shapeIn.m_blockMaxSubdivisions + 1 ) );

				double cellWSize = blockWSize / cellsW;
				double cellHSize = blockHSize / cellsH;
					
				for ( int j = 0; j < cellsW; ++j )
				{
					for ( int k = 0; k < cellsH; ++k )
					{
						if ( j == 0 || j == cellsW - 1 || k == 0 || k == cellsH - 1 )
						{
							double midW = block.lo.x + (j + 0.5) * cellWSize;
							double midH = block.lo.y + (k + 0.5) * cellHSize;

							// new object
							ModuleObjectOutput out;

							Shapes::DVertex v( midW, midH );

							out.position = v;
							out.rotation = 0.0;
							out.scaleX = cellWSize;
							out.scaleY = cellHSize;
							out.scaleZ = RandomInRangeF( m_shapeIn.m_buildingMinHeight, m_shapeIn.m_buildingMaxHeight );

							// used only by VLB for preview
							out.length = 1.0;
							out.width  = 1.0;
							out.height = 1.0;

							// adds to array
							m_tempObjectsOut.Add( out );
						}
					}
				}
			}
		}

//-----------------------------------------------------------------------------

		void NYDummyBuildingPlacer::RotateObjectsAboutShapeBarycenter()
		{
			Shapes::DVertex barycenter = m_currentShape->Barycenter();
			double angle = m_shapeIn.m_mainDirection / 180.0 * EtMathD::PI;

			int objectsCount = m_tempObjectsOut.Size();
			for ( int i = 0; i < objectsCount; ++i )
			{
				double dx = m_tempObjectsOut[i].position.x - barycenter.x;
				double dy = m_tempObjectsOut[i].position.y - barycenter.y;

				m_tempObjectsOut[i].position.x = barycenter.x + (dx * EtMathD::Cos( angle ) - dy * EtMathD::Sin( angle ));
				m_tempObjectsOut[i].position.y = barycenter.y + (dx * EtMathD::Sin( angle ) + dy * EtMathD::Cos( angle ));
				m_tempObjectsOut[i].rotation = angle;
			}
		}

//-----------------------------------------------------------------------------

		void NYDummyBuildingPlacer::DeleteOutsideObjects()
		{
			int objectsCount = m_tempObjectsOut.Size();
			int counter = 0;
			while ( counter < objectsCount )
			{
				ModuleObjectOutput out = m_tempObjectsOut[counter];
				double angle = out.rotation;
				double x0    = out.position.x;
				double y0    = out.position.y;

				double dimX = out.scaleX;
				double dimY = out.scaleY;

				Shapes::DVertex v[4];

				v[0].x = x0 + (-dimX * 0.5) * EtMathD::Cos( angle ) - (dimY * 0.5)  * EtMathD::Sin( angle );
				v[0].y = y0 + (-dimX * 0.5) * EtMathD::Sin( angle ) + (dimY * 0.5)  * EtMathD::Cos( angle );
				v[1].x = x0 + (-dimX * 0.5) * EtMathD::Cos( angle ) - (-dimY * 0.5) * EtMathD::Sin( angle );
				v[1].y = y0 + (-dimX * 0.5) * EtMathD::Sin( angle ) + (-dimY * 0.5) * EtMathD::Cos( angle );
				v[2].x = x0 + (dimX * 0.5)  * EtMathD::Cos( angle ) - (-dimY * 0.5) * EtMathD::Sin( angle );
				v[2].y = y0 + (dimX * 0.5)  * EtMathD::Sin( angle ) + (-dimY * 0.5) * EtMathD::Cos( angle );
				v[3].x = x0 + (dimX * 0.5)  * EtMathD::Cos( angle ) - (dimY * 0.5)  * EtMathD::Sin( angle );
				v[3].y = y0 + (dimX * 0.5)  * EtMathD::Sin( angle ) + (dimY * 0.5)  * EtMathD::Cos( angle );

				bool inside = true;
				for ( int i = 0; i < 4; ++i )
				{
					if ( !m_currentShape->PtInside( v[i] ) )
					{
						inside = false;
						break;
					}
				}

				if ( inside ) { m_objectsOut.Add( out ); }

				counter++;
			}
		}

//-----------------------------------------------------------------------------

		void NYDummyBuildingPlacer::ExportCreatedObjects( IMainCommands* cmdIfc )
		{
			for ( int i = 0; i < m_objectsOut.Size(); ++i )
			{
				char cNumber[50];

				int n = m_shapeIn.m_modelVariants + 1;
				while ( n > m_shapeIn.m_modelVariants )
				{
					n = (int)floor( RandomInRangeF( 1.0, m_shapeIn.m_modelVariants + 1 ) );
				}

				sprintf_s( cNumber, "%d", n );
				RString name = m_shapeIn.m_modelNameBase + cNumber;

				Vector3D position = Vector3D( m_objectsOut[i].position.x, 
                                      0.0, 
                                      m_objectsOut[i].position.y );

				Matrix4D mTranslation = Matrix4D( MTranslation, position );
				Matrix4D mRotation    = Matrix4D( MRotationY, m_objectsOut[i].rotation );
				Matrix4D mScaling     = Matrix4D( MScale, m_objectsOut[i].scaleX, 
                                                  m_objectsOut[i].scaleZ, 
                                                  m_objectsOut[i].scaleY );

				Matrix4D transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject( transform, name, 0 );
			}
		}

//-----------------------------------------------------------------------------

		void NYDummyBuildingPlacer::WriteReport()
		{
      LOGF( Info ) << "-----------------------------------------------------------";
			int objOutCount = m_objectsOut.Size();
      LOGF( Info ) << "Created " << objOutCount << " new objects";
      LOGF( Info ) << "-----------------------------------------------------------";
		}

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt
