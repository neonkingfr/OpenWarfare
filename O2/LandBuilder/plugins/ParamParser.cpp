//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "ParamParser.h"

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

  ParamParser::ParamParser( IMainCommands* data )
  : data( data )
  {
  }

//-----------------------------------------------------------------------------

  const char* ParamParser::GetTextValue( const char* name ) throw (ParamParserException)
  {
    const char* text = data->QueryValue( name );
    if ( text == 0 ) { throw ParamParserMissingValue( name, "" ); }
    return (text);
  }

//-----------------------------------------------------------------------------

  int ParamParser::GetIntValue( const char* name ) throw (ParamParserException)
  {
    const char* text = data->QueryValue( name );
    if ( text == 0 ) { throw ParamParserMissingValue( name, "" ); }
    int res;
    if ( sscanf( text, "%d", &res ) != 1 ) { throw ParamParserIntExcept( name, text ); }
    return (res);
  }

//-----------------------------------------------------------------------------

  float ParamParser::GetFloatValue( const char* name ) throw (ParamParserException)
  {
    const char* text = data->QueryValue( name );
    if ( text == 0 ) { throw ParamParserMissingValue( name, "" ); }
    float res;
    if ( sscanf( text, "%f", &res ) != 1 ) { throw ParamParserFloatExcept( name, text ); }
    return (res);
  }

//-----------------------------------------------------------------------------

  RString ParamParser::ListGetTextValue( const char* name, int index, char sep ) throw (ParamParserException)
  {
    const char* text = data->QueryValue( name );
    if ( text == 0 ) { throw ParamParserMissingValue( name, "" ); }

    const char* c = name;
    for ( int i = 0; i < index; ++i )
    {
      c = strchr( c, sep );
      if ( c == 0 ) { throw ParamParserOutOfRange( name, text, index ); }
      ++c;
    }
    
    const char* d = strchr( c, sep );
    if ( d == 0 ) { d = strchr( c, 0 ); }
    return (RString( c, d - c ));
  }

//-----------------------------------------------------------------------------

  int ParamParser::ListGetIntValue( const char* name, int index, char sep ) throw (ParamParserException) 
  {
    RString text = ListGetTextValue( name, index, sep );
    int res;
    if ( sscanf( text, "%d", &res ) != 1 ) { throw ParamParserIntExcept( name, text ); }
    return (res);
  }

//-----------------------------------------------------------------------------

  float ParamParser::ListGetFloatValue( const char* name, int index, char sep ) throw (ParamParserException) 
  {
    RString text = ListGetTextValue( name, index, sep );
    float res;
    if ( sscanf( text, "%f", &res ) != 1 ) { throw ParamParserFloatExcept( name, text ); }
    return (res);
  }

//-----------------------------------------------------------------------------

  AutoArray< RString > ParamParser::GetTextArr( const char* name, char sep ) throw (ParamParserException)
  {
    const char* text = data->QueryValue( name );
    if ( text == 0 ) { throw ParamParserMissingValue( name, "" ); }
    AutoArray< RString > res;

    const char* e = strchr( text, sep );
    while ( e )
    {
      res.Add( RString( text, e - text ) );
      text = e + 1;
      e = strchr( text, sep );
    }
    res.Add( RString( text ) );
    return (res);
  }

//-----------------------------------------------------------------------------

  AutoArray< int > ParamParser::GetIntArr( const char* name, char sep ) throw (ParamParserException)
  {
    const char* text = data->QueryValue( name );
    if ( text == 0 ) { throw ParamParserMissingValue( name, "" ); }
    AutoArray< int > res;

    const char* e = strchr( text, sep );
    int x;
    while ( e )
    {
      if ( sscanf( text, "%d", &x ) != 1 ) { throw ParamParserIntExcept( name, text ); }
      res.Add( x );
      text = e + 1;
      e = strchr( text, sep );
    }
    
    if ( sscanf( text, "%d", &x ) != 1 ) { throw ParamParserIntExcept( name, text ); }
    res.Add( x );
    return (res); 
  }

//-----------------------------------------------------------------------------

  AutoArray< float > ParamParser::GetFloatArr( const char* name, char sep ) throw (ParamParserException)
  {
    const char* text = data->QueryValue( name );
    if ( text == 0 ) { throw ParamParserMissingValue( name, "" ); }
    AutoArray< float > res;

    const char* e = strchr( text, sep );
    float x;
    while ( e ) 
    {
      if ( sscanf( text, "%f", &x ) != 1 ) { throw ParamParserIntExcept( name, text ); }
      res.Add( x );
      text = e + 1;
      e = strchr( text, sep );
    }
    
    if ( sscanf( text, "%f", &x ) != 1 ) { throw ParamParserIntExcept( name, text ); }
    res.Add( x );
    return (res);
  }

//-----------------------------------------------------------------------------

} // namespace LandBuilder2