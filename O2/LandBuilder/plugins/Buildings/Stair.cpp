//-----------------------------------------------------------------------------
// File: Stair.cpp
//
// Desc: classes related to building stairs
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Stair.h"

//-----------------------------------------------------------------------------

CStair::CStair()
: m_type( NULL )
, m_height( CGConsts::ZERO )
, m_center( CGPoint2() )
, m_orientation( CGConsts::ZERO )
{
}

//-----------------------------------------------------------------------------

CStair::CStair( const CStairType* type, Float height, const CGPoint2& center,
                Float orientation )
: m_type( type )
, m_height( height )
, m_center( center )
, m_orientation( orientation )
{
  assert( m_type );
  assert( m_height > CGConsts::ZERO );
  Init();
}

//-----------------------------------------------------------------------------

CStair::CStair( const CStair& other )
: m_type( other.m_type )
, m_height( other.m_height )
, m_center( other.m_center )
, m_orientation( other.m_orientation )
{
  Init();
}

//-----------------------------------------------------------------------------

const CStairType* CStair::GetType() const
{
  return (m_type);
}

//-----------------------------------------------------------------------------

Float CStair::GetHeight() const
{
  return (m_height);
}

//-----------------------------------------------------------------------------

const CGPoint2& CStair::GetCenter() const
{
  return (m_center);
}

//-----------------------------------------------------------------------------

CGPoint2& CStair::GetCenter()
{
  return (m_center);
}

//-----------------------------------------------------------------------------

Float CStair::GetOrientation() const
{
  return (m_orientation);
}

//-----------------------------------------------------------------------------

void CStair::SetType( const CStairType* type )
{
  assert( type );
  m_type = type;
}

//-----------------------------------------------------------------------------

void CStair::SetHeight( Float height )
{
  assert( height > CGConsts::ZERO );
  m_height = height;
  Init();
}

//-----------------------------------------------------------------------------

void CStair::SetCenter( const CGPoint2& center )
{
  m_center = center;
}

//-----------------------------------------------------------------------------

void CStair::SetOrientation( Float orientation )
{
  m_orientation = orientation;
}

//-----------------------------------------------------------------------------

size_t CStair::RiserCount() const
{
  return (m_riserCount);
}

//-----------------------------------------------------------------------------

Float CStair::StepRiser() const
{
  return (m_stepRiser);
}

//-----------------------------------------------------------------------------

Float CStair::StepTread() const
{
  return (m_stepTread);
}

//-----------------------------------------------------------------------------

CGPolygon2 CStair::Footprint() const
{
  CGPolygon2 footprint;

  Float halfLength = CGConsts::HALF * m_riserCount * m_stepTread;
  Float halfWidth  = CGConsts::HALF * m_type->GetWidth();

  // basic shape
  footprint.AppendVertex( -halfLength, -halfWidth );
  footprint.AppendVertex(  halfLength, -halfWidth );
  footprint.AppendVertex(  halfLength,  halfWidth );
  footprint.AppendVertex( -halfLength,  halfWidth );

  // translate
  footprint.Translate( m_center.GetX(), m_center.GetY() );

  // rotate
  footprint.Rotate( m_center, m_orientation );

  return (footprint);
}

//-----------------------------------------------------------------------------

GeometriesList CStair::GroundFloorGeometries() const
{
  GeometriesList stairGeometry;

  Float rampWidth     = m_type->GetWidth();
  Float halfRampWidth = CGConsts::HALF * rampWidth;
  assert( m_height > CGConsts::ZERO );
  Float texScale = CGConsts::ONE / m_height;
  Float rampLength = m_riserCount * m_stepTread;

  const CTexture* rampTexture = m_type->GetTexture( STAIR_RAMP_TEX );
  const CTexture* wallTexture = m_type->GetTexture( STAIR_WALL_TEX );

  CGPolygon2 stairProfile;
  stairProfile.AppendVertex( CGConsts::ZERO, CGConsts::ZERO );
  stairProfile.AppendVertex( m_stepTread, CGConsts::ZERO );
  stairProfile.AppendVertex( rampLength, (m_riserCount - 1) * m_stepRiser );
  stairProfile.AppendVertex( rampLength, m_riserCount * m_stepRiser );
  for ( int i = m_riserCount - 1; i > 0; --i )
  {
    stairProfile.AppendVertex( i * m_stepTread, (i + 1) * m_stepRiser );
    stairProfile.AppendVertex( i * m_stepTread, i * m_stepRiser );
  }
  stairProfile.AppendVertex( CGConsts::ZERO, m_stepRiser );

  stairProfile.Partition();
  vector< CGPolygon2 >& partitions = stairProfile.GetPartitions();
  for ( size_t i = 0, cntI = partitions.size(); i < cntI; ++i )
  {
    partitions[i].Triangulate();
  }

  // step faces
  for ( size_t i = 0, cntI = partitions.size(); i < cntI; ++i )
  {
    const CGPolygon2& partFootprint = partitions[i];
    CGeometry partGeometry;

    // VERTICES
    // bottom
    for ( size_t j = 0, cntJ = partFootprint.VerticesCount(); j < cntJ; ++j )
    {
      const CGPoint2& p = partFootprint.Vertex( j );
      partGeometry.AddVertex( CGPoint3( p.GetX(), p.GetY(), CGConsts::ZERO ) );
    }
    // top
    for ( size_t j = 0, cntJ = partFootprint.VerticesCount(); j < cntJ; ++j )
    {
      const CGPoint2& p = partFootprint.Vertex( j );
      partGeometry.AddVertex( CGPoint3( p.GetX(), p.GetY(), rampWidth ) );
    }

    // FACES
    // steps
    FacesList stepsFaces = CFacesBuilder::BuildRectClosedStripFaces( partGeometry, CTextureProperties( rampTexture, texScale, CGPoint2(), CGConsts::HALF_PI ) );
    for ( size_t j = 0, cntJ = stepsFaces.Size(); j < cntJ; j += 2 )
    {
      CFace* face1 = stepsFaces.Get( j );
      CFace* face2 = stepsFaces.Get( j + 1 );
      const CGPoint3& p1 = partGeometry.Vertex( face1->GetIndex( 0 ) );
      const CGPoint3& p2 = partGeometry.Vertex( face1->GetIndex( 1 ) );
      CGPoint2 p1plan( p1.GetX(), p1.GetY() );
      CGPoint2 p2plan( p2.GetX(), p2.GetY() );

      int index = stairProfile.FindEdgeIndex( CGSegment2( p1plan, p2plan ) );
      if ( index == -1 )
      {
        // internal faces
        face1->SetTexture( NULL );
        face2->SetTexture( NULL );
        face1->SetVisible( false );
        face2->SetVisible( false );
        face1->SetShadow( false );
        face2->SetShadow( false );
      }
      else if ( index == 0 || index == 2 )
      {
        // contact faces
        face1->SetTexture( NULL );
        face2->SetTexture( NULL );
        face1->SetVisible( false );
        face2->SetVisible( false );
      }
      else 
      {
        if ( index == 1 )
        {
          face1->SetTexture( wallTexture );
          face2->SetTexture( wallTexture );
        }
        if ( abs( p1plan.Distance( p2plan ) - m_stepTread ) < CGConsts::ZERO_TOLERANCE )
        {
          face1->SetRoadWay( true );
          face2->SetRoadWay( true );
        }
      }

      partGeometry.AddFace( *face1 );
      partGeometry.AddFace( *face2 );
    }

    // left side
    FacesList leftFaces = CFacesBuilder::BuildFacesFromHorizontalTriangulation( partGeometry,
                                                                                partFootprint.GetTriangulation().GetTrianglesList(), 
                                                                                CGConsts::ZERO,
                                                                                CGConsts::ZERO,
                                                                                m_riserCount * m_stepRiser,
                                                                                m_height,
                                                                                wallTexture, 
                                                                                true );
    for ( size_t j = 0, cntJ = leftFaces.Size(); j < cntJ; ++j )
    {
      CFace face = *leftFaces.Get( j );
      partGeometry.AddFace( face );
    }

    // right side
    FacesList rightFaces = CFacesBuilder::BuildFacesFromHorizontalTriangulation( partGeometry,
                                                                                 partFootprint.GetTriangulation().GetTrianglesList(),
                                                                                 rampWidth,
                                                                                 CGConsts::ZERO,
                                                                                 m_riserCount * m_stepRiser,
                                                                                 m_height,
                                                                                 wallTexture );
    for ( size_t j = 0, cntJ = rightFaces.Size(); j < cntJ; ++j )
    {
      CFace face = *rightFaces.Get( j );
      partGeometry.AddFace( face );
    }

    partGeometry.Translate( CGConsts::ZERO, CGConsts::ZERO, -halfRampWidth );
    partGeometry.Rotate( CGVector3( CGConsts::ONE, CGConsts::ZERO, CGConsts::ZERO ),
                         CGConsts::HALF_PI, CGPoint3() );

    partGeometry.Translate( -CGConsts::HALF * rampLength, CGConsts::ZERO, CGConsts::ZERO );
    partGeometry.Translate( m_center.GetX(), m_center.GetY(), CGConsts::ZERO );
    partGeometry.Rotate( CGVector3( CGConsts::ZERO, CGConsts::ZERO, CGConsts::ONE ),
                         m_orientation,
                         CGPoint3( m_center.GetX(), m_center.GetY(), CGConsts::ZERO ) );

    stairGeometry.Append( partGeometry );
  }

  return (stairGeometry);
}

//-----------------------------------------------------------------------------

GeometriesList CStair::GenericFloorGeometries() const
{
  GeometriesList stairGeometry;

  Float rampWidth     = m_type->GetWidth();
  Float halfRampWidth = CGConsts::HALF * rampWidth;
  assert( m_height > CGConsts::ZERO );
  Float texScale = CGConsts::ONE / m_height;
  Float rampLength = m_riserCount * m_stepTread;

  const CTexture* rampTexture = m_type->GetTexture( STAIR_RAMP_TEX );
  const CTexture* wallTexture = m_type->GetTexture( STAIR_WALL_TEX );

  CGPolygon2 stairProfile;
  stairProfile.AppendVertex( CGConsts::ZERO, -m_stepRiser );
  stairProfile.AppendVertex( rampLength, (m_riserCount - 1) * m_stepRiser );
  stairProfile.AppendVertex( rampLength, m_riserCount * m_stepRiser );
  for ( int i = m_riserCount - 1; i > 0; --i )
  {
    stairProfile.AppendVertex( i * m_stepTread, (i + 1) * m_stepRiser );
    stairProfile.AppendVertex( i * m_stepTread, i * m_stepRiser );
  }
  stairProfile.AppendVertex( CGConsts::ZERO, m_stepRiser );

  stairProfile.Partition();
  vector< CGPolygon2 >& partitions = stairProfile.GetPartitions();
  for ( size_t i = 0, cntI = partitions.size(); i < cntI; ++i )
  {
    partitions[i].Triangulate();
  }

  // step faces
  for ( size_t i = 0, cntI = partitions.size(); i < cntI; ++i )
  {
    const CGPolygon2& partFootprint = partitions[i];
    CGeometry partGeometry;

    // VERTICES
    // bottom
    for ( size_t j = 0, cntJ = partFootprint.VerticesCount(); j < cntJ; ++j )
    {
      const CGPoint2& p = partFootprint.Vertex( j );
      partGeometry.AddVertex( CGPoint3( p.GetX(), p.GetY(), CGConsts::ZERO ) );
    }
    // top
    for ( size_t j = 0, cntJ = partFootprint.VerticesCount(); j < cntJ; ++j )
    {
      const CGPoint2& p = partFootprint.Vertex( j );
      partGeometry.AddVertex( CGPoint3( p.GetX(), p.GetY(), rampWidth ) );
    }

    // FACES
    // steps
    FacesList stepsFaces = CFacesBuilder::BuildRectClosedStripFaces( partGeometry, CTextureProperties( rampTexture, texScale, CGPoint2(), CGConsts::HALF_PI ) );
    for ( size_t j = 0, cntJ = stepsFaces.Size(); j < cntJ; j += 2 )
    {
      CFace* face1 = stepsFaces.Get( j );
      CFace* face2 = stepsFaces.Get( j + 1 );
      const CGPoint3& p1 = partGeometry.Vertex( face1->GetIndex( 0 ) );
      const CGPoint3& p2 = partGeometry.Vertex( face1->GetIndex( 1 ) );
      CGPoint2 p1plan( p1.GetX(), p1.GetY() );
      CGPoint2 p2plan( p2.GetX(), p2.GetY() );

      int index = stairProfile.FindEdgeIndex( CGSegment2( p1plan, p2plan ) );
      if ( index == -1 )
      {
        // internal faces
        face1->SetTexture( NULL );
        face2->SetTexture( NULL );
        face1->SetVisible( false );
        face2->SetVisible( false );
        face1->SetShadow( false );
        face2->SetShadow( false );
      }
      else if ( index == 1 || index == stairProfile.VerticesCount() )
      {
        // contact faces
        face1->SetTexture( NULL );
        face2->SetTexture( NULL );
        face1->SetVisible( false );
        face2->SetVisible( false );
      }
      else 
      {
        if ( index == 0 )
        {
          face1->SetTexture( wallTexture );
          face2->SetTexture( wallTexture );
        }
        if ( abs( p1plan.Distance( p2plan ) - m_stepTread ) < CGConsts::ZERO_TOLERANCE )
        {
          face1->SetRoadWay( true );
          face2->SetRoadWay( true );
        }
      }

      partGeometry.AddFace( *face1 );
      partGeometry.AddFace( *face2 );
    }

    // left side
    FacesList leftFaces = CFacesBuilder::BuildFacesFromHorizontalTriangulation( partGeometry,
                                                                                partFootprint.GetTriangulation().GetTrianglesList(), 
                                                                                CGConsts::ZERO,
                                                                                CGConsts::ZERO,
                                                                                m_riserCount * m_stepRiser,
                                                                                m_height,
                                                                                wallTexture, 
                                                                                true );
    for ( size_t j = 0, cntJ = leftFaces.Size(); j < cntJ; ++j )
    {
      CFace face = *leftFaces.Get( j );
      partGeometry.AddFace( face );
    }

    // right side
    FacesList rightFaces = CFacesBuilder::BuildFacesFromHorizontalTriangulation( partGeometry,
                                                                                 partFootprint.GetTriangulation().GetTrianglesList(),
                                                                                 rampWidth,
                                                                                 CGConsts::ZERO,
                                                                                 m_riserCount * m_stepRiser,
                                                                                 m_height,
                                                                                 wallTexture );
    for ( size_t j = 0, cntJ = rightFaces.Size(); j < cntJ; ++j )
    {
      CFace face = *rightFaces.Get( j );
      partGeometry.AddFace( face );
    }

    partGeometry.Translate( CGConsts::ZERO, CGConsts::ZERO, -halfRampWidth );
    partGeometry.Rotate( CGVector3( CGConsts::ONE, CGConsts::ZERO, CGConsts::ZERO ),
                         CGConsts::HALF_PI, CGPoint3() );

    partGeometry.Translate( -CGConsts::HALF * rampLength, CGConsts::ZERO, CGConsts::ZERO );
    partGeometry.Translate( m_center.GetX(), m_center.GetY(), CGConsts::ZERO );
    partGeometry.Rotate( CGVector3( CGConsts::ZERO, CGConsts::ZERO, CGConsts::ONE ),
                         m_orientation, 
                         CGPoint3( m_center.GetX(), m_center.GetY(), CGConsts::ZERO ) );

    stairGeometry.Append( partGeometry );
  }

  return (stairGeometry);
}

//-----------------------------------------------------------------------------

void CStair::Init()
{
  m_riserCount = static_cast< size_t >( floor( m_height / DEFAULT_STAIR_STEP_RISER ) ) + 1;
  m_stepRiser  = m_height / m_riserCount;
  m_stepTread  = static_cast< Float >( 0.64 ) - CGConsts::TWO * m_stepRiser;
}

//-----------------------------------------------------------------------------
