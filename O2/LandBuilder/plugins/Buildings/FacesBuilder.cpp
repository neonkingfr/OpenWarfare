//-----------------------------------------------------------------------------
// File: FacesBuilder.cpp
//
// Desc: class for building faces
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "FacesBuilder.h"

//-----------------------------------------------------------------------------

CTextureProperties::CTextureProperties( const CTexture* texture, Float scale, 
                                        const CGPoint2& offset, Float rotation )
: m_texture( texture )
, m_scale( scale )
, m_offset( offset ) 
, m_rotation( rotation ) 
{
}

//-----------------------------------------------------------------------------

const CTexture* CTextureProperties::GetTexture() const
{
	return (m_texture);
}

//-----------------------------------------------------------------------------

Float CTextureProperties::GetScale() const
{
	return (m_scale);
}

//-----------------------------------------------------------------------------

const CGPoint2& CTextureProperties::GetOffset() const
{
	return (m_offset); 
}

//-----------------------------------------------------------------------------

Float CTextureProperties::GetRotation() const
{
	return (m_rotation); 
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

FacesList CFacesBuilder::BuildRectOpenStripFaces( const CGeometry& geometry,
                                                  const CTextureProperties& texProperties )
{
	FacesList facesList;

	// geometry.VerticesCount() MUST BE an even number
	size_t size = geometry.VerticesCount();
	assert( size % 2 == 0 );

	size_t n = size / 2;

	FacesList quadFaces;
	for ( size_t i = 0, cntI = n - 1; i < cntI; ++i )
	{
		quadFaces = BuildQuadFaces( geometry, i, i + 1, i + n + 1, i + n, texProperties );
		for ( size_t j = 0, cntJ = quadFaces.Size(); j < cntJ; ++j )
		{
			facesList.Append( *quadFaces.Get( j ) );
		}
	}

	return (facesList);
}

//-----------------------------------------------------------------------------

FacesList CFacesBuilder::BuildRectClosedStripFaces( const CGeometry& geometry,
                                                    const CTextureProperties& texProperties )
{
	FacesList facesList = BuildRectOpenStripFaces( geometry, texProperties );

	size_t n = geometry.VerticesCount() / 2;

	FacesList quadFaces = BuildQuadFaces( geometry, n - 1, 0, n, 2 * n - 1, texProperties );
	for ( size_t j = 0, cntJ = quadFaces.Size(); j < cntJ; ++j )
	{
		facesList.Append( *quadFaces.Get( j ) );
	}

	return (facesList);
}

//-----------------------------------------------------------------------------

FacesList CFacesBuilder::BuildQuadsOpenStripFaces( const CGeometry& geometry,
                                                   const CTextureProperties& texProperties )
{
	FacesList facesList;

	// geometry.VerticesCount() MUST BE an even number
	size_t size = geometry.VerticesCount();
	assert( size % 2 == 0 );

	size_t n = size / 2;

	FacesList quadFaces;
	for ( size_t i = 0, cntI = n - 1; i < cntI; ++i )
	{
		quadFaces = BuildQuadFaces( geometry, i, i + 1, i + n + 1, i + n, texProperties );
		for ( size_t j = 0, cntJ = quadFaces.Size(); j < cntJ; ++j )
		{
			facesList.Append( *quadFaces.Get( j ) );
		}
	}

	return (facesList);
}

//-----------------------------------------------------------------------------

FacesList CFacesBuilder::BuildQuadsClosedStripFaces( const CGeometry& geometry,
                                                     const CTextureProperties& texProperties )
{
	FacesList facesList = BuildQuadsOpenStripFaces( geometry, texProperties );

	size_t n = geometry.VerticesCount() / 2;

	FacesList quadFaces = BuildQuadFaces( geometry, n - 1, 0, n, 2 * n - 1, texProperties );
	for ( size_t j = 0, cntJ = quadFaces.Size(); j < cntJ; ++j )
	{
		facesList.Append( *quadFaces.Get( j ) );
	}

	return (facesList);
}

//-----------------------------------------------------------------------------

FacesList CFacesBuilder::BuildQuadFaces( const CGeometry& geometry, size_t index0, 
                                        size_t index1, size_t index2, size_t index3,
                                        const CTextureProperties& texProperties )
{
	FacesList facesList;

	CFace face1;
	face1.SetIndex( 0, index0 );
	face1.SetIndex( 1, index1 );
	face1.SetIndex( 2, index2 );

	CFace face2;
	face2.SetIndex( 0, index0 );
	face2.SetIndex( 1, index2 );
	face2.SetIndex( 2, index3 );

	const CTexture* texture = texProperties.GetTexture();
	if ( texture )
	{
		const CGPoint2& offset = texProperties.GetOffset();
		Float rotation = texProperties.GetRotation();
		Float scale    = texProperties.GetScale();

		CUvTriangle3 tri1( geometry.Vertex( index0 ), geometry.Vertex( index1 ), 
                       geometry.Vertex( index2 ) );
		tri1.CalculateUVs( offset, rotation, scale );
		for ( size_t i = 0; i < 3; ++i )
		{
			face1.SetUV( i, tri1.GetUv( i ) );
		}
		face1.SetTexture( texture );

		CGVector2 vec01( tri1.GetUv( 1 ), tri1.GetUv( 0 ) );
		CGVector2 vec02( tri1.GetUv( 2 ), tri1.GetUv( 0 ) );
		Float correction = vec01.Direction() - vec02.Direction();
		CUvTriangle3 tri2( geometry.Vertex( index0 ), geometry.Vertex( index2 ), 
                       geometry.Vertex( index3 ) );
		tri2.CalculateUVs( offset, rotation + correction, scale );
		for ( size_t i = 0; i < 3; ++i )
		{
			face2.SetUV( i, tri2.GetUv( i ) );
		}
		face2.SetTexture( texture );
	}
	else
	{
		face1.SetVisible( false ); 
		face2.SetVisible( false ); 
		face1.SetShadow( false ); 
		face2.SetShadow( false ); 
	}

	facesList.Append( face1 );
	facesList.Append( face2 );

	return (facesList);
}

//-----------------------------------------------------------------------------

FacesList CFacesBuilder::BuildBoxoidFaces( const CGeometry& geometry, 
                                           const CTextureProperties& face1TexProps,
                                           const CTextureProperties& face2TexProps,
                                           const CTextureProperties& face3TexProps,
                                           const CTextureProperties& face4TexProps,
                                           const CTextureProperties& topTexProps,
                                           const CTextureProperties& bottomTexProps )
{
	assert( geometry.VerticesCount() == 8 );

	FacesList facesList;

	FacesList faces[6];
	faces[0] = BuildQuadFaces( geometry, 0, 1, 5, 4, face1TexProps );
	faces[1] = BuildQuadFaces( geometry, 1, 2, 6, 5, face2TexProps );
	faces[2] = BuildQuadFaces( geometry, 2, 3, 7, 6, face3TexProps );
	faces[3] = BuildQuadFaces( geometry, 3, 0, 4, 7, face4TexProps );
	faces[4] = BuildQuadFaces( geometry, 4, 5, 6, 7, topTexProps );
	faces[5] = BuildQuadFaces( geometry, 3, 2, 1, 0, bottomTexProps );

	for ( size_t i = 0; i < 6; ++i )
	{
		for ( size_t j = 0, cntJ = faces[i].Size(); j < cntJ; ++j )
		{
			facesList.Append( *faces[i].Get( j ) );
		}
	}

	return (facesList);
}

//-----------------------------------------------------------------------------

FacesList CFacesBuilder::BuildFacesFromHorizontalTriangulation( const CGeometry& geometry,
                                                                const vector< CGTriangle2 >& triangulation,
                                                                Float elevation, 
                                                                Float minX, Float maxY,
                                                                Float refLength,
                                                                const CTexture* texture,
                                                                bool invertNormal ) 
{
	assert( refLength > CGConsts::ZERO );
	Float invRefLength = CGConsts::ONE / refLength;

	// assumes that all the triangles have the vertices CCW oriented
	FacesList facesList;

	size_t index1;
	size_t index2;
	size_t index3;
	CGPoint2 uv1;
	CGPoint2 uv2;
	CGPoint2 uv3;
	for ( size_t i = 0, cntI = triangulation.size(); i < cntI; ++i )
	{
		const CGPoint2& point1 = triangulation[i].Vertex( 0 );
		const CGPoint2& point2 = triangulation[i].Vertex( 1 );
		const CGPoint2& point3 = triangulation[i].Vertex( 2 );
		index1 = geometry.FindVertexIndex( CGPoint3( point1.GetX(), point1.GetY(), elevation ) );
		index2 = geometry.FindVertexIndex( CGPoint3( point2.GetX(), point2.GetY(), elevation ) );
		index3 = geometry.FindVertexIndex( CGPoint3( point3.GetX(), point3.GetY(), elevation ) );
		assert( index1 != -1 );
		assert( index2 != -1 );
		assert( index3 != -1 );
		uv1 = CGPoint2( invRefLength * (point1.GetX() - minX), invRefLength * (maxY - point1.GetY()) );
		uv2 = CGPoint2( invRefLength * (point2.GetX() - minX), invRefLength * (maxY - point2.GetY()) );
		uv3 = CGPoint2( invRefLength * (point3.GetX() - minX), invRefLength * (maxY - point3.GetY()) );
		if ( invertNormal )
		{
			CFace face( index3, index2, index1, uv3, uv2, uv1, texture );
			facesList.Append( face );
		}
		else
		{
			CFace face( index1, index2, index3, uv1, uv2, uv3, texture );
			facesList.Append( face );
		}
	}

	return (facesList);
}

//-----------------------------------------------------------------------------

FacesList CFacesBuilder::BuildFacesFromTriangulation( const CGeometry& geometry,
                                                      const vector< CGTriangle2 >& triangulation, 
                                                      const CGSegment2& refEdge, 
                                                      Float refLength,
                                                      const CTexture* texture,
                                                      bool isTop, bool invertNormal )
{
	// assumes that all the triangles have the vertices CCW oriented
	FacesList facesList;

	size_t indexOffset = 0;
	if ( isTop )
	{
		indexOffset = geometry.VerticesCount() / 2;
	}

	const CGPoint2& pivot = refEdge.GetFrom();
	Float refX = pivot.GetX();
	Float refY = pivot.GetY();
	Float rotation = -refEdge.Direction();

	CGPoint2 p0;
	CGPoint2 p1;
	CGPoint2 p2;
	size_t index1;
	size_t index2;
	size_t index3;
	CGPoint2 uv1;
	CGPoint2 uv2;
	CGPoint2 uv3;
	for ( size_t i = 0, cntI = triangulation.size(); i < cntI; ++i )
	{
		p0 = triangulation[i].Vertex( 0 );
		p1 = triangulation[i].Vertex( 1 );
		p2 = triangulation[i].Vertex( 2 );
		index1 = geometry.FindVertexIndex( p0, indexOffset );
		index2 = geometry.FindVertexIndex( p1, indexOffset );
		index3 = geometry.FindVertexIndex( p2, indexOffset );
		assert( index1 != -1 );
		assert( index2 != -1 );
		assert( index3 != -1 );
		p0.Rotate( pivot, rotation );
		p1.Rotate( pivot, rotation );
		p2.Rotate( pivot, rotation );
		uv1 = CGPoint2( refLength * (p0.GetX() - refX), refLength * (refY - p0.GetY()) );
		uv2 = CGPoint2( refLength * (p1.GetX() - refX), refLength * (refY - p1.GetY()) );
		uv3 = CGPoint2( refLength * (p2.GetX() - refX), refLength * (refY - p2.GetY()) );
		if ( invertNormal )
		{
			CFace face( index3, index2, index1, uv3, uv2, uv1, texture );
			facesList.Append( face );
		}
		else
		{
			CFace face( index1, index2, index3, uv1, uv2, uv3, texture );
			facesList.Append( face );
		}
	}

	return (facesList);
}

//-----------------------------------------------------------------------------
