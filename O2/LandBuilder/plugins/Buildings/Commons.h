//-----------------------------------------------------------------------------
// File: Commons.h
//
// Desc: common data, definitions and functions
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <fstream>
#include <map>
#include <vector>

//-----------------------------------------------------------------------------

#include <projects/ObjektivLib/LODObject.h>

//-----------------------------------------------------------------------------

#include "CG_HoledPolygon2.h"
#include "CG_Triangle3.h"
#include "CG_Line2.h"
#include "CG_Line3.h"
#include "CG_StraightSkeleton2.h"

//-----------------------------------------------------------------------------

using std::endl;
using std::map;
using std::ofstream;
using std::vector;

//-----------------------------------------------------------------------------

using namespace ObjektivLib; 

//-----------------------------------------------------------------------------

typedef RString String;
typedef double Float;

//-----------------------------------------------------------------------------

typedef CG_Consts< Float >            CGConsts;
typedef CG_HoledPolygon2< Float >     CGHoledPolygon2;
typedef CG_Line2< Float >             CGLine2;
typedef CG_Line3< Float >             CGLine3;
typedef CG_Matrix3x3< Float >         CGMatrix3x3;
typedef CG_Point2< Float >            CGPoint2;
typedef CG_Point3< Float >            CGPoint3;
typedef CG_Polygon2< Float >          CGPolygon2;
typedef CG_Rectangle2< Float >        CGRectangle2;
typedef CG_Segment2< Float >          CGSegment2;
typedef CG_StraightSkeleton2< Float > CGStraightSkeleton2;
typedef CG_Triangle2< Float >         CGTriangle2;
typedef CG_Triangle3< Float >         CGTriangle3;
typedef CG_Vector2< Float >           CGVector2;
typedef CG_Vector3< Float >           CGVector3;

//-----------------------------------------------------------------------------

#define TOLERANCE_MM 0.001
#define TOLERANCE_CM 0.01
#define TOLERANCE_DM 0.1

//-----------------------------------------------------------------------------

Float RandomInRange( Float min, Float max );

//-----------------------------------------------------------------------------

typedef enum
{
	BLT_UNKNOWN,
	BLT_FLOOR,
	BLT_INTERFLOOR,
	BLT_ROOF
} EBuildingLevelType;

//-----------------------------------------------------------------------------

typedef enum
{
	BFT_SOLID,
	BFT_SOLID_GROUND,
	BFT_SOLID_UNDERGROUND,
	BFT_ENTERABLE,
	BFT_ENTERABLE_GROUND
} EBuildingFloorsType;

//-----------------------------------------------------------------------------

typedef enum
{
	BIFT_SOLID
} EBuildingInterFloorsType;

//-----------------------------------------------------------------------------

typedef enum
{
	BRT_FLAT,
	BRT_FLAT_WALLED,
	BRT_SLOPED,
	BRT_HIP,
	BRT_HIP_OFFSET,
	BRT_COUNT
} EBuildingRoofsType;

//-----------------------------------------------------------------------------
