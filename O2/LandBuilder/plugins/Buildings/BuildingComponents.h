//-----------------------------------------------------------------------------
// File: BuildingComponents.h
//
// Desc: classes related to building components
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"
#include "BuildingLevels.h"

//-----------------------------------------------------------------------------

class CBuildingComponent
{
	// The building level associated with this component
	CBuildingLevel* m_level;

	// The position of the center of the bounding rectangle of the footprint
	// of the level of this component with respect to the center of the bounding
	// rectangle containing the building
	// We use the center of the bounding rectangle instead of the barycenter
	// because it is easier to be identified when using the CAD
	CGPoint3 m_position;

	// The orientation of the footprint of the level of this component with respect 
	// to the building
	Float m_orientation;

	// The geometries of this building component
	GeometriesList m_geometries;

	// The proxies of this building component
	ProxyHinstancesList m_proxies;

public:
	CBuildingComponent( CBuildingLevel* level, const CGPoint3& position, 
                      Float orientation );

	// Getters
	// {
	const CBuildingLevel* GetLevel() const;
	const GeometriesList& GetGeometries() const;
	const ProxyHinstancesList& GetProxies() const;
	// }

	// Component manipulators
	// {
	// Transforms the geometry of this component so that it matches the building reference
	// system
	bool TransformGeometries();

	// Translates the geometry of this component using the given displacements
	void Translate( Float dx, Float dy, Float dz );

	// Rotates CCW the geometry of this component around the given axis passing through the given
	// center by the given angle in radians
	void Rotate( const CGVector3& axis, Float angle, const CGPoint3& center = CGPoint3() );
	// }
};

//-----------------------------------------------------------------------------

typedef TList< CBuildingComponent > BuildingComponentList;

//-----------------------------------------------------------------------------
