//-----------------------------------------------------------------------------
// File: FacesBuilder.h
//
// Desc: class for building faces
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"
#include "Geometry.h"
#include "UvTriangle3.h"

//-----------------------------------------------------------------------------

class CTextureProperties
{
	const CTexture* m_texture;
	Float           m_scale;
	const CGPoint2& m_offset; 
	Float           m_rotation; 

public:
	CTextureProperties( const CTexture* texture,
                      Float scale = CGConsts::ONE,
                      const CGPoint2& offset = CGPoint2(),
                      Float rotation = CGConsts::ZERO );

	// Getters
	// {
	const CTexture* GetTexture() const;
	Float GetScale() const;
	const CGPoint2& GetOffset() const; 
	Float GetRotation() const; 
	// }
};

//-----------------------------------------------------------------------------

class CFacesBuilder
{
public:
	// Builds the faces of an open strip of rectangles
	// Returns n - 2 faces, where n is the number of vertices of the given geometry
	static FacesList BuildRectOpenStripFaces( const CGeometry& geometry,
                                            const CTextureProperties& texProperties = CTextureProperties( NULL ) );

	// Builds the faces of a closed strip of rectangles
	// Returns n faces, where n is the number of vertices of the given geometry
	static FacesList BuildRectClosedStripFaces( const CGeometry& geometry,
                                              const CTextureProperties& texProperties = CTextureProperties( NULL ) );

	// Builds the faces of an open strip of quads
	// Returns n - 2 faces, where n is the number of vertices of the given geometry
	static FacesList BuildQuadsOpenStripFaces( const CGeometry& geometry,
                                             const CTextureProperties& texProperties = CTextureProperties( NULL ) );

	// Builds the faces of a closed strip of quads
	// Returns n faces, where n is the number of vertices of the given geometry
	static FacesList BuildQuadsClosedStripFaces( const CGeometry& geometry,
                                               const CTextureProperties& texProperties = CTextureProperties( NULL ) );

	// Builds the faces of a generic planar quad 
	// Returns 2 faces
	static FacesList BuildQuadFaces( const CGeometry& geometry, 
                                   size_t index0, size_t index1, 
                                   size_t index2, size_t index3, 
                                   const CTextureProperties& texProperties = CTextureProperties( NULL ) );

	// Builds the faces of a generic boxoid solid
	// Returns 12 faces
	static FacesList BuildBoxoidFaces( const CGeometry& geometry,
                                     const CTextureProperties& face1TexProps = CTextureProperties( NULL ),
                                     const CTextureProperties& face2TexProps = CTextureProperties( NULL ),
                                     const CTextureProperties& face3TexProps = CTextureProperties( NULL ),
                                     const CTextureProperties& face4TexProps = CTextureProperties( NULL ),
                                     const CTextureProperties& topTexProps = CTextureProperties( NULL ),
                                     const CTextureProperties& bottomTexProps = CTextureProperties( NULL ) );

	// Transforms the given triangulation in a set of faces
	// Returns a face for each triangle in the triangulation
	static FacesList BuildFacesFromHorizontalTriangulation( const CGeometry& geometry,
                                                          const vector< CGTriangle2 >& triangulation, 
                                                          Float elevation, 
                                                          Float minX, Float maxY, 
                                                          Float refLength,
                                                          const CTexture* texture,
                                                          bool invertNormal = false );

	// Transforms the given triangulation in a set of faces
	// It is supposed that the given geometry contain a cilinder-like geometry:
	// the vertices are the contours of the two caps of the cilinder, there is 
	// a biunivoc correspondence between the vertices of the two contours and the 
	// total count of vertices is even ( 2 x N ), the first N vertices are the 
	// contour of the bottom cap, the remaining are the contour of the top cap.
	// The 2 caps share the same projection on the XY plane, so they share its
	// triangulation too.
	// Returns a face for each triangle in the triangulation
	static FacesList BuildFacesFromTriangulation( const CGeometry& geometry,
                                                const vector< CGTriangle2 >& triangulation, 
                                                const CGSegment2& refEdge, 
                                                Float refLength,
                                                const CTexture* texture,
                                                bool isTop = false,
                                                bool invertNormal = false );
};

//-----------------------------------------------------------------------------
