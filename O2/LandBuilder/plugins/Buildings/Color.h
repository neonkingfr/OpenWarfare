//-----------------------------------------------------------------------------
// File: Color.h
//
// Desc: an RGB color class
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

// -------------------------------------------------------------------------- //

#include "Commons.h"

// -------------------------------------------------------------------------- //

class CColor
{
	size_t m_red;
	size_t m_green;
	size_t m_blue;

public:
	CColor();
	CColor( size_t red, size_t green, size_t blue );
	CColor( const CColor& other );

	// Getters
	// {
	size_t GetRed() const;
	size_t GetGreen() const;
	size_t GetBlue() const;
	// }

	// Setters
	// {
	void SetRed( size_t red );
	void SetGreen( size_t green );
	void SetBlue( size_t blue );
	// }

	// Comparison
	// {
	bool operator == ( const CColor& other ) const;
	bool operator != ( const CColor& other ) const;
	// }

	String ToString() const;
	String ToTexture() const;
};

// -------------------------------------------------------------------------- //
