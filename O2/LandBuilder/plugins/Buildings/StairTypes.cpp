//-----------------------------------------------------------------------------
// File: StairTypes.cpp
//
// Desc: classes related to stair types 
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "StairTypes.h"

//-----------------------------------------------------------------------------

CStairType::CStairType( const String& name, Float width )
: m_name( name )
, m_width( width )
{
}

//-----------------------------------------------------------------------------

CStairType::CStairType( const CStairType& other )
: m_name( other.m_name )
, m_width( other.m_width )
{
}

//-----------------------------------------------------------------------------

const String& CStairType::GetName() const
{
	return (m_name);
}

//-----------------------------------------------------------------------------

Float CStairType::GetWidth() const
{
	return (m_width);
}

//-----------------------------------------------------------------------------

const CTexture* CStairType::GetTexture( const String& key ) const
{
	return (m_textures.Get( key ));
}

//-----------------------------------------------------------------------------

void CStairType::AddTexture( const String& key, const CTexture& texture )
{
	m_textures.Append( key, texture );
}

//-----------------------------------------------------------------------------

bool CStairType::operator == ( const CStairType& other ) const
{
	if ( this == &other ) { return (true); }

	if ( m_name     != other.m_name )     { return (false); }
	if ( m_textures != other.m_textures ) { return (false); }

	return (true);
}

//-----------------------------------------------------------------------------

bool CStairType::operator != ( const CStairType& other ) const
{
	return (!(this->operator==( other )));
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CStairTypesLibrary::CStairTypesLibrary( const String& modelsPath )
: m_modelsPath( modelsPath )
{
}

//-----------------------------------------------------------------------------

CStairTypesLibrary::~CStairTypesLibrary()
{
}

//-----------------------------------------------------------------------------

void CStairTypesLibrary::SetModelsPath( const String& modelsPath )
{
	m_modelsPath = modelsPath;
}

//-----------------------------------------------------------------------------

bool CStairTypesLibrary::LoadFromCfgFile( const String& filename, ofstream& logFile )
{
	// clears the list
	Clear();

	String path = m_modelsPath;
	if ( path[path.GetLength() - 1] == '\\' )
	{
		path = path.Substring( 0, path.GetLength() - 1 );
	}
	String texturesPath = path + "\\Data\\";

	// adds the stairs types from the config file
	ifstream in( filename, std::ios::in );
	if ( in.fail() )
	{
    LOGF( Error ) << "Failed to open config file.";
    logFile << "ERROR: Failed to open the file \"" << filename << "\"." << endl;
		return (false);
	}
	else
	{
		in.close();
	}

	ParamFile parFile;
	LSError err = parFile.Parse( filename );

	if ( err != LSOK ) { return (false); }

	ParamEntryPtr entry = parFile.FindEntry( "StairTypesLibrary" );

	if ( entry.IsNull() )
	{
		LOGF( Error ) << "Cannot find class 'StairTypesLibrary'";
		logFile << "WARNING: Unable to find class \"StairTypesLibrary\" inside the file \"" << filename << "\"." << endl;
		logFile << "         No stair type defined." << endl << endl;
		return (true);
	}

	if ( !entry->IsClass() ) 
	{
		LOGF( Error ) << "'StairTypesLibrary' must be a class";
		logFile << "ERROR: entry \"StairTypesLibrary\" inside the file \"" << filename << "\" must be a class." << endl;
		return (false);
	}

	ParamClassPtr stairClass = entry->GetClassInterface();
	size_t i = 1;
	char buff[20];

    do
	{
		sprintf( buff, "Stair%04d", i );
		entry = stairClass->FindEntry( buff );

		if ( entry && entry->IsClass() )
		{
			ParamClassPtr item = entry->GetClassInterface();

			// name
			String name = buff;
			entry = item->FindEntry( STAIR_NAME );            
			if ( entry.IsNull() ) 
			{
				LOGF( Error ) << "Missing " << buff << "::" << STAIR_NAME << " field. Using default (" << name << ").";
				logFile << "WARNING: Missing " << buff << "::" << STAIR_NAME << " field. Using default (" << name << ")." << endl << endl;
			}
			else
			{
				name = entry->GetValue();
			}

			// width
			Float width = LoadDoubleFromCfgFile( buff, item, STAIR_WIDTH, DEFAULT_STAIR_WIDTH, CGConsts::ZERO, CGConsts::MAX_REAL, logFile );

			// ramp color 
			CColor rampColor = LoadColorFromCfgFile( buff, item,
													 DEFAULT_STAIR_RAMP_COLOR,
													 STAIR_RAMP_COLOR, logFile );

			// ramp texture
			String rampTex = LoadTextureFromCfgFile( buff, item, 
													 STAIR_RAMP_TEX, 
													 rampColor, logFile );

			// wall color 
			CColor wallColor = LoadColorFromCfgFile( buff, item,
													 DEFAULT_STAIR_WALL_COLOR,
													 STAIR_WALL_COLOR, logFile );

			// wall texture
			String wallTex = LoadTextureFromCfgFile( buff, item, 
													 STAIR_WALL_TEX, 
													 wallColor, logFile );

			CStairType* pType = new CStairType( name, width );

			pType->AddTexture( STAIR_RAMP_TEX, CTexture( rampTex, texturesPath, rampColor ) );
			pType->AddTexture( STAIR_WALL_TEX, CTexture( wallTex, texturesPath, wallColor ) );

			m_elements.push_back( pType );
		}

		++i;
		if ( i > 9999 ) { break; }
	}
	while ( true );

	return (true);
}

//-----------------------------------------------------------------------------
