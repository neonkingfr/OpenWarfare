//-----------------------------------------------------------------------------
// File: Models.h
//
// Desc: classes related to building models
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

// -------------------------------------------------------------------------- //

#include "Commons.h"
#include "Templates.h"

// -------------------------------------------------------------------------- //

class CModel
{
protected:
	// A pointer to the template associated to this model
	CTemplate* m_pTemplate;

	// The position of this model at creation time
	CGPoint2 m_creationPosition;

	// The orientation of this model at creation time
	Float m_creationOrientation;

	// The final position of this model
	CGPoint2 m_position;

	// The final orientation of this model
	Float m_orientation;

public:
	CModel( CTemplate* pTemplate, const CGPoint2& creationPosition, 
			Float creationOrientation );
	CModel( const CModel& other );
	virtual ~CModel();

	// Getters
	// {
	const CTemplate* GetTemplate() const;
	const CGPoint2& GetCreationPosition() const;
	Float GetCreationOrientation() const;
	const CGPoint2& GetPosition() const;
	Float GetOrientation() const;
	// }

protected:
	// Initializes this model
	// This method must be called by children of this class in their constructor
	void Initialize();
	virtual void CalcPosition() = 0;
	virtual void CalcDirection() = 0;
};

// -------------------------------------------------------------------------- //

class CBuilding : public CModel
{
public:
	CBuilding( CTemplate* pTemplate, const CGPoint2& creationPosition, 
			   Float creationOrientation );
	virtual ~CBuilding();

	// CModel implementation
	// {
	void CalcPosition();
	void CalcDirection();
	// }
};

// -------------------------------------------------------------------------- //
