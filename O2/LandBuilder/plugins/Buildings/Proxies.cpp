//-----------------------------------------------------------------------------
// File: Proxies.cpp
//
// Desc: classes related to proxies
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Proxies.h"

//-----------------------------------------------------------------------------

CProxy::CProxy()
: m_srcFilename( "" )
, m_dstFolder( "" )
, m_exported( false )
{
}

//-----------------------------------------------------------------------------

CProxy::CProxy( const String& filename, const String& dstFolder )
: m_srcFilename( filename )
, m_dstFolder( dstFolder )
, m_exported( false )
{
}

//-----------------------------------------------------------------------------

CProxy::CProxy( const CProxy& other )
: m_srcFilename( other.m_srcFilename )
, m_dstFolder( other.m_dstFolder )
, m_exported( other.m_exported )
{
}

//-----------------------------------------------------------------------------

CProxy::~CProxy()
{
}

//-----------------------------------------------------------------------------

bool CProxy::operator == ( const CProxy& other ) const
{
	if ( this == &other ) { return (true); }

	if ( m_srcFilename != other.m_srcFilename ) { return (false); }
	if ( m_dstFolder   != other.m_dstFolder )   { return (false); }

	return (true);
}

//-----------------------------------------------------------------------------

bool CProxy::operator != ( const CProxy& other ) const
{
	return (!(this->operator==( other )));
}

//-----------------------------------------------------------------------------

String CProxy::GetDstAbsFilename() const
{
	if ( m_srcFilename.IsEmpty() ) { return (""); }

	int pos = m_srcFilename.ReverseFind( '\\' );
	return (m_dstFolder + m_srcFilename.Substring( pos + 1, m_srcFilename.GetLength() ));
}

//-----------------------------------------------------------------------------

String CProxy::GetDstRelFilename() const
{
	if ( m_srcFilename.IsEmpty() ) { return (""); }
	
	String root = m_dstFolder;
  int pos = root.Find( ':' ) + 2;
  root = root.Substring( pos, root.GetLength() );

	pos = m_srcFilename.ReverseFind( '\\' );
	return (root + m_srcFilename.Substring( pos + 1, m_srcFilename.GetLength() ));
}

//-----------------------------------------------------------------------------

bool CProxy::Export() const
{
	if ( m_exported ) { return (true); }

	String dstFilename = GetDstAbsFilename();

	if ( !dstFilename.IsEmpty() )
	{
		// copy the proxy on disk.
		// first tests if the proxy has been already saved 
		// (it maybe be shared with other models or may already exist)
		// source proxies can be everywhere, destination proxies
		// will be saved in a \data\proxy subfolder of the folder containing
		// the models
		ifstream test( dstFilename, std::ios::binary );
		if ( test.fail() )
		{
			ifstream ifs( m_srcFilename, std::ios::binary );
			ofstream ofs( dstFilename, std::ios::binary );

			const int BUFFER_SIZE = 4096;
			char buffer[BUFFER_SIZE];

			while ( !ifs.eof() ) 
			{
				ifs._Read_s( buffer, BUFFER_SIZE, BUFFER_SIZE );
				if ( !ifs.bad() ) 
				{
					ofs.write( buffer, ifs.gcount() );
				}
			}

			ifs.close();
			ofs.close();
		}
		else
		{
			test.close();
		}		

		m_exported = true;
	}

	return (true);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CProxyHinstance::CProxyHinstance()
: m_proxy( NULL )
{
}

//-----------------------------------------------------------------------------

CProxyHinstance::CProxyHinstance( const CProxy* proxy, const CGPoint3& position, 
                                  const CGVector3& axisX, const CGVector3& axisY,
                                  const CGVector3& axisZ )
: m_proxy( proxy )
, m_position( position )
, m_axisX( axisX )
, m_axisY( axisY )
, m_axisZ( axisZ )
{
	// ensures that the vectors are unit vectors
	m_axisX.Normalize();
	m_axisY.Normalize();
	m_axisZ.Normalize();
}

//-----------------------------------------------------------------------------

CProxyHinstance::CProxyHinstance( const CProxyHinstance& other )
: m_proxy( other.m_proxy )
, m_position( other.m_position )
, m_axisX( other.m_axisX )
, m_axisY( other.m_axisY )
, m_axisZ( other.m_axisZ )
{
}

//-----------------------------------------------------------------------------

CProxyHinstance::~CProxyHinstance()
{
}

//-----------------------------------------------------------------------------

const CProxy* CProxyHinstance::GetProxy() const
{
	return (m_proxy);
}

//-----------------------------------------------------------------------------

const CGPoint3& CProxyHinstance::GetPosition() const
{
	return (m_position);
}

//-----------------------------------------------------------------------------

const CGVector3& CProxyHinstance::GetAxisX() const
{
	return (m_axisX);
}

//-----------------------------------------------------------------------------

const CGVector3& CProxyHinstance::GetAxisY() const
{
	return (m_axisY);
}

//-----------------------------------------------------------------------------

const CGVector3& CProxyHinstance::GetAxisZ() const
{
	return (m_axisZ);
}

//-----------------------------------------------------------------------------

void CProxyHinstance::Translate( Float dx, Float dy, Float dz )
{
	m_position.Translate( dx, dy, dz );
}

//-----------------------------------------------------------------------------

void CProxyHinstance::Rotate( const CGVector3& axis, Float angle, const CGPoint3& center )
{
	m_position.Rotate( axis, angle, center );
	m_axisX.Rotate( axis, angle );
	m_axisY.Rotate( axis, angle );
	m_axisZ.Rotate( axis, angle );
}

//-----------------------------------------------------------------------------
