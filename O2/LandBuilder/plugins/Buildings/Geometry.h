//-----------------------------------------------------------------------------
// File: Geometry.h
//
// Desc: class for templates geometry
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"
#include "Face.h"
#include "TList.h"

//-----------------------------------------------------------------------------

typedef TList< CGPoint3 > Point3List;
typedef TList< CFace >    FacesList;

//-----------------------------------------------------------------------------

class CGeometry
{
	Point3List m_vertices;
	FacesList  m_faces;

public:
	CGeometry();
	CGeometry( const CGeometry& other );
	~CGeometry();

	// assignement
	CGeometry& operator = ( const CGeometry& other );

	// Getters
	// {
	const Point3List& GetVertices() const;
	const FacesList& GetFaces() const;
	// }

	// Setters
	// {
	void SetFaces( const FacesList& faces );
	// }

	// Geometry manipulators
	// {
	// Clears this geometry
	void Clear();

	// Adds the given vertex to this geometry and returns its index
	size_t AddVertex( const CGPoint3& vertex, Float tolerance = CGConsts::ZERO_TOLERANCE );

	// Adds the given face to this geometry and returns its index
	size_t AddFace( const CFace& face );

	void RemoveFace( size_t index );
	void MergeWith( const CGeometry& other, bool removeDuplicatedFaces, Float tolerance = CGConsts::ZERO_TOLERANCE );

	// Translates this geometry using the given displacements
	void Translate( Float dx, Float dy, Float dz );

	// Rotates CCW this geometry around the given axis passing through the given
	// center by the given angle in radians
	void Rotate( const CGVector3& axis, Float angle, const CGPoint3& center = CGPoint3() );
	// }

	// Geometry properties
	// {
	// Returns the number of vertices of this geometry
	size_t VerticesCount() const;

	// Returns the number of faces of this geometry
	size_t FacesCount() const;

	// Returns the vertex of this geometry with the given index
	const CGPoint3& Vertex( size_t index ) const;
	CGPoint3& Vertex( size_t index );

	// Returns the face of this geometry with the given index
	const CFace& Face( size_t index ) const;
	CFace& Face( size_t index );

	// Returns the index of the vertex of this geometry which coincide with
	// the given point or -1 if no match is found
	int FindVertexIndex( const CGPoint3& point, Float tolerance = CGConsts::ZERO_TOLERANCE ) const;

	// Returns the index of the vertex of this geometry which has the x and y 
	// coordinates of the given point or -1 if no match is found
	int FindVertexIndex( const CGPoint2& point, size_t startIndex, 
                       Float tolerance = CGConsts::ZERO_TOLERANCE ) const;
};

//-----------------------------------------------------------------------------
