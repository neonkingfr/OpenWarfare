//-----------------------------------------------------------------------------
// File: Texture.h
//
// Desc: classes related to textures
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"
#include "TList.h"
#include "Color.h"

//-----------------------------------------------------------------------------

class CTexture
{
	// The filename (fullpath) of the source of this texture
	String m_srcFilename;

	// The folder (fullpath) of the destination file of this texture
	String m_dstFolder;

	// The uniform color of this texture when no filename is specified
	CColor m_color;

	// Whether or not this texture has already been exported to file
	mutable bool m_exported;

public:
	CTexture();
	CTexture( const String& srcFilename, const String& dstFolder, const CColor& color );
	CTexture( const CTexture& other );
	~CTexture();

	// Comparison
	// {
	bool operator == ( const CTexture& other ) const;
	bool operator != ( const CTexture& other ) const;
	// }

	// Returns the absolute destination filename of this texture 
	String GetDstAbsFilename() const;

	// Returns the destination filename of this texture relative to the root folder
	String GetDstRelFilename() const;

	// Exports this texture to file
	bool Export() const;
};

//-----------------------------------------------------------------------------

typedef TKeyList< CTexture > TexturesList;

//-----------------------------------------------------------------------------
