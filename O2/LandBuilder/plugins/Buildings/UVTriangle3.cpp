//-----------------------------------------------------------------------------
// File: UvTriangle3.cpp
//
// Desc: class for an uv mapped three dimensional triangle
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "UvTriangle3.h"

//-----------------------------------------------------------------------------

CUvTriangle3::CUvTriangle3()
: CGTriangle3()
{
}

//-----------------------------------------------------------------------------

CUvTriangle3::CUvTriangle3( const CGPoint3& v1, const CGPoint3& v2,
						    const CGPoint3& v3 )
: CGTriangle3( v1, v2, v3 )
{
}

//-----------------------------------------------------------------------------

CUvTriangle3::~CUvTriangle3()
{
}

//-----------------------------------------------------------------------------

const CGPoint2& CUvTriangle3::GetUv( size_t index ) const
{
	assert( index < 3 );
	return (m_uv[index]);
}

//-----------------------------------------------------------------------------

void CUvTriangle3::CalculateUVs( const CGPoint2& firstVertex, Float rotation, 
								 Float scale )
{
	// creates a copy of this triangle
	CGTriangle3 tempCopy( *this );

	// moves the triangle so that vertex 0 is in the origin
	const CGPoint3& p = tempCopy.Vertex( 0 );
	tempCopy.Translate( -p.GetX(), -p.GetY(), -p.GetZ() );

	// tranforms the triangle so that it will belong to the XY plane
	CGVector3 normal = tempCopy.Normal();
	CGVector3 vec01( tempCopy.Vertex( 1 ), tempCopy.Vertex( 0 ) );
	vec01.Normalize();
	CGVector3 perpVec01 = normal.Cross( vec01 ).Normalized();

	CGMatrix3x3 mat;
	mat.SetFromBasis( vec01, perpVec01, normal );
	tempCopy.Rotate( mat );

	// places the triangle with respect to the texture
	CGPoint2 p0( tempCopy.Vertex( 0 ).GetX(), tempCopy.Vertex( 0 ).GetY() );
	CGPoint2 p1( tempCopy.Vertex( 1 ).GetX(), tempCopy.Vertex( 1 ).GetY() );
	CGPoint2 p2( tempCopy.Vertex( 2 ).GetX(), tempCopy.Vertex( 2 ).GetY() );
	CGTriangle2 tempProj( p0, p1, p2 );

	tempProj.Scale( p0, scale );
	tempProj.Rotate( p0, rotation );
	tempProj.Translate( firstVertex.GetX() * scale, firstVertex.GetY() * scale );

	// v = 0 is at the top of the texture
	// v = 1 is at the bottom of the texture
	tempProj.Vertex( 0 ).SetY( CGConsts::ONE - tempProj.Vertex( 0 ).GetY() );
	tempProj.Vertex( 1 ).SetY( CGConsts::ONE - tempProj.Vertex( 1 ).GetY() );
	tempProj.Vertex( 2 ).SetY( CGConsts::ONE - tempProj.Vertex( 2 ).GetY() );

	m_uv[0] = tempProj.Vertex( 0 );
	m_uv[1] = tempProj.Vertex( 1 );
	m_uv[2] = tempProj.Vertex( 2 );
}

//-----------------------------------------------------------------------------
