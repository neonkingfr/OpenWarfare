#include "StdAfx.h"
#include "ModuleRegistrator.h"

// -------------------------------------------------------------------------- //

namespace LandBuilder2
{
	using namespace LandBuildInt;
}

// -------------------------------------------------------------------------- //

//source contains list of registered modules. 
/**Don't add new modules here. Instead, register module using the registrator 
at place of module's definition
*/
#include "Edifici.h"

// -------------------------------------------------------------------------- //

namespace LandBuilder2
{
	RegisterModule< Modules::Edifici > _register_BU_001( "Edifici" );
}

// -------------------------------------------------------------------------- //
