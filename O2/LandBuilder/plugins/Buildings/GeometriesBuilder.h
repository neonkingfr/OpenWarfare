//-----------------------------------------------------------------------------
// File: GeometriesBuilder.h
//
// Desc: class for building geometries
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"
#include "FacesBuilder.h"

//-----------------------------------------------------------------------------

typedef TList< CGeometry > GeometriesList;

//-----------------------------------------------------------------------------

class CGeometriesBuilder
{
public:
	// Builds the geometry of a wall section
	static GeometriesList BuildWallGeometry( const CGeometry& geometry,
                                           Float refHeight,
                                           const CTexture* exTexture, 
                                           const CTexture* inTexture,
                                           const CTexture* topTexture );

	// Builds the geometry of an internal wall section
	static GeometriesList BuildInternalWallGeometry( const CGeometry& geometry,
                                                   Float refHeight,
                                                   const CTexture* exTexture, 
                                                   const CTexture* inTexture );

	// Builds the geometry of a door section
	static GeometriesList BuildDoorGeometry( const CGeometry& geometry,
                                           Float doorWidth, 
                                           Float doorHeight,
                                           Float refHeight,
                                           const CTexture* exTexture, 
                                           const CTexture* inTexture );

	// Builds the geometry of a window section
	static GeometriesList BuildWindowGeometry( const CGeometry& geometry,
                                             Float windowWidth, 
                                             Float windowHeight,
                                             Float refHeight,
                                             const CTexture* exTexture, 
                                             const CTexture* inTexture );
};

//-----------------------------------------------------------------------------
