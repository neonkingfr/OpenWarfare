//-----------------------------------------------------------------------------
// File: Commons.cpp
//
// Desc: common data, definitions and functions
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Commons.h"

//-----------------------------------------------------------------------------

Float RandomInRange( Float min, Float max )
{
	return (min + (static_cast< Float >(rand()) / static_cast< Float >(RAND_MAX)) * (max - min));
}

// --------------------------------------------------------------------//
