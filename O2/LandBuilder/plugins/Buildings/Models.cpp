//-----------------------------------------------------------------------------
// File: Models.cpp
//
// Desc: classes related to building models
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include ".\Models.h"

// ------------------------------------------------------------------------------ //

CModel::CModel( CTemplate* pTemplate, const CGPoint2& creationPosition,
                Float creationOrientation )
: m_pTemplate( pTemplate )
, m_creationPosition( creationPosition )
, m_creationOrientation( creationOrientation )
{
  assert( m_pTemplate );
  m_pTemplate->IncreaseReferencesCount();
}

// ------------------------------------------------------------------------------ //

CModel::CModel( const CModel& other )
: m_pTemplate( other.m_pTemplate )
, m_creationPosition( other.m_creationPosition ) 
, m_creationOrientation( other.m_creationOrientation ) 
, m_position( other.m_position ) 
, m_orientation( other.m_orientation )
{
  m_pTemplate->IncreaseReferencesCount();
}

// ------------------------------------------------------------------------------ //

CModel::~CModel()
{
  if ( m_pTemplate ) { m_pTemplate->DecreaseReferencesCount(); }
}

// ------------------------------------------------------------------------------ //

const CTemplate* CModel::GetTemplate() const
{
  return (m_pTemplate);
}

// ------------------------------------------------------------------------------ //

const CGPoint2& CModel::GetCreationPosition() const
{
  return (m_creationPosition);
}

// ------------------------------------------------------------------------------ //

Float CModel::GetCreationOrientation() const
{
  return (m_creationOrientation);
}

// ------------------------------------------------------------------------------ //

const CGPoint2& CModel::GetPosition() const
{
  return (m_position);
}

// ------------------------------------------------------------------------------ //

Float CModel::GetOrientation() const
{
  return (m_orientation);
}

// ------------------------------------------------------------------------------ //

void CModel::Initialize()
{
  CalcPosition();
  CalcDirection();
}

// ------------------------------------------------------------------------------ //
// ------------------------------------------------------------------------------ //

CBuilding::CBuilding( CTemplate* pTemplate, const CGPoint2& creationPosition,
                      Float creationOrientation )
: CModel( pTemplate, creationPosition, creationOrientation )
{
}

// ------------------------------------------------------------------------------ //

CBuilding::~CBuilding()
{
}

// ------------------------------------------------------------------------------ //

void CBuilding::CalcPosition()
{
  m_position = m_creationPosition;
}

// ------------------------------------------------------------------------------ //

void CBuilding::CalcDirection()
{
  m_orientation = m_creationOrientation;
}

// ------------------------------------------------------------------------------ //
