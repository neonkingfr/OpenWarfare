//-----------------------------------------------------------------------------
// File: GeometriesBuilder.cpp
//
// Desc: class for building geometries
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "GeometriesBuilder.h"

//-----------------------------------------------------------------------------

GeometriesList CGeometriesBuilder::BuildWallGeometry( const CGeometry& geometry,
                                                      Float refHeight,
                                                      const CTexture* exTexture, 
                                                      const CTexture* inTexture,
                                                      const CTexture* topTexture )
{
	assert( geometry.VerticesCount() == 8 );

	assert( refHeight > CGConsts::ZERO );
	Float scale = CGConsts::ONE / refHeight;

	Float baseHeight = geometry.Vertex( 0 ).GetZ();

	GeometriesList wallGeometry;

	CGeometry geometryW( geometry );

	FacesList facesW = CFacesBuilder::BuildBoxoidFaces( geometryW,
                                                      CTextureProperties( exTexture, scale, CGPoint2( CGConsts::ZERO, baseHeight ) ),
                                                      CTextureProperties( NULL ), 
                                                      CTextureProperties( inTexture, scale, CGPoint2( CGConsts::ZERO, baseHeight ) ), 
                                                      CTextureProperties( NULL ),
                                                      CTextureProperties( topTexture, scale ),
                                                      CTextureProperties( NULL ) );
	facesW.Get( 8 )->SetRoadWay( true );
	facesW.Get( 9 )->SetRoadWay( true );
	facesW.Get( 10 )->SetShadow( true );
	facesW.Get( 11 )->SetShadow( true );
	geometryW.SetFaces( facesW );

	wallGeometry.Append( geometryW );

	return (wallGeometry);
}

//-----------------------------------------------------------------------------

GeometriesList CGeometriesBuilder::BuildInternalWallGeometry( const CGeometry& geometry, 
                                                             Float refHeight,
                                                             const CTexture* exTexture, 
                                                             const CTexture* inTexture )
{
	assert( geometry.VerticesCount() == 8 );

	assert( refHeight > CGConsts::ZERO );
	Float scale = CGConsts::ONE / refHeight;

	GeometriesList wallGeometry;

	CGeometry geometryW( geometry );

	FacesList facesW = CFacesBuilder::BuildBoxoidFaces( geometryW,
                                                      CTextureProperties( exTexture, scale ),
                                                      CTextureProperties( NULL ), 
                                                      CTextureProperties( inTexture, scale ), 
                                                      CTextureProperties( NULL ),
                                                      CTextureProperties( NULL ),
                                                      CTextureProperties( NULL ) );
	facesW.Get( 8 )->SetShadow( true );
	facesW.Get( 9 )->SetShadow( true );
	facesW.Get( 10 )->SetShadow( true );
	facesW.Get( 11 )->SetShadow( true );
	geometryW.SetFaces( facesW );

	wallGeometry.Append( geometryW );

	return (wallGeometry);
}

//-----------------------------------------------------------------------------

GeometriesList CGeometriesBuilder::BuildDoorGeometry( const CGeometry& geometry,
                                                      Float doorWidth, Float doorHeight,
                                                      Float refHeight,
                                                      const CTexture* exTexture, 
                                                      const CTexture* inTexture )
{
	assert( geometry.VerticesCount() == 8 );
	assert( geometry.Vertex( 0 ).GetZ() == CGConsts::ZERO );

	GeometriesList doorGeometry;

	// first calculate the new vertices required by the door hole
	CGVector3 exVec( geometry.Vertex( 1 ), geometry.Vertex( 0 ) );
	CGVector3 inVec( geometry.Vertex( 2 ), geometry.Vertex( 3 ) );
	CGVector3 wallVec( geometry.Vertex( 1 ), geometry.Vertex( 2 ) );

	Float width        = exVec.Length();
	Float vMarginWidth = CGConsts::HALF * (width - doorWidth);
	Float wallWidth    = wallVec.Length();

	assert( refHeight > CGConsts::ZERO );
	Float scale = CGConsts::ONE / refHeight;

	// extern
	exVec.Normalize();
	exVec *= vMarginWidth;
	CGPoint3 ex0 = exVec.Head( geometry.Vertex( 0 ) );
	CGPoint3 ex2 = exVec.Head( geometry.Vertex( 0 ) );
	ex2.SetZ( doorHeight );

	exVec.Normalize();
	exVec *= (vMarginWidth + doorWidth);
	CGPoint3 ex1 = exVec.Head( geometry.Vertex( 0 ) );
	CGPoint3 ex3 = exVec.Head( geometry.Vertex( 0 ) );
	ex3.SetZ( doorHeight );

	// intern
	inVec.Normalize();
	inVec *= vMarginWidth;
	CGPoint3 in0 = inVec.Head( geometry.Vertex( 3 ) );
	CGPoint3 in2 = inVec.Head( geometry.Vertex( 3 ) );
	in2.SetZ( doorHeight );

	inVec.Normalize();
	inVec *= (vMarginWidth + doorWidth);
	CGPoint3 in1 = inVec.Head( geometry.Vertex( 3 ) );
	CGPoint3 in3 = inVec.Head( geometry.Vertex( 3 ) );
	in3.SetZ( doorHeight );

	// now sets the three geometries
	CGeometry geometryW;
	geometryW.AddVertex( geometry.Vertex( 0 ) );
	geometryW.AddVertex( ex0 );
	geometryW.AddVertex( in0 );
	geometryW.AddVertex( geometry.Vertex( 3 ) );
	geometryW.AddVertex( geometry.Vertex( 4 ) );
	geometryW.AddVertex( ex2 );
	geometryW.AddVertex( in2 );
	geometryW.AddVertex( geometry.Vertex( 7 ) );
	FacesList facesW = CFacesBuilder::BuildBoxoidFaces( geometryW,
                                                      CTextureProperties( exTexture, scale ),
                                                      CTextureProperties( inTexture, scale, CGPoint2( vMarginWidth + doorWidth - wallWidth, CGConsts::ZERO ) ), 
                                                      CTextureProperties( inTexture, scale, CGPoint2( vMarginWidth + doorWidth, CGConsts::ZERO ) ),
                                                      CTextureProperties( NULL ), 
                                                      CTextureProperties( NULL ),
                                                      CTextureProperties( NULL ) );
	facesW.Get( 10 )->SetShadow( true );
	facesW.Get( 11 )->SetShadow( true );
	geometryW.SetFaces( facesW );

	CGeometry geometryN;
	geometryN.AddVertex( ex2 );
	geometryN.AddVertex( ex3 );
	geometryN.AddVertex( in3 );
	geometryN.AddVertex( in2 );
	geometryN.AddVertex( geometry.Vertex( 4 ) );
	geometryN.AddVertex( geometry.Vertex( 5 ) );
	geometryN.AddVertex( geometry.Vertex( 6 ) );
	geometryN.AddVertex( geometry.Vertex( 7 ) );
	FacesList facesN = CFacesBuilder::BuildBoxoidFaces( geometryN, 
                                                      CTextureProperties( exTexture, scale, CGPoint2( vMarginWidth, doorHeight ) ),
                                                      CTextureProperties( NULL ), 
                                                      CTextureProperties( inTexture, scale, CGPoint2( vMarginWidth, doorHeight ) ), 
                                                      CTextureProperties( NULL ),
                                                      CTextureProperties( NULL ),
                                                      CTextureProperties( inTexture, scale, CGPoint2( vMarginWidth + doorWidth, doorHeight ), CGConsts::PI ) );
	facesN.Get( 8 )->SetShadow( true );
	facesN.Get( 9 )->SetShadow( true );
	geometryN.SetFaces( facesN );

	CGeometry geometryE;
	geometryE.AddVertex( ex1 );
	geometryE.AddVertex( geometry.Vertex( 1 ) );
	geometryE.AddVertex( geometry.Vertex( 2 ) );
	geometryE.AddVertex( in1 );
	geometryE.AddVertex( ex3 );
	geometryE.AddVertex( geometry.Vertex( 5 ) );
	geometryE.AddVertex( geometry.Vertex( 6 ) );
	geometryE.AddVertex( in3 );
	FacesList facesE = CFacesBuilder::BuildBoxoidFaces( geometryE,
                                                      CTextureProperties( exTexture, scale , CGPoint2( vMarginWidth + doorWidth, CGConsts::ZERO ) ),
                                                      CTextureProperties( NULL ), 
                                                      CTextureProperties( inTexture, scale ), 
                                                      CTextureProperties( inTexture, scale, CGPoint2( vMarginWidth, CGConsts::ZERO ) ),
                                                      CTextureProperties( NULL ),
                                                      CTextureProperties( NULL ) );
	facesE.Get( 10 )->SetShadow( true );
	facesE.Get( 11 )->SetShadow( true );
	geometryE.SetFaces( facesE );

	doorGeometry.Append( geometryW );
	doorGeometry.Append( geometryN );
	doorGeometry.Append( geometryE );

	return (doorGeometry);
}

//-----------------------------------------------------------------------------

GeometriesList CGeometriesBuilder::BuildWindowGeometry( const CGeometry& geometry,
                                                        Float windowWidth, Float windowHeight,
                                                        Float refHeight,
                                                        const CTexture* exTexture, 
                                                        const CTexture* inTexture )
{
	assert( geometry.VerticesCount() == 8 );
	assert( geometry.Vertex( 0 ).GetZ() == CGConsts::ZERO );

	GeometriesList windowGeometry;

	// first calculate the new vertices required by the window hole
	CGVector3 exVec( geometry.Vertex( 1 ), geometry.Vertex( 0 ) );
	CGVector3 inVec( geometry.Vertex( 2 ), geometry.Vertex( 3 ) );
	CGVector3 wallVec( geometry.Vertex( 1 ), geometry.Vertex( 2 ) );

	Float width         = exVec.Length();
	Float vMarginWidth  = CGConsts::HALF * (width - windowWidth);
	Float hMarginHeight = CGConsts::ONE;
	Float wallWidth     = wallVec.Length();

	assert( refHeight > CGConsts::ZERO );
	Float scale = CGConsts::ONE / refHeight;

	// extern
	exVec.Normalize();
	exVec *= vMarginWidth;
	CGPoint3 ex0 = exVec.Head( geometry.Vertex( 0 ) );
	CGPoint3 ex2 = exVec.Head( geometry.Vertex( 0 ) );
	ex0.SetZ( hMarginHeight );
	ex2.SetZ( hMarginHeight + windowHeight);

	exVec.Normalize();
	exVec *= (vMarginWidth + windowWidth);
	CGPoint3 ex1 = exVec.Head( geometry.Vertex( 0 ) );
	CGPoint3 ex3 = exVec.Head( geometry.Vertex( 0 ) );
	ex1.SetZ( hMarginHeight );
	ex3.SetZ( hMarginHeight + windowHeight );

	// intern
	inVec.Normalize();
	inVec *= vMarginWidth;
	CGPoint3 in0 = inVec.Head( geometry.Vertex( 3 ) );
	CGPoint3 in2 = inVec.Head( geometry.Vertex( 3 ) );
	in0.SetZ( hMarginHeight );
	in2.SetZ( hMarginHeight + windowHeight );

	inVec.Normalize();
	inVec *= (vMarginWidth + windowWidth);
	CGPoint3 in1 = inVec.Head( geometry.Vertex( 3 ) );
	CGPoint3 in3 = inVec.Head( geometry.Vertex( 3 ) );
	in1.SetZ( hMarginHeight );
	in3.SetZ( hMarginHeight + windowHeight );

	// now sets the four geometries
	CGeometry geometryS;
	geometryS.AddVertex( geometry.Vertex( 0 ) );
	geometryS.AddVertex( geometry.Vertex( 1 ) );
	geometryS.AddVertex( geometry.Vertex( 2 ) );
	geometryS.AddVertex( geometry.Vertex( 3 ) );
	geometryS.AddVertex( ex0 );
	geometryS.AddVertex( ex1 );
	geometryS.AddVertex( in1 );
	geometryS.AddVertex( in0 );
	FacesList facesS = CFacesBuilder::BuildBoxoidFaces( geometryS, 
                                                      CTextureProperties( exTexture, scale ),
                                                      CTextureProperties( NULL ), 
                                                      CTextureProperties( inTexture, scale ),
                                                      CTextureProperties( NULL ), 
                                                      CTextureProperties( inTexture, scale, CGPoint2( vMarginWidth + windowWidth, hMarginHeight + wallWidth ), CGConsts::PI ),
                                                      CTextureProperties( NULL ) );
	facesS.Get( 10 )->SetShadow( true );
	facesS.Get( 11 )->SetShadow( true );
	geometryS.SetFaces( facesS );

	CGeometry geometryW;
	geometryW.AddVertex( geometry.Vertex( 0 ) );
	geometryW.AddVertex( ex0 );
	geometryW.AddVertex( in0 );
	geometryW.AddVertex( geometry.Vertex( 3 ) );
	geometryW.AddVertex( geometry.Vertex( 4 ) );
	geometryW.AddVertex( ex2 );
	geometryW.AddVertex( in2 );
	geometryW.AddVertex( geometry.Vertex( 7 ) );
	geometryW.SetFaces( CFacesBuilder::BuildBoxoidFaces( geometryW,
                                                       CTextureProperties( exTexture, scale, CGPoint2(), atan( hMarginHeight / vMarginWidth ) ),
                                                       CTextureProperties( inTexture, scale, CGPoint2( vMarginWidth + windowWidth - wallWidth, hMarginHeight ) ), 
                                                       CTextureProperties( inTexture, scale, CGPoint2( vMarginWidth + windowWidth, hMarginHeight ), -atan( hMarginHeight / vMarginWidth ) ),
                                                       CTextureProperties( NULL ), 
                                                       CTextureProperties( NULL ),
                                                       CTextureProperties( NULL ) ) );

	CGeometry geometryN;
	geometryN.AddVertex( ex2 );
	geometryN.AddVertex( ex3 );
	geometryN.AddVertex( in3 );
	geometryN.AddVertex( in2 );
	geometryN.AddVertex( geometry.Vertex( 4 ) );
	geometryN.AddVertex( geometry.Vertex( 5 ) );
	geometryN.AddVertex( geometry.Vertex( 6 ) );
	geometryN.AddVertex( geometry.Vertex( 7 ) );
	FacesList facesN = CFacesBuilder::BuildBoxoidFaces( geometryN, 
                                                      CTextureProperties( exTexture, scale, CGPoint2( vMarginWidth, hMarginHeight + windowHeight ) ),
                                                      CTextureProperties( NULL ), 
                                                      CTextureProperties( inTexture, scale, CGPoint2( vMarginWidth, hMarginHeight + windowHeight ) ), 
                                                      CTextureProperties( NULL ),
                                                      CTextureProperties( NULL ),
                                                      CTextureProperties( inTexture, scale, CGPoint2( vMarginWidth + windowWidth, hMarginHeight + windowHeight ), CGConsts::PI ) );
	facesN.Get( 8 )->SetShadow( true );
	facesN.Get( 9 )->SetShadow( true );
	geometryN.SetFaces( facesN );

	CGeometry geometryE;
	geometryE.AddVertex( ex1 );
	geometryE.AddVertex( geometry.Vertex( 1 ) );
	geometryE.AddVertex( geometry.Vertex( 2 ) );
	geometryE.AddVertex( in1 );
	geometryE.AddVertex( ex3 );
	geometryE.AddVertex( geometry.Vertex( 5 ) );
	geometryE.AddVertex( geometry.Vertex( 6 ) );
	geometryE.AddVertex( in3 );
	geometryE.SetFaces( CFacesBuilder::BuildBoxoidFaces( geometryE,
                                                       CTextureProperties( exTexture, scale, CGPoint2( vMarginWidth + windowWidth, hMarginHeight ), -atan( hMarginHeight / vMarginWidth ) ),
                                                       CTextureProperties( NULL ), 
                                                       CTextureProperties( inTexture, scale, CGPoint2(), atan( hMarginHeight / vMarginWidth ) ), 
                                                       CTextureProperties( inTexture, scale, CGPoint2( vMarginWidth, hMarginHeight ) ),
                                                       CTextureProperties( NULL ),
                                                       CTextureProperties( NULL ) ) );

	windowGeometry.Append( geometryS );
	windowGeometry.Append( geometryW );
	windowGeometry.Append( geometryN );
	windowGeometry.Append( geometryE );

	return (windowGeometry);
}

//-----------------------------------------------------------------------------
