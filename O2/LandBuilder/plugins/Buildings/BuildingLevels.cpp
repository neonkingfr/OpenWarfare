//-----------------------------------------------------------------------------
// File: BuildingLevels.cpp
//
// Desc: classes related to building levels
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008/2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "BuildingLevels.h"

//-----------------------------------------------------------------------------

CBuildingLevel::CBuildingLevel( const CBuildingType* buildingType,
                                const EBuildingLevelType& type,
                                const CGPolygon2* footprint, Float height, 
                                Float texReferenceLength, 
                                bool bottomFacesVisible, bool topFacesVisible,
                                ofstream* logFile )
: m_buildingType( buildingType )
, m_type( type )
, m_height( height )
, m_texReferenceLength( texReferenceLength )
, m_bottomFacesVisible( bottomFacesVisible )
, m_topFacesVisible( topFacesVisible )
, m_solidified( false )
, m_logFile( logFile )
{
	assert( buildingType );
	if ( footprint->GetType() == CGPolygon2::PolyType_HOLED )
	{
		m_footprint = new CGHoledPolygon2( *reinterpret_cast< const CGHoledPolygon2* >( footprint ) );
	}
	else
	{
		m_footprint = new CGPolygon2( *footprint );
	}
}

//-----------------------------------------------------------------------------

CBuildingLevel::CBuildingLevel( const CBuildingLevel& other )
: m_buildingType( other.m_buildingType )
, m_type( other.m_type )
, m_height( other.m_height )
, m_texReferenceLength( other.m_texReferenceLength )
, m_geometries( other.m_geometries )
, m_bottomFacesVisible( other.m_bottomFacesVisible )
, m_topFacesVisible( other.m_topFacesVisible )
, m_solidified( other.m_solidified )
, m_logFile( other.m_logFile )
{
	if ( m_footprint->GetType() == CGPolygon2::PolyType_HOLED )
	{
		m_footprint = new CGHoledPolygon2( *reinterpret_cast< CGHoledPolygon2* >( other.m_footprint ) );
	}
	else
	{
		m_footprint = new CGPolygon2( *other.m_footprint );
	}
}

//-----------------------------------------------------------------------------

CBuildingLevel::~CBuildingLevel()
{
	if ( m_footprint ) { delete m_footprint; }
}

//-----------------------------------------------------------------------------

CBuildingLevel& CBuildingLevel::operator = ( const CBuildingLevel& other )
{
	m_buildingType       = other.m_buildingType;
	m_type               = other.m_type;
	*m_footprint         = *other.m_footprint;
	m_height             = other.m_height;
	m_texReferenceLength = other.m_texReferenceLength;
	m_geometries         = other.m_geometries;
	m_bottomFacesVisible = other.m_bottomFacesVisible;
	m_topFacesVisible    = other.m_topFacesVisible;
  m_solidified         = other.m_solidified;
	m_logFile            = other.m_logFile;
	return (*this);
}

//-----------------------------------------------------------------------------

const CBuildingType* CBuildingLevel::GetBuildingType() const
{
	return (m_buildingType);
}

//-----------------------------------------------------------------------------

const EBuildingLevelType& CBuildingLevel::GetType() const
{
	return (m_type);
}

//-----------------------------------------------------------------------------

const CGPolygon2& CBuildingLevel::GetFootprint() const
{
	return (*m_footprint);
}

//-----------------------------------------------------------------------------

Float CBuildingLevel::GetHeight() const
{
	return (m_height);
}

//-----------------------------------------------------------------------------

Float CBuildingLevel::GetTexReferenceLength() const
{
	return (m_texReferenceLength);
}

//-----------------------------------------------------------------------------

const GeometriesList& CBuildingLevel::GetGeometries() const
{
	return (m_geometries);
}

//-----------------------------------------------------------------------------

const ProxyHinstancesList& CBuildingLevel::GetProxies() const
{
	return (m_proxies);
}

//-----------------------------------------------------------------------------

bool CBuildingLevel::IsSolidified() const
{
  return (m_solidified);
}

//-----------------------------------------------------------------------------

bool CBuildingLevel::BuildSolidGeometry()
{
	m_geometries.Clear();
	m_footprint->Partition();

	Float footprintMinX = m_footprint->MinX();
	Float footprintMaxY = m_footprint->MaxY();

	vector< CGPolygon2 >& partitions = m_footprint->GetPartitions();
	for ( size_t i = 0, cntI = partitions.size(); i < cntI; ++i )
	{
		partitions[i].Triangulate();
	}

	// textures setup
	const CTexture* bottomTexture = NULL;
	const CTexture* topTexture    = NULL;
	const CTexture* wallTexture   = NULL;
	const CTexture* inWallTexture = NULL;
	const CTexture* windowTexture = NULL;
	const CTexture* doorTexture   = NULL;

	switch ( m_type )
	{
	case BLT_ROOF:
		{
			topTexture    = m_buildingType->GetTexture( BUILDING_ROOF_TOP_TEX );
			bottomTexture = m_buildingType->GetTexture( BUILDING_CEILING_TEX );
			wallTexture   = m_buildingType->GetTexture( BUILDING_ROOF_SLOPE_TEX );
			inWallTexture = m_buildingType->GetTexture( BUILDING_FLOOR_INWALL_TEX );
		}
		break;
	case BLT_FLOOR:
		{
			topTexture    = m_buildingType->GetTexture( BUILDING_PAVEMENT_TEX );
			bottomTexture = m_buildingType->GetTexture( BUILDING_CEILING_TEX );
			if ( reinterpret_cast< const CBuildingFloor* >( this )->GetFloorType() == BFT_SOLID_UNDERGROUND )
			{
				wallTexture   = m_buildingType->GetTexture( BUILDING_BASEMENT_TEX );
				windowTexture = m_buildingType->GetTexture( BUILDING_BASEMENT_TEX );
				doorTexture   = m_buildingType->GetTexture( BUILDING_BASEMENT_TEX );
			}
			else
			{
				wallTexture   = m_buildingType->GetTexture( BUILDING_FLOOR_EXWALL_TEX );
				windowTexture = m_buildingType->GetTexture( BUILDING_WINDOW_TEX );
				doorTexture   = m_buildingType->GetTexture( BUILDING_DOOR_TEX );
			}
		}
		break;
	case BLT_INTERFLOOR:
	default:
		{
			topTexture    = m_buildingType->GetTexture( BUILDING_PAVEMENT_TEX );
			bottomTexture = m_buildingType->GetTexture( BUILDING_CEILING_TEX );
			wallTexture   = m_buildingType->GetTexture( BUILDING_INTERFLOOR_WALL_TEX );
		}
		break;
	}

	assert( m_texReferenceLength > CGConsts::ZERO );
	Float texScale = CGConsts::ONE / m_texReferenceLength;

  // corrects the height and texture for flatted roofs
  if ( m_type == BLT_ROOF )
  {
    if ( reinterpret_cast< const CBuildingRoof* >( this )->IsFlatted() )
    {
      m_height    = CGConsts::HALF;
			wallTexture = m_buildingType->GetTexture( BUILDING_INTERFLOOR_WALL_TEX );
    }
  }

	for ( size_t i = 0, cntI = partitions.size(); i < cntI; ++i )
	{
		const CGPolygon2& partFootprint = partitions[i];
		CGeometry partGeometry;

		// VERTICES
		// bottom
		for ( size_t j = 0, cntJ = partFootprint.VerticesCount(); j < cntJ; ++j )
		{
			const CGPoint2& p = partFootprint.Vertex( j );
			partGeometry.AddVertex( CGPoint3( p.GetX(), p.GetY(), CGConsts::ZERO ) );
		}

		// top
		for ( size_t j = 0, cntJ = partFootprint.VerticesCount(); j < cntJ; ++j )
		{
			const CGPoint2& p = partFootprint.Vertex( j );
			partGeometry.AddVertex( CGPoint3( p.GetX(), p.GetY(), m_height ) );
		}

		// FACES
		// bottom
		FacesList bottomFaces = CFacesBuilder::BuildFacesFromHorizontalTriangulation( partGeometry,
                                                                                  partFootprint.GetTriangulation().GetTrianglesList(), 
                                                                                  CGConsts::ZERO,
                                                                                  footprintMinX,
                                                                                  footprintMaxY,
                                                                                  m_texReferenceLength,
                                                                                  bottomTexture,
                                                                                  true );
		for ( size_t j = 0, cntJ = bottomFaces.Size(); j < cntJ; ++j )
		{
			CFace face = *bottomFaces.Get( j );
			face.SetVisible( m_bottomFacesVisible ? true : false );
			partGeometry.AddFace( face );
		}

		// top
		FacesList topFaces = CFacesBuilder::BuildFacesFromHorizontalTriangulation( partGeometry,
                                                                               partFootprint.GetTriangulation().GetTrianglesList(), 
                                                                               m_height,
                                                                               footprintMinX,
                                                                               footprintMaxY,
                                                                               m_texReferenceLength,
                                                                               topTexture );
		for ( size_t j = 0, cntJ = topFaces.Size(); j < cntJ; ++j )
		{
			CFace face = *topFaces.Get( j );
			face.SetRoadWay( m_topFacesVisible ? true : false );
			face.SetVisible( m_topFacesVisible ? true : false );
			partGeometry.AddFace( face );
		}

		// facade and holes
		FacesList facadeFaces = CFacesBuilder::BuildRectClosedStripFaces( partGeometry, CTextureProperties( wallTexture, texScale ) );
		for ( size_t j = 0, cntJ = facadeFaces.Size(); j < cntJ; j += 2 )
		{
			CFace* face1 = facadeFaces.Get( j );
			CFace* face2 = facadeFaces.Get( j + 1 );
			const CGPoint3& p1 = partGeometry.Vertex( face1->GetIndex( 0 ) );
			const CGPoint3& p2 = partGeometry.Vertex( face1->GetIndex( 1 ) );
			CGPoint2 p1plan( p1.GetX(), p1.GetY() );
			CGPoint2 p2plan( p2.GetX(), p2.GetY() );
			CGSegment2 segment12( p1plan, p2plan );

			if ( m_footprint->HasAsEdge( segment12 ) )
			{
				// window and door textures
				if ( m_type == BLT_FLOOR )
				{
					if ( m_buildingType->UsesDoors() || m_buildingType->UsesWindows() )
					{
						Float d12 = p1.Distance( p2 );
						if ( abs( d12 - m_texReferenceLength ) <= TOLERANCE_MM )
						{
							if ( m_buildingType->UsesDoors() )
							{
								if ( reinterpret_cast< const CBuildingFloor* >( this )->GetFloorType() == BFT_SOLID_GROUND )
								{
                  // 33% probability to have a door
									if ( rand() < RAND_MAX * static_cast< Float >( 0.33 ) )
									{
										face1->SetTexture( doorTexture );
										face2->SetTexture( doorTexture );
									}
									else if ( m_buildingType->UsesWindows() )
									{
										face1->SetTexture( windowTexture );
										face2->SetTexture( windowTexture );
									}
								}
								else if ( m_buildingType->UsesWindows() )
								{
									face1->SetTexture( windowTexture );
									face2->SetTexture( windowTexture );
								}
							}
							else if ( m_buildingType->UsesWindows() )
							{
								face1->SetTexture( windowTexture );
								face2->SetTexture( windowTexture );
							}
						}
					}
				}
			}
			else if ( m_footprint->GetType() == CGPolygon2::PolyType_HOLED )
			{
				const vector< CGPolygon2 >& holes = (reinterpret_cast< CGHoledPolygon2* >( m_footprint ))->GetHoles();
				bool found = false;
				for ( size_t k = 0, cntK = holes.size(); k < cntK; ++k )
				{
					const CGPolygon2& hole = holes[k];
					if ( hole.HasAsEdge( segment12 ) || hole.HasAsReversedEdge( segment12 ) )
					{
						found = true;
				    if ( m_type == BLT_ROOF )
				    {
					    face1->SetTexture( inWallTexture );
					    face2->SetTexture( inWallTexture );
            }
						break;
					}
				}
				if ( !found )
				{
					// internal faces
					face1->SetTexture( NULL );
					face2->SetTexture( NULL );
					face1->SetVisible( false );
					face2->SetVisible( false );
					face1->SetShadow( false );
					face2->SetShadow( false );
				}
			}
			else
			{
				// internal faces
				face1->SetTexture( NULL );
				face2->SetTexture( NULL );
				face1->SetVisible( false );
				face2->SetVisible( false );
				face1->SetShadow( false );
				face2->SetShadow( false );
			}

			partGeometry.AddFace( *face1 );
			partGeometry.AddFace( *face2 );
		}

		m_geometries.Append( partGeometry );
	}

	return (true);
}

//-----------------------------------------------------------------------------

bool CBuildingLevel::BuildEnterableGeometry()
{
	assert( m_type == BLT_FLOOR );

	m_geometries.Clear();

	CGPolygon2 offsetFootprint = m_footprint->Offset( -m_buildingType->GetWallsWidth(), TOLERANCE_CM );
	if ( offsetFootprint.VerticesCount() != m_footprint->VerticesCount() ) { return (false); }

	// textures setup
	const CTexture* exTexture = m_buildingType->GetTexture( BUILDING_FLOOR_EXWALL_TEX );
	const CTexture* inTexture = m_buildingType->GetTexture( BUILDING_FLOOR_INWALL_TEX );

	assert( m_texReferenceLength > CGConsts::ZERO );
	Float texScale = CGConsts::ONE / m_texReferenceLength;

	const CProxy* windowProxy = m_buildingType->GetProxy( BUILDING_WINDOW_PROXY );
	const CProxy* doorProxy   = m_buildingType->GetProxy( BUILDING_DOOR_PROXY );

	Float doorHeight   = m_buildingType->GetDoorsHeight();
	Float doorWidth    = m_buildingType->GetDoorsWidth();
	Float windowHeight = m_buildingType->GetWindowsHeight();
	Float windowWidth  = m_buildingType->GetWindowsWidth();

	for ( size_t i = 0, cntI = m_footprint->VerticesCount(), j = cntI - 1; i < cntI; j = i++ )
	{
		GeometriesList geometries;
		CGeometry geometry;

		// vertices
		const CGPoint2& exP1 = m_footprint->Vertex( j );
		const CGPoint2& exP2 = m_footprint->Vertex( i );
		const CGPoint2& inP1 = offsetFootprint.Vertex( j );
		const CGPoint2& inP2 = offsetFootprint.Vertex( i );
		geometry.AddVertex( CGPoint3( exP1.GetX(), exP1.GetY(), CGConsts::ZERO ) );
		geometry.AddVertex( CGPoint3( exP2.GetX(), exP2.GetY(), CGConsts::ZERO ) );
		geometry.AddVertex( CGPoint3( inP2.GetX(), inP2.GetY(), CGConsts::ZERO ) );
		geometry.AddVertex( CGPoint3( inP1.GetX(), inP1.GetY(), CGConsts::ZERO ) );
		geometry.AddVertex( CGPoint3( exP1.GetX(), exP1.GetY(), m_height ) );
		geometry.AddVertex( CGPoint3( exP2.GetX(), exP2.GetY(), m_height ) );
		geometry.AddVertex( CGPoint3( inP2.GetX(), inP2.GetY(), m_height ) );
		geometry.AddVertex( CGPoint3( inP1.GetX(), inP1.GetY(), m_height ) );

		// faces
		geometries = CGeometriesBuilder::BuildInternalWallGeometry( geometry, m_texReferenceLength, 
                                                                exTexture, inTexture );

		// windows and doors
		if ( m_buildingType->UsesDoors() || m_buildingType->UsesWindows() )
		{
			if ( abs( exP1.Distance( exP2 ) - m_texReferenceLength ) <= TOLERANCE_MM )
			{
				if ( m_buildingType->UsesDoors() )
				{
					if ( reinterpret_cast< const CBuildingFloor* >( this )->GetFloorType() == BFT_ENTERABLE_GROUND )
					{
						if ( rand() < RAND_MAX * static_cast< Float >( 0.33 ) )
						{
							geometries = CGeometriesBuilder::BuildDoorGeometry( geometry, 
                                                                  doorWidth, 
                                                                  doorHeight,
                                                                  m_texReferenceLength,
                                                                  exTexture, 
                                                                  inTexture );
							if ( doorProxy )
							{
								CGVector2 exVec12( exP2, exP1 );
								CGVector2 inVec12( inP2, inP1 );
								exVec12 *= CGConsts::HALF;
								inVec12 *= CGConsts::HALF;
								CGPoint2 exMidPoint = exVec12.Head( exP1 );
								CGPoint2 inMidPoint = inVec12.Head( inP1 );
								CGVector2 vec( inMidPoint, exMidPoint );
								vec *= CGConsts::HALF;
								CGPoint2 midPoint = vec.Head( exMidPoint );
								CGPoint3 position( midPoint.GetX(), midPoint.GetY(), CGConsts::HALF * doorHeight );
								vec.Normalize();
								CGVector3 axisX( vec.GetY(), -vec.GetX(), CGConsts::ZERO );
								CGVector3 axisY( vec.GetX(), vec.GetY(), CGConsts::ZERO );
								CGVector3 axisZ( CGConsts::ZERO, CGConsts::ZERO, CGConsts::ONE );
								m_proxies.Append( CProxyHinstance( doorProxy, position, axisX, axisY, axisZ ) );
							}
						}
						else if ( m_buildingType->UsesWindows() )
						{
							geometries = CGeometriesBuilder::BuildWindowGeometry( geometry, 
                                                                    windowWidth,
                                                                    windowHeight,
                                                                    m_texReferenceLength,
                                                                    exTexture, 
                                                                    inTexture );
							if ( windowProxy )
							{
								CGVector2 exVec12( exP2, exP1 );
								CGVector2 inVec12( inP2, inP1 );
								exVec12 *= CGConsts::HALF;
								inVec12 *= CGConsts::HALF;
								CGPoint2 exMidPoint = exVec12.Head( exP1 );
								CGPoint2 inMidPoint = inVec12.Head( inP1 );
								CGVector2 vec( inMidPoint, exMidPoint );
								vec *= CGConsts::HALF;
								CGPoint2 midPoint = vec.Head( exMidPoint );
								CGPoint3 position( midPoint.GetX(), midPoint.GetY(), CGConsts::ONE + CGConsts::HALF * windowHeight );
								vec.Normalize();
								CGVector3 axisX( vec.GetY(), -vec.GetX(), CGConsts::ZERO );
								CGVector3 axisY( vec.GetX(), vec.GetY(), CGConsts::ZERO );
								CGVector3 axisZ( CGConsts::ZERO, CGConsts::ZERO, CGConsts::ONE );
								m_proxies.Append( CProxyHinstance( windowProxy, position, axisX, axisY, axisZ ) );
							}
						}
					}
					else if ( m_buildingType->UsesWindows() )
					{
						geometries = CGeometriesBuilder::BuildWindowGeometry( geometry, 
                                                                  windowWidth,
                                                                  windowHeight,
                                                                  m_texReferenceLength,
                                                                  exTexture, 
                                                                  inTexture );
						if ( windowProxy )
						{
							CGVector2 exVec12( exP2, exP1 );
							CGVector2 inVec12( inP2, inP1 );
							exVec12 *= CGConsts::HALF;
							inVec12 *= CGConsts::HALF;
							CGPoint2 exMidPoint = exVec12.Head( exP1 );
							CGPoint2 inMidPoint = inVec12.Head( inP1 );
							CGVector2 vec( inMidPoint, exMidPoint );
							vec *= CGConsts::HALF;
							CGPoint2 midPoint = vec.Head( exMidPoint );
							CGPoint3 position( midPoint.GetX(), midPoint.GetY(), CGConsts::ONE + CGConsts::HALF * windowHeight );
							vec.Normalize();
							CGVector3 axisX( vec.GetY(), -vec.GetX(), CGConsts::ZERO );
							CGVector3 axisY( vec.GetX(), vec.GetY(), CGConsts::ZERO );
							CGVector3 axisZ( CGConsts::ZERO, CGConsts::ZERO, CGConsts::ONE );
							m_proxies.Append( CProxyHinstance( windowProxy, position, axisX, axisY, axisZ ) );
						}
					}
				}
				else if ( m_buildingType->UsesWindows() )
				{
					geometries = CGeometriesBuilder::BuildWindowGeometry( geometry, 
                                                                windowWidth,
                                                                windowHeight,
                                                                m_texReferenceLength,
                                                                exTexture, 
                                                                inTexture );
					if ( windowProxy )
					{
						CGVector2 exVec12( exP2, exP1 );
						CGVector2 inVec12( inP2, inP1 );
						exVec12 *= CGConsts::HALF;
						inVec12 *= CGConsts::HALF;
						CGPoint2 exMidPoint = exVec12.Head( exP1 );
						CGPoint2 inMidPoint = inVec12.Head( inP1 );
						CGVector2 vec( inMidPoint, exMidPoint );
						vec *= CGConsts::HALF;
						CGPoint2 midPoint = vec.Head( exMidPoint );
						CGPoint3 position( midPoint.GetX(), midPoint.GetY(), CGConsts::ONE + CGConsts::HALF * windowHeight );
						vec.Normalize();
						CGVector3 axisX( vec.GetY(), -vec.GetX(), CGConsts::ZERO );
						CGVector3 axisY( vec.GetX(), vec.GetY(), CGConsts::ZERO );
						CGVector3 axisZ( CGConsts::ZERO, CGConsts::ZERO, CGConsts::ONE );
						m_proxies.Append( CProxyHinstance( windowProxy, position, axisX, axisY, axisZ ) );
					}
				}
			}
		}

		for ( size_t k = 0, cntK = geometries.Size(); k < cntK; ++k )
		{
			m_geometries.Append( *geometries.Get( k ) );
		}
	}

	// stair
	CBuildingFloor* thisAsFloor = reinterpret_cast< CBuildingFloor* >( this );
	if ( thisAsFloor->GetStair() )
	{
		GeometriesList stairGeometries;
		if ( thisAsFloor->GetFloorType() == BFT_ENTERABLE_GROUND )
		{
			stairGeometries = thisAsFloor->GetStair()->GroundFloorGeometries();
		}
		else
		{
			stairGeometries = thisAsFloor->GetStair()->GenericFloorGeometries();
		}

		for ( size_t k = 0, cntK = stairGeometries.Size(); k < cntK; ++k )
		{
			m_geometries.Append( *stairGeometries.Get( k ) );
		}
	}

	return (true);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CBuildingFloor::CBuildingFloor( const CBuildingType* buildingType,
                                const EBuildingFloorsType& floorType, 
                                const CGPolygon2* footprint, Float height,
                                Float texReferenceLength, 
                                bool bottomFacesVisible, bool topFacesVisible,
                                ofstream* logFile, const CStair* stair )
: CBuildingLevel( buildingType, BLT_FLOOR, footprint, height, texReferenceLength, 
                  bottomFacesVisible, topFacesVisible, logFile )
, m_floorType( floorType )
, m_stair( NULL )
{
	if ( stair ) { m_stair = new CStair( *stair ); }
}

//-----------------------------------------------------------------------------

CBuildingFloor::CBuildingFloor( const CBuildingFloor& other )
: CBuildingLevel( other.m_buildingType, other.m_type, other.m_footprint,
                  other.m_height, other.m_texReferenceLength, 
                  other.m_bottomFacesVisible, 
                  other.m_topFacesVisible, other.m_logFile )
, m_floorType( other.m_floorType )
, m_stair( NULL )
{
	if ( other.m_stair ) { m_stair = new CStair( *other.m_stair ); }
}

//-----------------------------------------------------------------------------

CBuildingFloor::~CBuildingFloor()
{
	if ( m_stair ) { delete m_stair; }
}

//-----------------------------------------------------------------------------

const EBuildingFloorsType& CBuildingFloor::GetFloorType() const
{
	return (m_floorType);
}

//-----------------------------------------------------------------------------

const CStair* CBuildingFloor::GetStair() const
{
	return (m_stair);
}

//-----------------------------------------------------------------------------

CBuildingFloor& CBuildingFloor::operator = ( const CBuildingFloor& other )
{
	CBuildingLevel::operator = ( other );
	m_floorType = other.m_floorType;
	return (*this);
}

//-----------------------------------------------------------------------------

bool CBuildingFloor::BuildGeometry()
{
	size_t insertCount = 0;

	if ( m_floorType == BFT_SOLID        || 
       m_floorType == BFT_ENTERABLE    || 
       m_floorType == BFT_SOLID_GROUND || 
       m_floorType == BFT_ENTERABLE_GROUND )
	{
		if ( m_buildingType->UsesDoors() && !m_buildingType->UsesWindows() )
		{
			insertCount = AddDoorsSectionsOnly( static_cast< Float >( 1.05 ) * m_buildingType->GetWallsWidth() );
		}
		else if ( m_buildingType->UsesWindows() )
		{
			insertCount = AddWindowsAndDoorsSections( static_cast< Float >( 1.05 ) * m_buildingType->GetWallsWidth() );
		}
	}

	if ( insertCount == 0 )
	{
		if ( m_floorType == BFT_ENTERABLE ) 
		{ 
			if ( m_logFile ) { (*m_logFile) << "Unable to add windows or doors. Switching to solid geometry." << endl; }
			m_floorType  = BFT_SOLID; 
      m_solidified = true;
		} 
		else if ( m_floorType == BFT_ENTERABLE_GROUND ) 
		{ 
			if ( m_logFile ) { (*m_logFile) << "Unable to add windows or doors. Switching to solid geometry." << endl; }
			m_floorType  = BFT_SOLID_GROUND; 
      m_solidified = true;
		}
	}

	switch ( m_floorType )
	{
	case BFT_ENTERABLE_GROUND:
		{
			if ( !BuildEnterableGeometry() )
			{
				if ( m_logFile ) { (*m_logFile) << "Unable to generate enterable geometry. Switching to solid geometry." << endl; }
				m_floorType  = BFT_SOLID_GROUND;
        m_solidified = true;
				BuildSolidGeometry();
			}
		}
		break;
	case BFT_ENTERABLE:
		{
			if ( !BuildEnterableGeometry() )
			{
				if ( m_logFile ) { (*m_logFile) << "Unable to generate enterable geometry. Switching to solid geometry." << endl; }
				m_floorType  = BFT_SOLID;
        m_solidified = true;
				BuildSolidGeometry();
			}
		}
		break;
	case BFT_SOLID_UNDERGROUND:
	case BFT_SOLID_GROUND:
	case BFT_SOLID:
	default:
		{
			BuildSolidGeometry();
		}
		break;
	}

	return (true);
}

//-----------------------------------------------------------------------------

size_t CBuildingFloor::AddWindowsAndDoorsSections( Float margin )
{
	return (m_footprint->InsertConstantLengthSections( -1, m_height, margin ));
}

//-----------------------------------------------------------------------------

size_t CBuildingFloor::AddDoorsSectionsOnly( Float margin )
{
	return (m_footprint->InsertConstantLengthSections( 1, m_height, margin ));
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CBuildingInterFloor::CBuildingInterFloor( const CBuildingType* buildingType,
                                          const CGPolygon2* footprint, Float height,
                                          Float texReferenceLength,
                                          const EBuildingInterFloorsType& interFloorType,
                                          bool bottomFacesVisible, bool topFacesVisible,
                                          ofstream* logFile )
: CBuildingLevel( buildingType, BLT_INTERFLOOR, footprint, height, texReferenceLength,
                  bottomFacesVisible, topFacesVisible, logFile )
, m_interFloorType( interFloorType )
{
}

//-----------------------------------------------------------------------------

CBuildingInterFloor::CBuildingInterFloor( const CBuildingInterFloor& other )
: CBuildingLevel( other.m_buildingType, other.m_type, other.m_footprint, other.m_height,
                  other.m_texReferenceLength, other.m_bottomFacesVisible, 
                  other.m_topFacesVisible, other.m_logFile )
, m_interFloorType( other.m_interFloorType )
{
}

//-----------------------------------------------------------------------------

CBuildingInterFloor::~CBuildingInterFloor()
{
}

//-----------------------------------------------------------------------------

const EBuildingInterFloorsType& CBuildingInterFloor::GetInterFloorType() const
{
	return (m_interFloorType);
}

//-----------------------------------------------------------------------------

CBuildingInterFloor& CBuildingInterFloor::operator = ( const CBuildingInterFloor& other )
{
	CBuildingLevel::operator = ( other );
	m_interFloorType = other.m_interFloorType;
	return (*this);
}

//-----------------------------------------------------------------------------

bool CBuildingInterFloor::BuildGeometry()
{
	switch ( m_interFloorType )
	{
	case BIFT_SOLID:
	default:
		{
			BuildSolidGeometry();
		}
		break;
	}

	return (true);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CBuildingRoof::CBuildingRoof( const CBuildingType* buildingType,
                              const CGPolygon2* footprint, Float height,
                              Float texReferenceLength,
                              bool bottomFacesVisible, ofstream* logFile )
: CBuildingLevel( buildingType, BLT_ROOF, footprint, height, texReferenceLength, 
                  bottomFacesVisible, true, logFile )
, m_flatted( false )
{
	assert( m_buildingType );
	m_roofType = m_buildingType->GetRoofType();
}

//-----------------------------------------------------------------------------

CBuildingRoof::CBuildingRoof( const CBuildingRoof& other )
: CBuildingLevel( other.m_buildingType, other.m_type, other.m_footprint, other.m_height,
                  other.m_texReferenceLength, other.m_bottomFacesVisible, 
                  other.m_topFacesVisible, other.m_logFile  )
, m_roofType( other.m_roofType )
, m_flatted( other.m_flatted )
{
}

//-----------------------------------------------------------------------------

CBuildingRoof::~CBuildingRoof()
{
}

//-----------------------------------------------------------------------------

const EBuildingRoofsType& CBuildingRoof::GetRoofType() const
{
	return (m_roofType);
}

//-----------------------------------------------------------------------------

bool CBuildingRoof::IsFlatted() const
{
	return (m_flatted);
}

//-----------------------------------------------------------------------------

CBuildingRoof& CBuildingRoof::operator = ( const CBuildingRoof& other )
{
	CBuildingLevel::operator = ( other );
	m_roofType = other.m_roofType;
  m_flatted  = other.m_flatted;
	return (*this);
}

//-----------------------------------------------------------------------------

bool CBuildingRoof::BuildGeometry()
{
	switch ( m_roofType )
	{
	case BRT_SLOPED:
		{
			if ( !BuildSloped() )
			{
				if ( m_logFile ) { (*m_logFile) << "Unable to generate sloped roof. Switching to flat roof." << endl; }
				m_roofType = BRT_FLAT;
        m_flatted  = true;
  			BuildFlat();
			}
		}
		break;
	case BRT_FLAT_WALLED:
		{
			if ( !BuildFlatWalled() )
			{
				if ( m_logFile ) { (*m_logFile) << "Unable to generate flat walled roof. Switching to flat roof." << endl; }
				m_roofType = BRT_FLAT;
        m_flatted  = true;
  			BuildFlat();
			}
		}
		break;
	case BRT_HIP:
		{
      if ( !BuildHip( CGConsts::ZERO ) )
			{
				if ( m_logFile ) { (*m_logFile) << "Unable to generate hip roof. Switching to flat roof." << endl; }
				m_roofType = BRT_FLAT;
        m_flatted  = true;
  			BuildFlat();
			}
		}
		break;
	case BRT_HIP_OFFSET:
		{
			if ( !BuildHip( DEFAULT_ROOF_OFFSET ) )
			{
				if ( m_logFile ) { (*m_logFile) << "Unable to generate hip offset roof. Switching to flat roof." << endl; }
				m_roofType = BRT_FLAT;
        m_flatted  = true;
  			BuildFlat();
			}
		}
		break;
	case BRT_FLAT:
	default:
		{
			BuildFlat();
		}
		break;
	}

	return (true);
}

//-----------------------------------------------------------------------------

bool CBuildingRoof::BuildFlat()
{
  // build base
	BuildSolidGeometry();

	assert( m_texReferenceLength > CGConsts::ZERO );
	Float texScale = CGConsts::ONE / m_texReferenceLength;

	Float height = m_buildingType->GetRoofHeight();

  // build stair cover
  Float stairCoverHeight = static_cast< Float >( 3 );
  Float wallWidth        = m_buildingType->GetWallsWidth();

	// textures setup
	const CTexture* exTexture2  = m_buildingType->GetTexture( BUILDING_FLOOR_EXWALL_TEX );
	const CTexture* inTexture2  = m_buildingType->GetTexture( BUILDING_FLOOR_INWALL_TEX );
	const CTexture* topTexture2 = m_buildingType->GetTexture( BUILDING_ROOF_TOP_TEX );
	const CTexture* exTexture3  = m_buildingType->GetTexture( BUILDING_ROOF_SLOPE_TEX );

  if ( m_footprint->GetType() == CGPolygon2::PolyType_HOLED )
  {
    vector< CGPolygon2 >& holes = (reinterpret_cast< CGHoledPolygon2* >( m_footprint ))->GetHoles();
    if ( holes.size() > 0 )
    {
      CGPolygon2 stairHole = holes[0];
      CGPolygon2 offsetStairHole = stairHole.Offset( wallWidth, TOLERANCE_CM );

      CGVector2 edge01( offsetStairHole.Vertex( 1 ), offsetStairHole.Vertex( 0 ) );
      CGVector2 edge32( offsetStairHole.Vertex( 2 ), offsetStairHole.Vertex( 3 ) );
      Float newLength = edge01.Length() - wallWidth;
      edge01.Normalize();
      edge32.Normalize();
      edge01 *= newLength;
      edge32 *= newLength;

      CGPoint2& v1 = offsetStairHole.Vertex( 1 );
      v1 = edge01.Head( offsetStairHole.Vertex( 0 ) );
      CGPoint2& v2 = offsetStairHole.Vertex( 2 );
      v2 = edge32.Head( offsetStairHole.Vertex( 3 ) );

	    for ( size_t i = 0, cntI = stairHole.VerticesCount(), j = cntI - 1; i < cntI; j = i++ )
	    {
        if ( i != 2 )
        {
		      GeometriesList geometries;
		      CGeometry geometry;

		      // vertices
		      const CGPoint2& exP1 = offsetStairHole.Vertex( j );
		      const CGPoint2& exP2 = offsetStairHole.Vertex( i );
		      const CGPoint2& inP1 = stairHole.Vertex( j );
		      const CGPoint2& inP2 = stairHole.Vertex( i );
		      geometry.AddVertex( CGPoint3( exP1.GetX(), exP1.GetY(), height ) );
		      geometry.AddVertex( CGPoint3( exP2.GetX(), exP2.GetY(), height ) );
		      geometry.AddVertex( CGPoint3( inP2.GetX(), inP2.GetY(), height ) );
		      geometry.AddVertex( CGPoint3( inP1.GetX(), inP1.GetY(), height ) );
		      geometry.AddVertex( CGPoint3( exP1.GetX(), exP1.GetY(), stairCoverHeight - wallWidth ) );
		      geometry.AddVertex( CGPoint3( exP2.GetX(), exP2.GetY(), stairCoverHeight - wallWidth ) );
		      geometry.AddVertex( CGPoint3( inP2.GetX(), inP2.GetY(), stairCoverHeight - wallWidth ) );
		      geometry.AddVertex( CGPoint3( inP1.GetX(), inP1.GetY(), stairCoverHeight - wallWidth ) );

		      // faces
		      geometries = CGeometriesBuilder::BuildInternalWallGeometry( geometry, m_texReferenceLength, 
                                                                      exTexture2, inTexture2 );

          if ( i == 1 )
          {
            CFace& face1 = geometries.Get( 0 )->Face( 2 );
            face1.SetTexture( exTexture2 );
            face1.SetVisible( true );
            face1.SetShadow( true );
            CFace& face2 = geometries.Get( 0 )->Face( 3 );
            face2.SetTexture( exTexture2 );
            face2.SetVisible( true );
            face2.SetShadow( true );
		        CUvTriangle3 tri1( geometry.Vertex( 1 ), geometry.Vertex( 2 ), 
                               geometry.Vertex( 6 ) );
            tri1.CalculateUVs( CGConsts::ZERO, CGConsts::ZERO, texScale );
		        for ( size_t k = 0; k < 3; ++k )
		        {
			        face1.SetUV( k, tri1.GetUv( k ) );
		        }
		        CGVector2 vec01( tri1.GetUv( 1 ), tri1.GetUv( 0 ) );
		        CGVector2 vec02( tri1.GetUv( 2 ), tri1.GetUv( 0 ) );
		        Float correction = vec01.Direction() - vec02.Direction();
		        CUvTriangle3 tri2( geometry.Vertex( 1 ), geometry.Vertex( 6 ), 
                               geometry.Vertex( 5 ) );
        		tri2.CalculateUVs( CGConsts::ZERO, correction, texScale );
		        for ( size_t k = 0; k < 3; ++k )
		        {
			        face2.SetUV( k, tri2.GetUv( k ) );
		        }
          }
          else if ( i == 3 )
          {
            CFace& face1 = geometries.Get( 0 )->Face( 6 );
            face1.SetTexture( exTexture2 );
            face1.SetVisible( true );
            face1.SetShadow( true );
            CFace& face2 = geometries.Get( 0 )->Face( 7 );
            face2.SetTexture( exTexture2 );
            face2.SetVisible( true );
            face2.SetShadow( true );
		        CUvTriangle3 tri1( geometry.Vertex( 3 ), geometry.Vertex( 0 ), 
                               geometry.Vertex( 4 ) );
            tri1.CalculateUVs( CGConsts::ZERO, CGConsts::ZERO, texScale );
		        for ( size_t k = 0; k < 3; ++k )
		        {
			        face1.SetUV( k, tri1.GetUv( k ) );
		        }
		        CGVector2 vec01( tri1.GetUv( 1 ), tri1.GetUv( 0 ) );
		        CGVector2 vec02( tri1.GetUv( 2 ), tri1.GetUv( 0 ) );
		        Float correction = vec01.Direction() - vec02.Direction();
		        CUvTriangle3 tri2( geometry.Vertex( 3 ), geometry.Vertex( 4 ), 
                               geometry.Vertex( 7 ) );
        		tri2.CalculateUVs( CGConsts::ZERO, correction, texScale );
		        for ( size_t k = 0; k < 3; ++k )
		        {
			        face2.SetUV( k, tri2.GetUv( k ) );
		        }
          }

		      for ( size_t k = 0, cntK = geometries.Size(); k < cntK; ++k )
		      {
			      m_geometries.Append( *geometries.Get( k ) );
		      }
        }
	    }

      CGeometry geometry;
      for ( size_t i = 0, cntI = offsetStairHole.VerticesCount(); i < cntI; ++i )
      {
		      const CGPoint2& p = offsetStairHole.Vertex( i );
		      geometry.AddVertex( CGPoint3( p.GetX(), p.GetY(), stairCoverHeight - wallWidth ) );
      }
      for ( size_t i = 0, cntI = offsetStairHole.VerticesCount(); i < cntI; ++i )
      {
		      const CGPoint2& p = offsetStairHole.Vertex( i );
		      geometry.AddVertex( CGPoint3( p.GetX(), p.GetY(), stairCoverHeight ) );
      }

      FacesList facesW = CFacesBuilder::BuildBoxoidFaces( geometry,
                                                          CTextureProperties( exTexture3, texScale ),
                                                          CTextureProperties( exTexture3, texScale ),
                                                          CTextureProperties( exTexture3, texScale ),
                                                          CTextureProperties( exTexture3, texScale ),
                                                          CTextureProperties( topTexture2, texScale ),
                                                          CTextureProperties( inTexture2, texScale ) );
      geometry.SetFaces( facesW );
      m_geometries.Append( geometry );
    }
  }

	return (true);
}

//-----------------------------------------------------------------------------

bool CBuildingRoof::BuildFlatWalled()
{
  // build base
	BuildSolidGeometry();

  // build perimetral walls
	CGPolygon2 offsetFootprint = m_footprint->Offset( -m_buildingType->GetWallsWidth(), TOLERANCE_CM );
	if ( offsetFootprint.VerticesCount() != m_footprint->VerticesCount() ) { return (false); }

	// textures setup
	const CTexture* exTexture  = m_buildingType->GetTexture( BUILDING_ROOF_SLOPE_TEX );
	const CTexture* inTexture  = m_buildingType->GetTexture( BUILDING_ROOF_TOP_TEX );
	const CTexture* topTexture = m_buildingType->GetTexture( BUILDING_ROOF_SLOPE_TEX );

	assert( m_texReferenceLength > CGConsts::ZERO );
	Float texScale = CGConsts::ONE / m_texReferenceLength;

	Float height     = m_buildingType->GetRoofHeight();
	Float wallHeight = height + DEFAULT_ROOF_WALLS_HEIGHT;

	for ( size_t i = 0, cntI = m_footprint->VerticesCount(), j = cntI - 1; i < cntI; j = i++ )
	{
		GeometriesList geometries;
		CGeometry geometry;

		// vertices
		const CGPoint2& exP1 = m_footprint->Vertex( j );
		const CGPoint2& exP2 = m_footprint->Vertex( i );
		const CGPoint2& inP1 = offsetFootprint.Vertex( j );
		const CGPoint2& inP2 = offsetFootprint.Vertex( i );
		geometry.AddVertex( CGPoint3( exP1.GetX(), exP1.GetY(), height ) );
		geometry.AddVertex( CGPoint3( exP2.GetX(), exP2.GetY(), height ) );
		geometry.AddVertex( CGPoint3( inP2.GetX(), inP2.GetY(), height ) );
		geometry.AddVertex( CGPoint3( inP1.GetX(), inP1.GetY(), height ) );
		geometry.AddVertex( CGPoint3( exP1.GetX(), exP1.GetY(), wallHeight ) );
		geometry.AddVertex( CGPoint3( exP2.GetX(), exP2.GetY(), wallHeight ) );
		geometry.AddVertex( CGPoint3( inP2.GetX(), inP2.GetY(), wallHeight ) );
		geometry.AddVertex( CGPoint3( inP1.GetX(), inP1.GetY(), wallHeight ) );

		// faces
		geometries = CGeometriesBuilder::BuildWallGeometry( geometry, m_texReferenceLength, 
                                                        exTexture, inTexture, topTexture );

		for ( size_t k = 0, cntK = geometries.Size(); k < cntK; ++k )
		{
			m_geometries.Append( *geometries.Get( k ) );
		}
	}

  // build stair cover
  Float stairCoverHeight = static_cast< Float >( 3 );
  Float wallWidth        = m_buildingType->GetWallsWidth();

	// textures setup
	const CTexture* exTexture2  = m_buildingType->GetTexture( BUILDING_FLOOR_EXWALL_TEX );
	const CTexture* inTexture2  = m_buildingType->GetTexture( BUILDING_FLOOR_INWALL_TEX );
	const CTexture* topTexture2 = m_buildingType->GetTexture( BUILDING_ROOF_TOP_TEX );
	const CTexture* exTexture3  = m_buildingType->GetTexture( BUILDING_ROOF_SLOPE_TEX );

  if ( m_footprint->GetType() == CGPolygon2::PolyType_HOLED )
  {
    vector< CGPolygon2 >& holes = (reinterpret_cast< CGHoledPolygon2* >( m_footprint ))->GetHoles();
    if ( holes.size() > 0 )
    {
      CGPolygon2 stairHole = holes[0];
      CGPolygon2 offsetStairHole = stairHole.Offset( wallWidth, TOLERANCE_CM );

      CGVector2 edge01( offsetStairHole.Vertex( 1 ), offsetStairHole.Vertex( 0 ) );
      CGVector2 edge32( offsetStairHole.Vertex( 2 ), offsetStairHole.Vertex( 3 ) );
      Float newLength = edge01.Length() - wallWidth;
      edge01.Normalize();
      edge32.Normalize();
      edge01 *= newLength;
      edge32 *= newLength;

      CGPoint2& v1 = offsetStairHole.Vertex( 1 );
      v1 = edge01.Head( offsetStairHole.Vertex( 0 ) );
      CGPoint2& v2 = offsetStairHole.Vertex( 2 );
      v2 = edge32.Head( offsetStairHole.Vertex( 3 ) );

	    for ( size_t i = 0, cntI = stairHole.VerticesCount(), j = cntI - 1; i < cntI; j = i++ )
	    {
        if ( i != 2 )
        {
		      GeometriesList geometries;
		      CGeometry geometry;

		      // vertices
		      const CGPoint2& exP1 = offsetStairHole.Vertex( j );
		      const CGPoint2& exP2 = offsetStairHole.Vertex( i );
		      const CGPoint2& inP1 = stairHole.Vertex( j );
		      const CGPoint2& inP2 = stairHole.Vertex( i );
		      geometry.AddVertex( CGPoint3( exP1.GetX(), exP1.GetY(), height ) );
		      geometry.AddVertex( CGPoint3( exP2.GetX(), exP2.GetY(), height ) );
		      geometry.AddVertex( CGPoint3( inP2.GetX(), inP2.GetY(), height ) );
		      geometry.AddVertex( CGPoint3( inP1.GetX(), inP1.GetY(), height ) );
		      geometry.AddVertex( CGPoint3( exP1.GetX(), exP1.GetY(), stairCoverHeight - wallWidth ) );
		      geometry.AddVertex( CGPoint3( exP2.GetX(), exP2.GetY(), stairCoverHeight - wallWidth ) );
		      geometry.AddVertex( CGPoint3( inP2.GetX(), inP2.GetY(), stairCoverHeight - wallWidth ) );
		      geometry.AddVertex( CGPoint3( inP1.GetX(), inP1.GetY(), stairCoverHeight - wallWidth ) );

		      // faces
		      geometries = CGeometriesBuilder::BuildInternalWallGeometry( geometry, m_texReferenceLength, 
                                                                      exTexture2, inTexture2 );

          if ( i == 1 )
          {
            CFace& face1 = geometries.Get( 0 )->Face( 2 );
            face1.SetTexture( exTexture2 );
            face1.SetVisible( true );
            face1.SetShadow( true );
            CFace& face2 = geometries.Get( 0 )->Face( 3 );
            face2.SetTexture( exTexture2 );
            face2.SetVisible( true );
            face2.SetShadow( true );
		        CUvTriangle3 tri1( geometry.Vertex( 1 ), geometry.Vertex( 2 ), 
                               geometry.Vertex( 6 ) );
            tri1.CalculateUVs( CGConsts::ZERO, CGConsts::ZERO, texScale );
		        for ( size_t k = 0; k < 3; ++k )
		        {
			        face1.SetUV( k, tri1.GetUv( k ) );
		        }
		        CGVector2 vec01( tri1.GetUv( 1 ), tri1.GetUv( 0 ) );
		        CGVector2 vec02( tri1.GetUv( 2 ), tri1.GetUv( 0 ) );
		        Float correction = vec01.Direction() - vec02.Direction();
		        CUvTriangle3 tri2( geometry.Vertex( 1 ), geometry.Vertex( 6 ), 
                               geometry.Vertex( 5 ) );
        		tri2.CalculateUVs( CGConsts::ZERO, correction, texScale );
		        for ( size_t k = 0; k < 3; ++k )
		        {
			        face2.SetUV( k, tri2.GetUv( k ) );
		        }
          }
          else if ( i == 3 )
          {
            CFace& face1 = geometries.Get( 0 )->Face( 6 );
            face1.SetTexture( exTexture2 );
            face1.SetVisible( true );
            face1.SetShadow( true );
            CFace& face2 = geometries.Get( 0 )->Face( 7 );
            face2.SetTexture( exTexture2 );
            face2.SetVisible( true );
            face2.SetShadow( true );
		        CUvTriangle3 tri1( geometry.Vertex( 3 ), geometry.Vertex( 0 ), 
                               geometry.Vertex( 4 ) );
            tri1.CalculateUVs( CGConsts::ZERO, CGConsts::ZERO, texScale );
		        for ( size_t k = 0; k < 3; ++k )
		        {
			        face1.SetUV( k, tri1.GetUv( k ) );
		        }
		        CGVector2 vec01( tri1.GetUv( 1 ), tri1.GetUv( 0 ) );
		        CGVector2 vec02( tri1.GetUv( 2 ), tri1.GetUv( 0 ) );
		        Float correction = vec01.Direction() - vec02.Direction();
		        CUvTriangle3 tri2( geometry.Vertex( 3 ), geometry.Vertex( 4 ), 
                               geometry.Vertex( 7 ) );
        		tri2.CalculateUVs( CGConsts::ZERO, correction, texScale );
		        for ( size_t k = 0; k < 3; ++k )
		        {
			        face2.SetUV( k, tri2.GetUv( k ) );
		        }
          }

		      for ( size_t k = 0, cntK = geometries.Size(); k < cntK; ++k )
		      {
			      m_geometries.Append( *geometries.Get( k ) );
		      }
        }
	    }

      CGeometry geometry;
      for ( size_t i = 0, cntI = offsetStairHole.VerticesCount(); i < cntI; ++i )
      {
		      const CGPoint2& p = offsetStairHole.Vertex( i );
		      geometry.AddVertex( CGPoint3( p.GetX(), p.GetY(), stairCoverHeight - wallWidth ) );
      }
      for ( size_t i = 0, cntI = offsetStairHole.VerticesCount(); i < cntI; ++i )
      {
		      const CGPoint2& p = offsetStairHole.Vertex( i );
		      geometry.AddVertex( CGPoint3( p.GetX(), p.GetY(), stairCoverHeight ) );
      }

      FacesList facesW = CFacesBuilder::BuildBoxoidFaces( geometry,
                                                          CTextureProperties( exTexture3, texScale ),
                                                          CTextureProperties( exTexture3, texScale ),
                                                          CTextureProperties( exTexture3, texScale ),
                                                          CTextureProperties( exTexture3, texScale ),
                                                          CTextureProperties( topTexture2, texScale ),
                                                          CTextureProperties( inTexture2, texScale ) );
      geometry.SetFaces( facesW );
      m_geometries.Append( geometry );
    }
  }

	return (true);
}

//-----------------------------------------------------------------------------

bool CBuildingRoof::BuildSloped()
{
	m_geometries.Clear();

	CGPolygon2 offsetFootprint = m_footprint->Offset( -DEFAULT_ROOF_SLOPE_INSET, TOLERANCE_CM );
	if ( offsetFootprint.VerticesCount() != m_footprint->VerticesCount() ) { return (false); }

	Float footprintMinX       = m_footprint->MinX();
	Float footprintMaxY       = m_footprint->MaxY();
	Float offsetFootprintMinX = offsetFootprint.MinX();
	Float offsetFootprintMaxY = offsetFootprint.MaxY();

	m_footprint->Partition();

	vector< CGPolygon2 >& partitions = m_footprint->GetPartitions();

	for ( size_t i = 0, cntI = partitions.size(); i < cntI; ++i )
	{
		partitions[i].Triangulate();
	}

	// textures setup
	const CTexture* bottomTexture = m_buildingType->GetTexture( BUILDING_CEILING_TEX );
	const CTexture* topTexture    = m_buildingType->GetTexture( BUILDING_ROOF_TOP_TEX );
	const CTexture* slopeTexture  = m_buildingType->GetTexture( BUILDING_ROOF_SLOPE_TEX );

	assert( m_texReferenceLength > CGConsts::ZERO );
	Float texScale = CGConsts::ONE / m_texReferenceLength;

	for ( size_t i = 0, cntI = partitions.size(); i < cntI; ++i )
	{
		const CGPolygon2& partFootprint = partitions[i];
		CGeometry partGeometry;

		vector< CGTriangle2 > offsetTriangulation = partFootprint.GetTriangulation().GetTrianglesList();
		for ( size_t j = 0, cntJ = offsetTriangulation.size(); j < cntJ; ++j )
		{
			CGTriangle2& tri = offsetTriangulation[j];
			for ( size_t k = 0; k < 3; ++k )
			{
				tri.SetVertex( k, offsetFootprint.Vertex( m_footprint->FindVertexIndex( tri.Vertex( k ) ) ) );
			}
		}

		// VERTICES
		// bottom
		for ( size_t j = 0, cntJ = partFootprint.VerticesCount(); j < cntJ; ++j )
		{
			const CGPoint2& p = partFootprint.Vertex( j );
			partGeometry.AddVertex( CGPoint3( p.GetX(), p.GetY(), CGConsts::ZERO ) );
		}
		// top
		for ( size_t j = 0, cntJ = partFootprint.VerticesCount(); j < cntJ; ++j )
		{
			const CGPoint2& p = offsetFootprint.Vertex( m_footprint->FindVertexIndex( partFootprint.Vertex( j ) ) );
			partGeometry.AddVertex( CGPoint3( p.GetX(), p.GetY(), m_height ) );
		}

		// FACES
		// bottom
		FacesList bottomFaces = CFacesBuilder::BuildFacesFromHorizontalTriangulation( partGeometry,
                                                                                  partFootprint.GetTriangulation().GetTrianglesList(), 
                                                                                  CGConsts::ZERO,
                                                                                  footprintMinX,
                                                                                  footprintMaxY,
                                                                                  m_texReferenceLength,
                                                                                  bottomTexture,
                                                                                  true );
		for ( size_t j = 0, cntJ = bottomFaces.Size(); j < cntJ; ++j )
		{
			CFace face = *bottomFaces.Get( j );
			face.SetVisible( m_bottomFacesVisible ? true : false );
			partGeometry.AddFace( face );
		}

		// top
		FacesList topFaces = CFacesBuilder::BuildFacesFromHorizontalTriangulation( partGeometry,
                                                                               offsetTriangulation, 
                                                                               m_height,
                                                                               offsetFootprintMinX,
                                                                               offsetFootprintMaxY,
                                                                               m_texReferenceLength,
                                                                               topTexture );
		for ( size_t j = 0, cntJ = topFaces.Size(); j < cntJ; ++j )
		{
			CFace face = *topFaces.Get( j );
			face.SetRoadWay( true );
			partGeometry.AddFace( face );
		}

		// facade
		FacesList facadeFaces = CFacesBuilder::BuildQuadsClosedStripFaces( partGeometry, CTextureProperties( slopeTexture, texScale ) );
		for ( size_t j = 0, cntJ = facadeFaces.Size(); j < cntJ; j += 2 )
		{
			CFace* face1 = facadeFaces.Get( j );
			CFace* face2 = facadeFaces.Get( j + 1 );
			const CGPoint3& p1 = partGeometry.Vertex( face1->GetIndex( 0 ) );
			const CGPoint3& p2 = partGeometry.Vertex( face1->GetIndex( 1 ) );
			CGPoint2 p1plan( p1.GetX(), p1.GetY() );
			CGPoint2 p2plan( p2.GetX(), p2.GetY() );

			if ( !m_footprint->HasAsEdge( CGSegment2( p1plan, p2plan ) ) )
			{
				// internal faces
				face1->SetTexture( NULL );
				face2->SetTexture( NULL );
				face1->SetVisible( false );
				face2->SetVisible( false );
				face1->SetShadow( false );
				face2->SetShadow( false );
			}

			partGeometry.AddFace( *face1 );
			partGeometry.AddFace( *face2 );
		}

		m_geometries.Append( partGeometry );
	}

	return (true);
}

//-----------------------------------------------------------------------------

bool CBuildingRoof::BuildHip( Float offset )
{
	m_geometries.Clear();

	CGPolygon2 offsetFootprint = m_footprint->Offset( offset, TOLERANCE_CM );
	if ( offsetFootprint.VerticesCount() != m_footprint->VerticesCount() ) { return (false); }

	CGStraightSkeleton2 skeleton = offsetFootprint.StraightSkeleton();
	if ( skeleton.BonesCount() == 0 ) { return (false); }

	Float roofThickness = static_cast< Float >( 0.3 );

	Float height = m_buildingType->GetRoofHeight();
	assert( height > CGConsts::ZERO );

	// textures setup
	const CTexture* bottomTexture = m_buildingType->GetTexture( BUILDING_CEILING_TEX );
	const CTexture* topTexture    = m_buildingType->GetTexture( BUILDING_ROOF_SLOPE_TEX );
	const CTexture* borderTexture = NULL;
  if ( offset == CGConsts::ZERO )
  {
	  borderTexture = m_buildingType->GetTexture( BUILDING_FLOOR_EXWALL_TEX );
  }
  else
  {
	  borderTexture = m_buildingType->GetTexture( BUILDING_ROOF_SLOPE_TEX );
  }

	assert( m_texReferenceLength > CGConsts::ZERO );
	Float texScale = CGConsts::ONE / m_texReferenceLength;

	Float maxDist = CGConsts::ZERO;
	vector< CGPolygon2 > parts;
	for ( size_t i = 0, cntI = offsetFootprint.VerticesCount(); i < cntI; ++i )
	{
		const CGPoint2& p1 = offsetFootprint.Vertex( i );
		const CGPoint2& p2 = offsetFootprint.Vertex( i + 1 );

		CGPolygon2 part;
		part.AppendVertex( p1 );
		part.AppendVertex( p2 );
		CGLine2 edge( p1, p2 );

		vector< CGPoint2 > path = skeleton.PathConnecting( p2, p1 );

		if ( path.size() < 3 ) { return (false); }

		for ( size_t i = 1, cntI = path.size() - 1; i < cntI; ++i )
		{
			const CGPoint2& point = path[i];
			part.AppendVertex( point );
			Float dist = edge.Distance( point );
			maxDist = max( maxDist, dist );
		}

		parts.push_back( part );
	}

	assert( maxDist > CGConsts::ZERO );
	Float scaleFactor = (height + roofThickness) / maxDist;

	for ( size_t i = 0, cntI = parts.size(); i < cntI; ++i )
	{
		CGPolygon2& part = parts[i];
		CGLine2 lineEdge( part.Vertex( 0 ), part.Vertex( 1 ) );
		CGSegment2 refEdge( part.Vertex( 0 ), part.Vertex( 1 ) );

		part.Partition();
		vector< CGPolygon2 >& partitions = part.GetPartitions();
		for ( size_t j = 0, cntJ = partitions.size(); j < cntJ; ++j )
		{
			CGeometry partitionGeometry;

			CGPolygon2& partition = partitions[j];
			partition.Triangulate();

			// VERTICES
			// bottom
			for ( size_t k = 0, cntK = partition.VerticesCount(); k < cntK; ++k )
			{
				const CGPoint2& p = partition.Vertex( k );
				partitionGeometry.AddVertex( CGPoint3( p.GetX(), p.GetY(), (lineEdge.Distance( p ) - offset) * scaleFactor ) );
			}
			// top
			for ( size_t k = 0, cntK = partition.VerticesCount(); k < cntK; ++k )
			{
				const CGPoint2& p = partition.Vertex( k );
				partitionGeometry.AddVertex( CGPoint3( p.GetX(), p.GetY(), roofThickness + (lineEdge.Distance( p ) - offset) * scaleFactor ) );
			}

			// FACES
			// bottom
			FacesList bottomFaces = CFacesBuilder::BuildFacesFromTriangulation( partitionGeometry,
                                                                          partition.GetTriangulation().GetTrianglesList(), 
                                                                          refEdge,
                                                                          scaleFactor, 
                                                                          bottomTexture,
                                                                          false, true );
			for ( size_t j = 0, cntJ = bottomFaces.Size(); j < cntJ; ++j )
			{
				CFace face = *bottomFaces.Get( j );
				face.SetVisible( m_bottomFacesVisible ? true : false );
				partitionGeometry.AddFace( face );
			}

			// top
			FacesList topFaces = CFacesBuilder::BuildFacesFromTriangulation( partitionGeometry,
                                                                       partition.GetTriangulation().GetTrianglesList(), 
                                                                       refEdge,
                                                                       scaleFactor, 
                                                                       topTexture,
                                                                       true );
			for ( size_t j = 0, cntJ = topFaces.Size(); j < cntJ; ++j )
			{
				CFace face = *topFaces.Get( j );
				face.SetRoadWay( true );
				partitionGeometry.AddFace( face );
			}

			// facade
			FacesList facadeFaces = CFacesBuilder::BuildQuadsClosedStripFaces( partitionGeometry, CTextureProperties( borderTexture, texScale ) );
			for ( size_t j = 0, cntJ = facadeFaces.Size(); j < cntJ; j += 2 )
			{
				CFace* face1 = facadeFaces.Get( j );
				CFace* face2 = facadeFaces.Get( j + 1 );
				const CGPoint3& p1 = partitionGeometry.Vertex( face1->GetIndex( 0 ) );
				const CGPoint3& p2 = partitionGeometry.Vertex( face1->GetIndex( 1 ) );
				CGPoint2 p1plan( p1.GetX(), p1.GetY() );
				CGPoint2 p2plan( p2.GetX(), p2.GetY() );

				if ( !offsetFootprint.HasAsEdge( CGSegment2( p1plan, p2plan ) ) )
				{
					// internal faces
					face1->SetTexture( NULL );
					face2->SetTexture( NULL );
					face1->SetVisible( false );
					face2->SetVisible( false );
					face1->SetShadow( false );
					face2->SetShadow( false );
				}

				partitionGeometry.AddFace( *face1 );
				partitionGeometry.AddFace( *face2 );
			}

			m_geometries.Append( partitionGeometry );
		}
	}

	return (true);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CBuildingLevelsList::CBuildingLevelsList()
{
}

//-----------------------------------------------------------------------------

CBuildingLevelsList::~CBuildingLevelsList()
{
}

//-----------------------------------------------------------------------------

size_t CBuildingLevelsList::Size() const
{
	return (m_levels.size());
}

//-----------------------------------------------------------------------------

const CBuildingLevel* CBuildingLevelsList::Get( size_t index ) const
{
	assert( index < m_levels.size() );
	return (m_levels[index]);
}

//-----------------------------------------------------------------------------

CBuildingLevel* CBuildingLevelsList::Get( size_t index )
{
	assert( index < m_levels.size() );
	return (m_levels[index]);
}

//-----------------------------------------------------------------------------

void CBuildingLevelsList::Clear()
{
  for ( size_t i = 0, cntI = m_levels.size(); i < cntI; ++i )
  {
    delete m_levels[i];
    m_levels[i] = NULL;
  }

  m_levels.clear();
}

//-----------------------------------------------------------------------------

CBuildingLevel* CBuildingLevelsList::AddFloor( const CBuildingFloor& floor )
{
	CBuildingLevel* level = new CBuildingFloor( floor );
	m_levels.push_back( level );
	return (m_levels[m_levels.size() - 1]);
}

//-----------------------------------------------------------------------------

CBuildingLevel* CBuildingLevelsList::AddInterFloor( const CBuildingInterFloor& interFloor )
{
	CBuildingLevel* level = new CBuildingInterFloor( interFloor );
	m_levels.push_back( level );
	return (m_levels[m_levels.size() - 1]);
}

//-----------------------------------------------------------------------------

CBuildingLevel* CBuildingLevelsList::AddRoof( const CBuildingRoof& roof )
{
	CBuildingLevel* level = new CBuildingRoof( roof );
	m_levels.push_back( level );
	return (m_levels[m_levels.size() - 1]);
}

//-----------------------------------------------------------------------------

CBuildingLevel* CBuildingLevelsList::AddLevel( const CBuildingLevel& level )
{
	switch ( level.GetType() )
	{
	case BLT_FLOOR:      return (AddFloor( *reinterpret_cast< const CBuildingFloor* >( &level ) ));
	case BLT_INTERFLOOR: return (AddInterFloor( *reinterpret_cast< const CBuildingInterFloor* >( &level ) ));
	case BLT_ROOF:       return (AddRoof( *reinterpret_cast< const CBuildingRoof* >( &level ) ));
	default:             
		{
			assert( false );
			return (NULL);
		}
	}
}

//-----------------------------------------------------------------------------
