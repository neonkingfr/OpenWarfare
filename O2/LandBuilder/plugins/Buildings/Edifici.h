//-----------------------------------------------------------------------------
// File: Edifici.h
//
// Desc: module to generate procedural buildings
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

// -------------------------------------------------------------------------- //

#include "Windows.h"
#include "Models.h"
#include "StairTypes.h"

// -------------------------------------------------------------------------- //

#define LIBRARY_FILE      "libraryFile"
#define MODELS_PATH       "modelsPath"
#define RANDOM_SEED       "randomSeed"
#define SIMILARITY_FACTOR "similarityFactor"
#define NAME_PREFIX       "namePrefix"

#define TYPE           "type"
#define NUMFLOORS      "numFloors"
#define ENTERABLE      "enterable"
#define RECTIFYCORNERS "rectifyCorners"

// -------------------------------------------------------------------------- //

static const Float FOOTPRINT_AREA_LIMIT      = 1000.0;
static const Float FOOTPRINT_DIMENSION_LIMIT = 50.0;

// -------------------------------------------------------------------------- //

class CEdifici : public LandBuilder2::IBuildModule
{
	// internal use flags
	// {
	size_t m_shapesCount;
	// }

	// the path containing LandBuilder
	String m_workingPath;

	// the path to the folder which will contain the generated models
	String m_modelsPath;
	// the path to the folder which will contain the textures of the models
	String m_texturesPath;
	// the path to the folder which will contain the proxies of the models
	String m_proxiesPath;

	// the name of the file containing the library of buildings types
	String m_libraryFilename;

	// the prefix to add into the filename of the exported models
	String m_namesPrefix;

	// the library of building types
	CBuildingTypesLibrary m_buildingTypesLibrary;

	// the library of stair types
	CStairTypesLibrary m_stairTypesLibrary;

	// the library of created buildings templates
	CBuildingTemplatesLibrary m_buildingTemplatesLibrary;

	// logger to file
	ofstream m_logFile;

	// the random seed
	int m_randomSeed;

	// the similarity factor to apply when comparing two template's footprints
	Float m_similarityFactor;

	DWORD m_startingTime;

  // an error occurred
  bool m_aborted;

public:
	CEdifici();
	virtual ~CEdifici();

	virtual void Run( IMainCommands* cmdIfc );
  virtual void OnEndPass( IMainCommands* cmdIfc );
};

// -------------------------------------------------------------------------- //
