//-----------------------------------------------------------------------------
// File: UvTriangle3.h
//
// Desc: class for an uv mapped three dimensional triangle
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"

//-----------------------------------------------------------------------------

class CUvTriangle3 : public CGTriangle3
{
	CGPoint2 m_uv[3];

public:
	CUvTriangle3();
	CUvTriangle3( const CGPoint3& v1, const CGPoint3& v2,
				  const CGPoint3& v3 );
	virtual ~CUvTriangle3();

	// Getters
	// {
	const CGPoint2& GetUv( size_t index ) const;
	// }

	// Uv manipulators
	// {
	void CalculateUVs( const CGPoint2& firstVertex, Float rotation, Float scale );
	// }
};

//-----------------------------------------------------------------------------
