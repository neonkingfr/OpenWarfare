#pragma once

class IProgressLogSupport;
class ProgressBarFunctions;
#include <utility>

namespace LandBuilder2
{
	class ModuleRegistrator ;
}

namespace ExceptReg
{
	class ExceptionRegister;
}

// -------------------------------------------------------------------------- //

struct LBGlobalPointers
{
	///pointer to implementation of logging
	IProgressLogSupport* logObject;

	///pointer to progress bar handler
	ProgressBarFunctions* progressBarHandler;

	///pointer to register exception service
	ExceptReg::ExceptionRegister* exceptionHandler;

	///pointer to app's allocator
	void* (*mallocFunct)(std::size_t sz);

	///pointer to app's deallocator
	void (*freeFunct)(void* ptr, std::size_t sz);
};

// -------------------------------------------------------------------------- //

class IPlugin
{
public:

	///Retrieves list of registered modules
	/**@return pointer to registration class */  
	virtual LandBuilder2::ModuleRegistrator* GetModules() = 0;  

	///Called before DLL plugin is released
	virtual void Release() = 0;

	///Changes CWD stored in DLL
	/**
	Need to synchronize CWDs before module is initialized
	*/
	virtual void SetCWD(const char* cwd) = 0;
};
