//-----------------------------------------------------------------------------

#include "StdAfx.h"

#include "..\..\HighMapLoaders\include\EtMath.h"
#include "..\..\HighMapLoaders\include\EtHelperFunctions.h"

#include ".\AdvancedOnPlacePlacer.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

    AdvancedOnPlacePlacer::AdvancedOnPlacePlacer()
    : m_version( "1.1.1" )
    , m_shapeCounter( 0 )
    {
    }

//-----------------------------------------------------------------------------

		void AdvancedOnPlacePlacer::Run( IMainCommands* cmdIfc )
		{
      RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
      if ( taskName != m_currTaskName )
      {
        m_shapeCounter = 0;
        m_currTaskName = taskName;
      }

      ++m_shapeCounter;

			// gets shape and its properties
			m_currentShape = cmdIfc->GetActiveShape();  

      if ( !m_currentShape ) { return; }

      LOGF( Info ) << "                                                           ";
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "AdvancedOnPlacePlacer " << m_version << " - Started shape n. " << m_shapeCounter;
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "Parameters:";

			// resets arrays
      if ( m_objectsOut.Size() != 0) { m_objectsOut.Clear(); }

			// gets parameters from calling cfg file
			if ( GetShapeParameters( cmdIfc ) )
			{
				SetGeometry();

				// sets the output objects
				CreateObjects();

				// export objects
				ExportCreatedObjects( cmdIfc );

				// writes the report
				WriteReport();
			}
		}

//-----------------------------------------------------------------------------

		ModuleObjectOutputArray* AdvancedOnPlacePlacer::RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn )
		{
			m_currentShape = shape;
			m_shapeIn      = shapeIn;

      if ( !m_currentShape ) { return (NULL); }

			// resets arrays
      if ( m_objectsOut.Size() != 0 ) { m_objectsOut.Clear(); }

			SetGeometry();

			// sets the output objects
			CreateObjects();

      return (&m_objectsOut);
		}

//-----------------------------------------------------------------------------

		bool AdvancedOnPlacePlacer::GetShapeParameters( IMainCommands* cmdIfc )
		{
			// object (isMovable)
			const char* x = cmdIfc->QueryValue( "object", true );
      if ( !x ) { x = cmdIfc->QueryValue( "object", false ); }

      if ( x )
			{
				m_shapeIn.m_name = x;
			}
			else
			{        
				ExceptReg::RegExcpt( Exception( 11, ">>> No object specified <<<" ) );
				return (false);
			}

      LOGF( Info ) << "object: " << m_shapeIn.m_name;

      // dirCorrection (isMovable && hasDefault)
			m_shapeIn.m_dirCorrection = 0.0; // default value
			x = cmdIfc->QueryValue( "dirCorrection", true );
      if ( !x ) { x = cmdIfc->QueryValue( "dirCorrection", false ); }

			if ( x ) 
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_dirCorrection = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> ------------------------------------------------------ <<<";
          LOGF( Warn ) << ">>> Bad data for dirCorrection - using default value (0.0) <<<";
          LOGF( Warn ) << ">>> ------------------------------------------------------ <<<";
				}
			}
			else
			{
        LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
        LOGF( Warn ) << ">>> Missing dirCorrection value - using default value (0.0) <<<";
        LOGF( Warn ) << ">>> ------------------------------------------------------- <<<";
			}

      LOGF( Info ) << "dirCorrection: " << m_shapeIn.m_dirCorrection;

			return (true);
		}

//-----------------------------------------------------------------------------

		void AdvancedOnPlacePlacer::SetGeometry()
		{
			Shapes::DVertex barycenter = m_currentShape->Barycenter();

			size_t vCount = m_currentShape->GetVertexCount();
			
			// inertia moment about y axis
			double Iyy = 0.0;

			// inertia moment about x axis
			double Ixx = 0.0;

			// mixed inertia moment 
			double Ixy = 0.0;

			for ( size_t i = 0; i < vCount; ++i )
			{
				double dx = m_currentShape->GetVertex( i ).x - barycenter.x;
				double dy = m_currentShape->GetVertex( i ).y - barycenter.y;
				Iyy += (dx * dx);
				Ixx += (dy * dy);
				Ixy += (dx * dy);
			}

			if ( Ixx == Iyy )
			{
				m_orientation = 0.0;
			}
			else
			{
				double t = -Ixy / (Ixx - Iyy);
				double twoAlpha = EtMathD::Atan( t );
				m_orientation = twoAlpha * 0.5;
			}
		}

//-----------------------------------------------------------------------------

		void AdvancedOnPlacePlacer::CreateObjects()
		{
			ModuleObjectOutput out;

			// sets object out data
			out.position = m_currentShape->Barycenter();
			out.rotation = m_orientation + EtMathD::DegToRad( m_shapeIn.m_dirCorrection );
			out.scaleX = out.scaleY = out.scaleZ = 1.0;

			// used only by VLB for preview
			out.length = 10.0;
			out.width  = 10.0;
			out.height = 10.0;

			// adds to array
			m_objectsOut.Add( out );
		}

//-----------------------------------------------------------------------------

		void AdvancedOnPlacePlacer::ExportCreatedObjects( IMainCommands* cmdIfc )
		{
			int objOutCount = m_objectsOut.Size();
			for ( int i = 0; i < objOutCount; ++i )
			{
				RString name = m_shapeIn.m_name;
				Vector3D position = Vector3D( m_objectsOut[i].position.x, 
                                                             0.0, 
                                      m_objectsOut[i].position.y );

				Matrix4D mTranslation = Matrix4D( MTranslation, position );
				Matrix4D mRotation    = Matrix4D( MRotationY, m_objectsOut[i].rotation );

				Matrix4D transform = mTranslation * mRotation;
				cmdIfc->GetObjectMap()->PlaceObject( transform, name, 0 );
			}
		}

//-----------------------------------------------------------------------------

		void AdvancedOnPlacePlacer::WriteReport()
		{
			int objOutCount = m_objectsOut.Size();
      LOGF( Info ) << "-----------------------------------------------------------";
      LOGF( Info ) << "Created " << objOutCount << " new objects";
      LOGF( Info ) << "-----------------------------------------------------------";
		}

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt