//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\Shapes\ShapeHelpers.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
  namespace Modules
  {

//-----------------------------------------------------------------------------

    class BestFitPlacer : public IBuildModule
    {
    public:
      struct ShapeInput
      {
        double m_desiredHeight;
        double m_dirCorrection;
      };

      struct ObjectInput
      {
        RString m_name;
        double  m_length;
        double  m_width;
        double  m_height;

        ObjectInput() 
        {
        }

        ObjectInput( RString name, double length, double width, double height )
        : m_name( name )
        , m_length( length )
        , m_width( width )
        , m_height( height )
        {
        }

        ClassIsMovable( ObjectInput );
      };

    public:
      BestFitPlacer();

      virtual ~BestFitPlacer()
      {
      }

      virtual void Run( IMainCommands* cmdIfc );
      ModuleObjectOutputArray* RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn,
                                             AutoArray< BestFitPlacer::ObjectInput >& objectsIn );

      DECLAREMODULEEXCEPTION( "BestFitPlacer" )

    private:
      string m_version;
      size_t m_shapeCounter;

      const Shapes::IShape* m_currentShape;   
      ShapeInput            m_shapeIn;

      double m_calcLength;
      double m_calcWidth;
      double m_calcHeight;
      double m_calcOrientation;

      AutoArray< ObjectInput > m_objectsIn;
      ModuleObjectOutputArray  m_objectsOut;

      bool GetShapeParameters( IMainCommands* cmdIfc );
      bool GetObjectsParameters( IMainCommands* cmdIfc );
      void SetGeometry();
      void CreateObjects();
      void ExportCreatedObjects( IMainCommands* cmdIfc );
      void WriteReport();
    };

//-----------------------------------------------------------------------------

  } // namespace Modules
} // namespace LandBuildInt