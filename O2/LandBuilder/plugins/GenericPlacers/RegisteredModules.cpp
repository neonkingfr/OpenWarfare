#include "StdAfx.h"
#include "ModuleRegistrator.h"

namespace LandBuilder2
{
	using namespace LandBuildInt;
}

//source contains list of registered modules. 
/**Don't add new modules here. Instead, register module using the registrator 
at place of module's definition
*/
#include "AdvancedOnPlacePlacer.h"
#include "BestFitPlacer.h"
#include "OnPlacePlacer.h"
#include "RegularOnPathPlacer.h"

namespace LandBuilder2
{
	RegisterModule<Modules::AdvancedOnPlacePlacer> _register_GP_001("AdvancedOnPlacePlacer");
	RegisterModule<Modules::BestFitPlacer>         _register_GP_002("BestFitPlacer");
	RegisterModule<Modules::OnPlacePlacer>         _register_GP_003("OnPlacePlacer");
	RegisterModule<Modules::RegularOnPathPlacer>   _register_GP_004("RegularOnPathPlacer");
}