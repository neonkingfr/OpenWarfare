//-----------------------------------------------------------------------------

#include "StdAfx.h"

#include "..\..\HighMapLoaders\include\EtMath.h"
#include "..\..\HighMapLoaders\include\EtHelperFunctions.h"

#include ".\OnPlacePlacer.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

    OnPlacePlacer::OnPlacePlacer()
    : m_version( "1.1.1" )
    , m_shapeCounter( 0 )
    {
    }

//-----------------------------------------------------------------------------

		void OnPlacePlacer::Run( IMainCommands* cmdIfc )
		{
      RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
      if ( taskName != m_currTaskName )
      {
        m_shapeCounter = 0;
        m_currTaskName = taskName;
      }

      ++m_shapeCounter;

    	// gets shape and its properties
			m_currentShape = cmdIfc->GetActiveShape();    

      if ( !m_currentShape ) { return; }

      LOGF( Info ) << "                                                           ";
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "OnPlacePlacer " << m_version << " - Started shape n. " << m_shapeCounter;
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "Parameters:";

			// resets arrays
      if ( m_objectsOut.Size() != 0 ) { m_objectsOut.Clear(); }

			// gets parameters from calling cfg file
			if ( GetShapeParameters( cmdIfc ) )
			{
				// sets the output objects
				CreateObjects();

				// export objects
				ExportCreatedObjects( cmdIfc );

				// writes the report
				WriteReport();
			}
		}

//-----------------------------------------------------------------------------

		ModuleObjectOutputArray* OnPlacePlacer::RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn )
		{
			m_currentShape = shape;
			m_shapeIn      = shapeIn;

      if ( !m_currentShape ) { return (NULL); }

			// resets arrays
      if ( m_objectsOut.Size() != 0 ) { m_objectsOut.Clear(); }

			// sets the output objects
			CreateObjects();

			return (&m_objectsOut);
		}

//-----------------------------------------------------------------------------

		bool OnPlacePlacer::GetShapeParameters( IMainCommands* cmdIfc )
		{
			// orientation (isMovable)
			const char* x = cmdIfc->QueryValue( "orientation", true );
			if ( !x ) { x = cmdIfc->QueryValue( "orientation", false ); }

      if ( x )
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_orientation = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 11, ">>> Bad data for orientation <<<" ) );
					return false;
				}
			}
			else
			{        
				ExceptReg::RegExcpt( Exception( 12, ">>> Missing orientation value <<<" ) );
				return false;
			}

      LOGF( Info ) << "orientation: " << m_shapeIn.m_orientation;

      // object (isMovable)
			x = cmdIfc->QueryValue( "object", true );
			if ( !x ) { x = cmdIfc->QueryValue( "object", false ); }

      if ( x )
			{
				m_shapeIn.m_name = x;
			}
			else
			{        
				ExceptReg::RegExcpt( Exception( 13, ">>> No object specified <<<" ) );
				return false;
			}

      LOGF( Info ) << "object: " << m_shapeIn.m_name;

      // scaleX (isMovable && hasDefault)
      m_shapeIn.m_scaleX = 1.0; // default value

      x = cmdIfc->QueryValue( "scaleX", true );
			if ( !x ) { x = cmdIfc->QueryValue( "scaleX", false ); }

      if ( x )
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_scaleX = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> ----------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for scaleX - using default value (1.0) <<<";
          LOGF( Warn ) << ">>> ----------------------------------------------- <<<";
				}
			}
			else
			{        
        LOGF( Warn ) << ">>> ------------------------------------------------ <<<";
        LOGF( Warn ) << ">>> Missing scaleX value - using default value (1.0) <<<";
        LOGF( Warn ) << ">>> ------------------------------------------------ <<<";
			}

      LOGF( Info ) << "scaleX: " << m_shapeIn.m_scaleX;

      // scaleY (isMovable && hasDefault)
			m_shapeIn.m_scaleY = 1.0; // default value

      x = cmdIfc->QueryValue( "scaleY", true );
			if ( !x ) { x = cmdIfc->QueryValue( "scaleY", false ); }

      if ( x )
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_scaleY = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> ----------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for scaleY - using default value (1.0) <<<";
          LOGF( Warn ) << ">>> ----------------------------------------------- <<<";
				}
			}
			else
			{        
        LOGF( Warn ) << ">>> ------------------------------------------------ <<<";
        LOGF( Warn ) << ">>> Missing scaleY value - using default value (1.0) <<<";
        LOGF( Warn ) << ">>> ------------------------------------------------ <<<";
			}

      LOGF( Info ) << "scaleY: " << m_shapeIn.m_scaleY;

      // scaleZ (isMovable && hasDefault)
			m_shapeIn.m_scaleZ = 1.0; // default value

      x = cmdIfc->QueryValue( "scaleZ", true );
			if ( !x ) { x = cmdIfc->QueryValue( "scaleZ", false ); }

			if ( x )
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_scaleZ = atof( x );
				}
				else
				{
          LOGF( Warn ) << ">>> ----------------------------------------------- <<<";
          LOGF( Warn ) << ">>> Bad data for scaleZ - using default value (1.0) <<<";
          LOGF( Warn ) << ">>> ----------------------------------------------- <<<";
				}
			}
			else
			{        
        LOGF( Warn ) << ">>> ------------------------------------------------ <<<";
        LOGF( Warn ) << ">>> Missing scaleZ value - using default value (1.0) <<<";
        LOGF( Warn ) << ">>> ------------------------------------------------ <<<";
			}

      LOGF( Info ) << "scaleZ: " << m_shapeIn.m_scaleZ;

			return (true);
		}

//-----------------------------------------------------------------------------

		void OnPlacePlacer::CreateObjects()
		{
			int vCount = m_currentShape->GetVertexCount();
			for ( int i = 0; i < vCount; ++i )
			{
				ModuleObjectOutput out;

				// sets object out data
				out.position = m_currentShape->GetVertex( i );
        // WARNING: the minus sign is an hack
				out.rotation = -EtMathD::DegToRad( m_shapeIn.m_orientation );
				out.scaleX   = m_shapeIn.m_scaleX;
				out.scaleY   = m_shapeIn.m_scaleY;
				out.scaleZ   = m_shapeIn.m_scaleZ;

				// used only by VLB for preview
				out.length = 10.0;
				out.width  = 10.0;
				out.height = 10.0;

				// adds to array
				m_objectsOut.Add( out );
			}
		}

//-----------------------------------------------------------------------------

		void OnPlacePlacer::ExportCreatedObjects( IMainCommands* cmdIfc )
		{
			int objOutCount = m_objectsOut.Size();
			for ( int i = 0; i < objOutCount; ++i )
			{
				RString name = m_shapeIn.m_name;
				Vector3D position = Vector3D( m_objectsOut[i].position.x, 
                                                             0.0, 
                                      m_objectsOut[i].position.y );

				Matrix4D mTranslation = Matrix4D( MTranslation, position );
				Matrix4D mRotation    = Matrix4D( MRotationY, m_objectsOut[i].rotation );
				Matrix4D mScaling     = Matrix4D( MScale, m_objectsOut[i].scaleX, 
                                                  m_objectsOut[i].scaleZ, 
                                                  m_objectsOut[i].scaleY );

				Matrix4D transform = mTranslation * mRotation * mScaling;
				cmdIfc->GetObjectMap()->PlaceObject( transform, name, 0 );
			}
		}

//-----------------------------------------------------------------------------

		void OnPlacePlacer::WriteReport()
		{
      LOGF( Info ) << "-----------------------------------------------------------";
      LOGF( Info ) << "Created                    ";
      LOGF( Info ) << m_objectsOut.Size() << " : " << m_shapeIn.m_name << "                           ";
      LOGF( Info ) << "-----------------------------------------------------------";
		}

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt
