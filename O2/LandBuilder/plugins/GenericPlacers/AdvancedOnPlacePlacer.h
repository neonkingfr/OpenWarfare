//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\Shapes\ShapeHelpers.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		class AdvancedOnPlacePlacer : public IBuildModule
		{
		public:
			struct ShapeInput
			{
				RString m_name;
				double  m_dirCorrection;
			};

		public:
      AdvancedOnPlacePlacer();

      virtual ~AdvancedOnPlacePlacer()
      {
      }

			virtual void Run( IMainCommands* cmdIfc );
			ModuleObjectOutputArray* RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn );

      DECLAREMODULEEXCEPTION( "AdvancedOnPlacePlacer" )

		private:
      string m_version;
      size_t m_shapeCounter;

			const Shapes::IShape* m_currentShape;   
			ShapeInput            m_shapeIn;

			double m_orientation;

			ModuleObjectOutputArray m_objectsOut;

			bool GetShapeParameters( IMainCommands* cmdIfc );
			void SetGeometry();
			void CreateObjects();
			void ExportCreatedObjects( IMainCommands* cmdIfc );
			void WriteReport();
		};

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt