//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\Shapes\ShapeHelpers.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		class OnPlacePlacer : public IBuildModule
		{
		public:
			struct ShapeInput
			{
				RString m_name;
				double  m_orientation;
				double  m_scaleX;
				double  m_scaleY;
				double  m_scaleZ;
			};

		public:
      OnPlacePlacer();

      virtual ~OnPlacePlacer()
      {
      }

			virtual void Run( IMainCommands* cmdIfc );
			ModuleObjectOutputArray* RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn );

      DECLAREMODULEEXCEPTION( "OnPlacePlacer" )

		private:
      string m_version;
      size_t m_shapeCounter;

			const Shapes::IShape* m_currentShape;   
			ShapeInput            m_shapeIn;

			ModuleObjectOutputArray m_objectsOut;

			bool GetShapeParameters( IMainCommands* cmdIfc );
			void CreateObjects();
			void ExportCreatedObjects( IMainCommands* cmdIfc );
			void WriteReport();
		};

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt

