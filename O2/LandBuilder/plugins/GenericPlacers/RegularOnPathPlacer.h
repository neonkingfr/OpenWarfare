//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\Shapes\ShapeHelpers.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
  namespace Modules
  {

//-----------------------------------------------------------------------------

    class RegularOnPathPlacer : public IBuildModule
    {
    public:
      struct ShapeInput
      {
        size_t m_randomSeed;
        double m_offset;
        double m_offsetMaxNoise;
        double m_step;
        double m_stepMaxNoise;
        double m_firstStep;
      };

      struct ObjectInput
      {
        RString m_name;
        double  m_minHeight;
        double  m_maxHeight;
        bool    m_matchOrient;
        double  m_dirCorrection;
        bool    m_placeRight;
        bool    m_placeLeft;

        ObjectInput() 
        {
        }

        ObjectInput( RString name, double minHeight, double maxHeight, bool matchOrient, 
                     double dirCorrection, bool placeRight, bool placeLeft )
        : m_name( name )
        , m_minHeight( minHeight )
        , m_maxHeight( maxHeight )
        , m_matchOrient( matchOrient )
        , m_dirCorrection( dirCorrection )
        , m_placeRight( placeRight )
        , m_placeLeft( placeLeft )
        {
        }

        ClassIsMovable( ObjectInput ); 
      };

    public:
      string m_version;
      size_t m_shapeCounter;

      RegularOnPathPlacer();
      virtual ~RegularOnPathPlacer()
      {
      }

      virtual void Run( IMainCommands* cmdIfc );
      ModuleObjectOutputArray* RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn, 
                                             AutoArray< RegularOnPathPlacer::ObjectInput >& objectsIn );

      DECLAREMODULEEXCEPTION( "RegularOnPathPlacer" )

    private:
      AutoArray< ObjectInput > m_objectsIn;
      ModuleObjectOutputArray  m_objectsOut;

      ShapeInput            m_shapeIn;
      const Shapes::IShape* m_currentShape;      

      bool GetShapeParameters( IMainCommands* cmdIfc );
      bool GetObjectsParameters( IMainCommands* cmdIfc );
      void CreateObjects( bool preview );
      void ExportCreatedObjects( IMainCommands* cmdIfc );
			void WriteReport();

      Shapes::DVertex ConvertToXY( double s, Shapes::DVertex& v1, Shapes::DVertex& v2 );
    };

//-----------------------------------------------------------------------------

  } // namespace Modules
} // namespace LandBuildInt
