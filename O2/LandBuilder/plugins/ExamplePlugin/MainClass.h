#pragma once

///Class demonstrates, how you can override features implemented by PluginMainClass
/**
To override default features, derive PluginMainClass and create a static instance of this
class. Existence of the static instance causes, that pluginMain will use this instance 
instead of default instance. You should create only one instance per whole DLL.
*/
class MainClass: public PluginMainClass
{
    void *usermemory;
public:

    void InitPlugin(LBGlobalPointers* globalPointers);
    void Release();

    void SetCWD(const char *cwd);

};

///global instance of the MainClass
extern MainClass mainClass;