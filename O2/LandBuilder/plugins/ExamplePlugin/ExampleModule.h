//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

class ExampleModule : public LandBuilder2::IBuildModule
{
public:
  ///called on begin of pass during multipass processing
  virtual void OnBeginPass( IMainCommands* cmdIfc );
  
  ///called on begin of group
  virtual void OnBeginGroup( IMainCommands* cmdIfc );
  
  ///called on end of group
  virtual void OnEndGroup( IMainCommands* cmdIfc );
  
  ///called on end of pass       
  virtual void OnEndPass( IMainCommands* cmdIfc );
  
  ///called for every shape
  virtual void Run( IMainCommands* cmdIfc );
  
  ///called for destruction
  virtual void Release();
};

//-----------------------------------------------------------------------------
