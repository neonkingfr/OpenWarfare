//-----------------------------------------------------------------------------

#include "StdAfx.h"

#include "..\..\LandBuilder2\ObjectContainer.h"

#include "..\..\HighMapLoaders\include\EtHelperFunctions.h"
#include "..\..\HighMapLoaders\include\EtMath.h"
#include "..\..\Shapes\ShapePolygon.h"

#include ".\DeleteAlongPath.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

    DeleteAlongPath::DeleteAlongPath()
    : m_version( "1.1.2" )
    , m_shapeCounter( 0 )
    {
    }

//-----------------------------------------------------------------------------

		void DeleteAlongPath::Run( IMainCommands* cmdIfc )
		{
      RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
      if ( taskName != m_currTaskName )
      {
        m_shapeCounter = 0;
        m_currTaskName = taskName;
      }

      ++m_shapeCounter;

			// gets shape and its properties
			m_currentShape = cmdIfc->GetActiveShape();    

      if ( !m_currentShape ) { return; }

      LOGF( Info ) << "                                                           ";
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "DeleteAlongPath " << m_version << " - Started shape n. " << m_shapeCounter;
      LOGF( Info ) << "===========================================================";
      LOGF( Info ) << "Parameters:";

			m_deletionCounter = 0;

			// gets parameters from calling cfg file
			if ( GetShapeParameters( cmdIfc ) )
			{
				// delete the objects
				DeleteObjects( cmdIfc );

				// writes the report
				WriteReport();
			}
		}

//-----------------------------------------------------------------------------

		bool DeleteAlongPath::GetShapeParameters( IMainCommands* cmdIfc )
		{
			// width (isMovable)
			const char* x = cmdIfc->QueryValue( "width", true );
			if ( !x ) { x = cmdIfc->QueryValue( "width", false ); }
      
      if ( x )
			{
				if ( IsFloatingPoint( string( x ), false ) )
				{
					m_shapeIn.m_width = atof( x );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 11, ">>> Bad data for width <<<" ) );
					return (false);
				}
			}
			else
			{        
				ExceptReg::RegExcpt( Exception( 12, ">>> Missing width value <<<" ) );
				return (false);
			}

      LOGF( Info ) << "width: " << m_shapeIn.m_width;

			return (true);
		}

//-----------------------------------------------------------------------------

		void DeleteAlongPath::DeleteObjects( IMainCommands* cmdIfc )
		{
			int objCount = 0;
			int objID = -1;
			while ( cmdIfc->GetObjectMap()->EnumObjects( objID ) )
			{
				objCount++;
				break;
			}

			if ( objCount > 0 )
			{
				int vCount = m_currentShape->GetVertexCount();
				for ( int i = 0; i < vCount - 1; ++i )
				{
					const Shapes::DVertex& v1 = m_currentShape->GetVertex( i );
					const Shapes::DVertex& v2 = m_currentShape->GetVertex( i + 1 );

					Shapes::DVector v12( v2, v1 );
					double v12Length = v12.SizeXY();

					// goes on only if the segment is not a null segment
					if ( v12Length > 0.0 )
					{
						// extends the segment to avoid the formation of zones of non overlapping
						// between adjacents segments. otherwise objects in non-overlapping zones
						// will be not detected and deleted.
						double stretchFactor = (v12Length + m_shapeIn.m_width * 0.5) / v12Length;
						Shapes::DVector temp1( v2, v1 );
						temp1 *= stretchFactor;
						Shapes::DVertex newV2 = temp1.GetSecondEndXY( v1 );
						Shapes::DVector temp2( v1, v2 );
						temp2 *= stretchFactor;
						Shapes::DVertex newV1 = temp2.GetSecondEndXY( v2 );

						// now starts with the algorithm
						// first creates a rectangle around the segment
						Shapes::DVector v( newV2, newV1 );
						v.NormalizeXYInPlace();

						Shapes::DVector vPerpCW  = v.PerpXYCW();
						Shapes::DVector vPerpCCW = v.PerpXYCCW();

						vPerpCW.NormalizeXYInPlace();
						vPerpCCW.NormalizeXYInPlace();

						vPerpCW  *= (m_shapeIn.m_width * 0.5);
						vPerpCCW *= (m_shapeIn.m_width * 0.5);

						Shapes::DVertex v1R = vPerpCW.GetSecondEndXY( newV1 );
						Shapes::DVertex v1L = vPerpCCW.GetSecondEndXY( newV1 );
						Shapes::DVertex v2R = vPerpCW.GetSecondEndXY( newV2 );
						Shapes::DVertex v2L = vPerpCCW.GetSecondEndXY( newV2 );

						size_t points[] = { 0, 1, 2, 3 };
						Array< size_t > arrPoints( points, 4 );
						size_t parts[] = { 0 };
						Array< size_t > arrParts( parts, 1 );

						Shapes::VertexArray vertices;
						vertices.AddVertex( v1R );
						vertices.AddVertex( v2R );
						vertices.AddVertex( v2L );
						vertices.AddVertex( v1L );
					
						Shapes::ShapePolygon p( arrPoints, arrParts, &vertices );

						// then, searches for objects inside the rectangle
						int objID = -1;
						while ( cmdIfc->GetObjectMap()->EnumObjects( objID ) )
						{
							const Matrix4D& matr = cmdIfc->GetObjectMap()->GetObjectLocation( objID );
							const Vector3D objPos = matr.Position();

							Shapes::DVector tempV( objPos.X(), objPos.Z(), objPos.Y() );
							if ( p.PtInside( tempV ) )
							{
								cmdIfc->GetObjectMap()->DeleteObject( objID );
								m_deletionCounter++;
							}
						}
					}
				}
			}
		}

//-----------------------------------------------------------------------------

		void DeleteAlongPath::WriteReport()
		{
      LOGF( Info ) << "-----------------------------------------------------------";
      LOGF( Info ) << "Deleted " << m_deletionCounter << " objects.     ";
      LOGF( Info ) << "-----------------------------------------------------------";
		}

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt

