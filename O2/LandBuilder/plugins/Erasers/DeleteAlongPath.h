//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\Shapes\IShape.h"

//-----------------------------------------------------------------------------

using std::string;

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		class DeleteAlongPath : public IBuildModule
		{
		public:
			struct ShapeInput
			{
				double m_width;
			};

		public:
      DeleteAlongPath();

      virtual ~DeleteAlongPath()
      {
      }

			virtual void Run( IMainCommands* cmdIfc );

      DECLAREMODULEEXCEPTION( "DeleteAlongPath" )

		private:
      string m_version;
      size_t m_shapeCounter;

			const Shapes::IShape* m_currentShape;   
			ShapeInput            m_shapeIn;
			int                   m_deletionCounter;

			bool GetShapeParameters( IMainCommands* cmdIfc );
			void DeleteObjects( IMainCommands* cmdIfc );
			void WriteReport();
		};

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt

