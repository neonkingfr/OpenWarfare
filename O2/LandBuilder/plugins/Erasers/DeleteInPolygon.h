//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\Shapes\IShape.h"

//-----------------------------------------------------------------------------

using std::string;

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		class DeleteInPolygon : public IBuildModule
		{
		public:
      DeleteInPolygon();

      virtual ~DeleteInPolygon()
      {
      }

			virtual void Run( IMainCommands* cmdIfc );

      DECLAREMODULEEXCEPTION( "DeleteInPolygon" )

		private:
      string m_version;
      size_t m_shapeCounter;

			const Shapes::IShape* m_currentShape;   
			int                   m_deletionCounter;

			void DeleteObjects( IMainCommands* cmdIfc );
			void WriteReport();
		};

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt

