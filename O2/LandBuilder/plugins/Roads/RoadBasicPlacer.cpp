//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "RoadBasicPlacer.h"
#include "ShapeCross.h"
#include "ShapePolyLineWithStartEndDirection.h"
#include "ParamParser.h"

//-----------------------------------------------------------------------------

using namespace Shapes;

//-----------------------------------------------------------------------------

static LandBuilder2::RegisterModule< RoadBasicPlacer > roadBasicPlacer( "RoadBasicPlacer" );

//-----------------------------------------------------------------------------

RoadBasicPlacer::RoadBasicPlacer()
: m_version( "1.1.0" )
{
}

//-----------------------------------------------------------------------------

RoadBasicPlacer::~RoadBasicPlacer()
{
}

//-----------------------------------------------------------------------------

void RoadBasicPlacer::Run( IMainCommands* cmdIfc ) 
{
  const IShape* curShape   = cmdIfc->GetActiveShape();
  const IShapeExtra* extra = curShape->GetInterface< IShapeExtra >();

  if ( !extra )
  {
    LOGF( Error ) << "Found unknown shape in shapefile. Cannot get type of that shape";
    return;
  }

  RString shapeType = extra->GetShapeTypeName();
  if ( shapeType == ShapePolyLineWithStartEndDirection::s_name )
  {
    PlaceRoad( cmdIfc );
  }
  else if ( shapeType == ShapeCross::s_name )
  {
    PlaceCrossRoad( cmdIfc );
  }
  else
  {
    LOGF( Error ) << "Can't operate on shape: " << shapeType.Data();
    return;
  }
}

//-----------------------------------------------------------------------------

typedef Shapes::DVertex MyPoint;

//-----------------------------------------------------------------------------

static MyPoint CalculateSide( const MyPoint& p1, const MyPoint& p2, double side, int pos )
{
  MyPoint dir = p2 - p1;
  dir = dir / dir.SizeXY();
  MyPoint pdir( dir.y, -dir.x );
  return ((pos == 1 ? p2 : p1) + pdir * side);
}

//-----------------------------------------------------------------------------

#pragma warning (disable: 4244)

//-----------------------------------------------------------------------------

static void PlaceStraightObject( IObjectMap* land, const char* partNameBase, int idx, 
                                 const MyPoint& p1, const MyPoint& dir, double width, double flen )
{
  char* fullname = (char*)alloca( strlen( partNameBase ) + 20 );
  sprintf( fullname, "%s%ds", partNameBase, idx );
  Matrix4D pos( flen * dir.x, 0.0, -width * dir.y, p1.x,
                         0.0, 1.0,            0.0,  0.0,
                flen * dir.y, 0.0, width * dir.x, p1.y );            

  land->PlaceObject( pos, fullname, 100 );
}

//-----------------------------------------------------------------------------

static void PlaceCurveObject( IObjectMap* land, const char* partNameBase, const char* partId, 
                              const MyPoint& p1, const MyPoint& p2, const MyPoint& p3 )
{
  char* fullname = (char*)alloca( strlen( partNameBase ) + strlen( partId ) + 1 );
  sprintf( fullname, "%s%s", partNameBase, partId );

  MyPoint dir1 = p1 - p2;
  MyPoint dir3 = p3 - p2;

  Matrix4D pos( dir1.x, 0.0, dir3.x, p2.x,
                   0.0, 1.0,    0.0,  0.0,
                dir1.y, 0.0, dir3.y, p2.y );
  pos = pos * Matrix4D( MTranslation, Vector3D( 0.25, 0, 0.25 ) );
  land->PlaceObject( pos, fullname, 100 );      
}

//-----------------------------------------------------------------------------

static void PlaceCurveObject( IObjectMap* land, const char* partNameBase, char type, int idx, 
                              const MyPoint& p1, const MyPoint& p2, const MyPoint& p3 )
{
  char buff[50];
  sprintf( buff, "%d%c", idx, type );
  PlaceCurveObject( land, partNameBase, buff, p1, p2, p3 );
}

//-----------------------------------------------------------------------------

class Walker 
{
  const char* m_partNameBase;
  char        m_type;
  int         m_idx;
  int         m_count;
  MyPoint     m_p1;
  MyPoint     m_p2;
  MyPoint     m_p3;
  MyPoint     m_lastPt;
  int         m_side;
  IObjectMap* m_map;

public:
  Walker( const char* partNameBase, char startType, int startIdx, int partCount,
          const MyPoint& startPt, const MyPoint& vectorPt, IObjectMap* map )
	: m_partNameBase( partNameBase )
	, m_type( startType )
	, m_idx( startIdx )
	, m_count( partCount / 2 )
	, m_lastPt( startPt )
	, m_side( 1 )
	, m_map( map )
  {
    m_p1 = CalculateSide( startPt, vectorPt, startPt.m * m_side * 0.5, 0 ); 
		m_side = -m_side;
    m_p2 = CalculateSide( startPt, vectorPt, startPt.m * m_side * 0.5, 0 ); 
		m_side = -m_side;
  }

  void Step( const MyPoint& nextPt, char left, char right )
  {
    m_p3 = CalculateSide( m_lastPt, nextPt, nextPt.m * m_side * 0.5, 1 ); 
		m_side = -m_side;
    PlaceCurveObject( m_map, m_partNameBase, m_type, m_idx, m_p1, m_p2, m_p3 );
    m_p1 = m_p2;
    m_p2 = m_p3;
    if ( m_type == right )
		{
			m_type = left;
		}
		else 
    {
      if ( m_idx >= m_count ) { m_idx -= m_count; }
      ++m_idx;
      m_type = right;        
    }
    m_lastPt = nextPt;
  }

  void Terminate( const MyPoint& vectorPt, int endType, int endIdx )
  {
    m_p3 = CalculateSide( vectorPt, m_lastPt, m_lastPt.m * m_side * 0.5, 1 );
		m_side = -m_side;
    PlaceCurveObject( m_map, m_partNameBase, endType, endIdx, m_p1, m_p2, m_p3 );
  }
};

//-----------------------------------------------------------------------------

void RoadBasicPlacer::PlaceRoad( IMainCommands* cmdIfc ) 
{
  const IShape* curShape = cmdIfc->GetActiveShape();
  const ShapePolyLineWithStartEndDirection* shape = static_cast< const ShapePolyLineWithStartEndDirection* >( curShape );

  LandBuilder2::ParamParser params( cmdIfc );
  try 
	{
    const char* partName = params.GetTextValue( "partNameBase" );
    int partCount = params.GetIntValue( "partCount2" );
    IObjectMap* map = cmdIfc->GetObjectMap();

    Walker wlk( partName, 'B', 0, partCount, shape->GetVertex( 0 ), shape->GetStartDirPt(), map );
    ProgressBar< int > pb( shape->GetVertexCount() );
    for ( int i = 1, cnt = shape->GetVertexCount(); i < cnt; ++i )
    {
      pb.AdvanceNext( 1 );
      wlk.Step( shape->GetVertex( i ), 'L', 'R' );
    }
    wlk.Terminate( shape->GetEndDirPt(), 'E', 0 );
  } 
	catch ( LandBuilder2::ParamParserException& e ) 
	{
    LOGF( Error ) << "Cannot parse parameters: " << e.Message() << " " << e.GetName() << "=" << e.GetValue();
  }
}

//-----------------------------------------------------------------------------

void RoadBasicPlacer::PlaceCrossRoad( IMainCommands* cmdIfc )
{
  const IShape* curShape = cmdIfc->GetActiveShape();
  const ShapeCross* shape = static_cast< const ShapeCross* >( curShape );
  
  DVertex vx = shape->GetVertex( shape->GetVertexCount() - 1 );
  double rot = shape->GetRotation();
  double scale = shape->GetScale();
  rot = -rot * 3.1415926535 / 180.0;
  Matrix4D mx = Matrix4D( MTranslation, Vector3D( vx.x, 0.0, vx.y ) ) * 
                Matrix4D( MRotationY, rot ) * 
                Matrix4D( MScale, scale, scale, scale );

  RString model = shape->GetModel();
  const char* ext = Pathname::GetExtensionFromPath( model );
  model = model.Mid( 0, ext - model.Data() );

  cmdIfc->GetObjectMap()->PlaceObject( mx, model, 0 );
}

//-----------------------------------------------------------------------------
