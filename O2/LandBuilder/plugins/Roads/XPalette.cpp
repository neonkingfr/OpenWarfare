//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "XPalette.h"
#include <fstream>
#include <time.h>
#include <process.h>
#include <es/algorithms/qsort.hpp>
#include <set>

//-----------------------------------------------------------------------------

RString XPaletteException::BuildMsgText( int code, const char* text )
{
  char buff[50];
  return (RString( _itoa( code, buff, 10 ) ) + " " + text);
}

//-----------------------------------------------------------------------------

void XPalette::LoadPalette( const char* filename )
{
  Clear();    
  ParamFile pfile;
  LSError err = pfile.Parse( filename );
  if ( err ) { throw XPaletteException( err, "File format parse error" ); }

  ParamEntryPtr entry = pfile.FindEntry( "Crossroads" );
  if ( entry.IsNull() )    { throw XPaletteException( 1001, "Cannot find 'Crossroads' class" ); }
  if ( !entry->IsClass() ) { throw XPaletteException( 1002, "'Crossroads' must be class" ); }
  ParamClassPtr xroadclass = entry->GetClassInterface();

  int i = 0;
  char buff[20];
  do
  {
    sprintf( buff, "Item%04d", i );
    entry = xroadclass->FindEntry( buff );
    if ( entry.IsNull() ) { break; }
    if ( entry->IsClass() )
    {
      ParamClassPtr item = entry->GetClassInterface();
      entry = item->FindEntry( "numExits" );
      if ( entry.IsNull() ) { throw XPaletteException( 1003, "Missing field 'numExits'" ); }
      int numExits = *entry;
      if ( numExits <= 0 ) { throw XPaletteException( 1004, "'numExits' must be number above zero" ); }

      entry = item->FindEntry( "file" );
      if ( entry.IsNull() ) { throw XPaletteException( 1005, "Missing field 'file'" ); }
      RString filename = entry->GetValue();

      entry = item->FindEntry( "radius" );
      if ( entry.IsNull() ) { throw XPaletteException( 1005, "Missing field 'radius'" ); }
      double radius = *entry;

      RString forcedTexture;
      entry = item->FindEntry( "forcedTexture" );
      if ( !entry.IsNull() ) { forcedTexture = entry->GetValue(); }

      ParamEntryPtr e_type = item->FindEntry( "type" );
      if ( e_type.IsNull() ) { throw XPaletteException( 1006, "Missing field 'type'" ); }
      if ( e_type->GetSize() != numExits ) { throw XPaletteException( 1007, "Inconsistent count of items in array 'type'" ); }

      ParamEntryPtr e_width = item->FindEntry( "size" );
      if ( e_width.IsNull() ) { throw XPaletteException( 1008, "Missing field 'size'" ); }
      if ( e_width->GetSize() != numExits ) { throw XPaletteException( 1009, "Inconsistent count of items in array 'size'" ); }

      ParamEntryPtr e_azimut = item->FindEntry( "azimut" );
      if ( e_azimut.IsNull() ) { throw XPaletteException( 1010, "Missing field 'azimut'" ); }
      if ( e_azimut->GetSize() != numExits ) { throw XPaletteException( 1011, "Inconsistent count of items in array 'azimut'" ); }

      XPaletteItem itm;
      itm.m_model = filename;
      itm.m_radius = radius;
      itm.m_forcedTexture = forcedTexture;

      for ( int i = 0; i < numExits; ++i )
      {
        itm.m_exits.Add( XPaletteItemExit( e_type->operator[]( i ), e_width->operator[]( i ), e_azimut->operator[]( i ) ) );
      }

      Add( itm );
      ++i;
    }
  }
  while ( true );

  entry = xroadclass->FindEntry( "Parameters" );
  if ( entry.IsNull() )    { throw XPaletteException( 1020, "Cannot find 'Parameters' class" ); }
  if ( !entry->IsClass() ) { throw XPaletteException( 1021, "'Parameters' must be class" ); }
  ParamClassPtr xparams = entry->GetClassInterface();

  entry = xparams->FindEntry( "genOutputPath" );
  if ( entry.IsNull() )
  {
    LOGF( Warn ) << "Missing 'genOutputPath' in xpallette file: " << filename << " - generator will not operate";
  }
  else
  {
    m_genOutputPath = entry->GetValue();

    entry = xparams->FindEntry( "dirGran" );
    if ( entry.IsNull() ) { XPaletteException( 1023, "Missing field 'dirGran'" ); }
    m_dirGran = *entry;
    if ( m_dirGran < 0 ) { throw XPaletteException( 1024, "'dirGran' must be number above zero" ); }

    entry = xparams->FindEntry( "sizeGran" );
    if ( entry.IsNull() ) { XPaletteException( 1023, "Missing field 'sizeGran'" ); }
    m_sizeGran = *entry;
    if ( m_sizeGran < 0 ) { throw XPaletteException( 1024, "'sizeGran' must be number above zero" ); }

    entry = xparams->FindEntry( "genBasePath" );
    if ( entry.IsNull() ) { XPaletteException( 1025, "Missing field 'genBasePath'" ); }
    m_genBasePath = entry->GetValue();
  }

  entry = xparams->FindEntry( "maxSizeDeviation" );
  if ( entry.IsNull() ) { XPaletteException( 1026, "Missing field 'maxSizeDeviation'" ); }
  m_maxSizeDeviation = *entry;

  entry = xparams->FindEntry( "maxAzimutDeviation" );
  if ( entry.IsNull() ) { XPaletteException( 1027, "Missing field 'maxAzimutDeviation'" ); }
  m_maxAzimutDeviation = *entry;

  entry = xparams->FindEntry( "nextUID" );
  if ( entry.IsNull() ) 
  {
    m_nextUnique = 1;
  }
  else 
  {
    m_nextUnique = *entry;
  }
}

//-----------------------------------------------------------------------------

void XPalette::SavePalette( const char* filename )
{
  ParamFile pfile;

  ParamClassPtr cls = pfile.AddClass( "Crossroads" );
  for ( int i = 0, cnt = Size(); i < cnt; ++i )
  {
    const XPaletteItem& it = operator[]( i );
    char buff[20];
    sprintf( buff, "Item%04d", i );
    ParamClassPtr itm = cls->AddClass( buff );
    itm->Add( "numExits", it.m_exits.Size() );
    itm->Add( "file", it.m_model );
    itm->Add( "radius", (float)it.m_radius );
    if ( it.m_forcedTexture.GetLength() != 0 )
    {
      itm->Add( "forcedTexture", it.m_forcedTexture );
    }
    ParamEntryPtr e_type = itm->AddArray( "type" );
    ParamEntryPtr e_width = itm->AddArray( "size" );
    ParamEntryPtr e_azimut = itm->AddArray( "azimut" );
    for ( int i = 0, cnt = it.m_exits.Size(); i < cnt; ++i )
    {
      e_type->AddValue( it.m_exits[i].m_roadType );
      e_width->AddValue( (float)it.m_exits[i].m_width );
      e_azimut->AddValue( (float)it.m_exits[i].m_azimut );
    }
  }

  ParamClassPtr params = cls->AddClass( "Parameters" );
  if ( m_genOutputPath.GetLength() ) 
	{
    params->Add( "genOutputPath", m_genOutputPath );
    params->Add( "genBasePath", m_genBasePath );
    params->Add( "dirGran", (float)m_dirGran );
    params->Add( "sizeGran", (float)m_sizeGran );
  }

  params->Add( "maxAzimutDeviation", (float)m_maxAzimutDeviation );
  params->Add( "maxSizeDeviation", (float)m_maxSizeDeviation );
  params->Add( "nextUID", m_nextUnique );

  LSError err = pfile.Save( filename );
  if ( err ) { throw XPaletteException( err, RString( "Unable to save to file: ", filename ) ); }    
}

//-----------------------------------------------------------------------------

XPalBestResult XPalette::FindBest( const XPaletteItem& item, double maxRadius, bool allowCreate )
{
  XPalBestResult curBest;

  for ( int i = 0; i < Size(); ++i )
  {
    XPalBestResult x = operator[]( i ).FindBest( item, maxRadius, m_maxAzimutDeviation, m_maxSizeDeviation );
    if ( x.m_score > curBest.m_score ) { curBest = x; }
  }
  
  //no best item - create it
  if ( curBest.m_xroad == 0 )
  {
    if ( allowCreate )
    {
      curBest = CreateBestXRoad( item, maxRadius );
    }
    else
		{
      curBest = XPalBestResult( &item, 0, 0, 1, 1, true );
		}
	}
  else if ( curBest.m_score == 0 )
	{
    if ( allowCreate )
		{
      curBest = CreateBestXRoad( item, maxRadius );
		}
    else
		{
      curBest.m_created = true;
		}
	}
  
  return (curBest);
}

//-----------------------------------------------------------------------------

XPalBestResult XPaletteItem::FindBest( const XPaletteItem& item, double maxRadius, double maxAzimutDeviation,
                                       double maxSizeDeviation )
{
  if ( m_exits.Size() != item.m_exits.Size() )  { return (XPalBestResult( this, 0, 0, 1, 0, true )); }
  if ( m_forcedTexture!= item.m_forcedTexture ) { return (XPalBestResult( this, 0, 0, 1, 0, true )); }
  double bestscore = 0.0;
  double bestazimut;
  double bestscale;
  int bestorder = -1;
  for ( int i = 0, cnt = m_exits.Size(); i < cnt ; ++i )
  {
    FindRotRes res = FindBestRotated( item, maxRadius, maxAzimutDeviation, maxSizeDeviation, i );
    if ( res.m_score > bestscore )
    {
      bestscore  = res.m_score;
      bestazimut = res.m_rotation;
      bestscale  = res.m_scale;
      bestorder  = i;
    }
  }

  if ( bestorder >= 0 )
	{
    return (XPalBestResult( this, bestorder, bestazimut, bestscale, bestscore, false ));
	}
  else
	{
    return (XPalBestResult( this, 0, 0, 1, 0, true ));
	}
}

//-----------------------------------------------------------------------------

XPaletteItem::FindRotRes XPaletteItem::FindBestRotated( const XPaletteItem& item, double maxRadius, 
                                                        double maxAzimutDeviation, double maxSizeDeviation, 
                                                        int rotation )
{
  double az_ex = 0.0;
  double az_ex2 = 0.0;
  double sz_ex = 0.0;
  double sz_ex2 = 0.0;
  for ( int i = 0, cnt = m_exits.Size(); i < cnt; ++i )
  {
    XPaletteItemExit& aa = m_exits[(i + rotation) % cnt];
    const XPaletteItemExit& tt = item.m_exits[i];
    ///remove imposible state
    if ( aa.m_roadType != tt.m_roadType )
    {
      return (FindRotRes( 0, 0, 0 ));
    }

    double diff = tt.m_azimut - aa.m_azimut;
    while ( diff > 180.0 )  { diff -= 360.0; }
    while ( diff < -180.0 ) { diff += 360.0; }
    az_ex  += diff;
    az_ex2 += diff * diff;

    diff = tt.m_width / aa.m_width;
    sz_ex  += diff;
    sz_ex2 += diff * diff;
  }
  
  az_ex  /= (double)m_exits.Size();
  az_ex2 /= (double)m_exits.Size();
  double az_dev = sqrt( az_ex2 - az_ex * az_ex );    
    
  sz_ex  /= (double)m_exits.Size();
  sz_ex2 /= (double)m_exits.Size();
  double sz_dev = sqrt( sz_ex2 - sz_ex * sz_ex );

  //test if calcuted scale is below limited scale
  if ( sz_ex * m_radius <= maxRadius )
  {
    if ( az_dev <= maxAzimutDeviation && sz_dev <= maxSizeDeviation )
    {
      double score = exp( -az_dev ) * exp( -sz_dev * 10 );
      return (FindRotRes( score, az_ex, sz_ex ));
    }
  }
  
  return (FindRotRes( 0, 0, 0 ));
}

//-----------------------------------------------------------------------------

static double FreeAzimut( char* usedAngles, int anglesAvail, double angle )
{
  while ( angle < 0.0 )   { angle += 360.0; }
  while ( angle > 360.0 ) { angle -= 360.0; }
  double step = 360.0 / anglesAvail;
  int idx = toInt( floor( angle / step + 0.5 ) );
  if ( idx >= anglesAvail ) { idx -= anglesAvail; }

  bool swapper = false;
  int offset = 0;
  int center = idx;
  while ( usedAngles[idx] && offset < anglesAvail )
  {
//        swapper = !swapper;
/*        if ( swapper ) */offset++;
    idx = center + offset;
    while ( idx < 0 ) 
    {
      idx += anglesAvail;
    }
  }
  
  if ( offset >= anglesAvail )
	{
    return (angle);
	}
  else
  {
    usedAngles[idx] = true;
    angle = idx * step;
    return (angle);
  }
}

//-----------------------------------------------------------------------------

static int CompareFunct( const XPaletteItemExit* e1, const XPaletteItemExit* e2 )
{
    return (e1->m_azimut > e2->m_azimut) - (e1->m_azimut < e2->m_azimut);
}

//-----------------------------------------------------------------------------

XPalBestResult XPalette::CreateBestXRoad( const XPaletteItem& item, double maxRadius )
{
  XPaletteItem wkitm;

  double maxWd = 0.0;

  for ( int i = 0, cnt = item.m_exits.Size(); i < cnt; ++i )
  {
    maxWd = __max( floor( item.m_exits[i].m_width / m_sizeGran + 0.5 ), maxWd );
  }
    
  double angleStep = atan2( maxWd, item.m_radius )* 180 / 3.1415926535;

  int anglesAvail = (int)floor( 360.0 / angleStep );
  char* usedAngles = (char*)alloca( anglesAvail );
  memset( usedAngles, 0, anglesAvail );

  for ( int i = 0, cnt = item.m_exits.Size(); i < cnt; ++i )
  {
    double azimut = floor( item.m_exits[i].m_azimut / m_dirGran + 0.5 ) * m_dirGran;
    azimut = FreeAzimut( usedAngles, anglesAvail, azimut );

    wkitm.m_exits.Add( XPaletteItemExit( item.m_exits[i].m_roadType,
                                         floor( item.m_exits[i].m_width / m_sizeGran + 0.5 ) * m_sizeGran,
                                         azimut ) );
  }
    
//    QSort( wkitm._exits, CompareFunct );

  wkitm.m_radius = item.m_radius;
  wkitm.m_forcedTexture = item.m_forcedTexture;

  XPalBestResult test = FindBest( wkitm, maxRadius, false );

  if ( !test.m_created ) { return (test); }

  wkitm.BuildModelName( m_genOutputPath, m_genBasePath, m_nextUnique++ );    
    
  int pos = Add( wkitm );

  //TODO: Create model.
  
  return (XPalBestResult( &(*this)[pos], 0, 0, 1, 1, true ));
}

//-----------------------------------------------------------------------------

static RString PrvniVelkeDalsiMale( RString text )
{
  int cnt = text.GetLength();
  if ( cnt == 0 ) { return (text); }
  char* buff = text.MutableData();
  buff[0] = toupper( buff[0] );
  for ( int i = 1; i < cnt; ++i )
  {
    buff[i] = tolower( buff[i] );
  }
  return (text);
}

//-----------------------------------------------------------------------------

RString XPaletteItem::BuildModelName(const char* genOutputPath, const char* genBasePath, int unique )
{
  std::ostrstream namebuff;

  namebuff << m_exits.Size();

  std::set< RStringI > names;
  std::set< int > azimuts;
  std::set< int > widths;

  for ( int i = 0, cnt = m_exits.Size(); i < cnt; ++i )
  {
    RStringI name = PrvniVelkeDalsiMale( m_exits[i].m_roadType );        
    
    if ( names.find( name ) == names.end() ) 
    {
      names.insert( name );
      if ( name.GetLength() > 4 ) { name = name.Mid( 0, 4 ); }
      namebuff << name.Data();
    }
  }
  
  for ( int i = 0, cnt = m_exits.Size(); i < cnt; ++i )
  {
    int a = (int)floor( m_exits[i].m_azimut );
    if ( azimuts.find( a ) == azimuts.end() ) 
    {
      azimuts.insert( a );
      namebuff << 'A' << a;
    }
  }
  
  for ( int i = 0, cnt = m_exits.Size(); i < cnt; ++i )
  {
    int w = (int)floor( m_exits[i].m_width * 10 );
    if ( widths.find( w ) == widths.end() ) 
    {
      widths.insert( w );
      namebuff << 'W' << w;
    }
  }
  
  namebuff << 'r' << floor( m_radius * 100 );
  namebuff << 'i' << unique;
  namebuff << ".p3d" << std::ends;

  Pathname pth;
  pth.SetDirectory( genOutputPath );
  pth.SetFilename( namebuff.str() );
  namebuff.freeze( 0 );
  m_model = pth.FullToRelativeProjectRoot( pth, genBasePath );

  return (RString( pth.GetFullPath() ));
}

//-----------------------------------------------------------------------------

RString XPalette::PrepareGenData( const XPaletteItem& item ) const
{
  ParamFile pfile;
  ParamClassPtr common = pfile.AddClass( "Common" );
  common->Add( "textureBase", m_genBasePath );
  common->Add( "levels", 1 );
  common->Add( "textureSize", 2048 );

  ParamClassPtr item001 = pfile.AddClass( "Item001" );
  item001->SetBase( "Common" );
  Pathname texture = m_genBasePath + item.m_model;
  texture.SetDirectory( RString( texture.GetDirectoryWithDrive(), "data\\" ) );
  texture.SetExtension( ".tga" );
  texture.CreateFolder();
  item001->Add( "output", m_genBasePath + item.m_model );
  if ( item.m_forcedTexture.GetLength() )
  {
    item001->Add( "texture", m_genBasePath + item.m_forcedTexture );
    item001->Add( "textureExists", 1 );
  }
  else
  {
    item001->Add( "texture", texture.GetFullPath() );
    item001->Add( "textureExists", 0 );
  }
  
  ParamEntryPtr ends = item001->AddArray( "RoadEnds" );
  for ( int i = 0; i < item.m_exits.Size(); ++i )
  {
    IParamArrayValue* endinfo = ends->AddArrayValue();
    PrepareGenDataExportXRoadEnd( endinfo, item.m_exits[i], item.m_radius );
  }
  
  QOStrStream out;
  pfile.Save( out, 0, "\r\n" );
  out.put( 0 );
  
  return (out.str());
}

//-----------------------------------------------------------------------------

static void AppendVector( IParamArrayValue* endinfo, double x, double y, double z ) 
{
  IParamArrayValue* vector = endinfo->AddArrayValue();
  vector->AddValue( (float)x );
  vector->AddValue( (float)y );
  vector->AddValue( (float)z );
}

//-----------------------------------------------------------------------------

void XPalette::PrepareGenDataExportXRoadEnd( IParamArrayValue* endinfo, const XPaletteItemExit& exitNfo, 
                                             double radius ) const 
{
  double a = exitNfo.m_azimut * 3.1415926535 / 180.0;
  //vector Relative to center
  double xr = sin( a );
  double yr = cos( a );
  //vector Position (relative to center)
  double xp = xr * radius;
  double yp = yr * radius;
  //vector direction, inside to center
  double xd = -xr;
  double yd = -yr;
  //strength
  double strength = radius * 0.5;
  //relative to one side
  double xs = -yd * exitNfo.m_width * 0.5;
  double ys = +xd * exitNfo.m_width * 0.5;

  double xp1 = xp + xs;
  double yp1 = yp + ys;
  
  double xp2 = xp - xs;
  double yp2 = yp - ys;

  AppendVector( endinfo, xp1, yp1, 0 );
  AppendVector( endinfo, xp2, yp2, 0 );
  AppendVector( endinfo, xd, yd, 0 );
  AppendVector( endinfo, xd, yd, 0 );
  endinfo->AddValue( (float)strength );
}

//-----------------------------------------------------------------------------

RString XPalette::s_generatorExe;

//-----------------------------------------------------------------------------

struct IOWatcherData
{
  HANDLE m_process;
  HANDLE m_otherSideOut, m_otherSideIn;
};

//-----------------------------------------------------------------------------

static void IOWaitcher( void* data ) 
{
  IOWatcherData* watch = reinterpret_cast< IOWatcherData* >( data );
  WaitForSingleObject( watch->m_process, INFINITE );
  CloseHandle( watch->m_otherSideIn );
  CloseHandle( watch->m_otherSideOut );
}

//-----------------------------------------------------------------------------

int XPalette::GenerateP3D( const XPaletteItem& xitem ) const
{
  RString genData = PrepareGenData( xitem );

  HANDLE mySideOut, mySideIn;
  HANDLE otherSideOut, otherSideIn;

  CreatePipe( &mySideIn, &otherSideOut, 0, 0 );
  CreatePipe( &otherSideIn, &mySideOut, 0, 0 );

/*    SECURITY_ATTRIBUTES secattr;
    ZeroMemory( &secattr, sizeof( secattr ) );
    secattr.bInheritHandle = TRUE;
    secattr.nLength = sizeof( secattr );*/

  DuplicateHandle( GetCurrentProcess(), otherSideIn,
                   GetCurrentProcess(), &otherSideIn, 0, TRUE,
                   DUPLICATE_CLOSE_SOURCE | DUPLICATE_SAME_ACCESS );

  DuplicateHandle( GetCurrentProcess(), otherSideIn,
                   GetCurrentProcess(), &otherSideIn, 0, TRUE,
                   DUPLICATE_CLOSE_SOURCE | DUPLICATE_SAME_ACCESS);

  STARTUPINFO nfo;
  ZeroMemory( &nfo, sizeof( nfo ) );
  nfo.cb = sizeof( nfo );
  nfo.dwFlags = STARTF_USESTDHANDLES;
  nfo.hStdInput = otherSideIn;
  nfo.hStdOutput = otherSideOut;
  nfo.hStdError = GetStdHandle( STD_ERROR_HANDLE );
  PROCESS_INFORMATION pi;
  
  BOOL ok = CreateProcess( s_generatorExe.MutableData(), s_generatorExe.MutableData(),
                           NULL, NULL, TRUE, NORMAL_PRIORITY_CLASS | CREATE_NO_WINDOW,
                           0, 0, &nfo, &pi );

  if ( ok == FALSE )
  {
    LOGF( Error ) << "Cannot start helper app: " << s_generatorExe.Data() << " - Error code: " << GetLastError();
    return (-1);
  }

  CloseHandle( pi.hThread );
  IOWatcherData watch;
  watch.m_process = pi.hProcess;
  watch.m_otherSideIn = otherSideIn;
  watch.m_otherSideOut = otherSideOut;
  _beginthread( &IOWaitcher, 0, &watch );

  DWORD arrived;
  WriteFile( mySideOut, genData.Data(), genData.GetLength(), &arrived, 0 );
  CloseHandle( mySideOut );

  char buff[256];
  RString linedata;
  while ( ReadFile( mySideIn, buff, 255, &arrived, 0) && arrived )
  {
    buff[arrived] = 0;
    const char* c = strchr( buff, '\r' );
    while ( c )
    {
      strcpy( const_cast< char* >( c ), c + 1 );
      c = strchr( c, '\r' );
    }
    const char* d =  buff;
    c = strchr( d, '\n' );
    while ( c ) 
		{
      *const_cast< char* >( c ) = 0;
      linedata = linedata + d;
      LOGF( Info ) << linedata.Data();
      linedata = "";
      d = c + 1;
      c = strchr( d, '\r' );
    }
    linedata = linedata + d;
  }

  CloseHandle( mySideIn );
  DWORD exitCode;
  GetExitCodeProcess( pi.hProcess, &exitCode );
  CloseHandle( pi.hProcess );
  
  return (exitCode);
}

//-----------------------------------------------------------------------------

int XPalette::GenerateP3DWithLog( const XPaletteItem& xitem ) const
{
  RString name = m_genBasePath + xitem.m_model;

  HANDLE h = ::CreateFile( name, 0, 0, 0, OPEN_EXISTING, 0, 0 );
  if ( h != INVALID_HANDLE_VALUE )
  {
    LOGF( Debug ) << "Using existing xroad: " << xitem.m_model.Data();
    return (0);
  }

  LOGF( Progress ) << "Generating new xroad: " << xitem.m_model.Data();
  int res = GenerateP3D( xitem );
  if ( res )
	{
    LOGF( Progress ) << "Generating exites with error code: " << res ;
  }
  else
	{
    LOGF( Progress ) << "Successfully done - exit code:" << res;
	}

  return (res);
}

//-----------------------------------------------------------------------------
