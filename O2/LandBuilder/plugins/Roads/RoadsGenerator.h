//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>
#include "RoadsGeneratorHelpers.h"

//-----------------------------------------------------------------------------

using std::string;

//-----------------------------------------------------------------------------

using namespace RG_Help;

//-----------------------------------------------------------------------------

class RoadsGenerator : public LandBuilder2::IBuildModule
{
	bool    m_firstRun;
	bool    m_allRoadsLoaded;
	RString m_modelsPath;
	RString m_texturesPath;
	size_t  m_maskSize;

	TList< Road >      m_roadsList;
	TList< Crossroad > m_crossroadsList;

	ofstream m_logFile;

  string m_version;

public:
  RoadsGenerator();
  virtual ~RoadsGenerator();

	virtual void Run( IMainCommands* cmdIfc );
  virtual void OnEndPass( IMainCommands* cmdIfc );

private:
	// ectracts from the list of roads the candidate crossroads 
	// return false if there is any crossroad with more than 4 roads with the
	// same level, in this case failIndex contains the index of the crossroad
	// in the list of crossroads
	bool CreateCrossroadList( size_t crossRoadResolution, int* failIndex );

	// generates all crossroads' contours
	void GenerateCrossroadsContour();

	// moves the ends of all roads to the new positions
	void AdjustRoadsEnds();

	// creates the splines for each road
	void GenerateSplines();

	// generates the contour for every road's model
	void GenerateRoadModelsContours();

	// matches the roads contours to the crossroads' ones
	void MatchRoadsToCrossroads( double overlap );

	// generates and saves the roads p3d models
	// also generates the istances to the models to be saved
	// in the lbt file
	void GenerateRoadsP3DModels( IMainCommands* cmdIfc );

	// generates and saves the crossroads p3d models
	// also generates the istances to the models to be saved
	// in the lbt file
	void GenerateCrossroadsP3DModels( IMainCommands* cmdIfc );

	// extends the roads' spline to arrive to the crossroads center
	void ExtendRoadSplinesToCrossroadsPosition();

	// generates and saves the road network mask bitmap file
	// if the size is greater than 8192 pixels, splits the mask
	// in a series of file with size 8192 pixels
	void GenerateMask( IMainCommands* cmdIfc );

	// exports to dxf file
	void ExportDxf();
};

//-----------------------------------------------------------------------------
