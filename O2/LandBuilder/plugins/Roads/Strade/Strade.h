//-----------------------------------------------------------------------------
// File: Strade.h
//
// Desc: module to automatically generate road networks
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 / 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "Commons.h"
#include "Crossroads.h"
#include "DXF2007Exporter.h"

//-----------------------------------------------------------------------------

#define LIBRARY_FILE    "libraryFile"
#define MODELS_PATH     "modelsPath"
#define RANDOM_SEED     "randomSeed"
#define USE_SPLINES     "useSplines"
#define CROSS_TYPE      "crossType"
#define DXF_EXPORT      "dxfExport"
#define VISITOR_VERSION "visitorVersion"
#define ROAD_TYPE  "type"
#define ROAD_WIDTH "width"

//-----------------------------------------------------------------------------

class CStrade : public LandBuilder2::IBuildModule
{
	// internal use flags
	// {
	bool   m_allRoadsLoaded;
	size_t m_shapesCount;
	// }

	// the path containing LandBuilder
	String m_workingPath;
	
	// the path to the folder which will contain the generated models
	String m_modelsPath;
	// the path to the folder which will contain the textures of the models
	String m_texturesPath;

	// the name of the file containing the library of road types
	String m_libraryFilename;

	// the name of the file containing the exported dxf
	String m_dxfFilename;

	// logger to file
	ofstream m_logFile;

	// the random seed
	int m_randomSeed;

	// whether or not to use splines in place
  // of the original polylines
	bool m_useSplines;

	// the type of crossroads to be generated
	ECrossroadType m_crossroadType;

	// set to true to export to dxf
	bool m_dxfExport;

	// exporter to dxf
	CDXF2007Exporter m_dxfExporter;

	// the library of road types
	CRoadTypesLibrary m_roadTypesLibrary;

	// the library of the crossroad templates
	CCrossroadTemplatesLibrary m_crossroadTemplatesLibrary;

	// the list of roads
	RoadsList m_roads;

	// the list of crossroads
	CrossroadsList m_crossroads;

	// the list of joints
	JointsList m_joints;

  // the visitor version
  size_t m_visitorVersion;

  // an error occurred
  bool m_aborted;

public:
  CStrade();
  virtual ~CStrade();
  
  virtual void Run( IMainCommands* cmdIfc );
  virtual void OnEndPass( IMainCommands* cmdIfc );

private:
	// Extracts from the crossroads list all the crossroads where there are
  // only two roads of the same type and merges the two roads.
	// Returns the number of removed roads.
  size_t MergeRoadsInCross2();

	// Extracts from the crossroads list all the crossroads where there are
  // three roads, and at least two of them are of the same type, and merges 
  // the two roads.
	// Returns the number of removed roads.
  size_t MergeRoadsInCross3();

	// Extracts from the crossroads list all the crossroads where there are
  // four roads, and at least two of them are of the same type, and merges 
  // the two roads.
	// Returns the number of removed roads.
  size_t MergeRoadsInCross4();

	// Create the crossroads list and modifies the roads list where needed.
	// Returns the number of removed roads.
	size_t CreateCrossroadsList();

	// Removes all invalid roads from the roads list.
	// Returns the number of removed roads.
	size_t RemoveInvalidRoads();

	// Splits in two parts all the roads having a crossroads in both ends
	// and add a joint between them
	// Returns the number of splitted roads.
	size_t SplitConstrainedRoads();

  // Force all the roads to substitute their original polyline with
  // its spline
  void SplineRoads();

	// Exports objects to lbt file
	void ExportCreatedObjects( IMainCommands* cmdIfc );
};

//-----------------------------------------------------------------------------
