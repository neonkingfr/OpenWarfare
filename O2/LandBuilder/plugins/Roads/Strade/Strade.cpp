//-----------------------------------------------------------------------------
// File: Strade.cpp
//
// Desc: module to automatically generate road networks
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 / 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Strade.h"

#include "LBRoads.h"

//-----------------------------------------------------------------------------

static LandBuilder2::RegisterModule< CStrade > strade( "Strade" );

//-----------------------------------------------------------------------------

CStrade::CStrade()
: m_allRoadsLoaded( false )
, m_shapesCount( 0 )
, m_randomSeed( 1 )
, m_useSplines( false )
, m_crossroadType( CT_RightAngles )
, m_dxfExport( false )
, m_aborted( false )
{
}

//-----------------------------------------------------------------------------

CStrade::~CStrade()
{
  // closes log file
  if ( !m_logFile.fail() ) { m_logFile.close(); } 
}

//-----------------------------------------------------------------------------

void CStrade::Run( IMainCommands* cmdIfc )
{
  if ( m_aborted ) { return; }

  RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
  if ( taskName != m_currTaskName )
  {
    m_shapesCount    = 0;
    m_allRoadsLoaded = false;
    m_currTaskName   = taskName;
  }

  if ( m_allRoadsLoaded ) { return; }

  ++m_shapesCount;

  LandBuilder2::ParamParser params( cmdIfc );

  // this part contains all the "global" variables and it is run only the
  // first time we enter this module
  if ( m_shapesCount == 1 )
  {
    // sets the working path
    m_workingPath = cmdIfc->GetCurrentTask()->GetWorkFolder();

    // opens log file
    m_logFile.open( m_workingPath + "\\Strade.log", std::ios::out | std::ios::trunc );
    if ( m_logFile.fail() )
    {
      LOGF( Error ) << "Unable to open the log file.";
    }

    LOGF( Info ) << "                                                           ";
    LOGF( Info ) << "===========================================================";
    LOGF( Info ) << "Strade 1.2.5                                               ";
    LOGF( Info ) << "===========================================================";
    m_logFile << "============" << endl;
    m_logFile << "Strade 1.2.5" << endl;
    m_logFile << "============" << endl << endl;

    // try to load the road types library
    try 
    {
      m_libraryFilename = params.GetTextValue( LIBRARY_FILE );
      if ( !m_roadTypesLibrary.LoadFromCfgFile( m_libraryFilename, m_logFile ) ) 
      {
        m_logFile << "Module aborted." << endl;
        m_aborted = true;
        return;
      }
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      LOGF( Error ) << "Missing Library config file name.";
      m_logFile << "ERROR: Missing or wrong \"" << LIBRARY_FILE << "\" field in class Parameters." << endl ;
      m_logFile << "Module aborted." << endl;
      m_aborted = true;
      return;
    }

    m_logFile << "-------------------------------------------------------------" << endl;
    m_logFile << "Loaded road types:" << endl;
    m_logFile << "-------------------------------------------------------------" << endl;
    for ( size_t i = 0, cntI = m_roadTypesLibrary.Size(); i < cntI; ++i )
    {
      m_logFile << m_roadTypesLibrary.Get( i )->GetName() << endl;
    }
    m_logFile << "-------------------------------------------------------------" << endl << endl;

    // sets the models path
    try 
    {
      m_modelsPath = params.GetTextValue( MODELS_PATH );
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      m_modelsPath = "";
    }

    if ( m_modelsPath.IsEmpty() )
    {
      m_logFile << "WARNING: Missing \"" << MODELS_PATH << "\" field in class Parameters." << endl ;
      m_modelsPath = "c:\\ca\\Strade";
      m_logFile << "         Assuming \"" << m_modelsPath << "\" as default." << endl << endl ;
    }
    else if ( m_modelsPath.Find( ':' ) == -1 )
    {
      m_logFile << "WARNING: Wrong \"" << MODELS_PATH << "\" field in class Parameters." << endl ;
      if ( m_modelsPath[0] == '\\' )
      {
        m_modelsPath = "c:" + m_modelsPath;
      }
      else
      {
        m_modelsPath = "c:\\" + m_modelsPath;
      }
      m_logFile << "         Assuming \"" << m_modelsPath << "\" as default." << endl << endl ;
    }

    // removes eventual trailing slashes
    while ( m_modelsPath[m_modelsPath.GetLength() - 1] == '\\' )
    {
      m_modelsPath = m_modelsPath.Substring( 0, m_modelsPath.GetLength() - 1 );
    }

    // sets the texture path
    m_texturesPath = m_modelsPath + "\\data";

    m_logFile << "Models will be saved in: \"" << m_modelsPath << "\"" << endl ;
    m_logFile << "Textures will be saved in: \"" << m_texturesPath << "\"" << endl << endl;

    // Creates the target folders
    Pathname::CreateFolder( m_texturesPath );

    // *************************************
    // TODO:
    //
    // Clean the folders if already existing
    // *************************************

    // gets the random seed
    try
    {
      m_randomSeed = params.GetIntValue( RANDOM_SEED );
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      m_logFile << "WARNING: Missing \"" << RANDOM_SEED << "\" field in class Parameters." << endl;
      m_randomSeed = 1;
      m_logFile << "         Assuming \"" << m_randomSeed << "\" as default." << endl << endl;
    }

    m_logFile << "Using randomSeed = " << m_randomSeed << endl << endl;
    srand( m_randomSeed );

    // gets the visitor version
    m_visitorVersion = 3;
    try
    {
      m_visitorVersion = params.GetIntValue( VISITOR_VERSION );
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      m_logFile << "WARNING: Missing \"" << VISITOR_VERSION << "\" field in class Parameters." << endl;
      m_logFile << "         Assuming \"" << m_visitorVersion << "\" as default." << endl << endl;
    }

    if ( m_visitorVersion != 3 && m_visitorVersion != 4 )
    {
      m_visitorVersion = 3;
      m_logFile << "WARNING: Wrong value for \"" << VISITOR_VERSION << "\" field in class Parameters." << endl;
      m_logFile << "         Assuming \"" << m_visitorVersion << "\" as default." << endl << endl;
    }

    // gets the crossroad type
    try 
    {
      int crossType = params.GetIntValue( CROSS_TYPE );
      if ( (crossType < 0) || (crossType >= CT_COUNT) )
      {
        m_logFile << "WARNING: invalid value for \"" << CROSS_TYPE << "\" field in class Parameters." << endl;
        crossType = CT_NoCrossroads;
        m_logFile << "         Assuming \"" << crossType << "\" as default." << endl << endl;
      }
      m_crossroadType = static_cast< ECrossroadType >( crossType );
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      m_logFile << "WARNING: Missing \"" << CROSS_TYPE << "\" field in class Parameters." << endl;
      m_crossroadType = CT_NoCrossroads;
      m_logFile << "         Assuming \"" << m_crossroadType << "\" as default." << endl << endl;
    }

    m_logFile << "Using crossType = " << m_crossroadType << endl << endl;

    // gets the use splines flag
    try 
    {
      String use = params.GetTextValue( USE_SPLINES );
      if ( use == "1" ) { m_useSplines = true; }
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      m_logFile << "WARNING: Missing \"" << USE_SPLINES << "\" field in class Parameters." << endl;
      m_useSplines = false;
      m_logFile << "         Assuming \"No splines\" as default." << endl << endl;
    }

    if ( m_useSplines )
    {
      m_logFile << "Using splines in place of the original polylines." << endl << endl;
    }
    else
    {
      m_logFile << "Using the original polylines." << endl << endl;
    }

    // gets the dxf export flag
    try 
    {
      String dxf = params.GetTextValue( DXF_EXPORT );
      if ( dxf == "1" ) { m_dxfExport = true; }
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      m_logFile << "WARNING: Missing \"" << DXF_EXPORT << "\" field in class Parameters." << endl;
      m_dxfExport = false;
      m_logFile << "         No dxf export will be performed." << endl << endl;
    }

    if ( m_dxfExport )
    {
      m_dxfFilename = m_workingPath + "Strade.dxf";
      m_logFile << "Dxf export will be done in the file: \"" << m_dxfFilename << "\"" << endl << endl;
    }

    // clear the road list;
    m_roads.Clear();
  } // end of "global" settings

  // this part apply to all the shapes coming from the shapefile
  Shapes::IShape* curShape = const_cast< Shapes::IShape* >( cmdIfc->GetActiveShape() );

  // shape validation

  // checks for multipart shapes
  if ( curShape->GetPartCount() > 1 )
  {
    LOGF( Error ) << "Strade module accepts only shapefiles containing non-multipart polylines.";
    m_logFile << "ERROR: Strade module accepts only shapefiles containing non-multipart polylines." << endl;
    m_logFile << "       Shape n: " << m_shapesCount << " is a multipart shape." << endl;
    m_logFile << "Shape skipped." << endl;
    return;
  }

  // checks for known shapes
  const IShapeExtra* extra = curShape->GetInterface< IShapeExtra >();
  if ( !extra ) 
  {
    LOGF( Error ) << "Found unknown shape in shapefile. Cannot get type of that shape.";
    m_logFile << "ERROR: Found unknown shape in shapefile. Cannot get type of that shape." << endl;
    m_logFile << "       Shape n: " << m_shapesCount << " is unknown." << endl;
    m_logFile << "Shape skipped." << endl;
    return;
  }

  // checks for polylines 
  String shapeType = extra->GetShapeTypeName();
  if ( shapeType != String( SHAPE_NAME_POLYLINE ) ) 
  {
    LOGF( Error ) << "Strade module accepts only shapefiles containing non-multipart polylines.";
    m_logFile << "ERROR: Strade module accepts only shapefiles containing non-multipart polylines." << endl;
    m_logFile << "       Shape n: " << m_shapesCount << " is not a polyline." << endl;
    m_logFile << "Shape skipped." << endl;
    return;
  }

  // gets road type from dbf
  String type = "";
  try 
  {
    type = params.GetTextValue( ROAD_TYPE );
  }
  catch ( LandBuilder2::ParamParserException& e ) 
  {
    type = m_roadTypesLibrary.Get( 0 )->GetName();
    LOGF( Error ) << "Missing parameter in dbf: " << e.Message() << " " << e.GetName() << "=" << e.GetValue();
    m_logFile << "WARNING: Missing parameter in dbf: " << e.Message() << " " << e.GetName() << "=" << e.GetValue() << endl;
    m_logFile << "         Shape n: " << m_shapesCount << " misses the \"" << ROAD_TYPE << "\" field in the dbf." << endl;
    m_logFile << "         \"" << type << "\" assumed as type for this shape." << endl << endl;
  }

  // gets the road width from dbf
  Float width = DEF_ROAD_WIDTH;
  try 
  {
    width = static_cast< Float >( params.GetFloatValue( ROAD_WIDTH ) );
  }
  catch ( LandBuilder2::ParamParserException& e ) 
  {
    width = DEF_ROAD_WIDTH;
    LOGF( Error ) << "Missing parameter in dbf: " << e.Message() << " " << e.GetName() << "=" << e.GetValue();
    m_logFile << "WARNING: Missing parameter in dbf: " << e.Message() << " " << e.GetName() << "=" << e.GetValue() << endl;
    m_logFile << "         Shape n: " << m_shapesCount << " misses the \"" << ROAD_WIDTH << "\" field in the dbf." << endl;
    m_logFile << "         Default value assumed as width for this shape: " << width << endl << endl;
  }

  // checks if type is in library
  int libIndex = m_roadTypesLibrary.FindByNameAndWidth( type, width );

  if ( libIndex == -1 )
  {
    int baseLibIndex = m_roadTypesLibrary.FindByNameAndWidth( type, ZERO );

    if ( baseLibIndex == -1 )
    {
      LOGF( Error ) << "Found invalid road type (" << type << "). Using default.";
      m_logFile << "WARNING: Shape n: " << m_shapesCount << " has in the dbf an unknown road type (" << type << ")." << endl;
      type = m_roadTypesLibrary.Get( 0 )->GetName();
      m_logFile << "         \"" << type << "\" assumed as road type for this shape." << endl << endl;

      baseLibIndex = m_roadTypesLibrary.FindByNameAndWidth( type, width );
      if ( baseLibIndex == -1 ) 
      { 
        baseLibIndex = 0; 
      }
      else
      {
        libIndex = baseLibIndex;
      }
    }

    if ( libIndex == -1 )
    {
      const CRoadType* baseRoadType = m_roadTypesLibrary.Get( baseLibIndex );
      CRoadType* roadType = new CRoadType( baseRoadType->GetName(), baseRoadType->GetPrefix(),
                                           baseRoadType->GetMapP3D(), baseRoadType->GetTextures(), 
                                           baseRoadType->GetProbSpecStrTex(), baseRoadType->GetProbSpecCorTex(), 
                                           baseRoadType->GetProbSpecTerTex(), baseRoadType->GetMinSpecDist(), 
                                           baseRoadType->GetCornerResolution(), baseRoadType->GetProximityFactor(),
                                           baseRoadType->GetMatchThreshold() );
      roadType->SetWidth( width );
      m_roadTypesLibrary.Append( roadType );
      libIndex = m_roadTypesLibrary.Size() - 1;
    }
  }

  // creates the road from the shape
  CRoad road( m_roadTypesLibrary.Get( static_cast< size_t >( libIndex ) ), m_useSplines );
  for ( size_t i = 0, cntI = curShape->GetVertexCount(); i < cntI; ++i )
  {
    const Shapes::DVertex& v = curShape->GetVertex( i );
    road.GetPath().AppendVertex( v.x, v.y );
  }

  // searches for duplicates
  bool found = false;
  for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
  {
    const CRoad* listedRoad = m_roads.Get( i ); 
    if ( listedRoad->Equals( road ) || listedRoad->EqualsReverse( road ) )
    {
      found = true;
      break;
    }
  }

  if ( !found ) { m_roads.Append( road ); }
}

//-----------------------------------------------------------------------------

void CStrade::OnEndPass( IMainCommands* cmdIfc )
{
  if ( m_allRoadsLoaded ) { return; }
  if ( m_aborted )        { return; }

  m_allRoadsLoaded = true;

  // we should arrive here only after all shapes have been loaded
  if ( m_roads.Size() > 0 )
  {
    // if we want dxf export opens the export file
    // and writes the original road paths
    if ( m_dxfExport ) 
    {
      m_dxfExporter.Filename( m_dxfFilename ); 

      for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
      {
        m_dxfExporter.WritePolyline( m_roads.Get( i )->GetPath(), "OriginalRoads" );
      }
    }

    DWORD startTime = GetTickCount();

    // Step 1
    // roads pre-processing
    size_t initialVerticesCount = 0;
    size_t finalVerticesCount   = 0;
    for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
    {
      CGPolyline2& path = m_roads.Get( i )->GetPath();
      initialVerticesCount += path.VerticesCount();
      // removes eventual consecutive duplicated vertices
      path.RemoveConsecutiveDuplicatedVertices( TOLERANCE );
      // tries to replace quasi-straight regions with approximating single segments
      path.StraightenWherePossible( TOLERANCE, 0.00125, true );
      finalVerticesCount += path.VerticesCount();
    }

    DWORD endRoadsPreProcessingTime = GetTickCount();

    m_logFile << "-------------------------------------------------------------" << endl;
    m_logFile << "Preprocessing statistics" << endl;
    m_logFile << "-------------------------------------------------------------" << endl;
    m_logFile << "Number of roads: " << m_roads.Size() << endl;
    Float roadsLength = ZERO;
    for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
    {
      roadsLength += m_roads.Get( i )->GetPath().Length();
    }
    m_logFile << "Total length of roads: " << roadsLength / 1000 << " km" << endl;
    m_logFile << endl;
    m_logFile << "Total roads' vertices count before preprocessing: " << initialVerticesCount << endl;
    m_logFile << "Total roads' vertices count after preprocessing : " << finalVerticesCount << endl;
    m_logFile << "-------------------------------------------------------------" << endl << endl;

    // Step 2
    // creates the crossroads list and in the meanwhile checks all roads for validity
    LOGF( Error ) << "Creating crossroads list";
    {
      do
      {
        RemoveInvalidRoads();
      }
      while ( CreateCrossroadsList() > 0 );
    }
    LOGF( Error ) << "Created " << m_crossroads.Size() <<  " crossroads.";
    LOGF( Error ) << "------------------------------------------";
    m_logFile << "Created " << m_crossroads.Size() <<  " crossroads." << endl;
    m_logFile << "Number of roads after crossroads creation: " << m_roads.Size() << endl << endl;

    DWORD endCreationCrossroadsListTime = GetTickCount();

    // Step 3
    // splits all the roads having a crossroads in both ends
    LOGF( Error ) << "Splitting constrained roads";
    size_t splittedRoadsCount;
    {
      splittedRoadsCount = SplitConstrainedRoads();
    }
    LOGF( Error ) << "Splitted " << splittedRoadsCount <<  " roads.";
    LOGF( Error ) << "------------------------------------------";
    m_logFile << "Splitted " << splittedRoadsCount <<  " roads." << endl;
    m_logFile << "Created " << m_joints.Size() <<  " joints." << endl;
    m_logFile << "Number of roads after splitting: " << m_roads.Size() << endl << endl;

    DWORD endSplittingRoadsTime = GetTickCount();

    // Step 4
    // if using splines, force the roads to convert their polylines
    // to the corrisponding splines
    if ( m_useSplines )
    {
      LOGF( Error ) << "Converting to splines";
      {
        SplineRoads();
      }
    }

    DWORD endSpliningRoadsTime = GetTickCount();

    // Step 5
    // builds crossroads models
    // this must be done before creating roads models because during the crossroad
    // models creation roads path could be changed
    LOGF( Error ) << "Building crossroads models";
    {
      for ( size_t i = 0, cntI = m_crossroads.Size(); i < cntI; ++i )
      {
        CCrossroad* crossroad = m_crossroads.Get( i );
        crossroad->BuildModel( m_crossroadTemplatesLibrary );
      }
    }
    LOGF( Error ) << "Built " << m_crossroads.Size() <<  " crossroad models.";
    LOGF( Error ) << "------------------------------------------";
    m_logFile << "Created " << m_crossroadTemplatesLibrary.Size() << " crossroad model templates:" << endl;
    m_logFile << "-------------------------------------------------------------" << endl;
    size_t totalRefCount = 0;
    for ( size_t i = 0, cntI = m_crossroadTemplatesLibrary.Size(); i < cntI; ++i )
    {
      const CCrossroadTemplate* templ = m_crossroadTemplatesLibrary.Get( i );
      size_t templRefCount = templ ->GetReferencesCount();
      if ( templRefCount > 0 )
      {
        totalRefCount += templRefCount;
        char buff[80];
        sprintf( buff, " ( %d )", templRefCount );
        String str = templ->Name() + String( buff );
        m_logFile << str << endl;
      }
    }
    m_logFile << "-------------------------------------------------------------" << endl;
    m_logFile << "Total crossroad models: " << totalRefCount << endl;
    m_logFile << "-------------------------------------------------------------" << endl << endl;

    DWORD endBuildCrossroadModelsTime = GetTickCount();

    // Step 6
    // builds roads models
    LOGF( Error ) << "Building roads models";
    size_t iteration = 0;
    {
      size_t oldRoadPartCount = 0;
      size_t newRoadPartCount = 0;
      for ( size_t i = 0, cntI = m_roadTypesLibrary.Size(); i < cntI; ++i )
      {
        newRoadPartCount += m_roadTypesLibrary.Get( i )->GetTemplates().Size();
      }
      do
      {
        ++iteration;
        oldRoadPartCount = newRoadPartCount;

        {
          // clears eventual previous builds 
          for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
          {
            CRoad* road = m_roads.Get( i );
            if ( road->IsBuilt() )
            {
              road->ClearModels();
            }
          }
        }

        {
          // first: builds roads having a free endpoint and a crossroad in the
          // other endpoint
          for ( size_t i = 0, cntI = m_crossroads.Size(); i < cntI; ++i )
          {
            CCrossroad* crossroad = m_crossroads.Get( i );
            crossroad->BuildRoads();
          }
        }

        {
          // second: builds roads sharing a joint
          for ( size_t i = 0, cntI = m_joints.Size(); i < cntI; ++i )
          {
            CNullJoint* joint = m_joints.Get( i );
            joint->BuildRoads();
          }
        }

        {
          // third: builds all the roads with both free endpoints 
          for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
          {
            CRoad* road = m_roads.Get( i );
            if ( !road->GetStartJoint() && !road->GetEndJoint() )
            {
              road->SetStartDirectionConstraint( road->GetPath().FirstSegment().Direction() );
              road->SetEndDirectionConstraint( road->GetPath().LastSegment().Direction() );
              road->BuildModels();
            }
          }
        }

        newRoadPartCount = 0;
        for ( size_t i = 0, cntI = m_roadTypesLibrary.Size(); i < cntI; ++i )
        {
          newRoadPartCount += m_roadTypesLibrary.Get( i )->GetTemplates().Size();
        }
      }
      while ( newRoadPartCount != oldRoadPartCount );
    }
    m_logFile << "Road part models generation: performed " << iteration << " iterations." << endl << endl;

    size_t unbuiltCount = 0;
    for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
    {
      const CRoad* road = m_roads.Get( i );
      if ( !road->IsBuilt() && !road->IsDegenerated() ) { ++unbuiltCount; }
    }

    size_t degenerateCount = 0;
    for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
    {
      const CRoad* road = m_roads.Get( i );
      if ( road->IsDegenerated() ) { ++degenerateCount; }
    }

    if ( (unbuiltCount > 0) || (degenerateCount > 0) )
    {
      m_logFile << "WARNING !!!!" << endl;
      if ( unbuiltCount > 0 )
      {
        m_logFile << "Unable to build or complete " << unbuiltCount << " (" << 100.0 * unbuiltCount / m_roads.Size() << "%) roads: " << endl;
        m_logFile << "-------------------------------------------------------------" << endl;
        for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
        {
          const CRoad* road = m_roads.Get( i );
          if ( !road->IsBuilt() && !road->IsDegenerated() )
          {
            m_logFile << *road << endl;
          }
        }
      }
      if ( degenerateCount > 0 )
      {
        if ( unbuiltCount > 0 )
        {
          m_logFile << "-------------------------------------------------------------" << endl << endl;
        }
        m_logFile << "Found " << degenerateCount << " (" << 100.0 * degenerateCount / m_roads.Size() << "%) degenerate roads." << endl;
        m_logFile << "-------------------------------------------------------------" << endl;
        for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
        {
          const CRoad* road = m_roads.Get( i );
          if ( road->IsDegenerated() )
          {
            m_logFile << *road << endl;
          }
        }
      }
      m_logFile << "-------------------------------------------------------------" << endl << endl;
    }

    size_t unmatchedCount = 0;
    for ( size_t i = 0, cntI = m_joints.Size(); i < cntI; ++i )
    {
      if ( !m_joints.Get( i )->IsMatched() ) { ++unmatchedCount; }
    }

    if ( unmatchedCount > 0 )
    {
      m_logFile << "WARNING !!!!" << endl;
      m_logFile << "Unable to match " << unmatchedCount << " (" << 100.0 * unmatchedCount / m_joints.Size() << "%) road joints: " << endl;
      m_logFile << "-------------------------------------------------------------" << endl;
      for ( size_t i = 0, cntI = m_joints.Size(); i < cntI; ++i )
      {
        const CNullJoint* joint = m_joints.Get( i );
        if ( !joint->IsMatched() )
        {
          m_logFile << "At: " << *joint << endl;
          m_dxfExporter.WriteCircle( CGPoint2( joint->GetPosition().GetX(), joint->GetPosition().GetY() ), 30.0, "Joints" );
        }
        else
        {
          m_dxfExporter.WriteCircle( CGPoint2( joint->GetPosition().GetX(), joint->GetPosition().GetY() ), 30.0, "JointsMatched" );
        }
      }
      m_logFile << "-------------------------------------------------------------" << endl << endl;
    }

    size_t createdModels = 0;
    for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
    {
      createdModels += m_roads.Get( i )->GetModels().size();
    }
    m_logFile << "Number of road part models after generation = " << createdModels << endl << endl;

    size_t usedTemplatesCountBefore = 0;
    for ( size_t r = 0, cntR = m_roadTypesLibrary.Size(); r < cntR; ++r )
    {
      const CRoadType* roadType = m_roadTypesLibrary.Get( r );
      const CRoadPartTemplatesLibrary& library = roadType->GetTemplates();
      for ( size_t t = 0, cntT = library.Size(); t < cntT; ++t )
      {
        const CRoadPartTemplate* templ = library.Get( t );
        if ( templ->GetReferencesCount() > 0 )
        {
          ++usedTemplatesCountBefore;
        }
      }
    }
    m_logFile << "Number of used road part templates before reduction = " << usedTemplatesCountBefore << endl << endl;

    DWORD endBuildRoadsModelsTime = GetTickCount();

    {
      for ( size_t i = 0, cntI = m_roadTypesLibrary.Size(); i < cntI; ++i )
      {
        m_roadTypesLibrary.Get( i )->GetTemplates().RemoveUnused();
      }
    }

    // Step 7
    // insertion of special models 
    LOGF( Error ) << "Adding special models to road";
    {
      for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
      {
        m_roads.Get( i )->AddSpecialModels();
      }
    }
    DWORD endSpecialRoadModelsTime = GetTickCount();

    {
      for ( size_t i = 0, cntI = m_roadTypesLibrary.Size(); i < cntI; ++i )
      {
        m_roadTypesLibrary.Get( i )->GetTemplates().RemoveUnused();
      }
    }

    // Step 8
    // reduction of models count
    LOGF( Error ) << "Reducing roads";
    {
      for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
      {
        m_roads.Get( i )->ReduceModels();
      }
    }
    size_t reducedModels = 0;
    for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
    {
      reducedModels += m_roads.Get( i )->GetModels().size();
    }
    m_logFile << "Number of road part models after reduction = " << reducedModels << endl;
    m_logFile << "Reduction = " << 100.0 * (createdModels - reducedModels) / createdModels << " %" << endl << endl;

    size_t usedTemplatesCountAfter = 0;
    for ( size_t r = 0, cntR = m_roadTypesLibrary.Size(); r < cntR; ++r )
    {
      const CRoadType* roadType = m_roadTypesLibrary.Get( r );
      const CRoadPartTemplatesLibrary& library = roadType->GetTemplates();
      for ( size_t t = 0, cntT = library.Size(); t < cntT; ++t )
      {
        const CRoadPartTemplate* templ = library.Get( t );
        if ( templ->GetReferencesCount() > 0 )
        {
          ++usedTemplatesCountAfter;
        }
      }
    }
    m_logFile << "Number of used road part templates after reduction = " << usedTemplatesCountAfter << endl;
    m_logFile << "Increment = " << 100.0 * (usedTemplatesCountAfter - usedTemplatesCountBefore) / usedTemplatesCountBefore << " %" << endl << endl;

    DWORD endReductionRoadsModelsTime = GetTickCount();

    for ( size_t i = 0, cntI = m_roadTypesLibrary.Size(); i < cntI; ++i )
    {
      if ( m_roadTypesLibrary.Get( i )->GetWidth() > ZERO )
      {
        size_t totalRefCount = 0;
        String name = m_roadTypesLibrary.Get( i )->ExportName();
        m_logFile << "Road type: " << name << endl;
        m_logFile << "Templates: " << endl;
        m_logFile << "-------------------------------------------------------------" << endl;
        CRoadPartTemplatesLibrary& library = m_roadTypesLibrary.Get( i )->GetTemplates();
        for ( size_t j = 0, cntJ = library.Size(); j < cntJ; ++j )
        {
          const CRoadPartTemplate* templ = library.Get( j );
          size_t templRefCount = templ->GetReferencesCount();
          if ( templRefCount > 0 )
          {
            totalRefCount += templRefCount;
            char buff[80];
            sprintf( buff, " ( %d )", templRefCount );
            String str = templ->Name() + String( buff );
            m_logFile << str << endl;
          }
        }
        m_logFile << "-------------------------------------------------------------" << endl;
        m_logFile << "Total templates for " << name << ": " << library.Size() << endl;
        m_logFile << "Total models for " << name << ": " << totalRefCount << endl;
        m_logFile << "-------------------------------------------------------------" << endl << endl;
      }
    }

    // Final steps
    // exports p3d models 
    LOGF( Error ) << " ";
    LOGF( Error ) << "Exporting models to p3d file";
    LOGF( Error ) << "------------------------------------------";

    {
      // crossroad models
      for ( size_t i = 0, cntI = m_crossroadTemplatesLibrary.Size(); i < cntI; ++i )
      {
        const CCrossroadTemplate* crossroadTemplate = m_crossroadTemplatesLibrary.Get( i );
        if ( crossroadTemplate->GetReferencesCount() > 0 )
        {
          crossroadTemplate->ExportP3D( m_modelsPath );
        }
      }
    }
    {
      // road models
      for ( size_t r = 0, cntR = m_roadTypesLibrary.Size(); r < cntR; ++r )
      {
        const CRoadType* roadType = m_roadTypesLibrary.Get( r );
        const CRoadPartTemplatesLibrary& templLib = roadType->GetTemplates();
        for ( size_t t = 0, cntT = templLib.Size(); t < cntT; ++t )
        {
          const CRoadPartTemplate* roadPartTemplate = templLib.Get( t );
          if ( roadPartTemplate->GetReferencesCount() > 0 )
          {						 
            roadPartTemplate->ExportP3D( m_modelsPath );
          }
        }
      }
    }

    DWORD endP3DModelsExportTime = GetTickCount();

    // exports the created objects into lbt file
    {
      ExportCreatedObjects( cmdIfc );
    }

    DWORD endLbtExportTime = GetTickCount();

    if ( m_dxfExport ) 
    {
      LOGF( Error ) << "Please wait...Exporting to DXF";

//      // writes the modified road paths
//      for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
//      {
//        m_dxfExporter.WritePolyline( m_roads.Get( i )->GetPath(), "ModifiedRoads" );
//      }

      // writes the crossroad models
      for ( size_t c = 0, cntC = m_crossroads.Size(); c < cntC; ++c )
      {
        CCrossroad* crossroad = m_crossroads.Get( c );
        string layer = string( crossroad->GetModel().GetTemplate()->Name().Data() );
        vector< CGPolygon2 > geometry = crossroad->GetModel().Geometry();
        for ( size_t g = 0, cntG = geometry.size(); g < cntG; ++g )
        {
          m_dxfExporter.WritePolygon( geometry[g], layer );
        }
      }

      // writes the road models
      for ( size_t r = 0, cntR = m_roads.Size(); r < cntR; ++r )
      {
        const CRoad* road = m_roads.Get( r );
        const vector< CRoadPartModel >& models = road->GetModels();
        for ( size_t m = 0, cntM = models.size(); m < cntM; ++m )
        {
          string layer = string( models[m].GetTemplate()->Name().Data() );
          vector< CGPolygon2 > geometry = models[m].Geometry();
          m_dxfExporter.WritePolygon( geometry[0], layer );
        }
      }
    }

    DWORD endDxfExportTime = GetTickCount();

    m_logFile << "-------------------------------------------------------------" << endl;
    m_logFile << "Postprocessing statistics" << endl;
    m_logFile << "-------------------------------------------------------------" << endl;
    m_logFile << "Number of roads: " << m_roads.Size() << endl;
    Float roadsModelsLength = ZERO;
    for ( size_t r = 0, cntR = m_roads.Size(); r < cntR; ++r )
    {
      const CRoad* road = m_roads.Get( r );
      const vector< CRoadPartModel >& models = road->GetModels();
      for ( size_t m = 0, cntM = models.size(); m < cntM; ++m )
      {
        roadsModelsLength += models[m].Length();
      }
    }
    m_logFile << "Total length of roads: " << roadsModelsLength / 1000 << " km" << endl;
    m_logFile << "-------------------------------------------------------------" << endl << endl;

    m_logFile << "-------------------------------------------------------------" << endl;
    m_logFile << "Time statistics" << endl;
    m_logFile << "-------------------------------------------------------------" << endl;
    m_logFile << "Polylines preprocessing        = " << endRoadsPreProcessingTime - startTime << " ms" << endl;
    m_logFile << "Crossroads list creation       = " << endCreationCrossroadsListTime - endRoadsPreProcessingTime << " ms" << endl;
    m_logFile << "Constrained roads splitting    = " << endSplittingRoadsTime - endCreationCrossroadsListTime << " ms" << endl;

    if ( m_useSplines )
    {
      m_logFile << "Roads splining                 = " << endSpliningRoadsTime - endSplittingRoadsTime << " ms" << endl;
      m_logFile << "Crossroads models building     = " << endBuildCrossroadModelsTime - endSpliningRoadsTime << " ms" << endl;
    }
    else
    {
      m_logFile << "Crossroads models building     = " << endBuildCrossroadModelsTime - endSplittingRoadsTime << " ms" << endl;
    }

    m_logFile << "Roads models building          = " << endBuildRoadsModelsTime - endBuildCrossroadModelsTime << " ms" << endl;
    m_logFile << "Special roads models insertion = " << endSpecialRoadModelsTime - endBuildRoadsModelsTime << " ms" << endl;
    m_logFile << "Roads models reduction         = " << endReductionRoadsModelsTime - endSpecialRoadModelsTime << " ms" << endl;
    m_logFile << "Export to P3D files            = " << endP3DModelsExportTime - endReductionRoadsModelsTime << " ms" << endl;
    m_logFile << "Export to lbt file             = " << endLbtExportTime - endP3DModelsExportTime << " ms" << endl;
    if ( m_dxfExport ) 
    {
      m_logFile << "Export to dxf                  = " << endDxfExportTime - endP3DModelsExportTime << " ms" << endl;
    }
    m_logFile << "-------------------------------------------------------------" << endl << endl;
  }
}

//-----------------------------------------------------------------------------

size_t CStrade::MergeRoadsInCross2()
{
  // removes from the crossroads list all the candidates with two roads,
  // which are both of the same type, and merges the two roads if the
  // angle they form in greater than or equal to 110 degrees

  size_t removedRoads = 0;

  int c = 0;
  while ( static_cast< size_t >( c ) < m_crossroads.Size() )
  {
    CCrossroad* crossroad = m_crossroads.Get( c );

    if ( crossroad->GetRoads().Size() == 2 )
    {
      CRoad* road0 = crossroad->GetRoads().Get( 0 );
      CRoad* road1 = crossroad->GetRoads().Get( 1 );

      if ( road0 == road1 )
      {
        // this is a loop road.
        // no changes in the roads list

        // creates a new null joint
        CNullJoint joint( crossroad->GetPosition(), road0 );
        joint.GetRoads().Add( road0 );
        CNullJoint* newJoint = m_joints.Append( joint );

        // updates road's endpoints
        road0->SetStartJoint( newJoint );
        road0->SetEndJoint( newJoint );

        // removes the crossroad from the list
        m_crossroads.Remove( static_cast< size_t >( c ) );
        --c;
      }
      else if ( road0->GetType()->ExportName() == road1->GetType()->ExportName() )
      {
        if ( (road0->GetStartJoint() == crossroad) && (road1->GetStartJoint() == crossroad) )
        {
          // if both roads start in the crossroad
          road0->ReverseVerticesOrder();
        }
        else if ( (road0->GetEndJoint() == crossroad) && (road1->GetEndJoint() == crossroad) )
        {
          // if both roads end in the crossroad
          road1->ReverseVerticesOrder();
        }
        else if ( (road0->GetStartJoint() == crossroad) && (road1->GetEndJoint() == crossroad) )
        {
          // if first road starts and second road ends in the crossroad
          CRoad* temp = road0;
          road0 = road1;
          road1 = temp;
        }

        CGVector2 dir0( road0->GetPath().LastSegment().Direction() );
        CGVector2 dir1( CGConsts::PI + road1->GetPath().FirstSegment().Direction() );
        Float cos01 = dir0.Dot( dir1 );

        if ( cos01 <= (Float)(-0.3420201433256) )
        {
          // the angle is greater than or equal to 110 degrees

          // merges the two roads
          road0->GetPath().AppendVertices( road1->GetPath() );

          // updates road's endpoints
          road0->SetEndJoint( road1->GetEndJoint() );

          // updates crossroad, if any
          if ( road0->GetEndJoint() )
          {
            road0->GetEndJoint()->GetRoads().Replace( road1, road0 );
          }

          // removes the merged road
          m_roads.Remove( road1 );
          ++removedRoads;
        }
        else
        {
          // the angle is smaller than 110 degrees

          // updates road's endpoints
          road0->SetEndJoint( NULL );
          road1->SetStartJoint( NULL );
        }

        // removes the crossroad from the list
        m_crossroads.Remove( static_cast< size_t >( c ) );
        --c;
      }
    }
    ++c;
  }

  return (removedRoads);
}

//-----------------------------------------------------------------------------

size_t CStrade::MergeRoadsInCross3()
{
  size_t removedRoads = 0;

  int c = 0;
  while ( static_cast< size_t >( c ) < m_crossroads.Size() )
  {
    CCrossroad* crossroad = m_crossroads.Get( c );
    CRoadsSet& roads = crossroad->GetRoads();

    if ( roads.Size() == 3 )
    {
      // lets all roads to start in this crossroad
	    for ( size_t i = 0, cntI = roads.Size(); i < cntI; ++i )
	    {
		    if ( roads.Get( i )->GetStartJoint() != crossroad )
		    {
			    roads.Get( i )->ReverseVerticesOrder();
		    }
	    }

	    // sorts the roads by the direction of the first segment
	    roads.SortByDirection();

      size_t typesCount = roads.TypesCount();

      int index0 = -1;
      if ( typesCount == 1 )
      {
        // all the three roads are of the same type
        // we consider as main road the alignement of the two roads having the
        // greatest comprised angle 

        Float directions[3];
        for ( size_t i = 0; i < 3; ++i )
        {
          directions[i] = roads.Get( i )->GetPath().FirstSegment().Direction();
        }

        vector< Float > anglesCos;
        for ( size_t i = 0; i < 3; ++i )
        {
          CGVector2 v0;
          CGVector2 v1;
          if ( i < 2 )
          {
            v0 = CGVector2( directions[i + 1] );
            v1 = CGVector2( directions[i] );
          }
          else
          {
            v0 = CGVector2( directions[0] );
            v1 = CGVector2( directions[i] );
          }
          anglesCos.push_back( v0.Dot( v1 ) );
        }

        Float minCos = ONE;
        for ( size_t i = 0, cntI = anglesCos.size(); i < cntI; ++i )
        {
          if ( minCos > anglesCos[i] )
          {
            minCos = anglesCos[i];
            index0 = i;
          }
        }
      }
      else // typesCount == 2
      {
        // two roads are of the same type, the third is of a different type
        // we consider as main road the alignement of the two roads having the same type
        if ( roads.Get( 0 )->GetType() == roads.Get( 1 )->GetType() )
        {
          index0 = 0;
        }
        else if ( roads.Get( 1 )->GetType() == roads.Get( 2 )->GetType() )
        {
          index0 = 1;
        }
        else if ( roads.Get( 2 )->GetType() == roads.Get( 0 )->GetType() )
        {
          index0 = 2;
        }
      }

      if ( index0 >= 0 )
      {
        int index1 = index0 + 1;
        if ( index1 > 2 ) { index1 = 0; }
        int index2 = index1 + 1;
        if ( index2 > 2 ) { index2 = 0; }

        CRoad* road0 = roads.Get( index0 );
        CRoad* road1 = roads.Get( index1 );
        CRoad* road2 = roads.Get( index2 );

        if ( road0 == road1 )
        {
          // this is a loop road.
          // no changes in the roads list

          // updates road's endpoints
          road0->SetStartJoint( NULL );
          road0->SetEndJoint( NULL );
        }
        else
        {
          road0->ReverseVerticesOrder();

          CGVector2 dir0( road0->GetPath().LastSegment().Direction() );
          CGVector2 dir1( CGConsts::PI + road1->GetPath().FirstSegment().Direction() );
          Float cos01 = dir0.Dot( dir1 );

          if ( cos01 <= (Float)(-0.3420201433256) )
          {
            // the angle is greater than or equal to 110 degrees

            // merges the two roads
            road0->GetPath().AppendVertices( road1->GetPath() );

            // updates road's endpoints
            road0->SetEndJoint( road1->GetEndJoint() );

            // updates crossroad, if any
            CJoint* newJoint = road0->GetEndJoint();
            if ( newJoint /*&& (newJoint->Type() == JT_Crossroad)*/ )
            {
              newJoint->GetRoads().Replace( road1, road0 );
            }

            // removes the merged road
            m_roads.Remove( road1 );
            ++removedRoads;
          }
          else
          {
            // the angle is smaller than 110 degrees

            // updates road's endpoints
            road0->SetEndJoint( NULL );
            road1->SetStartJoint( NULL );
          }
        }

        road2->SetStartJoint( NULL );

        // removes the crossroad from the list
        m_crossroads.Remove( static_cast< size_t >( c ) );
        --c;
      }
    }

    ++c;
  }

  return (removedRoads);
}

//-----------------------------------------------------------------------------

size_t CStrade::MergeRoadsInCross4()
{
  size_t removedRoads = 0;

  bool processDegraded = false;

  int c = 0;
  while ( static_cast< size_t >( c ) < m_crossroads.Size() )
  {
    CCrossroad* crossroad = m_crossroads.Get( c );
    CRoadsSet& roads = crossroad->GetRoads();

    if ( roads.Size() == 4 )
    {
	    // lets all roads to start in this crossroad
	    for ( size_t i = 0, cntI = roads.Size(); i < cntI; ++i )
	    {
		    if ( roads.Get( i )->GetStartJoint() != crossroad )
		    {
			    roads.Get( i )->ReverseVerticesOrder();
		    }
	    }

	    // sorts the roads by the direction of the first segment
	    roads.SortByDirection();

      size_t typesCount = roads.TypesCount();

      if ( typesCount == 1 )
      {
        // all the four roads are of the same type
        // we merge them so that the two resulting roads cross

        CRoad* road0 = roads.Get( 0 );
        CRoad* road1 = roads.Get( 2 );

        if ( road0 == road1 )
        {
          // this is a loop road.
          // no changes in the roads list

          // updates road's endpoints
          road0->SetStartJoint( NULL );
          road0->SetEndJoint( NULL );
        }
        else
        {
          road0->ReverseVerticesOrder();

          CGVector2 dir0( road0->GetPath().LastSegment().Direction() );
          CGVector2 dir1( CGConsts::PI + road1->GetPath().FirstSegment().Direction() );
          Float cos01 = dir0.Dot( dir1 );

          if ( cos01 <= (Float)(-0.3420201433256) )
          {
            // merges the two roads
            road0->GetPath().AppendVertices( road1->GetPath() );

            // updates road's endpoints
            road0->SetEndJoint( road1->GetEndJoint() );

            // updates crossroad, if any
            CJoint* newJoint = road0->GetEndJoint();
            if ( newJoint /*&& (newJoint->Type() == JT_Crossroad)*/ )
            {
              newJoint->GetRoads().Replace( road1, road0 );
            }

            // removes the merged road
            m_roads.Remove( road1 );
            ++removedRoads;
          }
          else
          {
            // the angle is smaller than 110 degrees

            // updates road's endpoints
            road0->SetEndJoint( NULL );
            road1->SetStartJoint( NULL );
          }
        }

        road0 = roads.Get( 1 );
        road1 = roads.Get( 3 );

        if ( road0 == road1 )
        {
          // this is a loop road.
          // no changes in the roads list

          // updates road's endpoints
          road0->SetStartJoint( NULL );
          road0->SetEndJoint( NULL );
        }
        else
        {
          road0->ReverseVerticesOrder();

          CGVector2 dir0( road0->GetPath().LastSegment().Direction() );
          CGVector2 dir1( CGConsts::PI + road1->GetPath().FirstSegment().Direction() );
          Float cos01 = dir0.Dot( dir1 );

          if ( cos01 <= (Float)(-0.3420201433256) )
          {
            // merges the two roads
            road0->GetPath().AppendVertices( road1->GetPath() );

            // updates road's endpoints
            road0->SetEndJoint( road1->GetEndJoint() );

            // updates crossroad, if any
            CJoint* newJoint = road0->GetEndJoint();
            if ( newJoint /*&& (newJoint->Type() == JT_Crossroad)*/ )
            {
              newJoint->GetRoads().Replace( road1, road0 );
            }

            // removes the merged road
            m_roads.Remove( road1 );
            ++removedRoads;
          }
          else
          {
            // the angle is smaller than 110 degrees

            // updates road's endpoints
            road0->SetEndJoint( NULL );
            road1->SetStartJoint( NULL );
          }
        }

        // removes the crossroad from the list
        m_crossroads.Remove( static_cast< size_t >( c ) );
        --c;
      }
      else if ( typesCount == 2 )
      {
        // there are two roads type
        // there could be two cases: 2 + 2 or 3 + 1
        // in the second case we degrade the crossroad to a T
        // type and force a new iteration

        CRoadType* type0 = roads.Get( 0 )->GetType();
        size_t countType0 = 1;
        size_t removeId = 0;
        for ( size_t i = 1, cntI = roads.Size(); i < cntI; ++i )
        {
          if ( type0 == roads.Get( i )->GetType() )
          {
            ++countType0;
          }
          else
          {
            removeId = i;
          }
        }

        if ( (countType0 == 1) || (countType0 == 3) )
        {
          // we degrade this crossroad from X type to T type
          // by removing the only road with a different type
          roads.Get( removeId )->SetStartJoint( NULL );
          roads.Remove( removeId );
          processDegraded = true;
        }
        else
        {
          // we have two couple of roads to merge
          int index0 = 0;
          int index1 = -1;
          int index2 = -1;
          int index3 = -1;

          for ( size_t i = 1, cntI = roads.Size(); i < cntI; ++i )
          {
            if ( type0 == roads.Get( i )->GetType() )
            {
              index1 = i;
            }
            else
            {
              index2 = index3;
              index3 = i;
            }
          }

          if ( (index0 != -1) && (index1 != -1) &&
               (index2 != -1) && (index3 != -1) )
          {
            CRoad* road0 = roads.Get( index0 );
            CRoad* road1 = roads.Get( index1 );

            if ( road0 == road1 )
            {
              // this is a loop road.
              // no changes in the roads list

              // updates road's endpoints
              road0->SetStartJoint( NULL );
              road0->SetEndJoint( NULL );
            }
            else
            {
              road0->ReverseVerticesOrder();

              CGVector2 dir0( road0->GetPath().LastSegment().Direction() );
              CGVector2 dir1( CGConsts::PI + road1->GetPath().FirstSegment().Direction() );
              Float cos01 = dir0.Dot( dir1 );

              if ( cos01 <= (Float)(-0.3420201433256) )
              {
                // merges the two roads
                road0->GetPath().AppendVertices( road1->GetPath() );

                // updates road's endpoints
                road0->SetEndJoint( road1->GetEndJoint() );

                // updates crossroad, if any
                CJoint* newJoint = road0->GetEndJoint();
                if ( newJoint /*&& (newJoint->Type() == JT_Crossroad)*/ )
                {
                  newJoint->GetRoads().Replace( road1, road0 );
                }

                // removes the merged road
                m_roads.Remove( road1 );
                ++removedRoads;
              }
              else
              {
                // the angle is smaller than 110 degrees

                // updates road's endpoints
                road0->SetEndJoint( NULL );
                road1->SetStartJoint( NULL );
              }
            }

            road0 = roads.Get( index2 );
            road1 = roads.Get( index3 );

            if ( road0 == road1 )
            {
              // this is a loop road.
              // no changes in the roads list

              // updates road's endpoints
              road0->SetStartJoint( NULL );
              road0->SetEndJoint( NULL );
            }
            else
            {
              road0->ReverseVerticesOrder();

              CGVector2 dir0( road0->GetPath().LastSegment().Direction() );
              CGVector2 dir1( CGConsts::PI + road1->GetPath().FirstSegment().Direction() );
              Float cos01 = dir0.Dot( dir1 );

              if ( cos01 <= (Float)(-0.3420201433256) )
              {
                // merges the two roads
                road0->GetPath().AppendVertices( road1->GetPath() );

                // updates road's endpoints
                road0->SetEndJoint( road1->GetEndJoint() );

                // updates crossroad, if any
                CJoint* newJoint = road0->GetEndJoint();
                if ( newJoint /*&& (newJoint->Type() == JT_Crossroad)*/ )
                {
                  newJoint->GetRoads().Replace( road1, road0 );
                }

                // removes the merged road
                m_roads.Remove( road1 );
                ++removedRoads;
              }
              else
              {
                // the angle is smaller than 110 degrees

                // updates road's endpoints
                road0->SetEndJoint( NULL );
                road1->SetStartJoint( NULL );
              }
            }

            // removes the crossroad from the list
            m_crossroads.Remove( static_cast< size_t >( c ) );
            --c;
          }
        }
      }
      else if ( typesCount == 3 )
      {
        // only two of the four roads have the same type
        // we search for them and then we merge them
        int index0 = -1;
        int index1 = -1;
  	    for ( size_t i = 0, cntI = roads.Size(); i < cntI - 1; ++i )
        {
  	      for ( size_t j = i + 1, cntJ = roads.Size(); j < cntJ; ++j )
          {
            if ( roads.Get( i )->GetType() == roads.Get( j )->GetType() )
            {
              index0 = i;
              index1 = j;
              break;
            }
          }
          if ( (index0 != -1) && (index1 != -1) )
          {
            break;
          }
        }
      
        if ( (index0 != -1) && (index1 != -1) )
        {
          CRoad* road0 = roads.Get( index0 );
          CRoad* road1 = roads.Get( index1 );

          if ( road0 == road1 )
          {
            // this is a loop road.
            // no changes in the roads list

            // updates road's endpoints
            road0->SetStartJoint( NULL );
            road0->SetEndJoint( NULL );
          }
          else
          {
            road0->ReverseVerticesOrder();

            CGVector2 dir0( road0->GetPath().LastSegment().Direction() );
            CGVector2 dir1( CGConsts::PI + road1->GetPath().FirstSegment().Direction() );
            Float cos01 = dir0.Dot( dir1 );

            if ( cos01 <= (Float)(-0.3420201433256) )
            {
              // merges the two roads
              road0->GetPath().AppendVertices( road1->GetPath() );

              // updates road's endpoints
              road0->SetEndJoint( road1->GetEndJoint() );

              // updates crossroad, if any
              CJoint* newJoint = road0->GetEndJoint();
              if ( newJoint /*&& (newJoint->Type() == JT_Crossroad)*/ )
              {
                newJoint->GetRoads().Replace( road1, road0 );
              }

              // removes the merged road
              m_roads.Remove( road1 );
              ++removedRoads;
            }
            else
            {
              // the angle is smaller than 110 degrees

              // updates road's endpoints
              road0->SetEndJoint( NULL );
              road1->SetStartJoint( NULL );
            }
          }

  	      for ( size_t i = 0, cntI = roads.Size(); i < cntI; ++i )
          {
            CRoad* road = roads.Get( i );
            if ( (road != road0) && (road != road1) )
            {
              road->SetStartJoint( NULL );
            }
          }

          // removes the crossroad from the list
          m_crossroads.Remove( static_cast< size_t >( c ) );
          --c;
        }
      }
    }

    ++c;
  }

  if ( processDegraded )
  {
    removedRoads += MergeRoadsInCross3();
  }

  return (removedRoads);
}

//-----------------------------------------------------------------------------

size_t CStrade::CreateCrossroadsList()
{
  m_crossroads.Clear();

  // the crossroads candidates are all the roads' ending points
  for ( size_t r = 0, cntR = m_roads.Size(); r < cntR; ++r )
  {
    CRoad* road = m_roads.Get( r );

    const CGPoint2& startV = road->GetPath().FirstVertex();
    const CGPoint2& endV   = road->GetPath().LastVertex();

    bool startFound = false;
    bool endFound   = false;

    for ( size_t c = 0, cntC = m_crossroads.Size(); c < cntC; ++c )
    {
      CCrossroad* crossroad = m_crossroads.Get( c );
      const CGPoint2& pos = crossroad->GetPosition();

      if ( pos.Distance( startV ) <= TOLERANCE )
      {
        // start point already in list
        crossroad->GetRoads().Add( road );
        road->SetStartJoint( crossroad );
        startFound = true;
      }

      if ( pos.Distance( endV ) <= TOLERANCE )
      {
        // end point already in list
        crossroad->GetRoads().Add( road );
        road->SetEndJoint( crossroad );
        endFound = true;
      }
    }

    if ( !startFound )
    {
      // start point not in list
      CCrossroad* crossroad = m_crossroads.Append( CCrossroad( startV, m_crossroadType, road ) );
      road->SetStartJoint( crossroad );
    }

    if ( !endFound )
    {
      // end point not in list
      CCrossroad* crossroad = m_crossroads.Append( CCrossroad( endV, m_crossroadType, road ) );
      road->SetEndJoint( crossroad );
    }
  }

  size_t removedRoads = 0;

  // removes from the crossroads list all the candidates with only one road (road terminators).
  // no changes in the roads list.
  int c = 0; 
  while ( static_cast< size_t >( c ) < m_crossroads.Size() )
  {
    CCrossroad* crossroad = m_crossroads.Get( c );

    if ( crossroad->GetRoads().Size() <= 1 )
    {
      // gets the only existing road and updates its endpoints
      CRoad* road = crossroad->GetRoads().Get( 0 );
      if ( road->GetStartJoint() == crossroad )
      {
        road->SetStartJoint( NULL );
      }
      if ( road->GetEndJoint() == crossroad )
      {
        road->SetEndJoint( NULL );
      }

      // removes the crossroad from the list
      m_crossroads.Remove( static_cast< size_t >( c ) );
      --c;
    }
    ++c;
  }

  // removes from the crossroads list all the candidates with roads all of different type.
  // no changes in the roads list.
  c = 0;
  while ( static_cast< size_t >( c ) < m_crossroads.Size() )
  {
    CCrossroad* crossroad = m_crossroads.Get( c );

    size_t roadsCount = crossroad->GetRoads().Size();
    if ( roadsCount == crossroad->GetRoads().TypesCount() )
    {
      // updates roads' endpoints
      for ( size_t r = 0; r < roadsCount; ++r )
      {
        CRoad* road = crossroad->GetRoads().Get( r );
        if ( road->GetStartJoint() == crossroad )
        {
          road->SetStartJoint( NULL );
        }
        if ( road->GetEndJoint() == crossroad )
        {
          road->SetEndJoint( NULL );
        }
      }

      // removes the crossroad from the list
      m_crossroads.Remove( static_cast< size_t >( c ) );
      --c;
    }
    ++c;
  }

  // checks if there are crossroads with more than 4 roads
  // in this case the thinner roads will be detached from the crossroad
  for ( size_t c = 0, cntC = m_crossroads.Size(); c < cntC; ++c )
  {
    CCrossroad* crossroad = m_crossroads.Get( c );

    size_t roadsCount = crossroad->GetRoads().Size();
    if ( roadsCount > 4 )
    {
      crossroad->GetRoads().SortByWidth();

      // updates roads and removes them from the crossroad
      for ( size_t r = 4; r < roadsCount; ++r )
      {
        CRoad* road = crossroad->GetRoads().Get( 4 );
        if ( road->GetStartJoint() == crossroad )
        {
          road->SetStartJoint( NULL );
        }
        if ( road->GetEndJoint() == crossroad )
        {
          road->SetEndJoint( NULL );
        }
        crossroad->GetRoads().Remove( 4 );
      }
    }
  }

  if ( m_crossroadType == CT_NoCrossroads )
  {
    // no crossroads will be generated, we search only for all the
    // roads that could be merged

    // removes from the crossroads list all the candidates with two roads,
    // which are both of the same type, and merges the two roads if the
    // angle they form in greater than or equal to 110 degrees
    removedRoads += MergeRoadsInCross2();

    // removes from the crossroads list all the candidates with three roads,
    // where at least two of them have the same type, and merges the two roads 
    removedRoads += MergeRoadsInCross3();

    // removes from the crossroads list all the candidates with four roads,
    // where at least two of them have the same type, and merges the roads 
    removedRoads += MergeRoadsInCross4();

    // resets roads' endpoints
    for ( size_t c = 0, cntC = m_crossroads.Size(); c < cntC; ++c )
    {
      CCrossroad* crossroad = m_crossroads.Get( c );
      size_t roadsCount = crossroad->GetRoads().Size();
      for ( size_t r = 0; r < roadsCount; ++r )
      {
        CRoad* road = crossroad->GetRoads().Get( r );
        if ( road->GetStartJoint() == crossroad )
        {
          road->SetStartJoint( NULL );
        }
        if ( road->GetEndJoint() == crossroad )
        {
          road->SetEndJoint( NULL );
        }
      }
    }

    // resets joints' endpoints
    for ( size_t c = 0, cntC = m_joints.Size(); c < cntC; ++c )
    {
      CNullJoint* joint = m_joints.Get( c );
      size_t roadsCount = joint->GetRoads().Size();
      for ( size_t r = 0; r < roadsCount; ++r )
      {
        CRoad* road = joint->GetRoads().Get( r );
        if ( road->GetStartJoint() == joint )
        {
          road->SetStartJoint( NULL );
        }
        if ( road->GetEndJoint() == joint )
        {
          road->SetEndJoint( NULL );
        }
      }
    }

    m_crossroads.Clear();
    m_joints.Clear();
  }
  else
  {
    // removes from the crossroads list all the candidates with two roads,
    // which are both of the same type, and merges the two roads if the
    // angle they form in greater than or equal to 110 degrees
    removedRoads += MergeRoadsInCross2();

    // removes from the crossroads list all the candidates where a road
    // arrives with both endpoints (loop)
    for ( size_t r = 0, cntR = m_roads.Size(); r < cntR; ++r )
    {
      CRoad* road = m_roads.Get( r );

      CJoint* startJoint = road->GetStartJoint();
      CJoint* endJoint   = road->GetEndJoint();
      
      if ( startJoint && endJoint && (startJoint == endJoint) )
      {
        if ( startJoint->Type() == JT_Crossroad )
        {
          CCrossroad* crossroad = reinterpret_cast< CCrossroad* >( startJoint );

          bool loopSet = false; // to set the loop only once

          // updates roads' endpoints
          CRoadsSet& roads = crossroad->GetRoads();
          for ( size_t j = 0, cntJ = roads.Size(); j < cntJ; ++j )
          {
            CRoad* roadJ = roads.Get( j );
            if ( roadJ == road )
            {
              if ( !loopSet )
              {
                // this is the loop road.

                // creates a new null joint
                CNullJoint joint( crossroad->GetPosition(), roadJ );
                joint.GetRoads().Add( roadJ );
                CNullJoint* newJoint = m_joints.Append( joint );

                // updates road's endpoints
                roadJ->SetStartJoint( newJoint ); 
                roadJ->SetEndJoint( newJoint ); 

                loopSet = true;
              }
            }
            else
            {
              if ( roadJ->GetStartJoint() == crossroad ) { roadJ->SetStartJoint( NULL ); }
              if ( roadJ->GetEndJoint() == crossroad )   { roadJ->SetEndJoint( NULL ); }
            }
          }

          // removes the crossroad from the list
          m_crossroads.Remove( crossroad );
        }
      }
    }
  }

  return (removedRoads);
}

//-----------------------------------------------------------------------------

size_t CStrade::RemoveInvalidRoads()
{
  size_t removedRoads = 0;

  int r = 0; 

  while ( static_cast< size_t >( r ) < m_roads.Size() )
  {
    CRoad* road = m_roads.Get( r ); 
    if ( !road->IsValid( TOLERANCE ) )
    {
      m_logFile << "WARNING: Removed invalid" << *road << endl << endl;
      m_roads.Remove( static_cast< size_t >( r ) );
      --r;
      ++removedRoads;
    }
    ++r;
  }

  return (removedRoads);
}

//-----------------------------------------------------------------------------

size_t CStrade::SplitConstrainedRoads()
{
  m_joints.Clear();

  size_t counter = 0;

  size_t i = 0;
  while ( i < m_roads.Size() )
  {
    CRoad* road = m_roads.Get( i );
    const CJoint* jointS = road->GetStartJoint();
    const CJoint* jointE = road->GetEndJoint();

    if ( jointS && jointE )
    {
      if ( (jointS->Type() == JT_Crossroad) && (jointE->Type() == JT_Crossroad) )
      {
        int method = -1;

        CGPoint2 midpointLongestSegment = road->GetPath().MidPointOfLongestSegment();

        Float distStart = midpointLongestSegment.Distance( jointS->GetPosition() );
        Float distEnd   = midpointLongestSegment.Distance( jointE->GetPosition() );

        Float radiusS = (reinterpret_cast< const CCrossroad* >( jointS ))->Radius();
        Float radiusE = (reinterpret_cast< const CCrossroad* >( jointE ))->Radius();

        if ( (distStart < radiusS) || (distEnd < radiusE) )
        {
          CGPoint2& midpoint = road->GetPath().MidPoint();
          distStart = midpoint.Distance( jointS->GetPosition() );
          distEnd   = midpoint.Distance( jointE->GetPosition() );
          if ( (distStart < radiusS) || (distEnd < radiusE) )
          {
            // both the midpoint of the longest segment of the road path and the
            // midpoint of the path are contained in any of the two crossroads' 
            // circles, so we do not split the road and create a joint
          }
          else
          {
            // the midpoint of the longest segment of the road path is contained
            // in any of the two crossroads' circles, but the midpoint of the path
            // is not
            method = 0;
          }
        }
        else
        {
          // the midpoint of the longest segment of the road path is not contained
          // in any of the two crossroads' circles
          method = 1;
        }

        if ( method != -1 )
        {
          // splits the current road and adds the newly created road to the list
          CRoad* newRoad = m_roads.Append( road->Split( static_cast< size_t >( method ) ) );

          // updates the ending crossroad
          newRoad->GetEndJoint()->GetRoads().Replace( road, newRoad );

          // creates a new joint
          CNullJoint joint( road->GetPath().LastVertex(), road );
          joint.GetRoads().Add( newRoad );
          CNullJoint* newJoint = m_joints.Append( joint );

          // updates roads' endpoints
          newRoad->SetStartJoint( newJoint );
          road->SetEndJoint( newJoint );

          ++counter;
        }
      }
    }
    ++i;
  }

  return (counter);
}

//-----------------------------------------------------------------------------

void CStrade::SplineRoads()
{
  for ( size_t r = 0, cntR = m_roads.Size(); r < cntR; ++r )
  {
    m_roads.Get( r )->ConvertToSpline();
  }
}

//-----------------------------------------------------------------------------

void CStrade::ExportCreatedObjects( IMainCommands* cmdIfc )
{
  if ( m_visitorVersion == 3 )
  {
    // places crossroads
    for ( size_t c = 0, cntC = m_crossroads.Size(); c < cntC; ++c )
    {
      const CCrossroadModel& model = m_crossroads.Get( c )->GetModel();
      CGPoint2 center = model.GetVisitorPosition();

      RString name     = model.GetTemplate()->Name();
      Vector3D position = Vector3D( center.GetX(), 
                                              0.0,
                                    center.GetY() );

      Matrix4D mTranslation = Matrix4D( MTranslation, position );
      Matrix4D mRotation    = Matrix4D( MRotationY, model.GetDirection() );

      Matrix4D transform = mTranslation * mRotation;
      cmdIfc->GetObjectMap()->PlaceObject( transform, name, 0 );
    }

    // places roads
    for ( size_t r = 0, cntR = m_roads.Size(); r < cntR; ++r )
    {
      const CRoad* road = m_roads.Get( r );
      const vector< CRoadPartModel >& models = road->GetModels();
      for ( size_t m = 0, cntM = models.size(); m < cntM; ++m )
      {
        const CRoadPartModel& model = models[m];
        CGPoint2 center = model.GetVisitorPosition();

        RString name     = model.GetTemplate()->Name();
        Vector3D position = Vector3D( center.GetX(), 0.0, center.GetY() );

        Matrix4D mTranslation = Matrix4D( MTranslation, position );
        Matrix4D mRotation    = Matrix4D( MRotationY, model.GetDirection() );

        Matrix4D transform = mTranslation * mRotation;
        cmdIfc->GetObjectMap()->PlaceObject( transform, name, 0 );
      }
    }
  }
  else
  {
    // first creates roads definitions list
    vector< LandBuilder2::LBRoadDefinition > roadDefinitions;

    for ( size_t i = 0, cntI = m_roadTypesLibrary.Size(); i < cntI; ++i )
    {
      const CRoadType* roadType = m_roadTypesLibrary.Get( i );
      const CRoadPartTemplatesLibrary& templates = roadType->GetTemplates();

      LandBuilder2::LBRoadDefinition roadDef( string( roadType->ExportName() ) );
      roadDef.SetOutputDir( string( m_modelsPath ) );
      
      size_t counter = 0;
      for ( size_t j = 0, cntJ = templates.Size(); j < cntJ; ++j )
      {
        LandBuilder2::LBRoadPartModel roadPart;
        const CRoadPartTemplate* templ = templates.Get( j );

        if ( templ->GetReferencesCount() > 0 )
        {
          roadPart.SetName( string( templ->Name() ) );
          switch ( templ->Type() )
          {
          case TT_Straight:      roadPart.SetType( LandBuilder2::RPMT_Straight );   break;
          case TT_Corner:        roadPart.SetType( LandBuilder2::RPMT_Corner );     break;
          case TT_StrTerminator: roadPart.SetType( LandBuilder2::RPMT_Terminator ); break;
          default:               roadPart.SetType( LandBuilder2::RPMT_Straight );
          }
          roadDef.AddModel( roadPart );
          ++counter;
        }
      }

      if ( counter > 0 ) { roadDefinitions.push_back( roadDef ); }
    }

    // exports roads definitions list
    if ( roadDefinitions.size() > 0 )
    {
      for ( size_t i = 0, cntI = roadDefinitions.size(); i < cntI; ++i )
      {
        cmdIfc->GetRoadMap()->AddRoadDefinition( roadDefinitions[i] );
      }
    }

    // exports crossroads definitions list
    for ( size_t i = 0, cntI = m_crossroadTemplatesLibrary.Size(); i < cntI; ++i )
    {
      const CCrossroadTemplate* templ = m_crossroadTemplatesLibrary.Get( i );
      const CRoadsSet* roadSet = templ->GetRoadSet();

      LandBuilder2::LBCrossroadDefinition crossDef( string( templ->Name() ) );
      size_t roadsCount = roadSet->Size();
      for ( size_t j = 0; j < roadsCount; ++j )
      {
        const CRoad* road = roadSet->Get( j );
        String roadType = road->GetType()->ExportName();

        switch ( j )
        {
        case 0: 
          {
            crossDef.SetRoadTypeDirB( string( roadType ) ); 
          }
          break;
        case 1: 
          {
            if ( roadsCount == 3 )
            {
              crossDef.SetRoadTypeDirA( string( roadType ) ); 
            }
            else
            {
              crossDef.SetRoadTypeDirD( string( roadType ) ); 
            }
          }
          break;
        case 2: 
          {
            if ( roadsCount == 3 )
            {
              crossDef.SetRoadTypeDirC( string( roadType ) ); 
            }
            else
            {
              crossDef.SetRoadTypeDirA( string( roadType ) ); 
            }
          }
          break;
        case 3: 
          {
            crossDef.SetRoadTypeDirC( string( roadType ) ); 
          }
          break;
        }
      }

      cmdIfc->GetRoadMap()->AddCrossroadDefinition( crossDef );
    }

    // then exports the actual roads
    // begins from the roads starting from crossroads
    for ( size_t i = 0, cntI = m_crossroads.Size(); i < cntI; ++i )
    {
      CCrossroad* cross = m_crossroads.Get( i );
      const CCrossroadModel& model = cross->GetModel();
      String templateName = model.GetTemplate()->Name();
      CGPoint2 position   = model.GetPosition();
      Float direction     = model.GetVisitorDirection() * 180.0 / CGConsts::PI;

      // this is an hack to avoid that the following while()
      // becomes an infinite loop
      if ( _finite( direction ) == 0 )
      {
        direction = static_cast< Float >( 0 );
      }

      while ( direction < static_cast< Float >( 0 ) ) 
      {
        direction += static_cast< Float >( 360 );
      }

      LandBuilder2::LBRoadKeypart keypart;
      keypart.SetTemplateName( string( templateName ) );
      keypart.SetKeyType( "crossroad" );
      keypart.SetRoadType( "" );
      keypart.SetPosition( Shapes::DVertex( position.GetX(), position.GetY() ) );
      keypart.SetOrientation( direction );

      CRoadsSet& roads = cross->GetRoads();
      for ( size_t j = 0, cntJ = roads.Size(); j < cntJ; ++j )
      {
        CRoad* road = roads.Get( j );

        LandBuilder2::LBRoad& lbRoad = keypart.AppendRoad();
        lbRoad.SetType( string( road->GetType()->ExportName() ) );

        const vector< CRoadPartModel >& models = road->GetModels();
        for ( size_t k = 0, cntK = models.size(); k < cntK; ++k )
        {
          const CRoadPartModel& model = models[k];
          const CRoadBaseTemplate* templ = model.GetTemplate();

          LandBuilder2::RoadPartModelTypes type;
          switch ( templ->Type() )
          {
          case TT_Straight:
            {
              type = LandBuilder2::RPMT_Straight;
            }
            break;
          case TT_Corner:
            {
              type = LandBuilder2::RPMT_Corner;
            }
            break;
          case TT_StrTerminator:
            {
              type = LandBuilder2::RPMT_Terminator;
            }
            break;
          }
          lbRoad.AppendPart( LandBuilder2::LBRoadPartModel( string( templ->Name() ), type, !model.IsInverted() ) );
        }

        road->SetExported( true );
      }

      cmdIfc->GetRoadMap()->AddRoadKeypart( keypart );
    }

    // then search for any remaining road
    for ( size_t i = 0, cntI = m_roads.Size(); i < cntI; ++i )
    {
      CRoad* road = m_roads.Get( i );
      if ( !road->IsExported() && road->IsBuilt() )
      {
        const vector< CRoadPartModel >& models = road->GetModels();
        const CRoadPartModel& model = models[0];
        String templateName = model.GetTemplate()->Name();
        CGPoint2 position   = model.GetPosition();
        Float direction     = model.GetVisitorDirection() * 180.0 / CGConsts::PI;

        // this is an hack to avoid that the following while()
        // becomes an infinite loop
        if ( _finite( direction ) == 0 )
        {
          direction = static_cast< Float >( 0 );
        }

        while ( direction < static_cast< Float >( 0 ) ) 
        {
          direction += static_cast< Float >( 360 );
        }

        LandBuilder2::LBRoadKeypart keypart;
        keypart.SetTemplateName( string( templateName ) );
        switch ( model.GetTemplate()->Type() )
        {
        case TT_Straight:
          {
            keypart.SetKeyType( "straight" );
          }
          break;
        case TT_Corner:
          {
            keypart.SetKeyType( "corner" );
          }
          break;
        case  TT_StrTerminator:
          {
            keypart.SetKeyType( "terminator" );
          }
          break;
        }
        keypart.SetRoadType( string( road->GetType()->ExportName() ) );
        keypart.SetPosition( Shapes::DVertex( position.GetX(), position.GetY() ) );
        keypart.SetOrientation( direction );

        LandBuilder2::LBRoad& lbRoad = keypart.AppendRoad();
        lbRoad.SetType( string( road->GetType()->ExportName() ) );

        if ( models.size() > 1 )
        {
          for ( size_t k = 1, cntK = models.size(); k < cntK; ++k )
          {
            const CRoadPartModel& model = models[k];
            const CRoadBaseTemplate* templ = model.GetTemplate();

            LandBuilder2::RoadPartModelTypes type;
            switch ( templ->Type() )
            {
            case TT_Straight:
              {
                type = LandBuilder2::RPMT_Straight;
              }
              break;
            case TT_Corner:
              {
                type = LandBuilder2::RPMT_Corner;
              }
              break;
            case TT_StrTerminator:
              {
                type = LandBuilder2::RPMT_Terminator;
              }
              break;
            }
            lbRoad.AppendPart( LandBuilder2::LBRoadPartModel( string( templ->Name() ), type, !model.IsInverted() ) );
          }
        }

        cmdIfc->GetRoadMap()->AddRoadKeypart( keypart );

        road->SetExported( true );
      }
    }
  }
}

//-----------------------------------------------------------------------------

