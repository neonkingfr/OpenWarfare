//-----------------------------------------------------------------------------
// File: TList.inl
//
// Desc: a templated list
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

template < class T >
TList< T >::TList()
{
}

//-----------------------------------------------------------------------------

template < class T >
TList< T >::TList( const TList< T >& other )
{
	CopyFrom( other );
}

//-----------------------------------------------------------------------------

template < class T >
TList< T >::~TList()
{
	Clear();
}

// -------------------------------------------------------------------------- //

template < class T >
TList< T >& TList< T >::operator = ( const TList< T >& other )
{
	CopyFrom( other );
	return (*this);
}

// -------------------------------------------------------------------------- //

template < class T >
void TList< T >::Clear()
{
	for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
	{
		delete m_elements[i];
	}
	m_elements.clear();
}

// -------------------------------------------------------------------------- //

template < class T >
T* TList< T >::Append( const T& element )
{
	T* t = new T( element );
	m_elements.push_back( t );
	return (m_elements[m_elements.size() - 1]);
}

// -------------------------------------------------------------------------- //

template < class T >
T* TList< T >::Append( T* element )
{
	m_elements.push_back( element );
	return (m_elements[m_elements.size() - 1]);
}

// -------------------------------------------------------------------------- //

template < class T >
size_t TList< T >::Size() const
{
	return (m_elements.size());
}

// -------------------------------------------------------------------------- //

template < class T >
void TList< T >::Remove( const T* element )
{
	if ( element )
	{
		for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
		{
			if ( m_elements[i] == element )
			{
				Remove( i );
				return;
			}
		}
	}
}

// -------------------------------------------------------------------------- //

template < class T >
void TList< T >::Remove( size_t index )
{
	assert( index < m_elements.size() );
	delete m_elements[index];
	m_elements.erase( m_elements.begin() + index );
}

// -------------------------------------------------------------------------- //

template < class T >
T* TList< T >::Find( const T& t ) const
{
	for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
	{
		if ( m_elements[i]->Equals( t ) )
		{
			return (m_elements[i]);
		}
	}

	return (NULL);
}

// -------------------------------------------------------------------------- //

template < class T >
const T* TList< T >::Get( size_t index ) const
{
	assert( index < m_elements.size() );
	return (m_elements[index]);
}

// -------------------------------------------------------------------------- //

template < class T >
T* TList< T >::Get( size_t index )
{
	assert( index < m_elements.size() );
	return (m_elements[index]);
}

// -------------------------------------------------------------------------- //

template < class T >
void TList< T >::CopyFrom( const TList< T >& other )
{
	Clear();
	for ( size_t i = 0, cntI = other.Size(); i < cntI; ++i )
	{
		Append( *other.Get( i ) );
	}
}

// -------------------------------------------------------------------------- //
