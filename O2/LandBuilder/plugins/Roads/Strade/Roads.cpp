//-----------------------------------------------------------------------------
// File: Roads.cpp
//
// Desc: classes and types related to roads
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Roads.h"
#include "Crossroads.h"
#include "..\BSpline.h"

//-----------------------------------------------------------------------------

bool etRandomSelect( Float threshold )
{
  // clamps threshold
  if ( threshold < ZERO ) 
  { 
    threshold = ZERO; 
  }
  else if ( threshold > static_cast< Float >( 100 ) ) 
  { 
    threshold = static_cast< Float >( 100 ); 
  }

  // normalizes threshold to [0..1]
  threshold /= static_cast< Float >( 100 ); 

  // randomizes
  Float r = static_cast< Float >( rand() )/ (static_cast< Float >( RAND_MAX + 1 ));

  // return check
  return ( r <= threshold );
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CRoadType::CRoadType( const String& name, const String& prefix, const String& mapP3D, 
                      const map< string, string >& textures, Float probSpecStrTex, Float probSpecCorTex, 
                      Float probSpecTerTex, size_t minSpecDist, Float cornerResolution, 
                      Float proximityFactor, Float matchThreshold )
: m_name( name )
, m_prefix( prefix )
, m_width( ZERO )
, m_mapP3D( mapP3D )
, m_textures( textures )
, m_probSpecStrTex( probSpecStrTex )
, m_probSpecCorTex( probSpecCorTex )
, m_probSpecTerTex( probSpecTerTex )
, m_minSpecDist( minSpecDist )
, m_cornerResolution( cornerResolution )
, m_proximityFactor( proximityFactor )
, m_matchThreshold( matchThreshold )
{
}

//-----------------------------------------------------------------------------

const String& CRoadType::GetName() const
{
  return (m_name);
}

//-----------------------------------------------------------------------------

const String& CRoadType::GetPrefix() const
{
  return (m_prefix);
}

//-----------------------------------------------------------------------------

Float CRoadType::GetWidth() const
{
  return (m_width);
}

//-----------------------------------------------------------------------------

string CRoadType::GetTexture( const string& type ) const
{
  map< string, string >::const_iterator it = m_textures.find( type );
  if ( it == m_textures.end() )
  {
    return ("");
  }
  else
  {
    return (it->second);
  }
}

//-----------------------------------------------------------------------------

const map< string, string >& CRoadType::GetTextures() const
{
  return (m_textures);
}

//-----------------------------------------------------------------------------

const String& CRoadType::GetMapP3D() const
{
  return (m_mapP3D);
}

//-----------------------------------------------------------------------------

const CRoadPartTemplatesLibrary& CRoadType::GetTemplates() const
{
  return (m_templates);
}

//-----------------------------------------------------------------------------

CRoadPartTemplatesLibrary& CRoadType::GetTemplates()
{
  return (m_templates);
}

//-----------------------------------------------------------------------------

Float CRoadType::GetCornerResolution() const
{
  return (m_cornerResolution);
}

//-----------------------------------------------------------------------------

Float CRoadType::GetProbSpecStrTex() const
{
  return (m_probSpecStrTex);
}

//-----------------------------------------------------------------------------

Float CRoadType::GetProbSpecCorTex() const
{
  return (m_probSpecCorTex);
}

//-----------------------------------------------------------------------------

Float CRoadType::GetProbSpecTerTex() const
{
  return (m_probSpecTerTex);
}

//-----------------------------------------------------------------------------

size_t CRoadType::GetMinSpecDist() const
{
  return (m_minSpecDist);
}

//-----------------------------------------------------------------------------

Float CRoadType::GetMatchThreshold() const
{
  return (m_matchThreshold);
}

//-----------------------------------------------------------------------------

Float CRoadType::GetProximityFactor() const
{
  return (m_proximityFactor);
}

//-----------------------------------------------------------------------------

void CRoadType::SetWidth( Float width )
{
  if ( width < ZERO ) 
  { 
    width = DEF_ROAD_WIDTH; 
  }
  else if ( width > MAX_ROAD_WIDTH )
  {
    width = MAX_ROAD_WIDTH; 
  }

  m_width = width;

  m_templates.Clear();
  AddDefaultTemplates();
}

//-----------------------------------------------------------------------------

String CRoadType::ExportName() const
{
  return (m_name + "_W" + String( FormatNumber( m_width ).c_str() ));
}

//-----------------------------------------------------------------------------

String CRoadType::ExportPrefix() const
{
  return (m_prefix + "_W" + String( FormatNumber( m_width ).c_str() ));
}

//-----------------------------------------------------------------------------

CRoadPartTemplate* CRoadType::ClosestTemplateToPath( const CGPoint2& position, Float direction, 
                                                     const CGPolyline2& path, bool fit, bool& inverted )
{
  CRoadPartTemplate* ret = NULL;

  Float minDist       = std::numeric_limits< Float >::infinity();
  Float minAngDiff    = std::numeric_limits< Float >::infinity();
  Float thresholdDist = m_width * m_proximityFactor; 
  Float thresholdAng  = HALF * CGConsts::HALF_PI;

  CRoadPartModel model;
  CGPoint2       nextPosition;
  Float          nextDirection;
  CGVector2      nextTangent;
  Float          distFromPath;
  Float          directionDiff;

  for ( size_t i = 0, cntI = m_templates.Size(); i < cntI; ++i )
  {
    CRoadPartTemplate* part = m_templates.Get( i );

    // we check straights and corners only
    if ( (part->Type() == TT_Straight) || (part->Type() == TT_Corner) )
    {
      // creates a model using the current part template
      model = CRoadPartModel( part, position, direction );
      nextPosition = model.GetNextPosition();

      distFromPath = nextPosition.Distance( path.ClosestPointTo( nextPosition, TOLERANCE, &nextTangent, NULL ) );
      if ( fit )
      {
        if ( distFromPath <= thresholdDist )
        {
          nextDirection = model.GetNextDirection();
          CGVector2 vecNextDirection( nextDirection );
          directionDiff = acos( vecNextDirection.Dot( nextTangent ) );
          if ( (directionDiff <= thresholdAng) && (minAngDiff > directionDiff) )
          {
            minAngDiff = directionDiff;
            ret        = part;
            inverted   = false;
          }
        }
      }
      else if ( minDist > distFromPath )
      {
        minDist  = distFromPath;
        ret      = part;
        inverted = false;
      }

      // if the current part is a corner, check it also as left corner
      if ( part->Type() == TT_Corner )
      {
        model = CRoadPartModel( part, position, direction, true );
        nextPosition = model.GetNextPosition();

        distFromPath = nextPosition.Distance( path.ClosestPointTo( nextPosition, TOLERANCE, &nextTangent, NULL ) );
        if ( fit )
        {
          if ( distFromPath <= thresholdDist )
          {
            nextDirection = model.GetNextDirection();
            CGVector2 vecNextDirection( nextDirection );
            directionDiff = acos( vecNextDirection.Dot( nextTangent ) );
            if ( (directionDiff <= thresholdAng) && (minAngDiff > directionDiff) )
            {
              minAngDiff = directionDiff;
              ret        = part;
              inverted   = true;
            }
          }
        }
        else if ( minDist > distFromPath )
        {
          minDist  = distFromPath;
          ret      = part;
          inverted = true;
        }
      }
    }
  }

  return (ret);
}

//-----------------------------------------------------------------------------

CRoadPartTemplate* CRoadType::ClosestTemplateToPathAccurate( const CGPoint2& position, Float direction, 
                                                             const CGPolyline2& path, bool fit,
                                                             bool& inverted )
{
  CRoadPartTemplate* ret = NULL;

  Float minDist       = std::numeric_limits< Float >::infinity();
  Float minAngDiff    = std::numeric_limits< Float >::infinity();
  Float thresholdDist = m_width * m_proximityFactor; 
  Float thresholdAng  = HALF * CGConsts::HALF_PI;

  CRoadPartModel modelI;
  CRoadPartModel modelJ;
  CGPoint2       nextPositionI;
  CGPoint2       nextPositionJ;
  Float          nextDirectionI;
  Float          nextDirectionJ;
  CGVector2      nextTangent;
  Float          distFromPath;
  Float          directionDiff;

  for ( size_t i = 0, cntI = m_templates.Size(); i < cntI; ++i )
  {
    CRoadPartTemplate* partI = m_templates.Get( i );

    // we check straights and corners only
    if ( (partI->Type() == TT_Straight) || (partI->Type() == TT_Corner) )
    {
      // creates a model using the current part template
      modelI = CRoadPartModel( partI, position, direction );
      nextPositionI  = modelI.GetNextPosition();
      nextDirectionI = modelI.GetNextDirection();

      for ( size_t j = 0, cntJ = m_templates.Size(); j < cntJ; ++j )
      {
        CRoadPartTemplate* partJ = m_templates.Get( j );

        if ( (partJ->Type() == TT_Straight) || (partJ->Type() == TT_Corner) )
        {
          modelJ = CRoadPartModel( partJ, nextPositionI, nextDirectionI );
          nextPositionJ  = modelJ.GetNextPosition();

          distFromPath = nextPositionJ.Distance( path.ClosestPointTo( nextPositionJ, TOLERANCE, &nextTangent, NULL ) );
          if ( fit )
          {
            if ( distFromPath <= thresholdDist )
            {
              nextDirectionJ = modelJ.GetNextDirection();
              CGVector2 vecNextDirection( nextDirectionJ );
              directionDiff = acos( vecNextDirection.Dot( nextTangent ) );
              if ( (directionDiff <= thresholdAng) && (minAngDiff > directionDiff) )
              {
                minAngDiff = directionDiff;
                ret = partI;
                inverted = false;
              }
            }
          }
          else if ( minDist > distFromPath )
          {
            minDist = distFromPath;
            ret = partI;
            inverted = false;
          }

          if ( partJ->Type() == TT_Corner )
          {
            modelJ = CRoadPartModel( partJ, nextPositionI, nextDirectionI, true );
            nextPositionJ = modelJ.GetNextPosition();

            distFromPath = nextPositionJ.Distance( path.ClosestPointTo( nextPositionJ, TOLERANCE, &nextTangent, NULL ) );
            if ( fit )
            {
              if ( distFromPath <= thresholdDist )
              {
                nextDirectionJ = modelJ.GetNextDirection();
                CGVector2 vecNextDirection( nextDirectionJ );
                directionDiff = acos( vecNextDirection.Dot( nextTangent ) );
                if ( (directionDiff <= thresholdAng) && (minAngDiff > directionDiff) )
                {
                  minAngDiff = directionDiff;
                  ret = partI;
                  inverted = true;
                }
              }
            }
            else if ( minDist > distFromPath )
            {
              minDist = distFromPath;
              ret = partI;
              inverted = true;
            }
          }
        }
      }

      // if the current part is a corner, check it also as left corner
      if ( partI->Type() == TT_Corner )
      {
        modelI = CRoadPartModel( partI, position, direction, true );
        nextPositionI  = modelI.GetNextPosition();
        nextDirectionI = modelI.GetNextDirection();

        for ( size_t j = 0, cntJ = m_templates.Size(); j < cntJ; ++j )
        {
          CRoadPartTemplate* partJ = m_templates.Get( j );

          if ( (partJ->Type() == TT_Straight) || (partJ->Type() == TT_Corner) )
          {
            modelJ = CRoadPartModel( partJ, nextPositionI, nextDirectionI );
            nextPositionJ  = modelJ.GetNextPosition();

            distFromPath = nextPositionJ.Distance( path.ClosestPointTo( nextPositionJ, TOLERANCE, &nextTangent, NULL ) );
            if ( fit )
            {
              if ( distFromPath <= thresholdDist )
              {
                nextDirectionJ = modelJ.GetNextDirection();
                CGVector2 vecNextDirection( nextDirectionJ );
                directionDiff = acos( vecNextDirection.Dot( nextTangent ) );
                if ( (directionDiff <= thresholdAng) && (minAngDiff > directionDiff) )
                {
                  minAngDiff = directionDiff;
                  ret = partI;
                  inverted = false;
                }
              }
            }
            else if ( minDist > distFromPath )
            {
              minDist = distFromPath;
              ret = partI;
              inverted = false;
            }

            if ( partJ->Type() == TT_Corner )
            {
              modelJ = CRoadPartModel( partJ, nextPositionI, nextDirectionI, true );
              nextPositionJ = modelJ.GetNextPosition();

              distFromPath = nextPositionJ.Distance( path.ClosestPointTo( nextPositionJ, TOLERANCE, &nextTangent, NULL ) );
              if ( fit )
              {
                if ( distFromPath <= thresholdDist )
                {
                  nextDirectionJ = modelJ.GetNextDirection();
                  CGVector2 vecNextDirection( nextDirectionJ );
                  directionDiff = acos( vecNextDirection.Dot( nextTangent ) );
                  if ( (directionDiff <= thresholdAng) && (minAngDiff > directionDiff) )
                  {
                    minAngDiff = directionDiff;
                    ret = partI;
                    inverted = true;
                  }
                }
              }
              else if ( minDist > distFromPath )
              {
                minDist = distFromPath;
                ret = partI;
                inverted = true;
              }
            }
          }
        }
      }
    }
  }

  return (ret);
}

//-----------------------------------------------------------------------------

CRoadPartTemplate* CRoadType::CreateNewCornerTemplate( const CGPoint2& position, Float direction, const CGPolyline2& path )
{
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// 1.2.5 
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//  Float minCornerRadius = m_width;
  Float minCornerRadius = static_cast< Float >( 0.1 ) + HALF * m_width;
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

  Float cutLength = path.LengthAtClosestPointTo( position, TOLERANCE );

  if ( cutLength < path.Length() )
  {
    CGPolyline2 tempPath = path.SubPart( cutLength, path.Length(), false );
    CGCircle2 circle( position, m_width );
    CGPoint2 intPoint;
    int segIndex;
    FirstIntersect( circle, tempPath, intPoint, segIndex, TOLERANCE );

    if ( segIndex != -1 )
    {
      CGVector2 vec( intPoint, position );
      CGVector2 vecUnit = vec.Normalized();
      CGVector2 dirUnit( direction );

      Float halfAngle = acos( dirUnit.Dot( vecUnit ) );

      if ( (HALF * MIN_CORNER_ANGLE <= halfAngle) && (halfAngle <= HALF * MAX_CORNER_ANGLE) )
      {
        Float radius = HALF * vec.Length() / sin( halfAngle );
        if ( radius >= minCornerRadius )
        {
          Float angle = TWO * halfAngle;
          string texture;
          if ( radius > static_cast< Float >( 100 ) )
          {
            texture = GetTexture( MAIN_STR_TEX );
          }
          else
          {
            texture = GetTexture( MAIN_COR_TEX );
          }
          return (m_templates.Append( CCornerTemplate( this, radius, angle, texture ) ));
        }
      }
    }
  }

  return (NULL);
}

//-----------------------------------------------------------------------------

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// 1.2.5 
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

bool IsValidRadius( Float radius, Float roadWidth )
{
  if ( radius == ZERO ) { return (false); }

  Float angle = roadWidth / radius;

  return ((angle <= CGConsts::HALF_PI) ? true : false);
}

//-----------------------------------------------------------------------------
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

void CRoadType::AddDefaultTemplates()
{
  if ( m_width == ZERO ) { return; }

  // the basic straight part: a square
  m_templates.Append( CStraightTemplate( this, m_width, GetTexture( MAIN_STR_TEX ) ) );

//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// 1.2.5 
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  // extra straight part: quasi square
  m_templates.Append( CStraightTemplate( this, 0.9 * m_width, GetTexture( MAIN_STR_TEX ) ) );
  m_templates.Append( CStraightTemplate( this, 1.1 * m_width, GetTexture( MAIN_STR_TEX ) ) );
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

  // the basic terminator part: a square
  m_templates.Append( CStrTerminatorTemplate( this, m_width, GetTexture( MAIN_TER_TEX ) ) );


  // basic corners
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// 1.2.5 
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  const size_t numRadii = 20;
  Float radii[numRadii];
  size_t counter = 0;

//  Float smallestAngle  = static_cast< Float >( 45 ) * CGConsts::DEG_TO_RAD;
//  Float smallestRadius = static_cast< Float >( 0.1 ) + HALF * m_width;
//  if ( m_width / smallestRadius < smallestAngle )
//  {
//    radii[counter] = smallestRadius;
//    ++counter;
//  }

  {
    Float radius = m_width / (CGConsts::HALF_PI * 8 / 9);
    if ( IsValidRadius( radius, m_width ) )
    {
      radii[counter] = radius;
      ++counter;
    }
  }

  for ( size_t i = 1; i <= 10; ++i )
  {
    Float radius = m_width * i;
    if ( IsValidRadius( radius, m_width ) )
    {
      radii[counter] = radius;
      ++counter;
    }
  }

  for ( size_t i = 2; i <= 10; ++i )
  {
    Float radius = m_width * i * 10;
    if ( IsValidRadius( radius, m_width ) )
    {
      radii[counter] = radius;
      ++counter;
    }
  }



////  const size_t numRadii = 9;
////  Float radii[numRadii] = { 
////                            static_cast< Float >( 10 ),
////                            static_cast< Float >( 25 ),
////                            static_cast< Float >( 50 ),
////                            static_cast< Float >( 75 ),
////                            static_cast< Float >( 100 ),
////                            static_cast< Float >( 200 ),
////                            static_cast< Float >( 500 ),
////                            static_cast< Float >( 1000 ),
////                            static_cast< Float >( 2000 )
////                          };
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

  string tex;
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// 1.2.5 
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  for ( size_t i = 0; i < counter; ++i )

//  for ( size_t i = 0; i < numRadii; ++i )
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  {
    if ( radii[i] <= static_cast< Float >( 100 ) )
    {
      tex = GetTexture( MAIN_COR_TEX );
    }
    else
    {
      tex = GetTexture( MAIN_STR_TEX );
    }
    m_templates.Append( CCornerTemplate( this, radii[i], m_width / radii[i], tex ) );
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
// 1.2.5 
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    m_templates.Append( CCornerTemplate( this, radii[i], 0.9 * m_width / radii[i], tex ) );
    m_templates.Append( CCornerTemplate( this, radii[i], 1.1 * m_width / radii[i], tex ) );
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  }
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CRoadTypesLibrary::CRoadTypesLibrary()
{
  m_defaultProceduralTexture = "#(argb,8,8,3)color(1.0,0.0,0.0,1.0,CO)";
  SetDefault();
}

//-----------------------------------------------------------------------------

bool CRoadTypesLibrary::LoadFromCfgFile( const String& filename, ofstream& logFile )
{
  // clears the list
  Clear();

  // adds the default road type
  SetDefault();

  // adds the road types from the config file
  ifstream in( filename, std::ios::in );
  if ( in.fail() )
  {
    LOGF( Error ) << "Failed to open config file.";
    logFile << "ERROR: Failed to open the file \"" << filename << "\"." << endl;
    return (false);
  }
  else
  {
    in.close();
  }

  ParamFile parFile;
  LSError err = parFile.Parse( filename );

  if ( err != LSOK ) { return (false); }

  ParamEntryPtr entry = parFile.FindEntry( "RoadTypesLibrary" );
  if ( entry.IsNull() )
  {
    LOGF( Error ) << "Cannot find class 'RoadTypesLibrary'";
    logFile << "ERROR: Unable to find class \"RoadTypesLibrary\" inside the file \"" << filename << "\"." << endl;
    return (false);
  }
  if ( !entry->IsClass() ) 
  {
    LOGF( Error ) << "'RoadTypesLibrary' must be a class";
    logFile << "ERROR: entry \"RoadTypesLibrary\" inside the file \"" << filename << "\" must be a class." << endl;
    return (false);
  }

  ParamClassPtr roadClass = entry->GetClassInterface();
  size_t i = 1;
  char buff[20];

  do
  {
    sprintf( buff, "Road%04d", i );
    entry = roadClass->FindEntry( buff );

    if ( entry && entry->IsClass() )
    {
      ParamClassPtr item = entry->GetClassInterface();

      // typename
      String typeName = buff;
      entry = item->FindEntry( TYPENAME );            
      if ( entry.IsNull() ) 
      {
        LOGF( Error ) << "Missing " << buff << "::" << TYPENAME << " field. Using default (" << typeName << ").";
        logFile << "WARNING: Missing " << buff << "::" << TYPENAME << " field. Using default (" << typeName << ")." << endl << endl;
      }
      else
      {
        typeName = entry->GetValue();
      }

      // prefix
      String prefix = typeName;
      entry = item->FindEntry( PREFIX );            
      if ( entry.IsNull() ) 
      {
        LOGF( Error ) << "Missing " << buff << "::" << PREFIX << " field. Using default (" << prefix << ").";
        logFile << "WARNING: Missing " << buff << "::" << PREFIX << " field. Using default (" << prefix << ")." << endl << endl;
      }
      else
      {
        prefix = entry->GetValue();
      }

      // map property
      String mapP3D = "";
      entry = item->FindEntry( MAPP3D );            
      if ( entry.IsNull() ) 
      {
        LOGF( Error ) << "Missing " << buff << "::" << MAPP3D << " field. Models of this road type will be invisible in the engine's 2D map.";
        logFile << "WARNING: Missing " << buff << "::" << MAPP3D << " field. Models of this road type will be invisible in the engine's 2D map." << endl << endl;
      }
      else
      {
        mapP3D = entry->GetValue();
        if ( mapP3D == "" )
        {
          LOGF( Error ) << "Missing " << buff << "::" << MAPP3D << " field. Models of this road type will be invisible in the engine's 2D map.";
          logFile << "WARNING: Empty " << buff << "::" << MAPP3D << " field. Models of this road type will be invisible in the engine's 2D map." << endl << endl;
        }
      }

      // main straight texture
      String mainStrTex = "";
      entry = item->FindEntry( MAIN_STR_TEX );            
      if ( entry.IsNull() ) 
      {
        LOGF( Error ) << "Missing " << buff << "::" << MAIN_STR_TEX << " field. Using default procedural texture.";
        logFile << "WARNING: Missing " << buff << "::" << MAIN_STR_TEX << " field. Using default procedural texture." << endl << endl;
        mainStrTex = m_defaultProceduralTexture;
      }
      else
      {
        mainStrTex = entry->GetValue();
        ifstream tex;
        tex.open( mainStrTex, std::ios::in );
        if ( tex.fail() )
        {
          LOGF( Error ) << "Warning " << buff << "::" << MAIN_STR_TEX << " field. File not found: the model will be untextured.";
          logFile << "WARNING: " << buff << "::" << MAIN_STR_TEX << " field. File not found: the model will be untextured." << endl << endl;
        }
        else
        {
          tex.close();
        }
      }

      // special straight texture probability
      Float probSpecStrTex = ZERO;
      entry = item->FindEntry( PROB_SPEC_STR_TEX );            
      if ( entry.IsNull() ) 
      {
        LOGF( Error ) << "Missing " << buff << "::" << PROB_SPEC_STR_TEX << " field. Using default (" << probSpecStrTex << ").";
        logFile << "WARNING: Missing " << buff << "::" << PROB_SPEC_STR_TEX << " field. Using default (" << probSpecStrTex << ")." << endl << endl;
      }
      else
      {
        probSpecStrTex = *entry;
        if ( probSpecStrTex < ZERO || probSpecStrTex > static_cast< Float >( 100 ) )
        {
          LOGF( Error ) << "Invalid value for " << buff << "::" << PROB_SPEC_STR_TEX << " (" << probSpecStrTex << "). Allowed range [0..100].";
          logFile << "ERROR: Invalid value for " << buff << "::" << PROB_SPEC_STR_TEX << " (" << probSpecStrTex << "). Allowed range [0..100]." << endl << endl;
          return (false);
        }
      }

      // special straight texture
      String specStrTex = mainStrTex;
      entry = item->FindEntry( SPEC_STR_TEX );            
      if ( entry.IsNull() ) 
      {
        if ( specStrTex == "" && probSpecStrTex != ZERO )
        {
          LOGF( Error ) << "Missing " << buff << "::" << SPEC_STR_TEX << " field. Using default procedural texture.";
          logFile << "WARNING: Missing " << buff << "::" << SPEC_STR_TEX << " field. Using default procedural texture." << endl << endl;
          specStrTex = m_defaultProceduralTexture;
        }
        else
        {
          LOGF( Error ) << "Missing " << buff << "::" << SPEC_STR_TEX << " field. Using default (" << specStrTex << ").";
          logFile << "WARNING: Missing " << buff << "::" << SPEC_STR_TEX << " field. Using default (" << specStrTex << ")." << endl << endl;
        }
      }
      else
      {
        specStrTex = entry->GetValue();
        if ( probSpecStrTex != ZERO )
        {
          ifstream tex;
          tex.open( specStrTex, std::ios::in );
          if ( tex.fail() )
          {
            LOGF( Error ) << "Warning " << buff << "::" << SPEC_STR_TEX << " field. File not found: the model will be untextured.";
            logFile << "WARNING: " << buff << "::" << SPEC_STR_TEX << " field. File not found: the model will be untextured." << endl << endl;
          }
          tex.close();
        }
      }

      // main corner texture
      String mainCorTex = mainStrTex;
      entry = item->FindEntry( MAIN_COR_TEX );            
      if ( entry.IsNull() ) 
      {
        if ( mainCorTex == "" )
        {
          LOGF( Error ) << "Missing " << buff << "::" << MAIN_COR_TEX << " field. Using default procedural texture.";
          logFile << "WARNING: Missing " << buff << "::" << MAIN_COR_TEX << " field. Using default procedural texture." << endl << endl;
          mainCorTex = m_defaultProceduralTexture;
        }
        else
        {
          LOGF( Error ) << "Missing " << buff << "::" << MAIN_COR_TEX << " field. Using default (" << mainCorTex << ").";
          logFile << "WARNING: Missing " << buff << "::" << MAIN_COR_TEX << " field. Using default (" << mainCorTex << ")." << endl << endl;
        }
      }
      else
      {
        mainCorTex = entry->GetValue();
        ifstream tex;
        tex.open( mainCorTex, std::ios::in );
        if ( tex.fail() )
        {
          LOGF( Error ) << "Warning " << buff << "::" << MAIN_COR_TEX << " field. File not found: the model will be untextured.";
          logFile << "WARNING: " << buff << "::" << MAIN_COR_TEX << " field. File not found: the model will be untextured." << endl << endl;
        }
        else
        {
          tex.close();
        }
      }

      // special corner texture probability
      Float probSpecCorTex = ZERO;
      entry = item->FindEntry( PROB_SPEC_COR_TEX );            
      if ( entry.IsNull() ) 
      {
        LOGF( Error ) << "Missing " << buff << "::" << PROB_SPEC_COR_TEX << " field. Using default (" << probSpecCorTex << ").";
        logFile << "WARNING: Missing " << buff << "::" << PROB_SPEC_COR_TEX << " field. Using default (" << probSpecCorTex << ")." << endl << endl;
      }
      else
      {
        probSpecCorTex = *entry;
        if ( probSpecCorTex < ZERO || probSpecCorTex > static_cast< Float >( 100 ) )
        {
          LOGF( Error ) << "Invalid value for " << buff << "::" << PROB_SPEC_COR_TEX << "  (" << probSpecCorTex << "). Allowed range [0..100].";
          logFile << "ERROR: Invalid value for " << buff << "::" << PROB_SPEC_COR_TEX << " (" << probSpecCorTex << "). Allowed range [0..100]." << endl << endl;
          return (false);
        }
      }

      // special corner texture
      String specCorTex = mainCorTex;
      entry = item->FindEntry( SPEC_COR_TEX );            
      if ( entry.IsNull() ) 
      {
        if ( specCorTex == "" && probSpecCorTex != ZERO )
        {
          LOGF( Error ) << "Missing " << buff << "::" << SPEC_COR_TEX << " field. Using default procedural texture.";
          logFile << "WARNING: Missing " << buff << "::" << SPEC_COR_TEX << " field. Using default procedural texture." << endl << endl;
          specCorTex = m_defaultProceduralTexture;
        }
        else
        {
          LOGF( Error ) << "Missing " << buff << "::" << SPEC_COR_TEX << " field. Using default (" << specCorTex << ").";
          logFile << "WARNING: Missing " << buff << "::" << SPEC_COR_TEX << " field. Using default (" << specCorTex << ")." << endl << endl;
        }
      }
      else
      {
        specCorTex = entry->GetValue();
        if ( probSpecCorTex != ZERO )
        {
          ifstream tex;
          tex.open( specCorTex, std::ios::in );
          if ( tex.fail() )
          {
            LOGF( Error ) << "Warning " << buff << "::" << SPEC_COR_TEX << " field. File not found: the model will be untextured.";
            logFile << "WARNING: " << buff << "::" << SPEC_COR_TEX << " field. File not found: the model will be untextured." << endl << endl;
          }
          tex.close();
        }
      }

      // main terminator texture
      String mainTerTex = mainStrTex;
      entry = item->FindEntry( MAIN_TER_TEX );            
      if ( entry.IsNull() ) 
      {
        if ( mainTerTex == "" )
        {
          LOGF( Error ) << "Missing " << buff << "::" << MAIN_TER_TEX << " field. Using default procedural texture.";
          logFile << "WARNING: Missing " << buff << "::" << MAIN_TER_TEX << " field. Using default procedural texture." << endl << endl;
          mainTerTex = m_defaultProceduralTexture;
        }
        else
        {
          LOGF( Error ) << "Missing " << buff << "::" << MAIN_TER_TEX << " field. Using default (" << mainTerTex << ").";
          logFile << "WARNING: Missing " << buff << "::" << MAIN_TER_TEX << " field. Using default (" << mainTerTex << ")." << endl << endl;
        }
      }
      else
      {
        mainTerTex = entry->GetValue();
        ifstream tex;
        tex.open( mainTerTex, std::ios::in );
        if ( tex.fail() )
        {
          LOGF( Error ) << "Warning " << buff << "::" << MAIN_TER_TEX << " field. File not found: the model will be untextured.";
          logFile << "WARNING: " << buff << "::" << MAIN_TER_TEX << " field. File not found: the model will be untextured." << endl << endl;
        }
        else
        {
          tex.close();
        }
      }

      // special terminator texture probability
      Float probSpecTerTex = ZERO;
      entry = item->FindEntry( PROB_SPEC_TER_TEX );            
      if ( entry.IsNull() ) 
      {
        LOGF( Error ) << "Missing " << buff << "::" << PROB_SPEC_TER_TEX << " field. Using default (" << probSpecTerTex << ").";
        logFile << "WARNING: Missing " << buff << "::" << PROB_SPEC_TER_TEX << " field. Using default (" << probSpecTerTex << ")." << endl << endl;
      }
      else
      {
        probSpecTerTex = *entry;
        if ( probSpecTerTex < ZERO || probSpecTerTex > static_cast< Float >( 100 ) )
        {
          LOGF( Error ) << "Invalid value for " << buff << "::" << PROB_SPEC_TER_TEX << "  (" << probSpecTerTex << "). Allowed range [0..100].";
          logFile << "ERROR: Invalid value for " << buff << "::" << PROB_SPEC_TER_TEX << " (" << probSpecTerTex << "). Allowed range [0..100]." << endl << endl;
          return (false);
        }
      }

      // special terminator texture
      String specTerTex = mainTerTex;
      entry = item->FindEntry( SPEC_TER_TEX );            
      if ( entry.IsNull() ) 
      {
        if ( specTerTex == "" && probSpecTerTex != ZERO )
        {
          LOGF( Error ) << "Missing " << buff << "::" << SPEC_TER_TEX << " field. Using default procedural texture.";
          logFile << "WARNING: Missing " << buff << "::" << SPEC_TER_TEX << " field. Using default procedural texture." << endl << endl;
          specTerTex = m_defaultProceduralTexture;
        }
        else
        {
          LOGF( Error ) << "Missing " << buff << "::" << SPEC_TER_TEX << " field. Using default (" << specTerTex << ").";
          logFile << "WARNING: Missing " << buff << "::" << SPEC_TER_TEX << " field. Using default (" << specTerTex << ")." << endl << endl;
        }
      }
      else
      {
        specTerTex = entry->GetValue();
        if ( probSpecTerTex != ZERO )
        {
          ifstream tex;
          tex.open( specTerTex, std::ios::in );
          if ( tex.fail() )
          {
            LOGF( Error ) << "Warning " << buff << "::" << SPEC_TER_TEX << " field. File not found: the model will be untextured.";
            logFile << "WARNING: " << buff << "::" << SPEC_TER_TEX << " field. File not found: the model will be untextured." << endl << endl;
          }
          tex.close();
        }
      }

      // minimum distance between specials
      Float minSpecDistF = 10;
      entry = item->FindEntry( MIN_SPEC_DIST );            
      if ( entry.IsNull() ) 
      {
        LOGF( Error ) << "Missing " << buff << "::" << MIN_SPEC_DIST << " field. Using default (" << minSpecDistF << ").";
        logFile << "WARNING: Missing " << buff << "::" << MIN_SPEC_DIST << " field. Using default (" << minSpecDistF << ")." << endl << endl;
      }
      else
      {
        minSpecDistF = *entry;
        if ( minSpecDistF < ZERO )
        {
          LOGF( Error ) << "Invalid value for " << buff << "::" << MIN_SPEC_DIST << "  (" << minSpecDistF << "). It must be a positive value.";
          logFile << "ERROR: Invalid value for " << buff << "::" << MIN_SPEC_DIST << "  (" << minSpecDistF << "). It must be a positive value." << endl << endl;
          return (false);
        }
      }
      size_t minSpecDist = static_cast< size_t >( minSpecDistF );

      // corner resolution
      Float cornerRes = TWO;
      entry = item->FindEntry( CORNER_RES );            
      if ( entry.IsNull() ) 
      {
        LOGF( Error ) << "Missing " << buff << "::" << CORNER_RES << " field. Using default (" << cornerRes << ").";
        logFile << "WARNING: Missing " << buff << "::" << CORNER_RES << " field. Using default (" << cornerRes << ")." << endl << endl;
      }
      else
      {
        cornerRes = *entry;
        if ( cornerRes < ONE )
        {
          LOGF( Error ) << "Invalid value for " << buff << "::" << CORNER_RES << "  (" << cornerRes << "). It must be greater than or equal to 1.";
          logFile << "ERROR: Invalid value for " << buff << "::" << CORNER_RES << " (" << cornerRes << "). It must be greater than or equal to 1." << endl << endl;
          return (false);
        }
      }

      // proximity factor
      Float proxFactor = HALF;
      entry = item->FindEntry( PROX_FACTOR );            
      if ( entry.IsNull() ) 
      {
        LOGF( Error ) << "Missing " << buff << "::" << PROX_FACTOR << " field. Using default (" << proxFactor << ").";
        logFile << "WARNING: Missing " << buff << "::" << PROX_FACTOR << " field. Using default (" << proxFactor << ")." << endl << endl;
      }
      else
      {
        proxFactor = *entry;
        if ( proxFactor <= ZERO )
        {
          LOGF( Error ) << "Invalid value for " << buff << "::" << PROX_FACTOR << "  (" << proxFactor << "). It must be positive.";
          logFile << "ERROR: Invalid value for " << buff << "::" << PROX_FACTOR << " (" << proxFactor << "). It must be positive." << endl << endl;
          return (false);
        }
      }

      // match threshold
      Float matchThreshold = DEF_MATCH_THRESHOLD;
      entry = item->FindEntry( MATCH_THRESHOLD );            
      if ( entry.IsNull() ) 
      {
        LOGF( Error ) << "Missing " << buff << "::" << MATCH_THRESHOLD << " field. Using default (" << matchThreshold << ").";
        logFile << "WARNING: Missing " << buff << "::" << MATCH_THRESHOLD << " field. Using default (" << matchThreshold << ")." << endl << endl;
      }
      else
      {
        matchThreshold = *entry;
        if ( matchThreshold < ZERO )
        {
          LOGF( Error ) << "Invalid value for " << buff << "::" << MATCH_THRESHOLD << "  (" << matchThreshold << "). It must be positive.";
          logFile << "ERROR: Invalid value for " << buff << "::" << MATCH_THRESHOLD << " (" << matchThreshold << "). It must be positive." << endl << endl;
          return (false);
        }
      }

      map< string, string > textures;
      textures.insert( pair< String, String >( MAIN_STR_TEX, mainStrTex ) );
      textures.insert( pair< String, String >( SPEC_STR_TEX, specStrTex ) );
      textures.insert( pair< String, String >( MAIN_COR_TEX, mainCorTex ) );
      textures.insert( pair< String, String >( SPEC_COR_TEX, specCorTex ) );
      textures.insert( pair< String, String >( MAIN_TER_TEX, mainTerTex ) );
      textures.insert( pair< String, String >( SPEC_TER_TEX, specTerTex ) );

      CRoadType* type = new CRoadType( typeName, prefix, mapP3D, textures, probSpecStrTex, 
                                       probSpecCorTex, probSpecTerTex, minSpecDist, cornerRes,
                                       proxFactor, matchThreshold );
      m_elements.push_back( type );
    }

    ++i;
    if ( i > 9999 ) { break; }
  }
  while ( true );

  return (true);
}

//-----------------------------------------------------------------------------

int CRoadTypesLibrary::FindByNameAndWidth( const String& name, Float width ) const
{
  for ( size_t i = 0, cntI = m_elements.size(); i < cntI; ++i )
  {
    if ( m_elements[i]->GetName() == name && m_elements[i]->GetWidth() == width ) 
    { 
      return ( static_cast< int >( i ) ); 
    }
  }

  return (-1);
}

//-----------------------------------------------------------------------------

void CRoadTypesLibrary::SetDefault()
{
  String typeName = DEFAULT_ROAD;
  String prefix   = DEFAULT_ROAD;

  // no textures
  map< string, string > textures;
  textures.insert( pair< String, String >( MAIN_STR_TEX, m_defaultProceduralTexture ) );
  textures.insert( pair< String, String >( SPEC_STR_TEX, m_defaultProceduralTexture ) );
  textures.insert( pair< String, String >( MAIN_COR_TEX, m_defaultProceduralTexture ) );
  textures.insert( pair< String, String >( SPEC_COR_TEX, m_defaultProceduralTexture ) );
  textures.insert( pair< String, String >( MAIN_TER_TEX, m_defaultProceduralTexture ) );
  textures.insert( pair< String, String >( SPEC_TER_TEX, m_defaultProceduralTexture ) );

  // no special templates
  Float probSpecStrTex = ZERO;
  Float probSpecCorTex = ZERO;
  Float probSpecTerTex = ZERO;
  size_t minSpecDist = 0;

  // corner resolution
  Float cornerRes = TWO;

  // proximity factor
  Float proxFactor = HALF;

  // match threshold
  Float matchThreshold = DEF_MATCH_THRESHOLD;

  CRoadType* type = new CRoadType( typeName, prefix, "road", textures, probSpecStrTex, probSpecCorTex,
                                   probSpecTerTex, minSpecDist, cornerRes, proxFactor, matchThreshold );
  m_elements.push_back( type );
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CRoad::CRoad( CRoadType* type, bool useSplines )
: m_type( type )
, m_useSplines( useSplines )
, m_startJoint( NULL )
, m_endJoint( NULL )
, m_startDirectionConstraint( CGConsts::MAX_REAL ) 
, m_endDirectionConstraint( CGConsts::MAX_REAL )
, m_built( false )
, m_exported( false )
, m_degenerated( false )
{
}

//-----------------------------------------------------------------------------

CRoad::CRoad( const CRoad& other )
: m_type( other.m_type )
, m_useSplines( other.m_useSplines )
, m_path( other.m_path )
, m_startJoint( other.m_startJoint )
, m_endJoint( other.m_endJoint )
, m_startDirectionConstraint( other.m_startDirectionConstraint ) 
, m_endDirectionConstraint( other.m_endDirectionConstraint )
, m_built( other.m_built )
, m_exported( other.m_exported )
, m_degenerated( other.m_degenerated )
{
}

//-----------------------------------------------------------------------------

const CRoadType* CRoad::GetType() const
{
  return (m_type);
}

//-----------------------------------------------------------------------------

CRoadType* CRoad::GetType()
{
  return (m_type);
}

//-----------------------------------------------------------------------------

const CGPolyline2& CRoad::GetPath() const
{
  return (m_path);
}

//-----------------------------------------------------------------------------

CGPolyline2& CRoad::GetPath()
{
  return (m_path);
}

//-----------------------------------------------------------------------------

const CJoint* CRoad::GetStartJoint() const
{
  return (m_startJoint);
}

//-----------------------------------------------------------------------------

CJoint* CRoad::GetStartJoint()
{
  return (m_startJoint);
}


//-----------------------------------------------------------------------------

const CJoint* CRoad::GetEndJoint() const
{
  return (m_endJoint);
}


//-----------------------------------------------------------------------------

CJoint* CRoad::GetEndJoint()
{
  return (m_endJoint);
}


//-----------------------------------------------------------------------------

Float CRoad::GetStartDirectionConstraint() const
{
  return (m_startDirectionConstraint);
}

//-----------------------------------------------------------------------------

Float CRoad::GetEndDirectionConstraint() const
{
  return (m_endDirectionConstraint);
}

//-----------------------------------------------------------------------------

bool CRoad::IsBuilt() const
{
  return (m_built);
}

//-----------------------------------------------------------------------------

bool CRoad::IsExported() const
{
  return (m_exported);
}

//-----------------------------------------------------------------------------

const vector< CRoadPartModel >& CRoad::GetModels() const
{
  return (m_models);
}

//-----------------------------------------------------------------------------

vector< CRoadPartModel >& CRoad::GetModels()
{
  return (m_models);
}

//-----------------------------------------------------------------------------

bool CRoad::UseSplines() const
{
  return (m_useSplines);
}

//-----------------------------------------------------------------------------

bool CRoad::IsDegenerated() const
{
  return (m_degenerated);
}

//-----------------------------------------------------------------------------

void CRoad::SetStartJoint( CJoint* joint )
{
  m_startJoint = joint; 
}

//-----------------------------------------------------------------------------

void CRoad::SetEndJoint( CJoint* joint )
{
  m_endJoint = joint; 
}

//-----------------------------------------------------------------------------

void CRoad::SetStartDirectionConstraint( Float direction )
{
  m_startDirectionConstraint = direction;
}

//-----------------------------------------------------------------------------

void CRoad::SetEndDirectionConstraint( Float direction )
{
  m_endDirectionConstraint = direction;
}

//-----------------------------------------------------------------------------

void CRoad::SetExported( bool exported )
{
  m_exported = exported;
}

//-----------------------------------------------------------------------------

void CRoad::SetDegenerated( bool degenerated )
{
  m_degenerated = degenerated;
}

//-----------------------------------------------------------------------------

bool CRoad::Equals( const CRoad& other ) const
{
  if ( this == &other ) { return (true); }

  return (m_path.Equals( other.m_path ));
}

//-----------------------------------------------------------------------------

bool CRoad::EqualsReverse( const CRoad& other ) const
{
  return (m_path.EqualsReverse( other.m_path ));
}

//-----------------------------------------------------------------------------

void CRoad::ReverseVerticesOrder()
{
  // reverses vertices
  m_path.ReverseVerticesOrder();

  // swaps crossroads' pointers
  CJoint* tempJ = m_startJoint;
  m_startJoint  = m_endJoint;
  m_endJoint    = tempJ;

  // swaps orientation constraints
  Float temp                 = m_startDirectionConstraint + CGConsts::PI;
  m_startDirectionConstraint = m_endDirectionConstraint + CGConsts::PI;
  m_endDirectionConstraint   = temp;
}

//-----------------------------------------------------------------------------

bool CRoad::IsValid( Float tolerance ) const
{
  if ( m_path.VerticesCount() < 2 ) { return (false); }
  if ( m_path.HasConsecutiveOverlappingSegments( tolerance ) ) { return (false); }

  // we consider as valid the roads which have:
  // autointersections (in this case no crossroad will be detected in the
  //                    intersection point)
  // overlapping non consecutive segments 
  // TODO: log warning when these cases happen

  return (true);
}

//-----------------------------------------------------------------------------

void CRoad::ConvertToSpline()
{
  class DVertexEx : public Shapes::DVertex
  {
  public:
    DVertexEx( double x = 0.0, double y = 0.0, double z = DBL_MIN, 
               double m = DBL_MIN ) 
    : DVertex( x, y, z, m )
    {
    }

    DVertexEx( const DVector& v, double m = DBL_MIN ) 
    : DVertex( v, m ) 
    {
    }

    DVertexEx( const DVertex& v )
    : DVertex( v ) 
    {
    }

    double& operator [] ( size_t index )
    {        
      switch ( index )
      {
      default:
      case 0: { return (x); }
      case 1: { return (y); }
      case 2: { return (z); }
      case 3: { return (m); }
      }
    }

    ClassIsSimpleZeroed( DVertexEx );
  };

  CGPolyline2 spline;

  AutoArray< DVertexEx > points;
  size_t cntPath = m_path.VerticesCount();
  points.Reserve( cntPath + 4 );

  const CGPoint2& firstV = m_path.Vertex( 0 );
  const CGPoint2& lastV  = m_path.Vertex( cntPath - 1 );

  points.Add( DVertexEx( firstV.GetX(), firstV.GetY() ) );
  points.Add( DVertexEx( firstV.GetX(), firstV.GetY() ) );

  for ( size_t j = 0 ; j < cntPath; ++j )
  {
    const CGPoint2& point = m_path.Vertex( j );
    points.Add( DVertexEx( point.GetX(), point.GetY() ) );
  }

  points.Add( DVertexEx( lastV.GetX(), lastV.GetY() ) );
  points.Add( DVertexEx( lastV.GetX(), lastV.GetY() ) );

  BSpline< 4, double, Array< DVertexEx > > bspline( points, points.Size() );

  double curT  = 0.0;
  double maxT  = bspline.MaxT();
  double width = m_type->GetWidth();

  spline.AppendVertex( m_path.Vertex( 0 ) );

  while ( fabs( curT - maxT ) > CGConsts::ZERO_TOLERANCE ) 
  {          
    double pos[4];
    curT = bspline.GetNextPoint( curT, width, NULL, pos );
    spline.AppendVertex( pos[0], pos[1] );
  }		

  size_t cntSpline = spline.VerticesCount();
  double last = spline.Vertex( cntSpline - 2 ).Distance( spline.Vertex( cntSpline - 1 ) );

  if ( last < 0.1 )
  {
    spline.RemoveVertex( cntSpline - 1 );
    --cntSpline;
  }

  spline.Vertex( cntSpline - 1 ) = m_path.Vertex( cntPath - 1 );

  if ( spline.VerticesCount() > 2 )
  {
    m_path = spline;
  }
}

//-----------------------------------------------------------------------------

CRoad CRoad::Split( size_t method )
{
  vector< CGPolyline2 > newPaths;

  switch ( method )
  {
  case 0: { newPaths = m_path.SplitInTwoHalves( TOLERANCE );      break; }
  case 1: { newPaths = m_path.SplitOnLongestSegment( TOLERANCE ); break; }
  }

  // updates this road
  m_path.RemoveAllVertices();
  for ( size_t i = 0, cntI = newPaths[0].VerticesCount(); i < cntI; ++i )
  {
    m_path.AppendVertex( newPaths[0].Vertex( i ) );
  }

  // creates new road
  CRoad newRoad( m_type, m_useSplines );
  CGPolyline2& newPath = newRoad.GetPath();
  for ( size_t i = 0, cntI = newPaths[1].VerticesCount(); i < cntI; ++i )
  {
    newPath.AppendVertex( newPaths[1].Vertex( i ) );
  }

  // updates endpoints
  newRoad.SetEndJoint( GetEndJoint() );
  newRoad.SetEndDirectionConstraint( GetEndDirectionConstraint() );

  SetEndJoint( NULL );
  SetEndDirectionConstraint( CGConsts::MAX_REAL );

  // return the new road
  return (newRoad);
}

//-----------------------------------------------------------------------------

void CRoad::BuildModels()
{
  ClearModels();

  Float limit = m_path.Length();
  if ( m_endJoint && (m_endJoint->Type() == JT_NullJoint) )
  {
    limit -= m_type->GetWidth();
  }
  else
  {
    limit -= (Float)1.5 * m_type->GetWidth();
  }
  Float maxModelsLength = TWO * m_path.Length();

  Float halfRange = TWO * m_type->GetWidth();

  CGPoint2 currPosition     = m_path.FirstVertex();
  Float    currDirection    = m_startDirectionConstraint;
  Float    currLength       = ZERO;
  Float    currModelsLength = ZERO;

  // if the first endpoint is free we start with an inverted terminator
  if ( !m_startJoint )
  {
    CRoadPartTemplate* term = m_type->GetTemplates().GetTerminator();
    m_models.push_back( CRoadPartModel( term, currPosition, currDirection, true ) );
    currPosition      = m_models[m_models.size() - 1].GetNextPosition();
    currDirection     = m_models[m_models.size() - 1].GetNextDirection();
    currLength        = m_path.LengthAtClosestPointTo( m_models[m_models.size() - 1].GetNextPosition(), TOLERANCE );
    currModelsLength += m_models[m_models.size() - 1].Length();
  }

  CGPolyline2        currPath;
  CRoadPartTemplate* nextTemplate;
  bool               inverted;

  while ( (currLength < limit) && (currModelsLength < maxModelsLength) )
  {
    currPath = m_path.SubPart( currLength - halfRange, currLength + halfRange );

    // first: see if any of the already existing templates fits the path
    nextTemplate = m_type->ClosestTemplateToPath( currPosition, currDirection, currPath, true, inverted );

    if ( !nextTemplate )
    {
      // second: try to build a new template fitting the path
      nextTemplate = m_type->CreateNewCornerTemplate( currPosition, currDirection, currPath );
    }

    if ( !nextTemplate )
    {
      // third: apply the best non fitting template
      if ( !currPath.LastVertex().Equals( m_path.LastVertex(), TOLERANCE ) )
      {
        nextTemplate = m_type->ClosestTemplateToPath( currPosition, currDirection, currPath, false, inverted );
      }
    }

    if ( nextTemplate )
    {
      // adds a new model to the list
      m_models.push_back( CRoadPartModel( nextTemplate, currPosition, currDirection, inverted ) );
      currPosition      = m_models[m_models.size() - 1].GetNextPosition();
      currDirection     = m_models[m_models.size() - 1].GetNextDirection();
      currLength        = m_path.LengthAtClosestPointTo( m_models[m_models.size() - 1].GetNextPosition(), TOLERANCE );
      currModelsLength += m_models[m_models.size() - 1].Length();
    }
    else
    {
      m_built = false;
      return;
    }
  }

  // if the second endpoint is free we add a terminator
  if ( !m_endJoint )
  {
    CRoadPartTemplate* term = m_type->GetTemplates().GetTerminator();
    m_models.push_back( CRoadPartModel( term, currPosition, currDirection ) );
  }

  m_built = true;
}

//-----------------------------------------------------------------------------

void CRoad::ClearModels()
{
  m_models.clear();
  m_built = false;
}

//-----------------------------------------------------------------------------

void CRoad::AddSpecialModels()
{
  Float probSpecStrTex = m_type->GetProbSpecStrTex();
  Float probSpecCorTex = m_type->GetProbSpecCorTex();
  Float probSpecTerTex = m_type->GetProbSpecTerTex();

  size_t minDist = m_type->GetMinSpecDist();
  size_t lastInsertedPos = 0;

  for ( size_t i = 0, cntI = m_models.size(); i < cntI; ++i )
  {
    // we check all road's models
    if ( i - lastInsertedPos >= minDist )
    {
      // we perform the check only if the distance threshold
      // is matched
      CRoadPartModel& currModel = m_models[i];
      CRoadPartTemplate* newPart = NULL;

      ETemplateType partType = currModel.GetTemplate()->Type();

      if ( partType == TT_Straight && probSpecStrTex > ZERO )
      {
        if ( etRandomSelect( probSpecStrTex ) )
        {
          string tex = m_type->GetTexture( SPEC_STR_TEX );
          const CRoadPartTemplate* currPart = reinterpret_cast< const CRoadPartTemplate* >( currModel.GetTemplate() );
          Float length = currPart->Length();
          newPart = m_type->GetTemplates().Append( CStraightTemplate( m_type, length, tex, true ) );
        }
      }
      else if ( partType == TT_Corner && probSpecCorTex > ZERO )
      {			
        if ( etRandomSelect( probSpecCorTex ) )
        {
          string tex = m_type->GetTexture( SPEC_COR_TEX );
          const CCornerTemplate* corner = reinterpret_cast< const CCornerTemplate* >( currModel.GetTemplate() );
          Float radius = corner->GetRadius();
          Float angle  = corner->GetAngle();
          newPart = m_type->GetTemplates().Append( CCornerTemplate( m_type, radius, angle, tex, true ) );
        }
      }
      else if ( partType == TT_StrTerminator && probSpecTerTex > ZERO )
      {			
        if ( etRandomSelect( probSpecTerTex ) )
        {
          string tex = m_type->GetTexture( SPEC_TER_TEX );
          const CRoadPartTemplate* currPart = reinterpret_cast< const CRoadPartTemplate* >( currModel.GetTemplate() );
          Float length = currPart->Length();
          newPart = m_type->GetTemplates().Append( CStrTerminatorTemplate( m_type, length, tex, true ) );
        }
      }

      if ( newPart )
      {
        bool     inverted     = currModel.IsInverted();
        CGPoint2 insPosition  = currModel.GetCreationPosition();
        Float    insDirection = currModel.GetCreationDirection();

        // we create a new model to force the calculation of its properties
        // (see RoadPartModel constructor)
        // and then replaces the current model with the new one
        currModel = CRoadPartModel( newPart, insPosition, insDirection, inverted );

        lastInsertedPos = i;
      }
    }
  }
}

//-----------------------------------------------------------------------------

void CRoad::ReduceModels()
{
  if ( m_models.size() < 2 ) { return; }

  bool done = false;

  while ( !done )
  {
    done = true;

    for ( size_t i = 0, cntI = m_models.size() - 1; i < cntI; ++i )
    {
      const CRoadPartModel& currModelI = m_models[i];
      const CRoadPartModel& nextModelI = m_models[i + 1];

      // if two consecutive models are identical
      if ( currModelI.SameAs( nextModelI ) )
      {
        // first we search the library to see if the doubled template has
        // been already created while reducing another road
        const CRoadPartTemplate* currPartI = reinterpret_cast< const CRoadPartTemplate* >( currModelI.GetTemplate() );
        CRoadPartTemplate* newPart = m_type->GetTemplates().FindDoubled( *currPartI );

        if ( !newPart )
        {
          // not found
          // we create a template twice bigger than the current one only if
          // it is still smaller than the max allowed size
          if ( TWO * currPartI->Length() <= MAX_ROAD_WIDTH )
          {
            string tex = "";
            const vector< string >& textures = currPartI->GetTextures();
            if ( textures.size() > 0 ) { tex = textures[0]; }
            bool special = currPartI->IsSpecial();

            if ( currPartI->Type() == TT_Straight )
            {
              Float length = TWO * currPartI->Length();
              newPart = m_type->GetTemplates().Append( CStraightTemplate( m_type, length, tex, special ) );
              done = false;
            }
            else if ( currPartI->Type() == TT_Corner )
            {
              const CCornerTemplate* corner = reinterpret_cast< const CCornerTemplate* >( currPartI );
              Float radius = corner->GetRadius();
              Float angle  = TWO * corner->GetAngle();
              newPart = m_type->GetTemplates().Append( CCornerTemplate( m_type, radius, angle, tex, special ) );
              done = false;
            }
          }
        }
        else
        {
          // found
          done = false;
        }

        if ( newPart )
        {
          // now that we have the new template, we will substitute it along all the 
          // remaining road
          size_t j = i;
          while ( j < m_models.size() - 1 )
          {
            const CRoadPartModel& currModelJ = m_models[j];
            if ( currModelJ.SameAs( currModelI ) )
            {
              const CRoadPartModel& nextModelJ = m_models[j + 1];
              
              if ( currModelJ.SameAs( nextModelJ ) )
              {
                bool     inverted     = currModelJ.IsInverted();
                CGPoint2 insPosition  = currModelJ.GetCreationPosition();
                Float    insDirection = currModelJ.GetCreationDirection();

                // we create a new model to force the calculation of its properties
                // (see RoadPartModel constructor)
                // and then replaces the model in j position
                m_models[j] = CRoadPartModel( newPart, insPosition, insDirection, inverted );

                // removes the model in j + 1 position
                m_models.erase( m_models.begin() + j + 1 );
              }
            }
            ++j;
          }

          // we stop here the current for loop
          break;
        }
      }
    }
  }
}

//-----------------------------------------------------------------------------

void CRoad::ReplaceModel( size_t index, const CRoadPartModel& model )
{
  size_t size = m_models.size();
  if ( index >= size ) { return; }

  m_models[index] = model;
  for ( size_t i = index + 1; i < size; ++i )
  {
    m_models[i].Redefine( m_models[i - 1].GetNextPosition(), m_models[i - 1].GetNextDirection() );
  }
}

//-----------------------------------------------------------------------------

void CRoad::InsertModel( size_t index, const CRoadPartModel& model )
{
  size_t size = m_models.size();
  if ( index >= size ) { return; }

  vector< CRoadPartModel > tempModels;

  size_t counter = 0;
  for ( size_t i = 0; i <= index; ++i )
  {
    tempModels.push_back( m_models[i] );
    ++counter;
  }

  if ( counter > 0 )
  {
    CRoadPartModel newModel( model );
    newModel.Redefine( tempModels[counter - 1].GetNextPosition(), tempModels[counter - 1].GetNextDirection() ); 
    tempModels.push_back( newModel );
    ++counter;

    for ( size_t i = index + 1; i < size; ++i )
    {
      CRoadPartModel newModel( m_models[i] );
      newModel.Redefine( tempModels[counter - 1].GetNextPosition(), tempModels[counter - 1].GetNextDirection() ); 
      tempModels.push_back( newModel );
      ++counter;
    }

    m_models = tempModels;
  }
}

//-----------------------------------------------------------------------------

void CRoad::RemoveModel( size_t index )
{
  size_t size = m_models.size();
  if ( index >= size ) { return; }

  vector< CRoadPartModel > tempModels;

  size_t counter = 0;
  for ( size_t i = 0; i < index; ++i )
  {
    tempModels.push_back( m_models[i] );
    ++counter;
  }

  for ( size_t i = index + 1; i < size; ++i )
  {
    CRoadPartModel newModel( m_models[i] );
    newModel.Redefine( tempModels[counter - 1].GetNextPosition(), tempModels[counter - 1].GetNextDirection() ); 
    tempModels.push_back( newModel );
    ++counter;
  }

  m_models = tempModels;
}

//-----------------------------------------------------------------------------

const CRoadPartModel* CRoad::LastModel() const
{
  if ( m_models.size() > 0 ) { return (&m_models[m_models.size() - 1]); }
  return (NULL);
}

//-----------------------------------------------------------------------------

std::ostream& operator << ( std::ostream& o, const CRoad& road )
{
  o << " road of type " << road.GetType()->ExportName();
  o << " - from " << road.GetPath().FirstVertex();
  o << " to " << road.GetPath().LastVertex();

  return (o);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CRoadsSet::CRoadsSet()
{
}

//-----------------------------------------------------------------------------

CRoadsSet::CRoadsSet( const CRoadsSet& other )
{
  for ( size_t i = 0, cntI = other.m_roads.size(); i < cntI; ++i )
  {
    if ( other.m_roads[i] )
    {
      m_roads.push_back( other.m_roads[i] );
    }
    else
    {
      assert( false );
    }
  }
}

//-----------------------------------------------------------------------------

bool CRoadsSet::Equals( const CRoadsSet& other ) const
{
  size_t thisSize  = m_roads.size();
  size_t otherSize = other.m_roads.size();

  if ( thisSize != otherSize ) { return (false); }

  for ( size_t i = 0; i < thisSize; ++i )
  {
    if ( m_roads[i]->GetType() != other.m_roads[i]->GetType() ) { return (false); }
  }

  return (true);
}

//-----------------------------------------------------------------------------

const CRoad* CRoadsSet::Get( size_t index ) const
{
  assert( index < m_roads.size() );
  return (m_roads[index]);
}

//-----------------------------------------------------------------------------

CRoad* CRoadsSet::Get( size_t index )
{
  assert( index < m_roads.size() );
  return (m_roads[index]);
}

//-----------------------------------------------------------------------------

void CRoadsSet::Set( size_t index, CRoad* road )
{
  assert( index < m_roads.size() );
  m_roads[index] = road;
}

//-----------------------------------------------------------------------------

void CRoadsSet::Add( CRoad* road )
{
  if ( road ) { m_roads.push_back( road ); }
}

//-----------------------------------------------------------------------------

void CRoadsSet::Remove( size_t index )
{
  assert( index < m_roads.size() );
  m_roads.erase( m_roads.begin() + index );
}

//-----------------------------------------------------------------------------

void CRoadsSet::Remove( CRoad* road )
{
  for ( size_t i = 0, cntI = m_roads.size(); i < cntI; ++i )
  {
    if ( m_roads[i] == road )
    {
      Remove( i );
      return;
    }
  }
}

//-----------------------------------------------------------------------------

void CRoadsSet::Replace( CRoad* oldRoad, CRoad* newRoad )
{
  if ( oldRoad && newRoad )
  {
    for ( size_t i = 0, cntI = m_roads.size(); i < cntI; ++i )
    {
      if ( m_roads[i] == oldRoad ) { m_roads[i] = newRoad; }
    }
  }
}

//-----------------------------------------------------------------------------

void CRoadsSet::SortByWidth()
{
  sort( m_roads.begin(), m_roads.end(), RoadWidthLess() );
}

//-----------------------------------------------------------------------------

void CRoadsSet::SortByDirection()
{
  sort( m_roads.begin(), m_roads.end(), RoadDirectionLess() );
}

//-----------------------------------------------------------------------------

void CRoadsSet::Clear()
{
  m_roads.clear();
}

//-----------------------------------------------------------------------------

size_t CRoadsSet::Size() const
{
  return (m_roads.size());
}

//-----------------------------------------------------------------------------

size_t CRoadsSet::TypesCount() const
{
  vector< String > types;
  for ( size_t r = 0, cntR = m_roads.size(); r < cntR; ++r )
  {
    String type = m_roads[r]->GetType()->ExportName();
    bool found = false;
    for ( size_t t = 0, cntT = types.size(); t < cntT; ++t )
    {
      if ( type == types[t] )
      {
        found = true;
        break;
      }
    }
    if ( !found )
    {
      types.push_back( type );
    }
  }

  return (types.size());
}

//-----------------------------------------------------------------------------

Float CRoadsSet::MaxWidth() const
{
  Float maxWidth = ZERO;
  Float currWidth;
  for ( size_t i = 0, cntI = m_roads.size(); i < cntI; ++i )
  {
    currWidth = m_roads[i]->GetType()->GetWidth();
    if ( maxWidth < currWidth ) { maxWidth = currWidth; }
  }

  return (maxWidth);
}

//-----------------------------------------------------------------------------
