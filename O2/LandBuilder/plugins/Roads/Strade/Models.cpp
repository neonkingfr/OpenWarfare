//-----------------------------------------------------------------------------
// File: Models.cpp
//
// Desc: classes and types related to road models
//
// Auth: Enrico Turri 
//
// Copyright (c) 2008 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Models.h"

//-----------------------------------------------------------------------------

CModel::CModel()
: m_template( NULL )
, m_creationPosition( CGPoint2() ) 
, m_creationDirection( ZERO ) 
, m_position( CGPoint2() ) 
, m_direction( ZERO ) 
, m_visitorDirection( ZERO ) 
, m_visitorPosition( CGPoint2() ) 
{
}

//-----------------------------------------------------------------------------

CModel::CModel( CRoadBaseTemplate* templ, const CGPoint2& creationPosition,
                Float creationDirection )
: m_template( templ )
, m_creationPosition( creationPosition ) 
, m_creationDirection( creationDirection ) 
{
	assert( m_template );
	m_template->IncreaseReferencesCount();
}

//-----------------------------------------------------------------------------

CModel::CModel( const CModel& other )
: m_template( other.m_template )
, m_creationPosition( other.m_creationPosition ) 
, m_creationDirection( other.m_creationDirection ) 
, m_position( other.m_position ) 
, m_direction( other.m_direction )
, m_visitorDirection( other.m_visitorDirection )
, m_visitorPosition( other.m_visitorPosition ) 
{
	m_template->IncreaseReferencesCount();
}

//-----------------------------------------------------------------------------

CModel::~CModel()
{
	if ( m_template ) { m_template->DecreaseReferencesCount(); }
}

//-----------------------------------------------------------------------------

CModel& CModel::operator = ( const CModel& other )
{
	if ( m_template ) { m_template->DecreaseReferencesCount(); }

	m_template          = other.m_template;
	m_creationPosition  = other.m_creationPosition; 
	m_creationDirection = other.m_creationDirection;
	m_position          = other.m_position; 
	m_direction         = other.m_direction;
	m_visitorDirection  = other.m_visitorDirection; 
	m_visitorPosition   = other.m_visitorPosition; 

	assert( m_template );
	m_template->IncreaseReferencesCount();

	return (*this);
}

//-----------------------------------------------------------------------------

const CRoadBaseTemplate* CModel::GetTemplate() const
{
	return (m_template);
}

//-----------------------------------------------------------------------------

CRoadBaseTemplate* CModel::GetTemplate()
{
	return (m_template);
}

//-----------------------------------------------------------------------------

const CGPoint2& CModel::GetCreationPosition() const
{
	return (m_creationPosition);
}

//-----------------------------------------------------------------------------

const CGPoint2& CModel::GetPosition() const
{
	return (m_position);
}

//-----------------------------------------------------------------------------

const CGPoint2& CModel::GetVisitorPosition() const
{
	return (m_visitorPosition);
}

//-----------------------------------------------------------------------------

Float CModel::GetDirection() const
{
	return (m_direction);
}

//-----------------------------------------------------------------------------

Float CModel::GetCreationDirection() const
{
	return (m_creationDirection);
}

//-----------------------------------------------------------------------------

Float CModel::GetVisitorDirection() const
{
	return (m_visitorDirection);
}

//-----------------------------------------------------------------------------

vector< CGPolygon2 > CModel::Geometry() const
{
	vector< CGPolygon2 > ret( m_template->GetGeometry() );
	for ( size_t i = 0, cntI = ret.size(); i < cntI; ++i )
	{
		ret[i].Translate( m_position.GetX(), m_position.GetY() );
		ret[i].Rotate( m_position, m_direction );
	}
	return (ret);
}

//-----------------------------------------------------------------------------

void CModel::Initialize()
{
	CalcPosition();
	CalcDirection();
	CalcVisitorPosition();
	CalcVisitorDirection();
}

//-----------------------------------------------------------------------------

void CModel::CalcVisitorPosition()
{
	CGPoint2 center = m_template->GeometricCenter();
	center.Translate( m_position.GetX(), m_position.GetY() );
	center.Rotate( m_position, m_direction );

	m_visitorPosition = center;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CCrossroadModel::CCrossroadModel()
: CModel()
{
}

//-----------------------------------------------------------------------------

CCrossroadModel::CCrossroadModel( CRoadBaseTemplate* templ, const CGPoint2& creationPosition, 
                                  Float creationDirection )
: CModel( templ, creationPosition, creationDirection )
{
	Initialize();
}

//-----------------------------------------------------------------------------

CCrossroadModel::CCrossroadModel( const CCrossroadModel& other )
: CModel( other )
{
}

//-----------------------------------------------------------------------------

void CCrossroadModel::CalcPosition()
{
	m_position = m_creationPosition;
}

//-----------------------------------------------------------------------------

void CCrossroadModel::CalcDirection()
{
	m_direction = m_creationDirection - CGConsts::HALF_PI;
}

//-----------------------------------------------------------------------------

void CCrossroadModel::CalcVisitorDirection()
{
	m_visitorDirection = CGConsts::HALF_PI - m_creationDirection;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CRoadPartModel::CRoadPartModel()
: CModel()
, m_nextPosition( CGPoint2() ) 
, m_nextDirection( ZERO ) 
, m_inverted( false )
{
}

//-----------------------------------------------------------------------------

CRoadPartModel::CRoadPartModel( CRoadBaseTemplate* templ, const CGPoint2& creationPosition, 
                                Float creationDirection, bool inverted )
: CModel( templ, creationPosition, creationDirection )
, m_inverted( inverted )
{
	Initialize();
	CalcNextPosition();
	CalcNextDirection();
}

//-----------------------------------------------------------------------------

CRoadPartModel::CRoadPartModel( const CRoadPartModel& other )
: CModel( other )
, m_nextPosition( other.m_nextPosition ) 
, m_nextDirection( other.m_nextDirection ) 
, m_inverted( other.m_inverted )
{
}

//-----------------------------------------------------------------------------

CRoadPartModel::~CRoadPartModel()
{
}

//-----------------------------------------------------------------------------

CRoadPartModel& CRoadPartModel::operator = ( const CRoadPartModel& other )
{
	CModel* model = reinterpret_cast< CModel* >( this );
	*model = CModel::operator = ( other );

	m_inverted      = other.m_inverted;
	m_nextPosition  = other.m_nextPosition;
	m_nextDirection = other.m_nextDirection;

	return (*this);
}

//-----------------------------------------------------------------------------

bool CRoadPartModel::SameAs( const CRoadPartModel& other ) const
{
	if ( this == &other ) { return (true); }
	if ( m_inverted != other.m_inverted ) { return (false); }
	return (m_template->Equals( *other.m_template ));
}

//-----------------------------------------------------------------------------

const CGPoint2& CRoadPartModel::GetNextPosition() const
{
	return (m_nextPosition);
}

//-----------------------------------------------------------------------------

Float CRoadPartModel::GetNextDirection() const
{
	return (m_nextDirection);
}

//-----------------------------------------------------------------------------

bool CRoadPartModel::IsInverted() const
{
	return (m_inverted);
}

//-----------------------------------------------------------------------------

void CRoadPartModel::Redefine( const CGPoint2& newCreationPosition, Float newCreationDirection )
{
	m_creationPosition  = newCreationPosition;
	m_creationDirection = newCreationDirection;

	Initialize();
	CalcNextPosition();
	CalcNextDirection();
}

//-----------------------------------------------------------------------------

Float CRoadPartModel::Length() const
{
	CRoadPartTemplate* roadPartTemplate = reinterpret_cast< CRoadPartTemplate* >( m_template );
	return (roadPartTemplate->Length());
}

//-----------------------------------------------------------------------------

void CRoadPartModel::CalcPosition()
{
	if ( !m_inverted )
	{
		m_position = m_creationPosition;
	}
	else
	{
		Float direction = m_creationDirection - CGConsts::HALF_PI;
		CRoadPartTemplate* roadPartTemplate = reinterpret_cast< CRoadPartTemplate* >( m_template );
		CGVector2 chord = roadPartTemplate->GetChord();
		CGVector2 invChord = roadPartTemplate->GetInvertedChord().Rotated( direction );
		m_position = invChord.Tail( m_creationPosition );
	}
}

//-----------------------------------------------------------------------------

void CRoadPartModel::CalcDirection()
{
	m_direction = m_creationDirection - CGConsts::HALF_PI;

	if ( m_inverted )
	{
		CRoadPartTemplate* roadPartTemplate = reinterpret_cast< CRoadPartTemplate* >( m_template );
		CGVector2 chord = roadPartTemplate->GetChord();
		Float angle = TWO * ( CGConsts::HALF_PI - chord.Direction() );
		m_direction += (CGConsts::PI + angle);
	}
}

//-----------------------------------------------------------------------------

void CRoadPartModel::CalcVisitorDirection()
{
	m_visitorDirection = CGConsts::HALF_PI - m_creationDirection;
/*
	if ( m_inverted )
	{
		CRoadPartTemplate* pRoadPartTemplate = reinterpret_cast< CRoadPartTemplate* >( m_pTemplate );
		CGVector2 chord = pRoadPartTemplate->GetChord();
		Float angle = TWO * ( CGConsts::HALF_PI - chord.Direction() );
		m_visitorDirection += (CGConsts::PI + angle);
	}
*/
}

//-----------------------------------------------------------------------------

void CRoadPartModel::CalcNextPosition()
{
	// assumes that m_position and m_direction have been already
	// calculated

	if ( !m_inverted )
	{
		CRoadPartTemplate* roadPartTemplate = reinterpret_cast< CRoadPartTemplate* >( m_template );
		CGVector2 chord = roadPartTemplate->GetChord().Rotated( m_direction );
		m_nextPosition = chord.Head( m_creationPosition );
	}
	else
	{
		m_nextPosition = m_position;
	}
}

//-----------------------------------------------------------------------------

void CRoadPartModel::CalcNextDirection()
{
	Float sign = ONE;
	if ( m_inverted ) { sign = -ONE; }

	CRoadPartTemplate* roadPartTemplate = reinterpret_cast< CRoadPartTemplate* >( m_template );
	m_nextDirection = m_creationDirection + sign * roadPartTemplate->GetDeltaDirection();
}

//-----------------------------------------------------------------------------
