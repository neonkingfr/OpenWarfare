#pragma once

// -------------------------------------------------------------------------- //

#include <vector>
#include <fstream>
#include <string>

// -------------------------------------------------------------------------- //

using std::vector;
using std::ofstream;
using std::endl;
using std::string;

// -------------------------------------------------------------------------- //

namespace LRP_Help
{
	// ---------------------------------------------------------------------- //

	static const double PI      = 4.0 * atan(1.0);
	static const double HALF_PI = 0.5 * PI;
	static const double TWO_PI  = 2.0 * PI;
	static const double EPSILON = 1e-08;

	static const double TERMLENGTHMULTIPLIER = 1.0;

	// ---------------------------------------------------------------------- //

	typedef enum
	{
		RS_Unknown,
		RS_Straight,
		RS_Corner,
		RS_Terminator
	} RoadSegTypes;

	// ---------------------------------------------------------------------- //

	class Dxf2006Writer
	{
		ofstream m_File;

	public:
		Dxf2006Writer();
		Dxf2006Writer(const string& filename);
		~Dxf2006Writer();

		void Filename(const string& filename);

		void WriteCircle(const Shapes::DVertex& center, double radius, 
						 const string& layer);
		void WritePolylineAsLines(const vector<Shapes::DVertex>& polyline, 
								  const string& layer, bool close);
		void WritePolyline(const vector<Shapes::DVertex>& polyline, 
						   const string& layer, bool close, 
						   double startingWidth = 0, 
						   double endingWidth = 0);

	private:

		void WriteEntitiesHead();
		void WriteEntitiesTail();
		void WriteFileTail();
	};

	// ---------------------------------------------------------------------- //

	class RoadSegmentBase
	{
	protected:
		RoadSegTypes    m_Type;
		Shapes::DVertex m_Position;
		double          m_Orientation;

	public:
		RoadSegmentBase();
		RoadSegmentBase(const RoadSegTypes& type);

		virtual ~RoadSegmentBase();

		RoadSegTypes Type() const;
		virtual Shapes::DVertex Position() const = 0;
		double Orientation() const;

		void Type(const RoadSegTypes& type);
		virtual void Position(const Shapes::DVertex& position) = 0;
		void Orientation(double orientation);

		virtual bool Equals(const RoadSegmentBase& other) const = 0;
		virtual bool IsMultiple(const RoadSegmentBase& other, unsigned int count) const = 0;

		virtual Shapes::DVertex NextPosition() const = 0;
		virtual double NextOrientation() const = 0;
		virtual vector<Shapes::DVertex> ShapeVertices() const = 0;

		virtual vector<Shapes::DVertex> Contour(double width) const = 0;
		virtual string ModelName(const string& suffix) const = 0;
		virtual Shapes::DVertex ModelPosition(double width) const = 0;
		virtual double ModelOrientation() const = 0;
	};

	// ---------------------------------------------------------------------- //

	class RoadSegmentStraight : public RoadSegmentBase
	{
		double m_Length;

	public:
		RoadSegmentStraight();
		RoadSegmentStraight(double length);

		RoadSegmentStraight(const RoadSegmentStraight& other);

		virtual ~RoadSegmentStraight();

		bool operator == (const RoadSegmentStraight& other) const;

		virtual Shapes::DVertex Position() const;
		virtual void Position(const Shapes::DVertex& position);
		double Length() const;
		void Length(double length);

		virtual bool Equals(const RoadSegmentBase& other) const;
		virtual bool IsMultiple(const RoadSegmentBase& other, unsigned int count) const;

		virtual Shapes::DVertex NextPosition() const;
		virtual double NextOrientation() const;
		virtual vector<Shapes::DVertex> ShapeVertices() const;
		virtual vector<Shapes::DVertex> Contour(double width) const;
		virtual string ModelName(const string& suffix) const;
		virtual Shapes::DVertex ModelPosition(double width) const;
		virtual double ModelOrientation() const;
	};

	// ---------------------------------------------------------------------- //

	class RoadSegmentCorner : public RoadSegmentBase
	{
		double m_Angle;
		double m_Radius;
		bool   m_Inverted;

	public:
		RoadSegmentCorner();
		RoadSegmentCorner(double angle, double radius);

		RoadSegmentCorner(const RoadSegmentCorner& other);

		virtual ~RoadSegmentCorner();

		virtual bool operator == (const RoadSegmentCorner& other) const;

		virtual bool Equals(const RoadSegmentBase& other) const;
		virtual bool IsMultiple(const RoadSegmentBase& other, unsigned int count) const;

		virtual Shapes::DVertex Position() const;
		virtual void Position(const Shapes::DVertex& position);
		double Angle() const;
		double Radius() const;
		bool Inverted() const;

		void Angle(double angle);
		void Radius(double radius);
		void Inverted(bool inverted);

		virtual Shapes::DVertex NextPosition() const;
		virtual double NextOrientation() const;
		virtual vector<Shapes::DVertex> ShapeVertices() const;
		virtual vector<Shapes::DVertex> Contour(double width) const;
		virtual string ModelName(const string& suffix) const;
		virtual Shapes::DVertex ModelPosition(double width) const;
		virtual double ModelOrientation() const;
	};

	// ---------------------------------------------------------------------- //

	class RoadSegmentTerminator : public RoadSegmentBase
	{
		double m_Length;
		bool   m_Inverted;

	public:
		RoadSegmentTerminator();
		RoadSegmentTerminator(double length);

		RoadSegmentTerminator(const RoadSegmentTerminator& other);

		virtual ~RoadSegmentTerminator();

		bool operator == (const RoadSegmentTerminator& other) const;

		virtual Shapes::DVertex Position() const;
		virtual void Position(const Shapes::DVertex& position);
		double Length() const;
		void Length(double length);
		bool Inverted() const;
		void Inverted(bool inverted);

		virtual bool Equals(const RoadSegmentBase& other) const;
		virtual bool IsMultiple(const RoadSegmentBase& other, unsigned int count) const;

		virtual Shapes::DVertex NextPosition() const;
		virtual double NextOrientation() const;
		virtual vector<Shapes::DVertex> ShapeVertices() const;
		virtual vector<Shapes::DVertex> Contour(double width) const;
		virtual string ModelName(const string& suffix) const;
		virtual Shapes::DVertex ModelPosition(double width) const;
		virtual double ModelOrientation() const;
	};

	// ---------------------------------------------------------------------- //

	class RoadSegmentsLibrary
	{
		vector<RoadSegmentBase*> m_Segments;
		bool m_AcceptUpdate;

		double m_LengthStep;
		double m_AngleStep;
		double m_RadiusStep;

		bool m_TerminatorAlreadyDefined;

	public:
		RoadSegmentsLibrary();
		~RoadSegmentsLibrary();

		const RoadSegmentBase* operator [] (unsigned int index) const;
		RoadSegmentBase* operator [] (unsigned int index);

		const RoadSegmentBase* GetSegment(unsigned int index) const;
		RoadSegmentBase* GetSegment(unsigned int index);

		double LengthStep() const;
		double AngleStep() const;
		double RadiusStep() const;

		void LengthStep(double lengthStep);
		void AngleStep(double angleStep);
		void RadiusStep(double radiusStep);

		bool HasTerminator() const;
		double TerminatorLength() const;

		const RoadSegmentBase* Terminator() const;
		RoadSegmentBase* Terminator();

		void Add(const RoadSegmentBase& segment);
		void Clear();
		size_t SegmentsCount() const;

		// returns true if successfull
		bool LoadFromCfgFile(const char* filename);

	private:
		bool AlreadyInList(const RoadSegmentBase& segment) const;
	};

	// ---------------------------------------------------------------------- //

	typedef enum
	{
		etStarting = 0,
		etEnding   = 1,
		etTransit  = 2
	} RoadEndTypes;

	// ---------------------------------------------------------------------- //

	class Road
	{
		// non const because we could want to add
		// new segments to the library while creating
		// this road
		RoadSegmentsLibrary*      m_Library; 
		vector< Shapes::DVertex > m_Shape;
		double                    m_Width;
		string                    m_Type;

		Shapes::DVertex m_StartPosition;
		double          m_StartOrientation;

		vector< RoadSegmentBase* > m_Segments;
		vector< Shapes::DVertex >  m_SegmentShape;

		// the coordinate along the shape [0 <= c <= shape length]
		double m_CurrShapeCoordinate;

		// where to insert next segment
		Shapes::DVertex m_CurrInsPosition;
		double          m_CurrInsOrientation;

		bool m_InUse;
		bool m_Built;

	public:
		Road();
		Road( RoadSegmentsLibrary& library, double width, const string& type );

		~Road();

		const RoadSegmentsLibrary* Library() const;
		const vector< RoadSegmentBase* >& Segments() const;
		double Width() const;
		const string& Type() const;

		const Shapes::DVertex& StartPosition() const;
		void StartPosition( const Shapes::DVertex& startPosition );

		double StartOrientation() const;
		void StartOrientation( double startOrientation );

		bool InUse() const;
		void InUse( bool inUse );

		bool Built() const;
		void Built( bool built );

		bool Equal( const Road& other ) const;

		void Build();
		void V3Correction();

		// -------------------------------------------------------------------------- //
		// Shape
		// -------------------------------------------------------------------------- //
		size_t ShapeVerticesCount() const;
		Shapes::DVertex& ShapeStartVertex();
		const Shapes::DVertex& ShapeStartVertex() const;
		Shapes::DVertex& ShapeEndVertex();
		const Shapes::DVertex& ShapeEndVertex() const;
		void AddShapeVertex( const Shapes::DVertex& vertex );
		Shapes::DVertex& ShapeVertex( size_t index );
		const Shapes::DVertex& ShapeVertex( size_t index ) const;
		double ShapeLength() const;
		void ReverseVertices();
		void MergeShapeWith( const Road& other, RoadEndTypes thisEndType, RoadEndTypes otherEndType );
		Shapes::DVertex ClosestPointOnShapeTo( const Shapes::DVertex& point ) const;
		void ReplaceShape( const vector< Shapes::DVertex >& newshape );

		// -------------------------------------------------------------------------- //
		// SegmentShape
		// -------------------------------------------------------------------------- //
		size_t SegmentShapeVerticesCount() const;
		Shapes::DVertex& SegmentShapeVertex( size_t index );
		const Shapes::DVertex& SegmentShapeVertex( size_t index ) const;
		Shapes::DVertex ClosestPointOnSegmentShapeTo( const Shapes::DVertex& point ) const;

	private:
		void ResetSegments();
		RoadSegmentBase* ClosestSegmentOnlyDist();
		RoadSegmentBase* ClosestSegmentDistAndDir();
		double ShapeCoordinate( const Shapes::DVertex& vertex ) const;
		void RecalculateCurrShapeCoordinate();
		void AddSegment( RoadSegmentBase* pSegment );
		double OrientationShapeEdgeOwnerOf( const Shapes::DVertex& vertex );
		unsigned int CollectSimilar();
		unsigned int ReplaceWithBiggerSegment( size_t index, const RoadSegmentBase* pSegment, int count );
		bool PrumetNaUsecku( const Shapes::DVertex& pt, const Shapes::DVertex& a, const Shapes::DVertex& b, Shapes::DVertex& res, double& d2 ) const;
		void RecalculateSegmentShape();
	};

	// ---------------------------------------------------------------------- //

	class RoadEx
	{
		Road*        m_Road;
		RoadEndTypes m_RoadEndType;

	public:
		RoadEx(Road* road, RoadEndTypes roadEndType);

		Road*        GetRoad();
		const Road*  GetRoad() const;
		RoadEndTypes GetRoadEndType() const;

		void SetRoad(Road* road);
		void SetRoadEndType(RoadEndTypes roadEndType);

		void SwitchEndType();
	};

	// ---------------------------------------------------------------------- //

	class CandidatesItem
	{
		double         m_Width;
		string         m_Type;
		vector<size_t> m_Indices;

	public:
		CandidatesItem(double width, string type);

		bool operator == (const CandidatesItem& other) const;

		double Width() const;

		void AddIndex(size_t index);

		size_t Index(size_t index) const;

		size_t IndicesCount() const;
	};

	// ---------------------------------------------------------------------- //

	bool CandidatesItem_Smaller(const CandidatesItem& c1, const CandidatesItem& c2);
	bool CandidatesItem_Greater(const CandidatesItem& c1, const CandidatesItem& c2);

	// ---------------------------------------------------------------------- //

	class Node
	{
		Shapes::DVertex        m_Position;
		vector<RoadEx>         m_RoadsList;
		vector<CandidatesItem> m_CandidatesToMerge;
		bool                   m_Transit;

	public:
		Node(const Shapes::DVertex& position);
		~Node();

		void AddRoad(const RoadEx& road);

		Shapes::DVertex& Position();
		const Shapes::DVertex& Position() const;

		RoadEx&       GetRoad(unsigned int index);
		const RoadEx& GetRoad(unsigned int index) const;

		size_t RoadsCount() const;

		bool IsTransit() const;

		bool EqualPos(const Shapes::DVertex& point);
		size_t InitCandidatesToMerge();

		// merges the two roads in a 2-roads node
		void MergeRoads2_2(vector<Node>& nodesList);
		// merges two roads in a 3-roads node
		void MergeRoads2_3(vector<Node>& nodesList);
		// merges two roads in a 3-roads node with 3 equal roads
		void MergeRoads2_3E(vector<Node>& nodesList);
		// merges two roads in a 4-roads node
		void MergeRoads2_4(vector<Node>& nodesList);
		// merges two roads in a 4-roads node with 3 equal roads
		void MergeRoads2_4E(vector<Node>& nodesList);

		void RebuildNonTransitRoads();

	private:
		size_t GetBestCandidateItemIndex() const;
	};

	// ---------------------------------------------------------------------- //

	class PolylineInsersector
	{
		vector<Shapes::DVertex> m_Poly1;
		vector<Shapes::DVertex> m_Poly2;

		vector<Shapes::DVertex> m_IntPoints;

	public:
		PolylineInsersector(const vector<Shapes::DVertex>& poly1, const vector<Shapes::DVertex>& poly2);

		const vector<Shapes::DVertex>& IntersectionPoints() const;

	private:
		void CalculateIntersectionPoints();
	};
}

// -------------------------------------------------------------------------- //
