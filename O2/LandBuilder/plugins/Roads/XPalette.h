//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

struct XPaletteItemExit
{
  ///Road type on this exit
  RString m_roadType;

  ///Road width during generation
  double m_width;
  
  ///direction of exit
  double m_azimut;
    
  XPaletteItemExit() 
  {
  }

  XPaletteItemExit( RString roadType, double size, double azimut )
  : m_roadType( roadType )
  , m_width( size )
  , m_azimut( azimut ) 
  {
  }

  ClassIsMovable( XPaletteItemExit )
};

//-----------------------------------------------------------------------------

struct XPaletteItem;

//-----------------------------------------------------------------------------

struct XPalBestResult
{
  ///reference to an item that is best to specified
  const XPaletteItem* m_xroad;

  ///rotation in exits. For example value 2 means that trg exit 1 is mapped to src exit 3
  int m_exitRot;

  ///azimut of xroad. Default azimut is 0, but it can be rotated to best match
  double m_azimutRot;

  ///suggested crossroad scale to reach with of all exits
  double m_scale;

  ///true - when model was created. 
  /**If creation is forbidden, this flag is used to inform caller that it will be
  better to allow creation */
  bool m_created;

  double m_score;    

  XPalBestResult()
  : m_xroad( NULL )
  , m_exitRot( 0 )
  , m_azimutRot( 0.0 )
  , m_created( false )
  , m_score( 0.0 )
  , m_scale( 1.0 ) 
  {
  }

  XPalBestResult( const XPaletteItem* xroad, int exitRot,
                  double azimutRot, double scale, double score, bool create )
  : m_xroad( xroad )
  , m_exitRot( exitRot )
  , m_azimutRot( azimutRot )
  , m_scale( scale )
  , m_score( score )
  , m_created( create ) 
  {
  }
};

//-----------------------------------------------------------------------------

struct XPaletteItem
{
  ///list of exits
  AutoArray< XPaletteItemExit > m_exits;

  ///associated model
  RString m_model;

  ///radius from center to exit endpoint used during generation
  /**When it is used to search best item, specifi there maximum radius that 
  crossroad item may scale */
  double m_radius;

  RStringI m_forcedTexture;

  XPaletteItem() 
  {
  }

  XPaletteItem( const RString& model )
  : m_model( model ) 
  {
  }
    
  XPalBestResult FindBest( const XPaletteItem& item, double maxSize, double maxAzimutDeviation, 
                           double maxSizeDeviation );
    
  struct FindRotRes
  {
    double m_score;
    double m_rotation;
    double m_scale;

    FindRotRes( double score, double rotation, double scale )
    : m_score( score )
    , m_rotation( rotation )
    , m_scale( scale ) 
    {
    }
  };

  FindRotRes FindBestRotated( const XPaletteItem& item, double maxRadius, double maxAzimutDeviation, 
                              double maxSizeDeviation, int rotation );

  RString BuildModelName( const char* output, const char* projectBase, int unique );

  ClassIsGeneric( XPaletteItem );
};

//-----------------------------------------------------------------------------

class XPaletteException : public std::runtime_error
{
  int m_code;

  static RString BuildMsgText( int code, const char* text );

public:
  XPaletteException( int code, const char* text )
  : m_code( code )
  , std::runtime_error( text ) 
  {
  }

  int GetCode() const 
  {
    return (m_code);
  }
};

//-----------------------------------------------------------------------------

class XPalette : public AutoArray< XPaletteItem >
{
  double  m_dirGran;
  double  m_sizeGran;
  RString m_genOutputPath;
  RString m_genBasePath;
  double  m_maxAzimutDeviation;
  double  m_maxSizeDeviation;
  int     m_nextUnique;

public:    
  static RString s_generatorExe;

  void LoadPalette( const char* filename ) throw ( XPaletteException );
  void SavePalette( const char* filename ) throw ( XPaletteException );

  void SetGeneratorExe( const char* genExe ) 
  {
    s_generatorExe = genExe;
  }
  
  XPalBestResult FindBest( const XPaletteItem& item, double maxRadius, bool allowCreate );

  XPalBestResult CreateBestXRoad( const XPaletteItem& item, double maxRadius );

  int GenerateP3D( const XPaletteItem& xitem ) const;
  int GenerateP3DWithLog( const XPaletteItem& xitem ) const;

protected:
  RString PrepareGenData( const XPaletteItem& item ) const;
  void PrepareGenDataExportXRoadEnd( IParamArrayValue* endinfo, const XPaletteItemExit& exitNfo, double radius ) const;
};

//-----------------------------------------------------------------------------
