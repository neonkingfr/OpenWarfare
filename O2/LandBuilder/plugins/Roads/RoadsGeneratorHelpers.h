//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <vector>
#include <fstream>
#include <iostream>
#include <string>

#include "Models.h"
#include "BSpline.h"

//-----------------------------------------------------------------------------

using std::vector;
using std::ofstream;
using std::endl;
using std::string;

//-----------------------------------------------------------------------------

namespace RG_Help
{

//-----------------------------------------------------------------------------

  class Dxf2006Writer
  {
    ofstream m_file;

  public:
    Dxf2006Writer( const string& filename );
    ~Dxf2006Writer();

    void WriteCircle( const Shapes::DVertex& center, double radius, 
             const string& layer );
    void WritePolylineAsLines( const vector< Shapes::DVertex >& polyline, 
                               const string& layer, bool close );
    void WritePolyline( const vector< Shapes::DVertex >& polyline, 
                        const string& layer, bool close, 
                        double startingWidth = 0.0, 
                        double endingWidth = 0.0 );

  private:
    void WriteEntitiesHead();
    void WriteEntitiesTail();
    void WriteFileTail();
  };

//-----------------------------------------------------------------------------

  class Road
  {
    class DVertexEx : public Shapes::DVertex
    {
    public:
      DVertexEx( double x = 0.0, double y = 0.0, double z = DBL_MIN, 
                 double m = DBL_MIN ) 
      : DVertex( x, y, z, m )
      {
      }

      DVertexEx( const DVector& vx, double m = DBL_MIN ) 
      : DVertex( vx, m ) 
      {
      }

      DVertexEx( const DVertex& vx )
      : DVertex( vx ) 
      {
      }

      double& operator [] ( int i )
      {        
        switch ( i )
        {
        case 0: return (x);
        case 1: return (y);
        case 2: return (z);
        case 3: return (m);
        }
        throw std::runtime_error( "Array is out of bounds" );
      }

      ClassIsSimpleZeroed( DVertexEx );
    };

    double  m_width;
    size_t  m_level;
    RString m_mainTextureFilename;
    size_t  m_maskColor;
    bool    m_startTerminator;
    bool    m_endTerminator;
    RString m_endTextureFilename;

    vector< Shapes::DVertex > m_shape;
    vector< Shapes::DVertex > m_spline;

    double m_splineStep;

    vector< Contour >         m_modelsContours;
    vector< Shapes::DVertex > m_modelsPositions;

  public:
    Road( double width, int level, RString mainTextureFilename, 
          size_t maskColor );

    double  GetWidth() const;
    size_t  GetLevel() const;
    RString GetMainTextureFilename() const;
    size_t  GetMaskColor() const;
    bool    HasStartTerminator() const;
    bool    HasEndTerminator() const;
    RString GetEndTextureFilename() const;

    void SetWidth( double width );
    void SetLevel( size_t level );
    void SetMainTextureFilename( const RString& mainTextureFilename );
    void SetMaskColor( size_t maskColor );
    void SetStartTerminator( bool startTerminator );
    void SetEndTerminator( bool endTerminator );

    vector< Shapes::DVertex >&       GetShape(); 
    const vector< Shapes::DVertex >& GetShape() const;

    void SetShape( const vector< Shapes::DVertex >& shape ); 

    void AddShapeVertex( const Shapes::DVertex& vertex );
    void RemoveShapeVertex( size_t index );

    unsigned int GetShapeVerticesCount() const;

    const Shapes::DVertex& GetShapeVertex( size_t index ) const;
    Shapes::DVertex&       GetShapeVertex( size_t index );

    void SetShapeVertex( size_t index, const Shapes::DVertex& vertex );

    const Shapes::DVertex& GetShapeStartingVertex() const;
    const Shapes::DVertex& GetShapeEndingVertex() const;

    Shapes::DVector GetShapeStartingVector() const;
    Shapes::DVector GetShapeEndingVector() const;

    bool GenerateSpline();

    vector< Shapes::DVertex >&       GetSpline(); 
    const vector< Shapes::DVertex >& GetSpline() const;

    void AddSplineVertex( const Shapes::DVertex& vertex );
    void InsertSplineVertex( size_t index, const Shapes::DVertex& vertex );

    void GenerateModelsContours( double maxDimension );

    size_t GetModelsContoursCount();

    vector< Contour >& GetModelsContours();
    const vector< Contour >& GetModelsContours() const;

    Contour& GetModelContour( size_t index );
    const Contour& GetModelContour( size_t index ) const;

    void GenerateP3DModels( IMainCommands* cmdIfc, size_t roadCounter,
                            RString modelsPath, RString texturesPath );

    const vector<Shapes::DVertex>& GetModelsPositions() const;

    void AddModelPosition( const Shapes::DVertex& position );
    void InsertModelPosition( size_t index, const Shapes::DVertex& position );

    void PaintOnMask( HDC dc, HBITMAP bmp, DoubleRect mapRect, size_t x, 
                      size_t y, size_t maskSize);

  private:
    void   GenerateEndTextureFilenameFromMainTextureFilename();
    void   FirstSplineAttempt( double step, BSpline< 4, double, Array< DVertexEx > >& bspline );
    bool   RefineSpline( BSpline< 4, double, Array< DVertexEx > >& bspline );
    void   ClearSpline();
    double SplineLength();
    double SplineAverageSegmentLength();
    void   GenerateSegmentModelsContours();
    void   MergeSegmentModelsContours();
    void   EnhancedMergeModelsContours( double maxDimension );
  };

//-----------------------------------------------------------------------------

  class XRoad
  {
    Road*        m_road;
    RoadEndTypes m_roadEndType;

  public:
    XRoad( Road* road, RoadEndTypes roadEndType );

    Road*        GetRoad();
    const Road*  GetRoad() const;
    RoadEndTypes GetRoadEndType() const;

    Shapes::DVector EndVectorInsideCrossroad() const;
    double          AzimuthEndVectorInsideCrossroad() const;
  };

//-----------------------------------------------------------------------------

  class RoadsTransitionBuilder
  {
    Shapes::DVertex m_crossroadCenter;
    double          m_crossroadRadius;
    XRoad*          m_road1;
    XRoad*          m_road2;
    size_t          m_resolution;

    Shapes::DVector m_vecRoad1;
    Shapes::DVector m_vecRoad2;
    Shapes::DVector m_perpVecRoad1;
    Shapes::DVector m_perpVecRoad2;
    double          m_angle;
    bool            m_last;
    Shapes::DVertex m_newEndVertexRoad1;
    Shapes::DVertex m_newEndVertexRoad2;
    Shapes::DVertex m_road1EndOnLBorder;
    Shapes::DVertex m_road2EndOnRBorder;
    Shapes::DVertex m_center;
    Shapes::DVector m_vecRadius1;
    Shapes::DVector m_vecRadius2;

    vector< Shapes::DVertex > m_transitionPoints;

  public:
    RoadsTransitionBuilder( const Shapes::DVertex& crossroadCenter, 
                            double crossroadRadius,
                            XRoad* road1, XRoad* road2, 
                            size_t resolution );

    vector< Shapes::DVertex >&       GetTransitionPoints();
    const vector< Shapes::DVertex >& GetTransitionPoints() const;

  private:
    void CalculateGeometry();
    void CalculateTransitionPoints();
  };

//-----------------------------------------------------------------------------

  class Crossroad
  {
    class AzimuthLess
    {
    public:
      bool operator () ( const XRoad& r1, const XRoad& r2 )
      {
        return (r1.AzimuthEndVectorInsideCrossroad() < r2.AzimuthEndVectorInsideCrossroad());
      }
    };

    Shapes::DVertex m_position;
    size_t          m_resolution;
    double          m_radius;
    size_t          m_levelToBuild;

    vector< XRoad >                  m_xRoadsList;
    vector< RoadsTransitionBuilder > m_builders;

    Contour         m_contour;
    Shapes::DVertex m_modelPosition;

  public:
    Crossroad( const Shapes::DVertex& position, size_t resolution );

    Shapes::DVertex GetPosition() const;
    double          GetRadius() const;
    const Contour&  GetContour() const;
    size_t    GetLevel() const;

    void SetPosition( const Shapes::DVertex& position );

  protected:
    bool operator == ( const Crossroad& other );

  public:
    // Adds a road to this crossroad.
    // Returns false if there are already 4 roads
    // XRoad is not const because this function modifies the road's
    // ending state (they become non-terminator)
    bool AddXRoad( XRoad& road );

    size_t XRoadsCount() const;

    XRoad&       GetXRoad( size_t index );
    const XRoad& GetXRoad( size_t index ) const;

    void MatchXRoadsWithContour( double overlap );
    void ExtendXRoadSplinesToModelPosition();

    void GenerateP3DModels( IMainCommands* cmdIfc, RString modelsPath,
                            RString texturesPath );
    Shapes::DVertex GetModelPosition() const;

    void PaintOnMask( HDC dc, HBITMAP bmp, DoubleRect mapRect, size_t x, 
                      size_t y, size_t maskSize );

    size_t LevelsCount() const;

    void CalculateLevelToBuild();
    void CalculateRadius();
    void CreateBuilders();
    void GenerateCrossroadContour();
    void AdjustRoadsEnds();

  private:
    size_t MaxLevel() const;
    double MaxRoadsWidth() const;
    void   ReshapeRoadsInsideCrossroad();
    void   OrderRoadsByAzimut();
    double MinAngle();
    double MinMergingAngle();
    size_t MergedRoadsCount() const;
  };

//-----------------------------------------------------------------------------

  template < class T >
  class TList
  {
    vector< T > m_list;

  public:
    size_t Size() const;

    void Add( const T& t );
    void Remove( size_t index );

    T& operator [] ( size_t index );
    const T& operator [] ( size_t index ) const;

    int Find(const T& t) const;
  };

//-----------------------------------------------------------------------------

  #include "RoadsGeneratorHelpers.inl"

//-----------------------------------------------------------------------------

} // namespace RG_Help