#pragma once

// -------------------------------------------------------------------------- //

#include "LibRoadsPlacerHelpers.h"
#include "LBRoads.h"

// -------------------------------------------------------------------------- //

#include "vector"

// -------------------------------------------------------------------------- //

using std::vector;

// -------------------------------------------------------------------------- //

using namespace LRP_Help;

// -------------------------------------------------------------------------- //

class LibRoadsPlacer : public LandBuilder2::IBuildModule
{
  bool m_firstRun;
  bool m_canProceed;
  bool m_allRoadsLoaded;

  RString             m_libraryFile;
  RoadSegmentsLibrary m_library;
  vector< Road >      m_roadsList;
  vector< Node >      m_nodesList;

  vector< LandBuilder2::LBRoadDefinition > m_roadDefinitions;
  vector< LandBuilder2::LBRoadPath >       m_roadPaths;

  Dxf2006Writer m_dxfWriter;

public:
  LibRoadsPlacer();
  virtual ~LibRoadsPlacer();

  virtual void Run( IMainCommands* pCmdIfc );
  virtual void OnEndPass( IMainCommands* pCmdIfc );

private:
  void CreateNodeList();
  void PreprocessNodes();
  void PostprocessNodes();

  // exports to dxf file
  void ExportDxf( const Road& road );
  void ExportModels( IMainCommands* pCmdIfc );
};

// -------------------------------------------------------------------------- //
