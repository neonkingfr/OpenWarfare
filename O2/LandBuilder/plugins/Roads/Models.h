//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <vector>

#include <projects/ObjektivLib/LODObject.h>
#include <picture.hpp>

//-----------------------------------------------------------------------------

using namespace ObjektivLib; 
using std::vector;

//-----------------------------------------------------------------------------

namespace RG_Help
{

//-----------------------------------------------------------------------------

  static const double PI      = 4.0 * atan( 1.0 );
  static const double INV_PI  = 1.0 / PI;
  static const double TWO_PI  = 2.0 * PI;
  static const double HALF_PI = PI / 2.0;
  static const double EPSILON = 1e-08;

//-----------------------------------------------------------------------------

  typedef enum
  {
    etStarting = 0,
    etEnding   = 1
  } RoadEndTypes;

//-----------------------------------------------------------------------------

  typedef enum
  {
    Terminator_NONE,
    Terminator_START,
    Terminator_END
  } TerminatorTypes;

//-----------------------------------------------------------------------------

  struct BITMAPINFOEX : public BITMAPINFO
  {
    RGBQUAD buff[256];
  };

//-----------------------------------------------------------------------------

  class Color
  {
    size_t m_Red;
    size_t m_Green;
    size_t m_Blue;

  public:
    Color( size_t red, size_t green, size_t blue );
    Color( size_t color);

    size_t GetRed() const;
    size_t GetGreen() const;
    size_t GetBlue() const;
  };

//-----------------------------------------------------------------------------

  class Line
  {
    const Shapes::DVertex& m_V1;
    const Shapes::DVertex& m_V2;

    double m_A;
    double m_B;
    double m_C;

  public:
    Line( const Shapes::DVertex& v1, const Shapes::DVertex& v2 );

    const Shapes::DVertex& GetV1() const;
    const Shapes::DVertex& GetV2() const;

    // calculates the intersection point between this line and the given one
    // the intersection point, if any, will be returned in the intPoint variable
    // returns false if the two lines are parallel
    bool Intersection( const Line& other, Shapes::DVertex& intPoint ) const;

    // returns true if the given point lies on this line between the points V1 and V2
    bool ContainsBetweenEnds( const Shapes::DVertex& point ) const;

  private:
    void CalculateCoefficients();
  };

//-----------------------------------------------------------------------------

  class BoundingBox
  {
    double m_Width;
    double m_Height;
    double m_Orientation;

  public:
    BoundingBox();

    double GetWidth() const;
    double GetHeight() const;
    double GetOrientation() const;

    void Reset();

    void CalculateFromVertices( const vector< Shapes::DVertex >& vertices );
  };

//-----------------------------------------------------------------------------

  class Contour
  {
    vector< Shapes::DVertex > m_Vertices;
    BoundingBox               m_BoundingBox;

  public:
    Contour();
    Contour( const vector< Shapes::DVertex >& vertices );

    // returns true if this contour is equal to the given one.
    // two contour are equal if their vertices have the same
    // relative positions
    bool Equal( const Contour& other ) const;

    const vector< Shapes::DVertex >& GetVertices() const;
    vector< Shapes::DVertex >& GetVertices();

    void AddVertex( const Shapes::DVertex& vertex );
    void AddVertices( const vector< Shapes::DVertex >& vertices );

    Shapes::DVertex& operator [] ( size_t index );
    const Shapes::DVertex& operator [] ( size_t index ) const;

    Shapes::DVertex& GetVertex( size_t index );
    const Shapes::DVertex& GetVertex( size_t index ) const;

    void SetVertex( size_t index, const Shapes::DVertex& vertex );

    void InsertVertex( size_t index, const Shapes::DVertex& vertex );
    void InsertVertices( size_t index, const vector< Shapes::DVertex >& vertices );

    size_t GetVerticesCount() const;

    void SetVerticesOrderToCCW();

    // calculates the intersection point between this contour and the given 
    // line segment
    // the intersection point, if any, will be returned in the intPoint variable
    // returns false if there is no intersection point
    // if strictOnSegment is true the intersection point must be on the line segment
    // if strictOnSegment is false the intersection point can be on the line segment 
    // extension on the side of the first vertex of the line segment 
    bool IntersectionWithSegment( const Line& line, Shapes::DVertex& intPoint, 
                                  bool strictOnSegment = true) const;

    // returns the width of the minimum bounding box of this contour
    double GetMinBoundingBoxWidth();

    // returns the orientation of the minimum bounding box of this contour
    double GetMinBoundingBoxOrientation();

    // resets bounding box, to be called when modifying vertex using operator []
    void ResetBoundingBox();

    // draw this contour on the mask
    void PaintOnMask( HDC dc, HBITMAP bmp, DoubleRect mapRect, size_t x, size_t y, size_t maskSize );

  protected:
    double SignedArea() const;
    void   ReverseVerticesOrder();
  };

//-----------------------------------------------------------------------------

  class Texture
  {
    RString m_Pathname;
    RString m_Filename;
    size_t  m_Size;
    HBITMAP m_Mask;
    HDC     m_DC;

  public:
    Texture( RString pathname, RString filename, size_t size );
    ~Texture();

    RString GetPathname() const;
    RString GetFilename() const;
    size_t  GetSize() const;
    HBITMAP GetMask() const;
    HDC     GetDC() const;

    void SetMask( HBITMAP mask );
    void SetDC( HDC dc );

    void Reset();
    void GenerateFromModelFaces( const ObjectData& obj );
    bool Save() const;
  };

//-----------------------------------------------------------------------------

  class ModelP3D
  {
  protected:
    RString           m_Filename;

    mutable Contour   m_Contour;
    Shapes::DVertex   m_Position;
    double            m_Orientation;

    RString           m_ModelPath;
    RString           m_TexturesPath;

    vector< Texture > m_Textures;

  public:
    ModelP3D( RString filename, const Contour& contour, RString modelPath, 
              RString texturesPath, const vector<Texture>& textures );

    virtual ~ModelP3D();

    Shapes::DVertex GetPosition() const;
    double          GetOrientation() const;

    virtual void ExportToP3DFile() = 0;

  protected:
    virtual void GenerateLOD( ObjectData& obj, float resolution ) = 0;
    virtual void GenerateMemoryLOD( ObjectData& obj ) = 0;
    virtual void GenerateMapping( ObjectData& obj ) = 0;

    virtual void SetOrigin() = 0;
    void OrientateAsMinBoundingBox();
  };

//-----------------------------------------------------------------------------

  class CrossroadModelP3D : public ModelP3D
  {
    size_t                 m_TextureSize;
    size_t                 m_CrossroadResolution;
    vector< RoadEndTypes > m_RoadsEndTypes;
    size_t                 m_RoadsCount;

  public:
    CrossroadModelP3D( RString filename, const Contour& contour, 
                       RString modelPath, RString texturesPath,
                       const vector< Texture >& textures, size_t textureSize,
                       size_t crossroadResolution,
                       const vector< RoadEndTypes >& roadsEndTypes,
                       size_t roadsCount );

    virtual ~CrossroadModelP3D();

    virtual void ExportToP3DFile();

  protected:
    virtual void GenerateLOD( ObjectData& obj, float resolution );
    virtual void GenerateMemoryLOD( ObjectData& obj );
    virtual void GenerateMapping( ObjectData& obj );

    virtual void SetOrigin();

  private:
    void CalculatePositionWith2Roads();
    void CalculatePositionWith3Roads();
    void CalculatePositionWith4Roads();
    void GenerateTextureBitmap( ObjectData& obj );
  };

//-----------------------------------------------------------------------------

  class RoadModelP3D : public ModelP3D
  {
    TerminatorTypes m_Terminator;

  public:
    RoadModelP3D( RString filename, const Contour& contour, RString modelPath, 
                  RString texturesPath, const vector< Texture >& textures, 
                  TerminatorTypes terminator );

    virtual ~RoadModelP3D();

    virtual void ExportToP3DFile();

  protected:
    virtual void GenerateLOD( ObjectData& obj, float resolution );
    virtual void GenerateMemoryLOD( ObjectData& obj );
    virtual void GenerateMapping( ObjectData& obj );

    virtual void SetOrigin();
  };

//-----------------------------------------------------------------------------

} // namespace RG_Help