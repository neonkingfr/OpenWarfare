//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "RoadsGenerator.h"


//-----------------------------------------------------------------------------

static LandBuilder2::RegisterModule< RoadsGenerator > roadsGenerator( "RoadsGenerator" );

//-----------------------------------------------------------------------------

static const bool         EXPORT_DXF                  = true;
static const unsigned int CROSSROADS_RESOLUTION       = 16;
static const double       ROAD_CROSSROAD_OVERLAP_NULL = 0.0;
static const double       ROAD_CROSSROAD_OVERLAP      = 0.30;
static const double       ROAD_MAX_MODEL_DIMENSION    = 25.0;

//-----------------------------------------------------------------------------

RoadsGenerator::RoadsGenerator()
: m_firstRun( true )
, m_allRoadsLoaded( false )
, m_version( "1.1.0" )
{
}

//-----------------------------------------------------------------------------

RoadsGenerator::~RoadsGenerator()
{
}

//-----------------------------------------------------------------------------

void RoadsGenerator::Run( IMainCommands* cmdIfc )
{
  RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
  if ( taskName != m_currTaskName )
  {
    m_firstRun       = true;
    m_allRoadsLoaded = false;
    m_currTaskName   = taskName;
  }

  if ( m_allRoadsLoaded ) { return; }

  LandBuilder2::ParamParser params( cmdIfc );

  // here we get the global data
  if ( m_firstRun )
  {
    LOGF( Info ) << "                                                           ";
    LOGF( Info ) << "===========================================================";
    LOGF( Info ) << "RoadsGenerator " << m_version << " ";
    LOGF( Info ) << "===========================================================";

    try 
    {
      m_modelsPath = params.GetTextValue( "modelsPath" );
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      // if no path for models is specified in the cfg
      // creates a default one
      m_modelsPath = "C:\\TempRoads\\";
    }

    // TODO: add check for valid path

    if ( m_modelsPath[m_modelsPath.GetLength() - 1] != '\\' )
    {
      m_modelsPath = m_modelsPath + "\\";
    }

    m_texturesPath = m_modelsPath + "Data\\";

    Pathname::CreateFolder( m_texturesPath );

    RString logFileName = m_texturesPath + "log.txt";
    m_logFile.open( logFileName.Data(), std::ios::out | std::ios::trunc );
    if ( !m_logFile.fail() ) 
    {
      LOGF( Error ) << "Failed to open log file.";
    }

    try 
    {
      m_maskSize = params.GetIntValue( "maskSize" );
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      // if no mask size is specified we will not create
      // the mask
      m_maskSize = 0;
    }

    m_firstRun = false;
  }

  // here we get all polylines from the shapefile and add them to the
  // list of roads

  Shapes::IShape* curShape = const_cast< Shapes::IShape* >( cmdIfc->GetActiveShape() );

  string errorMsg = "RoadsGenerator module accepts only shapefiles containing non-multipart polylines.";

  // checks for multipart polylines
  if ( curShape->GetPartCount() > 1 )
  {
    LOGF( Error ) << errorMsg;
    m_logFile << errorMsg << endl;
    return;
  }

  // checks for known shapes
  const IShapeExtra* extra = curShape->GetInterface< IShapeExtra >();
  if ( !extra ) 
  {
    LOGF( Error ) << "Found unknown shape in shapefile. Cannot get type of that shape.";
    m_logFile << "Found unknown shape in shapefile. Cannot get type of that shape." << endl;
    return;
  }

  // checks for polylines only
  RString shapeType = extra->GetShapeTypeName();
  if ( shapeType != RString( SHAPE_NAME_POLYLINE ) ) 
  {
    LOGF( Error ) << errorMsg;
    m_logFile << errorMsg << endl;
    return;
  }

  // skips polylines with less than 2 vertices
  if ( curShape->GetVertexCount() > 1 )
  {
    double  width = 0.0;
    size_t  level = 0;
    RString texFile = "";
    size_t  maskColor = 0;

    // these are mandatory data
    try 
    {
      width   = (double)params.GetFloatValue( "width" );
      texFile = params.GetTextValue( "texFile" );
    }
    catch ( LandBuilder2::ParamParserException& e ) 
    {
      LOGF( Error ) << "Cannot parse parameters: " << e.Message() << " " << e.GetName() << "=" << e.GetValue();
      m_logFile << "Cannot parse parameters: " << e.Message() << " " << e.GetName() << "=" << e.GetValue() << endl;
      return;
    }

    if ( width == 0.0 )
    {
      LOGF( Error ) << "Found road with null width.";
      m_logFile << "Found road with null width." << endl;
      return;
    }

    if ( texFile.GetLength() == 0 )
    {
      LOGF( Error ) << "Found road with null texture filename.";
      m_logFile << "Found road with null texture filename." << endl;
      return;
    }

    // this check is not repeated in the Road object
    // when creating the end texture filename

    texFile.Lower();
    int pos = texFile.GetLength() - 3;
    
    if ( pos < 0 )
    {
      LOGF( Error ) << "Texture filename must end with \'_ca\'";
      m_logFile << "Texture filename must end with \'_ca\'" << endl;
      return;
    }

    RString endName = texFile.Substring( pos, texFile.GetLength() );
    if ( endName == "_ca" )
    {
      // this is because of problems with operator !=
    }
    else
    {
      LOGF( Error ) << "Texture filename must end with \'_ca\'";
      m_logFile << "Texture filename must end with \'_ca\'" << endl;
      return;
    }

    // these are mandatory data that can be recovered if missing
    try 
    {
      level = params.GetIntValue( "level" );
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      level = 0;
    }

    // these are non mandatory data
    try 
    {
      maskColor = (size_t)params.GetIntValue( "maskCol" );
    }
    catch ( LandBuilder2::ParamParserException& ) 
    {
      maskColor = 0;
    }

    // creates a new road object
    Road road( width, level, texFile, maskColor );
    for ( size_t i = 0, cnt = curShape->GetVertexCount(); i < cnt; ++i )
    {
      road.AddShapeVertex( curShape->GetVertex( i ) );
    }

    // adds the road to the list
    m_roadsList.Add( road );
  }
}

//-----------------------------------------------------------------------------

void RoadsGenerator::OnEndPass( IMainCommands* cmdIfc ) 
{
  if ( m_allRoadsLoaded ) { return; }
  m_allRoadsLoaded = true;

  LOGF( Progress ) << "Started crossroads\' detection...";
  int failIndex;
  if ( !CreateCrossroadList( CROSSROADS_RESOLUTION, &failIndex ) )
  {
    Shapes::DVertex pos = m_crossroadsList[failIndex].GetPosition();
    LOGF( Error ) << "ERROR - Found crossroads\' with more than 4 roads of the same level. (position: " << pos.x << ", " << pos.y << ")";
    m_logFile << "ERROR - Found crossroads\' with more than 4 roads of the same level. (position: " << pos.x << ", " << pos.y << ")" << endl;
    return;
  }
  LOGF( Progress ) << "Detected " << m_crossroadsList.Size() << " crossroads.";

  LOGF( Progress ) << "Started crossroads\' contours generation...";
  GenerateCrossroadsContour();
  LOGF( Progress ) << "Generated " << m_crossroadsList.Size() << " crossroads\' contours.";

  LOGF( Progress ) << "Started roads\' centerline update...";
  AdjustRoadsEnds();
  LOGF( Progress ) << m_roadsList.Size() << " roads\' centerline adjusted.";

  LOGF( Progress ) << "Started roads\' splines generation...";
  GenerateSplines();
  LOGF( Progress ) << m_roadsList.Size() << " roads\' splines generated.";

  LOGF( Progress ) << "Started roads\' models contours generation...";
  GenerateRoadModelsContours();
  size_t roadModelsCounter = 0;
  for ( size_t i = 0, cnt = m_roadsList.Size(); i < cnt; ++i )
  {
    roadModelsCounter += m_roadsList[i].GetModelsContoursCount();
  }
  LOGF( Progress ) << roadModelsCounter << " roads\' models contours generated.";

  LOGF( Progress ) << "Started roads-crossroads contours matching...";
  MatchRoadsToCrossroads( ROAD_CROSSROAD_OVERLAP_NULL );
  LOGF( Progress ) << "Roads-crossroads contours matching completed";

  LOGF( Progress ) << "Started roads\' p3d models generation...";
  GenerateRoadsP3DModels( cmdIfc );
  LOGF( Progress ) << roadModelsCounter << " roads\' p3d models generated.";

  LOGF( Progress ) << "Started crossroads\' p3d models generation...";
  GenerateCrossroadsP3DModels( cmdIfc );
  LOGF( Progress ) << m_crossroadsList.Size() << " crossroads\' p3d models generated.";

  LOGF( Progress ) << "Started roads\' splines extension...";
  ExtendRoadSplinesToCrossroadsPosition();
  LOGF( Progress ) << "Roads\' splines extension completed";

  if ( m_maskSize > 0 ) 
  {
    LOGF( Progress ) << "Started export mask";
    GenerateMask( cmdIfc );
    LOGF( Progress ) << "Export mask completed";
  }

  if ( EXPORT_DXF )
  {
    LOGF( Progress ) << "Started dxf export...";
    ExportDxf();
    LOGF( Progress ) << "Dxf export completed.";
  }
}

//-----------------------------------------------------------------------------

bool RoadsGenerator::CreateCrossroadList( size_t crossRoadResolution, int* failIndex )
{
  for ( size_t i = 0, cnt = m_roadsList.Size(); i < cnt; ++i )
  {
    // the crossroads canditates are all the roads ending points

    Road& road = m_roadsList[i];
    const Shapes::DVertex& start = road.GetShapeStartingVertex();
    const Shapes::DVertex& end   = road.GetShapeEndingVertex();

    Crossroad cStart( start, crossRoadResolution );
    int iStart = m_crossroadsList.Find( cStart );
    if ( iStart == -1 )
    {
      // this is a new crossroad
      cStart.AddXRoad( XRoad( &road, etStarting ) );
      m_crossroadsList.Add( cStart );
    }
    else
    {
      // this is an existing crossroad
      if ( !m_crossroadsList[iStart].AddXRoad( XRoad( &road, etStarting ) ) ) 
      {
        // there are already 4 roads of the same level in this crossroad
        *failIndex = iStart;
        return (false);
      }
    }

    Crossroad cEnd( end, crossRoadResolution );
    int iEnd = m_crossroadsList.Find( cEnd );
    if ( iEnd == -1 )
    {
      // this is a new crossroad
      cEnd.AddXRoad( XRoad( &road, etEnding ) );
      m_crossroadsList.Add( cEnd );
    }
    else
    {
      // this is an existing crossroad
      if ( !m_crossroadsList[iEnd].AddXRoad( XRoad( &road, etEnding ) ) ) 
      {
        // there are already 4 roads of the same level in this crossroad
        *failIndex = iEnd;
        return (false);
      }
    }
  }

  // now we have to remove from the candidates all the crossroads with
  // only a road
  size_t i = 0;
  while ( i < m_crossroadsList.Size() )
  {
    if ( m_crossroadsList[i].XRoadsCount() == 1 )
    {
      // reset roads ends as terminator
      if ( m_crossroadsList[i].GetXRoad( 0 ).GetRoadEndType() == etStarting )
      {
        m_crossroadsList[i].GetXRoad( 0 ).GetRoad()->SetStartTerminator( true );
      }
      else
      {
        m_crossroadsList[i].GetXRoad( 0 ).GetRoad()->SetEndTerminator( true );
      }
      m_crossroadsList.Remove( i );
      --i;
    }
    ++i;
  }

  // now we will remove from the candidates all crossroads where all
  // the roads have different levels
  i = 0;
  while ( i < m_crossroadsList.Size() )
  {
    if ( m_crossroadsList[i].XRoadsCount() == m_crossroadsList[i].LevelsCount() )
    {
      // reset roads ends as terminator
      for ( size_t j = 0, cnt = m_crossroadsList[i].XRoadsCount(); j < cnt; ++j )
      {
        if ( m_crossroadsList[i].GetXRoad( j ).GetRoadEndType() == etStarting )
        {
          m_crossroadsList[i].GetXRoad( j ).GetRoad()->SetStartTerminator( true );
        }
        else
        {
          m_crossroadsList[i].GetXRoad( j ).GetRoad()->SetEndTerminator( true );
        }
      }
      m_crossroadsList.Remove( i );
      --i;
    }
    ++i;
  }			
  return (true);
}

//-----------------------------------------------------------------------------

void RoadsGenerator::GenerateCrossroadsContour()
{
  ProgressBar< int > pb( m_crossroadsList.Size() );
  for ( size_t i = 0, cnt = m_crossroadsList.Size(); i < cnt; ++i )
  {
    pb.AdvanceNext( 1 );
    m_crossroadsList[i].CalculateLevelToBuild();
    m_crossroadsList[i].CalculateRadius();
    m_crossroadsList[i].CreateBuilders();
    m_crossroadsList[i].GenerateCrossroadContour();
  }
}

//-----------------------------------------------------------------------------

void RoadsGenerator::AdjustRoadsEnds()
{
  ProgressBar< int > pb( m_crossroadsList.Size() );
  for ( size_t i = 0, cnt = m_crossroadsList.Size(); i < cnt; ++i )
  {
    pb.AdvanceNext( 1 );
    m_crossroadsList[i].AdjustRoadsEnds();
  }
}

//-----------------------------------------------------------------------------

void RoadsGenerator::GenerateSplines()
{
  ProgressBar< int > pb( m_roadsList.Size() );
  for ( size_t i = 0, cnt = m_roadsList.Size(); i < cnt; ++i )
  {
    pb.AdvanceNext( 1 );
    if ( !m_roadsList[i].GenerateSpline() )
    {
      m_logFile << "Unable to spline road number " << i << ", starting at (";
      m_logFile << m_roadsList[i].GetShapeVertex( 0 ).x << ", " << m_roadsList[i].GetShapeVertex( 0 ).y << ")";
      m_logFile << ", is it too short ?" << endl;
    }
  }
}

//-----------------------------------------------------------------------------

void RoadsGenerator::GenerateRoadModelsContours()
{
  ProgressBar< int > pb( m_roadsList.Size() );
  for ( size_t i = 0, cnt = m_roadsList.Size(); i < cnt; ++i )
  {
    pb.AdvanceNext( 1 );
    m_roadsList[i].GenerateModelsContours( ROAD_MAX_MODEL_DIMENSION );
  }
}

//-----------------------------------------------------------------------------

void RoadsGenerator::MatchRoadsToCrossroads( double overlap )
{
  ProgressBar< int > pb( m_crossroadsList.Size() );
  for ( size_t i = 0, cnt = m_crossroadsList.Size(); i < cnt; ++i )
  {
    pb.AdvanceNext( 1 );
    m_crossroadsList[i].MatchXRoadsWithContour( overlap );
  }
}

//-----------------------------------------------------------------------------

void RoadsGenerator::GenerateRoadsP3DModels( IMainCommands* cmdIfc )
{
// check to see if there are identical models
// uncomment to run the check
/*
  unsigned int counter = 0;
  for (unsigned int i = 0, cnt = m_RoadsList.Size(); i < cnt; ++i)
  {
    const vector<Contour>& curContourList = m_RoadsList[i].GetModelsContours();
    for (unsigned int j = 0; j < i; ++j)
    {
      const vector<Contour>& oldContourList = m_RoadsList[j].GetModelsContours();
      for (unsigned int k = 0, cCcnt = curContourList.size(); k < cCcnt; ++k)
      {
        const Contour& curContour = curContourList[k];
        for (unsigned int m = 0, oCcnt = oldContourList.size(); m < oCcnt; ++m)
        {
          const Contour& oldContour = oldContourList[m];
          if (curContour.Equal(oldContour))
          {
            counter++;
          }
        }
      }
    }	
  }
*/

  ProgressBar< int > pb( m_roadsList.Size() );
  for ( size_t i = 0, cnt = m_roadsList.Size(); i < cnt; ++i )
  {
    pb.AdvanceNext( 1 );
    m_roadsList[i].GenerateP3DModels( cmdIfc, i, m_modelsPath, m_texturesPath );
  }
}

//-----------------------------------------------------------------------------

void RoadsGenerator::GenerateCrossroadsP3DModels( IMainCommands* cmdIfc )
{
  ProgressBar< int > pb( m_crossroadsList.Size() );
  for ( size_t i = 0, cnt = m_crossroadsList.Size(); i < cnt; ++i )
  {
    pb.AdvanceNext( 1 );
    m_crossroadsList[i].GenerateP3DModels( cmdIfc, m_modelsPath, m_texturesPath );
  }
}

//-----------------------------------------------------------------------------

void RoadsGenerator::ExtendRoadSplinesToCrossroadsPosition()
{
  ProgressBar< int > pb( m_crossroadsList.Size() );
  for ( size_t i = 0, cnt = m_crossroadsList.Size(); i < cnt; ++i )
  {
    pb.AdvanceNext( 1 );
    m_crossroadsList[i].ExtendXRoadSplinesToModelPosition();
  }
}

//-----------------------------------------------------------------------------

void RoadsGenerator::GenerateMask( IMainCommands* cmdIfc )
{
  const size_t maxSize = 8192;
  DoubleRect mapRect = cmdIfc->GetObjectMap()->GetMapArea();

  size_t curSize = min( maxSize, m_maskSize );

  for ( size_t x = 0; x < m_maskSize; x += maxSize )
  {
    for ( size_t y = 0; y < m_maskSize; y += maxSize )
    {
      char buf[50];
      sprintf( buf, "mask_%d_%d", x, y );
      Texture mask( m_texturesPath, buf, curSize );

      HDC display = ::CreateDC( "DISPLAY", 0, 0, 0 );
      if ( mask.GetDC() == 0 ) { mask.SetDC( CreateCompatibleDC( display ) ); }
      HDC dc = mask.GetDC();
      if ( mask.GetMask() == 0 ) 
      {
        BITMAPINFO bmpInfo;
        ZeroMemory( &bmpInfo, sizeof( BITMAPINFO ) );
        bmpInfo.bmiHeader.biSize = sizeof( BITMAPINFOHEADER );
        bmpInfo.bmiHeader.biWidth = curSize;
        bmpInfo.bmiHeader.biHeight = curSize;
        bmpInfo.bmiHeader.biPlanes = 1;
        bmpInfo.bmiHeader.biBitCount = 32;

        PVOID bits;
        mask.SetMask( CreateDIBSection( display, &bmpInfo, DIB_RGB_COLORS, &bits, NULL, 0 ) );

        HBITMAP bmp = mask.GetMask();
        DeleteDC( display );

        HBITMAP oldBmp = (HBITMAP)SelectObject( dc, bmp );

        RECT whole;
        whole.left = whole.top = 0;
        whole.right = whole.bottom = m_maskSize;
        FillRect( dc, &whole, (HBRUSH)GetStockObject( WHITE_BRUSH ) );

        for ( size_t i = 0, cnt = m_roadsList.Size(); i < cnt; ++i )
        {
          m_roadsList[i].PaintOnMask( dc, bmp, mapRect, x, y, m_maskSize );
        }

        for ( size_t i = 0, cnt = m_crossroadsList.Size(); i < cnt; ++i )
        {
          m_crossroadsList[i].PaintOnMask( dc, bmp, mapRect, x, y, m_maskSize );
        }

        if ( !mask.Save() ) 
        {
          throw new std::exception( "Unable to save mask file (tga)" );
        }

        mask.Reset();
        SelectObject( dc, oldBmp );
      }
    }
  }
}

//-----------------------------------------------------------------------------

void RoadsGenerator::ExportDxf()
{
  if ( m_modelsPath.GetLength() == 0 ) { return; }

  RString dxfFileName = m_modelsPath + "Data\\Roads.dxf";
  Dxf2006Writer dxfWriter( dxfFileName.Data() );

  // draws roads' shape
  for ( size_t i = 0, cnt = m_roadsList.Size(); i < cnt; ++i )
  {
    char buffer[80];
    _itoa( m_roadsList[i].GetLevel(), buffer, 10 );
    string level = "RoadsShapes" + string( buffer );
    dxfWriter.WritePolyline( m_roadsList[i].GetShape(), level, false );
  }

  // draws crossroads' circles
  for ( size_t i = 0, cnt = m_crossroadsList.Size(); i < cnt; ++i )
  {
    dxfWriter.WriteCircle( m_crossroadsList[i].GetPosition(), m_crossroadsList[i].GetRadius(), "CrossroadsDimensions" );
  }

  // draws crossroads' contour
  for ( size_t i = 0, cnt = m_crossroadsList.Size(); i < cnt; ++i )
  {
    char buffer[80];
    _itoa( m_crossroadsList[i].GetLevel(), buffer, 10 );
    string level = "CrossroadsContours" + string( buffer );
    dxfWriter.WritePolyline( m_crossroadsList[i].GetContour().GetVertices(), level, true );
  }

  // draws roads' spline
  for ( size_t i = 0, cnt = m_roadsList.Size(); i < cnt; ++i )
  {
    char buffer[80];
    _itoa( m_roadsList[i].GetLevel(), buffer, 10 );
    string level = "RoadsSplines" + string( buffer );
    dxfWriter.WritePolyline( m_roadsList[i].GetSpline(), level, false );
  }

  // draws roads' models's contours
  for ( size_t i = 0, cnt = m_roadsList.Size(); i < cnt; ++i )
  {
    char buffer[80];
    _itoa( m_roadsList[i].GetLevel(), buffer, 10 );
    string level = "RoadsModelsContours" + string( buffer );
    const vector< Contour >& contours = m_roadsList[i].GetModelsContours();
    for ( size_t j = 0, cntC = contours.size(); j < cntC; ++j )
    {
      dxfWriter.WritePolyline( contours[j].GetVertices(), level, true );
    }
  }

  // draws roads' path
  for ( size_t i = 0, cnt = m_roadsList.Size(); i < cnt; ++i )
  {
    dxfWriter.WritePolyline( m_roadsList[i].GetModelsPositions(), "RoadsPaths", false );
  }
}

//-----------------------------------------------------------------------------
