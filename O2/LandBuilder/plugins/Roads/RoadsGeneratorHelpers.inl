//-----------------------------------------------------------------------------

template < class T >
size_t TList< T >::Size() const
{
	return (m_list.size());
}

//-----------------------------------------------------------------------------

template < class T >
void TList< T >::Add( const T& t )
{
	m_list.push_back( t );
}

//-----------------------------------------------------------------------------

template < class T >
void TList< T >::Remove( size_t index )
{
	assert( index < m_list.size() );
	m_list.erase( m_list.begin() + index );
}

//-----------------------------------------------------------------------------

template < class T >
T& TList< T >::operator [] ( size_t index )
{
	assert( index < m_list.size() );
	return (m_list[index]);
}

//-----------------------------------------------------------------------------

template < class T >
const T& TList< T >::operator [] ( size_t index ) const
{
	assert( index < m_list.size() );
	return (m_list[index]);
}

//-----------------------------------------------------------------------------
