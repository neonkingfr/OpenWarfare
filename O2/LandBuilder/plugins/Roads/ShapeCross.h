//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <shapemultipoint.h>
#include <IShapeExtra.h>

//-----------------------------------------------------------------------------

class ShapeCross : public Shapes::ShapeMultiPoint, public IShapeExtra
{
  RString m_model;
  double  m_scale;
  double  m_rotation;

public:

  ShapeCross( RString model, double scale, double rotation,
              const Array< size_t >& points = Array< size_t >( 0, 0 ),
              Shapes::VertexArray* vx = NULL )
  : ShapeMultiPoint( points, vx )
	, m_model( model )
	, m_scale( scale )
  , m_rotation( rotation ) 
	{
	}
  
  const RString& GetModel() const 
	{
		return (m_model);
	}

  double GetScale() const 
	{
		return (m_scale);
	}

  double GetRotation() const 
	{
		return (m_rotation);
	}

  virtual void* GetInterfacePtr( size_t ifcuid );

  virtual ShapeCross* NewInstance( const ShapeCross& other ) const
  {
    return (new ShapeCross( other ));
  }

  virtual Shapes::DVector NearestEdge( const Shapes::DVector& vx ) const;
	virtual const char* GetShapeTypeName() const;

  static const char* s_name;
};

//-----------------------------------------------------------------------------
