//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "ShapeCross.h"

//-----------------------------------------------------------------------------

const char* ShapeCross::s_name = "cross";

//-----------------------------------------------------------------------------

void* ShapeCross::GetInterfacePtr( size_t ifcuid )
{
  switch ( ifcuid )
  {
  case IShapeExtra::IFCUID: return (static_cast< IShapeExtra* >( this )); 
  default:                  return (Shapes::ShapeMultiPoint::GetInterfacePtr( ifcuid ));
  }
}

//-----------------------------------------------------------------------------

Shapes::DVector ShapeCross::NearestEdge( const Shapes::DVector& vx ) const 
{
  return (Shapes::ShapeMultiPoint::NearestToEdge( vx ));
}

//-----------------------------------------------------------------------------

const char* ShapeCross::GetShapeTypeName() const 
{
  return (s_name);
}

//-----------------------------------------------------------------------------

