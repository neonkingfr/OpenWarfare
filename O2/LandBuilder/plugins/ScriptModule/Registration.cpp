#include "StdAfx.h"
#include <ModuleRegistrator.h>
#include "ScriptModule.h"

namespace LandBuilder2
{
  using namespace LandBuildInt;



class ScriptModuleHelper: public RegisterModule<Modules::ScriptModule>
  {
  public:
    ScriptModuleHelper(const RStringI &name):RegisterModule<Modules::ScriptModule>(name) {}
    IBuildModule *CreateModule(const char *params) const
    {
      Modules::ScriptModule* mod = new Modules::ScriptModule();
      if (mod->LoadScript(params))
      {
        return mod;
      }
      else
      {
        delete mod;        
        return 0;
      }
    }
  };


  ScriptModuleHelper _register16("Script");

  }
