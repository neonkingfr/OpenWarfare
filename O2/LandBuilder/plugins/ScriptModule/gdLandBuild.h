//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/Evaluator/express.hpp>
#include <shared/IShape.h>

//-----------------------------------------------------------------------------

#define TYPES_SHAPE(XX, Category) \
  XX("Shape",GdShapeTypeId,CreateShapeType,"@Shape","Shape","One shape from shapefile",Category)\

//-----------------------------------------------------------------------------

TYPES_SHAPE( DECLARE_TYPE, "LandBuilder" )

//-----------------------------------------------------------------------------

class IGdShape : public GameData
{
public:
  virtual const Shapes::IShape* GetShapeRef() const = 0;

  virtual const GameType& GetType() const  
  {
    return (GdShapeTypeId);
  }

  virtual RString GetText() const;
  virtual bool IsEqualTo( const GameData* data ) const;

  virtual const char* GetTypeName() const 
  {
    return ("Shape");
  }

  static IGdShape* Instance( GameData* gd ) 
  {
    return (static_cast< IGdShape* >( gd ));
  }

  static const IGdShape* Instance( const GameData* gd ) 
  {
    return (static_cast< const IGdShape* >( gd ));
  }

  static const Shapes::IShape* Shape( const GameData* gd ) 
  {
    return (Instance( gd )->GetShapeRef());
  }
};

//-----------------------------------------------------------------------------

class GdConstShape : public IGdShape
{
  const Shapes::IShape* m_shape;

public:
  GdConstShape( const Shapes::IShape* shape )
  : m_shape( shape ) 
  {
  }

  virtual const Shapes::IShape* GetShapeRef() const 
  {
    return (m_shape);
  }

  virtual GameData* Clone() const 
  {
    return (new GdConstShape( *this ));
  }
};

//-----------------------------------------------------------------------------

class GdTempShape: public IGdShape
{
  Shapes::IShape*     m_shape;
  Shapes::VertexArray m_vxArray;

public:
  GdTempShape()
  : m_shape( NULL ) 
  {
  }

  GdTempShape( const GdTempShape& other)
  : m_shape( other.m_shape->Copy( &m_vxArray ) ) 
  {
  }

  Shapes::VertexArray* GetVertexArray() 
  {
    return (&m_vxArray);
  }

  void SetShape( Shapes::IShape* shp )
  {
    delete m_shape;
    m_shape = shp;
  }

  ~GdTempShape() 
  {
    delete m_shape;
  }

  virtual GameData* Clone() const 
  {
    return (new GdTempShape( *this ));
  }

  virtual const Shapes::IShape* GetShapeRef() const 
  {
    return (m_shape);
  }
};

//-----------------------------------------------------------------------------

class GdLandBuild
{
public:
  static void RegisterToGameState( GameState* gState );
};

//-----------------------------------------------------------------------------
