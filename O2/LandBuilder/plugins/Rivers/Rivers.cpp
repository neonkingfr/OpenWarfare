//-----------------------------------------------------------------------------
// File: Rivers.cpp
//
// Desc: module to generate procedural rivers
//
// Auth: Enrico Turri 
//
// Copyright (c) 2009 Bohemia Interactive a.s. All rights reserved
//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "Rivers.h"

//-----------------------------------------------------------------------------

static LandBuilder2::RegisterModule< CRivers > rivers( "Rivers" );

//-----------------------------------------------------------------------------

CRivers::CRivers()
: m_shapesCount( 0 )
, m_containmentMatrix( NULL )
, m_noWaterElevation( CGConsts::ZERO )
, m_filterSize( 3 )
, m_filePrefix( "" )
{
}

//-----------------------------------------------------------------------------

CRivers::~CRivers()
{
  if ( m_containmentMatrix ) { delete m_containmentMatrix; }

  // closes log file
	if ( !m_logFile.fail() ) { m_logFile.close(); } 
}

//-----------------------------------------------------------------------------

void CRivers::Run( IMainCommands* cmdIfc )
{
  RString taskName = cmdIfc->GetCurrentTask()->GetTaskName();
  if ( taskName != m_currTaskName )
  {
    m_shapesCount  = 0;
    m_currTaskName = taskName;
  }

	++m_shapesCount;
	LandBuilder2::ParamParser params( cmdIfc );

	// this part contains all the "global" variables and it is run only the
	// first time we enter this module
	if ( m_shapesCount == 1 )
	{
		// sets the working path
		m_workingPath = cmdIfc->GetCurrentTask()->GetWorkFolder();

#ifdef _DEBUG
    m_dxfPath = m_workingPath + "\\dxf\\";
		Pathname::CreateFolder( m_dxfPath );
#endif

    // opens log file
    m_logFile.open( m_workingPath + "\\Rivers.log", std::ios::out | std::ios::trunc );
    if ( m_logFile.fail() )
    {
      LOGF( Error ) << "Unable to open the log file.";
    }

    LOGF( Info ) << "===========================================================";
    LOGF( Info ) << "Rivers 1.1.5                                               ";
    LOGF( Info ) << "===========================================================";
    m_logFile << "============" << endl;
    m_logFile << "Rivers 1.1.5" << endl;
    m_logFile << "============" << endl << endl;

		// gets maps parameters from calling cfg file
    if ( !GetTerrainParameters( cmdIfc, params ) ) { return; }

    // gets the file prefix
    m_filePrefix = "";
	  try 
	  {
		  m_filePrefix = params.GetTextValue( FILE_PREFIX );
	  }
	  catch ( LandBuilder2::ParamParserException& ) 
	  {
		  LOGF( Error ) << "Warning: Missing \"" << FILE_PREFIX << "\" field in class Parameters. Using no prefix.";
		  m_logFile << "Warning: Missing \"" << FILE_PREFIX << "\" field in class Parameters. Using no prefix." << endl;
      m_filePrefix = "";
	  }

		// loads terrain
		LoadHighMap();

    m_noWaterElevation = m_terrainIn.GetNoWaterElevation();
    m_waterDepth       = abs( m_terrainIn.GetWaterDepth() );

    m_logFile << "Applied no water elevation (m): " << m_noWaterElevation << endl;
    m_logFile << "Applied water depth (m): " << m_waterDepth << endl << endl;
  }

	// this part apply to all the shapes coming from the shapefile
	LOGF( Info ) << "Working on shape: " << m_shapesCount << "                        ";

	// gets shape and its properties
	const Shapes::IShape* curShape = cmdIfc->GetActiveShape();

	// shape validation
	// checks for multipart shapes
	if ( curShape->GetPartCount() > 1 )
	{
		LOGF( Error ) << "Rivers module accepts only shapefiles containing non-multipart polygons.";
		m_logFile << "WARNING: Skipped shape (" << m_shapesCount << ")" << endl;
		m_logFile << "Rivers module accepts only shapefiles containing non-multipart polygons." << endl;
		return;
	}

	// checks for known shapes
	const IShapeExtra* extra = curShape->GetInterface< IShapeExtra >();
	if ( !extra ) 
	{
		LOGF( Error ) << "Found unknown shape in shapefile. Cannot get type of that shape.";
		m_logFile << "WARNING: Skipped shape (" << m_shapesCount << ")" << endl;
		m_logFile << "Cannot get type of that shape." << endl;
		return;
	}

	// checks for polygons 
	String shapeType = extra->GetShapeTypeName();
	if ( shapeType != String( SHAPE_NAME_POLYGON ) ) 
	{
		LOGF( Error ) << "Rivers module accepts only shapefiles containing non-multipart polygons.";
		m_logFile << "WARNING: Skipped shape (" << m_shapesCount << ")" << endl;
		m_logFile << "Rivers module accepts only shapefiles containing non-multipart polygons." << endl;
		return;
	}

  // copies shape to polygon 
	CGPolygon2 contour;
	for ( size_t i = 0, cntI = curShape->GetVertexCount(); i < cntI; ++i )
	{
		const Shapes::DVertex& v = curShape->GetVertex( i );
		contour.AppendVertex( v.x, v.y );
	}

	// pre-processes contour
	contour.RemoveConsecutiveDuplicatedVertices( TOLERANCE_CM );

	if ( contour.VerticesCount() < 3 )
	{
		LOGF( Error ) << "Skipped shape: found degenerate polygon (vertices count).";
		m_logFile << "WARNING: Skipped shape n: " << m_shapesCount << " because reduced to a degenerate polygon (vertices count)." << endl << endl;
		return;
	}

	Float area = contour.Area();
	if ( area < TOLERANCE_DM )
	{
		LOGF( Error ) << "Skipped shape: found degenerate polygon (area).";
		m_logFile << "WARNING: Skipped shape n: " << m_shapesCount << " because reduced to a degenerate polygon (area)." << endl << endl;
		return;
	}

  // For each polygon we calculate the river bed and the water surface.
  // The river bed is generated by pushing down the terrain (m_terrainIn) vertices
  // contained into the polygon by a fixed amount.
  // The water surface of each shape is stored into a separated height map.
  // All these heightmaps are then merged togheter into OnEndPass().
  // {
  Elevations& hmData = m_terrainIn.GetHmData();

  contour.Triangulate();

  Float left   = contour.MinX();
  Float right  = contour.MaxX();
  Float top    = contour.MaxY();
  Float bottom = contour.MinY();

  m_riverBed     = hmData.subGrid( left, bottom, right, top ); 
  m_waterSurface = hmData.subGrid( left, bottom, right, top ); 
  m_waterSurface.setAllWorldWToSingleValue( NO_ELEVATION );

  DWORD startingTime = GetTickCount();
  CalculateContainmentMatrix( contour );
	DWORD calculateContainmentMatrixTime = GetTickCount();
  CalculateRiverBed();
	DWORD calculateRiverBedTime = GetTickCount();
  CalculateWaterSurface( contour );
	DWORD calculateWaterSurfaceTime = GetTickCount();

  Elevations waterLayer = hmData;
  waterLayer.setAllWorldWToSingleValue( NO_ELEVATION );

  MergeSurfaces( m_riverBed, hmData );
	DWORD mergeRiverBedTime = GetTickCount();

  MergeSurfaces( m_waterSurface, waterLayer );
  DWORD mergeWaterSurfaceTime = GetTickCount();
  // }

  m_waterLayers.push_back( waterLayer );

	m_logFile << "-------------------------------------------------------------" << endl;
	m_logFile << "Time statistics - Shape n." << m_shapesCount << endl;
	m_logFile << "-------------------------------------------------------------" << endl;
	m_logFile << "Containment Matrix calculation: " << calculateContainmentMatrixTime - startingTime << " ms" << endl;
	m_logFile << "River Bed calculation:          " << calculateRiverBedTime - calculateContainmentMatrixTime << " ms" << endl;
	m_logFile << "Water Surface calculation:      " << calculateWaterSurfaceTime - calculateRiverBedTime << " ms" << endl;
	m_logFile << "River Bed merging:              " << mergeRiverBedTime - calculateWaterSurfaceTime << " ms" << endl;
	m_logFile << "Water Surface merging:          " << mergeWaterSurfaceTime - mergeRiverBedTime << " ms" << endl;
	m_logFile << "-------------------------------------------------------------" << endl << endl;
}

//-----------------------------------------------------------------------------

void CRivers::OnEndPass( IMainCommands* cmdIfc )
{
  Elevations& terrain              = m_terrainIn.GetHmData();
  Elevations  waterLayer           = terrain;
  Elevations  terrainAndWaterLayer = terrain;
  waterLayer.setAllWorldWToSingleValue( m_noWaterElevation );

//  Float resW = terrain.getResolutionW();
  Float threshold = (Float)1;

  size_t sizeU = terrain.getSizeU();
  size_t sizeV = terrain.getSizeV();

  String filename = String( m_terrainIn.GetFileName().c_str() );
  filename = filename.Substring( 0, filename.GetLength() - 4 );

  for ( size_t i = 0, cntI = m_waterLayers.size(); i < cntI; ++i )
  {
    const Elevations& currWaterLayer = m_waterLayers[i];
  
    // water layer
    for ( size_t u = 0; u < sizeU; ++u )
    {
      for ( size_t v = 0; v < sizeV; ++v )
      {
        Float elev = currWaterLayer.getWorldWAt( u, v );
        if ( abs( elev - NO_ELEVATION ) > threshold ) 
        { 
          waterLayer.setWorldWAt( u, v, elev ); 
        }
      }
    }
  }

  // smooth water layer
  CGaussianFilter filter( m_filterSize );
  Elevations currReference = waterLayer;
  size_t totSize = sizeU * sizeV;

  for ( size_t u = 0; u < sizeU; ++u )
  {
    for ( size_t v = 0; v < sizeV; ++v )
    {
      Float newElevation  = CGConsts::ZERO;
      Float currElevation = currReference.getWorldWAt( u, v );

      if ( abs( currElevation - m_noWaterElevation ) > threshold ) 
      {
        Float weight = CGConsts::ZERO;

        int halfFilterSize = filter.GetSize() / 2;

        for ( int x = -halfFilterSize; x <= halfFilterSize; ++x )
        {
          for ( int y = -halfFilterSize; y <= halfFilterSize; ++y )
          {
            int uu = u + x;
            int vv = v + y;
            if ( (0 <= uu) && uu < (static_cast< int >( sizeU )) )
            {
              if ( (0 <= vv) && vv < (static_cast< int >( sizeV )) )
              {
                Float tempElevation = currReference.getWorldWAt( uu, vv );
                if ( abs( tempElevation - m_noWaterElevation ) > threshold ) 
                {
                  Float currWeight = filter.ValueAt( x, y );
                  weight       += currWeight;
                  newElevation += currWeight * tempElevation;
                }
              }
            }
          }
        }

        if ( weight > CGConsts::ZERO )
        {
          newElevation /= weight;
        }
        else
        {
          newElevation = currElevation;
        }

         waterLayer.setWorldWAt( u, v, newElevation );
      }
    }
  }


  // terrain + water layer
  for ( size_t u = 0; u < sizeU; ++u )
  {
    for ( size_t v = 0; v < sizeV; ++v )
    {
      Float elev = waterLayer.getWorldWAt( u, v );
      if ( abs( elev - m_noWaterElevation ) > threshold ) { terrainAndWaterLayer.setWorldWAt( u, v, elev ); }
    }
  }

  int lastSlashPos   = filename.ReverseFind( '\\' );
  String path        = filename.Substring( 0, lastSlashPos + 1 );
  String oldFilename = filename.Substring( lastSlashPos + 1, filename.GetLength() );

  // currently the module exports to asc file only
  String terrainFilename         = path + m_filePrefix + oldFilename + "_terrain"; 
  String waterFilename           = path + m_filePrefix + oldFilename + "_water"; 
  String terrainAndWaterFilename = path + m_filePrefix + oldFilename + "_terrain_water"; 

  terrain.saveAsWorldToARCINFOASCIIGrid( (terrainFilename + ".asc").Data() );
  waterLayer.saveAsWorldToARCINFOASCIIGrid( (waterFilename + ".asc").Data() );
  terrainAndWaterLayer.saveAsWorldToARCINFOASCIIGrid( (terrainAndWaterFilename + ".asc").Data() );

  // copies the prj file if present
  String prjFilename = filename + ".prj";
	ifstream test( prjFilename, std::ios::binary );
	if ( !test.fail() )
	{
    test.close();

		const int BUFFER_SIZE = 4096;
		char buffer[BUFFER_SIZE];

    ifstream ifs( prjFilename, std::ios::binary );
    ofstream ofs_terrain( terrainFilename + ".prj", std::ios::binary );
    ofstream ofs_water( waterFilename + ".prj", std::ios::binary );
    ofstream ofs_both( terrainAndWaterFilename + ".prj", std::ios::binary );

		while ( !ifs.eof() ) 
		{
			ifs._Read_s( buffer, BUFFER_SIZE, BUFFER_SIZE );
			if ( !ifs.bad() ) 
			{
				ofs_terrain.write( buffer, ifs.gcount() );
				ofs_water.write( buffer, ifs.gcount() );
				ofs_both.write( buffer, ifs.gcount() );
			}
		}

		ifs.close();
		ofs_terrain.close();
		ofs_water.close();
		ofs_both.close();
  }
}

//-----------------------------------------------------------------------------

bool CRivers::GetTerrainParameters( IMainCommands* cmdIfc, LandBuilder2::ParamParser& params )
{
  // gets demType from cfg file
  string demType = "";
	try 
	{
		demType = params.GetTextValue( DEM_TYPE );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
			LOGF( Error ) << "ERROR: Missing \"" << DEM_TYPE << "\" field in class Parameters.";
			m_logFile << "ERROR: Missing \"" << DEM_TYPE << "\" field in class Parameters." << endl;
      return (false);
	}

  m_terrainIn.SetDemType( demType );

  // checks demType
  if ( (demType != S_DTED2) && 
       (demType != S_USGSDEM) && 
       (demType != S_ARCINFOASCII) && 
       (demType != S_XYZ) )
  {
		LOGF( Error ) << "ERROR: Unknown dem type specified in \"" << DEM_TYPE << "\" field in class Parameters.";
		m_logFile << "ERROR: Unknown dem type specified in \"" << DEM_TYPE << "\" field in class Parameters." << endl;
	  return (false);
  }

  // gets filename from cfg file
  string filename = "";
	try 
	{
		filename = params.GetTextValue( FILENAME );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
			LOGF( Error ) << "ERROR: Missing \"" << FILENAME << "\" field in class Parameters.";
			m_logFile << "ERROR: Missing \"" << FILENAME << "\" field in class Parameters." << endl;
      return (false);
	}

  // checks file extension
  if ( demType == S_DTED2 ) 
  {
    String tmpFileName = filename.c_str();
	  tmpFileName.Upper();

	  if ( !(tmpFileName.Find( ".DT2" ) == tmpFileName.GetLength() - 4) )
	  {
			LOGF( Error ) << "ERROR: The file specified into the \"" << FILENAME << "\" field of class Parameters should have extension .dt2";
			m_logFile << "ERROR: The file specified into the \"" << FILENAME << "\" field of class Parameters should have extension .dt2" << endl;
		  return (false);
	  }
  }
  else if ( demType == S_USGSDEM ) 
  {
	  String tmpFileName = filename.c_str();
	  tmpFileName.Upper();

	  if ( !(tmpFileName.Find( ".DEM" ) == tmpFileName.GetLength() - 4) )
	  {
			LOGF( Error ) << "ERROR: The file specified into the \"" << FILENAME << "\" field of class Parameters should have extension .dem";
			m_logFile << "ERROR: The file specified into the \"" << FILENAME << "\" field of class Parameters should have extension .dem" << endl;
		  return (false);
	  }
  }
  else if ( demType == S_ARCINFOASCII ) 
  {
	  String tmpFileName = filename.c_str();
	  tmpFileName.Upper();

	  if ( (!(tmpFileName.Find( ".GRD" ) == tmpFileName.GetLength() - 4)) && 
         (!(tmpFileName.Find( ".ASC" ) == tmpFileName.GetLength() - 4)) )
	  {
			LOGF( Error ) << "ERROR: The file specified into the \"" << FILENAME << "\" field of class Parameters should have extension .asc or .grd";
			m_logFile << "ERROR: The file specified into the \"" << FILENAME << "\" field of class Parameters should have extension .asc or .grd" << endl;
		  return (false);
	  }
  }
  else if ( demType == S_XYZ ) 
  {
	  String tmpFileName = filename.c_str();
	  tmpFileName.Upper();

	  if ( !(tmpFileName.Find( ".XYZ" ) == tmpFileName.GetLength() - 4) )
	  {
			LOGF( Error ) << "ERROR: The file specified into the \"" << FILENAME << "\" field of class Parameters should have extension .xyz";
			m_logFile << "ERROR: The file specified into the \"" << FILENAME << "\" field of class Parameters should have extension .xyz" << endl;
		  return (false);
	  }
  }

	ifstream in( filename.c_str(), std::ios::in );
	if ( in.fail() )
	{
		LOGF( Error ) << "ERROR: unable to open the file \"" << filename;
    m_logFile << "ERROR: unable to open the  \"" << filename << endl;
		return (false);
	}
	else
	{
		in.close();
	}

  m_terrainIn.SetFileName( filename );

  // gets srcLeft from cfg file
  size_t srcLeft = 0;
	try
	{
		srcLeft = params.GetIntValue( SRC_LEFT );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << SRC_LEFT << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << SRC_LEFT << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets srcRight from cfg file
  size_t srcRight = 0;
	try
	{
		srcRight = params.GetIntValue( SRC_RIGHT );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << SRC_RIGHT << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << SRC_RIGHT << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets srcTop from cfg file
  size_t srcTop = 0;
	try
	{
		srcTop = params.GetIntValue( SRC_TOP );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << SRC_TOP << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << SRC_TOP << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets srcBottom from cfg file
  size_t srcBottom = 0;
	try
	{
		srcBottom = params.GetIntValue( SRC_BOTTOM );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << SRC_BOTTOM << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << SRC_BOTTOM << "\" field in class Parameters." << endl;
    return (false);
	}

  m_terrainIn.SetSrcLeft( srcLeft );
  m_terrainIn.SetSrcRight( srcRight );
  m_terrainIn.SetSrcTop( srcTop );
  m_terrainIn.SetSrcBottom( srcBottom );

  // gets dstLeft from cfg file
  double dstLeft = 0;
	try
	{
		dstLeft = static_cast< double >( params.GetFloatValue( DST_LEFT ) );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << DST_LEFT << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << DST_LEFT << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets dstRight from cfg file
  double dstRight = 0;
	try
	{
		dstRight = static_cast< double >( params.GetFloatValue( DST_RIGHT ) );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << DST_RIGHT << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << DST_RIGHT << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets dstTop from cfg file
  double dstTop = 0;
	try
	{
		dstTop = static_cast< double >( params.GetFloatValue( DST_TOP ) );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << DST_TOP << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << DST_TOP << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets dstBottom from cfg file
  double dstBottom = 0;
	try
	{
		dstBottom = static_cast< double >( params.GetFloatValue( DST_BOTTOM ) );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << DST_BOTTOM << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << DST_BOTTOM << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets noWaterElev from cfg file
  double noWaterElevation = CGConsts::ZERO;
	try
	{
    noWaterElevation = static_cast< double >( params.GetFloatValue( NO_WATER_ELEV ) );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << NO_WATER_ELEV << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << NO_WATER_ELEV << "\" field in class Parameters." << endl;
    return (false);
	}

  // gets waterDepth from cfg file
  double waterDepth = CGConsts::ZERO;
	try
	{
    waterDepth = static_cast< double >( params.GetFloatValue( WATER_DEPTH ) );
	}
	catch ( LandBuilder2::ParamParserException& ) 
	{
		LOGF( Error ) << "ERROR: Missing \"" << WATER_DEPTH << "\" field in class Parameters.";
		m_logFile << "ERROR: Missing \"" << WATER_DEPTH << "\" field in class Parameters." << endl;
    return (false);
	}

  m_terrainIn.SetDstLeft( dstLeft );
  m_terrainIn.SetDstRight( dstRight );
  m_terrainIn.SetDstTop( dstTop );
  m_terrainIn.SetDstBottom( dstBottom );
  m_terrainIn.SetNoWaterElevation( noWaterElevation );
  m_terrainIn.SetWaterDepth( waterDepth );

  return (true);
}

//-----------------------------------------------------------------------------

bool CRivers::LoadHighMap()
{
	Elevations hmData;

  string demType = m_terrainIn.GetDemType();

	if ( demType == S_DTED2 )
	{
		if ( !(hmData.loadFromDT2( m_terrainIn.GetFileName() )) )
		{
      LOGF( Error ) << ">>> Error while loading DTED2 file <<<";
		  m_logFile << ">>> Error while loading DTED2 file <<<" << endl;
			return (false);
		}
	}
	else if ( demType == S_USGSDEM )
	{
		if ( !(hmData.loadFromUSGSDEM( m_terrainIn.GetFileName() )) )
		{
      LOGF( Error ) << ">>> Error while loading USGS DEM file <<<";
		  m_logFile << ">>> Error while loading USGS DEM file <<<" << endl;
			return (false);
		}
		else
		{
			// if the elevation units in the DEM are feet, converts to METERS
			if ( hmData.getWorldUnitsW() == Elevations::euFEET )
			{
				hmData.convertWorldUnitsW( Elevations::euMETERS );
			}
		}
	}
	else if ( demType == S_ARCINFOASCII )
	{
		if ( !(hmData.loadFromARCINFOASCIIGrid( m_terrainIn.GetFileName() )) )
		{
      LOGF( Error ) << ">>> Error while loading Arc/Info Ascii Grid file <<<";
		  m_logFile << ">>> Error while loading Arc/Info Ascii Grid file <<<" << endl;
			return (false);
		}
	}
  else if ( demType == S_XYZ )
	{
		if ( !(hmData.loadFromXYZ( m_terrainIn.GetFileName() )) )
		{
      LOGF( Error ) << ">>> Error while loading xyz file <<<";
		  m_logFile << ">>> Error while loading xyz file <<<" << endl;
			return (false);
		}
	}

	m_terrainIn.SetHmData( hmData );

	return (true);
}

//-----------------------------------------------------------------------------

void CRivers::CalculateContainmentMatrix( const CGPolygon2& riverContour )
{
  size_t sizeU = m_riverBed.getSizeU();
  size_t sizeV = m_riverBed.getSizeV();

  if ( m_containmentMatrix ) { delete m_containmentMatrix; }

  m_containmentMatrix = new bool[sizeU * sizeV];
  for ( size_t u = 0; u < sizeU; ++u )
  {
    for ( size_t v = 0; v < sizeV; ++v )
    {
      m_containmentMatrix[u + v * sizeU] = false;
    }
  }

  Float resolution = m_riverBed.getResolutionU();
  const vector< CGTriangle2 >& triangulation = riverContour.GetTriangulation().GetTrianglesList();
  for ( size_t i = 0, cntI = triangulation.size(); i < cntI; ++i )
  {
    const CGTriangle2& triangle = triangulation[i];
    Float left   = triangle.MinX();
    Float right  = triangle.MaxX();
    Float top    = triangle.MaxY();
    Float bottom = triangle.MinY();

    Elevations subGrid = m_riverBed.subGrid( left, bottom, right, top ); 
    size_t triSizeU = subGrid.getSizeU();
    size_t triSizeV = subGrid.getSizeV();
    size_t offsetU = static_cast< size_t >( (subGrid.getOriginWorldU() - m_riverBed.getOriginWorldU()) / resolution );
    size_t offsetV = static_cast< size_t >( (subGrid.getOriginWorldV() - m_riverBed.getOriginWorldV()) / resolution );

    if ( (triSizeU > 0) && (triSizeV > 0) )
    {
      for ( size_t u = 0; u < triSizeU; ++u )
      {
        Float x = subGrid.getWorldUAt( u );
        for ( size_t v = 0; v < triSizeV; ++v )
        {
          int index = (u + offsetU) + (v + offsetV) * sizeU;
          if ( !m_containmentMatrix[index] )
          {
            Float y = subGrid.getWorldVAt( v );
            if ( triangle.Contains( x, y, true ) )
            {
              m_containmentMatrix[(u + offsetU) + (v + offsetV) * sizeU] = true;
            }
          }
        }
      }
    }
  }
}

//-----------------------------------------------------------------------------

void CRivers::CalculateRiverBed()
{
  // the river bed is generated by pushing down the terrain vertices
  // contained into the polygon by a fixed amount and applying a smoothing filter

  if ( !m_containmentMatrix ) { return; }

  const Elevations& hmData = m_terrainIn.GetHmData();

  size_t sizeU = m_riverBed.getSizeU();
  size_t sizeV = m_riverBed.getSizeV();

  Float correction = m_waterDepth;

  for ( size_t u = 0; u < sizeU; ++u )
  {
    Float x = m_riverBed.getWorldUAt( u );
    for ( size_t v = 0; v < sizeV; ++v )
    {
      Float y = m_riverBed.getWorldVAt( v );
      size_t index = v * sizeU + u;
      Float newElevation = hmData.getWorldWAt( x, y, Elevations::imBILINEAR );
      size_t neighborsCount = 0;
      Float elevationCorrection = CGConsts::ZERO;

      if ( m_containmentMatrix[index] )
      {
        newElevation -= correction;

        // north neighbors
        if ( v < sizeV - 1 )
        {
          // north-west neighbor
          if ( u > 0 )
          {
            index = ( u - 1 ) + ( v + 1 ) * sizeU;
            x = m_riverBed.getWorldUAt( u - 1 );
            y = m_riverBed.getWorldVAt( v + 1 );
            elevationCorrection += hmData.getWorldWAt( x, y, Elevations::imBILINEAR );
            if ( m_containmentMatrix[index] ) { elevationCorrection -= correction; }
            ++neighborsCount;
          }

          // north neighbor
          index = ( u ) + ( v + 1 ) * sizeU;
          x = m_riverBed.getWorldUAt( u );
          y = m_riverBed.getWorldVAt( v + 1 );
          elevationCorrection += hmData.getWorldWAt( x, y, Elevations::imBILINEAR );
          if ( m_containmentMatrix[index] ) { elevationCorrection -= correction; }
          ++neighborsCount;

          // north-east neighbor
          if ( u < sizeU - 1 )
          {
            index = ( u + 1 ) + ( v + 1 ) * sizeU;
            x = m_riverBed.getWorldUAt( u + 1 );
            y = m_riverBed.getWorldVAt( v + 1 );
            elevationCorrection += hmData.getWorldWAt( x, y, Elevations::imBILINEAR );
            if ( m_containmentMatrix[index] ) { elevationCorrection -= correction; }
            ++neighborsCount;
          }
        }

        // south neighbors
        if ( v > 0 )
        {
          // south-west neighbor
          if ( u > 0 )
          {
            index = ( u - 1 ) + ( v - 1 ) * sizeU;
            x = m_riverBed.getWorldUAt( u - 1 );
            y = m_riverBed.getWorldVAt( v - 1 );
            elevationCorrection += hmData.getWorldWAt( x, y, Elevations::imBILINEAR );
            if ( m_containmentMatrix[index] ) { elevationCorrection -= correction; }
            ++neighborsCount;
          }

          // south neighbor
          index = ( u ) + ( v - 1 ) * sizeU;
          x = m_riverBed.getWorldUAt( u );
          y = m_riverBed.getWorldVAt( v - 1 );
          elevationCorrection += hmData.getWorldWAt( x, y, Elevations::imBILINEAR );
          if ( m_containmentMatrix[index] ) { elevationCorrection -= correction; }
          ++neighborsCount;

          // south-east neighbor
          if ( u < sizeU - 1 )
          {
            index = ( u + 1 ) + ( v - 1 ) * sizeU;
            x = m_riverBed.getWorldUAt( u + 1 );
            y = m_riverBed.getWorldVAt( v - 1 );
            elevationCorrection += hmData.getWorldWAt( x, y, Elevations::imBILINEAR );
            if ( m_containmentMatrix[index] ) { elevationCorrection -= correction; }
            ++neighborsCount;
          }
        }

        // remaining west neighbor
        if ( u > 0 )
        {
          index = ( u - 1 ) + ( v ) * sizeU;
          x = m_riverBed.getWorldUAt( u - 1 );
          y = m_riverBed.getWorldVAt( v );
          elevationCorrection += hmData.getWorldWAt( x, y, Elevations::imBILINEAR );
          if ( m_containmentMatrix[index] ) { elevationCorrection -= correction; }
          ++neighborsCount;
        }

        // remaining east neighbor
        if ( u < sizeU - 1 )
        {
          index = ( u + 1 ) + ( v ) * sizeU;
          x = m_riverBed.getWorldUAt( u + 1 );
          y = m_riverBed.getWorldVAt( v );
          elevationCorrection += hmData.getWorldWAt( x, y, Elevations::imBILINEAR );
          if ( m_containmentMatrix[index] ) { elevationCorrection -= correction; }
          ++neighborsCount;
        }
      }

      newElevation = (newElevation + elevationCorrection) / ( 1 + neighborsCount );
      m_riverBed.setWorldWAt( u, v, newElevation );
    }
  }
}

//-----------------------------------------------------------------------------

void CRivers::CalculateWaterSurface( const CGPolygon2& riverContour )
{
  const Elevations& hmData = m_terrainIn.GetHmData();

  const vector< CGTriangle2 >& triangulation = riverContour.GetTriangulation().GetTrianglesList();

  size_t maxSize   = 0;
  Float resolution = m_waterSurface.getResolutionU();
//  Float resW       = hmData.getResolutionW();
  Float threshold  = (Float)1;

  // first:
  // for each triangle in the polygon's triangulation set the elevation of all the
  // terrain vertices contained to a fixed value equal to the lowest elevation
  // of the triangle's vertices
  for ( size_t i = 0, cntI = triangulation.size(); i < cntI; ++i )
  {
    const CGTriangle2& triangle = triangulation[i];
    Float left   = triangle.MinX();
    Float right  = triangle.MaxX();
    Float top    = triangle.MaxY();
    Float bottom = triangle.MinY();

    Elevations subGrid = m_waterSurface.subGrid( left, bottom, right, top ); 
    size_t sizeU = subGrid.getSizeU();
    size_t sizeV = subGrid.getSizeV();

    if ( (sizeU > 0) && (sizeV > 0) )
    {
      maxSize = std::max( std::max( maxSize, sizeU ), sizeV );

      Float minElev = CGConsts::MAX_REAL;
      for ( size_t j = 0; j < 3; ++j )
      {
        const CGPoint2& point = triangle.Vertex( j );
        Float elev = hmData.getWorldWAt( point.GetX(), point.GetY(), Elevations::imBILINEAR );
        if ( abs( elev - NO_ELEVATION ) > threshold ) 
        {
          minElev = std::min( minElev, elev );
        }
      }

      // push down another meter
      minElev -= CGConsts::ONE;

      size_t offsetU = static_cast< size_t >( (subGrid.getOriginWorldU() - m_waterSurface.getOriginWorldU()) / resolution );
      size_t offsetV = static_cast< size_t >( (subGrid.getOriginWorldV() - m_waterSurface.getOriginWorldV()) / resolution );

      for ( size_t u = 0; u < sizeU; ++u )
      {
        for ( size_t v = 0; v < sizeV; ++v )
        {
          Float x = subGrid.getWorldUAt( u );
          Float y = subGrid.getWorldVAt( v );
          if ( triangle.Contains( x, y, true ) )
          {
            m_waterSurface.setWorldWAt( offsetU + u, offsetV + v, minElev );
          }
        }
      }
    }
  }

  // second:
  // smooths the obtained surface (formed by the union of all the triangles)
  int surfaceSizeU   = static_cast< int >( m_waterSurface.getSizeU() );
  int surfaceSizeV   = static_cast< int >( m_waterSurface.getSizeV() );
  int surfaceTotSize = surfaceSizeU * surfaceSizeV;

  Elevations currReference;
  size_t STEPSCOUNT = 1;
  for ( size_t i = 0; i < STEPSCOUNT; ++i )
  {
    CGaussianFilter filter( maxSize );
    currReference = m_waterSurface;

    for ( int u = 0; u < surfaceSizeU; ++u )
    {
      for ( int v = 0; v < surfaceSizeV; ++v )
      {
        int index = u + v * surfaceSizeU;
        Float newElevation = CGConsts::ZERO;

        if ( m_containmentMatrix[index] )
        {
          Float currElevation = currReference.getWorldWAt( u, v );
          Float weight = CGConsts::ZERO;

          int halfFilterSize = filter.GetSize() / 2;

          for ( int x = -halfFilterSize; x <= halfFilterSize; ++x )
          {
            for ( int y = -halfFilterSize; y <= halfFilterSize; ++y )
            {
              int pickX = u + x;
              int pickY = v + y;
              if ( (0 <= pickX) && (pickX < surfaceSizeU) )
              {
                if ( (0 <= pickY) && (pickY < surfaceSizeV) )
                {
                  int vIndex = pickX + pickY * surfaceSizeU;
                  if ( m_containmentMatrix[vIndex] )
                  {
                    Float currWeight = filter.ValueAt( x, y );
                    weight       += currWeight;
                    newElevation += (currWeight * currReference.getWorldWAt( pickX, pickY ));
                  }
                }
              }
            }
          }

          if ( weight > CGConsts::ZERO )
          {
            newElevation /= weight;
          }
          else
          {
            newElevation = currElevation;
          }

          m_waterSurface.setWorldWAt( u, v, newElevation );
        }
      }
    }
  }

  // third:
  // sets the borders of the triangulation
  currReference = m_waterSurface;
  for ( int u = 0; u < surfaceSizeU; ++u )
  {
    for ( int v = 0; v < surfaceSizeV; ++v )
    {
      size_t index = u + v * surfaceSizeU;
      size_t neighborsCount = 0;
      Float  newElevation   = CGConsts::ZERO;

      if ( !m_containmentMatrix[index] )
      {
        // north neighbors
        if ( v < surfaceSizeV - 1 )
        {
          // north-west neighbor
          if ( u > 0 )
          {
            index = ( u - 1 ) + ( v + 1 ) * surfaceSizeU;
            if ( m_containmentMatrix[index] ) 
            { 
              newElevation += currReference.getWorldWAt( u - 1, v + 1 ); 
              ++neighborsCount;
            }
          }

          // north neighbor
          index = ( u ) + ( v + 1 ) * surfaceSizeU;
          if ( m_containmentMatrix[index] ) 
          { 
            newElevation += currReference.getWorldWAt( u, v + 1 ); 
            ++neighborsCount;
          }

          // north-east neighbor
          if ( u < surfaceSizeU - 1 )
          {
            index = ( u + 1 ) + ( v + 1 ) * surfaceSizeU;
            if ( m_containmentMatrix[index] ) 
            { 
              newElevation += currReference.getWorldWAt( u + 1, v + 1 ); 
              ++neighborsCount;
            }
          }
        }

        // south neighbors
        if ( v > 0 )
        {
          // south-west neighbor
          if ( u > 0 )
          {
            index = ( u - 1 ) + ( v - 1 ) * surfaceSizeU;
            if ( m_containmentMatrix[index] ) 
            { 
              newElevation += currReference.getWorldWAt( u - 1, v - 1 ); 
              ++neighborsCount;
            }
          }

          // south neighbor
          index = ( u ) + ( v - 1 ) * surfaceSizeU;
          if ( m_containmentMatrix[index] ) 
          { 
            newElevation += currReference.getWorldWAt( u, v - 1 ); 
            ++neighborsCount;
          }

          // south-east neighbor
          if ( u < surfaceSizeU - 1 )
          {
            index = ( u + 1 ) + ( v - 1 ) * surfaceSizeU;
            if ( m_containmentMatrix[index] ) 
            { 
              newElevation += currReference.getWorldWAt( u + 1, v - 1 ); 
              ++neighborsCount;
            }
          }
        }

        // remaining west neighbor
        if ( u > 0 )
        {
          index = ( u - 1 ) + ( v ) * surfaceSizeU;
          if ( m_containmentMatrix[index] ) 
          { 
            newElevation += currReference.getWorldWAt( u - 1, v ); 
            ++neighborsCount;
          }
        }

        // remaining east neighbor
        if ( u < surfaceSizeU - 1 )
        {
          index = ( u + 1 ) + ( v ) * surfaceSizeU;
          if ( m_containmentMatrix[index] ) 
          { 
            newElevation += currReference.getWorldWAt( u + 1, v ); 
            ++neighborsCount;
          }
        }

        if ( neighborsCount > 0 )
        {
          m_waterSurface.setWorldWAt( u, v, newElevation / neighborsCount );
        }
      }
    }
  }

  m_filterSize = maxSize / 2;
}

//-----------------------------------------------------------------------------

void CRivers::MergeSurfaces( const Elevations& srcSurface, Elevations& dstSurface )
{
  int srcSizeU = static_cast< int >( srcSurface.getSizeU() );
  int srcSizeV = static_cast< int >( srcSurface.getSizeV() );
  int dstSizeU = static_cast< int >( dstSurface.getSizeU() );
  int dstSizeV = static_cast< int >( dstSurface.getSizeV() );

  int offsetU = static_cast< int >( (srcSurface.getOriginWorldU() - dstSurface.getOriginWorldU()) / dstSurface.getResolutionU() );
  int offsetV = static_cast< int >( (srcSurface.getOriginWorldV() - dstSurface.getOriginWorldV()) / dstSurface.getResolutionV() );

  for ( int u = 0; u < srcSizeU; ++u )
  {
    int uu = offsetU + u;
    if ( 0 <= uu && uu < dstSizeU )
    {
      for ( int v = 0; v < srcSizeV; ++v )
      {
        int vv = offsetV + v;
        if ( 0 <= vv && vv < dstSizeV )
        {
          dstSurface.setWorldWAt( uu, vv, srcSurface.getWorldWAt( u, v ) );
        }
      }
    }
  }
}

//-----------------------------------------------------------------------------
