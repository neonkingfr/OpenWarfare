//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\Shapes\ShapeHelpers.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		class AdvancedRandomPlacer : public IBuildModule
		{
		public:
			struct ShapeInput
			{
				size_t m_randomSeed;
				double m_hectareDensity;
			};

			struct ObjectInput
			{
				RString m_name;
				double  m_prob;
				double  m_normProb;
				double  m_minHeight;
				double  m_maxHeight;
				double  m_minDistance;
				int     m_counter;

				ObjectInput() 
				{
				}

				ObjectInput( RString name, double prob, double minHeight, double maxHeight, double minDistance )
				: m_name( name )
				, m_prob( prob )
				, m_minHeight( minHeight )
				, m_maxHeight( maxHeight )
				, m_minDistance( minDistance )
				, m_counter( 0 )
				{
				}

				ClassIsMovable( ObjectInput );
			};

		private:
			struct ShapeGeo
			{
				double m_minX;
				double m_maxX;
				double m_minY;
				double m_maxY;

        double WidthBB() const
        {
			    return (m_maxX - m_minX);
        }

        double HeightBB() const
        {
			    return (m_maxY - m_minY);
        }

        double AreaBB() const
        {
          return (WidthBB() * HeightBB());
        }

        double AreaHectaresBB() const
        {
          return (AreaBB() / 10000.0);
        }
			};

		public:
      AdvancedRandomPlacer();

      virtual ~AdvancedRandomPlacer()
      {
      }

			virtual void Run( IMainCommands* cmdIfc );
			ModuleObjectOutputArray* RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn, 
                                             AutoArray< AdvancedRandomPlacer::ObjectInput >& objectsIn );

      DECLAREMODULEEXCEPTION( "AdvancedRandomPlacer" )

		private:
      string m_version;
      size_t m_shapeCounter;

			const Shapes::IShape* m_currentShape;      

			ShapeGeo m_shpGeo;
			int      m_numberOfIndividuals;

			AutoArray< ObjectInput > m_objectsIn;
			ModuleObjectOutputArray  m_objectsOut;

			ShapeInput m_shapeIn;

			bool GetShapeGeoData();
			bool GetShapeParameters( IMainCommands* cmdIfc );
			bool GetObjectsParameters( IMainCommands* cmdIfc );

			bool NormalizeProbs();
			bool PtCloseToObject( const Shapes::DVector& v ) const;

			void CreateObjects( bool preview );
			int  GetRandomType();
			void ExportCreatedObjects( IMainCommands* cmdIfc );
			void WriteReport();
		};

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt
