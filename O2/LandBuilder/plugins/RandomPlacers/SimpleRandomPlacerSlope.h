//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/MultiThread/ExceptionRegister.h>

#include "..\..\landbuilder2\IBuildModule.h"
#include "..\..\landbuilder2\MapInput.h"
#include "..\..\landbuilder2\ProgressLog.h"
#include "..\..\Shapes\ShapeHelpers.h"
#include "..\..\HighMapLoaders\include\EtRectElevationGrid.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	namespace Modules
	{

//-----------------------------------------------------------------------------

		class SimpleRandomPlacerSlope : public IBuildModule
		{
		public:
			struct ShapeInput
			{
				size_t m_randomSeed;
				double m_hectareDensity;
			};

			struct ObjectInput
			{
				RString m_name;
				double  m_minSlope;
				double  m_maxSlope;
				int     m_counter;

				ObjectInput() 
        {
        }

				ObjectInput( RString name, double minSlope, double maxSlope )
        : m_name( name )
        , m_minSlope( minSlope )
        , m_maxSlope( maxSlope )
        , m_counter( 0 )
				{
				}

				ClassIsMovable( ObjectInput );
			};

		private:
			struct ShapeGeo
			{
				double m_minX;
				double m_maxX;
				double m_minY;
				double m_maxY;

        double WidthBB() const
        {
			    return (m_maxX - m_minX);
        }

        double HeightBB() const
        {
			    return (m_maxY - m_minY);
        }

        double AreaBB() const
        {
          return (WidthBB() * HeightBB());
        }

        double AreaHectaresBB() const
        {
          return (AreaBB() / 10000.0);
        }
      };

		public:
			SimpleRandomPlacerSlope();

			virtual ~SimpleRandomPlacerSlope()
      {
      }

			virtual void Run( IMainCommands* );
			ModuleObjectOutputArray* RunAsPreview( const Shapes::IShape* shape, ShapeInput& shapeIn, MapInput& mapIn,
                                             SimpleRandomPlacerSlope::ObjectInput& objectsIn );

		public:
      DECLAREMODULEEXCEPTION( "SimpleRandomPlacerSlope" )

		private:
      string m_version;
			bool   m_mapLoaded;
      size_t m_shapeCounter;

			const Shapes::IShape* m_currentShape;      

			ShapeGeo m_shpGeo;
			int      m_numberOfIndividuals;

			ObjectInput             m_objectsIn;
			ModuleObjectOutputArray m_objectsOut;

			ShapeInput  m_shapeIn;
			MapInput    m_mapIn;

			EtRectElevationGrid< double, int > m_mapData;

			bool GetShapeGeoData();
			bool GetShapeParameters( IMainCommands* cmdIfc );
			bool GetObjectsParameters( IMainCommands* cmdIfc );
			bool GetMapParameters( IMainCommands* cmdIfc );
			bool LoadHighMap();

			void CreateObjects( bool preview );
			void ExportCreatedObjects( IMainCommands* cmdIfc );
			void WriteReport();
		};

//-----------------------------------------------------------------------------

	} // namespace Modules
} // namespace LandBuildInt

