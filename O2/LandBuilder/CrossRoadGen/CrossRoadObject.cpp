#include "StdAfx.h"
#include "CrossRoadObject.h"

namespace Functors
{
	class SortRoadEnds
	{
		const Vector3& _center;
	public:
		SortRoadEnds(const Vector3& center)
		: _center(center) 
		{
		}

		int operator()(const RoadEndDesc* first, const RoadEndDesc* second) const
		{
			return first->Compare(*second, _center);
		}
	};
}

CrossRoadObject::CrossRoadObject(const Array<RoadEndDesc>& roads)
{
	bezierTeselationFactor = 0.025f;
	bezierMaxRecursions = 10;

	_roads = roads;
	Vector3 center(0,0,0);
	int cnt = 0;
	for (int i = 0; i < _roads.Size(); i++)
	{
		_roads[i].AddToCenterCalc(center, cnt);
	}
	center *= 1.0f / cnt;

	for (int i = 0; i < _roads.Size(); i++)
	{
		_roads[i].SortPoints(center);
	}

	QSort(_roads, Functors::SortRoadEnds(center));

	_texMask = 0;
	_texDC = 0;
}

template<class T>
void AppendLine(ObjectData& object, T& arr, const Vector3& pt)
{
	arr.Add(object.NPoints());
	PosT* pos = object.NewPoint();
	pos->SetPoint(Vector3(pt.X(), 0, pt.Y()));
}

static void delit_krivku(const Vector3* p, Vector3* left, Vector3* right)
{
	Vector3 hlp;

	left[0] = p[0];
	left[1] = (p[0] + p[1]) * 0.5f;
	hlp = (p[1] + p[2]) * 0.5f;
	left[2] = (left[1] + hlp) * 0.5f;
	right[3] = p[3];
	right[2] = (p[2] + p[3]) * 0.5f;
	right[1] = (hlp + right[2]) * 0.5f;
	right[0] = (right[1] + left[2]) * 0.5f;
	left[3] = right[0];
}

static float obsah(const Vector3* pt)
{
	float f = fabs((pt[3] - pt[0]).CrossProduct(pt[1] - pt[0]).Z() * 0.5f) +
			  fabs((pt[0] - pt[3]).CrossProduct(pt[2] - pt[3]).Z() * 0.5f);
    return f;
}

template<class T>
void AppendBezier(CrossRoadObject& owner, ObjectData& object, T& arr, const Vector3* vectors4, int recurse = 0)
{
	recurse++;
    if (obsah(vectors4) < (owner.bezierTeselationFactor * owner._curLodLevel) || recurse >= owner.bezierMaxRecursions)
    {
		AppendLine(object, arr, vectors4[3]);
    }
	else
    {
		Vector3 left[4], right[4];
		delit_krivku(vectors4, left, right);
		AppendBezier(owner, object, arr, left, recurse);
		AppendBezier(owner, object, arr, right, recurse);
    }
}

static void SolveVectorCrossing(Vector3* p)
{
  //R1x * u + A1x = R2x * v + A2x
  //R1y * u + A1y = R2y * v + A2y

  //R1x * u - R2x * v = A2x - A1x
  //R1y * u - R2y * v = A2y - A1y

	Vector3 r1 = p[1] - p[0];
	Vector3 r2 = p[3] - p[2];

	float lx = p[3].X() - p[0].X();
	float ly = p[3].Y() - p[0].Y();

	float d = r1.X() * (-r2.Y()) - (-r2.Y()) * r1.X();
	if (fabs(d) < 0.00001) return;
  
	float du = lx * (-r2.Y()) - (ly)*r1.X();
	float dv = r1.X() * (lx) - (-r2.Y()) * ly;

	float u = du / d;
	float v = dv / d;

	if (u > 0 && u < 1 && v > 0 && v < 1)
	{
		p[1] = p[0] + r1 * u;
		p[2] = p[3] + r2 * v;
	}
}

static void GenerateMapping(ObjectData& object)
{
  float mappingFactor = 8.0f;
	Selection sel(&object);
	sel.SelectAll();
	std::pair<VecT, VecT> bbox = sel.GetBoundingBox();
	for (int i = 0; i < object.NFaces(); i++)
	{
		FaceT fc(object, i);
		for (int j = 0; j < fc.N(); j++)
		{
			const VecT& pos = fc.GetPointVector<VecT>(j);
			fc.SetUV(j,(pos[0] - bbox.first[0]) / (bbox.second[0] - bbox.first[0])*mappingFactor,
					 (pos[2] - bbox.first[2]) / (bbox.second[2] - bbox.first[2])*mappingFactor);
		}
	}
}

void GenerateFakeMapping(const Array<int>& begend, ObjectData& obj)
{
	AutoArray<ObjUVSet_Item> tmp;

	for (int i = 0; i < begend.Size(); i+= 2)
	{
		int bg = begend[i];
		int en = begend[i + 1];

		int cnt = (en - bg);
		int halfcnt = (cnt + 1) / 2;

		tmp.Resize(cnt);
    
		float pos1 = 0.0f, pos2 = 0.0f;
//		float left = 0.0f;
//		float right = 0.0f;
		for (int j = 0; j < halfcnt; j++)
		{
			tmp[j].u = 1;
			tmp[j].v = pos1;
			tmp[cnt - j - 1].u = 0;
			tmp[cnt - j - 1].v = pos2;

			pos1 += obj.Point(bg + j + 1).Distance(obj.Point(bg + j)) / 8.0f;
			pos2 += obj.Point(en - j - 2).Distance(obj.Point(en - j - 1)) / 8.0f;
		}
       
		for (int j = 0; j < obj.NFaces(); j++)
		{
			FaceT fc(obj, j);
			for (int k = 0; k < fc.N(); k++)
			{
				int pti = fc.GetPoint(k);
				if (pti >= bg && pti < en)
				{
					fc.SetUV(k, tmp[pti - bg].u, tmp[pti - bg].v);
				}
			}
		}

		tmp.Clear();
	}

	CEdges graph(obj.NFaces());
	CEdges nocross(obj.NFaces());

	obj.BuildFaceGraph(graph, nocross);

	bool rep = true;
	while (rep)
	{
		rep = false;
		for (int i = 0; i < obj.NFaces(); i++)
		{
			FaceT fc(obj, i);
		//	for (int j = 0; i < o(); i++)
		}
	}
}

//-----------------------------------------------------------------------------

void CrossRoadObject::GenerateCrossRoad( ObjectData& object, int level )
{
	_curLodLevel = __max(level, 1);
	AutoArray< int > points;
	AutoArray< int > beginend;
  
//  AppendLine(object, points, _roads[_roads.Size() - 1]._p2);

	for ( int i = 0, k = _roads.Size() - 1; i < _roads.Size(); k = i, ++i )
	{
		const RoadEndDesc& a1 = _roads[k];
		const RoadEndDesc& a2 = _roads[i];
    
		Vector3 p[4];
		p[0] = a1._p2;
		p[1] = a1._p2 + a1._d2 * a1._dmult;
		p[2] = a2._p1 + a2._d1 * a2._dmult;
		p[3] = a2._p1;

		SolveVectorCrossing( p );

		beginend.Add( points.Size() );

		AppendLine( object, points, a1._p2 );
    
		AppendBezier( *this, object, points, p );

		beginend.Add( points.Size() );
	}

	ObjToolTopology& topology = object.GetTool< ObjToolTopology >();

	topology.TessellatePolygon( points.Size(), points.Data() );
	topology.MergePoints( 0.0001, false, 0 );
	topology.Squarize();
	GenerateMapping( object );

	if ( _textureSize ) 
	{
		GenerateTextureBitmap( object, level );
	}
	else 
	{
		GenerateFakeMapping( beginend, object );
	}
	object.RecalcNormals();
}

//-----------------------------------------------------------------------------

void CrossRoadObject::GenerateMemory(ObjectData& object)
{
	Vector3 center(0, 0, 0);
	for (int i = 0; i < _roads.Size(); i++)
	{
		Vector3 pt = (_roads[i]._p1 + _roads[i]._p2) * 0.5f;
		PosT* pos = object.NewPoint();
		pos->SetPoint(Vector3(pt[0], 0, pt[1]));
		center += pt;
	}

	center /= (float)(_roads.Size());
	PosT* pos = object.NewPoint();
	pos->SetPoint(Vector3(center[0], 0, center[1]));

	for (int i = 0; i < object.NPoints(); i++)
	{
		char x[50];
		sprintf(x, "crossroad_%d", i);
		Selection sel(&object);
		sel.PointSelect(i);
		object.SaveNamedSel(x, &sel);
	}
}

void CrossRoadObject::GenerateCrossRoad(LODObject& object,int levels)
{
	for (int i = 0; i < levels; i++)
	{
		ObjectData* obj = new ObjectData;
		GenerateCrossRoad(*obj, (int)pow(5.0, i));
		obj->SetNamedProp("autocenter", "0");
		obj->SetNamedProp("class", "road");
		obj->SetNamedProp("map", "road");
		object.InsertLevel(obj, (float)i);
	}
	object.DeleteLevel(0);

	ObjectData* obj = new ObjectData;
	GenerateCrossRoad(*obj, 1 << (levels - 1));
	object.InsertLevel(obj, LOD_ROADWAY);
  
	obj = new ObjectData;
	GenerateMemory(*obj);
	object.InsertLevel(obj, LOD_MEMORY);
}

struct BITMAPINFOEX : public BITMAPINFO
{
	RGBQUAD buff[256];
};

void CrossRoadObject::GenerateTextureBitmap(ObjectData& object, int level)
{
	int finalSize = _textureSize;
	bool inittex = false;

	HDC display = ::CreateDC("DISPLAY", 0, 0, 0);
	if (_texDC == 0) _texDC = CreateCompatibleDC(display);
	HDC dc = _texDC;
	if (_texMask == 0) 
	{
		_texMask = CreateCompatibleBitmap(display, finalSize, finalSize);
		inittex = true;
	}
	HBITMAP bmp = _texMask;
	DeleteDC(display);

	HBITMAP oldbmp = (HBITMAP)SelectObject(dc, bmp);
	SelectObject(dc, GetStockObject(WHITE_BRUSH));
  
	RECT whole;
	whole.left = whole.top = 0;
	whole.right = whole.bottom = finalSize;
	if (inittex) FillRect(dc, &whole, (HBRUSH)GetStockObject(WHITE_BRUSH));

	SelectObject(dc, GetStockObject(DC_BRUSH));
	SetDCBrushColor(dc, 0x7f7f7f);
	SelectObject(dc, GetStockObject(BLACK_PEN));
	SetROP2(dc, R2_MASKPEN);

	for (int i = 0; i < object.NFaces(); i++)
	{
		FaceT fc(object, i);
		POINT pt[4];
		for (int i = 0; i < fc.N(); i++)
		{
			const ObjUVSet_Item uv = fc.GetUV(i);
			float u = uv.u * finalSize;
			float v = uv.v * finalSize;
			pt[i].x = toInt(floor(u - 0.5f));
			pt[i].y = toInt(floor(v - 0.5f));
		}
		Polygon(dc, pt, fc.N());
		fc.SetTexture(_textureName);
	}
  
	SelectObject(dc, oldbmp);
}

void CrossRoadObject::SetTextureParams(RString textureName, RString texturePathname, int textureSize)
{
	_textureName   = textureName;
	_textureFolder = texturePathname;
	_textureSize   = textureSize;
}


CrossRoadObject::~CrossRoadObject()
{
	if (_texMask) DeleteObject(_texMask);
	if (_texDC) DeleteObject(_texDC);
}

bool CrossRoadObject::SaveTexture()
{
	if (_texDC == 0) return true;

	HDC dc = _texDC;
	HBITMAP bmp =  _texMask;
	BITMAPINFOEX binfo;

	memset(&binfo, 0, sizeof(binfo));
	binfo.bmiHeader.biSize = sizeof(binfo.bmiHeader);
  
	GetDIBits(dc, bmp, 0, 1, 0, &binfo, DIB_RGB_COLORS);
	binfo.bmiHeader.biBitCount = 32;

	LONG bitsize = binfo.bmiHeader.biWidth * binfo.bmiHeader.biHeight;
	unsigned long* bits = new unsigned long[bitsize];

	GetDIBits(dc, bmp, 0, binfo.bmiHeader.biHeight, bits, &binfo, DIB_RGB_COLORS);

	Picture respic;
	respic.Create(binfo.bmiHeader.biWidth, binfo.bmiHeader.biHeight);

	unsigned long* ln = bits;
	for (int y = binfo.bmiHeader.biHeight -1 ; y >= 0; y--)
	{
		for (int x = 0; x < binfo.bmiHeader.biWidth; x++)
		{
			PPixel px = ln[x];
			px |= 0xFF000000;
			respic.SetPixel(x, y, px);
		}
		ln += binfo.bmiHeader.biWidth;
	}

	delete [] bits;

	return respic.SaveTGA(_textureFolder) == 0; 
}
