#pragma once

struct RoadEndDesc
{
public:
  
	Vector3 _p1;
	Vector3 _p2;
	Vector3 _d1;
	Vector3 _d2;

	float _dmult;
  
	ClassIsMovable(RoadEndDesc);

	RoadEndDesc(void)  
	{
	}

	RoadEndDesc(const Vector3& p1, const Vector3& p2, const Vector3& d1, const Vector3& d2, float dmult)
	: _p1(p1)
	, _p2(p2)
	, _d1(d1)
	, _d2(d2)
	, _dmult(dmult) 
	{
	}  

	void AddToCenterCalc(Vector3& sum, int& count)
	{
		sum = sum + _p1 + _p2;
		count += 2;
	}

	static float GetOrderValue(const Vector3& center, const Vector3& pt)
	{
		Vector3 dir = pt - center;
		return atan2(dir[1], dir[0]);
	}

	static void FixAngleValues(float& f1, float& f2)
	{
		if (fabs(f2 - f1) > 3.2)
		{
			if (f2 > f1) 
			{
				f1 += 2.0f * 3.1415926535f;
			}
			else if (f2 < f1) 
			{
				f2 += 2.0f * 3.1415926535f;
			}
		}
	}

	int Compare(const RoadEndDesc& other, const Vector3& center) const
	{
		float f1 = GetOrderValue(center, _p1);
		float f2 = GetOrderValue(center, _p2);
		FixAngleValues(f1, f2);
		float s1 = GetOrderValue(center, other._p1);
		float s2 = GetOrderValue(center, other._p2);
		FixAngleValues(s1, s2);
		int cmpid = 0;
		cmpid += (f1 > s1) - (f1 < s1);
		cmpid += (f2 > s1) - (f2 < s1);
		cmpid += (f1 > s2) - (f1 < s2);
		cmpid += (f2 > s2) - (f2 < s2);
		return (cmpid > 0) - (cmpid < 0);
	}

	void SortPoints(const Vector3& center)
	{
		float f1 = GetOrderValue(center, _p1);
		float f2 = GetOrderValue(center, _p2);

		FixAngleValues(f1, f2);

		if (f2 < f1) std::swap(_p1, _p2);
	}
};