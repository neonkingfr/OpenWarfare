
// ViewerSim.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "AskForFilename.h"


void LogF(char const *str,...) 
{
    static DWORD initTime = GetTickCount();
    DWORD curTime = GetTickCount();
    va_list args;
    va_start(args,str);
    printf("[%10.3f]: ", (float)(curTime-initTime)/1000.0f);
    vprintf(str,args);
    puts("");
}

class Capture : public IVisViewerExchangeDocView
{
    std::ostream &out;
    bool quitMessage;
    std::string finishSection;
    int mode;
    int itmcnt;

    void FinishSection() {out<<finishSection.c_str();finishSection.clear();mode=0;}
public:
    Capture (std::ostream &out):out(out),quitMessage(false),mode(0) {}

    bool inProgress() const {return !quitMessage;}
        
    void SystemQuit() {FinishSection();LogF("SystemQuit");quitMessage=true;}
    void SelectionObjectClear() {}
    void CursorPositionSet(const SMoveObjectPosMessData &data)  {}
    void ObjectCreate(const SMoveObjectPosMessData &data, const char *name) {
        
        if (mode != 11) {
            FinishSection();            
            out << "\tclass Objects\n"
                   "\t{\n";            
            finishSection = "\t};\n";
            mode = 11;            
            LogF("Building object list...");
        }
        out << "\t\tO"<<data.nID<<"[]={\""<<name<<"\"";
        for (int i = 0;i<3;i++)
            for (int j = 0; j<4;j++)
                out << "," << data.Position[i][j];
        out << "};\n";
   
    }
    void ObjectDestroy(const SMoveObjectPosMessData &data) {}
    void SelectionObjectAdd(const SObjectPosMessData &data) {}
    void SystemInit(const SLandscapePosMessData &data, const char *configName) {
        
        FinishSection();
        out<<"\tclass SystemInit\n"
             "\t{\n"
             "\t\ttextureRange[]={" << data.textureRangeX << "," << data.textureRangeY << "};\n"
             "\t\tlandRange[]={" << data.landRangeX << "," << data.landRangeY << "};\n"
             "\t\tlandGrid[]={" << data.landGridX << "," << data.landGridY << "};\n"
             "\t\tconfigName=\"" << configName << "\";\n"
             "\t};\n";
        LogF("Capturing a header data");
        LogF("Land size: %gm x %gm ", data.landGridX * data.textureRangeX ,
                                      data.landGridY * data.textureRangeY );
        LogF("Land grid: %gm x %gm ", data.landGridX * data.textureRangeX / data.landRangeX,
                                      data.landGridY * data.textureRangeY / data.landRangeY );
        LogF("Texture grid: %gm x %gm", data.landGridX, 
                                        data.landGridY);
        LogF("Land's config: %s", configName);
    }

    void FileImportBegin(const STexturePosMessData& data) {
        LogF("Capturing the land data...");
    }

    void FileImportEnd() {
        FinishSection();
        LogF("Capture is complette");
        quitMessage  = true;
    }

    void LandHeightChange(const STexturePosMessData& data) {}
    void LandTextureChange(const STexturePosMessData& data) {}
    void RegisterObjectType(const char *name) {
        if (mode != 10) {
            FinishSection();            
            out << "\tObjectTypes[]={\n\t\t\"" << name << "\"";
            finishSection = "\n\t};\n";
            mode = 10;            
            LogF("Capturing an object type list...");
        } else
            out << ",\n\t\t\""<<name<<"\"";    
    }
    void RegisterLandscapeTexture(const SMoveObjectPosMessData &data, const char *name) {
        if (mode != 1) {
            FinishSection();            
            out << "\tclass Textures\n"
                   "\t{\n";            
            finishSection = "\t};\n";
            mode = 1;            
            LogF("Capturing a texture list...");
        }
        out << "\t\tT"<<data.nID<<"=\""<<name<<"\";\n";
    }

    void SelectionLandAdd(const SLandSelPosMessData& data) {}
    void SelectionLandClear() {}
    void BlockMove(const Array<SMoveObjectPosMessData> &data) {}
    void BlockSelectionObject(const Array<SMoveObjectPosMessData> &data) {}
    void BlockSelectionLand(const Array<SLandSelPosMessData> &data) {}
    void BlockLandHeightChange(const Array<STexturePosMessData> &data) {}
    void BlockLandHeightChangeInit(const Array<float> &heights) {
        if (heights.Size() > 0) {
            int beg = 0;
            if (mode != 2) {
                FinishSection();
                out << "\theightMap[]={"<<heights[0];
                finishSection="};\n";
                mode = 2;
                beg = 1;
                itmcnt = 0;
                LogF("Capturing a terrain...");
            }
            for (int i = beg; i < heights.Size(); i++) {
                out << ",";
                if (itmcnt++ > 20) {out << "\n\t\t"; itmcnt = 0;}
                out << heights[i];
            }
        }
    }

    void BlockLandTextureChangeInit(const Array<int> &ids) {
        if (ids.Size() > 0) {
            int beg = 0;
            if (mode != 3) {
                FinishSection();
                out << "\ttextureMap[]={"<<ids[0];
                finishSection="};\n";
                mode = 3;
                beg = 1;
                itmcnt = 0;
                LogF("Capturing a texture grid...");
            }
            for (int i = beg; i < ids.Size(); i++) {
                out << ",";
                if (itmcnt++ > 20) {out << "\n\t\t"; itmcnt = 0;}
                out << ids[i];
            }
        }
    }
    void BlockWaterHeightChangeInit(const Array<float> &heights) {}
    void ComVersion(int version) {}
    void ObjectMove(const SMoveObjectPosMessData& data) {}
    void FileImport(const char * data) {}
    void FileExport(const char * data) {}
    int AddRef() const {return 1;}
    int Release() const {return 0;}

};

void Message(int type, const char *format, ...) {

    va_list list;
    va_start(list,format);
    _vfprintf_p(stderr,format,list);
    fputs("\n",stderr);

    char buff[1024];
    _vsprintf_p(buff,1024,format,list);
    MessageBox(0,buff,"CaptureLandInfo",type);
}


static int runCapture(const char *name, const char *connectStr);
int _tmain(int argc, _TCHAR* argv[])
{
    if (argc == 2 && _strnicmp(argv[1],"-connect=pipe\\",14) == 0) {
        
        char buff[MAX_PATH];
        if (AskFilename(buff,MAX_PATH)==false) return -1;
        return runCapture(buff,argv[1]);
    } else if (argc == 3 && _strnicmp(argv[2],"-connect=pipe\\",14) == 0) {
    
        return runCapture(argv[1],argv[2]);
    } else {
        Message(0,"Error in parameters: \r\n\r\nUsage: \r\n\r\nCaptureLandInfo <connectString>"
            "or\r\nCaptureLandInfo <target><connectString>");
        return -1;
    }

    return 0;
}

static int runCapture(const char *name, const char *connectStr) {
    
    std::ofstream output(name,std::ios::out);
    if (!output) {
        Message(0,"Error: Cannot open %s for writting", name);
        Sleep(2000);
        return -2;
    }


    output << "class LandInfo\n"
              "{\n";             

    Capture cap(output);

    VisViewerPipe pipe(cap);
    bool res = pipe.Create(connectStr+14);

    if (res == false) {
        Message(0,"Pipe creation failed");
        Sleep(2000);        
        return -3;
    }

    LogF("Connection established");

    pipe.ComVersion(MSG_VERSION);
    
    while (cap.inProgress()) {
        pipe.CheckInterfaceStatus();
        Sleep(100);
    }

    if (!output) {
        Message(0,"Error: Write error in file %s", name);
        return -2;
    }
    
    output << "};";

    Message(0,"Data has been captured, press OK to exit");

	return 0;
}

