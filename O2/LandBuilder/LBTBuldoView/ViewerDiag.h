#pragma once
#include "ivisviewerexchange.h"

void LogF(char const *format,...);

class ViewerDiag : public IVisViewerExchangeDocView
{
public:
    bool quitMessage;
    ViewerDiag():quitMessage(false) {}

    void SystemQuit() {LogF("SystemQuit");quitMessage=true;}
    void SelectionObjectClear() {LogF("SelectionObjectClear");}
    void CursorPositionSet(const SMoveObjectPosMessData &data) 
        {LogF("SetCursor: %g %g %g",data.Position[0][3],data.Position[1][3],data.Position[2][3]);}
    void ObjectCreate(const SMoveObjectPosMessData &data, const char *name)
        {LogF("ObjectCreate %d:%s,\n\t%+8.4f%+8.4f%+8.4f%+8.4f%\n\t%+8.4f%+8.4f%+8.4f%+8.4f%\n\t%+8.4f%+8.4f%+8.4f%+8.4f%",
        data.nID,name,data.Position[0][0],data.Position[0][1],data.Position[0][2],data.Position[0][3],
                      data.Position[1][0],data.Position[1][1],data.Position[1][2],data.Position[1][3],
                      data.Position[2][0],data.Position[2][1],data.Position[2][2],data.Position[2][3]);
    }
    void ObjectDestroy(const SMoveObjectPosMessData &data)
        {LogF("ObjectCreate: ID %d",data.nID);}
    void SelectionObjectAdd(const SObjectPosMessData &data)
        {LogF("SelectionObjectAdd: ID %d (%s)",data.nID,data.bState?"Add":"Del");}
    void SystemInit(const SLandscapePosMessData &data, const char *configName)
    {LogF("SystemInit - tex(%d,%d),land(%d,%d),grid(%g,%g), cfg:%s", 
        data.textureRangeX,data.textureRangeY,
        data.landRangeX,data.landRangeY,data.landGridX,data.landGridY,configName);}
    void FileImportBegin(const STexturePosMessData& data)
        {LogF("FileImportBegin: textures %d objects %d instances %d", 
            data.nTextureID,data.nX,data.nZ);}
    void FileImportEnd()
        {LogF("FileImportEnd");}
    void FileImport(const char * data)
        {LogF("FileImport %s", data);}
    void FileExport(const char * data)
        {LogF("FileExport %s", data);}
    void LandHeightChange(const STexturePosMessData& data)
        {LogF("LandHeightChange pos(%d,%d) height %g", data.nX,data.nZ,data.Y);}
    void LandTextureChange(const STexturePosMessData& data)
        {LogF("LandTextureChange pos(%d,%d) tex %d", data.nX,data.nZ,data.nTextureID);}
    void RegisterObjectType(const char *name)
        {LogF("RegisterObject %s", name);}
    void RegisterLandscapeTexture(const SMoveObjectPosMessData &data, const char *name)
        {LogF("RegisterTexture %d:%s", data.nID,name);}
    void SelectionLandAdd(const SLandSelPosMessData& data)
        {LogF("SelectionLandAdd (%d,%d,%d,%d)", data.xs,data.zs,data.xe,data.ze);}
    void SelectionLandClear()
        {LogF("SelectionLandClear");}
    void BlockMove(const Array<SMoveObjectPosMessData> &data)
        {LogF("BlockMove (%d objects)",data.Size());}
    void BlockSelectionObject(const Array<SMoveObjectPosMessData> &data)
        {LogF("BlockSelectionObject (%d objects)",data.Size());}
    void BlockSelectionLand(const Array<SLandSelPosMessData> &data)
        {LogF("BlockSelectionLand (%d objects)",data.Size());}
    void BlockLandHeightChange(const Array<STexturePosMessData> &data)
        {LogF("BlockLandHeightChange (%d)",data.Size());}
    void BlockLandHeightChangeInit(const Array<float> &heights)
        {LogF("BlockLandHeightChangeInit (%d)",heights.Size());
        for (int i = 0; i<heights.Size(); i++) printf("%+8.2f",heights[i]);
        }
    void BlockLandTextureChangeInit(const Array<int> &ids)
        {LogF("BlockLandTextureChangeInit (%d)",ids.Size());
        for (int i = 0; i<ids.Size(); i++) printf("%d ",ids[i]);
        }
    void BlockWaterHeightChangeInit(const Array<float> &heights)
        {LogF("BlockWaterHeightChangeInit (%d)",heights.Size());}
    void ComVersion(int version)
        {LogF("Viewer Com Version %d",version);}
    void ObjectMove(const SMoveObjectPosMessData& data)
        {LogF("Moving object (%d)",data.nID);}


    int AddRef() const {return 1;}
    int Release() const {return 1;}
};
