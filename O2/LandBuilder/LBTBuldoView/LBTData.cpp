#include <iostream>
#include <memory>
#include <utility>
#include <el/ProgressBar/progressBar.h>
#include <windows.h>
#include <Es/containers/array.hpp>
#include <el/paramfile/paramfile.hpp>
#include "messagedll.h"
#include <es/types/pointers.hpp>
#include "IVisViewerExchange.h"
#include <set>
#include <string.h>
#include <es/common/fltopts.hpp>

#include "LBTData.h"


float LBTerrain::CalculateHeight(float x, float y) {
    
    if (x < 0) x = 0;
    if (y < 0) y = 0;
    if (x >= width) x = width - 0.0001f;
    if (y >= height) y = height - 0.0001f;
    float fix = floor(x);
    float fiy = floor(y);
    float frx = x - fix;
    float fry = y - fiy;
    int ix = toLargeInt(fix);
    int iy = toLargeInt(fiy);

    int ix1 = ix +1;
    int iy1 = iy +1;
    if (ix1 >= width) ix1 = width - 1;
    if (iy1 >= height) iy1 = height - 1;

    if (frx + fry < 1.0f) {
        
        float f = frx + fry;
        if (f < 0.0000001f)
            return (*this)[iy][ix];
        float p = fry / f;
        float q = frx / f;
        float px = (*this)[iy][ix] + ((*this)[iy][ix1] - (*this)[iy][ix]) * f;
        float py = (*this)[iy][ix] + ((*this)[iy1][ix] - (*this)[iy][ix]) * f;
        return px * q + py * p;

    } else {

        frx = 1 - frx;
        fry = 1 - fry;
        float f = frx + fry;
        if (f < 0.0000001f)
            return (*this)[iy1][ix1];
        float p = fry / f;
        float q = frx / f;
        float px = (*this)[iy1][ix1] + ((*this)[iy][ix1] - (*this)[iy1][ix1]) * f;
        float py = (*this)[iy1][ix1] + ((*this)[iy1][ix] - (*this)[iy1][ix1]) * f;
        return px * q + py * p;
    }


}

LBTData::LBTData(ParamEntryPtr textureMap,ParamClassPtr textureList,const SLandscapePosMessData &landscape,ParamEntryPtr terrain)
                 :terrain(landscape.landRangeX,landscape.landRangeY),
                 textureMap(textureMap),
                 textureList(textureList),
                 landscape(landscape)
{
    int idx = 0;
    for(int nX =0; nX < this->terrain.width; nX++)
    {
      for(int nZ =0; nZ <this->terrain.height; nZ++)
      {        
        this->terrain[nZ][nX] = terrain->operator [](idx++).GetFloat();
      };
    }
}

LBTData::~LBTData(void)
{
}


void LBTData::Load(std::istream &infile)
{
  char strbuff[2000];
  int strbufflen=sizeof(strbuff);  


  infile.seekg(0,std::ios::end);
  ProgressBar<unsigned long> pb(infile.tellg());
  infile.seekg(0);
  

  infile.getline(strbuff,strbufflen,'\n');
  if (_stricmp(strbuff,"heightmap")==0)
  {
    int left=0,top=0,right=0,bottom=0;
    float stepx=0, stepy=0;
    infile>>left>>top>>right>>bottom>>stepx>>stepy;
    
    if (!infile)
        throw std::string("Parse error in heightmap");
    

    for (int y=top;y<=bottom;y++)
      for (int x=left;x<=right;x++)
      {
        float val=0;
        infile>>val;
        if (x>=0  && y>=0 && x < terrain.width && y < terrain.height)
          terrain[y][x] = val;
      }      
    ws(infile);
    infile.getline(strbuff,strbufflen,'\n');
    if (_stricmp(strbuff,"end heightmap"))
        throw std::string ("Excepting 'end heightmap' - format corrupted");

    infile.getline(strbuff,strbufflen,'\n');
  }

  if (_stricmp(strbuff,"objects")==0)
  {
    do 
    {          
      float a11,a12,a13,a21,a22,a23,a31,a32,a33,a41,a42,a43;
      int tag;
      infile>>a11>>a12>>a13>>a21>>a22>>a23>>a31>>a32>>a33>>a41>>a42>>a43>>tag;
      if (!infile) infile.clear();
      ws(infile);
      infile.getline(strbuff,strbufflen,'\n');
      _strlwr(strbuff);
      if (!infile) 
        throw std::string ("Input stream error");
      if (_strnicmp(strbuff,"end objects",11)==0) break;
        
      LBTObject lbobj;
      lbobj.mx[0][0] = a11;
      lbobj.mx[0][1] = a12;
      lbobj.mx[0][2] = a13;
      lbobj.mx[0][3] = 0;
      lbobj.mx[1][0] = a21;
      lbobj.mx[1][1] = a22;
      lbobj.mx[1][2] = a23;
      lbobj.mx[1][3] = 0;
      lbobj.mx[2][0] = a31;
      lbobj.mx[2][1] = a32;
      lbobj.mx[2][2] = a33;
      lbobj.mx[2][3] = 0;
      lbobj.mx[3][0] = a41;
      lbobj.mx[3][1] = a42;
      lbobj.mx[3][2] = a43;
      lbobj.mx[3][3] = 1;
      lbobj.name = strbuff;
      objects.push_back(lbobj);
    } while (true);

  }

}


void LBTData::SendToViewer(IVisViewerExchange *ifc) {

    STexturePosMessData msg;
    msg.nTextureID = textureList->GetEntryCount();
    msg.nX = GetCountOfRegisteredObjects();
    msg.nZ = objects.size();
    ifc->FileImportBegin(msg);

    ifc->StartBuffer(10*1024*1024);

    RegisterExplicitBitmaps(ifc);
    SendHeightMap(ifc);
    SendExplicitBitmaps(ifc);
    RegisterObjects(ifc);
    SendObjects(ifc);
    ifc->EndBuffer();
    ifc->FileImportEnd();
}

namespace Functors { class SendTextures 
{
    mutable IVisViewerExchange *ifc;
public:
    SendTextures(IVisViewerExchange *ifc):ifc(ifc) {}
    bool operator()(const ParamEntryVal &entry) const {
        if (entry.GetName()[0] == 'T') {
            int id = atoi(entry.GetName().Data()+1);
            RString value = entry.GetValue();
            ifc->RegisterLandscapeTexture(SMoveObjectPosMessData(id),value);
        }
        return false;
    }
};}

void LBTData::RegisterExplicitBitmaps(IVisViewerExchange *ifc) {

    if (textureList.IsNull()) {
        LogF("Note: No textures found, skipping...");
    } else {        
        textureList->ForEachEntry(Functors::SendTextures(ifc));        
    }
}

void LBTData::SendHeightMap(IVisViewerExchange *ifc) {

    AutoArray<float> msgData;

    for(int nX =0; nX <terrain.width; nX++)
    {
      for(int nZ =0; nZ <terrain.height; nZ++)
      {        
        float nHeight = terrain[nZ][nX];
        msgData.Append()=nHeight;
      };
    }
    ifc->BlockLandHeightChangeInit(msgData);    
}

void LBTData::SendExplicitBitmaps(IVisViewerExchange *ifc) {
 
    if (textureList.IsNull() || textureMap.IsNull()) {
        LogF("Note: No texture map or list found, skipping ...");
    } else {
        int sz = landscape.textureRangeX * landscape.textureRangeY;

        AutoArray<int> ids;
        ids.Reserve(sz);

        for (int i =  0; i< sz; i++) 
            ids.Add(textureMap->operator[](i).GetInt());
        
        ifc->BlockLandTextureChangeInit(ids);
    }
}

template<class _Ty>
	struct strless
        : public std::binary_function<_Ty, _Ty, bool>
	{	// functor for operator<
	bool operator()(const _Ty& _Left, const _Ty& _Right) const
		{	// apply operator< to operands
		return (strcmp(_Left,_Right)<0);
		}
	};


void LBTData::RegisterObjects(IVisViewerExchange *ifc) {

    std::set<const char *, strless<const char *> > registered;

    for (LBTObjectList::const_iterator it = objects.begin(); it != objects.end(); ++it) {
    
        std::set<const char *, strless<const char *> >::iterator found = registered.find(it->name.c_str());
        if (found == registered.end()) {

            ifc->RegisterObjectType(it->name.c_str());
            registered.insert(it->name.c_str());
        }
    }    
}


void LBTData::SendObjects(IVisViewerExchange *ifc) {

    float gridX = (landscape.textureRangeX * landscape.landGridX) / landscape.landRangeX;
    float gridY = (landscape.textureRangeY * landscape.landGridY) / landscape.landRangeY;
    int index = 0;
    for (LBTObjectList::const_iterator it = objects.begin(); it != objects.end(); ++it) {
    
        SMoveObjectPosMessData data;
        data.nID = index++;
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 4;j ++) 
                data.Position[i][j] = it->mx[j][i];

        data.Position[1][3] += terrain.CalculateHeight(data.Position[0][3]/gridX,data.Position[2][3]/gridY);
 
        ifc->ObjectCreate(data,it->name.c_str());
    }    

}

int LBTData::GetCountOfRegisteredObjects() {
    std::set<const char *, strless<const char *> > registered;    

    for (LBTObjectList::const_iterator it = objects.begin(); it != objects.end(); ++it) {    
        std::set<const char *, strless<const char *> >::iterator found = registered.find(it->name.c_str());
        if (found == registered.end()) 
            registered.insert(it->name.c_str());
        
    }    
    return registered.size();

}