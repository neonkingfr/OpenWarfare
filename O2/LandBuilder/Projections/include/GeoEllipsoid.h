#ifndef GEOELLIPSOID_H
#define GEOELLIPSOID_H

#include <string>

#include ".\Ellipsoid.h"

using std::string;

// ***************************************** //
// Predefined geo ellipsoid codes enumerator //
// ***************************************** //
typedef enum 
{
	GeoEllipsoidCode_UNDEFINED      =     0,
	GeoEllipsoidCode_USERDEFINED    = 32767, // reserved but not used
	GeoEllipsoidCode_NOTIMPLEMENTED = 65534,
	GeoEllipsoidCode_INVALIDVALUE   = 65535, // reserved but not used
 // ---------------------------------------- //
	GeoEllipsoidCode_WGS84          =  7030
} PredefinedGeoEllipsoidCodes;

template <class Real>
class GeoEllipsoid : public Ellipsoid<Real>
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// -------- //
	// the name //
	// -------- //
	string m_Name;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ------------ //
	// Constructors //
	// ------------ //

	// Default
	GeoEllipsoid();

	// Constructs this ellipsoid with the given parameters
	GeoEllipsoid(string name, Real semiMajorAxis, Real semiMinorAxis);

	// Constructs this ellipsoid getting the parameters from the given predefined ellipsoid code
	GeoEllipsoid(PredefinedGeoEllipsoidCodes code);

	// Copy constructor
	GeoEllipsoid(const GeoEllipsoid<Real>& other);

	// ----------- //
	// Assignement //
	// ----------- //
	GeoEllipsoid<Real>& operator = (const GeoEllipsoid<Real>& other);

	// ------------------------ //
	// Member variables getters //
	// ------------------------ //

	// GetName()
	// Returns the name of this ellipsoid.
	const string& GetName() const;

	// --------- //
	// Interface //
	// --------- //
public:
	// SetPredefined()
	// Sets this ellipsoid getting the parameters from the given predefined ellipsoid code
	void SetPredefined(PredefinedGeoEllipsoidCodes code);
};

#include "..\src\GeoEllipsoid.inl"

#endif