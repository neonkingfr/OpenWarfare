#ifndef PROJTRANSFORMATION_H
#define PROJTRANSFORMATION_H

#include <vector>

#include ".\GeoEllipsoid.h"

using std::vector;

// ***************************************** //
// Predefined geo ellipsoid codes enumerator //
// ***************************************** //
typedef enum 
{
	ProjTransformationCode_UNDEFINED      =     0,
	ProjTransformationCode_USERDEFINED    = 32767, // reserved but not used
	ProjTransformationCode_NOTIMPLEMENTED = 65534,
	ProjTransformationCode_INVALIDVALUE   = 65535, // reserved but not used
 // ---------------------------------------------- //
	ProjTransformationCode_WGS84TOUTM     =  1001
} PredefinedProjTransformationCodes;

// **************************** //
// Transformation return values //
// **************************** //
typedef enum 
{
	PjTrRet_SUCCESS              = 0,
	PjTrRet_INVALID_ELLIPSOID    = 1,
	PjTrRet_INVALID_PARAM_NUMBER = 2,
	PjTrRet_OUTOFBOUND_LONGITUDE = 3,
	PjTrRet_OUTOFBOUND_LATITUDE  = 4

} ProjTransformationReturnValues;

// ************************** //
// Transformation return data //
// ************************** //

template <class Real>
class ProjTransformationReturnData
{
public:
	Real easting;
	Real northing;
	char utmLatZone;
	Real utmLonZone;
};

template <class Real>
class ProjTransformation
{
	// ************************************************************** //
	// Member variables                                               //
	// ************************************************************** //
protected:
	// -------- //
	// the code //
	// -------- //
	PredefinedProjTransformationCodes m_Code;

	// ------------- //
	// the ellipsoid //
	// ------------- //
	GeoEllipsoid<Real> m_Ellipsoid;

	// ----------------------- //
	// the parameters in input //
	// ----------------------- //
	vector<Real> m_ParametersIn;

	// ************************************************************** //
	// Member functions                                               //
	// ************************************************************** //
public:
	// ------------ //
	// Constructors //
	// ------------ //

	// Default
	ProjTransformation();

	// Constructs this transformation with the given parameters
	ProjTransformation(PredefinedProjTransformationCodes code);

	// Copy constructor
	ProjTransformation(const ProjTransformation<Real>& other);

	// ----------- //
	// Assignement //
	// ----------- //
	ProjTransformation<Real>& operator = (const ProjTransformation<Real>& other);

	// ------------------------ //
	// Member variables getters //
	// ------------------------ //

	// GetEllipsoid()
	// Returns the Ellipsoid of this tranformation.
	const GeoEllipsoid<Real>& GetEllipsoid() const;

	// ------------------------ //
	// Transformation functions //
	// ------------------------ //
public:
	// Wgs84ToUtm()
	// Transforms the point having the given WGS84 coordinates (latitude, longitude in radians) 
	// to the corrispondent point projected into the UTM coordinates.
	// On return, returnData will contain:
	// [0] = easting in meters
	// [1] = northing in meters
	// [2] = utm zone
	// Returns PjTrRet_SUCCESS if successfull, another value if any error occours;
	ProjTransformationReturnValues Wgs84ToUtm(Real latitude, Real longitude, ProjTransformationReturnData<Real>& returnData);

	// --------- //
	// Interface //
	// --------- //
public:
	// TransformDirect()
	// Performs the direct transformation, from geographic to projected coordinate, on the point
	// whose geographical coordinates are given.
	// On return, returnData will contain different data depending on the type of transformation
	// applied.
	// Returns PjTrRet_SUCCESS if successfull, another value if any error occours;
	ProjTransformationReturnValues TransformDirect(Real latitude, Real longitude, ProjTransformationReturnData<Real>& returnData);

	// ---------------- //
	// Helper functions //
	// ---------------- //
private:
	// SetWgs84ToUtmParametersIn()
	// Sets the input parameters needed to perform the direct transformation from WGS84 to UTM.
	bool SetWgs84ToUtmParametersIn();
};

#include "..\src\ProjTransformation.inl"

#endif