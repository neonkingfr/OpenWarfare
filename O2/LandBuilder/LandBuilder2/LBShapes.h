//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "../Shapes/IShape.h"
#include "../Shapes/IShapeFactory.h"

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

	class LBShapes : public Shapes::IShapeFactory
	{
	public:
		virtual Shapes::IShape* CreatePoint( const Shapes::ShapePoint& origShape );
		virtual Shapes::IShape* CreateMultipoint( const Shapes::ShapeMultiPoint& origShape );
		virtual Shapes::IShape* CreatePolyLine( const Shapes::ShapePoly& origShape );
		virtual Shapes::IShape* CreatePolygon( const Shapes::ShapePoly& origShape );
	};

//-----------------------------------------------------------------------------

} // namespace LandBuilder2