//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "LBObjectMap.h"
#include "LBRoads.h"
#include "HighMapBase.h"

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

	using LandBuildInt::HeightMapBase;
	using LandBuildInt::DoubleRect;

//-----------------------------------------------------------------------------

	class LBWorkspace
	{
	public:
		HeightMapBase m_highMap;
		LBObjectMap   m_objects;
		LBRoadMap     m_roads;
		double        m_ofpMapSegmentSizeX;
		double        m_ofpMapSegmentSizeY;
    int           m_visitorVersion;

		LBWorkspace( const DoubleRect& mapArea, double mapSegX, double mapSegY, int visitorVersion ) 
		: m_objects( mapArea )
		, m_roads( mapArea )
		, m_ofpMapSegmentSizeX( mapSegX )
		, m_ofpMapSegmentSizeY( mapSegY )
    , m_visitorVersion( visitorVersion )
		{
			m_highMap.SetMapArea( mapArea );
		}

		bool ExportHeightMap( std::ostream& out ) const;    
		bool SaveResult( const char* target ) const;
		bool SaveResult( std::ostream& out ) const;
	};

//-----------------------------------------------------------------------------

} // namespace LandBuilder2