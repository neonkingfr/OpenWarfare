//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "LBShapes.h"

#include "IShapeExtra.h"
#include "../Shapes/ShapePoint.h"
#include "../Shapes/ShapeMultiPoint.h"
#include "../Shapes/ShapePolygon.h"
#include "../Shapes/ShapePolyLine.h"

//-----------------------------------------------------------------------------

template< class T >
class MyShapeTemplateClass : public T, public IShapeExtra
{
public:
	MyShapeTemplateClass( const T& other ) 
  : T( other ) 
	{
	}

	virtual void* GetInterfacePtr( size_t IFCUID )
	{
		if ( IFCUID == IShapeExtra::IFCUID ) 
		{
			return (static_cast< IShapeExtra* >( this ));
		}
		else 
		{
			return (T::GetInterfacePtr( IFCUID ));
		}
	}
	
	virtual const char* GetShapeTypeName() const;
	virtual Shapes::DVector NearestEdge( const Shapes::DVector& vx ) const;

	virtual MyShapeTemplateClass< T >* NewInstance( const T& other ) const 
	{
		return (new MyShapeTemplateClass< T >( other ));
	}
};

//-----------------------------------------------------------------------------

template < class T >
const char* MyShapeTemplateClass< T >::GetShapeTypeName() const 
{
	return (NULL);
}

//-----------------------------------------------------------------------------

template <>
const char* MyShapeTemplateClass< Shapes::ShapePoint >::GetShapeTypeName() const 
{
	return (SHAPE_NAME_POINT);
}

//-----------------------------------------------------------------------------

template <>
const char* MyShapeTemplateClass< Shapes::ShapeMultiPoint >::GetShapeTypeName() const 
{
	return (SHAPE_NAME_MULTIPOINT);
}

//-----------------------------------------------------------------------------

template <>
const char* MyShapeTemplateClass< Shapes::ShapePolyLine >::GetShapeTypeName() const 
{
	return (SHAPE_NAME_POLYLINE);
}

//-----------------------------------------------------------------------------

template <>
const char* MyShapeTemplateClass< Shapes::ShapePolygon >::GetShapeTypeName() const 
{
	return (SHAPE_NAME_POLYGON);
}

//-----------------------------------------------------------------------------

template < class T >
Shapes::DVector MyShapeTemplateClass<T>::NearestEdge( const Shapes::DVector& vx ) const 
{
	return (T::NearestInside( vx ));
}

//-----------------------------------------------------------------------------

template <>
Shapes::DVector MyShapeTemplateClass< Shapes::ShapePolygon >::NearestEdge( const Shapes::DVector& vx ) const 
{
	return (Shapes::ShapePolygon::NearestToEdge( vx, true ));
}

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

	Shapes::IShape* LBShapes::CreatePoint( const Shapes::ShapePoint& origShape )
	{
		return (new MyShapeTemplateClass< Shapes::ShapePoint >( origShape ));
	}

//-----------------------------------------------------------------------------

	Shapes::IShape* LBShapes::CreateMultipoint( const Shapes::ShapeMultiPoint& origShape )
	{
		return (new MyShapeTemplateClass< Shapes::ShapeMultiPoint >( origShape ));
	}

//-----------------------------------------------------------------------------

	Shapes::IShape* LBShapes::CreatePolyLine( const Shapes::ShapePoly& origShape )
	{
		return (new MyShapeTemplateClass< Shapes::ShapePolyLine >( origShape ));
	}

//-----------------------------------------------------------------------------

	Shapes::IShape* LBShapes::CreatePolygon( const Shapes::ShapePoly& origShape )
	{
		return (new MyShapeTemplateClass< Shapes::ShapePolygon >( origShape ));
	}

//-----------------------------------------------------------------------------

} // namespace LandBuilder2