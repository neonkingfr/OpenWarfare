//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <string>
#include <vector>
#include <algorithm>

#include ".\ILandBuildCmds.h"
#include ".\IBuildModule.h"
#include "..\HighMapLoaders\include\EtRectElevationGrid.h"

//-----------------------------------------------------------------------------

using std::string;
using std::vector;

//-----------------------------------------------------------------------------

namespace LandBuildInt
{

//-----------------------------------------------------------------------------

  const string S_DTED2        = "DTED2";
  const string S_USGSDEM      = "USGSDEM";
  const string S_ARCINFOASCII = "ARCINFOASCII";
  const string S_XYZ          = "XYZ";

//-----------------------------------------------------------------------------

  class MapInput
  {
    string m_demType;
    string m_fileName;
    size_t m_srcLeft;
    size_t m_srcTop;
    size_t m_srcRight;
    size_t m_srcBottom;
    double m_dstLeft;
    double m_dstTop;
    double m_dstRight;
    double m_dstBottom;

    EtRectElevationGrid< double, int > m_hmData;

    DECLAREMODULEEXCEPTION( "MapInput" )

  public:
    MapInput()
    {
    }

    MapInput( string demType, string fileName, size_t srcLeft, size_t srcTop,
              size_t srcRight, size_t srcBottom, double dstLeft, double dstTop,
              double dstRight, double dstBottom )
    : m_demType( demType )
    , m_fileName( fileName )
    , m_srcLeft( srcLeft )
    , m_srcTop( srcTop )
    , m_srcRight( srcRight )
    , m_srcBottom( srcBottom )
    , m_dstLeft( dstLeft )
    , m_dstTop( dstTop )
    , m_dstRight( dstRight )
    , m_dstBottom( dstBottom )
    {
    }

    MapInput( const MapInput& other )
    : m_demType( other.m_demType )
    , m_fileName( other.m_fileName )
    , m_srcLeft( other.m_srcLeft )
    , m_srcTop( other.m_srcTop )
    , m_srcRight( other.m_srcRight )
    , m_srcBottom( other.m_srcBottom )
    , m_dstLeft( other.m_dstLeft )
    , m_dstTop( other.m_dstTop )
    , m_dstRight( other.m_dstRight )
    , m_dstBottom( other.m_dstBottom )
    , m_hmData( other.m_hmData )
    {
    }

    MapInput& operator = ( const MapInput& other )
    {
      m_demType   = other.m_demType;
      m_fileName  = other.m_fileName;
      m_srcLeft   = other.m_srcLeft;
      m_srcTop    = other.m_srcTop;
      m_srcRight  = other.m_srcRight;
      m_srcBottom = other.m_srcBottom;
      m_dstLeft   = other.m_dstLeft;
      m_dstTop    = other.m_dstTop;
      m_dstRight  = other.m_dstRight;
      m_dstBottom = other.m_dstBottom;
      m_hmData    = other.m_hmData;

      return (*this);
    }

    const string& GetDemType() const
    {
      return (m_demType);
    }

    const string& GetFileName() const
    {
      return (m_fileName);
    }

    size_t GetSrcLeft() const
    {
      return (m_srcLeft);
    }

    size_t GetSrcTop() const
    {
      return (m_srcTop);
    }

    size_t GetSrcRight() const
    {
      return (m_srcRight);
    }

    size_t GetSrcBottom() const
    {
      return (m_srcBottom);
    }

    double GetDstLeft() const
    {
      return (m_dstLeft);
    }

    double GetDstTop() const
    {
      return (m_dstTop);
    }

    double GetDstRight() const
    {
      return (m_dstRight);
    }

    double GetDstBottom() const
    {
      return (m_dstBottom);
    }

    const EtRectElevationGrid< double, int >& GetHmData() const
    {
      return (m_hmData);
    }

    EtRectElevationGrid< double, int >& GetHmData()
    {
      return (m_hmData);
    }

    void SetDemType( const string& demType ) 
    {
      m_demType = demType;
    }

    void SetFileName( const string& filename )
    {
      m_fileName = filename;
    }

    void SetSrcLeft( size_t srcLeft )
    {
      m_srcLeft = srcLeft;
    }

    void SetSrcTop( size_t srcTop )
    {
      m_srcTop = srcTop;
    }

    void SetSrcRight( size_t srcRight )
    {
      m_srcRight = srcRight;
    }

    void SetSrcBottom( size_t srcBottom )
    {
      m_srcBottom = srcBottom;
    }

    void SetDstLeft( double dstLeft )
    {
      m_dstLeft = dstLeft;
    }

    void SetDstTop( double dstTop ) 
    {
      m_dstTop = dstTop;
    }

    void SetDstRight( double dstRight ) 
    {
      dstRight = dstRight;
    }

    void SetDstBottom( double dstBottom )
    {
      m_dstBottom = dstBottom;
    }

    void SetHmData( const EtRectElevationGrid< double, int >& hmData )
    {
      m_hmData = hmData;
    }

    double DstWidth() const
    {
      return (m_dstRight - m_dstLeft);
    }

    double DstHeight() const
    {
      return (m_dstTop - m_dstBottom);
    }

    bool DstContains( double x, double y ) const
    {
      if ( (x >= m_dstLeft) &&
           (x <= m_dstRight) && 
           (y >= m_dstBottom) && 
           (y <= m_dstTop) )
      {
        return (true);
      }
      else
      {
        return (false);
      }
    }

		bool LoadMapParameters( IMainCommands* cmdIfc, int suffix = -1 )
    {
			string demType;
      string fileName;
			string srcLeft;
      string srcTop;
      string srcRight; 
      string srcBottom;
			string dstLeft;
      string dstTop;
      string dstRight;
      string dstBottom;
      char buffer[50];

      if ( suffix != -1 )
      {
			  sprintf( buffer, "demType%d"  , suffix );
			  demType = string( buffer );
			  sprintf( buffer, "filename%d" , suffix );
        fileName = string( buffer );
			  sprintf( buffer, "srcLeft%d"  , suffix );
			  srcLeft = string( buffer );
			  sprintf( buffer, "srcTop%d"   , suffix );
        srcTop = string( buffer );
			  sprintf( buffer, "srcRight%d" , suffix );
        srcRight = string( buffer );
			  sprintf( buffer, "srcBottom%d", suffix );
        srcBottom = string( buffer );
			  sprintf( buffer, "dstLeft%d"  , suffix );
			  dstLeft = string( buffer );
			  sprintf( buffer, "dstTop%d"   , suffix );
        dstTop = string( buffer );
			  sprintf( buffer, "dstRight%d" , suffix );
        dstRight = string( buffer );
			  sprintf( buffer, "dstBottom%d", suffix );
        dstBottom = string( buffer );
      }
      else
      {
			  demType   = "demType";
			  fileName  = "filename";
			  srcLeft   = "srcLeft";
			  srcTop    = "srcTop";
			  srcRight  = "srcRight";
			  srcBottom = "srcBottom";
			  dstLeft   = "dstLeft";
			  dstTop    = "dstTop";
			  dstRight  = "dstRight";
			  dstBottom = "dstBottom";
      }

			// gets demType from cfg file
      const char* tmp = cmdIfc->QueryValue( demType.c_str(), false );
			if ( !tmp ) 
			{
				ExceptReg::RegExcpt( Exception( 51, ">>> Missing Dem\'s type <<<" ) );
				return (false);
			}

			m_demType = tmp;

			// checks demType
			if ( m_demType != S_DTED2 && 
           m_demType != S_USGSDEM && 
           m_demType != S_ARCINFOASCII && 
           m_demType != S_XYZ )
			{
				ExceptReg::RegExcpt( Exception( 52, ">>> Dem type not implemented <<<" ) );
				return (false);
			}

			// gets filename from cfg file
			tmp = cmdIfc->QueryValue( fileName.c_str(), false );
			if ( !tmp )
			{
				ExceptReg::RegExcpt( Exception( 53, ">>> Missing Dem\'s filename <<<" ) );
				return (false);
			}

			m_fileName = tmp;

			// checks file extension
			if ( m_demType == S_DTED2 ) 
			{
				string tmpFileName = m_fileName;
				transform( tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper );

				if ( !(tmpFileName.find( ".DT2" ) == tmpFileName.length() - 4) )
				{
					ExceptReg::RegExcpt( Exception( 54, ">>> The file is not a DTED2 <<<" ) );
					return (false);
				}
			}
			else if ( m_demType == S_USGSDEM ) 
			{
				string tmpFileName = m_fileName;
				transform( tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper );

				if ( !(tmpFileName.find( ".DEM" ) == tmpFileName.length() - 4) )
				{
					ExceptReg::RegExcpt( Exception( 55, ">>> The file is not a USGS DEM <<<" ) );
					return (false);
				}
			}
			else if ( m_demType == S_ARCINFOASCII ) 
			{
				string tmpFileName = m_fileName;
				transform( tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper );

				if ( (!(tmpFileName.find( ".GRD" ) == tmpFileName.length() - 4) ) && 
             (!(tmpFileName.find( ".ASC" ) == tmpFileName.length() - 4) ) )
				{
					ExceptReg::RegExcpt( Exception( 56, ">>> The file is not a Arc/Info ASCII Grid <<<" ) );
					return (false);
				}
			}
			else if ( m_demType == S_XYZ ) 
			{
				string tmpFileName = m_fileName;
				transform( tmpFileName.begin(), tmpFileName.end(), tmpFileName.begin(), toupper );

				if ( !(tmpFileName.find( ".XYZ" ) == tmpFileName.length() - 4) )
				{
					ExceptReg::RegExcpt( Exception( 56, ">>> The file is not a XYZ <<<" ) );
					return (false);
				}
			}

			// gets srcLeft from cfg file
			tmp = cmdIfc->QueryValue( srcLeft.c_str(), false );
			if ( tmp )
			{
				if ( IsInteger( string( tmp ), false ) )
				{
					m_srcLeft = atoi( tmp );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 61, ">>> Bad data for srcLeft <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 62, ">>> Missing srcLeft value <<<" ) );
				return (false);
			}

			// gets srcRight from cfg file
			tmp = cmdIfc->QueryValue( srcRight.c_str(), false );
			if ( tmp )
			{
				if ( IsInteger( string( tmp ), false ) )
				{
					m_srcRight = atoi( tmp );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 63, ">>> Bad data for srcRight <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 64, ">>> Missing srcRight value <<<" ) );
				return (false);
			}

			// gets srcTop from cfg file
			tmp = cmdIfc->QueryValue( srcTop.c_str(), false );
			if ( tmp )
			{
				if ( IsInteger( string( tmp ), false ) )
				{
					m_srcTop = atoi( tmp );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 65, ">>> Bad data for srcTop <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 66, ">>> Missing srcTop value <<<" ) );
				return (false);
			}

			// gets srcBottom from cfg file
			tmp = cmdIfc->QueryValue( srcBottom.c_str(), false );
			if ( tmp )
			{
				if ( IsInteger( string( tmp ), false ) )
				{
					m_srcBottom = atoi( tmp );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 67, ">>> Bad data for srcBottom <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 68, ">>> Missing srcBottom value <<<" ) );
				return (false);
			}

			// gets dstLeft from cfg file
			tmp = cmdIfc->QueryValue( dstLeft.c_str(), false );
			if ( tmp )
			{
				if ( IsFloatingPoint( string( tmp ), false ) )
				{
					m_dstLeft = atof( tmp );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 69, ">>> Bad data for dstLeft <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 70, ">>> Missing dstLeft value <<<" ) );
				return (false);
			}

			// gets dstRight from cfg file
			tmp = cmdIfc->QueryValue( dstRight.c_str(), false );
			if ( tmp )
			{
				if ( IsFloatingPoint( string( tmp ), false ) )
				{
					m_dstRight = atof( tmp );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 71, ">>> Bad data for dstRight <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 72, ">>> Missing dstRight value <<<" ) );
				return (false);
			}

			// gets dstTop from cfg file
			tmp = cmdIfc->QueryValue( dstTop.c_str(), false );
			if ( tmp )
			{
				if ( IsFloatingPoint( string( tmp ), false ) )
				{
					m_dstTop = atof( tmp );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 73, ">>> Bad data for dstTop <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 74, ">>> Missing dstTop value <<<" ) );
				return (false);
			}

			// gets dstBottom from cfg file
			tmp = cmdIfc->QueryValue( dstBottom.c_str(), false );
			if ( tmp )
			{
				if ( IsFloatingPoint( string( tmp ), false ) )
				{
					m_dstBottom = atof( tmp );
				}
				else
				{
					ExceptReg::RegExcpt( Exception( 75, ">>> Bad data for dstBottom <<<" ) );
					return (false);
				}
			}
			else
			{
				ExceptReg::RegExcpt( Exception( 76, ">>> Missing dstBottom value <<<" ) );
				return (false);
			}

			return (true);
    }

		bool LoadHighMap()
    {
			if ( m_demType == S_DTED2 )
			{
				if ( !(m_hmData.loadFromDT2( m_fileName )) )
				{
					ExceptReg::RegExcpt( Exception( 81, ">>> Error while loading DTED2 file <<<" ) );
					return (false);
				}
			}
			else if ( m_demType == S_USGSDEM )
			{
				if ( !(m_hmData.loadFromUSGSDEM( m_fileName )) )
				{
					ExceptReg::RegExcpt( Exception( 82, ">>> Error while loading USGS DEM file <<<" ) );
					return (false);
				}
				else
				{
					// if the elevation units in the DEM are feet, converts to METERS
					if ( m_hmData.getWorldUnitsW() == EtRectElevationGrid< double, int >::euFEET )
					{
						m_hmData.convertWorldUnitsW( EtRectElevationGrid< double, int >::euMETERS );
					}
				}
			}
			else if ( m_demType == S_ARCINFOASCII )
			{
				if ( !(m_hmData.loadFromARCINFOASCIIGrid( m_fileName )) )
				{
					ExceptReg::RegExcpt( Exception( 83, ">>> Error while loading Arc/Info Ascii Grid file <<<" ) );
					return (false);
				}
			}
			else if ( m_demType == S_XYZ )
			{
				if ( !(m_hmData.loadFromXYZ( m_fileName )) )
				{
					ExceptReg::RegExcpt( Exception( 84, ">>> Error while loading xyz file <<<" ) );
					return (false);
				}
			}
      else
      {
        return (false);
      }
      
      return (true);
    }
  };

//-----------------------------------------------------------------------------

  class DemSampler : public IHeightMapSampler
  {
    vector< MapInput > m_sources;

  public:
    DemSampler( const vector< MapInput >& sources ) 
    : m_sources( sources )
    {
    }

    virtual double GetHeight( const HeightMapLocInfo& heightInfo ) const
    {
      for ( int i = 0, cnt = m_sources.size(); i < cnt; ++i )
      {
        // select the source owner of the heightInfo point
        // and get the height from it
        if ( m_sources[i].DstContains( heightInfo.GetX(), heightInfo.GetY() ) )
        {
          double srcWLeft   = m_sources[i].GetHmData().getWorldUAt( m_sources[i].GetSrcLeft() );
          double srcWBottom = m_sources[i].GetHmData().getWorldVAt( m_sources[i].GetSrcBottom() );
          double srcWRight  = m_sources[i].GetHmData().getWorldUAt( m_sources[i].GetSrcRight() );
          double srcWTop    = m_sources[i].GetHmData().getWorldVAt( m_sources[i].GetSrcTop() );

          double srcSizeX = srcWRight - srcWLeft;
          double srcSizeY = srcWTop - srcWBottom;
          double dstSizeX = m_sources[i].DstWidth();
          double dstSizeY = m_sources[i].DstHeight();
          
          double newX = srcWLeft   + (heightInfo.GetX() - m_sources[i].GetDstLeft())* srcSizeX / dstSizeX;
          double newY = srcWBottom + (heightInfo.GetY() - m_sources[i].GetDstBottom()) * srcSizeY / dstSizeY;

          return (m_sources[i].GetHmData().getWorldWAt( newX, newY, EtRectElevationGrid< double, int >::imBILINEAR ));
        }
      }
      // if we arrive here something went wrong
      return (0.0);
    }
  };

//-----------------------------------------------------------------------------

} // namespace LandBuildInt