//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <map>

#include "IBuildModule.h"

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

	using namespace LandBuildInt;

//-----------------------------------------------------------------------------

	class ICreateModule
	{
	public:
		virtual IBuildModule* CreateModule( const char* params ) const = 0;
	};

//-----------------------------------------------------------------------------

	class ModuleRegistrator
	{
		std::map< RStringI, ICreateModule* > m_moduleMap;

	public:

		static ModuleRegistrator* GetInstance() 
		{
			static ModuleRegistrator instance;
			return (&instance);
		}

		void Register( const RStringI& moduleName, ICreateModule* creator )
		{
			m_moduleMap.insert( std::pair< RStringI, ICreateModule* >( moduleName, creator ) );
		}

		ICreateModule* Find( const RStringI& moduleName ) const 
		{
			std::map< RStringI, ICreateModule* >::const_iterator it = m_moduleMap.find( moduleName );
      if ( it == m_moduleMap.end() ) { return (NULL); }
			return (it->second);
		}

		IBuildModule* CreateModule( const RStringI& moduleName, const char* params )
		{
			ICreateModule* res = Find( moduleName );
			if ( res ) 
			{
				return (res->CreateModule( params ));
			}
			else 
			{
				return (NULL);
			}
		}

		static std::pair< RStringI, RString > SplitNameAndParams( RStringI moduleName )
		{
			char* name = moduleName.MutableData();
			char* params = 0;
			char* doubledots = const_cast< char* >( strchr( name, ':' ) );
			if ( doubledots ) 
			{
				*doubledots = 0;
				params = doubledots + 1;
			}
			return (std::pair< RStringI, RString >( moduleName, params ));
		}

		template< class Functor >
		bool ForEachModule( const Functor& functor ) const
		{
			for ( std::map< RStringI, ICreateModule* >::const_iterator it = m_moduleMap.begin(); it != m_moduleMap.end(); ++it )
			{
        if ( functor( it->first, it->second ) ) { return (true); }
			}

			return (false);
		}
	};

//-----------------------------------------------------------------------------

	template< class ModuleClass >
	class RegisterModule : public ICreateModule
	{
  public:  
		RegisterModule( const RStringI& name )
		{
			ModuleRegistrator::GetInstance()->Register( name, this );
		} 

		virtual IBuildModule* CreateModule( const char* params ) const 
		{
			return (new ModuleClass);
		}
	};

//-----------------------------------------------------------------------------

	template< class ModuleCLass >
	class RegisterModuleParams : public ICreateModule
	{
	public:
		RegisterModuleParams( const RStringI& name )
		{
			ModuleRegistrator::GetInstance()->Register( name, this );
		} 

		virtual IBuildModule* CreateModule( const char* params ) const
		{
			return (new ModuleClass( params ));
		}
	};

//-----------------------------------------------------------------------------

} // namespace LandBuilder2

//-----------------------------------------------------------------------------
