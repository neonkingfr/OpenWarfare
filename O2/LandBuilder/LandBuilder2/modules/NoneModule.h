#pragma once

#include "IBuildModule.h"

namespace LandBuildInt
{
	namespace Modules
	{
		class NoneModule: public IBuildModule
		{
		public:
			virtual void Run(IMainCommands*) 
			{
			};
		};
	}
}