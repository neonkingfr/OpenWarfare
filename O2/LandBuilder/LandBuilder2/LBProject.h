//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <map>
#include "ShapeGroup.h"
#include "../Shapes/ShapeDatabase.h"
#include "../Shapes/ShapeOrganizer.h"
#include "IProjectTask.h"
#include "LBWorkspace.h"
#include "LBTaskQueue.h"
#include "LBShapes.h"

//-----------------------------------------------------------------------------

namespace LandBuilder2 
{

//-----------------------------------------------------------------------------

	typedef std::map< RStringI, RString > StringMap;

//-----------------------------------------------------------------------------

  class IShapefileEnumerator;

//-----------------------------------------------------------------------------

	class LBProject
	{
	protected:
		friend class LBProjectBuildHelp;

		LBShapes               m_lbshape;
		ShapeGroupSet          m_groupSet;
		Shapes::ShapeOrganizer m_shapes;
		LBTaskQueue            m_queue;
		StringMap              m_resourceTranslate;    
		std::set< RString >    m_flags;
  
	public:
		enum Result 
		{
			resOk                 = 0,
			resShapeFileNotExists = 1,
			resShapeLoadError     = 2
		};

		LBProject() 
		: m_shapes( &m_lbshape ) 
		{
		}

		void RunProject( LBWorkspace* workspace );
		bool EnqueueTask( LandBuilder2::IProjectTask* task, bool highpriority );
 
		ShapefileDesc* GetShapeGroup( const char* groupName ) const;
		ShapefileDesc* CreateShapeGroup( const char* groupName );

    int EnumShapefiles( const IShapefileEnumerator& enumerator, const char* prefix = NULL ) const;

		StringMap& GetResourceTranslateTable() 
		{
			return (m_resourceTranslate);
		}

		ShapeGroupSet& GetGroupSet()
    {
      return (m_groupSet);
    }

		Result AddShapeFile( const Pathname& name, const char* groupName,
                         Shapes::IDBColumnNameConvert* dbconvert = NULL,
                         const Pathname& DBFName = Pathname( PathNull ) );
  
		DoubleRect GetShapeBoundingBox() const;

		void CutShapesIntoFrame( const DoubleRect& rect );

		void SetFlag( const RString& flag );
		bool CanRunThisTask( IProjectTask* task, bool report = false ) const;
		bool ProcessTask( IProjectTask* task, LBProjectBuildHelp& helper );
	};

//-----------------------------------------------------------------------------

} // namespace LandBuilder2