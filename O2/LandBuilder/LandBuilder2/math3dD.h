//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#define Vector3DPar const Vector3D&
#define Vector3DVal const Vector3D&
#define Matrix3DPar const Matrix3D&
#define Matrix3DVal const Matrix3D&

//-----------------------------------------------------------------------------

class Vector3D;
class Matrix3D;
class Matrix4D;

//-----------------------------------------------------------------------------

extern const Vector3D VZeroD;

//-----------------------------------------------------------------------------

static __forceinline double Inv( double c ) 
{
  return (1.0 / c);
}

//-----------------------------------------------------------------------------

static double InvSqrtD( double x )
{
  return (1.0 / sqrt( x ));
}

//-----------------------------------------------------------------------------

static inline double SquareD( double x ) { return (x * x); }

//-----------------------------------------------------------------------------

static inline double dSign( double x )
{
  if ( x > 0.0 ) { return (+1.0); }
  if ( x < 0.0 ) { return (-1.0); }
	return (0);
}

//-----------------------------------------------------------------------------

/// maximum of two values
/** if a is NaN, b is used - helps in saturate */
inline double floatMaxD( double a, double b) { return (a > b ? a : b); }

/// minimum of two values
/** if a is NaN, b is used - helps in saturate */
inline double floatMinD( double a, double b) { return (a < b ? a : b); }

/// if a is above b, saturate it : a = min(a,b)
__forceinline void saturateMinD( double& a, double b ) { a = floatMinD( a, b ); }

/// if a is below b, saturate it : a = max(a,b)
__forceinline void saturateMaxD( double& a, double b ) { a = floatMaxD( a, b ); }

/// saturate a between minVal and maxVal
inline void saturateD( double& a, double minVal, double maxVal )
{
  a = floatMaxD( a, minVal );
  a = floatMinD( a, maxVal );
}

//-----------------------------------------------------------------------------

class Vector3D
{
  friend class Matrix4D;
  friend class Matrix3D;
  
  // 3D type - used for rendering, screen clipping ...
protected:
  //! Vector of three coordinates with XYZ order.
  double m_e[3];

  //! Getting of the specified coordinate.
  /*!
    This method returns a non l-value.
    \param i Index of coordinate to retrieve. 0 = X, 1 = Y, 2 = Z.
    \return Coordinate itself.
  */
  __forceinline double Get( int i ) const {return m_e[i];}

  //! Setting of the specified coordinate.
  /*!
    This method returns a l-value.
    \param i Index of coordinate to set. 0 = X, 1 = Y, 2 = Z.
    \return Coordinate itself.
  */
  __forceinline double& Set( int i ) { return m_e[i]; }
  
public:
  // data initializers
  //! Initialization of a vector by transformation of the specified vector by the specified matrix.
  /*!
    \param v Source vector.
    \param a Matrix to transform source vector by.
  */
  void SetMultiplyLeft( Vector3DPar v, const Matrix3D& a );

  //! Initialization of a vector by transformation of the specified vector by the specified matrix.
  /*!
    Only 3x3 sub-matrix of the specified matrix is used.
    \param a Matrix to transform source vector by.
    \param v Source vector.
  */
  void SetRotate( const Matrix4D& a, Vector3DPar v );

  //! Initialization of a vector by transformation of the specified vector by the specified matrix.
  /*!
    This function is coded in assembly.
    Only 3x3 sub-matrix of the specified matrix is used.
    \param a Matrix to transform source vector by.
    \param v Source vector.
  */
  void SetFastTransform( const Matrix4D& a, Vector3DPar v );

  //! Inline version of the function SetFastTransform.
  /*!
    \param a Matrix to transform source vector by.
    \param v Source vector.
  */
  __forceinline void SetMultiply( const Matrix3D& a, Vector3DPar v );

  //! Inline version of the function SetFastTransform.
  /*!
    \param a Matrix to transform source vector by.
    \param v Source vector.
  */
  __forceinline void SetMultiply( const Matrix4D& a, Vector3DPar v );

  // ???
  /*!
  */
  double SetPerspectiveProject( const Matrix4D& a, Vector3DPar o );
  
  // constructors
  #if !_RELEASE
    Vector3D(){m_e[0]=m_e[1]=m_e[2]=FLT_SNAN;} // default no init
  #else
    __forceinline Vector3D(){}
  #endif
  __forceinline void Init() {} // init 4th component
  //__forceinline Vector3D( enum _vZero ){m_e[0]=m_e[1]=m_e[2]=coord(0);}
  //! Constructor which accepts 3 coordinates.
  /*!
    \param x X coordinate.
    \param y Y coordinate.
    \param z Z coordinate.
  */
  __forceinline Vector3D( double x, double y, double z )
  {
    m_e[0]=x,m_e[1]=y,m_e[2]=z;
  }

  //! Copy constructor.
  /*!
    Note that compiler generated default copy is not inline.
    \param src Source vector.
  */
  __forceinline Vector3D( const Vector3D& src )
  {
    m_e[0]=src.m_e[0],m_e[1]=src.m_e[1],m_e[2]=src.m_e[2];
  }

  __forceinline Vector3D& operator = ( const Vector3D& src )
  {
    // copying uninitialized data is not a problem - problem is using them
    m_e[0]=src.m_e[0],m_e[1]=src.m_e[1],m_e[2]=src.m_e[2];
    return *this;
  }

  __forceinline Vector3D( enum _noInit ){}
  __forceinline Vector3D( enum _guardInit ){m_e[0]=m_e[1]=m_e[2]=FLT_SNAN;}

  __forceinline Vector3D( enum _vMultiply, const Matrix3D& a, Vector3DPar v ){SetMultiply(a,v);}
  __forceinline Vector3D( enum _vMultiply, const Matrix4D& a, Vector3DPar v ){SetMultiply(a,v);}
  __forceinline Vector3D( enum _vMultiplyLeft, Vector3DPar v, const Matrix3D& a ){SetMultiplyLeft(v,a);}

  __forceinline Vector3D( enum _vRotate, const Matrix4D& a, Vector3DPar v ){SetRotate(a,v);}
  __forceinline Vector3D( enum _vFastTransform, const Matrix4D& a, Vector3DPar v ){SetFastTransform(a,v);}
  __forceinline Vector3D( enum _vFastTransformA, const Matrix4D& a, Vector3DPar v ){SetFastTransform(a,v);}
  
  // properties
  __forceinline double operator []( int i ) const {return m_e[i];}
  __forceinline double& operator [] ( int i ) {return m_e[i];}
  
  __forceinline double X() const {return m_e[0];}
  __forceinline double Y() const {return m_e[1];}
  __forceinline double Z() const {return m_e[2];}

  operator const double * () const {return m_e;}
  
  double SquareSize() const {return X()*X()+Y()*Y()+Z()*Z();}
  __forceinline double SquareSizeInline() const {return X()*X()+Y()*Y()+Z()*Z();}

  double Size() const
  {
    //return sqrt(X()*X()+Y()*Y()+Z()*Z());}
    // optimization: |v|=|v|*|v|/|v|
    double size2=SquareSizeInline();
    if( size2<=DBL_MIN ) return 0;
    double invSize=InvSqrtD(size2);
    return size2*invSize;
  }

  double InvSize() const {return InvSqrtD(SquareSize());}
  double InvSquareSize() const {return Inv(SquareSize());}

  double SquareSizeXZ() const {return X()*X()+Z()*Z();}

  double SizeXZ() const
  {
    double size2=SquareSizeXZ();
    if( size2<=DBL_MIN  ) return 0.0;
    double invSize=InvSqrtD(size2);
    return size2*invSize;
  }

  double InvSizeXZ() const {return InvSqrtD(SquareSizeXZ());}
  double InvSquareSizeXZ() const {return Inv(SquareSizeXZ());}
  
  // vector arithmetics
  __forceinline Vector3D operator - () const
  {
    return Vector3D(-X(),-Y(),-Z());
  }
  
  __forceinline Vector3D operator + ( Vector3DPar op ) const
  {
    return Vector3D(X()+op.X(),Y()+op.Y(),Z()+op.Z());
  }

  __forceinline Vector3D operator - ( Vector3DPar op ) const
  {
    return Vector3D(X()-op.X(),Y()-op.Y(),Z()-op.Z());
  }

  __forceinline friend Vector3D operator * ( double f, Vector3DPar op );

  __forceinline Vector3D operator * ( double f ) const
  {
    return Vector3D(Get(0)*f,Get(1)*f,Get(2)*f);
  }

  __forceinline Vector3D Modulate( Vector3DPar op ) const
  {
    return Vector3D(Get(0)*op.Get(0),Get(1)*op.Get(1),Get(2)*op.Get(2));
  }

  Vector3D operator / ( double f ) const
  {
    double invF=1.0/f;
    return Vector3D(X()*invF,Y()*invF,Z()*invF);
  }
  
  Vector3D& operator += ( Vector3DPar op )
  {
    m_e[0]+=op.X();
    m_e[1]+=op.Y();
    m_e[2]+=op.Z();
    return *this;
  }

  Vector3D& operator -= ( Vector3DPar op )
  {
    m_e[0]-=op.X();
    m_e[1]-=op.Y();
    m_e[2]-=op.Z();
    return *this;
  }

  Vector3D& operator *= ( double f )
  {
    m_e[0]*=f;
    m_e[1]*=f;
    m_e[2]*=f;
    return *this;
  }

  Vector3D& operator /= ( double f )
  {
    double invF=1.0/f;
    m_e[0]*=invF;
    m_e[1]*=invF;
    m_e[2]*=invF;
    return *this;
  }
  
  Vector3D Normalized() const;
  inline Vector3D NormalizedFast() const;
  void Normalize(); // no return to avoid using instead of Normalized
  double NormalizeSize();

  __forceinline double DotProduct( Vector3DPar op ) const
  {
    return X()*op.X()+Y()*op.Y()+Z()*op.Z();
  }

  __forceinline double operator * ( Vector3DPar op ) const
  {
    return DotProduct(op);
  }

  __forceinline Vector3D operator * ( const Matrix3D& op ) const
  {
    return Vector3D(VMultiplyLeft,*this,op);
  }

  ///Calculates cosinus angle between this vector and another vector
  /**
   * @param other vector
   * @return cosinus of angle. Use arccos to get angle in range between 0 .. PI
   */
  double CosAngle( Vector3DPar op ) const;

  ///Calculates sinus angle between this vector and another vector
  /**
   * @param other vector
   * @param worldsTop To define quadrant for negative values, you have to declare vector
   *  that representing "worlds top" direction. In other words, you have to define, which
   *  direction is up, and which direction is bottom. This also defines rotation direction
   *  using the three finger rule (left hand rule or right hand rule). The vector
   *  doesn't need to normalized and doesn't need to be upright, 
   *  bud result vector of cross product (is upright to
   *  both vectors) needn't be upright to worlds top. In such a case, 
   *  result is always positive
   * @return sinus of angle. Use arcsin to get angle in range between -PI/2 .. PI/2
   */
  double SinAngle( Vector3DPar op, Vector3DPar worldsTop ) const;
  double Distance( Vector3DPar op ) const;

  __forceinline double Distance2( Vector3DPar op ) const
  {
    return (*this-op).SquareSizeInline();
  }

  __forceinline double Distance2Inline( Vector3DPar op ) const
  {
    return (*this-op).SquareSizeInline();
  }

  double DistanceXZ( Vector3DPar op ) const
  {
    return (*this-op).SizeXZ();
  }

  double DistanceXZ2( Vector3DPar op ) const
  {
    return (*this-op).SquareSizeXZ();
  }

  Vector3D Project( Vector3DPar op ) const;

  __forceinline Vector3D CrossProduct( Vector3DPar op ) const
  {
	  double ox = op.X();
	  double oy = op.Y();
	  double oz = op.Z();
	  double lx = X();
	  double ly = Y();
	  double lz = Z();

	  double x=ly*oz-lz*oy;
	  double y=lz*ox-lx*oz;
	  double z=lx*oy-ly*ox;
	  return Vector3D(x,y,z);
  }

  Matrix3D Tilda() const;
  bool IsFinite() const;

  bool operator == ( Vector3DPar cmp ) const {return cmp.X()==X() && cmp.Y()==Y() && cmp.Z()==Z();}
  bool operator != ( Vector3DPar cmp ) const {return cmp.X()!=X() || cmp.Y()!=Y() || cmp.Z()!=Z();}

  /**
   * Solves homogen. equation system, returns reliability of the result
   *  (higher values are better) or -1 if not solvable.
   */
  double SetSolve2x2( double a, double b, double c, double d, double e, double f );

  /**
   * Finds eigenvector to the given eigenvalue ("a" to "f" are items
   *  of symmetric matrix to be solved). Returns reliability (higher is better)
   *  or -1 if not solvable.
   */
  double SetEigenVector( double a, double b, double c, double d, double e, double f );

  /// Returns true for [1,1,1]
  bool AllOnes () const;
};

//-----------------------------------------------------------------------------

TypeIsSimple( Vector3D );

//-----------------------------------------------------------------------------

__forceinline Vector3D operator * ( double f, Vector3DPar op )
{
  return Vector3D(op.Get(0)*f,op.Get(1)*f,op.Get(2)*f);
}

//-----------------------------------------------------------------------------

class Matrix3D
{
  friend class Vector3D;
  friend class Matrix4D;
  
  // homogeneous matrix - transformations
private:

  // non-array implementation
  Vector3D m_aside;
  Vector3D m_up;
  Vector3D m_dir;
  __forceinline double Get( int i, int j ) const {return (&m_aside)[j][i];}
  __forceinline double& Set( int i, int j ) {return (&m_aside)[j][i];}
  
public:
  // functions that load matrix with data
  // used internally in constructors, but may be useful also to other purpose
  void SetIdentity();
  void SetZero();

  //! Sets the rotation matrix according to the X axis
  void SetRotationX( double angle );

  //! Sets the rotation matrix according to the Y axis
  /*!
    Note that this method is not in consistency with the other 2 methods.
    The angle here is of the opposite meaning.
  */
  void SetRotationY( double angle );

  //! Sets the rotation matrix according to the X axis
  void SetRotationZ( double angle );

  //! Sets the rotation matrix according to an arbitrary axis
  void SetRotationAxis( Vector3DPar axis, double angle );
  
  void SetScale( double x, double y, double z );
  void SetScale( double scale );

  /// get squared average scale (some vectors can be after transformation longer)
  double Scale2() const;

  /// get squared maximal scale
  double MaxScale2() const;

  // optimization calculate scale and maxscale at once
  void ScaleAndMaxScale2( double& scale, double& maxScale ) const;
  
  /// get average scale  (some vectors can be after transformation longer)
  double Scale() const
  {
    // if scale is zero, the matrix is singular
    double scale2=Scale2();
    Assert(scale2>=0);
    return sqrt(scale2);
  }

  /// get maximal scale
  double MaxScale() const
  {
    // if scale is zero, the matrix is singular
    double scale2=MaxScale2();
    Assert(scale2>=0);
    return sqrt(scale2);
  }

  void ScaleAndMaxScale( double& scale, double& maxScale ) const
  {
    ScaleAndMaxScale2(scale, maxScale);
    Assert(scale>=0 && maxScale>=0);
    scale = sqrt(scale);
    maxScale = sqrt(maxScale);
  }

  /// get inverse scale
  double InvScale() const
  {
    double scale2=Scale2();
    if( scale2<=0 ) return 1; // singular matrix
    return InvSqrtD(scale2);
  }

  //! Setting the orientation of the matrix according to direction and up vectors.
  /*!
    Note that axis Z is the reference axis for the directional vector and Y is
    the reference axis for the up vector. By this I mean that if you set those
    vectors to dir = (0,0,1) and up = (0,1,0), the matrix will be identity.
    After calling this function the matrix will be orthogonalized. The direction
    will remain unchanged whereas the up vector will be modified to be orthogonal
    to the direction vector.
    \param dir Directional vector.
    \param up Up vector.
    \sa SetUpAndAside, SetUpAndDirection, SetDirectionAndAside
  */
  void SetDirectionAndUp( Vector3DPar dir, Vector3DPar up ); // sets only 3x3 sub-matrix

  //! Setting the orientation of the matrix according to up and aside vectors.
  /*!
    Up vector will remain unchanged, aside vector will be modified to be orthogonal
    to the up vector.
    \param up Up vector.
    \param aside Aside vector.
    \sa SetDirectionAndUp, SetUpAndDirection, SetDirectionAndAside
  */
  void SetUpAndAside( Vector3DPar up, Vector3DPar aside );

  //! Setting the orientation of the matrix according to up and direction vectors.
  /*!
    Up vector will remain unchanged, dir vector will be modified to be orthogonal
    to the up vector.
    \param up Up vector.
    \param dir Directional vector.
    \sa SetDirectionAndUp, SetUpAndAside, SetDirectionAndAside
  */
  void SetUpAndDirection( Vector3DPar up, Vector3DPar dir );

  //! Setting the orientation of the matrix according to direction and aside vectors.
  /*!
    Directional vector will remain unchanged, aside vector will be modified to be orthogonal
    to the directional vector.
    \param dir Directional vector.
    \param aside Aside vector.
    \sa SetDirectionAndUp, SetUpAndAside, SetUpAndDirection
  */
  void SetDirectionAndAside( Vector3DPar dir, Vector3DPar aside );

  __forceinline void InlineSetMultiply( const Matrix3D& a, double op )
  {
    SetMultiply(a,op);
  }

  __forceinline void InlineAddMultiply( const Matrix3D& a, double op )
  {
    AddMultiply(a,op);
  }

  __forceinline void InlineSetAdd( const Matrix3D& a, const Matrix3D& b )
  {
    SetAdd(a,b);
  }

  void AddMultiply( const Matrix3D& a, double op );

  void SetMultiply( const Matrix3D& a, const Matrix3D& b )
  {
    double x,y,z;
    x=b.Get(0,0);
    y=b.Get(1,0);
    z=b.Get(2,0);
    Set(0,0)=(a.Get(0,0)*x+a.Get(0,1)*y+a.Get(0,2)*z);
    Set(1,0)=(a.Get(1,0)*x+a.Get(1,1)*y+a.Get(1,2)*z);
    Set(2,0)=(a.Get(2,0)*x+a.Get(2,1)*y+a.Get(2,2)*z);

    x=b.Get(0,1);
    y=b.Get(1,1);
    z=b.Get(2,1);
    Set(0,1)=(a.Get(0,0)*x+a.Get(0,1)*y+a.Get(0,2)*z);
    Set(1,1)=(a.Get(1,0)*x+a.Get(1,1)*y+a.Get(1,2)*z);
    Set(2,1)=(a.Get(2,0)*x+a.Get(2,1)*y+a.Get(2,2)*z);

    x=b.Get(0,2);
    y=b.Get(1,2);
    z=b.Get(2,2);
    Set(0,2)=(a.Get(0,0)*x+a.Get(0,1)*y+a.Get(0,2)*z);
    Set(1,2)=(a.Get(1,0)*x+a.Get(1,1)*y+a.Get(1,2)*z);
    Set(2,2)=(a.Get(2,0)*x+a.Get(2,1)*y+a.Get(2,2)*z);
  }

  void SetAdd( const Matrix3D& a, const Matrix3D& b );
  void SetMultiply( const Matrix3D& a, double op );
  void SetInvertRotation( const Matrix3D& op );
  void SetInvertScaled( const Matrix3D& op );
  void SetInvertGeneral( const Matrix3D& op );
  void SetNormalTransform( const Matrix3D& op );
  void SetTilda( Vector3DPar a );
  
  // placeholder parameter describes constructor type
  Matrix3D( double m00, double m01, double m02,
            double m10, double m11, double m12,
            double m20, double m21, double m22 )
  {
    Set(0,0)=m00,Set(0,1)=m01,Set(0,2)=m02;
    Set(1,0)=m10,Set(1,1)=m11,Set(1,2)=m12;
    Set(2,0)=m20,Set(2,1)=m21,Set(2,2)=m22;
  }

  __forceinline Matrix3D(){}
  __forceinline Matrix3D( enum _noInit ){}
  __forceinline Matrix3D( enum _guardInit ) : m_aside(GuardInit),m_up(GuardInit),m_dir(GuardInit){}
  __forceinline Matrix3D( enum _mRotationX, double angle ){SetRotationX(angle);}
  __forceinline Matrix3D( enum _mRotationY, double angle ){SetRotationY(angle);}
  __forceinline Matrix3D( enum _mRotationZ, double angle ){SetRotationZ(angle);}
  __forceinline Matrix3D( enum _mRotationAxis, Vector3DPar axis, double angle ) {SetRotationAxis(axis,angle);}
  __forceinline Matrix3D( enum _mScale, double x, double y, double z ){SetScale(x,y,z);}
  __forceinline Matrix3D( enum _mScale, double x ){SetScale(x,x,x);}

  __forceinline Matrix3D( enum _mDirection, Vector3DPar dir, Vector3DPar up )
  {
    SetDirectionAndUp(dir,up);
  }

  __forceinline Matrix3D( enum _mUpAndDirection, Vector3DPar up, Vector3DPar dir )
  {
    SetUpAndDirection(up,dir);
  }

  __forceinline Matrix3D( enum _mMultiply, const Matrix3D& a, const Matrix3D& b ){SetMultiply(a,b);}
  __forceinline Matrix3D( enum _mMultiply, const Matrix3D& a, double op ){SetMultiply(a,op);}
  __forceinline Matrix3D( enum _mInverseRotation, const Matrix3D& a ){SetInvertRotation(a);}
  __forceinline Matrix3D( enum _mInverseGeneral, const Matrix3D& a ){SetInvertGeneral(a);}
  __forceinline Matrix3D( enum _mInverseScaled, const Matrix3D& a ){SetInvertScaled(a);}
  __forceinline Matrix3D( enum _mNormalTransform, const Matrix3D& a ){SetNormalTransform(a);}
  __forceinline Matrix3D( enum _mTilda, Vector3DPar a ){SetTilda(a);}
  
  // following operators are defined so that no copy constuctor is used
  // if they are expanded inline, copy is not needed
  __forceinline Matrix3D operator * ( const Matrix3D& op ) const
  {
    // matrix multiplication
    return Matrix3D(MMultiply,*this,op);
  }

  __forceinline Vector3D operator * ( Vector3DPar op ) const
  {
    // vector transformation
    return Vector3D(VMultiply,*this,op);
  }

  __forceinline Matrix3D operator * ( double op ) const
  {
    return Matrix3D(MMultiply,*this,op);
  }

  void operator *= ( double op );
  Matrix3D operator + ( const Matrix3D& a ) const;
  Matrix3D operator - ( const Matrix3D& a ) const;
  Matrix3D& operator += ( const Matrix3D& a );
  Matrix3D& operator -= ( const Matrix3D& a );    

  Matrix3D operator - () const
  {
    Matrix3D ret;
    ret.m_aside = -m_aside;
    ret.m_up = -m_up;
    ret.m_dir = -m_dir;
    return ret;
  }

  bool operator == ( const Matrix3D& op) const {return m_aside==op.m_aside && m_up==op.m_up && m_dir==op.m_dir;}
  bool operator != ( const Matrix3D& op) const {return m_aside!=op.m_aside || m_up!=op.m_up || m_dir!=op.m_dir;}
  
  __forceinline Matrix3D InverseRotation() const {return Matrix3D(MInverseRotation,*this);}
  __forceinline Matrix3D InverseGeneral() const {return Matrix3D(MInverseGeneral,*this);}
  __forceinline Matrix3D InverseScaled() const {return Matrix3D(MInverseScaled,*this);}
  __forceinline Matrix3D NormalTransform() const {return Matrix3D(MNormalTransform,*this);}

  // member access operators
  __forceinline double operator () ( int i, int j ) const {return Get(i,j);}
  __forceinline double &operator () ( int i, int j ) {return Set(i,j);}

  bool IsFinite() const;

  // simple access to generic transformation matrix
  __forceinline const Vector3D& Direction() const {return m_dir;}
  __forceinline const Vector3D& DirectionUp() const {return m_up;}
  __forceinline const Vector3D& DirectionAside() const {return m_aside;}
  void SetDirection( const Vector3D& v ) { m_dir=v;}
  void SetDirectionUp( const Vector3D& v ) { m_up=v;}
  void SetDirectionAside( const Vector3D& v ) { m_aside=v;}

  __forceinline double Distance2( const Matrix3D& mat ) const
  {
    return m_dir.Distance2(mat.m_dir)+m_up.Distance2(mat.m_up)+m_aside.Distance2(mat.m_aside);
  }

  void Orthogonalize();

    // math for PCA:

  /// Transformation matrix is non-mandatory, if NULL, no transform is performed
  void SetCovarianceCenter ( const AutoArray< Vector3D >& data, const Matrix4D* m =NULL );

  bool SetEigenStandard( Matrix3DPar cov, double* eig = NULL );

  bool IsOrthogonal ( Vector3D* sqrSize ) const;

  /// calculate matrix determinant
  double Determinant() const;

  /// calculate determinant of upper-left 2x2 submatrix
  double Determinant2x2() const
  {
    return Get(0,0)*Get(1,1)-Get(1,0)*Get(0,1);
  }
};

//-----------------------------------------------------------------------------

// define EMPTY constructors
TypeIsSimple( Matrix3D );

//-----------------------------------------------------------------------------

class Matrix4D
{
  friend class Vector3D;
  
  // homogenous matrix - transformations
  private:
  Matrix3D m_orientation;
  Vector3D m_position;
  
  __forceinline double Get( int i, int j ) const { return m_orientation.Get( i, j ); }
  __forceinline double& Set( int i, int j ) { return m_orientation.Set( i, j );}

  __forceinline double  GetPos( int i ) const	{return m_position.Get(i);}
  __forceinline double& SetPos( int i ) {return m_position.Set(i);}

public:
  // functions that load matrix with data
  // used internaly in constuctors, but may be useful also to other purpose
  void SetIdentity();
  void SetZero();
  void SetTranslation( Vector3DPar offset );
  void SetRotationX( double angle );
  void SetRotationY( double angle );
  void SetRotationZ( double angle );
  void SetRotationAxis( Vector3DPar axis, double angle ) { m_orientation.SetRotationAxis(axis,angle); m_position=VZeroD; }
  //! Sets the matrix to express rotation according to specified sine and cosine of the angle
  void SetRotationY( double s, double c );
  
  void SetScale( double x, double y, double z );
  void SetPerspective( double cLeft, double cTop );

  // sets only 3x3 submatrix
  __forceinline void SetDirectionAndUp( Vector3DPar dir, Vector3DPar up )
  {
    m_orientation.SetDirectionAndUp( dir, up );
  }

  __forceinline void SetUpAndAside( Vector3DPar up, Vector3DPar aside )
  {
    m_orientation.SetUpAndAside(up,aside);
  }

  __forceinline void SetUpAndDirection( Vector3DPar up, Vector3DPar dir )
  {
    m_orientation.SetUpAndDirection(up,dir);
  }

  __forceinline void SetDirectionAndAside( Vector3DPar dir, Vector3DPar aside )
  {
    m_orientation.SetDirectionAndAside(dir,aside);
  }

  /// get average scale  (some vectors can be after transformation longer)
  __forceinline void SetScale( double scale ){ m_orientation.SetScale(scale); }
  __forceinline double Scale() const { return m_orientation.Scale(); }

  /// get maximal scale
  __forceinline double MaxScale() const {return m_orientation.MaxScale();}

  // optimalization calculate scale and maxscale at once  
  void ScaleAndMaxScale( double& scale, double& maxScale) const { m_orientation.ScaleAndMaxScale(scale, maxScale); }  
  
  void SetOriented( Vector3DPar dir, Vector3DPar up ); // sets whole 4x4 matrix
  void SetView( Vector3DPar point, Vector3DPar dir, Vector3DPar up );
  void SetMultiply( const Matrix4D& a, const Matrix4D& b )
  {
    // result cannot be one of the sources
    Assert(&a!=this);
    Assert(&b!=this);

    double x, y, z;
    x=b.Get(0,0);
    y=b.Get(1,0);
    z=b.Get(2,0);
    Set(0,0)=(a.Get(0,0)*x+a.Get(0,1)*y+a.Get(0,2)*z);
    Set(1,0)=(a.Get(1,0)*x+a.Get(1,1)*y+a.Get(1,2)*z);
    Set(2,0)=(a.Get(2,0)*x+a.Get(2,1)*y+a.Get(2,2)*z);
    x=b.Get(0,1);
    y=b.Get(1,1);
    z=b.Get(2,1);
    Set(0,1)=(a.Get(0,0)*x+a.Get(0,1)*y+a.Get(0,2)*z);
    Set(1,1)=(a.Get(1,0)*x+a.Get(1,1)*y+a.Get(1,2)*z);
    Set(2,1)=(a.Get(2,0)*x+a.Get(2,1)*y+a.Get(2,2)*z);
    x=b.Get(0,2);
    y=b.Get(1,2);
    z=b.Get(2,2);
    Set(0,2)=(a.Get(0,0)*x+a.Get(0,1)*y+a.Get(0,2)*z);
    Set(1,2)=(a.Get(1,0)*x+a.Get(1,1)*y+a.Get(1,2)*z);
    Set(2,2)=(a.Get(2,0)*x+a.Get(2,1)*y+a.Get(2,2)*z);
    x=b.GetPos(0);
    y=b.GetPos(1);
    z=b.GetPos(2);
    SetPos(0)=(a.Get(0,0)*x+a.Get(0,1)*y+a.Get(0,2)*z+a.GetPos(0));
    SetPos(1)=(a.Get(1,0)*x+a.Get(1,1)*y+a.Get(1,2)*z+a.GetPos(1));
    SetPos(2)=(a.Get(2,0)*x+a.Get(2,1)*y+a.Get(2,2)*z+a.GetPos(2));
  }

  void SetAdd( const Matrix4D& a, const Matrix4D& b );
  void SetMultiply( const Matrix4D& a, double b );
  void AddMultiply( const Matrix4D& a, double b );

  __forceinline void InlineSetMultiply( const Matrix4D& a, double op )
  {
    SetMultiply(a,op);
  }

  __forceinline void InlineAddMultiply( const Matrix4D& a, double op )
  {
    AddMultiply(a,op);
  }

  __forceinline void InlineSetAdd( const Matrix4D& a, const Matrix4D& b )
  {
    SetAdd(a,b);
  }

  void SetMultiplyByPerspective( const Matrix4D& A, const Matrix4D& B );
  void SetInvertRotation( const Matrix4D& op );
  void SetInvertScaled( const Matrix4D& op );
  void SetInvertGeneral( const Matrix4D& op );
  
  // placeholder parameter describes constructor type
  Matrix4D( double m00, double m01, double m02, double m03,
            double m10, double m11, double m12, double m13,
            double m20, double m21, double m22, double m23 )
  {
    Set(0,0)=m00,Set(0,1)=m01,Set(0,2)=m02;
    Set(1,0)=m10,Set(1,1)=m11,Set(1,2)=m12;
    Set(2,0)=m20,Set(2,1)=m21,Set(2,2)=m22;
    SetPos(0)=m03,SetPos(1)=m13,SetPos(2)=m23;
  }

  __forceinline Matrix4D(){}

  __forceinline Matrix4D( enum _noInit ){}
  __forceinline Matrix4D( enum _guardInit ) : m_orientation(GuardInit), m_position(GuardInit){}
  __forceinline Matrix4D( enum _mTranslation, Vector3DPar offset ){SetTranslation(offset);}
  __forceinline Matrix4D( enum _mRotationX, double angle ){SetRotationX(angle);}
  __forceinline Matrix4D( enum _mRotationY, double angle ){SetRotationY(angle);}
  __forceinline Matrix4D( enum _mRotationZ, double angle ){SetRotationZ(angle);}
  __forceinline Matrix4D( enum _mRotationAxis, Vector3DPar axis, double angle ) {SetRotationAxis(axis,angle);}
  __forceinline Matrix4D( enum _mScale, double x, double y, double z ){SetScale(x,y,z);}
  __forceinline Matrix4D( enum _mScale, double x ){SetScale(x,x,x);}

  // Scale space according to center
  __forceinline Matrix4D( enum _mScale, double x, Vector3DPar center ) 
  {
    SetScale(x,x,x);
    SetPosition(center - Rotate(center));
  }

  __forceinline Matrix4D( enum _mPerspective, double cLeft, double cTop )
  {
    SetPerspective(cLeft,cTop);
  }

  __forceinline Matrix4D( enum _mDirection, Vector3DPar dir, Vector3DPar up )
  {
    SetOriented(dir,up);
  }

  __forceinline Matrix4D( enum _mUpAndDirection, Vector3DPar up, Vector3DPar dir )
  {
    SetUpAndDirection( up, dir );
    SetPosition( VZeroD );
  }

  __forceinline Matrix4D( enum _mView, Vector3DPar point, Vector3DPar dir, Vector3DPar up )
  {
    SetView( point, dir, up );
  }

  __forceinline Matrix4D( enum _mMultiply, const Matrix4D& a, const Matrix4D& b ){SetMultiply(a,b);}
  __forceinline Matrix4D( enum _mMultiply, const Matrix4D& a, double b ){SetMultiply(a,b);}
  __forceinline Matrix4D( enum _mInverseRotation, const Matrix4D& a ){SetInvertRotation(a);}
  __forceinline Matrix4D( enum _mInverseScaled, const Matrix4D& a ){SetInvertScaled(a);}
  __forceinline Matrix4D( enum _mInverseGeneral, const Matrix4D& a ){SetInvertGeneral(a);}

  // following operators are defined so that no copy constuctor is used
  // if they are expanded inline, copy is not needed
  __forceinline Matrix4D operator * ( const Matrix4D& op ) const
  {
    // matrix multiplication
    return Matrix4D(MMultiply,*this,op);
  }

  __forceinline Matrix4D operator * ( double op ) const
  {
    return Matrix4D(MMultiply,*this,op);
  }

  Matrix4D operator + ( const Matrix4D& op ) const;
  Matrix4D operator - ( const Matrix4D& op ) const;
  void operator += ( const Matrix4D& op );
  void operator -= ( const Matrix4D& op );
  bool operator == ( const Matrix4D& op ) const {return m_orientation==op.m_orientation && m_position==op.m_position;}
  bool operator != ( const Matrix4D& op ) const {return m_orientation!=op.m_orientation || m_position!=op.m_position;}

  __forceinline Vector3D Rotate( Vector3DPar op ) const {return Vector3D(VRotate,*this,op);}
  __forceinline Vector3D FastTransform( Vector3DPar op ) const {return Vector3D(VFastTransform,*this,op);}
  __forceinline Vector3D FastTransformA( Vector3DPar op ) const {return Vector3D(VFastTransformA,*this,op);}
  __forceinline Vector3D operator *( Vector3DPar op ) const {return Vector3D(VMultiply,*this,op);}
  __forceinline Matrix4D InverseRotation() const {return Matrix4D(MInverseRotation,*this);}
  __forceinline Matrix4D InverseScaled() const {return Matrix4D(MInverseScaled,*this);}
  __forceinline Matrix4D InverseGeneral() const {return Matrix4D(MInverseGeneral,*this);}

  // member access operators
  __forceinline double operator () ( int i, int j ) const {return Get(i,j);}
  __forceinline double& operator () ( int i, int j ) {return Set(i,j);}

  double Characteristic() const; // used in fast comparison
  bool IsFinite() const;

  // simple access to generic transformation matrix
  __forceinline const Matrix3D& Orientation() const {return m_orientation;}
  void SetOrientation( const Matrix3D& m ) {m_orientation=m;}
  void SetOrientationScaleOnly( double scale ) {m_orientation.SetScale(scale,scale,scale);}

  __forceinline const Vector3D& Position() const { return (m_position); }
  __forceinline const Vector3D& Direction() const { return (m_orientation.m_dir); }
  __forceinline const Vector3D& DirectionUp() const { return (m_orientation.m_up); }
  __forceinline const Vector3D& DirectionAside() const { return (m_orientation.m_aside); }
  void SetPosition( const Vector3D& v ) { m_position = v; }
  void SetDirection( const Vector3D& v ) { m_orientation.m_dir = v; }
  void SetDirectionUp( const Vector3D& v ) { m_orientation.m_up = v; }
  void SetDirectionAside( const Vector3D& v ) { m_orientation.m_aside = v; }

  __forceinline double Distance2(const Matrix4D& mat) const
  {
    return ( m_orientation.Distance2(mat.m_orientation) + m_position.Distance2(mat.m_position) );
  }
  void Orthogonalize();
};

//-----------------------------------------------------------------------------

TypeIsSimple( Matrix4D );

//-----------------------------------------------------------------------------

// in fact vector is not the same as point
// some operation meaningfull on point have no mean on vector and vice versa
// unmeaningfull are:

// Point+Point
// Point DotProduct Point
// Point CrossProduct Point
// Translate vector
// and maybe some more

// it would be too complicated to build all arithmetics twice so for now
// we suppose the types are equivalent

//-----------------------------------------------------------------------------

inline Vector3D sign( Vector3DPar v )
{
  return Vector3D( dSign(v[0]), dSign(v[1]), dSign(v[2]));
}

//-----------------------------------------------------------------------------

Vector3D VectorMin( Vector3DPar a, Vector3DPar b );
Vector3D VectorMax( Vector3DPar a, Vector3DPar b );

//-----------------------------------------------------------------------------

#define Limit(speed,min,max ) saturateFast(speed,min,max)

//-----------------------------------------------------------------------------

void CheckMinMax( Vector3D& min, Vector3D& max, Vector3DPar val );

//-----------------------------------------------------------------------------

__forceinline void CheckMinMaxInline( Vector3D& min, Vector3D& max, Vector3DPar val )
{
  saturateMinD( min[0], val[0] ), saturateMaxD( max[0], val[0] );
  saturateMinD( min[1], val[1] ), saturateMaxD( max[1], val[1] );
  saturateMinD( min[2], val[2] ), saturateMaxD( max[2], val[2] );
}

//-----------------------------------------------------------------------------

void SaturateMin( Vector3D& min, Vector3DPar val );
void SaturateMax( Vector3D& min, Vector3DPar val );

//-----------------------------------------------------------------------------

// it can improve semantics if we strictly distinguish these two types in source

__forceinline Matrix3D Vector3D::Tilda() const {return Matrix3D(MTilda,*this);}

//-----------------------------------------------------------------------------

__forceinline void Vector3D::SetRotate( const Matrix4D& a, Vector3DPar v )
{
  SetMultiply(a.Orientation(),v);
}

//-----------------------------------------------------------------------------

inline double Vector3D::SetPerspectiveProject( const Matrix4D& a, Vector3DPar o )
{
  // optimize: suppose that matrix is scaled and shifted perspective projection, i.e.
  // member [3,2] is 1.0
  // zero members:
  // [0,1], [0,3],
  // [1,0], [1,3], [2,0], [2,1], [2,2]
  // [3,0], [3,1], [3,3]

  //note: [0,0] and [1,1] need not be 1,
  double oow = 1.0 / o.Get(2);
  Set(0) = a.Get(0,2)+a.Get(0,0)*o.Get(0)*oow;
  Set(1) = a.Get(1,2)+a.Get(1,1)*o.Get(1)*oow;
  Set(2) = a.Get(2,2)+a.GetPos(2)*oow;
  return oow;
}

//-----------------------------------------------------------------------------

__forceinline void Vector3D::SetMultiply( const Matrix4D& a, Vector3DPar o )
{
  double x = o[0], y = o[1], z = o[2];
  Set(0)=a.Get(0,0)*x+a.Get(0,1)*y+a.Get(0,2)*z+a.GetPos(0);
  Set(1)=a.Get(1,0)*x+a.Get(1,1)*y+a.Get(1,2)*z+a.GetPos(1);
  Set(2)=a.Get(2,0)*x+a.Get(2,1)*y+a.Get(2,2)*z+a.GetPos(2);
}

//-----------------------------------------------------------------------------

__forceinline void Vector3D::SetMultiply( const Matrix3D& a, Vector3DPar o )
{ // vector rotation - only 3x3 matrix is used, translation is ignored
  // u=M*v
  double x = o[0], y = o[1], z = o[2];
  Set(0)=a.Get(0,0)*x+a.Get(0,1)*y+a.Get(0,2)*z;
  Set(1)=a.Get(1,0)*x+a.Get(1,1)*y+a.Get(1,2)*z;
  Set(2)=a.Get(2,0)*x+a.Get(2,1)*y+a.Get(2,2)*z;
}

//-----------------------------------------------------------------------------

inline void Vector3D::SetFastTransform( const Matrix4D& a, Vector3DPar o )
{
  double x = o[0], y = o[1], z = o[2];
  Set(0)=a.Get(0,0)*x+a.Get(0,1)*y+a.Get(0,2)*z+a.GetPos(0);
  Set(1)=a.Get(1,0)*x+a.Get(1,1)*y+a.Get(1,2)*z+a.GetPos(1);
  Set(2)=a.Get(2,0)*x+a.Get(2,1)*y+a.Get(2,2)*z+a.GetPos(2);
}

//-----------------------------------------------------------------------------
