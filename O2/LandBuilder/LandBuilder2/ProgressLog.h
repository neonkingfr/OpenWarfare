//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

class IProgressLogSupport
{
public:  
	virtual ~IProgressLogSupport()
  {
  }

  virtual void ReportLineBeg( int level ) = 0;
	virtual std::ostream& GetStream() = 0;
	virtual std::ostream& GetFileStream() = 0;
	virtual void ReportLineEnd( const char* function ) = 0;
	virtual int GetCurLevel() const = 0;
	virtual void SetCurLevel( int lev ) = 0;
};

//-----------------------------------------------------------------------------

class ProgressLog
{
public:
	enum LogLevel
	{
		///debug logging leve;
		levDebug = 0,
		///information messages 
		levInfo = 10,
		///notice that can contain important information
		levNote = 20,
		///log progress messages, such as entering and exitting various phases in app
		levProgress = 30,
		///warning message
		levWarn = 40,
		/// attention, urgent message
		levAtten = 50,
		/// error, but program can continue normally
		levError = 60,  
		/// error, program continuing may cause unusable results
		levCritical = 70,
		/// error, cannot continue.
		levFatal = 80
	};

protected:
	bool          m_showit;
	const char*   m_function;
	std::ostream& m_stream;

public:
	static IProgressLogSupport* logSupport;

	ProgressLog( const char* function, int level )
	: m_showit( level >= logSupport->GetCurLevel() )
	, m_function( function )
	, m_stream( logSupport->GetStream() )
	{
//    if ( m_showit ) { logSupport->ReportLineBeg( level ); }
	}

	~ProgressLog()
	{
    if ( m_showit ) { logSupport->ReportLineEnd( m_function ); }
	}

	template < class T >
	ProgressLog& operator << ( const T& obj )
	{
    if ( m_showit ) 
    { 
      logSupport->GetStream() << obj; 
      logSupport->GetFileStream() << obj; 
    }
		return (*this);
	}

	static void SetLevel( int level ) 
	{
		logSupport->SetCurLevel( level );
	}

	static int GetLevel() 
	{
		return (logSupport->GetCurLevel());
	}
};

//-----------------------------------------------------------------------------

#define LOGF(level) ProgressLog(__FUNCTION__, ProgressLog::lev##level)

//-----------------------------------------------------------------------------


