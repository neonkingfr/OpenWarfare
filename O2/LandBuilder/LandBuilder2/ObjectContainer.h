//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "ObjectDesc.h"
#include "../Shapes/AllocIDArray.h"
#include "../Shapes/Vertex.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{

//-----------------------------------------------------------------------------

  class ObjectContainer
  {
    AllocIDArray< ObjectDesc, 32768 / sizeof( ObjectDesc ) > m_objects;

    class IndexBase : public BTreeDefaultCompare
    {
    protected:
      AllocIDArray< ObjectDesc, 32768 / sizeof( ObjectDesc ) >* m_objects;

    public:
      virtual ~IndexBase()
      {
      }

      void SetOwner( AllocIDArray< ObjectDesc, 32768 / sizeof( ObjectDesc ) >* objects )
      {
        m_objects = objects;
      }

      const ObjectDesc* ConvIdToObject( int id ) const
      {
        if ( id & 0x1 ) 
        {
          return (reinterpret_cast< ObjectDesc* >( id & ~0x1 ));
        }
        else 
        {
          return (&(*m_objects)[id >> 1]);
        }
      }

      int IndexById( int cmpres, int a, int b ) 
      {                
        if ( cmpres == 0 ) 
        {
          if ( (a & 0x1) && (~b & 0x1) ) { return -1; }
          if ( (~a & 0x1) && (b & 0x1) ) { return 1; }
          return ((a > b) - (a < b));
        }
        else
        {
          return (cmpres);
        }
      }
    };

    class IndexByName : public IndexBase
    {
    public:
      virtual ~IndexByName()
      {
      }

      int Cmp( const int& a, const int& b )
      {
        return (IndexById( ConvIdToObject( a )->IndexByName( *ConvIdToObject( b ) ), a, b ));
      }
    };

    class IndexByXPos : public IndexBase
    {
    public:
      virtual ~IndexByXPos()
      {
      }

      int Cmp( const int& a, const int& b )
      {
        return (IndexById( ConvIdToObject( a )->IndexByXPos( *ConvIdToObject( b ) ), a, b ));
      }
    };

    class IndexByYPos : public IndexBase
    {
    public:
      virtual ~IndexByYPos()
      {
      }

      int Cmp( const int& a, const int& b )
      {
        return (IndexById( ConvIdToObject( a )->IndexByYPos( *ConvIdToObject( b ) ), a, b ));
      }
    };

    class IndexByTag : public IndexBase
    {
    public:
      virtual ~IndexByTag()
      {
      }

      int Cmp( const int& a, const int& b )
      {
        return IndexById( ConvIdToObject( a )->IndexByTag( *ConvIdToObject( b ) ), a, b );
      }
    };

    class SearchObj : public ObjectDesc
    {
    public:
      virtual ~SearchObj()
      {
      }

      SearchObj( double x, double y, size_t tag = 0 ) 
      : ObjectDesc( "", Matrix4D( MTranslation, Vector3D( x, y, 0.0 ) ), tag ) 
      {
      }
    };

    BTree< int, IndexByName > m_idxName;
    BTree< int, IndexByXPos > m_idxXPos;
    BTree< int, IndexByYPos > m_idxYPos;
    BTree< int, IndexByTag >  m_idxTag;
    BTree< RStringI >         m_stringTable;

    void IndexItem( int id );

    const ObjectDesc* NearestOnX( double x, double y, size_t tag, const Array< size_t >& ignore ) const;
    const ObjectDesc* NearestOnY( double x, double y, size_t tag, const Array< size_t >& ignore ) const;

    public:
    ObjectContainer();
    ~ObjectContainer();

    RStringI ShareString( const RStringI& str );
    RStringI ShareString( const RStringI& str ) const;

    int Add( const ObjectDesc& object );
    bool Delete( int id );
    bool Replace( int id, const ObjectDesc& object );

    bool SetObjectName( int ID, const RStringI& name );
    int GetObjectID( const ObjectDesc* obj ) const;

    template<class Functor>
    bool ForEachF( const Functor& f ) const
    {
      return (m_objects.ForEachF( f ));
    }

    SRef< AutoArray< const ObjectDesc* > > ObjectsInBox( const Shapes::DBox& box, size_t tag = 0 ) const;

    const ObjectDesc* Nearest( double x, double y, size_t tag = 0, 
                               const Array< size_t >& ignore = Array< size_t >( 0 ) ) const;

    const ObjectDesc& operator [] ( int pos ) const 
    {
      return (m_objects[pos]);
    }

    void ReIndex();

    bool IsValidObjectID( int ID ) const
    {
      return (m_objects.IsIdValid( ID ));
    }

    int Size() const 
    {
      return (m_objects.Size());
    }
  };

//-----------------------------------------------------------------------------

} // namespace LandBuildInt

//-----------------------------------------------------------------------------

TypeIsSimpleZeroed( const LandBuildInt::ObjectDesc* )

//-----------------------------------------------------------------------------
