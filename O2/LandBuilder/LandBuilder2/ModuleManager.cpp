//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "ModuleManager.h"
#include "ModuleRegistrator.h"

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

	ModuleManager::~ModuleManager()
	{
		for ( std::map< RStringI, IBuildModule* >::iterator it = m_moduleMap.begin(); it != m_moduleMap.end(); ++it ) 
		{
      if ( it->second ) { it->second->Release(); }
		}
	}

//-----------------------------------------------------------------------------

	IBuildModule* ModuleManager::GetModule( const RStringI& name )
	{
		std::map< RStringI, IBuildModule* >::iterator found = m_moduleMap.find( name );
    if ( found != m_moduleMap.end() ) { return (found->second); }

		LOGF( Progress ) << "Initializing module: " << name.Data();
		std::pair< RStringI, RString > splitted = ModuleRegistrator::SplitNameAndParams( name );
		IBuildModule* mod = ModuleRegistrator::GetInstance()->CreateModule( splitted.first, splitted.second );
		if ( !mod ) 
		{
			mod = m_pluginManager.CreateModule( splitted.first, splitted.second, Pathname::GetCWD().GetDirectoryWithDrive() );
		}
		m_moduleMap.insert( std::pair< RStringI, IBuildModule* >( name, mod ) );
		return (mod);
	}

//-----------------------------------------------------------------------------

	ModuleManager::ModuleManager()
	{
		m_pluginManager.LoadList();
	}

//-----------------------------------------------------------------------------

} // namespace LandBuilder2
