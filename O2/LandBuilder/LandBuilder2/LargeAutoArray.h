#pragma once

#include <es/containers/array.hpp>

template<class T, int blockSz=4096/sizeof(T)>
class LargeAutoArray
{
  typedef T **arrType ;
  arrType _data;
  int _n;

  int Rows() const {return Row(_n+blockSz-1);}
  int Row(int n) const {return n/blockSz;}
  int Col(int n) const {return n%blockSz;}
  int LastRowCols() const {return Col(_n);}

public:

  LargeAutoArray(void): _data(0),_n(0) {}

  void Clear()
  {
    for (int i=0;i<_n;i++) _data[Row(i)][Col(i)].~T();
    for (int i=0,cnt=Rows();i<cnt;i++) free(_data[i]);
    delete [] _data;
  }

  ~LargeAutoArray(void)
  {
    Clear();
  }

  void *AddPtr()
  {
    int r=(Rows()+31)&~31;    
    _n++;
    int r2=(Rows()+31)&~31;
    if (r!=r2)
    {
      void *k=realloc(_data,sizeof(*_data)*(r2));
      if (k==0) {_n--;return 0;}
      _data=reinterpret_cast<arrType>(k);
      for (int i=r;i<r2;i++)_data[i]=0;
    }
    r=Row(_n-1);
    if (_data[r]==0)
      _data[r]=(T *)malloc(sizeof(T)*blockSz);
    return &_data[Row(_n-1)][Col(_n-1)];
  }

  int Add(const T &obj)
  {
    int ret=_n;
    new(AddPtr()) T(obj);
    return ret;
  }


  void Delete(int index, int count)
  {
    Assert(index>=0 && count>=0 && index+count<=_n);
    if (count==0) return;
    for (int i=0;i<count;i++)
    {
      _data[Row(i+index)][Col(i+index)].~T();
    }
    for (int i=0+count;i<_n;i++)
      memcpy(&(_data[Row(i-count)][Col(i-count)]),&(_data[Row(i)][Col(i)]),sizeof(T));
    _n-=count;
  }

  bool Insert(int index, int count, const T &obj)
  {
    if (count==0) return;
    Assert(index>=0 && count>=0 && index<=_n);
    int r=Rows();   
    _n+=count;
    int r2=Rows();
    if (r!=r2)
    {
      void *k=realloc(_data,sizeof(*data)*(r2));
      if (k==0) return false;
      _data=reinterpret_cast<arrType>(k);
      for (int i=r;i<r2;i++) _data[i]=malloc(sizeof(T)*blockSz);
    }
    for (int i=_n-1;i>=(index+count);i--)
      memcpy(&(data[Row(i)][Col(i)]),&(data[Row(i-count)][Col(i-count)],sizeof(T)));
    for (int i=0;i<count;i++)
      new(data[Row(i+index)]+Col(i+index))
  }

  const T &GetAt(int n) const
  {
    Assert(n>=0 && n<_n);
    return _data[Row(n)][Col(n)];
  }
  T &GetAt(int n)
  {
    Assert(n>=0 && n<_n);
    return _data[Row(n)][Col(n)];
  }

  const T &operator[](int n) const {return GetAt(n);}
  T &operator[](int n) {return GetAt(n);}

  int Size() const {return _n;}

  int GetObjectIndex(const T *obj) const
  {
    for (int i=0,cnt=Rows();i<cnt;i++)
    {
      if (obj>=_data[i] && obj<_data[i]+blockSz)
      {
        return i*blockSz+(_data[i]-obj);
      }
    }
    return -1;
  }
};
