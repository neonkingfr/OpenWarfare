//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "PluginManager.h"
#include "ModuleRegistrator.h"
#include <windows.h>
#include <set>

//-----------------------------------------------------------------------------

#ifdef _DEBUG
#define PLUGININIT "LandBuilderPlugInit_Debug"
#else
#define PLUGININIT "LandBuilderPlugInit"
#endif

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

	typedef IPlugin* (*PlugInit_Type)( LBGlobalPointers* globPointers );

//-----------------------------------------------------------------------------

	PluginInfo PluginManager::LoadDll( const char* dllName )
	{
		HMODULE mod = LoadLibrary( dllName );
		if ( mod == 0 )
		{
			LOGF( Error ) << "Can't open file: " << dllName;
		}
		else
		{
			PlugInit_Type plugInit = (PlugInit_Type)GetProcAddress( mod, PLUGININIT );
			if ( plugInit == 0 )
			{
				LOGF( Warn ) << "File is not valid plugin: " << dllName;
				FreeLibrary( mod );
			}
			else
			{
				IPlugin* res = plugInit( &m_globParams );
				if ( res == 0 )
				{
					LOGF( Error ) << "PluginInfo failed to initialize: " << dllName;
					FreeLibrary( mod );
				}
				else
				{
					return (PluginInfo( res, mod ));
				}
			}
		}
		return (PluginInfo( 0, 0 ));
	}

//-----------------------------------------------------------------------------

	void PluginManager::UnloadDll( PluginInfo& plugin )
	{
    if ( plugin.first )  { plugin.first->Release(); }
    if ( plugin.second ) { FreeLibrary( (HMODULE)plugin.second ); }
	}

//-----------------------------------------------------------------------------

	Pathname getLbSectionPath()
	{
    // new implementation:
    // the map file is into the same folder of the exe
		Pathname cesta = Pathname::GetExePath( 0 );
		cesta.GetDirectoryWithDrive();
		cesta.SetFilename( "plugins.map" );

//    // old implementation:
//    // the map file is into its own folder
//		cesta.SetDirAllUsersAppData();
//		cesta.SetFilename( "LandBuilder2" );
//		cesta.SetDirectory( cesta );
//		cesta.SetFilename( "plugins.map" );
//		cesta.CreateFolder();

		return (cesta);
	}

//-----------------------------------------------------------------------------

	PluginManager::~PluginManager()
	{
		for ( std::map< RStringI, PluginInfo >::iterator itr = m_loadedDLLs.begin(); itr != m_loadedDLLs.end(); ++itr )
		{
			UnloadDll( itr->second );
		}
	}

//-----------------------------------------------------------------------------

	class EnumerateModules
	{
		mutable PluginManager* outer;

		RStringI dllName;

	public:
		EnumerateModules( PluginManager* outer, RString dllName ) 
    : outer( outer )
    , dllName( dllName ) 
		{
		}

		bool operator () ( RStringI name, ICreateModule* creator ) const
		{
			LOGF(Info) << "...Found module: " << name.Data();
			outer->RegisterModule( name.Data(), dllName );
			return (false);
		}
	};

//-----------------------------------------------------------------------------

	void PluginManager::ScanDll( const char* dllName )
	{
		LOGF( Progress ) << "Scanning DLL: " << dllName;
    std::map< RStringI, PluginInfo >::const_iterator iter = m_loadedDLLs.find( dllName );
    ModuleRegistrator* reg;
    PluginInfo plug;
    if ( iter == m_loadedDLLs.end() ) //you don't need to load already loaded modules
    {
      plug = LoadDll( dllName );
      if ( plug.first == 0 )
      {
        return;
      }
      reg = plug.first->GetModules();

      if ( reg == 0 )
      {
        LOGF( Error ) << "PluginInfo : " << dllName << " initialization failed";
        UnloadDll( plug );
        return;
      }
    }
    else  
		{
      reg = iter->second.first->GetModules();
		}

    reg->ForEachModule( EnumerateModules( this, dllName ) );
    
    //you don't need to load already loaded modules
    if ( iter == m_loadedDLLs.end() ) { UnloadDll( plug ); }
	}

//-----------------------------------------------------------------------------

	void PluginManager::RegisterModule( RStringI module, RString dll )
	{
		std::map< RStringI, RStringI >::const_iterator tst = m_modules.find( module );
		if ( tst != m_modules.end() )
		{
			if ( tst->second != RStringI( dll ) )
			{
				LOGF( Error ) << "Conflict: Cannot register module " << module.Data() << " from plugin: " << dll.Data()
                      << ". There is already another one registered in " << tst->second.Data();
			}
		}
		else
		{
			m_modules.insert( std::make_pair( module, dll ) );
      m_paths.insert( dll.Mid( 0, Pathname::GetNameFromPath( dll ) - dll.Data() ) );
		}
	}

//-----------------------------------------------------------------------------

	IBuildModule* PluginManager::CreateModule( const RStringI& name, const char* params, const char* cwd )
	{
		std::map< RStringI, RStringI >::const_iterator iter = m_modules.find( name );
		if ( iter == m_modules.end() )
		{
			MakeScanning();
			iter = m_modules.find( RStringI( name ) );
      if ( iter == m_modules.end() ) { return (0); }
		}

		std::map< RStringI, PluginInfo >::const_iterator iter2 = m_loadedDLLs.find( iter->second );
		PluginInfo lib;
		if ( iter2 == m_loadedDLLs.end() )
		{
			Pathname libpath = iter->second;
			lib = LoadDll( libpath );
			if ( lib.first == 0 ) 
			{
				MakeScanning();
				return (CreateModule( name, params, cwd ));
			}    
			m_loadedDLLs.insert( std::pair< RStringI, PluginInfo >( iter->second, lib ) );
			LOGF( Note ) << "Loaded plugin: " << iter->second;
		}
		else
		{
			lib = iter2->second;
		}
		ModuleRegistrator* reg = lib.first->GetModules();;
		if ( reg == 0 )
		{
			LOGF( Error ) << "PluginInfo : " << iter->second.Data() << " initialization failed";      
			return (0);
		}
  
		lib.first->SetCWD( cwd );
		return (reg->CreateModule( name, params ));
	}

//-----------------------------------------------------------------------------

	void PluginManager::MakeScanning()
	{
    if ( m_scanned ) { return; }

    LOGF( Note ) << "### File 'plugins.map' contains invalid records - rebuild started";
    m_modules.clear();
        
		m_scanned = true;
		std::set< RStringI > allpaths = m_paths;
		Pathname cur;
		allpaths.insert( cur.GetDirectoryWithDrive() );
		cur = Pathname::GetExePath( 0 );
		allpaths.insert( cur.GetDirectoryWithDrive() );
		
		for ( std::set< RStringI >::const_iterator iter = allpaths.begin(); iter != allpaths.end(); ++iter )
		{
			ScanFolder( *iter );
		}

		SaveList();
    LOGF( Note ) << "### Rebuild finished...";
	}

//-----------------------------------------------------------------------------

	void PluginManager::SaveList() const
	{
		std::ofstream out( getLbSectionPath(), std::ios::out | std::ios::trunc );
		for ( std::map< RStringI, RStringI >::const_iterator iter = m_modules.begin(); iter != m_modules.end(); ++iter )
		{
			out << iter->first.Data() << "\t" << iter->second.Data() << "\n";
		}
	}

//-----------------------------------------------------------------------------

	void PluginManager::ScanFolder( const char* folder )
	{
		WIN32_FIND_DATA wfind;

		HANDLE h = FindFirstFile( RString( folder, "*.DLL" ), &wfind );
		if ( h != 0 && h != INVALID_HANDLE_VALUE )
		{
			do
			{
				if ( !(wfind.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
				{
					ScanDll( RString( folder, wfind.cFileName ) );
				}
			}
			while ( FindNextFile( h, &wfind ) );
			FindClose( h );
		}
	}

//-----------------------------------------------------------------------------

	void PluginManager::LoadList()
	{
		std::ifstream in( getLbSectionPath(), std::ios::in );
		RStringI mod;
		RStringI dll;
		char* modb = mod.CreateBuffer( 2048 );
		char* dllb = dll.CreateBuffer( 2048 );
		if ( !(!in) )
		{
			while ( !in.eof() )
			{
				in.getline( modb, 2048, '\t' );
				in.getline( dllb, 2048, '\n' );
				mod.Trim();
				dll.Trim();
				if ( mod.GetLength() != 0 && dll.GetLength() != 0 )
				{
					RegisterModule( modb, dllb );
				}
			}
		}
		else
		{
			MakeScanning();
		}
	}

//-----------------------------------------------------------------------------

	void PluginManager::AddModulePaths( const std::set< RStringI >& path )
  {
    if ( path.size() )
    {
      LOGF( Progress ) << "Adding new paths for rescan";
      m_paths.insert( path.begin(), path.end() );
      MakeScanning();
    }
  }
}

//-----------------------------------------------------------------------------
