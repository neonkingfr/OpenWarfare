//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include <el/Interfaces/IMultiInterfaceBase.hpp>
#include <stdexcept>

//-----------------------------------------------------------------------------

namespace Shapes 
{
  class IDBColumnNameConvert;
}

//-----------------------------------------------------------------------------

namespace LandBuilder2 
{

//-----------------------------------------------------------------------------

  class IShapefileEnumerator  
  {
  public:
    ///called for every shape
    /**
    @param shapefileName name of found shapefile
    @retval zero continue in enumeration
    @retval non-zero stop enumeration, returned value is used as return value from 
    EnumShapefiles function
    */
    virtual int operator () ( const char* shapefileName ) const = 0;
    };

//-----------------------------------------------------------------------------

  ///extra interface that extends I
  class IShapeDbAccess : public IMultiInterfaceBase
  {
  public:
    DECLARE_IFCUIDA( 'S','p','D','b','A','c' );

    ///Adds shape object into shape group
    /**
    Shape group is group of shapes created during processing. Shapes group
    can be referenced in project. Groups are like shapefiles, but they
    are not exists at disk. 

    @param shapeGroupName name of shape group. If group doesn't exists, it will
    be created. It also can be name of existing shapefile. In that case, shape
    is added into group as it has been in shapefile from begining
    @param shape pointer to new shape. Shape will be accepted only when it uses
    the same vertex array as other shapes. 
    @param parameters list of parameters that will be stored in database with the shape
    @retval true shape group added
    @retval false request reject
    */
    virtual bool AddToShapeGroup( const char* shapeGroupName, Shapes::IShape* shape,
                                  const Array< std::pair< RStringI, RString > >& parameters = Array< std::pair< RStringI, RString > >( 0 ) ) 
    {
      return (false);
    }

    ///Retrieves indexes for given shapefile
    /**
    *@param name name of shapefile. Use NULL to retrieve indexes for current shapefile.
    *@return Array of indexes
    */
    virtual AutoArray< size_t > GetShapefileIndexes( const char* name = NULL ) const 
    {
      return (AutoArray< size_t >( 0 ));
    }

    ///Enumerates shapefiles
    /**
    @param enumerator instance of class that derives IShapeEnumerator. Function
    calls operator() for each shapefile. 
    @param prefix if set, it enumerates all shapefiles, whiches name start with the prefix          
    @return zero enumeration processes all shapefiles
    @retval non-zero enumeration has been stopped on an item and this is result returned
    by operator()
    */
    virtual int EnumShapefiles( const IShapefileEnumerator& enumerator, const char* prefix = NULL ) const = 0;

    ///Retrieves shape at given index
    /**
    @param index index of shape. To retrieve index, use GetShapefileIndexes to retrieve
    array of indexes
    @return pointer to shape of given index or NULL, if index is not used
    */
    virtual const Shapes::IShape* GetShape( int index ) const 
    {
      return (NULL);
    }

    ///Queries DB to a value associated with the shape
    /**
    @param shapeIndex index if the shape. To retrieve index, use GetShapefuleIndexes to retrieve
    array of indexes
    @param name name of field that should contain value to retrived
    @param directDb set to true, if no translation for name should be provided. By default
    given name is translated as defined in config file. If field with translated name
    doesn't exists in DB, function will use predefined constant value from config file. If this 
    option is set to true, function will not use any translation and will not return
    predefined constant in case, that field doesn't exit in DB. This allowes to access 
    data directly
    @retval string content of field, or predefined constant (if exists and directDb is false).
    @retval NULL no field in DB found and also no predefined constant found.
    */
    virtual const char* QueryValue( int shapeIndex, const char* name, bool directDb = false ) = 0;

    ///Result of LoadShapeFile
    enum LoadResult 
    {
            ///Shapefile has been loaded or already has been in memory
      resOk                 = 0,
            ///Shapefile was not loaded because it was not found
      resShapeFileNotExists = 1,
            ///Shapefile was not loaded due load error (but file exists)
      resShapeLoadError     = 2
    };

    ///Loads shapefile into memory.
    /**Function loads shapefile only if the shapefile was not loaded yet
    @param name filename of shapefile. Name should be full specified name, but it
    is allowed to use relative path. In that case, function will use Pathname::GetCWD to
    obtain current path
    @param dbconvert pointer to interface that used to translate column name. It can
    contain NULL (in most cases)
    @param DBFName This parameter can override default DBF file
    @return result of operation
    */
    virtual LoadResult LoadShapeFile( const char* name, Shapes::IDBColumnNameConvert* dbconvert = NULL,
                                      const Pathname& DBFName = Pathname( PathNull ) ) = 0;
    };

//-----------------------------------------------------------------------------

  class ShapeFile
  {
    IShapeDbAccess* m_ifc;
    Array< size_t > m_indexes;

  public:
    ShapeFile( IShapeDbAccess* interfaceIn )
    : m_ifc( interfaceIn )
    , m_indexes( 0 ) 
    {
    }

    int Size() const 
    {
      return (m_indexes.Size());
    }

    const Shapes::IShape* operator [] ( int index ) const
    {
      if ( index >= 0 && index < m_indexes.Size() )
      {
        return (m_ifc->GetShape( m_indexes[index] ));
      }
      else
      {
        return (NULL);
      }
    }

    void Open( const char* name )
    {
      m_indexes = m_ifc->GetShapefileIndexes( name );
    }

    const char* operator () ( int shapeIndex, const char* name, bool directDb = false ) const
    {
      return (m_ifc->QueryValue( shapeIndex, name, directDb ));
    }
  };   

//-----------------------------------------------------------------------------

} // LandBuilder2