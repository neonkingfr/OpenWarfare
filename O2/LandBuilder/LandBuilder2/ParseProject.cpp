//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "ParseProject.h"
#include "../Shapes/ShapeNull.h"

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

  ParseProject::ParseProject()
  {
  }

//-----------------------------------------------------------------------------

  ParseProject::~ParseProject()
  {
  }

//-----------------------------------------------------------------------------

  DoubleRect ParseProject::LoadRectangle( ParamClassPtr cls )
  {
    double left   = 0.0;
    double top    = 0.0;
    double right  = 0.0;
    double bottom = 0.0;

    ParamEntryPtr entry;

    entry = cls->FindEntry( "left" );
    if ( entry.NotNull() ) { left = entry->operator float(); }

    entry = cls->FindEntry( "top" );
    if ( entry.NotNull() ) { top = entry->operator float(); }

    entry = cls->FindEntry( "right" );
    if ( entry.NotNull() ) { right = entry->operator float(); }

    entry = cls->FindEntry( "bottom" );
    if ( entry.NotNull() ) { bottom = entry->operator float(); }

    DoubleRect res( left, top, right, bottom );

    return (res);
  }

//-----------------------------------------------------------------------------

  template < class Functor >
  void LoadTranslationTable( ParamEntryPtr entry, const Functor& funct )
  {
    int sz = entry->GetSize();
    for ( int i = 0; i < sz; ++i )
    {
      RString val = entry->operator []( i ).operator RStringB();
      char* c = val.MutableData();
      char* d = strstr( c, "->" );
      if ( d != 0 )
      {
        *d = 0;
        d += 2;
        funct( d, c );
      }
    }
  }

//-----------------------------------------------------------------------------

  class ColumnNameConvert : public StringMap,  public Shapes::IDBColumnNameConvert
  {
  public:
    virtual const char* TranslateColumnName( const char* name ) 
    {
      StringMap::const_iterator it = find( name );
      if ( it != end() )
      {
        return (it->second.Data());
      }
      else 
      {
        return (NULL);
      }
    }
  };

//-----------------------------------------------------------------------------

  class FillResourceConvertTable
  {
    mutable ParseProject* m_outer;

  public:
    FillResourceConvertTable( ParseProject* outer ) 
    : m_outer( outer ) 
    {
    }

    bool operator () ( const char* a, const char* b ) const 
    {
      m_outer->m_project.GetResourceTranslateTable().insert( std::make_pair( RStringI( a ), RStringI( b ) ) );
      return (false);
    }
  };

//-----------------------------------------------------------------------------

  bool ParseProject::LoadProject( const char* configName )
  {        
    LOGF( Progress ) << "Processing project:" << configName;

    Pathname::SetCWD( Pathname( configName ) );

    LSError err = m_config.Parse( configName );
    if ( err != LSOK )
    {
      LOGF( Fatal ) << "Parse error:" << err;
      return (false);
    }

    ParamEntryPtr entry;
    entry = m_config.FindEntry( "LandBuildConfig" );
    if ( entry.NotNull() && entry->IsClass() )
    {
      ParamClassPtr cfgClass = entry->GetClassInterface();

      entry = cfgClass->FindEntry( "TaskList" );
      if ( entry.NotNull() && entry->IsArray() )
      {
        for ( int i = 0; i < entry->GetSize(); ++i )
        {
          RStringB val = entry->operator []( i );
          char* splt = const_cast< char* >( strchr( val.MutableData(), '@' ) );
          Pathname taskPath( configName );
          Pathname shapePath( configName );
          Pathname dbfName( PathNull );
          if ( !splt )
          {
            taskPath.SetFiletitle( val );
            shapePath.SetFiletitle( val );
          }
          else
          {
            *splt = 0;
            taskPath.SetFiletitle( val );
            splt++;
            if ( *splt ) 
            {
              char* splt2 = const_cast< char* >( strchr( splt, '@' ) );
              if ( splt2 )
              {
                *splt2 = 0;
                splt2++;
                dbfName = configName;
                dbfName.SetFiletitle( splt2 );
                dbfName.SetExtension( ".dbf" );
              }
              shapePath.SetFiletitle( splt );
            }
            else 
            {
              shapePath.SetNull();
            }
          }
          if ( !shapePath.IsNull() ) { shapePath.SetExtension( ".shp" ); }

          LoadTask( taskPath, shapePath, dbfName );
        }
      }

      entry = cfgClass->FindEntry( "MapArea" );
      if ( entry.NotNull() && entry->IsClass() )
      {
        int visitor = 4;
        ParamEntryPtr vis = entry->GetClassInterface()->FindEntry( "visitor" );
        if ( vis.NotNull() ) { visitor = vis->operator int(); }
        if ( visitor != 3 ) { visitor = 4; }

        DoubleRect mapArea = LoadRectangle( entry->GetClassInterface() );

        LOGF( Debug ) << "Loaded mapArea: (" << mapArea.GetLeft() << "," << mapArea.GetTop() << ","
                      << mapArea.GetRight() << "," << mapArea.GetBottom() << ")";

        double segmentSizeX = 0.0;
        double segmentSizeY = 0.0;

        entry = cfgClass->FindEntry( "HighMapGrid" );
        if ( entry.NotNull() && entry->IsClass() )
        {
          ParamEntryPtr x, y;
          x = entry->FindEntry( "x" );
          y = entry->FindEntry( "y" );
          if ( y.IsNull() ) { y = x; }
          if ( x.IsNull() ) { x = y; }
          if ( x.NotNull() && y.NotNull() )
          {
            segmentSizeX = *x;
            segmentSizeY = *y;
          }
        }

        entry = cfgClass->FindEntry( "resourceTransl" );
        if ( entry.NotNull() && entry->IsArray() ) 
        {
          LoadTranslationTable( entry, FillResourceConvertTable( this ) );
        }

        m_project.CutShapesIntoFrame( mapArea );
        m_workspace = new LBWorkspace( mapArea, segmentSizeX, segmentSizeY, visitor );

        return (true);
      }
      else
      {
        LOGF( Fatal ) << "Missing MapArea class";
      }
    } 
    else
    {
      LOGF( Fatal ) << "Missing LandBuildConfig class";
    }
    return (false);
  }

//-----------------------------------------------------------------------------

  class TaskGeneratedFromConfigFile : public IProjectTask
  {
    IBuildModule* m_module;
    StringMap     m_params;
    StringMap     m_colTranslateTable;
    RString       m_shapeName;
    RString       m_taskName;
    RString       m_cwd;

  public:
    TaskGeneratedFromConfigFile() 
    : m_module( NULL ) 
    {
    }

    void AddParam( const RStringI& name, const RString& value ) 
    {
      m_params[name] = value;
    }

    void AddColTranslate( const RStringI& name, const RString& value ) 
    {
      m_colTranslateTable[name] = value;
    }

    void SetModule( IBuildModule* module ) 
    {
      m_module = module;
    }

    void SetCwd( const char* cwd )
    {
      m_cwd = cwd;
    }

    void SetShapeName( const RString& shapeName ) 
    {
      m_shapeName = shapeName;
    }

    void SetTaskName( const RString& taskName ) 
    {
      m_taskName = taskName;
    }

    virtual IBuildModule* GetModule() const 
    {
      return (m_module);
    }

    virtual const char* GetShapeGroupName() const 
    {
      return (m_shapeName);
    }

    virtual const char* GetTaskName() const 
    {
      return (m_taskName);
    }

    virtual const char* GetWorkFolder() const 
    {
      return (m_cwd);
    }

    virtual const char* TranslateColumnName( const char* colName ) const;

    virtual const char* QueryValue( const char* name ) const;
  };

//-----------------------------------------------------------------------------

  const char* TaskGeneratedFromConfigFile::QueryValue( const char* name ) const
  {
    StringMap::const_iterator iter = m_params.find( RStringI( name ) );
    if ( iter == m_params.end() ) { return (NULL); }
    return (iter->second.Data());
  }

//-----------------------------------------------------------------------------

  const char* TaskGeneratedFromConfigFile::TranslateColumnName( const char* name ) const
  {
    StringMap::const_iterator iter = m_colTranslateTable.find( RString( name ) );
    if ( iter == m_colTranslateTable.end() ) 
    {
      return (IProjectTask::TranslateColumnName( name ));
    }
    return (iter->second.Data());
  }

//-----------------------------------------------------------------------------

  namespace Functors
  {
    class LoadConstantParams
    {
      mutable TaskGeneratedFromConfigFile& m_task;

    public:
      LoadConstantParams( TaskGeneratedFromConfigFile& task ) 
      : m_task( task ) 
      {
      }

      bool operator () ( const ParamEntryVal& entry ) const
      {
        m_task.AddParam( entry.GetName(), entry.GetValue() );
        return (false);    
      }
    };

    class FillColumnTranslateTable
    {
      mutable TaskGeneratedFromConfigFile& m_task;

    public:
      FillColumnTranslateTable( TaskGeneratedFromConfigFile& task ) 
      : m_task( task ) 
      {
      }

      bool operator () ( const char* a, const char* b ) const 
      {
        m_task.AddColTranslate( RStringI( a ), RStringI( b ) );
//				m_task.AddColTranslate( RStringI( b ), RStringI( a ) );
        return (false);
      }
    };
  }

//-----------------------------------------------------------------------------

  void ParseProject::LoadTask( const Pathname& configName, const Pathname& shapeName, 
                               const Pathname& dbfName )
  {
    LOGF( Info ) << "loading: " << configName.GetFilename();
    ParamFile taskCfg;
    LSError err = taskCfg.Parse( configName );
    
    if ( err != LSOK )
    {
      LOGF( Error ) << "!Parse error:";
      return;
    }

    ParamEntryPtr entry = taskCfg.FindEntry( "TaskConfig" );
    if ( entry.NotNull() && entry->IsClass() )
    {
      ParamClassPtr tcfg = entry->GetClassInterface();
      entry = tcfg->FindEntry( "module" );
      if ( entry.NotNull() && entry->IsTextValue() )
      {
        RStringB modname = entry->GetValue();

        SRef< TaskGeneratedFromConfigFile > task = new TaskGeneratedFromConfigFile();
        IBuildModule* mod = m_moduleManager.GetModule( modname );
        task->SetCwd( configName.GetDirectoryWithDrive() );
        if ( !mod )
        {
          LOGF( Error ) << "Unknown module:" << modname.Data();
        } 
        else
        {
          const char* shapeGroup = shapeName.GetFilename();
          task->SetModule( mod );
          task->SetShapeName( shapeGroup );
          task->SetTaskName( configName.GetFilename() );                  

          entry = tcfg->FindEntry( "parameters" );
          if ( entry.NotNull() && entry->IsClass() )
          {
            ParamClassPtr params = entry->GetClassInterface();
            params->ForEachEntry( Functors::LoadConstantParams( *task ) );
          }

          entry = tcfg->FindEntry( "columnTransl" );
          if ( entry.NotNull() && entry->IsArray() )
          {
            LoadTranslationTable( entry, Functors::FillColumnTranslateTable( *task ) );
          }

          if ( shapeName.IsNull() )
          {
            //not supported
          }
          else 
          {
            ShapefileDesc* shapeDesc = m_project.GetShapeGroup( shapeGroup );
            if ( !shapeDesc )
            {
              LBProject::Result res = m_project.AddShapeFile( shapeName, shapeGroup, NULL, dbfName );
              if ( res == LBProject::resShapeFileNotExists )
              {
                LOGF( Info ) << "Using virtual shapefile: " << shapeGroup;
              }
              else if ( res == LBProject::resOk )
              {
                LOGF( Info ) << "Loaded shapefile: " << shapeGroup;
              }
              else 
              {
                LOGF( Error ) << "Error load shapefile: " << shapeGroup;             
              }
            }
          }
          m_project.EnqueueTask( task.Unlink(), false );
        }
      }
      else
      {
        LOGF( Error ) << "!missing 'module' entry";
      }
    }
    else
    {
      LOGF( Error ) << "!Each config for shape file must have TaskConfig class";
    }
  }

//-----------------------------------------------------------------------------

  void ParseProject::RunProject() 
  {
    LOGF( Progress + 5 ) << "Starting generator";
    m_project.RunProject( m_workspace );
    LOGF( Progress + 5 ) << "Generator finished";    
  }

//-----------------------------------------------------------------------------

  bool ParseProject::SaveResult( const char* targetName )
  {
    LOGF( Progress + 5 ) << "Saving result to file: " << targetName;
    if ( m_workspace->SaveResult( targetName ) )    
    {
      LOGF( Progress + 5 ) << "Done";    
      return (false);
    }
    else
    {
      LOGF( Error ) << "Failed to save result:" << targetName;        
      return (true);
    }
  }

//-----------------------------------------------------------------------------

} // namespace LandBuilder2

