//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include ".\math3dD.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{
	class ObjectDesc
	{
		RStringI      m_name;
		Matrix4D      m_position;
		unsigned long m_tag;

	public:
		ObjectDesc() 
		{
		}

		ObjectDesc( const RStringI& name, const Matrix4D& position, unsigned long tag )
		: m_name( name )
		, m_position( position )
		, m_tag( tag ) 
		{
		}

		const RStringI& GetName() const 
		{
			return (m_name);
		}

		const Matrix4D& GetPositionMatrix() const 
		{
			return (m_position);
		}

    const Vector3D& GetPositionVector() const 
		{
			return (m_position.Position());
		}

		int IndexByName( const ObjectDesc& other ) const
		{
			return (_stricmp( m_name, other.m_name ));
		}

		int IndexByXPos( const ObjectDesc& other ) const
		{
			return ((m_position.Position().X() > other.m_position.Position().X()) - (m_position.Position().X() < other.m_position.Position().X()));
		}

		int IndexByYPos( const ObjectDesc& other ) const
		{
			return ((m_position.Position().Y() > other.m_position.Position().Y()) - (m_position.Position().Y() < other.m_position.Position().Y()));
		}

		int IndexByTag( const ObjectDesc& other ) const
		{
			return ((m_tag > other.m_tag) - (m_tag < other.m_tag));
		}

		unsigned long GetTag() const 
		{
			return (m_tag);
		}

		ClassIsMovable( ObjectDesc );
	};

//-----------------------------------------------------------------------------

} // namespace LandBuildInt
