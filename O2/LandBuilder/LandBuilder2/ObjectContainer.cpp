//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include ".\objectcontainer.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{

//-----------------------------------------------------------------------------

	RStringI ObjectContainer::ShareString( const RStringI& str )
	{
		RStringI* fnd = m_stringTable.Find( str );
		if ( !fnd ) 
		{
			m_stringTable.Add( str );
			return (str);
		}
		else
		{
			return (*fnd);
		}
	}

//-----------------------------------------------------------------------------

	RStringI ObjectContainer::ShareString( const RStringI& str ) const
	{
		RStringI* fnd = m_stringTable.Find( str );
		if ( !fnd ) 
		{
			return (str);
		}
		else
		{
			return (*fnd);
		}
	}

//-----------------------------------------------------------------------------

	ObjectContainer::ObjectContainer() 
  : m_idxXPos( 16 )
  , m_idxYPos( 16 )
  , m_idxName( 16 )
  , m_stringTable( 16 )
	{
		m_idxName.SetOwner( &m_objects );
		m_idxXPos.SetOwner( &m_objects );
		m_idxYPos.SetOwner( &m_objects );
	}

//-----------------------------------------------------------------------------

	ObjectContainer::~ObjectContainer()
	{
	}

//-----------------------------------------------------------------------------

	void ObjectContainer::IndexItem( int id )
	{    
		id = id << 1;
		m_idxName.Add( id );
		m_idxXPos.Add( id );
		m_idxYPos.Add( id );
	}

//-----------------------------------------------------------------------------

	int ObjectContainer::Add( const ObjectDesc& object )
	{
		int id = m_objects.Add( ObjectDesc( ShareString( object.GetName() ), object.GetPositionMatrix(), 
                                        object.GetTag() ) );
		IndexItem( id );
		return (id);
	}

//-----------------------------------------------------------------------------

	void ObjectContainer::ReIndex()
	{
		m_idxName.Clear();
		m_idxXPos.Clear();
		m_idxYPos.Clear();
		for ( int i = 0, cnt = m_objects.Size(); i < cnt; ++i )
		{
      if ( m_objects.IsIdValid( i ) ) { IndexItem( i ); }
		}
	}

//-----------------------------------------------------------------------------

	bool ObjectContainer::Delete( int id )
	{
//        printf("Delete object: %d\n",id);
		int arrid = id << 1;
		m_idxName.Remove( arrid );
		m_idxXPos.Remove( arrid );
		m_idxYPos.Remove( arrid );
		return (m_objects.Delete( id ));
	}

//-----------------------------------------------------------------------------

	static int TempID( ObjectDesc* obj )
	{
		int id = (int)obj;
		id |= 1;
		return (id);
	}

//-----------------------------------------------------------------------------

	const ObjectDesc* ObjectContainer::NearestOnX( double x, double y, size_t tag, 
                                                 const Array< size_t >& ignore ) const
	{
		SearchObj refPos( x, y );
		double best = DBL_MAX;
		const ObjectDesc* bestObj = NULL;
		BTreeIterator< int, IndexByXPos > fwd( m_idxXPos );
		fwd.BeginFrom( TempID( &refPos ) );
		BTreeIterator< int, IndexByXPos > bkw = fwd;
		int* fnd;
		while ( (fnd = fwd.Next()) != 0 )
		{
			const ObjectDesc* obj = m_idxXPos.ConvIdToObject( *fnd );
			if ( tag == 0 || obj->GetTag() == tag )
			{
				double cx = obj->GetPositionVector()[0];
				double cy = obj->GetPositionVector()[1];
				double dx = cx - x;
				double dy = cy - y;
        if ( fabs( dx ) > best ) { break; }
				double sz = sqrt( dx * dx + dy * dy );
				if ( sz < best ) 
				{          
					for ( int i = 0, cnt = ignore.Size(); i < cnt; ++i )
					{
            if ( ignore[i] == fnd[0] >> 1 ) { goto skip; }
					}
					best = sz;
					bestObj = obj;
skip:;
				}
			}
		}

		bkw.Next();
		while ( (fnd = bkw.Previous()) != 0 )
		{
			const ObjectDesc* obj = m_idxXPos.ConvIdToObject( *fnd );
			if ( tag == 0 || obj->GetTag() == tag )
			{
				double cx = obj->GetPositionVector()[0];
				double cy = obj->GetPositionVector()[1];
				double dx = cx - x;
				double dy = cy - y;
        if ( fabs( dx ) > best ) { break; }
				double sz = sqrt( dx * dx + dy * dy );
				if ( sz < best ) 
				{
					for ( int i = 0, cnt = ignore.Size(); i < cnt; ++i )
					{
            if ( ignore[i] == fnd[0] >> 1 ) { goto skip2; }
					}
					best = sz;
					bestObj = obj;
skip2:;
				}
			}
		}
		return (bestObj);
	}

//-----------------------------------------------------------------------------

	const ObjectDesc* ObjectContainer::NearestOnY( double x, double y, size_t tag, 
                                                 const Array< size_t >& ignore ) const
	{
		SearchObj refPos( x, y );
		double best = DBL_MAX;
		const ObjectDesc* bestObj = NULL;
		BTreeIterator< int, IndexByYPos > fwd( m_idxYPos );
		fwd.BeginFrom( TempID( &refPos ) );
		BTreeIterator< int, IndexByYPos > bkw = fwd;
		int* fnd;
		while ( (fnd = fwd.Next()) != 0 )
		{
			const ObjectDesc* obj = m_idxYPos.ConvIdToObject( *fnd );
			double cx = obj->GetPositionVector()[0];
			double cy = obj->GetPositionVector()[1];
			double dx = cx - x;
			double dy = cy - y;
      if ( fabs( dy ) > best ) { break; }
			double sz = sqrt( dx * dx + dy * dy );
			if ( sz < best ) 
			{
				best = sz;
				bestObj = obj;
			}
		}
		bkw.Next();
		while ( (fnd = bkw.Previous()) != 0 )
		{
			const ObjectDesc* obj = m_idxYPos.ConvIdToObject( *fnd );
			double cx = obj->GetPositionVector()[0];
			double cy = obj->GetPositionVector()[1];
			double dx = cx - x;
			double dy = cy - y;
      if ( fabs( dy ) > best ) { break; }
			double sz = sqrt( dx * dx + dy * dy );
			if ( sz < best ) 
			{
				best = sz;
				bestObj = obj;
			}
		}
		return (bestObj);
	}

//-----------------------------------------------------------------------------

	int ObjectContainer::GetObjectID( const ObjectDesc* obj ) const
	{
		int k = m_objects.GetObjectIndex( obj );
    if ( !m_objects.IsIdValid( k ) ) { return (-1); }
		return (k);
	}

//-----------------------------------------------------------------------------

	bool ObjectContainer::SetObjectName( int ID, const RStringI& name )
	{
		if ( m_objects.IsIdValid( ID ) )
		{
			m_objects[ID] = ObjectDesc( ShareString( name ), m_objects[ID].GetPositionMatrix(), 
                                  m_objects[ID].GetTag() );
			return (true);
		}
		else
		{
			return (false);
		}
	}

//-----------------------------------------------------------------------------

	SRef< AutoArray< const ObjectDesc* > > ObjectContainer::ObjectsInBox( const Shapes::DBox& box, 
                                                                        size_t tag ) const
	{
		int left, right, top, bottom;
		left   = m_idxXPos.RankOf( TempID( &SearchObj( box.lo.x, 0 ) ) );
		right  = m_idxXPos.RankOf( TempID( &SearchObj( box.hi.x, 0 ) ) );
		top    = m_idxYPos.RankOf( TempID( &SearchObj( 0, box.lo.y ) ) );
		bottom = m_idxYPos.RankOf( TempID( &SearchObj( 0, box.hi.y ) ) );

		AutoArray< const ObjectDesc* >* ret = new AutoArray< const ObjectDesc* >;

		if ( right - left < bottom - top )
		{
			BTreeIterator< int, IndexByXPos > iter( m_idxXPos );
			iter.BeginFrom( *m_idxXPos.ItemWithRank( left ) );
			int* fnd;
			while ( (fnd = iter.Next()) != 0 )        
			{
				const ObjectDesc* obj = m_idxXPos.ConvIdToObject( *fnd );
				if ( tag == 0 || obj->GetTag() == tag )
				{
					const Vector3D& vx = obj->GetPositionVector();
          if ( vx[0] >= box.hi.x ) { break; }
          if ( vx[1] >= box.lo.y && vx[1] < box.hi.y ) { ret->Add( obj ); }
				}
			}
		}
		else
		{
			BTreeIterator< int, IndexByYPos > iter( m_idxYPos );
			iter.BeginFrom( *m_idxYPos.ItemWithRank( top ) );
			int* fnd;
			while ( (fnd = iter.Next()) != 0 )        
			{
				const ObjectDesc* obj = m_idxYPos.ConvIdToObject( *fnd );
				if ( tag == 0 || obj->GetTag() == tag )
				{
					const Vector3D& vx = obj->GetPositionVector();
          if ( vx[1] >= box.hi.y ) { break; }
          if ( vx[0] >= box.lo.x && vx[0] < box.hi.x ) { ret->Add( obj ); }
				}
			}
		}
		return (ret);
	}

//-----------------------------------------------------------------------------

	const ObjectDesc* ObjectContainer::Nearest( double x, double y, size_t tag, const Array< size_t >& ignore ) const
	{
    if ( m_objects.Size() == 0 ) { return (NULL); }
		int rankx = m_idxXPos.RankOf( TempID( &SearchObj( x, y, tag ) ) );
		int ranky = m_idxYPos.RankOf( TempID( &SearchObj( x, y, tag ) ) );
    if ( rankx >= m_idxXPos.Size() ) { rankx = m_idxXPos.Size() - 1; }
    if ( ranky >= m_idxYPos.Size() ) { ranky = m_idxYPos.Size() - 1; }

		const ObjectDesc* obj1 = m_idxXPos.ConvIdToObject( *m_idxXPos.ItemWithRank( rankx ) );
		const ObjectDesc* obj2 = m_idxYPos.ConvIdToObject( *m_idxYPos.ItemWithRank( ranky ) );
		Vector3D vx( x, y, 0.0 );
		if ( obj1->GetPositionVector().Distance2( vx ) < obj2->GetPositionVector().Distance2( vx ) )
		{
			return (NearestOnX( x, y, tag, ignore ));
		}
		else
		{
			return (NearestOnY( x, y, tag, ignore ));
		}
	}

//-----------------------------------------------------------------------------

	bool ObjectContainer::Replace( int id, const ObjectDesc& object )
	{
    if ( !IsValidObjectID( id ) ) { return (false); }
		int arrid = TempID( &m_objects[id] );
		m_idxName.Remove( arrid );
		m_idxXPos.Remove( arrid );
		m_idxYPos.Remove( arrid );
		m_objects[id] = object;
		IndexItem( id );
		return (true);
	}

//-----------------------------------------------------------------------------

} // namespace LandBuildInt