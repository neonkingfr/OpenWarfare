//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "IBuildModule.h"
#include "IProjectTask.h"
#include <set>
#include <queue>

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

	class LBTaskQueue
	{
		std::set< IProjectTask* >   m_tasks;
		std::queue< IProjectTask* > m_queue;

	public:    
		~LBTaskQueue() 
		{
			Clear();    
		}

		void Clear() 
		{
			for ( std::set< IProjectTask* >::iterator it = m_tasks.begin(); it != m_tasks.end(); ++it )
			{
				(*it)->Release();
			}
		}

		void AddTask( IProjectTask* task )
		{
			m_tasks.insert( task );
		}

		void EnqueueTask( IProjectTask* task )
		{
			m_queue.push( task );
		}

		IProjectTask* GetNextTask()
		{
      if ( m_queue.empty() ) { return (NULL); }
			IProjectTask* res = m_queue.front();
			m_queue.pop();
			return (res);
		}

		class ICombiner 
		{
		public:     
			virtual IMainCommands* getCommandInterface( IProjectTask* task ) = 0;
		};

		void ForAllTasks( void (IBuildModule::*fn)( IMainCommands* cmds ), ICombiner& combiner ) 
		{
			for ( std::set< IProjectTask* >::iterator it = m_tasks.begin(); it != m_tasks.end(); ++it )
			{
				IBuildModule* mod = (*it)->GetModule();
				IMainCommands* cmds = combiner.getCommandInterface( *it );
				(mod->*fn)( cmds );
			}
		}

		bool AnyTask() const 
		{
			return (!m_queue.empty());
		}
	};

//-----------------------------------------------------------------------------

} // namespace LandBuilder2