//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "IBuildModule.h"
#include "PluginManager.h"
#include <map>

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

	using namespace LandBuildInt;

//-----------------------------------------------------------------------------

	class ModuleManager
	{
		std::map< RStringI, IBuildModule* > m_moduleMap;
		PluginManager                       m_pluginManager;

	public:
		ModuleManager();
		~ModuleManager();

		IBuildModule* GetModule( const RStringI& name );

    void AddModulePaths( const std::set< RStringI >& path )
    {
			m_pluginManager.AddModulePaths( path );
		}
	};

//-----------------------------------------------------------------------------

} // namespace LandBuilder2

//-----------------------------------------------------------------------------
