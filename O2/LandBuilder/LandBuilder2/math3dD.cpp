//-----------------------------------------------------------------------------

#include "StdAfx.h"

#include "math3dD.h"

//-----------------------------------------------------------------------------

const Vector3D VZeroD( 0.0, 0.0, 0.0 ) INIT_PRIORITY_HIGH;
const Vector3D VUpD( 0.0, 1.0, 0.0 ) INIT_PRIORITY_HIGH;
const Vector3D VForwardD( 0.0, 0.0, 1.0 ) INIT_PRIORITY_HIGH;
const Vector3D VAsideD( 1.0, 0.0, 0.0 ) INIT_PRIORITY_HIGH;

const Matrix3D M3ZeroD ( 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ) INIT_PRIORITY_HIGH;
const Matrix3D M3IdentityD( 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 ) INIT_PRIORITY_HIGH;
const Matrix4D M4ZeroD( 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 ) INIT_PRIORITY_HIGH;
const Matrix4D M4IdentityD( 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0 ) INIT_PRIORITY_HIGH;

//-----------------------------------------------------------------------------

bool Matrix4D::IsFinite() const
{
  for ( int i=0; i<3; ++i )
  {
    if ( !_finite(Get(i,0)) ) return false;
    if ( !_finite(Get(i,1)) ) return false;
    if ( !_finite(Get(i,2)) ) return false;
    if ( !_finite(GetPos(i)) ) return false;
  }
  return true;
}

//-----------------------------------------------------------------------------

bool Matrix3D::IsFinite() const
{
  for ( int i=0; i<3; ++i )
  {
    if ( !_finite(Get(i,0)) ) return false;
    if ( !_finite(Get(i,1)) ) return false;
    if ( !_finite(Get(i,2)) ) return false;
  }
  return true;
}

//-----------------------------------------------------------------------------

double Matrix4D::Characteristic() const
{
  // used in fast comparison
  // sum of all data members
  double sum1=0.0; // lower dependecies
  double sum2=0.0;
  for ( int i=0; i<3; ++i )
  {
    sum1+=Get(i,0);
    sum2+=Get(i,1);
    sum1+=Get(i,2);
    sum2+=GetPos(i);
  }
  return sum1+sum2;
}

//-----------------------------------------------------------------------------

void Matrix4D::SetIdentity()
{
  *this = M4IdentityD;
}

//-----------------------------------------------------------------------------

void Matrix4D::SetZero()
{
  *this = M4ZeroD;
}

//-----------------------------------------------------------------------------

void Matrix4D::SetTranslation( Vector3DPar offset )
{
  SetIdentity();
  SetPosition( offset );
}

//-----------------------------------------------------------------------------

void Matrix4D::SetRotationX( double angle )
{
  SetIdentity();
  double s = sin( angle ), c = cos( angle );
  Set( 1, 1 ) = +c, Set( 1, 2 ) = -s;
  Set( 2, 1 ) = +s, Set( 2, 2 ) = +c;
}

//-----------------------------------------------------------------------------

void Matrix4D::SetRotationY( double angle )
{
  SetIdentity();
  double s = sin( angle ), c = cos( angle );
  Set( 0, 0 ) = +c, Set( 0, 2 ) = -s;
  Set( 2, 0 ) = +s, Set( 2, 2 ) = +c;
}

//-----------------------------------------------------------------------------

void Matrix4D::SetRotationZ( double angle )
{
  SetIdentity();
  double s = sin( angle ),c = cos( angle );
  Set( 0, 0 ) = +c, Set( 0, 1 ) = -s;
  Set( 1, 0 ) = +s, Set( 1, 1 ) = +c;
}

//-----------------------------------------------------------------------------

void Matrix4D::SetRotationY( double s, double c )
{
  SetIdentity();
  Set( 0, 0 ) = +c, Set( 0, 2 ) = -s;
  Set( 2, 0 ) = +s, Set( 2, 2 ) = +c;
}

//-----------------------------------------------------------------------------

void Matrix4D::SetScale( double x, double y, double z )
{
  SetZero();
  Set( 0, 0 ) = x;
  Set( 1, 1 ) = y;
  Set( 2, 2 ) = z;
}

//-----------------------------------------------------------------------------

void Matrix4D::SetPerspective( double cLeft, double cTop )
{
  SetZero();
  Set( 0, 0 ) = 1.0 / cLeft;
  Set( 1, 1 ) = 1.0 / cTop;
  
  Set( 2, 2 ) = 1.0; // DirectX has Q here, Q = zFar/(zFar-zNear)

  SetPos( 2 ) = -1; // DirectX has -Q/zNear here
}

//-----------------------------------------------------------------------------

void Matrix4D::Orthogonalize()
{
  Vector3DVal dir = Direction();
  Vector3DVal up  = DirectionUp();
  SetDirectionAndUp( dir, up );
}

//-----------------------------------------------------------------------------

void Matrix3D::Orthogonalize()
{
  Vector3DVal dir = Direction();
  Vector3DVal up  = DirectionUp();
  SetDirectionAndUp( dir, up );
}

//-----------------------------------------------------------------------------

void Matrix4D::SetOriented( Vector3DPar dir, Vector3DPar up )
{
  SetDirectionAndUp( dir, up );
  SetPosition( VZeroD );
}

//-----------------------------------------------------------------------------

void Matrix4D::SetView( Vector3DPar point, Vector3DPar dir, Vector3DPar up )
{
  Matrix4D translate( MTranslation, -point );
  Matrix4D direction( MDirection, dir, up );
  SetMultiply( direction, translate );
}

//-----------------------------------------------------------------------------

void Matrix4D::SetAdd( const Matrix4D& a, const Matrix4D& b )
{
  m_orientation.m_aside = a.m_orientation.m_aside + b.m_orientation.m_aside;
  m_orientation.m_up    = a.m_orientation.m_up    + b.m_orientation.m_up;
  m_orientation.m_dir   = a.m_orientation.m_dir   + b.m_orientation.m_dir;
  m_position = a.m_position + b.m_position;
}

//-----------------------------------------------------------------------------

void Matrix4D::SetMultiply( const Matrix4D& a, double b )
{
  m_orientation.m_aside = a.m_orientation.m_aside * b;
  m_orientation.m_up    = a.m_orientation.m_up * b;
  m_orientation.m_dir   = a.m_orientation.m_dir * b;
  m_position = a.m_position * b;
}

//-----------------------------------------------------------------------------

void Matrix4D::AddMultiply( const Matrix4D& a, double b )
{
  m_orientation.m_aside += a.m_orientation.m_aside * b;
  m_orientation.m_up    += a.m_orientation.m_up * b;
  m_orientation.m_dir   += a.m_orientation.m_dir * b;
  m_position += a.m_position * b;
}

//-----------------------------------------------------------------------------

void Matrix4D::SetMultiplyByPerspective( const Matrix4D& a, const Matrix4D& b )
{
  // matrix multiplication
  int i, j;
  for( i = 0; i < 3; ++i ) 
  {
    for( j = 0; j < 3; ++j )
    {
      Set( i, j )= ( a.Get( i, 0 ) * b.Get( 0, j )+
                     a.Get( i, 1 ) * b.Get( 1, j )+
                     a.Get( i, 2 ) * b.Get( 2, j )+
                     a.GetPos( i ) * ( j==2 ) );
    }
  }

  for ( i = 0; i < 3; ++i )
  {
    SetPos( i )= ( a.Get( i, 0 ) * b.GetPos( 0 ) +
                   a.Get( i, 1 ) * b.GetPos( 1 ) +
                   a.Get( i, 2 ) * b.GetPos( 2 ) );
  }
}

//-----------------------------------------------------------------------------

inline double InvSquareSize( double x, double y, double z )
{
  return (Inv( x * x + y * y + z * z ));
}

//-----------------------------------------------------------------------------

Vector3D Vector3D::Normalized() const
{
  double size2 = SquareSizeInline();
  if ( size2 <= DBL_MIN ) { return (*this); }
  double invSize = InvSqrtD( size2 );
  return (Vector3D( X() * invSize, Y() * invSize, Z() * invSize ));
}

//-----------------------------------------------------------------------------

void Vector3D::Normalize() // no return to avoid using instead of Normalized
{  
  double size2 = SquareSizeInline();
  if ( size2 <= DBL_MIN ) { return; }
  double invSize = InvSqrtD( size2 );
  Set( 0 ) *= invSize, Set( 1 ) *= invSize, Set( 2 ) *= invSize;
}

//-----------------------------------------------------------------------------

double Vector3D::NormalizeSize()
{
  double size2 = SquareSizeInline();
  if ( size2 <= DBL_MIN ) { return (0.0); }
  double size = sqrt( size2 );
  double invSize = 1.0 / size;
  Set( 0 ) *= invSize, Set( 1 ) *= invSize, Set( 2 ) *= invSize;
  return (size);
}

//-----------------------------------------------------------------------------

double Vector3D::CosAngle( Vector3DPar op ) const
{
  double denom2 = op.SquareSizeInline() * SquareSizeInline();
  if ( denom2 <= 0.0 ) { return (0.0); }
  return (DotProduct( op ) * InvSqrtD( denom2 ));
}

//-----------------------------------------------------------------------------

double Vector3D::SinAngle( Vector3DPar op, Vector3DPar worldsTop ) const
{
  double denom2 = op.SquareSizeInline() * SquareSizeInline();
  if ( denom2 <= 0.0 ) { return (0.0); }
  Vector3D xprod = CrossProduct( op );
  double sz = xprod.Size();
  if ( xprod.DotProduct( worldsTop ) < 0 ) { sz = -sz; }
  return (sz * InvSqrtD( denom2 ));
}

//-----------------------------------------------------------------------------

double Vector3D::Distance( Vector3DPar op ) const
{
  return ((*this - op).Size());
}

//-----------------------------------------------------------------------------

Vector3D Vector3D::Project( Vector3DPar op ) const
{
  return (op * DotProduct( op ) * op.InvSquareSize());
}

//-----------------------------------------------------------------------------

bool VerifyFloat( double x );

//-----------------------------------------------------------------------------

bool Vector3D::IsFinite() const
{
  if ( !_finite( Get( 0 ) ) ) { return (false); }
  if ( !_finite( Get( 1 ) ) ) { return (false); }
  if ( !_finite( Get( 2 ) ) ) { return (false); }
  return (true);
}

//-----------------------------------------------------------------------------

void Vector3D::SetMultiplyLeft( Vector3DPar o, const Matrix3D& a )
{ // vector rotation - only 3x3 matrix is used, translation is ignored
  // u=M*v
  double o0 = o[0], o1 = o[1], o2 = o[2];
  Set( 0 ) = a.Get( 0, 0 ) * o0 + a.Get( 1, 0 ) * o1 + a.Get( 2, 0 ) * o2;
  Set( 1 ) = a.Get( 0, 1 ) * o0 + a.Get( 1, 1 ) * o1 + a.Get( 2, 1 ) * o2;
  Set( 2 ) = a.Get( 0, 2 ) * o0 + a.Get( 1, 2 ) * o1 + a.Get( 2, 2 ) * o2;
}

//-----------------------------------------------------------------------------

#define MySqrEps      1.e-5f
#define MyEps         1.e-3f
#define AbsZero(x)    (x < MyEps)
#define AbsSqrZero(x) (x < MySqrEps)

//-----------------------------------------------------------------------------

double Vector3D::SetSolve2x2( double a, double b, double c,
                              double d, double e, double f )
{
  double det01 = a * e - b * d;
  double det02 = a * f - c * d;
  double det12 = b * f - c * e;
  double ad01 = fabs(det01);
  double ad02 = fabs(det02);
  double ad12 = fabs(det12);
  double inv;
  if ( ad01 >= ad02 &&
       ad01 >= ad12 )
  {
    if ( AbsZero(ad01) ) return -1.0;
      // result[2] is free variable
    inv = 1.0 / det01;
    Set(0) = -det12 * inv;
    Set(1) =  det02 * inv;
    Set(2) = -1.0;
    return ad01;
  }
  if ( ad02 >= ad01 &&
       ad02 >= ad12 )
  {
    if ( AbsZero(ad02) ) return -1.0;
      // result[1] is free variable
    inv = 1.0 / det02;
    Set(0) =  det12 * inv;
    Set(1) = -1.0;
    Set(2) =  det01 * inv;
    return ad02;
  }
  if ( AbsZero(ad12) ) return -1.0;
    // result[0] is free variable
  inv = 1.0 / det12;
  Set(0) = -1.0;
  Set(1) =  det02 * inv;
  Set(2) = -det01 * inv;
  return ad12;
}

//-----------------------------------------------------------------------------

#define IsOne(x)  ( (x) > 1.0-MySqrEps && (x) < 1.0+MySqrEps )

//-----------------------------------------------------------------------------

bool Vector3D::AllOnes() const
{
  return( IsOne(Get(0)) &&
          IsOne(Get(1)) &&
          IsOne(Get(2)) );
}

//-----------------------------------------------------------------------------

double Vector3D::SetEigenVector( double a, double b, double c,
                                 double d, double e, double f )
{
    // try rows 0 and 1:
  double reliability = SetSolve2x2(a,b,c,b,d,e);
  if ( reliability > 0.0 ) return reliability;
    // try rows 1 and 2:
  return SetSolve2x2(b,d,e,c,e,f);
}

//-----------------------------------------------------------------------------

void Matrix4D::SetInvertRotation( const Matrix4D& op )
{
  // invert orientation
  m_orientation.SetInvertRotation( op.Orientation() );
  // invert translation
  SetPosition(Rotate(-op.Position()));
}

//-----------------------------------------------------------------------------

void Matrix4D::SetInvertScaled( const Matrix4D& op )
{
  // invert orientation
  m_orientation.SetInvertScaled(op.Orientation());
  // invert translation
  Vector3D pos;
  pos.SetMultiply(Orientation(),-op.Position());
  SetPosition(pos);
}

//-----------------------------------------------------------------------------

void Matrix4D::SetInvertGeneral( const Matrix4D& op )
{
  // invert orientation
  m_orientation.SetInvertGeneral(op.Orientation());
  // invert translation
  SetPosition(Rotate(-op.Position()));
}

//-----------------------------------------------------------------------------

void Matrix3D::SetNormalTransform( const Matrix3D& op )
{
  // normal transformation for scale matrix (a,b,c) is (1/a,1/b,1/c)
  // all matrices we use are rotation combined with scale
  SetIdentity();
  int j;
  double invRow0size2=InvSquareSize(op.Get(0,0),op.Get(0,1),op.Get(0,2));
  double invRow1size2=InvSquareSize(op.Get(1,0),op.Get(1,1),op.Get(1,2));
  double invRow2size2=InvSquareSize(op.Get(2,0),op.Get(2,1),op.Get(2,2));
  for( j=0; j<3; j++ )
  {
    Set(0,j)=op.Get(0,j)*invRow0size2;
    Set(1,j)=op.Get(1,j)*invRow1size2;
    Set(2,j)=op.Get(2,j)*invRow2size2;
  }
}

//-----------------------------------------------------------------------------

// implementation of 3x3 matrices

void Matrix3D::SetIdentity()
{
  *this=M3IdentityD;
}

//-----------------------------------------------------------------------------

void Matrix3D::SetZero()
{
  *this=M3ZeroD;
}

//-----------------------------------------------------------------------------

void Matrix3D::SetRotationX( double angle )
{
  double s = sin(angle),c=cos(angle);
  SetIdentity();
  Set(1,1)=+c,Set(1,2)=-s;
  Set(2,1)=+s,Set(2,2)=+c;
}

//-----------------------------------------------------------------------------

void Matrix3D::SetRotationY( double angle )
{
  double s=sin(angle),c=cos(angle);
  SetIdentity();
  Set(0,0)=+c,Set(0,2)=-s;
  Set(2,0)=+s,Set(2,2)=+c;
}

//-----------------------------------------------------------------------------

void Matrix3D::SetRotationZ( double angle )
{
  double s = sin(angle), c = cos(angle);
  SetIdentity();
  Set(0,0)=+c,Set(0,1)=-s;
  Set(1,0)=+s,Set(1,1)=+c;
}

//-----------------------------------------------------------------------------

void Matrix3D::SetRotationAxis( Vector3DPar axis, double angle )
{
  // Normalize input vector
  Vector3D axisN = axis;
  axisN.Normalize();
  
  // Convert axis and angle into a quaternion (w, x, y, z)
  double halfAngle = angle * 0.5f;
  double w = cos(halfAngle);
  double sinHalfAngle = sin(halfAngle);
  double x = sinHalfAngle * axisN.X();
  double y = sinHalfAngle * axisN.Y();
  double z = sinHalfAngle * axisN.Z();

  // Convert the quaternion into the matrix
  double wx = w*x*2;
  double wy = w*y*2;
  double wz = w*z*2;
  double xx = x*x*2;
  double xy = x*y*2;
  double xz = x*z*2;
  double yy = y*y*2;
  double yz = y*z*2;
  double zz = z*z*2;

  Set(0, 0) = 1 - yy - zz;
  Set(0, 1) = xy - wz;
  Set(0, 2) = xz + wy;
  Set(1, 0) = xy + wz;
  Set(1, 1) = 1 - xx - zz;
  Set(1, 2) = yz - wx;
  Set(2, 0) = xz - wy;
  Set(2, 1) = yz + wx;
  Set(2, 2) = 1 - xx - yy;
}

//-----------------------------------------------------------------------------

void Matrix3D::SetScale( double x, double y, double z )
{
  SetZero();
  Set(0,0)=x;
  Set(1,1)=y;
  Set(2,2)=z;
}

//-----------------------------------------------------------------------------

void Matrix3D::SetScale( double scale )
{
  Assert(_finite(scale));
  // note: old scale may be zero
  double invOldScale=InvScale();
  if( invOldScale>0 )
  {
    (*this)*=scale*invOldScale;
  }
  else
  {
    SetScale(scale,scale,scale);
  }
  // note: any SetDirection will remove scale
}

//-----------------------------------------------------------------------------

/** Calculates average scale. There can exist vectors with bigger scale.*/
double Matrix3D::Scale2() const
{
  // Frame transformation is composed from
  // rotation*scale
  // current scale can be determined as
  // orient * transpose orient

  //Matrix3P invR=InverseRotation();
  //Matrix3P oTo=(*this)*invR;
  // note all but [i][i] should be zero
  //float sx2=oTo(0,0);
  //float sy2=oTo(1,1);
  //float sz2=oTo(2,2);

  // optimized matrix transposition + multiplication
  Vector3D sv;
  for( int i=0; i<3; i++ )
  {
    sv[i]=SquareD(Get(i,0))+SquareD(Get(i,1))+SquareD(Get(i,2));
  }

  // calculate average scale
  return (sv[0]+sv[1]+sv[2])*(1.0/3);
}

//-----------------------------------------------------------------------------

/** Calculates estimation of squared maximal possible scale. 
  * The correct squared maximal possible scale is equal or lower than this estimation. The values are equal for orthogonal 
  * matrices. 
  */
double Matrix3D::MaxScale2() const
{
  /// estimation sqrt(max_{i = 0..2} (|S_i|^2) + 2 * max_{i,j = 0..2, i != j} abs(S_i * S_j)) where S_i is i-th column
  double maxS = m_aside.SquareSize(); // maximal column size
  saturateMaxD( maxS, m_dir.SquareSize() );
  saturateMaxD( maxS, m_up.SquareSize() );

  double maxSS = fabs(m_aside * m_dir); //maximal in-between column product
  saturateMaxD( maxSS, fabs( m_aside * m_up ) );
  saturateMaxD( maxSS, fabs( m_dir * m_up ) );

  return maxS + 2 * maxSS;
}

//-----------------------------------------------------------------------------

void Matrix3D::ScaleAndMaxScale2( double& scale, double& maxScale ) const
{
  /// estimation max scale sqrt(max_{i = 0..2} (|S_i|^2) + 2 * max_{i,j = 0..2, i != j} abs(S_i * S_j)) where S_i is i-th column   
  scale = maxScale = m_aside.SquareSize(); // maximal column size   
  double temp = m_dir.SquareSize();
  saturateMaxD( maxScale, temp );
  scale += temp;
  temp = m_up.SquareSize();
  saturateMaxD( maxScale, temp );
  scale += temp;
  scale *= 1.0/3.0;

  double maxSS = fabs(m_aside * m_dir); //maximal in-between column product
  saturateMaxD( maxSS, fabs( m_aside * m_up ) );
  saturateMaxD( maxSS, fabs( m_dir * m_up ) );
  maxScale += 2 * maxSS;
}

//-----------------------------------------------------------------------------

void Matrix3D::SetDirectionAndUp( Vector3DPar dir, Vector3DPar up )
{
  SetDirection(dir.Normalized());
  // Project into the plane
  double t=up*Direction();
  SetDirectionUp((up-Direction()*t).Normalized());
  // Calculate the vector pointing along the x axis (i.e. aside)
  // sign is experimental - I never know what is right and what is left
  // no need to normalize - cross product of two perpendicular unit vectors is unit vector
  SetDirectionAside(DirectionUp().CrossProduct(Direction()));
}

//-----------------------------------------------------------------------------

void Matrix3D::SetDirectionAndAside( Vector3DPar dir, Vector3DPar aside )
{
  SetDirection(dir.Normalized());
  // Project into the plane
  double t=aside*Direction();
  SetDirectionAside((aside-Direction()*t).Normalized());
  // Calculate the vector pointing along the x axis (i.e. aside)
  // sign is experimental - I never know what is right and what is left
  // no need to normalize - cross product of two perpendicular unit vectors is unit vector
  SetDirectionUp(Direction().CrossProduct(DirectionAside()));
}

//-----------------------------------------------------------------------------

void Matrix3D::SetUpAndAside( Vector3DPar up, Vector3DPar aside )
{
  SetDirectionUp(up.Normalized());
  // Project into the plane
  double t=DirectionUp()*aside;
  SetDirectionAside((aside-DirectionUp()*t).Normalized());
  // Calculate the vector pointing along the x axis (i.e. aside)
  // sign is experimental - I never know what is right and what is left
  // no need to normalize - cross product of two perpendicular unit vectors is unit vector
  SetDirection(DirectionAside().CrossProduct(DirectionUp()));
}

//-----------------------------------------------------------------------------

void Matrix3D::SetUpAndDirection( Vector3DPar up, Vector3DPar dir )
{
  SetDirectionUp(up.Normalized());
  // Project into the plane
  double t=DirectionUp()*dir;
  // Calculate the vector pointing along the x axis (i.e. aside)
  // sign is experimental - I never know what is right and what is left
  // no need to normalize - cross product of two perpendicular unit vectors is unit vector
  SetDirection((dir-DirectionUp()*t).Normalized());
  SetDirectionAside(DirectionUp().CrossProduct(Direction()));
}

//-----------------------------------------------------------------------------

void Matrix3D::SetTilda( Vector3DPar a )
{
  SetZero();
  Set(0,1)=-a[2],Set(0,2)=+a[1];
  Set(1,0)=+a[2],Set(1,2)=-a[0];
  Set(2,0)=-a[1],Set(2,1)=+a[0];
}

//-----------------------------------------------------------------------------

void Matrix3D::operator *= ( double op )
{
  m_aside*=op;
  m_up*=op;
  m_dir*=op;
}

//-----------------------------------------------------------------------------

void Matrix3D::SetAdd( const Matrix3D& a, const Matrix3D& b )
{
  m_aside = a.m_aside+b.m_aside;
  m_up = a.m_up+b.m_up;
  m_dir = a.m_dir+b.m_dir;
}

//-----------------------------------------------------------------------------

void Matrix3D::SetMultiply( const Matrix3D& a, double op )
{
  // matrix multiplication
  m_aside = a.m_aside*op;
  m_up = a.m_up*op;
  m_dir = a.m_dir*op;
}

//-----------------------------------------------------------------------------

void Matrix3D::AddMultiply( const Matrix3D& a, double op )
{
  // matrix multiplication
  m_aside += a.m_aside*op;
  m_up += a.m_up*op;
  m_dir += a.m_dir*op;
}

//-----------------------------------------------------------------------------

Matrix3D Matrix3D::operator + ( const Matrix3D& a ) const
{
  Matrix3D res;
  res.m_aside=m_aside+a.m_aside;
  res.m_up=m_up+a.m_up;
  res.m_dir=m_dir+a.m_dir;
  return res;
}

//-----------------------------------------------------------------------------

Matrix3D Matrix3D::operator - ( const Matrix3D& a ) const
{
  Matrix3D res;
  res.m_aside=m_aside-a.m_aside;
  res.m_up=m_up-a.m_up;
  res.m_dir=m_dir-a.m_dir;
  return res;
}

//-----------------------------------------------------------------------------

Matrix3D& Matrix3D::operator -= ( const Matrix3D& a )
{
  m_aside-=a.m_aside;
  m_up-=a.m_up;
  m_dir-=a.m_dir;
  return *this;
}

//-----------------------------------------------------------------------------

Matrix3D& Matrix3D::operator += ( const Matrix3D& a )
{
  m_aside+=a.m_aside;
  m_up+=a.m_up;
  m_dir+=a.m_dir;
  return *this;
}

//-----------------------------------------------------------------------------

Matrix4D Matrix4D::operator + ( const Matrix4D& a ) const
{
  Matrix4D res;
  res.m_orientation.m_aside=m_orientation.m_aside+a.m_orientation.m_aside;
  res.m_orientation.m_up=m_orientation.m_up+a.m_orientation.m_up;
  res.m_orientation.m_dir=m_orientation.m_dir+a.m_orientation.m_dir;
  res.m_position=m_position+a.m_position;
  return res;
}

//-----------------------------------------------------------------------------

Matrix4D Matrix4D::operator - ( const Matrix4D& a ) const
{
  Matrix4D res;
  res.m_orientation.m_aside = m_orientation.m_aside - a.m_orientation.m_aside;
  res.m_orientation.m_up    = m_orientation.m_up    - a.m_orientation.m_up;
  res.m_orientation.m_dir   = m_orientation.m_dir   - a.m_orientation.m_dir;
  res.m_position = m_position - a.m_position;
  return (res);
}

//-----------------------------------------------------------------------------

void Matrix4D::operator += ( const Matrix4D& op )
{
  m_orientation.m_aside+=op.m_orientation.m_aside;
  m_orientation.m_up+=op.m_orientation.m_up;
  m_orientation.m_dir+=op.m_orientation.m_dir;
  m_position+=op.m_position;
}

//-----------------------------------------------------------------------------

void Matrix4D::operator -= ( const Matrix4D& op )
{
  m_orientation.m_aside-=op.m_orientation.m_aside;
  m_orientation.m_up-=op.m_orientation.m_up;
  m_orientation.m_dir-=op.m_orientation.m_dir;
  m_position-=op.m_position;
}

//-----------------------------------------------------------------------------

void Matrix3D::SetInvertRotation( const Matrix3D& op )
{
#if _DEBUG
  Vector3D sizes;
  Assert(op.IsOrthogonal(&sizes) && AbsSqrZero(fabs(sizes[0] - 1)) && AbsSqrZero(fabs(sizes[1] - 1)) && AbsSqrZero(fabs(sizes[2] - 1)));
#endif

  for ( int i = 0; i < 3; ++i ) //for( int j=0; j<3; j++ )
  {
    Set( i, 0 ) = op.Get( 0, i );
    Set( i, 1 ) = op.Get( 1, i );
    Set( i, 2 ) = op.Get( 2, i );
  }
}

//-----------------------------------------------------------------------------

void Matrix3D::SetInvertScaled( const Matrix3D& op )
{
  // matrix inversion is calculated based on these prepositions:
  // matrix is S*R, where S is scale, R is rotation
  // inversion of such matrix is Inv(R)*Inv(S)
  // Inv(R) is Transpose(R), Inv(S) is C: C(i,i)=1/S(i,i)
  // sizes of row(i) are scale coeficients a,b,c
  // all member are set
  //SetIdentity();
#if _DEBUG
  Assert(op != M3ZeroD);
#endif

  // calculate scale
  double invScale0=InvSquareSize(op.Get(0,0),op.Get(0,1),op.Get(0,2));
  double invScale1=InvSquareSize(op.Get(1,0),op.Get(1,1),op.Get(1,2));
  double invScale2=InvSquareSize(op.Get(2,0),op.Get(2,1),op.Get(2,2));

  // invert rotation and scale
  for( int i=0; i<3; i++ ) //for( int j=0; j<3; j++ )
  {
    Set(i,0)=op.Get(0,i)*invScale0;
    Set(i,1)=op.Get(1,i)*invScale1;
    Set(i,2)=op.Get(2,i)*invScale2;
  }

  #if _DEBUG
    // verify the result is matrix inverse
    {
      // verify matrix is S*R
      // scaled matrix has form S*R, where S is scale matrix and R is rotation
      // (S*R)*(RT*ST)=S*ST=S*S
      // transpose
      Matrix3D se = *this*op;
      // check if the result is identity
      const double max=3e-6f;
      if(
        fabs(se(0,0)-1)<max && fabs(se(1,1)-1)<max && fabs(se(2,2)-1)<max &&
        fabs(se(0,1))<max && fabs(se(0,2))<max &&
        fabs(se(1,0))<max && fabs(se(1,2))<max &&
        fabs(se(2,0))<max && fabs(se(2,1))<max
      )
      {}
      else
      {
        LogF("%s(%d) : Inverse failed - not scale*rotation?",__FILE__,__LINE__);
      }
    }
  #endif

}

//-----------------------------------------------------------------------------

#define swap(a,b) {double p;p=a;a=b;b=p;}

//-----------------------------------------------------------------------------

void Matrix3D::SetInvertGeneral( const Matrix3D& op )
{
  Assert(op != M3ZeroD);
  #if 1
    // TODO: detect general matrices as soon as possible
    // check if they are really necessary
    // check if matrix is really general
    // if not, we can use scaled version
    // scaled matrix has form S*R, where S is scale matrix and R is rotation
    // (S*R)*(RT*ST)=S*ST=S*S
    // 
    Matrix3D t;
    for( int i=0; i<3; i++ )
    {
      t(0,i)=op(i,0),t(1,i)=op(i,1),t(2,i)=op(i,2);
    }
    Matrix3DVal se=op*t;
    // check if se is diagonal
    #if 0
    static int diag=0;
    static int unit=0;
    static int total=0;
    if( total>1000 )
    {
      LogF("G Diagonal: %.1f %% matrices",diag*100.0/total);
      total=0;
      diag=0;
    }
    total++;
    #endif
    const double max=1e-6;
    if
    (
      fabs(se(0,1))<max*fabs(se(0,0)) && fabs(se(0,2))<max*fabs(se(0,0)) &&
      fabs(se(1,0))<max*fabs(se(1,1)) && fabs(se(1,2))<max*fabs(se(1,1)) &&
      fabs(se(2,0))<max*fabs(se(2,2)) && fabs(se(2,1))<max*fabs(se(2,2))
    )
    {
      // matrix is diagonal - use special case inversion
      //diag++;
      SetInvertScaled(op);
      return;
    }
  #endif

  // calculate inversion using Gauss-Jordan elimination
  Matrix3D a=op;
  // load result with identity
  SetIdentity();
  int row,col;
  // construct result by pivoting
  // pivot column
  for( col=0; col<3; col++ )
  {
    // use maximal number as pivot
    double max=0.0;
    int maxRow=col;
    for( row=col; row<3; row++ )
    {
      double mag=fabs(a.Get(row,col));
      if( max<mag ) max=mag,maxRow=row;
    }
    if( max<=0.0 ) continue; // no pivot exists
    // swap lines col and maxRow
    swap(a.Set(col,0),a.Set(maxRow,0));
    swap(a.Set(col,1),a.Set(maxRow,1));
    swap(a.Set(col,2),a.Set(maxRow,2));
    swap(Set(col,0),Set(maxRow,0));
    swap(Set(col,1),Set(maxRow,1));
    swap(Set(col,2),Set(maxRow,2));
    // use a(col,col) as pivot
    double quotient=1.0/a.Get(col,col);
    // make pivot 1
    a.Set(col,0)*=quotient,a.Set(col,1)*=quotient,a.Set(col,2)*=quotient;
    Set(col,0)*=quotient,Set(col,1)*=quotient,Set(col,2)*=quotient;
    // use pivot line to zero all other lines
    for( row=0; row<3; row++ ) if( row!=col )
    {
      double factor=a.Get(row,col);
      a.Set(row,0)-=a.Get(col,0)*factor;
      a.Set(row,1)-=a.Get(col,1)*factor;
      a.Set(row,2)-=a.Get(col,2)*factor;
      Set(row,0)-=Get(col,0)*factor;
      Set(row,1)-=Get(col,1)*factor;
      Set(row,2)-=Get(col,2)*factor;
    }
  }
  // result constructed
}

//-----------------------------------------------------------------------------

// math for PCA:

void Matrix3D::SetCovarianceCenter ( const AutoArray< Vector3D>& data, const Matrix4D* m )
{
  double sx  = 0.0;
  double sy  = 0.0;
  double sz  = 0.0;
  double sxx = 0.0;
  double sxy = 0.0;
  double sxz = 0.0;
  double syy = 0.0;
  double syz = 0.0;
  double szz = 0.0;
  int i;
  int len = data.Size();
  if ( len < 3 )
  {
    SetIdentity();
    return;
  }
  for ( i = 0; i < len; i++ )
  {
    Vector3D v;
    if ( m )
      v.SetMultiply(*m,data[i]);
    else
      v = data[i];
    sx  += v[0];
    sy  += v[1];
    sz  += v[2];
    sxx += v[0] * v[0];
    sxy += v[0] * v[1];
    sxz += v[0] * v[2];
    syy += v[1] * v[1];
    syz += v[1] * v[2];
    szz += v[2] * v[2];
  }
  double inv = 1.0f / len;
  Set(0,0) =            (sxx - inv * sx * sx) * inv;
  Set(0,1) = Set(1,0) = (sxy - inv * sx * sy) * inv;
  Set(0,2) = Set(2,0) = (sxz - inv * sx * sz) * inv;
  Set(1,1) =            (syy - inv * sy * sy) * inv;
  Set(1,2) = Set(2,1) = (syz - inv * sy * sz) * inv;
  Set(2,2) =            (szz - inv * sz * sz) * inv;
}

//-----------------------------------------------------------------------------

#define D_PI  ( 3.14159265358979323846 )
#define Pi3   (D_PI/3.0)
#define Delta (2.0*Pi3)
#define Inv3  (1.0/3.0)
#define Inv6  (1.0/6.0)
#define Inv27 (1.0/27.0)

//-----------------------------------------------------------------------------

bool Matrix3D::SetEigenStandard ( Matrix3DPar cov, double* eig )
{
  double eigS[3];
  if ( !eig ) eig = eigS;
    // assuming "cov" to be symmetric matrix:
  double a = cov.Get(0,0);
  double b = cov.Get(0,1);
  double c = cov.Get(0,2);
  double d = cov.Get(1,1);
  double e = cov.Get(1,2);
  double f = cov.Get(2,2);
  double bb = b * b;
  double cc = c * c;
  double ee = e * e;
    // characteristic polynomial: L^3 + a1 * L^2 + a2 * L + a3
  double a1 = - a - d - f;
  double a2 = d*f + a*f + a*d - bb - cc - ee;
  double a3 = a * ee + f * bb + d * cc - a * d * f - 2.0f * b * c * e;
  double a12 = a1 * a1;
  double q = (a12 * Inv3 - a2) * Inv3;
  double r = a1 * (a2 * Inv6 - a12 * Inv27) - 0.5f * a3;
  double sqrtq = sqrt( q );
  double t;
  if ( q * q * q < r * r )
  {
    t = (r > 0.0) ? 0.0 : Pi3;
  }
  else
  {
    double cost = r / (q * sqrtq);
    saturateD( cost, -1.0, 1.0 ); // cost might be of value -1.0000001
    t = acos(cost) * Inv3;
  }
  double a13 = - a1 * Inv3;
  sqrtq += sqrtq;
  eig[0] = sqrtq * cos(t) + a13;

  // Vieto's formulae:
  eig[1] = sqrtq * cos(t + Delta) + a13;
  eig[2] = - a1 - eig[0] - eig[1];

    // eigenvectors:
  Vector3D v[3];
  double   rel[3];
  int i;
  for ( i = 0; i < 3; i++ )
    rel[i] = v[i].SetEigenVector(a-eig[i],b,c,d-eig[i],e,f-eig[i]);
  int p0, p1;                               // p0-th will be the most reliable vector
  if ( rel[0] > rel[1] )                    // p1-th the second most reliable one..
  {
    p0 = 0;
    p1 = 1;
  }
  else
  {
    p0 = 1;
    p1 = 0;
  }
  if ( rel[p1] < rel[2] )
    if ( rel[p0] < rel[2] )
    {
      p1 = p0;
      p0 = 2;
    }
    else
      p1 = 2;

    // the most reliable eigenvector:
  if ( rel[p0] < 0.0 )
  {
    m_aside[0] = 1.0;
    m_aside[1] = m_aside[2] = 0.0;
  }
  else
  {
    m_aside = v[p0];
    m_aside.Normalize();
  }

    // the second one:
  if ( rel[p1] < 0.0 )
  {
    m_up[0] = m_aside[1];
    m_up[1] = m_aside[2];
    m_up[2] = m_aside[0];
  }
  else
    m_up = v[p1];
  m_up = m_aside.CrossProduct( m_up );
  m_up.Normalize();

    // the third one:
  m_dir = m_aside.CrossProduct( m_up );          // implicit unit length

  return (true);
}

//-----------------------------------------------------------------------------

double Matrix3D::Determinant() const
{
  return (
    +Get(0,0)*Get(1,1)*Get(2,2)
    +Get(1,0)*Get(2,1)*Get(0,2)
    +Get(0,1)*Get(2,0)*Get(1,2)
    -Get(0,0)*Get(2,1)*Get(1,2)    
    -Get(1,1)*Get(2,0)*Get(0,2)
    -Get(2,2)*Get(0,1)*Get(1,0)
  );
}

//-----------------------------------------------------------------------------

bool Matrix3D::IsOrthogonal( Vector3D* sqrSize ) const
{
  double sqAside =  m_aside.SquareSize();
  double sqUp = m_up.SquareSize();
  if ( sqAside==0 || sqUp==0 ) return false;
  if ( !AbsZero(fabs( m_aside * m_up  ) * InvSqrtD(sqAside)) ||
       !AbsZero(fabs( m_aside * m_dir ) * InvSqrtD(sqAside)) ||
       !AbsZero(fabs( m_up    * m_dir ) * InvSqrtD(sqUp)) )
    return false;
  if ( sqrSize )
  {
    (*sqrSize)[0] = sqAside;
    (*sqrSize)[1] = sqUp;
    (*sqrSize)[2] = m_dir.SquareSize();
  }
  return true;
}

//-----------------------------------------------------------------------------

// general calculations

void SaturateMin( Vector3D& min, Vector3DPar val )
{
  saturateMinD( min[0], val[0] );
  saturateMinD( min[1], val[1] );
  saturateMinD( min[2], val[2] );
}

//-----------------------------------------------------------------------------

void SaturateMax( Vector3D& max, Vector3DPar val )
{
  saturateMaxD( max[0], val[0] );
  saturateMaxD( max[1], val[1] );
  saturateMaxD( max[2], val[2] );
}

//-----------------------------------------------------------------------------

void CheckMinMax( Vector3D& min, Vector3D& max, Vector3DPar val )
{
  saturateMinD( min[0], val[0] ), saturateMaxD( max[0], val[0] );
  saturateMinD( min[1], val[1] ), saturateMaxD( max[1], val[1] );
  saturateMinD( min[2], val[2] ), saturateMaxD( max[2], val[2] );
}

//-----------------------------------------------------------------------------

Vector3D VectorMin( Vector3DPar a, Vector3DPar b )
{
  return (Vector3D( floatMinD( a[0], b[0] ), floatMinD( a[1], b[1] ), floatMinD( a[2], b[2] ) ));
}

//-----------------------------------------------------------------------------

Vector3D VectorMax( Vector3DPar a, Vector3DPar b )
{
  return (Vector3D( floatMaxD( a[0], b[0] ), floatMaxD( a[1], b[1] ), floatMaxD( a[2], b[2] ) ));
}

//-----------------------------------------------------------------------------

