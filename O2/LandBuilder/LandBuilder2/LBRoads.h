//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "ilandbuildcmds.h"

#include <string>
#include <vector>

//-----------------------------------------------------------------------------

using std::vector;
using std::string;

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

  using namespace LandBuildInt;

//-----------------------------------------------------------------------------

  typedef enum
  {
    RPMT_Straight,
    RPMT_Corner,
    RPMT_Terminator,
    RPMT_Special,
    RPMT_Cross
  } RoadPartModelTypes;

//-----------------------------------------------------------------------------

  class LBRoadPartModel
  {
    string             m_name;
    RoadPartModelTypes m_type;
    bool               m_rightOriented;

  public:
    LBRoadPartModel()
    : m_rightOriented( true )
    {
    }

    LBRoadPartModel( const string& name, const RoadPartModelTypes& type, bool rightOriented )
    : m_name( name )
    , m_type( type )
    , m_rightOriented( rightOriented )
    {
    }

    LBRoadPartModel( const LBRoadPartModel& other )
    : m_name( other.m_name )
    , m_type( other.m_type )
    , m_rightOriented( other.m_rightOriented )
    {
    }

    LBRoadPartModel& operator = ( const LBRoadPartModel& other )
    {
      m_name          = other.m_name;
      m_type          = other.m_type;
      m_rightOriented = other.m_rightOriented;
      return (*this);
    }

    void SetName( const string& name )
    {
      string temp = name;
      for ( size_t i = 0, cntI = temp.length(); i < cntI; ++i )
      {
        if ( temp[i] == ' ' ) { temp[i] = '#'; }
      }
      m_name = temp;
    }

    const string& GetName() const
    {
      return (m_name);
    }

    void SetType( const RoadPartModelTypes& type )
    {
      m_type = type;
    }

    RoadPartModelTypes GetType() const
    {
      return (m_type);
    }

    const string GetTypeAsString() const
    {
      switch ( m_type )
      {
      case RPMT_Straight:   return ( "straight" );
      case RPMT_Corner:     return ( "corner" );
      case RPMT_Terminator: return ( "terminator" );
      case RPMT_Special:    return ( "special" );
      case RPMT_Cross:      return ( "cross" );
      default:              return ( "straight" );
      }
    }

    const string GetOrientation() const
    {
      if ( m_rightOriented )
      {
        return ( "right" );
      }
      else
      {
        return ( "left" );
      }
    }

    bool IsRightOriented() const
    {
      return (m_rightOriented);
    }

    void SetRightOriented( bool rightOriented )
    {
      m_rightOriented = rightOriented;
    }
  };

//-----------------------------------------------------------------------------

  std::ostream& operator << ( std::ostream& o, const LBRoadPartModel& part );

//-----------------------------------------------------------------------------

  class LBCrossroadDefinition
  {
    string m_name;
    string m_roadTypeDirA;
    string m_roadTypeDirB;
    string m_roadTypeDirC;
    string m_roadTypeDirD;

  public:
    LBCrossroadDefinition()
    : m_name( "" )
    , m_roadTypeDirA( "" )
    , m_roadTypeDirB( "" )
    , m_roadTypeDirC( "" )
    , m_roadTypeDirD( "" )
    {
    }

    LBCrossroadDefinition( const string& name )
    : m_name( name )
    , m_roadTypeDirA( "" )
    , m_roadTypeDirB( "" )
    , m_roadTypeDirC( "" )
    , m_roadTypeDirD( "" )
    {
    }

    const string& GetName() const
    {
      return ( m_name );
    }

    void SetRoadTypeDirA( const string& roadTypeDirA )
    {
      m_roadTypeDirA = roadTypeDirA;
    }

    void SetRoadTypeDirB( const string& roadTypeDirB )
    {
      m_roadTypeDirB = roadTypeDirB;
    }

    void SetRoadTypeDirC( const string& roadTypeDirC )
    {
      m_roadTypeDirC = roadTypeDirC;
    }

    void SetRoadTypeDirD( const string& roadTypeDirD )
    {
      m_roadTypeDirD = roadTypeDirD;
    }

    const string& GetRoadTypeDirA() const
    {
      return ( m_roadTypeDirA );
    }

    const string& GetRoadTypeDirB() const
    {
      return ( m_roadTypeDirB );
    }

    const string& GetRoadTypeDirC() const
    {
      return ( m_roadTypeDirC );
    }

    const string& GetRoadTypeDirD() const
    {
      return ( m_roadTypeDirD );
    }
  };

//-----------------------------------------------------------------------------

  class LBRoadDefinition
  {
    string                    m_name;
    string                    m_outputDir;
    vector< LBRoadPartModel > m_models;

  public:
    LBRoadDefinition()
    : m_name( "" )
    , m_outputDir( "" )
    {
    }

    LBRoadDefinition( const string& name )
    : m_name( name )
    , m_outputDir( "" )
    {
    }

    const string& GetName() const
    {
      return (m_name);
    }

    const string& GetOutputDir() const
    {
      return (m_outputDir);
    }

    void SetOutputDir( const string& outputDir )
    {
      m_outputDir = outputDir;
    }

    const vector< LBRoadPartModel >& GetModels() const
    {
      return (m_models);
    }

    void AddModel( const LBRoadPartModel& model )
    {
      bool found = false;
      for ( size_t i = 0, cntI = m_models.size(); i < cntI; ++i )
      {
        if ( m_models[i].GetName() == model.GetName() )
        {
          found = true;
          break;
        }
      }
      if ( !found ) { m_models.push_back( model ); }
    }
  };

//-----------------------------------------------------------------------------

  class LBRoad
  {
    vector< LBRoadPartModel > m_parts;
    string m_type;

  public:
    LBRoad()
    {
    }

    void SetType( const string& type )
    {
      m_type = type;
    }

    void AppendPart( const LBRoadPartModel& part )
    {
      m_parts.push_back( part );
    }

    size_t GetPartsCount() const
    {
      return (m_parts.size());
    }

    const string& GetType() const
    {
      return (m_type);
    }

    const LBRoadPartModel& GetPart( size_t index ) const
    {
      assert( index < m_parts.size() );
      return (m_parts[index]);
    }

    LBRoadPartModel& GetPart( size_t index )
    {
      assert( index < m_parts.size() );
      return (m_parts[index]);
    }
  };

//-----------------------------------------------------------------------------

  class LBRoadKeypart
  {
    string           m_templateName;
    string           m_roadType;
    string           m_keyType;
    Shapes::DVertex  m_position;
    double           m_orientation;
    vector< LBRoad > m_roads;

  public:
    LBRoadKeypart()
    {
    }

    const string& GetTemplateName() const
    {
      return (m_templateName);
    }

    const string& GetRoadType() const
    {
      return (m_roadType);
    }

    const string& GetKeyType() const
    {
      return (m_keyType);
    }

    const Shapes::DVertex& GetPosition() const
    {
      return (m_position);
    }
      
    double GetOrientation() const
    {
      return (m_orientation);
    }

    void SetTemplateName( const string& templateName ) 
    {
      m_templateName = templateName;
    }

    void SetRoadType( const string& roadType )
    {
      m_roadType = roadType;
    }

    void SetKeyType( const string& keyType )
    {
      m_keyType = keyType;
    }

    void SetPosition( const Shapes::DVertex& position )
    {
      m_position = position;
    }
      
    void SetOrientation( double orientation )
    {
      m_orientation = orientation;
    }

    LBRoad& AppendRoad()
    {
      m_roads.push_back( LBRoad() );
      return (m_roads[m_roads.size() - 1]);
    }

    size_t GetRoadsCount() const
    {
      return (m_roads.size());
    }

    LBRoad& GetRoad( size_t index )
    {
      assert( index < m_roads.size() );
      return (m_roads[index]);
    }

    const LBRoad& GetRoad( size_t index ) const
    {
      assert( index < m_roads.size() );
      return (m_roads[index]);
    }
  };

//-----------------------------------------------------------------------------

  class LBRoadPath
  {
    string                    m_roadType;
    Shapes::DVertex           m_startPosition;
    double                    m_startOrientation;
    vector< LBRoadPartModel > m_roadParts;

  public:
    LBRoadPath()
    {
    }

    LBRoadPath( const string& roadType )
    : m_roadType( roadType )
    {
    }

    const string& GetRoadType() const
    {
      return (m_roadType);
    }

    void SetStartPosition( const Shapes::DVertex& startPosition )
    {
      m_startPosition = startPosition;
    }

    const Shapes::DVertex GetStartPosition() const
    {
      return (m_startPosition);
    }

    void SetStartOrientation( double startOrientation )
    {
      m_startOrientation = startOrientation;
    }

    double GetStartOrientation() const
    {
      return (m_startOrientation);
    }

    const vector< LBRoadPartModel >& GetRoadParts() const
    {
      return (m_roadParts);
    }

    void AddRoadPart( const LBRoadPartModel& part )
    {
      m_roadParts.push_back( part );
    }
  };

//-----------------------------------------------------------------------------

  class LBRoadMap : public LandBuildInt::IRoadMap
  {
    vector< LBCrossroadDefinition > m_crossroadDefinitions;
    vector< LBRoadDefinition >      m_roadDefinitions;
    vector< LBRoadKeypart >         m_roadKeyparts;
    vector< LBRoadPath >            m_roadPaths;
    DoubleRect                      m_mapArea;

  public:
    LBRoadMap( const DoubleRect& mapArea ) 
    : m_mapArea( mapArea ) 
    {
    }

    const vector< LBCrossroadDefinition >& GetCrossroadDefinitions() const 
    {
      return (m_crossroadDefinitions);
    }

    const vector< LBRoadDefinition >& GetRoadDefinitions() const 
    {
      return (m_roadDefinitions);
    }

    const vector< LBRoadKeypart >& GetRoadKeyparts() const 
    {
      return (m_roadKeyparts);
    }

    const vector< LBRoadPath >& GetRoadPaths() const 
    {
      return (m_roadPaths);
    }

    virtual void AddCrossroadDefinition( const LBCrossroadDefinition& crossroadDefinition )
    {
      m_crossroadDefinitions.push_back( crossroadDefinition );
    }

    virtual void AddRoadDefinition( const LBRoadDefinition& roadDefinition )
    {
      m_roadDefinitions.push_back( roadDefinition );
    }

    virtual void AddRoadKeypart( const LBRoadKeypart& roadKeypart )
    {
      m_roadKeyparts.push_back( roadKeypart );
    }

    virtual void AddRoadPath( const LBRoadPath& roadPath )
    {
      m_roadPaths.push_back( roadPath );
    }
  };

//-----------------------------------------------------------------------------

} // namespace LandBuilder2