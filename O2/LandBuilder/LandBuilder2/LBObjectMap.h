//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------

#include "ilandbuildcmds.h"
#include "ObjectContainer.h"

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

  using namespace LandBuildInt;

  class LBObjectMap : public LandBuildInt::IObjectMap
  {
    ObjectContainer m_objects;
    DoubleRect      m_mapArea;

  public:
    LBObjectMap( const DoubleRect& mapArea ) 
    : m_mapArea( mapArea ) 
    {
    }

    virtual ~LBObjectMap()
    {
    }

    virtual size_t PlaceObject( const Matrix4D& location, const char* objectName, size_t tag );
    virtual const Matrix4D& GetObjectLocation( size_t objId );
    virtual const char* GetObjectType( size_t objId );
    virtual size_t GetObjectTag( size_t objId );
    virtual size_t FindNearestObject( double x, double y, size_t tag, const Array< size_t >& ignoreIDs );

    virtual Shapes::IShape* CreateObjectShape( size_t objId ) 
    {
      return (NULL);
    }

    virtual bool EnumObjects( int& objId );
    virtual int ObjectsInArea( const DoubleRect& area, size_t tag, const IEnumObjectCallback& callback );
    virtual int ObjectsInArea( const Shapes::IShape& shape, size_t tag, const IEnumObjectCallback& callback );
    virtual bool DeleteObject( size_t objId );
    virtual bool ReplaceObject( size_t objId, const Matrix4D& location, const char* objectName, size_t tag );

    ///Returns active map area in meters
    virtual DoubleRect GetMapArea() const 
    {
      return (m_mapArea);
    }

    ObjectContainer& GetObjectContainer() 
    {
      return (m_objects);
    }
    
    const ObjectContainer& GetObjectContainer() const 
    {
      return (m_objects);
    }
  };

//-----------------------------------------------------------------------------

} // namespace LandBuilder2