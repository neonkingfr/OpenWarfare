//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "LBRoads.h"

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

  std::ostream& operator << ( std::ostream& o, const LBRoadPartModel& part )
  {
    switch ( part.GetType() )
    {
    case RPMT_Straight:   
      {
        o << "| "; 
      }
      break;
    case RPMT_Corner:
      {
        if ( part.IsRightOriented() )
        {
          o << "( "; 
        }
        else
        {
          o << ") "; 
        }
      }
      break;
    case RPMT_Terminator: 
      {
        o << "- "; 
      }
      break;
    case RPMT_Special:    
      {
        o << "* "; 
      }
      break;
    case RPMT_Cross:      
      {
        o << "+ "; 
      }
      break;
    }

    o << part.GetName();
	  o << "\n";

	  return (o);
  }

//-----------------------------------------------------------------------------

} // LandBuilder2