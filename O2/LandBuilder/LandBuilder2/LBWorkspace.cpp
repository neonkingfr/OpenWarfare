//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include "LBWorkspace.h"
#include <iomanip>

//-----------------------------------------------------------------------------

namespace LandBuilder2
{

//-----------------------------------------------------------------------------

	bool LBWorkspace::ExportHeightMap( std::ostream& out ) const
	{
		DoubleRect mapArea = m_objects.GetMapArea();
		printf( "Exporting height map\n" );
		int minx = toInt( floor( mapArea.GetLeft() / m_ofpMapSegmentSizeX ) );
		int miny = toInt( floor( mapArea.GetTop() / m_ofpMapSegmentSizeY ) );
		int maxx = toInt( ceil( mapArea.GetRight() / m_ofpMapSegmentSizeX ) );
		int maxy = toInt( ceil( mapArea.GetBottom() / m_ofpMapSegmentSizeY ) );

		int minxExp = minx;
		int minyExp = miny;
		int maxxExp = maxx;
		int maxyExp = maxy;
    if ( m_visitorVersion == 3 ) 
    { 
		  minxExp -= minx;
		  minyExp -= miny;
		  maxxExp -= minx;
		  maxyExp -= miny;
    }

		out << "heightmap\n";
		out << minxExp << " " << minyExp << " " << maxxExp << " " << maxyExp << " " << m_ofpMapSegmentSizeX 
        << " " << m_ofpMapSegmentSizeY << "\n";

		ProgressBar< int > pb( maxx - minx );
		for ( int y = miny; y <= maxy; ++y )
		{
			pb.AdvanceNext( 1 );
			for ( int x = minx; x <= maxx; ++x )
			{
				double fx = x * m_ofpMapSegmentSizeX;
				double fy = y * m_ofpMapSegmentSizeY;
				double h = m_highMap.GetHeight( fx, fy );
				out << h << " ";
			}
			out << "\n";
		}
		out << "end heightmap\n";
		printf( "Finished ... (height map)\n" );
		return !( !out );
	}

//-----------------------------------------------------------------------------

	bool LBWorkspace::SaveResult( const char* target ) const
	{
		using namespace std;
		ofstream out( target, ios::out|ios::trunc );
    if ( !out ) { return (false); }
		out << setiosflags( ios::scientific );
		if ( m_ofpMapSegmentSizeX > 0.0001 && m_ofpMapSegmentSizeX > 0.0001 )
		{
			ProgressBar< int > pb( 2 );
			pb.AdvanceNext( 1 );
      if ( !ExportHeightMap( out ) ) { return (false); }
			pb.AdvanceNext( 1 );
      if ( !SaveResult( out ) ) { return (false); }
			return (true);
		}
		else
		{
			return (SaveResult( out ));
		}
	}

//-----------------------------------------------------------------------------

	bool LBWorkspace::SaveResult( std::ostream& out ) const
	{
		using namespace std;
		DoubleRect mapArea = m_objects.GetMapArea();

		// saves road definitions
		const vector< LBRoadDefinition >& roadDefinitions = m_roads.GetRoadDefinitions();
		if ( roadDefinitions.size() > 0 )
		{
			ProgressBar< int > pb( roadDefinitions.size() );
			for ( size_t i = 0, cntI = roadDefinitions.size(); i < cntI; ++i )
			{
				pb.AdvanceNext( 1 );
				out << "road template\n";
				const LBRoadDefinition& road = roadDefinitions[i];
				out << road.GetName() << "\n";
        out << "output_dir = \"" << road.GetOutputDir() << "\"\n";
				const vector< LBRoadPartModel >& roadModels = road.GetModels();
				for ( size_t j = 0, cntJ = roadModels.size(); j < cntJ; ++j )
				{
          out << roadModels[j].GetTypeAsString() << " " <<  roadModels[j].GetName() << "\n";					
				}
				out << "end road template\n";
			}
		}

		// saves crossroad definitions
		const vector< LBCrossroadDefinition >& crossroadDefinitions = m_roads.GetCrossroadDefinitions();
		if ( crossroadDefinitions.size() > 0 )
		{
			ProgressBar< int > pb( crossroadDefinitions.size() );
			for ( size_t i = 0, cnt = crossroadDefinitions.size(); i < cnt; ++i )
			{
				pb.AdvanceNext( 1 );
				out << "crossroad template\n";
				const LBCrossroadDefinition& crossroad = crossroadDefinitions[i];
				out << crossroad.GetName() << "\n";
        out << crossroad.GetRoadTypeDirA() << "\n";
        out << crossroad.GetRoadTypeDirB() << "\n";
        out << crossroad.GetRoadTypeDirC() << "\n";
        out << crossroad.GetRoadTypeDirD() << "\n";
				out << "end crossroad template\n";
      }
    }

		// saves road keyparts
		const vector< LBRoadKeypart >& roadKeyparts = m_roads.GetRoadKeyparts();
		if ( roadKeyparts.size() > 0 )
		{
			ProgressBar< int > pb( roadKeyparts.size() );
			for ( size_t i = 0, cnt = roadKeyparts.size(); i < cnt; ++i )
      {
				pb.AdvanceNext( 1 );
				out << "road keypart\n";
				const LBRoadKeypart& keypart = roadKeyparts[i];
        out << keypart.GetKeyType() << "\n";
        out << keypart.GetRoadType() << "\n";
        out << keypart.GetTemplateName() << "\n";

        double x = keypart.GetPosition().x;
        double y = keypart.GetPosition().y;
        if ( m_visitorVersion == 3 ) 
        { 
          x -= mapArea.GetLeft();
          y -= mapArea.GetBottom();
        }

        char buffer[50];
        sprintf( buffer, "%.3f", x );
				out << buffer << "\n";
			  sprintf( buffer, "%.3f", y );
				out << buffer << "\n";
        out << keypart.GetOrientation() << "\n";

        size_t roadsCount = keypart.GetRoadsCount();
        out << roadsCount << "\n";

        for ( size_t j = 0; j < roadsCount; ++j )
        {
          const LBRoad& road = keypart.GetRoad( j );
          size_t partsCount = road.GetPartsCount();
          out << partsCount << "\n";
          out << road.GetType() << "\n";
          for ( size_t k = 0; k < partsCount; ++k )
          {
            out << road.GetPart( k );
          }
        }
        
        out << "end road keypart\n";
      }
    }

		// saves road paths
		const vector< LBRoadPath >& roadPaths = m_roads.GetRoadPaths();
		if ( roadPaths.size() > 0 )
		{
			ProgressBar< int > pb( roadPaths.size() );
			for ( size_t i = 0, cntI = roadPaths.size(); i < cntI; ++i )
			{
				pb.AdvanceNext( 1 );
				out << "road definition\n";
				const LBRoadPath& roadPath = roadPaths[i];
				const Shapes::DVertex& pos = roadPath.GetStartPosition();
				double PI = 4.0 * atan( 1.0 );
				double orientation = roadPath.GetStartOrientation() * 180.0 / PI; 
				out << roadPath.GetRoadType() << " ";

        double x = pos.x;
        double y = pos.y;
        if ( m_visitorVersion == 3 ) 
        { 
          x -= mapArea.GetLeft();
          y -= mapArea.GetBottom();
        }

        char buffer[50];
			  sprintf( buffer, "%.3f", x );
				out << buffer;
        out << " ";
			  sprintf( buffer, "%.3f", y );
				out << buffer;
        out << " " << orientation << "\n";
				const vector< LBRoadPartModel >& roadParts = roadPath.GetRoadParts();
				for ( size_t j = 0, cntJ = roadParts.size(); j < cntJ; ++j )
				{
					const LBRoadPartModel& roadPart = roadParts[j];
					out << roadPart.GetTypeAsString() << " " << roadPart.GetName() << " " << roadPart.GetOrientation() << "\n";
				}				

				out << "end road definition\n";
			}
		}

		// saves objects
		const ObjectContainer& objects = m_objects.GetObjectContainer();
		ProgressBar< int > pb( objects.Size() );
		out << "objects\n";
		for ( int i = 0, cnt = objects.Size(); i < cnt; ++i ) 
		{
			if ( objects.IsValidObjectID( i ) )
			{
				pb.AdvanceNext( 1 );
				const ObjectDesc& obj = objects.operator []( i );
				const Matrix4D& pos = obj.GetPositionMatrix();
				const char* name = obj.GetName();
				out << pos( 0, 0 ) << "\t" << pos( 1, 0 ) << "\t" << pos( 2, 0 ) << "\t";
				out << pos( 0, 1 ) << "\t" << pos( 1, 1 ) << "\t" << pos( 2, 1 ) << "\t";
				out << pos( 0, 2 ) << "\t" << pos( 1, 2 ) << "\t" << pos( 2, 2 ) << "\t";

        double x = pos( 0, 3 );
        double y = pos( 2, 3 );
        if ( m_visitorVersion == 3 ) 
        { 
          x -= mapArea.GetLeft();
          y -= mapArea.GetBottom();
        }

        char buffer[50];
			  sprintf( buffer, "%.3f", x );
				out << buffer << "\t";
			  sprintf( buffer, "%.3f", pos( 1, 3 ) );
				out << buffer << "\t";
			  sprintf( buffer, "%.3f", y );
				out << buffer << "\t";        
				out << obj.GetTag() << "\t";
				out << name << "\n";
			}
		}
		out << "end objects\n";
		return (!(!out));
	}

//-----------------------------------------------------------------------------

} // namespace LandBuilder2