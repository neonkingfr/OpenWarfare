//-----------------------------------------------------------------------------

#include "StdAfx.h"
#include ".\highmapbase.h"

//-----------------------------------------------------------------------------

namespace LandBuildInt
{

//-----------------------------------------------------------------------------

  HeightMapBase::HeightMapBase() 
  : m_minRes( FLT_MAX / 10.0 )
  {
  }

//-----------------------------------------------------------------------------

  HeightMapBase::~HeightMapBase()
  {
  }

//-----------------------------------------------------------------------------

  void HeightMapBase::SetMapArea( const DoubleRect& mapSize )
  {
    m_mapArea = mapSize;
  }

//-----------------------------------------------------------------------------

  DoubleRect HeightMapBase::GetMapArea()
  {
    return (m_mapArea);
  }

//-----------------------------------------------------------------------------

  void HeightMapBase::AddSamplerModule( const IHeightMapSampler* sampler, double resolution, 
                                        const DoubleRect* area )
  {
    DoubleRect zr( 0.0, 0.0, 0.0, 0.0 );
    if ( !area ) { area = &zr; }

    m_samplers.Add( SamplerModule( sampler, resolution, *area ) );
    if ( resolution < m_minRes ) { m_minRes = resolution; }
  }

//-----------------------------------------------------------------------------

  double HeightMapBase::GetHeight( double x, double y ) const
  {
    HeightMapLocInfo locInfo( x, y, 0.0 );
    for ( int i = 0, cnt = m_samplers.Size(); i < cnt; ++i )
    {
      const SamplerModule& mod = m_samplers[i];
      if ( (x >= mod.m_area.GetLeft() && x <= mod.m_area.GetRight() && 
            y >= mod.m_area.GetBottom() && y <= mod.m_area.GetTop()) || 
           (mod.m_area.GetLeft() == 0 && mod.m_area.GetTop() == 0 &&  
            mod.m_area.GetRight() == 0 && mod.m_area.GetBottom() == 0) )
      {
        locInfo.SetAlt( mod.m_sampler->GetHeight( locInfo ) );
      }
    }    
    return (locInfo.GetAlt());
  }

//-----------------------------------------------------------------------------

} // namespace LandBuildInt