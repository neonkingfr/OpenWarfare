// ModelConfigEditor.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "ModelConfigEditor.h"
#include "DlgMainFrame.h"
#include "StructuredProperties.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Application

BEGIN_MESSAGE_MAP(Application, CWinApp)
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()


// Application construction

Application::Application()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only Application object

Application theApp;

static void StructPropTest()
{
/*  StructuredProperties<CString> structProp;
  StructuredPropSection<CString> *root=structProp.GetRootSection();
  root->AddValue("Test",10);
  root->AddValue("Text",CString("Ahoj"));
  StructuredPropSection<CString> *cls=root->AddSection("SubSect");
  cls->AddValue("
*/
}


// Application initialization

BOOL Application::InitInstance()
{
	CWinApp::InitInstance();
    InitLogHandles();
    CurrentAppFrameFunctions=this;
    StructPropTest();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Bohemia Interactive Studio"));

	DlgMainFrame dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}


static bool ScanLogPipeWait(HANDLE readPipe, tchar *buff, int &sz)
{
  bool fake=true;
  bool *p=&fake;
  int datacnt=0;
  do
  {
    if (buff[0])
    {
      p=(bool *)alloca(1);
      *p=false;
      tchar *block=(tchar *)alloca((sz+1)*sizeof(tchar *));
      memcpy(block,buff,sz*sizeof(tchar *));
      block[sz]=0;
      PostMessage(theApp.m_pMainWnd->GetSafeHwnd(),WM_LOGMESSAGE,(WPARAM)p,(LPARAM)block);
      datacnt+=1+sz*sizeof(tchar *);
      if (datacnt>256*1024) Sleep(1000);
    }
    DWORD rd=0;        
    ReadFile(readPipe,buff,256*sizeof(*buff),&rd,0);
    if (rd==0) return false;
    sz=rd/sizeof(*buff);
  }
  while (!*p && datacnt<300*1024);
  return true;
}

static UINT ScanLogPipe(LPVOID data)
{
  HANDLE readPipe=(HANDLE)data;
  tchar buff[256]="";
  int sz;
  while (ScanLogPipeWait(readPipe,buff,sz));
  return 0;
}

void Application::InitLogHandles()
{
  HANDLE rdp;
  CreatePipe(&rdp,&_logHandle,0,0);
  AfxBeginThread(ScanLogPipe,(LPVOID)rdp);
}

void Application::LogF(const char *format, va_list argptr)
{
  CString text;
  text.FormatV(CharToTCHAR(format),argptr);
  DWORD wr;
  WriteFile(_logHandle,text.GetString(),text.GetLength()*sizeof(*text.GetString()),&wr,0);
  WriteFile(_logHandle,_T("\r\n"),2*sizeof(tchar),&wr,0);
}