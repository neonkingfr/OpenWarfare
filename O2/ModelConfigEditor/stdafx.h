// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0500		// Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0500		// Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0510 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 4.0 or later.
#define _WIN32_IE 0x0500	// Change this to the appropriate value to target IE 5.0 or later.
#endif

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

static inline char *WideToChar(const wchar_t  *text, char *space)
{
  int len=(int)wcslen(text);
  WideCharToMultiByte(CP_THREAD_ACP,0,text,len,space,len,NULL,NULL);
  space[len]=0;
  return space;
}

static inline wchar_t *CharToWide(const char  *text, wchar_t *space)
{
  int len=(int)strlen(text);
  MultiByteToWideChar(CP_THREAD_ACP,0,text,len,space,len);
  space[len]=0;
  return space;
}

#ifdef _UNICODE
#define TCHARToChar(tchar) WideToChar(tchar,(char *)alloca(wcslen(tchar)+1));
#define CharToTCHAR(schar) WideToChar(schar,(wchar_t *)alloca((strlen(wide)+1)*sizeof(wchar_t));
#else
#define TCHARToChar(tchar) ((const char *)tchar)
#define CharToTCHAR(schar) ((const char *)schar)
#endif

#include <projects\bredy.libs\ExtendedPropertySheet\PropShCommon.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropShBase.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropShCanvas.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropShCombo.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropSheetWindow.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropShFileLine.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropShFrameGroup.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropShGridGroup.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropShGroup.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropShCheckBox.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropShItem.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropShPanelGroup.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropShSliderLine.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropShSpace.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropShTabGroup.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropShTextLine.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropShTextWithButton.h>
#include <projects\bredy.libs\ExtendedPropertySheet\PropShTitleOnly.h>
#include "PropShGroupWithSwitches.h"
#include "PropShComboEdit.h"
#include ".\propshbutton.h"
