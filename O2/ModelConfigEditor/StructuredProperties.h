#pragma once

#include <El/BTree/BTree.h>
#include <Es/Types/Pointers.hpp>

template<class Key>
class StructuredPropSection;

template<class Key>
class NamedStructProp: public RefCount
{
  Key _name;
  const StructuredPropSection<Key> *_owner;
public:
  NamedStructProp(const Key &name):_name(name),_owner(0) {};
  NamedStructProp():_owner(0) {}
  NamedStructProp(const NamedStructProp<Key> &other): _name(other._name),_owner(0) {}
  NamedStructProp& operator=(const NamedStructProp<Key> &other) {_name=other._name;return *this;}

  const Key &GetName() const {return _name;}
  void SetName(const Key &name) {_name=name;}

  void SetOwner(const StructuredPropSection<Key> *owner) {_owner=owner;}
  const StructuredPropSection<Key> *GetOwner() {return _owner;}
  
  
  virtual StructuredPropSection<Key> *GetSectionPtr() {return 0;}
  
  template<class Archive>
  void Serialize(Archive &arch)
  {
    arch("name",_name);
  }

};

template <class Key>
class NamedStructPropRef: public Ref<NamedStructProp <Key> >
{

#define DECLARE_COMPARE(op) \
  bool operator op(const NamedStructPropRef<Key> &other) const \
    {\
    if (GetRef()->GetOwner()==other->GetOwner()) return GetRef()->GetName() op other->GetName();\
    else return GetRef()->GetOwner() op other->GetOwner();\
    }

public:
  NamedStructPropRef(NamedStructProp <Key> *source):Ref<NamedStructProp <Key> >(source) {}
  NamedStructPropRef<Key>( const NamedStructPropRef<Key> &sRef ):Ref<NamedStructProp <Key> >(sRef) {}
  NamedStructPropRef<Key>() {}
  NamedStructPropRef<Key> &operator = ( NamedStructProp <Key> *source ) {Ref<NamedStructProp <Key> >::operator =(source);return *this;}
  NamedStructPropRef<Key> &operator = ( const NamedStructPropRef<Key> &sRef ) {Ref<NamedStructProp <Key> >::operator =(sRef);return *this;}
  

  DECLARE_COMPARE(==);
  DECLARE_COMPARE(>=);
  DECLARE_COMPARE(<=);
  DECLARE_COMPARE(!=);
  DECLARE_COMPARE(>);
  DECLARE_COMPARE(<);
#undef DECLARE_COMPARE
};



template <class Key>
class StructuredProperties
{
  BTree<NamedStructPropRef<Key> > _db;
public:

  StructuredProperties(void)
  {
  }

  ~StructuredProperties(void)
  {
  }

  const NamedStructProp<Key> *FindEntry(const StructuredPropSection<Key> &section, const Key &name) const;
  void AddEntry(NamedStructProp<Key> *entry);
  bool DeleteEntry(const StructuredPropSection<Key> &section,const Key &name);
  int SectionSize(const StructuredPropSection<Key> *section) const;
  NamedStructProp<Key> *FindEntryByIndex(const StructuredPropSection<Key> *section, int index) const;
  StructuredPropSection<Key> *GetRootSection(int number=0);

};

template <class Key, class Value>
class StructuredPropValue: public NamedStructProp<Key>
{
  Value _value;
public:
  StructuredPropValue(const Key& name, const Value& value):NamedStructProp<Key>(name),_value(value) {}
  StructuredPropValue() {}
  StructuredPropValue(const StructuredPropValue<Key,Value> &other):NamedStructProp<Key>(name),_value(other._value) {}
  
  StructuredPropValue<Key, Value> &operator=(const StructuredPropValue<Key,Value> &other)
  {NamedStructProp<Key>::operator =(other);_value=other._value; return *this;}
  StructuredPropValue<Key, Value> &operator=(const Value &value) {_value=value;return *this;}
};

template <class Key>
class StructuredPropSection: public NamedStructProp<Key>
{
  friend class StructuredProperties<Key>;
  StructuredProperties<Key> *_refDb;
  const StructuredPropSection<Key> *_base;  

public:
  StructuredPropSection(const Key& name):NamedStructProp<Key>(name),_base(0),_refDb(0) {}
  StructuredPropSection():_base(0),_refDb(0) {}
  StructuredPropSection(const StructuredPropSection<Key>& other):NamedStructProp<Key>(other),_base(0),_refDb(0) {}

  StructuredPropSection<Key> &operator=(StructuredPropSection<Key> &other) {NamedStructProp<Key>::operator =(other);return *this;}
  StructuredPropSection<Key> &operator=(const Key& name) {NamedStructProp<Key>::operator =(name);return *this;}

  const StructuredPropSection<Key> *GetBaseSection() const {return _base;}
  void SetBaseSection(StructuredPropSection<Key> *base) {_base=base;}


  const NamedStructProp<Key> *FindEntry(const Key& name, bool noinherit) const
  {
    NamedStructProp<Key> *p=_refDB->FindEntry(this,name);
    if (p==0 && !noinherit)
    {
      StructuredPropSection *base=GetBaseSection();
      while (base) 
      {
        p=base->FindEntry(name);
        if (p!=0) break;
        base=base->GetBaseSection();
      }
    }
    return p;
  }

  template<class Value>
  bool AddValue(const Key &name, const Value &value)
  {
    bool replaced=DeleteEntry(name);
    StructuredPropValue<Key,Value> *valentry=new StructuredPropValue<Key,Value>(name,value);
    valentry->SetOwner(this);
    _refDb->AddEntry(valentry);
    return replaced;
  }

  bool DeleteEntry(const Key &name)
  {
    return _refDb->DeleteEntry(*this,name);
  }

  bool AddSection(const Key &name, const StructuredPropSection<Key> *extends=0)
  {
    bool replaced=DeleteEntry(key);
    StructuredPropSection<Key> *section=new StructuredPropSection<Key>(name);
    section->_refDb=_refDb;
    section->SetBaseSection(extends);
    _refDb->AddEntry(section);
    return replaced;
  }

  int Size() {return _refDb->SectionSize(*this);}
  NamedStructProp<Key> *operator[] (int i) {return _refDb->FindEntryByIndex(*this, i);}

  class Iterator
  {
    StructuredPropSection<Key> *_curSection;
    int _index;
    int _max;
    BTree<Key> _keys;
  public:
    Iterator(StructuredPropSection<Key> &section) {Restart(section);}
    void Restart(StructuredPropSection<Key> &section)
      {
        _curSection=&section;
        _index=-1;
        _max=_curSection->Size();
        _keys->Clear();
      }
    bool Next()
    { 
      if (_curSection=0) 
      {
        _curSection=_owner;_index=-1;
      }
      do
      {
        _index++;
        while (_index>=_max)
        {
          _curSection=_curSection->GetBaseSection();
          if (_curSection==0) {return false;}
          _max=_curSection->Size();
          _index=0;
        }
        NamedStructProp<Key> *p=(*_curSection)[_index];
      }
      while (_keys->Find(p->GetKey())!=0);
      _keys->Add(p->GetKey());
      return true;
    }

    NamedStructProp<Key> *GetEntry()
    {
      if (_curSection==0 || _index<0) return 0;
      return (*_curSection)[_index];
    }
  };

  virtual StructuredPropSection<Key> *GetSectionPtr() {return this;}

};


template <class Key>
const NamedStructProp<Key> *StructuredProperties<Key>::FindEntry(const StructuredPropSection<Key> &section, const Key &name) const
{
  NamedStructProp<Key> keysearch(name);
  keysearch.AddRef();
  keysearch.SetOwner(&section);
  NamedStructPropRef<Key> *found=_db.Find(NamedStructPropRef<Key>(&keysearch));
  if (found) return found->GetRef();
  else return 0;
}

template <class Key>
void StructuredProperties<Key>::AddEntry(NamedStructProp<Key> *entry)
{
  if (entry)
  {
    _db.Add(NamedStructPropRef<Key>(entry));
  }
}

template <class Key>
bool StructuredProperties<Key>::DeleteEntry(const StructuredPropSection<Key> &section,const Key &name)
{
  NamedStructProp<Key> keysearch(name);
  keysearch.AddRef();
  keysearch.SetOwner(&section);
  return _db.Remove(NamedStructPropRef<Key>(&keysearch)).NotNull();
}


template <class Key>
int StructuredProperties<Key>::SectionSize(const StructuredPropSection<Key> *section) const
{
  if (_db.Size()==0) return 0;
  const StructuredPropSection<Key> *owner=section;
  int start;
  if (owner==0) start=0;
  else {
    NamedStructProp<Key> tmp;
    tmp.AddRef();
    tmp.SetOwner(reinterpret_cast<const StructuredPropSection<Key> *>(reinterpret_cast<const char *>(owner)-1));
    start=_db.RankOf(NamedStructPropRef<Key>(&tmp));
  }
    NamedStructProp<Key> tmp;
    tmp.AddRef();
    tmp.SetOwner(reinterpret_cast<const StructuredPropSection<Key> *>(reinterpret_cast<const char *>(owner)+1));
   int end=_db.RankOf(NamedStructPropRef<Key>(&tmp))+1;
  return end-start;
}

template <class Key>
NamedStructProp<Key> *StructuredProperties<Key>::FindEntryByIndex(const StructuredPropSection<Key> *section, int index) const
{
  const StructuredPropSection<Key> *owner=section;
  int start;
  if (owner==0) start=0;
  else {
    NamedStructProp<Key> tmp;
    tmp.AddRef();
    tmp.SetOwner(reinterpret_cast<const StructuredPropSection<Key> *>(reinterpret_cast<const char *>(owner)-1));
    start=_db.RankOf(NamedStructPropRef<Key>(&tmp));
  }
  if (start+index>=_db.Size()) return 0;
  NamedStructPropRef<Key> *itm=_db.ItemWithRank(start+index);
  NamedStructProp<Key> *res=itm->GetRef();
  if (res->GetOwner()!=section) return 0;
  return res;
}

template <class Key>
StructuredPropSection<Key> *StructuredProperties<Key>::GetRootSection(int number)
{
  int max=SectionSize(0);
  while (number>=max) 
  {
    StructuredPropSection<Key> *p=new StructuredPropSection<Key>;
    p->_refDb=this;
    AddEntry(p);
    max++;
  }
  return FindEntryByIndex(NULL,number)->GetSectionPtr();
}
