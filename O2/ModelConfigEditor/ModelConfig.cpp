#include "StdAfx.h"
#include ".\modelconfig.h"
#include "ParamEntry_DirectAccess.h"
#include "SafeEntryRef.h"

#define CFGSKELETONS "CfgSkeletons"
#define CFGMODELS "CfgModels"

bool ModelConfig::Add(const Pathname &fname)
{
  bool res=false;
  if (fname.TestFile(Pathname::TFCanRead))
  {
    ParamFile *base=new ParamFile;
    if (base->Parse(fname)==LSOK)      
    {
      _stages.Append()=base;      
      res=true;
    }
    else
      delete base;
  }    
  return res;
}

bool ModelConfig::Load(const Pathname &fname)
{
  _contextTransform.Clear();
  _relations.Clear();
  _stages.Clear();
  Pathname pthnm(fname);
  Pathname helper=fname;
  int bufsize=_tcslen(fname)+1;
  _TCHAR *buffer=(_TCHAR *)alloca(bufsize*sizeof(_TCHAR));
  bool lastres=false;

  int index=0;
  helper.SetFiletitle(_T("model"));
  while (pthnm.GetPart(index,buffer,bufsize,-1))
  {
    helper.SetDirectory(buffer);
    lastres=Add(helper);
    index++;
  }
  if (_stricmp(pthnm,helper)!=0)
  {    
    lastres=Add(pthnm);
  }
  if (lastres!=true) return false; //last config must be loaded.

  Update();
  return true;
}

bool ModelConfig::Update()
{
  _contextTransform.Clear();
  _relations.Clear();
  Clear();
  for (int i=0,cnt=_stages.Size();i<cnt;++i)  
    ParamFile::Update(*_stages[i]);
  Save("t:\\result.cfg");

  CreateEvaluator(NULL);
  SetFile(this);

  RebuildRelationDB();

  _contextTransform.Clear();
  bool res=FillUpDatabase(ParamClassPtr(this));
  if (res==false)
    Fail("Update failed, inconsitent merged config with originals");  
  return res;
}

static ParamEntry *FindEntryInConfig(ParamClass *owner, ParamEntry *entry, ParamFile &pfile, ParamEntry *& orgparent)
{
  ParamEntry *hlp=entry;
  int needspace=0;
  while (hlp!=0)
  {
    needspace++;
    if (hlp==entry) hlp=owner;
    else hlp=const_cast<ParamEntry *>(hlp->FindParent().GetPointer());
  }
  if (needspace==0) return 0;
  const ParamEntry **list=(const ParamEntry **)alloca(needspace*sizeof(ParamEntry *));
  hlp=entry;
  int index=needspace;
  while (hlp!=0)
  {
    index--;
    list[index]=hlp;
    if (hlp==entry) hlp=owner;
    else hlp=const_cast<ParamEntry *>(hlp->FindParent().GetPointer());
  }
  
  entry=&pfile;
  for (int i=1;i<needspace;++i)
  {
    if (entry->IsClass())
    {
      if (entry->IsDefined())
      {
        orgparent=entry;
        entry=orgparent->FindEntryNoInheritance(list[i]->GetName()).GetPointer();
        if (entry==0) return entry;        
      }
      else return 0;
    }
    else return 0;
  }
  if (entry->IsClass() && !entry->IsDefined()) return 0;
  return entry;

}

bool ModelConfig::FillUpDatabase(ParamClassPtr pclass)
{
  if (pclass->IsDefined())
    for (int i=0,cnt=pclass->GetEntryCount();i<cnt;++i)
    {
      ParamEntryVal entryval=pclass->GetEntry(i);
      ParamEntry *entry=const_cast<ParamEntry *>(entryval.GetPointer());
      ParamEntry *origin=0;
      ParamEntry *originParent=0;
      for (int j=_stages.Size();j-- && origin==0;)    
        origin=FindEntryInConfig(pclass.GetPointer(),entry,*_stages[j],originParent);      
      if (!origin) 
      {
        LogF("Error in config. Item with name '%s' was not found. Inconsistent model config.",entry->GetName().Data());
        continue;
      }
/*      else
      {
        LogF("Update Database: Item %25s found derived from %s",entry->GetName().Data(),originParent->GetClassInterface()->GetRoot()->GetName().Data());
      }*/
      PFEntryItem dbitem;
      dbitem._merged=ParamEntryPtr(pclass.GetPointer(),entry);
      dbitem._origin=ParamEntryPtr(originParent->GetClassInterface(),origin);
      _contextTransform.Add(dbitem);
      if (entry->IsClass())
        if (FillUpDatabase(ParamClassPtr(entry->GetClassInterface()))==false) 
          return false;
    }
  return true;
}

void ModelConfig::ReportEntry(ConstParamEntryPtr pentry) const
{
  if (pentry.IsNull())
  {
    LogF("ReportEntry detects NULL entry");
    return;
  }
  LogF("Reporting entry: %s",pentry->GetName().Data());
  LogF("\tEntry value: \"%s\"", pentry->GetValue().Data());
  ParamEntryPtr org=Transform(pentry);
  LogF("\tDefined by:");
  if (org.IsNull())
  {
    LogF("\t\tEntry was not found in database - Update database needed");
  }
  else
  {
    ParamEntry *origin=0;
    ParamEntry *originParent=0;
    for(int i=0,cnt=_stages.Size();i<cnt;++i)
    {
      origin=FindEntryInConfig(org.GetClass(),org.GetPointer(),*_stages[i],originParent);     
      if (origin) LogF("\t\t%s",_stages[i]->GetName().Data());         
    }
  
    LogF("\tPath:");
    ParamClassPtr parent=pentry.GetClass();
    while (!parent.IsNull())
    {
      ParamClassPtr org=Transform(parent);
      if (org.IsNull()) LogF("\t\t%s - No database record",parent->GetName());
      else
        LogF("\t\t%s defined at %s",org->GetName().Data(),org->GetRoot()->GetName().Data());
      const ParamEntry *pp=parent->FindParent().GetPointer();
      parent=pp?pp->GetClassInterface():0;
    }
    if (pentry->IsClass())
    {
      LogF("\tBase classes:");
      ParamEntryPtr entry=pentry->GetClassInterface()->FindBase();
      if (entry.NotNull())
      {
        ParamClassPtr base=entry->GetClassInterface();
        while (!base.IsNull())
        {
          ParamClassPtr org=Transform(base);
          if (org.IsNull()) LogF("\t\t%s - No database record",base->GetName().Data());
          else
            LogF("\t\t%s defined at %s",org->GetName().Data(),org->GetRoot()->GetName().Data());
          const ParamEntry *pp=base->FindBase().GetPointer();
          base=pp?pp->GetClassInterface():0;
        }
      }
    }
  }
    

}

ParamEntryPtr ModelConfig::Transform(ParamEntryPtr fromMerged) const
{
  PFEntryItem search,*found;
  search._merged=fromMerged;
  found=_contextTransform.Find(search);
  if (found==0) return ParamEntryPtr();  
  return found->_origin;
}


void ModelConfig::GetSelections(ConstParamClassPtr skeleton, BTree<SkeletonInfo> &selContainer, bool isderived)
{
  ParamEntryPtr bones=skeleton->FindEntry("skeletonBones");
  if (bones.IsNull() || !bones->IsArray()) return;
  
  for (int i=0;i<bones->GetSize();i+=2)
  {
    SkeletonInfo nfo;
    nfo.selName=bones->operator [](i).GetValue();
    nfo.parent=bones->operator [](i+1).GetValue();
    nfo.derived=isderived;
    if (selContainer.Find(nfo)==NULL)
    {
      selContainer.Add(nfo);
    }
  }
}


ParamClassPtr ModelConfig::FindModelDefinition(const RString *modelName) const
{
  ParamEntryPtr entry=FindEntry(CFGMODELS);
  if (entry.IsNull() || !entry->IsClass()) return false;
  ParamClassPtr pclass=entry->GetClassInterface();
  if (modelName)
  {
    ParamEntryPtr entry=pclass->FindEntry(*modelName);
    if (entry.IsNull() || !entry->IsClass()) return false;
    return ParamClassPtr(entry->GetClassInterface());
  }
  for(int i=0,cnt=pclass->GetEntryCount();i<cnt;i++)
  {
    ParamEntryVal entry=pclass->GetEntry(i);
    if (entry.IsClass() && !entry.FindEntry("skeletonName").IsNull())
    {
      return ParamClassPtr(entry.GetClassInterface());
    }
  }
  return ParamClassPtr();

}

ParamClassPtr ModelConfig::FindSkeletonDefinition(ConstParamClassPtr foundAnimations,bool excludeUpper)
{
  ParamEntryPtr entry=excludeUpper?foundAnimations->FindEntryNoInheritance("skeletonName"):foundAnimations->FindEntry("skeletonName");
  if (entry.IsNull()) return ParamClassPtr();
//  ReportEntry(entry);
  RString name=entry->GetValue();
  if (name==RString()) return ParamClassPtr();

  entry=FindEntry(CFGSKELETONS);
  if (entry.IsNull() || !entry->IsClass()) return ParamClassPtr();
  ParamClassPtr pclass=entry->GetClassInterface();

  entry=pclass->FindEntry(name);
  if (entry.IsNull() || !entry->IsClass()) return ParamClassPtr();

  return ParamClassPtr(entry->GetClassInterface());
}

bool ModelConfig::CreateSelectionList(BTree<SkeletonInfo> &list, const RString *modelName)
{
  ParamClassPtr model=FindModelDefinition(modelName);
  if (model.IsNull()) 
  {
    LogF("Error: CreateSelectionList failed - model definition not found in config");
    return false;
  }
  
  ParamClassPtr skeleton=FindSkeletonDefinition(model);
  if (skeleton.IsNull()) 
  {
    LogF("Error: CreateSelectionList failed - skeleton definition not found in config");
    return false;
  }
  
  RString inherit;

  GetSelections(skeleton,list,false);
  do
  {
    ParamEntryPtr entry1=skeleton->FindEntry("skeletonInherit");
    if (entry1.NotNull())
    {
      RString name=entry1->GetValue();
      if (name.GetLength()!=0)
      {
        ParamEntryPtr entry2=skeleton->GetClassInterface()->GetParent()->FindEntry(name);
        if (entry2.NotNull() && entry2->IsClass())
        {
          skeleton=entry2->GetClassInterface();
          GetSelections(skeleton,list,true);
        }
        else
          skeleton=ParamClassPtr();
      }
      else
        skeleton=ParamClassPtr();
    }
    else
      skeleton=ParamClassPtr();
  }
  while (!skeleton.IsNull());  
  return true;
}
/*
static ConstParamClassPtr  SearchClassRecursive(ParamFile &modelCfg,ConstParamClassPtr cls)
{
  ConstParamClassPtr parent=cls->GetParent();  
  if (parent.IsNull() || parent->GetParent()==0)
  {
    ParamEntryPtr entry=modelCfg.FindEntry(cls->GetName());
    if (entry.NotNull() && entry->IsClass() && entry->GetClassInterface()->IsDefined())
    {
      ConstParamClassPtr fnd=entry->GetClassInterface();
      return fnd;
    }
  }
  else
  {
    ConstParamClassPtr fnd=SearchClassRecursive(modelCfg,parent);
    if (fnd.IsNull()) return fnd;
    ParamEntryPtr entry=fnd->FindEntry(cls->GetName());
    if (entry.NotNull() && entry->IsClass())
    {
      ConstParamClassPtr fnd=entry->GetClassInterface();
      return fnd;
    }
  }
  return ConstParamClassPtr();
}

ConstParamClassPtr ModelConfig::GetBaseClass(ConstParamClassPtr pclass) const
{
  ConstParamEntryPtr baseEntry=pclass->FindBase();
  if (baseEntry.NotNull())
  {
    ConstParamClassPtr base=baseEntry->GetClassInterface();
    if (base->IsDefined()) return base;
    for (int i=0,cnt=_inherited.Size();i<cnt;i++)
    {
      ConstParamClassPtr res=SearchClassRecursive(*_inherited[i].GetRef(),base);
      if (!base.IsNull()) return res;
    }
  }
  return ConstParamClassPtr();
}

ParamEntryPtr ModelConfig::FindEntryInherited(ConstParamClassPtr pclass,const char *name) const
{
  if (!pclass->IsDefined())
  {
    ConstParamClassPtr base;
    for (int i=0,cnt=_inherited.Size();i<cnt;i++)
    {
      base=SearchClassRecursive(*_inherited[i].GetRef(),pclass);
      if (!base.IsNull()) 
      {
        pclass=base;
        break;
      }
    }
    if (base.IsNull()) return ParamEntryPtr();
    pclass=base;    
  }
  ParamEntryPtr entry=pclass->FindEntryNoInheritance(name);
  while (entry.IsNull())
  {
    if (entry.IsNull())
    {
      for (int i=0,cnt=_inherited.Size();i<cnt;i++)
      {
        ConstParamClassPtr aboveclass=SearchClassRecursive(*_inherited[i].GetRef(),pclass);
        if (!aboveclass.IsNull())
        {
          entry=aboveclass->FindEntryNoInheritance(name);          
          if (entry.NotNull()) break;
        }
      }
    }
    if  (entry.IsNull())
    {
      pclass=GetBaseClass(pclass);    
      if (pclass.IsNull()) return entry;
      entry=pclass->FindEntryNoInheritance(name);
    }
  }
  while (entry.NotNull() && entry->IsClass() && !entry->GetClassInterface()->IsDefined())
  {
    for (int i=0,cnt=_inherited.Size();i<cnt;i++)
    {
      ConstParamClassPtr base=SearchClassRecursive(*_inherited[i].GetRef(),ConstParamClassPtr(entry->GetClassInterface()));
      if (!base.IsNull()) return ParamEntryPtr(base.GetPointer());
    }      
  }
  return entry;
}
*/
bool ModelConfig::EnumerateClassesForSelectionFunctor::operator ()(ParamEntryVal entry) const
  {
    if (entry.IsClass())
    {
      ParamClassPtr cls=entry.GetClassInterface();
      ParamEntryPtr entry=cls->FindEntry("selection");
      if (entry.NotNull())  
      {
        RString name=entry->GetValue();
        if (_stricmp(name,selectionName)==0) classes.Append()=cls;      
      }
    }
    return false;
  }

void ModelConfig::EnumerateClassesForSelection(const RString &selectionName, AutoArray<ParamClassPtr> &classes, const RString *modelName)
{
  ParamClassPtr model=FindModelDefinition(modelName);
  if (model.IsNull()) 
  {
    LogF("Error: EnumerateClassesForSelection - model definition not found in config");    
  }
  
  ParamEntryPtr entry=model->FindEntry("Animations");
  if (entry.NotNull() && entry->IsClass())
  {
    ParamClassPtr pclass=entry->GetClassInterface();
    pclass->ForEachEntry(EnumerateClassesForSelectionFunctor(selectionName,classes));
  }
}


class EnumModelsFunctor
{
  AutoArray<RString> &list;
public:
  EnumModelsFunctor(AutoArray<RString> &l):list(l) {}
  bool operator ()(ParamEntryVal &val) const
  {
    if (val.IsClass())
    {
      if (val.FindEntry("skeletonName").NotNull())
      {
        list.Append()=val.GetName();
      }
    }
    return false;
  }
};

void ModelConfig::ListAllModels(AutoArray<RString> &list)
{
  ParamEntryPtr entry=FindEntry(CFGMODELS);
  if (entry.IsNull() || !entry->IsClass()) return;
  entry->GetClassInterface()->ForEachEntry(EnumModelsFunctor(list));
}

ParamClassPtr ModelConfig::FindAnimationDefintion(ConstParamClassPtr modelDefinition) const
{
  if (modelDefinition.IsNull()) return ParamClassPtr();
  ParamEntryPtr entry=modelDefinition->FindEntry("Animations");
  if (entry.NotNull() && entry->IsClass() && entry->GetClassInterface()->IsDefined())
    return entry->GetClassInterface();
  else
    return ParamClassPtr();
}

ParamFile *ModelConfig::RegisterSelection(const char *modelName, const char *oldName, const char *newName, const char *newParent)
{
  ParamClassPtr model=FindModelDefinition(&RString(modelName));
  if (model.IsNull()) return 0;
  ParamClassPtr skeleton=FindSkeletonDefinition(model); 
  if (skeleton.IsNull()) return 0;
  ParamEntryPtr bones=skeleton->FindEntry("skeletonBones");  
  ParamEntryPtr srcbones=Transform(bones);
  if (bones.NotNull() && bones->IsArray())
  {
    ParamEntryPtr orgbones=Transform(bones);
    for (int i=0;i<bones->GetSize();i+=2)
    {
      SkeletonInfo nfo;
      nfo.selName=bones->operator [](i).GetValue();
      nfo.parent=bones->operator [](i+1).GetValue();
      if (_stricmp(nfo.selName,oldName)==0)
      {
        if (newName==0 || newName[0]==0)
        {
          orgbones->DeleteValue(i);
          orgbones->DeleteValue(i);
        }
        else
        {
          orgbones->SetValue(i,RStringB(newName));
          if (newParent) orgbones->SetValue(i+1,RStringB(newParent));
        }
        return static_cast<ParamFile *>(const_cast<ParamClass *>(orgbones.GetClass()->GetRoot()->GetClassInterface()));
      }
    }
  }
  if (srcbones.NotNull() && newName && newName[0])
  {
    srcbones->AddValue(RString(newName));
    srcbones->AddValue(RString(newParent));
    return static_cast<ParamFile *>(const_cast<ParamClass *>(srcbones.GetClass()->GetRoot()->GetClassInterface()));
  }
  return 0;

} 
ParamClassPtr  ModelConfig::CreateAnimClassNw(const RString &modelName, const RString & name, const RString & derived, const RString & controler, const RString & selection)
{
  ParamClassPtr pmodel=FindModelDefinition(&RString(modelName));
  if (pmodel.IsNull()) return 0;
  ParamClassPtr pclasses=FindAnimationDefintion(pmodel);
  if (pclasses.IsNull()) return 0;
  if (pclasses->FindEntryNoInheritance(name).NotNull()) return 0;
  ParamClassPtr oclasses=Transform(pclasses)->GetClassInterface();
  if (oclasses.IsNull()) return 0;
  ParamClassPtr newclass=oclasses->AddClass(name,true);
  if (newclass.IsNull()) return 0;
  if (derived[0]) 
  {
    if (newclass->SetBaseEx(derived)==false)
    {
      ImportBaseClass(pclasses,newclass,derived);    
      newclass->SetBaseEx(derived);
    }
    CleanUpAfterImport(newclass);
  }
  if (controler[0]) newclass->Add("source",controler);
  if (selection[0]) newclass->Add("selection",selection);
  return newclass;
}

ParamFile * ModelConfig::CreateAnimClass(const RString &modelName, const RString & name, const RString & derived, const RString & controler, const RString & selection)
{
  ParamClassPtr nwclass=CreateAnimClassNw(modelName, name, derived, controler, selection);
    if (nwclass.IsNull()) return 0;
  return static_cast<ParamFile *>(const_cast<ParamClass *>(nwclass->GetRoot()));
}

ParamClassPtr ModelConfig::FindAnimClass(const RString &modelName, const RString &name)
{
  ParamClassPtr pmodel=FindModelDefinition(&RString(modelName));
  if (pmodel.IsNull()) return ParamClassPtr();
  ParamClassPtr pclasses=FindAnimationDefintion(pmodel);
  if (pclasses.IsNull()) return ParamClassPtr();
  ParamEntryPtr entry=pclasses->FindEntry(name);
  if (entry.NotNull())  
    return ParamClassPtr(entry->GetClassInterface());
  else
    return ParamClassPtr();
}

ParamFile * ModelConfig::DeleteAnimClass(ParamEntryPtr pclass)
{
  pclass=Transform(pclass);
  if (pclass.IsNull()) return 0;
  ParamClassPtr cls=pclass->GetClassInterface();
  if (cls.IsNull()) return 0;
  pclass=0;
  _contextTransform.Clear();
  RString name=cls->GetName();  
  if (cls->GetLockCount()>1) 
  {
    FillUpDatabase(ParamClassPtr(this));
    return 0;
  }  
  cls=cls->GetParent()->GetClassInterface();
  cls->Delete(name);
  return static_cast<ParamFile *>(const_cast<ParamClass *>(cls->GetRoot()));
}

static void RebuildRelationDB(BTree<ClassRelationItem> &db,ParamClassPtr cls)
{
  for (int i=0;i<cls->GetEntryCount();i++)
  {
    ParamEntryVal val=cls->GetEntry(i);
    if (val.IsClass())
    {
      ParamClassPtr p=val.GetClassInterface();
      db.Add(ClassRelationItem(cls,p,ClassRelationItem::Child));
      RebuildRelationDB(db,p);
    }
  }
  ParamEntryPtr base=cls->FindBase();
  if (!base.IsNull()) db.Add(ClassRelationItem(base->GetClassInterface(),cls,ClassRelationItem::Derived));
}

void ModelConfig::RebuildRelationDB()
{
  _relations.Clear();
  ::RebuildRelationDB(_relations,ParamClassPtr(this));
}

bool ModelConfig::CreateNewConfig(const char *fname)
{
  for (int i=0;i<_stages.Size();i++)
  {
    if (_stricmp(_stages[i]->GetName(),fname)==0) return false;
  }
  ParamFile *pf=new ParamFile;
  pf->SetName(fname);
  _stages.Append()=pf;
  pf->AddClass(CFGMODELS);
  pf->AddClass(CFGSKELETONS);
  pf->Save();
  Update();
  return true;
}

static MultiThread::SyncInt Gunique_counter=0;
/*
static void EnsurePathImported(ParamClassPtr motherClass, const char *base, ParamClassPtr defineInClass)
{
  if (motherClass->FindEntryNoInheritance(base).NotNull()) //base is defined in current class
    return;   //we can return, it will solved by EnsureBaseImported function
  if (motherClass->FindEntry(base).NotNull()) //base is defined in some of base classes
  {
    ModelConfig::EnsureBaseImported(
      _stricmp(motherClass->GetBaseName(),motherClass->GetName())==0?
        motherClass->GetParent()->GetClassInterface()->FindBase()->GetClassInterface():
        motherClass->GetParent()->GetClassInterface(),
      motherClass->GetBaseName(),
      defineInClass->GetParent()->GetClassInterface(),
      false);
    ParamEntryPtr bbase=defineInClass->FindBase();
    while (bbase.NotNull() && !bbase->IsDefined()) 
      bbase=bbase->FindBase();    
    if (bbase.IsNull()) 
    {
      defineInClass->SetBaseEx(motherClass->GetBaseName());
      bbase=defineInClass->FindBase();
    }
    EnsurePathImported(motherClass->FindBase()->GetClassInterface(),base,bbase->GetClassInterface());
    return;
  }
  if (motherClass->GetParent()!=0)
  {
    EnsurePathImported(motherClass->GetParent()->GetClassInterface(),base,defineInClass->GetParent()->GetClassInterface());
  }
}
*/

/*bool ModelConfig::EnsureBaseImported(ParamClassPtr motherClass, const char *baseName, ParamClassPtr visibleFor, bool lastUndefined)
{

  if (baseName[0]==0) return true;
  ParamClassPtr ptr=motherClass->AddClass(RString("____temp___",baseName));
  if (ptr->SetBaseEx(baseName)==false) return false;
  int needspace=0;
  EnsurePathImported(motherClass,ptr->GetBaseName(),visibleFor);
  const ParamClass *p=ptr->FindBase()->GetClassInterface();
  while (p->GetParent()!=0) {needspace++;p=p->GetParent();}
  const ParamClass **path=(const ParamClass **)alloca(sizeof(ParamClass *)*needspace);
  p=ptr->FindBase()->GetClassInterface();
  for (int i=needspace-1;i>=0;i--) {path[i]=p;p=p->GetParent();}
  ParamClassPtr root=visibleFor->GetRoot();
  for (int i=0;i<needspace;i++)
  {
    bool createundef=lastUndefined?(i+1==needspace):false;
    ParamEntryPtr entry=root->FindEntry(path[i]->GetName());    
    if (entry.IsNull()) 
    {
      ParamClassPtr newroot=root->AddClass(path[i]->GetName(),false,createundef);
      if (!createundef) 
      {
        const char *newBase=path[i]->GetBaseName();
        if (newBase[0])
        {
          EnsureBaseImported(path[i]->GetParent(),newBase,root,true);
          newroot->SetBaseEx(newBase);
        }
      } 
      root=newroot;
    }
    else if (!entry->IsClass()) 
    {
      motherClass->Delete(ptr->GetName());
      return false;
    }
    else if (!entry->IsDefined() && !createundef)
    {
      ParamClassPtr undefclass=entry->GetClassInterface();
      char buff[20];
      sprintf(buff,"$%d$_",Gunique_counter++);
      static_cast<ParamClass_DirectAccess *>(undefclass.GetPointer())->SetName(RString(buff,undefclass->GetName()));
      ParamClassPtr newdef=root->AddClass(path[i]->GetName(),false,false);
      undefclass->SetBaseEx(path[i]->GetName());
      const char *newBase=path[i]->GetBaseName();
      if (newBase[0])
      {
        EnsureBaseImported(path[i]->GetParent(),newBase,root,true);
        newdef->SetBaseEx(newBase);
      }
      root=newdef;
    }
    else root=entry->GetClassInterface();
  }  
  RString tmpname=ptr->GetName();
  ptr=0;
  motherClass->Delete(tmpname);  
  return true;
}*/
bool ModelConfig::ImportBaseClass(ParamClassPtr parentClass, ParamClassPtr thisClass, const char *baseName)
{
  if (baseName[0]==0) return true;  
  ParamEntryPtr ptr;
  
  if (_stricmp(thisClass->GetName(),baseName)==0)
  {
    if (parentClass->FindBase().IsNull()) return false;
    else 
    {
      parentClass=parentClass->FindBase()->GetClassInterface();
      ptr=parentClass->FindEntry(baseName);
    }
  }
  else
    ptr=parentClass->FindEntry(baseName);  
  while (ptr.IsNull() && parentClass->GetParent()!=0) {parentClass=parentClass->GetParent();ptr=parentClass->FindEntry(baseName);}
  if (ptr.IsNull()) return false;

  SafeEntryRef sref=parentClass.GetPointer();  
  sref.SetPath(RString(sref.GetPath(),RString("\n",baseName)));
  bool imp=sref.ImportEntry(*static_cast<ParamFile *>(const_cast<ParamClass *>(thisClass->GetRoot())));
  if (imp)
  {
    ParamClassPtr ptr=sref(*static_cast<ParamFile *>(const_cast<ParamClass *>(thisClass->GetRoot()))).GetClass()->GetParent();
    ParamClassPtr tmp=parentClass;
    while (!ptr.IsNull() && !tmp.IsNull())
    {
      if  (RStringI(ptr->GetBaseName())!=RStringI(tmp->GetBaseName()))
      {
        if (ptr->SetBaseEx(tmp->GetBaseName())==false)
        {
          ImportBaseClass(tmp->GetParent(),ptr,tmp->GetBaseName());
          if (ptr->SetBaseEx(tmp->GetBaseName())==false) imp=false;
        }
      }
      ptr=ptr->GetParent();
      tmp=tmp->GetParent();
    }
  }
  return imp;
}

void ModelConfig::CleanUpAfterImport(ParamClassPtr visibleFor)
{
  return SafeEntryRef::CleanUpAfterImport(visibleFor);
}
/*
bool ModelConfig::ImportBaseClass(ParamClassPtr parentClass, ParamClassPtr thisClass, const char *baseName)
{  
  if (baseName[0]==0) return true;  
  ParamEntryPtr ptr;
  
  if (_stricmp(thisClass->GetName(),baseName)==0)
  {
    if (parentClass->FindBase().IsNull()) return false;
    else ptr=parentClass->FindBase()->FindEntry(baseName);
  }
  else
    ptr=parentClass->FindEntry(baseName);  
  while (ptr.IsNull() && parentClass->GetParent()!=0) parentClass=parentClass->GetParent();
  if (ptr.IsNull()) return false;
 
  const ParamClass *p=ptr->GetClassInterface();
  if (p->GetParent()!=parentClass)        //pokud je base trida v jine tride nez je ta nase
  {
    ParamEntryPtr e=parentClass->FindEntry(baseName);   //pak se kouknem, zda neni nahodou v nejake nase base tride
    if (e.NotNull() && e->IsClass() && e.GetPointer()!=thisClass.GetPointer()) //ano, je to trida, a nejsem to ja sam
    {
      ParamClassPtr pths=thisClass->GetParent();        
      e=pths->FindEntry(baseName); //ted se koukni, zda je deklarovana i parentu thisClass
      if (e.IsNull())         //nemela by
      {                
        pths->AddClass(baseName,false,true);  //pak ji deklarujeme
        goto cleanUp;
      } //pokud uz tam je, musi se to resit jinak.
    }
  }
  {
  int needspace=0;

  while (p->GetParent()!=0) {needspace++;p=p->GetParent();}
  const ParamClass **path=(const ParamClass **)alloca(sizeof(ParamClass *)*needspace);
  p=ptr->GetClassInterface();
  for (int i=needspace-1;i>=0;i--) {path[i]=p;p=p->GetParent();}
  ParamClassPtr root=thisClass->GetRoot();
  for (int i=0;i<needspace;i++)
  {
    bool createundef=i+1==needspace;
    ParamEntryPtr entry=root->FindEntry(path[i]->GetName());    
    if (entry.IsNull()) 
    {
      ParamClassPtr newroot=root->AddClass(path[i]->GetName(),false,createundef);
      if (!createundef) 
      {
        const char *newBase=path[i]->GetBaseName();
        if (newBase[0])
        {
          ImportBaseClass(path[i]->GetParent(),newroot,newBase);
          newroot->SetBaseEx(newBase);
        }
      } 
      root=newroot;
    }
    else if (!entry->IsClass()) 
    {
      parentClass->Delete(ptr->GetName());
      return false;
    }
    else if (!entry->IsDefined() && !createundef)
    {
      ParamClassPtr undefclass=entry->GetClassInterface();
      char buff[20];
      sprintf(buff,"$%d$_",Gunique_counter++);
      static_cast<ParamClass_DirectAccess *>(undefclass.GetPointer())->SetName(RString(buff,undefclass->GetName()));
      ParamClassPtr newdef=root->AddClass(path[i]->GetName(),false,false);
      undefclass->SetBaseEx(path[i]->GetName());
      const char *newBase=path[i]->GetBaseName();
      if (newBase[0])
      {
        ImportBaseClass(path[i]->GetParent(),newdef,newBase);
        newdef->SetBaseEx(newBase);
      }
      root=newdef;
    }
    else root=entry->GetClassInterface();
  }  
}
cleanUp:
/*  RString tmpname=ptr->GetName();
  ptr=0;
  parentClass->Delete(tmpname);  
  return true;

}*/
ModelConfig::Error ModelConfig::CreateNewModelDef(const char *name, const char *derivedFrom, const char *skeletonSufix, ParamFile **toSave)
{
  {
    ParamEntryPtr entry=FindEntry(CFGMODELS);
    if (entry.IsNull() || !entry->IsClass()) return errCfgModelsNotFound;
    ParamClassPtr cfgModels=entry->GetClassInterface();
    entry=FindEntry(name);
    if (entry.NotNull()) return errAlreadyExists;
    ParamClassPtr o_cfgModels=Transform(cfgModels);
    if (o_cfgModels.IsNull()) return errNoTransform;
    ParamClassPtr newdef=o_cfgModels->AddClass(name);
    if (derivedFrom && derivedFrom[0])
    {
      if (ImportBaseClass(cfgModels,newdef,derivedFrom)==false) return errUnableToImportBase;
      if (newdef->SetBaseEx(derivedFrom)==false) return errUnableToSetBase;  
      CleanUpAfterImport(o_cfgModels);
    }
  }
  Update();
  RString skelName=RString(name,skeletonSufix);  
  {
    ParamClassPtr modelDef=FindModelDefinition(&RString(name));
    ParamClassPtr skelDef=FindSkeletonDefinition(modelDef);
    RString baseSkeleton=skelDef.IsNull()?"":skelDef->GetName();
    ParamEntryPtr entry=FindEntry(CFGSKELETONS);
    if (entry.IsNull()|| !entry->IsClass()) {return errCfgSkeletonsNotFound;}
    ParamClassPtr cfgSkeletons=entry->GetClassInterface();
    while ((entry=FindEntry(skelName)).NotNull()) skelName=RString(skelName,"_");
    ParamClassPtr o_cfgSkeletons=Transform(cfgSkeletons);
    if (o_cfgSkeletons.IsNull()) return errNoTransform;
    ParamClassPtr newdef=o_cfgSkeletons->AddClass(skelName);
    if (ImportBaseClass(cfgSkeletons,newdef,baseSkeleton)==true)
    {
      newdef->SetBaseEx(baseSkeleton);
      newdef->Add("skeletonInherit",baseSkeleton);
    }
    newdef->AddArray("skeletonBones");
    ParamClassPtr o_modelDef=Transform(modelDef);
    o_modelDef->Add("skeletonName",skelName);
    ParamClassPtr animdef=o_modelDef->AddClass("Animations");
    ImportBaseClass(modelDef,animdef,"Animations");
    animdef->SetBaseEx("Animations");
    /*ParamEntryPtr e=modelDef->FindBase();
    if (!e.IsNull())
    {
      ParamClassPtr base=e->GetClassInterface();      
      e=base->FindBase();
      if (e.NotNull())
      {
        ParamClassPtr basebase=e->GetClassInterface();
        EnsureBaseImported(modelDef->GetParent()->GetClassInterface(),basebase->GetName(),o_modelDef->GetParent()->GetClassInterface(),true);
      }
    }*/
    if (toSave) *toSave=static_cast<ParamFile *>(const_cast<ParamClass *>(o_modelDef->GetRoot()));
    CleanUpAfterImport(o_modelDef);

  }
  Update();  
  return errOk;
}

ModelConfig::Error ModelConfig::CreateCloneModelDef(const char *name, const char *derivedFrom,  ParamFile **toSave)
{
  {
    ParamEntryPtr entry=FindEntry(CFGMODELS);
    if (entry.IsNull() || !entry->IsClass()) return errCfgModelsNotFound;
    ParamClassPtr cfgModels=entry->GetClassInterface();
    entry=FindEntry(name);
    if (entry.NotNull()) return errAlreadyExists;
    ParamClassPtr o_cfgModels=Transform(cfgModels);
    if (o_cfgModels.IsNull()) return errNoTransform;
    ParamClassPtr newdef=o_cfgModels->AddClass(name);
    if (derivedFrom && derivedFrom[0])
    {
      if (ImportBaseClass(cfgModels,newdef,derivedFrom)==false) return errUnableToImportBase;
      if (newdef->SetBaseEx(derivedFrom)==false) return errUnableToSetBase;  
      CleanUpAfterImport(o_cfgModels);
    }
    if (toSave) *toSave=static_cast<ParamFile *>(const_cast<ParamClass *>(newdef->GetRoot()));
  }
  Update();
  return errOk;
}


ParamClassPtr ModelConfig::FindModelDefinitionClass() const
{
    ParamEntryPtr entry=FindEntry(CFGMODELS);
    if (entry.IsNull() || !entry->IsClass()) return ParamClassPtr();
    ParamClassPtr cfgModels=entry->GetClassInterface();
    return cfgModels;

}

ParamClassPtr ModelConfig::FindSkeletonDefinitionClass() const
{
    ParamEntryPtr entry=FindEntry(CFGSKELETONS);
    if (entry.IsNull() || !entry->IsClass()) return ParamClassPtr();
    ParamClassPtr cfgModels=entry->GetClassInterface();
    return cfgModels;

}


ModelConfig::Error ModelConfig::DeleteModelDef(const char *name, ParamFile **tosave)
{
  ParamClassPtr cfgModels=FindModelDefinition(&RString(name));
  if (!cfgModels) return errCfgModelsNotFound;
  ParamClassPtr cfgSkeletons=FindSkeletonDefinition(cfgModels,true);


  cfgModels=Transform(cfgModels);
  if (!cfgSkeletons.IsNull()) cfgSkeletons=Transform(cfgSkeletons);


  _contextTransform.Clear();
  _relations.Clear();

  if (cfgModels->GetLockCount()>2) 
  {
    Update();
    return errInUse;
  }
  if (!cfgSkeletons.IsNull() && cfgSkeletons->GetLockCount()>2) 
  {
    Update();
    return errInUse;
  }
  
  if (cfgModels.IsNull()) 
  {
    Update();
    return errNoTransform;
  }
  
  RString modelName=cfgModels->GetName();
  cfgModels=cfgModels->GetParent()->GetClassInterface();
  cfgModels->Delete(modelName);

  if (!cfgSkeletons.IsNull())
  {  
    RString skelName=cfgSkeletons->GetName();
    cfgSkeletons=cfgSkeletons->GetParent()->GetClassInterface();
    cfgSkeletons->Delete(skelName);
  }

  *tosave=static_cast<ParamFile *>(const_cast<ParamClass *>(cfgModels->GetRoot()->GetClassInterface()));
  Update();
  return errOk;
}


void ModelConfig::Reload()
{
  AutoArray<RString> files;
  for (int i=0;i<_stages.Size();i++) files.Append(_stages[i]->GetName());
  _contextTransform.Clear();
  _relations.Clear();
  _stages.Clear();
  for (int i=0;i<files.Size();i++) 
    Add(Pathname(files[i]));
  Update();
}

bool ModelConfig::IsNotDefinedInClassFile(ParamClassPtr owningClass, ParamEntryPtr entry, bool file)
{
  ParamClassPtr ow=entry.GetClass();
  if (file)
  {
    if (ow->GetRoot()!=owningClass->GetRoot()) return true;
    const ModelConfig *mdlcfg=dynamic_cast<const ModelConfig *>(ow->GetRoot());
    if (mdlcfg==0) return false;
    ParamClassPtr o1=mdlcfg->Transform(ParamClassPtr(owningClass->GetRoot()));
    ParamClassPtr o2=mdlcfg->Transform(ParamClassPtr(ow->GetRoot()));
    return o1!=o2;
  }
  else
  {
    if (owningClass->GetEntryCount()==0) 
    {
      ParamEntryPtr e=owningClass->FindBase();
      if (e.IsNull()) return false;
      owningClass=e->GetClassInterface();
    }
    return owningClass!=ow;
  }
}

void ModelConfig::Reset()
{
  _contextTransform.Clear();
  _relations.Clear();
  _stages.Clear();
  __super::Clear();
}

ParamClassPtr ModelConfig::FindInheritedSkeleton(ConstParamClassPtr skeleton)
{
  ParamClassPtr parent=skeleton->GetParent();
  ParamEntryPtr inhr=skeleton->FindEntry("skeletonInherit");
  if (inhr.IsNull() || inhr->GetValue().GetLength()==0) return 0;
  ParamEntryPtr upper=parent->FindEntry(inhr->GetValue());
  if (upper.NotNull() && upper->IsClass()) return upper->GetClassInterface();
  else return 0;
}


bool ModelConfig::IsSelectionExists(const char *name, const char *model)
{
  ParamClassPtr modeldef=FindModelDefinition(&RString(model));
  if (modeldef.IsNull()) return false;
  ParamClassPtr modelanm=FindAnimationDefintion(modeldef);
  if (modelanm.IsNull()) return false;
  ParamClassPtr skelet=FindSkeletonDefinition(modeldef);
  if (skelet.IsNull()) return false;
  while (!skelet.IsNull())
  {
    ParamEntryPtr bones=skelet->FindEntry("skeletonBones");
    if (bones.NotNull() && bones->IsArray())
    {    
      for (int i=0;i<bones->GetSize();i+=2)
        if (_stricmp((RString)(*bones)[i],name)==0) return true;
    }
    skelet=FindInheritedSkeleton(skelet);    
  }
  return false;
}