#pragma once
#include <projects\bredy.libs\extendedpropertysheet\propshitem.h>


#define PROPSHBUTTON_BUTTONACTION 11223

class PropShButton : public PropShItem
{
public: 
  enum Align
  {
    AlignLeft, AlignCenter, AlignRight
  };

  class ButtonAction: public IPropShEvent::ActionData
  {
    int _code;
  public:
    ButtonAction(int command, int code): ActionData(command),_code(code) {}
    int GetCode() {return _code;}
  };

protected:
  HWND _hButton;
  float _fwidth;
  int _width;
  int _height;
  Align _align;
  bool _singleline;
  
    
public:
  PropShButton(Align align=AlignRight, float width=-1.0f, int height=-1, bool singleline=true);
  ~PropShButton(void);

  void SetWidth(float width=-1.0f);
  void SetWidth(int width) {SetWidth((float)width);}
  void SetAlign(Align align) {_align=align;}
  void SetHeight(int height=-1) {_height=height;}

  bool IsWithFixed() const{return _width>0;} 
  int GetFixedWidth() const{return _width;} 
  float GetWidthAspect() const{return _fwidth;} 
  int GetHeight() const{return _height;} 
  Align GetAlign() const{return _align;} 

  bool Create(const tchar *name, const tchar *title, PropShItem *parent=NULL);
  HWND GetWindowHandle() {return _hButton;} //default - no handle
  void BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam);
  virtual void UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height);
  virtual bool ReflectMessage(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam, LRESULT *result);  
  
};
