// ModelConfigEditor.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include <Es/Framework/AppFrame.hpp>
#include <El/Interfaces/iAppInfo.hpp>


// Application:
// See ModelConfigEditor.cpp for the implementation of this class
//

#define WM_LOGMESSAGE (WM_APP+9891)  //WPARAM bool *processed, LPARAM const char *text

class Application : public CWinApp, public AppFrameFunctions
{
    HANDLE _logHandle;

public:
	Application();

    void InitLogHandles();

// Overrides
	public:
	virtual BOOL InitInstance();


    void LogF(const char *format, va_list argptr);
// Implementation

	DECLARE_MESSAGE_MAP()
};

extern Application theApp;