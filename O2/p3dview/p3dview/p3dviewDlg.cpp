// p3dviewDlg.cpp : implementation file
//

#include "stdafx.h"
#include "p3dview.h"
#include "p3dviewDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cp3dviewDlg dialog

static UINT BASED_CODE statusbar[] = {
    ID_INDICATOR_STATUS,
    ID_INDICATOR_VERTICES,
	ID_INDICATOR_FACES
};


Cp3dviewDlg::Cp3dviewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(Cp3dviewDlg::IDD, pParent)
{
	mlodh = NULL;
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

Cp3dviewDlg::~Cp3dviewDlg() {
	if(mlodh)
		delete mlodh;
}

BOOL Cp3dviewDlg::DestroyWindow() {
	WINDOWPLACEMENT wp;
	if(GetWindowPlacement(&wp)) {
		RptF("Saving window position");
		AfxGetApp()->WriteProfileBinary("P3dview", "WindowPosition", (LPBYTE)&wp, sizeof(wp));
	}
	return 1;
}

void Cp3dviewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LODLIST, cLodList);
	DDX_Control(pDX, IDC_PREVMODEL, cButPrev);
	DDX_Control(pDX, IDC_NEXTMODEL, cButNext);
	DDX_Control(pDX, IDC_BA, cButBoxa);
	DDX_Control(pDX, IDC_BB, cButBoxb);
	DDX_Control(pDX, IDC_BC, cButBoxc);
	DDX_Control(pDX, IDC_BD, cButBoxd);
	DDX_Control(pDX, IDC_BE, cButBoxe);
	DDX_Control(pDX, IDC_BGRID, cButBoxGrid);
	DDX_Control(pDX, IDC_FILELIST, cFileList);
	DDX_Control(pDX, IDC_COPYPATH, cButCopypath);
	DDX_Control(pDX, IDC_OPENO2, cButOpeno2);
	DDX_Control(pDX, IDC_SCREENSHOT, cButScreenshot);
	DDX_Control(pDX, IDC_PROPERTIES, cButProperties);
	DDX_Control(pDX, IDC_TEXPATH, cTexPath);
}

BEGIN_MESSAGE_MAP(Cp3dviewDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_NEXTMODEL, &Cp3dviewDlg::OnBnClickedNextmodel)
	ON_BN_CLICKED(IDC_PREVMODEL, &Cp3dviewDlg::OnBnClickedPrevmodel)
	ON_MESSAGE(WM_USER+501, &Cp3dviewDlg::OnModelLoaded)
	ON_MESSAGE(WM_USER+502, &Cp3dviewDlg::OnLODLoaded)
	ON_MESSAGE(WM_USER+503, &Cp3dviewDlg::OnModelList)
	ON_MESSAGE(WM_COPYDATA, &Cp3dviewDlg::OnRequestLoadFile)
	ON_WM_MOUSEMOVE()
	ON_LBN_SELCHANGE(IDC_LODLIST, &Cp3dviewDlg::OnLbnSelchangeLodlist)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BA, &Cp3dviewDlg::OnBnClickedBa)
	ON_BN_CLICKED(IDC_BB, &Cp3dviewDlg::OnBnClickedBb)
	ON_BN_CLICKED(IDC_BC, &Cp3dviewDlg::OnBnClickedBc)
	ON_BN_CLICKED(IDC_BD, &Cp3dviewDlg::OnBnClickedBd)
	ON_BN_CLICKED(IDC_BE, &Cp3dviewDlg::OnBnClickedBe)
	ON_BN_CLICKED(IDC_BGRID, &Cp3dviewDlg::OnBnClickedBgrid)
	ON_WM_ERASEBKGND()
	ON_WM_SIZING()
	ON_LBN_SELCHANGE(IDC_FILELIST, &Cp3dviewDlg::OnLbnSelchangeFilelist)
	ON_BN_CLICKED(IDC_COPYPATH, &Cp3dviewDlg::OnBnClickedCopypath)
	ON_WM_DROPFILES()
	ON_EN_CHANGE(IDC_TEXPATH, &Cp3dviewDlg::OnEnChangeTexpath)
END_MESSAGE_MAP()


// Cp3dviewDlg message handlers

BOOL Cp3dviewDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	DragAcceptFiles(true);

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	cView.Create(NULL, _T(""), WS_VISIBLE, CRect(10,11,512,512), this, 1);
	mlodh = new mlodloader(m_hWnd, cView.m_hWnd);
	cView.setMlodh(mlodh);

	if(__argc > 1)
		mlodh->loadModel(__argv[1]);
	cLodList.ResetContent();

	RECT r;
	GetClientRect(&r);
	lW = r.right;
	lH = r.bottom;

	m_bar.Create(this); 
	m_bar.SetIndicators(statusbar, 2);

	m_bar.SetPaneInfo(0, ID_INDICATOR_STATUS, SBPS_NORMAL, 200); 
	m_bar.SetPaneInfo(1, ID_INDICATOR_VERTICES, SBPS_STRETCH, 200);
	//m_bar.SetPaneInfo(2, ID_INDICATOR_FACES, SBPS_STRETCH, 200);
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST,AFX_IDW_CONTROLBAR_LAST,ID_INDICATOR_STATUS);

#define BSETBMP(x,y) {CBitmap *t = new CBitmap;t->LoadBitmap(y);x.SetBitmap(*t);};
	BSETBMP(cButBoxa, IDB_BOXA);
	BSETBMP(cButBoxb, IDB_BOXB);
	BSETBMP(cButBoxc, IDB_BOXC);
	BSETBMP(cButBoxd, IDB_BOXD);
	BSETBMP(cButBoxe, IDB_BOXE);
	BSETBMP(cButBoxGrid, IDB_GRID);

	UpdateTitle();

	// Get tools path from registry
	HKEY hKey;
	char tpath[256] = ".";
	DWORD len = 256;
	if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, "Software\\Bohemia Interactive Studio\\VBS2_Tools", 0, KEY_READ, &hKey) == ERROR_SUCCESS) {
		RegQueryValueEx(hKey, "DriveLetter", NULL, NULL, (LPBYTE)&tpath, &len);
		RegCloseKey(hKey);
	}
	cTexPath.SetWindowTextA(tpath);
	mlodh->setProxyPath(tpath);
	mlodh->setTexturePath(tpath);

	WINDOWPLACEMENT *wp = NULL;
	UINT dlen;
	AfxGetApp()->GetProfileBinary("P3dview", "WindowPosition", (LPBYTE*) &wp, &dlen);
	if(dlen == sizeof(WINDOWPLACEMENT) && wp) {
		SetWindowPlacement(wp);
	}

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void Cp3dviewDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR Cp3dviewDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void Cp3dviewDlg::UpdateTitle() {
	char t[512];
	if(strlen(mlodh->GetModelName()) > 1) 
		snprintf(t, 512, "%s (%d/%d) - P3Dview", mlodh->GetModelName(), mlodh->getModelNumber(), mlodh->getNumModels());
	else
		snprintf(t, 512, "No model loaded - P3Dview");
	(AfxGetMainWnd( ))->SetWindowText(t);

	// Update status bar
	ObjectData *lod = mlodh->getLOD();	
	if(lod) {
		char str[512];
		snprintf(str, 512, "%d vertices, %d faces", lod->NPoints(), lod->NFaces());
		m_bar.SetPaneText(0, str);
	} else
		m_bar.SetPaneText(0, "Drag model to open");

	char str[512];
	snprintf(str, 512, "Projection: %s (Press tab to switch)", mlodh->GetProjection()?"Orthographic":"Perspective");
	m_bar.SetPaneText(1, str);
}

LRESULT Cp3dviewDlg::OnModelList(WPARAM wParam, LPARAM lParam) {
	// Sent from mlodloader when new model list is available	
	cFileList.ResetContent();

	int nm = mlodh->getNumModels();
	for(int i=0;i<nm;i++) {
		const char *name = mlodh->GetModelName(i);
		char *fname = strrchr((char*)name, '\\');
		cFileList.AddString(fname?fname+1:name);
	}	
	cFileList.SetCurSel(mlodh->getModelNumber());
	return true;
}

LRESULT Cp3dviewDlg::OnRequestLoadFile(WPARAM wParam, LPARAM lParam) {
	// Request to load a p3d file, from another p3dview instance (double click on p3d model)
	COPYDATASTRUCT *cp = (COPYDATASTRUCT*) lParam;
	RptF("LOAD: <%s>", cp->lpData);
	mlodh->loadModel((char*) cp->lpData);
	return true;
}

LRESULT Cp3dviewDlg::OnModelLoaded(WPARAM wParam, LPARAM lParam) {
	// Sent from mlodloader when a model has been loaded
	cLodList.ResetContent();

	if(wParam) {
		// Model loaded
		LODObject *obj = mlodh->getModel();
		if(obj) {
			for(int i=0;i<obj->NLevels();i++)  {
				const char *name = mlodh->getLODname(obj->Resolution(i));
				cLodList.AddString(name);
				RptF("LOD %d: resolution: %s", i, name);	
			}
		}
	} else {
		// Model cleared
		cLodList.AddString("Loading...");
		m_bar.SetPaneText(0, " ");
	}

	UpdateTitle();
	return true;
}

LRESULT Cp3dviewDlg::OnLODLoaded(WPARAM wParam, LPARAM lParam) {
	// Sent from mlodloader when requested LOD has been successfully loaded
	UpdateTitle();
	return true;
}

void Cp3dviewDlg::OnBnClickedNextmodel() {
	if(mlodh) mlodh->nextModel();
	UpdateTitle();
}

void Cp3dviewDlg::OnBnClickedPrevmodel() {
	if(mlodh) mlodh->prevModel();
	UpdateTitle();
}

void Cp3dviewDlg::OnMouseMove(UINT nFlags, CPoint p) {	
	/*
	static int lx = p.x;
	static int ly = p.y;

	if(nFlags & MK_RBUTTON) {
		int dx = p.x - lx;
		int dy = p.y - ly;
		mlodh->AdjustCamera(dx, dy, 0);
	} else if(nFlags & MK_LBUTTON) {
		int dy = p.y - ly;
		mlodh->AdjustCamera(0, 0, dy);
	}
	lx = p.x;
	ly = p.y;
*/
	CDialog::OnMouseMove(nFlags, p);
}

void Cp3dviewDlg::OnLbnSelchangeLodlist() {
	int i = cLodList.GetCurSel();
	mlodh->loadLOD(i);
}

void Cp3dviewDlg::OnSize(UINT nType, int cx, int cy) {
	int dx = cx - lW;
	int dy = cy - lH;

	if(nType == SIZE_MINIMIZED) {
		CDialog::OnSize(nType, cx, cy);
		return;
	}

	// MFC... how hard can it be to have auto-scaling widgets?
#define MFC_REPOS(x,l,r,t,b) {\
	WINDOWPLACEMENT wp;x.GetWindowPlacement(&wp);\
	wp.rcNormalPosition.left += l;\
	wp.rcNormalPosition.right += r;\
	wp.rcNormalPosition.top += t;\
	wp.rcNormalPosition.bottom += b;\
	x.SetWindowPlacement(&wp);\
	x.Invalidate(false);\
}
	MFC_REPOS(cView, 0, dx, 0, dy);

	MFC_REPOS(cLodList, dx, dx, 0, 0);
	MFC_REPOS(cButBoxa, dx, dx, 0, 0);
	MFC_REPOS(cButBoxb, dx, dx, 0, 0);
	MFC_REPOS(cButBoxc, dx, dx, 0, 0);
	MFC_REPOS(cButBoxd, dx, dx, 0, 0);
	MFC_REPOS(cButBoxe, dx, dx, 0, 0);
	MFC_REPOS(cButBoxGrid, dx, dx, 0, 0);

	MFC_REPOS(cFileList, dx, dx, 0, dy);

	MFC_REPOS(cButCopypath, dx, dx, dy, dy);
	MFC_REPOS(cButOpeno2, dx, dx, dy, dy);
	/*MFC_REPOS(cButScreenshot, dx, dx, dy, dy);
	MFC_REPOS(cButProperties, dx, dx, dy, dy);*/
	MFC_REPOS(cButPrev, dx, dx, dy, dy);
	MFC_REPOS(cButNext, dx, dx, dy, dy);
	MFC_REPOS(cTexPath, 0, dx, dy, dy);	
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST,AFX_IDW_CONTROLBAR_LAST,ID_INDICATOR_STATUS);	

	lW = cx;
	lH = cy;
	CDialog::OnSize(nType, cx, cy);
}

void Cp3dviewDlg::OnBnClickedBa() {
	mlodh->SetDrawStyle(mlodh->S_WIRE);
	RedrawWindow();
}

void Cp3dviewDlg::OnBnClickedBb() {
	mlodh->SetDrawStyle(mlodh->S_SOLID);
	RedrawWindow();
}

void Cp3dviewDlg::OnBnClickedBc() {
	mlodh->SetDrawStyle(mlodh->S_SOLIDWIRE);
	RedrawWindow();
}

void Cp3dviewDlg::OnBnClickedBd() {
	mlodh->SetDrawStyle(mlodh->S_TEXTURE);
	RedrawWindow();
}

void Cp3dviewDlg::OnBnClickedBe() {
	mlodh->SetDrawStyle(mlodh->S_TEXTUREWIRE);
	RedrawWindow();
}

void Cp3dviewDlg::OnBnClickedBgrid() {
	mlodh->SetGrid(!mlodh->GetGrid());
	RedrawWindow();
}

void Cp3dviewDlg::OnNMCustomdraw3dview(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}

BOOL Cp3dviewDlg::OnEraseBkgnd(CDC* pDC) {
	// Dont erase 3D view background
	CRect c;
	cView.GetWindowRect(&c);	
	ScreenToClient(&c);
	pDC->ExcludeClipRect(&c);	
	return CDialog::OnEraseBkgnd(pDC);
}

void Cp3dviewDlg::OnSizing(UINT fwSide, LPRECT r) {
	int w = r->right-r->left;
	int h = r->bottom-r->top;
	if(w < 400)
		r->right += 400-w;
	if(h < 400)
		r->bottom += 400-h;
	CDialog::OnSizing(fwSide, r);
}

void Cp3dviewDlg::OnLbnSelchangeFilelist() {
	int i = cFileList.GetCurSel();
	mlodh->setModel(i);
	UpdateTitle();
}

void Cp3dviewDlg::OnBnClickedCopypath() {
	const char *str = mlodh->GetModelName();
	if(!OpenClipboard())
		return;
	EmptyClipboard();
	char *tbuf;
	HGLOBAL cbuf;

	cbuf = GlobalAlloc(GMEM_DDESHARE, strlen(str)+1);
	tbuf = (char*)GlobalLock(cbuf);
	strcpy(tbuf, str);
	GlobalUnlock(cbuf);

	::SetClipboardData(CF_TEXT, cbuf);

	CloseClipboard();

}

BOOL Cp3dviewDlg::PreTranslateMessage(MSG* pMsg) {
	if(pMsg->message==WM_KEYDOWN) {
		if(pMsg->hwnd == cTexPath.m_hWnd)
			return FALSE;
		switch(pMsg->wParam) {
			case VK_TAB:
				mlodh->SetProjection(!mlodh->GetProjection());
				RedrawWindow();
				UpdateTitle();
				return TRUE;
			case '1':
				OnBnClickedBa();
				return TRUE;
			case '2':
				OnBnClickedBb();
				return TRUE;
			case '3':
				OnBnClickedBc();
				return TRUE;
			case '4':
				OnBnClickedBd();
				return TRUE;
			case '5':
				OnBnClickedBe();
				return TRUE;
			case 'G':
				OnBnClickedBgrid();
				return TRUE;
			case VK_RIGHT:
				OnBnClickedNextmodel();
				return TRUE;
			case VK_LEFT:
				OnBnClickedPrevmodel();
				return TRUE;
		}		
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void Cp3dviewDlg::OnDropFiles(HDROP hDropInfo)
{
	
	int nf = ::DragQueryFile(hDropInfo, (int) -1, NULL, 0);
	for(int i=0;i<nf;i++) {
		TCHAR fn[_MAX_PATH];
		::DragQueryFile(hDropInfo, i, fn, _MAX_PATH);
		mlodh->loadModel(fn);
	}

	CDialog::OnDropFiles(hDropInfo);
}

// Texture path changed
void Cp3dviewDlg::OnEnChangeTexpath() {
	char tpath[256];
	cTexPath.GetWindowTextA(tpath, 256);
	mlodh->setProxyPath(tpath);
	mlodh->setTexturePath(tpath, true);
}
