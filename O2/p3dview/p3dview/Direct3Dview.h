#pragma once


#include "mlodhandler.h"

// CDirect3Dview

class CDirect3Dview : public CWnd
{
	DECLARE_DYNAMIC(CDirect3Dview)

public:
	CDirect3Dview();
	virtual ~CDirect3Dview();

	void setMlodh(mlodloader *h);

private:
	mlodloader *mlodh;

protected:
	DECLARE_MESSAGE_MAP()
public:
	void OnMouseMove(UINT nFlags, CPoint p);
	afx_msg void OnPaint();
	BOOL OnEraseBkgnd(CDC* pDC);
};


