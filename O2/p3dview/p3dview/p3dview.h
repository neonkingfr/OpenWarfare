// p3dview.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols

// Cp3dviewApp:
// See p3dview.cpp for the implementation of this class
//

class Cp3dviewApp : public CWinApp
{
public:
	Cp3dviewApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern Cp3dviewApp theApp;