#include "common.h"
#include ".\p3dexporter.h"
#include <el\Evaluator\Express.hpp>
#include <projects\objektivlib\o2scriptlib\o2scriptclass.h>
#include <projects\objektivlib\o2scriptlib\dialogs.h>
#include <projects\objektivlib\o2scriptlib\gdLodObject.h>
#include "ObjectExport.h"
#include <el/pathname/pathname.h>
#include "resource.h"
#include <windowsx.h>
#include <Commdlg.h>



P3DExporter::P3DExporter(void)
{
  _suppressMsg=false;
}

P3DExporter::~P3DExporter(void)
{
}

void *P3DExporter::Create()
{
  return new P3DExporter();
}

MStatus	P3DExporter::reader ( const MFileObject& file,
                             const MString& optionsString,
                             FileAccessMode mode)
{
  return MStatus::kFailure;
}

MStatus P3DExporter::writer ( const MFileObject& file,
                             const MString& optionsString,
                             FileAccessMode mode)
{
  _file=file;
  _optionsString=optionsString;
  _mode=mode;
  return EnterToDialog()?MStatus::kSuccess:MStatus::kFailure;  
}

bool P3DExporter::haveReadMethod () const
{
  return false;
}

bool P3DExporter::haveWriteMethod() const
{
  return true;
}

bool P3DExporter::haveNamespaceSupport() const
{
  return false;
}

MString P3DExporter::defaultExtension () const
{
  return "p3d";
}

MString P3DExporter::filter () const
{
  return "*.p3d";
}

bool P3DExporter::canBeOpened () const
{
  return false;
}

MPxFileTranslator::MFileKind P3DExporter::identifyFile (	const MFileObject& file,
                                                        const char* buffer,
                                                        short size) const
{
  return kCouldBeMyFileType;
}

bool P3DExporter::ExportSelection()
{
  	MStatus status;

	//create an iterator for the selected mesh components of the DAG
	//
	MSelectionList selectionList;
	if (MStatus::kFailure == MGlobal::getActiveSelectionList(selectionList)) {
		MGlobal::displayError("MGlobal::getActiveSelectionList");
		return false;
	}

	MItSelectionList itSelectionList(selectionList, MFn::kMesh, &status);	
	if (MStatus::kFailure == status) {
		MGlobal::displayError("Cannot iterate selection...");
		return false;
	}
    int objcnt=0;
	for(;!itSelectionList.isDone();itSelectionList.next()) objcnt++;
    itSelectionList.reset();
    
    ProgressBar<int> progress(objcnt);


	for (itSelectionList.reset(); !itSelectionList.isDone(); itSelectionList.next()) 
    {
		MDagPath dagPath;

        progress.AdvanceNext(1);
        if (progress.StopSignaled()) break;
  
        if (MStatus::kFailure==itSelectionList.getDagPath(dagPath))
        {
		  MGlobal::displayError("MDagPath::getPath");
        }	  
      else
        ExportNode(dagPath);
    }
	
	return true;
}

bool P3DExporter::ExportNode(const MDagPath &dagPath)
{

	MFnDagNode visTester(dagPath);

	//if this node is visible, then process the poly mesh it represents
	//
	MStatus status;
	if(IsVisible(visTester, status) && MStatus::kSuccess == status) {
		ProcessPolyMesh(dagPath);
	}
    return true;
}



bool P3DExporter::ExportAll()
{
   MStatus status;	
    

   MItDag itDag(MItDag::kDepthFirst, MFn::kMesh, &status);

    if (MStatus::kFailure == status) {
	    MGlobal::displayError("MItDag::MItDag");
	    return false;
    }

    int objcnt=0;
	for(;!itDag.isDone();itDag.next()) objcnt++;
    itDag.reset();
    
    ProgressBar<int> progress(objcnt);

	for(;!itDag.isDone();itDag.next()) 
    {
      progress.AdvanceNext(1);
      if (progress.StopSignaled()) break;
	  MDagPath dagPath;
	  if (MStatus::kFailure == itDag.getPath(dagPath)) {
		  MGlobal::displayError("MDagPath::getPath");
	  }
      else
        ExportNode(dagPath);
    }
    return true;
}

bool P3DExporter::IsVisible(MFnDagNode & fnDag, MStatus& status) 
//Summary:	determines if the given DAG node is currently visible
//Args   :	fnDag - the DAG node to check
//Returns:	true if the node is visible;		
//			false otherwise
{
	if(fnDag.isIntermediateObject())
		return false;

	MPlug visPlug = fnDag.findPlug("visibility", &status);
	if (MStatus::kFailure == status) {
		MGlobal::displayError("MPlug::findPlug");
		return false;
	} else {
		bool visible;
		status = visPlug.getValue(visible);
		if (MStatus::kFailure == status) {
			MGlobal::displayError("MPlug::getValue");
		}
		return visible;
	}
}


#define IDS_LOD_GEOMETRY        "Geometry"
#define IDS_LOD_xxx             "???"
#define IDS_LOD_MEMORY          "Memory"
#define IDS_LOD_LANDCONTACT     "LandContact"
#define IDS_LOD_ROADWAY         "RoadWay"
#define IDS_LOD_PATHS           "Paths"
#define IDS_LOD_HPOINTS         "Hit_points"
#define IDS_LOD_VIEWGEO         "ViewGeometry"
#define IDS_LOD_FIREGEO         "FireGeometry"
#define IDS_LOD_VIEW_GUNNER     "View_Gunner"
#define IDS_LOD_VIEW_PILOT      "View_Pilot"
#define IDS_LOD_VIEW_CARGO      "View_Cargo"
#define IDS_LOD_VIEW_CARGO_GEOMETRY "View_Cargo_Geometry"
#define IDS_LOD_VIEW_CARGO_FGEOMETRY "View_Cargo_FireGeometry"
#define IDS_LOD_VIEW_COMMANDER  "View_Commander"
#define IDS_LOD_VIEW_COMM_GEOMETRY "View_Commander_Geometry"
#define IDS_LOD_VIEW_COMM_FGEOMETRY "View_Commander_FireGeometry"
#define IDS_LOD_VIEW_PILOT_GEOMETRY "View_Pilot_Geometry"
#define IDS_LOD_VIEW_PILOT_FGEOMETRY "View_Pilot_FireGeometry"
#define IDS_LOD_VIEW_GUNNER_GEOMETRY "View_Gunner_Geometry"
#define IDS_LOD_VIEW_GUNNER_FGEOMETRY "View_Gunner_FireGeometry"
#define IDS_LOD_SHADOW          "ShadowVolume"

#define strCockpitGunner IDS_LOD_VIEW_GUNNER
#define strCockpitPilot IDS_LOD_VIEW_PILOT
#define strCockpitCommander IDS_LOD_VIEW_CARGO


#define GetStringRes(ids) ids


float LODNameToResol(const char *name)
{
  if( !strcmpi(name,GetStringRes(IDS_LOD_GEOMETRY)) ) return 1e13f;
  if( !strcmpi(name,GetStringRes(IDS_LOD_MEMORY)) ) return SPEC_LOD*1;
  if( !strcmpi(name,GetStringRes(IDS_LOD_LANDCONTACT)) ) return SPEC_LOD*2;
  if( !strcmpi(name,GetStringRes(IDS_LOD_ROADWAY)) ) return SPEC_LOD*3;
  if( !strcmpi(name,GetStringRes(IDS_LOD_PATHS)) ) return SPEC_LOD*4;
  if( !strcmpi(name,GetStringRes(IDS_LOD_HPOINTS)) ) return SPEC_LOD*5;
  if( !strcmpi(name,GetStringRes(IDS_LOD_VIEWGEO)) ) return SPEC_LOD*6;
  if( !strcmpi(name,GetStringRes(IDS_LOD_FIREGEO)) ) return SPEC_LOD*7;
  if( !strcmpi(name,GetStringRes(IDS_LOD_VIEW_CARGO_GEOMETRY)) ) return SPEC_LOD*8;
  if( !strcmpi(name,GetStringRes(IDS_LOD_VIEW_CARGO_FGEOMETRY)) ) return SPEC_LOD*9;
  if( !strcmpi(name,GetStringRes(IDS_LOD_VIEW_COMMANDER)) ) return SPEC_LOD*10;
  if( !strcmpi(name,GetStringRes(IDS_LOD_VIEW_COMM_GEOMETRY)) ) return SPEC_LOD*11;
  if( !strcmpi(name,GetStringRes(IDS_LOD_VIEW_COMM_FGEOMETRY)) ) return SPEC_LOD*12;
  if( !strcmpi(name,GetStringRes(IDS_LOD_VIEW_PILOT_GEOMETRY)) ) return SPEC_LOD*13;
  if( !strcmpi(name,GetStringRes(IDS_LOD_VIEW_PILOT_FGEOMETRY)) ) return SPEC_LOD*14;
  if( !strcmpi(name,GetStringRes(IDS_LOD_VIEW_GUNNER_GEOMETRY)) ) return SPEC_LOD*15;
  if( !strcmpi(name,GetStringRes(IDS_LOD_VIEW_GUNNER_FGEOMETRY)) ) return SPEC_LOD*16;
  if( !strcmpi(name,GetStringRes(strCockpitGunner)) ) return 1000;
  if( !strcmpi(name,GetStringRes(strCockpitPilot)) ) return 1100;
  if( !strcmpi(name,GetStringRes(strCockpitCommander)) ) return 1200;
  RString sprf=GetStringRes(IDS_LOD_SHADOW);
  if (!strnicmp(name,sprf,sprf.GetLength()))
    {
    float z=0.0f;
    sscanf(name+sprf.GetLength(),"%f",&z);
    return z+10000.0f;
    }
  return (float)atof(name);

}


static bool GetLevelByName(const char *levelName, float &levelId)
{  
  levelId=LODNameToResol(levelName);
  if (levelId==0) return false;
  return true;
}

bool P3DExporter::ProcessPolyMesh(const MDagPath dagPath) 
{
  ProgressBar<int> progress(3);
  ObjectExport export(_masterScale);  
  MStatus status;
  MDagPath path(dagPath);  

  MString meshName;
  MFnMesh mesh(dagPath, &status);
  if (status==MStatus::kFailure) return false;

  if (_fullnames) meshName=path.fullPathName();
  else meshName=mesh.name();

  if (!_suppressMsg)
  {  
  MString note=MString("Processing object: ")+meshName;
  MGlobal::displayInfo(note);
  }
  
  progress.AdvanceNext(1);

  if (!export.ExtractGeometry(mesh,path,_swapfc))
  { 
    MString warning=MString("Unable to export object: ")+meshName;
    MGlobal::displayWarning(warning);
    return false;
  }

 
  progress.AdvanceNext(1);

  export.ExtractWeights(mesh,path);

  progress.AdvanceNext(1);

  MItDependencyGraph itDG(mesh.object(),MFn::kDisplayLayer,
    MItDependencyGraph::kUpstream,
    MItDependencyGraph::kBreadthFirst,
    MItDependencyGraph::kNodeLevel);

  bool LODOk=false;
  MString name=MString("-")+meshName;
  for (;!itDG.isDone();itDG.next())
  {    
    MFnDependencyNode node(itDG.thisNode());
    if (stricmp(name.asChar(),"DefaultLayer")==0)
    {        
        _lObject->Level(0)->Merge(export,name.asChar());
        LODOk=true;
    }
    else
    {
      RString lodname=node.name().asChar();
      lodname.Upper();
      
      if (strnicmp(lodname,"LOD",3)==0)
      {
        const char *lodNameBeg=lodname+3;
        float lodLevel;
        while (*lodNameBeg=='_') lodNameBeg++;
        if (sscanf(lodNameBeg,"%f",&lodLevel)==1 || GetLevelByName(lodNameBeg,lodLevel))
        {
          int lodid=_lObject->FindLevelExact(lodLevel);
          if (lodid==-1) lodid=_lObject->AddLevel(ObjectData(),lodLevel);
          _lObject->Level(lodid)->Merge(export,name.asChar());
        LODOk=true;
        }
      }
    }
    if (!LODOk)
    {    
      MString error=MString("Cannot add object to LOD, unknown layer name: ")+node.name()+MString(" (Skipped)");
      MGlobal::displayError(error);
      LODOk=true;
    }
  }
  if (!LODOk)
  {
    _lObject->Level(0)->Merge(export,name.asChar());
  }
  LODOk=true;


  return true;  
}

class ProgressBarControlSite:public ProgressBarFunctions
{
  HWND _progressBar;
  bool stopped;
public:
  ProgressBarControlSite(HWND progressBar):_progressBar(progressBar)
  {
    SendMessage(progressBar,PBM_SETRANGE,0,MAKELPARAM (0, 1000));
    SendMessage(progressBar,PBM_SETPOS,0,0);
    stopped=false;
  }
  bool ManualUpdate() {return true;} //needs manual update
  void Update() 
  {
    int res=toInt(ReadResult()*1000);
    SendMessage(_progressBar,PBM_SETPOS,res,0);
    MSG msg;
    if (PeekMessage(&msg,0,0,0,PM_REMOVE))
    {
      DispatchMessage(&msg);
    }
    if (stopped==true)
      SignalStop();
  }
};


static float GLastValue=1;
static bool GLastSwap=false;
static RString GScript;
static bool GRotate180y;
static bool GAnimExport;
static bool GFullNames=false;
static LRESULT ExportDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
  HWND combo=GetDlgItem(hWnd,IDC_MASTERSCALE);
  switch (msg)
  {
  case WM_INITDIALOG:
    {
      SetWindowLong(hWnd,DWL_USER,lParam);      
      ComboBox_AddString(combo,"0.0001");
      ComboBox_AddString(combo,"0.001");
      ComboBox_AddString(combo,"0.01");
      ComboBox_AddString(combo,"0.1");
      ComboBox_AddString(combo,"1");
      ComboBox_AddString(combo,"10");
      ComboBox_AddString(combo,"100");
      ComboBox_AddString(combo,"1000");
      char tmp[256];
      sprintf(tmp,"%g",GLastValue);
      SetWindowText(combo,tmp);
      if (GLastSwap) CheckDlgButton(hWnd,IDC_SWAPFACES,BST_CHECKED);
      if (GRotate180y) CheckDlgButton(hWnd,IDC_ROTATEY,BST_CHECKED);
      if (GAnimExport) CheckDlgButton(hWnd,IDC_ANIMATION,BST_CHECKED);
      if (GFullNames) CheckDlgButton(hWnd,IDC_FULLNAMES,BST_CHECKED);
      SetDlgItemText(hWnd,IDC_RUNSCRIPT,GScript);
      return 0;
    }
  case WM_COMMAND:
    switch (LOWORD(wParam))
    {
    case IDOK:
      {
        bool success;
        char buff[256];
        char *endStop;
        GetWindowText(combo,buff,sizeof(buff));
        float scale=(float)strtod(buff,&endStop);
        if (scale==0 || *endStop)
        {
          MessageBeep(MB_ICONEXCLAMATION);
          return 1;
        }
        P3DExporter *self=(P3DExporter *)GetWindowLong(hWnd,DWL_USER);
        ProgressBarControlSite progress(GetDlgItem(hWnd,IDC_PROGRESSBAR));
        ProgressBarFunctions *previous=progress.SelectGlobalProgressBarHandler(&progress);
        
        EnableWindow(GetDlgItem(hWnd,IDOK),FALSE);
        EnableWindow(GetDlgItem(hWnd,IDC_BROWSE),FALSE);
        EnableWindow(combo,FALSE);
        SetCursor(LoadCursor(NULL,IDC_WAIT));
        bool swap=(IsDlgButtonChecked(hWnd,IDC_SWAPFACES) & BST_CHECKED)!=0;
        bool rotatey=(IsDlgButtonChecked(hWnd,IDC_ROTATEY) & BST_CHECKED)!=0;
        bool animation=(IsDlgButtonChecked(hWnd,IDC_ANIMATION) & BST_CHECKED)!=0;
        bool fullnames=(IsDlgButtonChecked(hWnd,IDC_FULLNAMES) & BST_CHECKED)!=0;

        int len=GetWindowTextLength(GetDlgItem(hWnd,IDC_RUNSCRIPT));        
        GetDlgItemText(hWnd,IDC_RUNSCRIPT,GScript.CreateBuffer(len+1),len+1);

        success=self->OnExport(scale,swap,GScript,hWnd,rotatey,animation,fullnames);

        
        progress.SelectGlobalProgressBarHandler(previous);
        EndDialog(hWnd,success?IDOK:IDCANCEL);
        GLastValue=scale;
        GLastSwap=swap;
        GRotate180y=rotatey;
        GAnimExport=animation;
        GFullNames=fullnames;
      }
      return 1;
    case IDCANCEL:
      {
        ProgressBarFunctions::GetGlobalProgressBarInstance()->SignalStop();
        EndDialog(hWnd,IDCANCEL);
      }
      return 1;
    case IDC_BROWSE:
      {
        OPENFILENAME ofn;
        char buff[MAX_PATH];
        GetDlgItemText(hWnd,IDC_RUNSCRIPT,buff,lenof(buff));

        memset(&ofn,0,sizeof(ofn));
        ofn.lStructSize=sizeof(ofn);
        ofn.hwndOwner=hWnd;
        ofn.lpstrFilter="*.bio2s\0*.bio2s\0";
        ofn.lpstrFile=buff;
        ofn.nMaxFile=lenof(buff);
        ofn.Flags=OFN_FILEMUSTEXIST|OFN_HIDEREADONLY|OFN_NOCHANGEDIR;
        
        if (GetOpenFileName(&ofn)==TRUE)
          SetDlgItemText(hWnd,IDC_RUNSCRIPT,buff);
      }
      return 1;
    };
  };
  return 0;
}

static void RotateObject(ObjectData *obj)
{
  for (int i=0;i<obj->NPoints();i++)
  {
    PosT &ps=obj->Point(i);
    ps[0]=-ps[0];
    ps[2]=-ps[2];
  }
}

bool P3DExporter::EnterToDialog()
{
 int i=DialogBoxParam(MhInstPlugin,MAKEINTRESOURCE(IDD_EXPORTDLG),GetForegroundWindow(),(DLGPROC)ExportDlgProc,(LPARAM)this);
 return i==IDOK;
}

static float GetTimeUnitStep(MTime::Unit unit)
{
  switch (unit)
  {
    ///
  case MTime::kInvalid:return 1;
      /// 3600 seconds
  case MTime::kHours:return 3600.0f;
      /// 60 seconds
  case MTime::kMinutes:return 60.0f;
      /// 1 second
  case MTime::kSeconds:return 1;
      /// 1/1000 of a second
  case MTime::kMilliseconds:return 1.0f/1000.0f;
      /// 15 frames per second
  case MTime::kGames:return 1.0f/15.0f;
      /// 24 frames per second
  case MTime::kFilm:return 1.0f/24.0f;
      /// 25 frames per second
  case MTime::kPALFrame:return 1.0f/25.0f;
      /// 30 frames per second
  case MTime::kNTSCFrame:return 1.0f/30.0f;
      /// twice the speed of film (48 frames per second)
  case MTime::kShowScan:return 1.0f/48.0f;
      /// twice the speed of PAL (50 frames per second)
  case MTime::kPALField:return 1.0f/50.0f;
      /// twice the speed of NTSC (60 frames per second)
  case MTime::kNTSCField:return 1.0f/60.0f;
      /// 2 frames per second
  case MTime::k2FPS:return 1.0f/2.0f;
      /// 3 frames per second
  case MTime::k3FPS:return 1.0f/3.0f;
      /// 4 frames per second
  case MTime::k4FPS:return 1.0f/4.0f;
      /// 5 frames per second
  case MTime::k5FPS:return 1.0f/5.0f;
      /// 6 frames per second
  case MTime::k6FPS:return 1.0f/6.0f;
      /// 8 frames per second
  case MTime::k8FPS:return 1.0f/8.0f;
      /// 10 frames per second
  case MTime::k10FPS:return 1.0f/10.0f;
      /// 12 frames per second
  case MTime::k12FPS:return 1.0f/12.0f;
      /// 16 frames per second
  case MTime::k16FPS:return 1.0f/16.0f;
      /// 20 frames per second
  case MTime::k20FPS:return 1.0f/20.0f;
      /// 40 frames per second
  case MTime::k40FPS:return 1.0f/40.0f;
      /// 75 frames per second
  case MTime::k75FPS:return 1.0f/75.0f;
      /// 80 frames per second
  case MTime::k80FPS:return 1.0f/80.0f;
      /// 100 frames per second
  case MTime::k100FPS:return 1.0f/100.0f;
      /// 120 frames per second
  case MTime::k120FPS:return 1.0f/120.0f;
      /// 125 frames per second
  case MTime::k125FPS:return 1.0f/125.0f;
      /// 150 frames per second
  case MTime::k150FPS:return 1.0f/150.0f;
      /// 200 frames per second
  case MTime::k200FPS:return 1.0f/200.0f;
      /// 240 frames per second
  case MTime::k240FPS:return 1.0f/240.0f;
      /// 250 frames per second
  case MTime::k250FPS:return 1.0f/250.0f;
      /// 300 frames per second
  case MTime::k300FPS:return 1.0f/300.0f;
      /// 375 frames per second
  case MTime::k375FPS:return 1.0f/375.0f;
      /// 400 frames per second
  case MTime::k400FPS:return 1.0f/400.0f;
      /// 500 frames per second
  case MTime::k500FPS:return 1.0f/500.0f;
      /// 600 frames per second
  case MTime::k600FPS:return 1.0f/600.0f;
      /// 750 frames per second
  case MTime::k750FPS:return 1.0f/750.0f;
      /// 1200 frames per second
  case MTime::k1200FPS:return 1.0f/1200.0f;
      /// 1500 frames per second
  case MTime::k1500FPS:return 1.0f/1500.0f;
      /// 2000 frames per second
  case MTime::k2000FPS:return 1.0f/2000.0f;
      /// 3000 frames per second
  case MTime::k3000FPS:return 1.0f/3000.0f;
      /// 6000 frames per second
  case MTime::k6000FPS:return 1.0f/6000.0f;
      /// user defined units (not implemented yet)
  case MTime::kUserDef:return 1.0f;
  case MTime::kLast:return 1.0f;
  }
  return 15.0f;
}

static void AddAsAnimFrame(LODObject *src, LODObject *frame, double curTime)
{
  if (frame->NLevels() != src->NLevels())
  {
    MGlobal::displayError("Frame cannot be exported - not same levels");
    return;
  }

  for (int i=0;i<frame->NLevels();i++)
  {
    ObjectData *srco=src->Level(i);
    ObjectData *frameo=frame->Level(i);
    if (srco->NPoints()==frameo->NPoints())
    {
      AnimationPhase phase(srco);
      phase.SetTime((float)curTime);
      phase.Validate();
      for (int i=0;i<frameo->NPoints();i++)      
        phase[i]=frameo->Point(i);
      srco->AddAnimation(phase);
    }
    else
    {
      char buff[256];
      sprintf(buff,"Cannot create frame at level %d (%g) - not same point count",i,frame->Resolution(i));
      MGlobal::displayWarning(buff);
    }
  }
}

bool P3DExporter::OnExport(float masterScale, bool swapfc, RString script, HWND hWnd,bool rotate180y, bool animation,bool fullnames)
{
  ProgressBar<int> pb(1);
  pb.AdvanceNext(1);

  _masterScale=masterScale;
  _swapfc=swapfc;
  _rotate180y=rotate180y;
  _animation=animation;
  _fullnames=fullnames;

  _lObject=new LODObject;
  if (MPxFileTranslator::kExportAccessMode == _mode) 
  {
    if (!ExportAll()) return false;
  } 
  else if (MPxFileTranslator::kExportActiveAccessMode == _mode) 
  {
    if (!ExportSelection()) return false;
  } 
  else 
  {
    return false;
  }

  for (int i=0;i<_lObject->NLevels();i++) 
  {
    _lObject->Level(i)->ClearSelection();
    if (rotate180y) 
    {
      RotateObject(_lObject->Level(i));
    }
  }

  if (_animation)
  {
    _suppressMsg=true;
    pb.SetPos(0);
    pb.AdvanceNext(1);
    SRef<LODObject> save=_lObject;
    MAnimControl animControl;
    MTime curTime=animControl.currentTime();
    MTime minTime=animControl.minTime();
    MTime maxTime=animControl.maxTime();
    double step=animControl.playbackBy();
    char buff[256];
    ProgressBar<double> pb((maxTime-minTime).value());
    for (MTime start=minTime;start<=maxTime;start+=step)
    {
      if (pb.StopSignaled())
      {
        break;
      }
      pb.SetPos(start.value()-minTime.value());
      pb.AdvanceNext(step);
      sprintf(buff,"ANIMATION: Processing frame at time %10.3f",start.value());
      MGlobal::displayInfo(buff);

      animControl.setCurrentTime(start);
      _lObject=new LODObject;
      if (MPxFileTranslator::kExportAccessMode == _mode) 
      {
        if (!ExportAll()) return false;
      } 
      else if (MPxFileTranslator::kExportActiveAccessMode == _mode) 
      {
        if (!ExportSelection()) return false;
      } 
      if (rotate180y) 
      {
        for (int i=0;i<_lObject->NLevels();i++) 
          RotateObject(_lObject->Level(i));
      }

      AddAsAnimFrame(save,_lObject,start.value());
    }
    _lObject=save;
    animControl.setCurrentTime(curTime);
    
  }

  if (pb.StopSignaled())
  {
    return false;
  }

  while (_lObject->Level(0)->NPoints()==0 && _lObject->NLevels()>1) _lObject->DeleteLevel(0);
  Pathname fullname(_file.fullName().asChar());
  if (script.GetLength())
  {
    SetCursor(LoadCursor(0,IDC_WAIT));
    ShowWindow(GetDlgItem(hWnd,IDC_RUNNINGSCRPT),SW_SHOW);
    ShowWindow(GetDlgItem(hWnd,IDC_PROGRESSBAR),SW_HIDE);
    O2ScriptClass o2script;
    o2script.SetLibraryPaths("c:\\bis\\oxygen");
    if (o2script.LoadScript(script))
    {
      GameState gs;
      o2script.PrepareGameState(gs);
      gs.VarSet("this",GameValue(new GdLODObject(new LODObjectRef(_lObject),fullname)));
      GdDialogs::SetParentWindow(&o2script,hWnd);
      o2script.RunScript(gs);
    }
    else
    {
      RString text=RString("Unable to load script: ",o2script.GetPreprocError());
      MGlobal::displayError(text.Data());
    }
  }
  if (_lObject->Save(fullname,9999,false,NULL,NULL)==-1)
    MGlobal::displayError("Object save error. Probably system cannot write to the target file. Check, if file is not checked in");

  _lObject=NULL;
  return true;
}
