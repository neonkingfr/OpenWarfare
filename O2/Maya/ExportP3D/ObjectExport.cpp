#include "common.h"
#include ".\objectexport.h"
#include <projects/ObjektivLib/ObjToolTopology.h>
#include <el/Pathname/pathname.h>




static MObject findShader(const MObject& setNode) 
//Summary:	finds the shading node for the given shading group set node
//Args   :	setNode - the shading group set node
//Returns:  the shader node for setNode if found;
//			MObject::kNullObj otherwise
{

	MFnDependencyNode fnNode(setNode);
	MPlug shaderPlug = fnNode.findPlug("surfaceShader");
			
	if (!shaderPlug.isNull()) {			
		MPlugArray connectedPlugs;

		//get all the plugs that are connected as the destination of this 
		//surfaceShader plug so we can find the surface shaderNode
		//
		MStatus status;
		shaderPlug.connectedTo(connectedPlugs, true, false, &status);
		if (MStatus::kFailure == status) {
			MGlobal::displayError("MPlug::connectedTo");
			return MObject::kNullObj;
		}

		if (1 == connectedPlugs.length()) {return connectedPlugs[0].node();}
	}
	
	return MObject::kNullObj;
}




bool ObjectExport :: ExtractGeometry(MFnMesh &fMesh, MDagPath &fDagPath, bool invert)
{   
		MPointArray			fVertexArray;
//		MColorArray			fColorArray;
		MFloatVectorArray	fNormalArray;
		MString				fCurrentUVSetName;
        MObjectArray        fPolygonSets;
        MObjectArray        fPolygonComponents;
        MStatus             status;

    
  	if (MStatus::kFailure == fMesh.getPoints(fVertexArray, MSpace::kWorld)) {
		MGlobal::displayError("MFnMesh::getPoints"); 
		return false;
	}
    
    ReservePoints(fVertexArray.length());    

    for (int i=0,cnt=fVertexArray.length();i<cnt;i++)
    {
      const MPoint &pt=fVertexArray[i];
      Vector3 vx((float)(-pt.x/pt.w),(float)(pt.y/pt.w),(float)(pt.z/pt.w));
      vx*=_masterScale;
      Point(i).SetPoint(vx);
    }

/*	if (MStatus::kFailure == fMesh.getFaceVertexColors(fColorArray)) {
		MGlobal::displayError("MFnMesh::getFaceVertexColors"); 
		return false;
	}*/

	if (MStatus::kFailure == fMesh.getNormals(fNormalArray, MSpace::kWorld)) {
		MGlobal::displayError("MFnMesh::getNormals"); 
		return false;
	}

    for (int i=0,cnt=fNormalArray.length();i<cnt;i++)
    {
      const MPoint &pt=fNormalArray[i];
      Vector3 vx((float)(pt.x/pt.w),(float)(pt.y/pt.w),(float)(pt.z/pt.w));
      AddNormal(vx);
    }


	if (MStatus::kFailure == fMesh.getCurrentUVSetName(fCurrentUVSetName)) {
		MGlobal::displayError("MFnMesh::getCurrentUVSetName"); 
		return false;
	}
    const char *name=fCurrentUVSetName.asChar();

	//Have to make the path include the shape below it so that
	//we can determine if the underlying shape node is instanced.
	//By default, dag paths only include transform nodes.
	//
    fDagPath.extendToShape();

	//If the shape is instanced then we need to determine which
	//instance this path refers to.
	//
	int instanceNum = 0;
	if (fDagPath.isInstanced())
		instanceNum = fDagPath.instanceNumber();



    MObjectArray shaders;
    MIntArray shindex;
    MObjectArray textures;  //list of texture nodes
    MStringArray rvmats;
    
    fMesh.getConnectedShaders(instanceNum,shaders,shindex);
    
    textures.setLength(shaders.length());  //one texture node for one shader
    rvmats.setLength(shaders.length());
    for (unsigned int i=0,cnt=textures.length();i<cnt;i++)
    {
        MObject surfaceShader=shaders[i];
   	      MItDependencyGraph itDG(surfaceShader, MFn::kFileTexture,
								      MItDependencyGraph::kUpstream, 
								      MItDependencyGraph::kBreadthFirst,
								      MItDependencyGraph::kNodeLevel, 
								      &status);
          if (status==MStatus::kSuccess)
          {
       		    itDG.disablePruningOnFilter();    		
	    	    if (!itDG.isDone()) 
                  {
  			      MObject textureNode = itDG.thisNode();
                  textures[i]=textureNode;
                  }
          }        
          MFnDependencyNode curshader(shaders[i]);
          MPlug rvmat(curshader.findPlug("RVMAT",&status));
            if (status==MStatus::kSuccess)
            {
              MString val;
              rvmat.getValue(val);
              rvmats[i]=val;
            }
         

    }

    MObjectArray sets,comps;
    fMesh.getConnectedSetsAndMembers(instanceNum,sets,comps,false);
    int setcnt=sets.length();
    for (int i=0;i<setcnt;i++)
    {      
      MFnSet setnode(sets[i]);
      if (setnode.restriction()==MFnSet::kVerticesOnly || setnode.restriction()==MFnSet::kNone) //interesting set
      {
          MPlug attribute(setnode.findPlug("ObjektivName",&status));
          if (status==MStatus::kSuccess)
          {
              MString val;
              attribute.getValue(val);
              RString name=val.asChar();              
              MItMeshVertex itv(fDagPath,comps[i],&status);
              if (status==MStatus::kFailure) 
              {
                MGlobal::displayError("MItMeshVertex(fDagPath,comps[i],&status)"); 
                break;
              }
              Selection sel(this);        
              for (;!itv.isDone();itv.next()) sel.PointSelect(itv.index());
              SaveNamedSel(name,&sel);
           
          }    
      }
    }

    MStringArray uvsetlist;
    fMesh.getUVSetNames(uvsetlist);
    
    for (int i=0,cnt=fMesh.numPolygons();i<cnt;i++)
    {
      MVector polyNormal;
      fMesh.getPolygonNormal(i,polyNormal,MSpace::kWorld);
      Vector3 vc(-polyNormal[0],polyNormal[1],polyNormal[2]);

      int indexShader=shindex[i];
      RString material;
      RString texture;
      if (indexShader>=0)
      {
        material=rvmats[indexShader].asChar();
        MObject textureNode = textures[indexShader];
        if (!textureNode.isNull())
        {
          MString texFileName;
          MPlug filenamePlug = MFnDependencyNode(textureNode).findPlug("fileTextureName");
          filenamePlug.getValue(texFileName);
          Pathname texName(texFileName.asChar());
          texture=RString(texName.GetDirectory(),texName.GetFilename()); 
        }
      }


      int fcnx=NFaces();

      if (TessellatePolygon(i,fMesh,texture,material,uvsetlist)==false)
      {
        MGlobal::displayWarning("Polygon tessellation failed. Some faces can missing"); 
      }


/*

      MIntArray ivx;
      MPointArray uvs;
      fMesh.getPolygonVertices(i,ivx);
      MString uvSetName;      
      uvs.setLength(ivx.length());  
      MString texture;
      int indexShader=shindex[i];
      MVector polyNormal;


      if (indexShader>=0)
      {
  		MObject textureNode = textures[indexShader];
        if (!textureNode.isNull())
        {
		  MPlug filenamePlug = MFnDependencyNode(textureNode).findPlug("fileTextureName");
  		  filenamePlug.getValue(texture);          
        }
      }


      for (unsigned int j=0;j<ivx.length();j++)
      {
        float u,v;
        fMesh.getPolygonUV(i,j,u,v,&fCurrentUVSetName);
        uvs.set(j,u,v);
      }      
      int fcnx=NFaces();
      */
      for (int i=fcnx;i<NFaces();i++)
      {
        FaceT fc(this,i);
        Vector3 rawpn=fc.CalculateRawNormal();
        if (rawpn*vc>0) 
          fc.Reverse();
      }
      

    }
    for (unsigned int i=0,cnt=fMesh.numEdges();i<cnt;i++) if (!fMesh.isEdgeSmooth(i))
   {
        int edge[2];
        fMesh.getEdgeVertices(i,edge);
        AddSharpEdge(edge[0],edge[1]);
   }
   for (int i=0;i<MAX_NAMED_SEL;i++)
   {
     Selection *sel=GetNamedSel(i);
     if (sel) sel->SelectFacesFromPoints();
   }
   if (invert)
     for (int i=0;i<NFaces();i++)
      {FaceT fc(this,i);fc.Reverse();}
   RecalcNormals(true);   
   return true;
}


bool ObjectExport::TessellatePolygon(int polygon, MFnMesh &fMesh, RString texture, RString material, MStringArray &uvsetlist)
{
  MIntArray vertices;
  fMesh.getPolygonVertices(polygon,vertices);
  int *tempArr=(int *)alloca(vertices.length()*sizeof(int));
  for (unsigned int i=0,cnt=vertices.length();i<cnt;i++) tempArr[i]=vertices[cnt-i-1];
  ObjToolTopology::STessellateInfo *tnfodata=(ObjToolTopology::STessellateInfo *)alloca(sizeof(ObjToolTopology::STessellateInfo)*vertices.length()*3);
  Array<ObjToolTopology::STessellateInfo> tnfo(tnfodata,vertices.length()*3);
  

  bool res=GetTool<ObjToolTopology>().TessellatePolygon(vertices.length(),tempArr,0,texture,material,&tnfo);


  for (int i=0;i<uvsetlist.length();i++)
  {  
    if (SetActiveUVSet(i)==false)
    {
      AddUVSet(i);
      SetActiveUVSet(i);
    }
    for (int j=0;j<tnfo.Size();j++)
    {
      ObjToolTopology::STessellateInfo &tes=tnfo[j]; 
      float u,v;
      MStatus s=fMesh.getPolygonUV(polygon,vertices.length()-tes.index-1,u,v,&(uvsetlist[i]));
      if (s==MS::kSuccess)
      {
        FaceT fc(this,tes.face);
        fc.SetUV(tes.vx,u,1-v);
      }
    }
  }

  /*
  RString texName=uvSet.asChar();
  char *c=texName.MutableData();
  if (c)
  {
    while (*c) {if (*c=='/') *c='\\';c++;}
    if (texName[1]==':') texName=texName.Mid(3);
  }
  return this->GetTool<ObjToolTopology>().TessellatePolygon(vertices.length(),tempArr,tempUv,texName,rvmat.asChar());
  */
  return res;
}

bool ObjectExport::ExtractWeights(MFnMesh &fMesh, MDagPath fDagPath)
{
  MStatus status;

  fDagPath.extendToShape();
  int instanceNum = 0;
  if (fDagPath.isInstanced())
	  instanceNum = fDagPath.instanceNumber();



  MItDependencyGraph itDP(fMesh.object(),MFn::kSkinClusterFilter,
    MItDependencyGraph::kUpstream,
    MItDependencyGraph::kBreadthFirst,
    MItDependencyGraph::kNodeLevel,
    &status);

  

  if (status==MStatus::kSuccess)
  for (;!itDP.isDone();itDP.next())
  {
    MFnSkinCluster skinCluster(itDP.thisNode());
    MDagPathArray influenceObjs;
    skinCluster.influenceObjects(influenceObjs,&status);
    if (status==status.kFailure)
    {
      MGlobal::displayError("Unable to get any influence object for skinCluster");
      return false;
    }

    Selection **selArr=(Selection **)alloca(sizeof(Selection *)*influenceObjs.length());

    for (unsigned int i=0,cnt=influenceObjs.length();i<cnt;i++)
    {
      MFnDependencyNode infDEP(influenceObjs[i].node());

      MString name=infDEP.name().asChar();
      NamedSelection *namedSel=const_cast<NamedSelection *>(GetNamedSel(name.asChar()));
      if (namedSel==NULL) 
      {
        Selection empty(this);
        SaveNamedSel(name.asChar(),&empty);
      }
      namedSel=const_cast<NamedSelection *>(GetNamedSel(name.asChar()));
      selArr[i]=namedSel;
    }

    MFloatArray weights;

		MItGeometry gIter(fDagPath);
        int vindex=0;

		for ( /* nothing */ ; !gIter.isDone(); gIter.next() ) 
        {
			MObject comp = gIter.component(&status);

			// Get the weights for this vertex (one per influence object)
			//
			MFloatArray wts;
			unsigned int infCount;
			status = skinCluster.getWeights(fDagPath,comp,wts,infCount);			
			for (unsigned int jj = 0; jj < infCount ; ++jj ) 
            {
              float weight=wts[jj];                
              selArr[jj]->SetPointWeight(vindex,weight);
            }            			
            vindex++;
		}
    for (unsigned int jj = 0,cnt=influenceObjs.length(); jj < cnt; ++jj )  selArr[jj]->SelectFacesFromPoints();
	}
  return true;
}