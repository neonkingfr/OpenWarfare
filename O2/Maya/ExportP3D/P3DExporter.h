#pragma once


class P3DExporter: public MPxFileTranslator
{
public:

  static void *Create();

  P3DExporter(void);
  ~P3DExporter(void);

  ///This routine is called by Maya when it is necessary to load a file of a type supported by this translator.

  /**

  The only parameter is an MFileObject that contains the pathname of the file 
  to be loaded. This routine is responsible for reading the contents of the 
  given file, and creating Maya objects via API or MEL calls to reflect the 
  data in the file.

  param file the name of the file to read 
  param options String a string representation of any file options 
  param mode the method used to read the file - open or import 
  return MS::kSuccess if the read succeeds, and a failure code if it does not 
  */

  virtual MStatus		reader ( const MFileObject& file,
								const MString& optionsString,
								FileAccessMode mode);
  ///This routine is called by Maya when it is necessary to save a file of a type supported by this translator.

  /**
  The only parameter is an MFileObject that contains the pathname of the 
  file to be written to. This routine is responsible for traversing all objects 
  in the current Maya scene, and writing a representation to the given file in the 
  supported format.

  It is not essential that all information in the current Maya scene be written out, 
  although if it is not, the scene will not be recreated when this file is read via 
  the "reader" method.

  param file the name of the file to write 
  param optionsString a string representation of any file options 
  param mode the method used to write the file - save, export, or export active. 
  return MS::kSuccess if the write succeeds, and a failure code if it does not 
  */

  virtual MStatus		writer ( const MFileObject& file,
								const MString& optionsString,
								FileAccessMode mode);
  
  
  /**
  This routine is called by Maya while it is executing in 
  the MPxFileTranslator constructor. Maya uses this entry point 
  to query the translator and determine if it provides a reader method.
  */
  virtual bool		haveReadMethod () const;
  
  /**
  This routine is called by Maya while it is executing in the 
  MPxFileTranslator constructor. Maya uses this entry point to 
  query the translator and determine if it provides a writer method.
  */
  virtual bool		haveWriteMethod () const;
  ///
  virtual bool		haveNamespaceSupport () const;
  ///
  virtual MString     defaultExtension () const;
  ///
  virtual MString     filter () const;
  ///
  virtual bool        canBeOpened () const;
  ///
  virtual MPxFileTranslator::MFileKind identifyFile (	const MFileObject& file,
													  const char* buffer,
													  short size) const;

  void SetFileName(const MFileObject fileObject) {_file=fileObject;}
  void SetFileAccessMode(const FileAccessMode mode) {_mode=mode;}
  bool OnExport(float masterScale,bool swapfc, RString script, HWND hWnd,bool rotate180y, bool animation,bool fullnames);
protected:
  friend LRESULT ExportDlgProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);

  SRef<LODObject >_lObject;

  MFileObject _file;
  MString _optionsString;
  FileAccessMode _mode;
  float _masterScale;
  bool _swapfc;
  bool _rotate180y;
  bool _animation;
  bool _suppressMsg;
  bool _fullnames;

  bool ExportAll();
  bool ExportSelection();
  bool P3DExporter::IsVisible(MFnDagNode & fnDag, MStatus& status);
  bool ProcessPolyMesh(const MDagPath dagPath);
  bool ExportNode(const MDagPath &dag); 

  
  bool EnterToDialog();
};
