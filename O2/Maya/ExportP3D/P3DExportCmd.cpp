#include "common.h"
#include ".\p3dexportcmd.h"
#include "P3DExporter.h"

MSyntax P3DExportCmd::GetSyntax()
{
  MSyntax sx;
  sx.addArg(MSyntax::kString);
  sx.addFlag("sf","swapfc");
  sx.addFlag("a","animation");
  sx.addFlag("sc","script",MSyntax::kString);
  sx.addFlag("sel","selection");
  sx.addFlag("r","rotatey");
  sx.addFlag("ms","masterscale",MSyntax::kDouble);
  return sx;
}
void *P3DExportCmd::Create()
{
  return new P3DExportCmd;
}

MStatus P3DExportCmd::doIt( const MArgList& args )
{
  P3DExporter exporter;
  
  double masterScale=1.0f;
  bool swapfc=false;
  MString script;
  bool rotatey=false;
  bool animation=false;
  bool fullnames=false;

  MStatus st;
  MSyntax sx=GetSyntax();
  MArgDatabase argDb(sx,args,&st);
  if (st!=MS::kSuccess) return MS::kFailure;

  argDb.getFlagArgument("sc",0,script);
  swapfc=argDb.isFlagSet("sf");
  animation=argDb.isFlagSet("a");
  rotatey=argDb.isFlagSet("r");
  fullnames=argDb.isFlagSet("fname");
  argDb.getFlagArgument("ms",0,masterScale);

  if (argDb.isFlagSet("sel"))  
    exporter.SetFileAccessMode(MPxFileTranslator::kExportActiveAccessMode);
  else
    exporter.SetFileAccessMode(MPxFileTranslator::kExportAccessMode);

  MString name;
  if (argDb.getCommandArgument(0,name)==MS::kFailure)
  {
    displayInfo("exportP3D <outputname> [-sf] [-a] [-sc script] [-r] [-m scale] [-sel] [-fname]");
    displayInfo("");
    displayInfo("outputname  pathname of output file");
    displayInfo("-sf         swap faces");
    displayInfo("-a          export animation");
    displayInfo("-sc script  run O2 script");
    displayInfo("-r          rotate about 180� Y");
    displayInfo("-ms scale    set master scale");
    displayInfo("-sel        export selected only");
    displayInfo("-fname      Use dagPath fullname as object name (instead mesh name)");
    displayError("invalid arguments");
    return MS::kFailure;
  }

  MFileObject mf;
  mf.setFullName(name);
  exporter.SetFileName(mf);


  bool res=exporter.OnExport((float)masterScale,swapfc,script.asChar(),0,rotatey,animation,fullnames);
  return res?MS::kSuccess:MS::kFailure;
}
