//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ExportP3D.rc
//
#define IDC_BROWSE                      3
#define IDD_EXPORTDLG                   101
#define IDC_MASTERSCALE                 1002
#define IDC_PROGRESSBAR                 1003
#define IDC_SWAPFACES                   1004
#define IDC_RUNSCRIPT                   1005
#define IDC_RUNNINGSCRPT                1006
#define IDC_ROTATEY                     1007
#define IDC_ANIMATION                   1008
#define IDC_CHECK1                      1009
#define IDC_FULLNAMES                   1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
