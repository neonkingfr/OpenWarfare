#pragma once
#include "dialogclass.h"

class DlgMain :
  public CDialogClass
{
  HWND _list;
  bool _skipNotify;
  NOTIFYICONDATA nif;

protected:
  void ReadAppList();
  void IntegrateSS(DWORD id);
  void DisableIntegrationSS(DWORD id);
  void MinimizeTray();
  void RestoreTray();

public:
  DlgMain(void);
  ~DlgMain(void);

  LRESULT OnInitDialog();
  LRESULT OnOK();
  LRESULT OnCancel();
  LRESULT DlgProc(UINT msg, WPARAM wParam, LPARAM lParam);
  virtual LRESULT OnNotify(int id, NMHDR *ntf);


};
