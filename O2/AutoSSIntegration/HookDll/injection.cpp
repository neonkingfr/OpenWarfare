#include "StdAfx.h"
#include "Injection.h"


int Injection::RedirectCalling(void *oldPtr, void *newPtr, HINSTANCE hMod)
{
  int successes=0;
  __try
  {
    IMAGE_DOS_HEADER *dosheader=(IMAGE_DOS_HEADER *)hMod;
    IMAGE_OPTIONAL_HEADER * opthdr = (IMAGE_OPTIONAL_HEADER *) ((BYTE*)hMod+dosheader->e_lfanew+24);
    IMAGE_IMPORT_DESCRIPTOR  *descriptor= (IMAGE_IMPORT_DESCRIPTOR *)((BYTE*) hMod +
       opthdr->DataDirectory[ IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
    while(descriptor ->FirstThunk)
      {
        __try
        {
          void **IATentry=(void **)((BYTE*) hMod + descriptor->FirstThunk);
          while (*IATentry!=0)
          {
            if (*IATentry==oldPtr)
            {
              __try
              {
                DWORD oldProtect;
                VirtualProtect(IATentry,4,PAGE_READWRITE,&oldProtect);
                *IATentry=newPtr;              
                successes++;
              }
              __except(EXCEPTION_EXECUTE_HANDLER) {}
            }
            IATentry++;
          }
        }
        __except(EXCEPTION_EXECUTE_HANDLER) {}
        
        descriptor++;
    }
  }
  __except(EXCEPTION_EXECUTE_HANDLER) {}
  return successes;
}

/*
void Injection::InstallCreateFileHook(void *newCreateFileA, void *newCreateFileW,void *newLoadLibraryA, void *newLoadLibraryW, HMODULE hMod)
{
  IMAGE_DOS_HEADER *dosheader=(IMAGE_DOS_HEADER *)hMod;
  IMAGE_OPTIONAL_HEADER * opthdr = (IMAGE_OPTIONAL_HEADER *) ((BYTE*)hMod+dosheader->e_lfanew+24);
  IMAGE_IMPORT_DESCRIPTOR  *descriptor= (IMAGE_IMPORT_DESCRIPTOR *)((BYTE*) hMod +
     opthdr->DataDirectory[ IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);

  while(descriptor ->FirstThunk)
    {
        char*dllname=(char*)((BYTE*)hMod+ descriptor ->Name);
        if (_stricmp(dllname,"KERNEL32.DLL")==0)
        {
          if (descriptor ->OriginalFirstThunk!=0)
          {

            IMAGE_THUNK_DATA* thunk=( IMAGE_THUNK_DATA*)((BYTE*) hMod +
                                         descriptor ->OriginalFirstThunk);

            int x=0;
            while(thunk->u1.Function)
            {
                char*functionname=(char*)((BYTE*) hMod +
                        ( DWORD)thunk->u1.AddressOfData+2);
                DWORD *IATentryaddress=( DWORD *)((BYTE*) hMod +
                        descriptor->FirstThunk)+x;

                if (strcmp(functionname,"CreateFileA")==0)
                {

                  DWORD oldProtect;
                  VirtualProtect(IATentryaddress,4,PAGE_READWRITE,&oldProtect);
                  if (old_CreateFileA==0) old_CreateFileA=(old_CreateFileA_type)*IATentryaddress;
                  *IATentryaddress=(DWORD)newCreateFileA;
                }
                else if (strcmp(functionname,"CreateFileW")==0)
                {

                  DWORD oldProtect;
                  VirtualProtect(IATentryaddress,4,PAGE_READWRITE,&oldProtect);
                  if (old_CreateFileW==0) old_CreateFileW=(old_CreateFileW_type)*IATentryaddress;
                  *IATentryaddress=(DWORD)newCreateFileW;
                }
                else if (strcmp(functionname,"LoadLibraryA")==0)
                {
                  DWORD oldProtect;
                  VirtualProtect(IATentryaddress,4,PAGE_READWRITE,&oldProtect);
                  if (old_LoadLibraryA==0) old_LoadLibraryA=(old_LoadLibraryA_type)*IATentryaddress;
                  *IATentryaddress=(DWORD)newLoadLibraryA;
                }
                else if (strcmp(functionname,"LoadLibraryW")==0)
                {
                  DWORD oldProtect;
                  VirtualProtect(IATentryaddress,4,PAGE_READWRITE,&oldProtect);
                  if (old_LoadLibraryW==0) old_LoadLibraryW=(old_LoadLibraryW_type)*IATentryaddress;
                  *IATentryaddress=(DWORD)newLoadLibraryW;
                }
                x++; thunk++;
            }
            return;
          }
        }

    descriptor++;
  }

}
void Injection::RemoveCreateFileHook(HMODULE mod)
{
  InstallCreateFileHook(old_CreateFileA,old_CreateFileW,old_LoadLibraryA,old_LoadLibraryW,mod);
}
*/

int Injection::RedirectCallingInAllModules(void **oldPtrs, void **newPtrs, int count, HINSTANCE exclusion)
{  
  int sucesses=0;
  HANDLE h=CreateToolhelp32Snapshot(TH32CS_SNAPMODULE,GetCurrentProcessId());
  MODULEENTRY32 entry;
  entry.dwSize=sizeof(entry);
  for (BOOL iter=Module32First(h,&entry);iter;iter=Module32Next(h,&entry))
  {
    if (entry.hModule!=exclusion)
    {
      for (int i=0;i<count;i++)
        sucesses+=RedirectCalling(oldPtrs[i],newPtrs[i], entry.hModule);
    }
  }
  CloseHandle(h);
  return sucesses;
}

/*void Injection::InstallCreateFileHookToAllModules(void *newCreateFileA, void *newCreateFileW,void *newLoadLibraryA, void *newLoadLibraryW, HMODULE exclude)
{
  HANDLE h=CreateToolhelp32Snapshot(TH32CS_SNAPMODULE,GetCurrentProcessId());
  MODULEENTRY32 entry;
  entry.dwSize=sizeof(entry);
  for (BOOL iter=Module32First(h,&entry);iter;iter=Module32Next(h,&entry))
  {
    if (entry.hModule!=exclude)
      InstallCreateFileHook(newCreateFileA, newCreateFileW, newLoadLibraryA, newLoadLibraryW, entry.hModule);
  }
}
void Injection::RemoveCreateFileHookFromAllModules(HMODULE exclude)
{
  InstallCreateFileHookToAllModules(old_CreateFileA,old_CreateFileW,old_LoadLibraryA,old_LoadLibraryW,exclude);
}
*/