#pragma once



class Injection
{
public:
  static int RedirectCalling(void *oldPtr, void *newPtr, HINSTANCE module);
  static int RedirectCallingInAllModules(void **oldPtrs, void **newPtrs, int count, HINSTANCE exclusion);

};
