// HookDll.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "Injection.h"
#include "../IPC.h"
#include <el/MultiThread/CriticalSection.h>



HINSTANCE hInst;
HINSTANCE hComDlg;


void *old_CreateFileA=0;
void *old_CreateFileW=0;
void *old_LoadLibraryA=0;
void *old_LoadLibraryW=0;
void *old_LoadLibraryExA=0;
void *old_LoadLibraryExW=0;

void Inject();
static bool IsHooked=false;

static HANDLE sharedMem;
static HANDLE otherSharedMem;

static DWORD cfg;

struct ControlStructure
{
  int forceInjectCmdCntr;
  int forceRestoreCmdCntr;
  ControlStructure():forceInjectCmdCntr(0),forceRestoreCmdCntr(0) {}
};

static ControlStructure lastCmds;




struct CreateFileCatchupStructure
{
  void *fname; //pointer to filename;
  DWORD dwDesiredAccess;    //dwDesiredAccess from CreateFile
  DWORD dwCreationDisposition; //dwCreationDisposition from CreateFile
  DWORD unicodeFlag;    //0 - ASCII, 1 - UNICODE
};

static DWORD LoadLibLevels=0;


static HANDLE PackageMem=0;
static MsgPackage *Package=0;
static HWND ControlWnd=0;
static HANDLE PackageControlApp=0;
static HANDLE ControlApp=0;

bool StartControlApp();

static HWND GetMyForegroundWindow()
{  
  HWND a=GetForegroundWindow();
  DWORD id=GetCurrentProcessId();
  DWORD pid;
  GetWindowThreadProcessId(a,&pid);

  while (a && pid!=id)
  {
    a=GetWindow(a,GW_HWNDNEXT);
    if (a) GetWindowThreadProcessId(a,&pid);
  }
  return a;
}

bool __fastcall CatchFilename(CreateFileCatchupStructure &catchup)
{
  if (!IsWindow(ControlWnd))
  {
    if (StartControlApp()==false) return true;
  }

  if (cfg & CFG_NOCOMDLG)
  {
    HWND hWnd=GetMyForegroundWindow();
    if (hWnd!=0)
    {
      HINSTANCE hWndInst=(HINSTANCE)GetWindowLongPtr(hWnd,GWL_HINSTANCE);
      if (hWndInst==hComDlg) 
        return true;
    }
  }

  if (!catchup.unicodeFlag)
  {
    size_t reqsz=MultiByteToWideChar(CP_THREAD_ACP,0,(LPCSTR)catchup.fname,strlen((LPCSTR)catchup.fname)+1,0,0);
    wchar_t *buff=(wchar_t *)alloca(sizeof(wchar_t)*reqsz);
    MultiByteToWideChar(CP_THREAD_ACP,0,(LPCSTR)catchup.fname,strlen((LPCSTR)catchup.fname)+1,buff,reqsz);
    catchup.fname=buff;
    catchup.unicodeFlag=1;
  }

  if (Package==0)
  {
      PackageMem=CreateFileMapping(INVALID_HANDLE_VALUE,0,PAGE_READWRITE,0,sizeof(MsgPackage),0);
      if (PackageMem==0) return true;
      Package=(MsgPackage *)MapViewOfFile(PackageMem,FILE_MAP_WRITE,0,0,0);
  }
  if (otherSharedMem==0)
  {
    if (DuplicateHandle(GetCurrentProcess(),PackageMem,ControlApp,&otherSharedMem,0,0,DUPLICATE_SAME_ACCESS)==FALSE)
      return true;
  }
  

  Package->dwCreationDisposition=catchup.dwCreationDisposition;
  Package->dwDesiredAccess=catchup.dwDesiredAccess;
  LPWSTR part;
  DWORD res=GetFullPathNameW((wchar_t *)catchup.fname,sizeof(Package->name)/sizeof(Package->name[0]),Package->name,&part);
  if (res>sizeof(Package->name)/sizeof(Package->name[0])) return true;
  if (res==0) return true;


  res=SendMessage(ControlWnd,CNM_FILEACTIONREPORT,0,(LPARAM)otherSharedMem);


  return res!=0;
}


HANDLE _declspec(naked) new_CreateFileA(
    LPCSTR lpFileName,
    DWORD dwDesiredAccess,
    DWORD dwShareMode,
    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    DWORD dwCreationDisposition,
    DWORD dwFlagsAndAttributes,
    HANDLE hTemplateFile
    )
{
  _asm
  {
    push ebp
    mov ebp,esp
    sub esp,SIZE CreateFileCatchupStructure
    pushad
    pushfd    
    lea ecx,[ebp][-SIZE CreateFileCatchupStructure];
    mov eax,[ebp][2*4]
    mov ebx,[ebp][3*4]
    mov [ecx][4],ebx 
    mov [ecx][0],eax
    mov eax,[ebp][6*4]
    mov [ecx][8],eax
    mov dword ptr [ecx][12],0;
    call CatchFilename
    test al,al
    jne okay;
    push 5
    mov eax,SetLastError
    call eax
    popfd
    popad
    mov esp,ebp
    xor eax,eax;
    pop ebp
    dec eax
    ret 1ch;
okay:popfd
    popad
    mov esp,ebp
    pop ebp
    jmp [old_CreateFileA];
  }
}


HANDLE _declspec(naked) new_CreateFileW(
    LPCWSTR lpFileName,
    DWORD dwDesiredAccess,
    DWORD dwShareMode,
    LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    DWORD dwCreationDisposition,
    DWORD dwFlagsAndAttributes,
    HANDLE hTemplateFile
    )
{
  _asm
  {
    push ebp
    mov ebp,esp
    sub esp,SIZE CreateFileCatchupStructure
    pushad
    pushfd    
    lea ecx,[ebp][-SIZE CreateFileCatchupStructure];
    mov eax,[ebp][2*4]
    mov ebx,[ebp][3*4]
    mov [ecx][4],ebx 
    mov [ecx][0],eax
    mov eax,[ebp][6*4]
    mov [ecx][8],eax
    mov dword ptr [ecx][12],1;
    call CatchFilename
    test al,al
    jne okay;
    push 5
    mov eax,SetLastError
    call eax
    popfd
    popad
    mov esp,ebp
    xor eax,eax;
    pop ebp
    dec eax
    ret 1ch;
okay:popfd
    popad
    mov esp,ebp
    pop ebp
    jmp [old_CreateFileW];
  }
}


HMODULE _declspec(naked) new_LoadLibraryA(LPCSTR lpLibFileName)
{
  _asm
  {
    lock inc LoadLibLevels;
    push [esp][4];
    call [old_LoadLibraryA];
    lock dec LoadLibLevels;
    jnz skip
    pushad
    pushfd
    call Inject;
    popfd
    popad
skip:
    ret 4
  }
}

HMODULE _declspec(naked) new_LoadLibraryW(LPCWSTR lpLibFileName)
{
  _asm
  {
    lock inc LoadLibLevels;
    push [esp][4];
    call [old_LoadLibraryW];
    lock dec LoadLibLevels;
    jnz skip
    pushad
    pushfd
    call Inject;
    popfd
    popad
skip:
    ret 4
  }
}
HMODULE _declspec(naked) new_LoadLibraryExA(LPCSTR lpLibFileName, HANDLE hFile, DWORD dwFlags )
{
  _asm
  {
    lock inc LoadLibLevels;
    push [esp][12];
    push [esp][12];
    push [esp][12];
    call [old_LoadLibraryExA];
    lock dec LoadLibLevels;
    jnz skip
    pushad
    pushfd
    call Inject;
    popfd
    popad
skip:
    ret 12
  }
}
HMODULE _declspec(naked) new_LoadLibraryExW(LPCWSTR lpLibFileName, HANDLE hFile, DWORD dwFlags )
{
  _asm
  {
    lock inc LoadLibLevels;
    push [esp][12];
    push [esp][12];
    push [esp][12];
    call [old_LoadLibraryExW];
    lock dec LoadLibLevels;
    jnz skip
    pushad
    pushfd
    call Inject;
    popfd
    popad
skip:
    ret 12
  }
}

static void Inject()
{
  HINSTANCE hInst=GetModuleHandle(L"KERNEL32.DLL");
  if (old_CreateFileA==0) old_CreateFileA=GetProcAddress(hInst,"CreateFileA");
  if (old_CreateFileW==0) old_CreateFileW=GetProcAddress(hInst,"CreateFileW");
  if (old_LoadLibraryA==0) old_LoadLibraryA=GetProcAddress(hInst,"LoadLibraryA");
  if (old_LoadLibraryW==0) old_LoadLibraryW=GetProcAddress(hInst,"LoadLibraryW");
  if (old_LoadLibraryExA==0) old_LoadLibraryExA=GetProcAddress(hInst,"LoadLibraryExA");
  if (old_LoadLibraryExW==0) old_LoadLibraryExW=GetProcAddress(hInst,"LoadLibraryExW");

  void *oldPtrs[6]={old_CreateFileA,old_CreateFileW,old_LoadLibraryA,old_LoadLibraryW,old_LoadLibraryExA,old_LoadLibraryExW};
  void *newPtrs[6]={&new_CreateFileA,&new_CreateFileW,&new_LoadLibraryA,&new_LoadLibraryW,&new_LoadLibraryExA,&new_LoadLibraryExW};
  Injection::RedirectCallingInAllModules(oldPtrs,newPtrs,6,::hInst);
  hComDlg=GetModuleHandle(L"COMDLG32.DLL");
  IsHooked=true;
}

static void Restore()
{
  void *oldPtrs[6]={&new_CreateFileA,&new_CreateFileW,&new_LoadLibraryA,&new_LoadLibraryW,&new_LoadLibraryExA,&new_LoadLibraryExW};
  void *newPtrs[6]={old_CreateFileA,old_CreateFileW,old_LoadLibraryA,old_LoadLibraryW,old_LoadLibraryExA,old_LoadLibraryExW};
  Injection::RedirectCallingInAllModules(oldPtrs,newPtrs,6,hInst);
  IsHooked=false;
}

static bool IsConfigPresent()
{
  wchar_t buff[2048];
  GetModuleFileNameW(0,buff,sizeof(buff)/sizeof(buff[0])-20);
  const wchar_t *s=wcsrchr(buff,'\\');
  const wchar_t *d=wcsrchr(buff,'.');
  if (d==0) d=wcschr(buff,0);
  else if (d<s) d=wcschr(buff,0);
  wcscpy(const_cast<wchar_t *>(d),L".ssint.ini");

  HANDLE h=CreateFileW(buff,0,FILE_SHARE_DELETE|FILE_SHARE_WRITE|FILE_SHARE_READ,0,OPEN_EXISTING,0,0);
  if (h==INVALID_HANDLE_VALUE) return false;
  CloseHandle(h);
  return true;
}

#ifndef _T
#define _T(x) L##x
#endif

static BOOL CALLBACK GetAppMsgWindow(HWND hwnd,LPARAM lParam)
{
  wchar_t className[256];
  GetClassName(hwnd,className,256);
  if (wcscmp(className,MAINWNDCLASS)) return TRUE;
  ControlWnd=hwnd;
  return FALSE;
}


static bool StartControlApp()
{
  wchar_t buff[2048];
  GetModuleFileNameW(hInst,buff,sizeof(buff)/sizeof(buff[0])-50);
  wchar_t *part=const_cast<wchar_t *>(wcsrchr(buff,'\\'));
  wcscpy(part,L"\\AutoSSCntrl.exe");

  STARTUPINFO stnfo;
  memset(&stnfo,0,sizeof(stnfo));
  stnfo.cb=sizeof(stnfo);
  PROCESS_INFORMATION pi;
  BOOL res=CreateProcessW(buff,buff,0,0,FALSE,0,0,0,&stnfo,&pi);
  if (res==FALSE) return false;
  if (WaitForInputIdle(pi.hProcess,2000)!=0)
  {
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);
    return false;
  }
  ControlApp=pi.hProcess;
  CloseHandle(pi.hThread);
  EnumThreadWindows(pi.dwThreadId,GetAppMsgWindow,0);

  DWORD resd=SendMessage(ControlWnd,CNM_SETSERVERID,0,GetCurrentProcessId());
  if ((resd & CFG_READY)==0) return false;
  cfg=resd;

  otherSharedMem=0;

  return true;
  
}

static bool StartHook()
{
  if (IsConfigPresent()==false) return false;
  if (IsHooked) return true;

  StartControlApp();

  Inject();
  return true;
}




static bool InitializeSharedMem()
{
  wchar_t buff[2048];
  GetModuleFileNameW(hInst,buff+10,sizeof(buff)/sizeof(buff[0])-10);
  wcsncpy(buff,L"SHAREDMEM:",10);
  wchar_t *z;
  while ((z=const_cast<wchar_t *>(wcschr(buff,'\\')))!=0) *z='/';
  HANDLE h=CreateFileMapping(INVALID_HANDLE_VALUE,0,PAGE_READWRITE,0,sizeof(ControlStructure),buff);
  if (h==0) return false;
  sharedMem=h;
  return true;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
    hInst=hModule;

    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH: 
      {
        InitializeSharedMem();        
        IsHooked=false;
      } 
    break;
    case DLL_PROCESS_DETACH: 
      {
      if (IsHooked) Restore();
      CloseHandle(sharedMem);
      if (Package) 
      {
        UnmapViewOfFile(Package);
        CloseHandle(PackageMem);
        Package=0;
      }
      if (ControlApp)
      {
        PostMessage(ControlWnd,CNM_EXIT,0,0);
        CloseHandle(ControlApp);
      }
      }
      break;
    }

    return TRUE;
}

static MultiThread::CriticalSection csect;

extern "C"
{

void HookMainDispatch()
{
  csect.Lock();
    ControlStructure *cmd=(ControlStructure *)MapViewOfFile(sharedMem,FILE_MAP_READ,0,0,0);
    if (cmd==0)
    {
      return;
    }
    if (cmd->forceRestoreCmdCntr!=lastCmds.forceRestoreCmdCntr)
    {
      if (IsHooked && !IsConfigPresent())
      {
        Restore();
        if (ControlApp)
        {
          CloseHandle(ControlApp);
          PostMessage(ControlWnd,CNM_EXIT,0,0);
          otherSharedMem=0;
        }
      }
      lastCmds.forceRestoreCmdCntr=cmd->forceRestoreCmdCntr;              
    }
    if (cmd->forceInjectCmdCntr!=lastCmds.forceInjectCmdCntr)
    {
      StartHook();
      lastCmds.forceInjectCmdCntr=cmd->forceInjectCmdCntr;
    }
    UnmapViewOfFile(cmd);
    csect.Unlock();
}

LRESULT _declspec(dllexport) CALLBACK HookCBTProc( int nCode,
    WPARAM wParam,
    LPARAM lParam)
{
    HookMainDispatch();
  ///we will do nothing in this function. It is useful only for keeping DLL in memory
    return CallNextHookEx(0,nCode,wParam,lParam);    
}

LRESULT _declspec(dllexport) CALLBACK HookCallWndProc( int nCode,
    WPARAM wParam,
    LPARAM lParam)
{
    CWPSTRUCT *msg=(CWPSTRUCT *)lParam;
    if (msg->message==WM_ACTIVATE) 
    {
      HookMainDispatch();
      if (IsWindow(ControlWnd)) PostMessage(ControlWnd,CNM_ACTIVEWINDOWCHANGED,msg->wParam,msg->lParam);
    }
  ///we will do nothing in this function. It is useful only for keeping DLL in memory
    return CallNextHookEx(0,nCode,wParam,lParam);    
}

void _declspec(dllexport) InitCommands()
{
    ControlStructure *cmd=(ControlStructure *)MapViewOfFile(sharedMem,FILE_MAP_WRITE,0,0,0);
    cmd->forceInjectCmdCntr=0;
    cmd->forceRestoreCmdCntr=0;
    UnmapViewOfFile(cmd);
}

void _declspec(dllexport) EnforceReInject()
{
    ControlStructure *cmd=(ControlStructure *)MapViewOfFile(sharedMem,FILE_MAP_WRITE,0,0,0);
    cmd->forceInjectCmdCntr++;
    UnmapViewOfFile(cmd);
}

void _declspec(dllexport) EnforceRestore()
{
    ControlStructure *cmd=(ControlStructure *)MapViewOfFile(sharedMem,FILE_MAP_WRITE,0,0,0);
    cmd->forceRestoreCmdCntr++;
    UnmapViewOfFile(cmd);
}

}


