#pragma once
#include "dialogclass.h"

class CheckOutDlg :  public CDialogClass
{  
  HWND titleWnd;
  HFONT titleFont;  
  HWND projectCombo;
  HBRUSH winBgrn;
public:
  enum AskMeResult
  {
    amAskMeNextTime=0,
    am10Sec=10,
    am20Sec=20,
    am1Min=60,
    am5Min=5*60,
    amThisSession=0xFFFFFFFF,
    amUntilSave=0xFFFFFFFE   
  };

  AskMeResult askMeRes;
  bool saveOption;

  Array<const _TCHAR *>availAbleProjects;
  int selectedProject;

  const _TCHAR *title;


  CheckOutDlg(void);
  ~CheckOutDlg(void);

  virtual LRESULT OnInitDialog();
  virtual LRESULT OnOK();
  virtual LRESULT OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt);
  virtual LRESULT DlgProc(UINT msg, WPARAM wParam, LPARAM lParam);
  virtual HBRUSH OnCtlColorDlg(WPARAM wParam, LPARAM lParam);
};
