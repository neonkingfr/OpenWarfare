//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AutoSSCntrl.rc
//
#define IDS_UNABLETOATTACHPROCESS       1
#define IDS_APP_TITLE                   2
#define IDS_UNBOUND                     3
#define IDC_AUTOSSCNTRL                 4
#define IDI_MAINICON                    5
#define IDS_TABALLFILES                 5
#define IDS_TABCHECKEDOUT               6
#define IDS_TABNEWFILES                 7
#define IDD_CHECKOUTDLG                 7
#define IDS_TABOTHER                    8
#define IDB_LISTICONS                   8
#define IDS_TABNOTCHECKED               9
#define IDR_POPUPMENU                   9
#define IDS_TABOLDVER                   10
#define IDD_COMMENTS                    10
#define IDS_TABLOG                      11
#define IDI_CHECKOUT                    11
#define IDS_UNABLETOLOADSOURCESAFEPROVIDER 12
#define IDS_NOPROJECTCHOOSEN            13
#define IDI_GETLATEST                   13
#define IDS_UNABLETOOPENPROJECT         14
#define IDI_VIEWLOCAL                   14
#define IDS_NOPROJECT                   15
#define IDI_CANCEL                      15
#define IDS_CURRPROJECT                 16
#define IDD_OPTIONS                     16
#define IDS_CURRPROJECTANDDESCENDANTS   17
#define IDS_CHECKINWARING               18
#define IDS_NEWVERSIONNOTIFY            19
#define IDS_HISTORYWARNING              20
#define IDS_ASKPROCESSRECURSIVE         21
#define IDS_GETPROJECTFROMSERVER        22
#define IDS_TABDELETED                  23
#define IDS_APPLICATIONMUSTBERESTARTED  24
#define IDS_NEEDRESTART                 25
#define IDS_CHOOSE_FILES_TO_ADD_TO_SOURCE_CONTROL 26
#define IDC_CHECKOUT                    1000
#define IDC_GETLATESTVER                1001
#define IDC_VIEWLOCAL                   1002
#define IDC_ASKMENEXT                   1004
#define IDC_THISSESSION                 1005
#define IDC_UNTILSAVE                   1006
#define IDC_10SEC                       1007
#define IDC_20SEC                       1008
#define IDC_1MIN                        1009
#define IDC_5MIN                        1010
#define IDC_SAVEOPTIONS                 1012
#define IDC_AVAILABLEPROJECTS           1013
#define IDC_FILENAME                    1014
#define IDC_COMMENTS                    1014
#define IDC_FILELIST                    1015
#define IDC_KEEPCHECKOUT                1016
#define IDC_NOHISTORY                   1017
#define IDC_FILETYPEAUTO                1019
#define IDC_FILETYPETEXT                1020
#define IDC_FILETYPEBIN                 1021
#define IDC_SERVICEPROVIDER             1021
#define IDC_CHANGESP                    1022
#define IDC_DEFAULT                     1023
#define IDC_ASKMEESS                    1024
#define IDC_NOCOMDLG                    1025
#define IDC_EXCLUDED                    1026
#define IDC_INCLUDED                    1027
#define ID_ITEMPOPUP_                   32772
#define ID_ITEMPOPUP_CHECKOUT           32773
#define ID_ITEMPOPUP_CHECKIN            32774
#define ID_ITEMPOPUP_GETLATESTVERSION   32775
#define ID_ITEMPOPUP_HISTORY            32776
#define ID_ITEMPOPUP_PROPERTIES         32777
#define ID_ITEMPOPUP_DIFFERENCIES       32778
#define ID_ITEMPOPUP_PROPERTIES32779    32779
#define ID_ITEMPOPUP_EXPLORE            32780
#define ID_ITEMPOPUP_CLEARLOCKEDFILES   32781
#define ID_ITEMPOPUP_OPTIONS            32782
#define ID_ITEMPOPUP_CHANGEPROJECT      32783
#define ID_Menu                         32784
#define ID_Menu32785                    32785
#define ID_ITEMPOPUP_GETPROJECTFROMSOURCESAFE 32786
#define ID_ITEMPOPUP_UNDOCHECKOUT       32787
#define ID_ITEMPOPUP_SELECTALL          32788
#define ID_ITEMPOPUP_VIRTUALPROJECTS    32789
#define ID_ITEMPOPUP_ADD                32790
#define ID_ITEMPOPUP_REFRESH            32791
#define ID_ITEMPOPUP_DELETELOCALCOPY    32792
#define ID_ITEMPOPUP_REACTIVATEDLG      32793
#define ID_ITEMPOPUP_DELETEFROMSOURCESAFE 32794
#define ID_ITEMPOPUP_OTHER              32796
#define ID_OTHER_SHARE                  32797
#define ID_OTHER_CREATEPROJECT          32798
#define ID_OTHER_ADDFILES               32799
#define ID_OTHER_RUNSCCFRONTEND         32800

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        17
#define _APS_NEXT_COMMAND_VALUE         32801
#define _APS_NEXT_CONTROL_VALUE         1029
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
