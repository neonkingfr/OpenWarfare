#include "StdAfx.h"
#include ".\dlgcomments.h"
#include "resource.h"
#include "FileList.h"

DlgComments::DlgComments():files(0)
{
  LOGFONT lg;  
  GetObject(GetStockObject(DEFAULT_GUI_FONT),sizeof(lg),&lg);
  lg.lfHeight+=lg.lfHeight<0?-6:6;
  lg.lfWeight=FW_BOLD;
  commentsFont=CreateFontIndirect(&lg);
  checkIn=false;
  keepCheckedOut=false;
  noHistory=false;
  fileType=0;
}

DlgComments::~DlgComments()
{
  DeleteObject(commentsFont);
}


LRESULT DlgComments ::OnOK()
{
 comments=GetDlgItemText(IDC_COMMENTS);
 keepCheckedOut=IsDlgButtonChecked(*this,IDC_KEEPCHECKOUT)==BST_CHECKED;
 noHistory=IsDlgButtonChecked(*this,IDC_NOHISTORY)==BST_CHECKED;
 fileType=IsDlgButtonChecked(*this,IDC_FILETYPEAUTO)==BST_CHECKED?0:(
    IsDlgButtonChecked(*this,IDC_FILETYPETEXT)==BST_CHECKED?1:(
    IsDlgButtonChecked(*this,IDC_FILETYPEBIN)==BST_CHECKED?2:0));
 return CDialogClass::OnOK();
}

LRESULT DlgComments ::OnInitDialog()
{
  SendDlgItemMessage(IDC_COMMENTS,WM_SETFONT,(WPARAM)commentsFont,(LPARAM)TRUE);
  SetDlgItemText(IDC_COMMENTS,comments);

  HWND list=GetDlgItem(IDC_FILELIST);
  for (int i=0;i<files.Size();i++) ListBox_AddString(list,files[i]->_file);

  if (keepCheckedOut) CheckDlgButton(*this,IDC_KEEPCHECKOUT,BST_CHECKED);
  if (noHistory) CheckDlgButton(*this,IDC_NOHISTORY,BST_CHECKED);
  if (fileType==0) CheckDlgButton(*this,IDC_FILETYPEAUTO,BST_CHECKED);
  if (fileType==1) CheckDlgButton(*this,IDC_FILETYPETEXT,BST_CHECKED);
  if (fileType==2) CheckDlgButton(*this,IDC_FILETYPEBIN,BST_CHECKED);

  if (checkIn)
  {
    EnableWindow(GetDlgItem(IDC_NOHISTORY),FALSE);
    EnableWindow(GetDlgItem(IDC_FILETYPEAUTO),FALSE);
    EnableWindow(GetDlgItem(IDC_FILETYPETEXT),FALSE);
    EnableWindow(GetDlgItem(IDC_FILETYPEBIN),FALSE);
  }

  return 1;
}
