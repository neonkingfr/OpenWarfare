#include "StdAfx.h"
#include ".\mainwnd.h"
#include "../IPC.h"
#include "resource.h"
#include "CheckOutDlg.h"
#include <shellapi.h>
#include "DlgComments.h"
#include "DlgOptions.h"

#define WRITE_RIGHTS (FILE_WRITE_DATA|FILE_APPEND_DATA|GENERIC_WRITE|GENERIC_ALL)

using namespace MultiThread;

MainWnd::MainWnd(HWND bWindow):_forceExit(EventBlocker::Blocked_AutoReset),_servBlock(0)
{  
  _hWnd=bWindow;
  SetWindowLongPtr(_hWnd,GWL_USERDATA,(LONG_PTR)this);
  _curMode=modeUnbound;


  InitCommonControls();
  _listCtrl=CreateWindow(WC_LISTVIEW,_T(""),WS_CHILD|WS_VISIBLE|LVS_REPORT|LVS_NOCOLUMNHEADER|LVS_OWNERDATA|LVS_SHOWSELALWAYS,0,0,0,0,_hWnd,(HMENU)1,(HINSTANCE)GetWindowLongPtr(_hWnd,GWL_HINSTANCE),0);
  _tabCtrl=CreateWindow(WC_TABCONTROL,_T(""),WS_CHILD|WS_VISIBLE|TCS_MULTILINE|WS_CLIPSIBLINGS,0,0,0,0,_hWnd,0,(HINSTANCE)GetWindowLongPtr(_hWnd,GWL_HINSTANCE),0);
  TCITEM itt;
  int tabs[]={IDS_TABALLFILES,IDS_TABCHECKEDOUT,IDS_TABNOTCHECKED,IDS_TABNEWFILES,IDS_TABOLDVER,IDS_TABDELETED};
  for (int i=0;i<lenof(tabs);i++)
  {  
    itt.mask=TCIF_PARAM|TCIF_TEXT;
    itt.lParam=tabs[i];
    itt.pszText=const_cast<_TCHAR *>(StrRes[tabs[i]]);
    SendMessage(_tabCtrl,TCM_INSERTITEM,i,(LPARAM)&itt);
  }

  _curTabPos=IDS_TABALLFILES;

  SendMessage(_tabCtrl,WM_SETFONT,(WPARAM)GetStockObject(DEFAULT_GUI_FONT),TRUE);
  LVCOLUMN lvc;
  lvc.mask=LVCF_FMT|LVCF_WIDTH|LVCF_TEXT|LVCF_SUBITEM;
  lvc.fmt=LVCFMT_LEFT;
  lvc.iSubItem=0;
  lvc.cx=-1;
  lvc.pszText=_T("");
  ListView_InsertColumn(_listCtrl,0,&lvc);
  SendMessage(_listCtrl,WM_SETFONT,(WPARAM)GetStockObject(DEFAULT_GUI_FONT),TRUE);
  OnWmSize();  
  _decisionExpiration=0;
  _checkOutDecision=0;
  _icons=ImageList_LoadBitmap((HINSTANCE)GetWindowLongPtr(_hWnd,GWL_HINSTANCE),MAKEINTRESOURCE(IDB_LISTICONS),16,16,RGB(255,0,255));
//  ListView_SetImageList(_listCtrl,LVSIL_NORMAL,_icons);
  ListView_SetImageList(_listCtrl,_icons,LVSIL_SMALL);
  ListView_SetExtendedListViewStyleEx(_listCtrl,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
}


MainWnd::~MainWnd(void)
{
  _forceExit.Unblock();
  Join();
  if (_hWnd) DestroyWindow(_hWnd);
}

LRESULT MainWnd::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch (msg)
  {
  case WM_DESTROY:
      _hWnd=0;
      PostQuitMessage(0);
      return 0;
  case CNM_SETSERVERID:
      return SetServerID((DWORD)lParam);;
  case CNM_EXIT:
      SetPostExit();
      return 1;
  case CNM_FILEACTIONREPORT: 
      return OnFileActionReport(lParam);
  case WM_CLOSE:
      OnWmClose();
      return 1;
  case WM_EXITSIZEMOVE:
      OnExitSizeMove();
      return 0;
  case WM_SIZE:
      if (wParam!=SIZE_MINIMIZED && _decisionExpiration==0xFFFFFFFE)
        _decisionExpiration=0;
      OnWmSize();
      return 1;
  case WM_NOTIFY:
    switch (wParam)
    {
    case 0:
      return OnNotifyTab((LPNMHDR)lParam);          
    case 1:
      return OnNotifyList((LPNMHDR)lParam);
    };
    return DefWindowProc(hWnd,msg,wParam,lParam);
  case WM_CONTEXTMENU:
    return OnContextMenu(wParam,lParam);
  case WM_TIMER:
    if (OnTimer(wParam,lParam))
      return 0;
    else
      return DefWindowProc(hWnd,msg,wParam,lParam);
  case WM_ACTIVATE:
  case CNM_ACTIVEWINDOWCHANGED:    
    SetTimer(_hWnd,2,500,0);
    return 0;

  default:
    return DefWindowProc(hWnd,msg,wParam,lParam);
  }
 
}

unsigned long MainWnd::Run()
{
  const BlockerSimpleBase *objects[]={&_servBlock,&_forceExit};  
  int res=BlockerSimpleBase::WaitForMultiple(Array<const BlockerSimpleBase *>(objects,2),false,INFINITE);
  if (res==2) return 0;
  PostMessage(_hWnd,CNM_EXIT,0,0);
  return 0;
}

DWORD MainWnd::SetServerID(DWORD processId)
{
  if (IsRunning()) return 0;
  _servBlock=ProcessBlocker(processId);


  HANDLE h=CreateToolhelp32Snapshot(TH32CS_SNAPMODULE,processId);
  MODULEENTRY32 module;
  module.dwSize=sizeof(module);
  Module32First(h,&module);
  CloseHandle(h);
  SetWindowText(_hWnd,Pathname::GetNameFromPath(module.szExePath));

  _TCHAR *buff=_tcscpy((_TCHAR *)alloca(sizeof(_TCHAR)*(_tcslen(module.szExePath)+32)),module.szExePath);
  _TCHAR *ext=const_cast<_TCHAR *>(Pathname::GetExtensionFromPath(buff));
  _tcscpy(ext,_T(".ssint.ini"));

  _config.LoadConfig(Pathname(buff));

  WINDOWPLACEMENT wp;
  wp.length=sizeof(wp);
  GetWindowPlacement(_hWnd,&wp);
  wp.rcNormalPosition=_config.winRect;
  SetWindowPlacement(_hWnd,&wp);

  InitSourceSafeProvider();

  Start();
  _curMode=modeConnected;

  _servPID=processId;

  return (_config.detectComDlgOpen?CFG_NOCOMDLG:0)|CFG_READY ;
}

void MainWnd::PostExitCheck()
{
  ShowWindow(_hWnd,SW_SHOW);
  OnRefreshStatus();
  _files.ClearLocked();
  ReFilter();
  if (_files.Size())
  {  
    if (_filter.Size()==0)
    {
      TabCtrl_SetCurSel(_tabCtrl,0);
      _curTabPos=IDS_TABALLFILES;
      ReFilter();      
    }
    else
      InvokeMenuCommand(ID_ITEMPOPUP_SELECTALL);
  }
  else
  {
    OnWmClose();
  }

}
void MainWnd::SetPostExit()
{
  _curMode=modePostExit;
  _forceExit.Unblock();
  OpenIcon(_hWnd);
  TabCtrl_SetCurSel(_tabCtrl,1);
  _curTabPos=IDS_TABCHECKEDOUT;
  PostExitCheck();
  if (_files.Size())
  {
    InvokeMenuCommand(ID_ITEMPOPUP_CHECKIN);
  }
}

void MainWnd::OnWmClose()
{
  if (_curMode==modeConnected)
  {
    CloseWindow(_hWnd);    
  }
  else
  {
    DestroyWindow(_hWnd);
  }
}

void MainWnd::OnWmSize()
{
  RECT rc;
  GetClientRect(_hWnd,&rc);
  MoveWindow(_tabCtrl,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,FALSE);
  SendMessage(_tabCtrl,TCM_ADJUSTRECT,FALSE,(LPARAM)&rc);
  MoveWindow(_listCtrl,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,FALSE);
  GetClientRect(_listCtrl,&rc);
  ListView_SetColumnWidth(_listCtrl,0,rc.right-rc.left); 
  InvalidateRect(_hWnd,0,FALSE);
} 

int MainWnd::GetCurSelTab() const
{
  int curPos=TabCtrl_GetCurSel(_tabCtrl);
  TCITEM itm;
  itm.mask=TCIF_PARAM;
  TabCtrl_GetItem(_tabCtrl,curPos,&itm);
  return itm.lParam;
}
LRESULT MainWnd::OnFileActionReport(LPARAM lParam)
{
  HANDLE h=(HANDLE)lParam;
  if (h==0) return 0;
  MsgPackage *package=(MsgPackage *)MapViewOfFile(h,FILE_MAP_READ,0,0,0);
  if (package==0) return 0;
  ///special, network, pipes and services.
  if (wcsncmp(package->name,L"\\\\",2)==0) return 1;
  

  bool allowOpen=OnOpenFileReport(package->name,package->dwCreationDisposition,package->dwDesiredAccess);

  if (!allowOpen) return 0;
  if (_curTabPos==IDS_TABLOG)
  {
    _logFiles.Add(FileItem(Pathname(package->name),FileItem::stUnknown));
    RefreshList();
    
  }
  return 1;
}


void MainWnd::InitSourceSafeProvider()
{
  _curProject=0;
  _msScc=0;
  SRef<char ,SRefArrayTraits<char> >ssprovider=DupStrMB(_config.SSprovidersDll);
  SRef<char ,SRefArrayTraits<char> >ssserver=DupStrMB(_config.SSserver);
  SRef<char ,SRefArrayTraits<char> >ssproject=DupStrMB(_config.SSproject);
  SRef<char ,SRefArrayTraits<char> >sslocalpath=DupStrMB(_config.SSlocalpath);

   memset(&_providerInfo,0,sizeof(_providerInfo));
  _msScc=new MsSccLib();
  SCCRTN ret=_msScc->LoadScc((ssprovider.IsNull() || ssprovider.GetRef()[0]==0)?0:ssprovider.GetRef());
  if (ret && ret!=SCC_CE_LIBLINKERROR)
  {
    MessageBox(_hWnd,StrRes[IDS_UNABLETOLOADSOURCESAFEPROVIDER],0,MB_OK);
    _msScc=0;
    return;
  }

  _providerInfo.callerName=0;
  _curProject=new MsSccProject(*_msScc,&_providerInfo,_hWnd);
  if (_config.askOnFirstAccess || ssserver.IsNull() || ssserver.GetRef()[0]==0)
  {
    if (_curProject->ChooseProjectEx(ssproject,sslocalpath,ssserver)!=0)
    {
      MessageBox(_hWnd,StrRes[IDS_NOPROJECTCHOOSEN],0,MB_OK);
      _curProject=0;
      _msScc=0;
      return;
    }
  }
  else
  {
    if (_curProject->Open(ssproject,sslocalpath,ssserver)!=0)
    {
      MessageBox(_hWnd,StrRes[IDS_UNABLETOOPENPROJECT],0,MB_OK);
      _curProject=0;
      _msScc=0;
      return;
    }
  }
  _config.SSlocalpath=DupStrMB(_curProject->GetLocalPath());
  _config.SSserver=DupStrMB(_curProject->GetServerName());
  _config.SSusername=DupStrMB(_curProject->GetUserName());
  _config.SSproject=DupStrMB(_curProject->GetProjName());  
  _config.SaveConfig();
}

void MainWnd::ReFilter()
{
  if (_curTabPos==IDS_TABALLFILES)
    _files.ShowAll(_filter);
  else
  {
    _files.FilterBy(GetFilter(_curTabPos),_filter);
  }
  RefreshList();
}

LRESULT MainWnd::OnNotifyTab(LPNMHDR hdr)
{
  if (hdr->code==TCN_SELCHANGE)
  {
    _curTabPos=GetCurSelTab();
  }
  ReFilter();
  return 0;
}

class FilesAlphabet
{
  FileList &files;
public:
  FilesAlphabet(FileList &files):files(files) {}
  int operator()(const int *left, const int *right) const
  {
    return _tcsicmp(files[*left]._file.GetFilename(),files[*right]._file.GetFilename());
  }
};

class FilesAlphabetD
{
public:
  int operator()(const FileItem *left, const FileItem *right) const
  {
    return _tcsicmp(left->_file,right->_file);
  }
};

void MainWnd::RefreshList()
{
  int min=0;
  int max=_curTabPos==IDS_TABLOG?_logFiles.Size():_filter.Size();
  if (_curTabPos!=IDS_TABLOG)
  {
    QSort(_filter,FilesAlphabet(_files));
  }
  ListView_SetItemCount(_listCtrl,max);
  ListView_RedrawItems(_listCtrl,min,max);
}

LRESULT MainWnd::OnNotifyList(LPNMHDR hdr)
{
  switch (hdr->code)
  {
  case LVN_GETDISPINFO:
    OnDispInfoList((NMLVDISPINFO *)hdr);
    return 0;    
  }
  return 0;
}

void MainWnd::OnDispInfoList(NMLVDISPINFO *hdr)
{
  if (_curTabPos==IDS_TABLOG)
  {
    FileItem &item=_logFiles[hdr->item.iItem];
    hdr->item.pszText=const_cast<LPWSTR>(item._file.GetFullPath());
    hdr->item.iImage=0;
  }
  else
  {  
    FileItem &item=_files[_filter[hdr->item.iItem]];
    hdr->item.pszText=const_cast<LPWSTR>(item._file.GetFilename());
    hdr->item.iImage=item._state;  
  }
}

FileItem::FileState MainWnd::GetFilter(int tabMode)
{
  switch (tabMode)
  {
  case IDS_TABCHECKEDOUT: return FileItem::stCheckedOut;
  case IDS_TABNOTCHECKED: return FileItem::stCheckedIn;
  case IDS_TABNEWFILES: return FileItem::stNew;
  case IDS_TABOTHER: return FileItem::stOther;
  case IDS_TABOLDVER: return FileItem::stOldVer;
  case IDS_TABDELETED: return FileItem::stDeleted;
  }
  return FileItem::stUnknown;
}

static void ExploreFile(const Pathname &p)
{
    _TCHAR *buff=(_TCHAR *)alloca(sizeof(_TCHAR)*(_tcslen(p.GetFullPath())+50));
    _stprintf(buff,_T("explorer.exe /select,\"%s\""),p.GetFullPath());
    STARTUPINFO strinfo;
    PROCESS_INFORMATION pinfo;

    memset(&strinfo,0,sizeof(strinfo));
    strinfo.cb=sizeof(strinfo);
    strinfo.dwFlags=0;  
    BOOL ret=CreateProcess(NULL,buff,NULL,NULL,FALSE,CREATE_DEFAULT_ERROR_MODE|NORMAL_PRIORITY_CLASS,
      NULL,NULL,&strinfo,&pinfo);
    if (ret==FALSE) return;
    SetCursor(LoadCursor(NULL,IDC_WAIT));
    WaitForInputIdle(pinfo.hProcess,30000);
    CloseHandle(pinfo.hProcess);
    CloseHandle(pinfo.hThread);
}

bool MainWnd::OnOpenFileReport(const _TCHAR *filename, DWORD createDisposition, DWORD desiredAccess)
{
  if (_curProject.IsNull()) return true;

  if (!IsInterestingPath(filename)) return true;

  Pathname pthnm=filename;
  SCCSTAT sstat=_curProject->Status(pthnm.SBS_GetFullPath());
  if (sstat==SCC_STATUS_INVALID) return true;
  if (sstat & SCC_STATUS_CONTROLLED)
  {
    int i=_files.FindFile(pthnm);
    if (i==-1)
    {
      if (sstat & SCC_STATUS_CHECKEDOUT)
        _files.Add(FileItem(pthnm,FileItem::stCheckedOut));
      else if (sstat & SCC_STATUS_DELETED)
        _files.Add(FileItem(pthnm,FileItem::stDeleted));
      else
      {
        int action=_checkOutDecision;        
        if (GetTickCount()>_decisionExpiration)
        {                 
          CheckOutDlg dlg;
          dlg.title=pthnm;        
          dlg.askMeRes=(CheckOutDlg::AskMeResult)_config.savedExpiration;          
          action=dlg.DoModal(IDD_CHECKOUTDLG,_hWnd);
          if (action==IDCANCEL) return false;
          DWORD exp=dlg.askMeRes;
          if (exp<0xFFFFFFFE)
            _decisionExpiration=GetTickCount()+exp*1000;
          else
            _decisionExpiration=dlg.askMeRes;
          if (exp==0xFFFFFFFE)
            CloseWindow(_hWnd);
          _checkOutDecision=action;
          if (dlg.saveOption)
          {
            _config.savedExpiration=dlg.askMeRes;
            _config.SaveConfig();
          }
          if (dlg.selectedProject<-1)
          {
            ProcessAutoProject(dlg.selectedProject<-2,pthnm,action);
            ReFilter();
            return true;
          }
        }
        if (action==IDC_CHECKOUT)
        {
          _curProject->CheckOut(pthnm.SBS_GetFullPath());
          _files.Add(FileItem(pthnm,FileItem::stCheckedOut));
        }
        else if (action==IDC_GETLATESTVER)
        {
          _curProject->GetLatestVersion(pthnm.SBS_GetFullPath());
          _files.Add(FileItem(pthnm,FileItem::stCheckedIn));
        }
        else if (action==IDC_VIEWLOCAL)
        {
          _files.Add(FileItem(pthnm,FileItem::stLocalVersion));
        }      
      }
    }
    else
    {
      if (sstat & SCC_STATUS_CHECKEDOUT)      
        _files[i]._state=FileItem::stCheckedOut;
      else if (sstat & SCC_STATUS_DELETED)
        _files[i]._state=FileItem::stDeleted;
      else
      {
        if (_files[i]._state==FileItem::stCheckedOut)
          _files[i]._state=FileItem::stCheckedIn;
        if (desiredAccess & (WRITE_RIGHTS))
        {
           bool res=MakePostModifyCheckOut(pthnm);
           ReFilter(); 
           return res;
        }
      }
    }
    ReFilter();
    return true;
  }
  else
  {
    if (IsControledDir(pthnm) && (desiredAccess & WRITE_RIGHTS))
    {
      int i=_files.FindFile(pthnm);
      if (i==-1) {
        _files.Add(FileItem(pthnm,FileItem::stNew));
        SetTimer(_hWnd,1,4000,0);
      }
      ReFilter();
    }
    return true;
  }
}

bool MainWnd::IsControledDir(Pathname dirPath)
{
  if (_files.FindControlledDir(dirPath)) return true;
  dirPath.SetFilename(L"vssver.scc");
  return dirPath.TestFile();
}

void MainWnd::ProcessAutoProject(bool recursive, Pathname pth, int action,bool noget)
{
  if (!noget && action!=IDC_VIEWLOCAL)
  {
    _curProject->GetLatestVersionDir(pth.SBS_GetDirectoryWithDrive(),recursive);
  }
  AutoArray<Pathname> files;
  AutoArray<Pathname> folders;
  pth.SetFilename(_T("*.*"));
  folders.Add(pth);
  while (folders.Size())
  {
    WIN32_FIND_DATA wf;
    pth=folders[0];
    folders.Delete(0); 
    HANDLE h=FindFirstFile(pth,&wf);
    if (h!=INVALID_HANDLE_VALUE) 
    {
      do 
      {
        if (_tcscmp(wf.cFileName,_T("."))!=0 && _tcscmp(wf.cFileName,_T(".."))!=0)
        {
          Pathname nw=pth;
          nw.SetFilename(wf.cFileName);
          if (wf.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
          {
            if (recursive)
            {          
              nw.SetDirectory(nw);
              nw.SetFilename(_T("*.*"));
              folders.Add(nw);
            }
          }      
          else
          {
            files.Add(nw);
          }
        }
      } 
      while(FindNextFile(h,&wf));
      FindClose(h);
    }
    OnRefreshStatus();
  }

  SCCSTAT *status=(SCCSTAT *)alloca(sizeof(SCCSTAT)*files.Size());
  const char **filelist=(const char **)alloca(sizeof(const char *)*files.Size());
  const char **checkout=(const char **)alloca(sizeof(const char *)*files.Size());
  for (int i=0;i<files.Size();i++) filelist[i]=files[i].SBS_GetFullPath();
  _curProject->Status(filelist,files.Size(),status);
  int checkoutfs=0;
  FileItem::FileState st=action==IDC_CHECKOUT?FileItem::stCheckedOut:(action==IDC_GETLATESTVER?FileItem::stCheckedIn:FileItem::stLocalVersion);
  for (int i=0;i<files.Size();i++)
  {
    if (status[i]!=SCC_STATUS_INVALID && (status [i] & SCC_STATUS_CONTROLLED))
    {      
      if (action==IDC_CHECKOUT)
      {
        checkout[checkoutfs++]=files[i].SBS_GetFullPath();        
      }
      int sspos=_files.FindFile(files[i]);
      if (sspos==-1)
      {
        _files.Add(FileItem(files[i],st));
      }
      else
      {
        _files[sspos]._state=st;
      }
    }
  }
  if (checkoutfs)
  {
    _curProject->CheckOut(checkout,checkoutfs);
  }
  OnRefreshStatus();
}

static bool GetFileDateTime(const Pathname &pthname, FILETIME &time)
{
  WIN32_FIND_DATA wf;
  HANDLE h=FindFirstFile(pthname,&wf);
  if (h)
  {
    time=wf.ftLastWriteTime;
    FindClose(h);
    return true;
  }
  return false;
}

bool MainWnd::MakePostModifyCheckOut(const Pathname &pthnm)
{
  FILETIME f1,f2;
  bool msg=false;
  if (GetFileDateTime(pthnm,f1)==false) msg=true;
  else
  {
    _curProject->CheckOut(pthnm.SBS_GetFullPath());
    if (GetFileDateTime(pthnm,f2)==false) msg=true;
    msg=memcmp(&f1,&f2,sizeof(f1))!=0;
  }
  if (msg)
  {
    _curProject->UndoCheckOut(pthnm.SBS_GetFullPath());
    MessageBox(_hWnd,StrRes[IDS_NEWVERSIONNOTIFY],pthnm,MB_OK);
    return false;
  }
  else
    return true;
}

enum AllowedOpers
{
  opAdd=1,
  opCheckOut=2,
  opCheckIn=4,
  opGetLatestVer=8,
  opUndoCheckOut=16,
  opSSStats=32,
  opDelete=64,
  opRemove=128
};
LRESULT MainWnd::OnContextMenu(WPARAM wParam, LPARAM lParam)
{
  DWORD allowedOpers=0xFFFFFFFF;
  int countsel=ListView_GetSelectedCount(_listCtrl); 
  if (countsel==0 || _curProject.IsNull()) allowedOpers=0;  
  else
    OnRefreshStatus(); 


  int i=ListView_GetNextItem(_listCtrl,-1,LVIS_SELECTED);
  while (i>=0)
  {
    FileItem &itm=_files[_filter[i]]; 
    switch (itm._state)
    {
    case FileItem::stOther:
    case FileItem::stUnknown:allowedOpers&=~(opAdd|opCheckOut|opCheckIn|opGetLatestVer|opUndoCheckOut|opSSStats|opDelete|opRemove);break;
    case FileItem::stLocalVersion:
    case FileItem::stOldVer:
    case FileItem::stCheckedIn:allowedOpers&=~(opAdd|opCheckIn|opUndoCheckOut);break;
    case FileItem::stCheckedOut:allowedOpers&=~(opAdd|opCheckOut|opDelete|opRemove);break;
    case FileItem::stDeleted:allowedOpers&=~(opCheckOut|opCheckIn|opGetLatestVer|opUndoCheckOut|opRemove);break;
    case FileItem::stNew:allowedOpers&=~(opCheckOut|opCheckIn|opGetLatestVer|opUndoCheckOut|opSSStats|opDelete|opRemove);break;        
    };
    i=ListView_GetNextItem(_listCtrl,i,LVIS_SELECTED);
  }
  HMENU mnu1=LoadMenu((HINSTANCE)GetWindowLongPtr(_hWnd,GWL_HINSTANCE),MAKEINTRESOURCE(IDR_POPUPMENU));
  HMENU mnu2=GetSubMenu(mnu1,0);
  if (~allowedOpers & opAdd) 
  {
    EnableMenuItem(mnu2,ID_ITEMPOPUP_ADD,MF_GRAYED);
    EnableMenuItem(mnu2,ID_OTHER_CREATEPROJECT,MF_GRAYED);
  }
  if (~allowedOpers & opCheckOut) EnableMenuItem(mnu2,ID_ITEMPOPUP_CHECKOUT,MF_GRAYED);
  if (~allowedOpers & opCheckIn) EnableMenuItem(mnu2,ID_ITEMPOPUP_CHECKIN,MF_GRAYED);
  if (~allowedOpers & opGetLatestVer) EnableMenuItem(mnu2,ID_ITEMPOPUP_GETLATESTVERSION,MF_GRAYED);
  if (~allowedOpers & opUndoCheckOut) EnableMenuItem(mnu2,ID_ITEMPOPUP_UNDOCHECKOUT,MF_GRAYED);
  if (~allowedOpers & opRemove) EnableMenuItem(mnu2,ID_ITEMPOPUP_DELETEFROMSOURCESAFE,MF_GRAYED);
  if (~allowedOpers & opSSStats || countsel!=1)
  {
    EnableMenuItem(mnu2,ID_ITEMPOPUP_HISTORY,MF_GRAYED);
    EnableMenuItem(mnu2,ID_ITEMPOPUP_DIFFERENCIES,MF_GRAYED);
    EnableMenuItem(mnu2,ID_ITEMPOPUP_PROPERTIES32779,MF_GRAYED);
  }
  if (countsel!=1) EnableMenuItem(mnu2,ID_ITEMPOPUP_EXPLORE,MF_GRAYED);
  if (_curProject.IsNull()) 
  {
    EnableMenuItem(mnu2,ID_ITEMPOPUP_GETPROJECTFROMSOURCESAFE,MF_GRAYED);
  }
  if (~allowedOpers & opDelete) EnableMenuItem(mnu2,ID_ITEMPOPUP_DELETELOCALCOPY,MF_GRAYED);
  if (GetTickCount()>=_decisionExpiration) EnableMenuItem(mnu2,ID_ITEMPOPUP_REACTIVATEDLG,MF_GRAYED);
  UINT res=TrackPopupMenuEx(mnu2,TPM_NONOTIFY|TPM_RETURNCMD,GET_X_LPARAM(lParam),GET_Y_LPARAM(lParam),_hWnd,0);
  DestroyMenu(mnu1);
  InvokeMenuCommand(res);
  return res;
}


class ChangeSSProvider: public IDlgOptions
{
  Config &_config;
  MainWnd *outer;
public:
  ChangeSSProvider(MainWnd *outer, Config &config):outer(outer),_config(config) {}
  const char *ChangeServiceProvider(const _TCHAR *providersModule)
  {
    _config.SSprovidersDll=DupStr(providersModule);
    outer->InitSourceSafeProvider();
    return outer->_providerInfo.sccName;
  }
};


void MainWnd::AddFiles()
{
  int buffersize=65536;
  _TCHAR *buffer=new _TCHAR[buffersize];
  buffer[0]=0;

  OPENFILENAME ofn;
  ZeroMemory(&ofn,sizeof(ofn));

  ofn.lStructSize=OPENFILENAME_SIZE_VERSION_400;
  ofn.hwndOwner=_hWnd;
  ofn.hInstance=(HINSTANCE)GetWindowLong(_hWnd,GWL_HINSTANCE);
  ofn.lpstrFile=buffer;
  ofn.nMaxFile=buffersize;
  ofn.lpstrTitle=StrRes[IDS_CHOOSE_FILES_TO_ADD_TO_SOURCE_CONTROL];
  ofn.Flags=OFN_ALLOWMULTISELECT|OFN_DONTADDTORECENT|OFN_ENABLESIZING|OFN_EXPLORER|OFN_FILEMUSTEXIST|OFN_HIDEREADONLY|OFN_LONGNAMES|OFN_NONETWORKBUTTON|OFN_SHAREAWARE;
  if (GetOpenFileName(&ofn)==FALSE) 
  {
    delete [] buffer;
    return;
  }

  if (buffer[ofn.nFileOffset-1]==0)
  {
    Pathname pth;
    pth.SetDirectory(buffer);
    _TCHAR *c=buffer+ofn.nFileOffset;
    while (*c)
    {
      pth.SetFilename(c);
      _files.Add(FileItem(pth,FileItem::stNew));
      c=_tcschr(c,0)+1;      
    }    
  }
  else
  {
    _files.Add(FileItem(Pathname(buffer),FileItem::stNew));
  }

  OnRefreshStatus();
  delete [] buffer;
}

void MainWnd::InvokeMenuCommand(UINT res)
{
  int countsel=ListView_GetSelectedCount(_listCtrl); 
  int i;
  
  if (countsel)
  {
    const char **arr=(const char **)alloca(sizeof(const char *)*countsel);    
    FileItem **fl=(FileItem **)alloca(sizeof(FileItem *)*countsel);    
    int idx=0;
    i=ListView_GetNextItem(_listCtrl,-1,LVIS_SELECTED);
    while (i>=0)
    {
      FileItem &itm=_files[_filter[i]]; 
      fl[idx]=&itm;
      arr[idx++]=itm._file.SBS_GetFullPath();
      i=ListView_GetNextItem(_listCtrl,i,LVIS_SELECTED);
    }

    switch (res)
    {
    case ID_ITEMPOPUP_ADD:
      {
        DlgComments dlg;
        dlg.files=Array<const FileItem *>((const FileItem **)fl,countsel);                
        if (dlg.DoModal(IDD_COMMENTS,_hWnd)==IDOK)
        {
          LONG *flags=(LONG *)alloca(sizeof(DWORD)*countsel);
          for (int i=0;i<countsel;i++) 
            flags[i]=(dlg.noHistory?SCC_ADD_STORELATEST:0)|(dlg.fileType);
          SRef<char>sbstext=DupStrMB(dlg.comments.GetRef());
          _curProject->MsSccProjectBase::Add(countsel,arr,sbstext.GetRef(),0,flags,flags);
          if (dlg.keepCheckedOut)
            _curProject->CheckOut(arr,countsel);
          if (_curMode==modePostExit)   {PostExitCheck(); }
        }
      }
      break;
    case ID_ITEMPOPUP_CHECKOUT:
      _curProject->CheckOut(arr,countsel);break;
    case ID_ITEMPOPUP_CHECKIN:
      {        
        DlgComments dlg;
        dlg.checkIn=true;
        dlg.files=Array<const FileItem *>((const FileItem **)fl,countsel);        
        if (dlg.DoModal(IDD_COMMENTS,_hWnd)==IDOK)
        {
          SRef<char>sbstext=DupStrMB(dlg.comments);
          _curProject->Checkin(countsel,arr,sbstext.GetRef(),0,dlg.keepCheckedOut?SCC_KEEP_CHECKEDOUT:0);
          if (_curMode==modePostExit)   {PostExitCheck(); }
        }
      }
      break;
    case ID_ITEMPOPUP_GETLATESTVERSION:
      _curProject->GetLatestVersion(arr,countsel);
      for (int i=0;i<countsel;i++) 
        if (fl[i]->_state==FileItem::stOldVer ||
          fl[i]->_state==FileItem::stLocalVersion)
          fl[i]->_state=FileItem::stCheckedOut;
    case ID_ITEMPOPUP_UNDOCHECKOUT:
      _curProject->UndoCheckOut(arr,countsel);
      if (_curMode==modePostExit)   {PostExitCheck(); }
      break;
    case ID_ITEMPOPUP_HISTORY:
      {      
      SCCRTN res=_curProject->History(arr[0]);
      if (res== SCC_I_RELOADFILE) 
      {
        MessageBox(_hWnd,StrRes[IDS_HISTORYWARNING],fl[0]->_file,MB_OK);
        fl[0]->_state=FileItem::stOldVer;
      }
      }
      break;
    case ID_ITEMPOPUP_DELETEFROMSOURCESAFE:      
        _curProject->Remove(arr,countsel);        
      break;    
    case ID_ITEMPOPUP_DIFFERENCIES:
      _curProject->Diff(arr[0]);break;
    case ID_ITEMPOPUP_PROPERTIES32779:
      _curProject->Properties(arr[0]);break;
    case ID_ITEMPOPUP_EXPLORE:
      ExploreFile(fl[0]->_file);
      break;
    case ID_ITEMPOPUP_DELETELOCALCOPY:
      {
        size_t needbuff=0;
        for (int i=0;i<countsel;i++) needbuff+=_tcslen(fl[i]->_file.GetFullPath())+1;
        needbuff++;
        wchar_t *buff=(wchar_t *)alloca(sizeof(wchar_t)*needbuff);
        needbuff=0;
        for (int i=0;i<countsel;i++) 
        {
          _tcscpy(buff+needbuff,fl[i]->_file);
          needbuff+=_tcslen(fl[i]->_file.GetFullPath())+1;
        }
        buff[needbuff]=0;  
        SHFILEOPSTRUCT info;
        ZeroMemory(&info,sizeof(info));
        info.hwnd=_hWnd;
        info.wFunc=PO_DELETE;
        info.pFrom=buff;
        info.pTo=0;
        info.fFlags=FOF_ALLOWUNDO;
        SHFileOperation(&info);
      }
      break;
    case ID_OTHER_CREATEPROJECT:
      {
          if (_curProject==0) InitSourceSafeProvider();
          const char *c=fl[0]->_file.ToSBS(fl[0]->_file.GetDirectoryFullPath());
          c=strrchr(c,'\\')+1;
          char *d=(char *)alloca(strlen(c)+1);
          strcpy(d,c);
          _curProject->CreateProject(d,fl[0]->_file.ToSBS(fl[0]->_file.GetDirectoryFullPath()),0,0);
      }
      break;
    case ID_OTHER_RUNSCCFRONTEND:
      _curProject->RunScc(countsel,arr,_hWnd);
      break;
    }
    OnRefreshStatus();
  }
  else
  {
    switch (res)
    {
    case ID_OTHER_RUNSCCFRONTEND:
      _curProject->RunScc(0,0,_hWnd);
      break;
    }
  }

  switch (res)
  {
  case ID_ITEMPOPUP_REACTIVATEDLG:
    _decisionExpiration=0;
    break;
  case ID_ITEMPOPUP_CLEARLOCKEDFILES:
    _files.ClearLocked();
    ReFilter();
    break;
  case ID_ITEMPOPUP_SELECTALL:
    for (int i=0,cnt=ListView_GetItemCount(_listCtrl);i<cnt;i++)    
      ListView_SetItemState(_listCtrl,i,LVIS_SELECTED,LVIS_SELECTED);
    break;
  case ID_ITEMPOPUP_CHANGEPROJECT:
    {
      bool ask=_config.askOnFirstAccess;
      _config.askOnFirstAccess=true;
      InitSourceSafeProvider();
      _config.askOnFirstAccess=false;
      _config.SaveConfig();
      OnRefreshStatus();
      break;
    }
    
  case ID_ITEMPOPUP_REFRESH:
      OnRefreshStatus();
      break;
  case ID_ITEMPOPUP_GETPROJECTFROMSOURCESAFE:
    {
      SccFunctions *scc=_curProject->Clone();
      if (scc->ChooseProjectEx(_curProject->GetProjName(),_curProject->GetLocalPath(),_curProject->GetServerName(),_curProject->GetUserName())==0)
      {
        int rec=MessageBox(_hWnd,StrRes[IDS_ASKPROCESSRECURSIVE],StrRes[IDS_GETPROJECTFROMSERVER],MB_YESNOCANCEL|MB_ICONQUESTION);
        if (rec!=IDCANCEL)
          _curProject->GetLatestVersionDir(scc->GetLocalPath(),rec==IDYES); 
      }
      delete scc;
    }
    break;
  case ID_ITEMPOPUP_OPTIONS:
    {
      ChangeSSProvider pom(this,_config);
      DlgOptions dlg(_config,&pom);
      dlg._providerName=_providerInfo.sccName;
      if (dlg.DoModal(IDD_OPTIONS,_hWnd)==IDOK)
      {
        _config.SaveConfig();
      }
    }
    break;
  case ID_OTHER_SHARE:
    {
      MsSccProject *project=static_cast<MsSccProject *>(_curProject);
      LONG files;
      LPCSTR *lpFileNames;
      project->AddFromScc(&files,&lpFileNames,_hWnd);
      for (int i=0;i<files;i++)
      {
#ifdef _UNICODE
        size_t needsz=MultiByteToWideChar(CP_THREAD_ACP,0,lpFileNames[i],strlen(lpFileNames[i])+1,0,0);
        _TCHAR *t=new _TCHAR[needsz];
        MultiByteToWideChar(CP_THREAD_ACP,0,lpFileNames[i],strlen(lpFileNames[i])+1,t,needsz);
        _files.Add(FileItem(Pathname(t),FileItem::stCheckedIn));
        delete [] t;       
#else
        _files.Add(FileItem(Pathname(lpFileNames[i]),FileItem::stCheckedIn));
#endif
      }
      OnRefreshStatus();
    }
    break;

  case ID_OTHER_ADDFILES:
    {
      AddFiles();
    }
    break;
  }
    
  
}

void MainWnd::OnRefreshStatus()
{
  if (_curProject.IsNull()) return;

  const char **arr=(const char **)alloca(sizeof(const char *)*_files.Size()); 
  SCCSTAT *st=(SCCSTAT *)alloca(sizeof(SCCSTAT)*_files.Size());

  for (int i=0;i<_files.Size();i++) arr[i]=_files[i]._file.SBS_GetFullPath();
  _curProject->Status(arr,_files.Size(),st);

  for (int i=0;i<_files.Size();i++)
  {
    if (st[i]==0 || st[i]==-1) _files[i]._state=FileItem::stNew;
    else 
    {
      if (st[i] & SCC_STATUS_CHECKEDOUT) _files[i]._state=FileItem::stCheckedOut;
      else if (st[i] & SCC_STATUS_DELETED) _files[i]._state=FileItem::stDeleted;
      else 
      {
        if (_files[i]._state!=FileItem::stOldVer && 
          _files[i]._state!=FileItem::stLocalVersion &&
          _files[i]._state!=FileItem::stCheckedIn)  _files[i]._state=FileItem::stCheckedIn;
      }
    }
  }
  for (int i=0;i<_files.Size();i++)
  {
    if (_files[i]._file.TestFile()==false)
    {
      CheckForNewFilesOnDeletion(_files[i]._file,_files[i]._creation);
      _files.Delete(i--);
    }
  }
  ReFilter();
}

void MainWnd::CheckForNewFilesOnDeletion(const Pathname &deletedFile, const FILETIME &ftime)
{
  Pathname search;
  search.SetDirectory(deletedFile.GetDirectoryWithDrive());
  WIN32_FIND_DATA fdata;
  HANDLE h=FindFirstFile(search,&fdata);
  bool hit=false;
  Pathname found=search;
  if (h!=INVALID_HANDLE_VALUE)
  {
    do
    {
      if (~fdata.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
      {
        if (fdata.ftLastWriteTime.dwHighDateTime>ftime.dwHighDateTime ||
          fdata.ftLastWriteTime.dwHighDateTime==ftime.dwHighDateTime && fdata.ftLastWriteTime.dwLowDateTime>=ftime.dwLowDateTime)
        {
          found.SetFilename(fdata.cFileName);
          int i=_files.FindFile(found);
          if (i==-1)
          {
             _files.Add(FileItem(found,FileItem::stNew));
              hit=true;              
          }
        }
      }
    }
    while (FindNextFile(h,&fdata));
    FindClose(h);
  }

  if (hit)
  {
    SetTimer(_hWnd,1,500,0);
    ReFilter();
  } 
}


void MainWnd::OnExitSizeMove()
{
  WINDOWPLACEMENT wpos;
  wpos.length=sizeof(wpos);
  GetWindowPlacement(_hWnd,&wpos);
  _config.winRect=wpos.rcNormalPosition;
  _config.SaveConfig();
}

bool MainWnd::OnTimer(WPARAM wParam, LPARAM lParam)
{
      if (wParam==1)
    {
      KillTimer(_hWnd,1);
      OnRefreshStatus();
      return true;
    }
    else if (wParam==2)
    {
      if (_curMode==modeConnected)
      {
        HWND fore=GetForegroundWindow();
        DWORD pid;
        ::GetWindowThreadProcessId(fore,&pid);
        if (pid==_servPID || pid==GetCurrentProcessId())
        {
          if (!IsWindowVisible(_hWnd)) ShowWindow(_hWnd,SW_SHOWNA);
          _TCHAR winName[256];
          GetWindowText(fore,winName,sizeof(winName)/sizeof(winName[0]));
          SetWindowText(_hWnd,winName);
        }
        else
          ShowWindow(_hWnd,SW_HIDE);
      }
      KillTimer(_hWnd,2);
      return true;
    }
    return false;
}

bool MainWnd::IsInterestingPath(const _TCHAR *path)
{
  for (int i=0;i<_config.alwaysSSPaths.Size();i++)
  {
    size_t len=_tcslen(_config.alwaysSSPaths[i]);
    if (_tcsnicmp(path,_config.alwaysSSPaths[i],len)==0 && (_config.alwaysSSPaths[i][len-1]=='\\' || path[len]=='\\'))
      return true;
  }
  for (int i=0;i<_config.neverSSPaths.Size();i++)
  {
    size_t len=_tcslen(_config.neverSSPaths[i]);
    if (_tcsnicmp(path,_config.neverSSPaths[i],len)==0 && (_config.neverSSPaths[i][len-1]=='\\' || path[len]=='\\'))
      return false;
  }
  return true;
}