#pragma once

#include "Config.h"
#include "FileList.h"

class MainWnd: public MultiThread::ThreadBase
{
  friend class ChangeSSProvider;
  HWND _hWnd;

  MultiThread::ProcessBlocker _servBlock;
  MultiThread::EventBlocker _forceExit;
  DWORD _servPID;

  HIMAGELIST _icons;

  HWND _tabCtrl;
  HWND _listCtrl;
  enum AppMode
  {
    modeUnbound, ///<application is unbound, there is no connection to the server
    modeConnected, ///<application is connected and listening notifications
    modePostExit ///<application exited, user leaved some file checkOut
  };

  SccProviderInfo _providerInfo;
  SRef<MsSccLib> _msScc;
  SRef<MsSccProject> _curProject;
  Config _config;

  AppMode _curMode;
  FileList _files;
  AutoArray<int,MemAllocStack<int,256> > _filter;
  FileList _logFiles;

  int _curTabPos;

  DWORD _decisionExpiration;
  int _checkOutDecision;
  

  void ProcessAutoProject(bool recursive, Pathname pth, int action, bool noget=false);
  


protected:
  DWORD SetServerID(DWORD processId);
  void SetPostExit();
  void PostExitCheck();
  void OnWmClose();
  void OnWmSize();
  LRESULT OnFileActionReport(LPARAM);
  int GetCurSelTab() const;    
  void InitSourceSafeProvider();
  LRESULT OnNotifyTab(LPNMHDR hdr);
  LRESULT OnNotifyList(LPNMHDR hdr);
  void RefreshList();
  void OnDispInfoList(NMLVDISPINFO *hdr);
  FileItem::FileState GetFilter(int tabMode);
  bool OnOpenFileReport(const _TCHAR *filename, DWORD createDisposition, DWORD desiredAccess);
  bool IsControledDir(Pathname dirPath);
  void ReFilter();
  bool MakePostModifyCheckOut(const Pathname &pthnm);
  LRESULT OnContextMenu(WPARAM wParam, LPARAM lParam);
  void OnRefreshStatus();
  void InvokeMenuCommand(UINT cmd);
  void CheckForNewFilesOnDeletion(const Pathname &deletedFile,const FILETIME &ftime);
  void OnExitSizeMove();
  bool OnTimer(WPARAM wParam, LPARAM lParam);
  bool IsInterestingPath(const _TCHAR *pathname);
  void AddFiles();

public:
  MainWnd(HWND bWindow);
  ~MainWnd(void);


  HWND GetHwnd() const {return _hWnd;}
  virtual LRESULT WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
  virtual unsigned long Run();
 

};
