#pragma once
#include "dialogclass.h"
#include "Config.h"

class IDlgOptions
{
public:
  virtual const char *ChangeServiceProvider(const _TCHAR *providersModule)=0;
};
  
class DlgOptions : public CDialogClass
{
  IDlgOptions *_notify;
public:
  const char *_providerName;
  
  Config &_config;

  DlgOptions(Config &config,IDlgOptions *notify);
  ~DlgOptions(void);

  LRESULT OnInitDialog();
  LRESULT OnOK();
  LRESULT OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt);

protected:

  void OnChangeServiceProvider();
  void OnDefaultserviceProvider();
};
