#pragma once

#include "XMLCommon.h"

template<class Stream, bool autoDelete=false>
class XMLOut
{
  bool _nl;
public:
  enum Error
  {
    noError=0,
    errNestedAttributes,
    errUnexceptedAttribute,
    errUnclosedAttribute,
    errUnsupported,
    errStreamError,
  };
protected:
  Error _err;
  Stream *_stream;

  void WriteStr(const char *str)
  {
    if (_err!=noError) return;
    size_t len=strlen(str);    
    size_t wr;
    if (_stream->WriteStream(str,len,wr)==false || wr!=len) _err=errStreamError;
  }

  bool _inTag;
  bool _inAttr;
  
  void CloseTag()
  {
    if (_inTag)
    {
      if (_inAttr) CloseSection("_");
      WriteStr(">");
      _inTag=false;
    }
  }

  void PrepareForData()
  {
    if (_inAttr) return;
    CloseTag();
  }

  void WriteXMLData(const char *data, int size=-1)
  {
    if (size==-1) size=strlen(data);
    char buff[256];
    int buffPtr=0;
    for (int i=0;i<size;i++)
    {
      switch (data[i])
      {
      case '&': strcpy(buff+buffPtr,"&amp;");buffPtr+=5;break;
      case '\"': strcpy(buff+buffPtr,"&quot;");buffPtr+=6;break;
      case '<': strcpy(buff+buffPtr,"&lt;");buffPtr+=4;break;
      case '>': strcpy(buff+buffPtr,"&gt;");buffPtr+=4;break;
      default: buff[buffPtr]=data[i];buffPtr++;break;
      };
      if (buffPtr>240)
      {
        buff[buffPtr]=0;
        WriteStr(buff);
        buffPtr=0;
      }
    }
    buff[buffPtr]=0;
    WriteStr(buff);
  }
public:

  XMLOut(Stream *stream):_err(noError),_stream(stream) {_inTag=false;_inAttr=false;}
  ~XMLOut() {if (autoDelete) delete _stream;}  

  bool OpenSection(const char *name)
  {
    if (_err!=noError) return false;
    
    if (name[0]=='_') //attribute
    {
      name++;
      if (_inTag)
      {
        if (_inAttr) {_err=errNestedAttributes;return false;}
        const char *attrName=XMLTagName(name,(char *)alloca(strlen(name)+1));
        WriteStr("\n\t");
        WriteStr(attrName);
        WriteStr("=\"");
        _inAttr=true;
        return true;
      }
      else
      {
        _err=errUnexceptedAttribute;
        return false;
      }
    }
    if (_inAttr)
    {
      _err=errUnclosedAttribute;
      return false;
    }
    CloseTag();
    if (!_nl) {WriteStr("\n");}
    else _nl=false;
    WriteStr("<");
    WriteStr(XMLTagName(name,(char *)alloca(strlen(name)+1)));
    _inTag=true;
    return true;
  }

  void CloseSection(const char *name)
  {
    if (_err!=noError) return;
    if (_inAttr)
    {
      WriteStr("\"");
      _inAttr=false;
      return;
    }
    if (_inTag)
    {
      WriteStr("/>");
      _inTag=false;
      return;
    }
    WriteStr("</");
    WriteStr(XMLTagName(name,(char *)alloca(strlen(name)+1)));
    WriteStr(">\n");
    _nl=true;
  }

  int PeekSection(char *buffer, int bufferSize)
  {
    if (_err!=noError) return 0;
    _err=errUnsupported;
    return 0;
  }

  bool IsWritting() const
  {
    return true;
  }

  bool IsError() const
  {
    return _err!=noError;
  }

  void BinExchange(void *data, int size)
  {
    PrepareForData();
    WriteXMLData(data,size);
  }

  void Exchange(char item)
  {
    PrepareForData();
    WriteXMLData(&item,1);
  }

  void Exchange(unsigned char item) {Exchange((unsigned long)item);}
  void Exchange(short item) {Exchange((long)item);}
  void Exchange(unsigned short item) {Exchange((unsigned long)item);}
  void Exchange(int item) {Exchange((long)item);}
  void Exchange(unsigned int item) {Exchange((unsigned long)item);}
  void Exchange(long item) 
  {
    PrepareForData();
    char buff[50];
    sprintf(buff,"%d",item);
    WriteXMLData(buff);    
  }
  void Exchange(unsigned long item) 
  {
    PrepareForData();
    char buff[50];
    sprintf(buff,"%u",item);
    WriteXMLData(buff);    
  }
  void Exchange(float item) 
  {
    PrepareForData();
    char buff[100];
    sprintf(buff,"%g",item);
    WriteXMLData(buff);    
  }
  void Exchange(double item) 
  {
    PrepareForData();
    char buff[100];
    sprintf(buff,"%g",item);
    WriteXMLData(buff);    
  }
  void Exchange(long long item) 
  {
    PrepareForData();
    char buff[100];
    sprintf(buff,"%I64d",item);
    WriteXMLData(buff);    
  }
  void Exchange(unsigned long long item) 
  {
    PrepareForData();
    char buff[100];
    sprintf(buff,"%I64u",item);
    WriteXMLData(buff);    
  }
  void TextExchange(const char *text)
  {
    PrepareForData();
    if (text) WriteXMLData(text);
  }
  void TextExchange(const wchar_t *text)
  {
    if (_err!=noError) return;    
    PrepareForData();
    if (text)
    {
      int bufsz=XMLInOutHelp::TextUnicodeToUTF8(text,0,0);
      char *wtext=new char[bufsz];
      XMLInOutHelp::TextUnicodeToUTF8(text,wtext,bufsz);
      if (wtext) WriteXMLData(wtext);
      delete [] wtext;
    }
  }
  void Ver(int ver, const char *name)
  {
    OpenSection(name);
    Exchange(ver);
    CloseSection(name);
  }
  void Exchange(bool item)
  {
    if (item) WriteStr("true");
    else WriteStr("false");
  }

  void WriteHeader(const char *encoding="ISO-8859-1", const char *version="1.0")
  {
    WriteStr("<?xml version=\"");
    WriteStr(version);
    WriteStr("\" encoding=\"");
    WriteStr(encoding);
    WriteStr("\"?>");
  }
};
