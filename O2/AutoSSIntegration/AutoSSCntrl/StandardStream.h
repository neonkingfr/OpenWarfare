#pragma once

template<class IOStream, bool autoDelete>
class StandardStream
{
  IOStream *_stream;
public:

  StandardStream(IOStream *stream)
  {
    _stream=stream;
  }

  ~StandardStream(void)
  {
    if (autoDelete) delete _stream;
  }

  template<class T>
  bool WriteStream(T *data, size_t towrite, size_t &waswritten)
  {
    _stream->write(reinterpret_cast<const char *>(data),towrite*sizeof(T));
    if (!(*_stream)) {waswritten=0;return false;}
    else {waswritten=towrite;return true;}
  }

  template <class Item>
  bool ReadStream(Item *array, size_t toread, size_t &wasread)
  {    
    _stream->read(reinterpret_cast<char *>(array),toread*sizeof(Item));
    if (_stream->rdstate() & ios_base::eofbit)
      _stream->clear();
    if (!(*_stream)) {wasread=0;return false;}
    wasread=_stream->gcount()/sizeof(Item);
    return true;
  }
};

