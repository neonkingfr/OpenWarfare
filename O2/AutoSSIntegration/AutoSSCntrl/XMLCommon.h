#pragma  once


static inline const char *XMLTagName(const char *name, char *buffer)
{
  const char *a=name;
  char *b=buffer;
  while (*a)
  {
    if (isalpha(*a)) *b=*a;
    else if (isdigit(*a))
      if (a==name) b--;
      else
        *b=*a;
    else
      *b='_';
    a++;
    b++;
  }
  *b=0;
  return buffer;
}
