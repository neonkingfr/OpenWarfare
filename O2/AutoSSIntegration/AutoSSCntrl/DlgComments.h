#pragma once
#include "dialogclass.h"

struct FileItem;

class DlgComments :
  public CDialogClass
{
  HFONT commentsFont;
public:
  SRef<wchar_t> comments;

  Array<const FileItem *> files;

  bool keepCheckedOut;
  bool noHistory;
  int fileType;

  bool checkIn;
  
  DlgComments();
  ~DlgComments();

  virtual LRESULT OnOK();
  virtual LRESULT OnInitDialog();
};
