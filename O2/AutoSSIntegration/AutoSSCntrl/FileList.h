#pragma once

struct FileItem
{
  enum FileState
  {
    stUnknown=0,
    stCheckedOut=1,
    stCheckedIn=2,
    stLocalVersion=3,
    stNew=4,
    stOldVer=5,
    stOther=6,
    stDeleted=7
  };

  Pathname _file;
  FileState _state;
  FILETIME _creation;

  FileItem() {}
  FileItem(const Pathname &pathname, FileState state):_file(pathname),_state(state) 
  { 
    SYSTEMTIME systm;
    GetSystemTime(&systm);
    SystemTimeToFileTime(&systm,&_creation);
  }
  ClassIsMovable(FileItem);
};

class FileList: public AutoArray<FileItem>
{
public:
  int FindFile(const Pathname &name);
  bool FindControlledDir(const Pathname &name);

  void FileList::ClearLocked()
  {
    for (int i=0;i<Size();i++)
      if ((*this)[i]._state!=FileItem::stCheckedOut && (*this)[i]._state!=FileItem::stNew)
      {
        Delete(i);
        i--;
      }
  }

  template<class Array>
  void FilterBy(FileItem::FileState state, Array &arr)
  {
    arr.Clear();
    for (int i=0;i<Size();i++)
      if ((*this)[i]._state==state)
        arr.Add(i);
  }

  template<class Array>
    void ShowAll(Array &arr)
  {
    arr.Clear();
    for (int i=0;i<Size();i++) arr.Add(i);
  }
};
