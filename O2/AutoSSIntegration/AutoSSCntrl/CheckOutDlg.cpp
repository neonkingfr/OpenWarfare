#include "StdAfx.h"
#include ".\checkoutdlg.h"
#include "resource.h"

static HBRUSH CreateChessBoardBrush(HWND wnd)
{
  HDC tempdc=GetDC(wnd);
  HDC canvas=CreateCompatibleDC(tempdc);
  HBITMAP img=CreateCompatibleBitmap(tempdc,8,8);
  HBITMAP old=(HBITMAP)SelectObject(canvas,img);
  COLORREF col1=GetSysColor(COLOR_WINDOW);
  COLORREF col2=GetSysColor(COLOR_BTNFACE);
  for(int x=0;x<8;x++)
    for (int y=0;y<8;y++)
      if ((x+y)&1)
        SetPixel(canvas,x,y,col1);
      else
        SetPixel(canvas,x,y,col2);
  SelectObject(canvas,old);
  HBRUSH res=CreatePatternBrush(img);
  ReleaseDC(wnd,tempdc);
  DeleteDC(canvas);
  DeleteBitmap(img);
  return res;
}


CheckOutDlg::CheckOutDlg(void):availAbleProjects(0)
{
  LOGFONT lgf;
  lgf.lfCharSet=DEFAULT_CHARSET;
  lgf.lfClipPrecision=CLIP_DEFAULT_PRECIS;
  lgf.lfEscapement=0;
  _tcscpy(lgf.lfFaceName,_T("Arial"));
  lgf.lfHeight=-20;
  lgf.lfItalic=FALSE;
  lgf.lfOrientation=0;
  lgf.lfOutPrecision=OUT_DEFAULT_PRECIS;
  lgf.lfPitchAndFamily=DEFAULT_PITCH|FF_SWISS;
  lgf.lfQuality=DEFAULT_QUALITY;
  lgf.lfStrikeOut=FALSE;
  lgf.lfUnderline=FALSE;
  lgf.lfWeight=FW_BOLD;
  lgf.lfWidth=0;
  titleFont=CreateFontIndirect(&lgf);
  askMeRes=amAskMeNextTime;
  saveOption=false;
  selectedProject=-1;
  title=_T("");
}

CheckOutDlg::~CheckOutDlg(void)
{
  DeleteObject(titleFont);
  DeleteBrush(winBgrn);
}

static CheckOutDlg::AskMeResult tmp1[]={
  CheckOutDlg::amAskMeNextTime,
    CheckOutDlg::amThisSession,
    CheckOutDlg::amUntilSave,
    CheckOutDlg::am10Sec,
    CheckOutDlg::am20Sec,
    CheckOutDlg::am1Min,
    CheckOutDlg::am5Min};

LRESULT CheckOutDlg::OnInitDialog()
{
  titleWnd=GetDlgItem(IDC_FILENAME);
  for (int i=0;i<lenof(tmp1);i++) if (askMeRes==tmp1[i]) CheckDlgButton(*this,IDC_ASKMENEXT+i,BST_CHECKED);
  projectCombo=GetDlgItem(IDC_AVAILABLEPROJECTS);
  int p=ComboBox_AddString(projectCombo,StrRes[IDS_NOPROJECT]);
  ComboBox_SetCurSel(projectCombo,p);
  ComboBox_SetItemData(projectCombo,p,-1);
  p=ComboBox_AddString(projectCombo,StrRes[IDS_CURRPROJECT]);
  ComboBox_SetItemData(projectCombo,p,-2);
  p=ComboBox_AddString(projectCombo,StrRes[IDS_CURRPROJECTANDDESCENDANTS]);
  ComboBox_SetItemData(projectCombo,p,-3);
  for (int i=0;i<availAbleProjects.Size();i++)
  {
    p=ComboBox_AddString(projectCombo,availAbleProjects[i]);
    ComboBox_SetItemData(projectCombo,p,i);
  }
  SetWindowText(titleWnd,title);
  ::SendMessage(titleWnd,WM_SETFONT,(WPARAM)titleFont,TRUE);
  CheckDlgButton(*this,IDC_SAVEOPTIONS,saveOption?BST_CHECKED:BST_UNCHECKED);

  SendDlgItemMessage(IDC_CHECKOUT,BM_SETIMAGE,IMAGE_ICON,(LPARAM)LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_CHECKOUT)));
  SendDlgItemMessage(IDC_GETLATESTVER,BM_SETIMAGE,IMAGE_ICON,(LPARAM)LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_GETLATEST)));
  SendDlgItemMessage(IDC_VIEWLOCAL,BM_SETIMAGE,IMAGE_ICON,(LPARAM)LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_VIEWLOCAL)));
  SendDlgItemMessage(IDCANCEL,BM_SETIMAGE,IMAGE_ICON,(LPARAM)LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_CANCEL)));

  winBgrn=CreateChessBoardBrush(*this);  
/*  SetWindowLong(*this,GWL_EXSTYLE,GetWindowLong(*this,GWL_EXSTYLE)|WS_EX_LAYERED);
  SetLayeredWindowAttributes(*this,RGB(255,0,255),0,LWA_COLORKEY);*/

  return 1;
}

LRESULT CheckOutDlg::OnOK()
{
  for (int i=0;i<lenof(tmp1);i++) if (IsDlgButtonChecked(*this,IDC_ASKMENEXT+i)==BST_CHECKED)
    askMeRes=tmp1[i];
  int cursel=ComboBox_GetCurSel(projectCombo);
  selectedProject=ComboBox_GetItemData(projectCombo,cursel);
  saveOption=IsDlgButtonChecked(*this,IDC_SAVEOPTIONS)==BST_CHECKED;
  return 0;
}

LRESULT CheckOutDlg::OnCommand(unsigned short wID, unsigned short wNotifyCode, HWND clt)
{
  switch(wID)
  {
  case IDC_CHECKOUT:
  case IDC_GETLATESTVER:
  case IDC_VIEWLOCAL:
    OnOK();
    EndDialog(wID);
    return 1;  
  }
  return CDialogClass::OnCommand(wID,wNotifyCode,clt);
}

LRESULT CheckOutDlg::DlgProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
  case WM_CTLCOLORDLG:
  case WM_CTLCOLORSTATIC:
    return (LRESULT)OnCtlColorDlg(wParam,lParam);    
  default: return CDialogClass::DlgProc(msg,wParam,lParam);
  }
}
HBRUSH CheckOutDlg::OnCtlColorDlg(WPARAM wParam, LPARAM lParam)
{
  HDC dc=(HDC)wParam;
  SetBkMode(dc,TRANSPARENT);
  return winBgrn;
}