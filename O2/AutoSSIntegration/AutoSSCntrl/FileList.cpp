#include "StdAfx.h"
#include ".\filelist.h"

int FileList::FindFile(const Pathname &name)
{
  for (int i=0;i<Size();i++)
  {
    if (_tcsicmp(name,operator[](i)._file)==0) return i;
  }
  return -1;
}


bool FileList::FindControlledDir(const Pathname &name)
{
  for (int i=0;i<Size();i++) if ((*this)[i]._state==FileItem::stCheckedOut || (*this)[i]._state==FileItem::stCheckedIn || (*this)[i]._state==FileItem::stLocalVersion || (*this)[i]._state==FileItem::stOldVer)
  {
    if (_tcsicmp(name.GetDirectoryWithDrive(),(*this)[i]._file.GetDirectoryWithDrive())==0) return true;
  }
  return false;

}
