// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows XP or later.
#define WINVER 0x0501		// Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 6.0 or later.
#define _WIN32_IE 0x0600	// Change this to the appropriate value to target other versions of IE.
#endif

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include <WindowsX.h>



// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <fstream>
#include "StringRes.h"
#include <CommDlg.h>

#include <es/strings/RString.hpp>
#include <es/types/pointers.hpp>
#include <es/containers/array.hpp>
#include <el/MultiThread/ThreadBase.h>
#include <TlHelp32.h>
#include <el/Pathname/pathname.h>
#include <el/MultiThread/BlockerSimpleBase.h>
#include <CommCtrl.h>

#include "StandardStream.h"
//#include "..\..\..\Linda\LindaStudioGen\StreamParser.tli"
#include "XMLParser.tli"
#include "XMLIn.h"
#include "XMLOut.h"
#include "Archive.h"

#include <el/Interfaces/iScc.hpp>
#include <el/Scc/MsSccLib.hpp>
#include <el/Scc/MsSccProject.hpp>

#include <es/algorithms/qsort.hpp>


extern CStringRes StrRes;

HINSTANCE AfxGetInstanceHandle();

TypeIsMovable(Pathname);
// TODO: reference additional headers your program requires here
