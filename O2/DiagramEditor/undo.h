#ifndef _UNDO_H
#define _UNDO_H

#include <el/paramfile/paramfile.hpp>
#include "..\Bredy.Libs\archive\ArchiveParamFile.h"

class UndoRedo
{
public:
  const int maxCount;    //number of undo items
  ParamFile **undoBuf;
  int ix;             //current ix (points to empty place)
  int redoIx;         //current redoIx (redo can be done as far as this index)
  int undoCount;      //number of undo values
  UndoRedo(int max);
  ~UndoRedo();
  void SaveUndo(ParamFile *arch);
  ParamFile *Undo();
  ParamFile  *Redo();
  bool RedoUninitialized() {return redoIx==ix;}
  void DecrementRedo()
  {
    if (undoCount>0) redoIx=(redoIx+maxCount-1)%maxCount;
  }
};

#endif
