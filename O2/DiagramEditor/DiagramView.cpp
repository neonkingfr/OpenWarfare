// DiagramView.cpp: implementation of the CDiagramView class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DiagramEditor.h"
#include "DiagramView.h"
#include <malloc.h>
#include <math.h>
#include <float.h>
#include ".\diagramview.h"
#include <el/paramfile/paramFile.hpp>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define DEFWIDTH 20
#define DEFHEIGHT 10

#define DEFBKCOLOR defItemColor

BEGIN_MESSAGE_MAP(CDiagramView, CGraphCtrl)
  //{{AFX_MSG_MAP(CDiagramView
  ON_WM_KILLFOCUS()
  ON_WM_LBUTTONDOWN()
  ON_WM_LBUTTONUP()
  ON_WM_MOUSEMOVE()
  ON_EN_KILLFOCUS(IDC_TEXTINPUT, SaveEditedText)
  ON_WM_CTLCOLOR()
  //}}AFX_MSG_MAP
  ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()


CDiagramView::ItemInfo::ItemInfo()
{
  shape=CDiagramView::stBox;
  lineColor=0;
  memset(&lgFont,0,sizeof(lgFont));
  lgFont.lfHeight=20;
  lgFont.lfCharSet=EASTEUROPE_CHARSET;
  lgFont.lfWeight=400;
  strcpy(lgFont.lfFaceName,"Arial");
  fontColor=0;
  lWidth=1;
  textAlign=DT_CENTER;
  ffHeight=10;
  ffWidth=0;
}

static CDiagramView::ItemInfo defItemInfo;
static COLORREF defItemColor=TRANSPARENTCOLOR;
static COLORREF defLinkColor=RGB(0x60,0x60,0x60);
static SDLinkInfo defLinkFlags;
static bool defLinkUseCustom=false;

#define DEFITEMFLAGS SGRI_CANMOVE|SGRI_CANRESIZE|SGRI_CANSELFLINKED|SGRI_CANBELINKED|SGRI_CANMAKELINKS|SGRI_CUSTOMDRAW

void CDiagramView::ItemInfo::FromDefault()
{
  *this=defItemInfo;
}

void CDiagramView::ItemInfo::SaveAsDefault()
{
  defItemInfo=*this;
}

CDiagramView::CDiagramView()
{
  SFloatRect rect(-300,-300,300,300);
  SetPanZoom(rect);
  SetSmallSize(0,0);
  _newitemmode=false;
  grpcnt=1;
  _grpselected=false;
  _pageClip=false;
  _pageColor=false;
  _pageColorVal=RGB(255,255,255);
  _pageImageX=640;
  _pageImageY=480;
  _curlink=-1;
  _grid=0.0f;
  _gridshow=false;
  _showVariables=false;  
  _exportdraw=false;
  _savingText=false;

}

CDiagramView::~CDiagramView()
{
  ResetContent();
}

void CDiagramView::ResetContent()
{
  for (int id=-1;(id = EnumItems(id)) != -1;) delete GetItemExtraData(id);
  for (int id=-1;(id = EnumLinks(id)) != -1;) delete GetLinkExtraData(id);
  CGraphCtrl::ResetContent();
}

CDiagramView::ItemInfo * CDiagramView::GetItemExtraData(int itemid)
{
  SGraphItem itm;
  GetItem(itemid,&itm);
  return (ItemInfo *)itm.data;
}

SDLinkInfo * CDiagramView::GetLinkExtraData(int itemid)
{
  SGraphLink &lnk=GetLink(itemid);
  return (SDLinkInfo *)lnk.data;
}

void CDiagramView::DeleteItem(int id)
{
  delete GetItemExtraData(id);
  CGraphCtrl::DeleteItem(id);
}

void CDiagramView::DeleteLink(int &enm) 
{
  delete GetLinkExtraData(enm);
  CGraphCtrl::DeleteLink(enm);
}

void CDiagramView::NewItem()
{
  ItemInfo *newinfo=new ItemInfo;
  newinfo->FromDefault();
  InsertItem(DEFITEMFLAGS|SGRI_NOGUESSPOSITION,DEFBKCOLOR,"",DEFWIDTH,DEFHEIGHT,(void *)newinfo);
  Update();
}

bool CDiagramView::OnCustomDraw(int item, SGraphItem &itm, CDC &dc, CRect &rc)
{
  ItemInfo *info=(ItemInfo *)itm.data;
  dc.SetBkMode(TRANSPARENT);
  this->PrepareToDrawingCell(dc,info,itm.color);
  switch (info->shape)
  {
  case stBox: dc.Rectangle(&rc);break;
  case stEllipse: dc.Ellipse(&rc);break;
  case stRoundedBox: dc.RoundRect(&rc,CPoint(rc.Size().cx/2,rc.Size().cy/2));break;
  case stDiamond: 
    {
      POINT pt[4];
      pt[0].x=rc.left;pt[0].y=(rc.top+rc.bottom)>>1;
      pt[1].x=(rc.left+rc.right)>>1;pt[1].y=rc.top;
      pt[2].x=rc.right;pt[2].y=(rc.top+rc.bottom)>>1;
      pt[3].x=(rc.left+rc.right)>>1;pt[3].y=rc.bottom;
      dc.Polygon(pt,4);
    }
    break;
  case stParallelogramR:
    {
      POINT pt[4];
      pt[0].x=rc.left;pt[0].y=rc.bottom;
      pt[1].x=(rc.left*9+rc.right*1)/10;pt[1].y=rc.top;
      pt[2].x=rc.right;pt[2].y=rc.top;
      pt[3].x=(rc.left*1+rc.right*9)/10;pt[3].y=rc.bottom;
      dc.Polygon(pt,4);
    }
    break;
  case stParallelogramL:
    {
      POINT pt[4];
      pt[0].x=rc.left;pt[0].y=rc.top;
      pt[1].x=(rc.left*1+rc.right*9)/10;pt[1].y=rc.top;
      pt[2].x=rc.right;pt[2].y=rc.bottom;;
      pt[3].x=(rc.left*9+rc.right*1)/10;pt[3].y=rc.bottom;
      dc.Polygon(pt,4);
    }
  }
  dc.SetTextColor(info->fontColor);
  const char *textToDraw=info->text;
  char buff[50];
  char buff2[50];
  if (IsExpression(textToDraw))
  {
    if (info->lasterror)
    {
      const char *errtext;
      switch (info->lasterror)
      {
      case CDS_ERR_SYNTAXERROR: errtext="?Syntax";break;
      case CDS_ERR_CANNOTGETVALUE: 
        errtext="?Get %s";
        if (info->lasterrordata>0) IndexToVariable(info->lasterrordata,buff2);
        else IndexToPin(-info->lasterrordata,buff2);
        break;
      case CDS_ERR_CANNOTSETVALUE: 
        errtext="?Set %s";
        if (info->lasterrordata>0) IndexToVariable(info->lasterrordata,buff2);
        else IndexToPin(-info->lasterrordata,buff2);
        break;
      case CDS_ERR_UNKNOWNFUNCTION: errtext="?Funct";break;
      case CDS_ERR_INVALIDCOUNTOPERANDS: 
        errtext="?Oper %s";
        itoa(info->lasterrordata,buff2,10);
        break;
      default: errtext="?Error";
      }
      sprintf(buff,errtext,buff2);
    }
    else
      sprintf(buff,"%g",info->value);
    textToDraw=buff;
  }
  if (textToDraw[0])
    DrawMultipleLines(dc,textToDraw,rc,info->textAlign,info->lgFont.lfOrientation);
  if (_showVariables && !_exportdraw)
  {
    dc.SetBkMode(OPAQUE);
    dc.SetBkColor(RGB(255,255,255));
    dc.SetTextColor(0);
    if (wVarFont.m_hObject==NULL)	  
      wVarFont.CreateFont(20,0,0,0,400,0,0,0,0,0,0,0,0,"Arial");	  
    CFont *old=dc.SelectObject(&wVarFont);
    dc.SetTextAlign(TA_BASELINE|TA_RIGHT);
    char buff[50];
    IndexToVariable(item,buff);
    dc.TextOut(rc.right,rc.bottom,buff,strlen(buff));
    dc.SelectObject(old);
  }
  return true;
}

void CDiagramView::ChangeShape(ShapeType shape)
{
  SaveUndo();
  for (int id=-1;(id = EnumItems(id,true)) != -1;) 
  {
    ItemInfo *nfo=GetItemExtraData(id);
    nfo->shape=shape;
  }
  Update();
  defItemInfo.shape=shape;
}

void CDiagramView::OnLinkItems(int beginitem, int enditem)
{
  SaveUndo();
  InsertLink(beginitem,enditem,defLinkColor,defLinkUseCustom?SGRI_CUSTOMDRAW:0,(void *)new SDLinkInfo(defLinkFlags));
  OrderLinks() ;
}

void CDiagramView::OnChangeLink(int lnk, int beginitem, int enditem, int newitem, void *data)
{
  SaveUndo();
  SGraphLink &linfo=GetLink(lnk);
  linfo.fromitem=beginitem;
  linfo.toitem=newitem;
  OrderLinks();
}

void CDiagramView::OnUnlinkItems(int lnk, int beginitem, int enditem, void *data)
{
  SaveUndo();
  DeleteLink(lnk);
  OrderLinks();
}

void CDiagramView::OnKillFocus(CWnd* pNewWnd)
{

}

void CDiagramView::ChangeLineWidth(int width, int style)
{
  SaveUndo();
  for (int id=-1;(id = EnumItems(id,true)) != -1;) 
  {
    ItemInfo *nfo=GetItemExtraData(id);
    nfo->lWidth=width;
    nfo->lStyle=style;
  }
  Update();
  defItemInfo.lWidth=width;
  defItemInfo.lStyle=style;
}

CDiagramView::ItemInfo * CDiagramView::GetRefItem()
{
  int p=EnumItems(-1,true);
  if (p==-1) return &defItemInfo;
  else return GetItemExtraData(p);  
}

void CDiagramView::ChangeBkColor(COLORREF color)
{
  SaveUndo();
  for (int id=-1;(id = EnumItems(id,true)) != -1;) 
  {
    SGraphItem item;
    GetItem(id,&item);
    item.color=color;
    SetItem(id,&item);
  }
  Update();
  defItemColor=color;
}

COLORREF CDiagramView::GetRefColor()
{
  int p=EnumItems(-1,true);
  if (p==-1) return defItemColor;
  else 
  {
    SGraphItem item;
    GetItem(p,&item);
    return item.color;
  }
}

void CDiagramView::ChangeLineColor(COLORREF color)
{
  SaveUndo();
  for (int id=-1;(id = EnumItems(id,true)) != -1;) 
  {
    ItemInfo *nfo=GetItemExtraData(id);
    nfo->lineColor=color;
  }
  Update();
  defItemInfo.lineColor=color;
}

void CDiagramView::SetTextContentAndStyle(CString text, LOGFONT &lg, COLORREF color, int align)
{
  SaveUndo();
  float h,w;
  GetPixelSize(h,w);
  for (int id=-1;(id = EnumItems(id,true)) != -1;) 
  {
    ItemInfo *nfo=GetItemExtraData(id);
    nfo->text=text;
    nfo->lgFont=lg;
    nfo->fontColor=color;
    nfo->textAlign=align;
    nfo->ffHeight=lg.lfHeight*h;
    nfo->ffWidth=lg.lfWidth*w;
  }
  defItemInfo.lgFont=lg;
  defItemInfo.fontColor=color;
  defItemInfo.textAlign=align;
  defItemInfo.ffHeight=lg.lfHeight/h;
  defItemInfo.ffWidth=lg.lfWidth/w;
  Update();
}

void CDiagramView::DrawMultipleLines(CDC &dc, const char *text, CRect &rc, int align, int angle)
{
  char *p=strcpy((char *)alloca(strlen(text)+1),text);
  char *c=p;
  int linecnt=1,line=0;
  while (*c) if (*c++=='\r') linecnt++;
  do
  {
    c=strchr(p,'\r');
    if (c!=NULL) *c=0;
    DrawSingleLine(dc,p,rc,align,line++,linecnt,angle);
    if (c==NULL) break;
    p=c;
    p+=2; //skip zero and \n to next line
  }
  while (true);
}

static inline float sqr(float x)
{return x*x;}

static inline int ToInt(float p)
{
  int retr;
  __asm
  {    
    fld p
      fistp retr;    
  }
  return retr;
}

void CDiagramView::DrawSingleLine(CDC &dc, const char *line, CRect &rc, int align, int curline, int totallines, int angle)
{
  CSize sz=dc.GetTextExtent("W");
  int halfheight=(sz.cy*totallines)>>1;	  //halfheight = pulka sirky textu (vsech radek)
  int lnpos=sz.cy*curline-halfheight; //poloha radku v pravouhelniku
  int xcent=(rc.right+rc.left)>>1;	  //vypocitej stred zadane oblasti
  int ycent=(rc.top+rc.bottom)>>1;
  float rangle=angle*3.14159265f/1800.0f;	  //vypocitej uhel v radianech
  xcent+=ToInt((float)sin(rangle)*lnpos);			  //posun stred o polohu radky
  ycent+=ToInt((float)cos(rangle)*lnpos);			  
  int iposx,iposy; //nyni spocitej zarovnani a pocatek textu
  if (align==DT_CENTER)
  {
    dc.SetTextAlign(TA_CENTER|TA_TOP);iposx=xcent;iposy=ycent;
  }
  else
  {
    float rectlength=(float)sqrt(sqr((float)(rc.left-rc.right))+sqr(float(rc.top-rc.bottom))); 
    //rectlenght je delka uhlopricky, nejdelsi vzdalenost v oblasti
    float x=(float)cos(rangle)*rectlength;	  //spocitej virtualni zacatek a konec radky
    float y=-(float)sin(rangle)*rectlength;
    if (align==DT_LEFT)
    {
      x=-x;
      y=-y;
      dc.SetTextAlign(TA_LEFT|TA_TOP);
    }
    else
    {
      dc.SetTextAlign(TA_RIGHT|TA_TOP);
    }
    float fl,ft,fr,fb,sl=sz.cy*0.5f;
    fl=(float)(rc.left-xcent+sl)/x;
    ft=(float)(rc.top-ycent+sl)/y;
    fr=(float)(rc.right-xcent-sl)/x;
    fb=(float)(rc.bottom-ycent-sl)/y;
    float ff=1.0f;
    if (!_finite(ff) || _finite(fl) && fl>0 && fl<ff) ff=fl;
    if (!_finite(ff) || _finite(ft) && ft>0 && ft<ff) ff=ft;
    if (!_finite(ff) || _finite(fr) && fr>0 && fr<ff) ff=fr;
    if (!_finite(ff) || _finite(fb) && fb>0 && fb<ff) ff=fb;
    iposx=ToInt(xcent+ff*x);
    iposy=ToInt(ycent+ff*y);
  }
  dc.ExtTextOut(iposx,iposy,ETO_CLIPPED,&rc,line,NULL);
}

void CDiagramView::OnLButtonDown(UINT nFlags, CPoint point)
{
  if (wTextEdit.GetSafeHwnd()!=NULL) 
  {
    SaveEditedText();
    float x,y;
    MapPointFromWindow(point,&x,&y);
    int item=ItemFromPoint(x,y);
    if (item!=-1)
    {
      Deselect();
      SelectItem(item,true);
      BeginEditText();
      return;
    }
  }

  if (!_newitemmode) CGraphCtrl::OnLButtonDown(nFlags,point);
  else
  {
    dragrc.left=point.x;
    dragrc.top=point.y;
    dragrc.right=point.x;
    dragrc.bottom=point.y;
    SetCapture();	
  }
}

void CDiagramView::OnLButtonUp(UINT nFlags, CPoint point)
{
  if (!_newitemmode) CGraphCtrl::OnLButtonUp(nFlags,point);
  else
  {
    dragrc.NormalizeRect();
    if (dragrc.Size().cx>2 || dragrc.Size().cy>2) 
    {
      SaveUndo();
      SFloatRect rc=MapRectFromWindow(dragrc);
      ItemInfo *newinfo=new ItemInfo;
      newinfo->FromDefault();
      int item=InsertItem(DEFITEMFLAGS,DEFBKCOLOR,"",DEFWIDTH,DEFHEIGHT,(void *)newinfo);
      AlignToGrid(rc);
      SetItemRect(item,rc);
      AfxGetMainWnd()->PostMessage(MSG_NEWITEM,item);
      Update();
    }
    ReleaseCapture();
  }
}

void CDiagramView::OnMouseMove(UINT nFlags, CPoint point)
{
  if (!_newitemmode) CGraphCtrl::OnMouseMove(nFlags,point);
  else
  {
    if (GetCapture()==this)
    {
      dragrc.right=point.x;
      dragrc.bottom=point.y;
      Invalidate(FALSE);
      UpdateWindow();
      CDC *dc=GetDC();
      CPen pen(PS_DASHDOT,1,GetSysColor(COLOR_WINDOWTEXT));
      CPen *old=dc->SelectObject(&pen);
      dc->SetBkMode(TRANSPARENT);
      dc->SelectStockObject(HOLLOW_BRUSH);
      dc->Rectangle(&dragrc);
      dc->SelectObject(old);
      ReleaseDC(dc);
    }
  }
}

int CDiagramView::Mode(int mode)
{
  if (mode==SGRM_NEWITEM)
  {
    _newitemmode=true;
    return CGraphCtrl::Mode(SGRM_GETCURRENT);
  }
  else
    if (_newitemmode) 
    {	  
      if (mode!=SGRM_GETCURRENT) 
      {
        _newitemmode=false;
        CGraphCtrl::Mode(mode);
      }
      return SGRM_NEWITEM;
    }
    else return CGraphCtrl::Mode(mode);
}

void CDiagramView::DeleteSelected()
{
  SaveUndo();
  for (int id=-1;(id = EnumItems(-1,true)) != -1;) 
  {
    DeleteItem(id);
  }
  Update();
}

void CDiagramView::DuplicateSelected()
{
  struct IdMap {
    IdMap() {}
    int oldId;
    int newId;
  };
  SaveUndo();
  int cnt=0;
  int *field;
  int id;
  for (id=-1;(id = EnumItems(id,true)) != -1;) cnt++;
  field=(int *)alloca(sizeof(int)*cnt);
  IdMap *idMap = new IdMap[cnt];
  cnt=0;
  for (id=-1;(id = EnumItems(id,true)) != -1;) 
  {field[cnt++]=id;SelectItem(id,false);}
  for (id=0;id<cnt;id++) //duplicate items
  {
    ItemInfo *newinfo=new ItemInfo;
    *newinfo=*GetItemExtraData(field[id]);
    int item=InsertItem(SGRI_NOGUESSPOSITION,DEFBKCOLOR,"",DEFWIDTH,DEFHEIGHT,(void *)newinfo);
    idMap[id].oldId = field[id]; idMap[id].newId = item;
    SGraphItem itm;
    GetItem(field[id],&itm);
    float szy=itm.rect.bottom-itm.rect.top;
    itm.rect.bottom+=szy*1.5f;
    itm.rect.top+=szy*1.5f;
    itm.data=newinfo;
    SetItem(item,&itm);
    SelectItem(item);
  }
  //duplicate links
  for (id=-1;(id=EnumLinks(id))!=-1;)
  {
    SGraphLink &lnk=GetLink(id);
    //test whether link is between duplicated items
    int _newFrom = -1, _newTo = -1;
    for (int i=0; i<cnt; i++)
    {
      if (lnk.fromitem==idMap[i].oldId) { _newFrom = idMap[i].newId; }
      if (lnk.toitem==idMap[i].oldId) { _newTo = idMap[i].newId; }
    }
    if ((_newTo!=-1) && (_newFrom!=-1))
    {
      SDLinkInfo *newLinkInfo = NULL;
      if (lnk.data) newLinkInfo = new SDLinkInfo(*((SDLinkInfo*)(lnk.data)));
      InsertLink(_newFrom,_newTo,lnk.color,lnk.flags, (void *)newLinkInfo);
    }
  }
  OrderLinks() ;
  Update();
}

CPoint GetMousePosition(CWnd *wnd)
{
  POINT pt;
  if (GetCursorPos(&pt)) {
    wnd->ScreenToClient(&pt);
    CRect rect; wnd->GetClientRect(&rect);
    if (pt.x>=rect.left && pt.x<=rect.right &&pt.y>=rect.top && pt.y<=rect.bottom)
      return CPoint(pt.x, pt.y);
    else
      return CPoint(0,0);
  }
  return CPoint(0,0);
}

#include <el/QStream/QStream.hpp>
#include <es/Strings/rString.hpp>
QOStrStream *CDiagramView::GetDataForClipboard()
{
  struct IdMap {
    IdMap() {}
    int oldId;
    int newId;
  };
  QOStrStream *pclipstr = new QOStrStream;
  QOStrStream &clipstr = *pclipstr;
  CPoint mousePos = GetMousePosition(this);
  float mx,my; MapPointFromWindow(mousePos, &mx, &my);
  clipstr.write(&mx, sizeof(mx)); clipstr.write(&my, sizeof(my));
  int cnt=0;
  int *field;
  int id;
  for (id=-1;(id = EnumItems(id,true)) != -1;) cnt++;
  field=(int *)alloca(sizeof(int)*cnt);
  IdMap *idMap = new IdMap[cnt];
  cnt=0;
  for (id=-1;(id = EnumItems(id,true)) != -1;) field[cnt++]=id;
  //serialize items
  clipstr.write(&cnt, sizeof(cnt));
  for (id=0;id<cnt;id++)
  {
    SGraphItem itm;
    GetItem(field[id],&itm);
    clipstr.write(&itm, sizeof(itm));
    CString *itmdata = (CString*)itm.data;
    clipstr.write(itmdata+1, sizeof(ItemInfo)-sizeof(CString));
    ItemInfo *itminfo=(ItemInfo*)itm.data;
    int txtsize = itminfo->text.GetLength()+1;
    clipstr.write(&txtsize, sizeof(int));
    clipstr.write(itminfo->text.GetBuffer(txtsize),txtsize);
    idMap[id].oldId = field[id]; idMap[id].newId = id;
  }
  //serialize links
  int linkcnt=0;
  for (id=-1;(id=EnumLinks(id))!=-1;)
  {
    SGraphLink &lnk=GetLink(id);
    //test whether link is between duplicated items
    int _newFrom = -1, _newTo = -1;
    for (int i=0; i<cnt; i++)
    {
      if (lnk.fromitem==idMap[i].oldId) { _newFrom = idMap[i].newId; }
      if (lnk.toitem==idMap[i].oldId) { _newTo = idMap[i].newId; }
    }
    if ((_newTo!=-1) && (_newFrom!=-1)) linkcnt++;
  }
  clipstr.write(&linkcnt, sizeof(linkcnt));
  for (id=-1;(id=EnumLinks(id))!=-1;)
  {
    SGraphLink lnk=GetLink(id);
    //test whether link is between duplicated items
    int _newFrom = -1, _newTo = -1;
    for (int i=0; i<cnt; i++)
    {
      if (lnk.fromitem==idMap[i].oldId) { _newFrom = idMap[i].newId; }
      if (lnk.toitem==idMap[i].oldId) { _newTo = idMap[i].newId; }
    }
    if ((_newTo!=-1) && (_newFrom!=-1))
    {
      lnk.fromitem=_newFrom; lnk.toitem=_newTo;
      clipstr.write(&lnk, sizeof(SGraphLink));
      char writeData= (lnk.data!=NULL);
      clipstr.write(&writeData, sizeof(char));
      if (writeData) clipstr.write(lnk.data, sizeof(SDLinkInfo));
    }
  }
  delete idMap;
  return pclipstr;
}

bool CDiagramView::TestMouseOffset(int cnt, int *mapId, float curmousex, float curmousey)
{
  bool pasteInsideWindow = false;
  CRect rect; GetClientRect(&rect);
  SFloatRect minmax(1e10f, 1e10f, -1e10f, -1e10f);
  for (int i=0; i<cnt; i++)
  {
    SGraphItem itm; GetItem(mapId[i], &itm);
    if (itm.rect.top<minmax.top) minmax.top=itm.rect.top;
    if (itm.rect.left<minmax.left) minmax.left=itm.rect.left;
    if (itm.rect.bottom>minmax.bottom) minmax.bottom=itm.rect.bottom;
    if (itm.rect.right>minmax.right) minmax.right=itm.rect.right;
    float bottom = itm.rect.bottom - _mouseHotSpotY + curmousey;
    float left = itm.rect.left - _mouseHotSpotX + curmousex;
    float top = itm.rect.top - _mouseHotSpotY + curmousey;
    float right = itm.rect.right - _mouseHotSpotX + curmousex;
    CPoint winpt = MapPointToWindow(left, bottom);
    if (winpt.x>=rect.left && winpt.x<=rect.right && winpt.y>=rect.top && winpt.y<=rect.bottom) pasteInsideWindow=true;
    winpt = MapPointToWindow(right, top);
    if (winpt.x>=rect.left && winpt.x<=rect.right && winpt.y>=rect.top && winpt.y<=rect.bottom) pasteInsideWindow=true;
  }
  //test whether hotspot is close to selection
  //float centerx = (minmax.right+minmax.left)/2.0f; float centery = (minmax.bottom+minmax.bottom)/2.0f;
  CPoint pt(0,0); float topleftx, toplefty, bottomrightx, bottomrighty;
  MapPointFromWindow(pt, &topleftx, &toplefty);
  pt = CPoint(rect.right, rect.bottom);
  MapPointFromWindow(pt, &bottomrightx, &bottomrighty);
  float sizx = bottomrightx - topleftx;
  float sizy = bottomrighty - toplefty;
  minmax.left -= sizx/3; minmax.right += sizx/3;
  minmax.top -= sizy/3; minmax.bottom += sizy/3;
  if (_mouseHotSpotX<minmax.left || _mouseHotSpotX>minmax.right || _mouseHotSpotY<minmax.top || _mouseHotSpotY>minmax.bottom)
    return false;
  return pasteInsideWindow;
}

void CDiagramView::PasteFromClipboard(QIStrStream &in)
{
  int size;
  in.read(&size, sizeof(int));
  in.read(&_mouseHotSpotX, sizeof(_mouseHotSpotX)); in.read(&_mouseHotSpotY, sizeof(_mouseHotSpotY));
  int cnt;
  in.read(&cnt, sizeof(int));
  int *mapId = new int[cnt];  
  float mintop =1e10f;
  float minleft=1e10f;
  for (int i=0; i<cnt; i++)
  {
    SGraphItem itm;
    in.read(&itm,sizeof(SGraphItem));
    CString *newinfo=(CString *)new ItemInfo;
    in.read(newinfo+1,sizeof(ItemInfo)-sizeof(CString));
    int txtsize;
    in.read(&txtsize, sizeof(int));
    ItemInfo *itinfo = (ItemInfo*)newinfo;
    char *txt=new char[txtsize];
    in.read(txt,txtsize);
    itinfo->text.SetString(txt,txtsize);
    delete txt;
    int item=InsertItem(SGRI_NOGUESSPOSITION,DEFBKCOLOR,"",DEFWIDTH,DEFHEIGHT,(void *)newinfo);
    mapId[i]=item;
    float szy=itm.rect.bottom-itm.rect.top;
    if (itm.rect.top<mintop) mintop=itm.rect.top;
    if (itm.rect.left<minleft) minleft=itm.rect.left;
    itm.data=newinfo;
    SetItem(item,&itm);
    SelectItem(item);
  }
  float curmousex, curmousey;
  CPoint pt(0,0); pt = pt+GetMousePosition(this);
  MapPointFromWindow(pt, &curmousex, &curmousey);
  bool mouseOffsetIsValid = ((pt.x==0 && pt.y==0) ? false : TestMouseOffset(cnt, mapId, curmousex, curmousey));
  for (int i=0; i<cnt; i++)
  {
    SGraphItem itm; GetItem(mapId[i], &itm);
    if (!mouseOffsetIsValid)
    {//paste to current mouse position
      itm.rect.bottom = itm.rect.bottom - mintop + curmousey;
      itm.rect.top = itm.rect.top - mintop + curmousey;
      itm.rect.left = itm.rect.left - minleft + curmousex;
      itm.rect.right = itm.rect.right - minleft + curmousex;
    }
    else
    {//paste using "mouse offset"
      itm.rect.bottom = itm.rect.bottom - _mouseHotSpotY + curmousey;
      itm.rect.top = itm.rect.top - _mouseHotSpotY + curmousey;
      itm.rect.left = itm.rect.left - _mouseHotSpotX + curmousex;
      itm.rect.right = itm.rect.right - _mouseHotSpotX + curmousex;
    }
    SetItem(mapId[i],&itm);
  }
  in.read(&cnt, sizeof(int));
  for (int i=0; i<cnt; i++)
  {
    SGraphLink lnk; in.read(&lnk,sizeof(SGraphLink));
    char readData;
    in.read(&readData, sizeof(char));
    SDLinkInfo *info = NULL;
    if (readData) {    
      info = new SDLinkInfo; in.read(info, sizeof(SDLinkInfo));
    }
    InsertLink(mapId[lnk.fromitem], mapId[lnk.toitem], lnk.color, lnk.flags, info);
  }
  delete mapId;
  OrderLinks() ;
  Update();
}

void CDiagramView::GroupSelect(bool group)
{
  SaveUndo();
  int grp;
  if (group) grp=grpcnt++;else grp=0;
  for (int id=-1;(id = EnumItems(id,true)) != -1;) 
    GetItemExtraData(id)->group=grp;
  SelectGroup();
}

void CDiagramView::SelectGroup()
{
  int curgrp=0;
  _grpselected=false;
  while (true)
  {
    int nextgrp=grpcnt;  
    for (int id=-1;(id = EnumItems(id,false)) != -1;) 
    {
      ItemInfo *nfo=GetItemExtraData(id);
      if (ItemSelected(id))
        if (nfo->group<nextgrp && nfo->group>curgrp) nextgrp=nfo->group;
      if (curgrp && nfo->group==curgrp) 
      {
        SelectItem(id);
        _grpselected=true;
      }
    }
    if (nextgrp==grpcnt) break;
    curgrp=nextgrp;
  }
}

void CDiagramView::OnEndSelection()
{
  SetFocus();
  SelectGroup();
  AfxGetMainWnd()->PostMessage(MSG_NOTIFYENDSEL,0,0);
}

int CDiagramView::OrderItem(int item, int order)
{  
  int i,l;
  int cnt=0;
  for (i=-1;(i=EnumItems(i))!=-1;) cnt=i+1;
  int *map=(int *)alloca(sizeof(int)*cnt);
  for (i=0;i<cnt;i++) map[i]=i;
  if (order==-1)
  {
    SGraphItem save;
    SGraphItem move;	
    GetItem(item,&save);	
    i=-1;
    l=item;
    while (i!=item)
    {
      move=save;
      i=EnumItems(i);
      map[l]=i;
      GetItem(i,&save);
      SetItem(i,&move);
      l=i;
    }
  }
  else
  {
    SGraphItem save;
    GetItem(item,&save);	
    SGraphItem move;
    i=l=item;
    while ((i=EnumItems(i))!=-1)
    {
      GetItem(i,&move);
      SetItem(l,&move);
      map[i]=l;
      l=i;
    }
    SetItem(l,&save);
    map[item]=l;
  }
  for (i=-1;(i=EnumLinks(i))!=-1;)
  {
    SGraphLink &lnk=GetLink(i);
    lnk.fromitem=map[lnk.fromitem];
    lnk.toitem=map[lnk.toitem];
  }
  return map[item];
}

void CDiagramView::OrderSelected(int order)
{
  SaveUndo();
  int i;
  int cnt=0;
  if (order>0)
  {
    for (i=-1;(i=EnumItems(i))!=-1;) cnt=i+1;
    for (i=0;(i=EnumItems(i-1,true))!=-1 && i<cnt;)
    {
      OrderItem(i,order);cnt--;
    }
  }
  else
    for (i=0;(i=EnumItems(i,true))!=-1;)
      OrderItem(i,order);cnt--;
  Update();
}

void CDiagramView::PrepareToDrawingCell(CDC &dc, ItemInfo *info, COLORREF bgcolor)
{
  // PEN OPTIMALIZATION
  {
    if (info->lineColor==TRANSPARENTCOLOR)
    {
      if (GetStockObject(NULL_PEN)!=::GetCurrentObject(dc,OBJ_PEN)) 
      {
        HPEN old=(HPEN)::SelectObject(dc,GetStockObject(NULL_PEN));
        if (old==_oldpen) DeleteObject(_oldpen);
        _oldpen=NULL;
      }
    }
    else
    {
      CPen *pen=dc.GetCurrentPen();
      LOGPEN lgpen;
      pen->GetLogPen(&lgpen);    
      if (lgpen.lopnColor!=info->lineColor || 
        lgpen.lopnStyle!=info->lStyle ||
        lgpen.lopnWidth.x!=info->lWidth)
      {
        HPEN hpen=CreatePen(info->lStyle,info->lWidth,info->lineColor);
        HPEN old=(HPEN)::SelectObject(dc,hpen);
        if (old==_oldpen) DeleteObject(_oldpen);
        _oldpen=hpen;
      }
      CPen::DeleteTempMap();
    }
  }
  // BRUSH OPTIMALIZATION
  {
    if (bgcolor==TRANSPARENTCOLOR)
    {
      if (GetStockObject(HOLLOW_BRUSH)!=::GetCurrentObject(dc,OBJ_BRUSH)) 
      {
        HBRUSH old=(HBRUSH)::SelectObject(dc,GetStockObject(HOLLOW_BRUSH));
        if (old==_oldbrush) DeleteObject(_oldbrush);
        _oldbrush=NULL;
      }
    }
    else
    {
      CBrush *curbrush=dc.GetCurrentBrush();
      LOGBRUSH lgbrush;
      curbrush->GetLogBrush(&lgbrush);
      if (lgbrush.lbColor!=bgcolor || lgbrush.lbStyle!=BS_SOLID)
      {
        lgbrush.lbColor=bgcolor;
        lgbrush.lbStyle=BS_SOLID;        
        HBRUSH hbrush=CreateBrushIndirect(&lgbrush);
        HBRUSH old=(HBRUSH)::SelectObject(dc,hbrush);
        if (old==_oldbrush) DeleteObject(_oldbrush);
        _oldbrush=hbrush;
      }
      CBrush::DeleteTempMap();
    }
  }
  // FONT OPTIMALIZATION
  {
    float h,w;
    GetPixelSize(h,w);
    info->lgFont.lfHeight=ToInt(info->ffHeight/h);
    info->lgFont.lfWidth=ToInt(info->ffWidth/w);

    CFont *curfont=dc.GetCurrentFont();
    LOGFONT lgfont;
    curfont->GetLogFont(&lgfont);    
    for (int i=strchr(lgfont.lfFaceName,0)-lgfont.lfFaceName+1;i<sizeof(lgfont.lfFaceName);i++)
      lgfont.lfFaceName[i]=info->lgFont.lfFaceName[i];
    if (memcmp(&lgfont,&info->lgFont,sizeof(LOGFONT)))
    {
      HFONT hfont=CreateFontIndirect(&info->lgFont);
      HFONT old=(HFONT)::SelectObject(dc,hfont);
      if (old==_oldfont) DeleteObject(_oldfont);
      _oldfont=hfont;
    }
    CFont::DeleteTempMap();
  }
}

void CDiagramView::Draw(CDC &dc, const CRect &cliprc)
{
  _oldfont=NULL;
  _oldpen=NULL;
  _oldbrush=NULL;
  HPEN pen=(HPEN)GetCurrentObject(dc,OBJ_PEN);
  HBRUSH brush=(HBRUSH)GetCurrentObject(dc,OBJ_BRUSH);
  HFONT font=(HFONT)GetCurrentObject(dc,OBJ_FONT);
  if (_pageColor)
  {
    CRect rc=GetClipRect();    
    dc.FillSolidRect(&rc,_pageColorVal);
  }
  if (_pageClip && !_exportdraw)
  {
    CRect rc=GetClipRect();
    rc+=CRect(1,1,1,1);
    CPen pen;
    pen.CreatePen(PS_DOT,1,(COLORREF)0);
    CPen *old=dc.SelectObject(&pen);
    dc.SelectStockObject(HOLLOW_BRUSH);
    dc.Rectangle(&rc);
    dc.SelectObject(old);
  }
  if (_grid!=0.0 && this->_gridshow && !_exportdraw)
  {
    SFloatRect clprc=this->MapRectFromWindow(cliprc);
    AlignToGrid(clprc);
    if ((clprc.right-clprc.left)/_grid<(float)cliprc.Size().cx*0.3f &&
      (clprc.bottom-clprc.top)/_grid<(float)cliprc.Size().cy*0.3f)

      for (float fx=clprc.left;fx<clprc.right;fx+=_grid)
        for (float fy=clprc.top;fy<clprc.bottom;fy+=_grid)
        {
          CPoint pt=MapPointToWindow(fx,fy);
          dc.InvertRect(CRect(pt.x,pt.y,pt.x+1,pt.y+1));
        }
  }
  CGraphCtrl::Draw(dc,cliprc);
  SelectObject(dc,pen);
  SelectObject(dc,brush);
  SelectObject(dc,font);
  if (_oldfont) DeleteObject(_oldfont);
  if (_oldbrush) DeleteObject (_oldbrush);
  if (_oldpen) DeleteObject(_oldpen);
  if (this->_showVariables && !_exportdraw)
  {
    dc.SetBkMode(OPAQUE);
    dc.SetBkColor(RGB(255,255,255));
    dc.SetTextColor(0);
    CFont *old=dc.SelectObject(&wVarFont);
    dc.SetTextAlign(TA_BASELINE|TA_CENTER);
    char buff[50];
    for (int i=-1;(i=EnumLinks(i))!=-1;)
    {
      SGraphLink &nfo=GetLink(i);      
      int index=GetLinkExtraData(i)==NULL?index=0:GetLinkExtraData(i)->pinid;
      if (index>=CDS_PIN_INPUT_MIN)
      {
        CRect rc;
        CalcLinkRect(nfo.fromitem,nfo.toitem,nfo.order,rc);
        IndexToPin(index,buff);
        dc.TextOut((rc.right+rc.left)>>1,(rc.top+rc.bottom)>>1,buff,strlen(buff));
      }
    }
    dc.SelectObject(old);
  }
}

CRect CDiagramView::GetClipRect(void)
{
  if (_pageClip)
  {
    CRect client;
    GetClientRect(&client);
    CRect clip(0,0,_pageImageX,_pageImageY);
    int diffx=(int)(client.Size().cx-_pageImageX)/2;
    int diffy=(int)(client.Size().cy-_pageImageY)/2;
    clip.left+=diffx;
    clip.top+=diffy;
    clip.right+=diffx;
    clip.bottom+=diffy;
    return clip;
  }
  else
  {
    CRect clip;
    GetClientRect(&clip);
    return clip;
  }  
}

void CDiagramView::SetLinkColor(int link, COLORREF color)
{
  SaveUndo();
  if (link!=-1) GetLink(link).color=color;
  defLinkColor=color;
}

void CDiagramView::SetLinkToLastColor(int link)
{
  SaveUndo();
  SetLinkColor(link,defLinkColor);  
  SetArrowType(link,defLinkUseCustom?new SDLinkInfo(defLinkFlags):0);
}

void CDiagramView::OnEndMoveResize(int corner)
{  
  if (_grid!=0.0f)
    for (int i=-1;(i=EnumItems(i,true))!=-1;)
    {
      SFloatRect rc=GetItemRect(i);
      AlignToGrid(rc);
      SetItemRect(i,rc);
    }
    Update();
}

void CDiagramView::AlignToGrid(SFloatRect & rc)
{
  if (_grid!=0.0f)
  {
    rc.left=floor(rc.left/_grid+0.5f)*_grid;
    rc.top=floor(rc.top/_grid+0.5f)*_grid;
    rc.right=floor(rc.right/_grid+0.5f)*_grid;
    rc.bottom=floor(rc.bottom/_grid+0.5f)*_grid;
    if (rc.left==rc.right) rc.left-=_grid;
    if (rc.bottom==rc.top) rc.top-=_grid;
  };
}

void CDiagramView::ChangeTextFontChanges(LOGFONT & srcfont, LOGFONT & trgfont, COLORREF srccolor , COLORREF trgcolor, int trgalign)
{
  SaveUndo();
  for (int i=-1;(i=EnumItems(i,true))!=-1;)
  {
    float h,w;
    GetPixelSize(h,w);
    ItemInfo *info=GetItemExtraData(i);
    if (srcfont.lfEscapement!=trgfont.lfEscapement)
      info->lgFont.lfEscapement=trgfont.lfEscapement;
    if (stricmp(srcfont.lfFaceName,trgfont.lfFaceName))
      strcpy(info->lgFont.lfFaceName,trgfont.lfFaceName);
    if (srcfont.lfHeight!=trgfont.lfHeight)
    {
      info->lgFont.lfHeight=trgfont.lfHeight;
      info->ffHeight=trgfont.lfHeight*h;
    }
    if (srcfont.lfItalic!=trgfont.lfItalic)
      info->lgFont.lfItalic=trgfont.lfItalic;
    if (srcfont.lfOrientation!=trgfont.lfOrientation)
      info->lgFont.lfOrientation=trgfont.lfOrientation;
    if (srcfont.lfStrikeOut!=trgfont.lfStrikeOut)
      info->lgFont.lfStrikeOut=trgfont.lfStrikeOut;
    if (srcfont.lfUnderline!=trgfont.lfUnderline)
      info->lgFont.lfUnderline=trgfont.lfUnderline;
    if (srcfont.lfWeight!=trgfont.lfWeight)
      info->lgFont.lfWeight=trgfont.lfWeight;
    if (srcfont.lfWidth!=trgfont.lfWidth)
    {
      info->lgFont.lfWidth=trgfont.lfWidth;
      info->ffWidth=trgfont.lfWidth*w;
    }
    if (srccolor!=trgcolor)
      info->fontColor=trgcolor;
    if (trgalign!=-1)
      info->textAlign=trgalign;

  }  
}

bool CDiagramView::Save(const char * filename)
{
  ParamFile paramfile;
  ArchiveParamFile arch(&paramfile,true);
  Streaming(arch);  
  bool res=paramfile.Save(filename)==0;
  if (res) _dirty=false;
  return res;
}


bool CDiagramView::Load(const char * filename)
{
  ParamFile paramfile;
  if (paramfile.Parse(filename)) return false;
  ArchiveParamFile arch(&paramfile,false);
  ResetContent();
  Streaming(arch);
  return !(!arch);
}


void CDiagramView::SaveUndo(void)
{
  AfxGetMainWnd()->SendMessage(MSG_SAVEUNDO,0,0);
  _dirty=true;
}

void CDiagramView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
  AfxGetMainWnd()->SendMessage(MSG_NOTIFYDBLCLICK,0,0);
}

void CDiagramView::OnLinkNewItem(int beginitem, float x, float y)
{
  SaveUndo();
  SGraphItem item;
  GetItem(beginitem,&item);
  float xs2=(item.rect.right-item.rect.left)*0.5f;
  float ys2=(item.rect.bottom-item.rect.top)*0.5f;
  SFloatRect newrc(x-xs2,y-ys2,x+xs2,y+ys2);
  ItemInfo *newinfo=new ItemInfo(*GetItemExtraData(beginitem));
  int itemp=InsertItem(DEFITEMFLAGS|SGRI_NOGUESSPOSITION,item.color,"",DEFWIDTH,DEFHEIGHT,(void *)newinfo);
  SetItemRect(itemp,newrc);
  InsertLink(beginitem,itemp,defLinkColor,defLinkUseCustom?SGRI_CUSTOMDRAW:0,(void *)new SDLinkInfo(defLinkFlags));
  OrderLinks();  
}

void CDiagramView::GetPixelSize(float & x, float & y)
{
  CRect rc(0,0,1,1);
  SFloatRect frc=MapRectFromWindow(rc);
  x=frc.right-frc.left;
  y=frc.bottom-frc.top;
}

static void ExchangeStringArray(IArchive &arch,const char *name, char *text, int len)
{
  if (+arch)
  {
    char *c=text;
    arch.RValue(name,c);
  }
  else
  {
    char *c=NULL;
    arch(name,c);
    if (c!=NULL) 
    {
      strncpy(text,c,len);
      text[len]=0;
    }
    else
      text[0]=0;
    free(c);
  }

}

static void ArchiveVariableExchange(IArchive &arch,SGraphItem &data)
{
  CDiagramView::ItemInfo *nfo=(CDiagramView::ItemInfo *)(data.data);
  if (-arch)
  {
    nfo=new CDiagramView::ItemInfo;
    data.data=(void *)nfo;
    memset(&nfo->lgFont,0,sizeof(nfo->lgFont));
  }
  arch("Text",nfo->text);
  arch("Left",data.rect.left);
  arch("Top",data.rect.top);
  arch("Right",data.rect.right);
  arch("Bottom",data.rect.bottom);
  arch("BgColor",data.color,(COLORREF)TRANSPARENTCOLOR);
  arch("Align",nfo->textAlign,DT_CENTER);
  arch("lWidth",nfo->lWidth,1);
  arch("lStyle",nfo->lStyle,PS_SOLID);
  arch("lColor",nfo->lineColor,(COLORREF)0);
  arch("Shape",nfo->shape,CDiagramView::stBox);
  arch("Group",nfo->group,0);
  ExchangeStringArray(arch,"FontFace",nfo->lgFont.lfFaceName,sizeof(nfo->lgFont.lfFaceName));
  arch("FontColor",nfo->fontColor,(COLORREF)0);
  arch("FontHeight",nfo->ffHeight);
  arch("FontWidth",nfo->ffWidth,0.0f);
  arch("FontAngle",nfo->lgFont.lfOrientation,(LONG)0);
  arch("FontWeight",nfo->lgFont.lfWeight,(LONG)400);
  arch("FontItalic",nfo->lgFont.lfItalic,(BYTE)0);
  arch("FontUnderline",nfo->lgFont.lfUnderline,(BYTE)0);
  arch("FontStickeOut",nfo->lgFont.lfStrikeOut,(BYTE)0);
  arch("FontCharSet",nfo->lgFont.lfCharSet,(BYTE)EASTEUROPE_CHARSET);
  if (arch.RValue("Inactive",(data.flags & LINKACTIVE)!=LINKACTIVE,false))
    data.flags&=~LINKACTIVE;
  else
    data.flags|=LINKACTIVE;
   
  if (-arch)
  {
    nfo->lgFont.lfEscapement=nfo->lgFont.lfOrientation;
  }
}

static void ArchiveVariableExchange(IArchive &arch,CString &data)
{
  if (-arch)
  {
    char *z=NULL;
    arch(z);
    data=z;
    delete [] z;
  }
  else
  {
    char *z=data.LockBuffer();
    arch.RValue(z);
    data.UnlockBuffer();
  }
}

static void ArchiveVariableExchange(IArchive &arch,CDiagramView::ShapeType &shape)
{
  shape=(CDiagramView::ShapeType)(arch.RValue((int)shape));
}

static void ArchiveVariableExchange(IArchive &arch,SDLinkInfo &linfo)
{
  linfo.Serialize(arch);
}

static void ArchiveVariableExchange(IArchive &arch,SGraphLink &lnk)
{
  SDLinkInfo def,cur;
  memset(&def,0,sizeof(def));

  arch("From",lnk.fromitem);
  arch("To",lnk.toitem);
  arch("Color",lnk.color);
  if (+arch)
  {    
    if (lnk.data) arch("Extra",*(SDLinkInfo *)lnk.data);    
  }
  else
  {
    if (arch("Extra",cur)==true) lnk.data=new SDLinkInfo(cur);
    else lnk.data=NULL;
  }

/// Compatible section
  if (-arch)
  {
    short flags;
    if (arch("Flags",flags))
    {
      DWORD data=arch.RValue("Data",0);
      SDLinkInfo *nfo=(SDLinkInfo *)lnk.data;
      if (nfo==NULL) lnk.data=nfo=new SDLinkInfo;
      nfo->Convert(*(SDLinkFlags1 *)(&flags),data);
    }
  }
/// End of compatibility
  if (-arch)
  {
    if (lnk.data) lnk.flags=SGRI_CUSTOMDRAW;
    else lnk.flags=0;
  }
}


void CDiagramView::Streaming(IArchive & arch, bool serializeWindow)
{  
  int cnt=0;
  int i,j;
  int *map;
  /*  char test[]="Pokusn� text";
  arch.Binary("BinaryTest",test,sizeof(test));*/
  arch.SetError(0);
  if (arch.IsStoring())
  {    
    ArchiveSection asect(arch);    
    for (i=-1;(i=EnumItems(i))!=-1;) cnt=i;
    map=(int *)alloca(sizeof(int)*(cnt+1));    
    for (i=-1,j=0;(i=EnumItems(i))!=-1;j++) map[i]=j;
    {
      while (asect.Block("GraphItems"))
        for (i=-1,j=0;(i=EnumItems(i))!=-1;j++)
        {
          arch.vSectionData=i;
          SGraphItem gitem;
          GetItem(i,&gitem);
          arch("Item",gitem);
        }
    }
    {
      while (asect.Block("GraphLinks"))
        for (i=-1,j=0;(i=EnumLinks(i))!=-1;j++)
        {
          SGraphLink lnk=GetLink(i);
          arch.vSectionData=j;
          lnk.fromitem=map[lnk.fromitem];
          lnk.toitem=map[lnk.toitem];
          arch("Link",lnk);
        }
    }
  }
  else
  {
    char buff[50];
    ArchiveSection asect(arch);    
    {
      i=0;
      while (asect.Block("GraphItems"))
        while (true)
        {        
          SGraphItem gitem;
          gitem.flags=DEFITEMFLAGS;
          if (arch("Item",gitem)==false) 
          {
            sprintf(buff,"Item%04d",i);
            if (arch(buff,gitem)==false) break;
          }
          int ipos=InsertItem(SGRI_NOGUESSPOSITION,0,"",1,1,NULL);
          SetItem(ipos,&gitem);
          i++;
        }
    }
    {
      i=0;
      while (asect.Block("GraphLinks"))      
        while (true)
        {
          SGraphLink lnk;
          arch.vSectionData=i;
          if (arch("Link",lnk)==false)
          {
            sprintf(buff,"Link%04d",i);
            if (arch(buff,lnk)==false) break;
          }
          InsertLink(lnk.fromitem,lnk.toitem,lnk.color,lnk.flags,lnk.data);
          i++;
        }
        OrderLinks();
    }
  }
  {
    ArchiveSection asect(arch);    
    while (asect.Block("Globals"))      
    {
      arch("Grid",_grid,0.0f);
      arch("GridShow",_gridshow,false);
      arch("PageClip",_pageClip,false);
      arch("PageColor",_pageColor,false);
      arch("PageColorVal",_pageColorVal,(COLORREF)0);
      arch("PageImageX",_pageImageX,(UINT)640);
      arch("PageImageY",_pageImageY,(UINT)480);	  
      arch("NextGroupID",grpcnt,(int)0);	  
    }
    while (asect.Block("Window"))
    {
      if (serializeWindow) AfxGetMainWnd()->SendMessage(MSG_SERIALIZEWINDOW,0,(LPARAM)&arch);
    }
  }
}

void CDiagramView::ChangeFontFaceSize(const char *facename, int height,int widthper, int angle)
{
  SaveUndo();
  for (int i=-1;(i=EnumItems(i,true))!=-1;)
  {
    ItemInfo *nfo=GetItemExtraData(i);
    int width=ToInt(nfo->ffWidth/nfo->ffHeight*100.0f);
    if (facename!=NULL) strncpy(nfo->lgFont.lfFaceName,facename,sizeof(nfo->lgFont.lfFaceName));
    if (widthper!=-1) 
    {
      width=widthper;
      nfo->ffWidth=width*nfo->ffHeight*0.01f;
    }
    if (height!=-1) 
    {
      nfo->ffHeight=height;
      nfo->ffWidth=width*nfo->ffHeight*0.01f;
    }
    if (angle!=-1) nfo->lgFont.lfOrientation=nfo->lgFont.lfEscapement=angle;
  }
  Update();
  {
    int width=ToInt(defItemInfo.ffWidth/defItemInfo.ffHeight*100.0f);
    if (facename!=NULL) strncpy(defItemInfo.lgFont.lfFaceName,facename,sizeof(defItemInfo.lgFont.lfFaceName));
    if (widthper!=-1) 
    {
      width=widthper;
      defItemInfo.ffWidth=width*defItemInfo.ffHeight*0.01f;;  
    }
    if (height!=-1) 
    {
      defItemInfo.ffHeight=height;
      defItemInfo.ffWidth=width*defItemInfo.ffHeight*0.01f;
    }
  }
}

void CDiagramView::ChangeTextColor(COLORREF color)
{
  SaveUndo();
  for (int id=-1;(id = EnumItems(id,true)) != -1;) 
  {
    ItemInfo *nfo=GetItemExtraData(id);
    nfo->fontColor=color;
  }
  Update();
  defItemInfo.fontColor=color;
}

void CDiagramView::ChangeTextAlign(int align)
{
  SaveUndo();
  for (int id=-1;(id = EnumItems(id,true)) != -1;) 
  {
    ItemInfo *nfo=GetItemExtraData(id);
    nfo->textAlign=align;
  }
  Update();
  defItemInfo.textAlign=align;
}

SFloatRect CDiagramView::GetFirstSelectItemRect()
{
  int id=EnumItems(-1,true);
  if (id==-1) return SFloatRect(0,0,1,1);
  else return GetItemRect(id);
}

void CDiagramView::ChangeText(const CString &text)
{
  SaveUndo();
  for (int id=-1;(id = EnumItems(id,true)) != -1;) 
  {
    ItemInfo *nfo=GetItemExtraData(id);
    nfo->text=text;
  }
  Update();
}


void CDiagramView::BeginEditText()
{
  if (wTextEdit.GetSafeHwnd()!=NULL) return;
  if (EnumItems(-1,true)==-1) return;
  CDiagramView::ItemInfo *nfo=GetRefItem();	
  int style[]={ES_LEFT,ES_CENTER,ES_RIGHT};
  SFloatRect frc=GetFirstSelectItemRect();
  CRect rc=MapRectToWindow(frc);
  if (nfo->lgFont.lfOrientation>450 && nfo->lgFont.lfOrientation<1250 ||
    nfo->lgFont.lfOrientation>2250 && nfo->lgFont.lfOrientation<3150)
  {
    CSize sz=rc.Size();
    CPoint cent=rc.CenterPoint();	
    rc.right=cent.x+sz.cy/2;
    rc.bottom=cent.y+sz.cx/2;
    rc.left=cent.x-sz.cy/2;
    rc.top=cent.y-sz.cx/2;
  }
  if (rc.left<0) rc.left=0;
  if (rc.top<0) rc.top=0;
  wTextEdit.Create(WS_CHILD |WS_VISIBLE|WS_BORDER|ES_AUTOHSCROLL|ES_AUTOVSCROLL|ES_MULTILINE|ES_WANTRETURN|style[nfo->textAlign],rc,this,IDC_TEXTINPUT);	
  wEditFont.CreateFont(nfo->lgFont.lfHeight,nfo->lgFont.lfWidth,0,0,nfo->lgFont.lfWeight,
    nfo->lgFont.lfItalic,nfo->lgFont.lfUnderline,nfo->lgFont.lfStrikeOut,
    nfo->lgFont.lfCharSet,0,0,0,0,nfo->lgFont.lfFaceName);
  wTextEdit.SetFont(&wEditFont);
  wTextEdit.SetFocus();
  wTextEdit.SetWindowText(nfo->text);  
  SGraphItem itm;
  GetItem(EnumItems(-1,true),&itm);
  if (itm.color!=TRANSPARENTCOLOR)
    wEditBrush.CreateSolidBrush(itm.color);
}

void CDiagramView::SaveEditedText()
{
  if (_savingText) return;
  if (wTextEdit.GetSafeHwnd()==NULL) return;
  _savingText=true;
  CString text;
  wTextEdit.GetWindowText(text);
  ChangeText(text);
  wTextEdit.DestroyWindow();
  wEditFont.DeleteObject();
  wEditBrush.DeleteObject();
  RecalculateDiagram();
  _savingText=false;
}

BOOL CDiagramView::PreTranslateMessage(MSG* pMsg) 
{
  if (pMsg->hwnd==wTextEdit.GetSafeHwnd())
  {
    if (pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE)
    {
      SaveEditedText();
      return TRUE;
    }
    if (pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_TAB)
    {
      SaveEditedText();
      int p=EnumItems(-1,true);
      if (p!=-1)
      {
        p=GetNextItem(p,(GetKeyState(VK_SHIFT) & 0x80)!=0);
        if (p!=-1)
        {
          Deselect();
          SelectItem(p);
          BeginEditText();
        }
      }
      return TRUE;
    }
    TranslateMessage(pMsg);
    DispatchMessage(pMsg);
    return TRUE;
  }

  return CGraphCtrl::PreTranslateMessage(pMsg);
}


HBRUSH CDiagramView::OnCtlColor( CDC* pDC, CWnd* pWnd, UINT nCtlColor )
{    
  pDC->SetTextColor(GetRefItem()->fontColor);
  HBRUSH ret;
  if (wEditBrush.GetSafeHandle())
  {
    LOGBRUSH br;
    wEditBrush.GetLogBrush(&br);
    pDC->SetBkColor(br.lbColor);
    ret=wEditBrush;
  }
  else
  {
    ret=CGraphCtrl::OnCtlColor(pDC,pWnd,nCtlColor);
  }
  pDC->SetTextColor(GetRefItem()->fontColor);
  return ret;
}

int CDiagramView::GetNextItem(int current, bool prev)
{
  float minx,miny;
  int selected=-1;
  bool rep=false;
  SFloatRect refrc=GetItemRect(current);
rep:
  if (prev)
  {
    minx=-FLT_MAX;
    miny=-FLT_MAX;
  }
  else
  {
    minx=FLT_MAX;
    miny=FLT_MAX;
  }
  for (int i=-1;(i=EnumItems(i))!=-1;) if (i!=current)
  {
    SFloatRect rc=GetItemRect(i);
    if (prev)
    {
      if (rc.top<refrc.top || (rc.top==refrc.top && rc.left<refrc.left))
      {
        if (rc.top>miny || (rc.top==miny && rc.left>minx))
        {
          minx=rc.left;
          miny=rc.top;
          selected=i;
        }
      }
    }
    else
      if (rc.top>refrc.top || (rc.top==refrc.top && rc.left>refrc.left))
      {
        if (rc.top<miny || (rc.top==miny && rc.left<minx))
        {
          minx=rc.left;
          miny=rc.top;
          selected=i;
        }
      }
  }

  if (selected==-1 && !rep)
  {
    rep=true;
    if (prev) 
    {
      refrc.left=FLT_MAX;
      refrc.top=FLT_MAX;
      goto rep;
    }
    else 
    {
      refrc.left=-FLT_MAX;
      refrc.top=-FLT_MAX;
      goto rep;
    }
  }
  return selected;
}

void CDiagramView::RecalculateDiagram()
{ 
  int iters=0;
  do
  {
    _expressChanged=false;
    for (int i=-1;(i=EnumItems(i))!=-1;)
    {
      ItemInfo *nfo=this->GetItemExtraData(i);
      const char *express=IsExpression(nfo->text);
      if (express)
      {
        _expressContext=i;
        double value;
        double wtime;
        nfo->lasterror=0;
        DoCalc(express,value,wtime);		  
        if (wtime<0.0f)
          SetValue(i,false,value);
        else if (wtime>=0.0f)
          AddEvent(wtime,i,false,value);
      }
    }
  }
  while (++iters<MAXITERS && _expressChanged);
}

bool CDiagramView::SetValue(int index, bool ispin, double value)
{
  if (ispin) 	
    index= FindItemByLinkID(_expressContext,index);
  if (IsValidItem(index))
  {
    ItemInfo *nfo=GetItemExtraData(index);
    if (nfo->value!=value)
    {
      nfo->value=value;
      _expressChanged=true;
    }
    return true;
  }
  else
    return false;
}

double CDiagramView::QueryValue(int index, bool ispin)
{
  if (ispin) 	
    index= FindItemByLinkID(_expressContext,index);

  if (IsValidItem(index))
  {
    ItemInfo *nfo=GetItemExtraData(index);
    if (IsExpression(nfo->text)) return nfo->value;
    else
    {
      double value;
      if (sscanf(nfo->text,"%lg",&value)==1) return value;
      return GetNAN();
    }
  }
  else
    return GetNAN();
}

bool CDiagramView::SetError(int errnumb, int errdata)
{
  ItemInfo *nfo=GetItemExtraData(_expressContext);
  if (nfo->lasterror==0)
  {
    nfo->lasterror=errnumb;
    nfo->lasterrordata=errdata;
  }
  return true;
}

int CDiagramView::FindItemByPin(int srcitem,int trgitem,int pin)
{
  pin+=1;
  for (int i=-1;(i=EnumLinks(i,srcitem,trgitem))!=-1;)
  {
    SDLinkInfo *lnk=GetLinkExtraData(i);
    if (lnk && lnk->pinid==pin) return i;
  }
  return -1;
}

int CDiagramView::FindItemByLinkID(int srcitem, int index)
{
  if (CDS_ISPIN_INPUT(index)) 
  {
    index-=CDS_PIN_INPUT_MIN;
    int i=FindItemByPin(-1,srcitem,index);
    if (i==-1) return -1;
    return GetLink(i).fromitem;
  }
  else if (CDS_ISPIN_OUTPUT(index)) 
  {
    index-=CDS_PIN_OUTPUT_MIN;
    int i=FindItemByPin(srcitem,-1,index); 
    if (i==-1) return -1;
    return GetLink(i).toitem;
  }
  else if (index==CDS_PIN_THIS) return srcitem;
  else return -1;
}

static CPoint sipka1[]=   
{CPoint(0,0),CPoint(5,-2),CPoint(4,0),CPoint(5,2),CPoint(0,0)};
static CPoint sipka2[]=   
{CPoint(0,0),CPoint(5,-2),CPoint(5,0),CPoint(5,2),CPoint(0,0)};
static CPoint sipka3[]=   
{CPoint(0,0),CPoint(5,-2),CPoint(7,-2),CPoint(2,0),CPoint(7,2),CPoint(5,2),CPoint(0,0)};
static CPoint sipka4[]=   
{CPoint(0,0),CPoint(5,-3),CPoint(3,0),CPoint(5,3),CPoint(0,0)};
static CPoint sipka5[]=   
{CPoint(5,-2),CPoint(0,0),CPoint(5,2),CPoint(0,0)};
static CPoint sipka6[]=   
{CPoint(4,-2),CPoint(0,0),CPoint(4,2),CPoint(8,0)};
static CPoint sipka7[]=   
{CPoint(0,0),CPoint(1,2),CPoint(3,3),CPoint(5,2),
 CPoint(6,0),CPoint(5,-2),CPoint(3,-3),CPoint(1,-2)};
static CPoint sipka8[]=   
{CPoint(0,2),CPoint(0,-2),CPoint(5,0)};
static CPoint sipka9[]=   
{CPoint(0,3),CPoint(6,3),CPoint(6,-3),CPoint(0,-3)};
static CPoint sipka10[]=   
{CPoint(0,0),CPoint(5,2),CPoint(4,0)};
static CPoint sipka11[]=   
{CPoint(0,0),CPoint(5,-2),CPoint(4,0)};
static CPoint sipka12[]=   
{CPoint(0,0),CPoint(4,2),CPoint(6,2),CPoint(6,-2),CPoint(4,-2)};
static CPoint sipka13[]=   
{CPoint(0,0),CPoint(4,2),CPoint(6,2),CPoint(0,0),CPoint(6,-2),CPoint(4,-2)};



void NewSipka(CDC &dc, CPoint pos, CPoint dir, int style, int size)
{
  CPoint *points;
  int num;
  switch (style)
  {
  case 0: points=sipka1;num=5;break;
  case 1: points=sipka2;num=5;break;
  case 2: points=sipka3;num=7;break;
  case 3: points=sipka4;num=5;break;
  case 4: return;
  case 5: points=sipka5;num=4;break;
  case 6: points=sipka6;num=4;break;
  case 7: points=sipka7;num=8;break;
  case 8: points=sipka8;num=3;break;
  case 9: points=sipka9;num=4;break;
  case 10: points=sipka10;num=3;break;
  case 11: points=sipka11;num=3;break;
  case 12: points=sipka12;num=5;break;
  case 13: points=sipka13;num=6;break;
  default: ASSERT(false);return;
  }
  CPen *pen=dc.GetCurrentPen();
  CPen newpen;
  LOGPEN lgpen;
  pen->GetLogPen(&lgpen); 
  if (lgpen.lopnStyle!=PS_SOLID && lgpen.lopnWidth.x==1)
  {
    newpen.CreatePen(PS_SOLID,lgpen.lopnWidth.x,lgpen.lopnColor);
    pen=dc.SelectObject(&newpen);
  }
  size=size*4+8;
  int d=(int)(sqrt((float)(dir.x*dir.x+dir.y*dir.y)));
  if (d==0) return;
  dir.x=(dir.x*size*8+d/2)/d;
  dir.y=(dir.y*size*8+d/2)/d;
  CRect rc(dir.x,dir.y,dir.y,-dir.x);
  dc.BeginPath();
  for (int i=0;i<num;i++)
  {
    CPoint ptq;
    ptq.x=((rc.left*points[i].x+rc.right*points[i].y+16)>>5)+pos.x;
    ptq.y=((rc.top*points[i].x+rc.bottom*points[i].y+16)>>5)+pos.y;
    if (i==0) dc.MoveTo(ptq);else dc.LineTo(ptq);
  }
  dc.EndPath();
  dc.StrokeAndFillPath();  
  dc.SelectObject(pen);
}

bool CDiagramView::OnCustomDrawLink(int lnk, int beginitem, int enditem, int order, void *data, CDC &dc, CRect rc) 
{
  SDLinkInfo *flags;
  SGraphLink &lk=GetLink(lnk);
  flags=(SDLinkInfo *)lk.data;
  ASSERT(flags!=NULL);
  CPen *pen=dc.GetCurrentPen();
  LOGPEN lgpen;
  pen->GetLogPen(&lgpen); 
  int width=flags->linewidth;
  int style=flags->linestyle;
  if (lnk==_curlink) {width++;style=PS_SOLID;}
  if (lgpen.lopnColor!=lk.color || 
    lgpen.lopnStyle!=style ||
    lgpen.lopnWidth.x!=width)
  {
    HPEN hpen=CreatePen(style,width,lk.color);
    HPEN old=(HPEN)::SelectObject(dc,hpen);
    if (old==_oldpen) DeleteObject(_oldpen);
    _oldpen=hpen;
  }
  CPen::DeleteTempMap();
  CBrush *curbrush=dc.GetCurrentBrush();
  LOGBRUSH lgbrush;
  COLORREF lkcolor=lk.color;
  if (flags->usebkcolor) lkcolor=_pageColor?_pageColorVal:GetSysColor(COLOR_WINDOW);
  curbrush->GetLogBrush(&lgbrush);
  if (lgbrush.lbColor!=lkcolor)
  {
    lgbrush.lbColor=lkcolor;
    lgbrush.lbStyle=BS_SOLID;        
    HBRUSH hbrush=CreateBrushIndirect(&lgbrush);
    HBRUSH old=(HBRUSH)::SelectObject(dc,hbrush);
    if (old==_oldbrush) DeleteObject(_oldbrush);
    _oldbrush=hbrush;
  }
  CBrush::DeleteTempMap();
  SFloatRect fr=GetItemRect(beginitem);
  CRect pp=MapRectToWindow(fr);
  if (lk.fromitem==lk.toitem)
  {
    int zfx=300/(rc.right-rc.left);
    int zfy=300/(rc.bottom-rc.top);
    switch(lk.order & 0x3)
    {
    case 0: dc.Arc(&rc,CPoint(pp.right,pp.top),CPoint(pp.left,pp.bottom));
      NewSipka(dc,CPoint(pp.left,rc.bottom),CPoint(-10,-zfy),flags->arrowtype,flags->arrowsize);
      break;
    case 1: dc.Arc(&rc,CPoint(pp.right,pp.bottom),CPoint(pp.left,pp.top));
      NewSipka(dc,CPoint(rc.left,pp.top),CPoint(zfx,-10),flags->arrowtype,flags->arrowsize);
      break;
    case 2: dc.Arc(&rc,CPoint(pp.left,pp.bottom),CPoint(pp.right,pp.top));
      NewSipka(dc,CPoint(pp.right,rc.top),CPoint(10,zfy),flags->arrowtype,flags->arrowsize);
      break;
    case 3: dc.Arc(&rc,CPoint(pp.left,pp.top),CPoint(pp.right,pp.bottom));
      NewSipka(dc,CPoint(rc.right,pp.bottom),CPoint(-zfx,10),flags->arrowtype,flags->arrowsize);
      break;
    }
  }
  else
  {
    dc.MoveTo(rc.left,rc.top);
    dc.LineTo(rc.right,rc.bottom);
    NewSipka(dc,CPoint(rc.right,rc.bottom),CPoint(rc.left-rc.right,rc.top-rc.bottom),flags->arrowtype,flags->arrowsize);
  }
  return true;
}

void CDiagramView::SetArrowType(int lnk, SDLinkInfo *flags)
{
  SaveUndo();
  if (lnk!=-1) 
  {
    delete GetLinkExtraData(lnk);
    GetLink(lnk).data=(void *)flags;
    if (flags) GetLink(lnk).flags|=SGRI_CUSTOMDRAW;else GetLink(lnk).flags&=~SGRI_CUSTOMDRAW;
  }
  if (flags) {defLinkFlags=*flags;defLinkUseCustom=true;}
  else defLinkUseCustom=false;
} 


void CDiagramView::SimulateStep(bool stepover)
{
  double lasttime=GetCurTime();
rep:
  _expressChanged=false;
  while (IsWaiting() && _expressChanged==false  && (!stepover || lasttime==GetCurTime())) 
    Step();
  if (_expressChanged) 
    RecalculateDiagram();
  if (stepover && IsWaiting() && lasttime==GetCurTime()) goto rep;
}

static bool InsideBox(float intr1x, float intr1y, SFloatRect &box)
{
  float eps=abs(box.right-box.left)*1e-5;
  if (intr1x>box.right+eps || intr1x<box.left-eps || intr1y>box.bottom+eps || intr1y<box.top-eps) return false;
  return true;
}
static float PointDistance(float x1, float y1, float x2, float y2)
{
  return sqrt(sqr(x1-x2)+sqr(y1-y2));
}
static inline float Det(float a, float b, float c, float d)
{
  return (a*d-b*c);
}
static void LineIntersection
(
 float x1, float y1, float x2, float y2, 
 float x3, float y3, float x4, float y4,
 float &x, float &y
 )
{
  //and compute the intersection
  //      ||x1 y1|  x1-x2|
  //      ||x2 y2|       |
  //      |              |
  //      ||x3 y3|  x3-x4|
  //      ||x4 y4|       |
  //x =  ------------------
  //      |x1-x2    y1-y2|
  //      |x3-x4    y3-y4|
  //
  //      ||x1 y1|  y1-y2|
  //      ||x2 y2|       |
  //      |              |
  //      ||x3 y3|  y3-y4|
  //      ||x4 y4|       |
  //y =  ------------------
  //      |x1-x2    y1-y2|
  //      |x3-x4    y3-y4|
  float det1122 = Det(x1, y1, x2, y2);
  float det3344 = Det(x3, y3, x4, y4);
  float denominator = Det(x1-x2, y1-y2, x3-x4, y3-y4);
  x = Det(det1122, x1-x2, det3344, x3-x4)/denominator;
  y = Det(det1122, y1-y2, det3344, y3-y4)/denominator;
  if (!_finite(x)) x = x3;
  if (!_finite(y)) y = y3;
}

void ClipLineByDiamond(SFloatRect &line, SFloatRect &box, bool begin)
{
  float x1=line.left, x2=line.right;
  float y1=line.top, y2=line.bottom;
  //now, find intersections with all border lines of diamond and return one, which is 
  //closest to other end of line and lies inside box
  float ax = box.left, ay = (box.bottom+box.top)/2;
  float bx = (box.left+box.right)/2, by = box.top;
  float cx = box.right, cy = ay;
  float dx = bx, dy = box.bottom;
  float intr1x, intr1y;
  LineIntersection(x1,y1,x2,y2,ax,ay,bx,by,intr1x,intr1y);
  float intr2x, intr2y;
  LineIntersection(x1,y1,x2,y2,bx,by,cx,cy,intr2x,intr2y);
  float intr3x, intr3y;
  LineIntersection(x1,y1,x2,y2,cx,cy,dx,dy,intr3x,intr3y);
  float intr4x, intr4y;
  LineIntersection(x1,y1,x2,y2,dx,dy,ax,ay,intr4x,intr4y);
  float endx, endy;
  if (begin) { endx = x2; endy = y2; }
  else { endx = x1; endy = y1; }
  float len1 = PointDistance(endx, endy, intr1x,intr1y);
  float len2 = PointDistance(endx, endy, intr2x,intr2y);
  float len3 = PointDistance(endx, endy, intr3x,intr3y);
  float len4 = PointDistance(endx, endy, intr4x,intr4y);
  float min=0;
  float x,y;
  //not found initialization
  float xs=x2-x1;
  if ( (xs>0) ^ begin ) x=ax;
  else x=cx;
  y = ay;
  //
  bool found=false;
  if (InsideBox(intr1x, intr1y, box)) { min = len1; x=intr1x; y=intr1y; found=true;}
  if ((!found || len2<min) && InsideBox(intr2x, intr2y, box)) { min = len2; x=intr2x; y=intr2y; found=true;}
  if ((!found || len3<min) && InsideBox(intr3x, intr3y, box)) { min = len3; x=intr3x; y=intr3y; found=true;}
  if ((!found || len4<min) && InsideBox(intr4x, intr4y, box)) { min = len4; x=intr4x; y=intr4y; found=true;}
  if (begin)
  {
    line.left=x;
    line.top=y;
  }
  else
  {
    line.right=x;
    line.bottom=y;
  }
}

bool CDiagramView::CalcLinkRect(int from, int to, int order, CRect &rc)
{
  SGraphItem fromIt, toIt;
  GetItem(from, &fromIt); GetItem(to,&toIt);
  if (fromIt.flags & SGRI_NOTDRAWED || toIt.flags & SGRI_NOTDRAWED) return false;
  if (from==to) CGraphCtrl::CalcLinkRect(from, to, order, rc);
  else
  {
    ItemInfo *fromInfo = static_cast<ItemInfo*>(fromIt.data);
    ItemInfo *toInfo = static_cast<ItemInfo*>(toIt.data);
    SFloatRect ln, *fr=NULL, *tt=NULL;
    PrecalcLinkRect(from, to, order, fr, tt, ln);
    if (fromInfo->shape==stDiamond) ClipLineByDiamond(ln,*fr,true);
    else ClipLineByBox(ln,*fr,true);
    if (toInfo->shape==stDiamond) ClipLineByDiamond(ln,*tt,false);
    else ClipLineByBox(ln,*tt,false);
    rc=MapRectToWindow(ln);
  }
  return true;
}

