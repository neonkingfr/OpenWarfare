// DlgText.cpp : implementation file
//

#include "stdafx.h"
#include "DiagramEditor.h"
#include "DlgText.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgText dialog


DlgText::DlgText(CWnd* pParent /*=NULL*/)
	: CDialog(DlgText::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgText)
	vAngle = 0;
	vFontBold = FALSE;
	vFontFaceID = -1;
	vFontItalic = FALSE;
	vFontSize = 0;
	vFontStrikeOut = FALSE;
	vFontUnderline = FALSE;
	vText = _T("");
	vWidth = 0;
	vAlign = -1;
	//}}AFX_DATA_INIT
    notext=false;
}


void DlgText::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgText)
	DDX_Control(pDX, IDC_TEXTINPUT, wText);
	DDX_Control(pDX, IDC_FONTFACE, wFontFace);
	DDX_Control(pDX, IDC_COLORBUTT, wColorButt);
	DDX_Text(pDX, IDC_ANGLE, vAngle);
	DDV_MinMaxInt(pDX, vAngle, 0, 360);
	DDX_Check(pDX, IDC_FONTBOLD, vFontBold);
	DDX_CBIndex(pDX, IDC_FONTFACE, vFontFaceID);
	DDX_Check(pDX, IDC_FONTITALIC, vFontItalic);
	DDX_Text(pDX, IDC_FONTSIZE, vFontSize);
	DDV_MinMaxUInt(pDX, vFontSize, 1, 999);
	DDX_Check(pDX, IDC_FONTSTRIKEOUT, vFontStrikeOut);
	DDX_Check(pDX, IDC_FONTUNDERLINE, vFontUnderline);
	DDX_Text(pDX, IDC_TEXTINPUT, vText);
	DDX_Text(pDX, IDC_WIDTH, vWidth);
	DDV_MinMaxUInt(pDX, vWidth, 0, 1000);
	DDX_Radio(pDX, IDC_ALIGN, vAlign);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgText, CDialog)
	//{{AFX_MSG_MAP(DlgText)
	ON_WM_DRAWITEM()
	ON_NOTIFY(UDN_DELTAPOS, IDC_FONTSIZESPIN, OnDeltaposFontsizespin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_ANGLESPIN, OnDeltaposAnglespin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_WIDTHSPIN, OnDeltaposWidthspin)
	ON_BN_CLICKED(IDC_FONTBOLD, OnRefresh)
	ON_BN_CLICKED(IDC_COLORBUTT, OnColorbutt)
	ON_BN_CLICKED(IDC_FONTITALIC, OnRefresh)
	ON_BN_CLICKED(IDC_FONTUNDERLINE, OnRefresh)
	ON_BN_CLICKED(IDC_FONTSTRIKEOUT, OnRefresh)
	ON_EN_CHANGE(IDC_FONTSIZE, OnRefresh)
	ON_EN_CHANGE(IDC_WIDTH, OnRefresh)
	ON_CBN_EDITCHANGE(IDC_FONTFACE, OnRefresh)
	ON_CBN_SELENDOK(IDC_FONTFACE, OnRefresh)
	ON_EN_KILLFOCUS(IDC_TEXTINPUT, OnKillfocusTextinput)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgText message handlers

void DlgText::SaveToLogFont(LOGFONT &lgfont)
  {
  if (UpdateData()==FALSE) return;
  memset(&lgfont,0,sizeof(lgfont));
  lgfont.lfEscapement=vAngle*10;
  wFontFace.GetLBText(vFontFaceID,lgfont.lfFaceName);
  lgfont.lfHeight=vFontSize;
  lgfont.lfItalic=vFontItalic;
  lgfont.lfOrientation=vAngle*10;
  lgfont.lfStrikeOut=vFontStrikeOut;
  lgfont.lfUnderline=vFontUnderline;
  lgfont.lfWeight=vFontBold?FW_BOLD:FW_REGULAR;
  lgfont.lfWidth=vFontSize*vWidth/100;
  lgfont.lfCharSet=EASTEUROPE_CHARSET;
  }

void DlgText::LoadFromLogFont(LOGFONT &lgfont)
  {
  int p=wFontFace.FindStringExact(-1,lgfont.lfFaceName);
  vFontSize=lgfont.lfHeight;
  vFontItalic=lgfont.lfItalic;
  vAngle=lgfont.lfOrientation/10;
  vFontStrikeOut=lgfont.lfStrikeOut;
  vFontUnderline=lgfont.lfUnderline;
  vFontBold=lgfont.lfWeight>FW_SEMIBOLD;
  vWidth=lgfont.lfWidth*100/vFontSize;
  UpdateData(FALSE);
  wFontFace.SetCurSel(p);
  }

static int __stdcall EnumFontCB(const LOGFONT *lpelfe, const TEXTMETRIC *lpntme, 
  DWORD flags,LPARAM lParam)
  {
  HWND list=(HWND)lParam;
  SendMessage(list,CB_ADDSTRING,0,(LPARAM)lpelfe->lfFaceName);
  return 1;
  }


BOOL DlgText::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  
  HDC dc=::GetDC(*this);
  LOGFONT fnt;
  fnt.lfCharSet=EASTEUROPE_CHARSET;
  fnt.lfFaceName[0]=0;
  fnt.lfPitchAndFamily=0;
  EnumFontFamiliesEx(dc,&fnt,EnumFontCB,(LPARAM)wFontFace.GetSafeHwnd(),0);
  ::ReleaseDC(*this,dc);	

  LoadFromLogFont(vFont);
  OnRefresh();

  if (notext) GetDlgItem(IDC_TEXTINPUT)->EnableWindow(FALSE);

  return TRUE;  // return TRUE unless you set the focus to a control
                // EXCEPTION: OCX Property Pages should return FALSE
  }

void DlgText::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
  {
  switch (nIDCtl)
	{
	case IDC_FONTFACE: OnDrawFontFace(lpDrawItemStruct);break;
	case IDC_COLORBUTT: OnDrawColorButton(lpDrawItemStruct);break;
	}
  }

void DlgText::OnDrawFontFace(LPDRAWITEMSTRUCT lpDis)
  {
  if (lpDis<0) return;
  CDC dc;
  dc.Attach(lpDis->hDC);
  if (lpDis->itemState & ODS_SELECTED)
	{
	dc.SetBkColor(GetSysColor(COLOR_HIGHLIGHT));
	dc.SetTextColor(GetSysColor(COLOR_HIGHLIGHTTEXT));
	}
  else
	{
	dc.SetBkColor(GetGValue(vColor)<128?RGB(255,255,255):RGB(0,0,0));
	dc.SetTextColor(vColor);
	}
  LOGFONT lg;
  memset(&lg,0,sizeof(lg));
  ::SendMessage(lpDis->hwndItem,CB_GETLBTEXT,lpDis->itemID,(LPARAM)lg.lfFaceName);
  lg.lfHeight=16;
  lg.lfWeight=IsDlgButtonChecked(IDC_FONTBOLD)?FW_BOLD:FW_NORMAL;
  lg.lfItalic=IsDlgButtonChecked(IDC_FONTITALIC);
  lg.lfStrikeOut=IsDlgButtonChecked(IDC_FONTSTRIKEOUT);
  lg.lfUnderline=IsDlgButtonChecked(IDC_FONTUNDERLINE);
  lg.lfCharSet=EASTEUROPE_CHARSET;
  CFont fnt;
  fnt.CreateFontIndirect(&lg);
  CFont *old=dc.SelectObject(&fnt);
  dc.ExtTextOut(0,0,ETO_OPAQUE,&lpDis->rcItem,NULL,0,NULL);
  dc.DrawText(lg.lfFaceName,strlen(lg.lfFaceName),&(lpDis->rcItem),DT_VCENTER|DT_SINGLELINE|DT_LEFT|DT_NOPREFIX);  
  dc.SelectObject(old);
  dc.Detach();
  }

void DlgText::OnDrawColorButton(LPDRAWITEMSTRUCT lpDis)
  {
  CDC dc;
  dc.Attach(lpDis->hDC);
  CRect rc=lpDis->rcItem;
  dc.DrawFrameControl(&rc, DFC_BUTTON, DFCS_BUTTONPUSH|((lpDis->itemState & 1)?DFCS_PUSHED:0));
  rc-=CRect(3,3,3,3);
  dc.FillSolidRect(&rc,vColor);
  dc.Detach();
  }

void DlgText::OnDeltaposFontsizespin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	DeltaPosSpin(IDC_FONTSIZE,1,999,pNMUpDown->iDelta);
	*pResult = 0;
}

void DlgText::OnDeltaposAnglespin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	DeltaPosSpin(IDC_ANGLE,0,360,pNMUpDown->iDelta);
	
	*pResult = 0;
}

void DlgText::OnDeltaposWidthspin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	DeltaPosSpin(IDC_WIDTH,0,1000,pNMUpDown->iDelta);
	
	*pResult = 0;
}

void DlgText::DeltaPosSpin(int idc, int min, int max, int delta)
  {
  int pos=(int)GetDlgItemInt(idc,NULL,TRUE);
  pos-=delta;
  if (pos<min) pos=min;
  if (pos>max) pos=max;
  SetDlgItemInt(idc,pos,TRUE);
  }

void DlgText::OnRefresh() 
  {
  wFontFace.Invalidate();
  LOGFONT fnt; 
  SaveToLogFont(fnt);
  fnt.lfOrientation=fnt.lfEscapement=0;
  CFont ff;
  ff.CreateFontIndirect(&fnt);
  wText.SetFont(&ff,TRUE);
  curfont.DeleteObject();
  curfont.Attach(ff.Detach());  
  }

void DlgText::OnColorbutt() 
  {
  CMainFrame *frm=static_cast<CMainFrame *>(AfxGetApp()->m_pMainWnd);
  frm->colorDialog.m_cc.rgbResult=vColor;
  if (frm->colorDialog.DoModal()==IDOK)
	{
	vColor=frm->colorDialog.GetColor();
	OnRefresh();
	}
  }



void DlgText::OnOK() 
  {
  SaveToLogFont(vFont);	
  CDialog::OnOK();
  }

void DlgText::OnKillfocusTextinput() 
{
	// TODO: Add your control notification handler code here
	
}
