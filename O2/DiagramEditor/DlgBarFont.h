#pragma once

#include "DiagramView.h"

// DlgBarFont dialog

#define DBFM_BOOLSTYLECHANGED (WM_APP+987)
#define DBFM_FONTSTYLECHANGED (WM_APP+988)
#define DBFM_FONTCOLORCHANGED (WM_APP+989)
#define DBFM_TEXTALIGNCHANGED (WM_APP+990)

class DlgBarFont : public CDialogBar
{
	DECLARE_DYNAMIC(DlgBarFont)
	bool _enablechange;

public:
	DlgBarFont(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgBarFont();

    COLORREF vColor;

// Dialog Data
	//{{AFX_DATA(DlgBarFont)

	enum { IDD = IDD_FONTPROPERTIESTOOLBAR };
	//}}AFX_DATA	


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgBarFont)
	//}}AFX_VIRTUAL

  void OnDrawFontFace(LPDRAWITEMSTRUCT lpDrawItemStruct);
  void OnDrawColorButton(LPDRAWITEMSTRUCT lpDis);

	// Generated message map functions
	//{{AFX_MSG(DlgBarFont)
  afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
  afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
  afx_msg void OnBnStyleClicked();
	afx_msg void OnEditchangeFontface();
	afx_msg void OnChangeFontsize();
	afx_msg void OnChangeWidth();
	afx_msg void OnChangeAngle();
	afx_msg void OnSelchangeFontface();
	afx_msg void OnColorbutt();
	afx_msg void OnSetAlign();
	afx_msg void OnDeltaposFontsizespin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposAnglespin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposWidthspin(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
  DECLARE_MESSAGE_MAP()
public:
	virtual void OnUpdateCmdUI( CFrameWnd* pTarget, BOOL bDisableIfNoHndler );
  BOOL OnInitDialog(void);
  BOOL Create(CWnd * parent, UINT id);
  void UpdateFields(CDiagramView::ItemInfo * info);
  void DeltaPosSpin(int idc, int min, int max, int delta);
  };
