#if !defined(AFX_DLGTEXT_H__8D11D842_246C_42B9_9DA3_7D9D8CB672D8__INCLUDED_)
#define AFX_DLGTEXT_H__8D11D842_246C_42B9_9DA3_7D9D8CB672D8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgText.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgText dialog

class DlgText : public CDialog
{
// Construction
	CFont curfont;
public:
	void DeltaPosSpin(int idc, int min, int max, int delta);
	void OnDrawColorButton(LPDRAWITEMSTRUCT lpDis);
	void OnDrawFontFace(LPDRAWITEMSTRUCT lpDrawItemStruct);
	void LoadFromLogFont(LOGFONT &lgfont);
	void SaveToLogFont(LOGFONT &lgfont);
	DlgText(CWnd* pParent = NULL);   // standard constructor	

// Dialog Data
	//{{AFX_DATA(DlgText)
	enum { IDD = IDD_TEXT };
	CEdit	wText;
	CComboBox	wFontFace;
	CButton	wColorButt;
	int		vAngle;
	BOOL	vFontBold;
	int		vFontFaceID;
	BOOL	vFontItalic;
	UINT	vFontSize;
	BOOL	vFontStrikeOut;
	BOOL	vFontUnderline;
	CString	vText;
	UINT	vWidth;
	int		vAlign;
	//}}AFX_DATA

	LOGFONT vFont;
	COLORREF vColor;
    bool notext;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgText)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgText)
	virtual BOOL OnInitDialog();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnDeltaposFontsizespin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposAnglespin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposWidthspin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRefresh();
	afx_msg void OnColorbutt();
	virtual void OnOK();
	afx_msg void OnKillfocusTextinput();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGTEXT_H__8D11D842_246C_42B9_9DA3_7D9D8CB672D8__INCLUDED_)
