#if !defined(AFX_DLGLINKPROPERTIES_H__6C319470_4978_4E35_B1F6_8FEC62743CB6__INCLUDED_)
#define AFX_DLGLINKPROPERTIES_H__6C319470_4978_4E35_B1F6_8FEC62743CB6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgLinkProperties.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgLinkProperties dialog

class DlgLinkProperties : public CDialog
{
// Construction
public:
	void DialogRules();
	DlgLinkProperties(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgLinkProperties)
	enum { IDD = IDD_LINKPROPERTIES };
	CListBox	wLineStyle;
	CSpinButtonCtrl	wLineSizeSpin;
	CListBox	wArrowType;
	UINT	vLineWidth;
	int		vArrowSize;
	BOOL	vUseArrows;
	//}}AFX_DATA

	UINT	vLineStyle;
	UINT	vArrowType;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgLinkProperties)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgLinkProperties)
	afx_msg void OnUserarrows();
	virtual BOOL OnInitDialog();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnDeltaposLinestylespin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeLinewidth();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
  BOOL vUseBKColor;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGLINKPROPERTIES_H__6C319470_4978_4E35_B1F6_8FEC62743CB6__INCLUDED_)
