// DlgPageSetup.cpp : implementation file
//

#include "stdafx.h"
#include "DiagramEditor.h"
#include "DlgPageSetup.h"
#include ".\dlgpagesetup.h"
#include "MainFrm.h"

IMPLEMENT_DYNAMIC(DlgPageSetup, CDialog)
DlgPageSetup::DlgPageSetup(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPageSetup::IDD, pParent)
    , vForceColor(FALSE)
    , vForceDim(FALSE)
    , vImageX(640)
    , vImageY(480)
    , vShowGrid(FALSE)
    , vGrid(0)
    , vEnableGrid(FALSE)
  {
}

DlgPageSetup::~DlgPageSetup()
{
}

void DlgPageSetup::DoDataExchange(CDataExchange* pDX)
{
CDialog::DoDataExchange(pDX);
DDX_Control(pDX, IDC_FORCEBKCOLOR, wForceColor);
DDX_Check(pDX, IDC_FORCEBKCOLOR, vForceColor);
DDX_Control(pDX, IDC_FORCEIMAGEDIM, wForceDim);
DDX_Check(pDX, IDC_FORCEIMAGEDIM, vForceDim);
DDX_Text(pDX, IDC_IMAGEX, vImageX);
DDV_MinMaxUInt(pDX, vImageX, 16, 16383);
DDX_Text(pDX, IDC_IMAGEY, vImageY);
DDV_MinMaxUInt(pDX, vImageY, 16, 16383);
DDX_Control(pDX, IDC_IMAGEX, wImageX);
DDX_Control(pDX, IDC_IMAGEY, wImageY);
DDX_Control(pDX, IDC_SELECTCOLOR, wColorButt);
DDX_Control(pDX, IDC_SHOWGRID, wShowGrid);
DDX_Check(pDX, IDC_SHOWGRID, vShowGrid);
DDX_Text(pDX, IDC_GRID, vGrid);
DDX_Control(pDX, IDC_ENABLEGRID, wEnableGrid);
DDX_Check(pDX, IDC_ENABLEGRID, vEnableGrid);
DDX_Control(pDX, IDC_GRID, wGrid);
}


BEGIN_MESSAGE_MAP(DlgPageSetup, CDialog)
  ON_BN_CLICKED(IDC_FORCEBKCOLOR, OnBnClickedForcebkcolor)
  ON_BN_CLICKED(IDC_FORCEIMAGEDIM, OnBnClickedForceimagedim)
  ON_WM_DRAWITEM()
  ON_BN_CLICKED(IDC_SELECTCOLOR, OnBnClickedSelectcolor)
  ON_BN_CLICKED(IDC_ENABLEGRID, OnBnClickedEnablegrid)
END_MESSAGE_MAP()


// DlgPageSetup message handlers

BOOL DlgPageSetup::OnInitDialog()
  {
  CDialog::OnInitDialog();

  DialogRules();



  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

void DlgPageSetup::DialogRules(void)
  {
  wColorButt.EnableWindow(wForceColor.GetCheck());
  wImageX.EnableWindow(wForceDim.GetCheck());
  wImageY.EnableWindow(wForceDim.GetCheck());
  wGrid.EnableWindow(wEnableGrid.GetCheck());
  wShowGrid.EnableWindow(wEnableGrid.GetCheck());
  }

void DlgPageSetup::OnBnClickedForcebkcolor()
  {
  DialogRules();
  }

void DlgPageSetup::OnBnClickedForceimagedim()
  {
  DialogRules();
  }

void DlgPageSetup::OnDrawColorButton(LPDRAWITEMSTRUCT lpDis)
  {
  CDC dc;
  dc.Attach(lpDis->hDC);
  CRect rc=lpDis->rcItem;
  dc.DrawFrameControl(&rc, DFC_BUTTON, DFCS_BUTTONPUSH|((lpDis->itemState & 1)?DFCS_PUSHED:0));
  rc-=CRect(3,3,3,3);
  if (lpDis->itemState!=ODS_DISABLED) dc.FillSolidRect(&rc,vColor);
  else dc.FillSolidRect(&rc,GetSysColor(COLOR_3DFACE));
  dc.Detach();
  }


void DlgPageSetup::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
  {
  if (nIDCtl==IDC_SELECTCOLOR) OnDrawColorButton(lpDrawItemStruct);
  
  }

void DlgPageSetup::OnBnClickedSelectcolor()
  {
  CMainFrame *frm=static_cast<CMainFrame *>(AfxGetMainWnd());
  frm->colorDialog.m_cc.rgbResult=vColor;
  if (frm->colorDialog.DoModal()==IDOK)
    {
    vColor=frm->colorDialog.GetColor();
    wColorButt.Invalidate();
    }
  }

void DlgPageSetup::OnBnClickedEnablegrid()
  {
  DialogRules();
  }
