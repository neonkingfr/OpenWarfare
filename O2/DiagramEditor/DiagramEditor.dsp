# Microsoft Developer Studio Project File - Name="DiagramEditor" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=DiagramEditor - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "DiagramEditor.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "DiagramEditor.mak" CFG="DiagramEditor - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "DiagramEditor - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "DiagramEditor - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Projects/DiagramEditor", MDUAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "DiagramEditor - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /Zi /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "MFC_NEW" /YX"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /nodefaultlib:"LIBC.LIB"

!ELSEIF  "$(CFG)" == "DiagramEditor - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "MFC_NEW" /FR /YX"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x405 /d "_DEBUG"
# ADD RSC /l 0x405 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "DiagramEditor - Win32 Release"
# Name "DiagramEditor - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\aaaa.cpp
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\archive\ArchiveParamFile.cpp
# End Source File
# Begin Source File

SOURCE=.\ChildView.cpp
# End Source File
# Begin Source File

SOURCE=.\DiagramEditor.cpp
# End Source File
# Begin Source File

SOURCE=.\DiagramEditor.rc
# End Source File
# Begin Source File

SOURCE=.\DiagramSimulator.cpp
# End Source File
# Begin Source File

SOURCE=.\DiagramView.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgBarFont.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgLineStyle.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgLinkProperties.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPageSetup.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPinID.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPrinting.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgText.cpp
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\GraphView\GraphCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\Bredy.Libs\archive\ArchiveParamFile.h
# End Source File
# Begin Source File

SOURCE=.\ChildView.h
# End Source File
# Begin Source File

SOURCE=.\DiagramEditor.h
# End Source File
# Begin Source File

SOURCE=.\DiagramSimulator.h
# End Source File
# Begin Source File

SOURCE=.\DiagramView.h
# End Source File
# Begin Source File

SOURCE=.\DlgBarFont.h
# End Source File
# Begin Source File

SOURCE=.\DlgLineStyle.h
# End Source File
# Begin Source File

SOURCE=.\DlgLinkProperties.h
# End Source File
# Begin Source File

SOURCE=.\DlgPageSetup.h
# End Source File
# Begin Source File

SOURCE=.\DlgPinID.h
# End Source File
# Begin Source File

SOURCE=.\DlgText.h
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\GraphView\geAutoArray.tli
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\GraphView\GraphCtrl.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\boldfont.ico
# End Source File
# Begin Source File

SOURCE=.\res\calign.ico
# End Source File
# Begin Source File

SOURCE=.\res\DiagramEditor.ico
# End Source File
# Begin Source File

SOURCE=.\res\DiagramEditor.rc2
# End Source File
# Begin Source File

SOURCE=.\DlgPrinting.h
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\archive\IArchive.h
# End Source File
# Begin Source File

SOURCE=.\res\italicfont.ico
# End Source File
# Begin Source File

SOURCE=.\res\lalign.ico
# End Source File
# Begin Source File

SOURCE=.\res\lupazoom.cur
# End Source File
# Begin Source File

SOURCE=.\res\ralign.ico
# End Source File
# Begin Source File

SOURCE=.\res\strickfont.ico
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbar1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ulinefont.ico
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
