#if !defined(AFX_DLGPINID_H__EE6E7929_F0B3_459F_B03E_6C457840240A__INCLUDED_)
#define AFX_DLGPINID_H__EE6E7929_F0B3_459F_B03E_6C457840240A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPinID.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgPinID dialog

class DlgPinID : public CDialog
{
// Construction
public:
	DlgPinID(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgPinID)
	enum { IDD = IDD_PINID };
	CListBox	wID;
	//}}AFX_DATA

	int value;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPinID)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPinID)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPINID_H__EE6E7929_F0B3_459F_B03E_6C457840240A__INCLUDED_)
