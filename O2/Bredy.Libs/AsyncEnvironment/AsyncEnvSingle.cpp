
#include ".\asyncenvsingle.h"
#include <process.h>

class AsyncEnvSingle_StopMessage: public AsyncMessage
{
public:
  virtual void Run()
  {
    AsyncEnvSingle *self=static_cast<AsyncEnvSingle *>(AsyncEnvironment::GetCurrentEnvironment());
    CloseHandle(self->_noMsgWait);
    Release();
    _endthread();
  }
};

AsyncEnvSingle::AsyncEnvSingle(void)
{
  _waitTimeout=0;
  _noMsgWait=0;
  _thisThread=0;
  _thisThreadId=0;
  _previousEnv=NULL;
}

AsyncEnvSingle::~AsyncEnvSingle(void)
{  
  PreDestroy();
  if (IsMyContext())
  {
    CloseHandle(_thisThread);      
    CloseHandle(_noMsgWait);
  }
}

void AsyncEnvSingle::SetPernamentMode()
{
  SetNonPernamentMode(INFINITE);
}

void AsyncEnvSingle::SetNonPernamentMode(long timeout)
{
  _waitTimeout=timeout;//set new timeout
  if (_noMsgWait) SetEvent(_noMsgWait); //update timeout releasing the thread
}

bool AsyncEnvSingle::IsMyContext()
{
  return GetCurrentThreadId()==_thisThreadId;
}

void AsyncEnvSingle::Send(AsyncMessage *message)
{
    _queue.Lock();        //enter to crit.section. Thread shouldn't die during next test
    bool succ=_msg_queue.AddMessageToQueue(message);           
    if (!succ)        //message cannot be added into queue
    {
      message->AddRef();    //add ref this message to ensure, that pointer will be valid after Reply
      message->Reply(true); //Reply on message and notify sender, that it was rejected
      message->Release();   //release the pointer
    }
    if (_thisThread==0)     //nobody can process this message
    {
      _noMsgWait=CreateEvent(NULL,FALSE,FALSE,NULL);    //create event
      _thisThread=(HANDLE)_beginthreadex(NULL,0,AsyncStartThread,this,0,&_thisThreadId); //create thread
    }
    SetEvent(_noMsgWait); //message was added, so notify thread, that it has new message in queue
    _queue.Unlock();    //unlocks message queue
}

unsigned _stdcall AsyncEnvSingle::AsyncStartThread(void *ptr)
{
  AsyncEnvSingle *self=(AsyncEnvSingle *)ptr;      
  AsyncEnvReportNewThread(self);
  self->SetCurrentEnvironment();
  self->_queue.Lock();    //enter crit.sect - prevents to add message during test
  while (self->_msg_queue.IsMessageReady())          //is any message ready?
  {
    self->_queue.Unlock();   //yes, so leave crit.sect 
    self->PumpAllMessages();                          //and continue processing messages
    self->_queue.Lock();    //enter crit.sect - prevents to add message during test
  }
  CloseHandle(self->_thisThread);       //no message remain, close handles and mark thread not running
  CloseHandle(self->_noMsgWait);
  self->_thisThread=0;    
  self->_thisThreadId=0;
  self->_noMsgWait=0;
  self->_queue.Unlock();   //finish, so we allow other threads to run this thread again
  return 0;
}

void AsyncEnvSingle::PumpAllMessages()
{
  bool leave=false;
  do
  {    
    if (!PumpMessage())
    {
      unsigned long waitRes=WaitForSingleObject(_noMsgWait,_waitTimeout);
      leave=waitRes==WAIT_TIMEOUT;      
    }
  }
  while (!leave);
}

bool AsyncEnvSingle::PumpMessage()
{
  if (IsMyContext())
  {
    _queue.Lock();
    AsyncMessage *msg=_msg_queue.GetMessageFromQueue();
    _queue.Unlock();
    if (msg!=0) 
    { 
      msg->Run();
      msg->Reply();
      msg->Release();
      return true;
    }
    else
      return false;
  }
  else return false;
}

void AsyncEnvSingle::Attach()
{
  if (_thisThread==0)
  {
    DuplicateHandle(GetCurrentProcess(),GetCurrentThread(),GetCurrentProcess(),&_thisThread,0,FALSE,DUPLICATE_SAME_ACCESS);
    _thisThreadId=GetCurrentThreadId();
    _noMsgWait=CreateEvent(NULL,FALSE,FALSE,NULL);
    _waitTimeout=0;
	_previousEnv=GetCurrentEnvironment();
	SetCurrentEnvironment();
  }
}
void AsyncEnvSingle::Detach()
{
  if (_thisThread!=0)
  {
    CloseHandle(_thisThread);
    CloseHandle(_noMsgWait);
    _thisThread=0;
    _noMsgWait=0;
    _thisThreadId=0;
	SetCurrentEnvironment(_previousEnv);
	_previousEnv=0;
  }
}

void AsyncEnvSingle::PreDestroy()
{
  if (_msg_queue.IsQueueDisabled()) return;   //queue already disabled - no special action is needed
  _queue.Lock();      //lock message queue, no other thread can handle this
  if (!_msg_queue.IsQueueDisabled()) //check queue again, because this state could be changed.
  { 
    _msg_queue.Flush();   //remove all messages from queue
    if (_thisThread)  //thread still running, we must stop it
    {
      if (!IsMyContext())  //check if we aren't trying to stop self.
      {
        HANDLE dupThread;
          //we must duplicate the handle, because ending thread can close it;
        DuplicateHandle(GetCurrentProcess(),_thisThread,GetCurrentProcess(),&dupThread,0,FALSE,DUPLICATE_SAME_ACCESS);
        SetNonPernamentMode(0); //disable any timeout or any pernament mode. 
            //in this point, thread will try to get next message
            //but we flushes queue, so no message is processed, and thread leaves cycle
        _queue.Unlock(); //we must unlock queue, because thread will want to check queue status.
        WaitForSingleObject(dupThread,INFINITE); //no, wait for thread terminating
        CloseHandle(dupThread); //close handle        
        return ;
      }
      else
      {
        ///Current thread cannot destroy itself, so thread will continue.
      }
    }
  }
  _queue.Unlock();
}

