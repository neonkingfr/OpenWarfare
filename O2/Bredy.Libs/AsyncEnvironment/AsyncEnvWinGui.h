#pragma once
#include "asyncenvsingle.h"

/**
Class AsyncEnvWinGui implements AsyncEnvironment on Windows Gui thread. Most of time these threads are waiting and processing 
windows messages in message loop. AsyncEnvWinGui is using message loop to notify gui thread, that there are new
messages in environment to process. Because message loop works "automatically", gui thread don't need any special
handling for posted messages. All posted messages are processed during message loop.
*/
class AsyncEnvWinGui : public AsyncEnvSingle
{
  HWND _dispatchHandle; ///<Handle of message window
  LONG _posted;         ///<-1 no message posted, 0 - some message posted, 1 - don't post a message.
public:
  AsyncEnvWinGui(void);
  virtual ~AsyncEnvWinGui(void);

  ///Attaches instance to the current thread
  /** When instance is attached, message window is created, and all 
    messages can be posted to this environment using special windows message.
    Host thread don't need make some special handling, because all messages
    are automatically delivered using DispatchMessage
    */
  bool AttachToWinMsgQueue();
  ///Detaches instance to the current thread
  /** It destroys message windows. After it, none messages can be delivered automatically
  */
  bool DetachFromWinMsgQueue();


  void PumpAllMessages() {_posted=-1;AsyncEnvSingle::PumpAllMessages();}

  virtual void Send(AsyncMessage *message);


};
