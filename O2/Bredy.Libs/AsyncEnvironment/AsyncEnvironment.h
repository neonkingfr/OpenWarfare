#pragma once

#include "Tls.h"
#include "AsyncBlocker.h"
#include <es/types/pointers.hpp>

inline bool AsyncMessage_InStackFrame(void *ptr)
{
  const char *cptr=(const char *)ptr;
  const char *rptr=(const char *)&ptr;
  if (rptr<cptr && rptr+1000>cptr) return true;
  return false;
}

class AsyncEnvironment;
class AsyncMessageQueue;

///Implements message for interthread communication
class AsyncMessage
{
private:
  long _ref;    ///<Ref count for current message;
  AsyncMessage *_next;
  AsyncBlocker *_blocker;
                ///Freies message
  virtual void Free() {delete this;}

  friend AsyncMessageQueue;

  AsyncMessage *NextMessage() {return _next;}
  void AddMessage(AsyncMessage *msg)
  {
    AsyncMessage *l=this;
    while (l->_next!=0) l=l->_next;
    if (l==msg) return; //cannot add message twice
    l->_next=msg;
  }
  void Unlink() {_next=0;}

public:
  AsyncMessage() :_next(0),_ref(0),_blocker(0) {if (AsyncMessage_InStackFrame(this)) AddRef();}

  
  ///Adds reference for current message;
  void AddRef() {InterlockedIncrement(&_ref);}
  ///Releases reference for current message
  /** When reference count reaches zero, deletes the message */
  void Release() {if (InterlockedDecrement(&_ref)<=0) Free();}
  
  ///Procedure to run in foreing environment
  virtual void Run()=0;

  ///Notifies message, that message was replied.
  /** If message cointan an event object, this function notifies event, that 
  reply has been written into the message
  @param reject if this parameter is true, message has been rejected without reply
  */
  virtual void Reply(bool reject=false) {if (_blocker) _blocker->Unblock();}

  virtual ~AsyncMessage() {if (_blocker) _blocker->Release();}

  AsyncBlocker *GetBlocker() 
  {
	if (!_blocker) _blocker=AsyncBlocker::AllocBlocker();
	return _blocker;
  }


};

///Implements message queue. It can be instanciated in derived class
class AsyncMessageQueue
{
  AsyncMessage *_queue;
  long _queuelen;
public:

  ///Constructs empty message queue
  AsyncMessageQueue():_queue(0),_queuelen(0) {}
  ///Destructs message queue
  /** All messages in queue are extracted, marked as rejected and disposed*/
  ~AsyncMessageQueue()
  {
  }

  void Flush(bool disableQueue=true)
  {
    AsyncMessage *msg=GetMessageFromQueue();
    while (msg)
    {
      msg->Reply(true);
      msg->Release();
      msg=GetMessageFromQueue();
    }
    if (disableQueue) _queuelen=-1;else _queuelen=0;
  }

  void EnableQueue() {InterlockedCompareExchange(&_queuelen,0,-1);}
  bool IsQueueDisabled() {return _queuelen==-1;}

    /// adds message in queue
  /** @note Function is not thread safe, use proper synchronisation
  @param msg message that is added to queue. Reference counter of message is incerased
  */
  bool AddMessageToQueue(AsyncMessage *msg)
  {
    if (_queuelen<0) return false;
	if (msg->NextMessage()!=0) return true;  //message is added - we cannot process this message
    msg->AddRef();
    if (_queue==0) _queue=msg;
    else 
      _queue->AddMessage(msg);
    ++_queuelen; 
    return true;
  }

  /// Gets message from queue
  /** @note Function is not thread safe, use proper synchronisation
  @return function returns first message in queue and removes message from queue. Doesn't
  changes reference counter. To dispose message, call Release method.
  */
  AsyncMessage *GetMessageFromQueue()
  {
    if (_queue)
    {
      AsyncMessage *cur=_queue;
      _queue=cur->NextMessage();
      --_queuelen;
      cur->Unlink();
      return cur;
    }
    else
      return 0;
  }

  /// Adds priority message into queue. 
  /** Priority message is added at the first position in queue.
  Function simulates a stack, so adding two priority messages causes, that second message will be extracted first
  @param msg message to insert into queue
  
  @note Function is not thread safe, use proper synchronisation
  */
  bool AddPriorMessage(AsyncMessage *msg)
  {
    if (_queuelen<0) return false;
	if (msg->NextMessage()!=0) return true;  //message is added - we cannot process this message
    msg->AddRef();
    msg->AddMessage(_queue);
    _queue=msg;    
    ++_queuelen;
    return true;
  }

  ///Returns true, if queue contains any message
  /**
  @return true Queue contains a message
  @note because function contains single test, this function can be used without thread synchronization */
  bool IsMessageReady() {return _queue!=0;}



};

///Base class for asynchronious environment.
/**This class is abstract, you cannot create instance from this class. Use any derived class 
@note Please, pay attantion about destruction this class 
@see PreDestroy
*/

class AsyncEnvironment
{
  static Tls<AsyncEnvironment> *thisTls;
protected:

  ///Saves pointer to instance to TLS.
  /**Derived classes must call this function after they starts new thread.
  It allows usage of GetCurrentEnvironment function */
  void SetCurrentEnvironment(AsyncEnvironment *restore=0)
  {
	if (!restore) restore=this;
    thisTls->SetValue(restore);
  }

  friend void AsyncEnvTlsInit();

public:
  ///Standard destructor will destroy base class
  /**AsyncEnvironmnent must be prepared for destroying before class is destroyed. 
  @see PreDestroy
  virtual ~AsyncEnvironment()=0 {}
  ///Sends a message to enviroment. 
  /**
  @param message Message to send
  */
  virtual void Send(AsyncMessage *message)=0;

  ///Sends message and waits for reply
  /**Function sends the message and blocks current environment until message is
  prcessed and replied. Message can be replied using Reply method
  @param message message to send to environment
  @param timeout timeout in miliseconds for waiting. Default value causes infinite waiting.
  @return function returns true, when message was successfuly processed and replied. Function
	return false, if message was not replied during requested time. In this case, message is not
	removed from queue, and can be replied later. Caller can check reply status using blocker object
  @note During blocking, waiting environment can process other messages. To prevent this, use SendAndWaitNoYield
    function.

  */
  bool SendAndWait(AsyncMessage *message, unsigned long timeout=0xFFFFFFFF)
  {
    if (IsMyContext()) {message->Run();return true;}
	Ref<AsyncMessage> pmsg=message;
	AsyncBlocker *blocker=message->GetBlocker();	
	Send(message);	
	return blocker->BlockAndPumpMessages(timeout);
  }
 
  ///Sends message and waits for reply.
  /**Function sends the message and blocks current environment until message is
  prcessed and replied. Message can be replied using Reply method. 

  During waiting, caller is blocked unconditionaly, any pending messages are not processed until current message
  is processed.
  @param message message to send to environment
  @param timeout timeout in miliseconds for waiting. Default value causes infinite waiting.
  @return function returns true, when message was successfuly processed and replied. Function
	return false, if message was not replied during requested time. In this case, message is not
	removed from queue, and can be replied later. Caller can check reply status using blocker object
  */
  bool SendAndWaitNoYield(AsyncMessage *message, unsigned long timeout=0xFFFFFFFF)
  {
    if (IsMyContext()) {message->Run();return true;}
	Ref<AsyncMessage> pmsg=message;
	AsyncBlocker *blocker=message->GetBlocker();
	Send(message);
	return blocker->Block(timeout);
  }
  ///Retuns pointer to environment associated with current thread
  /**
  @return pointer to environment. NULL, when none environment is associated.
  @note One environment can be shared with multiple threads
  */
  static AsyncEnvironment *GetCurrentEnvironment() 
    {
      return thisTls->GetValue();
    }

  ///Returns true, when program currently working in context that belongs to it.
  /** Function can help detect possible deadlock, when one AsyncEnvironment
  wants send message into current environment and waits for reply. There is deadlock
  because function would wait on reply of message, that will be never processed 
  (because current thread is waiting). In these situations, which are
  detectable using IsMyContext, message is processed immediatelly, without waiting,
  and out of order of message queue. Message is processed asi priority message, but
  with no dangerous of deadlocks.
  */
  virtual bool IsMyContext()=0;

  ///Prepares environment for destroying
  /**
  This function MUST be called on destructor of any derived class.
  Function can be called multipletimes, to give each derived class ensurement that
  environment is prepared for destroying;
  */
  virtual void PreDestroy()=0;

};

void ___AsyncEnvReportNewThread(AsyncEnvironment *self);


#ifdef _DEBUG
#define AsyncEnvReportNewThread(x) ___AsyncEnvReportNewThread(x)
#else
#define AsyncEnvReportNewThread(x)
#endif