#pragma once
#include "asyncenvironment.h"


class AsyncClassMessage: public AsyncMessage
{
public:
  void *_owner;
  AsyncClassMessage(): _owner(0) {}
};

enum AsyncBaseAttachableException {AsyncBaseAttachable_NotAttached};

class AsyncBaseAttachable
{
protected:
  AsyncEnvironment *_attached;
  AsyncEnvironment *GetAttached()
  {
    if (_attached==0) throw AsyncBaseAttachable_NotAttached;
    return _attached;
  }
public:
  void FinalizeMessage(AsyncMessage *msg)
  {
	  AsyncClassMessage &amsg=dynamic_cast<AsyncClassMessage &>(*msg);
	  amsg._owner=reinterpret_cast<void *>(this);
  }
  AsyncBaseAttachable(): _attached(0) {}
  AsyncBaseAttachable(AsyncEnvironment *a): _attached(a) {}
  ///Sends a message to enviroment. 
  /**@copydoc AsyncEnvironment::Send */  
  void Send(AsyncMessage *msg) {FinalizeMessage(msg);GetAttached()->Send(msg);}
  ///Sends message and waits for reply.
  /**@copydoc AsyncEnvironment::SendAndWait */  
  bool SendAndWait(AsyncMessage *msg) {FinalizeMessage(msg);return GetAttached()->SendAndWait(msg);}
  ///Sends message and waits for reply.
  /**@copydoc AsyncEnvironment::SendAndWaitNoYield*/  
  bool SendAndWaitNoYield(AsyncMessage *msg) {FinalizeMessage(msg);return GetAttached()->SendAndWaitNoYield(msg);}
  bool IsMyContext() {return GetAttached()->IsMyContext();}

  void Attach(AsyncEnvironment *a) {_attached=a;}
  AsyncEnvironment *Detach() {AsyncEnvironment *a=_attached;_attached=0;return a;}
};


enum AsyncClassException {AsyncClass_MessageHasNoOwner};

template <class BaseEnv=AsyncEnvSingle>
class AsyncClass: public BaseEnv
{
  friend AsyncClassMessage;
  public:
	void Send(AsyncMessage *msg)
	{
      AsyncClassMessage *amsg=dynamic_cast<AsyncClassMessage *>(msg);
	  if (amsg) amsg->_owner=reinterpret_cast<void *>(static_cast<BaseEnv *>(this));
	  BaseEnv::Send(msg);
	}
    static __forceinline BaseEnv *GetMessageOwner(AsyncMessage *msg) 
      { 
        AsyncClassMessage &amsg=dynamic_cast<AsyncClassMessage &>(*msg);
        if (amsg._owner==0) throw AsyncClass_MessageHasNoOwner;
        return reinterpret_cast<BaseEnv *>(amsg._owner);
      }
};


#define DEF_MESSAGE0(message_name,  owner_class) \
class message_name: public AsyncClassMessage \
{ \
  public: \
	virtual void Run()	\
	  {owner_class *p=static_cast<owner_class *>(GetMessageOwner(this));p->On##message_name(this);} \
};	\
friend message_name

#define DEF_MESSAGE1(message_name, owner_class, param_type1) \
class message_name: public AsyncClassMessage \
{ \
  public: \
	param_type1 _p1;  \
	virtual void Run()	\
	  {owner_class *p=static_cast<owner_class *>(GetMessageOwner(this));p->On##message_name(this,_p1);} \
	message_name(param_type1 p1):_p1(p1) {}\
};	\
friend message_name

#define DEF_MESSAGE2(message_name,  owner_class, param_type1, param_type2) \
class message_name: public AsyncClassMessage \
{ \
  public: \
	param_type1 _p1;  \
	param_type2 _p2;  \
	virtual void Run()	\
	  {owner_class *p=static_cast<owner_class *>(GetMessageOwner(this));p->On##message_name(this,_p1,_p2);} \
	message_name(param_type1 p1,param_type2 p2):_p1(p1),_p2(p2) {} \
};	\
friend message_name

#define DEF_MESSAGE3(message_name,  owner_class, param_type1, param_type2, param_type3) \
class message_name: public AsyncClassMessage \
{ \
  public: \
	param_type1 _p1;  \
	param_type2 _p2;  \
	param_type3 _p3;  \
	virtual void Run()	\
	  {owner_class *p=static_cast<owner_class *>(GetMessageOwner(this));p->On##message_name(this,_p1,_p2,_p3);} \
	message_name(param_type1 p1,param_type2 p2, param_type3 p3):\
	  _p1(p1),_p2(p2),_p3(p3) {} \
};	\
friend message_name

#define DEF_MESSAGE4(message_name,  owner_class, param_type1, param_type2, param_type3, param_type4) \
class message_name: public AsyncClassMessage \
{ \
  public: \
	param_type1 _p1;  \
	param_type2 _p2;  \
	param_type3 _p3;  \
	param_type4 _p4;  \
	virtual void Run()	\
	  {owner_class *p=static_cast<owner_class *>(GetMessageOwner(this));p->On##message_name(this,_p1,_p2,_p3,_p4);} \
	message_name(param_type1 p1,param_type2 p2, param_type3 p3, param_type4 p4):\
	  _p1(p1),_p2(p2),_p3(p3),_p4(p4) {} \
};	\
friend message_name

#define DEF_MESSAGE5(message_name,  owner_class, param_type1, param_type2, param_type3, param_type4, param_type5) \
class message_name: public AsyncClassMessage \
{ \
  public: \
	param_type1 _p1;  \
	param_type2 _p2;  \
	param_type3 _p3;  \
	param_type4 _p4;  \
	param_type5 _p5;  \
	virtual void Run()	\
	  {owner_class *p=static_cast<owner_class *>(GetMessageOwner(this));p->On##message_name(this,_p1,_p2,_p3,_p4,_p5);} \
	message_name(param_type1 p1,param_type2 p2, param_type3 p3, param_type4 p4,param_type5 p5):\
	  _p1(p1),_p2(p2),_p3(p3),_p4(p4),_p5(p5) {} \
};	\
friend message_name

#define DEF_MESSAGE6(message_name,  owner_class, param_type1, param_type2, param_type3, param_type4, param_type5, param_type6) \
class message_name: public AsyncClassMessage \
{ \
  public: \
	param_type1 _p1;  \
	param_type2 _p2;  \
	param_type3 _p3;  \
	param_type4 _p4;  \
	param_type5 _p5;  \
	param_type6 _p6;  \
	virtual void Run()	\
	  {owner_class *p=static_cast<owner_class *>(GetMessageOwner(this));p->On##message_name(this,_p1,_p2,_p3,_p4,_p5,_p6);} \
	message_name(param_type1 p1,param_type2 p2, param_type3 p3, param_type4 p4,param_type5 p5, param_type6 p6):\
	  _p1(p1),_p2(p2),_p3(p3),_p4(p4),_p5(p5),_p6(p6) {} \
};	\
friend message_name

#define DEF_MESSAGE7(message_name,  owner_class, param_type1, param_type2, param_type3, param_type4, param_type5, param_type6, param_type7) \
class message_name: public AsyncClassMessage \
{ \
  public: \
	param_type1 _p1;  \
	param_type2 _p2;  \
	param_type3 _p3;  \
	param_type4 _p4;  \
	param_type5 _p5;  \
	param_type6 _p6;  \
	param_type7 _p7;  \
	virtual void Run()	\
	  {owner_class *p=static_cast<owner_class *>(GetMessageOwner(this));p->On##message_name(this,_p1,_p2,_p3,_p4,_p5,_p6,_p7);} \
	message_name(param_type1 p1,param_type2 p2, param_type3 p3, param_type4 p4,param_type5 p5, param_type6 p6, param_type7 p7):\
	  _p1(p1),_p2(p2),_p3(p3),_p4(p4),_p5(p5),_p6(p6), _p7(p7) {} \
};	\
friend message_name

#define DEF_MESSAGE8(message_name,  owner_class, param_type1, param_type2, param_type3, param_type4, param_type5, param_type6, param_type7, param_type8) \
class message_name: public AsyncClassMessage \
{ \
  public: \
	param_type1 _p1;  \
	param_type2 _p2;  \
	param_type3 _p3;  \
	param_type4 _p4;  \
	param_type5 _p5;  \
	param_type6 _p6;  \
	param_type7 _p7;  \
	param_type8 _p8;  \
	virtual void Run()	\
	  {owner_class *p=static_cast<owner_class *>(GetMessageOwner(this));p->On##message_name(this,_p1,_p2,_p3,_p4,_p5,_p6,_p7,_p8);} \
	message_name(param_type1 p1,param_type2 p2, param_type3 p3, param_type4 p4,param_type5 p5, param_type6 p6, param_type7 p7, param_type8 p8):\
	  _p1(p1),_p2(p2),_p3(p3),_p4(p4),_p5(p5),_p6(p6), _p7(p7),_p8(p8) {} \
};	\
friend message_name

#define DEF_MESSAGE9(message_name,  owner_class, param_type1, param_type2, param_type3, param_type4, param_type5, param_type6, param_type7, param_type8, param_type9) \
class message_name: public AsyncClassMessage \
{ \
  public: \
	param_type1 _p1;  \
	param_type2 _p2;  \
	param_type3 _p3;  \
	param_type4 _p4;  \
	param_type5 _p5;  \
	param_type6 _p6;  \
	param_type7 _p7;  \
	param_type8 _p8;  \
	param_type9 _p9;  \
	virtual void Run()	\
	  {owner_class *p=static_cast<owner_class *>(GetMessageOwner(this));p->On##message_name(this,_p1,_p2,_p3,_p4,_p5,_p6,_p7,_p8,_p9);} \
	message_name(param_type1 p1,param_type2 p2, param_type3 p3, param_type4 p4,param_type5 p5, param_type6 p6, param_type7 p7, param_type8 p8, param_type9 p9):\
	  _p1(p1),_p2(p2),_p3(p3),_p4(p4),_p5(p5),_p6(p6),_p7(p7),_p8(p8),_p9(p9) {} \
};	\
friend message_name

#define DEF_COMPLEX_MESSAGE(message_name,  owner_class)  \
class message_name: public AsyncClassMessage\
{\
  public:\
	virtual void Run()\
	  {owner_class *p=static_cast<owner_class *>(GetMessageOwner(this));p->On##message_name(this,*this);}

#define END_COMPLEX_MESSAGE(message_name)\
};\
  friend message_name