#pragma once
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "asyncenvironment.h"
#include "CriticalSection.h"

class AsyncEnvSingle_StopMessage;

/**
class AsyncEnvSingle implements single thread environment. 
*/
class AsyncEnvSingle :  public AsyncEnvironment
{
  AsyncEnvironment *_previousEnv;
protected:
  AsyncMessageQueue _msg_queue;     ///<Message queue
  friend AsyncEnvSingle_StopMessage; ///<Special message used internally for stopping the thread
  HANDLE _thisThread;                 ///<Handle of current running thread, or 0 when thread is not running
  unsigned int _thisThreadId;         ///<ID of current thread
  HANDLE _noMsgWait;                  ///<Handle to event used to wait for messages
  CriticalSection _queue;            ///<Critical section that guarding message queue
  long _waitTimeout;                  ///<time in milliseconds, that thread will wait for messages before dies
  
protected:
  ///Thread procedure
  static unsigned _stdcall AsyncStartThread(void *ptr);
public:
  ///Constructs new asynchronious environment
  AsyncEnvSingle(void);
  ///Destroys environment
  /**
  @note
    Destroying this class is little complicated. The most easy way is destroy
    this class in another environment. Destructor posts a special "Stop" message into environment
    and waits for processing this message. After that, it closes all resourses and exits. Any 
    messages, that are pending in queue, are marked as rejected.<br><br>
    Environment can be destroyed from inside. Destruction can detect this situation and terminates
    the thread after all resources are released. During stack destroying, object inside are not 
    notified about, so beware on it. Don't allocate objects on stack before environment is destryed.
    */
  virtual ~AsyncEnvSingle(void);

  ///Sets pernament mode
  /**
  Environment can work in two modes:

  non-pernament mode - it optimizes usage of thread resource. When environment has no pending messages in queue,
  it terminates itself. Next posted message starts new thread. This mode is default and allows use environments
  on huge count of objects, when most of objects are inactive and only very little count of objects processing 
  some messages. There is one disadvantage: Starting thread is not very fast operation.

  pernament mode - this mode forces thread to stay active even if no pending messages is in queue. In this 
  case, thread sleeps and waits for messages. This mode is useful for small count environments that process
  large count of messages.

  Function SetPrentamentMode sets pernament mode for current environment. Mode can be changed anytime. Changing
  mode to pernament during environment's inactive state doesn't start the thread.

  @see SetNonPernamentMode
  */
  void SetPernamentMode();

  ///Sets non-pernament mode and defines timeout
  /**
  About pernament and non-pernament mode, see SetPernamentMode;

  @param timeout defines timeout in milliseconds, during thread is waiting for messages before it dies
    (becomes inactive). Default value means immediatelly. Sometimes can be useful to define a little time, especially
    when messages arrives periodically and environments process these messages faster. Starting and terminating
    the thread can degrade performance.


  @see SetPernamentMode
  */
  void SetNonPernamentMode(long timeout=0);
  
  virtual void Send(AsyncMessage *message);

  ///Pumps one message
  /** 
  Allows to force environment pumps a single message. 
  This function can be called only from inside of environment. Function extracts message
  and processes it. It can be used, when environment is middle of something and wants to get some
  extra data from caller. 
  */
  bool PumpMessage();

  ///Pumps all messages in queue
  /**
  Function processes all messages in queue and then returns. This behaviour is depends on current mode 
  (pernament or non-pernament). If pernament mode is in effect, function never returns, until mode
  is changed to non-pernament. If non-pernament mode is in effect, and timeout is defined, function
  will process all messages and wait for another messages specified period of time.
  */
  void PumpAllMessages();
  

  ///Attachs environment to current thread
  /** When thread is attached, it can be used as undepreciated environment. But thread must extract and
    process messages manually using PumpMessage or PumpAllMessages. Before thread is terminated, it must
    call Detach function to switch environment into the inactive mode.
    @see AsyncEnvWinGui::AttachToWinMsgQueue
    */
  
  void Attach();
  ///Detaches current thread from environment
  /** Detaches thread from environment. This function must be called before attached thread exits.
  @see AsyncEnvWinGui::DetachFromWinMsgQueue
  */
  void Detach();

  ///Returns true, if function is called in context of instance thread
  bool IsMyContext();

  void PreDestroy();


};
