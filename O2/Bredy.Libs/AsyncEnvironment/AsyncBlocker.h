// AsyncBlocker.h: interface for the AsyncBlocker class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ASYNCBLOCKER_H__71209AEA_9475_4FFA_B9B2_83DDB0C562EB__INCLUDED_)
#define AFX_ASYNCBLOCKER_H__71209AEA_9475_4FFA_B9B2_83DDB0C562EB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class AsyncBlocker  
{
public:
  virtual ~AsyncBlocker()=0 {}

  static AsyncBlocker *AllocBlocker();    

  virtual bool Block(unsigned long timeout=0xFFFFFFFF)=0;
  virtual bool BlockAndPumpMessages(unsigned long timeout=0xFFFFFFFF)=0;
  virtual void Unblock()=0;
  virtual void Release()=0;

};

#endif // !defined(AFX_ASYNCBLOCKER_H__71209AEA_9475_4FFA_B9B2_83DDB0C562EB__INCLUDED_)
