// AsyncBlocker.cpp: implementation of the AsyncBlocker class.
//
//////////////////////////////////////////////////////////////////////

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "AsyncBlocker.h"
#include "AsyncEnvSingle.h"
#include "CriticalSection.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

static CriticalSection AsyncBlockerCSect;


class AsyncWinApiBlocker: public AsyncBlocker
{
  HANDLE _event;
public:
  AsyncWinApiBlocker(): _event(CreateEvent(NULL,FALSE,FALSE,NULL)) {}
  ~AsyncWinApiBlocker() {CloseHandle(_event);}
  AsyncWinApiBlocker *next;
  virtual bool Block(unsigned long timeout=0xFFFFFFFF);
  virtual bool BlockAndPumpMessages(unsigned long timeout=0xFFFFFFFF);
  virtual void Unblock();
  virtual void Release();
};

static AsyncWinApiBlocker* GBlockerList=0;
static bool FirstTouch=false;

static void FreeBlockerList()
{
  AsyncBlockerCSect.Lock();
  while (GBlockerList)
  {
	AsyncWinApiBlocker *b=GBlockerList;
	GBlockerList=GBlockerList->next;
	delete b;
  }
  AsyncBlockerCSect.Unlock();
}

AsyncBlocker *AsyncBlocker::AllocBlocker()
{
  AsyncBlocker *res;
  AsyncBlockerCSect.Lock();
  if (!FirstTouch)
  {
	FirstTouch=true;
	atexit(FreeBlockerList);
	res=new AsyncWinApiBlocker;
  }
  else if (GBlockerList)
  {
	res=GBlockerList;
	GBlockerList=GBlockerList->next;
  }
  else
	res=new AsyncWinApiBlocker;
  AsyncBlockerCSect.Unlock();
  return res;
}

bool AsyncWinApiBlocker::Block(unsigned long timeout)
{
  bool res=WaitForSingleObject(_event,timeout)!=WAIT_TIMEOUT;
  return res;
}
void AsyncWinApiBlocker::Unblock()
{
  SetEvent(_event);
}

class AsyncWinApiBlockerEnvSingleWrapper: public AsyncEnvSingle
{
public:
  HANDLE GetEventObject() {return _noMsgWait;}
};

bool AsyncWinApiBlocker::BlockAndPumpMessages(unsigned long timeout)
{
  AsyncEnvironment *env=AsyncEnvironment::GetCurrentEnvironment();
  AsyncEnvSingle *singEnv=dynamic_cast<AsyncEnvSingle *>(env);
  if (singEnv==NULL) return Block(timeout);
  AsyncWinApiBlockerEnvSingleWrapper *wrp=static_cast<AsyncWinApiBlockerEnvSingleWrapper *>(singEnv);
  DWORD time=GetTickCount();  
  HANDLE wfld[2];
  wfld[0]=_event;
  wfld[1]=wrp->GetEventObject();
  int res;
  while ((res=WaitForMultipleObjects(2,wfld,FALSE,timeout)==WAIT_OBJECT_0+1))
  {
	while (wrp->PumpMessage()) {}
	if (timeout!=0xFFFFFFFF)
	{
	  DWORD curtime=GetTickCount();
	  time=curtime-time;
	  if (timeout>time) timeout-=time;else timeout=0;
	}
  }
  return res!=WAIT_TIMEOUT;
}

void AsyncWinApiBlocker::Release()
{
  AsyncBlockerCSect.Lock();
  next=GBlockerList;
  GBlockerList=this;
  AsyncBlockerCSect.Unlock();
}
