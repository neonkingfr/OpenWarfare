#include ".\asyncenvmultiple.h"
#include <process.h>

struct ThreadProcHelp
{
  AsyncEnvMultiple *self;
  HANDLE start;
  AsyncMessage *message;  
};

static unsigned int _stdcall ThreadProc(void *data)
{
  ThreadProcHelp &hlp=*(ThreadProcHelp *)data;
  AsyncEnvReportNewThread(hlp.self);
  hlp.self->SetCurrentEnvironment();
  AsyncMessage *msg=hlp.message;
  SetEvent(hlp.start);
  msg->Run();
  msg->Reply();
  msg->Release();
  return 0;
}

void AsyncEnvMultiple::Send(AsyncMessage *message)
{
  message->AddRef();
  ThreadProcHelp hlp;
  hlp.start=CreateEvent(NULL,0,0,NULL);
  hlp.self=this;
  hlp.message=message;
  unsigned int id;
  HANDLE h=(HANDLE)_beginthreadex(NULL,0,ThreadProc,&hlp,0,&id);
  ThreadStarted(h,id);
  WaitForSingleObject(hlp.start,INFINITE);
  CloseHandle(hlp.start);
}