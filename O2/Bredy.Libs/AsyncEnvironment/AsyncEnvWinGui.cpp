#include ".\asyncenvwingui.h"
#include "CriticalSection.h"
#include <tchar.h>

#if WINVER<0x0500
#define GetWindowLongPtr GetWindowLong
#define SetWindowLongPtr SetWindowLong
#define LONG_PTR LONG
#define HWND_MESSAGE NULL
#endif


AsyncEnvWinGui::AsyncEnvWinGui(void)
{
  _dispatchHandle=0;
  _posted=-1;
}

AsyncEnvWinGui::~AsyncEnvWinGui(void)
{
  DetachFromWinMsgQueue();
}


static CriticalSection wndClassReg;
static int wndClassCnt;
#define WNDCLASSNAME _T("AsyncEnvironmentWindowsGuiMessageWindow@Bredy@15ew69")


static LRESULT CALLBACK AsyncEnvWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  if (msg==WM_APP)
  {
    AsyncEnvWinGui *self=(AsyncEnvWinGui *)GetWindowLongPtr(hWnd,GWL_USERDATA);
    self->SetNonPernamentMode(0);
    self->PumpAllMessages();
    return 1;
  }
  if (msg==WM_CREATE)
  {
    LPCREATESTRUCT cs=(LPCREATESTRUCT)lParam;
    SetWindowLongPtr(hWnd,GWL_USERDATA,(LONG_PTR)cs->lpCreateParams);
    return 1;
  }
  return DefWindowProc(hWnd,msg,wParam,lParam);
}

bool AsyncEnvWinGui::AttachToWinMsgQueue()
{
  HINSTANCE hInst=GetModuleHandle(NULL);
  if (_dispatchHandle==0)
  {
    Attach();
    wndClassReg.Lock();
    if (wndClassCnt==0)
    {
      WNDCLASS cls;
      memset(&cls,0,sizeof(cls));
      cls.lpfnWndProc=AsyncEnvWndProc;
      cls.lpszClassName=WNDCLASSNAME;
      cls.hInstance=hInst;
      if (RegisterClass(&cls)==FALSE) return false;
      wndClassCnt++;
    }
    wndClassReg.Unlock();
    _dispatchHandle=CreateWindow(WNDCLASSNAME,_T(""),0,0,0,0,0,HWND_MESSAGE,NULL,hInst,(LPVOID)this);
    _posted=-1;
    _msg_queue.EnableQueue();
    return true;
  }
  return false;
}

bool AsyncEnvWinGui::DetachFromWinMsgQueue()
{
  if (_dispatchHandle==0) return false;
  _msg_queue.Flush();
  DestroyWindow(_dispatchHandle);
  wndClassReg.Lock();;
  --wndClassCnt;
  if (wndClassCnt<1) 
  {
    HINSTANCE hInst=GetModuleHandle(NULL);
    UnregisterClass(WNDCLASSNAME,hInst);
  }
  wndClassReg.Unlock();;
  Detach();
  return true;
}

void AsyncEnvWinGui::Send(AsyncMessage *message)
{
  AsyncEnvSingle::Send(message);
  if (_dispatchHandle)
  {
    if (InterlockedIncrement(&_posted)==0)
      PostMessage(_dispatchHandle,WM_APP,0,0);
    else
      InterlockedDecrement(&_posted);
  }
}