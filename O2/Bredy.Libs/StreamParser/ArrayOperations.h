#ifndef _ARRAY_OPERATIONS_HEADER_INCLUDED_BREDY_15022005_
#define _ARRAY_OPERATIONS_HEADER_INCLUDED_BREDY_15022005_
#pragma once

/// Array operations library defines standard templates and rules for fast array operations
/**
Rules:
======

Each array must define following operations:
unsigned int Size() - returns size of array
Item &operator[] (unsigned int pos) - returns item on index;


*/

template<class Array, class Item> 
class CombinedArrays
{
protected:
  Array _a1;
  Array _a2;
public:
  CombinedArrays(const Array &a1, const Array &a2):_a1(a1),_a2(a2) {}
  unsigned int Size() const {return _a1.Size()+_a2.Size();}
  const Item& operator[] (unsigned int pos) const 
  {
    if (pos<_a1.Size()) return _a1[pos];
    else return _a2[pos-_a1.Size()];
  }
};


template<class Array1,class Array2, class Item> 
class CombinedArraysMixed
{
protected:
  Array1 _a1;
  Array2 _a2;
public:
  CombinedArraysMixed(const Array1 &a1, const Array2 &a2):_a1(a1),_a2(a2) {}
  unsigned int Size() const {return _a1.Size()+_a2.Size();}
  const Item& operator[] (unsigned int pos) const 
  {
    if (pos<_a1.Size()) return _a1[pos];
    else return _a2[pos-_a1.Size()];
  }
};


template<class Array, class Item>
class SubArray
{
protected:
  Array _a;
  unsigned int _beg;
  unsigned int _siz;
public:
  SubArray(Array a, int beg, int siz):_a(a),_beg(beg),_siz(siz) {}
  unsigned int Size() const {return _siz;}
  const Item& operator[] (unsigned int pos) const 
  {
    return _a[pos+_beg];
  }
};

template<class Array, class Functor>
bool ForEach(const Array &array,Functor func)
{
  for (unsigned int i=0, cnt=array.Size();i<cnt;i++) 
    if (!func(array[i])) return false;
  return true;
}

///EXAMPLE:
/*

class StringToArray
{
  const char *_text;
  unsigned int _length;
public:
  StringToArray(const char *text,unsigned int length=0):_text(text),_length(length==0?strlen(text):length) {}
  const char &operator[] (unsigned int index) const {return _text[index];}
  unsigned int Size() const {return _length;}
};

void CombArrayTest()
{
StringToArray text1("Ahoj svete!");
StringToArray text2("Jak se mas!");
StringToArray text3("Co delas!");
CombinedArrays<StringToArray,char> comb1(text2,text3);
CombinedArraysMixed<StringToArray,CombinedArrays<StringToArray,char>,char> comb2(text1,comb1);
SubArray<CombinedArraysMixed<StringToArray,CombinedArrays<StringToArray,char>,char>,char> subArr(comb2,5,20);

... use subArr array here ...
}
*/

#endif