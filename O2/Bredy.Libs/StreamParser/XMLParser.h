#ifndef _XMLPARSER_BASED_ON_STREAM_PARSER_INCLUDED_BY_BREDY_15022005_
#define _XMLPARSER_BASED_ON_STREAM_PARSER_INCLUDED_BY_BREDY_15022005_

#pragma once

#include "StreamParser.h"

#define XMLPARSER_MAX_ATTRIBUTES 256

///This is a simple XML parser.
/**
Parser works differently way, than two other known parsers. This is
not SAX parser, and also it is not DOM parser. The parser can
be used to read XML files as streams. Each XML file is presented
as many but relative small streams, that starts and ends between the tags.
Atribute values are also streams.

Parser has one function to read streams. You can obtain next character
in stream calling GetChar(). (XMLParser can act as Stream in StreamParser
for parsing content of XML archive, because ReadStream function is also defined)

To select stream in XML file, you need call OpenTag and CloseTag, when you need
enter to subtag section. Every OpenTag call flushs remain data and compares next tag with requested 
tag. When comparing fails, tag is not opened, and you can try to open another tag. You can test
multiple tags, until one of them passes. 

When you close tag using the CloseTag function, all data AND TAGS (AND any count of included sub-sections)
are discarded until closing tag is reached. 

XMLParser supports all recomended subtitution char sequences, and it also supports CDATA section. 
CDATA sections are transparent for streams. This mean, you cannot detect CDATA section. Section
can start and ends anywhere inside of stream, XMLParser only switchs decoding procedures everytime
CDATA section is detected, or leaved.
*/

template<class Stream>
class XMLParser: protected StreamParser<char,Stream>
{
  int _attribRdPos; ///<index of next read character in attribute (-1 we reading stream)
  int _attribRdEnd; ///<index of first character, not included in attribute
  bool _cdataSection; ///<true, we currently reading CDATA section
  int _attribList[XMLPARSER_MAX_ATTRIBUTES]; ///<offsets of attributes
  int _attribCount; ///<count of attributes;
  bool _inTag;      ///<true, we still can read tag attributes
  bool _singleTag;  ///<true, we reading single tag  

  ///Closes current tag, and prepares parser to read a stream data.
  void CloseTagSection();
public:
  ///Constructor. Parser needs pointer to class, that feeds parser with data
  /**
  @param stream pointer to stream that can supply a data.
  @param autodestruct true causes, that parser will cast delete operator on the stream during
	parser destruction 
  */
  XMLParser(Stream *_stream, bool autodestruct);

  ///Opens tag
  /**
  Function opens tag in XML structure. It discards all data, that remains in current
  stream, and loads next tag. Then, it compares tag names, and returns the result of this comparing. 
	If test passes, it also parses attributes.
  @param tagName name of tag to open
  @return Fnction returns true, when tag has been opened. In this case, next calling of OpenTag
   will cause that parser opens next tag that follows this tag. Function returns false, when tag has not been
   opened, because his name was not equal to requested name. In this case, next calling of
   OpenTag will cause that parser will try to open this tag.
   */
  bool OpenTag(const char *tagName);
  ///Closes tag
  /**
  Function closes the tag. Parser doesn't check proper tag nesting, so it is your job to
  check, if tags are closed in reverse order of openning. Function discards any data that 
  remained until closing tag. Then function finds requested closing tag. Searching
  causes that all unwanted tags will be discarded.

  If OpenTag function has opened a single tag, function CloseTag would only mark the tag
  as closed.
  @param tagName Name of the tag to close
  @return true, function succesfully closes the tag, false - an error occured (tag has not been found).

  */
  bool CloseTag(const char *tagName);

  ///Function opens an attribute.
  /**
  Function searchs and opens the attribute. You can work with attributes, until you starts reading
  the content of tag. You also cannot do nesting with this function.
  @param name Name of the attribute to open.
  @return Function returns true, when attributte has been found and opened.
  
  @note You can open one attribute multiple-times. Everytime you get the same data.
  */
  bool OpenAttribute(const char *name);

  ///Function closes an attribute
  /**
  Function closes an opened attribute. You must supply name of attribute to check, if
  this attribute can be closed. Function discards any unread data in stream.

  @param name Name of the attribute to close
  @return Function returns true, when attribute has been closed.
  */
  
  bool CloseAttribute(const char *name);

  ///Function check, whether OpenAttribute function can be succesfully handled
  /**
  @return Function returns true, when parser can still open an attribute using the OpenAttribute 
	function. If function returns false, parser already discards the tag, and now processing the
	stream.
  */
  bool CanReadAttribute() const {return _inTag;} 

  ///Function check, whether needs to close attribute before any next action
  /**
  @return Function returns true, when parser has openned attribute and any reading reads data
  from attribute.
  */
  bool NeedCloseAttribute() const {return _inTag && _attribRdPos!=-1;}
  

  ///Reads next char
  /**
  @return <0-65535> Unicode character, or -1 end of stream.
  @note: Function is not processing character encoding. In most of cases,
	  function returns ascii codes of reading character. But &#xxx; sequence is also
	  accepted, and supplied number can break ascii code range.
  */
  int GetChar();
  
  ///Reads buffer
  /**
  Function allows to use XMLParser as stream to next parser. 
  @param array pointer to array of items to fill with data
  @param toread count of items to read
  @param wasread count of items that was read. Zero is used as EOF.
  @return function returns true, when no error has occurred. 
  */
  template <class Item>
  bool ReadStream(Item *array, size_t toread, size_t &wasread)
  {
	wasread=toread;
	for (size_t sz=0;sz<toread;sz++)
	{
	  int i=GetChar();
	  if (i==-1) {wasread=sz;break;}
	  array[sz]=(Item)i;
	}
	return true;
  }

  ///Peeks active tag.
  bool PeekTag(char *buff, size_t size);

};

#endif