#ifndef _TEXTCOMMONHEADER
#define _TEXTCOMMONHEADER

#pragma once

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <malloc.h>
#include <tchar.h>
#ifndef AssertDesc
#define AssertDesc(x,y) assert(x)
#endif

typedef _TCHAR CharT;


#endif