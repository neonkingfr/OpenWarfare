// WString.cpp: implementation of the WString class.
//
//////////////////////////////////////////////////////////////////////

#define WIN32_LEAN_AND_MEAN
#include "TextCommonHeader.h"
#include <stdio.h>
#include <windows.h>
#include "TextClass.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


int Text::FormatV(const CharT * format, va_list lst)
{
  size_t curSize=4096;
  int written;
  do
  {
	_ref=new (NULL,curSize) TextProxy;
	CharT * str=const_cast<CharT * >(_ref->GetStringFromMemBlock());
	written=_vsntprintf(str,curSize,format,lst);
	if (written<0)
	{
	  curSize*=2;
	  delete _ref;
	}
  }
  while (written<-1);
  _ref->RecalcLength();
  _ref->AddRef();
  return written;
}

int Text::Format(const CharT * format, ...)
{
  va_list valst;
  va_start(valst,format);
  return FormatV(format,valst);
}


int Text::ScanStringV(const CharT * format, va_list lst) const
{
  unsigned long *ptr=(unsigned long *)lst;
  return _stscanf(GetString(),format,ptr[0],ptr[1],ptr[2],ptr[3],ptr[4],ptr[5],ptr[6],ptr[7],ptr[8],ptr[9],
									ptr[10],ptr[11],ptr[12],ptr[13],ptr[14],ptr[15],ptr[16],ptr[17],ptr[18],ptr[19]);

}

int Text::ScanString(const CharT * format, ...) const
{
  va_list valst;
  va_start(valst,format);
  return ScanStringV(format,valst);
}

bool Text::ReplaceOnce(const Text &findWhat,const Text &replaceWith)
{
  int pos=Find(findWhat);
  if (pos==-1) return false;
  (*this)=Left(pos)+replaceWith+Right(pos+replaceWith.GetLength());
  return true;
}

bool Text::ReplaceAll(const Text &findWhat,const Text &replaceWith)
{
  Text process=*this;
  Text result;
  int pos;
  bool processed=false;
  while ((pos=process.Find(findWhat))!=-1)
  {
	result=result+process.Left(pos)+replaceWith;
	process=process.Right(pos+findWhat.GetLength());
	processed=true;
  }
  *this=result+process;
  return processed;
}

Text Text::TrimLeft() const
{
  return TrimLeft(_T(" \r\n\t"));
}

Text Text::TrimRight() const
{
  return TrimRight(_T(" \r\n\t"));
}

Text Text::TrimLeft(CharT * trimChars) const
{
  const CharT * proStr=GetString();
  int p=0;
  while (proStr[p] && _tcschr(trimChars,proStr[p]!=NULL)) p++;
  return Right(p);
}

Text Text::TrimRight(CharT * trimChars) const
{
  const CharT * proStr=GetString();
  int p=GetLength()-1;
  while (p>=0 && _tcschr(trimChars,proStr[p]!=NULL)) p--;
  return Left(p+1);
}

Text Text::Upper() const
{
  return Text(new TextProxy(TextProxy(_ref,TextProxy::EfUpper)));
}

Text Text::Lower() const
{
  return Text(new TextProxy(TextProxy(_ref,TextProxy::EfLower)));
}

Text Text::Reverse() const
{
  return Text(new TextProxy(TextProxy(_ref,TextProxy::EfReverse)));
}

Text Text::Effect(ITextEffect *effect) const
{
  return Text(new TextProxy(TextProxy(_ref,effect)));
}

Text Text::Format(size_t space, FormatAlign align, bool crop, CharT fillchar)
{
  int len=GetLength();
  if (len>space)
  {
	if (!crop) return *this;
	switch (align)
	{
	case FormatAlignLeft: return Mid(0,space);
	case FormatAlignRight: return Mid(len-space,space);
	case FormatAlignCenter: return Mid((len-space)/2,space);
	};
	return *this;
  }
  else if (len<space)
  {
	int leftsp;
	int rightsp;
	switch(align)
	{
	case FormatAlignLeft: leftsp=0;rightsp=space-len;break;
	case FormatAlignRight: leftsp=space-len;rightsp=0;break;
	case FormatAlignCenter: leftsp=(space-len)/2;rightsp=space-len-leftsp;break;
	};
	Text left;
	Text right;
	if (leftsp)
	{
	  CharT *z=left.CreateBuffer(leftsp);
	  for (int i=0;i<leftsp;i++) z[i]=fillchar;
	  left.UnlockBuffer(leftsp);
	}
	if (rightsp)
	{
	  CharT *z=right.CreateBuffer(rightsp);
	  for (int i=0;i<rightsp;i++) z[i]=fillchar;
	  right.UnlockBuffer(rightsp);
	}
  Text res=left+*this+right;
	return res;
  }
  else
	return *this;

}


void Text::ReadFromStream(size_t (*streamReader)(CharT * buffer, size_t bufferLen, void *context),void *context)
{
  _ref->Release();
  _ref=NULL;
  CharT  buff[256];
  size_t rd;
  while ((rd=streamReader(buff,sizeof(buff)/sizeof(CharT ),context))!=0)
  {
	*this=*this+Text(buff,rd);
  }
}


#ifdef _UNICODE
MBText::MBText(const char *mbtext, size_t charsz,long codepage)
{
  if (codepage==-1) codepage=CP_THREAD_ACP;
  _codePage=codepage;
  if (mbtext==0) {_ref=0;return;}
  size_t mbsz=charsz==-1?strlen(mbtext):charsz;
  if (mbsz==0) {_ref=0;return;}
  size_t reqSz=MultiByteToWideChar(codepage,0,mbtext,mbsz,0,0);
  if (reqSz==0) {_ref=0;return;}
  _ref=new(0,reqSz) TextProxy;
  CharT *buff=const_cast<CharT *>(_ref->GetStringFromMemBlock());
  MultiByteToWideChar(codepage,0,mbtext,mbsz,buff,reqSz);
  buff[reqSz]=0;
  _ref->RecalcLength();
  _ref->AddRef();
}

const char *MBText::GetString() const
{
  if (_ref==0) return "";
  size_t reqSz=WideCharToMultiByte(_codePage,0,Text::GetString(),GetLength(),0,0,0,0);
  if (_ref->_blockData2 & 2) return reinterpret_cast<const char *>(_ref->GetStringFromMemBlock()+GetLength()+1);
  TextProxy *nwproxy=new(0,GetLength()+1+(reqSz+1)/sizeof(CharT)) TextProxy;
  CharT *res=const_cast<CharT *>(nwproxy->GetStringFromMemBlock());
  char *mbtext=reinterpret_cast<char *>(res+GetLength()+1);
  _tcscpy(res,_ref->GetStringFromMemBlock());
  nwproxy->_blockData2|=2;
  WideCharToMultiByte(_codePage,0,res,GetLength(),mbtext,reqSz,0,0);
  mbtext[reqSz]=0;
  
  _ref->Release();
  _ref=nwproxy;
  _ref->AddRef();
  _ref->RecalcLength();
  
  return mbtext;
}


char *MBText::LockBuffer()
{
  if (_ref==0) return 0;
  size_t reqSz=WideCharToMultiByte(_codePage,0,Text::GetString(),GetLength(),0,0,0,0);
  TextProxy *nwproxy=new(0,reqSz/2+(reqSz<2)) TextProxy;
  CharT *res=const_cast<CharT *>(nwproxy->GetStringFromMemBlock());
  char *mbtext=reinterpret_cast<char *>(res);
  WideCharToMultiByte(_codePage,0,_ref->GetStringFromMemBlock(),GetLength(),mbtext,reqSz,0,0);

  _ref->Release();
  _ref=nwproxy;
  _ref->AddRef();
  _ref->Lock();

  return mbtext;
}

char *MBText::CreateBuffer(size_t sz)
{
  TextProxy *nwproxy=new(0,sz/2+(sz<2)) TextProxy;
  CharT *res=const_cast<CharT *>(nwproxy->GetStringFromMemBlock());
  char *mbtext=reinterpret_cast<char *>(res);

  _ref->Release();
  _ref=nwproxy;
  _ref->AddRef();
  _ref->Lock();

  return mbtext;
}

char *MBText::CreateBufferCopy(size_t sz)
{
  MBText save=*this;
  char *buff=CreateBuffer(sz);
  const char *copy=save;
  strncpy(buff,copy,sz);
  return buff;
}

void MBText::UnlockBuffer(int sz)
{
  _ref->Unlock();
  const char *data=reinterpret_cast<const char *>(_ref->GetStringFromMemBlock());
  (*this)=(MBText(data,sz,_codePage));
}

void MBText::SetCodePage(int codePage)
{
  if (codePage==-1) codePage=CP_THREAD_ACP;
  _codePage=codePage;
}


#endif _UNICODE

