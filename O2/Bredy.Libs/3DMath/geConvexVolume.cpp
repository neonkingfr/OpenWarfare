// geConvexVolume.cpp: implementation of the geConvexVolume class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "geConvexVolume.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

geBoundingBox::geBoundingBox(const geVector4 *vlist8, float signum)
{
  planes[0]=gePlaneGen(vlist8[0],vlist8[1],vlist8[2]);
  planes[1]=gePlaneGen(vlist8[2],vlist8[1],vlist8[5]);
  planes[2]=gePlaneGen(vlist8[1],vlist8[0],vlist8[4]);
  planes[3]=gePlaneGen(vlist8[4],vlist8[0],vlist8[3]);
  planes[4]=gePlaneGen(vlist8[7],vlist8[3],vlist8[2]);
  planes[5]=gePlaneGen(vlist8[7],vlist8[6],vlist8[5]);
  this->signum=signum;
}

geBoundingBox::geBoundingBox(const geVector4 &lv, const geVector4& pv, float signum)
{
  planes[0]=gePlaneGen(lv,geVector4(0,0,-1));
  planes[1]=gePlaneGen(lv,geVector4(0,-1,0));
  planes[2]=gePlaneGen(lv,geVector4(-1,0,0));
  planes[3]=gePlaneGen(pv,geVector4(0,0,1));
  planes[4]=gePlaneGen(pv,geVector4(0,1,0));
  planes[5]=gePlaneGen(pv,geVector4(1,0,0));
  this->signum=signum;
}

geBoundingBox::geBoundingBox(const gePlaneGen *plist6, float signum)
{
  for (int i=0;i<6;i++) planes[i]=gePlaneGen(plist6[i]);
  this->signum=signum;  
}

geBoundingBox::geBoundingBox(const gePlane2V *plist6, float signum)
{
  for (int i=0;i<6;i++) planes[i]=gePlaneGen(plist6[i]);
  this->signum=signum;
}

bool geBoundingBox::TestPoint(const geVector4& pt)
{
  return ::geTestPoint(pt,planes,6);
}

bool geBoundingBox::TestLine(const geLine& pt, TestInfo* info)
{  
  return ::geTestLine(pt,planes,6,GetSign(),info);
}

geVector4 geBoundingBox::GetCenter()
{
  geVector4 suma(0,0,0);
  for (int i=0;i<6;i++)
  {
    geLine ln;
    ln.p=geVector4(0,0,0);
    ln.r=geVector4(planes[i].a,planes[i].b,planes[i].c);
    suma+=ln.GetPoint(planes[i].d);
  }
  suma/=6;
  return suma;
}

bool geTestPoint(const geVector4 &pt, const gePlaneGen *planes, int count)
{
  if (count<=0) return false;
  bool wrk=planes->Side(pt);count--;planes++;
  while (count--)
  {
    if (wrk!=planes->Side(pt)) return false;
    planes++;
  }  
  return true;
}

bool geTestLine(const geLine& ln, const gePlaneGen *planes, int count, float sign, geConvexVolume::TestInfo *nfo)
{
  if (sign==0) return false;
  const gePlaneGen *front;
  const gePlaneGen *back;
  float enterpt;
  float exitpt;
  float prac;
  bool frstp=true; 
  bool frstn=true;
  bool pttest=false;  
  for (int i=0;i<count;i++)
  {
    float side=planes[i].Intersection(ln,prac)*sign;	
    if (side==0)
    {
      bool sd=planes[i].Side(ln.p);
      if ((sd && sign>0) || (!sd && sign<0)) return false;	  
    }
    else if (side<0)
    {
      if (frstn || prac<exitpt) 
      {exitpt=prac;back=planes+i;frstn=false;}
    }
    else if (side>0)
    {
      if (frstp || prac>enterpt) 
      {enterpt=prac;front=planes+i;frstp=false;}
    }
    if (!frstn && !frstp && enterpt>exitpt) return false;
  }
  if (nfo)
  {
    if (nfo->flags & GECV_ENTERPT) nfo->enterpt=enterpt;
    if (nfo->flags & GECV_EXITPT) nfo->exitpt=exitpt;
    if (nfo->flags & GECV_ENTERPL) nfo->enterplane=*front;
    if (nfo->flags & GECV_EXITPL) nfo->exitplane=*back;
  }
  return true;
}

float geTestConvexVolume(const gePlaneGen *planes, int count)
{
  if (planes==NULL || count<=3) return 0; //eliminuj chybne parametry - vraci 0
  geVector4 *ptlist=new geVector4[3*count]; //alokuj prostor pro pruseciky
  int *kstore=new int[count];			  //alokuj pomocny datovy prostor
  int ptusage=0;						  //ptusage pocet zaznamu v datovem prostoru
  for (int i=0;i<count;i++)				  //pro vsechny dvojice rovinach
  {
    for (int j=i+1;j<count;j++) 
    {
      geLine q=planes[i].Intersection(planes[j]); //spocitej primku, ktera lezi na obou rovinach
      float f;
      int k,l;
      if (q.IsFinite())					  //pokud primka existuje
      {
        ptusage=0;
        for (k=j+1;k<count;k++)			  //pro libovolnou zbyvajici rovinu
        {
          if (planes[k].Intersection(q,f)!=0.0f)  //najdi prusecik s primkou a pokud existuje
          {
            ptlist[ptusage]=q.GetPoint(f);	 //vloz prusecik do pole
            kstore[ptusage]=k;				 //oznac, se kterou rovinou to prusecik je
            ptusage++;						 //zvys index poctu zaznamu
            if (ptusage==count) 
              ptusage=0; //dosla-li pamet, nektere pruseciky holt zahodime
          }
        }
        for (k=0;k<ptusage;k++)			  //a ted, pro vsechny nalezene pruseciky
        {
          char val=0;					  //val je nalezena strana, zatim zadna
          int nl=kstore[k];				  //nl je index roviny, ktera nas nezajima
          for(l=0;l<count;l++) if (l!=i && l!=j && l!=nl) //pro zbyvajici roviny
          {
            if (planes[l].Side(ptlist[k]))	//odtestuj stranu pruseciku
            if (!val) val=1;else if (val!=1) break; //zkontroluj, zda nalezena strana je stejna jako predchozi
            else;else
              if (!val) val=-1;else if (val!=-1) break; //a pokud ne, prusecik nemuzeme pouzit
              
          }
          if (l==count)		//byl nalezen spravny prusecik
          {
            delete [] ptlist;	//uvolni pamet
            delete [] kstore;
            return (float)val;	//vrat informaci o strane
          }
        }
      }
    }
  }
  //Sem program zabloudi, pokud se nejedna o konvexni objem
  delete [] ptlist;	//uvolni pamet
  delete [] kstore;	
  return 0.0f; //vrat chybu
}

/*
geVector4 geFindPointAtCV(const gePlaneGen *planes, int count, const geVector4& pt, float sign)
  {
  if (count<4 || planes==NULL) return pt;
  geVector4 *vxlist=(geVector4 *)alloca(sizeof(geVector4)*count);
  int *info1=(int *)alloca(sizeof(int)*count);
  int *info2=(int *)alloca(sizeof(int)*count);
  int *info3=(int *)alloca(sizeof(int)*count);
  
	
  }
*/
geBoundingSphere::geBoundingSphere(const geVector4& pt,float radius):itransf(true),
center(pt)
{
  //  geMatrix4 trans;
  //  trans.Scale(radius,radius,radius).TranslateC(pt);
  //  trans=trans.Invert();
  itransf.a11=itransf.a22=itransf.a33=1.0f/radius;
  itransf.a41=-pt.x/radius;
  itransf.a42=-pt.y/radius;
  itransf.a43=-pt.z/radius;
}

geBoundingSphere::geBoundingSphere(const geMatrix4 &trans):
itransf(trans.Invert()),center(trans.pos)
{
}

bool geBoundingSphere::TestPoint(const geVector4& pt)
{
  geVector4 qt;
  itransf.Transform3T(pt,qt);
  return qt.Module2()<1.0f;
}

/*

  px*px+py*py+pz*pz-1=0;
  (Ax+k*rx)^2+(Ay+k*ry)^2+(Az+k*rz)^2-1=0;
  (Ax*Ax+2*Ax*rx*k+rx*rx*k*k)+(Ay*Ay+2*Ay*ry*k+ry*ry*k*k)+(Az*Az+2*Az*rz*k+rz*rz*k*k)-1=0;
  a   = rx*rx+ry*ry+rz*rz
  b/2 = (Ax*rx+Ay*ry+Az*rz)
  c   = Ax*Ax+Ay*Ay+Az*Az-1
*/


bool geBoundingSphere::TestLine(const geLine& pt, TestInfo* info)
{
  geVector4 A,r;
  itransf.Transform3T(pt.p,A);
  itransf.Transform3T(pt.p+pt.r,r);
  r-=A;
  float a=r.Module2();
  float bp=2*A*r;
  float c=A.Module2()-1;
  float d=bp*bp-4*a*c;
  if (d<0.0) return false;
  if (info==NULL) return true;
  d=(float)sqrt(d);
  float k1=(-bp+d)/(2*a);
  float k2=(-bp-d)/(2*a);
  if (k1>k2) 
  {float k=k1;k1=k2;k2=k;}
  if (info->flags & GECV_ENTERPT) info->enterpt=k1;
  if (info->flags & GECV_EXITPT) info->exitpt=k2;
  if (info->flags & GECV_ENTERPL)
  {
    geVector4 P(pt.GetPoint(k1));
    geVector4 s(P-center);
    info->enterplane=gePlaneGen(P,s);
  }
  if (info->flags & GECV_EXITPL)
  {
    geVector4 P(pt.GetPoint(k2));
    geVector4 s(P-center);
    info->exitplane=gePlaneGen(P,s);
  }
  return true;
}

