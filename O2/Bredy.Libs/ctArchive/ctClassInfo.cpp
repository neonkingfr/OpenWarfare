// ctClassInfo.cpp: implementation of the ctClassInfo class.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <stdlib.h>
#include "stdafx.h"
#include "ctClassInfo.h"

#define CTERROR "ctClassInfo Error"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

long ctClassInfo::CalculateDuid(const char *name)
{
  unsigned long duid=0;
  while (*name)
  {
    duid=duid*31+(unsigned char)(*name);
    name++;
  }
  return (long) duid;
}

ctClassInfo::ctClassInfo(long cd, long dd, ctCreateClass cr, ctDestroyClass ds, const char *classname):
cuid(cd),duid(CalculateDuid(classname)+dd),creatclass(cr),destroy(ds),classname(classname),userptr(NULL)
{
  if (cuid==0) cuid++;
  if (duid==0) duid++;
  ctClassTable::Add(this);
}

ctLongLongTable  *ctClassTable::cuidtab=NULL;
ctLongLongTable *ctClassTable::duidtab=NULL;


void  ctClassTable::Add(ctClassInfo *nfo)
{
  if (cuidtab==NULL) cuidtab=new ctLongLongTable;
  if (duidtab==NULL) duidtab=new ctLongLongTable;
  if ((*duidtab)[nfo->GetDUID()])
  {
    char buff[1024];
    sprintf(buff,"Class ID colision! Two classes has one ID: %s and %s", GetClass(nfo->GetDUID())->GetClassName(),nfo->GetClassName());
    #ifdef _WINUSER_
    MessageBox(NULL,buff,CTERROR,MB_OK|MB_SYSTEMMODAL);
    #else
    fprintf(stderr,buff);
    #endif
    abort();
  }
  if ((*cuidtab)[nfo->GetCUID()])
  {
    char buff[1024];
    sprintf(buff,"Class %s is not serializable, or not define Serialize function. "
    "This function must be defined even if do nothing or only calls parent function.",
    nfo->GetClassName());
    #ifdef _WINUSER_
    MessageBox(NULL,buff,CTERROR,MB_OK|MB_SYSTEMMODAL);
    #else
    fprintf(stderr,buff);
    #endif
    abort();
  }
  cuidtab->Add(nfo->GetCUID(),(long)nfo);
  duidtab->Add(nfo->GetDUID(),(long)nfo);
}

ctClassInfo *ctClassTable::GetClass(long tagid)
{
  if (duidtab==NULL) return NULL;
  long p=(*duidtab)[tagid];
  return (ctClassInfo *)p;
}

ctClassInfo *ctClassTable::GetClass(CTLPSC ptr)
{
  if (cuidtab==NULL) return NULL;
  const char *name=typeid(*ptr).name();
  long p=(*cuidtab)[(long)((void *)(name))];
  return (ctClassInfo *)p;  
}

void ctLostDataClass::Enum(bool (*EnumProc)(CTLPSC cls,void *context),void *context)
{
  Data *q=list;
  while (q && EnumProc(q->cls,context)==true) q=q->next;
}

void ctLostDataClass::Add(CTLPSC cls)
{
  Data *z=new Data;
  z->cls=cls;
  z->next=list;
  list=z;
}

ctLostDataClass ctLostData;