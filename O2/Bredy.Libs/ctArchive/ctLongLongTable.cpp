// ctLongLongTable.cpp: implementation of the ctLongLongTable class.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ctLongLongTable.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ctLongLongTable::ctLongLongTable()
{
  table=NULL;
  size=0;
  Expand();
}

ctLongLongTable::~ctLongLongTable()
{
  delete [] table;
}

void ctLongLongTable::Expand()
{
  int nwsize=size*2;
  if (nwsize<11) nwsize=11;
  nwsize++;
  LongLong *old=table;
  table=new LongLong[nwsize];
  memset(table,0,sizeof(*old)*nwsize);
  int p=size;
  size=nwsize;
  for (int i=0;i<p;i++) 
    if (old[i].key)	  
      Add(old[i].key,old[i].value);
  delete [] old;
}

bool ctLongLongTable::Add(long key, long value)
{
  long index=(unsigned long)key%size;
  long endrule=index;
  bool replac=false;
  do
  {
    if (table[index].key==0) break;
    if (table[index].key==key) 
    {replac=true;break;}
    index++;
    if (index>=size) index=0;
  }
  while (index!=endrule);
  if (table[index].key && table[index].key!=key) 
  {
    Expand();
    return Add(key,value);	
  }
  table[index].key=key;
  table[index].value=value;
  return replac;
}

long ctLongLongTable::operator [](long key)
{
  if (size==0) return 0;
  long index=(unsigned long)key%size;
  long endrule=index;
  do
  {
    if (table[index].key==0) return 0;
    if (table[index].key==key) return table[index].value;
    index++;
    if (index>=size) index=0;
  }
  while (index!=endrule);
  return 0;
}

ctLongLongTable::ctLongLongTable(const ctLongLongTable &other)
{
  table=new LongLong[other.size];
  size=other.size;
  memcpy(table,other.table,size*sizeof(*table));
}

long ctLongLongTable::FindValueIndex(long value, long &tabindex)
{
  tabindex++;
  while (tabindex<size && table[tabindex].value!=value) tabindex++;
  if (tabindex<size) return table[tabindex].key;
  else return 0;
}

void ctLongLongTable::Clear()
{
  memset(table,0,sizeof(*table)*size);
}

