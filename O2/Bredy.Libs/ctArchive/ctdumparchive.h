// ctdumparchive.h: interface for the ctDumpArchive class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTDUMPARCHIVE_H__E3409A56_C4AB_41A2_A054_BF7AA13AD259__INCLUDED_)
#define AFX_CTDUMPARCHIVE_H__E3409A56_C4AB_41A2_A054_BF7AA13AD259__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ctArchive.h"

class ctDumpArchive : public ctArchiveOut  
{
  ostream &out;
  DWORD voffset;
  DWORD frame;
  void DumpOut(ostream &out, DWORD offset, DWORD size, const char *type, const char *comment, const char *lead="");
  public:
    ctDumpArchive(ostream &file,unsigned long flags=0);
    
    virtual CTLPSC serial(CTLPSC src); //serializes class
    virtual bool ver(unsigned long version);
    virtual bool check(const char *tagId);
    
    virtual void serial(void *ptr, unsigned long size); //makes binary serialization
    virtual ctArchive& operator>>=(bool &val);
    virtual ctArchive& operator>>=(char &val);
    virtual ctArchive& operator>>=(unsigned char &val);
    virtual ctArchive& operator>>=(short &val);
    virtual ctArchive& operator>>=(unsigned short &val);
    virtual ctArchive& operator>>=(int &val);
    virtual ctArchive& operator>>=(unsigned int &val);
    virtual ctArchive& operator>>=(long &val);
    virtual ctArchive& operator>>=(unsigned long &val);
    virtual ctArchive& operator>>=(float &val);
    virtual ctArchive& operator>>=(double &val);
    
    virtual bool operator<< (bool val) 
    {(*this)>>=val;return val;}
    virtual char operator<< (char val) 
    {(*this)>>=val;return val;}
    virtual unsigned char operator<< (unsigned char val) 
    {(*this)>>=val;return val;}
    virtual short operator<< (short val) 
    {(*this)>>=val;return val;}
    virtual unsigned short operator<< (unsigned short val) 
    {(*this)>>=val;return val;}
    virtual int operator<< (int val) 
    {(*this)>>=val;return val;}
    virtual unsigned int operator<< (unsigned int val) 
    {(*this)>>=val;return val;}
    virtual long operator<< (long val) 
    {(*this)>>=val;return val;}
    virtual unsigned int operator<< (unsigned long val) 
    {(*this)>>=val;return val;}
    virtual float operator<< (float val) 
    {(*this)>>=val;return val;}
    virtual double operator<< (double val) 
    {(*this)>>=val;return val;}
    
    virtual void reserve(unsigned long bytes); //reserves/skips number of bytes
    
    virtual char *operator<< (const char *str);
    virtual short *operator<< (const short *str);
    
    ctArchive& operator>>=(CTLPSTR& str)
    {str=(*this)<<str;return *this;}
    ctArchive& operator>>=(CTLPSTRW& str)
    {str=(*this)<<str;return *this;}
    
};

#endif // !defined(AFX_CTDUMPARCHIVE_H__E3409A56_C4AB_41A2_A054_BF7AA13AD259__INCLUDED_)
