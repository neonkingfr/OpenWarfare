// ctArchiveMem.h: interface for the ctArchiveOutMem class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ctArchIVEMem_H__CA7521ED_FAC7_4238_94F7_C19274CD9905__INCLUDED_)
#define AFX_ctArchIVEMem_H__CA7521ED_FAC7_4238_94F7_C19274CD9905__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class ctArchiveOutMem : public ctArchiveOut
{
  char *buffer;
  int size;
  int alloc;
  bool Expand(int s);
  public:
    ctArchiveOutMem(DWORD flags=0):ctArchiveOut(flags) 
    {buffer=0;size=alloc=0;}
    virtual ~ctArchiveOutMem() 
    {free(buffer);}
    virtual void serial(void *ptr, unsigned long size);
    virtual void reserve(unsigned long bytes);
    char *GetBuffer() 
    {return buffer;}
    int GetBufferSize() 
    {return size;}
};

class ctArchiveInMem: public ctArchiveIn
{
  protected:
    char *buffer;
    int size;
    int pos;
  public:
    ctArchiveInMem(char *buf, int siz, DWORD flags=0):ctArchiveIn(flags) 
    {buffer=buf;size=siz;pos=0;}
    void Rewind() 
    {pos=0;}
    virtual void serial(void *ptr, unsigned long size);
    virtual void reserve(unsigned long bytes);  
    
};

class ctArchiveSimpleCounting: public ctArchiveOut
{
  int size;
  public:
    ctArchiveSimpleCounting(DWORD flags=0):ctArchiveOut(flags) 
    {mode|=CTAF_COUNTING;size=0;}
    virtual void serial(void *ptr, unsigned long size) 
    {this->size+=size;}
    virtual void reserve(unsigned long bytes) 
    {size+=bytes;}
    int GetCount() 
    {return size;}
};

class ctArchiveInReg: public ctArchiveInMem
{
  public:
    ctArchiveInReg(HKEY key, const char *value, DWORD flags=0):ctArchiveInMem(NULL,0,flags)
    {
      DWORD sz=0;
      if (RegQueryValueEx(key,value,NULL,NULL,NULL,&sz)!=ERROR_SUCCESS) 
      {err(-50);return;}
      buffer=new char[sz];
      if (RegQueryValueEx(key,value,NULL,NULL,(LPBYTE)buffer,&sz)!=ERROR_SUCCESS) 
      {err(-50);return;}
      size=sz;
    }
    ~ctArchiveInReg()
    {
      delete [] buffer;
    }
};

class ctArchiveOutReg: public ctArchiveOutMem
{
  char *v;
  HKEY k;
  public:
    ctArchiveOutReg(HKEY key, const char *value, DWORD flags=0):ctArchiveOutMem(flags)
    {
      k=key;
      v=strdup(value);
    }
    ~ctArchiveOutReg()
    {
      RegSetValueEx(k,v,0,REG_BINARY,(CONST LPBYTE)GetBuffer(),GetBufferSize());
      free(v);
    }
};

#endif // !defined(AFX_ctArchIVEMem_H__CA7521ED_FAC7_4238_94F7_C19274CD9905__INCLUDED_)
