// ctLongLongTable.h: interface for the ctLongLongTable class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTLONGLONGTABLE_H__1E116B7E_A10E_405E_B20F_187C41757A81__INCLUDED_)
#define AFX_CTLONGLONGTABLE_H__1E116B7E_A10E_405E_B20F_187C41757A81__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class ctLongLongTable  
{
  struct LongLong
  {
    long key;
    long value;
  };
  LongLong *table;
  int size;
  public:
    void Clear();
    ctLongLongTable(const ctLongLongTable& other);
    bool Add(long key, long value);
    ctLongLongTable();
    virtual ~ctLongLongTable();
    long operator [](long key);
    long FindValueIndex(long value, long &tabindex);
    int GetSize() 
    {return size;}
    long GetValueAtIndex(int index) 
    {return table[index].value;}
    long GetKeyAtIndex(int index) 
    {return table[index].key;}
    
  protected:
    void Expand();
};

#endif // !defined(AFX_CTLONGLONGTABLE_H__1E116B7E_A10E_405E_B20F_187C41757A81__INCLUDED_)
