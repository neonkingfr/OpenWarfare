// ROCheck.cpp : Defines the entry point for the application.
//

#include <windows.h>
#include "ROCheck.h"

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
 	ROCheckResult res=TestFileRO(NULL,"StdAfx.h");
    if (res==ROCHK_FileOK) MessageBox(NULL,"Soubor je R/W nebo neexistuje",NULL,MB_OK);
    if (res==ROCHK_FileRO) MessageBox(NULL,"Soubor je R/O, nelze zapisovat",NULL,MB_OK);
    if (res==ROCHK_FileSaveAs) MessageBox(NULL,"Soubor je R/O, uzivatel vybral SaveAs",NULL,MB_OK);

	return 0;
}



