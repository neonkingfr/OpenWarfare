#pragma once



namespace MVC
{

  ///Singlethread lock
  class SingleThread
  {
  public:
    void Lock() {}
    void Unlock() {}
  };


  ///Multithread Lock
  template<class BlockerType>
  class MultipleThreads
  {
    BlockerType _locker;
  public:
    void Lock() {_locker.Lock();}    
    void Unlock() {_locker.Unlock();}    
  };

  ///Forward declaration of Model class
  template<class DefMVC>
  class Model;

  ///Forward declaration of View class
  template<class DefMVC>
  class View;


  ///Defines MVC configuration
  /**
  * Best usage of this struct is use typedef to define configuration
  * for your MVC system. MVC is defined by serveral parameters
  * @param IModel Model interface, that contains interface for the model
  *  Interface must be divided into two parts. First part is separate
  *  interface that defines actions on the model. Second part extends first by
  *  query commands that should not to change the model.
  *  Model interface have to define type Actions that references base
  *  class which is first part of the model (actions)
  * @param ThreadLock Defines class to provide accessing in multiple thread environment
  *   You can use SingleThread or MultipleThreads
  */
  template<class IModel, class ThreadLock>
  struct DefineMVC
  {
    ///Alias for this class
    typedef DefineMVC<IModel,ThreadLock> ThisType;
    ///Definition of Model class in current configuration
    typedef Model<ThisType> CurModel;
    ///Definition of View class in current configuration
    typedef View<ThisType> CurView;
    ///Definition of IModel base interface
    typedef IModel IModelBase;
    ///Definition of action interface
    typedef typename IModel::Actions IModelActions;
    ///Definition of class that handles multithread locking
    typedef ThreadLock Lock;
  };

  template<class T>
  class IModelViewControlerBase: public T
  {
  public:
    ///Starts the batch of commands 
    /**
    * Document starts a new batch
    * @param startBatch, pointer to pointer that receives begin of this batch. If this parameter
    * is NULL, function will not store the begin of group.
    * Stored value has no meaning for user. It used internally. Use stored value to RollbackTo
    * function.
    * @retval true success
    * @retval false failed
    *
    *
    * @note View receives this notification, when document starts new group. It can
    * use value of pointer to track creating and rollbacking groups
    *
    */
    virtual bool BeginBatch(void **startBatch=0) {return false;}
    ///Notification from document, that batch ended, and view can present results
    /**
    * Document closes a batch.
    *
    * View can take use this to redraw itself
    */
    virtual bool EndBatch() {return false;}

    ///Function rollbacks current batch till stored pointer
    /**
    * @param startofbatch pointer that contains value stored by BeginBatch
    */
    virtual bool RollbackBatch(void *startofbatch) {return false;}
  };

  ///Class provides simple linked-list
  /**
  * @param ItemPtr defines class that implements pointer to item. It can be common pointer, or smart pointer 
  */
  template<class ItemPtr>
  class ItemList
  {
    ///First item ptr
    ItemPtr _ptr;
  public:
    ///Inicializes empty list
    ItemList():_ptr(0) {}
    ///Disconnects list, will not delete items
    ~ItemList()
    {
      while (_ptr)
      {      
        ItemPtr a=_ptr;
        _ptr=a;
        a->_nextItem=0;
      }
    }

    ///Adds item into list
    /**
    * @param item pointer into item that should be added
    * @retval true item has been added
    * @retval false cannot add, item is already referenced by another list
    */
    bool Add(ItemPtr item)
    {
      Assert(item->_nextItem==0);//item is already added
      if (item->_nextItem) return false;
      item->_nextItem=_ptr;
      _ptr=item;
      return true;
    }

    ///Removes item from list
    /**
    * @param item pointer to item that should be removed
    * @retval true item has been removed
    * @retval false item has not been found
    */
    bool Remove(ItemPtr item)
    {
      if (_ptr==item)
      {
        _ptr=item->_nextItem;
        item->_nextItem=0;
        return true;
      }
      else
      {
        ItemPtr a=_ptr;
        while (a && a->_nextItem!=item) a=a->_nextItem;
        if (a->_nextItem)
        {
          a->_nextItem=item->_nextItem;
          item->_nextItem=0;
          return true;
        }
        else
        {
          return false;
        }
      }
    }

    ///Returns pointer to the first item
    /**
    @retval pointer pointer to the first item
    @retval 0 list is empty
    */
    ItemPtr GetFirstItem() const
    {
      return const_cast<ItemPtr>(_ptr);
    }

    ///Returns pointe to the next item
    /**
    * @param itm Pointer to reference item. If this value is 0, it returns also 0
    * @retval pointer pointe to the next item
    * @retval 0 this has been the last item in list, or itm has been equal to 0.
    */
    static ItemPtr GetNextItem(ItemPtr itm)
    {
      if (itm==0) return 0;
      return itm=itm->_nextItem;
    }

    void SwapLists(ItemList<ItemPtr> &other)
    {
      ItemPtr a=_ptr;
      _ptr=other._ptr;
      other._ptr=a;
    }
  };

  template<class MVCDef>
  class LockableClass: public MVCDef::Lock
  {
  public:
    ///Class handles exclusive access to the object in multi thread environment
    class Lock
    {
      typename MVCDef::Lock &_lock;
    public:
      Lock(typename MVCDef::Lock &lock):_lock(lock) {_lock.Lock();}
      ~Lock() {_lock.Unlock();}
      Lock(const Lock& other):_lock(other._lock) {_lock.Lock();}    
    };

    ///Returns lock object.
    /**
    * During lifetime of this object, model is locked 
    */
    Lock LockObject()
    {
      return Lock(*this);
    }
  };

  

  ///Model in MVC
  template<class MVCDef>
  class Model: public IModelViewControlerBase<typename MVCDef::IModelBase>, public LockableClass<MVCDef>
  {
    ItemList<typename MVCDef::CurView *> _viewList; ///<list of all views
    typename MVCDef::CurView *_nextEnum; ///<next view during forEach    
    unsigned int _batchRecursion; ///<current count of BeginBatch
  public:


    Model():_nextEnum(0) {}
    ~Model() { PrepareDestroy(); }


    ///Used for enumerating view
    /**
    * for(Each x(this);x;x->DoSomething()); 
    */
    class Each
    {
      Model &_model;
      typename MVCDef::CurView *_curView;
    public:
      Each(Model &model):_model(model) 
      {
        static_cast<MVCDef::Lock &>(_model).Lock();
        Assert(_model._nextEnum==0);
        _model._nextEnum=_model._viewList.GetFirstItem();
        _curView=0;
      }

      Each(Model *model):_model(*model) 
      {
        static_cast<MVCDef::Lock &>(_model).Lock();
        Assert(_model._nextEnum==0);
        _model._nextEnum=_model._viewList.GetFirstItem();
        _curView=0;
      }

      ~Each()
      {
        _model._nextEnum=0;
        _model.Unlock();
      }

      bool GetNext()
      {
        if (_model._nextEnum==0) return false;
        _curView=_model._nextEnum;
        _model._nextEnum=_model._viewList.GetNextItem(_model._nextEnum);
        return true;
      }

      typename MVCDef::CurView *GetCur()
      {
        return _curView;
      }

      operator bool()
      {
        return GetNext();
      }

      typename MVCDef::CurView *operator->()
      {
        return GetCur();
      }

      typename MVCDef::CurView &operator *()
      {
        *return GetCur();
      }
    };

  protected:
    friend class View<MVCDef>;

    ///Adds view
    /**
    * Don't call this function directly. Model can define own implementation, but
    * it is strongly recommended to call base implementation at the end of function
    * @param p pointer to view to add
    */
    virtual bool AddView(typename MVCDef::CurView *p)
    {
      Lock x(*this);
      if (p==0) return false;
      return _viewList.Add(p);
    }

    ///Removes the view
    /**
    * Don't call this function directly. Model can define own implementation, but
    * it is strongly recommended to call base implementation at the end of function
    * @param p pointer to view to add
    */
    virtual bool DeleteView(typename MVCDef::CurView *p)
    {
      Lock x(*this);
      if (p==0) return false;
      if (_nextEnum==p) _nextEnum=_viewList.GetNextItem(_nextEnum);
      return _viewList.Remove(p);
    }

    ///Removes all views and prepares model to deletion
    /**
    * It is recommended to call this function in destructor of derived class
    */
    void PrepareDestroy()
    {
      Lock x(*this);
      MVCDef::CurView *v=_viewList.GetFirstItem();
      while (v) //is there any view
      {
        //notify view that document is destroying
        v->OnModelDestroy();
        //view has not been removed, okay, remove it manually
        if (v==_viewList.GetFirstItem()) _viewList.Remove(v);
        //check if there is any additional view
        v=_viewList.GetFirstItem();
      }
    }

    ///returns true, if there is any connected view
    bool AnyView() const
    {
      return _viewList.GetFirstItem()!=0;
    }

    ///returns count of views connected to the model
    /**
    * @param limit Because views are connected through linked list, this operation
    * has linear complexity. If you want to ensure, that count of views is above or below
    * to limit, specify this limit as parameters. Counting stops when total count 
    * reaches the limit.
    *
    * @return Returns count of views connected to the model. If limit specified
    *  function returns limit, when count of views is above the limit
    */
    int GetViewCount(int limit=INT_MAX) const
    {
      int z=0;
      MVCDef::CurView *v=_viewList.GetFirstItem();
      while (v && z<limit)
      {
        z++;
        v=_viewList.GetNextItem(v);
      }
    }


    public:
    virtual bool BeginBatch(void **startBatch=0)
    {
      if (_batchRecursion++==0)  for(Each x(this);x;x->BeginBatch(0));
      return 0;
    }

    virtual bool EndBatch()
    {
      if (--_batchRecursion==0)for(Each x(this);x;x->EndBatch());
      return true;
    }

    virtual bool RollbackTo(void *ptr)
    {
      return false;
    }

    virtual bool UpdateMe(typename MVCDef::CurView *curView)
    {
      if (curView==0)
      {
        for(Each x(this);x;x->UpdateMe(this));
        return true;
      }
      else
        return false;
    }

  };

  ///View
  template<class MVCDef>
  class View: public IModelViewControlerBase<typename MVCDef::IModelActions>
  {
    ///allow access for linked list provider
    friend class ItemList<typename MVCDef::CurView *>;
    ///Pointer to model
    typename MVCDef::CurModel *_curModel;
    ///Pointer to next item in linked list
    typename MVCDef::CurView *_nextItem;

  public:
    View():_curModel(0),_nextItem(0) {}
    ///Destructor automatically removes itself from document
    ~View()
    {
      if (_curModel) DetachModel();
    }

    ///Attaches view to a model
    /**
    * Each view can be attached only once 
    */
    bool AttachModel(typename MVCDef::CurModel *model)
    {
      if (_curModel) DetachModel();
      if (!model->AddView(this)) return false;
      _curModel=model;
      return true;
    }

    ///Detaches view from a model
    bool DetachModel()
    {
      if (_curModel==0) return true;
      if (!_curModel->DeleteView(this)) return false;
      _curModel=0;
      return true;
    }

    ///Called, when document is destroying
    virtual void OnModelDestroy()
    {
      _curModel=0;
    }

    //Returns active model
    typename MVCDef::CurModel *GetModel()
    {
      return _curModel;
    }

    //Returns active model
    const typename MVCDef::CurModel *GetModel() const
    {
      return _curModel;
    }

    virtual bool BeginBatch(void **startBatch) {return false;}
    virtual bool EndBatch() {return false;}

    virtual bool UpdateMe(typename MVCDef::CurModel *model) {return false;}
  };


  ///class provides simple undo/redo container.
  /**
  To use this class, derive a new class from this template
  and define implementation for each action. You can use StdActionX classes to create simple undo actions.

  Actions are stored into the container by calling function representing the reversed actions during (before or after) processing the action in model. 

  @param MVCDef MVC Definition, instance of DefineMVC template class that defines MVC configuration
  
  */
  template<class MVCDef>
  class UndoRedo: public IModelViewControlerBase<typename MVCDef::IModelActions>
  {  
  protected:
    ///Declares base class for model
    typedef Model<MVCDef> ModelClass;    
    ///Declares lockable class
    typedef LockableClass<MVCDef> LClass;
    ///Declares lock class - it uses Lock from Model
    typedef typename LClass::Lock Lock;

    ///Defines base for all actions
    class ActionBase
    {      
      friend class ItemList<ActionBase *>;
      ///Next item in linked list
      ActionBase *_nextItem;
    public:
      ///Constructs action
      ActionBase():_nextItem(0) {}

      ///This function is called in for given action
      /**
      @param model model reference which this action is processed on.
      */
      virtual void RunAction(ModelClass &model)=0;
    };

  private:
    ///List of actions for undo
    ItemList<ActionBase *> _undoStack;
    ///List of actions for redo
    ItemList<ActionBase *> _redoStack;
    bool _inUndo; ///<sets to true, when object currently in undo process
    bool _inRedo; ///<sets to true, when object currently in redo process. Note, both _inUndo and _inRedo can be set, during redo process
    int _groupCount; ///<count of opened groups
    ActionBase *_groupBegin; ///<Mark in undo history where group is begins
    bool _dirty;    ///<simple dirty flag. It is set every time the new action is added. Useful to notify user that model has been modified
    ModelClass &_model; ///<reference to owner model. 
  public:
    ///constructs the undo/redo container
    UndoRedo(ModelClass &model):_model(model), _inUndo(false),_inRedo(false),_groupBegin(0),_groupCount(0),_dirty(false) {}
    ///destructor
    /**
     removes all actions from the stacks
    */
    ~UndoRedo() 
    {
      CleanStack(_undoStack);
      CleanStack(_redoStack);    
    }

  protected:
    ///Used internally to clean stack
    /*
    @param list reference to stack
    */
    static void CleanStack(ItemList<ActionBase *> &list)
    {
      ActionBase *a=list.GetFirstItem();
      while (a)
      {
        list.Remove(a);
        delete a;        
        a=list.GetFirstItem();
      }      
    }

    ///Pushes action to undo stack
    /**
    Use this function only from the derived class. It adds action to the stack. Don't destroy the object referenced by @b action parameter
    after function returns. Given object is taken over by the container.

    @param action pointer to the action
    */
    void PushAction(ActionBase *action)
    {
      Lock lk(_model);
      _undoStack.Add(action);
      if (!_inUndo) CleanStack(_redoStack);
      _dirty=true;
    }

  private:
    ///Processes one redo step
    /** Function is called internally**/
    bool OneRedoStep()
    {
      Lock lk(_model);
      ActionBase *item=_redoStack.GetFirstItem();
      bool res;
      if (item)
      {      
        _redoStack.Remove(item);
        item->RunAction(_model);
        delete item;
        res=true;
      }
      else
        res=false;
      return res;
    }

  public:

    ///Provides Undo operation
    /**
    It takes first action on the undo stack, popes it and processes the action. 

    @note During processing action, undo and redo queues are swapped. That causes, that revered actions will be placed at redo buffer. Thus
    you don't need to handle the action reversion when it is transfered from undo to redo state. This is done automatically by the model.
    @retval true undo processed
    @retval false cannot process the undo
    */
    bool Undo()
    {
      //lock container
      Lock lk(_model);
      //undo is not in progress yet
      assert(_inUndo==false);
      //you cannot call undo recursively
      if (_inUndo) return false;
      //set to undo state
      _inUndo=true;
      //prepare for undoing (make undo to be redo and vice versa)
      _undoStack.SwapLists(_redoStack);
      //process one redo step (swapped - undo step)
      bool res=OneRedoStep();
      //cleaning
      _undoStack.SwapLists(_redoStack);
      //we are no longer in undo state
      _inUndo=false;
      //return result
      return res;      
    }


    ///Provides Undo operation
    /**
    It takes first action on the redo stack, popes it and processes the action. 

    @retval true redo processed
    @retval false cannot process the redo
    */
    bool Redo()
    {
      Lock lk(_model);
      assert(_inUndo==false);
      assert(_inRedo==false);
      if (_inUndo || _inRedo) return false;
      _inUndo=true;
      _inRedo=true;
      OneRedoStep();     
      _inUndo=false;
      _inRedo=false;
    }

    ///Opens the group of actions
    /**
    Group of action is processed as one action. You can create new actions as groups of actions. These groups act as single actions. Undo takes
    back whole group, not a single action. It is recommended to call OpenGroup in BeginGroup function.

    Group can be opened multiple times. But only first open is noticed. Each OpenGroup should have CloseGroup. The final CloseGroup will create
    group action that provides all necessary steps to undo/redo groups

    @return function returns rollback pointer. This helps you to construct simple transactions. You can use returned pointer with RollbackGroup 
    function anytime in currently opened group. 

    @see RollbackGroup

    @note function can return NULL. It has no special meaning. Value NULL can be used in RollbackGroup until group is closed.
    */
    void *OpenGroup()
    {
      Lock lk(_model);
      if (_groupCount++==0) _groupBegin=_undoStack.GetFirstItem();
      return _undoStack.GetFirstItem();
    }

    friend class Group;
    class Group:public ActionBase
    {
      ActionBase *_groupEnd;
      UndoRedo *_owner;
    public:
      Group(ActionBase *groupEnd,UndoRedo *owner):_groupEnd(groupEnd),_owner(owner) {}
      //function excepts, that _inUndo is set
      virtual void RunAction(Model<MVCDef> &model)
      {
        model.BeginBatch(0);
        assert(_owner->_inUndo);
        while (_owner->_redoStack.GetFirstItem()!=_groupEnd)
        {
          if (_owner->OneRedoStep()==false) break;
        }
        model.EndBatch();
      }
    };

    ///Closes the group of actions
    /**
    It closes group level. Count of the CloseGroup must be the same as the count of the OpenGroup. Only final CloseGroup creates a group object. 
    Pointer returned by corresponding OpenGroup is invalidated, and can be no longer used by the RollbackGroup (using pointer after
    CloseGroup is not catches as error, and may cause unpredictable results and/or incompatibility with future versions of the library.

    @note The group object is created only when group contains more then one action. In case of one action, group is transformed into single action.
    In case of no actions in group, group is not created.
    */
    void CloseGroup()
    {
      Lock lk(_model);

      if (_groupCount)
      {
        _groupCount--;
        if (_groupCount==0)
        {
          if (_undoStack.GetFirstItem()!=_groupBegin && _undoStack.GetNextItem(_undoStack.GetFirstItem())!=_groupBegin)
            PushAction(new Group(_groupBegin,this));            
        }
      }
    }


    ///roll backs the opened group.
    /**
    Function roll backs the opened group. Container invokes all actions stored in undo buffer.
    
    @param groupBegin pointer returned by the OpenGroup
    @param enableRedo when this is true, undoed actions are placed into redo buffer. User can reconstruct rollbacked group by the redo action.
    */
    void RollbackGroup(void *groupBegin, bool enableRedo=false)
    {
      Lock lk(_model);
      assert(_inUndo==false);
      if (_inUndo) return false;
      _inUndo=true;
      _undoStack.SwapLists(_redoStack);
      while (_redoStack.GetFirstItem()!=groupBegin)
      {
        if (OneRedoStep()==false) break;
      }
      _undoStack.SwapLists(_redoStack);
      _inUndo=false;
      if (!enableRedo) CleanStack(_redoStack);
    }

    ///returns true, if there were atleast one action processed by the container
    bool IsDirty() const
    {
      return _dirty;
    }


    ///removes dirty flag
    void ResetDirty()
    {
      _dirty=false;
    }

    ///sets dirty flag
    void SetDirty()
    {
      _dirty=true;
    }

    //returns true, when undo is available
    bool CanUndo() const
    {
      return _undoStack.GetFirstItem()!=0;
    }

    //returns true, when redo is available
    bool CanRedo() const
    {
      return _redoStack.GetFirstItem()!=0;
    }

    ///removes undo/redo history
    void Clear()
    {
      CleanStack(_redoStack);
      CleanStack(_undoStack)
    }

    //Returns active model
    typename MVCDef::CurModel *GetModel()
    {
      return _model;
    }

    //Returns active model
    const typename MVCDef::CurModel *GetModel() const
    {
      return _model;
    }    

    template<class FunctionType, FunctionType function>
    class StdAction0: public ActionBase
    {
    public:
      virtual void RunAction(ModelClass &model) 
       {(model.*function)();}
    };

    template<class FunctionType, FunctionType function,
      class P1>
    class StdAction1: public ActionBase
    {
    protected:
      P1 p1;
    public:
      StdAction1(const P1 &p1):p1(p1) {}
      virtual void RunAction(ModelClass &model) 
      {(model.*function)(p1);}
    };

    template<class FunctionType, FunctionType function,
    class P1,class P2>
    class StdAction2: public ActionBase
    {
    protected:
      P1 p1;
      P2 p2;
    public:
      StdAction2(const P1 &p1,const P2 &p2):p1(p1),p2(p2) {}
      virtual void RunAction(ModelClass &model) 
      {(model.*function)(p1,p2);}
    };

    template<class FunctionType, FunctionType function,
    class P1,class P2,class P3>
    class StdAction3: public ActionBase
    {
    protected:
      P1 p1;
      P2 p2;
      P3 p3;
    public:
      StdAction3(const P1 &p1,const P2 &p2,const P3 &p3):
          p1(p1),p2(p2),p3(p3) {}
      virtual void RunAction(ModelClass &model) 
      {(model.*function)(p1,p2,p3);}
    };

    template<class FunctionType, FunctionType function,
    class P1,class P2,class P3,class P4>
    class StdAction4: public ActionBase
    {
    protected:
      P1 p1;
      P2 p2;
      P3 p3;
      P4 p4;
    public:
      StdAction4(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4):
          p1(p1),p2(p2),p3(p3),p4(p4) {}
          virtual void RunAction(ModelClass &model) 
          {(model.*function)(p1,p2,p3,p4);}
    };

    template<class FunctionType, FunctionType function,
    class P1,class P2,class P3,class P4,class P5>
    class StdAction5: public ActionBase
    {
    protected:
      P1 p1;
      P2 p2;
      P3 p3;
      P4 p4;
      P5 p5;
    public:
      StdAction5(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4,const P5 &p5):
          p1(p1),p2(p2),p3(p3),p4(p4),p5(p5) {}
          virtual void RunAction(ModelClass &model) 
          {(model.*function)(p1,p2,p3,p4,p5);}
    };

    template<class FunctionType, FunctionType function,
    class P1,class P2,class P3,class P4,class P5,class P6>
    class StdAction6: public ActionBase
    {
    protected:
      P1 p1;
      P2 p2;
      P3 p3;
      P4 p4;
      P5 p5;
      P6 p6;
    public:
      StdAction6(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4,const P5 &p5,
          const P6 &p6):
          p1(p1),p2(p2),p3(p3),p4(p4),p5(p5),p6(p6) {}
          virtual void RunAction(ModelClass &model) 
          {(model.*function)(p1,p2,p3,p4,p5,p6);}
    };

    template<class FunctionType, FunctionType function,
    class P1,class P2,class P3,class P4,class P5,class P6,
    class P7>
    class StdAction7:public ActionBase
    {
    protected:
      P1 p1;
      P2 p2;
      P3 p3;
      P4 p4;
      P5 p5;
      P6 p6;
      P7 p7;
    public:
      StdAction7(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4,const P5 &p5,
        const P6 &p6,const P7 &p7):
      p1(p1),p2(p2),p3(p3),p4(p4),p5(p5),p6(p6),p7(p7) {}
      virtual void RunAction(ModelClass &model) 
      {(model.*function)(p1,p2,p3,p4,p5,p6,p7);}
    };

    template<class FunctionType, FunctionType function,
    class P1,class P2,class P3,class P4,class P5,class P6,
    class P7,class P8>
    class StdAction8:public ActionBase
    {
    protected:
      P1 p1;
      P2 p2;
      P3 p3;
      P4 p4;
      P5 p5;
      P6 p6;
      P7 p7;
      P8 p8;
    public:
      StdAction8(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4,const P5 &p5,
        const P6 &p6,const P7 &p7,const P8 &p8):
      p1(p1),p2(p2),p3(p3),p4(p4),p5(p5),p6(p6),p7(p7),p8(p8) {}
      virtual void RunAction(ModelClass &model) 
      {(model.*function)(p1,p2,p3,p4,p5,p6,p7,p8);}
    };

    template<class FunctionType, FunctionType function,
    class P1,class P2,class P3,class P4,class P5,class P6,
    class P7,class P8,class P9>
    class StdAction9: public ActionBase
    {
    protected:
      P1 p1;
      P2 p2;
      P3 p3;
      P4 p4;
      P5 p5;
      P6 p6;
      P7 p7;
      P8 p8;
      P9 p9;
    public:
      StdAction9(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4,const P5 &p5,
        const P6 &p6,const P7 &p7,const P8 &p8, const P9 &p9):
      p1(p1),p2(p2),p3(p3),p4(p4),p5(p5),p6(p6),p7(p7),p8(p8),p9(p9) {}
      virtual void RunAction(ModelClass &model) 
      {(model.*function)(p1,p2,p3,p4,p5,p6,p7,p8,p9);}
    };

    template<class FunctionType, FunctionType function,
    class P1,class P2,class P3,class P4,class P5,class P6,
    class P7,class P8,class P9,class P10>
    class StdAction10: public ActionBase
    {
    protected:
      P1 p1;
      P2 p2;
      P3 p3;
      P4 p4;
      P5 p5;
      P6 p6;
      P7 p7;
      P8 p8;
      P9 p9;
      P10 p10;
    public:
      StdAction10(const P1 &p1,const P2 &p2,const P3 &p3,const P4 &p4,const P5 &p5,
        const P6 &p6,const P7 &p7,const P8 &p8, const P9 &p9, const P10 &p10):
      p1(p1),p2(p2),p3(p3),p4(p4),p5(p5),p6(p6),p7(p7),p8(p8),p9(p9),p10(p10) {}
      virtual void RunAction(ModelClass &model) 
      {(model.*function)(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10);}
    };
  };
}