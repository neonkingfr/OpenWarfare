#if !defined(AFX_ISTRINGRES_H__69BE0992_F681_49E8_BCC8_350B0EBCF1D3__INCLUDED_)
#define AFX_ISTRINGRES_H__69BE0992_F681_49E8_BCC8_350B0EBCF1D3__INCLUDED_

#pragma once

template<class T, class Index>
class IResourceContainer
{
public:
  virtual T operator[](Index resourceId)=0;
  const T operator[](Index resourceId) const {return (*this)[resourceId];}

  virtual ~IResourceContainer()=0 {}
};


#endif