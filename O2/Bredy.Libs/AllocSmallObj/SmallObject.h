#pragma once

#include <set>
#include <stack>
#include <assert.h>
#include <exception>

///Namespace groups all classes needed for this library
/**
To use libary, see SmallObject class
*/
namespace SmallObjectAllocator
{  
  ///Chunk header declaration
  /**
  Each chunk in memory is divided into a header and data. Header is represented by Chunk class. Data are follow immediatelly after class.
  Block in data section are addressed using the simple "FAT-like" system.

  @note Class doesn't remember size of one block and total count of blocks. These informations are not necesery to be stored in chunk header. 
  Class excepts, that these values will be controlled by owner class (at higher level)
  */
  class Chunk
  {
    ///Defines value in FAT that represents end of chain
    static const unsigned char EndOfChain=255;
    union
    {
      struct  
      {
        ///first free slot - start of chain
        unsigned char _firstFree;
        ///count of used slots - to detect empty or full chunk
        unsigned char _usage;
      };
      ///Keep class alignment
      int _align;
    };    

    ///Returns pointer to data section
    unsigned char *FirstSlot() const 
      {
        return reinterpret_cast<unsigned char *>(const_cast<Chunk *>(this)+1);
      }
    
  public:

    ///Inicializes the chunk. 
    /**
    Function excepts, that chunk is already allocated (including the data section)
    @param sz excepted block size
    @param maxSlots count of slots in this chunk
    */
    void InitChunk(size_t sz, unsigned char maxSlots)
    {
      unsigned char *chkbeg=FirstSlot();
      for (int i=0;i<maxSlots;i++) chkbeg[i*sz]=(unsigned char)(i+1);
      chkbeg[(maxSlots-1)*sz]=EndOfChain;
      _firstFree=0;
      _usage=0;
    }

    ///Allocates new chunk for given parameters
    /**
    @param sz requested block size
    @param maxSlots count of slots in this chunk
    @return pointer to new chunk
    */
    static Chunk *AllocChunk(size_t sz, unsigned char maxSlots)
    {
      Chunk *chk=reinterpret_cast<Chunk *>(new char[maxSlots*sz+sizeof(Chunk)]);
      if (chk==0) return 0;
      chk->InitChunk(sz,maxSlots);
      return chk;
    }

    ///Allocates slot in chunk
    /**
    @param sz requested size. Because chunk doesn't store size for one slot, this parameter is used to "recall" the slot size. 
    @return pointer to the block, or NULL, if chunk is empty
    @note parameter 'sz' must have the same value as parameter 'sz' in AllocChunk function on time chunk allocation. You cannot allocate 
    different size. Result of such operation will have unpredictable behavior
    */
    void *Alloc(size_t sz)
    {
      if (_firstFree==EndOfChain) return 0;
      int choosen=_firstFree;
      unsigned char *chkbeg=FirstSlot();
      _firstFree=chkbeg[choosen*sz];
      _usage++;
      return chkbeg+(choosen*sz);
    }

    ///Deallocates used block
    /**
    @param what pointer to block to deallocate
    @param sz size of deallocating block. This value is provided by standard delete operator.
    */
    void Free(void *what, size_t sz)
    {
      unsigned char *w=reinterpret_cast<unsigned char *>(what);
      unsigned char *chkbeg=FirstSlot();
      size_t index=(w-chkbeg)/sz;
      *w=_firstFree;
      _firstFree=(unsigned char)index;
      _usage--;
    }

    ///Deallocates (physically) a given chunk
    void DeleteChunk()
    {
      char *c=reinterpret_cast<char *>(this);
      delete [] c;
    }

    ///Returns true, when chunk is empty
    bool IsEmpty() const {return _usage==0;}
    ///Returns true, when chunk is full
    bool IsFull() const {return _firstFree==EndOfChain;}

    ///Returns true, when slot pointed by ptr belongs to given chunk
    /**
    @param ptr pointer to slot
    @param sz size of the slot
    @param maxSlots total count of slots in chunk
    @retval true slot is laying in given chunk
    @retval false no informations abount slot, slot was not found in chunk
    */
    bool IsMine(void *ptr, size_t sz, unsigned char maxSlots) const
    {
      unsigned char *w=reinterpret_cast<unsigned char *>(ptr);
      unsigned char *chkbeg=FirstSlot();
      size_t index=(w-chkbeg)/sz;
      return index>=0 && index<maxSlots;
    }
  };    

  ///ChunkMaster is class that groups chunks of fixed size
  /**
   * It processes allocation and deallocation tasks. 
   * @note ChunkMaster doesn't store chunk size. It is not necessary, because
   *  size is remembered by owner class. You have to call Alloc everytime with the
   * same sz paramatere
   */
  class ChunkMaster
  {
    typedef std::set<Chunk *> ChunkSet;
    typedef std::set<Chunk *> FreeChunkSet;
    typedef ChunkSet::iterator ChunkSetIter;
    typedef FreeChunkSet::iterator FreeChunkSetIter;
    ///List of all allocated chunks
    ChunkSet _chunks;
    ///Pointer to chunk that was recently used for allocating
    Chunk *_lastAlloc;
    ///Pointer to chunk that is complette free and waiting for deleteing
    Chunk *_wholeEmpty;
    ///Pointer to chunk that has been recently subject of deallocation
    Chunk *_lastFree;
    ///List of chunks that contains some empty space
    FreeChunkSet _freeChunks;

    ///count of slots in one chunk
    unsigned char _maxSlots;
  public:
    ///inicialization
    ChunkMaster(int maxSlots):_lastAlloc(0),_wholeEmpty(0),_maxSlots(maxSlots),_lastFree(0) {}
    ///destruction
    ~ChunkMaster()
    {
      for (ChunkSetIter it=_chunks.begin(),end=_chunks.end();it!=end;++it) (*it)->DeleteChunk();
    }
    
    ///allocates memory space
    /**
     * @param sz Size of requested memory space. Remember: This parameter MUST HAVE the
     * same value during lifetime of this object. You cannot allocate different sizes
     * in one ChunkMaster object. This can cause unpredictable results.
     * @return pointer to new memory space. Function can return NULL, when allocation
     * has failed (it depends on current behaviour of operator new)
     */
    void *Alloc(size_t sz)
    {
      if (_lastAlloc)
      {
        if (_lastAlloc==_wholeEmpty) _wholeEmpty=0;
        void *res=_lastAlloc->Alloc(sz);
        if (res) return res;        
        _lastAlloc=0;
      }
      if (!_freeChunks.empty())
      {      
        _lastAlloc=*_freeChunks.begin();
        _freeChunks.erase(_lastAlloc);
        return Alloc(sz);
      }
      if (_wholeEmpty)
      {
        _lastAlloc=_wholeEmpty;
        return Alloc(sz);
      }
      _lastAlloc=Chunk::AllocChunk(sz,_maxSlots);
      if (_lastAlloc==0) return 0;
      _chunks.insert(_lastAlloc);
      return Alloc(sz);     
    }

  protected:
    ///Sets empty chunk into pre-destroy state giving it the last chance for any allocation.
    /**
     * Empty chunk is moved to _wholeEmpty variable. This chunk is waiting
     * here to get the latest chance for any allocation request, before it is
     * completely destroyed. It can be later destroyed by another empty chunk. ChunkMaster
     * remembers only one empty chunk. But this behavior increases performance, in
     * case of chain of single allocating and deallocating memory is repeatedly processed.
     */
    void FreeChunk(Chunk *chk)
    {
      if (_wholeEmpty)
      {
        _freeChunks.erase(_wholeEmpty);
        _chunks.erase(_wholeEmpty);
        _wholeEmpty->DeleteChunk();
      }
      _wholeEmpty=chk;
      if (_lastAlloc==_wholeEmpty) _lastAlloc=0;
      if (_lastFree==_wholeEmpty) _lastFree=0;
    }
  public:
    ///Deallocates memory block
    /**
     * @param ptr pointer to allocated block
     * @param sz size of this block. This parameter have to match the parameters sz during allocation     
     */
    void Free(void *ptr, size_t sz)
    {
      if (_lastAlloc && _lastAlloc->IsMine(ptr,sz,_maxSlots))
      {
        _lastAlloc->Free(ptr,sz);
        if (_lastAlloc->IsEmpty()) FreeChunk(_lastAlloc);
        return;
      }
      
      if (_lastFree && _lastFree->IsMine(ptr,sz,_maxSlots))
      {
        if (_lastFree->IsFull()) _freeChunks.insert(_lastFree);
        _lastFree->Free(ptr,sz);
        if (_lastFree->IsEmpty()) FreeChunk(_lastFree);
        return;
      }

      Chunk *search(reinterpret_cast<Chunk *>(ptr));
      ChunkSetIter fnd=_chunks.upper_bound(search);      

      assert(fnd!=_chunks.begin());
      --fnd;      
      Chunk *k=*fnd;
      assert(k->IsMine(ptr,sz,_maxSlots));
      if (k->IsFull()) 
        _freeChunks.insert(k);
      k->Free(ptr,sz);
      _lastFree=k;
      if (k->IsEmpty()) FreeChunk(k);
      
    }
  };   

  ///Provides all small object allocation requests 
  /**
   *  This class provides all allocation request. If can complete replace standard
   * new operator. But this not recommended, because allocator itself uses standard
   * new and delete to allocate helper structures. Best usage of this allocator is
   * through SmallObject class.
   */
  class Allocator
  {
    ///list of chunk masters
    ChunkMaster **_list;
    ///list size
    size_t _listSize;    
    ///chunk size limit
    size_t _maxChunkSize;
  public:
    ///Inicialization
    /**
     * @param maxSmallBlock Defines threshold for small block. Allocation request greater
     * then this parameter are en routed to the standard allocator. Only equal and smaller
     * request are processed. Example, when you specify 1024, request in range 0-1024 will
     * be processed by this allocator and request above 1024 (1025...) will be processed
     * by standard the allocator
     * @param maxChunkSize Specifies maximum size for one chunk. Value is used for calculation
     * of count of slots in one chunk. This value is treat as hint and can be exceeded in
     * some special situations
     */
    Allocator(size_t maxSmallBlock, size_t maxChunkSize):_listSize(maxSmallBlock),_maxChunkSize(maxChunkSize),_list(0) {}
    ~Allocator()
    {
      for (unsigned int i=0;i<_listSize;i++) delete _list[i];
      delete [] _list;
    }
    
    ///Allocates memory space
    /**
     * @param sz requested size. There is no special conditions on this value. 
     *  Depends on settings allocator can en route this request to the standard allocator
     *  When sz is zero, value is silently replaced by 1 and small allocator (with
     *  allocation of 1 byte) is used. This is in agreement with C++ standard that
     *  each object need to have unique address even it is empty.
     * @return pointer to new allocated space or NULL in case out of memory
     */
    void *Alloc(size_t sz)
    {
      if (sz==0) sz=1;
      if (sz>_listSize) return new char[sz];
      if (_list==0) 
      {
        _list=new ChunkMaster *[_listSize];
        for (unsigned int i=0;i<_listSize;i++) _list[i]=0;
      }
      if (_list[sz-1]==0) 
      {
        size_t slots=_maxChunkSize/sz;
        if (slots<0) slots=1;
        if (slots>255) slots=255;
        _list[sz-1]=new ChunkMaster((unsigned char)slots);
      }
      return _list[sz-1]->Alloc(sz);    
    }

    ///Deallocates memory space
    /**
     * @param p pointer to memory block to free
     * @param sz size of this block. This value MUST be the same as sz argument in time of
     * allocation. If allocator is used in operator delete, this parameter is filled
     * by the compiler or by the destructor during deletion. So you don't need to store it.     
     */
    void Free(void *p, size_t sz)
    {
      if (p==0) return ;      
      if (sz==0) sz=1;
      if (sz>_listSize) 
      {
        delete [] (char *)p;
        return;
      }

      assert(_list[sz-1]!=0);
      _list[sz-1]->Free(p,sz);
    }

  };

  ///Singleton interface defines operations on active allocator  
  class IGlobalAllocator
  { 
    static IGlobalAllocator *ActiveInstance;
  public:

    ///Inicializes and registers active allocator
    /**
     * Only one allocator can be active. Constructor will not 
     * register any additional allocator.
     */
    IGlobalAllocator()
    {
      assert(ActiveInstance==0); //some allocator is already installed
      if (ActiveInstance==0) ActiveInstance=this;
    }

    ~IGlobalAllocator()
    {
      if (ActiveInstance==this) ActiveInstance=0;
    }
    
    ///Allocates memory space
    /**
     * @copydoc Allocator::Alloc 
     */
    virtual void *Alloc(size_t sz)=0;

    ///Deallocates memory space
    /**
    * @copydoc Allocator::Free 
    */
    virtual void Free(void *p, size_t sz)=0;

    ///Retrieves the pointer to the active allocator
    static IGlobalAllocator *Instance() 
    {
      assert(ActiveInstance!=0); //No allocator installed. Install allocator singleton anywhere in your application
      return ActiveInstance;
    }
    
  };

  ///Provides NULL multi thread locking - use in single thread only
  class NullLocker
  {
  public:
    void Lock() {}
    void Unlock() {}
  };


  ///Provides NULL error handling
  /**
   * NULL error handling means, that no exception will be thrown when allocation failed 
   */
  class NullErrorHandling
  {  
  public:
    static void ReportError() {}
  };

  ///Exception for all bad allocations
  class bad_alloc  : public std::exception
  {	// base of all bad allocation exceptions
  public:
    bad_alloc(const char *_Message = "SmallObject Allocator - bad allocation") _THROW0()
      : exception(_Message)    {}
  };


  ///Provides standard error handling 
  /**
   * It throws SmallObjectAllocator::bad_alloc exception 
   */
  class ExceptionErrorHandling
  {  
  public:
    static void ReportError() {throw bad_alloc();}
  };

  ///FINAL ALLOCATOR CLASS (read documentation from this point!)
  /**
   * This class is responsible for all allocations and deallocations of small objects.
   * To use this class, create one (and the only one) instance of this class.
   * You can configure this class using the template arguments and constructor parameters
   * 
   * Singleton is not instanced automatically, so you will get an assertation
   * when you use allocation if allocator is not installed
   *
   * If you have trouble with instancing class, when your application allocates
   * some memory during CRT startup, place instance declaraction into compiler's
   * initseg /#pragma initseg(compiler)/ or use GCC priority attributte to specifiy
   * inicialization order
   *
   * @param ErrorHandling Specify error handling for allocator. Use NullErrorHandling to
   * handle bad allocations by returning NULL value. Use ExceptionErrorHandling to handle
   * bad allocations by exception.
   * @param ThreadLocker Choose locker for thread safe. Default NullLocker means no locking
   * during allocation
   */
  template<class ErrorHandling=ExceptionErrorHandling, class ThreadLocker=NullLocker>
  class GlobalAllocator: public IGlobalAllocator, public ThreadLocker
  {
    Allocator _allocator;    
  public:
    ///Allocator initialization
    /**
     * @copydoc Allocator::Allocator
     */
    GlobalAllocator(size_t maxSmallBlock=256, size_t maxChunkSize=4096):_allocator(maxSmallBlock,maxChunkSize) {}

    virtual void *Alloc(size_t sz) throw(...)
    {
      Lock();
      void *res;
      try
      {      
        res=_allocator.Alloc(sz);
      }
      catch (std::bad_alloc e)
      {
        res=0;
      }
      catch (...)
      {
        Unlock();
        throw;
      }
      Unlock();
      if (res==0) ErrorHandling::ReportError();
      return res;
    }
    virtual void Free(void *p,size_t sz) throw(...)
    {
      Lock();
      try
      {      
        _allocator.Free(p,sz);
      }
      catch (...)
      {
        Unlock();
        throw;
      }
      Unlock();
    }
  };
}

///Base class for all small objects
/**
 * The easiest way how to use Small Object Allocator is derive this class. That is all
 * Class defines operators new and delete, and en routes all allocations through the
 * global allocator (represented by IGlobalAllocator).
 *
 * @note Beware to deleting. If this class is used in some objects, that are deriving
 * a common interface, don't delete these object using the delete interface. It will
 * not call the right delete operator. This situation can be solved by two ways:<br>
 * Use SmallObject as base to the common interface<br> @b or<br> Define virtual void Release() {delete this;} to deleting classes through interface and
 * use this function instead the direct call delete. Derived classes that uses
 * SmallObject class must overwrite this function by calling its version of delete operator
 * 
 */
class SmallObject
{
public:
  ///operator new replacement
  void *operator new(size_t sz)
  {
    return SmallObjectAllocator::IGlobalAllocator::Instance()->Alloc(sz);
  }
  ///operator delete replacement
  void operator delete(void *ptr, size_t sz)
  {
    return SmallObjectAllocator::IGlobalAllocator::Instance()->Free(ptr,sz);
  }
};
