// AllocSmallObj.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "SmallObject.h"

class Test: public SmallObject
{
  int a;
  int b;
  char c;
  float d;
};

static SmallObjectAllocator::GlobalAllocator<> GAllocator;

int _tmain(int argc, _TCHAR* argv[])
{
  Test *ff[1000];
  for (int i=0;i<1000;i++) ff[i]=0;  
  for (int i=0;i<1000000;i++)
  {
    int x=rand()%1000;
    if (ff[x]==0) ff[x]=new Test();
    else {delete ff[x];ff[x]=0;}
  }
  for (int i=0;i<1000;i++) delete ff[i];

  return 0;
}

