#pragma once
#include "propshitem.h"

class PropShTitleOnly :
  public PropShItem
{
public:
  enum Style
  {
    AlignLeft=SS_LEFT,
    AlignCenter=SS_CENTER,
    AlignRight=SS_RIGHT,
    ShowEMF=SS_ENHMETAFILE,
    HorzLine=SS_ETCHEDHORZ,
    Frame=SS_ETCHEDFRAME,
  };
  
protected:
  HWND _hTitle;  
  Style _style;
public:
  PropShTitleOnly(Style style=AlignCenter);
  ~PropShTitleOnly(void);

  virtual bool Create(const tchar *name, const tchar *title, PropShItem *parent=NULL);
  virtual HWND GetWindowHandle() {return _hTitle;} //default - no canvas
  virtual void UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height);
  virtual void BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam);

  int ResizeTitle(PropShHDWP &dwp, int left, int top, int wspace, int hspace);
  int CalculateFontHeight(HWND hControl);

};
