#include "PropShCommon.h"
#include ".\propshfileline.h"
#include <Commdlg.h>

PropShFileLine::PropShFileLine(const tchar *filter, const tchar *defext, bool saveDlg, unsigned long flags, const tchar *defpath)
{
  _filter=(tchar *)malloc((_tcslen(filter)+5)*sizeof(tchar));
  _tcscpy(_filter,filter);
  _defext=_tcsdup(defext);
  tchar *c=_filter;
  while (*c) {if (*c=='|') *c=0;c++;}
  *c++=0;
  *c++=0;
  _flags=flags;
  _save=saveDlg;
  _defpath=_tcsdup(defpath);
}

PropShFileLine::~PropShFileLine(void)
{
  free(_filter);
  free(_defext);
  free(_defpath);
}

void PropShFileLine::OnButtonPressed()
{
  OPENFILENAME ofn; 
  memset(&ofn,0,sizeof(ofn));
  ofn.lStructSize=sizeof(ofn);
  ofn.hwndOwner=GetParentItem()->GetWindowHandle();
  ofn.hInstance=GetParentItem()->GetInstance();
  ofn.lpstrFilter=_filter;
  tchar fname[MAX_PATH*2];  
  GetStringData(fname,MAX_PATH*2);
  ofn.lpstrFile=fname;
  ofn.lpstrInitialDir=_defpath;
  ofn.nMaxFile=MAX_PATH*2;
  ofn.lpstrDefExt=_defext;
  tchar *title=(tchar *)alloca((GetWindowTextLength(_hTitle)+1)*sizeof(tchar));
  GetWindowText(_hTitle,title,GetWindowTextLength(_hTitle)+1);
  ofn.lpstrTitle=title;
  ofn.Flags=_flags;
  bool res;
  if (_save)  
    res=GetSaveFileName(&ofn)!=FALSE;
  else
    res=GetOpenFileName(&ofn)!=FALSE;
  if (res) 
  {
    SetStringData(fname);  
  }
}
void PropShFileLine::SetStringData(const tchar *data)
{
    PropShTextLine::SetStringData(data);
    int len=GetDataSize();
    SendMessage(_hControl,EM_SETSEL,len,len);
}
