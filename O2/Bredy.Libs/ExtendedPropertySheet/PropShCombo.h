#pragma once
#include "propshtextline.h"

class PropShCombo :
  public PropShTextLine
{
  bool _dropList; //combobox is droplist;
  bool _sortItems;
  bool _allowNull;
public:
  PropShCombo(bool dropList=false, bool sortItems=false, bool allowNull=true);
  ~PropShCombo(void);

  
  /// Creates item in parent canvas. The item has the name, and the title
  virtual bool Create(const tchar *name, const tchar *title, PropShItem *parent=NULL);
  virtual int GetDataSize() const;
  virtual bool GetStringData(tchar *buffer, size_t size) const;
  virtual void SetStringData(const tchar *data);
  virtual void UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height);

  void AddItem(const tchar *value, long userData=0);
  int FindItem(const tchar *value, int index=-1);
  bool DeleteItem(int index);
  int GetCurItem();
  void SelectItem(int index);
  long GetItemData(int index);
  bool SetItemData(int index, long data);
  void DropDown();
  void CloseUp();
  bool IsDroppedDown();
};
