#pragma once
#include "PropShGroup.h"

class PropSheetWindow :public PropShGroup
{
  struct CreationInfo
  {
	int x,y,cx,cy;
	unsigned long style;
	HWND hwndParent;
	HMENU menu;
	unsigned long exstyle;    
  };

  CreationInfo *_cinfo;	///this pointer is valid only during sheet creation
  bool _lockResize;	  ///if true, window ignores all WM_SIZE messages. Prevents recursion during layout update
  int _minWidth,_minHeight; ///these values caches the smallest size of window, if canvas enables it, for MINMAXINFO
  bool _resizeX,_resizeY; ///enables resizing of windows, default true
  HFONT _dlgfont;     ///font in dialog
  HWND _lastFocus;    ///saves last focus when dialog is deactivated  


public:
  PropSheetWindow(void);
  ~PropSheetWindow(void);

  bool Create(const tchar *title, unsigned long style,  int x, int y, int cx, int cy, HWND parentWnd, unsigned long exstyle=NULL, HMENU menu=NULL, HINSTANCE hInst=NULL);
  HWND CreateCanvasWindow(const tchar *className,const tchar *title, HWND parent, HINSTANCE hInst);
  virtual LRESULT OnMessage(UINT message, WPARAM wParam, LPARAM lParam);
  virtual void ResizeCanvas(PropShHDWP &hdwp, int left, int top, int cx,int cy, int *outCx=NULL, int *outCy=NULL);
  void EnableResizing(bool resizeX,bool resizeY)
  {_resizeX=resizeX, _resizeY=resizeY;}

  void SetFont(const LOGFONT &font);
  void SetFont(const tchar *faceName, int size, bool bold=false, bool italic=false, int codepage=0);
  bool IsMyMessage(const MSG *msg);
  void LockResize(bool lock) {_lockResize=lock;}
};
