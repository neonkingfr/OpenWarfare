#include "PropShCommon.h"
#include <commctrl.h>
#include ".\propshsliderline.h"
#include "WindowPos.h"
#include <stdlib.h>
#include <math.h>


static unsigned char sliderWindow[]={1,0,255,255,0,0,0,0,128,0,0,0,72,0,204,128,1,0,0,0,
0,0,158,0,23,0,0,0,0,0,83,0,108,0,105,0,100,0,101,0,114,
0,0,0,8,0,144,1,0,1,77,0,83,0,32,0,83,0,104,0,101,0,
108,0,108,0,32,0,68,0,108,0,103,0,0,0,0,0,0,0,0,0,0,
0,33,0,1,80,0,0,0,0,158,0,23,0,246,3,0,0,109,0,115,0,
99,0,116,0,108,0,115,0,95,0,116,0,114,0,97,0,99,0,107,0,98,
0,97,0,114,0,51,0,50,0,0,0,0,0,0,0};

PropShSliderLine::PropShSliderLine(double minval, double maxval,int sldrSteps, int sldrTicks, int decimals, Mode mode):
  _rangeMin(minval),
  _rangeMax(maxval),
  _steps(sldrSteps),
  _ticks(sldrTicks),
  _mode(mode),
  _decimals(decimals)
  {
    _sliderWnd=NULL;
}

PropShSliderLine::~PropShSliderLine(void)
{
}

static LRESULT WINAPI SliderDlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
  PropShSliderLine *sliderItem=(PropShSliderLine *)GetWindowLong(hWnd,DWL_USER);
  if (sliderItem==NULL)
  {
    if (message==WM_INITDIALOG)
    {
      sliderItem=(PropShSliderLine *)lParam;
      SetWindowLong(hWnd,DWL_USER,lParam);      
      sliderItem->_sliderWnd=hWnd;
      sliderItem->_sliderCntr=GetWindow(hWnd,GW_CHILD);
    }
  }
  if (sliderItem!=NULL)
    return sliderItem->OnSliderDlgMsg(message,wParam,lParam);
  return 0;
}

void PropShSliderLine::OnButtonPressed()
{    
  if (_sliderWnd) return;
  DialogBoxIndirectParam(GetInstance(),(LPCDLGTEMPLATE)sliderWindow,_hButton,(DLGPROC)SliderDlgProc,(LPARAM)this);
  _sliderWnd=NULL;
  _sliderCntr=NULL;
  
}

LRESULT PropShSliderLine::OnSliderDlgMsg(UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
  case WM_INITDIALOG:
    {
      EnableWindow(GetParent(_sliderWnd),TRUE);
      RECT rc;
      GetWindowRect(_hButton,&rc);
      POINT pt={rc.right-rc.left,0};      
      ClientToScreen(_hButton,&pt);
      pt.y-=GetSystemMetrics(SM_CYSMCAPTION)+GetSystemMetrics(SM_CYSIZEFRAME);
      SetWindowPos(_sliderWnd,NULL,pt.x,pt.y,0,0,SWP_NOZORDER|SWP_NOSIZE);
      if (EnsureWindowVisible(_sliderWnd,pt))
      {
        if (pt.x==0) pt.x=1;
        if (pt.y==0) pt.y=1;
        SetWindowPos(_sliderWnd,NULL,pt.x,pt.y,0,0,SWP_NOZORDER|SWP_NOSIZE);
      }
      size_t namesz;
      tchar *name=(tchar *)alloca((namesz=GetWindowTextLength(_hTitle)+1)*sizeof(tchar));
      GetWindowText(_hTitle,name,namesz);
      SetWindowText(_sliderWnd,name);
      double pos=GetValue();
      SendMessage(_sliderCntr,TBM_SETRANGE,true,MAKELONG(0,_steps));
      if (_ticks>0) SendMessage(_sliderCntr,TBM_SETTICFREQ,_steps/_ticks,0);
      if (_ticks>0)
        SendMessage(_sliderCntr,TBM_SETPAGESIZE,0,_steps/_ticks);
      else
        SendMessage(_sliderCntr,TBM_SETPAGESIZE,_steps/8,0);
      if (_mode==Relative)
      {
        double midl=(_rangeMin+_rangeMax)*0.5f;
        _relpos=pos-midl;
        pos=midl;
      }
      else if (_mode==Wrap)
      {
        double range=(_rangeMax-_rangeMin)*0.5f;
        double section=floor((pos+range*0.5f)/range)*range-range;        
        _relpos=section-_rangeMin;
        pos=pos-_relpos;
      }
      else
        _relpos=0;
      SendMessage(_sliderCntr,TBM_SETPOS,true,(int)((pos-_rangeMin)/(_rangeMax-_rangeMin)*_steps+0.5f));
      GetWindowRect(_sliderWnd,&rc);
      SetWindowPos(_sliderWnd,NULL,0,0,0,rc.bottom-rc.top,SWP_NOMOVE|SWP_NOZORDER);
      ShowWindow(_sliderWnd,SW_SHOW);
      DWORD endtime=GetTickCount()+250;
      DWORD lasttime=0;
      while (lasttime<endtime)
      {
        lasttime=GetTickCount();
        float factor=(lasttime-endtime+250)/(float)250;
        if (factor>1) factor=1;
        SetWindowPos(_sliderWnd,NULL,0,0,(int)((rc.right-rc.left)*factor),rc.bottom-rc.top,SWP_NOMOVE|SWP_NOZORDER);
        UpdateWindow(_sliderWnd);
      }
    }
    break;
  case WM_ACTIVATE:
    {
      if (wParam==WA_INACTIVE)
      {
/*        SendMessage(_sliderWnd,WM_CANCELMODE,0,0);*/
        SetTimer(_sliderWnd,2,20,NULL);
        return 1;
      }
    }
    break;
  case WM_SIZE:
    MoveWindow(_sliderCntr,0,0,LOWORD(lParam),HIWORD(lParam),TRUE);
    break;
  case WM_TIMER:
    if (wParam==2) DestroyWindow(_sliderWnd);
    break;
  case WM_COMMAND:
    {
      switch (LOWORD(wParam))
      {
        case IDOK:
        case IDCANCEL: EndDialog(_sliderWnd,0);break;
      }
      return 1;
    }
  case WM_HSCROLL:
      if ((HWND)lParam==_sliderCntr)
      {
        int pos=SendMessage(_sliderCntr,TBM_GETPOS,0,0);
        double dpos=(double)pos*(_rangeMax-_rangeMin)/(_steps)+_rangeMin;
        dpos+=_relpos;
        SetValue(dpos);
      }    
    return 1;
  }

  return 0;
}

double PropShSliderLine::GetValue()
{
      tchar data[50];
      GetStringData(data,50);
      double pos=_tcstod(data,NULL);
      return pos;
}

void PropShSliderLine::SetValue(double value)
{
          tchar data[50];
        tchar format[50];
          if (_decimals<0)
            _tcscpy(format,"%g");
          else
            _stprintf(format,"%%1.%df",_decimals);
        _sntprintf(data,sizeof(data)/sizeof(data[0]),format,value);
        SetStringData(data);
}

bool PropShSliderLine::ReflectMessage(HWND hWnd,UINT message,WPARAM wParam, LPARAM lParam, LRESULT *result)
{
  if (hWnd==_hControl)
  {
    if (message==WM_COMMAND && HIWORD(wParam)==EN_KILLFOCUS)
    {
          double val=GetValue();
          if (_mode==CheckRange)
          {
            if (val<_rangeMin) val=_rangeMin;
            if (val>_rangeMax) val=_rangeMax;
          }
          SetValue(val);
          return true;       
    }
  }
    return PropShTextWithButton::ReflectMessage(hWnd,message,wParam,lParam,result);
}