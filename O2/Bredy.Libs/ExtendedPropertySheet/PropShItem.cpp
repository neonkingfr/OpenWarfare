// PropShItem.cpp: implementation of the PropShItem class.
//
//////////////////////////////////////////////////////////////////////

#include "PropShCommon.h"
#include "PropShItem.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

PropShItem::PropShItem():_name(NULL),_parent(NULL),_event(NULL)
{

}

PropShItem::~PropShItem()
{
free(_name);
}



bool PropShItem::Create(const tchar *name, const tchar *title, PropShItem *parent/*=NULL*/)
{
  if (name==NULL || _name!=NULL) return false;  
  _name=_tcsdup(name);
  _parent=parent;
  return true;
}

void PropShItem::LayoutChanged()
{
  assert (_parent!=NULL);
  _parent->LayoutChanged();
}


void IPropShEvent::LayoutDirty(PropShItem *item,bool animatedChange)
{
  if (animatedChange) item->LayoutChanged();
}

HINSTANCE PropShItem::GetInstance()
{
  HWND hWnd=*this;
  HINSTANCE hInst=hWnd?(HINSTANCE)GetWindowLong(hWnd,GWL_HINSTANCE):GetModuleHandle(NULL);
  return hInst;
}

static IPropShEvent defaultEvent;

IPropShEvent &PropShItem::ThrowEvent() const
{
  const PropShItem *item=this;
  while (item && item->_event==0) {item=item->GetParentItem();}
  if (item==0)  
    return defaultEvent;
  else
    return *item->_event;
}

IPropShEvent &IPropShEvent::ThrowEvent(PropShItem *item, IPropShEvent *curEvent) const
{  
  while (item && item->_event!=curEvent) {item=item->GetParentItem();}
  if (item==0) return defaultEvent;  
  if (item->GetParentItem()) return item->GetParentItem()->ThrowEvent();
  else return defaultEvent;
}