#include "PropShCommon.h"
#include ".\propshcheckbox.h"
#include "WindowPos.h"

PropShCheckBox::PropShCheckBox(Style style):_style(style),PropShTitleOnly(PropShTitleOnly::AlignLeft)
{
  _hButton=NULL;
}

PropShCheckBox::~PropShCheckBox(void)
{
  if (_hButton) DestroyWindow(_hButton);
}

bool PropShCheckBox::Create(const tchar *name, const tchar *title, PropShItem *parent)
{
  if (PropShTitleOnly::Create(name,title,parent)==false) return false;
  _hButton=CreateWindow("BUTTON"," ",_style|WS_GROUP|WS_TABSTOP|WS_CHILD|WS_VISIBLE,0,0,0,0,parent->GetWindowHandle(),NULL,GetInstance(),NULL);
  return _hButton!=NULL;
}

void PropShCheckBox::UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height)
{
  height=CalculateFontHeight(_hTitle);
  int midl=ResizeTitle(hdwp,left,top,wspace,height);
  int bwidth=wspace-midl;
  if (_style==PushLike) bwidth=height;
  hdwp.SetWindowPos(_hButton,left+midl,top,bwidth,height);
  width=midl+bwidth;
}

void PropShCheckBox::BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam)
{
  SendMessage(_hButton,message,wParam,lParam);
  PropShTitleOnly::BroadcastMessage(message,wParam,lParam);
}

bool PropShCheckBox::GetBinaryData(void *buffer, size_t size) const
{
  if (size<1) return false;
  int chk=SendMessage(_hButton,BM_GETCHECK,0,0);
  *(char *)buffer=(char)chk;
  return true;
}

void PropShCheckBox::SetBinaryData(const void *data, size_t size)
{
  if (size<1) return;
  char *val=(char *)data;
  SendMessage(_hButton,BM_SETCHECK,*val,0);
}

