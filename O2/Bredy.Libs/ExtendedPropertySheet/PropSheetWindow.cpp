#include "PropShCommon.h"
#include ".\propsheetwindow.h"

PropSheetWindow::PropSheetWindow(void):_resizeX(true),_resizeY(true)
{
  _cinfo=NULL;
  _lockResize=false;
  _dlgfont=NULL;
}

PropSheetWindow::~PropSheetWindow(void)
{
  if (_dlgfont)
  {
    BroadcastMessage(WM_SETFONT,NULL,FALSE);
    DeleteObject(_dlgfont);
  }
}

bool PropSheetWindow::Create(const tchar *title, unsigned long style, int x, int y, int cx, int cy, HWND parentWnd, unsigned long exstyle, HMENU menu, HINSTANCE hInst)
{
  if (hInst==NULL) hInst=GetModuleHandle(NULL);
  CreationInfo nfo;
  nfo.x=x;
  nfo.y=y;
  nfo.cx=cx;
  nfo.cy=cy;
  nfo.hwndParent=parentWnd;
  nfo.style=style;
  nfo.menu=menu;  
  nfo.exstyle=exstyle;
  _cinfo=&nfo;
  bool res=PropShCanvas::CreateCanvas("",title,NULL,hInst);
  _cinfo=NULL;
  if (res) SetCanvasSize(cx,cy);
  return res;
}

HWND PropSheetWindow::CreateCanvasWindow(const tchar *className,const tchar *title, HWND parent, HINSTANCE hInst)
{
  if (_cinfo)
    return CreateWindowEx(_cinfo->exstyle,className,title,_cinfo->style,_cinfo->x,_cinfo->y,_cinfo->cx,
    _cinfo->cy,_cinfo->hwndParent,_cinfo->menu,hInst,this);
  else
    return PropShCanvas::CreateCanvasWindow(className,title,parent,hInst);
}

LRESULT PropSheetWindow::OnMessage(UINT message, WPARAM wParam, LPARAM lParam)
{
  switch (message)
  {
  case WM_SIZE:
    {
      if (_lockResize) return 0;
      int cx= LOWORD(lParam);  // width of client area 
      int cy = HIWORD(lParam); // height of client area       
      int fwSizeType = wParam;
      if (fwSizeType==SIZE_MINIMIZED) return 0;
      SetCanvasSize(_resizeX?cx:0,_resizeY?cy:0);
      if (*this) 
      {
        _lockResize=true;
        LayoutChanged();
        _lockResize=false;
      }
      return 0;
    }
  case WM_GETMINMAXINFO:
  {
    DefWindowProc(*this,message,wParam,lParam);
    MINMAXINFO *nfo=(MINMAXINFO *)lParam;
    if (_nohscroll || !_resizeX) nfo->ptMinTrackSize.x=_minWidth;
    if (_novscroll || !_resizeY) nfo->ptMinTrackSize.y=_minHeight;
    if (!_resizeX) nfo->ptMaxTrackSize.x=_minWidth;
    if (!_resizeY) nfo->ptMaxTrackSize.y=_minHeight;
    return 0;
  }
  case WM_SETFOCUS:
    if (_lastFocus) SetFocus(_lastFocus);
    break;
  case WM_ACTIVATE:
    {
    if (wParam!=WA_INACTIVE) { if ( _lastFocus) SetFocus(_lastFocus);}
    else _lastFocus=GetFocus();
    }
  default: return PropShGroup::OnMessage(message,wParam,lParam);
  }
  return 0;
}


void PropSheetWindow::ResizeCanvas(PropShHDWP &hdwp, int left, int top, int cx,int cy, int *outCx, int *outCy)
{
  PropShGroup::ResizeCanvas(hdwp,left,top,cx,cy,outCx,outCy);
  if (_nohscroll || _novscroll|| !_resizeX || !_resizeY)
  {
    HWND hWnd=*this;
    RECT rc;
    rc.left=0;rc.top=0; rc.right=_xsize; rc.bottom=_ysize;
    long scroll=GetWindowLong(*this,GWL_STYLE);
    AdjustWindowRectEx(&rc,GetWindowLong(hWnd,GWL_STYLE),GetMenu(hWnd)!=NULL,GetWindowLong(hWnd,GWL_EXSTYLE));  
    if (scroll & WS_VSCROLL) rc.right+=GetSystemMetrics(SM_CXVSCROLL);
    if (scroll & WS_HSCROLL) rc.bottom+=GetSystemMetrics(SM_CYHSCROLL);
    SIZE sz;
    sz.cx=rc.right-rc.left;
    sz.cy=rc.bottom-rc.top;
    if (*outCx>sz.cx) _minWidth=*outCx;else if (*outCx!=_minWidth) _minWidth=0;
    if (*outCy>sz.cy) _minHeight=*outCy;else if (*outCy!=_minHeight)  _minHeight=0;
  }
}

void PropSheetWindow::SetFont(const LOGFONT &font)
{
  HFONT oldFont=_dlgfont;
  _dlgfont=CreateFontIndirect(&font);
  BroadcastMessage(WM_SETFONT,(WPARAM)_dlgfont,TRUE);
  RecalcLayout();
  if (oldFont) DeleteObject(oldFont);
}

void PropSheetWindow::SetFont(const tchar *faceName, int size, bool bold, bool italic, int codepage)
{
  if (codepage=0) codepage=EASTEUROPE_CHARSET;
  LOGFONT lg;
  memset(&lg,0,sizeof(lg));
  lg.lfHeight=size;
  lg.lfWeight=bold?FW_BOLD:FW_NORMAL;
  lg.lfItalic=italic;
  lg.lfCharSet=codepage;
  strncpy(lg.lfFaceName,faceName,sizeof(lg.lfFaceName));
  SetFont(lg);
}

bool PropSheetWindow::IsMyMessage(const  MSG *msg)
{
  if (msg->message>=WM_KEYFIRST && msg->message<=WM_KEYLAST ||
      msg->message>=WM_MOUSEFIRST && msg->message<=WM_MOUSELAST ||
      msg->message==WM_MOUSEWHEEL)
  {
    HWND hSearchWnd=msg->hwnd;
    //search my window in hierarchy;
    while (hSearchWnd!=NULL && hSearchWnd!=*this) hSearchWnd=GetParent(hSearchWnd);
    if (hSearchWnd==NULL) return false; //not my window, not my message;
    if (msg->message==WM_SYSKEYDOWN)
	  {
		LRESULT res=0;
		switch (msg->wParam)
		{
		case VK_LEFT: if (ReflectMessage(GetFocus(),WM_COMMAND,PROPCMD_ACTIONBUTTON3,0,&res)) return true;
 	    case VK_UP: if (ReflectMessage(GetFocus(),WM_COMMAND,PROPCMD_ACTIONBUTTON1,0,&res)) return true;
		case VK_DOWN: if (ReflectMessage(GetFocus(),WM_COMMAND,PROPCMD_ACTIONBUTTON2,0,&res)) return true;
		case VK_RIGHT: if (ReflectMessage(GetFocus(),WM_COMMAND,PROPCMD_ACTIONBUTTON4,0,&res)) return true;
		}
	  }
    else if (msg->message==WM_KEYDOWN)
    {
	  if (GetKeyState(VK_CONTROL) & 0x8000)
	  {
		LRESULT res=0;
		if (msg->wParam==VK_SPACE)
		  if (ReflectMessage(GetFocus(),WM_COMMAND,PROPCMD_ACTIONBUTTON5,0,&res)) return true;
	  }
      switch (msg->wParam)
      {
      case VK_TAB:
      case VK_DOWN:
      case VK_UP:
      case VK_RETURN:
        {
          int code=SendMessage(msg->hwnd,WM_GETDLGCODE,0,0);
          if (code & DLGC_WANTALLKEYS) return false;
          if (code & DLGC_WANTMESSAGE) return false;
          if (code & DLGC_UNDEFPUSHBUTTON && msg->wParam==VK_RETURN) return false;
          if (GetCapture() && msg->wParam!=VK_TAB) return false;
/*          if (code & DLGC_WANTARROWS && (msg->wParam==VK_UP || msg->wParam==VK_DOWN)) return false;*/
          if (code & DLGC_WANTTAB && msg->wParam==VK_TAB) return false;
          bool previous=msg->wParam==VK_UP || msg->wParam==VK_TAB && (GetKeyState(VK_SHIFT) & 0x8000)!=0;
          HWND nextWnd=GetNextDlgTabItem(*this,msg->hwnd,previous);
          SetFocus(nextWnd);
          return true;
        }	  
      }
    }
  }
 return false; //not my message
}