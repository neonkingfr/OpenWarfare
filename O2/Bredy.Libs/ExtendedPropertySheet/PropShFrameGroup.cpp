#include "PropShCommon.h"
#include ".\propshframegroup.h"
#include "windowPos.h"

PropShFrameGroup::PropShFrameGroup(void):PropShGroup(false)
{
  _frame=NULL;
  _padding=8;
}

PropShFrameGroup::~PropShFrameGroup(void)
{
  if (_frame) DestroyWindow(_frame);
}

bool PropShFrameGroup::Create(const tchar *name, const tchar *title, PropShItem *parent)
{
  if (PropShGroup::Create(name,title,parent)==false) return false;
  _frame=CreateWindow("BUTTON",title,BS_GROUPBOX|WS_CHILD|WS_VISIBLE,0,0,0,0,GetWindowHandle(),NULL,GetInstance(),NULL);
  return _frame!=NULL;
}

void PropShFrameGroup::UpdateGroupContent(PropShHDWP &hdwp, int x, int y, int wspace, int hspace, int &width, int &height)
{
  int subwspace=wspace-_padding*2;
  SIZE titleSz=GetWindowsTextSize(_frame);

  int subhspace=hspace-titleSz.cy-_padding*2;
  int shiftx=x+_padding;
  int shifty=y+_padding+titleSz.cy;
  PropShHDWP subdwp;
  PropShGroup::UpdateGroupContent(subdwp,shiftx,shifty,subwspace,subhspace,width,height);
  width+=_padding*2;
  height+=_padding*2+titleSz.cy;
  subdwp.SetWindowPos(_frame,x,y,width,height);
}


void PropShFrameGroup::BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam)
{
  PropShGroup::BroadcastMessage(message,wParam,lParam);
  SendMessage(_frame,message,wParam,lParam);
}