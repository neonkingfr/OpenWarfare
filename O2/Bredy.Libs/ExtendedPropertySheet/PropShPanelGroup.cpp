#include "PropShCommon.h"
#include ".\propshpanelgroup.h"

PropShPanelGroup::PropShPanelGroup(bool border):PropShGroup(border)
{
  _button=NULL;
  _title=NULL;
}

PropShPanelGroup::~PropShPanelGroup(void)
{
  if (_button!=NULL) DestroyWindow(_button);
  free(_title);
}

bool PropShPanelGroup::Create(const tchar *name, const tchar *title, PropShItem *parent)
{
  if (PropShGroup::Create(name,title,parent)==false) return false;
  _button=CreateWindow("BUTTON",title,WS_TABSTOP|WS_CHILD|WS_VISIBLE|BS_AUTOCHECKBOX|BS_PUSHLIKE|BS_TEXT|BS_VCENTER|BS_CENTER,
    0,0,0,0,*this,(HMENU)9999,GetInstance(),NULL);
  _title=_tcsdup(title);
  UpdateButtonTitle();
  return _button!=NULL;
}


void PropShPanelGroup::UpdateButtonTitle()
{
  tchar *tempText=(tchar *)alloca((strlen(_title)+10)*sizeof(*_title));
  _stprintf(tempText,"%c %s",(SendMessage(_button,BM_GETSTATE,0,0) & 0x3)==BST_CHECKED?'-':'+',_title);
  SetWindowText(_button,tempText);
}


 void PropShPanelGroup::UpdateGroupContent(PropShHDWP &hdwp, int x, int y, int wspace, int hspace, int &width, int &height)
 {
   int textlen=GetWindowTextLength(_button)+1;
   tchar *tempText=(tchar *)alloca(textlen*sizeof(*_title));
   GetWindowText(_button,tempText,textlen);
   HFONT font=(HFONT)SendMessage(_button,WM_GETFONT,0,0);
   HDC wdc=GetDC(_button);
   HFONT oldfnd=(HFONT)SelectObject(wdc,font);
   SIZE sz;
   GetTextExtentPoint32(wdc,tempText,_tcslen(tempText),&sz);
   SelectObject(wdc,oldfnd);
   ReleaseDC(_button,wdc);
   PropShHDWP subdwp;
   subdwp.SetWindowPos(_button,x,y,sz.cx+20,sz.cy+5);
   int bh=sz.cy+5+_itemSpace;
   y+=bh;
   hspace-=bh;
   if ((SendMessage(_button,BM_GETSTATE,0,0) & 0x3)==BST_CHECKED)
   {
     PropShGroup::UpdateGroupContent(subdwp,x,y,wspace,hspace,width,height);
     height+=bh;
   }
   else
   {
     if (_dirty) 
     {
       PropShGroup::UpdateGroupContent(subdwp,x,y,wspace,hspace,width,height);
       EnableAllItems(false);
     }
     _dirty=false;     
     width=wspace;
     height=bh-_paddingX;
     SetScrollPos(*this,SB_VERT,0,FALSE);
     SetScrollPos(*this,SB_HORZ,0,FALSE);
   }
 }

LRESULT PropShPanelGroup::OnCommand(WORD wNotifyCode, WORD wId, HWND ctl)
{
  if (wId==9999)
  {
    UpdateButtonTitle();
    SetScrollPos(*this,SB_VERT,0,FALSE);
    SetScrollPos(*this,SB_HORZ,0,FALSE);
    bool opened=(SendMessage(_button,BM_GETSTATE,0,0) & 0x3)==BST_CHECKED;
    EnableAllItems(opened);
    LayoutChanged();
    return 0;
  }
  else
    return PropShGroup::OnCommand(wNotifyCode,wId,ctl);
}

void PropShPanelGroup::BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam)
{
  SendMessage(_button,message,wParam,lParam);
  PropShGroup::BroadcastMessage(message,wParam,lParam);
}

void PropShPanelGroup::EnableAllItems(bool enable)
{
    HWND trws=GetNextWindow(_button,GW_HWNDNEXT);
    while (trws)
    {
      ShowWindow(trws,enable?SW_SHOW:SW_HIDE);
      trws=GetNextWindow(trws,GW_HWNDNEXT);
    }
}

