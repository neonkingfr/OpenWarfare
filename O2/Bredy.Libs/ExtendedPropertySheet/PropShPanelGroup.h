#pragma once
#include "propshgroup.h"

class PropShPanelGroup :public PropShGroup
{
  HWND _button;
  tchar *_title;
  void EnableAllItems(bool enable);
public:
  PropShPanelGroup(bool border=false);
  ~PropShPanelGroup(void);

  bool PropShPanelGroup::Create(const tchar *name, const tchar *title, PropShItem *parent=NULL);
  virtual void UpdateGroupContent(PropShHDWP &hdwp, int x, int y, int wspace, int hspace, int &width, int &height);
  virtual LRESULT OnCommand(WORD wNotifyCode, WORD wId, HWND ctl);
  void UpdateButtonTitle();
  virtual void BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam);


};
