#define WIN32_LEAN_AND_MEAN
#ifndef _WIN32_WINNT 
#define _WIN32_WINNT 0x500
#endif
#include <windows.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <windowsx.h>

#define PROPSH_TITLESIZE 1
