// PropShBase.h: interface for the PropShBase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PROPSHBASE_H__C8DAB9B7_2B36_492A_ABA4_7AA647FD8968__INCLUDED_)
#define AFX_PROPSHBASE_H__C8DAB9B7_2B36_492A_ABA4_7AA647FD8968__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <TCHAR.h>
#include <typeinfo.h>

typedef _TCHAR tchar;

#define tsizeof(x) (sizeof(x)/sizeof(x[0]));

/// base class for Extender Property Sheet
class PropShHDWP;


class PropShBase  
{
public:
	virtual ~PropShBase()=0 {}
	///Recalculates layout and updates window positions
	/**
	@param hdwp object used for window reposition.
	@param left left offset of current item
	@param top top offset of current item
	@param wspace width that can be used for item
	@param height out: height of item after reposition
	@param width out: width of item after reposition
	*/
	virtual void UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height)=0;
	virtual const tchar *GetName() const=0;
	
	virtual int GetDataSize() const {return 0;}
	virtual bool GetStringData(tchar *buffer, size_t size) const {return false;}
	virtual bool GetBinaryData(void *buffer, size_t size) const {return false;}
	
	virtual void SetStringData(const tchar *data) {}
	virtual void SetBinaryData(const void *data, size_t size) {}

	virtual const tchar *IsMyName(const tchar *name) const
	{
	  const tchar *myname=GetName();
	  while (*name==*myname) 
		if (!*name) return name;
		else {name++;myname++;}
	  return NULL;
	}

    virtual void EnableItem(bool enable)=0;

    virtual PropShBase *Find(const tchar *name) const {return NULL;}

    virtual bool ItemPropertySupport(int propertyId, void *data, const type_info &type, bool retrieve) {return false;}

    template <class T>
    bool SetItemProperty(int propertyId, const T &value)
    {
      return ItemPropertySupport(propertyId,const_cast<T *>(&value),typeid(value), false);
    }

    template <class T>
    bool GetItemProperty(int propertyId, T &value)
    {
      return ItemPropertySupport(propertyId,&value,typeid(value), true);
    }
	

    template <class T>
    void GetItemProperty(int propertyId, T &value, const T &defaultVal)
    {
      if (!ItemPropertySupport(propertyId,&value,typeid(value), true))
        value=defaultVal;
    }

    	
	
};

#endif // !defined(AFX_PROPSHBASE_H__C8DAB9B7_2B36_492A_ABA4_7AA647FD8968__INCLUDED_)
