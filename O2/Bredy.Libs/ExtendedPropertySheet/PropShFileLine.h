#pragma once
#include "propshtextwithbutton.h"

class PropShFileLine : public PropShTextWithButton
{
protected:
  tchar *_filter;  
  tchar *_defext;
  tchar *_defpath;
  unsigned long _flags;
  bool _save;
public:
  PropShFileLine(const tchar *filter, const tchar *defext, bool saveDlg, unsigned long flags, const tchar *defpath=NULL);
  ~PropShFileLine(void);
  void OnButtonPressed();
  void SetStringData(const tchar *data);
};
