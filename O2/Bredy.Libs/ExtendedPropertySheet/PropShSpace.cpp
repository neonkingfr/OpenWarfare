#include "PropShCommon.h"
#include ".\propshspace.h"

PropShSpace::PropShSpace(int vspace, int hspace):_vspace(vspace),_hspace(hspace)
{
}

void PropShSpace::UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height)
{
  if (_hspace<1) width=wspace+_hspace-left; else width=_hspace;
  if (_vspace<1) height=hspace+_vspace-top;else height=_vspace;
  if (height<0) height=0;
  if (width<0) width=0;
}
