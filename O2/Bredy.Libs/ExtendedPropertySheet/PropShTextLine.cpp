// PropShTextLine.cpp: implementation of the PropShTextLine class.
//
//////////////////////////////////////////////////////////////////////

#include "PropShCommon.h"
#include "PropShTextLine.h"
#include "WindowPos.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

PropShTextLine::PropShTextLine():_hTitle(NULL),_hControl(NULL)
{

}

PropShTextLine::~PropShTextLine()
{
if (_hTitle) DestroyWindow(_hTitle);
if (_hControl) DestroyWindow(_hControl);
}

bool PropShTextLine::Create(const tchar *name, const tchar *title, PropShItem *parent/*=NULL*/)
{
  if (PropShItem::Create(name,title,parent)==false) return false;
  HWND hparent=parent?parent->GetWindowHandle():NULL;
  HINSTANCE hInst=hparent?(HINSTANCE)GetWindowLong(hparent,GWL_HINSTANCE):GetModuleHandle(NULL);
  _hTitle=CreateWindow("STATIC",title,WS_CHILD|SS_LEFT|SS_CENTERIMAGE|SS_ENDELLIPSIS|WS_VISIBLE,0,0,100,100,hparent,NULL,hInst,NULL);
  _hControl=CreateWindow("EDIT","",WS_TABSTOP|WS_CHILD|WS_VISIBLE|ES_AUTOHSCROLL|ES_LEFT,0,0,100,100,hparent,NULL,hInst,NULL);
  return _hTitle!=NULL && _hControl!=NULL;	
}

void PropShTextLine::UpdateLayout(PropShHDWP &hdwp, int left, int top, int wspace, int hspace, int &width, int &height)
{
  height=CalculateFontHeight(_hControl);
  int title=ResizeTitle(hdwp,left,top,wspace,height);
  hdwp.SetWindowPos(_hControl,left+title,top,wspace-title,height);
  width=wspace;  
}

void PropShTextLine::BroadcastMessage(UINT message, WPARAM wParam, LPARAM lParam)
{
  SendMessage(_hTitle,message,wParam,lParam);
  SendMessage(_hControl,message,wParam,lParam);
}

int PropShTextLine::GetDataSize() const
{
  return GetWindowTextLength(_hControl);
}
bool PropShTextLine::GetStringData(tchar *buffer, size_t size) const
{
  GetWindowText(_hControl,buffer, size);
  return true;
}
void PropShTextLine::SetStringData(const tchar *data)
{
  SetWindowText(_hControl,data);
}

int PropShTextLine::ResizeTitle(PropShHDWP &dwp, int left, int top, int wspace, int hspace)
{
  float percentsize;
  GetParentItem()->GetItemProperty(PROPSH_TITLESIZE,percentsize,0.5f);
  int midl=percentsize<1?((int)(percentsize*wspace)):((int)percentsize);
  dwp.SetWindowPos(_hTitle,left,top,midl,hspace);
  InvalidateRect(_hTitle,NULL,TRUE);
  return midl;
}

int PropShTextLine::CalculateFontHeight(HWND hControl)
{
/*  HDC infc=::GetDC(_hControl);
  HFONT font=(HFONT)SendMessage(hControl,WM_GETFONT,0,0);
  HFONT old=(HFONT)SelectObject(infc,font);
  SIZE sz;
  GetTextExtentPoint32(infc,"W",1,&sz);
  int height=sz.cy+2;
  SelectObject(infc,old);
  ReleaseDC(_hControl,infc);
  */  
  return GetWindowsTextSize(hControl).cy;
}

void PropShTextLine::EnableItem(bool enable)
{    
    Edit_SetReadOnly(_hControl,!enable); 
}