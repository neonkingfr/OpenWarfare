// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "GraphView.h"

#include "MainFrm.h"
#include "InsertItemDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)
  
  BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
    //{{AFX_MSG_MAP(CMainFrame)
    ON_WM_SETFOCUS()
      ON_WM_CREATE()
        ON_COMMAND(ID_EDIT_INSERTITEM, OnEditInsertitem)
          ON_COMMAND(ID_EDIT_DELETEITEMS, OnEditDeleteitems)
            ON_UPDATE_COMMAND_UI(ID_EDIT_DELETEITEMS, OnUpdateEditDeleteitems)
              ON_COMMAND(ID_PROPERTIES_EDITITEM, OnPropertiesEdititem)
                ON_UPDATE_COMMAND_UI(ID_PROPERTIES_EDITITEM, OnUpdatePropertiesEdititem)
                  ON_WM_CONTEXTMENU()
                    ON_UPDATE_COMMAND_UI(ID_PROPERTIES_CANBELINKED, OnUpdatePropertiesCanbelinked)
                      ON_UPDATE_COMMAND_UI(ID_PROPERTIES_CANMAKELINKS, OnUpdatePropertiesCanmakelinks)
                        ON_UPDATE_COMMAND_UI(ID_PROPERTIES_CANMOVE, OnUpdatePropertiesCanmove)
                          ON_UPDATE_COMMAND_UI(ID_PROPERTIES_CANSELFLINKED, OnUpdatePropertiesCanselflinked)
                            ON_UPDATE_COMMAND_UI(ID_PROPERTIES_CANSIZE, OnUpdatePropertiesCansize)
                              ON_UPDATE_COMMAND_UI(ID_PROPERTIES_CUSTOMDRAW, OnUpdatePropertiesCustomdraw)
                                ON_COMMAND(ID_PROPERTIES_CUSTOMDRAW, OnPropertiesCustomdraw)
                                  ON_COMMAND(ID_PROPERTIES_CANSIZE, OnPropertiesCansize)
                                    ON_COMMAND(ID_PROPERTIES_CANSELFLINKED, OnPropertiesCanselflinked)
                                      ON_COMMAND(ID_PROPERTIES_CANMOVE, OnPropertiesCanmove)
                                        ON_COMMAND(ID_PROPERTIES_CANMAKELINKS, OnPropertiesCanmakelinks)
                                          ON_COMMAND(ID_PROPERTIES_CANBELINKED, OnPropertiesCanbelinked)
                                            ON_COMMAND(ID_LINK_NORMAL, OnLinkNormal)
                                              ON_COMMAND(ID_LINK_DESIGN, OnLinkDesign)
                                                ON_COMMAND(ID_LINK_ZOOM, OnLinkZoom)
                                                  ON_UPDATE_COMMAND_UI(ID_LINK_NORMAL, OnUpdateLinkNormal)
                                                    ON_UPDATE_COMMAND_UI(ID_LINK_ZOOM, OnUpdateLinkZoom)
                                                      ON_UPDATE_COMMAND_UI(ID_LINK_DESIGN, OnUpdateLinkDesign)
                                                        //}}AFX_MSG_MAP
    END_MESSAGE_MAP()
      
      /////////////////////////////////////////////////////////////////////////////
      // CMainFrame construction/destruction
      
      CMainFrame::CMainFrame()
      {
        // TODO: add member initialization code here
        
      }

CMainFrame::~CMainFrame()
{
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
  if( !CFrameWnd::PreCreateWindow(cs) )
    return FALSE;
  // TODO: Modify the Window class or styles here by modifying
  //  the CREATESTRUCT cs
  
  cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
  cs.lpszClass = AfxRegisterWndClass(0);
  return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
  CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
  CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
void CMainFrame::OnSetFocus(CWnd* pOldWnd)
{
  // forward focus to the view window
  View.SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
  // let the view have first crack at the command
  if (View.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
    return TRUE;
  
  // otherwise, do default handling
  return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
  if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
    return -1;
  
  View.Create(NULL,"",WS_VISIBLE|WS_CHILD,CRect(0,0,0,0),this,AFX_IDW_PANE_FIRST,NULL);
  srand(GetTickCount());
  
  DWORD yellow=RGB(255,255,128);
  DWORD cian=RGB(125,255,255);
  DWORD green=RGB(128,255,128);
  DWORD blue=RGB(128,128,255);
  int a=View.InsertItem(SGRI_NORMALSELFLINK, yellow,"Ko�en",20,10);
  int b=View.InsertItem(SGRI_NORMALSELFLINK, cian,"Kmen 1",10,5);
  int c=View.InsertItem(SGRI_NORMALSELFLINK, cian,"Kmen 2",10,5);
  int d=View.InsertItem(SGRI_NORMALSELFLINK, green,"List 1",5,2.5f);
  int e=View.InsertItem(SGRI_NORMALSELFLINK, green,"List 2",5,2.5f);
  View.InsertLink(a,b,blue);
  View.InsertLink(b,c,blue);
  View.InsertLink(c,d,blue);
  View.InsertLink(b,d,blue);
  View.InsertLink(c,e,blue);
  View.InsertLink(b,a,blue);
  View.InsertLink(c,c,blue);
  View.InsertLink(e,e,blue);
  View.OrderLinks();
  ResetZoom();
  return 0;
}

void CMainFrame::OnEditInsertitem() 
{
  static DWORD defflags=SGRI_NORMAL;
  static float defdimx=10;
  static float defdimy=5;
  static DWORD defcolor=RGB(255,255,128);
  CInsertItemDlg dlg;
  dlg.flags=defflags;
  dlg.m_dimx=defdimx;
  dlg.m_dimy=defdimy;
  dlg.color=defcolor;
  if (dlg.DoModal()==IDOK)
  {
    defdimx=dlg.m_dimx;
    defdimy=dlg.m_dimy;
    defflags=dlg.flags;
    defcolor=dlg.color;
    int i=View.InsertItem(dlg.flags|SGRI_NOGUESSPOSITION|SGRI_FILLBKCOLOR,defcolor,dlg.text,dlg.m_dimx,dlg.m_dimy);
    SFloatRect rc=View.GetPanZoom();
    float xc=(rc.left+rc.right)*0.5f;
    float yc=(rc.bottom+rc.top)*0.5f;
    rc=View.GetItemRect(i);
    rc.left+=xc;
    rc.right+=xc;
    rc.top+=yc;
    rc.bottom+=yc;
    View.SetItemRect(i,rc);	
    View.Update();
  }
}

void CMainFrame::ResetZoom()
{
  SFloatRect rc=View.GetBoundingBox();
  float xs=(rc.right-rc.left)*0.2f;
  float ys=(rc.bottom-rc.top)*0.2f;
  rc.left-=xs;
  rc.top-=ys;
  rc.right+=xs;
  rc.bottom+=ys;
  View.SetPanZoom(rc,true);
}

void CMainFrame::OnEditDeleteitems() 
{
  if (AfxMessageBox(IDS_QDELETE,MB_YESNO|MB_ICONQUESTION)==IDNO) return;
  for (int i=-1;(i=View.EnumItems(i,true))!=-1;) 
    View.DeleteItem(i);
  View.Update();
}

void CMainFrame::OnUpdateEditDeleteitems(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(View.EnumItems(-1,true)!=-1);	
}

void CMainFrame::OnPropertiesEdititem() 
{
  int i=View.EnumItems(-1,true);
  CInsertItemDlg dlg;
  SGraphItem it;
  View.GetItem(i,&it);
  dlg.color=it.color;
  dlg.text=it.text;
  dlg.flags=it.flags;
  dlg.m_dimx=it.rect.right-it.rect.left;
  dlg.m_dimy=it.rect.bottom-it.rect.top;
  if (dlg.DoModal()==IDOK)
  {
    it.color=dlg.color;
    strncpy(it.text,dlg.text,SGRI_MAXTEXTLEN);
    it.text[SGRI_MAXTEXTLEN-1]=0;
    it.flags=dlg.flags|SGRI_FILLBKCOLOR;
    it.rect.right=it.rect.left+dlg.m_dimx;
    it.rect.bottom=it.rect.top+dlg.m_dimy;
    View.SetItem(i,&it);
    View.Update();
  }
}

void CMainFrame::OnUpdatePropertiesEdititem(CCmdUI* pCmdUI) 
{
  int i=View.EnumItems(-1,true);
  pCmdUI->Enable(i!=-1 && View.EnumItems(i,true)==-1);	
}

void CMainFrame::OnContextMenu(CWnd* pWnd, CPoint point) 
{
  static bool zoomwarn=true;
  if (pWnd==&View)
  {
    int l=View.GetFocusedLink();
    if (l==-1 && (GetKeyState(VK_SHIFT) & 0x80))
    {
      CPoint q=point;
      View.ScreenToClient(&q);
      l=View.LinkFromPoint(q);
    }
    CMenu mnu;
    mnu.LoadMenu(IDR_LINKMENU);
    CMenu *popup=mnu.GetSubMenu(0+(l==-1));
    short flg=l!=-1?View.GetLinkFlags(l):0;
    if (flg & SGRI_FROZEN) popup->CheckMenuItem(ID_LINK_FROZEN,MF_CHECKED);
    if (flg & SGRI_CUSTOMDRAW) popup->CheckMenuItem(ID_LINK_ZAPNOUTPOPISEK,MF_CHECKED);
    if (flg & SGRI_DONTFOCUS) popup->CheckMenuItem(ID_LINK_DONTFOCUS,MF_CHECKED);
    switch (View.Mode(SGRM_GETCURRENT))
    {
      case SGRM_NORMAL: popup->CheckMenuItem(ID_LINK_NORMAL,MF_CHECKED);break;
      case SGRM_DESIGN: popup->CheckMenuItem(ID_LINK_DESIGN,MF_CHECKED);break;
      case SGRM_ZOOM: popup->CheckMenuItem(ID_LINK_ZOOM,MF_CHECKED);break;
    }
    int cmd=popup->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON|TPM_NONOTIFY|TPM_RETURNCMD,point.x,point.y,this,NULL);
    switch (cmd)
    {
      case ID_LINK_FROZEN: View.SetLinkFlags(l,SGRI_FROZEN,0);break;
      case ID_LINK_ZAPNOUTPOPISEK: View.SetLinkFlags(l,SGRI_CUSTOMDRAW,0);break;
      case ID_LINK_NORMAL: View.Mode(SGRM_NORMAL);break;
      case ID_LINK_DESIGN: View.Mode(SGRM_DESIGN);break;
      case ID_LINK_ZOOM: if (zoomwarn) 
      {AfxMessageBox(IDS_ZOOMWARNING);zoomwarn=false;}
      View.Mode(SGRM_ZOOM);break;
      case ID_LINK_DONTFOCUS: if (~View.GetLinkFlags(l) & SGRI_DONTFOCUS)AfxMessageBox(IDS_DONTFOCUSWARNING,MB_OK);
      View.SetLinkFlags(l,SGRI_DONTFOCUS,0);break;
    }
    View.Update();
  }
}

void CMainFrame::OnUpdatePropertiesCanbelinked(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck((View.GetSelectionFlags() & SGRI_CANBELINKED)!=0);
}

void CMainFrame::OnUpdatePropertiesCanmakelinks(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck((View.GetSelectionFlags() & SGRI_CANMAKELINKS)!=0);
}

void CMainFrame::OnUpdatePropertiesCanmove(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck((View.GetSelectionFlags() & SGRI_CANMOVE)!=0);
}

void CMainFrame::OnUpdatePropertiesCanselflinked(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck((View.GetSelectionFlags() & SGRI_CANSELFLINKED)!=0);
}

void CMainFrame::OnUpdatePropertiesCansize(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck((View.GetSelectionFlags() & SGRI_CANRESIZE)!=0);
}

void CMainFrame::OnUpdatePropertiesCustomdraw(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck((View.GetSelectionFlags() & SGRI_CUSTOMDRAW)!=0);
}

void CMainFrame::OnPropertiesCustomdraw() 
{
  View.SetFlagSelection(SGRI_CUSTOMDRAW|SGRI_FILLBKCOLOR,SGRI_FILLBKCOLOR );	
  View.Update();
}

void CMainFrame::OnPropertiesCansize() 
{
  View.SetFlagSelection(SGRI_CANRESIZE,0);	
  View.Update();
}

void CMainFrame::OnPropertiesCanselflinked() 
{
  View.SetFlagSelection(SGRI_CANSELFLINKED,0);	
  View.Update();
}

void CMainFrame::OnPropertiesCanmove() 
{
  View.SetFlagSelection(SGRI_CANMOVE,0);	
  View.Update();
}

void CMainFrame::OnPropertiesCanmakelinks() 
{
  View.SetFlagSelection(SGRI_CANMAKELINKS,0);	
  View.Update();
}

void CMainFrame::OnPropertiesCanbelinked() 
{
  View.SetFlagSelection(SGRI_CANBELINKED,0);	
  View.Update();
}

void CMainFrame::OnLinkNormal() 
{
  View.Mode(SGRM_NORMAL);	
}

void CMainFrame::OnLinkDesign() 
{
  View.Mode(SGRM_DESIGN);
}

void CMainFrame::OnLinkZoom() 
{
  View.Mode(SGRM_ZOOM);	
}

void CMainFrame::OnUpdateLinkNormal(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(View.Mode(SGRM_GETCURRENT)==SGRM_NORMAL);	
}

void CMainFrame::OnUpdateLinkZoom(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(View.Mode(SGRM_GETCURRENT)==SGRM_ZOOM);	
}

void CMainFrame::OnUpdateLinkDesign(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(View.Mode(SGRM_GETCURRENT)==SGRM_DESIGN);	
}

