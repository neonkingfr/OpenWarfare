#include "common.h"
#include ".\ipcsharedmem.h"

#define RESERVESIZE(p) ((void *)((size_t *)(p)+1))

SendPackageSharedMem::~SendPackageSharedMem(void)
{
  if (_lockedData) UnmapViewOfFile(_lockedData);
  if (_sharedMem!=NULL) CloseHandle(_sharedMem);
}

void *SendPackageSharedMem::Lock(size_t sz)
{
  if (_lockedData && sz<_lockedSize) { return _lockedData;}
  if (_sharedMem==NULL)
  {
    _sharedMem=CreateFileMapping(NULL,NULL,PAGE_READWRITE,0,(DWORD)sz+sizeof(size_t),NULL);
    if (_sharedMem==NULL) return NULL;
    _lockedData=MapViewOfFile(_sharedMem,FILE_MAP_ALL_ACCESS,0,0,0);
    _lockedSize=sz;
    _sharedAlloc=sz;
    return RESERVESIZE(_lockedData);
  }
  else
  {
    if (_lockedData) Unlock(_lockedSize);
    if (sz>_sharedAlloc) 
    {      
      _lockedData=MapViewOfFile(_sharedMem,FILE_MAP_ALL_ACCESS,0,0,0);
      _lockedSize=_sharedAlloc;
      if (_lockedData==NULL) return NULL;
      HANDLE copy=CreateFileMapping(NULL,NULL,PAGE_READWRITE,0,(DWORD)sz+sizeof(size_t),NULL);
      if (copy==NULL) return NULL;
      void *cpy=MapViewOfFile(copy,FILE_MAP_ALL_ACCESS,0,0,0);
      if (cpy==NULL) {CloseHandle(copy);return NULL;}
      memcpy(cpy,_lockedData,_sharedSize+sizeof(size_t));
      UnmapViewOfFile(_lockedData);
      _lockedData=cpy;
      _lockedSize=sz;
      _sharedAlloc=sz;
      return RESERVESIZE(_lockedData);
    }
    else
    {
      _lockedData=MapViewOfFile(_sharedMem,FILE_MAP_ALL_ACCESS,0,0,sz);
      _lockedSize=sz;      
      return RESERVESIZE(_lockedData);
    }
  }
}

bool SendPackageSharedMem::Unlock(size_t sz)
{
  if (sz==0) sz=_lockedSize;
  if (_lockedData)
  {
    _sharedSize=__max(sz,_sharedSize);
    *(size_t *)_lockedData=_sharedSize;
    UnmapViewOfFile(_lockedData);
    _lockedData=NULL;
    return true;
  }
  else
    return false;
}

size_t SendPackageSharedMem::Write(const void *data, size_t sz, bool block)
{
  if (_lockedData==NULL)
  {
    if (_sharedAlloc && _sharedSize+sz>_sharedAlloc) Lock(_sharedAlloc*2);
  }
  char *p=(char *)Lock(_sharedSize+sz);
  if (p==NULL) return 0;
  memcpy(p+_sharedSize,data,sz);
  _sharedSize+=sz;
  return sz;
}

bool SendPackageSharedMem::SetOption(unsigned long type, int data)
{
  switch (type)
  {
  case typeWParam: _wParam=data;return true;
  case typeMessage: _message=data;return true;
  case OptSendTimeout: _sendTimeOut=data;return true;
  case OptSendResult: _sendResult=(DWORD_PTR *)data;return true;
  }
  return false;
}

#include <Tlhelp32.h>
static DWORD FindProcessToThread(DWORD thread)
{
  HANDLE snapshot=CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD,0);
  THREADENTRY32 entry;
  if (Thread32First(snapshot,&entry))
  {
    do
    {
      if (entry.th32ThreadID==thread) 
      {
        CloseHandle(snapshot);
        return entry.th32OwnerProcessID;
      }
    }
    while (Thread32Next(snapshot,&entry));
  }
  return 0;
}


bool SendPackageSharedMem::SendPackage()
{
  if (_lockedData) Unlock(0);
  HANDLE targetprocess=NULL;
  if (_targetWnd!=0)
  {
    DWORD process;
    if (::GetWindowThreadProcessId(_targetWnd,&process)==0) return false;
    targetprocess=OpenProcess(PROCESS_DUP_HANDLE,FALSE,process);
    if (targetprocess==NULL) return false;
  }
  else
  {
    DWORD process=FindProcessToThread(_targetThread);
    if (process==0) return false;
    targetprocess=OpenProcess(PROCESS_DUP_HANDLE,FALSE,process);
    if (targetprocess==NULL) return false;
  }
  HANDLE otherSide;
  if (DuplicateHandle(GetCurrentProcess(),_sharedMem,targetprocess,&otherSide,0,TRUE,DUPLICATE_SAME_ACCESS)==FALSE)
    {CloseHandle(targetprocess);return false;}
  BOOL succ=FALSE;
  if (_sendResult && _targetWnd)
  {
    succ=SendMessageTimeout(_targetWnd,_message,_wParam,(LPARAM)otherSide,SMTO_NORMAL,_sendTimeOut,_sendResult);
  }
  else
  {    
    if (_targetThread)
    {
      succ=PostThreadMessage(_targetThread,_message,_wParam,(LPARAM)otherSide);
    }
    else if (_targetWnd)
    {
      succ=PostMessage(_targetWnd,_message,_wParam,(LPARAM)otherSide);
    }
  }
  CloseHandle(targetprocess);
  return succ!=FALSE;
}

ConnectionSharedMem::ConnectionSharedMem(HWND targetWnd, UINT message):
  _targetWnd(targetWnd),_targetThread(0),_message(message)
  {
  }

ConnectionSharedMem::ConnectionSharedMem(DWORD targetThread, UINT message):
  _targetWnd(0),_targetThread(targetThread),_message(message)
  {
  }

ISendPackage *ConnectionSharedMem::OpenPackage()
{
  return new SendPackageSharedMem(_targetWnd,_targetThread,_message);
}

bool ConnectionSharedMem::ClosePackage(ISendPackage *package)
{
#ifdef _CPPRTTI
SendPackageSharedMem *pk=dynamic_cast<SendPackageSharedMem *>(package);
#else
SendPackageSharedMem *pk=static_cast<SendPackageSharedMem *>(package);
#endif
if (pk==NULL) return false;
bool succ=pk->SendPackage();
delete pk;
return succ;
}

ReceivedPackageSharedMem::ReceivedPackageSharedMem(HANDLE sharedMem)
{
  _sharedMem=sharedMem;
  size_t *sz=(size_t *)MapViewOfFile(sharedMem,FILE_MAP_READ,0,0,0);
  if (sz==NULL) 
  {
    _data=NULL;
    _size=0;    
  }
  else
  {
    _size=*sz;
    _data=(void *)(sz+1);
  }
  _rdpos=0;
}
ReceivedPackageSharedMem::~ReceivedPackageSharedMem()
{
  if (_data) UnmapViewOfFile((size_t *)_data-1);
  if (_sharedMem) CloseHandle(_sharedMem);
}

size_t ReceivedPackageSharedMem::Read(void *buffer, size_t size, bool block)
{
  if (_rdpos+size>_size) size=_size-_rdpos;
  memcpy(buffer,(char *)_data+_rdpos,size);
  _rdpos+=size;
  return size;
}


ConnReceiverSharedMem::ConnReceiverSharedMem():_current(0) {}

IReceivedPackage *ConnReceiverSharedMem::OpenPackage(ICHANDLE dataHandle, bool detach)
{
  IReceivedPackage *pack=new ReceivedPackageSharedMem((HANDLE)dataHandle);
  if (!detach)
  {
    if (_current) delete _current;
    pack=_current;
  }
  return pack;
}

bool ConnReceiverSharedMem::ClosePackage(IReceivedPackage *package)
{
  if (package->PackageTag()==ReceivedPackageSharedMem_TAG)
  {
    if (package==_current) _current=NULL;
    delete package;
    return true;
  }
  return false;
}

ConnReceiverSharedMem::~ConnReceiverSharedMem(void)
{
  if (_current) delete _current;
}