#pragma once
#include "common.h"
#include "IProcComunicator.h"


class SendPackagePipe: public ISendPackage
{
  HWND _target;
  UINT _message;
  DWORD _thread;
  WPARAM _wParam;
  LPARAM _lParam;
  HANDLE _thisSide;
  void *_lockData;
  size_t _lockSize;
  enum ConState {notConnected,Connected,LostConn};
  ConState _state;  
  bool _blocking;
 public:
  SendPackagePipe(HWND target, UINT message, DWORD thread);
  ~SendPackagePipe();

  virtual void *Lock(size_t sz);
  virtual bool Unlock(size_t sz=0);
  virtual size_t Write(const void *data, size_t sz, bool block=true);
  virtual bool Lost();

  virtual bool SetOption(unsigned long type, int data);
protected:
  bool InitiateConnection();
};

class ConnectionPipe: public IConnection
{
  UINT _message;
  HWND _target;
  DWORD _thread;

public:
  ConnectionPipe(HWND target, UINT message):_message(message),_target(target),_thread(0) {}
  ConnectionPipe(DWORD thread, UINT message):_message(message),_thread(thread),_target(0) {}
  virtual ISendPackage *OpenPackage();
  virtual bool ClosePackage(ISendPackage *package);
};

#define ReceivedPackagePipe_TAG 5153
class ReceivedPackagePipe:public IReceivedPackage
{  
  HANDLE _pipe;
  bool _eos;
  bool _blocking;
public:  
  ReceivedPackagePipe(HANDLE pipe);
  ~ReceivedPackagePipe(void);
  
  ///Returns size of package. Returns 0 if package is stream
  virtual size_t Size() const  {return 0;}
  ///Returns true, if package is streamed
  virtual bool IsStream() const {return true;}
  ///Reads package
  virtual size_t Read(void *pointer, size_t size, bool block=true);

  ///function return true, when end of stream has been reached
  virtual bool Eos() const {return _eos;}

  ///Function returns pointer to received data.
  /**Function returns NULL, when package is stream. 
  Generally, it is more flexibile using Read function
  */
  virtual const void *Data() const {return NULL;}

  ///function returns package's tag. It is useful to separate different types of packages
  virtual unsigned long PackageTag() const {return ReceivedPackagePipe_TAG;}
};

class ConnReceiverPipe
{
  IReceivedPackage *_curPackage;
public:  
  virtual IReceivedPackage *OpenPackage(ICHANDLE dataHandle, bool detach=true);
  virtual bool ClosePackage(IReceivedPackage *package);
  virtual ~ConnReceiverPipe(void);
};

