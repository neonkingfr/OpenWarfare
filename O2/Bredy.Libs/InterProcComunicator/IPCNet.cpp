#include <stdio.h>
#include "IPCNet.h"

SendPackageTCP::~SendPackageTCP()
{
  free(_data);
}

SendPackageTCP::SendPackageTCP()
{
  _data=0;
  _size=_alloc=_pos=0;
}

void *SendPackageTCP::Lock(size_t sz)
{
  size_t needsz=_pos+sz;
  if (needsz>_alloc)
  {
    if (needsz-_size>4096) _alloc=needsz;
    else _alloc=_size+4096;
    void *p=realloc(_data,_alloc);
    if (p==0) return 0;
    _data=p;
  }
  if (_pos+sz>_size) _size=_pos+sz;
  return (void *)((char *)_data+_pos);
}

bool SendPackageTCP::Unlock(size_t sz)
{
  if (sz==0) sz=_size-_pos;
  if (sz>_size-_pos) return false;
  _pos+=sz;
  return true;
}

ISendPackage *ConnectionTCP::OpenPackage()
{
  return new SendPackageTCP;
}

static bool BlockWrite(Socket &sock,const void *data, size_t sz)
{
  const char *stream=reinterpret_cast<const char *>(data);
  int written=sock.Write(stream,(int)sz);  
  while (written<(int)sz)
  {
    if (written<1) return false; //connection error;
    sz-=written;
    stream+=sz;
  }
  return true;
}

bool ConnectionTCP::ClosePackage(ISendPackage *package)
{
  SendPackageTCP *mypackage=dynamic_cast<SendPackageTCP *>(package);
  if (mypackage==NULL) return false;
  if (BlockWrite(_sock,&mypackage->_size,4)==false) return false;
  if (BlockWrite(_sock,&mypackage->_data,mypackage->_size)) return false;
  return true;
}

static bool BlockRead(Socket &sock,void *data, size_t sz)
{
  char *stream=reinterpret_cast<char *>(data);
  int read=sock.Read(stream,(int)sz);  
  while (read<(int)sz)
  {
    if (read<1) return false; //connection error;
    sz-=read;
    stream+=sz;
  }
  return true;
}


bool ReceivedPackageTCP::BuildPackage(Socket &sock)
{
  free(_data);
  if (BlockRead(sock,&_size,sizeof(_size))==false)
  {
    _size=0;
    return false;
  }
  _data=malloc(_size);
  _rdpos=0;
  if (BlockRead(sock,_data,_size)==false)
  {
    _size=0;
    free(_data);
    _data=0;
    return false;
  }
  return true;
}

size_t ReceivedPackageTCP::Read(void *buffer, size_t size, bool block)
{
  if (size>_size-_rdpos) size=_size-_rdpos;
  memcpy(buffer,(char *)_data+_rdpos,size);
  _rdpos+=size;
  return size;
}

 
IReceivedPackage *ConnReceiverTCP::OpenPackage(ICHANDLE dataHandle, bool detach)
{
  if ((SOCKET)dataHandle!=_sock) return NULL;
  if (_attached==NULL) _attached=new ReceivedPackageTCP;
  if (_attached->BuildPackage(_sock)==NULL) {_fail=true;return NULL;}
  _fail=false;
  IReceivedPackage *res=_attached;
  if (detach) _attached=0;
  return res;
}

bool ConnReceiverTCP::ClosePackage(IReceivedPackage *package)
{
  if (package==_attached) return true;
  ReceivedPackageTCP *rc=dynamic_cast<ReceivedPackageTCP *>(package);
  if (rc==NULL) return false;
  if (_attached==0) _attached=rc;
  else delete rc;
  return true;
}
