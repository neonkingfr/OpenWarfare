#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <limits.h>
#include <assert.h>
#include <el/BTree/Btree.h>
#include <iostream>
#include "AllocatorDef.h"
#include <new>



#define SYSTEM_PAGE_SIZE 4096
#define SYSTEM_PAGE_OFFSET_MASK 0xFFF
#define SYSTEM_ENDOFPAGE 0xFFE
#define SYSTEM_RESERVE_SIZE 65536
#define SYSTEM_RESERVE_PAGES (SYSTEM_RESERVE_SIZE/SYSTEM_PAGE_SIZE )
#define PADDING sizeof(int)
#define SYSTEM_SMALL_BLOCK_SIZE (SYSTEM_PAGE_SIZE/2-sizeof(Segment<>)) 

#define SEGPADDING_MAGIC 0xABCD

#define SEGMENTS_TO_REMOVE_COUNT 16

using namespace std;

#define BITMASKSIZE (3*1024/32*1024*1024/SYSTEM_PAGE_SIZE)

namespace BredyAllocator32
{
  using namespace BredyAllocator;

    class FastAllocInternal
    {
    public:
      static void *operator new(size_t sz);
      void operator delete(void *ptr);

    };

#pragma pack(1)
  struct BlockPageData
  {
    union 
    {
      unsigned __int8 _bytes[SYSTEM_PAGE_SIZE];
      struct  
      {
        unsigned __int32 _largeBlockSize;
        unsigned __int8 _lbdata[SYSTEM_PAGE_SIZE-sizeof(__int32)-sizeof(__int16)];
        unsigned __int16 _selector:14;
        unsigned __int16 _primary:1;
        unsigned __int16 _secondary:1;
      };
    };

    static BlockPageData *GetPageInfo(void *pagePtr)
    {
      unsigned __int32 lptr=(unsigned __int32)pagePtr;
      lptr&=~SYSTEM_PAGE_OFFSET_MASK;
      return (BlockPageData *)(lptr);
    }

    unsigned __int32 GetLargeBlockSize() const {return _largeBlockSize;}
    unsigned __int16 GetSelector() const {return _selector;}
    void *GetLargeBlockData() const {return (void *)_lbdata;}

    void SetLargeBlockSize(unsigned __int32 size) {_largeBlockSize=size;}
    void SetSelector(unsigned __int16 pos) {_selector=pos;}

    bool IsPrimaryPresent() const {return _primary!=0;}
    bool IsSecondaryPresent() const {return _secondary!=0;}
    void EnablePrimary(bool en) {_primary=en;}
    void EnableSecondar(bool en) {_secondary=en;}

    bool AllFree() const {return !IsPrimaryPresent() && !IsSecondaryPresent();}
    bool AnyFree() const {return !IsPrimaryPresent() || !IsSecondaryPresent();}
    int GetFree() const {return AllFree()?3:(IsPrimaryPresent()?1:(IsSecondaryPresent()?2:0));}

    void InitBlock() {_largeBlockSize=0;_selector=0;_primary=false;_secondary=false;}

    void *GetPrimaryPtr() {return this;}
    void *GetSecondaryPtr() {return _bytes+_selector;}    

    unsigned __int16 PtrToOffset(void *ptr) const
    {
      return reinterpret_cast<const unsigned char *>(ptr)-_bytes;
    }

    void *OffsetToPtr(unsigned __int16 offset) const
    {
      return (void *)(_bytes+offset);
    }

    static void SetSecondaryPtr(void *newptr) 
    {
      BlockPageData *bk=GetPageInfo(newptr);
      bk->SetSelector(bk->PtrToOffset(newptr));
    }

    size_t PrimarySpace() const
    {
      return _selector;
    }

    size_t SecondarySpace() const
    {
      return GetSmallBlockTotalSize()-_selector;
    }

    void *PrimaryOrSecondaryBase(void *ptr)
    {
      if (PtrToOffset(ptr)>=_selector)
      {
        return GetSecondaryPtr();
      }
      else
      {
        return GetPrimaryPtr();
      }
    }
    static size_t GetSmallBlockTotalSize()
    {
      return SYSTEM_PAGE_SIZE-sizeof(unsigned __int16);
    }
    bool Inside(void *ptr)
    {
      return reinterpret_cast<unsigned char *>(ptr)>=_bytes && reinterpret_cast<unsigned char *>(ptr)<_bytes+GetSmallBlockTotalSize();
    }

  };

#pragma pack()

  class EmptyBase
  {
  public:
    void InitializeSegment(char) {}
  };

  class SegmentBase
  {

  public:
    SegmentBase():_segType(0) {}

    void InitializeSegment(char segType)
    {
      _segType=segType;
      _next=0;
    }

    char GetType() const
    {
      return _segType;
    }
    SegmentBase *_next; //next segmen in not fully used list
  private:
    char _segType;    //specifies segmen type;
  };

  template<class Base=SegmentBase>
  class Segment: public Base
  {    
    unsigned char _totalBlocks; //total available blocks
    unsigned char _firstEmptyBlock; //first available block
    unsigned char _usedBlocks;    //total used blocks
    unsigned short _itemSize;   //size of the item in structure
    unsigned short _padding;    //padding to keep structure aligned.
  public:
    Segment():_totalBlocks(0),_firstEmptyBlock(0),_usedBlocks(0),_itemSize(0),_padding(SEGPADDING_MAGIC) {}


    bool CheckFreeChain() const
    {
      unsigned char bk=_firstEmptyBlock;
      unsigned char usd=_totalBlocks-_usedBlocks;
      while (bk!=0 && usd!=0)
      {
        if (bk>_totalBlocks) 
          return false;
        usd--;
        bk=GetSegmentData()[(bk-1)*_itemSize];
      }
      return bk==0 && usd==0;
    }

    bool CheckIntegrity() const
    {
      if (GetItemSize()<1 || GetItemSize()>SYSTEM_SMALL_BLOCK_SIZE ||
      GetSegmentSize()>=SYSTEM_PAGE_SIZE ||
      _padding!=SEGPADDING_MAGIC ||        
      _usedBlocks>_totalBlocks ||
      _usedBlocks!=_totalBlocks && _firstEmptyBlock==0 ||
      _usedBlocks==_totalBlocks && _firstEmptyBlock!=0 ||
      !CheckFreeChain()) return false;
      return true;
    }

    int GetSegmentSize() const
    {
      return sizeof(*this)+_totalBlocks*_itemSize;
    }

    unsigned char *GetSegmentData() const
    {
      return const_cast<unsigned char *>(reinterpret_cast<const unsigned char *>(this+1));
    }

    size_t GetItemSize() const
    {
      return _itemSize;
    }


    ///returns minimal useful size for requested segment
    static int GetEffectiveMinimalBlockSize(int itemSize)
    {
      return 2*itemSize+sizeof(Segment);
    }

    ///returns maximal useful size for requested segment
    static int GetEffectiveMaximalBlockSize(int itemSize)
    {
      return 255*itemSize+sizeof(Segment);
    }

    ///returns efficiency factor for requested segment and offered size
    static int GetAllocationEfficiencyFactor(int itemSize, int offerSize)
    {
      int items=(offerSize-sizeof(Segment))/itemSize;
      if (items<2 || items>255) return INT_MAX;
      return 255/items+(offerSize-(items*itemSize-sizeof(Segment)));
    }

    ///incializes the segment
    /**
    * @param availableSize - available size of rest of the page
    * @return remain memory of the rest of the page after initialization
    */
    int InitializeSegment(int availableSize, int itemSize)
    {
      int items=(availableSize-sizeof(*this))/itemSize;
      if (items<1) return 0;
      if (items>255) items=255;
      _itemSize=itemSize;

      Base::InitializeSegment(0);

      _totalBlocks=(unsigned char)items;      

      unsigned char *z=GetSegmentData();
      //inicialize segment blocks
      for (int i=0;i<_totalBlocks;i++)
      {
        z[i*itemSize]=i+2; //last block gets 0 as end of list
      }
      z[(_totalBlocks-1)*itemSize]=0;
      _firstEmptyBlock=1;
      return availableSize-(sizeof(*this)+items*_itemSize);
    }

    bool AnyEmpty() const
    {
      return _firstEmptyBlock!=0;
    }

    bool AllEmpty() const
    {
      return _usedBlocks==0;
    }

    void *Allocate()
    {
      int itemSize=_itemSize;
      unsigned char *z=GetSegmentData();
      int nextEmpty=_firstEmptyBlock;
      if (nextEmpty==0) return 0;
      nextEmpty--;
      _firstEmptyBlock=z[nextEmpty*itemSize];
      _usedBlocks++;
      return z+(nextEmpty*itemSize);
    }        

    bool Deallocate(void *ptr)
    {
      unsigned char *cptr=reinterpret_cast<unsigned char *>(ptr);     
      assert(cptr>=GetSegmentData() && cptr<GetSegmentData()+GetSegmentSize()-sizeof(*this));
      //invalid pointer value - address is outside of segment

      int itemsz=_itemSize;
      int item=(cptr-GetSegmentData())/itemsz;
      assert(GetSegmentData()+(item*itemsz)==cptr);
      //invalid pointer value - not points at begin of the object
#ifdef _DEBUG //check in debug only
      int k=_firstEmptyBlock;
      while (k!=0)
      {
        assert(k-1!=item); 
        //invalid pointer value - memory was already deallocated
        k=GetSegmentData()[(k-1)*itemsz];
      }
#endif
      *cptr=_firstEmptyBlock;
      _firstEmptyBlock=item+1;
      _usedBlocks--;
      return _usedBlocks==0;
    }

    bool IsMyPtr(void *ptr) const
    {
      unsigned char *cptr=reinterpret_cast<unsigned char *>(ptr);     
      return (cptr>=GetSegmentData() && cptr<GetSegmentData()+GetSegmentSize()-sizeof(*this));
    }

    unsigned char GetTotalBlockCount() const {return _totalBlocks;}
    unsigned char GetUsedBlockCount() const {return _usedBlocks;}
  };

  class SegmentOfSegments: public SegmentBase
  {
  public:
    typedef Segment<EmptyBase> ShSegment;    
  private:
    mutable unsigned short _lastUsed_offset;
    unsigned short _totalUsed;
    unsigned short _segSize;
  public:
    SegmentOfSegments():_lastUsed_offset(0),_totalUsed(0) {}

    bool CheckIntegrity() const
    {
      int tot=0;
      for (const ShSegment *seg=0;seg=GetNextSegment(seg);)
      {
        if (reinterpret_cast<const char *>(this)+_segSize<=reinterpret_cast<const char *>(seg))
          return false;
        if (seg->CheckIntegrity()==false) return false;
        tot+=seg->GetUsedBlockCount();
      }
      return true;
    }

    int InitializeSegment(int availableSize, int itemSize)
    {
      _segSize=availableSize;
      SegmentBase::InitializeSegment(1);
      ShSegment *start=reinterpret_cast<ShSegment *>(this+1);
      availableSize-=sizeof(*this);
      while (availableSize>(signed)sizeof(ShSegment))
      {
        new (start) ShSegment();
        int remain=start->InitializeSegment(availableSize,itemSize);
        int step=availableSize-remain;
        if (step==0) break;
        step=(step+PADDING-1) & ~(PADDING-1);
        start=reinterpret_cast<ShSegment *>(reinterpret_cast<char *>(start)+step);
        availableSize-=step;
      }
      char *ptr=reinterpret_cast<char *>(start);
      for (int i=0;i<availableSize;i++) ptr[i]=0;
      _totalUsed=0;
      return availableSize;
    }

    ///returns minimal useful size for requested segment
    static int GetEffectiveMinimalBlockSize(int itemSize)
    {
      return 2*itemSize+sizeof(ShSegment)+sizeof(SegmentOfSegments);
    }

    ///returns maximal useful size for requested segment
    static int GetEffectiveMaximalBlockSize(int itemSize)
    {
      return SYSTEM_PAGE_SIZE;
    }

    ///returns efficiency factor for requested segment and offered size
    static int GetAllocationEfficiencyFactor(int itemSize, int offerSize)
    {
      int x=SYSTEM_PAGE_SIZE/itemSize-offerSize/itemSize;
      return x;
    }

    size_t GetItemSize() const
    {
      return GetNextSegment(0)->GetItemSize();
    }

    ShSegment *GetNextSegment(ShSegment *cur) 
    {
      //cur==0 we want first segment
      if (cur==0) return reinterpret_cast<ShSegment *>(this+1);
      //get next segment from segment size
      unsigned short segSz=cur->GetSegmentSize();
      //apply padding
      segSz=(segSz+PADDING-1)&~(PADDING-1);
      //calculate new pos
      char *newpos=reinterpret_cast<char *>(cur)+segSz;
      //check, whether the new post is outside of big segment
      if (newpos+sizeof(ShSegment)-reinterpret_cast<char *>(this)>_segSize) return 0;
      //convert pointer to segment pointer
      ShSegment *res=reinterpret_cast<ShSegment *>(newpos);
      //if this segment has zero size, it is terminator - return 0;
      if (res->GetSegmentSize()==0) return 0;
      //return result segment;
      return res;
    }
    const ShSegment *GetNextSegment(const ShSegment *cur) const
    {
      return const_cast<SegmentOfSegments *>(this)->GetNextSegment(const_cast<ShSegment *>(cur));
    }

    void SetLastUsedOffset(const ShSegment *seg) const
    {
      _lastUsed_offset=reinterpret_cast<const char *>(seg)-reinterpret_cast<const char *>(this);
    }

    ShSegment *GetLastUsedOffset()
    {
      if (_lastUsed_offset==0) return GetNextSegment(0);
      return reinterpret_cast<ShSegment *>(reinterpret_cast<char *>(this)+_lastUsed_offset);
    }

    const ShSegment *GetLastUsedOffset() const 
    {
      if (_lastUsed_offset==0) return GetNextSegment(0);
      return reinterpret_cast<const ShSegment *>(reinterpret_cast<const char *>(this)+_lastUsed_offset);
    }

    bool AnyEmpty() const
    {      if (GetLastUsedOffset()->AnyEmpty()) return true;
      for (const ShSegment *seg=0;seg=GetNextSegment(seg);)
        if (seg->AnyEmpty()) 
        {
          SetLastUsedOffset(seg);
          return true;
        }
        return false;
    }

    bool AllEmpty() const
    {
      return _totalUsed==0;
    }

    void *Allocate()
    {
      ShSegment *cur=GetLastUsedOffset();
      if (!cur->AnyEmpty())
      {
        cur=0;
        for (ShSegment *seg=0;seg=GetNextSegment(seg);)
          if (seg->AnyEmpty()) 
          {
            cur=seg;
            SetLastUsedOffset(cur);
          }
          if (cur==0) return 0;
      }
      _totalUsed++;
      return cur->Allocate();
    }      

    bool Deallocate(void *ptr)
    {
      ShSegment *cur=GetLastUsedOffset();
      if (!cur->IsMyPtr(ptr))
      {
        cur=0;
        for (ShSegment *seg=0;seg=GetNextSegment(seg);)
          if (seg->IsMyPtr(ptr)) 
          {
            cur=seg;
            SetLastUsedOffset(cur);
          }
          assert(cur!=0); //pointer cannot be deallocated, invalid pointer
          if (cur==0) return _totalUsed==0;
      }
      _totalUsed--;
      return cur->Deallocate(ptr);
    }

    bool IsMyPtr(void *ptr) const
    {
      const char *cptr=reinterpret_cast<char *>(ptr);     
      const char *t=reinterpret_cast<const char *>(this);
      return (cptr>=t && cptr<t+_segSize);
    }


  };




  class FreeList
  {
    friend class MemoryManagerSingleton;
    void *_freeList[SYSTEM_PAGE_SIZE];
  public:
    FreeList() {for (int i=0;i<SYSTEM_PAGE_SIZE;i++) _freeList[i]=0;}

    void FindAndRemoveFreeBlock(void *block, unsigned long sz)
    {
      if (sz==0) return;
      assert(sz>0 && sz<SYSTEM_PAGE_SIZE);
      void *x=_freeList[sz],**l=0;
      while (x && x!=block)
      {
        l=(void **)x;
        x=*l;
      }
      if (x==block)
      {
        if (l==0) _freeList[sz]=*(void **)x;
        else *l=*(void **)x;;          
      }
    }

    bool IsAvalableBlock(size_t sz) const
    {
      assert(sz>0 && sz<SYSTEM_PAGE_SIZE);
      return _freeList[sz]!=0;
    }

    void *RetrieveBlock(size_t sz)
    {
      assert(sz>0 && sz<SYSTEM_PAGE_SIZE);
      void *p=_freeList[sz];
      if (p!=0) _freeList[sz]=*(void **)p;

      BlockPageData *page=BlockPageData::GetPageInfo(p);      
      if (p==page->GetPrimaryPtr())
      {
        assert(!page->IsPrimaryPresent()); //Primary is used but also in free list;
        page->EnablePrimary(true);
      }
      else if (p==page->GetSecondaryPtr())
      {
        assert(!page->IsSecondaryPresent()); //Secondary is used but also in free lits;
        page->EnableSecondar(true);
      }
      else
      {
        assert(false); //selector of the page was corrupted.
        return 0;
      }

      return p;
    }

    //returns page, that should be decommited, or NULL, if no decomition is necesery
    BlockPageData *DropBlock(void *ptr)
    {
      BlockPageData *page=BlockPageData::GetPageInfo(ptr); 
      size_t sz;
      if (ptr==page->GetPrimaryPtr())
      {
        page->EnablePrimary(false);
        if (page->AllFree())
        {
          FindAndRemoveFreeBlock(page->GetSecondaryPtr(),page->SecondarySpace());
          return page;
        }
        sz=page->PrimarySpace();
      }
      else if (ptr==page->GetSecondaryPtr())
      {
        page->EnableSecondar(false);
        if (page->AllFree())
        {
          FindAndRemoveFreeBlock(page->GetPrimaryPtr(),page->PrimarySpace());
          return page;
        }
        sz=page->SecondarySpace();
      }
      else 
      {
        assert(false); //invalid pointer specified
        return 0;
      }
      void **x=(void **)ptr;
      *x=_freeList[sz];
      _freeList[sz]=x;
      return 0;
    }

    template<class T>
    size_t GetOptimalSizeForSegment(int itemSize)
    {
      size_t best=0;
      int bestEff=INT_MAX;
      for (int i=T::GetEffectiveMinimalBlockSize(itemSize),cc=__min(T::GetEffectiveMaximalBlockSize(itemSize),SYSTEM_PAGE_SIZE-1);i<=cc;i++)
        if (_freeList[i]!=0)
        {
          int b=T::GetAllocationEfficiencyFactor(itemSize,i);
          if (b<bestEff)
          {
            bestEff=b;
            best=i;
          }
        }
      return best;
    }

    bool IsOurPage(void *p)
    {
      for (int i=0;i<SYSTEM_PAGE_SIZE;i++)
      {
        void *x=_freeList[i];
        while (x)
        {        
          if (BlockPageData::GetPageInfo(x)==BlockPageData::GetPageInfo(p)) return true;
          x=*(void **)x;
        }
      }
      return false;
    }
  };  

  class BigAllocBlock: public FastAlloc
  {
  public:
    int pageIndex;
    int numPages;
    BigAllocBlock *left;
    BigAllocBlock *right;
    BigAllocBlock(int pageIndex=0,int numPages=0):left(0),right(0),pageIndex(pageIndex),numPages(numPages) {}
    BigAllocBlock(BigAllocBlock *at,bool before, const BigAllocBlock &src):
      left(before?at->left:at),right(before?at:at->right),pageIndex(src.pageIndex),numPages(src.numPages)
      {
          if (left) left->right=this;        
          if (right) right->left=this;
      }
    ~BigAllocBlock() 
    {
      if (left) left->right=right;
      if (right) right->left=left;
    }

    int Compare(BigAllocBlock &other) const
    {
      if (numPages==other.numPages)
        return (pageIndex>other.pageIndex)-(pageIndex<other.pageIndex);
      else
        return (numPages>other.numPages)-(numPages<other.numPages);
    }


  };


  class BigAllocator
  {
    BigAllocBlock _root;
  public:
    BigAllocBlock *FindFreeBlock(int numpages)
    {
      BigAllocBlock *a=_root.right;
      BigAllocBlock *biggest=a;
      while (a)
      {
        if (a->numPages==numpages) return a;
        if (a->numPages>biggest->numPages) biggest=a;
        a=a->right;
      }
      return (biggest && biggest->numPages>=numpages)?biggest:0;
    }

    void AddFreeBlocks(const BigAllocBlock &blk)
    {      
      BigAllocBlock *a=_root.right;
      while (a && a->pageIndex<blk.pageIndex)
      {
        if (a->pageIndex+a->numPages==blk.pageIndex)
        {
          a->numPages+=blk.numPages;
          if (a->right && a->right->pageIndex==a->pageIndex+a->numPages)
          {
            a->numPages+=a->right->numPages;
            delete a->right;
          }
          return;
        }
        a=a->right;
      }
      bool pos;
      if (a)
      {
        if (a->pageIndex==blk.numPages+blk.pageIndex)
        {
          a->numPages+=blk.numPages;
          a->pageIndex=blk.pageIndex;
          return;
        }        
        pos=true;
      }
      else
      {
        a=&_root;
        while (a && a->right!=0) a=a->right;
        pos=false;
      }
      char _tempBuffer[sizeof(BigAllocBlock)];
      BigAllocBlock *x=::new(_tempBuffer) BigAllocBlock(a,pos,blk);
      BigAllocBlock *n=new BigAllocBlock(x,true,blk);
      n->numPages=x->numPages;
      n->pageIndex=x->pageIndex;
      x->~BigAllocBlock();
    }

    void RemoveFreeBlock(BigAllocBlock *whichBlock, int pages)
    {
      if (whichBlock->numPages==pages)
      {
        delete whichBlock;
      }
      else
      {
        whichBlock->pageIndex+=pages;
        whichBlock->numPages-=pages;
      }
    }

    void *AllocPages(int pages)
    {
      BigAllocBlock *bk=FindFreeBlock(pages);
      if (bk==0)
      {        
        size_t ppages=(pages+15)&~15;
        void *adr=VirtualAlloc(0,ppages*SYSTEM_PAGE_SIZE,MEM_RESERVE,PAGE_READWRITE);
        if (adr==0) return 0; //no more memory available;
        unsigned __int32 pageNum=(unsigned __int32)adr;
        AddFreeBlocks(BigAllocBlock(pageNum/SYSTEM_PAGE_SIZE,ppages));       
        return AllocPages(pages);
      }
      void *res=(void *)(bk->pageIndex*SYSTEM_PAGE_SIZE);
      RemoveFreeBlock(bk,pages);
      char *p=(char *)res;
      while (pages)
      {      
        MEMORY_BASIC_INFORMATION nfo;
        VirtualQuery(p,&nfo,sizeof(nfo));
        char *b=(char *)nfo.AllocationBase;
        int pagesAtTime=nfo.RegionSize/SYSTEM_PAGE_SIZE;
        pagesAtTime=__min(pagesAtTime,pages);
        if (VirtualAlloc(p,pagesAtTime*SYSTEM_PAGE_SIZE,MEM_COMMIT,PAGE_READWRITE)==0)
        {
          assert(0);
          abort();
        }
        pages-=pagesAtTime;
        p+=pagesAtTime*SYSTEM_PAGE_SIZE;
      }
        
      return res;
    }

    void FreePages(void *pageAdr, int pageCount)
    {
      char *p=(char *)pageAdr;
      int pages=pageCount;
      while (pages)
      {      
        MEMORY_BASIC_INFORMATION nfo;
        VirtualQuery(p,&nfo,sizeof(nfo));
        char *b=(char *)nfo.AllocationBase;
        int pagesAtTime=(nfo.RegionSize)/SYSTEM_PAGE_SIZE;
        pagesAtTime=__min(pagesAtTime,pages);
        if (VirtualFree(p,pagesAtTime*SYSTEM_PAGE_SIZE,MEM_DECOMMIT)==0)
        {
          assert(0);
          abort();
        }
        pages-=pagesAtTime;
        p+=pagesAtTime*SYSTEM_PAGE_SIZE;
      }
      unsigned __int32 pageNum=(unsigned __int32)pageAdr/SYSTEM_PAGE_SIZE;
      AddFreeBlocks(BigAllocBlock(pageNum,pageCount));      
    }
  };


  class AllocatorRootTable
  {

    SegmentBase *_availableSegments[SYSTEM_SMALL_BLOCK_SIZE];
/*#ifdef _DEBUG
    SegmentBase *_fullBlockList;
#endif*/
    SegmentBase *_segmentsToRemove[SEGMENTS_TO_REMOVE_COUNT];
    int _segmentsToRemSlots[SEGMENTS_TO_REMOVE_COUNT];
    int _segmentsToRemoveCnt;
    

  public:
    AllocatorRootTable() {for (int i=0;i<SYSTEM_PAGE_SIZE;i++) _availableSegments[i]=0;
/*#ifdef _DEBUG    
    _fullBlockList=0;
#endif*/
    _segmentsToRemoveCnt=0;
    memset(_segmentsToRemove,0,sizeof(_segmentsToRemove));
    }

    template<class T>
    void *AllocateInSegment(SegmentBase *seg, bool &full, bool &wasempty)
    {
      void *adr;
      T *sseg=static_cast<T *>(seg);
      wasempty=sseg->AllEmpty();
      adr=sseg->Allocate();
      full=!sseg->AnyEmpty();
      return adr;
    }

    void *Allocate(size_t sz)
    {
      assert(sz<=SYSTEM_SMALL_BLOCK_SIZE);
      if (!sz) sz=1;
      SegmentBase **root=_availableSegments+sz-1;
      SegmentBase *cur=*root;
      if (cur==0) return 0;
      void *adr;
      SegmentBase *seg=cur;
      bool full;
      bool wasempty;
      if (seg->GetType()==0) adr=AllocateInSegment<Segment<> >(seg,full,wasempty);
      else adr=AllocateInSegment<SegmentOfSegments>(seg,full,wasempty);
      assert(adr!=0);
      if (full)
      {
        *root=cur->_next;
        cur->_next=0;
/*#ifdef _DEBUG    
        cur->_next=_fullBlockList;
        _fullBlockList=cur;
#endif*/
      }
      if (wasempty)
      {
        for (int i=0;i<SEGMENTS_TO_REMOVE_COUNT;i++)
          if (_segmentsToRemove[i]==seg) _segmentsToRemove[i]=0;
      }
      return adr;
    }

    template<class T>
    size_t DeallocateInSegment(SegmentBase *base,void *ptr,bool &empty, bool &wasfull)
    {
      T *sseg=static_cast<T *>(base);
      wasfull=!sseg->AnyEmpty();
      sseg->Deallocate(ptr);
      empty=sseg->AllEmpty();
      return sseg->GetItemSize();
      
    }


    SegmentBase *Deallocate(void *ptr)
    {
      BlockPageData *page=BlockPageData::GetPageInfo(ptr);
      SegmentBase *seg=reinterpret_cast<SegmentBase *>(page->PrimaryOrSecondaryBase(ptr));
      bool empty;
      bool wasfull;
      size_t itemSz;

      if (seg->GetType()==0)
        itemSz=DeallocateInSegment<Segment<> >(seg,ptr,empty,wasfull);
      else
        itemSz=DeallocateInSegment<SegmentOfSegments>(seg,ptr,empty,wasfull);
      SegmentBase **root=_availableSegments+itemSz-1;
      if (wasfull)
      {
/*#ifdef _DEBUG
        SegmentBase **x=&_fullBlockList;
        while (*x && *x!=seg) x=&(*x)->_next;
        if (*x==seg) *x=seg->_next;          
#endif      */
        seg->_next=*root;
        *root=seg;
      }
      else if (empty) //block is empty
      {               
        SegmentBase *k=_segmentsToRemove[_segmentsToRemoveCnt];
        int slot=_segmentsToRemSlots[_segmentsToRemoveCnt];
        _segmentsToRemove[_segmentsToRemoveCnt]=seg;
        _segmentsToRemSlots[_segmentsToRemoveCnt]=itemSz-1;
        _segmentsToRemoveCnt=(_segmentsToRemoveCnt+1)%SEGMENTS_TO_REMOVE_COUNT;
        if (k)
        {
          SegmentBase **z=_availableSegments+slot;
          while (*z && *z!=k) z=&((*z)->_next);
          if (*z) *z=k->_next;
        }
        return k;
      }
      else
      {
        SegmentBase *ls=*root;
        if (ls!=*root)
        {
          while (ls && ls->_next!=seg) ls=ls->_next;
          if (ls)
            ls->_next=seg->_next;
          seg->_next=*root;
          *root=seg;
        }
       }
      return 0;
    }

    void RegisterNewEmptyBlock(SegmentBase *seg, int itemSz)
    {
      SegmentBase **root=_availableSegments+itemSz-1;
      seg->_next=*root;
      *root=seg;
    }

    bool IsOurPage(void *page)
    {
      BlockPageData *bk=BlockPageData::GetPageInfo(page);
      SegmentBase *x;
/*#ifdef _DEBUG
      x=_fullBlockList;
      while (x)
      {
        if (bk->Inside(x)) return true;
        x=x->_next;
      }
#endif*/
      for (int i=0;i<SYSTEM_SMALL_BLOCK_SIZE;i++)
      {
        x=_availableSegments[i];
        while (x)
        {          
          if (bk->Inside(x)) return true;
          x=x->_next;
        }
      }
      if (bk->GetSelector()>=8 && bk->GetSelector()<=bk->GetSmallBlockTotalSize())
      {
        if (bk->IsSecondaryPresent())
        {
          SegmentBase *sg=reinterpret_cast<SegmentBase *>(bk->GetSecondaryPtr());
          if (sg->GetType()==0 && static_cast<Segment<> *>(sg)->CheckIntegrity())            
            return true;
          if (sg->GetType()==1 && static_cast<SegmentOfSegments *>(sg)->CheckIntegrity())            
            return true;
        }
      }
     
      return false;
    }
  };




#define SPACEENOUGH(x) ((x)>=(sizeof(Segment<>)+sizeof(unsigned short)+4))

  class MemManager
  {
    friend class MemoryManagerSingleton;
    AllocatorRootTable _root;
    FreeList _freeList;
    BigAllocator _bigAllocator;

  public:

    void *Allocate(size_t sz)
    {
      if (sz==0) sz=1;
      if (sz>SYSTEM_SMALL_BLOCK_SIZE) //allocation of large block
        return AllocateLarge(sz);
      else
        return AllocateSmall(sz);
    }

    void Deallocate(void *ptr)
    {
      if (ptr==0) return;
      BlockPageData *page=BlockPageData::GetPageInfo(ptr);
      if (page->GetLargeBlockData()==ptr)
        DeallocateLarge(ptr);
      else
        DeallocateSmall(ptr);
    }

  protected:
    void *AllocateLarge(size_t sz)
    {
      sz+=sizeof(unsigned __int32);
      int reqPages=(sz+SYSTEM_PAGE_SIZE-1)/SYSTEM_PAGE_SIZE;
      void *res=_bigAllocator.AllocPages(reqPages);
      if (res==0) return 0;
      BlockPageData *page=BlockPageData::GetPageInfo(res);
      page->SetLargeBlockSize(sz);
      res=page->GetLargeBlockData();
      
      page+=reqPages-1;
      sz-=(reqPages-1)*SYSTEM_PAGE_SIZE;

      sz=(sz+PADDING-1)&~(PADDING-1);
      size_t remain=(page->GetSmallBlockTotalSize()-sz);
      page->EnablePrimary(true);
      if (SPACEENOUGH(remain))
      {
        page->SetSelector(sz);
        _freeList.DropBlock(page->OffsetToPtr(sz));
      }
      return res;
    }

    void DeallocateLarge(void *ptr)
    {
      BlockPageData *page=BlockPageData::GetPageInfo(ptr);
      size_t sz=page->GetLargeBlockSize();
      int freePages=(sz-1)/SYSTEM_PAGE_SIZE;
      _bigAllocator.FreePages(page,freePages);

      page+=freePages;
      sz-=freePages*SYSTEM_PAGE_SIZE;

      size_t remain=SYSTEM_PAGE_SIZE-sz;
      if (SPACEENOUGH(remain))
      {
        BlockPageData *b=_freeList.DropBlock(page);
        if (b) _bigAllocator.FreePages(b,1);
      }
      else
      {
        _bigAllocator.FreePages(page,1);
      }
    }

    void *AllocateSmall(size_t sz)
    {
      void *ptr=_root.Allocate(sz);
      if (ptr==0)
      {
        size_t x=_freeList.GetOptimalSizeForSegment<Segment<> >(sz);
        if (x==0)
        {
          x=_freeList.GetOptimalSizeForSegment<SegmentOfSegments>(sz);
          if (x==0)
          {
            void *pag=_bigAllocator.AllocPages(1);
            if (pag==0) return 0;
            Segment<> *seg=new(pag) Segment<>;
            BlockPageData *bk=BlockPageData::GetPageInfo(seg);
            int remain=seg->InitializeSegment(bk->GetSmallBlockTotalSize(),sz);
            int alloc=bk->GetSmallBlockTotalSize()-remain;
            alloc=(alloc+PADDING-1)/PADDING*PADDING;
            bk->EnablePrimary(true);
            if (SPACEENOUGH(remain))
            {
              bk->SetSelector(alloc);
              _freeList.DropBlock(bk->OffsetToPtr(alloc));
            }
            else
            {
              bk->SetSelector(bk->GetSmallBlockTotalSize());
            }
            ptr=seg->Allocate();
            _root.RegisterNewEmptyBlock(seg,sz);
          }
          else
          {                        
            SegmentOfSegments *seg=new(_freeList.RetrieveBlock(x)) SegmentOfSegments;
            seg->InitializeSegment(x,sz);
            ptr=seg->Allocate();
            _root.RegisterNewEmptyBlock(seg,sz);           
          }
        }
        else
        {
          Segment<> *seg=new(_freeList.RetrieveBlock(x)) Segment<>;
          seg->InitializeSegment(x,sz);
          ptr=seg->Allocate();
          _root.RegisterNewEmptyBlock(seg,sz);           
        }
      }      
      return ptr;
    }

    void DeallocateSmall(void *ptr)
    {
      SegmentBase *seg=_root.Deallocate(ptr);
      if (seg) 
      {
        BlockPageData *block=_freeList.DropBlock(seg);
        if (block) _bigAllocator.FreePages(block,1);
      }
    }

    void NotASegment(SegmentBase *segment, ostream &out)
    {
      BlockPageData *bk=BlockPageData::GetPageInfo(segment);
      if (segment==bk->GetPrimaryPtr())
      {
        size_t sz=bk->PrimarySpace();
        while (bk->GetLargeBlockSize()!=sz)
        {
          bk--;
          if (IsBadReadPtr(bk,4)) goto error;
          sz+=SYSTEM_PAGE_SIZE;
        }
        out<<"\t\t\tEnd of LARGE BLOCK\n\t\t\tallocated at page: "<<(int)bk/SYSTEM_PAGE_SIZE<<" ("<<hex<<(int)bk<<dec<<
          ") - size: "<<sz-4<<" bytes\n";
        return;
      }
error:;
      out<<"\t\t\t!PROBABLY CORRUPTED SEGMENT or client random data\n";
    }

    void ShowSegmentStats(SegmentBase *segment,ostream &out)
    {
      if ((SegmentBase *)((long)segment & ~3)!=segment) //not aligned ptr
        return NotASegment(segment,out);
      
      if ((SegmentBase *)((long)(segment->_next) & ~3)!=segment->_next) //next ptr should point to aligned addr
        return NotASegment(segment,out);

      if (segment->_next!=0 && IsBadReadPtr(segment->_next,4)) 
        return NotASegment(segment,out);

      switch (segment->GetType())
      {
      case 0:
        {
          Segment<> *seg=static_cast<Segment<> *>(segment);
          if (!seg->CheckIntegrity()) return NotASegment(segment,out);
          out<<"\t\tSEGMENT "<<(int)seg->GetItemSize()<<" bytes per item\n"
            <<"\t\tUsed     "
            <<(int)seg->GetUsedBlockCount()
            <<"/"
            <<(int)seg->GetTotalBlockCount()
            <<" blocks ("
            <<(int)seg->GetUsedBlockCount()*seg->GetItemSize()
            <<"/"
            <<(int)seg->GetTotalBlockCount()*seg->GetItemSize()
            <<" bytes) "
            <<(int)seg->GetUsedBlockCount()*100/seg->GetTotalBlockCount()
            <<"%\n";
        }
        break;
      case 1:
        {
          SegmentOfSegments *seg=static_cast<SegmentOfSegments *>(segment);
          if (!seg->CheckIntegrity()) return NotASegment(segment,out);
          out<<"\t\tGROUP\n";
          int t=0;
          int itsz=0;
          int u=0;
          for (SegmentOfSegments::ShSegment *s=0;s=seg->GetNextSegment(s);)
          {
            u+=s->GetUsedBlockCount();
            t+=s->GetTotalBlockCount();
            itsz=s->GetItemSize();
            out<<"\t\t\tSEGMENT "<<(int)s->GetItemSize()<<" bytes per item\n"
              <<"\t\t\tUsed     "
              <<(int)s->GetUsedBlockCount()
              <<"/"
              <<(int)s->GetTotalBlockCount()
              <<" blocks ("
              <<(int)s->GetUsedBlockCount()*s->GetItemSize()
              <<"/"
              <<(int)s->GetTotalBlockCount()*s->GetItemSize()
              <<" bytes) "
              <<(int)s->GetUsedBlockCount()*100/s->GetTotalBlockCount(
              )<<"%\n";
          }
          out<<"\t\tAll segments "
            <<u
            <<"/"
            <<t
            <<" blocks ("
            <<u*itsz
            <<"/"
            <<t*itsz
            <<" bytes) "
            <<u*100/t
            <<"%\n";
          break;

        }
      default: NotASegment(segment,out);
      }
    }

    void ShowMemoryMap(ostream &out)
    {
      for (unsigned int i=0;i<(3*1024*1024)/(SYSTEM_PAGE_SIZE/1024);i++)
      {
        void *page=(void *)(i*SYSTEM_PAGE_SIZE);
        MEMORY_BASIC_INFORMATION info;
        VirtualQuery(page,&info,sizeof(info));
        if (info.State==MEM_COMMIT)
        {          
          if (_root.IsOurPage(page) || _freeList.IsOurPage(page))
          {
            out<<"Page: "<<i<<" ("<<hex<<(int)page<<dec<<")\n";
            BlockPageData *bk=BlockPageData::GetPageInfo(page);
            SegmentBase *seg;
            out<<"\tPrimary segment size:"<<bk->PrimarySpace()<<" bytes\n";
            if (bk->IsPrimaryPresent())
            {
              seg=reinterpret_cast<SegmentBase *>(bk->GetPrimaryPtr());
              ShowSegmentStats(seg,out);
            }
            else
              out<<"\t\t<...free...>\n";
            out<<"\tSecondary segment size:"<<bk->SecondarySpace()<<" bytes\n";
            if (bk->IsSecondaryPresent())
            {
              seg=reinterpret_cast<SegmentBase *>(bk->GetSecondaryPtr());
              ShowSegmentStats(seg,out);
            }
            else
              out<<"\t\t<...free...>\n";
          }
        }

      }
    }
  };

  class MemoryManagerSingleton
  {
    MemManager *_memMng;


  public:
    MemoryManagerSingleton():_memMng(0){}
    void *Allocate(size_t sz)
    {
      void *res;
      if (_memMng==0) 
      {
        int psz=(sizeof(MemManager)+SYSTEM_PAGE_SIZE-1)/SYSTEM_PAGE_SIZE;
        void *ptr=VirtualAlloc(0,psz*SYSTEM_PAGE_SIZE,MEM_COMMIT,PAGE_READWRITE);
        _memMng=new(ptr) MemManager;        
        res=_memMng->Allocate(sz);
        
      }
      else
      {
        res=_memMng->Allocate(sz);
      }
      return res;
    }

    void Deallocate(void *ptr) 
    {
      assert(_memMng!=0);
      _memMng->Deallocate(ptr);
    }

    bool IsInited() const {return _memMng!=0;}


    void ShowMemoryMap(ostream &str)
    {
      _memMng->ShowMemoryMap(str);
    }
  };

};

#pragma init_seg(compiler)
static BredyAllocator32::MemoryManagerSingleton memManager;

#ifdef _MT
#include <el/MultiThread/CriticalSection.h>

static MultiThread::CriticalSection zamek;
struct Zamek
{
  Zamek() {zamek.Lock();}
  ~Zamek() {zamek.Unlock();}  
};
#else
struct Zamek
{
  Zamek() {}
  ~Zamek() {}  
};
#endif

namespace BredyAllocator
{
  void *Allocate(size_t sz)
  { 
    Zamek zamek;
    return memManager.Allocate(sz);
  }
  void Deallocate(void *ptr)
  {    
    Zamek zamek;
    return memManager.Deallocate(ptr);
  }
  void ShowMemoryMap(ostream &str)
  {
    Zamek zamek;   
    memManager.ShowMemoryMap(str);
  }
}

