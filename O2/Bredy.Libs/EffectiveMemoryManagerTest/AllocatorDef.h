#pragma once
namespace BredyAllocator
{
  void *Allocate(size_t sz);
  void Deallocate(void *ptr);

  void ShowMemoryMap(std::ostream &str);

  class FastAlloc
  {
  public:
    static void *operator new(size_t sz)
    {
      return Allocate(sz);
    }
    static void operator delete(void *ptr)
    {
      return Deallocate(ptr);
    }
  };
}


