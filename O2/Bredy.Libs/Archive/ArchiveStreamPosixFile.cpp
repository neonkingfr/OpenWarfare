// ArchiveStreamPosixFile.cpp: implementation of the ArchiveStreamPosixFile class.
//
//////////////////////////////////////////////////////////////////////

#include "ArchiveStreamPosixFile.h"
#include <io.h>
#include <stdio.h>
#include <string.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ArchiveStreamPosixFile::ArchiveStreamPosixFile(int handle,bool autoclose):_handle(handle),_autoclose(autoclose)
  {
  Reset();
  }

ArchiveStreamPosixFile::~ArchiveStreamPosixFile()
  {
  if (_autoclose) _close(_handle);
  }

int ArchiveStreamPosixFile::IsError()
  {
  return _err;
  }

void ArchiveStreamPosixFile::Reset()
  {
  _err=0;
  }
__int64 ArchiveStreamPosixFile::Tell()
  {
  if (_err) return -1;
  return _telli64(_handle);
  }

void ArchiveStreamPosixFile::Seek(__int64 lOff, SeekOp seekop)
  {  
  if (_err) return;
  switch (seekop)
    {
    case begin:lOff=_lseeki64(_handle,lOff,SEEK_SET );break;
    case current:lOff=_lseeki64(_handle,lOff,SEEK_CUR);break;
    case end:lOff=_lseeki64(_handle,lOff,SEEK_END);break;
    }
  if (lOff<0) _err=-1;
  }

int ArchiveStreamPosixFileIn::DataExchange(void *buffer, int maxsize)
  {
  if (_err) return _err;
  int cntrd=_read(_handle,buffer,maxsize);
  if (cntrd==-1) _err=-1;
  else if (cntrd!=maxsize) ReportUncomplette(cntrd);
  return _err;
  }


int ArchiveStreamPosixFileOut::DataExchange(void *buffer, int maxsize)
  {
  if (_err) return _err;
  if (_write(_handle,buffer,maxsize)==-1) _err=-1;
  return _err;
  }
