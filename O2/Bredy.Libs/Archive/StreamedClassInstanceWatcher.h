// StreamedClassInstanceWatcher.h: interface for the StreamedClassInstanceWatcher class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_STREAMEDCLASSINSTANCEWATCHER_H__1ABB7449_FDE0_4695_9055_CAB77721B352__INCLUDED_)
#define AFX_STREAMEDCLASSINSTANCEWATCHER_H__1ABB7449_FDE0_4695_9055_CAB77721B352__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
class SerializedClass;

#define SCIW_ERROR (SerializedClass *)0xFFFFFFFF


class tBTreeDB_SC;
class IArchive;
class StreamedClassInstanceWatcher  
{
    tBTreeDB_SC *db;
	unsigned long counter;
public:
	void ResetDBSection(unsigned long section);
	bool SaveInstanceID(IArchive &arch,SerializedClass *cls);
	long LoadInstanceID(IArchive &arch,SerializedClass **cls);
	void RegisterNewInstance(long keyid, SerializedClass *cls);
	SerializedClass *FindInstance(long keyid);
	StreamedClassInstanceWatcher();	
	~StreamedClassInstanceWatcher();	
};

#endif // !defined(AFX_STREAMEDCLASSINSTANCEWATCHER_H__1ABB7449_FDE0_4695_9055_CAB77721B352__INCLUDED_)
