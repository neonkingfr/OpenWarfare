// StreamedClassInstanceWatcher.cpp: implementation of the StreamedClassInstanceWatcher class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "StreamedClassInstanceWatcher.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


#include "BTreeDB.h"
#include "IArchive.h"
#include "assert.h"

#define ASSERT assert

struct StreamedClassInstanceInfo
  {
  unsigned long keyid;
  unsigned long dataid;
  unsigned long section;
  int GetKeyPart (int bit, int sz)  const
	{
	return (keyid>>bit) & ((1<<sz)-1);
	}
  int GetBitLen() const {return 32;} 
  bool KeySame(const StreamedClassInstanceInfo &other)  const 
	{return keyid==other.keyid;}
  };

TypeIsSimple(StreamedClassInstanceInfo)


class tBTreeDB_SC: public BTreeDB<StreamedClassInstanceInfo>
  {
  };


StreamedClassInstanceWatcher::StreamedClassInstanceWatcher()
  {
  db=NULL;
  counter=1;
  }

StreamedClassInstanceWatcher::~StreamedClassInstanceWatcher()
  {
  delete db;
  }

void StreamedClassInstanceWatcher::ResetDBSection(unsigned long section)
  {
  if (db==NULL) return;
  tBTreeDB_SC *newdb=new tBTreeDB_SC;
  int cnt=db->GetNKeys();
  for (int i=0;i<cnt;i++)
	{
	StreamedClassInstanceInfo &nfo=db->GetKey(i);
	if (nfo.section<=section) newdb->AddKey(nfo);
	}
  delete db;
  db=newdb;
  }

bool StreamedClassInstanceWatcher::SaveInstanceID(IArchive &arch,SerializedClass *cls)
  {
  if (db==NULL) db=new tBTreeDB_SC;
  StreamedClassInstanceInfo key,*found;
  key.keyid=(unsigned long)cls;
  found=db->FindKey(key);
  if (found)
	{
	key.keyid=-key.keyid;
	arch("_Instance:",key.keyid);
	return false;
	}
  else
	{
	key.dataid=counter++;
	ASSERT(counter<0x7FFFFFFF);
	key.section=0; //not supported yet
	db->AddKey(key);
	arch("_Instance:",key.keyid);
	return true;
	}
  }

long StreamedClassInstanceWatcher::LoadInstanceID(IArchive &arch,SerializedClass **cls)
	{
    if (db==NULL) db=new tBTreeDB_SC;
	StreamedClassInstanceInfo key,*found;
    arch("_Instance:",key.keyid);
	bool folowinstance=key.keyid>0;
	key.keyid=abs(key.keyid);
	found=db->FindKey(key);
	if (found && folowinstance) return -1;
	if (!found && !folowinstance) return -1;
	if (!found) 
	  {
	  *cls=NULL;
	  return (long)key.keyid;
	  }
	else
	  {
	  *cls=(SerializedClass *)found->dataid;
	  return (long)key.keyid;
	  }
	}

void StreamedClassInstanceWatcher::RegisterNewInstance(long keyid, SerializedClass *cls)
  {
  if (db==NULL) db=new tBTreeDB_SC;
  StreamedClassInstanceInfo key;
  key.keyid=keyid;
  key.dataid=(DWORD)cls;
  db->AddKey(key);
  }

SerializedClass *StreamedClassInstanceWatcher::FindInstance(long keyid)
  {
  StreamedClassInstanceInfo key,*found;
  key.keyid=keyid;
  found=db->FindKey(key);
  if (found==NULL) return NULL;
  return (SerializedClass *)found->dataid;
  }