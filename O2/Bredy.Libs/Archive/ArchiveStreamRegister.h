#pragma once
#include "archivestreammemory.h"

class ASR_RegisterInfo
{
  HKEY _key;
  char *_valueName;
public:
  ASR_RegisterInfo();
  ~ASR_RegisterInfo();

  bool OpenKeyValue(HKEY base, const char *keyName, const char *valueName);
protected:

  void SaveData(void *ptr, DWORD len);
  void LoadData(void **ptr, DWORD *len);
  
};

class ArchiveStreamRegisterOut : public ArchiveStreamMemoryOut,public ASR_RegisterInfo
{
public:
  ArchiveStreamRegisterOut(HKEY base, const char *keyName, const char *valueName);
  ~ArchiveStreamRegisterOut(void);
};


class ArchiveStreamRegisterIn : public ArchiveStreamMemoryIn,public ASR_RegisterInfo
{
  HKEY _key;
  char *valueName;
public:
  ArchiveStreamRegisterIn(HKEY base, const char *keyName, const char *valueName);
  ~ArchiveStreamRegisterIn(void);
};
