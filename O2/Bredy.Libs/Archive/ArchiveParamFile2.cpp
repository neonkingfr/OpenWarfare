#include ".\archiveparamfile2.h"
#include "ArchiveStreamMemory.h"
#include "ArchiveStreamBase64Filter.h"

#define VERSION_TEXT "_version"
#define CHECKMARK_TEXT "_mark"
#define DEFAULT_TEXT "_data"

Thread ArchiveStreamMemoryOut *ArchiveParamItemCounter::_stringMem=NULL;

int ArchiveParamEntryLevel::GuessItemIndex(ArchiveStreamMemoryOut &_stringMem, const char *itemName)
	{
    ArchiveParamItemCounter::_stringMem=&_stringMem;
	ArchiveParamItemCounter search,*found;
    search.itemName=itemName-_stringMem.GetBuffer();
	found=level.Find(search);
	if (found!=NULL) return ++found->count;
    if (stringPos==-1) stringPos=(int)_stringMem.Tell();
    search.itemName=(int)_stringMem.Tell();
    _stringMem.DataExchange((void *)itemName,strlen(itemName)+1);
	level.Add(search);
	return -1;
	}  

void ArchiveParamEntryLevel::BeforeFreeLevel(ArchiveStreamMemoryOut &_stringMem)
    {
    if (stringPos!=-1) _stringMem.Seek(stringPos,_stringMem.begin);
    }


ArchiveParamFile2::ArchiveParamFile2(ParamFile &pfile):_pfile(pfile)
  {
  ParamEntryPtr ptr=&_pfile;
  Push(ptr);
  _state=0;
  _error=0;
  _inArrayCnt=0;
  }

ArchiveParamFile2::~ArchiveParamFile2(void)
  {
  }

bool ArchiveParamFile2::OverrideExchangeVersion(int &ver)
  {
  IArchive &arch=*this;
  return arch(VERSION_TEXT,ver);
  }


bool ArchiveParamFile2Out::Check(const char *text)
  {
  IArchive &arch=*this;
  char *ptext=const_cast<char *>(text);
  return arch(CHECKMARK_TEXT,ptext);  
  }

bool ArchiveParamFile2Out::OpenSection(const char *name,int type)
  {	  
  if (_error) return false;
  ArchiveParamEntryLevel &level=CurEntry();
  if (_state==0)
    {
	int index=level.GuessItemIndex(_stringMem,name);
	if (index!=-1)
	  {
	  char *p;
	  sprintf(p=(char *)alloca(strlen(name)+20),"%s%d",name,index);
	  name=p;
	  }
    _variableName=name;
    _state=APFPHF_VARIABLENAME;
    return true;
    }
  if (_state==APFPHF_VARIABLENAME)
    {
	ParamEntryPtr cur=level.ptr;
    ParamClassPtr className=cur->AddClass(_variableName);
    ParamEntryPtr save=className.GetPointer();
    Push(save);
    _variableName=name;
	CurEntry().GuessItemIndex(_stringMem,name);
    return true;
    }
  if (_state & APFPHF_VALUE)
    {
    CreateArray();
    ParamEntryPtr cur=CurEntry().ptr;
    Push(cur->AddArray(name));
    return true;
    }
  if (_state==APFPHF_INARRAY)
    {
    _inArrayCnt++;
    return true;
    }
  return false;
  }

void ArchiveParamFile2Out::CloseSection(const char *name,int type)
  {
  if (_error) return;
  if (_state==APFPHF_INARRAY)
    {
    if (_inArrayCnt--) return;
    _inArrayCnt=0;
    Pop();
    ParamEntryPtr cur=CurEntry().ptr;
    if (!cur->IsArray()) _state=0;            
    return ;
    }
  if (_state==0)
    {
    Pop();
    return ;
    }
  if (_state & APFPHF_VALUE) SaveValue();
  _state &= ~APFPHF_VARIABLENAME;
  return ;
  }

void ArchiveParamFile2Out::SaveArrayValue(ParamEntry *array)
  {
  if (_state & APFPHF_STRINGVALUE) array->AddValue(_strValue);
  else if (_state & APFPHF_INTVALUE) array->AddValue(_intValue);
  else if (_state & APFPHF_REALVALUE) array->AddValue(_realValue);
  _state&=APFPHF_INARRAY;
  }


void ArchiveParamFile2Out::CreateArray()
  {
  ParamEntryPtr cur=CurEntry().ptr;
  ParamEntryPtr array=cur->AddArray(_variableName);
  Push(array);
  SaveArrayValue(array.GetPointer());
  _state=APFPHF_INARRAY;
  }

void ArchiveParamFile2Out::SaveValue()
  {
  if ((_state & APFPHF_VARIABLENAME)==0) 
    {
    ParamEntryPtr cur=CurEntry().ptr;
    ParamEntryPtr def=cur->FindEntry(DEFAULT_TEXT);
    if (def.IsNull()) def=cur->AddArray(DEFAULT_TEXT);
    SaveArrayValue(def.GetPointer());
    }
  else
    {
    ParamEntryPtr cur=CurEntry().ptr;	
    if (_state & APFPHF_STRINGVALUE) cur->Add(_variableName,_strValue);
    else if (_state & APFPHF_INTVALUE) cur->Add(_variableName,_intValue);
    else if (_state & APFPHF_REALVALUE) cur->Add(_variableName,_realValue);  
    }
  _state=0;
  }

void ArchiveParamFile2Out::ExchangeInt(int data)
  {
  if (_error) return;
  if (_state & APFPHF_VALUE) CreateArray();
  _intValue=data;
  if (_state==0) {SaveDefault(APFPHF_INTVALUE);return;}
  _state |=APFPHF_INTVALUE;
  if (_state & APFPHF_INARRAY) SaveArrayValue(CurEntry().ptr.GetPointer());    
  }

void ArchiveParamFile2Out::ExchangeFloat(float data)
  {
  if (_error) return;
  if (_state & APFPHF_VALUE) CreateArray();
  _realValue=data;
  if (_state==0) {SaveDefault(APFPHF_REALVALUE);return;}
  _state |=APFPHF_REALVALUE;
  if (_state & APFPHF_INARRAY) SaveArrayValue(CurEntry().ptr.GetPointer());  
  }
void ArchiveParamFile2Out::ExchangeString(const char *string)
  {
  if (_error) return;
  if (_state & APFPHF_VALUE) CreateArray();
  _strValue=string;
  if (_state==0) {SaveDefault(APFPHF_STRINGVALUE);return;}
  _state |=APFPHF_STRINGVALUE;
  if (_state & APFPHF_INARRAY) SaveArrayValue(CurEntry().ptr.GetPointer());  
  }

void ArchiveParamFile2Out::ExchangeBinary(const void *bindata, size_t size)
  {
  ArchiveStreamMemoryOut memout;
  char *binary="$@";
  memout.DataExchange(binary,2);
  ArchiveStreamBase64Filter coder(&memout);
  coder.TextWrap(false);
  coder.DataExchange(const_cast<void *>(bindata),size);
  memout.DataExchange(binary+2,1);
  ExchangeString(memout.GetBuffer());
  }

void ArchiveParamFile2Out::SaveDefault(int state)
  {
  _state=state;
  SaveValue();
  }

bool ArchiveParamFile2In::Check(const char *text)
  {
  IArchive &arch=*this;
  char *ptext=NULL;
  if (arch(CHECKMARK_TEXT,ptext)==false) return false;
  return strcmp(text,ptext)==0;
  }

bool ArchiveParamFile2In::OpenSection(const char *name,int type)
  {
  ParamEntryPtr fnd;
  ArchiveParamEntryLevel &level=CurEntry();
  if (_arrayIndex>=0) //we are reading an array 
    {
    _inArrayCnt++;
    return true;
    }
  int index=level.GuessItemIndex(_stringMem,name);
  if (index!=-1)
    {
    char *p=(char *)alloca(strlen(name)+20);
    sprintf(p,"%s%d",name,index);
    name=p;
    }
  ParamEntryPtr cur=level;
  fnd=cur->FindEntry(name);
  _item=ParamEntryPtr(); //reset current item
  if (fnd)
    {
    if (fnd->IsClass())
      {
      Push(fnd);
      _popClass=true;
      return true;
      }
    else if (fnd->IsArray())
      {
      Push(fnd);
      _popClass=true;
      _arrayIndex=0;  //mark that it is array
      return true;
      }
    else if (fnd->IsError())
      return false;   
    _item=fnd;            //save current item as actual
    _popClass=false;      //disable pop class on close
    return true;  
    }
  else
    return false;
  }

void ArchiveParamFile2In::CloseSection(const char *name,int type)
  {
  if (_inArrayCnt--) return;
  _inArrayCnt=0;
  if (_popClass)
    Pop();
  _popClass=true; //every close section will move focus to a class. Each next Close must pop this class
  _item=ParamEntryPtr(); //reset current item
  }


bool ArchiveParamFile2In::FindDefaultItem()
  {
  ArchiveParamEntryLevel &level=CurEntry(); //get created level
  _item=level.ptr->FindEntry(DEFAULT_TEXT);
  if (_item)
    {
    if (_item->IsArray())
      level.defArrayIndex++;
    return true;
    }
  return false;
  }


void ArchiveParamFile2In::ExchangeInt(int &data)
  {  
  ArchiveParamEntryLevel &level=CurEntry(); 
  if (_arrayIndex>=0)
    {
    const IParamArrayValue *arr=&(*level.ptr.GetPointer())[_arrayIndex++];
    if (arr) data=arr->GetInt();    
    }
  else if (_item.IsNull())
    {
    if (FindDefaultItem()==false) return;
    if (level.defArrayIndex>=0) 
      {
      const IParamArrayValue *arr=&(*(_item.GetPointer()))[level.defArrayIndex++];
      if (arr) data=arr->GetInt(); 
      }
    else  ExchangeInt(data);      
    }
  else
    {
    data=_item->GetInt();    
    }
  }


void ArchiveParamFile2In::ExchangeFloat(float &data)
  {  
  ArchiveParamEntryLevel &level=CurEntry(); 
  if (_arrayIndex>=0)
    {
    const IParamArrayValue *arr=&(*level.ptr.GetPointer())[_arrayIndex++];
    if (arr) data=arr->GetFloat();    
    }
  else if (_item.IsNull())
    {
    if (FindDefaultItem()==false) return;
    if (level.defArrayIndex>=0) 
      {
      const IParamArrayValue *arr=&(*(_item.GetPointer()))[level.defArrayIndex++];
      if (arr) data=arr->GetFloat(); 
      }
    else  ExchangeFloat(data);      
    }
  else
    {
    data=*_item.GetPointer();    
    }
  }