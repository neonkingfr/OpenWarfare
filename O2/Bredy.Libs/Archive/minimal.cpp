#include "stdafx.h"
#include <Es/essencepch.hpp>


class AllCountersLink;

AllCountersLink *AllCountInstances[1024];
int AllCountInstancesCount;


class AllCountersLink
{
  int _counter;
  
	public:
	AllCountersLink()
	{
	  _counter = 0;
	  if (AllCountInstancesCount>lenof(AllCountInstances))
	  {
	    TRACE0("AllCountInstances overflow");
	    return;
	  }
	  AllCountInstances[AllCountInstancesCount++] = this;
	}
	
	~AllCountersLink(){}

	void operator ++ (int) {_counter++;}
	void operator -- (int) {_counter--;}
	int GetValue() const {return _counter;}
};

void __cdecl LogF(char const *format,...)
  {
  CString s;
  va_list list;
  va_start(list,format);
  s.FormatV(format,list);
  TRACE0(s);
  }

void __cdecl ErrF(char const *,...)
  {
  }