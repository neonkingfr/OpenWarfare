#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <windows.h>
#include ".\archivestreamregister.h"

ASR_RegisterInfo::ASR_RegisterInfo()
{
  _key=NULL;
  _valueName=NULL;
}

ASR_RegisterInfo::~ASR_RegisterInfo()
{
  if (_key) RegCloseKey(_key);
  if (_valueName==NULL) free(_valueName);
}

bool ASR_RegisterInfo::OpenKeyValue(HKEY base, const char *keyName, const char *valueName)
{  
  DWORD disp;
  ASR_RegisterInfo::~ASR_RegisterInfo();  
  if (RegCreateKeyEx(base,keyName,0,NULL,REG_OPTION_NON_VOLATILE,KEY_WRITE|KEY_READ,NULL,&_key,&disp)==ERROR_SUCCESS)
  {
    _valueName=strdup(valueName);
    return true;
  }
  else
  {
    _valueName=NULL;
    _key=NULL;
    return false;
  }
}

void ASR_RegisterInfo::LoadData(void **ptr, DWORD *len)
{
  *len=0;
  *ptr=NULL;
  if (_key==NULL) return;  
  if (RegQueryValueEx(_key,_valueName,NULL,NULL,NULL,len)==ERROR_SUCCESS)
  {
    *ptr=malloc(*len);
    RegQueryValueEx(_key,_valueName,NULL,NULL,*(LPBYTE *)ptr,len);
  }  
}


void ASR_RegisterInfo::SaveData(void *ptr, DWORD len)
{
  ::RegSetValueEx(_key,_valueName,0,REG_BINARY,(BYTE *)ptr,len);
}


ArchiveStreamRegisterIn::ArchiveStreamRegisterIn(HKEY base, const char *keyName, const char *valueName):ArchiveStreamMemoryIn(NULL,0,false)
{
  void *data;
  DWORD size;
  if (OpenKeyValue(base,keyName,valueName)==false)
  {
    SetError(-1);
  }
  else
  {
    LoadData(&data,&size);
    if (data==NULL) SetError(-1);
    else
    {
      this->_buffer=(char *)data;
      this->_alloc=size;
      this->_bufuse=size;
    }
  }
}

ArchiveStreamRegisterIn::~ArchiveStreamRegisterIn(void)
{
  free(this->_buffer);
}

ArchiveStreamRegisterOut::ArchiveStreamRegisterOut(HKEY base, const char *keyName, const char *valueName)
{
  if (OpenKeyValue(base,keyName,valueName)==false) 
  {
    SetError(-1);
    return;
  }
}

ArchiveStreamRegisterOut::~ArchiveStreamRegisterOut(void)
{
  SaveData(GetBuffer(),GetBufferSize());
}
