// ArchiveStreamCompressFilter.cpp: implementation of the ArchiveStreamCompressFilter class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ArchiveStreamCompressFilter.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define DISABLED_ENTRY 0xFFFF
#define OUTBOUND_ENTRY 0xFFFE

ArchiveStreamCompressFilter::ArchiveStreamCompressFilter(ArchiveStream *host)
  {
  _host=host;
  _dict=new Dictionary[ARCHCMPRS_DICTSIZE];
  SetError(-1);
  Reset();

  }

ArchiveStreamCompressFilter::~ArchiveStreamCompressFilter()
  {
  Flush();
  delete [] _dict;
  }

void ArchiveStreamCompressFilter::Flush()
  {
  if (IsStoring() && !IsError()) 
    WriteEndStream();
  _host->Flush();
  }


void ArchiveStreamCompressFilter::Reset()
  {
  Flush();
  for (int i=0;i<256;i++) _dict[i].Init(i);
  _accum=0;
  _accUse=0;
  _nextItem=0;
  _dicRdPos=DISABLED_ENTRY;
  _curBits=9;
  _levelCode=256;
  _eofCode=257;
  _clearCode=258;
  _nextItem=259;
  _host->Reset();
  }


void ArchiveStreamCompressFilter::Dictionary::Init(unsigned short i)
  {
  parent=DISABLED_ENTRY;
  child=DISABLED_ENTRY;
  sibling=DISABLED_ENTRY;
  symbol=i;
  }


void ArchiveStreamCompressFilter::Write(unsigned char zn)
  {
  if (_dicRdPos!=DISABLED_ENTRY)
    {
    unsigned short p=_dict[_dicRdPos].child;  //hledej v dalsi urovni
    if (p!=DISABLED_ENTRY)        //existuje-li dalsi uroven
      {                             //prohledej ji a najdi znak v teto urovni
      if (p==OUTBOUND_ENTRY) 
        {
        FlushWrite();
        WriteClearCode();
        _dicRdPos=zn;
        return;
        }
      while (_dict[p].symbol!=zn && _dict[p].sibling<OUTBOUND_ENTRY) p=_dict[p].sibling;
      if (_dict[p].symbol==zn)  //nalezen znak?
        {
        _dicRdPos=p;    //pokracuj dalsi urovni, zapis timto konci, komprese uspesna
        return;
        }
      else if (_dict[p].sibling==OUTBOUND_ENTRY) //dostali jsme se mimo slovnik?
        {
        FlushWrite();  //v tom pripade je nutne zapsat aktualni stav
        WriteClearCode(); //vymazat slovnik
        _dicRdPos=zn; //a zapisovany znak nechat jako zaklad dalsi skupiny
        }
      else
        {
        FlushWrite();  //zapis aktualni cislo skupiny
        if (_nextItem>=ARCHCMPRS_DICTSIZE) //pokud by dalsi skupina zacala mimo slovnik
          {
          _dict[p].sibling=OUTBOUND_ENTRY; //poznac, ze jsem mimo slovnik
          }
        else
          {
          _dict[_nextItem].Init(zn);      //zaloz novou skupinu
          _dict[_nextItem].parent=_dict[p].parent;
          _dict[p].sibling=_nextItem;     //pripoji jako dcerinou
          _nextItem++;
          }
        }
      }
    else //pokud neni dalsi uroven
      {
      FlushWrite();  //zapis aktualni cislo skupiny
      if (_nextItem>=ARCHCMPRS_DICTSIZE) //pokud by dalsi skupina zacala mimo slovnik
        _dict[_dicRdPos].child=OUTBOUND_ENTRY; //poznac, ze jsem mimo slovnik
      else
        {
        _dict[_nextItem].Init(zn);      //zaloz novou skupinu
        _dict[_nextItem].parent=_dicRdPos;
        _dict[_dicRdPos].child=_nextItem;     //pripoji jako potomka
        _nextItem++;
        }
      }
    }
  _dicRdPos=zn; //zacni hledat novou skupinu
  }

static inline int GetBitCount(int p)
  {
  int cnt=0;
  while (p)
    {
    cnt++;
    p>>=1;
    }
  return cnt;
  }

void ArchiveStreamCompressFilter::FlushWrite()
  {
  int bits=GetBitCount(_dicRdPos);  //zjisti pocet bitu potrebnych pro zapis cisla  
//  while ((_dicRdPos | (1<<bits))<_nextItem) bits++; //zjisti pripadne dalsi bity pro rozliseni dle velikosti slovniku
  while (_curBits<bits) //jestlize soucasna bitova uroven nestaci
    {
    WriteBits(_levelCode,_curBits); //zapis priznak pro zmenu poctu bitu
    _curBits++;               //zvysi interni pocet bitu
    }
  WriteBits(_dicRdPos,_curBits); //zapis cislo v zadanem poctu bitu
  }

void ArchiveStreamCompressFilter::WriteBits(unsigned short value, int bits)
  {
  _accUse+=bits;      //pricti dalsi bity do akumulatoru
  _accum=(_accum<<bits) | (value & ((1<<bits)-1)); //pridej tyto bity do akumulatoru
  if (_accUse>=16)  //pokud akumulator obsahuje vic jak 16 bitu
    {
    _accUse-=16;    //odecti techto 16 bitu
    unsigned short out=(unsigned short)(_accum>>_accUse); //vyzvedni je pres posun
    out=(out>>8)|(out<<8);
    _host->ExSimple(out); //posli na vystup    
    }
  if (_accUse>=8) //pokud akumulator obsahuje aspon 8 bitu
    {
    _accUse-=8;  //odecti je z akumulatoru
    unsigned char out=(unsigned char)(_accum>>_accUse); //vyzvedni je pres posun
    _host->ExSimple(out); //posli na vystup
    }
  }

void ArchiveStreamCompressFilter::WriteClearCode()
  {
  WriteBits(_clearCode,_curBits);
  for (int i=0;i<256;i++) _dict[i].Init(i);
  _nextItem=259;
  _curBits=9;
  }

void ArchiveStreamCompressFilter::WriteEndStream()
  {
  if (IsStoring())
    {
    if (_dicRdPos!=DISABLED_ENTRY) FlushWrite();
    WriteBits(_eofCode,_curBits);
    if (_curBits) 
      WriteBits(0,8-_accUse);
    SetError(ARCHCMPRS_SHUTDOWN);
    }
  }

unsigned char ArchiveStreamCompressFilter::Read()
  { 
  if (_dicRdPos==DISABLED_ENTRY) //stane se pouze na zacatku
    {
    _dicRdPos=ReadGroup();  
    unsigned char znk=(unsigned char) _dict[_dicRdPos].symbol;
    return znk;
    }
  unsigned short nx=_dict[_dicRdPos].child;
  if (nx==OUTBOUND_ENTRY)  //mimo slovnik! chyba
    {
    SetError(ARCHCMPRS_INTEGRITYERROR);
    return 0;
    }
  if (nx==DISABLED_ENTRY)
    {
    bool dble=false;
    nx=ReadGroup();
    if (_dicRdPos==DISABLED_ENTRY)
      {
      _dicRdPos=nx;
      unsigned char znk=(unsigned char) _dict[_dicRdPos].symbol;
      return znk;
      }
    if (nx>_nextItem) //tento pripad znaci chybu
      {
      SetError(ARCHCMPRS_INTEGRITYERROR);
      return 0;
      }
    if (nx==_nextItem) //tento pripad znaci dva a vice znaku stejne za sebou
      { //je nutne pracovat s neexistujici skupinou, proto se musi nyni vlozit
      _dict[_nextItem].Init(_dict[_dicRdPos].symbol);
      _dict[_nextItem].parent=_dicRdPos;
      _nextItem++;
      dble=true;
      }
    _dict[nx].child=DISABLED_ENTRY;
    while (_dict[nx].parent!=DISABLED_ENTRY)
      {
      unsigned short p=nx;
      nx=_dict[nx].parent;
      _dict[nx].child=p;
      }
    if (!dble) //vlozi se nova skupina pokud jiz nebyla vytvorena
      {
      if (_nextItem<ARCHCMPRS_DICTSIZE)
        {
        _dict[_nextItem].Init(_dict[nx].symbol);
        _dict[_nextItem].parent=_dicRdPos;
        _nextItem++;
        }
      }
    else      
      _dict[_nextItem-1].symbol=_dict[nx].symbol;

    _dicRdPos=nx;
    return (unsigned char)_dict[nx].symbol;
    }
  _dicRdPos=_dict[_dicRdPos].child;
  return (unsigned char)_dict[_dicRdPos].symbol;
  }

unsigned short ArchiveStreamCompressFilter::ReadGroup()
  {
  unsigned short command=ReadBits(_curBits);
  while (true)
    {
    if  (command==_levelCode) 
      _curBits++;
    else if (command==_clearCode)
      {
      _curBits=9;
      _nextItem=259;
      _dicRdPos=DISABLED_ENTRY;
      for (int i=0;i<256;i++) _dict[i].Init(i);
      }
    else if (command==_eofCode)
      {
      SetError(ARCHCMPRS_SHUTDOWN);
      return 0;
      }
    else return command;
    command=ReadBits(_curBits);
    }  
  }

unsigned short ArchiveStreamCompressFilter::ReadBits(int bits)
  {
  unsigned char c;
  unsigned short out;
  while (_accUse<bits)
    {
    _host->ExSimple(c);
    if (IsError()) 
      return 0;
    _accum=(_accum<<8) | c;
    _accUse+=8;
    }
  _accUse-=bits;
  out=(unsigned short)((_accum>>_accUse) & ((1<<bits)-1));
  return out;
  }

int ArchiveStreamCompressFilter::DataExchange(void *buffer, int maxsize)
  {
  if (IsError()) return IsError();
  unsigned char *datap=(unsigned char *)buffer;
  if (IsStoring())
    {
    for (int i=0;i<maxsize && !IsError();i++) Write(datap[i]);
    }
  else
    {
    for (int i=0;i<maxsize && !IsError();i++) 
      {
      unsigned char c=Read();
      if (!IsError()) datap[i]=c;
      }
    }
  return IsError();
  }