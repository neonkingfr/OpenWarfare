#ifndef _XMLARCHIVE_H_BREDY_
#define _XMLARCHIVE_H_BREDY_

#include <wchar.h>
#include "../Archive.h"
#include "../ArchiveStreamMemory.h"

#define XMLARCH_IMPROPERUSAGEOFLOCALVARIABLE -2501 //trying open the section of local variable
#define XMLARCH_UNCOMPLETTEDATA -2502 //requested data is larger, that data in stream. This is reported as error in Archive
#define XMLARCH_BADDATA -2503
#define XMLARCH_UNABLETOOPENROOT -2504

#define XMLARCH_EXCHANGEOUT(type,funct)\
	protected: virtual int OverrideDoExchange(type &data) \
      {funct(data);return _stream->IsError();}

class XMLArchiveCommon: public Archive
{
public:
  const char *encoding;
  const char *version;

  XMLArchiveCommon(ArchiveStream &str):Archive(str),encoding(0),version(0) {}
  ~XMLArchiveCommon() {if (IsReading()) {delete [] const_cast<char *>(encoding);delete [] const_cast<char *>(version);} }
  virtual bool OverrideExchangeVersion(int &ver);
  virtual bool Check(const char *text);
  
  bool ExchangeHeader(const char *encoding);
};


class XMLArchiveOut: public XMLArchiveCommon
{
  bool _tagopen;  
  bool _variableSect;
  char _separate;  
  const char *_topSection;  

public:
  XMLArchiveOut(ArchiveStream &str,const char *topSection=NULL, const char *encoding=NULL):
      XMLArchiveCommon(str),_tagopen(false),_variableSect(false),_separate(0),_topSection(topSection)        
      {if (encoding) ExchangeHeader(encoding);        
        if (topSection) OpenSection(topSection);}
  ~XMLArchiveOut() 
     {if (_topSection) CloseSection(_topSection);}
  virtual bool OpenSection(const char *name,int type=0);
  virtual void CloseSection(const char *name,int type=0);
  virtual bool HandleDefaultValue() {return true;}
  virtual bool CanCloseSection() {return true;}
  virtual int OverrideDoBinaryExchange(void *ptr, unsigned long size);
  virtual void Reserved(int bytes) {}

  XMLARCH_EXCHANGEOUT(bool,WriteBool)
  XMLARCH_EXCHANGEOUT(char,WriteChar)
  XMLARCH_EXCHANGEOUT(unsigned char,WriteChar)
  XMLARCH_EXCHANGEOUT(short,WriteInt)
  XMLARCH_EXCHANGEOUT(unsigned short,WriteInt)
  XMLARCH_EXCHANGEOUT(int,WriteInt)
  XMLARCH_EXCHANGEOUT(unsigned int,WriteInt)
  XMLARCH_EXCHANGEOUT(long,WriteInt)
  XMLARCH_EXCHANGEOUT(unsigned long,WriteInt)
  XMLARCH_EXCHANGEOUT(__int64,WriteInt)
  XMLARCH_EXCHANGEOUT(unsigned __int64,WriteInt)
  XMLARCH_EXCHANGEOUT(float,WriteReal)
  XMLARCH_EXCHANGEOUT(double,WriteReal)
  XMLARCH_EXCHANGEOUT(char *,WriteString)
  XMLARCH_EXCHANGEOUT(short *,WriteString)
  XMLARCH_EXCHANGEOUT(unsigned short *,WriteString)
  XMLARCH_EXCHANGEOUT(unsigned char *,WriteString)


  void WriteInt(__int64 val);
  void WriteReal(double val);
  void WriteString(const char *text);  
  void WriteString(const unsigned short *text);  
  void WriteString(const unsigned char *text) {WriteString((const char *)text);}
  void WriteString(const short *text) {WriteString((const unsigned short *)text);}
  void WriteBool(bool val);
  void WriteChar(char c) {OverrideDoBinaryExchange(&c,1);}
  void WriteChar(unsigned char c) {OverrideDoBinaryExchange(&c,1);}

  bool CloseTag(bool sectionclose);
  bool CloseVariable();

  void WriteSeparator();
};


#undef XMLARCH_EXCHANGEOUT
#define XMLARCH_EXCHANGEINSTR(type1,type2) \
	protected: virtual int OverrideDoExchange(type1 &data) \
      {data=(type1)ReadString((type2)data);return _stream->IsError();}

#define XMLARCH_EXCHANGEIN(type1,type2,func) \
	protected: virtual int OverrideDoExchange(type1 &data) \
      { \
        type2 pdata=(type2)data;\
        if (func(pdata)==false) return XMLARCH_BADDATA;\
        data=(type1)pdata;        \
        return _stream->IsError();\
      }

#include "..\..\StreamParser\XMLParser.h"

class XMLArchiveInStream
{
  ArchiveStream *_stream;
public:
  XMLArchiveInStream(ArchiveStream *stream):_stream(stream) {}
  bool ReadStream(char *array, size_t toread, size_t &wasread); 
};

class XMLArchiveIn: public XMLArchiveCommon
{
  XMLParser<XMLArchiveInStream> _parser;
  char *ReadIntoBuffer(size_t &sz);
  XMLArchiveInStream _xmlstream;

public:
  XMLArchiveIn(ArchiveStream &stream,const char *root=NULL);
  virtual bool OpenSection(const char *name,int type=0);
  virtual void CloseSection(const char *name,int type=0);
  virtual int OverrideDoBinaryExchange(void *ptr, unsigned long size);
  XMLARCH_EXCHANGEINSTR(char *,char *)
  XMLARCH_EXCHANGEINSTR(unsigned char *,char *)
  XMLARCH_EXCHANGEINSTR(short *,unsigned short *)
  XMLARCH_EXCHANGEINSTR(unsigned short *,unsigned short *)

  XMLARCH_EXCHANGEIN(bool,bool,ReadBool)
  XMLARCH_EXCHANGEIN(char,char,ReadChar)
  XMLARCH_EXCHANGEIN(unsigned char,char,ReadChar)
  XMLARCH_EXCHANGEIN(short,__int64,ReadInt)
  XMLARCH_EXCHANGEIN(unsigned short,__int64,ReadInt)
  XMLARCH_EXCHANGEIN(int,__int64,ReadInt)
  XMLARCH_EXCHANGEIN(unsigned int,__int64,ReadInt)
  XMLARCH_EXCHANGEIN(long,__int64,ReadInt)
  XMLARCH_EXCHANGEIN(unsigned long,__int64,ReadInt)
  XMLARCH_EXCHANGEIN(__int64,__int64,ReadInt)
  XMLARCH_EXCHANGEIN(unsigned __int64,__int64,ReadInt)
  XMLARCH_EXCHANGEIN(float,double,ReadReal)
  XMLARCH_EXCHANGEIN(double,double,ReadReal)

  char *ReadString(char *val);
  unsigned short *ReadString(unsigned short *val);
  bool ReadInt(__int64 &val);
  bool ReadReal(double &val);
  bool ReadBool(bool &val);
  bool ReadChar(char &val);
  
  virtual void Reserved(int bytes) {}

};
/*
class XMLArchiveIn: public XMLArchiveCommon
{
  
  int _nextChar;
  int _nextData;
  int _putBack;
  ArchiveStreamMemoryOut _readBuff;   //buffer that holds whole tag
  int _parsePos;        //currently parsing position in variable section -1 no variable selected
  int *_index;			//pointer to index table
  const char *_topSection;
  bool _inTag;          //inTag readings are available
  
  void GetNext() 
    {
      char p;
      if (_stream->IsError()) _nextChar=-1;
      else
      {       
        _stream->ExSimple(p);
        if (_stream->IsError()) _nextChar=-1;
        else _nextChar=p;
      }
    }
  
  void GetNextDataChar();

  bool ReadTag();
  void ProcessTagVariables();
  int GetXMLChar();

  ArchiveStreamMemoryOut _stringBuff;

public:
  XMLArchiveIn(ArchiveStream &str,const char *topSection=NULL):
     _topSection(topSection),_nextChar(0),_parsePos(-1),
        _nextData(0),_putBack(-2),XMLArchiveCommon(str),_inTag(false)
      {
        GetNext();
        ExchangeHeader(NULL);
        if (_topSection) 
          if (OpenSection(_topSection)==false) SetError(XMLARCH_BADFILE);               
      }
  ~XMLArchiveIn()
  {
    if (_topSection) CloseSection(_topSection);
  }


  virtual bool OpenSection(const char *name,int type=0);
  virtual void CloseSection(const char *name,int type=0);
  virtual int OverrideDoBinaryExchange(void *ptr, unsigned long size);

  XMLARCH_EXCHANGEINSTR(char *,char *)
  XMLARCH_EXCHANGEINSTR(unsigned char *,char *)
  XMLARCH_EXCHANGEINSTR(short *,unsigned short *)
  XMLARCH_EXCHANGEINSTR(unsigned short *,unsigned short *)

  XMLARCH_EXCHANGEIN(bool,bool,ReadBool)
  XMLARCH_EXCHANGEIN(char,char,ReadChar)
  XMLARCH_EXCHANGEIN(unsigned char,char,ReadChar)
  XMLARCH_EXCHANGEIN(short,__int64,ReadInt)
  XMLARCH_EXCHANGEIN(unsigned short,__int64,ReadInt)
  XMLARCH_EXCHANGEIN(int,__int64,ReadInt)
  XMLARCH_EXCHANGEIN(unsigned int,__int64,ReadInt)
  XMLARCH_EXCHANGEIN(long,__int64,ReadInt)
  XMLARCH_EXCHANGEIN(unsigned long,__int64,ReadInt)
  XMLARCH_EXCHANGEIN(__int64,__int64,ReadInt)
  XMLARCH_EXCHANGEIN(unsigned __int64,__int64,ReadInt)
  XMLARCH_EXCHANGEIN(float,double,ReadReal)
  XMLARCH_EXCHANGEIN(double,double,ReadReal)

  char *ReadString(char *val);
  unsigned short *ReadString(unsigned short *val);
  bool ReadInt(__int64 &val);
  bool ReadReal(double &val);
  bool ReadBool(bool &val);
  bool ReadChar(char &val);
  
  virtual void Reserved(int bytes) {}

};
*/
#undef XMLARCH_EXCHANGEINSTR
#undef XMLARCH_EXCHANGEIN




















#endif




