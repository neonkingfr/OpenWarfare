// XMLArchive.cpp : Defines the entry point for the console application.
//
#include <stdio.h>
#include <string.h>
#include "XMLArchive.h"
#include "../ArchiveStreamMemory.h"
#include "../ArchiveStreamWindowsFile.h"
#include <math.h>
#include "..\..\StreamParser\XMLParser.tli"



bool XMLArchiveCommon::ExchangeHeader(const char *encode)
{
   const char *xmlname="?xml";
   if (OpenSection(xmlname,0)==false) return false;   
   version=RValuePtr("_version","1.0");
   encoding=RValuePtr("_encoding",(char * const)encode);
   CloseSection(xmlname,0);   
   return true;
}

bool  XMLArchiveOut::OpenSection(const char *name,int type)
{
  Archive::OpenSection(name,type);
  if (_variableSect)
  {
    SetError(XMLARCH_IMPROPERUSAGEOFLOCALVARIABLE);
    return false;
  }
  _separate=false;
  if (name[0]=='_')   
    if (_tagopen)
    {
      _stream->DataExchange((void *)(" "),1);
      _stream->DataExchange((void *)(name+1),strlen(name+1));
      _stream->DataExchange((void *)("=\""),2);
      _variableSect=true;
      return true;
    }
    else
      name=name+1;
  CloseTag(false);
  unsigned char begin='<';
  _stream->ExSimple(begin);
  _stream->DataExchange((void *)name,strlen(name));
  _tagopen=true;
  return true;
}

void XMLArchiveOut::CloseSection(const char *name,int type)
{
  if (_variableSect) CloseVariable();
  else
  {
    if (name[0]=='?' && _tagopen)
    {
      _stream->DataExchange((void *)("?>"),2);
      _tagopen=false;
    }
    else if (!CloseTag(true))
    {
      if (name[0]=='_') name++;
      _stream->DataExchange((void *)("</"),2);
      _stream->DataExchange((void *)name,strlen(name));
      _stream->DataExchange((void *)(">"),1);
    }
  }
  _separate=false;
  Archive::CloseSection(name,type);
}


bool XMLArchiveOut::CloseVariable()
{
  if (_variableSect)
  {
    char *end="\"";
    _stream->DataExchange(end,1);
    _variableSect=false;
    return true;
  }
  else
    return false;
}
bool XMLArchiveOut::CloseTag(bool sectionclose)
{
  if (_tagopen)
  {
    char *end="/>";
    if (sectionclose)
    {
      _stream->DataExchange(end,2);
    }
    else
    {
      _stream->DataExchange(end+1,1);
    }
    _tagopen=false;
    return true;
  }
  else 
    return false;
}

bool XMLArchiveCommon::OverrideExchangeVersion(int &ver)
{
  this->RValue("_version",ver);
  return true;
}

bool XMLArchiveCommon::Check(const char *text)
{  
  char *sign=this->RValuePtr("_signature",(char *const)text);
  if (IsStoring()) return true;

  bool res=(strcmp(sign,text)==0);
  delete [] sign;
  return res;
}

int XMLArchiveOut::OverrideDoBinaryExchange(void *ptr, unsigned long size)
{
  if (!_variableSect) CloseTag(false);
  char *p=(char *)ptr;
  char buff[25];
  int beg=0;
  buff[0]=0;
  for (int i=0;i<(signed)size;i++) 
  {
    switch (p[i])
    {
    case '\"':strcpy(buff,"&quot;");break;
    case '&':strcpy(buff,"&amp;");break;
    case '<':strcpy(buff,"&lt;");break;
    case '>':strcpy(buff,"&gt;");break;
    case '\'':strcpy(buff,"&apos;");break;
    default:
      if ((unsigned)p[i]<32) sprintf(buff,"&#%d;",(unsigned char)p[i]);
    }
    if (buff[0])
    {
      if (i!=beg) _stream->DataExchange(p+beg,i-beg);
      _stream->DataExchange(buff,strlen(buff));
      beg=i+1;
      buff[0]=0;
    }
  }
  if (i!=beg) _stream->DataExchange(p+beg,i-beg);
  return _stream->IsError();
}

void XMLArchiveOut::WriteInt(__int64 val)
{
  WriteSeparator();
  char buff[256];
  sprintf(buff,"%I64d",val);
  OverrideDoBinaryExchange(buff,strlen(buff));
  _separate=1;
}
void XMLArchiveOut::WriteReal(double val)
{
  WriteSeparator();
  char buff[256];
  sprintf(buff,"%g",val);
  OverrideDoBinaryExchange(buff,strlen(buff));
  _separate=1;
}
void XMLArchiveOut::WriteString(const char *text)
{
  WriteSeparator();
  OverrideDoBinaryExchange((void *)text,text?strlen(text):0);
  _separate=2;
}

void XMLArchiveOut::WriteString(const unsigned short *text)
{
  WriteSeparator();
  char buff[100];
  int sz=WideCharToMultiByte(CP_UTF8,0,text,wcslen(text),NULL,0,NULL,NULL);
  char *z;
  if (sz>=100)  z=new char[sz+1];else z=buff;
  z[sz]=0;
  WideCharToMultiByte(65001,0,text,wcslen(text),z,sz,NULL,NULL);
  OverrideDoBinaryExchange(z,sz);
  if (sz<100) delete []z;
  _separate=2;
}


void XMLArchiveOut::WriteBool(bool val)
{
  char buff=val?'Y':'N';
  WriteSeparator();
  OverrideDoBinaryExchange(&buff,1);
  _separate=0;
}


void XMLArchiveOut::WriteSeparator()
{
  if (_separate)  
  {
	char *sep=_separate==1?",":"&#9;";
    _stream->DataExchange(sep,strlen(sep));
    _separate=0;
  }
}

bool XMLArchiveIn::OpenSection(const char *name,int type)
{
  if (GetError()) return false;
  if (name[0]=='_')
  {
	name=name+1;
	if (_parser.CanReadAttribute())
	{
	  return _parser.OpenAttribute(name);
	}	
  }
  return _parser.OpenTag(name);
}

void XMLArchiveIn::CloseSection(const char *name,int type)
{
  if (GetError()) return;
  if (name[0]=='_')
  {
	name=name+1;
	if (_parser.NeedCloseAttribute()) 
	{
	  _parser.CloseAttribute(name);
	  return;
	}
  }
  _parser.CloseTag(name);
}

int XMLArchiveIn::OverrideDoBinaryExchange(void *ptr, unsigned long size)
{
  if (GetError()) return GetError();
  int i=_parser.GetChar();
  char *out=reinterpret_cast<char *>(ptr);
  while(i>=0 && size--)
  {
	*out++=(char)i;
	i=_parser.GetChar();
  }
  if (size!=0) return XMLARCH_UNCOMPLETTEDATA;
  return 0;
}

char *XMLArchiveIn::ReadIntoBuffer(size_t &sz)
{
  ArchiveStreamMemoryOut memout;
  int i=_parser.GetChar();
  while (i>0 && i!=9)
  {
	char znk=(char)i;
	memout.ExSimple(znk);
	i=_parser.GetChar();
  }
  memout.DataExchange((void *)(""),1);
  sz=memout.GetBufferSize();
  return memout.DetachBuffer();

}


char *XMLArchiveIn::ReadString(char *val)
{
  if (val) free(val);
  size_t sz;
  char *buff=ReadIntoBuffer(sz);
  char *res=strdup(buff);
  free(buff);
  return res;
}

unsigned short *XMLArchiveIn::ReadString(unsigned short *val)
{
  if (val) free(val);  
  size_t mbsz;
  char *mbstr=ReadIntoBuffer(mbsz);
  size_t needsz=MultiByteToWideChar(65001,0,mbstr,mbsz,NULL,0);
  unsigned short *out=(unsigned short *)malloc((mbsz+1)*2);
  MultiByteToWideChar(65001,0,mbstr,mbsz,out,needsz);
  out[needsz]=0;
  free(mbstr);
  return out;
}

bool XMLArchiveIn::ReadInt(__int64 &val)
{
  val=0;
  bool neg=false;
  bool res=false;
  int i=_parser.GetChar();
  if (i=='+') {i=_parser.GetChar();}
  if (i=='-') {neg=true;i=_parser.GetChar();}
  while (isdigit(i))
  {
	val=val*10+(i-48);
	i=_parser.GetChar();
	res=true;
  }
  if (neg) val=-val;
  return res;
}

bool XMLArchiveIn::ReadReal(double &val)
{
  val=0;
  bool neg=false;
  bool res=false;
  int i=_parser.GetChar();
  if (i=='+') {i=_parser.GetChar();}
  if (i=='-') {neg=true;i=_parser.GetChar();}
  while (isdigit(i))
  {
	val=val*10+(i-48);
	i=_parser.GetChar();
	res=true;
  }
  if (i=='.')
  {
	i=_parser.GetChar();
	double frac=0.1;
	while (isdigit(i))
	{
	  val+=frac*(i-48);
	  i=_parser.GetChar();
	  res=true;
	  frac/=10;
	}
  }
  if (i=='E' || i=='e')
  {
	__int64 expval;
	if (ReadInt(expval)==false) return false;
	double expf=pow(10.0,(double)expval);
	val*=expf;
  }
  if (neg) val=-val;
  return res;
}

bool XMLArchiveIn::ReadBool(bool &val)
{
  int i=_parser.GetChar();
  if (i=='Y' || i=='A' || i=='1' || i=='T') 
	{val=true;return true;}
  if (i=='N' || i=='1' || i=='F') 
	{val=false;return true;}
  if (i=='o' || i=='O')
  {
	i=_parser.GetChar();
	if (i=='n' || i=='N') 
	  {val=true;return true;}
	if (i=='f' || i=='f') 
	{
	  i=_parser.GetChar();
	  if (i=='f' || i=='f') 
		{val=false;return true;}
	}
  }
  return false;
}

bool XMLArchiveIn::ReadChar(char &val)
{
  int i=_parser.GetChar();
  if (i<0) return false;
  val=(char)i;
  return true;
}

bool XMLArchiveInStream::ReadStream(char *array, size_t toread, size_t &wasread)
{
  ///ne�pln� implementace
  int err=_stream->DataExchange(array,toread);
  if (err) wasread=0;
  else wasread=toread;
  return true;
}

XMLArchiveIn::XMLArchiveIn(ArchiveStream &stream,const char *root)
:XMLArchiveCommon(stream),_xmlstream(&stream),_parser(&_xmlstream,false)
{
   ExchangeHeader(encoding);
   if (root) 
   {
	 if (OpenSection(root)==false)
	 {
	   SetError(XMLARCH_UNABLETOOPENROOT);
	 }
   }
}

#ifdef XMLARCHIVE_TEST

void Serialize(IArchive &arch)
{
  bool a=true;
  int b=10;
  char *c=strdup("Ahoj");
  char *d=strdup("Toto je dlouh� v�ta,\n��st se nach�z� na nov� ��dce\npou��vaj� se tu zna�ky < a > a jin� & a \"uvozovky\"");
  int _l=3;
  char *_lc=strdup("both");
  if (arch.Check("TESTXML")==false) return;
  ArchiveSection sect(arch);
  while (sect.Block("block1"))
  {
    arch.RequestVer(1);
    arch("_local",_l);
    arch("_loctext",_lc);
    arch("name",c);
    arch("description",d);
    ArchiveSection sect(arch);
    while (sect.Block("multi-str"))
    {
      const char *x=arch.RValuePtr("Hallo");
      const char *y=arch.RValuePtr("World");
      int v=arch.RValue((int)1235);
      float fv=arch.RValue((float)-1.323E-0033);
      bool q=arch.RValue(false);
    }
  }
  while (sect.Block("block2"))
  {
    arch("_enabled",a);
    arch("_value",b);
  }
  free(_lc);
  free(c);
  free(d);
}

int main(int argc, char* argv[])
{
  {
  ArchiveStreamWindowsFileOut  strout(
     CreateFile("Pokus.xml",GENERIC_WRITE,0,NULL,CREATE_ALWAYS,0,NULL),true);
  XMLArchiveOut arch(strout,"xml","Windows-1250");

  Serialize(arch);
  }

  {
  ArchiveStreamWindowsFileIn  strin(
     CreateFile("Pokus.xml",GENERIC_READ,0,NULL,OPEN_EXISTING,0,NULL),true);
  XMLArchiveIn arch(strin,"xml");

  Serialize(arch);
  ASSERT(arch.GetError()==0);
  }


  return 0;
}

#endif