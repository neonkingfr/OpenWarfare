// ArchiveStreamBuffer.h: interface for the ArchiveStreamBuffer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARCHIVESTREAMBUFFER_H__01305E7F_DDB9_4937_8793_1D0D85C5DDF4__INCLUDED_)
#define AFX_ARCHIVESTREAMBUFFER_H__01305E7F_DDB9_4937_8793_1D0D85C5DDF4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ArchiveStream.h"

class ArchiveStreamBuffer : public ArchiveStream  
{
    char *_buffer;
    unsigned long _size;
    unsigned long _used;
    ArchiveStream *_host;
public:
	ArchiveStreamBuffer();
	virtual ~ArchiveStreamBuffer();

};

#endif // !defined(AFX_ARCHIVESTREAMBUFFER_H__01305E7F_DDB9_4937_8793_1D0D85C5DDF4__INCLUDED_)
