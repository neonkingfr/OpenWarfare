#if !defined(AFX_MERGENEARDLG_H__C7EFE7B2_40F7_4C20_A385_6AAA55087134__INCLUDED_)
#define AFX_MERGENEARDLG_H__C7EFE7B2_40F7_4C20_A385_6AAA55087134__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MergeNearDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMergeNearDlg dialog

class CMergeNearDlg : public CDialog
  {
  // Construction
  public:
    ObjectData *obj;
    CMergeNearDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CMergeNearDlg)
    enum 
      { IDD = IDD_MERGENEAR };
    float	distance;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CMergeNearDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CMergeNearDlg)
    afx_msg void OnDetect();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MERGENEARDLG_H__C7EFE7B2_40F7_4C20_A385_6AAA55087134__INCLUDED_)
