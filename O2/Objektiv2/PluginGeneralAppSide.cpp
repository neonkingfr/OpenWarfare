#include "StdAfx.h"
#include "../ObjektivLib/ObjToolTopology.h"
#include "MainFrm.h"
#include "CreatePrimDlg.h"
#include ".\plugingeneralappside.h"


static HANDLE GPluginInitSharedMemory=0;



PluginGeneralAppSide::PluginGeneralAppSide(void)
{
  _pluginSide=0;
  _appframe=0;
  this->O2Allocator=&malloc;
  this->O2Deallocator=&free;
  _plugInstance=0;
  }

PluginGeneralAppSide::~PluginGeneralAppSide(void)
{
  UnloadPlugin();
}

bool PluginGeneralAppSide::UnloadPlugin()
{
  if (_pluginSide) {_pluginSide->BeforePluginUnload();_pluginSide=0;}
  if (_plugInstance) {FreeLibrary(_plugInstance);  _plugInstance=0;}
  return true;
}

typedef IPluginGeneralPluginSide *(*InitProcType)(IPluginGeneralAppSide *appside);

bool PluginGeneralAppSide::LoadPlugin(const char *dllpath)
{
  LogF("(Plugins) Loading %s",dllpath);
  _plugInstance=LoadLibrary(dllpath);
  if (_plugInstance==NULL) 
  {
    LogF("(Plugins) LoadLibrary failed: error code %d",GetLastError());
    return false;
  }
  InitProcType initProc=(InitProcType)GetProcAddress(_plugInstance,ObjektivPluginMainStr);
  if (initProc==NULL) 
  {
    LogF("(Plugins) Invalid module. Missing link to InitPluginModule function (%d)",GetLastError());
    FreeLibrary(_plugInstance);
    _plugInstance=NULL;
    return false;
  }
  _pluginSide=initProc(this);
  if (_pluginSide==NULL) 
  {
    LogF("(Plugins) Unable to get plugin's interface. Init failed");
    FreeLibrary(_plugInstance);
    _plugInstance=NULL;
    return false;
  }
  LogF("(Plugins) Plugin successfully loaded");
  return true;

}
void PluginGeneralAppSide ::UpdateViewer()
{
  frame->_externalViewer.RedrawViewer();
}

void PluginGeneralAppSide ::ReloadViewer()
{
  doc->UpdateViewer(0);
}
SccFunctions *PluginGeneralAppSide ::GetSccInterface()
{
  return theApp._scc;
}

const char *PluginGeneralAppSide ::GetProjectRoot()
{
  return config->GetString(IDS_CFGPATHFORTEX);
}

ROCheckResult PluginGeneralAppSide ::TestFileRO(const _TCHAR *filename,int flags,HWND hWnd)
{  
  HWND old=theApp.rocheck.hWndOwner;
  if (hWnd) theApp.rocheck.hWndOwner=hWnd;
  ROCheckResult res=theApp.rocheck.TestFileRO(filename,flags);
  theApp.rocheck.hWndOwner=old;
  return res;
}

bool PluginGeneralAppSide ::IsViewerActive()
{
  ExternalViewer::ErrorEnum error=frame->_externalViewer.IsReady();
  return error==ExternalViewer::ErrBusy || error==ExternalViewer::ErrOK;
}

IExternalViewer *PluginGeneralAppSide ::GetViewerInterface()
{
  return &frame->_externalViewer;
}

bool PluginGeneralAppSide ::OpenOptionsDialog()
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  frame->OpenOptions();
  return true;
}

void PluginGeneralAppSide ::UpdateViewer(ObjectData *obj)
{
  AFX_MANAGE_STATE(::AfxGetAppModuleState());
  LODObject lod;
  lod.AddLevel(*obj,1.0f);
  lod.DeleteLevel(0);  
  frame->_externalViewer.Update(&lod,NULL,1000);
}

HMENU PluginGeneralAppSide ::AddSubMenu(PopupMenu menuId, const char *menuName)
{
  AFX_MANAGE_STATE(::AfxGetAppModuleState());
  return frame->AddSubMenu(menuId,menuName);
}

/*bool PluginGeneralAppSide ::AllocMenuItem(PopupMenu menuId, HMENU &popup, int &recomendedID)
{
  return frame->AllocMenuItem(menuId,popup,recomendedID);
}*/

void PluginGeneralAppSide ::SelectInO2(const char *resourceName, ResourceType resourceType)
{
  AFX_MANAGE_STATE(::AfxGetAppModuleState());
  ObjectData &obj=*doc->LodData->Active();
  if (resourceType==resourceMaterial)
  {
    for(int i=0;i<obj.NFaces();i++) 
    {
      FaceT fc(obj,i);
      obj.FaceSelect(i,_stricmp(fc.GetMaterial(),resourceName)==0);
    }
    obj.SelectPointsFromFaces();
  }
  else if (resourceType==resourceMaterial)
  {
    for(int i=0;i<obj.NFaces();i++) 
    {
      FaceT fc(obj,i);
      obj.FaceSelect(i,_stricmp(fc.GetTexture(),resourceName)==0);
    }
    obj.SelectPointsFromFaces();
  }
  doc->UpdateAllViews(NULL);
}

bool PluginGeneralAppSide ::AddMenuItem(HMENU mnu, const char *menuName, IPluginCommand *command)
{
  AFX_MANAGE_STATE(::AfxGetAppModuleState());
  return frame->AddMenuCommand(mnu,menuName,command);
}
bool PluginGeneralAppSide ::AddMenuItem(PopupMenu menuId, const char *menuName, IPluginCommand *command)
{
  AFX_MANAGE_STATE(::AfxGetAppModuleState());
  return frame->AddMenuCommand(menuId,menuName,command);
}

bool PluginGeneralAppSide::AddPopupMenuItem(HMENU mnu, const char *menuName, IPluginCommand *command)
  {
  AFX_MANAGE_STATE(::AfxGetAppModuleState());
    return theApp.AddToPopupMenu(mnu,menuName,command);   
  }

const LODObject *PluginGeneralAppSide::GetActiveObject()
{
  AFX_MANAGE_STATE(::AfxGetAppModuleState());
  CObjektiv2Doc *doc=static_cast<CObjektiv2Doc *>(frame->GetActiveDocument());
  return doc->LodData;
}
const ObjectData *PluginGeneralAppSide::GetActiveLevel()
{
  AFX_MANAGE_STATE(::AfxGetAppModuleState());
  CObjektiv2Doc *doc=static_cast<CObjektiv2Doc *>(frame->GetActiveDocument());
  return doc->LodData->Active();
}
class CIFakeEmpty: public CICommand
{
public:
  CIFakeEmpty() {recordable=false;}
  virtual int Execute(LODObject *obj) {return 0;}
};

bool PluginGeneralAppSide::UpdateActiveObject(const LODObject &newObject)
{
  AFX_MANAGE_STATE(::AfxGetAppModuleState());

  class CISet: public CICommand
  {
      LODObject newObject;
    public:
        CISet(const LODObject &newObject):newObject(newObject) {recordable=false;}
        virtual int Execute(LODObject *obj) {
            *obj = newObject;
            return 0;
        }
  };
  
  comint.Run(new CISet(newObject));
  return true;
}
bool PluginGeneralAppSide::UpdateActiveLevel(const ObjectData &objectData)
{
  AFX_MANAGE_STATE(::AfxGetAppModuleState());
  class CISet: public CICommand
  {
      ObjectData newObject;
    public:
        CISet(const ObjectData &newObject):newObject(newObject) {recordable=false;}
        virtual int Execute(LODObject *obj) {
            *obj->Active() = newObject;
            return 0;
        }
  };
  
  comint.Run(new CISet(objectData));
  return true;
}
bool PluginGeneralAppSide::ModifyActiveLevel(const ObjectData *merge, Selection *delSelects)
{
  AFX_MANAGE_STATE(::AfxGetAppModuleState());
  class CISet: public CICommand
  {
      ObjectData *merge;
      Selection *delSelects;
    public:
        CISet(const ObjectData *merge, Selection *delSelects):
          merge(merge?new ObjectData(*merge):0),
         delSelects(delSelects?new Selection(*delSelects):0) {}
        virtual int Execute(LODObject *lod) 
        {
          ObjectData *obj=lod->Active();
          if (delSelects) obj->SelectionDelete(delSelects);
          if (merge) obj->Merge(*merge);
          return 0;
        }
        ~CISet() {delete merge; delete delSelects;}
  };
  
  comint.Run(new CISet(merge,delSelects));
  return true;
}
bool PluginGeneralAppSide::BeginUpdate(const char *description)
{
  AFX_MANAGE_STATE(::AfxGetAppModuleState());
  if (comint.Begin(WString(description))!=0) return false;
  if (comint.Run(new CIFakeEmpty)!=0) return false;
  return true;
}
bool PluginGeneralAppSide::EndUpdate()
{
  AFX_MANAGE_STATE(::AfxGetAppModuleState());
  CObjektiv2Doc *doc=static_cast<CObjektiv2Doc *>(frame->GetActiveDocument());
  doc->UpdateAllToolbars(0);
  doc->LoadEdges();
  doc->UpdateAllViews(NULL);
  doc->UpdateStatus();
  return true;

}
const char *PluginGeneralAppSide::ReadConfigValue(int idsConfig)
{
  AFX_MANAGE_STATE(::AfxGetAppModuleState());
  return config->GetString(idsConfig);
}
ProgressBarFunctions *PluginGeneralAppSide::GetProgressBarHandler()
{
    return ProgressBarFunctions::GetGlobalProgressBarInstance();
}

IFileDlgHistory *PluginGeneralAppSide::GetFileDialogExHandler() {
    
    return CFileDialogEx::GetHistoryInterface();

}