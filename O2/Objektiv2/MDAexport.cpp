#include "stdafx.h"
#include "MDAExport.h"


static bool ComparePoints(FaceT &fc1, int pt1, FaceT &fc2, int pt2)
  {
  return fc1.GetPoint(pt1)==fc2.GetPoint(pt2) && 
    fc1.GetNormal(pt1)==fc2.GetNormal(pt2) &&
      fabs(fc1.GetU(pt1)-fc2.GetU(pt2))<0.00001 &&
        fabs(fc1.GetV(pt1)-fc2.GetV(pt2))<0.00001;
  }

//--------------------------------------------------

static int GetNxPt(FaceT &fc, int pt)
  {
  pt=pt+1;
  if (pt>=fc.N()) pt=0;
  return pt;
  }

//--------------------------------------------------

static int GetPrPt(FaceT &fc, int pt)
  {
  pt=pt-1;
  if (pt<0) pt+=fc.N();
  return pt;
  }

//--------------------------------------------------

static void OutQuads(ObjectData *obj, const char *texture, char *usemask, ostream &out)
  {
  for (int i=0;i<obj->NFaces();i++)
    {
    FaceT fc(obj,i);
    if (fc.N()==4 && usemask[i]==0 && strcmp(texture,fc.GetTexture())==0)
      {
      for (int j=0;j<fc.N();j++)
        {
        PosT &pos=obj->Point(fc.GetPoint(j));
        VecT &norm=obj->Normal(fc.GetNormal(j));
        char buff[256];
        sprintf(buff,"P %0.8f %0.8f %0.8f ~ %0.8f %0.8f %0.8f ~ %0.8f %0.8f\n",
        pos[0],pos[2],-pos[1],norm[0],norm[2],-norm[1],fc.GetU(j),fc.GetV(j));
        out<<buff;
        }
      usemask[i]=1;
      }
    }
  }

//--------------------------------------------------

static void OutTriangles(ObjectData *obj, const char *texture, char *usemask, ostream &out)
  {
  for (int i=0;i<obj->NFaces();i++)
    {
    FaceT fc(obj,i);
    if (fc.N()==3 && usemask[i]==0 && strcmp(texture,fc.GetTexture())==0)
      {
      for (int j=0;j<fc.N();j++)
        {
        PosT &pos=obj->Point(fc.GetPoint(j));
        VecT &norm=obj->Normal(fc.GetNormal(j));
        char buff[256];
        sprintf(buff,"P %0.8f %0.8f %0.8f ~ %0.8f %0.8f %0.8f ~ %0.8f %0.8f\n",
        pos[0],pos[2],-pos[1],norm[0],norm[2],-norm[1],fc.GetU(j),fc.GetV(j));
        out<<buff;
        }
      usemask[i]=1;
      }
    }
  }

//--------------------------------------------------

bool SaveMDA(ObjectData *obj, const char *filename)
  {
  ofstream out(filename,ios::out|ios::trunc);
  if (!out) return false;
  char *usemask=new char[obj->NFaces()];
  memset(usemask,0,obj->NFaces());
  int i;
  do
    {
    for (i=0;i<obj->NFaces();i++) if (usemask[i]==0)
      {
      FaceT fc(obj,i);
      out<<"M "<<fc.GetTexture()<<"\n";
      out<<"Q\n";
      OutQuads(obj,fc.GetTexture(),usemask,out);
      out<<"T\n";
      OutTriangles(obj,fc.GetTexture(),usemask,out);
      }
    }
  while (i<obj->NFaces());
  delete [] usemask;
  return !(!out);
  }

//--------------------------------------------------

