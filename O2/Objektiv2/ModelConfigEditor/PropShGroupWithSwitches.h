#pragma once
#include "../../bredy.libs\extendedpropertysheet\propshgroup.h"

class PropShGroupWithSwitches : public PropShGroup
{
  int _ident;
  int _lastItemCount;
  void UpdateChkBoxCount();
public:
  PropShGroupWithSwitches(void);
  ~PropShGroupWithSwitches(void);

  void UpdateGroupContent(PropShHDWP &hdwp, int x, int y, int wspace, int hspace, int &width, int &height);
  virtual LRESULT OnCommand(WORD wNotifyCode,WORD wID, HWND ctl);

  void EnableItem(int index,bool enable);
  void HideCheckbox(int index,bool hide);
  bool IsItemEnabled(const PropShBase *item);


};
