// ModelConfigEditor.h : main header file for the ModelConfigEditor DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include <Es/Framework/AppFrame.hpp>
#include <El/Interfaces/iAppInfo.hpp>
#include "IFCModelConfig.h"
#include "DlgMainFrame.h"

namespace ObjektivLib {
    class LODObject;
}

using namespace ObjektivLib;

// CModelConfigEditorApp
// See ModelConfigEditor.cpp for the implementation of this class
//

#define WM_LOGMESSAGE (WM_APP+9891)  //WPARAM bool *processed, LPARAM const char *text

class Application : public CWinApp, public AppFrameFunctions, public IMCEPluginSide, public IPluginGeneralPluginSide
{ 
    HANDLE _logHandle;
    void InitLogHandles();
    IPluginGeneralAppSide *_application;
    IMCEAppSide *_service;
    SRef<DlgMainFrame> _mainFrame;
    CString _curDocument;
    CWinThread *pipethread;
    CString _textEditor;
public:
	Application();

// Overrides
public:
	virtual BOOL InitInstance();

   void LogF(const char *format, va_list argptr);
   const CString &GetCurrentDocument() {return _curDocument;}


  virtual void Init(IMCEAppSide *appSide);
  virtual bool OpenModelConfig(const char *p3dname);
  virtual int GetListOfSkeletons(char **array);
  virtual bool AddBones(const char **boneList, int count);
  const char *GetOpenedConfig();
  virtual bool OnNewDocument();

  void SetNewEditor(const _TCHAR *editor);
  const _TCHAR *GetTxtEditor() {return _textEditor;}


   virtual void Init(IPluginGeneralAppSide *appInterface);
   virtual void *GetService(ServiceName name);
   virtual bool BeforeClose();
   virtual void AfterClose();
   virtual void AfterOpen(HWND O2Window);
   virtual void AfterLoadDocument(const char *filename);
   virtual void AfterSaveDocument(const char *filename);
   virtual void AfterNewDocument();
   virtual int GetAllDependencies(const char *p3dname, const LODObject *object, char **array);
   virtual BOOL OnPretranslateMessage(MSG *msg);
   virtual void BeforePluginUnload();
   virtual void AfterViewerUpdated();
   virtual void InitPopupMenu(PopupMenuName name, HMENU mnu,HWND clientWnd);
   IPluginGeneralAppSide *App() {return _application;}
   IMCEAppSide *AppSrv() {return _service;}
   
   void ChangeDocument(const _TCHAR *docName);
   void LoadDocumentToWindow(const _TCHAR *docName);

   void AddSelectionsFromO2(HWND o2wnd);

   HWND GetMainFrame()
   {
     return _mainFrame->GetSafeHwnd();
   }

    virtual int ExitInstance();    
	DECLARE_MESSAGE_MAP()
};

extern Application theApp;

class CommandOpenModelConfig: public IPluginCommand, public RefCount
{
  Application *a;
public:
  CommandOpenModelConfig(Application *a):a(a) {}
  virtual void AddRef() {RefCount::AddRef();}
  virtual void Release() {RefCount::Release();}
  virtual void OnCommand() {a->OpenModelConfig(0);}  
};

class CommandAddSelections: public IPluginCommand, public RefCount
{
  Application *a;
  HWND o2wnd;
public:
  CommandAddSelections(Application *a,HWND o2wnd):a(a),o2wnd(o2wnd) {}
  virtual void AddRef() {RefCount::AddRef();}
  virtual void Release() {RefCount::Release();}
  virtual void OnCommand() {a->AddSelectionsFromO2(o2wnd);}  
  virtual void Update(IPluginMenuCommandUpdate *u) {u->Enable(a->GetCurrentDocument().GetLength()!=0);}
};