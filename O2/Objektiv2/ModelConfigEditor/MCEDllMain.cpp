#include "stdafx.h"

extern "C"
{
BOOL WINAPI _DllMainCRTStartup(HANDLE  hDllHandle, DWORD   dwReason, LPVOID  lpreserved);
}

HINSTANCE hDllInstance;

BOOL WINAPI MCEDllMain(HANDLE  hDllHandle, DWORD   dwReason, LPVOID  lpreserved)
{
  hDllInstance=(HINSTANCE)hDllHandle;
  if (dwReason==DLL_PROCESS_DETACH || dwReason==DLL_PROCESS_ATTACH) return TRUE;
  else return _DllMainCRTStartup(hDllHandle, dwReason, lpreserved);
}

BOOL ProcessDllInit()
{
  return _DllMainCRTStartup(hDllInstance,DLL_PROCESS_ATTACH,0);
}

BOOL ProcessDllShutdown()
{
  return _DllMainCRTStartup(hDllInstance,DLL_PROCESS_DETACH,0);
}

#include "ModelConfigEditor.h"
extern Application theApp;


void *operator new(size_t sz)
{
  return theApp.App()->O2Allocator(sz);
}

void operator delete(void *ptr)
{
  return theApp.App()->O2Deallocator(ptr);
}

void *operator new[](size_t sz)
{
  return theApp.App()->O2Allocator(sz);
}

void operator delete[](void *ptr)
{
  return theApp.App()->O2Deallocator(ptr);
}

