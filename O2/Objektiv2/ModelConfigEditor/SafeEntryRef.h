#pragma once
#include <es/Strings/rString.hpp>
#include <el/ParamFile/ParamFile.hpp>

class SafeEntryRef
{
  RString _path;
public:
  SafeEntryRef(void);
  ~SafeEntryRef(void);


  void SetRef(ParamEntryPtr entry);
  ParamEntryPtr GetRef(const ParamFile &relativeTo) const;

  SafeEntryRef &operator=(const ParamEntryPtr entry) {SetRef(entry);return *this;}
  SafeEntryRef(const ParamEntryPtr entry) {SetRef(entry);}
  SafeEntryRef(ParamClass *centry) {SetRef(ParamEntryPtr(centry,centry));}
  SafeEntryRef &operator=(ParamClass *centry) {SetRef(centry?ParamEntryPtr(centry,centry):0);return *this;}

  ParamEntryPtr operator()(const ParamFile &relativeTo) const {return GetRef(relativeTo);}

  void EntryRenamed(const RString &newname);  

  bool IsNull() {return _path[0]==0;}
  bool NotNull() {return _path[0]!=0;}

  bool operator==(const SafeEntryRef& other) {return _path==other._path;}
  bool operator!=(const SafeEntryRef& other) {return _path!=other._path;}

  RString GetPath()const {return _path;}
  void SetPath(RString path) {_path=path;}

  bool ImportEntry(ParamFile &relativeTo) const;
  static void CleanUpAfterImport(ParamClassPtr root);
};


