#pragma once
#include "afxwin.h"
#include "afxcmn.h"

// TimeLineControl dialog

class TimeLineControlEvent
{
public:
  virtual void SetPosition(float pos)=0;
  virtual void SetPingpong(bool enabled)=0;
  virtual void SetAutoAnim(bool enabled)=0;
  virtual void ChangeSpeed(float factor)=0;
};

class TimeLineControl : public CDialog
{
	DECLARE_DYNAMIC(TimeLineControl)
  CSize lastSz;
  TimeLineControlEvent *_event;
  bool _nolimit;
  float _min;
  float _max;  

  float _curTime;
public:
	TimeLineControl(TimeLineControlEvent *event, CWnd* pParent = NULL);   // standard constructor
	virtual ~TimeLineControl();

// Dialog Data
	enum { IDD = IDD_TIMELINE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support


  
  DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnSize(UINT nType, int cx, int cy);
  CButton wPingPingMode;
  CButton wPlayMode;
  CSliderCtrl wSliderPos;
  virtual BOOL OnInitDialog();
  afx_msg void OnBnClickedPingpongmode();
  afx_msg void OnBnClickedPlaybutton();
  afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

  void SetMinMax(float min, float max);
  void SetPosition(float pos);
  void SetMarkRange(float min, float max);
  void Enable(bool enable);
  void SetButtonAnimate(bool animate);
  void SetButtonPingPong(bool animate);

  bool IsPingPongActive();
  virtual BOOL PreTranslateMessage(MSG* pMsg);  
};
