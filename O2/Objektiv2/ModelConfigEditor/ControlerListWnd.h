#pragma once


// ControlerListWnd

class ControlerListWndEvent
{
public:
  virtual void UpdateController(ControlInfoItem *cntr)=0;
  virtual void StartAnimThread()=0;
  virtual void StopAllAnimations()=0;
  virtual void ResetAllAnimations()=0;
};

class ControlerListWnd : public CWnd
{
	DECLARE_DYNAMIC(ControlerListWnd)

  int _latestY;
  ControlerListWndEvent *_event;

  class TimeLineControlEventEx;
  friend class TimeLineControlEventEx;
  
  class TimeLineControlEventEx: public TimeLineControlEvent
  {
    RString _controler;
    ControlerListWnd *outer;
  public:
    TimeLineControlEventEx(ControlerListWnd *outer, RString controler):outer(outer),_controler(controler) {}
    virtual void SetPosition(float pos) {outer->SetPosition(_controler,pos);}
    virtual void SetPingpong(bool enabled) {outer->SetPingpong(_controler,enabled);}
    virtual void SetAutoAnim(bool enabled) {outer->SetAutoAnim(_controler,enabled);}
    virtual void ChangeSpeed(float factor) {outer->ChangeSpeed(_controler,factor);}

  };

  ControlList &_cntrList;

  AutoArray<RString, MemAllocLocal<RString, 50> > _names;
  AutoArray<SRef<TimeLineControl>, MemAllocLocal<SRef<TimeLineControl>, 50>  > _controls;
  AutoArray<SRef<CStatic>, MemAllocLocal<SRef<CStatic>, 50>  > _titles;
  AutoArray<SRef<TimeLineControlEventEx>, MemAllocLocal<SRef<TimeLineControlEventEx>, 50>  > _events;

  class FillNamesControls;
  friend class FillNamesControls;
  class FillNamesControls
  {
    ControlerListWnd *outer;
    mutable int y;
  public:
    FillNamesControls(ControlerListWnd *outer):outer(outer) {}
    bool operator()(const ControlInfoItem &item) const;
  };


  void SetPosition(RString controler, float pos);
  void SetPingpong(RString controler, bool enabled);
  void SetAutoAnim(RString controler, bool enabled);
  void ChangeSpeed(RString controler, float factor);

public:
	ControlerListWnd(ControlList &cntrList,ControlerListWndEvent *event);
	virtual ~ControlerListWnd();

  BOOL Create(CWnd *owner);

  void Rebuild();
  void RecalcLayout(int cx,int cy);
  void Update();

protected:
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnDestroy();
  afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
  afx_msg void OnPaint();
  afx_msg LRESULT AfterResize(WPARAM wParam,LPARAM lParam);
  virtual BOOL PreTranslateMessage(MSG* pMsg);
  afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
};


