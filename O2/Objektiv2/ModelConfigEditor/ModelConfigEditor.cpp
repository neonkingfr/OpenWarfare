// ModelConfigEditor.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "ModelConfigEditor.h"
#include "DlgAbout.h"
#include ".\modelconfigeditor.h"

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

// CModelConfigEditorApp

BEGIN_MESSAGE_MAP(Application, CWinApp)
END_MESSAGE_MAP()


// CModelConfigEditorApp construction

Application::Application()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CModelConfigEditorApp object

Application theApp;
AppFrameFunctions *CurrentAppFrameFunctions=&theApp;

// CModelConfigEditorApp initialization

BOOL Application::InitInstance()
{
	CWinApp::InitInstance();
    CurrentAppFrameFunctions=this;    
    InitLogHandles(); 

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Bohemia Interactive Studio"));
  _textEditor=GetProfileString(_T("Editor"),_T("Pathname"),_T("C:\\Program Files\\UltraEdit\\UEDIT32.EXE"));

	return TRUE;
}

static bool ScanLogPipeWait(HANDLE readPipe, tchar *buff, int &sz)
{
  bool fake=true;
  bool *p=&fake;
  int datacnt=0;
  do
  {
    if (buff[0])
    {
      p=(bool *)alloca(1);
      *p=false;
      tchar *block=(tchar *)alloca((sz+1)*sizeof(tchar *));
      memcpy(block,buff,sz*sizeof(tchar *));
      block[sz]=0;
      HWND sendTo=theApp.GetMainFrame();
      if (sendTo)       
        PostMessage(sendTo,WM_LOGMESSAGE,(WPARAM)p,(LPARAM)block);
      else      
        *p=true;
      datacnt+=1+sz*sizeof(tchar *);
      if (datacnt>256*1024) Sleep(1000);
    }
    DWORD rd=0;        
    ReadFile(readPipe,buff,256*sizeof(*buff),&rd,0);
    if (rd==0) return false;
    sz=rd/sizeof(*buff);
  }
  while (!*p && datacnt<300*1024);
  return true;
}

static UINT ScanLogPipe(LPVOID data)
{
  HANDLE readPipe=(HANDLE)data;
  tchar buff[256]="";
  int sz;
  while (ScanLogPipeWait(readPipe,buff,sz));
  ExitThread(0);
  return 0;
}

void Application::InitLogHandles()
{
  HANDLE rdp;
  CreatePipe(&rdp,&_logHandle,0,0);
  pipethread=AfxBeginThread(ScanLogPipe,(LPVOID)rdp);
}

void Application::LogF(const char *format, va_list argptr)
{
  CString text;
  text.FormatV(CharToTCHAR(format),argptr);
  DWORD wr;
  WriteFile(_logHandle,text.GetString(),text.GetLength()*sizeof(*text.GetString()),&wr,0);
  WriteFile(_logHandle,_T("\r\n"),2*sizeof(tchar),&wr,0);
}


void Application::Init(IMCEAppSide *appSide)
{
  _service=appSide;  
}
bool Application::OpenModelConfig(const char *p3dname)
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  if (_mainFrame->CanHandleRequestNow()==false) return false;
  if (_mainFrame->GetSafeHwnd()==0) 
  {
    _mainFrame=new DlgMainFrame;
    _mainFrame->Create(_mainFrame->IDD,CWnd::GetDesktopWindow());
  }
  _mainFrame->ShowWindow(SW_SHOW);
  _mainFrame->BringWindowToTop();
  CString docName=_curDocument;
  _curDocument=docName+_T(".");
   ChangeDocument(docName);
  return true;
}
int Application::GetListOfSkeletons(char **array)
{
  return 0;
}
bool Application::AddBones(const char **boneList, int count)
{
  return false;
}
const char *Application::GetOpenedConfig()
{
  return 0;
}

void Application::Init(IPluginGeneralAppSide *appInterface)
{
  _application=appInterface;
}
void *Application::GetService(ServiceName name)
{
  if (name==Service_ModelConfigEditor) return reinterpret_cast<void *>(static_cast<IMCEPluginSide *>(this));
  else return 0;
}
bool Application::BeforeClose()
{ 
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  if (_mainFrame->CanHandleRequestNow()==false) return false;
  return true;
}

void Application::AfterClose()
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  if (_mainFrame->GetSafeHwnd())
  {
    _mainFrame->DestroyWindow();
    _mainFrame=0;
  }
}

void Application::AfterOpen(HWND O2Window)
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
   CString about;
  about.LoadString(IDS_ABOUTMENUTITILE);
  _application->AddMenuItem(_application->mnuHelp,about,&GDlgAboutOpenCmd);
  about.LoadString(IDS_OPENMODELCFG);
  _application->AddMenuItem(_application->mnuTools,about,new CommandOpenModelConfig(this));
}
void Application::AfterLoadDocument(const char *filename)
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  ChangeDocument(filename);
}
void Application::AfterSaveDocument(const char *filename)
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  ChangeDocument(filename);
}

bool Application::OnNewDocument()
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
    if (_mainFrame->CanHandleRequestNow()==false) return false;
    return true;
}

void Application::AfterNewDocument()
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  if (_mainFrame->GetSafeHwnd())
  {
    _mainFrame->Reset();
    _mainFrame->DisableMCE(true);
  }
  _curDocument="";
}
int Application::GetAllDependencies(const char *p3dname, const LODObject *object, char **array)
{
  if (p3dname==0 || p3dname[0]==0) return 0;
  Pathname pth=p3dname;
  Pathname fl="model.cfg";
  int needlen=_tcslen(p3dname)+1;
  TCHAR *c=(TCHAR *)alloca(sizeof(TCHAR)*needlen);
  int i;
  int count=0;
  int sz=0;
  for ( i=0;pth.GetPart(i,c,needlen,-1);i++,count++) sz+=_tcslen(c)+_tcslen(fl.GetFilename())+2;
  sz+=_tcslen(p3dname)+1;
  count++;
  if (array==0) return sz+sizeof(char *)*count;
  char *store=reinterpret_cast<char *>(array+count);
  for (i=0;pth.GetPart(i,c,needlen,-1);i++,count++) 
  {
    fl.SetDirectory(c);
    array[i]=store;
    _tcscpy(store,fl);
    store=_tcschr(store,0)+1;
  }
  fl=p3dname;
  fl.SetExtension(".cfg");
  array[i]=store;
  i++;
  _tcscpy(store,fl);
  return i;
}
BOOL Application::OnPretranslateMessage(MSG *msg)
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  return _mainFrame->GetSafeHwnd()==0?FALSE:_mainFrame->PreTranslateMessage(msg);
}

extern "C" __declspec(dllexport) IPluginGeneralPluginSide * __cdecl  ObjektivPluginMain(IPluginGeneralAppSide *appside)
{
  theApp.Init(appside);
  if (ProcessDllInit()==0) return 0;
  return &theApp;
}

int Application::ExitInstance()
{
  pipethread->m_bAutoDelete=false;
  CloseHandle(_logHandle);
  WaitForSingleObject(*pipethread,INFINITE);
  delete pipethread;

  return __super::ExitInstance();
}

void Application::BeforePluginUnload()
{
  _mainFrame=0;
  ProcessDllShutdown();
}

void Application::ChangeDocument(const _TCHAR *docName)
{  
  if (_mainFrame->GetSafeHwnd())
  {
    if (docName!=0 && docName[0]!=0 && _stricmp(_curDocument,docName)!=0)
    {
      LoadDocumentToWindow(docName);
      Pathname pth=docName;
      _mainFrame->SwitchToModel(CString(pth.GetTitle()));
    }
    else
      _mainFrame->DisableMCE(true);
    }
  _curDocument=docName;
}


void Application::LoadDocumentToWindow(const _TCHAR *docName)
{
  Pathname path=docName;
  path.SetFilename("model.cfg");
 if ( _mainFrame->LoadModelConfig(CString(path.GetFullPath()))==false)
 {
    _mainFrame->DisableMCE(true);;    
 }
 else
 {
   _mainFrame->DisableMCE(false);
 }
}

void Application::InitPopupMenu(PopupMenuName name, HMENU mnu,HWND clientWnd)
{
  if (name==PMSelectionList)
  {
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  CommandAddSelections *command=new CommandAddSelections(this,clientWnd);
  CString title;
  title.LoadString(IDS_ADDSELTITLE);
  _application->AddPopupMenuItem(mnu,title,command);
  }
}

void Application::AddSelectionsFromO2(HWND o2wnd)
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  if (_mainFrame->CanHandleRequestNow()==false) return;
  CDialog dlg;
  dlg.Attach(o2wnd);
  CListCtrl list;
  list.Attach(*dlg.GetNextWindow(GW_CHILD));

  POSITION pos=list.GetFirstSelectedItemPosition();
  AutoArray<RString> selList;
  while (pos)
  {
    int index=list.GetNextSelectedItem(pos);
    CString name=list.GetItemText(index,0);
    selList.Append()=RString(TCHARToChar(name));
  }

  list.Detach();
  dlg.Detach();

  if (_mainFrame->GetSafeHwnd()==0) OpenModelConfig(_curDocument);
  _mainFrame->AddBones(selList);
}



void Application::SetNewEditor(const _TCHAR *editor)
{
  _textEditor=editor;
  WriteProfileString(_T("Editor"),_T("Pathname"),editor);
}

void Application::AfterViewerUpdated()
{
  if (_mainFrame->GetSafeHwnd()) _mainFrame->AfterViewerUpdated();
}
