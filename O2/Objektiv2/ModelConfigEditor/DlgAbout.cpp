// DlgAbout.cpp : implementation file
//

#include "stdafx.h"
#include "ModelConfigEditor.h"
#include "DlgAbout.h"
#include "IFCModelConfig.h"


// DlgAbout dialog

IMPLEMENT_DYNAMIC(DlgAbout, CDialog)
DlgAbout::DlgAbout(CWnd* pParent /*=NULL*/)
	: CDialog(DlgAbout::IDD, pParent)
{
}

DlgAbout::~DlgAbout()
{
}

void DlgAbout::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(DlgAbout, CDialog)
END_MESSAGE_MAP()


// DlgAbout message handlers

void DlgAboutOpenCmd::OnCommand()
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  DlgAbout dlg;
  dlg.DoModal();
}

DlgAboutOpenCmd GDlgAboutOpenCmd;