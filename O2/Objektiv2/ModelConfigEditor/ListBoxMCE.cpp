// ListBoxMCE.cpp : implementation file
//

#include "stdafx.h"
#include "ModelConfigEditor.h"
#include "ListBoxMCE.h"
#include ".\listboxmce.h"


// CListBoxMCE

IMPLEMENT_DYNAMIC(CListBoxMCE, CListBox)
CListBoxMCE::CListBoxMCE():_nfo(0)
{
  _palette[0]=GetSysColor(COLOR_WINDOWTEXT);
  _palette[1]=GetSysColor(COLOR_HOTLIGHT);
}

CListBoxMCE::~CListBoxMCE()
{
}


BEGIN_MESSAGE_MAP(CListBoxMCE, CListBox)
  ON_WM_DRAWITEM_REFLECT()
  ON_WM_MEASUREITEM_REFLECT()
  ON_WM_LBUTTONDOWN()
//  ON_WM_CREATE()
  ON_WM_DESTROY()
  ON_WM_KEYDOWN()
END_MESSAGE_MAP()



void CListBoxMCE::SetContentInfo(AutoArray<ContentInfo> *nfo)
{
  _nfo=nfo;
}
// CListBoxMCE message handlers


void CListBoxMCE::Update()
{
  unsigned long curlines=0;
  _mapindexes.Clear();
  for (int i=_nfo->Size()-1;i>=0;i--)
  {
    ContentInfo &cnfo=_nfo->operator [](i);
    curlines|=(1<<cnfo.ident);
    curlines&=~(0xFFFFFFFF<<(cnfo.ident+1));
    cnfo.lines=curlines;
  }
  int skipident=-1;
  for (int i=0;i<_nfo->Size();i++)
  {
    if (skipident>=0 && _nfo->operator [](i).ident<=skipident)
    {
      skipident=-1;
    }
    if (skipident<0)
    {
      _mapindexes.Append(i);      
      if (!_nfo->operator [](i).opened) skipident=_nfo->operator [](i).ident;
    }
  }
  int top=GetTopIndex();
  SendMessage(LB_SETCOUNT,_mapindexes.Size(),0);
  SetTopIndex(top);
  int sel= GetAnchorIndex();
  if (sel>=0) SetSel(sel);
  Invalidate();
}

void CListBoxMCE::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
  const unsigned int &item=lpDrawItemStruct->itemID;
  const RECT &rcitem=lpDrawItemStruct->rcItem;
  CDC dc;dc.Attach(lpDrawItemStruct->hDC);
  const unsigned int &flags=lpDrawItemStruct->itemState;
  COLORREF fore;
  COLORREF back;

  if (_nfo==0 || item>(unsigned)_mapindexes.Size()) return;
  int mapitem=_mapindexes[item];
  if (mapitem>_nfo->Size()) return;
  if (flags & ODS_SELECTED)
  {
    fore=GetSysColor(COLOR_HIGHLIGHTTEXT);
    back=GetSysColor(COLOR_HIGHLIGHT);
  }
  else
  {
    fore=_palette[_nfo->operator [](mapitem).palcolor];
    back=GetSysColor(COLOR_WINDOW);
  }

  CFont *oldfont=0;
  if (_nfo->operator [](mapitem).bold) 
    oldfont=dc.SelectObject(&_bold);
  dc.SetBkColor(back);
  dc.SetTextColor(fore);
  dc.FillSolidRect(&rcitem,back);
  CRect moditem=rcitem;
  moditem.left=GetItemIdent(mapitem,&rcitem)+20;
  dc.DrawTextA(_nfo->operator [](mapitem).title,moditem,DT_SINGLELINE|DT_VCENTER|DT_LEFT|DT_NOPREFIX|DT_END_ELLIPSIS);
  bool hasChild=mapitem+1<_nfo->Size() && _nfo->operator [] (mapitem+1).ident>_nfo->operator [] (mapitem).ident;
  unsigned long lines=_nfo->operator[] (mapitem).lines;
  CPen pen(PS_SOLID,1,RGB(128,128,128));
  CPen *oldpen=dc.SelectObject(&pen);
  for (int i=0;i<32;i++) if (lines & (1<<i))
  {
    if (i==_nfo->operator[] (mapitem).ident)
    {
    dc.MoveTo(i*16+8,(rcitem.bottom+rcitem.top)>>1);
    dc.LineTo(i*16+16,(rcitem.bottom+rcitem.top)>>1);
    if (mapitem+1>=_nfo->Size() || (_nfo->operator[] (mapitem+1).lines & _nfo->operator[] (mapitem).lines & (1<<i))==0)
      {
      dc.MoveTo(i*16+8,rcitem.top);
      dc.LineTo(i*16+8,(rcitem.bottom+rcitem.top)>>1);
      }
    else
      {
      dc.MoveTo(i*16+8,rcitem.top);
      dc.LineTo(i*16+8,rcitem.bottom);
      }
    }
    else
    {
      dc.MoveTo(i*16+8,rcitem.top);
      dc.LineTo(i*16+8,rcitem.bottom);
    }

  }
  if (_imgList.NotNull())
  {
    int sysicon=hasChild?(_nfo->operator[] (mapitem).opened?2:1):0;
    int y=((rcitem.bottom+rcitem.top)>>1)-8;
    _imgList->Draw(&dc,sysicon,CPoint(moditem.left-36,y),ILD_NORMAL);
    _imgList->Draw(&dc,_nfo->operator[] (mapitem).icon,CPoint(moditem.left-20,y),ILD_NORMAL);
  }
  dc.SelectObject(oldpen);
  if (oldfont) dc.SelectObject(oldfont);
  dc.Detach();
}

void CListBoxMCE::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
  CDC *dc=GetDC();
  CSize sz;
  dc->GetTextExtent("W",1);
  lpMeasureItemStruct->itemHeight=__max(sz.cx,16);
  lpMeasureItemStruct->itemWidth=__max(sz.cy,16);
  ReleaseDC(dc);
}

int CListBoxMCE::GetItemIdent(int index, const RECT *rcitem)
{
  RECT rc;
  if (rcitem==0)
  {
    int idx=GetItemIndex(index);    
    if (idx<0) return idx;
    GetItemRect(index,&rc);
    index=idx;
    rcitem=&rc;
  }
  return rcitem->left+(rcitem->bottom-rcitem->top)*_nfo->operator [](index).ident+16;  
}

void CListBoxMCE::OnLButtonDown(UINT nFlags, CPoint point)
{
  BOOL out;
  UINT item=ItemFromPoint(point,out);    
  if (!out) 
  {
    int ident=GetItemIdent(item);
    if (point.x<ident && point.x>ident-16)
    {
      OpenCloseItem(item);      
    }
  }
  CListBox::OnLButtonDown(nFlags, point);
}

int CListBoxMCE::GetItemIndex(UINT item)
{
  if (_nfo==0 || item>(unsigned)_mapindexes.Size()) return -1;
  int mapitem=_mapindexes[item];
  if (mapitem>_nfo->Size()) return -1;
  return mapitem;
}

void CListBoxMCE::OpenCloseItem(UINT item)
{
  int mapitem=GetItemIndex(item);
  if (mapitem>=0)
  {
    _nfo->operator [](mapitem).opened=!_nfo->operator [](mapitem).opened;
    Update();
  }
}

//int CListBoxMCE::OnCreate(LPCREATESTRUCT lpCreateStruct)
//{
//  if (CListBox::OnCreate(lpCreateStruct) == -1)
//    return -1;
//
//  return 0;
//}

void CListBoxMCE::OnDestroy()
{
  CListBox::OnDestroy();

  _bold.DeleteObject();
}

void CListBoxMCE::PreSubclassWindow()
{
  CDC *dc=GetDC();
  CFont *fnt=dc->GetCurrentFont();
  LOGFONT lg;fnt->GetLogFont(&lg);
  lg.lfWeight=FW_BOLD;
  _bold.CreateFontIndirect(&lg);
  ReleaseDC(dc);
 
  CListBox::PreSubclassWindow();
}


bool CListBoxMCE::SetActiveItem(int item)
{
  if (item<0 || item>=_nfo->Size()) return false;
  int p=item;
  int ident=_nfo->operator [](p).ident;
  while (p--)
  {
    if (_nfo->operator[](p).ident<ident)
    {
      _nfo->operator[](p).opened=true;
      ident=_nfo->operator[](p).ident;
        if (ident==0) break;
    }
  }
  Update();
  for (int i=0,cnt=_mapindexes.Size();i<cnt;++i)  
  {
    if (_mapindexes[i]==item) 
    {
      SetAnchorIndex(i);
      SetCurSel(i);
    }
    SetSel(i,_mapindexes[i]==item) ;
  }
  
  return true;
}
void CListBoxMCE::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
  if (nChar==VK_LEFT || nChar==VK_RIGHT || nChar==VK_SPACE)
  {
    OpenCloseItem(this->GetAnchorIndex());
    return;
  }


  CListBox::OnKeyDown(nChar, nRepCnt, nFlags);
}
