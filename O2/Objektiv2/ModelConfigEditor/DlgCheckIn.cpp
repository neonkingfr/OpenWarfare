// DlgCheckIn.cpp : implementation file
//

#include "stdafx.h"
#include "ModelConfigEditor.h"
#include "DlgCheckIn.h"


// DlgCheckIn dialog

IMPLEMENT_DYNAMIC(DlgCheckIn, CDialog)
DlgCheckIn::DlgCheckIn(CWnd* pParent /*=NULL*/)
	: CDialog(DlgCheckIn::IDD, pParent)
    , vComments(_T(""))
{
}

DlgCheckIn::~DlgCheckIn()
{
}

void DlgCheckIn::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_COMMENTS, vComments);
}


BEGIN_MESSAGE_MAP(DlgCheckIn, CDialog)
  ON_BN_CLICKED(IDC_UNDOCHECKIN, OnBnClickedUndocheckin)
END_MESSAGE_MAP()


// DlgCheckIn message handlers

void DlgCheckIn::OnBnClickedUndocheckin()
{
  EndDialog(IDNO);
}
