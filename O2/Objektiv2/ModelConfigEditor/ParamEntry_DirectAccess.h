#pragma once

class ParamClass_DirectAccess: public ParamClass
{
public:
  bool SetName(const RStringB &name)
  {
    ParamEntryPtr entry=FindParent()->FindEntryNoInheritance(name);
    if (entry.NotNull() && entry.GetPointer()!=this) return false;
    __super::SetName(name);
    return true;
  }
};
