#pragma once


// DlgAbout dialog

class DlgAbout : public CDialog
{
	DECLARE_DYNAMIC(DlgAbout)

public:
	DlgAbout(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgAbout();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};


class DlgAboutOpenCmd: public IPluginCommand
{
public:
  virtual void OnCommand();
  virtual void AddRef() {}
  virtual void Release() {}
  virtual void Update(const IPluginMenuCommandUpdate *update) {}
};

extern DlgAboutOpenCmd GDlgAboutOpenCmd;