#if !defined(AFX_MESSAGEWND_H__1474D23E_CB9C_492F_B03E_C98C880078D8__INCLUDED_)
#define AFX_MESSAGEWND_H__1474D23E_CB9C_492F_B03E_C98C880078D8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MessageWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMessageWnd dialog

class CMessageWnd : public CDialog
  {
  // Construction   
  DWORD curtime;
  public:
    void ShowText(const char *text);
    static void ShowMessage(const char *text, DWORD timetoshow=5000);
    virtual void OnCancel();
    DWORD timetoshow;
    CSize orgsize;
    CMessageWnd(CWnd* pParent = NULL);   // standard constructor	
    
    // Dialog Data
    //{{AFX_DATA(CMessageWnd)
    enum 
      { IDD = IDD_MESSAGEWND };
    CString	text;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CMessageWnd)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CMessageWnd)
    virtual BOOL OnInitDialog();
    afx_msg void OnTimer(UINT nIDEvent);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MESSAGEWND_H__1474D23E_CB9C_492F_B03E_C98C880078D8__INCLUDED_)
