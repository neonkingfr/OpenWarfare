#pragma once
#include "comint.h"

class CIAddUVSet: public CICommand
{    
  int _id;
private:
  CIAddUVSet(const CIAddUVSet &other);
  CIAddUVSet &operator=(const CIAddUVSet &other);
public:
  CIAddUVSet(int id=-1);

  virtual int Execute(LODObject *obj);
};

class CIDeleteUVSet: public CICommand
{
  int _id;

private:
  CIDeleteUVSet(const CIDeleteUVSet &other);
  CIDeleteUVSet &operator=(const CIDeleteUVSet &other);
public:
  CIDeleteUVSet(int id);

  virtual int Execute(LODObject *obj);
};

class CISetActiveUVSet: public CICommand
{
  int _id;
private:
  CISetActiveUVSet(const CISetActiveUVSet &other);
  CISetActiveUVSet &operator=(const CISetActiveUVSet &other);
public:
  CISetActiveUVSet(int id);

  virtual int Execute(LODObject *obj);
  virtual int Tag() const   {return CITAG_SETACTIVEUVSET;}
  virtual bool CanRemove(const CICommand *prev)
  {
    int tg=prev->Tag();
    return tg==CITAG_SETACTIVEUVSET;
  }

};

class CIChangeUVSetID: public CICommand
{
  int _id;
private:
  CIChangeUVSetID(const CIChangeUVSetID &other);
  CIChangeUVSetID &operator=(const CIChangeUVSetID &other);

protected:
  //TODO: protected member functions
public:
  CIChangeUVSetID(int id);

  virtual int Execute(LODObject *obj);
};