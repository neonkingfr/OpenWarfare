#pragma once
#include "afxcmn.h"

class CAnimPropsToolbar : public CDialogBar
{	
  ObjectData *_objData;
  bool _metadataChanged;
  bool _lockUpdate;

  CListCtrl	m_KeyList;
  CSliderCtrl m_timeSlider;
  CButton m_AddKeyBut;
  CButton m_DeleteKeyBut;
  CEdit m_AnimLenEdit;
  CEdit m_WalkCyclesEdit;
  CEdit m_KeyTimeEdit;
  CEdit m_KeyNameEdit;
  CEdit m_KeyValueEdit;

public:
	CAnimPropsToolbar(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAnimPropsToolbar();

  // Dialog Data
  enum { IDD = IDD_ANIMATIONSPROPERTIES };

  CDIALOGBAR_RESIZEABLE

  BOOL Create(CWnd * parent, int menuid);

  void UpdateData(ObjectData *odata, float curlod);
  void SetTimeFromAnimation();

  virtual BOOL PreTranslateMessage(MSG* pMsg);
  // If this function is not overridden, all buttons are permanently disabled!!!
  virtual void OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);

protected:
	DECLARE_MESSAGE_MAP()

protected:
   afx_msg void OnDestroy();
   afx_msg void OnSize(UINT nType, int cx, int cy);  

   afx_msg void OnAddKey();
   afx_msg void OnDeleteKey();
   afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
   afx_msg void OnMetadataChanged();
   afx_msg void OnEditLostFocus();
   afx_msg void OnKeyListChanged(NMHDR* pNMHDR, LRESULT* pResult);

   void UpdateMetadata();
   void UpdateKeyList();
   void UpdateTime();
   void SetTime(float time);
   int GetActiveItem();
   void SetAnimationTime();
};
