// IDeletePointsTriangulate.cpp: implementation of the CIDeletePointsTriangulate class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "objektiv2.h"
#include "IDeletePointsTriangulate.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CIDeletePointsTriangulate::CIDeletePointsTriangulate()
{

}

CIDeletePointsTriangulate::~CIDeletePointsTriangulate()
{

}

struct SPointTwinInfo
  {
  int face;
  int ptcnt;
  int pts[3];
  int pidx[3];
  };

static SPointTwinInfo *NajdiNejblizsiOkruhBodu(int pt,ObjectData *obj, int &count)
  {
  int curalloc=32;
  SPointTwinInfo *nfo=new SPointTwinInfo[curalloc];
  int curcount=0;  
  int i;  
  for (i=0;i<obj->NFaces();i++)
    {
    FaceT fc(obj,i);
    int j;
    for (j=0;j<fc.N();j++) if (fc.GetPoint(j)==pt) break;
    if (j!=fc.N()) 
      {
      if (curcount+1==curalloc)
        {
        SPointTwinInfo *tmp=new SPointTwinInfo[curalloc+=32];
        memcpy(tmp,nfo,sizeof(SPointTwinInfo)*(curcount+1));
        delete [] nfo;
        nfo=tmp;
        }
      nfo[curcount].face=i;
      nfo[curcount].ptcnt=fc.N()-1;      
      for (int k=0;k<fc.N()-1;k++)
        {
        j++;
        if (j==fc.N()) j=0;
        nfo[curcount].pts[k]=fc.GetPoint(j);
        nfo[curcount].pidx[k]=j;
        }
      curcount++;
      }
    }
  bool opak=true;
  int cm1=curcount-1;
  int cycleout=curcount*2;
  while (opak && cycleout--)
    {
    opak=false;
    for (i=0;i<cm1;i++)
      {      
      if (nfo[i].pts[nfo[i].ptcnt-1]!=nfo[i+1].pts[0])
        {
        int j;
        for (j=0;j<curcount;j++) if (i!=j)                
          if (nfo[i].pts[nfo[i].ptcnt-1]==nfo[j].pts[0]) break;
        if (i!=j && j!=i+1 && j!=curcount && i+1<curcount) 
          {
          SPointTwinInfo tmp=nfo[i+1];
          nfo[i+1]=nfo[j];
          nfo[j]=tmp;
          opak=true;
          }
        }
      }
    }
  if (cycleout<0) 
    {delete [] nfo; return NULL;}
  for (i=0;i<curcount;i++)
    {
    int j=i+1;
      if (nfo[i].pts[nfo[i].ptcnt-1]==nfo[j].pts[0]) nfo[i].ptcnt--;
    }
  if (curcount>1 && nfo[curcount-1].pts[nfo[curcount-1].ptcnt-1]==nfo[0].pts[0]) nfo[curcount-1].ptcnt--;
  count=curcount;
  return nfo;
  }

static bool GetNextPoint(SPointTwinInfo *nfo, int max, int &fp, int &pp)
  {
  bool rep=false;
  do
    {
    pp++;
    if (pp>=nfo[fp].ptcnt) 
      {
      fp++;
      pp=0;
      if (fp>=max) fp=0;
      }
    if (pp==0 && fp==0)
      {
      if (rep) return false;
      rep=true;
      }
    }
  while (nfo[fp].pts[pp]==-1);
  return true;
  }


int CIDeletePointsTriangulate::Execute(LODObject *lodobj)
  {
  ObjectData *obj=lodobj->Active();
  int total=obj->GetSelection()->NPoints();
  int curr=0;
  ProgressBar<int> pb(obj->NPoints());
  for (int i=0;pb.AdvanceNext(1),i<obj->NPoints();i++) if (obj->PointSelected(i))
    {
    curr++;
    int nfocnt;
    int fp; //current neuborgh face    
    int pp; //current point at face
    float minangle=1.0f;
    SPointTwinInfo *nfo=::NajdiNejblizsiOkruhBodu(i,obj,nfocnt);
    if (nfo==NULL) goto leave;
    fp=nfocnt-1;
    pp=999;
    if (nfocnt && GetNextPoint(nfo,nfocnt,fp,pp))
    while (true)
      {
      int face=nfo[fp].face;
      FaceT nwfc(obj),curfc(obj,face);
      Vector3 n1=curfc.CalculateRawNormal();
      Vector3 n2;
      nwfc.SetN(3);
      nwfc.SetMaterial(curfc.GetMaterial());
      nwfc.SetTexture(curfc.GetTexture());
      nwfc.SetFlags(curfc.GetFlags());
      int savefp=fp;
      int savepp=pp;
      int *reset=NULL;
      for (int i=0;i<3;i++)
        {
        if (i)
          {
          if (GetNextPoint(nfo,nfocnt,fp,pp)==false) goto leave;
          if (!reset) reset=&nfo[fp].pts[pp];
          if (fp==savefp && pp==savepp) goto leave;
          }
        FaceT pfc(obj,nfo[fp].face);
        nwfc.SetPoint(i,nfo[fp].pts[pp]);
        nwfc.SetUV(i,pfc.GetU(nfo[fp].pidx[pp]),pfc.GetV(nfo[fp].pidx[pp]));
        nwfc.SetNormal(i,pfc.GetNormal(nfo[fp].pidx[pp]));
        }
      n2 =  nwfc.CalculateRawNormal();
      float angle=n1.CosAngle(n2);
      if (angle>=minangle)
        {
        nwfc.CopyFaceTo(obj->NewFace());
        if (reset) *reset=-1;
        }
      else
        {
        if (fp<savefp || fp==savefp && pp<=savepp) minangle-=0.2f;
        if (minangle<-1.0) 
          goto leave;
        fp=savefp;
        pp=savepp;
		if (GetNextPoint(nfo,nfocnt,fp,pp)==false) break;
        }
      }
leave:;
    obj->DeletePoint(i);
    i--;
    delete [] nfo;
    }
  return 0;
  }