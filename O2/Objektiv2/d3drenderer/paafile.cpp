//
// Simple class for loading raw PAA texture data
//

#include "paafile.h"

typedef unsigned short USHORT;
typedef unsigned char BYTE;

#define paadbg printf

int paaFile::fgeti(QIStream &f, int len) {
	int d = 0;
	f.read(&d, len);
	return d;
}


paaFile::paaFile() {
	data = NULL;
	size = 0;
	loadedLevel = -1;
}
 
paaFile::~paaFile() {
	delete[] data;
}

bool paaFile::load(char *name, int level) {	
	QIFStream f;
	f.open(name);

	delete[] data;
	loadedLevel = -1;
	data = NULL;
	size = 0;

	DWORD smul = 1;	// Size multiplier (image data size is width*height*smul bytes)
	USHORT type = fgeti(f, 2);
	switch(type) {
		case PAA_88:
		case PAA_1555:
		case PAA_4444:
			smul = 2;
			break;
		case PAA_DXT1:
		case PAA_DXT5:
			smul = 0;	// No compression - raw image data
			break;
		default:
			paadbg("Unknown format 0x%4X\n", type);
			// Unspported format or invalid file
			return false;
	}
	fmt = (paaFormat) type;

	// Skip over tag data - not needed
	while(fgeti(f, 4) == 'TAGG') {
		DWORD tag = fgeti(f, 4);
		DWORD len = fgeti(f, 4);
		seekf(f, len);	
	}
	seekf(f, -4);	// Rewind by 'tagg'	
	seekf(f, 3*fgeti(f, 2)); // Seek over palette data

	int l=0;
	while(true) {
		// LOD level data
		// Width + height, then compressed size
		w = fgeti(f, 2);
		h = fgeti(f, 2);
		DWORD cSize = fgeti(f, 3);
		DWORD fSize = smul?w*h*smul:cSize;

		// LZO compression. TODO: Is this actually used somewhere?
		bool lzo = (w & 0x8000) != 0;
		if(lzo)	break;

		//paadbg("Mipmap %d: %dx%d (%d->%dbytes)\n", l, w, h, cSize, fSize);
		if(w == 0 || h == 0) 
			break;
		if(l == level) {
			data = new BYTE[fSize];
			size = fSize;
			if(smul)	// Compressed data?
				DecodeSS((char*)data, fSize, f);
			else
				f.read(data, fSize);
			loadedLevel = l;
			break;
		} else
			seekf(f, cSize);
		l++;
	}
	f.close();
	return (data>0);
}