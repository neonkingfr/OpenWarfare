//
//	D3D texture manager class - manages d3d texture loading
//

#include "stdafx.h"

#include <d3d9.h>
#include <d3dx9.h>
#include <dxerr9.h>
#pragma comment(lib, "d3d9")
#pragma comment(lib, "d3dx9")
#pragma comment(lib, "dxerr9")


#include <vector>
using namespace std;

#define d3dmdbg RptF

typedef struct {
	char name[256];		// Path this texture was loaded from
	LPDIRECT3DTEXTURE9 tex;
	bool unused;
	__int64 mdate;	// Last modifictaion date
	char fullPath[512];	// Full texture path (with texPath prefix if not abslute)	
} D3DMTEX;

class d3dtexmanager {
public:
	d3dtexmanager(LPDIRECT3DDEVICE9 dev);
	~d3dtexmanager();
	DWORD addTexture(char *name);
	LPDIRECT3DTEXTURE9 getTexture(DWORD id);
	void flushAll();
	void prepareFlush();	// Marks all loaded textures as unused
	void flushUnused();		// Releases all textures still marked as unused
	void setTexturePath(char *);
	bool isDefaultTexture(DWORD id) {return (id==defaultTexture);};

private:
	bool isPaa(char *s);		// Returns true if file extension is ".paa"
	void getFullPath(char *path, char *fullpath);
	__int64 getFileDate(char *path);
	bool loadTexture(D3DMTEX *tex);

	vector<D3DMTEX>	textures;
	LPDIRECT3DDEVICE9 d3dd;

	bool valid;
	char texPath[256];
	const static DWORD defaultTexture = 0;
	const static bool generateMipmaps = false;	// Should auto-gen mipmaps when loading textures?
};