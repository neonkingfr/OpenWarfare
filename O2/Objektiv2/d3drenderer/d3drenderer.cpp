//
//	D3D renderer class - renders o2 model data with D3D
//

#include "d3drenderer.h"

#include <stdio.h>
#include <process.h>
using namespace d3dr;

LPDIRECT3D9 d3drenderer::d3d;		// Direct3D handle
LPDIRECT3DDEVICE9 d3drenderer::d3dd;	// Device handle
d3dtexmanager	*d3drenderer::texMgr;

const static char *PROXYPFX = "proxy:";	// Proxy selection prefix

d3drenderer::d3drenderer() {
	init();
}

d3drenderer::~d3drenderer() {
	destroy();
}

void d3drenderer::init() {
	//doc=NULL;
	d3d=NULL;
	d3dd=NULL;
	depth=NULL;
	bb=NULL;
	ZeroMemory(&baseModel, sizeof(MODELBLOCK));
	ZeroMemory(&bgModel, sizeof(MODELBLOCK));
	baseModel.isBackground = false;
	bgModel.isBackground = true;
	vrtxMarker=NULL;
	massMarker=NULL;
	vbGrid=NULL;
	texMgr=NULL;
	membb=NULL;
	vpBuf=NULL;
	vbNormal=NULL;
	vbMass=NULL;
	vbBgrnd=NULL;
	ibBgrnd=NULL;
	d3dsc=NULL;
	rsSize = 0;
	useSwapchain = false;
	ZeroMemory(&pp, sizeof(pp));
	strcpy(proxyPath, ".");
}

void d3drenderer::destroy() {
	d3drdbg("Destroy Direct3D renderer");
	delete texMgr;	
	texMgr = NULL;

	baseModel.release();
	bgModel.release();

	SAFE_REL_FINAL(d3dsc);	
	SAFE_REL_FINAL(vbBgrnd);	
	SAFE_REL_FINAL(ibBgrnd);	
	SAFE_REL_FINAL(vxShader);	
	SAFE_REL_FINAL(vbGrid);	
	SAFE_REL_FINAL(depth);
	SAFE_REL_FINAL(bb);
	SAFE_REL_FINAL(membb);
	SAFE_REL_FINAL(vbNormal);
	SAFE_REL_FINAL(vbMass);	
	SAFE_REL_FINAL(vpBuf);	
	SAFE_REL_FINAL(vrtxMarker);	
	SAFE_REL_FINAL(massMarker);		
	SAFE_REL_FINAL(d3dd);
	SAFE_REL_FINAL(d3d);
}

void d3drenderer::setDoc(CObjektiv2Doc *odoc) {
	//doc = odoc;
}

void d3drenderer::setTexturePath(char *path, bool doFlush) {
	if(doFlush)
		texMgr->flushAll();
	texMgr->setTexturePath(path);
}

int d3drenderer::initD3D(HWND win, DWORD antialias, bool withSC) {
	if(d3dd)
		return 0;
	d3drdbg("Initialize Direct3D renderer");

	if(!d3d)
		d3d = Direct3DCreate9(D3D_SDK_VERSION);
	if(!d3d)
		return -1;	// Couldn't init d3d
	do {
		switch(antialias) {
			case 1:
			case 0:	multisampling=D3DMULTISAMPLE_NONE;break;
			case 2:	multisampling=D3DMULTISAMPLE_2_SAMPLES;break;
			case 3:	multisampling=D3DMULTISAMPLE_3_SAMPLES;break;
			case 4:	multisampling=D3DMULTISAMPLE_4_SAMPLES;break;
			case 8:	multisampling=D3DMULTISAMPLE_8_SAMPLES;break;
			case 16:multisampling=D3DMULTISAMPLE_16_SAMPLES;break;
			default:
				d3drdbg("d3drenderer::initD3D: Unknown multisample level");
				multisampling = D3DMULTISAMPLE_NONE;
				break;
		}
		if(antialias <= 1)
			break;	// Dont check for nonantialiased support, it must work
		antialias = antialias / 2;	// Try one level below if not supported
	} while(d3d->CheckDeviceMultiSampleType(0, D3DDEVTYPE_HAL, D3DFMT_A8R8G8B8, true, multisampling, NULL) != D3D_OK);
	d3drdbg("d3drenderer::initD3D: Using multisample level %d", multisampling);

	useSwapchain = withSC;

	ZeroMemory(&pp, sizeof(pp));
	pp.BackBufferWidth = 64;	// Device backbuffer is not used for anything
	pp.BackBufferHeight = 64;
	pp.BackBufferFormat = D3DFMT_A8R8G8B8;
	pp.BackBufferCount = 0;
	pp.MultiSampleType = D3DMULTISAMPLE_NONE;
	pp.SwapEffect = D3DSWAPEFFECT_DISCARD;	
	pp.hDeviceWindow = win;
	pp.Windowed = TRUE;
	pp.EnableAutoDepthStencil = FALSE;
	pp.AutoDepthStencilFormat = D3DFMT_D24X8;
	pp.Flags = 0;
	pp.FullScreen_RefreshRateInHz = 0;
	pp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

	DWORD flags = D3DCREATE_MIXED_VERTEXPROCESSING+D3DCREATE_FPU_PRESERVE+D3DCREATE_MULTITHREADED;

	HRESULT ret = d3d->CreateDevice(0, D3DDEVTYPE_HAL, win, flags, &pp, &d3dd);
	if(ret != D3D_OK) {
		d3drdbg("d3drenderer::initD3D: CreateDevice failed: %s", DXGetErrorString9(ret));
		return -1;
	} 
	d3dd->GetDeviceCaps(&caps);

	// Compile shaders
	D3DXAssembleShader(vertexPixelShader, (UINT) strlen(vertexPixelShader)-1, NULL, NULL, NULL, &vxShaderCode, NULL);
	D3DCALLCR(d3dd->CreatePixelShader((const DWORD*) vxShaderCode->GetBufferPointer(), &vxShader));

	d3dd->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &devbb);
	devbb->Release();

	// Create a default size viewport surface
	rsSize = 512;
	setRenderSize(rsSize, rsSize);

	// Create vertex maker point sprite texture
	// TODO: check for failure (7x7 texture not supported) and create 8x8 one
	D3DCALLCR(d3dd->CreateTexture(vrtxMarkerSize, vrtxMarkerSize, 1, NULL, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, &vrtxMarker, NULL));
	
	D3DLOCKED_RECT lck;
	D3DCALLCR(vrtxMarker->LockRect(0, &lck, NULL, 0));
	memcpy(lck.pBits, vrtxMarkerData, vrtxMarkerSize*vrtxMarkerSize*4);
	vrtxMarker->UnlockRect(0);

	// Create mass maker point sprite texture
	// TODO: check for failure (5x5 texture not supported) and create 8x8 one
	D3DCALLCR(d3dd->CreateTexture(massMarkerSize, massMarkerSize, 1, NULL, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, &massMarker, NULL));	
	D3DCALLCR(massMarker->LockRect(0, &lck, NULL, 0));
	memcpy(lck.pBits, massMarkerData, massMarkerSize*massMarkerSize*4);
	massMarker->UnlockRect(0);

	// Init texture manager
	texMgr = new d3dtexmanager(d3dd);

	// Create grid lines
	int numV = ((gridSize+1) + (gridSize+1) + 2)*2;
	D3DCALLCR(d3dd->CreateVertexBuffer(numV*sizeof(D3DBASICV), D3DUSAGE_WRITEONLY, D3DGRIDV_VERTEXTYPE, D3DPOOL_MANAGED, &vbGrid, NULL));	
	D3DBASICV *vdata;
	D3DCALLCR(vbGrid->Lock(0, 0, (void**)&vdata, NULL));
	for(int i=-gridSize/2;i<=gridSize/2;i++) {
		vdata->x = (float)i;vdata->z = (float)-gridSize/2;vdata->y = 0;	vdata->col = i==0?gridColorCntr:gridColor; vdata++;
		vdata->x = (float)i;vdata->z = (float) gridSize/2;vdata->y = 0;	vdata->col = i==0?gridColorCntr:gridColor; vdata++;
		vdata->z = (float)i;vdata->x = (float)-gridSize/2;vdata->y = 0;	vdata->col = i==0?gridColorCntr:gridColor; vdata++;
		vdata->z = (float)i;vdata->x = (float) gridSize/2;vdata->y = 0;	vdata->col = i==0?gridColorCntr:gridColor; vdata++;
	}
	vdata->x = 0;vdata->z = 0;vdata->y = -gridSize/2;	vdata->col = gridColorCntr; vdata++;
	vdata->x = 0;vdata->z = 0;vdata->y =  gridSize/2;	vdata->col = gridColorCntr; vdata++;
	vbGrid->Unlock();

	return 1;
}

// Return true if device is in good condition (not lost state)
bool d3drenderer::deviceOK() {
	if(!d3dd || !bb || !depth)
		return false;
	return (d3dd->TestCooperativeLevel()==D3D_OK);
}

void d3drenderer::clear(DWORD color) {
	d3dd->Clear(0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, color, 1.0f, 0);
}

void d3drenderer::saveModelScreenshot(char *path) {
	int dpitch, dheight;
	void* data = getBufferData(&dpitch, &dheight);
	if(data) {
		FILE *f = fopen(path, "wb");
		if(f) {
			BYTE tgaHdr[12];ZeroMemory(&tgaHdr, 12);tgaHdr[2] = 0x02;
			fwrite(&tgaHdr, 12, 1, f);
			USHORT w = rWidth; fwrite(&w, 2, 1, f);		
			USHORT h = rHeight; fwrite(&h, 2, 1, f);
			fwrite(" ", 1, 1, f);

			// Conver RGBA to ARGB
			BYTE *tdata = (BYTE*)data;
			while(tdata-(BYTE*)data < dpitch*dheight) {
				BYTE r = tdata[0];
				BYTE g = tdata[1];
				BYTE b = tdata[2];
				BYTE a = tdata[3];
				*tdata++ = a;
				*tdata++ = r;
				*tdata++ = g;
				*tdata++ = b;
			}
			// Write inverted
			for(int y=0;y<dheight;y++)
				fwrite(((char*)data)+(((dheight-y)-1)*dpitch), dpitch, 1, f);
			//fwrite(data, dpitch, dheight, f);
			

			BYTE tgaFoot[27] = {
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
				0x00, 0x54, 0x52, 0x55, 0x45, 0x56, 0x49, 0x53,
				0x49, 0x4F, 0x4E, 0x2D, 0x58, 0x46, 0x49, 0x4C,
				0x45, 0x2E, 0x00 
			};

			fwrite(&tgaFoot, 27, 1, f);
			fclose(f);

		}
		releaseBufferData();
	}
}

int d3drenderer::setRenderSize(int width, int height) {
	if(!d3dd)
		return -1;	
	rWidth = width;
	rHeight = height;

	if(rWidth > (int)caps.MaxTextureWidth) rWidth = caps.MaxTextureWidth;
	if(rHeight > (int)caps.MaxTextureHeight) rHeight = caps.MaxTextureHeight;
	rAspect = (float)rWidth/(float)rHeight;
	
	if(useSwapchain) {
		// Create swapchain for rendering
		D3DPRESENT_PARAMETERS scPP;
		memcpy(&scPP, &pp, sizeof(D3DPRESENT_PARAMETERS));
		scPP.BackBufferWidth = rWidth;
		scPP.BackBufferHeight = rHeight;
		scPP.MultiSampleType = multisampling;
		SAFE_REL_FINAL(d3dsc);
		SAFE_REL_FINAL(depth);
		D3DCALLCR(d3dd->CreateAdditionalSwapChain(&scPP, &d3dsc));
		D3DCALLCR(d3dd->CreateDepthStencilSurface(rWidth, rHeight, D3DFMT_D24X8, multisampling, 0, false, &depth, NULL));
		D3DCALL(d3dd->SetDepthStencilSurface(depth));

		LPDIRECT3DSURFACE9 scBB;
		D3DCALLCR(d3dsc->GetBackBuffer(0, D3DBACKBUFFER_TYPE_MONO, &scBB));
		D3DCALL(d3dd->SetRenderTarget(0, scBB));
		scBB->Release();

		D3DVIEWPORT9 vp = {0, 0, rWidth, rHeight, 0.0f, 1.0f};
		d3dd->SetViewport(&vp);	
		return 1;
	} else {
		int s = rWidth>rHeight?rWidth:rHeight;
		if(s > rsSize || !bb) {
			rsSize = s;
			//d3drdbg("Resizing render surface to %dx%d", rsSize, rsSize);
			// Resize render surface & depth buffer
			d3dd->SetDepthStencilSurface(NULL);
			d3dd->SetRenderTarget(0, devbb);
			SAFE_REL_FINAL(bb);SAFE_REL_FINAL(depth);

			D3DCALLCR(d3dd->CreateRenderTarget(rsSize, rsSize, D3DFMT_A8R8G8B8, multisampling, 0, false, &bb, NULL));
			D3DCALLCR(d3dd->CreateDepthStencilSurface(rsSize, rsSize, D3DFMT_D24X8, multisampling, 0, false, &depth, NULL));
			d3dd->SetDepthStencilSurface(depth);
			d3dd->SetRenderTarget(0, bb);
		}

		// Create viewport size buffer & system memory surface
		SAFE_REL_FINAL(vpBuf);SAFE_REL_FINAL(membb);
		D3DCALLCR(d3dd->CreateRenderTarget(rWidth, rHeight, D3DFMT_A8R8G8B8, D3DMULTISAMPLE_NONE, 0, false, &vpBuf, NULL));
		D3DCALLCR(d3dd->CreateOffscreenPlainSurface(rWidth, rHeight, D3DFMT_A8R8G8B8, D3DPOOL_SYSTEMMEM, &membb, NULL));

		D3DVIEWPORT9 vp = {0, 0, rWidth, rHeight, 0.0f, 1.0f};
		d3dd->SetViewport(&vp);	
		return 1;
	}
}

void d3drenderer::presentWin(HWND win) {
	if(!d3dd)
		return;
	if(useSwapchain) {
		d3dsc->Present(NULL, NULL, win, NULL, 0);
	} else {
		// Copy our bb -> real bb, then present to window (debug)
		d3dd->StretchRect(bb, NULL, devbb, NULL, D3DTEXF_LINEAR);
		d3dd->Present(NULL, NULL, win, NULL);
	}
}

void* d3drenderer::getBufferData(int *pitch, int *height) {
	if(!d3dd)
		return 0;

	// Copy rendered data to viewport buffer
	RECT sr = {0, 0, rWidth, rHeight};
	D3DCALL(d3dd->StretchRect(bb, &sr, vpBuf, &sr, D3DTEXF_NONE));

	// Get viewport buffer data, lock & return pointer to data
	if(d3dd->GetRenderTargetData(vpBuf, membb) != D3D_OK) {
		d3drdbg("d3drenderer::getBufferData: GetRenderTargetData failed!");
		return 0;
	}
	D3DLOCKED_RECT lck;
	if(membb->LockRect(&lck, NULL, D3DLOCK_READONLY) != D3D_OK) {
		d3drdbg("d3drenderer::getBufferData: LockRect failed!");
		return 0;
	}
	if(pitch) *pitch = lck.Pitch;
	if(height) *height = rHeight;
	return lck.pBits;
}

void d3drenderer::releaseBufferData() {
	if(membb)
		membb->UnlockRect();
}
 
void d3drenderer::positionCamera(float x, float y, float z, float rotx, float roty, float dist) {
	// TODO: there is a much simplier way for doing all this
	D3DXMATRIX viewmat, mroty, mrotx, mtrans, mdist;
	D3DXMatrixLookAtLH(&viewmat, &D3DXVECTOR3(0, 1.0f,  -0.000001f), // Camera position
						&D3DXVECTOR3(0, 0, 0),   // Look-at point
						&D3DXVECTOR3(0.0f, 1.0f, 0.0f )); // Up vector
	D3DXMatrixRotationYawPitchRoll(&mroty, 0, roty, 0);
	D3DXMatrixRotationYawPitchRoll(&mrotx, -rotx, 0, 0);
	D3DXMatrixTranslation(&mtrans, x, y, z);
	D3DXMatrixTranslation(&mdist, 0, 0, dist-1.0f);
	viewmat = mtrans * mrotx * mroty * viewmat * mdist;
	d3dd->SetTransform(D3DTS_VIEW, &viewmat);

	// Set identity world matrix - only used to rotate grid
	D3DXMATRIX worldmat;
	D3DXMatrixIdentity(&worldmat);
	d3dd->SetTransform(D3DTS_WORLD, &worldmat);
}

void d3drenderer::positionCameraPerspective(float x, float y, float z, float rotx, float roty, float dist, float fov) {
	if(!d3dd)
		return;
	D3DXMATRIX projmat;
	D3DXMatrixPerspectiveFovLH(&projmat, fov, rAspect, 0.1f, abs(dist) + 50);
	d3dd->SetTransform(D3DTS_PROJECTION, &projmat);
	positionCamera(x, y, z, rotx, roty, dist);

	d3dd->SetRenderState(D3DRS_CLIPPING, TRUE);	
}

void d3drenderer::positionCameraOrtho(float x, float y, float z, float rotx, float roty, float dist, float scale) {
	if(!d3dd)
		return;
	if(rAspect < 1)
		scale *= rAspect;	// Narrow viewport = fit horizontally
	D3DXMATRIX projmat;
	D3DXMatrixOrthoLH(&projmat, 2/(scale/rAspect), 2/scale, -333.0f, 333.0f);
	d3dd->SetTransform(D3DTS_PROJECTION, &projmat);
	positionCamera(x, y, z, rotx, roty, 1.0f);

	d3dd->SetRenderState(D3DRS_CLIPPING, FALSE);	// No clipping for orthographic projection
}


void d3drenderer::positionLight(void) {
	if(!d3dd)
		return;
	// Set up light
	D3DLIGHT9 l;ZeroMemory(&l, sizeof(D3DLIGHT9));
	l.Type = D3DLIGHT_DIRECTIONAL;
	l.Diffuse = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);


  D3DXMATRIX projmat;
  d3dd->GetTransform(D3DTS_VIEW , &projmat);
  D3DXMatrixInverse(&projmat,NULL,&projmat);
  D3DXVECTOR3 vecOut;  
  const D3DXVECTOR3 vecIn = D3DXVECTOR3(0,0,-1);
  D3DXVec3Normalize(&vecOut,&vecIn);
  D3DXVec3TransformNormal(&vecOut,&vecOut,&projmat);
  l.Direction = D3DXVECTOR3(vecOut.x,vecOut.y,vecOut.z);

	l.Range = 15000.0f;	
	d3dd->SetLight(0, &l);
	d3dd->LightEnable(0, TRUE);

	// Negative lighting from below for some additional shading
	l.Diffuse = D3DXCOLOR(-0.66f, -0.66f, -0.66f, 1.0f);
	l.Direction = D3DXVECTOR3(-l.Direction.x,-l.Direction.y,-l.Direction.z);
	d3dd->SetLight(1, &l);
	d3dd->LightEnable(1, TRUE);

	// Set ambient material - used for normal 3D rendering
	ZeroMemory(&ambient, sizeof(ambient));
	ambient.Specular = D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f);
	ambient.Diffuse = D3DXCOLOR(0.55f, 0.55f, 0.55f, 1.0f);
	ambient.Ambient = D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f);
	ambient.Emissive = D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f);
	d3dd->SetMaterial(&ambient);
	d3dd->SetRenderState(D3DRS_AMBIENT, 0xFF505050);
}

// Applies a render state block
void d3drenderer::applyRenderStates(const RSTATE *st) {
	if(!d3dd)
		return;
	int i=0;
	while(st[i].state != NULL) {
		d3dd->SetRenderState((D3DRENDERSTATETYPE)st[i].state, st[i].val);
		i++;
	}
}

void d3drenderer::renderDataProxies(DWORD flags) {
	// Draw them proxies - must have called loadProxies before
	for(DWORD i=0;i<proxies.size();i++) {
		D3DPROXY *px = &proxies[i];

		D3DXMATRIX mat;
		D3DXMatrixIdentity(&mat);

		// Get vectors
		D3DXVECTOR3 dir = px->dir;
		D3DXVECTOR3 up = px->up;
		D3DXVec3Normalize(&dir, &dir);
		D3DXVec3Normalize(&up, &up);
		D3DXVECTOR3 right;
		D3DXVec3Cross(&right, &up, &dir);

		// Build matrix
		mat._11 = right[0]; mat._12 = right[1]; mat._13 = right[2];
		mat._21 = up[0]; mat._22 = up[1]; mat._23 = up[2];
		mat._31 = dir[0]; mat._32 = dir[1]; mat._33 = dir[2];
		mat._41 = px->pos[0]; mat._42 = px->pos[1]; mat._43 = px->pos[2];
		
		d3dd->SetTransform(D3DTS_WORLD, &mat);
		if(px->pidx != -1) {
			d3dr::MODELBLOCK *mb = proxyModels[px->pidx]->getModelData();
			if((flags && D3DR_SOLID) || (flags && D3DR_TEXTURE))
				renderDataTX(flags, mb); 
			if((flags && D3DR_WIREFRAME) || (flags && D3DR_VERTICES))
				renderDataVL(flags, mb); 
		}
	}

	D3DXMATRIX mat;
	D3DXMatrixIdentity(&mat);
	d3dd->SetTransform(D3DTS_WORLD, &mat);
}

void d3drenderer::loadProxies(ObjectData &obj) {

	proxyModels.clear();
	proxies.clear();

	// Find proxy models
	int i = 0;
	const char *sname;
	while(sname = obj.NamedSel(i)) {
		if(!strncmp(sname, PROXYPFX, strlen(PROXYPFX))) {
			const char *pmodel = sname+strlen(PROXYPFX)+1;

			obj.UseNamedSel(sname);
			const Selection *sel = obj.GetSelection();
			if(sel->NPoints() != 3) {
				RptF("Invalid proxy <%s> (%d points)", pmodel, sel->NPoints());
#ifdef PRINTF_ERRORS
				printf("WARNING: Invalid proxy: <%s> (%d points != 3)\n", pmodel, sel->NPoints());
#endif
				i++;
				continue;
			}
			AutoArray<int> points = sel->GetSelectedPoints();
			
			D3DPROXY px;
			PosT &pa = obj.Point(points[0]);	// Position
			PosT &pb = obj.Point(points[1]);	// Up (typically 2x length)
			PosT &pc = obj.Point(points[2]);	// Dir					
			for(int u=0;u<3;u++) {
				px.pos[u] = pa[u];
				px.up[u] = pb[u]-pa[u];
				px.dir[u] = pc[u]-pa[u];
			}

			// Does this proxy already exist?
			int idx = -1;
			for(DWORD i=0;i<proxyModels.size();i++)
				if(proxyModels[i] && proxyModels[i]->MatchName(pmodel))
					idx = i;

			if(idx == -1) {
				// Attempt to load proxy
				d3dproxy *proxy = new d3dproxy((char*)pmodel, proxyPath);
				if(proxy->isValid()) {
					proxyModels.push_back(proxy);
					idx = (int)proxyModels.size()-1;
				} else {
					RptF("Cannot load proxy <%s>", pmodel);
					delete proxy;
				}
			}
			px.pidx = idx;
			proxies.push_back(px);
		}
		i++;
	}
}

// Creates mass data vertexbuffer
int d3drenderer::createDataM(ObjectData &obj) {
	if(!d3dd)
		return -1;	

	int np = obj.NPoints();	
	int cm = 0;
	for(int i=0;i<np;i++)
		if(obj.PointSelected(i) && obj.GetPointMass(i) != 0)
			cm++;
		//cm += obj.GetPointMass(i)!=0?1:0;
	SAFE_REL_FINAL(vbMass);
	if(!cm)
		return 0;

//	d3drdbg("d3drenderer::createDataM creating mass vertex buffer (%d points)", cm);	
	D3DCALLCR(d3dd->CreateVertexBuffer(cm*sizeof(D3DSPOINTV), D3DUSAGE_WRITEONLY, D3DSPOINTV_VERTEXTYPE, D3DPOOL_MANAGED, &vbMass, NULL));
	D3DSPOINTV *vdata;
	D3DCALLCR(vbMass->Lock(0, 0, (void**)&vdata, NULL));

	D3DSPOINTV *cdata = vdata;
	double max = obj.GetMaxMass();
	for(int i=0;i<np;i++) {
		double m = obj.GetPointMass(i);
		if(m != 0 && obj.PointSelected(i)) {			
			cdata->x = obj.Point(i)[0];
			cdata->y = obj.Point(i)[1];
			cdata->z = obj.Point(i)[2];
			cdata->size = (float) ((m/max) * 20.0);
			cdata++;
		}
	}
	vbMass->Unlock();
	return 1;
}

// Creates normal data vertexbuffer
int d3drenderer::createDataN(ObjectData &obj) {
	if(!d3dd)
		return -1;
	int nf = obj.NFaces();
	if(nf == 0 || obj.NNormals() == 0)
		return 0;

	// Count real normals
	int nn = 0;
	for(int i=0;i<nf;i++)
		nn += obj.FaceSelected(i)?obj.Face(i).n:0;
	SAFE_REL_FINAL(vbNormal);
	if(!nn)
		return 0;

//	d3drdbg("d3drenderer::createDataN creating normal data vertex buffer (%d normals)", nn);	
	D3DCALLCR(d3dd->CreateVertexBuffer(nn*sizeof(D3DBASICV)*2, D3DUSAGE_WRITEONLY, D3DGRIDV_VERTEXTYPE, D3DPOOL_MANAGED, &vbNormal, NULL));
	D3DBASICV *vdata;
	D3DCALLCR(vbNormal->Lock(0, 0, (void**)&vdata, NULL));
	
	D3DBASICV *cdata = vdata;
	for(int i=0;i<nf;i++) {
		if(obj.FaceSelected(i)) {
			const FaceTraditional *f = &obj.Face(i);		
			for(int o=0;o<f->n;o++) {
				DWORD fi = f->vs[o].point;
				cdata->x = obj.Point(fi)[0];
				cdata->y = obj.Point(fi)[1];
				cdata->z = obj.Point(fi)[2];
				cdata->col = normalColor;
				cdata++;

				float normalLen = 0.05f;
				DWORD ni = f->vs[o].normal;
				cdata->x = obj.Point(fi)[0] - (obj.Normal(ni)[0] * normalLen);
				cdata->y = obj.Point(fi)[1] - (obj.Normal(ni)[1] * normalLen);
				cdata->z = obj.Point(fi)[2] - (obj.Normal(ni)[2] * normalLen);
				cdata->col = normalColorEnd;
				cdata++;
			}
		}
	}	

	vbNormal->Unlock();
	return 1;
}

int d3drenderer::createDataVL(ObjectData &obj, bool isBackground, bool noHide) {
	return createDataVL(obj, isBackground?bgModel:baseModel);
};

// Creates D3D vertexbuffers from ObjectData for D3DPT_LINELIST and D3DPT_POINTLIST
int d3drenderer::createDataVL(ObjectData &obj, d3dr::MODELBLOCK &dst) {
	if(!d3dd)
		return -1;

	bool isBackground = false; // TODO: remove
	LPDIRECT3DVERTEXBUFFER9 *vb = &(dst.vbVrtx);
	LPDIRECT3DINDEXBUFFER9 *ib = &dst.ibLines;
	/*
	if(!isBackground) {
		// Creating normal model data
		vb = &vbVrtx;
		ib = &ibLines;
	} else {
		// Create background LOD data
		vb = &vbBgrnd;
		ib = &ibBgrnd;
	}
*/
	const Selection *hid = obj.GetHidden();
	int np = obj.NPoints();
	int nf = obj.NFaces();
	SAFE_REL_FINAL(*vb);
	SAFE_REL_FINAL(*ib);
	if(np == 0)
		return -1;

	dst.modelSize = 1.0f;
	dst.modelHeight = 0.0f;

	// Create vertex buffer from vertex data
	debugout("d3drenderer::createDataVL creating vertex data vertex buffer");
	D3DCALLCR(d3dd->CreateVertexBuffer(np*sizeof(D3DBASICV), D3DUSAGE_WRITEONLY, D3DBASICV_VERTEXTYPE, D3DPOOL_MANAGED, vb, NULL));	
	D3DBASICV *vdata;
	D3DCALLCR((*vb)->Lock(0, 0, (void**)&vdata, NULL));
	for(int i=0;i<np;i++) {
		PosT &pos = getTransformedPoint(obj, i);
		vdata[i].x = pos[0];
		vdata[i].y = pos[1];
		vdata[i].z = pos[2];

		bool h = hid->PointSelected(i);

		if(!h) {
			if(abs(vdata[i].x) > dst.modelSize) dst.modelSize = abs(vdata[i].x);
			if(abs(vdata[i].y) > dst.modelSize) dst.modelSize = abs(vdata[i].y);
			if(abs(vdata[i].z) > dst.modelSize) dst.modelSize = abs(vdata[i].z);
			if(abs(vdata[i].y) > dst.modelHeight) dst.modelHeight = abs(vdata[i].y);		
		}

		if(obj.PointSelected(i)) {
			BYTE w = (BYTE)(255.0f*obj.GetSelection()->PointWeight(i));
			vdata[i].col = D3DCOLOR_ARGB(128, w, h?200:0, 255-w);
		} else {
			//if(!h || noHide)
			if(!h)
				vdata[i].col = D3DCOLOR_ARGB(10, 0, 0, 0);
			else
				vdata[i].col = D3DCOLOR_ARGB(0, 0, 0, 0);
		}
	}

	// Count number of needed lines
	int nl = 0;
	int nls = 0;
	for(int i=0;i<nf;i++) {
		if(hid->FaceSelected(i))
			continue;
		nl += obj.Face(i).n*2;
		if(obj.FaceSelected(i))
			nls += obj.Face(i).n*2;
	}

	CEdges edges;
	obj.LoadSharpEdges(edges);

	// Create index buffer for lines
	// contains first indices for unselected lines, then selected smooth edges (idataSel), then sharp (ibLinesNselSharp)
	debugout("d3drenderer::createDataVL creating lines index data (%d lines, %d selected)", nl/2, nls/2);
	D3DCALLCR(d3dd->CreateIndexBuffer(nl*4, D3DUSAGE_WRITEONLY, D3DFMT_INDEX32, D3DPOOL_MANAGED, ib, NULL));	
	DWORD *idata;	
	D3DCALLCR((*ib)->Lock(0, 0, (void**)&idata, NULL));
	DWORD *idataSel = idata + nl - nls;
	DWORD *idataSelSharp = idata + nl;

	if(!isBackground)
		dst.ibLinesNsel = dst.ibLinesNselSharp = 0;
	for(int i=0;i<nf;i++) {
		// Create index buffer data
		if(hid->FaceSelected(i))
			continue;	// Face hidden
		FaceTraditional *f = &obj.Face(i);
		bool sel = isBackground?false:obj.FaceSelected(i);
		for(int o=0;o<f->n;o++) {
			int ia = f->vs[o].point;
			int ib = f->vs[o==f->n-1?0:o+1].point;
			if(sel) {
				if(edges.IsEdge(ia, ib)) {
					dst.ibLinesNselSharp++;
					*--idataSelSharp = ia;
					*--idataSelSharp = ib;
				} else {
					dst.ibLinesNsel++;
					*idataSel++ = ia;
					*idataSel++ = ib;
				}
			} else {
				*idata++ = ia;
				*idata++ = ib;
			}
		}
	}

	(*vb)->Unlock();
	(*ib)->Unlock();

	return 1;
}

// Translates point cooordinates by selection translation
PosT d3drenderer::getTransformedPoint(ObjectData &obj, int i) {
#ifndef D3DRENDERER_EXE
	if(doc && doc->seltransform && obj.PointSelected(i)) {
		HVECTOR vx;
		PosT pos = obj.Point(i);
		doc->SelectionTransform(i, pos, vx, obj.GetSelection()->PointWeight(i));
		pos[0] = vx[0];
		pos[1] = vx[1];
		pos[2] = vx[2];
		return pos;	
	} else
		return obj.Point(i);
#else
	// No selection transforms in exe build (View only)
	return obj.Point(i);
#endif
}

// Helper function to convert object face data to d3d textured vertex format
void d3drenderer::objFaceToD3DTexv(D3DTEXV *odata, ObjectData &obj, int face, int pi) {
	const FaceTraditional *f = &obj.Face(face);
	DWORD p = f->vs[pi].point;
	DWORD n = f->vs[pi].normal;
	PosT &pos = getTransformedPoint(obj, p);
	odata->x = pos[0];	  odata->y = pos[1];   odata->z = pos[2];
	if(obj.NNormals() >= 3) {
		odata->nx = obj.Normal(n)[0]; odata->ny = obj.Normal(n)[1]; odata->nz = obj.Normal(n)[2];
	} else {
		odata->nx = 0.0f; odata->ny = 0.0f; odata->nz = 0.0f;
	}
	ObjUVSet *uvs = obj.GetActiveUVSet();
	ObjUVSet_Item uv = uvs->GetUV(face, pi);
	odata->tu = uv.u; odata->tv = uv.v;
	odata->col = obj.FaceSelected(face)? 0xFFFF1111 : 0x00444444;
}

// Helper to set min- mip- and mag texture filtering + anisotropy
void d3drenderer::setTexFiltering(DWORD min, DWORD mip, DWORD mag, DWORD ani, DWORD stage) {
	if(d3dd) {
		d3dd->SetSamplerState(stage, D3DSAMP_MAXANISOTROPY, ani);	
		d3dd->SetSamplerState(stage, D3DSAMP_MINFILTER, min);
		d3dd->SetSamplerState(stage, D3DSAMP_MIPFILTER, mip);
		d3dd->SetSamplerState(stage, D3DSAMP_MAGFILTER, mag);
	}
}

// 2.8.2009 hotfix by bxbx: face selection ("T") and face reverse ("W") didn't refresh D3D      

unsigned long long d3drenderer::getModelChecksum(ObjectData &obj) {
	// Returns a "checksum" of model data, used to detect changes in model
	// TODO: Awful method to do it, should track changes in O2 instead
	unsigned long long		csum = 0;
	const Selection *hid = obj.GetHidden();
	for(int i=0;i<obj.NPoints();i++) {
		csum = csum + *(DWORD*)(&obj.Point(i)[0]);
		csum = csum + *(DWORD*)(&obj.Point(i)[1]);
		csum = csum + *(DWORD*)(&obj.Point(i)[2]);
		if(obj.PointSelected(i)) csum += 256*(i+1);
		if(hid->PointSelected(i)) csum += 512*(i+1);
	}
	ObjUVSet *uvs = obj.GetActiveUVSet();
	for(int fi=0;fi<obj.NFaces();fi++) {
		const FaceTraditional *f = &obj.Face(fi);
		if(hid->FaceSelected(fi)) csum += 1024*(fi+1);
    if(obj.FaceSelected(fi)) csum += 2048*(fi+1);
    csum += f->vs[0].point*3+f->vs[1].point*5+f->vs[2].point*7+f->vs[3].point*13;
		for(int i=0;i<f->n;i++) {
			if(uvs) {
				ObjUVSet_Item uv = uvs->GetUV(fi, i);
				csum = csum + (*(DWORD*)(&uv.u) * 10);
				csum = csum + (*(DWORD*)(&uv.v) * 10);
			}
			int n = f->vs[i].normal;
			if(obj.NNormals() > n) {
				csum = csum + (*(DWORD*)(&obj.Normal(n)[0]) * 100);
				csum = csum + (*(DWORD*)(&obj.Normal(n)[1]) * 100);
				csum = csum + (*(DWORD*)(&obj.Normal(n)[2]) * 100);
			}
		}
	}
	return csum;
}


int d3drenderer::createDataTX(ObjectData &obj, bool withTextures, bool noHide) {
	return createDataTX(obj, baseModel, withTextures);
}

// Creates vertex- and indexbuffers for textured rendering
int d3drenderer::createDataTX(ObjectData &obj, d3dr::MODELBLOCK &dst, bool withTextures) {
	if(!d3dd || !texMgr)
		return -1;

	bool noHide = false;	// TODO: remove
	LPDIRECT3DVERTEXBUFFER9 *vb = &(dst.vbTex);

	const Selection *hid = obj.GetHidden();
	// Count needed faces (after quad -> tri)
	int nf = 0;
	for(int i=0;i<obj.NFaces();i++)
		if(!hid->FaceSelected(i) || noHide)
			nf += obj.Face(i).n==3?1:2;
	int np = nf*3;	// Number of points

	debugout("d3drenderer::createDataTX creating face data vertex buffer (%df %dp, %d bytes)", nf, np, np*sizeof(D3DTEXV));
	SAFE_REL_FINAL(*vb);
	D3DCALLCR(d3dd->CreateVertexBuffer(np*sizeof(D3DTEXV), D3DUSAGE_WRITEONLY, D3DTEXV_VERTEXTYPE, D3DPOOL_MANAGED, vb, NULL));	
	D3DTEXV *vdata;
	D3DCALLCR((*vb)->Lock(0, 0, (void**)&vdata, NULL));

	D3DTEXV *cdata = vdata;
	dst.texBlocks.clear();
//	texMgr->prepareFlush();
	IDXBLOCK cb;
	int lastI = 0;
	char lastTex[256]="_KEGETYS_";
	int fid = 0;
	for(int i=0;i<obj.NFaces();i++) {
		if(hid->FaceSelected(i) && !noHide)
			continue;	// Face hidden

		bool hadAddedFace = false;
		const FaceTraditional *f = &obj.Face(i);
		for(int o=0;o<3;o++)
			objFaceToD3DTexv(cdata++, obj, i, o);		
		if(f->n == 4) {
			// Create additional triangle for quad
			objFaceToD3DTexv(cdata++, obj, i, 2);
			objFaceToD3DTexv(cdata++, obj, i, 3);
			objFaceToD3DTexv(cdata++, obj, i, 0);
			hadAddedFace = true;
		}

		const char *tex = f->vTexture;
		if(_strnicmp(tex, lastTex, 256) && withTextures) {
			// Texture changed
			if(i!=0) {
				cb.begin = lastI;
				cb.num = fid-lastI;
				cb.tex = texMgr->addTexture(lastTex);
				dst.texBlocks.push_back(cb);
				//d3drdbg("New iblock: <%s(%d)> %d %d\n", lastTex, cb.tex, cb.begin, cb.num);
			}
			lastI = fid;
			strncpy(lastTex, tex, 256);
		}
		fid += hadAddedFace?2:1;
	}

	if(withTextures) {
		// Add last iblock
		cb.begin = lastI;
		cb.num = fid-lastI;
		cb.tex = texMgr->addTexture(lastTex);
		dst.texBlocks.push_back(cb);
		//d3drdbg("New iblock: <%s(%d)> %d %d\n", lastTex, cb.tex, cb.begin, cb.num);
	}

	(*vb)->Unlock();
//	texMgr->flushUnused();	// Releases old textures which were not referenced to by addTexture
	return 1;
}

void d3drenderer::renderGrid(float rotx, float roty) {
	if(!vbGrid)
		return;

	// Rotate grid for non-projection views
	D3DXMATRIX worldmat, mroty, mrotx;
	D3DXMatrixRotationYawPitchRoll(&mroty, 0, roty, 0);
	D3DXMatrixRotationYawPitchRoll(&mrotx, 0, 0, rotx);
	worldmat = mrotx * mroty;
	d3dd->SetTransform(D3DTS_WORLD, &worldmat);

	D3DVERTEXBUFFER_DESC desc; ZeroMemory(&desc, sizeof(D3DVERTEXBUFFER_DESC));
	vbGrid->GetDesc(&desc);
	int numLines = desc.Size/sizeof(D3DBASICV)/2;

	applyRenderStates(state_wireframe);
	d3dd->SetTexture(0, NULL);
	d3dd->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);

	d3dd->SetStreamSource(0, vbGrid, 0, sizeof(D3DBASICV));
	d3dd->SetFVF(D3DGRIDV_VERTEXTYPE);
	d3dd->BeginScene();
	D3DCALL( d3dd->DrawPrimitive(D3DPT_LINELIST, 0, numLines) );
	d3dd->EndScene();

	D3DXMatrixIdentity(&worldmat);
	d3dd->SetTransform(D3DTS_WORLD, &worldmat);
}

void d3drenderer::renderDataN(DWORD flags) {
	if(!vbNormal)
		return;

	D3DVERTEXBUFFER_DESC desc; ZeroMemory(&desc, sizeof(D3DVERTEXBUFFER_DESC));
	vbNormal->GetDesc(&desc);
	int numLines = desc.Size/sizeof(D3DBASICV)/2;

	applyRenderStates(state_wireframe);
	d3dd->SetTexture(0, NULL);
	d3dd->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
	d3dd->SetRenderState(D3DRS_ANTIALIASEDLINEENABLE, (flags&D3DR_AALINES)?TRUE:FALSE);

	d3dd->SetStreamSource(0, vbNormal, 0, sizeof(D3DBASICV));
	d3dd->SetFVF(D3DGRIDV_VERTEXTYPE);
	d3dd->BeginScene();
	D3DCALL( d3dd->DrawPrimitive(D3DPT_LINELIST, 0, numLines) );
	d3dd->EndScene();
}

void d3drenderer::renderDataTX(DWORD flags) {
	renderDataTX(flags, &baseModel);
}

void d3drenderer::renderDataTX(DWORD flags, d3dr::MODELBLOCK *src) {
	LPDIRECT3DVERTEXBUFFER9 *vb = &(src->vbTex);

	if(!(*vb))
		return;	

	D3DVERTEXBUFFER_DESC desc; ZeroMemory(&desc, sizeof(D3DVERTEXBUFFER_DESC));
	(*vb)->GetDesc(&desc);
	int numTris = desc.Size/sizeof(D3DTEXV)/3;

	// Apply a bit of depth biasing to bring wireframes more visible later on
	if(!(flags&D3DR_NOBIAS)) {
		float db = 0.0001f;
		d3dd->SetRenderState(D3DRS_DEPTHBIAS,  *((DWORD*)(&db)));
	}

	if(flags&D3DR_SOLID || flags&D3DR_DEPTHONLY) {
		// Solid rendering
		d3dd->SetTexture(0, NULL);
		applyRenderStates(state_solid);
		d3dd->SetMaterial(&ambient);

		if(flags&D3DR_DEPTHONLY)
			d3dd->SetRenderState(D3DRS_COLORWRITEENABLE, 0);

		d3dd->SetStreamSource(0, *vb, 0, sizeof(D3DTEXV));
		d3dd->SetFVF(D3DTEXV_VERTEXTYPE);
		d3dd->BeginScene();
		D3DCALL( d3dd->DrawPrimitive(D3DPT_TRIANGLELIST, 0, numTris) );
		d3dd->EndScene();

		d3dd->SetRenderState(D3DRS_COLORWRITEENABLE, 0x0000000F);
	} else if(flags&D3DR_TEXTURE) {
		// Textured rendering, split into iblocks
		applyRenderStates(state_solid);
		d3dd->SetMaterial(&ambient);

		for(DWORD i=0;i < src->texBlocks.size();i++) {
			int beg = src->texBlocks[i].begin;
			int num = src->texBlocks[i].num;			
			d3dd->SetTexture(0, texMgr->getTexture(src->texBlocks[i].tex));

			if(texMgr->isDefaultTexture(src->texBlocks[i].tex))
				setTexFiltering(D3DTEXF_POINT, D3DTEXF_POINT, D3DTEXF_POINT, 1);
			else
				setTexFiltering(D3DTEXF_ANISOTROPIC, D3DTEXF_LINEAR, D3DTEXF_LINEAR, 8);

			d3dd->SetStreamSource(0, (*vb), sizeof(D3DTEXV)*beg*3, sizeof(D3DTEXV));
			d3dd->SetFVF(D3DTEXV_VERTEXTYPE);
			d3dd->BeginScene();
			D3DCALL( d3dd->DrawPrimitive(D3DPT_TRIANGLELIST, 0, num) );
			d3dd->EndScene();
		}
		d3dd->SetTexture(0, 0);
	}

	d3dd->SetRenderState(D3DRS_DEPTHBIAS, 0);
}

void d3drenderer::setLineColor(float r, float g, float b, float a) {
	// Helper to set line color, using lighting. seems to be the only way to do it without vertex color?
	d3dd->SetRenderState(D3DRS_LIGHTING, TRUE);
	ZeroMemory(&linecolor, sizeof(linecolor));
	linecolor.Specular = D3DXCOLOR(r, g, b, a);
	linecolor.Diffuse = D3DXCOLOR(r, g, b, a);
	linecolor.Ambient = D3DXCOLOR(r, g, b, a);
	linecolor.Emissive = D3DXCOLOR(r, g, b, a);
	d3dd->SetMaterial(&linecolor);	
}

void d3drenderer::renderDataVL(DWORD flags, bool isBackground) {
	renderDataVL(flags, isBackground?&bgModel:&baseModel);
}

void d3drenderer::renderDataVL(DWORD flags, d3dr::MODELBLOCK *src) {
	LPDIRECT3DVERTEXBUFFER9 *vb = &(src->vbVrtx);
	LPDIRECT3DINDEXBUFFER9 *ib = &src->ibLines;
	bool isBackground = src->isBackground;	// TODO: fix

	int ibLinesNsel = src->ibLinesNsel;
	int ibLinesNselSharp = src->ibLinesNselSharp;

	if(!(*vb))
		return;	

	D3DVERTEXBUFFER_DESC desc; ZeroMemory(&desc, sizeof(D3DVERTEXBUFFER_DESC));
	(*vb)->GetDesc(&desc);
	int numVrtx = desc.Size/sizeof(D3DBASICV);

	d3dd->LightEnable(0, FALSE);
	d3dd->LightEnable(1, FALSE);

	if(flags&D3DR_WIREFRAME && *ib) {
		D3DINDEXBUFFER_DESC desci; ZeroMemory(&desci, sizeof(D3DINDEXBUFFER_DESC));
		(*ib)->GetDesc(&desci);
		int numLines = desci.Size/4/2;

		// Draw wireframe lines
		applyRenderStates(state_wireframe);
		d3dd->SetTexture(0, NULL);
		d3dd->SetRenderState(D3DRS_ANTIALIASEDLINEENABLE, (flags&D3DR_AALINES)?TRUE:FALSE);

		if(flags&D3DR_DEPTHTEST) {
			d3dd->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);	
			d3dd->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);				 
		}

		d3dd->SetStreamSource(0, *vb, 0, sizeof(D3DBASICV));
		d3dd->SetIndices(*ib);
		d3dd->SetFVF(D3DBASICV_VERTEXTYPE);
		d3dd->BeginScene();

		if(isBackground) {
			// Drawing background LOD
			setLineColor(1, 1, 0.15f, 0.75f);
			D3DCALL( d3dd->DrawIndexedPrimitive(D3DPT_LINELIST, 0, 0, numVrtx, 0, numLines) );
		} else {
			// Draw unselected lines
			if(numLines-ibLinesNsel != 0) {
				setLineColor(0, 0, 0, 1);
				D3DCALL( d3dd->DrawIndexedPrimitive(D3DPT_LINELIST, 0, 0, numVrtx, 0, numLines-ibLinesNsel) );
			}

			// Draw selected lines, smooth then sharp
			if(ibLinesNsel != 0) {
				setLineColor(1, 0, 0, 1);
				D3DCALL( d3dd->DrawIndexedPrimitive(D3DPT_LINELIST, 0, 0, numVrtx, (numLines-ibLinesNsel-ibLinesNselSharp)*2, ibLinesNsel) );
			}
			if(ibLinesNselSharp != 0) {
				setLineColor(0.4f, 0, 0.26f, 1);
				D3DCALL( d3dd->DrawIndexedPrimitive(D3DPT_LINELIST, 0, 0, numVrtx, (numLines-ibLinesNselSharp)*2, ibLinesNselSharp) );
			}
		}

		d3dd->EndScene();
		d3dd->SetIndices(0);
	}

	if(flags&D3DR_VERTICES) {
		// Draw vertices as point sprites
		applyRenderStates(state_points);
		d3dd->SetTexture(0, vrtxMarker);

		float ps = (float)vrtxMarkerSize;
		d3dd->SetRenderState(D3DRS_POINTSIZE, *((DWORD*)&ps));
		d3dd->SetRenderState(D3DRS_POINTSIZE_MIN, *((DWORD*)&ps));
		setTexFiltering(D3DTEXF_POINT, D3DTEXF_POINT, D3DTEXF_POINT, 1);

		d3dd->SetStreamSource(0, *vb, 0, sizeof(D3DBASICV));
		d3dd->SetFVF(D3DBASICV_VERTEXTYPE);
		d3dd->SetPixelShader(vxShader);
		d3dd->BeginScene();
		D3DCALL( d3dd->DrawPrimitive(D3DPT_POINTLIST, 0, numVrtx) );
		d3dd->EndScene();
		d3dd->SetPixelShader(0);
	}

	if(flags&D3DR_MASSVERTICES && vbMass) {
		// Draw mass vertices (vertex size depends on mass)
		D3DVERTEXBUFFER_DESC descm; ZeroMemory(&descm, sizeof(D3DVERTEXBUFFER_DESC));
		vbMass->GetDesc(&descm);
		int numMass = descm.Size/sizeof(D3DSPOINTV);

		applyRenderStates(state_points);
		d3dd->SetTexture(0, massMarker);

		setTexFiltering(D3DTEXF_POINT, D3DTEXF_POINT, D3DTEXF_POINT, 1);

		d3dd->SetStreamSource(0, vbMass, 0, sizeof(D3DSPOINTV));
		d3dd->SetFVF(D3DSPOINTV_VERTEXTYPE);
		d3dd->BeginScene();
		D3DCALL( d3dd->DrawPrimitive(D3DPT_POINTLIST, 0, numMass) );
		d3dd->EndScene();
	}

	d3dd->LightEnable(0, TRUE);
	d3dd->LightEnable(1, TRUE);
}

// Renders background texture maps (background texture mapping tool)
void d3drenderer::renderDataBackgrondTexture(CTextureMapping *tex) {
#ifndef D3DRENDERER_EXE
	if(!tex || tex->hidden)
		return;
	DWORD gray = 0x80808080;
	D3DTEXVPRE data[4] = {
		0,0,0,0, gray, 1,0,
		0,0,0,0, gray, 1,1,
		0,0,0,0, gray, 0,0,
		0,0,0,0, gray, 0,1,
	};	
	const int map[4] = {1, 2, 0, 3};	// re-order for triangle strip rendering
	for(int i=0;i<4;i++) {
		data[i].x = tex->GetPos(map[i]).x();
		data[i].y = tex->GetPos(map[i]).y();	
		tex->GetTuTv(map[i], &data[i].tu, &data[i].tv);
	}
	
	applyRenderStates(state_solid);
	DWORD texi = texMgr->addTexture(tex->textureName);
	d3dd->SetTexture(0, texMgr->getTexture(texi));
	d3dd->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	d3dd->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
	d3dd->SetRenderState(D3DRS_LIGHTING, FALSE);

	d3dd->SetPixelShader(0);
	d3dd->SetFVF(D3DTEXSPEVPRE_VERTEXTYPE);
	d3dd->BeginScene();
	d3dd->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, &data, sizeof(D3DTEXVPRE));
	d3dd->EndScene();

	if(tex->gray) {
		// Draw semitransparent gray box over the texture
		d3dd->SetTexture(0, 0);
		d3dd->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		d3dd->SetFVF(D3DTEXVPRE_VERTEXTYPE);
		d3dd->BeginScene();
		d3dd->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, &data, sizeof(D3DTEXVPRE));
		d3dd->EndScene();
	}
#endif
}

void d3drenderer::rotateWorld(D3DXMATRIX m, float x, float y, float z) {
	static D3DXMATRIX mat;
	D3DXMatrixRotationYawPitchRoll(&mat, D3DXToRadian(x), D3DXToRadian(y), D3DXToRadian(z));
	mat = mat * m;
	d3dd->SetTransform(D3DTS_WORLD, &mat);
}

void d3drenderer::translateWorld(D3DXMATRIX m, float x, float y, float z) {
	static D3DXMATRIX mat;
	D3DXMatrixTranslation(&mat, x, y, z);
	mat = mat * m;
	d3dd->SetTransform(D3DTS_WORLD, &mat);
}

void d3drenderer::renderArrow(DWORD color) {
	// Draw wireframe arrow for gizmo
	DWORD c = color;
	D3DBASICV data[] = {
		 0.0f, 0.0f, 1.0f, c,	 0.0f, 0.0f,-1.0f, c,	// center bar
		 0.0f, 0.0f, 1.0f, c,	-0.1f, 0.0f, 0.9f, c,	// Head...
		 0.0f, 0.0f, 1.0f, c,	 0.1f, 0.0f, 0.9f, c,
		 0.0f, 0.0f, 1.0f, c,	 0.0f,-0.1f, 0.9f, c,
		 0.0f, 0.0f, 1.0f, c,	 0.0f, 0.1f, 0.9f, c,
		-0.1f, 0.0f, 0.9f, c,	 0.1f, 0.0f, 0.9f, c,
		 0.0f,-0.1f, 0.9f, c,	 0.0f, 0.1f, 0.9f, c,
		-0.1f, 0.0f,-1.0f, c,	 0.1f, 0.0f,-1.0f, c,	// Tail...
		 0.0f,-0.1f,-1.0f, c,	 0.0f, 0.1f,-1.0f, c,
	};
	int nump = sizeof(data)/sizeof(D3DBASICV)/2;

	applyRenderStates(state_wireframe);
	d3dd->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
	d3dd->SetRenderState(D3DRS_COLORVERTEX, TRUE);
	d3dd->SetTexture(0, NULL);
	d3dd->SetFVF(D3DGRIDV_VERTEXTYPE);
	d3dd->BeginScene();
	d3dd->DrawPrimitiveUP(D3DPT_LINELIST, nump, &data, sizeof(D3DBASICV));
	d3dd->EndScene();

}

void d3drenderer::renderCircle(DWORD color) {
	// Draw wireframe circle for gizmo
	const int num = 60;
	D3DBASICV data[num*2];
	for(int i=0;i<num;i++) {
		double a = 6.28318531*((double)i/(double)(num-1));
		data[i].x = (float)sin(a); data[i].y = (float)cos(a); data[i].z = 0;
		data[i].col = color;
	}
	int nump = (sizeof(data)/sizeof(D3DBASICV)/2) - 1;

	applyRenderStates(state_wireframe);
	d3dd->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
	d3dd->SetRenderState(D3DRS_COLORVERTEX, TRUE);
	d3dd->SetTexture(0, NULL);
	d3dd->SetFVF(D3DGRIDV_VERTEXTYPE);
	d3dd->BeginScene();
	d3dd->DrawPrimitiveUP(D3DPT_LINESTRIP, nump, &data, sizeof(D3DBASICV));
	d3dd->EndScene();
}

void d3drenderer::renderGizmo(HMATRIX gmat) {
	if(!d3dd)
		return;

	D3DXMATRIX mat;
	hmtodm(gmat, mat);
#ifndef D3DRENDERER_EXE
	if(doc && doc->seltransform && doc->gismo_mode==gismo_selected) {
		D3DXMATRIX matsel;
		hmtodm(doc->seltr, matsel);
		mat *= matsel;
	}	
#endif

	translateWorld(mat, 0, 0, 0);
	rotateWorld(mat, 0, 0, 0);
	renderArrow(0xFF0000D0);
	rotateWorld(mat, 0,-90, 0);
	renderArrow(0xFF00D000);
	rotateWorld(mat, 90, 0, 0);
	renderArrow(0xFFD00000);

	rotateWorld(mat, 0, 0, 0);
	renderCircle(0xFF0000D0);
	translateWorld(mat, 0, 0, 1);
	renderCircle(0xFF0000D0);
	translateWorld(mat, 0, 0, -1);
	renderCircle(0xFF0000D0);
	
	D3DXMATRIX worldmat;
	D3DXMatrixIdentity(&worldmat);
	d3dd->SetTransform(D3DTS_WORLD, &worldmat);
}

// O2 HMATRIX[4] to D3DXMATRIX
void d3dr::hmtodm(HMATRIX gmat, D3DXMATRIX &mat) {
	for(int x=0;x<4;x++)
		for(int y=0;y<4;y++)
			mat.m[x][y] = gmat[x][y];
}

// memcpy with pitch values
void d3dr::memcpyPitched(void* dst, void *src, DWORD size, DWORD pitchDst, DWORD pitchSrc) {
	if(pitchDst == pitchSrc)
		memcpy(dst, src, size); // Well that was easy
	else {
		DWORD cpyPitch = pitchDst<pitchDst?pitchDst:pitchSrc;
		DWORD copied = 0;
		while(true) {
			memcpy(dst, src, cpyPitch);
			dst = ((BYTE*)dst) + pitchDst;
			src = ((BYTE*)src) + pitchSrc;
			copied += cpyPitch;
			if(copied >= size)
				break;
		}
	}
}