//
//	D3D renderer class - renders o2 model data with D3D
//

//#include "stdafx.h"

#ifndef __d3drenderer_h__
#define __d3drenderer_h__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef D3DRENDERER_EXE
// O2 build
#include "../stdafx.h"
#include "../MainFrm.h"
#include "../Objektiv2Doc.h"
#include "../Objektiv2.h"
#include "../TexMapList.h"
#include "../TextureMapping.h"
//#include "../Objektiv2View.h"

#else

// Non-O2 build (p3d view, debug exe, etc.)
#include "stdafx.h"
#define CObjektiv2Doc void*
#define CTextureMapping void*
#include "../../ObjektivLib/Edges.h"
#endif

#include <d3d9.h>
#include <d3dx9.h>
#include <dxerr9.h>
#pragma comment(lib, "d3d9")
#pragma comment(lib, "d3dx9")
#pragma comment(lib, "dxerr9")

#include <vector>

#include "d3dtexmanager.h"

//#define d3drdbg printf
#define d3drdbg RptF
#define D3DCALL(x)	{HRESULT tr = x; if(tr != D3D_OK) d3drdbg("WARNING! D3DCall fail line %d, %s <%s>", __LINE__, __FILE__, DXGetErrorString9(tr));}
#define D3DCALLCR(x)	{HRESULT tr = x; if(tr != D3D_OK) {d3drdbg("ERROR! D3DCall fail line %d, %s <%s>", __LINE__, __FILE__, DXGetErrorString9(tr));return -1;}}
#define SAFE_REL_FINAL(x) if(x){int r=(x)->Release();if(r!=0) d3drdbg("SAFE_REL_FINAL: Memory leak: %s->Release() = %d!", #x, r); else x=NULL;};

#ifdef _DEBUG
#define debugout RptF
#else
#define debugout //
#endif

namespace d3dr {
	// Vertex type for vertex/line data
	#define D3DBASICV_VERTEXTYPE	(D3DFVF_XYZ|D3DFVF_SPECULAR)
	#define D3DGRIDV_VERTEXTYPE		(D3DFVF_XYZ|D3DFVF_DIFFUSE)
	typedef struct {
		float x,y,z;
		DWORD col;
	} D3DBASICV;

	// Vertex type with size, for mass visualisation
	#define D3DSPOINTV_VERTEXTYPE	(D3DFVF_XYZ|D3DFVF_PSIZE)
	typedef struct {
		float x,y,z;
		float size;
	} D3DSPOINTV;

	// Vertex type for textured triangle data
	#define D3DTEXV_VERTEXTYPE	(D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_SPECULAR|D3DFVF_TEX1)
	typedef struct {
		float x,y,z;
		float nx,ny,nz;
		DWORD col;
		float tu, tv;
	} D3DTEXV;

	// Pretransformed vertex type
	#define D3DTEXVPRE_VERTEXTYPE	(D3DFVF_XYZRHW|D3DFVF_DIFFUSE|D3DFVF_TEX1)
	#define D3DTEXSPEVPRE_VERTEXTYPE	(D3DFVF_XYZRHW|D3DFVF_SPECULAR|D3DFVF_TEX1)
	typedef struct {
		float x,y,z,w;
		DWORD col;
		float tu, tv;
	} D3DTEXVPRE;

	typedef struct {
		DWORD state;
		DWORD val;
	} RSTATE;

	// Renderstates for solid/textured rendering
	const static RSTATE state_solid[]= {
		D3DRS_ZENABLE, D3DZB_TRUE,
		D3DRS_FILLMODE, D3DFILL_SOLID,
		D3DRS_LIGHTING, TRUE,
		D3DRS_ALPHATESTENABLE, FALSE,
		D3DRS_CULLMODE, D3DCULL_CW,
		D3DRS_NORMALIZENORMALS, FALSE,
		D3DRS_SHADEMODE, D3DSHADE_GOURAUD,
		D3DRS_ALPHABLENDENABLE, TRUE,
		D3DRS_SRCBLEND, D3DBLEND_SRCALPHA,
		D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA,
		D3DRS_COLORVERTEX, TRUE,
		D3DRS_SPECULARMATERIALSOURCE, D3DMCS_COLOR1,
		D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_COLOR2,
		D3DRS_EMISSIVEMATERIALSOURCE, D3DMCS_COLOR2,

		NULL, NULL,
	};

	// Renderstates for wireframe rendering
	const static RSTATE state_wireframe[]= {
		D3DRS_ZENABLE, D3DZB_FALSE,
		D3DRS_FILLMODE, D3DFILL_WIREFRAME,
		D3DRS_LIGHTING, FALSE,
		D3DRS_SRCBLEND, D3DBLEND_SRCALPHA,
		D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA,
		D3DRS_ALPHATESTENABLE, FALSE,
		D3DRS_ANTIALIASEDLINEENABLE, TRUE,
		D3DRS_ALPHABLENDENABLE, TRUE,
		D3DRS_SHADEMODE, D3DSHADE_FLAT,
		D3DRS_COLORVERTEX, FALSE,
		NULL, NULL,
	};

	// Renderstate for points rendering (point sprites)
	const static RSTATE state_points[]= {
		D3DRS_ZENABLE, D3DZB_FALSE,
		D3DRS_FILLMODE, D3DFILL_WIREFRAME,
		D3DRS_POINTSPRITEENABLE, TRUE,
		D3DRS_LIGHTING, FALSE,
		D3DRS_ALPHABLENDENABLE, TRUE,
		D3DRS_SRCBLEND, D3DBLEND_SRCALPHA,
		D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA,
		D3DRS_ALPHABLENDENABLE, TRUE,
		D3DRS_ALPHATESTENABLE, TRUE,
		D3DRS_ALPHAREF, 0x000000F0,
		D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL,
		D3DRS_COLORVERTEX, FALSE,
		D3DRS_LIGHTING, FALSE,
		NULL, NULL,
	};

	// Simple pixel shader for coloring vertex point sprites
	// Changes the border pixel's alpha in such a manner it will pass alpha test if colored (for selections)
	const static char vertexPixelShader[] = 
			"ps.1.1\n"
			"tex t0\n"
			"mul r0.rgb, t0, v1\n"
			"+add r0.a, t0, v1\n"			
	;

	// Texture data for vertex marker (7x7 pixel cross with outline)
	#define AB	0xFF, 0xFF, 0xFF, 0x7F	// 0x7f = barely fails alpha test, modified by ps
	#define TB	0x00, 0x00, 0x00, 0x00
	#define BB	0x00, 0x00, 0x00, 0xEF

	const static BYTE vrtxMarkerData[]= {
		AB, AB, AB, AB, AB, AB, AB, 
		AB, TB, TB, BB, TB, TB, AB,
		AB, TB, TB, BB, TB, TB, AB,
		AB, BB, BB, BB, BB, BB, AB,
		AB, TB, TB, BB, TB, TB, AB,
		AB, TB, TB, BB, TB, TB, AB,
		AB, AB, AB, AB, AB, AB, AB, 
	};
	const static int vrtxMarkerSize = 7;
	#undef TB
	#undef BB
	#undef AB

	// Mass vertex marker data
	#define GB	0x66, 0x66, 0x66, 0xFF
	#define RB	0x33, 0x33, 0xFF, 0xFF
	const static BYTE massMarkerData[]= {
		RB, RB, RB, RB, RB,
		RB, GB, GB, GB, RB,
		RB, GB, GB, GB, RB,
		RB, GB, GB, GB, RB,
		RB, RB, RB, RB, RB,
	};
	const static int massMarkerSize = 5;
	#undef GB
	#undef RB

	#define D3DR_WIREFRAME		0x01	// Render wireframe
	#define D3DR_BACKWIRE		0x02	// Render back facing faces as wireframe
	#define D3DR_VERTICES		0x04	// Render vertices
	#define D3DR_TEXTURE		0x08	// Render with textures
	#define D3DR_SOLID			0x10	// Render solid
	#define D3DR_AALINES		0x20	// Use antialiasing for lines
	#define D3DR_MASSVERTICES	0x40	// Draw mass vertices
	#define D3DR_DEPTHONLY		0x80	// Draw to depth buffer only
	#define D3DR_DEPTHTEST		0x100	// Use depth testing for wireframe
	#define D3DR_NOBIAS			0x200	// No depth biasing

	typedef struct {
		DWORD tex;	// d3dtexmanager texture ID
		DWORD begin;	// First index buffer element
		DWORD num;		// Number of elements
	} IDXBLOCK;

	void memcpyPitched(void* dst, void *src, DWORD size, DWORD pitchDst, DWORD pitchSrc);
	void hmtodm(HMATRIX gmat, D3DXMATRIX &mat);

	class MODELBLOCK {
	public:
		void release() {
			SAFE_REL_FINAL(vbVrtx);	
			SAFE_REL_FINAL(ibLines);	
			SAFE_REL_FINAL(vbTex);	
			SAFE_REL_FINAL(vbNormal);	
			SAFE_REL_FINAL(vbMass);	
		};

		LPDIRECT3DVERTEXBUFFER9	vbVrtx;		// Contains raw vertice data
		LPDIRECT3DINDEXBUFFER9	ibLines;	// Indexbuffer for rendering lines
		DWORD ibLinesNsel, ibLinesNselSharp;
		LPDIRECT3DVERTEXBUFFER9	vbTex;		// Textured vertex data
		LPDIRECT3DVERTEXBUFFER9	vbNormal;	// Normal data
		LPDIRECT3DVERTEXBUFFER9	vbMass;		// Mass data (vertices with size)
		vector<d3dr::IDXBLOCK>	texBlocks;	// vbTex per-material blocks
		float modelSize, modelHeight;
		bool isBackground;
	};

	typedef struct {
		float pos[3];	// Position
		float up[3];	// Up vector
		float dir[3];	// Direction vector
		int pidx;	// Index to proxyModels vector
	} D3DPROXY;
}

class d3dproxy {
public:
	d3dproxy(char *name, char *pathdir);
	~d3dproxy();

	bool loadModel(char *name, char *pathdir);
	const char* getName() {return name;};
	d3dr::MODELBLOCK* getModelData() {return &modelData;};
	bool isValid() {return valid;};
	bool MatchName(const char *tname);
private:
	bool valid;
	char name[256];
	d3dr::MODELBLOCK modelData;	// Contains vertex buffers etc. for rendering
};

class d3drenderer {
public:
	static int createDataVL(ObjectData &obj, d3dr::MODELBLOCK &dst);
	static int createDataTX(ObjectData &obj, d3dr::MODELBLOCK &dst, bool withTextures);

public:
	d3drenderer();
	~d3drenderer();
	int initD3D(HWND win, DWORD antialias=0, bool withSC=false);		// Initializes direct3D
	void init();
	void destroy();
	bool deviceOK();
	void presentWin(HWND win);	// Dump bb contents to window
	void* getBufferData(int *pitch, int *height);
	int setRenderSize(int width, int height);
	void releaseBufferData();
	void positionCamera(float x, float y, float z, float rotx, float roty, float dist);
	void positionCameraPerspective(float x, float y, float z, float rotx, float roty, float dist, float fovRad);
	void positionCameraOrtho(float x, float y, float z, float rotx, float roty, float dist, float scale);
	void positionLight(void);
	int createDataM(ObjectData &obj);
	int createDataN(ObjectData &obj);
	int createDataVL(ObjectData &obj, bool isBackground=false, bool noHide=false);	
	int createDataTX(ObjectData &obj, bool withTextures, bool noHide=false);
	void renderGrid(float rotx=0, float roty=0);
	void renderDataN(DWORD flags);				// Draw vertex normals
	void renderDataVL(DWORD flags, bool isBackground=false);		
	void renderDataVL(DWORD flags, d3dr::MODELBLOCK *src); // Draw line/point data
	void renderDataTX(DWORD flags);
	void renderDataTX(DWORD flags, d3dr::MODELBLOCK *src);		// Draw solid/textured data
	void renderDataProxies(DWORD flags);
	void renderDataBackgrondTexture(CTextureMapping *tex);
	void clear(DWORD color=0x00000000);
	void setDoc(CObjektiv2Doc *odoc);
	void setTexturePath(char *path, bool doFlush=false);
	void setProxyPath(char *path) {strncpy(proxyPath, path, 256);};
	void saveModelScreenshot(char *path);
	void setLineColor(float r, float g, float b, float a);
	void renderGizmo(HMATRIX gmat);
	unsigned long long getModelChecksum(ObjectData &obj);
	float getModelSize() {return baseModel.modelSize;};
	float getModelHeight() {return baseModel.modelHeight;};
	void loadProxies(ObjectData &obj);

	void texPrepareFlush() {if(texMgr) texMgr->prepareFlush();};
	void texFlushUnused() {if(texMgr) texMgr->flushUnused();};

private:
	void translateWorld(D3DXMATRIX m, float x, float y, float z);
	void rotateWorld(D3DXMATRIX m, float x, float y, float z);
	static PosT getTransformedPoint(ObjectData &obj, int i);
	void renderArrow(DWORD color);
	void renderCircle(DWORD color);
	void applyRenderStates(const d3dr::RSTATE *st);
	static void objFaceToD3DTexv(d3dr::D3DTEXV *odata, ObjectData &obj, int face, int p);
	void setTexFiltering(DWORD min, DWORD mip, DWORD mag, DWORD ani, DWORD stage=0);

private:
	static LPDIRECT3D9 d3d;		// Direct3D handle
	static LPDIRECT3DDEVICE9 d3dd;	// Device handle
	LPDIRECT3DSWAPCHAIN9 d3dsc;	// Swap chain handle
	D3DPRESENT_PARAMETERS pp;
	D3DMULTISAMPLE_TYPE multisampling;
	//CObjektiv2Doc *doc;		// Invalid in exe build!	// TODO: restore, needs compatiblity for static functions
	D3DCAPS9	caps;

	vector<d3dproxy*> proxyModels;
	vector<d3dr::D3DPROXY>  proxies;
	char proxyPath[256];

private:

	LPDIRECT3DSURFACE9 depth, bb;	// Depth- & backbuffers (rsSize width&height)
	LPDIRECT3DSURFACE9 devbb, membb;	// Device backbuffer, memory buffer for GetRenderTargetData
	int rsSize;		// Render surface width & height

	LPDIRECT3DSURFACE9 vpBuf;	// Render viewport data texture
	int rWidth, rHeight;	// Current render viewport size
	float rAspect;
	bool useSwapchain;		// If true, use d3d swapchain (else using custom RT with not present capability)

	LPDIRECT3DTEXTURE9 vrtxMarker, massMarker;

	d3dr::MODELBLOCK baseModel;	// Base model data
	d3dr::MODELBLOCK bgModel;	// Background model data

	LPDIRECT3DVERTEXBUFFER9	vbGrid;		// Contains grid lines
	//LPDIRECT3DVERTEXBUFFER9	vbVrtx;		// Contains raw vertice data
	//LPDIRECT3DINDEXBUFFER9	ibLines;	// Indexbuffer for rendering lines
	//DWORD ibLinesNsel, ibLinesNselSharp;
	//LPDIRECT3DVERTEXBUFFER9	vbTex;		// Textured vertex data
	LPDIRECT3DVERTEXBUFFER9	vbNormal;	// Normal data
	LPDIRECT3DVERTEXBUFFER9	vbMass;		// Mass data (vertices with size)
	//vector<d3dr::IDXBLOCK>	texBlocks;	// vbTex per-material blocks
	static d3dtexmanager	*texMgr;

	LPDIRECT3DVERTEXBUFFER9	vbBgrnd;	// Background lod data
	LPDIRECT3DINDEXBUFFER9	ibBgrnd;	// Background lod data

	//float modelSize, modelHeight;

	LPDIRECT3DPIXELSHADER9 vxShader;
	LPD3DXBUFFER vxShaderCode;
	D3DMATERIAL9 ambient, linecolor;

	//HWND win32createWindow();

private:
	const static int gridSize = 20;
	const static DWORD gridColor = 0xe0777777;
	const static DWORD gridColorCntr = 0xe0660000;
	const static DWORD normalColor = 0xFF0000ff;
	const static DWORD normalColorEnd = 0x800055ff;
};

#endif