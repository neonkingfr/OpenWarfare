#if !defined(AFX_ALLRENAMEDLG_H__FCB6D42D_D056_4EB0_A48D_FA2963A9411F__INCLUDED_)
#define AFX_ALLRENAMEDLG_H__FCB6D42D_D056_4EB0_A48D_FA2963A9411F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AllRenameDlg.h : header file
//

#include "comint.h"
/////////////////////////////////////////////////////////////////////////////
// CAllRenameDlg dialog

namespace ObjektivLib {
    class LODObject;
}
class CAllRenameDlg : public CDialog
{
// Construction
    CWinThread *_build;
    bool _signend;
    CSize _minsize;
    CSize _lastsize;
public:
	void StartBuildList();
	void StopBuildList();
	void ScanWholeLod();
	void ScanTexturesAndMaterials(ObjectData *obj);
	CAllRenameDlg(CWnd* pParent = NULL);   // standard constructor

    LODObject *iCurLod;
// Dialog Data
	//{{AFX_DATA(CAllRenameDlg)
	enum { IDD = IDD_ALLTEXTURES };
	CListBox	wTextureList;
	CEdit	wOldName;
	CEdit	wNewName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAllRenameDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAllRenameDlg)
	virtual void OnCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	afx_msg void OnSelchangeTexturelist();
	virtual void OnOK();
	afx_msg void OnUndo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CIMassRenameTextures: public CICommand
  {
  protected:
    RString _oldname;
    RString _newname;
    bool _allLods;
  public:
    CIMassRenameTextures(const char *oldname, const char *newname, bool allLods=true);
    int Execute(LODObject *lod);
    void RenameInOneLod(ObjectData *obj);
  
  };

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ALLRENAMEDLG_H__FCB6D42D_D056_4EB0_A48D_FA2963A9411F__INCLUDED_)
