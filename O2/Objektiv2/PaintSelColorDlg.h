#if !defined(AFX_PAINTSELCOLORDLG_H__378122BF_DB87_4A4F_910F_AEDC3E06AC6F__INCLUDED_)
#define AFX_PAINTSELCOLORDLG_H__378122BF_DB87_4A4F_910F_AEDC3E06AC6F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PaintSelColorDlg.h : header file
//

#include "ColorSlider.h"

/////////////////////////////////////////////////////////////////////////////
// CPaintSelColorDlg dialog

class CPaintSelColorDlg : public CDialog
  {
  // Construction
  public:
    virtual void OnOK();
    CPaintSelColorDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CPaintSelColorDlg)
    enum 
      { IDD = IDD_PAINTSELCOLOR };
    CSliderCtrl	wStrength;
    CColorSlider wColorSlider;
    CSliderCtrl	wMaxSize;
    CSliderCtrl	wMinSize;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CPaintSelColorDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    virtual void OnCancel();
    
    // Generated message map functions
    //{{AFX_MSG(CPaintSelColorDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnTimer(UINT nIDEvent);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PAINTSELCOLORDLG_H__378122BF_DB87_4A4F_910F_AEDC3E06AC6F__INCLUDED_)
