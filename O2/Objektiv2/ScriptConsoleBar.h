#pragma once


// CScriptConsoleBar

#include <afxmt.h>

class CScriptConsoleBar : public CDialogBar
{
	DECLARE_DYNAMIC(CScriptConsoleBar)
    HANDLE _pipeIn,_pipeOut;
    HANDLE _stdIn,_stdOut;
    CEdit _consoleWindow;  
    int _lastOutput;
    LOGFONT textfont;
    CFont curfont;
    bool _inputEnabled;
    CCriticalSection _writePipe;
    CString _writeText;

public:
    enum {IDD=IDD_CONSOLEBAR};

	CScriptConsoleBar();
	virtual ~CScriptConsoleBar();

    CDIALOGBAR_RESIZEABLE
    void Create(CWnd *parent, int menuid);

    void AttachIO();
    void DetachIO();

protected:
	DECLARE_MESSAGE_MAP()
  static UINT CALLBACK ConsoleReadThread(LPVOID self);
  static UINT CALLBACK ConsoleWriteThread(LPVOID self);
public:
  afx_msg void OnSize(UINT nType, int cx, int cy);
  virtual BOOL PreTranslateMessage(MSG* pMsg);
  afx_msg void OnDestroy();
  afx_msg LRESULT OnIncomeData(WPARAM wParam, LPARAM lParam);

  const LOGFONT& GetTextFont() {return textfont;}
  void SetTextFont(const LOGFONT &font);

  void EnableInput(bool enable);
};

