// 3DC2.cpp: implementation of the C3DC2 class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Objektiv2.h"
#include "3DC2.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

C3DC2::C3DC2()
{
  vlist=NULL;
  nvl=0;
}

C3DC2::~C3DC2()
{
  delete [] vlist;
}

void C3DC2::AllocVList(int vl)
{
  if (vl>nvl || vl<nvl/2)
  {
    delete [] vlist;
    vlist=new Vertex[vl];
    nvl=vl;
  }
}

void C3DC2::SetVertex(int idx, HVECTOR vect, CPen *pen, CBrush *brush)
{
  ASSERT(idx>=0 && idx<nvl);
  Vertex *vx=vlist+idx;
  TransformVector(transform,vect,vx->vect);
  XYZW2XYZQ(vx->vect);
  vx->pen=pen;
  vx->brush=brush;
  //GetClipFlags(vx->vext, RECT& rc)
}

void C3DC2::SetPointStyle(int idx, char style, char size)
{
  ASSERT(idx>=0 && idx<nvl);
  Vertex *vx=vlist+idx;
  vx->pointstyle=style;
  vx->pointsize=size;
}

void C3DC2::DrawAllPoints(int pcount)
{
  for (int i=0;i<pcount;i++) 
  {
    Vertex *vx=vlist+i;
    if (vx->pointsize!=0) 
    {
      if (vx->pen) SelectObject(vx->pen);
      if (vx->brush) SelectObject(vx->brush);
      CopyVektor(oldpos,vx->vect);
      Point3D(NULL,vx->pointstyle,vx->pointsize);
    }
  }
}

void C3DC2::Line3D(int from, int to)
{
  ASSERT(from>=0 && from<nvl);
  ASSERT(to>=0 && to<nvl);
  Line3DClipped(vlist[from].vect,vlist[to].vect);
}

bool C3DC2::GetFaceVisible(int pt1, int pt2, int pt3)
{
  ASSERT(pt1>=0 && pt1<nvl);
  ASSERT(pt2>=0 && pt2<nvl);
  ASSERT(pt3>=0 && pt3<nvl);
  if (curcull==0) return true;
  return GetCull(vlist[pt1].vect, vlist[pt2].vect, vlist[pt3].vect)!=curcull;
}

CFaceVisList::CFaceVisList()
{
  facevis=NULL;
  nfaces=0;
}

CFaceVisList::~CFaceVisList()
{
  delete [] facevis;  
}

void CFaceVisList::Alloc(int nfc)
{
  int nnfc=(nfc+7)>>3;
  if (nfaces<nnfc || nfaces>nnfc/2) 
  {
    delete [] facevis;
    facevis=new unsigned char[nnfc];
    nfaces=nnfc;
  }
}

void CFaceVisList::SetVisibility(int face, bool vis)
{
  ASSERT(face>=0 && face/8<nfaces);
  if (vis) facevis[face>>3]|=(1<<(face & 7));
  else
    facevis[face>>3]&=~(1<<(face & 7));
}

bool CFaceVisList::GetVisibility(int face)
{
  ASSERT(face>=0 && face/8<nfaces);
  return (facevis[face>>3] & (1<<(face & 7)))!=0;
}

void C3DC2::Attach(C3DC &dc)
{
  C3DC2 *copy=(C3DC2 *)&dc;
  cliprect=copy->cliprect;
  curcull=copy->curcull;
  flags=copy->flags;
  oldbitmap=copy->oldbitmap;
  oldbrush=copy->oldbrush;
  oldfont=copy->oldfont;
  oldpen=copy->oldpen;
  CopyVektor(oldpos,copy->oldpos);
  CopyVektor(oldpos2,copy->oldpos2);
  CopyMatice(transform,copy->transform);
  CDC::Attach(dc);
}

bool C3DC2::GetScreenCoords(int index, POINT &pt)
{
  if (vlist[index].vect[ZVAL]<0||vlist[index].vect[WVAL]<0) return false;
  pt.x=ToInt(vlist[index].vect[XVAL]);
  pt.y=ToInt(vlist[index].vect[YVAL]);
  return pt.x>-32000 && pt.x<32000 && pt.y>-32000 && pt.y<32000;  
}

