// DlgRunScript.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "DlgRunScript.h"
#include <el/evaluator/ScriptDebuggerBase.h>
#include ".\dlgrunscript.h"
#include "MainFrm.h"



// DlgRunScript dialog

IMPLEMENT_DYNAMIC(DlgRunScript, CDialog)
DlgRunScript::DlgRunScript(CWnd* pParent /*=NULL*/)
	: CDialog(DlgRunScript::IDD, pParent)
{
}

DlgRunScript::~DlgRunScript()
{
}

void DlgRunScript::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(DlgRunScript, CDialog)  
  ON_WM_TIMER()
  ON_WM_DESTROY()
  ON_BN_CLICKED(IDC_UNLOCK, OnBnClickedUnlock)
END_MESSAGE_MAP()



// DlgRunScript message handlers

BOOL DlgRunScript::OnInitDialog()
{
  CDialog::OnInitDialog();

  unlock=NULL;
  OnUnlockConsole();

  SetTimer(1,1,NULL);
  return FALSE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}


void DlgRunScript::OnTimer(UINT nIDEvent)
{
    if (nIDEvent==1) ResumeThread(waitHandle);
    if (WaitForSingleObject(waitHandle,0)!=WAIT_TIMEOUT) 
    {
      EndDialog(IDOK);
    }
    GetDlgItem(IDOK)->EnableWindow(debugger?(debugger->GetTraceMode()!=debugger->traceBreakNext):FALSE);
    KillTimer(nIDEvent);
    SetTimer(nIDEvent+1,nIDEvent+1,NULL);        
}


void DlgRunScript::OnCancel()
{
  ScriptDebuggerBase::TraceMode mode=debugger->GetTraceMode();
  debugger->SetTraceMode(debugger->traceTerminate);
  GetDlgItem(IDOK)->EnableWindow(TRUE);
  CWaitCursor wait;
  int p=WaitForSingleObject(waitHandle,10000);
  if (p==WAIT_TIMEOUT && AfxMessageBox(IDS_TERMINATEASK,MB_YESNO|MB_ICONEXCLAMATION)==IDYES)
  {
    wait.Restore();
    int p=WaitForSingleObject(waitHandle,2000);
    if (p==WAIT_TIMEOUT) TerminateThread(waitHandle,-1);    
  }
}

void DlgRunScript::OnOK()
{
  debugger->SetTraceMode(debugger->traceBreakNext);
  GetDlgItem(IDOK)->EnableWindow(FALSE);
}



void DlgRunScript::OnDestroy()
{
  if (!mainfrm->_scriptConsole.IsFloating())
  {
    mainfrm->EnableWindowSpecial(&mainfrm->_scriptConsole,TRUE);
  }
  else if (unlock)
  {
    if (unlock->GetNextWindow(GW_CHILD))
      mainfrm->EnableWindowSpecial(unlock->GetNextWindow(GW_CHILD),TRUE);
    else
      mainfrm->EnableWindowSpecial(unlock,TRUE);
  }
  CDialog::OnDestroy();

}

void DlgRunScript::OnUnlockConsole()
{
  if (mainfrm->_scriptConsole.IsFloating())
  {    
    mainfrm->_scriptConsole.GetParent()->GetParent()->EnableWindow(TRUE);
  } 
  else
  {
    if (unlock)
    {
      if (unlock->GetNextWindow(GW_CHILD))
        mainfrm->EnableWindowSpecial(unlock->GetNextWindow(GW_CHILD),TRUE);
      else
        mainfrm->EnableWindowSpecial(unlock,TRUE);
    }
    mainfrm->EnableWindow(TRUE);
    mainfrm->EnableWindowSpecial(&mainfrm->_scriptConsole,FALSE);
    unlock=mainfrm->_scriptConsole.GetParent();
  }  
}

void DlgRunScript::OnBnClickedUnlock()
{
  mainfrm->ShowControlBar(&mainfrm->_scriptConsole,TRUE,FALSE);
  OnUnlockConsole();
}
