#if !defined(AFX_TIMERANGEDLG_H__B299AE9D_AD2E_41F3_89AF_9DD2A328CEE0__INCLUDED_)
#define AFX_TIMERANGEDLG_H__B299AE9D_AD2E_41F3_89AF_9DD2A328CEE0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TimeRangeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTimeRangeDlg dialog

class CTimeRangeDlg : public CDialog
  {
  // Construction
  public:
    CTimeRangeDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CTimeRangeDlg)
    enum 
      { IDD = IDD_TIMERANGE };
    float	timerange;
    BOOL	loop;
    BOOL	interpolate;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CTimeRangeDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CTimeRangeDlg)
    // NOTE: the ClassWizard will add member functions here
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TIMERANGEDLG_H__B299AE9D_AD2E_41F3_89AF_9DD2A328CEE0__INCLUDED_)
