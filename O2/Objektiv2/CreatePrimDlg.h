#if !defined(AFX_CREATEPRIMDLG_H__81072987_EE17_4768_AB5F_ED2B33D4ECB6__INCLUDED_)
#define AFX_CREATEPRIMDLG_H__81072987_EE17_4768_AB5F_ED2B33D4ECB6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CreatePrimDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCreatePrimDlg dialog

class CCreatePrimDlg : public CDialog
  {
  struct CreatePointStruct
    {
    ObjectData *odata;
    float x,y,z;
    LPHMATRIX mm;
    };
  
  // Construction
  public:
    void CreateGeoSpehere(ObjectData *odata, HMATRIX mm);
    void Save(ostream &str);
    void Load(istream &str);
    void CreateCylinder(ObjectData *odata, HMATRIX pinmatrix);
    void CreatePrimitive(ObjectData *odata,HMATRIX pinmatrix);
    int CreatePoint(CreatePointStruct *cp);
    CCreatePrimDlg(CWnd* pParent = NULL, int idd=IDD);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CCreatePrimDlg)
    enum 
      { IDD = IDD_CREATE_PRIMITIVE };
    CButton	wndCenter;
    CStatic	stdr;
    CStatic	stx;
    CEdit	wndSizeZ;
    CEdit	wndSizeY;
    CEdit	wndSizeX;
    CEdit	wndRadius;
    CEdit	wndNumZ;
    CEdit	wndNumY;
    CEdit	wndNumX;
    UINT	numX;
    UINT	numY;
    UINT	numZ;
    float	radius;
    float	sizeX;
    float	sizeY;
    float	sizeZ;
    int		align;
    BOOL	center;
    //}}AFX_DATA
    
    int	create_type; //ID number of type of creating primitive
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CCreatePrimDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    void CreateCircle2(ObjectData *odata, HMATRIX mm, bool back);
    void CreateSpehere(ObjectData *odata, HMATRIX mm);
    void CreatePlane(ObjectData *odata, HMATRIX mm, bool back=false);
    void CreateBox(ObjectData *odata, HMATRIX mm);
    void CreateCircle(ObjectData *odata, HMATRIX pinmatrix, bool back);
    // Generated message map functions
    //{{AFX_MSG(CCreatePrimDlg)
    virtual BOOL OnInitDialog();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CREATEPRIMDLG_H__81072987_EE17_4768_AB5F_ED2B33D4ECB6__INCLUDED_)
