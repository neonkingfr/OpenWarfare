#pragma once


// CStatusLight

class CStatusLight : public CStatic
{
	DECLARE_DYNAMIC(CStatusLight)
    int _statusCurrent;
   CBitmap _statusImages;

public:
	CStatusLight();
	virtual ~CStatusLight();

  static const int statusReady=0;
  static const int statusPreparing=1;
  static const int statusUpdating=2;
  static const int statusNA=3;
  void DrawStatusIcon(CDC *pDC=NULL);


  void SetStatus(int status) 
  {
    if (status!=_statusCurrent)
    {
      _statusCurrent=status;Invalidate();UpdateWindow();
    }
  }
protected:
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnPaint();
};


