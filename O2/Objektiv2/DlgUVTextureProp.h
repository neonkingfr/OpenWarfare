#pragma once
#include "afxcmn.h"


// DlgUVTextureProp dialog

class IDlgUVTexturePropNotify
{
public:
  virtual int GetBrightness() const=0;
  virtual int GetContrast() const=0;
  virtual bool GetBlackOnWhite() const=0;
  virtual void BrightnessChanged(int newval)=0;
  virtual void ContrastChanged(int newval)=0;
  virtual void BrightnessContrastReset()=0;
  virtual void BlackOnWhiteChanged(bool newval)=0;


};

class DlgUVTextureProp : public CDialog
{
	DECLARE_DYNAMIC(DlgUVTextureProp)

  IDlgUVTexturePropNotify *_notify;
public:
	DlgUVTextureProp(IDlgUVTexturePropNotify *notify, CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgUVTextureProp();

// Dialog Data
	enum { IDD = IDD_UVTEXTUREPROP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CSliderCtrl wContrast;
  CSliderCtrl wBrightness;
protected:
  virtual BOOL OnInitDialog();
public:
  afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
protected:
  virtual void OnOK();
  virtual void OnCancel();
public:
  afx_msg void OnBnClickedReset();
  afx_msg void OnBnClickedBlackonwhte();
};
