#if !defined(AFX_UNLOCKSOFTDLG_H__999A4315_1FCF_4EC6_A3D6_7622A776DDFA__INCLUDED_)
#define AFX_UNLOCKSOFTDLG_H__999A4315_1FCF_4EC6_A3D6_7622A776DDFA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UnlockSoftDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUnlockSoftDlg dialog

class CUnlockSoftDlg : public CDialog
  {
  // Construction
  public:
    void OnOK();
    CUnlockSoftDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CUnlockSoftDlg)
    enum 
      { IDD = IDD_UNLOCKSOFT };
    LONG	codein2;
    LONG	codein1;
    CString	codeout;
    long	codein3;
    long	codein4;
    //}}AFX_DATA
    
    CSize orgsize;
    LONG regcode;
    
    bool timeexpand;
    bool forceextend;
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CUnlockSoftDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CUnlockSoftDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnUnlock();
    afx_msg void OnUnlocknow();
    virtual void OnCancel();
    afx_msg void OnVisitweb();
    afx_msg void OnOrder();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UNLOCKSOFTDLG_H__999A4315_1FCF_4EC6_A3D6_7622A776DDFA__INCLUDED_)
