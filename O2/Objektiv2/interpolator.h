// Interpolator.h: interface for the CInterpolator class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INTERPOLATOR_H__49434C57_D073_481E_B698_C35D8B9FB989__INCLUDED_)
#define AFX_INTERPOLATOR_H__49434C57_D073_481E_B698_C35D8B9FB989__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CInterpolator  
  {
  int begin,end;
  int steps;
  int diff;
  int akumulator;
  int direction;
  int pos;
  public:
    void Reset();
    int GetNextValue();
    int GetValue() 
      {return pos;}
    int GetBegin() 
      {return begin;}
    int GetEnd() 
      {return end;}
    int GetSteps() 
      {return steps;}
    CInterpolator(int b,int e,int s);
    
    
  };

//--------------------------------------------------

#endif // !defined(AFX_INTERPOLATOR_H__49434C57_D073_481E_B698_C35D8B9FB989__INCLUDED_)
