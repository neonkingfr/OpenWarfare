#include "StdAfx.h"
#include "PluginServices.h"
#include "SceneLoad.h"
#include "P3DImporter.h"
#include "IFBXPluginServices.h"
#include <sstream>
#include "FBXFileFeaturesDetector.h"
#include <el/ProgressBar/ProgressBar.h>
#include <projects/objektivlib/ObjToolClipboard.h>
#include <projects/objektivlib/BiTXTImportDefault.h>
#include "RVMATExplorer.h"
#include "hlpfuncts.h"
#include "P3DExporter.h"


using namespace FbxSdk;


class SceneLoadReportError: public SceneLoad
{
    IFBXImportControl *report;
public:
    SceneLoadReportError(IFBXImportControl *report):report(report) {}

    virtual void ImportError(MImporter &importer, const char* pFilename) {
        report->ImportError(importer->GetLastErrorID(),importer->GetLastErrorString(), pFilename);
    }
    virtual void InitializeError(MImporter &importer, const char* pFilename) {
        report->ImportInitError(importer->GetLastErrorID(),importer->GetLastErrorString(), pFilename);
    }
    virtual const char *AskPassword(char *buffer, std::size_t sz) {
        return report->AskPassword(buffer,sz);
    }
};

void ImportAnimation(LODObject &p3d, IFBXImportControl *control, const MScene& scene, FbxSdk::MManager &manager) ;

void PluginServices::ImportFBX(const char *fbxfilename, IFBXImportControl *control) {

    MManager manager;
    MScene scene (manager);

    SceneLoadReportError loader(control);
    loader.LoadScene(manager,scene,fbxfilename);

    FBXFileFeaturesDetector detector(control);
    detector.TakeList(scene);
    KTime kt(0);
    detector.WalkScene(scene,kt);

    if (control->OpenConfigurationDialog() == false)
        return;

    char *take = const_cast<char *>(control->GetChoosenTake());
    if (take) scene->SetCurrentTake(take);
    //P3DImporter importer(control);
	P3DImporter importer(control, manager);
    importer.WalkScene(scene,kt);
    importer.CleanupP3D();
    const LODObject &p3d = importer.GetP3D();

    if (control->GetImportMask() & control->importAnimation)
        ImportAnimation(const_cast<LODObject &>(p3d), control, scene, manager);

    if (control->StoreResult(p3d) == false) {

        std::ostringstream str;
        p3d.Save(str,OBJDATA_LATESTVERSION,false);
        std::string outstr = str.str();
        control->StoreResult(outstr.data(), (unsigned int)outstr.size());
    }
}
static void AddObjectFrame(ObjectData &trg, const ObjectData &src, double time) {

    AnimationPhase phs(&trg);
    phs.Validate();
    for (int i = 0; i < src.NPoints(); i++) {
        phs[i] = src.Point(i);
    }
    phs.SetTime((float)time);
    trg.AddAnimation(phs);

}

static void AddP3DFrame(LODObject &trg, const LODObject &src, double time) {

    for (int i = 0; i<src.NLevels(); i++) {

        float level = src.Resolution(i);
        int tlevel = trg.FindLevelExact(level);
        if (tlevel != -1) {

            ObjectData *trgobj = trg.Level(tlevel);
            ObjectData *srcobj = src.Level(i);

            if (trgobj->NPoints() == srcobj->NPoints())
                AddObjectFrame(*trgobj,*srcobj,time);

        }

    }

}

void ImportAnimation(LODObject &p3d, IFBXImportControl *control, const MScene& scene, FbxSdk::MManager &manager) {

    KFbxTakeInfo *nfo = scene->GetTakeInfo(scene->GetCurrentTakeName());
    if (nfo == 0)
        return;
    double begin = nfo->mLocalTimeSpan.GetStart().GetSecondDouble();
    double end = nfo->mLocalTimeSpan.GetStop().GetSecondDouble();
    double cur = begin;
    int speed = control->GetAnimationSpeed();
    if (speed < 0) {
        control->InvalidFPS(speed);
        return;
    }

    ProgressBar<double> pb(end - begin);
    pb.ReportState(-10);
    double fps = 1.0/(double)speed;
    while (cur < end && !pb.StopSignaled()) {

        double pos = (cur - begin)/(end - begin);        

        pb.AdvanceNext(fps);

        KTime tm;
        tm.SetSecondDouble(cur);
        P3DImporter importer(control, manager);
        importer.WalkScene(scene,tm);
        importer.CleanupP3D();
        const LODObject &framep3d = importer.GetP3D();

        AddP3DFrame(p3d,framep3d, pos);        

        cur += fps;

    }

}

bool SaveScene(KFbxSdkManager* pSdkManager, KFbxScene* pScene, const char* pFilename,IFBXExportControl *control)
{
//    int lMajor, lMinor, lRevision;
    bool lStatus = true;

    // Create an exporter.
    KFbxExporter* lExporter = KFbxExporter::Create(pSdkManager, MakeUniqueName());

//    KFbxIO::GetCurrentVersion(lMajor, lMinor, lRevision);

    int pFileFormat = pSdkManager->GetIOPluginRegistry()->GetNativeWriterFormat();

    if (control->ASCIIFormat()) {
        //Try to export in ASCII if possible
        int lFormatIndex, lFormatCount = pSdkManager->GetIOPluginRegistry()->GetWriterFormatCount();

        for (lFormatIndex=0; lFormatIndex<lFormatCount; lFormatIndex++)
        {
            if (pSdkManager->GetIOPluginRegistry()->WriterIsFBX(lFormatIndex))
            {
                KString lDesc = pSdkManager->GetIOPluginRegistry()->GetWriterFormatDescription(lFormatIndex);
                if (lDesc.Find("ascii")>=0)
                {
                    pFileFormat = lFormatIndex;
                    break;
                }
            }
        }
    }

    // Initialize the exporter by providing a filename.
    if(lExporter->Initialize(pFilename) == false)
    {
      control->ExporterInitializeFailed(pFilename, lExporter->GetLastErrorString());
      return false;
    }

    // Set the file format
//    lExporter->SetFileFormat(pFileFormat);

    // Export the scene.
    lStatus = lExporter->Export(pScene); 

    if (lStatus == false) 
        control->ExporterFailed(pFilename, lExporter->GetLastErrorString());

    // Destroy the exporter.
    lExporter->Destroy();


    return lStatus;
}


void PluginServices::ExportFBX(const char *fbxfilename, IFBXExportControl *control) {

    MManager manager;
    MScene scene (manager);

    const LODObject &p3d = control->GetSubject();
/*    double cntfaces = 0;
    for (int i = levelMin; i <= levelMax; i++) 
        cntfaces += p3d.Level(i)->NPoints();*/
    ProgressBar<int> pb(1);
    pb.ReportState(-10);
    pb.AdvanceNext(1);

    RString vxcachepath = Pathname(fbxfilename).GetDirectoryWithDrive();

    RVMATExplorer rvmatExplorer;
    if (control->ExtractRVMats())
        rvmatExplorer.ExploreP3D(p3d,control->GetProjectRoot());

    IFBXExportControl::MeshExportMode exportMode = control->GetCurMeshExportMode();

    P3DExporter exporter(manager,scene,rvmatExplorer,vxcachepath, control);

    switch (exportMode) {
        case IFBXExportControl::mAllLods:
            if (!exporter.exportAllLods(p3d)) return;
            break;
        case IFBXExportControl::mCurLod: 
            if (!exporter.exportLevel(*p3d.Active())) return;
            break;
        case IFBXExportControl::mSel:
            if (!exporter.exportSelected(*p3d.Active(),*p3d.Active()->GetSelection())) return;
            break;
    }

/*
    for (int i = levelMin; i <= levelMax; i++) {

        pb.AdvanceNext(p3d.Level(i)->NPoints());
        if (exportMode == IFBXExportControl::mSel) {
            ObjectData selOnly = *p3d.Level(i);
            ObjToolClipboard &tool = selOnly.GetTool<ObjToolClipboard>();
            tool.ExtractSelection();
            ExportLevel(manager, scene, selOnly, control,rvmatExplorer,vxcachepath);
        } else {
            ExportLevel(manager, scene, *p3d.Level(i), control,rvmatExplorer,vxcachepath);
        }
        if (pb.StopSignaled())
            return;


    }
    */

    SaveScene(manager,scene,fbxfilename,control);

}