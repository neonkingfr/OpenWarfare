#include "StdAfx.h"
#include "SceneWalker.h"
#include <el/ProgressBar/ProgressBar.h>

using namespace FbxSdk;


void SceneWalker::WalkScene(KFbxScene* pScene, KTime& pTime)
{
    KFbxXMatrix lDummyGlobalPosition;
    int i, lCount = pScene->GetRootNode()->GetChildCount();

    ProgressBar<int> pb(lCount);
    for (i = 0; i < lCount; i++)
    {
        pb.AdvanceNext(1);
        WalkNodeRecursive(pScene->GetRootNode()->GetChild(i), pTime, lDummyGlobalPosition);
    }
}


static KFbxXMatrix GetGeometry(KFbxNode* pNode) {
	KFbxVector4 lT, lR, lS;
	KFbxXMatrix lGeometry;

	lT = pNode->GetGeometricTranslation(KFbxNode::eSOURCE_SET);
	lR = pNode->GetGeometricRotation(KFbxNode::eSOURCE_SET);
	lS = pNode->GetGeometricScaling(KFbxNode::eSOURCE_SET);

	lGeometry.SetT(lT);
	lGeometry.SetR(lR);
	lGeometry.SetS(lS);

	return lGeometry;
}

void SceneWalker::WalkNodeRecursive(KFbxNode* pNode, 
                       KTime& pTime, 
                       KFbxXMatrix& pParentGlobalPosition)
{
    // Compute the node's global position.
    KFbxXMatrix lGlobalPosition = pNode->GetGlobalFromCurrentTake(pTime);

	// Geometry offset.
	// it is not inherited by the children.
	KFbxXMatrix lGeometryOffset = GetGeometry(pNode);
	KFbxXMatrix lGlobalOffPosition = lGlobalPosition * lGeometryOffset;

    WalkNode(pNode, pTime, pParentGlobalPosition, lGlobalOffPosition);

    int i, lCount = pNode->GetChildCount();

    ProgressBar<int> pb(lCount);
    for (i = 0; i < lCount; i++)
    {
        pb.AdvanceNext(1);
        WalkNodeRecursive(pNode->GetChild(i), pTime, lGlobalPosition);
    }
}

void SceneWalker::WalkNode(KFbxNode* pNode, 
                  KTime& lTime, 
                  KFbxXMatrix& pParentGlobalPosition,
                  KFbxXMatrix& pGlobalPosition)
{
    KFbxNodeAttribute* lNodeAttribute = pNode->GetNodeAttribute();

    if (lNodeAttribute)
    {

        if (lNodeAttribute->GetAttributeType() == KFbxNodeAttribute::eMARKER)
        {
            WalkMarker(pGlobalPosition);
        }
        else if (lNodeAttribute->GetAttributeType() == KFbxNodeAttribute::eSKELETON)
        {
            WalkSkeleton(pNode, pParentGlobalPosition, pGlobalPosition);
        }
        else if (lNodeAttribute->GetAttributeType() == KFbxNodeAttribute::eMESH)
        {
            WalkMesh(pNode, lTime, pGlobalPosition);
        }
        else if (lNodeAttribute->GetAttributeType() == KFbxNodeAttribute::eCAMERA)
        {
            WalkCamera(pNode, lTime, pGlobalPosition);
        }
        else if (lNodeAttribute->GetAttributeType() == KFbxNodeAttribute::eLIGHT)
        {
            WalkLight(pNode, lTime, pGlobalPosition);
        }

    }
}

void SceneWalker::WalkSkeleton(KFbxNode* pNode, 
                      KFbxXMatrix& pParentGlobalPosition, 
                      KFbxXMatrix& pGlobalPosition) 
{
    KFbxSkeleton* lSkeleton = (KFbxSkeleton*) pNode->GetNodeAttribute();

    // Only draw the skeleton if it's a limb node and if 
    // the parent also has an attribute of type skeleton.
    if (lSkeleton->GetSkeletonType() == KFbxSkeleton::eLIMB_NODE &&
        pNode->GetParent() &&
        pNode->GetParent()->GetNodeAttribute() &&
        pNode->GetParent()->GetNodeAttribute()->GetAttributeType() == KFbxNodeAttribute::eSKELETON)
            LimbNode(pNode, pParentGlobalPosition, pGlobalPosition);     
}



// Draw the vertices of a mesh.
void SceneWalker::WalkMesh(KFbxNode* pNode, KTime& pTime, KFbxXMatrix& pGlobalPosition)
{
    KFbxMesh* lMesh = (KFbxMesh*) pNode->GetNodeAttribute();
	int lClusterCount = 0;
    int lSkinCount= 0;
	int lVertexCount = lMesh->GetControlPointsCount();
    
    // No vertex to draw.
    if (lVertexCount == 0)
    {
        return;
    }

    // Create a copy of the vertex array to receive vertex deformations.
    KFbxVector4* lVertexArray = new KFbxVector4[lVertexCount];
    memcpy(lVertexArray, lMesh->GetControlPoints(), lVertexCount * sizeof(KFbxVector4));

	// Active vertex cache deformer will overwrite any other deformer
	if (lMesh->GetDeformerCount(KFbxDeformer::eVERTEX_CACHE) &&
		(static_cast<KFbxVertexCacheDeformer*>(lMesh->GetDeformer(0, KFbxDeformer::eVERTEX_CACHE)))->IsActive())
	{
		ReadVertexCacheData(lMesh, pTime, lVertexArray);
	}
	else
	{
		if (lMesh->GetShapeCount())
		{
			// Deform the vertex array with the shapes.
			ComputeShapeDeformation(pNode, lMesh, pTime, lVertexArray);
		}

		//we need to get the number of clusters
		lSkinCount = lMesh->GetDeformerCount(KFbxDeformer::eSKIN);
		for( int i=0; i< lSkinCount; i++)
			lClusterCount += ((KFbxSkin *)(lMesh->GetDeformer(i, KFbxDeformer::eSKIN)))->GetClusterCount();
		if (lClusterCount)
		{
			// Deform the vertex array with the links.
			ComputeClusterDeformation(pGlobalPosition, lMesh, pTime, lVertexArray);
		}
	}

    Mesh(pGlobalPosition, lMesh,lVertexArray);


    delete [] lVertexArray;
}

void SceneWalker::WalkCamera(KFbxNode* pNode, 
                KTime& pTime, 
                KFbxXMatrix& pGlobalPosition) {

    KFbxXMatrix lCameraGlobalPosition;
    KFbxVector4 lCameraPosition, lCameraDefaultDirection, lCameraInterestPosition;

    lCameraPosition = pGlobalPosition.GetT();

    // By default, FBX cameras point towards the X positive axis.
    KFbxVector4 lXPositiveAxis(1.0, 0.0, 0.0);
    lCameraDefaultDirection = lCameraPosition + lXPositiveAxis;

    lCameraGlobalPosition = pGlobalPosition;

    // If the camera is linked to an interest, get the interest position.
    if (pNode->GetTarget())
    {
        lCameraInterestPosition = pNode->GetTarget()->GetGlobalFromCurrentTake(pTime).GetT();

        // Compute the required rotation to make the camera point to it's interest.
        KFbxVector4 lCameraDirection;
        KFbxVector4::AxisAlignmentInEulerAngle(lCameraPosition, 
                                               lCameraDefaultDirection, 
                                               lCameraInterestPosition, 
                                               lCameraDirection);

        // Must override the camera rotation 
        // to make it point to it's interest.
        lCameraGlobalPosition.SetR(lCameraDirection);
    }
/*
    // Get the camera roll.
    double lRoll = pNode->GetCurrentTakeNode()->GetCameraRoll()->Evaluate(pTime);

    Camera(lCameraGlobalPosition, lRoll);
*/
}

void SceneWalker::WalkLight(KFbxNode* pNode, 
                   KTime& pTime, 
                   KFbxXMatrix& pGlobalPosition)
{
/*
    KFbxLight* lLight = (KFbxLight*) pNode->GetNodeAttribute();
    KFbxXMatrix lLightRotation, lLightGlobalPosition;
    KFbxColor lColor;
    double lConeAngle = 0.0;

    // Must rotate the light's global position because 
    // FBX lights point towards the Y negative axis.
    KFbxVector4 lYNegativeAxis(-90.0, 0.0, 0.0);
    lLightRotation.SetR(lYNegativeAxis);
    lLightGlobalPosition = pGlobalPosition * lLightRotation;

    // Get the light color.
    lColor.mRed = pNode->GetCurrentTakeNode()->GetColorR()->Evaluate(pTime);
    lColor.mGreen = pNode->GetCurrentTakeNode()->GetColorG()->Evaluate(pTime);
    lColor.mBlue = pNode->GetCurrentTakeNode()->GetColorB()->Evaluate(pTime);

    // The cone angle is only relevant if the light is a spot.
    if (lLight->GetLightType() == KFbxLight::eSPOT)
    {
        lConeAngle = pNode->GetCurrentTakeNode()->GetLightConeAngle()->Evaluate(pTime);
    }

    Light(lLightGlobalPosition, 
                lLight, 
                lColor, 
                lConeAngle);
*/
}


// Deform the vertex array with the shapes contained in the mesh.
void SceneWalker::ComputeShapeDeformation(KFbxNode* pNode,
                             KFbxMesh* pMesh, 
                             KTime& pTime, 
                             KFbxVector4* pVertexArray)
{
    int i, j;
    int lShapeCount = pMesh->GetShapeCount();
    int lVertexCount = pMesh->GetControlPointsCount();

    KFbxVector4* lSrcVertexArray = pVertexArray;
    KFbxVector4* lDstVertexArray = new KFbxVector4[lVertexCount];
    memcpy(lDstVertexArray, pVertexArray, lVertexCount * sizeof(KFbxVector4));

    for (i = 0; i < lShapeCount; i++)
    {
        KFbxShape* lShape = pMesh->GetShape(i);

        // Get the percentage of influence of the shape.
        KFCurve* lFCurve = pMesh->GetShapeChannel(i, false, /*pNode->GetCurrentTakeNodeName()*/NULL);
        double lWeight = lFCurve->Evaluate(pTime) / 100.0;

        for (j = 0; j < lVertexCount; j++)
        {
            // Add the influence of the shape vertex to the mesh vertex.
            KFbxVector4 lInfluence = (lShape->GetControlPoints()[j] - lSrcVertexArray[j]) * lWeight;
            lDstVertexArray[j] += lInfluence;
        }
    }

    memcpy(pVertexArray, lDstVertexArray, lVertexCount * sizeof(KFbxVector4));

    delete [] lDstVertexArray;
}


// Scale all the elements of a matrix.
static void MatrixScale(KFbxXMatrix& pMatrix, double pValue)
{

    pMatrix = pMatrix * pValue;
/*    int i;

    for (i = 0; i < 16; i++)
    {
        pMatrix[i] *=  pValue;
    }*/
}

// Add a value to all the elements in the diagonal of the matrix.
static void MatrixAddToDiagonal(KFbxXMatrix& pMatrix, double pValue)
{
    pMatrix[0][0] += pValue;
    pMatrix[1][1] += pValue;
    pMatrix[2][2] += pValue;
    pMatrix[3][3] += pValue;
}

// Sum two matrices element by element.
static void MatrixAdd(KFbxXMatrix& pDstMatrix, KFbxXMatrix& pSrcMatrix)
{
    int i,j;

    for (i = 0; i < 4; i++)
        for (j = 0; j < 4; j++)
    {
        pDstMatrix[i][j] += pSrcMatrix[i][j];
    }
}

// Deform the vertex array with the links contained in the mesh.
void SceneWalker::ComputeClusterDeformation(KFbxXMatrix& pGlobalPosition, 
                            KFbxMesh* pMesh, 
                            KTime& pTime, 
                            KFbxVector4* pVertexArray)
{
    // All the links must have the same link mode.
	KFbxCluster::ELinkMode lClusterMode = ((KFbxSkin*)pMesh->GetDeformer(0, KFbxDeformer::eSKIN))->GetCluster(0)->GetLinkMode();

    int i, j;
	int lClusterCount=0;

    //int lLinkCount = pMesh->GetLinkCount();
    int lVertexCount = pMesh->GetControlPointsCount();
    
    KFbxXMatrix* lClusterDeformation = new KFbxXMatrix[lVertexCount];
    memset(lClusterDeformation, 0, lVertexCount * sizeof(KFbxXMatrix));
    double* lClusterWeight = new double[lVertexCount];
    memset(lClusterWeight, 0, lVertexCount * sizeof(double));

	int lSkinCount=pMesh->GetDeformerCount(KFbxDeformer::eSKIN);
	


    //if (lLinkMode == KFbxLink::eADDITIVE)
	if(lClusterMode == KFbxCluster::eADDITIVE)
    {   
        for (i = 0; i < lVertexCount; i++)
        {   
            lClusterDeformation[i].SetIdentity();
        }
    }

	for(i=0; i<lSkinCount; ++i)
	{
		lClusterCount =( (KFbxSkin *)pMesh->GetDeformer(i, KFbxDeformer::eSKIN))->GetClusterCount();
		for (j=0; j<lClusterCount; ++j)
		{
			KFbxCluster* lCluster =((KFbxSkin *) pMesh->GetDeformer(i, KFbxDeformer::eSKIN))->GetCluster(j);
			if (!lCluster->GetLink())
				continue;
			KFbxXMatrix lReferenceGlobalInitPosition;
			KFbxXMatrix lReferenceGlobalCurrentPosition;
			KFbxXMatrix lClusterGlobalInitPosition;
			KFbxXMatrix lClusterGlobalCurrentPosition;
			KFbxXMatrix lReferenceGeometry;
			KFbxXMatrix lClusterGeometry;

			KFbxXMatrix lClusterRelativeInitPosition;
			KFbxXMatrix lClusterRelativeCurrentPosition;
			KFbxXMatrix lVertexTransformMatrix;

			if (lClusterMode == KFbxLink::eADDITIVE && lCluster->GetAssociateModel())
			{
				lCluster->GetTransformAssociateModelMatrix(lReferenceGlobalInitPosition);
                lReferenceGlobalCurrentPosition = lCluster->GetAssociateModel()->GetGlobalFromCurrentTake(pTime);
				// Geometric transform of the model
				lReferenceGeometry = GetGeometry(lCluster->GetAssociateModel());
				lReferenceGlobalCurrentPosition *= lReferenceGeometry;
			}
			else
			{
				lCluster->GetTransformMatrix(lReferenceGlobalInitPosition);
				lReferenceGlobalCurrentPosition = pGlobalPosition;
				// Multiply lReferenceGlobalInitPosition by Geometric Transformation
				lReferenceGeometry = GetGeometry(pMesh->GetNode());
				lReferenceGlobalInitPosition *= lReferenceGeometry;
			}
			// Get the link initial global position and the link current global position.
			lCluster->GetTransformLinkMatrix(lClusterGlobalInitPosition);
            lClusterGlobalCurrentPosition = lCluster->GetLink()->GetGlobalFromCurrentTake(pTime);

			// Compute the initial position of the link relative to the reference.
			lClusterRelativeInitPosition = lClusterGlobalInitPosition.Inverse() * lReferenceGlobalInitPosition;

			// Compute the current position of the link relative to the reference.
			lClusterRelativeCurrentPosition = lClusterGlobalCurrentPosition.Inverse() * lReferenceGlobalCurrentPosition;

			// Compute the shift of the link relative to the reference.
			lVertexTransformMatrix = lClusterRelativeCurrentPosition.Inverse() * lClusterRelativeInitPosition;
	        
			int k;
			int lVertexIndexCount = lCluster->GetControlPointIndicesCount();

			for (k = 0; k < lVertexIndexCount; ++k) 
			{            
				int lIndex = lCluster->GetControlPointIndices()[k];
				double lWeight = lCluster->GetControlPointWeights()[k];

				if (lWeight == 0.0)
				{
					continue;
				}

				// Compute the influence of the link on the vertex.
				KFbxXMatrix lInfluence = lVertexTransformMatrix;
				MatrixScale(lInfluence, lWeight);

				if (lClusterMode == KFbxCluster::eADDITIVE)
				{   
					// Multiply with to the product of the deformations on the vertex.
					MatrixAddToDiagonal(lInfluence, 1.0 - lWeight);
					lClusterDeformation[lIndex] = lInfluence * lClusterDeformation[lIndex];

					// Set the link to 1.0 just to know this vertex is influenced by a link.
					lClusterWeight[lIndex] = 1.0;
				}
				else // lLinkMode == KFbxLink::eNORMALIZE || lLinkMode == KFbxLink::eTOTAL1
				{
					// Add to the sum of the deformations on the vertex.
					MatrixAdd(lClusterDeformation[lIndex], lInfluence);

					// Add to the sum of weights to either normalize or complete the vertex.
					lClusterWeight[lIndex] += lWeight;
				}
				
			}
		}
    }

    for (i = 0; i < lVertexCount; i++) 
    {
        KFbxVector4 lSrcVertex = pVertexArray[i];
        KFbxVector4& lDstVertex = pVertexArray[i];
        double lWeight = lClusterWeight[i];

        // Deform the vertex if there was at least a link with an influence on the vertex,
        if (lWeight != 0.0) 
        {
            lDstVertex = lClusterDeformation[i].MultT(lSrcVertex);

            if (lClusterMode == KFbxCluster::eNORMALIZE)
            {
                // In the normalized link mode, a vertex is always totally influenced by the links. 
                lDstVertex /= lWeight;
            }
            else if (lClusterMode == KFbxCluster::eTOTAL1)
            {
                // In the total 1 link mode, a vertex can be partially influenced by the links. 
                lSrcVertex *= (1.0 - lWeight);
                lDstVertex += lSrcVertex;
            }
        } 
    }

    delete [] lClusterDeformation;
    delete [] lClusterWeight;
}

void SceneWalker::ReadVertexCacheData(KFbxMesh* pMesh, 
						 KTime& pTime, 
						 KFbxVector4* pVertexArray)
{
	KFbxVertexCacheDeformer* lDeformer     = static_cast<KFbxVertexCacheDeformer*>(pMesh->GetDeformer(0, KFbxDeformer::eVERTEX_CACHE));
	KFbxCache*               lCache        = lDeformer->GetCache();
	int                      lChannelIndex = -1;
	unsigned int             lVertexCount  = (unsigned int)pMesh->GetControlPointsCount();
	bool                     lReadSucceed  = false;
	double*                  lReadBuf      = new double[3*lVertexCount];		

	if (lCache->GetCacheFileFormat() == KFbxCache::eMC)
	{
		if ((lChannelIndex = lCache->GetChannelIndex(lDeformer->GetCacheChannel())) > -1)
		{
			lReadSucceed = lCache->Read(lChannelIndex, pTime, lReadBuf, lVertexCount);
		}
	}
	else // ePC2
	{
		lReadSucceed = lCache->Read((unsigned int)pTime.GetFrame(true), lReadBuf, lVertexCount);
	}
 
	if (lReadSucceed)
	{
		unsigned int lReadBufIndex = 0;

		while (lReadBufIndex < 3*lVertexCount)
		{
			pVertexArray[lReadBufIndex/3].SetAt(0, lReadBuf[lReadBufIndex++]);
			pVertexArray[lReadBufIndex/3].SetAt(1, lReadBuf[lReadBufIndex++]);
			pVertexArray[lReadBufIndex/3].SetAt(2, lReadBuf[lReadBufIndex++]);
		}
	}

	delete [] lReadBuf;
}
