#pragma once

#include <stdarg.h>

class MessageDlg :
    public CDialog
{
    CWnd *parent;
public:



    void SetParent(CWnd *parent) {this->parent = parent;}
    void AppendMsg(const char *str);

    void Format(const char *str, ...);
    void FormatV(const char *str, va_list args);

    void Format(int idc, ...);
    void FormatV(int idc, va_list args);

    virtual void OnOK();
    virtual void OnCancel();

    void Pump();

};
