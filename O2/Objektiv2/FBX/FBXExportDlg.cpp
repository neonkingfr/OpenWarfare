#include "StdAfx.h"
#include "FBXExportDlg.h"
#include "fbxrc.h"
#include "PluginServices.h"

#include "textureTypes.h"


extern "C" {
const char *GetPluginVersionString();
}

using namespace ObjektivLib;

BEGIN_MESSAGE_MAP(FBXExportDlg, CDialog)
    ON_LBN_SELCHANGE(IDC_UVASSIGNLB, OnChooseUVAssignLB)
    ON_CBN_SELCHANGE(IDC_UVASSIGNCB, OnChooseUVAssignCB)
    ON_BN_CLICKED(IDC_EXPORTTEXTURES, OnExportTexturesChange)
    ON_BN_CLICKED(IDC_EXPORTANIM, OnExportAnimBT)
END_MESSAGE_MAP()

FBXExportDlg::FBXExportDlg(const LODObject &p3d):p3d(p3d),lockLB(false),okclicked(false)
{
   
}

FBXExportDlg::~FBXExportDlg(void)
{
}

static const char *layerElementOrOff(int idx) {

    if (idx < 0) {
        static CString str(MAKEINTRESOURCE(IDS_OFFLAYER));
        return str;
    }

    return FBXTextureTypes::getType(idx);
}

namespace Functors {
    class InitListOfLayers {

        MapUVSetToLayer &layers;
        mutable int counter;        
    public:

        InitListOfLayers(MapUVSetToLayer &layers):layers(layers),counter(0) {}
        bool operator()(const ObjUVSet *uvset) const {

            int index = uvset->GetIndex();
            if (layers.find(index) == layers.end()) {
                layers.insert(MapUVSetToLayerItem(index,counter));            
                counter++;
                if (counter >= FBXTextureTypes::getCountTypes()) counter = 0;
            }
            return false;
        }

    };
}


BOOL FBXExportDlg::OnInitDialog() {

    CheckRadioButton(IDC_EXPORTALLLODS,IDC_EXPORTSELECTION,IDC_EXPORTALLLODS);
    CheckDlgButton(IDC_SPLITMESH,1);
    CheckDlgButton(IDC_RECONSTRUCTNAME,1);
    CheckRadioButton(IDC_NOSKINNING,IDC_SKINNINGFULL,IDC_NOSKINNING);
    CheckRadioButton(IDC_ALLUVSETS,IDC_NOUVSET,IDC_ALLUVSETS);
    CheckDlgButton(IDC_RVMATREF,1);
    CheckDlgButton(IDC_EXPORTEDGES,1);
    CheckDlgButton(IDC_EXPORTNORMALS,1);
    CheckDlgButton(IDC_EXPORTTEXTURES,1);
    GetDlgItem(IDC_EXPORTANIMFPS)->EnableWindow(FALSE);       
    SetDlgItemInt(IDC_EXPORTANIMFPS,30);
    
    Functors::InitListOfLayers lrass(layerAssign);
    for (int i = 0; i < p3d.NLevels(); i++) {
        const ObjectData *objl = p3d.Level(i);
        objl->EnumUVSets(lrass);
    }

    for (MapUVSetToLayer::iterator it = layerAssign.begin(); it != layerAssign.end(); ++it) {
        

        char buff[500];
        sprintf(buff,"%d\t%s",it->first,layerElementOrOff(it->second));
        int idx = SendDlgItemMessage(IDC_UVASSIGNLB,LB_ADDSTRING,0,(LPARAM)buff);
        SendDlgItemMessage(IDC_UVASSIGNLB,LB_SETITEMDATA,idx,it->first);

    }


    for (int i = -1; i< FBXTextureTypes::getCountTypes(); i++) {
        int idx = SendDlgItemMessage(IDC_UVASSIGNCB,CB_ADDSTRING,0,(LPARAM)layerElementOrOff(i));
        SendDlgItemMessage(IDC_UVASSIGNCB,CB_SETITEMDATA,idx,i);
    }

    CComboBox wMasterScale;
    wMasterScale.Attach(*GetDlgItem(IDC_MASTERSCALE));
    for (int i = -6; i < 7; i++) {
        char buff[50];
        sprintf_s(buff,50,"%g",exp(i * log(10)));
        wMasterScale.AddString(buff);
        if (i == 0) wMasterScale.SetWindowText(buff);
    }
    wMasterScale.Detach();

    CString title;
    GetWindowText(title);
    title = title + " (version: " + GetPluginVersionString() + ")";
    SetWindowText(title);

    return 0;
}

void FBXExportDlg::OnOK() {
    okclicked = true;        
}
void FBXExportDlg::OnCancel() {
    
    DestroyWindow();
}

void FBXExportDlg::OnChooseUVAssignLB() {
    
    if (lockLB)
        return;
    int idx = SendDlgItemMessage(IDC_UVASSIGNLB,LB_GETCURSEL,0,0);
    int uvl = SendDlgItemMessage(IDC_UVASSIGNLB,LB_GETITEMDATA,idx,0);
    MapUVSetToLayer::iterator it = layerAssign.find(uvl);
    if (it == layerAssign.end())
        SendDlgItemMessage(IDC_UVASSIGNCB,CB_SETCURSEL,-1,0);
    else {
        int pos = -1;
        int lr = layerAssign[uvl];
        int cnt = SendDlgItemMessage(IDC_UVASSIGNCB,CB_GETCOUNT,0,0);
        for (int i = 0; i< cnt; i++)
            if (lr == SendDlgItemMessage(IDC_UVASSIGNCB,CB_GETITEMDATA,i,0))
                pos = i;
        SendDlgItemMessage(IDC_UVASSIGNCB,CB_SETCURSEL,pos,0);   
    }

}
void FBXExportDlg::OnChooseUVAssignCB() {

    int lridx = SendDlgItemMessage(IDC_UVASSIGNCB,CB_GETCURSEL,0,0);
    int lr = SendDlgItemMessage(IDC_UVASSIGNCB,CB_GETITEMDATA,lridx,0);
    int uvidx = SendDlgItemMessage(IDC_UVASSIGNLB,LB_GETCURSEL,0,0);
    int uv = SendDlgItemMessage(IDC_UVASSIGNLB,LB_GETITEMDATA,uvidx,0);

    layerAssign[uv] = lr;

    char buff[500];
    lockLB = true;
    sprintf(buff,"%d\t%s",uv,layerElementOrOff(lr));
    int idx = SendDlgItemMessage(IDC_UVASSIGNLB,LB_INSERTSTRING,uvidx,(LPARAM)buff);
    SendDlgItemMessage(IDC_UVASSIGNLB,LB_SETITEMDATA, idx, uv);
    SendDlgItemMessage(IDC_UVASSIGNLB,LB_DELETESTRING,idx + 1,0);
    SendDlgItemMessage(IDC_UVASSIGNLB,LB_SETCURSEL,idx,0);
    lockLB = false;


}

FBXExportDlg::MeshExportMode FBXExportDlg::GetCurMeshExportMode() const {
    int item = const_cast<FBXExportDlg *>(this)->GetCheckedRadioButton(IDC_EXPORTALLLODS,IDC_EXPORTSELECTION);
    switch (item) {
        case IDC_EXPORTALLLODS: return mAllLods;
        case IDC_EXPORTCURLOD: return mCurLod;
        case IDC_EXPORTSELECTION: return mSel;
        default: return mAllLods;
    }
}

bool FBXExportDlg::SplitEnabled() const {
    return IsDlgButtonChecked(IDC_SPLITMESH) != FALSE;
}


bool FBXExportDlg::DetectObjectNames() const {
    return IsDlgButtonChecked(IDC_RECONSTRUCTNAME) != FALSE;
}

FBXExportDlg::SkinExportMode FBXExportDlg::GetSkinningMode() const {
    int item = const_cast<FBXExportDlg *>(this)->GetCheckedRadioButton(IDC_NOSKINNING,IDC_SKINNINGFULL);
    switch (item) {
        case IDC_NOSKINNING: return sNoSkinning;
        case IDC_SKINNINGLITE: return sLiteSkinning;
        case IDC_SKINNINGFULL: return sFullSkinning;
        default: return sNoSkinning;
    }
}


bool FBXExportDlg::ExportRVMatRef() const {
    return IsDlgButtonChecked(IDC_RVMATREF) != FALSE;   
}

int FBXExportDlg::GetAssignedLayer(int uvSetId) const {
    MapUVSetToLayer::const_iterator iter = layerAssign.find(uvSetId);
    if (iter == layerAssign.end()) return 0;
    else return iter->second;
}

bool FBXExportDlg::ExportEdges() const {
    return IsDlgButtonChecked(IDC_EXPORTEDGES) != FALSE;   

}
bool FBXExportDlg::ExportNormals() const {
    return IsDlgButtonChecked(IDC_EXPORTNORMALS) != FALSE;   

}

float FBXExportDlg::GetMasterScale() const {
    CString txt;
    GetDlgItemText(IDC_MASTERSCALE,txt);
    float scale;
    sscanf(txt,"%g",&scale);
    return scale;
}

bool FBXExportDlg::ExtractRVMats() const {
   return IsDlgButtonChecked(IDC_EXTRACTRVMAT) != FALSE;   
}

bool FBXExportDlg::ASCIIFormat() const {
    return IsDlgButtonChecked(IDC_ASCIIFMT) != FALSE;   
}

bool FBXExportDlg::GetSkinSkipOrdSel() const {
    return IsDlgButtonChecked(IDC_EXCLUDEORDSEL) != FALSE;   
}


void FBXExportDlg::ExporterInitializeFailed(const char *filename, const char *errorText) {
    msgDlg.Format(IDS_ERR_EXPORTERINITIALIZEFAILED, errorText,filename);    
}
void FBXExportDlg::ExporterFailed(const char *filename, const char *errorText){
    msgDlg.Format(IDS_ERR_EXPORTERFAILED, errorText,filename);    
}
void FBXExportDlg::LayerTypeIsAlreadyUsed(int uvid, int layertype) {
    msgDlg.Format(IDS_ERR_LAYERTYPEISALREADYUSED, uvid,FBXTextureTypes::getType(layertype));    
}
void FBXExportDlg::UnableToOpenVertexCacheChannel(const char *channelName) {
    msgDlg.Format(IDS_ERR_UNABLETOOPENVERTEXCACHECHANNEL, channelName);    
}
void FBXExportDlg::OnExportTexturesChange() {
    BOOL enabled = IsDlgButtonChecked(IDC_EXPORTTEXTURES) !=0;
    GetDlgItem(IDC_EXTRACTRVMAT)->EnableWindow(enabled);
    GetDlgItem(IDC_RVMATREF)->EnableWindow(enabled);
}

bool FBXExportDlg::ExportTextures() const {
    return IsDlgButtonChecked(IDC_EXPORTTEXTURES) != FALSE;   
}
void FBXExportDlg::OnExportAnimBT() {
    BOOL enabled = IsDlgButtonChecked(IDC_EXPORTANIM) !=0;
    GetDlgItem(IDC_EXPORTANIMFPS)->EnableWindow(enabled);
}

unsigned int FBXExportDlg::ExportAnimationFPS() const {
    if (IsDlgButtonChecked(IDC_EXPORTANIM) == FALSE) return 0;
    return GetDlgItemInt(IDC_EXPORTANIMFPS,0,FALSE);
}