#pragma once
#include "afxwin.h"
#include "IFBXPluginServices.h"
#include <projects/ObjektivLib/LODObject.h>
#include "MessageDlg.h"
#include "DialogWithMsgWnd.h"




#include <vector>
#include <utility>
#include <map>


class CFBXImportDlg :public DialogWithMsgWnd, public IFBXImportControl
{
protected:

    struct UVAssign: public std::pair<int,int>
    {

        UVAssign(int layer, int assign):
                std::pair<int,int>(layer,assign)
                    {}

        int GetLayer() const {return first;}
        int GetUVSet() const {return second;}
        void SetUVSet(int i)  {second = i;}
    };

    std::vector<UVAssign> assignedTextures;

    typedef std::map<unsigned long, HTREEITEM> TreeMap;
    TreeMap treeMap;

    typedef std::vector<std::pair<DataForRVMat, RStringI> >RVMATProto;
    RVMATProto rvmatproto;
    RString rvmatTarget;


    CString lastTake;


    LODObject p3d;
    int result;

    CButton wCancel;
    CButton wOk;

    CComboBox wMasterScale;
    CComboBox wTake;
    CEdit wFPS;
    CTreeCtrl wObjectTree;
    CListCtrl wUVSetAssign;
    CButton wImportSkeleton;
    CButton wImportRVMAT;
    CButton wImportLODs;
    CButton wImportSkin;
    CButton wImportSharp;
    CButton wImportUVs;
    CButton wImportTex;
    CButton wImportMesh;
    CButton wImportAnim;
    CButton wPrepareMaterials;

    CString basePath;


    CButton *buttonMap[9];

public:
    CFBXImportDlg(void);
    ~CFBXImportDlg(void);

    void SetBasePath(CString basePath) {this->basePath = basePath;}

    const LODObject &GetImportedP3D() const {return p3d;}
    int GetImportResult() const {return result;}

    virtual bool StoreResult(const LODObject &object);
    virtual int GetImportMask();
    virtual int GetAnimationSpeed();
    virtual float GetMasterScale();
    virtual int GetUVSetForTextureType(int layer);
    virtual void DefineNewTake(const char *takeName);
    virtual void DefineUVSetType(int layer);
    virtual const char *GetChoosenTake();
    virtual const char *AskPassword(char *buffer, std::size_t sz);
    virtual const char *GetTextureBasePath();
    virtual bool OpenConfigurationDialog();
    virtual bool CanImportObject(unsigned long objectId);
    virtual void DefineNewObject(const char *objectName, 
                                 unsigned long objectId, 
                                 unsigned long parentObjectId);
    virtual void EnableWhatImportGroup(WhatImport groupEnabled);
    virtual void EnableTextureAtLayer(int layer, int textureType);


    virtual BOOL OnInitDialog();

    virtual void OnOK();
    virtual void OnCancel();

    void OnEditUVSetAssign(NMHDR *hdr, LRESULT *res);
    void OnDestroy();
    void OnChangeObjectTree(NMHDR *hdr, LRESULT *res);

    virtual void SetPreferedAnimationFPS(int fps);

    virtual const char *CreateRVMATPrototype(const DataForRVMat &data);

    virtual void ImportError(int code,const char *popis, const char* pFilename);
    virtual void ImportInitError(int code,const char *popis, const char* pFilename);
    virtual void UnsupportedReferenceModeForUVCoords(const char *objName, int mapMode);
    virtual void UnsupportedReferenceModeForTextures(const char *objName, int mapMode);
    virtual void UnsupportedReferenceModeForSmoothing(const char *objName, int mapMode);
    virtual void InvalidFPS(int fps);
    virtual void InvalidIndexInArray(const char *objname, int layertype);
    virtual bool DontCreateObjectSelections() ;


    DECLARE_MESSAGE_MAP();
};
