#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "../IPluginGeneral.h"


                                                           

class FBXPlugin: public IPluginGeneralPluginSide {

    IPluginGeneralAppSide *appside;
    HWND o2Window;
    friend void ImportCb(const ObjektivLib::LODObject &p3d, void *context);
protected:
   virtual void AfterOpen(HWND O2Window);                //is called after main window is opened (not document!)
   virtual void AfterClose();
   virtual BOOL OnPretranslateMessage(MSG *msg) {return FALSE;};     //vola aplikace, aby si plugin mohl odchytnout p��padn� hotkeye
   virtual int GetAllDependencies(const char *p3dname, 
       const ObjektivLib::LODObject *object, char **array) {return 0;}

public:
       void Init(IPluginGeneralAppSide *appside);


    class FBXImportCommand: public IPluginCommand, public RefCount
    {
      virtual void OnCommand();
      virtual void AddRef() {RefCount::AddRef();}
      virtual void Release() {RefCount::Release();}
        
    };

    class FBXExportCommand: public IPluginCommand, public RefCount
    {
      virtual void OnCommand();
      virtual void AddRef() {RefCount::AddRef();}
      virtual void Release() {RefCount::Release();}
        
    };
};

