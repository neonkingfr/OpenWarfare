#pragma once

#include <vector>
#include <string>

class IFBXImportControl;
class IFBXExportControl;
                                      
namespace ObjektivLib {
    class LODObject;
}

class IFBXPluginServices
{
public:

    virtual ~IFBXPluginServices(void)    {}

    virtual void ImportFBX(const char *fbxfilename, IFBXImportControl *control) = 0;
    virtual void ExportFBX(const char *fbxfilename, IFBXExportControl *control) = 0;
};

class IFBXImportErrors
{
public:

    virtual void ImportError(int code,const char *popis, const char* pFilename) {}
    virtual void ImportInitError(int code,const char *popis, const char* pFilename) {}
    virtual void UnsupportedReferenceModeForUVCoords(const char *objName, int mapMode) {}
    virtual void UnsupportedReferenceModeForTextures(const char *objName, int mapMode) {}
    virtual void UnsupportedReferenceModeForSmoothing(const char *objName, int mapMode) {}
    virtual void InvalidFPS(int fps) {} 
    virtual void InvalidIndexInArray(const char *objname, int layertype) {}
};

class IFBXExportErrors
{
public:
    virtual void ExporterInitializeFailed(const char *filename, const char *errorText) {}
    virtual void ExporterFailed(const char *filename, const char *errorText) {}
    virtual void LayerTypeIsAlreadyUsed(int uvid, int layertype) {}
    virtual void UnableToOpenVertexCacheChannel(const char *channelName) {}
};

class IFBXImportControl : public IFBXImportErrors
{
public:
    virtual ~IFBXImportControl (void)  {}


    ///called to store imported result
    /**
      @param object reference to created object
      @retval true object has been copied
      @retval false control class don't want object in  this form. If function returns false, plugin tries to 
      import object in serialized form
     */
    virtual bool StoreResult(const ObjektivLib::LODObject &object) {return false;}
    ///called to store imported result in serialized form
    /**
        Function is called, when StoreResult for object failed.

      @param serializedP3Ddata serialized P3D (file in memory)
      @param dataSize size of serialized P3D
      @retval true object has been copied
      @retval false control class don't want object in  this form. In most in cases, this leads to error
     */
    virtual bool StoreResult(const void *serializedP3Ddata, unsigned int dataSize) {return false;}


    enum WhatImport {

        importMesh = 1,
        importTextures = 2,
        importUVSets = 4,
        importSmooths = 8,
        importSkinning = 16,
        importLODs = 32,
        importRVMATRefs = 64,
        importSkeleton = 128,
        importAnimation = 256
        
    };

    ///retrieves mask of WhatImport combinations
    virtual int GetImportMask() {return importMesh|importUVSets|importSmooths;};
    ///retrieves animations speed in frames per seconds. Use zero to import first frame only
    virtual int GetAnimationSpeed() {return 0;}
    ///retrieves master scale
    virtual float GetMasterScale() {return 0.01f;}
    ///retrieves index of UVSet for FBX texture type.
    virtual int GetUVSetForTextureType(int layer) {return layer;}
    ///Informs control object about available takes
    virtual void DefineNewTake(const char *takeName) {}
    ///Informs control object about available uvset
    virtual void DefineUVSetType(int layer) {}
    ///Informs control object that layer has assigned texture
    virtual void EnableTextureAtLayer(int layer, int textureType) {}
    ///Asks control object on choosen take    
    virtual const char *GetChoosenTake()  {return 0;}
    ///Asks control object for password needed during import. Function can use specified buffer to store the password
    virtual const char *AskPassword(char *buffer, std::size_t sz) {return 0;}
    ///retrieves textures base path for the P3D
    virtual const char *GetTextureBasePath() {return 0;}
    ///gives control object change to open configuration dialog
    /**
      Importer first informs control object of all abilities of imported file. Then it calls
      OpenCofigurationDialog for give user chance to conifgure import. When configuration dialog
      is closed, importer continues in importing. 
      @retval true continue
      @retval false cancel the import
      */
    virtual bool OpenConfigurationDialog() {return true;}
    
    ///asks control object, whether is allowed to import this object
    /**
     @param object identifier
     @retval true object will be imported
     @retval false object will not be imported
     @note disabling object that is parent of another object will also disable all child objects
     */
    virtual bool CanImportObject(unsigned long objectId) {return true;}

    ///defines new object.
    /**
     @param objectName name of the object
     @param objectId object identifier associated to the object
     @param parentObjectId identifier of parent object
     */
    virtual void DefineNewObject(const char *objectName, 
                                 unsigned long objectId, 
                                 unsigned long parentObjectId) {}

    virtual void EnableWhatImportGroup(WhatImport groupEnabled) {}

    virtual void SetPreferedAnimationFPS(int fps) {}

    ///Converts texture
    /**
      @param textureName texture file name readed from FBX
      @return name of converted texture (full pathname). Texture should be placed under
        the "base path". Caller should be prepared that this function can be called
        multiple times for one texture
      */
    virtual const char *ConvertTexture(const char *textureName) {return textureName;}

    struct DataForRVMat {

        std::vector<std::pair<int, std::string> > uvSetsAndTextures;

        bool operator==(const DataForRVMat& other) const {
            if (uvSetsAndTextures.size() != other.uvSetsAndTextures.size())
                return false;
            for (unsigned int i = 0; i < uvSetsAndTextures.size(); i++) 
                if (uvSetsAndTextures[i].first != other.uvSetsAndTextures[i].first ||
                    _stricmp(uvSetsAndTextures[i].second.c_str(),
                    other.uvSetsAndTextures[i].second.c_str()) != 0)
                        return false;
            return true;             
        }

        bool operator!=(const DataForRVMat& other) const {return !operator==(other);}

        void Add(int layer, std::string texture) {
            uvSetsAndTextures.push_back(std::make_pair(layer,texture));
        }

        void Clear() {uvSetsAndTextures.clear();}

        int Size() const {return uvSetsAndTextures.size();}

    };

    ///creates prototype of RVMAT
    /**
     function takes data about textures and will create material prototype from them
     Caller must be prepared, that function will be called multiple times for one
     combination.
     @param data informations about the textures in material
     @return name of rvmat prototype
     @note Function should always create a valid string. If NULL is returned
       importer stops material creation cycle.
     */
    virtual const char *CreateRVMATPrototype(const DataForRVMat &data) {return 0;}

    virtual bool DontCreateObjectSelections() = 0;
    
};

class IFBXExportControl : public IFBXExportErrors {
    
public:
    enum MeshExportMode {        
        mAllLods, mCurLod, mSel
    };

    enum SkinExportMode {
        sNoSkinning, sLiteSkinning, sFullSkinning
    };
    
    virtual MeshExportMode GetCurMeshExportMode() const =0;
    virtual bool SplitEnabled() const = 0;
    virtual bool DetectObjectNames() const = 0;
    virtual SkinExportMode GetSkinningMode() const = 0;
    virtual bool ExportRVMatRef() const = 0;
    virtual int GetAssignedLayer(int uvSetId) const = 0;
    virtual bool ExportEdges() const = 0;
    virtual bool ExportNormals() const = 0;
    virtual const ObjektivLib::LODObject &GetSubject() const = 0;
    virtual float GetMasterScale() const = 0;
    virtual bool ExtractRVMats() const = 0;
    virtual bool ASCIIFormat() const = 0;
    virtual const char *GetProjectRoot() const = 0;
    virtual bool GetSkinSkipOrdSel() const = 0;
    virtual bool ExportTextures() const = 0;
    
    ///Enables animation export.
    /**
    @retval 0 animation will not be exported
    @retval other Animation will be exported in specified FPS.
    */
    virtual unsigned int ExportAnimationFPS() const = 0;


};

