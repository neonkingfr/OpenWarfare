#include "StdAfx.h"
#include "SceneLoad.h"

namespace FbxSdk
    {

    bool SceneLoad::LoadScene(MManager& manager, 
                   MScene &scene, 
                   const char* pFilename)
    {
/*
        MStreamOptionsFbxReader lImportOptions(manager);

         lImportOptions->SetOption(KFBXSTREAMOPT_FBX_MATERIAL, true);
         lImportOptions->SetOption(KFBXSTREAMOPT_FBX_TEXTURE, true);
         lImportOptions->SetOption(KFBXSTREAMOPT_FBX_LINK, true);
         lImportOptions->SetOption(KFBXSTREAMOPT_FBX_SHAPE, true);
         lImportOptions->SetOption(KFBXSTREAMOPT_FBX_GOBO, true);
         lImportOptions->SetOption(KFBXSTREAMOPT_FBX_ANIMATION, true);
         lImportOptions->SetOption(KFBXSTREAMOPT_FBX_GLOBAL_SETTINGS, true);
*/
        // Create an importer.
	    MImporter lImporter(manager);

	    // Get the file version number generate by the FBX SDK.
//	    KFbxIO::GetCurrentVersion(lSDKMajor, lSDKMinor, lSDKRevision);

/*    if (manager->GetIOPluginRegistry()->DetectFileFormat(pFilename, lFileFormat))
	    {
		    // Unrecognizable file format. Try to fall back to native format.
		    lFileFormat = manager->GetIOPluginRegistry()->GetNativeReaderFormat();
	    }
	    lImporter->SetFileFormat(lFileFormat);
*/

        // Initialize the importer by providing a filename.
	    const bool lImportStatus = lImporter->Initialize(pFilename);
	    lImporter->GetFileVersion(lFileMajor, lFileMinor, lFileRevision);

	    if( !lImportStatus )
	    {
            ImportError(lImporter,pFilename);
            return false;
        }

	    if (lImporter->IsFBX())
	    {
            ExtraFbxProcessing(*lImporter);
//            SetOptions(*lImportOptions);
        }

	    // Import the scene.
	    bool lStatus = lImporter->Import(scene);

        int retries = 3;

	    while (lStatus == false 
                && lImporter->GetLastErrorID() == KFbxIO::ePASSWORD_ERROR 
                && retries > 0)
    	{
            retries--;

            char buff[1024];
            const char *passwordSet = AskPassword(buff,lenof(buff));
            if (passwordSet == 0) 
                break;
/*
    		lImportOptions->SetOption(KFBXSTREAMOPT_FBX_PASSWORD, KString(buff));
	    	lImportOptions->SetOption(KFBXSTREAMOPT_FBX_PASSWORD_ENABLE, true);
*/
            lStatus = lImporter->Import(scene/*, lImportOptions*/);
        }

        if (lStatus == false)
            ImportError(lImporter,pFilename);
            

        return lStatus;
    }
}