#include "StdAfx.h"
#include "MessageDlg.h"
#include "fbxrc.h"

void MessageDlg::AppendMsg(const char *str) {

    if (GetSafeHwnd() == NULL) {

        Create(IDD_MESSAGEWND, parent);


    }

    CEdit edt;
    edt.Attach(*GetDlgItem(IDC_MESSAGEBOX));
    int len = edt.GetWindowTextLength();
    edt.SetSel(len,len);
    edt.ReplaceSel(str);
    edt.ReplaceSel("\r\n");
    edt.UpdateWindow();
    UpdateWindow();
    edt.Detach();

}


void MessageDlg::Format(const char *str, ...) 
{

    va_list args;
    va_start(args,str);
    CString text;
    text.FormatV(str,args);
    va_end(args);
    AppendMsg(text);
}

void MessageDlg::FormatV(const char *str, va_list args) 
{

    CString text;
    text.FormatV(str,args);

    AppendMsg(text);

}

void MessageDlg::Format(int idc, ...) 
{
    va_list args;
    va_start(args,idc);
    CString text;
    text.FormatV(CString(MAKEINTRESOURCE(idc)),args);
    va_end(args);
    AppendMsg(text);
}
void MessageDlg::FormatV(int idc, va_list args) 
{
    CString text;
    text.FormatV(CString(MAKEINTRESOURCE(idc)),args);
    AppendMsg(text);
}


void MessageDlg::OnOK() {
    DestroyWindow();
}
void MessageDlg::OnCancel() {
    DestroyWindow();
}


void MessageDlg::Pump() {

    while (AfxPumpMessage()) {
        if (GetSafeHwnd() == 0) break;
    }

}