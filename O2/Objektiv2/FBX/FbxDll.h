#include "IFBXPluginServices.h"
#include "IFBXPluginUI.h"

class IPluginGeneralPluginSide;
class IPluginGeneralAppSide;

#if defined DOXYGEN
    #define FBXDLL_DECL
#elif defined FBXDLL_EXPORTS
    #define FBXDLL_DECL __declspec(dllexport)
#else
    #define FBXDLL_DECL __declspec(dllimport)
#endif


// Export init function
extern "C" {

///retrieves interface for connection with O2.
/**
    @param pointer to implementation of IPluginGeneralAppSide interface - connection with O2 application
    @return pointer to implementation of IPluginGeneralPluginSide that O2 can use to communicate with the plugin
*/

FBXDLL_DECL IPluginGeneralPluginSide * __cdecl  ObjektivPluginMain(IPluginGeneralAppSide *appside);

///retrieves pointer to interface IFBXPluginServices useful to call import export function directly
FBXDLL_DECL IFBXPluginServices * __cdecl  GetPluginServices();


///Creates import dlg
/**
    @return pointer to import dlg. To destroy dialog, use delete operator
*/
FBXDLL_DECL IFBXImportUI * __cdecl CreateImportDialog();

///Creates export dlg
/**
    @param p3d reference to P3D object
    @return pointer to export dlg. To destroy dialog, use delete operator
*/
FBXDLL_DECL IFBXExportUI * __cdecl CreateExportDialog(const ObjektivLib::LODObject &p3d);


///Parses and loads the P3D file
/**
  Function can be useful, when it is need to convert P3D to FBX without ObjektivLib library
  @param data pointer to memory block contained the loaded P3D from the file
  @param sz size of memory block. 
  @retval ptr pointer to parsed P3D as pointer to LODObject;
  @retval 0 in case of error.
  @note don't forget to call FreeP3D function to release allocated memory
 */
FBXDLL_DECL ObjektivLib::LODObject *LoadP3D(const void *data, size_t sz);

///Releases memory occupied by loaded P3D
/**
  @param p3d pointer p3d 
*/
FBXDLL_DECL void FreeP3D(ObjektivLib::LODObject *p3d);

FBXDLL_DECL const char *GetPluginVersionString();


}
//@{
/**
Wraps the function as type. Use this type to declare function call
if you extracting function from the DLL using the GetProcAddress
*/
typedef IPluginGeneralPluginSide * (__cdecl  *ObjektivPluginMain_fn)(IPluginGeneralAppSide *appside);
typedef IFBXPluginServices * (__cdecl  *GetPluginServices_fn)();
typedef void (*ExportFBXDlg_fn)(HWND parentWnd, const char *filename, const ObjektivLib::LODObject &p3d, const char *projectRoot);
typedef void (*ImportFBXDlg_fn)(HWND parentWnd, const char *filename, const char *projectRoot, void *context, void (*callback)(const ObjektivLib::LODObject &p3d, void *context));
typedef ObjektivLib::LODObject (*LoadP3D_fn)(const void *data, size_t sz);
typedef void (*FreeP3D_fn)(ObjektivLib::LODObject *p3d);
typedef const char *(*GetPluginVersionString_fn)();
//@}
