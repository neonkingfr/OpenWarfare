#pragma once
#include "afxwin.h"
#include "MessageDlg.h"

class DialogWithMsgWnd :
    public CDialog
{
protected:
    MessageDlg msgDlg;
public:
    DialogWithMsgWnd(void);
    void ShowMessages();
};
