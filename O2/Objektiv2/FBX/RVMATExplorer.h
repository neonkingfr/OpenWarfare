#pragma once

#include <map>                          


class RVMATExplorer: protected std::map<std::pair<RStringI, int>, RStringI>
{
protected:
    typedef std::pair<RStringI, int> Key;
    typedef std::pair<Key, RStringI> Item;
public:

    void ExploreP3D(const ObjektivLib::LODObject &p3d, const char *projectRoot);

    const char *GetTextureOfMaterial(const RStringI &materialName, int uvset) const;

protected:
    
    void ExploreMaterial(const char *matname, const char *projectRoot);


};
