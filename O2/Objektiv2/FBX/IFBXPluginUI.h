#pragma once

#include "IFBXPluginServices.h"

class IFBXImportUI {

public:
    virtual ~IFBXImportUI() {}

    ///Opens Import dialog and ask user for the settings
    /**
    @param hWnd handle to the parent window. Cannot be NULL (Use GetDesktopWnd())
    @param filename FBX to import
    @param projectRoot Project root for the textures
    @param skipImport if set true, function will always cancel the import. Useful
            to store user options
    @retval true user clicked OK
    @retval false user clicked Cancel;
    */
    virtual bool Open(HWND hWnd, 
                      const char *filename,
                      const char *projectRoot,
                      bool skipImport) = 0;

    ///retrieves object name of given id
    /**
     @param objId ID of the object @see DefineNewObject
     @return object name
    */
    virtual const char *GetObjectName(unsigned long objId) = 0;

    ///Retrieves imported P3D as object
    virtual const ObjektivLib::LODObject &GetImportedP3D() = 0;

    ///Retrieves imported P3D as serialized data
    virtual const void *GetImportedP3D(size_t &sz) = 0;        

    virtual IFBXImportControl *GetControl() = 0;

    
};

class IFBXExportUI {

public:
    virtual ~IFBXExportUI() {}

    ///opens the export dialog
    /**
    @param hWnd handle of parent window. Cannot be NULL (Use GetDesktopWnd())
    @param filename FBX to export. It can be NULL, if skipExport is true
    @param projectRoot Project root for the textures
    @param skipExport if set true, function always cancel the export. Useful
            to store user options
    @retval true user clicked OK
    @retval false user clicked Cancel    
    */

    virtual bool Open(HWND hWnd, 
                      const char *filename, 
                      const char *projectRoot, 
                      bool skipExport) = 0;
    

    
    virtual IFBXExportControl *GetControl() = 0;

    
};
