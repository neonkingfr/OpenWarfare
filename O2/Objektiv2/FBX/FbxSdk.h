#pragma once

#include <fbxsdk.h>
//#include <fbxfilesdk_def.h>
#include <es/types/pointers.hpp>
#include <exception>
#include <typeinfo.h>
#include <string>

namespace ObjektivLib {
    class LODObject;
}


namespace FbxSdk
{
    using namespace FBXFILESDK_NAMESPACE;

    class FbxException: public std::exception
    {
        mutable std::string wholeMsg;
    public:

        virtual int GetCode() const = 0;
        virtual  std::string GetMessage() const = 0;
        const std::string &GetWholeMsg() const {

            if (wholeMsg.empty()) {
                char buff[50];
                wholeMsg = std::string(_itoa(GetCode(),buff,10)).append(GetMessage());                    
            }
            return wholeMsg;

        }

        const char *what() const throw() {
            return GetWholeMsg().c_str();
        }

        int GetAllDependencies(const char *p3dname, 
            const ObjektivLib::LODObject *object, 
            char **array) {return 0;}


    };



    class CreationException: public FbxException
    {
    public:
        virtual int GetCode() const {return 1;}
        virtual  std::string GetMessage() const 
        {
            return std::string("Unable to create class: ") +
                GetClassName();
        }
        virtual std::string GetClassName() const = 0;


    };

    template<class T>
    class FailedToCreate: public CreationException
    {
    public:
        virtual std::string GetClassName() const
        {
            return typeid(T).name();
        }
    };

    template <class Type>
    struct FBXRefTraits
    {
        static void Delete(Type *ptr) {if (ptr) ptr->Destroy();}
    };

    template <class Type>
    class FBXRef: public SRef<Type, FBXRefTraits<Type> >
    {
    public:
        typedef SRef<Type, FBXRefTraits<Type> > Base;

        FBXRef() {}
        FBXRef( Type *source ): Base(source) {}

        void CheckInstance() const
        {
            if (Base::GetRef() == 0)
                throw FailedToCreate<Type>();
        }

        Type *operator -> () const {CheckInstance(); return Base::operator->();}
        operator Type *() const {CheckInstance(); return Base::operator->();}
    };

    template<>
    void FBXRefTraits<KFbxSdkManager>::Delete(KFbxSdkManager *ptr) {
        ptr->Destroy();
    }


    class MManager: public FBXRef<KFbxSdkManager>
    {
    public:
        MManager():FBXRef(KFbxSdkManager::Create()) {}
    };  

    template<class T>
    class FbxStdObject: public FBXRef<T>
    {
    public:
        FbxStdObject(MManager &manager, const char *name=""):
          FBXRef(T::Create(manager.GetRef(),name)) {}
        FbxStdObject(const FbxStdObject<T> &other):FBXRef(static_cast<T *>(other->Clone())) {}
        FbxStdObject(const FbxStdObject<T> &other, 
                     KFbxObject::ECloneType cloneType):FBXRef(other->Clone(cloneType)) {}
        FbxStdObject(T *other):FBXRef(other) {}
        template<class X>
        FbxStdObject(X *other):FBXRef(static_cast<T *>(other)) {}

        operator T &() const {return *GetRef();}
    };

    //managed objects
    //they are created and destroyed as static or auto objects
    //but still they are pointers to original SDK objects
    //Using this object allowes to you throw exceptions, because managed objects will
    //always delete its instance at the end of scope.
    //Note: Managed objects are not ref counted.
    typedef FbxStdObject<KFbxImporter> MImporter;
    typedef FbxStdObject<KFbxScene> MScene;
//    typedef FbxStdObject<KFbxStreamOptionsFbxReader> MStreamOptionsFbxReader;
    typedef FbxStdObject<KFbxNode> MNode;


}
