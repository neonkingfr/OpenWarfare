#include <stdafx.h>
#include "hlpfuncts.h"

RString MakeUniqueName(const char *name) 
{
    static int uniqueId = 1;
    RString buffer;
    buffer.CreateBuffer((name?strlen(name):0)+20);
    sprintf(buffer.MutableData(),"%s_NNN%d",name?name:"",uniqueId++);
    return buffer;
}

RString RemoveUniqueId(const char *name)
{
    const char *nidepos = strstr(name,"_NNN");
    if (nidepos == 0) return name;
    return RString(name,nidepos - name);
}

