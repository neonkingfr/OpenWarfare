#pragma once
#include "afxwin.h"
#include "IFBXPluginServices.h"
#include <projects/ObjektivLib/LODObject.h>
#include "DialogWithMsgWnd.h"

#include <vector>
#include <utility>
#include <map>

typedef std::map<int, int> MapUVSetToLayer;

typedef std::pair<int, int> MapUVSetToLayerItem;



class FBXExportDlg :public DialogWithMsgWnd, public IFBXExportControl
{
    
    const LODObject &p3d;
    bool lockLB;
    bool okclicked;
    RString projectRoot;

public:
    FBXExportDlg(const LODObject &p3d);
    ~FBXExportDlg(void);

    void SetProjectRoot(const char *pj) {
        projectRoot = pj;
    }


    bool OkClicked() const {return okclicked;}

protected:

    virtual BOOL OnInitDialog();
    virtual void OnOK();
    virtual void OnCancel(); 

    void OnChooseUVAssignLB();
    void OnChooseUVAssignCB();
    void OnExportTexturesChange();
    void OnExportAnimBT();

protected:


    MapUVSetToLayer layerAssign;

    virtual MeshExportMode GetCurMeshExportMode() const;
    virtual bool SplitEnabled() const;
    virtual bool DetectObjectNames() const;
    virtual SkinExportMode GetSkinningMode() const;
    virtual bool ExportRVMatRef() const;
    virtual int GetAssignedLayer(int uvSetId) const;
    virtual bool ExportEdges() const;
    virtual bool ExportNormals() const;
    virtual const LODObject &GetSubject() const {return p3d;}
    virtual float GetMasterScale() const;
    virtual bool ExtractRVMats() const;
    virtual bool ASCIIFormat() const;
    virtual const char *GetProjectRoot() const {return projectRoot;}
    virtual bool GetSkinSkipOrdSel() const ;

    virtual void ExporterInitializeFailed(const char *filename, const char *errorText);
    virtual void ExporterFailed(const char *filename, const char *errorText);
    virtual void LayerTypeIsAlreadyUsed(int uvid, int layertype);
    virtual bool ExportTextures() const;
    virtual void UnableToOpenVertexCacheChannel(const char *channelName) ;

    ///Enables animation export.
    /**
    @retval 0 animation will not be exported
    @retval other Animation will be exported in specified FPS.
    */
    virtual unsigned int ExportAnimationFPS() const;


    DECLARE_MESSAGE_MAP();
};
