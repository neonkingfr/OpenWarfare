#pragma once
#include "p3dstorehelper.h"

class SkeletonToP3D :  public P3DStoreHelper
{
public:

    virtual void LimbNode(KFbxNode *node, KFbxXMatrix& pGlobalBasePosition, KFbxXMatrix& pGlobalEndPosition);

};
