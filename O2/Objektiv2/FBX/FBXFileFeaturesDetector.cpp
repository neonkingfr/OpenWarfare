#include "StdAfx.h"
#include "FBXFileFeaturesDetector.h"
#include "textureTypes.h"

FBXFileFeaturesDetector::FBXFileFeaturesDetector(IFBXImportControl *control):
    control(control) {}


void FBXFileFeaturesDetector::WalkNode(KFbxNode* pNode, 
              KTime& lTime, 
              KFbxXMatrix& pParentGlobalPosition,
              KFbxXMatrix& pGlobalPosition) {

    const char *name = pNode->GetName();
    if (name == 0 || name[0] == 0) {

        char buff[50];
        sprintf_s(buff,50,"unnamed%lX",(unsigned long)pNode);
        control->DefineNewObject(buff,(unsigned long)pNode,(unsigned long)(pNode->GetParent()));
    }
    else
    {
        control->DefineNewObject(name,(unsigned long)pNode,(unsigned long)(pNode->GetParent()));
        if (strstr(name,"_LOD")!=0) control->EnableWhatImportGroup(IFBXImportControl::importLODs);
    }

    SceneWalker::WalkNode(pNode,lTime,pParentGlobalPosition,pGlobalPosition);
}

void FBXFileFeaturesDetector::Mesh(KFbxXMatrix& pGlobalPosition, KFbxMesh* pMesh, 
                                   KFbxVector4* pVertexArray) {
    
    control->EnableWhatImportGroup(IFBXImportControl::importMesh);

    for (int i = 0,  cnt = pMesh->GetLayerCount(); i < cnt; i++) {

        KFbxLayer *layer = pMesh->GetLayer(i);
        if (layer) {

            if (layer->GetUVs() != 0) {
                control->EnableWhatImportGroup(IFBXImportControl::importUVSets);
                control->DefineUVSetType(i);
            }

            KFbxLayerElementSmoothing *smoothing = layer->GetSmoothing();
            if (smoothing) {
                KFbxLayerElement::EMappingMode mapMode = smoothing->GetMappingMode();
                if (mapMode == KFbxLayerElement::eBY_EDGE || mapMode == KFbxLayerElement::eBY_POLYGON)
                    control->EnableWhatImportGroup(IFBXImportControl::importSmooths);
            }

            KFbxLayerElementMaterial *layerMat = layer->GetMaterials();
            if (layerMat) {
                for (int i = 0; i < layerMat->GetIndexArray().GetCount(); i++) {
                    int idx = layerMat->GetIndexArray().GetAt(i);
                    if (idx == -1) continue;
                    KFbxSurfaceMaterial* lMaterial = pMesh->GetNode()->GetMaterial(idx);
                    if (lMaterial) {
                        for (int j = 0; j <FBXTextureTypes::getCountTypes();j++)  {
                            KFbxProperty lProperty = lMaterial->FindProperty(FBXTextureTypes::getType(j));
                            int lNbTextures = lProperty.GetSrcObjectCount(KFbxTexture::ClassId);
                            if (lNbTextures>0) {
                                control->EnableWhatImportGroup(IFBXImportControl::importTextures);
                                control->EnableWhatImportGroup(IFBXImportControl::importRVMATRefs);
                                control->DefineUVSetType(i);
                                control->EnableTextureAtLayer(i,j);
                                goto out;
                            }
                        }
                    }
                }
            }
out:;

        }
    }
    int deformers = pMesh->GetDeformerCount(KFbxDeformer::eSKIN);
    if (deformers)
            control->EnableWhatImportGroup(IFBXImportControl::importSkinning);
    SceneWalker::Mesh(pGlobalPosition,pMesh,pVertexArray);

}

void FBXFileFeaturesDetector::TakeList(const FbxSdk::MScene &scene) {
    
    KArrayTemplate<KString *>   nameArr;
    scene->FillTakeNameArray(nameArr);
    for (int i = 0; i < nameArr.GetCount(); i++) {
        KString *itm = nameArr.GetAt(i);
        const char *takename = itm->Buffer();
        control->DefineNewTake(takename);
        KFbxTakeInfo *nfo = scene->GetTakeInfo(*itm);
        double sec = nfo?nfo->mLocalTimeSpan.GetDuration().GetSecondDouble():0;
        if (sec > 0.000000001)
            control->EnableWhatImportGroup(IFBXImportControl::importAnimation);
        if (nfo) 
            control->SetPreferedAnimationFPS(nfo->mLocalTimeSpan.GetStart().GetFrameRate(
                    nfo->mLocalTimeSpan.GetStart().GetGlobalTimeMode()));

    }

}

void FBXFileFeaturesDetector::LimbNode(KFbxNode *node, KFbxXMatrix& pGlobalBasePosition, 
                                       KFbxXMatrix& pGlobalEndPosition) {

    control->EnableWhatImportGroup(IFBXImportControl::importSkeleton);
}
