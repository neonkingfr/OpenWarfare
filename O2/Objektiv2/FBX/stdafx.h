// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

               
#ifndef VC_EXTRALEAN                         
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

// Modify the following defines if you have to target a platform prior to the ones specified below.
// Refer to MSDN for the latest info on corresponding values for different platforms.
#ifndef WINVER				// Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0501		// Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0500	// Change this to the appropriate value to target Windows 2000 or later.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0510 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 4.0 or later.
#define _WIN32_IE 0x0500	// Change this to the appropriate value to target IE 5.0 or later.
#endif


#include <afxwin.h>         // MFC core and standard components
#include <afxcmn.h>			// MFC support for Windows Common Controls
#include <afxext.h>         // MFC extensions
#include <afxmt.h>         // MFC extensions
//#pragma comment(lib,"fbxsdk_md2005d.lib")


#include <El/Pathname/Pathname.h>
#include <malloc.h>
#include "../StringRes.h"

#include <El/elementpch.hpp>
#include <El/paramFile/paramFile.hpp>


#include "FbxSdk.h"
#include <el/pathname/pathname.h>

#include <projects/objektivlib/Lodobject.h>
#include "package_version.h"

#ifdef _DEBUG
#ifdef _DLL
    #define VERSION_EXTRA "-debug-dynlibs"
#else
    #define VERSION_EXTRA "-debug-staticlibs"
#endif
#else
#ifdef _DLL
    #define VERSION_EXTRA "-release-dynlibs"
#else
    #define VERSION_EXTRA "-release-staticlibs"
#endif
#endif



using namespace ObjektivLib;

// TODO: reference additional headers your program requires here
