#pragma once
#include "scenewalker.h"
#include "IFBXPluginServices.h" 

class FBXFileFeaturesDetector : public SceneWalker
{
    IFBXImportControl *control;
public:
    FBXFileFeaturesDetector(IFBXImportControl *control);

    virtual void WalkNode(KFbxNode* pNode, 
                  KTime& lTime, 
                  KFbxXMatrix& pParentGlobalPosition,
                  KFbxXMatrix& pGlobalPosition);

    virtual void Mesh(KFbxXMatrix& pGlobalPosition, KFbxMesh* pMesh, 
                 KFbxVector4* pVertexArray);


    void TakeList(const FbxSdk::MScene &scene);

    virtual void LimbNode(KFbxNode *node, KFbxXMatrix& pGlobalBasePosition, 
                 KFbxXMatrix& pGlobalEndPosition);

};
