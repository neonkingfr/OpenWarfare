#include "StdAfx.h"
#include "FBXImportDlg.h"
#include "fbxrc.h"
#include "texturetypes.h"

extern "C" {
const char *GetPluginVersionString();
}

BEGIN_MESSAGE_MAP(CFBXImportDlg,CDialog)
    ON_NOTIFY(LVN_ENDLABELEDIT,IDC_UVSETASSIGN,OnEditUVSetAssign)
    ON_NOTIFY(NM_CLICK, IDC_OBJECTTREE,OnChangeObjectTree)
    ON_WM_DESTROY()
END_MESSAGE_MAP()

CFBXImportDlg::CFBXImportDlg(void):result(0)
{
}

CFBXImportDlg::~CFBXImportDlg(void)
{
}



static int maskIndex(int mask) {

    int idx = 0;
    if (mask == 0) return -1;
    while ((mask & 1) == 0) {mask = mask >> 1; idx++;}
    return idx;
}


BOOL CFBXImportDlg::OnInitDialog() 
{
    wCancel.SubclassDlgItem(IDOK,this);
    wOk.SubclassDlgItem(IDCANCEL,this);

    wMasterScale.SubclassDlgItem(IDC_MASTERSCALE,this);
    wTake.SubclassDlgItem(IDC_TAKE,this);
    wFPS.SubclassDlgItem(IDC_ANIMFPX,this);
    wImportAnim.SubclassDlgItem(IDC_CHB_ENABLEANIM,this);
    wObjectTree.SubclassDlgItem(IDC_OBJECTTREE,this);
    wUVSetAssign.SubclassDlgItem(IDC_UVSETASSIGN,this);
    wImportSkeleton.SubclassDlgItem(IDC_CHB_SKELETON,this);
    wImportRVMAT.SubclassDlgItem(IDC_CHB_RVMAT,this);
    wImportLODs.SubclassDlgItem(IDC_CHB_LODS,this);
    wImportSkin.SubclassDlgItem(IDC_CHB_SKINNING,this);
    wImportSharp.SubclassDlgItem(IDC_CHB_SMOOTH,this);
    wImportUVs.SubclassDlgItem(IDC_CHB_UVSETS,this);
    wImportTex.SubclassDlgItem(IDC_CHB_TEXTURES,this);
    wImportMesh.SubclassDlgItem(IDC_CHB_MESH,this);
    wPrepareMaterials.SubclassDlgItem(IDC_CREATERVMATS,this);
    wUVSetAssign.InsertColumn(0,"UVSet",0,50,0);
    wUVSetAssign.InsertColumn(1,"Layer",0,50,1);
    wUVSetAssign.InsertColumn(2,"Detected UVSet type",0,300,2);
    ListView_SetExtendedListViewStyleEx(wUVSetAssign,LVS_EX_GRIDLINES|LVS_EX_FULLROWSELECT,LVS_EX_GRIDLINES|LVS_EX_FULLROWSELECT);

    memset(buttonMap,0,sizeof(buttonMap));

    buttonMap[maskIndex(importMesh)] = &wImportMesh;
    buttonMap[maskIndex(importTextures)] = &wImportTex;
    buttonMap[maskIndex(importUVSets)] = &wImportUVs;
    buttonMap[maskIndex(importSmooths)] = &wImportSharp;
    buttonMap[maskIndex(importSkinning)] = &wImportSkin;
    buttonMap[maskIndex(importLODs)] = &wImportLODs;
    buttonMap[maskIndex(importRVMATRefs)] = &wImportRVMAT;
    buttonMap[maskIndex(importSkeleton)] = &wImportSkeleton;
    buttonMap[maskIndex(importAnimation)] = &wImportAnim;
    for (int i = 0; i < lenof(buttonMap); i++) buttonMap[i]->EnableWindow(FALSE);
    for (int i = -6; i < 7; i++) {
        char buff[50];
        sprintf_s(buff,50,"%g",exp(i * log(10)));
        int itm = wMasterScale.AddString(buff);
        if (i == 0) wMasterScale.SetCurSel(itm);
    }

    CString title;
    GetWindowText(title);
    title = title + " (version: " + GetPluginVersionString() + ")";
    SetWindowText(title);

    return FALSE;
}


bool CFBXImportDlg::StoreResult(const LODObject &object) 
{
    p3d = object;
    return true;
}
int CFBXImportDlg::GetImportMask()
{
    int mask = 0;
    for (int i = 0; i < lenof(buttonMap); i++)
        if (buttonMap[i]->GetCheck()) mask |= 1<< i;

    return mask;
}

int CFBXImportDlg::GetAnimationSpeed() 
{
    return GetDlgItemInt(wFPS.GetDlgCtrlID(),0,FALSE);
}

float CFBXImportDlg::GetMasterScale() 
{
    CString str;
    float scale = 1;
    wMasterScale.GetWindowText(str);
    sscanf(str,"%f",&scale);
    return scale;            
}

int CFBXImportDlg::GetUVSetForTextureType(int layer)
{
    UVAssign search(layer,0);
    for (std::vector<UVAssign>::const_iterator iter = assignedTextures.begin();
        iter != assignedTextures.end(); ++iter)
        if (iter->GetLayer() == search.GetLayer())
            return iter->GetUVSet();
    return -1;

}

void CFBXImportDlg::DefineNewTake(const char *takeName)
{
    int idx = wTake.AddString(takeName);
    wTake.SetCurSel(idx);
}

void CFBXImportDlg::DefineUVSetType(int layer) 
{
    if (GetUVSetForTextureType(layer)!=-1) 
        return;
    int idx = (int)assignedTextures.size();
    assignedTextures.push_back(UVAssign(layer,idx));
    CString noTextures(MAKEINTRESOURCE(IDS_NOTEXTURES));
    const char *texName = noTextures;

    char buff[100];
    int item = wUVSetAssign.InsertItem(idx,_itoa(idx,buff,10));
    wUVSetAssign.SetItemText(item,1,_itoa(layer,buff,10));
    wUVSetAssign.SetItemText(item,2,texName);
    wUVSetAssign.SetItemData(item,idx);
}

const char *CFBXImportDlg::GetChoosenTake()
{
    int sel = wTake.GetCurSel();
    if (sel < 0) return 0;
    wTake.GetLBText(sel,lastTake);
    return lastTake;
}

const char *CFBXImportDlg::AskPassword(char *buffer, std::size_t sz)
{
    return 0;
}
const char *CFBXImportDlg::GetTextureBasePath()
{
    return basePath;
}
bool CFBXImportDlg::OpenConfigurationDialog()
{
    CWnd *parent = GetParent();
    if (parent) parent->EnableWindow(FALSE);
    MSG msg;
    result = -1;

    while(GetMessageA(&msg,0,0,0)) {
        if (!IsDialogMessage(&msg)) {

            TranslateMessage(&msg);
            DispatchMessage(&msg);

        }
        if (result != -1)
            break;
    }

   ::SetCursor(::LoadCursor(0,IDC_WAIT));
    if (parent) parent->EnableWindow(TRUE);
    return result == IDOK;
}

bool CFBXImportDlg::CanImportObject(unsigned long objectId)
{
    TreeMap::const_iterator iter = treeMap.find(objectId);
    if (iter == treeMap.end()) return false;
    int res = wObjectTree.GetCheck(iter->second);
    return res == 1;
}

void CFBXImportDlg::DefineNewObject(const char *objectName, 
                                    unsigned long objectId, 
                                    unsigned long parentObjectId) {


                                        HTREEITEM parent = NULL;
                                        if (parentObjectId != 0) {

                                            TreeMap::const_iterator lookup = treeMap.find(parentObjectId);
                                            if (lookup != treeMap.end())
                                                parent = lookup->second;
                                        }

                                        HTREEITEM itm = wObjectTree.InsertItem(objectName,parent,TVI_SORT);
                                        wObjectTree.SetItemData(itm,objectId);
                                        TreeView_SetCheckState(wObjectTree,itm,TRUE);
                                        treeMap.insert(std::make_pair(objectId,itm));

}
void CFBXImportDlg::EnableWhatImportGroup(WhatImport groupEnabled) {

    int index = maskIndex(groupEnabled);
    buttonMap[index]->EnableWindow(TRUE);
    buttonMap[index]->SetCheck(TRUE);

}

void CFBXImportDlg::OnOK() {
    result = IDOK;    
}

void CFBXImportDlg::OnCancel() {
    result = IDCANCEL;
}

void CFBXImportDlg::OnEditUVSetAssign(NMHDR *hdr, LRESULT *res) 
{
    NMLVDISPINFO *dispnfo = reinterpret_cast<NMLVDISPINFO *>(hdr);
    const char *txt = dispnfo->item.pszText;
    *res = 0;
    int id;
    if (txt == 0) return;
    if (sscanf(txt,"%d", &id) == 1)  {
        assignedTextures[dispnfo->item.iItem].SetUVSet(id);
        * res = 1;
    }  
}

static void RecursiveCheck(CTreeCtrl &tree, HTREEITEM startParent, int state) 
{
    HTREEITEM c = tree.GetChildItem(startParent);
    while (c != NULL) {
        tree.SetCheck(c,state);
        RecursiveCheck(tree, c, state) ;
        c = tree.GetNextSiblingItem(c);
    }
}

bool CFBXImportDlg::DontCreateObjectSelections() {
    return IsDlgButtonChecked(IDC_NOOBJSELS) != 0;
}

void CFBXImportDlg::OnChangeObjectTree(NMHDR *hdr, LRESULT *res) 
{
    POINT pt;
    GetCursorPos(&pt);                                
    wObjectTree.ScreenToClient(&pt);
    TVHITTESTINFO hti;
    hti.pt.x = pt.x;
    hti.pt.y = pt.y;
    wObjectTree.HitTest(&hti);

    if (hti.flags & TVHT_ONITEMSTATEICON)
    {
        int bCheck = !wObjectTree.GetCheck(hti.hItem);
        if (bCheck == 1)  {
            HTREEITEM x = hti.hItem;
            x = wObjectTree.GetParentItem(x);
            while (x != NULL) {
                wObjectTree.SetCheck(x,TRUE);
                x = wObjectTree.GetParentItem(x);
            }

            if (GetKeyState(VK_SHIFT) >= 0) 
                RecursiveCheck(wObjectTree,hti.hItem,1);
        }
        else 
            RecursiveCheck(wObjectTree,hti.hItem,0);
    }


    *res = 0;
}

void CFBXImportDlg::OnDestroy() 
{
    CImageList *imgl = wObjectTree.GetImageList(TVSIL_STATE);
    imgl->DeleteImageList();
}

void CFBXImportDlg::EnableTextureAtLayer(int layer, int textureType)
{
    int idx = GetUVSetForTextureType(layer);
    int cnt = wUVSetAssign.GetItemCount();
    for (int i = 0; i< cnt; i++) {
        int iix = wUVSetAssign.GetItemData(i);
        if (assignedTextures[iix].GetUVSet() == idx)
            wUVSetAssign.SetItemText(i,2,FBXTextureTypes::getType(textureType));
    }
}

void CFBXImportDlg::SetPreferedAnimationFPS(int fps) {
    SetDlgItemInt(wFPS.GetDlgCtrlID(), fps, FALSE);
}


static void BuildRVMATFile(const Pathname &target, const CFBXImportDlg::DataForRVMat &data) {

    std::ofstream out(target,std::ios::out|std::ios::trunc);
    if (!out) {
        CString msg;
        AfxFormatString1(msg,IDS_UNABLETOCREATEFILE,target);
        AfxMessageBox(msg,MB_OK|MB_ICONEXCLAMATION);
        return;
    }

    out << "ambient[]={1.000000,1.000000,1.000000,1.000000};" << std::endl 
        << "diffuse[]={1.000000,1.000000,1.000000,1.000000};" << std::endl 
        << "forcedDiffuse[]={0.000000,0.000000,0.000000,0.000000};" << std::endl 
        << "emmisive[]={0.000000,0.000000,0.000000,1.000000};" << std::endl 
        << "specular[]={0.000000,0.000000,0.000000,1.000000};" << std::endl 
        << "specularPower=1.000000;" << std::endl
        << "PixelShaderID=\"Normal\";" << std::endl
        << "VertexShaderID=\"Basic\";" << std::endl;

    for (int i = 0; i < data.Size(); i++) {

        int uvset = data.uvSetsAndTextures[i].first;
        std::string tex = data.uvSetsAndTextures[i].second;

        out << "class Stage" << uvset << std::endl
            << "{" << std::endl
            << "\ttexture=\"" << tex.c_str() <<"\";" << std::endl
	        << "\tuvSource=\"tex" << uvset <<"\";"  << std::endl
	        << "\tclass uvTransform"  << std::endl
	        << "\t{"  << std::endl
		    << "\t\taside[]={1.000000,0.000000,0.000000};" << std::endl
		    << "\t\tup[]={0.000000,1.000000,0.000000};"  << std::endl
		    << "\t\tdir[]={0.000000,0.000000,0.000000};"  << std::endl
		    << "\t\tpos[]={0.000000,0.000000,0.000000};"  << std::endl
	        << "\t};"  << std::endl
            << "}; " << std::endl;
    }
}

const char *CFBXImportDlg::CreateRVMATPrototype(const DataForRVMat &data) {
    
    if (rvmatproto.empty() && wPrepareMaterials.GetCheck()==0)
        return 0;

    for (RVMATProto::const_iterator iter = rvmatproto.begin(); iter != rvmatproto.end(); ++iter) {

        if (iter->first == data) return iter->second.Data();
    }

    RString matname;

    for (int i = 0; i < data.Size(); i++) 
        matname = matname + "_" + Pathname(data.uvSetsAndTextures[i].second.c_str()).GetTitle();

    matname = matname;
    matname = matname.Data()+1;

    if (rvmatTarget.GetLength() == 0) {
        CFileDialog fdlg(FALSE,"",CString(MAKEINTRESOURCE(IDS_BROWSEFORDIRECTORYNAME)),OFN_PATHMUSTEXIST,CString(MAKEINTRESOURCE(IDS_BROWSEFORDIRECTORY)));
        if (fdlg.DoModal() == IDOK) {
            CString pathname = fdlg.GetPathName();
            rvmatTarget = Pathname(pathname).GetDirectoryWithDrive();
        }
        else
        {
            return 0;
        }
    }


    Pathname target;
    target.SetDirectory(rvmatTarget);
    target.SetFiletitle(matname);
    target.SetExtension(".rvmat");

    rvmatproto.push_back(std::make_pair(data,RStringI(target)));

    if (!target.TestFile()) {
        
        BuildRVMATFile(target,data);
        
        
    }

    return rvmatproto.at(rvmatproto.size() - 1).second.Data();

}

void CFBXImportDlg::ImportError(int code,const char *popis, const char* pFilename) {
    msgDlg.Format(IDS_ERR_IMPORTERERROR,code,popis,pFilename);
}
void CFBXImportDlg::ImportInitError(int code,const char *popis, const char* pFilename) {
    msgDlg.Format(IDS_ERR_IMPORTINITERR,code,popis,pFilename);
}
void CFBXImportDlg::UnsupportedReferenceModeForUVCoords(const char *objName, int mapMode) {
    msgDlg.Format(IDS_ERR_UNSUPREFMODEUV,objName,mapMode);
}
void CFBXImportDlg::UnsupportedReferenceModeForTextures(const char *objName, int mapMode) {
    msgDlg.Format(IDS_ERR_UNSUPREFMODETEX,objName,mapMode);
}
void CFBXImportDlg::UnsupportedReferenceModeForSmoothing(const char *objName, int mapMode){
    msgDlg.Format(IDS_ERR_UNSUPREFMODESMT,objName,mapMode);
}
void CFBXImportDlg::InvalidFPS(int fps) {
    msgDlg.Format(IDS_ERR_INVALIDFPS,fps);
}
void CFBXImportDlg::InvalidIndexInArray(const char *objname, int layertype) {
//TODO:    msgDlg.Format(IDS_ERR_INVALIDINDEXINARRAY,objname,layerElements[layertype]);
}
