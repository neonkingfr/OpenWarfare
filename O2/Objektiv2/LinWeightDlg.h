#if !defined(AFX_LINWEIGHTDLG_H__91A2ED74_6017_4350_851E_E8DA8436EFBC__INCLUDED_)
#define AFX_LINWEIGHTDLG_H__91A2ED74_6017_4350_851E_E8DA8436EFBC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LinWeightDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLinWeightDlg dialog

class CLinWeightDlg : public CDialog
  {
  // Construction
  CPoint last1,last2;		
  float left,right,top,bottom;
  float xa,ya;
  Selection saved;
  CRect extent;
  public:
    void MakeSel();
    CWnd *drwwnd;
    LPHMATRIX scrtrans;
    ObjectData *curobj;
    
    CLinWeightDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CLinWeightDlg)
    enum 
      { IDD = IDD_LINWEIGHTSEL };
    CStatic	wDrawFrame;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CLinWeightDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CLinWeightDlg)
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    virtual BOOL OnInitDialog();
    afx_msg void OnBottom();
    afx_msg void OnLeft();
    afx_msg void OnRight();
    afx_msg void OnTop();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LINWEIGHTDLG_H__91A2ED74_6017_4350_851E_E8DA8436EFBC__INCLUDED_)
