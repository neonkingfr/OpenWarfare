#if !defined(AFX_FLATTENPOINTSDLG_H__7AB85DC9_EA6A_4BC1_876F_ABD9BD8DCB99__INCLUDED_)
#define AFX_FLATTENPOINTSDLG_H__7AB85DC9_EA6A_4BC1_876F_ABD9BD8DCB99__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FlattenPointsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFlattenPointsDlg dialog

class CFlattenPointsDlg : public CDialog
  {
  // Construction
  public:
    CFlattenPointsDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CFlattenPointsDlg)
    enum 
      { IDD = IDD_FLATTENPOINTS };
    int		mode;
    BOOL	pin;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CFlattenPointsDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CFlattenPointsDlg)
    // NOTE: the ClassWizard will add member functions here
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FLATTENPOINTSDLG_H__7AB85DC9_EA6A_4BC1_876F_ABD9BD8DCB99__INCLUDED_)
