// LexTree.cpp: implementation of the CLexTree class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LexTree.h"
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLexTree::CLexTree()
  {
  tree=NULL;
  }

//--------------------------------------------------

CLexTree::~CLexTree()
  {
  delete tree;
  }

//--------------------------------------------------

bool CLexTree::GeneralOp::AddToTree(GeneralOp **ptr)
  {
  GeneralOp *tree=*ptr;
  if (tree==NULL || tree->priority>priority)
    {
    if (tree!=NULL)
      {
      if (num<1 || child[0]) return false;
      child[0]=*ptr;
      }
    *ptr=this;
    return true;
    }
  for (int i=0;i<tree->num;i++) if (tree->child[i]==NULL) 
    {
    tree->child[i]=this;return true;
    }
  if (tree->num>0)
    return AddToTree(tree->child+tree->num-1);
  return false;	
  }

//--------------------------------------------------

float * CLexTree::Varible::Calculate(float *tostore, bool optimize)
  {
  if (tostore==NULL) return var;
  if (optimize) return NULL;
  *tostore=*var;
  return tostore;
  }

//--------------------------------------------------

float * CLexTree::Number::Calculate(float *tostore, bool optimize)
  {
  if (tostore==NULL) return NULL;
  *tostore=num;
  return tostore;
  }

//--------------------------------------------------

float * CLexTree::SpecialVarible::Calculate(float *tostore,bool optimize)
  {
  if (tostore==NULL) return lvalue;
  if (optimize) return NULL;
  *tostore=*varptr;
  return tostore;
  }

//--------------------------------------------------

float * CLexTree::UnOp::Calculate(float *tostore, bool optimize)
  {
  if (tostore==NULL) return NULL;
  float *out;
  if (child[0]==NULL) return NULL;
  out=child[0]->Calculate(tostore,optimize);
  if (out==NULL) return NULL;
  switch (symb)
    {
    case lexPlus: break;
    case lexMinus: *out=-*out;break;
    case lexFSin: *out=(float)sin(*out);break;
    case lexFCos: *out=(float)cos(*out);break;
    case lexFTan: *out=(float)(sin(*out)/cos(*out));break;
    case lexFExp: *out=(float)exp(*out);break;
    case lexFLn: *out=(float)log(*out);break;
    case lexFSqr: *out=(*out)*(*out);break;
    case lexFSqrt: *out=(float)sqrt(*out);break;
    case lexFAtan: *out=(float)atan(*out);break;	
    default:return NULL;
    }  
  return out;
  }

//--------------------------------------------------

float * CLexTree::BinOp::Calculate(float *tostore, bool optimize)
  {
  if (tostore==NULL) return NULL;
  float left,right;
  float *lpleft,*lpright;
  
  if (child[0]==NULL) return NULL;
  if (child[1]==NULL) return NULL;
  lpleft=child[0]->Calculate(&left,optimize);
  lpright=child[1]->Calculate(&right,optimize);
  if (optimize)
    {
    if (lpleft)
      {
      delete child[0];
      child[0]=new Number(left);
      }
    if (lpright)
      {
      delete child[1];
      child[1]=new Number(right);
      }
    if (lpleft==NULL || lpright==NULL) return NULL;
    }
  switch (symb)
    {
    case lexCommdiv:
    case lexComma: *tostore=right;break;
    case lexPlus: *tostore=left+right;break;
    case lexMinus: *tostore=left-right;break;
    case lexMult: *tostore=left*right;break;
    case lexDiv: *tostore=left/right;break;
    case lexEqual: *tostore=left==right;break;
    case lexGreat: *tostore=left>right;break;
    case lexLower: *tostore=left<right;break;
    case lexGEq: *tostore=left>=right;break;
    case lexLEq: *tostore=left<=right;break;
    case lexNoEq: *tostore=left!=right;break;
    case lexAnd: *tostore=(left!=0) && (right!=0);break;
    case lexOr: *tostore=(left!=0) || (right!=0);break;
    case lexXor: *tostore=(float)((left!=0) ^ (right!=0));break;
    case lexRad: *tostore=(float)atan2(left,right);break;
    default : return NULL;
    }
  return tostore;
  }

//--------------------------------------------------

float * CLexTree::Command::Calculate(float *tostore, bool optimize)
  {
  if (tostore==NULL) return NULL;
  float left,right;
  float *lpleft,*lpright;
  
  if (child[0]!=NULL) lpleft=child[0]->Calculate(&left,optimize);else lpleft=NULL;
  if (child[1]!=NULL) lpright=child[1]->Calculate(&right,optimize);else lpright=NULL;
  if (optimize)
    {
    if (lpleft)
      {
      delete child[0];
      child[0]=new Number(left);
      }
    if (lpright)
      {
      delete child[1];
      child[1]=new Number(right);
      }
    if (lpleft==NULL || lpright==NULL) return NULL;
    } 
  if (lpright==NULL) *tostore=0;else *tostore=right;
  return tostore;
  }

//--------------------------------------------------

float * CLexTree::Assign::Calculate(float *tostore, bool optimize)
  {
  if (tostore==NULL) return NULL;
  if (child[0]==NULL) return NULL;
  if (child[1]==NULL) return NULL;
  float *p;
  p=child[1]->Calculate(tostore,optimize);
  if (optimize)
    {
    if (p)
      {
      delete child[1];
      child[1]=new Number(*p);
      }
    return NULL;
    }
  float *save=child[0]->Calculate(NULL,false);
  *save=*p;  
  return p;
  }

//--------------------------------------------------

float *CLexTree::Iff::Calculate(float *tostore, bool optimize)
  {
  if (tostore==NULL) return NULL;
  if (child[0]==NULL) return NULL;
  if (child[1]==NULL) return NULL;
  if (child[2]==NULL) return NULL;
  float *fout,out;
  fout=child[0]->Calculate(&out,optimize);
  if (optimize)
    {
    float *lf,f;
    if (fout)
      {
      delete child[0];
      child[0]=new Number(out);
      }
    lf=child[1]->Calculate(&f,optimize);
    if (lf)
      {
      delete child[1];
      child[1]=new Number(f);
      }
    lf=child[2]->Calculate(&f,optimize);
    if (lf)
      {
      delete child[2];
      child[2]=new Number(f);
      }
    if (fout==NULL) return NULL;
    }
  if (out!=0.0f)	
    return child[1]->Calculate(tostore,false);
  else 
    return child[2]->Calculate(tostore,false);
  }

//--------------------------------------------------

float *CLexTree::While::Calculate(float *tostore, bool optimize)
  {
  if (tostore==NULL) return NULL;
  if (child[0]==NULL) return NULL;
  if (child[1]==NULL) return NULL;
  if (optimize)
    {
    float f;
    if (child[1]->Calculate(&f,true)!=NULL)
      {
      delete child[1];
      child[1]=new Number(f);
      }
    return NULL;
    }
  float f;
  do
    {
    if (child[0]->Calculate(&f,false)==NULL) return NULL;
    if (f!=0.0f) 
      {if (child[1]->Calculate(&f,false)==NULL) return NULL;}
    else 
      break;
    }
  while(1);
  *tostore=f;
  return tostore;
  }

//--------------------------------------------------

float *CLexTree::Repeat::Calculate(float *tostore, bool optimize)
  {
  if (tostore==NULL) return NULL;
  if (optimize) 
    {
    float out;
    child[0]->Calculate(&out,false);
    return NULL;
    }
  if (child[0]==NULL) return NULL;
  float out;
  if (child[0]->Calculate(&out,false)==NULL) return NULL;
  while(out!=0.0)
    {
    if (child[0]->Calculate(&out,false)==NULL) return NULL;
    }
  *tostore=out;
  return tostore;
  }

//--------------------------------------------------

float *CLexTree::For::Calculate(float *tostore, bool optimize)
  {
  if (tostore==NULL) return NULL;
  for (int i=0;i<3;i++)
    {
    float out;
    if (child[i]==NULL) return NULL;
    if (optimize && child[i]->Calculate(&out,true)!=NULL)
      {
      delete child[i];
      child[i]=new Number(out);
      }	
    }
  if (optimize) return NULL;  
  if (child[0]->Calculate(tostore,false)==NULL) return NULL;
  if (child[1]->Calculate(tostore,false)==NULL) return NULL;
  while (*tostore!=0.0f)
    {
    if (child[2]->Calculate(tostore,false)==NULL) return NULL;
    if (child[1]->Calculate(tostore,false)==NULL) return NULL;
    }
  return tostore;
  }

//--------------------------------------------------

CLexTree::GeneralOp *CLexTree::BuildTree(CLexAnl &lex,LexSymbol stop)
  {
  GeneralOp *top=NULL;  
  bool number=true;
  
  while (lex.GetAct()!=stop)
    {
    GeneralOp *opr;
    if (number)
      switch(lex.GetAct())
        {
        case lexNumber: opr=new Number(lex.GetNumber());break;
        case lexVarible: opr=new Varible(varspace+lex.GetVarible()-'A');break;
        case lexVertX: opr=new SpecialVarible(&xval,&lxval);break;
        case lexVertY: opr=new SpecialVarible(&yval,&lyval);break;
        case lexVertZ: opr=new SpecialVarible(&zval,&lzval);break;
        case lexFaceTu: opr=new SpecialVarible(&tuval,&ltuval);break;
        case lexFaceTv: opr=new SpecialVarible(&tvval,&ltvval);break;
        case lexWeight: opr=new SpecialVarible(&weightval,&lweightval);break;
        case lexFSin:
        case lexFCos:
        case lexFTan:
        case lexFExp:
        case lexFLn:
        case lexFSqr:
        case lexFSqrt: 
        case lexFAtan:
        case lexPlus:
        case lexMinus: opr=new UnOp(lex.GetAct());number=!number;break;
        case lexLP:lex.ReadNext();opr=BuildTree(lex,lexRP);if (opr) opr->SetPriority(10000);break;
        case lexIff: 
          {
          opr=NULL;
          GeneralOp *o1,*o2,*o3;
          lex.ReadNext();if (lex.GetAct()!=lexLP) break;
          lex.ReadNext();if ((o1=BuildTree(lex,lexComma))==NULL) break;
          lex.ReadNext();if ((o2=BuildTree(lex,lexComma))==NULL) 
            {delete o1;break;}
          lex.ReadNext();if ((o3=BuildTree(lex,lexRP))==NULL) 
            {delete o2;delete o1;break;}
          opr=new Iff(o1,o2,o3);		  
          }
        break;
        case lexRepeat: 
          {
          opr=NULL;
          GeneralOp *o1;
          lex.ReadNext();if (lex.GetAct()!=lexLP) break;
          lex.ReadNext();if ((o1=BuildTree(lex,lexRP))==NULL) break;
          opr=new Repeat(o1);		  
          }
        break;
        case lexWhile:
          {
          opr=NULL;
          GeneralOp *o1,*o2;
          lex.ReadNext();if (lex.GetAct()!=lexLP) break;
          lex.ReadNext();if ((o1=BuildTree(lex,lexComma))==NULL) break;
          lex.ReadNext();if ((o2=BuildTree(lex,lexRP))==NULL) 
            {delete o1;break;}
          opr=new While(o1,o2);		  
          }
        break;
        case lexFor: 
          {
          opr=NULL;
          GeneralOp *o1,*o2,*o3;
          lex.ReadNext();if (lex.GetAct()!=lexLP) break;
          lex.ReadNext();if ((o1=BuildTree(lex,lexComma))==NULL) break;
          lex.ReadNext();if ((o2=BuildTree(lex,lexComma))==NULL) 
            {delete o1;break;}
          lex.ReadNext();if ((o3=BuildTree(lex,lexRP))==NULL) 
            {delete o2;delete o1;break;}
          opr=new For(o1,o2,o3);		  
          }
        break;
        default: opr=NULL;break;
        }
    else
      switch (lex.GetAct())
        {
        case lexPlus:
        case lexMinus: opr=new BinOp(lex.GetAct(),3000);break;
        case lexMult:
        case lexRad:
        case lexDiv: opr=new BinOp(lex.GetAct(),4000);break;
        case lexAssign:opr=new Assign();break;
        case lexGreat:
        case lexLower:
        case lexEqual:
        case lexGEq:
        case lexLEq:
        case lexNoEq: opr=new BinOp(lex.GetAct(),2000);break;
        case lexAnd: opr=new BinOp(lex.GetAct(),1750);break;
        case lexOr:	opr=new BinOp(lex.GetAct(),1700);break;
        case lexXor: opr=new BinOp(lex.GetAct(),1650);break;
        case lexCommdiv:opr=new Command();break;	  
        case lexComma: opr=new BinOp(lex.GetAct(),1000);break;	  
        default: opr=NULL;break;		  
        }	
    if (opr==NULL)
      {delete top; return NULL;}
    if (opr->AddToTree(&top)==false)
      {delete top; return NULL;}
    number=!number;
    lex.ReadNext();
    }
  return top;
  }

//--------------------------------------------------

bool CLexTree::Parse(CLexAnl &lex)
  {
  delete tree;
  float out;
  tree=BuildTree(lex,lexEof);
  if (tree==NULL) return false;
  if (tree->Calculate(&out,true)!=NULL)
    {
    delete tree;
    tree=new Number(out);
    }
  return true;
  }

//--------------------------------------------------

bool CLexTree::Calculate(float &out)
  {
  if (tree==NULL) return false;
  if (tree->Calculate(&out,false)!=NULL) return true;
  return false;
  }

//--------------------------------------------------

#include <strstream>
using namespace std;

int CLexTree::Parse(const char *program)
  {
  istrstream prg((char *)program,strlen(program)+1);
  CLexAnl lexanl(prg);
  bool ok=Parse(lexanl);
  if (!ok)	
    return prg.tellg();	
  else
    return 0;
  }

//--------------------------------------------------

void *CLexTree::SwitchTree(void *other)
  {
  void *old=(void *) tree;
  tree=(GeneralOp *)other;
  return old;
  }

//--------------------------------------------------

