// Objektiv2Doc.cpp : implementation of the CObjektiv2Doc class
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "MainFrm.h"
#include "Objektiv2Doc.h"
#include "Objektiv2View.h"
#include "ROCheck.h"
#include "IDeletePointsTriangulate.h"
#include "ifindnonconvexpoints.h"
#include "icreateconvexvolume.h"
#include "oldStuff\pal2pac.hpp"
#include "../ObjektivLib/bitxtimport.h"
#include "../ObjektivLib/ObjToolAnimation.h"
#include "../ObjektivLib/ObjToolClipboard.h"
#include "../ObjektivLib/ObjToolsMatLib.h"
#include "../ObjektivLib/ObjToolProxy.h"
#include "../ObjektivLib/ObjToolTopology.h"
#include "ProxyNameDlg.h"
#include "DlgSccCheckInOut.h"
#include <el/Scc/MsSccProject.hpp>
#include "DlgProxyLevel.h"
#include "CIUVSetActions.h"
#include "dlginputfile.h"
#include <sstream>


//#include "ObjectColorizer.h"
#include "SpaceWarpDlg.h"
#include <io.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <malloc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CObjektiv2Doc


IMPLEMENT_DYNCREATE(CObjektiv2Doc, CDocument)

BEGIN_MESSAGE_MAP(CObjektiv2Doc, CDocument)
  //{{AFX_MSG_MAP(CObjektiv2Doc)
  ON_COMMAND(ID_FILE_SAVE_AS, OnFileSaveAs)
  ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
  ON_COMMAND(ID_FILE_SAVE, OnFileSave)
  ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
  ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
  ON_COMMAND(ID_EDIT_REDO, OnEditRedo)
  ON_UPDATE_COMMAND_UI(ID_EDIT_REDO, OnUpdateEditRedo)
  ON_COMMAND(ID_VIEW_REFRESH, OnViewRefresh)
  ON_COMMAND(ID_UPDATE_ALL_VIEWS, OnUpdateAllViews)
  ON_COMMAND(ID_VIEW_HIDESEL, OnViewHidesel)
  ON_COMMAND(ID_VIEW_LOCKSEL, OnViewLocksel)
  ON_COMMAND(ID_VIEW_UNHIDESEL, OnViewUnhidesel)
  ON_COMMAND(ID_VIEW_UNLOCK, OnViewUnlock)
  ON_COMMAND(ID_EDIT_SELECT_ALL, OnEditSelectAll)
  ON_COMMAND(ID_EDIT_UNSELECTTRIANGLES, OnEditUnselectTriangles)
  ON_COMMAND(ID_EDIT_DESELECT, OnEditDeselect)
  ON_COMMAND(ID_EDIT_DESELECTVERTICES, OnEditDeselectVertices)
  ON_COMMAND(ID_EDIT_DESELECTFACES, OnEditDeselectFaces)
  ON_COMMAND(ID_EDIT_SHRINKSELECTED, OnShrinkSelect)
  ON_COMMAND(ID_EDIT_GROWSELECTED, OnGrowSelect)  
  ON_COMMAND(ID_EDIT_INVERTSELECTION, OnEditInvertselection)
  ON_COMMAND(ID_EDIT_CENTERPIN, OnEditCenterpin)
  ON_COMMAND_RANGE(ID_TRANSF_MOVE,ID_TRANSF_SCALE, OnTransform)
  ON_COMMAND(ID_TRANSFORM_USER, OnTransformUser)
  ON_COMMAND(ID_SURFACES_SPEHEREMAP, OnSurfacesSpeheremap)
  ON_UPDATE_COMMAND_UI(ID_SURFACES_CYLMAP, OnUpdateSurfacesCylmap)
  ON_UPDATE_COMMAND_UI(ID_SURFACES_SPEHEREMAP, OnUpdateSurfacesSpeheremap)
  ON_COMMAND_RANGE(ID_SURFACES_RECALCULATENORMALS,ID_STRUCTURE_TRIANGULATE_CONCAVE,OnSimpleCommand)	
  ON_COMMAND(ID_POINTS_PROPERTIES, OnPointsProperties)
  ON_COMMAND(ID_FACE_PROPERTIES, OnFaceProperties)
  ON_UPDATE_COMMAND_UI(ID_FACE_PROPERTIES, OnUpdateFaceProperties)
  ON_COMMAND(ID_VIEW_COLORIZEOBJECTS, OnViewColorizeobjects)
  ON_COMMAND(ID_EDIT_LOCKPIN, OnEditLockpin)
  ON_UPDATE_COMMAND_UI(ID_EDIT_LOCKPIN, OnUpdateEditLockpin)
  ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
  ON_COMMAND(ID_EDIT_CUT, OnEditCut)
  ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
  ON_COMMAND(ID_SURFACES_MAPPING, OnSurfacesMapping)
  ON_COMMAND(ID_UPDATE_MODEL, OnUpdateModel)
  ON_COMMAND(ID_FACES_EXTRUDE, OnFacesExtrude)
  ON_COMMAND(ID_TRANS2_ROTATE, OnTrans2Rotate)
  ON_COMMAND(ID_TRANS2_SCALE, OnTrans2Scale)
  ON_COMMAND(ID_TRANS2_MIRRORX, OnTrans2Mirrorx)
  ON_COMMAND(ID_TRANS2_MIRRORY, OnTrans2Mirrory)
  ON_COMMAND(ID_CREATE_SHAPE, OnCreateShape)
  ON_COMMAND(ID_EDIT_ELASTICSEL, OnEditElasticsel)
  ON_UPDATE_COMMAND_UI(0, OnUpdateStatusBar)
  ON_COMMAND(ID_VIEW_RESTARTEXTERNAL, OnViewRestartexternal)
  ON_COMMAND_EX(ID_FILE_MERGE, OnFileMerge)
  ON_COMMAND(ID_FILE_EXPORT_3DSTUDIO, OnFileExport3dstudio)
  ON_COMMAND(ID_VIEW_CENTERZOOM_AUTO, OnViewCenterzoomAuto)
  ON_COMMAND(ID_EDIT_SELECTONESIDE, OnEditSelectoneside)
  ON_COMMAND(ID_POINTS_MERGENEAR, OnPointsMergenear)
  ON_COMMAND(ID_FACES_SPLITHOLE, OnFacesSplithole)
  ON_COMMAND_RANGE(ID_FACES_MOVETOP ,ID_FACES_MOVETOPREVALPHA,OnFacesMove)
  ON_COMMAND(ID_STRUCTURE_MULTIPLEPINCULL, OnStructureMultiplepincull)
  ON_COMMAND(ID_CREATE_PROXY, OnCreateProxy)
  ON_COMMAND(ID_FILE_SAVETOSERVER, OnFileSavetoserver)
  ON_UPDATE_COMMAND_UI(ID_FILE_SAVETOSERVER, OnUpdateFileSavetoserver)
  ON_COMMAND(ID_POINTS_FLATTEN, OnPointsFlatten)
  ON_COMMAND(IDS_TOOLS_ANIMNORM, OnToolsAnimnorm)
  ON_COMMAND(ID_FILE_IMPORT3DS, OnFileImport3ds)
  ON_COMMAND(ID_TOOLS_COPYWEIGHTS, OnToolsCopyweights)
  ON_COMMAND(ID_SURFACES_SHOWGISMO, OnSurfacesShowgismo)
  ON_COMMAND(ID_SURFACES_EDITGISMO, OnSurfacesEditgismo)
  ON_COMMAND(ID_SURFACES_RESETGISMO, OnSurfacesResetgismo)
  ON_COMMAND(ID_SURFACES_UNDOGIZMO, OnSurfacesUndogizmo)
  ON_COMMAND(ID_SURFACES_GIZMOMAPPING, OnSurfacesGizmomapping)
  ON_COMMAND(ID_FILE_EXPORT_MDA, OnFileExportMda)
  ON_COMMAND(ID_TOOLS_FIND3WEIGHTS, OnToolsFind3weights)
  ON_COMMAND(ID_TOOLS_WEIGHTAUTOMATION, OnToolsWeightautomation)
  ON_COMMAND(ID_TOOLS_ADAPTOVANIANIMACE, OnToolsAdaptovaniAnimace)
  ON_COMMAND(ID_FILE_EXPORT_P3DOLDVERSION,OnFileExportOldVersion)
  ON_COMMAND(ID_FILE_IMPORT_ASFAMC,OnFileImportASFAMC)
  ON_COMMAND(ID_FILE_IMPORT_BIOVISION,OnFileImportBVH)
  ON_COMMAND(ID_STRUCTURE_SQUARIZEALLLODS, OnStructureSquarizealllods)
  ON_COMMAND(ID_STRUCTURE_GHOSTS_SHOW, OnStructureGhostsShow)
  ON_UPDATE_COMMAND_UI(ID_STRUCTURE_GHOSTS_SHOW, OnUpdateStructureGhostsShow)
  ON_COMMAND(ID_STRUCTURE_GHOSTS_SELECT, OnStructureGhostsSelect)
  ON_UPDATE_COMMAND_UI(ID_STRUCTURE_GHOSTS_SELECT, OnUpdateStructureGhostsSelect)
  ON_COMMAND(ID_STRUCTURE_GHOST_MATERIALIZE, OnStructureGhostMaterialize)
  ON_UPDATE_COMMAND_UI(ID_STRUCTURE_GHOST_MATERIALIZE, OnUpdateStructureGhostMaterialize)
  ON_COMMAND(ID_STRUCTURE_GHOST_CREATE, OnStructureGhostCreate)
  ON_COMMAND(ID_SURFACES_TEXELCALCULATIONS, OnSurfacesTexelcalculations)
  ON_COMMAND(ID_POINTS_LOCKNORMALS, OnPointsLocknormals)
  ON_COMMAND(ID_FILE_IMPORT_MAYAWEIGHTSCRIPT, OnFileImportMayaweightscript)
  ON_COMMAND(ID_FILE_IMPORT_BIANMMAYAANIMATIONEXPORT, OnFileImportBianmmayaanimationexport)
  ON_COMMAND(ID_FILE_IMPORT_OBJFILE, OnFileImportObjfile)
  ON_COMMAND(ID_VIEW_VIEWERS_FREEZEANIMATION, OnViewViewersFreezeanimation)
  ON_UPDATE_COMMAND_UI(ID_VIEW_VIEWERS_FREEZEANIMATION, OnUpdateViewViewersFreezeanimation)
  ON_COMMAND(ID_SURFACES_CHILLISKINNER, OnSurfacesChilliskinner)
  ON_COMMAND(ID_STRUCTURE_CARVE, OnStructureCarve)
  ON_COMMAND(ID_TOOLS_MASSTEXTUREMATERIALRENAMING, OnToolsMasstexturematerialrenaming)
  ON_UPDATE_COMMAND_UI(ID_POINTS_PROPERTIES, OnUpdateFaceProperties)
  ON_COMMAND_EX(ID_FILE_MERGEASSELECTION, OnFileMerge)
  ON_COMMAND(ID_STRUCTURE_CHECKTEXTURES, OnStructureChecktextures)
  ON_COMMAND(ID_POINTS_REMOVE,OnPointsRemove)
  ON_COMMAND(ID_CONVEXITY_FINDNCPOINTS, OnConvexityFindncpoints)
  ON_COMMAND(ID_CONVEXITY_NEWCONVEXHULL, OnConvexityNewconvexhull)
  ON_COMMAND(ID_IMPORT_UNIVERSALBISTUDIOTXT, OnImportUniversalbistudiotxt)
  ON_COMMAND(ID_SOURCECONTROL_SAVEANDCHECKIN, OnSourcecontrolSaveandcheckin)
  ON_COMMAND(ID_SOURCECONTROL_GETLATESTVERSION, OnSourcecontrolGetlatestversion)
  ON_COMMAND(ID_SOURCECONTROL_UNDOCHECKOUT, OnSourcecontrolUndocheckout)
  ON_COMMAND(ID_SOURCECONTROL_CHECKOUT, OnSourcecontrolCheckout)
  ON_COMMAND(ID_SOURCECONTROL_SHOWHISTORY, OnSourcecontrolShowhistory)
  ON_UPDATE_COMMAND_UI(ID_SOURCECONTROL_SAVEANDCHECKIN, OnUpdateSourceControl_Loaded)
  ON_UPDATE_COMMAND_UI(ID_SOURCECONTROL_GETLATESTVERSION, OnUpdateSourceControl_GetVer)
  ON_UPDATE_COMMAND_UI(ID_SOURCECONTROL_UNDOCHECKOUT, OnUpdateSourceControl_CheckedOut)
  ON_UPDATE_COMMAND_UI(ID_SOURCECONTROL_CHECKOUT, OnUpdateSourceControl_CheckedIn)
  ON_UPDATE_COMMAND_UI(ID_SOURCECONTROL_SHOWHISTORY, OnUpdateSourceControl_Controlled)
  ON_COMMAND(ID_FILE_OPENFROMSS, OnFileOpenfromss)
  ON_UPDATE_COMMAND_UI(ID_FILE_OPENFROMSS, OnUpdateSourceControl_Loaded)
  ON_COMMAND(ID_SOURCECONTROL_REDOCHECKOUT, OnSourcecontrolRedocheckout)
  ON_UPDATE_COMMAND_UI(ID_SOURCECONTROL_REDOCHECKOUT, OnUpdateSourceControl_RedoCheckout)
  ON_COMMAND(ID_CONVEXITY_CONVEXITYFINETUNE, OnConvexityConvexityfinetune)
  ON_COMMAND(ID_STRUCTURE_FINDFLAGS, OnStructureFindflags)
  ON_COMMAND(ID_EXPORT_BITXT, OnExportBitxt)
  //    ON_COMMAND(ID_DEGENERATEDFACES_CHECK, OnDegeneratedfacesCheck)
  ON_COMMAND(ID_VIEW_PROXYLEVEL, OnViewProxylevel)
  ON_COMMAND(ID_TRANSFORM3D_LOCALAXISMENU, OnTransform3dLocalaxismenu)
  ON_COMMAND(ID_TRANSFORM3D_EDTLOCALAXIS, OnTransform3dEdtlocalaxis)
  ON_UPDATE_COMMAND_UI(ID_TRANSFORM3D_EDTLOCALAXIS, OnUpdateTransform3dEdtlocalaxis)
  ON_COMMAND(ID_TRANSFORM3D_SHOWLOCALAXIS, OnTransform3dShowlocalaxis)
  ON_UPDATE_COMMAND_UI(ID_TRANSFORM3D_SHOWLOCALAXIS, OnUpdateTransform3dShowlocalaxis)
  ON_COMMAND(ID_TRANSFORM3D_SELECTIONWITHLOCALAXIS, OnTransform3dSelectionwithlocalaxis)
  ON_UPDATE_COMMAND_UI(ID_TRANSFORM3D_SELECTIONWITHLOCALAXIS, OnUpdateTransform3dSelectionwithlocalaxis)
  ON_COMMAND(ID_TRANSFORM3D_ATTACHTOFACE, OnTransform3dAttachtoface)
  ON_COMMAND(ID_TRANSFORM3D_FROMPROXY, OnTransform3dFromproxy)
  ON_COMMAND_RANGE(ID_UVSETS_ACTIVATE0,ID_UVSETS_ACTIVATE100,OnUvSetActivate)
  ON_COMMAND(ID_UVSETS_ADD, OnUvsetsAdd)
  ON_COMMAND(ID_UVSETS_DELETEACTIVE, OnUvsetsDeleteactive)
  ON_UPDATE_COMMAND_UI(ID_UVSETS_DELETEACTIVE, OnUpdateUvsetsDeleteactive)
  ON_UPDATE_COMMAND_UI(ID_UVSETS_CHANGEID, OnUpdateUvsetsDeleteactive)
  ON_COMMAND(ID_UVSETS_CHANGEID, OnUvsetsChangeid)
  ON_COMMAND(ID_SURFACES_MERGETEXTURES,OnSurfacesMergetextures)
  ON_COMMAND_RANGE(ID_SELOP_RECALL,ID_SELOP_COMPINTER, OnSelectionOperation)

  ON_COMMAND(ID_TRANSFORMRECORD_RECORDFREEHANDTRANSFORM33843,OnRecordTransform)
  ON_UPDATE_COMMAND_UI(ID_TRANSFORMRECORD_RECORDFREEHANDTRANSFORM33843,OnUpdateRecordTransform)
  ON_COMMAND(ID_TRANSFORMRECORD_USERECORDEDTRANSFORM,OnUseRecordedTransform)
  ON_COMMAND(ID_TRANSFORMRECORD_SAVE,OnSaveRecordedTransform)
  ON_COMMAND(ID_TRANSFORMRECORD_LOAD,OnLoadRecordedTransform)
  END_MESSAGE_MAP()
  //}}AFX_MSG_MAP

/////////////////////////////////////////////////////////////////////////////
// CObjektiv2Doc construction/destruction

// REST OF SOURCES ARE IN Objektiv2Doc2.cpp

void CObjektiv2Doc::OnTransform3dLocalaxismenu()
{
  CPoint pt=GetMessagePos();
  CMenu mnu;
  mnu.LoadMenu(IDR_LOCALAXISMENU);
  CMenu *sub=mnu.GetSubMenu(0);
  sub->TrackPopupMenu(TPM_LEFTALIGN,pt.x,pt.y,AfxGetMainWnd());  
}

void CObjektiv2Doc::OnTransform3dEdtlocalaxis()
{
  localAxisMode=true;
  if (gismo_mode==gismo_selected || !localAxisMode) gismo_mode=gismo_show;else gismo_mode=gismo_selected;
  this->pinlock=true;
  UpdateAllViews(NULL);
}

void CObjektiv2Doc::OnUpdateTransform3dEdtlocalaxis(CCmdUI *pCmdUI)
{
  pCmdUI->SetCheck(gismo_mode==gismo_selected && localAxisMode==true);
}

void CObjektiv2Doc::OnTransform3dShowlocalaxis()
{
  if (gismo_mode==gismo_hide || !localAxisMode) gismo_mode=gismo_show;else gismo_mode=gismo_hide;
  localAxisMode=true;
  if (gismo_mode!=gismo_hide) this->pinlock=true;
  UpdateAllViews(NULL);
}

void CObjektiv2Doc::OnUpdateTransform3dShowlocalaxis(CCmdUI *pCmdUI)
{
  pCmdUI->SetCheck((gismo_mode==gismo_show || gismo_mode==gismo_selected)  && localAxisMode==true);
}

#include "ProxyNameDlg.h"



class CICreateSelWithLocalAxis: public CICommand
{
  HMATRIX transform;
  CString name;
public:
  CICreateSelWithLocalAxis(  HMATRIX transform, CString name):
    name(name)
    {
      CopyMatice(this->transform,transform);
      recordable=false;
    }
  virtual int Execute(LODObject *lo)
  {
    ObjectData *obj=lo->Active();
    Selection sel=*obj->GetSelection();

    int idx=obj->NFaces();

    ObjToolProxy &prox=obj->GetTool<ObjToolProxy>();
    Matrix4 mx(transform[0][0],transform[1][0],transform[2][0],transform[3][0],transform[0][1],transform[1][1],transform[2][1],transform[3][1],transform[0][2],transform[1][2],transform[2][2],transform[3][2]);
    if (prox.CreateProxy(name,mx)==false) return -1;
    RString nm;
    
    FaceT fc(obj,idx);
    NamedSelection *nsel=const_cast<NamedSelection *>(prox.GetProxySelection(fc,false));
    if (nsel==0) return -1;

    sel.Validate();

    nsel->FaceSelect(idx,false);
    fc.SetFlags(FACE_USER_MASK,FACE_USER_MASK);

    (*nsel)+=sel;
    nsel->SetName("-"+name);

    obj->UseSelection(nsel);
    return 0;
  }



};

void CObjektiv2Doc::OnTransform3dSelectionwithlocalaxis()
{
  CProxyNameDlg dlg;
  dlg.vNoId=true;
  if (dlg.DoModal()==IDOK)
  {
    comint.Begin(WString(IDS_CREATEPROXY));
    comint.Run(new CICreateSelWithLocalAxis(gismo,dlg.Name));
  }
  UpdateAllViews(0);
  UpdateAllToolbars(1);
}

void CObjektiv2Doc::OnUpdateTransform3dSelectionwithlocalaxis(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(gismo_mode!=gismo_hide);
}


void CObjektiv2Doc::AttachLocalAxis()
{
  if (gismo_mode==gismo_show && localAxisMode)
  {
    for (int i=0,cnt=LodData->Active()->NFaces();i<cnt;i++) if (!LodData->Active()->FaceSelected(i))
    {
      FaceT fc(LodData->Active(),i);
      if ((fc.GetFlags() & FACE_USER_MASK)!=FACE_USER_MASK) continue;
      int j;
      for (j=0;j<fc.N();j++)
        if (fc.GetObject()->PointSelected(fc.GetPoint(j))==false) break;
      if (j==fc.N())
      {
        Matrix4 fcmx;
        fc.GetObject()->GetTool<ObjToolProxy>().GetProxyTransform(fc,fcmx,false);
        CopyVektor(gismo[0],mxVector3(fcmx(0,0),fcmx(1,0),fcmx(2,0)));
        CopyVektor(gismo[1],mxVector3(fcmx(0,1),fcmx(1,1),fcmx(2,1)));
        CopyVektor(gismo[2],mxVector3(fcmx(0,2),fcmx(1,2),fcmx(2,2)));
        CopyVektor(gismo[3],mxVector3(fcmx(0,3),fcmx(1,3),fcmx(2,3)));
        gismo[0][3]=gismo[1][3]=gismo[2][3]=0;
        break;
      }
    }
  }
}

void CObjektiv2Doc::OnTransform3dAttachtoface()
{
  VecT vc1(0,0,0);
  VecT vc2(0,0,0);
  bool valid=false;
  bool valid2=false;
  float gismoscale=(float)(sqrt(gismo[0][0]*gismo[0][0]+gismo[0][1]*gismo[0][1]+gismo[0][2]*gismo[0][2])+
    sqrt(gismo[1][0]*gismo[1][0]+gismo[1][1]*gismo[1][1]+gismo[1][2]*gismo[1][2])+
    sqrt(gismo[2][0]*gismo[2][0]+gismo[2][1]*gismo[2][1]+gismo[2][2]*gismo[2][2]))/3.0f;
  if (gismoscale<0.00001) gismoscale=1.0f;
  const Selection *cur=LodData->Active()->GetSelection();
  for (int i=0,cnt=LodData->Active()->NFaces();i<cnt;i++) if (cur->FaceSelected(i))
  {
    FaceT fc(cur->GetOwner(),i);
    VecT vc;
    vc=fc.CalculateRawNormal();
    float cangle=vc.CosAngle(vc1);
    if (valid)
    {
      if (cangle<0.7f && cangle>-0.95)
      {
        vc2+=vc;
        valid2=true;
        continue;
      }
      else if (cangle<=-0.95) continue;
    }    
    valid=true;
    vc1+=vc;
  }
  if (valid)
  {
    if (!valid2) 
    {
      vc2=VecT(0,1,0);
      float cangle=vc2.CosAngle(vc1);
      if (cangle>0.95 || cangle<-0.95) vc2=VecT(0,0,1);
    }
    VecT m1,m2,m3;
    m1=vc1.CrossProduct(vc2).Normalized();    
    m2=m1.CrossProduct(vc1).Normalized();
    m3=m2.CrossProduct(m1);
    HMATRIX mm,zz,trn;
    CopyVektor(mm[0],mxVector3(m1[0],m1[1],m1[2]));
    CopyVektor(mm[1],mxVector3(m2[0],m2[1],m2[2]));
    CopyVektor(mm[2],mxVector3(m3[0],m3[1],m3[2])); 
    CopyVektor(mm[3],mxVector3(0,0,0)); 
    mm[0][3]=mm[1][3]=mm[2][3]=0;    
    Zoom(zz,gismoscale,gismoscale,gismoscale);
    Translace(trn,pin[0],pin[1],pin[2]);
    SoucinMatic(zz,mm,gismo);
    SoucinMatic(gismo,trn,gismo);
    UpdateAllViews(NULL);
  }
}

void CObjektiv2Doc::OnTransform3dFromproxy()
{
  const Selection *cur=LodData->Active()->GetSelection();
  for (int i=0,cnt=LodData->Active()->NFaces();i<cnt;i++) if (cur->FaceSelected(i))
  {
         FaceT fc(LodData->Active(),i);
        Matrix4 fcmx;
        fc.GetObject()->GetTool<ObjToolProxy>().GetProxyTransform(fc,fcmx,false);
        CopyVektor(gismo[0],mxVector3(fcmx(0,0),fcmx(1,0),fcmx(2,0)));
        CopyVektor(gismo[1],mxVector3(fcmx(0,1),fcmx(1,1),fcmx(2,1)));
        CopyVektor(gismo[2],mxVector3(fcmx(0,2),fcmx(1,2),fcmx(2,2)));
        CopyVektor(gismo[3],mxVector3(fcmx(0,3),fcmx(1,3),fcmx(2,3)));
        gismo[0][3]=gismo[1][3]=gismo[2][3]=0;
        UpdateAllViews(NULL);
        break;
  }
  
}

class FunctorSelectUVSetByPosition
{
  mutable ObjUVSet **_foundSet;
  mutable int _position;
public:
  FunctorSelectUVSetByPosition(ObjUVSet **foundSet, int pos):_position(pos),_foundSet(foundSet) {}
  bool operator()(ObjUVSet *uvset) const
  {
    if (_position--) return false;
    *_foundSet=uvset;
    return true;
  }
};

void CObjektiv2Doc::OnUvSetActivate(UINT cmd)
{
  ObjUVSet *ff=0;
  LodData->Active()->SortUVSets();
  LodData->Active()->EnumUVSets(FunctorSelectUVSetByPosition(&ff,cmd-ID_UVSETS_ACTIVATE0));
  if (ff) 
  {
    comint.Begin(WString(IDS_ACTIVATEUVSET,ff->GetIndex()));
    comint.Run(new CISetActiveUVSet(ff->GetIndex()));
    UpdateViewer(0);
    if (frame->_dlguveditor.GetSafeHwnd())
      frame->_dlguveditor.UpdateFromDocument(this);
  }
  else
    MessageBeep(MB_ICONEXCLAMATION);
}

void CObjektiv2Doc::OnUvsetsAdd()
{
  comint.Begin(WString(IDS_ADDNEWUVSET));
  comint.Run(new CIAddUVSet());
  UpdateViewer(0);
  if (frame->_dlguveditor.GetSafeHwnd())
    frame->_dlguveditor.UpdateFromDocument(this);
}

void CObjektiv2Doc::OnUvsetsDeleteactive()
{
  int activeUv=LodData->Active()->GetActiveUVSet()->GetIndex();
  if (activeUv!=0)
  {
    comint.Begin(WString(IDS_DELETEUVSET,activeUv));
    comint.Run(new CIDeleteUVSet(activeUv));
    UpdateViewer(0);
    if (frame->_dlguveditor.GetSafeHwnd())
      frame->_dlguveditor.UpdateFromDocument(this);
  }
}

void CObjektiv2Doc::OnUpdateUvsetsDeleteactive(CCmdUI *pCmdUI)
{
  int activeUv=LodData->Active()->GetActiveUVSet()->GetIndex();
  pCmdUI->Enable(activeUv!=0);
}

void CObjektiv2Doc::OnUvsetsChangeid()
{
  int activeUv=LodData->Active()->GetActiveUVSet()->GetIndex();
  CDlgInputFile dlg;
  dlg.browse=false;
  dlg.title.LoadString(IDS_UVSETCHANGEID);
  dlg.vFilename.Format("%d",activeUv);
  while (dlg.DoModal()==IDOK)
  {
    int newid;
    if (sscanf(dlg.vFilename,"%d",&newid)==1 && newid>=0)
    {
      comint.Begin(WString(IDS_UVSETCHANGEIDACTION,activeUv,newid));
      comint.Run(new CIChangeUVSetID(newid));
      UpdateViewer(0);
      if (frame->_dlguveditor.GetSafeHwnd())
        frame->_dlguveditor.UpdateFromDocument(this);
      break;
    }
  }
}

#include "ITexMerge.h"

void CObjektiv2Doc::OnSurfacesMergetextures()
{
  {
    CFileDialogEx fdlg(TRUE,"ptm",NULL,OFN_FILEMUSTEXIST|OFN_LONGNAMES,
      WString(IDS_MERGEFILTERS),NULL);
    config->FillOpenDialogByPath(IDS_CFGSLUCFOLDER,fdlg.m_ofn,GetPathName());
    if (fdlg.DoModal()!=IDOK) return;
    config->SaveCurrPath(IDS_CFGSLUCFOLDER,fdlg.GetPathName());
    comint.Begin(WString(IDS_MERGETEXTURESALL));
    AutoArray<CITexMerge::MatchInfo> matchInfo;
    CITexMerge *mm = new CITexMerge(fdlg.GetPathName(),true,&matchInfo);
    int res = comint.Run(mm) ;
    if (res == -1)
    {
        MessageBox(*frame,"Error reading ptm file",0,MB_OK|MB_ICONEXCLAMATION);    
    } else {


        std::ostringstream buff;
        for (int i = 0; i < matchInfo.Size();i++) {
            CITexMerge::MatchInfo &nfo = matchInfo[i];
            buff << nfo._texture << ":\t ";
            if (nfo._face && nfo._rvmat) buff << "rvmat + texture";
            else if (nfo._face) buff << "texture";
            else if (nfo._rvmat) buff << "rvmat";
            else buff << "NOT MATCHED!";
            buff << std::endl;
                 
        }
        MessageBox(*frame,buff.str().c_str(),"Match Result",MB_OK|MB_ICONINFORMATION);
    }


    UpdateAllToolbars(0);
    UpdateAllViews(NULL);
  }
}


void CObjektiv2Doc::OnSelectionOperation(UINT cmd)
{
  if (combine_saved.IsNull())
    combine_saved=new Selection(LodData->Active());
  else
    combine_saved->SetOwner(LodData->Active());
  const Selection &active=*LodData->Active()->GetSelection();
  switch (cmd)
  {
  case ID_SELOP_COPY: *combine_saved=active;break;
  case ID_SELOP_UNION: (*combine_saved)+=active;break;
  case ID_SELOP_INTERSECTION: (*combine_saved)&=active;break;
  case ID_SELOP_COMPLEMENT: (*combine_saved)-=active;break;
  case ID_SELOP_COMPINTER: 
    {
      Selection sel=*combine_saved;
      sel&=active;
      (*combine_saved)+=active;
      (*combine_saved)-=sel;
    }
    break;
  }

  if (*combine_saved!=active)
  {
    comint.Begin(WString(IDS_COMBINESELECTIONS));
    comint.Run(new CISelect(new Selection(*combine_saved)));
    UpdateAllViews(NULL);
  }
}

void CObjektiv2Doc::OnRecordTransform()
{
  selrecordenable=!selrecordenable;
  if (selrecordenable) JednotkovaMatice(selrecord);
}

void CObjektiv2Doc::OnUpdateRecordTransform(CCmdUI *pCmd)
{
  pCmd->SetCheck(selrecordenable);
}

void CObjektiv2Doc::OnUseRecordedTransform()
{
  if (selrecordenable) OnRecordTransform();
  CopyMatice(seltr,selrecord);
  seltransform=true;
  UpdateAllViews(0,1,0);
  ApplyPreviewTransform(false);
  seltransform=false;
  UpdateAllViews(0,0,0);
}
void CObjektiv2Doc::OnSaveRecordedTransform()
{
  CFileDialogEx fdlg(FALSE,"trn",0,OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT|OFN_PATHMUSTEXIST,
    CString(MAKEINTRESOURCE(IDS_RECORDTRANSFILTER)));
  if (fdlg.DoModal()==IDOK)
  {
    std::ofstream out(fdlg.GetPathName(),ios::out|ios::trunc|ios::binary);
    out.write((char *)selrecord,sizeof(selrecord));
  }
}

void CObjektiv2Doc::OnLoadRecordedTransform()
{
  CFileDialogEx fdlg(TRUE,"trn",0,OFN_HIDEREADONLY|OFN_FILEMUSTEXIST,
    CString(MAKEINTRESOURCE(IDS_RECORDTRANSFILTER)));
  if (fdlg.DoModal()==IDOK)
  {
    std::ifstream in(fdlg.GetPathName(),ios::in|ios::binary);
    in.read((char *)selrecord,sizeof(selrecord));
  }
}
