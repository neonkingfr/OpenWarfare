#if !defined(AFX_DLGNORMALDIRECTION_H__D8110D33_176E_4939_A2E1_6AE1921675B0__INCLUDED_)
#define AFX_DLGNORMALDIRECTION_H__D8110D33_176E_4939_A2E1_6AE1921675B0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgNormalDirection.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgNormalDirection dialog

class CDlgNormalDirection : public CDialog
  {
  // Construction
  static float st_vX, st_vY, st_vZ;   //!< Saved values for next time use
  static BOOL st_vRelativeTo;         //!< Saved value for neext time use
  
  void LoadNormalsToCB();             //!< Creates list of normals in ComboBox
  public:
    void LockNormals();                 //!< Locks normals in current selection using dialog values
    void UnlockNormals();               //!< Unlocks normals in current selection
    CDlgNormalDirection(CWnd* pParent = NULL);   // standard constructor
    
    void DialogRules();         //!< Handles general rules for enabing/disabling windows
    
    
    // Dialog Data
    //{{AFX_DATA(CDlgNormalDirection)
    enum 
      { IDD = IDD_NORMALDIRECTION }; 
    CButton	wRelativeTo;          
    CComboBox	wNormalList;
    BOOL	vRelativeTo;
    float	vX;
    float	vY;
    float	vZ;
    //}}AFX_DATA
    ObjectData *vObject;           //!< Referenced object
    float   vFinalX;               //!< Final normal orientation (output value X)
    float   vFinalY;               //!< Final normal orientation (output value Y)
    float   vFinalZ;               //!< Final normal orientation (output value Z)
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CDlgNormalDirection)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CDlgNormalDirection)
    virtual BOOL OnInitDialog();
    afx_msg void OnRelativeto();
    afx_msg void OnPreview();
    virtual void OnOK();
    afx_msg void OnUnlocknormals();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
    private:
    void CalculateFinalDirection();
  };

//--------------------------------------------------

#include "ComInt.h"

class CINormalDirection: public CICommand
  {
  protected:
    CDlgNormalDirection  *_dlg;    
  public:
    CINormalDirection(CDlgNormalDirection *dlg): _dlg(dlg) 
      {}; //!<Pokud je dlg==NULL, pak dela Unlock Normals
    ~CINormalDirection() 
      {delete _dlg;}
    CINormalDirection(istream &in)
      {
      bool p;
      datard(in,p);
      if (p)
        {
        _dlg=new CDlgNormalDirection;
        datard(in,_dlg->vFinalX);
        datard(in,_dlg->vFinalY);
        datard(in,_dlg->vFinalZ);
        }
      else
        _dlg=NULL;
      }
    virtual int Execute(LODObject *lod)
      {
      ObjectData *obj=lod->Active();
      if (_dlg==NULL)
        {
        CDlgNormalDirection dlg;
        dlg.vObject=obj;
        dlg.UnlockNormals();
        }
      else
        {
        _dlg->vObject=obj;
        _dlg->LockNormals();
        }
      return 0;
      }
    virtual void Save(ostream &str) 
      {
      bool p=_dlg!=NULL;
      datawr(str,p);
      if (p)
        {
        datawr(str,_dlg->vFinalX);
        datawr(str,_dlg->vFinalY);
        datawr(str,_dlg->vFinalZ);
        }
      }
    virtual int Tag() const 
      {return CITAG_NORMALDIRECTION;}
  };

//--------------------------------------------------

CICommand *CINormalDirection_Create(istream &str);

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGNORMALDIRECTION_H__D8110D33_176E_4939_A2E1_6AE1921675B0__INCLUDED_)
