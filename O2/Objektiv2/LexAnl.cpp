// LexAnl.cpp: implementation of the CLexAnl class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "LexAnl.h"
#include <string.h>
#include <ctype.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

struct LexTable
  {
  char srcname[10];
  LexSymbol symb;
  };

//--------------------------------------------------

LexTable tbl[]=
  {
    {"(",lexLP},
    {")",lexRP},
    {"+",lexPlus},
    {"-",lexMinus},
    {"*",lexMult},
    {"/",lexDiv},
    {"=",lexAssign},
    {"==",lexEqual},
    {">",lexGreat},
    {"<",lexLower},
    {">=",lexGEq},
    {"<=",lexLEq},
    {"!=",lexNoEq},
    {",",lexComma},
    {";",lexCommdiv},
    {"vx",lexVertX},
    {"vy",lexVertY},
    {"vz",lexVertZ},
    {"tu",lexFaceTu},
    {"tv",lexFaceTv},
    {"sin",lexFSin},
    {"cos",lexFCos},
    {"tan",lexFTan},
    {"exp",lexFExp},
    {"ln",lexFLn},
    {"sqr",lexFSqr},
    {"sqrt",lexFSqrt},
    {"and",lexAnd},
    {"&&",lexAnd},
    {"or",lexOr},
    {"||",lexOr},
    {"xor",lexXor},
    {"iff",lexIff},
    {"repeat",lexRepeat},
    {"while",lexWhile},
    {"for",lexFor},
    {"weight",lexWeight},
    {"atan",lexFAtan},
    {"<)",lexRad},
    {"rad",lexRad}
  };

//--------------------------------------------------

static LexSymbol FindSymbol(const char *text)
  {
  for (int i=0;i<sizeof(tbl)/sizeof(LexTable);i++)
    {
    if (strcmp(tbl[i].srcname,text)==0) return tbl[i].symb;
    if (strncmp(tbl[i].srcname,text,strlen(text))==0) return lexPartOf;
    }
  return lexUnknown;
  }

//--------------------------------------------------

static LexSymbol ScanToken(istream& str, char& varible)
  {
  char text[32];
  memset(text,0,sizeof(text));
  int p=0;
  int i=str.get();
  while (i!=EOF)
    {
    text[p]=i;
    LexSymbol s=FindSymbol(text);
    if (s==lexUnknown)
      {
      if (text[1]==0 && (text[0]>='a' && text[0]<='z' || text[0]>='A' && text[0]<='Z' )) goto var;
      text[p]=0;
      str.putback(i);
      if (text[1]==0 && (text[0]>='a' && text[0]<='z' || text[0]>='A' && text[0]<='Z' )) goto var;
      s=FindSymbol(text);
      return s;
      }
    p++;	
    if (p>=sizeof(text)) return lexUnknown;
    i=str.get();
    }
  return FindSymbol(text);
  var:
  varible=toupper(text[0]);
  if (text[1]==0 && varible>='A' && varible<='Z')		  
    return lexVarible;
  return lexUnknown;
  }

//--------------------------------------------------

LexSymbol CLexAnl::ReadSymb()
  {
  int c;
  
  c=istr.peek();
  while (c<33 && c>=0) 
    {
    istr.ignore();
    c=istr.peek();
    }
  if (c==EOF) return lexEof;
  if (c=='.' || (c>='0' && c<='9'))
    {
    istr>>rnumb;
    return lexNumber;
    }
  else
    {
    return ScanToken(istr,varible);	
    }
  }

//--------------------------------------------------

