#include "stdafx.h"
#include "Objektiv2.h"
#include "Objektiv2Doc.h"
#include "ComInt.h"
#include "ISimpleCommand.h"
#include "../ObjektivLib/ObjToolCheck.h"
#include "../ObjektivLib/ObjToolClipboard.h"
#include "../ObjektivLib/ObjToolTopology.h"
#include "../ObjektivLib/ObjToolSections.h"
#include "../ObjektivLib/ObjToolProxy.h"
#include "../ObjektivLib/ObjToolConvex.h"
#include "../ObjektivLib/ObjToolMapping.h"

int CISimpleCommand::Execute(LODObject *obj)
{
    ObjectData *odata=obj->Active();
    switch (comm)
    {
    case CIS_RECALCNORM: RecalcNormals2(odata);break;
    case CIS_OPTIMIZE: odata->GetTool<ObjToolCheck>().Optimize();break;
    case CIS_CHECKFACES:odata->GetTool<ObjToolCheck>().CheckFaces();
        if (!odata->GetSelection()->IsEmpty())
        {
            WString s(IDS_NONPLANARAFACES);
            odata->DeleteNamedSel(s);
            odata->SaveNamedSel(s);
        }
        odata->GetTool<ObjToolCheck>().CheckMapping();
        if (!odata->GetSelection()->IsEmpty())
        {
            WString s(IDS_NONLINEARMAPPING);
            odata->DeleteNamedSel(s);
            odata->SaveNamedSel(s);
        }
        odata->GetTool<ObjToolCheck>().CheckSTVectors();
        if (!odata->GetSelection()->IsEmpty())
        {
            WString s(IDS_FACESSTERROR);
            odata->DeleteNamedSel(s);
            odata->SaveNamedSel(s);
        }
        break;
    case CIS_ISOLATEDPOINTS:odata->GetTool<ObjToolCheck>().IsolatedPoints();break;
    case CIS_TRIANGULATE:odata->Triangulate(odata->tmNormal);break;
    case CIS_TRIANGULATE2:odata->Triangulate(odata->tmReversed);break;
    case CIS_TRIANGULATE_CONCAVE:odata->Triangulate(odata->tmConcave);break;
    case CIS_TRIANGULATE_CONVEX:odata->Triangulate(odata->tmConvex);break;
    case CIS_SQUARIZE:odata->Squarize();break;
    case CIS_FINDCOMPONENTS:
        {
            Selection sel(odata);
            odata->GetTool<ObjToolProxy>().GetAllProxies(sel);
            odata->GetTool<ObjToolTopology>().CreateComponents(&sel);
//            comint.SetLongOp();break;
        }
        break;
    case CIS_COMPONENTHULL: odata->GetTool<ObjToolTopology>().ComponentConvexHull();/*comint.SetLongOp();*/break;
    case CIS_CONVEXHULL:odata->GetTool<ObjToolTopology>().CreateConvexHull();/*comint.SetLongOp()*/;break;
    case CIS_FINDNONCONVEX: 
        {
            ObjectData save;
            save=*odata;
            odata->GetTool<ObjToolTopology>().CheckConvexComponents();
            Selection *cur=const_cast<Selection *>(save.GetSelection());
            *cur=*odata->GetSelection();
            *odata=save;
            break;
        }
    case CIS_SELECTHALFSPACE: 
        {
            int faceIndex=odata->SingleSelectedFace();
            if( faceIndex<0 ) return FALSE;
            const FaceT face(odata,faceIndex);
            odata->SelectHalfSpace(face);
        }
        break;
    case CIS_FINDNONCLOSED: odata->GetTool<ObjToolTopology>().CheckClosedTopology();break;
    case CIS_CLOSE: odata->GetTool<ObjToolTopology>().CloseTopology();break;		  
    case CIS_SPLIT: odata->SelectionSplit();break;
    case CIS_DELETE: odata->SelectionDelete();break;
    case CIS_CUT:
        odata->SelectionDelete();          
        break;
    case CIS_REVERSEFACE: 
        {
            for( int i=0; i<odata->NFaces(); i++ ) if( odata->FaceSelected(i) )
            {
                FaceT face(odata,i);
                face.Reverse();
            }
            //odata->RecalcNormals();            		  
            RecalcNormals2(odata);		  
            break;
        }
    case CIS_UNCROSS:
        {
            for( int i=0; i<odata->NFaces(); i++ ) if( odata->FaceSelected(i) )
            {
                FaceT face(odata,i);
                face.Cross();
            }
            //odata->RecalcNormals();            		  
            RecalcNormals2(odata);
            break;
        }
    case CIS_REMOVE:
        odata->SelectionDeleteFaces();
        //odata->RecalcNormals();            		  
        RecalcNormals2(odata);
        break;
    case CIS_MERGESEL:
        {
            HVECTOR vl;
            GetObjectCenter(true,vl,odata);
            odata->MergePoints(1e10,true);
            for (int i=0;i<odata->NPoints();i++) if (odata->PointSelected(i))
            {
                PosT& ps=odata->Point(i);
                ps[0]=vl[0];
                ps[1]=vl[1];
                ps[2]=vl[2];
            }
            break;
        }
    case CIS_SHARP:
        {
            int cnt=odata->NFaces();
            ProgressBar<int> pb(cnt);
            for( int i=0; i<cnt; i++ )
            {
                pb.AdvanceNext(1);
                const FaceT face(odata,i);
                int prev=face.N()-1;
                for( int v=0; v<face.N(); v++ )
                {
                    int v0=face.GetPoint(prev);
                    int v1=face.GetPoint(v);
                    if( odata->PointSelected(v0) && odata->PointSelected(v1) )
                    {
                        odata->AddSharpEdge(v0,v1);
                    }
                    prev=v;
                }
            }
            //odata->RecalcNormals();
            RecalcNormals2(odata);
        }
        break;
    case CIS_SMOOTH:
        {
            bool all=true;
            for( int i=0; i<odata->NPoints(); i++ ) if( !odata->PointSelected(i) ) all=false;
            if( all )
            {
                // special trick to remove all sharp edges
                odata->RemoveAllSharpEdges();
            }
            else
            {
                int last=0;
                for (int i=0,a,b;odata->EnumEdges(i,a,b);)
                {
                    if (odata->PointSelected(a) && odata->PointSelected(b))
                    {
                        i=last;
                        odata->RemoveSharpEdge(i);
                    }
                    last=i;
                }
            }
            //odata->RecalcNormals();
            RecalcNormals2(odata);
        }
        break;
    case CIS_CREATEFACE:
        {
            int count=odata->CountSelectedPoints();
            if( count<3 || count>4 ) return FALSE;
            int index=odata->NFaces();
            FaceT face(odata,odata->NewFace());
            int i;
            if( odata->NNormals()<=0 ) odata->NewNormal();
            int pcnt=odata->NPoints();
            int fcnt=odata->NFaces();
            for( i=0; i<pcnt; i++ ) if( odata->PointSelected(i) )
            {
                int n=face.N();
                face.SetPoint(n,i);
                face.SetNormal(n,0);
                face.SetN(n+1);
            }
            for( i=0; i<fcnt; i++ ) odata->FaceSelect(i,false);
            // try to uncross until it is not crossed
            face.AutoUncross();
            face.AutoReverse();		  
            odata->FaceSelect(index);
            //odata->RecalcNormals();
            RecalcNormals2(odata);
            odata->SetDirty();
        }
        break;
    case CIS_PINCULL:
        odata->BackfaceCull(Vector3(doc->pin[0],doc->pin[1],doc->pin[2]));
//        comint.SetLongOp();
        break;		  
    case CIS_CENTERALL:
        obj->CenterAll();
        break;
    case CIS_FACESFROMPOINTS:
        odata->SelectFacesFromPoints();
        break;
    case CIS_POINTSFROMFACES:
        odata->SelectPointsFromFaces();		  
        break;
    case CIS_REPAIRFACES:
        odata->GetTool<ObjToolCheck>().RepairDegeneratedFaces();
        break;
    case CIS_CHECKDEGENERATED:
        odata->GetTool<ObjToolCheck>().CheckDegeneratedFaces();
        break;
    case CIS_CREATEPOLYGON:
        {
            bool succ=odata->GetTool<ObjToolTopology>().TessellateFromPointSelection(*odata->GetSelection(),(odata->GetSelection()));
            if (succ==false)
            {
                if (obj==doc->LodData) AfxMessageBox(IDS_TESSELATIONFAILED,MB_OK|MB_ICONINFORMATION);
            }
            break;
        }
    case CIS_SORTFACES:
        odata->GetTool<ObjToolSections>().SortTextures();
        break;      
    case CIS_HIDEPROXIES:
        odata->GetTool<ObjToolProxy>().ShowProxyContent(false);
        break;
    case CIS_SHOWPROXIES:
        odata->GetTool<ObjToolProxy>().ShowProxyContent(true);
        break;
    case CIS_SORTALPHAFACES:
        {
            ProgressBar<int> pb(1);
            pb.AdvanceNext(1);
            pb.ReportState(ENABLE_STOP);
            const Selection *sel=odata->GetSelection();
            odata->GetTool<ObjToolTopology>().SortAlphaFaces(ObjIsFaceAlphaDefault(),sel->NFaces()==0?0:sel);

        }
        break;
    case CIS_NEIGHBOURMAPPING:
        {
            ObjToolMapping::NMState state = odata->GetTool<ObjToolMapping>().NeighbourMapping();
            if (state == ObjToolMapping::nmsNoMapped)
            {
                AfxMessageBox(IDS_NBMAPPINGERR2);
                return -1;
            }
            if (state == ObjToolMapping::nmsNoUnmaped)
            {
                AfxMessageBox(IDS_NBMAPPINGERR2);
                return -2;
            }      
        }
        break;
    case CIS_BREAKWELDS: 
        {
            int n = odata->NPoints();
            odata->GetTool<ObjToolTopology>().BreakApart(odata->GetSelection());
            while (n < odata->NPoints()) odata->PointSelect(n++);
        }
        break;
    case CIS_BREAKTOCONVEX:
        {            
            ProgressBar<int> pb(1);
            pb.AdvanceNext(1);
            pb.ReportState(ENABLE_STOP);

            odata->GetTool<ObjToolConvex>().SplitToConvexByComponents(
                *odata->GetSelection(),ObjToolConvex::SplitToConvexProp());
        }
        break;
    }
    

    return 0;
}


CICommand *CISimpleCommand_Create(istream &str)
{
    return new CISimpleCommand(str);
}
