#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// DlgManageScripts dialog

struct DlgManageScriptsItemInfo
{
  Pathname scriptPath;
  CString itemName;
  CString cmdline;
  CString promptInput;
  CString promptOutput;
  int idassign;
  union
  {
    struct
    {
      bool asExternal:1;
      bool saveFile:1;
      bool saveMaterials:1;
      bool newFolder:1;       //item is new folder
      bool lastItem:1;     //item is last item in folder (including folder itself)
      bool logOutput:1;
      bool inputFile:1;
    };
    unsigned long flags;
  };
  DlgManageScriptsItemInfo() {flags=0;idassign=-1;}
};

TypeIsMovable(DlgManageScriptsItemInfo);

class DlgManageScripts : public CDialog
{
	DECLARE_DYNAMIC(DlgManageScripts)
    AutoArray<DlgManageScriptsItemInfo> _items;
    bool inedit;
    HTREEITEM _dragItem;
    CImageList *_dragImage;


public:
	DlgManageScripts(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgManageScripts();

// Dialog Data
	enum { IDD = IDD_MANAGESCRIPTS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

  bool IsFolder(HTREEITEM itm);
  void MoveItemToItem(HTREEITEM what, HTREEITEM where);
  void MoveItemToItemNoTest(HTREEITEM what, HTREEITEM where,bool last=false);
  int LoadToTree(HTREEITEM itm=NULL, int idx=0);
  void DeleteChilds(HTREEITEM itm);
  void DeleteTreeItem(HTREEITEM itm);
  void SaveFromTree(HTREEITEM subtree=NULL);

  DECLARE_MESSAGE_MAP()
public:
  void SaveRegistry();
  void LoadRegistry();
  int InitMenu(CMenu *menu, int idoffset, int index=0, int delafter=-1);
  const DlgManageScriptsItemInfo *FromMenu(int id, int idoffset);
  

  CTreeCtrl wTree;
  CButton wRunInO2;

  CString promptInput;
  CString promptOutput;

  void (*RunScriptSettings)(const char *scriptName, void *context,HWND hWnd);
  void *RunScriptContext;

  virtual BOOL OnInitDialog();
  afx_msg void OnDestroy();
protected:
  virtual void OnOK();
public:
  afx_msg void OnTvnEndlabeleditMenutree(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnBnClickedNewfolder();
  afx_msg void OnBnClickedDelete();
  afx_msg void OnTvnBeginlabeleditMenutree(NMHDR *pNMHDR, LRESULT *pResult);
  virtual BOOL PreTranslateMessage(MSG* pMsg);
  afx_msg void OnBnClickedRename();
  CButton wRunAsExternal;
  CButton wSaveCurFile;
  CButton wSaveAllMaterials;
  afx_msg void OnBnClickedBbrowse();
  afx_msg void OnBnClickedInsertafter();
  void SaveToStruct(DlgManageScriptsItemInfo &result);
  afx_msg void OnNMDblclkMenutree(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnBnClickedReplace();
  afx_msg void OnTvnBegindragMenutree(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnMouseMove(UINT nFlags, CPoint point);
  afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
  afx_msg void OnCancelMode();
  afx_msg void OnCaptureChanged(CWnd *pWnd);
  afx_msg void OnBnClickedAddfolder();
  afx_msg void OnBnClickedEdit();
  afx_msg void OnTvnSelchangedMenutree(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnEnUpdateScriptfilename();
  afx_msg void OnBnClickedConfigure();
  afx_msg void OnBnClickedEditscript();
  afx_msg void OnBnClickedRunino2();
  afx_msg void OnBnClickedRunasexternal();
  CButton wLogOutput;
  CButton wInputFile;
  afx_msg void OnBnClickedLogoutput();
  afx_msg void OnBnClickedInputfile();
  afx_msg void OnBnClickedAboutscript();
  CButton wAboutScript;
};
