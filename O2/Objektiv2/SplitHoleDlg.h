#if !defined(AFX_SPLITHOLEDLG_H__B50639B9_81BB_48FB_96CA_36A265E53C35__INCLUDED_)
#define AFX_SPLITHOLEDLG_H__B50639B9_81BB_48FB_96CA_36A265E53C35__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SplitHoleDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSplitHoleDlg dialog

#include "ComInt.h"

class CSplitHoleDlg : public CDialog
  {
  // Construction
  public:
    CSplitHoleDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CSplitHoleDlg)
    enum 
      { IDD = IDD_SPLITHOLE };
    UINT	points;
    int		direction;
    int		mode;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CSplitHoleDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CSplitHoleDlg)
    afx_msg void OnDeltaposSpin(NMHDR* pNMHDR, LRESULT* pResult);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

class CISplitHole:public CICommand
  {
  int points;
  int direction;
  int mode;
  void CalcTransform(FaceT& fc, ObjectData *obj, HMATRIX mm);
  void GetHoleProp(FaceT &fc, ObjectData *obj, float &radius, LPHVECTOR center);
  void SplitFace(FaceT fc, ObjectData *obj);
  public:
    void CreateEdgeFacesSpec(FaceT &fc, ObjectData *obj, int pt1, int pts,LPHVECTOR center);
    void CreateHole(FaceT fc, ObjectData *obj);
    int FindNearestPoint(FaceT &fc, ObjectData *obj, int pt, LPHVECTOR center);
    void CreateEdgeFace(FaceT &fc, ObjectData *obj, int pt1, int pt2, LPHVECTOR center);
    void CreateCircle(HMATRIX mm, ObjectData *obj);
    virtual void Save(ostream& str);
    virtual int Execute(LODObject *lod);
    virtual int Tag() const ;
    CISplitHole(int mode,int pt, int dir);
    CISplitHole(istream &str);
    
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SPLITHOLEDLG_H__B50639B9_81BB_48FB_96CA_36A265E53C35__INCLUDED_)
