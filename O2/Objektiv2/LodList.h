#if !defined(AFX_LODLIST_H__391FD74B_197F_468C_A23C_49603E4F515E__INCLUDED_)
#define AFX_LODLIST_H__391FD74B_197F_468C_A23C_49603E4F515E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LodList.h : header file
//

#include "comint.h"

/////////////////////////////////////////////////////////////////////////////
// CLodList dialog

extern CComInt comint;


class CLodList : public CDialogBar
  {
  // Construction
  CWnd *notify;
  CListCtrl list;
  LODObject *lod;
  bool inmenu;
  public:
    void UpdateReport();
    void SelectLOD(float level);
    float GetLODIndex();
    void UpdateListOfLods(LODObject *lodobject);
    CDIALOGBAR_RESIZEABLE
      void Create(CWnd *parent,int menuid);
    void OnLodInsert(float srclod);
    CLodList();   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CLodList)
    enum 
      { IDD = IDD_LODLIST };
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CLodList)
  protected:
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CLodList)
    afx_msg LRESULT OnExitMenuLoop(WPARAM wParam, LPARAM lParam);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnDestroy();
    afx_msg void OnLodDisplaydetails();
    afx_msg void OnRclickList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnLodDisplayshort();
    afx_msg void OnLodCopy();
    afx_msg void OnLodInsert();
    afx_msg void OnLodDelete();
    afx_msg void OnLodProperies();
    afx_msg void OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnTimer(UINT nIDEvent);
    afx_msg void OnClickList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnLodSetasback();
    afx_msg void OnBeginlabeleditList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnPluginPopupCommand(UINT cmd);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

class CILodDelete: public CICommand
  {
  protected:
    float lod;
  public:
    CILodDelete(istream &str) 
      {datard(str,lod);}
    CILodDelete(float ld): CICommand() 
      {lod=ld;}
    virtual int Execute(LODObject *obj)
      {
      int i=obj->FindLevelExact(lod);
      if (i==-1) return i;
      return !obj->DeleteLevel(i);	
      }
    virtual void Save(ostream &str) 
      {datawr(str,lod);}	  
    virtual int Tag() const 
      {return CITAG_LODDELETE;}
  };

//--------------------------------------------------

class CILodSetActive: public CILodDelete
  {
  public:
    CILodSetActive(istream& str): CILodDelete(str) 
      {}
    CILodSetActive(float ld): CILodDelete(ld) 
      {;marksave=false;}
    virtual int Execute(LODObject *obj)
      {
      int idx=obj->FindLevel(lod);
      if (idx<0) return idx;
      obj->SelectLevel(idx);
      ObjectData *data=obj->Active();
      if (data->CurrentAnimation()==-1 && data->NAnimations()>0) 
        data->UseAnimation(0);	
      comint.sectchanged=true;
      return 0;
      }
    virtual int Tag() const 
      {return CITAG_LODSETACTIVE;}
    virtual bool CanRemove(const CICommand *prev) 
      {
      return prev->Tag()==CITAG_LODSETACTIVE;
      }
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LODLIST_H__391FD74B_197F_468C_A23C_49603E4F515E__INCLUDED_)
