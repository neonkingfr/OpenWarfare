// 3dsExport.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "3dsExport.h"
#include "../ObjektivLib/Edges.h"
#include "Objektiv2Doc.h"
#include "../ObjektivLib/ObjToolsMatLib.h"
#include "../ObjektivLib/ObjToolTopology.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// C3dsExport dialog


C3dsExport::C3dsExport(CWnd* pParent /*=NULL*/)
  : CDialog(C3dsExport::IDD, pParent)
    {
    //{{AFX_DATA_INIT(C3dsExport)
    creat_material = TRUE;
    export_tex = TRUE;
    path = _T("");
    create_smooth = TRUE;
    opt_vertices = TRUE;
    dirselect = 0;
    scale = 1000;
    create_folder = FALSE;
    nonalphadetect = TRUE;
	byTopology = FALSE;
	//}}AFX_DATA_INIT
    }

//--------------------------------------------------

void C3dsExport::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(C3dsExport)
  DDX_Control(pDX, IDC_BROWSE, BBrowse);
  DDX_Check(pDX, IDC_CREATEMATERIALS, creat_material);
  DDX_Check(pDX, IDC_EXPORTTEX, export_tex);
  DDX_Text(pDX, IDC_PATH, path);
  DDX_Check(pDX, IDC_SMOOTGROUPS, create_smooth);
  DDX_Check(pDX, IDC_VERTOPT, opt_vertices);
  DDX_Radio(pDX, IDC_SAMEDIR, dirselect);
  DDX_Text(pDX, IDC_SCALE, scale);
  DDV_MinMaxLong(pDX, scale, 1, 2147483647);
  DDX_Check(pDX, IDC_EXPTOFOLDER, create_folder);
  DDX_Check(pDX, IDC_NONALPHADETECTS, nonalphadetect);
	DDX_Check(pDX, IDC_BYTOPOLOGY, byTopology);
	//}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(C3dsExport, CDialog)
  //{{AFX_MSG_MAP(C3dsExport)
  ON_BN_CLICKED(IDC_CREATEMATERIALS, OnDialogChanged)
    ON_BN_CLICKED(IDC_BROWSE, OnBrowse)
      ON_BN_CLICKED(IDC_EXPORTTEX, OnDialogChanged)
        ON_BN_CLICKED(IDC_SAMEDIR, OnDialogChanged)
          ON_BN_CLICKED(IDC_SELECTPATH, OnDialogChanged)
            ON_BN_CLICKED(IDC_VERTOPT, OnDialogChanged)
              //}}AFX_MSG_MAP
              END_MESSAGE_MAP()
                
                /////////////////////////////////////////////////////////////////////////////
                // C3dsExport message handlers
                
                
                BOOL C3dsExport::OnInitDialog() 
                  {
                  CFileDialogEx fdlg(FALSE,"3ds",NULL,OFN_PATHMUSTEXIST|OFN_LONGNAMES|OFN_OVERWRITEPROMPT|OFN_NOCHANGEDIR,
                  "3DS|*.3ds||",NULL);
                  CString pp=theApp.GetProfileString("Export3ds","LastPath");
                  fdlg.m_ofn.lpstrInitialDir=pp;
                  if (fdlg.DoModal()==IDCANCEL) 
                    {EndDialog(IDCANCEL);return FALSE;}
                  sname=fdlg.GetPathName();
                  pp=sname.GetDirectoryWithDrive();                  
                  if (path="") path=pp;
                  theApp.WriteProfileString("Export3ds","LastPath",pp);
                  CDialog::OnInitDialog();
                  BBrowse.SetIcon(::LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_BROWSE)));
                  CheckDialogRules();
                  
                  return TRUE;  // return TRUE unless you set the focus to a control
                  // EXCEPTION: OCX Property Pages should return FALSE
                  }

//--------------------------------------------------

void C3dsExport::CheckDialogRules()
  { 
  BOOL p=IsDlgButtonChecked(IDC_VERTOPT)==1;
  GetDlgItem(IDC_SMOOTGROUPS)->EnableWindow(p);
  BOOL q=IsDlgButtonChecked(IDC_CREATEMATERIALS)==1;
  GetDlgItem(IDC_EXPORTTEX)->EnableWindow(q);
  BOOL r=IsDlgButtonChecked(IDC_EXPORTTEX) && q;
  GetDlgItem(IDC_SAMEDIR)->EnableWindow(r);
  GetDlgItem(IDC_SELECTPATH)->EnableWindow(r);
  BOOL s=IsDlgButtonChecked(IDC_SELECTPATH) && r;
  GetDlgItem(IDC_PATH)->EnableWindow(s);
  BBrowse.EnableWindow(s);  
  }

//--------------------------------------------------

void C3dsExport::OnDialogChanged() 
  {
  CheckDialogRules()	;
  }

//--------------------------------------------------

int WINAPI PosBrowseCallbackProc(HWND hwnd,UINT uMsg,LPARAM lParam,LPARAM lpData);
void Browse_SetCurrentPath(const char *path);


void C3dsExport::OnBrowse() 
  {
  Browse_SetCurrentPath(path);
  
  BROWSEINFO brw;
  char path[MAX_PATH];
  CString s;
  ITEMIDLIST *il;
  
  UpdateData();
  memset(&brw,0,sizeof(brw));
  brw.hwndOwner=m_hWnd;
  brw.pidlRoot=NULL;
  brw.pszDisplayName=path;  
  s.LoadString(IDS_SELECTTEXPATH);
  brw.lpszTitle=(LPCTSTR)s;
  brw.ulFlags= BIF_RETURNONLYFSDIRS |BIF_STATUSTEXT; 	
  brw.lpfn = (BFFCALLBACK)(PosBrowseCallbackProc);
  il=SHBrowseForFolder( &brw );
  if (il==NULL) return;
  SHGetPathFromIDList(il,path);
  if (path[0]==0) return;
  this->path=path;
  UpdateData(FALSE);
  }

//--------------------------------------------------

void C3dsExport::CreateVertexList()
  {
  fca->Create(obj.NFaces());
  txe->Create(obj.NFaces()*3);
  pta->Create(obj.NFaces()*3);
  for (int i=0;i<obj.NFaces();i++)
    {
    int idx=i*3;
    FaceT fc(obj,i);
    S3DSTRIANGLE *trg;
    for (int j=0;j<3;j++)
      {
      S3DSVECTOR *pp=(*pta)[idx+j];
      S3DSTEXCOORD *txr=(*txe)[idx+j];
      PosT& pos=obj.Point(fc.GetPoint(j));
      pp->x=-pos[0]*scale/100.0;
      pp->y=pos[1]*scale/100.0;
      pp->z=pos[2]*scale/100.0;
      txr->tu=fc.GetU(j);
      txr->tv=1.0f-fc.GetV(j);
      }
    trg=(*fca)[i];
    trg->uk1=6;
    trg->v1=idx;
    trg->v2=idx+1;
    trg->v3=idx+2;
    }   
  }

//--------------------------------------------------

struct SFINDKEY
  {
  S3DSVECTOR vect;
  S3DSTEXCOORD tutv;
  };

//--------------------------------------------------

struct SFINDINDEX
  {
  SFINDKEY key;
  int index;
  };

//--------------------------------------------------

static void OptimizeIndexList(C3dsPointArr * &pta,C3dsFaceArr * &fca,C3dsTexCoord * &txe)
  { 
  THASHTABLE tbl;
  int i,j;
  int *indexlist=new int[pta->GetCount()];
  Hash_InitEx(&tbl,10001,SFINDINDEX,key,0);
  for (i=0;i<pta->GetCount();i++)
    {
    SFINDINDEX idx,*fnd;
    idx.index=i;
    idx.key.tutv=*(*txe)[i];
    idx.key.vect=*(*pta)[i];
    fnd=(SFINDINDEX *)Hash_Find(&tbl,&idx);
    if (fnd==NULL) fnd=(SFINDINDEX *)Hash_Add(&tbl,&idx);
    indexlist[i]=fnd->index;
    }
  for (j=0,i=0;i<pta->GetCount();i++)
    {
    if (indexlist[i]==i)
      {
      if (i!=j) 
        {
        *(*pta)[j]=*(*pta)[i];
        *(*txe)[j]=*(*txe)[i];
        }
      indexlist[i]=j;
      j++;
      }
    else 
      indexlist[i]=indexlist[indexlist[i]];
    }
  C3dsPointArr *ptn=new C3dsPointArr();
  C3dsTexCoord *txn=new C3dsTexCoord();
  ptn->Create(j);
  txn->Create(j);
  for (i=0;i<j;i++)
    {
    *(*ptn)[i]=*(*pta)[i];
    *(*txn)[i]=*(*txe)[i];
    }
  for (i=0;i<fca->GetCount();i++)
    {
    S3DSTRIANGLE *trg=(*fca)[i];
    trg->v1=indexlist[trg->v1];
    trg->v2=indexlist[trg->v2];
    trg->v3=indexlist[trg->v3];
    }
  delete pta;
  delete txe;
  delete [] indexlist;
  pta=ptn;
  txe=txn;
  pta->SetChunkID(POINT_ARRAY);
  txe->SetChunkID(TEX_VERTS);
  Hash_Done(&tbl);
  }

//--------------------------------------------------

#include "oldStuff\pal2pac.hpp"
#include "TexList.h"

static bool Pac2Tga(const char *pacname, const char *tganame)
  //Funkce vraci TRUE, pokud je potreba alfa mapa
  {
  TextureData tex;
  if (!tex.Load(pacname)) return false;
  fstream str(tganame,ios::out|ios::binary|ios::trunc);
  if (!str) return false;
  OpenTGAFile(str,tex.W(),tex.H(),24);
  bool alpha=false;
  for (int y=tex.H()-1;y>=0;y--)
    for (int x=0;x<tex.W();x++)
      {
      DWORD p=tex.GetPixel(x,y);
      str.write((char *)&p,3);
      if ((p>>24)!=0xFF) alpha=true;
      }
  void CloseTGAFile(ostream& str);
  return alpha;
  }

//--------------------------------------------------

static void Pac2TgaAlpha(const char *pacname, const char *tganame)
  {
  TextureData tex;
  if (!tex.Load(pacname)) return;
  fstream str(tganame,ios::out|ios::binary|ios::trunc);
  if (!str) return;
  OpenTGAFile(str,tex.W(),tex.H(),8);
  for (int y=tex.H()-1;y>=0;y--)
    for (int x=0;x<tex.W();x++)
      {
      DWORD p=tex.GetPixel(x,y)>>24;
      str.write((char *)&p,1);
      }
  void CloseTGAFile(ostream& str);
  }

//--------------------------------------------------

struct SMATLISTITEM
  {
  char texname[256];
  char dosname[13];
  char alphname[13];
  };

//--------------------------------------------------

static void helper3ds(C3dsChunk *top,C3dsChunk *in1,C3dsChunk *in2,CHUNKID id1, CHUNKID id2, DWORD per=0)
  {
  in1->SetChunkID(id1);
  in2->SetChunkID(id2);
  top->InsertChunk(true,in1);
  in1->InsertChunk(true,in2);
  if (id2==INT_PERCENTAGE) ((C3dsPercentage *)in2)->SetValue(per);
  if (id2==COLOR_24) 
    {
    S3DSRGB rgb;
    rgb.r=(per>>16)&0xFF;
    rgb.g=(per>>8)&0xFF;
    rgb.b=per&0xFF;
    ((C3dsRGBTriplet *)in2)->SetRGB(rgb);
    }
  }

//--------------------------------------------------

static void TexMapHelper(C3dsChunk *texmap)
  {
  C3dsPercentage *per=new C3dsPercentage();per->SetChunkID(INT_PERCENTAGE);
  C3dsData *tmp;
  
  per->SetValue(100);
  texmap->InsertChunk(true,per);
  tmp=new C3dsData();
  texmap->InsertChunk(true,tmp);tmp->SetChunkID(MAT_MAP_TILING);
  *(short *)tmp->CreateData(2)=0;
  tmp=new C3dsData();
  texmap->InsertChunk(true,tmp);tmp->SetChunkID(MAT_MAP_TEXBLUR);
  *(long *)tmp->CreateData(4)=0;	
  }

//--------------------------------------------------

static const char *RemoveBackslash(const char *q)
  {
  const char *i=strrchr(q,'\\');
  if (i==NULL) return q;
  else return ++i;
  }

//--------------------------------------------------
/*
void C3dsExport::CreateMaterialList(C3dsChunk *mdata, C3dsChunk *farray)
  {
  ObjToolMatLib matlib=obj.GetTool<ObjToolMatLib>();
  BTree<ObjMatLibItem> matlibdb;
  matlib.ReadMatLib(matlibdb,ObjToolMatLib::ReadTextures);
  AutoArray<RString> textures;
  matlib.CheckFilesExists(matlibdb,config->GetString(IDS_CFGPATHFORTEX));
  matlib.ConvContainerToArray(matlibdb,textures);
  unsigned short *buff=new unsigned short[obj.NFaces()];
/**  THASHTABLE tbl;
  Hash_InitEx(&tbl,1001,SMATLISTITEM,texname,HASHF_EXISTERROR);
  int i,cnt=0;
  for (i=0;i<obj.NFaces();i++)
    {
    SMATLISTITEM it;
    memset(it.texname,0,sizeof(it.texname));
    FaceT pos(obj,i);
    strncpy(it.texname,pos.GetTexture(),sizeof(it.texname)-1);
    if (Hash_Add(&tbl,&it)!=NULL) cnt+=2;
    }
  SMATLISTITEM *fnd;**\
  int idx=0;
  Pathname src,trg;
  src.SetDirectory(config->GetString(IDS_CFGPATHFORTEX));
  trg.SetDirectory(path);
  char ntempl[13];
  strncpy(ntempl,sname.GetFilename(),5);
  ntempl[5]=0;
    {
    char *c=strchr(ntempl,'.');
    if (c!=NULL) c[0]=0;
    }
  strcat(ntempl,"%03X.TGA");
  strupr(ntempl);
  int p=0;
  for (int i=0;i<textures.Size();i++)
    {
    C3dsData *tmp;	
    char ntemp[15];
    C3dsChunk *top=new C3dsChunk();
    C3dsChunk *texmap;
    C3dsChunk *alpharem;
    C3dsNamedObject *nm=new C3dsNamedObject();
    top->SetChunkID(MAT_ENTRY);
    sprintf(ntemp,"%03X",p++);
    strncpy(ntemp+3,RemoveBackslash(textures[i]),sizeof(ntemp)-3);
    ntemp[sizeof(ntemp)-1]=0;
    nm->SetChunkID(MAT_NAME);
    nm->SetName(ntemp);
    sprintf(dosname,ntempl,idx++);
    sprintf(alphname,ntempl,idx++);	
    C3dsNamedObject *tex;	
    // Opacity map
    tex=new C3dsNamedObject();
    texmap=new C3dsChunk();
    texmap->SetChunkID(MAT_OPACMAP);
    texmap->InsertChunk(true,tex);
    tex->SetChunkID(MAT_MAPNAME);
    tex->SetName(alphname);
    TexMapHelper(texmap);
    top->InsertChunk(true,texmap);
    // Texture map
    tex=new C3dsNamedObject();
    texmap=new C3dsChunk();
    texmap->SetChunkID(MAT_TEXMAP);
    texmap->InsertChunk(true,tex);
    tex->SetChunkID(MAT_MAPNAME);
    tex->SetName(dosname);
    TexMapHelper(texmap);
    top->InsertChunk(true,texmap);
    alpharem=texmap;
    // XPFALLIN
    tmp=new C3dsData();tmp->SetChunkID(MAT_XPFALLIN);
    top->InsertChunk(true,tmp);
    // WIRESUZE
    tmp=new C3dsData();tmp->SetChunkID(MAT_WIRESIZE);
    *(float *)tmp->CreateData(4)=1.0f;
    top->InsertChunk(true,tmp);
    //
    helper3ds(top,new C3dsChunk(),new C3dsPercentage(),MAT_SELF_ILPCT,INT_PERCENTAGE,0);
    //SHADING
    tmp=new C3dsData();
    top->InsertChunk(true,tmp);tmp->SetChunkID(MAT_SHADING);
    *(short *)tmp->CreateData(2)=3;
    //
    helper3ds(top,new C3dsChunk(),new C3dsPercentage(),MAT_REFBLUR,INT_PERCENTAGE,0);
    helper3ds(top,new C3dsChunk(),new C3dsPercentage(),MAT_XPFALL,INT_PERCENTAGE,0);
    helper3ds(top,new C3dsChunk(),new C3dsPercentage(),MAT_TRANSPARENCY,INT_PERCENTAGE,0);
    helper3ds(top,new C3dsChunk(),new C3dsPercentage(),MAT_SHIN2PCT,INT_PERCENTAGE,5);
    helper3ds(top,new C3dsChunk(),new C3dsPercentage(),MAT_SHININESS,INT_PERCENTAGE,25);
    helper3ds(top,new C3dsChunk(),new C3dsRGBTriplet(),MAT_SPECULAR,COLOR_24,0xFFFFFF);
    helper3ds(top,new C3dsChunk(),new C3dsRGBTriplet(),MAT_DIFFUSE,COLOR_24,0xCCCCCC);
    helper3ds(top,new C3dsChunk(),new C3dsRGBTriplet(),MAT_AMBIENT,COLOR_24,0x00);
    top->InsertChunk(true,nm);
    if (export_tex)
      {
      bool alfneed;
      src.SetFilename(textures[i]);
      trg.SetFilename(dosname);
      alfneed=Pac2Tga(src,trg);
      if (!nonalphadetect) alfneed=true;
      if (alfneed)
        {
        trg.SetFilename(alphname);
        Pac2TgaAlpha(src,trg);
        }
      else
        {
        alpharem->DeleteNext(false,false);
        }
      }
    int k;
    for (i=0,k=0;i<obj.NFaces();i++)
      {
      FaceT pos(obj,i);
      if (stricmp(textures[i],pos.GetTexture())==0)
        {
        buff[k++]=i;
        }
      }
    C3dsData *matg=new C3dsData();
    matg->SetChunkID(MSH_MAT_GROUP);
    char *c=strcpy((char *)matg->CreateData(k*2+strlen(ntemp)+3),ntemp);
    c=strchr(c,0)+1;
    memcpy(c,&k,2);c+=2;
    memcpy(c,buff,k*2);
    farray->InsertChunk(true,matg);
    mdata->InsertChunk(false,top);
    SetProgress(idx*100/cnt);
    }
  delete [] buff;  
  }
*/
//--------------------------------------------------

static void RemoveHidden(ObjectData& obj)
  {
  for (int i=0;i<obj.NFaces();i++)
    {
    const FaceT fc(obj,i);
    int j;
    for (j=0;j<3;j++)
      {
      const PosT& ps=obj.Point(fc.GetPoint(j));
      if (ps.flags & POINT_SPECIAL_HIDDEN) break;
      }
    if (j!=3)
      {
      obj.DeleteFace(i);
      i-=2;
      if (i<0) i=0;
      }
    }
  }

//--------------------------------------------------

void C3dsExport::OnOK() 
  {
  if (UpdateData(TRUE)==FALSE) return;
  CDialog::OnOK();
  if (dirselect==0)
    {
    path=sname.GetDirectoryWithDrive();    
    }
  obj=*odata;
  RemoveHidden(obj);
  obj.Triangulate(true);  
/*  HRSRC src=FindResource(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDR_3DS),"IDA_3DS");
  HGLOBAL hglb=LoadResource(AfxGetInstanceHandle(),src);
  char *ptr=(char *)LockResource(hglb);
  DWORD size=SizeofResource(AfxGetInstanceHandle(),src);
  istrstream f(ptr,size);*/
  C3dsMaster master;
//  master.Read3ds(f);  
  master.SetChunkID(M3DMAGIC);
  C3dsChunk *mdata=new C3dsChunk();
  mdata->SetChunkID(MDATA);
  master.InsertChunk(true,mdata);
  C3dsData *version=new C3dsData();
  version->SetChunkID(M3D_VERSION);
  unsigned long *data=(unsigned long *)version->CreateData(4);
  *data=3;
  master.InsertChunk(true,version);
  if (mdata)
    {
    ExportByTopology(mdata,obj);
    version=new C3dsData();
    version->SetChunkID(MASTER_SCALE);
    data=(unsigned long *)version->CreateData(4);
    *data=0x3F800000;
    mdata->InsertChunk(true,version);
    version=new C3dsData();
    version->SetChunkID(MESH_VERSION);
    data=(unsigned long *)version->CreateData(4);
    *data=3;
    mdata->InsertChunk(true,version);
    ofstream out(sname,ios::out|ios::binary|ios::trunc);
    if (!(!out))
      {
      master.Save(out);
      return;
      }
    AfxMessageBox(IDS_FAILEDTOOPEN3DS);
    }
  AfxMessageBox(IDS_INVALID3DSTEMPLATE);
  }

//--------------------------------------------------

struct SFaceIncidence
  {
  long fromvx;
  long tovx;
  long facenum;
  };

//--------------------------------------------------

static void UpdateIncidence(int *graph, int face1, int face2)
  {
  int *q1=graph+face1*8;
  int *q2=graph+face2*8;
  int i;
  if (face1==face2) return;
  for (i=0;i<8;i++) 
    if (q1[i]==face2) break;
  else if (q1[i]==-1) 
    {q1[i]=face2;break;}
  for (i=0;i<8;i++) 
    if (q2[i]==face1) break;
  else if (q2[i]==-1) 
    {q2[i]=face1;break;}
  }

//--------------------------------------------------

void C3dsExport::CreateGraph()
  {
  THASHTABLE tbl;
  CEdges egs;
  obj.LoadSharpEdges(egs);
  
  incidence.SetVertices(obj.NFaces());
  incidence.Clear();
  sharpi.SetVertices(obj.NFaces());
  sharpi.Clear();
  Hash_Init2(&tbl,10001,sizeof(SFaceIncidence),0,8,0);
  for (int i=0;i<obj.NFaces();i++)
    {
    int *q=graph+(i*3);
    FaceT fc(obj,i);
    SFaceIncidence fcinc,*fnd;
    for (int j=0;j<3;j++)
      {
      int k=j+1;if (k>=3) k=0;
      fcinc.fromvx=fc.GetPoint(k);
      fcinc.tovx=fc.GetPoint(j);
      if ((fnd=(SFaceIncidence *)Hash_Find(&tbl,&fcinc))==NULL)
        {
        fcinc.fromvx=fc.GetPoint(j);
        fcinc.tovx=fc.GetPoint(k);
        fcinc.facenum=i;
        Hash_Add(&tbl,&fcinc);
        }
      else if (i!=fnd->facenum)
        {
        if (egs.IsEdge(fcinc.fromvx,fcinc.tovx))
          {
          sharpi.SetEdge(i,fnd->facenum);
          sharpi.SetEdge(fnd->facenum,i);
          }
        else
          {
          incidence.SetEdge(i,fnd->facenum);
          incidence.SetEdge(fnd->facenum,i);
          }
        }
      }
    }
  Hash_Done(&tbl);
  }

//--------------------------------------------------

void C3dsExport::CreateSmoothGroups()
  {
  smoothgrps=new unsigned long[obj.NFaces()];
  obj.GetTool<ObjToolTopology>().BuildSmoothGroups(smoothgrps);
/*  memset(smoothgrps,0,sizeof(long)*obj.NFaces());
  unsigned long *forbidden=new unsigned long[obj.NFaces()];
  memset(forbidden,0,sizeof(long)*obj.NFaces());
  bool modify=true;
  bool repeat=false;
  unsigned long l=1;
  int p;
  do
    {
    int i,j;
    int k;
    repeat=false;
    do
      {
      modify=false;
      for (i=0;i<obj.NFaces();i++)
        {
        if (!repeat && smoothgrps[i]==0) 
          {repeat=true;p=i;}
        for (j=0;(k=incidence.GetEdge(i,j))>=0;j++)
          {
          if (forbidden[i]!=forbidden[k]) modify=true;
          forbidden[k]|=forbidden[i];
          }
        for (j=0;(k=sharpi.GetEdge(i,j))>=0;j++)
          {
          if ((smoothgrps[i] & forbidden[k]) != smoothgrps[i]) modify=true;
          forbidden[k]|=smoothgrps[i];
          }
        }
      }
    while(modify);
    if (!repeat) break;
    unsigned long f=forbidden[p];
    l=1;
    while (f&1) 
      {l<<=1;f>>=1;}
    if (l==0) l=1;
    smoothgrps[p]=l;
    SetProgress(p*100/obj.NFaces());
    for (i=0;i<obj.NFaces();i++)
      {
      for (j=0;(k=incidence.GetEdge(i,j))>=0;j++)
        {
        if (smoothgrps[k]==0 && smoothgrps[i]!=0) 
          {
          smoothgrps[k]=smoothgrps[i];		  
          modify=true;
          }
        }
      }
    }
  while (true);
  delete [] forbidden;*/
  }

//--------------------------------------------------

/*
void C3dsExport::CreateSmoothGroups()
  {
  smoothgrps=new unsigned long[obj.NFaces()];
  CEdges egs;
  ReadEdges(&obj, egs);
  THASHTABLE tbl;
  Hash_Init2(&tbl,10001,sizeof(SFaceIncidence),0,8,0);
  for (int i=0;i<obj.NFaces();i++)
	{
	FaceT& fc=obj.Face(i);
	long l=0,nl=0;
	  {	  
	  for (int j=0;j<fc.n;j++)
		{
		int k=j+1;if (k>=fc.n) k=0;
		SFaceIncidence fcinc,*fnd;
		bool eg;

		fcinc.fromvx=fc.vs[k].point;
		fcinc.tovx=fc.vs[j].point;
		eg=egs.IsEdge(fcinc.fromvx,fcinc.tovx);
		fnd=(SFaceIncidence *)Hash_Find(&tbl,&fcinc);
		if (fnd!=NULL)		  
		  if (eg) {l&=~fnd->facenum;nl|=fnd->facenum;}
		  else l|=fnd->facenum;		  
		fcinc.fromvx=fc.vs[j].point;
		fcinc.tovx=fc.vs[k].point;
		fnd=(SFaceIncidence *)Hash_Find(&tbl,&fcinc);
		if (fnd!=NULL)		  
		  if (eg) {l&=~fnd->facenum;nl|=fnd->facenum;}
		  else l|=fnd->facenum;		  
		}
	  long grp=1;
	  if (l==0)
		while (nl&1) {grp<<=1;nl>>=1;}
	  for (j=0;j<fc.n;j++)
		{
		int k=j+1;if (k>=fc.n) k=0;
		SFaceIncidence fcinc,*fnd;
		
		fcinc.fromvx=fc.vs[k].point;
		fcinc.tovx=fc.vs[j].point;
		fcinc.facenum=grp;
		fnd=(SFaceIncidence *)Hash_Find(&tbl,&fcinc);
		if (fnd!=NULL)		  
		  fnd->facenum|=grp;		  
		else Hash_Add(&tbl,&fcinc);
		fcinc.fromvx=fc.vs[j].point;
		fcinc.tovx=fc.vs[k].point;
		fnd=(SFaceIncidence *)Hash_Find(&tbl,&fcinc);
		if (fnd!=NULL)		  
	    fnd->facenum|=grp;		  
		}
	  smoothgrps[i]=grp;
	  }
	}
  }
*/

/*void C3dsExport::ExportObject(ObjectData &obj, C3dsChunk *mdata, C3dsChunk *triobj, int index)
{
    fca=new C3dsFaceArr();
    txe=new C3dsTexCoord();
    pta=new C3dsPointArr();
    CreateVertexList();
    if (opt_vertices)	  
      OptimizeIndexList(pta,fca,txe);
    triobj->DeleteNext(true,true);
    C3dsObjectMatrix *objm=new C3dsObjectMatrix();
    HMATRIX mm;
    NulovaMatice(mm);   
    mm[0][0]=-1;
    mm[1][2]=1;
    mm[2][1]=1;
    mm[3][3]=1;
    objm->SetMatrix(mm);
    fca->SetChunkID(FACE_ARRAY);
    txe->SetChunkID(TEX_VERTS);
    pta->SetChunkID(POINT_ARRAY);
    objm->SetChunkID(MESH_MATRIX);
    triobj->InsertChunk(true,pta);
    pta->InsertChunk(false,txe);
    txe->InsertChunk(false,objm);
    objm->InsertChunk(false,fca);
    if (creat_material) CreateMaterialList(mdata->SubChunk(),fca);
    if (create_smooth)
      {
      CreateGraph();
      CreateSmoothGroups();
      C3dsData *smooth=new C3dsData();
      unsigned long *s=(unsigned long *)smooth->CreateData(obj.NFaces()*4);
      for (int i=0;i<obj.NFaces();i++) 
        {s[i]=smoothgrps[i];}
      smooth->SetChunkID(SMOOTH_GROUP);
      fca->InsertChunk(true,smooth);
      delete [] smoothgrps;
      }

}*/

bool C3dsExport::CreateObjectFromSelection(Selection *sel, S3dsExportFaceInfo *nfo, int index)
  {
  char buff[40];
  sprintf(buff,"OBJ%04d",index);
  C3dsNamedObject *namedObject=new C3dsNamedObject();
  namedObject->SetChunkID(NAMED_OBJECT);
  namedObject->SetName(buff);
  int nfaces=sel->NFaces();
  if (nfaces>65000/3) return false;
  C3dsPointArr *pta=new C3dsPointArr();
  C3dsFaceArr *fca=new C3dsFaceArr();
  C3dsTexCoord *txe=new C3dsTexCoord();
  fca->SetChunkID(FACE_ARRAY);
  txe->SetChunkID(TEX_VERTS);
  pta->SetChunkID(POINT_ARRAY);
  fca->Create(nfaces);
  txe->Create(nfaces*3);
  pta->Create(nfaces*3);
  int pos=0;
  for (int i=0;i<obj.NFaces();i++) if (sel->FaceSelected(i))
    {
    int idx=pos*3;
    FaceT fc(obj,i);
    S3DSTRIANGLE *trg;
    for (int j=0;j<3;j++)
      {
      S3DSVECTOR *pp=(*pta)[idx+j];
      S3DSTEXCOORD *txr=(*txe)[idx+j];
      PosT& pos=obj.Point(fc.GetPoint(j));
      pp->x=-pos[0]*scale/100.0;
      pp->y=pos[1]*scale/100.0;
      pp->z=pos[2]*scale/100.0;
      txr->tu=fc.GetU(j);
      txr->tv=1.0f-fc.GetV(j);
      }
    nfo[i].faceindex=pos;
    nfo[i].object=namedObject;
    trg=(*fca)[pos++];
    trg->uk1=6;
    trg->v1=idx;
    trg->v2=idx+1;
    trg->v3=idx+2;
    }   
  OptimizeIndexList(pta,fca,txe);
  C3dsChunk *tri_object=new C3dsChunk();
  C3dsObjectMatrix *objm=new C3dsObjectMatrix();
  HMATRIX mm;
  NulovaMatice(mm);   
  mm[0][0]=-1;
  mm[1][2]=1;
  mm[2][1]=1;
  mm[3][3]=1;
  objm->SetMatrix(mm);
  objm->SetChunkID(MESH_MATRIX);
  tri_object->SetChunkID(N_TRI_OBJECT);
  tri_object->InsertChunk(true,fca);
  tri_object->InsertChunk(true,objm);
  tri_object->InsertChunk(true,txe);
  tri_object->InsertChunk(true,pta);
  namedObject->InsertChunk(true,tri_object);
  return true;
  }


void C3dsExport::CreateMaterialList( C3dsChunk *mdata, S3dsExportFaceInfo *nfo, C3dsNamedObject **nmlist, int nmlistcnt)
  {
  ObjToolMatLib matlib=obj.GetTool<ObjToolMatLib>();
  BTree<ObjMatLibItem> matlibdb;
  AutoArray<RString> textures;

  matlib.ReadMatLib(matlibdb,ObjToolMatLib::ReadTextures);
  matlib.CheckFilesExists(matlibdb,config->GetString(IDS_CFGPATHFORTEX));
  matlib.ConvContainerToArray(matlibdb,textures);

  int idx=0;
  Pathname src,trg;
  char ntempl[13];
  unsigned short *buff=new unsigned short[obj.NFaces()];

  src.SetDirectory(config->GetString(IDS_CFGPATHFORTEX));
  trg.SetDirectory(path);

  strncpy(ntempl,sname.GetFilename(),5);
  ntempl[5]=0;
    {
    char *c=strchr(ntempl,'.');
    if (c!=NULL) c[0]=0;
    }
  strcat(ntempl,"%03X.TGA");
  _strupr(ntempl);

  int p=0;
  int i;
  ProgressBar<int> pb(textures.Size());
  for (int itex=0;itex<textures.Size();itex++)
    {
    pb.AdvanceNext(1);
    char dosname[13];
    char alphname[13];
    char ntemp[15];
    const char *texname=textures[itex];
    C3dsData *tmp;	
    C3dsChunk *top=new C3dsChunk();
    C3dsChunk *texmap;
    C3dsChunk *alpharem;
    C3dsNamedObject *nm=new C3dsNamedObject();
    C3dsNamedObject *tex;	

    top->SetChunkID(MAT_ENTRY);
    sprintf(ntemp,"%03X",p++);
    strncpy(ntemp+3,RemoveBackslash(texname),sizeof(ntemp)-3);
    ntemp[sizeof(ntemp)-1]=0;
    nm->SetChunkID(MAT_NAME);
    nm->SetName(ntemp);
    sprintf(dosname,ntempl,idx++);
    sprintf(alphname,ntempl,idx++);	
    // Opacity map
    tex=new C3dsNamedObject();
    texmap=new C3dsChunk();
    texmap->SetChunkID(MAT_OPACMAP);
    texmap->InsertChunk(true,tex);
    tex->SetChunkID(MAT_MAPNAME);
    tex->SetName(alphname);
    TexMapHelper(texmap);
    top->InsertChunk(true,texmap);
    // Texture map
    tex=new C3dsNamedObject();
    texmap=new C3dsChunk();
    texmap->SetChunkID(MAT_TEXMAP);
    texmap->InsertChunk(true,tex);
    tex->SetChunkID(MAT_MAPNAME);
    tex->SetName(dosname);
    TexMapHelper(texmap);
    top->InsertChunk(true,texmap);
    alpharem=texmap;
    // XPFALLIN
    tmp=new C3dsData();tmp->SetChunkID(MAT_XPFALLIN);
    top->InsertChunk(true,tmp);
    // WIRESUZE
    tmp=new C3dsData();tmp->SetChunkID(MAT_WIRESIZE);
    *(float *)tmp->CreateData(4)=1.0f;
    top->InsertChunk(true,tmp);
    //
    helper3ds(top,new C3dsChunk(),new C3dsPercentage(),MAT_SELF_ILPCT,INT_PERCENTAGE,0);
    //SHADING
    tmp=new C3dsData();
    top->InsertChunk(true,tmp);tmp->SetChunkID(MAT_SHADING);
    *(short *)tmp->CreateData(2)=3;
    //
    helper3ds(top,new C3dsChunk(),new C3dsPercentage(),MAT_REFBLUR,INT_PERCENTAGE,0);
    helper3ds(top,new C3dsChunk(),new C3dsPercentage(),MAT_XPFALL,INT_PERCENTAGE,0);
    helper3ds(top,new C3dsChunk(),new C3dsPercentage(),MAT_TRANSPARENCY,INT_PERCENTAGE,0);
    helper3ds(top,new C3dsChunk(),new C3dsPercentage(),MAT_SHIN2PCT,INT_PERCENTAGE,5);
    helper3ds(top,new C3dsChunk(),new C3dsPercentage(),MAT_SHININESS,INT_PERCENTAGE,25);
    helper3ds(top,new C3dsChunk(),new C3dsRGBTriplet(),MAT_SPECULAR,COLOR_24,0xFFFFFF);
    helper3ds(top,new C3dsChunk(),new C3dsRGBTriplet(),MAT_DIFFUSE,COLOR_24,0xCCCCCC);
    helper3ds(top,new C3dsChunk(),new C3dsRGBTriplet(),MAT_AMBIENT,COLOR_24,0x00);
    top->InsertChunk(true,nm);
    if (export_tex)
      {
      bool alfneed;
      src.SetFilename(texname);
      trg.SetFilename(dosname);
      alfneed=Pac2Tga(src,trg);
      if (!nonalphadetect) alfneed=true;
      if (alfneed)
        {
        trg.SetFilename(alphname);
        Pac2TgaAlpha(src,trg);
        }
      else
        {
        alpharem->DeleteNext(false,false);
        }
      }
    for (int nmi=0;nmi<nmlistcnt;nmi++)
      {
      int k;
      for (i=0,k=0;i<obj.NFaces();i++) if (nfo[i].object==nmlist[nmi])
        {
        FaceT pos(obj,i);
        if (strcmp(texname,pos.GetTexture())==0)
          {
          buff[k++]=nfo[i].faceindex;
          }
        }
      if (k)
        {
        C3dsData *matg=new C3dsData();
        matg->SetChunkID(MSH_MAT_GROUP);
        char *c=strcpy((char *)matg->CreateData(k*2+strlen(ntemp)+3),ntemp);
        c=strchr(c,0)+1;
        memcpy(c,&k,2);c+=2;
        memcpy(c,buff,k*2);
        C3dsChunk *tri_object=nmlist[nmi]->FindChunkSub(FACE_ARRAY);
        if (tri_object) tri_object->InsertChunk(true,matg);
        }
      }
    mdata->InsertChunk(true,top);
    }
  delete [] buff;
  }


static inline int GetFirstUnusedFaceIndex(Selection &sel)
  {
  const ObjectData *obj=sel.GetOwner();
  for (int i=0;i<obj->NFaces();i++) if (!sel.FaceSelected(i)) return i;
  return -1;
  }

static void SetupSmoothGroups(unsigned long *smoothgrps, int nfaces, S3dsExportFaceInfo *nfo, C3dsNamedObject **list, int lcount)
  {
  for (int i=0;i<lcount;i++)
    {
    int j;
    int size=0;
    for (j=0;j<nfaces;j++) if (nfo[j].object==list[i]) size++;
    if (size)
      {
      C3dsData *chunk=new C3dsData;
      chunk->SetChunkID(SMOOTH_GROUP);
      unsigned long *data=(unsigned long *)chunk->CreateData(size*4);
      for (j=0;j<nfaces;j++) if (nfo[j].object==list[i])        
        data[nfo[j].faceindex]=smoothgrps[j];        
      C3dsChunk *faces=list[i]->FindChunkSub(FACE_ARRAY);
      faces->InsertChunk(true,chunk);
      }
    }
  }

void C3dsExport::ExportByTopology(C3dsChunk *mdata,ObjectData &obj)
{
  S3dsExportFaceInfo *nfo=new S3dsExportFaceInfo[obj.NFaces()];
  C3dsNamedObject **objlist=new C3dsNamedObject *[obj.NFaces()];  
  Selection total(&obj),current(&obj);
  bool warn=true;
  int selface;
  int index=0;
  ProgressBar<int> pb(3);
  pb.AdvanceNext(1);
  {
    ProgressBar<int> pb(obj.NFaces());
    pb.ReportState(IDS_EXPORT3DSCREATINGOBJECT);
    while ((selface=GetFirstUnusedFaceIndex(total))!=-1)
    {
      pb.SetNextPos(selface);
      current.Clear();
      if (byTopology)
      {
        current.FaceSelect(selface);
        FaceT fc(obj,selface);      
        for (int i=0;i<fc.N();i++) current.PointSelect(fc.GetPoint(i));
        CISelect::SelectObjects(&obj,&current);    
      }
      else      
        for (int i=selface;i<obj.NFaces();i++) current.FaceSelect(i);
      while (CreateObjectFromSelection(&current,nfo,++index)==false)
      {
        index--;
        if (warn) 
        {
          AfxMessageBox(IDS_EXPORT3DSSPLIT);
          warn=false;
        }
        int nfaces=current.NFaces()/2;
        for (int i=0;i<obj.NFaces();i++) if (current.FaceSelected(i))
        {nfaces--;
        if (nfaces<0) 
          current.FaceSelect(i,0);
        }
      }
      objlist[index-1]=nfo[selface].object;
      total+=current;
    }
  }
  pb.AdvanceNext(1);
  for (int i=0;i<index;i++)    
    mdata->InsertChunk(true,objlist[i]);
  pb.ReportState(IDS_EXPORT3DCREATINGMATERIALS);
  if (creat_material) this->CreateMaterialList(mdata,nfo,objlist,index);
  pb.AdvanceNext(1);
  if (create_smooth)
  {
    pb.ReportState(IDS_EXPORT3DSMOOTHING);
    CreateGraph();
    CreateSmoothGroups();
    SetupSmoothGroups(smoothgrps,obj.NFaces(),nfo,objlist,index);
    delete [] smoothgrps;
  }
  delete [] nfo;
  delete [] objlist;
}
