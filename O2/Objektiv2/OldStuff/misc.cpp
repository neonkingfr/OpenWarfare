#include "..\stdafx.h"
#include "..\resource.h"
#include "optima2mfc.h"
#include "misc.h"


#define strCockpitGunner IDS_LOD_VIEW_GUNNER
#define strCockpitPilot IDS_LOD_VIEW_PILOT
#define strCockpitCommander IDS_LOD_VIEW_CARGO


#define WResourceString GetStringRes

RString GetStringRes(int IDC)
  {
  char buff[256];
  LoadString(AfxGetInstanceHandle(),IDC,buff,sizeof(buff));
  return buff;
  }



RString LODResolToNamePositive( float resol )
  {
  char buff[256];
  //if( resol>1e14 && resol<1e16 ) return "Memory";
  if (CheckSpecLOD(resol, LOD_GEOMETRY_BUOYANCY)) return GetStringRes(IDS_LOD_GEOMETRY_BUOYANCY);
  if (CheckSpecLOD(resol, LOD_GEOMETRY_PHYS)) return GetStringRes(IDS_LOD_GEOMETRY_PHYS);
  if (CheckSpecLOD(resol, LOD_GEOMETRY_PHYS_OLD)) return GetStringRes(IDS_LOD_GEOMETRY_PHYS_OLD);

  if (ISMASS(resol)) return GetStringRes(IDS_LOD_GEOMETRY);
  if( resol>SPEC_LOD*0.5 )
    {
    int resolId=(resol+SPEC_LOD*0.5)/SPEC_LOD;
    switch( resolId )
      {
      case 0: return GetStringRes(IDS_LOD_xxx);
      case 1: return GetStringRes(IDS_LOD_MEMORY);
      case 2: return GetStringRes(IDS_LOD_LANDCONTACT);
      case 3: return GetStringRes(IDS_LOD_ROADWAY);
      case 4: return GetStringRes(IDS_LOD_PATHS);
      case 5: return GetStringRes(IDS_LOD_HPOINTS);
      case 6: return GetStringRes(IDS_LOD_VIEWGEO);
      case 7: return GetStringRes(IDS_LOD_FIREGEO);
      case 8: return GetStringRes(IDS_LOD_VIEW_CARGO_GEOMETRY);
      case 9: return GetStringRes(IDS_LOD_VIEW_CARGO_FGEOMETRY);
      case 10: return GetStringRes(IDS_LOD_VIEW_COMMANDER);
      case 11: return GetStringRes(IDS_LOD_VIEW_COMM_GEOMETRY);
      case 12: return GetStringRes(IDS_LOD_VIEW_COMM_FGEOMETRY);
      case 13: return GetStringRes(IDS_LOD_VIEW_PILOT_GEOMETRY);
      case 14: return GetStringRes(IDS_LOD_VIEW_PILOT_FGEOMETRY);
      case 15: return GetStringRes(IDS_LOD_VIEW_GUNNER_GEOMETRY);
      case 16: return GetStringRes(IDS_LOD_VIEW_GUNNER_FGEOMETRY);
      case 17: return GetStringRes(IDS_LOD_SUB_PARTS);
      case 18: return GetStringRes(IDS_LOD_VIEWCARGOSHADOWVOLUME);
      case 19: return GetStringRes(IDS_LOD_VIEWPILOTSHADOWVOLUME);
      case 20: return GetStringRes(IDS_LOD_VIEWGUNNERSHADOWVOLUME);
      case 21: return GetStringRes(IDS_LOD_WRECK);
      default:
        {
        char buff[256];
        sprintf(buff,"%g Spec",resol);        
        return buff;
        }
      }
    }
  if( resol>999 && resol<1001 ) return WResourceString(strCockpitGunner);
  if( resol>1099 && resol<1101 ) return WResourceString(strCockpitPilot);
  if( resol>1199 && resol<1201 ) return WResourceString(strCockpitCommander);
  if (resol>=10000 && resol<20000) 
    {
    sprintf(buff,"%s %.3f",GetStringRes(IDS_LOD_SHADOW).Data(),resol-10000.0f);        
    return buff;
    }
  else if (resol>=20000 && resol<30000) 
    {
    sprintf(buff,"%s %.3f",GetStringRes(IDS_LOD_EDITTEMP).Data(),resol-20000.0f);        
    return buff;
    }
  else
    {
    snprintf(buff,sizeof(buff),"%7.3f",resol);
    return buff;
    }
  }

float LODNameToResolPositive( RString name )
  {

  if( !_strcmpi(name,GetStringRes(IDS_LOD_GEOMETRY_BUOYANCY)) ) return LOD_GEOMETRY_BUOYANCY;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_GEOMETRY_PHYS)) ) return LOD_GEOMETRY_PHYS;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_GEOMETRY_PHYS_OLD)) ) return LOD_GEOMETRY_PHYS_OLD;

  if( !_strcmpi(name,GetStringRes(IDS_LOD_GEOMETRY)) ) return 1e13;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_MEMORY)) ) return SPEC_LOD*1;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_LANDCONTACT)) ) return SPEC_LOD*2;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_ROADWAY)) ) return SPEC_LOD*3;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_PATHS)) ) return SPEC_LOD*4;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_HPOINTS)) ) return SPEC_LOD*5;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_VIEWGEO)) ) return SPEC_LOD*6;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_FIREGEO)) ) return SPEC_LOD*7;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_VIEW_CARGO_GEOMETRY)) ) return SPEC_LOD*8;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_VIEW_CARGO_FGEOMETRY)) ) return SPEC_LOD*9;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_VIEW_COMMANDER)) ) return SPEC_LOD*10;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_VIEW_COMM_GEOMETRY)) ) return SPEC_LOD*11;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_VIEW_COMM_FGEOMETRY)) ) return SPEC_LOD*12;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_VIEW_PILOT_GEOMETRY)) ) return SPEC_LOD*13;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_VIEW_PILOT_FGEOMETRY)) ) return SPEC_LOD*14;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_VIEW_GUNNER_GEOMETRY)) ) return SPEC_LOD*15;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_VIEW_GUNNER_FGEOMETRY)) ) return SPEC_LOD*16;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_SUB_PARTS)) ) return SPEC_LOD*17;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_VIEWCARGOSHADOWVOLUME)) ) return SPEC_LOD*18;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_VIEWPILOTSHADOWVOLUME)) ) return SPEC_LOD*19;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_VIEWGUNNERSHADOWVOLUME)) ) return SPEC_LOD*20;
  if( !_strcmpi(name,GetStringRes(IDS_LOD_WRECK)) ) return SPEC_LOD*21;
  if( !_strcmpi(name,GetStringRes(strCockpitGunner)) ) return 1000;
  if( !_strcmpi(name,GetStringRes(strCockpitPilot)) ) return 1100;
  if( !_strcmpi(name,GetStringRes(strCockpitCommander)) ) return 1200; 
  RString sprf=GetStringRes(IDS_LOD_SHADOW);
  if (!strncmp(name,sprf,sprf.GetLength()))
    {
    float z=0.0f;
    sscanf((LPCTSTR)sprf+sprf.GetLength(),"%f",&z);
    return z+10000.0f;
    }
  sprf=GetStringRes(IDS_LOD_EDITTEMP);
  if (!strncmp(name,sprf,sprf.GetLength()))
    {
    float z=0.0f;
    sscanf(name.Data()+sprf.GetLength(),"%f",&z);
    return z+20000.0f;
    }
  return atof(name);
  }

  RString LODResolToName( float resol )
  {
    if (resol >= 0)
      return LODResolToNamePositive(resol);

    return RString("-", LODResolToNamePositive(-resol));  
  }

  float LODNameToResol( RString name )
  {
    if (name[0] != '-')
      return LODNameToResolPositive(name);

    return - LODNameToResolPositive(RString(name.Data() + 1));
  }


//WFilePath DataRoot; // path to viewer application

bool DeCzech( char *dst, const char *src )
  {
  char *dst0=dst;
  const char *src0=src;
  strcpy(dst,src);
  dst=strrchr(dst,'\\');if( !dst ) dst=dst0;else dst++;
  src=strrchr(src,'\\');if( !src ) src=src0;else src++;
  int secLen=0;
  while( *src )
    {
    char c=*src++;
    if( c=='\\' || c=='.' || c==':' ) secLen=0,*dst++=c;
    else if( secLen<20 && c!='&' )
      {
      if( isascii(c) && c!=' ' )
        {
        *dst++=c;
        }
      else
        {
        static const char from[]="�������������̊�؎������ҍ��";
        static const char to[]="escrzyaieuuntdoESCRZYAIEUUNTDO";
        const char *isC=strchr(from,c);
        if( isC ) *dst++=to[isC-from];
        else *dst++='_';
        }
      secLen++;
      }
    }
  *dst=0;
  return strcmp(dst0,src0)!=0;
  }










/*void __cdecl ErrorMessage(const char *format,...)
  {
  CString text;
  va_list a;
  va_start(a,format);
  text.FormatV(format,a);
  AfxMessageBox(text);
  }
*/








/*
void __cdecl WarningMessage(const char *format,...)
  {
  CString text;
  va_list a;
  va_start(a,format);
  text.FormatV(format,a);
  AfxMessageBox(text);
  }
*/









/*void __cdecl LstF(const char *format,...)
  {
  CString text;
  va_list a;
  va_start(a,format);
  text.FormatV(format,a);
  AfxMessageBox(text);
  }
*/









//TypeIsSimple(SSectionInfo);
/*
static DWORD miscCalculateStringHash(const char *string, DWORD multval)
  {
  DWORD suma=0;
  while (*string)
    {
    suma=suma*multval+*(const unsigned char *)string;
    string++;
    }
  return suma;
  }










static DWORD miscCalculateHash(const SSectionInfo *v1, DWORD multval)
  {
  return miscCalculateStringHash(v1->material,multval)+
    miscCalculateStringHash(v1->texture,multval)+
      v1->flags;
  }










static bool miscCompareFaces(FaceT &fc1, FaceT &fc2)
  {
  return fc1.GetTexture()==fc2.GetTexture() && fc1.GetMaterial()==fc2.GetMaterial()&& fc1.GetFlags()==fc2.GetFlags();
  }










static int miscCompareFacesHashs(const void *v1, const void *v2)
  {
  const SSectionInfo *ss1=(const SSectionInfo *)v1;
  const SSectionInfo *ss2=(const SSectionInfo *)v2;
  if (ss1->hash==ss2->hash)
    {
    int cp=((DWORD_PTR)ss1->material>(DWORD_PTR)ss2->material)-((DWORD_PTR)ss1->material<(DWORD_PTR)ss2->material);
    if (cp==0)
      {
      cp=((DWORD_PTR)ss1->texture>(DWORD_PTR)ss2->texture)-((DWORD_PTR)ss1->texture<(DWORD_PTR)ss2->texture);
      if (cp==0)
        {
        return (ss1->flags>ss2->flags)-(ss1->flags<ss2->flags);
        }
      else
        return cp;
      }
    return cp;
    }
  return (ss1->hash>ss2->hash)-(ss1->hash<ss2->hash);
  }










struct miscCalculateSectionsThreadInfo
  {
  static HANDLE thread;
  AutoArray<SSectionInfo> *target;
  AutoArray<SSectionInfo> temp;
  HWND message;
  int *result;
  };










static miscCalculateSectionsThreadInfo CalcSectInfo;

HANDLE miscCalculateSectionsThreadInfo::thread=0;

ULONG _stdcall miscCalculateSectionsThread(LPVOID data)
  {
  LogF("(CalcSect) Entering miscCalculateSectionsThread");
  CalcSectInfo.target->QSortBin(miscCompareFacesHashs);  //serad pole podle sekci a hash
  int newindex=0;                       //zacni prerozdelovat sekce od nuly
  int i;
  for (i=1;i<CalcSectInfo.target->Size();i++)      
    {                                           //dva sousedni faces
    if (miscCompareFacesHashs(&(*CalcSectInfo.target)[i-1],&(*CalcSectInfo.target)[i]))
      {
      newindex++;
      }
    (*CalcSectInfo.target)[i].section=newindex;
    }
  *CalcSectInfo.result=(*CalcSectInfo.target)[CalcSectInfo.target->Size()-1].section+1;  //vrat celkovy pocet sekci.
  LogF("(CalcSect) Exitting miscCalculateSectionsThread");
  PostMessage(CalcSectInfo.message,MSG_ENDSECTIONSCALC ,0,0);
  return 0;
  }




bool miscCalculateSectionsRunning()
  {
  return WaitForSingleObject(CalcSectInfo.thread,0)==WAIT_TIMEOUT;
  }





HANDLE miscCalculateSections(ObjectData *object, int *result, AutoArray<SSectionInfo> *target, HWND message)
  {
  LogF("(CalcSect) Request calculate sections");
  if (WaitForSingleObject(CalcSectInfo.thread,0)==WAIT_TIMEOUT)
    return NULL;
  if (object->NFaces()==0) 
    {
    *result=0;
    return NULL;
    //pro nula facu je nula sekci
    }
  LogF("(CalcSect) Begin calculate sections");
  if (target==NULL) CalcSectInfo.target=&CalcSectInfo.temp;        
  else CalcSectInfo.target=target;
  CalcSectInfo.result=result;
  CalcSectInfo.message=message;
  int i;
  CalcSectInfo.target->Resize(object->NFaces());
  for (i=0;i<object->NFaces();i++)    
    {
	CalcSectInfo.target->Access(i);
    SSectionInfo &nfo=(*CalcSectInfo.target)[i];   //nejprve napln pomocne pole
    FaceT fc(object,i);            //Zaznamenej face
    nfo.texture=(const char *)fc.GetTexture();
    nfo.material=(const char *)fc.GetMaterial();
    nfo.flags=fc.GetFlags()& ~FACE_COLORIZE_MASK;
    nfo.hash=0;//miscCalculateHash(&nfo,31);
    nfo.section=0;                        //kazda face patri do prvni sekce
    }
  
  DWORD id;
  LogF("(CalcSect) Sections prepared, starting background thread");
  CloseHandle(CalcSectInfo.thread);
  CalcSectInfo.thread=CreateThread(NULL,0,miscCalculateSectionsThread,NULL,0,&id);
  SetThreadPriority(CalcSectInfo.thread,THREAD_PRIORITY_IDLE);
  LogF("(CalcSect) Done");
  return CalcSectInfo.thread;
  }






*/





