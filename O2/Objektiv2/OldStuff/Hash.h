#ifndef _HASH_H_
#define _HASH_H_

#ifdef __cplusplus
extern "C"
{
#endif

typedef char (*ITEMDESTRUCTOR)(void *item);

#define HASH_DEFUSIZE 16

#define HASHF_ADDEND 0x1 //pri zakladani se uklada zezadu
#define HASHF_EXISTERROR 0x2 //pokud prvek existuje je nahlasena chyba
#define HASHF_UPDATE 0x4 //pri zakladani se nejprve prvek naleze a updatuje
#define HASHF_DELALL 0x8 //pri mazani se vymazou vsechny duplicity
#define HASHF_DELLAST 0x20 //pri mazani se vymaze pouze naposledy vlozeny klic
#define HASHF_POINTERS 0x40 //Uklada do tabulek jen ukazatele
#define HASHF_SPEEDADD 0x80 //Urychluje operace vkladani, tvorbou seznamu volnych popisovacu

typedef struct _thashitem
  {
  void *data;
  struct _thashitem *next;
  }THASHITEM,* HHASHITEM;


typedef struct _thashtable
  {
  int entries;   //pocet vstupu v tabulce - u retezcu max 65536
  int datasize;  //max velikost ulozenych dat
  int keyoffset; //pozice klice
  int keylen;    //velikost klice - u retezcu max 2.
  int flags;     //vlajky - HASHF_????
  HHASHITEM *table;   //ukazatel na pole navigatoru, pro inicializaci musi byt NULL;
  HHASHITEM freelist; //seznam volnych polozek;
  ITEMDESTRUCTOR destructor;
  int dataoffs;    //pro zaruceni kompatibility - obsahuje offset, kde zacinaji uzivatelska data
  }THASHTABLE;

typedef struct _thashenumstate
  {
  long eline;
  long eitem;
  void **enav;
  long reserved;
  }THASHENUMSTATE;

typedef struct _thashstate
  {
  long total_data;  //pocet vsech dat
  long largest_string; //nejvetsi delka retezcu
  long shortest_string; //nejvetsi delka retezcu
  int unused_entries;   //pocet nepouzitych klicu
  int avg_entry;  //pocet zaznamu na jeden entry v prumeru
  int avg_entry2; //pocey zaznamu na jeden pouzity klic v prumeru
  int percent;  //procentualni vyuziti vsech klicu
  }THASHSTATE;

char Hash_Init(THASHTABLE *table);
char Hash_Init2(THASHTABLE *table, int entries, int datasize, int keyoffset, int keysize, int flags);
void *Hash_Add(THASHTABLE *table,void *data); //pridava do tabulky
void *Hash_AddEx(THASHTABLE *table,THASHITEM *data); //pridava do tabulky pripraveny item
void *Hash_Find(THASHTABLE *table,void *data); //hleda duplicitni data
void *Hash_FindKey(THASHTABLE *table,void *key); //hleda podle klice
void *Hash_FindNext(THASHTABLE *table,void *data); //hleda dalsi. data je pointer na predchozi data
void *Hash_EnumData(THASHTABLE *table, void *lastfind);
//void Hash_InitEnum(THASHTABLE *table,THASHENUMSTATE *enumstate);//inicializuje enumeraci.
//void *Hash_EnumNext(THASHTABLE *table, THASHENUMSTATE *enumstate); //prohledava celou tabulku.
char Hash_Delete(THASHTABLE *table,void *key); //maze klic z tabulky
void *Hash_Fill(THASHTABLE *table,void *data); //vyplni data hodnotou z tabulky
void Hash_Done(THASHTABLE *table); //likviduje tabulku
void Hash_GetState(THASHTABLE *table,THASHSTATE *state);//zjistuje ruzne informace
void Hash_Clean(THASHTABLE *table);
char Hash_DeleteExact(THASHTABLE *table, void *found, char flags); //vymaze data nalezena funkci FindKey, Find, FindNext, Enum
void Hash_Flush(THASHTABLE *table); //likviduje tabulku
#define HASHD_DESTROYED 1


#define Hash_SetDestructor(table,destruct) ((table)->destructor=(destruct))


#define GetOffset(s,m) (int)&(((s *)0)->m)
#define GetKeySize(s,m) sizeof((((s *)0)->m))


#define Hash_GetKey(tbl,dt) ((void *)((char *)(dt)+(tbl)->keyoffset))
#define Hash_InitEx(table,entries,field,key,flags) Hash_Init2(table,entries,sizeof(field),GetOffset(field,key),GetKeySize(field,key),flags)
//Napriklad Hash_InitEx(table,16391,TMYOBJECT,mykey,HASHF_UPDATE);


//------------------- HASH CACHE--------------------------

typedef struct _tagHCHEITEM
  {
  struct _tagHCHEITEM *down;
  struct _tagHCHEITEM *up;
  char item[1];
  }THCHEITEM;


#define THCHESIZE() (2*sizeof(THCHEITEM *))

typedef struct _tagHASHCACHE
  {
  THASHTABLE hash;
  THCHEITEM *begin,*end;
  int cachelen,cachemax;
  }THASHCACHE;

#define Cache_InitEx(cache,entries,cachelen,field,key) Cache_Init(cache,entries,cachelen,sizeof(field),GetOffset(field,key),GetKeySize(field,key))

#define Cache_SetDestructor(cache,destr) Hash_SetDestructor(&((cache)->hash),destr)

char Cache_Init(THASHCACHE *cache,int entries,int cachelen,int datasize, int keyoffs,int keylen);
char Cache_Done(THASHCACHE *cache);
char Cache_Flush(THASHCACHE *cache);
char Cache_Invalidate(THASHCACHE *cache,void *key);
void *Cache_Find(THASHCACHE *cache,void *key);
void *Cache_Add(THASHCACHE *cache,void *data);
  //znici pripadna data, pokud jsou v cache dlouho.

//------------------- TIMERS --------------------------

typedef unsigned long HTIMER;

typedef struct _tagTimerItem
  {
  unsigned long time,nextcall;
  long counter;
  HTIMER timerid;
  void *data;
  }TTIMERITEM;

typedef struct _tagTHashTimer
  {
  THASHTABLE hash;
  unsigned long lastcheck;
  }THASHTIMER;


char Timer_Init(THASHTIMER *timer, int entries, int datasize, int flags);
HTIMER Timer_Add(THASHTIMER *timer, unsigned long time, void *data);
HTIMER Timer_AddEx(THASHTIMER *timer, TTIMERITEM *item);
TTIMERITEM *Timer_Run(THASHTIMER *timer, unsigned long curtime, TTIMERITEM *last);
char Timer_Kill(THASHTIMER *timer, HTIMER timerid);
char Timer_KillAll(THASHTIMER *timer);
void Timer_Done(THASHTIMER *timer);
void Timer_SetTime(THASHTIMER *timer, unsigned long time);

#define Timer_SetDestructor(timer,destr) Hash_SetDestructor(&((timer)->hash),destr)


#ifdef __cplusplus
}
#endif

#endif


