#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <math.h>
#include "matrix.h"

HVECTOR _hmatrix_static_vector;

void JednotkovaMatice(LPHMATRIX m)
{
  int i;
  int j;
  for(i=0;i<4;i++)
    for(j=0;j<4;j++)
      m[i][j]=i==j?1.0f:0.0f;
}

//--------------------------------------------------

void NulovaMatice(LPHMATRIX m)
{
  memset(m,0,sizeof(HMATRIX));
}

//--------------------------------------------------

void SoucinMatic(LPHMATRIX m1,LPHMATRIX m2,LPHMATRIX vysledek)
{
  int i,k;
  int j;
  float s;
  char copy=0;

  if (vysledek==m1)
  {
    copy=1;vysledek=(void *)alloca(sizeof(HMATRIX));
  }
  for(k=0;k<4;k++)
    for(i=0;i<4;i++)
    {
      s=0.0f;
      for(j=0;j<4;j++)
        s+=m1[k][j]*m2[j][i];
      vysledek[k][i]=s;
    }
    if (copy) memcpy(m1,vysledek,sizeof(HMATRIX));
}

//--------------------------------------------------

void MocninaMatice(LPHMATRIX m,LPHMATRIX vysledek)
{
  SoucinMatic(m,m,vysledek);
}

//--------------------------------------------------

void Translace(LPHMATRIX m,float xs,float ys,float zs)
{
  JednotkovaMatice(m);
  m[3][0]=xs;
  m[3][1]=ys;
  m[3][2]=zs;
}

//--------------------------------------------------

void RotaceX(LPHMATRIX m,float uhel)
{
  float sins,coss;
  sins=(float)sin(uhel);
  coss=(float)cos(uhel);
  JednotkovaMatice(m);
  m[1][1]=coss;
  m[1][2]=sins;
  m[2][1]=-sins;
  m[2][2]=coss;
}

//--------------------------------------------------

void RotaceY(LPHMATRIX m,float uhel)
{
  float sins,coss;
  sins=(float)sin(uhel);
  coss=(float)cos(uhel);
  JednotkovaMatice(m);
  m[0][0]=coss;
  m[0][2]=sins;
  m[2][0]=-sins;
  m[2][2]=coss;
}

//--------------------------------------------------

void RotaceZ(LPHMATRIX m,float uhel)
{
  float sins,coss;
  sins=(float)sin(uhel);
  coss=(float)cos(uhel);
  JednotkovaMatice(m);
  m[0][0]=coss;
  m[0][1]=sins;
  m[1][0]=-sins;
  m[1][1]=coss;
}

//--------------------------------------------------

void Zoom(LPHMATRIX m,float Sx,float Sy,float Sz)
{
  JednotkovaMatice(m);
  m[0][0]=Sx;
  m[1][1]=Sy;
  m[2][2]=Sz;
}

//--------------------------------------------------

void TransformVector2(LPHMATRIX m,LPHVECTOR v,LPHVECTOR z)
{
  z[XVAL]=v[XVAL]*m[0][0]+v[YVAL]*m[1][0]+v[ZVAL]*m[2][0]+v[WVAL]*m[3][0];
  z[YVAL]=v[XVAL]*m[0][1]+v[YVAL]*m[1][1]+v[ZVAL]*m[2][1]+v[WVAL]*m[3][1];
  z[ZVAL]=v[XVAL]*m[0][2]+v[YVAL]*m[1][2]+v[ZVAL]*m[2][2]+v[WVAL]*m[3][2];
  z[WVAL]=v[XVAL]*m[0][3]+v[YVAL]*m[1][3]+v[ZVAL]*m[2][3]+v[WVAL]*m[3][3];
}

//--------------------------------------------------

void TransformVector(LPHMATRIX m, LPHVECTOR v, LPHVECTOR z)
{
  __asm
  {
    mov   ebx,m
      mov   esi,v
      mov   edi,z
      fld   dword ptr [esi]
      fld   dword ptr [esi+4]
      fld   dword ptr [esi+8]
      fld   dword ptr [esi+12]
      fld   dword ptr [ebx+0]
      fmul  st,st(4)
        fld   dword ptr [ebx+16]
        fmul  st,st(4)
          fld   dword ptr [ebx+32]
          fmul  st,st(4)
            fld   dword ptr [ebx+48]
            fmul  st,st(4)
              faddp st(1),st
              faddp st(1),st
              faddp st(1),st
              fstp  dword ptr [edi+0]
              fld   dword ptr [ebx+4]
              fmul  st,st(4)
                fld   dword ptr [ebx+20]
                fmul  st,st(4)
                  fld   dword ptr [ebx+36]
                  fmul  st,st(4)
                    fld   dword ptr [ebx+52]
                    fmul  st,st(4)
                      faddp st(1),st
                      faddp st(1),st
                      faddp st(1),st
                      fstp  dword ptr [edi+4]
                      fld   dword ptr [ebx+8]
                      fmul  st,st(4)
                        fld   dword ptr [ebx+24]
                        fmul  st,st(4)
                          fld   dword ptr [ebx+40]
                          fmul  st,st(4)
                            fld   dword ptr [ebx+56]
                            fmul  st,st(4)
                              faddp st(1),st
                              faddp st(1),st
                              faddp st(1),st
                              fstp  dword ptr [edi+8]
                              fld   dword ptr [ebx+12]
                              fmul  st,st(4)
                                fld   dword ptr [ebx+28]
                                fmul  st,st(4)
                                  fld   dword ptr [ebx+44]
                                  fmul  st,st(4)
                                    fld   dword ptr [ebx+60]
                                    fmul  st,st(4)
                                      faddp st(1),st
                                      faddp st(1),st
                                      faddp st(1),st
                                      fstp  dword ptr [edi+12]
                                      ffree st(0)
                                        ffree st(1)
                                        ffree st(2)
                                        ffree st(3)
  }
}

//--------------------------------------------------

void TransformVectorNoTranslate2(LPHMATRIX m,LPHVECTOR v,LPHVECTOR z)
{
  z[XVAL]=v[XVAL]*m[0][0]+v[YVAL]*m[1][0]+v[ZVAL]*m[2][0];
  z[YVAL]=v[XVAL]*m[0][1]+v[YVAL]*m[1][1]+v[ZVAL]*m[2][1];
  z[ZVAL]=v[XVAL]*m[0][2]+v[YVAL]*m[1][2]+v[ZVAL]*m[2][2];
  z[WVAL]=1.0f;
}

//--------------------------------------------------

void TransformVectorNoTranslate(LPHMATRIX m,LPHVECTOR v,LPHVECTOR z)
{
  __asm
  {
    mov   ebx,m
      mov   esi,v
      mov   edi,z
      fld   dword ptr [esi]
      fld   dword ptr [esi+4]
      fld   dword ptr [esi+8]
      fld1
        fld   dword ptr [esi+12]
        fdivr st(1),st
          fmul  st(2),st
          fmul  st(3),st
          fmulp st(4),st
          fld   dword ptr [ebx+0]
          fmul  st,st(4)
            fld   dword ptr [ebx+16]
            fmul  st,st(4)
              fld   dword ptr [ebx+32]
              fmul  st,st(4)
                faddp st(1),st
                faddp st(1),st
                fstp  dword ptr [edi+0]
                fld   dword ptr [ebx+4]
                fmul  st,st(4)
                  fld   dword ptr [ebx+20]
                  fmul  st,st(4)
                    fld   dword ptr [ebx+36]
                    fmul  st,st(4)
                      faddp st(1),st
                      faddp st(1),st
                      fstp  dword ptr [edi+4]
                      fld   dword ptr [ebx+8]
                      fmul  st,st(4)
                        fld   dword ptr [ebx+24]
                        fmul  st,st(4)
                          fld   dword ptr [ebx+40]
                          fmul  st,st(4)
                            faddp st(1),st
                            faddp st(1),st
                            fstp  dword ptr [edi+8]
                            fstp  dword ptr [edi+12]
                            ffree st(0)
                              ffree st(1)
                              ffree st(2)
  }
}

//--------------------------------------------------

void InvertujMatici(LPHMATRIX m) /* v karterskych souradnicich!!! */
{
  float f;

  f=m[0][1];m[0][1]=m[1][0];m[1][0]=f;
  f=m[0][2];m[0][2]=m[2][0];m[2][0]=f;
  f=m[1][2];m[1][2]=m[2][1];m[2][1]=f;
  f=m[2][2];m[2][2]=m[2][2];m[2][2]=f;
  m[3][0]=-m[3][0];
  m[3][1]=-m[3][1];
  m[3][2]=-m[3][2];
}

//--------------------------------------------------

/*void TransformVertex(HMATRIX m,HVERTEX *vin,HVERTEX *vout)
{
float modul;
TransformVector(m,vin->pozice,vout->pozice);
TransformVectorNoTranslate(m,vin->,vout->smer);
modul=ModulVektoru2(vout->smer);
modul=sqrt(modul);
Normalize(vout->smer,modul);
}*/


void CreateProjection(LPHMATRIX m,float front,float back, float half)
{
  float    dTmp1,dTmp2;
  float		 DhF=front/(half*back);


  dTmp1 = back / (back - front);
  dTmp2 = -dTmp1*front;


  NulovaMatice(m);
  m[0][0]=DhF;
  m[1][1]=DhF;
  m[2][2]=dTmp1;
  m[2][3]=1.0f;
  m[3][2]=dTmp2;
  m[3][3]=0.0f;
}

//--------------------------------------------------

void CreateProjection2(LPHMATRIX m,float front,float back, float fov)
{
  float cotarg=(fov/2.0f);
  float cot=(float)(cos(cotarg)/sin(cotarg));
  float Q=back/(back-front);

  NulovaMatice(m);
  m[0][0]=cot;
  m[1][1]=cot;
  m[2][2]=Q;
  m[2][3]=1.0f;
  m[3][2]=-Q*front;
  m[3][3]=0.0f;
}

//--------------------------------------------------

#define LINEOP(mm,cnt,lin1,lin2)\
  mm[lin2][0]=mm[lin1][0]*(cnt)+mm[lin2][0],\
  mm[lin2][1]=mm[lin1][1]*(cnt)+mm[lin2][1],\
  mm[lin2][2]=mm[lin1][2]*(cnt)+mm[lin2][2],\
  mm[lin2][3]=mm[lin1][3]*(cnt)+mm[lin2][3]

#define COMB(mm,lin1,lin2,sl) (-mm[lin2][sl]/mm[lin1][sl])
#define LINEDIV(mm,lin,cnt) \
  mm[lin][0]/=cnt,mm[lin][1]/=cnt,mm[lin][2]/=cnt,mm[lin][3]/=cnt

/*char InverzeMatice(LPHMATRIX in,LPHMATRIX out)
{
float c;
int i,j;
JednotkovaMatice(out);

for (i=0;i<4;i++)
for (j=0;j<4 && (fabs(in[i][i])<0.05f);j++)
if (i!=j) {LINEOP(in,1.0f,j,i);LINEOP(out,1.0f,j,i);}

c=COMB(in,0,1,0);LINEOP(in,c,0,1);LINEOP(out,c,0,1);
c=COMB(in,0,2,0);LINEOP(in,c,0,2);LINEOP(out,c,0,2);
c=COMB(in,0,3,0);LINEOP(in,c,0,3);LINEOP(out,c,0,3);
c=COMB(in,1,2,1);LINEOP(in,c,1,2);LINEOP(out,c,1,2);
c=COMB(in,1,3,1);LINEOP(in,c,1,3);LINEOP(out,c,1,3);
c=COMB(in,2,3,2);LINEOP(in,c,2,3);LINEOP(out,c,2,3);

c=COMB(in,3,2,3);LINEOP(in,c,3,2);LINEOP(out,c,3,2);
c=COMB(in,3,1,3);LINEOP(in,c,3,1);LINEOP(out,c,3,1);
c=COMB(in,3,0,3);LINEOP(in,c,3,0);LINEOP(out,c,3,0);
c=COMB(in,2,1,2);LINEOP(in,c,2,1);LINEOP(out,c,2,1);
c=COMB(in,2,0,2);LINEOP(in,c,2,0);LINEOP(out,c,2,0);
c=COMB(in,1,0,1);LINEOP(in,c,1,0);LINEOP(out,c,1,0);

c=in[0][0];LINEDIV(out,0,c);
c=in[1][1];LINEDIV(out,1,c);
c=in[2][2];LINEDIV(out,2,c);
c=in[3][3];LINEDIV(out,3,c);
return 0;
}
*/

#include "invmat.cpp"

char InverzeMatice(LPHMATRIX in,LPHMATRIX out)
{
  Invert2((float *)in,(float *)out);
  return 0;
}

//--------------------------------------------------

/*char InverzeMatice( LPHMATRIX in, LPHMATRIX out)
{
char lx,ly;
float lDeterminant;

lDeterminant = 0.0f;
for (lx=0;lx<4;lx++)
{
for (ly=0;ly<4;ly++)
{
out[ lx][ly ] =
in[ (lx+1) & 3][(ly+1) & 3] *
in[ (lx+2) & 3][(ly+2) & 3] *
in[ (lx+3) & 3][(ly+3) & 3] +

in[ (lx+1) & 3][(ly+2) & 3] *
in[ (lx+2) & 3][(ly+3) & 3] *
in[ (lx+3) & 3][(ly+1) & 3] +

in[ (lx+1) & 3][(ly+3) & 3] *
in[ (lx+2) & 3][(ly+1) & 3] *
in[ (lx+3) & 3][(ly+2) & 3] -

in[ (lx+1) & 3][(ly+3) & 3] *
in[ (lx+2) & 3][(ly+2) & 3] *
in[ (lx+3) & 3][(ly+1) & 3] -

in[ (lx+1) & 3][(ly+2) & 3] *
in[ (lx+2) & 3][(ly+1) & 3] *
in[ (lx+3) & 3][(ly+3) & 3] -

in[ (lx+1) & 3][(ly+1) & 3] *
in[ (lx+2) & 3][(ly+3) & 3] *
in[ (lx+3) & 3][(ly+2) & 3] ;
if (((lx+ly) & 1 ) == 1) out[lx][ly ]=-out[ ly][ly ];
}
lDeterminant+=out[0][lx] * in[ lx][0 ];
}
if (lDeterminant!=0) return -1;
lDeterminant=1.0f/lDeterminant;
for (lx= 0;lx<4;lx++)
for (ly= 0;ly<4;ly++)
out[lx][ly] = out[lx][ly]*lDeterminant;
return 0;
}
*/
void CorrectVector(LPHVECTOR v)
{
  float w1=1/v[WVAL];

  v[XVAL]*=w1;
  v[YVAL]*=w1;
  v[ZVAL]*=w1;
  v[WVAL]=1.0f;
}

//--------------------------------------------------

void NormalizeVector(LPHVECTOR v)
{
  float m,m2;

  m2=ModulVektoru2(v);
  m=(float)sqrt(m2);
  m=1/m;
  v[XVAL]*=m;
  v[YVAL]*=m;
  v[ZVAL]*=m;
  v[WVAL]=1.0f;
}

//--------------------------------------------------

void PohledovaMatice(LPHVECTOR fromvect, LPHVECTOR tovect, LPHVECTOR up, LPHMATRIX mm)
{
  HVECTOR v1;
  HVECTOR v2;

  memcpy(v1,tovect,sizeof(HVECTOR));
  RozdilVektoru(v1,fromvect);
  v1[WVAL]=1.0f;
  NormalizeVector(v1);
  VektorSoucin(v1,up,v2);
  mm[0][0]=up[XVAL];
  mm[1][0]=up[YVAL];
  mm[2][0]=up[ZVAL];
  mm[3][0]=-Scalar1(up,fromvect);
  mm[0][1]=v2[XVAL];
  mm[1][1]=v2[YVAL];
  mm[2][1]=v2[ZVAL];
  mm[3][1]=-Scalar1(v2,fromvect);
  mm[0][2]=v1[XVAL];
  mm[1][2]=v1[YVAL];
  mm[2][2]=v1[ZVAL];
  mm[3][2]=-Scalar1(v1,fromvect);
  mm[0][3]=mm[1][3]=mm[2][3]=0.0f;
  mm[3][3]=1.0f;
}

//--------------------------------------------------

static TMATRIXSTACKITEM *mxsfree=NULL;

void mxsInitStack(HMATRIXSTACK stk)
{
  memset(stk,0,sizeof(TMATRIXSTACK));
}

//--------------------------------------------------

static char mxsForceCalc(HMATRIXSTACK stk)
{
  TMATRIXSTACKITEM *it;
  if (mxsfree==NULL) it=(TMATRIXSTACKITEM *)malloc(sizeof(TMATRIXSTACKITEM));
  else 
  {it=mxsfree;mxsfree=it->next;}
  if (it!=NULL)
  {
    if ((it->next=stk->top)==NULL)
      CopyMatice(it->matrix,stk->temp);
    else
      if (stk->forcecalc==1)
        SoucinMatic(stk->temp,stk->top->matrix,it->matrix);
      else
        SoucinMatic(stk->top->matrix,stk->temp,it->matrix);
    stk->top=it;
  }
  stk->forcecalc=0;
  if (it==NULL) stk->errorcounter++;
  return (it==NULL);
}

//--------------------------------------------------

LPHMATRIX mxsPushL(HMATRIXSTACK stk)
{
  if (stk->forcecalc) mxsForceCalc(stk);
  stk->forcecalc=1;
  return stk->temp;
}

//--------------------------------------------------

LPHMATRIX mxsPushR(HMATRIXSTACK stk)
{
  if (stk->forcecalc) mxsForceCalc(stk);
  stk->forcecalc=2;
  return stk->temp;
}

//--------------------------------------------------

LPHMATRIX mxsTop(HMATRIXSTACK stk)
{
  if (stk->forcecalc) mxsForceCalc(stk);
  if (stk->top==NULL)
  {
    JednotkovaMatice(stk->temp);
    return stk->temp;
  }
  else return stk->top->matrix;
}

//--------------------------------------------------

LPHMATRIX mxsPop(HMATRIXSTACK stk, HMATRIX mm)
{
  TMATRIXSTACKITEM *it;
  if (stk->forcecalc)
    if (mm==NULL)
    {
      stk->forcecalc=0;
      return mm;
    }
    else
    {
      if (stk->top==NULL)
      {
        CopyMatice(mm,stk->temp);
        return mm;
      }
      mxsForceCalc(stk);
    }
    it=stk->top;
    if (it==NULL)
    {
      if (mm!=NULL) JednotkovaMatice(mm);
      return mm;
    }
    if (mm!=NULL)
      CopyMatice(mm,it->matrix);
    if (stk->errorcounter)
      stk->errorcounter--;
    else
    {
      stk->top=it->next;
      it->next=mxsfree;
      mxsfree=it;
    }
    return mm;
}

//--------------------------------------------------

void mxsFreeStack(HMATRIXSTACK stk)
{
  while (stk->top!=NULL)
  {
    mxsPop(stk,NULL);
  }
}

//--------------------------------------------------

void mxsResetTables(void)
{
  TMATRIXSTACKITEM *it;
  while (mxsfree!=NULL)
  {
    it=mxsfree;
    mxsfree=it->next;
    free(it);
  }
}

//--------------------------------------------------

//-------------------- quaternions ---------------------------

#define EPSILON 0.00001f
#define HALFPI 1.570796326794895f

static int nxt[3]=
{YVAL,ZVAL,XVAL};

//--------------------------------------------------

LPHMATRIX mxQuatToMatrix(HQUATER q, HMATRIX mat)
{
  float s,xs,ys,zs,wx,wy,wz,xx,xy,xz,yy,yz,zz;

  s=2.0f/(q[XVAL]*q[XVAL]+q[YVAL]*q[YVAL]+q[ZVAL]*q[ZVAL]+q[WVAL]*q[WVAL]);

  xs=q[XVAL]*s; ys=q[YVAL]*s; zs=q[ZVAL]*s;
  wx=q[WVAL]*xs; wy=q[WVAL]*ys; wz=q[WVAL]*zs;
  xx=q[XVAL]*xs; xy=q[XVAL]*ys; xz=q[XVAL]*zs;
  yy=q[YVAL]*ys; yz=q[YVAL]*zs; 
  zz=q[ZVAL]*zs;

  mat[0][0]=1.0f-(yy+zz);
  mat[0][1]=xy+wz;
  mat[0][2]=xz-wy;

  mat[1][0]=xy-wz;
  mat[1][1]=1.0f-(xx+zz);
  mat[1][2]=yz+wx;

  mat[2][0]=xz+wy;
  mat[2][1]=yz-wx;
  mat[2][2]=1.0f-(xx+yy);

  mat[0][3]=mat[1][3]=mat[2][3]=mat[3][0]=mat[3][1]=mat[3][2]=0.0f;
  mat[3][3]=1.0f;
  return mat;
}

//--------------------------------------------------

LPHQUATER mxMatrixToQuat(HMATRIX mat, HQUATER q)
{
  float tr,s;
  int i,j,k;

  tr=mat[0][0]+mat[1][1]+mat[2][2];
  if (tr>0.0f)
  {
    s=(float)sqrt(tr+1.0f);
    q[WVAL]=s*0.5f;
    s=0.5f/s;

    q[XVAL]=(mat[1][2]-mat[2][1])*s;
    q[YVAL]=(mat[2][0]-mat[0][2])*s;
    q[ZVAL]=(mat[0][1]-mat[1][0])*s;
  }
  else
  {
    i=XVAL;
    if (mat[YVAL][YVAL]>mat[XVAL][XVAL]) i=YVAL;
    if (mat[ZVAL][ZVAL]>mat[i][i]) i=ZVAL;
    j=nxt[i]; k=nxt[j];

    s=(float)sqrt( (mat[i][i]- (mat[j][j]+mat[k][k]))+1.0f);

    q[i]=s*0.5f;
    s=0.5f/s;
    q[WVAL]=(mat[j][k]-mat[k][j])*s;
    q[j]=(mat[i][j]+mat[j][i])*s;
    q[k]=(mat[i][k]+mat[k][i])*s;
  }
  return q;
}

//--------------------------------------------------

LPHQUATER mxSlerpQuat(HQUATER p, HQUATER q, float t, HQUATER qt)
{
  float omega, cosom, sinom, sclp, sclq;
  int i;

  cosom=p[XVAL]*q[XVAL]+p[YVAL]*q[YVAL]+p[ZVAL]*q[ZVAL]+p[WVAL]*q[WVAL];
  if ((1.0f+cosom)>EPSILON)
  {
    if ((1.0f-cosom)>EPSILON)
    {
      omega=(float)acos(cosom);
      sinom=1.0f/(float)sin(omega);
      sclp=(float)(sin((1.0f-t)*omega)*sinom);
      sclq=(float)(sin(t*omega)*sinom);
    }
    else
    {
      sclp=1.0f-t;
      sclq=t;
    }
    for(i=0;i<4;i++) qt[i]=sclp*p[i]+sclq*q[i];
  }
  else
  {
    qt[XVAL]=-p[YVAL];
    qt[YVAL]=p[XVAL];
    qt[ZVAL]=-p[WVAL];
    qt[WVAL]=p[ZVAL];
    sclp=(float)sin((1.0f-t)*HALFPI);
    sclq=(float)sin(t*HALFPI);
    for(i=0;i<4;i++) qt[i]=sclp*p[i]+sclq*qt[i];
  }
  return qt;
}

//--------------------------------------------------

LPHQUATER mxSquadQuat(HQUATER p, HQUATER q, HQUATER r, HQUATER s, float t, HQUATER res)
{
  HQUATER ps, qr;
  return mxSlerpQuat(mxSlerpQuat(p,s,t,ps),mxSlerpQuat(q,r,t,qr),2*t*(1-t),res);
}

//--------------------------------------------------

LPHQUATER mxMultQuat(HQUATER q, HQUATER p, HQUATER res)
{
  HVECTOR v1, v2;
  res[WVAL]=p[WVAL]*q[WVAL]-Scalar1(p,q);
  v1[XVAL]=p[XVAL]*q[WVAL];
  v1[YVAL]=p[YVAL]*q[WVAL];
  v1[ZVAL]=p[ZVAL]*q[WVAL];
  v2[XVAL]=q[XVAL]*p[WVAL];
  v2[YVAL]=q[YVAL]*p[WVAL];
  v2[ZVAL]=q[ZVAL]*p[WVAL];
  VektorSoucin(p,q,res);
  SoucetVektoru(res,v1);
  SoucetVektoru(res,v2);
  return res;
}

//--------------------------------------------------

LPHQUATER mxMakeQuat(HVECTOR axis, float rotate, HQUATER res)
{
  HVECTOR v;
  float si=(float)sin(rotate/2.0f);
  float ci=(float)cos(rotate/2.0f);
  CopyVektor(v,axis);
  NormalizeVector(v);
  res[XVAL]=si*v[XVAL];
  res[YVAL]=si*v[YVAL];
  res[ZVAL]=si*v[ZVAL];
  res[WVAL]=ci;
  return res;
}

//--------------------------------------------------

LPHQUATER mxMakeRelativeRotation(HQUATER cur, HVECTOR axis, float rotate)
{
  HQUATER temp,nw;
  CopyVektor(temp,cur);
  mxMultQuat(mxMakeQuat(axis,rotate,nw),temp,cur);
  return cur;
}

//--------------------------------------------------

LPHMATRIX mxBuildKey(HVECTOR pivot, HVECTOR pos, HQUATER rot, HVECTOR scale, HMATRIX res)
{
  res[0][0]=rot[XVAL];
  res[0][1]=rot[YVAL];
  res[0][2]=rot[ZVAL];
  res[0][3]=rot[WVAL];
  res[1][0]=scale[XVAL];
  res[1][1]=scale[YVAL];
  res[1][2]=scale[ZVAL];
  res[1][3]=scale[WVAL];  
  res[2][0]=pivot[XVAL];
  res[2][1]=pivot[YVAL];
  res[2][2]=pivot[ZVAL];
  res[2][3]=pivot[WVAL];
  res[3][0]=pos[XVAL];
  res[3][1]=pos[YVAL];
  res[3][2]=pos[ZVAL];
  res[3][3]=pos[WVAL];
  return res;
}

//--------------------------------------------------

LPHMATRIX mxInterpolateKey(HMATRIX mat1, HMATRIX mat2, float t,HMATRIX res)
{
  int i,j;
  float s=(1-t);
  for (j=1;j<4;j++)
    for (i=0;i<4;i++)
      res[j][i]=mat1[j][i]*s+mat2[j][i]*t;
  mxSlerpQuat(mat1[0],mat2[0],t,res[0]);
  return res;
}

//--------------------------------------------------

LPHMATRIX mxKeyToMatrix(HMATRIX quat, HMATRIX mat)
{
  HMATRIX m1,m2,m3;
  Translace(m1,-quat[2][0],-quat[2][1],-quat[2][2]);
  Zoom(m2,quat[1][0],quat[1][1],quat[1][2]);
  SoucinMatic(m1,m2,m3);
  mxQuatToMatrix(quat[0],m2);
  SoucinMatic(m3,m2,m1);
  Translace(m2,quat[3][XVAL],quat[3][YVAL],quat[3][ZVAL]);
  SoucinMatic(m1,m2,mat);
  //memcpy(mat[3],quat[0],sizeof(HVECTOR));
  //mat[3][3]=1.0f;
  return mat;
}

//--------------------------------------------------

LPHMATRIX mxRotateAroundAxis(HVECTOR vect, float fi, HMATRIX res)
{
  float cfi, sfi, x2, y2, z2, x, y, z;
  x=vect[XVAL];
  x2=x*x;
  y=vect[YVAL];
  y2=y*y;
  z=vect[ZVAL];
  z2=z*z;
  cfi=(float)cos(fi);
  sfi=(float)sin(fi);
  res[0][0]=x2 + (1.0f - x2)*cfi;
  res[1][0]=-z*sfi + x*y*(1-cfi);
  res[2][0]=y*sfi + x*z*(1-cfi);
  res[3][0]=0.0f;

  res[0][1]=z*sfi + x*y*(1-cfi);
  res[1][1]=y2 +(1.0f - y2)*cfi;
  res[2][1]=-x*sfi + y*z*(1-cfi);
  res[3][1]=0.0f;

  res[0][2]=-y*sfi + x*z*(1-cfi);
  res[1][2]=x*sfi + y*z*(1-cfi);
  res[2][2]=z2 + (1.0f - z2)*cfi;
  res[3][2]=0.0f;

  res[0][3]=0.0f;
  res[1][3]=0.0f;
  res[2][3]=0.0f;
  res[3][3]=1.0f;
  return res;
}

//--------------------------------------------------

//------------------ Bezier --------------

LPHVECTOR mxBezierInterpolate(HMATRIX bezier, float t, HVECTOR res)
{
  float bt[4];
  float t2,t3,u1,u2,u3;
  int i,j;

  u1=(1-t);
  u2=u1*u1;
  u3=u2*u1;
  t2=t*t;
  t3=t2*t;
  bt[0]=u3;
  bt[1]=u2*3*t;
  bt[2]=u1*3*t2;
  bt[3]=t3;
  for (i=0;i<3;i++)
    for (j=0,res[i]=0;j<4;j++)
      res[i]+=bezier[j][i]*bt[j];
  return res;
}

//--------------------------------------------------

