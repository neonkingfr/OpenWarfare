#ifndef _PAL2PAC_HPP
#define _PAL2PAC_HPP

#define MYDLL __declspec(dllexport) __stdcall

// dynamic linking types etc...

extern "C"
  {
  
  // Objektiv Interface
  
  int MYDLL AddTexture( const char *libPath, const char *textPath, int size );
  int MYDLL UpdateTexture( const char *dest, const char *src, int size );
  
  int MYDLL CombineTexture
    (
      const char *D, const char *S1, const char *S2, const char *S3, const char *S4,
  int size, const char *bank, const char *water
    );
  
  // Visitor Interface
  void MYDLL TextureMix
    (
      const char *t,
  const char *s1, const char *s2, const char *s3, const char *s4,
  const char *bankDirectory, const char *bankTexture
    );
  
  struct TextureDataStruct
    {
    int _w,_h; // data dimensions
    DWORD *_data; // pointer to ARGB 8888 data
    };
  void MYDLL CreateTextureData( TextureDataStruct &data, const char *name );
  void MYDLL DestroyTextureData( TextureDataStruct &data );
  
  COLORREF MYDLL TextureColor( const char *s );
  DWORD MYDLL TextureColorARGB( const char *s );
  
  typedef int __stdcall AddTextureT( const char *libPath, const char *textPath, int size );
  typedef int __stdcall UpdateTextureT( const char *dest, const char *src, int size );
  
  typedef void __stdcall CreateTextureDataT( TextureDataStruct &data, const char *name );
  typedef void __stdcall DestroyTextureDataT( TextureDataStruct &data );
  
  };

//--------------------------------------------------

extern UpdateTextureT *UpdateTextureF;
extern CreateTextureDataT *CreateTextureDataF;
extern DestroyTextureDataT *DestroyTextureDataF;

bool InitPal2Pac(const char *dllpath);

// class encapsulation of TextureDataStruct
class TextureData
  {
  struct TextureDataStruct _data;
  
  public:
    TextureData( const char *name=NULL )
      {
      if (!CreateTextureDataF) return;
      CreateTextureDataF(_data,name);
      }
    ~TextureData()
      {
      if (!DestroyTextureDataF) return;
      DestroyTextureDataF(_data);
      }
    bool Load( const char *name )
      {
      if (!CreateTextureDataF) return false;
      if (!DestroyTextureDataF) return false;
      DestroyTextureDataF(_data);
      CreateTextureDataF(_data,name);
      return true;
      }
    
    int W() const 
      {return _data._w;}
    int H() const 
      {return _data._h;}
    const DWORD *Data() const 
      {return _data._data;}
    DWORD GetPixel( int x, int y ) const 
      {return _data._data[y*_data._w+x];}
    
  private:
    TextureData( const TextureData &src );
    void operator =( const TextureData &src );
  };

//--------------------------------------------------

#endif
