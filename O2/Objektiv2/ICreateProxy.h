// ICreateProxy.h: interface for the CICreateProxy class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ICREATEPROXY_H__E04A4E76_D547_4DF8_B3E4_C2874F42E879__INCLUDED_)
#define AFX_ICREATEPROXY_H__E04A4E76_D547_4DF8_B3E4_C2874F42E879__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ComInt.h"

class CICreateProxy : public CICommand  
  {
  HMATRIX pin;  
  CString name;
  int id;
  public:
    virtual void Save(ostream &str);
    virtual int Execute(LODObject *lo);
    CICreateProxy(LPHMATRIX p,const char *nname)
      {CopyMatice(pin,p);name=nname;}
    virtual ~CICreateProxy();
    CICreateProxy(istream &str);
    virtual int Tag() 
      {return CITAG_CREATEPROXY;}
    
    
    
    
  };

//--------------------------------------------------

#endif // !defined(AFX_ICREATEPROXY_H__E04A4E76_D547_4DF8_B3E4_C2874F42E879__INCLUDED_)

