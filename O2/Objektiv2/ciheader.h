/***

  TODO: If you want to add new action into O2, you have to write class derived form CICommand
  This class must have min only method virtual int Execute(LODObject *obj) and this implements body of
  the action.

  There is two kind of actions: Recordable and Nonrecordable
  Nonsaveable cannot be saved in action file, because it depeneds on current situation in editor.

  If you are writting nonrecordable action, you have to reset flag recordable to false.
  If you are writting recordable action, you have to implement more of wirtual methods

  Save() - preform save operation
  Tag() - returns tag of action
  constructor, which excepts istream; - preform load operation
  and loader function, which can construct your action from the stream. 


This file is separated to the three parts. First part containts prototype list of loader function. 
There is notation for name of the function. Yoy should use this notation for new actions.

Second part containts tag definitions. If you adding new tag, you must reserve one unique value for it.

Third part is loading function. You must register new action to the loading function. Insert new line into 
"switch block" like many definition above.
*/

#include <iostream>
using namespace std;


class CICommand;

/* ----------------------- Loader function part -----------*/

CICommand *CICreatePrim_Create(istream& str);
CICommand *CIDeleteSelection_Create(istream& str);
CICommand *CIInsertPoint_Create(istream &str);
CICommand *CILodChange_Create(istream& str);
CICommand *CILodDelete_Create(istream& str);
CICommand *CILodInsert_Create(istream& str);
CICommand *CILodSetActive_Create(istream& str);
CICommand *CINewFace_Create(istream& str);
CICommand *CIRenameNamedSel_Create(istream& str);
CICommand *CISaveSelection_Create(istream& str);
CICommand *CISelOperation_Create(istream& str);
CICommand *CISelUse_Create(istream& str);
CICommand *CITransformPoints_Create(istream& str);
CICommand *CIFreeHandTrns_Create(istream &str);
CICommand *CIBackgrndMap_Create(istream &str);
CICommand *CISimpleCommand_Create(istream &str);
CICommand *CISetVertexProp_Create(istream &str);
CICommand *CISetFaceProp_Create(istream &str);
CICommand *CIFromMatrices_Create(istream &str);
CICommand *CISoftenSelected_Create(istream &str);
CICommand *CISetMass_Create(istream& str);
CICommand *CIMerge_Create(istream& str);
//CICommand *CIUserMapping_Create(istream& str);
CICommand *CIExtrude_Create(istream& str);
CICommand *CIShape_Create(istream& str);
CICommand *CISelectByTexture_Create(istream& str);
CICommand *CIMergeNear_Create(istream& str);
CICommand *CISplitHole_Create(istream& str);
CICommand *CIMoveFaces_Create(istream &str);
CICommand *CICreateProxy_Create(istream &str);
CICommand *CIFlattenPoints_Create(istream &str);
CICommand *CIToolNormalAnim_Create(istream &str);
CICommand *CIGizmoMap_Create(istream &str);
CICommand *CIBevel_Create(istream &str);
CICommand *CINormalDirection_Create(istream &str);
CICommand *CICarving_Create(istream &str);
CICommand *CIDelEmptySels_Create(istream &str);
CICommand *CINewPolygon_Create(istream &str);

/* Insert new loader functions here */

/* ----------------------- TAG PART -----------*/

#define CITAG_EOF				  0
#define CITAG_CREATEPRIM		  1
#define CITAG_DELETESELECTION	  2
#define CITAG_LODCHANGE			  3
#define CITAG_LODDELETE			  4
#define CITAG_LODINSERT			  5
#define CITAG_LODSETACTIVE		  6
#define CITAG_NEWFACE			  7
#define CITAG_RENAMENAMEDSEL	  8
#define CITAG_SAVESELECTION		  9
#define CITAG_SELOPERATION		 11
#define CITAG_SELUSE			 12
#define CITAG_TRANSFORMPOINTS    13
#define CITAG_INSERTPOINT		 14
#define CITAG_FREEHANDTRNS		 15
#define CITAG_BACKGRNDMAP		 16
#define CITAG_SIMPLECOMMAND		 17
#define CITAG_SETVERTPROP		 18
#define CITAG_SETFACEPROP		 19
#define CITAG_FROMMATRICES		 20
#define CITAG_SOFTENSELCTED		 21
#define CITAG_SETMASS			 22
#define CITAG_MERGE				 23
#define CITAG_USERMAPPING		 24
#define CITAG_EXTRUDE			 25
#define CITAG_SHAPE				 26
#define CITAG_SELECTBYTEXTURE	 27
#define CITAG_MERGENEAR			 29
#define CITAG_SPLITHOLE			 30
#define CITAG_MOVEFACES			 31
#define CITAG_SELECT			 32
#define CITAG_USEANIM			 33
#define CITAG_CREATEPROXY		 34
#define CITAG_FLATTENPOINTS		 35
#define CITAG_TOOLNORMALANIM	 36
#define CITAG_GIZMOMAP			 37
#define CITAG_BEVEL				 38
#define CITAG_NORMALDIRECTION    39
#define CITAG_CARVING            40
#define CITAG_DELETEEMPTYSELS    41
#define CITAG_NEWPOLYGON         42
#define CITAG_SETACTIVEUVSET     43
#define CITAG_CHANGEUVMAP        44

/* Insert new action tag here */


/* ----------------------- Loader function - registration -----------*/

_inline CICommand* CI_LoadTag(istream& str)
  {
  if (str.fail()) return NULL;
  int tag=-1;
  str.read((char *)&tag,sizeof(tag));
  if (str.fail() || tag==CITAG_EOF) return NULL;
  switch (tag)
    {
    case CITAG_CREATEPRIM: return CICreatePrim_Create(str);	
    case CITAG_DELETESELECTION: return CIDeleteSelection_Create(str);
    case CITAG_LODCHANGE: return CILodChange_Create(str);
    case CITAG_LODINSERT: return CILodInsert_Create(str);
    case CITAG_LODSETACTIVE:return CILodSetActive_Create(str);
    case CITAG_NEWFACE: return CINewFace_Create(str);
    case CITAG_RENAMENAMEDSEL: return CIRenameNamedSel_Create(str);
    case CITAG_SAVESELECTION: return CISaveSelection_Create(str);
    case CITAG_SELOPERATION: return CISelOperation_Create(str);
    case CITAG_SELUSE: return CISelUse_Create(str);
    case CITAG_TRANSFORMPOINTS: return CITransformPoints_Create(str);
    case CITAG_INSERTPOINT: return CIInsertPoint_Create(str);
    case CITAG_FREEHANDTRNS: return CIFreeHandTrns_Create(str);
    case CITAG_BACKGRNDMAP: return CIBackgrndMap_Create(str);
    case CITAG_SIMPLECOMMAND: return CISimpleCommand_Create(str);
    case CITAG_SETVERTPROP: return CISetVertexProp_Create(str);
    case CITAG_SETFACEPROP: return CISetFaceProp_Create(str);
    case CITAG_FROMMATRICES: return CIFromMatrices_Create(str);
    case CITAG_SOFTENSELCTED:return CISoftenSelected_Create(str);
    case CITAG_SETMASS: return CISetMass_Create(str);
    case CITAG_MERGE: return CIMerge_Create(str);
//    case CITAG_USERMAPPING: return CIUserMapping_Create(str);
    case CITAG_EXTRUDE: return CIExtrude_Create(str);
    case CITAG_SHAPE: return CIShape_Create(str);
    case CITAG_SELECTBYTEXTURE: return CISelectByTexture_Create(str);
    case CITAG_MERGENEAR: return CIMergeNear_Create(str);
    case CITAG_SPLITHOLE: return CISplitHole_Create(str);
    case CITAG_MOVEFACES: return CIMoveFaces_Create(str);
    case CITAG_CREATEPROXY: return CICreateProxy_Create(str);
    case CITAG_FLATTENPOINTS: return CIFlattenPoints_Create(str);
    #ifdef FULLVER
    case CITAG_TOOLNORMALANIM: return CIToolNormalAnim_Create(str);
    #endif
    case CITAG_GIZMOMAP: return CIGizmoMap_Create(str);
    case CITAG_BEVEL: return CIBevel_Create(str);
    case CITAG_NORMALDIRECTION: return CINormalDirection_Create(str);
    case CITAG_CARVING: return CICarving_Create(str);
    /* Insert registration here */
    case CITAG_DELETEEMPTYSELS: return CIDelEmptySels_Create(str);
    case CITAG_NEWPOLYGON:return CINewPolygon_Create(str);
    default: return NULL;
    }
  }










#define datard(str,a) str.read((char *)&a,sizeof(a))
#define datardp(str,p) str.read((char *)p,sizeof(p))
#define datardf(str,p,cnt) str.read((char *)p,sizeof(*p)*cnt)
#define datawr(str,a) str.write((char *)&a,sizeof(a))
#define datawrp(str,p) str.write((char *)p,sizeof(p))
#define datawrf(str,p,cnt) str.write((char *)p,sizeof(*p)*cnt)

#define datard_raw(str,p,size) str.read((char *)p,size))
#define datawr_raw(str,p,size) str.write((char *)p,size))