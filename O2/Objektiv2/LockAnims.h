// LockAnims.h: interface for the CLockAnims class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOCKANIMS_H__05E41219_449F_4343_AA45_6F1C004E0CC9__INCLUDED_)
#define AFX_LOCKANIMS_H__05E41219_449F_4343_AA45_6F1C004E0CC9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "optima\ObjLOD.hpp" 


#define CLA_LOCKSIZE MAX_LOD_LEVELS

class CLockAnims  
  {
  float levels[CLA_LOCKSIZE];
  DWORD lru[CLA_LOCKSIZE];
  float *weight[CLA_LOCKSIZE];
  int size[CLA_LOCKSIZE];
  DWORD lrucnt;
  public:
    CLockAnims(const CLockAnims& other);
    void DeleteFrame(float lod, int frame);
    void CheckNewAnims(float lod, int anims);
    float GetWeight(int idx, int i);
    void SetWeight(int idx,int i, float weight);
    float *GetWeights(int idx) 
      {return weight[idx];}
    int GetSize(int idx) 
      {return size[idx];}
    void Reset();
    CLockAnims();
    virtual ~CLockAnims();
    void DeleteLod(int idx);
    int CreateLod(float level, int anims);
    int FindLod(float lod);
  };

//--------------------------------------------------

#endif // !defined(AFX_LOCKANIMS_H__05E41219_449F_4343_AA45_6F1C004E0CC9__INCLUDED_)
