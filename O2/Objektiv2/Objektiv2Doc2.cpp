#include "stdafx.h"
#include <El/Btree/Btree.h>
#include "LockAnims.h"	// Added by ClassView
#include "Objektiv2.h"
#include "MainFrm.h"
#include "Objektiv2Doc.h"
#include "Objektiv2View.h"
#include "TransformDlg.h"
#include "Calculator.h"
#include "TexMappingDlg.h"
#include "VertexProp.h"
#include "FaceProp.h"
#include "GizmoMapDlg.h"
#include "ICarving.h"
#include "AllRenameDlg.h"
//#include "DlgReadOnly.h"
#include "ROCheck.h"
#include "IDeletePointsTriangulate.h"
#include "ifindnonconvexpoints.h"
#include "icreateconvexvolume.h"
#include "oldStuff\pal2pac.hpp"
#include "../ObjektivLib/bitxtimport.h"
#include "../ObjektivLib/ObjToolAnimation.h"
#include "../ObjektivLib/ObjToolClipboard.h"
#include "../ObjektivLib/ObjToolsMatLib.h"
#include "../ObjektivLib/ObjToolProxy.h"
#include "../ObjektivLib/ObjToolTopology.h"
#include "ProxyNameDlg.h"
#include "DlgSccCheckInOut.h"
#include <el/Scc/MsSccProject.hpp>
#include "DlgProxyLevel.h"



//#include "ObjectColorizer.h"
#include "SpaceWarpDlg.h"
#include <io.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <malloc.h>


CObjektiv2Doc *doc;
CComInt comint(100);

#define REDOFILE ".redo.p3d"

static CCriticalSection GLVInProgress;

static bool GLVStop=false;

CObjektiv2Doc::CObjektiv2Doc()
{
  doc=this;
  // TODO: add one-time construction code here
  LodData=NULL;
  pin=gismo[3];
  JednotkovaMatice(gismo);
  seltransform=false;
  g3dview=NULL;
  pinlock=false;
  prevanim=-1;
  curanim=-1;
  colorize=false;
  selcopy=false;
  extupdate=false;
  combine_saved=NULL;
  extaschild=NULL;
  orgname="";
  gismo_mode=gismo_hide;
  automapsel=false;
  numSections=0;    
  _proxyLevel=1;
  localAxisMode=true;
  _ssGettingVer=false;
  selrecordenable=false;
  JednotkovaMatice(selrecord);
}

static DWORD GetFileSizeByFilename(const char *lpszPathName)
{
  DWORD lastP3Dsize;
  HANDLE h=CreateFile(lpszPathName,0,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
  if (h!=INVALID_HANDLE_VALUE) lastP3Dsize=GetFileSize(h,NULL);else lastP3Dsize=0;
  CloseHandle(h);
  return lastP3Dsize;
}









CObjektiv2Doc::~CObjektiv2Doc()
{
  GLVInProgress.Lock();
  delete LodData;
  GLVInProgress.Unlock();
}










bool FileNewer(const char *src, const char *trg)
{
  struct _stat buff;  
  _stat(src,&buff);
  CTime srctm(buff.st_mtime);
  if (_stat(trg,&buff)==-1) return TRUE;
  CTime trgtm(buff.st_mtime);
  return (srctm>trgtm)==TRUE;
}










static bool SaveOxySetup(ostream &f, void *context )
{
  /*  char buff[256];
  strncpy(buff,config->GetString(IDS_CFGTEXTUREPREFIX),256);
  buff[255]=0;
  f.write(buff,256);*/
  return true;
}










static bool LoadOxySetup(istream &f, void *context )
{
  /*  char buff[256];
  f.read(buff,256);
  if (!f || !isalnum(buff[0])) 
  {strcpy(buff,"data\\");} // POZOR NATRVDO
  const char *old=config->GetString(IDS_CFGTEXTUREPREFIX);
  if (_stricmp(old,buff))
  {
  CString q;
  AfxFormatString2(q,IDS_PREFIXCHANGING,old,buff);
  if (AfxMessageBox(q,MB_YESNO)==IDYES)
  config->SetString(IDS_CFGTEXTUREPREFIX,buff,true);
  }*/
  return true;
}



class FunctorOnNewDocument
{
  mutable bool &enabled;
public:
  FunctorOnNewDocument(bool &enabled):enabled(enabled) {}
  bool operator ()(SRef<PluginGeneralAppSide> &plugin) const
  {
    if (plugin->GetPlugin().OnNewDocument()==false) {enabled=false;return true;}
    return false;
  }  
};

class FunctorAfterNewDocument
{
public:  
  bool operator ()(SRef<PluginGeneralAppSide> &plugin) const
  {
    plugin->GetPlugin().AfterNewDocument();
    return false;
  }
};

BOOL CObjektiv2Doc::OnNewDocument()
{
  /*  TCOLMODEL mdl1,mdl2,mdl3;
  ::cmCreateColorModel("ARGB8888",&mdl1);
  ::cmCreateColorModel("I8",&mdl2);
  ::cmCreateDiffModel(&mdl1,&mdl2,NULL,&mdl3);*/
  bool canClose=true;
  theApp.ForEachPlugin(FunctorOnNewDocument(canClose));
  if (!canClose) return FALSE;
  if (LodData)
  {
    if (LodData->Dirty())
    {	  
      int id=WMessageBox::Messagef(NULL,MB_YESNOCANCEL|MB_ICONQUESTION,::AfxGetAppName(),WString(IDS_SAVEQUESTION),GetPathName());
      switch (id)
      {
      case IDCANCEL: return false;
      case IDYES:if (AskSave()==FALSE) return FALSE;break;
      case IDNO: break;
      }
    }
    if (!orgname.IsNull() && orgname!=(LPCTSTR)GetPathName())
    {
      if (WMessageBox::Messagef(NULL,MB_YESNO|MB_ICONQUESTION,::AfxGetAppName(),WString(IDS_SAVESERVERQUESTION),orgname)==IDYES)
      {
        SetPathName(orgname);
        if (AskSave()==FALSE) return FALSE;
      }
    }
    if (_isCheckedOut)
    {
      int id=AfxMessageBox(IDS_CHECKINSUGGEST,MB_YESNOCANCEL|MB_ICONQUESTION);
      switch (id)
      {
      case IDCANCEL: return false;
      case IDYES: this->OnSourcecontrolSaveandcheckin();break;
      case IDNO: break;
      }
    }
  }
  //Document Inicialization

  orgname="";
  filename="";
  if (!CDocument::OnNewDocument())
    return FALSE;

  delete LodData;
  //Delete old document
  LodData=new LODObject();
  //Create new document
  UpdateAll();
  comint.SetObject(LodData);
  prevanim=-1; 
  curanim=-1;
  frozenanim=-1;
  frame->RestartAutosaveTimer();
  LodData->ClearDirty();
  frame->m_wndAnimListBar.OnDocumentReset();  
  frame->m_wndSelBar.selgrp.Reset();
  JednotkovaMatice(ghost);
  showghost=false;
  selghost=false;
  automapsel=false;
  numSections=-1;
  comint.sectchanged=true;
  lastP3Dsize=0;
  frame->m_wndMatLib.SetModelPath(Pathname(PathNull));
  _isSccControled=false;
  _isCheckedOut=false;
  _proxyCache.Reset();
  theApp.ForEachPlugin(FunctorAfterNewDocument());
  return TRUE;
}










/////////////////////////////////////////////////////////////////////////////
// CObjektiv2Doc serialization

void CObjektiv2Doc::Serialize(CArchive& ar)
{
  if (ar.IsStoring())
  {
    // TODO: add storing code here
  }
  else
  {
    // TODO: add loading code here
  }
}










/////////////////////////////////////////////////////////////////////////////
// CObjektiv2Doc diagnostics

#ifdef _DEBUG
void CObjektiv2Doc::AssertValid() const
{
  CDocument::AssertValid();
}










void CObjektiv2Doc::Dump(CDumpContext& dc) const
{
  CDocument::Dump(dc);
}










#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CObjektiv2Doc commands


#include "ImportSDADlg.h"
#include "DlgGetLatestVer.h"

class FunctorLoadDocument
{
  const char *name;
public:
  FunctorLoadDocument(const char *n):name(n) {}
  bool operator ()(SRef<PluginGeneralAppSide> &plugin) const
  {
    plugin->GetPlugin().AfterLoadDocument(name);
    return false;
  }
};

bool CObjektiv2Doc::LoadDocument(Pathname &filename)
{
  if (frame->initphase) 
  {
    theApp.startuptoload=(LPCTSTR)filename;
    return true;
  }
  lastP3Dsize=GetFileSizeByFilename(filename);
  ROCheckResult result;
  if (config->GetBool(IDS_CFGDISABLEROWARNING))
    result=ROCHK_FileRO;
  else
    result=theApp.rocheck.TestFileRO(filename,ROCHF_GetLatestVersion);
  if (result==ROCHK_GetLatestVersion)
    theApp._scc->GetLatestVersion(filename);
  if (_stricmp(filename.GetExtension(),".SDA")==0)
  {
#ifdef FULLVER
    static CImportSDADlg dlg;
    dlg.obj=LodData->Active();
    dlg.filename=filename;
    if (dlg.DoModal()==IDCANCEL) return false;	
#else
    AfxMessageBox("SDA Import is not supported in Light version!");
#endif
  }
  else
  {
    SetCursor(LoadCursor(NULL,IDC_WAIT));
    ProgressBar<int> progress(1);
    progress.AdvanceNext(1);
    progress.ReportState(IDS_LOADINGFILE,filename.GetTitle());
    frame->m_wndTexList.UpdateModelTextures(LodData->Active(),false,false);
    if (LodData->Load(filename,LoadOxySetup,NULL))
    {
      OnNewDocument();
      AfxMessageBox(IDS_LOADUNSUCCESFUL);
      return false;
    }
    else
      UpdateAllViews(NULL);
    /*	CString local=config->GetString(IDS_CFGLOCALSPACE);*/
    this->filename=filename;
    /*if (local!="")
    {
    orgname=this->filename;
    this->filename.SetDrive("");
    this->filename.SetDirectory(local);
    }*/
    SetPathName(this->filename, true);
    Pathname redo=filename;
    redo.SetExtension(REDOFILE);
    DeleteFile(redo);
    theApp.ForEachPlugin(FunctorLoadDocument(this->filename));    
  }
  comint.SetObject(LodData);
  UpdateAll();
  frame->m_wndMatLib.SetModelPath(filename);
  UpdateFileSSStatus();
  CloseProgressWindow();
  frame->UpdateWindow();
  if (_isSccControled && result!=ROCHK_FileRO) 
  {
    GLVStop=true;
    POSITION pos=GetFirstViewPosition();
    CView *ww=GetNextView(pos);
    DlgGetLatestVer *dlgGet=new DlgGetLatestVer(AfxGetMainWnd(),ID_SOURCECONTROL_GETLATESTVERSION);
    dlgGet->Create(dlgGet->IDD,ww);
  }
  const char *binprop = LodData->Active()->GetNamedProp("-binarized");
  if (binprop && binprop[0]=='1')
      AfxMessageBox(IDS_BINARIZEDWARNING,MB_ICONINFORMATION);
  return true;
}










void CObjektiv2Doc::SetDocumentName(Pathname &filename)
{
  this->filename=filename;
  SetPathName(filename, false);
}










void CObjektiv2Doc::OnFileSaveAs() 
{    
  AskSaveAs();
}








RString GetCurrentDrive()
{
  // Retrieve the current directory
  RString currDir;
  int needsz = GetCurrentDirectory(0,0) + 1;
  GetCurrentDirectory(needsz, currDir.CreateBuffer(needsz));

  // Create the current drive (by finding the first '\' char)
  int firstBackslash = currDir.Find('\\');
  RString currDrive;
  if (firstBackslash >= 0)
  {
    currDrive = currDir.Substring(0, firstBackslash + 1);
  }
  else
  {
    currDrive = currDir;
  }
  return currDrive;
}

void CObjektiv2Doc::OnFileOpen() 
{
  CString defext;defext.LoadString(IDS_FILEDEFEXT);
  CString filters;filters.LoadString(IDS_FILEOPENFILTERS);
  CFileDialogEx fdlg(TRUE,defext,NULL,OFN_CREATEPROMPT|OFN_LONGNAMES,
    filters,NULL);
  config->FillOpenDialogByPath(IDS_CFGOPENFOLDER,fdlg.m_ofn,"");
  if (fdlg.DoModal()!=IDOK) return;
  config->SaveCurrPath(IDS_CFGOPENFOLDER,fdlg.GetPathName());
  if (OnNewDocument()==false) return;
  LoadDocument(Pathname(fdlg.GetPathName()));  
}










void CObjektiv2Doc::OnFileSave() 
{
  AskSave();
}










BOOL CObjektiv2Doc::OnOpenDocument(LPCTSTR lpszPathName) 
{
  if (OnNewDocument()==false) return FALSE;
  LoadDocument(Pathname(lpszPathName));	
  return TRUE;
}










void CObjektiv2Doc::UpdateAllToolbars(int updatelevel)
{
  if (updatelevel<1)frame->m_wndLodList.UpdateListOfLods(LodData);
  if (updatelevel<2)
  {
    frame->m_wndSelBar.UpdateListOfSel(LodData);
    frame->m_wndNamedPropBar.LoadProperties(LodData);
    frame->m_wndAnimListBar.UpdateListOfAnims(LodData->Active(),LodData->ActiveLevel());
    frame->m_wndAnimPropsToolbar.UpdateData(LodData->Active(),LodData->ActiveLevel());
    UpdateMatLib();
    frame->_dlguveditor.UpdateFromDocument(this);
  }
  if (updatelevel<3)
  {
    int level=LodData->ActiveLevel();
    float resolution=LodData->Resolution(level);
    if (ISMASS(resolution)) frame->m_wndMassBar.Update(LodData->Active());
    else frame->m_wndMassBar.Update(NULL);
  }
}


void CObjektiv2Doc::UpdateMatLib()
{
  frame->m_wndTexList.UpdateModelTextures(LodData->Active());
  frame->m_wndMatLib.UpdateCurModel(LodData);
  frame->m_wndMatLib.UpdateCurLOD(LodData->Active());
}



void CObjektiv2Doc::UpdateAllViews( CView* pSender,LPARAM lHint, CObject* pHint )
{
  AttachLocalAxis();
  LoadEdges();
  UpdateStatus();
  frame->m_wndAnimPropsToolbar.SetTimeFromAnimation();
  CDocument::UpdateAllViews(pSender,lHint,pHint);
}



void CObjektiv2Doc::UpdateAll()
{
  UpdateAllToolbars();
  LoadEdges();
  UpdateAllViews(NULL);
  UpdateStatus();
}










void CObjektiv2Doc::OnEditUndo() 
{
  comint.DoUndo();
  UpdateAll();
}










void CObjektiv2Doc::OnUpdateEditUndo(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(comint.CanUndo());	
}










void CObjektiv2Doc::OnEditRedo() 
{
  comint.DoRedo();
  UpdateAll();
}










void CObjektiv2Doc::OnUpdateEditRedo(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(comint.CanRedo());	  	
}










void CObjektiv2Doc::OnViewRefresh() 
{
  LoadEdges();
  UpdateAllViews(NULL);	
  UpdateAllToolbars(1);
  CObjektiv2View::GetCurrentView()->SetFocus();
  OnUpdateModel();
}










LPHVECTOR CObjektiv2Doc::GetPointFromFace(ObjectData *od, FaceT& face, int index,LPHVECTOR vx)
{
  int ix=face.GetPoint(index);
  PosT& pt=od->Point(ix);	  
  static HVECTOR v;
  if (vx==NULL) vx=v;
  if (seltransform && od->PointSelected(ix))   
    SelectionTransform(ix, pt,vx,od->GetSelection()->PointWeight(ix));
  else
  {
    vx[XVAL]=pt[0];
    vx[YVAL]=pt[1];
    vx[ZVAL]=pt[2];
    vx[WVAL]=1.0f;
  }
  return vx;
}










CPen normpen(PS_SOLID,1,RGB(0,0,255));
CPen normlockedpen(PS_SOLID,1,RGB(220,0,255));
CPen selpen(PS_SOLID,1,RGB(255,0,0));
CPen selsharp(PS_SOLID,2,RGB(192,0,64));
CPen lockpen(PS_SOLID,1,RGB(0,255,0));
CPen drawpen(PS_SOLID,1,RGB(0,0,0));
CBrush selbrush(RGB(255,0,0));
static CPen zosa(PS_SOLID,1,RGB(0,0,150));
static CPen xosa(PS_SOLID,1,RGB(150,0,0));
static CPen yosa(PS_SOLID,1,RGB(0,150,0));
static CPen zosasel(PS_SOLID,2,RGB(0,0,255));
static CPen yosasel(PS_SOLID,2,RGB(255,0,0));
static CPen xosasel(PS_SOLID,2,RGB(0,255,0));

static CEdges drwedges;

void  CObjektiv2Doc::DrawGismo(C3DC &ct)
{
  HMATRIX mm;
  CopyMatice(mm,ct.GetMatrix());
  HMATRIX out;
  bool sel=gismo_mode==gismo_selected;
  if ((sel || localAxisMode) && seltransform) 
  {
    SoucinMatic(gismo,seltr,out);
    SoucinMatic(out,mm,out);
  }
  else
    SoucinMatic(gismo,mm,out);
  ct.SetMatrix(out);  
  ct.SelectObject(sel?&xosasel:&xosa);
  ct.MoveTo3D(-1,-0.1f,0);ct.LineTo3D(-1,0.1f,0);
  ct.MoveTo3D(-1,0,-0.1f);ct.LineTo3D(-1,0,0.1f);
  ct.MoveTo3D(-1,0,0);ct.LineTo3D(1,0,0);
  ct.MoveTo3D(0.9f,-0.1,0);ct.LineTo3D(1,0,0);ct.LineTo3D(0.9,0.1,0);
  ct.SelectObject(sel?&yosasel:&yosa);
  ct.MoveTo3D(-0.1f,-1,0);ct.LineTo3D(0.1f,-1,0);
  ct.MoveTo3D(0,-1,-0.1f);ct.LineTo3D(0,-1,0.1f);
  ct.MoveTo3D(0,-1,0);ct.LineTo3D(0,1,0);
  ct.MoveTo3D(-0.1,0.9f,0);ct.LineTo3D(0,1,0);ct.LineTo3D(0.1,0.9,0);
  ct.SelectObject(sel?&zosasel:&zosa);
  ct.MoveTo3D(-0.1f,0,-1);ct.LineTo3D(0.1f,0,-1);
  ct.MoveTo3D(0,-0.1f,-1);ct.LineTo3D(0,0.1f,-1);
  ct.MoveTo3D(0,0,-1);ct.LineTo3D(0,0,1);
  ct.MoveTo3D(-0.1,0,0.9f);ct.LineTo3D(0,0,1);ct.LineTo3D(0.1,0,0.9f);
  ct.MoveTo3D(0,-0.1,0.9f);ct.LineTo3D(0,0,1);ct.LineTo3D(0,0.1,0.9f);
  ct.LineTo3D(0.1,0,0.9f);ct.LineTo3D(0,-0.1,0.9f);
  ct.LineTo3D(-0.1,0,0.9f);ct.LineTo3D(0,0.1,0.9f);
  if (localAxisMode)
  {
    int ang;
    for (int q=0;q<3;q++)
      for (ang=0;ang<=40;ang++)
      {
        float anf=FPI*ang*2.0f/40.0f;
        float x=(float)cos(anf)*0.5;
        float y=(float)sin(anf)*0.5;
        
        if (q==2)
        {
          ct.SelectObject(sel?&zosasel:&zosa);
          if (ang==0) ct.MoveTo3D(x,y,0);else ct.LineTo3D(x,y,0);
        }
        else if (q==1)
        {
          ct.SelectObject(sel?&yosasel:&yosa);
          if (ang==0) ct.MoveTo3D(x,0,y);else ct.LineTo3D(x,0,y);
        }
        else
        {
          ct.SelectObject(sel?&xosasel:&xosa);
          if (ang==0) ct.MoveTo3D(0,x,y);else ct.LineTo3D(0,x,y);
        }
      }

  }
  else if (CGizmoMapDlg::gizmotype<2)
  {
    for (int q=-1;q<2;q++)
    {
      for (int ang=0;ang<=40;ang++)
      {
        float anf=FPI*ang*2.0f/40.0f;
        float x=(float)cos(anf);
        float y=(float)sin(anf);
        if (ang==0) ct.MoveTo3D(x,y,q);else ct.LineTo3D(x,y,q);
      }
    }
  }
  else
  {
    int ang;
    for (int q=0;q<3;q++)
      for (ang=0;ang<=40;ang++)
      {
        float anf=FPI*ang*2.0f/40.0f;
        float x=(float)cos(anf);
        float y=(float)sin(anf);
        if (q==2)
          if (ang==0) ct.MoveTo3D(x,y,0);else ct.LineTo3D(x,y,0);
        else if (q==1)
          if (ang==0) ct.MoveTo3D(x,0,y);else ct.LineTo3D(x,0,y);
        else
          if (ang==0) ct.MoveTo3D(0,x,y);else ct.LineTo3D(0,x,y);
      }
  }
  ct.SetMatrix(mm);
}










static float GetNormalsLen(C3DC &ct)
{
  HMATRIX mc;
  InverzeMatice(ct.GetMatrix(),mc);
  HVECTOR v1=
  {107,107,0,1.0f};
  HVECTOR p1;
  HVECTOR v2=
  {100,100,0,1.0f};
  HVECTOR p2;
  TransformVector(mc,v1,p1);
  TransformVector(mc,v2,p2);
  CorrectVector(p1);
  CorrectVector(p2);
  RozdilVektoru(p1,p2);
  return ModulVektoru(p1);
}










/*

void CObjektiv2Doc::DrawToViewGhost(C3DC &ct)
{  
float lev=LodData->Resolution(LodData->ActiveLevel());
CPen *drawsel=&drawpen;
ct.SelectObject(drawsel);
ObjectData *odata=LodData->Active();
const Selection *hide=odata->GetHidden();
const Selection *lock=odata->GetLocked();
const Selection *sel=odata->GetSelection();
bool select=!sel->IsEmpty();
drawct.Attach(ct);
LPHMATRIX mx=drawct.GetMatrix();
if (seltransform && selghost) 
{
SoucinMatic(ghost,seltr,mx);
SoucinMatic(mx,ct.GetMatrix(),mx);
}
else
SoucinMatic(ghost,ct.GetMatrix(),mx);
int pcount=odata->NPoints();
int ncount=odata->NNormals();
drawct.AllocVList(pcount);  
drwedges.SetVertices(pcount,1);    
for (int i=0;i<pcount;i++)
{
PosT &pos=odata->Point(i);
if (seltransform && sel->PointSelected(i))
{
HVECTOR vx;	
TransformVector(seltr,mxVector3(pos[0],pos[1],pos[2]),vx);
float f=sel->PointWeight(i);
vx[XVAL]=pos[0]+(vx[XVAL]-pos[0])*f;
vx[YVAL]=pos[1]+(vx[YVAL]-pos[1])*f;
vx[ZVAL]=pos[2]+(vx[ZVAL]-pos[2])*f;
drawct.SetVertex(i,vx,NULL,NULL);
}
else
drawct.SetVertex(i,mxVector3(pos[0],pos[1],pos[2]),NULL,NULL);
drawct.SetPointStyle(i,C3DCPT_CROSS,hide->PointSelected(i)?0:2);
}  
int fcount=odata->NFaces();  
for (int b=0;b<2;b++)
for (int i=0;i<fcount;i++) if (!sel->FaceSelected(i)==(b==1))
if (!hide->FaceSelected(i))
{		
CPen *plist[4];	
FaceT &fc=odata->Face(i);	
if (fc.N()>2 && !drawct.GetFaceVisible(fc.GetPoint(0),fc.GetPoint(1),fc.GetPoint(2))) continue;
bool fcsel=sel->FaceSelected(i);
if (fcsel)
if (lock->FaceSelected(i))
{
plist[0]=plist[1]=plist[2]=plist[3]=&lockpen;
}
else
{
drawsel=&selpen;		
for (int i=0;i<fc.N();i++) 
{
int j=i+1;if (j>=fc.N()) j=0;
if (edges.IsEdge(fc.vs[i].point,fc.GetPoint(j)))
plist[i]=&selsharp;
else
plist[i]=&selpen;
}
}
else
plist[0]=plist[1]=plist[2]=plist[3]=coldlg.GetColorizePen(odata->Face(i));
for (int k=0;k<fc.N();k++)
{
int j=k+1;if (j>=fc.N()) j=0;
int a=fc.GetPoint(k);
int b=fc.GetPoint(j);
if (drwedges.IsEdge(a,b)) 
continue;
drawct.SelectObject(plist[k]);
drawct.Line3D(a,b);
drwedges.SetEdge(a,b);
}
}
drawct.SelectObject(&drawpen);
drawct.DrawAllPoints(pcount);
drawct.Detach();
}



*/

extern HMATRIX p3d2g3d;

bool CObjektiv2Doc::DrawToViewObject(ObjectData &obj, C3DC &ct, bool calcmas)
{
  int proxyOffs=strlen(ObjToolProxy::ProxyPrefix);
  float normlen;  
  bool normals=config->GetBool(IDS_CFGVIEWNORMALS);
  if (normals) 
    normlen=GetNormalsLen(ct);
  double maxmass;
  if (calcmas)  maxmass=obj.GetMaxMass()+1;
  CPen *drawsel=&drawpen;
  ct.SelectObject(drawsel);
  ObjectData *odata=&obj;
  const Selection *hide=odata->GetHidden();
  const Selection *lock=odata->GetLocked();
  const Selection *sel=odata->GetSelection();
  const Selection *proxies=odata->GetTool<ObjToolProxy>().GetShowProxySelection();
  bool select=!sel->IsEmpty();
  
  drawct.Attach(ct);
  if (calcmas)
  {
    VecT t1;
    drawct.SelectStockObject(BLACK_BRUSH);
    drawct.SelectObject(&normpen);
    t1=odata->GetMassCentre(false);
    drawct.Point3D(mxVector3(t1[0],t1[1],t1[2]),C3DCPT_XCROSS,6);
    if (select)
    {
      t1=odata->GetMassCentre(true);
      drawct.Point3D(mxVector3(t1[0],t1[1],t1[2]),C3DCPT_CIRCLE,4);
    }
  }
  int pcount=odata->NPoints();
  int ncount=odata->NNormals();
  drawct.AllocVList(pcount);  
  drwedges.SetVertices(pcount,1);  
  for (int i=0;i<pcount;i++)
  {
    PosT &pos=odata->Point(i);
    if (seltransform && sel->PointSelected(i))
    {
      HVECTOR vx;	
      SelectionTransform(i, pos, vx,sel->PointWeight(i));
      drawct.SetVertex(i,vx,NULL,NULL);
    }
    else
      drawct.SetVertex(i,mxVector3(pos[0],pos[1],pos[2]),NULL,NULL);
    drawct.SetPointStyle(i,C3DCPT_CROSS,hide->PointSelected(i) || proxies!=0 && proxies->PointSelected(i)?0:2);
  }  
  int fcount=odata->NFaces();    
  if (automapsel)
  {
    CBrush brsh, *old;
    brsh.CreateHatchBrush(HS_DIAGCROSS,RGB(255,128,0));
    drawct.SetBkMode(TRANSPARENT);
    old=drawct.GetCurrentBrush();
    drawct.SelectObject(&brsh);
    drawct.SelectStockObject(NULL_PEN);
    combine_saved->SetOwner(odata); //Validate selection
    for (int i=0;i<fcount;i++) if (!hide->FaceSelected(i) && !combine_saved->FaceSelected(i))
    {
      POINT pt[4];
      FaceT fc(odata,i);	
      if (fc.N()>2 && !drawct.GetFaceVisible(fc.GetPoint(0),fc.GetPoint(1),fc.GetPoint(2))) continue;
      for (int k=0;k<fc.N();k++)
      {
        int a=fc.GetPoint(k);
        if (drawct.GetScreenCoords(a,pt[k])==false) goto skip;
      }
      drawct.Polygon(pt,fc.N());
skip:;
    }
    drawct.SelectObject(old);
  }

  CEdges edges;
  obj.LoadSharpEdges(edges);
  
  for (int b=0;b<2;b++)
    for (int i=0;i<fcount;i++) if (!sel->FaceSelected(i)==(b==1))
      if (proxies && proxies->FaceSelected(i))
      {
        if (!hide->FaceSelected(i))
        {
          FaceT fc(odata,i);	
          const NamedSelection *proxy=odata->GetTool<ObjToolProxy>().GetProxySelection(fc);
          if (proxy)
          {
            Matrix4 trans;
            odata->GetTool<ObjToolProxy>().GetProxyTransform(fc,trans);
            ObjectData *obj=_proxyCache.GetBestProxyObject(proxy->Name()+proxyOffs,GetProxyLevel());
            if (obj) DrawProxyToView(obj,trans,ct,sel->FaceSelected(i));
            else const_cast<Selection *>(proxies)->FaceSelect(i,false);
          }
        }
      }
      else if (!hide->FaceSelected(i))
      {	
        CPen *plist[4];	
        FaceT fc(odata,i);	
        if (fc.N()>2 && !drawct.GetFaceVisible(fc.GetPoint(0),fc.GetPoint(1),fc.GetPoint(2))) continue;
        bool fcsel=sel->FaceSelected(i);
        if (fcsel)
          if (lock->FaceSelected(i))
          {
            plist[0]=plist[1]=plist[2]=plist[3]=&lockpen;
          }
          else
          {
            drawsel=&selpen;		
            for (int i=0;i<fc.N();i++) 
            {
              int j=i+1;if (j>=fc.N()) j=0;
              if (edges.IsEdge(fc.GetPoint(i),fc.GetPoint(j)))
                plist[i]=&selsharp;
              else
                plist[i]=&selpen;
            }
          }
        else
          plist[0]=plist[1]=plist[2]=plist[3]=coldlg.GetColorizePen(FaceT(odata,i));
        for (int k=0;k<fc.N();k++)
        {
          int j=k+1;if (j>=fc.N()) j=0;
          int a=fc.GetPoint(k);
          int b=fc.GetPoint(j);
          if (normals && odata->PointSelected(a) && odata->NNormals())
          {
            PosT &pos=odata->Point(a);
            if (pos.flags & POINT_SPECIAL_LOCKNORMAL)
              drawct.SelectObject(&normlockedpen);		
            else
              drawct.SelectObject(&normpen);		
            Vector3P &nor=odata->Normal(fc.GetNormal(k));
            HVECTOR v1,v2;
            CopyVektor(v1,mxVector3(pos[0],pos[1],pos[2]));
            CopyVektor(v2,mxVector3(-nor[0],-nor[1],-nor[2]));
            drawct.MoveTo3D(v1);
            float n=0.03f/drawct.GetOldPos()[3];
            if (drawct.GetMatrix()[2][3]==0.0f) n=normlen;
            NasobVektor(v2,n);
            SoucetVektoru(v2,v1);
            drawct.LineTo3D(v2);
          }
          if (drwedges.IsEdge(a,b)) 
            continue;
          drawct.SelectObject(plist[k]);
          drawct.Line3D(a,b);
          drwedges.SetEdge(a,b);
        }
      }
      drawct.SelectObject(&drawpen);
      drawct.DrawAllPoints(pcount);
      if (select)
      {
        if (calcmas) drawct.SelectStockObject(GRAY_BRUSH);
        else drawct.SelectStockObject(HOLLOW_BRUSH);
        drawct.SelectObject(&selpen);
        {
          //fcount=sel->NPoints();
          for (int i=0;i<pcount;i++) if (sel->PointSelected(i))
          {
            int siz=calcmas?(int)(odata->GetPointMass(i)/maxmass*10.0f)+1:3;
            if (lock->PointSelected(i)) 
            {drawct.SelectObject(&lockpen);}
            else 
            {drawct.SelectObject(CColorizeDlg::WeightColor(sel->PointWeight(i)));}
            PosT& pt=odata->Point(i);
            if (seltransform && odata->PointSelected(i) && !lock->PointSelected(i))
            {
              HVECTOR vx;
              SelectionTransform(i,pt,vx,1.0f);
              drawct.Point3D(vx,C3DCPT_RECTANGLE,siz);
            }
            else
              drawct.Point3D(mxVector3(pt[0],pt[1],pt[2]),C3DCPT_RECTANGLE,siz);
          }
        }
      }
      drawct.Detach();


  return true;
}

bool CObjektiv2Doc::DrawProxyToView(ObjectData *proxy, const Matrix4 &proxyMatrix, C3DC &ct, bool selected)
{
  C3DC2 drawct;
  CEdges drwedges;
  
  drawct.Attach(ct);

  int pcount=proxy->NPoints();
  int ncount=proxy->NNormals();
  drawct.AllocVList(pcount);  
  drwedges.SetVertices(pcount,1);  

  Matrix4 finMatrix;

  if (selected && seltransform)
  { 
    Matrix4 seltrans(seltr[0][0],seltr[1][0],seltr[2][0],seltr[3][0],
      seltr[0][1],seltr[1][1],seltr[2][1],seltr[3][1],
      seltr[0][2],seltr[1][2],seltr[2][2],seltr[3][2]);
      finMatrix=seltrans*proxyMatrix;
  }
  else finMatrix=proxyMatrix;

  for (int i=0;i<pcount;i++)
  {
    PosT &pos=proxy->Point(i);
    VecT vx;
    vx.SetFastTransform(finMatrix,pos);
    drawct.SetVertex(i,mxVector3(vx[0],vx[1],vx[2]),0,0);
      drawct.SetPointStyle(i,C3DCPT_CROSS,2);
  }  
  int fcount=proxy->NFaces();    

    for (int i=0;i<fcount;i++)
    {
      FaceT fc(proxy,i);	
      if (fc.N()>2 && !drawct.GetFaceVisible(fc.GetPoint(0),fc.GetPoint(1),fc.GetPoint(2))) continue;
      for (int k=0;k<fc.N();k++)
      {
        int j=k+1;if (j>=fc.N()) j=0;
        int a=fc.GetPoint(k);
        int b=fc.GetPoint(j);
        if (drwedges.IsEdge(a,b)) 
          continue;
        drawct.SelectObject(selected?&selpen:&drawpen);
        drawct.Line3D(a,b);
        drwedges.SetEdge(a,b);
      }
    }
    drawct.SelectObject(&drawpen);
    //    drawct.DrawAllPoints(pcount);
    drawct.Detach();


    return true;
}

bool CObjektiv2Doc::DrawToView(C3DC &ct, DrawMode mode, int frame)
{  
  _proxyCache.Init(config->GetString(IDS_CFGPATHFORTEX));
  float lev=LodData->Resolution(LodData->ActiveLevel());


  HVECTOR v,c;
  if (mode==Solid || mode==SolidWires)
  {
    HMATRIX mm2,im;
    CopyMatice(mm2,ct.GetMatrix());
    //      SoucinMatic(p3d2g3d,mm,mm2);
    InverzeMatice(mm2,im);
    TransformVectorNoTranslate(im,mxVector3(0,0,1),v);
    NormalizeVector(v);
    TransformVectorNoTranslate(im,mxVector3(0,0,1),c);
    NormalizeVector(c);
  }
  switch (mode)
  {
  case WireFrame: DrawToViewObject(*LodData->Active(),ct,ISMASS(LodData->Resolution(LodData->ActiveLevel())));break;
  case Solid: DrawToViewSolidObject(*LodData->Active(),ct,false,v,c);break;
  case SolidWires: DrawToViewSolidObject(*LodData->Active(),ct,true,v,c);break;
  };
  if (gismo_mode!=gismo_hide) DrawGismo(ct);
  return true;
}


void CObjektiv2Doc::DrawToViewSolidObject(ObjectData &obj,C3DC &ct, bool wires, HVECTOR ldir, HVECTOR cdir)
{
  ObjectData *odata=&obj;
  const Selection *sel=odata->GetSelection();
  const Selection *hide=odata->GetHidden();
  const Selection *lock=odata->GetLocked();
  const Selection *proxies=odata->GetTool<ObjToolProxy>().GetShowProxySelection();  
  AutoArray<int,MemAllocLocal<int, 100> >proxyList;
  bool select=!sel->IsEmpty();
  int fcount=odata->NFaces();  
  int pcount=odata->NPoints();  
  if (fcount==0) return;
  CFaceSortList fci;
  fci.AttachDC(&ct);
  fci.SetLightDirection(ldir,cdir);
  for (int i=0;i<fcount;i++) 
    if (proxies && proxies->FaceSelected(i))
      if (!hide->FaceSelected(i))
      {
        proxyList.Append()=i;
      }
      else;
    else if (!hide->FaceSelected(i))
    {
      FaceT fc(odata,i);
      HMATRIX mm;
      HMATRIX norms;
      if (odata->NNormals()==0) RecalcNormals2(odata,true);
      for(int j=0;j<fc.N();j++)
      {
        GetPointFromFace(odata,fc,j,mm[j]);
        VecT pp;
        int norm=fc.GetNormal(j);
        if (odata->NNormals()>norm && norm>=0)
          pp=odata->Normal(fc.GetNormal(j));
        else
          pp=VecT(0,0,1);
        CopyVektor(norms[j],mxVector3(pp[0],pp[1],pp[2]));
      }
      if (sel->FaceSelected(i))
        fci.AddFace(fc.N(),mm,RGB(255,0,0),norms);
      else
        fci.AddFace(fc.N(),mm,coldlg.GetColorizeColor(fc),norms);
    }
    int proxyOffs=strlen(ObjToolProxy::ProxyPrefix);
    for (int i=0;i<proxyList.Size();i++)
    {

      FaceT fc(odata,proxyList[i]);	
      const NamedSelection *proxy=odata->GetTool<ObjToolProxy>().GetProxySelection(fc);
      if (proxy)
      {
        Matrix4 trans;
        odata->GetTool<ObjToolProxy>().GetProxyTransform(fc,trans);
        ObjectData *obj=_proxyCache.GetBestProxyObject(proxy->Name()+proxyOffs,GetProxyLevel());
        bool selected=sel->FaceSelected(fc.GetFaceIndex());
        if (obj) 
        {
          Matrix4 finMatrix;
          Matrix4 normMatrix;

          if (selected && seltransform)
          { 
            Matrix4 seltrans(seltr[0][0],seltr[1][0],seltr[2][0],seltr[3][0],
              seltr[0][1],seltr[1][1],seltr[2][1],seltr[3][1],
              seltr[0][2],seltr[1][2],seltr[2][2],seltr[3][2]);
            finMatrix=seltrans*trans;
          }
          else finMatrix=trans;
          normMatrix=finMatrix;
          normMatrix(0,3)=normMatrix(1,3)=normMatrix(2,3)=0;

          for (int i=0;i<obj->NFaces();i++) 
          {
            FaceT fc(obj,i);
            if (obj->NNormals()==0) obj->RecalcNormals();

            HMATRIX mm;
            HMATRIX norms;

            for (int j=0;j<fc.N();j++)
            {
              PosT &ps=obj->Point(fc.GetPoint(j));
              VecT pvc;pvc.SetFastTransform(finMatrix,ps);
              VecT &n=obj->Normal(fc.GetNormal(j));
              VecT nvc;nvc.SetFastTransform(normMatrix,n);
              mm[j][0]=pvc[0];norms[j][0]=nvc[0];
              mm[j][1]=pvc[1];norms[j][1]=nvc[1];
              mm[j][2]=pvc[2];norms[j][2]=nvc[2];
              mm[j][3]=norms[j][3]=1.0f;
            }
            if (selected)
              fci.AddFace(fc.N(),mm,RGB(255,0,0),norms);
            else
              fci.AddFace(fc.N(),mm,coldlg.GetColorizeColor(fc),norms);
          }
        }
        else const_cast<Selection *>(proxies)->FaceSelect(proxyList[i],false);
      }
    }
    ct.SelectObject(&drawpen);
    if (fci.Prepare())
    {
      fci.DrawFaces(wires);
    }
    if (select)
    {
      ct.SelectStockObject(BLACK_BRUSH);
      ct.SelectObject(&selpen);
      {
        //fcount=sel->NPoints();
        for (int i=0;i<pcount;i++) if (sel->PointSelected(i))
        {
          int siz=3;
          if (lock->PointSelected(i)) 
          {ct.SelectObject(&lockpen);}
          else 
          {ct.SelectObject(CColorizeDlg::WeightColor(sel->PointWeight(i)));}
          PosT& pt=odata->Point(i);
          if (seltransform && odata->PointSelected(i))
          {
            HVECTOR vx;
            SelectionTransform(i,pt,vx,1.0f);
            ct.Point3D(vx,C3DCPT_RECTANGLE,siz);
          }
          else
            ct.Point3D(mxVector3(pt[0],pt[1],pt[2]),C3DCPT_RECTANGLE,siz);
        }
      }
    }
}










void CObjektiv2Doc::DrawPointsToView(C3DC &ct)
{
  ObjectData *odata=LodData->Active();
  bool normals=config->GetBool(IDS_CFGVIEWNORMALS);
  const Selection *sel=odata->GetSelection();
  const Selection *hide=odata->GetHidden();
  const Selection *lock=odata->GetLocked();
  ct.SelectObject(&drawpen);
  int fcount=odata->NPoints();  
  bool drawsel=false;
  for (int i=0;i<fcount;i++) if (!hide->PointSelected(i))
  {
    PosT& pt=odata->Point(i);
    ct.Point3D(mxVector3(pt[0],pt[1],pt[2]),C3DCPT_CROSS,2);
  }
  if (!sel->IsEmpty())
  {
    ct.SelectStockObject(HOLLOW_BRUSH);
    ct.SelectObject(&selpen);
    {
      //fcount=sel->NPoints();
      for (int i=0;i<fcount;i++) if (sel->PointSelected(i))
      {
        if (lock->PointSelected(i)) 
        {ct.SelectObject(&lockpen);ct.SelectStockObject(HOLLOW_BRUSH);}
        else 
        {ct.SelectObject(&selpen);ct.SelectStockObject(HOLLOW_BRUSH);}
        PosT& pt=odata->Point(i);
        if (seltransform && odata->PointSelected(i))
        {
          HVECTOR vx;
          SelectionTransform(i, pt, vx,1.0f);
          ct.Point3D(vx,C3DCPT_RECTANGLE,3);
        }
        else
          ct.Point3D(mxVector3(pt[0],pt[1],pt[2]),C3DCPT_RECTANGLE,3);
      }
    }
  }
}










void CObjektiv2Doc::OnUpdateAllViews() 
{
  UpdateAllViews(NULL);
  CObjektiv2View::GetCurrentView()->SetFocus();
  LoadEdges();
}










#define CISELOP_HIDDEN 0
#define CISELOP_LOCKED 1
#define CISELOP_SELALL 2
#define CISELOP_SELLOCKED 3
#define CISELOP_INVERT 4
#define CISELOP_ONESIDE 5
#define CISELOP_UNSELECTTRIANGLES 6
#define CISELOP_DESEL_VERTICES 7
#define CISELOP_DESEL_FACES 8
#define CISELOP_GROW 9
#define CISELOP_SHRINK 10

class CISelOperation:public CICommand
{
  int mode;
  bool set;
public:
  CISelOperation(istream &str) 
  {datard(str,mode);datard(str,set);}
  CISelOperation(int mode, bool set):mode(mode),set(set) 
  {marksave=false;}
  int Execute(LODObject *obj)
  {
    ObjectData *odata=obj->Active();
    if (odata==NULL) return -1;
    switch(mode)
    {
    case CISELOP_HIDDEN:
      {
        const Selection *hid=odata->GetHidden();
        for (int i=0;i<odata->NFaces();i++) if (!odata->FaceSelected(i) && !hid->FaceSelected(i))
        {
          FaceT fc(odata,i);
          for (int j=0;j<fc.N();j++) 	odata->PointSelect(fc.GetPoint(j),false);
        }
        if (set) odata->HideSelection();else odata->UnhideSelection();break;
      }
    case CISELOP_LOCKED:
      if (set) odata->LockSelection();else odata->UnlockSelection();break;
    case CISELOP_SELALL:
      {
        Selection *sel=(Selection *)odata->GetSelection();
        for (int i=0;i<odata->NPoints();i++)
          sel->PointSelect(i,set);
        for (int j=0;j<odata->NFaces();j++)
          sel->FaceSelect(j,set);
      }
      break;
    case CISELOP_SELLOCKED:
      {
        int i;
        Selection *sel=(Selection *)odata->GetSelection();
        const Selection *lock=odata->GetHidden();
        for (i=0;i<odata->NPoints();i++)
          sel->PointSelect(i,lock->PointSelected(i));
        for (int j=0;i<odata->NFaces();j++)
          sel->FaceSelect(j,lock->FaceSelected(j));
      }
      break;
    case CISELOP_INVERT:
      {
        Selection *sel=(Selection *)odata->GetSelection();
        int i;
        for (i=0;i<odata->NPoints();i++)
          sel->PointSelect(i,!sel->PointSelected(i));
        for (i=0;i<odata->NFaces();i++)
          sel->FaceSelect(i,!sel->FaceSelected(i));
      }
      break;
    case CISELOP_UNSELECTTRIANGLES:
      {
        Selection *sel=(Selection *)odata->GetSelection();
        for (int i=0;i<odata->NFaces();i++) if (sel->FaceSelected(i))
        {
          FaceT fc(odata,i);
          if (fc.N()==3) sel->FaceSelect(i,false);
        }      
        odata->SelectPointsFromFaces();
      }
      break;
    case CISELOP_ONESIDE:
      {
        static char swch=1;
        odata->SelectFacesFromPoints();
        Selection temp(odata);
        if (swch)
          for (int i=0;i<odata->NFaces();i++) if (odata->FaceSelected(i))
          {
            bool remove=true;
            FaceT fc(odata,i);
            for (int j=0;j<fc.N();j++) if (!temp.PointSelected(fc.GetPoint(j)))
            {
              remove=false;
              temp.PointSelect(fc.GetPoint(j));
            }
            if (remove) odata->FaceSelect(i,false);
          }
          else;
        else
          for (int i=odata->NFaces()-1;i>=0;i--) if (odata->FaceSelected(i))
          {
            bool remove=true;
            FaceT fc(odata,i);
            for (int j=0;j<fc.N();j++) if (!temp.PointSelected(fc.GetPoint(j)))
            {
              remove=false;
              temp.PointSelect(fc.GetPoint(j));
            }
            if (remove) odata->FaceSelect(i,false);
          }
          swch=!swch;
      }
      break;
    case CISELOP_GROW:
      {
        Selection *sel=(Selection *)odata->GetSelection();
        for (int i=0;i<odata->NFaces();i++)
        {
          FaceT fc(odata,i);
          for (int j=0;j<fc.N();j++) if (odata->PointSelected(fc.GetPoint(j)))
          {
            odata->FaceSelect(i);
          }
        }
        for (int i=0;i<odata->NFaces();i++)
        {
          FaceT fc(odata,i);
          for (int j=0;j<fc.N();j++) 
          if (odata->FaceSelected(i))
          {
            FaceT fc(odata,i);
            for (int j=0;j<fc.N();j++) 
              odata->PointSelect(fc.GetPoint(j));
          }
        }
      }
      break;
    case CISELOP_SHRINK:
      {
        Selection *sel=(Selection *)odata->GetSelection();
        for (int i=0;i<odata->NFaces();i++)
        {
          FaceT fc(odata,i);
          for (int j=0;j<fc.N();j++) 
            if (!odata->FaceSelected(i))
            {
              FaceT fc(odata,i);
              for (int j=0;j<fc.N();j++) 
                odata->PointSelect(fc.GetPoint(j),false);
            }
        }
        for (int i=0;i<odata->NFaces();i++)
        {
          FaceT fc(odata,i);
          for (int j=0;j<fc.N();j++) if (!odata->PointSelected(fc.GetPoint(j)))
          {
            odata->FaceSelect(i,false);
          }
        }
      }
      break;
    case CISELOP_DESEL_VERTICES:
      {
        Selection *sel=(Selection *)odata->GetSelection();
        for (int i=0;i<odata->NPoints();i++)
          sel->PointSelect(i,set);
      }
      break;
    case CISELOP_DESEL_FACES:
      {
        Selection *sel=(Selection *)odata->GetSelection();
        for (int j=0;j<odata->NFaces();j++)
          sel->FaceSelect(j,set);
      }
      break;
    }
    return 0;
  }
  virtual void Save(ostream &str)
  {datawr(str,mode);datawr(str,set);}
  virtual int Tag() const 
  {return CITAG_SELOPERATION;}
};










CICommand *CISelOperation_Create(istream& str)
{
  return new CISelOperation(str);
}










void CObjektiv2Doc::OnViewHidesel() 
{
  comint.Begin(WString(IDS_HIDESELECTION));
  comint.Run(new CISelOperation(CISELOP_HIDDEN,true));
  UpdateAllViews(NULL);
  frame->_dlguveditor.UpdateFromDocument(this);
}










void CObjektiv2Doc::OnViewLocksel() 
{
  comint.Begin(WString(IDS_LOCKSELECTION));
  comint.Run(new CISelOperation(CISELOP_LOCKED,true));	
  UpdateAllViews(NULL);
}










void CObjektiv2Doc::OnViewUnhidesel() 
{
  comint.Begin(WString(IDS_UNHIDESELECTION));
  comint.Run(new CISelOperation(CISELOP_HIDDEN,false));	
  UpdateAllViews(NULL);
  frame->_dlguveditor.UpdateFromDocument(this);
}










void CObjektiv2Doc::OnViewUnlock() 
{
  comint.Begin(WString(IDS_UNLOCKSELECTION));
  comint.Run(new CISelOperation(CISELOP_LOCKED,false));		
  UpdateAllViews(NULL);
}










void CObjektiv2Doc::OnEditSelectAll() 
{
  comint.Begin(WString(IDS_SELECTALL));
  comint.Run(new CISelOperation(CISELOP_SELALL,true));		
  UpdateAllViews(NULL);
  UpdateAllToolbars(2);
}










void CObjektiv2Doc::OnEditDeselect() 
{
  comint.Begin(WString(IDS_DESELECT));
  comint.Run(new CISelOperation(CISELOP_SELALL,false));		
  UpdateAllViews(NULL);
  UpdateAllToolbars(2);
}


void CObjektiv2Doc::OnEditDeselectVertices() 
{
  comint.Begin(WString(IDS_DESELECTVERTICES));
  comint.Run(new CISelOperation(CISELOP_DESEL_VERTICES,false));		
  UpdateAllViews(NULL);
  UpdateAllToolbars(2);
}


void CObjektiv2Doc::OnEditDeselectFaces() 
{
  comint.Begin(WString(IDS_DESELECTFACES));
  comint.Run(new CISelOperation(CISELOP_DESEL_FACES,false));		
  UpdateAllViews(NULL);
  UpdateAllToolbars(2);
}



void CObjektiv2Doc::OnShrinkSelect() 
{
  comint.Begin(WString(IDS_DESELECT));
  comint.Run(new CISelOperation(CISELOP_SHRINK,false));		
  UpdateAllViews(NULL);
  UpdateAllToolbars(2);
}


void CObjektiv2Doc::OnGrowSelect() 
{
  comint.Begin(WString(IDS_DESELECT));
  comint.Run(new CISelOperation(CISELOP_GROW,false));		
  UpdateAllViews(NULL);
  UpdateAllToolbars(2);
}

void CObjektiv2Doc::OnEditInvertselection() 
{
  comint.Begin(WString(IDS_INVERTSEL));
  comint.Run(new CISelOperation(CISELOP_INVERT,false));		
  UpdateAllViews(NULL);
  UpdateAllToolbars(2);
}










bool CISelect::SelectByMode(Selection *sel, ObjectData *obj, int mode, bool shift, bool ctrl)
{
  Selection s=*obj->GetSelection();
  if (mode==ID_EDIT_SELECTFACES) 
  {		
    CISelect::SelectFaces(obj,sel,false);
  }
  else if (mode==ID_EDIT_SELECTOBJECTS)
  {
    CISelect::SelectObjects(obj,sel);
  }
  sel->SelectFacesFromPoints();
  if (ctrl && !shift) 
  {
    s+=*sel;
    for (int i=0;i<obj->NFaces();i++) if (!s.FaceSelected(i))
    {
      bool select=true;
      FaceT fc(obj,i);
      for (int j=0;j<fc.N();j++)
        if (!s.PointSelected(fc.GetPoint(j))==true) 
        {select=false;break;}
      if (select)
      for (int j=0;j<fc.N();j++)
        if (sel->PointSelected(fc.GetPoint(j))==true) {s.FaceSelect(i);break;}
    }
  }
  else if (ctrl && shift) 
  {
    s-=*sel;
    for (int i=0;i<obj->NFaces();i++) if (s.FaceSelected(i))
    {
      FaceT fc(obj,i);
      for (int j=0;j<fc.N();j++)
        if (s.PointSelected(fc.GetPoint(j))==false)
        {
          s.FaceSelect(i,false);
          break;
        }
    }
  }
  else if (shift && !ctrl) s&=*sel;
  else s=*sel;
  *sel=s;  
  return true;
}










bool CISelect::SelectFaces(ObjectData *obj, Selection *sel, bool verttoo)
{
  int cnt=obj->NFaces();  
  bool res=false;
  for (int i=0;i<cnt;i++) if (!sel->FaceSelected(i))
  {
    FaceT f(obj,i);	
    int j;
    for (j=0;j<f.N();j++)
    {
      int idx=f.GetPoint(j);
      if (sel->PointSelected(idx)) break;
    }
    if (j!=f.N()) 
    {
      if (verttoo) for (int k=0;k<f.N();k++)	  
      {
        int idx=f.GetPoint(k);
        if (!res) res=!sel->PointSelected(idx);
        sel->PointSelect(idx);
      }
      sel->FaceSelect(i);
    }
  }
  return res;
}



class FunctorProxyPointSelect
{
  mutable C3DC& cnt;
  mutable CRect rect;
public:
  FunctorProxyPointSelect(  C3DC& cnt,  CRect rect):cnt(cnt),rect(rect) {}
  bool operator()(VecT &vc) const
  {
    return cnt.MoveToRect(rect,vc[0],vc[1],vc[2]);
  }
};




bool CISelect::SelectObjects(ObjectData *obj, Selection *s)
{
  bool res=false;
  while (SelectFaces(obj,s)) res=true;
  return res;
}





void CObjektiv2Doc::RectangleSelection(C3DC& cnt,CRect lastrect, bool set, bool ctrl, bool shift)
{
  ObjectData *odata=LodData->Active();
  const Selection *hid=odata->GetHidden();
  const Selection *proxies=odata->GetTool<ObjToolProxy>().GetShowProxySelection();
  int count=odata->NPoints();
  lastrect.NormalizeRect();
  if (!set)
  {
    cnt.SelectObject(&selpen);
    cnt.SelectStockObject(HOLLOW_BRUSH);
    cnt.Rectangle(&lastrect);
    cnt.SelectStockObject(BLACK_BRUSH);
    for (int i=0;i<count;i++) if (!hid->PointSelected(i) && (proxies==0 || !proxies->PointSelected(i)))
    {
      PosT& pt=odata->Point(i);
      if (cnt.MoveToRect(lastrect,pt[0],pt[1],pt[2])) 
        cnt.Point3D(NULL,C3DCPT_RECTANGLE,3);
    }
  }
  else
  {	
    Selection *sel=new Selection(odata);
    int count=odata->NPoints();
    for (int i=0;i<count;i++) if (!hid->PointSelected(i) && (proxies==0 || !proxies->PointSelected(i)))
    {
      PosT& pt=odata->Point(i);
      if (cnt.MoveToRect(lastrect,pt[0],pt[1],pt[2])) 
        sel->PointSelect(i);
    }
    if (proxies) EnumAllProxyPoints(odata,hid,proxies,_proxyCache,GetProxyLevel(),FunctorProxyPointSelect(cnt,lastrect),sel);
    if (comint.IsLocked(false) 
      || (sel->IsEmpty() && odata->CountSelectedFaces()==0 && odata->CountSelectedPoints()==0)) 
    {
      UpdateAllViews(NULL);
      delete sel;
      return;
    }
    comint.Begin(WString(IDS_MAKESELECTION));
    CISelect::SelectByMode(sel,LodData->Active(),frame->GetEditCommand(),shift,ctrl);
    comint.Run(new CISelect(sel));
    seltransform=false;
    UpdateAllViews(NULL);    
    UpdateAllToolbars(2);	
  }
}










void CObjektiv2Doc::TouchFace(C3DC& cnt, CPoint &pt,bool ctrl, bool shift, bool polygon)
{
  if (comint.IsLocked()==true) return;
  ObjectData *odata=LodData->Active();
  const Selection *hid=odata->GetHidden();
  const Selection *proxies=odata->GetTool<ObjToolProxy>().GetShowProxySelection();
  AutoArray<int, MemAllocLocal<int, 100> >proxyFaces;
  int count=odata->NFaces();
  Selection *sel=new Selection(odata);
  if (ctrl) 
  {
    for (int i=0;i<count;i++) sel->FaceSelect(i,odata->FaceSelected(i));
  }
  HVECTOR lpt;
  CopyVektor(lpt,mxVector3(pt.x,pt.y,0));
  int selected=-1;
  float planepos=FLT_MAX;
  int i;
  for (i=0;i<count;i++) 
    if (proxies && proxies->FaceSelected(i))
      if (!hid->FaceSelected(i)) proxyFaces.Append()=i;
      else;
    else if (!hid->FaceSelected(i))
  {
    FaceT fc(odata,i);
    for (int j=0;j<fc.N();j++)
    {
      PosT& pos=odata->Point(fc.GetPoint(j));
      LPHVECTOR l=mxVector3(pos[0],pos[1],pos[2]);
      switch (j)
      {
      case 0:cnt.MoveTo3D(l);break;
      case 1:cnt.BeginStripFan(l);break;
      case 2:
      case 3:if (cnt.PtTestFan(l,lpt,-1)==true)           
               if (cnt.lasttestdistance<planepos && cnt.lasttestdistance>0.0f)
               {
                 selected=i;
                 planepos=cnt.lasttestdistance;
               }
      }
    }
  }
  if (proxies && proxyFaces.Size())
    for (int i=0;i<proxyFaces.Size();i++)
    {
      FaceT pfc(odata,proxyFaces[i]);
      ObjectData *proxy=_proxyCache.GetBestProxyObject(odata->GetTool<const ObjToolProxy>().GetProxySelection(pfc),GetProxyLevel());
      if (proxy)
      {
        Matrix4 mx;
        proxy->GetTool<ObjToolProxy>().GetProxyTransform(pfc,mx);
        for (int k=0,count=proxy->NFaces();k<count;k++) 
        {
          FaceT fc(proxy,k);
          for (int j=0;j<fc.N();j++)
          {
            PosT& pos=proxy->Point(fc.GetPoint(j));
            VecT vc;vc.SetFastTransform(mx,pos);
            LPHVECTOR l=mxVector3(vc[0],vc[1],vc[2]);
            switch (j)
            {
            case 0:cnt.MoveTo3D(l);break;
            case 1:cnt.BeginStripFan(l);break;
            case 2:
            case 3:if (cnt.PtTestFan(l,lpt,-1)==true)           
                     if (cnt.lasttestdistance<planepos && cnt.lasttestdistance>0.0f)
                     {
                       selected=proxyFaces[i];
                       k=count;
                       planepos=cnt.lasttestdistance;

                     }
            }
          }

        }
      }
    }


  if (polygon)
  {
    if (selected!=-1)
    {
      ObjToolTopology& topology=odata->GetTool<ObjToolTopology>();
      int *indices=new int[65536];
      CEdges facegraph(topology.NFaces()),nocross(topology.NFaces());
      LoadEdges();      
      topology.BuildFaceGraph(facegraph,nocross);
      int cnt=topology.FindPolygon(selected,facegraph,indices,65536,
        topology.fpPrecisionMedium|topology.fpSameTextue|topology.fpSameMaterial|topology.fpSameFlags|topology.fpSameMapping,
        shift && !ctrl?NULL:&edges,sel);
      for (int i=0;i<cnt;i++)
        if (indices[i] >= 0) sel->PointSelect(indices[i]);

      delete [] indices;
    }
  }
  else
  {
    if (selected!=-1) sel->FaceSelect(selected,!shift || !ctrl);
    for (i=0;i<count;i++)if (sel->FaceSelected(i))
    {
      FaceT fc(odata,i);
      for (int j=0;j<fc.N();j++) sel->PointSelect(fc.GetPoint(j));
    }
    if (shift && !ctrl)
    {
      CISelect::SelectObjects(odata,sel);
    }
  }
  comint.Begin(WString(IDS_MAKESELECTION));
  comint.Run(new CISelect(sel));
  seltransform=false;
  UpdateAllViews(NULL);    
  UpdateAllToolbars(2);	
}










void CObjektiv2Doc::OnEditCenterpin() 
{
  ObjectData *odata=LodData->Active();
  if (!odata) return;
  bool test=true;
  const Selection *testsel=odata->GetSelection();
  if (testsel->IsEmpty()) 
  {
    testsel=odata->GetHidden();
    test=false;
  }
  HVECTOR v1=
  {0,0,0,1};
  int sumacnt=0;
  int cnt=odata->NPoints();
  for (int i=0;i<cnt;i++) if (testsel->PointSelected(i)==test)
  {
    PosT& pos=odata->Point(i);
    v1[XVAL]+=pos[0];
    v1[YVAL]+=pos[1];
    v1[ZVAL]+=pos[2];
    sumacnt+=1;
  }
  if (sumacnt!=0) NasobVektor(v1,1.0/sumacnt);
  CopyVektor(pin,v1);
  UpdateAllViews(NULL);
}





class FunctorPolygonSelect
{
  mutable C3DC &cnt;
public:
  FunctorPolygonSelect(C3DC &cnt):cnt(cnt) {}
  bool operator () (VecT &vc) const
  {
    COLORREF col=cnt.GetColorAt(vc[0],vc[1],vc[2],1);
    return col==0;
  }
};




void CObjektiv2Doc::PolygonSelection(C3DC &cnt, CPolygonSelection &pol, bool ctrl, bool shift)
{
  if (comint.IsLocked()==true) return;
  RECT rc;
  cnt.GetClipBox(&rc);
  cnt.SelectStockObject(WHITE_BRUSH);
  cnt.SelectStockObject(NULL_PEN);
  cnt.Rectangle(&rc);
  pol.Draw(&cnt,false);
  ObjectData *odata=LodData->Active();
  const Selection *hid=odata->GetHidden();
  Selection *sel=new Selection(odata);
  const Selection *proxies=odata->GetTool<ObjToolProxy>().GetShowProxySelection();
  int count=odata->NPoints();
  for (int i=0;i<count;i++) if (!hid->PointSelected(i) && (proxies==0 || !proxies->PointSelected(i)))
  {
    PosT& pt=odata->Point(i);
    COLORREF col=cnt.GetColorAt(pt[0],pt[1],pt[2],1);
    if (col==0) sel->PointSelect(i);
  }
  if (proxies) EnumAllProxyPoints(odata,hid,proxies,_proxyCache,GetProxyLevel(),FunctorPolygonSelect(cnt),sel);
  if (sel->IsEmpty() && odata->CountSelectedFaces()==0) 
  {
    UpdateAllViews(NULL);
    return;
  }
  comint.Begin(WString(IDS_MAKESELECTION));
  CISelect::SelectByMode(sel,LodData->Active(),frame->GetEditCommand(),shift,ctrl);
  comint.Run(new CISelect(sel));  
  seltransform=false;
  UpdateAllViews(NULL);    
  UpdateAllToolbars(2);  
}










class CITransformPoints:public CICommand
{
  CTransformDlg *dlg;
public:
  CITransformPoints(istream &str) 
  {dlg=new CTransformDlg(); dlg->Load(str);}
  CITransformPoints(CTransformDlg *indlg):dlg(indlg),CICommand() 
  {}
  virtual ~CITransformPoints() 
  {delete dlg;}
  virtual int Execute(LODObject *lod)
  {
    dlg->DoTransforms(lod);
    return 0;
  }
  virtual void Save(ostream &str) 
  {dlg->Save(str);}
  virtual int Tag() const 
  {return CITAG_TRANSFORMPOINTS;}
};










CICommand *CITransformPoints_Create(istream& str)
{
  return new CITransformPoints(str);
}










void CObjektiv2Doc::OnTransform(UINT cmd) 
{
  CTransformDlg *dlg;
  dlg=new CTransformDlg();
  dlg->cmd=cmd;
  dlg->gismo=gismo_mode==gismo_selected;
  if (dlg->DoModal()==IDOK)
  {
    if (LodData->Active()->CountSelectedPoints() || dlg->allLODs || !dlg->selOnly)
    {
      comint.Begin(WString(IDS_TRANSFORMPOINTS));
      comint.Run(new CITransformPoints(dlg));
    }
    dlg->DoGismoTranform();
  }
  else
    delete dlg;  
  UpdateAllViews(NULL); 
}










void CObjektiv2Doc::TransformPreview(HMATRIX mm)
{
  seltransform=true;
  CopyMatice(seltr,mm);
}





inline float power2(float p)
{
  return p*p;
}

static void CalculateByNormals(ObjectData &obj, AutoArray<VecT> &byNormals)
{
  AutoArray<Vector3> centers;
  AutoArray<int> counts;
  RecalcNormals2(&obj,true);
  int nf=obj.NFaces();
  int np=obj.NPoints();
  int i;

  centers.Access(obj.NPoints());
  for (i=0;i<np;i++) centers[i]=VZeroP;
  for (i=0;i<nf;i++) if (obj.FaceSelected(i))
  {
    FaceT fc(obj,i);
    VecT norm=fc.CalculateNormal();
    for (int j=0;j<fc.N();j++)
    {
      centers[fc.GetPoint(j)]+=norm;
    }        
  }
  for (i=0;i<np;i++) {centers[i].Normalize();counts.Append()=0;byNormals.Append()=VZeroP;}
  for (i=0;i<nf;i++) if (obj.FaceSelected(i))
  {
    FaceT fc(obj,i);
    VecT actnormal=fc.CalculateNormal();
    for (int j=0;j<fc.N();j++)
    {
      int vx;
      Vector3P center=centers[vx=fc.GetPoint(j)];
      double angle=acos(actnormal.DotProduct(center));
      if (angle<3.1415926535/2-0.0001)
      {
        byNormals[fc.GetPoint(j)]+=center*sqrt(1.0+power2((float)tan(angle)));
        counts[vx]++;
      }
    }
  }
  for (i=0;i<np;i++) {byNormals[i]=byNormals[i]/(float)counts[i];}
  for (i=0;i<np;i++) if (!_finite(byNormals[i][0]) || !_finite(byNormals[i][1]) || !_finite(byNormals[i][2]))
    byNormals[i]=Vector3(0,0,0);

}



class CIFreeHandTrns: public CICommand
{
  HMATRIX mm;
  bool copy;
  float *locks;
  int lockcount;
  bool byNormals;
public:
  CIFreeHandTrns(istream& str) 
  {datardp(str,mm);datard(str,copy);datard(str,byNormals);lockcount=0;locks=0;}
  CIFreeHandTrns(LPHMATRIX mx, bool byN,bool shift=false) 
  {CopyMatice(mm,mx);locks=NULL;copy=shift;byNormals=byN;}
  void SetLockWeights(float *lockw, int size)
  {
    locks=new float[size];
    lockcount=size;
    memcpy(locks,lockw,sizeof(float)*size);
  }
  virtual ~CIFreeHandTrns()
  {
    delete [] locks;
  }
  virtual int Execute(LODObject *obj)
  {
    ObjectData *odata=obj->Active();
    AutoArray<VecT> byNormalsArr;
    if  (copy) 
    {
      if (odata->NNormals()==0) RecalcNormals2(odata);
      SRef<ObjectData> nobj=new ObjectData(*odata);
      if( !nobj ) return -1;
      nobj->GetTool<ObjToolClipboard>().ExtractSelection();
      odata->Merge(*nobj);							
    }    

    const Selection *sel=odata->GetSelection();
    const Selection *lock=odata->GetLocked();
    int cnt=odata->NPoints();
    HMATRIX *lockmx;
    if (locks) 
    {
      lockmx=(HMATRIX *)alloca(sizeof(HMATRIX)*lockcount);
      for (int j=0;j<lockcount;j++)
      {
        float f=locks[j];
        CopyMatice(lockmx[j],mm);
        NasobVektor(lockmx[j][0],f);lockmx[j][0][0]+=(1-f);
        NasobVektor(lockmx[j][1],f);lockmx[j][1][1]+=(1-f);
        NasobVektor(lockmx[j][2],f);lockmx[j][2][2]+=(1-f);
        NasobVektor(lockmx[j][3],f);
        if (lockmx[j][0][1]!=0 || lockmx[j][1][0]!=0 ||
          lockmx[j][0][2]!=0 || lockmx[j][2][0]!=0 ||
          lockmx[j][1][2]!=0 || lockmx[j][2][1]!=0)
        {
          NormalizeVector(lockmx[j][0]);lockmx[j][0][3]=0.0f;
          NormalizeVector(lockmx[j][1]);lockmx[j][1][3]=0.0f;
          NormalizeVector(lockmx[j][2]);lockmx[j][2][3]=0.0f;
        }
      }
    }
    else lockmx=NULL;			  
    if (byNormals)      
    {      
      CalculateByNormals(*odata,byNormalsArr);
    }
    float factor=mm[3][0]+mm[3][1]+mm[3][2];
    for(int i=0;i<cnt;i++) if (odata->PointSelected(i) && !lock->PointSelected(i))
    {
      PosT &pos=odata->Point(i);
      HVECTOR tv;
      float f=sel->PointWeight(i);		
      if (byNormals)
      {
        tv[XVAL]=pos[0]+byNormalsArr[i][0]*factor;
        tv[YVAL]=pos[1]+byNormalsArr[i][1]*factor;
        tv[ZVAL]=pos[2]+byNormalsArr[i][2]*factor;
      }
      else
      {
        TransformVector(mm,mxVector3(pos[0],pos[1],pos[2]),tv);
        CorrectVectorM(tv);
      }
      pos[0]=pos[0]+(tv[XVAL]-pos[0])*f;
      pos[1]=pos[1]+(tv[YVAL]-pos[1])*f;
      pos[2]=pos[2]+(tv[ZVAL]-pos[2])*f;
      if (!_finite(pos[0]) || !_finite(pos[1]) || !_finite(pos[2]))
        pos[0]=pos[1]=pos[2]=0;
    }
    if (lockmx)
      for(int k=0;k<odata->NAnimations();k++)
      {
        AnimationPhase *a=odata->GetAnimation(k);
        if (k<lockcount && locks[k]!=0.0f)
        {
          for(int i=0;i<cnt;i++) if (odata->PointSelected(i) && !lock->PointSelected(i))
          {
            Vector3P &pos=(*a)[i];
            HVECTOR tv;
            float f=sel->PointWeight(i);		
            LPHMATRIX mx=lockmx[k];
            float factor=mx[3][0]+mx[3][1]+mx[3][2];
            if (byNormals)
            {
              tv[XVAL]=pos[0]+byNormalsArr[i][0]*factor;
              tv[YVAL]=pos[1]+byNormalsArr[i][1]*factor;
              tv[ZVAL]=pos[2]+byNormalsArr[i][2]*factor;
            }
            else
            {
              TransformVector(mx,mxVector3(pos[0],pos[1],pos[2]),tv);
              CorrectVectorM(tv);
            }
            pos[0]=pos[0]+(tv[XVAL]-pos[0])*f;
            pos[1]=pos[1]+(tv[YVAL]-pos[1])*f;
            pos[2]=pos[2]+(tv[ZVAL]-pos[2])*f;
          }
        }
      }
      return 0;	  
  }
  virtual void Save(ostream& str) 
  {datawrp(str,mm);datawr(str,copy);datawr(str,byNormals);}
  virtual int Tag() const 
  {return CITAG_FREEHANDTRNS;}
};










CICommand *CIFreeHandTrns_Create(istream &str)
{
  return new CIFreeHandTrns(str);
}










void CObjektiv2Doc::ApplyPreviewTransform(bool shift)
{
  if (!seltransform) return;
  if (gismo_mode==gismo_selected) 
    EditGizmo(seltr);
  else if (LodData->Active()->CountSelectedPoints())
  {
    CIFreeHandTrns *trns=new CIFreeHandTrns(seltr,frame->movebynormals,shift);
    float *weightlist;
    int wsize;
    weightlist=frame->m_wndAnimListBar.GetWeightList(wsize);
    trns->SetLockWeights(weightlist,wsize);
    comint.Begin(WString(IDS_FREEHANDTRNS));  
    comint.Run(trns);
    if (shift) UpdateAllToolbars(1);    
    if (selrecordenable)
      SoucinMatic(selrecord,seltr,selrecord);
  }
  if (selghost)
  {
    SoucinMatic(ghost,seltr,ghost);
  }
  UpdateAllViews(NULL);
  LoadEdges();
  if (automapsel)
  {
    CObjektiv2View *curview=CObjektiv2View::GetCurrentView();
    if (curview && curview->texview && curview->texview->textureName && curview->texview->textureName[0])
    {
      Selection *sel=new Selection(*LodData->Active()->GetSelection());
      Selection *block=new Selection(*combine_saved);
      comint.Run(new CISelect(block));
      curview->SendMessage(WM_COMMAND,ID_SURFACES_BACKGROUNDMAPPING,0);
      comint.Run(new CISelect(sel));
    }    
  }
  byNormals.Clear();
}










void CObjektiv2Doc::OnTransformUser() 
{
  /*  frame->calcdlg.DoModal();*/
}


















void CObjektiv2Doc::UpdateViewer(CObjektiv2View *sender)
{ 
  extupdate=true;
}










class CIMapping:public CICommand
{
  CTexMappingDlg *mapdlg;
public:
  CIMapping(CTexMappingDlg *dlg):mapdlg(dlg) 
  {}
  virtual int Execute(LODObject *lod)
  {
    ObjectData *odata=lod->Active();
    if (mapdlg->spehere)
      mapdlg->CalcSpehereMappingToObject(odata);
    else
      mapdlg->CalcCylindricMappingToObject(odata);
    comint.sectchanged=true;
    return 0;
  }
  virtual ~CIMapping() 
  {delete mapdlg;}
};










void CObjektiv2Doc::OnSurfacesSpeheremap() 
{
  CTexMappingDlg mapdlg;
  int res;
  mapdlg.spehere=true;
  CopyVektor(mapdlg.pos,pin);
  comint.Lock(true);
  mapdlg.previewobject=LodData->Active();  
  mapdlg.TexName=GetTextureFrom(IDC_SETFROMVIEW,"");
  res=mapdlg.DoModal();        	
  comint.Lock(false);
  if (res==IDCANCEL) return;
  comint.Begin(WString(IDS_SPEHEREMAP));
  comint.Run(new CIMapping(new CTexMappingDlg(mapdlg)));
  UpdateMatLib();
}










void CObjektiv2Doc::OnCylindricMapping(HVECTOR pos)
{
  int res;
  mapdlg.spehere=false;
  CopyVektor(mapdlg.pos,pin);
  CopyVektor(mapdlg.dir,pos);
  RozdilVektoru(mapdlg.dir,mapdlg.pos);
  mapdlg.previewobject=LodData->Active();
  mapdlg.TexName=GetTextureFrom(IDC_SETFROMVIEW,mapdlg.TexName);
  comint.Lock(true);
  res=mapdlg.DoModal();        	
  comint.Lock(false);
  if (res==IDCANCEL) return;
  comint.Begin(WString(IDS_CYLINDRICMAP));
  comint.Run(new CIMapping(new CTexMappingDlg(mapdlg)));
  UpdateMatLib();
}










void CObjektiv2Doc::OnUpdateSurfacesCylmap(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(frame->GetEditCommand()==ID_SURFACES_CYLMAP);
  pCmdUI->Enable((HWND)mapdlg==NULL);
}










void CObjektiv2Doc::OnUpdateSurfacesSpeheremap(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable((HWND)mapdlg==NULL);
}







#include "ISimpleCommand.h"




void CObjektiv2Doc::OnSimpleCommand(UINT cmd)
{
  int text=cmd-ID_SURFACES_RECALCULATENORMALS+IDS_RECALCNORMALS;
  comint.Begin(WString(text));
  if (LodData->Active()->NNormals()<1)
    comint.Run(new CISimpleCommand(CIS_RECALCNORM));
  comint.Run(new CISimpleCommand(cmd-ID_SURFACES_RECALCULATENORMALS+1));
  if (cmd==ID_STRUCTURE_CHECKFACES || cmd==ID_STRUCTURE_TOPOLOGY_FINDCOMPONENTS ||
    cmd==ID_STRUCTURE_CONVEXITY_CONVEXHULL || cmd==ID_STRUCTURE_CONVEXITY_COMPONENTCONVEXHULL ||
    cmd==ID_STRUCTURE_PINCULL || cmd==ID_STRUCTURE_CONVEXITY_FINDNONCONVEXITIES)	
    UpdateAllToolbars(1);
  UpdateAllViews(NULL);
  if (cmd==ID_SURFACES_RECALCULATENORMALS || cmd==ID_STRUCTURE_OPTIMIZE)
    comint.SetLongOp();
  if (cmd==ID_SURFACES_SHARPEDGE || cmd==ID_SURFACES_SMOOTHEDGE)
    LoadEdges();
  if (cmd==ID_EDIT_DELETE || cmd==ID_POINTS_MERGE || cmd==ID_POINTS_REMOVE || cmd==ID_FACES_REMOVE)
    frame->_dlguveditor.UpdateFromDocument(this);

}










#define EnableMenu(pMenu,id,flag) \
  (pMenu)->EnableMenuItem((id),((flag)?MF_ENABLED:MF_GRAYED)|MF_BYCOMMAND)
#define CheckMenu(pMenu,id,flag) \
  (pMenu)->CheckMenuItem((id),((flag)?MF_CHECKED:MF_UNCHECKED)|MF_BYCOMMAND)


class FunctorUVSetToMenu
{
  CMenu *menu;
  mutable CString str;
  CString format;
  mutable int pos;
  ObjectData *objdata;
public:
  FunctorUVSetToMenu(CMenu *menu,ObjectData *data):menu(menu),objdata(data),pos(0) 
  {
    format.LoadString(IDS_UVSET_ACTIVATE);
  }
  bool operator()(ObjUVSet *uvset) const
  {
    str.Format(format,uvset->GetIndex());
    menu->InsertMenu(pos,MF_STRING|MF_BYPOSITION,ID_UVSETS_ACTIVATE0+pos,str);
    if (uvset==objdata->GetActiveUVSet()) menu->CheckMenuItem(pos,MF_CHECKED|MF_BYPOSITION);
    pos++;
    return false;
  }

};

void CObjektiv2Doc::OnInitMenu(CMenu *pMenu)
{
  ObjectData *obj=LodData->Active();
  int countFace=obj->CountSelectedFaces();
  int countPoint=obj->CountSelectedPoints();
  int cnt=pMenu->GetMenuItemCount();
  for (int i=0;i<cnt;i++)
  {
    //	CMenu *pSubMenu=pMenu->GetSubMenu(i);
    //	if (pSubMenu!=NULL) OnInitMenu(pSubMenu);
    //	else
    {
      int id=pMenu->GetMenuItemID(i);
      switch (id)
      {
      case ID_EDIT_GROWSELECTED:
      case ID_EDIT_SHRINKSELECTED:
        EnableMenu(pMenu,id,countPoint>=1||countFace>=1);break;
      case ID_CREATE_FACE:
        EnableMenu(pMenu,id,countPoint==3||countPoint==4);break;
      case ID_VIEW_COLORIZEOBJECTS:
        CheckMenu(pMenu,id,colorize);break;
      case ID_STRUCTURE_TRIANGULATE:
      case ID_STRUCTURE_TRIANGULATE2:
      case ID_STRUCTURE_TRIANGULATE_CONVEX:
      case ID_STRUCTURE_TRIANGULATE_CONCAVE:
      case ID_STRUCTURE_SQARIZE:
      case ID_FACES_REVERSE:
      case ID_FACES_UNCROSS:
      case ID_FACES_REMOVE:
      case ID_STRUCTURE_TOPOLOGY_SPLIT:
      case ID_VIEW_LOOKATFACE:
      case ID_FACES_EXTRUDE:
      case ID_CREATE_SHAPE:
      case ID_FACES_SPLITHOLE:
      case ID_FACES_MOVETOP:
      case ID_FACES_MOVEBOTTOM:
      case ID_FACES_MOVETONEXTALPHA:
      case ID_FACES_MOVETOPREVALPHA:
      case ID_SURFACES_BGRFROMFACE:
      case ID_STRUCTURE_TOPOLOGY_CLOSE:
      case ID_SURFACES_BACKGROUNDMAPPING:
      case ID_SURFACES_CYLMAP:
      case ID_SURFACES_SPEHEREMAP:
      case ID_SURFACES_GIZMOMAPPING:
      case ID_POINTS_BEVEL:
      case ID_EDIT_UNSELECTTRIANGLES:
      case ID_EDIT_SELECTONESIDE:
      case ID_CONVEXITY_NEWCONVEXHULL:  
      case ID_CONVEXITY_FINDNCPOINTS:
      case ID_CONVEXITY_CONVEXITYFINETUNE:
      case ID_TRANSFORM3D_ATTACHTOFACE:
      case ID_STRUCTURE_CONVEXITY_BREAKCONVEX:
        EnableMenu(pMenu,id,countFace>=1);break;
      case ID_SURFACES_SHARPEDGE:
      case ID_SURFACES_SMOOTHEDGE:
        EnableMenu(pMenu,id,countPoint>=2);break;
      case ID_SURFACES_BGRFROMSEL:
      case ID_STRUCTURE_CONVEXITY_CONVEXHULL:
      case ID_EDIT_LINEARDEFORM:
      case ID_POINTS_MERGE:
      case ID_POINTS_FLATTEN:
      case ID_POINTS_MERGENEAR:	
      case ID_POINTS_LOCKNORMALS:
      case ID_SURFACES_UNWRAP:
        EnableMenu(pMenu,id,countPoint>=1);break;
      case ID_STRUCTURE_CONVEXITY_SELECTHALFSPACE:
        //		case IDS_TOOLS_ANIMNORM:
      case ID_TRANSFORM3D_FROMPROXY:
        EnableMenu(pMenu,id,countFace==1);break;			
      case ID_EDIT_COPY:
      case ID_EDIT_CUT:
      case ID_EDIT_DELETE:
        EnableMenu(pMenu,id,countPoint>0);break;
      case ID_EDIT_PASTE:
        EnableMenu(pMenu,id,::IsClipboardFormatAvailable(theApp.ClipFormat1));
        break;
      case ID_SURFACES_SHOWGISMO:
        CheckMenu(pMenu,id,gismo_mode!=gismo_hide && !localAxisMode);break;
      case ID_SURFACES_EDITGISMO:
        CheckMenu(pMenu,id,gismo_mode==gismo_selected  && !localAxisMode);break;
      case ID_SURFACES_UNDOGIZMO:
      case ID_SURFACES_RESETGISMO:
        EnableMenu(pMenu,id,gismo_mode==gismo_selected);break;
      case ID_TRANSF_MOVE:
      case ID_TRANSF_ROTATE:
      case ID_TRANSF_SCALE:
      case ID_TRANS2_ROTATE:
      case ID_TRANS2_SCALE:
      case ID_TRANS2_MIRRORX:
      case ID_TRANS2_MIRRORY:
        EnableMenu(pMenu,id,countPoint>0 || gismo_mode==gismo_selected);break;
      case ID_FACES_REPAIRDEGENERATED:
      case ID_DEGENERATEDFACES_CHECK:
        EnableMenu(pMenu,id,obj->NFaces()>0);break;
      case ID_CREATE_POLYGON:
        EnableMenu(pMenu,id,countPoint>2);break;
      case ID_UVSETS_ADD:
        {
          MENUITEMINFO nfo;
          nfo.cbSize=sizeof(nfo);
          nfo.fMask=MIIM_FTYPE;    
          pMenu->GetMenuItemInfo(0,&nfo,TRUE);
          while (nfo.fType!=MFT_SEPARATOR)
          {
            pMenu->DeleteMenu(0,MF_BYPOSITION);
            pMenu->GetMenuItemInfo(0,&nfo,TRUE);
          }
          LodData->Active()->SortUVSets();
          LodData->Active()->EnumUVSets(FunctorUVSetToMenu(pMenu,LodData->Active()));    
        }
        return;
      }
    }
  }
}










void CObjektiv2Doc::OnPointsProperties() 
{
  CVertexProp dlg;
  dlg.obj=LodData->Active();
  dlg.DoModal();
  UpdateAllViews(NULL);
}










void CObjektiv2Doc::OnFaceProperties() 
{
  CFaceProp dlg;
  dlg.obj=LodData->Active();
  dlg.DoModal();
  UpdateAllViews(NULL);
  UpdateAllToolbars(0);
}










void CObjektiv2Doc::OnUpdateFaceProperties(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(!LodData->Active()->GetSelection()->IsEmpty());
}










void CObjektiv2Doc::OnViewColorizeobjects() 
{
  int i=coldlg.DoModal();
  if (i<0) return;
  coldlg.ColorizeSelection(LodData->Active(),i);
  comint.SetLongOp();
  UpdateAllViews(NULL);
  frame->_dlguveditor.UpdateFromDocument(this);
}










void CObjektiv2Doc::OnEditLockpin() 
{
  pinlock=!pinlock;
  UpdateAllViews(NULL);
}










void CObjektiv2Doc::OnUpdateEditLockpin(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(pinlock==true);
}










static LPHVECTOR GetPointFromFace(ObjectData *od, FaceT& face, int index,LPHVECTOR vx=NULL,AnimationPhase *phs=NULL)
{
  int ix=face.GetPoint(index);
  static HVECTOR v;
  if (vx==NULL) vx=v;
  if (phs==NULL)
  {
    PosT& pt=od->Point(ix);	  
    if (vx==NULL) vx=v;
    vx[XVAL]=pt[0];
    vx[YVAL]=pt[1];
    vx[ZVAL]=pt[2];
    vx[WVAL]=1.0f;  
  }
  else
  {	
    vx[XVAL]=(*phs)[ix].X();
    vx[YVAL]=(*phs)[ix].Y();
    vx[ZVAL]=(*phs)[ix].Z();
    vx[WVAL]=1.0f;  
  }
  return vx;
}










void DrawModel(ObjectData *odata, C3DC& cnt, COLORREF color, int animation)
{
  if (odata==0) return;
  CPen pen(PS_SOLID,1,color);  
  CPen *old=cnt.GetCurrentPen();
  cnt.SelectObject(&pen);
  AnimationPhase *phs;
  const Selection *sel=odata->GetHidden();
  if (animation!=-1) 
  {
    if (odata->NAnimations()==0) animation=-1;
    else 
    {
      animation=animation%odata->NAnimations();
      phs=odata->GetAnimation(animation);
    }
  }
  else phs=NULL;
  int fcount=odata->NFaces();  
  for (int i=0;i<fcount;i++) if (!sel->FaceSelected(i))
  {		
    FaceT fc(odata,i);
    switch (fc.N())
    {
    case 2: cnt.MoveTo3D(GetPointFromFace(odata,fc,0,NULL,phs));
      cnt.LineTo3D(GetPointFromFace(odata,fc,1,NULL,phs));
      break;
    case 3: cnt.MoveTo3D(GetPointFromFace(odata,fc,0,NULL,phs));
      cnt.BeginStripFan(GetPointFromFace(odata,fc,1,NULL,phs));
      cnt.WiredFan(GetPointFromFace(odata,fc,2,NULL,phs));
      break;
    case 4: 
      {
        HVECTOR v1,v2;
        cnt.MoveTo3D(GetPointFromFace(odata,fc,0,NULL,phs));
        cnt.BeginStripFan(GetPointFromFace(odata,fc,1,NULL,phs));
        cnt.Quad3D(GetPointFromFace(odata,fc,2,v1,phs),GetPointFromFace(odata,fc,3,v2,phs));
      }
      break;
    }			  
  }
  cnt.SelectObject(old);
}










void CObjektiv2Doc::OnEditCopy() 
{
  if (LodData->Active()->GetTool<ObjToolClipboard>().SelectionCopy(theApp.ClipFormat1)==false)
    AfxMessageBox(IDS_COPYFAILED,MB_ICONSTOP);
}










void CObjektiv2Doc::OnEditCut() 
{
  ObjectData *obj=LodData->Active();
  if (obj->GetTool<ObjToolClipboard>().SelectionCopy(theApp.ClipFormat1)==false) return;
  comint.Begin(WString(IDS_CUTTEXT));
  comint.Run(new CISimpleCommand(CIS_DELETE));
  UpdateAllToolbars();
  UpdateAllViews(NULL);
  frame->_dlguveditor.UpdateFromDocument(this);

}










#include "IMerge.h"







CICommand *CIMerge_Create(istream& str)
{
  return new CIMerge(str);
}










class CIMergeAll:public CICommand
{
  LODObject *obj;
  bool createlods;
  char *selname;
public:
  CIMergeAll(LODObject *obj,bool crlods,const char *selname=NULL):obj(obj),createlods(crlods) 
  {
    recordable=false;
    if (selname) this->selname=_strdup(selname);else this->selname=NULL;
  }
  virtual int Execute(LODObject *lod)
  {
    comint.sectchanged=true;
    return !lod->Merge(*obj,createlods,selname);
  }
  virtual ~CIMergeAll() 
  {delete obj;free(selname);}
};










void CObjektiv2Doc::OnEditPaste() 
{
  ObjectData *obj=new ObjectData();
  if( obj )
  {
    if( obj->GetTool<ObjToolClipboard>().LoadClipboard(theApp.ClipFormat1) )
    {
      comint.Begin(WString(IDS_PASTETEXT));
      comint.Run(new CIMerge(obj));
      UpdateAllToolbars();
      UpdateAllViews(NULL);
    }
    else
      delete obj;
  }
}










void CObjektiv2Doc::OnSurfacesMapping() 
{
  /*  frame->mapdlg.DoModal();*/
}










void CObjektiv2Doc::OnUpdateModel()
{
  extupdate=!config->GetBool(IDS_CFGMANUALUPDATE);
}










void CObjektiv2Doc::LoadEdges()
{
  ObjectData *odata=LodData->Active();
  odata->LoadSharpEdges(edges);
}










#include "IExtrude.h"

void CObjektiv2Doc::OnFacesExtrude() 
{
#ifdef FULLVER
  int i=frame->m_wndExtrude.GetSegments();
  comint.Begin(WString(IDS_EXTRUDE));
  comint.Run(new CIExtrude(i));
  UpdateAllViews(NULL);
#endif
}










void CObjektiv2Doc::UpdateStatus()
{
  int selfc=LodData->Active()->CountSelectedFaces();
  int selpt=LodData->Active()->CountSelectedPoints();
  char buff[25];
  if (numSections==-1) strcpy(buff,"???");
  else _itoa(numSections,buff,10);
  frame->SetStatus(WString(IDS_STATUSTEXT,LodData->Active()->NPoints(),LodData->Active()->NFaces(),buff,selpt,selfc,(lastP3Dsize+512)/1024));
}









#include "Transform2D.h"

void CObjektiv2Doc::OnTrans2Rotate() 
{
  static float save=0;
  CTransform2D dlg;
  dlg.UsePin=this->pinlock;
  dlg.idctext=IDS_TITLEROTATE;
  dlg.Value=save;
  CObjektiv2View *view=view->GetCurrentView();
  dlg.toview=view;
  dlg.mode=CTransform2D::em_rotate;
  if (dlg.DoModal()==IDOK)
  {
    save=dlg.Value;
    HMATRIX mm;
    if (localAxisMode && gismo_mode!=gismo_hide && dlg.UsePin)
      view->CalcRotationOnLocalAxis(torad(dlg.Value),mm);
    else
      view->CalcRotationOnPlane(torad(dlg.Value),mm,dlg.UsePin==TRUE,true);
    if (gismo_mode==gismo_selected)
      EditGizmo(mm);
    else  if (LodData->Active()->CountSelectedPoints())
    {
      CIFreeHandTrns *trns=new CIFreeHandTrns(mm,false);
      float *weightlist;
      int wsize;
      weightlist=frame->m_wndAnimListBar.GetWeightList(wsize);
      trns->SetLockWeights(weightlist,wsize);
      comint.Begin(WString(IDS_TRANSFORMPOINTS));  
      comint.Run(trns);
    }

    if (selghost)
    {
      SoucinMatic(ghost,mm,ghost);
    }
    UpdateAllViews(NULL);			
  }
}










void CObjektiv2Doc::OnTrans2Scale() 
{
  static float save=1;
  CTransform2D dlg;
  dlg.UsePin=this->pinlock;
  dlg.idctext=IDS_TITLESCALE;  
  dlg.Value=save;
  CObjektiv2View *view=view->GetCurrentView();
  dlg.toview=view;
  dlg.mode=CTransform2D::em_scale;
  if (dlg.DoModal()==IDOK)
  {
    save=dlg.Value;
    HMATRIX mm;
    view->CalcScaleOnPlane(dlg.Value,mm,dlg.UsePin==TRUE,true);
    if (LodData->Active()->CountSelectedPoints())
    {
      CIFreeHandTrns *trns=new CIFreeHandTrns(mm,false);
      //view->CalcScaleOnPlane(torad(dlg.Value),mm,dlg.UsePin==TRUE);
      float *weightlist;
      int wsize;
      weightlist=frame->m_wndAnimListBar.GetWeightList(wsize);
      trns->SetLockWeights(weightlist,wsize);
      comint.Begin(WString(IDS_TRANSFORMPOINTS));  
      comint.Run(trns);
    }
    if (gismo_mode==gismo_selected)
      EditGizmo(mm);
    if (selghost)
    {
      SoucinMatic(ghost,mm,ghost);
    }
    UpdateAllViews(NULL);
  }
}










void CObjektiv2Doc::OnTrans2Mirrorx() 
{
  CObjektiv2View *view=view->GetCurrentView();
  HMATRIX mm;
  view->CalcMirror(false,mm,pinlock);
  if (LodData->Active()->CountSelectedPoints())
  {
    float *weightlist;
    int wsize;
    CIFreeHandTrns *trns=new CIFreeHandTrns(mm,false);
    weightlist=frame->m_wndAnimListBar.GetWeightList(wsize);
    trns->SetLockWeights(weightlist,wsize);
    comint.Begin(WString(IDS_TRANSFORMPOINTS));  
    comint.Run(trns);
  }
  if (gismo_mode==gismo_selected)
    EditGizmo(mm);
  if (selghost)
  {
    SoucinMatic(ghost,mm,ghost);
  }
  UpdateAllViews(NULL);
}










void CObjektiv2Doc::OnTrans2Mirrory() 
{
  CObjektiv2View *view=view->GetCurrentView();
  HMATRIX mm;
  view->CalcMirror(true,mm,pinlock);
  if (LodData->Active()->CountSelectedPoints())
  {
    float *weightlist;
    int wsize;
    CIFreeHandTrns *trns=new CIFreeHandTrns(mm,false);
    weightlist=frame->m_wndAnimListBar.GetWeightList(wsize);
    trns->SetLockWeights(weightlist,wsize);
    comint.Begin(WString(IDS_TRANSFORMPOINTS));  
    comint.Run(trns);
  }

  if (gismo_mode==gismo_selected)
    EditGizmo(mm);
  if (selghost)
  {
    SoucinMatic(ghost,mm,ghost);
  }
  UpdateAllViews(NULL);
}










void CObjektiv2Doc::OnCreateShape() 
{
#ifdef FULLVER
  int i=frame->m_wndExtrude.GetSegments();
  comint.Begin(WString(IDS_SHAPE));
  comint.Run(new CIShape(i));
  UpdateAllViews(NULL);
#endif
}










static CSpaceWarpDlg spacewarpdlg;

class CIElasticSel:public CICommand
{
  CSpaceWarpDlg *dlg;
public:
  CIElasticSel(CSpaceWarpDlg *d):dlg(d) 
  {recordable=false;}
  virtual int Execute(LODObject *lod)
  {
    return dlg->Run(lod->Active());
  }
  virtual ~CIElasticSel() 
  {delete dlg;}
};










void CObjektiv2Doc::OnEditElasticsel() 
{
  spacewarpdlg.odata=LodData->Active();
  spacewarpdlg.DoModal();
  comint.Begin(WString(IDS_SPACEWARP));
  comint.Run(new CIElasticSel(spacewarpdlg.Clone()));
}










void CObjektiv2Doc::OnUpdateStatusBar(CCmdUI* pCmdUI) 
{
  this->UpdateStatus();	
}










void CObjektiv2Doc::OnViewRestartexternal() 
{
  if (frame->ExternalError(frame->_externalViewer.Restart()))
    UpdateModel();
  /*  iviewer.CloseExternal(true);
  iviewer.StartExternal(config->GetString(IDS_CFGEXTERNARVIEWER),
  &LODObject(),1.0,config->GetBool(IDS_CFGTWOMONITORS));	
  UpdateModel();*/    
}










void CObjektiv2Doc::Autosave()
{
  const char *c=config->GetString(IDS_CFGAUTOSAVENAME);
  if (c=="") return;
  LodData->Save(Pathname(c),OBJDATA_LATESTVERSION,false,SaveOxySetup,NULL);
}






/*



void CObjektiv2Doc::OnFileSend() 
{
CString defext;defext.LoadString(IDS_FILEDEFEXT);
CString filters;filters.LoadString(IDS_FILEOPENFILTERS);
filename=this->GetPathName();
CFileDialogEx fdlg(FALSE,defext,Pathname::GetNameFromPath(filename),OFN_PATHMUSTEXIST|OFN_LONGNAMES|OFN_OVERWRITEPROMPT, filters,NULL);
config->FillOpenDialogByPath(IDS_CFGSENDFOLDER,fdlg.m_ofn,filename);
if (fdlg.DoModal()!=IDOK) return;
Pathname s=fdlg.GetPathName();
config->SaveCurrPath(IDS_CFGSENDFOLDER,s);
LodData->Save(s,OBJDATA_LATESTVERSION,false,SaveOxySetup,NULL);
//  s.SetFilename("data");
s.SetExtension("");
CreateDirectory(s,NULL);
Pathname trg;
trg.SetDirectory(s);
Pathname src;
src.SetDirectory(config->GetString(IDS_CFGPATHFORTEX));  
for (int p=0;p<LodData->NLevels();p++)
{
ObjectData *obj=LodData->Level(p);
frame->m_wndTexList.UpdateModelTextures(obj,false,false);
}
void *enume=NULL;
int max=0,i=0;
while (frame->m_wndTexList.EnumModelTextures(&enume)) max++;
enume=NULL;
const char *name=frame->m_wndTexList.EnumModelTextures(&enume);
while (name)
{
src.SetFilename(name);
trg.SetFilename(name);
Pathname fld((LPCTSTR)trg);
fld.SetFilename("");
CreateDirectory(fld,NULL);
CopyFile(src,trg,FALSE);
SetProgress(i*100/max);
name=frame->m_wndTexList.EnumModelTextures(&enume);
i++;
}
}



*/






#include "OpenQDlg.h"

bool CObjektiv2Doc::ImportTextures(const char *name)
{
  WString n(name);
  Pathname pth(name);
  //  n+=".tex\\";
  //  pth.SetFilename("data");
  pth.SetExtension("");
  if (_access(pth,0)!=0) pth=n+".tex";
  Pathname mask;
  mask.SetDirectory(pth);
  CFileFind fnd;
  if (fnd.FindFile(mask))
  {
    COpenQDlg dlg;
    int mode=dlg.DoModal();
    if (mode==IDCANCEL) return false;
    if (mode==IDC_IGNORE) return true;
    Pathname src(n);
    Pathname trg;
    trg.SetDirectory(config->GetString(IDS_CFGPATHFORTEX));
    while (fnd.FindNextFile()) if (!fnd.IsDots() && !fnd.IsDirectory())
    {
      src.SetFilename(fnd.GetFileName());
      trg.SetFilename(fnd.GetFileName());
      if (mode==IDC_UPDATENONEXISTS)
        CopyFile(src,trg,TRUE);
      if (mode==IDC_UPDATENEWER)
        if (FileNewer(src,trg)) 
          CopyFile(src,trg,FALSE);
      if (mode==IDC_UPDATEALL)
        CopyFile(src,trg,FALSE);
    }
  }
  return true;
}










void CObjektiv2Doc::OnStructCoranim() 
{
  /*  CFileDialogEx fdlg(TRUE, WString(IDS_RTMDEFEXT), NULL,OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, WString(IDS_RTMFILTER));
  if (fdlg.DoModal()==IDCANCEL) return;
  ObjectData *memory=LodData->Level(LodData->FindLevelExact(1e15));
  ObjectData *graph=LodData->Level(LodData->FindLevel(0.5f));
  if (memory==NULL || graph==NULL) return;
  memory->AutoAnimation(fdlg.GetPathName());
  graph->AutoAnimation(fdlg.GetPathName());
  FaceT& proxy=FindSelFace(memory->GetNamedSel("proxy"),memory);
  HVECTOR vx={0,0,0,1};
  for (int i=1;i<memory->NAnimations();i++)
  {
  memory->UseAnimation(i);
  LPHVECTOR vc=GetFaceNormal(proxy,memory);
  SoucetVektoru(vx,vc);
  }
  NormalizeVector(vx);
  float alpha=(float)atan2(vx[XVAL],-vx[ZVAL]);
  float beta=(float)atan2(vx[YVAL],-vx[ZVAL]);
  PosT& osamir=FindSelPoint(memory->GetNamedSel("osa mireni"),memory);
  PosT& osamir2=FindSelPoint(memory->GetNamedSel("osa mireni2"),memory);
  Selection *aiming=graph->GetNamedSel("*aiming");
  Selection *aiming2=graph->GetNamedSel("*aiming2");
  TMATRIXSTACK mx1,mx2;
  mxsInitStack(&mx1);
  mxsInitStack(&mx2);
  ObjectData *x;
  for (int k=0;k<2;k++)
  {
  if (k) x=graph;else x=memory;}
  for (i=1;i<x->NAnimations();i++)
  {
  x->UseAnimation(i);
  Translace(mxsPushR(&mx1),-osamir[0],-osamir[1],-osamir[2]);
  Translace(mxsPushR(&mx2),-osamir2[0],-osamir2[1],-osamir2[2]);
  RotaceY(mxsPushR(&mx1),-alpha);
  RotaceX(mxsPushR(&mx2),-beta);
  Translace(mxsPushR(&mx1),osamir[0],osamir[1],osamir[2]);
  Translace(mxsPushR(&mx2),osamir2[0],osamir2[1],osamir2[2]);
  for (int j=0;j<x->NPoints();j++) 
  {
  PosT& ps=x->Point(j);
  if (aiming->PointSelected(j))
  {
  float w=aiming->PointWeight(j);
  HVECTOR vv;
  TransformVector(mxsTop(&mx1),mxVector3(ps[0],ps[1],ps[2]),vv);
  CorrectVectorM(vv);
  ps[0]=ps[0]+(vv[0]-ps[0])*w;
  ps[1]=ps[1]+(vv[1]-ps[1])*w;
  ps[2]=ps[2]+(vv[2]-ps[2])*w;
  }
  if (aiming2->PointSelected(j))
  {
  float w=aiming2->PointWeight(j);
  HVECTOR vv;
  TransformVector(mxsTop(&mx2),mxVector3(ps[0],ps[1],ps[2]),vv);
  CorrectVectorM(vv);
  ps[0]=ps[0]+(vv[0]-ps[0])*w;
  ps[1]=ps[1]+(vv[1]-ps[1])*w;
  ps[2]=ps[2]+(vv[2]-ps[2])*w;
  }
  }
  mxsFreeStack(&mx1);
  mxsFreeStack(&mx2);
  }
  rename(fdlg.GetPathName(),Pathname(fdlg.GetPathName())+".bak");
  graph->ExportAnimationLooped(fdlg.GetPathName());*/
}

























static bool TestSelectionExists(LODObject *obj, const char *selname)
{
  for (int i=0;i<obj->NLevels();i++)
    if (obj->Level(i)->FindNamedSel(selname)!=-1) return true;
  return false;
}










BOOL CObjektiv2Doc::OnFileMerge(UINT cmd) 
{
  CString defext;defext.LoadString(IDS_FILEDEFEXT);
  CString filters;filters.LoadString(IDS_FILEOPENFILTERS);
  CFileDialogEx fdlg(TRUE,defext,NULL,OFN_CREATEPROMPT|OFN_LONGNAMES,
    filters,NULL);
  if (fdlg.DoModal()!=IDOK) return TRUE;
  LODObject *obj=new LODObject();
  if (obj->Load(Pathname(fdlg.GetPathName()),NULL,NULL))
  {
    AfxMessageBox(IDS_LOADUNSUCCESFUL);
  }
  else
  {
    int reply;
    if (cmd!=ID_FILE_MERGEASSELECTION)
      reply=AfxMessageBox(IDS_ASKMERGECREATELODS,MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON1);
    else
      reply=IDNO;
    CString fname;
    const char *selname;
    if (cmd==ID_FILE_MERGEASSELECTION) 
    {
      fname=fdlg.GetFileName();
      for (int i=2;TestSelectionExists(LodData,fname);i++) 
        fname.Format(fdlg.GetFileName()+"%d",i);
      fname="-"+fname;
      selname=fname;
    }
    else
      selname=NULL;
    comint.Begin(WString(IDS_PASTETEXT));
    comint.Run(new CIMergeAll(obj,reply==IDYES,selname));
    UpdateAllToolbars();
    UpdateAllViews(NULL,0);
  }
  return TRUE;
}






















#include "3dsExport.h"

void CObjektiv2Doc::OnFileExport3dstudio() 
{
//#ifndef PUBLIC // enabled in public tools
  static C3dsExport expdlg;
  expdlg.odata=LodData->Active();
  expdlg.DoModal();
//#endif
}










void CObjektiv2Doc::OnViewCenterzoomAuto() 
{
  POSITION pos=GetFirstViewPosition( );
  while (pos!=NULL)
  {
    CObjektiv2View *w=(CObjektiv2View *)GetNextView(pos);
    w->SendMessage(WM_COMMAND,ID_VIEW_PAGEZOOM,0);
  }
}










void CObjektiv2Doc::OnEditSelectoneside() 
{
  comint.Begin(WString(IDS_INVERTSEL));
  comint.Run(new CISelOperation(CISELOP_ONESIDE,false));		
  UpdateAllViews(NULL);
  UpdateAllToolbars(2);
}










class CIMergeNear:public CICommand
{
  float mergvalue;
public:
  CIMergeNear(istream &str);
  CIMergeNear(float val);
  virtual int Execute(LODObject *obj);
  virtual int Tag() const 
  {return CITAG_MERGENEAR;}
  virtual void Save(ostream &str);
};










CIMergeNear::CIMergeNear(istream &str)
{
  datard(str,mergvalue);
}










CIMergeNear::CIMergeNear(float val):mergvalue(val) 
{}










int CIMergeNear::Execute(LODObject *lod)
{
  ObjectData *obj=lod->Active();
  comint.sectchanged=true;
  obj->MergePoints(mergvalue,true);
  return 0;  
}










void CIMergeNear::Save(ostream &str)
{
  datawr(str,mergvalue);
}










CICommand* CIMergeNear_Create(istream &str)
{
  return new CIMergeNear(str);
}










#include "MergeNearDlg.h"

void CObjektiv2Doc::OnPointsMergenear() 
{
  static CMergeNearDlg dlg;
  dlg.obj=LodData->Active();
  if (dlg.DoModal()==IDCANCEL) return;
  comint.Begin(WString(IDS_MERGENEAR));
  comint.Run(new CIMergeNear(dlg.distance));
  UpdateAllViews(NULL);
  frame->_dlguveditor.UpdateFromDocument(this);
}










#include "SplitHoleDlg.h"

void CObjektiv2Doc::OnFacesSplithole() 
{
  static CSplitHoleDlg dlg;
  if (dlg.DoModal()==IDCANCEL) return;
  comint.Begin(WString(IDS_SPLITHOLE));
  comint.Run(new CISplitHole(dlg.mode,dlg.points,dlg.direction));
  UpdateAllViews(NULL);
}










void CObjektiv2Doc::OnViewUncolorize() 
{
}









void CObjektiv2Doc::UpdateModel()
{
  if (LodData->Active()->NNormals()<1)
    //LodData->Active()->DoRecalcNormals();
  {bool dirty=LodData->Dirty();
  RecalcNormals2(LodData->Active());
  if (!dirty) LodData->ClearDirty();
  }
  if (frame->_externalViewer.IsReady()==ExternalViewer::ErrOK)
  {
    frame->_externalCfg.pin=Vector3(pin[0],pin[1],pin[2]);    
    frame->_externalCfg.freezeAnim=frozenanim;
    ExternalViewer::ErrorEnum err=frame->_externalViewer.Update(LodData,filename,5000);
    frame->ExternalError(err);
    extupdate=false;
  }
  UpdateStatus();
  int act=config->GetInt(IDS_CFGAUTOSAVEACT);
  int cur=comint.GetActionCounter(act);
  if (act && cur>=act)
    Autosave();
  comint.AckNotify();
}










class CIMoveFaces:public CICommand
{
  int idcommand;
public:
  CIMoveFaces(int cmd):idcommand(cmd) 
  {};
  CIMoveFaces(istream& str) 
  {datard(str,idcommand);}
  virtual int Execute(LODObject *obj);
  virtual int Tag() const 
  {return CITAG_MOVEFACES;}
  virtual void Save(ostream &str) 
  {datawr(str,idcommand);}
};










int CIMoveFaces::Execute(LODObject *lod)
{
  ObjectData *obj=lod->Active();
  int cnt=obj->NFaces();
  FaceT *fc=new FaceT[cnt];
  int *midxs=new int[cnt];
  bool *selects=new bool[cnt];
  int i,a,b;
  for (i=0;i<cnt;i++)
  {
    fc[i]=FaceT(obj,i);
    selects[i]=obj->FaceSelected(i);
  }
  switch (idcommand)
  {
  case ID_FACES_MOVETOP:
    a=0;
    for (i=0;i<cnt;i++) if (!selects[i])
    {
      fc[i].CopyFaceTo(a);obj->FaceSelect(a,false);midxs[a]=i;a++;
    }
    for (i=0;i<cnt;i++) if (selects[i])
    {
      fc[i].CopyFaceTo(a);obj->FaceSelect(a,true);midxs[a]=i;a++;
    }
    break;
  case ID_FACES_MOVEBOTTOM:
    a=0;
    for (i=0;i<cnt;i++) if (selects[i])
    {
      fc[i].CopyFaceTo(a);obj->FaceSelect(a,true);midxs[a]=i;a++;
    }
    for (i=0;i<cnt;i++) if (!selects[i])
    {
      fc[i].CopyFaceTo(a);obj->FaceSelect(a,false);midxs[a]=i;a++;
    }
    break;
  case ID_FACES_MOVETONEXTALPHA:
    a=0;b=-1;
    for (i=0;i<cnt;i++) if (!selects[i])
    {
      fc[i].CopyFaceTo(a);obj->FaceSelect(a,false);midxs[a]=i;a++;      
      if (strstr(fc[i].GetTexture(),".paa")!=NULL && b!=-1)
      {
        for (int j=b;j<i;j++) if (selects[j])
        {
          fc[j].CopyFaceTo(a);obj->FaceSelect(a,true);midxs[a]=j;a++;          
        }
        b=-1;
      }
    }
    else if (b==-1) b=i;		
    if (b!=-1)
      for (int j=b;j<i;j++) if (selects[j])
      {
        fc[j].CopyFaceTo(a);obj->FaceSelect(a,true);midxs[a]=j;a++;        
      }
      break;
  case ID_FACES_MOVETOPREVALPHA:
    a=cnt-1;b=-1;
    for (i=cnt-1;i>=0;i--) if (!selects[i])
    {
      fc[i].CopyFaceTo(a);obj->FaceSelect(a,false);midxs[a]=i;a--;      
      if (strstr(fc[i].GetTexture(),".paa")!=NULL && b!=-1)
      {
        for (int j=b;j>i;j--) if (selects[j])
        {
          fc[j].CopyFaceTo(a);obj->FaceSelect(a,true);midxs[a]=i;a--;          
        }
        b=-1;
      }
    }
    else if (b==-1) b=i;
    if (b!=-1)
      for (int j=b;j>i;j--) if (selects[j])
      {
        fc[j].CopyFaceTo(a);obj->FaceSelect(a,true);midxs[a]=j;a--;        
      }
      break;
  }
  for (i=-2;i<MAX_NAMED_SEL;i++)
  {Selection *sel;
  if (i==-2)
    sel=(Selection *)obj->GetLocked();
  else if (i==-1)
    sel=(Selection *)obj->GetHidden();
  else
    sel=(Selection *)obj->GetNamedSel(i);
  if (sel)
  {
    Selection save(*sel);
    for (int j=0;j<cnt;j++)
      sel->FaceSelect(j,save.FaceSelected(midxs[j]));
  }
  }
  delete [] fc;
  delete [] selects;
  delete [] midxs;
  return 0;
}










CICommand *CIMoveFaces_Create(istream &str)
{
  return new CIMoveFaces(str);
}










void CObjektiv2Doc::OnFacesMove(UINT cmd)
{  
  comint.Begin(WString(IDS_MOVEFACES));
  comint.Run(new CIMoveFaces(cmd));
  UpdateAllViews(NULL);
}









/*
void CObjektiv2Doc::OnSelopClear() 
{
  combine_saved->Clear();
  this->automapsel=false;
  frame->seloperator=ID_SELOP_NONE;
}










void CObjektiv2Doc::OnSelopCombine() 
{
;  ObjectData *act=LodData->Active();
  Selection *use;
  this->automapsel=false;
  use=CombineSelection(frame->seloperator,combine_saved,(Selection *)act->GetSelection());
  if (use!=combine_saved) *combine_saved=*use;
  else
  {
    use=new Selection(*use);
    comint.Begin(WString(IDS_COMBINESELECTIONS));
    comint.Run(new CISelect(use));
  }
  UpdateAllViews(NULL);
}









*/
#include "MultPinCullDlg.h"

void CObjektiv2Doc::OnStructureMultiplepincull() 
{
  bool first=true;
  CMultPinCullDlg dlg;
  dlg.obj=LodData->Active();
  if (dlg.DoModal()==IDCANCEL || dlg.selection=="") return;
  Selection sel(*dlg.obj->GetNamedSel(dlg.selection));
  int cnt=dlg.obj->NPoints();
  Selection *out=new Selection(dlg.obj);
  for (int i=0;i<cnt;i++)
  {
    if (sel.PointSelected(i))
    {
      PosT &ps=dlg.obj->Point(i);
      dlg.obj->BackfaceCull(ps);
      if (first) (*out)=*(dlg.obj->GetSelection());
      else
      {
        int j;
        int fcnt=dlg.obj->NFaces();
        for (j=0;j<fcnt;j++) if (!dlg.obj->FaceSelected(j)) out->FaceSelect(j,false);
        for (j=0;j<cnt;j++) if (!dlg.obj->PointSelected(j)) out->PointSelect(j,false);
      }
      first=false;	  
    }
  }
  comint.Begin(WString(IDS_PINCULL));
  comint.Run(new CISelect(out));
  UpdateAllViews(NULL);
}







class FunctorSaveDocument
{
  const char *name;
public:
  FunctorSaveDocument(const char *name):name(name) {}
  bool operator ()(SRef<PluginGeneralAppSide> &plugin) const
  {
    plugin->GetPlugin().AfterSaveDocument(name);
    return false;
  }
};


BOOL CObjektiv2Doc::OnSaveDocument(LPCTSTR lpszPathName) 
{
  for (int i=0;i<LodData->NLevels();i++) RecalcNormals2(LodData->Level(i));
  Pathname src(lpszPathName),bak(lpszPathName);
  bak.SetExtension(".backup");
  if (_access(src,0)!=-1 && MoveFile(src,bak)==FALSE)
  {
    AfxMessageBox(IDS_SAVEUNSUCCESFUL);	
    return FALSE;	
  }
  if (LodData->Save(src,OBJDATA_LATESTVERSION,false,SaveOxySetup,NULL) || _access(src,0)==-1)
  {
    AfxMessageBox(IDS_SAVEUNSUCCESFUL);	
    MoveFile(bak,src);
    return FALSE;
  }
  DeleteFile(bak);
  SetPathName(lpszPathName, true);
  filename=lpszPathName;
  lastP3Dsize=GetFileSizeByFilename(lpszPathName);
  LodData->ClearDirty();
  theApp.ForEachPlugin(FunctorSaveDocument(lpszPathName));
  return TRUE;
}










BOOL CObjektiv2Doc::AskSave()
{
  if (GetPathName()=="") return AskSaveAs();
  if (GetPathName()=="") return FALSE;
  ROCheckResult sug=theApp.rocheck.TestFileRO(GetPathName(),0);
  if (sug==ROCHK_FileSaveAs) return AskSaveAs();
  if (sug==ROCHK_FileRO) return FALSE;
  Pathname name(GetPathName());
  return OnSaveDocument(name);
}










BOOL CObjektiv2Doc::AskSaveAs()
{
  CString defext;defext.LoadString(IDS_FILEDEFEXT);
  CString filters;filters.LoadString(IDS_FILESAVEFILTERS);
  CFileDialogEx fdlg(FALSE,defext,Pathname::GetNameFromPath(GetPathName()),OFN_PATHMUSTEXIST|OFN_LONGNAMES|OFN_OVERWRITEPROMPT,
    filters,NULL);
  config->FillOpenDialogByPath(IDS_CFGSAVEASFOLDER,fdlg.m_ofn,GetPathName());
  if (fdlg.DoModal()!=IDOK) return FALSE;
  SetPathName(filename=fdlg.GetPathName(),true);  
  frame->m_wndMatLib.SetModelPath(filename);
  config->SaveCurrPath(IDS_CFGSAVEASFOLDER,fdlg.GetPathName());
  frame->m_wndLodList.UpdateListOfLods(LodData);  
  return AskSave();
}










#include "ICreateProxy.h"

void CObjektiv2Doc::OnCreateProxy() 
{
  CProxyNameDlg dlg;
  dlg.vNoId=true;
  if (dlg.DoModal()==IDOK)
  {
    comint.Begin(WString(IDS_CREATEPROXY));
    comint.Run(new CICreateProxy(gismo,dlg.Name));
    //    if (DefineProxy(LodData->Active(),"$$$$PROXY$$$$")==false) comint.DoUndo();
    UpdateAllViews(NULL);
    UpdateAllToolbars(1);
  }
}










void CObjektiv2Doc::OnFileSavetoserver() 
{
#ifdef FULLVER
  Pathname src(GetPathName());
  src.SetDirectory(config->GetString(IDS_CFGSERVERPATH));
  if (_access(src,0)!=-1)
  {
    if (AfxMessageBox(WString(IDS_ASKOVERWRITE,src),MB_YESNO)==IDNO) return;
  }
  if (LodData->Save(src,OBJDATA_LATESTVERSION,false,SaveOxySetup,NULL) || _access(src,0)==-1)
    AfxMessageBox(IDS_SAVEUNSUCCESFUL);			
  else
    CMessageWnd::ShowMessage(WString(IDS_OBJECTSAVED));
#else
  AfxMessageBox("Save to server is not supported in Light version!");
#endif
}










void CObjektiv2Doc::OnUpdateFileSavetoserver(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(_access(config->GetString(IDS_CFGSERVERPATH),0)!=-1);	
}



















#include "FlattenPointsDlg.h"
#include "IFlattenPoints.h"

void CObjektiv2Doc::OnPointsFlatten() 
{
  CFlattenPointsDlg dlg;
  dlg.pin=pinlock;
  if (dlg.DoModal()==IDCANCEL) return;
  LPHVECTOR plane;
  HVECTOR front=
  {0,0,1,0};
  HVECTOR left=
  {1,0,0,0};
  HVECTOR top=
  {0,1,0,0};
  switch (dlg.mode)
  {
  case 0:plane=front;break;
  case 1:plane=left;break;
  case 2:plane=top;break;
  case 3:
    {  
      HMATRIX mm,im;
      HVECTOR v1,v2,v3,tv1,tv2,tv3;
      CopyMatice(mm,CObjektiv2View::GetCurrentView()->camera.GetTransform());
      InverzeMatice(mm,im);
      v1[XVAL]=0.0f;v1[YVAL]=0.0f;v1[ZVAL]=0.0f;v1[WVAL]=1.0f;
      v2[XVAL]=300.0f;v2[YVAL]=0.0f;v2[ZVAL]=0.0f;v2[WVAL]=1.0f;
      v3[XVAL]=300.0f;v3[YVAL]=300.0f;v3[ZVAL]=0.0f;v3[WVAL]=1.0f;
      TransformVector(im,v1,tv1);
      TransformVector(im,v2,tv2);
      TransformVector(im,v3,tv3);
      CorrectVector(tv1);
      CorrectVector(tv2);
      CorrectVector(tv3);
      plane=calcFacePlane(tv1, tv2, tv3);
    }	  
  }
  plane[WVAL]=1;
  NormalizeVector(plane);
  HVECTOR center;
  if (dlg.pin)
    CopyVektor(center,pin);
  else
    GetObjectCenter(true, center, LodData->Active());
  plane[WVAL]=-(plane[XVAL]*center[XVAL]+plane[YVAL]*center[YVAL]+plane[ZVAL]*center[ZVAL]);
  comint.Begin(WString(IDS_FLATTENPOINTS));
  comint.Run(new CIFlattenPoints(plane));
  UpdateAllViews(NULL);
}










#include "IToolNormalAnim.h"
#include "NormAnimDlg.h"

void CObjektiv2Doc::OnToolsAnimnorm() 
{
#ifdef FULLVER
  CNormAnimDlg dlg;
  dlg.lod=LodData;
  if (dlg.DoModal()==IDOK)
  {
    comint.Begin(WString(IDS_TOOLANIMNORM));
    comint.Run(new CIToolNormalAnim(dlg.flod,dlg.selection,dlg.selonly!=0));
  }
#else
  AfxMessageBox("This is not supported in Light version!");
#endif
}










#include "Import3DS.h"

void CObjektiv2Doc::OnFileImport3ds() 
{
  CFileDialogEx  fdlg(TRUE,NULL,NULL,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,WString(IDS_3DSIMPORTFILTER));
  if (fdlg.DoModal()==IDOK)
  {    
    static BOOL imp_noanim=TRUE;
    static BOOL imp_mattexname=TRUE;
    static float imp_scalemag=1;
    static float imp_scalemin=1000;
    static BOOL imp_convtex=TRUE;
    CImport3DSDlg dlg;
    dlg.noanim=imp_noanim;
    dlg.mattexname=imp_mattexname;
    dlg.scalemag=imp_scalemag;
    dlg.scalemin=imp_scalemin;
    dlg.convtex=imp_convtex;    
    dlg.f3ds=fdlg.GetPathName();
    dlg.lod=LodData;
    if (dlg.DoModal()==IDOK)
    {
      comint.SetObject(LodData);
      comint.LoadToListbox();
      UpdateAllToolbars(0);
      LoadEdges();
      comint.sectchanged=true;
      UpdateAllViews(NULL);

      imp_noanim=dlg.noanim;
      imp_mattexname=dlg.mattexname;
      imp_scalemag=dlg.scalemag;
      imp_scalemin=dlg.scalemin;
      imp_convtex=dlg.convtex;      
    }
  }
}










#include "CopyWeightsDlg.h"

void CObjektiv2Doc::OnToolsCopyweights() 
{
#ifdef FULLVER
  CCopyWeightsDlg dlg;
  dlg.lod=LodData;
  dlg.DoModal();
  UpdateAll();
  UpdateAllToolbars(1);
#endif
}










void CObjektiv2Doc::OnSurfacesShowgismo() 
{
  if (gismo_mode==gismo_hide || localAxisMode) gismo_mode=gismo_show;else gismo_mode=gismo_hide;
  if (gismo_mode!=gismo_hide) this->pinlock=true;
  localAxisMode=false;
  UpdateAllViews(NULL);
}










void CObjektiv2Doc::OnSurfacesEditgismo() 
{
  if (gismo_mode==gismo_selected || localAxisMode) gismo_mode=gismo_show;else gismo_mode=gismo_selected;
  this->pinlock=true;
  localAxisMode=false;
  UpdateAllViews(NULL);
}










void CObjektiv2Doc::OnSurfacesResetgismo() 
{
  CopyMatice(gslast,gismo);
  HVECTOR p;
  CopyVektor(p,pin);
  JednotkovaMatice(gismo);
  CopyVektor(pin,p);
  UpdateAllViews(NULL);
}










void CObjektiv2Doc::OnSurfacesUndogizmo() 
{
  UndoGizmo();
  UpdateAllViews(NULL);
}










void CObjektiv2Doc::OnSurfacesGizmomapping() 
{
  if (comint.IsLocked()) return;
  CGizmoMapDlg dlg;
  EGismo curmode=gismo_mode;
  bool curpinlock=pinlock;

  comint.Lock(true);
  ObjectData &obj=*(LodData->Active());
  ObjectData save(obj);
  int n=obj.NPoints();
  int i;
  for ( i=0;i<n;i++) obj.PointSelect(i,false);
  gismo_mode=gismo_selected;
  pinlock=true;
  UpdateAllViews(NULL);
  int result=dlg.DoModal();
  obj=save;
  comint.Lock(false);
  gismo_mode=curmode;
  pinlock=curpinlock;
  if (result==IDOK)
  {
    comint.Begin(WString(IDS_GIZMOMAPPING));
    comint.Run(new CIGizmoMap(doc->gismo,dlg.nfo));
  }
  UpdateAllViews(NULL);
}










#include "MDAExport.h"

void CObjektiv2Doc::OnFileExportMda() 
{
//#ifndef PUBLIC // enabled in public tools
  CFileDialogEx dlg(FALSE,"mda",NULL,	OFN_OVERWRITEPROMPT|OFN_PATHMUSTEXIST,"MDA files|*.MDA||");
  if (dlg.DoModal()==IDCANCEL) return;
  if (SaveMDA(LodData->Active(),dlg.GetPathName())==false)
    AfxMessageBox("Error while exporting MDA file");
//#endif
}










void CObjektiv2Doc::OnToolsFind3weights() 
{
  ObjectData *obj=LodData->Active();
  Selection sel(obj);
  int i,j,cnt=obj->NPoints();
  bool first=true;
  for (i=0;i<cnt;i++) if (obj->PointSelected(i))
  {
    int selcnt=0;
    for (j=0;j<MAX_NAMED_SEL;j++) if (obj->GetNamedSel(j))  
      if (obj->GetNamedSel(j)->PointSelected(i)) selcnt++;	  
    if (selcnt>3) 
    {sel.PointSelect(i);}
  }
  if (sel.NPoints()==0)
  {
    AfxMessageBox(IDS_NOPOINTSFOUND);
  }
  else
  {	
    comint.Begin(WString(IDS_MAKESELECTION));
    comint.Run(new CISelect(new Selection(sel)));
    comint.Run(new CISimpleCommand(CIS_FACESFROMPOINTS ));
    UpdateAllViews(NULL);
  }
}










#include "WeightAutoDlg.h"

void CObjektiv2Doc::OnToolsWeightautomation() 
{
  CWeightAutoDlg dlg;
  if (dlg.DoModal()==IDOK)
  {
    WeightAuto(LodData,dlg.filename,dlg.level,dlg.findbones!=FALSE);
    UpdateAll();
    UpdateAllToolbars();
  }
}










#include "IToolAdaptaceAnimace.h"
#include "VyberLodDlg.h"


void CObjektiv2Doc::OnToolsAdaptovaniAnimace()
{
#ifdef FULLVER
  CVyberLodDlg dlg;
  dlg.lodobj=LodData;
  if (dlg.DoModal()==IDOK)
  {
    comint.Begin(WString("Adaptace animace"));
    comint.Run(new CIToolAdaptaceAnimace(dlg.sellod));
    UpdateAllViews(NULL);
    UpdateAllToolbars();
    comint.SetLongOp();
  }
#else
  AfxMessageBox("This is not supported in Light version!");
#endif

}










void CObjektiv2Doc::OnFileExportOldVersion()
{
//#ifndef PUBLIC // enabled in public tools
  CString defext;defext.LoadString(IDS_FILEDEFEXT);
  CString filters;filters.LoadString(IDS_FILESAVEFILTERS);
  CFileDialogEx fdlg(FALSE,defext,Pathname::GetNameFromPath(GetPathName()),OFN_PATHMUSTEXIST|OFN_LONGNAMES|OFN_OVERWRITEPROMPT,
    filters,NULL);
  config->FillOpenDialogByPath(IDS_CFGSAVEASFOLDER,fdlg.m_ofn,GetPathName());
  if (fdlg.DoModal()!=IDOK) return ;
  config->SaveCurrPath(IDS_CFGSAVEASFOLDER,fdlg.GetPathName());
  frame->m_wndLodList.UpdateListOfLods(LodData);
  Pathname src(fdlg.GetPathName()),bak(fdlg.GetPathName());
  bak.SetExtension(".backup");
  if (_access(src,0)!=-1 && MoveFile(src,bak)==FALSE)
  {
    AfxMessageBox(IDS_SAVEUNSUCCESFUL);	
    return;	
  }
  if (LodData->Save(src,0,false,SaveOxySetup,NULL) || _access(src,0)==-1)
  {
    AfxMessageBox(IDS_SAVEUNSUCCESFUL);	
    MoveFile(bak,src);
    return;
  }
  DeleteFile(bak);
  return;
//#endif
}










#include "ASFHierarchy.h"
#include "ASFImportDlg.h"

void CObjektiv2Doc::OnFileImportASFAMC()
{
//#ifndef PUBLIC // enabled in public tools
  CFileDialogEx  fdlg(TRUE,NULL,NULL,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,WString(IDS_ASFIMPORTFILTER));
  if (fdlg.DoModal()==IDOK)
  {
    CASFHierarchy asf;
    CString err;
    if (asf.ReadASF(fdlg.GetPathName(),err)==false)
    {
      CString p;
      AfxFormatString2(p,IDS_ASFIMPORTERROR,fdlg.GetFileName(),err);
      AfxMessageBox(p,MB_OK|MB_ICONEXCLAMATION);
      return;
    }
    CASFImportDlg idlg;
    idlg.asf=&asf;
    if (idlg.DoModal()==IDOK)
    {
      CFileDialogEx  famc(TRUE,NULL,NULL,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,WString(IDS_AMCIMPORTFILTER));
      if (famc.DoModal()==IDOK)
      {
        if (asf.ImportToModel(*(LodData->Active()),famc.GetPathName(),err)==false)
        {
          CString p;
          AfxFormatString2(p,IDS_ASFIMPORTERROR,fdlg.GetFileName(),err);
          AfxMessageBox(p,MB_OK|MB_ICONEXCLAMATION);
        }
        UpdateAllViews(NULL);
        UpdateAllToolbars();
        comint.SetObject(LodData);
      }
    }
  }
//#endif
}










#include "BVHImport.h"






void CObjektiv2Doc::OnFileImportBVH()
{
//#ifndef PUBLIC // enabled in public tools
  CFileDialogEx  fdlg(TRUE,NULL,NULL,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,WString(IDS_BVHIMPORTFILTER));
  if (fdlg.DoModal()==IDOK)
  {
    CBVHImport import;
    ifstream fin(fdlg.GetPathName(),ios::in|ios::binary);
    if (!fin)
    {AfxMessageBox(IDS_LOADUNSUCCESFUL,MB_OK);return;}
    import.SetCallback(BVHImportModel,LodData->Active());
    import.SetSource(&fin);
    int errline;
    int err=import.Parse(&errline);
    if (err)
      AfxMessageBox(WString(IDS_BVHIMPORTFAILED,fdlg.GetFileName(),import.GetError(err),errline),MB_OK|MB_ICONSTOP);
    comint.SetObject(LodData);	
    LodData->Active()->GetTool<ObjToolAnimation>().AutoTimeAnimations(true);
    UpdateAllViews(NULL);
    UpdateAllToolbars(0);
  }
//#endif
}
















void CObjektiv2Doc::OnStructureSquarizealllods() 
{
  comint.Begin(WString(IDS_SQUARIZEALL));
  float active=LodData->Resolution(LodData->ActiveLevel());
  comint.Run(new CISimpleCommand(CIS_TRIANGULATE));
  for (int i=0;i<LodData->NLevels();i++)
  {
    comint.Run(new CILodSetActive(LodData->Resolution(i)));
    comint.Run(new CISelOperation(CISELOP_SELALL,true));
    comint.Run(new CISimpleCommand(CIS_TRIANGULATE));
    comint.Run(new CISimpleCommand(CIS_SQUARIZE));
  }
  comint.Run(new CILodSetActive(active));
  UpdateAllToolbars(0);
  UpdateAllViews(NULL);
}










void CObjektiv2Doc::OnStructureGhostsShow() 
{
  showghost=!showghost;  	
  if (showghost==false) selghost=false;
}










void CObjektiv2Doc::OnUpdateStructureGhostsShow(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(showghost);
}










void CObjektiv2Doc::OnStructureGhostsSelect() 
{
  selghost=!selghost;    		
  if (selghost)
  {
    showghost=true;
    OnEditDeselect();
  }
}










void CObjektiv2Doc::OnUpdateStructureGhostsSelect(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(selghost);  
}










void CObjektiv2Doc::OnStructureGhostMaterialize() 
{

}










void CObjektiv2Doc::OnUpdateStructureGhostMaterialize(CCmdUI* pCmdUI) 
{

}










void CObjektiv2Doc::OnStructureGhostCreate() 
{
  JednotkovaMatice(ghost);
  selghost=true;
  showghost=true;
  OnEditDeselect();
}










#include "DlgTexelMeasure.h"

void CObjektiv2Doc::OnSurfacesTexelcalculations() 
{
  CDlgTexelMeasure dlg;
  dlg.source=LodData->Active();
  dlg.DoModal();
  Selection *sel=dlg.selout;
  if (sel)
  {
    comint.Begin(WString(IDS_SELECTUSING,""));
    comint.Run(new CISelect(sel));
    comint.Run(new CISimpleCommand(CIS_POINTSFROMFACES));
    UpdateAllViews(NULL);
  }
}










#include "DlgNormalDirection.h"
void CObjektiv2Doc::OnPointsLocknormals() 
{
  CDlgNormalDirection *dlg=new	CDlgNormalDirection;
  dlg->vObject=LodData->Active();
  int p=dlg->DoModal();
  if (p==IDCANCEL)
  {
    delete dlg;
    return;
  }
  if (p==IDOK)
  {
    comint.Begin(WString(IDS_NORMALDIRECTION));
    comint.Run(new CINormalDirection(dlg));
  }
  else if (p==IDC_UNLOCKNORMALS)
  {
    comint.Begin(WString(IDS_UNLOCKNORMALS));
    comint.Run(new CINormalDirection(NULL));
    delete dlg;
  }
  UpdateAllViews(NULL);
}










void CObjektiv2Doc::OnFileImportMayaweightscript() 
{
//#ifndef PUBLIC // enabled in public tools
  CFileDialogEx  fdlg(TRUE,NULL,NULL,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,WString(IDS_MAYAWEIGHTMAP));
  if (fdlg.DoModal()==IDOK)
  {
    ObjectData *obj=LodData->Active();
    ifstream file;
    char buff[256];
    file.open(fdlg.GetPathName(),ios::out);
    comint.Begin(WString(IDS_MAYAWEIGHTS));
    while (!file.eof())
    {
      ws(file);
      buff[0]=(char) file.get();
      if (buff[0]=='/' && file.peek()=='/') 
      {
        file.ignore(0x7fffffff,'\n');
        continue;
      }
      file.getline(buff+1,256,'\t');
      if (buff[0]==0) break;
      Pathname pth=fdlg.GetPathName();
      pth.SetFilename("");
      pth+=fdlg.GetFileTitle();
      ws(file);
      buff[0]=0;
      file.getline(buff,256,'\t');
      if (buff[0]==0) 
      {
        AfxMessageBox(IDS_MAYAWEIGHTS_MISINGSELECTIONNAME);
        break;
      }
      NamedSelection *sel=new NamedSelection(obj,buff);
      buff[0]=0;
      file.getline(buff,256,'\n');
      if (buff[0]==0)
      {
        AfxMessageBox(IDS_MAYAWEIGHTS_MISINGMAPNAME);
        delete sel;
        break;
      }
      pth.SetFilename(buff);
      TUNIPICTURE *pic=::cmLoadUni(const_cast<char *>(pth.GetFullPath()));      
      if (pic==NULL)
      {
        CString text;
        AfxFormatString1(text,IDS_MAYAWEIGHTS_MAPLOADERROR,buff);
        AfxMessageBox(text);
      }
      else
      {
        TUNIPICTURE *cmpic=::cmConvertUni(pic,"I8",1);
        if (cmpic==NULL)
        {
          CString text;
          AfxFormatString1(text,IDS_MAYAWEIGHTS_MAPCONVERROR,buff);
          AfxMessageBox(text);
        }
        else
        {
          free(pic);pic=cmpic;
          for (int p=0;p<obj->NFaces();p++)
          {
            FaceT fc(obj,p);
            for (int q=0;q<fc.N();q++)
            {
              int pt=fc.GetPoint(q);
              float fw;
              float u=fmod(fc.GetU(q),1.0f);
              float v=fmod(fc.GetV(q),1.0f);
              if (u<0.0f) u+=1.0f;
              if (v<0.0f) v+=1.0f;
              int x=ToInt(cmGetXSize(pic)*u-0.5f);
              int y=ToInt(cmGetYSize(pic)*v-0.5f);
              if (x<0) x=0;
              if (y<0) y=0;
              if (x>=(int)cmGetXSize(pic)) x-=cmGetXSize(pic);
              if (y>=(int)cmGetYSize(pic)) y-=cmGetYSize(pic);
              unsigned char w=*((unsigned char *)cmGetData(pic)+x+y*cmGetXlen(pic));
              fw=(float)w/255.0f;
              float gw=sel->PointWeight(pt);
              if (fw>gw) sel->SetPointWeight(pt,fw);
            }
          }
        }
        free(pic);
        ws(file);
      }      
      comint.Run(new CIDefineNamedSelection(sel));
    }
    UpdateAllViews(NULL,0);
    UpdateAllToolbars(1);
  }	
//#endif
}










#include "..\ObjektivLib\BIANMImport.h"
#include "BIANMImportDlg.h"


void CObjektiv2Doc::OnFileImportBianmmayaanimationexport() 
{
//#ifndef PUBLIC // enabled in public tools
  CFileDialogEx  fdlg(TRUE,NULL,NULL,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,WString(IDS_MAYAANIMATIONFILTER));
  if (fdlg.DoModal()==IDOK)
  {
    CBIANMImport anm;
    CBIANMImportDlg dlg;
    ifstream infile(fdlg.GetPathName(),ios::in|ios::binary);
    if (!infile) 
    {AfxMessageBox(IDS_LOADUNSUCCESFUL,MB_OK|MB_ICONEXCLAMATION);return;}
    int errline;
    int error=anm.ParseSkeleton(infile,&errline);
    if (!error)
    {
      if (dlg.DoModal()==IDCANCEL) return;
      ObjectData *obj=new ObjectData;
      ObjectData *objactive=LodData->Active();
      anm.ImportBones(*obj,dlg.vMasterScale,dlg.vInvertX!=FALSE,dlg.vInvertY!=FALSE,dlg.vInvertZ!=FALSE);
      comint.Begin(WString("Import"));
      comint.Run(new CIMerge(obj));
      AnimationPhase *phs;
      phs=new AnimationPhase(objactive);
      phs->Redefine(objactive);
      phs->SetTime(-0.5f);
      anm.LoadAnimationFrame(*objactive,*phs,dlg.vMasterScale,dlg.vInvertX!=FALSE,dlg.vInvertY!=FALSE,dlg.vInvertZ!=FALSE);
      comint.Run(new CIAnimLoadPhase(phs));   
      float timpos=0;    
      while ((error=anm.ParseNextFrame(infile,&errline))==0)
      {
        phs=new AnimationPhase(objactive);
        phs->Redefine(objactive);
        phs->SetTime(timpos);
        anm.LoadAnimationFrame(*objactive,*phs,dlg.vMasterScale,dlg.vInvertX!=FALSE,dlg.vInvertY!=FALSE,dlg.vInvertZ!=FALSE);
        comint.Run(new CIAnimLoadPhase(phs));   
        timpos+=1.0f;
      }
      if (error==-12) error=0;
      comint.Run(new CIAnimAutoTime());
      comint.SetObject(LodData);
    }
    if (error)
    {
      CString s;
      s.Format(IDS_MAYANIMFAILED,fdlg.GetFileName(),anm.GetError(error),errline);
      AfxMessageBox(s,MB_OK|MB_ICONEXCLAMATION);
    }

  }
  UpdateAllViews(NULL);
  UpdateAllToolbars();
//#endif
}










#include "OBJFileImport.h"

void CObjektiv2Doc::OnFileImportObjfile() 
{
  CFileDialogEx  fdlg(TRUE,NULL,NULL,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,WString(IDS_OBJFILTER));
  if (fdlg.DoModal()==IDOK)
  {
    COBJFileImport obj;
    CBIANMImportDlg dlg;
    dlg.enableEdgeDetect=true;
    ifstream infile(fdlg.GetPathName(),ios::in);
    if (!infile) 
    {AfxMessageBox(IDS_LOADUNSUCCESFUL,MB_OK|MB_ICONEXCLAMATION);return;}
    if (dlg.DoModal()==IDCANCEL) return;
    obj.SetScale(dlg.vMasterScale);
    obj.SetInvert(dlg.vInvertX!=FALSE,dlg.vInvertY!=FALSE,dlg.vInvertZ!=FALSE);
    obj.SetAngle(dlg.vAngle);
    ProgressBar<int> progress(0);
    progress.ReportState(IDS_OBJIMPORTSTATUS);
    int errline;
    int error=obj.LoadFile(infile,&errline);
    if (!error)
    {
      comint.Begin(WString("Import"));
      comint.Run(new CIMerge(new ObjectData(obj.GetObject())));
    }
    if (error)
    {
      CString s;
      s.Format(IDS_OBJIMPORTFAILED,fdlg.GetFileName(),obj.GetError(error),errline);
      AfxMessageBox(s,MB_OK|MB_ICONEXCLAMATION);
    }
  }
  LoadEdges();
  UpdateAllViews(NULL);
  UpdateAllToolbars();
  comint.sectchanged=true;
}










void CObjektiv2Doc::OnViewViewersFreezeanimation() 
{
  if (frozenanim==-1)  frozenanim=curanim;
  else frozenanim=-1;
  UpdateAll();
  UpdateModel();
}










void CObjektiv2Doc::OnUpdateViewViewersFreezeanimation(CCmdUI* pCmdUI) 
{
  int nanim=LodData->Active()->NAnimations();
  pCmdUI->Enable(nanim!=0 && curanim>=0);
  pCmdUI->SetCheck(frozenanim>=0 && frozenanim<nanim);
}










#include "IChilliSkinner.h"
#include ".\objektiv2doc.h"

void CObjektiv2Doc::OnSurfacesChilliskinner() 
{  
  int begface=CIChilliSkinner::SelectBeginFaceFromPin(*LodData->Active(),pin);
  comint.Begin(WString(IDS_CHILLISKINNER));
  comint.Run(new CIChilliSkinner(begface));
  UpdateAllViews(NULL,0,0);
}










void CObjektiv2Doc::SelectionTransform(int point, VecT &vector, LPHVECTOR out, float weight)
{  
  if (frame->movebynormals)
  {
    float f=seltr[3][0]+seltr[3][1]+seltr[3][2];
    if (byNormals.Size()==0)
    {
      ObjectData &obj=*LodData->Active();
      CalculateByNormals(obj,byNormals);
    }
    VecT &norm=byNormals[point];
    out[XVAL]=vector[XVAL]+f*norm[XVAL];
    out[YVAL]=vector[YVAL]+f*norm[YVAL];
    out[ZVAL]=vector[ZVAL]+f*norm[ZVAL];
  }
  else
  {
    TransformVector(seltr,mxVector3(vector[0],vector[1],vector[2]),out);
  }
  if  (weight>=0.0f)
  {
    out[XVAL]=vector[0]+(out[XVAL]-vector[0])*weight;
    out[YVAL]=vector[1]+(out[YVAL]-vector[1])*weight;
    out[ZVAL]=vector[2]+(out[ZVAL]-vector[2])*weight;
  }
  out[WVAL]=1.0;
}











void CObjektiv2Doc::OnStructureCarve() 
{
  int csel=LodData->Active()->CountSelectedFaces();
  if (csel==0)
    AfxMessageBox(IDS_CARVINGHELP,MB_OK);
  else
  {
    comint.Begin(WString(IDS_CARVE));
    comint.Run(new CICarving());
    UpdateAllViews(NULL);
  }

}

void CObjektiv2Doc::OnToolsMasstexturematerialrenaming() 
{
  CAllRenameDlg dlg;
  dlg.iCurLod=LodData;
  dlg.DoModal();
  UpdateAllToolbars(0);
}



void CObjektiv2Doc::OnStructureChecktextures() 
{
  int nlods=LodData->NLevels();
  const char *ptrs[256];
  int misscnt=0;
  CString p;
  p=config->GetString(IDS_CFGPATHFORTEX);
  int plen=p.GetLength();
  int fctotal,fccur;
  int i;

  for (i=0,fctotal=0;i<nlods;i++) fctotal+=LodData->Level(i)->NFaces();
  fccur=0;
  ProgressBar<int> progress(fctotal);
  for (i=0;i<nlods;i++)
  {
    ObjectData *obj=LodData->Level(i);
    int nfaces=obj->NFaces();
    for (int j=0;j<nfaces;j++)
    {
      FaceT fc(obj,j);
      fccur++;
      progress.SetNextPos(fccur);
      for (int k=0;k<2;k++)
      {
        const char *file;
        if (k) file=fc.GetTexture(); else file=fc.GetMaterial();
        if (file[0])
        {
          p+=file;
          if (_access(p,0))
          {
            int l;
            for (l=0;l<misscnt;l++)
              if (_stricmp(ptrs[l],file)==0) break;
            if (l==misscnt && misscnt<256)			
              ptrs[misscnt++]=file;			
          }
          p.Delete(plen,p.GetLength()-plen);
        }
      }
    }
  }
  p="";
  if (misscnt)
  {
    for (i=0;i<misscnt;i++)
    {
      p+=ptrs[i];
      p+="\r\n";
    }
    frame->ShowMessageWindow(IDS_MISSINGTEXTURES,p);
  }
  else
    AfxMessageBox(IDS_NOMISSINGTEXTURES,MB_OK|MB_ICONINFORMATION);
}


void CObjektiv2Doc::OnEditUnselectTriangles()
{
  comint.Begin(WString(IDS_UNSELECTTRIANGLES));
  comint.Run(new CISelOperation(CISELOP_UNSELECTTRIANGLES ,true));		
  UpdateAllViews(NULL);
  UpdateAllToolbars(2);
}

void CObjektiv2Doc::OnPointsRemove()
{
  comint.Begin(WString(IDS_REMOVEPOINTS));
  comint.Run(new CIDeletePointsTriangulate());		
  UpdateAllViews(NULL);  
}

void CObjektiv2Doc::OnConvexityFindncpoints()
{
  comint.Begin(WString(IDS_FINDNONCONVEXPOINTS));
  comint.Run(new CIFindNonConvexPoints());
  LoadEdges();
  UpdateAllViews(NULL);  
}

void CObjektiv2Doc::OnConvexityNewconvexhull()
{
  comint.Begin(WString(IDS_NEW_CONVEX_HULL));

  ProgressBar<int>pb(1);
  pb.ReportState(-10);
  pb.AdvanceNext(1);
  comint.Run(new CICreateConvexVolume(GetKeyState(VK_SHIFT)<0));
  if (pb.StopSignaled()) 
      comint.DoUndo();
  else
    comint.Run(new CIMakeConvex());
  LoadEdges();
  UpdateAllViews(NULL);  
}

static const char *CorrectPath(const char *path)
{
  const char *part=config->GetString(IDS_CFGPATHFORTEX);
  const char *c=path;
  while (*part && toupper(*part)==toupper(*c))
  {
    part++;
    c++;
  }
  if (*part==0) return c;
  else return path;
}

static void ConvertTextures(ObjectData &obj)
{
  AutoArray<RString> textures;
  for (int i=0;i<obj.NFaces();i++)
  {
    int cnt=0;
    FaceT fc(obj,i);
    RString oldt=fc.GetTexture();
    const char *ext=strrchr(oldt,'.');
    if (ext && _stricmp(ext,".paa")==1 && _stricmp(ext,".pac")==1)
    {
      int j;
      RString newt=oldt.Substring(0,oldt.GetLength()-strlen(ext))+".paa";            
      newt=CorrectPath(newt);
      for (j=0;j<textures.Size();j++)
        if (textures[j]==newt)
          break;
      if (j==textures.Size())
      {
        if (FileNewer(oldt,newt) && _access(newt,02)==0)
          UpdateTextureF(newt,oldt,-1);      
        textures.Append()=newt;
      }
      fc.SetTexture(textures[j]);      
    }
    fc.SetMaterial(CorrectPath(fc.GetMaterial()));
  }
}

class CBiTXTImportO2: public CBiTXTImport
{
public:
  virtual float LODNameToResol(const char *name)
  {
    return ::LODNameToResol(name);
  }

  virtual RStringB LODResolToName(float resol) const
  {
    return RStringB();
  }

};


static bool SccError(SCCRTN ret)
{  
  if (ret==0 ) return true;
  else if (ret==2) return false;
  else
  {
    CString err;
    err.Format(IDS_SCCERROR,ret);
    AfxMessageBox(err,MB_ICONSTOP);
    return false;
  }
}

class FunctorGetnerateListOfFiles
{
  const char *p3dFileName;
  LODObject *lodobj;
  mutable BTree<ObjMatLibItem> &fileList;
public:
  FunctorGetnerateListOfFiles(const char *p3dFileName,LODObject *lodobj,BTree<ObjMatLibItem> &filelist):p3dFileName(p3dFileName),lodobj(lodobj),fileList(filelist) {}
  bool operator ()(SRef<PluginGeneralAppSide> &plugin) const
  {
    int space=plugin->GetPlugin().GetAllDependencies(p3dFileName,lodobj,0);
    if (space)
    {
      char **list=(char **)malloc(space);
      int count=plugin->GetPlugin().GetAllDependencies(p3dFileName,lodobj,list);
      for (int i=0;i<count;i++) 
      {
        ObjMatLibItem it(list[i]);
        if (fileList.Find(it)==0) fileList.Add(it);
      }
      free(list);
    }
    return false;
  }

};

static bool GLVYield()
{
  CWnd *w=AfxGetMainWnd();
  if (w==0 || theApp._scc==0 || GLVStop) return false;
  w->SendMessage(MSG_BGRYIELD,0,0);
  w=AfxGetMainWnd();
  if (w==0 || theApp._scc==0) return false;
  return true;
};


static void GenerateListOfFiles(const char *name, LODObject& lodData,bool checkedOut,AutoArray<RString>& out)
{
  ProgressBar<int> progress(1);
  progress.ReportState(ENABLE_STOP);
  progress.ReportState(IDS_PROGRESS_GETLATESVER);
  BTree<ObjMatLibItem> allstuff;      
  out.Clear();
  int i;
  for (i=0;i<lodData.NLevels();i++)
  {
    ObjectData *obj=lodData.Level(i);
    ObjToolMatLib& matlib=obj->GetTool<ObjToolMatLib>();
    ObjToolProxy &proxies=obj->GetTool<ObjToolProxy>();
    matlib.ReadMatLib(allstuff,matlib.ReadTextures);
    matlib.ReadMatLib(allstuff,matlib.ReadMaterials);
    proxies.EnumProxies(allstuff,0,true,config->GetString(IDS_CFGPATHFORTEX));          
  }
  theApp.ForEachPlugin(FunctorGetnerateListOfFiles(name,&lodData,allstuff));
  if (allstuff.Size())
  {
    progress.SetMax(allstuff.Size());
    BTreeIterator<ObjMatLibItem> iter(allstuff);
    iter.BeginFrom(*allstuff.Smallest());      
    i=0;          
    for (ObjMatLibItem *item=NULL;(item=iter.Next());)
    {            
      progress.SetNextPos(++i);
      if (progress.StopSignaled()) break;
      const char *name=item->name;
      if (name[0] && name[0]!='#')
      {
        if (name[0]=='\\' && name[1]!='\\') name++;
        Pathname itempath(name,Pathname(config->GetString(IDS_CFGPATHFORTEX)));
        if (checkedOut)
        {
//          SCCSTAT stat=theApp._scc->Status(itempath);
//          if ((stat & SCC_STATUS_CONTROLLED) && (stat & SCC_STATUS_CHECKEDOUT))
          if (itempath.TestFile(Pathname::TFExistence)==true)
            out.Append()=itempath.GetFullPath();              
        }
        else
        {          
          if (itempath.TestFile(Pathname::TFExistence)==false || 
            itempath.TestFile(Pathname::TFCanWrite)==false)
              out.Append()=itempath.GetFullPath();              
        }
        if (!GLVYield()) break;
      }
    }
  }
  CloseProgressWindow();
}


static void GenerateListOfTexturesFromMaterials(const AutoArray<RString> &files,AutoArray<RString> &out, bool allFiles)
{
  BTree<RStringI> items;
  Pathname base=config->GetString(IDS_CFGPATHFORTEX);
  for (int i=0;i<files.Size();i++)
  {
    if (_stricmp(Pathname::GetExtensionFromPath(files[i]),".rvmat")==0)
    {      
      Pathname path(files[i],base);
      ObjToolMatLib::ExploreMaterial(path,items);
    }
  }
  out.Clear();
  if (items.Size())
  {
    BTreeIterator<RStringI> iter(items);
    RString *item=items.Smallest();
    iter.BeginFrom(*item);
    while ((item=iter.Next())!=NULL)
    {
      Pathname path(*item,base);
      if (allFiles || (theApp._scc->UnderSSControlNotDeleted(path) && !theApp._scc->CheckedOut(path)))
        out.Append()=RString(path.GetFullPath());
      if (!GLVYield()) break;
    }
  }
}

static SCCRTN GetLatestVersionOfArray(AutoArray<RString> &files)
{
  const char **localList=(const char **)alloca(files.Size()*sizeof(const char *));
  for (int i=0;i<files.Size();i++) localList[i]=files[i];
  SCCRTN rtn=theApp._scc->GetLatestVersion(localList,files.Size());
  return rtn;
}

static void GetLatestVersionLODObject(const char *name, LODObject &lodData)
{
  AutoArray<RString>files;
  GenerateListOfFiles(name,lodData,false,files);
  if (files.Size() && GLVYield())
  {
    SCCRTN rtn=GetLatestVersionOfArray(files);
    if (SccError(rtn))
    {
      AutoArray<RString> matfiles;
      GenerateListOfTexturesFromMaterials(files,matfiles,false);
      rtn=GetLatestVersionOfArray(matfiles);
      SccError(rtn);
    }
  }
}

static void GetListOfFilesFromProxy(LODObject &lodData, BTree<ObjMatLibItem> &list, int level=0)
{
  BTree<ObjMatLibItem> enumlist;      
  int i;
  for (i=0;i<lodData.NLevels();i++)
  {
    ObjectData *obj=lodData.Level(i);
    ObjToolProxy &proxies=obj->GetTool<ObjToolProxy>();
    proxies.EnumProxies(enumlist,0,true,config->GetString(IDS_CFGPATHFORTEX));          
  }
  BTreeIterator<ObjMatLibItem> it(enumlist);
  ObjMatLibItem *item;


  while ((item=it.Next()))
  {
    LODObject lodData;

    if (_access(item->name,0)!=0)
    {
       SCCSTAT status=theApp._scc->Status(item->name);
       if ((status &  SCC_STATUS_CONTROLLED) && (~status & SCC_STATUS_CHECKEDOUT)) theApp._scc->GetLatestVersion(item->name);
    }
    if (lodData.Load(Pathname(item->name),NULL,NULL)==0)
    {
      int i;
      for (i=0;i<lodData.NLevels();i++)
      {
        ObjectData *obj=lodData.Level(i);
        ObjToolMatLib& matlib=obj->GetTool<ObjToolMatLib>();
        ObjToolProxy &proxies=obj->GetTool<ObjToolProxy>();
        matlib.ReadMatLib(list,matlib.ReadTextures);
        matlib.ReadMatLib(list,matlib.ReadMaterials);
        //          proxies.EnumProxies(list,0,true,config->GetString(IDS_CFGPATHFORTEX));          
      }
      theApp.ForEachPlugin(FunctorGetnerateListOfFiles(item->name,&lodData,list));
      if (level<10) GetListOfFilesFromProxy(lodData, list, level+1);
      if (!GLVYield()) break;
   }
    else
      theApp.ConsolePrintf(IDS_BACKSS_PROXYNOTFOUND,item->name.Data());
  }
}

static void GetLatestVersionProxies(LODObject &lodData)
{
  AutoArray<RString> out;
  AutoArray<RString> out2;
  BTree<ObjMatLibItem> allstuff;
  GetListOfFilesFromProxy(lodData,allstuff,0);
  if (allstuff.Size())
  {    

    BTreeIterator<ObjMatLibItem> iter(allstuff);
    iter.BeginFrom(*allstuff.Smallest());      
    for (ObjMatLibItem *item=NULL;(item=iter.Next());)
    {            
      const char *name=item->name;
      if (name[0] || name[0]!='#')
      {
        if (name[0]=='\\' && name[1]!='\\') name++;
        Pathname itempath(name,Pathname(config->GetString(IDS_CFGPATHFORTEX)));
        SCCSTAT status=theApp._scc->Status(itempath);
        if ((status &  SCC_STATUS_CONTROLLED) && (~status & SCC_STATUS_CHECKEDOUT))
        {
          out.Append()=itempath.GetFullPath();              
        }
        else
          TRACE1("Skipping... %s\n",itempath.GetFullPath());
      }
      if (!GLVYield()) break;
    }
    if (!GLVYield()) return;
    if (SccError(GetLatestVersionOfArray(out)))
    {
      GenerateListOfTexturesFromMaterials(out,out2,false);
      SccError(GetLatestVersionOfArray(out2));

    }
  }
}


template<class Type>
struct AppendArr_BTreeIndex
{
  const Type *_item;
  bool operator==(const AppendArr_BTreeIndex &other) const {return *_item==*other._item;}
  bool operator>(const AppendArr_BTreeIndex &other) const {return *_item>*other._item;}
  bool operator<(const AppendArr_BTreeIndex &other) const {return *_item<*other._item;}
  bool operator>=(const AppendArr_BTreeIndex &other) const {return *_item>=*other._item;}
  bool operator<=(const AppendArr_BTreeIndex &other) const {return *_item<=*other._item;}
  bool operator!=(const AppendArr_BTreeIndex &other) const {return *_item!=*other._item;}
  int operator=(int zero) {_item=0;return zero;}
  AppendArr_BTreeIndex(const Type &item):_item(&item) {}
  AppendArr_BTreeIndex():_item(0) {}
};

template<class Type>
void AppendDistinctArray(AutoArray<Type> &a1,AutoArray<Type> &a2)
{
  BTree<AppendArr_BTreeIndex<Type> > db;
  a1.Reserve(a1.Size()+a2.Size(),a1.Size()+a2.Size());
  for (int i=0;i<a1.Size();i++) 
    if (db.Find(AppendArr_BTreeIndex<Type>(a1[i]))==NULL) db.Add(AppendArr_BTreeIndex<Type>(a1[i]));
  for (int i=0;i<a2.Size();i++)
    if (db.Find(AppendArr_BTreeIndex<Type>(a2[i]))==NULL) 
    {
      db.Add(AppendArr_BTreeIndex<Type>(a2[i]));
      a1.Append(a2[i]);
    }  
}

void CObjektiv2Doc::OnSourcecontrolSaveandcheckin()
{
  if (AskSave())
  {
    CDlgSccCheckInOut dlg;
   dlg.enableKeepCheck=true;
    if (dlg.DoModal()==IDOK)
    {
      if (!_isSccControled)  if (!SccError(theApp._scc->Add(GetPathName()))) return;
      if (SccError(theApp._scc->CheckIn(GetPathName(),dlg.vComments)))
      {
        if (dlg.vKeepCheck) theApp._scc->CheckOut(GetPathName());        
        CString datacomment=Pathname::GetNameFromPath(GetPathName());
        if (dlg.vComments.GetLength())
        {
          datacomment+=": ";
          datacomment+=dlg.vComments;
        }
        if (dlg.vCheckOther)
        {        
          AutoArray<RString>files;
          AutoArray<RString>matrefs;
          GLVStop=false;
          GenerateListOfFiles(GetPathName(),*LodData,true,files);
          GenerateListOfTexturesFromMaterials(files,matrefs,true);
          AppendDistinctArray(files,matrefs);
          if (files.Size())
          {
            SCCSTAT  *status=(SCCSTAT  *)alloca(files.Size()*sizeof(SCCSTAT ));            
            const char **localList=(const char **)alloca(files.Size()*sizeof(const char *));            
            for (int i=0;i<files.Size();i++) localList[i]=files[i];
            theApp._scc->Status(localList,files.Size(),status);
            
            int pos=0;
            for (int i=0;i<files.Size();i++) 
              if ((~status[i] & SCC_STATUS_CONTROLLED) || (status[i] & SCC_STATUS_DELETED))
                localList[pos++]=files[i];

            if (pos>0)
              SccError(theApp._scc->Add(localList,pos,datacomment));

            pos=0;
            for (int i=0;i<files.Size();i++) 
              if (status[i] & SCC_STATUS_CHECKEDOUT) 
                localList[pos++]=files[i];
            if (pos>0)

            SccError(theApp._scc->CheckIn(localList,pos,datacomment));
          }
        }
      }
      Pathname redo=GetPathName();
      redo.SetExtension(REDOFILE);
      DeleteFile(redo);
    }
  }
  UpdateFileSSStatus();  
}


class FunctorGetLatestVersion
{
public:
  bool operator ()(SRef<PluginGeneralAppSide> &plugin) const
  {
    plugin->GetPlugin().SSReload();
    return false;
  }
};

void CObjektiv2Doc::OnUpdateSourceControl_GetVer(CCmdUI *pCmdUI)
{
  OnUpdateSourceControl_Loaded(pCmdUI);
  pCmdUI->SetCheck(_ssGettingVer);
}


void CObjektiv2Doc::OnSourcecontrolGetlatestversion()
{
  if (_ssGettingVer) 
  {
    GLVStop=true;
    return;
  }
  SccError(theApp._scc->GetLatestVersion(GetPathName()));
  _ssGettingVer=true;
  GLVStop=false;
  if (GLVYield()) GetLatestVersionLODObject(GetPathName(),*LodData);
  if (GLVYield()) theApp.ForEachPlugin(FunctorGetLatestVersion());
  if (GLVYield()) GetLatestVersionProxies(*LodData); ///not threaded.
  if (AfxGetMainWnd()) UpdateMatLib();
  if (GLVYield()) MessageBeep(MB_ICONEXCLAMATION);else MessageBeep(MB_ICONHAND);
  _ssGettingVer=false;
  GLVStop=true;
}

void CObjektiv2Doc::OnSourcecontrolUndocheckout()
{ 
  if (AfxMessageBox(IDS_ASKUNDOCHECKOUT,MB_YESNO|MB_ICONQUESTION)==IDNO) return;
  const char *filename=GetPathName();
  SccError(theApp._scc->UndoCheckOut(filename));
  ReloadCurEditedFile();
  UpdateFileSSStatus();
}

void CObjektiv2Doc::OnSourcecontrolCheckout()
{
  const char *filename=GetPathName();
  if (theApp._scc->UnderSSControlNotDeleted(filename) && !theApp._scc->CheckedOut(filename))
  {
    struct stat fst1,fst2;
    stat(filename,&fst1);
    SccError(theApp._scc->CheckOut(filename));
    stat(filename,&fst2);
    if (fst1.st_mtime!=fst2.st_mtime) 
      if (AfxMessageBox(IDS_CHECKOUTCHANGEVER,MB_YESNO|MB_DEFBUTTON1|MB_ICONQUESTION)==IDYES)      
        ReloadCurEditedFile();
    UpdateFileSSStatus();
  }
}

void CObjektiv2Doc::OnSourcecontrolShowhistory()
{
  if (theApp._scc->UnderSSControl(GetPathName()))
  {
    SCCRTN ret=theApp._scc->History(GetPathName());
    if (ret==5) 
      ReloadCurEditedFile();
    else
      SccError(ret);
  }
}

void CObjektiv2Doc::OnUpdateSourceControl_Controlled(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_isSccControled);
}

void CObjektiv2Doc::OnUpdateSourceControl_Loaded(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(theApp._scc!=NULL);
}

void CObjektiv2Doc::OnUpdateSourceControl_CheckedOut(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_isSccControled && _isCheckedOut);
}

void CObjektiv2Doc::OnUpdateSourceControl_CheckedIn(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_isSccControled && !_isCheckedOut);
}

void CObjektiv2Doc::OnFileOpenfromss()
{
  SccFunctions *scc=theApp._scc;
  SccFunctions *clone=scc->Clone();
  if (clone==0) return;
  if (SccError(clone->ChooseProjectEx(scc->GetProjName(),scc->GetLocalPath(),scc->GetServerName(),scc->GetUserName())))
  {
    clone->GetLatestVersionDir(clone->GetLocalPath(),false);    
    config->SetString(IDS_CFGOPENFOLDER,clone->GetLocalPath());
    OnFileOpen();
    delete clone;
  }
}


void CObjektiv2Doc::UpdateFileSSStatus()
{
  const char *filename=GetPathName();
  _isSccControled=theApp._scc && theApp._scc->UnderSSControlNotDeleted(filename);
  if (_isSccControled) _isCheckedOut=theApp._scc->CheckedOut(filename);
  else _isCheckedOut=false;
  Pathname redo=filename;
  redo.SetExtension(REDOFILE);
  _ssCanBeRedoed=_access(redo,0)==0;
} 

void CObjektiv2Doc::OnSourcecontrolRedocheckout()
{
  if (_ssCanBeRedoed)
  {
    Pathname curname=GetPathName();
    Pathname redo=curname;
    redo.SetExtension(REDOFILE);
    if (theApp.rocheck.TestFileRO(curname,ROCHF_DisableSaveAs)==ROCHK_FileOK)
    {      
      if (FileNewer(curname,redo))
      {
        if (AfxMessageBox(IDS_REDOCHECKOUTWARNING,MB_YESNO|MB_DEFBUTTON2|MB_ICONEXCLAMATION)==IDNO) 
        {
          LoadDocument(curname);          
          return;
        }
      }
      LoadDocument(redo);
      SetPathName(curname,FALSE);
      this->filename=curname;
      UpdateFileSSStatus();
    }

  }
}

void CObjektiv2Doc::ReloadCurEditedFile(void)
{
  LodData->ClearDirty();
  Pathname curname=GetPathName();
  Pathname redo=curname;
  redo.SetExtension(REDOFILE);
  LodData->Save(redo,OBJDATA_LATESTVERSION,false,NULL,NULL);
  if (LodData->Load(curname,NULL,NULL))
  {
    OnNewDocument();
    AfxMessageBox(IDS_LOADUNSUCCESFUL);
  }
  else
  {
    comint.SetObject(LodData);
    UpdateAll();
    UpdateAllViews(NULL);
  }
  UpdateFileSSStatus();
}

void CObjektiv2Doc::OnUpdateSourceControl_RedoCheckout(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_isSccControled && !_isCheckedOut && _ssCanBeRedoed);
}

void CObjektiv2Doc::OnConvexityConvexityfinetune()
{
  comint.Begin(WString(IDS_NEW_CONVEX_HULL));
  comint.Run(new CIMakeConvex());
  UpdateAllViews(NULL);
}

#include "DlgInputFile.h"

void CObjektiv2Doc::OnStructureFindflags()
{
  CDlgInputFile dlg;
  dlg.browse=false;
  dlg.title="Find Flags";
  int p=dlg.DoModal();
  if (p==IDOK)
  {
    DWORD flags=0;
    const char *c=dlg.vFilename;
    while (*c)
    {
      char item=toupper(*c++);
      if (item>='0' && item<='9')      
        flags=(flags<<4)|(item-'0');        
      else if (item>='A' && item<='F')
        flags=(flags<<4)|(item-'A'+10);        
    }
    int i;
    ObjectData *cobj=LodData->Active();
    Selection *sel=new Selection (cobj);
    for (i=0;i<cobj->NFaces();i++)
    {
      FaceT fc(cobj,i);
      if ((fc.GetFlags() & ~FACE_COLORIZE_MASK)==(flags & ~FACE_COLORIZE_MASK))
      {
        sel->FaceSelect(i);
      }
    }
    for (i=0;i<cobj->NPoints();i++)
    {
      PosT ps=cobj->Point(i);
      if (ps.flags==flags)
      {
        sel->PointSelect(i);
      }
    }
    comint.Begin(WString(IDS_SELECTUSING,dlg.vFilename));
    comint.Run(new CISelect(sel));
    UpdateAllViews(NULL);
  }
}

class CExportBiTXTFileDialog: public CFileDialogEx
{
public:
  CBiTXTExport::ExportMode mode;
  bool sortFaces;
  bool initExport;
public:
  CExportBiTXTFileDialog(BOOL hOpenFileDialog,const char *lpszDefExt,const char *lpszFileName,DWORD dwFlags,const char *lpszFilter):CFileDialogEx(hOpenFileDialog,lpszDefExt,lpszFileName,dwFlags,lpszFilter)
    {initExport=hOpenFileDialog==FALSE;}  
  virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
  virtual BOOL OnInitDialog();
  bool SortFaces() const {return sortFaces;}
};

int CExportBiTXTFileDialog::OnCommand(WPARAM wParam, LPARAM lParam)
{
  int id=LOWORD(wParam);
  switch (id)
  {
  case IDC_NORMALS: mode=CBiTXTExport::ExpNormals;break;
  case IDC_SMOOTHGROUPS: mode=CBiTXTExport::ExpSG;break;
  case IDC_EDGELIST: mode=CBiTXTExport::ExpEdges;break;
  case IDC_SORTFACES: sortFaces=!sortFaces;
  default: return __super::OnCommand(wParam,lParam);
  }
  return TRUE;
}

BOOL CExportBiTXTFileDialog::OnInitDialog()
{
  switch (mode)
  {
  case CBiTXTExport::ExpNormals:CheckDlgButton(IDC_NORMALS,1);break;
  case CBiTXTExport::ExpSG:CheckDlgButton(IDC_SMOOTHGROUPS,1);break;
  case CBiTXTExport::ExpEdges:CheckDlgButton(IDC_EDGELIST,1);break;
  }
  sortFaces=true;
  CheckDlgButton(IDC_SORTFACES,BST_CHECKED);
  if (GetDlgItem(IDC_NORMALS))
  {  
    GetDlgItem(IDC_NORMALS)->EnableWindow(initExport);
    GetDlgItem(IDC_SMOOTHGROUPS)->EnableWindow(initExport);
    GetDlgItem(IDC_EDGELIST)->EnableWindow(initExport);
    GetDlgItem(IDC_SORTFACES)->EnableWindow(!initExport);
  }
  return __super::OnInitDialog();
}

void CObjektiv2Doc::OnExportBitxt()
{
//#ifndef PUBLIC // enabled in public tools
  CExportBiTXTFileDialog fdlg(FALSE,"txt",0,OFN_PATHMUSTEXIST|OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY|OFN_EXPLORER|OFN_ENABLETEMPLATE|OFN_ENABLESIZING,0);
  fdlg.m_pOFN->lpTemplateName=MAKEINTRESOURCE(IDD_EXPORTBITXTTEMPLATE);
  fdlg.m_pOFN->hInstance=AfxGetInstanceHandle();
  fdlg.m_pOFN->Flags|=OFN_ENABLESIZING;
  fdlg.mode=(CBiTXTExport::ExportMode)theApp.GetProfileInt("Settings","BiTXTExportMode",2);
  if (fdlg.DoModal()==IDOK)
  {
    CBiTXTExport exp;
    theApp.WriteProfileInt("Settings","BiTXTExportMode",fdlg.mode);
    if (exp.Export(fdlg.GetPathName(),LodData,fdlg.mode)==false)
    {
      AfxMessageBox(IDS_EXPORTFAILED,MB_OK|MB_ICONEXCLAMATION);
    }
  }
//#endif
}


void CObjektiv2Doc::OnImportUniversalbistudiotxt()
{
//#ifndef PUBLIC // enabled in public tools
  CExportBiTXTFileDialog fdlg(TRUE,0,0,OFN_HIDEREADONLY|OFN_FILEMUSTEXIST|OFN_ENABLETEMPLATE|OFN_ENABLESIZING,0);
  fdlg.m_pOFN->lpTemplateName=MAKEINTRESOURCE(IDD_EXPORTBITXTTEMPLATE);
  fdlg.m_pOFN->hInstance=AfxGetInstanceHandle();
  fdlg.m_pOFN->Flags|=OFN_ENABLESIZING;
  if (fdlg.DoModal()==IDOK)
  {
    CBiTXTImportO2 import;
    if (OnNewDocument()==FALSE) return;
    import.EnableSortFaces(fdlg.SortFaces());
    import.Import(fdlg.GetPathName(),LodData);
    if (import.IsError())
    {
      char buff[512];
      import.FormatError(buff,sizeof(buff));
      AfxMessageBox(buff,MB_ICONEXCLAMATION|MB_OK);
    }
    if (UpdateTextureF)
      for (int i=0;i<LodData->NLevels();i++)ConvertTextures(*LodData->Level(i));
    UpdateAllViews(NULL);
    UpdateAllToolbars(0);
    comint.SetObject(LodData);
  }
//#endif
}


void CObjektiv2Doc::OnViewProxylevel()
{
  DlgProxyLevel *lev=new DlgProxyLevel;
  lev->document=this;
  lev->Create(lev->IDD);
}

static UINT AFX_CDECL ProxyNotFoundThread(LPVOID data)
{
  RString *nam=(RString *)data;

  CString P3DFilter;P3DFilter.LoadString(IDS_FILESAVEFILTERS);
  CString topic;
  AfxFormatString1(topic,IDS_PROXYNOTFOUND,*nam);
  CFileDialogEx fdlg(TRUE,0,Pathname::GetNameFromPath(*nam),OFN_HIDEREADONLY|OFN_FILEMUSTEXIST,P3DFilter);
  fdlg.m_ofn.lpstrTitle=topic;
  if (fdlg.DoModal()==IDOK)  
    *nam=RString(fdlg.GetPathName().GetString());  
  else
    *nam=RString();
  return 0;
}

RString CObjektiv2Doc::DocProxyCache::ProxyNotFound(RString name)
{
  CObjektiv2Doc *ddoc=reinterpret_cast<CObjektiv2Doc *>(reinterpret_cast<char *>(this)-offsetof(CObjektiv2Doc,_proxyCache));
  Pathname searchName=ddoc->GetPathName().GetString();
  searchName.SetFilename(Pathname::GetNameFromPath(name));
  
  if (searchName.BottomTopSearchExist()) return RString(searchName);
  ///This event MUST be processed in another thread. This is called during WM_PAINT. 
  //Openning new windows during WM_PAINT may cause infinite loop.
  CWinThread *thr=AfxBeginThread(ProxyNotFoundThread,(LPVOID)&name,THREAD_PRIORITY_NORMAL,0,CREATE_SUSPENDED,0);
  thr->m_bAutoDelete=FALSE; thr->ResumeThread();
  while (MsgWaitForMultipleObjects(1,&thr->m_hThread,FALSE,1000,QS_SENDMESSAGE)!=WAIT_OBJECT_0)
  {
    MSG msg;PeekMessage(&msg,0,0,0,PM_REMOVE);
    theApp.idlecnt++;
  }
  delete thr;
  return name;
}

float CObjektiv2Doc::GetProxyLevel()
{
  float res=LodData->Resolution(LodData->ActiveLevel());
  if (res!=LOD_GEOMETRY && (res<LOD_SHADOW_MIN || res>=LOD_SHADOW_MAX) && res!=LOD_FIREGEO && res!=LOD_VIEWGEO) res=_proxyLevel;
  return res;
}


