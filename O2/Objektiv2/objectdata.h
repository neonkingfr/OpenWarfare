// ObjectData.h: interface for the CObjectData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OBJECTDATA_H__FED10227_55E1_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_OBJECTDATA_H__FED10227_55E1_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#include "AutoArray.h"
#include "SPoint.h"
#include "faceflags.h"

struct SPoint3DFP: public SPoint3DF
  {
  int flags;
  bool deleted; //vertex is deleted;
  SPoint3DFP():flags(0),SPoint3DF() 
    {deleted=false;}
  SPoint3DFP(float *f):flags(0),SPoint3DF(f) 
    {deleted=false;}
  SPoint3DFP(float f,...):flags(0),SPoint3DF(&f) 
    {deleted=false;}
  void Delete() 
    {deleted=true;}
  };

//--------------------------------------------------

struct SDataVertex
  {
  int point,normal;
  float mapU,mapV;
  };

//--------------------------------------------------

#define MAX_DATA_POLY 4

struct SFaceInfo
  {
  char texture[32]; // warning - name length is limited!
  int n;
  SDataVertex vs[MAX_DATA_POLY];
  int flags;
  void SetTexture(const char *name) 
    {strncpy(texture,name,sizeof(texture));texture[sizeof(texture)-1]=0;}
  };

//--------------------------------------------------

typedef CAutoArray<SPoint3DFP> CPointArray;
typedef CAutoArray<SPoint3DF> CNormalArray;
typedef CAutoArray<SFaceInfo> CFaceArray;


class CSelection
  {
  CAutoArray<int> points;
  int lastquery;
  void Reset() 
    {lastquery=0;}
  void Release() 
    {Reset();points.Release();}
  public:
    bool IsSelected(int index);
    bool SelectVertex(int index);
  };

//--------------------------------------------------

#define NAMEDSELECTIONSIZE 64
class CNamedSelection: public CSelection
  {
  private:
    char _name[64];
  public:
    const char *Name() const 
      {return _name;}
    void SetName( const char *name ) 
      {strncpy(_name,name,sizeof(_name));_name[sizeof(_name)-1]=0;}
  };

//--------------------------------------------------

class CNamedProperty
  {
  private:
    char _name[64];
    char _value[64];
    
  public:
    CNamedProperty() 
      {}
    CNamedProperty( const char *name, const char *value="" )
      {
      SetName(name);SetValue(value);
      }
    
    const char *Name() const 
      {return _name;}
    const char *Value() const 
      {return _value;}
    void SetName( const char *name ) 
      {strncpy(_name,name,sizeof(_name));_name[sizeof(_name)-1]=0;}
    void SetValue( const char *value ) 
      {strncpy(_value,value,sizeof(_value));_value[sizeof(_value)-1]=0;}
    friend ostream& operator<<(ostream& str,const CNamedProperty& prop)
      {FSaveString(str,prop._name);FSaveString(str,prop._value);return str;}
    friend istream& operator>>(istream& str,CNamedProperty& prop)
      {FLoadString(str,prop._name,sizeof(prop._name));FLoadString(str,prop._value,sizeof(prop._name));return str;}
  };

//--------------------------------------------------

typedef CAutoArray<CNamedProperty> CNamedPropertyArray;
typedef CAutoArray<CNamedSelection> CNamedSelectionArray;

class CLodData  
  {
  private:
    CPointArray points;	  //array of points
    CNormalArray normals; //array of normals
    CFaceArray faces;	  //array of faces
    CNamedPropertyArray property; //array of properties
    CNamedSelectionArray selection;
    float resolution;	//Lod Resolution
    int lodtype;
  public:
    CLodData();
    virtual ~CLodData();
    
  };

//--------------------------------------------------

#endif // !defined(AFX_OBJECTDATA_H__FED10227_55E1_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
