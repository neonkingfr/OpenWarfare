
#include "stdafx.h"

long prot_componumber=0xFFFFFFFF;  //computer number
long prot_encryptcompo=0xFFFFFFFF;  //computer number
long prot_unlockcode=0xFFFFFFFF;  //unlock code
long prot_requestcode=0xFFFFFFFF;  //request code
long prot_magicnumber=0xFFFFFFFF;  //magic number
long prot_result=0xFFFFFFFF;  //magic number
long prot_handle;		 //protection handle
long prot_dummy=0x7FFFFF00;


#ifdef FULLVER

void protInitProtection()
  {}

//--------------------------------------------------

#else

void protInitProtection()
  {
  //  pp_initlib(AfxGetInstanceHandle());
  if (!protCheckKeyLib3()) ExitProcess(1);
  prot_componumber = pp_compno(COMPNO_WINPRODID | COMPNO_MACADDR | COMPNO_HDSERIAL, "", "C");  
  prot_encryptcompo = pp_nencrypt(prot_componumber & 16383,PP_SUCCESS);
  char buff[1024];
  GetModuleFileName(NULL, buff, 1024);
  char *s=strrchr(buff,'\\');
  strcpy(s,"\\objektiv2.lf");
  DWORD out=pp_lfopen(buff, LF_CREATE_MISSING|LF_CREATE_HIDDEN, LF_FILE, "bredyheslo", &prot_handle);
  if (out!=PP_SUCCESS) 
    {
    MessageBox(NULL,"Unable to open licence file. Sorry, but application cannot continue","Licence error!",MB_OK|MB_TASKMODAL);
    ExitProcess(1);
    }
  if (pp_valdate(prot_handle)==PP_FALSE)
    {
    MessageBox(NULL,"Time is going back? Don't cheating, return clock back to the current valid date!","Licence error!",MB_OK|MB_TASKMODAL);
    ExitProcess(1);
    }
  }

//--------------------------------------------------

#endif