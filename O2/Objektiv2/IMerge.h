
class CIMerge:public CICommand
  {
  ObjectData *obj;
  char *name;
  public:
    CIMerge(ObjectData *obj,const char *selname=NULL):obj(obj) 
      {if (selname)name=_strdup(selname);else name=NULL;}
    CIMerge(istream& str) 
      {
      obj=new ObjectData();obj->LoadBinary(str);
      name=NULL;
      }
    virtual int Execute(LODObject *lod)
      {
	  comint.sectchanged=true;
      return !lod->Active()->Merge(*obj,name);
      }
    virtual ~CIMerge() 
      {delete obj;free(name);}
    virtual int Tag() const 
      {return CITAG_MERGE;}
    virtual void Save(ostream& str) 
      {obj->SaveBinary(str,OBJDATA_LATESTVERSION);}
  };

