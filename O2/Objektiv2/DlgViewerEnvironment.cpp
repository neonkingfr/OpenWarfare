// DlgViewerEnvironment.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "DlgViewerEnvironment.h"
#include ".\dlgviewerenvironment.h"
#include "MainFrm.h"


// CDlgViewerEnvironment dialog

IMPLEMENT_DYNAMIC(CDlgViewerEnvironment, CDialog)
CDlgViewerEnvironment::CDlgViewerEnvironment(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgViewerEnvironment::IDD, pParent)
  {
    _lastState.time=0;
    _lastState.eyeAccom=1;
}

CDlgViewerEnvironment::~CDlgViewerEnvironment()
{
}

void CDlgViewerEnvironment::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_OVERCASTSLIDER, wOvercast);
  DDX_Control(pDX, IDC_FOGSLIDER, wFog);
  DDX_Control(pDX, IDC_TIMEOFDAYNUM, wTimeOfDay);
  DDX_Control(pDX, IDC_TIMEOFDAY, wTimeOfDayScr);
  DDX_Control(pDX, IDC_PRESETS, wPresets);
  DDX_Control(pDX, IDC_HDRSLIDER, wHdr);
  DDX_Control(pDX, IDC_HDRENABLE, wHdrEnable);
  DDX_Control(pDX, IDC_OVERCASTVAL, wOvercastVal);
  DDX_Control(pDX, IDC_FOGVAL, wFogVal);
  DDX_Control(pDX, IDC_HDRVAL, wHdrVal);
}


BEGIN_MESSAGE_MAP(CDlgViewerEnvironment, CDialog)
  ON_WM_TIMER()
  ON_WM_HSCROLL()
  ON_WM_DRAWITEM()
  ON_BN_CLICKED(IDC_SAVE_PRESET, OnBnClickedSavePreset)
  ON_BN_CLICKED(IDC_LOAD_PRESET, OnBnClickedDelPreset)
  ON_CBN_EDITCHANGE(IDC_PRESETS, OnCbnEditchangePresets)
  ON_CBN_SELENDOK(IDC_PRESETS, OnCbnSelendokPresets)
  ON_BN_CLICKED(IDC_HDRENABLE, OnBnClickedHdrenable)
  ON_EN_CHANGE(IDC_OVERCASTVAL, OnEnChangeOFH)
  ON_EN_CHANGE(IDC_FOGVAL, OnEnChangeOFH)
  ON_EN_CHANGE(IDC_HDRVAL, OnEnChangeOFH)
  ON_NOTIFY(DTN_DATETIMECHANGE, IDC_TIMEOFDAYNUM, OnDtnDatetimechangeTimeofdaynum)
END_MESSAGE_MAP()


// CDlgViewerEnvironment message handlers

static void SetTimeOfDay(DWORD time, CDateTimeCtrl &wTimeOfDay,CScrollBar &wTimeOfDayScr)
  {
    if (wTimeOfDay.GetFocus()!=&wTimeOfDay)
      wTimeOfDay.SetTime(COleDateTime(0,0,0,time/3600,(time/60)%60,time%60));
    wTimeOfDayScr.SetScrollPos(time/4);
  }

static void FormatColorInfo(COLORREF ambient, COLORREF diffuse, CDialog &indlg)
  {
  CString colors;
  colors.Format("[%3d, %3d, %3d]        [%3d, %3d, %3d]",
    GetRValue(ambient),GetGValue(ambient),GetBValue(ambient),
    GetRValue(diffuse),GetGValue(diffuse),GetBValue(diffuse));
  indlg.SetDlgItemText(IDC_COLORNUMBERS,colors);
  }

void CDlgViewerEnvironment::OnTimer(UINT nIDEvent)
  {
    ReadViewerState();
  }


void CDlgViewerEnvironment::ReadViewerState()
{
  long timeadv=_lastState.time;
  if (frame->_externalViewer.GetEnvironment(_lastState)==ExternalViewer::ErrOK)
  {
    if (timeadv!=0)
    {
      frame->_externalViewer.SetEnvironmentValue(_lastState.valTime,(long)(timeadv-_lastState.time));
      _lastState.time=timeadv;
    }
    wOvercast.SetPos(toInt(_lastState.overcast*1000));
    wFog.SetPos(toInt(_lastState.fog*1000));
    if (_lastState.eyeAccom>=0) wHdr.SetPos(toInt(log(_lastState.eyeAccom)*1000));
    wHdrEnable.SetCheck(_lastState.eyeAccom>=0);
    SetFloatValue(wOvercastVal,_lastState.overcast);
    SetFloatValue(wFogVal,_lastState.fog);
    if (_lastState.eyeAccom>=0) SetFloatValue(wHdrVal,log(_lastState.eyeAccom));
    SetTimeOfDay(_lastState.time,wTimeOfDay,wTimeOfDayScr);
    ambient=RGB(toInt(_lastState.ambient[0]*255.0f),toInt(_lastState.ambient[1]*255.0f),toInt(_lastState.ambient[2]*255.0f));
    diffuse=RGB(toInt(_lastState.diffuse[0]*255.0f),toInt(_lastState.diffuse[1]*255.0f),toInt(_lastState.diffuse[2]*255.0f));

    GetDlgItem(IDC_COLORVIEW)->Invalidate(FALSE);
    FormatColorInfo(ambient,diffuse,*this);
  }  
}
BOOL CDlgViewerEnvironment::OnInitDialog()
  {
  CDialog::OnInitDialog(); 
  SetTimer(1000,1000,NULL);
  wOvercast.SetRange(0,1000);
  wFog.SetRange(0,1000);
  wHdr.SetRange(-9000,-4000);
  wHdr.SetPos(1000);
  wOvercast.SetTicFreq(200);
  wFog.SetTicFreq(200);
  wHdr.SetTicFreq(1000);
  SCROLLINFO info;
  info.cbSize = sizeof(SCROLLINFO);     
  info.fMask = SIF_ALL;     
  info.nMin = 0;     
  info.nMax = 24*60*15; 
  info.nPage = 0;     
  info.nPos = 0;    
  info.nTrackPos = 0; 
  wTimeOfDayScr.SetScrollInfo(&info);
  wPresets.SetCurSel(-1);
  ReadViewerState();
  LoadPresets();
  return TRUE;
  }

void CDlgViewerEnvironment::SetFloatValue(CEdit &edt, float val)
{
  if (edt.GetFocus()==&edt) return;
  char buff[50];
  sprintf(buff,"%d",toLargeInt(val*1000));
  edt.SetWindowText(buff);
}
void CDlgViewerEnvironment::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
  {
  CSliderCtrl *slider=reinterpret_cast<CSliderCtrl *>(pScrollBar);
  if (slider==&wOvercast || slider==&wFog || slider==&wHdr) nPos=slider->GetPos();
  float value=(int)nPos/1000.0f;  
  if (slider==&wOvercast)
  {
    frame->_externalViewer.SetEnvironmentValue(EnvInfoStruct::valOvercast,value);    
    SetFloatValue(wOvercastVal,value);
  }
  else if (slider==&wFog)        
  {
    frame->_externalViewer.SetEnvironmentValue(EnvInfoStruct::valFog,value);    
    SetFloatValue(wFogVal,value);
  }
  else if (slider==&wHdr)        
  {
    frame->_externalViewer.SetEnvironmentValue(EnvInfoStruct::valEyeAccom,exp(value));    
    SetFloatValue(wHdrVal,value);
  }
  else if (pScrollBar==&wTimeOfDayScr)
    {
    int delta=0;
    switch (nSBCode)
      {
      case SB_LINELEFT: delta=-60;break;
      case SB_LINERIGHT: delta=+60;break;
      case SB_PAGELEFT: delta=-3600;break;
      case SB_PAGERIGHT: delta=+3600;break;
      case SB_LEFT: delta=-(long)_lastState.time;break;
      case SB_RIGHT: delta=24*60*60-_lastState.time;break;
      case SB_THUMBPOSITION:
      case SB_THUMBTRACK:
        delta=nPos*4-_lastState.time;        break;
      default: delta=0;break;
      }    
    if (delta) 
      {
      if (frame->_externalViewer.SetEnvironmentValue(_lastState.valTime,(long)delta)==IExternalViewer::ErrOK)
        {
          _lastState.time=0;
          ReadViewerState();
        }
      }
    }
  CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
  }


void CDlgViewerEnvironment::OnOK()
  {
  // TODO: Add your specialized code here and/or call the base class

  }

void CDlgViewerEnvironment::OnCancel()
  {
  // TODO: Add your specialized code here and/or call the base class

  DestroyWindow();
  }

void CDlgViewerEnvironment::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
  {
  if (nIDCtl==IDC_COLORVIEW)
    {
    CRect rc=lpDrawItemStruct->rcItem;
    CSize sz=rc.Size();
    CDC &dc=*CDC::FromHandle(lpDrawItemStruct->hDC);
    dc.FillSolidRect(rc.left,rc.top,sz.cx/2,sz.cy,ambient);
    dc.FillSolidRect(rc.left+sz.cx/2,rc.top,sz.cx/2,sz.cy,diffuse);
    }
  }

#define EXTERNALCONFIGPRESETS "ExternalViewer/Presets"

void CDlgViewerEnvironment::LoadPresets()
  {
    wPresets.ResetContent();
    HKEY key=theApp.GetSectionKey(EXTERNALCONFIGPRESETS);
    DWORD values;
    DWORD maxvalnamelen;
    CString nameBuff;
    RegQueryInfoKey(key,0,0,0,0,0,0,&values,&maxvalnamelen,0,0,0);
    char *name=nameBuff.GetBuffer(++maxvalnamelen);
    for (DWORD i=0;i<values;i++)
    {
      DWORD len=maxvalnamelen;
      RegEnumValue(key,i,name,&len,0,0,0,0);
      wPresets.AddString(name);
    }
    nameBuff.ReleaseBuffer();
    if (values==0)
    {
      wPresets.SetWindowText(ENVSTARTUPPROFILE);
    }
    RegCloseKey(key);
  }

class SerializeWritter
{
  static const int maxbuffer=1024;
  char data[maxbuffer];
  int ptr;

  void Write(void *p, UINT size)
  {
    if (size+ptr>maxbuffer) return;
    memcpy(data+ptr,p,size);
    ptr+=size;
  }

public:
  SerializeWritter() {ptr=0;}
  
  template<class T> void operator()(T &t) {t.Serialize(*this);}    
  template<> void operator()(float &t) {Write(&t,sizeof(t));}    
  template<> void operator()(long &t) {Write(&t,sizeof(t));}    
  template<> void operator()(unsigned long &t) {Write(&t,sizeof(t));}    
  
  const char *GetData() const {return data;}
  int GetSize() const {return ptr;}  
};

void CDlgViewerEnvironment::OnBnClickedSavePreset()
  {
    CString name;
    wPresets.GetWindowText(name);
    if (name.GetLength()!=0)     
    {
      SerializeWritter wr;
      _lastState.Serialize(wr);
      theApp.WriteProfileBinary(EXTERNALCONFIGPRESETS,name,(LPBYTE)wr.GetData(),wr.GetSize());
      LoadPresets();
    }
    else
      AfxMessageBox(IDS_ENVPRESET_NONAME);
  }

void CDlgViewerEnvironment::OnBnClickedDelPreset()
  {
    CString name;
    wPresets.GetWindowText(name);
    if (name.GetLength() && AfxMessageBox(IDS_ENVPRESET_DELETEASK,MB_YESNO)==IDYES)
      theApp.WriteProfileString(EXTERNALCONFIGPRESETS,name,0);
  }


void CDlgViewerEnvironment::OnCbnEditchangePresets()
  {
    CString name;
    wPresets.GetWindowText(name);
    EnvInfoStruct envinfo;
    if (LoadProfile(name,envinfo))
    {
      _lastState.time=0;
      frame->_externalViewer.SetEnvironment(envinfo);
    }

  }

class SerializeReader
{
  LPBYTE data;
  UINT sz;
  UINT ptr;

  void Read(void *p, UINT size)
  {
    if (size+ptr>sz) return;
    memcpy(p,data+ptr,size);
    ptr+=size;
  }
public:
  SerializeReader(LPBYTE data, UINT size):data(data),sz(size),ptr(0) {}

  template<class T> void operator()(T &t) {t.Serialize(*this);}    
  template<> void operator()(float &t) {Read(&t,sizeof(t));}    
  template<> void operator()(long &t) {Read(&t,sizeof(t));}    
  template<> void operator()(unsigned long &t) {Read(&t,sizeof(t));}    
};

bool CDlgViewerEnvironment::LoadProfile(const char *name, EnvInfoStruct &envInfo)
{
  LPBYTE data;
  UINT bytes;
  if (theApp.GetProfileBinary(EXTERNALCONFIGPRESETS,name,&data,&bytes)==FALSE) return false;
  SerializeReader rd(data,bytes);
  memset(&envInfo,0,sizeof(envInfo));
  envInfo.Serialize(rd);
  delete [] data;
  return true;
}

void CDlgViewerEnvironment::OnCbnSelendokPresets()
{
  OnCbnEditchangePresets();  
}

void CDlgViewerEnvironment::OnBnClickedHdrenable()
{
if (!wHdrEnable.GetCheck())
  frame->_externalViewer.SetEnvironmentValue(EnvInfoStruct::valEyeAccom,(float)-1);
else
  frame->_externalViewer.SetEnvironmentValue(EnvInfoStruct::valEyeAccom,wHdr.GetPos()/1000.f);
}

void CDlgViewerEnvironment::OnEnChangeOFH()
{
  CWnd *focus=CWnd::GetFocus();
  if (focus==0) return;
  char buff[50];
  focus->GetWindowText(buff,50);
  int value;
  if (sscanf(buff,"%d",&value)!=1) return;
  float fvalue=0.001f*value;

  if (focus==&wOvercastVal)
  {
    frame->_externalViewer.SetEnvironmentValue(EnvInfoStruct::valOvercast,fvalue); 
    wOvercast.SetPos(value);
  }
  else if (focus==&wFogVal)
  {
    frame->_externalViewer.SetEnvironmentValue(EnvInfoStruct::valFog,fvalue); 
    wFog.SetPos(value);
  }
  else if (focus==&wHdrVal)
  {
    frame->_externalViewer.SetEnvironmentValue(EnvInfoStruct::valEyeAccom,exp(fvalue)); 
    wHdr.SetPos(value);
  }
}

void CDlgViewerEnvironment::OnDtnDatetimechangeTimeofdaynum(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMDATETIMECHANGE pDTChange = reinterpret_cast<LPNMDATETIMECHANGE>(pNMHDR);
  *pResult = 0;

  long newTime=pDTChange->st.wHour*3600+pDTChange->st.wMinute*60+pDTChange->st.wSecond;
  long delta=newTime-_lastState.time;

  if (frame->_externalViewer.SetEnvironmentValue(_lastState.valTime,(long)delta)==IExternalViewer::ErrOK)
  {
    _lastState.time=0;
    ReadViewerState();
  }

}
