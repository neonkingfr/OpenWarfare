#if !defined(AFX_CONSOLE_H__3800EF72_D661_4ED8_BAE1_ADA27C8C3805__INCLUDED_)
#define AFX_CONSOLE_H__3800EF72_D661_4ED8_BAE1_ADA27C8C3805__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Console.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CConsole dialog

class CConsole : public CDialog
  {
  // Construction
  public:
    CConsole(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CConsole)
    enum  { IDD = IDD_CONSOLE };
    CString	consoleText;
    //}}AFX_DATA
	const char *vCaption;
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CConsole)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CConsole)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONSOLE_H__3800EF72_D661_4ED8_BAE1_ADA27C8C3805__INCLUDED_)
