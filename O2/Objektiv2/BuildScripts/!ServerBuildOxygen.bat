@echo off
SETLOCAL ENABLEDELAYEDEXPANSION
::=============================================================================
::TWO empty lines are required for LineFeed to work
set LineFeed=^


::TWO empty lines are required for LineFeed to work
::=============================================================================

:: parameters

set batchDir=%~dp0
set criticalError=0

:: Date time
set timeBuild=%time%
FOR /F "tokens=1-2 delims= " %%A IN ("%date%") DO (
	set dateBuild=%%B
)

:: Script constants
set wantSendToNG=1

:: Tools path
set svn=c:\Bis\Subversion\bin\svn
set SendToNG=c:\bis\build\SendToNG.bat
set perl=perl
set python=c:\Python27\python.exe
set /p skypeChat=<!batchDir!skypeChat.txt
set skype=!python! c:\Python27\Clisk\clisk.py msg !skypeChat!
set signTool="c:\Bis\sign\sign.bat"

:: Set this for project
set productName=Oxygen

:: Build params
set devenv="c:\Program Files (x86)\Microsoft Visual Studio 8\Common7\IDE\devenv.exe"
::set incredibuild="c:\Program Files (x86)\Xoreax\IncrediBuild\BuildConsole.exe"
set sourcePath=w:\c\projects\Objektiv2\
set sourcePathProjects=w:\c\projects\
set sourcePathEl=w:\c\El\
set sourcePathEs=w:\c\Es\
set sourcePathImg=w:\c\img\
set toolsln=!sourcePath!Objektiv2.sln
set outExePath=!sourcePath!Bin\
set outPdbPath=!sourcePath!pdb\
set buildCfg="Release Public|Win32"
set buildDir=O:\Tools\Oxygen\
set buildLog=%batchDir%BuildLogOXYGEN.txt
set logsDstPath=O:\Tools.Logs\Oxygen\

cd /d %batchDir%

echo ----------------------------------------------------------------------
echo --- %productName%  build ---------------------------------------------
echo ---------------------------------------- %dateBuild% %timeBuild% -----
echo parameters:
echo   - buildCfg: %buildCfg%
echo   - buildDir: %buildDir%
echo ----------------------------------------------------------------------


::=============================================================================
:: Update sources from svn
::=============================================================================
call:Color 2 "Getting sources revision %SVNrevisionNo% ..." \n
:: TODO: aggregate logs?
%svn% update --non-interactive %sourcePathEl% 2> %batchDir%svnUpdateErrorEl.txt
%svn% update --non-interactive %sourcePathEs% 2> %batchDir%svnUpdateErrorEs.txt
%svn% update --non-interactive %sourcePathImg% 2> %batchDir%svnUpdateErrorImg.txt
%svn% update --non-interactive %sourcePathProjects% 2> %batchDir%svnUpdateErrorProjets.txt
:: TODO: testovat po kazdem updatu
if errorlevel 1 goto svnUpdateFailed

:: Get SVN info
pushd
:: TODO: svn info ktere adresare?
%svn% info %sourcePath% > %batchDir%svninfo_uncensored.txt
echo SVN Info:
type svninfo_uncensored.txt

:: Set SVNrevisionNo
%perl% %batchDir%getLastChangedRevisionNumber.pl %batchDir%svninfo_uncensored.txt > %batchDir%svninfoRevNo.txt
set /p SVNrevisionNo=<%batchDir%svninfoRevNo.txt

:: Set full path to autotest logs and backup dir
set timeBuildHour=!timeBuild:~0,2!
set timeBuildMinute=!timeBuild:~3,2!
set logDirName=!SVNrevisionNo!-!DATE:~3!-!timeBuildHour: =0!.!timeBuildMinute: =0!
set logsDstFullPath=!logsDstPath!!logDirName!
md !logsDstFullPath!

popd

::=============================================================================
:: Build configurations
::=============================================================================
set BuildSuccessful=0

del !buildLog! > nul
call:funcBuildConfiguration Oxygen !buildCfg!
:: All ok, set flag
set BuildSuccessful=1


::=============================================================================
:: Sign app .exe
::=============================================================================
call:Color 2 "Sign app exe" \n

for %%f in (!outExePath!*.exe !outExePath!*.dll) do if [%BuildSuccessful%]==[1] call:funcSignAppExe %%f

::=============================================================================
:: Deploying
::=============================================================================
call:Color 2 "Deploying..." \n

del /q !buildDir!*.exe
del /q !buildDir!*.dll
copy /y !outExePath!*.exe !buildDir!
copy /y !outExePath!*.dll !buildDir!
::backup
copy /y !outExePath!*.exe !logsDstFullPath!
copy /y !outExePath!*.dll !logsDstFullPath!
copy /y !outPdbPath!*.pdb !logsDstFullPath!

start !skype! Build !productName!: !dateBuild! !timeBuild! SVN Revision:!SVNrevisionNo! has been distributed.
:: Send to NG
if [%wantSendToNG%]==[1] (
	call %SendToNG% "!productName! build distributed (revision !SVNrevisionNo!)" !batchDir!empty.txt
)

echo Finished successfully

goto end

::=============================================================================
:: Something Failed
::=============================================================================

:svnUpdateFailed
call:funcReportError "SVN Update failed." !batchDir!svnUpdateError.txt
set criticalError=1
goto end

:buildFail
call:funcReportError "BUILD failed. (revision %SVNrevisionNo%)" !buildLog!
set criticalError=1
goto end

:signFailed
call:funcReportError "Sign failed." !batchDir!empty.txt
set criticalError=1
goto end

::=============================================================================
:: Functions
::=============================================================================

::-----------------------------------------------------------------------------
:: Report error to NG and Kecal
:: %1 message
:: %2 error log file
:funcReportError
start !skype! Build !productName! failed: %1
if [%wantSendToNG%]==[1] call %SendToNG% %1 %2
echo Finished - %1
goto:eof

::-----------------------------------------------------------------------------
:: Build one configuration
:: %1 Version of application for echo
:: %2 Build configuration
:: %3 Full path to exe backup directory
:: %4 Name of application (without .exe)
:funcBuildConfiguration
SETLOCAL
set infoName=%1
set buildCfg=%2
::set exebackupFullPath=%3
::set outputBuildName=%4
call:Color 2 "*** Building !infoName! version..." \n
!devenv! !toolsln! /rebuild !buildCfg! /Out "!buildLog!"
echo errorlevel %errorlevel%
if errorlevel 1 goto buildFail
echo Build of !infoName! version finished successfully

ENDLOCAL
goto:eof

::-----------------------------------------------------------------------------
:: Sign app .exe
:: %1 Path to the exe application
:funcSignAppExe
SETLOCAL
set outputBuildName=%1
cmd /c call !signTool! !outputBuildName!
if errorlevel 1 goto signFailed
ENDLOCAL
goto:eof

::-----------------------------------------------------------------------------
:: v20. Arguments: hexColor text [\n]
:: Supported in windows XP, 7, 8.
:: In XP extended ascii characters are printed as dots.
:: For print quotes, use empty text.
:Color
SetLocal EnableExtensions EnableDelayedExpansion
Set "Text=%~2"
If Not Defined Text (Set Text=^")
Subst `: "!Temp!" >Nul &`: &Cd \
If Not Exist `.7 (
Echo(|(Pause >Nul &Findstr "^" >`)
Set /P "=." >>` <Nul
For /F "delims=;" %%# In (
'"Prompt $H;&For %%_ In (_) Do Rem"') Do (
Set /P "=%%#%%#%%#" <Nul >`.3
Set /P "=%%#%%#%%#%%#%%#" <Nul >`.5
Set /P "=%%#%%#%%#%%#%%#%%#%%#" <Nul >`.7))
Set /P "LF=" <` &Set "LF=!LF:~0,1!"
For %%# in ("!LF!") Do For %%_ In (
\ / :) Do Set "Text=!Text:%%_=%%~#%%_%%~#!"
For /F delims^=^ eol^= %%# in ("!Text!") Do (
If #==#! SetLocal DisableDelayedExpansion
If \==%%# (Findstr /A:%~1 . \` Nul
Type `.3) Else If /==%%# (Findstr /A:%~1 . /.\` Nul
Type `.5) Else (Echo %%#\..\`>`.dat
Findstr /F:`.dat /A:%~1 .
Type `.7))
If "\n"=="%~3" (Echo()
goto:eof

::-----------------------------------------------------------------------------
:: Sets the errorlevel and stops the batch immediately (Only use this at the end of script)!
:: %1 errorlevel to end application with
:haltProgram
call :__SetErrorLevel %1
call :__ErrorExit 2> nul
goto:eof

::-----------------------------------------------------------------------------
:: Sets the errorlevel of the appliacation
:: %1 errorlevel to end application with
:__SetErrorLevel
exit /b %1
goto :eof

::-----------------------------------------------------------------------------
:: Creates a syntax error, stops immediately (Don't use this anywhere!)
:__ErrorExit
()
goto :eof

::=============================================================================
:: End of building script
::=============================================================================
:end

::=============================================================================
:: Backup scripts
::=============================================================================
call:Color 2 "=====================================================================" \n
call:Color 2 "SERVER BUILD %productName% ENDED." \n
call:Color 2 "=====================================================================" \n
if [!criticalError!]==[1] call:haltProgram %errorlevel%
ENDLOCAL
exit /B 0
