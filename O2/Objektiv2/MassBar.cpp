// MassBar.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "MassBar.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMassBar dialog


CMassBar::CMassBar()
  : CDialogBar()
    {
    //{{AFX_DATA_INIT(CMassBar)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    //{{AFX_DATA_MAP(CMassBar)
    //}}AFX_DATA_MAP
    obj=NULL;
    }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CMassBar, CDialogBar)
  //{{AFX_MSG_MAP(CMassBar)
  ON_BN_CLICKED(IDC_APPLY, OnApply)
    ON_WM_DESTROY()
      //}}AFX_MSG_MAP
      ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()
        
        /////////////////////////////////////////////////////////////////////////////
        // CMassBar message handlers
        
        void CMassBar::Create(CWnd *parent, int menuid)
          {
          CDialogBar::Create(parent,IDD,CBRS_BOTTOM,menuid);
          SetBarStyle(GetBarStyle()| CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
          EnableDocking(CBRS_ALIGN_ANY);
          CString top;top.LoadString(IDS_MASSINFOTITLE);
          SetWindowText(top);
          main=parent;
          check.Attach(*GetDlgItem(IDC_CHECK));
          Mass.Attach(*GetDlgItem(IDC_MASS));  
          Update(obj);
          }

//--------------------------------------------------

void CMassBar::Update(ObjectData *obj)
  {
  if (obj==NULL)
    {
    check.EnableWindow(FALSE);
    Mass.EnableWindow(FALSE); 
    GetDlgItem(IDC_APPLY)->EnableWindow(FALSE);
    }
  else
    {
    int c=obj->CountSelectedPoints();
    check.EnableWindow(c);
    Mass.EnableWindow(c); 
    GetDlgItem(IDC_APPLY)->EnableWindow(c);
    char buff[50];
    float f=obj->GetSelectionMass();
    sprintf(buff,"%.3f",f);
    Mass.SetWindowText(buff);
    if( c==0|| c==obj->NPoints() )    
      check.EnableWindow(FALSE);    	
    else
      check.SetCheck(1);  
    }
  this->obj=obj;
  }

//--------------------------------------------------

class CISetMass:public CICommand
  {
  float mass;
  bool constTotal;
  public:
    CISetMass(float ms, bool ct):mass(ms),constTotal(ct) 
      {}
    CISetMass(istream& str) 
      {datard(str,mass);datard(str,constTotal);}
    virtual int Execute(LODObject *obj)
      {
      obj->Active()->SetSelectionMass(mass,constTotal);
      return 0;
      }
    virtual int Tag() const
      {return CITAG_SETMASS;}
    virtual void Save(ostream &str) 
      {datawr(str,mass);datawr(str,constTotal);}
  };

//--------------------------------------------------

CICommand *CISetMass_Create(istream& str)
  {
  return new CISetMass(str);
  }

//--------------------------------------------------

void CMassBar::OnApply() 
  {
  bool chk=check.GetCheck()==1;
  CString s;
  Mass.GetWindowText(s);
  float f;
  if (sscanf(s,"%f",&f)!=1) 
    {
    AfxMessageBox(IDS_INVALIDMASS);
    return;
    }
  comint.Begin(WString(IDS_SETMASS));
  comint.Run(new CISetMass(f,chk));
  main->SendMessage(WM_COMMAND,ID_VIEW_REFRESH,0);
  }

//--------------------------------------------------

void CMassBar::OnDestroy() 
  {
  check.Detach();
  Mass.Detach();
  CDialogBar::OnDestroy();
  }

//--------------------------------------------------


BOOL CMassBar::PreTranslateMessage(MSG* pMsg) 
{
if (IsDialogMessage(pMsg)) return TRUE;	
return CDialogBar::PreTranslateMessage(pMsg);
}


#include <afxpriv.h>
void CMassBar::OnLButtonDown(UINT nFlags, CPoint point)
{
  //drag also on static dialog components (Labels, disabled buttons,...)
  if (m_pDockBar != NULL /*&& OnToolHitTest(point, NULL) != -1*/)
  {
    // start the drag
    ASSERT(m_pDockContext != NULL);
    ClientToScreen(&point);
    m_pDockContext->StartDrag(point);
  }
  CWnd::OnLButtonDown(nFlags, point);
}
