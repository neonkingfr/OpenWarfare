// LodList.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "LodList.h"
#include "LodConfig.h"
#include "Objektiv2Doc.h"
#include "ComInt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLodList dialog


CLodList::CLodList()
: CDialogBar()
{
  //{{AFX_DATA_INIT(CLodList)
  // NOTE: the ClassWizard will add member initialization here
  //}}AFX_DATA_INIT
  //{{AFX_DATA_MAP(CLodList)
  // NOTE: the ClassWizard will add member initialization here
  //}}AFX_DATA_MAP
  inmenu=false;
}










BEGIN_MESSAGE_MAP(CLodList, CDialogBar)
  //{{AFX_MSG_MAP(CLodList)
  ON_MESSAGE(WM_EXITMENULOOP,OnExitMenuLoop)
  ON_WM_SIZE()
  ON_WM_DESTROY()
  ON_COMMAND(ID_LOD_DISPLAYDETAILS, OnLodDisplaydetails)
  ON_NOTIFY(NM_RCLICK, IDC_LIST, OnRclickList)
  ON_COMMAND(ID_LOD_DISPLAYSHORT, OnLodDisplayshort)
  ON_COMMAND(ID_LOD_COPY, OnLodCopy)
  ON_COMMAND(ID_LOD_INSERT, OnLodInsert)
  ON_COMMAND(ID_LOD_DELETE, OnLodDelete)
  ON_COMMAND(ID_LOD_PROPERIES, OnLodProperies)
  ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclkList)
  ON_WM_TIMER()
  ON_NOTIFY(NM_CLICK, IDC_LIST, OnClickList)
  ON_COMMAND(ID_LOD_SETASBACK, OnLodSetasback)
  ON_NOTIFY(LVN_BEGINLABELEDIT, IDC_LIST, OnBeginlabeleditList)
  ON_COMMAND_RANGE(100,1000,OnPluginPopupCommand)

  //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLodList message handlers
void CLodList::Create(CWnd *parent, int menuid)
{
  CDialogBar::Create(parent,IDD_LODLIST,CBRS_RIGHT,menuid);
  SetBarStyle(GetBarStyle()| CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
  EnableDocking(CBRS_ALIGN_ANY);
  CString top;top.LoadString(IDS_LODLIST);
  SetWindowText(top);
  notify=parent;
  list.Attach(::GetDlgItem(m_hWnd,IDC_LIST));
  list.SetImageList(&shrilist,LVSIL_SMALL);
  list.InsertColumn(0,WString(IDS_LODTITLE),LVCFMT_LEFT,100,0);
  list.InsertColumn(1,WString(IDS_LODPOINTSTITLE),LVCFMT_RIGHT,50,1);
  list.InsertColumn(2,WString(IDS_LODFACESTITLE),LVCFMT_RIGHT,50,2);
  //  text.LoadString(IDS_PROPVALUE);
  LoadBarSize(*this,160,0);
}










void CLodList::OnSize(UINT nType, int cx, int cy) 
{
  CDialogBar::OnSize(nType, cx, cy);
  if ((HWND)list) list.MoveWindow(5,5,cx-10,cy-10);
}










void CLodList::OnDestroy() 
{
  list.Detach();
  SaveBarSize(*this);
  CDialogBar::OnDestroy();	
}










void CLodList::UpdateListOfLods(LODObject *lodobject)
{  
  int max=lodobject->NLevels();
  lod=lodobject;
  float f=GetLODIndex();
  list.DeleteAllItems();
  WString s;
  for (int i=0;i<max;i++)
  {
    LVITEM it;
    float f=lodobject->Resolution(i);
    it.mask=LVIF_TEXT |LVIF_IMAGE |LVIF_PARAM;
    it.iItem=i;
    it.iSubItem=0;
    it.iImage=ILST_LODDEFINITION;
    it.lParam=*(LPARAM *)&f;
    s=LODResolToName(lodobject->Resolution(i));
    it.pszText=(char *)((LPCTSTR)s);
    list.InsertItem(&it);
  }
  UpdateReport();
  SelectLOD(lod->Resolution(lod->ActiveLevel()));
}










float CLodList::GetLODIndex()
{
  int i=list.GetNextItem(-1,LVNI_FOCUSED);    
  if (i==-1) return 1.0f;
  LVITEM it;
  it.mask=LVIF_PARAM;
  it.iItem=i;
  it.iSubItem=0;
  list.GetItem(&it);
  return *(float *)&it.lParam;
}










void CLodList::SelectLOD(float level)
{
  LPARAM p=*(LPARAM *)&level;
  int cnt=list.GetItemCount();
  for (int i=0;i<cnt;i++)
  {
    LVITEM it;
    it.mask=LVIF_PARAM;
    it.iItem=i;
    it.iSubItem=0;
    list.GetItem(&it);
    if (p==it.lParam)
    {
      list.SetItemState(i,LVIS_FOCUSED|LVIS_SELECTED,LVIS_FOCUSED|LVIS_SELECTED);	  	  
      list.EnsureVisible(i,FALSE);
    }
    else
      list.SetItemState(i,0,LVIS_SELECTED);
  }
}










void CLodList::UpdateReport()
{
  int cnt=list.GetItemCount();
  for (int i=0;i<cnt;i++)
  {
    LVITEM it;
    it.mask=LVIF_PARAM;
    it.iItem=i;
    it.iSubItem=0;
    list.GetItem(&it);
    float f=*(float *)&it.lParam;
    ObjectData *odata=lod->Level(lod->FindLevelExact(f));
    if (odata!=NULL)
    {
      char buff[32];
      list.SetItemText(i,1,_itoa(odata->NPoints(),buff,10));
      list.SetItemText(i,2,_itoa(odata->NFaces(),buff,10));
    }
  }
}










void CLodList::OnLodDisplaydetails() 
{
  ::SetWindowLong(list,GWL_STYLE,::GetWindowLong(list,GWL_STYLE) & ~(LVS_TYPEMASK) | LVS_REPORT );
  list.Invalidate();
}










void CLodList::OnRclickList(NMHDR* pNMHDR, LRESULT* pResult) 
{
  CMenu menu;
  LoadPopupMenu(IDR_LODMENU_POPUP,menu);
  bool report=(::GetWindowLong(list,GWL_STYLE) &  LVS_TYPEMASK)==LVS_REPORT;
  if (report)menu.CheckMenuItem(ID_LOD_DISPLAYDETAILS,MF_BYCOMMAND|MF_CHECKED);
  else menu.CheckMenuItem(ID_LOD_DISPLAYSHORT,MF_BYCOMMAND|MF_CHECKED);
  theApp.PluginPopupHook(IPluginGeneralPluginSide::PMLodList,menu,*this);
  CPoint pt;
  GetCursorPos(&pt);
  inmenu=true;  
  menu.TrackPopupMenu(TPM_LEFTALIGN|TPM_LEFTBUTTON|TPM_RIGHTBUTTON,pt.x,pt.y,this);  
  SetTimer(1,1000,NULL);inmenu=false;
  inmenu=false;
}










void CLodList::OnLodDisplayshort() 
{
  ::SetWindowLong(list,GWL_STYLE,::GetWindowLong(list,GWL_STYLE) & ~(LVS_TYPEMASK) | LVS_LIST);
  list.Invalidate();
}










class CILodInsert: public CICommand
{
protected:
  float lod;
  float srclod;
public:	  
  CILodInsert(istream &str) 
  {datard(str,lod);datard(str,srclod);}
  CILodInsert(float ld,float src): CICommand() 
  {lod=ld;srclod=src;}
  virtual int Execute(LODObject *obj)
  {
    int i;
    ObjectData *ob,*copy;
    i=obj->FindLevelExact(srclod);
    if (i>=0) copy=obj->Level(i);else copy=NULL;
    if (copy==NULL) ob=new ObjectData();else ob=new ObjectData(*copy);
    int res=(obj->InsertLevel(ob,lod)<0);
    if (res) delete ob;
    else
      obj->SelectLevel(obj->FindLevelExact(lod));
    return res;
  }	  
  virtual void Save(ostream &str) 
  {datawr(str,lod);datawr(str,srclod);}
  virtual int Tag() const 
  {return CITAG_LODINSERT;}
};










CICommand *CILodInsert_Create(istream& str)
{
  return new CILodInsert(str);
}










CICommand *CILodDelete_Create(istream& str)
{
  return new CILodDelete(str);
}










class CILodChange: public CILodInsert
{
public:
  CILodChange(istream& str): CILodInsert(str) 
  {}
  CILodChange(float src,float ld): CILodInsert(ld,src) 
  {}
  virtual int Execute(LODObject *obj)
  {
    bool err=!obj->ChangeResolution(srclod,lod);
    if (!err) obj->SelectLevel(obj->FindLevelExact(lod));
    comint.sectchanged=true;
    return err;
  }
  virtual int Tag() const 
  {return CITAG_LODCHANGE;}

};










CICommand *CILodChange_Create(istream& str)
{  
  return new CILodChange(str);
}










CICommand *CILodSetActive_Create(istream& str)
{
  return new CILodSetActive(str);
}










void CLodList::OnLodInsert(float srclod) 
{
  float subs=srclod>=LOD_SHADOW_MIN && srclod<LOD_SHADOW_MAX?LOD_SHADOW_MIN :
  (srclod>=LOD_EDIT_MIN && srclod<=LOD_EDIT_MAX?LOD_EDIT_MIN:0);  
  int cnt=lod->NLevels();
  float max=0;
  float res;
  for (int i=0;i<cnt;i++) 
  {
    res=lod->Resolution(i)-subs;
    if (res>max && res<1000.0f) max=res;    
  }
  max=max+1.0f;
  comint.Begin(WString(IDS_COM_INSERTLOD,max+subs));
  comint.Run(new CILodInsert(max+subs,srclod));
  UpdateListOfLods(lod);
}










void CLodList::OnLodCopy() 
{
  float f=GetLODIndex();
  OnLodInsert(f);
}










void CLodList::OnLodInsert() 
{
  OnLodInsert(-999999.0);
}










void CLodList::OnLodDelete() 
{
  float f=GetLODIndex();
  comint.Begin(WString(IDS_COM_DELETELOD,f)); 	
  comint.Run(new CILodDelete(f));
  UpdateListOfLods(lod);
}










void CLodList::OnLodProperies() 
{
  float f=GetLODIndex();
  CLodConfig lc;
  lc.lod=f;
  if (lc.DoModal()==IDOK)
  {
    comint.Begin(WString(IDS_COM_CHANGELOD,(LPCTSTR)LODResolToName(f),(LPCTSTR)LODResolToName(f)));
    comint.Run(new CILodChange(f,lc.lod));
    UpdateListOfLods(lod);
    doc->UpdateAllToolbars(1);
    doc->UpdateAllViews(NULL);
  }
}










void CLodList::OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult) 
{
  float f=GetLODIndex();
  comint.Begin(WString(IDS_COM_CHANGEACTIVE,(LPCTSTR)LODResolToName(f)));
  comint.Run(new CILodSetActive(f));
  notify->SendMessage(WM_COMMAND,ID_VIEW_REFRESH);
  KillTimer(1);
}










LRESULT CLodList::OnExitMenuLoop(WPARAM wParam, LPARAM lParam)
{
  return 0;
}










void CLodList::OnTimer(UINT nIDEvent) 
{
  if (nIDEvent==1 && !inmenu)
  {
    SelectLOD(lod->Resolution(lod->ActiveLevel()));
    KillTimer(nIDEvent);
  }
}










void CLodList::OnClickList(NMHDR* pNMHDR, LRESULT* pResult) 
{
  SetTimer(1,1000,NULL);inmenu=false;
}










void CLodList::OnLodSetasback() 
{
  float f=GetLODIndex();
  doc->bgrnlod=f;
  doc->UpdateAllViews(NULL);
}










void CLodList::OnBeginlabeleditList(NMHDR* pNMHDR, LRESULT* pResult) 
{
  LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
  // TODO: Add your control notification handler code here
  OnLodProperies()	;
  *pResult = 1;
}


void CLodList::OnPluginPopupCommand(UINT cmd)
{
  theApp.HookMenuCommand(cmd);
}







