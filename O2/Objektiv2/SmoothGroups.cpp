#include "stdafx.h"
#include "SmoothGroups.h"

struct SSmoothGroupEdgeInfo
  {
  int vxfrom;
  int vxto;
  DWORD flags;
  };

static void AddEdgeFlags(THASHTABLE *tbl, int vxfrom, int vxto, DWORD flags)
  {
  if (vxfrom>vxto)
    {
    vxfrom^= vxto;
    vxto^= vxfrom;
    vxfrom^= vxto;
    }
  SSmoothGroupEdgeInfo nfo;
  nfo.vxfrom=vxfrom;
  nfo.vxto=vxto;
  nfo.flags=flags;
  SSmoothGroupEdgeInfo *found=(SSmoothGroupEdgeInfo  *)Hash_Find(tbl,&nfo);
  if (found==NULL) Hash_Add(tbl,&nfo);
  else 
    found->flags &= flags;
  }

static void BuildEdges(THASHTABLE *tbl, ObjectData &obj)
  {
  SSmoothGroupEdgeInfo *nfo=NULL;
  while ((nfo=(SSmoothGroupEdgeInfo *)Hash_EnumData(tbl,(void *)nfo))!=NULL)
    if (nfo->flags==0) 
      obj.AddSharpEdge(nfo->vxfrom,nfo->vxto);    
  }

void HandleSmoothGroups(DWORD *groups, ObjectData &obj, bool selfaces)
  {
  int cnt=obj.NFaces();
  int g=0;
  THASHTABLE tbl;
  Hash_Init2(&tbl,10001,sizeof(SSmoothGroupEdgeInfo),0,sizeof(int)*2,0);
  for (int i=0;i<cnt;i++) if (!selfaces || obj.FaceSelected(i))
    {
    FaceT fc(obj,i);
    for (int j=0;j<fc.N();j++)
      {
      int k=j+1;
      if (k>=fc.N()) k-=fc.N();
      AddEdgeFlags(&tbl,fc.GetPoint(j),fc.GetPoint(k),groups[g]);      
      }
    g++;
    }
  BuildEdges(&tbl,obj);
  Hash_Done(&tbl);
  }
