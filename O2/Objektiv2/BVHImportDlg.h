#if !defined(AFX_BVHIMPORTDLG_H__F96B6879_BB77_4C09_8308_916EEDB8519B__INCLUDED_)
#define AFX_BVHIMPORTDLG_H__F96B6879_BB77_4C09_8308_916EEDB8519B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BVHImportDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBVHImportDlg dialog

struct SBVHHierarchyItem;

class CBVHImportDlg : public CDialog
  {
  // Construction
  public:
    CBVHImportDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CBVHImportDlg)
    enum 
      { IDD = IDD_BVHIMPORT };
    CButton	extrms;
    CListCtrl	list;
    float	scale;
    BOOL	ztop;    
    BOOL	invertX;
    BOOL	invertY;
    BOOL	invertZ;
    //}}AFX_DATA
    
    SBVHHierarchyItem *itemlist;
    int itemcount;
    
    CSize sz;
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CBVHImportDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CBVHImportDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    virtual void OnOK();
    afx_msg void OnEndlabeleditList1(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnExterms();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BVHIMPORTDLG_H__F96B6879_BB77_4C09_8308_916EEDB8519B__INCLUDED_)
