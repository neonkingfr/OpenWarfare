// Transform2D.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "Transform2D.h"
#include "Objektiv2Doc.h"
#include "Objektiv2View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTransform2D dialog


CTransform2D::CTransform2D(CWnd* pParent /*=NULL*/)
  : CDialog(CTransform2D::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CTransform2D)
    UsePin = FALSE;
    Value = 0.0f;
    //}}AFX_DATA_INIT
    toview=NULL;
    mode=em_rotate;
    }

//--------------------------------------------------

void CTransform2D::DoDataExchange(CDataExchange* pDX)
  {
  if (pDX->m_bSaveAndValidate)
    CorrectFloatValues(this,IDC_VALUE,0);
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CTransform2D)
  DDX_Check(pDX, IDC_PIN, UsePin);
  DDX_Text(pDX, IDC_VALUE, Value);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CTransform2D, CDialog)
  //{{AFX_MSG_MAP(CTransform2D)
  ON_BN_CLICKED(IDC_PREVIEW, OnPreview)
    ON_WM_DESTROY()
      ON_WM_TIMER()
        //}}AFX_MSG_MAP
        END_MESSAGE_MAP()
          
          /////////////////////////////////////////////////////////////////////////////
          // CTransform2D message handlers
          
          BOOL CTransform2D::OnInitDialog() 
            {
            CDialog::OnInitDialog();
            
            SetWindowText(WString(idctext));
            SetTimer(5000,5000,NULL);	
            return TRUE;  // return TRUE unless you set the focus to a control
            // EXCEPTION: OCX Property Pages should return FALSE
            }

//--------------------------------------------------

void CTransform2D::OnPreview() 
  {
  char buff[256];
  GetDlgItemText(IDC_VALUE,buff,sizeof(buff));
  char *c;
  while ((c=strchr(buff,','))!=NULL) *c='.';
  float f;
  bool usepin=IsDlgButtonChecked(IDC_PIN)!=0;
  sscanf(buff,"%f",&f);
  if (toview==NULL) 
    {
    GetDlgItem(IDC_PREVIEW)->EnableWindow(FALSE);
    }
  else
    {	
    HMATRIX mm;
    switch (mode)
      {
      case em_rotate:toview->CalcRotationOnPlane(torad(f),mm,usepin,true);break;
      case em_scale:toview->CalcScaleOnPlane(f,mm,usepin,true);break;
      }
    doc->TransformPreview(mm);
    doc->UpdateAllViews(NULL);
    }
  }

//--------------------------------------------------

void CTransform2D::OnDestroy() 
  {
  HMATRIX mm;
  JednotkovaMatice(mm);
  doc->TransformPreview(mm);
  CDialog::OnDestroy(); 	
  doc->UpdateAllViews(NULL);
  }

//--------------------------------------------------

void CTransform2D::OnTimer(UINT nIDEvent) 
  {
  OnPreview();	
  CDialog::OnTimer(nIDEvent);
  }

//--------------------------------------------------

