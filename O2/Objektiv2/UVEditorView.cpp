// UVEditorView.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "UVEditorView.h"
#include ".\uveditorview.h"
#include <el/MultiThread/HashTable.h>
#include "../ObjektivLib/ObjectData.h"
#include "../ObjektivLib/Edges.h"

#define SELPT_SIZE 2
#define SELUCHYT_SIZE 12
#define SNAPTOLERANCE 16

// UVEditorView

IMPLEMENT_DYNAMIC(UVEditorView, CWnd)
UVEditorView::UVEditorView()
{
  _scrtrackbeg=0;
  SetWhiteMode(false);
  _dragMode=DragNone;
  _zoomMode=false;
  _previewCache=0;
  _autoSnap=false;
}

UVEditorView::~UVEditorView()
{
  if (m_hWnd) DestroyWindow();
}

struct SPreviewXORBlindPreventStruct
{
  POINT from;
  POINT to;
  SPreviewXORBlindPreventStruct();
  SPreviewXORBlindPreventStruct(const POINT &pt1, const POINT &pt2)
  {
    if (pt1.x<pt2.x)
    {
      from=pt1;
      to=pt2;
    }
    else if (pt1.x>pt2.x)
    {
      from=pt2;
      to=pt1;
    }
    else if (pt1.y>pt2.y)
    {
      from=pt1;
      to=pt2;
    }
    else 
    {
      from=pt2;
      to=pt1;
    }
  }
  unsigned int operator%(int mask) const
  {
    unsigned int carry=from.x%mask;
    carry=(from.y+carry*65536)%mask;
    carry=(to.x+carry*65536)%mask;
    carry=(to.y+carry*65536)%mask;
    return carry;
  }
  bool operator==(const SPreviewXORBlindPreventStruct &other) const
  {
    return from.x==other.from.x && from.y==other.from.y &&   
        to.x==other.to.x && to.y==other.to.y;
  }
  bool operator!=(const SPreviewXORBlindPreventStruct &other) const
  {
    return !operator==(other);
  }
};

BEGIN_MESSAGE_MAP(UVEditorView, CWnd)
  ON_WM_PAINT()
  ON_WM_DESTROY()
  ON_WM_ERASEBKGND()
  ON_WM_VSCROLL()
  ON_WM_HSCROLL()
  ON_WM_MOUSEWHEEL()
  ON_WM_LBUTTONDOWN()
  ON_WM_LBUTTONDBLCLK()
  ON_WM_LBUTTONUP()
  ON_WM_MOUSEMOVE()
  ON_WM_RBUTTONDOWN()
  ON_WM_RBUTTONUP()
  ON_WM_SETCURSOR()
  ON_WM_SIZE()
END_MESSAGE_MAP()


bool UVEditorView::Create(CWnd *parent, IUVEditorViewEvent *notify,unsigned long id, unsigned long style,unsigned long exstyle)
{
  BOOL res=CWnd::CreateEx(exstyle,AfxRegisterWndClass(CS_DBLCLKS,0,0,0),"",WS_CHILD|WS_VISIBLE|style,
    CRect(0,0,0,0),parent,id);
  if (res==FALSE) return false;
  _owner=notify;
  _uvbounding=FRect(0,0,1,1);
  _origin=FPoint2D(0,0);
  _zoom=1.0f;
  LoadDefaultTexture();
  return true;
}

bool UVEditorView::LoadTexture(BITMAPINFO *bitmap, const char *bitmapdata)
{
  CDC *windc=GetDC();
  
  if (_backbmpbitmap.GetSafeHandle()) _backbmpbitmap.DeleteObject();
  _backbmpbitmap.CreateCompatibleBitmap(windc,bitmap->bmiHeader.biWidth,bitmap->bmiHeader.biHeight);
  
  SetDIBits(*windc,_backbmpbitmap,0,bitmap->bmiHeader.biHeight,bitmapdata,bitmap,DIB_RGB_COLORS);

  ReleaseDC(windc);

  _bmpsize.cx=bitmap->bmiHeader.biWidth;
  _bmpsize.cy=bitmap->bmiHeader.biHeight;
  Invalidate(FALSE);
  return true;
}

FTransform UVEditorView::CalcFloatToPixelTransf()
{
  CSize zs(toLargeInt(_bmpsize.cx*_zoom),toLargeInt(_bmpsize.cy*_zoom));
  FTransform res;
  res.mx[2][0]=_origin.x*zs.cx;
  res.mx[2][1]=_origin.y*zs.cy;
  res.mx[0][0]=zs.cx;
  res.mx[1][1]=zs.cy;
  res.mx[0][1]=0;
  res.mx[1][0]=0;
  return res;
}

FTransform UVEditorView::CalcPixelToFloatTransf()
{
  CSize zs(toLargeInt(_bmpsize.cx*_zoom),toLargeInt(_bmpsize.cy*_zoom));
  FTransform res;
  res.mx[2][0]=-_origin.x;
  res.mx[2][1]=-_origin.y;
  res.mx[0][0]=1.0f/zs.cx;
  res.mx[1][1]=1.0f/zs.cy;
  res.mx[0][1]=0;
  res.mx[1][0]=0;
  return res;
}


static bool TestLineShownInRect(const POINT &pt1, const POINT &pt2, const RECT &rc)
{
  if (PtInRect(&rc,pt1) || PtInRect(&rc,pt2)) return true;
  POINT diff;
  diff.x=pt2.x-pt1.x;
  diff.y=pt2.y-pt1.y; //x=p1.x+k*diff.x  => k =x-p1.x/diff.x
  float k;
  float y;
  if (diff.x!=0)
  {  
    k=((rc.left-pt1.x))/(float)diff.x;
    if (k>=0 && k<=1.0f)
    {  
      y=pt1.y+diff.y*k;
      if (y>=rc.top && y<=rc.bottom) return true;
    }
    k=((rc.right-pt1.x))/(float)diff.x;
    if (k>=0 && k<=1.0f)
    {  
      y=pt1.y+diff.y*k;
      if (y>=rc.top && y<=rc.bottom) return true;
    }
  }
  if (diff.y!=0)
  {  
    k=((rc.top-pt1.y))/(float)diff.y;
    if (k>=0 && k<=1.0f)
    {  
      y=pt1.x+diff.x*k;
      if (y>=rc.left && y<=rc.right) return true;
    }
    k=((rc.bottom-pt1.y))/(float)diff.y;
    if (k>=0 && k<=1.0f)
    {  
      y=pt1.x+diff.x*k;
      if (y>=rc.left && y<=rc.right) return true;
    }
  }
  return false;
}

static bool TestPolyShownInRect(const POINT *pts,int count,const RECT &rct)
{
  for (int i=0;i<count-1;i++)
    if (TestLineShownInRect(pts[i],pts[i+1],rct)) return true;
  return TestLineShownInRect(pts[count-1],pts[0],rct);
}

static void InvertRect(CDC *dc,int x,int y, int xs, int ys)
{
  CRect rc(x,y,x+xs,y+ys);
  dc->InvertRect(&rc);  
}

static void DrawUchytky(CDC *dc, CRect rect)
{
  InvertRect(dc,rect.left-SELUCHYT_SIZE,rect.top-SELUCHYT_SIZE,SELUCHYT_SIZE,SELUCHYT_SIZE);
  InvertRect(dc,(rect.left+rect.right-SELUCHYT_SIZE)>>1,rect.top-SELUCHYT_SIZE,SELUCHYT_SIZE,SELUCHYT_SIZE);
  InvertRect(dc,rect.right,rect.top-SELUCHYT_SIZE,SELUCHYT_SIZE,SELUCHYT_SIZE);
  InvertRect(dc,rect.right,(rect.top+rect.bottom-SELUCHYT_SIZE)>>1,SELUCHYT_SIZE,SELUCHYT_SIZE);
  InvertRect(dc,rect.right,rect.bottom,SELUCHYT_SIZE,SELUCHYT_SIZE);
  InvertRect(dc,(rect.left+rect.right-SELUCHYT_SIZE)>>1,rect.bottom,SELUCHYT_SIZE,SELUCHYT_SIZE);
  InvertRect(dc,rect.left-SELUCHYT_SIZE,rect.bottom,SELUCHYT_SIZE,SELUCHYT_SIZE);
  InvertRect(dc,rect.left-SELUCHYT_SIZE,(rect.top+rect.bottom-SELUCHYT_SIZE)>>1,SELUCHYT_SIZE,SELUCHYT_SIZE);
}



void UVEditorView::DrawToDC(CDC &dc, const CRect &dirty)
{
  CSize zs(toLargeInt(_bmpsize.cx*_zoom),toLargeInt(_bmpsize.cy*_zoom));
  CPoint offs(toLargeInt(_origin.x*zs.cx),toLargeInt(_origin.y*zs.cy));
  if (zs.cx==0 || zs.cy==0) return;
  while (offs.x>dirty.left) offs.x-=zs.cx;
  while (offs.y>dirty.top) offs.y-=zs.cy;
  while (offs.x+zs.cx<=dirty.left) offs.x+=zs.cx;
  while (offs.y+zs.cy<=dirty.top) offs.y+=zs.cy;

  CDC bmpdc;
  bmpdc.CreateCompatibleDC(&dc);
  CBitmap *oldbmp=bmpdc.SelectObject(&_backbmpbitmap);
  CPen border(PS_DOT,1,RGB(255,255,255)); 
  dc.SetBkColor(0);
  CPen *prevborder=dc.SelectObject(&border);

  float factorx=(float)_bmpsize.cx/(float)zs.cx,factory=(float)_bmpsize.cy/(float)zs.cy; 

  if (factorx<1 && factory<1) dc.SetStretchBltMode(HALFTONE); 
  else dc.SetStretchBltMode(COLORONCOLOR);   

  DWORD wr=GetTickCount();
  for (int y=offs.y;y<dirty.bottom;y+=zs.cy)
  {
    if (GetTickCount()-wr>500) break;  
    for (int x=offs.x;x<dirty.right;x+=zs.cx)
    {
      CRect pos(x,y,x+zs.cx,y+zs.cy);
      pos.IntersectRect(pos,dirty);
      
      FRect src(floor((pos.left-x)*factorx),floor((pos.top-y)*factory), 
        ceil((pos.right-x)*factorx),ceil((pos.bottom-y)*factory)); 

      pos=CRect(toLargeInt(src.leftTop.x/factorx+x),
                toLargeInt(src.leftTop.y/factory+y),
                toLargeInt(src.rightBottom.x/factorx+x),
                toLargeInt(src.rightBottom.y/factory+y)); 

      dc.StretchBlt(
        pos.left,
        pos.top,
        pos.right-pos.left,
        pos.bottom-pos.top,
        &bmpdc,
        toLargeInt(src.leftTop.x),
        toLargeInt(src.leftTop.y),
        toLargeInt(src.Cx()),
        toLargeInt(src.Cy()),
        SRCCOPY); 
      dc.MoveTo(x,y+zs.cy-1);
      dc.LineTo(x+zs.cx-1,y+zs.cy-1);
      dc.LineTo(x+zs.cx-1,y);
    }
  }
  dc.SelectObject(prevborder);

  FTransform trns=CalcFloatToPixelTransf();

  dc.SelectStockObject(DC_PEN);
  dc.SelectStockObject(NULL_BRUSH);
  dc.SetDCPenColor(_backUvColor);
  for (int i=0;i<_backgroundUV.Size();i++) 
  {
    UVEditorViewObject &obj=_backgroundUV[i]; 
    DrawUVObject(dc,obj,trns,false,dirty);
  }
  dc.SetDCPenColor(_linecolor);  
  for (int i=0;i<_uvObjects.Size();i++)
  {
    UVEditorViewObject &obj=_uvObjects[i];
    if (!obj.IsAllVerticesSelected())
    {
      DrawUVObject(dc,obj,trns,true,dirty);
    }
  }
  dc.SetDCPenColor(_selectcolor);  
  for (int i=0;i<_uvObjects.Size();i++)
  {
    UVEditorViewObject &obj=_uvObjects[i];
    if (obj.IsAllVerticesSelected())
    {
      DrawUVObject(dc,obj,trns,false,dirty);
    }
  }
  for (int i=0;i<_uvObjects.Size();i++)
  {
    UVEditorViewObject &obj=_uvObjects[i];
    for (int j=0;j<obj.n;j++)    
      if (obj.IsSelectedVertex(j))
      {
        FPoint2D z=trns.Transform(FPoint2D(obj.uv[j].u,obj.uv[j].v));
        CPoint pts(toLargeInt(z.x),toLargeInt(z.y));
        if (dirty.PtInRect(pts))
        {
          dc.FillSolidRect(pts.x-SELPT_SIZE,pts.y-SELPT_SIZE,SELPT_SIZE*2,SELPT_SIZE*2,_selectcolor);
        }
      }
  }
  /*

  CPen drawPen(PS_SOLID,1,_linecolor);
  CPen colPen[2];
  CPen selPen(PS_SOLID,1,_selectcolor);
  CPen *old=dc.SelectObject(&drawPen);
  dc.SelectStockObject(NULL_BRUSH);
  bool seldraw=false;
  int colSwapper=-1;
  COLORREF lastcol=0xFFFFFFFF;

  for (int sm=0;sm<2;sm++)
  {
    if (sm) dc.SelectObject(&selPen);
    for (int i=0;i<_uvObjects.Size();i++)
    {
      UVEditorViewObject &obj=_uvObjects[i];
      POINT pts[MAX_DATA_POLY];    


      if ((sm!=0) == (obj.IsAllVerticesSelected()))
      {    
        for (int j=0;j<obj.n;j++)
        {
          FPoint2D z=trns.Transform(FPoint2D(obj.uv[j].u,obj.uv[j].v));
          pts[j].x=toLargeInt(z.x);
          pts[j].y=toLargeInt(z.y);      
        }
        if (TestPolyShownInRect(pts,obj.n,dirty))
        {
          if (sm==0 && obj.color!=lastcol)
          {
            if (obj.color!=0)
            {            
              colSwapper=!colSwapper;
              if (colPen[colSwapper].m_hObject) colPen[colSwapper].DeleteObject(); 
              colPen[colSwapper].CreatePen(PS_SOLID,1,obj.color);
              dc.SelectObject(colPen[colSwapper]);
            }
            else
            {
              dc.SelectObject(&drawPen); 
            }
            lastcol=obj.color;
          }
          dc.Polygon(pts,obj.n);
        }
        for (int j=0;j<obj.n;j++) if (obj.IsSelectedVertex(j) && dirty.PtInRect(pts[j]))
        {
          dc.FillSolidRect(pts[j].x-SELPT_SIZE,pts[j].y-SELPT_SIZE,SELPT_SIZE*2,SELPT_SIZE*2,_selectcolor);
        }
      }
    }
  }
  */
    if (!_selArea.IsEmpty())
    {  
      CRect selArea=TransformToInt(trns,_selArea);
      DrawUchytky(&dc,selArea);
    }


    bmpdc.SelectObject(oldbmp);
}

void UVEditorView::DrawUVObject(CDC &dc,UVEditorViewObject &obj, const FTransform &trns, bool colorize,const CRect &dirty)
{
  POINT pts[MAX_DATA_POLY];    
  COLORREF curColor=dc.GetDCPenColor();


  for (int j=0;j<obj.n;j++)
  {
    FPoint2D z=trns.Transform(FPoint2D(obj.uv[j].u,obj.uv[j].v));
    pts[j].x=toLargeInt(z.x);
    pts[j].y=toLargeInt(z.y);      
  }
  
  if (TestPolyShownInRect(pts,obj.n,dirty))
  {
    if (colorize && obj.color!=0)      
        dc.SetDCPenColor(obj.color);

    dc.Polygon(pts,obj.n);

    if (colorize && obj.color!=curColor)
      dc.SetDCPenColor(curColor);
  }
}

void UVEditorView::OnPaint()
{
  CPaintDC dc(this); 
  CRect dirty=dc.m_ps.rcPaint;
  DrawToDC(dc,dirty);
}

bool UVEditorView::LoadDefaultTexture()
{
  CDC *windc=GetDC();

  CDC dc;
  dc.CreateCompatibleDC(windc);

  if (_backbmpbitmap.GetSafeHandle()) _backbmpbitmap.DeleteObject();
  _backbmpbitmap.CreateCompatibleBitmap(windc,512,512);

  ReleaseDC(windc);

  CBitmap *oldbmp=dc.SelectObject(&_backbmpbitmap);
  dc.SelectStockObject(NULL_BRUSH);
  for (int i=0;i<256;i++)
  {
    CPen pen;
    int color=i/2+64;
    pen.CreatePen(PS_SOLID,1,RGB(color,color,color));
    CPen *oldpen=dc.SelectObject(&pen);
    dc.Rectangle(i,i,512-i,512-i);
    dc.SelectObject(oldpen);
  }
  dc.SelectObject(oldbmp); 
  _bmpsize.cx=512;
  _bmpsize.cy=512;
  return true;
}
void UVEditorView::OnDestroy()
{
  CWnd::OnDestroy();
  _backbmpbitmap.DeleteObject();
  if (_previewCache)
    delete reinterpret_cast<HashTable::Expandable<SPreviewXORBlindPreventStruct> *>(_previewCache);
  _previewCache=0;
}

BOOL UVEditorView::OnEraseBkgnd(CDC* pDC)
{
  return 1;
}

void UVEditorView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
  int pos=(int)nPos;
  float newtrck;
  CSize zs(toLargeInt(_bmpsize.cx*_zoom),toLargeInt(_bmpsize.cy*_zoom));
  FPoint2D oldorigin=_origin;
  int rmin,rmax;
  GetScrollRange(SB_VERT,&rmin,&rmax);
  switch (nSBCode)
  {
  case SB_TOP:break;
  case SB_BOTTOM: break;
  case SB_PAGEUP: _origin.y+=(-rmin)/(float)zs.cy;break;
  case SB_PAGEDOWN: _origin.y-=(-rmin)/(float)zs.cy;break;
  case SB_LINEUP: _origin.y+=0.1f/_zoom;break;
  case SB_LINEDOWN: _origin.y-=0.1f/_zoom;break;
  case SB_THUMBTRACK:
  case SB_THUMBPOSITION: newtrck=-(float)pos/(float)zs.cy;
    _origin.y=_origin.y-_scrtrackbeg+newtrck;
    _scrtrackbeg=newtrck;
    break;
  case SB_ENDSCROLL:_scrtrackbeg=0; break;
  }
  ScrollOriginTo(oldorigin,_origin);
}

void UVEditorView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
  int pos=(int)nPos;
  float newtrck;
  CSize zs(toLargeInt(_bmpsize.cx*_zoom),toLargeInt(_bmpsize.cy*_zoom));
  FPoint2D oldorigin=_origin;
  int rmin,rmax;
  GetScrollRange(SB_HORZ,&rmin,&rmax);
  switch (nSBCode)
  {
  case SB_LEFT:break;
  case SB_RIGHT: break;
  case SB_PAGELEFT: _origin.x+=(-rmin)/(float)zs.cx;break;
  case SB_PAGERIGHT: _origin.x-=(-rmin)/(float)zs.cx;break;
  case SB_LINELEFT: _origin.x+=0.1f/_zoom;break;
  case SB_LINERIGHT: _origin.x-=0.1f/_zoom;break;
  case SB_THUMBTRACK:
  case SB_THUMBPOSITION: newtrck=-(float)pos/(float)zs.cx;
    _origin.x=_origin.x-_scrtrackbeg+newtrck;
    _scrtrackbeg=newtrck;
    break;
  case SB_ENDSCROLL:_scrtrackbeg=0; break;
  }

  ScrollOriginTo(oldorigin,_origin);

  CWnd::OnHScroll(nSBCode, nPos, pScrollBar);
}

void UVEditorView::SetZoom(float zoom, CPoint staticPt)
{ 
  FTransform z1=CalcPixelToFloatTransf(); 
  FPoint2D pt1=z1.Transform(FPoint2D(staticPt.x,staticPt.y));
  _zoom=zoom;
  FTransform z2=CalcPixelToFloatTransf();
  FPoint2D pt2=z2.Transform(FPoint2D(staticPt.x,staticPt.y));
  _origin=_origin+(pt2-pt1);
  Invalidate();
}

BOOL UVEditorView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
  pt=CPoint(GetMessagePos());
  ScreenToClient(&pt);
  if (zDelta>0) SetZoom(_zoom/0.9f,pt);else SetZoom(_zoom*0.9f,pt);
  return 1;
}

FRect UVEditorView::CalculateUVBounding(bool selected)
{
  FRect uvb(FLT_MAX,FLT_MAX,-FLT_MAX,-FLT_MAX);  
  if (_uvObjects.Size())
  {
    int i;
    for (i=0;i<_uvObjects.Size();i++)
    {
      UVEditorViewObject &obj=_uvObjects[i];
      for (int j=0;j<obj.n;j++) if (!selected || obj.IsSelectedVertex(j))
      {
        if (uvb.leftTop.x>obj.uv[j].u)
          uvb.leftTop.x=obj.uv[j].u;
        if (uvb.leftTop.y>obj.uv[j].v)
          uvb.leftTop.y=obj.uv[j].v;
        if (uvb.rightBottom.x<obj.uv[j].u)
          uvb.rightBottom.x=obj.uv[j].u;
        if (uvb.rightBottom.y<obj.uv[j].v)
          uvb.rightBottom.y=obj.uv[j].v;
      }
    }
  }
  if (!uvb.IsValid())
  {
    uvb=FRect(0,0,0,0);
  }
  return uvb;
}

void UVEditorView::LoadUVs(const Array<UVEditorViewObject> &uvs, bool keepsel)
{
  int a=0;
  unsigned long *selInfo;
  if (keepsel)
  {  
    selInfo=(unsigned long *)alloca(sizeof(unsigned long)*uvs.Size());
    memset(selInfo,0,sizeof(unsigned long)*uvs.Size());
    for (int i=0;i<_uvObjects.Size();i++) 
    {
      UVEditorViewObject &obj=_uvObjects[i];
      if (obj.selectedVertices)
      {
        while (a<uvs.Size() && uvs[a].faceIndex<obj.faceIndex) a++;
        if (a>=uvs.Size()) break;
        if (uvs[a].faceIndex==obj.faceIndex)
        {
          selInfo[a]=obj.selectedVertices;
        }
      }
    }
  }
  _uvObjects=uvs;
  Invalidate(FALSE);
  _uvbounding=CalculateUVBounding(false);
  if (_uvbounding.IsNull())
  {
    _uvbounding=FRect(0,0,1,1);
  }
  if (keepsel)   
    for (int i=0;i<_uvObjects.Size();i++) _uvObjects[i].selectedVertices=selInfo[i];
 _selArea=CalculateUVBounding(true);
 CorrectSelArea();
 _snapPoints.Clear();
}


static bool MouseInUchyt(CPoint mouse, int xuchyt, int yuchyt, int xdif, int ydif)
{
  xuchyt+=xdif*SELUCHYT_SIZE/2-(SELUCHYT_SIZE-SELUCHYT_SIZE/2);
  yuchyt+=ydif*SELUCHYT_SIZE/2-(SELUCHYT_SIZE-SELUCHYT_SIZE/2);
  CRect rcuchyt(xuchyt,yuchyt,xuchyt+SELUCHYT_SIZE,yuchyt+SELUCHYT_SIZE);
  return rcuchyt.PtInRect(mouse)!=FALSE;
}

int UVEditorView::HitTestToPoint(const CPoint &mouse)
{
  FTransform backTrns=CalcPixelToFloatTransf();
  FRect fr(backTrns.Transform(FPoint2D(mouse.x-SELPT_SIZE-2,mouse.y-SELPT_SIZE-2)),
    backTrns.Transform(FPoint2D(mouse.x+SELPT_SIZE+2,mouse.y+SELPT_SIZE+2)));
  FPoint2D center=fr.CenterPoint();
  int bestPt=-1;
  float bestDist=FLT_MAX;
  for (int i=0;i<_uvObjects.Size();i++) 
  {
    const UVEditorViewObject &item=_uvObjects[i];
    if (item.selectedVertices)
    {
      for (int j=0;j<item.n;j++)
      {
        if (item.IsSelectedVertex(j))
        {
          FPoint2D f(item.uv[j].u,item.uv[j].v);
          if (fr.PtInRect(f))
          {
            float dist=f.Distance2(center);
            if (dist<bestDist)
            {
              bestDist=dist;
              bestPt=i*MAX_DATA_POLY+j;
            }
          }
        }
      }
    }
  }
  return bestPt;
}

template<class Allocator>
bool UVEditorView::HitTestToPoint(const CPoint &mouse, AutoArray<int,Allocator> &indexList)
{
  int bestPt=HitTestToPoint(mouse);
  indexList.Clear();
  if (bestPt==-1) return false;
  indexList.Append(bestPt);
  const ObjUVSet_Item &fp=_uvObjects[bestPt/MAX_DATA_POLY].uv[bestPt%MAX_DATA_POLY];
  FPoint2D test(fp.u,fp.v);
  for (int i=0;i<_uvObjects.Size();i++) 
  {
    const UVEditorViewObject &item=_uvObjects[i];
    if (item.selectedVertices)
    {
      for (int j=0;j<item.n;j++)
      {
        if (item.IsSelectedVertex(j))
        {
          FPoint2D f(item.uv[j].u,item.uv[j].v);
          if (test.Distance2(f)<1e-10f)
          {
            int newid=i*MAX_DATA_POLY+j;
            if (newid!=bestPt)
              indexList.Append(newid);
          }
        }
      }
    }
  }
  return true;
}


UVEditorView::DragMode UVEditorView::DragHitTest(const CPoint &point, CRect *area)
{
  if (_selArea.IsEmpty()) return DragRect;
  FTransform trns=CalcFloatToPixelTransf();
  CRect selArea=TransformToInt(trns,_selArea);
  DragMode mode;

  if (MouseInUchyt(point,selArea.left,selArea.top,-1,-1)) mode=DragTopLeft;
  else if (MouseInUchyt(point,(selArea.left+selArea.right)>>1,selArea.top,0,-1)) mode=DragTop;
  else if (MouseInUchyt(point,selArea.right,selArea.top,1,-1)) mode=DragRightTop;
  else if (MouseInUchyt(point,selArea.right,(selArea.top+selArea.bottom)>>1,1,0)) mode=DragRight;
  else if (MouseInUchyt(point,selArea.right,selArea.bottom,1,1)) mode=DragRightBottom;
  else if (MouseInUchyt(point,(selArea.right+selArea.left)>>1,selArea.bottom,0,1)) mode=DragBottom;
  else if (MouseInUchyt(point,selArea.left,selArea.bottom,-1,1)) mode=DragBottomLeft;
  else if (MouseInUchyt(point,selArea.left,(selArea.bottom+selArea.top)>>1,-1,0)) mode=DragLeft;
  else if (selArea.PtInRect(point)) mode=DragMove;
  else mode=DragRect;
  if (area) *area=selArea;
  return mode;

}
void UVEditorView::OnLButtonDown(UINT nFlags, CPoint point)
{
  bool alt=GetKeyState(VK_MENU)<0;
  CRect selArea;
  if (_zoomMode && !alt)
  {
    _dragMode=DragZoom;
    _dragArea=CRect(point,point);
    _transformArea.Identity();    
  }
  else if (alt)
  {
    if (nFlags & MK_RBUTTON)
    {
      _dragMode=DragO2Zoom;
      _dragArea=CRect(point,point);
      SetCapture();
    }
    else
      return;
  }
  else if (!(nFlags & (MK_SHIFT | MK_CONTROL)) && HitTestToPoint(point,_draggedPts))
  {
    FTransform trn=CalcPixelToFloatTransf();
    FPoint2D curPt=trn.Transform(FPoint2D((float)point.x,(float)point.y));
    FPoint2D tolPt=trn.Transform(FPoint2D((float)point.x+SNAPTOLERANCE,(float)point.y));
    _snapPt=FindNearestSnapPoint(curPt,curPt.Distance(tolPt));
    _dragMode=DragVertex;
    _dragArea=CRect(point,point);
    _transformArea.Identity();
  }
  else
  {  
    _dragMode=DragHitTest(point,&selArea);
    if (_dragMode==DragMove) 
    {
      if (nFlags & (MK_SHIFT | MK_CONTROL)) 
      {
        _dragMode=DragRect;
        SetCursor(LoadCursor(0,IDC_CROSS));
      }
      _dragArea=CRect(point,point);  
      _transformArea.Identity();
      _clearArea=selArea;
    }
    else if (_dragMode==DragRect)
    {  
      _dragArea=CRect(point,point);
      SetCursor(LoadCursor(0,IDC_CROSS));
    }
    else
    {
      _dragArea=selArea;
      _transformArea.Identity();
    }
  }
  SetCapture();

  CWnd::OnLButtonDown(nFlags, point);
}

void UVEditorView::OnLButtonUp(UINT nFlags, CPoint point)
{
  if (GetCapture()==this)
  {
    EndEdit(nFlags,point);
  }
  CWnd::OnLButtonUp(nFlags, point);
}


void UVEditorView::ProcessTransform()
{
  for (int i=0;i<_uvObjects.Size();i++)
  {
    UVEditorViewObject &obj=_uvObjects[i];
    if (obj.selectedVertices)
    {    
      for (int j=0;j<obj.n;j++) 
      {
        if (obj.IsSelectedVertex(j)) 
        {
          FPoint2D zz(obj.uv[j].u,obj.uv[j].v);
          zz=_transformArea.Transform(zz);      
          obj.uv[j].u=zz.x;
          obj.uv[j].v=zz.y;
        }
      }
    }
  }
  _selArea=CalculateUVBounding(true);
CorrectSelArea();
}


void UVEditorView::ProcessPtTransform()
{
  for (int i=0;i<_draggedPts.Size();i++)
  {
    int objindex=_draggedPts[i]/MAX_DATA_POLY;
    int uvindex=_draggedPts[i]%MAX_DATA_POLY;
    UVEditorViewObject &obj=_uvObjects[objindex];
    FPoint2D trn=_transformArea.Transform(FPoint2D(obj.uv[uvindex].u,obj.uv[uvindex].v));
    obj.uv[uvindex].u=trn.x;
    obj.uv[uvindex].v=trn.y;
  }
  _selArea=CalculateUVBounding(true);
  CorrectSelArea();
}

void UVEditorView::EndEdit(UINT nFlags, CPoint point)
{
  if (_dragMode==DragRect) 
  {  
    CDC *dc=GetDC();

    int old=dc->SetROP2(R2_XORPEN);

    dc->SelectStockObject(WHITE_PEN);
    dc->SelectStockObject(NULL_BRUSH);
    dc->Rectangle(&_dragArea);
    dc->SetROP2(old);
    ReleaseDC(dc);
    _dragArea.NormalizeRect();
    SelectRectangle(_dragArea,(nFlags & MK_CONTROL)==0,(nFlags & MK_SHIFT)!=0);
    _owner->SelectionChanged(_uvObjects);
    InvalidateRect(&_dragArea);
  }
  else if (_dragMode==DragVertex || _dragMode==DragVertexSnap)
  {
    ProcessPtTransform();
    _owner->MovedPoints(_uvObjects,_draggedPts);
    Invalidate(FALSE);
  }
  else if (_dragMode==DragPan || _dragMode==DragO2Zoom) 
  {
    if (nFlags & MK_RBUTTON) 
    {
      _dragMode=DragPan;  
      return;
    }
  }
  else if (_dragMode==DragZoom)
  {
    if (_dragArea.Size().cx>5 || _dragArea.Size().cy>5) 
    {    
      FTransform trns=CalcPixelToFloatTransf();
      FRect insd(trns.Transform(FPoint2D((float)_dragArea.left,(float)_dragArea.top)),
        trns.Transform(FPoint2D((float)_dragArea.right,(float)_dragArea.bottom)));
      CalculateZoomRect(insd);
      Invalidate(FALSE);
    }
    else
    {
      SetZoom(_zoom*3,_dragArea.CenterPoint());
    }
  }
  else 
  {
    Invalidate(FALSE);
    ProcessTransform();
    _owner->TransformedSelection(_uvObjects);
  }
  ReleaseCapture();
  _snapPoints.Clear();
}




void UVEditorView::DrawPreviewPolygon(CDC *dc, const POINT *pts, int count)
{
  HashTable::Expandable<SPreviewXORBlindPreventStruct> *cache=
    reinterpret_cast<HashTable::Expandable<SPreviewXORBlindPreventStruct> *>(_previewCache);
  
  int last=count-1;
  for (int i=0;i<count;i++)
  {
    SPreviewXORBlindPreventStruct item(pts[last],pts[i]);
    if (!cache->Find(item))
    {
      dc->MoveTo(pts[last]);
      dc->LineTo(pts[i]);
      cache->Add(item);
    }
    last=i;
  }
}

void UVEditorView::ClearCache()
{
  if (_previewCache==0)
    _previewCache=new HashTable::Expandable<SPreviewXORBlindPreventStruct>;
  HashTable::Expandable<SPreviewXORBlindPreventStruct> *cache=
    reinterpret_cast<HashTable::Expandable<SPreviewXORBlindPreventStruct> *>(_previewCache);
  cache->Clear();
}

void UVEditorView::DrawUVPreview(CDC *dc)
{
  ClearCache(); 
 

  FTransform trns=CalcFloatToPixelTransf();
  for (int i=0;i<_uvObjects.Size();i++)
  {
    UVEditorViewObject &obj=_uvObjects[i];
    if (obj.selectedVertices)
    {    
    POINT pts[MAX_DATA_POLY];    

    for (int j=0;j<obj.n;j++) 
    {
      FPoint2D zz(obj.uv[j].u,obj.uv[j].v);
      if (obj.IsSelectedVertex(j)) zz=_transformArea.Transform(zz);      
      FPoint2D z=trns.Transform(zz);
      pts[j].x=toLargeInt(z.x);
      pts[j].y=toLargeInt(z.y);      
    }
    DrawPreviewPolygon(dc,pts,obj.n);
    }
  }
}

void UVEditorView::DrawPtDragPreview(CDC *dc)
{
  ClearCache(); 
  FTransform trns=CalcFloatToPixelTransf();
  int lastIdx=-1;
  POINT pts[MAX_DATA_POLY];    
  int npts=0;
  for (int i=0;i<_draggedPts.Size();i++)
  {
    int polyidx=_draggedPts[i]/MAX_DATA_POLY;
    if (polyidx!=lastIdx)
    {
      if (npts)
        DrawPreviewPolygon(dc,pts,npts);
      UVEditorViewObject &obj=_uvObjects[polyidx];      
      for (int j=0;j<obj.n;j++)
      {
        FPoint2D zz(obj.uv[j].u,obj.uv[j].v);
        FPoint2D z=trns.Transform(zz);
        pts[j].x=toLargeInt(z.x);
        pts[j].y=toLargeInt(z.y);              
      }
      lastIdx=polyidx;
      npts=obj.n;
    }
    {    
    int vs=_draggedPts[i]%MAX_DATA_POLY;
    UVEditorViewObject &obj=_uvObjects[polyidx];      
    FPoint2D zz(obj.uv[vs].u,obj.uv[vs].v);
    zz=_transformArea.Transform(zz);
    FPoint2D z=trns.Transform(zz);
    pts[vs].x=toLargeInt(z.x);
    pts[vs].y=toLargeInt(z.y);      
    }
  }
  if (npts)
    DrawPreviewPolygon(dc,pts,npts);
}


#include "../ObjektivLib/determinant.h"

FTransform UVEditorView::CalculateDifferenceMatrix(
  const FPoint2D &src1, 
  const FPoint2D &src2, 
  const FPoint2D &src3, 
  const FPoint2D &res1, 
  const FPoint2D &res2, 
  const FPoint2D &res3)
{
  float rovnice[6][6]=
  {
    {src1.x,src1.y,1,0,0,0},
    {src2.x,src2.y,1,0,0,0},
    {src3.x,src3.y,1,0,0,0},
    {0,0,0,src1.x,src1.y,1},
    {0,0,0,src2.x,src2.y,1},
    {0,0,0,src3.x,src3.y,1},
  };
  float vysledky[6]={res1.x,res2.x,res3.x,res1.y,res2.y,res3.y};
  float matice[6];
  float save[6];
  float det=Determinant(reinterpret_cast<float *>(rovnice),6);  
  for (int i=0;i<6;i++)
  {
    for (int j=0;j<6;j++)
    {
      save[j]=rovnice[j][i];rovnice[j][i]=vysledky[j];
    }
    matice[i]=Determinant(reinterpret_cast<float *>(rovnice),6)/det;  
    for (int j=0;j<6;j++) rovnice[j][i]=save[j];
  }
  FTransform res;
  res.mx[0][0]=matice[0];
  res.mx[1][0]=matice[1];
  res.mx[2][0]=matice[2];
  res.mx[0][1]=matice[3];
  res.mx[1][1]=matice[4];
  res.mx[2][1]=matice[5];
  return res;
}

void UVEditorView::OnMouseMove(UINT nFlags, CPoint point)
{
  if (GetCapture()==this)
  {
    CDC *dc=GetDC();

    int old=dc->SetROP2(R2_XORPEN);
    dc->SelectStockObject(WHITE_PEN);
    dc->SelectStockObject(NULL_BRUSH);

    if (_dragMode==DragRect || _dragMode==DragZoom)
    {    
     dc->Rectangle(&_dragArea);
     _dragArea.right=point.x;
     _dragArea.bottom=point.y;
     dc->Rectangle(&_dragArea);
    }
    else if (_dragMode==DragMove || _dragMode==DragMoveSnap)
    {
      FTransform trns=CalcFloatToPixelTransf();
      FTransform trnBack=CalcPixelToFloatTransf();
      _dragArea.right=point.x;
      _dragArea.bottom=point.y;
      if (nFlags & MK_SHIFT)
      {
        if (abs(_dragArea.Size().cx)>abs(_dragArea.Size().cy))        
          _dragArea.bottom=_dragArea.top;
        else
          _dragArea.right=_dragArea.left;
      }
      CRect selArea=TransformToInt(trns,_selArea);
      DrawUchytky(dc,_clearArea);
      DrawUVPreview(dc);
      bool doSnap=false;
      if (_dragMode==DragMoveSnap)
      {
        FPoint2D curPt=trnBack.Transform(FPoint2D((float)point.x,(float)point.y));
        FPoint2D tolPt=trnBack.Transform(FPoint2D((float)point.x+SNAPTOLERANCE,(float)point.y));
        bool found;
        FPoint2D nearPt=FindNearestSnapPoint(curPt,tolPt.Distance(curPt),&found);
        if (found)
        {
          FPoint2D diff=nearPt-_snapPt;
          _transformArea.Translation(diff);
          FRect fselArea;
          fselArea.leftTop=_transformArea.Transform(_selArea.leftTop);
          fselArea.rightBottom=_transformArea.Transform(_selArea.rightBottom);
          selArea=TransformToInt(trns,fselArea);
          doSnap=true;
        }        
      }
      if (!doSnap)
      {      
        CPoint diff(_dragArea.right-_dragArea.left,_dragArea.bottom-_dragArea.top);
        FRect newArea;
        selArea+=diff;
        newArea.leftTop=trnBack.Transform(FPoint2D((float)selArea.left,(float)selArea.top));
        newArea.rightBottom=trnBack.Transform(FPoint2D((float)selArea.right,(float)selArea.bottom));
        _transformArea=CalculateDifferenceMatrix(
          _selArea.leftTop,_selArea.rightBottom,FPoint2D(_selArea.leftTop.x,_selArea.rightBottom.y),
          newArea.leftTop,newArea.rightBottom,FPoint2D(newArea.leftTop.x,newArea.rightBottom.y));
      }
      DrawUVPreview(dc);
      DrawUchytky(dc,selArea);
      _clearArea=selArea;
    }
    else if (_dragMode==DragRotate)
    {
      DrawUVPreview(dc);
      _dragArea.right=point.x;
      _dragArea.bottom=point.y;
      FTransform trns=CalcFloatToPixelTransf();      
      CRect selArea=TransformToInt(trns,_selArea);
      CPoint center=selArea.CenterPoint();
      FPoint2D fcenter=_selArea.CenterPoint();
      float angl1=atan2((float)(_dragArea.top-center.y),(float)(_dragArea.left-center.x));
      float angl2=atan2((float)(_dragArea.bottom-center.y),(float)(_dragArea.right-center.x));
      float angldiff=angl2-angl1;
      if ((nFlags & (MK_SHIFT|MK_CONTROL)) == (MK_SHIFT|MK_CONTROL))
        angldiff=floor((angldiff+M_PI_4/4)/(M_PI_4/2))*(M_PI_4/2);
      else if (nFlags & MK_CONTROL)      
         angldiff=floor((angldiff+M_PI_4/2)/M_PI_4)*M_PI_4;     
      else if (nFlags & MK_SHIFT)      
         angldiff=floor((angldiff*18.0f+M_PI_2)/(M_PI_2*2))*(M_PI_2*2)/18.0f;           
      CSize txsz=GetCurTextureDimensions();
      _transformArea=FTransform().Translation(-fcenter)*FTransform().Scale(FPoint2D(1.0f*txsz.cx,1.0f*txsz.cy))*FTransform().Rotation(angldiff)*FTransform().Scale(FPoint2D(1.0f/txsz.cx,1.0f/txsz.cy))*FTransform().Translation(fcenter);
      DrawUVPreview(dc);
    }
    else if (_dragMode==DragVertex || _dragMode==DragVertexSnap)
    {
       FTransform trns=CalcPixelToFloatTransf(); 
       CRect rct1(_dragArea.right-5,_dragArea.bottom-5,_dragArea.right+5,_dragArea.bottom+5);
       dc->Rectangle(&rct1);
      _dragArea.right=point.x;
      _dragArea.bottom=point.y;
      FPoint2D pt1=trns.Transform(FPoint2D((float)_dragArea.left,(float)_dragArea.top));
      FPoint2D pt2=trns.Transform(FPoint2D((float)_dragArea.right,(float)_dragArea.bottom));
      if (((nFlags & MK_CONTROL)!=0)!=_autoSnap)
      {
        FPoint2D curPt=trns.Transform(FPoint2D((float)point.x,(float)point.y));
        FPoint2D tolPt=trns.Transform(FPoint2D((float)point.x+SNAPTOLERANCE,(float)point.y));
        bool found;
        FPoint2D nearPt=FindNearestSnapPoint(curPt,tolPt.Distance(curPt),&found);
        if (found)
        {
          pt1=_snapPt;
          pt2=nearPt;
        }
      }
      DrawPtDragPreview(dc);
      _transformArea.Translation(pt2-pt1);
      DrawPtDragPreview(dc);
      CRect rct2(_dragArea.right-5,_dragArea.bottom-5,_dragArea.right+5,_dragArea.bottom+5);
      dc->Rectangle(&rct2);
    }
    else if (_dragMode==DragPan)
    {
      FTransform trns=CalcPixelToFloatTransf(); 
      _dragArea.right=point.x;
      _dragArea.bottom=point.y;
      FPoint2D oldorg=_origin;
      CPoint diff=_dragArea.Size();
      if (diff.x!=0 || diff.y!=0)
      {
         FPoint2D pt1=trns.Transform(FPoint2D((float)_dragArea.left,(float)_dragArea.top));
         FPoint2D pt2=trns.Transform(FPoint2D((float)_dragArea.right,(float)_dragArea.bottom));
         FPoint2D diff=pt2-pt1;
         _origin=_origin+diff;
         ScrollOriginTo(oldorg,_origin);
         CPoint curpos(_dragArea.left,_dragArea.top);
         ClientToScreen(&curpos);
         SetCursorPos(curpos.x,curpos.y);
      }
    }
    else if (_dragMode==DragO2Zoom)
    {
      _dragArea.right=point.x;
      _dragArea.bottom=point.y;
      CPoint diff=_dragArea.Size();
      if (diff.y!=0)
      {        
        CPoint curpos(_dragArea.left,_dragArea.top);
        float zoomFactor=diff.y*0.01f;
        SetZoom(_zoom*(1.0f-zoomFactor),curpos); 
        Invalidate(FALSE);
        ClientToScreen(&curpos);
        SetCursorPos(curpos.x,curpos.y);
      }
    }
    else
    {
     DrawUVPreview(dc);
     DrawUchytky(dc,_dragArea);
     switch (_dragMode)
     {
     case DragRightBottom:
       _dragArea.right=point.x-SELUCHYT_SIZE/2;
       _dragArea.bottom=point.y-SELUCHYT_SIZE/2;
       break;
     case DragTopLeft:
       _dragArea.left=point.x+SELUCHYT_SIZE/2;
       _dragArea.top=point.y+SELUCHYT_SIZE/2;
       break;
     case DragBottomLeft:
       _dragArea.left=point.x+SELUCHYT_SIZE/2;
       _dragArea.bottom=point.y-SELUCHYT_SIZE/2;
       break;
     case DragRightTop:
       _dragArea.right=point.x-SELUCHYT_SIZE/2;
       _dragArea.top=point.y+SELUCHYT_SIZE/2;
       break;
     case DragTop: _dragArea.top=point.y+SELUCHYT_SIZE/2;break;
     case DragLeft: _dragArea.left=point.x+SELUCHYT_SIZE/2;break;
     case DragRight: _dragArea.right=point.x-SELUCHYT_SIZE/2;break;
     case DragBottom: _dragArea.bottom=point.y-SELUCHYT_SIZE/2;break;
     }     
     FTransform trnBack=CalcPixelToFloatTransf(); 
     FRect newArea;
     newArea.leftTop=trnBack.Transform(FPoint2D((float)_dragArea.left,(float)_dragArea.top));
     newArea.rightBottom=trnBack.Transform(FPoint2D((float)_dragArea.right,(float)_dragArea.bottom));
     if (nFlags & MK_CONTROL && (_dragMode==DragRightBottom || _dragMode==DragRightTop ||
       _dragMode==DragTopLeft|| _dragMode==DragBottomLeft))
     {
       float aspect1=newArea.Cy()/newArea.Cx(); 
       float aspect2=_selArea.Cy()/_selArea.Cx();
       if (aspect1<aspect2) 
         if (_dragMode==DragRightBottom || _dragMode==DragRightTop)
           newArea.rightBottom.x=newArea.leftTop.x+newArea.Cy()/aspect2;
         else
           newArea.leftTop.x=newArea.rightBottom.x-newArea.Cy()/aspect2;
       else       
         if (_dragMode==DragBottomLeft|| _dragMode==DragRightBottom)
           newArea.rightBottom.y=newArea.leftTop.y+newArea.Cx()*aspect2;
         else
           newArea.leftTop.y=newArea.rightBottom.y-newArea.Cx()/aspect2;
     }
     if (nFlags & MK_SHIFT) 
     {
       FPoint2D nwsz(newArea.Cx(),newArea.Cy());
       FPoint2D szaspect(nwsz.x/_selArea.Cx(),nwsz.y/_selArea.Cy());
       if (fabs(szaspect.x)<1.0) szaspect.x=(float)floor(szaspect.x*10.0f)/10.0f;
       else szaspect.x=(float)floor(szaspect.x*2.0f)/2.0f;
       if (fabs(szaspect.y)<1.0) szaspect.y=(float)floor(szaspect.y*10.0f)/10.0f;
       else szaspect.y=(float)floor(szaspect.y*2.0f)/2.0f;
       nwsz.x=_selArea.Cx()*szaspect.x;
       nwsz.y=_selArea.Cy()*szaspect.y;
       if (_dragMode==DragLeft || _dragMode==DragTopLeft || _dragMode==DragBottomLeft)
        {newArea.leftTop.x=newArea.rightBottom.x-nwsz.x;}
       else
        {newArea.rightBottom.x=newArea.leftTop.x+nwsz.x;}
       if (_dragMode==DragTopLeft|| _dragMode==DragTop || _dragMode==DragRightTop)
        {newArea.leftTop.y=newArea.rightBottom.y-nwsz.y;}
       else
        {newArea.rightBottom.y=newArea.leftTop.y+nwsz.y;}
     }
     _transformArea=CalculateDifferenceMatrix(
       _selArea.leftTop,_selArea.rightBottom,FPoint2D(_selArea.leftTop.x,_selArea.rightBottom.y),
       newArea.leftTop,newArea.rightBottom,FPoint2D(newArea.leftTop.x,newArea.rightBottom.y));
     DrawUVPreview(dc);
     DrawUchytky(dc,_dragArea);
    }
    dc->SetROP2(old);

    ReleaseDC(dc);
  }
  CWnd::OnMouseMove(nFlags, point);
}

CPoint UVEditorView::TransformToInt(const FTransform &trns, const FPoint2D &pt)
{
  FPoint2D fp=trns.Transform(pt);
  return CPoint (toLargeInt(fp.x),toLargeInt(fp.y));
}

CRect UVEditorView::TransformToInt(const FTransform &trns, const FRect &rc)
{
  return CRect(TransformToInt(trns,rc.leftTop),TransformToInt(trns,rc.rightBottom));
}

void UVEditorView::SelectRectangle(const CRect &rc, bool clearSel, bool deselect)
{
  FTransform trns=CalcFloatToPixelTransf();
  CRect oldArea=TransformToInt(trns,_selArea)+CRect(SELUCHYT_SIZE+1,SELUCHYT_SIZE+1,SELUCHYT_SIZE+1,SELUCHYT_SIZE+1);
  for (int i=0;i<_uvObjects.Size();i++)
  {
    UVEditorViewObject &obj=_uvObjects[i];
    for (int j=0;j<obj.n;j++)
    {
      CPoint pt=TransformToInt(trns,FPoint2D(obj.uv[j].u,obj.uv[j].v));
      if (rc.PtInRect(pt))
      {
        if (deselect)
        {
          if (!clearSel)          
            obj.SelectVertex(j,false);
        }
        else 
        {
          obj.SelectVertex(j,true);
        }
      }
      else
      {
        if (clearSel)
          obj.SelectVertex(j,false);
      }
    }
  } 
  _selArea=CalculateUVBounding(true);
  CorrectSelArea();
  CRect newArea=TransformToInt(trns,_selArea)+CRect(SELUCHYT_SIZE+1,SELUCHYT_SIZE+1,SELUCHYT_SIZE+1,SELUCHYT_SIZE+1);
  InvalidateRect(oldArea,FALSE);
  InvalidateRect(newArea,FALSE);
}

void UVEditorView::BreakSelection()
{
  FTransform trns=CalcFloatToPixelTransf();
 CRect oldArea=TransformToInt(trns,_selArea)+CRect(SELUCHYT_SIZE+1,SELUCHYT_SIZE+1,SELUCHYT_SIZE+1,SELUCHYT_SIZE+1);
  for (int i=0;i<_uvObjects.Size();i++) 
  {
    UVEditorViewObject &obj=_uvObjects[i];
    if (obj.selectedVertices && !obj.IsAllVerticesSelected())
    {
      obj.selectedVertices=0;
    }
  }
  _selArea=CalculateUVBounding(true);
  CorrectSelArea();
  CRect newArea=TransformToInt(trns,_selArea)+CRect(SELUCHYT_SIZE+1,SELUCHYT_SIZE+1,SELUCHYT_SIZE+1,SELUCHYT_SIZE+1);
  InvalidateRect(oldArea,FALSE);
  InvalidateRect(newArea,FALSE);
  _owner->SelectionChanged(GetUVObjects());
}
void UVEditorView::OnRButtonDown(UINT nFlags, CPoint point)
{
  bool alt=GetKeyState(VK_MENU)<0;
  if (_zoomMode  && !alt)
  {
    SetZoom(_zoom/3.0f,point);
    return;
  }
  CRect selArea;
  _dragMode=DragHitTest(point,&selArea);

  if (alt)
  {
    _dragMode=DragPan;
    if (nFlags & MK_LBUTTON)
    {
      _dragMode=DragO2Zoom;
    }
    _dragArea=CRect(point,point);
    CWnd::OnRButtonDown(nFlags, point);
    SetCapture();
  }
    else if (_dragMode==DragTopLeft || _dragMode==DragBottomLeft ||
        _dragMode==DragRightBottom || _dragMode==DragRightTop)
    {  
      _dragArea=CRect(point,point);
      _transformArea.Identity();
      _dragMode=DragRotate;
      SetCapture();
      CDC *dc=GetDC();
      DrawUchytky(dc,selArea);
      ReleaseDC(dc);
      SetCursor(theApp.LoadCursor(IDI_OBJROTATE));
    }
  else if (!_selArea.IsEmpty())
  {
    if ((nFlags & MK_CONTROL) || _autoSnap) 
    {
      FTransform trn=CalcPixelToFloatTransf();
      FPoint2D curPt=trn.Transform(FPoint2D((float)point.x,(float)point.y));
      FPoint2D tolPt=trn.Transform(FPoint2D((float)point.x+SNAPTOLERANCE,(float)point.y));
      bool fnd;
      _snapPt=FindNearestSnapPoint(curPt,curPt.Distance(tolPt),&fnd);
      if (fnd) _dragMode=DragMoveSnap;
      else _dragMode=DragMove;
    }
    else
      _dragMode=DragMove;
    _dragArea=CRect(point,point);  
    _transformArea.Identity();
    _clearArea=selArea;
    SetCapture();

  }
}

void UVEditorView::OnRButtonUp(UINT nFlags, CPoint point)
{
  if (GetCapture()==this)
  {
    EndEdit(nFlags,point);
  }

  CWnd::OnRButtonUp(nFlags, point);
}

BOOL UVEditorView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
  CPoint point(GetMessagePos());
  ScreenToClient(&point);


  DragMode md=DragHitTest(point,0);

  HCURSOR crs=0;

  if (_zoomMode)
  {
    crs=theApp.LoadCursor(IDI_LUPA);
  }
  else
  {
    int pt=HitTestToPoint(point);
    if (pt!=-1)
    {
      crs=theApp.LoadCursor(IDI_POINTARROW);
    }
    else
      switch (md)
    {
      case DragNone: crs=::LoadCursor(0,IDC_ARROW);break;
      case DragTopLeft:
      case DragRightBottom: crs=::LoadCursor(0,IDC_SIZENWSE);break;
      case DragBottomLeft:
      case DragRightTop: crs=::LoadCursor(0,IDC_SIZENESW);break;
      case DragTop:
      case DragBottom: crs=::LoadCursor(0,IDC_SIZENS);break;
      case DragLeft:
      case DragRight: crs=::LoadCursor(0,IDC_SIZEWE);break;
      case DragMove: if (GetKeyState(VK_SHIFT)<0 || GetKeyState(VK_CONTROL)<0)
                       crs=::LoadCursor(0,IDC_ARROW);
                     else                   
                       crs=::LoadCursor(0,IDC_SIZEALL);break;
      default: crs=::LoadCursor(0,IDC_ARROW);break;
    }
  }
  SetCursor(crs);
  return TRUE;
}

void UVEditorView::ScrollOriginTo(FPoint2D oldpos, FPoint2D newpos)
{
  FTransform trns=CalcFloatToPixelTransf();
  CPoint pt1=TransformToInt(trns,oldpos);
  CPoint pt2=TransformToInt(trns,newpos);
 
  if (pt1!=pt2) 
  { 
    ScrollWindow(pt2.x-pt1.x,pt2.y-pt1.y);
  }
}
void UVEditorView::OnSize(UINT nType, int cx, int cy)
{
  CWnd::OnSize(nType, cx, cy);
  
  _scrollPage.x=cx/3;
  _scrollPage.y=cy/3;
  SCROLLINFO nfo;
  nfo.cbSize=sizeof(nfo);
  nfo.fMask=SIF_PAGE|SIF_POS|SIF_RANGE;
  nfo.nMin=-cy;
  nfo.nMax=cy+_scrollPage.y;
  nfo.nPage=_scrollPage.y;
  nfo.nPos=0;
  SetScrollInfo(SB_VERT,&nfo,TRUE);
  nfo.nMin=-cx;
  nfo.nMax=cx+_scrollPage.x;
  nfo.nPage=_scrollPage.x;
  SetScrollInfo(SB_HORZ,&nfo,TRUE);
  // TODO: Add your message handler code here
}

void UVEditorView::FitTextureToWindow()
{
  _origin.x=0;
  _origin.y=0;
  CRect rc;
  GetClientRect(&rc);
  float zx=(float)rc.right/(float)_bmpsize.cx;
  float zy=(float)rc.bottom/(float)_bmpsize.cy;
  _zoom=__min(zx,zy);
  CenterRect(FRect(0,0,1,1));
  Invalidate(FALSE);
}

void UVEditorView::CalculateZoomRect(FRect rc)
{
  _origin.x=-rc.leftTop.x;
  _origin.y=-rc.leftTop.y;
  float szx=rc.Cx();
  float szy=rc.Cy();
  float bmpszx=szx*_bmpsize.cx;
  float bmpszy=szy*_bmpsize.cy;
  CRect crc;
  GetClientRect(&crc);
  float zx=(float)crc.right/bmpszx;
  float zy=(float)crc.bottom/bmpszy;
  _zoom=__min(zx,zy);
  CenterRect(rc);

}
void UVEditorView::FitUVSToWindow(bool selected)
{
  FRect rc=CalculateUVBounding(selected);
  if (rc.IsEmpty()) {FitTextureToWindow();return;}
  rc=FRect(rc.leftTop.x-rc.Cx()*0.05f,rc.leftTop.y-rc.Cy()*0.05f,
    rc.rightBottom.x+rc.Cx()*0.05f,rc.rightBottom.y+rc.Cy()*0.05f);

  CalculateZoomRect(rc);
  Invalidate(FALSE);

}

void UVEditorView::CenterRect(FRect rc)
{
  FPoint2D cp=rc.CenterPoint();
  FTransform trns=CalcPixelToFloatTransf();
  CRect wnd;
  GetClientRect(&wnd);
  CPoint curcntr=wnd.CenterPoint();
  FPoint2D fcurcntr=trns.Transform(FPoint2D((float)curcntr.x,(float)curcntr.y));
  _origin=_origin+(fcurcntr-cp);
}

void UVEditorView::SetZoomMode(bool zoomMode)
{
  _zoomMode=zoomMode;
  CPoint pt;
  GetCursorPos(&pt);
  ScreenToClient(&pt);
  CRect client;
  GetClientRect(&client);
  if (client.PtInRect(pt)) SetCursor(theApp.LoadCursor(IDI_LUPA));
}


void UVEditorView::TransformXYToUV(Array<UVEditorViewObject> objects)
{
  FTransform trans=CalcPixelToFloatTransf();
  for (int i=0;i<objects.Size();i++)
  {
    UVEditorViewObject &obj=objects[i];
    for (int j=0;j<obj.n;j++)
    {
      FPoint2D tr=trans.Transform(FPoint2D(obj.uv[j].u,obj.uv[j].v));
      obj.uv[j].u=tr.x;
      obj.uv[j].v=tr.y;
    }
  }
}

void UVEditorView::Snap()
{
  int bestface1, bestpoint1, bestface2, bestpoint2;
  FPoint2D best1, best2; 
  float bestdist=FLT_MAX;
  int pbcnt=0;
  for (int i=0;i<_uvObjects.Size();i++) if (_uvObjects[i].selectedVertices) pbcnt++;
  ProgressBar<int> pb(pbcnt);

  for (int i=0;i<_uvObjects.Size();i++) if (_uvObjects[i].selectedVertices)
  {
    pb.AdvanceNext(1);
    const UVEditorViewObject &obj1=_uvObjects[i];
    for (int j=0;j<obj1.n;j++) if (obj1.IsSelectedVertex(j))
    {
      FPoint2D pt1(obj1.uv[j].u,obj1.uv[j].v);
      for (int k=0;k<_uvObjects.Size();k++) if (!_uvObjects[k].selectedVertices)
      {
        const UVEditorViewObject &obj2=_uvObjects[k];
        for (int l=0;l<obj2.n;l++) 
        {
          FPoint2D pt2(obj2.uv[l].u,obj2.uv[l].v);
          float distance=pt2.Distance2(pt1);
          if (distance<bestdist)
          {
            bestface1=i;
            bestface2=k;
            bestpoint1=j;
            bestpoint2=l;
            best1=pt1;
            best2=pt2;
            bestdist=distance;
          }
        }
      }
    }
  }
  if (bestdist!=FLT_MAX)
  {
    _transformArea.Translation(best2-best1);
    ProcessTransform();
    _owner->TransformedSelection(_uvObjects);
    Invalidate();
  }
}

struct UnwrapHelpStruct
{
  int iFaceProcess;
  int iFaceRelTo;
  VecT normal;
  UnwrapHelpStruct() {}
  UnwrapHelpStruct(int IFP,int IFRT,const VecT &norm):iFaceRelTo(IFRT),iFaceProcess(IFP),normal(norm) {}
};

TypeIsMovable(UnwrapHelpStruct);

void UVEditorView::CalculateUnwrap(ObjectData &objdata, int mode)
{  
  BreakSelection();
  Selection forbiden(&objdata);
  for (int i=0;i<_uvObjects.Size();i++) if (!_uvObjects[i].IsAllVerticesSelected()) forbiden.FaceSelect(_uvObjects[i].faceIndex);
  CEdges facegrph(objdata.NFaces());
  CEdges nocross(objdata.NFaces());
  objdata.BuildFaceGraph(facegrph,nocross);
  AutoArray<UnwrapHelpStruct> queue;
  int queuebottom=0;
  AutoArray<UVEditorViewObject> newuvs;
  newuvs.Resize(objdata.NFaces());  
  for (int i=0;i<_uvObjects.Size();i++) if (_uvObjects[i].IsAllVerticesSelected())
  {
    queue.Append(UnwrapHelpStruct(_uvObjects[i].faceIndex,-1,VecT(0,0,0)));
    newuvs[_uvObjects[i].faceIndex]=_uvObjects[i];
  }

  VecT curNormal(0,0,0);

  while (queue.Size())
  {
    float maxNormalAngle=2;
    int qidx=0;
    for (int i=0;i<queue.Size();i++)
    {
      float angle=1-queue[i].normal.DotProduct(curNormal); 
      if (angle<maxNormalAngle) 
      {
        maxNormalAngle=angle;
        qidx=i;
        if (angle==0) break;
      }
    }
    UnwrapHelpStruct *curfcnfo=&queue[qidx];
    int startFcIdx=curfcnfo->iFaceProcess;
    if (!forbiden.FaceSelected(startFcIdx))
    {    
      forbiden.FaceSelect(startFcIdx);
      FaceT startFc(objdata,startFcIdx);
      for (int i=0,k;(k=facegrph.GetEdge(startFcIdx,i))!=-1;i++)
      {
        FaceT nextFc(objdata,k);
        queue.Append(UnwrapHelpStruct(k,startFcIdx,nextFc.CalculateNormal()));
      }
      curfcnfo=&queue[qidx];
      if (curfcnfo->iFaceRelTo!=-1)
      {      
        FaceT fromFace(objdata,curfcnfo->iFaceRelTo);
        int n1,n2,n3,n4,s1,s2;
        startFc.GetEdgeIndices(fromFace,n1,n2,s1,s2);
        curNormal=curfcnfo->normal;
        if (n2<n1 || (n1==0 && n2==startFc.N()-1))
        {
          n1^=n2;n2^=n1;n1^=n2;
          s1^=s2;s2^=s1;s1^=s2;
        }
        FPoint2D uvpts[MAX_DATA_POLY];
        uvpts[n1].x=0;
        uvpts[n1].y=0;
        uvpts[n2].x=1;
        uvpts[n2].y=0;
        n3=(n2+1)%startFc.N();
        n4=(n3+1)%startFc.N();

        VecT ps1=startFc.GetPointVector<VecT>(n2);
        VecT ps1a=startFc.GetPointVector<VecT>(n1)-ps1;
        VecT ps1b=startFc.GetPointVector<VecT>(n3)-ps1;
        float angle=-ps1a.CosAngle(ps1b);

        uvpts[n3].x=angle;
        uvpts[n3].y=sqrt(1-angle*angle);

        float ratio1=ps1b.Size()/ps1a.Size();
        uvpts[n3]=uvpts[n3]*ratio1;
        uvpts[n3].x+=1.0f;

        if (startFc.N()==4)
        {
          VecT ps2=startFc.GetPointVector<VecT>(n1);
          VecT ps2a=startFc.GetPointVector<VecT>(n2)-ps2;
          VecT ps2b=startFc.GetPointVector<VecT>(n4)-ps2;
          float angle=ps2a.CosAngle(ps2b);

          uvpts[n4].x=angle;
          uvpts[n4].y=sqrt(1-angle*angle);

          float ratio2=ps2b.Size()/ps2a.Size();
          uvpts[n4]=uvpts[n4]*ratio1;
        }

        FTransform trns;
        UVEditorViewObject &starUvs=newuvs[fromFace.GetFaceIndex()];
        FPoint2D dirvect(starUvs.uv[s2].u-starUvs.uv[s1].u,starUvs.uv[s2].v-starUvs.uv[s1].v);
        trns.mx[0][0]=dirvect.x;
        trns.mx[0][1]=dirvect.y;
        trns.mx[1][0]=dirvect.y;
        trns.mx[1][1]=-dirvect.x;
        trns.mx[2][0]=starUvs.uv[s1].u;
        trns.mx[2][1]=starUvs.uv[s1].v;
        UVEditorViewObject &newUvs=newuvs[startFc.GetFaceIndex()];
        newUvs.n=startFc.N();
        for (int i=0;i<newUvs.n;i++)
        {
          FPoint2D res=trns.Transform(uvpts[i]);
          newUvs.uv[i].u=res.x;
          newUvs.uv[i].v=res.y;
        }
        newUvs.faceIndex=startFc.GetFaceIndex();
        newUvs.SelectAllVertices();
      }
    }
    queue.Delete(qidx);
  }
  _owner->TransformedSelection(newuvs);
}

void UVEditorView::SelectAll()
{
  for (int i=0;i<_uvObjects.Size();i++) _uvObjects[i].SelectAllVertices();
  Invalidate(FALSE);
}

void UVEditorView::SetWhiteMode(bool enable)
{
  _whiteMode=enable;
  if (enable)
  {
    _linecolor=RGB(0,0,0);
    _selectcolor=RGB(128,0,0);
    _backUvColor=RGB(255,255,128);  
  }
  else
  {
    _linecolor=RGB(255,255,190);
    _selectcolor=RGB(255,80,80); 
    _backUvColor=RGB(80,80,255);    
  }
}
void UVEditorView::CorrectSelArea()
{
  CRect rc=TransformToInt(CalcFloatToPixelTransf(),_selArea);
  if (abs(rc.Size().cx)<2 && abs(rc.Size().cy)>=2) 
  {
    FTransform trns=CalcPixelToFloatTransf();
    FPoint2D pt=trns.Transform(FPoint2D(5,5),0);
    _selArea.leftTop.x-=pt.x*0.5f; 
    _selArea.rightBottom.x+=pt.x*0.5f;
  }
  else if (abs(rc.Size().cx)>=2 && abs(rc.Size().cy)<2) 
  {
    FTransform trns=CalcPixelToFloatTransf();
    FPoint2D pt=trns.Transform(FPoint2D(5,5),0);
    _selArea.leftTop.y-=pt.y*0.5f;
    _selArea.rightBottom.y+=pt.y*0.5f;
  }
}
void UVEditorView::UpdateSnapPoints()
{
  for (int i=0;i<_uvObjects.Size();i++)
  {
    UVEditorViewObject &obj=_uvObjects[i];
    for (int j=0;j<obj.n;j++)
    {
      FSnapPoint pt(obj.uv[j].u,obj.uv[j].v);
      FSnapPoint *found=_snapPoints.Find(pt);
      if (!found) _snapPoints.Add(pt);
    }    
  }
  for (int i=0;i<_backgroundUV.Size();i++)
  {
    UVEditorViewObject &obj=_backgroundUV[i];
    for (int j=0;j<obj.n;j++)
    {
      FSnapPoint pt(obj.uv[j].u,obj.uv[j].v);
      FSnapPoint *found=_snapPoints.Find(pt);
      if (!found) _snapPoints.Add(pt);
    }    
  }
}

FPoint2D UVEditorView::FindNearestSnapPoint(const FPoint2D &pt, float distance, bool *found)
{
  if (_snapPoints.Size()==0) UpdateSnapPoints();  
  FSnapPoint startSearch(pt.x-distance,pt.y);
  BTreeIterator<FSnapPoint> iter(_snapPoints);
  FSnapPoint *fp;
  FSnapPoint *best=0;    
  iter.BeginFrom(startSearch);
  for (fp=iter.Next();fp&& fp->x<=pt.x+distance;fp=iter.Next())
  {
    float d=fp->Distance(pt);
    if (d<distance)
    {
      distance=d;
      best=fp;
    }
  }
  if (best)
  {
    if (found) *found=true;
    return *best;
  }
  else
  {
    if (found) *found=false;
    return pt;
  }
}

void UVEditorView::TransformSelection(const FTransform &transform, bool centerSel, bool preview)
{
  FTransform final;
  FPoint2D center=_selArea.CenterPoint();
  if (centerSel)
  {
    CSize sz=GetCurTextureDimensions();
    final=FTransform().Translation(-center)*FTransform().Scale(FPoint2D(1.0f*sz.cx,1.0f*sz.cy))*transform*FTransform().Scale(FPoint2D(1.0f/sz.cx,1.0f/sz.cy))*FTransform().Translation(center);
  }
  else
  {
    final=transform;
  }
  if (preview)
  {
    CClientDC dc(this);
    Invalidate(FALSE);
    UpdateWindow();
    _transformArea=final;
    DrawUVPreview(&dc);
  }
  else
  {
    _transformArea=final;
    Invalidate(FALSE);
    ProcessTransform();
    _owner->TransformedSelection(_uvObjects);

  }
}

void UVEditorView::SelectionMirrorHorz()
{
  TransformSelection(FTransform().Scale(FPoint2D(-1,1)),true,false);
}
void UVEditorView::SelectionMirrorVert()
{
  TransformSelection(FTransform().Scale(FPoint2D(1,-1)),true,false);
}

void UVEditorView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
  DragMode mode=DragHitTest(point,0);
  if (mode==DragRight || mode==DragLeft) SelectionMirrorHorz();
  if (mode==DragTop || mode==DragBottom) SelectionMirrorVert();
}

