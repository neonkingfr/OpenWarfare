// IExtrude.cpp: implementation of the CIExtrude class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Objektiv2.h"
#include "IExtrude.h"
#include "../ObjektivLib/Edges.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////


static void FindEdges(CEdges& eg, ObjectData *obj)
  {
  int cnt=obj->NFaces();
  for (int i=0;i<cnt;i++)
    if (obj->FaceSelected(i))
      {
      FaceT fc(obj,i);
      for (int j=0;j<fc.N();j++)
        {
        int k=j+1;
        if (k>=fc.N()) k=0;
        int a=fc.GetPoint(j);
        int b=fc.GetPoint(k);
        if (eg.IsEdge(a,b)) 
          eg.RemoveEdge(a,b);
        else 
          eg.SetEdge(a,b);
        }
      }
  }

//--------------------------------------------------

/*
static void FindOutEdges(CEdges& eg, ObjectData *obj)
  {
  int cnt=obj->NFaces();
  for (int i=0;i<cnt;i++) if (!obj->FaceSelected(i))
	{
	FaceT &fc=obj->Face(i);
	int lastpt=-1;
	for (int j=0;j<fc.n;j++)
	  {
	  int k=fc.vs[i].point;
	  if (!obj->PointSelected(k)) k=-1;
	  if (lastpt!=-1 && k!=-1)		
		{
		if (!eg.IsEdge(lastpt,k)) eg.SetEdge(lastpt,k);		
		}
	  lastpt=k;
	  }
	int k=fc.vs[0].point;
	if (lastpt!=-1 && obj->PointSelected(k)
	  {
	  if (!eg.IsEdge(lastpt,k)) eg.SetEdge(lastpt,k);		
	  }
	}
  }
*/

static void DoExtrude(CEdges &eg, ObjectData *obj)
  {
  int cnt=obj->NFaces();
  int np=obj->NPoints();
  int *idx=new int[np*2];  
  memset(idx,0xff,sizeof(int)*np*2);
  for (int i=0;i<cnt;i++)
    if (obj->FaceSelected(i))
      {	  
      FaceT fc(obj,i);
      FaceT nwfc(fc);      
      int j;
      for (j=0;j<fc.N();j++) //duplicate face
        {
        int fold=fc.GetPoint(j);
        ASSERT(fold<np*2 && fold>=0);
        int id=idx[fold];
        if (id==-1) 
          {
          id=obj->ReservePoints(1);
          PosT &nw=obj->Point(id);
          PosT &old=obj->Point(fold);
          nw=old;
          }
        nwfc.SetPoint(j,id);

        int nextPt = nwfc.GetPoint((j+1)%fc.N());
        if (obj->FindSharpEdge(fold,nextPt)>=0) 
        {
          obj->AddSharpEdge(id,nextPt);
          if (fold <np && nextPt>=np)
            obj->RemoveSharpEdge(fold,nextPt); // delete temporary edges
        }

        if (j==0) nextPt =nwfc.GetPoint(fc.N()-1);
        else nextPt = nwfc.GetPoint(j-1);

        if (obj->FindSharpEdge(fold,nextPt)>=0) 
        {
          obj->AddSharpEdge(id,nextPt);
          if (fold <np && nextPt>=np)
            obj->RemoveSharpEdge(fold,nextPt); // delete temporary edges
        }


        obj->PointSelect(fold,false);
        obj->PointSelect(id,true);
        idx[fold]=id;
        }
      for (j=0;j<fc.N();j++) //create extrude
        {
        int k=j+1;
        if (k>=fc.N()) k=0;
        int a=fc.GetPoint(j);
        int b=fc.GetPoint(k);
        if (eg.IsEdge(a,b))
          {
          int nxi=obj->ReserveFaces(1);
          FaceT nxfc(obj,nxi);
          nxfc.SetPoint(0,nwfc.GetPoint(k));
          nxfc.SetPoint(1,nwfc.GetPoint(j));
          nxfc.SetPoint(2,a);
          nxfc.SetPoint(3,b);
          nxfc.SetN(4);
          }
        }
      nwfc.CopyFaceTo(i);
      }	
  delete [] idx;
  }

//--------------------------------------------------

int CIExtrude::Execute(LODObject *lod)
  {
  ObjectData *obj=lod->Active();
  Selection sel(obj);
  for (int i=0;i<segments;i++)
    {
    CEdges hrany;  
    hrany.SetVertices(obj->NPoints());
    FindEdges(hrany, obj);
    //	FindOutEdges(hrany, obj)
    DoExtrude(hrany, obj);
    sel.SetOwner(obj);
    int pcnt=obj->NPoints();
    int fcnt=obj->NFaces();
    int j;
    float w=(float)(i+1)/(float)segments;
    for (j=0;j<pcnt;j++) if (obj->PointSelected(j))
      sel.SetPointWeight(j,obj->GetSelection()->PointWeight(j)*w);
    for (j=0;j<fcnt;j++) if (obj->FaceSelected(j))
      sel.FaceSelect(j);
    }
  Selection *ss=(Selection *)obj->GetSelection();  
  (*ss)=sel;
  return 0;
  }

//--------------------------------------------------

CICommand *CIExtrude_Create(istream& str)
  {
  return new CIExtrude(str);
  }

//--------------------------------------------------

CICommand *CIShape_Create(istream& str)
  {
  return new CIShape(str);
  }

//--------------------------------------------------

int CIShape::Execute(LODObject *lod)
  {
  ObjectData *obj=lod->Active();
  const Selection *sel=obj->GetSelection();  
  int fcnt=sel->NFaces();
  int cnt=obj->NFaces();
  int ff=obj->ReserveFaces(fcnt);
  for (int i=0;i<cnt;i++) if (obj->FaceSelected(i))
    {
    FaceT fc(obj,i);
    FaceT nw(obj,ff);	
    for (int j=0;j<fc.N();j++)
      {
      int k=fc.N()-j-1;
      nw.CopyPointInfo(k,fc,j);
      }
    nw.SetTexture(fc.GetTexture());
    nw.SetFlags(fc.GetFlags());
    nw.SetMaterial(fc.GetMaterial());
//    nw.dirty=fc.dirty;
    nw.SetN(fc.N());
    obj->FaceSelect(ff,false);
    ff++;
    }
  return CIExtrude::Execute(lod);  
  }

//--------------------------------------------------

