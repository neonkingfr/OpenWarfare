#pragma once
#include "afxcmn.h"
#include "UVEditorView.h"
#include "ComInt.h"
#include "DlgUVTextureProp.h"
#include "DlgUVManualTransform.h"


class CHotKeyCfgDlg;
class ColorizeDlg;

// DlgUVEditor dialog

class CObjektiv2Doc;

struct UVEditorMenuItem
{
  enum ItemType {texFilter,matFilter,texmatFilter,texShow};
  ItemType type;
  RString name;
  UVEditorMenuItem() {}
  UVEditorMenuItem(ItemType type,RString name):type(type),name(name) {}
};

TypeIsMovable(UVEditorMenuItem)
class IMatPluginSide;


class DlgUVEditor : public CFrameWnd, public IUVEditorViewEvent,public IDlgUVTexturePropNotify, public IDlgUVManualTransform
{
	DECLARE_DYNAMIC(DlgUVEditor)

  AutoArray<UVEditorMenuItem> _menuItems;

  AutoArray<RString> _filterUv;

  DlgUVTextureProp _texProp;

  CHotKeyCfgDlg* _hotKeyDlg;
  HACCEL _userAccel;
  int _titlesize;

  CObjektiv2Doc *_curDocument;
  bool _sizing;
  bool _checker;
  bool _autoselect;
  bool _backLod;
  CString _lastTexture;

  int _tex_bright;
  int _tex_contrst;

  bool _anyselect;
  bool _anybreakableselect;
  CToolBar wToolBar;
  IMatPluginSide *_matPlugin;

  friend class EnumUVSets;
  class EnumUVSets
  {
    mutable DlgUVEditor *outer;
    mutable ObjUVSet *active;
    mutable int index;
  public:
    EnumUVSets(DlgUVEditor *outer);
    bool operator()(const ObjUVSet *uvset) const;
  };

public:
	DlgUVEditor(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgUVEditor();

  DlgUVManualTransform _dlgTransform;

protected:
	
  CTabCtrl wUVSetTab;
  UVEditorView wUVView;


  afx_msg LRESULT OnExitSizeMove(WPARAM wParam, LPARAM lParam);
  afx_msg LRESULT OnEnterSizeMove(WPARAM wParam, LPARAM lParam);

  DECLARE_MESSAGE_MAP()
public:

  BOOL Create(CWnd *owner);
  void UpdateFromDocument(CObjektiv2Doc *document);
  afx_msg void OnSize(UINT nType, int cx, int cy);
protected:
  bool AnySelection() const;
  bool AnyBreakableSelection() const;
  void ExploreObject();
  void LoadUVS();
  void LoadTexture(const char *texture);
  void ReloadLastTexture();
  bool ShowSubmenu(int containItem, CMenu *findWhere=0);

  void UpdateLongStates()
  {
    _anyselect=AnySelection();
    _anybreakableselect=_anyselect & AnyBreakableSelection();
  }

  afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
  afx_msg void OnUvsetsDeleteactive();
  afx_msg void OnUpdateUvsetsDeleteactive(CCmdUI *pCmdUI);
  afx_msg void OnUvsetsAdd();
  afx_msg void OnUvsetsChangeid();
  afx_msg void OnUveditorExit();
  afx_msg void OnDestroy();
  afx_msg void OnTcnSelchangeUvsetlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnSelectTextureFromMenu(UINT cmd);
  afx_msg void OnViewAlphachecker();
  afx_msg void OnUpdateViewAlphachecker(CCmdUI *pCmdUI);
  afx_msg void OnAlignCmd(UINT cmd);

  virtual void SelectionChanged(const Array<UVEditorViewObject> &newSelection);
  virtual void TransformedSelection(const Array<UVEditorViewObject> &newSelection);
  virtual void MovedPoints(const Array<UVEditorViewObject> &data, const Array<int> movedPts);

  virtual int GetBrightness() const {return _tex_bright;}
  virtual int GetContrast() const  {return _tex_contrst;}
  virtual void  BrightnessContrastReset() 
  {
    _tex_bright=50;
    _tex_contrst=50;
    ReloadLastTexture();
  }

  virtual void BrightnessChanged(int newval)
  {
    _tex_bright=newval;
    ReloadLastTexture();
  }
  virtual void ContrastChanged(int newval)
  {
    _tex_contrst=newval;
    ReloadLastTexture();
  }
  virtual bool GetBlackOnWhite() const
  {
    return wUVView.IsWhiteMode();
  }

  virtual void BlackOnWhiteChanged(bool newval)
  {
    wUVView.SetWhiteMode(newval);
    UpdateFromDocument(_curDocument);
  }


afx_msg void OnZoomCmd(UINT cmd);
afx_msg void OnEditBreak();
afx_msg void OnViewTexturelightness();
afx_msg void OnUpdateViewTexturelightness(CCmdUI *pCmdUI);

public:
  afx_msg void OnZoomZoommode();
  afx_msg void OnUpdateZoomZoommode(CCmdUI *pCmdUI);
  afx_msg void OnEditSelectinobject();
  afx_msg void OnEditGetselectionfromobject();
  afx_msg void OnEditAutoselectinobject();
  afx_msg void OnUpdateEditAutoselectinobject(CCmdUI *pCmdUI);
  afx_msg void OnEditCopy();
  afx_msg void OnEditPaste();
  afx_msg void OnUpdateEditPaste(CCmdUI *pCmdUI);
  afx_msg void OnEditSelectiontouv();
  afx_msg void OnUpdateEditSelectiontouv(CCmdUI *pCmdUI);
  afx_msg void OnEditMerge();
  afx_msg void OnEditSnaptopoint();
  afx_msg void OnEditUndo();
  afx_msg void OnUpdateEditUndo(CCmdUI *pCmdUI);
  afx_msg void OnEditRedo();
  afx_msg void OnUpdateEditRedo(CCmdUI *pCmdUI);
protected:
  virtual void PostNcDestroy();
public:
  afx_msg void OnPaint();
protected:
  virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:
  afx_msg void OnEditDelete();
  afx_msg void OnUpdateEditDelete(CCmdUI *pCmdUI);
  afx_msg void OnEditUnwrap();
  afx_msg void OnUpdateEditUnwrap(CCmdUI *pCmdUI);
  afx_msg void OnEditUndeform();
  afx_msg void OnFileHotkeys();
  afx_msg void OnUpdateEditUndeform(CCmdUI *pCmdUI);
  virtual BOOL PreTranslateMessage(MSG* pMsg);
  HACCEL GetDefaultAccelerator();
  afx_msg LRESULT OnChangeAccelTable(WPARAM wParam, LPARAM lParam);
  afx_msg void OnUveditorNextset();
  afx_msg void OnUveditorPreviousset();
  afx_msg void OnEditSelectall();
  afx_msg void OnEditCut();
  afx_msg void OnAlignAlignmenu();
  afx_msg void OnFilterShowalluvs();
  afx_msg void OnFilterMaintexturemenu();
  afx_msg void OnFilterMaterialmenu();
  afx_msg void OnFilterMaterialtexturemenu();
  afx_msg void OnZoomZoommenu();
  afx_msg void OnViewBrowsetexture();
  afx_msg void OnFilterBrowsenewtexture();
  afx_msg void OnFilterBrowsenewmaterial();
  afx_msg void OnFilterCreatematerial();
  void SaveHotkeys();
  void LoadHotkeys();
  void UpdateTitle();
protected:
  virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);

  virtual void ManualTransformPerformed(const DlgUVManualTransform &dlg,bool preview);

public:
  void SetMatPlugin(IMatPluginSide *mat)
  {
    _matPlugin=mat;
  }
  afx_msg void OnUpdateFilterCreatematerial(CCmdUI *pCmdUI);
  afx_msg void OnCurrentfilterNone();
  afx_msg void OnUpdateEditCopy(CCmdUI *pCmdUI);
  afx_msg void OnUpdateEditCut(CCmdUI *pCmdUI);
  afx_msg void OnViewRefresh33769();
  afx_msg void OnUveditorExport();
  afx_msg void OnViewBlackonwhite();
  afx_msg void OnUpdateViewBlackonwhite(CCmdUI *pCmdUI);
//  afx_msg void OnActivateApp(BOOL bActive, DWORD dwThreadID);
  afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
  afx_msg void OnViewBackgroundLOD();
  afx_msg void OnUpdateViewBackgroundlod(CCmdUI *pCmdUI);
  afx_msg void OnEditAutosnap();
  afx_msg void OnUpdateEditAutosnap(CCmdUI *pCmdUI);
  afx_msg void OnEditTransform();
  afx_msg void OnEditHorizontalmirror();
  afx_msg void OnEditVerticalmirror();
};


class CIChangeUVMap: public CICommand
{
  AutoArray<UVEditorViewObject,MemAllocLocal<UVEditorViewObject,10> > _uvChanged;
  bool _material;
  RString _texMat;
public:
  CIChangeUVMap(const Array<UVEditorViewObject> change, const char *texMat, bool material)
  {
    recordable=false;
    for (int i=0;i<change.Size();i++)
      if (change[i].selectedVertices) _uvChanged.Append(change[i]);
    _texMat=texMat;
    _material=material;
  }
  int Tag() const
  {
    return CITAG_CHANGEUVMAP;
  }
  bool CanRemove(const CICommand *prev)
  {
    if (prev->Tag()==Tag())
    {
      const CIChangeUVMap *prevmap=static_cast<const CIChangeUVMap *>(prev);
      if (prevmap->_uvChanged.Size()==_uvChanged.Size())
      {
        for (int i=0;i<_uvChanged.Size();i++)
          if (_uvChanged[i].faceIndex!=prevmap->_uvChanged[i].faceIndex) return false;
        if (_texMat!=prevmap->_texMat) return false;
        if (_material!=prevmap->_material) return false;
        return true;
      }
    }
    return false;
  }
  int Execute(LODObject *lodobj)
  {
    ObjectData &obj=*lodobj->Active();
    for (int i=0;i<_uvChanged.Size();i++)
    {
      const UVEditorViewObject &uvobj=_uvChanged[i];
      FaceT fc(obj,uvobj.faceIndex);
      for (int i=0;i<fc.N();i++)
      {
        fc.SetUV(i,uvobj.uv[i].u,uvobj.uv[i].v);
      }
      if (_texMat.GetLength()!=0)
        if (_material)
          fc.SetMaterial(_texMat);
        else
          fc.SetTexture(_texMat);
    }
    return 0;
  }
};