// OBJFileImport.h: interface for the COBJFileImport class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OBJFILEIMPORT_H__318C9BCF_A17D_4BF3_83B7_6AC2723E7072__INCLUDED_)
#define AFX_OBJFILEIMPORT_H__318C9BCF_A17D_4BF3_83B7_6AC2723E7072__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class COBJFileImport  
{  
    ObjektivLib::ObjectData object;

    enum _LexSymb 
      {lx_Vertex,lx_VNormal,lx_VTexture,lx_Face,lx_BeginSelection,lx_UseMat,lx_Unknown,lx_EOF,lx_NONE};

   struct _LexInfo
      {
      const char *text;
      _LexSymb value;
      };

   static _LexSymb GetCommand(std::istream &fIn);
   static _LexInfo lxinfo[];

   float _scale;
   bool _invertx,_inverty,_invertz;
   float _angle;
   
public:
	void SetScale(float s);
    void SetAngle(float s) {_angle=s;}
    void SetInvert(bool ix,bool iy,bool iz);
    
    static const char * GetError(int err);
	COBJFileImport();
	virtual ~COBJFileImport();

    int LoadFile(std::istream &fIn, int *errline=NULL);
    ObjektivLib::ObjectData &GetObject() {return object;}
};

#endif // !defined(AFX_OBJFILEIMPORT_H__318C9BCF_A17D_4BF3_83B7_6AC2723E7072__INCLUDED_)
