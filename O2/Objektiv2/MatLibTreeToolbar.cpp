// MatLibTreeToolbar.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "MatLibTreeToolbar.h"
#include ".\matlibtreetoolbar.h"
#include "../ObjektivLib/ObjToolsMatLib.h"
#include "Objektiv2Doc.h"
#include "Objektiv2View.h"
#include "ISelUse.h"
#include "ICreateProxy.h"
#include "MainFrm.h"
#include "dlginputfile.h"
#include <io.h>
#include <El/Interfaces/iScc.hpp>


#define ICON_SECTION 0
#define ICON_FOLDER 1
#define ICON_TEXTURE 2
#define ICON_MATERIAL 3
#define ICON_UNKNOWN 4
#define ICON_LEVELTOP 5
#define ICON_IMAGE 6
#define ICON_MODEL 7
#define ICON_OBJECTSECTION 8
#define ICON_SCRIPT 9

// CMatLibTreeToolbar dialog

#define DATAISPATH(data) ((data)>0xFFFF && *(char *)(data)!=0)
#define DATAISSECTION(data) ((data)>0xFFFF && *(char *)(data)==0)
#define DATAISSPECIAL(data) ((data)<=0xFFFF)
#define TMR_UPDATESSSTATUS 1023

CMatLibTreeToolbar::CMatLibTreeToolbar(CWnd* pParent /*=NULL*/)
	: CDialogBar()
{
_draggedFile=NULL;
_sectsort=SectByTex;
_draggedImage=NULL;
}

CMatLibTreeToolbar::~CMatLibTreeToolbar()
{
}


BEGIN_MESSAGE_MAP(CMatLibTreeToolbar, CDialogBar)
  ON_WM_DESTROY()
  ON_WM_SIZE()
  ON_NOTIFY(TVN_ITEMEXPANDING, IDC_TREE, OnTvnItemexpandingTree)
  ON_NOTIFY(TVN_ITEMEXPANDED, IDC_TREE, OnTvnItemexpandedTree)
  ON_WM_CONTEXTMENU()
  ON_COMMAND(ID_MATLIB_OPEN, OnMatlibOpen)
  ON_COMMAND(ID_MATLIB_EXPLORE, OnMatlibExplore)
  ON_COMMAND(ID_MATLIB_LOAD, OnMatlibLoad)
  ON_COMMAND(ID_MATLIB_SELECT, OnMatlibSelect)
  ON_COMMAND(ID_MATLIB_CREATEPROXY, OnMatlibCreateproxy)
  ON_NOTIFY(TVN_BEGINDRAG, IDC_TREE, OnTvnBegindragTree)
  ON_WM_MOUSEMOVE()
  ON_WM_LBUTTONUP()
  ON_NOTIFY(NM_DBLCLK, IDC_TREE, OnNMDblclkTree)
  ON_COMMAND_EX(ID_MATLIBSORT_BYCOUNT, OnMatlibsort)
  ON_COMMAND_EX(ID_MATLIBSORT_BYFLAGS, OnMatlibsort)
  ON_COMMAND_EX(ID_MATLIBSORT_BYTEXTURES, OnMatlibsort)
  ON_COMMAND_EX(ID_MATLIBSORT_BYMATERIALS, OnMatlibsort)
  ON_COMMAND(ID_MATLIB_RENAME, OnMatlibRename)
  ON_NOTIFY(TVN_BEGINLABELEDIT, IDC_TREE, OnTvnBeginlabeleditTree)
  ON_COMMAND(ID_MATLIB_PROPERTIES, OnMatlibProperties)
  ON_COMMAND(ID_MATLIB_HISTORY, OnMatlibHistory)
  ON_COMMAND(ID_MATLIB_GETLATESTVERSION, OnMatlibGetlatestversion)
  ON_COMMAND(ID_MATLIB_CREATEMATERIAL, OnMatLibCreateMaterial)
  ON_COMMAND(ID_MATLIB_CHECKOUT, OnMatLibCheckOut)
  ON_COMMAND(ID_MATLIB_CHECKIN, OnMatLibCheckIn)
  ON_COMMAND(ID_MATLIB_UNDOCHECKOUT, OnMatLibUndoCheckOut)
  ON_WM_TIMER()
END_MESSAGE_MAP()


// CMatLibTreeToolbar message handlers

BOOL CMatLibTreeToolbar::Create(CWnd * parent, int menuid)
  {
  if (CDialogBar::Create(parent,IDD,CBRS_LEFT,menuid)==FALSE) return FALSE;
  SetBarStyle(GetBarStyle()| CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
  EnableDocking(CBRS_ALIGN_ANY);
  SetWindowText(StrRes[IDS_MATERIALLIBRARYTITLE]);
  tree.SubclassWindow(::GetDlgItem(*this,IDC_TREE));
  if (ilist.GetSafeHandle()==NULL)
    ilist.Create(IDB_MATLIBICONS,16,0,RGB(255,0,255));
  if (iStatus.GetSafeHandle()==0)
    iStatus.Create(IDB_SSSTATUS,5,0,RGB(255,0,255));
  tree.SetImageList(&ilist,TVSIL_NORMAL);
  tree.SetImageList(&iStatus,TVSIL_STATE);
  LoadBarSize(*this,160,400);
  InitContent();
  return TRUE;
  }

void CMatLibTreeToolbar::InitContent(void)
  {
  TreeResetContent();
  _curlod=AddItemToTree(StrRes[IDS_MATLIBCURLOD],ICON_SECTION,NULL,true,true);
  _curmodel=AddItemToTree(StrRes[IDS_MATLIBCURMODEL],ICON_SECTION,NULL,true,true);
  _modelfolder=AddItemToTree(StrRes[IDS_MATLIBMODELFOLDER],ICON_SECTION,NULL,true,true);
  _libfull=AddItemToTree(StrRes[IDS_MATLIBWHOLELIB],ICON_SECTION,NULL,true,true);
  _sections=AddItemToTree(StrRes[IDS_MATLIBSECTIONS],ICON_SECTION,NULL,true,true);
  }

bool CMatLibTreeToolbar::UseLoadModeDefault(const char *filename)
  {
/*  const char *ext=Pathname::GetExtensionFromPath(filename);
  if (_stricmp(ext,".p3d")!=0)
    if (!_curLOD->GetSelection()->IsEmpty()  || _stricmp(ext,".rvmat")!=0 && frame->GetEditCommand()==ID_SURFACES_BACKGROUNDTEXTURE)
      return true;*/
  return false;
  }

HTREEITEM CMatLibTreeToolbar::AddItemToTree(const char * pathname, int icon, HTREEITEM parent, bool folder, bool isspec,int id, HTREEITEM insertAfter)
  {
  TVINSERTSTRUCT item;
  item.hInsertAfter=insertAfter;
  item.hParent=parent;
  Pathname path;
  item.item.iImage=icon;
  item.item.iSelectedImage=icon;
  item.item.mask=TVIF_CHILDREN|TVIF_IMAGE|TVIF_PARAM|TVIF_TEXT|TVIF_SELECTEDIMAGE;
  if (!isspec)
    {
    path=pathname;
    char *c=_strdup(pathname);
    item.item.cChildren=folder;
    item.item.lParam=(DWORD)c;
    item.item.pszText=const_cast<char *>(path.GetFilename());
    }
  else
    {
    item.item.cChildren=folder;
    item.item.lParam=id;
    item.item.pszText=const_cast<char *>(pathname);
    }
  SetTimer(TMR_UPDATESSSTATUS,1,0);
  return  tree.InsertItem(&item);  
  }

void CMatLibTreeToolbar::DeleteTreeItem(HTREEITEM item)
  {
  DeleteChildItems(item);
  DWORD pathid=tree.GetItemData(item);
  if (DATAISPATH(pathid) || DATAISSECTION(pathid))
    {
    char *path=(char *)pathid;
    free(path);
    }
  tree.DeleteItem(item);
  }

void CMatLibTreeToolbar::DeleteChildItems(HTREEITEM parent)
  {
  HTREEITEM chld;
  while ((chld=tree.GetNextItem(parent,TVGN_CHILD))!=NULL) DeleteTreeItem(chld);
  }

void CMatLibTreeToolbar::TreeResetContent(void)
  {
  HTREEITEM root=tree.GetRootItem();
  while (root) 
    {
    DeleteTreeItem(root);
    root=tree.GetRootItem();
    }
  }

void CMatLibTreeToolbar::OnDestroy()
  {
  TreeResetContent();
  SaveBarSize(*this);
  CDialogBar::OnDestroy();
  }

void CMatLibTreeToolbar::OnSize(UINT nType, int cx, int cy)
  {
  CRect rc(5,5,cx-5,cy-5);
  if (tree.GetSafeHwnd()) tree.MoveWindow(&rc,1);
  CDialogBar::OnSize(nType, cx, cy);
  // TODO: Add your message handler code here
  }

HTREEITEM CMatLibTreeToolbar::UpdateItemInTree(HTREEITEM &walker, const char * pathname, int icon, HTREEITEM parent, bool folder, bool isspec, int id)
  {
  if (walker==NULL) {return AddItemToTree(pathname,icon,parent,folder,isspec,id);}
  HTREEITEM probe=walker;
  HTREEITEM ret;
  if (!isspec)
    {
    DWORD data=tree.GetItemData(walker);
    const char *c=DATAISPATH(data)?(const char *)data:StrRes[IDS_MATLIBNOTMAPPED];
    while (probe && (c==NULL || _stricmp(c,pathname)!=0))
      {
      probe=tree.GetNextItem(probe,TVGN_NEXT);
      if (probe) 
        {
        DWORD data=tree.GetItemData(walker);
        c=DATAISPATH(data)?(const char *)data:StrRes[IDS_MATLIBNOTMAPPED];
        c=(const char *)tree.GetItemData(probe);
        }
      else c=NULL;
      }
    }
  else
    {
    CString s=tree.GetItemText(walker);
    while (probe && _stricmp(s,pathname)!=0)
      {
      probe=tree.GetNextItem(probe,TVGN_NEXT);
      if (probe) s=tree.GetItemText(walker);
      }
    }
  if (probe!=NULL) 
    {
    HTREEITEM delit=walker;
    while (walker!=probe)
      {
      delit=walker;
      walker=tree.GetNextItem(walker,TVGN_NEXT);
      DeleteTreeItem(delit);
      }        
    tree.SetItemImage(walker,icon,icon);
    ret=walker;
    walker=tree.GetNextItem(walker,TVGN_NEXT);    
    }
  else
    {        
    ret=tree.GetNextItem(walker,TVGN_PREVIOUS);
    if (ret==NULL) ret=TVI_FIRST;
    ret=AddItemToTree(pathname,icon,parent,folder,isspec,id,ret);
    }
  return ret;
  }

void CMatLibTreeToolbar::BeginUpdateInTree(HTREEITEM parent, HTREEITEM &walker)
  {
  walker=tree.GetNextItem(parent,TVGN_CHILD);
  }

void CMatLibTreeToolbar::EndUpdateInTree(HTREEITEM &walker)
  {
    HTREEITEM delit=walker;
    while (walker!=NULL)
      {
      delit=walker;
      walker=tree.GetNextItem(walker,TVGN_NEXT);
      DeleteTreeItem(delit);
      }        
  }

static void LoadContainer(CMatLibTreeToolbar &tree, BTree<ObjMatLibItem> &container, HTREEITEM root, int icon, bool forcemat)
  {
  ObjMatLibItem *first=container.Smallest();
  RString path;
  RString base=config->GetString(IDS_CFGPATHFORTEX);
  HTREEITEM walker;
  tree.BeginUpdateInTree(root,walker);
  if (first)
    {
    BTreeIterator<ObjMatLibItem> iter(container);
    iter.BeginFrom(*first);
    while ((first=iter.Next())!=NULL)
      {
      if (first->name.GetLength()==0) 
        tree.UpdateItemInTree(walker,StrRes[IDS_MATLIBNOTMAPPED],ICON_UNKNOWN,root,false,true,forcemat?1:2);
      else
        {
        int curicon=icon;
        path=base+first->name;

        if ((_access(path,0)!=0)||(path[path.GetLength()-1]==' ')||(path.Find('/')!=-1)) curicon=ICON_UNKNOWN;
        tree.UpdateItemInTree(walker,path,curicon,root);      
        }
      }
    }
  else
     tree.UpdateItemInTree(walker,StrRes[IDS_MATLIBEMPTY],0xFF,root,false,true);
  tree.EndUpdateInTree(walker);
  }

static void LoadMatResoures(BTree<ObjMatLibItem> &matlibtex)
{
    Pathname root=config->GetString(IDS_CFGPATHFORTEX);
    AutoArray<RString> matlist;
    ObjToolMatLib::ConvContainerToArray(matlibtex,matlist);    
    BTree<RStringI> reffiles;
    matlibtex.Clear();
    for (int i=0;i<matlist.Size();i++)
    {
      Pathname fullmatname(matlist[i],root);
      if (fullmatname.GetFullPath() != NULL)
            ObjToolMatLib::ExploreMaterial(fullmatname,reffiles);
    }
    BTreeIterator<RStringI> itr(reffiles);
    RString *item;
    while ((item=itr.Next())!=NULL) matlibtex.Add(ObjMatLibItem(*item));
}

void CMatLibTreeToolbar::UpdateCurLOD(ObjectData *obj,HTREEITEM expand)
  {
  _curLOD=obj;
  UpdateCurModelLOD(expand,NULL,obj);
  }
/*
static void UpdateTexturesInMaterial(CTreeCtrl *tree, HTREEITEM parent, BTree<ObjMatLibItem> &matlib)
  {
  Pathname base=config->GetString(IDS_CFGPATHFORTEX);
  BTree<RString> texlib;
  ObjMatLibItem *item=matlib.Smallest();
  BTreeIterator<ObjMatLibItem> iter(matlib);
  iter.BeginFrom(*item);
  while ((item=iter.Next())!=NULL)
    {
    Pathname fullpath(item->name,base);
    ObjToolMatLib::ExploreMaterial(fullpath,texlib);
    }
  tree->InsertItem(StrRes[IDS_MATLIBTEXINMAT],ICON_FOLDER ,ICON_FOLDER,
  }
*/
void CMatLibTreeToolbar::UpdateCurModelLOD(HTREEITEM expand, LODObject *lod, ObjectData *obj)
  {
  HTREEITEM textures=tree.GetNextItem(lod?_curmodel:_curlod,TVGN_CHILD);
  HTREEITEM materials=tree.GetNextItem(textures,TVGN_NEXT);
  HTREEITEM rvmatres=tree.GetNextItem(materials, TVGN_NEXT);

  if (textures && (tree.GetItemState(textures,TVIS_EXPANDED)!=0 || textures==expand))
    {
    BTree<ObjMatLibItem> matlibtex; 
    if (lod) for (int i=0;i<lod->NLevels();i++)
      lod->Level(i)->GetTool<ObjToolMatLib>().ReadMatLib(matlibtex,ObjToolMatLib::ReadTextures);
    else 
      obj->GetTool<ObjToolMatLib>().ReadMatLib(matlibtex,ObjToolMatLib::ReadTextures);
    LoadContainer(*this,matlibtex,textures,ICON_TEXTURE,false);
    }
  if (materials && (tree.GetItemState(materials,TVIS_EXPANDED)!=0 || materials==expand))
    {
    BTree<ObjMatLibItem> matlibtex; 
    if (lod) for (int i=0;i<lod->NLevels();i++)
      lod->Level(i)->GetTool<ObjToolMatLib>().ReadMatLib(matlibtex,ObjToolMatLib::ReadMaterials);
    else
      obj->GetTool<ObjToolMatLib>().ReadMatLib(matlibtex,ObjToolMatLib::ReadMaterials);
    LoadContainer(*this,matlibtex,materials,ICON_MATERIAL,true);
    }
  if (rvmatres && (tree.GetItemState(materials,TVIS_EXPANDED)!=0 || rvmatres==expand))
    {
    BTree<ObjMatLibItem> matlibtex; 
    if (lod) for (int i=0;i<lod->NLevels();i++)
      lod->Level(i)->GetTool<ObjToolMatLib>().ReadMatLib(matlibtex,ObjToolMatLib::ReadMaterials);
    else
      obj->GetTool<ObjToolMatLib>().ReadMatLib(matlibtex,ObjToolMatLib::ReadMaterials);
    LoadMatResoures(matlibtex);
    LoadContainer(*this,matlibtex,rvmatres,ICON_TEXTURE,true);
    }

  }

void CMatLibTreeToolbar::UpdateCurModel(LODObject *lod,HTREEITEM expand)
{
  _curModel=lod;
  UpdateCurModelLOD(expand,lod);
}

void CMatLibTreeToolbar::OnTvnItemexpandingTree(NMHDR *pNMHDR, LRESULT *pResult)
  {
  LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
  // TODO: Add your control notification handler code here
  *pResult = 0;
  HTREEITEM selected=pNMTreeView->itemNew.hItem;
  HTREEITEM parent=selected?tree.GetNextItem(selected,TVGN_PARENT):NULL;
  
  if (tree.GetNextItem(selected,TVGN_CHILD)!=NULL || pNMTreeView->action==TVE_COLLAPSE) return;
  
  DWORD ldata=pNMTreeView->itemNew.lParam;
  const char *data=(const char *)ldata;
  SetCursor(LoadCursor(NULL,IDC_WAIT));
  if (selected==_curlod)
    {
    AddItemToTree(StrRes[IDS_MATLIBTEXTURES],ICON_FOLDER,_curlod,true,true);
    AddItemToTree(StrRes[IDS_MATLIBMATERIALS],ICON_FOLDER,_curlod,true,true);
    AddItemToTree(StrRes[IDS_MATLIBRVMATREF],ICON_FOLDER,_curlod,true,true);
    UpdateCurLOD(_curLOD);
    }
  else if (selected==_curmodel)
    {
    AddItemToTree(StrRes[IDS_MATLIBTEXTURES],ICON_FOLDER,_curmodel,true,true);
    AddItemToTree(StrRes[IDS_MATLIBMATERIALS],ICON_FOLDER,_curmodel,true,true);
    AddItemToTree(StrRes[IDS_MATLIBRVMATREF],ICON_FOLDER,_curmodel,true,true);
    UpdateCurModel(_curModel);
    }
  else if (selected==_libfull)
    {
    Pathname path;
    path.SetDirectory(config->GetString(IDS_CFGPATHFORTEX));
    LoadDirectoryToTree(selected,path,false,SearchAll);
    }  
  else if (selected==_modelfolder)
    {
    if (_mdlprefixed)  AddItemToTree(StrRes[IDS_MATLIBDEFAULTFOLDER],ICON_LEVELTOP,_modelfolder,true,true,(int)_strdup(_modelfldr.GetDirectoryWithDrive()));
    else LoadDirectoryToTree(selected,_modelfldr,true,SearchAll);
    }  
  else if (selected==_models)
    {
    if (_mdlprefixed)  AddItemToTree(StrRes[IDS_MATLIBDEFAULTFOLDER],ICON_LEVELTOP,_models,true,true,(int)_strdup(_modelfldr.GetDirectoryWithDrive()));
    else LoadDirectoryToTree(selected,_modelfldr,true,SearchModels);
    }  
  else if (selected==_sections)
    {
    AddItemToTree(StrRes[IDS_MATLIBSECTIONSWAIT],0xFF,selected,false,true);
    tree.UpdateWindow();
    frame->PostMessage(MSG_CALCULATESECTIONS);
    }
  else if (parent==_curlod) UpdateCurLOD(_curLOD,selected);
  else if (parent==_curmodel) UpdateCurModel(_curModel,selected);
  else if (DATAISPATH(ldata)) 
    {
    HTREEITEM i1=selected,i2;
    while ((i2=tree.GetNextItem(i1,TVGN_PARENT))!=NULL) i1=i2;    
    Pathname path;
    path.SetDirectory(data);      
    LoadDirectoryToTree(selected,path,pNMTreeView->itemNew.iImage==ICON_LEVELTOP,SearchAll);
    }
  }

void CMatLibTreeToolbar::OnTvnItemexpandedTree(NMHDR *pNMHDR, LRESULT *pResult)
  {
  LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
  // TODO: Add your control notification handler code here
  *pResult = 0;
  if (pNMTreeView->action!=TVE_COLLAPSE) return;  
  HTREEITEM selected=pNMTreeView->itemNew.hItem;
  if (tree.GetNextItem(selected,TVGN_CHILD)!=NULL || pNMTreeView->action==TVE_COLLAPSE)
    {DeleteChildItems(selected);return;}

  }

struct FolderFileInfo
  {
  int order;
  Pathname path;
  };

TypeIsMovable(FolderFileInfo);

static int _cdecl SortFiles(const void *left, const void *right)
  {
  const FolderFileInfo *strleft=(const FolderFileInfo *)left;
  const FolderFileInfo *strright=(const FolderFileInfo *)right;
  if (strleft->order<strright->order) return -1;
  if (strleft->order>strright->order) return 1;
  return _stricmp(strleft->path,strright->path);
  }

void CMatLibTreeToolbar::LoadDirectoryToTree(HTREEITEM parent, Pathname path, bool topexpand, int SearchEnum)
  {
  CFileFind fnd;
  path.SetFilename("*.*");
  BOOL status=fnd.FindFile(path);
  AutoArray<FolderFileInfo> files;
  int i;
  while (status)
    {
    status=fnd.FindNextFile();
    if (fnd.IsDirectory())
      {
      if (topexpand && fnd.GetFileName()=="..")
        {
        Pathname parentpath("..\\",path);
        if (strlen(parentpath.GetDirectoryWithDrive())>=strlen(config->GetString(IDS_CFGPATHFORTEX)))
          {
          FolderFileInfo &nfo=files.Append();
          nfo.order=1;
          nfo.path=parentpath;          
          }
        }
      else 
        {
        if (!fnd.IsDots())
          {
          FolderFileInfo &nfo=files.Append();
          nfo.order=2;
          nfo.path=fnd.GetFilePath();       
          }
        }
      }
    else
      {
      Pathname filename=fnd.GetFilePath();
      const char *ext=filename.GetExtension();
      int order=0;
      if (SearchEnum & SearchModels && _stricmp(ext,".p3d")==0) order=6;
      else if (SearchEnum & SearchTextures && (_stricmp(ext,".paa")==0 || _stricmp(ext,".pac")==0)) order=5;
      else if (SearchEnum & SearchMaterials && _stricmp(ext,".rvmat")==0) order=4;
      else if (SearchEnum & SearchImages && (_stricmp(ext,".gif")==0 || _stricmp(ext,".tga")==0)) order=3;        
      else if (SearchEnum & SearchModels && (_stricmp(ext,".bio2s")==0)) order=7;        
      if (order)
        {
        FolderFileInfo &nfo=files.Append();
        nfo.order=order;
        nfo.path=filename;
        }
      }
    } 
  if (files.Size()==0)
    {
    TVITEM item;
    item.mask=TVIF_CHILDREN|TVIF_HANDLE;
    item.hItem=parent;
    item.cChildren=0;
    tree.SetItem(&item);
    }
  else
    {
    files.QSortBin(SortFiles);
    ProgressBar<int> pb(files.Size());
    for (i=0;i<files.Size();i++) 
      {
      pb.AdvanceNext(1);
      switch (files[i].order)
        {
        case 1:
          AddItemToTree(StrRes[IDS_MATLIBPARENTFOLDER],ICON_LEVELTOP,parent,true,true,(int)(_strdup(files[i].path.GetDirectoryWithDrive())));
          break;
        case 2: AddItemToTree(files[i].path,ICON_FOLDER,parent,true);break;
        case 5: AddItemToTree(files[i].path,ICON_TEXTURE,parent,false);break;
        case 4: AddItemToTree(files[i].path,ICON_MATERIAL,parent,false);break;
        case 3: AddItemToTree(files[i].path,ICON_IMAGE,parent,false);break;
        case 6: AddItemToTree(files[i].path,ICON_MODEL,parent,false);break;
        case 7: AddItemToTree(files[i].path,ICON_SCRIPT,parent,false);break;
        }
      }
    }
  }


static VOID CALLBACK DelayedUpdate(HWND hwnd,UINT uMsg,UINT_PTR idEvent,DWORD dwTime)
  {
  CMatLibTreeToolbar *self=(CMatLibTreeToolbar *)idEvent;
  KillTimer(hwnd,idEvent);
  }

void CMatLibTreeToolbar::SetModelPath(const Pathname& path)
  {  
  if (path.IsNull())
    {
    RString prefixed(config->GetString(IDS_CFGPATHFORTEX));
    if (_stricmp(_modelfldr,prefixed)==0) return;
    _modelfldr.SetDirectory(prefixed);    
    _mdlprefixed=true;
    }
  else
    {
    if (_stricmp(_modelfldr,path)==0) return;
    _modelfldr=path;
    _mdlprefixed=false;
    }
  if (tree.GetItemState(_modelfolder,TVIS_EXPANDED))
    {
    DeleteChildItems(_modelfolder);
    if (_mdlprefixed)
      {
      AddItemToTree(StrRes[IDS_MATLIBDEFAULTFOLDER],ICON_LEVELTOP,_modelfolder,true,true,(int)_strdup(_modelfldr.GetFullPath()));
      }
    else
      {
      LoadDirectoryToTree(_modelfolder,_modelfldr,true,SearchAll);
      }
    }
/*  if (tree.GetItemState(_models,TVIS_EXPANDED))
    {
    DeleteChildItems(_models);
    if (_mdlprefixed)
      {
      AddItemToTree(StrRes[IDS_MATLIBDEFAULTFOLDER],ICON_LEVELTOP,_models,true,true,(int)_strdup(_modelfldr.GetFullPath()));
      }
    else
      {
      LoadDirectoryToTree(_models,_modelfldr,true,SearchModels);
      }
    }*/
  }
void CMatLibTreeToolbar::OnContextMenu(CWnd* pWnd, CPoint point)
  {
  UINT flags;
  CPoint treept=point;
  tree.ScreenToClient(&treept);
  HTREEITEM it=tree.HitTest(treept,&flags);
  _menuitem=it;
  if (it)
    {
    CMenu mnu;
    mnu.LoadMenu(IDR_MATLIB_MENU);    
    CMenu *popup=mnu.GetSubMenu(0);
    DWORD data=tree.GetItemData(it);
    if (DATAISPATH(data))
      {
      Pathname p=(char *)data;
      const char *ext=p.GetExtension();
      DWORD attrs=GetFileAttributes(p);
      if (attrs!=0xFFFFFFFF && (attrs & FILE_ATTRIBUTE_DIRECTORY)) 
        ext="";
      else
        popup->EnableMenuItem(ID_MATLIB_CREATEMATERIAL,MF_GRAYED);

      if (_stricmp(ext,".pac")==0 || _stricmp(ext,".paa")==0
        || _stricmp(ext,".tga")==0 || _stricmp(ext,".rvmat")==0)
        {
        popup->EnableMenuItem(ID_MATLIB_CREATEPROXY,MF_GRAYED);
        }
      else if (_stricmp(ext,".p3d")==0)
        {
        }
      else if (p.GetTitle()[0]=='#')
      {
        popup->EnableMenuItem(ID_MATLIB_LOAD,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_RENAME,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_CREATEPROXY,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_HISTORY,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_GETLATESTVERSION,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_PROPERTIES,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_CHECKOUT,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_CHECKIN,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_UNDOCHECKOUT,MF_GRAYED);
      }
      else 
        {
        popup->EnableMenuItem(ID_MATLIB_LOAD,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_SELECT,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_RENAME,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_CREATEPROXY,MF_GRAYED);
        }
      if (!theApp._scc)
        {
        popup->EnableMenuItem(ID_MATLIB_HISTORY,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_GETLATESTVERSION,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_PROPERTIES,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_CHECKOUT,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_CHECKIN,MF_GRAYED);
        }
      else
      {
        tree.SetItemState(it,0,INDEXTOSTATEIMAGEMASK(0xF));
        UpdateSSStatusOneItem(it);
        int img=tree.GetItemState(it,INDEXTOSTATEIMAGEMASK(0xF))>>12;
        if (img!=2)        
          popup->EnableMenuItem(ID_MATLIB_CHECKOUT,MF_GRAYED);
        if (img!=3)
        {
          popup->EnableMenuItem(ID_MATLIB_CHECKIN,MF_GRAYED);
          popup->EnableMenuItem(ID_MATLIB_UNDOCHECKOUT,MF_GRAYED);
        }
      }
      MENUITEMINFO nfo;
      nfo.cbSize=sizeof(nfo);
      nfo.fMask=MIIM_STATE;
      nfo.fState=MFS_DEFAULT;
      if (UseLoadModeDefault(p)) popup->SetMenuItemInfo(ID_MATLIB_LOAD,&nfo);      
      else popup->SetMenuItemInfo(ID_MATLIB_OPEN,&nfo);      
      }
    else
      {
      popup->EnableMenuItem(ID_MATLIB_OPEN,MF_GRAYED);
      popup->EnableMenuItem(ID_MATLIB_EXPLORE,MF_GRAYED);
      popup->EnableMenuItem(ID_MATLIB_LOAD,MF_GRAYED);
      popup->EnableMenuItem(ID_MATLIB_SELECT,MF_GRAYED);
      popup->EnableMenuItem(ID_MATLIB_RENAME,MF_GRAYED);
      popup->EnableMenuItem(ID_MATLIB_CREATEPROXY,MF_GRAYED);
      popup->EnableMenuItem(ID_MATLIB_PROPERTIES,MF_GRAYED);
      popup->EnableMenuItem(ID_MATLIB_GETLATESTVERSION,MF_GRAYED);
      popup->EnableMenuItem(ID_MATLIB_HISTORY,MF_GRAYED);
      popup->EnableMenuItem(ID_MATLIB_CREATEMATERIAL,MF_GRAYED);
      if (it!=_curlod && it!=_curmodel)
      {
        popup->EnableMenuItem(ID_MATLIB_CHECKOUT,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_CHECKIN,MF_GRAYED);
        popup->EnableMenuItem(ID_MATLIB_UNDOCHECKOUT,MF_GRAYED);
      }
      }
    CMenu *sectsub;
    int maxi=popup->GetMenuItemCount();
    int pos;
    for (pos=0;pos<maxi;pos++) 
      {
      sectsub=popup->GetSubMenu(pos);
      if (sectsub && sectsub->GetMenuState(ID_MATLIBSORT_BYTEXTURES,MF_BYCOMMAND)!=0xFFFFFFFF) break;
      }
    if (sectsub)
      if (tree.GetNextItem(it,TVGN_CHILD)==_sections || it==_sections)
        {
        if (sectsub)
          {
          switch (_sectsort)
            {
            case SectByTex:sectsub->CheckMenuItem(ID_MATLIBSORT_BYTEXTURES,MF_CHECKED);break;
            case SectByMat:sectsub->CheckMenuItem(ID_MATLIBSORT_BYMATERIALS,MF_CHECKED);break;
            case SectByFlags:sectsub->CheckMenuItem(ID_MATLIBSORT_BYFLAGS,MF_CHECKED);break;
            case SectByCount:sectsub->CheckMenuItem(ID_MATLIBSORT_BYCOUNT,MF_CHECKED);break;
            }
          }
        }
        else
          popup->EnableMenuItem(pos,MF_BYPOSITION|MF_GRAYED);          
    popup->TrackPopupMenu(TPM_RIGHTBUTTON|TPM_LEFTBUTTON,point.x,point.y,this);      
    }
  }

void CMatLibTreeToolbar::OnMatlibOpen()
  {
  DWORD data=tree.GetItemData(_menuitem);
  if (DATAISPATH(data))
    {
    //Pathname p=(char *)data;
    char * p = _strdup((const char *)data);
    Pathname path = p; 
    if (_stricmp(path.GetExtension(),".rvmat")==0 && frame->_MAT.IsLoaded())
      {
      frame->_MAT.CallMat().EditMaterial(p);
      }
    else
      {
      int result=(int)(ShellExecute(*this,NULL,p,NULL,NULL,SW_SHOWNORMAL));
      if (result<32)
        AfxMessageBox(IDS_MATLIBSHELLERROR,MB_OK|MB_ICONINFORMATION);
      }
    }  
  }

void CMatLibTreeToolbar::OnMatlibExplore()
  {
  DWORD data=tree.GetItemData(_menuitem);
  if (DATAISPATH(data))
    {
    Pathname p=(char *)data;
    char *buff=(char *)alloca(strlen(p.GetFullPath())+50);
    sprintf(buff,"explorer.exe /select,\"%s\"",p.GetFullPath());
    STARTUPINFO strinfo;
    PROCESS_INFORMATION pinfo;

    memset(&strinfo,0,sizeof(strinfo));
    strinfo.cb=sizeof(strinfo);
    strinfo.dwFlags=0;  
    BOOL ret=CreateProcess(NULL,buff,NULL,NULL,FALSE,CREATE_DEFAULT_ERROR_MODE|NORMAL_PRIORITY_CLASS,
      NULL,NULL,&strinfo,&pinfo);
    if (ret==FALSE) return;
    SetCursor(LoadCursor(NULL,IDC_WAIT));
    WaitForInputIdle(pinfo.hProcess,30000);
    CloseHandle(pinfo.hProcess);
    CloseHandle(pinfo.hThread);
    }
  }

class CISetTextureOrMat: public CICommand
  {
  public:
    CString _texname;
    ObjToolMatLib::ReadMode _rmode;
    
    CISetTextureOrMat(const char *texname, ObjToolMatLib::ReadMode rmode):
     _texname(texname),_rmode(rmode) {recordable=false;}
    virtual int Execute(LODObject *lod)
      {
      ObjectData *obj=lod->Active();
      ObjToolMatLib &tool=obj->GetTool<ObjToolMatLib>();
      tool.SetTexOrMat(_texname,_rmode,NULL);
      return 0;
      }
  };


void CMatLibTreeToolbar::OnMatlibLoad()
  {
  DWORD data=tree.GetItemData(_menuitem);
  if (DATAISPATH(data))
    {
    Pathname p=(char *)data;
    if (_stricmp(p.GetExtension(),".p3d")==0)
      {
      if (doc->OnNewDocument()==false) return;
      doc->LoadDocument(p);
      }
    else if (_stricmp(p.GetExtension(),".rvmat")!=0 && frame->GetEditCommand()==ID_SURFACES_BACKGROUNDTEXTURE)
      {
      CObjektiv2View *curview=CObjektiv2View::GetCurrentView();
      if (curview && curview->texview)        
        {
        const char *part=ConvertTexture(p);
        curview->texview->SetTextureName(p,part);
        }
      curview->recalc_texview=true;
      curview->Invalidate();      
      }
    else
      {      
      const char *shortp=ConvertTexture(p);
      comint.Begin(WString(IDS_SETMATERIAL));
      comint.Run(new CISetTextureOrMat(shortp,_stricmp(p.GetExtension(),".rvmat")==0?ObjToolMatLib::ReadMaterials:ObjToolMatLib::ReadTextures));
      doc->UpdateAllViews(NULL);
      UpdateCurLOD(_curLOD);
      UpdateCurModel(_curModel);
      }
    }
  }

void CMatLibTreeToolbar::OnMatlibSelect()
  {
  DWORD data=tree.GetItemData(_menuitem);
  if (DATAISPATH(data))
    {
    char * path = _strdup((const char *)data);
    Pathname p=path;

    const char *partpath=config->GetString(IDS_CFGPATHFORTEX);
    int pathsz=strlen(partpath);
    char *partname=(char *)alloca(pathsz+50);
    strncpy(partname,path,pathsz);
    partname[pathsz]=0;
    if (_stricmp(partpath,partname)==0) partpath=path+pathsz;
    else partpath=path;
    if (_stricmp(p.GetExtension(),".p3d")==0)
      {
      comint.Begin(WString(IDS_SELECTUSING,p.GetFilename()));
      int i=0;
      do
        {
        i++;
        sprintf(partname,"%s\\%s.%02d",StrRes[IDS_PROXYNAME],partpath,i);
        }
      while (!comint.Run(new CISelUse(partname,i==1?SelSet:SelAdd)));
      }
    else
      {
      ObjToolMatLib &matlib=_curLOD->GetTool<ObjToolMatLib>();
      Selection *sel=new Selection(_curLOD);
      matlib.CreateSelection(partpath,ObjToolMatLib::SearchBoth,*sel);
      comint.Begin(WString(IDS_SELECTUSING,p.GetFilename()));
      comint.Run(new CISelect(sel));
      }
    }
  doc->UpdateAllViews(NULL);
  }

void CMatLibTreeToolbar::OnMatlibCreateproxy()
  {
  DWORD data=tree.GetItemData(_menuitem);
  if (DATAISPATH(data))
    {
    Pathname p=(char *)data;
    const char *shortp;
    if (_stricmp(p.GetExtension(),".p3d")==0)
      {
      shortp=ConvertTexture(p);
      comint.Begin(WString(IDS_CREATEPROXY));
      comint.Run(new CICreateProxy(doc->gismo,shortp));
      doc->UpdateAllViews(NULL);
      doc->UpdateAllToolbars(1);
      }  
    }
  }

void CMatLibTreeToolbar::OnTvnBegindragTree(NMHDR *pNMHDR, LRESULT *pResult)
  {
  LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
  if (DATAISPATH(pNMTreeView->itemNew.lParam))
    {
    _draggedFile=(const char *)pNMTreeView->itemNew.lParam;
    bool isdir=(GetFileAttributes(_draggedFile) & FILE_ATTRIBUTE_DIRECTORY)!=0;
    if (isdir) return;
    _menuitem=pNMTreeView->itemNew.hItem;
    SetCapture();
    tree.Invalidate();
    tree.UpdateWindow();
    delete _draggedImage;
    _draggedImage=tree.CreateDragImage(pNMTreeView->itemNew.hItem);
    _draggedImage->BeginDrag(0,CPoint(0,0));
    _draggedImage->DragEnter(NULL,CPoint(GetMessagePos()));
    }

  *pResult = 0;
  }

void CMatLibTreeToolbar::OnMouseMove(UINT nFlags, CPoint point)
  {
  if (GetCapture()==this && _draggedFile!=NULL)
    {
    _draggedImage->DragMove(CPoint(GetMessagePos()));
    }
  CDialogBar::OnMouseMove(nFlags, point);
  }

void CMatLibTreeToolbar::OnLButtonUp(UINT nFlags, CPoint point)
  {
  if (GetCapture()==this && _draggedFile!=NULL)
    {
    CPoint scrpt=GetMessagePos();
    _draggedImage->DragLeave(NULL);
    _draggedImage->EndDrag();
    ReleaseCapture();
    CWnd *dropview=NULL;
    bool isdir=(GetFileAttributes(_draggedFile) & FILE_ATTRIBUTE_DIRECTORY)!=0;
    POSITION pos;
    bool inwnd=false;
    for(pos=doc->GetFirstViewPosition();pos;)
      {
      dropview=doc->GetNextView(pos);
      CRect winrc;
      dropview->GetWindowRect(&winrc);
      if (winrc.PtInRect(scrpt)) {inwnd=true;break;}
      }
    if (!inwnd)
      {
      CRect winrc;
      theApp.m_pMainWnd->GetWindowRect(&winrc);
      if (winrc.PtInRect(scrpt) && !isdir) {OnMatlibLoad();return;}
/*      CWnd *wnd=WindowFromPoint(scrpt);
      CWnd *chld=ChildWindowFromPoint(scrpt);
      if (chld) wnd=chld;
      if (GetWindowLong(*wnd,GWL_EXSTYLE) & WS_EX_ACCEPTFILES)
        {
        HGLOBAL drop=GlobalAlloc(GPTR,strlen(_draggedFile)+2+sizeof(DROPFILES));
        LPDROPFILES dropinfo=(LPDROPFILES)GlobalLock(drop);
        dropinfo->pFiles=sizeof(*dropinfo);
        dropinfo->fWide=FALSE;
        dropinfo->fNC=FALSE;
        wnd->ScreenToClient(&scrpt);
        dropinfo->pt=scrpt;
        char *filelist=(char *)dropinfo+dropinfo->pFiles;
        strcpy(filelist,_draggedFile);
        filelist=strchr(filelist,0)+1;
        *filelist=0;
        GlobalUnlock(drop);
        _COleDataSource.CacheGlobalData(CF_HDROP, drop);
        _COleDataSource.Do
        DROPEFFECT dropEffect=_COleDataSource.DoDragDrop(DROPEFFECT_COPY|DROPEFFECT_MOVE,NULL);  
        GlobalFree(drop);
        }      */
      }
    else if (!isdir)
      {
      if (_stricmp(Pathname::GetExtensionFromPath(_draggedFile),".p3d")==0)        
        OnMatlibCreateproxy();
      else
        OnMatlibLoad();
      }
    }
  delete _draggedImage;
  _draggedImage=NULL;
  CDialogBar::OnLButtonUp(nFlags, point);
  }

void CMatLibTreeToolbar::OnNMDblclkTree(NMHDR *pNMHDR, LRESULT *pResult)
  {
  // TODO: Add your control notification handler code here
  OnTreeActivateItem();
  *pResult = 1;
  }

void CMatLibTreeToolbar::OnTreeActivateItem(void)
  {
  HTREEITEM item=_menuitem=tree.GetSelectedItem();
  DWORD data=tree.GetItemData(item);
  if (data==1 || data==2)
    {
    ObjToolMatLib &matlib=_curLOD->GetTool<ObjToolMatLib>();
    Selection *sel=new Selection(_curLOD);
    matlib.CreateSelection("",data==1?ObjToolMatLib::SearchMaterials:ObjToolMatLib::SearchTextures,*sel);
    comint.Begin(WString(IDS_SELECTUSING,StrRes[IDS_MATLIBNOTMAPPED]));
    comint.Run(new CISelect(sel));
    doc->UpdateAllViews(NULL);
    }
  else if (DATAISPATH(data))
    {
    const char *fname=(const char *)data;
    if (GetKeyState(VK_CONTROL) & 0x80)
      {
      if (GetKeyState(VK_SHIFT) & 0x80) OnMatlibLoad();
      else OnMatlibSelect();
      }
    else if (GetKeyState(VK_SHIFT) & 0x80) OnMatlibCreateproxy();
    else
      {
      if (UseLoadModeDefault(fname))
        OnMatlibLoad();
      else
        OnMatlibOpen();
      }
    }
  else if (DATAISSECTION(data))
  {
    void *datablock=(void *)data;
    ObjToolSections &sect=_curLOD->GetTool<ObjToolSections>();
    Selection *sel=new Selection(_curLOD);
    if (GetKeyState(VK_CONTROL)<0) (*sel)=*_curLOD->GetSelection();
    sect.SelectSection(datablock,sizeof(unsigned long),true,sel);
    comint.Begin(WString(IDS_SELECTUSING,(LPCTSTR)tree.GetItemText(item)));
    comint.Run(new CISelect(sel));
    doc->UpdateAllViews(NULL);
    }
  }


const char *CMatLibTreeToolbar::GetSelected()
  {
  HTREEITEM it=tree.GetSelectedItem();
  DWORD data=tree.GetItemData(it);
  if (DATAISPATH(data)) return (const char *)data;
  else return "";
  }

void CMatLibTreeToolbar::UpdateSections(BTree<ObjToolSections::ObjToolSectionInfo> & sections)
  {
  if (tree.GetItemState(_sections, TVIS_EXPANDED) !=0)
    {    
    char *title;   
    HTREEITEM itm=tree.GetChildItem(_sections);
    while (itm) 
      {
      void *data=(void *)tree.GetItemData(itm);
      free(data);
      tree.SetItemData(itm,0);
      itm=tree.GetNextItem(itm,TVGN_NEXT);
      }
    int arrsize=sections.Size();
    ObjToolSections::ObjToolSectionInfo **sinfo=(ObjToolSections::ObjToolSectionInfo **)alloca(arrsize*sizeof(ObjToolSections::ObjToolSectionInfo *));
    ObjToolSections::BuildSectionArray(sections,sinfo);
    for (int i=0;i<sections.Size();i++)
    {
      ObjToolSections::ObjToolSectionInfo *info=sinfo[i];
      long predata=0xFFFFFF00;
      void *blockinfo=info->PackSectionInfo(&predata,sizeof(predata));
      const char *mattitle=Pathname::GetNameFromPath(info->ptrMaterial);
      const char *textitle=Pathname::GetNameFromPath(info->ptrTexture);
      if (mattitle[0]==0) mattitle=StrRes[IDS_MATLIBNOTMAPPED];
      if (textitle[0]==0) textitle=StrRes[IDS_MATLIBNOTMAPPED];
      title=(char *)alloca(strlen(mattitle)+strlen(textitle)+50);
      switch (_sectsort)
      { 
        case SectByTex:sprintf(title,"%s [] %s (%d)",textitle,mattitle,info->count);break;
        case SectByMat:sprintf(title,"%s [] %s (%d)",mattitle,textitle,info->count);break;
        case SectByFlags:sprintf(title,"%X [] %s [] %s (%d)",info->flags,mattitle,textitle,info->count);break;
        case SectByCount:sprintf(title,"%8d [] %s [] %s",info->count,mattitle,textitle);break;
      }
      AddItemToTree(title,ICON_OBJECTSECTION,_sections,false,true,(int)blockinfo, TVI_SORT);
    }
    itm=tree.GetChildItem(_sections);
    while (itm) 
      {
      HTREEITEM next=tree.GetNextItem(itm,TVGN_NEXT);
      if (tree.GetItemData(itm)==0) tree.DeleteItem(itm);
      itm=next;
      }
    }
  }

BOOL CMatLibTreeToolbar::OnMatlibsort(UINT cmd)
  {
  // TODO: Add your command handler code here

  switch (cmd)
    {
    case ID_MATLIBSORT_BYMATERIALS:_sectsort=SectByMat;break;
    case ID_MATLIBSORT_BYTEXTURES:_sectsort=SectByTex;break;
    case ID_MATLIBSORT_BYFLAGS:_sectsort=SectByFlags;break;
    case ID_MATLIBSORT_BYCOUNT:_sectsort=SectByCount;break;
    }
  if (tree.GetItemState(_sections, TVIS_EXPANDED) !=0)
    {
    DeleteChildItems(_sections);
    AddItemToTree(StrRes[IDS_MATLIBSECTIONSWAIT],0xFF,_sections,false,true);
    tree.UpdateWindow();
    frame->PostMessage(MSG_CALCULATESECTIONS);
    }
  return TRUE;
  }

#include "AllRenameDlg.h"

void CMatLibTreeToolbar::OnMatlibRename()
  {
  DWORD data=tree.GetItemData(_menuitem);
  if (DATAISPATH(data))
    {
    const char *filename=(const char *)data;
    bool ismat=_stricmp(Pathname::GetExtensionFromPath(filename),".rvmat")==0;
    CDlgInputFile dlg(this);
    dlg.GuessFilter(filename);
    dlg.vFilename=filename;
    dlg.title=StrRes[IDS_MATLIBRENAME];
    if (dlg.DoModal()==IDOK)
      {      
      HTREEITEM p=_menuitem;
      while (tree.GetParentItem (p)!=NULL) p=tree.GetParentItem(p);      
      comint.Begin(WString(IDS_MASSRENAME,filename,dlg.vFilename));
      comint.Run(new CIMassRenameTextures(ShortPath(filename),ShortPath(dlg.vFilename),p==_curmodel));
      UpdateCurLOD(_curLOD);
      UpdateCurModel(_curModel);
      }    
    }  
  }

void CMatLibTreeToolbar::OnTvnBeginlabeleditTree(NMHDR *pNMHDR, LRESULT *pResult)
  {
  LPNMTVDISPINFO pTVDispInfo = reinterpret_cast<LPNMTVDISPINFO>(pNMHDR);
  // TODO: Add your control notification handler code here
  _menuitem=pTVDispInfo->item.hItem;
  OnMatlibRename();
  *pResult = 1;
  }

void CMatLibTreeToolbar::OnMatlibProperties()  
  {
  DWORD data=tree.GetItemData(_menuitem);
  if (DATAISPATH(data))
    {
    const char *filename=(const char *)data;
    if (theApp._scc)
      theApp._scc->Properties(filename);
    }
  }

void CMatLibTreeToolbar::OnMatlibHistory()
  {
  DWORD data=tree.GetItemData(_menuitem);
  if (DATAISPATH(data))
    {
    const char *filename=(const char *)data;
    if (theApp._scc)
      theApp._scc->History(filename);
    }
  }

void CMatLibTreeToolbar::OnMatlibGetlatestversion()
  {
  DWORD data=tree.GetItemData(_menuitem);
  if (DATAISPATH(data))
    {
    const char *filename=(const char *)data;
    if (theApp._scc)
      {
      if (GetFileAttributes(filename) & FILE_ATTRIBUTE_DIRECTORY)
        { 
        int id=AfxMessageBox(IDS_RECURSIVEQUESTION,MB_YESNOCANCEL|MB_ICONQUESTION);
        if (id==IDCANCEL) return;
        theApp._scc->GetLatestVersionDir(filename,id==IDYES);
        }
      else if (theApp._scc->UnderSSControlNotDeleted(filename))  theApp._scc->GetLatestVersion(filename);
      }
    }
  }


void CMatLibTreeToolbar::OnMatLibCreateMaterial()
  {
  DWORD data=tree.GetItemData(_menuitem);
  if (DATAISPATH(data))
    {
    const char *filename=(const char *)data;
    if (frame->_MAT.IsLoaded())
      {
      frame->_MAT.CallMat().CreateMaterial(filename);
      if (tree.GetItemState(_menuitem,TVIS_EXPANDED))
        {
        int icon;
        DeleteChildItems(_menuitem);
        tree.GetItemImage(_menuitem,icon,icon);
        LoadDirectoryToTree(_menuitem,filename,icon==ICON_LEVELTOP,SearchAll);
        }
      }
    }
  }
  void CMatLibTreeToolbar::OnTimer(UINT nIDEvent)
  {    
    if (nIDEvent==TMR_UPDATESSSTATUS && UpdateSSStatusOneItem(tree.GetRootItem())==false)
    {
      KillTimer(TMR_UPDATESSSTATUS);
    }
    CDialogBar::OnTimer(nIDEvent);
  }

  bool CMatLibTreeToolbar::UpdateSSStatusOneItem(HTREEITEM root)
  {
    if (theApp._scc)
    {
      HTREEITEM x=root;
      while (x)
      {
        DWORD data=tree.GetItemData(x);
        int icn;
        tree.GetItemImage(x,icn,icn);
        int img=tree.GetItemState(x,INDEXTOSTATEIMAGEMASK(0xF))>>12;
        tree.GetItemImage(x,icn,icn);
        if (DATAISPATH(data) && img==0 && icn!=ICON_FOLDER && icn!=ICON_FOLDER && icn!=ICON_LEVELTOP)
        {        
          const char *p=(char *)data;
          const char *flist[16];
          HTREEITEM hlist[16];
          SCCSTAT stats[16];
          int cnt=0;
          hlist[cnt]=x;
          stats[cnt]=0;
          flist[cnt++]=p;
          x=tree.GetNextItem(x,TVGN_NEXT);
          while (x!=0 && cnt<16)
          {
            DWORD data=tree.GetItemData(x);
            tree.GetItemImage(x,icn,icn);
            int img=tree.GetItemState(x,INDEXTOSTATEIMAGEMASK(0xF))>>12;
            int icn;
            tree.GetItemImage(x,icn,icn);
            if (DATAISPATH(data) && img==0 && icn!=ICON_FOLDER && icn!=ICON_FOLDER && icn!=ICON_LEVELTOP)
            {
              const char *p=(char *)data;
              hlist[cnt]=x;
              stats[cnt]=0;
              flist[cnt++]=p;
            }
            x=tree.GetNextItem(x,TVGN_NEXT);
          }

          SCCSTAT stat=theApp._scc->Status(flist,cnt,stats);
          for (int i=0;i<cnt;i++)
          {            
            if (stats[i] & SCC_STATUS_CONTROLLED)
              {
                if (stats[i] & SCC_STATUS_CHECKEDOUT)
                  img=3;
                else if (stats[i] & SCC_STATUS_OUTEXCLUSIVE || stat & SCC_STATUS_OUTOTHER || stat & SCC_STATUS_OUTBYUSER || stat & SCC_STATUS_PINNED)                
                  img=4;
                else if (stats[i] & SCC_STATUS_DELETED)
                  img=5;
                else 
                  img=2;
              }
              else
                img=1;
              tree.SetItemState(hlist[i],INDEXTOSTATEIMAGEMASK(img),INDEXTOSTATEIMAGEMASK(0xF));
          }          
          return true;
        }
        HTREEITEM child=tree.GetChildItem(x);
        if (child)
          if (UpdateSSStatusOneItem(child)) return true;
        x=tree.GetNextItem(x,TVGN_NEXT);
      }
    }
    return false;
  }

  void CMatLibTreeToolbar::OnMatLibCheckOut()
  {
    DWORD data=tree.GetItemData(_menuitem);
    if (DATAISPATH(data) && theApp._scc)
    {
      const char *filename=(const char *)data;
      tree.SetItemState(_menuitem,0,INDEXTOSTATEIMAGEMASK(0xF));
      theApp._scc->CheckOut(filename);
      UpdateSSStatusOneItem(_menuitem);
    }
    else
    {
      HTREEITEM x=tree.GetChildItem(_menuitem);
      while (x)
      {
        _menuitem=x;OnMatLibCheckOut();
        x=tree.GetNextItem(x,TVGN_NEXT);
      }
    }
  }
  void CMatLibTreeToolbar::OnMatLibCheckIn()
  {
    DWORD data=tree.GetItemData(_menuitem);
    if (DATAISPATH(data) && theApp._scc)
    {
      const char *filename=(const char *)data;
      tree.SetItemState(_menuitem,0,INDEXTOSTATEIMAGEMASK(0xF));
      theApp._scc->CheckIn(filename);
      UpdateSSStatusOneItem(_menuitem);
    }
    else
    {
      HTREEITEM x=tree.GetChildItem(_menuitem);
      while (x)
      {
        _menuitem=x;OnMatLibCheckIn();
        x=tree.GetNextItem(x,TVGN_NEXT);
      }
    }
  }
  void CMatLibTreeToolbar::OnMatLibUndoCheckOut()
  {
    DWORD data=tree.GetItemData(_menuitem);
    if (DATAISPATH(data) && theApp._scc)
    {
      const char *filename=(const char *)data;
      tree.SetItemState(_menuitem,0,INDEXTOSTATEIMAGEMASK(0xF));
      theApp._scc->UndoCheckOut(filename);
      UpdateSSStatusOneItem(_menuitem);
    }
    else
    {
      HTREEITEM x=tree.GetChildItem(_menuitem);
      while (x)
      {
        _menuitem=x;OnMatLibUndoCheckOut();
        x=tree.GetNextItem(x,TVGN_NEXT);
      }
    }
  }

