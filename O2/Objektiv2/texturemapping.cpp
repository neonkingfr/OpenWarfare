// TextureMapping.cpp: implementation of the CTextureMapping class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Objektiv2.h"
#include "TextureMapping.h"
#include "Interpolator.h"
#include "SPoint.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CTextureMapping::CTextureMapping() 
{
  pic=NULL;
  textureName=NULL;
  gray=false;
  hidden=false;
  forcefast=false;
  transp100=false;
  error=false;
  state=-1;
  SetRect(CRect(10,10,20,20));
  for (int i=0;i<4;i++)
  {
    ::CopyVektor(points3d[i],mxVector3(0,0,0));
    ::CopyVektor(undo3d[i],mxVector3(0,0,0));
  }

  
}

//--------------------------------------------------

CTextureMapping::~CTextureMapping() 
  {
  free(textureName);free(pic);
  }

//--------------------------------------------------

const unsigned char * CTextureMapping::GetRGB(int x, int y)
  {
  unsigned char *pc=(unsigned char *)cmGetData(pic);
  pc+=x*format+y*cmGetXlen(pic);
  return pc;
  }

//--------------------------------------------------

void CTextureMapping::DetectTextureFormat()
  {
  char *formchar=cmGetFormat(pic);
  if (strcmp(formchar,"RGB888")==0) format=3;else
    if (strcmp(formchar,"ARGB8888")==0) format=4;else
      format=0;
  }

//--------------------------------------------------

//---------------------- TEXTURE MAPPER ----------------------------

static int xmin,xmax,ymin,ymax,ysize,xsize;

struct MappingYLines
  {
  int xpos;
  int tupos;
  int tvpos;
  };

//--------------------------------------------------

typedef MappingYLines MapDuet[2];

static MapDuet *field;
static BITMAPINFO bmi;
static unsigned char *oneline;
static int scalex,scaley;

static void CalculateMinMax(CPoint *pt4)
  {
  xmin=xmax=pt4[0].x;
  ymin=ymax=pt4[0].y;
  for (int i=1;i<4;i++)
    {
    if (xmin>pt4[i].x) xmin=pt4[i].x;
    if (xmax<pt4[i].x) xmax=pt4[i].x;
    if (ymin>pt4[i].y) ymin=pt4[i].y;
    if (ymax<pt4[i].y) ymax=pt4[i].y;
    }  
  ysize=ymax-ymin+1;
  xsize=xmax-xmin+1;
  }

//--------------------------------------------------

static void AllocStructs()
  {    
  field=new MapDuet[ysize];
  memset(field,-1,sizeof(*field)*ysize);
  oneline=new unsigned char[3*xsize];
  memset(&bmi,0,sizeof(bmi));
  bmi.bmiHeader.biSize=sizeof(bmi.bmiHeader);
  bmi.bmiHeader.biBitCount=24;
  bmi.bmiHeader.biHeight=1;
  bmi.bmiHeader.biWidth=xsize;
  bmi.bmiHeader.biCompression=BI_RGB;
  bmi.bmiHeader.biPlanes=1;
  }

//--------------------------------------------------

static void DeallocStructs()
  {
  delete [] field;
  delete [] oneline;
  }

//--------------------------------------------------

static void InterpolateLine(CPoint *pfrom, CPoint *pto, int tu1, int tv1, int tu2, int tv2)
  {
  if (pfrom->y>pto->y)
    {
    CPoint *helper=pfrom;pfrom=pto;pto=helper;
    tu1^=tu2;
    tu2^=tu1;
    tu1^=tu2;
    tv1^=tv2;
    tv2^=tv1;
    tv1^=tv2;
    }
  int size=pto->y-pfrom->y;
  CInterpolator xint(pfrom->x,pto->x,size);
  CInterpolator tuint(tu1,tu2,size);
  CInterpolator tvint(tv1,tv2,size);
  for (int i=0;i<size;i++)
    {
    int x=xint.GetNextValue();
    if (x<0) x=0;
    int y=i+pfrom->y;
    int pos;
    if (x<field[y][0].xpos)
      {
      field[y][1]=field[y][0];
      pos=0;
      }
    else
      pos=field[y][0].xpos==-1?0:1;
    MappingYLines *pp=&(field[y][pos]);
    pp->xpos=x;
    pp->tupos=tuint.GetNextValue();
    pp->tvpos=tvint.GetNextValue();
    }
  }

//--------------------------------------------------

static void CopyLine(CTextureMapping& mapping,CInterpolator& tu,CInterpolator& tv)
  {
  int cnt=tu.GetSteps();
  unsigned char *c=oneline;
  for (int i=0;i<cnt;i++)
    {
    int ttu=tu.GetNextValue();
    int ttv=tv.GetNextValue();
    mapping.WrapXY(ttu,ttv);	
    register long p=*(long *)mapping.GetRGB(ttu,ttv);
    if (mapping.transp100 && ((p & 0xFF000000)==0)) p=0x00FF00FF;
    if (mapping.gray)  p=(p>>1) | 0x80808080;
    *(unsigned short *)c=(unsigned short)p;
    c[2]=(unsigned char)(p>>16);
    c+=3;
    }
  }

//--------------------------------------------------

static void DrawToContext(CDC *pDC, CTextureMapping& mapping)
  {
  CRect rc;
  int step,stepx;
  if (config->GetBool(IDS_CFGFASTTEXTURES) || mapping.forcefast)
    {
    stepx=1+xsize/(mapping.GetTexSizeX()*7/6);
    step=1+ysize/(mapping.GetTexSizeY()*7/6);
    }
  else
    {
    step=1;
    stepx=1;
    }
  pDC->GetClipBox(&rc);
  for (int i=0;i<ysize;i+=step) if (field[i][0].xpos>=0)
    {
    MappingYLines *ml=&(field[i][0]);
    int bx=ml[0].xpos+xmin;	
    int by=i+ymin;
    if (by<rc.top || by>rc.bottom) continue;
    int sz=ml[1].xpos-ml[0].xpos;
    int zz=0,cz=sz;
    if (bx+cz>rc.right) cz=rc.right-bx;
    if (bx<rc.left) 
      {zz=rc.left-bx;cz-=zz;bx=rc.left;}
    if (cz<0) continue;
    int cbx=bx/stepx;
    int czz=zz/stepx;
    int csz=sz/stepx;
    int ccz=cz/stepx;
    CInterpolator tu(ml[0].tupos,ml[1].tupos,csz);
    CInterpolator tv(ml[0].tvpos,ml[1].tvpos,csz);
    CopyLine(mapping,tu,tv);
    bmi.bmiHeader.biWidth=sz;
    int res=::StretchDIBits(*pDC,bx,by,cz,step,czz,0,ccz,1,oneline,&bmi,DIB_RGB_COLORS,SRCCOPY);
    //	for (int j=0;j<step;j++)
    //	  SetDIBitsToDevice((HDC)*pDC,bx,by+j,cz,1,zz,0,0,1,oneline,&bmi,DIB_RGB_COLORS);
    }
  }

//--------------------------------------------------

static bool PrepareFields( CPoint *pt4, int *tu4, int *tv4)
  {
  CalculateMinMax(pt4);
  if ((xsize*3+(int)sizeof(MapDuet)*ysize)>(config->GetInt(IDS_CFGTEXTUREMEM,1024)*1024)) return false;
  CPoint pt[4];
  for (int i=0;i<4;i++)
    {
    pt[i].x=pt4[i].x-xmin;
    pt[i].y=pt4[i].y-ymin;
    }
  AllocStructs();
  InterpolateLine(pt+0,pt+1,tu4[0],tv4[0],tu4[1],tv4[1]);
  InterpolateLine(pt+1,pt+2,tu4[1],tv4[1],tu4[2],tv4[2]);
  InterpolateLine(pt+2,pt+3,tu4[2],tv4[2],tu4[3],tv4[3]);
  InterpolateLine(pt+3,pt+0,tu4[3],tv4[3],tu4[0],tv4[0]);
  return true;
  }

//--------------------------------------------------

static void Map2Triangles(CDC *dContext, CTextureMapping& mapping, CPoint *pt4, int *tu4, int *tv4)
  {
  if (PrepareFields(pt4,tu4,tv4)==false)
    {
    if (!mapping.error && !mapping.forcefast)
      {
      AfxMessageBox(IDS_BGTEXTURELARGE);
      mapping.error=true;
      }
    return;
    }
  mapping.error=false;
  DrawToContext(dContext,mapping);
  /*  memset(field,-1,sizeof(*field)*ysize);
  InterpolateLine(pt+0,pt+2,tu4[0],tv4[0],tu4[2],tv4[2]);
  InterpolateLine(pt+2,pt+3,tu4[2],tv4[2],tu4[3],tv4[3]);
  InterpolateLine(pt+3,pt+0,tu4[3],tv4[3],tu4[0],tv4[0]);
  DrawToContext(dContext,mapping);*/
  DeallocStructs();
  }

//--------------------------------------------------

void CTextureMapping::DrawTexture(CDC *dc)
  {
  if (pic==NULL) return;
  CPoint pt[4];
  for (int i=0;i<4;i++) 
    {pt[i].x=(int)floor(pts[i].x()+0.5f);pt[i].y=(int)floor(pts[i].y()+0.5f);}
  Map2Triangles(dc,*this,pt,tu,tv);
  }

//--------------------------------------------------

void CTextureMapping::DrawBouding(CDC *pDC)
  {
  CPoint ptx[4];
  RecalculateByCommand(ptx);
  pDC->SelectStockObject(HOLLOW_BRUSH);
  pDC->Polygon(ptx,4);
  for (int i=0;i<4;i++)
    {
    CRect rc(ptx[i].x-3,ptx[i].y-3,ptx[i].x+3,ptx[i].y+3);
    pDC->FillSolidRect(&rc,0);
    }
  }

//--------------------------------------------------

int CTextureMapping::GetIndex(CPoint &pt)
  {  
  int last=0;
  if (!_finite(pts[0].x()) || !_finite(pts[0].y())) return -1;
  int a=(pt.x-pts[0].x());
  int b=(pt.y-pts[0].y());
  float min=(a*a)+(b*b);
  float c;
  for (int i=1;i<4;i++)
    {	
    if (_finite(pts[i].x()) && _finite(pts[i].y()))
      {
      a=(pt.x-pts[i].x());
      b=(pt.y-pts[i].y());
      c=a*a+b*b;
      if (c<min) 
        {min=c;last=i;}
      }
    }
  if (abs(pts[last].x()-pt.x)>4 || abs(pts[last].y()-pt.y)>4) return -1;
  return last;
  }

//--------------------------------------------------

void CTextureMapping::SetIndex(int idx, float x, float y)
  {
  pts[idx].x()=x;
  pts[idx].y()=y;
  }

//--------------------------------------------------

static bool Prusecik(SPoint2DF *pt4, SPoint2DF& prusecik)
  {
  float ax,ay,bx,by;
  ax=pt4[0].x()-pt4[1].x();
  bx=pt4[3].x()-pt4[2].x();
  ay=pt4[0].y()-pt4[1].y();
  by=pt4[3].y()-pt4[2].y();
  float d=ax*(-by)-bx*(-ay);
  if (fabs(d)<0.00001f) return false;
  d=1/d;
  float xs=(float)(pt4[3].x()-pt4[0].x());
  float ys=(float)(pt4[3].y()-pt4[0].y());
  float t1=d*(-xs*by+ys*bx);
  float t2=d*(-xs*ay+ys*by);
  prusecik.x()=(pt4[0].x()+ax*t1);
  prusecik.y()=(pt4[0].y()+ay*t1);
  return true;
  }

//--------------------------------------------------

static float BodNaUsecce(SPoint2DF &pt1, SPoint2DF &pt2, SPoint2DF &ptt)
  {
  SPoint2DF dpa,dpb;
  
  dpa=pt2-pt1;
  dpb=ptt-pt1;
  if (abs(dpa.x())<abs(dpa.y()))	
    return (float)dpb.y()/(float)dpa.y();
  else
    return (float)dpb.x()/(float)dpa.x(); 
  }

//--------------------------------------------------

bool CTextureMapping::GetTextureMapping(HVECTOR v, float &ntu, float &ntv)
  {  
  SPoint2DF pt(v);
  if (pic==NULL) return false;
  SPoint2DF prusecik,tx1,tx2;
  float tu1,tv1,tu2,tv2;
  SPoint2DF data[4];
  SPoint2DF ptx[4];
  for (int i=0;i<4;i++)
    ptx[i].x()=pts[i].x(),ptx[i].y()=pts[i].y();
  if (Prusecik(ptx,prusecik)==false)
    {
    if (pts[0]==pts[1])
      {
      if (pts[2]==pts[3]) return false;
      prusecik=ptx[2]+(pt-ptx[3]);
      if (prusecik==pt) prusecik=ptx[3]+(pt-ptx[2]);
      }
    else
      {
      prusecik=ptx[0]+(pt-ptx[1]);
      if (prusecik==pt) prusecik=ptx[1]+(pt-ptx[0]);
      }
    }
  data[0]=ptx[1];
  data[1]=ptx[2];
  data[2]=prusecik;
  data[3]=pt;
  if (Prusecik(data,tx1)==false) tx1=ptx[1];
  data[0]=ptx[3];
  data[1]=ptx[0];
  if (Prusecik(data,tx2)==false) tx1=ptx[2];
  float f1=BodNaUsecce(ptx[1],ptx[2],tx1);
  float f2=BodNaUsecce(ptx[3],ptx[0],tx2);
  tu1=tu[1]+(tu[2]-tu[1])*f1;
  tv1=tv[1]+(tv[2]-tv[1])*f1;
  tu2=tu[3]+(tu[0]-tu[3])*f2;
  tv2=tv[3]+(tv[0]-tv[3])*f2;
  float f3=BodNaUsecce(tx1,tx2,pt);
  ntu=tu1+(tu2-tu1)*f3;
  ntv=tv1+(tv2-tv1)*f3;
  ntu/=(float)cmGetXSize(pic);
  ntv/=(float)cmGetYSize(pic);
  return true;
  }

//--------------------------------------------------

void CTextureMapping::BackgroundFromTex(HVECTOR v1, float tu1, float tv1, HVECTOR v2, float tu2, float tv2, HVECTOR v3, float tu3, float tv3)
  {
  HMATRIX m1,m2,m3,m4;
  SPoint2DF pt1(v1),pt2(v2),pt3(v3);
  
  JednotkovaMatice(m1);
  m1[0][0]=tu2-tu1;m1[0][1]=tv2-tv1;
  m1[1][0]=tu3-tu1;m1[1][1]=tv3-tv1;
  m1[3][0]=tu1;m1[3][1]=tv1;
  InverzeMatice(m1,m4);
  JednotkovaMatice(m2);
  SPoint2DF base1,base2;
  base1=pt2-pt1;
  base2=pt3-pt1;
  m2[0][0]=(float)base1.x();m2[0][1]=(float)base1.y();
  m2[1][0]=(float)base2.x();m2[1][1]=(float)base2.y();
  m2[3][0]=(float)pt1.x();m2[3][1]=(float)pt1.y();
  SoucinMatic(m4,m2,m3);
  HVECTOR vx[4]=
    {
      {0,0,0,1},
      {1,0,0,1},
      {1,1,0,1},
      {0,1,0,1}
    }  ;
  for (int i=0;i<4;i++)
    {
    HVECTOR tv;
    TransformVector(m3,vx[i],tv);
    CorrectVectorM(tv);
    pts[i].x()=tv[XVAL];
    pts[i].y()=tv[YVAL];
    }
  SetTuTv(0,0,0);
  SetTuTv(1,1,0);
  SetTuTv(2,1,1);
  SetTuTv(3,0,1);
  }

//--------------------------------------------------

void CTextureMapping::CheckPoints(int moveidx)
  {
  int left=(moveidx+1)&3;
  int right=(moveidx-1)&3;
  SPoint2DF pt=(pts[left]+pts[right]);
  pt.x()/=2;
  pt.y()/=2;
  pts[(moveidx+2)&3]=(pt-pts[moveidx])+pt;
  }

//--------------------------------------------------

void CTextureMapping::SetTextureName(const char *filename,const char *shortname)
  {
  TUNIPICTURE *pic=cmLoadUni((char *)filename);
  if (pic==NULL) 
    {
    free(SetTexture(NULL));
    return;
    }
  TUNIPICTURE *pic2=cmConvertUni(pic,"ARGB8888",1);
    {
    unsigned int x,y;
    unsigned char a,*c,*d;
    int ln=pic2->xlinelen;
    c=(unsigned char *)cmGetData(pic2);
    for (y=0;y<pic->ysize;y++,c+=ln)
      for (d=c,x=0;x<pic->xsize;x++,d+=4)
        {
        int col;
        if ((x>>3)+(y>>3)&1) col=0x80;else col=0x70;
        a=d[3];
        d[0]=((255-a)*col+a*d[0])>>8;
        d[1]=((255-a)*col+a*d[1])>>8;
        d[2]=((255-a)*col+a*d[2])>>8;	  
        }
    } 
  free(pic);
  pic2=SetTexture(pic2);
  free(pic2);
  free(textureName);
  textureName=_strdup(shortname);  
  }

//--------------------------------------------------

static int TestLine(SPoint2DF &pt1, SPoint2DF &pt2, SPoint2DF &pt)
  {
  SPoint2DF pr=pt2-pt1;
  int a,b,c;
  a=pr.y();
  b=-pr.x();
  c=(a*pt1.x()+b*pt1.y());
  int e=(a*pt.x()+b*pt.y());
  return (e>c)-(e<c);
  }

//--------------------------------------------------

bool CTextureMapping::TestPointInside(CPoint &pt)
  {
  int a1,a2,a3,a4;
  SPoint2DF ptf;
  ptf.x()=pt.x;
  ptf.y()=pt.y;
  a1=TestLine(pts[0],pts[1],ptf);
  a2=TestLine(pts[1],pts[2],ptf);
  a3=TestLine(pts[2],pts[3],ptf);
  a4=TestLine(pts[3],pts[0],ptf);
  return (a1==a2 && a2==a3 && a3==a4 && a1!=0);
  }

//--------------------------------------------------

void CTextureMapping::MoveToPoint(CPoint pt)
  {
  CPoint rel=pt-ref;
  for (int i=0;i<4;i++) 
    {
    pts[i].x()+=rel.x;
    pts[i].y()+=rel.y;
    }
  ref=pt;
  }

//--------------------------------------------------

void CTextureMapping::MirrorX()
  {  
  if (pic==NULL) return;
  for (int i=0;i<4;i++)
    tu[i]=cmGetXSize(pic)-tu[i];
  }

//--------------------------------------------------

void CTextureMapping::MirrorY()
  {
  if (pic==NULL) return;
  for (int i=0;i<4;i++)
    tv[i]=cmGetYSize(pic)-tv[i];
  }

//--------------------------------------------------

void CTextureMapping::RotateL()
  {
  float u,v;
  u=tu[0];v=tv[0];
  tu[0]=tu[1];tv[0]=tv[1];
  tu[1]=tu[2];tv[1]=tv[2];
  tu[2]=tu[3];tv[2]=tv[3];
  tu[3]=u;tv[3]=v;
  }

//--------------------------------------------------

void CTextureMapping::RotateR()
  {
  float u,v;
  u=tu[3];v=tv[3];
  tu[3]=tu[2];tv[3]=tv[2];
  tu[2]=tu[1];tv[2]=tv[1];
  tu[1]=tu[0];tv[1]=tv[0];
  tu[0]=u;tv[0]=v;
  }

//--------------------------------------------------

void CTextureMapping::CorrectActpect()
  {
  if (pic==NULL) return;
  SPoint2DF lf=pts[0];
  SPoint2DF a=pts[1]-lf;
  SPoint2DF b=pts[3]-lf;
  SPoint2DF c=pts[2]-pts[1];
  float asp=(float)cmGetYSize(pic)/(float)cmGetXSize(pic);
  b.x()=-a.y();
  b.y()=a.x();
  pts[3]=lf+b*asp;
  pts[2]=pts[1]+b*asp;
  }

//--------------------------------------------------

void CTextureMapping::Scale(int pt, CPoint &nw)
  {
  modifycom=CTMCOM_SCALE;
  ref=nw;
  modifyparm=pt;
  }

//--------------------------------------------------

void CTextureMapping::CalcScale(CPoint &out, int pt, CPoint &nw)
  {
  int refpt=(pt+2)&3;
  SPoint2DF fnw;fnw.x()=nw.x;fnw.y()=nw.y;
  SPoint2DF rf1=pts[pt]-pts[refpt];
  SPoint2DF rf2=fnw-pts[refpt];
  float scalex=(float)rf2.x()/(float)rf1.x();
  float scaley=(float)rf2.y()/(float)rf1.y();
  out.x=(int)floor(((out.x-pts[refpt].x())*scalex)+pts[refpt].x()+0.5f);
  out.y=(int)floor(((out.y-pts[refpt].y())*scaley)+pts[refpt].y()+0.5f);
  }

//--------------------------------------------------

void CTextureMapping::CalcRotate(CPoint &out, CPoint &center, int rotate)
  {
  float xr=(float)(out.x-center.x);
  float yr=(float)(out.y-center.y);
  float rad=torad((float)rotate/100.0f);
  float xr1=xr*cos(rad)-yr*sin(rad);
  float yr1=xr*sin(rad)+yr*cos(rad);
  out.x=(int)floor(center.x+xr1+0.5f);
  out.y=(int)floor(center.y+yr1+0.5f);
  }

//--------------------------------------------------

void CTextureMapping::Rotate(float dec, CPoint &center)
  {
  modifycom=CTMCOM_ROTATE;
  ref=center;
  modifyparm=(int)(dec*100.0f);  
  }

//--------------------------------------------------

CPoint CTextureMapping::GetCenter()
  {
  SPoint2DF pt=pts[0]+pts[1]+pts[2]+pts[3];
  pt.x()/=4;
  pt.y()/=4;
  return CPoint(pt.x(),pt.y());
  }

//--------------------------------------------------

void CTextureMapping::DoCommand()
  {
  CPoint ptx[4];
  RecalculateByCommand(ptx);
  for (int i=0;i<4;i++) 
    {
    pts[i].x()=ptx[i].x;
    pts[i].y()=ptx[i].y;
    }
  modifycom=CTMCOM_NONE;
  }

//--------------------------------------------------

void CTextureMapping::RecalculateByCommand(CPoint *ptx)
  {
  for (int i=0;i<4;i++)
    {
    ptx[i].x=(int)floor(pts[i].x()+0.5f);
    ptx[i].y=(int)floor(pts[i].y()+0.5f);
    switch (modifycom)
      {
      case CTMCOM_ROTATE: CalcRotate(ptx[i],ref,modifyparm);break;
      case CTMCOM_SCALE: CalcScale(ptx[i],modifyparm,ref);break;
      }	
    }
  }

//--------------------------------------------------

void CTextureMapping::SaveUndo()
  {
  for (int i=0;i<4;i++) 
    undo[i]=pts[i];
  CopyMatice(undo3d,points3d);
  }

//--------------------------------------------------

void CTextureMapping::Undo()
  {
  HMATRIX mm;
  SPoint2DF ptx;
  for (int i=0;i<4;i++) 
    {
    ptx=pts[i];
    pts[i]=undo[i];
    undo[i]=ptx;
    }
  CopyMatice(mm,points3d);
  CopyMatice(points3d,undo3d);
  CopyMatice(undo3d,mm);
  }

//--------------------------------------------------

void CTextureMapping::KeyMove(int x, int y)
  {
  SPoint2DF pt;
  pt.x()=x;
  pt.y()=y;
  for (int i=0;i<4;i++) pts[i]+=pt;
  }

//--------------------------------------------------

void CTextureMapping::KeyScale(int x, int y)
  {
  SPoint2DF pt=GetCenter();
  int i;
  for (i=0;i<4;i++) 
    if (pts[i].x()>pt.x() && pts[i].y()>pt.y()) break;
  if (i==4) i=2;
  pt=pts[i];
  pt.x()+=x;
  pt.y()+=y;
  Scale(i,CPoint(floor(pt.x()+0.5f),floor(pt.y()+0.5f)));
  DoCommand();
  //  CheckPoints(i);
  }

//--------------------------------------------------

