// Objektiv2Doc.h : interface of the CObjektiv2Doc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_OBJEKTIV2DOC_H__6F94A43A_55DE_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_OBJEKTIV2DOC_H__6F94A43A_55DE_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#include "../ObjektivLib/Edges.h"	// Added by ClassView
#include "TexMappingDlg.h"	// Added by ClassView
#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ComInt.h"
#include "PolygonSelection.h"
#include "3DC2.h"	// Added by ClassView
#include "ColorizeDlg.h"	// Added by ClassView

class CObjektiv2View;

enum EGismo 
  {gismo_hide,gismo_show,gismo_selected};

class SccFunctions;

#include "ProxyCache.h"


class CObjektiv2Doc : public CDocument
  {
  protected: // create from serialization only
    CObjektiv2Doc();
    DECLARE_DYNCREATE(CObjektiv2Doc)
      
      // Attributes
    public:
    float *pin;
    HMATRIX gismo;
    HMATRIX gslast;
    HMATRIX ghost;    
    bool localAxisMode;
    bool showghost;
    bool selghost;
    EGismo gismo_mode;
    LODObject *LodData;
    Pathname filename;
    Pathname orgname;
    bool isuntitled;
    bool seltransform; //selection is transfomed;
    bool selcopy; //selection will be copied
    HMATRIX seltr;	//selection transfom Matrix
    HMATRIX selrecord;
    bool selrecordenable;
    LPHVECTOR GetPointFromFace(ObjectData *od, FaceT& face, int index,LPHVECTOR vx=NULL);
    CTexMappingDlg mapdlg;
    bool pinlock;
    int prevanim;
    int curanim; //curent animation frame number
    int frozenanim; //frozen animation frame number; -1 unfrozen
    float bgrnlod;
    CEdges edges;
    CEdges drawedges;
    C3DC2 drawct;
    bool colorize;
    bool extupdate; //extenal update is needed
    bool automapsel; //shows automap selection
    SRef<Selection> combine_saved; //saved selection to combine	    
    AutoArray<VecT> byNormals; //Transform by Normals
    int numSections;
    HWND extaschild;
    DWORD lastP3Dsize;

    bool _isSccControled;
    bool _isCheckedOut;
    bool _ssCanBeRedoed;
    bool _ssGettingVer; 

    class DocProxyCache: public ProxyCache
    {
    public:
      virtual RString ProxyNotFound(RString name);
    };

    DocProxyCache _proxyCache;
    float _proxyLevel;

    float GetProxyLevel();

    ObjectData *GetBgrnLod()
      {
      if (!config->GetBool(IDS_CFGSHOWBACKGROUNDLOD)) return NULL;
      int i=LodData->FindLevel(bgrnlod);
      if (i==-1) return NULL;
      ObjectData *odata=LodData->Level(i);
      if (odata==LodData->Active()) return NULL;
      return odata;
      }
    
    // Operations
  public:
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CObjektiv2Doc)
  public:
    virtual BOOL OnNewDocument();
    virtual void Serialize(CArchive& ar);
    virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
    virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
    //}}AFX_VIRTUAL
    
    // Implementation
  public:
    enum DrawMode {WireFrame, Solid, SolidWires, Background, Animation};

    CColorizeDlg coldlg;
    BOOL AskSaveAs(); //ulozeni dokumentu s dotazem na jmeno. vraci TRUE pri uspechu
    BOOL AskSave();	//ulozeni dokumentu. Vraci TRUE pri uspechu.
    void UpdateModel();
    void UpdateMatLib();
    bool ImportTextures(const char *name);
    void Autosave();
    //	void LoadDrawEdges();
    CFaceVisList facevis;
    void UpdateStatus();
    void LoadEdges();
    void OnInitMenu(CMenu *pMenu);
    void OnCylindricMapping(HVECTOR pos);
    void UpdateViewer(CObjektiv2View *sender);
    CObjektiv2View *g3dview;
    void ApplyPreviewTransform(bool shift=false);
    void EndTransformPreview() 
      {seltransform=false;}
    void TransformPreview(HMATRIX mm);
    void PolygonSelection(C3DC& cnt,CPolygonSelection &pol,bool ctrl=false, bool shift=false);
    void RectangleSelection(C3DC& cnt,CRect lastrect,bool set, bool ctrl=false, bool shift=false);
    void TouchFace(C3DC& cnt, CPoint &pt,bool ctrl, bool shift, bool polygon);
    void DrawPointsToView(C3DC &ct);
    bool DrawToViewObject(ObjectData &obj, C3DC &ct, bool mass);
    void DrawToViewSolidObject(ObjectData &obj, C3DC& ct, bool wires, HVECTOR ldir, HVECTOR cdir);
    bool DrawToView(C3DC& ct, DrawMode mode, int frame=-1);
    bool DrawProxyToView(ObjectData *proxy,const Matrix4 &proxyMatrix, C3DC &ct, bool selected);
    //    void DrawToViewGhost(C3DC &ct);
    void DrawGismo(C3DC &ct);
    void UpdateAll();
    void UpdateAllToolbars(int updatelevel=0);
    void SetDocumentName(Pathname &filename);
    bool LoadDocument(Pathname& filename);
    Pathname& GetDocumentName() 
      {return filename;}
    virtual ~CObjektiv2Doc();
    
    void UndoGizmo() 
      {HMATRIX x;CopyMatice(x,gismo);CopyMatice(gismo,gslast);CopyMatice(gslast,x);}
    void EditGizmo(HMATRIX matice)
      {CopyMatice(gslast,gismo);SoucinMatic(gslast,matice,gismo);}

      void UpdateAllViews( CView* pSender,LPARAM lHint = 0L, CObject* pHint = NULL );

    bool FindAttachPoint(const CPoint &ms, const LPHMATRIX camera, VecT &found);

#ifdef _DEBUG
    virtual void AssertValid() const;
    virtual void Dump(CDumpContext& dc) const;
    #endif

    void UpdateFileSSStatus();

  public:	// Need access to this from d3drenderer
    void SelectionTransform(int point, VecT &vector, LPHVECTOR out, float weight);
  protected:
    void AttachLocalAxis();
    
    // Generated message map functions
  protected:
    //{{AFX_MSG(CObjektiv2Doc)
    afx_msg void OnFileSaveAs();
    afx_msg void OnFileOpen();
    afx_msg void OnFileSave();
    afx_msg void OnViewRefresh();
    afx_msg void OnUpdateAllViews();
    afx_msg void OnViewHidesel();
    afx_msg void OnViewLocksel();
    afx_msg void OnViewUnhidesel();
    afx_msg void OnViewUnlock();
    afx_msg void OnEditSelectAll();
    afx_msg void OnEditDeselect();
    afx_msg void OnEditDeselectVertices();
    afx_msg void OnEditDeselectFaces();
    afx_msg void OnGrowSelect();
    afx_msg void OnShrinkSelect();
    afx_msg void OnEditInvertselection();
    afx_msg void OnEditCenterpin();
    afx_msg void OnTransform(UINT cmd);
    afx_msg void OnTransformUser();
    afx_msg void OnSurfacesSpeheremap();
    afx_msg void OnUpdateSurfacesCylmap(CCmdUI* pCmdUI);
    afx_msg void OnUpdateSurfacesSpeheremap(CCmdUI* pCmdUI);
    afx_msg void OnSimpleCommand(UINT cmd);
    afx_msg void OnPointsProperties();
    afx_msg void OnFaceProperties();
    afx_msg void OnUpdateFaceProperties(CCmdUI* pCmdUI);
    afx_msg void OnViewColorizeobjects();
    afx_msg void OnEditLockpin();
    afx_msg void OnUpdateEditLockpin(CCmdUI* pCmdUI);
    afx_msg void OnEditCopy();
    afx_msg void OnEditCut();
    afx_msg void OnEditPaste();
    afx_msg void OnSurfacesMapping();
    afx_msg void OnUpdateModel();
    afx_msg void OnFacesExtrude();
    afx_msg void OnTrans2Rotate();
    afx_msg void OnTrans2Scale();
    afx_msg void OnTrans2Mirrorx();
    afx_msg void OnTrans2Mirrory();
    afx_msg void OnCreateShape();
    afx_msg void OnEditElasticsel();
    afx_msg void OnUpdateStatusBar(CCmdUI* pCmdUI);
    afx_msg void OnViewRestartexternal();
    afx_msg void OnStructCoranim();
    afx_msg BOOL OnFileMerge(UINT cmd);
    afx_msg void OnFileExport3dstudio();
    afx_msg void OnViewCenterzoomAuto();
    afx_msg void OnEditSelectoneside();
	afx_msg void OnEditUnselectTriangles();
    afx_msg void OnPointsMergenear();
    afx_msg void OnFacesSplithole();
    afx_msg void OnViewUncolorize();
    afx_msg void OnFacesMove(UINT cmd);
    afx_msg void OnStructureMultiplepincull();
    afx_msg void OnCreateProxy();
    afx_msg void OnFileSavetoserver();
    afx_msg void OnUpdateFileSavetoserver(CCmdUI* pCmdUI);
    afx_msg void OnSurfacesSlucovani();
    afx_msg void OnPointsFlatten();
    afx_msg void OnToolsAnimnorm();
    afx_msg void OnFileImport3ds();
    afx_msg void OnToolsCopyweights();
    afx_msg void OnSurfacesShowgismo();
    afx_msg void OnSurfacesEditgismo();
    afx_msg void OnSurfacesResetgismo();
    afx_msg void OnSurfacesUndogizmo();
    afx_msg void OnSurfacesGizmomapping();
    afx_msg void OnFileExportMda();
    afx_msg void OnToolsFind3weights();
    afx_msg void OnToolsWeightautomation();
    afx_msg void OnToolsAdaptovaniAnimace();
    afx_msg void OnFileExportOldVersion();
    afx_msg void OnFileImportASFAMC();
    afx_msg void OnFileImportBVH();
    afx_msg void OnToolsMergesquarizealllods();
    afx_msg void OnSurfacesMergetextures();
    afx_msg void OnStructureSquarizealllods();
    afx_msg void OnStructureGhostsShow();
    afx_msg void OnUpdateStructureGhostsShow(CCmdUI* pCmdUI);
    afx_msg void OnStructureGhostsSelect();
    afx_msg void OnUpdateStructureGhostsSelect(CCmdUI* pCmdUI);
    afx_msg void OnStructureGhostMaterialize();
    afx_msg void OnUpdateStructureGhostMaterialize(CCmdUI* pCmdUI);
    afx_msg void OnStructureGhostCreate();
    afx_msg void OnSurfacesTexelcalculations();
    afx_msg void OnPointsLocknormals();
    afx_msg void OnFileImportMayaweightscript();
	afx_msg void OnFileImportBianmmayaanimationexport();
	afx_msg void OnFileImportObjfile();
	afx_msg void OnViewViewersFreezeanimation();
	afx_msg void OnUpdateViewViewersFreezeanimation(CCmdUI* pCmdUI);
	afx_msg void OnSurfacesChilliskinner();
	afx_msg void OnStructureCarve();
	afx_msg void OnToolsMasstexturematerialrenaming();
	afx_msg void OnStructureChecktextures();
    afx_msg void OnPointsRemove();

    afx_msg void OnRecordTransform();
    afx_msg void OnUseRecordedTransform();
    afx_msg void OnSaveRecordedTransform();
    afx_msg void OnLoadRecordedTransform();
    afx_msg void OnUpdateRecordTransform(CCmdUI *pCmd);
    
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  public:
    afx_msg void OnConvexityFindncpoints();
    afx_msg void OnConvexityNewconvexhull();
    afx_msg void OnImportUniversalbistudiotxt();
    afx_msg void OnSourcecontrolSaveandcheckin();
    afx_msg void OnUpdateSourcecontrol(CCmdUI *pCmdUI);
    afx_msg void OnSourcecontrolGetlatestversion();
    afx_msg void OnSourcecontrolUndocheckout();
    afx_msg void OnSourcecontrolCheckout();
    afx_msg void OnSourcecontrolShowhistory();
    afx_msg void OnUpdateSourceControl_Controlled(CCmdUI *pCmdUI);
    afx_msg void OnUpdateSourceControl_Loaded(CCmdUI *pCmdUI);
    afx_msg void OnUpdateSourceControl_CheckedOut(CCmdUI *pCmdUI);
    afx_msg void OnUpdateSourceControl_CheckedIn(CCmdUI *pCmdUI);
    afx_msg void OnUpdateSourceControl_RedoCheckout(CCmdUI *pCmdUI);
    afx_msg void OnUpdateSourceControl_GetVer(CCmdUI *pCmdUI);
    afx_msg void OnFileOpenfromss();
    afx_msg void OnSourcecontrolRedocheckout();
    void ReloadCurEditedFile(void);
    afx_msg void OnConvexityConvexityfinetune();
    afx_msg void OnStructureFindflags();
    afx_msg void OnExportBitxt();

    afx_msg void OnViewProxylevel();
    afx_msg void OnTransform3dLocalaxismenu();
    afx_msg void OnTransform3dEdtlocalaxis();
    afx_msg void OnUpdateTransform3dEdtlocalaxis(CCmdUI *pCmdUI);
    afx_msg void OnTransform3dShowlocalaxis();
    afx_msg void OnUpdateTransform3dShowlocalaxis(CCmdUI *pCmdUI);
    afx_msg void OnTransform3dSelectionwithlocalaxis();
    afx_msg void OnUpdateTransform3dSelectionwithlocalaxis(CCmdUI *pCmdUI);
    afx_msg void OnTransform3dAttachtoface();
    afx_msg void OnTransform3dFromproxy();

    afx_msg void OnUvSetActivate(UINT cmd);
    afx_msg void OnUvsetsAdd();
    afx_msg void OnUvsetsDeleteactive();
    afx_msg void OnUpdateUvsetsDeleteactive(CCmdUI *pCmdUI);
    afx_msg void OnUvsetsChangeid();

    afx_msg void OnEditUndo();
    afx_msg void OnUpdateEditUndo(CCmdUI* pCmdUI);
    afx_msg void OnEditRedo();
    afx_msg void OnUpdateEditRedo(CCmdUI* pCmdUI);

    afx_msg void OnSelectionOperation(UINT cmd);
};









extern CObjektiv2Doc *doc;
extern CComInt comint;

void DrawModel(ObjectData *odata, C3DC& cnt, COLORREF color, int animation=-1);

class CISelect: public CICommand
  {
  Selection *sel;
  public:
    CISelect(Selection *sel):sel(sel)
      {recordable=false;marksave=false;}	 
    virtual ~CISelect() 
      {delete sel;}
    int Execute(LODObject *obj)
      {
      ObjectData *odata=obj->Active();	 
      sel->SetOwner(odata);
      Selection *s=(Selection *)odata->GetSelection();
      *s=*sel;	  
      return 0;
      }
    virtual int Tag() const 
      {return CITAG_SELECT;}
    virtual bool CanRemove(const CICommand *prev) 
      {
      int tg=prev->Tag();
      return tg==CITAG_SELECT || 
        tg==CITAG_SELECTBYTEXTURE || 
          tg==CITAG_SELUSE;
      
      }
    static bool SelectByMode(Selection *sel, ObjectData *obj, int mode, bool shift, bool ctrl);
    static bool SelectFaces(ObjectData *obj, Selection *s,bool verttoo=true);
    static bool SelectObjects(ObjectData *obj, Selection *s);
  };










class CIDefineNamedSelection:public CICommand
  {
  NamedSelection *sel;
  public:
    CIDefineNamedSelection(NamedSelection *s):sel(s) 
      {recordable=false;}
    virtual ~CIDefineNamedSelection() 
      {delete sel;}
    int Execute(LODObject *lod)
      {
      ObjectData *obj=lod->Active();
      int n=obj->FindNamedSel(sel->Name());
      if (n==-1) 
        {
        obj->SaveNamedSel(sel->Name());
        n=obj->FindNamedSel(sel->Name());
        if (n==-1) return -1;
        }
      NamedSelection *q=obj->GetNamedSel(n);
      *q=*sel;
      return 0;
      }
  };





template<class Functor>
void EnumAllProxyPoints(const ObjectData *odata, const Selection *hid, const Selection *proxies, ProxyCache &proxyCache, float proxyLevel, const Functor &funct, Selection *sel)
{
    if (proxies)
    for (int i=0,count=odata->NFaces();i<count;++i) if (proxies->FaceSelected(i) && !hid->FaceSelected(i))
    {      
      FaceT fc(odata,i);
      ObjectData *proxy=proxyCache.GetBestProxyObject(odata->GetTool<const ObjToolProxy>().GetProxySelection(fc),proxyLevel);
      if (proxy)
      {
        Matrix4 mx;
        proxy->GetTool<ObjToolProxy>().GetProxyTransform(fc,mx);
        for (int k=0,count=proxy->NPoints();k<count;k++) 
        {
          PosT& pt=proxy->Point(k);
          VecT vc;vc.SetFastTransform(mx,pt);
          if (funct(vc)) 
          {
            if (sel)
            {
              sel->FaceSelect(i);
              for (int z=0,cnt=fc.N();z<cnt;++z) sel->PointSelect(fc.GetPoint(z));
            }
            break;
          }
        }

      }
    }
}





/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OBJEKTIV2DOC_H__6F94A43A_55DE_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
