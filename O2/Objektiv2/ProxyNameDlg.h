#if !defined(AFX_PROXYNAMEDLG_H__89275F17_46A6_4595_B89F_35C9FB6F1CBA__INCLUDED_)
#define AFX_PROXYNAMEDLG_H__89275F17_46A6_4595_B89F_35C9FB6F1CBA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProxyNameDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CProxyNameDlg dialog

class CProxyNameDlg : public CDialog
  {
  // Construction
  CString lastname;
  public:
    CProxyNameDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CProxyNameDlg)
    enum 
      { IDD = IDD_PROXYNAME };
    CComboBox	NameCtrl;
    CString	Name;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CProxyNameDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CProxyNameDlg)
    virtual BOOL OnInitDialog();
    virtual void OnOK();
    afx_msg void OnEditchangeCombo1();
    afx_msg void OnSelchangeCombo1();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  public:
    afx_msg void OnBnClickedButton1();
    CString vProxyId;
    bool vNoId;
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROXYNAMEDLG_H__89275F17_46A6_4595_B89F_35C9FB6F1CBA__INCLUDED_)
