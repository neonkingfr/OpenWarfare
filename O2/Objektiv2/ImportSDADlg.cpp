// ImportSDADlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "ImportSDADlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImportSDADlg dialog


CImportSDADlg::CImportSDADlg(CWnd* pParent /*=NULL*/)
  : CDialog(CImportSDADlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CImportSDADlg)
    bxsize = 0.1f;
    max = 99999;
    min = 0;
    vxtype = 0;
    scale = _T("1000:1");
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CImportSDADlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CImportSDADlg)
  DDX_Control(pDX, IDC_SELECT, wnd_select);
  DDX_Control(pDX, IDC_BXSIZE, wnd_bxsize);
  DDX_Control(pDX, IDC_LIST, list);
  DDX_Text(pDX, IDC_BXSIZE, bxsize);
  DDX_Text(pDX, IDC_MAX, max);
  DDX_Text(pDX, IDC_MIN, min);
  DDX_Radio(pDX, IDC_VXTYPE, vxtype);
  DDX_CBString(pDX, IDC_SCALE, scale);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CImportSDADlg, CDialog)
  //{{AFX_MSG_MAP(CImportSDADlg)
  ON_BN_CLICKED(IDC_VXTYPE, OnVxtype)
    ON_BN_CLICKED(IDC_SELECT, OnSelect)
      ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LIST, OnEndlabeleditList)
        ON_WM_DESTROY()
          ON_BN_CLICKED(IDC_CREATPRIM, OnCreatprim)
            ON_BN_CLICKED(IDC_BXAUTO, OnVxtype)
              ON_BN_CLICKED(IDC_BXTYPE, OnVxtype)
                ON_BN_CLICKED(IDC_OXTYPE, OnVxtype)
                  ON_BN_CLICKED(IDC_DELETEPRIM, OnDeleteprim)
                    //}}AFX_MSG_MAP
                    END_MESSAGE_MAP()
                      
                      /////////////////////////////////////////////////////////////////////////////
                      // CImportSDADlg message handlers
                      
                      static void ScanString(istream &str, char *buff, int size)
                        {  
                        buff[0]=0;size--;
                        int i=str.get();
                        while (i!=EOF && i<32) i=str.get();  
                        while (i>32 && size>0) 
                          {*buff++=(char)i;size--;i=str.get();}
                        *buff=0;
                        while (i>32) i=str.get();    
                        str.putback(i);
                        }

//--------------------------------------------------

BOOL CImportSDADlg::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  
  instr.open(filename,ios::in );
  instr.seekg(0,ios::end);
  fsize=instr.tellg();
  instr.seekg(0);
  if (!instr)
    {
    AfxMessageBox(IDS_LOADUNSUCCESFUL);  PostMessage(WM_COMMAND,IDCANCEL); return FALSE;
    }
  char buff[256];
  bool ok=true;
  ScanString(instr,buff,sizeof(buff));
  delimiter=instr.get();
  if (strcmp(buff,"Field")) ok=false;
  if (ok)
    {
    ScanString(instr,buff,sizeof(buff));
    if (strcmp(buff,"Time")) ok=false;
    }
  if (ok==false)
    {
    AfxMessageBox(IDS_LOADUNSUCCESFUL);  PostMessage(WM_COMMAND,IDCANCEL); return FALSE;
    }
  CString last;
  int idx=0;
  CRect rc;
  list.GetClientRect(rc);
  list.InsertColumn(0,WString(IDS_NAMEDSELECTIONS),LVCFMT_LEFT,rc.right-GetSystemMetrics(SM_CXVSCROLL)-GetSystemMetrics(SM_CXBORDER)-50,0);
  list.InsertColumn(1,"",LVCFMT_LEFT,50,1);
  pllx=new strstream();
  strstream &llx=*pllx;
  while (instr.peek()!='\n' && !instr.eof())
    {
    ScanString(instr,buff,sizeof(buff));
    char *p=strrchr(buff,':');
    if (p==NULL) llx<<' ';
    else llx<<(p+1);
    *p=0;
    if (last!=buff)
      {
      list.InsertItem(idx,buff);
      last=buff;
      idx++;
      }
    llx<<(char)(idx);
    }
  primcntr='A';
  OnDialogRules()	;
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

void CImportSDADlg::OnDialogRules()
  {
  wnd_bxsize.EnableWindow(GetCheckedRadioButton( IDC_VXTYPE, IDC_OXTYPE)==IDC_BXTYPE);
  wnd_select.EnableWindow(GetCheckedRadioButton( IDC_VXTYPE, IDC_OXTYPE)==IDC_OXTYPE);
  }

//--------------------------------------------------

void CImportSDADlg::OnVxtype() 
  {
  OnDialogRules()	;
  }

//--------------------------------------------------

void CImportSDADlg::OnOK() 
  {
  bool emptywarn=true;
  if (UpdateData()==FALSE) return;
  strstream &llx=*pllx;
  float a=1,b=1;
  const char *strx;
  sscanf(scale,"%f",&a);
  strx=strchr(scale,':');
  if (strx) sscanf(strx+1,"%f",&b);
  fscale=a/b;
  if (vxtype==1)
    {
    prim.create_type=IDD_CREATE_PRIM_BOX;	
    prim.sizeX=prim.sizeY=prim.sizeZ=bxsize;
    prim.numX=prim.numY=prim.numZ=1;
    }
  int cnt=list.GetItemCount();
  HMATRIX trns;
  JednotkovaMatice(trns);
  CString selname;
  CString primpresent;
  bool createprims=false;
  for (int i=0;i<cnt;i++)
    {
    selname=list.GetItemText(i,0);
    primpresent=list.GetItemText(i,1);
    int ptsx=-1;
    if (primpresent!="") createprims=true;
    if (vxtype==0 || primpresent!="")
      ptsx=obj->ReservePoints(1);
    if (vxtype!=0)
      prim.CreatePrimitive(obj,trns);	  
    if (ptsx!=-1) obj->PointSelect(ptsx);
    obj->SaveNamedSel(selname);
    obj->ClearSelection();
    }
  if (createprims) CreatePrims(obj);
  char buff[256];
  AnimationPhase anim(obj);
  anim.SetTime(0.5f);
  obj->AddAnimation(anim);
  obj->RedefineAnimation(0);
  ProgressBar<int> pb(fsize);
  for(;;)
    {
    int frame;
    instr.getline(buff,sizeof(buff),delimiter);
    if (instr.eof()) break;
    sscanf(buff,"%d",&frame);
    if (frame<min || frame>max)
      {
      instr.ignore(0x7FFFFFFF,'\n');
      continue;
      }
    pb.ReportState(IDS_SDLOADINGFRAME,frame);
    pb.SetPos(instr.tellg());
    AnimationPhase anim(obj);
    instr.getline(buff,sizeof(buff),delimiter);
    float d=0.0f;sscanf(buff,"%f",&d);
    anim.SetTime(d);
    AnimationPhase *lap;
    if (obj->NAnimations()>0)
      lap=obj->GetAnimation(obj->NAnimations()-1);
    obj->AddAnimation(anim);	
    obj->UseAnimation(0);
    obj->RedefineAnimation(obj->NAnimations()-1);
    obj->UseAnimation(obj->NAnimations()-1);
    llx.clear();
    llx.seekg(0,ios::beg);
    int p=llx.get();
    int idx;
    float x,y,z;
    int last=-1;
    bool last1=false,last2=false;
    bool skip=false;
    while (true)
      {
      idx=llx.get();
      idx--;
      if (idx!=last)
        {
        if (last!=-1)
          {
          int pcnt=obj->NPoints();		  
          for (int i=0;i<pcnt;i++) if (obj->PointSelected(i))
            {
            PosT &ps=obj->Point(i);
            if (skip && lap)
              {
              ps[0]=(*lap)[i][0];
              ps[1]=(*lap)[i][1];
              ps[2]=(*lap)[i][2];
              }
            else
              {
              ps[0]+=x;
              ps[1]+=y;
              ps[2]+=z;
              }
            }
          skip=false;
          }
        if (p==EOF) break;
        selname=list.GetItemText(idx,0);
        obj->UseNamedSel(selname);
        last=idx;
        }
      d=0.0f;
      last2=p=='Z';
      int nx=llx.get();
      last1=nx==EOF;
      instr.getline(buff,sizeof(buff),(last1 && last2)?'\n':delimiter);
      if (buff[0]==0 && emptywarn)
        {AfxMessageBox(IDS_IMPORTSDAEMPTY);emptywarn=false;}
      skip|=(sscanf(buff,"%f",&d)!=1);
      switch (p)
        {
        case 'X': x=(float)d*fscale;break;
        case 'Y': z=(float)d*fscale;break;
        case 'Z': y=(float)d*fscale;break;
        }
      p=nx;
      }
    }
  CDialog::OnOK();
  }

//--------------------------------------------------

void CImportSDADlg::OnSelect() 
  {
  CMenu mnu;

#ifdef PUBLIC
  mnu.LoadMenu(IDR_MAINFRAME_PUBLIC);
#else
  mnu.LoadMenu(IDR_MAINFRAME);
#endif

  CMenu *sub;
  int i;
  for (i=0;i<mnu.GetMenuItemCount();i++) 
    {
    sub=mnu.GetSubMenu(i); if (sub!=NULL && sub->GetMenuState(ID_CREATE_PLANE,MF_ENABLED)!=0xFFFFFFFF) break;
    }
  sub->EnableMenuItem(ID_CREATE_FACE,MF_GRAYED);
  sub->EnableMenuItem(ID_CREATE_PROXY,MF_GRAYED);
  sub->EnableMenuItem(ID_FACES_EXTRUDE,MF_GRAYED);
  sub->EnableMenuItem(ID_CREATE_SHAPE,MF_GRAYED);
  sub->CheckMenuItem(prim.create_type,MF_CHECKED);
  CPoint pt;
  GetCursorPos(&pt);
  int id=TrackPopupMenu(*sub,TPM_LEFTALIGN|TPM_NONOTIFY|TPM_RETURNCMD|TPM_LEFTBUTTON,pt.x,pt.y,0,*this,NULL);
  if (id!=0)
    {
    prim.create_type=id;
    prim.DoModal();	
    }
  }

//--------------------------------------------------

void CImportSDADlg::OnEndlabeleditList(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
  // TODO: Add your control notification handler code here
  //	list.SetItemText(pDispInfo->item.iItem,0,pDispInfo->item.pszText);
  *pResult = 1;
  }

//--------------------------------------------------

void CImportSDADlg::OnDestroy() 
  {
  CDialog::OnDestroy();
  instr.close();
  delete pllx;
  
  }

//--------------------------------------------------

void CImportSDADlg::OnCreatprim() 
  {
  int p=list.GetItemCount();
  char buff[10];
  char end=primcntr;
  bool endrule;
  do
    {
    endrule=true;
    for (int i=0;i<p;i++) 
      {
      list.GetItemText(i,1,buff,10);
      if (strchr(buff,primcntr)) 
        {
        primcntr++;
        if (primcntr>'Z') primcntr='A';
        if (primcntr==end) return;
        }
      }
    }
  while (!endrule);
  POSITION pos=list.GetFirstSelectedItemPosition();
  int cnt=0;
  while (pos)
    {
    int i=list.GetNextSelectedItem(pos);
    list.GetItemText(i,1,buff,10);
    for (int j=9;j>=1;j--) buff[j]=buff[j-1];
    buff[9]=0;
    buff[0]=primcntr;
    list.SetItemText(i,1,buff);
    cnt++;
    }
  if (cnt!=3 && cnt!=4) 
    {
    AfxMessageBox(IDS_SDAPRIMCREATERROR,MB_OK|MB_ICONEXCLAMATION);
    OnDeleteprim();
    }
  }

//--------------------------------------------------

void CImportSDADlg::OnDeleteprim() 
  {
  POSITION pos=list.GetFirstSelectedItemPosition();
  if (pos)
    {
    int i=list.GetNextSelectedItem(pos);
    char buff[10];
    char z;
    list.GetItemText(i,1,buff,10);
    if (buff[0])
      {
      z=buff[0];
      int p=list.GetItemCount();	
      for (int i=0;i<p;i++) 
        {
        list.GetItemText(i,1,buff,10);
        char *c=strchr(buff,z);
        if (c) 
          {
          strcpy(c,c+1);
          list.SetItemText(i,1,buff);
          }
        }
      }
    }
  
  }

//--------------------------------------------------

void CImportSDADlg::CreatePrims(ObjectData *obj)
  {
  int vxbuf[5];
  int p=list.GetItemCount();
  for (char a='A';a<='Z';a++)
    {
    char buff[10];
    int pos=0;
    memset(vxbuf,0xFF,sizeof(vxbuf));
    for (int i=0;i<p;i++)
      {
      list.GetItemText(i,1,buff,sizeof(buff));
      if (strchr(buff,a)!=NULL)
        {
        CString selname=list.GetItemText(i,0);
        Selection *sel=obj->GetNamedSel(selname);
        int j;
        for (j=0;j<obj->NPoints();j++) if (sel->PointSelected(j)==true) break;
        if (j==obj->NPoints()) goto error1;
        vxbuf[pos]=j;		
        pos++;
        if (pos==4) break;
        }
      }
    if (pos>2)
      {
      FaceT fc(obj,obj->NewFace());
      fc.SetPoint(0,vxbuf[0]);
      fc.SetPoint(1,vxbuf[1]);
      fc.SetPoint(2,vxbuf[2]);
      fc.SetN(3);
      if (pos==4)
        {
          {
          FaceT fc(obj,obj->NewFace());
          fc.SetPoint(0,vxbuf[1]);
          fc.SetPoint(1,vxbuf[0]);
          fc.SetPoint(2,vxbuf[3]);
          fc.SetN(3);
          }
          {
          FaceT fc(obj,obj->NewFace());
          fc.SetPoint(0,vxbuf[3]);
          fc.SetPoint(1,vxbuf[0]);
          fc.SetPoint(2,vxbuf[2]);
          fc.SetN(3);
          }
          {
          FaceT fc(obj,obj->NewFace());
          fc.SetPoint(0,vxbuf[2]);
          fc.SetPoint(1,vxbuf[1]);
          fc.SetPoint(2,vxbuf[3]);
          fc.SetN(3);
          }
        }
      }
    error1:;
    }
  }

//--------------------------------------------------

