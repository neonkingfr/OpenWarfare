// acroRecordPlayback.h: interface for the CMacroRecordPlayback class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MACRORECORDPLAYBACK_H__EA354D4A_423D_491E_9479_DA63F0C2BE46__INCLUDED_)
#define AFX_MACRORECORDPLAYBACK_H__EA354D4A_423D_491E_9479_DA63F0C2BE46__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class ParamClass;

enum EMacroRecordType {mrKeyboard, mrMouse};

  struct SMacroRecord
    {
    EMacroRecordType type;   
    RStringB path;      //path to window which mouse is relative to or keyboard target
    RStringB topname;   //name of top window
    RStringB topclass;  //class of top window
    bool ismainfrm;     //true if top window is main app frame
    bool hasfocus;      //true this window has focus
    bool hwndchange;      //true if top window is currently active
    bool capture;       //true if current window currently owns mouse capture
    int controlID;      //ID of current control
    CSize size;         //size of current window;    
    POINT scrpt;         //window identified by position
    union
      {
      struct
        {
        WPARAM vKey;  //virtual key id
        LPARAM flags; //additional flags;
        }kbinfo;
      struct
        {
        POINT pt;     //mouse position when mrMouse;
        WPARAM buttons; //mouse buttons info
        UINT message; //mouse message;        
        }msinfo;
      };

    void SaveRecord(ParamClass *pclass);
    bool LoadRecord(ParamClass *pclass);

    };

TypeIsMovable(SMacroRecord);


class CMacroRecordPlayback  
{
  
  CWnd _debugWnd;
  HHOOK _curHook;
  HHOOK _idleHook;
  AutoArray<SMacroRecord> record;
  bool _playback;
  int _curindex;
  CPoint _mouseSave;
  CPoint _lastMsClick;
  bool _waitRelease;
  DWORD _waitTime;

  DWORD _lastPressTime;
  UINT _lastMsgCode;
  WPARAM _lastVKey;
  HWND _lastHwnd;

  WPARAM _previousFlags;
protected:  
	void PlaybackKeyboard(const SMacroRecord &rcrd);
	bool PlaybackMouse(const SMacroRecord &rcrd);
	void DebugRecord(const SMacroRecord &rcrd, int index);
	void AddKeyboardRecord(const MSG &msg);
	void AddMouseRecord(const MSG &msg);
    static void CreateWindowIdentification(HWND hWnd, SMacroRecord& rcrd);
    static HWND FindWindowByIdentification(const SMacroRecord& rcrd);
public:
	void ResetInputDevices(int macropos=-1);
	DWORD PlayOnRecordStep(int nCode);
	void PlaybackRecord();
	void RecordMacro();
    void StopRecord();
	CMacroRecordPlayback();
	virtual ~CMacroRecordPlayback();


    LRESULT OnGetMsgEvent(int nCode, WPARAM wParam, const MSG *msg);

    bool IsRecording() {return _curHook!=NULL;}
    bool IsMacroRecorder() {return !IsRecording() && record.Size()!=0;}
    bool IsPlaybacking() {return _playback;}
    bool WasFinished() {return _curindex>=record.Size();}


	static bool CheckActiveWindow();

  bool SaveMacro(const char *filename);
  bool LoadMacro(const char *filename);



};

#endif // !defined(AFX_MACRORECORDPLAYBACK_H__EA354D4A_423D_491E_9479_DA63F0C2BE46__INCLUDED_)
