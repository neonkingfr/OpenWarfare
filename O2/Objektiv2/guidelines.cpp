// GuideLines.cpp: implementation of the CGuideLines class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Objektiv2.h"
#include "GuideLines.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

int CGuideLines::GuideCursor(CPoint &pt, int *linev, int *lineh)
  { 
  int hline,hlen=MAXINT;
  int vline,vlen=MAXINT;
  int res=0;
  for (int i=0;i<MAXGUIDELINES;i++)
    {
    int l=abs(horz[i]-pt.y);
    if (l<hlen) 
      {hline=i;hlen=l;}
    l=abs(vert[i]-pt.x);
    if (l<vlen) 
      {vline=i;vlen=l;}
    }
  if (vlen<gDiff) 
    {
    if (linev) *linev=vline;
    res|=CGL_VERT;
    pt.x=vert[vline];
    }
  else if (linev) *linev=-1;
  if (hlen<gDiff)
    {
    if (lineh) *lineh=hline;
    res|=CGL_HORZ;
    pt.y=horz[hline];
    }
  else if (lineh) *lineh=-1;
  return res;
  }

//--------------------------------------------------

void CGuideLines::Draw(CDC &pDC)
  {
  CPen *old=pDC.SelectObject(&color);
  CRect clip;
  pDC.GetClipBox(&clip);
  for (int i=0;i<MAXGUIDELINES;i++)
    {
    if (horz[i])
      {
      pDC.MoveTo(0,horz[i]);
      pDC.LineTo(clip.right,horz[i]);
      }
    if (vert[i])
      {
      pDC.MoveTo(vert[i],0);
      pDC.LineTo(vert[i],clip.bottom);
      }
    }
  pDC.SelectObject(old);
  }

//--------------------------------------------------

void CGuideLines::SetGuideLine(int gline, int lineindex, int pos, RECT *cliprect)
  {
  if (gline & CGL_HORZ)
    {	
    if (cliprect)
      {
      if (pos<cliprect->top) pos=cliprect->top;
      if (pos>cliprect->bottom) pos=cliprect->bottom;
      }
    if (pos<0) pos=0;
    horz[lineindex]=pos;
    }
  if (gline & CGL_VERT)
    {	
    if (cliprect)
      {
      if (pos<cliprect->top) pos=cliprect->top;
      if (pos>cliprect->bottom) pos=cliprect->bottom;
      }
    if (pos<0) pos=0;
    vert[lineindex]=pos;
    }
  }

//--------------------------------------------------

