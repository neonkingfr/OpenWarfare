#include "..\stdafx.h"
extern "C" void __cdecl _penter();

#include <class\debugLog.hpp>
#include <class\array.hpp>
#include <fstream.h>
#include <stdlib.h>
#include "mapFile.hpp"
#include "..\AutoArray.h"


static unsigned long WalkList[2048];
static int pt=0;
static bool zavora=true;

void AddWalk(unsigned long adr)
  {
  WalkList[pt++]=adr;
  if (pt>=2048) pt=0;
  }

//--------------------------------------------------

__declspec( naked ) void __cdecl _penter()
  {
  __asm
    {
    test zavora,1
      jnz end
        mov zavora,1
          push	eax
            mov eax,[esp+4]
              push ebx
                push ecx
                  push edx
                    push esi
                      push edi
                        push eax
                          call AddWalk
                            pop  eax
                              pop  esi
                                pop  esi
                                  pop  edx
                                    pop  ecx
                                      pop  ebx
                                        pop  eax
                                          mov  zavora,0
                                            end:  
    ret
    }
  }

//--------------------------------------------------

void EnableWalk(bool enable)
  {
  zavora=!enable;
  }

//--------------------------------------------------

void ReportWalk()
  {
  MapFile map;
  
  EnableWalk(false);
  map.ParseMapFile();
  int penterAddr=(int)_penter;
  int penterMap=map.PhysicalAddress("__penter");
  int offset=penterMap-penterAddr;
  for (int i=pt+1;i!=pt;i++)
    {
    if (i>2048) i=0;
    unsigned long ww=WalkList[i];
    const char *name=map.MapNameFromPhysical(ww+offset);	
    LogF("%s",name);
    }
  }

//--------------------------------------------------

