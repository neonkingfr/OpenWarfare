#ifdef _MSC_VER
#pragma once
#endif

#ifndef _MAPFILE_HPP
#define _MAPFILE_HPP

struct MapInfo
  {
  char name[256];
  int physAddress; // symbol value
  int logAddress; // logical address
  };

//--------------------------------------------------

typedef int MapInfo::*MapAddressId;

TypeIsSimple(MapInfo);

class MapFile
  {
  char _name[256];
  AutoArray<MapInfo> _map;
  
  
  public:
    void ParseMapFile();
    const char *GetName() const 
      {return _name;}
    const char *MapName( int address, MapAddressId id, int *lower=NULL );
    const char *MapNameFromPhysical( int fAddress, int *lower=NULL )
      {
      return MapName(fAddress,&MapInfo::physAddress,lower);
      }
    const char *MapNameFromLogical( int lAddress, int *lower=NULL )
      {
      return MapName(lAddress,&MapInfo::logAddress,lower);
      }
    
    int Address( const char *name, MapAddressId id ) const ;
    int MinAddress( MapAddressId id ) const ;
    int MaxAddress( MapAddressId id ) const ;
    
    int PhysicalAddress( const char *name ) const 
      {return Address(name,&MapInfo::physAddress);}
    int LogicalAddress( const char *name ) const 
      {return Address(name,&MapInfo::logAddress);}
    
    int MinPhysicalAddress() const 
      {return MinAddress(&MapInfo::physAddress);}
    int MaxPhysicalAddress() const 
      {return MaxAddress(&MapInfo::physAddress);}
    int MinLogicalAddress() const 
      {return MinAddress(&MapInfo::logAddress);}
    int MaxLogicalAddress() const 
      {return MaxAddress(&MapInfo::logAddress);}
    
    bool Empty() const 
      {return _map.Size()<=0;}
  };

//--------------------------------------------------

#endif
