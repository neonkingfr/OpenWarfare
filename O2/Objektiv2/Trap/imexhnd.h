#ifndef __IMEXHND_H__
#define __IMEXHND_H__
//-----------------------------------------------------------------------------
//
// FILE: IMEXHND.H
//
//
// (c) Interactive Magic (1997)
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------
// Version tracking information
//-----------------------------------------------------------------------

#define USE_IMAGE 0
#if USE_IMAGE
#include <imagehlp.h>
#endif

#include <windows.h>

class DebugExceptionTrap
  {
  TCHAR m_szLogFileName[MAX_PATH];
  LPTOP_LEVEL_EXCEPTION_FILTER m_previousFilter;
  HANDLE m_hReportFile;
  bool m_header;
  void *m_stackBottom;
  
  public:
    
    DebugExceptionTrap();
    ~DebugExceptionTrap();
    
    void SetLogFileName( PTSTR pszLogFileName );
    static void LogF( const char *text, bool stack=true );
    
    void LogFSP( void *eip, DWORD *esp, const char *text, bool stack=true );
    void SetStackBottom( void *stackBottom )
      {m_stackBottom=stackBottom;}
    
    void ReportContext( const char *text, CONTEXT *context );
    
  private:
    
    void OpenLogFile();
    void CloseLogFile();
    void PrintHeader();
    
    // entry point where control comes on an unhandled exception
    static LONG WINAPI ExceptionCallback( PEXCEPTION_POINTERS pExceptionInfo );
    LONG UnhandledExceptionFilter( PEXCEPTION_POINTERS pExceptionInfo );
    
    // where report info is extracted and generated	
    void PrintBanner();
    void PrintContext( void *ip, CONTEXT *ctx );
    void GenerateExceptionReport( PEXCEPTION_POINTERS pExceptionInfo );
    
    // Helper functions
    static LPTSTR GetExceptionString( DWORD dwCode );
    static BOOL GetLogicalAddress( 	PVOID addr, PTSTR szModule, DWORD len,
    DWORD& section, DWORD& offset );
    
    void IntelStackWalk
      (
        DWORD eip, DWORD *pStackTop, DWORD *pStackBot
          );
    void IntelStackWalk( PCONTEXT pContext );
    void IntelPCPrint( class MapFile &map, int pc );
    #if USE_IMAGE
    static void ImagehlpStackWalk( PCONTEXT pContext );
    #endif
    int CCALL _tprintf(const TCHAR * format, ...);
    
    #if USE_IMAGE
    static BOOL InitImagehlpFunctions( void );
    #endif
    
    // Variables used by the class
    #if USE_IMAGE
    // Make typedefs for some IMAGEHLP.DLL functions so that we can use them
    // with GetProcAddress
    typedef BOOL (__stdcall * SYMINITIALIZEPROC)( HANDLE, LPSTR, BOOL );
    typedef BOOL (__stdcall *SYMCLEANUPPROC)( HANDLE );
    
    typedef BOOL (__stdcall * STACKWALKPROC)
      ( DWORD, HANDLE, HANDLE, LPSTACKFRAME, LPVOID,
    PREAD_PROCESS_MEMORY_ROUTINE,PFUNCTION_TABLE_ACCESS_ROUTINE,
    PGET_MODULE_BASE_ROUTINE, PTRANSLATE_ADDRESS_ROUTINE );
    
    typedef LPVOID (__stdcall *SYMFUNCTIONTABLEACCESSPROC)( HANDLE, DWORD );
    
    typedef DWORD (__stdcall *SYMGETMODULEBASEPROC)( HANDLE, DWORD );
    
    typedef BOOL (__stdcall *SYMGETSYMFROMADDRPROC)
      ( HANDLE, DWORD, PDWORD, PIMAGEHLP_SYMBOL );
    
    static SYMINITIALIZEPROC _SymInitialize;
    static SYMCLEANUPPROC _SymCleanup;
    static STACKWALKPROC _StackWalk;
    static SYMFUNCTIONTABLEACCESSPROC _SymFunctionTableAccess;
    static SYMGETMODULEBASEPROC _SymGetModuleBase;
    static SYMGETSYMFROMADDRPROC _SymGetSymFromAddr;
    
    #endif
    
  };

//--------------------------------------------------

extern DebugExceptionTrap GDebugExceptionTrap;	//  global instance of class

#endif
