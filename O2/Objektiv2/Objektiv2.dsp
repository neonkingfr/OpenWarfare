# Microsoft Developer Studio Project File - Name="Objektiv2" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Objektiv2 - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Objektiv2.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Objektiv2.mak" CFG="Objektiv2 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Objektiv2 - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Objektiv2 - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "Objektiv2 - Win32 Light" (based on "Win32 (x86) Application")
!MESSAGE "Objektiv2 - Win32 LightRelease" (based on "Win32 (x86) Application")
!MESSAGE "Objektiv2 - Win32 Alpha Release" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Projects/Objektiv2", LIDAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Objektiv2 - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GR /GX /Zi /O2 /I "../g3d" /D "NDEBUG" /D "FULLVER" /D "WIN32" /D "_WINDOWS" /D "MFC_NEW" /D "NONSTANDARD_FOR_SCOPE" /Fr /Yu"StdAfx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x405 /d "NDEBUG" /d "FULLVER"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 ../g3d/g3dpp.lib, ../g3d/g3d.lib /nologo /subsystem:windows /map /debug /machine:I386 /nodefaultlib:"libc.lib" /nodefaultlib:"libcd.lib" /fixed:no
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /YX /FD /c
# ADD CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "../g3d" /D "_DEBUG" /D "_AFXDLL" /D "FULLVER" /D "WIN32" /D "_WINDOWS" /D "MFC_NEW" /D "NONSTANDARD_FOR_SCOPE" /FR /Yu"StdAfx.h" /FD /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x405 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x405 /d "_DEBUG" /d "_AFXDLL" /d "FULLVER"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /map /debug /machine:I386 /pdbtype:sept

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Light"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Objektiv2___Win32_Light"
# PROP BASE Intermediate_Dir "Objektiv2___Win32_Light"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Light"
# PROP Intermediate_Dir "Light"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GR /GX /Zi /O2 /I "../g3d" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "FULLVER" /Fr /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /G5 /MT /W3 /GR /GX /ZI /Od /Op /I "../g3d" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "MFC_NEW" /D "NONSTANDARD_FOR_SCOPE" /Yu"StdAfx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /map /debug /machine:I386 /nodefaultlib:"libc.lib"
# SUBTRACT BASE LINK32 /incremental:yes /nodefaultlib
# ADD LINK32 ..\g3d\g3d.lib ..\g3d\g3dpp.lib keylib32.lib /nologo /subsystem:windows /incremental:yes /map /debug /machine:I386 /nodefaultlib:"libc.lib" /nodefaultlib:"libcd.lib" /out:"Light/Objektiv2Light.exe"
# SUBTRACT LINK32 /nodefaultlib

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 LightRelease"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Objektiv2___Win32_LightRelease"
# PROP BASE Intermediate_Dir "Objektiv2___Win32_LightRelease"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "LightRelease"
# PROP Intermediate_Dir "LightRelease"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /G5 /MT /W3 /GR /GX /Zi /Od /Op /I "../g3d" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /G5 /MT /W3 /GR /GX /Zd /O2 /Op /I "../g3d" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "MFC_NEW" /D "NONSTANDARD_FOR_SCOPE" /Yu"StdAfx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 ..\g3d\g3d.lib ..\g3d\g3dpp.lib keylib32.lib /nologo /subsystem:windows /incremental:yes /map /debug /machine:I386 /nodefaultlib:"libc.lib" /nodefaultlib:"libcd.lib" /out:"Light/Objektiv2Light.exe"
# SUBTRACT BASE LINK32 /nodefaultlib
# ADD LINK32 ..\g3d\g3d.lib ..\g3d\g3dpp.lib /nologo /subsystem:windows /map /debug /machine:I386 /nodefaultlib:"libc.lib" /nodefaultlib:"libcd.lib" /out:"LightRelease/Objektiv2Light.exe"
# SUBTRACT LINK32 /incremental:yes /nodefaultlib

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Alpha Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Objektiv2___Win32_Alpha_Release"
# PROP BASE Intermediate_Dir "Objektiv2___Win32_Alpha_Release"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "AlphaRelease"
# PROP Intermediate_Dir "AlphaRelease"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GR /GX /Zi /O2 /I "../g3d" /D "NDEBUG" /D "FULLVER" /D "WIN32" /D "_WINDOWS" /D "MFC_NEW" /D "NONSTANDARD_FOR_SCOPE" /Fr /Yu"StdAfx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GR /GX /Zi /O2 /I "../g3d" /D "_DEBUG" /D "FULLVER" /D "WIN32" /D "_WINDOWS" /D "MFC_NEW" /D "NONSTANDARD_FOR_SCOPE" /Fr /Yu"StdAfx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG" /d "FULLVER"
# ADD RSC /l 0x405 /d "NDEBUG" /d "FULLVER"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /map /debug /machine:I386 /nodefaultlib:"libc.lib" /nodefaultlib:"libcd.lib" /fixed:no
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 ..\g3d\g3dpp.lib ..\g3d\g3d.lib /nologo /subsystem:windows /map /debug /machine:I386 /nodefaultlib:"libc.lib" /nodefaultlib:"libcd.lib" /out:"AlphaRelease/Objektiv2Alpha.exe" /fixed:no
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "Objektiv2 - Win32 Release"
# Name "Objektiv2 - Win32 Debug"
# Name "Objektiv2 - Win32 Light"
# Name "Objektiv2 - Win32 LightRelease"
# Name "Objektiv2 - Win32 Alpha Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\3DC.cpp
# End Source File
# Begin Source File

SOURCE=.\3DC2.cpp
# End Source File
# Begin Source File

SOURCE=.\3dsExport.cpp
# End Source File
# Begin Source File

SOURCE=.\3dsExport.h
# End Source File
# Begin Source File

SOURCE=..\g3d\3ds_trc\3dsFile2.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\3dsMasterSaver.cpp
# End Source File
# Begin Source File

SOURCE=.\AllRenameDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AnimListBar.cpp
# End Source File
# Begin Source File

SOURCE=.\ASFHierarchy.cpp
# End Source File
# Begin Source File

SOURCE=.\ASFImportDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BevelDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BIANMImport.cpp
# End Source File
# Begin Source File

SOURCE=.\BIANMImportDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\BVHImport.cpp
# End Source File
# Begin Source File

SOURCE=.\BVHImportDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Calculator\CalcInitDataDlg.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\Calculator\CalcModifyDlg.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\Calculator.cpp

!IF  "$(CFG)" == "Objektiv2 - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Debug"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Light"

# SUBTRACT BASE CPP /YX /Yc /Yu
# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 LightRelease"

# SUBTRACT BASE CPP /YX /Yc /Yu
# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Alpha Release"

# SUBTRACT BASE CPP /YX /Yc /Yu
# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Calculator\CalculatorDlg.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\ColorizeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorSlider.cpp
# End Source File
# Begin Source File

SOURCE=.\ComInt.cpp
# End Source File
# Begin Source File

SOURCE=.\Config.cpp
# End Source File
# Begin Source File

SOURCE=.\Console.cpp
# End Source File
# Begin Source File

SOURCE=.\CopyWeightsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CreatePrimDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgNormalDirection.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgTexelMeasure.cpp
# End Source File
# Begin Source File

SOURCE=.\Edges.cpp
# End Source File
# Begin Source File

SOURCE=.\edgesplit.cpp

!IF  "$(CFG)" == "Objektiv2 - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Light"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 LightRelease"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Alpha Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\EditPropDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ExtrudeStepsBar.cpp
# End Source File
# Begin Source File

SOURCE=.\FaceProp.cpp
# End Source File
# Begin Source File

SOURCE=.\FlattenPointsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\geMatrix4.cpp
# End Source File
# Begin Source File

SOURCE=.\GizmoMapDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\GizmoTexWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\GuideLines.cpp
# End Source File
# Begin Source File

SOURCE=.\Hierarchy.cpp
# End Source File
# Begin Source File

SOURCE=.\HierarchyDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\HistoryBar.cpp
# End Source File
# Begin Source File

SOURCE=.\HotKeyCfgDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\IBackgrndMap.cpp
# End Source File
# Begin Source File

SOURCE=.\ICarving.cpp
# End Source File
# Begin Source File

SOURCE=.\IChilliSkinner.cpp
# End Source File
# Begin Source File

SOURCE=.\ICreateProxy.cpp
# End Source File
# Begin Source File

SOURCE=.\IDeletePointsTriangulate.cpp
# End Source File
# Begin Source File

SOURCE=.\IExtrude.cpp
# End Source File
# Begin Source File

SOURCE=.\IFlattenPoints.cpp
# End Source File
# Begin Source File

SOURCE=.\Import3DS.cpp
# End Source File
# Begin Source File

SOURCE=.\Import3dsTexConvDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ImportSDADlg.cpp
# End Source File
# Begin Source File

SOURCE=.\InternalView.cpp
# End Source File
# Begin Source File

SOURCE=.\Interpolator.cpp
# End Source File
# Begin Source File

SOURCE=.\ITexMerge.cpp
# End Source File
# Begin Source File

SOURCE=.\IToolAdaptaceAnimace.cpp
# End Source File
# Begin Source File

SOURCE=.\IToolAdaptaceAnimace.h
# End Source File
# Begin Source File

SOURCE=.\IToolNormalAnim.cpp

!IF  "$(CFG)" == "Objektiv2 - Win32 Release"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Debug"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Light"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 LightRelease"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\IUserMapping.cpp
# End Source File
# Begin Source File

SOURCE=.\LexAnl.cpp
# End Source File
# Begin Source File

SOURCE=.\LexTree.cpp
# End Source File
# Begin Source File

SOURCE=.\LicenceAgrDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LinWeightDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\LockAnims.cpp
# End Source File
# Begin Source File

SOURCE=.\LodConfig.cpp
# End Source File
# Begin Source File

SOURCE=.\LodList.cpp
# End Source File
# Begin Source File

SOURCE=.\MacroRecordPlayback.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\MapDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MassBar.cpp
# End Source File
# Begin Source File

SOURCE=.\MatchDirection.cpp
# End Source File
# Begin Source File

SOURCE=.\MDAexport.cpp
# End Source File
# Begin Source File

SOURCE=.\MDAexport.h
# End Source File
# Begin Source File

SOURCE=.\MeasureBar.cpp
# End Source File
# Begin Source File

SOURCE=.\MergeNearDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MessageWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\MessageWnd.h
# End Source File
# Begin Source File

SOURCE=.\MultPinCullDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\NamedPropDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\NormalRecalc.cpp
# End Source File
# Begin Source File

SOURCE=.\NormAnimDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ObjectColorizer.cpp
# End Source File
# Begin Source File

SOURCE=.\Objektiv2.cpp
# End Source File
# Begin Source File

SOURCE=.\Objektiv2.rc
# End Source File
# Begin Source File

SOURCE=.\Objektiv2Doc.cpp
# End Source File
# Begin Source File

SOURCE=.\Objektiv2View.cpp
# End Source File
# Begin Source File

SOURCE=.\OBJFileImport.cpp
# End Source File
# Begin Source File

SOURCE=.\OpenQDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PaintSelColorDlg.cpp
# End Source File
# Begin Source File

SOURCE=pal2pacDL.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\PointAttacher.cpp
# End Source File
# Begin Source File

SOURCE=.\PolygonSelection.cpp
# End Source File
# Begin Source File

SOURCE=.\Protection.cpp

!IF  "$(CFG)" == "Objektiv2 - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Light"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 LightRelease"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Alpha Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\ProxyNameDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\PtInfoBar.cpp
# End Source File
# Begin Source File

SOURCE=.\ROCheck.cpp

!IF  "$(CFG)" == "Objektiv2 - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Light"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 LightRelease"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Alpha Release"

# SUBTRACT BASE CPP /YX /Yc /Yu
# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\SelectionGroup.cpp
# End Source File
# Begin Source File

SOURCE=.\SelectionList.cpp
# End Source File
# Begin Source File

SOURCE=.\SelectionNameDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SelectSelectionDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SoftenDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SpaceWarpDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SplashWin.cpp
# End Source File
# Begin Source File

SOURCE=.\SplitHoleDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SPoint.tli
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\TexList.cpp

!IF  "$(CFG)" == "Objektiv2 - Win32 Release"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Debug"

# ADD CPP /Yu

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Light"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 LightRelease"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Alpha Release"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\TexMapList.cpp
# End Source File
# Begin Source File

SOURCE=.\TexMappingDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\TexPreviewData.cpp
# End Source File
# Begin Source File

SOURCE=.\TexPrewWin.cpp
# End Source File
# Begin Source File

SOURCE=.\TextureMapping.cpp
# End Source File
# Begin Source File

SOURCE=.\TimeRangeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Transform2D.cpp
# End Source File
# Begin Source File

SOURCE=.\TransformDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\UnlockSoftDlg.cpp

!IF  "$(CFG)" == "Objektiv2 - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Light"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 LightRelease"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Alpha Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\UnWrapDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\VertexProp.cpp
# End Source File
# Begin Source File

SOURCE=.\VyberLodDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\VyberLodDlg.h
# End Source File
# Begin Source File

SOURCE=.\WeightAutoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Weights.cpp
# End Source File
# Begin Source File

SOURCE=.\WFilePath.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\3DC.h
# End Source File
# Begin Source File

SOURCE=.\3DC2.h
# End Source File
# Begin Source File

SOURCE=..\g3d\3ds_trc\3dsFile2.h
# End Source File
# Begin Source File

SOURCE=.\3dsMasterSaver.h
# End Source File
# Begin Source File

SOURCE=.\AllRenameDlg.h
# End Source File
# Begin Source File

SOURCE=.\AnimListBar.h
# End Source File
# Begin Source File

SOURCE=.\ASFHierarchy.h
# End Source File
# Begin Source File

SOURCE=.\ASFImportDlg.h
# End Source File
# Begin Source File

SOURCE=.\BevelDlg.h
# End Source File
# Begin Source File

SOURCE=.\BIANMImport.h
# End Source File
# Begin Source File

SOURCE=.\BIANMImportDlg.h
# End Source File
# Begin Source File

SOURCE=.\BVHImport.h
# End Source File
# Begin Source File

SOURCE=.\BVHImportDlg.h
# End Source File
# Begin Source File

SOURCE=.\Calculator\CalcInitDataDlg.h
# End Source File
# Begin Source File

SOURCE=.\Calculator\CalcModifyDlg.h
# End Source File
# Begin Source File

SOURCE=.\Calculator.h
# End Source File
# Begin Source File

SOURCE=.\Calculator\CalculatorDlg.h
# End Source File
# Begin Source File

SOURCE=.\ciheader.h
# End Source File
# Begin Source File

SOURCE=.\ColorizeDlg.h
# End Source File
# Begin Source File

SOURCE=.\ColorSlider.h
# End Source File
# Begin Source File

SOURCE=.\ComInt.h
# End Source File
# Begin Source File

SOURCE=.\Config.h
# End Source File
# Begin Source File

SOURCE=.\Console.h
# End Source File
# Begin Source File

SOURCE=.\CopyWeightsDlg.h
# End Source File
# Begin Source File

SOURCE=.\CreatePrimDlg.h
# End Source File
# Begin Source File

SOURCE=.\DlgNormalDirection.h
# End Source File
# Begin Source File

SOURCE=.\DlgTexelMeasure.h
# End Source File
# Begin Source File

SOURCE=.\Edges.h
# End Source File
# Begin Source File

SOURCE=.\EditPropDlg.h
# End Source File
# Begin Source File

SOURCE=.\ExtrudeStepsBar.h
# End Source File
# Begin Source File

SOURCE=.\faceflags.h
# End Source File
# Begin Source File

SOURCE=.\FaceProp.h
# End Source File
# Begin Source File

SOURCE=.\FlattenPointsDlg.h
# End Source File
# Begin Source File

SOURCE=.\geMatrix4.h
# End Source File
# Begin Source File

SOURCE=.\gePlane.h
# End Source File
# Begin Source File

SOURCE=.\geVector4.h
# End Source File
# Begin Source File

SOURCE=.\GizmoMapDlg.h
# End Source File
# Begin Source File

SOURCE=.\GizmoTexWnd.h
# End Source File
# Begin Source File

SOURCE=.\GuideLines.h
# End Source File
# Begin Source File

SOURCE=.\Hierarchy.h
# End Source File
# Begin Source File

SOURCE=.\HierarchyDlg.h
# End Source File
# Begin Source File

SOURCE=.\HistoryBar.h
# End Source File
# Begin Source File

SOURCE=.\HotKeyCfgDlg.h
# End Source File
# Begin Source File

SOURCE=.\IBackgrndMap.h
# End Source File
# Begin Source File

SOURCE=.\ICarving.h
# End Source File
# Begin Source File

SOURCE=.\IChilliSkinner.h
# End Source File
# Begin Source File

SOURCE=.\ICreateProxy.h
# End Source File
# Begin Source File

SOURCE=.\IDeletePointsTriangulate.h
# End Source File
# Begin Source File

SOURCE=.\IExtrude.h
# End Source File
# Begin Source File

SOURCE=.\IFlattenPoints.h
# End Source File
# Begin Source File

SOURCE=.\Import3DS.h
# End Source File
# Begin Source File

SOURCE=.\Import3dsTexConvDlg.h
# End Source File
# Begin Source File

SOURCE=.\ImportSDADlg.h
# End Source File
# Begin Source File

SOURCE=.\InternalView.h
# End Source File
# Begin Source File

SOURCE=.\Interpolator.h
# End Source File
# Begin Source File

SOURCE=.\ITexMerge.h
# End Source File
# Begin Source File

SOURCE=.\IToolNormalAnim.h
# End Source File
# Begin Source File

SOURCE=.\IUserMapping.h
# End Source File
# Begin Source File

SOURCE=.\LexAnl.h
# End Source File
# Begin Source File

SOURCE=.\LexTree.h
# End Source File
# Begin Source File

SOURCE=.\LicenceAgrDlg.h
# End Source File
# Begin Source File

SOURCE=.\LinWeightDlg.h
# End Source File
# Begin Source File

SOURCE=.\LockAnims.h
# End Source File
# Begin Source File

SOURCE=.\LodConfig.h
# End Source File
# Begin Source File

SOURCE=.\LodList.h
# End Source File
# Begin Source File

SOURCE=.\MacroRecordPlayback.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\MapDlg.h
# End Source File
# Begin Source File

SOURCE=.\MassBar.h
# End Source File
# Begin Source File

SOURCE=.\MatchDirection.h
# End Source File
# Begin Source File

SOURCE=.\MeasureBar.h
# End Source File
# Begin Source File

SOURCE=.\MergeNearDlg.h
# End Source File
# Begin Source File

SOURCE=.\MultPinCullDlg.h
# End Source File
# Begin Source File

SOURCE=.\NamedPropDlg.h
# End Source File
# Begin Source File

SOURCE=.\NormalRecalc.h
# End Source File
# Begin Source File

SOURCE=.\NormAnimDlg.h
# End Source File
# Begin Source File

SOURCE=.\ObjectColorizer.h
# End Source File
# Begin Source File

SOURCE=.\Objektiv2.h
# End Source File
# Begin Source File

SOURCE=.\Objektiv2Doc.h
# End Source File
# Begin Source File

SOURCE=.\Objektiv2View.h
# End Source File
# Begin Source File

SOURCE=.\OBJFileImport.h
# End Source File
# Begin Source File

SOURCE=.\OpenQDlg.h
# End Source File
# Begin Source File

SOURCE=.\PaintSelColorDlg.h
# End Source File
# Begin Source File

SOURCE=.\PointAttacher.h
# End Source File
# Begin Source File

SOURCE=.\PolygonSelection.h
# End Source File
# Begin Source File

SOURCE=.\Protection.h
# End Source File
# Begin Source File

SOURCE=.\ProxyNameDlg.h
# End Source File
# Begin Source File

SOURCE=.\PtInfoBar.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\ROCheck.h
# End Source File
# Begin Source File

SOURCE=.\SelectionGroup.h
# End Source File
# Begin Source File

SOURCE=.\SelectionList.h
# End Source File
# Begin Source File

SOURCE=.\SelectionNameDlg.h
# End Source File
# Begin Source File

SOURCE=.\SelectSelectionDlg.h
# End Source File
# Begin Source File

SOURCE=.\SoftenDlg.h
# End Source File
# Begin Source File

SOURCE=.\SpaceWarpDlg.h
# End Source File
# Begin Source File

SOURCE=.\SplashWin.h
# End Source File
# Begin Source File

SOURCE=.\SplitHoleDlg.h
# End Source File
# Begin Source File

SOURCE=.\SPoint.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\TexList.h
# End Source File
# Begin Source File

SOURCE=.\TexMapList.h
# End Source File
# Begin Source File

SOURCE=.\TexMappingDlg.h
# End Source File
# Begin Source File

SOURCE=.\TexPreviewData.h
# End Source File
# Begin Source File

SOURCE=.\TextureMapping.h
# End Source File
# Begin Source File

SOURCE=.\TimeRangeDlg.h
# End Source File
# Begin Source File

SOURCE=.\Transform2D.h
# End Source File
# Begin Source File

SOURCE=.\TransformDlg.h
# End Source File
# Begin Source File

SOURCE=.\UnlockSoftDlg.h

!IF  "$(CFG)" == "Objektiv2 - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Debug"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Light"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 LightRelease"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Alpha Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\UnWrapDlg.h
# End Source File
# Begin Source File

SOURCE=.\VertexProp.h
# End Source File
# Begin Source File

SOURCE=.\WeightAutoDlg.h
# End Source File
# Begin Source File

SOURCE=.\Weights.h
# End Source File
# Begin Source File

SOURCE=.\WFilePath.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Group "Class Wizard"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Objektiv2.clw
# End Source File
# End Group
# Begin Source File

SOURCE=.\res\50gray.bmp
# End Source File
# Begin Source File

SOURCE=.\arrow.cur
# End Source File
# Begin Source File

SOURCE=.\arrowcop.cur
# End Source File
# Begin Source File

SOURCE=.\res\bin3ds.bin
# End Source File
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\box.bmp
# End Source File
# Begin Source File

SOURCE=.\res\browse.ico
# End Source File
# Begin Source File

SOURCE=.\res\circle.bmp
# End Source File
# Begin Source File

SOURCE=.\res\control1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cur00001.cur
# End Source File
# Begin Source File

SOURCE=.\res\cur00002.cur
# End Source File
# Begin Source File

SOURCE=.\res\cur00003.cur
# End Source File
# Begin Source File

SOURCE=.\res\cursor1.cur
# End Source File
# Begin Source File

SOURCE=.\res\cursor2.cur
# End Source File
# Begin Source File

SOURCE=.\res\cylinder.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cylmap.cur
# End Source File
# Begin Source File

SOURCE=.\res\editbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\editbar2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\frombgr.ico
# End Source File
# Begin Source File

SOURCE=.\res\fromlist.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\idi_move.cur
# End Source File
# Begin Source File

SOURCE=.\res\idi_objr.cur
# End Source File
# Begin Source File

SOURCE=.\res\licence.rtf
# End Source File
# Begin Source File

SOURCE=.\res\lockedic.ico
# End Source File
# Begin Source File

SOURCE=.\res\logonorm.bmp
# End Source File
# Begin Source File

SOURCE=.\res\logosave.bmp
# End Source File
# Begin Source File

SOURCE=.\res\mainfram.bmp
# End Source File
# Begin Source File

SOURCE=.\res\menuimg.bmp
# End Source File
# Begin Source File

SOURCE=.\res\o2.ico
# End Source File
# Begin Source File

SOURCE=.\res\Objektiv2.ico
# End Source File
# Begin Source File

SOURCE=.\res\Objektiv2.rc2
# End Source File
# Begin Source File

SOURCE=.\res\Objektiv2Doc.ico
# End Source File
# Begin Source File

SOURCE=.\res\Oxysplash.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Oxysplash_light.bmp
# End Source File
# Begin Source File

SOURCE=.\res\paintsel.cur
# End Source File
# Begin Source File

SOURCE=.\res\plane.bmp
# End Source File
# Begin Source File

SOURCE=.\res\pointer.cur
# End Source File
# Begin Source File

SOURCE=.\optima\Rotate.cur
# End Source File
# Begin Source File

SOURCE=.\optima\Scale.cur
# End Source File
# Begin Source File

SOURCE=.\res\selfacla.cur
# End Source File
# Begin Source File

SOURCE=.\res\selfacre.cur
# End Source File
# Begin Source File

SOURCE=.\res\selobjla.cur
# End Source File
# Begin Source File

SOURCE=.\res\selobjre.cur
# End Source File
# Begin Source File

SOURCE=.\res\selpttla.cur
# End Source File
# Begin Source File

SOURCE=.\res\selpttre.cur
# End Source File
# Begin Source File

SOURCE=.\res\spehere.bmp
# End Source File
# Begin Source File

SOURCE=.\res\splash.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbar1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbar2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\unknownt.bmp
# End Source File
# Begin Source File

SOURCE=.\res\zarovka.cur
# End Source File
# End Group
# Begin Group "optima"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\AutoArray.h
# End Source File
# Begin Source File

SOURCE=.\optima\data3d.h
# End Source File
# Begin Source File

SOURCE=.\optima\DdeViewer.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\optima\DdeViewer.hpp
# End Source File
# Begin Source File

SOURCE=.\optima\express.cpp

!IF  "$(CFG)" == "Objektiv2 - Win32 Release"

# ADD CPP /Yu"../stdafx.h"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Light"

# ADD BASE CPP /Yu"../stdafx.h"
# ADD CPP /Yu"../stdafx.h"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 LightRelease"

# ADD BASE CPP /Yu"../stdafx.h"
# ADD CPP /Yu"../stdafx.h"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Alpha Release"

# ADD BASE CPP /Yu"../stdafx.h"
# ADD CPP /Yu"../stdafx.h"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\optima\express.hpp
# End Source File
# Begin Source File

SOURCE=.\optima\Fileutil.c
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\optima\Fileutil.h
# End Source File
# Begin Source File

SOURCE=..\g3d\g3dmodul.h
# End Source File
# Begin Source File

SOURCE=.\optima\global.hpp
# End Source File
# Begin Source File

SOURCE=.\optima\i3ds.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\optima\i3ds.h
# End Source File
# Begin Source File

SOURCE=.\optima\Macros.h
# End Source File
# Begin Source File

SOURCE=.\optima\math3d.hpp
# End Source File
# Begin Source File

SOURCE=.\optima\math3dP.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\optima\math3dP.hpp
# End Source File
# Begin Source File

SOURCE=.\optima\mathDefs.hpp
# End Source File
# Begin Source File

SOURCE=.\optima\mathOpt.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\optima\mathOpt.hpp
# End Source File
# Begin Source File

SOURCE=.\optima\misc.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\optima\misc.h
# End Source File
# Begin Source File

SOURCE=.\optima\objAnim.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\optima\ObjAsc.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\objectdata.h
# End Source File
# Begin Source File

SOURCE=.\optima\ObjLOD.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\optima\ObjLOD.hpp
# End Source File
# Begin Source File

SOURCE=.\optima\objMass.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\optima\Objobje.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\optima\objobje.hpp
# End Source File
# Begin Source File

SOURCE=.\optima\ObjSelect.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\optima\objTopo.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\optima\optima2mfc.h
# End Source File
# Begin Source File

SOURCE=.\pal2pac.hpp
# End Source File
# Begin Source File

SOURCE=.\texprewwin.h
# End Source File
# Begin Source File

SOURCE=.\optima\types3d.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\optima\types3d.hpp
# End Source File
# Begin Source File

SOURCE=.\optima\wpch.hpp
# End Source File
# End Group
# Begin Group "Trap"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Trap\debugTrap.cpp

!IF  "$(CFG)" == "Objektiv2 - Win32 Release"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Debug"

# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Light"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 LightRelease"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Alpha Release"

# PROP BASE Exclude_From_Build 1
# PROP Exclude_From_Build 1

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\Trap\imexhnd.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\Trap\imexhnd.h
# End Source File
# Begin Source File

SOURCE=.\Trap\mapFile.cpp
# ADD CPP /Yu"../stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\Trap\mapFile.hpp
# End Source File
# End Group
# Begin Group "Merged"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\paramFile.cpp
# End Source File
# Begin Source File

SOURCE=.\paramFile.hpp
# End Source File
# Begin Source File

SOURCE=.\qstream.cpp
# End Source File
# Begin Source File

SOURCE=.\QStream.hpp
# End Source File
# End Group
# Begin Group "Es"

# PROP Default_Filter ""
# Begin Group "Strings"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Strings\rString.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=..\..\Es\Strings\rString.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=..\..\Es\Containers\array.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\cachelist.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\checkMem.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\compactBuf.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\debugLog.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\debugNew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\essencepch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\fastAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Common\fltopts.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\hashMap.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\listBidir.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Common\mathND.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\memAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\normalNew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\platform.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\pointers.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Algorithms\qsort.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\typeDefines.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\typeOpts.hpp
# End Source File
# End Group
# Begin Group "Traditional"

# PROP Default_Filter ""
# Begin Group "Headers"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\TraditionalInterface\FaceT.h
# End Source File
# End Group
# Begin Group "Sources"

# PROP Default_Filter "c;cpp"
# Begin Source File

SOURCE=.\TraditionalInterface\FaceT.cpp

!IF  "$(CFG)" == "Objektiv2 - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Debug"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Light"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 LightRelease"

!ELSEIF  "$(CFG)" == "Objektiv2 - Win32 Alpha Release"

# SUBTRACT BASE CPP /YX /Yc /Yu
# SUBTRACT CPP /YX /Yc /Yu

!ENDIF 

# End Source File
# End Group
# End Group
# Begin Source File

SOURCE=S:\Bredy\Objektiv2\BUGS.TXT
# End Source File
# Begin Source File

SOURCE=S:\Bredy\Objektiv2\IDEAS.TXT
# End Source File
# Begin Source File

SOURCE=.\res\krichle.3DS
# End Source File
# Begin Source File

SOURCE=.\Objektiv2.reg
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
