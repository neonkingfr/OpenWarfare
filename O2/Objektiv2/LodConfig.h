#if !defined(AFX_LODCONFIG_H__EC43DC6F_20B4_4B31_BBFE_CAC581F20C81__INCLUDED_)
#define AFX_LODCONFIG_H__EC43DC6F_20B4_4B31_BBFE_CAC581F20C81__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LodConfig.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLodConfig dialog

class CLodConfig : public CDialog
  {
  // Construction
  public:
    bool exitdlg;
    CLodConfig(CWnd* pParent = NULL);   // standard constructor
    float lod;
    
    // Dialog Data
    //{{AFX_DATA(CLodConfig)
    enum 
      { IDD = IDD_LODCONFIG };
    CListBox	List;
    float	res;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CLodConfig)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CLodConfig)
    virtual BOOL OnInitDialog();
    afx_msg void OnSelchangeList();
    virtual void OnOK();
    afx_msg void OnDblclkList();
    afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  public:
    virtual BOOL PreTranslateMessage(MSG* pMsg);
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LODCONFIG_H__EC43DC6F_20B4_4B31_BBFE_CAC581F20C81__INCLUDED_)
