#pragma once


// CDlgSccCheckInOut dialog

class CDlgSccCheckInOut : public CDialog
{
	DECLARE_DYNAMIC(CDlgSccCheckInOut)

public:
	CDlgSccCheckInOut(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgSccCheckInOut();

// Dialog Data
	enum { IDD = IDD_DLGCHECKINOUT };

    bool enableKeepCheck;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CString vComments;
  BOOL vKeepCheck;
  virtual BOOL OnInitDialog();
  BOOL vCheckOther;
};
