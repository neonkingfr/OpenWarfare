// StatusLight.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "StatusLight.h"
#include ".\statuslight.h"


// CStatusLight

IMPLEMENT_DYNAMIC(CStatusLight, CStatic)
CStatusLight::CStatusLight()
{
  _statusCurrent=statusReady;
}

CStatusLight::~CStatusLight()
{
}


BEGIN_MESSAGE_MAP(CStatusLight, CStatic)
  ON_WM_PAINT()
END_MESSAGE_MAP()



// CStatusLight message handlers

void CStatusLight::DrawStatusIcon(CDC *pDC)
{
  if (_statusImages.GetSafeHandle()==NULL)
  {
    _statusImages.LoadBitmap(IDB_STATUSICONS);
  }
  CDC compdc;
  CRect rc;
  GetClientRect(&rc);
  bool nuldc=pDC==NULL;
  if (nuldc) pDC=GetDC();

  int last=pDC->SetStretchBltMode(HALFTONE);
  compdc.CreateCompatibleDC(pDC);
  CBitmap *old=compdc.SelectObject(&_statusImages);
  StretchBlt(*pDC,rc.left,rc.top,rc.right,rc.bottom,
            compdc,32*_statusCurrent,0,32,32,SRCCOPY);

  pDC->SelectObject(old);
  pDC->SetStretchBltMode(last);
  if (nuldc) ReleaseDC(pDC);
}


void CStatusLight::OnPaint()
{
  CPaintDC dc(this); // device context for painting
  // Do not call CStatic::OnPaint() for painting messages
  DrawStatusIcon(&dc);
}
