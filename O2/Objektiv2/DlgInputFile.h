#pragma once
#include "afxwin.h"


// CDlgInputFile dialog

class CDlgInputFile : public CDialog
{
	DECLARE_DYNAMIC(CDlgInputFile)

public:
	CDlgInputFile(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgInputFile();

// Dialog Data
	enum { IDD = IDD_INPUTFILE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    

	DECLARE_MESSAGE_MAP()
public:
  bool save;
  bool browse;
  CEdit wEdit;
  CButton wBrowse;
  CString vFilename;
  CString title;
  CString filter;
  virtual BOOL OnInitDialog();
  afx_msg void OnBnClickedBrowse();

  void GuessFilter(const char *filename);
  void AdjustEditPos(void);
};
