#if !defined(AFX_IMPORT3DSTEXCONVDLG_H__CF0CEB6F_73B7_4550_A5DA_BBA9E880D32A__INCLUDED_)
#define AFX_IMPORT3DSTEXCONVDLG_H__CF0CEB6F_73B7_4550_A5DA_BBA9E880D32A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Import3dsTexConvDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CImport3dsTexConvDlg dialog

#include "..\g3d\3ds_trc\3dsFile2.h"



class CImport3dsTexConvDlg : public CDialog
  {
  // Construction
  struct TexInfo
    {
    enum Texinfostate 
      {inQueue,Done,Error,Warning,Force,InProgress};
    CString matname; //name of material
    CString texname; //name of target texture
    Pathname sourcemap; //source RGB map filename
    Pathname sourcealpha; //source alpha map filename
    Pathname targetmap; //target map filename	
    int idscomment; //string ID of comment
    Texinfostate state;
    unsigned char rd,gd,bd; //
    unsigned char ad;
    unsigned char dmix;
    unsigned char amix;
    };
  
  int texcount; //count of textures;
  TexInfo *texinfo; //list of textures;
  void SetState(int item, TexInfo::Texinfostate state, int comment);
  public:
    ~CImport3dsTexConvDlg();    
    const char * GetTextureName(const char *matname);
    CImport3dsTexConvDlg(CWnd* pParent = NULL);   // standard constructor
    C3dsChunk *mdata; //material chunk - to scan materials;
    Pathname basepath; //path to textures;
    Pathname targetpath; //path to store textures;
    CString prefix;
    // Dialog Data
    //{{AFX_DATA(CImport3dsTexConvDlg)
    enum 
      { IDD = IDD_IMPORT3DSTEXCONV };
    CButton	wBrowse;
    CButton	wForceConv;
    CListCtrl	wToDo;
    //}}AFX_DATA
    bool usematname;
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CImport3dsTexConvDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    void ProcessAll();
    bool DoConvert(int item);
    
    // Generated message map functions
    //{{AFX_MSG(CImport3dsTexConvDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnDestroy();
    virtual void OnOK();
    afx_msg void OnBrowsepath();
    afx_msg void OnForceconv();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMPORT3DSTEXCONVDLG_H__CF0CEB6F_73B7_4550_A5DA_BBA9E880D32A__INCLUDED_)
