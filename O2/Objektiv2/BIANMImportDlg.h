#include "afxwin.h"
#if !defined(AFX_BIANMIMPORTDLG_H__4E78D64A_7BDA_4CB1_827C_07953F61FC31__INCLUDED_)
#define AFX_BIANMIMPORTDLG_H__4E78D64A_7BDA_4CB1_827C_07953F61FC31__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BIANMImportDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBIANMImportDlg dialog

class CBIANMImportDlg : public CDialog
{
// Construction
public:
	CBIANMImportDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBIANMImportDlg)
	enum { IDD = IDD_BIANMIMPORTDLG };
	BOOL	vInvertX;
	BOOL	vInvertY;
	BOOL	vInvertZ;
	float	vMasterScale;
	//}}AFX_DATA
    bool enableEdgeDetect;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBIANMImportDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CBIANMImportDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
  int vAngle;
  CEdit wAngle;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BIANMIMPORTDLG_H__4E78D64A_7BDA_4CB1_827C_07953F61FC31__INCLUDED_)
