// SplitHoleDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "SplitHoleDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSplitHoleDlg dialog


CSplitHoleDlg::CSplitHoleDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CSplitHoleDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CSplitHoleDlg)
    points = 3;
    direction = 0;
    mode = 0;
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CSplitHoleDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CSplitHoleDlg)
  DDX_Text(pDX, IDC_POINTS, points);
  DDV_MinMaxUInt(pDX, points, 3, 100);
  DDX_Radio(pDX, IDC_RADIO1, direction);
  DDX_Radio(pDX, IDC_SPLIT, mode);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CSplitHoleDlg, CDialog)
  //{{AFX_MSG_MAP(CSplitHoleDlg)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN, OnDeltaposSpin)
    //}}AFX_MSG_MAP
    END_MESSAGE_MAP()
      
      /////////////////////////////////////////////////////////////////////////////
      // CSplitHoleDlg message handlers
      
      
      
      void CSplitHoleDlg::OnDeltaposSpin(NMHDR* pNMHDR, LRESULT* pResult) 
        {
        NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
        // TODO: Add your control notification handler code here
        
        int i;
        i=GetDlgItemInt(IDC_POINTS,NULL,FALSE);
        i-=pNMUpDown->iDelta;
        if (i<3) i=3;
        if (i>100) i=100;
        SetDlgItemInt(IDC_POINTS,i,FALSE);
        *pResult = 0;
        }

//--------------------------------------------------

CISplitHole::CISplitHole(istream &str)
  {
  datard(str,points);
  datard(str,direction);
  datard(str,mode);
  }

//--------------------------------------------------

CISplitHole::CISplitHole(int mode, int pt, int dir):
points(pt),mode(mode),direction(dir)
  {
  
  }

//--------------------------------------------------

int CISplitHole::Tag() const 
  {
  return CITAG_SPLITHOLE;
  }

//--------------------------------------------------

void CISplitHole::Save(ostream &str)
  {
  datawr(str,points);
  datawr(str,direction);
  datawr(str,mode);
  }

//--------------------------------------------------

int CISplitHole::Execute(LODObject *lod)
  {
  ObjectData *obj=lod->Active();
  int i;
  int cnt=obj->NFaces();
  for (i=0;i<cnt;i++) if (obj->FaceSelected(i))
    {
    FaceT fc(obj,i);
    if (mode==0) SplitFace(fc,obj);else CreateHole(fc,obj);
    }
  obj->SelectionDeleteFaces();
  return 0;
  }

//--------------------------------------------------

CICommand *CISplitHole_Create(istream& str)
  {
  return new CISplitHole(str);
  }

//--------------------------------------------------

void CISplitHole::SplitFace(FaceT fc, ObjectData *obj)
  {
  int a=direction;
  int b=(a+1);
  if (b>=fc.N()) b-=fc.N();
  int pt1,pt2;
  pt1=obj->NPoints();
  VecT &ps1=*obj->NewPoint();
  PosT &psa=obj->Point(fc.GetPoint(a));
  PosT &psb=obj->Point(fc.GetPoint(b));
  ps1=(psa+psb)/2;
  FaceT fc1(obj,obj->NewFace());
  FaceT fc2(obj,obj->NewFace());
  fc.CopyFaceTo(fc1);
  fc.CopyFaceTo(fc2);  
  fc1.SetPoint(b,pt1);
  fc2.SetPoint(a,pt1);
  fc1.SetUV(b,(fc.GetU(a)+fc.GetU(b))*0.5f,(fc.GetV(a)+fc.GetV(b))*0.5f);
  fc2.SetUV(a,fc1.GetU(a),fc1.GetV(a));
  if (fc.N()==4)
    {
    a=(direction+2)&3;
    b=(a+1)&3;
    pt2=obj->NPoints();
    VecT &ps2=*obj->NewPoint();
    PosT &psa=obj->Point(fc.GetPoint(a));
    PosT &psb=obj->Point(fc.GetPoint(b));
    ps2=(psa+psb)/2;	
    fc2.SetPoint(b,pt2);
    fc1.SetPoint(a,pt2);
    fc1.SetUV(b,(fc.GetU(a)+fc.GetU(b))*0.5f,(fc.GetV(a)+fc.GetV(b))*0.5f);
    fc2.SetUV(a,fc1.GetU(a),fc1.GetV(a));
    }
  }

//--------------------------------------------------

void CISplitHole::GetHoleProp(FaceT &fc, ObjectData *obj, float &radius, LPHVECTOR center)
  {
  int i;
  VecT centr(0,0,0);
  for (i=0;i<fc.N();i++) 
    {
    VecT &p=obj->Point(fc.GetPoint(i));
    centr+=p;
    }
  centr/=fc.N();
  radius=1e10;
  for (i=0;i<fc.N();i++) 
    {
    VecT &p=obj->Point(fc.GetPoint(i));
    float distance=p.Distance(centr);
    if (distance<radius) radius=distance;
    }
  radius*=0.2f;
  CopyVektor(center,mxVector3(centr[0],centr[1],centr[2]));
  }

//--------------------------------------------------

void CISplitHole::CalcTransform(FaceT &fc, ObjectData *obj, HMATRIX mm)
  {
  Vector3 vect=fc.CalculateNormal();;
  HVECTOR cent;
  float radius;
  GetHoleProp(fc,obj,radius,cent);
  HMATRIX mx;
  Zoom(mm,radius,radius,0);
  RotaceY(mx,atan2(vect[XVAL],vect[ZVAL]));
  SoucinMatic(mm,mx,mm);
  float d=(float)sqrt(vect[XVAL]*vect[XVAL]+vect[ZVAL]*vect[ZVAL]);
  RotaceX(mx,atan2(vect[YVAL],d));
  SoucinMatic(mm,mx,mm);
  Translace(mx,cent[XVAL],cent[YVAL],cent[ZVAL]);
  SoucinMatic(mm,mx,mm);
  }

//--------------------------------------------------

void CISplitHole::CreateCircle(HMATRIX mm, ObjectData *obj)
  {
  for (int i=0;i<points;i++)
    {
    HVECTOR v,tv;
    float f=FPI*2*(float)i/(float)points;
    v[XVAL]=(float)cos(f);
    v[YVAL]=(float)sin(f);
    v[ZVAL]=0;
    v[WVAL]=1;
    TransformVector(mm,v,tv);
    PosT &ps=*obj->NewPoint();
    ps[0]=tv[0];
    ps[1]=tv[1];
    ps[2]=tv[2];
    }
  }

//--------------------------------------------------

void CISplitHole::CreateEdgeFace(FaceT &fc, ObjectData *obj, int pt1, int pt2, LPHVECTOR center)
  {
  FaceT fn(obj,obj->NewFace());
  fn.SetPoint(1,pt1);
  fn.SetPoint(2,pt2);
  fn.SetPoint(0,FindNearestPoint(fc,obj,pt1,center));
  fn.SetPoint(3,FindNearestPoint(fc,obj,pt2,center));
  fn.SetFlags(fc.GetFlags());
  if (fn.GetPoint(3)==fn.GetPoint(0)) fn.SetN(3);else fn.SetN(4);
  }

//--------------------------------------------------

int CISplitHole::FindNearestPoint(FaceT &fc, ObjectData *obj, int pt,LPHVECTOR center)
  {
  int i;
  float d=-100.0f;
  int px;
  bool set=true;
  VecT &v1=obj->Point(pt);
  VecT centr(center[0],center[1],center[2]);
  VecT temp1,temp2;
  temp1=v1-centr;
  for (i=0;i<fc.N();i++)
    {
    VecT &v2=obj->Point(fc.GetPoint(i));
    temp2=v2-centr;
    float x=temp2.CosAngle(temp1);
    if (x>d || set) 
      {d=x;set=false;px=fc.GetPoint(i);}
    }
  return px;
  }

//--------------------------------------------------

void CISplitHole::CreateHole(FaceT fc, ObjectData *obj)
  {
  HMATRIX mm;
  CalcTransform(fc,obj,mm);
  int a=obj->NPoints();
  CreateCircle(mm,obj);
  int b=obj->NPoints();
  if (points<=fc.N()) CreateEdgeFacesSpec(fc,obj,a,points,mm[3]);
  else
    for (int i=a;i<b;i++)
      {
      int j=i+1;if (j==b) j=a;
      CreateEdgeFace(fc,obj,i,j,mm[3]);
      }
  }

//--------------------------------------------------

void CISplitHole::CreateEdgeFacesSpec(FaceT &fc, ObjectData *obj, int pt1, int pts,LPHVECTOR center)
  {
  int ptx=FindNearestPoint(fc,obj,pt1,center);
  int i;
  for (i=0;i<fc.N();i++) if (fc.GetPoint(i)==ptx) break;
  for (int j=0;j<pts;j++)
    {
    int k=j+i;if (k>=fc.N()) k-=fc.N();
    int l=j+i+1;if (l>=fc.N()) l-=fc.N();
    int pt2;
    if (j+1==pts) pt2=pt1;else pt2=pt1+j+1;
    FaceT fn(obj,obj->NewFace());
    fn.SetPoint(1,pt1+j);
    fn.SetPoint(2,pt2);
    fn.SetPoint(0,fc.GetPoint(k));
    fn.SetPoint(3,fc.GetPoint(l));
    fn.SetFlags(fc.GetFlags());
    fn.SetN(4);
    }
  }

//--------------------------------------------------

