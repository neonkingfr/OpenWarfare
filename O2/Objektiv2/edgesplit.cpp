#ifdef _TOVIEW_INCLUDED_OBJECTIV2_

#include ".\geVector4.h"
#include ".\gePlane.h"

struct EdgeInfo
  {
  int edgept;
  int face;  
  };

//--------------------------------------------------

bool FindNearestEdges(LPHVECTOR pt, LPHVECTOR dir, ObjectData *obj,LPHVECTOR out, EdgeInfo *nfo=NULL, int maxnfo=10);
bool FindNearestPoints(LPHVECTOR pt, LPHVECTOR dir, ObjectData *obj, LPHVECTOR out, EdgeInfo *nfo=NULL, int maxnfo=NULL);
static bool FindNearestEdges(LPHVECTOR pt, LPHVECTOR dir, ObjectData *obj, LPHVECTOR out, EdgeInfo *nfo, int maxnfo)
  {
  int lst=0;
  float dist=1.0f;
  bool fnd=false;
  geVector4 vdist;
  geLine ln(geVector4(pt),geVector4(dir),true);
  const Selection &hid=*(obj->GetHidden());
  for (int f=0;f<obj->NFaces();f++) if (!hid.FaceSelected(f))
    {
    FaceT fc(obj,f);
    for (int p=0;p<fc.N();p++)
      {
      PosT &pt1=obj->Point(fc.GetPoint(p));
      PosT &pt2=obj->Point(p+1>=fc.N()?fc.GetPoint(0):fc.GetPoint(p+1));
      geLine eg(geVector4(pt1[0],pt1[1],pt1[2]),geVector4(pt2[0],pt2[1],pt2[2]));
      geVector4 dsp;
      float d=ln.Distance(eg,NULL,&dsp);
      float mp=eg.MapPoint(dsp);
      float dist10=dist+dist*.1f;
      if (d<=dist10 && mp>=0.0f && mp<=eg.r.Module2())
        {
        float q=dsp.Distance(vdist);
        if (d<dist-dist*.1f) 
          lst=0;	
        else
          if (q>dist10) 
            goto skip;
        vdist=dsp;
        dist=d;
        if (nfo && lst<maxnfo)
          {
          nfo[lst].edgept=p;
          nfo[lst].face=f;
          lst++;
          }
        fnd=true;
        skip:;
        }
      }
    }
  CopyVektor(out,vdist);  
  return fnd;
  }

//--------------------------------------------------

static bool FindNearestPoints(LPHVECTOR pt, LPHVECTOR dir, ObjectData *obj, LPHVECTOR out, EdgeInfo *nfo, int maxnfo)
  {
  int lst=0;
  float dist=1.0f;
  bool fnd=false;
  geVector4 vdist;
  geLine ln(geVector4(pt),geVector4(dir),true);
  const Selection &hid=*(obj->GetHidden());
  for (int f=0;f<obj->NFaces();f++) if (!hid.FaceSelected(f))
    {
    FaceT fc(obj,f);
    for (int p=0;p<fc.N();p++)
      {
      PosT &pt1=obj->Point(fc.GetPoint(p));
      geVector4 dsp;
      dsp.x=pt1[0];
      dsp.y=pt1[1];
      dsp.z=pt1[2];
      float d=ln.Distance(dsp);
      float dist10=dist+dist*.1f;
      if (d<=dist10)
        {
        float q=dsp.Distance(vdist);
        if (d<dist-dist*.1f) 
          lst=0;	
        else
          if (q>dist10) 
            goto skip;
        vdist=dsp;
        dist=d;
        if (nfo && lst<maxnfo)
          {
          nfo[lst].edgept=p;
          nfo[lst].face=f;
          lst++;
          }
        fnd=true;
        skip:;
        }
      }
    }
  CopyVektor(out,vdist);  
  return fnd;
  }

//--------------------------------------------------

static EdgeInfo lastedges[10]=
  {
    {-1,-1}};

//--------------------------------------------------

static HVECTOR lastmark;
static int CreatePointNearest(ObjectData *obj,LPHVECTOR pos,float maxdist)
  {
  const Selection *hid=obj->GetHidden();
  float d=maxdist;
  int fnd=-1;
  PosT qq;
  qq[0]=pos[0];
  qq[1]=pos[1];
  qq[2]=pos[2];
  qq.flags=0;
  for (int i=0;i<obj->NPoints();i++) if (hid->PointSelected(i)==false)
    {
    PosT &ps=obj->Point(i);
    float df=ps.Distance(qq);
    if (df<d)
      {
      d=df;
      fnd=i;
      }
    }
  if (fnd==-1)
    {
    fnd=obj->ReservePoints(1);
    obj->Point(fnd)=qq;
    }
  return fnd;
  }

//--------------------------------------------------

class CISplitFaceEx: public CICommand
  {
  int face;
  int edge1;
  int edge2;
  HVECTOR pos1;
  HVECTOR pos2;
  float attach;
  public:
    CISplitFaceEx(int fc, int e1, int e2, LPHVECTOR p1, LPHVECTOR p2,float att);
    virtual int Execute(LODObject *lod);
  };

//--------------------------------------------------

CISplitFaceEx::CISplitFaceEx(int fc, int e1, int e2, LPHVECTOR p1, LPHVECTOR p2, float att)
  {
  face=fc;
  edge1=e1;
  edge2=e2;
  CopyVektor(pos1,p1);
  CopyVektor(pos2,p2);
  attach=att;
  recordable=false;
  }

//--------------------------------------------------

static void SplitOptimizeFace(FaceT &fc)
  {
  bool opak=true;
  while (opak)
    {
    opak=false;
    for (int p=0;p<fc.N();p++)
      {
      int q=p+1;
      if (q>=fc.N()) q=0;
      if (fc.GetPoint(p)==fc.GetPoint(q))
        {
        if (q>p)
          for (int j=q;j<fc.N();j++) fc.CopyPointInfo(j-1,j);
        fc.SetN(fc.N()-1);
        opak=true;
        }
      }
    }
  }

//--------------------------------------------------

int CISplitFaceEx::Execute(LODObject *lod)
  {
  bool sel;
  ObjectData *obj=lod->Active();
  int nfc=obj->NFaces();
  int nwp1=CreatePointNearest(obj,pos1,attach);
  int nwp2=CreatePointNearest(obj,pos2,attach);
  PosT &pos1=obj->Point(nwp1);
  PosT &pos2=obj->Point(nwp2);
  sel=obj->FaceSelected(face);
  FaceT fdef(obj,face);
  obj->PointSelect(nwp1,sel);
  obj->PointSelect(nwp2,sel);
  float tu1,tv1,tu2,tv2;
  int e11=edge1;
  int e12=edge1+1>=fdef.N()?0:edge1+1;
  int e21=edge2;
  int e22=edge2+1>=fdef.N()?0:edge2+1;
  PosT &p11=obj->Point(fdef.GetPoint(e11));
  PosT &p12=obj->Point(fdef.GetPoint(e12));
  PosT &p21=obj->Point(fdef.GetPoint(e21));
  PosT &p22=obj->Point(fdef.GetPoint(e22));
  float f1=pos1.Distance(p11)/p12.Distance(p11);
  float f2=pos2.Distance(p21)/p22.Distance(p21);
  tu1=fdef.GetU(e11)*(1-f1)+fdef.GetU(e12)*f1;
  tv1=fdef.GetV(e11)*(1-f1)+fdef.GetV(e12)*f1;
  tu2=fdef.GetU(e21)*(1-f2)+fdef.GetU(e22)*f2;
  tv2=fdef.GetV(e21)*(1-f2)+fdef.GetV(e22)*f2;
  FaceT ff1(obj);
  ff1=fdef;
  ff1.SetPoint(0,fdef.GetPoint(e11));
  ff1.SetPoint(1,nwp1);
  ff1.SetPoint(2,nwp2);
  ff1.SetPoint(3,fdef.GetPoint(e22));
  ff1.SetN(e11==e22?3:4);
  ff1.SetU(0,fdef.GetU(e11));
  ff1.SetV(0,fdef.GetV(e11));
  ff1.SetU(1,tu1);
  ff1.SetV(1,tv1);
  ff1.SetU(2,tu2);
  ff1.SetV(2,tv2);
  ff1.SetU(3,fdef.GetU(e22));
  ff1.SetV(3,fdef.GetV(e22));
  ff1.CopyFaceTo(obj->NewFace());
  obj->FaceSelect(obj->NFaces()-1,sel);
  ff1=fdef;
  ff1.SetPoint(0,fdef.GetPoint(e21));
  ff1.SetPoint(1,nwp2);
  ff1.SetPoint(2,nwp1);
  ff1.SetPoint(3,fdef.GetPoint(e12));
  ff1.SetN(e21==e12?3:4);
  ff1.SetU(0,fdef.GetU(e21));
  ff1.SetV(0,fdef.GetV(e21));
  ff1.SetU(1,tu2);
  ff1.SetV(1,tv2);
  ff1.SetU(2,tu1);
  ff1.SetV(2,tv1);
  ff1.SetU(3,fdef.GetU(e12));
  ff1.SetV(3,fdef.GetV(e12));
  ff1.CopyFaceTo(obj->NewFace());
  obj->FaceSelect(obj->NFaces()-1,sel);
  
  for (int np=0;np<fdef.N();np++)
    if (np!=e11 && np!=e12 && np!=e21 && np!=e22)
      {
      ff1=fdef;
      ff1.CopyPointInfo(0,fdef,np);
      ff1.CopyPointInfo(1,fdef,np+1>=fdef.N()?0:np+1);
      ff1.CopyPointInfo(2,fdef,np-1<0?fdef.N()-1:np-1);
      ff1.SetN(3);
      ff1.CopyFaceTo(obj->NewFace());
      obj->FaceSelect(obj->NFaces()-1,sel);	  
      }
  for (;nfc<obj->NFaces();nfc++)
    {
    FaceT fc(obj,nfc);
    SplitOptimizeFace(fc);
    if (fc.N()<3) 
      {obj->DeleteFace(nfc);nfc--;}
    }
  obj->DeleteFace(face);
  return 0;
  }

//--------------------------------------------------

static bool SplitFaces(LPHVECTOR pt, LPHVECTOR dir, ObjectData *obj, float attachptdist, bool control)
  {
  HVECTOR out;
  static EdgeInfo edges[10];
  memset(edges,0xFF,sizeof(edges));
  if (control)
    {if (FindNearestPoints(pt,dir,obj,out,edges,10)==false) 
      if (FindNearestEdges(pt,dir,obj,out,edges,10)==false) return false;}
  else
    {if (FindNearestEdges(pt,dir,obj,out,edges,10)==false) return false;}
  int ffc=-1;
  int fpt1=-1;
  int fpt2=-1;
  for (int i=0;i<10;i++) if (lastedges[i].face!=-1)
    for (int j=0;j<10;j++) if (edges[j].face!=-1)	  
      {
      if (lastedges[i].face==edges[j].face) 
        {
        ffc=edges[j].face;
        fpt1=lastedges[i].edgept;
        ASSERT(fpt1>=0);
        fpt2=edges[j].edgept;
        ASSERT(fpt2>=0);
        i=10;
        break;
        }
      }
  else break;
  else break;
  if (ffc!=-1 && fpt1!=fpt2)
    {
    comint.Begin(WString("SplitFaces"));
    comint.Run(new CISplitFaceEx(ffc,fpt1,fpt2,lastmark,out,attachptdist));
    lastedges[0].face=-1;
    }
  else
    {
    CopyVektor(lastmark,out);
    memcpy(lastedges,edges,sizeof(edges));
    }
  return true;
  }

//--------------------------------------------------

class CIPointAtEdge:public CICommand
  {
  int face;
  int edge;
  HVECTOR pos;
  public:
    int ptidx;
    CIPointAtEdge(int fc, int e,  LPHVECTOR p, int pt=-1);
    virtual int Execute(LODObject *lod);
  };

//--------------------------------------------------

CIPointAtEdge::CIPointAtEdge(int fc, int e, LPHVECTOR p,int pt)
  {
  face=fc;
  edge=e;
  CopyVektor(pos,p);
  recordable=false;
  ptidx=pt;
  }

//--------------------------------------------------

int CIPointAtEdge::Execute(LODObject *lod)
  {
  ObjectData *obj=lod->Active();
  FaceT fdef(obj,face);
  int nwp1;
  if (ptidx<0 || ptidx>=obj->NPoints())
    {
    ptidx=obj->NPoints();	  
    obj->NewPoint();
    nwp1=ptidx;
    }
  nwp1=ptidx;
  PosT &ps=obj->Point(nwp1);
  for (int xp=0;xp<3;xp++) ps[xp]=pos[xp];
  int e11=edge;
  int e12=e11+1>=fdef.N()?0:e11+1;
  int e13=e12+1>=fdef.N()?0:e12+1;
  int e14=e13+1>=fdef.N()?0:e13+1;
  PosT &p11=obj->Point(fdef.GetPoint(e11));
  PosT &p12=obj->Point(fdef.GetPoint(e12));
  float f1=ps.Distance(p11)/p12.Distance(p11);
  float tu1=fdef.GetU(e11)*(1-f1)+fdef.GetU(e12)*f1;
  float tv1=fdef.GetV(e11)*(1-f1)+fdef.GetV(e12)*f1;
  FaceT ff1(obj);
  if (fdef.N()==3)
    {
    ff1=fdef;
    ff1.SetPoint(0,fdef.GetPoint(e11));
    ff1.SetPoint(1,nwp1);
    ff1.SetPoint(2,fdef.GetPoint(e12));;
    ff1.SetPoint(3,fdef.GetPoint(e13));
    ff1.SetN(4);
    ff1.SetU(0,fdef.GetU(e11));
    ff1.SetV(0,fdef.GetV(e11));
    ff1.SetU(1,tu1);
    ff1.SetV(1,tv1);
    ff1.SetU(2,fdef.GetU(e12));
    ff1.SetV(2,fdef.GetV(e12));
    ff1.SetU(3,fdef.GetU(e13));
    ff1.SetV(3,fdef.GetV(e13));
    ff1.CopyFaceTo(obj->NewFace());
    }
  else
    {
    ff1=fdef;
    ff1.SetPoint(0,fdef.GetPoint(e11));
    ff1.SetPoint(1,nwp1);
    ff1.SetPoint(2,fdef.GetPoint(e13));;
    ff1.SetPoint(3,fdef.GetPoint(e14));;
    ff1.SetN(4);
    ff1.SetU(0,fdef.GetU(e11));
    ff1.SetV(0,fdef.GetV(e11));
    ff1.SetU(1,tu1);
    ff1.SetV(1,tv1);
    ff1.SetU(2,fdef.GetU(e13));
    ff1.SetV(2,fdef.GetV(e13));
    ff1.SetU(3,fdef.GetU(e14));
    ff1.SetV(3,fdef.GetV(e14));
    ff1.CopyFaceTo(obj->NewFace());
    ff1=fdef;
    ff1.SetPoint(0,fdef.GetPoint(e13));
    ff1.SetPoint(1,nwp1);
    ff1.SetPoint(2,fdef.GetPoint(e12));
    ff1.SetN(3);
    ff1.SetU(0,fdef.GetU(e13));
    ff1.SetV(0,fdef.GetV(e13));
    ff1.SetU(1,tu1);
    ff1.SetV(1,tv1);
    ff1.SetU(2,fdef.GetU(e12));
    ff1.SetV(2,fdef.GetV(e12));
    ff1.CopyFaceTo(obj->NewFace());
    }
  obj->DeleteFace(face);	
  return 0;
  }

//--------------------------------------------------

static bool PointAtEdge(LPHVECTOR pt, LPHVECTOR dir, ObjectData *obj)
  {
  HVECTOR out;
  static EdgeInfo edges[10];
  memset(edges,0xFF,sizeof(edges));
  if (FindNearestEdges(pt,dir,obj,out,edges,10)==false) return false;
  comint.Begin(WString("Insert Edge Point"));
  int lpt=-1;
  for (int i=0;i<10;i++) if (edges[i].face!=-1)
    {
    CIPointAtEdge *pp=new CIPointAtEdge(edges[i].face,edges[i].edgept,out,lpt);
    comint.Run(pp);
    lpt=pp->ptidx;
    for (int j=i+1;j<10;j++)
      {
      if (edges[i].face<edges[j].face) edges[j].face--;
      }
    }
  return true;
  }

//--------------------------------------------------

#endif