// ProxyNameDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "ProxyNameDlg.h"
#include ".\proxynamedlg.h"
#include <el/interfaces/Iscc.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProxyNameDlg dialog


CProxyNameDlg::CProxyNameDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CProxyNameDlg::IDD, pParent)
  , vProxyId(_T("")),
  vNoId(false)
{
    //{{AFX_DATA_INIT(CProxyNameDlg)
    Name = _T("");
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CProxyNameDlg::DoDataExchange(CDataExchange* pDX)
  {
    CDialog::DoDataExchange(pDX);
    //{{AFX_DATA_MAP(CProxyNameDlg)
    DDX_Control(pDX, IDC_COMBO1, NameCtrl);
    DDX_CBString(pDX, IDC_COMBO1, Name);
    //}}AFX_DATA_MAP
    DDX_Text(pDX, IDC_PROXYID, vProxyId);
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CProxyNameDlg, CDialog)
  //{{AFX_MSG_MAP(CProxyNameDlg)
  ON_CBN_EDITCHANGE(IDC_COMBO1, OnEditchangeCombo1)
    ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
      //}}AFX_MSG_MAP
      ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
END_MESSAGE_MAP()
        
        /////////////////////////////////////////////////////////////////////////////
        // CProxyNameDlg message handlers
        
        #define CONFIGLOK "Proxy","History"
        
        
        BOOL CProxyNameDlg::OnInitDialog() 
          {
          CDialog::OnInitDialog();
          
          CString s=theApp.GetProfileString(CONFIGLOK,"");
          int p=s.Find('|',0);
          int idx=0;
          while (p!=-1)
            {
            NameCtrl.AddString(s.Mid(idx,p-idx));
            idx=p+1;
            p=s.Find('|',idx);
            }

          if (vNoId)
            GetDlgItem(IDC_PROXYID)->EnableWindow(FALSE);
          
          return TRUE;  // return TRUE unless you set the focus to a control
          // EXCEPTION: OCX Property Pages should return FALSE
          }

//--------------------------------------------------

void CProxyNameDlg::OnOK() 
  {
  UpdateData();
  CString s=theApp.GetProfileString(CONFIGLOK,"");  
  CString nw=Name+"|";  
  CString temp;
  int q=0;
  int cnt=NameCtrl.GetCount();
  for (int i=0;i<cnt;i++)
    {
    NameCtrl.GetLBText(i,temp);
    if (temp.CompareNoCase(Name)) nw+=temp+"|";
    }
  theApp.WriteProfileString(CONFIGLOK,nw);  
  CDialog::OnOK();
  }

//--------------------------------------------------

void CProxyNameDlg::OnEditchangeCombo1() 
  {
  int sel=LOWORD(NameCtrl.GetEditSel());
  CString p;
  CString tmp;
  NameCtrl.GetWindowText(p);
  int len=p.GetLength();
  if (p=="" && NameCtrl.GetDroppedState() && NameCtrl.GetCurSel()!=-1)
    {
    int curs=NameCtrl.GetCurSel();
    NameCtrl.GetLBText(curs,tmp);
    if (tmp==lastname)
      {
      NameCtrl.DeleteString(curs);
      return;
      }
    }
  if (sel==len && len && lastname!=p)
    {
    lastname=p;
    int cnt=NameCtrl.GetCount();
    for (int i=0;i<cnt;i++)
      {
      NameCtrl.GetLBText(i,tmp);
      if (tmp.Left(len).CompareNoCase(p)==0)
        {
        tmp.Delete(0,len);
        p+=tmp; 
        NameCtrl.SetWindowText(p);
        NameCtrl.SetEditSel(len,len+tmp.GetLength());
        NameCtrl.SetTopIndex(i);
        break;
        
        }
      }
    }
  else
    lastname=p;
  
  }

//--------------------------------------------------

void CProxyNameDlg::OnSelchangeCombo1() 
  {
  int curs=NameCtrl.GetCurSel();
  NameCtrl.GetLBText(curs,lastname);  	
  }

//--------------------------------------------------


void CProxyNameDlg::OnBnClickedButton1()
{
  CString name;
  NameCtrl.GetWindowText(name);
  if (name.GetLength())
  {
    if (name[0]=='\\') name=name.Mid(1);
    name=config->GetString(IDS_CFGPATHFORTEX)+name+".p3d";
    if (theApp._scc)
      theApp._scc->GetLatestVersion(name);
  }
  CString filter;
  filter.LoadString(IDS_FILESAVEFILTERS);
  CString title;
  title.LoadString(IDS_SELECTPROXYTITLE);
  CFileDialogEx fdlg(TRUE,0,name,OFN_HIDEREADONLY|OFN_FILEMUSTEXIST,filter);
  fdlg.m_pOFN->lpstrTitle=title;
  if (fdlg.DoModal()==IDOK)
  {
    name=fdlg.GetPathName();
    name=name.Mid(strlen(config->GetString(IDS_CFGPATHFORTEX)));
    if (name.GetLength())
    {
      if (name[0]!='\\') name='\\'+name;
      name=name.Mid(0,name.GetLength()-4);
      NameCtrl.SetWindowText(name);
    }
  }
}
