#if !defined(AFX_NORMALIZESTEPDLG_H__A113B583_DE79_4C9D_BAA3_44F9B876CA76__INCLUDED_)
#define AFX_NORMALIZESTEPDLG_H__A113B583_DE79_4C9D_BAA3_44F9B876CA76__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NormalizeStepDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CNormalizeStepDlg dialog

class CNormalizeStepDlg : public CDialog
{
// Construction
public:
	CNormalizeStepDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNormalizeStepDlg)
	enum { IDD = IDD_NORMALIZESTEP };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNormalizeStepDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNormalizeStepDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NORMALIZESTEPDLG_H__A113B583_DE79_4C9D_BAA3_44F9B876CA76__INCLUDED_)
