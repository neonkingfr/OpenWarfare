#if !defined(AFX_CALCMODIFYDLG_H__9DA93C4E_C069_11D4_B399_00C0DFAE7D0A__INCLUDED_)
#define AFX_CALCMODIFYDLG_H__9DA93C4E_C069_11D4_B399_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CalcModifyDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCalcModifyDlg dialog

struct CCalcInfo
  {
  CString varlist;
  CString initprog;
  CString pointprog;
  CString faceprog;
  bool reltopin;
  bool wrapu;
  bool wrapv;
  float initials['Z'-'A'+1];
  public:
    bool Load(const char *name);
    bool Save(const char *name);
  };

//--------------------------------------------------

class CCalcModifyDlg : public CDialog
  {
  // Construction
  float initials['Z'-'A'+1];
  public:
    static CString GetProgramFileName(const char *name);
    static void DeleteProgram(const char *name);
    CCalcModifyDlg(CWnd* pParent = NULL);   // standard constructor
    CString clcname; //program to load ("" = for empty)
    // Dialog Data
    //{{AFX_DATA(CCalcModifyDlg)
    enum 
      { IDD = IDD_CALCULATOR_MODIFY };
    CString	initProg;
    CString	inputVars;
    CString	name;
    CString	pointProg;
    BOOL	relToPin;
    BOOL	wrapU;
    BOOL	wrapV;
    CString	faceProg;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CCalcModifyDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CCalcModifyDlg)
    virtual BOOL OnInitDialog();
    virtual void OnOK();
    afx_msg void OnSetvars();
    afx_msg void OnHelp();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALCMODIFYDLG_H__9DA93C4E_C069_11D4_B399_00C0DFAE7D0A__INCLUDED_)
