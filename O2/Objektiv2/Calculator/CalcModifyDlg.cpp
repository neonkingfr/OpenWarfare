// CalcModifyDlg.cpp : implementation file
//

#include "../stdafx.h"
#include "../resource.h"
#include "CalcModifyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <fstream>
using namespace std;

#define MAGIC "Objektiv2_program"

bool CCalcInfo::Save(const char *name)
  {
  ofstream f(name);
  if (!f) return false;
  f<<MAGIC<<"\n"<<reltopin<<"\n"<<wrapu<<"\n"<<wrapv<<"\n";
  f<<varlist<<"~\n";
  f<<initprog<<"~\n";
  f<<faceprog<<"~\n";
  f<<pointprog<<"~\n";
  for (int i=0;i<sizeof(initials)/sizeof(initials[0]);i++)
    f<<initials[i]<<" ";
  f<<"\n";
  return true;
  }

//--------------------------------------------------

static void LoadLongString(CString& s, ifstream &f, int terminator)
  {
  char buff[256]; //TEST TEST TEST TEST - upravit na 256
  s="";
  do
    {
    f.get(buff,sizeof(buff),(char)terminator);
    s+=buff;
    }
  while (f.peek()!=terminator);
  f.ignore();
  }

//--------------------------------------------------

bool CCalcInfo::Load(const char *name)
  {
  ifstream f(name,ios::in);
  if (!f) return false;
  CString mag;
  LoadLongString(mag,f,'\n');
  if (mag!=MAGIC) return false;
  int i;
  f>>i;reltopin=i!=0;
  f>>i;wrapu=i!=0;
  f>>i;wrapv=i!=0;
  f.ignore(0x7fffffff,'\n');
  LoadLongString(varlist,f,'~');f.ignore(0x7fffffff,'\n');
  LoadLongString(initprog,f,'~');f.ignore(0x7fffffff,'\n');
  LoadLongString(faceprog,f,'~');f.ignore(0x7fffffff,'\n');
  LoadLongString(pointprog,f,'~');f.ignore(0x7fffffff,'\n');
  for (i=0;i<sizeof(initials)/sizeof(initials[0]);i++)
    f>>initials[i];
  return true;
  }

//--------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
// CCalcModifyDlg dialog


CCalcModifyDlg::CCalcModifyDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CCalcModifyDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CCalcModifyDlg)
    initProg = _T("");
    inputVars = _T("");
    name = _T("");
    pointProg = _T("");
    relToPin = FALSE;
    wrapU = FALSE;
    wrapV = FALSE;
    faceProg = _T("");
    //}}AFX_DATA_INIT
    memset(initials,0,sizeof(initials));
    }

//--------------------------------------------------

void CCalcModifyDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CCalcModifyDlg)
  DDX_Text(pDX, IDC_INITPROGRAM, initProg);
  DDX_Text(pDX, IDC_INPUTVARS, inputVars);
  DDX_Text(pDX, IDC_NAME, name);
  DDX_Text(pDX, IDC_POINTPROGRAM, pointProg);
  DDX_Check(pDX, IDC_RELTOPIN, relToPin);
  DDX_Check(pDX, IDC_WARPU, wrapU);
  DDX_Check(pDX, IDC_WARPV, wrapV);
  DDX_Text(pDX, IDC_FACEPROGRAM, faceProg);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CCalcModifyDlg, CDialog)
  //{{AFX_MSG_MAP(CCalcModifyDlg)
  ON_BN_CLICKED(IDC_SETVARS, OnSetvars)
    ON_BN_CLICKED(IDC_HELPBUTT, OnHelp)
      //}}AFX_MSG_MAP
      END_MESSAGE_MAP()
        
        /////////////////////////////////////////////////////////////////////////////
        // CCalcModifyDlg message handlers
        
        void CCalcModifyDlg::DeleteProgram(const char *name)
          {
          remove(name);
          }

//--------------------------------------------------

BOOL CCalcModifyDlg::OnInitDialog() 
  {
  if (clcname!="")
    {
    CCalcInfo nfo;
    nfo.Load(clcname);
    const char *c=strrchr(clcname,'\\');
    if (c) name=(c+1);else name=clcname;
    faceProg=nfo.faceprog;
    initProg=nfo.initprog;
    inputVars=nfo.varlist;
    pointProg=nfo.pointprog;
    relToPin=nfo.reltopin;
    wrapU=nfo.wrapu;
    wrapV=nfo.wrapv;
    memcpy(initials,nfo.initials,sizeof(initials));
    }
  CDialog::OnInitDialog();	
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

#include "..\LexTree.h"

void CCalcModifyDlg::OnOK() 
  {
  if (UpdateData()==FALSE) return;
  if (name=="") 
    {
    AfxMessageBox(IDS_MISSINGNAME);
    return;
    }
  CLexTree tr;
  int i=initProg==""?0:tr.Parse(initProg);
  if (i) 	
    ((CEdit *)GetDlgItem(IDC_INITPROGRAM))->SetSel(i-1,i);
  else
    {
    i=pointProg==""?0:tr.Parse(pointProg);
    if (i)
      ((CEdit *)GetDlgItem(IDC_POINTPROGRAM))->SetSel(i-1,i);
    else
      {
      i=faceProg==""?0:tr.Parse(faceProg);
      if (i)
        ((CEdit *)GetDlgItem(IDC_FACEPROGRAM))->SetSel(i-1,i);
      }
    }
  if (i) 
    {
    AfxMessageBox(IDS_SYNTAXERROR);return;
    }
  CCalcInfo nfo;
  nfo.faceprog=faceProg;
  nfo.initprog=initProg;
  nfo.pointprog=pointProg;
  nfo.reltopin=relToPin!=0;
  nfo.varlist=inputVars;
  nfo.wrapu=wrapU!=0;
  nfo.wrapv=wrapV!=0;
  memcpy(nfo.initials,initials,sizeof(initials));
  if (nfo.Save(GetProgramFileName(name))==false)
    {
    AfxMessageBox(IDS_UNABLETOSAVEPROGRAM);return;
    }
  CDialog::OnOK();
  }

//--------------------------------------------------

#include "CalcInitDataDlg.h"

void CCalcModifyDlg::OnSetvars() 
  {
  CString s;
  for (int i='A';i<='Z';i++)
    {
    char text[100];
    sprintf(text,"%c:Varible %c:\n",i,i);
    s+=text;
    }
  CCalcInitDataDlg dlg;
  dlg.initials=initials;
  dlg.varlist=s;
  dlg.DoModal();
  }

//--------------------------------------------------

void CCalcModifyDlg::OnHelp() 
  {  
  ShellExecute(*this,NULL,"clchelp.htm",NULL,NULL,SW_SHOWDEFAULT);
  }

//--------------------------------------------------

#define PGMEXT ".clc"

CString CCalcModifyDlg::GetProgramFileName(const char *name)
  {
  static Pathname srcpath;
  srcpath.SetFilename(name);
  if (strchr(name,'.')==NULL) return CString(srcpath)+PGMEXT;
  return CString(srcpath);
  }

//--------------------------------------------------

