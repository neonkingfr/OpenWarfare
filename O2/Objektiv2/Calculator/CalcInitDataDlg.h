#if !defined(AFX_CALCINITDATADLG_H__5C77DCC0_C0C0_11D4_B399_00C0DFAE7D0A__INCLUDED_)
#define AFX_CALCINITDATADLG_H__5C77DCC0_C0C0_11D4_B399_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CalcInitDataDlg.h : header file
//


#include "../LexTree.h"
/////////////////////////////////////////////////////////////////////////////
// CCalcInitDataDlg dialog

class CCalcInitDataDlg : public CDialog
  {
  class CEditEx:public CEdit
    {
    long uservalue;
    public:
      void SetFloat(float f, int precision);
      float GetFloat();
      CEditEx():CEdit() 
        {}
      long GetLong() 
        {return uservalue;}
      void SetLong(long val) 
        {uservalue=val;}
    };
  // Construction
  CLexTree tempvarspace;
  public:
    void ScanList();
    int ListOfVars(int offset);
    CCalcInitDataDlg(CWnd* pParent = NULL);   // standard constructor
    const char *varlist;
    float *initials;
    
    // Dialog Data
    CStatic	TextBox[8];
    CEditEx	InputBox[8];
    //{{AFX_DATA(CCalcInitDataDlg)
    enum 
      { IDD = IDD_CALCULATOR_INITVARS };
    CScrollBar	scroll;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CCalcInitDataDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CCalcInitDataDlg)
    virtual void OnOK();
    afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    virtual BOOL OnInitDialog();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALCINITDATADLG_H__5C77DCC0_C0C0_11D4_B399_00C0DFAE7D0A__INCLUDED_)
