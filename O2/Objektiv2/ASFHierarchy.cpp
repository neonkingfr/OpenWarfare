// ASFHierarchy.cpp: implementation of the CASFHierarchy class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "objektiv2.h"
#include "ASFHierarchy.h"
#include "CreatePrimDlg.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CASFHierarchy::CASFHierarchy()
  {
  count=0;
  NulovaMatice(osy);
  osy[0][0]=1;osy[1][2]=1;osy[2][1]=1;osy[3][3]=1;
  }

//--------------------------------------------------

CASFHierarchy::~CASFHierarchy()
  {
  
  }

//--------------------------------------------------

//--- LEXICAL_SYMBOLS



#define DDOTVERSION ":version"
#define DDOTNAME ":name"
#define DDOTUNITS ":units"
#define DDOTDOCUMENTATION ":documentation"
#define DDOTROOT ":root"
#define DDOTBONEDATA ":bonedata"
#define DDOTHIERARCHY ":hierarchy"
#define DDOTAXISROTATION ":axis-rotation"
#define DDOTASFFILE ":asf-file"
#define DDOTASFPATH ":asf-path"
#define DDOTFPS ":samples-per-second"
#define DDOTSMPTE ":smpte"
#define DDOTSAMPLECOUNT ":sample-count"
#define DDOTFULLYSPECIFIED ":fully-specified"
#define DDOTDEGREES ":degrees"
#define MASS "mass"
#define LENGTH "length"
#define ANGLE "angle"
#define AXIS "axis"
#define ORDER "order"
#define POSITION "position"
#define ORIENTATION "orientation"
#define BEGIN "begin"
#define END "end"
#define ID "id"
#define NAME "name"
#define DIRECTION "direction"
#define DOF "dof"
#define RX "rx"
#define RY "ry"
#define RZ "rz"
#define TX "tx"
#define TY "ty"
#define TZ "tz"
#define RAD "rad"
#define DEG "deg"
#define LOCAL "local"
#define GLOBAL "global"
#define COMMENT "#"
#define LIMITS "limits"

static bool ToNextLine(istream &in)
  {
  int i=in.get();
  while (i!='\n' && i!=EOF) i=in.get();  
  ws(in);
  return i!=EOF;
  }

//--------------------------------------------------

static bool GetWord(istream &in, char *buffer, int maxsz)
  {
  ws(in);
  int i=in.get();
  maxsz-=sizeof(char);
  while (i!=EOF && !isspace(i) && maxsz>0) 
    {
    *buffer++=(char)i;
    maxsz-=sizeof(char);
    i=in.get();
    }
  in.putback((char)i);
  *buffer=0;
  return i!=EOF;
  }

//--------------------------------------------------

static bool IsEndOfLine(istream &in)
  {
  int i=in.get();
  while (i!='\n' && i!=EOF && isspace(i)) i=in.get();  
  if (i!=EOF) in.putback((char)i);
  return i=='\n';
  }

//--------------------------------------------------

static bool ASFVersion(istream &in, SASFInfo& nfo)
  {
  in>>nfo.version;
  return !in!=0;
  }

//--------------------------------------------------

static bool ASFDotAxisRot(istream &in, SASFInfo& nfo, char *buffer, int size)
  {
  GetWord(in,buffer,size);
  if (_stricmp(buffer,GLOBAL)==0) nfo.localaxis=false;
  else if (_stricmp(buffer,LOCAL)==0) nfo.localaxis=true;
  else return true;
  return !in!=0;
  }

//--------------------------------------------------

static bool ASFName(istream &in, SASFInfo &nfo)
  {
  char buffer[512];
  ws(in);
  in.get(buffer,512);
  nfo.name=buffer;
  return !in!=0;
  }

//--------------------------------------------------

static bool ASFUnits(istream &in, SASFInfo &nfo, char *buffer, int size)
  {
  ToNextLine(in);
  while (in.peek()!=':' && GetWord(in,buffer,size))
    {
    if (_stricmp(buffer,MASS)==0) in>>nfo.mass;
    else if (_stricmp(buffer,LENGTH)==0) in>>nfo.length;
    else if (_stricmp(buffer,ANGLE)==0) 
      {
      GetWord(in,buffer,size);
      if (_stricmp(buffer,RAD)==0) nfo.deg=false;
      else if (_stricmp(buffer,DEG)==0) nfo.deg=true;
      else return true;
      }
    ToNextLine(in);
    }
  return !in!=0;
  }

//--------------------------------------------------

static bool ASFDocumentation(istream &in,SASFInfo &info)
  {
  ToNextLine(in);
  info.documentation="";
  while (in.peek()!=':' && in.peek()!=EOF) 
    {
    char buffer[256];
    in.get(buffer,256);
    while (in.peek()!='\n')
      {
      info.documentation+=buffer;
      in.get(buffer,256);  	  
      }
    info.documentation+=buffer;
    info.documentation+="\n";
    ToNextLine(in);
    }
  return !in!=0;
  }

//--------------------------------------------------

bool CASFHierarchy::ReadASF(istream &in)
  {
  ws(in);
  info.localaxis=false;
  char buffer[256];
  while (GetWord(in,buffer,sizeof(buffer)))
    {
    if (_stricmp(buffer,DDOTVERSION)==0) 
      {if (ASFVersion(in,info)) return false;}
    else if (_stricmp(buffer,DDOTNAME)==0) 
      {if (ASFName(in,info)) return false;}
    else if (_stricmp(buffer,DDOTUNITS)==0) 
      {if (ASFUnits(in,info,buffer,sizeof(buffer))) return false;}
    else if (_stricmp(buffer,DDOTDOCUMENTATION)==0) 
      {if (ASFDocumentation(in,info)) return false;}
    else if (_stricmp(buffer,DDOTHIERARCHY)==0) 
      {if (ASFHierarchy(in,buffer,sizeof(buffer))) return false;}
    else if (_stricmp(buffer,DDOTROOT)==0) 
      {if (ASFRoot(in,buffer,sizeof(buffer))) return false;}
    else if (_stricmp(buffer,DDOTAXISROTATION)==0) 
      {if (ASFDotAxisRot(in,info,buffer,sizeof(buffer))) return false;}
    else if (_stricmp(buffer,DDOTBONEDATA)==0) 
      {if (ASFBoneData(in,buffer,sizeof(buffer))) return false;}
    else if (_stricmp(buffer,COMMENT)==0) 
      {in.ignore(0x7fffffff,'\n');}
    else return false;
    if (in.peek()!=':') if (!ToNextLine(in)) break;
    }
  if (in.eof()) in.clear();
  if (!in) return false;
  return TRUE;
  }

//--------------------------------------------------

int CASFHierarchy::FindHierarchy(const char *name)
  {
  for (int i=0;i<count;i++)
    if (_stricmp(items[i].name,name)==0) return i;
  return -1;
  }

//--------------------------------------------------

bool CASFHierarchy::ASFHierarchy(istream &in, char *buffer, int size)
  {
  ToNextLine(in);
  GetWord(in,buffer,size);
  if (_stricmp(buffer,BEGIN)) return true;
  ToNextLine(in);
  while (GetWord(in,buffer,size))
    {
    if (_stricmp(buffer,END)==0) return false;
    int p=FindHierarchy(buffer);
    if (p==-1) return true;
    while (!IsEndOfLine(in))
      {
      if (!GetWord(in,buffer,size)) return true;
      int q=FindHierarchy(buffer);
      if (q==-1) return true;
      items[q].parent=p;
      }
    ToNextLine(in);
    }
  return true;
  }

//--------------------------------------------------

bool CASFHierarchy::ASFRoot(istream &in, char *buffer, int size)
  {
  ToNextLine(in);
  SASFHierarchyItem &sitem=items[0];
  while (in.peek()!=':' && GetWord(in,buffer,size))
    {
    if (_stricmp(buffer,AXIS)==0)
      {
      ws(in);
      sitem.order[0]=(char)in.get();
      sitem.order[1]=(char)in.get();
      sitem.order[2]=(char)in.get();
      }
    else if (_stricmp(buffer,ORDER)==0)
      {
      int p=0;
      while (!IsEndOfLine(in))
        {
        if (p>6) return true;
        SASFInfo::torderenum nm;
        GetWord(in,buffer,size);
        if (_stricmp(buffer,TX)==0) nm=SASFInfo::tx;
        else if (_stricmp(buffer,TY)==0) nm=SASFInfo::ty;
        else if (_stricmp(buffer,TZ)==0) nm=SASFInfo::tz;
        else if (_stricmp(buffer,RX)==0) nm=SASFInfo::rx;
        else if (_stricmp(buffer,RY)==0) nm=SASFInfo::ry;
        else if (_stricmp(buffer,RZ)==0) nm=SASFInfo::rz;
        else return true;
        info.torder[p]=nm;
        p++;
        }
      if (p<6) return true;
      }
    else if (_stricmp(buffer,POSITION)==0)
      {
      for (int i=0;i<3;i++)
        if (!IsEndOfLine(in)) in>>sitem.direction[i];
      sitem.direction[3]=1;
      }
    else if (_stricmp(buffer,ORIENTATION)==0)
      {
      for (int i=0;i<3;i++)
        if (!IsEndOfLine(in)) in>>sitem.axis[i];
      sitem.axis[3]=1;
      }
    ToNextLine(in);
    }
  sitem.length=0.0f;
  sitem.name="root";
  sitem.parent=-1;
  count=1;
  return !in!=0;
  }

//--------------------------------------------------

static bool LoadBone(istream &in, SASFHierarchyItem &sitem, char *buffer, int size)
  {
  if (_stricmp(buffer,BEGIN)) return true;
  ToNextLine(in);
  while (GetWord(in,buffer,size) && _stricmp(buffer,END))
    {
    int id;
    if (_stricmp(buffer,ID)==0) in>>id;
    else if (_stricmp(buffer,NAME)==0) 
      {
      ws(in);
      in.get(buffer,size);
      sitem.name=buffer;
      }
    else if (_stricmp(buffer,DIRECTION)==0)
      {
      for (int i=0;i<3;i++) in>>sitem.direction[i];		
      }
    else if (_stricmp(buffer,LENGTH)==0)
      {
      in>>sitem.length;
      }
    else if (_stricmp(buffer,AXIS)==0)
      {
      int i;
      for (i=0;i<3;i++) in>>sitem.axis[i];
      ws(in);
      for (i=0;i<3;i++) in>>sitem.order[i];
      }
    else if (_stricmp(buffer,DOF)==0)
      {
      int c=0;
      while (c<10 && !IsEndOfLine(in))
        {
        GetWord(in,buffer,size);
        if (_stricmp(buffer,RX)==0) sitem.dof[c]='x';
        else if (_stricmp(buffer,RY)==0) sitem.dof[c]='y';
        else if (_stricmp(buffer,RZ)==0) sitem.dof[c]='z';
        else if (_stricmp(buffer,"l")==0) sitem.dof[c]='l';
        else return true;
        c++;
        }
      sitem.dof[c]=0;	  
      }
    else if (_stricmp(buffer,LIMITS)==0)
      {
      char p[50];
      char *c=sitem.dof;
      while (*c)
        {
        if (c!=sitem.dof) ToNextLine(in);else ws(in);
        if (in.get()!='(') return true;
        in.get(p,50,')');
        ws(in);
        if (in.get()!=')') return false;
        c++;
        }
      }
    ToNextLine(in);
    }
  sitem.process=false;
  return !in!=0;
  }

//--------------------------------------------------

bool CASFHierarchy::ASFBoneData(istream &in, char *buffer, int size)
  {
  ToNextLine(in);
  while (in.peek()!=':' && GetWord(in,buffer,size))
    {
    SASFHierarchyItem &sitem=items[count];
    memset(sitem.dof,0,sizeof(sitem.dof));
    memset(sitem.order,0,sizeof(sitem.order));
    sitem.process=true;
    sitem.parent=-1;
    if (LoadBone(in,sitem,buffer,size)) return true;
    count++;
    ToNextLine(in);
    }
  return !in!=0;
  }

//--------------------------------------------------

static void DumpFile(istream &in, CString& text)
  {
  in.clear();
  char *c=text.GetBuffer(256);
  int i;
  int z;
  for (i=0;i<250;i++)
    {	  
    z=in.get();
    if (z=='\n') c[i++]='\r';
    if (z!=EOF) c[i]=(char)z;else break;
    }
  c[i]=0;
  text.ReleaseBuffer();
  if (z!=EOF) text+="...";
  text.FreeExtra();
  }

//--------------------------------------------------

bool CASFHierarchy::ReadASF(const char *filename,CString &errortext)
  {
  ifstream in(filename,ios::in);
  if (!in)
    {
    errortext="Unable to open file!";
    return false;
    }
  if (ReadASF(in)==false)
    {
    DumpFile(in,errortext);
    return false;
    }
  return true;
  }

//--------------------------------------------------

void SASFHierarchyItem::DoPrecalculate(bool deg,SASFHierarchyItem *parent, float mscale)
  {
  int i;
  HMATRIX mx;
  HMATRIX tmp;
  JednotkovaMatice(mx);
  for (i=0;i<3;i++)
    {
    float uhel=axis[i];
    if (deg) uhel*=FPI/180.0f;
    switch (toupper(order[i]))
      {
      case 'X': RotaceX(tmp,uhel);break;
      case 'Y': RotaceY(tmp,uhel);break;
      case 'Z': RotaceZ(tmp,uhel);break;
      }
    SoucinMatic(mx,tmp,mx);
    }
  InverzeMatice(mx,Cinv);
  if (parent)
    {
    Translace(tmp,parent->direction[0]*parent->length*mscale,parent->direction[1]*parent->length*mscale,parent->direction[2]*parent->length*mscale);
    SoucinMatic(mx,tmp,CB);
    }
  else
    CopyMatice(CB,mx);
  CopyMatice(selfmx,tmp);
  }

//--------------------------------------------------

void CASFHierarchy::RecalculateStatic()
  {
  int i;
  for (i=0;i<count;i++)
    {
    if (items[i].parent==-1) items[i].DoPrecalculate(info.deg,NULL,mscale);
    else items[i].DoPrecalculate(info.deg,items+items[i].parent,mscale);
    }
  SoucinMatic(items[0].selfmx,osy,items[0].selfmx);
  for (i=0;i<count;i++)
    {
    int parent=items[i].parent;
    if (parent!=-1)
      SoucinMatic(items[i].selfmx,items[parent].globmx,items[i].globmx);
    else
      CopyMatice(items[i].globmx,items[i].selfmx);
    }
  }

//--------------------------------------------------

bool CASFHierarchy::OpenAMCFile(istream &in, CString &error)
  {
  ws(in);
  amc.fps=1.0f;
  amc.samplecount=-1;
  amc.fullspec=false;
  char buffer[256];
  while (GetWord(in,buffer,sizeof(buffer)))
    {
    if (_stricmp(buffer,DDOTASFFILE)==0) 
      {
      ws(in);
      in.get(buffer,sizeof(buffer));
      amc.fname=buffer;
      }
    else if (_stricmp(buffer,DDOTASFPATH)==0) 
      {
      ws(in);
      in.get(buffer,sizeof(buffer));
      amc.path=buffer;
      }
    else if (_stricmp(buffer,DDOTFPS)==0) in>>amc.fps;
    else if (_stricmp(buffer,DDOTSMPTE)==0) 
      {
      ws(in);
      in.get(buffer,sizeof(buffer));
      amc.smpte=buffer;
      }
    else if (_stricmp(buffer,DDOTSAMPLECOUNT)==0) in>>amc.samplecount;
    else if (_stricmp(buffer,DDOTFULLYSPECIFIED)==0) amc.fullspec=true;
    else if (_stricmp(buffer,DDOTDEGREES)==0)
      {
      ToNextLine(in);
      /*	  if (amc.fullspec!=true) 
		{
		error="Only fullspecified frames are accepted!";
		return false;
		}*/
      return true;
      }
    else if (_stricmp(buffer,COMMENT)==0) 
      {in.ignore(0x7fffffff,'\n');in.putback('\n');}
    ToNextLine(in);
    }  
  DumpFile(in,error);
  return false;
  }

//--------------------------------------------------

int CASFHierarchy::ReadAMCFrame(istream &in)
  {
  int frame=-1;
  in>>frame;
  if (in.eof()) return 1;
  if (!in) return -1;
  char buff[256];
  while (!isdigit(in.peek()) && GetWord(in,buff,sizeof(buff)))
    {
    int q=FindHierarchy(buff);
    if (q==-1) return -1;
    if (q==0)
      {
      float p;
      HMATRIX mx;
      JednotkovaMatice(mx);
      HMATRIX tmp;
      float v[6];
      for (int i=0;i<6;i++)
        {
        in>>p;
        float ang=p;
        float dis=p*mscale;
        if (info.deg) ang*=FPI/180.0f;
        switch (info.torder[i])
          {
          case SASFInfo::tx: v[3]=dis;break;
          case SASFInfo::ty: v[4]=dis;break;
          case SASFInfo::tz: v[5]=dis;break;
          case SASFInfo::rx: v[0]=ang;break;
          case SASFInfo::ry: v[1]=ang;break;
          case SASFInfo::rz: v[2]=ang;break;
          /*  case SASFInfo::tx: Translace(tmp,dis,0,0);break;
		  case SASFInfo::ty: Translace(tmp,0,dis,0);break;
		  case SASFInfo::tz: Translace(tmp,0,0,dis);break;
		  case SASFInfo::rx: RotaceZ(tmp,ang);break;
		  case SASFInfo::ry: RotaceY(tmp,ang);break;
		  case SASFInfo::rz: RotaceX(tmp,ang);break;*/
          }
        /*		SoucinMatic(tmp,mx,tmp2);
		CopyMatice(mx,tmp2);*/
        }
      RotaceZ(tmp,v[2]);SoucinMatic(mx,tmp,mx);
      RotaceX(tmp,v[0]);SoucinMatic(mx,tmp,mx);
      RotaceY(tmp,-v[1]);SoucinMatic(mx,tmp,mx);
      Translace(tmp,v[3],v[4],v[5]);SoucinMatic(mx,tmp,mx);
      SoucinMatic(mx,osy,items[q].selfmx);
      }
    else
      {
      SASFHierarchyItem  &itm=items[q];
      HMATRIX M;
      JednotkovaMatice(M);
      HMATRIX tmp;
      for (int i=0;i<10;i++)
        {
        float p;
        if (itm.dof[i]==0) break;
        in>>p;
        if (info.deg) p*=FPI/180.0f;
        switch (toupper(itm.dof[i]))
          {
          case 'X': RotaceX(tmp,p);break;
          case 'Y': RotaceY(tmp,-p);break;
          case 'Z': RotaceZ(tmp,p);break;
          case 'L': break;
          }
        SoucinMatic(M,tmp,M);
        }
      SoucinMatic(itm.Cinv,M,tmp);
      SoucinMatic(tmp,itm.CB,itm.selfmx);
      }
    ToNextLine(in);
    }
  for (int i=0;i<count;i++)
    {
    int parent=items[i].parent;
    if (parent!=-1)
      SoucinMatic(items[i].selfmx,items[parent].globmx,items[i].globmx);
    else
      CopyMatice(items[i].globmx,items[i].selfmx);
    }
  return 0;
  }

//--------------------------------------------------

static void CreateBonePrimitive(ObjectData &obj,HVECTOR direction, float length)
  {
  float ml=length/5;
  HVECTOR x,y,z,t1,t2,tmp;
  CopyVektor(x,direction);
  CopyVektor(t1,mxVector3(1,0,0));
  CopyVektor(t2,mxVector3(0,1,0));
  VektorSoucin(x,t1,tmp);
  VektorSoucin(x,t1,y);
  if (ModulVektoru2(tmp)>ModulVektoru2(y)) CopyVektor(y,tmp);
  NormalizeVector(y);
  VektorSoucin(x,y,z);
  NormalizeVector(z);
  int p=obj.NPoints();
  obj.ReservePoints(5);
  PosT ps;
  ps[0]=-y[XVAL]*ml; ps[1]=-y[YVAL]*ml; ps[2]=-y[ZVAL]*ml;
  ps.flags=0;
  obj.Point(p)=ps;
  ps[0]=x[XVAL]*length; ps[1]=x[YVAL]*length; ps[2]=x[ZVAL]*length;
  obj.Point(p+1)=ps;
  ps[0]=y[XVAL]*ml; ps[1]=y[YVAL]*ml; ps[2]=y[ZVAL]*ml;
  obj.Point(p+2)=ps;
  ps[0]=z[XVAL]*ml; ps[1]=z[YVAL]*ml; ps[2]=z[ZVAL]*ml;
  obj.Point(p+3)=ps;
  ps[0]=-z[XVAL]*ml; ps[1]=-z[YVAL]*ml; ps[2]=-z[ZVAL]*ml;
  obj.Point(p+4)=ps;
  int f=obj.NFaces();
  obj.ReserveFaces(5);
  FaceT fc(&obj);
  memset(&fc,0,sizeof(fc));
  fc.SetN(3);fc.SetPoint(0,p+0);fc.SetPoint(2,p+1);fc.SetPoint(1,p+4);
  fc.SetFlags(fc.GetFlags()|(1<<FACE_COLORIZE_SHIFT));
  fc.CopyFaceTo(f);  
  fc.SetPoint(0,p+1);fc.SetPoint(2,p+2);fc.SetPoint(1,p+4);
  fc.SetFlags(0);
  fc.CopyFaceTo(f+1);
  fc.SetPoint(0,p+1);fc.SetPoint(2,p+3);fc.SetPoint(1,p+2);
  fc.CopyFaceTo(f+2);
  fc.SetPoint(0,p+0);fc.SetPoint(2,p+3);fc.SetPoint(1,p+1);
  fc.CopyFaceTo(f+4);
  fc.SetN(4);
  fc.SetPoint(0,p+0);fc.SetPoint(3,p+4);fc.SetPoint(2,p+2);fc.SetPoint(1,p+3);
  fc.CopyFaceTo(f+3);
  }

//--------------------------------------------------

bool CASFHierarchy::ImportToModel(ObjectData &obj, const char *acm, CString &err)
  {
  items[0].process=false;
  RecalculateStatic();
  ifstream in(acm,ios::in);
  if (!in)
    {
    err="Unable to open file!";
    return false;
    }
  if (OpenAMCFile(in,err)==false) return false;
  int idx=obj.AnimationIndex(-0.5f);
  AnimationPhase p(&obj);
  if (idx==-1) 
    {
    p.SetTime(-0.5f);
    obj.AddAnimation(p);
    idx=obj.AnimationIndex(-0.5f);
    }
  else
    obj.UseAnimation(idx);
  int i;
  for (i=0;i<count;i++) if (items[i].process && items[i].length)
    {
    obj.SaveNamedSel(items[i].name);
    Selection *sel=items[i].ns=obj.GetNamedSel(items[i].name);
    int np=obj.NPoints();
    int nf=obj.NFaces();
    int j;
    CreateBonePrimitive(obj,items[i].direction,items[i].length*mscale);
    for (j=np;j<obj.NPoints();j++) sel->PointSelect(j);
    for (j=nf;j<obj.NFaces();j++) sel->FaceSelect(j);
    }
  obj.RedefineAnimation(idx);
  int cidx=0;
  while (cidx<obj.NAnimations() && obj.GetAnimation(cidx)->GetTime()<0.0f) cidx++;
  float lasttime=0.0f;
  int res;
  while ((res=ReadAMCFrame(in))==0)
    {
    if (cidx>=obj.NAnimations())
      {
      AnimationPhase phs(&obj);
      lasttime+=1.0f;
      phs.SetTime(lasttime);
      obj.AddAnimation(phs);	  
      }
    AnimationPhase *cur=obj.GetAnimation(cidx);
    lasttime=cur->GetTime();
    for (i=0;i<count;i++) if (items[i].process)
      {	  
      for (int j=0;j<obj.NPoints();j++) if (items[i].ns->PointSelected(j))
        {
        PosT ps=obj.Point(j);
        HVECTOR vx;
        TransformVector(items[i].globmx,mxVector3(ps[0],ps[1],ps[2]),vx);
        ps[0]=vx[0];
        ps[1]=vx[1];
        ps[2]=vx[2];
        (*cur)[j]=ps;
        }
      }
    cidx++;
    }  
  if (res==-1)
    {
    DumpFile(in,err);
    err+="\r\n\r\nIncomplette data imported!";
    return false;
    }
    {
    obj.UseAnimation(-1);
    AnimationPhase *cur=obj.GetAnimation(idx);
    RecalculateStatic();
    for (i=0;i<count;i++) if (items[i].process)
      {	  
      for (int j=0;j<obj.NPoints();j++) if (items[i].ns->PointSelected(j))
        {
        PosT ps=obj.Point(j);
        HVECTOR vx;
        TransformVector(items[i].globmx,mxVector3(ps[0],ps[1],ps[2]),vx);
        ps[0]=vx[0];
        ps[1]=vx[1];
        ps[2]=vx[2];
        (*cur)[j]=ps;
        }
      }
    }
  obj.UseAnimation(0);
  return true;
  }

//--------------------------------------------------

