#if !defined(AFX_INTERNALVIEWER_H__9679F6E0_7657_48E9_87E5_1D3A00742312__INCLUDED_)
#define AFX_INTERNALVIEWER_H__9679F6E0_7657_48E9_87E5_1D3A00742312__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InternalViewer.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInternalViewer view

class CInternalViewer : public CView
  {
  HWCONFIG2 cfg;
  protected:
    CInternalViewer();           // protected constructor used by dynamic creation
    DECLARE_DYNCREATE(CInternalViewer)
      
      // Attributes
    public:
    bool InitViewer();
    // Operations
  public:
    bool InitViewer(CDocument *doc);
    static bool g3dInited;
    static bool g3dOpened;
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CInternalViewer)
  protected:
    virtual void OnDraw(CDC* pDC);      // overridden to draw this view
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    virtual ~CInternalViewer();
    #ifdef _DEBUG
    virtual void AssertValid() const;
    virtual void Dump(CDumpContext& dc) const;
    #endif
    
    // Generated message map functions
  protected:
    //{{AFX_MSG(CInternalViewer)
    afx_msg void OnDestroy();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INTERNALVIEWER_H__9679F6E0_7657_48E9_87E5_1D3A00742312__INCLUDED_)
