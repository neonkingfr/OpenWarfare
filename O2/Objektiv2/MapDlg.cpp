// MapDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "MapDlg.h"
#include "Calculator.h"
#include "optima\express.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMapDlg dialog


CMapDlg::CMapDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CMapDlg::IDD, pParent)
    {
    for (int i=0;i<6;i++)
      {
      Value[i] = 0.0f;
      Calc[i] = _T("");
      }
    //{{AFX_DATA_INIT(CMapDlg)
    onlypoints = FALSE;
    SchemeName = _T("");
    uwrap = FALSE;
    vwrap = FALSE;
    onlyfaces = FALSE;
    //}}AFX_DATA_INIT
    curschemetype=1;
    }

//--------------------------------------------------

void CMapDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  for (int i=0;i<6;i++)
    {
    DDX_Control(pDX, IDC_AVALUENAME+i, wndValueName[i]);
    DDX_Control(pDX, IDC_AVALUE+i, wndValue[i]);
    DDX_Text(pDX, IDC_AVALUE+i, Value[i]);
    DDX_Text(pDX, IDC_ACALC+i, Calc[i]);
    }
  //{{AFX_DATA_MAP(CMapDlg)
  DDX_Control(pDX, IDC_LIST, list);
  DDX_Check(pDX, IDC_POIINTSONLY, onlypoints);
  DDX_Text(pDX, IDC_SCHEMENAME, SchemeName);
  DDX_Check(pDX, IDC_UWRAP, uwrap);
  DDX_Check(pDX, IDC_VWRAP, vwrap);
  DDX_Check(pDX, IDC_FACESONLY, onlyfaces);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CMapDlg, CDialog)
  //{{AFX_MSG_MAP(CMapDlg)
  ON_BN_CLICKED(IDC_DEFINE, OnDefine)
    ON_LBN_SELCHANGE(IDC_LIST, OnSelchangeList)
      ON_BN_CLICKED(IDC_SAVE, OnSave)
        ON_BN_CLICKED(IDC_NEW, OnNew)
          ON_BN_CLICKED(IDC_REMOVE, OnRemove)
            //}}AFX_MSG_MAP
            END_MESSAGE_MAP()
              
              /////////////////////////////////////////////////////////////////////////////
              // CMapDlg message handlers
              
              #define HALFDLGSIZE 121
              
              BOOL CMapDlg::OnInitDialog() 
                {
                CDialog::OnInitDialog();
                CRect rc,rc2(0,0,10,HALFDLGSIZE);
                GetWindowRect(&rc);
                dlgsize=rc.Size();  
                MapDialogRect(&rc2);
                GetClientRect(&rc);
                CSize sz=rc.Size();
                sz.cy=rc2.bottom-rc2.top+GetSystemMetrics(SM_CYCAPTION);
                SetWindowPos(NULL,0,0,sz.cx,sz.cy,SWP_NOMOVE|SWP_NOZORDER);  
                LoadSchemesToLB(NULL);
                SelectScheme("");
                return TRUE;  // return TRUE unless you set the focus to a control
                // EXCEPTION: OCX Property Pages should return FALSE
                }

//--------------------------------------------------

void CMapDlg::OnDefine() 
  {
  SetWindowPos(NULL,0,0,dlgsize.cx,dlgsize.cy,SWP_NOMOVE|SWP_NOZORDER);  
  GetDlgItem(IDC_DEFINE)->EnableWindow(FALSE);
  }

//--------------------------------------------------

void CMapDlg::SelectScheme(const char *name)
  {
  CSchemes::HashEntry *he,key;
  he=schemes.GetScheme(curschemetype,name);
  key.SetKey(curschemetype,name);
  if (he==NULL) he=&key;
  for (int i=0;i<6;i++)
    {
    if (he->calc[i].GetLength()>0 && he->calc[i][0]=='$') 
      {
      char *c=he->calc[i].LockBuffer();
      c++;
      wndValueName[i].SetWindowText(c);
      Value[i]=0.0f;
      wndValueName[i].ShowWindow(SW_SHOW);
      wndValue[i].ShowWindow(SW_SHOW);
      }
    else
      {
      wndValueName[i].ShowWindow(SW_HIDE);
      wndValue[i].ShowWindow(SW_HIDE);
      }
    Calc[i]=he->calc[i];
    }
  char c;
  if (he->calc[6].GetLength()>0) c=he->calc[6][0];else c=0;
  uwrap=(c & 1)!=0;
  vwrap=(c & 2)!=0;
  SchemeName=he->name;
  UpdateData(FALSE);
  }

//--------------------------------------------------

void CMapDlg::OnSelchangeList() 
  {
  int cursel=list.GetCurSel();
  if (cursel==LB_ERR) return;
  CString s;
  list.GetText(cursel,s);
  SelectScheme(s);	
  }

//--------------------------------------------------

static char variables[]=
  {'a'-'a','b'-'a','c'-'a','d'-'a','u'-'a','v'-'a'};

//--------------------------------------------------

void CMapDlg::LoadSchemesToLB(const char *active)
  {
  list.ResetContent();
  for (CSchemes::HashEntry *he=NULL;he=schemes.Enum(he);)
    if (he->type==curschemetype)
      {
      list.AddString(he->name);
      }
  if (active) list.SelectString(-1,active);
  }

//--------------------------------------------------

void CMapDlg::OnSave() 
  {
  UpdateData();
  for (int j=0;j<6;j++) SetVariable(variables[j],12.0);
  for (int i=0;i<6;i++)
    {
    EvalError err;
    if (Calc[i]!="")
      if (Calc[i][0]!='$') 
        {
        const char *c=Evaluate(Calc[i],err);
        if (err==EvalOpenB||err==EvalCloseB||err==EvalOper||err==EvalNum||err==EvalVar)
          {
          AfxMessageBox(WString(IDS_EVALUTIONERROR,variables[i]+'A',c));
          GetDlgItem(IDC_ACALC+i)->SetFocus();
          return;
          }
        }
    }  
  CSchemes::HashEntry *he,key;
  he=schemes.GetScheme(curschemetype,SchemeName);
  key.SetKey(curschemetype,SchemeName);
  if (he) 
    {
    *he=key;
    for (int i=0;i<6;i++) he->calc[i]=Calc[i];    
    he->calc[6]=(char)(64+(vwrap<<1)+(uwrap));
    }
  else 
    {
    he=new CSchemes::HashEntry(key);
    for (int i=0;i<6;i++) he->calc[i]=Calc[i];
    he->calc[6]=(char)(64+(vwrap<<1)+(uwrap));
    schemes.AddScheme(he);
    }
  LoadSchemesToLB(SchemeName);
  SelectScheme(SchemeName);
  schemes.SaveSchemes();
  }

//--------------------------------------------------

void CMapDlg::OnNew() 
  {
  for (int i=0;i<6;i++) Calc[i]="";
  SchemeName="";	
  uwrap=FALSE;
  vwrap=FALSE;
  UpdateData();
  LoadSchemesToLB(NULL);
  SelectScheme("");
  }

//--------------------------------------------------

void CMapDlg::OnRemove() 
  {
  UpdateData();
  CSchemes::HashEntry *he;
  he=schemes.GetScheme(curschemetype,SchemeName);
  if (he==NULL) 
    {
    AfxMessageBox(IDS_SCHEMENOTFOUND);
    return;
    }
  if (AfxMessageBox(WString(IDS_ASKSCHEMEREMOVE,he->name),MB_YESNO|MB_ICONQUESTION)==IDYES)
    {
    schemes.RemoveScheme(curschemetype,SchemeName);
    LoadSchemesToLB(NULL);
    SelectScheme("");
    }
  }

//--------------------------------------------------

