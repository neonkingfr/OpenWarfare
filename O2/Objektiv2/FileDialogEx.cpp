// FileDialogEx.cpp : implementation file
//

#include "stdafx.h"
#include "FileDialogEx.h"
#include "dlgs.h"
#include ".\filedialogex.h"

static const int HistoryCommand=8598;

IFileDlgHistory *CFileDialogEx::historyInterface=0;

IMPLEMENT_DYNAMIC(CFileDialogEx, CFileDialog)
CFileDialogEx::CFileDialogEx(BOOL bOpenFileDialog, LPCTSTR lpszDefExt, LPCTSTR lpszFileName,
		DWORD dwFlags, LPCTSTR lpszFilter, CWnd* pParentWnd) :
		CFileDialog(bOpenFileDialog, lpszDefExt, lpszFileName, dwFlags, lpszFilter, pParentWnd),sdlg(this)
{
}

CFileDialogEx::~CFileDialogEx()
{
}


BEGIN_MESSAGE_MAP(CFileDialogEx, CFileDialog)
END_MESSAGE_MAP()



// CFileDialogEx message handlers

BOOL CALLBACK FindToolBar(HWND hwnd,
                 LPARAM lParam
                 )
{
  char buffer[256];
  GetClassName(hwnd,buffer,sizeof(buffer));
  if (strcmp(buffer,"ToolbarWindow32")==0)
  {
    CRect rc;
    GetWindowRect(hwnd,&rc);
    CSize sz=rc.Size();
    if (sz.cx>sz.cy)
    {
      *(HWND *)lParam=hwnd;
      return FALSE;
    }
  }
  return TRUE;
}



void CFileDialogEx::OnInitDone()
{
  _toolbarhwnd=0;
  if (historyInterface)
  {  
    HWND toolBar=0;
    EnumChildWindows(*(this->GetParent()),FindToolBar,(LPARAM)&toolBar);
    if (toolBar)
    {
      CToolBarCtrl toolbar;
      toolbar.Attach(toolBar);
      int idx=toolbar.AddBitmap(1,CBitmap::FromHandle(historyInterface->GetHistoryBitmap()));
      TBBUTTON butt;
      butt.iBitmap=idx;
      butt.idCommand=HistoryCommand;
      butt.fsState=TBSTATE_ENABLED;
      butt.fsStyle=TBSTYLE_BUTTON;
      butt.dwData=0;
      butt.bReserved[0]=0;      
      butt.bReserved[1]=0;      
      toolbar.AddButtons(1,&butt);
      CRect rc;
      toolbar.GetWindowRect(&rc);
      toolbar.SetWindowPos(0,0,0,rc.Size().cx+16,rc.Size().cy,SWP_NOMOVE|SWP_NOZORDER);
      toolbar.Detach();
      sdlg.SubclassWindow(*GetParent());
      _toolbarhwnd=toolBar;
    }
  }
  CFileDialog::OnInitDone();
}

void CFileDialogEx::OnHistoryCommand()
{
  AutoArray<RString,MemAllocLocal<RString,256> > dirList;
  int idx=0;
  const char *path=historyInterface->GetNextHistoryFolder(idx);
  while (path)
  {
    idx++;
    RString name=path;
    dirList.Add(name);
    path=historyInterface->GetNextHistoryFolder(idx);
  }

  CMenu menu;
  menu.CreatePopupMenu();
  for (int i=0;i<dirList.Size();i++)       
  {
    RString name=dirList[i]; 
    if (name.GetLength()>30)
    {
      int end=name.GetLength()-27;
      while (end>3 && name[end]!='\\') end--;
      if (end>3)
        name=name.Mid(0,3)+"..."+name.Mid(end,name.GetLength()-end);
    }
    menu.AppendMenu(MF_STRING,i+1000,name);
    CBitmap *bmp=CBitmap::FromHandle(historyInterface->GetHistoryMenuBitmap());
    menu.SetMenuItemBitmaps(i,MF_BYPOSITION,bmp,bmp);
  }
  
  CToolBarCtrl tb;
  tb.Attach(_toolbarhwnd);
  CRect rcbutt;
  tb.GetItemRect(0,&rcbutt);
  tb.ClientToScreen(&rcbutt);
  int id=menu.TrackPopupMenu(TPM_NONOTIFY|TPM_RETURNCMD,rcbutt.left,rcbutt.bottom,this);
  if (id>=1000)
  {
    id-=1000;
    GotoPath(dirList[id]);
  }
  tb.Detach();
}
void CFileDialogEx::GotoPath(LPCTSTR path)
{
  SetControlText(cmb13,path);
  SetControlText(edt1,path);
  GetParent()->SendMessage(WM_COMMAND,IDOK,0);
  SetControlText(edt1,"");
}

BOOL CFileDialogEx::SubclassDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
  if (LOWORD(wParam)==HistoryCommand)
    outer->OnHistoryCommand();
  return __super::OnCommand(wParam,lParam);
}
BOOL CFileDialogEx::OnFileNameOK()
{
  if (historyInterface)
  {
    Pathname pth=GetPathName();
    historyInterface->StoreHistoryFolder(pth.GetDirectoryWithDriveWLBS());
  }

  return CFileDialog::OnFileNameOK();
}
