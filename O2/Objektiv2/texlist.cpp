// TexList.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "TexList.h"
#include "MainFrm.h"
#include "Objektiv2Doc.h"
#include "Objektiv2View.h"
#include <malloc.h>
#include <fstream>
using namespace std;
#include <shlobj.h>
#include "oldStuff/pal2pac.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTexList dialog

class CISelectByTexture:public CICommand
  {
  CString texname;
  public:
    CISelectByTexture(const char *tex):texname(tex) 
      {marksave=false;};
    CISelectByTexture(istream& str)
      {
      char text[256];
      FLoadString(str,text,sizeof(text));
      texname=text;
      }
    virtual int Tag() const 
      {return CITAG_SELECTBYTEXTURE;}
    virtual void Save(ostream &str) 
      {FSaveString(str,texname);}
    virtual int Execute(LODObject *lod)
      {
      ObjectData *obj=lod->Active();
      obj->ClearSelection();
      int i,n=obj->NFaces();
      for( i=0; i<n; i++ )
        {
        FaceT ns(obj,i);
        if( !_strcmpi(ns.GetTexture(),texname) || (ns.GetTexture().GetLength()==0 && texname.GetLength()==0))
          {
          obj->FaceSelect(i);
          }
        }
      obj->SelectPointsFromFaces();
      return 0;
      }
  };










CICommand *CISelectByTexture_Create(istream& str)
  {
  return new CISelectByTexture(str);
  }










CTexList::CTexList()
  : CDialogBar()
    {
    //{{AFX_DATA_MAP(CTexList)
    //}}AFX_DATA_MAP
    //{{AFX_DATA_INIT(CTexList)
    //}}AFX_DATA_INIT
    Hash_InitEx(&hash,1001,texinfo,name,0);
    Hash_InitEx(&model,101,texinfo,name,HASHF_EXISTERROR);
    preview=false;
    scan=false;
    error=false;
    }










CTexList::~CTexList()
  {
  Hash_Done(&hash);
  Hash_Done(&model);
  }










BEGIN_MESSAGE_MAP(CTexList, CDialogBar)
  //{{AFX_MSG_MAP(CTexList)
  ON_WM_MEASUREITEM()
    ON_WM_DRAWITEM()
      ON_BN_CLICKED(IDC_PATH, OnPath)
        ON_WM_DESTROY()
          ON_LBN_DBLCLK(IDC_TEXLIST, OnDblclkTexlist)
            ON_WM_SIZE()
              ON_CBN_SELCHANGE(IDC_LIBRARY, OnSelchangeLibrary)
                ON_BN_CLICKED(IDC_CUT, OnCut)
                  ON_BN_CLICKED(IDC_PASTE, OnPaste)
                    ON_CBN_EDITCHANGE(IDC_LIBRARY, OnEditchangeLibrary)
                      ON_BN_CLICKED(IDC_PREVIEW, OnPreview)
                        ON_WM_TIMER()
                          //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CTexList message handlers
    
    
    #define LIBNAME "textures.tlb"
    #define MODELLIB "#This object"
    #define MYFOLDER "#My folder"
    #define SHAREDFOLDER "#Shared folder"
    
    void CTexList::LoadLibraryList()
      {  
      needtosave=false;
      char *c=(char *)alloca(prvtable.GetFullNameSize(LIBNAME));
      prvtable.GetFullName(c,LIBNAME);
      ifstream rstr(c, ios::in|ios::binary); 
      if (!rstr.is_open()) return;
      texcnt=0;
      while(rstr.eof()==0)
        {
        texinfo tinfo;
        tinfo.SetKey("");
        FLoadString(rstr,tinfo.name,sizeof(tinfo.name));
        FLoadString(rstr,tinfo.lib,sizeof(tinfo.lib));
		_strlwr(tinfo.name);
		_strlwr(tinfo.lib);
        if (rstr.eof()) break;
        Hash_Add(&hash,&tinfo);
        texcnt++;
        }
      }










void CTexList::SaveLibrary()
  {  
  if (!needtosave) return;
  char *c=(char *)alloca(prvtable.GetFullNameSize(LIBNAME));
  prvtable.GetFullName(c,LIBNAME);
  ofstream rstr(c, ios::out|ios::binary|ios::trunc); 
  if (!rstr.is_open()) return;
  for (texinfo *txt=NULL; txt=(texinfo *)Hash_EnumData(&hash,(void *)txt);)
    {
    FSaveString(rstr,txt->name);
    FSaveString(rstr,txt->lib);  
    }
  }










void CTexList::CheckForNewTextures()
  {
  CFileFind fnd;
  THASHTABLE tbl;
  CNamedPropertyArray npa;
  char *c=(char *)alloca(prvtable.GetFullNameSize("*.*"));
  prvtable.GetFullName(c,"*.*");
  BOOL succ=fnd.FindFile(c);
  CString defname;
  defname.LoadString(IDS_DEFLIBRARY);
  Hash_InitEx(&tbl,1001,texinfo,name,HASHF_UPDATE);
  EnableWindow(FALSE);
  m_TexList.ShowWindow(SW_HIDE);
  m_Progress.ShowWindow(SW_SHOW);
  m_Progress.SetRange(0,512);
  int cntr=0;
  scan=true;
  if (succ) 
    {
    succ=fnd.FindNextFile();
    while (succ)
      {
      //	TRACE1("%s\n",fnd.GetFileName());
      if (!fnd.IsDots() && !fnd.IsDirectory())
        {
        texinfo txt;
        txt.SetKey(fnd.GetFileName());
        strncpy(txt.lib,defname,sizeof(txt.lib));
        //	  ASSERT(txt.name[0]!='C' || txt.name[0]!='G');
        //	  ASSERT(Hash_Fill(&hash,&txt)!=NULL);
		_strlwr(txt.name);
		_strlwr(txt.lib);
        Hash_Add(&tbl,&txt);
        cntr++;
        m_Progress.SetPos(cntr & 0x1FF);
        MSG msg;
        while (::PeekMessage(&msg,NULL,0,0,PM_REMOVE)) 	   
          if (theApp.PreTranslateMessage(&msg)==FALSE)
            {
            ::TranslateMessage(&msg);
            ::DispatchMessage(&msg);
            if (scan==false) 
              goto quit;
            }
        }	
      succ=fnd.FindNextFile();
      }
    }
  quit:
  Hash_Done(&hash);
  hash=tbl;
  EnableWindow(TRUE);
  m_TexList.ShowWindow(SW_SHOW);
  m_Progress.ShowWindow(SW_HIDE);
  texcnt=cntr;
  scan=false;
  }










BOOL CTexList::OnInitDialog() 
  {
  //	CDialogBar::OnInitDialog();
  
  strcpy(curpath,config->GetString(IDS_CFGINTVIEWXRES));
  prvtable.SetRefPath(curpath);
  LoadLibraryList();
  //	CheckForNewTextures();
  //	UpdateLibraryList();
  CString defname;
  defname.LoadString(IDS_DEFLIBRARY);
  defname=theApp.GetProfileString("TexList","DefLibrary",MODELLIB);
  m_LibList.SetWindowText(defname);
  //	m_LibList.SetWindowText(MODELLIB);
  //	UpdateList();
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }










void CTexList::UpdateList()
  {
  CString str;
  int cursel=m_TexList.GetCurSel();
  m_LibList.GetWindowText(str);
  m_TexList.ResetContent();
  m_TexList.InitStorage(texcnt+100,(texcnt+100)*20);
  m_TexList.SetRedraw(FALSE);
  if (str!=MODELLIB)
    for (texinfo *txt=NULL; txt=(texinfo *)Hash_EnumData(&hash,(void *)txt);)
      {
      if (str==txt->lib)
        {
        int index=m_TexList.AddString(txt->name);
        m_TexList.SetItemDataPtr(index,txt);
        }
      }
  else
    {
    for (texinfo *txt=NULL; txt=(texinfo *)Hash_EnumData(&model,(void *)txt);)
      {
      int index=m_TexList.AddString(txt->name);
      m_TexList.SetItemDataPtr(index,txt);
      }
    }
  m_TexList.SetSel(cursel);
  m_TexList.SetRedraw(TRUE);
  }










void CTexList::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
  {
  if (nIDCtl==IDC_TEXLIST)
    {
    lpMeasureItemStruct->itemWidth=50;
    if (preview) lpMeasureItemStruct->itemHeight=51;
    else 
      {		
      CWnd *wnd=GetDlgItem(nIDCtl);
      CDC *pDC=wnd->GetWindowDC();
      CSize sz=pDC->GetTextExtent("W");
      lpMeasureItemStruct->itemHeight=sz.cy;
      wnd->ReleaseDC(pDC);
      }
    }
  else
    CDialogBar::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
  }










void CTexList::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
  {
  if (nIDCtl==IDC_TEXLIST)
    {
    int item=lpDrawItemStruct->itemID;
    if (item<0) return;
    texinfo *txt=(texinfo *)m_TexList.GetItemDataPtr(item);
    if (txt==NULL) return;
    char *name=txt->name;
    if (name==NULL)
      {
      name=NULL;
      }
    else
      if (name[0]==0)
        {
        name=(char *)alloca(100);
        LoadString(AfxGetInstanceHandle(),IDS_UNMAPPED,name,90);
        }
    int x=lpDrawItemStruct->rcItem.left;
    int y=lpDrawItemStruct->rcItem.top;
    char desc[128];
    RECT rc=lpDrawItemStruct->rcItem;
    CDC *drwdc=CDC::FromHandle(lpDrawItemStruct->hDC);
    if (lpDrawItemStruct->itemState & ODS_SELECTED)
      drwdc->SelectStockObject(GRAY_BRUSH);
    else
      drwdc->SelectStockObject(WHITE_BRUSH);
    drwdc->SelectStockObject(preview?BLACK_PEN:NULL_PEN);
    drwdc->Rectangle(&lpDrawItemStruct->rcItem);
    drwdc->SetBkMode(TRANSPARENT);
    if (preview)
      {
      CDC *dc=prvtable.GetBitmap((int)txt,name,drwdc,desc,sizeof(desc),!::GetQueueStatus(QS_KEY) && !::GetQueueStatus(QS_MOUSEMOVE));
      if (dc) ::BitBlt(lpDrawItemStruct->hDC,x+2,y+2,48,48,dc->m_hDC,0,0,SRCCOPY );
      else 
        needredraw=true;
      rc.left=x+50;
      drwdc->DrawTextA(desc,strlen(desc),&rc,DT_SINGLELINE|DT_VCENTER|DT_PATH_ELLIPSIS|DT_NOPREFIX|DT_LEFT);
      }
    else
      {
      drwdc->DrawTextA(name,strlen(name),&rc,DT_SINGLELINE|DT_VCENTER|DT_PATH_ELLIPSIS|DT_NOPREFIX|DT_LEFT);
      needredraw=false;
      }
    }
  else  	
    CDialogBar::OnDrawItem(nIDCtl, lpDrawItemStruct);
  }










void CTexList::UpdateLibraryList()
  {
  CString oldname;
  m_LibList.GetWindowText(oldname);
  m_LibList.ResetContent();
  for (texinfo *txt=NULL; txt=(texinfo *)Hash_EnumData(&hash,(void *)txt);)
    {
    if (m_LibList.FindString(-1,txt->lib)==CB_ERR)	  
      m_LibList.AddString(txt->lib);	  
    }
  m_LibList.AddString(MODELLIB);
  m_LibList.AddString(MYFOLDER);
  m_LibList.AddString(SHAREDFOLDER);
  m_LibList.SetWindowText(oldname);
  }










//DEL void CTexList::OnShow() 
//DEL   {
//DEL   UpdateList();	
//DEL   }

//DEL void CTexList::OnMove() 
//DEL   {
//DEL /*  int cnt=m_TexList.GetCount();
//DEL   CString str;
//DEL   for (int i=0;i<cnt;i++) if (m_TexList.GetSel(i))
//DEL 	{
//DEL 	CNamedProperty &np=texlist[m_TexList.GetItemData(i)];
//DEL 	m_LibList.GetWindowText(str);
//DEL 	np.SetValue(str);
//DEL 	}
//DEL   UpdateLibraryList();
//DEL   UpdateList();	*/
//DEL   }

static CString curpath;

void Browse_SetCurrentPath(const char *path)
  {
  curpath=path;
  }










int WINAPI PosBrowseCallbackProc(HWND hwnd,UINT uMsg,LPARAM lParam,LPARAM lpData)
  {
  if (uMsg == BFFM_INITIALIZED)
    {
    ::SendMessage(hwnd,BFFM_SETSELECTION,TRUE,(LPARAM)((LPCSTR)curpath));
    ::SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,(LPARAM)((LPCSTR)curpath));
    } else
    if (uMsg == BFFM_SELCHANGED)
      {
      char buff[_MAX_PATH];
      if (SHGetPathFromIDList((LPITEMIDLIST)lParam,buff))
        {
        CString sPath = buff;
        ::SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,(LPARAM)((LPCSTR)sPath));
        }
      }
  return 0;
  };










void CTexList::OnPath() 
  {
  BROWSEINFO brw;
  char path[MAX_PATH];
  CString s;
  ITEMIDLIST *il;
  
  SaveLibrary();
  memset(&brw,0,sizeof(brw));
  brw.hwndOwner=m_hWnd;
  brw.pidlRoot=NULL;
  strncpy(path,curpath,MAX_PATH);
  brw.pszDisplayName=path;
  ::curpath=curpath;
  s.LoadString(IDS_SELECTTEXPATH);
  brw.lpszTitle=(LPCTSTR)s;
  brw.ulFlags= BIF_RETURNONLYFSDIRS |BIF_STATUSTEXT; 	
  brw.lpfn = (BFFCALLBACK)(PosBrowseCallbackProc);
  il=SHBrowseForFolder( &brw );
  if (il==NULL) return;
  SHGetPathFromIDList(il,path);
  if (path[0]==0) return;
  strncpy(curpath,path,sizeof(curpath));
  curpath[sizeof(curpath)-1]=0;
  prvtable.SetRefPath(curpath);
  Hash_Flush(&hash);
  LoadLibraryList();
  CheckForNewTextures();
  UpdateLibraryList();
  UpdateList();	
  }










void CTexList::Create(CWnd * parent, int menuid)
  {
  CDialogBar::Create(parent,IDD,CBRS_LEFT,menuid);
  SetBarStyle(GetBarStyle()| CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
  EnableDocking(CBRS_ALIGN_ANY);
  CString top;top.LoadString(IDS_TEXLIST);
  SetWindowText(top);
  m_LibList.Attach(::GetDlgItem(m_hWnd,IDC_LIBRARY));
  m_TexList.Attach(::GetDlgItem(m_hWnd,IDC_TEXLIST));
  m_Progress.Attach(::GetDlgItem(m_hWnd,IDC_PROGRESS));
  OnInitDialog();
  cursize=m_sizeDefault;
  LoadBarSize(*this,160,0);
  }










void CTexList::OnDestroy() 
  {
  SaveBarSize(*this);
  CString deflib;
  m_LibList.GetWindowText(deflib);
  theApp.WriteProfileString("TexList","DefLibrary",deflib);
  m_LibList.Detach();	
  m_TexList.Detach();	
  m_Progress.Detach();
  CDialogBar::OnDestroy();
  SaveLibrary();
  }










void CTexList::OnDblclkTexlist() 
  {
  int index=m_TexList.GetCurSel();
  CString name;
  texinfo *info=(texinfo *)m_TexList.GetItemData(index);
  name=info->name;
  char *c=(char *)alloca(prvtable.GetFullNameSize(name));
  prvtable.GetFullName(c,name);
  if (GetKeyState(VK_CONTROL) & 0x80)
    {
    int s=name.GetLength()+1;
    char *c=(char *)alloca(s);
    if (name.GetLength()==0) c[0]=0;else strcpy(c,name);
    comint.Begin(WString(IDS_SELECTBYTEXTURE,c));
    comint.Run(new CISelectByTexture(c));
    doc->UpdateAllViews(NULL);
    int cnt=m_TexList.GetCount();
    m_TexList.SelItemRange(FALSE,0,cnt-1);
    m_TexList.SetSel(index,TRUE);
    }
  else if (frame->GetEditCommand()==ID_SURFACES_BACKGROUNDTEXTURE)
    {
    CObjektiv2View *cur=CObjektiv2View::GetCurrentView();
    char *z=(char *)alloca(strlen(c)+5);
/*    if (config->GetBool(IDS_CFGCONVERTGIFTGA))
      ConvertTexture(c,z,strlen(c)+5);
    else*/
      strcpy(z,name);
    cur->texview->SetTextureName(c,z);
    cur->Invalidate();
    cur->recalc_texview=true;
    cur->SetFocus();
    }
  else
    prew.Create(c);
  }










void CTexList::OnSize(UINT nType, int cx, int cy) 
  {
  CDialogBar::OnSize(nType, cx, cy);
  if ((HWND)m_TexList==NULL) return;
  cx-=cursize.cx;
  cy-=cursize.cy;
  HDWP hdwp=BeginDeferWindowPos(20);
  ::MoveWindowRel(m_TexList,0,0, cx,cy,hdwp);
  ::MoveWindowRel(m_LibList,0,cy,cx,0,hdwp);
  ::MoveWindowRel(m_Progress,0,cy,cx,0,hdwp);
  ::MoveWindowRel(*GetDlgItem(IDC_SHOW),0,cy,0,0,hdwp);
  ::MoveWindowRel(*GetDlgItem(IDC_MOVE),0,cy,0,0,hdwp);
  ::MoveWindowRel(*GetDlgItem(IDC_PATH),0,cy,0,0,hdwp);
  ::MoveWindowRel(*GetDlgItem(IDC_PREVIEW),0,cy,0,0,hdwp);
   EndDeferWindowPos(hdwp);
  cursize.cx+=cx;
  cursize.cy+=cy;
  Invalidate();
  }










void CTexList::GetActiveTextureName(char *name, int size)
  {
  int index=m_TexList.GetCurSel();
  CString p;
  m_TexList.GetText(index,p);
  strncpy(name,p,size);
  name[size-1]=0;  
  }










/*
const char *CTexList::RemovePrefix(const char *name)
  {
  const char *prefix=config->GetString(IDS_CFGTEXTUREPREFIX);
  if (name==NULL) return NULL;
  if (::strnicmp(name,prefix,strlen(prefix))) return name;
  else return name+strlen(prefix);
  }

void CTexList::AddPrefix(const char *src, char *buff, int len)
  {
  if (src==NULL) {buff[0]=0;return;}
  const char *prefix=config->GetString(IDS_CFGTEXTUREPREFIX);
  ::_snprintf(buff,len,"%s%s",prefix,RemovePrefix(src));  
  }
*/
void CTexList::OnSelchangeLibrary() 
  {
  bool update=false;  
  int idx=m_LibList.GetCurSel(); 
  if (idx>=0)
    {
    CString s;
    m_LibList.GetLBText(idx,s);
    m_LibList.SetWindowText(s);
    if (s==MYFOLDER)
      {
      strncpy(curpath,config->GetString(IDS_CFGMYFOLDER),sizeof(curpath));
      update=true;
      }
    if (s==SHAREDFOLDER)
      {
      strncpy(curpath,config->GetString(IDS_CFGPATHFORTEX),sizeof(curpath));
      update=true;
      }
    }
  if (update)
    {
    SaveLibrary();
    m_LibList.SetWindowText(WString(IDS_DEFLIBRARY));
    curpath[sizeof(curpath)-1]=0;
    prvtable.SetRefPath(curpath);
    Hash_Flush(&hash);
    LoadLibraryList();
    CheckForNewTextures();
    UpdateLibraryList();
    prvtable.Flush();
    }
  UpdateList();	
  }










//DEL void CTexList::OnKillfocusLibrary() 
//DEL   {
//DEL   UpdateList();	
//DEL   }

#define CLIPBOARDNAME "****"

void CTexList::OnCut() 
  {
  int cnt=m_TexList.GetCount();
  CString str;
  for (int i=0;i<cnt;i++) if (m_TexList.GetSel(i))
    {
    texinfo *txt=(texinfo *)m_TexList.GetItemDataPtr(i);
    strncpy(txt->lib,CLIPBOARDNAME,sizeof(txt->lib));
    }
  UpdateList();	
  UpdateLibraryList();
  }










void CTexList::OnPaste() 
  {
  CString str;
  m_LibList.GetWindowText(str);
  for (texinfo *txt=NULL; txt=(texinfo *)Hash_EnumData(&hash,(void *)txt);)
    {
    if (strncmp(txt->lib,CLIPBOARDNAME,sizeof(txt->lib))==0)
      {strncpy(txt->lib,str,sizeof(txt->lib));needtosave=true;}
    }
  UpdateList();	
  UpdateLibraryList();
  }










BOOL CTexList::PreTranslateMessage(MSG* msg) 
  {
  if( IsDialogMessage( msg ) )
    return TRUE;
  else
    return CWnd::PreTranslateMessage( msg );
  }










void CTexList::OnEditchangeLibrary() 
  {
  UpdateList();	
  }










void CTexList::UpdateModelTextures(ObjectData *obj, bool flush, bool delay)
  {  
  CString str;
  m_LibList.GetWindowText(str);
  KillTimer(1234);
  if (delay)
    {
    SetTimer(1234,1000,NULL);
    needupdate=obj;
    }
  else
    {
    if (flush) Hash_Flush(&model);
    int cnt=obj->NFaces();
    for (int i=0;i<cnt;i++)
      {
      texinfo nfo;	  
      FaceT fc(obj,i);
      nfo.SetKey(fc.GetTexture());
	  _strlwr(nfo.name);
	  _strlwr(nfo.lib);
      Hash_Add(&model,&nfo);
      }
    needupdate=NULL;
    if (str==MODELLIB)
      {
      UpdateList();	
      UpdateLibraryList();
      prvtable.Flush();
      }
    }
  }










void CTexList::OnPreview() 
  {
  CButton *butt=(CButton *)CButton::FromHandle(*GetDlgItem(IDC_PREVIEW));
  preview=butt->GetCheck()==1;
  MEASUREITEMSTRUCT meas;
  meas.CtlID=IDC_TEXLIST;
  meas.CtlType=ODT_LISTBOX;
  OnMeasureItem(meas.CtlID,&meas);
  m_TexList.SetItemHeight(0,meas.itemHeight);
  m_TexList.Invalidate();
  }










const char * CTexList::EnumModelTextures(void **enumerator)
  {
  void *last=*enumerator;
  last=Hash_EnumData(&model,last);
  *enumerator=last;
  if (last==NULL) return NULL;
  return ((texinfo *)last)->name;
  }










/*void CTexList::ConvertTexture(const char *name, char *outname, int size)
  {  
  const char *b;
  b=::DeriveTexPath(config->GetString(IDS_CFGPATHFORTEX),name);
  if (b==NULL)
    {
    b=strrchr(name,'\\');
    const char *prefix=config->GetString(IDS_CFGTEXTUREPREFIX);
    if (b==NULL) b=(char *)name; else b++;
    if ((int)(strlen(b)+strlen(prefix)+4)>=size) 
      {outname[0]=0;return;}
    sprintf(outname,"%s%s",prefix,b);
    }
  else
    {
    if (strlen(b)+4>=size) 
      {outname[0]=0;return;}
    strcpy(outname,b);
    }
  char *c=strrchr(outname,'.');
  if (_stricmp(c,".pac") && _stricmp(c,".paa"))
    {
    if (_stricmp(c,".gif")==0) strcpy(c,".pac");
    else if (_stricmp(c,".tga")==0) strcpy(c,".paa");
    else 
      {
      AfxMessageBox("Unable to convert this format. Only GIF & TGA can be converted");
      return;
      }
    Pathname pth;    
    pth.SetDirectory(config->GetString(IDS_CFGPATHFORTEX));
    pth.SetFilename(outname);
    char *e=(char *)alloca(strlen(outname)+1);
    strcpy(e,outname);
    //	remove(pth);
    if (::UpdateTextureF==NULL)
      {
      AfxMessageBox("Unable to convert texture, pac2pal error!");
      }
    else
      ::UpdateTextureF(pth,name,-1);
    }
  else
    {
    Pathname pth;
    pth.SetDirectory(config->GetString(IDS_CFGPATHFORTEX));
    pth.SetFilename(outname);	
    if (FileNewer(name,pth))
      CopyFile(name,pth,true);
    }
  }

*/





void CTexList::OnTimer(UINT nIDEvent) 
  {
  if (needupdate) UpdateModelTextures(needupdate, true, false);
  CDialogBar::OnTimer(nIDEvent);
  }










