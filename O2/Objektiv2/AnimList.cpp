// AnimList.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "AnimList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAnimList dialog


CAnimList::CAnimList(CWnd* pParent /*=NULL*/)
  : CDialog(CAnimList::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CAnimList)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CAnimList::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CAnimList)
  // NOTE: the ClassWizard will add DDX and DDV calls here
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CAnimList, CDialog)
  //{{AFX_MSG_MAP(CAnimList)
  // NOTE: the ClassWizard will add message map macros here
  //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CAnimList message handlers
    