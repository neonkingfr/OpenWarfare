// InternalView.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "Objektiv2Doc.h"
#include "Objektiv2View.h"
#include "InternalView.h"
#include "MainFrm.h"
#include "TexList.h"
//#include ".\optima\DdeViewer.hpp"

#include <afxmt.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInternalView



CInternalView::CInternalView()
{
  showhided=false;
  showproxies=false;
  external=false;
  hwndtoopen=NULL;
  error=false;
  showselects=true;
  externalNotifier=CreateWindow("STATIC",O2EXTERNALNOTIFIER,0,0,0,0,0,NULL,NULL,AfxGetInstanceHandle(),NULL);
}


Pathname profile("g3dprof.cfg");

CInternalView::~CInternalView()
{
  g3dResetTables();
  ::DestroyWindow(externalNotifier);
  if (external) 
    {
    CWnd *wnd=FindWindow(NULL,O2EXTERNALNOTIFIER);
//    CloseExternal(wnd==NULL);
    }
}

bool CInternalView::g3dInited=false;
bool CInternalView::g3dOpened=false;
bool CInternalView::g3dDllLoaded=false;



BEGIN_MESSAGE_MAP(CInternalView, CWnd)
  //{{AFX_MSG_MAP(CInternalView)
  ON_WM_DESTROY()
    ON_WM_PAINT()
      ON_WM_CONTEXTMENU()
        ON_WM_MOUSEMOVE()
          ON_WM_SETCURSOR()
            //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    
    /////////////////////////////////////////////////////////////////////////////
    // CInternalView message handlers
    
    void CInternalView::OnDestroy() 
    {
      g3dCloseHardware3D();
      UnsubclassWindow();
      CWnd::OnDestroy();
      g3dInited=false;
      g3dOpened=false;
      g3dFreeDevice();
      InitPlugins();
    }

bool CInternalView::InitViewer()
{
  HWND wnd;
  if (!g3dInited)
  {
    Pathname pth;
    pth.SetDirectory(config->GetString(IDS_CFGOBJEKTIVDLLS));
    pth.SetFilename("");
    memset(&cfg,0,sizeof(cfg));
    cfg.dwSize=sizeof(cfg);
    cfg.szDllPath=pth;
    cfg.szProfile=profile;
    if (::g3dConfigureHardware(&cfg,HWA_LOADDEVICE|HWA_SELECTHARDWARE)) return false;
    g3dInited=true;
  }
  if (!g3dOpened)
  {
    wnd=CreateWindowEx(0,AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS),
    "Internal Viewer",WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_VISIBLE,
    CW_USEDEFAULT,0,cfg.sResolution.xres,cfg.sResolution.yres,*frame,
    NULL,AfxGetInstanceHandle(),NULL);
    if (::g3dInitHardware3D((long)wnd,G3DRES(&cfg)))
    {
      ::g3dFreeDevice();
      g3dInited=false;
      ::DestroyWindow(wnd);
      InitPlugins();
      return false;
    }
    ::SetWindowPos(wnd,HWND_NOTOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
    SubclassWindow(wnd);
    ShowCursor(TRUE);
    g3dOpened=true;
    //SetWindowPos(&wndTopMost,0,0,0,0,SWP_NOSIZE|SWP_NOMOVE);
  }
  ::g3dSetLightTransformation(NULL,G3D_CLEARLT);
  float xs=g3dQuerySettingInt(G3DQ_SCRSIZEX)/2;
  float ys=g3dQuerySettingInt(G3DQ_SCRSIZEY)/2;
  ::g3dSetViewport(xs,ys,xs*1.5f,-xs*1.5f,false);
  TLIGHTDEF lt;
  lt.col_b=lt.col_g=lt.col_r=0.5f;
  CopyVektor(lt.direction,mxVector3(0,0,-1));
  lt.light_flags=LGH_DIRECTLIGHT|LGH_ACTIVE;
  g3dResizeLightTable(2);
  g3dSetLight(0,&lt);  
  g3dSetAmbientLight(0.5f);
  g3dtEnableMipMapGen(G3DTM_LINEAR);
  return true;
}

void CInternalView::SetTexturePath(const char *path)
{
  ::texSetTexturePath((char *)path);
}

void CInternalView::OnPaint() 
{
  CPaintDC dc(this); // device context for painting
  int i=g3dQuerySettingInt(G3DQ_DISPLAYMODE);
  switch(i)
  {
    case G3DQD_FULLSCREEN:break;
    case G3DQD_WINDOWED: g3dSwap(0);break;
  }
}

void CInternalView::UpdateWindow(const ObjectData *odata,CGCamera& cam)
{
  g3dBeginScene(NULL);
  TVERTEXCOLOR col;
  int normal, selected,notshown;
  memset(&col,0,sizeof(col));
  col.pohlc_b=col.pohlc_g=col.pohlc_r=1.0f;
  selected=g3dAddColor(&col);
  col.pohlc_b=col.pohlc_g=0.5f;
  normal=g3dAddColor(&col);
  if (!showselects) normal=selected;
  col.alpha=0.0f;
  col.flags|=TVCOL_ALPHA;
  notshown=g3dAddColor(&col);
  int cnt;
  int i;
  if (odata->GetSelection()->IsEmpty()) normal=selected;
  g3dClearScreen(0.25,0.5,0.25);
  g3dBeginObject(NULL);
  cnt=odata->NPoints();
  for ( i=0;i<cnt;i++)
  {
    const PosT& pos=odata->Point(i);
    g3dAddPosition(mxVector3(pos[0],pos[2],-pos[1]));
  }
  cnt=odata->NNormals();
  for ( i=0;i<cnt;i++)
  {
    const VecT& pos=odata->Normal(i);
    g3dAddNormal(mxVector3(pos[0],pos[2],-pos[1]));
  }
  if (cnt==0) g3dAddNormal(mxVector3(1,0,0));
  cnt=odata->NFaces();
  SetProgressText(IDS_LOADTEXTURES);
  const Selection *hide=odata->GetHidden();
  for (i=0;i<cnt;i++) if (showhided || !hide->FaceSelected(i))
  {
    int col=odata->FaceSelected(i)?selected:normal;
    const FaceT face(odata,i);
    long handle=0;
    if (face.GetTexture().GetLength()) 
    {
      handle=::texFindTexture((char *)(LPCTSTR)face.GetTexture());
      if (!handle) 
      {
        const char *ddname=face.GetTexture();
        handle=::texLoadTexture((char *)ddname);
        if (handle)
        {
          SetCursor(theApp.LoadStandardCursor(IDC_WAIT));
          ::texRegisterTexure((char *)(LPCTSTR)face.GetTexture(),handle);
          SetProgress(i*100/cnt);
        }
        else handle=0;
      }
      if (!handle && config->GetBool(IDS_CFGDONTLOADERR))
      {
        ::texRegisterTexure((char *)(LPCTSTR)face.GetTexture(),1);
      }
      if (handle==1) handle=0;
    }
    g3dAddFace(face.N(),G3D_POLYGON,handle);
    for (int j=0;j<face.N();j++) 
    {
      int col2=col;
      if (!showhided && odata->Point(face.GetPoint(j)).flags & POINT_SPECIAL_HIDDEN) 
        col2=notshown;
      g3dAddFaceVertex(g3dAddVertex(face.GetPoint(j),face.GetNormal(j)),col2,face.GetV(j),face.GetU(j));
    }	  
  }  
  g3dSetLightTransformation(cam.GetView(),G3D_SCENELT);
  HMATRIX mm;
  JednotkovaMatice(mm);
  g3dDrawScene(mm,cam.GetProjection(),0);
  g3dSwap(0);
}

void CInternalView::InitPlugins()
{
  {
    CString s;
    s=config->GetString(IDS_CFGOBJEKTIVDLLS);
    if (s.GetLength()!=0) 
      if (s[s.GetLength()-1]!='\\') s+='\\';
    //Register different format loaders
    g3dRegisterModule(s+"jpgload.dll");
    g3dRegisterModule(s+"gifload.dll");
    g3dRegisterModule(s+"pacload.dll");
    //Init G3D modules;
    g3dInitModules();
    g3dDllLoaded=true;
  }
}

void CInternalView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
  CMenu menu;
  ::LoadPopupMenu(IDR_INTVIEW_POPUP,menu);
  if (showhided) menu.CheckMenuItem(ID_INTVIEW_SHOWHIDDEN,MF_BYCOMMAND|MF_CHECKED);
  menu.EnableMenuItem(ID_INTVIEW_SHOWPROXIES,MF_BYCOMMAND|MF_GRAYED);
  int idc=::TrackPopupMenu(menu,TPM_LEFTALIGN|TPM_TOPALIGN|TPM_RETURNCMD|TPM_NONOTIFY,point.x,point.y,0,*pWnd,NULL);
  switch (idc)
  {
    case ID_INTVIEW_SHOWHIDDEN: showhided=!showhided;break;
    case ID_INTVIEW_RELOAD:texForceReloadAllTextures();break;
    case ID_INTVIEW_SETWINDOW:
    doc->g3dview=doc->g3dview->GetCurrentView();
    break;	  
  }
  doc->UpdateViewer(doc->g3dview);
}

void CInternalView::OnMouseMove(UINT nFlags, CPoint point) 
{
  if (nFlags & MK_LBUTTON)
  {
    CRect rc;
    GetClientRect(&rc);
    float x=((float)point.x/(float)rc.right)*FPI-(FPI/2.0f);
    float y=((float)point.y/(float)rc.bottom)*FPI-(FPI/2.0f);
    HVECTOR v1, v2;
    CopyVektor(v1,mxVector3((float)sin(x),0.0f,-(float)cos(x)));
    CopyVektor(v2,mxVector3(0.0f,-(float)sin(y),-(float)cos(y)));
    SoucetVektoru(v1,v2);
    NormalizeVector(v1);
    TLIGHTDEF *def=::g3dGetLightPtr(0);
    CopyVektor(def->direction,v1);
    doc->UpdateViewer(doc->g3dview);
  }
  else
    CWnd::OnMouseMove(nFlags, point);
}

BOOL CInternalView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
  if (nHitTest==HTCLIENT)
  {
    SetCursor(theApp.LoadCursor(IDI_ZAROVKA));
    return TRUE;
  }	
  return CWnd::OnSetCursor(pWnd, nHitTest, message);
}

/*void CInternalView::StartExternal(const char *pathname, LODObject *obj, float lodbias, bool twomons)
{
  if(error) return;
  external=StartViewer(*frame,obj,pathname,lodbias,twomons);
  if (external==false)
  {
    AfxMessageBox(IDS_UNABLEOPENEXTERNAL,MB_SYSTEMMODAL);
    error=true;
  }
  hwndtoopen=*frame;
}*/
/*
struct UpdateExternalStruct
{
  ObjectData *object;
  HWND hwndtoopen;
  float lodbias;
  bool readed;
  int showselects;
  int frozen;  
  VecT pin;
};

static CMutex mut;

static UINT UpdateExternalThread( LPVOID pParam )
{
  UpdateExternalStruct *ss=(UpdateExternalStruct *)pParam;
  ObjectData *obj=ss->object;
  HWND hwndtoopen=ss->hwndtoopen;
  float lodbias=ss->lodbias;
  int showselects=ss->showselects;
  int frozen=ss->frozen;
  int i;
  VecT pin=ss->pin;
  if (mut.Lock(0)==FALSE) 
  {
    ss->readed=true;
    return 0;
  }
  LODObject lod;
  lod.AddLevel(*obj,1.0f);
  ss->readed=true;
  lod.DeleteLevel(0);
  ObjectData *odata=lod.Level(0);
  char select[256]="data\\select.pac";  
  //  CTexList::AddPrefix("SELECT.PAC",select,sizeof(select));
  if (odata->CountSelectedFaces()!=0)
    if (showselects==1)
    {
      for (int i=0;i<odata->NFaces();i++) if (!odata->FaceSelected(i))
      {
        FaceT fc(odata,i);
        fc.SetTexture(select);
      }
    }
  else if (showselects==2)
    for (i=0;i<odata->NPoints();i++) 
    {
      PosT &pt=odata->Point(i);
      if (!odata->PointSelected(i))
      {
        pt.flags = (pt.flags & ~(POINT_LIGHT_MASK | POINT_USER_MASK)) | POINT_AMBIENT;
      }
      else
      {
        pt.flags = (pt.flags & ~(POINT_LIGHT_MASK | POINT_USER_MASK)) | POINT_NOLIGHT;
        pt.SetPoint(pt-pin);
      }    
    }
  if (!config->GetBool(IDS_CFGDISCENTERPINVIEWER))
    {
    VecT lower=pin,higher=pin;    
    for (i=0;i<obj->NPoints();i++)
      {
      VecT &test=odata->Point(i);
      if (lower[0]>test[0]) lower[0]=test[0];
      if (lower[1]>test[1]) lower[1]=test[1];
      if (lower[2]>test[2]) lower[2]=test[2];
      if (higher[0]<test[0]) higher[0]=test[0];
      if (higher[1]<test[1]) higher[1]=test[1];
      if (higher[2]<test[2]) higher[2]=test[2];
      }
    VecT center((lower[0]+higher[0])*0.5f,(lower[1]+higher[1])*0.5f,(lower[2]+higher[2])*0.5f);
    VecT diff=pin-center;
    VecT newpt;
    if (diff[0]<0) newpt[0]=lower[0]+diff[0]*2; else newpt[0]=higher[0]+diff[0]*2;
    if (diff[1]<0) newpt[1]=lower[1]+diff[1]*2; else newpt[1]=higher[1]+diff[1]*2;
    if (diff[2]<0) newpt[2]=lower[2]+diff[2]*2; else newpt[2]=higher[2]+diff[2]*2;
    odata->NewPoint()->SetPoint(newpt);
    }
  if (frozen>=0 && frozen<odata->NAnimations())
  {
    odata->UseAnimation(frozen);;
    odata->DeleteAllAnimations();
  }
  UpdateViewer(hwndtoopen,&lod,lodbias);
  mut.Unlock();
  return 0;
}

/*bool CInternalView::UpdateExternal(LODObject *lod, float lodbias)
{
  if (!external) return false;
  /*  obj.InsertLevel(lod->Active(),lod->Resolution(lod->ActiveLevel()));
  obj.DeleteLevel(0);
  int res=UpdateViewer(hwndtoopen,&obj,lodbias);
  obj.InsertLevel(new ObjectData(),-9999999999.0f);
  obj.DetachLevel(1);*//*
  UpdateExternalStruct startup;
  startup.hwndtoopen=hwndtoopen;
  startup.lodbias=lodbias;
  startup.object=lod->Active();
  startup.readed=false;
  startup.showselects=this->showselects;
  startup.frozen=doc->frozenanim;
  startup.pin[0]=doc->pin[0];
  startup.pin[1]=doc->pin[1];
  startup.pin[2]=doc->pin[2];
  AfxBeginThread(UpdateExternalThread,&startup,THREAD_PRIORITY_BELOW_NORMAL,0,NULL);
  while (!startup.readed) Sleep(1);
  return true;
}

bool CInternalView::CloseExternal(bool closeapp)
{
  if (!external) return false;
  CloseViewer(hwndtoopen,closeapp);
  external=false;
  hwndtoopen=NULL;
  return true;
}

*/