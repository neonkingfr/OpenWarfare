// AnimPropsToolbar.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "MatLibTreeToolbar.h"
#include ".\matlibtreetoolbar.h"
#include "../ObjektivLib/ObjToolsMatLib.h"
#include "Objektiv2Doc.h"
#include "Objektiv2View.h"
#include "ISelUse.h"
#include "ICreateProxy.h"
#include "MainFrm.h"
#include "dlginputfile.h"
#include <io.h>
#include <El/Interfaces/iScc.hpp>
#include "AnimPropsToolbar.h"

#define METADATA_WALK_CYCLES "WalkCycles"
#define METADATA_ANIM_LENGTH "AnimLength"


CAnimPropsToolbar::CAnimPropsToolbar(CWnd* pParent)
	: CDialogBar()
{
  _objData = NULL;
  _metadataChanged = false;
  _lockUpdate = false;
}

CAnimPropsToolbar::~CAnimPropsToolbar()
{
}


BEGIN_MESSAGE_MAP(CAnimPropsToolbar, CDialogBar)
   ON_WM_DESTROY()
   ON_WM_SIZE()
   ON_BN_CLICKED(IDC_ADD_KEY, OnAddKey)
   ON_BN_CLICKED(IDC_DELETE_KEY, OnDeleteKey)
   ON_EN_CHANGE(IDC_EDIT_WALK_CYCLES, OnMetadataChanged)
   ON_EN_CHANGE(IDC_EDIT_ANIM_LENGTH, OnMetadataChanged)
   ON_EN_KILLFOCUS(IDC_EDIT_WALK_CYCLES, OnEditLostFocus)
   ON_EN_KILLFOCUS(IDC_EDIT_ANIM_LENGTH, OnEditLostFocus)
   ON_NOTIFY(LVN_ITEMCHANGED, IDC_KEY_LIST, OnKeyListChanged)
   ON_WM_VSCROLL()
END_MESSAGE_MAP()


BOOL CAnimPropsToolbar::Create(CWnd * parent, int menuid)
{
  if (CDialogBar::Create(parent,IDD,CBRS_RIGHT,menuid)==FALSE) return FALSE;
  SetBarStyle(GetBarStyle()| CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
  EnableDocking(CBRS_ALIGN_ANY);
  SetWindowText(StrRes[IDS_ANIM_PROPS]);

  m_KeyList.Attach(::GetDlgItem(m_hWnd,IDC_KEY_LIST));
  m_KeyList.InsertColumn(0, WString(IDS_TIMETITLE), LVCFMT_LEFT, 80, 0);
  m_KeyList.InsertColumn(1, WString(IDS_KEY), LVCFMT_LEFT, 50, 1);  
  m_KeyList.InsertColumn(2, WString(IDS_VALUE), LVCFMT_LEFT, 50, 2);  

  m_AddKeyBut.Attach(::GetDlgItem(m_hWnd,IDC_BUTTON_ADD_KEY));
  m_AddKeyBut.EnableWindow(TRUE);
  m_DeleteKeyBut.Attach(::GetDlgItem(m_hWnd,IDC_BUTTON_DELETE_KEY));
  m_DeleteKeyBut.EnableWindow(TRUE);

  m_AnimLenEdit.Attach(::GetDlgItem(m_hWnd,IDC_EDIT_ANIM_LENGTH));
  m_WalkCyclesEdit.Attach(::GetDlgItem(m_hWnd,IDC_EDIT_WALK_CYCLES));
  m_KeyTimeEdit.Attach(::GetDlgItem(m_hWnd,IDC_EDIT_TIME));
  m_KeyNameEdit.Attach(::GetDlgItem(m_hWnd,IDC_EDIT_KEY));
  m_KeyValueEdit.Attach(::GetDlgItem(m_hWnd,IDC_EDIT_VALUE));  

  m_timeSlider.Attach(::GetDlgItem(m_hWnd,IDC_TIME_SLIDER));
  m_timeSlider.SetRangeMin(0,TRUE);
  m_timeSlider.SetRangeMax(1000,TRUE);
  m_timeSlider.SetTicFreq(1000);
  UpdateTime();

  return TRUE;
}

void CAnimPropsToolbar::OnDestroy()
{
  m_KeyList.Detach();
  m_AddKeyBut.Detach();
  m_DeleteKeyBut.Detach();
  m_AnimLenEdit.Detach();
  m_WalkCyclesEdit.Detach();
  m_KeyTimeEdit.Detach();
  m_KeyNameEdit.Detach();
  m_KeyValueEdit.Detach();
  m_timeSlider.Detach();
  CDialogBar::OnDestroy();
}



void CAnimPropsToolbar::UpdateData(ObjectData *odata, float curlod)
{
  _objData = odata;
  UpdateMetadata();
  UpdateKeyList();
}

void CAnimPropsToolbar::SetTimeFromAnimation()
{
  if (_lockUpdate)
    return;
  if (_objData == NULL)
    return;
  if (doc->curanim >= 0 && doc->curanim < _objData->NAnimations())
  {
    SetTime(_objData->GetAnimation(doc->curanim)->GetTime());
  }
}

void CAnimPropsToolbar::SetTime(float time)
{
  if (time >= 0 && time <= 1.0f)
  {
    int pos = time * 1000;
    m_timeSlider.SetPos(pos);
    UpdateTime();
  }
}

void CAnimPropsToolbar::OnAddKey() 
{
  if (_objData == NULL)
    return;
  AnimationKeyStone keyStone;
  CString text;
  float time;
  // get time
  m_KeyTimeEdit.GetWindowText(text);
  const char *str = text.GetString();
  if (sscanf(str, "%f", &time) != 1)
    return;
  keyStone.time = time;
  // get key
  m_KeyNameEdit.GetWindowText(text);
  if (text.GetLength() == 0)
    return;
  keyStone.key = text.GetString();
  // get value  
  m_KeyValueEdit.GetWindowText(text);
  keyStone.value = text.GetString();
  // add new keyStone
  _objData->AddKeyStoneSorted(keyStone);
  UpdateKeyList();
}

void CAnimPropsToolbar::OnDeleteKey() 
{
  if (_objData == NULL)
    return;
  int index = GetActiveItem();
  if (index >= 0 && index < _objData->GetKeyStoneCount())
  {
    _objData->DeleteKeyStone(index);
    UpdateKeyList();
  }
}

void CAnimPropsToolbar::OnMetadataChanged()
{
  if (_lockUpdate)
    return;
  _metadataChanged = true;
}

void CAnimPropsToolbar::OnEditLostFocus()
{
  if (!_metadataChanged)
    return;
  _metadataChanged = false;

  if (_objData == NULL)
    return;

  AnimationMetadata metadata;
  CString text;

  float val;
  // Set animation length
  m_AnimLenEdit.GetWindowText(text);
  if (sscanf(text.GetString(), "%f", &val) == 1)
  {
    text.Format("%g", val);
    metadata.name = METADATA_ANIM_LENGTH;
    metadata.value = text.GetString();
    _objData->SetMetadata(metadata);
  }
  // Set walk cycles
  m_WalkCyclesEdit.GetWindowText(text);
  if (sscanf(text.GetString(), "%f", &val) == 1)
  {
    text.Format("%g", val);
    metadata.name = METADATA_WALK_CYCLES;
    metadata.value = text.GetString();
    _objData->SetMetadata(metadata);
  }
  UpdateMetadata();
}

void CAnimPropsToolbar::OnKeyListChanged(NMHDR* pNMHDR, LRESULT* pResult) 
{
  if (_lockUpdate)
    return;
  int index = GetActiveItem();
  if (index >= 0 && index < _objData->GetKeyStoneCount())
  {
    SetTime(_objData->GetKeyStone(index)->time);
    SetAnimationTime();
  }
}

int CAnimPropsToolbar::GetActiveItem()
{
  return m_KeyList.GetNextItem(-1,LVNI_FOCUSED);    
}

void CAnimPropsToolbar::UpdateMetadata()
{
  _lockUpdate = true;
  m_AnimLenEdit.SetWindowText("");
  m_WalkCyclesEdit.SetWindowText("");
  _lockUpdate = false;

  if (_objData == NULL)
    return;

  const AnimationMetadata *metadata;
  _lockUpdate = true;
  metadata = _objData->GetMetadata(METADATA_ANIM_LENGTH);
  if (metadata)
    m_AnimLenEdit.SetWindowText(metadata->value);
  metadata = _objData->GetMetadata(METADATA_WALK_CYCLES);
  if (metadata)
    m_WalkCyclesEdit.SetWindowText(metadata->value);
  _lockUpdate = false;
}

void CAnimPropsToolbar::UpdateKeyList()
{
  m_KeyList.DeleteAllItems();

  if (_objData == NULL)
    return;

  int count = _objData->GetKeyStoneCount();
  for (int i = 0; i < count; ++i)
  {
    AnimationKeyStone *keyStone = _objData->GetKeyStone(i);
    WString str;
    LVITEM it;
    str.Format("%g", keyStone->time);
    it.mask = LVIF_TEXT | LVIF_PARAM;
    it.iItem = i;
    it.iSubItem = 0;
    it.lParam = i;
    it.pszText = (char *)((const char *)str);
    int idx = m_KeyList.InsertItem(&it);
    str = keyStone->key;
    m_KeyList.SetItemText(idx, 1, str);	
    str = keyStone->value;
    m_KeyList.SetItemText(idx, 2, str);	
  }
}


void CAnimPropsToolbar::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
  UpdateTime();
  SetAnimationTime();
}


void CAnimPropsToolbar::SetAnimationTime()
{
  if (_objData == NULL)
    return;

  // Find nearest animation
  float time = m_timeSlider.GetPos() / 1000.0f;
  float prevTime = 0;
  int index = 0;
  for (; index < _objData->NAnimations(); ++index)
  {
    float curTime = _objData->GetAnimation(index)->GetTime();
    if (curTime >= time)
    {
      if (index > 0 && (curTime+prevTime)/2 > time)
        --index;
      break;
    }
    prevTime = curTime;
  }
  if (index >= _objData->NAnimations())
    index = _objData->NAnimations() - 1;

  _lockUpdate = true;
  // +1 because first animation is base pose
  frame->m_wndAnimListBar.SetActiveIndex(index);
  _lockUpdate = false;
}

void CAnimPropsToolbar::UpdateTime()
{
  float time = m_timeSlider.GetPos() / 1000.0f;
  CString str;
  str.Format("%g", time);
  m_KeyTimeEdit.SetWindowText(str);
}



void CAnimPropsToolbar::OnSize(UINT nType, int cx, int cy)
{
  if ((HWND)m_KeyList==NULL) 
    return;
  RECT rc;
  // list
  ::GetWindowRect(m_KeyList, &rc);
  ScreenToClient(&rc);
  rc.right = cx - 45;
  rc.bottom = cy - 5;
  m_KeyList.MoveWindow(&rc, 1);
  // slider
  ::GetWindowRect(m_timeSlider, &rc);
  ScreenToClient(&rc);
  rc.left = cx - 40;
  rc.right = cx - 5;
  rc.bottom = cy - 5;
  m_timeSlider.MoveWindow(&rc, 1);
  Invalidate();
  CDialogBar::OnSize(nType, cx, cy);
}


BOOL CAnimPropsToolbar::PreTranslateMessage(MSG* msg) 
{
  if( IsDialogMessage( msg ) )
    return TRUE;
  else
    return CWnd::PreTranslateMessage( msg );
}


void CAnimPropsToolbar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
}
