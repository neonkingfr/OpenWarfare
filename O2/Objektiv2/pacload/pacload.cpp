

// Declarations added here will be included at the top of the .HPP file

#include <iostream>
#include <fstream>
#include <strstream>
using namespace std;
#include <windows.h>
#include "pachead.h"


// Declarations added here will be included at the top of the .HPP file
#include "..\optima\pal2pac.hpp"

typedef unsigned short Pixel;


class Texture
  {
  
  private:
    // add your private instance data here
    
    TextureData _data;
    
  public:
    DWORD Data( int x, int y ) const
      {
      return _data.GetPixel(x,y);
      }
    int Width() const 
      {return _data.W();}
    int Height() const 
      {return _data.H();}
    
  public:
    Texture();
    
  public:
    ~Texture();
    
  public:
    int Load( const char *name );
    
  public:
    bool Error();
    
  };

//--------------------------------------------------

// Code added here will be included at the top of the .CPP file
//#include "global.hpp"

//#include "pal2pacDL.cpp"

//  Include definitions for resources.
//#include "WRes.h"

Texture::Texture()
  {
  }

//--------------------------------------------------

Texture::~Texture()
  {
  }

//--------------------------------------------------

int Texture::Load( const char *name )
  {
  // load palette and finest mip-map
  _data.Load(name);
  if (_data.Data() ) return 0;
  return -1;
  }

//--------------------------------------------------

bool Texture::Error()
  {
  return _data.Data()==NULL;
  }

//--------------------------------------------------

static Texture *pic;

int ReadPac(const char *name)
  {
  pic=new Texture();
  return pic->Load(name);
  }

//--------------------------------------------------

int GetPacXSize()
  {
  return pic->Width();
  }

//--------------------------------------------------

int GetPacYSize()
  {
  return pic->Height();
  }

//--------------------------------------------------

unsigned long GetPacPixel(int x,int y)
  {
  return pic->Data(x,y);
  }

//--------------------------------------------------

void CleanPac()
  {
  delete pic;
  }

//--------------------------------------------------

char *GetPacFormat()
  {
  return "ARGB8888";
  }

//--------------------------------------------------

int InitPal2PacDll(const char *path)
  {
  dbgprintf("%s",path);
  return InitPal2Pac(path)==false;
  }

//--------------------------------------------------

