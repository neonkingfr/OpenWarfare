#include <colman.h>
#include <g3dmodul.h>
#include <windows.h>
#include <string.h>
#include <setjmp.h>
#include "pachead.h"
#include <malloc.h>

TUNIPICTURE *(*NextChain)(HANDLE f); 
//pointer to next filter in chain
TUNIPICTURE *(*CreateUni)(int xsize, int ysize, char *format, int align, int pallen);
//pointer to function for creatin uni-bitmaps
char *(*GetImportName)(void);
//pointer to function for getting imported filename
void (*gfree)(void *);
//pointer to free function in G3D memory manager


TUNIPICTURE *LoadPac(HANDLE f);


//This function is called whe G3D is being inicialized.
//It must init all plug-in varibles to the correct values.
//It also must obtain all necessary callbacks.
char g3dplgInitHardware(TG3DINITSTRUCT *initinfo)
  {  
  DBG(memcpy(&NextChain,initinfo->ImportUni,sizeof(void *)));
  //Obtain pointer to next filter in chain
  *(initinfo->ImportUni)=LoadPac;
  //Add self to filter chain
  CreateUni=initinfo->CreateUni;
  //Obtain pointer to cmCreateUni function
  gfree=initinfo->free;
  //Obtain pointer to free function
  GetImportName=initinfo->GetImportName;
  //Obtain pointer to GetImportName function 
  //* it is accessable only in plug-in module, not in G3D app
  return G3DPE_OK;
  //report OK
  }

//--------------------------------------------------

//This function is called whe G3D is being inicialized.
//It must remove self from filter chain (writting pointer to next filter in chain)
char g3dplgDoneHardware(TG3DINITSTRUCT *initinfo)
  {
  memcpy(initinfo->ImportUni,&NextChain,sizeof(void *));  //do this
  return G3DPE_OK;
  }

//--------------------------------------------------

//Function is called when plugin is registred in application
//It must fillup necessary pointers in structure loadinfo.
char g3dInitPlugin(TG3DPLUGINIT *loadinfo)
  {  
  char *c=(char *)alloca(strlen(loadinfo->pathname));
  char *d;
  DBG(strcpy(c,loadinfo->pathname));
  d=strrchr(c,'\\');
  if (d!=NULL) *(d+1)=0;
  if (InitPal2PacDll(c)) return G3DPE_FAILED;
  DBG(loadinfo->g3dplgInitHardware=g3dplgInitHardware);
  DBG(loadinfo->g3dplgDoneHardware=g3dplgDoneHardware);
  return G3DPE_OK;
  }

//--------------------------------------------------

TUNIPICTURE *__cdecl LoadPac(HANDLE f)
  { 
  TUNIPICTURE *pic=NULL;
  const char *c=GetImportName();
  char *p=strrchr(c,'.');
  dbgprintf("PAC Load: %s",c);
  if (p==NULL || (stricmp(p,".paa") && stricmp(p,".pac"))) return NextChain(f);    
  if (ReadPac(c)==0)
    {
    int xs=GetPacXSize();
    int ys=GetPacYSize();
    pic=CreateUni(GetPacXSize(),GetPacYSize(),GetPacFormat(),1,0);
    if (pic!=NULL)
      {
      int x,y;
      unsigned long *pc=(unsigned long *)cmGetData(pic);
      for (y=0;y<ys;y++)
        for (x=0;x<xs;x++)
          {
          *pc++=GetPacPixel(x,y);
          }
      }
    CleanPac();
    }
  return pic;
  }

//--------------------------------------------------

