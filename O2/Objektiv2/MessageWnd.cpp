// MessageWnd.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "MessageWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMessageWnd dialog


static CMessageWnd msg;

CMessageWnd::CMessageWnd(CWnd* pParent /*=NULL*/)
  : CDialog(CMessageWnd::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CMessageWnd)
    text = _T("");
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CMessageWnd::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CMessageWnd)
  DDX_Text(pDX, IDC_TEXT, text);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CMessageWnd, CDialog)
  //{{AFX_MSG_MAP(CMessageWnd)
  ON_WM_TIMER()
    //}}AFX_MSG_MAP
    END_MESSAGE_MAP()
      
      /////////////////////////////////////////////////////////////////////////////
      // CMessageWnd message handlers
      
      
      BOOL CMessageWnd::OnInitDialog() 
        {
        CDialog::OnInitDialog();
        SetTimer(1,10,NULL);
        curtime=GetTickCount();
        CRect orgrect;
        GetWindowRect(&orgrect);
        orgsize=orgrect.Size();
        SetWindowPos(NULL,0,0,orgsize.cx,0,SWP_NOZORDER|SWP_NOMOVE|SWP_NOACTIVATE);
        // TODO: Add extra initialization here
        ShowWindow(SW_SHOW);
        return TRUE;  // return TRUE unless you set the focus to a control
        // EXCEPTION: OCX Property Pages should return FALSE
        }

//--------------------------------------------------

void CMessageWnd::OnTimer(UINT nIDEvent) 
  {
  DWORD pp=GetTickCount();
  int a=(pp-curtime);
  a=__min(a,(int)timetoshow-a);
  if (a>500) a=500;
  if (a<0) DestroyWindow();
  else SetWindowPos(NULL,0,0,orgsize.cx,orgsize.cy*a/500,SWP_NOZORDER|SWP_NOMOVE|SWP_NOACTIVATE);
  CDialog::OnTimer(nIDEvent);
  }

//--------------------------------------------------

void CMessageWnd::OnCancel()
  {
  DestroyWindow();
  }

//--------------------------------------------------

void CMessageWnd::ShowMessage(const char *text,DWORD timetoshow)
  {
  if ((HWND)msg==NULL) msg.Create(msg.IDD);
  msg.ShowText(text);
  msg.timetoshow=timetoshow;
  }

//--------------------------------------------------

void CMessageWnd::ShowText(const char *text)
  {
  this->text=text;
  UpdateData(false);
  curtime=GetTickCount();
  }

//--------------------------------------------------

