// SelectionList.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "Objektiv2Doc.h"
#include "SelectionList.h"
#include "ComInt.h"
#include "Weights.h"
#include "ProxyNameDlg.h"
#include "SelectionNameDlg.h"
#include "ISeparateToProxy.h"
#include "geVector4.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelectionList dialog


CSelectionList::CSelectionList()
: CDialogBar()
{
  //{{AFX_DATA_INIT(CSelectionList)
  // NOTE: the ClassWizard will add member initialization here
  //}}AFX_DATA_INIT
  //{{AFX_DATA_MAP(CSelectionList)
  // NOTE: the ClassWizard will add DDX and DDV calls here
  //}}AFX_DATA_MAP
}

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CSelectionList, CDialogBar)
  //{{AFX_MSG_MAP(CSelectionList)
  ON_WM_SIZE()
  ON_WM_DESTROY()
  ON_COMMAND(ID_SLCT_RENAME, OnSlctRename)
  ON_NOTIFY(NM_RCLICK, IDC_LIST, OnRclickList)
  ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclkList)
  ON_NOTIFY(LVN_BEGINLABELEDIT, IDC_LIST, OnBeginlabeleditList)
  ON_COMMAND(ID_SLCT_NEW, OnSlctNew)
  ON_COMMAND(ID_SLCT_DELETE, OnSlctDelete)
  ON_COMMAND(ID_SLCT_REDEFINE, OnSlctRedefine)
  ON_COMMAND(ID_SLCT_WEIGHTS, OnSlctWeights)
  ON_COMMAND(ID_SLCT_PROXY, OnDefineproxy)
  ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LIST, OnEndlabeleditList)
  ON_NOTIFY(NM_RELEASEDCAPTURE , IDC_LIST, OnReleaseList)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST, OnItemchangedList)
  ON_COMMAND(ID_SLCT_SETGROUP, OnSlctSetgroup)
  ON_COMMAND(ID_SLCT_REDEFNORM, OnSlctRedefnorm)
  //}}AFX_MSG_MAP
  ON_COMMAND_EX(ID_SLCT_MOVESELECTIONTOOBJECT, OnSlctMoveselectiontoobject)
  ON_COMMAND_EX(ID_SLCT_MOVESELECTIONTOOBJECT_AL, OnSlctMoveselectiontoobject)

  ON_COMMAND(ID_SLCT_EXTRACTPROXY, OnSlctExtractproxy)
  ON_COMMAND(ID_SLCT_EXTRACTPROXY_AL, OnSlctExtractproxyAl)
  ON_COMMAND(ID_SLCT_DELEMPTY, OnSlctDelempty)
  ON_COMMAND_RANGE(100,1000,OnPluginPopupCommand)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelectionList message handlers

void CSelectionList::Create(CWnd *parent, int menuid)
{
  CDialogBar::Create(parent,IDD,CBRS_RIGHT,menuid);
  SetBarStyle(GetBarStyle()| CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
  EnableDocking(CBRS_ALIGN_ANY);
  CString top;top.LoadString(IDS_NAMEDSELECTIONS);
  SetWindowText(top);
  notify=parent;
  list.Attach(::GetDlgItem(m_hWnd,IDC_LIST));
  list.SetImageList(&shrilist,LVSIL_SMALL);
  CString text;
  list.InsertColumn(0,"",LVCFMT_LEFT,100,0);
  LoadBarSize(*this,160,0);
  labelediting=false;
  selscan=false;
}

//--------------------------------------------------

void CSelectionList::OnSize(UINT nType, int cx, int cy) 
{
  CDialogBar::OnSize(nType, cx, cy);
  if ((HWND)list) list.MoveWindow(5,5,cx-10,cy-10);
}

//--------------------------------------------------

void CSelectionList::OnDestroy() 
{
  list.Detach();
  SaveBarSize(*this);
  CDialogBar::OnDestroy();	
}

//--------------------------------------------------

void CSelectionList::UpdateListOfSel(LODObject *lodobject)
{
  CString temp;
  list.DeleteAllItems();
  lod=lodobject;
  ObjectData *odata=lod->Active();
  int handle=selgrp.GetGroupHandle(lodobject->Resolution(lodobject->ActiveLevel()));
  for (int i=0;i<MAX_NAMED_SEL;i++)
  {
    NamedSelection *nm=odata->GetNamedSel(i);
    if (nm!=NULL)
    {
      LVITEM it;
      it.mask=LVIF_TEXT  |LVIF_IMAGE |LVIF_PARAM ;
      it.iItem=i;
      it.iSubItem=0;
      it.iImage=selgrp.GetSelMarked(handle,i)?ILST_MARKEDSELECTION:ILST_SELECTION;
      it.pszText=(char *)nm->Name();
      if (strncmp(nm->Name(),StrRes[IDS_PROXYNAME],strlen(StrRes[IDS_PROXYNAME]))==0 && strrchr(nm->Name(),'\\')!=NULL)
      {
        temp.Format("%s:...\\%s",StrRes[IDS_PROXYNAME],Pathname::GetNameFromPath(nm->Name()));
        it.pszText=(char *)((LPCTSTR)temp);
      }
      it.lParam=i;
      list.InsertItem(&it);
    }
  }  
}

//--------------------------------------------------

void CSelectionList::OnSlctRename() 
{
  int item=list.GetNextItem(-1,LVNI_FOCUSED);
  list.EditLabel(item);
}

//--------------------------------------------------

static bool IsProxy(const NamedSelection *nsel, char *proxyname, int size, int *id)
{
  if (nsel->NFaces()==1 && nsel->NPoints()==3)
  {
    WString w(IDS_PROXYNAME);
    if (strncmp(nsel->Name(),w,w.GetLength())==0)
    {
      if (proxyname!=NULL)
      {
        const char *c=nsel->Name();
        int i=w.GetLength();
        while (size>1 && c[i]!='.' && c[i]!=0)		  
        {*proxyname++=c[i++];size--;}
        *proxyname=0;
        if (id)
        {
          if (c[i++]=='.')           
            *id=atoi(c+i);          
          else
            *id=0;
        }
      }
      return true;
    }
  }
  return false;
}

//--------------------------------------------------

void CSelectionList::OnRclickList(NMHDR* pNMHDR, LRESULT* pResult) 
{
  int item=list.GetNextItem(-1,LVNI_FOCUSED);
  CMenu cxmenu;  
  cxmenu.LoadMenu(IDR_SELMENU_POPUP);
  CMenu &menu=*cxmenu.GetSubMenu(0);  
  if (item==-1)
  {
    menu.EnableMenuItem(ID_SLCT_DELETE,MF_BYCOMMAND|MF_GRAYED);
    menu.EnableMenuItem(ID_SLCT_RENAME,MF_BYCOMMAND|MF_GRAYED);
    menu.EnableMenuItem(ID_SLCT_REDEFINE,MF_BYCOMMAND|MF_GRAYED);
    menu.EnableMenuItem(ID_SLCT_PROXY,MF_BYCOMMAND|MF_GRAYED);
    menu.EnableMenuItem(ID_SLCT_MOVESELECTIONTOOBJECT,MF_BYCOMMAND|MF_GRAYED);
    menu.EnableMenuItem(ID_SLCT_MOVESELECTIONTOOBJECT_AL,MF_BYCOMMAND|MF_GRAYED);
    menu.EnableMenuItem(ID_SLCT_EXTRACTPROXY,MF_BYCOMMAND|MF_GRAYED);
    menu.EnableMenuItem(ID_SLCT_EXTRACTPROXY_AL,MF_BYCOMMAND|MF_GRAYED);
    menu.EnableMenuItem(ID_SLCT_REDEFNORM,MF_BYCOMMAND|MF_GRAYED);
    menu.EnableMenuItem(ID_SLCT_SETGROUP,MF_BYCOMMAND|MF_GRAYED);
  }
  else
  {
    NamedSelection *sel=lod->Active()->GetNamedSel(GetNamedSelectionIndex());
    if (sel!=NULL)
    {
      if (sel->NFaces()!=1 && sel->NPoints()!=3)
        menu.EnableMenuItem(ID_SLCT_PROXY,MF_BYCOMMAND|MF_GRAYED);
      else
        if (IsProxy(sel,NULL,0,0))
        {
          menu.CheckMenuItem(ID_SLCT_PROXY,MF_BYCOMMAND|MF_CHECKED);
        }
        if (IsProxy(sel,NULL,0,0))
        {
          menu.EnableMenuItem(ID_SLCT_MOVESELECTIONTOOBJECT,MF_BYCOMMAND|MF_GRAYED);
          menu.EnableMenuItem(ID_SLCT_MOVESELECTIONTOOBJECT_AL,MF_BYCOMMAND|MF_GRAYED);
          if (lod->NLevels()==1)menu.EnableMenuItem(ID_SLCT_EXTRACTPROXY_AL,MF_BYCOMMAND|MF_GRAYED);
        }
        else
        {
          menu.EnableMenuItem(ID_SLCT_EXTRACTPROXY,MF_BYCOMMAND|MF_GRAYED);
          menu.EnableMenuItem(ID_SLCT_EXTRACTPROXY_AL,MF_BYCOMMAND|MF_GRAYED);
          if (lod->NLevels()==1)menu.EnableMenuItem(ID_SLCT_MOVESELECTIONTOOBJECT_AL,MF_BYCOMMAND|MF_GRAYED);
        }

    }
  }
  theApp.PluginPopupHook(IPluginGeneralPluginSide::PMSelectionList,menu,*this);
  CPoint pt;
  GetCursorPos(&pt);
  menu.TrackPopupMenu(TPM_LEFTALIGN|TPM_LEFTBUTTON|TPM_RIGHTBUTTON,pt.x,pt.y,this);
}

//--------------------------------------------------

#include "ISelUse.h"
#include ".\selectionlist.h"

//--------------------------------------------------

int CSelectionList::GetNamedSelectionIndex()
{
  int item=list.GetNextItem(-1,LVNI_FOCUSED);
  if (item==-1) return -1;
  LVITEM it;
  it.mask=LVIF_PARAM;
  it.iItem=item;
  it.iSubItem=0;
  list.GetItem(&it);
  return it.lParam;
}

//--------------------------------------------------

void CSelectionList::OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult) 
{ /*
  int item=GetNamedSelectionIndex();
  const char *c=(lod->Active())->NamedSel(item);
  bool ctrl=(GetKeyState(VK_CONTROL) & 0x80)!=0;
  bool shift=(GetKeyState(VK_SHIFT)& 0x80)!=0;
  SelMode mode;
  if (ctrl)
  if (shift) mode=SelSub;else mode=SelAdd;
  else if (!ctrl)
  if (shift) mode=SelAnd;else mode=SelSet;

  comint.Begin(WString(IDS_SELECTUSING,c));
  comint.Run(new CISelUse(c,mode));
  notify->SendMessage(WM_COMMAND,ID_UPDATE_ALL_VIEWS);
  *pResult = 0;*/
}

//--------------------------------------------------


//--------------------------------------------------

CICommand *CIRenameNamedSel_Create(istream& str)
{
  char text1[256],text2[256];
  FLoadString(str,text1,sizeof(text1));
  FLoadString(str,text2,sizeof(text2));
  return new CIRenameNamedSel(text1,text2);
}

//--------------------------------------------------

void CSelectionList::OnEndlabeleditList(NMHDR* pNMHDR, LRESULT* pResult) 
{
  LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
  // TODO: Add your control notification handler code here
  labelediting=false;
  LVITEM *it=&pDispInfo->item;
  int idx=it->lParam;
  const char *text=it->pszText;	
  *pResult = 0;
  if (text==NULL) return;
  comint.Begin(WString(IDS_RENAMESELECTION,text));
  if (comint.Run(new CIRenameNamedSel(lod->Active()->NamedSel(idx),WString(text)))==0)
    list.SetItem(it);
}

//--------------------------------------------------

BOOL CSelectionList::PreTranslateMessage(MSG* pMsg) 
{
  // TODO: Add your specialized code here and/or call the base class
  if (labelediting && IsDialogMessage(pMsg)) return TRUE;
  else
    return CDialogBar::PreTranslateMessage(pMsg);
}

//--------------------------------------------------

void CSelectionList::OnBeginlabeleditList(NMHDR* pNMHDR, LRESULT* pResult) 
{
  CSelectionNameDlg dlg;
  labelediting=true;
  LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
  LVITEM *it=&pDispInfo->item;
  CRect rc;
  int idx=it->lParam;
  if (IsProxy(lod->Active()->GetNamedSel(idx),NULL,0,0))
  {
    OnDefineproxy();
    *pResult = 1;
    return;
  }
  dlg.vList=list.GetItemText(it->iItem,0);
  dlg.lod=this->lod;    
  list.GetItemRect(it->iItem,&rc,LVIR_LABEL);
  list.ClientToScreen(&rc);
  dlg.showpoint.x=rc.left-1;
  dlg.showpoint.y=rc.top-1;
  if (dlg.DoModal()==IDOK)
  {
    comint.Begin(WString(IDS_RENAMESELECTION,dlg.vList));
    if (comint.Run(new CIRenameNamedSel(lod->Active()->NamedSel(idx),WString(dlg.vList)))==0)
      list.SetItemText(it->iItem,0,dlg.vList);
  }
  labelediting=false;
  *pResult = 1;
  list.Invalidate(FALSE);
}

//--------------------------------------------------

class CISaveSelection: public CICommand
{
  WString name;
public:
  CISaveSelection(const char *n): name(n), CICommand() 
  {}
  virtual int Execute(LODObject *obj)
  {
    ObjectData *odata=obj->Active();
    return !odata->SaveNamedSel(name);
  }
  virtual void Save(ostream& str)
  {
    FSaveString(str,name);
  }
};

//--------------------------------------------------

CICommand *CISaveSelection_Create(istream& str)
{
  char text[256];
  FLoadString(str,text,sizeof(text));
  return new CISaveSelection(text);
}

//--------------------------------------------------

void CSelectionList::OnSlctNew() 
{
  WString w=FindAprNewName();
  comint.Begin(WString(IDS_SAVESELECTION));
  if (comint.Run(new CISaveSelection(w))==0) 
  {
    ObjectData *odata=lod->Active();
    LVITEM it;
    it.mask=LVIF_TEXT  |LVIF_IMAGE |LVIF_PARAM ;
    it.iItem=0;
    it.iSubItem=0;
    it.iImage=ILST_SELECTION;
    it.pszText=(char *)((LPCTSTR)w);
    it.lParam=odata->FindNamedSel(w);;
    int idx=list.InsertItem(&it);
    list.SetItemState(idx,LVIS_FOCUSED|LVIS_SELECTED,LVIS_FOCUSED|LVIS_SELECTED);  
    OnSlctRename();  
  }
}

//--------------------------------------------------

WString CSelectionList::FindAprNewName()
{
  WString name(IDS_NEWSELECTIONNAME);
  ObjectData *odata=lod->Active();
  int i=0;
  while (odata->FindNamedSel(name)!=-1)
  {
    i++;
    char buff[30];
    name.LoadString(IDS_NEWSELECTIONNAME);
    name+=" "+WString(_itoa(i,buff,10));
  }
  return name;
}

//--------------------------------------------------


CICommand *CIDeleteSelection_Create(istream& str)
{
  char text[256];
  FLoadString(str,text,sizeof(text));
  return new CIDeleteSelection(text);
}

//--------------------------------------------------

void CSelectionList::OnSlctDelete() 
{
  int i=list.GetNextItem(-1,LVNI_SELECTED);
  bool first=true;
  while (i!=-1)
  {
    CString s=lod->Active()->GetNamedSel(list.GetItemData(i))->Name();
    if (first) comint.Begin(WString(IDS_DELETESELECTION,s));
    if (comint.Run(new CIDeleteSelection(s))!=0) break;
    first=false;
    i=list.GetNextItem(i,LVNI_SELECTED);
  }
  UpdateListOfSel(lod);
}

//--------------------------------------------------

void CSelectionList::OnSlctRedefine() 
{
  int idx=GetNamedSelectionIndex();
  if (idx==-1) return;
  //ASSERT(lod->Active()->GetNamedSel);
  NamedSelection * namedSelection = ((lod->Active())->GetNamedSel(idx));
  if (!namedSelection) 
    return;
  CString w(namedSelection->Name());
  comint.Begin(WString(IDS_REDEFINESELECTION,w));
  if (comint.Run(new CIDeleteSelection(w))==0)
    comint.Run(new CISaveSelection(w));
  UpdateListOfSel(lod);
}

//--------------------------------------------------

void CSelectionList::OnSlctWeights() 
{
  CWeights dlg;
  dlg.obj=lod->Active();
  dlg.DoModal();
  UpdateListOfSel(lod);
}

//--------------------------------------------------

void CSelectionList::OnDefineproxy() 
{
  ObjectData *obj=lod->Active();
  const NamedSelection *nsel=obj->GetNamedSel(GetNamedSelectionIndex());
  if (!nsel) return;
  char name[256]="";
  int id;
  if (!IsProxy(nsel,name,sizeof(name),&id))
  {
    if (nsel->NFaces()!=1 || nsel->NPoints()!=3) return;
    name[0]=0;
  }
  CProxyNameDlg dlg;
opakuj:
  dlg.Name=name;
  dlg.vProxyId.Format("%d",id);
  if (dlg.DoModal()==IDOK)
  {
    CString selection;
    CString hlp;
    hlp.Format("%s%%s.%%03d",WString(IDS_PROXYNAME));
    if (dlg.vProxyId=="")
    {
      int proxyIndex=1;
      for(;;)
      { 
        selection.Format(hlp,(const char *)dlg.Name,proxyIndex);

        if( obj->FindNamedSel(selection)<0 ) break;
        proxyIndex++;
      }
    }
    else
    {
        selection.Format(hlp,(const char *)dlg.Name,atoi(dlg.vProxyId));
        if( obj->FindNamedSel(selection)>=0 )
        {
          AfxMessageBox(IDS_NELZEPREJMENOVATPROXY);
          goto opakuj;
        }
    }
    comint.Begin(WString(IDS_RENAMESELECTION,dlg.Name));
    if (comint.Run(new CISetProxy(nsel->Name(),selection))==0)
      UpdateListOfSel(lod);
  }

}

//--------------------------------------------------

void CSelectionList::OnReleaseList(NMHDR* pNMHDR, LRESULT* pResult) 
{
  // TODO: Add your control notification handler code here
  if (selscan)
  {
    selscan=false;
    int i=list.GetNextItem(-1,LVNI_SELECTED);
    bool first=true;
    while (i!=-1)
    {
      int data=list.GetItemData(i);
      if (data>=0 && data<MAX_NAMED_SEL) 
      {
        NamedSelection *sel=lod->Active()->GetNamedSel(data);
        if (sel)
        {
          CString s=sel->Name();
          if (first) comint.Begin(WString(IDS_SELECTUSING,s));
          SelMode mode;
          if (first) mode=SelSet;else mode=SelAdd;
          if (comint.Run(new CISelUse(s,mode))!=0) break;
          first=false;
        }
      }
      i=list.GetNextItem(i,LVNI_SELECTED);
    }
  }  
  *pResult = 0;
  doc->UpdateAllViews(NULL);
  doc->UpdateAllToolbars(2);
}

//--------------------------------------------------

void CSelectionList::OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult) 
{	
  NMLISTVIEW  *phdn = (NMLISTVIEW *) pNMHDR;
  if (GetKeyState(VK_RBUTTON) & 0x80) return;  	  
  // TODO: Add your control notification handler code here
  if ((phdn->uNewState & LVIS_SELECTED) || (phdn->uOldState & LVIS_SELECTED))
  {
    selscan=true;
    if (GetCapture()!=&list) 
      OnReleaseList(pNMHDR,pResult);
  }
  *pResult = 0;
}

//--------------------------------------------------

/*bool DefineProxy(ObjectData *obj, const char *oldname)
{
CString name;
CProxyNameDlg dlg;
if (dlg.DoModal()==IDOK)
{
int proxyIndex=1;
WString selection;
name.Format("%s%%s.%%02d",WString(IDS_PROXYNAME));
for(;;)
{ 
selection.Sprintf(name,(const char *)dlg.Name,proxyIndex);

if( obj->FindNamedSel(selection)<0 ) break;
proxyIndex++;
}
if (comint.Run(new CISetProxy(oldname,selection))==0)
{
doc->UpdateAllToolbars(1);
return true;
}
}
return false;	
}
*/
//--------------------------------------------------

void CSelectionList::OnSlctSetgroup() 
{
  POSITION pos;
  selgrp.ClearGroup(lod->Resolution(lod->ActiveLevel()));
  int handle=selgrp.CreateGroupHandle(lod->Resolution(lod->ActiveLevel()));
  pos=list.GetFirstSelectedItemPosition();
  while (pos)
  {
    int p=list.GetNextSelectedItem(pos);
    int i=list.GetItemData(p);
    selgrp.SetSelMark(handle,i);
  }
  UpdateListOfSel(lod);
}

//--------------------------------------------------

class CINormalizeSelections:public CICommand
{
  int selid;
  bool marks[MAX_NAMED_SEL];
public:
  CINormalizeSelections(bool *mrk, int sid) 
  {recordable=false,memcpy(marks,mrk,sizeof(marks));selid=sid;}
  int Execute(LODObject *obj);
};

//--------------------------------------------------

int CINormalizeSelections::Execute(LODObject *lod)
{
  ObjectData *obj=lod->Active();
  CSelectionGroup::NormalizeSelections(obj,marks,selid);
  return 0;
}

//--------------------------------------------------

void CSelectionList::OnSlctRedefnorm() 
{ 
  POSITION pos;
  pos=list.GetFirstSelectedItemPosition();
  if (pos)
  {
    int handle=selgrp.GetGroupHandle(lod->Resolution(lod->ActiveLevel()));
    if (handle==-1)
      handle=selgrp.CreateGroupHandle(lod->Resolution(lod->ActiveLevel()));
    int p=list.GetNextSelectedItem(pos);
    int i=list.GetItemData(p);
    CString w(((lod->Active())->GetNamedSel(i))->Name());
    comint.Begin(WString(IDS_REDEFINESELECTION,w));
    if (comint.Run(new CIDeleteSelection(w))==0)
    {
      selgrp.SetSelMark(handle,i,false);
      comint.Run(new CISaveSelection(w));	  	
      i=lod->Active()->FindNamedSel(w);
      selgrp.SetSelMark(handle,i,true);
      bool *marks=selgrp.GetMarks(handle);
      comint.Run(new CINormalizeSelections(marks,i));
    }
    UpdateListOfSel(lod);
  }
}

//--------------------------------------------------


BOOL CSelectionList::OnSlctMoveselectiontoobject(UINT command)
{
  if (doc->filename.IsNull()) if (doc->AskSave()==false) return FALSE;
  POSITION pos=list.GetFirstSelectedItemPosition();
  while (pos)
  {
    geVector4 pin(doc->pin);
    int p=list.GetNextSelectedItem(pos);
    int i=list.GetItemData(p);
    CString w(((lod->Active())->GetNamedSel(i))->Name());
    if (MoveSelectionToProxyEx(lod,command==ID_SLCT_MOVESELECTIONTOOBJECT_AL,w,doc->GetDocumentName(),doc->pinlock?&pin:NULL)==false)
    {
      AfxMessageBox(IDS_UNABLEMOVETOPROXY);
    }
  }
  doc->UpdateAllViews(NULL);
  doc->UpdateAllToolbars(0);
  return TRUE;
}


void CSelectionList::OnSlctExtractproxy()
{
  POSITION pos=list.GetFirstSelectedItemPosition();
  if (pos) comint.Begin(WString(IDS_EXTRACTPROXY));
  while (pos)
  {
    int p=list.GetNextSelectedItem(pos);
    int i=list.GetItemData(p);
    CString w(((lod->Active())->GetNamedSel(i))->Name());
    bool succ=ExtractProxy(lod->Active(), w, lod->Resolution(lod->ActiveLevel()));
    if (succ==false)
    {
      AfxMessageBox(IDS_EXTRACT_PROXY_ERROR);
      comint.DoUndo();
      doc->UpdateAllViews(NULL);
      doc->UpdateAllToolbars(0);
      return;
    }
  }
  doc->UpdateAllViews(NULL);
  doc->UpdateAllToolbars(0);
  return;
}

#include "LodList.h"

void CSelectionList::OnSlctExtractproxyAl()
{
  POSITION pos=list.GetFirstSelectedItemPosition();
  if (pos) comint.Begin(WString(IDS_EXTRACTPROXY));
  while (pos)
  {
    int p=list.GetNextSelectedItem(pos);
    int i=list.GetItemData(p);
    CString w(((lod->Active())->GetNamedSel(i))->Name());
    if (ExtractProxyAllLods(lod,w)==false)
    {
      AfxMessageBox(IDS_EXTRACT_PROXY_ERROR);
      comint.DoUndo();
      doc->UpdateAllViews(NULL);
      doc->UpdateAllToolbars(0);
      return;
    }
  }
  doc->UpdateAllViews(NULL);
  doc->UpdateAllToolbars(0);
}

class ISelDelEmpty:public CICommand
{
public:
  virtual int Execute(LODObject *obj)
  {
    obj->Active()->CleanEmptySelections();
    return 0;
  }
  virtual int Tag() {return CITAG_DELETEEMPTYSELS;}
};

CICommand *CIDelEmptySels_Create(istream &str)
{
  return new ISelDelEmpty;
}

void CSelectionList::OnSlctDelempty()
{
  comint.Begin(WString(IDS_DELETEEMPTYSELS));
  comint.Run(new ISelDelEmpty);
  UpdateListOfSel(lod);
}

void CSelectionList::OnPluginPopupCommand(UINT cmd)
{
  theApp.HookMenuCommand(cmd);
}