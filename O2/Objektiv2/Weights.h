#if !defined(AFX_WEIGHTS_H__7ED0A456_1747_4F66_A373_77F38FFE34E1__INCLUDED_)
#define AFX_WEIGHTS_H__7ED0A456_1747_4F66_A373_77F38FFE34E1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Weights.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CWeights dialog

class CWeights : public CDialog
  {
  // Construction
  public:
    bool CheckValues();
    CWeights(CWnd* pParent = NULL);   // standard constructor
    ObjectData *obj;
    // Dialog Data
    //{{AFX_DATA(CWeights)
    enum 
      { IDD = IDD_WEIGHTS };
    CListCtrl	List;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CWeights)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CWeights)
    virtual BOOL OnInitDialog();
    afx_msg void OnEndlabeleditList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult);
    virtual void OnOK();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WEIGHTS_H__7ED0A456_1747_4F66_A373_77F38FFE34E1__INCLUDED_)
