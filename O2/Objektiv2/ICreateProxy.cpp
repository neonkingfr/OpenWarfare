// ICreateProxy.cpp: implementation of the CICreateProxy class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "objektiv2.h"
#include "ICreateProxy.h"
#include "../ObjektivLib/ObjToolProxy.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CICreateProxy::CICreateProxy(istream &str)
  {
  datardp(str,pin);
  char buff[256];
  ::FLoadString(str,buff,256);
  name=buff;
  }

//--------------------------------------------------

CICreateProxy::~CICreateProxy()
  {
  
  }

//--------------------------------------------------

int CICreateProxy::Execute(LODObject *lo)
  {
  ObjectData *obj=lo->Active();
  Matrix4 mx(pin[0][0],pin[1][0],pin[2][0],pin[3][0],pin[0][1],pin[1][1],pin[2][1],pin[3][1],pin[0][2],pin[1][2],pin[2][2],pin[3][2]);
  return obj->GetTool<ObjToolProxy>().CreateProxy(name,mx)?0:1;
  }

//--------------------------------------------------

void CICreateProxy::Save(ostream &str)
  {
  datawrp(str,pin);
  FSaveString(str,name);
  }

//--------------------------------------------------

CICommand *CICreateProxy_Create(istream &str)
  {
  return new CICreateProxy(str);
  }

//--------------------------------------------------

