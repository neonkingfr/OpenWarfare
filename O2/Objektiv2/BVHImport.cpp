// BVHImport.cpp: implementation of the CBVHImport class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "objektiv2.h"
#include "BVHImport.h"
#include "..\ObjektivLib\BIANMimport.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBVHImport::CBVHImport()
  {
  nodecnt=0;
  axis[0]='x';
  axis[1]='y';
  axis[2]='z';
  axisdir[0]=1;
  axisdir[1]=1;
  axisdir[2]=1;
  callback=NULL;
  ztop=false;
  }

//--------------------------------------------------

static CBVHImport::LexInfo lxinfo[]=
  {
    {"HIERARCHY",CBVHImport::lx_HIERARCHY},
    {"ROOT",CBVHImport::lx_ROOT},
    {"OFFSET",CBVHImport::lx_OFFSET},
    {"CHANNELS",CBVHImport::lx_CHANNELS},
    {"JOINT",CBVHImport::lx_JOINT},
    {"Xposition",CBVHImport::lx_Xposition},
    {"Yposition",CBVHImport::lx_Yposition},
    {"Zposition",CBVHImport::lx_Zposition},
    {"Zrotation",CBVHImport::lx_Zrotation},
    {"Xrotation",CBVHImport::lx_Xrotation},
    {"Yrotation",CBVHImport::lx_Yrotation},
    {"MOTION",CBVHImport::lx_MOTION},
    {"Frames:",CBVHImport::lx_Frames},
    {"Frame",CBVHImport::lx_Frame},
    {"Time:",CBVHImport::lx_Time},
    {"End",CBVHImport::lx_EndSite},
    {"Site",CBVHImport::lx_EndSiteC},
    {"{",CBVHImport::lx_Begin},
    {"}",CBVHImport::lx_End}
  };

//--------------------------------------------------

CBVHImport::~CBVHImport()
  {
  
  }

//--------------------------------------------------

void CBVHImport::AddRotation(char osa, float value, HMATRIX mx)
  {
  HMATRIX p;   
  HMATRIX ss;
  CopyMatice(ss,mx);
  value*=axisdir[osa];
  value=value*FPI/180.0f;
  switch (axis[osa])
    {
    case 'x': RotaceX(p,value);break;
    case 'y': RotaceY(p,-value);break;
    case 'z': RotaceZ(p,value);break;
    }
  SoucinMatic(p,ss,mx);  
  }

//--------------------------------------------------

void CBVHImport::AddTranslate(char osa, float value, HMATRIX mx)
  {
  value*=axisdir[osa];
  switch (axis[osa])
    {
    case 'x': mx[3][0]+=value;break;
    case 'y': mx[3][1]+=value;break;
    case 'z': mx[3][2]+=value;break;
    }
  }

//--------------------------------------------------

void CBVHImport::ReadNext()
  {
  char buffer[256];
  ws(*data);
  int p;
  int i;
  for (i=0;i<255;i++)
    {
    p=data->get();
    if (isspace(p)) break;
    buffer[i]=p;
    }
  buffer[i]=0;
  if (i==0 && p==EOF) 
    {
    symb.type=lx_Eof;
    }
  else
    {
    symb.text=buffer;
    symb.type=lx_Text;
    for (int j=0;j<sizeof(lxinfo)/sizeof(lxinfo[0]);j++)
      {
      if (_stricmp(buffer,lxinfo[j].text)==0) 
        {symb.type=lxinfo[j].value;break;}
      }
    if (symb.type==lx_Text)
      {
      if (sscanf(symb.text,"%f",&symb.number)==1) symb.type=lx_Number;
      }
    }  
  }

//--------------------------------------------------

void CBVHImport::ReadTillPar()
  {
  char buffer[1024];
  data->get(buffer,1024,'{');  
  symb.type=lx_Text;
  int p=strlen(buffer);
  while (p>0 && isspace(buffer[p-1])) buffer[--p]=0;
  symb.text=buffer;  
  }

//--------------------------------------------------

char *CBVHImport::GetError(int code)
  {
  switch (code)
    {
    case 0: return "No errors found";
    case -1: return "Excepting HIERARCHY keyword";
    case -2: return "Excepting MOTION keyword";
    case -3: return "Excepting ROOT keyword";
    case -4: return "Excepting begin of the block ('{')";
    case -5: return "Excepting end of the block ('}')";
    case -6: return "Excepting keyword: OFFSET, CHANNELS or JOINT";
    case -7: return "Excepting float number";
    case -8: return "Integer out of range";
    case -9: return "Unknown channel name or list is shorter then specified";
    case -10: return "Internal error: Callback is set to NULL, unable to load motion.";
    case -11: return "Exceptiong Frames: or Frame Time: or motion data";
    case -12: return "Exceptiong 'Time:' word";
    case -13: return "Exceptiong 'Site' word";
    case -14: return "Need Z top (not an error)";
    }
  return "User defined error (raised from callback function)";
  }

//--------------------------------------------------

int CBVHImport::ReadFile()
  {
  ReadNext();
  if (symb.type!=lx_HIERARCHY) return -1;
  ReadNext();
  int err;  
  err=ReadHierarchy();
  if (err) return err;
  if (symb.type!=lx_MOTION) return -2;
  ReadNext();
  err=ReadMotion();
  if (err) return err;  
  return 0;
  }

//--------------------------------------------------

int CBVHImport::ReadHierarchy()
  {
  if (symb.type!=lx_ROOT) return -3;
  ReadTillPar();
  int err=ReadHierarchyItem(-1);  
  if (err) return err;
  return 0;
  }

//--------------------------------------------------

int CBVHImport::ReadHierarchyItem(int parent,bool term)
  {
  int cur=nodecnt;
  nodecnt++;
  SBVHHierarchyItem &hitem=nodes[cur];
  JednotkovaMatice(hitem.curmx);
  JednotkovaMatice(hitem.globmx);
  memset(hitem.offset,0,sizeof(hitem.offset));
  hitem.parent=parent;
  hitem.tcount=0;  
  hitem.name=symb.text;
  hitem.ustate=true;
  hitem.term=term;
  hitem.scale=1.0f;
  ReadNext();
  if (symb.type!=lx_Begin) return -4;
  ReadNext();
  while (symb.type!=lx_End)
    {
    int err;
    switch (symb.type)
      {
      case lx_OFFSET: ReadNext(); err=ReadOffset(hitem);break;
      case lx_CHANNELS: ReadNext(); err=ReadChannels(hitem);break;
      case lx_JOINT: ReadTillPar(); err=ReadHierarchyItem(cur);break;
      case lx_EndSite: ReadNext();
      if (symb.type!=lx_EndSiteC) return -13;
      symb.text.Format("Terminator %d",data->tellg());
      err=ReadHierarchyItem(cur,true);
      break;
      default: return -6;
      }
    if (err) return err;
    }
  ReadNext();
  return 0;
  }

//--------------------------------------------------

int CBVHImport::ReadOffset(SBVHHierarchyItem &itm)
  {
  if (symb.type!=lx_Number) return -7;
  itm.offset[0]=symb.number;
  ReadNext();
  if (symb.type!=lx_Number) return -7;
  itm.offset[1]=symb.number;
  ReadNext();
  if (symb.type!=lx_Number) return -7;
  itm.offset[2]=symb.number;
  ReadNext();
  itm.offset[3]=1.0f;
  return 0;
  }

//--------------------------------------------------

int CBVHImport::ReadChannels(SBVHHierarchyItem &itm)
  {
  if (symb.type!=lx_Number) return -7;
  if (symb.number<1 || symb.number>9) return -8;
  itm.tcount=(int)symb.number;
  ReadNext();
  for (int i=0;i<itm.tcount;i++)
    {
    switch (symb.type)
      {
      case lx_Xposition: itm.tlist[i]=SBVHHierarchyItem::tx;break;
      case lx_Yposition: itm.tlist[i]=SBVHHierarchyItem::ty;break;
      case lx_Zposition: itm.tlist[i]=SBVHHierarchyItem::tz;break;
      case lx_Xrotation: itm.tlist[i]=SBVHHierarchyItem::rx;break;
      case lx_Yrotation: itm.tlist[i]=SBVHHierarchyItem::ry;break;
      case lx_Zrotation: itm.tlist[i]=SBVHHierarchyItem::rz;break;
      default: return -9;
      }
    ReadNext();
    }
  return 0;
  }

//--------------------------------------------------

int CBVHImport::ReadMotion()
  {
  Frames=0x7FFFFFFF;
  FrameTime=1.0f/25.0f;
  if (callback==NULL) return -10;
  while (symb.type!=lx_Number)
    {
    if (symb.type==lx_Frames)
      {
      ReadNext();
      if (symb.type!=lx_Number) return -7;
      Frames=(int)symb.number;
      }
    else
      if (symb.type==lx_Frame)
        {
        ReadNext();
        if (symb.type!=lx_Time) return -12;
        ReadNext();
        if (symb.type!=lx_Number) return -7;
        FrameTime=symb.number;
        }
    else
      return -11;
    ReadNext();
    }
  DWORD flags;
  int err=callback(nodes,nodecnt,-2,Frames,-1.0f,ccontext,flags);
  if (flags & BVHFLAGS_ZTOP) ztop=true;
  if (flags & BVHFLAGS_INVERTX) invertx=true;
  if (flags & BVHFLAGS_INVERTY) inverty=true;
  if (flags & BVHFLAGS_INVERTZ) invertz=true;
  if (err==-14) 
    {ztop=true;err=0;}
  if (err) return err;
  RecalcHierarchy(true);
  err=callback(nodes,nodecnt,-1,Frames,-1.0f,ccontext,flags);
  if (err) return err;
  for (int i=0;i<Frames;i++)
    {
    for (int j=0;j<nodecnt;j++)
      {
      for (int k=0;k<nodes[j].tcount;k++)
        {
        if (symb.type!=lx_Number) return -7;
        nodes[j].tvals[k]=symb.number;
        ReadNext();
        }
      }
    RecalcHierarchy(false);
    int err=callback(nodes,nodecnt,i,Frames,i*FrameTime,ccontext,flags);
    if (err) return err;
    if (symb.type==lx_Eof) break;
    }
  return 0;
  }

//--------------------------------------------------

void CBVHImport::RecalcHierarchy(bool static_phase)
  {
  for (int i=0;i<nodecnt;i++)
    {
    SBVHHierarchyItem &hitem=nodes[i];
    Translace(hitem.curmx,hitem.offset[0]*hitem.scale,hitem.offset[1]*hitem.scale,hitem.offset[2]*hitem.scale);
    if (!static_phase)
      for (int j=0;j<hitem.tcount;j++)
        {
        switch (hitem.tlist[j])
          {
          case SBVHHierarchyItem::tx: AddTranslate(0,hitem.tvals[j]*hitem.scale,hitem.curmx);break;
          case SBVHHierarchyItem::ty: AddTranslate(1,hitem.tvals[j]*hitem.scale,hitem.curmx);break;
          case SBVHHierarchyItem::tz: AddTranslate(2,hitem.tvals[j]*hitem.scale,hitem.curmx);break;
          case SBVHHierarchyItem::rz: AddRotation(2,hitem.tvals[j],hitem.curmx);break;
          case SBVHHierarchyItem::ry: AddRotation(1,hitem.tvals[j],hitem.curmx);break;
          case SBVHHierarchyItem::rx: AddRotation(0,hitem.tvals[j],hitem.curmx);break;
          }
        }
    if (hitem.parent==-1) 
      {
      if (ztop)
        {
        HMATRIX mm;
        RotaceX(mm,-FPI/2);
        SoucinMatic(hitem.curmx,mm,hitem.globmx);
        }
      else
        CopyMatice(hitem.globmx,hitem.curmx);
      }
    else SoucinMatic(hitem.curmx,nodes[hitem.parent].globmx,hitem.globmx);
    }
  }

//--------------------------------------------------

int CBVHImport::Parse(int *errline)
  {
  int err=ReadFile();
  if (err && errline)
    {
    int p=1;
    int pos=data->tellg();
    data->clear();
    data->seekg(0,ios::beg);
    data->ignore(0x7fffffff,'\n');
    while (pos>data->tellg()) 
      {
      p++;
      data->ignore(0x7fffffff,'\n');
      data->clear();
      }
    *errline=p;
    }
  return err;
  }

//--------------------------------------------------


//--------------------------------------------------

#include "CreatePrimDlg.h"
#include "BVHImportDlg.h"

int BVHImportModel(SBVHHierarchyItem *hlist, int count, int frame, int total, float ftime, void *context, DWORD &flags)
  {
  static CBVHImportDlg dlg;
  if (frame==-2)
    {
    dlg.itemlist=hlist;
    dlg.itemcount=count;
    if (dlg.DoModal()==IDCANCEL) return -255;
    flags=0;
    if (dlg.ztop) flags |=BVHFLAGS_ZTOP;
    if (dlg.invertX) flags |=BVHFLAGS_INVERTX;
    if (dlg.invertY) flags |=BVHFLAGS_INVERTY;
    if (dlg.invertZ) flags |=BVHFLAGS_INVERTZ;
    return 0;
    }
  ObjectData &obj=*(ObjectData *)context;
  int idx=obj.AnimationIndex(-0.5f);
  AnimationPhase *cur;
  if (idx==-1)  //prepare base animation phase 
    {
    AnimationPhase a(&obj);
    a.SetTime(-0.5f);
    obj.AddAnimation(a);
    idx=obj.AnimationIndex(-0.5f);
    obj.RedefineAnimation(idx);
    }
  if (frame==-1) //prepare base phase data
    {
    float sz=0;
    for (int k=0;k<count;k++)
      {
      sz+=ModulVektoru(hlist[k].curmx[3]);	  
      }
    sz/=count*5;
    obj.UseAnimation(idx);
    obj.ClearSelection();
    for (int i=0;i<count;i++) if (hlist[i].ustate)
      {
      SBVHHierarchyItem &h=hlist[i];
      obj.DeleteNamedSel(h.name);
      bool bone=false;
      int p=obj.NPoints();
      int f=obj.NFaces();
      for (int j=0;j<count;j++) if (hlist[j].parent==i)
        {
        bone=true;
        CBIANMImport::CreateBonePrimitive(obj,Vector3(hlist[j].offset[0],hlist[j].offset[1],hlist[j].offset[2]),ModulVektoru(hlist[j].curmx[3]));
        obj.ClearSelection();
        }
      if (!bone)
        {
        CCreatePrimDlg dlg;
        dlg.align=0;
        dlg.create_type=IDD_CREATE_PRIM_BOX;
        dlg.numX=dlg.numY=dlg.numZ=1;
        dlg.sizeX=dlg.sizeY=dlg.sizeZ=sz;
        HMATRIX mm;
        JednotkovaMatice(mm);
        dlg.CreatePrimitive(&obj,mm);
        }
      obj.SaveNamedSel(h.name);
      Selection *sel=obj.GetNamedSel(h.name);
      while (p<obj.NPoints()) sel->PointSelect(p++);
      while (f<obj.NFaces()) sel->FaceSelect(f++);
      }
    obj.RedefineAnimation(idx);
    cur=obj.GetAnimation(idx);
    obj.UseAnimation(-1);
    }
  else
    {
    if (frame+1>=obj.NAnimations())
      {
      AnimationPhase a(&obj);
      a.SetTime(obj.GetAnimation(obj.NAnimations()-1)->GetTime()+1.0f);
      obj.AddAnimation(a);
      }	
    cur=obj.GetAnimation(frame+1);
    }
  float xmod=flags & BVHFLAGS_INVERTX?-1:1;
  float ymod=flags & BVHFLAGS_INVERTY?-1:1;
  float zmod=flags & BVHFLAGS_INVERTZ?-1:1;
  for (int i=0;i<count;i++) if (hlist[i].ustate)
    {
    SBVHHierarchyItem &h=hlist[i];
    Selection *sel=obj.GetNamedSel(h.name);
    for (int j=0;j<obj.NPoints();j++) 
      if (sel->PointSelected(j))
        {
        PosT ps=obj.Point(j);
        HVECTOR vx;
        TransformVector(h.globmx,mxVector3(ps[0],ps[1],ps[2]),vx);		
        ps[0]=vx[0]*xmod;
        ps[1]=vx[1]*ymod;
        ps[2]=vx[2]*zmod;
        (*cur)[j]=ps;
        }
    }
  return 0;
  }

//--------------------------------------------------




