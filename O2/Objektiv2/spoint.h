// SPoint.h: interface for the SPoint class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SPOINT_H__FED10226_55E1_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_SPOINT_H__FED10226_55E1_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#define SPOINT_ENUM(i) for (int i=0;i<e;i++)
#define SPOINT_ENUMF(i,f) for (int i=f;i<e;i++)
#define SPOINT_BINOP(op) 	SPoint ret;\
							SPOINT_ENUM(i) ret.vect[i]=op;\
							return ret

template <class T, int e> struct SPoint  
  {
  T vect[e];
  SPoint() 
    {}
  SPoint(T *init) 
    {SPOINT_ENUM(i) vect[i]=*init++;}
  SPoint(T a,...) 
    {
    va_list list;
    vect[0]=a;
    va_start(list,a);
    SPOINT_ENUMF(i,1) vect[i]=va_arg(list,T);
    }
  SPoint<T,e> operator+(const SPoint<T,e>& other) const 
    {SPOINT_BINOP(vect[i]+other.vect[i]);}
  SPoint<T,e> operator-(const SPoint<T,e>& other) const 
    {SPOINT_BINOP(vect[i]-other.vect[i]);}
  SPoint<T,e> operator*(T& other) const 
    {SPOINT_BINOP(vect[i]*other);}
  SPoint<T,e> operator/(T& other) const 
    {SPOINT_BINOP(vect[i]/other);}
  SPoint<T,e>& operator+=(SPoint<T,e>& other) 
    {SPOINT_ENUM(i) vect[i]+=other.vect[i];return *this;}
  SPoint<T,e>& operator-=(SPoint<T,e>& other) 
    {SPOINT_ENUM(i) vect[i]-=other.vect[i];return *this;}
  SPoint<T,e>& operator*=(T& other) 
    {SPOINT_ENUM(i) vect[i]*=other;return *this;}
  SPoint<T,e>& operator/=(T& other) 
    {SPOINT_ENUM(i) vect[i]/=other;return *this;}  
  SPoint<T,e>& operator=(SPoint<T,e>& other) 
    {SPOINT_ENUM(i) vect[i]=other.vect[i];return *this;}
  bool operator==(SPoint<T,e>& other) 
    {bool same=true;SPOINT_ENUM(i) same=same && (vect[i]==other.vect[i]);return same;}  
  friend ostream& operator<<(ostream& str, const SPoint<T,e>& p);
  friend istream& operator>>(istream& str, const SPoint<T,e>& p);
  };

//--------------------------------------------------

#include "spoint.tli"

struct SPoint2DF: public SPoint<float,2>
  {
  operator LPHVECTOR() 
    {return vect;}
  SPoint2DF() 
    {}
  SPoint2DF(float *init):SPoint<float,2>(init) 
    {}
  SPoint2DF(float a,...):SPoint<float,2>(&a) 
    {} 
  SPoint2DF(SPoint<float,2>& ff):SPoint<float,2>(ff.vect) 
    {}
  SPoint2DF(CPoint& pt) 
    {vect[0]=pt.x;vect[1]=pt.y;}
  float x() const 
    {return vect[0];}
  float y() const 
    {return vect[1];}
  float& x() 
    {return vect[0];}
  float& y() 
    {return vect[1];}
  SPoint2DF& operator=(SPoint2DF& other) 
    {int e=2;SPOINT_ENUM(i) vect[i]=other.vect[i];return *this;}
  SPoint2DF& operator=(SPoint<float,2>& other) 
    {int e=2;SPOINT_ENUM(i) vect[i]=other.vect[i];return *this;}
  };

//--------------------------------------------------

struct SPoint3DF: public SPoint<float,3>
  {
  operator LPHVECTOR() 
    {return vect;}
  SPoint3DF() 
    {}
  SPoint3DF(float *init):SPoint<float,3>(init) 
    {}
  SPoint3DF(float a,...):SPoint<float,3>(&a) 
    {} 
  };

//--------------------------------------------------

struct SPoint4DF: public SPoint<float,4>
  {
  operator LPHVECTOR () 
    {return vect;}
  SPoint4DF() 
    {}
  SPoint4DF(float *init):SPoint<float,4>(init) 
    {}
  SPoint4DF(float a,float b, float c):SPoint<float,4>(a,b,c,1.0f) 
    {} 
  SPoint4DF(float a,...):SPoint<float,4>(&a) 
    {} 
  };

//--------------------------------------------------

#endif // !defined(AFX_SPOINT_H__FED10226_55E1_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
