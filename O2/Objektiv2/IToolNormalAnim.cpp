// IToolNormalAnim.cpp: implementation of the CIToolNormalAnim class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "objektiv2.h"
#include "IToolNormalAnim.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CIToolNormalAnim::CIToolNormalAnim(float reflod, const char *refsel, bool selonly):
refsel(refsel),reflod(reflod),selonly(selonly)
  {
  
  }

//--------------------------------------------------

CIToolNormalAnim::~CIToolNormalAnim()
  {
  
  }

//--------------------------------------------------

CIToolNormalAnim::CIToolNormalAnim(istream &str)
  {
  char buff[256];
  FLoadString(str,buff,256);
  refsel=buff;
  datard(str,reflod);
  datard(str,selonly);
  }

//--------------------------------------------------

void GetMatrixFromFace(ObjectData &obj, FaceT &fc,HMATRIX mm)
  {
  PosT *ps[3];
  int i;
  for (i=0;i<3;i++) ps[i]=&obj.Point(fc.GetPoint(i));
  HVECTOR v1,v2,v3;    
  CopyVektor(v1,mxVector3((*ps[0])[0],(*ps[0])[1],(*ps[0])[2]));
  CopyVektor(v2,mxVector3((*ps[1])[0],(*ps[1])[1],(*ps[1])[2]));
  CopyVektor(v3,mxVector3((*ps[2])[0],(*ps[2])[1],(*ps[2])[2]));
  RozdilVektoru(v2,v1);
  RozdilVektoru(v3,v1);
  HVECTOR vn;
  VektorSoucin(v2,v3,vn);
  NormalizeVector(vn);
  VektorSoucin(v2,vn,v3);
  NormalizeVector(v3);
  NormalizeVector(v2);
  NulovaMatice(mm);
  mm[0][0]=v2[0];
  mm[0][1]=v2[1];
  mm[0][2]=v2[2];
  mm[1][0]=v3[0];
  mm[1][1]=v3[1];
  mm[1][2]=v3[2];
  mm[2][0]=vn[0];
  mm[2][1]=vn[1];
  mm[2][2]=vn[2];
  mm[3][0]=v1[0];
  mm[3][1]=v1[1];
  mm[3][2]=v1[2];
  mm[3][3]=1;
  }

//--------------------------------------------------

int CIToolNormalAnim::Execute(LODObject *lod)
  {
  ObjectData &obj=*lod->Active();
  ObjectData &refobj=*lod->Level(lod->FindLevel(reflod));
  int i;
  int nfaces=refobj.NFaces();
  const Selection *sel=refobj.GetNamedSel(refsel);
  if (sel==NULL) return -1;
  for (i=0;i<nfaces;i++) if (sel->FaceSelected(i)) break;
  if (i==nfaces) return -1;
  FaceT reffc(refobj,i);
  HMATRIX m,im,tm;
  GetMatrixFromFace(refobj,reffc,m);
  nfaces=obj.NFaces();
  sel=obj.GetNamedSel(refsel);
  if (sel==NULL) return -1;
  for (i=0;i<nfaces;i++) if (sel->FaceSelected(i)) break;
  if (i==nfaces) return -1;
  FaceT fc(obj,i);
  int nanim=obj.NAnimations();
  int curanim=obj.CurrentAnimation();
  int npoints=obj.NPoints();
  for (i=0;i<nanim;i++)
    {
    int j;
    obj.UseAnimation(i);
    GetMatrixFromFace(obj,fc,tm);
    InverzeMatice(tm,im);
    SoucinMatic(im,m,tm);
    for (j=0;j<npoints;j++) if (!selonly || obj.PointSelected(j))
      {
      PosT &ps=obj.Point(j);
      HVECTOR out;
      TransformVector(tm,mxVector3(ps[0],ps[1],ps[2]),out);
      ps[0]=out[0];
      ps[1]=out[1];
      ps[2]=out[2];
      }
    }
  obj.UseAnimation(curanim);
  return 0;
  }

//--------------------------------------------------

CICommand *CIToolNormalAnim_Create(istream &str)
  {
  return new CIToolNormalAnim(str);
  }

//--------------------------------------------------

void CIToolNormalAnim::Save(ostream &str)
  {
  FSaveString(str,refsel);
  datawr(str,reflod);
  datawr(str,selonly);
  }

//--------------------------------------------------

