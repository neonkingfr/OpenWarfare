// AutoArray.cpp: implementation of the AutoArray class.
//
//////////////////////////////////////////////////////////////////////

inline void *operator new(size_t size, void *ptr, int dummy) 
  {return ptr;}

//--------------------------------------------------

//void operator delete(void *ptr, int dummy) {}
/*
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
*/

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

template <class Item>
AutoArray<Item>::AutoArray(int slotsize)
  {
  if (slotsize<1) slotsize=1;
  Slotsize=slotsize;
  Slotcount=Slotalloc=0;
  Lastcount=slotsize;
  }

//--------------------------------------------------

template <class Item> 
AutoArray<Item>::~AutoArray()
  {
  Release();
  FreeExtra();
  free(array);
  }

//--------------------------------------------------

template <class Item> 
Item *AutoArray<Item>::AllocSlot()
  {
  if (Slotcount==Slotalloc)
    {
    Item **nw=(Item **)realloc(array,sizeof(Item *)*(Slotalloc+1));
    if (nw==NULL) RAISE(-1);
    array=nw;
    if ((array[Slotcount]=(Item *)malloc(sizeof(Item)*Slotsize))==NULL) RAISE(-1);	
    Slotalloc+=1;
    }
  return array[Slotcount++];
  }

//--------------------------------------------------

template <class Item>
void AutoArray<Item>::Expand(unsigned int items)
  {
  Item *p;
  if (Slotcount>0) p=array[Slotcount-1]+Lastcount; else p==NULL;
  while (items--)
    {
    if (Lastcount==Slotsize) 
      {p=AllocSlot();Lastcount=0;}
    p=new(p,0) Item();
    Lastcount++;
    }
  }

//--------------------------------------------------

template <class Item> 
void AutoArray<Item>::Release(int toindex)
  {
  int cur=GetCount()-1;
  while (cur>toindex)
    {
    Item *p=FindItem(cur);
    p->~Item();
    cur--;
    }
  }

//--------------------------------------------------

template <class Item> 
Item *AutoArray<Item>::FindItem(unsigned int index)
  {
  int sindx=index%Slotsize;
  int spos=index/Slotsize;
  if (spos>Slotcount) return NULL;
  if (spos==Slotcount && sindx>=Lastcount) return NULL;
  return array[spos]+sindx;
  }

//--------------------------------------------------

template <class Item> 
Item& AutoArray<Item>::operator [ ](unsigned int index)
  {
  Item *p=FindItem(index);
  if (p==NULL) RAISE(-2);
  return *p;
  }

//--------------------------------------------------

template <class Item> 
unsigned int AutoArray<Item>::Insert(Item& p)
  {
  Expand(1);
  int cnt=GetCount();
  Item *i=FindItem(cnt-1);
  *i=p;
  return cnt;
  }

//--------------------------------------------------

template <class Item> 
void AutoArray<Item>::Reserve(int index, int count)
  {
  Expand(count);
  for (int i=GetCount()-1;i>=index;i--)
    {
    Item *p1,*p2;
    p1=FindItem(i);
    p2=FindItem(i+count);
    *p2=*p1;
    }
  }

//--------------------------------------------------

template <class Item> 
void AutoArray<Item>::Delete(int index, int count)
  {
  int cnt=GetCount();
  for (int i=index+count;i<cnt;i++)
    {
    Item *p1,*p2;
    p1=FindItem(i);
    p2=FindItem(i-count);
    *p2=*p1;
    }
  Release(cnt-count);
  }

//--------------------------------------------------

template <class Item> 
void AutoArray<Item>::InsertSlot(int slotindex)
  {
  if (slotindex>=Slotcount) 
    {Expand(Slotsize);return;}
  Item *p=AllocSlot();
  for (int i=0;i<Slotsize;i++) new(p+i,0) Item();
  for (int j=Slotcount-2;j>=slotindex;j--) array[j+1]=array[j];
  array[slotindex]=p;
  }

//--------------------------------------------------

template <class Item> 
void AutoArray<Item>::RemoveSlot(int slotindex)
  {
  if (slotindex>=Slotcount-1)
    {
    Release(GetCount()-Slotsize);
    return;
    }
  for(int i=0;i<Slotsize;i++) array[slotindex]->~Item();
  free(array[slotindex]);
  for(int j=slotindex+1;j<Slotcount;j++) array[j-1]=array[j];
  Slotcount--;
  }

//--------------------------------------------------

template <class Item> 
void AutoArray<Item>::FreeExtra()
  {
  for (int i=Slotcount;i<Slotalloc;i++) free(array[i]);
  array=(Item **)realloc(array,sizeof(Item *)*Slotcount);
  Slotalloc==Slotcount;
  }

//--------------------------------------------------

template <class Item>
void AutoArray<Item>::Save(ostream& str)
  {
  unsigned int cnt=GetCount();
  str.Write((char *)&cnt,sizeof(cnt));
  for(i=0;i<cnt;i++) str<<operator[](i);
  }

//--------------------------------------------------

template <class Item>
void AutoArray<Item>::Load(istream& str)
  {
  unsigned int cnt;
  str.Read((char *)&cnt,sizeof(cnt));
  Release();
  Expand(cnt);
  for(i=0;i<cnt;i++) str>>operator[](i);
  }

//--------------------------------------------------

