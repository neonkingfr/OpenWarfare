// IToolNormalAnim.h: interface for the CIToolNormalAnim class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ITOOLNORMALANIM_H__3BBEC001_45BC_47B1_BF3D_03BA14B5EC60__INCLUDED_)
#define AFX_ITOOLNORMALANIM_H__3BBEC001_45BC_47B1_BF3D_03BA14B5EC60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ComInt.h"

class CIToolNormalAnim : public CICommand  
  {
  CString refsel;
  float reflod;
  bool selonly;
  public:
    virtual int Execute(LODObject *obj);
    CIToolNormalAnim(float reflod, const char *refsel, bool selonly);
    CIToolNormalAnim(istream &str);
    virtual int Tag() 
      {return CITAG_TOOLNORMALANIM;}
    virtual void Save(ostream &str);
    virtual ~CIToolNormalAnim();
    
  };

//--------------------------------------------------

#endif // !defined(AFX_ITOOLNORMALANIM_H__3BBEC001_45BC_47B1_BF3D_03BA14B5EC60__INCLUDED_)
