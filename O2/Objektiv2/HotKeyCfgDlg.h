#if !defined(AFX_HOTKEYCFGDLG_H__321316ED_CE87_4737_A3E5_B0034377B02D__INCLUDED_)
#define AFX_HOTKEYCFGDLG_H__321316ED_CE87_4737_A3E5_B0034377B02D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HotKeyCfgDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CHotKeyCfgDlg dialog

#define HKK_ACCELCHANGED (WM_APP+9876)

class CHotKeyCfgDlg : public CDialog
{
// Construction
	CFrameWnd *_wnd;
	HMENU _mnu;
	int _command;
public:
	static void RecursiveReplaceMenuHotkeys(HACCEL haccel, HMENU start);
	void OnCommandIncomed(CFrameWnd *wnd, int command);
	CHotKeyCfgDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CHotKeyCfgDlg)
	enum { IDD = IDD_HOTKEYCONFIG };
	CComboBox	wHotkey;
	CEdit	wItemName;
	BOOL	vAlt;
	BOOL	vControl;
	BOOL	vShift;
	int		vHotkey;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHotKeyCfgDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CHotKeyCfgDlg)
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnReset();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HOTKEYCFGDLG_H__321316ED_CE87_4737_A3E5_B0034377B02D__INCLUDED_)
