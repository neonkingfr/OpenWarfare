// NormAnimDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "NormAnimDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNormAnimDlg dialog


CNormAnimDlg::CNormAnimDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CNormAnimDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CNormAnimDlg)
    selonly = FALSE;
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CNormAnimDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CNormAnimDlg)
  DDX_Control(pDX, IDC_SELLIST, wSelList);
  DDX_Control(pDX, IDC_REFLOD, wRefLod);
  DDX_Check(pDX, IDC_SELONLY, selonly);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CNormAnimDlg, CDialog)
  //{{AFX_MSG_MAP(CNormAnimDlg)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_REFLOD, OnItemchangedReflod)
    //}}AFX_MSG_MAP
    END_MESSAGE_MAP()
      
      /////////////////////////////////////////////////////////////////////////////
      // CNormAnimDlg message handlers
      
      static void LoadSelections(CListCtrl& list, ObjectData *obj)
        {
        list.DeleteAllItems();
        int i,cnt;
        for (i=0,cnt=MAX_NAMED_SEL;i<cnt;i++)
          {
          const NamedSelection *p=obj->GetNamedSel(i);
          if (p) 
            {
            int q=list.InsertItem(0,p->Name(),ILST_SELECTION);
            list.SetItemData(q,i);
            }
          }
        }

//--------------------------------------------------

BOOL CNormAnimDlg::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  
  wSelList.SetImageList(&shrilist,LVSIL_SMALL);
  wSelList.InsertColumn(0,"",LVCFMT_LEFT,100,0);
  wRefLod.SetImageList(&shrilist,LVSIL_SMALL);
  wRefLod.InsertColumn(0,"",LVCFMT_LEFT,100,0);
  for (int i=0;i<lod->NLevels();i++)
    {
    int p=wRefLod.InsertItem(i,LODResolToName(lod->Resolution(i)),ILST_LODDEFINITION);
    wRefLod.SetItemData(p,i);
    }	
  const Selection *sel=lod->Active()->GetSelection();
  if (sel->NPoints()==0) ::EnableWindow(::GetDlgItem(*this,IDC_SELONLY),FALSE);
  else CheckDlgButton(IDC_SELONLY,TRUE);
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

void CNormAnimDlg::OnItemchangedReflod(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  NMLISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
  if (pNMListView->uNewState & LVIS_FOCUSED )
    {
    int ld=pNMListView->lParam;
    ObjectData *obj=lod->Level(ld);
    LoadSelections(wSelList,obj);
    }
  *pResult = 0;
  }

//--------------------------------------------------

void CNormAnimDlg::OnOK() 
  {
  int pos;
  pos=wSelList.GetNextItem(-1,LVNI_FOCUSED);
  if (pos==-1) return;
  selection=wSelList.GetItemText(pos,0);
  pos=wRefLod.GetNextItem(-1,LVNI_FOCUSED);
  if (pos==-1) return;
  flod=lod->Resolution(wRefLod.GetItemData(pos));
  if (lod->Active()->GetNamedSel(selection)==NULL)
    {
    AfxMessageBox(WString(IDS_TOOLANIMSELERROR,selection),MB_OK|MB_ICONEXCLAMATION);
    return;
    }
  CDialog::OnOK();
  }

//--------------------------------------------------

