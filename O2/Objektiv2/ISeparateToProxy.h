#pragma once

class geVector4;

bool MoveSelectionToProxy(LODObject *obj,bool allLods, const char *selectionName, const char *objectPath, const geVector4 *pin=NULL);

bool MoveSelectionToProxyEx(LODObject *obj,bool allLods, const char *selectionName, const char *curObjName, const geVector4 *pin=NULL);

bool ExtractProxy(ObjectData *obj, const char *selectionName, float resolution);

bool ExtractProxyAllLods(LODObject *lod, const char *selectionName);