// IBackgrndMap.cpp: implementation of the CIBackgrndMap class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Objektiv2.h"
#include "IBackgrndMap.h"
#include "Objektiv2Doc.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CIBackgrndMap::CIBackgrndMap(int mapsize, const char *texname)
  {
  map=new tuvmap[mapsize];
  texture=_strdup(texname);
  this->mapsize=mapsize;
  }

//--------------------------------------------------

CIBackgrndMap::~CIBackgrndMap()
  {
  delete [] map;
  free(texture);
  }

//--------------------------------------------------

void CIBackgrndMap::SetMapping(int index, int pt, float tu, float tv)
  {
  map[index].tu[pt]=tu;
  map[index].tv[pt]=tv;
  }

//--------------------------------------------------

int CIBackgrndMap::Execute(LODObject *obj)
  {
  ObjectData *odata=obj->Active();
  for (int i=0,p=0;i<mapsize && p<odata->NFaces();p++) 
    if (odata->FaceSelected(p))
      {
      FaceT fc(odata,p);
      for (int j=0;j<fc.N();j++)        
        fc.SetUV(j,map[i].tu[j],map[i].tv[j]);        
      fc.SetTexture(texture);
      i++;
      }
  comint.sectchanged=true;
  return 0;
  }

//--------------------------------------------------

CIBackgrndMap::CIBackgrndMap(istream &str)
  {
  datard(str,mapsize);
  map=new tuvmap[mapsize];
  datardf(str,map,mapsize);
  char texname[256];
  FLoadString(str,texname,sizeof(texname));
  texture=_strdup(texname);
  }

//--------------------------------------------------

void CIBackgrndMap::Save(ostream &str)
  {
  datawr(str,mapsize);
  datawrf(str,map,mapsize);
  FSaveString(str,texture);
  }

//--------------------------------------------------

CICommand *CIBackgrndMap_Create(istream &str)
  {
  return new CIBackgrndMap(str);
  }

//--------------------------------------------------

int CIBackgrndMap::Tag() const 
  {
  return CITAG_BACKGRNDMAP;
  }

//--------------------------------------------------

