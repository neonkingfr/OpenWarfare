// Hierarchy.cpp: implementation of the CHierarchy class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "objektiv2.h"
#include "Hierarchy.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#undef new
static void *operator new(size_t size, CHierarchy::SHierarchyItem  *ptr) 
  {return ptr;}

//--------------------------------------------------

static void operator delete(void *p, CHierarchy::SHierarchyItem *ptr) 
  {}

//--------------------------------------------------

CHierarchy::CHierarchy()
  {
  itemlist=NULL;
  order=NULL;
  parents=NULL;
  count=0;
  alloc=0;
  }

//--------------------------------------------------

CHierarchy::~CHierarchy()
  {
  for (int i=0;i<count;i++) itemlist[i].~SHierarchyItem();
  free(itemlist);
  free(order);
  free(parents);
  }

//--------------------------------------------------

CHierarchy::SHierarchyItem::SHierarchyItem()
  {
  filename=_strdup("");
  shortname=_strdup("<unnamed>");
  object=new LODObject();
  comstack=new CComInt(100);
  previewlod=10.0f;
  }

//--------------------------------------------------

CHierarchy::SHierarchyItem::~SHierarchyItem()
  {
  delete object;
  delete comstack;
  }

//--------------------------------------------------

bool CHierarchy::Expand(int items)
  {
  items+=count;
  if (items<alloc) return true;
  SHierarchyItem *ip=(SHierarchyItem *)realloc(itemlist,sizeof(SHierarchyItem)*items);
  if (ip==NULL) return false;
  itemlist=ip;
  int *op=(int *)realloc(order,sizeof(int)*items);
  if (op==NULL) return false;
  order=op;
  int *pp=(int *)realloc(parents,sizeof(int)*items);
  if (pp==NULL) return false;
  parents=pp;
  alloc=items;  
  return true;	
  }

//--------------------------------------------------

CHierarchy::SHierarchyItem *CHierarchy::AddItem()
  {
  if (count==alloc)	if (Expand(alloc)==false) return NULL;
  order[count]=count;
  parents[count]=-1;
  return new(itemlist+count++) SHierarchyItem();
  }

//--------------------------------------------------

void CHierarchy::DeleteItem(int idx)
  {
  if (idx<0 || idx>=count) return;
  itemlist[idx].~SHierarchyItem();
  if (idx+1<count)
    memcpy(itemlist+idx,itemlist+idx+1,sizeof(SHierarchyItem)*(count-idx-1));
  count--;
  }

//--------------------------------------------------

void CHierarchy::RecalcHierarchy()
  {
  for (int i=0;i<count;i++)
    {
    int idx=order[i];
    SHierarchyItem& it=itemlist[i];
    if (parents[i]==-1) CopyMatice(it.curmatrix,it.mm);
    else
      SoucinMatic(it.mm,itemlist[order[parents[i]]].curmatrix,it.curmatrix);
    }
  }

//--------------------------------------------------

void CHierarchy::DistributeFlag(SHierarchyItem *start, int flagset, int flagchild)
  {
  int idx=itemlist-start;
  start->flag=flagset;
  int st;
  for (st=0;st<count;st++) if (order[st]==idx) break;
  if (st==count) return;
  int i;
  for (i=0;i<count;i++)
    {
    if (parents[i]!=-1)
      {
      SHierarchyItem &it=itemlist[order[parents[i]]];
      if (it.flag==flagset || it.flag==flagchild)
        itemlist[order[i]].flag=flagchild;
      }
    }
  }

//--------------------------------------------------

