// UnWrapDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "UnWrapDlg.h"
#include "Objektiv2Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUnWrapDlg dialog


CUnWrapDlg::CUnWrapDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CUnWrapDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CUnWrapDlg)
    tuoffset = 0;
    tvoffset = 0;
    tvweight = FALSE;
    tuweight = FALSE;
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CUnWrapDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CUnWrapDlg)
  DDX_Control(pDX, IDC_TV_OFFSET_VAL, tvoffsetval);
  DDX_Control(pDX, IDC_TU_OFFSET_VAL, tuoffsetval);
  DDX_Control(pDX, IDC_TV_OFFSET, tvoffsetctrl);
  DDX_Control(pDX, IDC_TU_OFFSET, tuoffsetctrl);
  DDX_Slider(pDX, IDC_TU_OFFSET, tuoffset);
  DDX_Slider(pDX, IDC_TV_OFFSET, tvoffset);
  DDX_Check(pDX, IDC_WEIGHTTV, tvweight);
  DDX_Check(pDX, IDC_WEIGHTTU, tuweight);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CUnWrapDlg, CDialog)
  //{{AFX_MSG_MAP(CUnWrapDlg)
  ON_WM_HSCROLL()
    ON_WM_TIMER()
      //}}AFX_MSG_MAP
      END_MESSAGE_MAP()
        
        /////////////////////////////////////////////////////////////////////////////
        // CUnWrapDlg message handlers
        
        BOOL CUnWrapDlg::OnInitDialog() 
          {
          CDialog::OnInitDialog();
          
          tuoffsetctrl.SetRange(-5000,5000);
          tvoffsetctrl.SetRange(-5000,5000);
          tuoffsetctrl.SetPageSize(100);
          tvoffsetctrl.SetPageSize(100);
          tuoffsetctrl.SetLineSize(1);
          tvoffsetctrl.SetLineSize(1);
          
          SetTimer(1,500,NULL);
          tuoffsetctrl.SetPos(0);
          tvoffsetctrl.SetPos(0);
          UpdateValues();
          forcepreview=0;
          return TRUE;  // return TRUE unless you set the focus to a control
          // EXCEPTION: OCX Property Pages should return FALSE
          }

//--------------------------------------------------

void CUnWrapDlg::UpdateValues()
  {
  int val1=tuoffsetctrl.GetPos();
  int val2=tvoffsetctrl.GetPos();
  CString form;
  form.Format("%.4f",(float)val1/10000.0f);
  tuoffsetval.SetWindowText(form);
  form.Format("%.4f",(float)val2/10000.0f);
  tvoffsetval.SetWindowText(form);
  
  }

//--------------------------------------------------

void CUnWrapDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)	  
  {
  UpdateValues();
  CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
  forcepreview=2;
  }

//--------------------------------------------------

void CUnWrapDlg::OnTimer(UINT nIDEvent) 
  {
  if (forcepreview)
    {
    forcepreview--;
    if (forcepreview==1) return;
    if (UpdateData()==FALSE) return;
    ObjectData temp(*obj);
    Calc();
    doc->UpdateAllViews(NULL,1);
    *obj=temp;
    }
  CDialog::OnTimer(nIDEvent);
  }

//--------------------------------------------------

void CUnWrapDlg::Calc()
  {
  int cnt=obj->NFaces();
  float tu=tuoffset/10000.0f;
  float tv=tvoffset/10000.0f;
  const Selection *sel=obj->GetSelection();
  for (int i=0;i<cnt;i++)
    {
    FaceT fc(obj,i);
    for (int j=0;j<fc.N();j++) 
      if (sel->PointSelected(fc.GetPoint(j)))
        {
        float u=fc.GetU(j),v=fc.GetV(j);
        int   point=fc.GetPoint(j);
        float ttu,ttv;
        if (tuweight) ttu=sel->PointWeight(point)*tu;else ttu=tu;
        if (tvweight) ttv=sel->PointWeight(point)*tv;else ttv=tv;
        fc.SetUV(j,u+ttu,v+ttv);        
        }
    }
  }

//--------------------------------------------------

