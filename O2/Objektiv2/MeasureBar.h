#if !defined(AFX_MEASUREBAR_H__38B97282_7D6F_49A4_8941_7E356B2A1DE9__INCLUDED_)
#define AFX_MEASUREBAR_H__38B97282_7D6F_49A4_8941_7E356B2A1DE9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MeasureBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMeasureBar dialog

class CMeasureBar : public CDialogBar
  {
  CWnd *main;
  ObjectData *obj;
  CButton check1,check2;
  
  public:
    bool IsVisible();
    void UpdateValues(HVECTOR pin, HVECTOR cur);
    void Create(CWnd *parent, int menuid);
    CDIALOGBAR_RESIZEABLE
      CMeasureBar();   // standard constructor
      virtual void OnUpdateCmdUI( CFrameWnd* pTarget, BOOL bDisableIfNoHndler ) 
        {
        }
    
    // Dialog Data
    //{{AFX_DATA(CMeasureBar)
    enum 
      { IDD = IDD_MEASURING };
    // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CMeasureBar)
  protected:
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CMeasureBar)
    afx_msg void OnDestroy();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
    private:
    void UpdateFloat(int idc, float f);
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MEASUREBAR_H__38B97282_7D6F_49A4_8941_7E356B2A1DE9__INCLUDED_)
