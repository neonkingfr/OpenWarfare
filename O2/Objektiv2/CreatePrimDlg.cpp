// CreatePrimDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "CreatePrimDlg.h"
#include "Objektiv2Doc.h"
#include "..\ObjektivLib\ObjToolTopology.h"

#include <malloc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCreatePrimDlg dialog
                                      

CCreatePrimDlg::CCreatePrimDlg(CWnd* pParent, int idd)
  : CDialog(idd, pParent)
    {
    //{{AFX_DATA_INIT(CCreatePrimDlg)
    numX = 1;
    numY = 1;
    numZ = 1;
    radius = 1.0f;
    sizeX = 1.0f;
    sizeY = 1.0f;
    sizeZ = 1.0f;
    align = 0;
    center = FALSE;
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CCreatePrimDlg::DoDataExchange(CDataExchange* pDX)
  {
  if (pDX->m_bSaveAndValidate)
    CorrectFloatValues(this,IDC_NUMX,IDC_NUMY,IDC_NUMZ,IDC_RADIUS,IDC_SIZEX,IDC_SIZEY,IDC_SIZEZ,0);
  CDialog::DoDataExchange(pDX);
  switch (create_type)
    {
    case IDD_CREATE_PRIM_BOX:
        DDX_Control(pDX, IDC_SIZEZ, wndSizeZ);
        DDX_Control(pDX, IDC_SIZEY, wndSizeY);
        DDX_Control(pDX, IDC_SIZEX, wndSizeX);
        DDX_Control(pDX, IDC_NUMZ, wndNumZ);
        DDX_Control(pDX, IDC_NUMY, wndNumY);
        DDX_Control(pDX, IDC_NUMX, wndNumX);
        DDX_Text(pDX, IDC_NUMX, numX);
        DDV_MinMaxUInt(pDX, numX, 1, 1000);
        DDX_Text(pDX, IDC_NUMY, numY);
        DDV_MinMaxUInt(pDX, numY, 1, 1000);
        DDX_Text(pDX, IDC_NUMZ, numZ);
        DDV_MinMaxUInt(pDX, numZ, 1, 1000);
        DDX_Text(pDX, IDC_SIZEX, sizeX);
        DDV_MinMaxFloat(pDX, sizeX, 0.f, 1.e+020f);
        DDX_Text(pDX, IDC_SIZEY, sizeY);
        DDV_MinMaxFloat(pDX, sizeY, 0.f, 1.e+020f);
        DDX_Text(pDX, IDC_SIZEZ, sizeZ);
        DDV_MinMaxFloat(pDX, sizeZ, 0.f, 1.e+020f);
        break;
    case IDD_CREATE_PRIM_PLANE:
        DDX_Control(pDX, IDC_SIZEX, wndSizeX);
        DDX_Control(pDX, IDC_SIZEY, wndSizeY);
        DDX_Control(pDX, IDC_NUMX, wndNumX);
        DDX_Control(pDX, IDC_NUMY, wndNumY);
        DDX_Text(pDX, IDC_NUMX, numX);
        DDV_MinMaxUInt(pDX, numX, 1, 1000);
        DDX_Text(pDX, IDC_NUMY, numY);
        DDV_MinMaxUInt(pDX, numY, 1, 1000);
        DDX_Text(pDX, IDC_SIZEX, sizeX);
        DDV_MinMaxFloat(pDX, sizeX, 0.f, 1.e+020f);
        DDX_Text(pDX, IDC_SIZEY, sizeY);
        DDV_MinMaxFloat(pDX, sizeY, 0.f, 1.e+020f);
        break;
    case IDD_CREATE_PRIM_SPEHERE:
        DDX_Control(pDX, IDC_SIZEZ, wndSizeZ);
        DDX_Control(pDX, IDC_SIZEY, wndSizeY);
        DDX_Control(pDX, IDC_SIZEX, wndSizeX);
        DDX_Control(pDX, IDC_NUMY, wndNumY);
        DDX_Control(pDX, IDC_NUMX, wndNumX);
        DDX_Text(pDX, IDC_NUMX, numX);
        DDV_MinMaxUInt(pDX, numX, 1, 1000);
        DDX_Text(pDX, IDC_NUMY, numY);
        DDV_MinMaxUInt(pDX, numY, 1, 1000);
        DDX_Text(pDX, IDC_SIZEX, sizeX);
        DDV_MinMaxFloat(pDX, sizeX, 0.f, 1.e+020f);
        DDX_Text(pDX, IDC_SIZEY, sizeY);
        DDV_MinMaxFloat(pDX, sizeY, 0.f, 1.e+020f);
        DDX_Text(pDX, IDC_SIZEZ, sizeZ);
        DDV_MinMaxFloat(pDX, sizeZ, 0.f, 1.e+020f);
        break;
    case IDD_CREATE_PRIM_CYLINDER:
        DDX_Control(pDX, IDC_SIZEZ, wndSizeZ);
        DDX_Control(pDX, IDC_NUMX, wndNumX);
        DDX_Control(pDX, IDC_NUMZ, wndNumZ);
        DDX_Control(pDX, IDC_RADIUS, wndRadius);
        DDX_Control(pDX, IDC_CIRCCENTER, wndCenter);
        DDX_Text(pDX, IDC_SIZEZ, sizeZ);
        DDV_MinMaxFloat(pDX, sizeZ, 0.f, 1.e+020f);
        DDX_Check(pDX, IDC_CIRCCENTER, center);
        DDX_Text(pDX, IDC_RADIUS, radius);
        DDV_MinMaxFloat(pDX, radius, 0.f, 1.e+020f);
        DDX_Text(pDX, IDC_NUMX, numX);
        DDV_MinMaxUInt(pDX, numX, 1, 1000);
        DDX_Text(pDX, IDC_NUMZ, numZ);
        DDV_MinMaxUInt(pDX, numZ, 1, 1000);
        DDX_Check(pDX, IDC_CIRCCENTER, center);
        break;
    case IDD_CREATE_PRIM_CIRCLE:
        DDX_Control(pDX, IDC_CIRCCENTER, wndCenter);
        DDX_Control(pDX, IDC_RADIUS, wndRadius);
        DDX_Control(pDX, IDC_NUMX, wndNumX);
        DDX_Text(pDX, IDC_RADIUS, radius);
        DDV_MinMaxFloat(pDX, radius, 0.f, 1.e+020f);
        DDX_Check(pDX, IDC_CIRCCENTER, center);
        DDX_Text(pDX, IDC_NUMX, numX);
        DDV_MinMaxUInt(pDX, numX, 1, 1000);
        break;
    default:
        //{{AFX_DATA_MAP(CCreatePrimDlg)
        DDX_Control(pDX, IDC_CIRCCENTER, wndCenter);
        DDX_Control(pDX, IDC_ST_SEGRD, stdr);
        DDX_Control(pDX, IDC_ST_SEGX, stx);
        DDX_Control(pDX, IDC_SIZEZ, wndSizeZ);
        DDX_Control(pDX, IDC_SIZEY, wndSizeY);
        DDX_Control(pDX, IDC_SIZEX, wndSizeX);
        DDX_Control(pDX, IDC_RADIUS, wndRadius);
        DDX_Control(pDX, IDC_NUMZ, wndNumZ);
        DDX_Control(pDX, IDC_NUMY, wndNumY);
        DDX_Control(pDX, IDC_NUMX, wndNumX);
        DDX_Text(pDX, IDC_NUMX, numX);
        DDV_MinMaxUInt(pDX, numX, 1, 1000);
        DDX_Text(pDX, IDC_NUMY, numY);
        DDV_MinMaxUInt(pDX, numY, 1, 1000);
        DDX_Text(pDX, IDC_NUMZ, numZ);
        DDV_MinMaxUInt(pDX, numZ, 1, 1000);
        DDX_Text(pDX, IDC_RADIUS, radius);
        DDV_MinMaxFloat(pDX, radius, 0.f, 1.e+020f);
        DDX_Text(pDX, IDC_SIZEX, sizeX);
        DDV_MinMaxFloat(pDX, sizeX, 0.f, 1.e+020f);
        DDX_Text(pDX, IDC_SIZEY, sizeY);
        DDV_MinMaxFloat(pDX, sizeY, 0.f, 1.e+020f);
        DDX_Text(pDX, IDC_SIZEZ, sizeZ);
        DDV_MinMaxFloat(pDX, sizeZ, 0.f, 1.e+020f);
        DDX_Radio(pDX, IDC_CENTER, align);
        DDX_Check(pDX, IDC_CIRCCENTER, center);
        //}}AFX_DATA_MAP
        return;
        }
   DDX_Radio(pDX, IDC_CENTER, align);
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CCreatePrimDlg, CDialog)
  //{{AFX_MSG_MAP(CCreatePrimDlg)
  //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CCreatePrimDlg message handlers
    
    BOOL CCreatePrimDlg::OnInitDialog() 
      {
      if (create_type==ID_CREATE_CIRCLE || create_type==ID_CREATE_CYLINDER)
        if (numX<3) numX=10;
      CDialog::OnInitDialog();
  /*    
      switch (create_type)
        {
        case ID_CREATE_PLANE:
        wndSizeZ.EnableWindow(FALSE);
        wndNumZ.EnableWindow(FALSE);
        wndRadius.EnableWindow(FALSE);
        GetDlgItem(IDC_BOTTOM)->EnableWindow(FALSE);
        break;
        case ID_CREATE_BOX:
        wndRadius.EnableWindow(FALSE);
        break;
        case ID_CREATE_SPEHERE:
        wndNumZ.EnableWindow(FALSE);
        wndRadius.EnableWindow(FALSE);
        break;
        case ID_CREATE_CYLINDER:
        wndSizeX.EnableWindow(FALSE);
        wndSizeY.EnableWindow(FALSE);
        wndNumY.EnableWindow(FALSE);
        stx.ShowWindow(SW_HIDE);
        stdr.ShowWindow(SW_SHOW);
        wndCenter.ShowWindow(SW_SHOW);		
        break;
        case ID_CREATE_CIRCLE:
        wndSizeX.EnableWindow(FALSE);
        wndSizeY.EnableWindow(FALSE);
        wndSizeZ.EnableWindow(FALSE);
        wndNumY.EnableWindow(FALSE);
        wndNumZ.EnableWindow(FALSE);
        stx.ShowWindow(SW_HIDE);
        stdr.ShowWindow(SW_SHOW);
        wndCenter.ShowWindow(SW_SHOW);
        break;
        case ID_CREATE_GEOSPEHERE:
        wndNumY.EnableWindow(FALSE);
        wndNumZ.EnableWindow(FALSE);
        wndRadius.EnableWindow(FALSE);
        break;
        
        }*/
      return TRUE;  // return TRUE unless you set the focus to a control
      // EXCEPTION: OCX Property Pages should return FALSE
      }

//--------------------------------------------------
static void MapFace(ObjectData &obj,int face, ...)
  {
  va_list args;
  va_start(args,face);
  FaceT fc(obj,face);
  for (int i=0;i<fc.N();i++)
    {
    float u=va_arg(args,double);
    float v=va_arg(args,double);
    fc.SetUV(i,u,v);
//    fc.SetTexture("DetailMapy\\domsedy01.pac");
    }
  }

int CCreatePrimDlg::CreatePoint(CreatePointStruct *cp)
  {
  int idx=cp->odata->ReservePoints(1);
  PosT& pt=cp->odata->Point(idx);
  HVECTOR tv;
  TransformVector(cp->mm,mxVector3(cp->x,cp->y,cp->z),tv);
  XYZW2XYZQ(tv);
  pt[0]=tv[XVAL];
  pt[1]=tv[YVAL];
  pt[2]=tv[ZVAL];
  return idx;
  }

//--------------------------------------------------

void CCreatePrimDlg::CreatePlane(ObjectData *odata, HMATRIX mm, bool back)
  {
  CreatePointStruct cp;
  unsigned int x, y;
  int beg=odata->NPoints();
  
  cp.mm=mm;
  cp.odata=odata;
  for(y=0;y<=numY;y++)
    for(x=0;x<=numX;x++)
      {
      cp.x=((float)x/(float)numX)*2.0f-1.0f;
      cp.y=((float)y/(float)numY)*2.0f-1.0f;
      cp.z=0.0f;	  
      CreatePoint(&cp);
      }
  float ifnumy=1.0f/numY;
  float ifnumx=1.0f/numX;
  for (y=0;y<numY;y++)
    for (x=0;x<numX;x++)
      {
      int tt=beg+x+y*(numX+1);
      if (back)
        {
        int fc=FastCreateFace(odata,4,tt+numX+1,tt+numX+2,tt+1,tt);
        MapFace(*odata,fc,x*ifnumx,(y+1)*ifnumy,(x+1)*ifnumx,(y+1)*ifnumy,(x+1)*ifnumx,y*ifnumy,x*ifnumx,y*ifnumy);
        }
      else
        {
        int fc=FastCreateFace(odata,4,tt,tt+1,tt+numX+2,tt+numX+1);
        MapFace(*odata,fc,x*ifnumx,y*ifnumy,(x+1)*ifnumx,y*ifnumy,(x+1)*ifnumx,(y+1)*ifnumy,x*ifnumx,(y+1)*ifnumy);
        }
      }
  }

//--------------------------------------------------

#define CALCINDEXFROMPLANE(i,t)\
	  if ((i)>=z3) t=(numY-(i)+z3)*(numX+1);\
	  else if ((i)>=z2) t=(numX+1)*(numY+1)-((i)-z2)-1;\
	  else if ((i)>=z1) t=numX+(numX+1)*((i)-z1);\
	  else t=(i);

void CCreatePrimDlg::CreateBox(ObjectData *odata, HMATRIX mm)
  {
  CreatePointStruct cp;
  HMATRIX m1;
  unsigned int beg1=odata->NPoints();
  Translace(m1,0,0,-1.0f);
  SoucinMatic(m1,mm,m1);
  CreatePlane(odata,m1);
  unsigned int beg2=odata->NPoints();
  Translace(m1,0,0,1.0f);
  SoucinMatic(m1,mm,m1);
  CreatePlane(odata,m1,true);
  unsigned int idx_b=beg1;
  unsigned int z1=numX;
  unsigned int z2=z1+numY;
  unsigned int z3=z2+numX;
  unsigned int z4=z3+numY;
  unsigned int i,z;
  float ifnumX=1.0f/numX;
  float ifnumY=1.0f/numY;
  float ifnumZ=1.0f/numZ;
  cp.mm=mm;
  cp.odata=odata;
  for (z=1;z<=numZ;z++)
    {
    unsigned int idx_u=(z==numZ)?beg2:odata->NPoints();
    unsigned int t1,t2,t3,t4;	
    float curX;
    int curIdx;
    for (i=0;i<z4;i++)
      {
      unsigned int j=i+1;
      if (idx_u!=beg2)
        {
        if (i==0)
          {
          cp.x=-1.0f;
          cp.y=-1.0f;
          cp.z=((float)z/(float)numZ)*2.0f-1.0f;
          CreatePoint(&cp);	  
          }
        if (j<z4)		  
          if (j>=z3)
            {
            cp.y=-((float)(j-z3)/(float)numY)*2.0+1.0f;
            cp.x=-1.0f;
            }
        else if (j>=z2)
          {
          cp.x=-((float)(j-z2)/(float)numX)*2.0+1.0f;
          cp.y=1.0f;
          }
        else if (j>=z1)
          {
          cp.y=((float)(j-z1)/(float)numY)*2.0-1.0f;
          cp.x=1.0f;
          }
        else
          {
          cp.x=((float)(j)/(float)numX)*2.0-1.0f;
          cp.y=-1.0f;
          }
        if (j!=z4) CreatePoint(&cp);	  
        }
      if (idx_b==beg1)
        {
        CALCINDEXFROMPLANE(i,t1);
        CALCINDEXFROMPLANE(i+1,t2);
        }
      else
        {
        t2=(i+1)%z4;
        t1=i;
        }
      if (idx_u==beg2)
        {
        CALCINDEXFROMPLANE(i,t3);
        CALCINDEXFROMPLANE(i+1,t4);
        }
      else
        {
        t3=i;
        t4=(i+1)%z4;
        }
      if (i>=z3) {curX=ifnumX; curIdx=z3;}
      else if (i>=z2) {curX=ifnumY; curIdx=z2;}
      else if (i>=z1) {curX=ifnumX; curIdx=z1;}
      else {curX=ifnumY; curIdx=0;}
      int face=FastCreateFace(odata,4,t3+idx_u,t4+idx_u,t2+idx_b,t1+idx_b);
      MapFace(*odata,face,
        (i-curIdx)*curX,         (z+1)*ifnumZ,
        (i-curIdx+1)*curX,       (z+1)*ifnumZ,
        (i-curIdx+1)*curX,       z*ifnumZ,
        (i-curIdx)*curX,         z*ifnumZ
        );
      }
    idx_b=idx_u;
    }
  }

//--------------------------------------------------

void CCreatePrimDlg::CreatePrimitive(ObjectData *odata,HMATRIX pinmatrix)
  {
  HMATRIX m1,m2;
  Zoom(m1,sizeX*0.5f,sizeY*0.5f,sizeZ*0.5f);
  SoucinMatic(m1,pinmatrix,m2);
  int mpoint=odata->NPoints();
  int mface=odata->NFaces();
  if (align==1)
    {
    Translace(m1,0.0f,sizeZ*0.5f,0.0f);
    SoucinMatic(m2,m1,m2);
    }
  if (align==2)
    {
    Translace(m1,0.0f,-sizeZ*0.5f,0.0f);
    SoucinMatic(m2,m1,m2);
    }
  switch(create_type)
    {
    case IDD_CREATE_PRIM_PLANE:CreatePlane(odata,m2);break;
    case IDD_CREATE_PRIM_BOX:CreateBox(odata,m2);break;
    case IDD_CREATE_PRIM_CYLINDER:CreateCylinder(odata,m2);break;
    case IDD_CREATE_PRIM_SPEHERE:CreateSpehere(odata,m2);break;
    case IDD_CREATE_PRIM_CIRCLE:  if (center) CreateCircle2(odata,m2,false);else CreateCircle(odata,m2,false);break;
//    case IDD_CREATE_PRIM_GEOSPEHERE:CreateGeoSpehere(odata,m2);break;
    }
  int fpoint=odata->NPoints();
  int fface=odata->NFaces();
  ((Selection *)odata->GetSelection())->Clear();
  for(int i=mpoint;i<fpoint;i++) odata->PointSelect(i);
  for(int j=mface;j<fface;j++) odata->FaceSelect(j);

  odata->GetTool<ObjToolTopology>().AutoSharpEdges();
  odata->RecalcNormals();
  }

//--------------------------------------------------

void CCreatePrimDlg::CreateCircle(ObjectData *odata, HMATRIX mm, bool back)
  {
  CreatePointStruct cp;
  cp.mm=mm;
  cp.odata=odata;
  int b1=odata->NPoints();
  int b0 = b1;

  float *pointU = new float[numX];
  float *pointV = new float[numX];

  for (unsigned int i=0;i<numX;i++)
    {
    float f=(float)i*FPI*2/(float)numX;
    float tmp = (float)cos(f);
    pointU[i] = tmp/2+0.5;
    cp.x=2.0f*tmp*radius;

    tmp = (float)sin(f);
    pointV[i] = tmp/2+0.5;
    cp.y=2.0f*tmp*radius;
    cp.z=0.0f;
    CreatePoint(&cp);
    }
  int b2=odata->NPoints()-1;
  while (b1+1<=b2-1)
    {
    if (b1+1==b2-1)
      if (back)	
      {	
        int face=FastCreateFace(odata,3,b1,b2,b1+1);
        MapFace(*odata,face,pointU[b1-b0],pointV[b1-b0],pointU[b2-b0],pointV[b2-b0],pointU[b1+1-b0],pointV[b1+1-b0]);
      }
    else
    {
      int face=FastCreateFace(odata,3,b2,b1,b1+1);
      MapFace(*odata,face,pointU[b2-b0],pointV[b2-b0],pointU[b1-b0],pointV[b1-b0],pointU[b1+1-b0],pointV[b1+1-b0]);
    }
    else
      if (back)
      {
        int face=FastCreateFace(odata,4,b2-1,b1+1,b1,b2);
        MapFace(*odata,face,pointU[b2-1-b0],pointV[b2-1-b0],pointU[b1+1-b0],pointV[b1+1-b0],pointU[b1-b0],pointV[b1-b0],pointU[b2-b0],pointV[b2-b0]);
      }
    else
    {
      int face=FastCreateFace(odata,4,b2,b1,b1+1,b2-1);
      MapFace(*odata,face,pointU[b2-b0],pointV[b2-b0],pointU[b1-b0],pointV[b1-b0],pointU[b1+1-b0],pointV[b1+1-b0],pointU[b2-1-b0],pointV[b2-1-b0]);
    }
    b2--;
    b1++;
    }
  }

//--------------------------------------------------

void CCreatePrimDlg::CreateCircle2(ObjectData *odata, HMATRIX mm, bool back)
{
  CreatePointStruct cp;
  cp.mm=mm;
  cp.odata=odata;
  int center=odata->NPoints();
  cp.x=cp.y=cp.z=0;
  CreatePoint(&cp);
  int b1=odata->NPoints();
  unsigned int i;

  float *pointU = new float[numX];
  float *pointV = new float[numX];

  for ( i=0;i<numX;i++)
  {
    float f=(float)i*FPI*2/(float)numX;
    float tmp = (float)cos(f);
    pointU[i] = tmp/2+0.5;
    cp.x=2.0f*tmp*radius;

    tmp = (float)sin(f);
    pointV[i] = tmp/2+0.5;
    cp.y=2.0f*tmp*radius;
    cp.z=0.0f;
    CreatePoint(&cp);
  }
  for (i=0;i<numX;i++)
  {
    int a=i+b1;
    int b; if (i+1==numX) b=b1;else b=a+1;
    if (back)
    {
      int face=FastCreateFace(odata,3,center,b,a);	
      MapFace(*odata,face,0.5,0.5,pointU[b-b1],pointV[b-b1],pointU[i],pointV[i]);
    }
    else
    {
      int face=FastCreateFace(odata,3,center,a,b);	
      MapFace(*odata,face,0.5,0.5,pointU[i],pointV[i],pointU[b-b1],pointV[b-b1]);

    }
  }
}

//--------------------------------------------------

void CCreatePrimDlg::CreateCylinder(ObjectData *odata, HMATRIX mm)
  {
  CreatePointStruct cp;
  cp.mm=mm;
  cp.odata=odata;
  HMATRIX m1;
  unsigned int beg1=odata->NPoints()+(center?1:0);
  Translace(m1,0,0,-1.0f);
  SoucinMatic(m1,mm,m1);
  if (center) CreateCircle2(odata,m1,false);else CreateCircle(odata,m1,false);
  unsigned int beg2=odata->NPoints()+(center?1:0);
  Translace(m1,0,0,1.0f);
  SoucinMatic(m1,mm,m1);
  if (center) CreateCircle2(odata,m1,true);else CreateCircle(odata,m1,true);
  unsigned int idx_b=beg1;
  float ifnumX=1.0f/numX;
  float ifnumZ=1.0f/numZ;
  for (unsigned int z=1;z<=numZ;z++)
    {
    unsigned int idx_u=(z==numZ)?beg2:odata->NPoints();
    for (unsigned int i=0;i<numX;i++)
      {
      if (idx_u!=beg2)
        {
        if (i==0)
          {
          cp.x=2.0f*radius;
          cp.y=0.0f;
          cp.z=((float)z/(float)numZ)*2.0f-1.0f;
          CreatePoint(&cp);	  
          }
        if (i+1<numX)
          {
          float f=(float)(i+1)*FPI*2/(float)numX;
          cp.x=2.0f*(float)cos(f)*radius;
          cp.y=2.0f*(float)sin(f)*radius;
          CreatePoint(&cp);
          }
        }
      int t1=(i+1)%numX;
      int face=FastCreateFace(odata,4,idx_u+i,idx_u+t1,idx_b+t1,idx_b+i);
      MapFace(*odata,face,i*ifnumX,z*ifnumZ,(i+1)*ifnumX,z*ifnumZ,(i+1)*ifnumX,(z-1)*ifnumZ,i*ifnumX,(z-1)*ifnumZ);
      }
    idx_b=idx_u;
    }
  }

//--------------------------------------------------

void CCreatePrimDlg::CreateSpehere(ObjectData *odata, HMATRIX mm)
  {
  CreatePointStruct cp;
  cp.mm=mm;
  cp.odata=odata;
  int *idx1,*idx2;
  idx1=(int *)alloca(sizeof(int)*numX);
  idx2=(int *)alloca(sizeof(int)*numX);
  unsigned int x,y;
  cp.x=0;
  cp.y=0;
  cp.z=-1;  
  int cnumY=numY/2;
  int a=CreatePoint(&cp);
  for (x=0;x<numX;x++) idx1[x]=a;
  for (y=1;y<=cnumY;y++) 
    {
    cp.z=-cos((float)y/(float)cnumY*FPI);
    float r=(float)sqrt(1.0f-cp.z*cp.z);
    if (y!=cnumY)
      for (x=0;x<numX;x++)
        {
        float f=(float)x/(float)numX*FPI*2;
        cp.x=r*(float)cos(f);
        cp.y=r*(float)sin(f);
        idx2[x]=CreatePoint(&cp);
        }
    else
      {
      cp.x=cp.y=0.0f;
      a=CreatePoint(&cp);
      for (x=0;x<numX;x++) idx2[x]=a;
      }
    for(x=0;x<numX;x++)
      {
      int x2=(x+1)%numX;
      float ifnumX=1.0f/numX;
      float ifnumY=1.0f/cnumY;
      int face;      
      if (idx1[x]==idx1[x2])		
        {
        face=FastCreateFace(odata,3,idx2[x],idx2[x2],idx1[x]);
        MapFace(*odata,face,(x+1)*ifnumX,(y)*ifnumY,(x)*ifnumX,(y)*ifnumY,x*ifnumX,(y-1)*ifnumY);
        }
      else if (idx2[x]==idx2[x2])
        {
        face=FastCreateFace(odata,3,idx2[x],idx1[x2],idx1[x]);
        MapFace(*odata,face,x*ifnumX,(y)*ifnumY,(x+1)*ifnumX,(y-1)*ifnumY,(x)*ifnumX,(y-1)*ifnumY);
        }
      else
        {
        face=FastCreateFace(odata,4,idx1[x],idx2[x],idx2[x2],idx1[x2]);
        MapFace(*odata,face,x*ifnumX,(y-1)*ifnumY,(x)*ifnumX,(y)*ifnumY,(x+1)*ifnumX,(y)*ifnumY,(x+1)*ifnumX,(y-1)*ifnumY);
        }
      }
    memcpy(idx1,idx2,sizeof(int)*numX);
    }
  }

//--------------------------------------------------

void CCreatePrimDlg::Load(istream &str)
  {
  datard(str,numX);
  datard(str,numY);
  datard(str,numZ);
  datard(str,sizeX);
  datard(str,sizeY);
  datard(str,sizeZ);
  datard(str,radius);
  datard(str,create_type);
  }

//--------------------------------------------------

void CCreatePrimDlg::Save(ostream &str)
  {
  datawr(str,numX);
  datawr(str,numY);
  datawr(str,numZ);
  datawr(str,sizeX);
  datawr(str,sizeY);
  datawr(str,sizeZ);
  datawr(str,radius);
  datawr(str,create_type);
  }

//--------------------------------------------------

static int recursion=0;

void GeoFacesRecursive(ObjectData *odata, int a,int b, int c,int r)
  {
  if (r)
    {
    int d=odata->NPoints();
    VecT &nw=*odata->NewPoint();
    PosT& ps1=odata->Point(a);
    PosT& ps2=odata->Point(b);
    PosT& ps3=odata->Point(c);
    nw=ps1+ps2+ps3;
    nw.Normalize();
    GeoFacesRecursive(odata,a,b,d,r-1);
    GeoFacesRecursive(odata,b,c,d,r-1);
    GeoFacesRecursive(odata,c,a,d,r-1);
    }
  else
    {
    FaceT fc(odata,odata->NewFace());
    fc.SetN(3);
    fc.SetPoint(0,a);
    fc.SetPoint(1,b);
    fc.SetPoint(2,c);
    }
  }

//--------------------------------------------------

void CCreatePrimDlg::CreateGeoSpehere(ObjectData *odata, HMATRIX mm)
  {
  int p=odata->NPoints();
  float cos120=(float)cos(torad(120));
  float sin120=(float)sin(torad(120));
  float cos60=(float)cos(torad(30));
  float sin60=(float)sin(torad(30));
  
  odata->ReservePoints(6);
  PosT& ps1=odata->Point(p);  ps1[0]=1;ps1[1]=0;ps1[2]=0;
  PosT& ps2=odata->Point(p+1);ps2[0]=0;ps2[1]=1;ps2[2]=0;
  PosT& ps3=odata->Point(p+2);ps3[0]=-1;ps3[1]=0;ps3[2]=0;
  PosT& ps4=odata->Point(p+3);ps4[0]=0;ps4[1]=-1;ps4[2]=0;
  PosT& ps5=odata->Point(p+4);ps5[0]=0;ps5[1]=0;ps5[2]=1;
  PosT& ps6=odata->Point(p+5);ps6[0]=0;ps6[1]=0;ps6[2]=-1;
  GeoFacesRecursive(odata,p+1,p+0,p+4,numX-1);
  GeoFacesRecursive(odata,p+2,p+1,p+4,numX-1);
  GeoFacesRecursive(odata,p+3,p+2,p+4,numX-1);
  GeoFacesRecursive(odata,p+0,p+3,p+4,numX-1);
  GeoFacesRecursive(odata,p+0,p+1,p+5,numX-1);
  GeoFacesRecursive(odata,p+1,p+2,p+5,numX-1);
  GeoFacesRecursive(odata,p+2,p+3,p+5,numX-1);
  GeoFacesRecursive(odata,p+3,p+0,p+5,numX-1);
  }

//--------------------------------------------------

