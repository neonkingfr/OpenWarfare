// Calculator.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "Calculator.h"
#include <fstream>
using namespace std;

#include "optima\express.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCalculator dialog

CSchemes::CSchemes()
  {
  Hash_Init2(&hash,31,sizeof(HashEntry),GetOffset(HashEntry,name),HashEntry::GetSizeOfKey(),HASHF_POINTERS);
  Hash_SetDestructor(&hash,CSchemes::HashDestructor);
  profname="Objektiv2.sch";
  loaded=false;
  }

//--------------------------------------------------

char CSchemes::HashDestructor(void *pt)
  {
  HashEntry *he=(HashEntry *)pt;
  delete he;
  return 0;
  }

//--------------------------------------------------

CSchemes::~CSchemes()
  {
  Hash_Done(&hash);
  }

//--------------------------------------------------

void CSchemes::AddScheme(HashEntry *scheme)
  {
  Hash_Add(&hash,(void *)scheme);  
  }

//--------------------------------------------------

CSchemes::HashEntry *CSchemes::GetScheme(int type, const char *name)
  {
  HashEntry ent;
  ent.SetKey(type, name);
  return (HashEntry *)Hash_Find(&hash,(void *)&ent);
  }

//--------------------------------------------------

void CSchemes::RemoveScheme(int type, const char *name)
  {
  HashEntry *ent=GetScheme(type,name);
  if (ent!=NULL) Hash_DeleteExact(&hash,(void *)ent,0);
  }

//--------------------------------------------------

bool CSchemes::LoadSchemes()
  {
  ifstream in(profname,ios::in);
  if (in.fail()) return false;
  while (!in.eof())
    {
    HashEntry *he=new HashEntry();
    he->Load(in);
    if (in.fail() || in.eof())
      {
      delete he;
      break;
      }
    if (in.get()!='\n') break;
    Hash_Add(&hash,(void *)he);
    }
  loaded=true;
  return 0;
  }

//--------------------------------------------------

void CSchemes::SaveSchemes()
  {
  ofstream out(profname,ios::out|ios::trunc);
  if (out.fail()) return;  
  for (void *ptr=NULL;ptr=Hash_EnumData(&hash,ptr);)
    {
    HashEntry *he=(HashEntry *)ptr;
    he->Save(out);
    out<<'\n';
    }
  }

//--------------------------------------------------

CSchemes schemes;

CCalculator::CCalculator(CWnd* pParent /*=NULL*/)
  : CDialog(CCalculator::IDD, pParent)
    {
    for (int i=0;i<7;i++)
      {
      Value[i] = 0.0f;
      Calc[i] = _T("");
      }
    //{{AFX_DATA_INIT(CCalculator)
    SchemeName = _T("");
    //}}AFX_DATA_INIT
    curschemetype=0;
    }

//--------------------------------------------------

CCalculator::~CCalculator()
  {
  }

//--------------------------------------------------

void CCalculator::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  for (int i=0;i<7;i++)
    {
    DDX_Control(pDX, IDC_AVALUENAME+i, wndValueName[i]);
    DDX_Control(pDX, IDC_AVALUE+i, wndValue[i]);
    DDX_Text(pDX, IDC_AVALUE+i, Value[i]);
    DDX_Text(pDX, IDC_ACALC+i, Calc[i]);
    }
  //{{AFX_DATA_MAP(CCalculator)
  DDX_Control(pDX, IDC_LIST, list);
  DDX_Text(pDX, IDC_SCHEMENAME, SchemeName);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CCalculator, CDialog)
  //{{AFX_MSG_MAP(CCalculator)
  ON_BN_CLICKED(IDC_DEFINE, OnDefine)
    ON_LBN_SELCHANGE(IDC_LIST, OnSelchangeList)
      ON_BN_CLICKED(IDC_SAVE, OnSave)
        ON_BN_CLICKED(IDC_NEW, OnNew)
          ON_BN_CLICKED(IDC_REMOVE, OnRemove)
            //}}AFX_MSG_MAP
            END_MESSAGE_MAP()
              
              /////////////////////////////////////////////////////////////////////////////
              // CCalculator message handlers
              
              #define HALFDLGSIZE 135
              
              BOOL CCalculator::OnInitDialog() 
                {
                CDialog::OnInitDialog();
                CRect rc,rc2(0,0,10,HALFDLGSIZE);
                GetWindowRect(&rc);
                dlgsize=rc.Size();  
                MapDialogRect(&rc2);
                GetClientRect(&rc);
                CSize sz=rc.Size();
                sz.cy=rc2.bottom-rc2.top+GetSystemMetrics(SM_CYCAPTION);
                SetWindowPos(NULL,0,0,sz.cx,sz.cy,SWP_NOMOVE|SWP_NOZORDER);  
                LoadSchemesToLB(NULL);
                SelectScheme("");
                return TRUE;  // return TRUE unless you set the focus to a control
                // EXCEPTION: OCX Property Pages should return FALSE
                }

//--------------------------------------------------

void CCalculator::OnDefine() 
  {
  SetWindowPos(NULL,0,0,dlgsize.cx,dlgsize.cy,SWP_NOMOVE|SWP_NOZORDER);  
  GetDlgItem(IDC_DEFINE)->EnableWindow(FALSE);
  }

//--------------------------------------------------

void CSchemes::HashEntry::SetKey(int type, const char *name)
  {
  this->type=type;
  int len=strlen(name);
  memset(this->name,0,sizeof(this->name));
  strncpy(this->name,name,ArrSize(this->name)-1);
  this->name[ArrSize(this->name)-1]=0;
  }

//--------------------------------------------------

void CSchemes::HashEntry::Save(ostream &str)
  {
  str<<name<<'~';
  str<<type<<'~';
  for (int i=0;i<7;i++)
    str<<calc[i]<<'~';
  }

//--------------------------------------------------

void CSchemes::HashEntry::Load(istream& str)
  {
  SetKey(0,"");
  str.getline(name,ArrSize(name),'~');
  str>>type;
  str.get();
  for (int i=0;i<7;i++) calc[i]="";
  char buffer[256];
  for (int j=0;j<7;j++)
    {
    while (str.get(buffer,sizeof(buffer),'~'),str.peek()!='~' && !(!str))	  
      {calc[j]+=buffer;if (str.eof())return;}
    calc[j]+=buffer;
    str.get();
    }
  }

//--------------------------------------------------

void CCalculator::SelectScheme(const char *name)
  {
  CSchemes::HashEntry *he,key;
  he=schemes.GetScheme(curschemetype,name);
  key.SetKey(curschemetype,name);
  if (he==NULL) he=&key;
  for (int i=0;i<7;i++)
    {
    if (he->calc[i].GetLength()>0 && he->calc[i][0]=='$') 
      {
      char *c=he->calc[i].LockBuffer();
      c++;
      wndValueName[i].SetWindowText(c);
      Value[i]=0.0f;
      wndValueName[i].ShowWindow(SW_SHOW);
      wndValue[i].ShowWindow(SW_SHOW);
      }
    else
      {
      wndValueName[i].ShowWindow(SW_HIDE);
      wndValue[i].ShowWindow(SW_HIDE);
      }
    Calc[i]=he->calc[i];
    }
  SchemeName=he->name;
  UpdateData(FALSE);
  }

//--------------------------------------------------

void CCalculator::OnSelchangeList() 
  {
  int cursel=list.GetCurSel();
  if (cursel==LB_ERR) return;
  CString s;
  list.GetText(cursel,s);
  SelectScheme(s);
  }

//--------------------------------------------------

void CCalculator::LoadSchemesToLB(const char *active)
  {
  list.ResetContent();
  for (CSchemes::HashEntry *he=NULL;he=schemes.Enum(he);)
    if (he->type==curschemetype)
      {
      list.AddString(he->name);
      }
  if (active) list.SelectString(-1,active);
  }

//--------------------------------------------------

static char variables[]=
  {'a'-'a','b'-'a','c'-'a','d'-'a','x'-'a','y'-'a','z'-'a'};

//--------------------------------------------------

void CCalculator::OnSave() 
  {
  UpdateData();
  for (int j=0;j<7;j++) SetVariable(variables[j],12.0);
  for (int i=0;i<7;i++)
    {
    EvalError err;
    if (Calc[i]!="")
      if (Calc[i][0]!='$') 
        {
        const char *c=Evaluate(Calc[i],err);
        if (err==EvalOpenB||err==EvalCloseB||err==EvalOper||err==EvalNum||err==EvalVar)
          {
          AfxMessageBox(WString(IDS_EVALUTIONERROR,variables[i]+'A',c));
          GetDlgItem(IDC_ACALC+i)->SetFocus();
          return;
          }
        }
    }
  CSchemes::HashEntry *he,key;
  he=schemes.GetScheme(curschemetype,SchemeName);
  key.SetKey(curschemetype,SchemeName);
  if (he) 
    {
    *he=key;
    for (int i=0;i<7;i++) he->calc[i]=Calc[i];
    }
  else 
    {
    he=new CSchemes::HashEntry(key);
    for (int i=0;i<7;i++) he->calc[i]=Calc[i];
    schemes.AddScheme(he);
    }
  LoadSchemesToLB(SchemeName);
  SelectScheme(SchemeName);
  schemes.SaveSchemes();
  }

//--------------------------------------------------

int CSchemes::HashEntry::GetSizeOfKey()
  {
  return 32+4;
  }

//--------------------------------------------------

void CCalculator::OnNew() 
  {
  for (int i=0;i<7;i++) Calc[i]="";
  SchemeName="";	
  UpdateData();
  LoadSchemesToLB(NULL);
  SelectScheme("");
  }

//--------------------------------------------------

void CCalculator::OnRemove() 
  {
  UpdateData();
  CSchemes::HashEntry *he;
  he=schemes.GetScheme(curschemetype,SchemeName);
  if (he==NULL) 
    {
    AfxMessageBox(IDS_SCHEMENOTFOUND);
    return;
    }
  if (AfxMessageBox(WString(IDS_ASKSCHEMEREMOVE,he->name),MB_YESNO|MB_ICONQUESTION)==IDYES)
    {
    schemes.RemoveScheme(curschemetype,SchemeName);
    LoadSchemesToLB(NULL);
    SelectScheme("");
    }
  }

//--------------------------------------------------

