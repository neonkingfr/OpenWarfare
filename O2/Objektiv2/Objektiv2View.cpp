// Objektiv2View.cpp : implementation of the CObjektiv2View class
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "MainFrm.h"
#include "Objektiv2Doc.h"
#include "Objektiv2View.h"
#include "PolygonSelection.h"
#include "CreatePrimDlg.h"
#include "IBackgrndMap.h"
#include "TexList.h"
#include <direct.h>
#include "LinWeightDlg.h"
#include "BevelDlg.h"
#include  "gePlane.h"
#include "3DC.h"
#include "ITurnEdge.h"
#include "../ObjektivLib/ObjToolProxy.h"
#include "../ObjektivLib/ObjToolTopology.h"

#ifdef WITH_D3D
#include "d3drenderer/d3drenderer.h"
d3drenderer CObjektiv2View::d3dr;
#endif

#define VIEWMSG_REUPDATE (WM_APP+110)

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CObjektiv2View

CObjektiv2View  *CObjektiv2View::curview;
CObjektiv2View *CObjektiv2View::lastcursview=NULL;


IMPLEMENT_DYNCREATE(CObjektiv2View, CView)

BEGIN_MESSAGE_MAP(CObjektiv2View, CView)
  //{{AFX_MSG_MAP(CObjektiv2View)
  ON_WM_SIZE()
  ON_COMMAND_EX(ID_VIEW_LEFT, OnView)
  ON_WM_RBUTTONDOWN()
  ON_WM_LBUTTONDOWN()
  ON_WM_LBUTTONUP()
  ON_WM_RBUTTONUP()
  ON_WM_MOUSEMOVE()
  ON_COMMAND_EX(ID_LOCK_XY, OnLockXYZ)
  ON_UPDATE_COMMAND_UI(ID_LOCK_X, OnUpdateLockXYZ)
  ON_COMMAND_EX(ID_SET_LOCKX, OnSetLockxyz)
  ON_COMMAND(ID_LOCK_ROTATION, OnLockRotation)
  ON_UPDATE_COMMAND_UI(ID_LOCK_ROTATION, OnUpdateLockRotation)
  ON_WM_SETCURSOR()
  ON_WM_SETFOCUS()
  ON_COMMAND(ID_VIEW_PINCAMER, OnViewPincamer)
  ON_COMMAND(ID_VIEW_FACECULL, OnViewFacecull)
  ON_UPDATE_COMMAND_UI(ID_VIEW_FACECULL, OnUpdateViewFacecull)
  ON_COMMAND(ID_VIEW_GRID, OnViewGrid)
  ON_UPDATE_COMMAND_UI(ID_VIEW_GRID, OnUpdateViewGrid)
  ON_COMMAND(ID_USED3D, OnUseD3D)
  ON_UPDATE_COMMAND_UI(ID_USED3D, OnUpdateUseD3D)
  ON_COMMAND(ID_D3D_TEXTURED, OnD3DTextured)
  ON_UPDATE_COMMAND_UI(ID_D3D_TEXTURED, OnUpdateD3DTextured)
  ON_COMMAND(ID_VIEW_SOLIDFILL, OnViewSolidfill)
  ON_UPDATE_COMMAND_UI(ID_VIEW_SOLIDFILL, OnUpdateViewSolidfill)
  ON_WM_LBUTTONDBLCLK()
  ON_WM_KILLFOCUS()
  ON_COMMAND(ID_EDIT_INSERTPOINT, OnInsertPoint)
  ON_COMMAND_EX(ID_CREATE_PLANE, OnCreatePrim)
  ON_COMMAND(ID_SURFACES_BACKGROUNDMAPPING, OnSurfacesBackgroundmapping)
  ON_COMMAND(ID_SURFACES_BGRFROMFACE, OnSurfacesBgrfromface)
  ON_COMMAND(ID_SURFACES_BGRFROMSEL, OnSurfacesBgrfromsel)
  ON_COMMAND(ID_TEX_MIRROR_X, OnTexMirrorX)
  ON_COMMAND(ID_TEX_MIRROR_Y, OnTexMirrorY)
  ON_COMMAND(ID_TEX_ROTATEL, OnTexRotatel)
  ON_COMMAND(ID_TEX_ROTATER, OnTexRotater)
  ON_COMMAND(ID_TEX_ASPEC, OnTexAspec)
  ON_COMMAND(ID_TEX_UNLOAD, OnTexUnload)
  ON_COMMAND(ID_TEX_TRANSP100,OnTexTransp100)
  ON_COMMAND_EX(ID_VIEW_PAGEZOOM, OnViewPagezoom)
  ON_UPDATE_COMMAND_UI(ID_VIEW_PAGEZOOMSEL, OnUpdateViewPagezoomsel)
  ON_COMMAND_EX(ID_VIEW_ZOOMIN, OnViewZoomin)
  ON_COMMAND(ID_TEX_UNDO, OnTexUndo)
  ON_COMMAND(ID_TEX_GRAY, OnTexGray)
  ON_COMMAND(ID_TEX_LOAD, OnTexLoad)
  ON_COMMAND(ID_TEX_HIDE, OnTexHide)
  ON_WM_KEYDOWN()
  ON_COMMAND(ID_VIEW_LOOKATFACE, OnViewLookatface)
  ON_COMMAND(ID_TEX_ADD, OnTexAdd)
  ON_COMMAND(ID_TEX_SELECT, OnTexSelect)
  ON_COMMAND(ID_VIEW_EXTERNALASCHILD, OnViewExternalaschild)
  ON_WM_MOVE()
  ON_COMMAND(ID_EDIT_LINEARDEFORM, OnEditLineardeform)
  ON_COMMAND(ID_POINTS_BEVEL, OnPointsBevel)
  ON_COMMAND(ID_SURFACES_SETAUTOMAPSELECTION, OnSurfacesSetautomapselection)
  ON_UPDATE_COMMAND_UI(ID_SURFACES_SETAUTOMAPSELECTION, OnUpdateSurfacesSetautomapselection)
  ON_MESSAGE(VIEWMSG_REUPDATE,ReupdateView)
  ON_WM_MOUSEWHEEL()
  ON_COMMAND_EX(ID_LOCK_YZ, OnLockXYZ)
  ON_UPDATE_COMMAND_UI(ID_LOCK_YZ, OnUpdateLockXYZ)
  ON_COMMAND_EX(ID_VIEW_TOP, OnView)
  ON_COMMAND_EX(ID_VIEW_FRONT, OnView)
  ON_COMMAND_EX(ID_VIEW_PROJECTION, OnView)
  ON_COMMAND_EX(ID_VIEW_RIGHT, OnView)
  ON_COMMAND_EX(ID_VIEW_BACK, OnView)
  ON_COMMAND_EX(ID_VIEW_BOTTOM, OnView)
  ON_COMMAND_EX(ID_LOCK_XZ, OnLockXYZ)
  ON_COMMAND_EX(ID_LOCK_X, OnLockXYZ)
  ON_COMMAND_EX(ID_LOCK_Y, OnLockXYZ)
  ON_COMMAND_EX(ID_LOCK_Z, OnLockXYZ)
  ON_UPDATE_COMMAND_UI(ID_LOCK_Y, OnUpdateLockXYZ)
  ON_UPDATE_COMMAND_UI(ID_LOCK_Z, OnUpdateLockXYZ)
  ON_UPDATE_COMMAND_UI(ID_LOCK_XY, OnUpdateLockXYZ)
  ON_UPDATE_COMMAND_UI(ID_LOCK_XZ, OnUpdateLockXYZ)
  ON_COMMAND_EX(ID_SET_LOCKY, OnSetLockxyz)
  ON_COMMAND_EX(ID_SET_LOCKZ, OnSetLockxyz)
  ON_COMMAND_EX(ID_CREATE_BOX, OnCreatePrim)
  ON_COMMAND_EX(ID_CREATE_CYLINDER, OnCreatePrim)
  ON_COMMAND_EX(ID_CREATE_CIRCLE, OnCreatePrim)
  ON_COMMAND_EX(ID_CREATE_SPEHERE, OnCreatePrim)
  ON_COMMAND_EX(ID_CREATE_GEOSPEHERE, OnCreatePrim)
  ON_COMMAND_EX(ID_VIEW_PAGEZOOMSEL, OnViewPagezoom)
  ON_COMMAND_EX(ID_VIEW_ZOOMOUT, OnViewZoomin)
  ON_COMMAND(ID_VIEW_WIRESINSOLID, OnViewWiresinsolid)
  ON_UPDATE_COMMAND_UI(ID_VIEW_WIRESINSOLID, OnUpdateViewWiresinsolid)
  //}}AFX_MSG_MAP
  // Standard printing commands
  ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
  ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
  ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
  ON_WM_RBUTTONDBLCLK()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CObjektiv2View construction/destruction

#define _TOVIEW_INCLUDED_OBJECTIV2_
#include "edgesplit.cpp" //included helper library
#include ".\objektiv2view.h"

static CString locktexts[8];

CObjektiv2View::CObjektiv2View():attpoint(0,0,0)
{
  if (curview==NULL)
  {
    fov=torad(50.0f);
    rotx=torad(80.0f);
    rotz=torad(45.0f);
    pos[XVAL]=pos[YVAL]=pos[ZVAL]=0.0f; pos[WVAL]=1.0f;
    distance=20.0f;
    facecull=true;
    grid=true;
    projection=true;
    scale=0.1f;
    UnlockMode=LockMode=CMS_MAPX|CMS_MAPY;
    locktexts[CMS_MAPX].LoadString(IDS_LOCKX);
    locktexts[CMS_MAPY].LoadString(IDS_LOCKY);
    locktexts[CMS_MAPZ].LoadString(IDS_LOCKZ);
    rotlock=false;
    solid=false;
    solidwires=false;
    curview=this;
  }
  else
  {
    fov=curview->fov;
    rotx=curview->rotx;
    rotz=curview->rotz;
    distance=curview->distance;
    CopyVektor(pos,curview->pos);
    facecull=curview->facecull;
    grid=curview->grid;
    projection=curview->projection;
    scale=curview->scale;
    LockMode=curview->LockMode;
    UnlockMode=curview->UnlockMode;
    rotlock=curview->rotlock;
    glines=curview->glines;
    solid=curview->solid;
    solidwires=curview->solidwires;
  }
  used3d=false;
  d3dtextured=true;
  relmode=false;
  lastrect.SetRectEmpty();
  drawbmp=NULL;
  backbmp=NULL;
  texview=list.Alloc();
  texview->state=-1;
  extattach=NULL;

}

CObjektiv2View::~CObjektiv2View()
{
  CObjektiv2Doc *doc=GetDocument();
  POSITION pos=doc->GetFirstViewPosition();
  while (curview==this)
    curview=(CObjektiv2View *)doc->GetNextView(pos);
  if (doc->g3dview==this) doc->g3dview=curview;
  delete drawbmp;
  delete backbmp;
  //free(texview->SetTexture((TUNIPICTURE *)NULL));
}

BOOL CObjektiv2View::PreCreateWindow(CREATESTRUCT& cs)
{
  // TODO: Modify the Window class or styles here by modifying
  //  the CREATESTRUCT cs

  return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CObjektiv2View drawing

static int drawselection=0; //0- none 1-rect 2-polygon
static CPolygonSelection polselect;

void CObjektiv2View::OnDraw(CDC* pDC)
{
  CObjektiv2Doc* pDoc = GetDocument();
  ASSERT_VALID(pDoc);

  if (pDoc->LodData==NULL) return;
  if (drawbmp==NULL || redraw) 	
  {
    redraw=false;
	OnDrawToBitmap(pDC);
  }
  CDC mDC;
  RECT rc;
  mDC.CreateCompatibleDC(pDC);
  CBitmap *m=mDC.SelectObject(drawbmp);
  GetClientRect(&rc);
  pDC->BitBlt(2,2,rc.right-rc.left-5,rc.bottom-rc.top-5,&mDC,2,2,SRCCOPY);
  DrawFocusFrame(this==curview,pDC);
  if (drawselection || frame->GetEditCommand()==ID_EDIT_PIN)
  {
    C3DC drawct;drawct.Attach(*pDC);
    drawct.SetMatrix(camera.GetTransform());
    drawct.ClipRectFromContext ();
    if (drawselection)	  
      doc->RectangleSelection(drawct,lastrect,false,false);
    else
    {
      ObjectData *obj=pDoc->LodData->Active();
      if (attvalid)
      {        
        CBrush brs(RGB(0,0,255));
        drawct.SelectObject(&brs);
        drawct.Point3D(mxVector3(attpoint[0],attpoint[1],attpoint[2]),C3DCPT_RECTANGLE,5);
      }
    }
    drawct.Detach();
  }
  mDC.SelectObject(m);
  if (!polselect.IsEmpty()) 
    switch (frame->GetEditCommand())
  {
    case ID_CREATE_TRIANGLE:
    case ID_CREATE_FAN: polselect.DrawAsFan(pDC);break;
    case ID_CREATE_STRIP: polselect.DrawAsStrip(pDC);break;
    default:polselect.Draw(pDC,true);break;
  }
  if (frame->GetEditCommand()==ID_EDIT_PAINTVERTICES)
  {
    pDC->SelectStockObject(NULL_BRUSH);
    pDC->SelectStockObject(BLACK_PEN);
    pDC->Ellipse(curpos.x-frame->paintradiusmin,curpos.y-frame->paintradiusmin,
      curpos.x+frame->paintradiusmin,curpos.y+frame->paintradiusmin);
    pDC->Ellipse(curpos.x-frame->paintradiusmax,curpos.y-frame->paintradiusmax,
      curpos.x+frame->paintradiusmax,curpos.y+frame->paintradiusmax);
  }
  if (frame->GetEditCommand()==ID_SURFACES_CYLMAP)
  {
    HVECTOR tv;
    TransformVector(camera.GetTransform(),pDoc->pin,tv);
    XYZW2XYZQ(tv);
    if (tv[ZVAL]>0.0f && tv[WVAL]>0.0f) 
    {
      CPoint cursor;
      GetCursorPos(&cursor);
      ScreenToClient(&cursor);
      pDC->SelectStockObject(BLACK_PEN);
      pDC->MoveTo((int)tv[XVAL],(int)tv[YVAL]);
      pDC->LineTo(cursor);
    }
  }
  if (frame->GetEditCommand()==ID_EDIT_EDITMODES_TOUCHEDGES)
  {
    HVECTOR tv,tv2;
    TransformVector(camera.GetTransform(),help,tv);
    XYZW2XYZQ(tv);
    TransformVector(camera.GetTransform(),lastmark,tv2);
    XYZW2XYZQ(tv2);
    if (tv[ZVAL]>0.0f && tv[WVAL]>0.0f) 
    {
      CPoint cur((int)tv[XVAL],(int)tv[YVAL]);
      pDC->FillSolidRect(cur.x-1,cur.y-1,3,3,RGB(0,255,255));
      if (lastedges[0].face!=-1 && tv2[ZVAL]>0.0f && tv2[WVAL]>0.0f)
      {
        CPoint endc((int)tv2[XVAL],(int)tv2[YVAL]);
        pDC->SelectStockObject(BLACK_PEN);
        pDC->MoveTo(endc);
        pDC->LineTo(cur);
      }
    }	
  }
  if (texview->state==5) texview->DrawBouding(pDC);  
}

LRESULT CObjektiv2View::ReupdateView(WPARAM wParam, LPARAM lParam)
{
  redraw=true;
  InvalidateRect(NULL,FALSE);
  return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CObjektiv2View printing

BOOL CObjektiv2View::OnPreparePrinting(CPrintInfo* pInfo)
{
  // default preparation
  return DoPreparePrinting(pInfo);
}

void CObjektiv2View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
  // TODO: add extra initialization before printing
}

void CObjektiv2View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
  // TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CObjektiv2View diagnostics

#ifdef _DEBUG
void CObjektiv2View::AssertValid() const
{
  CView::AssertValid();
}

void CObjektiv2View::Dump(CDumpContext& dc) const
{
  CView::Dump(dc);
}

CObjektiv2Doc* CObjektiv2View::GetDocument() // non-debug version is inline
{
  ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CObjektiv2Doc)));
  return (CObjektiv2Doc*)m_pDocument;
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CObjektiv2View message handlers

CPen girdcolor(PS_SOLID,1,RGB(128,128,128));
CPen coordcolor(PS_SOLID,1,RGB(128,0,0));
CPen texbackcolor(PS_SOLID,1,RGB(0,255,0));
static CBrush winbgr((COLORREF)GetSysColor(COLOR_WINDOW));


#pragma warning( disable : 4244 )


static void DrawGrid(C3DC& dc, int grsize, int grstep)
{
  int x=-grsize*grstep;
  int a1=x,a2=-x;
  dc.SelectObject(&girdcolor);
  for (int i=-grsize;i<=grsize;i++)
  {
    if (i!=0) 
    {
      dc.MoveTo3D(x,0,a1);dc.LineTo3D(x,0,a2);
      dc.MoveTo3D(a1,0,x);dc.LineTo3D(a2,0,x);
    }
    x+=grstep;
  }
  dc.SelectObject(&coordcolor);
  dc.MoveTo3D(0,a1,0);dc.LineTo3D(0,a2,0);
  dc.MoveTo3D(0,0,a1);dc.LineTo3D(0,0,a2);
  dc.MoveTo3D(a1,0,0);dc.LineTo3D(a2,0,0);
  /*  dc.MoveTo3D(22,0,6);dc.LineTo3D(26,0,2);
  dc.MoveTo3D(26,0,6);dc.LineTo3D(22,0,2);
  dc.MoveTo3D(4,0,22);dc.LineTo3D(4,0,24);
  dc.MoveTo3D(4,0,24);dc.LineTo3D(6,0,26);
  dc.MoveTo3D(4,0,24);dc.LineTo3D(2,0,26);*/
}

HMATRIX p3d2g3d=
{
  {1,0,0,0},
  {0,0,-1,0},
  {0,1,0,0},
  {0,0,0,1},
};

void CObjektiv2View::DrawToBitmapD3D(CBitmap *drawbmp, DWORD width, DWORD height) {
#ifdef WITH_D3D

  if(width == 0 || height == 0)
	  return;	// huh?

  if(projection) {
    camera.SetCamera(0.1f,10000.0f,fov);
    camera.LookAt(pos,rotz,rotx,distance);
  } else {
    camera.SetCamera(scale);
    camera.LookAt(pos,rotz,rotx,100.0f);
  }
  camera.FlushStack(); 
  camera.PushMatrix(p3d2g3d,false);

  static unsigned long long txSum = 0, vxSum = 0, nSum = 0, mSum = 0;
  static DWORD lastBG = 0;

  HWND win = frame->m_hWnd;
  int aa = 8;
  int r = d3dr.initD3D(win, aa);

  if(r < 0) {
	  MessageBox("Unable to initialize Direct3D renderer\nSee log for details", "ERROR", MB_OK|MB_ICONEXCLAMATION);
	  used3d = false;
	  redraw = true;
	  return;
  }

  if(!d3dr.deviceOK()) {
	// Device was lost, recreate it
	RptF("Direct3D device lost, reinitializing");
	d3dr.destroy();
	d3dr.init();
	d3dr.initD3D(win, aa);
	txSum=vxSum=lastBG=0;
  }

  bool normals = config->GetBool(IDS_CFGVIEWNORMALS);
  d3dr.setTexturePath((char*)config->GetString(IDS_CFGPATHFORTEX));

  CObjektiv2Doc *doc=GetDocument();
  d3dr.setDoc(doc);
  ObjectData *obj=doc->LodData->Active();

  unsigned long long csum = d3dr.getModelChecksum(*obj);
  static int lastd3dtextured = -1;
  if(solid || facecull) {
	if((txSum != csum || lastd3dtextured != (int)d3dtextured) || (doc->seltransform && (!d3dtextured||facecull&&!solid))) {
	  d3dr.texPrepareFlush();
      d3dr.createDataTX(*obj, d3dtextured);
	  d3dr.texFlushUnused();
      txSum = csum;
	  lastd3dtextured = d3dtextured;
	}
  }

  if((vxSum != csum||doc->seltransform) && (!solid || solidwires || doc->seltransform))
	d3dr.createDataVL(*obj), vxSum = csum;
  if((nSum != csum||doc->seltransform) && (normals && !doc->seltransform))
	d3dr.createDataN(*obj), nSum = csum;
  if((mSum != csum) && (!doc->seltransform))
	d3dr.createDataM(*obj), mSum = csum;

/*
  if(vxSum != csum || doc->seltransform) {
	  if(!solid || solidwires || doc->seltransform)
		d3dr.createDataVL(*obj);
	  if(normals && !doc->seltransform)
		d3dr.createDataN(*obj);
	  if(!doc->seltransform)
		d3dr.createDataM(*obj);
	  vxSum = csum;
  }
*/
  // Create background LOD data if pointer to data has changed 
  bool drawBg = false;
  if(!projection || config->GetBool(IDS_CFGSHOWLOD3D))  {
		ObjectData *bg=doc->GetBgrnLod();
		if(bg && (DWORD)bg != lastBG)
			d3dr.createDataVL(*bg, true);			
		drawBg = bg!=0;
		lastBG = (DWORD) bg;
  }

  // Setup rendering
  d3dr.setRenderSize(width, height);
  if(projection)
	d3dr.positionCameraPerspective(pos[0], -pos[2], pos[1], rotz, rotx, distance, fov);
  else
	d3dr.positionCameraOrtho(pos[0], -pos[2], pos[1], rotz, rotx, 100.0f, scale);
  d3dr.positionLight();

  // Render
  d3dr.clear(config->GetHex(IDS_CFGBGCOLOR, 0xFFFFFF));

  // Draw background texture using Direct3D
  for(CTextureMapping *texp=NULL;texp=list.EnumTex(texp);) {
	  recalc_texview=false;
	  for(int i=0;i<4;i++) {
		HVECTOR tv;
		TransformVector(camera.GetTransform(),texp->points3d[i],tv);
		CorrectVector(tv);		
		texp->SetIndex(i,tv[XVAL],tv[YVAL]);
	  }
	  d3dr.renderDataBackgrondTexture(texp);
  }

  if(drawBg)
	d3dr.renderDataVL(D3DR_WIREFRAME, true);
  if(solid)
    d3dr.renderDataTX(d3dtextured?D3DR_TEXTURE:D3DR_SOLID);
  if(grid)
    if(projection)
      d3dr.renderGrid();
	else
      d3dr.renderGrid(rotz, rotx);
  if(!solid && facecull)	// Render depth-only pass for poor mans face culling
	d3dr.renderDataTX(D3DR_DEPTHONLY); 
  if(normals && !doc->seltransform)
	d3dr.renderDataN(NULL);
  if(!solid || solidwires || doc->seltransform)
	  d3dr.renderDataVL(D3DR_WIREFRAME+D3DR_VERTICES+D3DR_MASSVERTICES+(facecull?D3DR_DEPTHTEST:0));
  if(doc->gismo_mode!=gismo_hide)
	d3dr.renderGizmo(doc->gismo);

  // Get render result
  int dpitch, dheight;
  void* data = d3dr.getBufferData(&dpitch, &dheight);
  if(data) {
	if(dpitch != width*4) {
	  // Pitch does not match - different size surface or extra data in D3D data
	  BYTE *pdata = new BYTE[width*4*dheight];
	  d3dr::memcpyPitched(pdata, data, dpitch*dheight, width*4, dpitch);
	  drawbmp->SetBitmapBits(width*4*dheight, pdata);
	  delete[] pdata;
	} else {
	  drawbmp->SetBitmapBits(dpitch*dheight, data);
	}
    d3dr.releaseBufferData();
  } else
	  RptF("getBufferData failed");
#endif
}

void CObjektiv2View::OnDrawToBitmap(CDC *pDC)
{
  C3DC drawct;

  if (drawbmp==NULL)
  {
    RECT rc;
    GetClientRect(&rc);
    drawbmp=new CBitmap();
    drawbmp->CreateCompatibleBitmap(pDC,rc.right,rc.bottom);
	drawbmp->SetBitmapDimension(rc.right, rc.bottom);
  }
  if (projection)
  {
    camera.SetCamera(0.1f,10000.0f,fov);
    camera.LookAt(pos,rotz,rotx,distance);
  }
  else
  {
    camera.SetCamera(scale);
    camera.LookAt(pos,rotz,rotx,100.0f);
  }
  drawct.CreateCompatibleDC(pDC);
  camera.FlushStack();  
  camera.PushMatrix(p3d2g3d,false);
  drawct.SetMatrix(camera.GetTransform());
  drawct.SetCull(facecull?1:0);
  drawct.SelectObject(drawbmp);
  drawct.ClipRectFromBitmap(*drawbmp);

  CString *locktext=locktexts+LockMode;
  CObjektiv2Doc *doc=GetDocument();

  if(used3d) {
    // New Direct3D rendering
    DrawToBitmapD3D(drawbmp, drawbmp->GetBitmapDimension().cx, drawbmp->GetBitmapDimension().cy);
  } else {
	  DrawBackground(&drawct);

	  if (grid) 
	  {
		HMATRIX net;
		CalcNetMatrix(net);
		SoucinMatic(net,camera.GetTransform(),net);
		drawct.SetMatrix(net);
		DrawGrid(drawct,10,1);
	  }
	  drawct.SetMatrix(camera.GetTransform());
	  //addition drawing operations here....    
	  if (!projection || config->GetBool(IDS_CFGSHOWLOD3D))
	  {
		ObjectData *bgrn=doc->GetBgrnLod();
		if (doc->prevanim>=doc->LodData->Active()->NAnimations()) doc->prevanim=-1;
		if (doc->prevanim!=-1) 	DrawModel(doc->LodData->Active(),drawct,RGB(127,127,255),doc->prevanim);
		if (bgrn!=NULL) 
		{
		  int anim=-1;
		  if (doc->LodData->Active()->CurrentAnimation()!=-1)
		  {
			float tm=doc->LodData->Active()->GetAnimation(doc->LodData->Active()->CurrentAnimation())->GetTime();
			anim=bgrn->NearestAnimationIndex(tm);
		  }
		  DrawModel(bgrn,drawct,RGB(255,255,127),anim);
		}
	  }
	  /*  HMATRIX mm,mm2,im;
	  HVECTOR v,c;
	  CopyMatice(mm,camera.GetView());
	  SoucinMatic(p3d2g3d,mm,mm2);
	  InverzeMatice(mm2,im);
	  TransformVectorNoTranslate(im,mxVector3(0,0,1),v);
	  NormalizeVector(v);
	  TransformVectorNoTranslate(im,mxVector3(0,0,1),c);
	  NormalizeVector(c);*/
	  if (solid) doc->DrawToView(drawct,solidwires?CObjektiv2Doc::SolidWires:CObjektiv2Doc::Solid);else 
	  {
		//    doc->DrawPointsToView(drawct);	  
		if (doc->DrawToView(drawct,CObjektiv2Doc::WireFrame)==false)
		{
		  PostMessage(VIEWMSG_REUPDATE);
		}
	  }
  }
  //  if (doc->showghost) doc->DrawToViewGhost(drawct);
  drawct.SelectStockObject(BLACK_PEN);
  drawct.Point3D(doc->pin,C3DCPT_CROSS,10);  
  if (doc->pinlock) 
  {drawct.SelectStockObject(NULL_BRUSH);drawct.Point3D(doc->pin,C3DCPT_CIRCLE,6);}
  //if (drawselection)doc->RectangleSelection(drawct,lastrect,false,false);
  if (frame->GetEditCommand()==ID_SURFACES_BACKGROUNDTEXTURE)
    texview->DrawBouding(&drawct);
  glines.Draw(drawct);
  ObjectData *obj=doc->LodData->Active();
  int an=obj->CurrentAnimation();
  drawct.SetBkColor(config->GetHex(IDS_CFGBGCOLOR,0xFFFFFF));
  if(used3d)
	drawct.SetTextColor(RGB(0, 100, 175));
  if (frame->GetEditCommand()==ID_SURFACES_BACKGROUNDTEXTURE)	
    drawct.TextOut(2,2,WString(IDS_TOPSTATUSTEX,framename,texview->textureName));
  else if (an==-1 || an>=obj->NAnimations() )
    drawct.TextOut(2,2,WString(IDS_TOPSTATUS,framename, *locktext));
  else
    drawct.TextOut(2,2,WString(IDS_TOPSTATUSFRAME,framename,obj->GetAnimation(an)->GetTime(), *locktext));
  if (redraw)  InvalidateRect(NULL,FALSE);

}

void CObjektiv2View::OnSetFocus(CWnd* pOldWnd) 
{
  CView::OnSetFocus(pOldWnd);
  if (curview) curview->DrawFocusFrame(false);
  curview=this;
  DrawFocusFrame(true);  	
}

void CObjektiv2View::DrawFocusFrame(bool focused, CDC *dc)
{
  CDC *pdc;
  if (dc==NULL) pdc=GetDC();else pdc=dc;
  CPen pen;

  if (focused)	
    pen.CreatePen(PS_SOLID,3,(COLORREF)0);
  else	
    pen.CreatePen(PS_SOLID,3,config->GetHex(IDS_CFGBGCOLOR,0xFFFFFF));	
  RECT rc;
  GetClientRect(&rc);
  rc.right--;
  rc.bottom--;
  CPen *old=pdc->SelectObject(&pen);
  pdc->SelectStockObject(HOLLOW_BRUSH);
  pdc->Rectangle(&rc);
  pdc->SelectObject(old);
  if (dc==NULL) ReleaseDC(pdc);
}

void CObjektiv2View::OnSize(UINT nType, int cx, int cy) 
{
  CView::OnSize(nType, cx, cy);

  delete drawbmp;
  drawbmp=NULL;
  float xs=cx*0.5f,ys=cy*0.5f,xr=__min(xs,ys);
  camera.SetCanvas(xs,ys,xr,-xr);	
  recalc_texview=true;
  /*  if (extattach)
  {
  CRect rc;
  GetClientRect(&rc);
  ClientToScreen(&rc);
  ::SetWindowPos(extattach,HWND_TOPMOST,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,0);
  }*/
}

BOOL CObjektiv2View::OnView(UINT com) 
{
  projection=false;
  rotlock=true; 
  switch (com)
  {
  case ID_VIEW_TOP: 
    rotx=torad(0.0f);
    rotz=torad(0.0f);
    LockMode=CMS_MAPX|CMS_MAPY;
    facecull=false;
    framename.LoadString(IDS_TOPVIEW);
    break;
  case ID_VIEW_FRONT:
    rotz=0.0f;
    rotx=torad(90.0f);	
    facecull=false;
    LockMode=CMS_MAPX|CMS_MAPY;
    framename.LoadString(IDS_FRONTVIEW);
    break;
  case ID_VIEW_LEFT:
    rotz=torad(270.0f);
    rotx=torad(90.0f);	
    facecull=false;
    LockMode=CMS_MAPX|CMS_MAPY;
    framename.LoadString(IDS_LEFTVIEW);
    break;
  case ID_VIEW_PROJECTION:
    rotx=torad(80.0f);
    rotz=torad(45.0f);
    projection=true;
    rotlock=false;
    LockMode=CMS_MAPX|CMS_MAPY|CMS_MAPZ;
    framename.LoadString(IDS_PROJECTVIEW);
    break;
  case ID_VIEW_RIGHT:
    rotx=torad(90.0f);
    rotz=torad(90.0f);
    LockMode=CMS_MAPX|CMS_MAPY;
    facecull=false;
    framename.LoadString(IDS_RIGHTVIEW);
    break;
  case ID_VIEW_BACK:
    rotx=torad(90.0f);
    rotz=torad(180.0f);
    LockMode=CMS_MAPX|CMS_MAPY;
    facecull=false;
    framename.LoadString(IDS_BACKVIEW);
    break;
  case ID_VIEW_BOTTOM:
    rotx=torad(180.0f);
    rotz=torad(0.0f);
    LockMode=CMS_MAPX|CMS_MAPY;
    facecull=false;
    framename.LoadString(IDS_BOTTOMVIEW);
    break;
  }
  UnlockMode=LockMode;
  Invalidate();
  recalc_texview=true;
  return TRUE;
}

void CObjektiv2View::Invalidate()
{
  _attPointCache.Clear();
  InvalidateNoClearAttachDB();
}

void CObjektiv2View::InvalidateNoClearAttachDB()
{
  redraw=true;
  CView::Invalidate(FALSE);
}

static CPoint relpoint;
static CPoint lockpoint;
static VecT attbegtransf;
static bool attbegvalid;


#define RM_NONE 0
#define RM_ISMOVING 1
#define RM_LOCKED 2
#define RM_NOMOVING 3

static float rlast=0.0f;
static void MouseRotationReset()
{
  rlast=0;
}

static float MouseRotationHelper(HMATRIX mm, CPoint pt1, CPoint pt2, bool pin)
{  
  CPoint ptm;
  HVECTOR v,tv;
  if (pin) CopyVektor(v,doc->pin);
  else GetObjectCenter(true,v,NULL);
  TransformVector(mm,v,tv);
  CorrectVector(tv);
  ptm.x=(int)tv[XVAL];
  ptm.y=(int)tv[YVAL];
  pt1-=ptm;
  pt2-=ptm;
  float r1,r2;
  r1=atan2((float)pt1.x,(float)pt1.y);
  r2=atan2((float)pt2.x,(float)pt2.y);
  float out=r2-r1;
  while (out-rlast<FPI) out+=2*FPI;
  while (out-rlast>FPI) out-=2*FPI;
  rlast=out;
  return out;
}

static int BeginRelMoving(CWnd *msCapture)
{
  if (GetCapture()!=NULL) return RM_LOCKED;
  msCapture->SetCapture();
  GetCursorPos(&relpoint);
  return RM_ISMOVING;
}

static int IsRelMoving(CWnd *msCapture)
{
  HWND wnd=GetCapture();
  if (wnd==NULL) return RM_NONE;
  if (wnd!=msCapture->m_hWnd) return RM_LOCKED;
  return RM_ISMOVING;
}

static bool ms_swapper=false;

static int GetRelMoving(CPoint& pos)
{
  GetCursorPos(&pos);
  pos-=relpoint;
  if (ms_swapper)
  {
    if (pos.x!=0 || pos.y!=0) 
      SetCursorPos(relpoint.x,relpoint.y);
    else
      ms_swapper=false;
    return RM_NOMOVING;
  }
  else
  {
    TRACE2("MoveMouse: %d, %d\n",pos.x,pos.y);
    if (pos.x==0 && pos.y==0) return RM_NOMOVING;
    if (config->GetBool(IDS_CFGTABLET))
    {
      GetCursorPos(&relpoint);
      ms_swapper=false;
    }
    else
    {
      SetCursorPos(relpoint.x,relpoint.y);
      ms_swapper=true;
    }
    return RM_ISMOVING;
  }
}

static void DoneRelMoving()
{
  ReleaseCapture();
}

static void LockAttach(CPoint &pt,char lockmode)
{
  if (!(lockmode & CMS_MAPX)) pt.x=lockpoint.x;
  if (!(lockmode & CMS_MAPY)) pt.y=lockpoint.y;
}

UINT shiftstates;
int gmvert,gmhorz,gmflags;

inline void AltTest(UINT &nFlags)
{
  if (GetKeyState(VK_MENU) & 0x80) 
    nFlags|=MK_ALT;
}

class CINewFace: public CICommand
{
  int indexlist[MAX_DATA_POLY];
  int count;
public:
  CINewFace(istream& str) 
  {
    datard(str,count);
    datardp(str,indexlist);
  }
  CINewFace(int count,...)
  {
    va_list list;
    va_start(list,count);
    for (int i=0;i<count;i++) indexlist[i]=va_arg(list,int);
    this->count=count;
  }
  virtual int Execute(LODObject *obj)
  {
    ObjectData *odata=obj->Active();
    int n=odata->NFaces();
    FaceT face(odata,odata->NewFace());
    face.SetTexture("");
    face.SetN(count);
    for (int i=0;i<count;i++)
    {
      face.SetPoint(i,indexlist[i]);
      face.SetNormal(i,0);
      face.SetUV(i,0,0);
    }	  
    odata->FaceSelect(n);
    return 0;
  }
  virtual void Save(ostream &str) 
  {datawr(str,count);datawrp(str,indexlist);}
  virtual int Tag() const 
  {return CITAG_NEWFACE;}
};

class CINewPolygon: public CICommand
{
  int *indexlist;
  int count;
public:
  CINewPolygon(istream& str) 
  {
    datard(str,count);
    datardf(str,indexlist,count);
  }
  CINewPolygon(int count,const int *indexes)
  {
    indexlist=new int[count];
    for (int i=0;i<count;i++) indexlist[i]=indexes[i];
    this->count=count;
  }
  ~CINewPolygon()
  {
    delete [] indexlist;
  }
  virtual int Execute(LODObject *obj)
  {
    ObjectData *odata=obj->Active();
    if (odata->GetTool<ObjToolTopology>().TessellatePolygon(count,indexlist)==false && obj==doc->LodData)
    {
      AfxMessageBox(IDS_TESSELATIONFAILED,MB_ICONINFORMATION);
    }
    return 0;
  }
  virtual void Save(ostream &str) 
  {datawr(str,count);
  indexlist=new int[count];
  datawrf(str,indexlist,count);}
  virtual int Tag() const 
  {return CITAG_NEWPOLYGON;}
};


CICommand *CINewFace_Create(istream& str)
{
  return new CINewFace(str);
}

CICommand *CINewPolygon_Create(istream& str)
{
  return new CINewPolygon(str);
}

void CObjektiv2View::OnRButtonDown(UINT nFlags, CPoint point) 
{
  int mode=frame->GetEditCommand();
  lockpoint=point;
  AltTest(nFlags);
  shiftstates=nFlags & (MK_SHIFT | MK_CONTROL | MK_ALT);
  if (shiftstates == MK_ALT || shiftstates==(MK_ALT | MK_CONTROL))	
  {BeginRelMoving(this);list.ForceFastTex(true);}
  else if (mode==ID_EDIT_EDITMODES_TOUCHEDGES)
    lastedges[0].face=-1;
  else
    if (mode==ID_SURFACES_BACKGROUNDTEXTURE) TexContextMenu();
    else if (mode==ID_EDIT_TOUCH || mode==ID_EDIT_SELECTVERTICES || mode==ID_EDIT_SELECTPOLYS || mode==ID_EDIT_SELECTFACES || mode==ID_EDIT_SELECTOBJECTS || mode==ID_VIEW_ZOOM || mode==ID_EDIT_PIN||mode==ID_EDIT_PAINTVERTICES)
    {
      relpoint=point;
      if (mode==ID_EDIT_PIN)
      {
        attbegvalid=FindAttachPoint(point,camera.GetTransform(),attbegtransf);
      }
      SetCapture();
      OnSetCursor(this,HTCLIENT,0);
    }
   if (!polselect.IsEmpty()) polselect.Back();
}

void CObjektiv2View::OnLButtonDown(UINT nFlags, CPoint point) 
{
  int mode=frame->GetEditCommand();
  lockpoint=point;
  AltTest(nFlags);
  shiftstates=nFlags & (MK_SHIFT | MK_CONTROL | MK_ALT);
  if (shiftstates == MK_ALT || shiftstates==(MK_ALT | MK_CONTROL))	
  {BeginRelMoving(this);list.ForceFastTex(true);}
  else if (shiftstates == MK_CONTROL && mode==ID_SURFACES_BACKGROUNDTEXTURE)
  {
    texview->state=texview->GetIndex(point);
    relpoint=point;
    texview->SaveUndo();
    if (texview->state==-1) SetCursor(theApp.LoadCursor(IDI_OBJROTATE));
    else SetCursor(theApp.LoadStandardCursor(IDC_SIZEALL));
    SetCapture();
  }
  else if (shiftstates == MK_SHIFT && mode==ID_SURFACES_BACKGROUNDTEXTURE)
  {
    contextmenupt=point;
    OnTexSelect();
  }
  else
  {
    if ((gmflags=glines.GuideCursor(point,&gmvert,&gmhorz))!=0) SetCapture();
    if (mode==ID_EDIT_PAINTVERTICES) DoPaintVertices(point);
    else
      if ((mode==ID_EDIT_SELECTVERTICES || mode==ID_EDIT_SELECTFACES || mode==ID_EDIT_SELECTOBJECTS || mode==ID_SURFACES_BACKGROUNDTEXTURE ||
        mode==ID_VIEW_ZOOM) )
        if (frame->laso && mode!=ID_SURFACES_BACKGROUNDTEXTURE)		
        {
          if (polselect.IsEmpty()) polselect.AddPoint(point);
          polselect.AddPoint(point);
        }
        else
        {
          int idx=texview->GetIndex(point);
          bool inside=texview->TestPointInside(point);
          if (mode==ID_SURFACES_BACKGROUNDTEXTURE && (idx>=0||inside))
            if (idx>=0) 
            {texview->state=idx;	texview->SaveUndo();}
            else
            {
              texview->MarkRefPoint(point);
              texview->state=5;
              texview->SaveUndo();
            }
          else
          {
            lastrect.left=lastrect.right=point.x;
            lastrect.top=lastrect.bottom=point.y;
            drawselection=true;
            SetCapture();
            texview->state=-1;
          }
        }		  
        if (mode==ID_EDIT_PIN)
        {
          if (attvalid)
          {        
            CopyVektor(doc->pin,mxVector3(attpoint[0],attpoint[1],attpoint[2]));
          }
          else
          {
            LPHVECTOR mpos=MousePtTo3D(point,doc->pin);
            CopyVektor(doc->pin,mpos);
          }
          Invalidate();
        }
        if (mode==ID_CREATE_TRIANGLE || mode==ID_CREATE_QUAD ||  mode==ID_CREATE_FAN || mode==ID_CREATE_STRIP || mode==ID_DRAW_POLYGON)
        {
          bool ep=polselect.IsEmpty();
          bool sep=false;
          int idx;
          if (ep) ptattach.Create(GetDocument()->LodData->Active(),camera.GetTransform());
          idx=ptattach.AttachMouse(point);
          if (!ep) polselect.RemoveDouble();
          else polselect.AddPoint(point,idx);
          polselect.AddPoint(point,idx);		
          switch (mode)
          {
          case ID_CREATE_TRIANGLE: if (polselect.GetCount()>3)
                                   {
                                     comint.Begin(WString(IDS_DRAWTRIANGLE));
                                     comint.Run(new CINewFace(3,polselect[0],polselect[1],polselect[2]));
                                     sep=true;
                                     GetDocument()->UpdateAllViews(NULL);
                                   }
                                   break;
          case ID_CREATE_QUAD:	 if (polselect.GetCount()>4)
                                 {
                                   comint.Begin(WString(IDS_DRAWQUAD));
                                   comint.Run(new CINewFace(4,polselect[0],polselect[1],polselect[2],polselect[3]));
                                   sep=true;
                                   GetDocument()->UpdateAllViews(NULL);
                                 }
          case ID_DRAW_POLYGON: if (polselect.GetCount()>3 && polselect[0]==polselect[polselect.GetCount()-1])
                                {
                                  polselect.RemoveDouble();
                                  comint.Begin(WString(IDS_CREATEPOLYGON));
                                  comint.Run(new CINewPolygon(polselect.GetCount()-1,polselect));
                                  sep=true;
                                  GetDocument()->UpdateAllViews(NULL);
                                }
                                break;
                                break;

          }
          if (sep) polselect.Reset();
        }
        if (frame->GetEditCommand()==ID_EDIT_EDITMODES_TOUCHEDGES)
        {
          HVECTOR v2;
          HVECTOR ms,ms2;
          CopyVektor(ms,MousePtTo3D(point,GetDocument()->pin,v2));
          CopyVektor(ms2,MousePtTo3D(CPoint(point.x+3,point.y+3),GetDocument()->pin,NULL));
          RozdilVektoru(ms2,ms);
          float dd=ModulVektoru(ms2);
          if (SplitFaces(ms,v2,GetDocument()->LodData->Active(),dd,(nFlags & MK_CONTROL)!=0))
          {
            GetDocument()->UpdateAllViews(NULL);
          }
        }

  }
}

void CObjektiv2View::OnLButtonUp(UINT nFlags, CPoint point) 
{
  if ((shiftstates==MK_ALT || shiftstates==(MK_ALT | MK_CONTROL)))
  {
    if (!(nFlags & MK_RBUTTON))
    {
      DoneRelMoving();
      shiftstates=0;
      list.ForceFastTex(false);Invalidate();recalc_texview=true;
    }
    return;
  }
  else
    if (~shiftstates & MK_SHIFT && frame->GetEditCommand()==ID_SURFACES_CYLMAP)
    {
      frame->SetEditCommand(frame->GetPrevEditCommand());
      frame->SetEditCommand(frame->GetEditCommand());
      GetDocument()->OnCylindricMapping(MousePtTo3D(point,doc->pin));
    }

    if (gmvert!=-1 || gmhorz!=-1)
    {
      gmvert=gmhorz=-1;
      gmflags=0;
      ReleaseCapture();
    }
    if (frame->GetEditCommand()==ID_EDIT_PAINTVERTICES) EndPaintVertices();
    if (frame->GetEditCommand()==ID_EDIT_PIN) GetDocument()->UpdateAllViews(NULL);
    if (shiftstates == MK_CONTROL && frame->GetEditCommand()==ID_SURFACES_BACKGROUNDTEXTURE)
    {
      texview->DoCommand();Invalidate();	recalc_texview=true;
      for (int i=0;i<4;i++)
        CopyVektor(texview->points3d[i],MousePtTo3D(texview->GetPos(i),pos));
      GetDocument()->UpdateStatus();
      ReleaseCapture();
    }
    else if (texview->state==5) 
    {
      for (int i=0;i<4;i++)
        CopyVektor(texview->points3d[i],MousePtTo3D(texview->GetPos(i),pos));
      Invalidate();
      recalc_texview=true;
    }
    else
      if (drawselection || ((frame->GetEditCommand()==ID_EDIT_TOUCH || frame->GetEditCommand()==ID_EDIT_SELECTPOLYS)  && nFlags!=MK_ALT))
      {
        if (frame->GetEditCommand()==ID_SURFACES_BACKGROUNDTEXTURE)
        {
          texview->SetRect(&lastrect);
          Invalidate();
          for (int i=0;i<4;i++) CopyVektor(texview->points3d[i],MousePtTo3D(texview->GetPos(i),pos));	  
          recalc_texview=true;
        }
        else if (frame->GetEditCommand()==ID_VIEW_ZOOM)
          MakeZoomRect(lastrect);
        else
        {
          C3DC ddc; CDC *wdc=GetDC();
          ddc.CreateCompatibleDC(wdc);ReleaseDC(wdc);
          ddc.SelectObject(drawbmp);
          ddc.SetMatrix(camera.GetTransform());
          if (frame->GetEditCommand()==ID_EDIT_TOUCH)		
            doc->TouchFace(ddc,point,(nFlags & MK_CONTROL)!=0,(nFlags & MK_SHIFT)!=0,false);
          else if (frame->GetEditCommand()==ID_EDIT_SELECTPOLYS)
            doc->TouchFace(ddc,point,(nFlags & MK_CONTROL)!=0,(nFlags & MK_SHIFT)!=0,true);
          else
            doc->RectangleSelection(ddc,lastrect,true,(nFlags & MK_CONTROL)!=0,(nFlags & MK_SHIFT)!=0);
        }
        drawselection=false;
        lastrect.SetRectEmpty();
        ReleaseCapture();
      }
      else if (frame->GetEditCommand()==ID_EDITMODES_TURNEDGE && nFlags!=MK_ALT)
      {
        TurnEdgeSupp(point);
      }
      texview->state=-1;
}

void CObjektiv2View::OnRButtonUp(UINT nFlags, CPoint point) 
{
  int mode=frame->GetEditCommand();
  if ((shiftstates == MK_ALT || shiftstates==(MK_ALT | MK_CONTROL)))
  {
    if (!(nFlags & MK_LBUTTON))
    {
      DoneRelMoving();
      shiftstates=0;
      list.ForceFastTex(false);Invalidate();recalc_texview=true;
    }
    return;
  }  
  else if (mode==ID_EDIT_TOUCH || mode==ID_EDIT_SELECTVERTICES || mode==ID_EDIT_SELECTPOLYS || mode==ID_EDIT_SELECTFACES || mode==ID_EDIT_SELECTOBJECTS ||  mode==ID_VIEW_ZOOM || mode==ID_EDIT_PIN||mode==ID_EDIT_PAINTVERTICES)
  {
   if (mode==ID_EDIT_PIN && attbegvalid)
      {
        VecT attendtransf;
        if (FindAttachPoint(point,camera.GetTransform(),attendtransf))
        {
          HMATRIX trn;
          attendtransf=CopyVectWithLock(attendtransf,attbegtransf);
          Translace(trn,attendtransf[0]-attbegtransf[0],attendtransf[1]-attbegtransf[1],attendtransf[2]-attbegtransf[2]);
          doc->TransformPreview(trn);
          if (doc->pinlock) 
          {
            CopyVektor(doc->pin,mxVector3(attendtransf[0],attendtransf[1],attendtransf[2]));
          }
        }
      }
    doc->ApplyPreviewTransform (shiftstates==MK_SHIFT) ;
    MouseRotationReset();
    doc->EndTransformPreview();
    shiftstates=0;
    ReleaseCapture();
  }

}

class GenerateAttachPointDB
{
  mutable BTree<AttachPoint2DInfo> &output;
  const LPHMATRIX camera;
public:
  GenerateAttachPointDB(BTree<AttachPoint2DInfo> &output, const LPHMATRIX &camera):output(output),camera(camera) {}
  bool operator() (const VecT &vc) const
  {
    HVECTOR vect;
    TransformVector(camera,mxVector3(vc[0],vc[1],vc[2]),vect);
    if (vect[WVAL]>0.0f && vect[ZVAL]>0.0f)
    {
      CorrectVector(vect);
      CPoint pt((int)(round(vect[0])+0.5),(int)(round(vect[1])+0.5));
      AttachPoint2DInfo nwpt(pt.x,pt.y,vect[2],vc),*fnd;
      fnd=output.Find(nwpt);
      if (fnd==0) output.Add(nwpt);
      else
      {
        if (fnd->_Z>nwpt._Z) *fnd=nwpt;
      }
    }
    return false;
  }  
};


bool CObjektiv2View::FindAttachPoint(const CPoint &ms, const LPHMATRIX camera, VecT &found)
{
  if (_attPointCache.Size()==0)
  {
    ObjectData *obj=GetDocument()->LodData->Active();
    int i;
    const Selection *hid=obj->GetHidden();
    GenerateAttachPointDB functor(_attPointCache,camera);
    const Selection *proxies=obj->GetTool<ObjToolProxy>().GetShowProxySelection();
    for (i=0;i<obj->NPoints();i++) if (!hid->PointSelected(i) && (proxies==0 || !proxies->PointSelected(i)))
    {
      PosT &pos=obj->Point(i);
      functor(pos);
    }  
    if (proxies)
    {
      EnumAllProxyPoints(obj,hid,proxies,GetDocument()->_proxyCache,GetDocument()->GetProxyLevel(),functor,0);
    }
  }
  BTreeIterator<AttachPoint2DInfo> iter(_attPointCache);
  AttachPoint2DInfo fnd(ms.x-5,ms.y,0,VecT());
  AttachPoint2DInfo *pfound,*best=0;
  iter.BeginFrom(fnd);
  int distance=0x7FFFFFFF;
  while (pfound=iter.Next())
  {
    if (pfound->_dcX>ms.x+5) break;
    int dist=fnd.Distance2(*pfound);
    if (dist<distance) 
    {
      distance=dist;
      best=pfound;
    }
  }
  if (best)
  {
    int xdif=abs(ms.x-best->_dcX);
    int ydif=abs(ms.y-best->_dcY);
    if (xdif<=5 && ydif<=5)
    {
      found=best->vc;
      return true;
    }
  }
  return false;
}



void CObjektiv2View::OnMouseMove(UINT nFlags, CPoint point) 
{
  bool redrw=false;  
  CObjektiv2Doc *doc=GetDocument();
  if (gmflags & CGL_VERT) 
  {glines.SetGuideLine(CGL_VERT,gmvert,point.x);redrw=true;}
  if (gmflags & CGL_HORZ) 
  {glines.SetGuideLine(CGL_HORZ,gmhorz,point.y);redrw=true;}
  if (shiftstates == MK_ALT || shiftstates==(MK_ALT|MK_CONTROL))
  {
    ClientToScreen(&point);
    if (IsRelMoving(this)==RM_ISMOVING && GetRelMoving(point)==RM_ISMOVING)
    {	  	  	  
      switch (nFlags & (MK_LBUTTON | MK_RBUTTON))
      {	
      case MK_RBUTTON:		  
        if (!rotlock)
        {
          rotz+=torad(point.x*0.25f);
          rotx+=torad(point.y*0.25f);	
          while (rotx<0.0) rotx+=2*FPI;
          while (rotz<0.0) rotz+=2*FPI;
          while (rotx>2*FPI) rotx-=2*FPI;
          while (rotz>2*FPI) rotz-=2*FPI;
          if (!projection) framename.LoadString(IDS_USERVIEW);
          break;		  		  
        }
      case MK_LBUTTON: 
        {
          float x,y,z;
          MapMouse(point,0,x,y,z);
          float p=(projection?distance:0.5f/scale);
          pos[XVAL]+=x*p*0.01f;
          pos[YVAL]+=y*p*0.01f;
          pos[ZVAL]+=z*p*0.01f;
          break;
        }
      case MK_LBUTTON | MK_RBUTTON: 
        if (shiftstates & MK_CONTROL)
        {
          float x,y,z;
          MapMouse(CPoint(0,0),point.y,x,y,z);
          pos[XVAL]+=x*0.01f;
          pos[YVAL]+=y*0.01f;
          pos[ZVAL]+=z*0.01f;
          break;
        }
        else
          if (projection)
            distance+=(float)point.y*0.1f;
          else
            scale*=(float)1.0f-(point.y*0.01f);
        break;
      }
      redrw=true;
      recalc_texview=true;
    }
  }
  else
  {
    if (gmflags==0) 
    {
      glines.GuideCursor(point);
      //	  LockAttach(point,LockMode);
      LPHVECTOR mpos=MousePtTo3D(point,doc->pin);
      frame->UpdateXYZ(mpos);
      int mode=frame->GetEditCommand();
      if (mode==ID_EDIT_PIN)
        if (nFlags & MK_LBUTTON)
        {
          CopyVektor(doc->pin,mpos);
          Invalidate();
        }
        else if (~nFlags & MK_RBUTTON)
        {
          VecT fndVect(0,0,0);
          bool fnd=FindAttachPoint(point,camera.GetTransform(),fndVect);
          if (fnd!=attvalid || fndVect!=attpoint) InvalidateNoClearAttachDB();
          attvalid=fnd;
          attpoint=fndVect;
        }		
      else attvalid=false;
      if (nFlags & MK_LBUTTON && shiftstates == MK_CONTROL && mode==ID_SURFACES_BACKGROUNDTEXTURE)
      {
        if (texview->state==-1) 
        {
          CPoint center=texview->GetCenter();
          CPoint ref=point-center;
          CPoint ref2=relpoint-center;
          float rot=todec(atan2((float)ref.y,(float)ref.x)-atan2((float)ref2.y,(float)ref2.x));
          float f=config->GetInt(IDS_CFGROTATESTEP);
          if (f!=0.0f) rot=round(rot/f)*f;
          texview->Rotate(rot,center);
          frame->SetStatus(WString(IDS_ROTATION_STATUS,-rot));
        }
        else texview->Scale(texview->state,point);
        Invalidate();
        recalc_texview=true;
      }
      else if (nFlags & MK_LBUTTON && texview->state>=0 && texview->state<5)
      {
        texview->SetIndex(texview->state,point.x,point.y);
        if (!config->GetBool(IDS_CFGALLOWNOLINEAR))
          texview->CheckPoints(texview->state);
        for (int i=0;i<4;i++)
          CopyVektor(texview->points3d[i],MousePtTo3D(texview->GetPos(i),pos));
        Invalidate();
        recalc_texview=true;
      }
      if (mode==ID_SURFACES_CYLMAP) InvalidateRgn(NULL,FALSE);
      if (mode==ID_EDIT_PAINTVERTICES)
      {
        if (nFlags & MK_LBUTTON) DoPaintVertices(point);
        if (lastcursview!=this && lastcursview!=NULL)
        {
          lastcursview->curpos=CPoint(-1000,-1000);
          lastcursview->InvalidateRect(NULL,FALSE);
        }
        curpos=point;
        InvalidateRect(NULL,FALSE);
      }
      if ((mode==ID_EDIT_SELECTVERTICES || mode==ID_EDIT_SELECTFACES || mode==ID_EDIT_SELECTOBJECTS || mode==ID_SURFACES_BACKGROUNDTEXTURE
        || mode==ID_VIEW_ZOOM) )
        if (frame->laso && mode!=ID_SURFACES_BACKGROUNDTEXTURE)
        {
          polselect.ModifyLast(point);
          InvalidateRect(NULL,FALSE);
        }
        else 
          if (texview->state==5)	
          {
            texview->MoveToPoint(point);
            InvalidateRect(NULL,FALSE);
          }
          else
            if(drawselection)
            {
              lastrect.right=point.x;
              lastrect.bottom=point.y;
              InvalidateRect(NULL,FALSE);
            }
            if (mode==ID_CREATE_TRIANGLE || mode==ID_CREATE_QUAD ||  mode==ID_CREATE_FAN || mode==ID_CREATE_STRIP || mode==ID_DRAW_POLYGON)
            {
              int idx=ptattach.AttachMouse(point);
              polselect.ModifyLast(point,idx);
              InvalidateRect(NULL,FALSE);
            }	  
            if (nFlags & MK_RBUTTON && (mode==ID_EDIT_SELECTVERTICES || mode==ID_EDIT_SELECTPOLYS || mode==ID_EDIT_SELECTFACES || mode==ID_EDIT_SELECTOBJECTS || mode==ID_EDIT_TOUCH || mode==ID_VIEW_ZOOM || mode==ID_EDIT_PIN||mode==ID_EDIT_PAINTVERTICES) && GetCapture()==this)
            {
              HMATRIX mm;
              CPoint diff=point-relpoint;
              float f;
              switch (shiftstates & (MK_SHIFT | MK_CONTROL))
              {
              case MK_SHIFT|MK_CONTROL: 
                if (doc->gismo_mode!=gismo_hide && doc->localAxisMode!=false)
                  CalcScaleOnLocalAxis(1.0f+diff.y*0.01f,mm,doc->pinlock);
                else if (frame->uniformscale)
                  {f=1.0f-diff.y*0.01f;CalcScaleOnPlane(f,mm,doc->pinlock);}
                else
                  CalcNonuniformScaleOnPlane(1.0f+diff.x*(~LockMode & CMS_MAPX?0:0.01f),1.0f+diff.y*(~LockMode & CMS_MAPY?0:0.01f),mm,doc->pinlock);
                break;
              case MK_CONTROL: f=MouseRotationHelper(camera.GetTransform(),relpoint,point,doc->pinlock);            
                if (doc->gismo_mode==gismo_hide || doc->localAxisMode==false)
                  CalcRotationOnPlane(f,mm,doc->pinlock);  
                else
                  CalcRotationOnLocalAxis(f,mm);
                break;
              default: 
                if (doc->gismo_mode==gismo_hide || doc->localAxisMode==false)
                {                
                   if (mode==ID_EDIT_PIN && attbegvalid)
                    {
                      VecT attendtransf;
                      if (FindAttachPoint(point,camera.GetTransform(),attendtransf))
                      {                 
                        attvalid=true;
                        attpoint=CopyVectWithLock(attendtransf,attbegtransf);

                        Translace(mm,attpoint[0]-attbegtransf[0],attpoint[1]-attbegtransf[1],attpoint[2]-attbegtransf[2]);                     
                      }
                      else
                      {
                        attvalid=false;
                        MapMouseMovement(relpoint,point,mm,false);
                      }
                    }
                   else
                     MapMouseMovement(relpoint,point,mm,false);
                }
                else
                {
                  float x,y,z;
                  MapMouse(diff,0,x,y,z);
                  float p=(projection?distance:0.5f/scale);
                  float f=(x)*p*0.01f;;
                  CalcMoveOnLocalAxis(f,mm,false);
                }

                break;

              }
              doc->TransformPreview(mm);
              redrw=true;
            }
            if (mode==ID_EDIT_EDITMODES_TOUCHEDGES)
            {
              HVECTOR v1,v2;
              LPHVECTOR ms=MousePtTo3D(point,GetDocument()->pin,v2);
              if (nFlags & MK_CONTROL)
              {
                if (FindNearestPoints(ms,v2,GetDocument()->LodData->Active(),v1))
                {
                  CopyVektor(help,v1);
                  InvalidateRect(NULL,FALSE);
                }
              }
              else
              {
                if (FindNearestEdges(ms,v2,GetDocument()->LodData->Active(),v1))
                {
                  CopyVektor(help,v1);
                  InvalidateRect(NULL,FALSE);
                }
              }
            }
    }
  }
  if (redrw) Invalidate();
  lastcursview=this;
}

void CObjektiv2View::MapMouse2(CPoint& p, float& x, float& y, float& z)
{
  if (LockMode & CMS_MAPX) x=(float)p.x;else x=0.0f;
  if (LockMode & CMS_MAPY) y=(float)-p.y;else y=0.0f;
  if (LockMode & CMS_MAPZ) z=(float)-p.y;else z=0.0f;
}

void CObjektiv2View::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
  Invalidate();
  if (lHint==1) UpdateWindow();
}

void CObjektiv2View::MapMouse(CPoint& p, int iz, float& x, float& y, float& z)
{
  HMATRIX m1,m2,m3;
  RotaceZ(m2,-rotz);
  RotaceX(m1,-rotx);
  SoucinMatic(m1,m2,m3);
  HVECTOR v1,tv1;
  v1[XVAL]=(float)p.x;
  v1[YVAL]=(float)-p.y;
  v1[ZVAL]=(float)iz;
  v1[WVAL]=1.0f;
  TransformVector(m3,v1,tv1);
  x=tv1[XVAL];
  y=tv1[YVAL];
  z=tv1[ZVAL];
}

BOOL CObjektiv2View::OnLockXYZ(UINT id) 
{
  switch (id)
  {
  case ID_LOCK_YZ: LockMode=UnlockMode=CMS_MAPZ|CMS_MAPY;break;
  case ID_LOCK_XY: LockMode=UnlockMode=CMS_MAPX|CMS_MAPY;break;
  case ID_LOCK_XZ: LockMode=UnlockMode=CMS_MAPX|CMS_MAPZ;break;
  case ID_LOCK_X: LockMode=CMS_MAPX;break;
  case ID_LOCK_Y: LockMode=CMS_MAPY;break;
  case ID_LOCK_Z: LockMode=CMS_MAPZ;break;
  }
  Invalidate();
  return TRUE;
}

void CObjektiv2View::OnUpdateLockXYZ(CCmdUI* pCmdUI) 
{
  switch (pCmdUI->m_nID)
  {
  case ID_LOCK_YZ: pCmdUI->SetCheck(LockMode==(CMS_MAPZ|CMS_MAPY));break;
  case ID_LOCK_XY: pCmdUI->SetCheck(LockMode==(CMS_MAPX|CMS_MAPY));break;
  case ID_LOCK_XZ: pCmdUI->SetCheck(LockMode==(CMS_MAPX|CMS_MAPZ));break;
  case ID_LOCK_X: pCmdUI->SetCheck(LockMode==CMS_MAPX);break;
  case ID_LOCK_Y: pCmdUI->SetCheck(LockMode==CMS_MAPY);break;
  case ID_LOCK_Z: pCmdUI->SetCheck(LockMode==CMS_MAPZ);break;
  }
}

BOOL CObjektiv2View::OnSetLockxyz(UINT id) 
{
  switch (id)
  {
  case ID_SET_LOCKX: if (LockMode==CMS_MAPX) LockMode=UnlockMode;else OnLockXYZ(ID_LOCK_X);break;
  case ID_SET_LOCKY: if (LockMode==CMS_MAPY) LockMode=UnlockMode;else OnLockXYZ(ID_LOCK_Y);break;
  case ID_SET_LOCKZ: 
    if (projection)
      if (LockMode==CMS_MAPZ) LockMode=UnlockMode;else OnLockXYZ(ID_LOCK_Z);
    else
      frame->SendMessage(WM_COMMAND,ID_VIEW_ZOOM); break;
  }
  /*
  if (projection)
  switch (id)
  {
  case ID_SET_LOCKX: if (LockMode==CMS_MAPX) LockMode=UnlockMode;else OnLockXYZ(ID_LOCK_X);break;
  case ID_SET_LOCKY: if (LockMode==CMS_MAPY) LockMode=UnlockMode;else OnLockXYZ(ID_LOCK_Y);break;
  case ID_SET_LOCKZ: if (LockMode==CMS_MAPZ) LockMode=UnlockMode;else OnLockXYZ(ID_LOCK_Z);break;
  }
  else
  {	
  switch(UnlockMode)
  {
  case CMS_MAPX|CMS_MAPY: 
  if (id==ID_SET_LOCKX) if (LockMode==CMS_MAPX) LockMode=UnlockMode;else OnLockXYZ(ID_LOCK_X);
  if (id==ID_SET_LOCKY) if (LockMode==CMS_MAPY) LockMode=UnlockMode;else OnLockXYZ(ID_LOCK_Y);
  break;
  case CMS_MAPX|CMS_MAPZ:
  if (id==ID_SET_LOCKX) if (LockMode==CMS_MAPX) LockMode=UnlockMode;else OnLockXYZ(ID_LOCK_X);
  if (id==ID_SET_LOCKY) if (LockMode==CMS_MAPZ) LockMode=UnlockMode;else OnLockXYZ(ID_LOCK_Z);
  break;
  case CMS_MAPY|CMS_MAPZ:
  if (id==ID_SET_LOCKX) if (LockMode==CMS_MAPZ) LockMode=UnlockMode;else OnLockXYZ(ID_LOCK_Z);
  if (id==ID_SET_LOCKY) if (LockMode==CMS_MAPY) LockMode=UnlockMode;else OnLockXYZ(ID_LOCK_Y);
  break;
  }
  if (id==ID_SET_LOCKZ) frame->SendMessage(WM_COMMAND,ID_VIEW_ZOOM);
  }*/
  Invalidate();
  return TRUE;
}

void CObjektiv2View::OnLockRotation() 
{
  rotlock=!rotlock;
}

void CObjektiv2View::OnUpdateLockRotation(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(rotlock);	
}

BOOL CObjektiv2View::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{ 
  // TODO: Add your message handler code here and/or call default
  HCURSOR cur=NULL;
  int mode=frame->GetEditCommand();
  if (nHitTest!=HTCLIENT)	return CView::OnSetCursor(pWnd, nHitTest, message);	  
  CPoint pt;
  GetCursorPos(&pt);
  ScreenToClient(&pt);
  if (GetKeyState(VK_RBUTTON) & 0x80)
  {
    switch (shiftstates & (MK_SHIFT | MK_CONTROL))
    {
    case MK_SHIFT : cur=theApp.LoadCursor(IDI_POINTER_COPY);break;
    case MK_SHIFT | MK_CONTROL: cur=theApp.LoadCursor(IDI_OBJRESIZE);break;
    case MK_CONTROL : cur=theApp.LoadCursor(IDI_OBJROTATE);break;
    case 0:if (mode!=ID_EDIT_EDITMODES_TOUCHEDGES) cur=theApp.LoadStandardCursor(IDC_SIZEALL);break;
    }
  }
  else if (mode==ID_EDIT_EDITMODES_TOUCHEDGES)
    cur=theApp.LoadStandardCursor(IDC_CROSS);
  else if (mode==ID_EDIT_PAINTVERTICES)
    cur=theApp.LoadCursor(IDC_PAINTSEL);
  else if (mode==ID_EDITMODES_TURNEDGE)
    cur=theApp.LoadCursor(IDC_TURNEDGE);
  else if (mode==ID_EDIT_TOUCH)
    cur=theApp.LoadCursor(IDI_TOUCHFACE);
  else if (mode==ID_EDIT_SELECTPOLYS)
    cur=theApp.LoadCursor(IDI_TOUCHPOL);
  else if ((mode==ID_EDIT_SELECTVERTICES  || mode==ID_EDIT_SELECTFACES || mode==ID_EDIT_SELECTOBJECTS))
  {
    if (frame->laso) 
      switch(mode)
    {
      case ID_EDIT_SELECTVERTICES:cur=theApp.LoadCursor(IDI_SELPTTLASO);break;
      case ID_EDIT_SELECTFACES:cur=theApp.LoadCursor(IDI_SELFACLASO);break;
      case ID_EDIT_SELECTOBJECTS:cur=theApp.LoadCursor(IDI_SELOBJLASO);break;
    }
    else  switch(mode)
    {
case ID_EDIT_SELECTVERTICES:cur=theApp.LoadCursor(IDI_SELPTTRECT);break;
case ID_EDIT_SELECTFACES:cur=theApp.LoadCursor(IDI_SELFACRECT);break;
case ID_EDIT_SELECTOBJECTS:cur=theApp.LoadCursor(IDI_SELOBJRECT);break;
    }
  }
  else if (mode==ID_CREATE_TRIANGLE || mode==ID_CREATE_QUAD ||  mode==ID_CREATE_FAN || mode==ID_CREATE_STRIP ||  mode==ID_DRAW_POLYGON)  	
    cur=theApp.LoadCursor(IDI_DRAWFACE);	
  else if (mode==ID_SURFACES_CYLMAP)
    cur=theApp.LoadCursor(IDI_CYLMAP);
  else if (mode==ID_EDIT_PIN)
    cur=theApp.LoadCursor(IDI_PINCURSOR);
  else if (mode==ID_VIEW_ZOOM)
    cur=theApp.LoadCursor(IDI_LUPA);
  else if (mode==ID_SURFACES_BACKGROUNDTEXTURE)
    if (texview->GetIndex(pt)!=-1) cur=theApp.LoadStandardCursor(IDC_SIZEALL);
    else if (GetKeyState(VK_CONTROL) & 0x80) cur=theApp.LoadCursor(IDI_OBJROTATE);
    else if (texview->TestPointInside(pt)) cur=theApp.LoadCursor(IDI_MOVETEXTURE);
    else cur=theApp.LoadCursor(IDI_DRAWTEXTURE);
    int gl=glines.GuideCursor(pt,NULL);
    if (gl==CGL_HORZ) cur=theApp.LoadStandardCursor(IDC_SIZENS);
    else if (gl==CGL_VERT) cur=theApp.LoadStandardCursor(IDC_SIZEWE);
    else if (gl==(CGL_VERT|CGL_HORZ)) cur=theApp.LoadStandardCursor(IDC_SIZEALL);
    if (cur==NULL) return CView::OnSetCursor(pWnd, nHitTest, message);	
    else SetCursor(cur);
    return FALSE;
}

LPHVECTOR CObjektiv2View::MousePtTo3D(CPoint& pt, LPHVECTOR ref,  LPHVECTOR norm)
{
  float fp[2];
  fp[0]=(float)pt.x;
  fp[1]=(float)pt.y;
  return MousePtTo3D(SPoint2DF(fp),ref,norm);
}

LPHVECTOR CObjektiv2View::MousePtTo3D(SPoint2DF& pt, LPHVECTOR ref,  LPHVECTOR norm)
{
  HVECTOR v1,v2,v3;
  HVECTOR tv1,tv2,tv3;
  HMATRIX mm,im;
  static HVECTOR res;
  CopyMatice(mm,camera.GetTransform());
  InverzeMatice(mm,im);
  v1[XVAL]=0.0f;v1[YVAL]=0.0f;v1[ZVAL]=0.0f;v1[WVAL]=1.0f;
  v2[XVAL]=300.0f;v2[YVAL]=0.0f;v2[ZVAL]=0.0f;v2[WVAL]=1.0f;
  v3[XVAL]=300.0f;v3[YVAL]=300.0f;v3[ZVAL]=0.0f;v3[WVAL]=1.0f;
  TransformVector(im,v1,tv1);
  TransformVector(im,v2,tv2);
  TransformVector(im,v3,tv3);
  CorrectVector(tv1);
  CorrectVector(tv2);
  CorrectVector(tv3);
  LPHVECTOR plane=calcFacePlane(tv1, tv2, tv3);
  if (ref!=NULL)
    plane[WVAL]=-(plane[XVAL]*ref[XVAL]+plane[YVAL]*ref[YVAL]+plane[ZVAL]*ref[ZVAL]);
  else
  {
    plane[WVAL]=0.0f;
    ref=mxVector3(0,0,0);
  }
  if (norm)
  {
    norm[XVAL]=plane[XVAL];
    norm[YVAL]=plane[YVAL];
    norm[ZVAL]=plane[ZVAL];
    norm[WVAL]=1.0f;
  }
  v1[XVAL]=pt.x();
  v1[YVAL]=pt.y();
  v1[ZVAL]=0.0f;
  CopyVektor(v2,v1);
  v2[ZVAL]=1.0f;
  TransformVector(im,v1,tv1);
  TransformVector(im,v2,tv2);
  CorrectVector(tv1);
  CorrectVector(tv2);
  RozdilVektoru(tv2,tv1);
  float t1, t2,t;
  t1=-(plane[XVAL]*tv1[XVAL]+plane[YVAL]*tv1[YVAL]+plane[ZVAL]*tv1[ZVAL]+plane[WVAL]);
  t2=(plane[XVAL]*tv2[XVAL]+plane[YVAL]*tv2[YVAL]+plane[ZVAL]*tv2[ZVAL]);
  t=t1/t2;
  res[XVAL]=tv1[XVAL]+tv2[XVAL]*t;
  res[YVAL]=tv1[YVAL]+tv2[YVAL]*t;
  res[ZVAL]=tv1[ZVAL]+tv2[ZVAL]*t;
  res[WVAL]=1.0f;
  return res;
}

void CObjektiv2View::OnViewPincamer() 
{
  CObjektiv2Doc *doc=GetDocument();
  pos[XVAL]=-doc->pin[XVAL];
  pos[YVAL]=-doc->pin[ZVAL];
  pos[ZVAL]=doc->pin[YVAL]; 
  Invalidate();
}

void CObjektiv2View::OnViewFacecull() 
{
  facecull=!facecull;	
  Invalidate();
}

void CObjektiv2View::OnUpdateViewFacecull(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(facecull);
}

void CObjektiv2View::OnD3DTextured() 
{
  d3dtextured=!d3dtextured;	
  Invalidate();
}

void CObjektiv2View::OnUpdateD3DTextured(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(solid&&used3d);
  pCmdUI->SetCheck(d3dtextured);	
}

void CObjektiv2View::OnUseD3D() 
{
  used3d=!used3d;	
  Invalidate();
}

void CObjektiv2View::OnUpdateUseD3D(CCmdUI* pCmdUI) 
{
#ifdef WITH_D3D
  pCmdUI->SetCheck(used3d);	
#else
  pCmdUI->Enable(false);
  pCmdUI->SetCheck(false);
#endif
}

void CObjektiv2View::OnViewGrid() 
{
  grid=!grid;	
  Invalidate();
}

void CObjektiv2View::OnUpdateViewGrid(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(grid);	
}

void CObjektiv2View::OnViewSolidfill() 
{
  solid=!solid;	
  Invalidate();
}

void CObjektiv2View::OnUpdateViewSolidfill(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(solid);	
}

void CObjektiv2View::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
  if (!polselect.IsEmpty()) 
  {
    int mode=frame->GetEditCommand();
    if (mode==ID_CREATE_FAN)
    {
      polselect.RemoveDouble();
      if (polselect.GetCount()>3)
      {
        comint.Begin(WString(IDS_DRAWFAN));
        for (int i=2;i<polselect.GetCount();i++)
          comint.Run(new CINewFace(3,polselect[0],polselect[i-1],polselect[i]));
      }
    }
    else if (mode==ID_CREATE_STRIP)
    {
      polselect.RemoveDouble();
      if (polselect.GetCount()>3)
      {
        comint.Begin(WString(IDS_DRAWSTRIP));
        for (int i=2;i<polselect.GetCount();i++)
          if (i & 1)
            comint.Run(new CINewFace(3,polselect[i-1],polselect[i-2],polselect[i]));
          else
            comint.Run(new CINewFace(3,polselect[i-2],polselect[i-1],polselect[i]));
      }
    }
    else if (mode==ID_DRAW_POLYGON)
    {
      polselect.RemoveDouble();
      if (polselect.GetCount()>3)
      {
        comint.Begin(WString(IDS_CREATEPOLYGON));
        comint.Run(new CINewPolygon(polselect.GetCount(),polselect));
      }

    }
    else
    {
      CDC *wdc=GetDC();
      C3DC cdc; cdc.CreateCompatibleDC(wdc);
      cdc.SetMatrix(camera.GetTransform());
      cdc.SelectObject(drawbmp);ReleaseDC(wdc);
      GetDocument()->PolygonSelection(cdc,polselect,(nFlags & MK_CONTROL)!=0,(nFlags & MK_SHIFT)!=0);
    }
    Invalidate(); 
    polselect.Reset();
  }
}

void CObjektiv2View::OnKillFocus(CWnd* pNewWnd) 
{
  CView::OnKillFocus(pNewWnd);
  CancelPolygonialSelection();	
}

void CObjektiv2View::CancelPolygonialSelection()
{
  if (!polselect.IsEmpty()) 
  {
    polselect.Reset();
    Invalidate();
  }
}

class CIInsertPoint: public CICommand
{
  HVECTOR where;
public:
  CIInsertPoint(LPHVECTOR w):CICommand() 
  {CopyVektor(where,w);}
  virtual int Execute(LODObject *obj)
  {
    ObjectData *odata=obj->Active();
    int idx=odata->ReservePoints(1);
    PosT &pt=odata->Point(idx);
    pt[0]=where[XVAL];
    pt[1]=where[YVAL];
    pt[2]=where[ZVAL];	  
    odata->PointSelect(idx);       
    // set point position in all animation phases
    int p;
    for( p=0; p<odata->NAnimations(); p++ )
    {
      AnimationPhase *phase=odata->GetAnimation(p);
      (*phase)[idx]=pt;
    }
    return 0;
  }
  virtual void Save(ostream& str)
  {
    str.write((char *)where,sizeof(where));
  }
  virtual int Tag() const 
  {return CITAG_INSERTPOINT;}
};

CICommand *CIInsertPoint_Create(istream &str)
{
  HVECTOR v;str.read((char *)v,sizeof(v));
  return new CIInsertPoint(v);
}

void CObjektiv2View::OnInsertPoint() 
{
  if (GetDocument()->pinlock)
  {
    comint.Begin(WString(IDS_NEWPOINT));
    comint.Run(new CIInsertPoint(GetDocument()->pin));
  }
  else
  {
    CPoint pt;
    GetCursorPos(&pt);
    CWnd *wnd=WindowFromPoint(pt);
    if (wnd!=this)
    {
      if (wnd==NULL) return;
      wnd->SetFocus();
      wnd->SendMessage(WM_KEYDOWN,VK_INSERT,1);
      return;
    }
    ScreenToClient(&pt);
    if (frame->GetEditCommand()==ID_EDIT_EDITMODES_TOUCHEDGES)
    {
      HVECTOR v2;

      LPHVECTOR ms=MousePtTo3D(pt,GetDocument()->pin,v2);
      if (PointAtEdge(ms,v2,GetDocument()->LodData->Active()))
      {
        GetDocument()->UpdateAllViews(NULL);
      }
    }
    else
    {
      glines.GuideCursor(pt);
      LPHVECTOR mpos=MousePtTo3D(pt,doc->pin);
      comint.Begin(WString(IDS_NEWPOINT));
      comint.Run(new CIInsertPoint(mpos));
    }
  }
  Invalidate();
  GetDocument()->UpdateAllViews(this);
}

class CICreatePrim:public CICommand
{
  CCreatePrimDlg *data;
  HMATRIX pinpos;
public:
  CICreatePrim(CCreatePrimDlg *dlg, LPHMATRIX pin):data(dlg),CICommand() 
  {CopyMatice(pinpos,pin);}
  virtual ~CICreatePrim() 
  {delete data;}
  virtual int Execute(LODObject *obj)
  {
    ObjectData *odata=obj->Active();
    data->CreatePrimitive(odata,pinpos);
    return 0;
  }
  virtual void Save(ostream& str)
  {
    str.write((char *)pinpos,sizeof(HMATRIX));
    data->Save(str);
  }
  virtual int Tag() const 
  {return CITAG_CREATEPRIM;}
};

CICommand *CICreatePrim_Create(istream& str)
{
  CCreatePrimDlg *data=new CCreatePrimDlg();
  HMATRIX pinpos;
  str.read((char *)pinpos,sizeof(HMATRIX));
  data->Load(str);
  return new CICreatePrim(data,pinpos);
}

BOOL CObjektiv2View::OnCreatePrim(UINT com) 
{
  int idd;
  switch (com)
  {
  case ID_CREATE_PLANE:idd=IDD_CREATE_PRIM_PLANE;break;
  case ID_CREATE_BOX:idd=IDD_CREATE_PRIM_BOX;break;
  case ID_CREATE_SPEHERE: idd=IDD_CREATE_PRIM_SPEHERE;break;
  case ID_CREATE_CYLINDER: idd=IDD_CREATE_PRIM_CYLINDER;break;
  case ID_CREATE_CIRCLE:idd=IDD_CREATE_PRIM_CIRCLE; break; 
  }
  CGCamera2 cam;
  cam.LookAt(doc->pin,rotz,rotx,0);
  HMATRIX m1,m2;
  CopyMatice(m1,cam.GetView());
  SoucinMatic(p3d2g3d,m1,m2);
  InverzeMatice(m2,m1);
  CopyVektor(m1[3],doc->pin);
  CCreatePrimDlg *dlg;
  dlg=new CCreatePrimDlg(NULL,idd);
  dlg->create_type=idd;
  if (dlg->DoModal()==IDCANCEL)
  {
    delete dlg;
    return TRUE;
  }
  comint.Begin(WString(IDS_NEWPRIMITIVE));
  comint.Run(new CICreatePrim(dlg,m1));
  GetDocument()->UpdateAllViews(this);
  Invalidate();
  return TRUE;
}

void CObjektiv2View::MapMouseMovement(CPoint &pt1, CPoint &pt2, HMATRIX mm, bool nolock)
{
  float grid=config->GetInt(IDS_CFGMOVESTEP)/100.0f;
  HVECTOR v1,v2;
  if (!projection && !nolock)
  {
    if (~LockMode & CMS_MAPX) pt2.x=pt1.x;
    if (~LockMode & CMS_MAPY) pt2.y=pt1.y;
  }
  CopyVektor(v1,MousePtTo3D(pt1,doc->pin));
  CopyVektor(v2,MousePtTo3D(pt2,doc->pin));
  RozdilVektoru(v1,v2);
  if (projection && !nolock)
  {
    if (~LockMode & CMS_MAPX) v1[XVAL]=0;
    if (~LockMode & CMS_MAPY) v1[YVAL]=0;
    if (~LockMode & CMS_MAPZ) v1[ZVAL]=0;
  }
  if (grid>0.0f)
  {
    v1[XVAL]=floor(v1[XVAL]/grid)*grid;
    v1[YVAL]=floor(v1[YVAL]/grid)*grid;
    v1[ZVAL]=floor(v1[ZVAL]/grid)*grid;
  }
  Translace(mm,-v1[XVAL],-v1[YVAL],-v1[ZVAL]);
  frame->SetStatus(WString(IDS_TRANSLATE_STATUS,-v1[XVAL],-v1[YVAL],-v1[ZVAL]));
}

void CObjektiv2View::CalcRotationOnPlane(float rad, HMATRIX mm, bool pin, bool nogrid)
{
  HVECTOR v;

  if (pin) CopyVektor(v,doc->pin);
  else GetObjectCenter(true,v,NULL);
  TMATRIXSTACK mxs;
  mxsInitStack(&mxs);
  Translace(mxsPushR(&mxs),-v[XVAL],-v[YVAL],-v[ZVAL]);
  float grid=nogrid?0.0f:torad(config->GetInt(IDS_CFGROTATESTEP));
  if (grid>0.0f)
  {rad=floor(rad/grid+0.5)*grid;}
  RotaceY(mxsPushR(&mxs),rotz);
  RotaceX(mxsPushR(&mxs),rotx);
  RotaceY(mxsPushR(&mxs),rad);
  RotaceX(mxsPushR(&mxs),-rotx);
  RotaceY(mxsPushR(&mxs),-rotz);
  Translace(mxsPushR(&mxs),v[XVAL],v[YVAL],v[ZVAL]);
  CopyMatice(mm,mxsTop(&mxs));
  mxsFreeStack(&mxs);
  /*  for (int i=0;i<4;i++)
  {
  for (int j=0;j<4;j++)      
  mm[i][j]=floor(mm[i][j]*10000.0f+0.5f)/10000.0f;
  if (i<3) {NormalizeVector(mm[i]);mm[i][3]=0.0f;}
  }
  */    
  //if (rad>FPI) rad-=2*FPI;
  //if (rad<=-FPI) rad+=2*FPI;
  frame->SetStatus(WString(IDS_ROTATION_STATUS,todec(rad)));
}

void CObjektiv2View::CalcScaleOnPlane(float f, HMATRIX mm, bool pin,bool nogrid)
{
  HVECTOR v;
  float grid=nogrid?0.0f:config->GetInt(IDS_CFGSCALESTEP)/100.0f;
  if (pin) CopyVektor(v,doc->pin);
  else GetObjectCenter(true,v,NULL);
  TMATRIXSTACK mxs;
  mxsInitStack(&mxs);
  Translace(mxsPushR(&mxs),-v[XVAL],-v[YVAL],-v[ZVAL]);
  if (grid>0.0f)
  {f=floor(f/grid)*grid;}
  if (f<0.0f) f=0.0f;
  Zoom(mxsPushR(&mxs),f,f,f);
  Translace(mxsPushR(&mxs),v[XVAL],v[YVAL],v[ZVAL]);
  CopyMatice(mm,mxsTop(&mxs));
  mxsFreeStack(&mxs);
  frame->SetStatus(WString(IDS_SCALE_STATUS,f*100.0f));
}

void CObjektiv2View::CalcNonuniformScaleOnPlane(float x,float y, HMATRIX mm, bool pin, bool nogrid)
{
  HVECTOR v;

  if (pin) CopyVektor(v,doc->pin);
  else GetObjectCenter(true,v,NULL);
  TMATRIXSTACK mxs;
  mxsInitStack(&mxs);
  Translace(mxsPushR(&mxs),-v[XVAL],-v[YVAL],-v[ZVAL]);
  float grid=nogrid?0.0f:config->GetInt(IDS_CFGSCALESTEP)/100.0f;
  if (grid>0.0f)
  {x=floor((x+0.5*grid)/grid)*grid;y=floor((y+0.5*grid)/grid)*grid;}
  if (x<0) x=0;
  if (y<0) y=0;
  RotaceY(mxsPushR(&mxs),rotz);
  RotaceX(mxsPushR(&mxs),rotx);
  Zoom(mxsPushR(&mxs),x,1,y);
  RotaceX(mxsPushR(&mxs),-rotx);
  RotaceY(mxsPushR(&mxs),-rotz);
  Translace(mxsPushR(&mxs),v[XVAL],v[YVAL],v[ZVAL]);
  CopyMatice(mm,mxsTop(&mxs));
  mxsFreeStack(&mxs);
  frame->SetStatus(WString(IDS_NUSCALE_STATUS,x*100.0,y*100.0));
}

void CObjektiv2View::OnSurfacesBackgroundmapping() 
{
  CObjektiv2Doc *doc=GetDocument();
  ObjectData *odata=doc->LodData->Active();
  int cnt=odata->NFaces();
  int mapsize=0;
  int i,p;
  for (i=0;i<cnt;i++) if (odata->FaceSelected(i)) mapsize++;
  char texname[256];
  if (texview->textureName==NULL || texview->textureName[0]==0)
  {AfxMessageBox(IDC_NOTEXTURESET);return;}
  //  if (strlen(texview->textureName)>25) 
  //	AfxMessageBox(IDS_TEXNAMETOOLONG,MB_OK|MB_ICONEXCLAMATION);
  strcpy(texname, texview->textureName);
  //  CTexList::AddPrefix(texview->textureName,texname,sizeof(texname));
  CIBackgrndMap *map=new CIBackgrndMap(mapsize,texname);
  for (i=0,p=0;i<cnt;i++)
    if (odata->FaceSelected(i))
    {
      float tu,tv;
      FaceT fc(odata,i);
      for (int j=0;j<fc.N();j++)
      {
        PosT &pos=odata->Point(fc.GetPoint(j));
        HVECTOR tt;
        TransformVector(camera.GetTransform(),mxVector3(pos[0],pos[1],pos[2]),tt);
        CorrectVector(tt);		
        if (texview->GetTextureMapping(tt,tu,tv)==false)
        {
          AfxMessageBox(IDS_MAPPINGERROR,MB_OK|MB_ICONSTOP);
          delete map;
          return;
        }
        map->SetMapping(p,j,tu,tv);
      }
      p++;
    }
    comint.Begin(WString(IDS_BACKGROUNDMAPPING));
    comint.Run(map);
    comint.SetLongOp();
    GetDocument()->UpdateMatLib();
    doc->UpdateAllViews(NULL);
}

void CObjektiv2View::OnSurfacesBgrfromface() 
{
  CObjektiv2Doc *doc=GetDocument();
  ObjectData *odata=doc->LodData->Active();
  int cnt=odata->NFaces();
  int i;
  for (i=0;i<cnt;i++)
    if (odata->FaceSelected(i)) break;
  if (cnt==i) return;  
  FaceT face(odata,i);
  HVECTOR tv[3];
  for (int j=0;j<3;j++)
  {
    PosT& pos=odata->Point(face.GetPoint(j));
    TransformVector(camera.GetTransform(),mxVector3(pos[0],pos[1],pos[2]),tv[j]);
    CorrectVector(tv[j]);
  }
  texview->BackgroundFromTex(tv[0],face.GetU(0),face.GetV(0),
    tv[1],face.GetU(1),face.GetV(1),
    tv[2],face.GetU(2),face.GetV(2));
  for (i=0;i<4;i++) CopyVektor(texview->points3d[i],MousePtTo3D(texview->GetPos(i),pos));  
  const char *c=face.GetTexture();
  Pathname path;  
  path.SetDirectory(config->GetString(IDS_CFGPATHFORTEX));
  path.SetFilename(c);
  texview->SetTextureName(path,c);
  Invalidate();
  recalc_texview=true;
}

void CObjektiv2View::OnSurfacesBgrfromsel() 
{
  CObjektiv2Doc *doc=GetDocument();
  int i,cnt;
  ObjectData *odata=doc->LodData->Active();
  cnt=odata->NPoints();
  CRect rc;
  rc.SetRectEmpty();
  bool first=true;
  for (i=0;i<cnt;i++) if (odata->PointSelected(i))
  {
    PosT& pos=odata->Point(i);
    HVECTOR tv;
    TransformVector(camera.GetTransform(),mxVector3(pos[0],pos[1],pos[2]),tv);
    XYZW2XYZQ(tv);
    if (tv[ZVAL]<0.0f || tv[WVAL]<0.0f) continue;
    CPoint pt((int)tv[XVAL],(int)tv[YVAL]);
    if (first) 	  
    {
      rc.SetRect(pt,pt);	  
      first=false;
    }
    else
    {
      if (rc.left>pt.x) rc.left=pt.x;
      if (rc.right<pt.x) rc.right=pt.x;
      if (rc.top>pt.y) rc.top=pt.y;
      if (rc.bottom<pt.y) rc.bottom=pt.y;
    }
  }
  texview->SetRect(&rc);
  for (int j=0;j<4;j++)	
    CopyVektor(texview->points3d[j],MousePtTo3D(texview->GetPos(j),doc->pin));	
  Invalidate();
  recalc_texview=true;
}

void CObjektiv2View::TexContextMenu()
{
  CPoint pt;
  CMenu menu;

  LoadPopupMenu(IDR_TEXTURE_POPUP,menu);
  if (texview->gray) 
    menu.CheckMenuItem(ID_TEX_GRAY,MF_BYCOMMAND|MF_CHECKED);
  if (texview->hidden)
    menu.CheckMenuItem(ID_TEX_HIDE,MF_BYCOMMAND|MF_CHECKED);
  if (texview->transp100)
    menu.CheckMenuItem(ID_TEX_TRANSP100,MF_BYCOMMAND|MF_CHECKED);
  GetCursorPos(&pt);
  contextmenupt=pt;
  ScreenToClient(&contextmenupt);
  if (list.CheckHitTest(contextmenupt)==NULL)
    menu.EnableMenuItem(ID_TEX_SELECT,MF_BYCOMMAND|MF_GRAYED);
  if (list.GetCount()==TEXMAXLOAD)
    menu.EnableMenuItem(ID_TEX_ADD,MF_BYCOMMAND|MF_GRAYED);
  menu.TrackPopupMenu(TPM_LEFTALIGN,pt.x,pt.y,this,NULL);
}

void CObjektiv2View::OnTexMirrorX() 
{
  texview->MirrorX();
  Invalidate();recalc_texview=true;
}

void CObjektiv2View::OnTexMirrorY() 
{
  texview->MirrorY();
  Invalidate();recalc_texview=true;
  // TODO: Add your command handler code here

}

void CObjektiv2View::OnTexRotatel() 
{
  texview->RotateL();
  Invalidate();recalc_texview=true;
  // TODO: Add your command handler code here

}

void CObjektiv2View::OnTexRotater() 
{
  texview->RotateR();
  Invalidate();recalc_texview=true;
}

void CObjektiv2View::OnTexAspec() 
{
  texview->CorrectActpect();	
  Invalidate();recalc_texview=true;
  for (int i=0;i<4;i++) CopyVektor(texview->points3d[i],MousePtTo3D(texview->GetPos(i),pos));	  
}

void CObjektiv2View::OnTexUnload() 
{
  if (list.GetCount()>1)
  {
    list.Delete(texview);
    texview=list.EnumTex(NULL);
  }
  else
  {
    CRect rc(0,0,0,0);	
    texview->SetRect(&rc);
    free(texview->SetTexture(NULL));
    for (int i=0;i<4;i++) CopyVektor(texview->points3d[i],MousePtTo3D(texview->GetPos(i),pos));	  
  }
  Invalidate();
  recalc_texview=true;
}

BOOL CObjektiv2View::OnViewPagezoom(UINT cmd) 
{
  CRect rc,rcmin;
  CObjektiv2Doc *doc=GetDocument();
  GetClientRect(&rc);
  bool set=true;
  ObjectData *odata=doc->LodData->Active();
  int cnt=odata->NPoints();
  if (fabs(scale)<1e-10 || fabs(scale)>1e+10) scale=1.0f;
  for (int i=0;i<cnt;i++) if (cmd==ID_VIEW_PAGEZOOM || odata->PointSelected(i))
  {
    PosT& ps=odata->Point(i);
    HVECTOR tv;
    TransformVector(camera.GetTransform(),mxVector3(ps[0],ps[1],ps[2]),tv);
    XYZW2XYZQ(tv);
    if (tv[ZVAL]>0.0f && tv[WVAL]>0.0f)
    {
      int x=(int)tv[XVAL];
      int y=(int)tv[YVAL];
      if (set) 
      {rcmin=CRect(x,y,x,y);set=false;}
      else
      {
        if (rcmin.left>x) rcmin.left=x;
        if (rcmin.right<x) rcmin.right=x;
        if (rcmin.top>y) rcmin.top=y;
        if (rcmin.bottom<y) rcmin.bottom=y;
      }
    }
  }
  if (set) return TRUE;
  HVECTOR v1,v2;
  CopyVektor(v1,this->MousePtTo3D(CPoint((rc.right+rc.left)>>1,(rc.top+rc.bottom)>>1),doc->pin));
  CopyVektor(v2,this->MousePtTo3D(CPoint((rcmin.right+rcmin.left)>>1,(rcmin.top+rcmin.bottom)>>1),doc->pin));
  RozdilVektoru(v1,v2);
  v2[YVAL]=v1[ZVAL];
  v2[ZVAL]=-v1[YVAL];
  v2[XVAL]=v1[XVAL];
  SoucetVektoru(pos,v2);
  CSize sz1=rc.Size();
  CSize sz2=rcmin.Size();
  float x=(float)sz1.cx/(float)sz2.cx;
  float y=(float)sz1.cy/(float)sz2.cy;
  if (!projection && (sz2.cx!=0 || sz2.cy!=0)) scale*=min(x,y)*0.80f;
  if (fabs(scale)<1e-10) scale=1.0f;
  Invalidate();
  recalc_texview=true;
  return TRUE;
}

void CObjektiv2View::OnUpdateViewPagezoomsel(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(GetDocument()->LodData->Active()->CountSelectedPoints()!=0);		
}

BOOL CObjektiv2View::OnViewZoomin(UINT cmd) 
{
  if (cmd==ID_VIEW_ZOOMIN)
    if (projection) 
      distance-=distance*0.2f;
    else
      scale*=1.1f;
  else
    if (projection) 
      distance+=distance*0.2f;
    else
      scale*=0.9f;
  recalc_texview=true;
  Invalidate();
  return TRUE;
}

void CObjektiv2View::MakeZoomRect(CRect &rcmin)
{
  CRect rc;
  rcmin.NormalizeRect();
  if (rcmin.IsRectEmpty()) return;
  GetClientRect(&rc);
  HVECTOR v1,v2;
  CopyVektor(v1,this->MousePtTo3D(CPoint((rc.right+rc.left)>>1,(rc.top+rc.bottom)>>1),doc->pin));
  CopyVektor(v2,this->MousePtTo3D(CPoint((rcmin.right+rcmin.left)>>1,(rcmin.top+rcmin.bottom)>>1),doc->pin));
  RozdilVektoru(v1,v2);
  v2[YVAL]=v1[ZVAL];
  v2[ZVAL]=-v1[YVAL];
  v2[XVAL]=v1[XVAL];
  SoucetVektoru(pos,v2);
  CSize sz1=rc.Size();
  CSize sz2=rcmin.Size();
  float x=(float)sz1.cx/(float)sz2.cx;
  float y=(float)sz1.cy/(float)sz2.cy;
  if (!projection) scale*=min(x,y)*0.80f;
  if (scale<1e-10) scale=1.0f;
  recalc_texview=true;
  Invalidate();
}

void CObjektiv2View::OnTexUndo() 
{
  texview->Undo();	
  recalc_texview=true;
  Invalidate();
}

void CObjektiv2View::OnTexGray() 
{
  texview->gray=!texview->gray;
  recalc_texview=true;
  Invalidate();
}

void CObjektiv2View::OnTexTransp100()
{
  texview->transp100=!texview->transp100;
  recalc_texview=true;
  Invalidate();
}

void CObjektiv2View::CalcMirror(bool y, HMATRIX mm, bool pin)
{
  HVECTOR v;

  if (pin) CopyVektor(v,doc->pin);
  else GetObjectCenter(true,v,NULL);
  TMATRIXSTACK mxs;
  mxsInitStack(&mxs);
  Translace(mxsPushR(&mxs),-v[XVAL],-v[YVAL],-v[ZVAL]);
  RotaceY(mxsPushR(&mxs),rotz);
  RotaceX(mxsPushR(&mxs),rotx);
  if (y) Zoom(mxsPushR(&mxs),1,1,-1);else Zoom(mxsPushR(&mxs),-1,1,1);
  RotaceX(mxsPushR(&mxs),-rotx);
  RotaceY(mxsPushR(&mxs),-rotz);
  Translace(mxsPushR(&mxs),v[XVAL],v[YVAL],v[ZVAL]);
  CopyMatice(mm,mxsTop(&mxs));
  mxsFreeStack(&mxs);
}

static CString lasttexpath;


void CObjektiv2View::OnTexLoad() 
{  
  CString name=GetTextureFrom(IDC_BROWSE,texview->textureName);
  if (name=="") return;
  const char *noprefix=name;
  Pathname pth;
  //  if (config->GetBool(IDS_CFGCONVERTGIFTGA))
  pth.SetDirectory(config->GetString(IDS_CFGPATHFORTEX));
  //  else
  //	pth.SetDirectory(lasttexpath);
  if (noprefix[0] && noprefix[1]!=':') pth.SetFilename(noprefix);
  else pth=noprefix;
  texview->SetTextureName(pth,noprefix);
  Invalidate();recalc_texview=true;

  /*  CFileDialogEx fdlg(TRUE,NULL,NULL,OFN_FILEMUSTEXIST|OFN_LONGNAMES|OFN_OVERWRITEPROMPT,
  WString(IDS_PACPAAFILTER));
  fdlg.m_ofn.lpstrInitialDir=config->GetString(IDS_CFGPATHFORTEX);
  if (fdlg.DoModal()==IDCANCEL) return;
  WFilePath pth=fdlg.GetPathName();
  char temp[256]; 
  if (config->GetBool(IDS_CFGCONVERTGIFTGA))
  {
  char tx[256];
  CTexList::ConvertTexture(pth, tx, sizeof(tx));
  CTexList::AddPrefix(tx,temp,sizeof(temp));
  }
  else
  CTexList::AddPrefix(fdlg.GetFileName(),temp,sizeof(temp));
  texview->SetTextureName(pth,temp);
  Invalidate();*/
}

void CObjektiv2View::OnTexHide() 
{
  texview->hidden=!texview->hidden;
  recalc_texview=true;
  Invalidate();
}

CString GetTextureFrom(int idc, const char *OldTexName)
{  
  WString texname;
  switch (idc)
  {
  case IDC_BROWSE:
    {
      const char *c=OldTexName;
      char *curdir=_getcwd(NULL,0);
      static int lastfilter;      
      if (c!=NULL && c[0]==0) c=NULL;
      if (lasttexpath=="") lasttexpath=config->GetString(IDS_CFGPATHFORTEX);
      CFileDialogEx fdlg(TRUE,NULL,c,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY|OFN_NOCHANGEDIR,
        WString(IDS_PACPAAFILTER));
      fdlg.m_ofn.lpstrInitialDir=lasttexpath;
      fdlg.m_ofn.nFilterIndex=lastfilter;
      if (fdlg.DoModal()==IDOK)
      {
        texname=fdlg.GetPathName();
        Pathname pth(texname);
        lasttexpath=pth.GetDirectoryWithDrive();		
      }
      else 
        return CString(OldTexName);
      lastfilter=fdlg.m_ofn.nFilterIndex;
      _chdir(curdir);
      free(curdir);
      char tx[256];	  
      if (config->GetBool(IDS_CFGCONVERTGIFTGA))		
      {
        Pathname tex=texname;
        const char *shortpath=ConvertTexture(tex);
        strcpy(tx,shortpath);
      }
      else				
      {
        const char *c=::DeriveTexPath(config->GetString(IDS_CFGPATHFORTEX),texname);
        if (c==NULL) c=texname;
        strcpy(tx,c);
      }
      texname=tx;
      break;
    }
  case IDC_SETFROMLIST:
    {
      Pathname object=frame->m_wndMatLib.GetSelected();
      texname=ConvertTexture(object);
      break;
    }
  case IDC_SETFROMVIEW:
    {
      CObjektiv2View *view=CObjektiv2View::GetCurrentView();
      if (view->texview->textureName!=NULL)	texname=view->texview->textureName;
      else texname=OldTexName;
      break;
    }	  
  }
  if (texname=="") return texname;
  /*  if (config->GetBool(IDS_CFGCONVERTGIFTGA))
  {
  CTexList::ConvertTexture(texname, tx, sizeof(tx));
  CTexList::AddPrefix(tx,tt,sizeof(tt));
  }
  else
  {
  const char *c=strrchr(texname,'\\');
  if (c!=NULL) c=(const char *)((char *)c+1);else c=texname;
  CTexList::AddPrefix(c,tt,sizeof(tt));
  }*/
  return texname;
}

void CObjektiv2View::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
  bool ctrl=(GetKeyState(VK_CONTROL) & 0x80)!=0;
  int delta=(ctrl?16:1)*nRepCnt;
  if (frame->GetEditCommand()==ID_SURFACES_BACKGROUNDTEXTURE)
  {
    bool shift=(GetKeyState(VK_SHIFT) & 0x80)!=0;
    switch (nChar)
    {
    case VK_RIGHT: if (shift) texview->KeyScale(delta,0);else texview->KeyMove(delta,0);break;
    case VK_LEFT: if (shift) texview->KeyScale(-delta,0);else texview->KeyMove(-delta,0);break;
    case VK_UP: if (shift) texview->KeyScale(0,-delta);else texview->KeyMove(0,-delta);break;
    case VK_DOWN: if (shift) texview->KeyScale(0,delta);else texview->KeyMove(0,delta);break;
    default: CView::OnKeyDown(nChar, nRepCnt, nFlags);return;
    }
    for (int i=0;i<4;i++)
      CopyVektor(texview->points3d[i],MousePtTo3D(texview->GetPos(i),pos));
    Invalidate();
    recalc_texview=true;
  }
  else	
  {
    CPoint pt;
    delta*=16;
    switch(nChar)
    {
    case VK_RIGHT:pt.x=-delta;pt.y=0;break;
    case VK_LEFT:pt.x=delta;pt.y=0;break;
    case VK_UP:pt.y=delta;pt.x=0;break;
    case VK_DOWN:pt.y=-delta;pt.x=0;break;
    default: CView::OnKeyDown(nChar, nRepCnt, nFlags);return;
    }
    float x,y,z;
    MapMouse(pt,0,x,y,z);
    float p=(projection?distance:0.5f/scale);
    pos[XVAL]+=x*p*0.01f;
    pos[YVAL]+=y*p*0.01f;
    pos[ZVAL]+=z*p*0.01f;
    recalc_texview=true;
    Invalidate();
  }

}

void CObjektiv2View::OnViewLookatface() 
{
  ObjectData *obj=(GetDocument())->LodData->Active();
  int cnt=obj->NFaces();
  HVECTOR v=
  {0,0,0,1};
  for (int i=0;i<cnt;i++) if (obj->FaceSelected(i))
  {
    FaceT fc(obj,i);
    Vector3 lpv = fc.CalculateNormal();
    v[0] = lpv[0];
    v[1] = lpv[1];
    v[2] = lpv[2];
  }
  float prepona=(float)sqrt(v[XVAL]*v[XVAL]+v[ZVAL]*v[ZVAL]);
  rotz=(float)atan2(v[XVAL],v[ZVAL]);
  rotx=(float)atan2(prepona,-v[YVAL]);
  Invalidate();
}

void CObjektiv2View::CalcNetMatrix(HMATRIX mm)
{
  HMATRIX m1,m2;
  if (projection) 
  {JednotkovaMatice(mm);}
  else
  {
    RotaceY(m1,-rotz);
    RotaceX(m2,-rotx);
    SoucinMatic(m2,m1,mm);
  }
}

void CObjektiv2View::OnTexAdd() 
{
  texview=list.Alloc();
  Invalidate();
}

void CObjektiv2View::OnTexSelect() 
{
  CTextureMapping *temp=list.HitTest(contextmenupt);
  if (temp) texview=temp;
  Invalidate();
}

void CObjektiv2View::DrawBackground(CDC *pDC)
{
  CBitmap *temp;
  CDC bmpdc;
  bmpdc.CreateCompatibleDC(pDC);
  CRect rc;
  GetClientRect(&rc);
  if (recalc_texview || backbmp==NULL)
  {
    if (backbmp!=NULL) delete backbmp;
    backbmp=new CBitmap();
    backbmp->CreateCompatibleBitmap(pDC,rc.right,rc.bottom);
    temp=bmpdc.SelectObject(backbmp);
    bmpdc.FillSolidRect(0,0,rc.right,rc.bottom,config->GetHex(IDS_CFGBGCOLOR,0xFFFFFF));
    for(CTextureMapping *texp=NULL;texp=list.EnumTex(texp);)
    {
      recalc_texview=false;
      for (int i=0;i<4;i++)
      {
        HVECTOR tv;
        TransformVector(camera.GetTransform(),texp->points3d[i],tv);
        CorrectVector(tv);		
        texp->SetIndex(i,tv[XVAL],tv[YVAL]);
      }
    }  
    bmpdc.SelectObject(&texbackcolor);
    list.Draw(&bmpdc,texview,frame->GetEditCommand()==ID_SURFACES_BACKGROUNDTEXTURE);
  }
  else
    temp=bmpdc.SelectObject(backbmp);
  pDC->BitBlt(0,0,rc.right,rc.bottom,&bmpdc,0,0,SRCCOPY);
  bmpdc.SelectObject(temp);
}



void CObjektiv2View::OnViewExternalaschild() 
{
  if (extattach==NULL)
    extattach=frame->_externalViewer.GetWindow();
  else
  {
    ::SetParent(extattach,::GetDesktopWindow());
    SetWindowLong(extattach,GWL_STYLE,GetWindowLong(extattach,GWL_STYLE)& ~ WS_CHILD | WS_OVERLAPPEDWINDOW);
    extattach=NULL;    
  }
  if (extattach)
  {
    CRect rc;
    GetClientRect(rc);
    ::MoveWindow(extattach,0,0,rc.right,rc.bottom,FALSE);
    ::SetParent(extattach,*this);
    SetWindowLong(extattach,GWL_STYLE,GetWindowLong(extattach,GWL_STYLE)|WS_CHILD);
  }
}

void CObjektiv2View::OnMove(int x, int y) 
{
  CView::OnMove(x, y);
  /*  if (extattach)
  {
  CRect rc;
  GetClientRect(&rc);
  ClientToScreen(&rc);
  ::SetWindowPos(extattach,HWND_TOPMOST,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,0);
  }*/
  
}

int CObjektiv2View::AttachPoint(CPoint &ms)
{
  CDC *dc=GetDC();  
  ObjectData *obj=GetDocument()->LodData->Active();
  int i;
  for (i=0;i<obj->NPoints();i++) if (obj->PointSelected(i))
  {
    PosT &pos=obj->Point(i);
    HVECTOR vect;
    TransformVector(camera.GetTransform(),mxVector3(pos[0],pos[1],pos[2]),vect);
    if (vect[WVAL]>0.0f && vect[ZVAL]>0.0f)
    {
      CorrectVector(vect);
      CPoint pt((int)(round(vect[0])+0.5),(int)(round(vect[1])+0.5));
      pt-=ms;

#define ATT_DISTANCE 5
      if (pt.x>-ATT_DISTANCE && pt.x<ATT_DISTANCE && pt.y>-ATT_DISTANCE && pt.y<ATT_DISTANCE)
        break;	
    }
  }
  if (i==obj->NPoints()) return -1;
  return i;
}

void CObjektiv2View::OnEditLineardeform() 
{
  CLinWeightDlg dlg;
  dlg.drwwnd=this;
  dlg.scrtrans=camera.GetTransform();
  dlg.curobj=GetDocument()->LodData->Active();
  dlg.DoModal();
}

void CObjektiv2View::DoPaintVertices(CPoint &point)
{
  CObjektiv2Doc *doc=GetDocument();
  ObjectData *obj=doc->LodData->Active();
  Selection *s=const_cast<Selection *>(obj->GetSelection());
  int cnt=obj->NPoints();
  const Selection *hidden=obj->GetHidden();
  for (int i=0;i<cnt;i++) if (!hidden->PointSelected(i))
  {
    HVECTOR vc;
    PosT &pos=obj->Point(i);
    TransformVector(camera.GetTransform(),mxVector3(pos[0],pos[1],pos[2]),vc);
    CorrectVector(vc);
    if (vc[ZVAL]>=0.0 && vc[WVAL]>0.0f)
    {
      float distx=vc[XVAL]-point.x;
      float disty=vc[YVAL]-point.y;
      float dist=(float)sqrt(distx*distx+disty*disty);
      float w;
      if (dist<frame->paintradiusmin) w=frame->paintstrength;
      else if (dist>=frame->paintradiusmin && dist<frame->paintradiusmax) 
        w=(frame->paintradiusmax-dist)/(frame->paintradiusmax-frame->paintradiusmin)*frame->paintstrength;
      else w=0;
      float lw=s->PointWeight(i);
      if (w>0.0f)
        s->SetPointWeight(i,lw+(frame->paintweight-lw)*w);
    }
  }
  Invalidate();
}

void CObjektiv2View::EndPaintVertices()
{
  CObjektiv2Doc *doc=GetDocument();
  ObjectData *obj=doc->LodData->Active();
  Selection *s=new Selection(*obj->GetSelection());
  comint.Begin(WString(IDS_MAKESELECTION));  
  comint.Run(new CISelect(s));
  doc->UpdateAllViews(NULL);
}

void CObjektiv2View::OnPointsBevel() 
{
  CBevelDlg dlg;
  dlg.obj=GetDocument()->LodData->Active();
  if (LockMode==0) dlg.lockx=dlg.locky=dlg.lockz=true;
  else
  {
    dlg.lockx=(LockMode & CMS_MAPX)!=0;
    dlg.locky=(LockMode & CMS_MAPY)!=0;
    dlg.lockz=(LockMode & CMS_MAPZ)!=0;
  }
  if (dlg.DoModal()==IDOK)
  {
    comint.Begin(WString(IDS_BEVEL));
    comint.Run(new CIBevel(&dlg));
    GetDocument()->UpdateAllViews(this);
    Invalidate();
  }
}

void CObjektiv2View::OnSurfacesSetautomapselection() 
{
  GetDocument()->combine_saved=new Selection(*GetDocument()->LodData->Active()->GetSelection());
  GetDocument()->automapsel=GetDocument()->combine_saved->NFaces()!=0;
  GetDocument()->UpdateAllViews(this);
  Invalidate();
}

void CObjektiv2View::OnUpdateSurfacesSetautomapselection(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(GetDocument()->automapsel);
}


BOOL CObjektiv2View::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
  if (projection)
    distance+=(float)zDelta*0.01f;
  else
    scale*=(float)1.0f-(zDelta*0.001f);
  Invalidate();
  return TRUE;
}

void CObjektiv2View::OnViewWiresinsolid() 
{
  solidwires=!solidwires;	
  Invalidate();
}

void CObjektiv2View::OnUpdateViewWiresinsolid(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(solid);	
  pCmdUI->SetCheck(solidwires);
}

void CObjektiv2View::TurnEdgeSupp(CPoint & cursor)
{
  geVector4 a((float)cursor.x,(float)cursor.y,0),b((float)cursor.x,(float)cursor.y,1.0f);
  geMatrix4 mx,inv;
  memcpy(&mx,camera.GetTransform(),sizeof(mx));   //dost neadekvatni konverze
  inv.Invert(mx);
  inv.Transform4(a,a);
  inv.Transform4(b,b);
  a.Correct();
  b.Correct();
  geLine lline(a,b);
  comint.Begin(WString(IDS_TURNEDGEACTION));
  comint.Run(new CITurnEdge(lline));
  GetDocument()->UpdateAllViews(NULL);
}

void CObjektiv2View::OnRButtonDblClk(UINT nFlags, CPoint point)
{
  polselect.Reset();
  CView::OnRButtonDblClk(nFlags, point);
}

void CObjektiv2View::CalcRotationOnLocalAxis(float angle, HMATRIX mm)
{
  HMATRIX mx1,mx2;  
  HMATRIX lm1,lm2,lm3;
  LPHVECTOR lookvect;
  RotaceX(lm1,-rotx);
  RotaceZ(lm2,-rotz);
  SoucinMatic(lm1,lm2,lm3);
  lookvect=lm3[2];
    
  InverzeMatice(doc->gismo,mx1);
  if (LockMode & CMS_MAPX) 
  {
    float a=1;
    if (Scalar1(lookvect,doc->gismo[0])<0) a=-a;
    RotaceX(mx2,a*angle);SoucinMatic(mx1,mx2,mx1);
  }
  if (LockMode & CMS_MAPY) 
  {
    float a=1;
    if (Scalar1(lookvect,doc->gismo[2])<0) a=-a;
    RotaceY(mx2,a*angle);SoucinMatic(mx1,mx2,mx1);
  }
  if (LockMode & CMS_MAPZ) 
  {
    float a=1;
    if (Scalar1(lookvect,doc->gismo[1])<0) a=-a;
    RotaceZ(mx2,a*angle);SoucinMatic(mx1,mx2,mx1);
  }
  SoucinMatic(mx1,doc->gismo,mm);  
  frame->SetStatus(WString(IDS_ROTATION_STATUS,todec(angle)));
}

void CObjektiv2View::CalcScaleOnLocalAxis(float f, HMATRIX mm, bool pin)
{
  HMATRIX mx1,mx2;  
  HMATRIX lm1,lm2,lm3;
  LPHVECTOR lookvect;
  RotaceX(lm1,-rotx);
  RotaceZ(lm2,-rotz);
  SoucinMatic(lm1,lm2,lm3);
  lookvect=lm3[2];

  float grid=config->GetInt(IDS_CFGSCALESTEP)/100.0f;

  if (grid>0.0f)
  {f=floor(f/grid)*grid;}
  if (f<0.0f) f=0.0f;

  InverzeMatice(doc->gismo,mx1);
  if (LockMode & CMS_MAPX) 
  {
    float a=1;
    if (Scalar1(lookvect,doc->gismo[0])<0) a=-a;
    Zoom(mx2,f,1,1);SoucinMatic(mx1,mx2,mx1);
  }
  if (LockMode & CMS_MAPY) 
  {
    float a=1;
    if (Scalar1(lookvect,doc->gismo[2])<0) a=-a;
    Zoom(mx2,1,f,1);SoucinMatic(mx1,mx2,mx1);
  }
  if (LockMode & CMS_MAPZ) 
  {
    float a=1;
    if (Scalar1(lookvect,doc->gismo[1])<0) a=-a;
    Zoom(mx2,1,1,f);SoucinMatic(mx1,mx2,mx1);
  }
  SoucinMatic(mx1,doc->gismo,mm);  
  frame->SetStatus(WString(IDS_SCALE_STATUS,f*100.0f));
}

void CObjektiv2View::CalcMoveOnLocalAxis(float f, HMATRIX mm, bool pin)
{
  HMATRIX mx1,mx2;  
  HMATRIX lm1,lm2,lm3;
  LPHVECTOR lookvect;
  RotaceX(lm1,-rotx);
  RotaceZ(lm2,-rotz);
  SoucinMatic(lm1,lm2,lm3);
  lookvect=lm3[2];

  float grid=config->GetInt(IDS_CFGMOVESTEP)/100.0f;

  if (grid>0.0f)
  {f=floor(f/grid)*grid;}

  InverzeMatice(doc->gismo,mx1);
  if (LockMode & CMS_MAPX) 
  {
    float a=1;
    if (Scalar1(lookvect,doc->gismo[0])<0) a=-a;
    Translace(mx2,f,0,0);SoucinMatic(mx1,mx2,mx1);
  }
  if (LockMode & CMS_MAPY) 
  {
    float a=1;
    if (Scalar1(lookvect,doc->gismo[2])<0) a=-a;
    Translace(mx2,0,f,0);SoucinMatic(mx1,mx2,mx1);
  }
  if (LockMode & CMS_MAPZ) 
  {
    float a=1;
    if (Scalar1(lookvect,doc->gismo[1])<0) a=-a;
    Translace(mx2,0,0,f);SoucinMatic(mx1,mx2,mx1);
  }
  SoucinMatic(mx1,doc->gismo,mm);  
  frame->SetStatus(WString(IDS_TRANSLATE_STATUS,f,0,0));
}

VecT CObjektiv2View::CopyVectWithLock(const VecT &vect, const VecT &origin)
{
  VecT ret=origin;
  if (projection)
  {
    if (LockMode & CMS_MAPX) ret[0]=vect[0];
    if (LockMode & CMS_MAPY) ret[1]=vect[1];
    if (LockMode & CMS_MAPZ) ret[2]=vect[2];
  }
  else
  {
    LPHMATRIX mx1=camera.GetTransform();
    Matrix4 mx(mx1[0][0],mx1[1][0],mx1[2][0],0,
      mx1[0][1],mx1[1][1],mx1[2][1],0,
      mx1[0][2],mx1[1][2],mx1[2][2],0);
    Matrix4 inv=mx.InverseGeneral();
    
    VecT dif=inv.FastTransform(vect-origin);
    if ((LockMode & CMS_MAPX)==0)
    {
      mx(0,0)=0;
      mx(1,0)=0;
      mx(2,0)=0;
    }
    if ((LockMode & CMS_MAPY)==0)
    {
      mx(0,1)=0;
      mx(1,1)=0;
      mx(2,1)=0;
    }
    if ((LockMode & CMS_MAPZ)==0)
    {
      mx(0,2)=0;
      mx(1,2)=0;
      mx(2,2)=0;
    }

    ret=origin+mx.FastTransform(dif);
  }
  return ret;
}
