#if !defined(AFX_NORMANIMDLG_H__E6372D1A_1132_4ADD_ADEF_74E2C8C63963__INCLUDED_)
#define AFX_NORMANIMDLG_H__E6372D1A_1132_4ADD_ADEF_74E2C8C63963__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NormAnimDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CNormAnimDlg dialog

class CNormAnimDlg : public CDialog
  {
  // Construction
  public:
    CNormAnimDlg(CWnd* pParent = NULL);   // standard constructor
    
    LODObject *lod;
    CString selection;
    float flod;
    // Dialog Data
    //{{AFX_DATA(CNormAnimDlg)
    enum 
      { IDD = IDD_NORMANIM };
    CListCtrl	wSelList;
    CListCtrl	wRefLod;
    BOOL	selonly;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CNormAnimDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CNormAnimDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnItemchangedReflod(NMHDR* pNMHDR, LRESULT* pResult);
    virtual void OnOK();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NORMANIMDLG_H__E6372D1A_1132_4ADD_ADEF_74E2C8C63963__INCLUDED_)
