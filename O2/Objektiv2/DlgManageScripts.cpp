// DlgManageScripts.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "DlgManageScripts.h"
#include ".\dlgmanagescripts.h"
#include <strstream>
using namespace std;


// DlgManageScripts dialog

IMPLEMENT_DYNAMIC(DlgManageScripts, CDialog)
DlgManageScripts::DlgManageScripts(CWnd* pParent /*=NULL*/)
	: CDialog(DlgManageScripts::IDD, pParent)
{
  _dragItem=NULL;
  _dragImage=NULL;
}

DlgManageScripts::~DlgManageScripts()
{
}

void DlgManageScripts::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_MENUTREE, wTree);
#ifndef PUBLIC
  DDX_Control(pDX, IDC_RUNINO2, wRunInO2);
  DDX_Control(pDX, IDC_RUNASEXTERNAL, wRunAsExternal);
  DDX_Control(pDX, IDC_SAVECURFILE, wSaveCurFile);
  DDX_Control(pDX, IDC_SAVEALLMATS, wSaveAllMaterials);
  DDX_Control(pDX, IDC_LOGOUTPUT, wLogOutput);
  DDX_Control(pDX, IDC_INPUTFILE, wInputFile);
#endif
  DDX_Control(pDX, IDC_ABOUTSCRIPT, wAboutScript);
}


BEGIN_MESSAGE_MAP(DlgManageScripts, CDialog)
  ON_WM_DESTROY()
  ON_NOTIFY(TVN_ENDLABELEDIT, IDC_MENUTREE, OnTvnEndlabeleditMenutree)
  ON_BN_CLICKED(IDC_NEWFOLDER, OnBnClickedNewfolder)
  ON_BN_CLICKED(IDC_DELETE, OnBnClickedDelete)
  ON_NOTIFY(TVN_BEGINLABELEDIT, IDC_MENUTREE, OnTvnBeginlabeleditMenutree)
  ON_BN_CLICKED(IDC_RENAME, OnBnClickedRename)
  ON_BN_CLICKED(IDC_BBROWSE, OnBnClickedBbrowse)
  ON_BN_CLICKED(IDC_INSERTAFTER, OnBnClickedInsertafter)
  ON_NOTIFY(NM_DBLCLK, IDC_MENUTREE, OnNMDblclkMenutree)
  ON_BN_CLICKED(IDC_REPLACE, OnBnClickedReplace)
  ON_NOTIFY(TVN_BEGINDRAG, IDC_MENUTREE, OnTvnBegindragMenutree)
  ON_WM_MOUSEMOVE()
  ON_WM_LBUTTONUP()
  ON_WM_CANCELMODE()
  ON_WM_CAPTURECHANGED()
  ON_BN_CLICKED(IDC_ADDFOLDER, OnBnClickedAddfolder)
  ON_BN_CLICKED(IDC_EDIT, OnBnClickedEdit)
  ON_NOTIFY(TVN_SELCHANGED, IDC_MENUTREE, OnTvnSelchangedMenutree)
  ON_EN_UPDATE(IDC_SCRIPTFILENAME, OnEnUpdateScriptfilename)
  ON_BN_CLICKED(IDC_CONFIGURE, OnBnClickedConfigure)
  ON_BN_CLICKED(IDC_EDITSCRIPT, OnBnClickedEditscript)
  ON_BN_CLICKED(IDC_RUNINO2, OnBnClickedRunino2)
  ON_BN_CLICKED(IDC_RUNASEXTERNAL, OnBnClickedRunasexternal)
  ON_BN_CLICKED(IDC_LOGOUTPUT, OnBnClickedLogoutput)
  ON_BN_CLICKED(IDC_INPUTFILE, OnBnClickedInputfile)
  ON_BN_CLICKED(IDC_ABOUTSCRIPT, OnBnClickedAboutscript)
END_MESSAGE_MAP()


// DlgManageScripts message handlers


int DlgManageScripts::LoadToTree(HTREEITEM root, int idx)
{
  while (idx<_items.Size())
  {
    DlgManageScriptsItemInfo &item=_items[idx];
    if (item.newFolder)
    {
      HTREEITEM itm=wTree.InsertItem(item.itemName,ILST_FOLDER,ILST_FOLDER,root,TVI_LAST);
      idx=LoadToTree(itm,idx+1);
    }
    else if (item.itemName=="-")
    {
      HTREEITEM itm=wTree.InsertItem(item.itemName,ILST_SEPARATOR,ILST_SEPARATOR,root,TVI_LAST);
    }
    else 
    {
      HTREEITEM itm=wTree.InsertItem(item.itemName,ILST_SCRIPT,ILST_SCRIPT,root,TVI_LAST);
      wTree.SetItemData(itm,(DWORD_PTR)(new DlgManageScriptsItemInfo(item)));
    }
    if (item.lastItem) return idx;
    idx++;
  }
  return idx;
}

void DlgManageScripts::DeleteChilds(HTREEITEM itm)
{
  HTREEITEM chld=wTree.GetNextItem(itm,TVGN_CHILD);
  while (chld)
  {
    DeleteTreeItem(chld);
    chld=wTree.GetNextItem(itm,TVGN_CHILD);
  }
}

void DlgManageScripts::DeleteTreeItem(HTREEITEM itm)
{
  DlgManageScriptsItemInfo *val=(DlgManageScriptsItemInfo *)wTree.GetItemData(itm);
  delete val;  
  DeleteChilds(itm);
  wTree.DeleteItem(itm);
}


void DlgManageScripts::SaveFromTree(HTREEITEM subtree)
{
  HTREEITEM child;
  DlgManageScriptsItemInfo temp;
  if (subtree==NULL) child=wTree.GetRootItem();
  else child=wTree.GetNextItem(subtree,TVGN_CHILD);
  while (child)
  {
    DlgManageScriptsItemInfo *val=(DlgManageScriptsItemInfo *)wTree.GetItemData(child);
    HTREEITEM next=wTree.GetNextItem(child,TVGN_NEXT);
    HTREEITEM cch=wTree.GetNextItem(child,TVGN_CHILD);
    if (val!=NULL || cch!=NULL)
    {
      if (val==NULL) val=&temp;
      val->lastItem=next==NULL;
      val->newFolder=cch!=NULL;
      val->itemName=wTree.GetItemText(child);
      _items.Append()=*val;
    }
    if (cch) SaveFromTree(child);
    child=next;
  }
}

void DlgManageScripts::SaveRegistry()
{
  ostrstream buff;
  for (int i=0;i<_items.Size();i++)
  {
    DlgManageScriptsItemInfo &item=_items[i];
    buff<<(const char *)item.itemName;
    buff.put(0);
    buff<<(((const char *)item.scriptPath)?(const char *)item.scriptPath:"");
    buff.put(0);
    buff.write((char *)&item.flags,sizeof(item.flags));
    buff.write((char *)&item.idassign,sizeof(item.idassign));
    buff<<(const char *)item.cmdline;
    buff.put(0);
    if (item.inputFile)
    {
      buff<<(const char *)item.promptInput;
      buff.put(0);
    }
    if (item.logOutput)
    {
      buff<<(const char *)item.promptOutput;
      buff.put(0);
    }
  }
  theApp.WriteProfileBinary("Scripts","MenuItems",(LPBYTE)buff.str(),buff.pcount());
  buff.freeze(false);
}

void DlgManageScripts::LoadRegistry()
{
  BYTE *data;
  UINT size;
  _items.Clear();
  if (theApp.GetProfileBinary("Scripts","MenuItems",&data,&size))
  {
    char buff[2048];
    istrstream input((char *)data,size);
    input.getline(buff,2048,(char)0);
    while (!(!input))
    {
      DlgManageScriptsItemInfo item;
      item.itemName=buff;
      input.getline(buff,2048,(char)0);
      item.scriptPath=buff;
      input.read((char *)&item.flags,sizeof(item.flags));
      input.read((char *)&item.idassign,sizeof(item.idassign));
      input.getline(buff,2048,(char)0);
      item.cmdline=buff;
      _items.Append()=item;
      if (item.inputFile)
      {
        input.getline(buff,2048,(char)0);
        item.promptInput=buff;
      }
      if (item.logOutput)
      {
        input.getline(buff,2048,(char)0);
        item.promptInput=buff;
      }
      input.getline(buff,2048,(char)0);
    }
  delete [] data;
  }
}

static DlgManageScriptsItemInfo *FindID(AutoArray<DlgManageScriptsItemInfo>&  array, int item)
{
  for (int i=0;i<array.Size();i++) if (array[i].idassign==item) return &(array[i]);
  return NULL;
}

static int GetUsefulID(AutoArray<DlgManageScriptsItemInfo>&  array, int item)
{
  if (array[item].idassign!=-1) return array[item].idassign;
  int i=0;
  while (FindID(array,i)!=NULL) i++;
  array[item].idassign=i;
  return i;
}

int DlgManageScripts::InitMenu(CMenu *menu, int idoffset, int index, int delafter)
{
  if (delafter>=0)
  {
    while (menu->GetMenuItemCount()>(UINT)delafter)
    {
      menu->DeleteMenu(delafter,MF_BYPOSITION);
    }
  }
  while (index<_items.Size())
  {
    DlgManageScriptsItemInfo &info=_items[index];
    if (info.newFolder)
    {
      CMenu submenu;
      submenu.CreatePopupMenu();
      index=InitMenu(&submenu,idoffset,index+1);
      menu->AppendMenu(MF_POPUP|MF_STRING,(UINT_PTR)(submenu.Detach()),info.itemName);
      if (info.lastItem) return index;
    }
    else
    {
      if (info.itemName=='-')
        menu->AppendMenu(MF_SEPARATOR,0,"");
      else      
        menu->AppendMenu(MF_STRING,idoffset+GetUsefulID(_items,index),info.itemName);
      if (info.lastItem) return index;
    }
    index++;
  }
return index;
}

BOOL DlgManageScripts::OnInitDialog()
{
  CDialog::OnInitDialog();
  LoadToTree();
  wTree.SetImageList(&shrilist,TVSIL_NORMAL);
  wRunInO2.SetCheck(1);
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgManageScripts::OnDestroy()
{
  while (wTree.GetRootItem()) DeleteTreeItem(wTree.GetRootItem());
  // TODO: Add your message handler code here
  delete _dragImage;
  _dragItem=NULL;

}

void DlgManageScripts::OnOK()
{  
  _items.Clear();
 this->SaveFromTree();
 CDialog::OnOK();
}

void DlgManageScripts::OnTvnEndlabeleditMenutree(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMTVDISPINFO pTVDispInfo = reinterpret_cast<LPNMTVDISPINFO>(pNMHDR);
  *pResult = TRUE;  
  inedit=false;
}

void DlgManageScripts::OnBnClickedNewfolder()
{
  HTREEITEM cur=wTree.GetSelectedItem();
  HTREEITEM root=NULL;
  if (cur) root=wTree.GetParentItem(cur);  
  cur=wTree.InsertItem("",ILST_FOLDER,ILST_FOLDER,root,cur);
  wTree.Select(cur,TVGN_CARET);
  wTree.EditLabel(cur);
}


void DlgManageScripts::OnBnClickedDelete()
{
  HTREEITEM cur=wTree.GetSelectedItem();
  if (cur) DeleteTreeItem(cur);
}

void DlgManageScripts::OnTvnBeginlabeleditMenutree(NMHDR *pNMHDR, LRESULT *pResult)
{
  inedit=true;
  *pResult = FALSE;
}

BOOL DlgManageScripts::PreTranslateMessage(MSG* pMsg)
{
  if (inedit && pMsg->message>=WM_KEYFIRST && pMsg->message<=WM_KEYLAST)
  {
    TranslateMessage(pMsg);
    DispatchMessage(pMsg);
    return TRUE;
  }
  return CDialog::PreTranslateMessage(pMsg);
}

void DlgManageScripts::OnBnClickedRename()
{
  HTREEITEM cur=wTree.GetSelectedItem();
  if (cur) wTree.EditLabel(cur);
}

void DlgManageScripts::OnBnClickedBbrowse()
{
  CString defname;
  GetDlgItemText(IDC_SCRIPTFILENAME,defname);
  CFileDialogEx dlg(TRUE,NULL,defname,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,WString(IDS_SCRIPTFILTER),this);
  if (dlg.DoModal()==IDOK)
  {
    CString name=dlg.GetPathName();
    SetDlgItemText(IDC_SCRIPTFILENAME,name);
    SendDlgItemMessage(IDC_SCRIPTFILENAME,EM_SETSEL,name.GetLength(),name.GetLength());
  }
}

void DlgManageScripts::SaveToStruct(DlgManageScriptsItemInfo &result)
{
  CString text;
  GetDlgItemText(IDC_SCRIPTFILENAME,text);
  result.scriptPath=text;
#ifndef PUBLIC
  result.saveFile=wSaveCurFile.GetCheck()!=0;
  result.saveMaterials=wSaveAllMaterials.GetCheck()!=0;
  result.asExternal=wRunAsExternal.GetCheck()!=0;  
  result.logOutput=wLogOutput.GetCheck()!=0;
  result.inputFile=wInputFile.GetCheck()!=0;
  GetDlgItemText(IDC_CMDLINE,result.cmdline);
  result.promptInput=promptInput;
  result.promptOutput=promptOutput;
#else
  result.saveFile=FALSE;
  result.saveMaterials=FALSE;
  result.asExternal=FALSE;
  result.logOutput=FALSE;
  result.inputFile=FALSE;
#endif
}

void DlgManageScripts::OnBnClickedInsertafter()
{
  DlgManageScriptsItemInfo info;
  SaveToStruct(info);
  HTREEITEM cur=wTree.GetSelectedItem();
  HTREEITEM root=NULL;
  if (cur) root=wTree.GetNextItem(cur,TVGN_PARENT);
  const char *title=info.scriptPath.IsNull()?"-":info.scriptPath.GetTitle();
  cur=wTree.InsertItem(title,ILST_SCRIPT,ILST_SCRIPT,root,cur);
  wTree.SetItemData(cur,(DWORD_PTR)(new DlgManageScriptsItemInfo(info)));
  wTree.Select(cur,TVGN_CARET);
  if (!info.scriptPath.IsNull()) wTree.EditLabel(cur);
  SetDlgItemText(IDC_SCRIPTFILENAME,"");
}


void DlgManageScripts::OnNMDblclkMenutree(NMHDR *pNMHDR, LRESULT *pResult)
{
  OnBnClickedEdit();
  *pResult = 0;
}

void DlgManageScripts::OnBnClickedReplace()
{
  HTREEITEM cur=wTree.GetSelectedItem();
  if (cur)
  {
    DlgManageScriptsItemInfo info;
    SaveToStruct(info);
    DlgManageScriptsItemInfo *nfo=(DlgManageScriptsItemInfo *)wTree.GetItemData(cur);
    *nfo=info;
  }
}

void DlgManageScripts::OnTvnBegindragMenutree(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
  CPoint pos(GetMessagePos());
  ScreenToClient(&pos);

  _dragItem=pNMTreeView->itemNew.hItem;
  SetCapture();
  *pResult = 0;
}

void DlgManageScripts::OnMouseMove(UINT nFlags, CPoint point)
{
  // TODO: Add your message handler code here and/or call default
  if (_dragItem)
  {
    CPoint ls=point;
    ClientToScreen(&ls);
    wTree.ScreenToClient(&ls);
    UINT flags=0;
    HTREEITEM hit=wTree.HitTest(ls,&flags);
    wTree.SetInsertMark(hit, TRUE);
  }

  CDialog::OnMouseMove(nFlags, point);
}

bool DlgManageScripts::IsFolder(HTREEITEM itm)
{
  int img;
  wTree.GetItemImage(itm,img,img);
  return img==ILST_FOLDER;
}

void DlgManageScripts::MoveItemToItemNoTest(HTREEITEM what, HTREEITEM where,bool last)
{
  HTREEITEM pass=where;
  if (!IsFolder(where) || (wTree.GetItemState(where,TVIS_EXPANDED)& TVIS_EXPANDED)==0 && wTree.ItemHasChildren(where)) where=wTree.GetParentItem(where);  
  else pass=last?TVI_LAST:TVI_FIRST;
  int image;
  wTree.GetItemImage(what,image,image);
  CString text=wTree.GetItemText(what);
  DWORD_PTR data=wTree.GetItemData(what);
  HTREEITEM newitm=wTree.InsertItem(text,image,image,where,pass);
  wTree.SetItemData(newitm,data);
  HTREEITEM child=wTree.GetChildItem(what);
  while (child)
  { 
    MoveItemToItemNoTest(child,newitm,true);
    child=wTree.GetChildItem(what);
  }
  wTree.DeleteItem(what);
  wTree.SelectItem(newitm);
}

void DlgManageScripts::MoveItemToItem(HTREEITEM what, HTREEITEM where)
{
  HTREEITEM test=where;
  while (test!=what && test)
    test=wTree.GetParentItem(test);
  if (test==what) return;
  MoveItemToItemNoTest(what,where);
}

void DlgManageScripts::OnLButtonUp(UINT nFlags, CPoint point)
{
  if (_dragItem)
  {
    CPoint ls=point;
    ClientToScreen(&ls);
    wTree.ScreenToClient(&ls);
    UINT flags=0;
    HTREEITEM hit=wTree.HitTest(ls,&flags);
    if (hit && (GetKeyState(VK_CONTROL) & 0x80)!=0)
      hit=wTree.GetParentItem(hit);
    MoveItemToItem(_dragItem,hit);
    ReleaseCapture();
    ShowCursor(true);
    wTree.SetInsertMark(NULL);
    _dragItem=NULL;
  }
  CDialog::OnLButtonUp(nFlags, point);
}

void DlgManageScripts::OnCancelMode()
{
   if (_dragItem) 
   {
    wTree.SetInsertMark(NULL);
    ReleaseCapture();
    _dragItem=NULL;
   }
  CDialog::OnCancelMode();

  // TODO: Add your message handler code here
}

void DlgManageScripts::OnCaptureChanged(CWnd *pWnd)
{
   if (_dragItem) 
   {
    _dragItem=NULL;
    wTree.SetInsertMark(NULL);
   }

  CDialog::OnCaptureChanged(pWnd);
}

void DlgManageScripts::OnBnClickedAddfolder()
{
  const char *path=AskForPath(*this);
  if (path==NULL) return;
  Pathname mask;
  mask.SetDirectory(path);
  mask.SetFilename("*.bio2s");
  CFileFind fnd;
  if (fnd.FindFile(mask.GetFullPath()))
  {
    BOOL next;
    HTREEITEM fld=wTree.InsertItem(Pathname::GetNameFromPath(path),ILST_FOLDER,ILST_FOLDER);
    do
      {
      next=fnd.FindNextFile();      
      HTREEITEM item=wTree.InsertItem(fnd.GetFileTitle(),ILST_SCRIPT,ILST_SCRIPT,fld);
      DlgManageScriptsItemInfo *nfo=new DlgManageScriptsItemInfo;
      SaveToStruct(*nfo);
      nfo->scriptPath=fnd.GetFilePath();
      wTree.SetItemData(item,(DWORD_PTR)nfo);
      }
    while (next);
  }
}

void DlgManageScripts::OnBnClickedEdit()
{
  HTREEITEM cur=wTree.GetSelectedItem();
  if (cur)
  {
    DlgManageScriptsItemInfo *nfo=(DlgManageScriptsItemInfo *)wTree.GetItemData(cur);
    if (nfo)
    {
      CString name=nfo->scriptPath;
      SetDlgItemText(IDC_SCRIPTFILENAME,name);
      SendDlgItemMessage(IDC_SCRIPTFILENAME,EM_SETSEL,name.GetLength(),name.GetLength());
      wSaveCurFile.SetCheck(nfo->saveFile);
#ifndef PUBLIC
      wSaveAllMaterials.SetCheck(nfo->saveMaterials);
      wRunAsExternal.SetCheck(nfo->asExternal);
      wRunInO2.SetCheck(!nfo->asExternal);    
      wLogOutput.SetCheck(nfo->logOutput);
      wInputFile.SetCheck(nfo->inputFile);
      if (nfo->asExternal) OnBnClickedRunasexternal();
      else OnBnClickedRunino2();
      promptOutput=nfo->promptOutput;
      promptInput=nfo->promptInput;
#endif
      SetDlgItemText(IDC_CMDLINE,nfo->cmdline);
 
    }
  }
}

void DlgManageScripts::OnTvnSelchangedMenutree(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
  DlgManageScriptsItemInfo *nfo=(DlgManageScriptsItemInfo *)pNMTreeView->itemNew.lParam;
  GetDlgItem(IDC_EDIT)->EnableWindow(nfo!=NULL);
  GetDlgItem(IDC_CONFIGURE)->EnableWindow(nfo!=NULL && !nfo->asExternal);
  GetDlgItem(IDC_EDITSCRIPT)->EnableWindow(nfo!=NULL);
  GetDlgItem(IDC_RENAME)->EnableWindow(TRUE);
  GetDlgItem(IDC_DELETE)->EnableWindow(TRUE);
  GetDlgItem(IDC_ABOUTSCRIPT)->EnableWindow(nfo!=NULL);
  int cnt=GetDlgItem(IDC_SCRIPTFILENAME)->GetWindowTextLength();
  GetDlgItem(IDC_REPLACE)->EnableWindow(cnt!=0 && nfo!=NULL);
    
}

void DlgManageScripts::OnEnUpdateScriptfilename()
{
  int cnt=GetDlgItem(IDC_SCRIPTFILENAME)->GetWindowTextLength();  
  GetDlgItem(IDC_REPLACE)->EnableWindow(cnt!=0 && wTree.GetItemData(wTree.GetSelectedItem())!=0);
  GetDlgItem(IDC_INSERTAFTER)->EnableWindow(cnt!=0);
}

const DlgManageScriptsItemInfo *DlgManageScripts::FromMenu(int id, int idoffset)
{
  return FindID(_items,id-idoffset);
}
void DlgManageScripts::OnBnClickedConfigure()
{
  HTREEITEM cur=wTree.GetSelectedItem();
  if (cur)
  {
    DlgManageScriptsItemInfo *nfo=(DlgManageScriptsItemInfo *)wTree.GetItemData(cur);
    if (nfo)
    {
      RunScriptSettings(nfo->scriptPath,RunScriptContext,*this);
    }
  }
}

void DlgManageScripts::OnBnClickedEditscript()
{
  DlgManageScriptsItemInfo *nfo=(DlgManageScriptsItemInfo *)wTree.GetItemData(wTree.GetSelectedItem());
  CString editor=config->GetString(IDS_SCRIPTEDITOR);
  theApp.rocheck.TestFileRO(nfo->scriptPath,ROCHF_DisableSaveAs);
  ShellExecute(*this,NULL,editor,nfo->scriptPath,nfo->scriptPath.GetDirectoryWithDrive(),SW_SHOWNORMAL);
}

void DlgManageScripts::OnBnClickedRunino2()
{
  GetDlgItem(IDC_CMDLINE)->EnableWindow(FALSE);  
  wLogOutput.EnableWindow(FALSE);  
  wInputFile.EnableWindow(FALSE);  
}

void DlgManageScripts::OnBnClickedRunasexternal()
{
  GetDlgItem(IDC_CMDLINE)->EnableWindow(TRUE);
  wLogOutput.EnableWindow(TRUE);
  wInputFile.EnableWindow(TRUE);  
}

#include "dlginputfile.h"

void DlgManageScripts::OnBnClickedLogoutput()
{
  DlgManageScriptsItemInfo *nfo=(DlgManageScriptsItemInfo *)wTree.GetItemData(wTree.GetSelectedItem());
  if (wLogOutput.GetCheck())
  {
    CDlgInputFile dlg;
    dlg.vFilename=promptOutput;
    dlg.title.LoadString(IDS_SCRIPTOUTPUTPROMPT);
    dlg.browse=false;
    dlg.DoModal();
    promptOutput=dlg.vFilename;
  }
  
}

void DlgManageScripts::OnBnClickedInputfile()
{
  DlgManageScriptsItemInfo *nfo=(DlgManageScriptsItemInfo *)wTree.GetItemData(wTree.GetSelectedItem());
  if (wInputFile.GetCheck())
  {
    CDlgInputFile dlg;
    dlg.vFilename=promptInput;
    dlg.title.LoadString(IDS_SCRIPTINPUTPROMPT);
    dlg.browse=false;
    dlg.DoModal();
    promptInput=dlg.vFilename;
  }
}

void DlgManageScripts::OnBnClickedAboutscript()
{
  DlgManageScriptsItemInfo *nfo=(DlgManageScriptsItemInfo *)wTree.GetItemData(wTree.GetSelectedItem());
  ifstream infile(nfo->scriptPath,ios::in|ios::binary);
  if (!infile) return;
  ws(infile);
  int p=infile.get();
  int q=infile.get();
  if (p!='/' || q!='*') 
  {
    AfxMessageBox(IDS_NOSCRIPTDESCRIPTION);
  }
  else
  {
    CString text;
    char buff[256];
    do
    {
      infile.get(buff,256,'*');
      text+=buff;
      infile.clear();
      p=infile.get();
      if (p==EOF) break;
      while (infile.peek()=='*') 
        {
          text+=(char)p;    
          p=infile.get();
        }
      if (infile.peek()=='/') break;
      text+=(char)p;    
    }    
    while (true);
    MessageBox(text,nfo->scriptPath.GetFilename(),MB_ICONINFORMATION);
  }
      
}

