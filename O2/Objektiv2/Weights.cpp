// Weights.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "Weights.h"
#include "MainFrm.h"
#include "ComInt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWeights dialog


CWeights::CWeights(CWnd* pParent /*=NULL*/)
  : CDialog(CWeights::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CWeights)
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CWeights::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CWeights)
  DDX_Control(pDX, IDC_LIST, List);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CWeights, CDialog)
  //{{AFX_MSG_MAP(CWeights)
  ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LIST, OnEndlabeleditList)
    ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclkList)
      //}}AFX_MSG_MAP
      END_MESSAGE_MAP()
        
        /////////////////////////////////////////////////////////////////////////////
        // CWeights message handlers
        
        static float CalculateWeight( const NamedSelection *sel, ObjectData *obj )
          {
          float sumSel=0;
          int nSel=0;
          for( int i=0; i<obj->NPoints(); i++ )
            {
            if( !obj->PointSelected(i) ) continue;
            if( sel->PointSelected(i) )
              {
              nSel++;
              sumSel+=sel->PointWeight(i);
              }
            }
          if( nSel>0 ) return sumSel/nSel;
          return 0;
          }

//--------------------------------------------------

BOOL CWeights::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  
  CRect rc;
  List.GetClientRect(&rc);
  List.SetImageList(&shrilist,LVSIL_SMALL);
  List.InsertColumn(0,WString(IDS_WEIGHTVALUE),LVCFMT_LEFT,80,0);
  List.InsertColumn(1,WString(IDS_WEIGHTNAME),LVCFMT_LEFT,rc.right-81,1);	
  bool somePoint=false;
  for( int ns=0; ns<MAX_NAMED_SEL; ns++ )
    {
    const NamedSelection *sel=obj->GetNamedSel(ns);
    if( !sel ) continue;
    int value=(int)(CalculateWeight(sel,obj)*100+0.5);
    if( !value ) continue;
    // insert selection into the list view
    WString valueS;
    valueS.Sprintf("%2d",value);
    LVITEM it;
    it.mask=LVIF_TEXT|LVIF_IMAGE |LVIF_PARAM ;
    it.iItem=ns;
    it.iSubItem=0;
    it.pszText=(char *)((LPCTSTR)valueS);
    it.iImage=ILST_WEIGHT;
    it.lParam=ns;
    int idx=List.InsertItem(&it);
    List.SetItemText(idx,1,sel->Name());
    somePoint=true;
    }
  if( !somePoint ) 
    PostMessage(WM_COMMAND, IDCANCEL, 0);
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

void CWeights::OnEndlabeleditList(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
  // TODO: Add your control notification handler code here
  
  *pResult = 0;
  if (pDispInfo->item.pszText==NULL) return;
  List.SetItem(&pDispInfo->item);
  }

//--------------------------------------------------

void CWeights::OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  int idx=List.GetNextItem(-1,LVNI_FOCUSED);
  List.EditLabel(idx);
  *pResult = 0;
  }

//--------------------------------------------------

bool CWeights::CheckValues()
  {
  int cnt=List.GetItemCount();
  for (int i=0;i<cnt;i++)
    {
    char text[4];
    if (List.GetItemText(i,0,text,sizeof(text))!=0) 
      {
      int i;
      if (sscanf(text,"%d",&i)==1)
        {
        if (i>=0) continue;
        }	  
      }
    List.SetItemState(i,LVNI_FOCUSED,LVNI_FOCUSED);
    AfxMessageBox(IDS_INVALIDWEIGHT);
    return false;
    }
  return true;
  }

//--------------------------------------------------

class CIWeight:public CICommand
  {
  int selidx;
  float value;
  public:
    CIWeight(int idx,float weight):selidx(idx),value(weight) 
      {recordable=false;}
    virtual int Execute(LODObject *lod)
      {
      ObjectData *obj=lod->Active();
      NamedSelection *sel=obj->GetNamedSel(selidx);
      if (value==0)
        {
        for( int p=0; p<obj->NPoints(); p++ ) if( obj->PointSelected(p) )
          sel->PointSelect(p,false);
        obj->SetDirty();
        
        }
      else
        {
        float oldValue=CalculateWeight(sel,obj);
        if( value>=1 && oldValue<1 )
          {
          for( int p=0; p<obj->NPoints(); p++ ) if( obj->PointSelected(p) )
            {
            float wo=sel->PointWeight(p);
            if( wo>0 )
              {
              sel->SetPointWeight(p,1);
              obj->SetDirty();
              }
            }
          }
        else
          {
          float factor=value/oldValue;
          if( fabs(factor-1)>1-3 )
            { // some change required
            for( int p=0; p<obj->NPoints(); p++ ) if( obj->PointSelected(p) )
              {
              float wo=sel->PointWeight(p);
              float w=wo*factor; // never allow to disappear
              if( w<2.0/255 && wo>0 ) w=2.0/255;
              sel->SetPointWeight(p,w);
              obj->SetDirty();
              }
            }
          }
        }
      return 0;
      }
  };

//--------------------------------------------------

void CWeights::OnOK() 
  {
  if (CheckValues()==false) return;
  int cnt=List.GetItemCount();
  LVITEM it;
  char c[4];
  it.mask=LVIF_PARAM|LVIF_TEXT;
  it.iSubItem=0;
  it.pszText=c;
  it.cchTextMax=sizeof(c);
  comint.Begin(WString(IDS_SETWEIGHTS));
  for (int i=0;i<cnt;i++)
    {
    it.iItem=i;
    List.GetItem(&it);  
    int d;
    sscanf(c,"%d",&d);
    float f=(float)d/100.0f;
    if (comint.Run(new CIWeight(it.lParam,f))==-1) return;	
    }
  CDialog::OnOK();
  }

//--------------------------------------------------

