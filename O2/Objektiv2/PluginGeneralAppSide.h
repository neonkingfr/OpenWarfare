#pragma once
#include "iplugingeneral.h"

class CMainFrame;


class PluginGeneralAppSide : public IPluginGeneralAppSide
{
  IPluginGeneralPluginSide *_pluginSide;
  HINSTANCE _plugInstance;
  CMainFrame *_appframe;    

protected:
  virtual void UpdateViewer();                         
  virtual void ReloadViewer();                         
  virtual SccFunctions *GetSccInterface();         
  virtual const char *GetProjectRoot();           
  virtual bool IsViewerActive();
  virtual IExternalViewer *GetViewerInterface();
  virtual bool OpenOptionsDialog();
  virtual void UpdateViewer(ObjectData *obj);
  virtual HMENU AddSubMenu(PopupMenu menuId, const char *menuName);
  virtual bool AddMenuItem(HMENU mnu, const char *menuName, IPluginCommand *command);
  virtual bool AddMenuItem(PopupMenu menuId, const char *menuName, IPluginCommand *command);
  virtual void SelectInO2(const char *resourceName, ResourceType resourceType);
  virtual ROCheckResult TestFileRO(const _TCHAR *filename,int flags=0, HWND hWnd=NULL); //vola funkci pro TestFileRO v O2
  virtual bool AddPopupMenuItem(HMENU mnu, const char *menuName, IPluginCommand *command);
  virtual const LODObject *GetActiveObject();
  virtual const ObjectData *GetActiveLevel();
  virtual bool UpdateActiveObject(const LODObject &newObject);
  virtual bool UpdateActiveLevel(const ObjectData &objectData);
  virtual bool ModifyActiveLevel(const ObjectData *merge=0, Selection *delSelects=0);
  virtual bool BeginUpdate(const char *description);
  virtual bool EndUpdate();
  virtual const char *ReadConfigValue(int idsConfig);
  virtual ProgressBarFunctions *GetProgressBarHandler();
  virtual IFileDlgHistory *GetFileDialogExHandler();

public:
  PluginGeneralAppSide(void);
  ~PluginGeneralAppSide(void);

  void InitFramework(CMainFrame *appframe) {_appframe=appframe;}

  bool LoadPlugin(const char *dllpath);
  bool IsLoaded() {return _pluginSide!=NULL;}
  bool UnloadPlugin();

  IPluginGeneralPluginSide &GetPlugin() {return *_pluginSide;}


};

