// FlattenPointsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "FlattenPointsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFlattenPointsDlg dialog


CFlattenPointsDlg::CFlattenPointsDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CFlattenPointsDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CFlattenPointsDlg)
    mode = 3;
    pin = FALSE;
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CFlattenPointsDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CFlattenPointsDlg)
  DDX_Radio(pDX, IDC_RADIO1, mode);
  DDX_Check(pDX, IDC_CHECK1, pin);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CFlattenPointsDlg, CDialog)
  //{{AFX_MSG_MAP(CFlattenPointsDlg)
  // NOTE: the ClassWizard will add message map macros here
  //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CFlattenPointsDlg message handlers
    