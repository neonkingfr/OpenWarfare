#include "stdafx.h"
#include "Objektiv2.h"
#include "Config.h"
#include "MainFrm.h"
#include "dlginputfile.h"
#include "resource.h"
#include "..\ObjektivLib\O2ScriptLib\GdLODobject.h"
#include "O2ScriptCommands.h"
#include "..\ObjektivLib\BIANMimport.h"
#include "..\objektivlib\objtoolanimation.h"


#define Category "Objektiv2::Internal"

static GameValue o2GetConfig(const GameState *gs, GameValuePar oper1)
{
  int val=toInt(oper1);
  if (val<CFG_VALUENAMES || val>=CFG_VALUESCOUNT+CFG_VALUENAMES) return GameValue();
  return RString(config->GetString(val));
}

static GameValue o2GetConfigBool(const GameState *gs, GameValuePar oper1)
{
  int val=toInt(oper1);
  if (val<CFG_VALUENAMES || val>=CFG_VALUESCOUNT+CFG_VALUENAMES) return GameValue();
  return config->GetBool(val);
}

static GameValue o2GetConfigScalar(const GameState *gs, GameValuePar oper1)
{
  int val=toInt(oper1);
  if (val<CFG_VALUENAMES || val>=CFG_VALUESCOUNT+CFG_VALUENAMES) return GameValue();
  return (float)config->GetInt(val);
}

static GameValue o2MenuCommand(const GameState *gs, GameValuePar oper1)
{
  int val=toInt(oper1);
  frame->SendMessage(WM_COMMAND,val,0);
  return GameValue();
}

static const char *matname;
static HANDLE eventEdit;

static void __stdcall EditMaterial(HWND wnd, UINT, UINT_PTR nEvent, DWORD)
{
  frame->_MAT.CallMat().EditMaterial(matname);  
  SetEvent(eventEdit);
  KillTimer(wnd, nEvent);
}

static GameValue o2EditMaterial(const GameState *gs, GameValuePar oper1)
{
  if (!frame->_MAT.IsLoaded()) return false;
  eventEdit=CreateEvent(NULL,FALSE,FALSE,NULL);
  RString name=oper1;
  matname=name;
  frame->SetTimer(129786,1,EditMaterial);
  WaitForSingleObject(eventEdit,INFINITE);
  CloseHandle(eventEdit);
  return true;
}


static GameValue o2InputBox(const GameState *gs, GameValuePar oper1)
{
  const GameArrayType &arr=oper1;
  if (arr.Size()<3) return GameValue();
  RString content=arr[0];
  bool browse=arr[1];
  RString title=arr[2];
  RString filters=arr.Size()==3?"":arr[3];
  CDlgInputFile dlg;
  dlg.browse=browse;
  dlg.title=title;
  dlg.filter=filters;
  dlg.vFilename=content;
  if (arr.Size()==3) dlg.GuessFilter(content);
  if (dlg.DoModal()==IDOK)
  {
    return RString(dlg.vFilename);
  }
  else
    return RString("");
}

static BOOL CALLBACK SearchAndHide(HWND hWnd, LPARAM lParam)
{
  if (GetDlgItem(hWnd,IDC_UNLOCK)!=NULL)
  {
    ShowWindow(hWnd,SW_HIDE);
    return FALSE;
  }
  return TRUE;
}


static GameValue o2NoRunScriptDialog(const GameState *gs)
{
  EnumThreadWindows(::GetWindowThreadProcessId(*frame,NULL),SearchAndHide,0);
  return GameValue();
}

static GameValue o2UpdateThisObject(const GameState *gs)
{
  GameValue oper1=gs->VarGet("this");
    Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
    LODObject *current=doc->LodData;
    *(lodobj->_ref)=*current;
  return GameValue();    
}

static GameValue o2BiAnimImport(const GameState *gs, GameValuePar oper1)
{
  const GameArrayType &arr=oper1;
  if (arr.Size()<4) return RString("Less parameters");
  RString name=arr[0];
  float scale=arr[1];
  int invert=toInt((float)arr[2]);
  GdObjectData *obj=static_cast<GdObjectData *>(arr[3].GetData());
  CString err;
  
  CBIANMImport anm;
  ifstream infile(name,ios::in|ios::binary);
  if (!infile) {err.LoadString(IDS_LOADUNSUCCESFUL);return RString(err);}

  int errline;
  int error=anm.ParseSkeleton(infile,&errline);
  if (!error)
  {
    anm.ImportBones(*(obj->_object),scale,(invert & 1)!=0,(invert & 2)!=0, (invert & 4)!=0);
    AnimationPhase tmp(obj->_object);
    tmp.Validate();
    tmp.SetTime(-0.5f);    
    anm.LoadAnimationFrame(*(obj->_object),tmp,scale,(invert & 1)!=0,(invert & 2)!=0, (invert & 4)!=0);    
    obj->_object->AddAnimation(tmp);
    float timpos=0;    
    while ((error=anm.ParseNextFrame(infile,&errline))==0)
    {
      tmp.SetTime(timpos);
      anm.LoadAnimationFrame(*(obj->_object),tmp,scale,(invert & 1)!=0,(invert & 2)!=0, (invert & 4)!=0);    
      obj->_object->AddAnimation(tmp);
      timpos+=1.0f;
    }
    if (error==-12) error=0;
    obj->_object->UseAnimation(0);
  }
  if (error)
  {
      CString s;
      s.Format(IDS_MAYANIMFAILED,name.Data(),anm.GetError(error),errline);
      return RString(s);
  }
  return RString("");
}

static GameValue o2SetConfig(const GameState *gs, GameValuePar oper1)
{
  const GameArrayType& arr=oper1;
  if (arr.Size()!=2) return GameValue(false);
  if (arr[0].GetType()!=GameScalar) return GameValue(false);
  RString val;
  if (arr[1].GetType()==GameString) val=arr[1];
  else if (arr[1].GetType()==GameBool) val=((bool)arr[1]!=false)?"Yes":"";
  else val=arr[1].GetText();
  int id=toInt(arr[0]);
  if (id<CFG_VALUENAMES || id>=CFG_VALUESCOUNT+CFG_VALUENAMES) return GameValue(false);
  config->SetString(id,val,false);
  return true;
}

static GameValue o2SaveConfig(const GameState *gs)
{
  config->SaveConfig();
  frame->LoadViewerConfig();
  return GameValue();
}

static GameValue o2SetViewerAnimControler(const GameState *gs, GameValuePar val1)
{
  const GameArrayType &arr=val1;
  if (arr.Size()<2) return GameValue();

  RString name=arr[0];
  float value=arr[1];
  float minval, maxval;
  IExternalViewer::ErrorEnum err=frame->_externalViewer.SetViewerControler(name,value,&minval,&maxval);
  GameArrayType res;
  if (err!=IExternalViewer::ErrOK)
  {
    res.Append()=RString("ExternalViewerError");
    res.Append()=GameValue((float)err);
    const_cast<GameState *>(gs)->ThrowException(res);
    return GameValue();
  }
  else
  {
    res.Append()=minval;
    res.Append()=maxval;
    return res;
  }
}

/*#define OPERATORS_DEFAULT(XX, Category) \
  XX(GameBool, "dlgEnableControl",function, enableControl, GameScalar,GameBool, "controlID","enable", "Enables control in dialog. 'controlID' specified a control identifier, that can be optained by dlgGetControl function. 'enable' can be true=control is enabled or false=control is disabled", "", "", "", "",Category) \
  XX(GameBool, "dlgVisibleControl",function, showControl, GameScalar,GameBool, "controlID","visible", "Changes visibility of a control. 'controlID' specified a control identifier, that can be optained by dlgGetControl function. 'visible' can be true=control is visible or false=control is hidden", "", "", "","", Category) \*/

#define FUNCTIONS_DEFAULT(XX, Category) \
  XX(GameString, "o2GetConfig", o2GetConfig, GameScalar, "id", "Gets O2 configuration. ID is constant from std/o2config.inc", "","", "", "", Category) \
  XX(GameBool, "o2GetConfigBool", o2GetConfigBool, GameScalar, "id", "Gets O2 configuration. ID is constant from std/o2config.inc", "","", "", "", Category) \
  XX(GameScalar, "o2GetConfigScalar", o2GetConfigScalar, GameScalar, "id", "Gets O2 configuration. ID is constant from std/o2config.inc", "","", "", "", Category) \
  XX(GameBool, "o2SetConfig", o2SetConfig,GameArray,"[id,value]","Sets O2 configuration value. ID is constant from std/o2config.inc. value can be string, scalar, or bool.","","","","",Category) \
  XX(GameNothing, "o2MenuCommand", o2MenuCommand, GameScalar, "id", "Process any menu command. ID is constant from std/o2commands.inc. Remember: Changes in object by menu commands doesn't affect 'this' object. Use o2UpdateThis to update 'this'.","","","","",Category) \
  XX(GameBool, "o2EditMaterial", o2EditMaterial, GameString, "name", "Opens material 'name' in MAT editor, if available.","","","","",Category) \
  XX(GameString, "o2InputBox", o2InputBox, GameArray, "[\"content\",_browse,\"title\",\"filters\",]", "Opens o2 standard input box. 'content' is string that initialize text line. When _browse field if true, browse button is available. 'title' is displayed as title of dialog. 'filters' is optional, and is valid when _browse is true.","_file=o2InputBox [_file,true,\"Select a P3D file\",\"P3D Files|*.p3d|All files|*.*|\"];","","","",Category) \
  XX(GameString, "o2BiAnimImport", o2BiAnimImport, GameArray, "[\"filename\",_scale,_invert, _objResult]", "DEPRICATED","","","","",Category) \
  XX(GameArray, "o2SetViewerAnimControler", o2SetViewerAnimControler, GameArray, "[\"controler\",_value]", "","[minval,maxval]","","","",Category) \

#define NULARS_DEFAULT(XX, Category) \
  XX(GameNothing, "o2NoRunScriptDialog",o2NoRunScriptDialog, "Function hides \"Run Script\" dialog. User will cannot debug or stop script processing. Useful only when script has own dialog (for example, in options)", "", "", "", "", Category) \
  XX(GameNothing, "o2UpdateThisObject",o2UpdateThisObject, "Function updates 'this' object from editor. All changes made in 'this' will be discarded. Script cannot update editor's object from 'this'. It is done after script successfully exits.", "", "", "", "", Category) \
  XX(GameNothing, "o2SaveConfig",o2SaveConfig, "Saves config into registry, so all changes made in config will be restored in next session", "", "", "", "", Category) \
  


/*static GameOperator BinaryFuncts[]=
{
  OPERATORS_DEFAULT(REGISTER_OPERATOR, Category)
};*/

static GameFunction UnaryFuncts[]=
{
  FUNCTIONS_DEFAULT(REGISTER_FUNCTION, Category)
};

static GameNular NularyFuncts[]=
{
  NULARS_DEFAULT(REGISTER_NULAR, Category)
};

/*static GameData *CreateGameFor() {return new gdForClass;}

#define TYPES_DEFAULT(XX, Category) \
  XX("ForType",GameFor,CreateGameFor,"@FORTYPE","ForType",\
    "This type handles for cycles. Usage of this type: for \"_var\" from :expr: to :expr: [step &lt;expr&gt;] do {..code..};"\
    "Second usage: for [\":initPhase:\",\":condition:\",\":updatePhase:\"] do {...code...};",Category)\

static GameTypeType TypeDef[]=
{
    TYPES_DEFAULT(REGISTER_TYPE,Category)
};
*/
#if DOCUMENT_COMREF
static ComRefFunc DefaultComRefFunc[] =
{
  NULARS_DEFAULT(COMREF_NULAR, Category)
  FUNCTIONS_DEFAULT(COMREF_FUNCTION, Category)
  //OPERATORS_DEFAULT(COMREF_OPERATOR, Category)
};

/*static ComRefType DefaultComRefType[] =
{
//  TYPES_DEFAULT(COMREF_TYPE, Category)
/*  TYPES_DEFAULT_COMB(COMREF_TYPE, "Default")
};*/
#endif

void O2ScriptCommands::RegisterToGameState(GameState *gState)
{
//  gState->NewType(TypeDef[0]);
  //gState->NewOperators(BinaryFuncts,lenof(BinaryFuncts));
  gState->NewFunctions(UnaryFuncts,lenof(UnaryFuncts));
  gState->NewNularOps(NularyFuncts,lenof(NularyFuncts));
#if DOCUMENT_COMREF
  gState->AddComRefFunctions(DefaultComRefFunc,lenof(DefaultComRefFunc));
//  gState->AddComRefTypes(DefaultComRefType,lenof(DefaultComRefType));
#endif
}

