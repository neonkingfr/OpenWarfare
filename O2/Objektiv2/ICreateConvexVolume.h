#pragma once
#include "comint.h"

class CICreateConvexVolume :
  public CICommand
  {
      bool fast;
  public:
    CICreateConvexVolume(bool fast);
    ~CICreateConvexVolume(void);
    int Execute(LODObject *obj);
  };
