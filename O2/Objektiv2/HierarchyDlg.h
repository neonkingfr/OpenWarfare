#if !defined(AFX_HIERARCHYDLG_H__022A31E2_F433_4760_B76C_531DF31F706E__INCLUDED_)
#define AFX_HIERARCHYDLG_H__022A31E2_F433_4760_B76C_531DF31F706E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HierarchyDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CHierarchyDlg dialog

class CHierarchyDlg : public CDialogBar
  {
  CWnd *notify;
  CTreeCtrl htree;
  LODObject *lod;
  public:
    CDIALOGBAR_RESIZEABLE
      CHierarchyDlg();   // standard constructor
      void Create(CWnd *parent, int menuid);
    
    
    // Dialog Data
    //{{AFX_DATA(CHierarchyDlg)
    enum 
      { IDD = IDD_HIERARCHY };
    // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CHierarchyDlg)
  protected:
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CHierarchyDlg)
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnDestroy();
    afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HIERARCHYDLG_H__022A31E2_F433_4760_B76C_531DF31F706E__INCLUDED_)
