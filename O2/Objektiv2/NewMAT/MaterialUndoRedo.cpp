#include "StdAfx.h"
#include ".\materialundoredo.h"

MaterialUndoRedo::MaterialUndoRedo(void)
  {
  _undopos=0;
  _buftop=0;
  _bufbot=0;
  _empty=true;
  memset(_undobuffer,0,sizeof(_undobuffer));
  }

MaterialUndoRedo::~MaterialUndoRedo(void)
  {
  for (int i=0;i<MAXUNDOREDO;i++) delete _undobuffer[i];
  }

void MaterialUndoRedo::AllocUndoPos(char *buffer, size_t bufsize)
  {
  _undopos=_buftop;
  bool remold=!_empty && _buftop==_bufbot;
  _buftop++;
  if (_buftop>=MAXUNDOREDO) _buftop-=MAXUNDOREDO;
  if (remold) _bufbot=_buftop;
  if (_undobuffer[_undopos]) delete _undobuffer[_undopos];
  _undobuffer[_undopos]=new ArchiveStreamMemoryIn(buffer,bufsize,true);
  _empty=false;  
  }

ArchiveStreamMemoryIn &MaterialUndoRedo::Undo()
  {
  if (!CanUndo()) return *_undobuffer[_undopos];
  _undopos--;
  if (_undopos<0) _undopos+=MAXUNDOREDO;
  return *_undobuffer[_undopos]; 
  }

ArchiveStreamMemoryIn &MaterialUndoRedo::Redo()
  {
  if (!CanRedo()) return *_undobuffer[_undopos];
  _undopos++;
  if (_undopos>=MAXUNDOREDO) _undopos-=MAXUNDOREDO;
  return *_undobuffer[_undopos];
  }

void MaterialUndoRedo::ResetHistory(bool all)
  {
  if (_empty) return;
  if (all)
    {
    _empty=true;
    _undopos=_buftop=_bufbot=0;    
    }
  else
    {
    _buftop=_undopos+1;
    if (_buftop>=MAXUNDOREDO) _buftop-=MAXUNDOREDO;
    }
  }

