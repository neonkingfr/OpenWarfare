#pragma once
#include "afxwin.h"

#include "MassProcess.h"


// DlgTypeManager dialog

class DlgTypeManager : public CDialog, public MassProcess
{
	DECLARE_DYNAMIC(DlgTypeManager)

public:
	DlgTypeManager(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgTypeManager();

// Dialog Data
	enum { IDD = IDD_TYPEMANAGER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CString vLockedStages;
  CEdit wLockedStages;
  BOOL vLockAll;
  BOOL vLockAddButt;
  CString vVertexShaderName;
  CString vPixelShaderName;
  CString vTemplateName;
  CComboBox wTemplateList;
  Pathname vFullPath;
  CString vDefaultFlags;
  BOOL vOnlyFlags;
  CString vPredefinedFlags;
  BOOL vLockFlags;
  virtual BOOL OnInitDialog();
  virtual bool ProcessFile(const char *filename);
  void DialogRules();

  afx_msg void OnBnClickedLockallstages();
  afx_msg void OnCbnEditchangeCurrenttemplates();
protected:
  virtual void OnOK();
public:
  afx_msg void OnBnClickedDelete();
  afx_msg void OnCbnSelchangeCurrenttemplates();
};
