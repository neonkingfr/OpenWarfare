// DlgTabbedMat.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgTabbedMat.h"
#include ".\dlgtabbedmat.h"
#include "DlgMaterialEditor.h"
#include "ConfigPanelsCommon.h"


#define TABSIZE 16

// DlgTabbedMat dialog

IMPLEMENT_DYNAMIC(DlgTabbedMat, CDialog)
DlgTabbedMat::DlgTabbedMat(DlgMaterialEditor* pParent /*=NULL*/)
	: CDialog(DlgTabbedMat::IDD, pParent)
{
  _activePanel=NULL;
}

DlgTabbedMat::~DlgTabbedMat()
{
}

void DlgTabbedMat::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_TABS, wTabs);
}


BEGIN_MESSAGE_MAP(DlgTabbedMat, CDialog)
  ON_NOTIFY(TCN_SELCHANGE, IDC_TABS, OnTcnSelchangeTabs)
//  ON_WM_DESTROY()
ON_WM_CLOSE()
ON_WM_GETMINMAXINFO()
ON_WM_SIZE()
ON_WM_INITMENUPOPUP()
ON_COMMAND(ID_FILE_EXIT, OnFileExit)
ON_COMMAND(ID_FILE_NEW, OnFileNew)
ON_MESSAGE(MATMSG_TITLEUPDATED,OnTitleUpdated)
ON_WM_TIMER()
ON_WM_CONTEXTMENU()
ON_WM_ACTIVATE()
END_MESSAGE_MAP()


// DlgTabbedMat message handlers

void DlgTabbedMat::AddPanel(DlgMaterialEditor * panel)
{
  int index=wTabs.GetItemCount();
  wTabs.InsertItem(TCIF_PARAM,index,NULL,0,(LPARAM)panel);
  panel->ShowWindow(SW_HIDE);
  wTabs.SetCurSel(index);
  RecalcLayout();
  Activate();
}

void DlgTabbedMat::RecalcLayout(void)
{
  int sz=wTabs.GetRowCount();
  CRect itemrc;
  wTabs.GetItemRect(0,&itemrc);
  _tabSize=(itemrc.right-itemrc.left)*sz+sz;
  _panelX=_tabSize+5;
  _panelY=5;  

  int index=wTabs.GetCurSel();
  TCITEM itinfo;
  itinfo.mask=TCIF_PARAM;
  wTabs.GetItem(index,&itinfo);
  DlgMaterialEditor *panel=(DlgMaterialEditor *)itinfo.lParam;
  if (_activePanel!=panel)
  {
    if (_activePanel) _activePanel->ShowWindow(SW_HIDE);
    _activePanel=panel;
    _activePanel->ShowWindow(SW_SHOW);
  }
  CRect mypos;
  GetWindowRect(&mypos);    //moje aktualni poloha
  MoveWindow(&mypos);   //zkus ji aktualizovat (MINMAXINFO in effect);
  
  CRect client;
  GetClientRect(&client);  //jaka je vysledna klientska velikost?

  if (_activePanel) _activePanel->SetWindowPos(NULL,_panelX,_panelY,client.right-_panelX,client.bottom-_panelY,SWP_NOZORDER);
  wTabs.SetWindowPos(NULL,0,_panelY,_tabSize,client.bottom-_panelY,SWP_NOZORDER);   
  if (sz==0) wTabs.ShowWindow(SW_HIDE);else wTabs.ShowWindow(SW_SHOW);
}

void DlgTabbedMat::OnTcnSelchangeTabs(NMHDR *pNMHDR, LRESULT *pResult)
{
  *pResult = 0;
  RecalcLayout();
  if (_activePanel) _activePanel->CheckAndUpdateChanges();

}


DlgMaterialEditor *DlgTabbedMat::GetActive()
{
  return _activePanel;
}

void DlgTabbedMat::SetActive(DlgMaterialEditor *panel)
{
  if (GetSafeHwnd()==NULL) return;
  int sz=wTabs.GetItemCount();
  for (int i=0;i<sz;i++)
  {
    TCITEM itinfo;
    itinfo.mask=TCIF_PARAM;
    wTabs.GetItem(i,&itinfo);
    DlgMaterialEditor *p=(DlgMaterialEditor *)itinfo.lParam;
    if (p==panel)
    {
      wTabs.SetCurSel(i);
      return;
    }
  }
 AddPanel(panel);
}

DlgMaterialEditor *DlgTabbedMat::EnumPanels(unsigned int &position)
{
  if (GetSafeHwnd()==NULL) return NULL;
  int sz=wTabs.GetItemCount();
  if (position>=sz) return NULL;
  TCITEM itinfo;
  itinfo.mask=TCIF_PARAM;
  wTabs.GetItem(position,&itinfo);
  DlgMaterialEditor *p=(DlgMaterialEditor *)itinfo.lParam;
  ++position;
  return p;
}

void DlgTabbedMat::Activate()
{
  if (GetSafeHwnd()==NULL) return;
  if (IsIconic()) OpenIcon();
  if (!IsWindowVisible()) ShowWindow(SW_SHOW);
  BringWindowToTop();
  SetWindowPos(&wndTopMost,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
  SetTimer(1234,500,NULL);
}

bool DlgTabbedMat::BeforeClose()
{
  if (GetSafeHwnd()==NULL) return true;
  if (!IsWindowEnabled()) 
  {
    MessageBox(StrRes[IDS_MATBUSY],StrRes[IDS_MATNAME],MB_OK|MB_ICONSTOP);
    return false;
  }
  unsigned int i;
  DlgMaterialEditor *dlg;
  for (i=0;dlg=EnumPanels(i);)
  {
    if (dlg->AskExitMAT()==false) return false;
    dlg->DestroyWindow();
    delete dlg;
    wTabs.DeleteItem(--i);
    if (dlg==_activePanel) _activePanel=NULL;
  }
  return true;
}



void DlgTabbedMat::OnClose()
{
  SaveWindowPosition();
  if (BeforeClose())
  {
    DestroyWindow();
    theApp.CallMatClient().MatWindowClosed();
  }

}

BOOL DlgTabbedMat::PreTranslateMessage(MSG* pMsg)
{
  if (GetSafeHwnd()==NULL) return FALSE;
   DlgMaterialEditor *active=GetActive();
  if (active && active->PreTranslateMessage(pMsg)) return TRUE;

  return CDialog::PreTranslateMessage(pMsg);
}

void DlgTabbedMat::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
  if (wTabs.GetSafeHwnd() && GetActive())
  {
    GetActive()->SendMessage(WM_GETMINMAXINFO,0,(LPARAM)lpMMI);
    CRect rcmin(0,0,lpMMI->ptMinTrackSize.x,lpMMI->ptMinTrackSize.y),
          rcmax(0,0,lpMMI->ptMaxTrackSize.x,lpMMI->ptMaxTrackSize.y);
    rcmin.right+=_panelX;
    rcmax.right+=_panelX;
    AdjustWindowRectEx(&rcmin,GetWindowLong(*this,GWL_STYLE),TRUE,GetWindowLong(*this,GWL_EXSTYLE));
    AdjustWindowRectEx(&rcmax,GetWindowLong(*this,GWL_STYLE),TRUE,GetWindowLong(*this,GWL_EXSTYLE));
    lpMMI->ptMinTrackSize.x=rcmin.right-rcmin.left;
    lpMMI->ptMaxTrackSize.x=rcmax.right-rcmax.left;
  }
  CDialog::OnGetMinMaxInfo(lpMMI);
}

void DlgTabbedMat::OnSize(UINT nType, int cx, int cy)
{
  if (wTabs.GetSafeHwnd()) RecalcLayout();
  CDialog::OnSize(nType, cx, cy);

  // TODO: Add your message handler code here
}

void DlgTabbedMat::CloseCurrentTab()
{
  if (_activePanel)
  {
    if (_activePanel->AskExitMAT())
    {
      int index=wTabs.GetCurSel();
      wTabs.DeleteItem(index);
      _activePanel->DestroyWindow();
      delete _activePanel;
      _activePanel=NULL;
      wTabs.SetCurSel(0);
      RecalcLayout();
    }    
  }
}


void DlgTabbedMat::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu)
{
 if (_activePanel)
   _activePanel->OnInitMenuPopup(pPopupMenu,nIndex,bSysMenu);
 else if (!bSysMenu)
 {
   for (int i=0;i<pPopupMenu->GetMenuItemCount();i++)
   {
     int id=pPopupMenu->GetMenuItemID(i);     
     if (pPopupMenu->GetSubMenu(i)==NULL)
       pPopupMenu->EnableMenuItem(i,((id==ID_FILE_EXIT || id==ID_FILE_NEW)?MF_ENABLED:MF_GRAYED)|MF_BYPOSITION);
   }
 }
}

BOOL DlgTabbedMat::OnCommand(WPARAM wParam, LPARAM lParam)
{
  if (_activePanel) 
    return _activePanel->SendMessage(WM_COMMAND,wParam,lParam);

  return CDialog::OnCommand(wParam, lParam);
}

void DlgTabbedMat::OnFileExit()
{
  theApp.OnExitMatMenu();
}

void DlgTabbedMat::OnFileNew()
{
  const char *fname=NULL;  
  DlgMaterialEditor *dlg;
  for (unsigned int i;(dlg=EnumPanels(i))!=NULL && fname==NULL;) fname=dlg->GetFilename();
  if (fname)
  {
    Pathname p=fname;
    theApp.CreateMaterial(p.GetDirectory());
  }
  else
    theApp.CreateNewWindow();
}


LRESULT DlgTabbedMat::OnTitleUpdated(WPARAM wParam,LPARAM lParam)
{
  DlgMaterialEditor *dlg=(DlgMaterialEditor *)lParam;
  DlgMaterialEditor *fnd;
  for (unsigned int i=0;(fnd=EnumPanels(i))!=NULL;)
    if (fnd==dlg)
    {
      char buff[256];
      dlg->GetWindowText(buff,sizeof(buff));
      TCITEM itemText;
      itemText.mask=TCIF_TEXT|TCIF_IMAGE;
      itemText.iImage=wParam;
      itemText.pszText=buff;
      wTabs.SetItem(i-1,&itemText);
    }
    return 0;
}
BOOL DlgTabbedMat::OnInitDialog()
{
  LoadWindowPosition();
  __super::OnInitDialog();
  if (_images.GetSafeHandle()==0)
      _images.Create(IDB_TABIMAGES,12,0,RGB(255,0,255));  
  wTabs.SetImageList(&_images);
  wTabs.SetExtendedStyle(TCS_EX_FLATSEPARATORS);
  return TRUE;
}


#define WINDOW_SECTION "Window"


void DlgTabbedMat::LoadWindowPosition()
{
  CRect rc;
  rc.left=theApp.GetProfileInt(WINDOW_SECTION,"left",0);
  rc.top=theApp.GetProfileInt(WINDOW_SECTION,"top",0);
  rc.right=theApp.GetProfileInt(WINDOW_SECTION,"right",0);
  rc.bottom=theApp.GetProfileInt(WINDOW_SECTION,"bottom",0);
  if (rc.IsRectNull()) return;
  HMONITOR mon=::MonitorFromRect(&rc,MONITOR_DEFAULTTONULL);
  if (mon==NULL) return;
  MoveWindow(&rc,FALSE);
}

void DlgTabbedMat::SaveWindowPosition()
{
  CRect rc;
  GetWindowRect(&rc);
  theApp.WriteProfileInt(WINDOW_SECTION,"left",rc.left);
  theApp.WriteProfileInt(WINDOW_SECTION,"top",rc.top);
  theApp.WriteProfileInt(WINDOW_SECTION,"right",rc.right);
  theApp.WriteProfileInt(WINDOW_SECTION,"bottom",rc.bottom);
}

void DlgTabbedMat::OnTimer(UINT nIDEvent)
{
  if (nIDEvent==1234)
  {
    SetWindowPos(&wndNoTopMost,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
    KillTimer(nIDEvent);
  }

  CDialog::OnTimer(nIDEvent);
}

void DlgTabbedMat::OnContextMenu(CWnd* pWnd, CPoint point)
{
  if (pWnd==&wTabs)
  {
    CMenu mnu;
    mnu.LoadMenu(IDR_TABPOPUP);
    CPoint itpt=point;
    pWnd->ScreenToClient(&itpt);
    TCHITTESTINFO ht;
    ht.pt=itpt;
    int index=wTabs.HitTest(&ht);
    if (index!=-1) 
    {
      wTabs.SetCurSel(index);
      RecalcLayout();        
      CMenu *popup=mnu.GetSubMenu(0);
      popup->TrackPopupMenu(TPM_RIGHTBUTTON,point.x,point.y,this,NULL);
    }
  }
  else
    __super::OnContextMenu(pWnd,point);
}

void DlgTabbedMat::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
  CDialog::OnActivate(nState, pWndOther, bMinimized);
  if (nState==WA_ACTIVE||nState==WA_CLICKACTIVE && _activePanel)
    _activePanel->CheckAndUpdateChanges();

  // TODO: Add your message handler code here
}
