// DlgEditMatrix.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgEditMatrix.h"
#include ".\dlgeditmatrix.h"


// DlgEditMatrix dialog

IMPLEMENT_DYNAMIC(DlgEditMatrix, CDialog)
DlgEditMatrix::DlgEditMatrix(CWnd* pParent /*=NULL*/)
	: CDialog(DlgEditMatrix::IDD, pParent)
{
}

DlgEditMatrix::~DlgEditMatrix()
{
}

void DlgEditMatrix::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(DlgEditMatrix, CDialog)
END_MESSAGE_MAP()


// DlgEditMatrix message handlers

void DlgEditMatrix::SetItemFloat(int idc, float value)
  {
  char buff[50];
  sprintf(buff,"%.3f",value);
  SetDlgItemText(idc,buff);
  }

float DlgEditMatrix::GetItemFloat(int idc)
  {
  char buff[50];
  GetDlgItemText(idc,buff,50);
  return (float)atof(buff);
  }

BOOL DlgEditMatrix::OnInitDialog()
  {
  CDialog::OnInitDialog();

  SetItemFloat(IDC_EDIT_AS0,vMatrix.x.x);
  SetItemFloat(IDC_EDIT_AS1,vMatrix.x.y);
  SetItemFloat(IDC_EDIT_AS2,vMatrix.x.z);
  SetItemFloat(IDC_EDIT_UP0,vMatrix.y.x);
  SetItemFloat(IDC_EDIT_UP1,vMatrix.y.y);
  SetItemFloat(IDC_EDIT_UP2,vMatrix.y.z);
  SetItemFloat(IDC_EDIT_DI0,vMatrix.z.x);
  SetItemFloat(IDC_EDIT_DI1,vMatrix.z.y);
  SetItemFloat(IDC_EDIT_DI2,vMatrix.z.z);
  SetItemFloat(IDC_EDIT_PS0,vMatrix.pos.x);
  SetItemFloat(IDC_EDIT_PS1,vMatrix.pos.y);
  SetItemFloat(IDC_EDIT_PS2,vMatrix.pos.z);
  return true;
  }

void DlgEditMatrix::OnOK()
  {
  vMatrix.x.x=GetItemFloat(IDC_EDIT_AS0);
  vMatrix.x.y=GetItemFloat(IDC_EDIT_AS1);
  vMatrix.x.z=GetItemFloat(IDC_EDIT_AS2);
  vMatrix.y.x=GetItemFloat(IDC_EDIT_UP0);
  vMatrix.y.y=GetItemFloat(IDC_EDIT_UP1);
  vMatrix.y.z=GetItemFloat(IDC_EDIT_UP2);
  vMatrix.z.x=GetItemFloat(IDC_EDIT_DI0);
  vMatrix.z.y=GetItemFloat(IDC_EDIT_DI1);
  vMatrix.z.z=GetItemFloat(IDC_EDIT_DI2);
  vMatrix.pos.x=GetItemFloat(IDC_EDIT_PS0);
  vMatrix.pos.y=GetItemFloat(IDC_EDIT_PS1);
  vMatrix.pos.z=GetItemFloat(IDC_EDIT_PS2);

  CDialog::OnOK();
  }
