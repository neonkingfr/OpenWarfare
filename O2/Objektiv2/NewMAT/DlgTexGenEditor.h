#pragma once


#define TEXGEN_MAXIMUMWINDOWS 8
// CDlgTexGenEditor dialog

class CDlgTexGenEditor : public CDialog
{
	DECLARE_DYNAMIC(CDlgTexGenEditor)

  CWnd *_panels[TEXGEN_MAXIMUMWINDOWS];

  int _nTexGen;
  int _sizeFromTop;
public:

    int CreatePanel(CDialog * panel);
    virtual BOOL OnInitDialog();
    int AddTexGen(ParamEntryPtr texGenData=NULL,int number=-1);
    void Serialize(IArchive &arch);
    void DeleteAllTexGens();
    bool DeletePanel(int index);
    bool DeletePanel(CDialog * panel);
    int SaveTexGens(ParamFile &pf);


	CDlgTexGenEditor(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgTexGenEditor();

// Dialog Data
	enum { IDD = IDD_TEXGENEDITORDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()
public:
//  afx_msg void OnClose();
};
