// DlgSpecularCfg.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgSpecularCfg.h"
#include ".\dlgspecularcfg.h"
#include <math.h>


#define MAX_FACTOR 100.0f
// DlgSpecularCfg dialog

IMPLEMENT_DYNAMIC(DlgSpecularCfg, CDialog)
DlgSpecularCfg::DlgSpecularCfg(CWnd* pParent /*=NULL*/)
	: CDialog(DlgSpecularCfg::IDD, pParent)
{
_diffuse=0;
_specular=RGB(255,255,255);
_factor=10.0f;
_stepMode=false;
}

DlgSpecularCfg::~DlgSpecularCfg()
{
}

void DlgSpecularCfg::DoDataExchange(CDataExchange* pDX)
{
CDialog::DoDataExchange(pDX);
DDX_Control(pDX, IDC_SPECULARVAL, wSpecularValue);
DDX_Control(pDX, IDC_SPECULARSCROLL, wSpecularScroll);
DDX_Control(pDX, IDC_SPECULARVIEW, wSpecularView);
DDX_Control(pDX, IDC_SPECULARVIEW2, wSpecularGraph);
}


BEGIN_MESSAGE_MAP(DlgSpecularCfg, CDialog)
  ON_WM_DRAWITEM()
  ON_EN_CHANGE(IDC_SPECULARVAL, OnEnChangeSpecularval)
  ON_WM_HSCROLL()
END_MESSAGE_MAP()


// DlgSpecularCfg message handlers

BOOL DlgSpecularCfg::OnInitDialog()
  {
  CDialog::OnInitDialog();
  
  wSpecularScroll.SetScrollRange(25.0f,toLargeInt(sqrt(MAX_FACTOR)*25.0f));
  LoadValues();

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

void DlgSpecularCfg::LoadValues(void)
  {
  char buff[50];
  sprintf(buff,"%.1f",_factor);
  wSpecularValue.SetWindowText(buff);
  wSpecularValue.UpdateWindow();
  wSpecularScroll.SetScrollPos(toLargeInt(sqrt(_factor)*25.0f));
  UpdateGraphs();
  }

void DlgSpecularCfg::RecalculateView()
  {
  if (_specView.GetSafeHandle()!=NULL) _specView.DeleteObject();
  CRect rc;
  wSpecularView.GetClientRect(&rc);
  CDC *dc=wSpecularView.GetDC();
  CDC bmpdc;
  bmpdc.CreateCompatibleDC(dc);
  _specView.CreateCompatibleBitmap(dc,rc.right,rc.bottom);
  wSpecularView.ReleaseDC(dc);
  CBitmap *oldbmp=bmpdc.SelectObject(&_specView);
  COLORREF bgr=GetSysColor(COLOR_BTNFACE);
  int centx=rc.right/2;
  int centy=rc.bottom/2;
  for (int y=0;y<rc.bottom;y++)
    for(int x=0;x<rc.right;x++)
      {
      float xr=(float)fabs(1.0f-(float)x/(float)centx);
      float yr=(float)fabs(1.0f-(float)y/(float)centy);
      float r=sqrt(xr*xr+yr*yr);
      if (r<1.0f)
        {
        float sr=1-r*r;
        float ints=pow(sr,_factor);
        float iints=1.0f-ints;
        COLORREF col=
          RGB(
            __min(toLargeInt(GetRValue(_diffuse)+GetRValue(_specular)*ints),255.0f),
            __min(toLargeInt(GetGValue(_diffuse)+GetGValue(_specular)*ints),255.0f),
            __min(toLargeInt(GetBValue(_diffuse)+GetBValue(_specular)*ints),255.0f)
            );
        bmpdc.SetPixel(x,y,col);
        }
      else
        bmpdc.SetPixel(x,y,bgr);
      }
  bmpdc.SelectObject(oldbmp);
  bmpdc.DeleteDC();  
  }

void DlgSpecularCfg::RecalculateGraph()
  {
  if (_specGraph.GetSafeHandle()!=NULL) _specGraph.DeleteObject();
  CRect rc;
  wSpecularGraph.GetClientRect(&rc);
  CDC *dc=wSpecularGraph.GetDC();
  CDC bmpdc;
  bmpdc.CreateCompatibleDC(dc);
  _specGraph.CreateCompatibleBitmap(dc,rc.right,rc.bottom);
  wSpecularGraph.ReleaseDC(dc);
  CBitmap *oldbmp=bmpdc.SelectObject(&_specGraph);
  COLORREF bgr=GetSysColor(COLOR_WINDOW);
  COLORREF frg=GetSysColor(COLOR_WINDOWTEXT);
  CPen linepen,*oldpen;
  linepen.CreatePen(PS_SOLID,1,frg);  
  oldpen=bmpdc.SelectObject(&linepen);
  bmpdc.FillSolidRect(&rc,bgr);
  int centx=rc.right/2;
  for(int x=0;x<rc.right;x++)
      {
      float xr=(float)fabs(1.0f-(float)x/(float)centx);
      float ints=pow(1.0f-xr*xr,_factor);
      float intr1=rc.bottom*ints;
      float intr2=floor(intr1);
      ints=intr1-intr2;;
      float iints=1.0f-ints;
      int y=rc.bottom-toLargeInt(intr2);
      bmpdc.MoveTo(x,rc.bottom);
      bmpdc.LineTo(x,y);
        COLORREF col=
          RGB(
            toLargeInt(GetRValue(bgr)*(iints)+GetRValue(frg)*ints),
            toLargeInt(GetGValue(bgr)*(iints)+GetGValue(frg)*ints),
            toLargeInt(GetBValue(bgr)*(iints)+GetBValue(frg)*ints)
            );
      bmpdc.SetPixel(x,y,col);
      }
  bmpdc.SelectObject(oldbmp);
  bmpdc.SelectObject(oldpen);
  bmpdc.DeleteDC();  
  }

void DlgSpecularCfg::UpdateGraphs()
  {
  RecalculateView();
  RecalculateGraph();
  wSpecularView.Invalidate(FALSE);
  wSpecularGraph.Invalidate(FALSE);
  }

void DlgSpecularCfg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
  {
  if (nIDCtl==IDC_SPECULARVIEW || nIDCtl==IDC_SPECULARVIEW2)
    {        
    CDC bmpdc;
    CBitmap *bmpview;
    if (nIDCtl==IDC_SPECULARVIEW2) 
      {      
      bmpview=&_specGraph;
      if (bmpview->GetSafeHandle()==NULL) RecalculateGraph();
      }
    else 
      {
      bmpview=&_specView;
      if (bmpview->GetSafeHandle()==NULL) RecalculateView();
      }
    bmpdc.CreateCompatibleDC(CDC::FromHandle(lpDrawItemStruct->hDC));
    CBitmap *oldbmp=bmpdc.SelectObject(bmpview);
    BitBlt(lpDrawItemStruct->hDC,lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.top,
      lpDrawItemStruct->rcItem.right-lpDrawItemStruct->rcItem.left,
      lpDrawItemStruct->rcItem.bottom-lpDrawItemStruct->rcItem.top,
      bmpdc,0,0,SRCCOPY);
    bmpdc.SelectObject(oldbmp);
    bmpdc.DeleteDC();
    return;
    }
  CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
  }

void DlgSpecularCfg::OnEnChangeSpecularval()
  {
  char buff[50];
  wSpecularValue.GetWindowText(buff,50);
  _factor=(float)atof(buff);
  wSpecularScroll.SetScrollPos(toLargeInt(sqrt(_factor)*25.0f));  
  GetParent()->PostMessage(MATMSG_SPECULARCHANGED);
  }

static float skalovani[]=
  {2,4,6,8,10,12,14,16,20,24,28,32,40,48,56,64,96};
static int skalcount=sizeof(skalovani)/(sizeof(*skalovani));
static int GetSkalovaniPos(float p)
  {
  int best=0;
  float max=1000.0f;
  for (int i=0;i<skalcount;i++)
    {
    float val=(float)fabs(skalovani[i]-p);
    if (val<max)
      {best=i;max=val;}
    }
  return best;
  }

static float GetSkalovaniVal(int p)
  {
  if (p<0) return skalovani[0];
  if (p>=skalcount) return skalovani[skalcount-1];
  return skalovani[p];
  }

void DlgSpecularCfg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
  {
  if (pScrollBar==&wSpecularScroll)
    {
    switch (nSBCode)
      {
      case SB_LEFT: _factor=0;break;
      case SB_RIGHT: _factor=MAX_FACTOR;break;
      case SB_LINELEFT: 
          if (_stepMode)
            _factor=GetSkalovaniVal(GetSkalovaniPos(_factor)-1);
          else
            _factor-=0.1f;
          if (_factor<0.0f) _factor=0.0f;
          break;
      case SB_LINERIGHT: 
          if (_stepMode)
            _factor=GetSkalovaniVal(GetSkalovaniPos(_factor)+1);
          else
          _factor+=0.1f;
          if (_factor>MAX_FACTOR) _factor=MAX_FACTOR;
          break;
      case SB_PAGELEFT: 
          if (_stepMode)
            _factor=GetSkalovaniVal(GetSkalovaniPos(_factor)-5);
          else
            _factor-=1.0f;
          if (_factor<0.0f) _factor=0.0f;
          break;
      case SB_PAGERIGHT: 
          if (_stepMode)
            _factor=GetSkalovaniVal(GetSkalovaniPos(_factor)+5);
          else
          _factor+=1.0f;
          if (_factor>MAX_FACTOR) _factor=MAX_FACTOR;
          break;
      case SB_THUMBPOSITION:
      case SB_THUMBTRACK:          
          _factor=nPos/25.0f;
          _factor*=_factor;
          if (_stepMode) _factor=GetSkalovaniVal(GetSkalovaniPos(_factor));
          break;
      }

    }
  LoadValues();
  }

void DlgSpecularCfg::Serialize(IArchive &arch)
  {
  arch(_factor);
  if (-arch) LoadValues();
  }
