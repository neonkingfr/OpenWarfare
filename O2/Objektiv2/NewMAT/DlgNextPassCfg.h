#pragma once
#include "afxwin.h"


// DlgNextPassCfg dialog

class DlgNextPassCfg : public CDialog
{
	DECLARE_DYNAMIC(DlgNextPassCfg)

public:
	DlgNextPassCfg(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgNextPassCfg();

// Dialog Data
	enum { IDD = IDD_NEXTPASSCFG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CEdit wMatName;
  afx_msg void OnBnClickedBrowse();
  afx_msg void OnBnClickedEdit();

  void Serialize(IArchive &arch);  
  afx_msg void OnEnChangeName();
};
