// DlgAskCheckIn.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgAskCheckIn.h"
#include ".\dlgaskcheckin.h"


// DlgAskCheckIn dialog

IMPLEMENT_DYNAMIC(DlgAskCheckIn, CDialog)
DlgAskCheckIn::DlgAskCheckIn(CWnd* pParent /*=NULL*/)
	: CDialog(DlgAskCheckIn::IDD, pParent)
    , vName(_T(""))
{
}

DlgAskCheckIn::~DlgAskCheckIn()
{
}

void DlgAskCheckIn::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_NAME, vName);
}


BEGIN_MESSAGE_MAP(DlgAskCheckIn, CDialog)
  ON_BN_CLICKED(IDC_CHECKIN, OnBnClickedCheckin)
  ON_BN_CLICKED(IDC_UNDOCHECKOUT, OnBnClickedUndocheckout)
END_MESSAGE_MAP()


// DlgAskCheckIn message handlers

void DlgAskCheckIn::OnBnClickedCheckin()
{
    EndDialog(IDC_CHECKIN);
}

void DlgAskCheckIn::OnBnClickedUndocheckout()
{
    EndDialog(IDC_UNDOCHECKOUT);
}
