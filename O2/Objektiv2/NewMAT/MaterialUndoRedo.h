#ifndef __MATERIAL_UNDO_REDO_H_
#define __MATERIAL_UNDO_REDO_H_


#include "../../bredy.libs/Archive/ArchiveStreamMemory.h"

using namespace std;


#define MAXUNDOREDO 50

class MaterialUndoRedo
  {
  ArchiveStreamMemoryIn *_undobuffer[MAXUNDOREDO];
  int _undopos;
  int _buftop;
  int _bufbot;
  bool _empty;
  public:
    MaterialUndoRedo(void);
    ~MaterialUndoRedo(void);

    void AllocUndoPos(char *buffer, size_t bufsize);
    ArchiveStreamMemoryIn &Undo();
    ArchiveStreamMemoryIn &Redo();
    void ResetHistory(bool all=false);
    bool CanUndo() {return _undopos!=_bufbot;}
    bool CanRedo() {int p=_undopos+1;if (p>MAXUNDOREDO) p-=MAXUNDOREDO; return p!=_buftop && !_empty;}
    void SaveUndo(char *buffer, size_t bufsize)
      {
      ResetHistory();
      return AllocUndoPos(buffer,bufsize);
      }
  };

#endif