#pragma once


// DlgFresnelFce dialog

class DlgFresnelFce : public CDialog
{
	DECLARE_DYNAMIC(DlgFresnelFce)

public:
  CString _frnString;
  CString _format;
    int _width;
    int _height;
    int _levels;

  float _vN,_vK;

	DlgFresnelFce(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgFresnelFce();

// Dialog Data
	enum { IDD = IDD_FRESNELDLG };
  virtual BOOL OnInitDialog();
  void ParseFrnString(void);

  CComboBox wFormat;
  CComboBox wWidth;
  CComboBox wHeight;
  CComboBox wLevels;
  CSliderCtrl sliderN,sliderK;
  CEdit editN,editK;
  CButton wSpecularGraph;

  CBitmap _specGraph;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual void OnOK();
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
  void RecalculateGraph();
  void UpdateGraphs();
  afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
};
