// DlgProceduralTex.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgProceduralTex.h"
#include ".\dlgproceduraltex.h"


// DlgProceduralTex dialog

IMPLEMENT_DYNAMIC(DlgProceduralTex, CDialog)
DlgProceduralTex::DlgProceduralTex(CWnd* pParent /*=NULL*/)
	: CDialog(DlgProceduralTex::IDD, pParent)
    , vRed(0)
    , vGreen(0)
    , vBlue(0)
    , vAlpha(1)
    , vAlphamode(0)
    , vType("CO")
  {
}

DlgProceduralTex::~DlgProceduralTex()
{
}

void DlgProceduralTex::DoDataExchange(CDataExchange* pDX)
{
CDialog::DoDataExchange(pDX);
DDX_Text(pDX, IDC_COLORVAL1, vRed);
DDX_Text(pDX, IDC_COLORVAL2, vGreen);
DDX_Text(pDX, IDC_COLORVAL3, vBlue);
DDX_Text(pDX, IDC_COLORVAL4, vAlpha);
DDX_Text(pDX, IDC_TYPE, vType);
DDX_Radio(pDX, IDC_ALPHAMODE, vAlphamode);
}


BEGIN_MESSAGE_MAP(DlgProceduralTex, CDialog)
  ON_BN_CLICKED(IDC_PRESET1, OnBnClickedPreset1)
  ON_BN_CLICKED(IDC_PRESET3, OnBnClickedPreset3)
  ON_BN_CLICKED(IDC_PRESET4, OnBnClickedPreset4)
  ON_BN_CLICKED(IDC_PRESET5, OnBnClickedPreset5)
  ON_BN_CLICKED(IDC_ALPHAMODE, OnBnClickedAlphamode)
  ON_BN_CLICKED(IDC_RADIO2, OnBnClickedRadio2)
  ON_BN_CLICKED(IDC_RADIO3, OnBnClickedRadio3)
  ON_BN_CLICKED(IDC_PRESET7, OnBnClickedPreset7)
  ON_BN_CLICKED(IDC_PRESET6, OnBnClickedPreset6)
  ON_BN_CLICKED(IDC_BUTTON2, OnBnClickedPreset8)
  ON_BN_CLICKED(IDC_PRESET9, OnBnClickedPreset9)
END_MESSAGE_MAP()


// DlgProceduralTex message handlers

void DlgProceduralTex::OnBnClickedPreset1()
  {
  vRed=vGreen=vBlue=0;  
  UpdateData(FALSE);
  }


void DlgProceduralTex::OnBnClickedPreset3()
  {
  vRed=vGreen=vBlue=1.0f;  
  UpdateData(FALSE);
  }

void DlgProceduralTex::OnBnClickedPreset4()
  {
  vRed=vGreen=0.5f;
  vAlpha=vBlue=1.0f;
  vType="NOHQ";
  UpdateData(FALSE);
  }

void DlgProceduralTex::OnBnClickedPreset5()
  {
  vRed=vGreen=vBlue=0.5f;
  vType="DT";
  UpdateData(FALSE);
  }

void DlgProceduralTex::DialogRules()
  {  
  CDataExchange dlgex(this,true);
  int val;
  DDX_Radio(&dlgex,IDC_ALPHAMODE,val);
  BOOL encolor=val!=1;
  BOOL enalpha=val!=2;
  GetDlgItem(IDC_COLORVAL1)->EnableWindow(encolor);
  GetDlgItem(IDC_COLORVAL2)->EnableWindow(encolor);
  GetDlgItem(IDC_COLORVAL3)->EnableWindow(encolor);
  GetDlgItem(IDC_COLORVAL4)->EnableWindow(enalpha);
  }
void DlgProceduralTex::OnOK()
  {
  CDialog::OnOK();
  if (vType.GetLength()==0) 
  {
    MessageBeep(MB_ICONEXCLAMATION);
    return;
  }
  const char *format;
  switch (vAlphamode)
    {
    case 0:format="#(argb,8,8,3)color(%g,%g,%g,%g,%s)";break;
    case 2:format="#(rgb,8,8,3)color(%g,%g,%g,%g,%s)";break;
    case 1:format="#(a,8,8,3)color(%g,%g,%g,%g,%s)";break;
    }
  vCode.Format(format,vRed,vGreen,vBlue,vAlpha,vType.GetString());
  }

BOOL DlgProceduralTex::OnInitDialog()
  {
  char buff[50];
  char buff2[50];
  int x=8,y=8,lods=3;
  int cnt=sscanf(vCode,"#(%49[^,],%d,%d,%d)color(%f,%f,%f,%f,%49[^)])",
    buff,&x,&y,&lods,&vRed,&vGreen,&vBlue,&vAlpha,buff2);
  
  if (_stricmp(buff,"argb")==0 || _stricmp(buff,"ai")==0 ) vAlphamode=0;
  if (_stricmp(buff,"rgb")==0 || _stricmp(buff,"i")==0) vAlphamode=2;
  if (_stricmp(buff,"a")==0) vAlphamode=1;

  if (cnt>8) vType=buff2;
  

  CDialog::OnInitDialog();
  DialogRules();
  

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

void DlgProceduralTex::OnBnClickedAlphamode()
  {
  DialogRules();
  }

void DlgProceduralTex::OnBnClickedRadio2()
  {
  DialogRules();
  }

void DlgProceduralTex::OnBnClickedRadio3()
  {
  DialogRules();
  }

void DlgProceduralTex::OnBnClickedPreset7()
{
  vRed=vGreen=vBlue=vAlpha=0;
  vType="MC";
  UpdateData(FALSE);
}

void DlgProceduralTex::OnBnClickedPreset6()
{
  vRed=vGreen=vBlue=vAlpha=1.0f;
  vType="AS";
  UpdateData(FALSE);
}

void DlgProceduralTex::OnBnClickedPreset8()
{
  vRed=0.5f;
  vGreen=0;
  vBlue=vAlpha=1.0f;
  vType="DTSMDI";
  UpdateData(FALSE);
}

void DlgProceduralTex::OnBnClickedPreset9()
{
  vRed=0.0f;
  vGreen=0.0f;
  vBlue=1.0f;
  vAlpha=1.0f;
  vType="SMDI";
  UpdateData(FALSE);
  // TODO: Add your control notification handler code here
}
