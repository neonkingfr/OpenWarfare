// DlgNextPassCfg.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgNextPassCfg.h"
#include ".\dlgnextpasscfg.h"
#include "ArchiveExt.h"
#include "DlgMaterialEditor.h"
#include "ConfigPanelsCommon.h"


// DlgNextPassCfg dialog

IMPLEMENT_DYNAMIC(DlgNextPassCfg, CDialog)
DlgNextPassCfg::DlgNextPassCfg(CWnd* pParent /*=NULL*/)
	: CDialog(DlgNextPassCfg::IDD, pParent)
{
}

DlgNextPassCfg::~DlgNextPassCfg()
{
}

void DlgNextPassCfg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_NAME, wMatName);
}


BEGIN_MESSAGE_MAP(DlgNextPassCfg, CDialog)
  ON_BN_CLICKED(IDC_BROWSE, OnBnClickedBrowse)
  ON_BN_CLICKED(IDC_EDIT, OnBnClickedEdit)
  ON_EN_CHANGE(IDC_NAME, OnEnChangeName)
END_MESSAGE_MAP()


// DlgNextPassCfg message handlers

void DlgNextPassCfg::OnBnClickedBrowse()
{
  CString cur;
  wMatName.GetWindowText(cur);
  CFileDialog fdlg(FALSE,StrRes[IDS_RVMATDEFEXT],cur,OFN_HIDEREADONLY|OFN_FILEMUSTEXIST,StrRes[IDS_RVMATSAVEFILTERS]);
  if (fdlg.DoModal()==IDOK)
  {
    cur=fdlg.GetPathName();
    wMatName.SetWindowText(cur);
  }
  GetParent()->SendMessage(MATMSG_CONFIG_CHANGES,0,0);
}

void DlgNextPassCfg::OnBnClickedEdit()
{
  CString cur;
  wMatName.GetWindowText(cur);
  theApp.CreateNewWindow();
  theApp.EditMaterial(cur);
}


void DlgNextPassCfg::Serialize(IArchive &arch)
{
  CString cur;
  if (+arch) wMatName.GetWindowText(cur);  
  arch("NextPass",cur);
  if (-arch) wMatName.SetWindowText(cur);

}

void DlgNextPassCfg::OnEnChangeName()
{
GetParent()->SendMessage(MATMSG_CONFIG_CHANGES,0,0);
}
