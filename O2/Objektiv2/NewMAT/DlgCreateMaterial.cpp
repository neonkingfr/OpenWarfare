// DlgCreateMaterial.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgCreateMaterial.h"
#include ".\dlgcreatematerial.h"


// DlgCreateMaterial dialog

IMPLEMENT_DYNAMIC(DlgCreateMaterial, CDialog)
DlgCreateMaterial::DlgCreateMaterial(CWnd* pParent /*=NULL*/)
	: CDialog(DlgCreateMaterial::IDD, pParent)
    , vMatName(_T(""))
    , vTemplateName(_T(""))
    , vFolder(_T(""))
  {
}

DlgCreateMaterial::~DlgCreateMaterial()
{
}

void DlgCreateMaterial::DoDataExchange(CDataExchange* pDX)
{
CDialog::DoDataExchange(pDX);
DDX_Text(pDX, IDC_MATNAME, vMatName);
DDX_CBString(pDX, IDC_USETEMPLATES, vTemplateName);
DDX_Control(pDX, IDC_USETEMPLATES, wTemplateName);
DDX_Text(pDX, IDC_FOLDER, vFolder);
DDX_Control(pDX, IDC_FOLDER, wFolder);
}


BEGIN_MESSAGE_MAP(DlgCreateMaterial, CDialog)
END_MESSAGE_MAP()


bool DlgCreateMaterial::ProcessFile(const char *filename)
  {
  wTemplateName.AddString(Pathname::GetNameFromPath(filename));
  return true;
  }

// DlgCreateMaterial message handlers

BOOL DlgCreateMaterial::OnInitDialog()
  {
  CDialog::OnInitDialog();
  ProcessFolders(theApp.config.vTemplatePath,"*.rvmat",false);    
  wFolder.SetSel(vFolder.GetLength(),vFolder.GetLength());
  return TRUE;
  }
