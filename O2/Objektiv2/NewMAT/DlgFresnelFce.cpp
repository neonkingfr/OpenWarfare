// FresnelFce.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgFresnelFce.h"


// DlgFresnelFce dialog

IMPLEMENT_DYNAMIC(DlgFresnelFce, CDialog)

DlgFresnelFce::DlgFresnelFce(CWnd* pParent /*=NULL*/)
	: CDialog(DlgFresnelFce::IDD, pParent)
{

}

DlgFresnelFce::~DlgFresnelFce()
{
}

void DlgFresnelFce::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_FORMAT, wFormat);
  DDX_Control(pDX, IDC_WIDTH, wWidth);
  DDX_Control(pDX, IDC_HEIGHT, wHeight);
  DDX_Control(pDX, IDC_LEVELS, wLevels);
  DDX_Control(pDX, IDC_FRES_SLIDER_N, sliderN);
  DDX_Control(pDX, IDC_FRES_SLIDER_K, sliderK);
  DDX_Control(pDX, IDC_FRES_EDIT_N, editN);
  DDX_Control(pDX, IDC_FRES_EDIT_K, editK);
  DDX_Control(pDX, IDC_FRES_GRAF, wSpecularGraph);
}


BEGIN_MESSAGE_MAP(DlgFresnelFce, CDialog)
  ON_WM_HSCROLL()
  ON_WM_DRAWITEM()
END_MESSAGE_MAP()

BOOL DlgFresnelFce::OnInitDialog()
  {
  CDialog::OnInitDialog();

  char buff[50];
  for (int i=0;i<10;i++)
    {
    int val=1<<i;
    sprintf(buff,"%d",val);
    wWidth.AddString(buff);
    wHeight.AddString(buff);
    sprintf(buff,"%d",i);
    wLevels.AddString(buff);
    }
  wFormat.AddString("rgb");
  wFormat.AddString("argb");
  wFormat.AddString("i");
  wFormat.AddString("ai");
  wFormat.AddString("a");

  ParseFrnString();

  sliderN.SetRange(1,1000);
  sliderN.SetPos(_vN*100);
  sliderK.SetRange(1,1000);
  sliderK.SetPos(_vK*100);

  CString ftext;
  ftext.Format("%f",(float)sliderK.GetPos()/100);
  editK.SetWindowText(ftext); 

  ftext.Format("%f",(float)sliderN.GetPos()/100);
  editN.SetWindowText(ftext);

  return TRUE; 
  }

void DlgFresnelFce::ParseFrnString(void)
  {
  char format[50];
  int n=sscanf(_frnString,"#(%50[^,],%d,%d,%d)fresnel(%f,%f)",
    format,&_width,&_height,&_levels,&_vN,&_vK);
  if (n!=6)
    {
    _format="ai";
    _width=32;
    _height=128;
    _levels=1;
    _vK=0;
    _vN=0;
    }
  else
    _format=format;

  wFormat.SetWindowText(_format);
  wWidth.SelectString(-1,_itoa(_width,format,10));
  wHeight.SelectString(-1,_itoa(_height,format,10));
  wLevels.SelectString(-1,_itoa(_levels,format,10));
  }

void DlgFresnelFce::OnOK()
  {
  char buff[50];
 
  wFormat.GetWindowText(_format);
  wWidth.GetLBText(wWidth.GetCurSel(),buff);
  _width=atoi(buff);
  wHeight.GetLBText(wHeight.GetCurSel(),buff);
  _height=atoi(buff);
  wLevels.GetLBText(wLevels.GetCurSel(),buff);
  _levels=atoi(buff);

  _frnString.Format("#(%s,%d,%d,%d)fresnel(%g,%g)",_format,_width,_height,_levels,max(_vN,0.001),max(_vK,0.001));
  CDialog::OnOK();
  }

// DlgFresnelFce message handlers

void DlgFresnelFce::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
  if ((void*)pScrollBar==&sliderN)
  {
    CString ftext;
    _vN=(float)sliderN.GetPos()/100;
    ftext.Format("%f",_vN);
    editN.SetWindowText(ftext);
  }
  else if ((void*)pScrollBar==&sliderK)
  {
    CString ftext;
    _vK=(float)sliderK.GetPos()/100;
    ftext.Format("%f",_vK);
    editK.SetWindowText(ftext);  
  } 

  // TODO: Add your message handler code here and/or call default
  UpdateGraphs();
  CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}
void DlgFresnelFce::RecalculateGraph()
  {
  if (_specGraph.GetSafeHandle()!=NULL) _specGraph.DeleteObject();
  CRect rc;
  wSpecularGraph.GetClientRect(&rc);
  CDC *dc=wSpecularGraph.GetDC();
  CDC bmpdc;
  bmpdc.CreateCompatibleDC(dc);
  _specGraph.CreateCompatibleBitmap(dc,rc.right,rc.bottom);
  wSpecularGraph.ReleaseDC(dc);
  CBitmap *oldbmp=bmpdc.SelectObject(&_specGraph);
  COLORREF bgr=GetSysColor(COLOR_WINDOW);
  COLORREF frg=GetSysColor(COLOR_WINDOWTEXT);
  CPen linepen,linepengray,*oldpen;
  linepen.CreatePen(PS_SOLID,1,frg);  
  oldpen=bmpdc.SelectObject(&linepen);
  bmpdc.FillSolidRect(&rc,bgr);
  int centx=rc.right/2;
  for(int x=0;x<rc.right;x++)
      {
      float s = acos((float)x/rc.right);
      float sins = sin(s),coss = cos(s),tans = tan(s);
      float sins2 = sins*sins, coss2 = coss*coss, tans2 = tans*tans;
      float n= _vN,k = _vK , n2 = n*n, k2 = k*k;
      float t1 = n2-k2-sins2;
      float t2 = sqrt(t1*t1+4*n2*k2);
      float t3 = n2-k2-sins2;
      float aa = sqrt((t2+t3)/2);
      float bb = sqrt((t2-t3)/2);
      float aa2 = aa*aa, bb2 = bb*bb;
      float fs = (aa2+bb2-2*aa*coss+coss2)/(aa2+bb2+2*aa*coss+coss2);
      float fp = fs*(aa2+bb2-2*aa*sins*tans+sins2*tans2)/(aa2+bb2+2*aa*sins*tans+sins2*tans2);
      
      float ints=(fs+fp)/2;
    
      float intr1=rc.bottom*ints;
      float intr2=floor(intr1);
      ints=intr1-intr2;

      float iints=1.0f-ints;
      int y=(rc.bottom-toLargeInt(intr2));
      bmpdc.MoveTo(x,rc.bottom);
      bmpdc.LineTo(x,y);
        COLORREF col=
          RGB(
            toLargeInt(GetRValue(bgr)*(iints)+GetRValue(frg)*ints),
            toLargeInt(GetGValue(bgr)*(iints)+GetGValue(frg)*ints),
            toLargeInt(GetBValue(bgr)*(iints)+GetBValue(frg)*ints)
            );
      bmpdc.SetPixel(x,y,col);
      }

  linepengray.CreatePen(PS_SOLID,1,RGB(0xa0,0xa0,0xa0));  
  bmpdc.SelectObject(&linepengray);
  for(int x=1;x<9;x++)
  {
    int xpos = toLargeInt((float)x*rc.right/9);
    bmpdc.MoveTo(xpos,rc.bottom);
    bmpdc.LineTo(xpos,rc.top);
  }
  for(int y=1;y<10;y++)
  {
    int ypos = toLargeInt((float)y*rc.bottom/10);
    bmpdc.MoveTo(rc.left,ypos);
    bmpdc.LineTo(rc.right,ypos);
  }


  bmpdc.SelectObject(oldbmp);
  bmpdc.SelectObject(oldpen);
  bmpdc.DeleteDC();  
  }

void DlgFresnelFce::UpdateGraphs()
  {
  RecalculateGraph();
  wSpecularGraph.Invalidate(FALSE);
  }

void DlgFresnelFce::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
  {
  if (nIDCtl==IDC_FRES_GRAF)
    {        
    CDC bmpdc;
    CBitmap *bmpview;
    
    bmpview=&_specGraph;
    if (bmpview->GetSafeHandle()==NULL) RecalculateGraph();

    bmpdc.CreateCompatibleDC(CDC::FromHandle(lpDrawItemStruct->hDC));
    CBitmap *oldbmp=bmpdc.SelectObject(bmpview);
    BitBlt(lpDrawItemStruct->hDC,lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.top,
      lpDrawItemStruct->rcItem.right-lpDrawItemStruct->rcItem.left,
      lpDrawItemStruct->rcItem.bottom-lpDrawItemStruct->rcItem.top,
      bmpdc,0,0,SRCCOPY);
    bmpdc.SelectObject(oldbmp);
    bmpdc.DeleteDC();
    return;
    }
  CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
  }

