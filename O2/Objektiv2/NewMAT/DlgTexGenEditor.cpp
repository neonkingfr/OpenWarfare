// DlgTexGenEditor.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgTexGenEditor.h"
#include "DlgTexGenCfg.h"

// CDlgTexGenEditor dialog

IMPLEMENT_DYNAMIC(CDlgTexGenEditor, CDialog)

CDlgTexGenEditor::CDlgTexGenEditor(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgTexGenEditor::IDD, pParent)
{
  memset(_panels,0,sizeof(_panels));
  _nTexGen=0;
  _sizeFromTop=0;
}

CDlgTexGenEditor::~CDlgTexGenEditor()
{
}

void CDlgTexGenEditor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgTexGenEditor, CDialog)
//  ON_WM_CLOSE()
END_MESSAGE_MAP()


int CDlgTexGenEditor::CreatePanel(CDialog * panel)
{
  for (int i=0;i<TEXGEN_MAXIMUMWINDOWS;i++)
    if (_panels[i]==NULL)
    {
      _panels[i]=panel;
      return i;
    } 
    return -1;
}
BOOL CDlgTexGenEditor::OnInitDialog()
{
  CDialog::OnInitDialog();
  _sizeFromTop=0;
  //RecalcLayout();

  for (int i=0;i<TEXGEN_MAXIMUMWINDOWS;i++)
  {
    CDlgTexGenCfg *stage=dynamic_cast<CDlgTexGenCfg *>(_panels[i]);
    if (stage)
    {
      //CDlgTexGenCfg *stage2=new CDlgTexGenCfg;
      stage->Create(stage->IDD,this);
      stage->SetWindowPos(&wndTop ,0,_sizeFromTop,0,0,SWP_NOSIZE);
      CRect rc;
      stage->GetWindowRect(&rc);
      _sizeFromTop+=rc.bottom-rc.top;
      SetWindowPos(NULL,0,0,rc.right-rc.left+10,_sizeFromTop+50,SWP_NOZORDER|SWP_NOMOVE);//TODO
      stage->ShowWindow(SW_SHOW); 
    };
  };
  //SetWindowPos(&wndTopMost,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
  ShowWindow(SW_SHOW);
  return TRUE;
}
int CDlgTexGenEditor::AddTexGen(ParamEntryPtr texGenData,int number)
{
  int end=0;
  int i=number;
  if (number==-1)
  {  
    number=0;
    for (i=0;i<TEXGEN_MAXIMUMWINDOWS && _panels[i];i++)
    {
      CDlgTexGenCfg *stage=dynamic_cast<CDlgTexGenCfg *>(_panels[i]);
      if (stage)
      {
        number=__max(number,stage->GetNumber()+1);
      }
    }
    end=i;
  }
  if (end>=TEXGEN_MAXIMUMWINDOWS) return -1;
  CDlgTexGenCfg *stage=new CDlgTexGenCfg;
  stage->SetNumber(number);
  if (texGenData)
    stage->LoadTexGenPanel(texGenData);
  ++_nTexGen;
  int result;
  if ((result=CreatePanel(stage))==-1) delete stage;
  return result;
}

void CDlgTexGenEditor::Serialize(IArchive &arch)
{
/*  ArchiveSection asect(arch);
  for (int i=0;i<TEXGEN_MAXIMUMWINDOWS && _panels[i];i++)
    {    
      DlgTexGenCfg *stage=dynamic_cast<DlgTexGenCfg *>(_panels[i]);
      if (stage)
      {
        int q=1;
        arch(q);
        if (q==0)
        {
          while (_panels[i]) DeletePanel(static_cast<CDialog *>(_panels[i]));
          goto skip;
        }
        else
        {
          stage->Serialize(arch);          
          stage->SerializeLock(arch);
        }
      }
    }
  while (asect.Block("TexGen0")) _stage0Panel->Serialize(arch);*/
}
// CDlgTexGenEditor message handlers
/*
static void LoadTexGenPanel(DlgStageCfg *panel, const ParamEntry *stageinfo)
{
  if (stageinfo && stageinfo->IsClass())
  {

    temp=stageinfo->FindEntry("uvSource");
    if (temp.NotNull()) panel->SetUVSource(temp->GetValue());  
    temp=stageinfo->FindEntry("uvTransform");
    if (temp.NotNull() && temp->IsClass())
    {
      panel->_resultMatrix=LoadTransform(temp.GetPointer());
      panel->ResetMatrix();
    }
  }
}
*/
void CDlgTexGenEditor::DeleteAllTexGens()
{
  for (int i=0;i<TEXGEN_MAXIMUMWINDOWS && _panels[i];i++)
  {
    CDlgTexGenCfg *stage=dynamic_cast<CDlgTexGenCfg *>(_panels[i]);
    if (stage)
    {
      DeletePanel(stage);
      i--;
    }
    _nTexGen=0;
  }
}
bool CDlgTexGenEditor::DeletePanel(int index)
{
  _panels[index]->DestroyWindow();
  delete _panels[index];
  for (int i=index+1;i<TEXGEN_MAXIMUMWINDOWS;i++)
  {    
    _panels[i-1]=_panels[i];
    if (_panels[i]==NULL) return true;
  }
  _panels[TEXGEN_MAXIMUMWINDOWS-1]=NULL;
  --_nTexGen;
  return true;
}

bool CDlgTexGenEditor::DeletePanel(CDialog * panel)
{
  int index;
  for (index=0;index<TEXGEN_MAXIMUMWINDOWS && _panels[index];index++)
    if (_panels[index]==panel) 
    {
      --_nTexGen;
      return DeletePanel(index);
    }
  return false;
}

int CDlgTexGenEditor::SaveTexGens(ParamFile &pf)
{
  ParamEntryPtr array;
  for (int i=0;i<TEXGEN_MAXIMUMWINDOWS && _panels[i];i++)
  {
    CDlgTexGenCfg *stage=dynamic_cast<CDlgTexGenCfg *>(_panels[i]);
    if (stage)
    {        
      char buff[20];
      sprintf(buff,"TexGen%d",stage->GetNumber());
      ParamClassPtr cls=pf.AddClass(buff);    
      
      CString uvsource=stage->GetUVSource();
      cls->Add("uvSource",(LPCTSTR)uvsource);


      if (_strnicmp(uvsource,"tex",3)==0)
      {
        const geMatrix4 &m=stage->GetTransform();
        ParamClassPtr mxcls=cls->AddClass("uvTransform");              
        array=mxcls->AddArray("aside");
        array->AddValue(m.a11);
        array->AddValue(m.a12);
        array->AddValue(m.a13);
        array=mxcls->AddArray("up");
        array->AddValue(m.a21);
        array->AddValue(m.a22);
        array->AddValue(m.a23);
        array=mxcls->AddArray("dir");
        array->AddValue(m.a31);
        array->AddValue(m.a32);
        array->AddValue(m.a33);
        array=mxcls->AddArray("pos");
        array->AddValue(m.a41);
        array->AddValue(m.a42);
        array->AddValue(m.a43);
      }  
    }
  }
  return 0;
}

