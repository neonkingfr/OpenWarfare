#pragma once


// DlgAskCheckIn dialog

class DlgAskCheckIn : public CDialog
{
	DECLARE_DYNAMIC(DlgAskCheckIn)

public:
	DlgAskCheckIn(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgAskCheckIn();

// Dialog Data
	enum { IDD = IDD_CHECKASKDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CString vName;
  afx_msg void OnBnClickedCheckin();
  afx_msg void OnBnClickedUndocheckout();
};
