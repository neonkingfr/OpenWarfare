#include <math.h>
#include "XYZSet.h"
#include <stdlib.h>

/**
* x color matching function tabulated at 20-nm intervals.
* (10-degree 1964 CIE suppl. std. observer)
*/
float XYZSet::Px[] = {
	0.00327f, 0.03505f, 0.06577f, 0.05182f,
	0.01380f, 0.00065f, 0.02017f, 0.06458f,
	0.12087f, 0.17384f, 0.19266f, 0.14677f,
	0.07398f, 0.02616f, 0.00701f, 0.00165f
};

/**
* y color matching function tabulated at 20-nm intervals.
* (10-degree 1964 CIE suppl. std. observer)
*/
float XYZSet::Py[] = {
	0.00034f, 0.00367f, 0.01064f, 0.02197f,
	0.04347f, 0.07898f, 0.13058f, 0.16489f,
	0.17094f, 0.14893f, 0.11284f, 0.06824f,
	0.03082f, 0.01034f, 0.00273f, 0.00062f

};

/**
* z color matching function tabulated at 20-nm intervals.
* (10-degree 1964 CIE suppl. std. observer)
*/
float XYZSet::Pz[] = {
	0.01474f, 0.16669f, 0.33720f, 0.29917f,
	0.13234f, 0.03745f, 0.01040f, 0.00235f,
	0, 0, 0, 0, 0, 0, 0, 0
};

/*
RGB to HSV & HSV to RGB

The Hue/Saturation/Value model was created by A. R. Smith in 1978. It is based on such intuitive color characteristics as tint, shade and tone (or family, purety and intensity). The coordinate system is cylindrical, and the colors are defined inside a hexcone. The hue value H runs from 0 to 360o. The saturation S is the degree of strength or purity and is from 0 to 1. Purity is how much white is added to the color, so S=1 makes the purest color (no white). Brightness V also ranges from 0 to 1, where 0 is the black.

There is no transformation matrix for RGB/HSV conversion, but the algorithm follows:
*/
// r,g,b values are from 0 to 1
// h = [0,360], s = [0,1], v = [0,1]
//		if s == 0, then h = -1 (undefined)
void RGBtoHSV( float r, float g, float b, float *h, float *s, float *v )
{
	float min, max, delta;
	min = __min( r, __min(g, b ));
	max = __max( r, __max(g, b ));
	*v = max;				// v
	delta = max - min;
	if( max != 0 )
		*s = delta / max;		// s
	else {
		// r = g = b = 0		// s = 0, v is undefined
		*s = 0;
		*h = -1;
		return;
	}
	if( r == max )
		*h = ( g - b ) / delta;		// between yellow & magenta
	else if( g == max )
		*h = 2 + ( b - r ) / delta;	// between cyan & yellow
	else
		*h = 4 + ( r - g ) / delta;	// between magenta & cyan
	*h *= 60;				// degrees
	if( *h < 0 )
		*h += 360;
}
void HSVtoRGB( float *r, float *g, float *b, float h, float s, float v )
{
	int i;
	float f, p, q, t;
	if( s == 0 ) {
		// achromatic (grey)
		*r = *g = *b = v;
		return;
	}
	h /= 60;			// sector 0 to 5
	i = (int)floor( h );
	f = h - i;			// factorial part of h
	p = v * ( 1 - s );
	q = v * ( 1 - s * f );
	t = v * ( 1 - s * ( 1 - f ) );
	switch( i ) {
		case 0:
			*r = v;
			*g = t;
			*b = p;
			break;
		case 1:
			*r = q;
			*g = v;
			*b = p;
			break;
        case 2:
			*r = p;
			*g = v;
			*b = t;
			break;
		case 3:
			*r = p;
			*g = q;
			*b = v;
			break;
		case 4:
			*r = t;
			*g = p;
			*b = v;
			break;
		default:		// case 5:
			*r = v;
			*g = p;
			*b = q;
			break;
	}
}
