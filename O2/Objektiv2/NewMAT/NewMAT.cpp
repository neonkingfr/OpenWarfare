// NewMAT.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgCreateMaterial.h"
#include <el/interfaces/iScc.hpp>
#include "DlgCreatePrimitive.h"
#include ".\newmat.h"
#include "DlgAbout.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//	Note!
//
//		If this DLL is dynamically linked against the MFC
//		DLLs, any functions exported from this DLL which
//		call into MFC must have the AFX_MANAGE_STATE macro
//		added at the very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

// CNewMATApp

BEGIN_MESSAGE_MAP(CNewMATApp, CWinApp)
END_MESSAGE_MAP()


// CNewMATApp construction

CNewMATApp::CNewMATApp()
{
  _reloadTemplates=true;
  _prim_segments=2;
  _prim_type=IMatAppSide::PrimBox;
  _prim_texture="";
  // TODO: add construction code here,
  // Place all significant initialization in InitInstance
}


// The one and only CNewMATApp object

CNewMATApp theApp;


// CNewMATApp initialization

CStringRes StrRes;

BOOL CNewMATApp::InitInstance()
{
  CWinApp::InitInstance();
  StrRes.SetInstance(AfxGetInstanceHandle());
  SetRegistryKey(_T("Bohemia Interactive Studio"));
  config.LoadConfig();
  hAccTable=LoadAccelerators(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDR_ACCTABLE));
  return TRUE;
}

// Export init function
extern "C" __declspec(dllexport) IPluginGeneralPluginSide * __cdecl  ObjektivPluginMain(IPluginGeneralAppSide *appside)
{
  if (ProcessDllInit(appside->O2Allocator,appside->O2Deallocator)==0) return 0;
  theApp.Init(appside);
  return &theApp;
}


class CmdAbout: public IPluginCommand
{
public:
  virtual void OnCommand() 
  {
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
    DlgAbout dlg;
    dlg.DoModal();
  }
  virtual void AddRef() {}
  virtual void Release() {}
  virtual void Update(const IPluginMenuCommandUpdate *update) {}
};


static CmdAbout GCmdAbout;

void CNewMATApp::Init(IPluginGeneralAppSide *appInterface)
{
  _appGen=appInterface;  
}

void CNewMATApp::AfterOpen(HWND o2window)
{
  _appGen->AddMenuItem(_appGen->mnuHelp,StrRes[IDS_HELPMENUNAME],&GCmdAbout);
}

void CNewMATApp::EditMaterial(const char *filename)
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  if (_reloadTemplates) 
  {
    _reloadTemplates=false;
    ReloadTemplatesFromSS();
  }
  DlgMaterialEditor *editor;
  for (unsigned int pos=0;editor=_tabMat.EnumPanels(pos);)
  {
    const char *editName=editor->GetFilename();
    if (editName && stricmp(editName,filename)==0)
    {
      _tabMat.SetActive(editor);
      _tabMat.Activate();
      return;
    }
  }
  editor=CreateMatTab();
  editor->LoadMaterial(filename);
}

bool CNewMATApp::CreateMaterial(const char *refFolder)
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  ReloadTemplatesFromSS();
  DlgCreateMaterial dlg(&_tabMat);
  dlg.vFolder=refFolder;
  if (dlg.DoModal()==IDOK)
  {
    if (dlg.vMatName.GetLength())
    {
      Pathname matpath;
      matpath.SetDirectory(refFolder);
      matpath.SetExtension(".rvmat");
      matpath.SetFilename(dlg.vMatName);
      if (stricmp(matpath.GetExtension(),"")==0) matpath.SetExtension(".rvmat");
      if (dlg.vTemplateName.GetLength())
      {
        Pathname tempName;
        const char *root=_appGen->GetProjectRoot();
        tempName.SetDirectory(theApp.config.vTemplatePath);
        tempName.SetFilename(dlg.vTemplateName);        
        EditMaterial(tempName);
        Active()->SetMaterialName(matpath);
      }
      else
      {
        EditMaterial(matpath);
      }
      _appMat->UseMaterial(matpath);
      return true;
    }
  }
  return false;
}

const char *CNewMATApp::GetActiveMaterial()
{
  DlgMaterialEditor *editor=Active();
  if (editor)  return editor->GetFilename();
  else return NULL;
}

bool CNewMATApp::OnNewDocument()
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());  
  if (_tabMat.BeforeClose())
  {
    _tabMat.DestroyWindow();
    return true;
  }
  return false;
}

bool CNewMATApp::BeforeClose()
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());  
  if (_tabMat.BeforeClose())
  {
    _tabMat.DestroyWindow();
    return true;
  }
  return false;
}

void CNewMATApp::SSReload()
{

}

BOOL CNewMATApp::OnPretranslateMessage(MSG *msg)
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  if (_tabMat.PreTranslateMessage(msg))
    return TRUE;
  return FALSE;  
}

void CNewMATApp::AfterPrepareModelForViewer(const ObjectData &src, ObjectData &trg)
{
  DlgMaterialEditor *editor=Active();
  if (editor!=NULL)  editor->AfterPrepareModelForViewer(src,trg);
}

bool CNewMATApp::CreateNewWindow()
{
  CreateMatTab();
  return true;
}

bool GetFileTime(const char *filename,FILETIME& ftime)
{
  WIN32_FIND_DATA fnd;
  HANDLE hfnd=FindFirstFile(filename,&fnd);
  if (hfnd!=INVALID_HANDLE_VALUE)
  {
    ftime=fnd.ftLastWriteTime;    
    FindClose(hfnd);
    return true;
  }
  else
  {
    ftime.dwHighDateTime=ftime.dwLowDateTime=0;
    return false;
  }
}

void CNewMATApp::ReloadTemplatesFromSS()
{
  SccFunctions *scc=CallGen().GetSccInterface();
  if (scc && scc->UnderSSControlDir(config.vTemplatePath))
  {
    scc->GetLatestVersionDir(config.vTemplatePath);
  }
}

void CNewMATApp::ChangePrimitive()
{
  DlgCreatePrimitive dlg;
  dlg.quality=_prim_segments;
  dlg.primType=_prim_type;
  dlg.vSize=toInt(_prim_scale);
  if (dlg.DoModal()==IDOK)
  {
    _prim_segments=dlg.quality;
    _prim_type=dlg.primType;
    _prim_model="";
    _prim_scale=dlg.vSize;
  }
}

void CNewMATApp::SetPrimTexture()
{
  CFileDialog dlg(TRUE,NULL,_prim_texture,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,StrRes[IDS_TEXTUREFILTES]);
  if (dlg.DoModal()==IDOK)
  {
    _prim_texture=dlg.GetPathName();
  }
}

void CNewMATApp::ChangePrimitiveFromFile()
{
  CFileDialog dlg(TRUE,NULL,_prim_model,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,StrRes[IDS_P3DFILTERS]);
  if (dlg.DoModal()==IDOK)
  {
    _prim_model=dlg.GetPathName();
  }
}
/*
bool CNewMATApp::OnAppCommand(UINT command)
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());
  if (command==_menuHelp) 
  {
    DlgAbout dlg;
    dlg.DoModal();
    return true;
  }
  else 
    return false;
}
*/
/*
bool CNewMATApp::OnUpdateAppCommand(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(TRUE);
  return true;
}
*/
int CNewMATApp::ExitInstance()
{
  DestroyAcceleratorTable(hAccTable);
  return __super::ExitInstance();
}

DlgMaterialEditor *CNewMATApp::CreateMatTab()
{
  DlgMaterialEditor *dlg=new DlgMaterialEditor;
  if (_tabMat.GetSafeHwnd()==NULL)
  {
    _tabMat.Create(_tabMat.IDD,CWnd::GetDesktopWindow());
  }
  dlg->Create(IDD_MATEDITTAB,&_tabMat);
  _tabMat.AddPanel(dlg);
  return dlg;
}

void CNewMATApp::OnCloseTabMenu()
{
  _tabMat.CloseCurrentTab();
}

void CNewMATApp::OnExitMatMenu()
{
  _tabMat.SendMessage(WM_CLOSE,0,0);
}

void CNewMATApp::Init(IMatAppSide *appInterface)
{
  _appMat=appInterface;
}

void CNewMATApp::BeforePluginUnload()
{
  ProcessDllShutdown();
}