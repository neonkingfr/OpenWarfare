#pragma once

template<>
inline void ArchiveVariableExchange(IArchive &arch,RString &data)
  {
  if (-arch) 
    {
    char *c=NULL;
    arch(c);
    data=c;
    delete [] c;
    }
  else
    {    
    arch.RValue(const_cast<char *>(data.Data()));
    }
  }

template<>
inline void ArchiveVariableExchange(IArchive &arch, CString &data)
  {
  if (-arch) 
    {
    char *c=NULL;
    arch(c);
    data=c;
    free(c);
    }
  else
    {    
    arch.RValue(const_cast<char *>((LPCTSTR)data));
    }
  }
