// NewMAT.h : main header file for the NewMAT DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "NewMatInterface.hpp"
#include "DlgMaterialEditor.h"
#include "DlgOptions.h"
#include "DlgTabbedMat.h"

using namespace ObjektivLib;

#define MAXIMUM_MAT_WINDOWS 16

#define WRITECONFIGSTR(sect,x) theApp.WriteProfileString(sect,#x,x);
#define WRITECONFIGINT(sect,x) theApp.WriteProfileInt(sect,#x,x);
#define WRITECONFIGFLOAT(sect,x) theApp.WriteProfileInt(sect,#x,toLargeInt(x*100000));
#define GETCONFIGSTR(sect,x,def) x=theApp.GetProfileString(sect,#x,def);
#define GETCONFIGINT(sect,x,def) x=theApp.GetProfileInt(sect,#x,def);
#define GETCONFIGFLOAT(sect,x,def) x=(int)theApp.GetProfileInt(sect,#x,toLargeInt(def*100000))/100000.0f;


// CNewMATApp
// See NewMAT.cpp for the implementation of this class
//

class CNewMATApp : public CWinApp, public IMatPluginSide, public IPluginGeneralPluginSide
{
public:
	CNewMATApp();
    IPluginGeneralAppSide *_appGen;
    IMatAppSide *_appMat;

    DlgTabbedMat _tabMat;
    DlgOptions config;
    bool _reloadTemplates;

    int _menuHelp;

// Overrides
public:
	virtual BOOL InitInstance();

    void Init(IPluginGeneralAppSide *appInterface);     //vola jednou po inicializaci
    virtual void Init(IMatAppSide *appInterface);     //vola jednou po inicializaci
    virtual void EditMaterial(const char *filename);       //Spusti editor s materialem. Soubor nemus� existovat
    virtual const char *GetActiveMaterial();          //vrac� jm�no materi�lu, kter� se edituje
    virtual bool BeforeClose();                       //vola aplikace p�ed uzav�en�m O2
    virtual void SSReload();                          //vola aplikace po GetLatestVersion v�ech dat
    virtual bool CreateMaterial(const char *refFolder);
    virtual BOOL OnPretranslateMessage(MSG *msg);
    virtual void AfterPrepareModelForViewer(const ObjectData &src, ObjectData &trg);
    virtual int GetAllDependencies(const char *p3dname, const LODObject *object, char **array) {return 0;}
    virtual void *GetService(ServiceName name)
      {if (name==Service_MaterialEditor) return (void *)(static_cast<IMatPluginSide *>(this));else return 0;}
    virtual void AfterOpen(HWND o2window);
    virtual bool OnNewDocument();
    virtual void BeforePluginUnload();



    HACCEL hAccTable;
  
    IPluginGeneralAppSide &CallGen() {return *_appGen;}
    IMatAppSide &CallMatClient() {return *_appMat;}

    void SetActiveMatWindow(DlgMaterialEditor *active) 
      {_tabMat.SetActive(active);}
    DlgMaterialEditor *Active()
    {return _tabMat.GetSafeHwnd()?_tabMat.GetActive():NULL;}
    bool CreateNewWindow();

    void ReloadTemplatesFromSS();

    int _prim_segments;
    IMatAppSide::ObjPrimitiveType _prim_type;
    CString _prim_texture;
    CString _prim_model;
    float _prim_scale;

    void ChangePrimitive();
    void ChangePrimitiveFromFile();
    void SetPrimTexture();

    DlgMaterialEditor *CreateMatTab();

    void OnCloseTabMenu();
    void OnExitMatMenu();

    CWnd *GetMyTopLevelWindow(CWnd *me) {return &_tabMat;}

	DECLARE_MESSAGE_MAP()
    virtual int ExitInstance();
};

extern CNewMATApp theApp;

bool GetFileTime(const char *filename,FILETIME& ftime);