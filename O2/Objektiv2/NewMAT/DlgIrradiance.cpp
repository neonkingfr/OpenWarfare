// DlgIrradiance.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgIrradiance.h"
#include ".\dlgirradiance.h"


// DlgIrradiance dialog

IMPLEMENT_DYNAMIC(DlgIrradiance, CDialog)
DlgIrradiance::DlgIrradiance(CWnd* pParent /*=NULL*/)
	: CDialog(DlgIrradiance::IDD, pParent)
{
}

DlgIrradiance::~DlgIrradiance()
{
}

void DlgIrradiance::DoDataExchange(CDataExchange* pDX)
{
CDialog::DoDataExchange(pDX);
DDX_Control(pDX, IDC_FORMAT, wFormat);
DDX_Control(pDX, IDC_WIDTH, wWidth);
DDX_Control(pDX, IDC_HEIGHT, wHeight);
DDX_Control(pDX, IDC_LEVELS, wLevels);
}


BEGIN_MESSAGE_MAP(DlgIrradiance, CDialog)
END_MESSAGE_MAP()


// DlgIrradiance message handlers

BOOL DlgIrradiance::OnInitDialog()
  {
  CDialog::OnInitDialog();
  char buff[50];
  for (int i=0;i<10;i++)
    {
    int val=1<<i;
    sprintf(buff,"%d",val);
    wWidth.AddString(buff);
    wHeight.AddString(buff);
    sprintf(buff,"%d",i);
    wLevels.AddString(buff);
    }
  wFormat.AddString("rgb");
  wFormat.AddString("argb");
  wFormat.AddString("i");
  wFormat.AddString("ai");
  wFormat.AddString("a");

  CPoint pt(0,0);
  GetDlgItem(IDC_SUBDIALOG)->MapWindowPoints(this,&pt,1);
  _specular.Create(IDD_SPECULARCFGIRRADIANCE,this);
  _specular._stepMode=true;
  _specular.SetWindowPos(&wLevels,0,pt.y,0,0,SWP_NOSIZE);
  _specular.ShowWindow(SW_SHOW);

  ParseIrrString();
  
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

void DlgIrradiance::ParseIrrString(void)
  {
  char format[50];
  int n=sscanf(_irrString,"#(%50[^,],%d,%d,%d)irradiance(%f)",
    format,&_width,&_height,&_levels,&_exponent);
  if (n!=5)
    {
    _format="ai";
    _width=32;
    _height=128;
    _levels=1;
    _exponent=4.0f;
    }
  else
    _format=format;
  wFormat.SetWindowText(_format);
  wWidth.SelectString(-1,_itoa(_width,format,10));
  wHeight.SelectString(-1,_itoa(_height,format,10));
  wLevels.SelectString(-1,_itoa(_levels,format,10));
  _specular._factor=_exponent;
  _specular.LoadValues();
  }

void DlgIrradiance::OnOK()
  {
  char buff[50];
  wFormat.GetWindowText(_format);
  wWidth.GetLBText(wWidth.GetCurSel(),buff);
  _width=atoi(buff);
  wHeight.GetLBText(wHeight.GetCurSel(),buff);
  _height=atoi(buff);
  wLevels.GetLBText(wLevels.GetCurSel(),buff);
  _levels=atoi(buff);
  _exponent=_specular._factor;
  _irrString.Format("#(%s,%d,%d,%d)irradiance(%g)",_format,_width,_height,_levels,_exponent);
  CDialog::OnOK();
  }
