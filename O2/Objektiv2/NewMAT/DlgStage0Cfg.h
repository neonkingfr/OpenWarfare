#pragma once
#include "afxwin.h"

#include "ConfigPanelsCommon.h"
class IArchive;


// DlgStage0Cfg dialog

class DlgStage0Cfg : public CDialog
{
	DECLARE_DYNAMIC(DlgStage0Cfg)

public:
	DlgStage0Cfg(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgStage0Cfg();

// Dialog Data
	enum { IDD = IDD_STAGE0DLG };
    bool _edit;
    bool _browsed;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CComboBox wTextureName;
  CString vTextureName;
  afx_msg void OnEnChangeTexturename();
  afx_msg void OnCbnSelchangeTexturename();
  afx_msg void OnCbnKillfocusTexturename();
  virtual BOOL OnInitDialog();

  void Serialize(IArchive &arch);
  void DlgStage0Cfg::SetTexture(const char *string);
  CString vStageFilter;
  void SetStageFilter(const char *filter);
  afx_msg void OnCbnEditupdateStagefilter();
};
