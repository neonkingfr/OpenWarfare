#pragma once


// DlgMaterialEditor dialog

#include <iostream>
#include <strstream>

#include "SMatRestricts.h"
#include "MaterialUndoRedo.h"
#include "DlgUpdateMaterial.h"
#include "DlgTexGenEditor.h"



using namespace std;
using namespace ObjektivLib;

class DlgColorCfg;
class DlgGeneralCfg;
class DlgSpecularCfg;
class DlgStage0Cfg;
class DlgNextPassCfg;

#define MAT_MAXIMUMWINDOWS 100

class DlgMaterialEditor : public CDialog
{
	DECLARE_DYNAMIC(DlgMaterialEditor)
    CDlgTexGenEditor texGenDlg;
    int _texGenCount;
    CWnd *_panels[MAT_MAXIMUMWINDOWS];
    CSize _dlgsizes[MAT_MAXIMUMWINDOWS];
    
    DlgGeneralCfg *_generalPanel;
    DlgColorCfg *_ambientPanel;
    DlgColorCfg *_diffusePanel;
    DlgColorCfg *_forcedDPanel;
    DlgColorCfg *_emmissvPanel;
    DlgColorCfg *_speculrPanel;
    DlgSpecularCfg *_specpowPanel;
    DlgStage0Cfg *_stage0Panel;
    DlgNextPassCfg *_nextPassPanel;

    CBrush yellow;

    AutoArray<SMatRestricts> matRestricts;

    int _ypos_scroll;

    int _formLength;
    int _formWidth;
    int _stageIndex;

    int _scc_status;

    Pathname _rvmatName;
    Pathname _rvmatTempName;

    HANDLE _notepad;
    FILETIME _edit_file_time;

    CCriticalSection csect;

    bool _waitUpdate;
    bool _enabledirty;
    bool _dirty;
    bool _needSaveUndo;
    bool _canSaveUndo;
    bool _enableSaveUndo;

    MaterialUndoRedo _undo;

    ParamFile _pfCopy;
    DlgUpdateMaterial _dlgUpdateMat;


    


public:
	DlgMaterialEditor(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgMaterialEditor();

// Dialog Data
	enum { IDD = IDD_MATEDITORDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support


	DECLARE_MESSAGE_MAP()
public:
  int CreatePanel(CDialog * panel);
  bool DeletePanel(int index);
  bool DeletePanel(CDialog * panel);
  void RecalcLayout(bool update=true);
  virtual BOOL OnInitDialog();
  void RecreateOtherControls(void);
protected:
  virtual void OnOK();
  virtual void OnCancel();

public:
  afx_msg void OnDestroy();
  afx_msg void OnShowHidePanel(UINT cmd);
  afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar *pScrollBar);
  void RecalcScrollbar(void);
  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
  void EnsureVisible(HWND hWnd);
  virtual BOOL PreTranslateMessage(MSG* pMsg);
  afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
  afx_msg LRESULT OnMsgColorChanged(WPARAM id,LPARAM lParam);
  afx_msg LRESULT OnMsgDetectCycles(WPARAM id, LPARAM lParam);
  afx_msg LRESULT OnMsgGetMaterialName(WPARAM wParam,LPARAM lParam);
  afx_msg LRESULT OnMsgStageChanged(WPARAM wParam, LPARAM lParam);
  afx_msg void OnTimer(UINT nIDEvent);
protected:
  virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
public:
  int AddStage(int number,int texGenCount);
  void DeleteAllStages(BTree<RStringI> *scaveFiles=0);
  void OnAddStage();
  int LoadMaterial(const char * filename);
  int LoadMaterial(ParamFile &material);
  int LoadTexGens(ParamFile &material);
  int LoadMaterial(strstream &in);
  void DisplayPFileError(LSError error, const char *filename);
  bool ShowPanel(CDialog * panel, bool show);
//  bool SaveMaterial(ostream &matinfo, bool forpreview);
  void AfterPrepareModelForViewer(const ObjectData &src, ObjectData &trg);
  void UpdateViewer(bool delayed=true); 
  bool AskExitMAT();
  afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
  afx_msg void OnFileExit();
  afx_msg void OnFileNew();
  bool NewDocument(void);
  bool CloseDocument();
  void UpdateTitle(void);
  afx_msg void OnViewNewwindow();
  afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
  bool SaveMaterial(const Pathname & filename);
  afx_msg void OnFileSave();
  afx_msg void OnFileSaveas();
  afx_msg void OnFileOpen();
  afx_msg void OnFileOptions();
  afx_msg void OnFileMaterialtypemanager();
  void InitByRestricts();
  afx_msg void OnSoucecontrolReloadtemplatestypes();  
  LRESULT OnRestrictionsChanges(WPARAM wParam, LPARAM lParam);

  int DetectMaterialType();
  void SetMaterialTypeFromDialog();
  void SetMaterialName(const char *name);
  afx_msg void OnSave();
  afx_msg void OnFileSaveastemplate();
  afx_msg void OnFileSaveandcheckin();

  void UpdateSSStatus();
  afx_msg void OnUpdateFileSaveandcheckin(CCmdUI *pCmdUI);
  afx_msg void OnFileOpenfromsourcecontrol();
  afx_msg void OnUpdateFileOpenfromsourcecontrol(CCmdUI *pCmdUI);
  afx_msg void OnSoucecontrolCheckout();
  afx_msg void OnUpdateSoucecontrolCheckout(CCmdUI *pCmdUI);
  afx_msg void OnSoucecontrolGetlatestversion();
  afx_msg void OnUpdateSoucecontrolGetlatestversion(CCmdUI *pCmdUI);
  afx_msg void OnSoucecontrolHistory();
  afx_msg void OnUpdateSoucecontrolHistory(CCmdUI *pCmdUI);
  afx_msg void OnSoucecontrolUndocheckout();
  afx_msg void OnUpdateSoucecontrolUndocheckout(CCmdUI *pCmdUI);
  afx_msg LRESULT OnDelStageCommand(WPARAM wParam,LPARAM lParam);
  afx_msg void OnInsertStage();
  afx_msg void OnPrimitiveCreate();
  afx_msg void OnPrimitiveLoadfromfile();
  afx_msg void OnPrimitiveSelecttexture();
  afx_msg void OnViewShowprimitive();
  afx_msg void OnViewShowmodel();
  afx_msg void OnEditTextedito();
  afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
  void BeginDragScroll(const CPoint & pt);
  void DragScroll(const CPoint & pt);
  afx_msg void OnMouseMove(UINT nFlags, CPoint point);
  afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
  afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
  afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
  afx_msg void OnPrimitiveSetcolor();
  afx_msg void OnPrimitiveSetwhite();
  afx_msg void OnPrimitiveCleartexture();

  void SaveUndo();
  afx_msg void OnEditUndo();
  afx_msg void OnEditRedo();
  afx_msg void OnUpdateEditUndo(CCmdUI *pCmdUI);
  afx_msg void OnUpdateEditRedo(CCmdUI *pCmdUI);
  CDialog *GetFocusPanel();
  afx_msg void OnEditCopy(); 
  afx_msg void OnEditPaste();

  void Serialize(IArchive &arch);
  afx_msg void OnViewSelectino2();
  afx_msg void OnPrimitiveSetcolor01();
  afx_msg void OnPanelNextpass();

  const char *GetFilename() { return _rvmatName; }

  afx_msg void OnTabClosetab();

  bool SaveMaterial(ParamFile &pf, bool forpreview);
  void CheckAndUpdateChanges(void);
  afx_msg void OnFileReload();
  afx_msg void OnViewTexgen();
  afx_msg void OnUpdateViewTexgen(CCmdUI *pCmdUI);
};
