//DlgStageCfg.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgStageCfg.h"
#include ".\dlgstagecfg.h"
#include "DlgEditMatrix.h"
#include "DlgIrradiance.h"
#include "DlgFresnelFce.h"
#include <io.h>
#include "MassProcess.h"
#include "../../Bredy.Libs/Archive/Archive.h"
#include "../../Bredy.Libs/Archive/ArchiveStreamMemory.h"
#include "../../Bredy.Libs/Archive/ArchiveParamFile2.h"
#include "ArchiveExt.h"
#include "DlgProceduralTex.h"


// DlgStageCfg dialog
int DlgStageCfg::_clipboardFormat=-1;



IMPLEMENT_DYNAMIC(DlgStageCfg, CDialog)
DlgStageCfg::DlgStageCfg(CWnd* pParent /*=NULL*/)
: CDialog(DlgStageCfg::IDD, pParent),
_initialMatrix(true),
_resultMatrix(true),
_lockUVSource(false)
  {
  _trnsUpdate=true; 
  _matrixValid=true;
  _nTexGen=-1;
  if (_clipboardFormat==-1) _clipboardFormat=::RegisterClipboardFormat("O2 MAT Stage Info");
  }

DlgStageCfg::~DlgStageCfg()
  {
  }

void DlgStageCfg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_STACKLIST, wTransformStack);
  DDX_Control(pDX, IDC_OPERATION, wTransformOper);
  DDX_Control(pDX, IDC_VALUE, wTransformValue);
  DDX_Control(pDX, IDC_UVSOURCE, wUVSource);
  DDX_Control(pDX, IDC_SOURCENAME, wStageSource);
  DDX_Control(pDX, IDC_STAGENAMEEDIT, wStageName);
  DDX_Control(pDX, IDC_STAGEFILTER,wStageFilter);
  DDX_Text(pDX, IDC_SOURCENAME, vStageSource);
  DDX_Control(pDX,IDC_INSERTTRANSFORM, wInsertTransform);
  DDX_Control(pDX,IDC_DELETETRANSFORM, wDeleteTransform);
  DDX_Control(pDX,IDC_VALUESPIN, wTransformValueSpin);
  DDX_Control(pDX,IDC_EDITMATRIX, wEditMatrix);
  DDX_Control(pDX,IDC_STATIC_TEX_SOURCE, wTexSource);
  }

void DlgStageCfg::SetStageNumber(int number)
  {
  _number=number;
  SetDlgItemInt(IDC_STAGENUMBER,_number,FALSE);  
  }



BEGIN_MESSAGE_MAP(DlgStageCfg, CDialog)
  ON_EN_CHANGE(IDC_VALUE, OnEnChangeValue)
  ON_CBN_SELCHANGE(IDC_OPERATION, OnCbnSelchangeOperation)
  ON_NOTIFY(UDN_DELTAPOS, IDC_VALUESPIN, OnDeltaposValuespin)
  ON_CBN_SELCHANGE(IDC_STACKLIST, OnCbnSelchangeStacklist)
  ON_BN_CLICKED(IDC_EDITMATRIX, OnBnClickedEditmatrix)
  ON_BN_CLICKED(IDC_IRRADIANCE, OnBnClickedIrradiance)
  ON_CBN_EDITUPDATE(IDC_UVSOURCE, OnCbnSelchangeUvsource)
  ON_CBN_EDITUPDATE(IDC_SOURCENAME, OnCbnSelchangeSourcename)
  ON_BN_CLICKED(IDC_ENABLE, OnBnClickedEnable)
  ON_BN_CLICKED(IDC_INSERTTRANSFORM, OnBnClickedInserttransform)
  ON_BN_CLICKED(IDC_DELETETRANSFORM, OnBnClickedDeletetransform)
  ON_CBN_SELCHANGE(IDC_UVSOURCE, OnCbnSelectchangeUvsource)
  ON_CBN_SELCHANGE(IDC_SOURCENAME, OnCbnSelectchangeSourcename)
  ON_BN_CLICKED(IDC_DELSTAGE, OnBnClickedDelstage)
  ON_EN_CHANGE(IDC_STAGENUMBER, OnEnChangeStagenumber)
  ON_MESSAGE(WM_COPY, OnWmCopy)
  ON_MESSAGE(WM_PASTE, OnWmPaste)
  ON_STN_DBLCLK(IDC_EDITNAME, OnStnDblclickEditname)
  ON_EN_KILLFOCUS(IDC_STAGENAMEEDIT, OnEnKillfocusStagenameedit)
  ON_CBN_EDITUPDATE(IDC_STAGEFILTER, OnCbnEditupdateStagefilter)
  ON_CBN_SELENDOK(IDC_STAGEFILTER, OnCbnEditupdateStagefilter)
END_MESSAGE_MAP()


// DlgStageCfg message handlers

class InitStageSource: public MassProcess
  {
  public:
    CComboBox *target;
    bool ProcessFile(const char *filename);
  };

bool InitStageSource::ProcessFile(const char *filepath)
  {
  const char *filename=Pathname::GetNameFromPath(filepath);
  target->AddString(CString(StrRes[IDS_PREVIEWSTR])+filename);
  return true;
  }

BOOL DlgStageCfg::OnInitDialog()
  {
  CDialog::OnInitDialog();

  wTransformOper.AddString(StrRes[IDS_TRNS_POSX]);
  wTransformOper.AddString(StrRes[IDS_TRNS_POSY]);
  wTransformOper.AddString(StrRes[IDS_TRNS_SCALEX]);
  wTransformOper.AddString(StrRes[IDS_TRNS_SCALEY]);
  wTransformOper.AddString(StrRes[IDS_TRNS_ROTATEZ]);
  wTransformOper.AddString(StrRes[IDS_TRNS_SCALE]);  
  wTransformOper.AddString(StrRes[IDS_TRNS_SKEWX]);  
  wTransformOper.AddString(StrRes[IDS_TRNS_SKEWY]);  


  CheckDlgButton(IDC_ENABLE,1);
  ResetMatrix();


  InitStageSource initStage;
  initStage.target=&wStageSource;
  initStage.ProcessFolders(theApp.config.vTemplatePath,"*.paa",false);

  wStageSource.AddString(StrRes[IDS_PROCEDURALTEX]);
  wStageSource.AddString(StrRes[IDS_BROWSEFILE]);
  wStageSource.AddString(StrRes[IDS_IRRADIANCE]);
  wStageSource.AddString(StrRes[IDS_FRESNEL]);
  wStageSource.AddString(StrRes[IDS_FRESNEL_FCE]);

  SetTexGenCount(_nTexGen);
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }
void DlgStageCfg::SetTexGenCount(int _ncount)
  {
  _nTexGen=_ncount;
  wUVSource.ResetContent();
  bool ena=_nTexGen==-1;
  wTransformOper.EnableWindow(ena);
  wTransformStack.EnableWindow(ena);
  wInsertTransform.EnableWindow(ena);
  wDeleteTransform.EnableWindow(ena);
  wTransformValue.EnableWindow(ena);
  wTransformValueSpin.EnableWindow(ena);
  wEditMatrix.EnableWindow(ena);
  if (!ena)
    {
    wTexSource.SetWindowTextA(_T("TexGen"));
    for (int i=0;i<_nTexGen;++i)
      {
      CString text;
      text.Format(_T("%d"),i);
      wUVSource.AddString(text);
      }
    }
  else
    {
    wTexSource.SetWindowTextA(_T("UV Source:"));
    wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX]);
    wUVSource.AddString(StrRes[IDS_UVSOURCE_NONE]);
    wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX1]);
    wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX2]);
    wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX3]);
    wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX4]);
    wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX5]);
    wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX6]);
    wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX7]);
    wUVSource.AddString(StrRes[IDS_UVSOURCE_TEX8]);
    wUVSource.SetWindowText(StrRes[IDS_UVSOURCE_NONE]);  
    }
  };
void DlgStageCfg::ResetMatrix(void)
  {
  _initialMatrix=_resultMatrix;
  wTransformOper.SetCurSel(-1);
  wTransformStack.ResetContent();
  wTransformStack.AddString(StrRes[IDS_TRNS_IDENTITY]);
  wTransformValue.SetWindowText("0");
  wTransformStack.SetCurSel(0);
  }

void DlgStageCfg::SaveTransform(void)
  {
  char buff[50];
  if (wTransformOper.GetCurSel()<0 || !_trnsUpdate) return;
  int cursel=wTransformStack.GetCurSel();
  if (cursel+1==wTransformStack.GetCount())
    {
    wTransformStack.AddString(StrRes[IDS_TRNS_IDENTITY]);
    }
  wTransformOper.GetLBText(wTransformOper.GetCurSel(),buff);
  wTransformStack.InsertString(cursel,buff);
  wTransformStack.SetCurSel(cursel);
  wTransformValue.GetWindowText(buff,50);
  float value=(float)atof(buff);
  wTransformStack.SetItemData(cursel,*(DWORD_PTR *)&value);
  wTransformStack.DeleteString(cursel+1);
  _matrixValid=false;
  GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
  }

void DlgStageCfg::OnEnChangeValue()
  {
  SaveTransform();
  GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
  }

void DlgStageCfg::OnCbnSelchangeOperation()
  {
  SaveTransform();
  GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
  }

static int DetectOperation(const char *buffer)
  {
  for (int i=IDS_TRNS_POSX;i<IDS_TRNS_SKEWZ;i++)
    {
    if (_stricmp(buffer,StrRes[i])==0) return i;
    }
  return 0;
  }

void DlgStageCfg::OnDeltaposValuespin(NMHDR *pNMHDR, LRESULT *pResult)
  {
  LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
  char buff[50];
  wTransformOper.GetLBText(wTransformOper.GetCurSel(),buff);
  int oper=DetectOperation(buff);
  float offset;
  switch (oper)
    {
    case IDS_TRNS_POSX:
    case IDS_TRNS_POSY:
    case IDS_TRNS_POSZ:
    case IDS_TRNS_SKEWX:
    case IDS_TRNS_SKEWY:
    case IDS_TRNS_SKEWZ:
      offset=0.01f;
      break;
    case IDS_TRNS_SCALEX:
    case IDS_TRNS_SCALEY:
    case IDS_TRNS_SCALEZ:
    case IDS_TRNS_SCALE:
      offset=0.1f;
      break;
    case IDS_TRNS_ROTATEX:
    case IDS_TRNS_ROTATEY:
    case IDS_TRNS_ROTATEZ:
      offset=1.0f;
      break;
    default: offset=0.1f;
      break;
    }
  wTransformValue.GetWindowText(buff,50);
  float value=(float)atof(buff);
  value-=pNMUpDown->iDelta*offset;
  sprintf(buff,"%.2f",value);
  wTransformValue.SetWindowText(buff);

  *pResult = 0;
  }

void DlgStageCfg::OnCbnSelchangeStacklist()
  {
  char buff[50];
  _trnsUpdate=false;
  int cursel=wTransformStack.GetCurSel();
  wTransformStack.GetLBText(cursel,buff);
  DWORD_PTR data=wTransformStack.GetItemData(cursel);
  float value=*(float *)&data;
  wTransformOper.SetCurSel(-1);
  wTransformOper.SelectString(-1,buff);
  sprintf(buff,"%.2f",value);
  wTransformValue.SetWindowText(buff);
  _trnsUpdate=true;
  }

const geMatrix4 & DlgStageCfg::GetTransform(void)
  {
  if (!_matrixValid)
    {
    geMatrix4 temp=_initialMatrix;
    char buff[50];
    int cnt=wTransformStack.GetCount();
    for (int i=0;i<cnt;i++)
      {
      wTransformStack.GetLBText(i,buff);
      DWORD_PTR data=wTransformStack.GetItemData(i);
      float value=*(float *)&data;
      int op=DetectOperation(buff);
      switch (op)
        {
        case IDS_TRNS_POSX: temp.TranslateC(value,0,0);break;
        case IDS_TRNS_POSY: temp.TranslateC(0,value,0);break;
        case IDS_TRNS_POSZ: temp.TranslateC(0,0,value);break;
        case IDS_TRNS_SCALE: temp.ScaleC(value,value,value);break;
        case IDS_TRNS_SCALEX: temp.ScaleC(value,1,1);break;
        case IDS_TRNS_SCALEY: temp.ScaleC(1,value,1);break;
        case IDS_TRNS_SCALEZ: temp.ScaleC(1,1,value);break;
        case IDS_TRNS_ROTATEX: temp.RotateC(geAX,value*3.14159265f/180.0f);break;
        case IDS_TRNS_ROTATEY: temp.RotateC(geAY,value*3.14159265f/180.0f);break;
        case IDS_TRNS_ROTATEZ: temp.RotateC(geAZ,value*3.14159265f/180.0f);break;
        case IDS_TRNS_SKEWX: 
        case IDS_TRNS_SKEWY: 
          {
          geMatrix4 mx(true);
          if (op==IDS_TRNS_SKEWX) mx.a12=value;
          else mx.a21=value;
          temp.MultC(mx);
          }
        break;
        default: //currently unsupported 
          break;
        }
      }
    _resultMatrix=temp;
    _matrixValid=true;
    }
  return _resultMatrix;
  }

void DlgStageCfg::OnBnClickedEditmatrix()
  {
  const geMatrix4 &mx=GetTransform();
  DlgEditMatrix dlg;
  dlg.vMatrix=mx;
  if (dlg.DoModal()==IDOK)
    {
    _resultMatrix=dlg.vMatrix;
    ResetMatrix();
    _matrixValid=true;
    GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
    }
  }

void DlgStageCfg::OnBnClickedIrradiance()
  {
  DlgIrradiance dlg;  
  dlg._irrString=vStageSource;
  if (dlg.DoModal()==IDOK)
    {
    CString temp;
    vStageSource=dlg._irrString;    
    wStageSource.SetWindowText(vStageSource);
    SetDlgItemText(IDC_IRRADIANCE,StrRes[IDS_IRRADIANCEBUTTON]+vStageSource);
    GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
    }
  }

void DlgStageCfg::OnBnClickedFresnelFce()
  {
  DlgFresnelFce dlg;  
  dlg._frnString=vStageSource;
  if (dlg.DoModal()==IDOK)
    {
    CString temp;
    vStageSource=dlg._frnString;    
    wStageSource.SetWindowText(vStageSource);
    //SetDlgItemText(IDC_IRRADIANCE,StrRes[IDS_IRRADIANCEBUTTON]+vStageSource);
    GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
    }
  }


void DlgStageCfg::SetTexture(const char *texture)
  {
  wStageSource.SetWindowText(texture);
  int len=strlen(texture);
  wStageSource.SetEditSel(len,len);  
  int p=wStageSource.FindStringExact(-1,texture);
  if (p!=-1) wStageSource.DeleteString(p);
  wStageSource.InsertString(0,texture);
  vStageSource=texture;
  SetDlgItemText(IDC_IRRADIANCE,StrRes[IDS_IRRADIANCEBUTTON]+vStageSource);
  }

void DlgStageCfg::SetUVSource(const char *source)
  {  
  int i=wUVSource.FindStringExact(-1,source);
  if (i==-1) wUVSource.AddString(source);
  wUVSource.SetWindowText(source);
  OnCbnSelchangeUvsource();
  }

CString DlgStageCfg::GetTexture()
  {
  return vStageSource;
  }

CString DlgStageCfg::GetUVSource()
  {
  CString out;
  wUVSource.GetWindowText(out);
  return out;
  }


void DlgStageCfg::OnCbnSelchangeUvsource()
  {
  if (_nTexGen==-1)
    {
    CString s;
    wUVSource.GetWindowText(s);
    bool matrixenable=(s!=StrRes[IDS_UVSOURCE_NONE]);
    GetDlgItem(IDC_EDITMATRIX)->EnableWindow(matrixenable);
    wTransformValue.EnableWindow(matrixenable);
    wTransformStack.EnableWindow(matrixenable);
    wTransformOper.EnableWindow(matrixenable);
    }
  GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
  }

void DlgStageCfg::OnCbnSelchangeSourcename()
  {
  CString s;
  wStageSource.GetWindowText(s);
  if (strcmp(s,StrRes[IDS_PROCEDURALTEX])==0)
    {
    DlgProceduralTex dlg;
    dlg.vCode=vStageSource;
    if (dlg.DoModal()==IDOK)
      wStageSource.SetWindowText(dlg.vCode);
    else
      wStageSource.SetWindowText(vStageSource);    
    }  
  else if (strcmp(s,StrRes[IDS_BROWSEFILE])==0)
    {
    OnBnClickedBrowsetex();
    wStageSource.SetWindowText(vStageSource);
    }
  else if (strcmp(s,StrRes[IDS_IRRADIANCE])==0)
    {
    OnBnClickedIrradiance();
    wStageSource.SetWindowText(vStageSource);
    }
  else if (strcmp(s,StrRes[IDS_FRESNEL_FCE])==0)
    {
    OnBnClickedFresnelFce();
    wStageSource.SetWindowText(vStageSource);
    }
  const char *prevstr=StrRes[IDS_PREVIEWSTR];
  int prevstrlen=strlen(prevstr);
  wStageSource.GetWindowText(s);
  if (strncmp(s,StrRes[IDS_PREVIEWSTR],strlen(StrRes[IDS_PREVIEWSTR])))
    {
    vStageSource=s;
    if (vStageSource.GetLength()>2 && (vStageSource[0]=='#' || strncmp(prevstr,vStageSource,prevstrlen)==0 || (vStageSource[1]==':' && _access(vStageSource,0)==0) || (_access(theApp.CallGen().GetProjectRoot()+vStageSource,0)==0)))
      GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
    }
  else
    GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
  }


void DlgStageCfg::OnBnClickedEnable()
  {
  GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
  }

void DlgStageCfg::OnBnClickedBrowsetex()
  {
  CString name=vStageSource;     
  if (name.GetLength()>2 && name[1]!=':') name=theApp.CallGen().GetProjectRoot()+name;
  CFileDialog fdlg(TRUE,NULL,name,OFN_HIDEREADONLY|OFN_FILEMUSTEXIST,StrRes[IDS_TEXTUREFILTES],this);
  fdlg.m_ofn.lpstrInitialDir=((Pathname *)(GetParent()->SendMessage(MATMSG_GETMATERIALNAME,0,0)))->GetDirectoryWithDrive();
  fdlg.m_ofn.lpstrTitle=StrRes[IDS_SELECTTEXTURETITLE];
  const char *result;
  int res=fdlg.DoModal();  
  if (res==-1) 
    {
    fdlg.m_pOFN->lpstrFile[0]=0;
    res=fdlg.DoModal();
    }
  if (res==IDOK)
    {
    Pathname path=fdlg.GetPathName();
    if (_stricmp(path.GetExtension(),".rvmat")==0)
      {
      ParamFile pfile;
      pfile.Parse(path);
      bool first=true;
      for (int i=0;i<pfile.GetEntryCount();i++)
        {
        const ParamEntry &entry=pfile.GetEntry(i);
        if (entry.IsClass())
          {
          ParamEntryPtr value=entry.FindEntry("texture");
          if (value) 
            {
            Pathname finalvalue=value->GetValue();
            SetTexture(finalvalue);
            }
          }
        }
      }
    else
      {
      Pathname finalvalue=fdlg.GetPathName();;
      finalvalue.FullToRelative(Pathname(theApp.CallGen().GetProjectRoot()));
      SetTexture(finalvalue);
      }
    }
  }

void DlgStageCfg::OnBnClickedInserttransform()
  {
  int curSel=wTransformStack.GetCurSel();
  wTransformStack.InsertString(curSel,StrRes[IDS_TRNS_IDENTITY]);
  wTransformStack.SetCurSel(curSel);
  OnCbnSelchangeStacklist();
  }

void DlgStageCfg::OnBnClickedDeletetransform()
  {
  int curSel=wTransformStack.GetCurSel();
  if (curSel+1>=wTransformStack.GetCount()) return;
  wTransformStack.DeleteString(curSel);
  if (wTransformStack.GetCount()==0)
    OnBnClickedInserttransform();
  wTransformStack.SetCurSel(curSel?curSel-1:curSel);
  OnCbnSelchangeStacklist();
  }

bool DlgStageCfg::IsStageEnabled()
  {
  return IsDlgButtonChecked(IDC_ENABLE)!=0;
  }

void DlgStageCfg::LockStageSetup(bool lock,bool irradiance)
  {
  //wUVSource.EnableWindow(!lock);
    LockUVSource(lock||_lockUVSource);
  CEdit edit;
  edit.Attach(::GetDlgItem(*this,IDC_STAGENUMBER));
  edit.SetReadOnly(lock);
  edit.Detach();
  GetDlgItem(IDC_ENABLE)->EnableWindow(!lock);
  wStageSource.ShowWindow(!irradiance || !lock?SW_SHOW:SW_HIDE);
  GetDlgItem(IDC_IRRADIANCE)->ShowWindow(irradiance && lock?SW_SHOW:SW_HIDE);  
  }

void DlgStageCfg::LockDelButton(bool lock)
  {
  GetDlgItem(IDC_DELSTAGE)->EnableWindow(!lock);
  }
void DlgStageCfg::LockUVSource(bool lock)
  {
  wUVSource.EnableWindow(!lock);
  _lockUVSource=lock;
  }

void DlgStageCfg::OnCbnSelectchangeUvsource()
  {
  PostMessage(WM_COMMAND,MAKELPARAM(IDC_UVSOURCE,CBN_EDITUPDATE),(LPARAM)wUVSource.GetSafeHwnd());
  }

void DlgStageCfg::OnCbnSelectchangeSourcename()
  {
  PostMessage(WM_COMMAND,MAKELPARAM(IDC_SOURCENAME,CBN_EDITUPDATE),(LPARAM)wStageSource.GetSafeHwnd());
  }

void DlgStageCfg::OnBnClickedDelstage()
  {
  GetParent()->PostMessage(MATMSG_DELSTAGE,0,(LPARAM)this);
  }

void DlgStageCfg::OnEnChangeStagenumber()
  {
  _number=GetDlgItemInt(IDC_STAGENUMBER,NULL,FALSE);
  GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);
  }

CString DlgStageCfg::GetTextureEx(bool preview)
  {
  CString s;
  if (preview)
    {
    wStageSource.GetWindowText(s);
    int len=strlen(StrRes[IDS_PREVIEWSTR]);
    if (strncmp(s,StrRes[IDS_PREVIEWSTR],len)==0)
      {
      Pathname path;
      path.SetDirectory(theApp.config.vTemplatePath);
      path.SetFilename((LPCTSTR)s+len);
      s=path;
      }
    }
  else
    s=vStageSource;
  if (s.GetLength() && s[0]=='\\') s.Delete(0,1);
  return s;
  }

LRESULT DlgStageCfg::OnWmCopy(WPARAM wParam, LPARAM lParam)
  {
  ArchiveStreamMemoryOut memout;
  ArchiveSimpleBinary arch(memout);
  Serialize(arch);  
  if (OpenClipboard()==FALSE) 
    {
    AfxMessageBox(IDS_UNABLETOOPENCLIPBOARD,MB_OK|MB_ICONEXCLAMATION);
    return 0;
    }
  EmptyClipboard();
  HGLOBAL glob=GlobalAlloc(GMEM_MOVEABLE|GMEM_DDESHARE,memout.GetBufferSize());
  void *data=GlobalLock(glob);
  memcpy(data,memout.GetBuffer(),memout.GetBufferSize());
  GlobalUnlock(glob);  
  SetClipboardData(_clipboardFormat,glob);

  ParamFile pf;
  ArchiveParamFile2Out pfarch(pf);
  Serialize(pfarch);
  QOStrStream outbuf;
  pf.Save(outbuf,0);
  glob=GlobalAlloc(GMEM_MOVEABLE|GMEM_DDESHARE,outbuf.tellp()-1);
  data=GlobalLock(glob);
  memcpy(data,outbuf.str(),outbuf.tellp()-1);
  GlobalUnlock(glob);  
  SetClipboardData(CF_TEXT,glob);  
  CloseClipboard();

  return 1;
  }

LRESULT DlgStageCfg::OnWmPaste(WPARAM wParam, LPARAM lParam)
  {
  if (OpenClipboard()==FALSE)
    {
    AfxMessageBox(IDS_UNABLETOOPENCLIPBOARD,MB_OK|MB_ICONEXCLAMATION);
    return 1;
    }
  HGLOBAL glob=GetClipboardData(_clipboardFormat);
  if (glob!=NULL)
    {
    char *data=(char *)GlobalLock(glob);    
    ArchiveStreamMemoryIn memin(data,GlobalSize(glob),false);
    ArchiveSimpleBinary arch(memin);
    Serialize(arch);
    GlobalUnlock(glob);  
    }
  CloseClipboard();
  return 1;
  }


void DlgStageCfg::Serialize(IArchive &arch)
  {
  CString s;
  if (+arch) s=this->GetTexture();
  arch("texture",s);
  if (-arch) SetTexture(s);
  if (+arch) s=this->GetUVSource();
  arch("tuvSource",s);
  if (-arch) SetUVSource(s);
  if (+arch) s=this->GetStageFilter();
  arch("stageFilter",s);
  if (-arch) SetStageFilter(s);
  ArchiveSection asect(arch);
  while (asect.Block("transformStack"))
    {
    int cnt=wTransformStack.GetCount();
    arch("count",cnt);
    if (-arch) wTransformStack.ResetContent();
    for (int i=0;i<cnt;i++)
      {
      float val;
      if (+arch) 
        {
        wTransformStack.GetLBText(i,s);
        unsigned long uval=wTransformStack.GetItemData(i);
        val=*(float *)(&uval);
        }
      ArchiveSection asect2(arch);
      arch(s);
      arch(val);
      if (-arch)
        {
        int p=wTransformStack.AddString(s);
        wTransformStack.SetItemData(p,*(unsigned long *)(&val));
        }
      }
    }
  while (asect.Block("aside"))
    {for (int j=0;j<3;j++) arch(_initialMatrix[0][j]);}
  while (asect.Block("up"))
    {for (int j=0;j<3;j++) arch(_initialMatrix[1][j]);}
  while (asect.Block("dir"))
    {for (int j=0;j<3;j++) arch(_initialMatrix[2][j]);}
  while (asect.Block("pos"))
    {for (int j=0;j<3;j++) arch(_initialMatrix[3][j]);  }
  }

void DlgStageCfg::SerializeLock(IArchive &arch)
  {
  bool lock=wUVSource.IsWindowEnabled()==FALSE;
  bool irrad=wStageSource.IsWindowEnabled()==FALSE;
  bool defbut=GetDlgItem(IDC_DELSTAGE)->IsWindowEnabled()==FALSE;
  if (arch("locked",lock) && arch("irradiance",irrad) && -arch)
    LockStageSetup(lock,irrad);
  if (arch("lockedDel",defbut) && -arch)
    this->LockDelButton(defbut);
  arch("StageNumber",_number);
  if (-arch) SetDlgItemInt(IDC_STAGENUMBER,_number,FALSE);
  }

void DlgStageCfg::OnStnDblclickEditname()
  {
  CString s;
  GetDlgItemText(IDC_STAGENAMEFRM,s);
  wStageName.SetWindowText(s);
  wStageName.ShowWindow(SW_SHOW);
  wStageName.BringWindowToTop();
  wStageName.SetFocus();  
  }

void DlgStageCfg::OnEnKillfocusStagenameedit()
  {
  CString s;
  wStageName.ShowWindow(SW_HIDE);
  wStageName.GetWindowText(s);
  SetDlgItemText(IDC_STAGENAMEFRM,s);
  }

void DlgStageCfg::SetStageName(const char *name)
  {
  SetDlgItemText(IDC_STAGENAMEFRM,name);
  }

CString DlgStageCfg::GetStageName()
  {
  CString name;
  GetDlgItemText(IDC_STAGENAMEFRM,name);
  return name;
  }

void DlgStageCfg::SetStageFilter(const char *name)
{
  wStageFilter.SetWindowText(name);
}

CString DlgStageCfg::GetStageFilter()
{
  CString s;
  wStageFilter.GetWindowText(s);
  return s;
}
void DlgStageCfg::OnCbnEditupdateStagefilter()
{
  GetParent()->PostMessage(MATMSG_STAGECHANGED,0,0);  
}

void DlgStageCfg::ScaveFiles(BTree<RStringI> &scavenger)
{
  CString x;
  wStageSource.GetWindowText(x);
  scavenger.Add(RString(x));
  for (int i=0,cnt=wStageSource.GetCount();i<cnt;i++)
  {
    wStageSource.GetLBText(i,x);
    RStringI t=x;
    if (!scavenger.Find(t)) scavenger.Add(t);
  }
}

void DlgStageCfg::RecorverFiles(const BTree<RStringI> &scavenger)
{
  wStageSource.ResetContent();
  BTreeIterator<RStringI> iter(scavenger);
  RString *it;
  while ((it=iter.Next())!=0)
  {
    if(!it->IsEmpty()) 
      wStageSource.AddString(*it);
  }
  if (!scavenger.Find(StrRes[IDS_PROCEDURALTEX])) wStageSource.AddString(StrRes[IDS_PROCEDURALTEX]);
  if (!scavenger.Find(StrRes[IDS_BROWSEFILE]))    wStageSource.AddString(StrRes[IDS_BROWSEFILE]);
  if (!scavenger.Find(StrRes[IDS_IRRADIANCE]))    wStageSource.AddString(StrRes[IDS_IRRADIANCE]);
  if (!scavenger.Find(StrRes[IDS_FRESNEL]))       wStageSource.AddString(StrRes[IDS_FRESNEL]);
  if (!scavenger.Find(StrRes[IDS_FRESNEL_FCE]))   wStageSource.AddString(StrRes[IDS_FRESNEL_FCE]);
}