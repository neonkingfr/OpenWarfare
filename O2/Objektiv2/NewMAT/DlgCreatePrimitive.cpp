// DlgCreatePrimitive.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgCreatePrimitive.h"
#include ".\dlgcreateprimitive.h"


// DlgCreatePrimitive dialog

IMPLEMENT_DYNAMIC(DlgCreatePrimitive, CDialog)
DlgCreatePrimitive::DlgCreatePrimitive(CWnd* pParent /*=NULL*/)
	: CDialog(DlgCreatePrimitive::IDD, pParent)
    , vSize(0)
  {
}

DlgCreatePrimitive::~DlgCreatePrimitive()
{
}

void DlgCreatePrimitive::DoDataExchange(CDataExchange* pDX)
{
CDialog::DoDataExchange(pDX);
DDX_Control(pDX, IDC_PRIMITIVETYPE, wPrimitive);
DDX_Control(pDX, IDC_QUALITY, wQuality);
DDX_Control(pDX, IDC_SCALE, wSize);
}


BEGIN_MESSAGE_MAP(DlgCreatePrimitive, CDialog)
END_MESSAGE_MAP()


// DlgCreatePrimitive message handlers

BOOL DlgCreatePrimitive::OnInitDialog()
  {
  CDialog::OnInitDialog();

  wPrimitive.SetItemData(wPrimitive.AddString(StrRes[IDS_PRIMSPEHERE]),IMatAppSide::PrimSpehere);
  wPrimitive.SetItemData(wPrimitive.AddString(StrRes[IDS_PRIMPLANE]),IMatAppSide::PrimPlane);
  wPrimitive.SetItemData(wPrimitive.AddString(StrRes[IDS_PRIMBOX]),IMatAppSide::PrimBox);
  wPrimitive.SetItemData(wPrimitive.AddString(StrRes[IDS_PRIMCYLINDER]),IMatAppSide::PrimCylinder);

  wQuality.SetRange(1,10);
  wQuality.SetPos(quality);
  wSize.SetRange(1,5);
  wSize.SetPos(vSize);
  int cnt=wPrimitive.GetCount();
  wPrimitive.SetCurSel(0);
  for (int i=0;i<cnt;i++)
    if (wPrimitive.GetItemData(i)==primType) wPrimitive.SetCurSel(i);

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

void DlgCreatePrimitive::OnOK()
  {
  quality=wQuality.GetPos();
  vSize=wSize.GetPos();
  primType=(IMatAppSide::ObjPrimitiveType)wPrimitive.GetItemData(wPrimitive.GetCurSel());
  CDialog::OnOK();
  }

