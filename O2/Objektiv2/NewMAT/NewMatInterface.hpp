#ifndef __NEWMATINTERFACE_H_
#pragma once

#include "../rocheck.h"
#include "../IPluginGeneral.h"

class IMatGeneralAppSide;
class SccFunctions;
namespace ObjektivLib{
    class ObjectData;
    class IExternalViewer;
}
class CCmdUI;
class CWinApp;

class IMatAppSide;
class IMatPluginSide
  {
  public:
    virtual void Init(IMatAppSide *appInterface)=0;     //vola jednou po inicializaci
    virtual void EditMaterial(const char *filename)=0;       //Spusti editor s materialem. 
    virtual bool CreateMaterial(const char *refFolder)=0;
    virtual const char *GetActiveMaterial()=0;          //vrac� jm�no materi�lu, kter� se edituje
    virtual void AfterPrepareModelForViewer(const ObjektivLib::ObjectData &src, 
        ObjektivLib::ObjectData &trg)=0;
  };



class IMatAppSide
  {
  public:
    enum ObjPrimitiveType {PrimSpehere,PrimBox,PrimPlane,PrimCylinder,PrimToroid};
    virtual void UseMaterial(const char *filename)=0;      //vola pluhin pokud chce aby material byl pouzit na vybrane selekci
    virtual bool CanUseMaterial()=0;                 //vraci true, pokud lze pouzit funkci UseMaterial
    virtual ObjektivLib::ObjectData *CreatePrimitive(const char *filename, const char *texture, const char *material)=0;
    virtual ObjektivLib::ObjectData *CreatePrimitive(ObjPrimitiveType type, int segments,float scale, const char *texture,  const char *material)=0;
    virtual void DestroyPrimitive(ObjektivLib::ObjectData *obj)=0;
    virtual void MatWindowClosed() {} //plugin informuje klientskou aplikaci, �e okno MATu se zav�elo
  };

#endif