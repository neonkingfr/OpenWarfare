#pragma once
#include "../geMatrix4.h"



// DlgEditMatrix dialog

class DlgEditMatrix : public CDialog
{
	DECLARE_DYNAMIC(DlgEditMatrix)

public:
    geMatrix4 vMatrix;  
  
	DlgEditMatrix(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgEditMatrix();

// Dialog Data
	enum { IDD = IDD_EDITMATRIX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  void SetItemFloat(int idc, float value);
  float GetItemFloat(int idc);
  virtual BOOL OnInitDialog();
protected:
  virtual void OnOK();
};
