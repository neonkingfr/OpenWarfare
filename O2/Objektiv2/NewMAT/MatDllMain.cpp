#include "stdafx.h"

extern "C"
{
BOOL WINAPI _DllMainCRTStartup(HANDLE  hDllHandle, DWORD   dwReason, LPVOID  lpreserved);
}

HINSTANCE hDllInstance;

BOOL WINAPI MatDllMain(HANDLE  hDllHandle, DWORD   dwReason, LPVOID  lpreserved)
{
  hDllInstance=(HINSTANCE)hDllHandle;
  if (dwReason==DLL_PROCESS_DETACH || dwReason==DLL_PROCESS_ATTACH) return TRUE;
  else return _DllMainCRTStartup(hDllHandle, dwReason, lpreserved);
}

extern "C"
{
  extern void *_imp__malloc;
  extern void *_imp__free;
};

BOOL ProcessDllInit(void *ptr_malloc, void *ptr_free)
{
  return _DllMainCRTStartup(hDllInstance,DLL_PROCESS_ATTACH,0);
}

BOOL ProcessDllShutdown()
{
  return _DllMainCRTStartup(hDllInstance,DLL_PROCESS_DETACH,0);
}

