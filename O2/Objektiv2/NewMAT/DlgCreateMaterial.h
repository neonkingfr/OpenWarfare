#pragma once

#include "MassProcess.h"
#include "afxwin.h"


// DlgCreateMaterial dialog

class DlgCreateMaterial : public CDialog, public MassProcess
{
	DECLARE_DYNAMIC(DlgCreateMaterial)

public:
	DlgCreateMaterial(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgCreateMaterial();

// Dialog Data
	enum { IDD = IDD_CREATEMATERIAL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

    virtual bool ProcessFile(const char *filename);

	DECLARE_MESSAGE_MAP()
public:
  CString vMatName;
  CString vTemplateName;
  virtual BOOL OnInitDialog();
  CComboBox wTemplateName;
  CString vFolder;
  CEdit wFolder;
};
