// DlgTemplateManager.cpp : implementation file
//

#include "stdafx.h"
#include "NewMAT.h"
#include "DlgTemplateManager.h"
#include ".\dlgtemplatemanager.h"
#include <el/interfaces/iScc.hpp>


// DlgTemplateManager dialog

IMPLEMENT_DYNAMIC(DlgTemplateManager, CDialog)
DlgTemplateManager::DlgTemplateManager(CWnd* pParent /*=NULL*/)
	: CDialog(DlgTemplateManager::IDD, pParent)
    , vTemplateName(_T(""))
  {
}

DlgTemplateManager::~DlgTemplateManager()
{
}

void DlgTemplateManager::DoDataExchange(CDataExchange* pDX)
{
CDialog::DoDataExchange(pDX);
DDX_Control(pDX, IDC_TEMPLATENAME, wTemplateName);
DDX_CBString(pDX, IDC_TEMPLATENAME, vTemplateName);
}


BEGIN_MESSAGE_MAP(DlgTemplateManager, CDialog)
  ON_BN_CLICKED(IDC_DELETETEMPLATE, OnBnClickedDeletetemplate)
END_MESSAGE_MAP()


// DlgTemplateManager message handlers

BOOL DlgTemplateManager::OnInitDialog()
  {
  CDialog::OnInitDialog();


  ProcessFolders(theApp.config.vTemplatePath,"*.rvmat",false);
  // TODO:  Add extra initialization here

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }


bool DlgTemplateManager::ProcessFile(const char *pathname)
  {
  const char *filename=Pathname::GetNameFromPath(pathname);
  char *c=strcpy((char *)alloca(strlen(filename)+1),filename);
  char *z=strrchr(c,'.');
  if (z) *z=0;
  wTemplateName.AddString(c);
  return true;
  }

void DlgTemplateManager::OnBnClickedDeletetemplate()
  {
  CString filename;
  wTemplateName.GetWindowText(filename);
  if (!filename.GetLength()) 
    {
    AfxMessageBox(IDS_INVALID_FILENAME);
    return;
    }
  Pathname fpath;
  fpath.SetDirectory(theApp.config.vTemplatePath);
  fpath.SetFiletitle(filename);
  fpath.SetExtension(".rvmat");
  if (!fpath.IsPathValid()) 
    AfxMessageBox(IDS_INVALID_FILENAME);
  else 
    {
    CString msg;
    msg.Format(IDS_ASKDELETETEMPLATE,filename);
    if (AfxMessageBox(msg,MB_YESNO|MB_ICONQUESTION)==IDYES)
      {
      SccFunctions *scc=theApp.CallGen().GetSccInterface();
      if (scc && scc->UnderSSControlNotDeleted(fpath))
        {        
        scc->Remove(fpath);
        SetFileAttributes(fpath,GetFileAttributes(fpath) & ~FILE_ATTRIBUTE_READONLY);
        DeleteFile(fpath);
        }
      else
        DeleteFile(fpath);
      }
    }
  int pos=wTemplateName.FindStringExact(-1,filename);
  if (pos>=0) wTemplateName.DeleteString(pos);
  UpdateData(FALSE);
  }

void DlgTemplateManager::OnOK()
  {
  UpdateData(TRUE);
  vOutPath.SetDirectory(theApp.config.vTemplatePath);
  vOutPath.SetFiletitle(vTemplateName);
  vOutPath.SetExtension(".rvmat"); 
  if (!vOutPath.IsPathValid())
    AfxMessageBox(IDS_INVALID_FILENAME);
  else
    CDialog::OnOK();
  }
