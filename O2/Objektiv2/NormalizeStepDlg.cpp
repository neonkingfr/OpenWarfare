// NormalizeStepDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "NormalizeStepDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNormalizeStepDlg dialog


CNormalizeStepDlg::CNormalizeStepDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNormalizeStepDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNormalizeStepDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CNormalizeStepDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNormalizeStepDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNormalizeStepDlg, CDialog)
	//{{AFX_MSG_MAP(CNormalizeStepDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNormalizeStepDlg message handlers
