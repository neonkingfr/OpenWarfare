// NamedPropDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "MainFrm.h"
#include "NamedPropDlg.h"
#include "EditPropDlg.h"
#include "ComInt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNamedPropDlg dialog


CNamedPropDlg::CNamedPropDlg()
: CDialogBar()
{
  //{{AFX_DATA_INIT(CNamedPropDlg)
  // NOTE: the ClassWizard will add member initialization here
  //}}AFX_DATA_INIT
}

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CNamedPropDlg, CDialogBar)
  //{{AFX_MSG_MAP(CNamedPropDlg)
  ON_NOTIFY(NM_DBLCLK, IDC_LIST1, OnDblclkList1)
  ON_NOTIFY(NM_RCLICK, IDC_LIST1, OnRclickList1)
  ON_COMMAND(ID_NAMPROP_NEW, OnNampropNew)
  ON_WM_DESTROY()
  ON_COMMAND(ID_NAMPROP_EDIT, OnNampropEdit)
  ON_COMMAND(ID_NAMPROP_DELETE, OnNampropDelete)
  ON_COMMAND_RANGE(100,1000,OnPluginPopupCommand)
  ON_WM_SIZE()
  //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNamedPropDlg message handlers

void CNamedPropDlg::Create(CWnd *parent, int menuid)  
{
  CDialogBar::Create(parent,IDD_NAMEDPROPERTY,CBRS_BOTTOM,menuid);
  SetBarStyle(GetBarStyle()| CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
  EnableDocking(CBRS_ALIGN_ANY);
  CString top;top.LoadString(IDS_NAMEDPROPERTY);
  SetWindowText(top);
  notify=parent;
  list.Attach(::GetDlgItem(m_hWnd,IDC_LIST1));
  list.SetImageList(&shrilist,LVSIL_SMALL);
  CString text;
  text.LoadString(IDS_PROPNAME);
  list.InsertColumn(0,text,LVCFMT_LEFT,120,0);
  text.LoadString(IDS_PROPVALUE);
  list.InsertColumn(1,text,LVCFMT_LEFT,300,1);
  CRect rect;GetClientRect(&rect);
  LoadBarSize(*this,160,0);
}

//--------------------------------------------------

void CNamedPropDlg::LoadProperties(LODObject *LodObject)
{
  lod=LodObject;
  ObjectData *odata=lod->Active();
  list.DeleteAllItems();
  int cnt=MAX_NAMED_PROP;
  for (int i=0;i<cnt;i++) 
  {
    const char *np=odata->GetNamedProp(i);
    if (np)
    {
      const char *val=odata->GetNamedProp(np);
      int last = list.GetItemCount(); // due "if (np)" wasn't counting on "i" exactly right
      list.InsertItem(last,np,ILST_PROPERTY);
      list.SetItemText(last,1,val);
    }
  }
}

//--------------------------------------------------

#define CINP_ADD 0
#define CINP_DELETE 1
#define CINP_EDIT 2

class CINamedProp:public CICommand
{
  CString name;
  CString text;
  CString oldname;
  int op;
public:
  CINamedProp(int oper, const char *name, const char *text, const char *oldname):
      name(name),text(text),op(oper),oldname(oldname)
      {recordable=false;}
      virtual int Execute(LODObject *lod)
      {
        ObjectData *obj=lod->Active();
        switch (op)
        {
        case CINP_EDIT:obj->DeleteNamedProp(oldname); //no break;
        case CINP_ADD: obj->SetNamedProp(name,text);break;
        case CINP_DELETE: obj->DeleteNamedProp(name);break;
        }
        return 0;
      }

};

//--------------------------------------------------

void CNamedPropDlg::OnDblclkList1(NMHDR* pNMHDR, LRESULT* pResult) 
{
  OnNampropEdit();
}

//--------------------------------------------------

void CNamedPropDlg::OnRclickList1(NMHDR* pNMHDR, LRESULT* pResult) 
{ 
  CMenu popup;	
  *pResult = 0;

  LoadPopupMenu(IDR_NAMPROP_POPUP,popup);
  int index=list.GetNextItem( -1, LVNI_FOCUSED);
  if (index==-1)
  {
    popup.EnableMenuItem(ID_NAMPROP_EDIT,MF_BYCOMMAND|MF_DISABLED|MF_GRAYED);
    popup.EnableMenuItem(ID_NAMPROP_DELETE,MF_BYCOMMAND|MF_DISABLED|MF_GRAYED);
  }
  theApp.PluginPopupHook(IPluginGeneralPluginSide::PMNamedProperties,popup,*this);
  CPoint pt;
  GetCursorPos(&pt);
  popup.TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON|TPM_LEFTBUTTON,pt.x,pt.y,this);
}

//--------------------------------------------------

void CNamedPropDlg::OnNampropNew() 
{
  CString s;
  s.LoadString(IDS_PROPUNTITLED);
  CEditPropDlg dlg;
  dlg.m_Name=s;
  dlg.m_Value="";
  if (dlg.DoModal()==IDOK)
  {
    comint.Begin(WString(IDS_EDITNAMEDPROP));
    if (comint.Run(new CINamedProp(CINP_ADD,dlg.m_Name,dlg.m_Value,NULL))==0)
    {
      int i=list.InsertItem(0,dlg.m_Name,ILST_PROPERTY);
      list.SetItemText(i,1,dlg.m_Value);
    }
  }
}

//--------------------------------------------------

void CNamedPropDlg::OnDestroy() 
{
  SaveBarSize(*this);
  list.Detach();
  CDialogBar::OnDestroy();
}

//--------------------------------------------------

void CNamedPropDlg::OnNampropEdit() 
{
  int index=list.GetNextItem( -1, LVNI_FOCUSED);
  if (index==-1) return;
  CEditPropDlg dlg;
  dlg.m_Name=list.GetItemText(index,0);
  dlg.m_Value=list.GetItemText(index,1);
  CString oldname=dlg.m_Name;
  if (dlg.DoModal()==IDOK)
  {
    comint.Begin(WString(IDS_EDITNAMEDPROP));
    if (comint.Run(new CINamedProp(CINP_EDIT,dlg.m_Name,dlg.m_Value,oldname))==0)
    {
      list.SetItemText(index,0,dlg.m_Name);
      list.SetItemText(index,1,dlg.m_Value);
    }
  }
}

//--------------------------------------------------

void CNamedPropDlg::OnNampropDelete() 
{  
  int index=list.GetNextItem( -1, LVNI_SELECTED);
  if (index==-1) return;
  CString s = GetStringRes(IDS_QPROPDELETE);

  while (index!=-1) 
  {
    s += _T("\n  ") + CString(list.GetItemText(index,0)) + _T(" = ") + CString(list.GetItemText(index,1));
    index=list.GetNextItem( index, LVNI_SELECTED);
  }

  if (AfxMessageBox(s,MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON2)==IDYES)	
  {
    comint.Begin(WString(IDS_EDITNAMEDPROP));
    index = list.GetNextItem( -1, LVNI_SELECTED);
    while (index!=-1)
    {
      CString n=list.GetItemText(index,0);
      if (comint.Run(new CINamedProp(CINP_DELETE,n,"",NULL))==0)	
        list.DeleteItem(index);	
      index=list.GetNextItem( -1, LVNI_SELECTED);
    }
  }
}

//--------------------------------------------------

void CNamedPropDlg::OnSize(UINT nType, int cx, int cy) 	
{
  if ((HWND)list)list.SetWindowPos(NULL,5,5,cx-10,cy-10,SWP_NOZORDER);
}

//--------------------------------------------------

/*
CSize CNamedPropDlg::CalcDynamicLayout(int nLength, DWORD nMode)
{
CSize &mru=m_sizeDefault;
if (nMode & LM_STRETCH) TRACE0("LM_STRETCH |");
if (nMode & LM_HORZ) TRACE0("LM_HORZ |");
if (nMode & LM_MRUWIDTH) TRACE0("LM_MRUWIDTH |");
if (nMode & LM_HORZDOCK) TRACE0("LM_HORZDOCK |");
if (nMode & LM_VERTDOCK) TRACE0("LM_VERTDOCK |");
if (nMode & LM_LENGTHY) TRACE0("LM_LENGTHY |");
if (nMode & LM_COMMIT) TRACE0("LM_COMMIT |");
TRACE1(" Length %d\n",nLength);
/*
if (nLength != -1 && nMode & 0x2)
{
CRect rect; GetWindowRect(rect);
CSize sz(rect.Width(),rect.Height());
BOOL bVert = (nMode & LM_LENGTHY);
if (bVert) sz.cy = nLength ;
else sz.cx=nLength;  	  
return sz;
}
else 
}
*/


void CNamedPropDlg::OnPluginPopupCommand(UINT cmd)
{
  theApp.HookMenuCommand(cmd);
}