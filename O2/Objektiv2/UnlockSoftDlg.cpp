// UnlockSoftDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "UnlockSoftDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUnlockSoftDlg dialog


CUnlockSoftDlg::CUnlockSoftDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CUnlockSoftDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CUnlockSoftDlg)
    codein2 = 0;
    codein1 = 0;
    codeout = _T("");
    codein3 = 0;
    codein4 = 0;
    //}}AFX_DATA_INIT
    timeexpand=false;
    forceextend=false;
    }

//--------------------------------------------------

void CUnlockSoftDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CUnlockSoftDlg)
  DDX_Text(pDX, IDC_CODEIN2, codein2);
  DDX_Text(pDX, IDC_CODEIN1, codein1);
  DDX_Text(pDX, IDC_CODEOUT, codeout);
  DDX_Text(pDX, IDC_CODEIN3, codein3);
  DDX_Text(pDX, IDC_CODEIN4, codein4);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CUnlockSoftDlg, CDialog)
  //{{AFX_MSG_MAP(CUnlockSoftDlg)
  ON_BN_CLICKED(IDC_UNLOCK, OnUnlock)
    ON_BN_CLICKED(IDC_UNLOCKNOW, OnUnlocknow)
      ON_BN_CLICKED(IDC_VISITWEB, OnVisitweb)
        ON_BN_CLICKED(IDC_ORDER, OnOrder)
          //}}AFX_MSG_MAP
          END_MESSAGE_MAP()
            
            /////////////////////////////////////////////////////////////////////////////
            // CUnlockSoftDlg message handlers
            
            BOOL CUnlockSoftDlg::OnInitDialog() 
              {
              regcode=0;
              pp_getvarnum(prot_handle,VAR_UDEF_NUM_5,&regcode);
              while (regcode==0) regcode=pp_cenum();
              pp_setvarnum(prot_handle,VAR_UDEF_NUM_5,regcode);
              codeout.Format("%d - %d",regcode,prot_componumber);	
              CDialog::OnInitDialog();
              SetDlgItemText(IDC_CODEIN1,"");
              SetDlgItemText(IDC_CODEIN2,"");
              SetDlgItemText(IDC_CODEIN3,"");
              SetDlgItemText(IDC_CODEIN4,"");
              CRect rc;
              GetWindowRect(rc);
              orgsize=rc.Size();
              GetDlgItem(IDC_SEPARATE)->GetWindowRect(rc);
              ScreenToClient(&rc);	
              SetWindowPos(NULL,0,0,orgsize.cx,rc.top+GetSystemMetrics(SM_CYCAPTION)+GetSystemMetrics(SM_CYDLGFRAME),SWP_NOMOVE|SWP_NOZORDER);
              if (forceextend) GetDlgItem(IDC_EXTENDTXT)->ShowWindow(SW_SHOW);
              else if (timeexpand) GetDlgItem(IDC_EXPIRETXT)->ShowWindow(SW_SHOW);
              else GetDlgItem(IDC_UNLOCKTXT)->ShowWindow(SW_SHOW);
              return TRUE;  // return TRUE unless you set the focus to a control
              // EXCEPTION: OCX Property Pages should return FALSE
              }

//--------------------------------------------------

void CUnlockSoftDlg::OnUnlock() 
  {
  SetWindowPos(NULL,0,0,orgsize.cx,orgsize.cy,SWP_NOMOVE|SWP_NOZORDER);
  GetDlgItem(IDC_UNLOCK)->EnableWindow(FALSE);
  GetDlgItem(IDC_CODEOUT)->EnableWindow(TRUE);
  if (!timeexpand)
    {
    GetDlgItem(IDC_CODEIN1)->EnableWindow(TRUE);
    GetDlgItem(IDC_CODEIN2)->EnableWindow(TRUE);
    }
  else
    {
    SetDlgItemInt(IDC_CODEIN1,0);
    SetDlgItemInt(IDC_CODEIN2,0);
    }
  GetDlgItem(IDC_CODEIN3)->EnableWindow(TRUE);
  GetDlgItem(IDC_CODEIN4)->EnableWindow(TRUE);
  GetDlgItem(IDC_UNLOCKNOW)->EnableWindow(TRUE);
  GetDlgItem(IDC_CODEIN1)->SetFocus();
  }

//--------------------------------------------------

void CUnlockSoftDlg::OnUnlocknow() 
  {
  if (UpdateData(TRUE)==FALSE) return;
  LONG trig;
  LONG data;
  prot_result=pp_eztrig1ex(prot_handle,codein3,codein4,0,regcode,prot_componumber,400,125,&trig,&data);
  if (pp_ndecrypt(prot_encryptcompo,prot_result)==(prot_componumber & 16383) && trig==2)
    {
    long day,month,year,dow,result;
    result=pp_setvarchar(prot_handle, VAR_EXPIRE_TYPE, "D"); //nastav demo rezim
    if (result ==PP_SUCCESS)
      {
      pp_getdate(&month,&day,&year,&dow);
      pp_adddays(&month,&day,&year,data);
      result=pp_setvardate(prot_handle,VAR_EXP_DATE_HARD,month,day,year);
      }
    if (result != PP_SUCCESS) 
      {
      AfxMessageBox("Licence file error");ExitProcess(1);
      }	
    if (!timeexpand)
      {
      prot_result=pp_eztrig1ex(prot_handle,  codein1, codein2, 0,  regcode,prot_componumber,400, 124, &trig, &data);
      if (pp_ndecrypt(prot_encryptcompo,prot_result)==(prot_componumber & 16383) && trig==1)
        {
        pp_setvarnum(prot_handle,VAR_UDEF_NUM_4,codein1); //uloz kod 1
        pp_setvarnum(prot_handle,VAR_UDEF_NUM_3,codein2); //uloz kod 2
        pp_setvarnum(prot_handle,VAR_UDEF_NUM_2,regcode); //uloz kod 2
        pp_setvarnum(prot_handle,VAR_UDEF_NUM_1,data);	  //uloz data
        }
      else 
        {
        goto out;
        }
      }
    else
      {
      pp_setvarnum(prot_handle,VAR_UDEF_NUM_5,0);		//vynuluj registracni cislo
      }
    AfxMessageBox(IDS_AUTHORIZATIONSUCCES);
    if (protCheckValidity()==false) ExitProcess(255);
    EndDialog(IDOK);
    return;
    }
  out:
  AfxMessageBox(IDS_AUTHORIZATIONFAILED);
  char buff[1024];
  pp_upddate(prot_handle,UPDDATE_NOTIME);
  pp_lfclose(prot_handle);
  GetModuleFileName(NULL, buff, 1024);
  if (prot_result==PP_SUCCESS) ShellExecute(NULL,"",buff,"","",SW_NORMAL);
  ExitProcess(0);
  }

//--------------------------------------------------

void CUnlockSoftDlg::OnCancel() 
  {
  ExitProcess(0);
  }

//--------------------------------------------------

void CUnlockSoftDlg::OnVisitweb() 
  {
  CString www;
  www.LoadString(IDS_ORDERWWW);
  SetCursor(::LoadCursor(NULL,IDC_WAIT));
  pp_gotourl((LONG)((HWND)*this),prot_handle,0,www.LockBuffer());
  www.UnlockBuffer();
  }

//--------------------------------------------------

void CUnlockSoftDlg::OnOK()
  {
  
  }

//--------------------------------------------------

void CUnlockSoftDlg::OnOrder() 
  {
  SetCursor(::LoadCursor(NULL,IDC_WAIT));
  SendOrderMessage(*this,regcode,prot_componumber,timeexpand);	
  }

//--------------------------------------------------

