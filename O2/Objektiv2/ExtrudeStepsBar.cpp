// ExtrudeStepsBar.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "ExtrudeStepsBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CExtrudeStepsBar dialog


CExtrudeStepsBar::CExtrudeStepsBar()
  : CDialogBar()
    {
    //{{AFX_DATA_INIT(CExtrudeStepsBar)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    
    //{{AFX_DATA_MAP(CExtrudeStepsBar)
    // NOTE: the ClassWizard will add DDX and DDV calls here
    //}}AFX_DATA_MAP
    }










BEGIN_MESSAGE_MAP(CExtrudeStepsBar, CDialogBar)
  //{{AFX_MSG_MAP(CExtrudeStepsBar)
  ON_NOTIFY(UDN_DELTAPOS, IDC_SEGSPIN, OnDeltaposSegspin)
    ON_WM_DESTROY()
      ON_WM_SIZE()
        //}}AFX_MSG_MAP
        ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CExtrudeStepsBar message handlers
    
    void CExtrudeStepsBar::Create(CWnd *parent,int menuid)
      {
      CDialogBar::Create(parent,IDD,CBRS_TOP,menuid);
      SetBarStyle(GetBarStyle()| CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
      EnableDocking(CBRS_ALIGN_ANY);
      CString top;top.LoadString(IDS_EXTRUDE);
      SetWindowText(top);
      spin.Attach(*GetDlgItem(IDC_SEGSPIN));
      edit.Attach(*GetDlgItem(IDC_SEGMENTS));
      cursize=m_sizeDefault;
      SetDlgItemInt(IDC_SEGMENTS,1,FALSE);
      }










void CExtrudeStepsBar::OnDeltaposSegspin(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
  // TODO: Add your control notification handler code here
  int i=GetDlgItemInt(IDC_SEGMENTS,NULL,FALSE);
  i-=pNMUpDown->iDelta;
  if (i<1) i=1;
  if (i>99) i=99;
  SetDlgItemInt(IDC_SEGMENTS,i,FALSE);
  *pResult = 0;
  }










int CExtrudeStepsBar::GetSegments()
  {
  return GetDlgItemInt(IDC_SEGMENTS,NULL,FALSE);
  }










void CExtrudeStepsBar::OnDestroy() 
  {
  SaveBarSize(*this);
  spin.Detach();
  edit.Detach();
  CDialogBar::OnDestroy();
  }


#include <afxpriv.h>
void CExtrudeStepsBar::OnLButtonDown(UINT nFlags, CPoint point)
{
  //drag also on static dialog components (Labels, disabled buttons,...)
  if (m_pDockBar != NULL /*&& OnToolHitTest(point, NULL) != -1*/)
  {
    // start the drag
    ASSERT(m_pDockContext != NULL);
    ClientToScreen(&point);
    m_pDockContext->StartDrag(point);
  }
  CWnd::OnLButtonDown(nFlags, point);
}
