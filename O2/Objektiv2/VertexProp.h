#if !defined(AFX_VERTEXPROP_H__3A4ACEAF_EC3C_4EB0_9108_F382CD6CC676__INCLUDED_)
#define AFX_VERTEXPROP_H__3A4ACEAF_EC3C_4EB0_9108_F382CD6CC676__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// VertexProp.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CVertexProp dialog

class CVertexProp : public CDialog
  {
  // Construction
  public:
    void ForAllSelectedLoad();
    void FormatToList(int idx,PosT& pt);
    void ReadSelection();
    ObjectData * obj;
    ULONG GetFlags(ULONG src);
    void ReadFlags(ULONG flags, bool first);
    CVertexProp(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CVertexProp)
    enum 
      { IDD = IDD_VERTEX_PROPERTIES };
    CButton	HiddenVertex;
    CListBox List;
    int		Decal;
    int		Fog;
    int		Surface;
    int   Normal;
    CString	User;
    CString	YVal;
    CString	XVal;
    CString	ZVal;
    int		Light;
    CString	nVert;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CVertexProp)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CVertexProp)
    afx_msg void OnSelchangeList();
    virtual BOOL OnInitDialog();
    afx_msg void OnSelectall();
    afx_msg void OnFilter();
    afx_msg void OnApply();
    virtual void OnOK();
    afx_msg void OnCreatesel();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  public:
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnSizing(UINT fwSide, LPRECT pRect);
  protected:
    bool initialized;
};

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VERTEXPROP_H__3A4ACEAF_EC3C_4EB0_9108_F382CD6CC676__INCLUDED_)
