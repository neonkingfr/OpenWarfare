#pragma once
#include "comint.h"
#include "gePlane.h"

class CITurnEdge :
  public CICommand
  {
  geLine _atLine;
  bool _top;
  public:
    CITurnEdge(geLine &atLine,bool top=false);
    ~CITurnEdge(void);

    int Execute(LODObject *obj);
  };
