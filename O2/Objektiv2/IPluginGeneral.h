#ifndef __IPLUGINGENERAL_INTERFACE_H_BREDY_
#define __IPLUGINGENERAL_INTERFACE_H_BREDY_


namespace ObjektivLib {
    class ObjectData;
    class LODObject;
    class IExternalViewer;
    class Selection;
};
class SccFunctions;
class CCmdUI;
class CWinApp;
class ProgressBarFunctions;
class IFileDlgHistory;


class IPluginGeneralAppSide;
class IMatGeneralPluginSide;
class IMCEPluginSide;

class IPluginMenuCommandUpdate
{
public:
  virtual void Enable(bool enable)=0;
  virtual void SetCheck(bool check)=0;
};

class IPluginCommand
{
public:
  virtual void OnCommand()=0;
  virtual void AddRef()=0;
  virtual void Release()=0;
  virtual void Update(const IPluginMenuCommandUpdate *update) {}
};

enum ServiceName
{
  Service_MaterialEditor,
  Service_ModelConfigEditor
};

/**
Plugin initialization

This function must be exported in following form

extern "C" __declspec(dllexport) IPluginGeneralPluginSide * __cdecl  ObjektivPluginMain(IPluginGeneralAppSide *appside)
{
}

*/

#ifndef __READONLYCHECK_HEADER_
#define ROCheckResult int
#endif

class IPluginGeneralPluginSide
{
public:
  enum PopupMenuName {PMSelectionList,PMLodList, PMAnimationList, PMNamedProperties};
    
   ///Requestes plugin for a service
   /**
   Ask plugin, if it support requested service. 
   @param name Name of service.
   @return function should return pointer to interface, that defines all functions of requested interface. Resulting pointer
    will be casted to interface pointer via reinterpret_cast<>. When plugin doesn't support requested interface, function MUST return 0
   @note Some services can be supported by one plugin at time only. If there is more plugins, that supports the service, application will choose one randomly.
   */
   virtual void *GetService(ServiceName name) {return 0;}

   ///Notifies plugin, that Application is going to exit.
   /** Plugin can ask user to save changes.
   @return Function returns true, when it is clear to exit, or false, if plugin is not ready to exit now
   @note For cleaning procedure, AfterClose is better place
   */
   virtual bool BeforeClose() {return true;}                       //vola aplikace p�ed uzav�en�m O2
   ///Notifies plugin, that Application is fully initialized, and ready for action. 
   /**This is the best place to plugin's initialization.
   @param O2Window main window of the Application
   */
   virtual void AfterOpen(HWND O2Window) {}                //is called after main window is opened (not document!)
   
   ///Notifies plugin, that Source Safe controlled files has been reloaded.
   /** This function is called after GetLatestVersion is processed and before background version 
   of GetLatestVersion part is started.
   */
   virtual void SSReload() {};                          //vola aplikace po GetLatestVersion v�ech dat

   ///Notifies plugin, that user wants to clear current documenr
   /**Plugin can ask user for saveing a changes 
   @return true - successfully done
   */
   virtual bool OnNewDocument() {return true;} //O2 is going to create or load a new document

   ///Notifies plugin, that new document has ben loaded
   /**
   This function is called with File -> Open command, or other commands that causes changing of
   current document (excepting saveAs).
   @param filename filename of loaded document
   */
   virtual void AfterLoadDocument(const char *filename) {} //plugin is notified, that new document has been loaded.   
   ///Notifies plugin, that document has been saved
   /**
   @param filename filename of saved document
   */
   virtual void AfterSaveDocument(const char *filename) {} //plugin is notified, that document has been saved.
   
   ///Notifies plugin, that working area has been cleared
   virtual void AfterNewDocument() {}
   ///Notifies plugin, that the Application is going to exit. 
   /**Plugin can handle this and process some special cleaning. At this point, plugin cannot cancel this operation */
   virtual void AfterClose() {}

   virtual void InitPopupMenu(PopupMenuName name, HMENU mnu, HWND clientWindow) {}

  ///Retrieves list of files depends on given filename
  /**
    Function is used with Source Save operations, e.g. GetLatestVersion. 
    @param p3dname name of lodobject, can be empty string or null
    @param object pointer to object, that can contain some references
    @param array pointer to allocated space for array, Pass 0 to get required count of bytes to allocate
    @return if array is 0, function returns count of required bytes to allocate. If array points to 
    some memory, function fills memory and returns number of records stored in array  
    @note Do not move data to another location. All pointers in array points into allocated block. If 
    you want move data, you need relocate all pointers in array
    @note This function can be called in another thread. Keep function multi-thread safe. You can test
    current thread id and compared it with thread id of latest call of SSReload function (which is 
    always processed in main thread and before any background tasks).
  */

  virtual int GetAllDependencies(const char *p3dname, 
      const ObjektivLib::LODObject *object, char **array)=0;
      virtual BOOL OnPretranslateMessage(MSG *msg)=0;     //vola aplikace, aby si plugin mohl odchytnout p��padn� hotkeye

  ///Function is called before plugin is unloaded
  /**
    Function is called before plugin is unloaded. The very next action of application is FreeLibrary()
   */
  virtual void BeforePluginUnload() {}
  virtual void AfterViewerUpdated() {}
};

class IPluginGeneralAppSide
{
public:
  enum PopupMenu{mnuFile=0,mnuEdit=1,mnuCreate=2,mnuView=3,mnuStructure=4,mnuPoints=5,
        mnuFaces=6,mnuSurfaces=7,mnuTools=8,mnuWindow=9,mnuAutomation=10,mnuHelp=11,
        mnuImport=128, mnuExport=129, mnuMainBar=255};
  enum ResourceType{resourceMaterial,resourceTexture};

  ///Updates viewer window - no reload, only invalidates content and forces viewer to redraw it.
  virtual void UpdateViewer()=0;                         //vola plugin, pokud potrebuje aktualizovat okno prohlizece
  ///Reloads model in viewer
  virtual void ReloadViewer()=0;                         //vola plugin, pokud chce reloadovat objekt v prohlizeci
  ///Retrieves pointer to source safe interface
  /*
  @return pointer to interface or NULL in case, that no source safe is not supported.
  */
  virtual SccFunctions *GetSccInterface()=0;         //vola plugin, k ziskani SCC interface
  ///Retrieves path to project root (Path for textures in Options)
  virtual const char *GetProjectRoot()=0;           //vraci cestu na P:
  ///Returns status of viewer. 
  /**
  @return true, if viewer is active and ready. Function returns false, if no viewer is present
  */
  virtual bool IsViewerActive()=0;
  ///Retrieves pointer to viewer interface
  /**
  @return pointer to viewer interface or 0 if no viewer is active
  */
  virtual ObjektivLib::IExternalViewer *GetViewerInterface()=0;
  ///Opens Options dialog (Objektiv options)
  virtual bool OpenOptionsDialog()=0;
  ///Updates viewer with objekt specified by ObjectData
  virtual void UpdateViewer(ObjektivLib::ObjectData *obj)=0;
  ///Creates sub menu in Objektiv2 menu
  virtual HMENU AddSubMenu(PopupMenu menuId, const char *menuName)=0;
  ///Adds menu item into specified submenu. Submenu must be part of O2 menu
  /**
  @param mnu Handle to submenu
  @param menuName Name of menu (text). Use 0 to add a separator
  @param command Pointer to menu interface that handles menu action. Plugin must define this interface
  @return true, when operation is successful
  */
  virtual bool AddMenuItem(HMENU mnu, const char *menuName, IPluginCommand *command)=0;
  ///Adds menu item into specified submenu. Submenu must be part of O2 menu
  /**
  @param menuId One of predefined menu constants specifies popup menu.
  @param menuName Name of menu (text). Use 0 to add a separator
  @param command Pointer to menu interface that handles menu action. Plugin must define this interface
  @return true, when operation is successful
  */
  virtual bool AddMenuItem(PopupMenu menuId, const char *menuName, IPluginCommand *command)=0;

  virtual bool AddPopupMenuItem(HMENU mnu, const char *menuName, IPluginCommand *command)=0;

  virtual void SelectInO2(const char *resourceName, ResourceType resourceType)=0;  
  ///Displays standard ROCheck warining dialog.
  virtual ROCheckResult TestFileRO(const _TCHAR *filename,int flags=0, HWND hWnd=NULL)=0; //vola funkci pro TestFileRO v O2

  ///returns pointer to active object
  /**
  @note It is highly recommended to make copy of object for modifications
  */
  virtual const ObjektivLib::LODObject *GetActiveObject()=0;

  ///returns pointer to active object data
  /**
  @note It is highly recommended to make copy of level for modifications
  */
  virtual const ObjektivLib::ObjectData *GetActiveLevel()=0;

  ///Updates active object from object in DLL
  /**
  Updates object in editor, it creates new actions
  @param newObject new object that will replace old.
  @return true - success, false - fail
  */
  virtual bool UpdateActiveObject(const ObjektivLib::LODObject &newObject)=0;

  ///Updates active level from object in DLL
  /**
  Updates level in editor, it creates new actions
  @param objectData new level that will replace old.
  @return true - success, false - fail
  */
  virtual bool UpdateActiveLevel(const ObjektivLib::ObjectData &objectData)=0;
  
  ///Modifies current object 
  /**
  This function works following way: It deletes all point and face selected by delSelects. After
  that, it merges object "merge" into current object  
  @param merge object, that will be merged after selection is deleted. Can be NULL, that means, 
  no merge is provided
  @param delSelect selection of faces or points to delete. Can be NULL, that means no deleting is 
  provided
  @return true - success, false - fail
  */
  virtual bool ModifyActiveLevel(const ObjektivLib::ObjectData *merge=0, 
      ObjektivLib::Selection *delSelects=0)=0;

  ///Notifies objektiv2, that some data in editor will be updated.
  /**
  @param description pointer to text of update description. This text will be shown in history
  @return function returns true, if updating is allowed, otherwise, it returns false. In this
    case, plugin SHOULD NOT call any update function*/
  virtual bool BeginUpdate(const char *description)=0;


  virtual bool EndUpdate()=0;
  ///Pointer to O2's malloc function
  void *(_cdecl * O2Allocator)(size_t sz);
  ///Pointer to O2's free function
  void (_cdecl * O2Deallocator)(void *ptr);  

  virtual const char *ReadConfigValue(int idsConfig)=0;
    
  ///retrieves pointer to ProgressBar handler
  /**
   DLL can use returned pointer to set up own ProgressBar object. Progress will be 
   displayed using the standard O2 progress bar.
   */
  virtual ProgressBarFunctions *GetProgressBarHandler() = 0;

  ///Retrieves pointer to FileDialogEx history interface
  /**
  DLL can use returned pointer to set up own version of FileDialogEx. Then history will be shared
  */
  virtual IFileDlgHistory *GetFileDialogExHandler() = 0;

};

#ifdef _DEBUG
#define ObjektivPluginMain ObjektivPluginMainD
#define ObjektivPluginMainStr "ObjektivPluginMainD"
#else
#define ObjektivPluginMainStr "ObjektivPluginMain"
#endif

#endif