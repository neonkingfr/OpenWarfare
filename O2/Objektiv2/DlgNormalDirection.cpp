// DlgNormalDirection.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "DlgNormalDirection.h"
#include "Objektiv2Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgNormalDirection dialog


float CDlgNormalDirection::st_vX=0.0f;
float CDlgNormalDirection::st_vY=0.0f;
float CDlgNormalDirection::st_vZ=1.0f;
BOOL CDlgNormalDirection::st_vRelativeTo=FALSE;


CDlgNormalDirection::CDlgNormalDirection(CWnd* pParent /*=NULL*/)
  : CDialog(CDlgNormalDirection::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CDlgNormalDirection)
    vRelativeTo = st_vRelativeTo;
    vX = st_vX;
    vY = st_vY;
    vZ = st_vZ;
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CDlgNormalDirection::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  if (pDX->m_bSaveAndValidate)
    CorrectFloatValues(this,IDC_XVALUE,IDC_YVALUE,IDC_ZVALUE,0);
  //{{AFX_DATA_MAP(CDlgNormalDirection)
  DDX_Control(pDX, IDC_RELATIVETO, wRelativeTo);
  DDX_Control(pDX, IDC_NORMALLIST, wNormalList);
  DDX_Check(pDX, IDC_RELATIVETO, vRelativeTo);
  DDX_Text(pDX, IDC_XVALUE, vX);
  DDX_Text(pDX, IDC_YVALUE, vY);
  DDX_Text(pDX, IDC_ZVALUE, vZ);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CDlgNormalDirection, CDialog)
  //{{AFX_MSG_MAP(CDlgNormalDirection)
  ON_BN_CLICKED(IDC_RELATIVETO, OnRelativeto)
    ON_BN_CLICKED(IDC_PREVIEW, OnPreview)
      ON_BN_CLICKED(IDC_UNLOCKNORMALS, OnUnlocknormals)
        //}}AFX_MSG_MAP
        END_MESSAGE_MAP()
          
          /////////////////////////////////////////////////////////////////////////////
          // CDlgNormalDirection message handlers
          
          void CDlgNormalDirection::DialogRules()
            {
            wNormalList.EnableWindow(wRelativeTo.GetCheck());
            }

//--------------------------------------------------

BOOL CDlgNormalDirection::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  LoadNormalsToCB();
  
  DialogRules();	
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

void CDlgNormalDirection::LoadNormalsToCB()
  {
  int i;
  int cnt=vObject->NFaces();
  for (i=0;i<cnt;i++) 
    {
    int j;
    char szBuff[256];
    FaceT face(vObject,i);
    for (j=0;j<face.N();j++) 
      if (vObject->PointSelected(face.GetPoint(j)))
        break;
    if (j!=face.N())
      {
      Vector3 vx=face.CalculateNormal();
      sprintf(szBuff,"%+.2f %+.2f %+.2f",-vx[0],-vx[1],-vx[2]);
      int found=wNormalList.FindStringExact(-1,szBuff);
      if (found==CB_ERR) 
        {
        int index=wNormalList.AddString(szBuff);
        wNormalList.SetItemData(index,i); //Nastavi k polozce seznamu index face ktere se to tyka
        }
      }
    }
  wNormalList.SetCurSel(0);
  }

//--------------------------------------------------

void CDlgNormalDirection::OnRelativeto() 
  {
  DialogRules();
  }

//--------------------------------------------------

void CDlgNormalDirection::LockNormals()
  {
  int i;
  int pcnt=vObject->NPoints();
  int fcnt=vObject->NFaces();
  
  int *vxindex=new int[pcnt]; //pole pro zjistovani novych indexu normal
  memset(vxindex,0xFF,sizeof(int)*pcnt); //nejprve nastav na -1
  
  for (i=0;i<pcnt;i++) if (vObject->PointSelected(i))
    {
    PosT &pos=vObject->Point(i); 
    pos.flags |=POINT_SPECIAL_LOCKNORMAL;     //zamkni normalu
    vxindex[i]=vObject->NNormals();    //pridej normalu
    VecT *normal=vObject->NewNormal();        //alokuj normalu
    (*normal)[0]=vFinalX;                     //nastav normale hodnty
    (*normal)[1]=vFinalY;
    (*normal)[2]=vFinalZ;
    }
  
  for (i=0;i<fcnt;i++) 
    {
    FaceT face(vObject,i);
    for (int j=0;j<face.N();j++)
      {
      if (vxindex[face.GetPoint(j)]!=-1)
        face.SetNormal(j,vxindex[face.GetPoint(j)]);
      }
    }
  RecalcNormals2(vObject,false);    //Prepocitej normaly a optimalizuj pole
  delete [] vxindex;
  }

//--------------------------------------------------

void CDlgNormalDirection::UnlockNormals()
  {
  int i;
  int cnt=vObject->NPoints();
  
  for (i=0;i<cnt;i++) if (vObject->PointSelected(i)) // pro vsechny vybrane vertexy
    {
    PosT &pos=vObject->Point(i);
    pos.flags &=~POINT_SPECIAL_LOCKNORMAL;     //odemkni normalu
    }
  RecalcNormals2(vObject,false);    //Prepocitej normaly a optimalizuj pole
  }

//--------------------------------------------------

void CDlgNormalDirection::OnPreview() 
  {
  if (UpdateData(TRUE)==FALSE) return;
  CalculateFinalDirection();
  ObjectData saved;
  saved=*vObject;
  LockNormals();
  doc->UpdateAllViews(NULL,1);
  *vObject=saved;
  }

//--------------------------------------------------

void CDlgNormalDirection::CalculateFinalDirection()
  {
  if (vRelativeTo)
    {
    int cb_index=wNormalList.GetCurSel();
    if (cb_index!=LB_ERR)
      {
      int face_index=wNormalList.GetItemData(cb_index);
      FaceT face(vObject,face_index);
      HMATRIX mm;
      GetMatrixFromFace(*vObject,face,mm);
      HVECTOR v1,v2;
      v1[XVAL]=vX;v1[YVAL]=vY;v1[ZVAL]=vZ;v1[WVAL]=1.0f;
      TransformVectorNoTranslate(mm,v1,v2);
      CorrectVectorM(v2);
      NormalizeVector(v2);
      vFinalX=v2[XVAL];
      vFinalY=v2[YVAL];
      vFinalZ=v2[ZVAL];
      return;
      }
    }
  float m=1.0f/sqrt(vX*vX+vY*vY+vZ*vZ);
  vFinalX=vX*m;
  vFinalY=vY*m;
  vFinalZ=vZ*m;  
  }

//--------------------------------------------------

void CDlgNormalDirection::OnOK() 
  {
  if (UpdateData(TRUE)==FALSE) return;
  st_vRelativeTo=vRelativeTo;
  st_vX=vX;
  st_vY=vY;
  st_vZ=vZ;
  CalculateFinalDirection();
  CDialog::OnOK();
  }

//--------------------------------------------------

void CDlgNormalDirection::OnUnlocknormals() 
  {
  EndDialog(IDC_UNLOCKNORMALS);  
  }

//--------------------------------------------------

CICommand *CINormalDirection_Create(istream &str)
  {
  return new CINormalDirection(str);
  }

//--------------------------------------------------

