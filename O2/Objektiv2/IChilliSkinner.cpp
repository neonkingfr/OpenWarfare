// IChilliSkinner.cpp: implementation of the CIChilliSkinner class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "objektiv2.h"
#include "objektiv2doc.h"
#include "IChilliSkinner.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


static void SelectPointsFromFaces(ObjectData &obj, Selection &sel)
  {
  int i;
  int j;
  for (i=0;i<obj.NFaces();i++) if (sel.FaceSelected(i))
    {
    FaceT fc(obj,i);
    for (j=0;j<fc.N();j++) 
      sel.PointSelect(fc.GetPoint(j));
    }
  }

static void TransformSelection(ObjectData &obj, LPHMATRIX mm ,Selection *limitsel, Selection *transformsel, Selection *lockedpoints)
  {
  int i;
  int j;
  int npt=obj.NPoints();
  int nfc=obj.NFaces();
  
  int *ptt=new int[npt];
  for (i=0;i<npt;i++) ptt[i]=i;

  SelectPointsFromFaces(obj,*transformsel);
  for (i=0;i<npt;i++) 
    if (limitsel->PointSelected(i) && transformsel->PointSelected(i))
      {            
      PosT &orgpos=obj.Point(i);
      VecT trf=orgpos;
      HVECTOR vt;
      TransformVector(mm,mxVector3(trf[0],trf[1],trf[2]),vt);
      CorrectVectorM(vt);
      trf[0]=vt[0];trf[1]=vt[1];trf[2]=vt[2];
      if (lockedpoints && lockedpoints->PointSelected(i) && trf.Distance2(orgpos)>0.0001f)
        {
        PosT &nw=obj.Point(ptt[i]=obj.ReservePoints(1));
        nw.SetPoint(trf);
        for (int p=0;p<obj.NAnimations();p++)
          {
          AnimationPhase *anm=obj.GetAnimation(p);
          (*anm)[ptt[i]]=(*anm)[i];
          }
        limitsel->PointSelect(ptt[i],true);
        }
      else        
        orgpos.SetPoint(trf);
      }
  if (lockedpoints)
    for (i=0;i<obj.NFaces();i++) if (transformsel->FaceSelected(i))
      {
      FaceT fc(obj,i);
      for (j=0;j<fc.N();j++) 
        {
        int pt=fc.GetPoint(j);
        fc.SetPoint(j,ptt[pt]); 
        }
      }
    delete [] ptt;
    }

static void CalcRotFaceMatrix(VecT &fcn,VecT &rotax, VecT &refpt,LPHMATRIX out)
  {
  fcn.Normalize();
  rotax.Normalize();
  VecT vp=fcn.CrossProduct(rotax);
  vp.Normalize();
  VecT vp2=fcn.CrossProduct(vp);
  for (int i=0;i<3;i++)
    {
    out[0][i]=vp2[i];
    out[1][i]=fcn[i];
    out[2][i]=vp[i];
    out[3][i]=refpt[i];
    out[i][3]=0.0f;
    }
  out[3][3]=1.0;
  }

static void CalcRotationMatrixFromFace(ObjectData &obj, int face, int begpt, LPHMATRIX out)
  {
  FaceT fc(obj,face);
  int p1=begpt;
  int p2=(p1>=fc.N()-1)?0:(p1+1);
  int p3=(p2>=fc.N()-1)?0:(p2+1);
  VecT ps1=obj.Point(fc.GetPoint(p1));
  VecT ps2=obj.Point(fc.GetPoint(p2));
  VecT ps3=obj.Point(fc.GetPoint(p3));
  VecT v1=ps2-ps1;
  VecT v2=ps3-ps1;
  VecT fcn=v1.CrossProduct(v2);
  VecT pln=VecT(0,-1,0);
  HMATRIX src;
  HMATRIX trg;
  HMATRIX itrg;
  CalcRotFaceMatrix(fcn,v1,ps1,src);
  CalcRotFaceMatrix(pln,v1,VecT(ps1[0],0,ps1[2]),trg);
  InverzeMatice(src,itrg);
  SoucinMatic(itrg,trg,out);
  }

static void SelectContinualTopology(ObjectData &obj, Selection *limit, Selection *locked, Selection *result)
//NOTE: *result must contain one selected face. This is first face where algoritm starts.
  {
  bool a=false;
  do
    {
    a=false;
    int i;
    SelectPointsFromFaces(obj,*result); //
    for (i=0;i<obj.NFaces();i++) if (!result->FaceSelected(i) && !locked->FaceSelected(i) && limit->FaceSelected(i))
      {
      FaceT fc(obj,i);
      int cnt=0;
      for (int j=0;j<=fc.N();j++)
        {        
        int pt=fc.GetPoint(j==fc.N()?0:j);
        if (result->PointSelected(pt)) 
          cnt++;else cnt=0;
        if (cnt==2)
          {
          result->FaceSelect(i);
          a=true;
          break;
          }
        }
      }
    }
  while (a);
  SelectPointsFromFaces(obj,*result); 
  (*result)-=*locked;
  }

static void CreateTransformSelection(ObjectData &obj, Selection *limit, Selection *locked)
  {
  int nfc=obj.NFaces();  
  locked->Clear();
  for (int i=0;i<nfc;i++) if (limit->FaceSelected(i))
    {
    int cntlv=0;
    FaceT fc(obj,i);
    for (int j=0;j<fc.N();j++)
      {
      PosT &ps=obj.Point(fc.GetPoint(j));
      if (fabs(ps[1])<0.01f) cntlv++;
      }
    if (cntlv<3) 
      {
      locked->FaceSelect(i,false);
      }
    else
      {
      locked->FaceSelect(i,true);
      }
    }
  SelectPointsFromFaces(obj,*locked);
  }

static int SelectNextFace(ObjectData &obj, Selection *limit, Selection *locked, VecT &lastdir)
  {
  int nfc=obj.NFaces();  
  float maxcos=0.5;
  int maxfc=-1;
  VecT newlastdir;
  for (int i=0;i<nfc;i++) if (limit->FaceSelected(i) && !locked->FaceSelected(i))
    {
    int marked=-1;    
    FaceT fc(obj,i);
    for (int j=0;j<fc.N();j++)
      {
      int p1=fc.GetPoint(j);
      int p2=fc.GetPoint((j+1)%fc.N());
      PosT &pos=obj.Point(p1);
      if (locked->PointSelected(p1) && locked->PointSelected(p2)) 
        {
        PosT &ps1=obj.Point(p1);
        PosT &ps2=obj.Point(p2);
        VecT dir=ps2-ps1;
        dir.Normalize();
        float cos=dir.CosAngle(lastdir);
        if (cos>maxcos)
          {
          marked=j;       
          maxfc=i | (marked<<28);
          newlastdir=dir;
          maxcos=cos;
          }
        } 
      }
    }
  lastdir=newlastdir;
  return maxfc;
  }

static int SelectNextFace2(ObjectData &obj, Selection *limit, Selection *locked, VecT &lastdir)
  {
  int nfc=obj.NFaces();  
  float maxdist=1e20;
  int maxfc=-1;
  VecT dir;
  for (int i=0;i<nfc;i++) if (limit->FaceSelected(i) && !locked->FaceSelected(i))
    {
    float height=0;
    int marked=-1;    
    FaceT fc(obj,i);
    for (int j=0;j<fc.N();j++)
      {
      int p1=fc.GetPoint(j);
      int p2=fc.GetPoint((j+1)%fc.N());
      PosT &pos=obj.Point(p1);
      if (fabs(pos[1])>height) height=fabs(pos[1]);
      if (locked->PointSelected(p1) && locked->PointSelected(p2)) 
        {
        PosT &ps1=obj.Point(p1);
        PosT &ps2=obj.Point(p2);        
        dir=ps2-ps1;
        marked=j;
        }
      }
    if (marked!=-1 && height<maxdist)
      {
      maxdist=height;
      maxfc=i | (marked<<28);
      lastdir=dir;
      }
    }
  return maxfc;
  }

static void DoChilliSkinner(ObjectData &obj, int beginface, bool preview)
  {
  Selection limit(*obj.GetSelection());
  Selection transf=limit;
  Selection locked(&obj);
  int totalsteps=limit.NFaces();
  int curstep=0;
  VecT lastdir(1,0,0);
  HMATRIX mx;
  SelectPointsFromFaces(obj,transf);
  CalcRotationMatrixFromFace(obj,beginface,0,mx);
  TransformSelection(obj,mx,&limit,&transf,&locked);
/*    if (preview)
      {
      doc->UpdateAllViews(NULL,1,0);
      AfxMessageBox("ChilliSkinner Step",MB_OK);      
      }*/
  CreateTransformSelection(obj,&limit,&locked);
  int fc=SelectNextFace2(obj,&limit,&locked,lastdir);  
  ProgressBar<int> pb(totalsteps);
  while (fc!=-1)
    {
    pb.AdvanceNext(1);
    int sd=fc>>28;
    fc &=~((-1)<<28);
    transf.Clear();
    transf.FaceSelect(fc);
    SelectContinualTopology(obj,&limit,&locked,&transf);
    if (preview)
      {
      Selection *cur=const_cast<Selection *>(obj.GetSelection());
      *cur=transf;      
      cur->FaceSelect(0,cur->FaceSelected(0));
      doc->UpdateAllViews(NULL,1,0);
    //    if (AfxMessageBox("ChilliSkinner Step",MB_OKCANCEL)==IDCANCEL) break;
      }
    CalcRotationMatrixFromFace(obj,fc,sd,mx);
    TransformSelection(obj,mx,&limit,&transf,&locked);
    CreateTransformSelection(obj,&limit,&locked);    
    fc=SelectNextFace(obj,&limit,&locked,lastdir);
    if (fc==-1) 
      fc=SelectNextFace2(obj,&limit,&locked,lastdir);
    }
  }


CIChilliSkinner::CIChilliSkinner(int begfc)
{
beginface=begfc;
recordable=false;
}

CIChilliSkinner::~CIChilliSkinner()
{

}

int CIChilliSkinner::Execute(LODObject *lod)
  {
  DoChilliSkinner(*lod->Active(),beginface,lod==doc->LodData);  
  return 0;
  }

int CIChilliSkinner::SelectBeginFaceFromPin(ObjectData &obj, LPHVECTOR pin)
  {
  int nfc=obj.NFaces();  
  float dist=-1;
  int idx=-1;
  for (int i=0;i<nfc;i++) if (obj.FaceSelected(i))
    {
    FaceT fc(obj,i);
    HVECTOR accm={0,0,0,1};
    for (int j=0;j<fc.N();j++)
      {
      PosT &pos=obj.Point(fc.GetPoint(j));
      accm[0]+=pos[0];
      accm[1]+=pos[1];
      accm[2]+=pos[2];
      }
    NasobVektor4(accm,1.0f/fc.N());
    RozdilVektoru(accm,pin);
    float d2=ModulVektoru2(accm);
    if (dist<0 || dist>d2) 
      {
      dist=d2;
      idx=i;
      }
    }
  return idx;
  }
