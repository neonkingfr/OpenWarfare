// GuideLines.h: interface for the CGuideLines class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GUIDELINES_H__FC1BB6A4_570F_4D0C_8A73_AA0289B849BF__INCLUDED_)
#define AFX_GUIDELINES_H__FC1BB6A4_570F_4D0C_8A73_AA0289B849BF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "autoarray.h"

#define MAXGUIDELINES 6
#define CGL_HORZ 1
#define CGL_VERT 2
#define CGL_NONE 0

class CGuideLines
  {
  int gDiff;
  int horz[MAXGUIDELINES];
  int vert[MAXGUIDELINES];
  CPen color;
  public:
    void SetGuideLine(int gline, int lineindex, int pos, RECT *cliprect=NULL);
    void Draw(CDC& pDC);
    CGuideLines& operator=(const CGuideLines& other)
      {
      gDiff=other.gDiff;
      memcpy(horz,other.horz,sizeof(horz));
      memcpy(vert,other.vert,sizeof(vert));
      return *this;
      }
    CGuideLines(COLORREF gcolor=RGB(255,255,0),int gdiff=2):gDiff(gdiff),color(PS_SOLID,1,gcolor) 
      {
      memset(horz,0,sizeof(horz));
      memset(vert,0,sizeof(vert));
      }
    int GuideCursor(CPoint& pt, int *linev=NULL, int *lineh=NULL);
    
  };

//--------------------------------------------------

#endif // !defined(AFX_GUIDELINES_H__FC1BB6A4_570F_4D0C_8A73_AA0289B849BF__INCLUDED_)
