// IFlattenPoints.cpp: implementation of the CIFlattenPoints class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "objektiv2.h"
#include "IFlattenPoints.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CIFlattenPoints::CIFlattenPoints(HVECTOR p)
  {
  CopyVektor(plane,p);
  }

//--------------------------------------------------

void CIFlattenPoints::GetPlaneFromModel( ObjectData *obj, HVECTOR plane)
  {
  }

//--------------------------------------------------

CIFlattenPoints::CIFlattenPoints(istream &str)
  { 
  datardp(str,plane);
  }

//--------------------------------------------------

int CIFlattenPoints::Tag()
  {
  return CITAG_FLATTENPOINTS;
  }

//--------------------------------------------------

void CIFlattenPoints::Save(ostream &str)
  {
  datawrp(str,plane);
  }

//--------------------------------------------------

CICommand *CIFlattenPoints_Create(istream &str)
  {
  return new CIFlattenPoints(str);
  }

//--------------------------------------------------

int CIFlattenPoints::Execute(LODObject *lod)
  {
  ObjectData *obj=lod->Active();
  int cnt=obj->NPoints();
  for (int i=0;i<cnt;i++) if (obj->PointSelected(i))
    {
    PosT& pos=obj->Point(i);
    float td=ModulVektoru2(plane);
    float tu=-(plane[0]*pos[0]+plane[1]*pos[1]+plane[2]*pos[2]+plane[3]);
    float t=tu/td;
    pos[0]=pos[0]+plane[0]*t;
    pos[1]=pos[1]+plane[1]*t;
    pos[2]=pos[2]+plane[2]*t;
    }
  return 0;
  }

//--------------------------------------------------

