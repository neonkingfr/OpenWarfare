#include "stdafx.h"
//#include "NormalRecalc.h"
#include "../ObjektivLib/Edges.h"
#include "resource.h"
#include "Objektiv2.h"
#if 0

namespace ObjektivLib {

static HVECTOR *normlist;
static int *indexes;

static void PrepareData(ObjectData *obj)
  {
  int cnt=obj->NPoints()*2;
  normlist=new HVECTOR[cnt];
  indexes=new int[cnt];
  for (int i=0;i<cnt;i++)
    {
    normlist[i][0]=normlist[i][1]=normlist[i][2]=0;
    normlist[i][3]=1;
    indexes[i]=-1;
    }
  }

//--------------------------------------------------

LPHVECTOR GetFaceNormal(FaceT& fc, ObjectData *obj, bool normalize)
  {
  static HVECTOR vc;
  HVECTOR v1,v2,v3;
  PosT& ps1=obj->Point(fc.GetPoint(0));
  PosT& ps2=obj->Point(fc.GetPoint(1));
  PosT& ps3=obj->Point(fc.GetPoint(2));
  CopyVektor(v1,mxVector3(ps1[0],ps1[1],ps1[2]));
  CopyVektor(v2,mxVector3(ps2[0],ps2[1],ps2[2]));
  CopyVektor(v3,mxVector3(ps3[0],ps3[1],ps3[2]));
  RozdilVektoru(v2,v1);
  RozdilVektoru(v3,v1);
  VektorSoucin(v2,v3,vc);
  if (fc.N()==4)
    {
    HVECTOR vp;
    PosT& ps1=obj->Point(fc.GetPoint(1));
    PosT& ps2=obj->Point(fc.GetPoint(2));
    PosT& ps3=obj->Point(fc.GetPoint(3));
    CopyVektor(v1,mxVector3(ps1[0],ps1[1],ps1[2]));
    CopyVektor(v2,mxVector3(ps2[0],ps2[1],ps2[2]));
    CopyVektor(v3,mxVector3(ps3[0],ps3[1],ps3[2]));
    RozdilVektoru(v2,v1);
    RozdilVektoru(v3,v1);
    VektorSoucin(v2,v3,vp);
    SoucetVektoru(vc,vp);
    }
  if (normalize)  NormalizeVector(vc);
  return vc;
  }

//--------------------------------------------------

static void JoinFaces(FaceT& fc1, FaceT& fc2, ObjectData *obj,int pt)
  {
  int p1,p2;
  int normi;
  LPHVECTOR addnorm;
  for (p1=0;p1<fc1.N();p1++) 
    if (fc1.GetPoint(p1)==pt) break;
  for (p2=0;p2<fc2.N();p2++)
    if (fc2.GetPoint(p2)==pt) break;
  if (fc1.GetNormal(p1)==-1 && fc1.GetNormal(p1)==-1)
    {
    normi=obj->NNormals();
    VecT &vv=*obj->NewNormal();
    LPHVECTOR vx=GetFaceNormal(fc1,obj);	
    addnorm=GetFaceNormal(fc2,obj);
    }
  else if (fc1.GetNormal(p1)!=-1) 
    {
    normi=fc1.GetNormal(p1);
    addnorm=GetFaceNormal(fc2,obj);
    }
  else if (fc2.GetNormal(p2)!=-1) 
    {
    normi=fc2.GetNormal(p2);
    addnorm=GetFaceNormal(fc1,obj);
    }
  
  }

//--------------------------------------------------

LPHVECTOR GetTwoSidedNormal(FaceT& fc, ObjectData *obj, int point)
  {
  int a=point-1;
  int b=point;
  int c=point+1;
  if (a<0) a+=fc.N();
  if (c>=fc.N()) c-=fc.N();
  PosT& ps1=obj->Point(fc.GetPoint(a));
  PosT& ps2=obj->Point(fc.GetPoint(b));
  PosT& ps3=obj->Point(fc.GetPoint(c));
  static HVECTOR v1;
  HVECTOR v2;
  CopyVektor(v1,mxVector3(ps2[0],ps2[1],ps2[2]));
  CopyVektor(v2,mxVector3(
    (ps1[0]+ps3[0])*0.5f,(ps1[1]+ps3[1])*0.5f,(ps1[2]+ps3[2])*0.5f));
  RozdilVektoru(v2,v1);
  return v1;
  }

//--------------------------------------------------

LPHVECTOR GetSkyNormal(FaceT& fc, ObjectData *obj, int point)
  {
  PosT& ps1=obj->Point(fc.GetPoint(point));
  static HVECTOR v1;
  CopyVektor(v1,mxVector3(ps1[0],ps1[1],ps1[2]));
  NormalizeVector(v1);
  return v1;
  }

//--------------------------------------------------

static bool IsSharp(FaceT& fc, CEdges& egs, int point)
  {
  int a=point-1;
  int b=point;
  int c=point+1;
  if (a<0) a+=fc.N();
  if (c>=fc.N()) c-=fc.N();
  return egs.IsEdge(fc.GetPoint(a),fc.GetPoint(b)) && egs.IsEdge(fc.GetPoint(b),fc.GetPoint(c));
  }

//--------------------------------------------------

static void CalcSmoothedNormals(ObjectData *obj, CEdges& egs)
  {
  ProgressBar<int> p1(2);
  p1.AdvanceNext(1);
  int cnt=obj->NFaces();
  int i;
  for (i=0;i<cnt;i++)
    {
    ProgressBar<int> p2(cnt);
    p2.AdvanceNext(1);
    FaceT fc(obj,i);
    LPHVECTOR v=GetFaceNormal(fc,obj);
    for (int j=0;j<fc.N();j++) if (!IsSharp(fc,egs,j))
      SoucetVektoru(normlist[fc.GetPoint(j)],v);	  	    
    }
  p1.AdvanceNext(1);
  cnt=obj->NPoints();
  for (i=0;i<cnt;i++)
    {
    ProgressBar<int> p2(cnt);
    p2.AdvanceNext(1);
    NormalizeVector(normlist[i]);
    normlist[cnt+i][0]=-normlist[i][0];
    normlist[cnt+i][1]=-normlist[i][1];
    normlist[cnt+i][2]=-normlist[i][2];
    normlist[cnt+i][3]=1;
    }
  }

//--------------------------------------------------

static void CalcOtherNormals(ObjectData *obj, CEdges& egs)
  {
  int cnt=obj->NFaces();
  int i;
  int normcount=0;
  LPHVECTOR vc;
  obj->ResetNormals();
  ProgressBar<int> pb(cnt);
  for (i=0;i<cnt;i++)
    {
    pb.AdvanceNext(1);
    FaceT fc(obj,i);
    int flags=fc.GetFlags() & FACE_LIGHT_MASK;
    if (flags==FACE_BOTHSIDESLIGHT)
      {
      for (int j=0;j<fc.N();j++)
        {
        VecT *norm=obj->NewNormal();
        vc=GetTwoSidedNormal(fc,obj,j);
        (*norm)[0]=vc[XVAL];
        (*norm)[1]=vc[YVAL];
        (*norm)[2]=vc[ZVAL];
        fc.SetNormal(j,normcount++);
        }
      }
    else if (flags==FACE_SKYLIGHT)
      {
      for (int j=0;j<fc.N();j++)
        {
        VecT *norm=obj->NewNormal();
        vc=GetSkyNormal(fc,obj,j);
        (*norm)[0]=vc[XVAL];
        (*norm)[1]=vc[YVAL];
        (*norm)[2]=vc[ZVAL];
        fc.SetNormal(j,normcount++);
        }
      }
    else if (flags==FACE_FLATLIGHT)
      {
      VecT *norm=obj->NewNormal();
      vc=GetFaceNormal(fc,obj);
      (*norm)[0]=vc[XVAL];
      (*norm)[1]=vc[YVAL];
      (*norm)[2]=vc[ZVAL];
      int idx=normcount++;
      for (int j=0;j<fc.N();j++) fc.SetNormal(j,idx);		
      }
    else
      {
      float factor;
      bool reverse;
      if (flags == FACE_REVERSELIGHT)
        {
        reverse=true;
        factor=-1.0f;
        }
      else
        {
        reverse=false;
        factor=1.0f;
        }
      vc=GetFaceNormal(fc,obj);
      VecT *norm=NULL;
      int idx=-1;
      for (int j=0;j<fc.N();j++)
        {
        if (IsSharp(fc,egs,j))
          {
          if (idx==-1)
            {
            norm=obj->NewNormal();
            vc=GetFaceNormal(fc,obj);
            (*norm)[0]=vc[XVAL]*factor;
            (*norm)[1]=vc[YVAL]*factor;
            (*norm)[2]=vc[ZVAL]*factor;
            idx=normcount++;
            }
          fc.SetNormal(j,idx);
          }
        else
          {
          int pt=fc.GetPoint(j);
          if (reverse) pt+=obj->NPoints();
          if (indexes[pt]==-1)
            {
            VecT *norm=obj->NewNormal();
            indexes[pt]=normcount++;
            (*norm)[0]=normlist[pt][XVAL];
            (*norm)[1]=normlist[pt][YVAL];
            (*norm)[2]=normlist[pt][ZVAL];			
            }
          fc.SetNormal(j,indexes[pt]);
          }
        }
      }
    }	  
  }

//--------------------------------------------------

static void CleanUp()
  {
  delete [] normlist;
  delete [] indexes;
  }


//--------------------------------------------------

struct SGraphItem
  {
  int vx1,vx2,face;
  };

//--------------------------------------------------

static int GetFaceEdge(THASHTABLE *tbl,FaceT& fc,CEdges &nocross,int idx, int pos)
  {  
  SGraphItem it;
  int pos2=pos+1;
  if (pos2>=fc.N()) pos2-=fc.N();
  it.vx1=fc.GetPoint(pos2);
  it.vx2=fc.GetPoint(pos);
  if (nocross.IsEdge(it.vx1,it.vx2)) return -1;
  SGraphItem *fit;
  fit=(SGraphItem *)Hash_Find(tbl,&it);
  if (fit==NULL)
    {
    it.vx1=fc.GetPoint(pos);
    it.vx2=fc.GetPoint(pos2);
    fit=(SGraphItem *)Hash_Find(tbl,&it);
    }
  if (fit!=NULL) return fit->face;
  it.face=idx;
  Hash_Add(tbl,&it);
  return -1;
  }

//--------------------------------------------------

static void BuildGraph(ObjectData *obj, CEdges& egs, CEdges &nocross)
  {
  THASHTABLE tbl;
  Hash_Init2(&tbl,10001,sizeof(SGraphItem),0,8,0);
  for (int i=0;i<obj->NFaces();i++)
    {
    FaceT fc(obj,i);
    int flags=fc.GetFlags() & FACE_LIGHT_MASK;
    if (flags==FACE_BOTHSIDESLIGHT||flags==FACE_SKYLIGHT||flags==FACE_FLATLIGHT) continue;
    for (int j=0;j<fc.N();j++)
      {
      int spoj=GetFaceEdge(&tbl,fc,nocross,i,j);
      if (spoj!=-1) 
        {
        egs.SetEdge(i,spoj);
        egs.SetEdge(spoj,i);
        }
      }
    }
  Hash_Done(&tbl);
  }

//--------------------------------------------------

static int GetNormalAtPoint(ObjectData *obj, int face, int point)
  {
  FaceT fc(obj,face);
  for (int i=0;i<fc.N();i++)	
    if (fc.GetPoint(i)==point) return fc.GetNormal(i);	
  return -1;
  }

//--------------------------------------------------

static void SetNewNormalAtPoint(ObjectData *obj, int face, int point, int normal)
  {
  FaceT fc(obj,face);
  for (int i=0;i<fc.N();i++)	
    if (fc.GetPoint(i)==point) fc.SetNormal(i,normal);	
  }

//--------------------------------------------------

static void ProcessOneFace(ObjectData *obj, int face, CEdges& graph)
  {
  FaceT fc(obj,face);
  LPHVECTOR vc;
  int flags=fc.GetFlags()& FACE_LIGHT_MASK;
  if (flags==FACE_BOTHSIDESLIGHT)
    {
    for (int j=0;j<fc.N();j++)
      {
      fc.SetNormal(j,obj->NNormals());
      VecT *norm=obj->NewNormal();
      vc=GetTwoSidedNormal(fc,obj,j);
      (*norm)[0]=vc[XVAL];
      (*norm)[1]=vc[YVAL];
      (*norm)[2]=vc[ZVAL];
      }
    }
  else if (flags==FACE_SKYLIGHT)
    {
    for (int j=0;j<fc.N();j++)
      {
      fc.SetNormal(j,obj->NNormals());
      VecT *norm=obj->NewNormal();
      vc=GetSkyNormal(fc,obj,j);
      (*norm)[0]=vc[XVAL];
      (*norm)[1]=vc[YVAL];
      (*norm)[2]=vc[ZVAL];
      }
    }
  else if (flags==FACE_FLATLIGHT)
    {
    int idx=obj->NNormals();
    VecT *norm=obj->NewNormal();
    vc=GetFaceNormal(fc,obj);
    (*norm)[0]=vc[XVAL];
    (*norm)[1]=vc[YVAL];
    (*norm)[2]=vc[ZVAL];
    for (int j=0;j<fc.N();j++) fc.SetNormal(j,idx);		
    }
  else for (int i=0;i<fc.N();i++)
    {
    fc.SetNormal(i,-1);
    for (int j=0;;j++)
      {
      int nf=graph.GetEdge(face,j);
      if (nf==-1) break;
      if (fc.GetNormal(i)==-1) fc.SetNormal(i,GetNormalAtPoint(obj,nf,fc.GetPoint(i)));
      else SetNewNormalAtPoint(obj,nf,fc.GetPoint(i),fc.GetNormal(i));	  	  
      }
    if (fc.GetNormal(i)==-1) 
      {
      fc.SetNormal(i,obj->NNormals());
      VecT *v=obj->NewNormal();
      (*v)[0]=(*v)[1]=(*v)[2]=0.0f;
      }
    }
  }

//--------------------------------------------------

static bool PrepareOneGroupNormal(ObjectData *obj,CEdges& graph, char *facestat)
  {
  int fcnt=obj->NFaces();
  int i;
  for (i=0;i<fcnt;i++) if (facestat[i]==0) break;
  if (i==fcnt) return false;
  facestat[i]=1;
  int min=i;
  bool cont;
  do
    {	
    cont=false;
    for (i=min;i<fcnt;i++)
      if (facestat[i]==1)
        {
        int a=0,p;		
        while ((p=graph.GetEdge(i,a++))>=0)
          if (facestat[p]==0) 
            {facestat[p]=1;cont=true;p=__min(p,min);}
        facestat[i]=2;
        }
    }
  while (cont);
  int pcnt=obj->NPoints();
  int *normindex=new int[pcnt];
  memset(normindex,0xFF,sizeof(int)*pcnt);
  for (i=0;i<fcnt;i++)
    if (facestat[i]==2)
      {
      facestat[i]=3;
      FaceT fc(obj,i);
      LPHVECTOR vc;
      int flags=fc.GetFlags() & FACE_LIGHT_MASK;
      if (flags==FACE_BOTHSIDESLIGHT)
        {
        for (int j=0;j<fc.N();j++)
          {
          fc.SetNormal(j,obj->NNormals());
          VecT *norm=obj->NewNormal();
          vc=GetTwoSidedNormal(fc,obj,j);
          (*norm)[0]=vc[XVAL];
          (*norm)[1]=vc[YVAL];
          (*norm)[2]=vc[ZVAL];
          }
        }
      else if (flags==FACE_SKYLIGHT)
        {
        for (int j=0;j<fc.N();j++)
          {
          fc.SetNormal(j,obj->NNormals());
          VecT *norm=obj->NewNormal();
          vc=GetSkyNormal(fc,obj,j);
          (*norm)[0]=vc[XVAL];
          (*norm)[1]=vc[YVAL];
          (*norm)[2]=vc[ZVAL];
          }
        }
      else if (flags==FACE_FLATLIGHT)
        {
        int idx=obj->NNormals();
        VecT *norm=obj->NewNormal();
        vc=GetFaceNormal(fc,obj);
        (*norm)[0]=vc[XVAL];
        (*norm)[1]=vc[YVAL];
        (*norm)[2]=vc[ZVAL];
        for (int j=0;j<fc.N();j++) fc.SetNormal(j,idx);		
        }
      else for (int i=0;i<fc.N();i++)
        {
        int idx=fc.GetPoint(i);
        if (normindex[idx]!=-1) fc.SetNormal(i,normindex[idx]);
        else 
          {
          fc.SetNormal(i,normindex[idx]=obj->NNormals());
          VecT *vc=obj->NewNormal();
          (*vc)[0]=(*vc)[1]=(*vc)[2]=0.0f;
          }
        }
      }
  delete [] normindex; 
  return true;
  }

//--------------------------------------------------

/*
  static void PrepareNormals(ObjectData *obj, CEdges& nocross)
  {
  CEdges graph;
  char *facestats=new char [obj->NFaces()];
  graph.SetVertices(obj->NFaces());
  BuildGraph(obj,graph,nocross);
  memset(facestats,0,sizeof(char)*obj->NFaces());
  static int last=10;
  int p=0;
  while (PrepareOneGroupNormal(obj,graph,facestats))
  SetProgress((p*100)/last);p++;
  if (p==0) p++;
  last=p;
  delete [] facestats;
  }
*/


static void ObtoceniVertexu(ObjectData *obj, int face, int vertex, CEdges& graph, CEdges& stats)
  {
  int a=0;
  int p;
  stats.SetEdge(face,vertex);
  while ((p=graph.GetEdge(face,a++))!=-1)
    {
    FaceT fc(obj,p);
    int j;
    for (j=0;j<fc.N();j++) if (fc.GetPoint(j)==vertex) break;
    if (j!=fc.N())
      {
      int b=0;
      int q;
      while ((q=stats.GetEdge(p,b++))!=-1)
        if (q==vertex) break;
      if (q==-1)ObtoceniVertexu(obj,p,vertex,graph,stats);
      fc.SetNormal(j,obj->NNormals());
      }
    }
  }

//--------------------------------------------------


//--------------------------------------------------

//--------------------------------------------------


//--------------------------------------------------


//--------------------------------------------------

#endif
void RecalcNormals2(ObjectData *obj,bool noprogress)
  {
/*  VecT *locked_normals=new VecT[obj->NPoints()];
  memset(locked_normals,0,sizeof(VecT)*obj->NPoints());
  
  
  CEdges egs;
  ReadEdges(obj, egs);
  ReadLockedNormals(obj, locked_normals);
  obj->ResetNormals();
  ClearNormalInfo(obj);
  PrepareNormals(obj,egs,!noprogress);
  CalcNormals(obj,locked_normals);
  delete [] locked_normals;*/
  obj->RecalcNormals(noprogress);
  }

//--------------------------------------------------


