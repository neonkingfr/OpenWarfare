#if !defined(AFX_INTERNALVIEW_H__CD57D708_CD70_4857_879C_1971F377966D__INCLUDED_)
#define AFX_INTERNALVIEW_H__CD57D708_CD70_4857_879C_1971F377966D__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InternalView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInternalView window

class CInternalView : public CWnd
  {
  // Construction
  HWCONFIG2 cfg;
  HWND externalNotifier;
  public:
    bool InitViewer();
    CInternalView();
    
    // Attributes
  public:
    
    // Operations
  public:
    static bool g3dInited;
    static bool g3dOpened;
    static bool g3dDllLoaded;   
    bool showhided;
    bool showproxies;
    int showselects;
    bool external;
    bool error;
    HWND hwndtoopen;
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CInternalView)
    //}}AFX_VIRTUAL
    
    // Implementation
  public:
//    bool CloseExternal(bool force);
//    bool UpdateExternal(LODObject *lod, float lodbias);
//    void StartExternal(const char *pathname, LODObject *obj, float lodbias, bool twomons);
    static void InitPlugins();
    void UpdateWindow(const ObjectData *odata,CGCamera& cam);
    void SetTexturePath(const char *path);
    void ClearError() 
      {error=false;}
    virtual ~CInternalView();
    
    
    // Generated message map functions
  protected:
    //{{AFX_MSG(CInternalView)
    afx_msg void OnDestroy();
    afx_msg void OnPaint();
    afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INTERNALVIEW_H__CD57D708_CD70_4857_879C_1971F377966D__INCLUDED_)
