// Import3DS.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "Import3DS.h"
#include "Config.h"
#include "SmoothGroups.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImport3DS dialog

#ifndef ListView_SetCheckState
   #define ListView_SetCheckState(hwndLV, i, fCheck) \
      ListView_SetItemState(hwndLV, i, \
      INDEXTOSTATEIMAGEMASK((fCheck)+1), LVIS_STATEIMAGEMASK)
#endif


int CImport3DSDlg::SObjectInfo::defaultframecount=100;

CImport3DSDlg::CImport3DSDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CImport3DSDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CImport3DS)
    maxFrame = 0;
    minFrame = 0;
    framereport = _T("");
    noanim = TRUE;
    mattexname=TRUE;
    scalemag = 1;
    scalemin = 1000;
    convtex = TRUE;
    texpath = _T("");
	nosquarize = FALSE;
	nomerge = FALSE;
	//}}AFX_DATA_INIT
    objlist=NULL;
    _itemNoChange=false;
    }










void CImport3DSDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CImport3DS)
  DDX_Control(pDX, IDC_STATICTEXPREFIX, wStaticPrefix);
  DDX_Control(pDX, IDC_STATICTEXPATH, wStaticPath);
  DDX_Control(pDX, IDC_PATH, wPath);
  DDX_Control(pDX, IDC_PREFIX, wPrefix);
  DDX_Control(pDX, IDC_OBJECTLIST, wObjectList);
  DDX_Text(pDX, IDC_FRAMEMAX, maxFrame);
  DDX_Text(pDX, IDC_FRAMEMIN, minFrame);
  DDX_Text(pDX, IDC_FRAMEREPORT, framereport);
  DDX_Check(pDX, IDC_NOANIM, noanim);
  DDX_Check(pDX, IDC_MATNAME, mattexname);
  DDX_Text(pDX, IDC_SCALEMAG, scalemag);
  DDX_Text(pDX, IDC_SCALEMIN, scalemin);
  DDX_Check(pDX, IDC_CONTEX, convtex);
  DDX_Text(pDX, IDC_PATH, texpath);
	DDX_Check(pDX, IDS_NOSQUARIZE, nosquarize);
	DDX_Check(pDX, IDS_NOMERGE, nomerge);
	//}}AFX_DATA_MAP
  }










BEGIN_MESSAGE_MAP(CImport3DSDlg, CDialog)
//{{AFX_MSG_MAP(CImport3DS)
ON_WM_DESTROY()
ON_NOTIFY(LVN_ITEMCHANGED, IDC_OBJECTLIST, OnItemchangedObjectlist)
ON_BN_CLICKED(IDC_SETLOD, OnSetlod)
ON_NOTIFY(NM_DBLCLK, IDC_OBJECTLIST, OnDblclkObjectlist)
ON_BN_CLICKED(IDC_CONTEX, OnContex)
ON_EN_CHANGE(IDC_PREFIX, OnChangePrefix)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CImport3DS message handlers
    /*
template<class T>
T* FindNode(T *& set,C3dsChunk *chunk, CHUNKID chunkid)
  {
  return set=static_cast<T *>(chunk->FindChunkSub(chunkid));
  }*/
    
    BOOL CImport3DSDlg::ImportError(int ids,...)
      {
      va_list list;
      va_start(list,ids);
      CString a;a.LoadString(ids);
      CString d;d.FormatV(a,list);
      AfxMessageBox(d,MB_OK|MB_ICONEXCLAMATION);
      PostMessage(WM_COMMAND,IDCANCEL);
      return FALSE;
      }










static void InterpolateMatrices(CGMatrix &left, CGMatrix &right, CGMatrix &out, int lf, int rf, int k)
  {
  float p=(float)(k-lf)/(float)(rf-lf);
  CGMatrix pp;
  for (int i=0;i<4;i++)
    for (int j=0;j<4;j++)
      pp[i][j]=left[i][j]+(right[i][j]-left[i][j])*p;
  out*=pp;
  }










BOOL CImport3DSDlg::OnInitDialog() 
  {
  C3dsChunk *kfdata;
  C3dsChunk *object_node_tag;
  C3dsChunk *mdata;
  C3dsNamedObject *named_object;
  int count,i;
  ProgressBar<int> pb(100);
  
  pb.SetNextPos(10);
  CDialog::OnInitDialog();
  objlist=NULL;
  wObjectList.InsertColumn(0,WString(IDS_3DSOBJECT),LVCFMT_LEFT,130,0);
  wObjectList.InsertColumn(1,WString(IDS_3DSLOD),LVCFMT_CENTER,130,0);
  ListView_SetExtendedListViewStyleEx(wObjectList,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_CHECKBOXES);
  ifstream instr(f3ds,ios::binary|ios::in);
  if (!instr) return ImportError(IDS_3DSIMPFILEERROR);
  pb.SetNextPos(60);
  pb.ReportState(IDS_3DSIMPORTREADINGFILE);
  if (master.Read3ds(instr)==false) return ImportError(IDS_3DSINVALID);
  pb.ReportState(IDS_3DSIMPORTEXAMINE);
  // TODO: Add extra initialization here
  mdata=master.FindChunkSub(MDATA);
  if  (mdata==NULL) return ImportError(IDS_3DSMISSINGMDATA);
  kfdata=master.FindChunkSub(KFDATA);
  if (kfdata==NULL) return ImportError(IDS_3DSMISSINGKFDATA);
  count=0;
  for (object_node_tag=kfdata->FindChunkSub(OBJECT_NODE_TAG);object_node_tag!=NULL;count++,object_node_tag=object_node_tag->FindChunkNext(OBJECT_NODE_TAG));
  if (count==0) return ImportError(IDS_3DSNOOBJECTS);
    {
    C3dsKFSegment *segment;      
    segment=static_cast<C3dsKFSegment *>(kfdata->FindChunkSub(KFSEG));
    if (segment==NULL)
      {
      minFrame=0;
      maxFrame=0;
      }
    else
      {
      minFrame=segment->BeginFrame();
      maxFrame=segment->EndFrame();
      }
    frames=maxFrame-minFrame+1;
    objlist->defaultframecount=frames;
    objlist=new SObjectInfo[count];
    }
  objects=count;    
  for (i=0,object_node_tag=kfdata->FindChunkSub(OBJECT_NODE_TAG);i<count;i++,object_node_tag=object_node_tag->FindChunkNext(OBJECT_NODE_TAG))
    {    
    pb.SetNextPos(60+(i+1)*40/objects);
    C3dsNodeHdr *node_hdr;
    C3dsXYZTrack *scl_track_tag;
    C3dsRotTrack *rot_track_tag;
    C3dsXYZTrack *pos_track_tag;
    
    node_hdr=static_cast<C3dsNodeHdr *>(object_node_tag->FindChunkSub(NODE_HDR));
    if (node_hdr==NULL) return ImportError(IDS_3DSMISSINGNODEHDR);
    objlist[i].parent=node_hdr->GetParent();
    const char *objname=node_hdr->GetName();
    named_object=static_cast<C3dsNamedObject *>(mdata->FindChunkSub(NAMED_OBJECT));
    while (named_object && strcmp(named_object->GetName(),objname))
      named_object=static_cast<C3dsNamedObject *>(named_object->FindChunkNext(NAMED_OBJECT));
    if (strncmp(objname,"$$$",3)!=0 && named_object==NULL) return ImportError(IDS_3DSMESHNOTFOUND,objname);
    objlist[i].objref=named_object;
    if ((scl_track_tag=static_cast<C3dsXYZTrack *>(object_node_tag->FindChunkSub(SCL_TRACK_TAG)))!=NULL)
      {
      CGMatrix beg(true);
      S3DSTRACKHD &hd=scl_track_tag->GetHeader();
      int frms=hd.records;
      int lf=-1,nf=0;
      for (int j=0;j<frms;j++)
        {
        S3DSPOSTRACK *track=(*scl_track_tag)[j];
        nf=track->frame-minFrame;        
        CGMatrix ppp; ppp.Zoom(track->xpos, track->ypos, track->zpos);
        for (int k=lf+1;k<=nf;k++)  if (k>=0 && k<frames) InterpolateMatrices(beg,ppp,objlist[i].mlist[k],lf,nf,k);
        beg=ppp;
        lf=nf;
        }
      for (int k=nf+1;k<frames;k++) if (k>=0 && k<frames) objlist[i].mlist[k]*=beg;
      }
    if ((rot_track_tag=static_cast<C3dsRotTrack *>(object_node_tag->FindChunkSub(ROT_TRACK_TAG)))!=NULL)
      {
      CGMatrix mbeg;
      S3DSTRACKHD &hd=rot_track_tag->GetHeader();
      int frms=hd.records;
      int lf=-1,nf=0;
      for (int j=0;j<frms;j++)
        {
        S3DSROTTRACK *track=(*rot_track_tag)[j];
        CGMatrix ppp; 
        nf=track->frame;		  
        for (int k=lf+1;k<=nf;k++)  
          {
          float z;
          if (lf==-1) z=1.0f;else z=(float)(k-lf)/(float)(nf-lf);
          float rot=z*track->radius;
          HVECTOR v1;
          v1[XVAL]=-track->xpos;
          v1[YVAL]=-track->ypos;
          v1[ZVAL]=-track->zpos;
          mxRotateAroundAxis(v1,rot,ppp);			
          if (k>=0 && k<frames) objlist[i].mlist[k]*=CGMatrix(mbeg,ppp);
          }
        mbeg*=ppp;
        lf=nf;
        }
      for (int k=nf+1;k<frames;k++) if (k>=0 && k<frames) objlist[i].mlist[k]*=mbeg;
      }
    if ((pos_track_tag=static_cast<C3dsXYZTrack *>(object_node_tag->FindChunkSub(POS_TRACK_TAG)))!=NULL)
      {
      CGMatrix beg(true);
      S3DSTRACKHD &hd=pos_track_tag->GetHeader();
      int frms=hd.records;
      int lf=-1,nf=0;
      for (int j=0;j<frms;j++)
        {
        S3DSPOSTRACK *track=(*pos_track_tag)[j];
        nf=track->frame;
        CGMatrix ppp; ppp.Translate(track->xpos, track->ypos, track->zpos);        
        for (int k=lf+1;k<=nf;k++) if (k>=0 && k<frames)  InterpolateMatrices(beg,ppp,objlist[i].mlist[k],lf,nf,lf==-1?nf:k);
        beg=ppp;
        lf=nf;
        }
      for (int k=nf+1;k<frames;k++) if (k>=0 && k<frames) objlist[i].mlist[k]*=beg;
      }			  
    /*Calculate hierarchy */
    int parent=objlist[i].parent;
    if (parent>=0)  for (int k=0;k<frames;k++) objlist[i].mlist[k]*=objlist[parent].mlist[k];
    int itm=wObjectList.InsertItem(i,objname,0);	  
    wObjectList.SetItemText(itm,1,LODResolToName(objlist[i].objlod));		  
    }	
  for (i=0,object_node_tag=kfdata->FindChunkSub(OBJECT_NODE_TAG);i<count;i++,object_node_tag=object_node_tag->FindChunkNext(OBJECT_NODE_TAG))
  {
    C3dsPivot *pivot;
    if ((pivot=static_cast<C3dsPivot *>(object_node_tag->FindChunkSub(PIVOT)))!=NULL)
      {
        S3DSPIVOT *pv=pivot->GetPivot();
        CGMatrix pvm,cur;
        pvm.Translate(-pv->pivotx,-pv->pivoty,-pv->pivotz);
        for (int j=0;j<frames;j++) 
        {
          cur=objlist[i].mlist[j];
          objlist[i].mlist[j].Mul(pvm,cur);
        }  
      }
  }
  framereport.Format("%d - %d",minFrame,maxFrame);
  UpdateData(FALSE);
  wObjectList.SetExtendedStyle(wObjectList.GetExtendedStyle()|LVS_EX_FULLROWSELECT);
  framebeg=minFrame;
  frameend=maxFrame;
//  wPrefix.SetWindowText(config->GetString(IDS_CFGTEXTUREPREFIX));
  OnChangePrefix() ;
  OnContex() ;
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }










void CImport3DSDlg::OnDestroy() 
  {
  delete [] objlist;
  objlist=NULL;
  CDialog::OnDestroy();
  }










void CImport3DSDlg::OnItemchangedObjectlist(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  NMLISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
  ::EnableWindow(::GetDlgItem(*this,IDC_SETLOD),wObjectList.GetNextItem(-1,LVNI_SELECTED )!=-1);
  if ((pNMListView->uNewState ^ pNMListView->uOldState) & LVIS_STATEIMAGEMASK)
    {
    if (!_itemNoChange)
      {
      _itemNoChange=true;
      POSITION pos=wObjectList.GetFirstSelectedItemPosition();
      while (pos)
        {
        int i=wObjectList.GetNextSelectedItem(pos);
        wObjectList.SetItemState(i,pNMListView->uNewState,LVIS_STATEIMAGEMASK);
        }
      _itemNoChange=false;
      }
    }
  *pResult = 0;
  }










#include "LodConfig.h"

void CImport3DSDlg::OnSetlod() 
  {
  CLodConfig dlg;
  dlg.lod=0;
  if (dlg.DoModal()==IDOK)
    {
    for (int i=wObjectList.GetNextItem(-1,LVNI_SELECTED);i!=-1;i=wObjectList.GetNextItem(i,LVNI_SELECTED))
      {
      objlist[i].objlod=dlg.lod;
      wObjectList.SetItemText(i,1,LODResolToName(objlist[i].objlod));
      }
    }
  }










void CImport3DSDlg::OnOK() 
  {
  if (UpdateData()==FALSE) return;
  if (noanim) minFrame=maxFrame=framebeg;
  if (minFrame<framebeg || minFrame>frameend || maxFrame<framebeg || maxFrame>frameend 
    || minFrame>maxFrame) 
      {
      AfxMessageBox(IDS_3DSFRAMESOUTOFRANGE);
      return;
      }
  texconvdlg.basepath=this->f3ds;
  texconvdlg.mdata=master.FindChunkSub(MDATA);
  wPrefix.GetWindowText(texconvdlg.prefix);
  texconvdlg.targetpath=this->texpath;
  texconvdlg.usematname=mattexname!=FALSE;
  if (this->convtex) 
    if (texconvdlg.DoModal()==IDCANCEL) return;
  for (int i=0;i<objects;i++)
    {
    ObjectData *obj;
    progresbeg=i*1000/objects;
    progressize=1000/objects;
    int fnd=lod->FindLevelExact(objlist[i].objlod);
    if (fnd==-1) 
      {
      obj=new ObjectData();	  
      fnd=lod->InsertLevel(obj,objlist[i].objlod);
      }
    else 
      obj=lod->Level(fnd);    
    Import3DSObject(objlist[i],obj);
    if (ListView_GetCheckState(wObjectList,i))
      {            
      obj->SaveNamedSel(wObjectList.GetItemText(i,0));
      }
    RecalcNormals2(obj,true);    
    }
  if (noanim)   
  {
    for (int i=0;i<objects;i++)
    {    
      ObjectData *obj;
      int fnd=lod->FindLevelExact(objlist[i].objlod);
      if (fnd!=-1)
        {
        obj=lod->Level(fnd);    
        if (obj->NAnimations()==1)
          obj->DeleteAllAnimations();
        }
    }
  }
  CDialog::OnOK();
  }










static const char *GetMaterialTexture(const char *matname, C3dsChunk *master)
  {
  C3dsChunk *mdata=master->FindChunkSub(MDATA);
  C3dsChunk *mat_entry=mdata->FindChunkSub(MAT_ENTRY);
  while (mat_entry)
    {
    C3dsNamedObject *mat_name=static_cast<C3dsNamedObject *>(mat_entry->FindChunkSub(MAT_NAME));
    if (strcmp(matname,mat_name->GetName())==0)
      {
      C3dsChunk *mat_texmap=mat_entry->FindChunkSub(MAT_TEXMAP);
      if (mat_texmap==NULL) return NULL;
      C3dsNamedObject *mat_mapname=static_cast<C3dsNamedObject *>(mat_texmap->FindChunkSub(MAT_MAPNAME));
      if (mat_mapname==NULL) return NULL;
      return mat_mapname->GetName();
      }
    mat_entry=mat_entry->FindChunkNext(MAT_ENTRY);
    }
  return NULL;
  }










void CImport3DSDlg::Import3DSObject(SObjectInfo &nfo, ObjectData *obj)
  {
  ProgressBar<int> pb(1000);
  if (nfo.objref==NULL) return;
  float scale=(float)scalemag/(float)scalemin;
  CGMatrix final,help;
  final.Zoom(scale,scale,scale);
  help.Zero();
  help[0][0]=1;
  help[1][2]=1;
  help[2][1]=1;
  help[3][3]=1;
  final*=help;
  for (int k=0;k<frames;k++) nfo.mlist[k]*=final;
  HMATRIX mm,imm;  
  pb.ReportState(IDS_3DSIMPORTINGOBJECT,nfo.objref->GetName());
  C3dsChunk *n_tri_object=nfo.objref->FindChunkSub(N_TRI_OBJECT);
  if (n_tri_object==NULL) 
    {ImportError(IDS_3DSNTRIOBJECTERROR,nfo.objref->GetName());return;}
  C3dsObjectMatrix *mesh_matrix=static_cast<C3dsObjectMatrix *>(n_tri_object->FindChunkSub(MESH_MATRIX));
  if (mesh_matrix!=NULL)
    {
    mesh_matrix->GetMatrix(mm);
    InverzeMatice(mm,imm);
    }
  else
    JednotkovaMatice(imm);
  int indexoffs=obj->NPoints();
  int faceoffs=obj->NFaces();
  C3dsPointArr *point_array=static_cast<C3dsPointArr *>(n_tri_object->FindChunkSub(POINT_ARRAY));
  if (point_array==NULL) 
    {ImportError(IDS_3DSNOPOINTSINMESH,nfo.objref->GetName());return;}
  C3dsFaceArr *face_array=static_cast<C3dsFaceArr *>(n_tri_object->FindChunkSub(FACE_ARRAY));
  if (face_array==NULL) 
    {ImportError(IDS_3DSNOFACESINMESH,nfo.objref->GetName());return;}
  C3dsTexCoord *tex_verts=static_cast<C3dsTexCoord *>(n_tri_object->FindChunkSub(TEX_VERTS));
  C3dsSmoothGroups *smooth_grp=static_cast<C3dsSmoothGroups *>(n_tri_object->FindChunkSub(SMOOTH_GROUP));
  obj->ReservePoints(point_array->GetCount());
  obj->ReserveFaces(face_array->GetCount());
  int pt;
  int *ptarray=new int[point_array->GetCount()];
  obj->ClearSelection();
  int maxpt=0;
  pb.AdvanceNext(250);
  {
    ProgressBar<int> pb(point_array->GetCount());
  for (pt=0;pt<point_array->GetCount();pt++)
    {
      pb.AdvanceNext(1);
    S3DSVECTOR *vx=(*point_array)[pt];
    HVECTOR vt;
    TransformVector(imm,mxVector3(vx->x,vx->y,vx->z),vt);
    PosT pos;
    pos[0]=vt[0];
    pos[1]=vt[1];
    pos[2]=vt[2];
    pos.flags=0;
    int i=pt;
    if (!nomerge)
      for (i=0;i<maxpt;i++)
        {
        PosT &q=obj->Point(i+indexoffs);
        if ((pos-q).SquareSize()<=0.000001f) 
          break;
        }
    ptarray[pt]=i;
    if (i>=maxpt) maxpt=i+1;
    PosT &curpos=obj->Point(i+indexoffs);
    curpos=pos;
    obj->PointSelect(i+indexoffs);
    }    
  }
  for (pt=point_array->GetCount()-1;pt>=maxpt;pt--) obj->DeletePoint(pt+indexoffs);
  int fc;
  pb.ReportState(IDS_3DSIMPORTFACES,nfo.objref->GetName());
  pb.AdvanceNext(50);
  for (fc=0;fc<face_array->GetCount();fc++)
    {
    S3DSTRIANGLE *trg=(*face_array)[fc];
    FaceT fcc(obj,fc+faceoffs);
    fcc.SetPoint(0,trg->v1);
    fcc.SetPoint(1,trg->v2);
    fcc.SetPoint(2,trg->v3);
    if (tex_verts)
      {
      for (int i=0;i<3;i++)
        {
        S3DSTEXCOORD *tutv=(*tex_verts)[fcc.GetPoint(i)];
        fcc.SetUV(i,tutv->tu,1-tutv->tv);	  
        }
      }
      {
      for (int i=0;i<3;i++) fcc.SetPoint(i,ptarray[fcc.GetPoint(i)]+indexoffs);
      fcc.SetN(3);
      }
    obj->FaceSelect(fc+faceoffs);
    }
  if (smooth_grp!=NULL)
    HandleSmoothGroups((DWORD *)(smooth_grp->GetBuffer()),*obj,true);
  delete [] ptarray;
  C3dsData *msh_mat_group=static_cast<C3dsData *>(face_array->FindChunkSub(MSH_MAT_GROUP));
  pb.AdvanceNext(50);
  while (msh_mat_group)
    {
    char *name=(char *)msh_mat_group->GetBuffer();
    unsigned short *bufend=(unsigned short *)(name+msh_mat_group->GetSize());
    unsigned short *items=(unsigned short *)(name+strlen(name)+1);
    unsigned short *data=items+1;
    const char *texname;
    if (convtex)
      texname=texconvdlg.GetTextureName(name);
    else
      texname=GetMaterialTexture(name, &master);
    if (texname)
      {
      unsigned short i;
      for (i=0;i<*items && (data+i)<bufend;i++)
        {
        FaceT fc(obj,data[i]+faceoffs);
        fc.SetTexture(texname);
        }
      }
    msh_mat_group=static_cast<C3dsData *>(msh_mat_group->FindChunkNext(MSH_MAT_GROUP));
    }
//  obj->MergePoints(0.001,true);
  pb.ReportState( IDS_3DSIMPORTOPTFACES,nfo.objref->GetName());
  for (fc=faceoffs;fc<obj->NFaces();fc++)
    {
    FaceT fcc(obj,fc);
    if (fcc.GetPoint(0)==fcc.GetPoint(1) || fcc.GetPoint(1)==fcc.GetPoint(2) || fcc.GetPoint(2)==fcc.GetPoint(0))
      obj->DeleteFace(fc--);
    }
  pb.AdvanceNext(100);
  if (!nosquarize) obj->Squarize(false);
  int frames=maxFrame-minFrame+1;
  int curframes=obj->NAnimations();
  float timedif=1.0f;
  float lasttime=-1.0f;
  int curanim=obj->CurrentAnimation();
  obj->UseAnimation(-1);
  pb.ReportState(IDS_3DSIMPORTANIM,nfo.objref->GetName());
  pb.SetNextPos(1000);
  {
    ProgressBar<int> pb(frames);
  for (int i=0;i<frames;i++) 
    {	
      pb.AdvanceNext(1);
    bool newanim;
    AnimationPhase *anim;
    if (i>=curframes) 
      {
      newanim=true;
      anim=new AnimationPhase(obj);
      obj->RedefineAnimation(*anim);
      anim->SetTime(lasttime+timedif);
      }
    else
      {
      newanim=false;
      anim=obj->GetAnimation(i);
      timedif=anim->GetTime()-lasttime;
      }
    anim->Validate();
    int cnt=obj->NPoints();
    for (pt=0;pt<cnt;pt++) if (obj->PointSelected(pt))
      {
      PosT ps=obj->Point(pt);
      HVECTOR v1;
      TransformVector(nfo.mlist[i-framebeg+minFrame],mxVector3(ps[0],ps[1],ps[2]),v1);
      ps[0]=v1[0];
      ps[1]=v1[1];
      ps[2]=v1[2];
      (*anim)[pt]=ps;
      }
    lasttime=anim->GetTime();
    if (newanim)
      {
      obj->AddAnimation(*anim);
      delete anim;
      }
    }
  }
  if (curanim>=0 && curanim<curframes)  obj->UseAnimation(curanim);
  else 
    obj->UseAnimation(0);
  //  obj->Optimize();
  //  MSG msg;
  //while (::PeekMessage(&msg,NULL,0,0,PM_REMOVE))DispatchMessage(&msg);
  }



















void CImport3DSDlg::OnDblclkObjectlist(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  // TODO: Add your control notification handler code here
  OnSetlod();
  *pResult = 0;
  }










void CImport3DSDlg::OnContex() 
  {
  BOOL p=IsDlgButtonChecked(IDC_CONTEX);
  wStaticPath.EnableWindow(p);
  wStaticPrefix.EnableWindow(p);
  wPath.EnableWindow(p);
  wPrefix.EnableWindow(p);
  }





void CImport3DSDlg::OnChangePrefix() 
  {
  CString pp;
  Pathname pt;
  wPrefix.GetWindowText(pp);  
  pt.SetDirectory(config->GetString(IDS_CFGPATHFORTEX));
  wPath.SetWindowText(pt.GetDrive()+pt.GetDirectory()+pp);
  }


