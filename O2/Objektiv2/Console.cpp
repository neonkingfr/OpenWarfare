// Console.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "Console.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CConsole dialog
                                 

CConsole::CConsole(CWnd* pParent /*=NULL*/)
  : CDialog(CConsole::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CConsole)
    consoleText = _T("");
    //}}AFX_DATA_INIT
	vCaption=NULL;
    }

//--------------------------------------------------

void CConsole::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CConsole)
  DDX_Text(pDX, IDC_CONSOLE, consoleText);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CConsole, CDialog)
  //{{AFX_MSG_MAP(CConsole)
	//}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CConsole message handlers
    

BOOL CConsole::OnInitDialog() 
{
	if (vCaption) SetWindowText(vCaption);
	CDialog::OnInitDialog();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
