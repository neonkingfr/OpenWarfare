// IDeletePointsTriangulate.h: interface for the CIDeletePointsTriangulate class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IDELETEPOINTSTRIANGULATE_H__2C4DF2E0_91B8_4852_B9AA_3F4E7A9430C2__INCLUDED_)
#define AFX_IDELETEPOINTSTRIANGULATE_H__2C4DF2E0_91B8_4852_B9AA_3F4E7A9430C2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ComInt.h"

class CIDeletePointsTriangulate : public CICommand  
{
public:
	CIDeletePointsTriangulate();
	virtual ~CIDeletePointsTriangulate();
    int Execute(LODObject *lodobj);

};

#endif // !defined(AFX_IDELETEPOINTSTRIANGULATE_H__2C4DF2E0_91B8_4852_B9AA_3F4E7A9430C2__INCLUDED_)
