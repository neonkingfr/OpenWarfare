
//#include "stdafx.h"
#include <string.h>
#include "geMatrix4.h"

geMatrix4&  geMatrix4::Translate(float x, float y, float z)
  {
  mx[0][0]=1;mx[0][1]=0;mx[0][2]=0;mx[0][3]=0;
  mx[1][0]=0;mx[1][1]=1;mx[1][2]=0;mx[1][3]=0;
  mx[2][0]=0;mx[2][1]=0;mx[2][2]=1;mx[2][3]=0;
  mx[3][0]=x;
  mx[3][1]=y;
  mx[3][2]=z;
  return *this;
  }

//--------------------------------------------------

geMatrix4&  geMatrix4::TranslateC(float x, float y, float z)
  {
  mx[3][0]+=x;
  mx[3][1]+=y;
  mx[3][2]+=z;
  return *this;
  }

//--------------------------------------------------

geMatrix4& geMatrix4::Scale(float x, float y, float z)
  {
  mx[0][0]=x;mx[0][1]=0;mx[0][2]=0;mx[0][3]=0;
  mx[1][0]=0;mx[1][1]=y;mx[1][2]=0;mx[1][3]=0;
  mx[2][0]=0;mx[2][1]=0;mx[2][2]=z;mx[2][3]=0;
  mx[3][0]=0;mx[3][1]=0;mx[3][2]=0;mx[3][3]=1;
  return *this;
  }

//--------------------------------------------------

geMatrix4& geMatrix4::ScaleC(float x, float y, float z)
  {  
  mx[0][0]*=x;
  mx[0][1]*=y;
  mx[0][2]*=z;
  mx[1][0]*=x;
  mx[1][1]*=y;
  mx[1][2]*=z;
  mx[2][0]*=x;
  mx[2][1]*=y;
  mx[2][2]*=z;
  mx[3][0]*=x;
  mx[3][1]*=y;
  mx[3][2]*=z;
  return *this;
  }

//--------------------------------------------------

geMatrix4& geMatrix4::Rotate(geAxis axis, float angle)
  {
  Identity();
  float coss=(float)cos(angle);
  float sins=(float)sin(angle);
  switch (axis)
    {
    case geAX:
    mx[1][1]=coss;
    mx[1][2]=sins;
    mx[2][1]=-sins;
    mx[2][2]=coss;
    break;
    case geAY:
    mx[0][0]=coss;
    mx[0][2]=sins;
    mx[2][0]=-sins;
    mx[2][2]=coss;
    break;
    case geAZ:
    mx[0][0]=coss;
    mx[0][1]=sins;
    mx[1][0]=-sins;
    mx[1][1]=coss;
    break;
    }
  return *this;
  }

//--------------------------------------------------

geMatrix4& geMatrix4::RotateC(geAxis axis, float angle)
  {
  geMatrix4 p;
  p.Rotate(axis,angle);
  return MultC(p);
  }

//--------------------------------------------------

geMatrix4& geMatrix4::Rotate(float x, float y, float z, float angle)
  {  
  float cfi, sfi, x2, y2, z2;
  x2=x*x;
  y2=y*y;
  z2=z*z;
  cfi=(float)cos(angle);
  sfi=(float)sin(angle);
  mx[0][0]=x2 + (1.0f - x2)*cfi;
  mx[1][0]=-z*sfi + x*y*(1-cfi);
  mx[2][0]=y*sfi + x*z*(1-cfi);
  mx[3][0]=0.0f;
  
  mx[0][1]=z*sfi + x*y*(1-cfi);
  mx[1][1]=y2 +(1.0f - y2)*cfi;
  mx[2][1]=-x*sfi + y*z*(1-cfi);
  mx[3][1]=0.0f;
  
  mx[0][2]=-y*sfi + x*z*(1-cfi);
  mx[1][2]=x*sfi + y*z*(1-cfi);
  mx[2][2]=z2 + (1.0f - z2)*cfi;
  mx[3][2]=0.0f;
  
  mx[0][3]=0.0f;
  mx[1][3]=0.0f;
  mx[2][3]=0.0f;
  mx[3][3]=1.0f;
  return *this;
  }

//--------------------------------------------------

geMatrix4& geMatrix4::RotateC(float x, float y, float z, float angle)
  {
  geMatrix4 p;
  p.Rotate(x, y, z, angle);
  return MultC(p);
  }

//--------------------------------------------------

geMatrix4& geMatrix4::MultC(const geMatrix4& other)
  {
  other.Transform4(mx[0],mx[0]);
  other.Transform4(mx[1],mx[1]);
  other.Transform4(mx[2],mx[2]);
  other.Transform4(mx[3],mx[3]);
  return *this;
  }

//--------------------------------------------------

geMatrix4& geMatrix4::Mult(const geMatrix4& left,const geMatrix4& right)
  {
  right.Transform4(left.mx[0],mx[0]);
  right.Transform4(left.mx[1],mx[1]);
  right.Transform4(left.mx[2],mx[2]);
  right.Transform4(left.mx[3],mx[3]);
  return *this;
  }

//--------------------------------------------------

geMatrix4& geMatrix4::Projection(float front, float back, float fov, float aspec)
  {
  float cotarg=(fov/2.0f);
  float cot=(float)(cos(cotarg)/sin(cotarg));
  float Q=back/(back-front);
  
  memset(mx,0,sizeof(mx));
  mx[0][0]=cot;
  mx[1][1]=cot*aspec;
  mx[2][2]=Q;
  mx[2][3]=1.0f;
  mx[3][2]=-Q*front;
  mx[3][3]=0.0f;
  return *this;
  }

//--------------------------------------------------

geMatrix4& geMatrix4::ProjectionC(float front, float back, float fov, float aspec)
  {
  geMatrix4 out;
  out.Projection(front,back,fov,aspec);
  return MultC(out);
  }

//--------------------------------------------------

/*
************************************************************
*
* input:
* mat - pointer to array of 16 floats (source matrix)
* output:
* dst - pointer to array of 16 floats (invert matrix)
*
*************************************************************/
static void geInvertMatrix(const float *mat, float *dst)
  {
  float tmp[12]; /* temp array for pairs */
  float src[16]; /* array of transpose source matrix */
  float det; /* determinant */
  int i;
  int j;
  /* transpose matrix */
  for ( i = 0; i < 4; i++) 
    {
    src[i] = mat[i*4];
    src[i + 4] = mat[i*4 + 1];
    src[i + 8] = mat[i*4 + 2];
    src[i + 12] = mat[i*4 + 3];
    }
  /* calculate pairs for first 8 elements (cofactors) */
  tmp[0] = src[10] * src[15];
  tmp[1] = src[11] * src[14];
  tmp[2] = src[9] * src[15];
  tmp[3] = src[11] * src[13];
  tmp[4] = src[9] * src[14];
  tmp[5] = src[10] * src[13];
  tmp[6] = src[8] * src[15];
  tmp[7] = src[11] * src[12];
  tmp[8] = src[8] * src[14];
  tmp[9] = src[10] * src[12];
  tmp[10] = src[8] * src[13];
  tmp[11] = src[9] * src[12];
  /* calculate first 8 elements (cofactors) */
  dst[0] = tmp[0]*src[5] + tmp[3]*src[6] + tmp[4]*src[7];
  dst[0] -= tmp[1]*src[5] + tmp[2]*src[6] + tmp[5]*src[7];
  dst[1] = tmp[1]*src[4] + tmp[6]*src[6] + tmp[9]*src[7];
  dst[1] -= tmp[0]*src[4] + tmp[7]*src[6] + tmp[8]*src[7];
  dst[2] = tmp[2]*src[4] + tmp[7]*src[5] + tmp[10]*src[7];
  dst[2] -= tmp[3]*src[4] + tmp[6]*src[5] + tmp[11]*src[7];
  dst[3] = tmp[5]*src[4] + tmp[8]*src[5] + tmp[11]*src[6];
  dst[3] -= tmp[4]*src[4] + tmp[9]*src[5] + tmp[10]*src[6];
  dst[4] = tmp[1]*src[1] + tmp[2]*src[2] + tmp[5]*src[3];
  dst[4] -= tmp[0]*src[1] + tmp[3]*src[2] + tmp[4]*src[3];
  dst[5] = tmp[0]*src[0] + tmp[7]*src[2] + tmp[8]*src[3];
  dst[5] -= tmp[1]*src[0] + tmp[6]*src[2] + tmp[9]*src[3];
  dst[6] = tmp[3]*src[0] + tmp[6]*src[1] + tmp[11]*src[3];
  dst[6] -= tmp[2]*src[0] + tmp[7]*src[1] + tmp[10]*src[3];
  dst[7] = tmp[4]*src[0] + tmp[9]*src[1] + tmp[10]*src[2];
  dst[7] -= tmp[5]*src[0] + tmp[8]*src[1] + tmp[11]*src[2];
  /* calculate pairs for second 8 elements (cofactors) */
  tmp[0] = src[2]*src[7];
  tmp[1] = src[3]*src[6];
  tmp[2] = src[1]*src[7];
  tmp[3] = src[3]*src[5];
  tmp[4] = src[1]*src[6];
  tmp[5] = src[2]*src[5];
  tmp[6] = src[0]*src[7];
  tmp[7] = src[3]*src[4];
  tmp[8] = src[0]*src[6];
  tmp[9] = src[2]*src[4];
  tmp[10] = src[0]*src[5];
  tmp[11] = src[1]*src[4];
  /* calculate second 8 elements (cofactors) */
  dst[8] = tmp[0]*src[13] + tmp[3]*src[14] + tmp[4]*src[15];
  dst[8] -= tmp[1]*src[13] + tmp[2]*src[14] + tmp[5]*src[15];
  dst[9] = tmp[1]*src[12] + tmp[6]*src[14] + tmp[9]*src[15];
  dst[9] -= tmp[0]*src[12] + tmp[7]*src[14] + tmp[8]*src[15];
  dst[10] = tmp[2]*src[12] + tmp[7]*src[13] + tmp[10]*src[15];
  dst[10]-= tmp[3]*src[12] + tmp[6]*src[13] + tmp[11]*src[15];
  dst[11] = tmp[5]*src[12] + tmp[8]*src[13] + tmp[11]*src[14];
  dst[11]-= tmp[4]*src[12] + tmp[9]*src[13] + tmp[10]*src[14];
  dst[12] = tmp[2]*src[10] + tmp[5]*src[11] + tmp[1]*src[9];
  dst[12]-= tmp[4]*src[11] + tmp[0]*src[9] + tmp[3]*src[10];
  dst[13] = tmp[8]*src[11] + tmp[0]*src[8] + tmp[7]*src[10];
  dst[13]-= tmp[6]*src[10] + tmp[9]*src[11] + tmp[1]*src[8];
  dst[14] = tmp[6]*src[9] + tmp[11]*src[11] + tmp[3]*src[8];
  dst[14]-= tmp[10]*src[11] + tmp[2]*src[8] + tmp[7]*src[9];
  dst[15] = tmp[10]*src[10] + tmp[4]*src[8] + tmp[9]*src[9];
  dst[15]-= tmp[8]*src[9] + tmp[11]*src[10] + tmp[5]*src[8];
  /* calculate determinant */
  det=src[0]*dst[0]+src[1]*dst[1]+src[2]*dst[2]+src[3]*dst[3];
  /* calculate matrix inverse */
  det = 1/det;
  for (  j = 0; j < 16; j++)
    dst[j] *= det;
  }

//--------------------------------------------------

geMatrix4 geMatrix4::Invert() const
  {
  geMatrix4 other;
  geInvertMatrix((float *)mx,(float *)other.mx);
  return other;
  }

//--------------------------------------------------

geMatrix4& geMatrix4::Invert(const geMatrix4& mm) //Create inverted version of matrix mm
  {
  geInvertMatrix((float *)mm.mx,(float *)mx);
  return *this;
  }

//--------------------------------------------------

geMatrix4& geMatrix4::InvertC(const geMatrix4& mm) //Mupltiply with inverted version of matrix mm 
  {
  geMatrix4 wrk;
  return MultC(wrk.Invert(mm));
  }

//--------------------------------------------------

void geMatrix4::Transform4(const float *in, float *out) const
  {
  register float a=in[0];
  register float b=in[1];
  register float c=in[2];
  register float d=in[3];
  
  out[0]=a*mx[0][0]+b*mx[1][0]+c*mx[2][0]+d*mx[3][0];
  out[1]=a*mx[0][1]+b*mx[1][1]+c*mx[2][1]+d*mx[3][1];
  out[2]=a*mx[0][2]+b*mx[1][2]+c*mx[2][2]+d*mx[3][2];
  out[3]=a*mx[0][3]+b*mx[1][3]+c*mx[2][3]+d*mx[3][3];
  }

//--------------------------------------------------

float geMatrix4::Transform3w(const float *v, float *z) const
  {
  register float a=v[0];
  register float b=v[1];
  register float c=v[2];
  register float d=1;
  
  z[0]=a*mx[0][0]+b*mx[1][0]+c*mx[2][0]+d*mx[3][0];
  z[1]=a*mx[0][1]+b*mx[1][1]+c*mx[2][1]+d*mx[3][1];
  z[2]=a*mx[0][2]+b*mx[1][2]+c*mx[2][2]+d*mx[3][2];
  return a*mx[0][3]+b*mx[1][3]+c*mx[2][3]+d*mx[3][3];
  }

//--------------------------------------------------

void geMatrix4::Transform3T(const float *v, float *z) const
  {
  register float a=v[0];
  register float b=v[1];
  register float c=v[2];  
  
  z[0]=a*mx[0][0]+b*mx[1][0]+c*mx[2][0]+mx[3][0];
  z[1]=a*mx[0][1]+b*mx[1][1]+c*mx[2][1]+mx[3][1];
  z[2]=a*mx[0][2]+b*mx[1][2]+c*mx[2][2]+mx[3][2];  
  }

//--------------------------------------------------

void geMatrix4::Transform3(const float *v, float *z) const
  {
  register float a=v[0];
  register float b=v[1];
  register float c=v[2];  
  
  z[0]=a*mx[0][0]+b*mx[1][0]+c*mx[2][0];
  z[1]=a*mx[0][1]+b*mx[1][1]+c*mx[2][1];
  z[2]=a*mx[0][2]+b*mx[1][2]+c*mx[2][2];
  }

//--------------------------------------------------

void geMatrix4::Transform2T(const float *v, float *z) const
  {
  register float a=v[0];
  register float b=v[1];
  
  z[0]=a*mx[0][0]+b*mx[1][0]+mx[2][0];
  z[1]=a*mx[0][1]+b*mx[1][1]+mx[2][1];
  }

//--------------------------------------------------

void geMatrix4::Transform2(const float *v, float *z) const
  {
  register float a=v[0];
  register float b=v[1];
  
  z[0]=a*mx[0][0]+b*mx[1][0];
  z[1]=a*mx[0][1]+b*mx[1][1];
  }

//--------------------------------------------------

void geMatrix4::Transform1T(const float *v, float *z) const
  {
  register float a=v[0];
  
  z[0]=a*mx[0][0]+mx[1][0];
  }

//--------------------------------------------------

geMatrix4& geMatrix4::LookAt(geVector4& from, geVector4& to, geVector4& top, bool correct)
  {
  geVector4 v1,v2;
  
  v1=(to-from);
  v1.Norm(1);
  v2=v1^top;
  if (correct) top=v2^v1;
  mx[0][0]=top.x;
  mx[1][0]=top.y;
  mx[2][0]=top.z;
  mx[3][0]=-top*from;
  mx[0][1]=v2.x;
  mx[1][1]=v2.y;
  mx[2][1]=v2.z;
  mx[3][1]=-v2*from;
  mx[0][2]=v1.x;
  mx[1][2]=v1.y;
  mx[2][2]=v1.z;
  mx[2][2]=-v1*from;
  mx[0][3]=mx[1][3]=mx[2][3]=0.0f;
  mx[3][3]=1.0f;
  return *this;  
  }

//--------------------------------------------------

geMatrix4& geMatrix4::Interpolate3T(geMatrix4 &left, geMatrix4 &right, float factor, float normscale)
  {
  x=Interpolate(left.x,right.x,factor);
  y=Interpolate(left.y,right.y,factor);
  z=Interpolate(left.z,right.z,factor);
  pos=Interpolate(left.pos,right.pos,factor);
  if (normscale!=0.0)
    {
    x.Norm(normscale);
    y.Norm(normscale);
    z.Norm(normscale);
    }
  x.Extend(0);
  y.Extend(0);
  z.Extend(0);
  return *this;
  }

//--------------------------------------------------

/*
// geMatrix.cpp: implementation of the geMatrix class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "geMatrix.h"
#include "invmat.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

static float jmx[4][4]=
  {
	{1,0,0,0},
	{0,1,0,0},
	{0,0,1,0},
	{0,0,0,1},
  };

geMatrix::geMatrix(bool inicialize)
  {
  if (inicialize) memcpy(mx,jmx,sizeof(mx));
  }

void geMatrix::TransformVector4(const float *v, float *z) const
  {
  register float a=v[0];
  register float b=v[1];
  register float c=v[2];
  register float d=v[3];

  z[0]=a*mx[0][0]+b*mx[1][0]+c*mx[2][0]+d*mx[3][0];
  z[1]=a*mx[0][1]+b*mx[1][1]+c*mx[2][1]+d*mx[3][1];
  z[2]=a*mx[0][2]+b*mx[1][2]+c*mx[2][2]+d*mx[3][2];
  z[3]=a*mx[0][3]+b*mx[1][3]+c*mx[2][3]+d*mx[3][3];
  }



float TransformVector3w(const float *v, float *z) const
  {
  register float a=v[0];
  register float b=v[1];
  register float c=v[2];
  register float d=1;

  z[0]=a*mx[0][0]+b*mx[1][0]+c*mx[2][0]+d*mx[3][0];
  z[1]=a*mx[0][1]+b*mx[1][1]+c*mx[2][1]+d*mx[3][1];
  z[2]=a*mx[0][2]+b*mx[1][2]+c*mx[2][2]+d*mx[3][2];
  return a*mx[0][3]+b*mx[1][3]+c*mx[2][3]+d*mx[3][3];
  }

void geMatrix::TransformVector3T(const float *v, float *z) const
  {
  register float a=v[0];
  register float b=v[1];
  register float c=v[2];  

  z[0]=a*mx[0][0]+b*mx[1][0]+c*mx[2][0]+mx[3][0];
  z[1]=a*mx[0][1]+b*mx[1][1]+c*mx[2][1]+mx[3][1];
  z[2]=a*mx[0][2]+b*mx[1][2]+c*mx[2][2]+mx[3][2];  
  }

void geMatrix::TransformVector3(const float *v, float *z) const
  {
  register float a=v[0];
  register float b=v[1];
  register float c=v[2];  

  z[0]=a*mx[0][0]+b*mx[1][0]+c*mx[2][0];
  z[1]=a*mx[0][1]+b*mx[1][1]+c*mx[2][1];
  z[2]=a*mx[0][2]+b*mx[1][2]+c*mx[2][2];  
  }

void geMatrix::TransformVector2T(const float *v, float *z) const
  {
  register float a=v[0];
  register float b=v[1];  

  z[0]=a*mx[0][0]+b*mx[1][0]+mx[2][0];
  z[1]=a*mx[0][1]+b*mx[1][1]+mx[2][1];
  }

void geMatrix::TransformVector2(const float *v, float *z) const
  {
  register float a=v[0];
  register float b=v[1];  

  z[0]=a*mx[0][0]+b*mx[1][0];
  z[1]=a*mx[0][1]+b*mx[1][1];
  }

geMatrix::geMatrix(const geMatrix &left, const geMatrix &right)
  {
  right.TransformVector4(left.mx[0],mx[0]);
  right.TransformVector4(left.mx[1],mx[1]);
  right.TransformVector4(left.mx[2],mx[2]);
  right.TransformVector4(left.mx[3],mx[3]);
  }

geMatrix& geMatrix::operator *=(const geMatrix &other)
  {
  other.TransformVector4(mx[0],mx[0]);
  other.TransformVector4(mx[1],mx[1]);
  other.TransformVector4(mx[2],mx[2]);
  other.TransformVector4(mx[3],mx[3]);
  return *this;
  }


geMatrix geMatrix::operator !() const
  {
  geMatrix out;

  geInvertMatrix((float *)mx,(float *)out.mx);
  return out;
  }

geMatrix& geMatrix::Identity()
  {
  memcpy(mx,jmx,sizeof(mx));
  return *this;
  }


geMatrix& geMatrix::Translate(LPHVECTOR vect)
  {
  memcpy(mx,jmx,sizeof(mx));
  mx[3][0]=vect[0];
  mx[3][1]=vect[1];
  mx[3][2]=vect[2];
  return *this;
  }

geMatrix&  geMatrix::Translate(float x, float y, float z)
  {
  memcpy(mx,jmx,sizeof(mx));
  mx[3][0]=x;
  mx[3][1]=y;
  mx[3][2]=z;
  return *this;
  }

geMatrix&  geMatrix::Rotate(GenieAxisEnum axis, float angle)
  {
  float coss=(float)cos(angle);
  float sins=(float)sin(angle);
  memcpy(mx,jmx,sizeof(mx));
  switch (axis)
	{
	case XAxis:
	    mx[1][1]=coss;
		mx[1][2]=sins;
		mx[2][1]=-sins;
		mx[2][2]=coss;
		break;
	case YAxis:
	    mx[0][0]=coss;
		mx[0][2]=sins;
		mx[2][0]=-sins;
		mx[2][2]=coss;
		break;
	case ZAxis:
	    mx[0][0]=coss;
		mx[0][1]=sins;
		mx[1][0]=-sins;
		mx[1][1]=coss;
		break;
	}
  return *this;
  }

geMatrix&  geMatrix::Rotate(HVECTOR axis, float angle)
  {
  float cfi, sfi, x2, y2, z2, x, y, z;
	x=axis[XVAL];
    x2=x*x;
    y=axis[YVAL];
    y2=y*y;
    z=axis[ZVAL];
    z2=z*z;
    cfi=(float)cos(angle);
    sfi=(float)sin(angle);
    mx[0][0]=x2 + (1.0f - x2)*cfi;
    mx[1][0]=-z*sfi + x*y*(1-cfi);
    mx[2][0]=y*sfi + x*z*(1-cfi);
    mx[3][0]=0.0f;

    mx[0][1]=z*sfi + x*y*(1-cfi);
    mx[1][1]=y2 +(1.0f - y2)*cfi;
    mx[2][1]=-x*sfi + y*z*(1-cfi);
    mx[3][1]=0.0f;

    mx[0][2]=-y*sfi + x*z*(1-cfi);
    mx[1][2]=x*sfi + y*z*(1-cfi);
    mx[2][2]=z2 + (1.0f - z2)*cfi;
    mx[3][2]=0.0f;

    mx[0][3]=0.0f;
    mx[1][3]=0.0f;
    mx[2][3]=0.0f;
    mx[3][3]=1.0f;
  return *this;
  }

geMatrix& geMatrix::Scale(HVECTOR v)
  {
  memcpy(mx,jmx,sizeof(mx));
  mx[0][0]=v[0];
  mx[1][1]=v[1];
  mx[2][2]=v[2];
  return *this;
  }

geMatrix& geMatrix::Scale(float x, float y, float z)
  {
  memcpy(mx,jmx,sizeof(mx));
  mx[0][0]=x;
  mx[1][1]=y;
  mx[2][2]=z;
  return *this;
  }
geMatrix& geMatrix::Projection(float front, float back, float fov)
  {
  float cotarg=(fov/2.0f);
  float cot=(float)(cos(cotarg)/sin(cotarg));
  float Q=back/(back-front);

  memset(mx,0,sizeof(mx));
  mx[0][0]=cot;
  mx[1][1]=cot;
  mx[2][2]=Q;
  mx[2][3]=1.0f;
  mx[3][2]=-Q*front;
  mx[3][3]=0.0f;
  return *this;
  }

geMatrix& geMatrix::LookAt(geVector& from, geVector& to, geVector& top)
  {
  geVector v1,v2;

  v1=!(to-from);
  v2=v1*top;
  mx[0][0]=top.x();
  mx[1][0]=top.y();
  mx[2][0]=top.z();
    mx[3][0]=-top&from;
  mx[0][1]=v2.x();
  mx[1][1]=v2.y();
  mx[2][1]=v2.z();
  mx[3][1]=-v2&from;
  mx[0][2]=v1.x();
  mx[1][2]=v1.y();
  mx[2][2]=v1.z();
  mx[2][2]=-v1&from;
  mx[0][3]=mx[1][3]=mx[2][3]=0.0f;
  mx[3][3]=1.0f;
  return *this;  
  }


void geMatrix::TransformVectorWeight4(const LPHVECTOR src, LPHVECTOR trg, float weight)const
  {
  geVector *out=(geVector *)trg;
  geVector temp;
  TransformVector4(src,temp);
  temp.Correct();
  (*out)+=temp*weight;
  }

void geMatrix::TransformVectorWeight3T(const LPHVECTOR src, LPHVECTOR trg, float weight)const
  {
  geVector *out=(geVector *)trg;
  geVector temp;
  TransformVector3T(src,temp);
  temp.Correct();
  (*out)+=temp*weight;
  }
void geMatrix::TransformVectorWeight3(const LPHVECTOR src, LPHVECTOR trg, float weight)const
  {
  geVector *out=(geVector *)trg;
  geVector temp;
  TransformVector3(src,temp);
  temp.Correct();
  (*out)+=temp*weight;
  }

void geMatrix::TransformVectorWeight2T(const LPHVECTOR src, LPHVECTOR trg, float weight)const
  {
  geVector *out=(geVector *)trg;
  geVector temp;
  TransformVector2T(src,temp);
  temp.Correct();
  (*out)+=temp*weight;
  }

void geMatrix::TransformVectorWeight2(const LPHVECTOR src, LPHVECTOR trg, float weight)const
  {
  geVector *out=(geVector *)trg;
  geVector temp;
  TransformVector2(src,temp);
  temp.Correct();
  (*out)+=temp*weight;
  }


#define IItem(i,j) mx[i][j]=left.mx[i][j]+(right.mx[i][j]-left.mx[i][j])*factor
#define ILine(i) {IItem(i,0);IItem(i,1);IItem(i,2);IItem(i,3);}

void geMatrix::Interpolate(const geMatrix &left, const geMatrix &right, float factor)
  {
  ILine(0);
  ILine(1);
  ILine(2);
  ILine(3);
  }

#undef IItem
#undef ILine


void geMatrix::SetRow(int row, geVector &vx)
  {
  memcpy(mx[row],vx,sizeof(mx[row][0])*4);
  }

void geMatrix::CreateFromKey(geVector &pos, geVector &axis, geVector &scale, float angle)
  {
  if (scale[XVal]!=1.0f || scale[YVal]!=1.0f || scale[ZVal]!=1.0f)
	{
	geMatrix temp;
	temp.Rotate(axis,angle);
	Scale(scale);
	(*this)*=temp;
	}
  else
	{
	Rotate(axis,angle);
	}
  SetRow(3,pos);
  }
*/

