// ComInt.h: interface for the CComInt class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COMINT_H__1CB312C4_D5AD_489C_963A_888084AE9865__INCLUDED_)
#define AFX_COMINT_H__1CB312C4_D5AD_489C_963A_888084AE9865__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CIHeader.h"
#include "Objektiv2.h"

class CICommand
  {
  protected:
    CICommand *next;
  public:
    bool recordable;
    bool record;
    bool marksave;
    LODObject *FindCache();
    virtual int Redo(LODObject *obj, int levels, CICommand **redonext);
    virtual CICommand * DeleteBlock();
    CICommand() 
      {next=NULL;recordable=true;record=false;marksave=true;}
    virtual ~CICommand() 
      {delete next;}
    virtual int Execute(LODObject *obj) 
      {return 0;}
    void Clear() 
      {delete next;next=NULL;}	
    virtual CICommand *Remove() 
      {CICommand *res=next;next=NULL;return res;}
    virtual bool IsSeparator() 
      {return false;}	
    CICommand *Add(CICommand *com) 
      {if (next) delete next; next=com;return com;}
    CICommand *Next() 
      {return next;}
    virtual void Save(ostream &str) 
      {AfxMessageBox("Aptempt to record abstract command");};
    virtual int Tag() const 
      {return -1;}
    void SaveWithTag(ostream& str)
      {int tag=Tag();if (tag!=0) 
        {datawr(str,tag);Save(str);}}
    void SaveBlock(ostream& str) 
      {if (record) 
        {SaveWithTag(str);record=false;}
      if (!IsSeparator()) next->SaveBlock(str);}
    virtual bool CanRemove(const CICommand *prev) 
      {return false;}
  };

//--------------------------------------------------

class CIBeginAction: public CICommand
  {
  protected:
    WString text;
    LODObject *state;
  public:
    CIBeginAction(WString text): CICommand(), text(text),state(NULL) 
      {marksave=false;};
    WString GetText() 
      {return text;}
    void SetText(WString t) 
      {text=t;}
    virtual CICommand *DeleteBlock() 
      {return next;}
    virtual int Redo(LODObject *obj, int levels, CICommand **redonext);
    virtual bool IsSeparator() 
      {return true;}
    virtual ~CIBeginAction() 
      {delete state;}
    void SaveState(LODObject *obj) 
      {delete state; state=new LODObject(*obj);}
    LODObject *State() 
      {return state;}
    virtual void Save(ostream &str) 
      {}
  };

//--------------------------------------------------

class CComInt
  {
  CICommand first;
  CICommand *last;
  LODObject *current;
  LODObject *lastobj;
  LODObject *nonserial; //nonserial action - such as selections, animations, 
  //where next action not depends on previous.
  WString openname; //last name of action
  int maxundo;
  int curlevels;
  CListBox *lsb;
  int Run(CICommand *com,bool addtoqueue);
  ostream *rcstr;
  bool longop;
  CWnd *notify;
  bool notifyack;
  int ncom;
  int locked;
  int actioncounter;
  CICommand *GetPrevAction(CICommand *prev);
  public:
	bool sectchanged; //true, if section count may change
    void AckNotify() 
      {notifyack=false;}
    int GetActionCounter(int reset);
    void SetNotify(CWnd *wnd, int com) 
      {notify=wnd;ncom=com;notifyack=false;}
    void SendNotify();
    bool IsLocked(bool error=true);
    __declspec(deprecated) void  SetLongOp() {}
    void LoadAction(istream &str);
    ostream *StopRecord();
    void RecordTo(ostream *rc) 
      {rcstr=rc;}
    ostream *GetRecordingStream() 
      {return rcstr;}
    void ChangeSelectionInList();
    void LoadToListbox();
    void UndoTo(int level);
    int DoRedo();
    int DoUndo();
    bool CanRedo() 
      {return last->Next()!=NULL && !locked;}
    bool CanUndo() 
      {return curlevels!=0 && !locked;}
    void SetUpdateListbox(CListBox *ls) 
      {lsb=ls;}
    void Lock(bool lock) 
      {if (lock) locked++;else if (locked) locked--;}
    int Run(CICommand *com) 
      {return Run(com,true);}
    int Begin(WString& text) 
      {
      protCheckValid();
      return Begin2(text);
      }
    int Begin2(WString &text);
    void SetObject(LODObject *obj);
    CComInt(int maxu);
    virtual ~CComInt();
  };



//--------------------------------------------------

#endif // !defined(AFX_COMINT_H__1CB312C4_D5AD_489C_963A_888084AE9865__INCLUDED_)
