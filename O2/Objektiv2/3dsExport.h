#if !defined(AFX_3DSEXPORT_H__BA6F43A9_3280_4ADE_9107_893B60CEB1C6__INCLUDED_)
#define AFX_3DSEXPORT_H__BA6F43A9_3280_4ADE_9107_893B60CEB1C6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// 3dsExport.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// C3dsExport dialog

#include "3ds_trc\3dsFile2.h"
#include <strstream>
#include <fstream>
#include "../ObjektivLib/Edges.h"
using namespace std;


struct S3dsExportFaceInfo
  {
  int faceindex;  //index of face relative to object
  C3dsNamedObject *object; //object with this face
  };



class C3dsExport : public CDialog
  {
  // Construction

  Pathname sname;
  C3dsPointArr *pta;
  C3dsFaceArr *fca;
  C3dsTexCoord *txe;
  CEdges incidence; //graph of smooth faces
  CEdges sharpi;	//graph of sharp faces
  unsigned long *smoothgrps;
  C3dsChunk *mat; //material list
  int *graph;
  int *indexlist;
  void CreateVertexList();
  ObjectData obj;
  void CreateMaterialList(C3dsChunk *mdata, S3dsExportFaceInfo *nfo, C3dsNamedObject **nmlist, int nmlistcnt);
  void CreateMaterialList(C3dsChunk *mdata, C3dsChunk *farray);
  void CreateGraph();
  public:
	  void ExportByTopology(C3dsChunk *mdata, ObjectData &obj);
	  bool CreateObjectFromSelection(Selection *sel, S3dsExportFaceInfo *nfo, int index);
	  void ExportObject(ObjectData &obj, C3dsChunk *mdata, C3dsChunk *triobject, int index);
    void CreateSmoothGroups();
    ObjectData *odata;
    C3dsExport(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(C3dsExport)
	enum { IDD = IDD_EXPORT3DS };
    CButton	BBrowse;
    BOOL	creat_material;
    BOOL	export_tex;
    CString	path;
    BOOL	create_smooth;
    BOOL	opt_vertices;
    int		dirselect;
    long	scale;
    BOOL	create_folder;
    BOOL	nonalphadetect;
	BOOL	byTopology;
	//}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(C3dsExport)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    void CheckDialogRules();
    
    // Generated message map functions
    //{{AFX_MSG(C3dsExport)
    virtual BOOL OnInitDialog();
    afx_msg void OnDialogChanged();
    afx_msg void OnBrowse();
    virtual void OnOK();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_3DSEXPORT_H__BA6F43A9_3280_4ADE_9107_893B60CEB1C6__INCLUDED_)
