
#include <windows.h>
#include ".\sccatrequest.h"



SccAtRequest::~SccAtRequest(void)
{
  delete _scc;
}

static HWND GetActiveWindowCallback(const MsSccProjectBase *project)
{
  return GetActiveWindow();
}


bool SccAtRequest::InitScc()
{
  if (_scc==0)
  if (provider.IsLoaded())
    {
      MsSccProject *proj=new MsSccProject(provider);
      proj->SetWindowCallback(GetActiveWindowCallback);
      _scc=proj;
      if (proj->ChooseProject()!=0)
      {
        delete proj;
        _scc=0;
      }    
    }
    return _scc!=0;
}