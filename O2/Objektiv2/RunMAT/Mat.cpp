// Mat.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"
#include "Mat.h"
#include "../ExternalRequests.h"
#include <el/Scc/MsSccProject.hpp>
#include "../IFileDialogEx.h"


#include "../../bredy.libs\InterProcComunicator\IpcSharedMem.h"
#include ".\mat.h"
#include <el/ProgressBar/ProgressBar.h>

CStringRes StrRes;

CMatApp theApp;
bool SccDisabled=false;
ConnReceiverSharedMem ExternalReceiver;
char ProjectPath[256]="p:\\";

//void AssertMainThread() {}


CMatApp::CMatApp():scc(SccProvider)
{
  _hMatInst=NULL;
 SccProvider.LoadScc();
}

CMatApp::~CMatApp()
{
  FreeLibrary(_hMatInst);
  ExView.Detach();
}

typedef IPluginGeneralPluginSide *(*InitProcType)(IPluginGeneralAppSide *appside);



bool CMatApp::Load(const char *dllpath)
{  
#ifdef _DEBUG
    if (dllpath==NULL) dllpath="mat_d.dll";
#else
    if (dllpath==NULL) dllpath="mat.dll";
#endif
  _hMatInst=LoadLibrary(dllpath);
  if (_hMatInst==NULL) 
  {
    return false;
  }
#ifdef _DEBUG
  InitProcType initProc=(InitProcType)GetProcAddress(_hMatInst,"ObjektivPluginMainD");
#else
  InitProcType initProc=(InitProcType)GetProcAddress(_hMatInst,"ObjektivPluginMain");
#endif
  if (initProc==NULL) 
  {
    FreeLibrary(_hMatInst);
    _hMatInst=NULL;
     return false;
  }
  _plugin=initProc(this);
  if (_plugin==NULL) 
  {
    FreeLibrary(_hMatInst);
    _hMatInst=NULL;
    return false;
  }
  _mat=(IMatPluginSide *)_plugin->GetService(Service_MaterialEditor);
  if (_mat==0) return false;
  _mat->Init(this);
  _plugin->AfterOpen(0);
  return true;
}

void CMatApp::UpdateViewer()
{
  ExView.RedrawViewer();
}
void CMatApp::ReloadViewer()
{
  if (ExView.IsReady()==ExView.ErrNotInicialized)
  {
    ExView.SetViewerSettings(&ExConfig);
    if (ExView.Attach()==ExView.ErrAttachFailed) return;
  }
}
SccFunctions *CMatApp::GetSccInterface()
{
  return &scc;
}
void CMatApp::UseMaterial(const char *filename)
{
  MessageBox(_lastMatHWND,StrRes[IDS_NOTSUPPORTED],StrRes[IDS_APP_TITLE],MB_ICONSTOP);
}
bool CMatApp::CanUseMaterial()
{
  return FALSE;
}
CWinApp *CMatApp::GetAppInteface()
{
  return NULL;
}
const char *CMatApp::GetProjectRoot()
{
  return ProjectPath;
}
ROCheckResult CMatApp::TestFileRO(const _TCHAR *filename,int flags, HWND hWnd)
{
  _lastMatHWND=hWnd;
  WinROCheck chk;
  chk.hWndOwner=hWnd;
  chk.SetScc(&scc);
  return chk.TestFileRO(filename,flags);
}
bool CMatApp::IsViewerActive()
{
  return ExView.IsReady()==ExView.ErrOK||ExView.IsReady()==ExView.ErrBusy;
}
IExternalViewer *CMatApp::GetViewerInterface()
{
  return &ExView;
}
bool CMatApp::OpenOptionsDialog()
{
  MessageBox(_lastMatHWND,StrRes[IDS_NOTSUPPORTED],StrRes[IDS_APP_TITLE],MB_ICONSTOP);
  return false;
}
ObjectData *CMatApp::CreatePrimitive(const char *filename, const char *texture, const char *material)
{
  MessageBox(_lastMatHWND,StrRes[IDS_NOTSUPPORTED],StrRes[IDS_APP_TITLE],MB_ICONSTOP);
  return NULL;
}
ObjectData *CMatApp::CreatePrimitive(ObjPrimitiveType type, int segments,float scale, const char *texture,  const char *material)
{
  MessageBox(_lastMatHWND,StrRes[IDS_NOTSUPPORTED],StrRes[IDS_APP_TITLE],MB_ICONSTOP);
  return NULL;
}
void CMatApp::DestroyPrimitive(ObjectData *obj)
{
  return ;
}
void CMatApp::UpdateViewer(ObjectData *obj)
{  
}
HMENU CMatApp::AddSubMenu(PopupMenu menuId, const char *menuName)
{
  return NULL;
}
bool CMatApp::AddMenuItem(PopupMenu menuId, const char *menuName, IPluginCommand *command)
{
  return false;
}

bool CMatApp::AddMenuItem(HMENU mnu, const char *menuName, IPluginCommand *command)
{
  return false;
}

void CMatApp::SelectInO2(const char *resourceName, ResourceType resourceType)
{
  MessageBox(_lastMatHWND,StrRes[IDS_NOTSUPPORTED],StrRes[IDS_APP_TITLE],MB_ICONSTOP);
}

void CMatApp::MatWindowClosed()
{
  PostQuitMessage(0);  
}

const LODObject *CMatApp::GetActiveObject()
{
return 0;
}
const ObjectData *CMatApp::GetActiveLevel()
{
return 0;
}



bool CMatApp::UpdateActiveObject(const LODObject &newObject)
{
  return false;
}
bool CMatApp::UpdateActiveLevel(const ObjectData &objectData)
{
  return false;
}
bool CMatApp::ModifyActiveLevel(const ObjectData *merge, Selection *delSelects)
{
  return false;
}
bool CMatApp::BeginUpdate(const char *description)
{
  return false;
}
bool CMatApp::EndUpdate()
{
  return false;
}



// Global Variables:
HINSTANCE hInst;								// current instance

// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

BOOL CALLBACK RunO2Mat(HWND hwnd,LPARAM lParam)
{
  const char *matName=(const char *)lParam;
  char buff[256];
  GetWindowText(hwnd,buff,sizeof(buff));
  if (strcmp(buff,O2EXTERNALNOTIFIER)==0)
  {
    HWND wnd=GetWindow(hwnd,GW_OWNER);
    LRESULT sendRes;
    {
      ConnectionSharedMem connection(wnd,O2M_OPENRVMATFILE );
      PSendPackage packet(&connection);
      packet->SetOption(SendPackageSharedMem::OptSendTimeout,500);
      packet->SetOption(SendPackageSharedMem::OptSendResult,(int)&sendRes);
      packet->Write(matName,strlen(matName)+1);
    }
    if (sendRes==1) exit(0);
  }
  return TRUE;
}


int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
 	// TODO: Place code here.
	MSG msg;
    StrRes.SetInstance(hInstance);
    char *matName="";

    if (__argc>1) 
    {
      matName=strcpy((char *)alloca(strlen(__argv[1])+1),__argv[1]);      
      if (matName[0]=='"') 
      {
        matName[strlen(matName)-1]=0;
        matName++;
      }
    }
    EnumWindows(RunO2Mat,(LPARAM)matName);
    HWND wnd=FindWindowEx(HWND_MESSAGE,NULL,MATEXTERNALNOTIFIER,NULL);
    if (wnd)
    {
      ConnectionSharedMem connection(wnd,O2M_OPENRVMATFILE );
      PSendPackage packet(&connection);
      packet->Write(matName,strlen(matName)+1);
      return 0;
    }
    
/*	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_MAT, szWindowClass, MAX_LOADSTRING);*/
	if (MyRegisterClass(hInstance)==0) abort();

	// Perform application initialization:*/
	if (!InitInstance (hInstance, nCmdShow)) 
	{
		return FALSE;
	}

#ifdef _DEBUG
    if (!theApp.Load("mat_d.dll") && !theApp.Load("..\\MAT_d.DLL"))
#else
    if (!theApp.Load("mat.dll") && !theApp.Load("..\\MAT.DLL"))
#endif
    {
      MessageBox(NULL,StrRes[IDS_MATLOADFAILED],StrRes[IDS_APP_TITLE],MB_OK|MB_ICONEXCLAMATION);
      return -1;
    }
    theApp.Mat().EditMaterial(matName);
    

	// Main message loop:
	while (GetMessageA(&msg, NULL, 0, 0)) 
	{
      if (!theApp.Plugin().OnPretranslateMessage(&msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

    theApp.Plugin().BeforeClose();
    theApp.Plugin().AfterClose();
    return msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//

ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX); 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_MAT);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= MATEXTERNALNOTIFIER;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(MATEXTERNALNOTIFIER , "" , WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, HWND_MESSAGE, NULL, hInstance, NULL);

   int err=GetLastError();

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

switch (message) 
   {
   case WM_CREATE: return 1;
   case O2M_OPENRVMATFILE:
     {      
       PReceivedPackage package(&ExternalReceiver,lParam);
       if (!package.IsNull())
       {
         const char *name=(const char *)package->Data();
         theApp.Mat().EditMaterial(name);
       }
     }
     return 1;
   default: return DefWindowProc(hWnd,message,wParam,lParam);
   }
   return 0;
}


ProgressBarFunctions *CMatApp::GetProgressBarHandler() {
    return ProgressBarFunctions::GetGlobalProgressBarInstance();

}

IFileDlgHistory *CMatApp::GetFileDialogExHandler() {

    class SimHist: public IFileDlgHistory
    {
    public:
      virtual void StoreHistoryFolder(const char *folderName) {}
      virtual const char *GetNextHistoryFolder(int index) {return 0;}
      virtual HBITMAP GetHistoryBitmap() {return 0;}
      virtual HBITMAP GetHistoryMenuBitmap() {return 0;}        
    };
    static SimHist simhist;
    return &simhist;

}
