// 3DC.h: interface for the C3DC class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_3DC_H__2B9427C2_5735_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_3DC_H__2B9427C2_5735_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#define C3DCPT_DEFAULT 0
#define C3DCPT_RECTANGLE 1
#define C3DCPT_CROSS 2
#define C3DCPT_XCROSS 3
#define C3DCPT_CIRCLE 4
#define C3DCPT_CHARACTER 5
#define C3DCPT_STRING 6

#define C3DCPT_FASTTEST (LPHVECTOR)1
#define C3DCPT_CALCNORMAL NULL


class C3DC : public CDC  
  {
  protected:
    CRect cliprect;
    HMATRIX transform;
    int curcull;	
    HPEN oldpen;
    HBRUSH oldbrush;
    HFONT oldfont;
    HBITMAP oldbitmap;
    HVECTOR oldpos,oldpos2,reldist;
    bool stripswapper;
    CPen *soft,*sharp;
    bool softsharp;	
    int flags;
    static void ClipLine(HVECTOR tv1, HVECTOR tv2);
    static char GetClipFlags(HVECTOR tv1, RECT& rc);
    static bool Clip(HVECTOR tv1, HVECTOR tv2, RECT& rc);
  public:	
    float lasttestdistance;
    LPHVECTOR GetLastDistance() 
      {return reldist;}
    static int GetCull(HVECTOR v1, HVECTOR v2, HVECTOR v3);
    void SoftSharp(int flgs, CPen *sft, CPen *shrp)
      {softsharp=true;flags=flgs,soft=sft,sharp=shrp;}
    void SoftOff() 
      {soft=false;}
    COLORREF GetColorAt(float x, float y, float z, COLORREF def);
    bool MoveToRect(CRect &rect, float x, float y, float z);
    void FilledQuad(int pt, HMATRIX mm, COLORREF ref);
    void FilledQuadGuard(int pt, HMATRIX mm, COLORREF *ref, bool wires=false);
    void ClearCanvas(COLORREF col);
    void ClipRectFromContext() 
      {GetClipBox(&cliprect);}
    void ClipRectFromBitmap(CBitmap& bmp);
    void SetMatrix(HMATRIX mm) 
      {CopyMatice(transform,mm);}
    void SetClipRect(RECT &rc) 
      {cliprect=rc;}
    void SetCull(int cull) 
      {curcull=cull;}
    LPHMATRIX GetMatrix() 
      {return transform;}
    bool PtTestFan(HVECTOR v, HVECTOR pt, int cull) 
      {stripswapper=true;return PtTestStrip(v,pt,cull);}
    bool PtTestStrip(HVECTOR v, HVECTOR pt, int cull);
    void Point3D(LPHVECTOR v=NULL,int type=C3DCPT_DEFAULT, unsigned long size=0);
    void Quad3D(HVECTOR v3, HVECTOR v4);
    void WiredFan(float x, float y, float z) 
      {WiredFan(mxVector3(x,y,z));}
    void WiredFan(HVECTOR v);
    void WiredStrip(float x, float y, float z) 
      {WiredStrip(mxVector3(x,y,z));}
    void WiredStrip(HVECTOR v);
    void BeginStripFan(float x, float y, float z) 
      {BeginStripFan(mxVector3(x,y,z));}
    void BeginStripFan(HVECTOR v);
    void LineTo3D(float x, float y, float z);
    void LineTo3D(HVECTOR v);
    void MoveTo3D(HVECTOR v);
    void MoveTo3D(float x, float y, float z);
    void SelectObject(CBitmap *bitmap);
    virtual CFont* SelectObject(CFont *font);
    void SelectObject(CBrush *brush);
    void SelectObject(CPen *pen);
    void Line3D(HVECTOR v1, HVECTOR v2);
    LPHVECTOR GetOldPos() 
      {return oldpos;}
    C3DC();
    virtual ~C3DC();
    int FullTestTriangle(HVECTOR v1, HVECTOR v2, HVECTOR v3, HVECTOR pt, LPHVECTOR plane);
  protected:
    void MoveToT(HVECTOR pos);
    int TestLinePt(HVECTOR pt1, HVECTOR pt2, HVECTOR pt);
    bool TestTriangle(HVECTOR pt1, HVECTOR pt2, HVECTOR pt3, HVECTOR pt);
    bool TestQaud(HVECTOR v1, HVECTOR v2, HVECTOR v3, HVECTOR v4, HVECTOR pt);
    void Triangle(HVECTOR v1, HVECTOR v2, HVECTOR v3);
    void Line3DClipped(HVECTOR tv1, HVECTOR tv2);
  };

//--------------------------------------------------

/*
   Strip                  Fan

  1       2                1         2
  *-------*               ,*---------*
   \     / \             / |\       /
    \   /   \          5*  | \     /
	 \ /     \           \ |  \   /
    3 *-------*4          \|   \ /
	   \     /            4*----*3
	    \   /
		 \ /
		  *5
	  
1 - MoveTo3D(...);       1 - MoveTo3D(...);
2 - BeginStripFan(...);  2 - BeginStripFan(...);
3 - WiredStrip(...);     3 - WiredFan(...);
4 - WiredStrip(...);	 4 - WiredFan(...);
5 - WiredStrip(...);	 5 - WiredFan(...);

  Quad

  1*----------*2
    \        /
	 \      /
	  \    /
	  4*--*3

1 - MoveTo3D(...);       
2 - BeginStripFan(...);  
3,4 - Quad3D(...);     

*/

struct SFaceSortInfo
  {
  float minz,maxz;
  HVECTOR plane;
  HMATRIX points;
  int n;
  COLORREF color;
  COLORREF normrf[4];
  int dontchangeflag;
  int lasttested;
  };

//--------------------------------------------------

class CFaceSortList: public AutoArray<SFaceSortInfo>
  {
  SFaceSortInfo **sortedptrs;  
  C3DC *adc;
  HVECTOR lightdir;  
  HVECTOR halfdir;
  public:
    void DrawFaces(bool wires=false);
    void ApplyFaceSort();
    CFaceSortList(): AutoArray<SFaceSortInfo>() 
      {sortedptrs=NULL;}
    virtual ~CFaceSortList() 
      {free(sortedptrs);}
    bool Prepare();
    void AttachDC(C3DC *pdc) 
      {adc=pdc;}
    void SetLightDirection(HVECTOR ligh,HVECTOR cam) 
     {CopyVektor(lightdir,ligh);
      CopyVektor(halfdir,cam);
       SoucetVektoru(halfdir,lightdir);
       NormalizeVector(halfdir);
       }      
    void AddFace(int npts, HMATRIX points, COLORREF refcol,LPHMATRIX normals=NULL);
  };

TypeIsMovable(SFaceSortInfo);

//--------------------------------------------------

LPHVECTOR calcFacePlane(HVECTOR v1, HVECTOR v2, HVECTOR v3);

#endif // !defined(AFX_3DC_H__2B9427C2_5735_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
