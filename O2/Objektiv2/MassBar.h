#if !defined(AFX_MASSBAR_H__3EC459E4_1A6E_44C1_A59B_8C5953D327D7__INCLUDED_)
#define AFX_MASSBAR_H__3EC459E4_1A6E_44C1_A59B_8C5953D327D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MassBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMassBar dialog

class CMassBar : public CDialogBar
  {
  // Construction
  CWnd *main;
  ObjectData *obj;
  public:
    void Update(ObjectData *obj);
    CMassBar();   // standard constructor
    void Create(CWnd *parent, int menuid);
    virtual void OnUpdateCmdUI( CFrameWnd* pTarget, BOOL bDisableIfNoHndler ) 
      {
      }
    
    // Dialog Data
    //{{AFX_DATA(CMassBar)
    enum { IDD = IDD_MASSBAR };
    CEdit	Mass;
    CButton	check;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CMassBar)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CMassBar)
    afx_msg void OnApply();
    afx_msg void OnDestroy();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  public:
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MASSBAR_H__3EC459E4_1A6E_44C1_A59B_8C5953D327D7__INCLUDED_)
