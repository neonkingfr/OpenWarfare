// parameter file parser

#include "StdAfx.h"
#include "QStream.hpp"
#include "paramFile.hpp"
/*
#ifndef ACCESS_ONLY
#include "paramArchive.hpp"
#endif
#include "loadStream.hpp"
#include "serializeBin.hpp"*/
#include <ctype.h>

//#include "strIncl.hpp"

#include "optima/express.hpp"

typedef char WordBuf[2048];
RStringB RStringBNull(""); // empty string - TODO: make global


static bool GetWord( char *buf, int bufSize, QIStream &in, const char *termin )
{
  //char buf[2048];
  int len=0;
  buf[len]=0;
  int c=in.get();
  // LTrim the word
  while( isspace(c) ) c=in.get();
  if (c=='"')
  {
    c=in.get();
    for(;;)
    {
      if (c=='"')
      {
        c=in.get();
        if (c!='"')
        {
          in.unget();
          return true; // word parsed
        }
      }
      if( c=='\n' || c=='\r' )
      {
        ErrorMessage("Config: End of line encountered after %s",buf);
      }
      if( len<bufSize-1 ) buf[len++]=c,buf[len]=0;
      c=in.get();
    }
  }
  else
  {
    while (!strchr(termin,c) && c != EOF)
    {
      if( c=='\n' || c=='\r' )
      {
        // word terminated - only white spaces or termin now
        for(;;)
        {
          c=in.get();
          if( !isspace(c) ) break;
        }
        if( !strchr(termin,c) )
        {
          ErrorMessage("Config: '%c' after %s",c,buf);
        }
        else in.unget();
        goto Return;
      }
      if( len<bufSize-1 ) buf[len++]=c,buf[len]=0;
      c=in.get();
    }
    if (c != EOF) in.unget();
    Return:
    // RTrim the word
    while (len>0 && isspace(buf[len-1])) buf[--len]=0;
    /*
		if (buf[0] == '$')
		{
			RString string=LoadString(buf + 1);
			strcpy(buf,string);
			return true;
		}
*/
    return len>0;
  }
  /*NOTREACHED*/
}

static void GetAlphaWord( char *buf, int bufSize, QIStream &in )
{
  int len=0;
  int c=in.get();
  while( isspace(c) ) c=in.get();
  while( isalnum(c) || c=='_' )
  {
    if( len<bufSize ) buf[len++]=c;
    c=in.get();
  }
  in.unget();
  buf[len]=0;
}

ParamEntry::ParamEntry( const RStringB &name )
  //:_overload(false)
{
  if( name.GetLength()>0 ) _name=name;
}

inline void NotClass( const char *cName, const char *eName )
{
  ErrorMessage("'%s' is not a class ('%s' accessed)",cName,eName);
}

inline void NotClass( const char *cName )
{
  ErrorMessage("'%s' is not a class.",cName);
}

inline void NotValue( const char *eName )
{
  ErrorMessage("'%s' is not a value",eName);
}

inline void NotArray( const char *aName )
{
  ErrorMessage("'%s' is not an array.",aName);
}

enum SpecValueType
{
  SVGeneric, // generic - string
  SVFloat,
  SVInt,
  SVArray,
  NSpecValueType
    // note: char is used to contain values of this type
};

ParamEntry *ParamEntry::FindEntry( const char *name ) const
{
  NotClass(GetContext(),name);
  return NULL;
}

const ParamEntry &ParamEntry::operator >> ( const char *name ) const
{
  NotClass(GetContext(),name);
  return *this;
}

/*
const ParamEntry &ParamEntry::operator >> ( RString name ) const
{
	NotClass(GetContext(),name);
	return *this;
}
*/

ParamEntry::operator RStringB() const 
{NotValue(GetContext());return RStringBNull;}

ParamEntry::operator float() const 
{NotValue(GetContext());return 0;}

ParamEntry::operator int() const 
{NotValue(GetContext());return 0;}

ParamEntry::operator bool() const 
{NotValue(GetContext());return false;}

int ParamEntry::GetInt() const 
{NotValue(GetContext());return 0;}

void ParamEntry::Add( const RStringB &name, const RStringB &val ) 
{NotClass(GetContext(),name);}

void ParamEntry::Add( const RStringB &name, float val ) 
{NotClass(GetContext(),name);}

ParamEntry *ParamEntry::AddClass( const RStringB &name ) 
{NotClass(GetContext(),name);return NULL;}

ParamEntry *ParamEntry::AddArray( const RStringB &name ) 
{NotClass(GetContext(),name);return NULL;}

void ParamEntry::Clear() 
{NotArray(GetContext());}

void ParamEntry::AddValue(float val)
{NotArray(GetContext());}

void ParamEntry::AddValue(int val)
{NotArray(GetContext());}

void ParamEntry::AddValue(bool val)
{NotArray(GetContext());}

void ParamEntry::AddValue(const RStringB &val)
{NotArray(GetContext());}

void ParamEntry::AddValue(const char *val)
{NotArray(GetContext());}

int ParamEntry::GetEntryCount() const 
{NotClass(GetContext());return 0;}

const ParamEntry &ParamEntry::GetEntry( int i ) const 
{NotClass(GetContext());return *this;}

void ParamEntry::SetValue( const RStringB &val ) 
{NotValue(GetContext());}

void ParamEntry::SetValue( float val ) 
{NotValue(GetContext());}

void ParamEntry::SetValue(int val) 
{NotValue(GetContext());}

/*PackedColor ParamEntry::GetPackedColor() const
{
	ErrorMessage("'%s' not a color array",(const char *)GetContext());
	return 0;
}*/

/*Color ParamEntry::GetColor() const
{
	ErrorMessage("'%s' not a color array",(const char *)GetContext());
	return 0;
}

ParamEntry::operator SoundPars() const
{
	const static SoundPars nil;
	ErrorMessage("'%s' not a sound array",(const char *)GetContext());
	return nil;
}
*/
class ParamRawValue
{
  RStringB _value;
  ParamFile *_file;
  //float _fValue;
  //int _iValue;
  
  public:
    ParamRawValue() 
    {_file = NULL;}
    
    SpecValueType GetValueType() 
    {return SVGeneric;}
    
    void SetValue(const RStringB &value);
    void SetValue(float val);
    void SetValue(int val);
    void SetFile(ParamFile *file) 
    {_file = file;}
    
    const RStringB GetValue() const;
    float GetFloat() const;
    int GetInt() const;
    
    operator RStringB() const 
    {return _value;}
    operator float() const 
    {return GetFloat();}
    operator int() const 
    {return GetInt();}
    //operator const char *() const {return GetValue();}
    
    void Save(QOStream &f, int indent) const;
    void SerializeBin(SerializeBinStream &f);
};

class ParamRawValueFloat
{	// special case - number detected as float
  float _value;
  public:
    ParamRawValueFloat() 
    {}
    
    SpecValueType GetValueType() 
    {return SVFloat;}
    
    void SetValue( const RStringB &value )
    {Fail("Float value set as string");}
    void SetValue( float val )
    {_value=val;}
    void SetValue( int val )
    {_value=val;}
    void SetFile(ParamFile *file) 
    {}
    
    RStringB GetValue() const;
    float GetFloat() const 
    {return _value;}
    int GetInt() const 
    {return toLargeInt(_value);}
    
    operator RStringB() const 
    {return GetValue();}
    operator float() const 
    {return GetFloat();}
    operator int() const 
    {return GetInt();}
    //operator const char *() const {return GetValue();}
    
    void Save(QOStream &f, int indent) const;
    void SerializeBin(SerializeBinStream &f);
};

class ParamRawValueInt
{	// special case - number detected as int
  int _value;
  public:
    ParamRawValueInt() 
    {}
    
    SpecValueType GetValueType() 
    {return SVInt;}
    
    void SetValue( const RStringB &value )
    {Fail("Float value set as string");}
    void SetValue( float val )
    {_value=toLargeInt(val);}
    void SetValue( int val )
    {_value=val;}
    void SetFile(ParamFile *file) 
    {}
    
    RStringB GetValue() const;
    float GetFloat() const 
    {return _value;}
    int GetInt() const 
    {return _value;}
    
    operator RStringB() const 
    {return GetValue();}
    operator float() const 
    {return GetFloat();}
    operator int() const 
    {return GetInt();}
    //operator const char *() const {return GetValue();}
    
    void Save(QOStream &f, int indent) const;
    void SerializeBin(SerializeBinStream &f);
};

void ParamEntry::SetValue( int index, const RStringB &string )
{
  ErrorMessage("SetValue: '%s' not an array",(const char *)GetContext());
}

void ParamEntry::SetValue( int index, float val )
{
  ErrorMessage("SetValue: '%s' not an array",(const char *)GetContext());
}

void ParamEntry::SetValue( int index, int val )
{
  ErrorMessage("SetValue: '%s' not an array",(const char *)GetContext());
}

int ParamEntry::GetSize() const 
{
  ErrorMessage("Size: '%s' not an array",(const char *)GetContext());
  return 0;
}

//DEFINE_FAST_ALLOCATOR(ParamClass)

ParamClass::ParamClass()
  :ParamEntry(NULL)
  {
  }

ParamClass::ParamClass( const RStringB &name)
  :ParamEntry(name)
  {
  }

ParamClass::~ParamClass()
{
}

#include <Es/Memory/normalNew.hpp>


template <class ParamRawValueSpec>
class ParamValueSpec: public ParamEntry,public ParamRawValueSpec
{
  public:
    ParamValueSpec();
    ParamValueSpec(const RStringB &name);
    
    RStringB GetValue() const 
    {return ParamRawValueSpec::GetValue();}
    float GetFloat() const 
    {return ParamRawValueSpec::GetFloat();}
    int GetInt() const 
    {return ParamRawValueSpec::GetInt();}
    
    operator RStringB() const 
    {return ParamRawValueSpec::GetValue();}
    operator float() const 
    {return ParamRawValueSpec::GetFloat();}
    operator int() const
    {return ParamRawValueSpec::GetInt();}
    operator bool() const
    {return ParamRawValueSpec::GetInt()!=0;}
    
    void SetValue(const RStringB &val)
    {ParamRawValueSpec::SetValue(val);}
    void SetValue(float val)
    {ParamRawValueSpec::SetValue(val);}
    void SetValue(int val)
    {ParamRawValueSpec::SetValue(val);}
    void SetFile(ParamFile *file) 
    {ParamRawValueSpec::SetFile(file);}
    
    void Save( QOStream &f, int indent ) const;
    void SerializeBin(SerializeBinStream &f);
    
    //	USE_FAST_ALLOCATOR
};

template <class ParamRawValueSpec>
class ParamArrayValueSpec: public IParamArrayValue,public ParamRawValueSpec
{
  public:
    ParamArrayValueSpec(const RStringB &val)
    {ParamRawValueSpec::SetValue(val);}
    ParamArrayValueSpec(float val)
    {ParamRawValueSpec::SetValue(val);}
    ParamArrayValueSpec(int val)
    {ParamRawValueSpec::SetValue(val);}
    
    RStringB GetValue() const 
    {return ParamRawValueSpec::GetValue();}
    int GetInt() const 
    {return ParamRawValueSpec::GetInt();}
    float GetFloat() const 
    {return ParamRawValueSpec::GetFloat();}
    
    void SetValue(const RStringB &val)
    {ParamRawValueSpec::SetValue(val);}
    void SetValue(float val)
    {ParamRawValueSpec::SetValue(val);}
    void SetValue(int val)
    {ParamRawValueSpec::SetValue(val);}
    void SetFile(ParamFile *file) 
    {ParamRawValueSpec::SetFile(file);}
    
    void Save(QOStream &f, int indent) const;
    //	void SerializeBin(SerializeBinStream &f);
    
    // may be array of values
    const IParamArrayValue *GetItem(int i) const 
    {return NULL;}
    int GetItemCount() const
    {
      ErrorMessage("Value not an array.");
      return 0;
    }
    //	PackedColor GetPackedColor() const {return PackedBlack;}
    //	SoundPars GetSoundPars() const {return SoundPars();}
    
    //	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

typedef ParamValueSpec<ParamRawValue> ParamValue;
typedef ParamValueSpec<ParamRawValueFloat> ParamValueFloat;
typedef ParamValueSpec<ParamRawValueInt> ParamValueInt;

typedef ParamArrayValueSpec<ParamRawValue> ParamArrayValue;
typedef ParamArrayValueSpec<ParamRawValueFloat> ParamArrayValueFloat;
typedef ParamArrayValueSpec<ParamRawValueInt> ParamArrayValueInt;

//DEFINE_FAST_ALLOCATOR(ParamValue)
//DEFINE_FAST_ALLOCATOR(ParamValueFloat)
//DEFINE_FAST_ALLOCATOR(ParamValueInt)

//DEFINE_FAST_ALLOCATOR(ParamArrayValue)
//DEFINE_FAST_ALLOCATOR(ParamArrayValueFloat)
//DEFINE_FAST_ALLOCATOR(ParamArrayValueInt)

static IParamArrayValue *CreateParamArrayValue(RString val)
{
  return new ParamArrayValue(val);
}

static IParamArrayValue *CreateParamArrayValue(float val)
{
  return new ParamArrayValueFloat(val);
}

static IParamArrayValue *CreateParamArrayValue(int val)
{
  return new ParamArrayValueInt(val);
}

/*
static ParamEntry *CreateParamValue(SerializeBinStream &f)
{
	// load type and create value
	Assert( f.IsLoading() );
	char type;
	f.Transfer(type);
	switch (type)
	{
		case SVGeneric:
			return new ParamValue();
		case SVFloat:
			return new ParamValueFloat();
		case SVInt:
			return new ParamValueInt();
		default:
			ErrF("Unknown value type %d",type);
			return new ParamValue();
	}

}
*/
template <class ParamRawValueSpec>
ParamValueSpec<ParamRawValueSpec>::ParamValueSpec()
  :ParamEntry(NULL)
  {
  }

template <class ParamRawValueSpec>
ParamValueSpec<ParamRawValueSpec>::ParamValueSpec(const RStringB &name)
  :ParamEntry(name)
  {
  }

template <class ParamRawValueSpec>
void ParamArrayValueSpec<ParamRawValueSpec>::Save(QOStream &f, int indent) const
{
  // check type
  ParamRawValueSpec::Save(f,indent);
}

/*
template <class ParamRawValueSpec>
void ParamArrayValueSpec<ParamRawValueSpec>::SerializeBin(SerializeBinStream &f)
{
	if (f.IsSaving())
	{
		char type = GetValueType();
		f.Transfer(type);
	}
	ParamRawValueSpec::SerializeBin(f);
}*/

// scan some special value types

static int ScanHex(const char *val, bool &ok)
{
  ok = false;
  if (!strnicmp(val, "0x", 2))
  {
    char c;
    const char *ptr = (const char *)val + 2;
    ok = isxdigit(*ptr) != 0;
    if (!ok) return 0;
    int iValue = 0;
    while (c=*(ptr++), isxdigit(c))
    {
      iValue *= 16;
      if (isdigit(c))		// 0..9
      iValue += c - '0';
      else if (c<='F')	// A..F
      iValue += 10 + c - 'A';
      else							// a..f
      iValue += 10 + c - 'a';
    }
    return iValue;
  }
  else
  {
    return 0;
  }
}

static float ScanDb(const char *ptr, bool &ok)
{
  ok=false;
  if (ptr[0]!='d' || ptr[1]!='b') return 0;
  ok=true;
  char *end;
  float db = strtod(ptr+2,&end);
  if (*end!=0)
  {
    LogF("invalid db value %s",ptr);
  }
  return pow(10,db*(1.0f/20));
}

static float ScanFloatPlain(const char *ptr, bool &ok)
{
  char *end;
  float db = strtod(ptr,&end);
  ok = (*end==0);
  return db;
}

static int ScanIntPlain(const char *ptr, bool &ok)
{
  char *end;
  float db = strtol(ptr,&end,10);
  ok = (*end==0);
  return db;
}

static int ScanInt(const char *ptr, bool &ok)
{
  ok = false;
  if (!*ptr) return 0;
  int val = ScanIntPlain(ptr,ok);
  if (ok) return val;
  val = ScanHex(ptr,ok);
  if (ok) return val;
  return 0;
}

static float ScanFloat(const char *ptr, bool &ok)
{
  ok = false;
  if (!*ptr) return 0;
  float val = ScanFloatPlain(ptr,ok);
  if (ok) return val;
  val = ScanDb(ptr,ok);
  if (ok) return val;
  return 0;
}

const RStringB ParamRawValue::GetValue() const
{
  /*	const char *val = _value;
	if (strncmp(val, "$STR", 4) == 0) return LoadString(val + 1);
	else */return _value;
}

float ParamRawValue::GetFloat() const
{
  bool ok;
  // check for simple cases
  float valF = ScanFloat(_value,ok);
  if (ok) return valF;
  int valI = ScanInt(_value,ok);
  if (ok) return valI;
  /*	Assert(_file);
	GGameState.BeginContext(_file->GetVariables());
	GameValue result = GGameState.Evaluate(_value);
	GGameState.EndContext();*/
  //	return result;
  return 0;
}

int ParamRawValue::GetInt() const
{
  bool ok;
  
  // check for simple cases
  int valI = ScanInt(_value,ok);
  if (ok) return valI;
  float valF = ScanFloat(_value,ok);
  if (ok)
  {
    LogF("Warning: rounding float value %g",valF);
    return toLargeInt(valF);
  }
  /*	Assert(_file);
	GGameState.BeginContext(_file->GetVariables());
	GameValue result = GGameState.Evaluate(_value);
	GGameState.EndContext();
	return toLargeInt(result);*/
  return 0;
}

void ParamRawValue::SetValue( const RStringB &value )
{
  _value=value;
}

void ParamRawValue::SetValue( float val )
{
  char buf[256];
  sprintf(buf,"%f",val);
  _value=RString(buf);
  
  // check if ok
  char *end;
  float dummy = strtod(buf, &end);
  (void)dummy;
  if (*end != 0)
  {
    ErrF("Setting invalid value %s", buf);
    _value = RString("0");
  }
}

void ParamRawValue::SetValue(int val)
{
  char buf[256];
  sprintf(buf,"%d",val);
  _value=RString(buf);
}

#include <Es/Memory/normalNew.hpp>

class ParamRawArray
{
  protected:
    RefArray<IParamArrayValue> _value;
    
  public:
    void AddValue(float val);
    void AddValue(int val);
    void AddValue(bool val);
    void AddValue(const RStringB &val);
    void AddValue(const char *val);
    
    void Compact()
    {_value.Compact();}
    void Clear() 
    {_value.Clear();}
    void Copy(const ParamRawArray &src) 
    {_value = src._value;}
    
    void SetFile(ParamFile *file);
    
    int GetSize() const 
    {return _value.Size();}
    IParamArrayValue &GetValue( int i ) const;
    const IParamArrayValue &operator [] ( int i ) const 
    {return GetValue(i);}
    
    void SetValue( int index, const RStringB &string );
    void SetValue( int index, float val );
    //	PackedColor GetPackedColor() const;
    //	Color GetColor() const;
    //	operator SoundPars() const;
    
    void Parse(QIStream &in, ParamFile *file);
    void Save(QOStream &f, int indent) const;
    void SerializeBin(SerializeBinStream &f);
};

class ParamArrayValueArray: public IParamArrayValue,public ParamRawArray
{
  public:
    ParamArrayValueArray()
    {}
    
    RStringB GetValue() const 
    {return RStringBEmpty;}
    int GetInt() const 
    {return 0;}
    float GetFloat() const 
    {return 0;}
    
    void SetValue(const RStringB &val)
    {}
    void SetValue(float val)
    {}
    void SetValue(int val)
    {}
    void SetFile(ParamFile *file) 
    {ParamRawArray::SetFile(file);}
    
    //	PackedColor GetPackedColor() const;
    //	SoundPars GetSoundPars() const;
    
    void Save(QOStream &f,int indent) const;
    void SerializeBin(SerializeBinStream &f);
    
    // may be array of values
    const IParamArrayValue *GetItem(int i) const 
    {return &ParamRawArray::operator [](i);}
    int GetItemCount() const 
    {return ParamRawArray::GetSize();}
    
    //	USE_FAST_ALLOCATOR
};

class ParamArray: public ParamEntry, public ParamRawArray
{
  //ParamFile *_file;
  
  public:
    ParamArray( const RStringB &name );
    
    bool IsArray() const 
    {return true;}
    
    //void AddValue(const IParamArrayValue &val);
    
    void SetFile(ParamFile *file)
    {ParamRawArray::SetFile(file);}
    
    
    void Compact()
    {ParamRawArray::Compact();}
    void Clear() 
    {ParamRawArray::Clear();}
    /*
	void Compact(){_value.Compact();}
	void Clear() {_value.Clear();}
	void Copy(const ParamArray &src) {_value = src._value;}

	*/
    
    void AddValue(float val)
    {ParamRawArray::AddValue(val);}
    void AddValue(int val)
    {ParamRawArray::AddValue(val);}
    void AddValue(bool val)
    {ParamRawArray::AddValue(val);}
    void AddValue(const RStringB &val)
    {ParamRawArray::AddValue(val);}
    void AddValue(const char *val)
    {ParamRawArray::AddValue(val);}
    
    int GetSize() const 
    {return ParamRawArray::GetSize();}
    IParamArrayValue &GetValue( int i ) const 
    {return ParamRawArray::GetValue(i);}
    void SetValue(int i, const RStringB &string )
    {ParamRawArray::SetValue(i,string);}
    void SetValue(int i, float val )
    {ParamRawArray::SetValue(i,val);}
    //	PackedColor GetPackedColor() const {return ParamRawArray::GetPackedColor();}
    //	Color GetColor() const {return ParamRawArray::GetColor();}
    //	operator SoundPars() const {return ParamRawArray::operator SoundPars();}
    
    const IParamArrayValue &operator [] ( int i ) const 
    {return GetValue(i);}
    
    void Parse(QIStream &in, ParamFile *file);
    void Save(QOStream &f, int indent) const;
    //	void SerializeBin(SerializeBinStream &f);
    
    //	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

/*
static IParamArrayValue *CreateParamArrayValue(SerializeBinStream &f)
{
	// load type and create value
	Assert( f.IsLoading() );
	char type;
	f.Transfer(type);
	switch (type)
	{
		case SVGeneric:
			return new ParamArrayValue("");
		case SVFloat:
			return new ParamArrayValueFloat(0.0f);
		case SVInt:
			return new ParamArrayValueInt(0);
		case SVArray:
			return new ParamArrayValueArray();
		default:
			ErrF("Unknown value type %d",type);
			return new ParamArrayValue("");
	}

}

*/
//DEFINE_FAST_ALLOCATOR(ParamArrayValueArray)

/*PackedColor ParamArrayValueArray::GetPackedColor() const
{
	return ParamRawArray::GetPackedColor();
}

SoundPars ParamArrayValueArray::GetSoundPars() const
{
	return ParamRawArray::operator SoundPars();
}
*/
void ParamArrayValueArray::Save(QOStream &f, int indent) const
{
  ParamRawArray::Save(f,indent);
}

/*
void ParamArrayValueArray::SerializeBin(SerializeBinStream &f)
{
	if (f.IsSaving())
	{
		char type = SVArray;
		f.Transfer(type);
	}
	ParamRawArray::SerializeBin(f);
}
*/
//DEFINE_FAST_ALLOCATOR(ParamArray)


ParamArray::ParamArray( const RStringB &name)
  :ParamEntry(name) //,_file(NULL)
{
}

/*
PackedColor ParamRawArray::GetPackedColor() const
{
	if( _value.Size()!=4 )
	{
		//ErrorMessage("Config: '%s' not an ARGB color",(const char *)GetName());
		return 0;
	}
	return Color(_value[0]->GetFloat(),_value[1]->GetFloat(),_value[2]->GetFloat(),_value[3]->GetFloat());
}

Color ParamRawArray::GetColor() const
{
	if( _value.Size()!=4 )
	{
		//ErrorMessage("Config: '%s' not an ARGB color",(const char *)GetName());
		return 0;
	}
	return Color(_value[0]->GetFloat(),_value[1]->GetFloat(),_value[2]->GetFloat(),_value[3]->GetFloat());
}

ParamRawArray::operator SoundPars() const
{
	if( _value.Size()<3 )
	{
		const static SoundPars nil;
		//ErrorMessage("Config: '%s' not sound parameters.",(const char *)GetName());
		return nil;
	}
	SoundPars ret;
	ret.name=GetSoundName(*_value[0]);
	ret.vol = _value[1]->GetFloat();
	ret.freq = _value[2]->GetFloat();
	ret.freqRnd=0;
	ret.volRnd=0.05;
	return ret;
}
*/
void ParamRawArray::AddValue(float val)
{_value.Add(new ParamArrayValueFloat(val));}

void ParamRawArray::AddValue(int val)
{_value.Add(new ParamArrayValueInt(val));}

void ParamRawArray::AddValue(bool val)
{_value.Add(new ParamArrayValueInt(val));}

void ParamRawArray::AddValue(const RStringB &val)
{_value.Add(new ParamArrayValue(val));}

void ParamRawArray::AddValue(const char *val)
{_value.Add(new ParamArrayValue(val));}

const IParamArrayValue &ParamEntry::operator [] ( int index ) const 
{
  const static ParamArrayValue nil("");
  ErrorMessage("[]: '%s' not an array",(const char *)GetContext());
  return nil;
}

/*
#ifndef ACCESS_ONLY
LSError SoundPars::Serialize(ParamArchive &ar)
{
	CHECK(ar.Serialize("name", name, 1))
	CHECK(ar.Serialize("vol", vol, 1))
	CHECK(ar.Serialize("freq", freq, 1))
	CHECK(ar.Serialize("volRnd", volRnd, 1))
	CHECK(ar.Serialize("freqRnd", freqRnd, 1))
	return LSOK;
}
#endif
*/
void ParamRawArray::SetFile(ParamFile *file)
{
  // note: only ArrayValues of ParamArrays
  // loaded via Parse or SerializeBin have _file member set
  // SetFile is always called recursivelly after corresponding fucntions
  //_file = file;
  for (int i=0; i<_value.Size(); i++)
  {
    IParamArrayValue *value = _value[i];
    if (value) value->SetFile(file);
  }
}

IParamArrayValue &ParamRawArray::GetValue( int i ) const
{
  if( i>=_value.Size() )
  {
    //ErrorMessage("Config: '%s' does not have %d entries.",(const char *)GetName(),i+1);
    return *_value[0];
  }
  return *_value[i];
}

void ParamRawArray::SetValue( int index, const RStringB &val )
{
  while( index>_value.Size() )
  {
    _value.Add(new ParamArrayValue(""));
  }
  if (index>=_value.Size())
  {
    _value.Access(index);
    _value[index]= CreateParamArrayValue(val);
  }
  else
  {
    _value[index]->SetValue(val);
  }
}

void ParamRawArray::SetValue( int index, float val )
{
  while( index>_value.Size() )
  {
    _value.Add(new ParamArrayValue(""));
  }
  if (index>=_value.Size())
  {
    _value.Access(index);
    _value[index]= CreateParamArrayValue(val);
  }
  else
  {
    _value[index]->SetValue(val);
  }
}

int ParamClass::FindIndex( const char *name ) const
{
  for( int i=0; i<_entries.Size(); i++ )
  {
    if (!_entries[i]) continue;
    if( !strcmpi(_entries[i]->GetName(),name) ) return i;
  }
  return -1;
}

ParamEntry *ParamClass::Find( const char *name, bool parent, bool base ) const
{
  int i=FindIndex(name);
  if( i>=0 ) return _entries[i];
  if( base && _base!=NULL )
  {
    return _base->Find(name,parent,base);
  }
  if( parent && _parent!=NULL )
  {
    return _parent->Find(name,parent,base);
  }
  return NULL;
}

ParamEntry *ParamClass::FindEntry( const char *name ) const
{
  return Find(name,false,true);
}

const ParamEntry &ParamClass::operator >> ( const char *name ) const
{
  const ParamEntry *entry=FindEntry(name);
  if( entry ) return *entry;
  ErrorMessage("No entry '%s'.",(const char *)GetContext(name));
  return *this;
}

/*
const ParamEntry &ParamClass::operator >> ( const RStringB &name ) const
{
	const ParamEntry *entry=FindEntry(name);
	if( entry ) return *entry;
	ErrorMessage("No entry '%s'.",(const char *)GetContext(name));
	return *this;
}
*/

const ParamClass *ParamClass::GetClass( const char *name ) const
{
  ParamEntry *entry=Find(name,false,true);
  ParamClass *section=dynamic_cast<ParamClass *>(entry);
  if( !section )
  {
    ErrorMessage("No section '%s' in '%s'",name,(const char *)GetName());
    return this;
  }
  return section;
}

bool ParamClass::IsDerivedFrom( const ParamClass &parent ) const
{
  const ParamClass *base=this;
  while( base )
  {
    if( base==&parent ) return true;
    base=base->_base;
  }
  return false;
}

void ParamClass::Add( const RStringB &name, float val )
{
  Ref<ParamEntry> entry=FindEntry(name);
  if( !entry )
  {
    entry=new ParamValueFloat(name);
    NewEntry(entry);
  }
  entry->SetValue(val);
}

void ParamClass::Add( const RStringB &name, int val )
{
  Ref<ParamEntry> entry=FindEntry(name);
  if( !entry )
  {
    entry=new ParamValueInt(name);
    NewEntry(entry);
  }
  entry->SetValue(val);
}

void ParamClass::Add( const RStringB &name, const RStringB &val )
{
  Ref<ParamEntry> entry=FindEntry(name);
  if( !entry )
  {
    entry=new ParamValue(name);
    NewEntry(entry);
  }
  entry->SetValue(val);
}

ParamEntry *ParamClass::AddClass( const RStringB &name)
{
  Ref<ParamEntry> entry=FindEntry(name);
  if( !entry )
  {
    entry=new ParamClass(name);
    NewEntry(entry);
  }
  return entry;
}

ParamEntry *ParamClass::AddArray( const RStringB &name)
{
  Ref<ParamEntry> entry=FindEntry(name);
  if( !entry )
  {
    entry=new ParamArray(name);
    //entry->SetFile(_file);
    NewEntry(entry);
  }
  else
  {
    entry->Clear();
  }
  return entry;
}

void ParamClass::SetFile(ParamFile *file)
{
  for (int i=0; i<_entries.Size(); i++)
  {
    ParamEntry *entry = _entries[i];
    if (entry) entry->SetFile(file);
  }
}

void ParamClass::NewEntry( Ref<ParamEntry> entry )
{
  int index=FindIndex(entry->GetName());
  if( index>=0 )
  {
    ParamEntry *entry=_entries[index];
    ErrorMessage
      (
        "Config: '%s' already defined in '%s'.",
    (const char *)entry->GetName(),(const char *)GetName()
      );
    return;
  }
  _entries.Add(entry);
}

RString ParamEntry::GetContext( const char *member ) const
{
  char buf1[512];
  bool first=false;
  if( member ) strcpy(buf1,member),first=true;
  else *buf1=0;
  const ParamEntry *src=this;
  while( src )
  {
    char buf2[512];
    strncpy(buf2,buf1,sizeof(buf1));
    if( src->GetName() ) strncpy(buf1,src->GetName(),sizeof(buf1));
    else 
    {strcpy(buf1,"");Fail("Bad context");}
    if( first ) strncat(buf1,".",sizeof(buf1)),first=false;
    else strncat(buf1,"::",sizeof(buf1));
    strncat(buf1,buf2,sizeof(buf1));
    const ParamClass *cls=dynamic_cast<const ParamClass *>(src);
    if( !cls ) break;
    src=cls->_parent;
  }
  return buf1;
}

void ParamClass::Parse( QIStream &in, ParamFile *file)
{
  int c;
  // parse section content
  for(;;)
  {
    c=in.get();
    while( isspace(c) ) c=in.get();
    if( in.eof() || in.fail() )
    {
      //ErrorMessage("%s: EOF encountered.",(const char *)GetContext());
      return;
    }
    if( c=='}' )
    {
      c=in.get();
      while( isspace(c) || c==';' ) c=in.get();
      in.unget();
      break; // section end reached
    }
    in.unget();
    WordBuf word;
    GetAlphaWord(word,sizeof(word),in);
    // word is entry name
    Ref<ParamEntry> newEntry;
    if( !strcmp(word,"class") )
    {
      // "class" may be forgotten now
      GetAlphaWord(word,sizeof(word),in);
      ParamClass *section=new ParamClass(word);
      section->_parent=this;
      // section header
      int c=in.get();
      while( isspace(c) ) c=in.get();
      if( c==':' )
      {
        // base class
        WordBuf base;
        GetAlphaWord(base,sizeof(base),in);
        ParamEntry *entry=Find(base,true,true); // search parents and bases of my parent
        if( !entry )
        {
          ErrorMessage("%s: Undefined base class '%s'",(const char *)GetContext(),(const char *)base);
          return;
        }
        section->_base=dynamic_cast<ParamClass *>(entry);
        if( !base )
        {
          ErrorMessage("%s: '%s' is not class",(const char *)GetContext(),(const char *)base);
          return;
        }
        c=in.get();
      }
      // find opening brace
      while( c!='{' )
      {
        if( !isspace(c) )
        {
          ErrorMessage("%s: '%c' encountered instead of '{'",(const char *)GetContext(),c);
          return;
        }
        c=in.get();
      }
      // parse section content
      section->Parse(in, file);
      newEntry=section;
    }
    /*else if (!strcmp(word, "enum"))
		{
			// "enum" may be forgotten now
			GetAlphaWord(word, sizeof(word), in);
			// enum name not used

			// find opening brace
			int c = in.get();
			while (c != '{')
			{
				if (!isspace(c))
				{
					ErrorMessage("%s: '%c' encountered instead of '{'", (const char *)GetContext(), c);
					return;
				}
				c = in.get();
			}
			int enumValue = 0;
			do
			{
				GetAlphaWord(word, sizeof(word), in);
				RString name = word;
				c = in.get();
				while (isspace(c)) c = in.get();
				if (c == '=')
				{
					c = in.get();
					while (isspace(c)) c=in.get();
					in.unget();
					GetWord(word, sizeof(word), in,",}");
					GameValue result = GGameState.Evaluate(word);
					enumValue = toLargeInt(result);
				}
				GGameState.VarSet(name, GameValue((float)enumValue), true, true);
				enumValue++;
			} while (c == ',');
			if (c == '}')
			{
				c = in.get();
				while (isspace(c) || c == ';') c = in.get();
				in.unget();
			}
			else
			{
				ErrorMessage("%s: '%c' encountered instead of '}'", (const char *)GetContext(), c);
				return;
			}
		}
		else if (!strcmp(word, "__EXEC"))
		{
			// find opening brace
			int c = in.get();
			while (c != '(')
			{
				if (!isspace(c))
				{
					ErrorMessage("%s: '%c' encountered instead of '('", (const char *)GetContext(), c);
					return;
				}
				c = in.get();
			}
			GetWord(word, sizeof(word), in, ")");
			c = in.get();
			if (c == ')')
			{
				c = in.get();
				while (isspace(c) || c == ';') c = in.get();
				in.unget();
			}
			else
			{
				ErrorMessage("%s: '%c' encountered instead of ')'", (const char *)GetContext(), c);
				return;
			}
			GGameState.Execute(word);
		}
		*/
    else
    {
      // word should be value or array
      c=in.get();
      if( c=='[' )
      {
        // word is array name
        ParamArray *array=new ParamArray(word);
        //array->SetFile(file);
        c=in.get();
        while( isspace(c) ) c=in.get();
        if( c!=']' )
        {
          ErrorMessage("Config: %s: '%c' encountered instead of ']'",(const char *)GetContext(word),c);
          return;
        }
        c=in.get();
        while( isspace(c) ) c=in.get();
        if( c!='=' )
        {
          ErrorMessage("Config: %s: '%c' encountered instead of '='",(const char *)GetContext(word),c);
          return;
        }
        array->Parse(in,file);
        c=in.get();
        while( isspace(c) ) c=in.get();
        if( c!=';' )
        {
          ErrorMessage("%s: '%c' encountered instead of ';'",(const char *)GetContext(array->GetName()),c);
          return;
        }
        newEntry=array;
      }
      else
      {
        while( isspace(c) ) c=in.get();
        if( c!='=' )
        {
          char errorContext[1024];
          GetWord(errorContext,sizeof(errorContext),in,"\n");
          RptF("Error context %s",errorContext);
          ErrorMessage("'%s': '%c' encountered instead of '='",(const char *)GetContext(word),c);
          return;
        }
        RStringB valueName = word;
        c=in.get();
        while( isspace(c) ) c=in.get();
        in.unget();
        GetWord(word,sizeof(word),in,";\n\r");
        c=in.get();
        if( c!=';' && c!='\n' && c!='\r' )
        {
          ErrorMessage("'%s': '%c' encountered instead of ';'",(const char *)GetContext(valueName),c);
          return;
        }
        
        /*if (strncmp(word, "__EVAL", 6) == 0)
				{
					RString result = GGameState.Evaluate(word + 6).GetText();
					strcpy(word, result);
				}
				*/
        // check if value is integer or float
        // check for integer: convert using all letters must be 
        ParamEntry *value=NULL;
        bool ok = false;
        int val=ScanInt(word,ok);
        if (ok)
        {
          value = new ParamValueInt(valueName);
          value->SetValue(val);
        }
        else
        {
          float val = ScanFloat(word,ok);
          if (ok)
          {
            value = new ParamValueFloat(valueName);
            value->SetValue(val);
          }
        }
        if (!value)
        {
          value = new ParamValue(valueName);
          value->SetValue(word);
        }
        
        //value->SetFile(file);
        // done recursivelly in the end of parsing
        newEntry=value;
      }
    }
    // check for overload
    if (newEntry)
    {
      int baseIndex=FindIndex(newEntry->GetName());
      if( baseIndex<0 )
      {
        _entries.Add(newEntry);
      }
      else
      {
        ErrorMessage("%s: Member already defined.",(const char *)GetContext(newEntry->GetName()));
      }
    }
  }
  
  _entries.Compact();
  // class parsed
}

void ParamClass::Update(ParamClass &source, ParamFile *file)
{
  for (int i=0; i<source.GetEntryCount(); i++)
  {
    const ParamEntry &srcEntry = source.GetEntry(i);
    ParamEntry *dstEntry = FindEntry(srcEntry.GetName());
    if (srcEntry.IsClass())
    {
      if (!dstEntry)
        dstEntry = AddClass(srcEntry.GetName());
      else if (!dstEntry->IsClass())
      {
        Fail("Cannot update non class from class");
        return;
      }
      
      const ParamClass *srcCls = static_cast<const ParamClass *>(&srcEntry);
      ParamClass *dstCls = static_cast<ParamClass *>(dstEntry);
      dstCls->_parent = this;
      if (srcCls->_base)
      {
        ParamEntry *baseEntry = Find
          (
            srcCls->_base->GetName(), true, true
              );
        if (!baseEntry || !baseEntry->IsClass())
        {
          ErrorMessage
            (
              "%s: Cannot find base class '%s'",
          (const char *)GetContext(),
          (const char *)srcCls->_base->GetName()
            );
          return;
        }
        dstCls->_base = static_cast<ParamClass *>(baseEntry);
      }
      else
        dstCls->_base = NULL;
      dstCls->Update(*const_cast<ParamClass *>(srcCls), file);
    }
    else if (srcEntry.IsArray())
    {
      if (!dstEntry)
        dstEntry = AddArray(srcEntry.GetName());
      else if (!dstEntry->IsArray())
      {
        Fail("Cannot update non array from array");
        return;
      }
      
      const ParamArray *srcArr = static_cast<const ParamArray *>(&srcEntry);
      ParamArray *dstArr = static_cast<ParamArray *>(dstEntry);
      dstArr->Copy(*srcArr);
    }
    else
    {
      // check 
      // create dst entry of corresponding type
      if (dynamic_cast<const ParamValueFloat *>(&srcEntry))
      {
        if (!dstEntry)
        {
          dstEntry = new ParamValueFloat(srcEntry.GetName());
          NewEntry(dstEntry);
        }
        dstEntry->SetValue((float)srcEntry);
      }
      else if (dynamic_cast<const ParamValueInt *>(&srcEntry))
      {
        if (!dstEntry)
        {
          dstEntry = new ParamValueFloat(srcEntry.GetName());
          NewEntry(dstEntry);
        }
        dstEntry->SetValue((int)srcEntry);
      }
      else if (dynamic_cast<const ParamValue *>(&srcEntry))
      {
        if (!dstEntry)
        {
          dstEntry = new ParamValue(srcEntry.GetName());
          NewEntry(dstEntry);
        }
        dstEntry->SetValue((RStringB)srcEntry);
      }
    }
  }
}

void ParamClass::Diagnostics( int indent )
{
  /*
	for( int i=0; i<_entries.Size(); i++ )
	{
		ParamEntry *entry=_entries[i];
		ParamClass *section=dynamic_cast<ParamClass *>(entry);
		ParamValue *value=dynamic_cast<ParamValue *>(entry);
		ParamArray *array=dynamic_cast<ParamArray *>(entry);
		if( section )
		{
			LogF("%*sclass %s",indent*2,"",(const char *)section->GetName());
			LogF("%*s{",indent*2,"");
			section->Diagnostics(indent+1);
			LogF("%*s};",indent*2,"");
		}
		if( value )
		{
			LogF("%*s%s=%s;",indent*2,"",(const char *)value->GetName(),(const char *)value->GetValue());
		}
		if( array )
		{
			LogF("%*s%s[]=",indent*2,"",(const char *)array->GetName());
			for( int i=0; i<array->GetSize(); i++ )
			{
				LogF("%*s%s,",indent*2+2,"",array->GetValue(i).GetValue());
			}
			LogF("%*s;",indent*2,"");
		}
	}
	*/
}

/*
#include "preprocessor/preproc.h"
class Preprocessor : public Preproc
{
protected:
	QIStream *OnEnterInclude(const char *filename)
	{
		if (!QIFStreamB::FileExist(filename))
			return NULL;
		QIFStreamB *stream = new QIFStreamB();
		stream->AutoOpen(filename);
		return stream;
	}
	void OnExitInclude(QIStream *stream)
	{
		if (stream) delete (QIFStreamB *)stream;
	}
};
*/

//DEFINE_FAST_ALLOCATOR(ParamFile)

ParamFile::ParamFile()
{
}

ParamFile::~ParamFile()
{
  //LogF("Destruct paramfile %s",(const char *)GetName());
  Clear();
}

#define DIAG_OPEN 0

#if DIAG_OPEN
static int ParamFileOpen=0;
#endif

struct ParamFileContext
{
  // string pool
  FindArray<RStringB> _strings;
  int _version; // load - backward compatibility
  
  // transfer name
  void TransferString(SerializeBinStream &f, RStringB &string);
  void TransferInt(SerializeBinStream &f, int &a);
  void TransferIndex(SerializeBinStream &f, int &a, int verEncode=3);
};

/*
void ParamFileContext::TransferIndex(SerializeBinStream &f, int &a, int verEncode)
{
	if (_version>=verEncode)
	{
		// index encoded
		// we expect for most cfg files 2B should be enough
		TransferInt(f,a);
	}
	else
	{
		// plain string index
		f.Transfer(a);
	}
}*/
/*
void ParamFileContext::TransferString(SerializeBinStream &f, RStringB &string)
{
	if (f.IsSaving())
	{
		// check if name is already in table
		int index = _strings.Find(string);
		if (index>=0)
		{
			// already there - transfer only index
			TransferIndex(f,index);
		}
		else
		{
			// transfer new index and string defition
			index = _strings.Add(string);
			TransferIndex(f,index);
			f.Transfer(string);
		}
	}
	else
	{
		// transfer index
		int index = -1;
		TransferIndex(f,index);
		if (index<0)
		{
			f.SetError(f.EFileStructure);
			return;
		}
		if (index<_strings.Size())
		{
			// old string - use it
			string = _strings[index];
		}
		else
		{
			// new string - define and use it
			f.Transfer(string);
			Assert (index==_strings.Size());
			_strings.Access(index);
			_strings[index] = string;
		}
	}
}

void ParamFileContext::TransferInt(SerializeBinStream &f, int &a)
{
	// encoded integer (dynamic byte length)
	// TODO: terminator based on signed format (MSB propagated)?
	// use dynamic length int format
	if (f.IsLoading())
	{
		unsigned int val = 0;
		int offset = 0;
		while (f.GetError()==f.EOK)
		{
			unsigned char c = f.LoadChar();
			// transfer 7 bits ber byte
			val |= (c&0x7f)<<offset;
			// check terminator
			if ((c&0x80)==0)
			{
				// extend MSB?
				break;
			}
			offset += 7;
		}
		a = val;
	}
	else 
	{
		unsigned int val = a;
		for(;;)
		{
			unsigned char c = val&0x7f;
			val >>= 7;
			// check MSB?
			if (val)
			{
				f.SaveChar(c|0x80);
			}
			else
			{
				// no more bits left
				f.SaveChar(c);
				break;
			}
		}
	}
}
*/
void ParamFile::Clear()
{
  #if DIAG_OPEN
  if( _entries.Size()>0 )
  {
    LogF("%d: Clear paramfile %s",ParamFileOpen,(const char *)GetName());
    --ParamFileOpen;
  }
  #endif
  Assert( !_parent );
  Assert( !_base );
  _entries.Clear();
  _name=RString(NULL);
}

#define OUTPUT_PREPROC	0

LSError ParamFile::Parse( const char *name)
{
  SetName(name);
  /*	if (!QIFStreamB::FileExist(name)) return LSOK;

	Preprocessor preprocessor;
#if OUTPUT_PREPROC
	QOFStream out;
#else
	QOStream out;
#endif
	if (!preprocessor.Process(&out, name))
	{
	ErrorMessage("Preprocessor failed on file %s - error %d.", name, preprocessor.error);
		return LSStructure;
	}
#if OUTPUT_PREPROC
	::DeleteFile("bin/output.txt");
	out.export("bin/output.txt");
#endif
	*/
  QIFStream in;
  //	in.init(out.str(), out.pcount());
  in.open(name);
  Parse(in);
  #if DIAG_OPEN
  if( _entries.Size()>0 )
  {
    ParamFileOpen++;
    LogF("%d: Parsed paramfile %s",ParamFileOpen,name);
  }
  #endif
  return in.fail() ? in.error() : LSOK;
}

void Indent( QOStream &f, int indent )
{
  while( --indent>=0 ) f<<"\t";
}

inline bool IsNumerical(const char *strValue, float *ret=NULL )
{
  char *endptr = NULL;
  float fValue = strtod(strValue, &endptr);
  if( ret ) *ret=fValue;
  return (*strValue != 0 && *endptr == 0);
}

void ParamRawValue::Save(QOStream &f, int indent) const
{
  float fValue;
  const char *strValue = _value;
  if ( IsNumerical(strValue,&fValue))
  {
    int iValue = toLargeInt(fValue);
    if (iValue == fValue)
    {
      char buffer[256];
      sprintf(buffer, "%d", iValue);
      f << buffer;
    }
    else
    {
      char buffer[256];
      sprintf(buffer, "%f", fValue);
      f << buffer;
    }
  }
  else
  {
    f << "\"";
    while (*strValue)
    {
      if (*strValue == '"') f << "\"\"";
      else f.put(*strValue);
      strValue++;
    }
    f << "\"";
  }
}

/*
void ParamRawValue::SerializeBin(SerializeBinStream &f)
{
	// string are very likely to be class names
	// and there is high probability they can be reused
	ParamFileContext *context = (ParamFileContext *)f.GetContext();
	context->TransferString(f,_value);
	//f.Transfer(_value);
}*/

void ParamRawValueFloat::Save(QOStream &f, int indent) const
{
  char buffer[256];
  sprintf(buffer, "%f", _value);
  f << buffer;
}

RStringB ParamRawValueFloat::GetValue() const
{
  char buf[256];
  sprintf(buf,"%g",_value);
  return buf;
}

/*
void ParamRawValueFloat::SerializeBin(SerializeBinStream &f)
{
	f.Transfer(_value);
}

void ParamRawValueInt::SerializeBin(SerializeBinStream &f)
{
	// before version 3 integers were not encoded
	f.Transfer(_value);
}
*/
void ParamRawValueInt::Save(QOStream &f, int indent) const
{
  char buffer[256];
  sprintf(buffer, "%d", _value);
  f << buffer;
}

RStringB ParamRawValueInt::GetValue() const
{
  char buf[256];
  sprintf(buf,"%d",_value);
  return buf;
}

void ParamRawArray::Parse(QIStream &in, ParamFile *file)
{
  bool isFirst=true;
  int c=in.get();
  while( isspace(c) ) c=in.get();
  if( c!='{' )
  {
    //ErrorMessage("Config: %s: '%c' encountered instead of '{'",(const char *)GetContext(GetName()),c);
    ErrorMessage("Config: '%c' encountered instead of '{'",c);
    return;
  }
  for(;;)
  {
    // check for sub-array
    c=in.get();
    in.unget();
    if (c=='{')
    {
      // sub-array
      Ref<ParamArrayValueArray> sub = new ParamArrayValueArray();
      IParamArrayValue *val = sub;
      sub->Parse(in,file);
      _value.Add(val);
      c=in.get();
    }
    else
    {
      WordBuf word;
      bool someWord = GetWord(word,sizeof(word),in,",;}");
      c=in.get();
      // c may be one of ,;} or \n or \r
      if( c==',' || c==';' || someWord )
      {
        /*if (strncmp(word, "__EVAL", 6) == 0)
				{
					RString result = GGameState.Evaluate(word + 6).GetText();
					strcpy(word, result);
				}*/
        
        // autodetect value type
        bool ok = false;
        int val = ScanInt(word,ok);
        if (ok) AddValue(val);
        else
        {
          float val = ScanFloat(word,ok);
          if (ok) AddValue(val);
          else AddValue(word);
        }
        isFirst=false;
      }
    } // if subarray else 
    if( c<0 )
    {
      //ErrorMessage("%s: EOF encountered.",(const char *)GetContext(GetName()));
      ErrorMessage("EOF encountered.");
      return;
    }
    while( isspace(c) ) c=in.get();
    if( c=='}' ) break; // array terminated
    // , or ; should follow
    if( c!=',' && c!=';' )
    {
      //ErrorMessage("Config: %s: '%c' encountered instead of ','",(const char *)GetContext(word),c);
      ErrorMessage("Config: '%c' encountered instead of ','",c);
      return;
    }
  }
  Compact();
}

void ParamRawArray::Save( QOStream &f, int indent ) const
{
  // check if some item is string
  // (item may be numerical or string)
  bool someString = false;
  for( int i=0; i<_value.Size(); i++ )
  {
    
    if
      (
        dynamic_cast<ParamArrayValueArray *>(_value[i].GetRef()) ||
          !IsNumerical(_value[i]->GetValue())
            )
            {
              someString = true;
              break;
            }
    
  }
  if( someString )
  {
    // array of string values (at least one string)
    f << "\r\n";
    Indent(f,indent);
    f << "{\r\n";
    for( int i=0; i<_value.Size(); i++ )
    {
      Indent(f,indent+1);
      _value[i]->Save(f,indent);
      if( i<_value.Size()-1 ) f << ",";
      f << "\r\n";
    }
    Indent(f,indent);
    f << "};" << "\r\n";
  }
  else
  {
    // array of numeric values
    f << "{";
    for( int i=0; i<_value.Size(); i++ )
    {
      _value[i]->Save(f,indent);
      if( i<_value.Size()-1 ) f << ",";
    }
    f << "};" << "\r\n";
  }
}

/*
void ParamRawArray::SerializeBin(SerializeBinStream &f)
{
	ParamFileContext *context = (ParamFileContext *)f.GetContext();
	// make table of names
	if (f.IsSaving())
	{
		int n = _value.Size();
		context->TransferIndex(f,n,4);
		for (int i=0; i<n; i++)
			_value[i]->SerializeBin(f);
	}
	else
	{
		Assert(f.IsLoading());
		int n = 0;
		context->TransferIndex(f,n,4);
		_value.Realloc(n);
		_value.Resize(n);
		for (int i=0; i<n; i++)
		{
			// CreateValue - create value of appropriate type
			_value[i] = CreateParamArrayValue(f);
			_value[i]->SerializeBin(f);
		}
	}
}
*/
void ParamArray::Parse(QIStream &in, ParamFile *file)
{
  ParamRawArray::Parse(in,file);
}

void ParamArray::Save(QOStream &f, int indent) const
{
  Indent(f,indent);
  f << _name << "[]=";
  ParamRawArray::Save(f,indent);
}

/*
void ParamArray::SerializeBin(SerializeBinStream &f)
{
	ParamFileContext *context = (ParamFileContext *)f.GetContext();
	// make table of names
	context->TransferString(f,_name);
	ParamRawArray::SerializeBin(f);
}
*/

template <class ParamRawValueSpec>
void ParamValueSpec<ParamRawValueSpec>::Save( QOStream &f, int indent ) const
{
  Indent(f,indent);
  f << _name << "=";
  ParamRawValueSpec::Save(f,indent);
  f << ";\r\n";
}

/*
template <class ParamRawValueSpec>
void ParamValueSpec<ParamRawValueSpec>::SerializeBin(SerializeBinStream &f)
{
	if (f.IsSaving())
	{
		char type = GetValueType();
		f.Transfer(type);
	}
	// Loading - type processed by CreateParamValue
	
	ParamFileContext *context = (ParamFileContext *)f.GetContext();
	// make table of names
	context->TransferString(f,_name);
	ParamRawValueSpec::SerializeBin(f);
}*/

void ParamClass::Save( QOStream &f, int indent ) const
{
  Indent(f,indent);
  f << "class " << _name << "\r\n";
  // TODO: save parents (base class name)
  Indent(f,indent);
  f << "{\r\n";
  for( int i=0; i<GetEntryCount(); i++ )
  {
    GetEntry(i).Save(f,indent+1);
  }
  Indent(f,indent);
  f << "};\r\n";
}

/*
void ParamEntry::SerializeBin(SerializeBinStream &f)
{
	Fail("Should be never reached (Pure virtual function?)");
}

void ParamClass::SerializeBin(SerializeBinStream &f)
{
	const int idClass = 0;
	const int idValue = 1;
	const int idArray = 2;

	ParamFileContext *context = (ParamFileContext *)f.GetContext();
	// make table of names
	context->TransferString(f,_name);

	if (f.IsSaving())
	{
		RString base = "";
		if (_base) base = _base->GetName();
		f.Transfer(base);
		int n = _entries.Size();
		context->TransferIndex(f,n,4);
		for (int i=0; i<n; i++)
		{
			ParamEntry *entry = _entries[i];
			if (entry->IsClass())
			{
				f.SaveChar(idClass);
			}
			else if (entry->IsArray())
			{
				f.SaveChar(idArray);
			}
			else // value
			{
				f.SaveChar(idValue);
			}
			// SerializeBin is virtual function of ParamEntry
			entry->SerializeBin(f);
		}
	}
	else
	{
		Assert(f.IsLoading());
		RString base;
		f.Transfer(base);
		if (base.GetLength() > 0)
		{
			Assert(_parent);
			ParamEntry *entry = _parent->Find(base, true, true); // search parents and bases of my parent
			_base = static_cast<ParamClass *>(entry);
		}

		int n;
		context->TransferIndex(f,n,4);
		_entries.Resize(n);
		for (int i=0; i<n; i++)
		{
			int id = f.LoadChar();
			if (id == idClass)
			{
				ParamClass *cls = new ParamClass();
				cls->_parent = this;
				cls->SerializeBin(f);
				_entries[i] = cls;
			}
			else if (id == idArray)
			{
				ParamArray *array = new ParamArray("");
				array->SerializeBin(f);
				_entries[i] = array;
			}
			else // value
			{
				ParamEntry *entry = CreateParamValue(f);
				entry->SerializeBin(f);
				_entries[i] = entry;
			}
		}
	}
}
*/
void ParamFile::Save( QOStream &f, int indent ) const
{
  for( int i=0; i<GetEntryCount(); i++ )
  {
    GetEntry(i).Save(f,indent);
  }
}

LSError ParamFile::Save( const char *name) const
{
  QOFStream f;
  f.open(name);
  for( int i=0; i<GetEntryCount(); i++ )
  {
    GetEntry(i).Save(f,0);
  }
  f.close();
  return f.error();
}

void ParamFile::Parse( QIStream &in)
{
  _entries.Clear();
  // read all class definitions
  //	_vars = new GameVarSpace();
  //	GGameState.BeginContext(_vars);
  // variable context defined
  // int this context all enum values are defined
  ParamClass::Parse(in, this);
  //	GGameState.EndContext();
  SetFile(this);
  if( in.fail() || in.eof() )
  {
    return;
  }
  ErrorMessage("Config: some input after EndOfFile.");
}

void ParamFile::Reload()
{
  ParamFile source;
  source.Parse(_name);
  //	_vars = source._vars;
  Update(source, this);
  SetFile(this);
}

/*
void ParamFile::SerializeBin(SerializeBinStream &f)
{
	ParamFileContext context;
	void *oldContext = f.GetContext();
	f.SetContext(&context);
	if (!f.Version('Par\0'))
	{
		f.SetError(f.EBadFileType);
		f.SetContext(oldContext);
		return;
	}
	context._version = 4;
	f.Transfer(context._version);
	if (f.IsLoading())
	{
		if (context._version<2)
		{
			WarningMessage("Bad version in ParamFile");
			f.SetError(f.EBadVersion);
			f.SetContext(oldContext);
			return;
		}
	}
	ParamClass::SerializeBin(f);
	f.SetContext(oldContext);
}

bool ParamFile::ParseBin( const char *name)
{
	_entries.Clear();

	SetName(name);
	if (!QIFStreamB::FileExist(name)) return LSOK;

	QIFStreamB in;
	in.AutoOpen(name);
	
	SerializeBinStream f(&in);

	SerializeBin(f);
	if (f.GetError() != SerializeBinStream::EOK) return false;

	SetFile(this);

	// load variables
	_vars = new GameVarSpace();
	int n;
	f.Transfer(n);
	for (int i=0; i<n; i++)
	{
		RString name;
		int value;
		f.Transfer(name);
		f.Transfer(value);
		GameVariable var(name, (float)value, true);
		_vars->_vars.Add(var);
	}

#if DIAG_OPEN
	if (_entries.Size() > 0)
	{
		ParamFileOpen++;
		LogF("%d: Parsed paramfile %s",ParamFileOpen,name);
	}
#endif
	
	return f.GetError() == SerializeBinStream::EOK;
}

bool ParamFile::ParseBin(QFBank &bank, const char *name)
{
	_entries.Clear();

	SetName(name);

	QIFStreamB in;
	in.open(bank, name);
	
	SerializeBinStream f(&in);

	SerializeBin(f);
	if (f.GetError() != SerializeBinStream::EOK) return false;

	SetFile(this);

	// load variables
	_vars = new GameVarSpace();
	int n;
	f.Transfer(n);
	for (int i=0; i<n; i++)
	{
		RString name;
		int value;
		f.Transfer(name);
		f.Transfer(value);
		GameVariable var(name, (float)value, true);
		_vars->_vars.Add(var);
	}

#if DIAG_OPEN
	if (_entries.Size() > 0)
	{
		ParamFileOpen++;
		LogF("%d: Parsed paramfile %s",ParamFileOpen,name);
	}
#endif
	
	return f.GetError() == SerializeBinStream::EOK;
}

bool ParamFile::SaveBin( const char *name )
{
	QOFStream out;
	out.open(name);

	SerializeBinStream f(&out);

	SerializeBin(f);

	// save variables
	int n = _vars ? _vars->_vars.NItems() : 0;
	f.Transfer(n);
	if (n > 0)
		for (int i=0; i<_vars->_vars.NTables(); i++)
		{
			AutoArray<GameVariable> &table = _vars->_vars.GetTable(i);
			for (int j=0; j<table.Size(); j++)
			{
				RString name = table[j]._name;
				int value = ToInt(table[j]._value);
				f.Transfer(name);
				f.Transfer(value);
			}
		}

	out.close();

	return f.GetError() == SerializeBinStream::EOK;
}

bool ParamFile::ParseBinOrTxt(const char *name)
{
	if (ParseBin(name)) return true;
	return Parse(name) == LSOK;
}
*/
RString GetDefaultName( RString baseName, const char *dDir, const char *dExt )
{
  if( !baseName || !baseName[0] ) return "";
  char buf[256];
  const char *ext=strchr(baseName,'.');
  *buf=0;
  if( baseName[0]!='\\' )
  {
    strcat(buf,dDir);
    strcat(buf,baseName);
  }
  else
  {
    strcat(buf,baseName+1);
  }
  if( !ext ) strcat(buf,dExt);
  strlwr(buf);
  return buf;
  
}

RString GetShapeName( RString baseName )
{
  return GetDefaultName(baseName,"data3d\\",".p3d");
}

RString GetAnimationName( RString baseName )
{
  return GetDefaultName(baseName,"anim\\",".rtm");
}

RString GetPictureName( RString baseName )
{
  return GetDefaultName(baseName,"data\\",".paa");
}

RString GetSoundName( RString baseName )
{
  return GetDefaultName(baseName,"sound\\",".wss");
}

RString GetFontName( RString baseName )
{
  return GetDefaultName(baseName,"fonts\\","");
}

