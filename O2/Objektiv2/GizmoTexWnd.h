#if !defined(AFX_GIZMOTEXWND_H__7A9DAA44_5115_4D6C_817B_937E66C07882__INCLUDED_)
#define AFX_GIZMOTEXWND_H__7A9DAA44_5115_4D6C_817B_937E66C07882__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GizmoTexWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGizmoTexWnd window

class CGizmoTexWnd : public CStatic
  {
  // Construction
  bool hq;
  float scale;
  float xmid;
  float ymid;
  CDC memdc;
  CBitmap membmp;
  HBITMAP dcbmp;
  HMATRIX disp; //inverzni matice displaye
  HMATRIX invdisp; //inverzni matice displaye
  CPoint last;
  enum MousePos 
    {left,left_top,top,right_top,right,right_bottom,bottom,left_bottom,inside,outside};
  void PointDiff(CPoint &ms) 
    {CPoint x=ms;ms=ms-last;last=x;}
  MousePos lastpos;
  bool altpress;  
  BITMAPINFO *texback;
  
  public:
    CGizmoTexWnd();
    
    
    static float fleft;
    static float fright;
    static float ftop;
    static float fbottom;
    // Attributes
  public:
    
    // Operations
  public:
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CGizmoTexWnd)
    //}}AFX_VIRTUAL
    
    // Implementation
  public:
    void LoadBitmap(const char *filename);
    MousePos WhereMouse(CPoint &pt,LPHVECTOR pos);
    void Redraw();
    void OnDrawToBitmap(CDC *dc, int width, int height);
    virtual ~CGizmoTexWnd();
    
    // Generated message map functions
  protected:
    //{{AFX_MSG(CGizmoTexWnd)
    afx_msg void OnPaint();
    afx_msg void OnDestroy();
    afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
    afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    //}}AFX_MSG
    
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GIZMOTEXWND_H__7A9DAA44_5115_4D6C_817B_937E66C07882__INCLUDED_)
