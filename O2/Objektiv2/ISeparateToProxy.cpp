#include "StdAfx.h"
#include "Objektiv2.h"
#include "Objektiv2Doc.h"
#include "ComInt.h"
#include "ISimpleCommand.h"
#include "ICreateProxy.h"
#include "ISelUse.h"
#include "LodList.h"
#include ".\iseparatetoproxy.h"
#include "geVector4.h"
#include "IMerge.h"
#include "ROCheck.h"
#include "../ObjektivLib/ObjToolClipboard.h"
#include "../ObjektivLib/ObjToolProxy.h"
#include <io.h>


void GetProxyTransform(FaceT& fc, ObjectData *obj, Matrix4 &trans);

static void CopyObjectSelection(ObjectData *source, ObjectData *target,const char *selectionName, const geVector4 *pin)
  {
  SRef<ObjectData> obj=new ObjectData(*source);
  obj->UseNamedSel(selectionName);
  obj->GetTool<ObjToolClipboard>().ExtractSelection();
  obj->DeleteNamedSel(selectionName);
  if (pin) 
    {
    Vector3 vec=*pin;
    for (int i=0;i<obj->NPoints();i++) obj->Point(i)-=vec;
    }
  target->Merge(*obj);
  target->SetNamedProp("autocenter","0");
  target->ClearSelection();
  }

static void ExportToProxyP3DSingleLod(LODObject *obj, const char *selectionName, LODObject *target, float lod,const geVector4 *pin, bool aszero)
  {  
  int levelsrc=obj->FindLevel(lod);  
  if (aszero) lod=0;
  int leveltrg=target->FindLevelExact(lod);  
  if (leveltrg==-1) 
    {target->AddLevel(ObjectData(),lod);leveltrg=target->FindLevelExact(lod);}
  CopyObjectSelection(obj->Level(levelsrc),target->Level(leveltrg),selectionName,pin);
  }

static bool ExportToProxy(LODObject *obj,bool allLods, const char *selectionName, const char *objectPath,const geVector4 *pin)
  {
  LODObject targetP3d;
  targetP3d.SetResolution(targetP3d.ActiveLevel(),-999.0f);
  if (allLods)
    {
    for (int i=0;i<obj->NLevels();i++)
      {
      NamedSelection *ns=obj->Level(i)->GetNamedSel(selectionName);
      if (ns!=NULL) ExportToProxyP3DSingleLod(obj,selectionName,&targetP3d,obj->Resolution(i),pin,false);
      }
    }
  else
    {
    NamedSelection *ns=obj->Active()->GetNamedSel(selectionName);
    if (ns!=NULL) ExportToProxyP3DSingleLod(obj,selectionName,&targetP3d,obj->Resolution(obj->ActiveLevel()),pin,true);    
    }
  if (targetP3d.Resolution(0)<-100.0f)  targetP3d.DeleteLevel(0);
  if (targetP3d.Save(Pathname(objectPath),1,false,NULL,NULL)!=0) return false;
  return true;
  }


bool MoveSelectionToProxy(LODObject *obj,bool allLods, const char *selectionName, const char *objectPath,const geVector4 *pin)
  {
  if (_access(objectPath,02)==0)
    {
    CString msg;AfxFormatString1(msg,IDS_PROXYASKOVERWITE,objectPath);
    if (AfxMessageBox(msg,MB_YESNO|MB_ICONQUESTION)==IDNO) return false;
    }
  if (theApp.rocheck.TestFileRO(objectPath,ROCHF_DisableSaveAs)==ROCHK_FileRO) return false;
  if (ExportToProxy(obj,allLods,selectionName,objectPath,pin)==false) return false;
  comint.Begin(WString(IDS_MOVESELECTIONTOPROXY));

  int from=0;
  int to=obj->NLevels();
  int active;
  if (!allLods) to=1;else active=obj->ActiveLevel();
  for (int i=from;i<to;i++)
    {    
    if (allLods)
      {if (obj->Level(i)->FindNamedSel(selectionName)==-1) continue;}
    else
      {if (obj->Active()->FindNamedSel(selectionName)==-1) continue;}
    if (allLods) comint.Run(new CILodSetActive(obj->Resolution(i)));
    ObjectData *odata=obj->Active();
    comint.Run(new CISelUse(selectionName));
    comint.Run(new CISimpleCommand(CIS_SPLIT));
    comint.Run(new CISimpleCommand(CIS_DELETE));    
    if (objectPath[1]==':') objectPath+=2;
/*    char buff[256];
    int proxyIndex=1;
    sprintf(buff,"%s%%s.%%02d",WString(IDS_PROXYNAME));
    for(;;)
      { 
      selection.Sprintf(buff,objectPath,proxyIndex);
      
      if( odata->FindNamedSel(selection)<0 ) break;
      proxyIndex++;
      }*/
    HMATRIX zerom;    
    if (pin) Translace(zerom,pin->x,pin->y,pin->z);else JednotkovaMatice(zerom);
    comint.Run(new CICreateProxy(zerom,objectPath));
    comint.Run(new CIDeleteSelection(selectionName));
    }
  if (allLods)  comint.Run(new CILodSetActive(obj->Resolution(active)));
  return true;
}

#include "dlginputfile.h"


bool MoveSelectionToProxyEx(LODObject *obj,bool allLods, const char *selectionName, const char *curObjName,const geVector4 *pin)
  {
  CDlgInputFile dlg;
  dlg.filter.LoadString(IDS_FILESAVEFILTERS);
  char buff[MAX_PATH*4];
  strcpy(buff,curObjName);
  char *c=strrchr(buff,'\\')+1;
  strcpy(c,"data");
  strcat(c,"\\");
  strcat(c,selectionName);
  strcat(c,"\\");
  strcat(c,selectionName);
  strcat(c,".p3d");
  dlg.vFilename=buff;
  dlg.save=true;
  dlg.title.LoadString(IDS_MOVETOPROXYTITLE);
  dlg.DoModal();
  int i=1;
  while (Pathname::GetPartFromPath(dlg.vFilename,i++,buff,sizeof(buff),-1))
    CreateDirectory(buff,NULL);    
  return MoveSelectionToProxy(obj,allLods,selectionName,dlg.vFilename,pin);
  }

static bool ExtractProxyObject(ObjectData *obj, ObjectData *src, const char *selectionName,const char *name)
  {
  Selection *sel=obj->GetNamedSel(selectionName);
  int i;
  if (sel==NULL) return false;
  for (i=0;i<obj->NFaces();i++) if (sel->FaceSelected(i)) break;
  if (i==obj->NFaces()) return false;
  FaceT fc(obj,i);
  Matrix4 transform;
  GetProxyTransform(fc,obj,transform);  
  for (i=0;i<src->NPoints();i++)
    {
    PosT &pt=src->Point(i);
    pt.SetPoint(transform.FastTransform(pt));
    }
  comint.Run(new CIMerge(src,selectionName+strlen(ObjToolProxy::ProxyPrefix)));
  comint.Run(new CISelUse(selectionName));
  comint.Run(new CISimpleCommand(CIS_DELETE));
  comint.Run(new CIDeleteSelection(selectionName));
  return true;
  }

bool ExtractProxy(ObjectData *obj, const char *selectionName, float resolution)
  {
  const char *selection=selectionName;
  const char *prefix=ObjToolProxy::ProxyPrefix;
  const char *c=prefix;
  while (*c && toupper(*c)==toupper(*selectionName)) {c++;selectionName++;}
  if (*c) return false;
  while (isspace(*selectionName)) selectionName++;
  c=strrchr(selectionName,'.');
  if (!c) return false;
  c++;
  for (int i=0;c[i];i++) if (!isdigit(c[i])) return false;
  const char *path=doc->GetDocumentName();
  ObjectData *proxy=doc->_proxyCache.GetBestProxyObject(selectionName,resolution);
  if (proxy==0) return false;
  ObjectData *src=new ObjectData(*proxy);
  if (ExtractProxyObject(obj, src, selection,c)==false) delete src;
  return true;
  }

bool ExtractProxyAllLods(LODObject *lod, const char *selectionName)
  {
    int curlevel=lod->ActiveLevel();
    int i;
    for (i=0;i<lod->NLevels();i++)
      {
      if (lod->Level(i)->FindNamedSel(selectionName)!=-1)
          comint.Run(new CILodSetActive(lod->Resolution(i)));
      bool succ=ExtractProxy(lod->Active(), selectionName, lod->Resolution(lod->ActiveLevel()));
      if (succ==false) return false;
      }
  comint.Run(new CILodSetActive(lod->Resolution(curlevel)));
  return true;
  }
