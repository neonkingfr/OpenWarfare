// 3DC2.h: interface for the C3DC2 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_3DC2_H__FF14CC89_A785_4CE3_B2A7_68CDB533626F__INCLUDED_)
#define AFX_3DC2_H__FF14CC89_A785_4CE3_B2A7_68CDB533626F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "3DC.h"

class C3DC2 : public C3DC  
{
  struct Vertex
  {
    HVECTOR vect;
    CPen *pen;
    CBrush *brush;
    char region;
    char pointstyle;
    char pointsize;
  };
  Vertex *vlist;
  int nvl;
  public:
    void Attach(C3DC &dc);
    bool GetFaceVisible(int pt1, int pt2, int pt3);
    void Line3D(int from, int to);
    void DrawAllPoints(int pcount);
    void SetPointStyle(int vx, char style, char size);
    void SetVertex(int idx, HVECTOR vect, CPen *pen, CBrush *brush);
    //	LPHVECTOR GetVertex(int idx) {return vext[idx];}
    void AllocVList(int vl);
    bool GetScreenCoords(int index, POINT &pt);
    C3DC2();
    virtual ~C3DC2();
    
};

class CFaceVisList
{
  protected:
    unsigned char *facevis;
    int nfaces;
  public:
    bool GetVisibility(int face);
    void SetVisibility(int face, bool vis);
    void Alloc(int nfc);
    CFaceVisList();
    virtual ~CFaceVisList();
};

#endif // !defined(AFX_3DC2_H__FF14CC89_A785_4CE3_B2A7_68CDB533626F__INCLUDED_)
