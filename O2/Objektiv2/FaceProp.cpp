// FaceProp.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "MainFrm.h"
#include "ComInt.h"
#include "FaceProp.h"
#include "Objektiv2View.h"
#include <malloc.h>
#include ".\faceprop.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFaceProp dialog


CFaceProp::CFaceProp(CWnd* pParent /*=NULL*/)
  : CDialog(CFaceProp::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CFaceProp)
    Bias = -1;
    Lighting = -1;
    VertNum = _T("");
    TU1 = _T("");
    TU2 = _T("");
    TU3 = _T("");
    TU4 = _T("");
    TV1 = _T("");
    TV2 = _T("");
    TV3 = _T("");
    TV4 = _T("");
    TexName = _T("");
    userValue = _T("");
    MatName = _T("");
    //}}AFX_DATA_INIT
    deftmr=0;
    initialized=false;
    }

//--------------------------------------------------

void CFaceProp::DoDataExchange(CDataExchange* pDX)
  {
  if (pDX->m_bSaveAndValidate)
    CorrectFloatValues(this,IDC_TU1,IDC_TU2,IDC_TU3,IDC_TU4,IDC_TV1,IDC_TV2,IDC_TV3,IDC_TV4,0);
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CFaceProp)
  DDX_Control(pDX, IDC_BROWSE2, BBrowse2);
  DDX_Control(pDX, IDC_LISTEX, List);
  DDX_Control(pDX, IDC_SETFROMVIEW, BSetFromView);
  DDX_Control(pDX, IDC_SETFROMLIST, BSetFromList);
  DDX_Control(pDX, IDC_BROWSE, BBrowse);
  DDX_Control(pDX, IDC_COLOR, BColor);
  DDX_Control(pDX, IDC_ENABLE_TEXMERGE, EnTexMerge);
  DDX_Control(pDX, IDC_ENABLE_SHADOW, EnShadow);
  DDX_Radio(pDX, IDC_BIAS_NORMAL, Bias);
  DDX_Radio(pDX, IDC_LIGHTING_NORMAL, Lighting);
  DDX_Text(pDX, IDC_NUMVERTICES, VertNum);
  DDX_Text(pDX, IDC_TU1, TU1);
  DDX_Text(pDX, IDC_TU2, TU2);
  DDX_Text(pDX, IDC_TU3, TU3);
  DDX_Text(pDX, IDC_TU4, TU4);
  DDX_Text(pDX, IDC_TV1, TV1);
  DDX_Text(pDX, IDC_TV2, TV2);
  DDX_Text(pDX, IDC_TV3, TV3);
  DDX_Text(pDX, IDC_TV4, TV4);
  DDX_Text(pDX, IDC_EDIT1, TexName);
  DDX_Text(pDX, IDC_USERVALUE, userValue);
  DDX_Text(pDX, IDC_MATERIAL, MatName);
  //}}AFX_DATA_MAP
  DDX_Control(pDX, IDC_SETFROMLIST2, BFromList2);
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CFaceProp, CDialog)
  //{{AFX_MSG_MAP(CFaceProp)
  ON_BN_CLICKED(IDC_SELECTALL, OnSelectall)
    ON_BN_CLICKED(IDC_FILTER, OnFilter)
      ON_BN_CLICKED(IDC_APPLY, OnApply)
        ON_BN_CLICKED(IDC_SETFROMLIST, OnSetTexture)
          ON_BN_CLICKED(IDC_CREATESEL, OnCreatesel)
            ON_BN_CLICKED(IDC_BROWSE2, OnBrowse2)
              ON_NOTIFY(LVN_ITEMCHANGING, IDC_LISTEX, OnItemchangedListex)
                ON_WM_TIMER()
                  ON_BN_CLICKED(IDC_SETFROMVIEW, OnSetTexture)
                    ON_BN_CLICKED(IDC_BROWSE, OnSetTexture)
                    ON_BN_CLICKED(IDC_COLOR, OnSetColor)
                      //}}AFX_MSG_MAP
                      ON_BN_CLICKED(IDC_SETFROMLIST2, OnBnClickedSetfromlist2)
                      ON_WM_SIZE()
                      ON_WM_SIZING()
END_MESSAGE_MAP()
                        
                        /////////////////////////////////////////////////////////////////////////////
                        // CFaceProp message handlers
                        
                        static void SetFloat(CString& s, float f, bool first)
                          {
                          if (first) s.Format("%.10f",f);
                          else 
                            {
                            CString p;
                            p.Format("%.10f",f);
                            if (p!=s) s="";
                            }
                          }

//--------------------------------------------------

void CFaceProp::SetInt(CString& s, int f, bool first)
  {
  if (first) s.Format("%d",f);
  else 
    {
    CString p;
    p.Format("%d",f);
    if (p!=s) s="";
    }
  }

//--------------------------------------------------

static int SetFlags(int src, UINT flags, int count, bool first,...)
  {
  va_list list;
  va_start(list,first);
  int a=0;
  int out=0;
  while (count--)
    {
    int mask;
    mask=va_arg(list,DWORD);
    if (flags & mask) out=a;
    a++;
    }
  if (first) return out;
  else if (out!=src) return -1;
  else return out;
  }

//--------------------------------------------------

static void SetButton(CButton& butt, UINT flag, UINT mask, bool first)
  {
  int b=(flag & mask)!=0;
  if (first)
    butt.SetCheck(b);
  else
    if (b!=butt.GetCheck()) butt.SetCheck(2);
  }

//--------------------------------------------------

void CFaceProp::SetDialogData(FaceT &face,bool first)
  {  
  int p=(face.GetFlags() & FACE_USER_MASK)>>FACE_USER_SHIFT;
  SetInt(userValue,p,first);
  SetFloat(TU1,face.GetU(0),first);
  SetFloat(TV1,face.GetV(0),first);
  SetFloat(TU2,face.GetU(1),first);
  SetFloat(TV2,face.GetV(1),first);
  SetFloat(TU3,face.GetU(2),first);
  SetFloat(TV3,face.GetV(2),first);
  if (face.N()>3)
    {
    SetFloat(TU4,face.GetU(3),first);
    SetFloat(TV4,face.GetV(3),first);
    }
  Lighting=SetFlags(Lighting,face.GetFlags(),5,first,0,FACE_BOTHSIDESLIGHT,FACE_SKYLIGHT,FACE_REVERSELIGHT,FACE_FLATLIGHT);
  Bias=SetFlags(Bias,face.GetFlags(),4,first,0,FACE_Z_BIAS_STEP,FACE_Z_BIAS_STEP*2,FACE_Z_BIAS_STEP*3);  
  SetButton(EnShadow,~face.GetFlags(),FACE_NOSHADOW,first);
  SetButton(EnTexMerge,~face.GetFlags(),FACE_DISABLE_TEXMERGE,first);
  if (first) 
    {
    TexName=face.GetTexture();
    MatName=face.GetMaterial();
    }
  else 
    {
    if (TexName!=face.GetTexture()) TexName="...";
    if (MatName!=face.GetMaterial()) MatName="...";
    }
  }

//--------------------------------------------------

void CFaceProp::GetDialogData(FaceT &face, FaceT &mask)
  {
  
  UpdateData(TRUE);
  UINT LightTable[]=
    {0,FACE_BOTHSIDESLIGHT,FACE_SKYLIGHT,FACE_REVERSELIGHT,FACE_FLATLIGHT};
  UINT BiasTable[]=
    {0,FACE_Z_BIAS_STEP,FACE_Z_BIAS_STEP*2,FACE_Z_BIAS_STEP*3};
  float u,v;
  CString *idcu[]={&TU1,&TU2,&TU3,&TU4};
  CString *idcv[]={&TV1,&TV2,&TV3,&TV4};
  for (int i=0;i<4;i++)
    {
    mask.SetU(i,(sscanf(*idcu[i],"%f",&u)==1)*1.0f);
    mask.SetV(i,(sscanf(*idcv[i],"%f",&v)==1)*1.0f);
    face.SetU(i,u);
    face.SetV(i,v);
    }
  unsigned long flags;
  mask.SetFlags((sscanf(userValue,"%d",&flags)==1)*FACE_USER_MASK);
  face.SetFlags(flags<<FACE_USER_SHIFT);
  if (Lighting!=-1)
    {
    mask.SetFlags(FACE_LIGHT_MASK,FACE_LIGHT_MASK);
    face.SetFlags(FACE_LIGHT_MASK,LightTable[Lighting]);
    }
  if (Bias!=-1)
    {
    mask.SetFlags(FACE_Z_BIAS_MASK,FACE_Z_BIAS_MASK);
    face.SetFlags(FACE_Z_BIAS_MASK,BiasTable[Bias]);
    }
  if (EnShadow.GetCheck()!=2) 
    {
    mask.SetFlags(FACE_NOSHADOW,FACE_NOSHADOW);
    face.SetFlags(FACE_NOSHADOW,FACE_NOSHADOW*(1-EnShadow.GetCheck()));
    }
  if (EnTexMerge.GetCheck()!=2) 
    {
    mask.SetFlags(FACE_DISABLE_TEXMERGE,FACE_DISABLE_TEXMERGE);
    face.SetFlags(FACE_DISABLE_TEXMERGE,FACE_DISABLE_TEXMERGE*(1-EnTexMerge.GetCheck()));
    }
  if (TexName!="...")
    {
    mask.SetTexture("A");
    face.SetTexture((LPCTSTR)TexName);
    }
  if (MatName!="...")
    {
    mask.SetMaterial("A");
    face.SetMaterial((LPCTSTR)MatName);
    }
  }

//--------------------------------------------------

static void SetItemText(CListCtrl &list, int pos, int clm, const char *text, int *mem)
  {
  int w=list.GetStringWidth(text);
  if (mem[clm]<w) mem[clm]=w;
  list.SetItemText(pos,clm,text);
  }

//--------------------------------------------------

void CFaceProp::LoadToList()
  {
  int xp=25;  
  
  
  bool nw=List.GetItemCount()==0;
  int cnt=obj->NFaces();
  int mem[10];
  memset(mem,0,sizeof(mem));
  ProgressBar<int> pb(cnt);
  for (int i=0,idx=0;i<cnt;i++) if (obj->FaceSelected(i))
    {
    pb.SetPos(i);
    char text[50];
    FaceT face(obj,i);    
    if (nw) List.InsertItem(idx,"");
    for (int j=0;j<4;j++)
      {
      if (j<face.N()) sprintf(text,"%8d",face.GetPoint(j));
      else strcpy(text,"");	  
      SetItemText(List,idx,j,text,mem);
      }
    SetItemText(List,idx,4,face.GetTexture(),mem);
    SetItemText(List,idx,5,face.GetMaterial(),mem);
    SetItemText(List,idx,6,_itoa((face.GetFlags() & FACE_USER_MASK)>>FACE_USER_SHIFT,text,10),mem);
    SetItemText(List,idx,7,_itoa(i,text,10),mem);
    List.SetItemData(idx,i);
    idx++;
    }
  if (nw) OnSelectall();
  for (int k=0;k<8;k++) List.SetColumnWidth(k,mem[k]+15);
  }

//--------------------------------------------------

void CFaceProp::ForAllSelectedLoad()
  {
  if (GetCapture()!=&List) SetCursor(theApp.LoadStandardCursor(IDC_WAIT));
  int cnt=List.GetItemCount();
  VertNum.Format("%d",List.GetSelectedCount());
  bool first=true;
  TU1=TV1=TU2=TV2=TU3=TV3=TU4=TV4="";
  POSITION pos=List.GetFirstSelectedItemPosition();
  while (pos)
    {
    int p=List.GetNextSelectedItem(pos);
    int idx=List.GetItemData(p);
    FaceT face(obj,idx);
    SetDialogData(face,first);
    first=false;
    }
  UpdateData(FALSE);
  }

//--------------------------------------------------

void CFaceProp::OnSelchangeList() 
  {
  ForAllSelectedLoad();	
  }

//--------------------------------------------------

BOOL CFaceProp::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  
  List.InsertColumn(0, WString(IDS_FACEPROPH1), LVCFMT_RIGHT,0, 0);
  List.InsertColumn(1, WString(IDS_FACEPROPH2), LVCFMT_RIGHT,0, 1);
  List.InsertColumn(2, WString(IDS_FACEPROPH3), LVCFMT_RIGHT,0, 2);
  List.InsertColumn(3, WString(IDS_FACEPROPH4), LVCFMT_RIGHT,0, 3);
  List.InsertColumn(4, WString(IDS_FACEPROPH5), LVCFMT_LEFT,0, 4);
  List.InsertColumn(5, WString(IDS_FACEPROPH6), LVCFMT_LEFT,0, 5);
  List.InsertColumn(6, WString(IDS_FACEPROPH7), LVCFMT_RIGHT,0, 6);
  List.InsertColumn(7, "", LVCFMT_RIGHT,0, 7);
  ListView_SetExtendedListViewStyleEx(List,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
  
  LoadToList();	
  BBrowse.SetIcon(::LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_BROWSE)));
  BBrowse2.SetIcon(::LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_BROWSE)));
  BSetFromView.SetIcon(::LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_FROMBGR)));
  BSetFromList.SetIcon(::LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_FROMLIST)));
  BFromList2.SetIcon(::LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_FROMLIST)));
  BColor.SetIcon(::LoadIcon(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_COLOR)));

  initialized=true;
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

void CFaceProp::OnSelectall() 
  {
  int p=List.GetItemCount();
  for (int i=0;i<p;i++) List.SetItemState(i,LVIS_SELECTED ,LVIS_SELECTED );
  ForAllSelectedLoad();
  }

//--------------------------------------------------

void CFaceProp::OnFilter() 
  {
  FaceT face(obj),mask(obj);
  GetDialogData(face, mask);
  int cnt=List.GetItemCount();
  for (int i=0;i<cnt;i++)
    {
    int idx=List.GetItemData(i);
    FaceT fc(obj,idx);
    bool mark=((fc.GetFlags()&mask.GetFlags())^(face.GetFlags()&mask.GetFlags()))==0;
    for (int j=0;j<fc.N();j++)
      {
      if (mask.GetU(j)!=0.0f)
        mark&=(fabs(face.GetU(j)-fc.GetU(j))<0.005);
      if (mask.GetV(j)!=0.0f)
        mark&=(fabs(face.GetV(j)-fc.GetV(j))<0.005);
      }
    if (strcmp(mask.GetTexture(),"A")==0)
      mark&=fc.GetTexture()==face.GetTexture();
    if (strcmp(mask.GetMaterial(),"A")==0)
      mark&=fc.GetMaterial()==face.GetMaterial();
    List.SetItemState(i,mark==true?LVIS_SELECTED:0,LVIS_SELECTED);
    }
  ForAllSelectedLoad();
  }

//--------------------------------------------------

class CISetFaceProp:public CICommand
  {
  FaceT value;
  FaceT mask;
  int *idxlist;
  int index;
  public:
    CISetFaceProp(int cnt, FaceT& value, FaceT& mask);
    CISetFaceProp(istream& str);
    virtual ~CISetFaceProp()
      {delete [] idxlist;}
    void AddIdx(int idx)
      {idxlist[index++]=idx;}
    virtual int Execute(LODObject *obj);
    virtual void Save(ostream &str);
    virtual int Tag() const 
      {return CITAG_SETFACEPROP;}
  };

//--------------------------------------------------

CICommand *CISetFaceProp_Create(istream &str)
  {
  return new CISetFaceProp(str);
  }

//--------------------------------------------------

CISetFaceProp::CISetFaceProp(int cnt, FaceT& val,FaceT& mk):
mask(mk),value(val)
  {
  idxlist=new int[cnt];
  index=0;
  }

//--------------------------------------------------

CISetFaceProp::CISetFaceProp(istream &str)
  {
/*  for (int i=0;i<4;i++)
    {
    datard(str,value.vs[i].mapU);
    datard(str,value.vs[i].mapV);
    datard(str,mask.vs[i].mapU);
    datard(str,mask.vs[i].mapV);
    }
  datard(str,value.flags);
  datard(str,mask.flags); 
  char buff[2048];
  FLoadString(str,buff,sizeof(buff));mask.vTexture=buff;
  FLoadString(str,buff,sizeof(buff));mask.vMaterial=buff;
  FLoadString(str,buff,sizeof(buff));value.vTexture=buff;
  FLoadString(str,buff,sizeof(buff));value.vMaterial=buff;
  idxlist=NULL;*/
  }

//--------------------------------------------------

void CISetFaceProp::Save(ostream &str)
  {
/*  for (int i=0;i<4;i++)
    {
    datawr(str,value.vs[i].mapU);
    datawr(str,value.vs[i].mapV);
    datawr(str,mask.vs[i].mapU);
    datawr(str,mask.vs[i].mapV);
    }
  datawr(str,value.flags);
  datawr(str,mask.flags);  
  FSaveString(str,mask.vTexture);
  FSaveString(str,mask.vMaterial);
  FSaveString(str,value.vTexture);
  FSaveString(str,value.vMaterial);*/
  }

//--------------------------------------------------

int CISetFaceProp::Execute(LODObject *obj)
  {
  ObjectData *odata=obj->Active();
  value.SetObject(odata);
  mask.SetObject(odata);
  int cnt=odata->NFaces();
  int idx=0,sel=0;
  for (int i=0;i<cnt;i++)
    if (odata->FaceSelected(i))
      {
      if (idxlist==NULL || idxlist[idx]==sel)
        {
        FaceT ps(odata,i);
        for (int j=0;j<4;j++)
          {
          if (mask.GetU(j)!=0.0f) ps.SetU(j,value.GetU(j));
          if (mask.GetV(j)!=0.0f) ps.SetV(j,value.GetV(j));
          }
        if (strcmp(mask.GetTexture(),"A")==0) ps.SetTexture(value.GetTexture());
        if (strcmp(mask.GetMaterial(),"A")==0) ps.SetMaterial(value.GetMaterial());
        ps.SetFlags((ps.GetFlags() & ~mask.GetFlags()) | (value.GetFlags()& mask.GetFlags()));
        idx++;
        }
      sel++;
      }
  comint.sectchanged=true;
  return 0;
  }

//--------------------------------------------------

void CFaceProp::OnApply() 
  {
  Apply();
  frame->SendMessage(WM_COMMAND,ID_VIEW_REFRESH,0);
  LoadToList();
  }

//--------------------------------------------------

void CFaceProp::OnOK() 
  {
  Apply();
  CDialog::OnOK();
  }

//--------------------------------------------------

void CFaceProp::OnSetTexture() 
  {
  UpdateData();
  int id=GetFocus()->GetDlgCtrlID();
  TexName=GetTextureFrom(id,TexName);
  UpdateData(FALSE);
  }

//--------------------------------------------------

void CFaceProp::OnCreatesel() 
  {
  Selection *sel=new Selection(obj);
  POSITION pos=List.GetFirstSelectedItemPosition();
  while (pos) sel->FaceSelect(List.GetItemData(List.GetNextSelectedItem(pos)));
  comint.Begin(WString(IDS_MAKESELECTION));
  comint.Run(new CISelect(sel));
  EndDialog(IDOK);
  }

//--------------------------------------------------

void CFaceProp::OnBrowse2() 
  {
  UpdateData(TRUE);
  const char *pth=config->GetString(IDS_CFGPATHFORTEX);
  int pthlen=strlen(pth);  
  CString org="";
  if (MatName.GetLength()!=0 && (MatName.GetLength()<2 || MatName[1]!=':')) org=pth+MatName;
  else org=MatName;
  CFileDialogEx dlg(TRUE,WString(IDS_MATERIALEXT),org,OFN_FILEMUSTEXIST,WString(IDS_MATERIALFILTER));
  if (dlg.DoModal()==IDOK)
    {
    MatName=dlg.GetPathName();
    char *p=(char *)alloca(pthlen+1);
    strncpy(p,MatName,pthlen);
    p[pthlen]=0;
    if (_stricmp(pth,p)==0) MatName.Delete(0,pthlen);
    SetDlgItemText(IDC_MATERIAL,MatName);
    }
  
  }

//--------------------------------------------------

void CFaceProp::OnItemchangedListex(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
  if (!deftmr) deftmr=SetTimer(10,10,NULL);
  *pResult = 0;
  }

//--------------------------------------------------

void CFaceProp::OnTimer(UINT nIDEvent) 
  {
  if (nIDEvent==deftmr)
    {
    ForAllSelectedLoad();	
    KillTimer(deftmr);
    deftmr=0;
    }  	
  CDialog::OnTimer(nIDEvent);
  }

//--------------------------------------------------


void CFaceProp::Apply()
{
  FaceT face(obj),mask(obj);
  GetDialogData(face,mask)  ;	
  int cn=List.GetItemCount();
  CISetFaceProp *pp=new CISetFaceProp(List.GetItemCount(),face,mask);
  POSITION pos=List.GetFirstSelectedItemPosition();
  while (pos) pp->AddIdx(List.GetNextSelectedItem(pos));
  comint.Begin(WString(IDS_SETFACEPROP));
  comint.Run(pp);

}

void CFaceProp::OnBnClickedSetfromlist2()
  {
  UpdateData();
  int id=GetFocus()->GetDlgCtrlID();
  MatName=GetTextureFrom(IDC_SETFROMLIST,TexName);
  UpdateData(FALSE);
  }

class DlgTextureColor: public CColorDialog
{
  CString texType;
  CComboBox cb;
  CDialog extend;
public:

  DlgTextureColor(COLORREF rgbinit, CString texType):
      CColorDialog(rgbinit),texType(texType) {}
  virtual BOOL OnInitDialog()
  {
    BOOL res=CColorDialog::OnInitDialog();
    extend.Create(IDD_CHOOSETEXTYPE,this);
    CRect rc;
    CRect extrc;
    extend.GetWindowRect(&extrc);
    GetClientRect(&rc);
    extend.SetWindowPos(&CWnd::wndTop,rc.left,rc.bottom,0,0, SWP_NOSIZE);
    GetWindowRect(&rc);
    rc.bottom+=extrc.bottom-extrc.top;
    MoveWindow(&rc,TRUE);
    cb.SubclassDlgItem(IDC_TYPE,&extend);
    cb.SetWindowText(texType);
    return res;
  }

  virtual BOOL OnColorOK()
  {
    cb.GetWindowText(texType);
    return FALSE;
  }
  const CString& GetTextureType() const
  {
    return texType;
  }
};

void CFaceProp::OnSetColor()
{
  char mode[50];
  int szx;
  int szy;
  int chans;
  float fr=0,fg=0,fb=0,fa=1.0f;
  char texType[50];
  
  UpdateData();

  int cnt=sscanf(TexName,"#(%49[^,)],%d,%d,%d)color(%f,%f,%f,%f,%[^49,)])", 
      &mode,&szx,&szy,&chans,&fr,&fg,&fb, &fa,texType);
  if (cnt<9) strcpy(texType,"CO");



  DlgTextureColor cdlg(RGB(toInt(fr*255),toInt(fg*255),toInt(fb*255)),texType);
  cdlg.m_cc.Flags|=CC_FULLOPEN;
  if (cdlg.DoModal())
  {    
    COLORREF col=cdlg.GetColor();
    CString form;
    form.Format("#(argb,8,8,3)color(%g,%g,%g,1.0,%s)",GetRValue(col)/255.0f,
      GetGValue(col)/255.0f,GetBValue(col)/255.0f,cdlg.GetTextureType().GetString());
    TexName=form;
    UpdateData(FALSE);
  }  
}
void CFaceProp::MoveItem(int dx,int dy,CWnd &item)
{
    RECT btnRect;
    item.GetWindowRect(&btnRect);
    ScreenToClient(&btnRect);
    btnRect.left+=dx;
    btnRect.right+=dx;
    btnRect.top+=dy;
    btnRect.bottom+=dy;
    item.MoveWindow(&btnRect,0);
}
///////////////////////////////////////////////////////////////////////////////
void CFaceProp::SizeItem(int dx,int dy,CWnd &item)
{
    RECT btnRect;
    item.GetWindowRect(&btnRect);
    ScreenToClient(&btnRect);
    btnRect.right+=dx;
    btnRect.bottom+=dy;
    item.MoveWindow(&btnRect,0);
}
void CFaceProp::OnSize(UINT nType, int cx, int cy)
{
  CDialog::OnSize(nType,cx,cy);
  static int ocx;
  static int ocy;
  if (initialized && (List.GetSafeHwnd()!=NULL)) 
  {
    RECT oldwindowRect;
    GetWindowRect(&oldwindowRect);
    int dx=cx-ocx;
    int dy=cy-ocy;
    if ((ocx!=0)&&((dx!=0)||(dy!=0)))
    {
      HDWP hdwp=BeginDeferWindowPos(60);

      MoveWindowRel(List,0,0,dx,dy,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_APPLY),dx,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDCANCEL),dx,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDOK),dx,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_CREATESEL),dx,dy,0,0,hdwp);

      MoveWindowRel(*GetDlgItem(IDC_STATIC_LiGHTING),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_MAPPING),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_SELECTIONS),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_Z_BIAS),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_TEXTURES),0,dy,dx,0,hdwp);

      MoveWindowRel(*GetDlgItem(IDC_EDIT1),0,dy,dx,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_MATERIAL),0,dy,dx,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_USERVALUE),0,dy,dx,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_USER_VALUE),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_TU1),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_TU2),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_TU3),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_TU4),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_TV1),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_TV2),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_TV3),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_TV4),0,dy,0,0,hdwp);

      MoveWindowRel(*GetDlgItem(IDC_SELECTALL),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_FILTER),0,dy,0,0,hdwp);

      MoveWindowRel(*GetDlgItem(IDC_COLOR),dx,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_BROWSE),dx,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_SETFROMVIEW),dx,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_BROWSE),dx,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_SETFROMLIST),dx,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_SETFROMLIST2),dx,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_BROWSE2),dx,dy,0,0,hdwp);

      MoveWindowRel(*GetDlgItem(IDC_STATIC_TX),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_MT),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_ENABLE_TEXMERGE),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_BIAS_NORMAL),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_BIAS_LOW),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_BIAS_MIDDLE),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_BIAS_HIGH),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_ENABLE_SHADOW),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_LIGHTING_REVERSED),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_LIGHTING_FLAT),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_LIGHTING_POSITION),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_LIGHTING_BSIDES),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_LIGHTING_NORMAL),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_V1),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_V2),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_V3),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_V4),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_V),0,dy,0,0,hdwp);
      MoveWindowRel(*GetDlgItem(IDC_STATIC_U),0,dy,0,0,hdwp);

   
      EndDeferWindowPos(hdwp);
      List.Invalidate(FALSE);
    }
  }
  ocx=cx;
  ocy=cy;
}

void CFaceProp::OnSizing(UINT fwSide, LPRECT pRect)
{
  static int minx;
  static int miny;
  CRect oldwindowRect;
  GetWindowRect(&oldwindowRect);
  if (minx==0) 
  {
    minx=pRect->right-pRect->left;
  }
  if (miny==0) 
  {
    miny=pRect->bottom-pRect->top;
  }
  if (((fwSide==1)||(fwSide==7)||(fwSide==4))&&(pRect->right-pRect->left<minx)) 
  {
    pRect->left=pRect->right-minx;
    pRect->right=pRect->left+minx;
  }
  if (((fwSide==2)||(fwSide==8)||(fwSide==5))&&(pRect->right-pRect->left<minx)) 
  {
    pRect->right=pRect->left+minx;
    pRect->left=pRect->right-minx;
  }

  if (((fwSide==3)||(fwSide==5)||(fwSide==4))&&(pRect->bottom-pRect->top<miny)) 
  {
    pRect->top=pRect->bottom-miny;
    pRect->bottom=pRect->top+miny;
  }
  if (((fwSide==6)||(fwSide==8)||(fwSide==7))&&(pRect->bottom-pRect->top<miny)) 
  {
    pRect->bottom=pRect->top+miny;
    pRect->top=pRect->bottom-miny;
  }
  CDialog::OnSizing(fwSide, pRect);
}
