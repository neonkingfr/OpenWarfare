// LicenceAgrDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "LicenceAgrDlg.h"
#include <strstream>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLicenceAgrDlg dialog


CLicenceAgrDlg::CLicenceAgrDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CLicenceAgrDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CLicenceAgrDlg)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CLicenceAgrDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CLicenceAgrDlg)
  DDX_Control(pDX, IDOK, okbutt);
  DDX_Control(pDX, IDC_RICHTEXT, richtext);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CLicenceAgrDlg, CDialog)
  //{{AFX_MSG_MAP(CLicenceAgrDlg)
  ON_WM_TIMER()
    //}}AFX_MSG_MAP
    END_MESSAGE_MAP()
      
      /////////////////////////////////////////////////////////////////////////////
      // CLicenceAgrDlg message handlers
      
      static istream *instr;

DWORD CALLBACK CLicenceAgrDlg::EditStreamCallback(DWORD dwCookie, LPBYTE pbBuff, LONG cb, LONG *pcb)
  {
  instr->read((char *)pbBuff,cb);
  *pcb=instr->gcount();
  return 0;
  }

//--------------------------------------------------

void CLicenceAgrDlg::LoadLicenceToRichText(CRichEditCtrl &richtext) 
{
  HMODULE mod=AfxGetInstanceHandle();
#ifdef PUBLIC
  HRSRC rsrc=FindResource(mod,MAKEINTRESOURCE(IDR_LICENCE_PUBLIC),"LICENCE");
#else
  HRSRC rsrc=FindResource(mod,MAKEINTRESOURCE(IDR_LICENCE),"LICENCE");
#endif
  HGLOBAL glob=LoadResource(mod,rsrc);
  void *p=LockResource(glob);
  DWORD sz=SizeofResource(mod,rsrc);
  
  istrstream instream((char *)p,sz);
  instr=&instream;
  
  EDITSTREAM rs;
  rs.pfnCallback=EditStreamCallback;
  richtext.StreamIn(SF_RTF,rs);
}

BOOL CLicenceAgrDlg::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  
  LoadLicenceToRichText(richtext);

  readfull=false;
  SetTimer(2000,2000,NULL);
  
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

void CLicenceAgrDlg::OnTimer(UINT nIDEvent) 
  {
  SCROLLINFO nfo;
  richtext.GetScrollInfo(SB_VERT,&nfo,SIF_ALL);  
  if (nfo.nMax-nfo.nPage-100<(unsigned int)nfo.nPos) readfull=true;
  }

//--------------------------------------------------

void CLicenceAgrDlg::OnOK() 
  {
  if (!readfull)
    {
    AfxMessageBox(IDS_READFULLLICENCE,MB_OK);
    return;
    }
  CDialog::OnOK();
  }

//--------------------------------------------------

