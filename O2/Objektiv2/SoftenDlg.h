#if !defined(AFX_SOFTENDLG_H__DE89E0ED_9DCE_455C_A923_679678977AA8__INCLUDED_)
#define AFX_SOFTENDLG_H__DE89E0ED_9DCE_455C_A923_679678977AA8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SoftenDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSoftenDlg dialog

class CSoftenDlg : public CDialog
  {
  // Construction
  public:
    CSoftenDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CSoftenDlg)
    enum 
      { IDD = IDD_SOFTENANIM };
    BOOL	actframe;
    BOOL	interpolate;
    BOOL	loop;
    float	timerange;
    float	xval;
    float	yval;
    float	zval;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CSoftenDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CSoftenDlg)
    // NOTE: the ClassWizard will add member functions here
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SOFTENDLG_H__DE89E0ED_9DCE_455C_A923_679678977AA8__INCLUDED_)
