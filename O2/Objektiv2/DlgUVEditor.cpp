// DlgUVEditor.cpp : implementation file
//

#include "stdafx.h"
#include <colman.h>
#include "Objektiv2.h"
#include "DlgUVEditor.h"
#include "Objektiv2Doc.h"
#include "Objektiv2View.h"
#include "../ObjektivLib/LODObject.h"
#include "dlguveditor.h"
#include "../ObjektivLib/ObjToolsMatLib.h"
#include ".\dlguveditor.h"
#include "hotkeycfgdlg.h"
#include "NewMAT/NewMatInterface.hpp"
#include "DlgUVExport.h"

#undef IDC_UVSETLIST
#define TEXTURE_MENU_OFFSET 20000 
#define IDC_UVSETLIST AFX_IDW_PANE_FIRST

// DlgUVEditor dialog

#define HOTKEYSECTION "UVEditor"
#define HOTKEYITEM "Hotkeys"


IMPLEMENT_DYNAMIC(DlgUVEditor, CFrameWnd)
DlgUVEditor::DlgUVEditor(CWnd* pParent /*=NULL*/)
	: CFrameWnd(),_dlgTransform(this,this),
  _sizing(false),_texProp(this)
{
  _curDocument=0;
  _tex_bright=50;
  _tex_contrst=50;
  _autoselect=false;
  _hotKeyDlg=0;
  _matPlugin=0;
  _userAccel=0;
}


DlgUVEditor::~DlgUVEditor()
{
  if (_hotKeyDlg)
  {
    if (_hotKeyDlg->GetSafeHwnd()) _hotKeyDlg->DestroyWindow();
    delete _hotKeyDlg;
  }    
}


BEGIN_MESSAGE_MAP(DlgUVEditor, CFrameWnd)
  ON_MESSAGE(WM_EXITSIZEMOVE,OnExitSizeMove)
  ON_MESSAGE(WM_ENTERSIZEMOVE,OnEnterSizeMove)
ON_WM_SIZE()
ON_WM_INITMENUPOPUP()
ON_COMMAND(ID_UVSETS_DELETEACTIVE, OnUvsetsDeleteactive)
ON_UPDATE_COMMAND_UI(ID_UVSETS_DELETEACTIVE, OnUpdateUvsetsDeleteactive)
ON_UPDATE_COMMAND_UI(ID_UVSETS_CHANGEID, OnUpdateUvsetsDeleteactive)
ON_COMMAND(ID_UVSETS_ADD, OnUvsetsAdd)
ON_COMMAND(ID_UVSETS_CHANGEID, OnUvsetsChangeid)
ON_COMMAND(ID_UVEDITOR_EXIT, OnUveditorExit)
ON_COMMAND_RANGE(TEXTURE_MENU_OFFSET ,TEXTURE_MENU_OFFSET +10000,OnSelectTextureFromMenu)
ON_WM_DESTROY()
ON_NOTIFY(TCN_SELCHANGE, IDC_UVSETLIST, OnTcnSelchangeUvsetlist)
ON_COMMAND(ID_VIEW_ALPHACHECKER, OnViewAlphachecker)
ON_UPDATE_COMMAND_UI(ID_VIEW_ALPHACHECKER, OnUpdateViewAlphachecker)
ON_COMMAND(ID_EDIT_BREAK, OnEditBreak)
ON_COMMAND(ID_VIEW_TEXTURELIGHTNESS, OnViewTexturelightness)
ON_UPDATE_COMMAND_UI(ID_VIEW_TEXTURELIGHTNESS, OnUpdateViewTexturelightness)
ON_COMMAND_RANGE(ID_ZOOM_ORIGINAL,ID_ZOOM_FITSELECTIONTOWINDOW,OnZoomCmd)
ON_COMMAND(ID_ZOOM_ZOOMMODE, OnZoomZoommode)
ON_UPDATE_COMMAND_UI(ID_ZOOM_ZOOMMODE, OnUpdateZoomZoommode)
ON_COMMAND(ID_EDIT_SELECTINOBJECT, OnEditSelectinobject)
ON_COMMAND(ID_EDIT_GETSELECTIONFROMOBJECT, OnEditGetselectionfromobject)
ON_COMMAND(ID_EDIT_AUTOSELECTINOBJECT, OnEditAutoselectinobject)
ON_UPDATE_COMMAND_UI(ID_EDIT_AUTOSELECTINOBJECT, OnUpdateEditAutoselectinobject)
ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)
ON_COMMAND(ID_EDIT_SELECTIONTOUV, OnEditSelectiontouv)
ON_UPDATE_COMMAND_UI(ID_EDIT_SELECTIONTOUV, OnUpdateEditSelectiontouv)
ON_COMMAND(ID_EDIT_MERGE, OnEditMerge)
ON_COMMAND(ID_EDIT_SNAPTOPOINT, OnEditSnaptopoint)
ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
ON_COMMAND(ID_EDIT_REDO, OnEditRedo)
ON_UPDATE_COMMAND_UI(ID_EDIT_REDO, OnUpdateEditRedo)
ON_WM_PAINT()
ON_COMMAND(ID_EDIT_DELETE, OnEditDelete)
ON_UPDATE_COMMAND_UI(ID_EDIT_DELETE, OnUpdateEditDelete)
ON_COMMAND(ID_EDIT_UNWRAP, OnEditUnwrap)
ON_UPDATE_COMMAND_UI(ID_EDIT_UNWRAP, OnUpdateEditUnwrap)
ON_COMMAND(ID_EDIT_UNDEFORM, OnEditUndeform)
ON_UPDATE_COMMAND_UI(ID_EDIT_UNDEFORM, OnUpdateEditUndeform)
ON_MESSAGE(HKK_ACCELCHANGED,OnChangeAccelTable)
ON_COMMAND(ID_FILE_HOTKEYS,OnFileHotkeys)
ON_COMMAND(ID_UVEDITOR_NEXTSET, OnUveditorNextset)
ON_COMMAND(ID_UVEDITOR_PREVIOUSSET, OnUveditorPreviousset)
ON_COMMAND(ID_EDIT_SELECTALL, OnEditSelectall)
ON_COMMAND(ID_EDIT_CUT, OnEditCut)
ON_COMMAND(ID_ALIGN_ALIGNMENU, OnAlignAlignmenu)
ON_COMMAND(ID_FILTER_SHOWALLUVS, OnFilterShowalluvs)
ON_COMMAND(ID_FILTER_MAINTEXTUREMENU, OnFilterMaintexturemenu)
ON_COMMAND(ID_FILTER_MATERIALMENU, OnFilterMaterialmenu)
ON_COMMAND(ID_FILTER_MATERIALTEXTUREMENU, OnFilterMaterialtexturemenu)
ON_COMMAND(ID_ZOOM_ZOOMMENU, OnZoomZoommenu)
ON_COMMAND(ID_VIEW_BROWSETEXTURE, OnViewBrowsetexture)
ON_COMMAND(ID_FILTER_BROWSENEWTEXTURE, OnFilterBrowsenewtexture)
ON_COMMAND(ID_FILTER_BROWSENEWMATERIAL, OnFilterBrowsenewmaterial)
ON_COMMAND(ID_FILTER_CREATEMATERIAL, OnFilterCreatematerial)
ON_UPDATE_COMMAND_UI(ID_FILTER_CREATEMATERIAL, OnUpdateFilterCreatematerial)
ON_COMMAND(ID_CURRENTFILTER_NONE, OnCurrentfilterNone)
ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateEditCut)
ON_COMMAND_RANGE(ID_ALIGN_POINTS_LEFT,ID_ALIGN_OBJS_BOTTOM,OnAlignCmd)
ON_UPDATE_COMMAND_UI_RANGE(ID_ALIGN_POINTS_LEFT,ID_ALIGN_OBJS_BOTTOM,OnUpdateEditCopy)
ON_COMMAND(ID_VIEW_REFRESH, OnViewRefresh33769)
ON_COMMAND(ID_UVEDITOR_EXPORT, OnUveditorExport)
ON_COMMAND(ID_VIEW_BLACKONWHITE, OnViewBlackonwhite)
ON_COMMAND(ID_VIEW_BACKGROUNDLOD,OnViewBackgroundLOD)
ON_UPDATE_COMMAND_UI(ID_VIEW_BLACKONWHITE, OnUpdateViewBlackonwhite)
//ON_WM_ACTIVATEAPP()
ON_WM_ACTIVATE()
ON_UPDATE_COMMAND_UI(ID_VIEW_BACKGROUNDLOD, OnUpdateViewBackgroundlod)
ON_COMMAND(ID_EDIT_AUTOSNAP, OnEditAutosnap)
ON_UPDATE_COMMAND_UI(ID_EDIT_AUTOSNAP, OnUpdateEditAutosnap)
ON_COMMAND(ID_EDIT_MANUALTRANSFORM, OnEditTransform)
ON_COMMAND(ID_EDIT_HORIZONTALMIRROR, OnEditHorizontalmirror)
ON_COMMAND(ID_EDIT_VERTICALMIRROR, OnEditVerticalmirror)
ON_UPDATE_COMMAND_UI(ID_EDIT_HORIZONTALMIRROR, OnUpdateEditCopy)
ON_UPDATE_COMMAND_UI(ID_EDIT_VERTICALMIRROR, OnUpdateEditCopy)
END_MESSAGE_MAP()


LRESULT DlgUVEditor::OnExitSizeMove(WPARAM wParam, LPARAM lParam)
{
  CRect rc;
  GetClientRect(&rc);
  _sizing=false;
  OnSize(0,rc.right,rc.bottom);
  return 0;
}

LRESULT DlgUVEditor::OnEnterSizeMove(WPARAM wParam, LPARAM lParam)
{
  _sizing=true;
  return 0;
}
DlgUVEditor::EnumUVSets::EnumUVSets(DlgUVEditor *outer):outer(outer),active(active),index(0)
{
  outer->wUVSetTab.DeleteAllItems();
  active=outer->_curDocument->LodData->Active()->GetActiveUVSet();
  index=0;
}
bool DlgUVEditor::EnumUVSets::operator()(const ObjUVSet *uvset) const
{
  CString name;
  name.Format(IDS_UVEDITOR_UVSETNAME,uvset->GetIndex());
  outer->wUVSetTab.InsertItem(index,name);
  if (uvset==active)
    outer->wUVSetTab.SetCurSel(index);
  ++index;
  return false;
}


void DlgUVEditor::UpdateFromDocument(CObjektiv2Doc *document)
{
  _curDocument=document;
  const LODObject &LodObj=*document->LodData;
  if (m_hWnd==0) return;
  
  LodObj.Active()->SortUVSets();
  LodObj.Active()->EnumUVSets(EnumUVSets(this));  
  OnExitSizeMove(0,0);
  LoadUVS();
  UpdateLongStates();
  wUVView.Invalidate(FALSE);
}



void DlgUVEditor::OnSize(UINT nType, int cx, int cy)
{
  if (_sizing) return; 
  CFrameWnd::OnSize(nType, cx, cy);

//  wToolBar.MoveWindow(0,0,cx,cy);
  
  if (wUVSetTab.GetSafeHwnd()==0) return;
  CRect rc;
  wUVSetTab.GetWindowRect(&rc);
  ScreenToClient(&rc);
  wUVSetTab.MoveWindow(rc,FALSE);  
  wUVSetTab.AdjustRect(FALSE,&rc);
  MapWindowPoints(&wUVSetTab,&rc);
  wUVView.MoveWindow(&rc,FALSE);  
}


BOOL DlgUVEditor::Create(CWnd *owner)
{
  BOOL ok=CFrameWnd::Create(AfxRegisterWndClass(0,LoadCursor(0,IDC_ARROW),(HBRUSH)(COLOR_WINDOW+1),0),
    CString(MAKEINTRESOURCE(IDS_UVEDITOR_TITLE)),WS_OVERLAPPEDWINDOW,rectDefault,owner,MAKEINTRESOURCE(IDR_UVEDITORMENU));
   
  if (ok==FALSE) return FALSE;

  _backLod=config->GetBool(IDS_CFGSHOWBGINUV);

  wToolBar.Create(this,WS_CHILD|WS_VISIBLE|CBRS_TOP);
  wToolBar.LoadToolBar(IDR_UVTOOLBAR);
  wToolBar.SetBarStyle(wToolBar.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);  
  wToolBar.EnableDocking(CBRS_ALIGN_ANY);
  EnableDocking(CBRS_ALIGN_ANY);
  DockControlBar(&wToolBar,AFX_IDW_DOCKBAR_TOP);
  wUVSetTab.Create(WS_CHILD|WS_VISIBLE|TCS_MULTILINE|TCS_RAGGEDRIGHT|TCS_TABS,CRect(0,20,0,20),this,IDC_UVSETLIST);
  wUVView.Create(&wUVSetTab,this,0,0,WS_EX_STATICEDGE);

  CRect rc;
  rc.left=theApp.GetProfileInt("UVEditor","Window.left",0);
  rc.top=theApp.GetProfileInt("UVEditor","Window.top",0);
  rc.right=theApp.GetProfileInt("UVEditor","Window.right",0);
  rc.bottom=theApp.GetProfileInt("UVEditor","Window.bottom",-1);
  if (rc.IsRectEmpty())
  {
    rc.right=500;
    rc.bottom=500;    
    MoveWindow(&rc);
    CenterWindow();
  }
  else
    MoveWindow(&rc);

  _checker=theApp.GetProfileInt("UVEditor","Checker",0)!=0;
  _titlesize=0;

  HACCEL haccel=LoadAccelerators(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDR_UVEDITOR));
  m_hAccelTable=haccel;
  LoadHotkeys();

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}


static void FillMenu(CMenu *menu, int fromPos, const AutoArray<UVEditorMenuItem> &_menuItems, UVEditorMenuItem::ItemType filter)
{
  for (int i=0;i<_menuItems.Size();i++) if (_menuItems[i].type==filter)
  {
    menu->InsertMenu(fromPos++,MF_STRING|MF_BYPOSITION,i+TEXTURE_MENU_OFFSET ,Pathname::GetNameFromPath(_menuItems[i].name));
  }
}

void DlgUVEditor::OnInitMenuPopup(CMenu* pMenu, UINT nIndex, BOOL bSysMenu)
{
  if (pMenu->GetMenuItemID(0)==ID_FILTER_SHOWALLUVS)
  {
    ExploreObject();

    CMenu *out;
    out=pMenu->GetSubMenu(2);
    while (out->GetMenuItemCount()) out->DeleteMenu(0,MF_BYPOSITION);
    FillMenu(out,0,_menuItems,UVEditorMenuItem::texFilter);

    out=pMenu->GetSubMenu(3);
    while (out->GetMenuItemCount()) out->DeleteMenu(0,MF_BYPOSITION);
    FillMenu(out,0,_menuItems,UVEditorMenuItem::matFilter);
    
    out=pMenu->GetSubMenu(4);
    while (out->GetMenuItemCount()) out->DeleteMenu(0,MF_BYPOSITION);
    FillMenu(out,0,_menuItems,UVEditorMenuItem::texmatFilter);    
  }
  if (pMenu->GetMenuItemID(1)==ID_VIEW_TEXTURELIGHTNESS)
  {
    ExploreObject();
    MENUITEMINFO nfo;
    nfo.cbSize=sizeof(nfo);
    nfo.fMask=MIIM_FTYPE;    
    pMenu->GetMenuItemInfo(5,&nfo,TRUE);
    while (nfo.fType!=MFT_SEPARATOR) 
    {
      pMenu->DeleteMenu(4,MF_BYPOSITION);
      if (pMenu->GetMenuItemInfo(4,&nfo,TRUE) == FALSE) break;
    }
    FillMenu(pMenu,4,_menuItems,UVEditorMenuItem::texShow);    
  }
  if (pMenu->GetMenuItemID(pMenu->GetMenuItemCount()-1)==ID_CURRENTFILTER_NONE)
  {
    while (pMenu->GetMenuItemCount()>2) pMenu->DeleteMenu(0,MF_BYPOSITION);
    int pos=0;
    for (int i=0;i<_filterUv.Size();i++)
    {
      for (int j=0;j<_menuItems.Size();j++)
        if (_menuItems[j].type!=UVEditorMenuItem::texShow && RStringI(_filterUv[i])==RStringI(_menuItems[j].name)) 
        {
          pMenu->InsertMenu(pos++,MF_STRING|MF_BYPOSITION,j+TEXTURE_MENU_OFFSET,Pathname::GetNameFromPath(_menuItems[j].name));
          break;
        }
    }
  }
  CFrameWnd::OnInitMenuPopup(pMenu,nIndex,bSysMenu);
}

void DlgUVEditor::OnUvsetsDeleteactive()
{
  _curDocument->OnUvsetsDeleteactive();
}

void DlgUVEditor::OnUpdateUvsetsDeleteactive(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_curDocument->LodData->Active()->GetActiveUVSet()->GetIndex()!=0);
}

void DlgUVEditor::OnUvsetsAdd()
{
  _curDocument->OnUvsetsAdd();
  
}

void DlgUVEditor::OnUvsetsChangeid()
{
  _curDocument->OnUvsetsChangeid();
}

void DlgUVEditor::OnUveditorExit()
{
  DestroyWindow();
}

void DlgUVEditor::OnDestroy()
{
  CRect rc;
  GetWindowRect(&rc);
  theApp.WriteProfileInt("UVEditor","Window.left",rc.left);
  theApp.WriteProfileInt("UVEditor","Window.top",rc.top);
  theApp.WriteProfileInt("UVEditor","Window.right",rc.right);
  theApp.WriteProfileInt("UVEditor","Window.bottom",rc.bottom);
  theApp.WriteProfileInt("UVEditor","Checker",_checker?0:1);
  __super::OnDestroy();
  
  this->~DlgUVEditor();
  new(this) DlgUVEditor;
}

void DlgUVEditor::OnTcnSelchangeUvsetlist(NMHDR *pNMHDR, LRESULT *pResult)
{
  int index=wUVSetTab.GetCurSel();
  _curDocument->OnUvSetActivate(index+ID_UVSETS_ACTIVATE0);
  *pResult = 0;
}


void DlgUVEditor::ExploreObject()
{
  const ObjectData &obj=*_curDocument->LodData->Active();
  const ObjToolMatLib &matLib=obj.GetTool<const ObjToolMatLib>();
  BTree<ObjMatLibItem> resources;
  BTreeIterator<ObjMatLibItem> iter(resources);
  ObjMatLibItem *item;
  _menuItems.Clear();
 
  matLib.ReadMatLib(resources,matLib.ReadTextures);
  iter.Reset();
  while ((item=iter.Next())!=0) if (RString(item->name)!=RString("") &&  item->name[0]!='#')
  {
    _menuItems.Append(UVEditorMenuItem(UVEditorMenuItem::texFilter,item->name));
    _menuItems.Append(UVEditorMenuItem(UVEditorMenuItem::texShow,item->name));
  }

  int matExplore=_menuItems.Size();
  resources.Clear();
  matLib.ReadMatLib(resources,matLib.ReadMaterials);
  iter.Reset();
  while ((item=iter.Next())!=0)  if (RString(item->name)!=RString(""))
  {
    _menuItems.Append(UVEditorMenuItem(UVEditorMenuItem::matFilter,item->name));
  }
  

  BTree<RStringI> mattex;
  for (int i=matExplore;i<_menuItems.Size();i++)
  {
    matLib.ExploreMaterial(RString(config->GetString(IDS_CFGPATHFORTEX),_menuItems[i].name.Data()),mattex);
  }
  BTreeIterator<RStringI> mattex_iter(mattex);
  RString *mattexitem;
  while ((mattexitem=mattex_iter.Next())!=0)  
  if (*mattexitem!=RString("") &&   (*mattexitem)[0]!='#')
  {
    _menuItems.Append(UVEditorMenuItem(UVEditorMenuItem::texmatFilter,*mattexitem));
    _menuItems.Append(UVEditorMenuItem(UVEditorMenuItem::texShow,*mattexitem));
  }
}

afx_msg void DlgUVEditor ::OnSelectTextureFromMenu(UINT cmd)
{
  cmd-=TEXTURE_MENU_OFFSET;
  UVEditorMenuItem &selected=_menuItems[cmd];

  if (selected.type==selected.texShow || selected.type==selected.texFilter ||
    selected.type==selected.texmatFilter)
  {
    LoadTexture(RString(config->GetString(IDS_CFGPATHFORTEX),selected.name));
  }
  if (selected.type!=selected.texShow)
  {
    if (selected.type==selected.texFilter || selected.type==selected.matFilter)
    {
      if (GetKeyState(VK_CONTROL)>=0) _filterUv.Clear();
      _filterUv.Append()=selected.name;
    }
    else if (selected.type==selected.texmatFilter)
    {      
      if (GetKeyState(VK_CONTROL)>=0) _filterUv.Clear();
      for (int i=0;i<_menuItems.Size();i++) if (_menuItems[i].type==selected.matFilter)
      {        
        BTree<RStringI> zz;
        ObjToolMatLib::ExploreMaterial(RString(config->GetString(IDS_CFGPATHFORTEX),_menuItems[i].name),zz);
        if (zz.Find(selected.name))
          _filterUv.Append()=_menuItems[i].name;
      }
    }
    LoadUVS();
    wUVView.FitUVSToWindow(false);
  }
  UpdateTitle();
}

static void ProcessTextureLightness(unsigned char *data, int size, float brightness, float contrast)
{
  if (brightness<0.5f) 
  {  
    brightness=brightness*2.0f;
    while (size--)
    {
      float num=*data*(1.0f/255.0f)*brightness;
      num=(num-0.5f)*contrast+0.5f;
      if (num>1.0f) num=1.0f;
      if (num<0) num=0;
      *data=(unsigned char)toInt(num*255.0f);
      data++;
    }
  }
  else
  {
    brightness=(1.0f-brightness)*2.0f;
    while (size--)
    {
      float num=1.0f-(1.0-*data*(1.0f/255.0f))*brightness;
      num=(num-0.5f)*contrast+0.5f;
      if (num>1.0f) num=1.0f;
      if (num<0) num=0;
      *data=(unsigned char)toInt(num*255.0f);
      data++;
    }
  }
}

void DlgUVEditor::LoadTexture(const char *texture)
{
  CWaitCursor wt;
  TUNIPICTURE *pic=cmLoadUni(const_cast<char *>(texture));
  if (pic)
  {  
    if (_tex_bright!=50 || _tex_contrst!=50)
    {
      float br=_tex_bright*1.0f/100.0f;
      float cr=_tex_contrst/50.0f;
      cr*=cr*cr*cr;
      int sz;
      sz=cmGetYSize(pic)*cmGetXlen(pic);
      ProcessTextureLightness((unsigned char *)cmGetData(pic),sz,br,cr);
    }
    BITMAPINFO *bmp=(BITMAPINFO *)cmUni2BitMap(pic,_checker,4096,4096);
    if (bmp)
    {
      wUVView.LoadTexture(bmp,reinterpret_cast<const char *>(bmp)+(_checker?sizeof(BITMAPINFOHEADER):sizeof(BITMAPINFOHEADER)));        
      free(bmp);
    }
    else
      AfxMessageBox(IDS_UNABLETOLOADBITMAP);
    free(pic);
  }
  else
    AfxMessageBox(IDS_UNABLETOLOADBITMAP);
 _lastTexture=texture;
}


void DlgUVEditor::LoadUVS()
{
  CColorizeDlg &colDlg=_curDocument->coldlg;
  AutoArray<UVEditorViewObject> objs;
  const ObjectData &curObj=*(_curDocument->LodData->Active());
  const Selection *hidden=curObj.GetHidden();
  for (int i=0;i<curObj.NFaces();i++) if (!hidden->FaceSelected(i))
  {
    FaceT fc(curObj,i);
    bool choose=_filterUv.Size()==0;    
    for (int i=0;!choose && i<_filterUv.Size();i++)
    {
      if (_filterUv[i]=="")       
        choose=curObj.FaceSelected(i);
      else 
        if (RStringI(fc.GetTexture())==RStringI(_filterUv[i]) ||
            RStringI(fc.GetMaterial())==RStringI(_filterUv[i]))
            choose=true;
    }
    if (choose)
    {
      UVEditorViewObject newobj(fc,(fc.GetFlags() & FACE_COLORIZE_MASK)!=0?
        (wUVView.IsWhiteMode()?colDlg.GetColorizeLineColor(fc):colDlg.GetColorizeFillColor(fc)):0);
      if (!newobj.IsUnmapped()) objs.Append(newobj);
    }
  }
  wUVView.LoadUVs(objs);

  objs.Clear();
  if (_backLod) 
  {
    const ObjectData *bgrn=_curDocument->GetBgrnLod();
    if (bgrn!=0 && bgrn!=&curObj)
    {
      objs.Clear();
      const Selection *hidden=bgrn->GetHidden();
      for (int i=0;i<bgrn->NFaces();i++) if (!hidden->FaceSelected(i))
      {
        FaceT fc(bgrn,i);
        bool choose=_filterUv.Size()==0;    
        for (int i=0;!choose && i<_filterUv.Size();i++)
        {
          if (_filterUv[i]=="")       
            choose=curObj.FaceSelected(i);
          else 
            if (RStringI(fc.GetTexture())==RStringI(_filterUv[i]) ||
              RStringI(fc.GetMaterial())==RStringI(_filterUv[i]))
              choose=true;
        }
        if (choose)
        {
          UVEditorViewObject newobj(fc,0);
          if (!newobj.IsUnmapped()) objs.Append(newobj);
        }
      }
    }
  }
  wUVView.LoadBgrnUVs(objs);
}
void DlgUVEditor::OnViewAlphachecker()
{
  _checker=!_checker;
  ReloadLastTexture();
}

void DlgUVEditor::OnUpdateViewAlphachecker(CCmdUI *pCmdUI)
{
  pCmdUI->SetCheck(_checker);
}

void DlgUVEditor::OnEditBreak()
{
  wUVView.BreakSelection();
}

void DlgUVEditor::SelectionChanged(const Array<UVEditorViewObject> &newSelection)
{
  if (_autoselect) OnEditSelectinobject();
  UpdateLongStates();
}
void DlgUVEditor::TransformedSelection(const Array<UVEditorViewObject> &newSelection)
{
  comint.Begin(WString(IDS_CHANGEMAPUVSET));
  comint.Run(new CIChangeUVMap(newSelection,0,false));
}
void DlgUVEditor::MovedPoints(const Array<UVEditorViewObject> &data, const Array<int> movedPts)
{
  int lastIdx=-1;
  AutoArray<UVEditorViewObject, MemAllocLocal<UVEditorViewObject,10> >res;
  for (int i=0;i<movedPts.Size();i++)
  {
    int idx=movedPts[i]/MAX_DATA_POLY;
    if (idx!=lastIdx)
    {
      lastIdx=idx;
      res.Append(data[idx]);
    }
  }
  comint.Begin(WString(IDS_CHANGEMAPUVSET));
  comint.Run(new CIChangeUVMap(res,0,false));
}

void DlgUVEditor::ReloadLastTexture()
{
  if (_lastTexture=="") wUVView.LoadDefaultTexture();
  else LoadTexture(_lastTexture);
}
void DlgUVEditor::OnViewTexturelightness()
{
  if (_texProp.GetSafeHwnd())
    _texProp.DestroyWindow();
  else
    _texProp.Create(_texProp.IDD,this);
}

void DlgUVEditor::OnUpdateViewTexturelightness(CCmdUI *pCmdUI)
{
  pCmdUI->SetCheck(_texProp.GetSafeHwnd()!=0);
}

void DlgUVEditor::OnZoomCmd(UINT cmd)
{
  float zoomFactor;
  switch (cmd)
  {
  case ID_ZOOM_ORIGINAL: zoomFactor=1.0f;break;
  case ID_ZOOM_75: zoomFactor=.75f;break;
  case ID_ZOOM_50: zoomFactor=.50f;break;
  case ID_ZOOM_25: zoomFactor=.25f;break;
  case ID_ZOOM_10: zoomFactor=.10f;break;
  case ID_ZOOM_125: zoomFactor=1.25f;break;
  case ID_ZOOM_150: zoomFactor=1.50f;break;
  case ID_ZOOM_200: zoomFactor=2.0f;break;
  case ID_ZOOM_300: zoomFactor=3.0f;break;
  case ID_ZOOM_500: zoomFactor=5.0f;break;
  case ID_ZOOM_1000: zoomFactor=10.0f;break;
  case ID_ZOOM_5: zoomFactor=0.05f;break;
  case ID_ZOOM_1: zoomFactor=0.01f;break;
  case ID_ZOOM_FITWINDOW: wUVView.FitTextureToWindow();return;
  case ID_ZOOM_FITWINDOWUV: wUVView.FitUVSToWindow(false);return;
  case ID_ZOOM_FITSELECTIONTOWINDOW: wUVView.FitUVSToWindow(true);return;
  default: return;

  }
  CRect rc;
  wUVView.GetClientRect(&rc);
  wUVView.SetZoom(zoomFactor,rc.CenterPoint());
}
void DlgUVEditor::OnZoomZoommode()
{
  wUVView.SetZoomMode(!wUVView.IsZoomMode());
  CheckDlgButton(ID_ZOOM_ZOOMMODE,wUVView.IsZoomMode()?1:0);
}

void DlgUVEditor::OnUpdateZoomZoommode(CCmdUI *pCmdUI)
{
  pCmdUI->SetCheck(wUVView.IsZoomMode());
}

void DlgUVEditor::OnEditSelectinobject()
{
  const AutoArray<UVEditorViewObject> &uv=wUVView.GetUVObjects();
  Selection *newsel=new Selection(_curDocument->LodData->Active());
  for (int i=0;i<uv.Size();i++)
  {
    const UVEditorViewObject &obj=uv[i];
    if (obj.selectedVertices)
    {
      FaceT fc(newsel->GetOwner(),obj.faceIndex);
      for (int j=0;j<obj.n;j++) if (obj.IsSelectedVertex(j))
      {
        newsel->PointSelect(fc.GetPoint(j));
      }
      if (obj.IsAllVerticesSelected())
      {
        newsel->FaceSelect(fc.GetFaceIndex());
      }
    }
  }
  comint.Begin(WString(IDS_MAKESELECTION));
  comint.Run(new CISelect(newsel));
  _curDocument->UpdateAllViews(0);
}

void DlgUVEditor::OnEditGetselectionfromobject()
{
  const ObjectData &objdata=*_curDocument->LodData->Active();
  AutoArray<UVEditorViewObject> uv=wUVView.GetUVObjects();
  for (int i=0;i<uv.Size();i++)
  { 
    UVEditorViewObject &uvobj=uv[i];
    FaceT fc(objdata,uvobj.faceIndex);
    if (objdata.FaceSelected(uvobj.faceIndex))
      uvobj.SelectAllVertices();
    else
    {
      uvobj.selectedVertices=0;
      for (int j=0;j<fc.N();j++) if (objdata.PointSelected(fc.GetPoint(j)))
        uvobj.SelectVertex(j,true);
    }
  }
  wUVView.LoadUVs(uv,false); 
  UpdateLongStates();
}

void DlgUVEditor::OnEditAutoselectinobject()
{
  _autoselect=!_autoselect;  
  CheckDlgButton(ID_EDIT_AUTOSELECTINOBJECT,_autoselect?1:0);
  if (_autoselect) OnEditSelectinobject();
}



void DlgUVEditor::OnUpdateEditAutoselectinobject(CCmdUI *pCmdUI)
{
  pCmdUI->SetCheck(_autoselect);
}


static int GetUVClipboardFormat()
{
  static int clpFormat=-1;
  if (clpFormat==-1)
    clpFormat=RegisterClipboardFormat("Objektiv2UVEditor_UVObjects");
  return clpFormat;
}

void DlgUVEditor::OnEditCopy()
{
  if (OpenClipboard()==FALSE)
    {AfxMessageBox("OpenClipboard failed");return;}
  if (EmptyClipboard()==FALSE)
    {AfxMessageBox("EmptyClipboard failed");CloseClipboard();return;}
  const AutoArray<UVEditorViewObject> &uv=wUVView.GetUVObjects();
  AutoArray<UVEditorViewObject> selected;
  for (int i=0;i<uv.Size();i++) if (uv[i].selectedVertices) selected.Append(uv[i]);
  int size=selected.Size()*sizeof(UVEditorViewObject)+sizeof(long);
  HANDLE glob=GlobalAlloc(GMEM_MOVEABLE,size);
  long *ptr=(long *)GlobalLock(glob);
  ptr[0]=selected.Size();
  memcpy(ptr+1,selected.Data(),size-4);
  GlobalUnlock(glob);
  if (SetClipboardData(GetUVClipboardFormat(),glob)==0)
    AfxMessageBox("SetClipboardData failed");
  CloseClipboard();
}

void DlgUVEditor::OnEditPaste()
{
  ObjectData &curobj=*(_curDocument->LodData->Active());
  if (OpenClipboard()==TRUE)
  {
    HANDLE glob=GetClipboardData(GetUVClipboardFormat());
    if (glob)
    {
      long *data=(long *)GlobalLock(glob);
      int err=GetLastError();
      Array<UVEditorViewObject> uvarr((UVEditorViewObject *)(data+1),*data);
      AutoArray<UVEditorViewObject> copy;
      copy.Reserve(uvarr.Size(),uvarr.Size());
      for (int i=0;i<uvarr.Size();i++) if (uvarr[i].faceIndex<curobj.NFaces())
      {
        copy.Append()=uvarr[i];
      }
      const char *resource=0;
      if (_filterUv.Size()>0 && _filterUv[0]!=RString(""))
        resource=_filterUv[0];
      comint.Begin(WString(IDS_CHANGEMAPUVSET));
      comint.Run(new CIChangeUVMap(copy,resource,_stricmp(Pathname::GetExtensionFromPath(resource),".rvmat")==0));
      wUVView.LoadUVs(copy,false);
      LoadUVS();
      GlobalUnlock(glob);
      _curDocument->UpdateAllViews(0);
    }
    CloseClipboard();
    UpdateLongStates();
  }
}

void DlgUVEditor::OnUpdateEditPaste(CCmdUI *pCmdUI)
{
  bool canpaste=false;
  if (OpenClipboard()==TRUE)
  {  
    int p=0;
    do {
      p=EnumClipboardFormats(p);
      if (p==GetUVClipboardFormat()) {canpaste=true;break;}
    } while(p!=0);
    CloseClipboard();
  }
  pCmdUI->Enable(canpaste);
}


void DlgUVEditor::OnEditSelectiontouv()
{
  const char *resource=0;
  if (_filterUv.Size()>0 && _filterUv[0]!=RString(""))
    resource=_filterUv[0];
  const ObjectData &objdata=*(_curDocument->LodData->Active());
  CObjektiv2View *view=CObjektiv2View::GetCurrentView();
  LPHMATRIX hmx=view->camera.GetTransform();
  CSize txtsize=wUVView.GetCurTextureDimensions();
  
  AutoArray<UVEditorViewObject> uvobjs;
  for (int i=0;i<objdata.NFaces();i++) if (objdata.FaceSelected(i))
  {
    FaceT fc(objdata,i);
    UVEditorViewObject uvobj;
    uvobj.n=fc.N();
    for (int j=0;j<uvobj.n;j++)
    {
      const PosT &ps=fc.GetPointVector<PosT>(j);
      HVECTOR vx;
      TransformVector(hmx,mxVector3(ps[0],ps[1],ps[2]),vx);
      CorrectVectorM(vx);
      uvobj.uv[j].u=vx[0];
      uvobj.uv[j].v=vx[1];
    }
    uvobj.faceIndex=i;
    uvobj.SelectAllVertices();
    uvobjs.Append(uvobj);
  }
  wUVView.TransformXYToUV(uvobjs);
  comint.Begin(WString(IDS_CHANGEMAPUVSET));
  comint.Run(new CIChangeUVMap(uvobjs,resource,_stricmp(Pathname::GetExtensionFromPath(resource),".rvmat")==0));
  LoadUVS();
  _curDocument->UpdateAllViews(0);
  OnEditGetselectionfromobject();
  OnEditBreak();
}

void DlgUVEditor::OnUpdateEditSelectiontouv(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(!_curDocument->LodData->Active()->GetSelection()->IsEmpty());
}

void DlgUVEditor::OnEditMerge()
{
  AutoArray<UVEditorViewObject> modfobjs;
  const Array<UVEditorViewObject> &objs=wUVView.GetUVObjects();
  float x=0, y=0;
  int cnt=0;
  for (int i=0;i<objs.Size();i++) if (objs[i].selectedVertices)
  {
    const UVEditorViewObject &obj=objs[i];
    modfobjs.Append(obj);
    for (int j=0;j<obj.n;j++) if (obj.IsSelectedVertex(j))
    {
      x+=obj.uv[j].u;
      y+=obj.uv[j].v;
      cnt++;
    }
  }
  if (cnt)
  {  
    x/=cnt;
    y/=cnt;
    for (int i=0;i<modfobjs.Size();i++)
    {
      UVEditorViewObject &obj=modfobjs[i];
      for (int j=0;j<obj.n;j++) if (obj.IsSelectedVertex(j))
      {
        obj.uv[j].u=x;
        obj.uv[j].v=y;
      }
    }
    comint.Begin(WString(IDS_CHANGEMAPUVSET));
    comint.Run(new CIChangeUVMap(modfobjs,0,0));
    LoadUVS();
    _curDocument->UpdateAllViews(0);
  }
}

void DlgUVEditor::OnEditSnaptopoint()
{
  CWaitCursor wt;
  wUVView.Snap();
}

void DlgUVEditor::OnEditUndo()
{
  _curDocument->OnEditUndo();
}

void DlgUVEditor::OnUpdateEditUndo(CCmdUI *pCmdUI)
{
  _curDocument->OnUpdateEditUndo(pCmdUI);
}

void DlgUVEditor::OnEditRedo()
{
  _curDocument->OnEditRedo();
}

void DlgUVEditor::OnUpdateEditRedo(CCmdUI *pCmdUI)
{
  _curDocument->OnUpdateEditRedo(pCmdUI);
}

void DlgUVEditor::PostNcDestroy()
{
}

void DlgUVEditor::OnPaint()
{
  CPaintDC dc(this); 
}

BOOL DlgUVEditor::PreCreateWindow(CREATESTRUCT& cs)
{
  BOOL res=__super::PreCreateWindow(cs);
  if (res)
  {
    cs.dwExStyle&=~WS_EX_CLIENTEDGE;
  }
  return res;
}

void DlgUVEditor::OnEditDelete()
{
  const Array<UVEditorViewObject> &objs=wUVView.GetUVObjects();
  AutoArray<UVEditorViewObject,MemAllocLocal<UVEditorViewObject,100> >deleted;
  UVEditorViewObject del;
  for (int i=0;i<MAX_DATA_POLY;i++)
    del.uv[i].u=del.uv[i].v=0;
  for (int i=0;i<objs.Size();i++) if (objs[i].IsAllVerticesSelected())
  {
    del.n=objs[i].n;
    del.faceIndex=objs[i].faceIndex;
    del.SelectAllVertices();
    deleted.Append(del);
  }
  comint.Begin(WString(IDS_UVSETCHANGEID));
  comint.Run(new CIChangeUVMap(deleted,0,false));
  _curDocument->UpdateAllViews(0);
  LoadUVS();
  OnEditBreak();
}

void DlgUVEditor::OnUpdateEditDelete(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_anybreakableselect);
}


bool DlgUVEditor::AnySelection() const
{
  const Array<UVEditorViewObject> &objs=wUVView.GetUVObjects();
  for (int i=0;i<objs.Size();i++)
    if (objs[i].selectedVertices) return true;    
  return false;
}

bool DlgUVEditor::AnyBreakableSelection() const
{
  const Array<UVEditorViewObject> &objs=wUVView.GetUVObjects();
  for (int i=0;i<objs.Size();i++)
    if (objs[i].IsAllVerticesSelected()) return true;    
  return false;
}


void DlgUVEditor::OnEditUnwrap()
{
  wUVView.CalculateUnwrap(*_curDocument->LodData->Active(),0);
  LoadUVS();
}

void DlgUVEditor::OnUpdateEditUnwrap(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_anybreakableselect);
}

void DlgUVEditor::OnEditUndeform()
{
  ObjectData &odata=*_curDocument->LodData->Active();
  const Array<UVEditorViewObject> &objs=wUVView.GetUVObjects();
  AutoArray<UVEditorViewObject> undeformed;
  for (int i=0;i<objs.Size();i++) if (objs[i].IsAllVerticesSelected())
  {
    UVEditorViewObject &newobj=undeformed.Append();
    newobj=objs[i];
    FaceT fc(odata,newobj.faceIndex);

    float baseSizeU=newobj.uv[1].u-newobj.uv[0].u;
    float baseSizeV=newobj.uv[1].v-newobj.uv[0].v;

    VecT ps1=fc.GetPointVector<VecT>(1);
    VecT ps1a=fc.GetPointVector<VecT>(0)-ps1;
    VecT ps1b=fc.GetPointVector<VecT>(2)-ps1;
    float angle1=-ps1a.CosAngle(ps1b);
    float angle2=sqrt(1-angle1*angle1);
    float ratio1=sqrt(ps1b.SquareSize()/ps1a.SquareSize());

    newobj.uv[2].u=baseSizeU*angle1*ratio1+baseSizeV*angle2*ratio1+newobj.uv[1].u;
    newobj.uv[2].v=baseSizeV*angle1*ratio1-baseSizeU*angle2*ratio1+newobj.uv[1].v;
    if (newobj.n==4)
    {
      VecT ps2=fc.GetPointVector<VecT>(0);
      VecT ps2a=fc.GetPointVector<VecT>(1)-ps2;
      VecT ps2b=fc.GetPointVector<VecT>(3)-ps2;
      float angle1=-ps1a.CosAngle(ps1b);
      float angle2=sqrt(1-angle1*angle1);
      float ratio1=sqrt(ps1b.SquareSize()/ps1a.SquareSize());

      newobj.uv[3].u=baseSizeU*angle1*ratio1+baseSizeV*angle2*ratio1+newobj.uv[0].u;
      newobj.uv[3].v=baseSizeV*angle1*ratio1-baseSizeU*angle2*ratio1+newobj.uv[0].v; 
    }
  }
  TransformedSelection(undeformed);
  LoadUVS();
}

void DlgUVEditor::OnUpdateEditUndeform(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_anybreakableselect);
}

BOOL DlgUVEditor::PreTranslateMessage(MSG* pMsg)
{
  return __super::PreTranslateMessage(pMsg);
}

HACCEL DlgUVEditor::GetDefaultAccelerator()
{
  if (_userAccel) return _userAccel;
  else return CFrameWnd::GetDefaultAccelerator();
}

BOOL DlgUVEditor::OnCommand(WPARAM wParam, LPARAM lParam)
{
  if (_hotKeyDlg->GetSafeHwnd())
  {
    _hotKeyDlg->OnCommandIncomed(this,LOWORD(wParam));
    return TRUE;
  }
  return CFrameWnd::OnCommand(wParam,lParam);
}

LRESULT DlgUVEditor::OnChangeAccelTable(WPARAM wParam, LPARAM lParam)
{
  if (_userAccel!=NULL)
  {
    ::DestroyAcceleratorTable(_userAccel);
  }
  if (lParam!=0)
  {
    _userAccel=::CreateAcceleratorTable((LPACCEL)lParam,wParam);
  }
  else
    _userAccel=NULL;
  CHotKeyCfgDlg::RecursiveReplaceMenuHotkeys(GetDefaultAccelerator(),*GetMenu());
  SaveHotkeys();
  return 0;
}

void DlgUVEditor::SaveHotkeys()
{
  if (_userAccel==NULL)
  {
    theApp.WriteProfileString(HOTKEYSECTION,HOTKEYITEM,NULL);
  }
  else
  {
    int accls=CopyAcceleratorTable(_userAccel,NULL,0);
    ACCEL *accel=(ACCEL *)alloca(sizeof(ACCEL)*(accls+1));
    CopyAcceleratorTable(_userAccel,accel,accls);
    theApp.WriteProfileBinary(HOTKEYSECTION,HOTKEYITEM,(LPBYTE)accel,sizeof(ACCEL)*accls);
  }
}

void DlgUVEditor::LoadHotkeys()
{
  LPBYTE data;
  UINT size;
  if (theApp.GetProfileBinary(HOTKEYSECTION,HOTKEYITEM,&data,&size)!=FALSE) 
  {
    _userAccel=CreateAcceleratorTable((LPACCEL)data,size/sizeof(ACCEL));
    delete [] data;
  }
  CHotKeyCfgDlg::RecursiveReplaceMenuHotkeys(GetDefaultAccelerator(),*GetMenu());
}

void DlgUVEditor::OnFileHotkeys() 
{
  if (_hotKeyDlg==0) _hotKeyDlg=new CHotKeyCfgDlg;
  if (_hotKeyDlg->GetSafeHwnd()) return;
  _hotKeyDlg->Create(_hotKeyDlg->IDD,this);
  _hotKeyDlg->CenterWindow();
}

void DlgUVEditor::OnUveditorNextset()
{
  int cur=wUVSetTab.GetCurSel();
  int cmax=wUVSetTab.GetItemCount();
  cur=(cur+1)%cmax;
  _curDocument->OnUvSetActivate(cur+ID_UVSETS_ACTIVATE0);
}

void DlgUVEditor::OnUveditorPreviousset()
{
  int cur=wUVSetTab.GetCurSel();
  int cmax=wUVSetTab.GetItemCount();
  cur--;
  if (cur<0) cur=cmax-1;
  _curDocument->OnUvSetActivate(cur+ID_UVSETS_ACTIVATE0);
}

void DlgUVEditor::OnEditSelectall()
{
  wUVView.SelectAll();
}

void DlgUVEditor::OnEditCut()
{
  OnEditCopy();
  OnEditDelete();
}

bool DlgUVEditor::ShowSubmenu(int containItem, CMenu *findWhere)
{
  if (findWhere==0) findWhere=GetMenu();
  for (int i=0,cnt=findWhere->GetMenuItemCount();i<cnt;i++)
  {
    if (findWhere->GetSubMenu(i))
    {
      if (ShowSubmenu(containItem,findWhere->GetSubMenu(i))) return true;
    }
    else
    {
      int cmd=findWhere->GetMenuItemID(i);
      if (cmd==containItem)
      {
        CRect rc;
        wUVView.GetWindowRect(&rc);
        findWhere->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON|TPM_LEFTBUTTON,rc.left,rc.top,this,0);
        return true;
      }
    }    
  }
  return false;
}

void DlgUVEditor::OnAlignAlignmenu()
{
  ShowSubmenu(ID_ALIGN_ALIGNMENU);
}



void DlgUVEditor::OnFilterShowalluvs()
{
  _filterUv.Clear();
  LoadUVS();
  wUVView.LoadDefaultTexture();
  UpdateTitle();
  wUVView.FitUVSToWindow(false);
}

void DlgUVEditor::OnFilterMaintexturemenu()
{
  ExploreObject();
  for (int i=0;i<_menuItems.Size();i++) if (_menuItems[i].type==UVEditorMenuItem::texFilter)
  {ShowSubmenu(i+TEXTURE_MENU_OFFSET);break;}
}

void DlgUVEditor::OnFilterMaterialmenu()
{
  ExploreObject();
  for (int i=0;i<_menuItems.Size();i++) if (_menuItems[i].type==UVEditorMenuItem::matFilter)
  {ShowSubmenu(i+TEXTURE_MENU_OFFSET);break;}
}

void DlgUVEditor::OnFilterMaterialtexturemenu()
{
  ExploreObject();
  for (int i=0;i<_menuItems.Size();i++) if (_menuItems[i].type==UVEditorMenuItem::texmatFilter)
  {ShowSubmenu(i+TEXTURE_MENU_OFFSET);break;}
}

void DlgUVEditor::OnZoomZoommenu()
{
  ShowSubmenu(ID_ZOOM_ZOOMMENU);
}

void DlgUVEditor::OnViewBrowsetexture()
{
  CFileDialogEx dlg(TRUE,0,_lastTexture,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,CString(MAKEINTRESOURCE(IDS_PACPAAFILTER)),this);
  if (dlg.DoModal()==IDOK)
  {
    LoadTexture(dlg.GetPathName());
  }
}

void DlgUVEditor::OnFilterBrowsenewtexture()
{
  CFileDialogEx dlg(TRUE,0,_filterUv.Size()?RString(config->GetString(IDS_CFGPATHFORTEX),_filterUv[0]):RString(),OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,CString(MAKEINTRESOURCE(IDS_PACPAAFILTER)),this);
  if (dlg.DoModal()==IDOK)
  {
    _filterUv.Clear();
    _filterUv.Append(dlg.GetPathName().Mid(strlen(config->GetString(IDS_CFGPATHFORTEX))).GetString());
    LoadTexture(dlg.GetPathName()); 
    LoadUVS();
    UpdateTitle();
  }  
}

void DlgUVEditor::OnFilterBrowsenewmaterial()
{
  CFileDialogEx dlg(TRUE,0,_filterUv.Size()?RString(config->GetString(IDS_CFGPATHFORTEX),_filterUv[0]):RString(),OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,CString(MAKEINTRESOURCE(IDS_MATERIALFILTER)),this);
  if (dlg.DoModal()==IDOK)
  {
    _filterUv.Clear();
    _filterUv.Append(dlg.GetPathName().Mid(strlen(config->GetString(IDS_CFGPATHFORTEX))).GetString());
    LoadUVS();
    UpdateTitle();
  }
}

void DlgUVEditor::OnFilterCreatematerial()
{
  if (_matPlugin)
  {
    if (_matPlugin->CreateMaterial(CString(_curDocument->filename.GetDirectoryWithDrive())+"data"))
    {    
      _filterUv.Clear();
      _filterUv.Append()=_matPlugin->GetActiveMaterial()+strlen(config->GetString(IDS_CFGPATHFORTEX));
      LoadUVS();
      UpdateTitle();
    }
  }
}

void DlgUVEditor::OnUpdateFilterCreatematerial(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_matPlugin!=0);
}

void DlgUVEditor::OnCurrentfilterNone()
{
  OnFilterShowalluvs();
}

void DlgUVEditor::UpdateTitle()
{
CString title;
GetWindowText(title);
if (_titlesize==0) _titlesize=title.GetLength();
title=title.Right(_titlesize);
if (_filterUv.Size()) 
{
  CString sum;
  for (int i=0;i<_filterUv.Size();i++)
  {
    if (i) sum=sum+"+";
    sum=sum+Pathname::GetNameFromPath(_filterUv[i]);
  }
  title=sum+" - "+title;
}
SetWindowText(title);
}
void DlgUVEditor::OnUpdateEditCopy(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_anyselect);
}

void DlgUVEditor::OnUpdateEditCut(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_anybreakableselect);
}

void DlgUVEditor::OnAlignCmd(UINT cmd)
{
  FRect rc=wUVView.CalculateUVBounding(true);
  const Array<UVEditorViewObject> &uvobjs=wUVView.GetUVObjects();
  AutoArray<UVEditorViewObject> aligned;
  bool points=(cmd>=ID_ALIGN_POINTS_LEFT && cmd<=ID_ALIGN_POINTS_BOTTOM);  
  for (int i=0;i<uvobjs.Size();i++)
    if (uvobjs[i].selectedVertices)
    {
      UVEditorViewObject newobj=uvobjs[i];
      float max=(cmd==ID_ALIGN_OBJS_LEFT || cmd==ID_ALIGN_OBJS_RIGHT)?newobj.uv[0].u:newobj.uv[0].v;
      for (int j=0;j<newobj.n;j++) if (newobj.IsSelectedVertex(j))
      {             
        switch(cmd)
        {
        case ID_ALIGN_POINTS_LEFT: newobj.uv[j].u=rc.leftTop.x;break;
        case ID_ALIGN_POINTS_TOP: newobj.uv[j].v=rc.leftTop.y;break;
        case ID_ALIGN_POINTS_RIGHT: newobj.uv[j].u=rc.rightBottom.x;break;
        case ID_ALIGN_POINTS_BOTTOM: newobj.uv[j].v=rc.rightBottom.y;break;
        case ID_ALIGN_OBJS_LEFT: if (max>newobj.uv[j].u) max=newobj.uv[j].u;break;
        case ID_ALIGN_OBJS_TOP: if (max>newobj.uv[j].v) max=newobj.uv[j].v;break;
        case ID_ALIGN_OBJS_RIGHT: if (max<newobj.uv[j].u) max=newobj.uv[j].u;break;
        case ID_ALIGN_OBJS_BOTTOM: if (max<newobj.uv[j].v) max=newobj.uv[j].v;break;
        }
      }
      if (!points)
      {
        float xdiff=0,ydiff=0; 
        switch (cmd)
        {
        case ID_ALIGN_OBJS_LEFT: xdiff=rc.leftTop.x-max;break; 
        case ID_ALIGN_OBJS_TOP: ydiff=rc.leftTop.y-max;break;
        case ID_ALIGN_OBJS_RIGHT: xdiff=rc.rightBottom.x-max;break;
        case ID_ALIGN_OBJS_BOTTOM: ydiff=rc.rightBottom.y-max;break;
        }
        for (int j=0;j<newobj.n;j++)
        {
          newobj.uv[j].u+=xdiff;
          newobj.uv[j].v+=ydiff;
        }
      }
    aligned.Append(newobj);
    }
  TransformedSelection(aligned);
  LoadUVS();
}
void DlgUVEditor::OnViewRefresh33769()
{
  UpdateFromDocument(_curDocument);
}

void DlgUVEditor::OnUveditorExport()
{
  CSize texDim=wUVView.GetCurTextureDimensions();
  DlgUVExport fdlg(FALSE,"emf",_filterUv.Size()?(RString(config->GetString(IDS_CFGPATHFORTEX),_filterUv[0])+".emf"):RString(),OFN_PATHMUSTEXIST|OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,"EMF|*.emf|WMF|*.wmf|");
  fdlg._aspect=(float)texDim.cx/(float)texDim.cy;
  if (fdlg.DoModal()==IDOK)
  {
    int width=2048;
    int height=2048;

    if (fdlg._keepAspect) 
    {
      width=texDim.cx;
      height=texDim.cy;
    }
    if (fdlg._specifyDim)
    {
      width=fdlg._dimWidth;
      height=fdlg._dimHeight;
    }

    CDC dc;
    CString name=fdlg.GetPathName();
    bool wmf=_stricmp(Pathname::GetExtensionFromPath(name),".wmf")==0;

    if (wmf)
      dc.Attach(CreateMetaFile(fdlg.GetPathName()));
    else
      dc.Attach(CreateEnhMetaFile(0,fdlg.GetPathName(),0,"Objektiv2\0UV Editor\0Bohemia Interactive Studio\0\0"));
    const Array<UVEditorViewObject> &objs=wUVView.GetUVObjects();
    dc.SelectStockObject(BLACK_PEN);
    COLORREF lastcolor=0;
    dc.SelectStockObject(HOLLOW_BRUSH);
    CPen pn;
    for (int i=0;i<objs.Size();i++)
    {
      const UVEditorViewObject &obj=objs[i];
      POINT pts[MAX_DATA_POLY];
      for (int j=0;j<obj.n;j++)
      {
        pts[j].x=toLargeInt(obj.uv[j].u*width);
        pts[j].y=toLargeInt(obj.uv[j].v*height);
      }
      if (obj.color!=lastcolor)
      {
        dc.SelectStockObject(BLACK_PEN);
        pn.DeleteObject();
        pn.CreatePen(PS_SOLID,1,obj.color);
        dc.SelectObject(&pn);
        lastcolor=obj.color;
      }
      dc.Polygon(pts,obj.n);
    }    
    if (fdlg._addTextureOutline)
    {
      CPen pen(PS_DOT,1,RGB(64,0,128));
      CPen *old=dc.SelectObject(&pen);
      dc.Rectangle(0,0,width,height);
      dc.SelectObject(old);
    }
    dc.SelectStockObject(BLACK_PEN);
    if (wmf)
      DeleteMetaFile(CloseMetaFile(dc.Detach()));
    else
      DeleteEnhMetaFile(CloseEnhMetaFile(dc.Detach()));
  }
}

void DlgUVEditor::OnViewBlackonwhite()
{
  wUVView.SetWhiteMode(!wUVView.IsWhiteMode());
  UpdateFromDocument(_curDocument);
  wUVView.Invalidate();
}

void DlgUVEditor::OnUpdateViewBlackonwhite(CCmdUI *pCmdUI)
{
  pCmdUI->SetCheck(wUVView.IsWhiteMode());
}


void DlgUVEditor::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
if (_curDocument && nState!=WA_INACTIVE && !bMinimized) UpdateFromDocument(_curDocument);
__super::OnActivate(nState, pWndOther,bMinimized);
}
void DlgUVEditor::OnUpdateViewBackgroundlod(CCmdUI *pCmdUI)
{
  pCmdUI->SetCheck(_backLod);
}

void DlgUVEditor::OnViewBackgroundLOD()
{
  _backLod=!_backLod;
  LoadUVS();
}

void DlgUVEditor::OnEditAutosnap()
{
  wUVView.EnableAutoSnap(!wUVView.IsAutoSnapEnabled());
}

void DlgUVEditor::OnUpdateEditAutosnap(CCmdUI *pCmdUI)
{
  pCmdUI->SetCheck(wUVView.IsAutoSnapEnabled());
}

void DlgUVEditor::OnEditTransform()
{
  if (_dlgTransform.GetSafeHwnd()==0)
  {
    _dlgTransform.Create(_dlgTransform.IDD,this);
    _dlgTransform.ShowWindow(SW_SHOW);
  }
}

void DlgUVEditor::ManualTransformPerformed(const DlgUVManualTransform &dlg, bool preview)
{
  FTransform result;
  FTransform op;
  CSize txsz=wUVView.GetCurTextureDimensions();
  FTransform pixelToUv;
  FTransform UvToPixel;
  pixelToUv.Scale(FPoint2D(1.0f/txsz.cx,1.0f/txsz.cy));
  UvToPixel.Scale(FPoint2D(1.0f*txsz.cx,1.0f*txsz.cy));

  if (dlg.vUnits==0)
  {  
    switch (dlg.vOperation)
    {
    case 0: op.Translation(FPoint2D(dlg.vUAxe,dlg.vVAxe));break;
    case 1: op.Rotation(dlg.vRadius*3.14159265/180);break;
    case 2: op.Scale(FPoint2D(dlg.vUAxe,dlg.vVAxe));break;
    default: return;
    }
  }
  else
  {
    switch (dlg.vOperation)
    {
    case 0: op.Translation(FPoint2D(dlg.vUAxe/txsz.cx,dlg.vVAxe/txsz.cx));break;
    case 1: op=UvToPixel*op.Rotation(dlg.vRadius*3.14159265/180)*pixelToUv;break;
    case 2: op=UvToPixel*op.Scale(FPoint2D(dlg.vUAxe,dlg.vVAxe))*pixelToUv;break;
    }
  }

  FPoint2D center;
  FRect selArea=wUVView.CalculateUVBounding(true);
  switch (dlg.vCenter)
  {
  case 0: center=selArea.CenterPoint();break;
  case 1: center=selArea.leftTop;break;
  case 2: center.x=(selArea.leftTop.x+selArea.rightBottom.x)*0.5f;center.y=selArea.leftTop.y;break;
  case 3: center.x=selArea.rightBottom.x;center.y=selArea.leftTop.y;break;
  case 4: center.y=(selArea.leftTop.y+selArea.rightBottom.y)*0.5f;center.x=selArea.rightBottom.x;break;
  case 5: center=selArea.rightBottom;break;
  case 6: center.x=(selArea.leftTop.x+selArea.rightBottom.x)*0.5f;center.y=selArea.rightBottom.y;break;
  case 7: center.x=selArea.leftTop.x;center.y=selArea.rightBottom.y;break;
  case 8: center.y=(selArea.leftTop.y+selArea.rightBottom.y)*0.5f;center.x=selArea.leftTop.x;break;
  case 9: center.x=dlg.vCenterU;center.y=dlg.vCenterV;break;
  }

  result=FTransform().Translation(-center)*op*FTransform().Translation(center);
  wUVView.TransformSelection(result,false,preview);
}

void DlgUVEditor::OnEditHorizontalmirror()
{
  wUVView.SelectionMirrorHorz();
}

void DlgUVEditor::OnEditVerticalmirror()
{
  wUVView.SelectionMirrorVert();
}
