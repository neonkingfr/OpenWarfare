#if !defined(AFX_ASFIMPORTDLG_H__6A62531A_3AC9_434C_B8DF_681A8B44D88C__INCLUDED_)
#define AFX_ASFIMPORTDLG_H__6A62531A_3AC9_434C_B8DF_681A8B44D88C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ASFImportDlg.h : header file
//

class CASFHierarchy;
/////////////////////////////////////////////////////////////////////////////
// CASFImportDlg dialog

class CASFImportDlg : public CDialog
  {
  // Construction
  CSize sz;
  public:
    CASFHierarchy *asf;	
    CASFImportDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CASFImportDlg)
    enum 
      { IDD = IDD_ASFAMCIMPORT };
    CListCtrl	list;
    float	scale;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CASFImportDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CASFImportDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    virtual void OnOK();
    afx_msg void OnEndlabeleditList1(NMHDR* pNMHDR, LRESULT* pResult);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ASFIMPORTDLG_H__6A62531A_3AC9_434C_B8DF_681A8B44D88C__INCLUDED_)
