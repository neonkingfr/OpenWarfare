// SelectionGroup.h: interface for the CSelectionGroup class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SELECTIONGROUP_H__686BF9A7_B1E2_4604_B707_676744819CB6__INCLUDED_)
#define AFX_SELECTIONGROUP_H__686BF9A7_B1E2_4604_B707_676744819CB6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define SELGROUPMAX 4

class CSelectionGroup  
  {
  struct SelGroup
    {
    bool marked[MAX_NAMED_SEL];
    float level;
    };
  
  SelGroup list[SELGROUPMAX];
  SelGroup * FindLevel(float level);
  int pos;
  public:
    void ClearGroup(float level);
    
    int GetGroupHandle(float level);
    bool GetSelMarked(int handle, int selid)
      {if (handle<0) return false;else return list[handle].marked[selid];}
    
    int CreateGroupHandle(float level);
    void SetSelMark(int handle, int selid,bool mark=true)
      {if (handle>=0) list[handle].marked[selid]=mark;}
    bool *GetMarks(int handle)
      {return list[handle].marked;}
    static void NormalizeSelections(ObjectData *obj, bool *marks, int selid);
    CSelectionGroup();
    virtual ~CSelectionGroup();
    void Reset();
  };

//--------------------------------------------------

#endif // !defined(AFX_SELECTIONGROUP_H__686BF9A7_B1E2_4604_B707_676744819CB6__INCLUDED_)
