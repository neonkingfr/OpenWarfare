// TransformDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "TransformDlg.h"
#include "Objektiv2Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTransformDlg dialog

BOOL	CTransformDlg::sav_allAnims[3]=
  {FALSE,FALSE,FALSE};

//--------------------------------------------------

BOOL	CTransformDlg::sav_allLODs[3]=
  {FALSE,FALSE,FALSE};

//--------------------------------------------------

BOOL	CTransformDlg::sav_selOnly[3]=
  {TRUE,TRUE,TRUE};

//--------------------------------------------------

float	CTransformDlg::sav_xValue[3]=
  {0.0f,0.0,1.0};

//--------------------------------------------------

float	CTransformDlg::sav_yValue[3]=
  {0.0f,0.0f,1.0f};

//--------------------------------------------------

float	CTransformDlg::sav_zValue[3]=
  {0.0f,0.0f,1.0f};

//--------------------------------------------------

CTransformDlg::CTransformDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CTransformDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CTransformDlg)
    allAnims = FALSE;
    allLODs = FALSE;
    relToPin = FALSE;
    selOnly = FALSE;
    xValue = 0.0f;
    yValue = 0.0f;
    zValue = 0.0f;
    gismo = FALSE;
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CTransformDlg::DoDataExchange(CDataExchange* pDX)
  {
  if (pDX->m_bSaveAndValidate)
    CorrectFloatValues(this,IDC_XVALUE,IDC_YVALUE,IDC_ZVALUE,0);
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CTransformDlg)
  DDX_Check(pDX, IDC_APPLYTOANIMS, allAnims);
  DDX_Check(pDX, IDC_APPLYTOLODS, allLODs);
  DDX_Check(pDX, IDC_RELTOPIN, relToPin);
  DDX_Check(pDX, IDC_SELONLY, selOnly);
  DDX_Text(pDX, IDC_XVALUE, xValue);
  DDX_Text(pDX, IDC_YVALUE, yValue);
  DDX_Text(pDX, IDC_ZVALUE, zValue);
  DDX_Check(pDX, IDC_GISMO, gismo);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CTransformDlg, CDialog)
  //{{AFX_MSG_MAP(CTransformDlg)
  ON_BN_CLICKED(IDC_PREVIEW, OnPreview)
    ON_WM_TIMER()
      ON_EN_CHANGE(IDC_XVALUE, OnChangeValue)
        ON_BN_CLICKED(IDC_APPLYTOLODS, OnApplytolods)
          ON_EN_CHANGE(IDC_YVALUE, OnChangeValue)
            ON_EN_CHANGE(IDC_ZVALUE, OnChangeValue)
              //}}AFX_MSG_MAP
              END_MESSAGE_MAP()
                
/////////////////////////////////////////////////////////////////////////////
// CTransformDlg message handlers

BOOL CTransformDlg::OnInitDialog() 
  {
  switch (cmd)
    {
    case ID_TRANSF_MOVE: slot=0;break;
    case ID_TRANSF_ROTATE:slot=1;break;
    case ID_TRANSF_SCALE: slot=2;break;
    }
  allAnims = sav_allAnims[slot];
  allLODs = sav_allLODs[slot];
  selOnly = sav_selOnly[slot];
  xValue = sav_xValue[slot];
  yValue = sav_yValue[slot];
  zValue = sav_zValue[slot];
  
  relToPin=doc->pinlock;
  CDialog::OnInitDialog();
  switch (cmd)
    {
    case ID_TRANSF_MOVE: SetWindowText(WString(IDS_TITLEMOVE));break;
    case ID_TRANSF_ROTATE: SetWindowText(WString(IDS_TITLEROTATE));break;
    case ID_TRANSF_SCALE:SetWindowText(WString(IDS_TITLESCALE));break;
    }
  SetTimer(1,1000,NULL);
  prwfaze=2;
  CopyVektor (pin,doc->pin);
  GetDlgItem(IDC_SELONLY)->EnableWindow(IsDlgButtonChecked(IDC_APPLYTOLODS)==0);
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

void CTransformDlg::OnPreview() 
  {  
  HMATRIX mm;
  prwfaze=2;
  if (UpdateData()==FALSE) return;
  ObjectData *odata=doc->LodData->Active();
  CalcTransfMatrix(mm,odata);
  PosT *savelist;
  int cnt=odata->NPoints();
  savelist=new PosT[cnt];
  int i;
  int oldselOnly = selOnly;
  if (allLODs){selOnly=FALSE;};
  for (i=0;i<cnt;i++) savelist[i]=odata->Point(i);
  MakeTransformForLod(odata,mm,false);
  selOnly = oldselOnly;
  HMATRIX gs;
  if (gismo) 
    {
    CopyMatice(gs,doc->gismo);
    SoucinMatic(gs,mm,doc->gismo);
    }
  doc->UpdateAllViews(NULL,1);  
  if (gismo) 	CopyMatice(doc->gismo,gs);
  for (i=0;i<cnt;i++) odata->Point(i)=savelist[i];
  delete [] savelist;
  }

//--------------------------------------------------

void CTransformDlg::CalcTransfMatrix(HMATRIX mm, ObjectData *obj)
  {
  HVECTOR center;
  HMATRIX m1,m2;
  if (relToPin) CopyVektor(center,pin);else
    if (allAnims || allLODs) 	memset(center,0,sizeof(center));
  else GetObjectCenter(selOnly!=0,center,obj);
  switch (cmd)
    {
    case ID_TRANSF_MOVE: Translace(mm,xValue,yValue,zValue);break;
    case ID_TRANSF_SCALE: Translace(mm,-center[XVAL],-center[YVAL],-center[ZVAL]);
    Zoom(m1,xValue,yValue,zValue);
    SoucinMatic(mm,m1,m2);
    Translace(m1,center[XVAL],center[YVAL],center[ZVAL]);
    SoucinMatic(m2,m1,mm);
    break;
    case ID_TRANSF_ROTATE:Translace(mm,-center[XVAL],-center[YVAL],-center[ZVAL]);
    RotaceX(m1,torad(xValue));
    SoucinMatic(mm,m1,m2);
    RotaceY(m1,torad(yValue));
    SoucinMatic(m2,m1,mm);
    RotaceZ(m1,torad(zValue));
    SoucinMatic(mm,m1,m2);
    Translace(m1,center[XVAL],center[YVAL],center[ZVAL]);
    SoucinMatic(m2,m1,mm);
    break;
    default: JednotkovaMatice(mm);
    }
  }

//--------------------------------------------------

void CTransformDlg::OnTimer(UINT nIDEvent) 
  {
  if (nIDEvent==1)
    {
    if (prwfaze==1) OnPreview();else if (prwfaze==0) prwfaze=1;
    }	
  else
    CDialog::OnTimer(nIDEvent);
  }

//--------------------------------------------------

void CTransformDlg::OnChangeValue() 
  {
  prwfaze=0;  	
  }

//--------------------------------------------------

void CTransformDlg::OnApplytolods() 
  {
  GetDlgItem(IDC_SELONLY)->EnableWindow(IsDlgButtonChecked(IDC_APPLYTOLODS)==0);
  }

//--------------------------------------------------

void CTransformDlg::MakeTransformForLod(ObjectData *odata,HMATRIX mm, bool allanims)
  {
  int cnt=odata->NPoints();
  int i;
  HVECTOR tv;
  for (i=0;i<cnt;i++)
    if (!selOnly || odata->PointSelected(i))
      {
      PosT &pt=odata->Point(i);
      TransformVector(mm,mxVector3(pt[0],pt[1],pt[2]),tv);
      CorrectVectorM(tv);
      pt[0]=tv[XVAL];
      pt[1]=tv[YVAL];
      pt[2]=tv[ZVAL];
      }
  if (allanims)
    {
    cnt=odata->NAnimations();
    for (i=0;i<cnt;i++)
      {
      AnimationPhase *aphase=odata->GetAnimation(i);
      if (aphase->GetTime()>=-0.00001f) 
        for (int j=0;j<aphase->N();j++) if (odata->PointSelected(j) || !selOnly)
          {
          Vector3P &pt=(*aphase)[j];
          TransformVector(mm,mxVector3(pt[0],pt[1],pt[2]),tv);
          CorrectVectorM(tv);
          pt[0]=tv[XVAL];
          pt[1]=tv[YVAL];
          pt[2]=tv[ZVAL];
          }	  
      }
    }
  }

//--------------------------------------------------

void CTransformDlg::DoTransforms(LODObject *lod)
  {
  int ld;
  int cnt;
  HMATRIX mm;
  CalcTransfMatrix(mm,lod->Active());
  if (allLODs) 
    {selOnly=FALSE;ld=0;cnt=lod->NLevels();}
  else 
    {ld=lod->ActiveLevel();cnt=ld+1;}
  for (;ld<cnt;ld++)
    {
    ObjectData *odata=lod->Level(ld);
    MakeTransformForLod(odata,mm,allAnims==TRUE);
    }
  }

//--------------------------------------------------

void CTransformDlg::Load(istream &str)
  {
  datard(str,allAnims);
  datard(str,allLODs);
  datard(str,selOnly);
  datard(str,xValue);
  datard(str,yValue);
  datard(str,zValue);
  datard(str,cmd);
  }

//--------------------------------------------------

void CTransformDlg::Save(ostream &str)
  {
  datawr(str,allAnims);
  datawr(str,allLODs);
  datawr(str,selOnly);
  datawr(str,xValue);
  datawr(str,yValue);
  datawr(str,zValue);
  datawr(str,cmd);
  }

//--------------------------------------------------

void CTransformDlg::OnOK() 
  {
  if (UpdateData()==FALSE) return;
  CDialog::OnOK();
  sav_allAnims[slot]=allAnims;
  sav_allLODs[slot]=allLODs;
  sav_selOnly[slot]=selOnly;
  sav_xValue[slot]=xValue;
  sav_yValue[slot]=yValue;
  sav_zValue[slot]=zValue;  
  }

//--------------------------------------------------

void CTransformDlg::DoGismoTranform()
  {
  if (gismo)
    {
    HMATRIX mm;
    CalcTransfMatrix(mm,doc->LodData->Active());
    doc->EditGizmo(mm);
    }
  }

//--------------------------------------------------

