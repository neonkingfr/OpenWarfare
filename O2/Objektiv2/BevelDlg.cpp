// BevelDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "BevelDlg.h"
#include "Objektiv2Doc.h"
#include "gePlane.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBevelDlg dialog

BOOL CBevelDlg::savedlocks=TRUE;
float CBevelDlg::savedstr=0.0f;


CBevelDlg::CBevelDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CBevelDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CBevelDlg)
    strength = savedstr;
    locks = savedlocks;
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CBevelDlg::DoDataExchange(CDataExchange* pDX)
  {
  if (pDX->m_bSaveAndValidate)
    CorrectFloatValues(this,IDC_STRENGHT,0);
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CBevelDlg)
  DDX_Control(pDX, IDC_STRENGHTSLD, wSldr);
  DDX_Text(pDX, IDC_STRENGHT, strength);
  DDX_Check(pDX, IDC_LOCKS, locks);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CBevelDlg, CDialog)
  //{{AFX_MSG_MAP(CBevelDlg)
  ON_BN_CLICKED(IDC_PREVIEW, OnPreview)
    ON_BN_CLICKED(IDC_RNG, OnRng)
      ON_WM_HSCROLL()
        ON_BN_CLICKED(IDC_RNG2, OnRng)
          ON_BN_CLICKED(IDC_RNG3, OnRng)
            ON_BN_CLICKED(IDC_RNG4, OnRng)
              ON_EN_CHANGE(IDC_STRENGHT, OnChangeStrenght)
                //}}AFX_MSG_MAP
                END_MESSAGE_MAP()
                  
                  /////////////////////////////////////////////////////////////////////////////
                  // CBevelDlg message handlers
                  
                  BOOL CBevelDlg::OnInitDialog() 
                    {
                    CDialog::OnInitDialog();
                    SelectRange();
                    SetupRange();
                    wSldr.SetPos((int)(strength*1000));
                    // TODO: Add extra initialization here
                    
                    return TRUE;  // return TRUE unless you set the focus to a control
                    // EXCEPTION: OCX Property Pages should return FALSE
                    }

//--------------------------------------------------

void CBevelDlg::OnPreview() 
  {
  ObjectData saved=*obj;
  UpdateData();
  CalculateBevel(obj);
  doc->UpdateAllViews(NULL,1);
  *obj=saved;
  }

//--------------------------------------------------

void CBevelDlg::CalculateBevel(ObjectData *obj)
  {
  int npt=obj->NPoints();
  int nfc=obj->NFaces();
  int i;
  RecalcNormals2(obj);
  PosT *nwpos=new PosT[npt];
  memset(nwpos,0,sizeof(*nwpos)*npt);
  ProgressBar<int> pb(npt);
  for (i=0;pb.AdvanceNext(1),i<npt;i++) if (obj->PointSelected(i))
    {
    PosT mypos=obj->Point(i);;
    VecT norm;
    norm[0]=norm[1]=norm[2]=0;
    geVector4 my(mypos[0],mypos[1],mypos[2]);
    int nwcnt=0;
    gePlaneGen trp[3];
    int t=0,tm=0;
    for (int j=0;j<nfc;j++) if (obj->FaceSelected(j))
      {
      FaceT fc(obj,j);
      int k;
      for (k=0;k<fc.N();k++)
        {
        if (fc.GetPoint(k)==i) break;
        }
      if (k!=fc.N())
        {
        PosT v1,v2,v3;
        norm+=obj->Normal(fc.GetNormal(k));
        v2=obj->Point(fc.GetPoint(0));
        v1=obj->Point(fc.GetPoint(1));
        v3=obj->Point(fc.GetPoint(2));
        gePlaneGen q(geVector4(v1[0],v1[1],v1[2]),geVector4(v2[0],v2[1],v2[2]),geVector4(v3[0],v3[1],v3[2]));
        bool planeok=true;
        for (int p=0;p<tm;p++) 
          {
          geVector4 v1(trp[p].a,trp[p].b,trp[p].c);
          geVector4 v2(q.a,q.b,q.c);
          geVector4 v3=v1^v2;
          float d=v3.Module2();
          if (d<0.001 || d>1e10)
            {
            planeok=false;
            break;
            }
          }
        if (!planeok) continue;
        trp[t]=q;
        trp[t].d+=strength*sqrt(trp[t].a*trp[t].a+trp[t].b*trp[t].b+trp[t].c*trp[t].c);	
        t++;tm++;
        if (tm>=3) tm=3;
        if (t>=3)
          {t=0;
          if (!nwcnt) 
            nwcnt++;}
        if (nwcnt)
          {
          geVector4 p=trp[0].Intersection(trp[1],trp[2]);		  
          if (p.IsFinite() && p.Distance(my)<(strength+10)*10)
            {
            nwpos[i][0]+=p.x;
            nwpos[i][1]+=p.y;
            nwpos[i][2]+=p.z;
            nwcnt++;
            }
          }
        }
      }
    if (!nwcnt && t==2)
      {
      PosT ps=obj->Point(i);
      trp[t]=gePlaneGen(trp[0],trp[1],geVector4(ps[0],ps[1],ps[2]));
      geVector4 p=trp[0].Intersection(trp[1],trp[2]);
      if (p.IsFinite() && p.Distance(my)<strength*10)
        {
        nwpos[i][0]=p.x;
        nwpos[i][1]=p.y;
        nwpos[i][2]=p.z;
        nwcnt+=2;
        }	  
      }
    if (nwcnt>1) 
      {
      nwcnt--;
      nwpos[i]/=nwcnt;
      }
    else
      {
      norm.Normalize();
      nwpos[i]=obj->Point(i);
      nwpos[i]+=norm*strength;
      }
    }
  for (i=0;i<npt;i++)  if (obj->PointSelected(i)) obj->Point(i)=nwpos[i];
  delete [] nwpos;
  }

//--------------------------------------------------

int CIBevel::Execute(LODObject *obj)
  {
  data.CalculateBevel(obj->Active());
  return 0;
  }

//--------------------------------------------------

CIBevel::CIBevel(CBevelDlg *src)
  {
  memcpy(&data,src,sizeof(data));
  }

//--------------------------------------------------

CIBevel::CIBevel(istream &src)
  {
  datard(src,data.locks);
  datard(src,data.lockx);
  datard(src,data.locky);
  datard(src,data.lockz);
  datard(src,data.strength);
  }

//--------------------------------------------------

void CIBevel::Save(ostream &str)
  {
  datawr(str,data.locks);
  datawr(str,data.lockx);
  datawr(str,data.locky);
  datawr(str,data.lockz);
  datawr(str,data.strength);
  }

//--------------------------------------------------

CICommand *CIBevel_Create(istream &str)
  {
  return new CIBevel(str);
  }

//--------------------------------------------------

void CBevelDlg::SetupRange()
  {
  float rg=1.0f;
  if (IsDlgButtonChecked(IDC_RNG)) rg=1.0f;
  if (IsDlgButtonChecked(IDC_RNG2)) rg=5.0f;
  if (IsDlgButtonChecked(IDC_RNG3)) rg=10.0f;
  if (IsDlgButtonChecked(IDC_RNG4)) rg=50.0f;
  wSldr.SetRangeMax((int)(rg*1000),TRUE);
  wSldr.SetRangeMin(-(int)(rg*1000),TRUE);
  wSldr.SetTicFreq(1000);
  }

//--------------------------------------------------

void CBevelDlg::SelectRange()
  {
  float abstr=(float)fabs(strength);
  if (abstr<1.0f) CheckDlgButton(IDC_RNG,BST_CHECKED);
  else if (abstr<5.0f) CheckDlgButton(IDC_RNG2,BST_CHECKED);
  else if (abstr<10.0f) CheckDlgButton(IDC_RNG3,BST_CHECKED);
  else if (abstr<50.0f) CheckDlgButton(IDC_RNG4,BST_CHECKED);
  }

//--------------------------------------------------

void CBevelDlg::OnRng() 
  {
  SetupRange();	
  }

//--------------------------------------------------

void CBevelDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
  {
  if ((CSliderCtrl *)pScrollBar==&wSldr && nSBCode!=SB_ENDSCROLL)
    {
    int nPos=wSldr.GetPos();
    char buff[20];
    sprintf(buff,"%.3f",(int)nPos*0.001f);
    SetDlgItemText(IDC_STRENGHT,buff);
    }
  CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
  }

//--------------------------------------------------

void CBevelDlg::OnChangeStrenght() 
  {
  if (GetFocus()!=GetDlgItem(IDC_STRENGHT)) return;
  char buff[50];
  float pos=0;
  GetDlgItemText(IDC_STRENGHT,buff,50);
  char *c=buff;
  while ((c=strchr(c,','))!=0) *c='.';
  if (sscanf(buff,"%f",&pos)==1)
    {
    wSldr.SetPos((int)(pos*1000));
    }
  }

//--------------------------------------------------

