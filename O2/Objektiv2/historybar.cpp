// HistoryBar.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "HistoryBar.h"
#include "ComInt.h"
#include "Objektiv2Doc.h"
#include <malloc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHistoryBar dialog


CHistoryBar::CHistoryBar()
  : CDialogBar()
    {
    //{{AFX_DATA_INIT(CHistoryBar)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    //{{AFX_DATA_MAP(CHistoryBar)
    // NOTE: the ClassWizard will add DDX and DDV calls here
    //}}AFX_DATA_MAP
    }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CHistoryBar, CDialogBar)
  //{{AFX_MSG_MAP(CHistoryBar)
  ON_WM_SIZE()
    ON_WM_DESTROY()
      ON_WM_MEASUREITEM()
        ON_WM_DRAWITEM()
          ON_LBN_SELCHANGE(IDC_LIST, OnSelchangeList)
            ON_WM_CONTEXTMENU()
              //}}AFX_MSG_MAP
              END_MESSAGE_MAP()
                
                /////////////////////////////////////////////////////////////////////////////
                // CHistoryBar message handlers
                
                void CHistoryBar::Create(CWnd *parent, int menuid)
                  {
                  CDialogBar::Create(parent,IDD,CBRS_RIGHT,menuid);
                  SetBarStyle(GetBarStyle()| CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
                  EnableDocking(CBRS_ALIGN_ANY);
                  CString top;top.LoadString(IDS_HISTORYTITLE);
                  SetWindowText(top);
                  notify=parent;
                  lsb.Attach(::GetDlgItem(m_hWnd,IDC_LIST));
                  LoadBarSize(*this,160,0);
                  comint.SetUpdateListbox(&lsb);
                  }

//--------------------------------------------------

void CHistoryBar::OnSize(UINT nType, int cx, int cy) 
  {
  CDialogBar::OnSize(nType, cx, cy);
  if ((HWND)lsb) lsb.MoveWindow(5,5,cx-10,cy-10);
  }

//--------------------------------------------------

void CHistoryBar::OnDestroy() 
  {
  lsb.Detach();
  SaveBarSize(*this);
  CDialogBar::OnDestroy();	
  }

//--------------------------------------------------

void CHistoryBar::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct) 
  {
  if (nIDCtl==IDC_LIST)
    {
    CDC *dc=GetDC();
    CSize sz=dc->GetTextExtent("WWW");
    lpMeasureItemStruct->itemWidth=300;
    lpMeasureItemStruct->itemHeight=sz.cy+2;	
    ReleaseDC(dc);
    }
  else	  
    CDialogBar::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
  }

//--------------------------------------------------

void CHistoryBar::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
  {
  if (nIDCtl==IDC_LIST)
    {
    CDC dc;
    if (lpDrawItemStruct->itemAction!=ODA_DRAWENTIRE) return;
    dc.Attach(lpDrawItemStruct->hDC);
    int cursel=lsb.GetCurSel();
    int id=lpDrawItemStruct->itemID;
    bool grayed=id>cursel;	  	  
    if (grayed) 
      {
      dc.FillSolidRect(&lpDrawItemStruct->rcItem,GetSysColor(COLOR_3DFACE));
      dc.SetTextColor(GetSysColor(COLOR_GRAYTEXT));
      }
    else if (id==cursel)
      {
      dc.FillSolidRect(&lpDrawItemStruct->rcItem,GetSysColor(COLOR_HIGHLIGHT));
      dc.SetTextColor(GetSysColor(COLOR_HIGHLIGHTTEXT));
      }
    else
      {
      dc.FillSolidRect(&lpDrawItemStruct->rcItem,GetSysColor(COLOR_WINDOW));
      dc.SetTextColor(GetSysColor(COLOR_WINDOWTEXT));
      }
    char *text=(char *)alloca(lsb.GetTextLen(id)+1);
    lsb.GetText(id,text);
    dc.SetBkMode(TRANSPARENT);
    dc.TextOut(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.top,text,strlen(text));
    dc.SelectStockObject(BLACK_PEN);
    dc.MoveTo(0,lpDrawItemStruct->rcItem.bottom-1);
    dc.LineTo(lpDrawItemStruct->rcItem.right,lpDrawItemStruct->rcItem.bottom-1);
    dc.Detach();
    }
  else
    CDialogBar::OnDrawItem(nIDCtl, lpDrawItemStruct);
  }

//--------------------------------------------------

void CHistoryBar::OnSelchangeList() 
  {
  int item=lsb.GetCurSel();
  int level=lsb.GetItemData(item);
  comint.UndoTo(level);
  doc->UpdateAll();  
  lsb.Invalidate();
  }

//--------------------------------------------------

static BOOL GetActionFilename(BOOL open,Pathname &store)
  {  
  DWORD flags;
  if (open) flags=OFN_FILEMUSTEXIST|OFN_HIDEREADONLY|OFN_ENABLESIZING;
  else flags=OFN_OVERWRITEPROMPT|OFN_PATHMUSTEXIST|OFN_ENABLESIZING;
  CFileDialogEx fdlg(open,WString(IDS_ACT_DEFEXT),NULL,flags,WString(
#ifdef PUBLIC
    IDS_ACT_FILTER_PUBLIC
#else
    IDS_ACT_FILTER
#endif

    ));
  if (fdlg.DoModal()==IDCANCEL) return FALSE;
  store=fdlg.GetFileName();
  return TRUE;
  }

//--------------------------------------------------

void CHistoryBar::OnContextMenu(CWnd* pWnd, CPoint point) 
  {
  CMenu menu;
  Pathname filelist[100];
  int cnt=0;
  CFileFind flfind;
  Pathname name;
  bool record;  
  BOOL fnd=flfind.FindFile(WString(IDS_ACT_FINDEXT));
  while (fnd)
    {
    fnd=flfind.FindNextFile();
    filelist[cnt++]=flfind.GetFileName();	
    }
  ::LoadPopupMenu(IDR_HISTORY_POPUP,menu);
  for (int i=0;i<cnt;i++)	
    menu.AppendMenu(MF_STRING,i+1,filelist[i].GetFilename());	
  if (comint.GetRecordingStream()!=NULL)
    menu.EnableMenuItem(ID_HISTORY_RECORD,MF_BYCOMMAND|MF_GRAYED);
  else
    menu.EnableMenuItem(ID_HISTORY_STOP,MF_BYCOMMAND|MF_GRAYED);
  int cmd=::TrackPopupMenu(menu,TPM_LEFTALIGN|TPM_RETURNCMD|TPM_LEFTBUTTON|TPM_RIGHTBUTTON|TPM_NONOTIFY,
  point.x,point.y,0,*this,NULL);
  switch (cmd)
    {
    case ID_HISTORY_RECORD:
    if (GetActionFilename(FALSE,name)==FALSE) return;
    else record=true;break;
    case ID_HISTORY_STOP: 
      {
      ostream *fs=comint.StopRecord();
      delete fs;
      return;
      }
    case ID_HISTORY_DELETE:
    if (GetActionFilename(TRUE,name)==FALSE) return;
    if (AfxMessageBox(WString(IDS_ASKDELETEACTION,name),MB_YESNO|MB_ICONQUESTION)==IDNO) return;
    DeleteFile(name);
    return;
    case ID_HISTORY_CLEARHISTORY:
    comint.SetObject(doc->LodData);
    return;
    case ID_HISTORY_OPEN:
    if (GetActionFilename(TRUE,name)==FALSE) return;
    else record=false;break;
    default:
    if (cmd>100 || cmd<=0) return;
    name=filelist[cmd-1];
    record=false;
    break;
    }
  if (record)
    {
    ostream *fs=new ofstream(name,ios::out|ios::trunc|ios::binary);
    if (fs->fail())
      {
      delete fs;
      AfxMessageBox(IDS_UNABLETOCREATEFILE);
      return;
      }
    comint.RecordTo(fs);
    //don't delete fs. stream will be delete after stop operation
    }
  else
    {
    istream *is=new ifstream(name,ios::in|ios::binary);
    if (is->fail())
      {
      delete is;
      AfxMessageBox(IDS_LOADUNSUCCESFUL);
      return;
      }
    theApp.DoWaitCursor(1);
    comint.LoadAction(*is);
    delete is;
    theApp.DoWaitCursor(-1);
    doc->UpdateAll();
    }
  }

//--------------------------------------------------

