#if !defined(AFX_BEVELDLG_H__30418E81_FF72_4BFE_8054_352C2CF2945A__INCLUDED_)
#define AFX_BEVELDLG_H__30418E81_FF72_4BFE_8054_352C2CF2945A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BevelDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBevelDlg dialog

#include "ComInt.h"

class CBevelDlg : public CDialog
  {
  // Construction
  public:
    void SelectRange();
    void SetupRange();
    void CalculateBevel(ObjectData *obj);
    ObjectData *obj;
    
    bool lockx;
    bool locky;
    bool lockz;
    
    static BOOL savedlocks;
    static float savedstr;
    
    CBevelDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CBevelDlg)
    enum 
      { IDD = IDD_BEVEL };
    CSliderCtrl	wSldr;
    float	strength;
    BOOL	locks;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CBevelDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CBevelDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnPreview();
    afx_msg void OnRng();
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnChangeStrenght();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

class CIBevel: public CICommand
  {
  CBevelDlg data;
  public:
    virtual int Execute(LODObject *obj);
    CIBevel(CBevelDlg *src);
    CIBevel(istream &src);
    virtual ~CIBevel() 
      {}
    virtual int Tag() 
      {return CITAG_BEVEL;}
    virtual void Save(ostream &str);
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BEVELDLG_H__30418E81_FF72_4BFE_8054_352C2CF2945A__INCLUDED_)
