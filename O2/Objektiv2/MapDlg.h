#if !defined(AFX_MAPDLG_H__25D239CA_E5AA_4D22_BA33_1AB264939703__INCLUDED_)
#define AFX_MAPDLG_H__25D239CA_E5AA_4D22_BA33_1AB264939703__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MapDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMapDlg dialog

class CMapDlg : public CDialog
  {
  // Construction
  public:
    void LoadSchemesToLB(const char *active);
    void SelectScheme(const char *name);
    CMapDlg(CWnd* pParent = NULL);   // standard constructor
    CSize dlgsize;
    int curschemetype;	//scheme selector;
    
    // Dialog Data
    CStatic	wndValueName[6];
    CEdit	wndValue[6];
    float	Value[6];
    CString	Calc[6];
    
    // Dialog Data
    //{{AFX_DATA(CMapDlg)
    enum 
      { IDD = IDD_MAPDLG };
    CListBox	list;
    BOOL	onlypoints;
    CString	SchemeName;
    BOOL	uwrap;
    BOOL	vwrap;
    BOOL	onlyfaces;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CMapDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CMapDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnDefine();
    afx_msg void OnSelchangeList();
    afx_msg void OnSave();
    afx_msg void OnNew();
    afx_msg void OnRemove();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAPDLG_H__25D239CA_E5AA_4D22_BA33_1AB264939703__INCLUDED_)
