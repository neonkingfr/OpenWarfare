// ComInt.cpp: implementation of the CComInt class.
//
//////////////////////////////////////////////////////////////////////

#include <typeinfo.h>
#include "stdafx.h"
#include "Objektiv2.h"
#include "ComInt.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

static FILE *actionlog=0;

CComInt::CComInt(int maxu)
  {
  maxundo=maxu;
  curlevels=0;
  longop=false;
  locked=0;
  notify=NULL;
  actioncounter=0;
  nonserial=NULL;
  if (actionlog==0) 
    {
    char buff[MAX_PATH];
    GetModuleFileName(0,buff,MAX_PATH);
    char *z=strrchr(buff,'\\');
    strcpy(z,"\\actions.log");
    actionlog=fopen(buff,"w");
    }
  }

  CComInt::~CComInt()
  {
      delete lastobj;
      if (actionlog!=0) fclose(actionlog);
  }

//--------------------------------------------------

void CComInt::SetObject(LODObject *obj)
  {
  current=obj;
  if (last) delete lastobj;
  lastobj=new LODObject(*current);
  last=&first;
  first.Add(NULL);
  curlevels=0;
  LoadToListbox();
  SendNotify();
  sectchanged=1;
  if (actionlog)
  {
    fprintf(actionlog,"--------------------- RESET -----------------\n");
  }
  }

//--------------------------------------------------

int CComInt::Run(CICommand *com,bool addtoqueue)
  {
  if (locked) return -1;

  if (actionlog)
  {
    if (com->IsSeparator())
    {
      fprintf(actionlog,"------ Action : %s ----\n", (const char *)(static_cast<CIBeginAction *>(com)->GetText()));
    }
    else
    {
      fprintf(actionlog,"%s\n",typeid(*com).name());
    }
    fflush(actionlog);
  }

  CICommand *ls=last;
  int cr=curlevels;
  if (ls->IsSeparator()) 
    {ls=GetPrevAction(ls);cr--;}
  if (com->CanRemove(ls)) 
    {
    last=GetPrevAction(ls);
    curlevels=cr;	
    }
  if (rcstr!=NULL)
    {
    if (com->recordable==false) 
      {
      AfxMessageBox(IDS_UNABLETORECORDACTION);
      DoUndo();
      delete com;
      return -1;
      }
    com->record=true;
    }
  if (addtoqueue)
    {
    DWORD tm=GetTickCount();
    int i=com->Execute(current);
    if (GetTickCount()-tm>3000) 
      longop=true;
    if (i) 
      {DoUndo();delete com;return i;}
    last=last->Add(com);	
    if (com->IsSeparator())
      {	  
      curlevels++;
      if (curlevels>maxundo)
        {
        first.Add(first.Next()->Remove()); //remove CIBeginAction
        first.SaveBlock(*rcstr);  //Save block if record
        LODObject *obj=first.FindCache(); //Find cached lod
        if (obj!=NULL)		  
          {
          *lastobj=*obj;
          }
        else
          if (first.Redo(lastobj,0,NULL)) 
            {
            delete lastobj;
            lastobj=new LODObject(*current);
            if (rcstr) rcstr=StopRecord();
            first.Clear();		  
            }	
        first.DeleteBlock();
        curlevels--;
        }	
      LoadToListbox();
      }
    }
  else
    {
    com->Execute(lastobj);
    delete com;
    }
  SendNotify();
  if (com->marksave) current->SetDirty();
  actioncounter++;
  return 0;
  }

//--------------------------------------------------

CICommand * CICommand::DeleteBlock()
  {
  while (next && !next->IsSeparator())
    {
    Add(next->Remove());
    }
  return next;
  }

//--------------------------------------------------

int CICommand::Redo(LODObject *obj, int levels, CICommand **redonext)
  {
  if (redonext) *redonext=this;
  int res=Execute(obj);
  if (res==0) 
    if (next) return next->Redo(obj,levels,redonext);
  else return 0;
  return res;
  }

//--------------------------------------------------

int CIBeginAction::Redo(LODObject *obj, int levels, CICommand **redonext)
  {
  if (levels && next) return next->Redo(obj,levels-1,redonext);
  if (redonext) *redonext=this;
  return 0;
  }

//--------------------------------------------------

int CComInt::DoUndo()
  {
  if (IsLocked()) return -1;
  if (actionlog)
  {
    fprintf(actionlog,"### Undo (%d) ###\n",maxundo-curlevels); 
  }
  CICommand *ll,*nd=NULL;
  int lev;
  int llast=0;
  int needlv=curlevels;
  for (lev=0,ll=&first;lev<needlv;) 
    {
    if (ll==NULL) 
      {lev++;continue;}
    ll=ll->Next();
    if (ll->IsSeparator())
      {
      lev++;
      if (((CIBeginAction *)ll)->State()) 
        {nd=ll;llast=lev;}
      }
    }
  LODObject *redoed;
  if (nd) redoed=new LODObject(*((CIBeginAction *)nd)->State());
  else redoed=new LODObject(*lastobj);  
  ProgressBar<int> pb(curlevels);
  for (lev=llast,ll=nd?nd:&first;lev<needlv;) 
    {
    pb.SetNextPos(lev+1);
    int res=ll->Execute(redoed);
    if (res) return res;
    ll=ll->Next();
    if (ll->IsSeparator()) lev++;
    }
  last=ll;
  *current=*redoed;
  current->SetDirty();
  delete redoed;
  curlevels--;
  ChangeSelectionInList();
  SendNotify();
  return 0;
  }

//--------------------------------------------------

int CComInt::DoRedo()
  {
  if (IsLocked()) return -1;
  if (actionlog)
  {
    fprintf(actionlog,"### Redo ###\n");
  }
  CICommand *ll=last->Next();
  if (ll==NULL) return 0;
  int res=ll->Redo(current,0,&ll);
  if (res) return res;
  curlevels++;
  last=ll;
  ChangeSelectionInList();
  SendNotify();
  return 0;
  }

//--------------------------------------------------

void CComInt::UndoTo(int level)
  {
  int cnt=abs(level-curlevels);
  int cur=0;
  if (IsLocked()) return;  
  if (level==curlevels) return;
  if (level<curlevels)
    {
    curlevels=level+1;
    DoUndo();
    }
  else
  {
    ProgressBar<int> pb(cnt);
    while (curlevels!=level && last->Next()) 
      {
      pb.AdvanceNext(1);
      DoRedo();
      }
  }
  ChangeSelectionInList();
  SendNotify();
  }

//--------------------------------------------------

void CComInt::LoadToListbox()
  {
  lsb->SetRedraw(FALSE);
  lsb->ResetContent();
  CICommand *ll=first.Next();
  int lev=0;
  while (ll)
    {
    if (ll->IsSeparator())
      {
      WString w=((CIBeginAction *)ll)->GetText();
      lev++;
      if (ll->record) w="* "+w;
      int idx=lsb->AddString(w);	  
      lsb->SetItemData(idx,lev);
      if (curlevels==lev) lsb->SetCurSel(idx);
      }
    ll=ll->Next();
    }
  lsb->SetRedraw(TRUE);
  lsb->Invalidate();
  }

//--------------------------------------------------

void CComInt::ChangeSelectionInList()
  {
  int cnt=lsb->GetCount();
  int i;
  for (i=0;i<cnt;i++) 
    if (lsb->GetItemData(i)==(DWORD)curlevels) break;
  if (i!=cnt) 
    {
    lsb->SetCurSel(i);
    lsb->Invalidate();
    }
  }

//--------------------------------------------------

ostream *CComInt::StopRecord()
  {
  CICommand *cc=first.Next();
  while (cc)
    {
    if (cc->record) 
      {
      cc->SaveWithTag(*rcstr);
      cc->record=false;
      }
    cc=cc->Next();
    }
  ostream *out=rcstr;
  rcstr=NULL;
  return out;
  }

//--------------------------------------------------

void CComInt::LoadAction(istream &str)
  {
  Begin(WString(IDS_LOADACTION));
  CICommand *com=::CI_LoadTag(str);
  while (com)
    {
    if (Run(com)) 
      {
      AfxMessageBox(WString(IDS_LOADACTIONFAILED,com->Tag()));
      return;
      }
    com=::CI_LoadTag(str);	
    }
  }

//--------------------------------------------------

bool CComInt::IsLocked(bool error)
  {
  if (locked && error) AfxMessageBox(IDS_COMLOCKED,MB_OK|MB_ICONSTOP);
  return locked!=0;
  }

//--------------------------------------------------

void CComInt::SendNotify()
  {
  if (notifyack) return;
  if (notify) notify->PostMessage(WM_COMMAND, ncom);
  notifyack=true;
  }

//--------------------------------------------------

int CComInt::GetActionCounter(int reset)
  {
  int res=actioncounter;
  if (actioncounter>=reset)actioncounter=0;
  return res;
  }

//--------------------------------------------------

LODObject *CICommand::FindCache()
  {
  if (IsSeparator())
    {
    return ((CIBeginAction *)this)->State();
    }
  return next->FindCache();
  }

//--------------------------------------------------

CICommand *CComInt::GetPrevAction(CICommand *prev)
  {
  CICommand *p=&first;
  while (p->Next()!=prev) p=p->Next();
  return p;
  }

//--------------------------------------------------

int CComInt::Begin2(WString& text) 
  {
  if (IsLocked()) return -1;
  ProgressBar<int> pb(1);
  pb.AdvanceNext(1);
  pb.ReportState(IDS_INPROGRESS,text);
  openname=text;
  if (last->IsSeparator())
    {
    ((CIBeginAction *)last)->SetText(text);
    last->Add(NULL);
    curlevels++;
    LoadToListbox();
    if (longop) 
      {((CIBeginAction *)last)->SaveState(current);longop=false;}
    return 0;
    }
  else
    {
    CIBeginAction *bi=new CIBeginAction(text);
    if (longop) bi->SaveState(current);
    longop=false;
    return Run(bi);
    }
  }

//--------------------------------------------------

