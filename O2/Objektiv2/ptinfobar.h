#if !defined(AFX_PTINFOBAR_H__5968ED91_4690_4A4A_BADC_9AF27DFDEAD9__INCLUDED_)
#define AFX_PTINFOBAR_H__5968ED91_4690_4A4A_BADC_9AF27DFDEAD9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PtInfoBar.h : header file
//

#include "Objektiv2Doc.h"


/////////////////////////////////////////////////////////////////////////////
// CPtInfoBar dialog

class CPtInfoBar : public CDialogBar
  {
  // Construction
  CObjektiv2Doc *doc;
  public:
      void SetNewPt(const char *text, int value);
    CPtInfoBar();   // standard constructor
    void Create(CWnd *parent, int menuid, CObjektiv2Doc *parentdoc);
    CEdit xinfo;
    CEdit yinfo;
    CEdit zinfo;
    bool chg;
    
    // Dialog Data
    //{{AFX_DATA(CPtInfoBar)
    enum 
      { IDD = IDD_PTINFO };
    // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CPtInfoBar)
  public:
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CPtInfoBar)
    afx_msg void OnDestroy();
    afx_msg void OnKillfocusXinfo();
    afx_msg void OnKillfocusYinfo();
    afx_msg void OnKillfocusZinfo();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  public:
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PTINFOBAR_H__5968ED91_4690_4A4A_BADC_9AF27DFDEAD9__INCLUDED_)
