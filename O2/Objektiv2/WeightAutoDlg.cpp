// WeightAutoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "objektiv2.h"
#include "objektiv2doc.h"
#include "objektiv2view.h"
#include "WeightAutoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWeightAutoDlg dialog


CWeightAutoDlg::CWeightAutoDlg(CWnd* pParent /*=NULL*/)
  : CDialog(CWeightAutoDlg::IDD, pParent)
    {
    //{{AFX_DATA_INIT(CWeightAutoDlg)
    filename = _T("");
    findbones = FALSE;
    //}}AFX_DATA_INIT
    }

//--------------------------------------------------

void CWeightAutoDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CWeightAutoDlg)
  DDX_Control(pDX, IDC_DEFTEXTURE, m_deftex);
  DDX_Control(pDX, IDC_LOD, w_lod);
  DDX_Control(pDX, IDC_FILE, w_file);
  DDX_Control(pDX, IDC_DEFINITION, def);
  DDX_CBString(pDX, IDC_FILE, filename);
  DDX_Check(pDX, IDC_FINDBONES, findbones);
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CWeightAutoDlg, CDialog)
  //{{AFX_MSG_MAP(CWeightAutoDlg)
  ON_CBN_DROPDOWN(IDC_FILE, OnDropdownFile)
    ON_BN_CLICKED(IDC_CREATEFILE, OnCreatefile)
      ON_BN_CLICKED(IDC_SAVEFILE, OnSavefile)
        ON_BN_CLICKED(IDC_LOADFILE, OnLoadfile)
          ON_CBN_DROPDOWN(IDC_DEFTEXTURE, OnDropdownDeftexture)
            //}}AFX_MSG_MAP
            END_MESSAGE_MAP()
              
              /////////////////////////////////////////////////////////////////////////////
              // CWeightAutoDlg message handlers
              
              void CWeightAutoDlg::OnDropdownFile() 
                {
                // TODO: Add your control notification handler code here
                CFileDialogEx dlg(TRUE);
                if (dlg.DoModal()==IDOK)
                  {
                  w_file.SetWindowText(dlg.GetPathName());
                  }
                w_file.PostMessage(CB_SHOWDROPDOWN,0,0);
                w_file.PostMessage(WM_CANCELMODE,0,0);
                }

//--------------------------------------------------

static CString defgradient="1.000";
static CString defsolid="0.000";
static CString deffilename="";
static int defselect=0;
static bool deffindbones;

BOOL CWeightAutoDlg::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  LODObject *lod=doc->LodData;
  for (int i=0;i<lod->NLevels();i++)
    {
    float g=lod->Resolution(i);
    int p=w_lod.AddString(LODResolToName(g));	
    w_lod.SetItemData(p,i);
    }
  w_lod.SetCurSel(defselect);
  SetDlgItemText(IDC_FILE,deffilename);
  SetDlgItemText(IDC_DEFGRAD,defgradient);
  SetDlgItemText(IDC_DEFSOLID,defsolid);
  if (deffindbones) CheckDlgButton(IDC_FINDBONES,1);
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

//--------------------------------------------------

void CWeightAutoDlg::OnCreatefile() 
  {
  int p=w_lod.GetCurSel();
  if (p==-1) return;
  int q=w_lod.GetItemData(p);
  LODObject *lod=doc->LodData;
  ObjectData *obj=lod->Level(q);
  CString f1;
  CString f2;
  GetDlgItemText(IDC_DEFSOLID,f1);
  GetDlgItemText(IDC_DEFGRAD,f2);
  float f;
  if (sscanf(f1,"%f",&f)!=1) f1="0";
  if (sscanf(f2,"%f",&f)!=1) f2="1";
  CString ss="";
  defgradient=f2;
  defsolid=f1;
  CString txt;
  GetDlgItemText(IDC_DEFTEXTURE,txt);
  for (int i=0;i<MAX_NAMED_SEL;i++)
    {
    NamedSelection *sel=obj->GetNamedSel(i);
    if (sel)
      {
      ss+=f2+"\t"+f2+"\t"+f2+"\t"+f1+"\t\""+txt+"\"\t"+sel->Name()+"\r\n";
      }
    }
  def.SetWindowText(ss);
  }

//--------------------------------------------------

void CWeightAutoDlg::OnSavefile() 
  {
  GetDlgItemText(IDC_FILE,deffilename);
  if (deffilename=="") return;
  ofstream out(deffilename,ios::out|ios::binary|ios::trunc);
  if (!out)
    {
    AfxMessageBox("Save Error",MB_OK|MB_ICONEXCLAMATION);
    return;
    }
  CString q;
  def.GetWindowText(q);
  out<<q;
  if (!out)
    {
    AfxMessageBox("Save Error",MB_OK|MB_ICONEXCLAMATION);
    return;
    }
  }

//--------------------------------------------------

void CWeightAutoDlg::OnLoadfile() 
  {
  GetDlgItemText(IDC_FILE,deffilename);
  if (deffilename=="") return;
  ifstream in(deffilename,ios::in|ios::binary);
  if (!in)
    {
    AfxMessageBox("Open Error",MB_OK|MB_ICONEXCLAMATION);
    return;
    }
  CString q;
  char c[256];
  in.read(c,255);
  while (in.gcount()!=0)
    {
    c[in.gcount()]=0;
    q=q+c;
    in.read(c,255);
    }
  def.SetWindowText(q);
  }

//--------------------------------------------------

void CWeightAutoDlg::OnOK() 
  {
  level=w_lod.GetCurSel();
  if (level==-1) return;
  defselect=level;
  level=w_lod.GetItemData(level);
  if (doc->LodData->ActiveLevel()==level)
    {
    AfxMessageBox("Vybrals stejny LOD jako je aktualni. Tohle nesmis!");
    return;
    }
  GetDlgItemText(IDC_FILE,deffilename);
  if (deffilename=="") return;
  if (def.GetWindowTextLength()!=0) OnSavefile();
  deffindbones=IsDlgButtonChecked(IDC_FINDBONES)!=0;
  CDialog::OnOK();  
  }

//--------------------------------------------------

struct SelProp
  {
  NamedSelection *sel; //named selection pointer
  float gx,gy,gz; //gradient
  float solid;
  char texture[256];
  };

//--------------------------------------------------

int WeightAuto(LODObject *lods, CString &name, int level, bool findbones)
  {
  ObjectData *src=lods->Level(level);
  ObjectData *obj=lods->Active();
  
  TUNIPICTURE *pic=NULL;
  char texture[256]="...";
  
  comint.Begin(WString("Weight Automation"));
  SelProp proplist[MAX_NAMED_SEL];
  NamedSelection *namlist[MAX_NAMED_SEL];
  Selection normsel(obj);
  int cnt=0;
  int npoc=0;
  ifstream in(name,ios::in);
  if (!in) 
    {AfxMessageBox("Unable to open definition file");return -1;}
  SelProp prop;
  in>>prop.gx>>prop.gy>>prop.gz>>prop.solid;
  while (!(!in))
    {
    char name[256];
    prop.gx=1.0/prop.gx;
    prop.gy=1.0/prop.gy;
    prop.gz=1.0/prop.gz;
    ws(in);
    if (in.get()!='\"') 
      {AfxMessageBox("Excepting name of texture quoted in \"\"");return -1;}
    in.getline(prop.texture,sizeof(prop.texture),'"');
    ws(in);
    in.getline(name,sizeof(name));
    int n=src->FindNamedSel(name);
    if (n!=-1)
      {
      prop.sel=src->GetNamedSel(n);
      proplist[cnt]=prop;
      cnt++;
      }
    in>>prop.gx>>prop.gy>>prop.gz>>prop.solid;
    }
  int i;
  ProgressBar<int> pb(cnt);
  for (i=0;i<cnt;i++)
  {
    pb.AdvanceNext(1);
    SelProp &prop=proplist[i];
    if (stricmp(prop.texture,texture))
      {
      free(pic);
      strncpy(texture,prop.texture,255);
      texture[255]=0;
      TUNIPICTURE *tmp=cmLoadUni(texture);
      if (tmp) pic=cmConvertUni(tmp,"R8",1);else pic=NULL;
      free(tmp);	  
      normsel.Clear();
      for (int p=0;p<obj->NFaces();p++)
        {
        FaceT fc(obj,p);
        for (int q=0;q<fc.N();q++)
          {
          int pt=fc.GetPoint(q);
          float fw;
          if (pic==NULL)
            {
            fw=1.0f;
            }
          else
            {
            float u=fmod(fc.GetU(q),1.0f);
            float v=fmod(fc.GetV(q),1.0f);
            if (u<0.0f) u+=1.0f;
            if (v<0.0f) v+=1.0f;
            int x=(int)(cmGetXSize(pic)*u);
            int y=(int)(cmGetYSize(pic)*v);
            if (x<0) x=0;
            if (y<0) y=0;
            if (x>=(int)cmGetXSize(pic)) x=cmGetXSize(pic)-1;
            if (y>=(int)cmGetYSize(pic)) y=cmGetYSize(pic)-1;
            unsigned char w=*((unsigned char *)cmGetData(pic)+x+y*cmGetXlen(pic));
            fw=(float)w/255.0f;
            }
          float gw=normsel.PointWeight(pt);
          if (fw>gw) normsel.SetPointWeight(pt,fw);
          }
        }
      }
    NamedSelection *ss=new NamedSelection(obj,prop.sel->Name());
    ss->Clear();
    int scnt=src->NPoints();
    int pcnt=obj->NPoints();
    int j;
    bool isbone=findbones;
    HMATRIX bonmx;
    float blen;
    if (isbone)
      {
      int np=prop.sel->NPoints();
      int nf=prop.sel->NFaces();
      if (np!=5 || nf!=5) isbone=false;
      }
    if (isbone)
      {
      int nfc=src->NFaces();
      int i;
      for (i=0;i<nfc;i++) if (prop.sel->FaceSelected(i) && src->Face(i).n==4) break;
      if (i==nfc) isbone=false;
      else 
        {
        int arr[4];
        int j;
        for (j=0;j<4;j++) arr[j]=src->Face(i).vs[j].point;
        VecT z(0,0,0);
        for (j=0;j<4;j++) z+=src->Point(arr[j]);
        z*=0.25f;
        bonmx[3][0]=z[0];
        bonmx[3][1]=z[1];
        bonmx[3][2]=z[2];
        bonmx[3][3]=1.0f;
        for (j=0;j<scnt;j++) if (prop.sel->PointSelected(j))
          {
          int k;
          for (k=0;k<4;k++) if (arr[k]==j) break;
          if (k==4) break;
          }
        if (j==scnt) isbone=false;
        else 
          {
          VecT b=src->Point(j);
          b-=z;
          blen=b.Size();
          b.Normalize();
          VecT c(0,0,1);
          VecT d=c.CrossProduct(b);
          d.Normalize();
          c=d.CrossProduct(b);
          c.Normalize();
          bonmx[2][0]=b[0];
          bonmx[2][1]=b[1];
          bonmx[2][2]=b[2];
          bonmx[2][3]=b[3];
          bonmx[1][0]=c[0];
          bonmx[1][1]=c[1];
          bonmx[1][2]=c[2];
          bonmx[1][3]=c[3];
          bonmx[0][0]=d[0];
          bonmx[0][1]=d[1];
          bonmx[0][2]=d[2];
          bonmx[0][3]=d[3];
          HMATRIX x;
          InverzeMatice(bonmx,x);
          CopyMatice(bonmx,x);
          //		  pt_b=src->Point(j);
          }
        }
      }	
    if (isbone)
      {
      for (j=0;j<pcnt;j++)
        {
        VecT v1=obj->Point(j);
        HVECTOR p;
        TransformVector(bonmx,mxVector3(v1[0],v1[1],v1[2]),p);
        float dg;
        if (p[ZVAL]<0) 
          {
          float xg=p[XVAL]*prop.gx;
          float yg=p[YVAL]*prop.gy;
          float zg=p[ZVAL]*prop.gz;
          dg=(float)sqrt(xg*xg+yg*yg+zg*zg);
          }
        else if (p[ZVAL]>blen)
          {
          float xg=p[XVAL]*prop.gx;
          float yg=p[YVAL]*prop.gy;
          float zg=(p[ZVAL]-blen)*prop.gz;
          dg=(float)sqrt(xg*xg+yg*yg+zg*zg);
          }
        else 
          {
          float xg=p[XVAL]*prop.gx;
          float yg=p[YVAL]*prop.gy;
          dg=(float)sqrt(xg*xg+yg*yg);
          }
        if (dg<prop.solid) 
          ss->SetPointWeight(j,1.0f);
        else if (dg<1.0f)
          {
          float f=(dg-prop.solid)/(1.0f-prop.solid);
          ASSERT(f<=1.0f);
          f=1.0f-f;
          float g=ss->PointWeight(j);		  
          if (f>g) 
            ss->SetPointWeight(j,f);
          }
        }
      }
    else
      {
      for (j=0;j<scnt;j++) if (prop.sel->PointSelected(j))
        {
        PosT &ps1=src->Point(j);
        for (int k=0;k<pcnt;k++)
          {
          PosT &ps2=obj->Point(k);
          float xg=(ps1[0]-ps2[0])*prop.gx;
          float yg=(ps1[1]-ps2[1])*prop.gy;
          float zg=(ps1[2]-ps2[2])*prop.gz;
          float dg=(float)sqrt(xg*xg+yg*yg+zg*zg);
          if (dg<prop.solid) ss->SetPointWeight(k,1.0f);
          else if (dg<1.0f)
            {
            float f=(dg-prop.solid)/(1.0f-prop.solid);
            ASSERT(f<=1.0f);
            f=1.0f-f;
            float g=ss->PointWeight(k);		  
            if (f>g) 
              ss->SetPointWeight(k,f);
            }
          //	else
          //ss->SetPointWeight(k,0);
          }
        }	
      }
    for (j=0;j<pcnt;j++) ss->SetPointWeight(j,ss->PointWeight(j)*normsel.PointWeight(j));
    namlist[npoc++]=ss;
    }
  if (npoc<MAX_NAMED_SEL)
    {
    NamedSelection *ss=new NamedSelection(obj,"BASE");
    ss->Clear();
    int pcnt=obj->NPoints();
    for (int i=0;i<pcnt;i++)
      {
      float suma=0;
      for (int j=0;j<npoc;j++)
        suma+=namlist[j]->PointWeight(i);
      if (suma>1.0f)
        {
        float mult=1.0f/suma;
        for (int j=0;j<npoc;j++)
          namlist[j]->SetPointWeight(i,namlist[j]->PointWeight(i)*mult);
        suma=1.0f;
        }
      ss->SetPointWeight(i,1.0f-suma);
      }
    namlist[npoc++]=ss;
    }
  for (i=0;i<npoc;i++)	  
    comint.Run(new CIDefineNamedSelection(namlist[i]));	
  comint.SetLongOp();
  free(pic);
  return 0;
  }

//--------------------------------------------------

void CWeightAutoDlg::OnDropdownDeftexture() 
  {
  // TODO: Add your control notification handler code here 
  CString name;
  GetDlgItemText(IDC_DEFTEXTURE,name);
  CFileDialogEx dlg(TRUE,NULL,name,OFN_FILEMUSTEXIST,"Bitmapy|*.TGA;*.GIF;*.BMP;*.PAA;*.PAC||");
  if (dlg.DoModal()==IDOK)
    {
    SetDlgItemText(IDC_DEFTEXTURE,dlg.GetPathName());
    }
  m_deftex.PostMessage(CB_SHOWDROPDOWN,0,0);
  m_deftex.PostMessage(WM_CANCELMODE,0,0);
  }

//--------------------------------------------------

