// IUserMapping.h: interface for the CIUserMapping class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IUSERMAPPING_H__FD339542_23F7_4D93_BD48_8E063C5A69B0__INCLUDED_)
#define AFX_IUSERMAPPING_H__FD339542_23F7_4D93_BD48_8E063C5A69B0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ComInt.h"

class CIUserMapping : public CICommand  
  {
  CString values[6];
  bool wrapu,wrapv,onlypoints,onlyfaces;
  CString texture;
  public:
    virtual int Execute(LODObject *obj);
    virtual void Save(ostream &str);
    CIUserMapping(istream& str);
    CIUserMapping(bool wrapu, bool wrapv, bool onlyfaces, bool onlypoints, CString *vals, const char *texname);
    virtual int Tag() const 
      {return CITAG_USERMAPPING;}
    virtual ~CIUserMapping();
    
  };

//--------------------------------------------------

#endif // !defined(AFX_IUSERMAPPING_H__FD339542_23F7_4D93_BD48_8E063C5A69B0__INCLUDED_)
