// Objektiv2.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Objektiv2.h"
#include <afxmt.h>
#include "KeyCheck.h"

#include "MainFrm.h"
#include "Objektiv2Doc.h"
#include "Objektiv2View.h"
#include "TexList.h"

#include "InternalView.h"
#include <strstream>
using namespace std;


#include "oldStuff\pal2pac.hpp"

#include ".\Trap\imexhnd.h"
#include "UnlockSoftDlg.h"
#include "LicenceAgrDlg.h"

#include <mapi.h>
#include "Console.h"
#include <direct.h>

#include <El/Scc/MsSccProject.hpp>
#include ".\objektiv2.h"

#include "ROCheck.h"

#include "OldStuff/g3dPlugins.h"

#include <DbgHelp.h>

#include <El/paramfile/paramFile.hpp>
#include <Es/Algorithms/qsort.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif




/////////////////////////////////////////////////////////////////////////////
// CObjektiv2App

BEGIN_MESSAGE_MAP(CObjektiv2App, CWinApp)
  //{{AFX_MSG_MAP(CObjektiv2App)
  ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
  // NOTE - the ClassWizard will add and remove mapping macros here.
  //    DO NOT EDIT what you see in these blocks of generated code!
  //}}AFX_MSG_MAP
  // Standard file based document commands
  ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
  ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
  // Standard print setup command
  ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CObjektiv2App construction

CObjektiv2App::CObjektiv2App()
{
  // TODO: add construction code here,
  // Place all significant initialization in InitInstance
  startuptoload="";
}










/////////////////////////////////////////////////////////////////////////////
// The one and only CObjektiv2App object

CObjektiv2App theApp;

/////////////////////////////////////////////////////////////////////////////
// CObjektiv2App initialization

CImageList shrilist;

static MsSccLib SccProvider;

UINT _cdecl ProgressControl( LPVOID pParam );
static volatile bool exitprogress=false;
static volatile bool progresrun=true;
static volatile bool debug_pc_waiting=false;	// Debug, true if processcontrol is waiting for msg
void ReportWalk();
void EnableWalk(bool enable);

CStringRes StrRes;

#include <es/framework/appFrame.hpp>

class O2AppFuncts:public AppFrameFunctions
{
  FILE *_logf;
  CString txt;
public:
  O2AppFuncts()
  {
    char buff[MAX_PATH];
    GetModuleFileName(NULL,buff,MAX_PATH);
    Pathname logname=buff;
    logname.SetExtension(".log");
    _logf=fopen(logname,"w");      
  }
  ~O2AppFuncts()
  {
    if (_logf) fclose(_logf);
  }
  void LogF(const char *format, va_list argptr)
  {
    if (_logf)
    {
      struct tm *newtime;
      time_t aclock;
      time( &aclock );   // Get time in seconds
      newtime=localtime(&aclock);
      char buff[200];
      strftime(buff,sizeof(buff),"%H:%M:%S",newtime);      
      fprintf(_logf,"\n%s - [%04X]",buff,GetCurrentThreadId() & 0xFFFF);      
      txt.FormatV(format,argptr);
      fputs(txt,_logf);
      fflush(_logf);
      OutputDebugString(txt);      
      OutputDebugString("\r\n");      
    }        
  }  
}O2Frame;


extern "C" {
extern void *_imp__malloc;
}


typedef BOOL (WINAPI  *MiniDumpWriteDump_Type)(
  HANDLE hProcess,
  DWORD ProcessId,
  HANDLE hFile,
  MINIDUMP_TYPE DumpType,
  PMINIDUMP_EXCEPTION_INFORMATION ExceptionParam,
  PMINIDUMP_USER_STREAM_INFORMATION UserStreamParam,
  PMINIDUMP_CALLBACK_INFORMATION CallbackParam
);

static MiniDumpWriteDump_Type MiniDumpWriteDumpFn;

static DWORD WINAPI SafeMsgBox(LPVOID text)
{
  MessageBox(NULL,(LPCTSTR)text,NULL,MB_OK|MB_SYSTEMMODAL);
  return 0;
}


static bool GenerateMinidump(MINIDUMP_EXCEPTION_INFORMATION *ExceptionInfo, MINIDUMP_TYPE flags,const char *sufix)
{
  char buff[MAX_PATH+50];
  HANDLE hFile;
  GetModuleFileName(NULL,buff,MAX_PATH);
  strcat(buff,sufix);
  hFile=CreateFile(buff,GENERIC_WRITE,0,NULL,CREATE_ALWAYS,0,NULL);
  if (hFile==NULL)
  {
    char *text=(char *)alloca(strlen(buff)+200);
    sprintf(text,"Can't build the file record '%s', please check the write access to the game folder",buff);
    MessageBox(0,text,0,MB_SYSTEMMODAL);
    return false;
  }
  else
  {
    if (MiniDumpWriteDumpFn(GetCurrentProcess(),GetCurrentProcessId(),hFile,flags,ExceptionInfo,NULL,NULL)!=TRUE)
    {
      char *text=(char *)alloca(strlen(buff)+200);
      sprintf(text,"MiniDumpWriteDump call failed. Not possible to generate record to file '%s'",buff);
      MessageBox(0,text,0,MB_SYSTEMMODAL);
      return false;
    }
    CloseHandle(hFile);
  }
  return true;
}

static VOID CALLBACK SetAbortTitle(HWND hwnd,UINT uMsg,UINT_PTR idEvent,DWORD dwTime)
{
  HWND msg=::GetActiveWindow();
  SetDlgItemText(msg,IDCANCEL,_T("Exit"));
  KillTimer(0,idEvent);  
}

static DWORD WINAPI CrashReportGenerateThread(LPVOID data)
{
  MINIDUMP_EXCEPTION_INFORMATION *ExceptionInfo=(MINIDUMP_EXCEPTION_INFORMATION *)data;
  HWND hwnd;
  int res;

  if (MiniDumpWriteDumpFn!=NULL)
  {
    hwnd=CreateWindowEx(WS_EX_TOPMOST,"STATIC","An error happened in the application\r\n\r\nSystem Windows is collecting now all the information about the error and is generating the necessary files.\r\nPlease wait, this operation might take some time...",WS_POPUP|
      WS_DLGFRAME|WS_VISIBLE|SS_CENTER,0,0,640,80,NULL,NULL,GetModuleHandle(NULL),NULL);
    UpdateWindow(hwnd);
    if (GenerateMinidump(ExceptionInfo,MiniDumpWithIndirectlyReferencedMemory,".short.dmp")==false) return 0;
    GenerateMinidump(ExceptionInfo,MiniDumpWithFullMemory,".full.dmp");
    DestroyWindow(hwnd);
  }
  else
  {
    res=MessageBox(0,"An error happened in the application. File DbgHelp.dll is missing, "
      "it is not possible to make a record about the error. ",0,MB_OK|MB_SYSTEMMODAL);
  }
  return 0;
}


static LONG WINAPI CrashReportGenerate(EXCEPTION_POINTERS *ExceptionInfo)
{
  HANDLE msg;

  MINIDUMP_EXCEPTION_INFORMATION nfo;
  nfo.ThreadId=GetCurrentThreadId();
  nfo.ExceptionPointers=ExceptionInfo;
  nfo.ClientPointers=FALSE;

//  CloseAllWindows();

  msg=CreateThread(NULL,0,CrashReportGenerateThread,(void *)&nfo,0,NULL);
  WaitForSingleObject(msg,INFINITE);  
  return 0;
}


BOOL CObjektiv2App::InitInstance()
{
//  _asm int 3;
  /*
  KeyCheckClass checkKey;
  if (checkKey.VerifyKey()==false)
  {
    SetTimer(0,0,500,SetAbortTitle);
    AfxMessageBox(IDS_INVALID_CD_KEY,MB_OK|MB_ICONSTOP);
    exit(-1);
  }

*/
  CurrentAppFrameFunctions=&O2Frame;

  CFileDialogEx::historyInterface=this;
  StrRes.SetInstance(AfxGetInstanceHandle());

  SetUnhandledExceptionFilter(CrashReportGenerate);

  HMODULE lib=LoadLibrary("DbgHelp.dll");
  MiniDumpWriteDumpFn=(MiniDumpWriteDump_Type)GetProcAddress(lib,"MiniDumpWriteDump");

  SccProvider.LoadScc();

  splash.Create();
  splash.UpdateWindow();
  //#ifndef FULLVER
#if 0
  protInitProtection();
  if (protCheckValidity()==false) 
  {
    CUnlockSoftDlg dlg;
    dlg.DoModal();
  }
  if (protCheckExpiration()==true)
  {
    CUnlockSoftDlg dlg;
    dlg.timeexpand=true;
    dlg.DoModal();
  }
#endif
  config=new CConfig();
  AfxEnableControlContainer();
#ifdef _TRACEFUNCT
  EnableWalk(true);
#endif
  // Standard initialization
  // If you are not using these features and wish to reduce the size
  //  of your final executable, you should remove from the following
  //  the specific initialization routines you do not need.

#ifdef _AFXDLL
  //  Enable3dControls();			// Call this when using MFC in a shared DLL
#else
  //  Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

  // Change the registry key under which our settings are stored.
  // You should modify this string to be something appropriate
  // such as the name of your company or organization.
#ifdef APPNAME
  {
    m_pszAppName=strdup(APPNAME);
  }
#endif
  SetRegistryKey(_T("Bohemia Interactive Studio"));	

  AfxInitRichEdit();


#ifdef PUBLIC
  if (theApp.GetProfileInt("CConfig","FirstRun",0)==0)
  {
    CLicenceAgrDlg dlg;
    if (dlg.DoModal()==IDCANCEL) exit(1);
  }
#endif


  LoadStdProfileSettings(10);  // Load standard INI file options (including MRU)
  config->LoadConfig();
  g3dInitPlugins();

  // Register the application's document templates.  Document templates
  //  serve as the connection between documents, frame windows and views.

  shrilist.Create(IDB_ILIST,16,16,RGB(255,0,255));

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(

#ifdef PUBLIC
		IDR_MAINFRAME_PUBLIC,
#else
		IDR_MAINFRAME,
#endif
    RUNTIME_CLASS(CObjektiv2Doc),
    RUNTIME_CLASS(CMainFrame),       // main SDI frame window
    RUNTIME_CLASS(CObjektiv2View));
  AddDocTemplate(pDocTemplate);


  // Enable DDE open
  EnableShellOpen();
  RegisterShellFileTypes(TRUE);
  

  // Parse command line for standard shell commands, DDE, file open
  CCommandLineInfo cmdInfo;
  for (int i=1;i<__argc;i++)
  {
    cmdInfo.ParseParam(__argv[i],FALSE,i+1==__argc);
  }  
  LoadAllPlugins();

  // Dispatch commands specified on the command line
  if (!ProcessShellCommand(cmdInfo))
    return FALSE;

  if (!m_pMainWnd)
  {
    RptF("CObjektiv2App::InitInstance() - No main window created");
    return FALSE;
  }
  // The one and only window has been initialized, so show and update it.
  m_pMainWnd->ShowWindow(SW_SHOW);
  m_pMainWnd->UpdateWindow();
  AfxBeginThread(ProgressControl,NULL);

  // Enable drag/drop open
  m_pMainWnd->DragAcceptFiles();
  ClipFormat1=::RegisterClipboardFormat("SUMA Objektiv 3D object");
  ClipFormat2=::RegisterClipboardFormat("BREDY Objektiv2 3D object");
  InitPal2Pac(config->GetString(IDS_CFGOBJEKTIVDLLS));
  InitScc();
  rocheck.hIcon=theApp.LoadIcon(IDI_LOCKEDICON);
  rocheck.hInstance=AfxGetInstanceHandle();
  rocheck.hWndOwner=*m_pMainWnd;
  
  return TRUE;
}

// ////////////////////////////////////////////////////////////////////////////////////////////////
// Helper functions used in custom menu arrangement based on configuration file
//
struct MenuItemStruct
{
  HMENU	ownerMenu;
  int		pos;

  MenuItemStruct()
    : ownerMenu(0)
    , pos(-1)
  {
  }

  MenuItemStruct( HMENU menu, int position)
    : ownerMenu(menu)
    , pos(position)
  {
  }

  bool operator==(const MenuItemStruct& item)
  {
    if (pos != item.pos) return false;
    if (ownerMenu != item.ownerMenu) return false;
    return true;
  }
};
TypeIsMovable(MenuItemStruct)

typedef AutoArray<MenuItemStruct>	MenuItemStructArray;
typedef FindArray<RStringB>			UserConfigMenuItemsArray;

static int CompareMenuItems(const MenuItemStruct *c1, const MenuItemStruct *c2)
{
  if (c1->ownerMenu != c2->ownerMenu) return c2->ownerMenu - c1->ownerMenu;
  return c2->pos - c1->pos; // Higher index first
}

static void CollectMenuItemsToBeHidden( HMENU inputMenu, MenuItemStructArray& arrayOfRealMenuItemsToBeHidden, RString& menuPath, UserConfigMenuItemsArray& menuItemsToBeHidden)
{
  CMenu* baseMenu = CMenu::FromHandle(inputMenu);
  if (baseMenu == NULL) 
  {
    return;
  }

  for (UINT i = 0; i<baseMenu->GetMenuItemCount(); i++)
  {

    TCHAR szMenuString[512];
    if (::GetMenuString(inputMenu, i, szMenuString, sizeof(szMenuString)/sizeof(TCHAR), MF_BYPOSITION))
    {
      // Make it simple this one
      RString origMenuItemString(szMenuString);
      Ref< CompactBuffer<char> > string = CompactBuffer<char>::New(origMenuItemString.GetLength()+1);
      int cnt = 0;
      for (int j=0; j<origMenuItemString.GetLength(); ++j)
      {
        if (origMenuItemString[j]!='&')
        {
          string->Data()[cnt++]=origMenuItemString[j];
        }
      }
      string->Data()[cnt]=0;
      RString correctedMenuItemString(string->Data());
      RString fullMenuPath = menuPath + (menuPath.GetLength()?";":"") + correctedMenuItemString;

      // Go through all menu items that are designed to be hidden and hide them
      for (int m = 0; m < menuItemsToBeHidden.Size(); m++)
      {
        // Consider that the specified string could be just a substring of the menu string
        if (menuItemsToBeHidden[m] == fullMenuPath.Substring(0, menuItemsToBeHidden[m].GetLength()))
        {
          arrayOfRealMenuItemsToBeHidden.Add(MenuItemStruct(inputMenu,i));
        }
      }

      CMenu* subMenu = baseMenu->GetSubMenu(i);
      if (subMenu != NULL)
      {
        CollectMenuItemsToBeHidden(subMenu->GetSafeHmenu(),arrayOfRealMenuItemsToBeHidden,fullMenuPath,menuItemsToBeHidden);
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////

void CObjektiv2App::CorrectApplicationMenuBasedOnConfigFile()
{
  TCHAR buffer[MAX_PATH];
  DWORD dwRet = ::GetCurrentDirectory(MAX_PATH,buffer);

  RString fullNameStr;
  if( dwRet == 0 )
  {
    fullNameStr = RString(".\\","UserMenuConfig.cfg");
  }
  else
  {
    fullNameStr = RString(buffer,"\\UserMenuConfig.cfg");
  }

  Pathname pathName(PathNull);
  pathName.SetPathName(fullNameStr);

  ParamFile paramFile;
  paramFile.Parse(pathName);

  UserConfigMenuItemsArray menuItemsToBeHidden;

  int entriesCount = paramFile.GetEntryCount();
  for (int i = 0; i<paramFile.GetEntryCount(); i++) 
  {
    ParamEntryVal entry = paramFile.GetEntry(i);	
    if (entry.IsClass())
    {
      ParamClassPtr configClass = entry.GetClass();
      RString configClassName = configClass->GetName();

      int configClassItemCount = configClass->GetEntryCount();
      for (int j = 0; j<configClassItemCount; ++j)
      {
        ParamEntryVal menuClassEntry = configClass->GetEntry(j);
        RString menuClassEntryName = menuClassEntry.GetName();

        if (menuClassEntryName == "UserCustomMenuModification" && menuClassEntry.IsClass())
        {
          int menuClassItemCount = menuClassEntry.GetEntryCount();
          for (int k=0; k<menuClassItemCount; ++k)
          {

            ParamEntryVal menuItemClassEntry = menuClassEntry.GetEntry(k);
            RString menuItemClassEntryName = menuItemClassEntry.GetName();

            if (menuItemClassEntryName.Find("MenuItem") != -1 && menuItemClassEntry.IsClass())
            {
              ParamClassPtr menuItemClass = menuItemClassEntry.GetClassInterface();
              RString menuItemClassName = menuItemClass->GetName();

              ParamEntryPtr menuItemNameEntry = menuItemClass->FindEntry("menu");
              RStringB menuItemName;
              if (menuItemNameEntry.GetPointer() != NULL)
              {
                menuItemName = menuItemNameEntry->GetValue();
              }

              ParamEntryPtr menuItemHideOptionEntry = menuItemClass->FindEntry("hide");
              bool hide = false;
              if (menuItemHideOptionEntry.GetPointer() != NULL)
              {
                hide = *menuItemHideOptionEntry;
              }

              if (hide && menuItemName.GetLength())
              {
                menuItemsToBeHidden.AddUnique(menuItemName);
              }


            }

          }
        }

      }
    }

  }
  if (!menuItemsToBeHidden.Size()) return;

  CMenu* menu = frame->GetMenu();
  MenuItemStructArray arrayOfRealMenuItemsToBeHidden;
  CollectMenuItemsToBeHidden(menu->GetSafeHmenu(),arrayOfRealMenuItemsToBeHidden,RString(""),menuItemsToBeHidden);
  if (!arrayOfRealMenuItemsToBeHidden.Size()) return;

  // Sort the list of items to be deleted according to it's ID, the highest ID first (otherwise when deleting items in the next step according to ID, they could point somewhere else)
  QSort(arrayOfRealMenuItemsToBeHidden.Data(), arrayOfRealMenuItemsToBeHidden.Size(), CompareMenuItems);

  for (int i=0; i<arrayOfRealMenuItemsToBeHidden.Size(); ++i)
  {
    const MenuItemStruct& menuItemStruct = arrayOfRealMenuItemsToBeHidden[i];
    ::DeleteMenu(menuItemStruct.ownerMenu,menuItemStruct.pos,MF_BYPOSITION);
  }

  // Force a redraw of the menu bar
  frame->DrawMenuBar();
}

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
  CAboutDlg();

  // Dialog Data
  //{{AFX_DATA(CAboutDlg)
  enum 

#ifdef PUBLIC
  { IDD = IDD_ABOUTBOX_PUBLIC };
#else
  { IDD = IDD_ABOUTBOX };
#endif
  CRichEditCtrl	richtext;
//}}AFX_DATA

  // ClassWizard generated virtual function overrides
  //{{AFX_VIRTUAL(CAboutDlg)
protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual BOOL OnInitDialog();
  //}}AFX_VIRTUAL

  // Implementation
protected:
  DECLARE_MESSAGE_MAP()
};

BOOL CAboutDlg::OnInitDialog() 
{
  CDialog::OnInitDialog();
  
  CLicenceAgrDlg::LoadLicenceToRichText(richtext);
  return FALSE;
}








CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
  //{{AFX_DATA_INIT(CAboutDlg)
  //}}AFX_DATA_INIT	
}










void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CAboutDlg)
  DDX_Control(pDX, IDC_RICHTEXT, richtext);
  //}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
  //{{AFX_MSG_MAP(CAboutDlg)    
  //}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CObjektiv2App::OnAppAbout()
{
  CAboutDlg aboutDlg;
  aboutDlg.DoModal();
}










/////////////////////////////////////////////////////////////////////////////
// CObjektiv2App commands


void LoadPopupMenu(int IDD, CMenu& menu)
{
  protCheckValid();
  CMenu src;
  src.LoadMenu(IDD);
  int cnt=src.GetMenuItemCount();
  menu.CreatePopupMenu();
  CString name;
  for (int i=0;i<cnt;i++)
  {
    int state=src.GetMenuState(i,MF_BYPOSITION);
    src.GetMenuString(i,name,MF_BYPOSITION);
    CMenu *sub=src.GetSubMenu(i);
    int id=src.GetMenuItemID(i);
    src.ModifyMenu(i,MF_BYPOSITION|(state & ~MF_POPUP));
    if (sub!=NULL)	  	  
      menu.AppendMenu(state | MF_POPUP,(UINT)sub->m_hMenu,name);
    else
      menu.AppendMenu(state,id,name);
  }
}










void FSaveString(ostream& os, const char *str)
{
  int l=strlen(str)+1;
  os.write(str,l);
}










void FLoadString(istream& is, char *str, int size)
{
  int l=is.get();
  size--;
  while (size-- && l) 
  {*str++=l;l=is.get();}
  *str++=0;
  while (l) 
  {l=is.get(); if (is.eof()) return;}
}










void MoveWindowRel(HWND hwnd, int xm, int ym, int xsm, int ysm,HDWP dwp)
{
  RECT rc;
  GetWindowRect(hwnd,&rc);
  rc.left+=xm;
  rc.right+=xm;
  rc.top+=ym;
  rc.bottom+=ym;
  rc.right+=xsm;
  rc.bottom+=ysm;
  HWND parent=GetParent(hwnd);
  HWND owner=GetWindow(hwnd,GW_OWNER);
  if (parent && owner!=parent) 
  {
    ScreenToClient(parent,((POINT *)&rc));
    ScreenToClient(parent,((POINT *)&rc)+1);
  }
  DeferWindowPos(dwp,hwnd,NULL,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,SWP_NOZORDER);
}










#define BARSIZES "BarSizes"

void SaveBarSize(CDialogBar& bar)
{
  char text[100];
  sprintf(text,"%d",bar.GetDlgCtrlID());
  theApp.WriteProfileBinary(BARSIZES,text,(LPBYTE)&bar.m_sizeDefault,sizeof(bar.m_sizeDefault));
}










bool LoadBarSize(CDialogBar& bar,int xdef,int ydef)
{
  char text[100];
  SIZE *siz;
  unsigned int s;
  sprintf(text,"%d",bar.GetDlgCtrlID());
  if (theApp.GetProfileBinary(BARSIZES,text,(LPBYTE *)&siz,&s)==TRUE)
  {
    bar.m_sizeDefault=*siz;
    delete siz;
    return true;
  }
  else
  {
    if (xdef) bar.m_sizeDefault.cx=xdef;
    if (ydef) bar.m_sizeDefault.cy=ydef;
    return false;
  }	
}










void SaveWindowSize(CWnd *wnd, const char *name)
{
  WINDOWPLACEMENT wp;
  wp.length=sizeof(wp);
  if (wnd->IsIconic()) wnd->ShowWindow(SW_RESTORE);
  wnd->GetWindowPlacement(&wp);
  theApp.WriteProfileBinary(BARSIZES,name,(LPBYTE)&wp,sizeof(wp));
}










void LoadWindowSize(CWnd *wnd, const char *name)
{
  unsigned int s;
  WINDOWPLACEMENT *wp;
  if (theApp.GetProfileBinary(BARSIZES,name,(LPBYTE *)&wp,&s)==TRUE)
  {
    wnd->SetWindowPlacement(wp);
    delete wp;
  }
}










int FastCreateFace(ObjectData *odata, int cnt, ...)
{
  int idx=odata->ReserveFaces(1);
  FaceT face(odata,idx);
  va_list list;
  va_start(list,cnt);
  for (int i=0;i<cnt;i++)
  {
    face.SetPoint(i,va_arg(list,int));
    face.SetNormal(i,0);
    face.SetUV(i,0,0);
  }
  face.SetN(cnt);
  face.SetTexture("");
  return idx;
}










void GetObjectCenter(bool selection, HVECTOR center, ObjectData *obj)
{
  protCheckValid();
  ObjectData *odata=obj==NULL?doc->LodData->Active():obj;
  int cnt=odata->NPoints();
  int i;
  HVECTOR v1=
  {0,0,0,1};
  int sumacnt=0;
  for (i=0;i<cnt;i++)
    if (!selection || odata->PointSelected(i))
    {
      PosT& pos=odata->Point(i);
      v1[XVAL]+=pos[0];
      v1[YVAL]+=pos[1];
      v1[ZVAL]+=pos[2];
      sumacnt+=1;
    }
    if (sumacnt!=0) NasobVektor(v1,1.0f/sumacnt);
    CopyVektor(center,v1);
}










int CObjektiv2App::ExitInstance() 
{
  RptF("CObjektiv2App::ExitInstance()");
  if ((HWND)splash!=NULL) splash.DestroyWindow();
  delete config;
  RptF("exitprogress=true...");
  exitprogress=true;
  RptF("wait for !progresrun");
  if(debug_pc_waiting)
	  RptF("debug_pc_waiting=true!");
  while (progresrun) Sleep(1000);
  RptF("!progresrun");
#ifdef _TRACEFUNCT
  ReportWalk();
#endif
  RptF("protClose");
  protClose();
  _scc.Free();
  RptF("CObjektiv2App::ExitInstance() done");
  return CWinApp::ExitInstance();
}










const char *AskForPath(HWND hWnd)
{
  BROWSEINFO brw;
  ITEMIDLIST *il;
  static char path[MAX_PATH];

  protCheckValid();
  memset(&brw,0,sizeof(brw));
  brw.hwndOwner=hWnd;
  brw.pszDisplayName=path;
  brw.lpszTitle="";
  brw.ulFlags= BIF_RETURNONLYFSDIRS ; 	
  il=SHBrowseForFolder( &brw );
  if (il==NULL) return NULL;
  SHGetPathFromIDList(il,path);
  if (path[0]==0) return NULL;
  return path;
}










static LRESULT dummydlgproc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  return 0;
}









static CMutex mx;


class O2ProgressBarFunctions:public ProgressBarFunctions
{
public:
  CString progresstext;
  bool textChanged;
  DWORD timeoutbeg;
  DWORD timeoutend;  
  bool enableStop;
  int stopLevel;
  CCriticalSection csect;
  
  O2ProgressBarFunctions():textChanged(true),timeoutbeg(0),timeoutend(0),enableStop(false) {}
  
  virtual void Lock()
  {
    csect.Lock();
  }

  virtual void Unlock()
  {
    csect.Unlock();
    if (enableStop)
    {
      int levels=Length();
      if (levels<stopLevel) enableStop=false;
    }
  }

  virtual void ReportState(int messageId, va_list params)
  {
    if (messageId==PROGRESS_TIMEOUT)
    {
      DWORD tm=va_arg(params,DWORD);
      timeoutbeg=GetTickCount();
      timeoutend=tm+timeoutbeg;
      return;
    }
    else if (messageId==ENABLE_STOP)
    {
      enableStop=true;
      stopLevel=Length();
    }
    else
    {
    CString ids;
    ids.LoadString(messageId);
    progresstext.FormatV(ids,params);
    textChanged=true;
    }
  }

  int GetPos()
  {
    if (timeoutbeg!=timeoutend)
    {
      DWORD w=GetTickCount();
      if (w<timeoutend)
      {
        return (w-timeoutbeg)*1000/(timeoutend-timeoutbeg);
      }
      else
      {
        timeoutbeg=timeoutend=0;
      }
    }
    return toInt(this->ReadResult()*1000);
    
  }

};

#include "DlgInProgress.h"

static O2ProgressBarFunctions progressHandler;


#define	EXDEBUG(x) if(exitprogress) RptF(x);	// TODO: remove, debug junk for exit freeze bug

UINT _cdecl ProgressControl(LPVOID pParam)
{

  HWND frm=frame->m_hWnd;
  int idle=theApp.idlecnt;
  bool cleartext=false;
  progressHandler.SelectGlobalProgressBarHandler(&progressHandler);
  while (!exitprogress)
  {	
	EXDEBUG("exitprogress loop begin");
    Sleep(2000);
	EXDEBUG("exitprogress loop go");
    if (idle==theApp.idlecnt)
    {
	  EXDEBUG("dlgProgress.Create");
      DlgInProgress dlgProgress;
      dlgProgress.Create(dlgProgress.IDD,CWnd::GetDesktopWindow());
      CRect rc1,rc2;
      if (GetWindowRect(frm,&rc1)==FALSE) GetWindowRect(::GetDesktopWindow(),&rc1);
      dlgProgress.GetWindowRect(&rc2);
      rc1.right=rc1.left+(rc1.Size().cx-rc2.Size().cx)/2;
      rc1.bottom=rc1.top+(rc1.Size().cy-rc2.Size().cy)/3;
      dlgProgress.SetWindowPos(NULL,rc1.right,rc1.bottom,0,0,SWP_NOSIZE|SWP_NOZORDER);
      dlgProgress.ShowWindow(SW_SHOW);
      dlgProgress.SetWindowPos(&CWnd::wndTopMost,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
      dlgProgress.SetWindowPos(&CWnd::wndNoTopMost,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
      while (idle==theApp.idlecnt && !exitprogress)
      {
        EXDEBUG("dlgProgress.WaitForNextUpdate()");
		debug_pc_waiting = true;
        dlgProgress.WaitForNextUpdate();	// Waits for win32 window messages to arrive
		debug_pc_waiting = false;
		EXDEBUG("dlgProgress.WaitForNextUpdate() done");
        bool inpr=progressHandler.InProgress();
        dlgProgress.EnableProgress(inpr);
		EXDEBUG("exitprogress stage a");
        if (inpr)
        {
			EXDEBUG("exitprogress inpr?");
          dlgProgress.SetProgress(progressHandler.GetPos()*0.001f);          
          if (progressHandler.enableStop && dlgProgress.IsStopped()) progressHandler.SignalStop();
        }
        if (progressHandler.textChanged)
        {
			EXDEBUG("exitprogress textChanged?");
          dlgProgress.SetDescription(progressHandler.progresstext);
          progressHandler.textChanged=false;
        }
      }
	  EXDEBUG("dlgProgress.DestroyWindow()");
      dlgProgress.DestroyWindow();
    }

/*      DWORD start=GetTickCount()-4000;
      int curvalue=-1;
      DWORD speed=0;
      HWND wnd=CreateDialog(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDD_INPROGRESS),NULL,(DLGPROC)dummydlgproc);
      CRect rc1,rc2;
      if (GetWindowRect(frm,&rc1)==FALSE) GetWindowRect(::GetDesktopWindow(),&rc1);
      GetWindowRect(wnd,&rc2);
      rc1.right=rc1.left+(rc1.Size().cx-rc2.Size().cx)/2;
      rc1.bottom=rc1.top+(rc1.Size().cy-rc2.Size().cy)/3;
      SetWindowPos(wnd,NULL,rc1.right,rc1.bottom,0,0,SWP_NOSIZE|SWP_NOZORDER);
      if (wnd!=NULL)
      {
        CWnd cw;
        cw.Attach(wnd);
        cw.InvalidateRect(NULL,TRUE);
        /*  if (theApp.m_pMainWnd)
        {
        CRect rc1,rc2;
        theApp.m_pMainWnd->GetWindowRect(&rc1);
        cw.GetWindowRect(rc2);
        CSize sz=rc2.Size();
        rc2-=CSize(rc2.left,rc2.top);
        rc2+=CSize(rc1.right-sz.cx-16,rc1.bottom-sz.cy-16);
        cw.MoveWindow(rc2);
        }
        cw.SetWindowPos(&CWnd::wndTopMost ,0,0,0,0,SWP_NOSIZE|SWP_NOMOVE);
        cw.SetWindowPos(&CWnd::wndNoTopMost ,0,0,0,0,SWP_NOSIZE|SWP_NOMOVE);
        cw.ShowWindow(SW_SHOW);
        cw.SetOwner(CWnd::FromHandle(frm));
        cw.UpdateWindow();
        CProgressCtrl progress;
        progress.Attach(GetDlgItem(wnd,IDC_PROGRESS));
        COLORREF cols[4]=
        {RGB(255,0,0),RGB(255,220,0),RGB(0,255,0),RGB(255,255,0)};
        progress.SetRange(0,1000);
        int fakeprogrs=0;
        int fakecur=0;
        int fakemm=10;
        while (idle==theApp.idlecnt && !exitprogress)
        {
          progress.SetPos(progressHandler.InProgress()?progressHandler.GetPos():fakeprogrs);
          if (progressHandler.textChanged)
          {
            cw.SetDlgItemText(IDC_STATICTEXT,progressHandler.progresstext);
            progressHandler.textChanged=false;
          }
/*          CDC pDC;
          HWND flasher=::GetDlgItem(wnd,IDC_FLASHING);
          pDC.Attach(::GetWindowDC(flasher));
          RECT rc;
          GetClientRect(flasher,&rc);
          rc.left+=2;
          rc.top+=2;
          rc.bottom+=2;
          rc.right+=2;
//          pDC.FillSolidRect(&rc,cols[progressHandler.InProgress()?2:0]);
//          ReleaseDC(flasher,pDC.Detach());
          if (idle==theApp.idlecnt) Sleep(100);
          progress.SetPos(progressHandler.InProgress()?progressHandler.GetPos():fakeprogrs);
//          pDC.Attach(::GetWindowDC(flasher));
//          pDC.FillSolidRect(&rc,cols[progressHandler.GetPos()?3:1]);
//          ReleaseDC(flasher,pDC.Detach());
          if (idle==theApp.idlecnt) Sleep(100);
          int pos=progressHandler.GetPos();
          if (pos>1)
          {
            if (curvalue!=pos)
            {
              DWORD curtime=GetTickCount();
              DWORD ellapsed=curtime-start;
              DWORD desetina=speed/100;
              if (curvalue<pos || desetina<ellapsed)
              {
                DWORD perperc=ellapsed/(pos-curvalue);
                if (curvalue==-1) speed=99*perperc;else  speed-=desetina;
                speed+=perperc;
                start=curtime;
              }
              DWORD remain=(speed/1000)*(1000-pos)/100;
              CString text;
              if (remain>60)
                text.Format(IDS_PROGRESSREMAIN,(remain+30)/60);
              else
                text.Format(IDS_PROGRESSREMAINSEC,remain);			  
              cw.SetDlgItemText(IDC_ODHAD,text);
              curvalue=pos;
            }
          }
          cw.UpdateWindow();
          fakecur++;
          fakemm++;
          fakeprogrs=(1000*fakecur)/fakemm;
        }
        progress.SetPos(1000);
        cw.UpdateWindow();
        Sleep(500);
        progress.Detach();
        DestroyWindow(cw.Detach());
      }
    }*/
    idle=theApp.idlecnt;
	EXDEBUG("if progressHandler.InProgress()");
    if (!progressHandler.InProgress()) {progressHandler.progresstext="";progressHandler.textChanged=false;}
	EXDEBUG("exitprogress mx?");
    mx.Lock();
    mx.Unlock();
	EXDEBUG("exitprogress loop end");
  }  
  progresrun=false;
  return 0;
}










/*
UINT ProgressControl( LPVOID pParam )
{
while (!exitprogress)
{
if (frame!=NULL && frame->m_hWnd!=NULL)
{
DWORD res;
LRESULT rr=SendMessageTimeout(*frame,WM_PROGRESSCONTROL,0,0,SMTO_NORMAL,1000,&res);
if (rr==0 && (HWND)theApp.splash==NULL)
{
HWND wnd=CreateDialog(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDD_INPROGRESS),NULL,(DLGPROC)dummydlgproc);
if (wnd!=NULL)
{
CWnd cw;
cw.Attach(wnd);
cw.InvalidateRect(NULL,TRUE);
/*  if (theApp.m_pMainWnd)
{
CRect rc1,rc2;
theApp.m_pMainWnd->GetWindowRect(&rc1);
cw.GetWindowRect(rc2);
CSize sz=rc2.Size();
rc2-=CSize(rc2.left,rc2.top);
rc2+=CSize(rc1.right-sz.cx-16,rc1.bottom-sz.cy-16);
cw.MoveWindow(rc2);
}
cw.SetWindowPos(&CWnd::wndTopMost ,0,0,0,0,SWP_NOSIZE|SWP_NOMOVE);
cw.SetWindowPos(&CWnd::wndNoTopMost ,0,0,0,0,SWP_NOSIZE|SWP_NOMOVE);
cw.ShowWindow(SW_SHOW);
cw.SetOwner(frame);
cw.UpdateWindow();
CProgressCtrl progress;
progress.Attach(GetDlgItem(wnd,IDC_PROGRESS));
COLORREF cols[2]={RGB(255,255,0),RGB(0,0,255)};
progress.SetRange(0,100);
int fakeprogrs=0;
int fakecur=0;
int fakemm=10;
while (rr==0 && !exitprogress)
{
progress.SetPos(progresval==-1?fakeprogrs:progresval);
if (changetext)
{
cw.SetDlgItemText(IDC_STATICTEXT,progresstext);
changetext=false;
}
CDC pDC;
HWND flasher=::GetDlgItem(wnd,IDC_FLASHING);
pDC.Attach(::GetWindowDC(flasher));
RECT rc;
GetClientRect(flasher,&rc);
rc.left+=2;
rc.top+=2;
rc.bottom+=2;
rc.right+=2;
pDC.FillSolidRect(&rc,cols[0]);
rr=SendMessageTimeout(*frame,WM_PROGRESSCONTROL,0,0,SMTO_NORMAL,250,&res);
progress.SetPos(progresval==-1?fakeprogrs:progresval);
pDC.FillSolidRect(&rc,cols[1]);
if (rr==0) rr=SendMessageTimeout(*frame,WM_PROGRESSCONTROL,0,0,SMTO_NORMAL,250,&res);
ReleaseDC(flasher,pDC.Detach());
cw.UpdateWindow();
fakecur++;
fakemm++;
fakeprogrs=(100*fakecur)/fakemm;
}
progress.Detach();
DestroyWindow(cw.Detach());
}
}
}
mx.Lock();
progresval=-1;
progresstext="";
changetext=false;
mx.Unlock();
Sleep(1000);
}
progresrun=false;
return 0;
}
*/


void CloseProgressWindow()
{
  theApp.idlecnt++;
}









void CObjektiv2App::BarsOtherInfo(CFrameWnd *frame,bool save ...)
{
  va_list list;
  va_start(list,save);
  int id=va_arg(list,int);
  while (id!=-1)
  {
    CControlBar *br=frame->GetControlBar(id);
    if (br==NULL) br=(CControlBar *)frame->GetDlgItem(id);
    if (br)
      if(save) SaveBarInfo(id,br);else LoadBarInfo(id,br,frame);
    id=va_arg(list,int);
  }
}










#define BARINFOKEY "BARINFO"


void CObjektiv2App::SaveBarInfo(int id, CControlBar *bar)
{
  char text[1024];
  ostrstream str(text,sizeof(text));  
  str<<"*";
  if (bar->GetStyle() & WS_VISIBLE) str<<'V';
  if (bar->IsDockBar()) str<<'D';
  if (bar->IsFloating()) 
  {
    str<<'F';
    CRect rc;
    bar->GetWindowRect(rc);
    str<<'X'<<rc.left<<'Y'<<rc.top;
  }
  str<<'\0';
  WString w;
  w.Sprintf("%d",id);
  WriteProfileString(BARINFOKEY,w,text);
}










void CObjektiv2App::LoadBarInfo(int id, CControlBar *bar, CFrameWnd *frame)
{
  CString str;
  WString w;
  w.Sprintf("%d",id);
  str=GetProfileString(BARINFOKEY,w,NULL);
  if (str=="") return;
  istrstream ss(str.LockBuffer(),str.GetLength());
  bool visible=false;
  bool floating=false;
  CPoint pos;
  while (!ss.eof())
  {
    char c;
    ss>>c;
    switch(c)
    {
    case 'V':visible=true;break;
    case 'F':floating=true;break;
    case 'X':ss>>pos.x;break;
    case 'Y':ss>>pos.y;break;
    }
  }
  frame->ShowControlBar(bar,visible,TRUE);
  if (floating) frame->FloatControlBar(bar,pos);
}










void CorrectFloatValues(CWnd *dlg, ...) //IDC LIST terminated by ZERO
{
  protCheckValid();
  va_list list;
  va_start(list,dlg);
  int idc=va_arg(list, int);
  CString s;
  while (idc)
  {
    CEdit *edit=(CEdit *)dlg->GetDlgItem(idc);
    if (edit)
    {
      edit->GetWindowText(s);
      DWORD pos=edit->GetSel();	
      int i=s.Find(',');
      if (i!=-1) s.SetAt(i,'.');
      edit->SetWindowText(s);
      edit->SetSel(pos);
    }
    idc=va_arg(list, int);      
  }
}


















const char *DeriveTexPath(const char *texpath, const char *texname)
{
  char *c=(char *)texpath;
  char *d=(char *)texname;
  while (*c && toupper(*c)==toupper(*d)) 
  {c++;d++;}
  if (*c==0) 
  {
    if (*d=='\\') d++;
    return d;
  }
  return NULL;
}










const char *RemovePathPrefix(const char *texname)
{
  const char *q=strrchr(texname,'\\');
  if (q==NULL) return texname;
  else return q+1;
}










#pragma pack (1)
struct TgaHeader
{
  unsigned short sign1,sign2;
  unsigned long unknw1,unknw2;
  unsigned short xsize,ysize;
  char format;
  char unknw3;
} TGAHEADER;










#pragma pack ()

typedef ULONG (FAR PASCAL *MAPISendMailFnc)(
  LHANDLE lhSession, 
  HWND hWnd, 
  lpMapiMessage lpMessage, 
  FLAGS flFlags, 
  ULONG ulReserved
  );

void OpenTGAFile(ostream& out, int xsize, int ysize, int bitedepth)
{
  TgaHeader hdr;
  hdr.sign1=0;
  hdr.sign2=bitedepth==8?3:2;
  hdr.unknw1=hdr.unknw2=0;
  hdr.xsize=xsize;
  hdr.ysize=ysize;
  hdr.format=bitedepth;
  hdr.unknw3=bitedepth==24?0:8;
  out.write((char *)&hdr,sizeof(hdr));
}










void CloseTGAFile(ostream& out)
{
  char sign1[8]=	
  {0,0,0,0,0,0,0,0};
  out.write(sign1,8);
  char sign2[]="TRUEVISION-XFILE.";
  out.write(sign2,strlen(sign2)+1);
}










void SendOrderMessage(HWND hWnd, LONG code1, LONG code2, bool extendonly)
{
  CString s;
  if (extendonly)
    s.Format(IDS_EXTENDOXYGEN,code1,code2);
  else
    s.Format(IDS_ORDEROXYGEN,code1,code2);
  CString subject;
  subject.LoadString(IDS_ORDEROXYGENSUBJ);
  CString to;
  to.LoadString(IDS_ORDEROXYGENTO);

  bool mapiok=true;


  MapiRecipDesc MapiRecp;
  memset(&MapiRecp,0,sizeof(MapiRecp));
  MapiRecp.ulRecipClass = MAPI_TO;
  MapiRecp.lpszName   = "BIStudio";
  MapiRecp.lpszAddress = to.LockBuffer();

  MapiMessage msg;
  memset(&msg,0,sizeof(msg));
  msg.ulReserved=0;
  msg.lpszSubject=subject.LockBuffer();
  msg.lpszNoteText=s.LockBuffer();
  msg.nRecipCount  = 1;
  msg.lpRecips = &MapiRecp;

  HINSTANCE hLib = LoadLibrary("mapi32.dll");
  if (hLib==NULL) mapiok=false;
  MAPISendMailFnc MAPISendMail=NULL;

  if (mapiok) 
  {
    MAPISendMail = (MAPISendMailFnc)GetProcAddress(hLib, "MAPISendMail"); 
    if (MAPISendMail==NULL) mapiok=false;
  }
  if (mapiok)
  {
    try
    {
      char *z=_getcwd(NULL,MAX_PATH);
      DWORD nRes = MAPISendMail(0,hWnd,&msg,MAPI_DIALOG|MAPI_NEW_SESSION,0);
      if (nRes==SUCCESS_SUCCESS) mapiok=true;
      else if (nRes!=MAPI_E_USER_ABORT) mapiok=false;	  
      _chdir(z);
      free(z);
    }
    catch(...)
    {
      mapiok=false;
    }
  }	
  if (!mapiok) 
  {
    AfxMessageBox(IDS_MAPINOTFOUND,MB_OK|MB_ICONSTOP);
    CConsole dlg;
    dlg.consoleText.Format(IDS_CONSOLEMAIL,to,subject,s);
    dlg.DoModal();
    return;	
  };
  if (hLib!=NULL) FreeLibrary(hLib);
  s.UnlockBuffer();
  subject.UnlockBuffer();
  to.UnlockBuffer();
}

BOOL CObjektiv2App::PreTranslateMessage(MSG* pMsg) 
{
  idlecnt++;	
  return CWinApp::PreTranslateMessage(pMsg);
}


const char *ShortPath(const char *pathname)
{
  if (pathname==NULL) return NULL;
  const char *partpath=config->GetString(IDS_CFGPATHFORTEX);
  const char *p=pathname;
  while (toupper(*p)==toupper(*partpath) && *p) {p++;partpath++;}
  if (*partpath==0) return p;
  else return pathname;    
}


const char *ConvertTexture(Pathname &filename)
{
  if (filename.IsNull()) return "";
  if (config->GetBool(IDS_CFGCONVERTGIFTGA))
  {
    const char *ext=filename.GetExtension();
    bool conv=false;
    Pathname old=filename;
    if (_stricmp(ext,".tga")==0) {filename.SetExtension(".paa");conv=true;}
    else if (_stricmp(ext,".gif")==0) {filename.SetExtension(".pac");conv=true;}
    if (conv) 
    {
      if (::UpdateTextureF==NULL)
      {
        AfxMessageBox("Unable to convert texture, pac2pal error!");
      }
      else
        ::UpdateTextureF(filename,old,-1);
    }
  }
  return ShortPath(filename);  
}

static SCCRTN  MsSccTextOut(const char *message,long msg_type)
{
  HANDLE hndl=GetStdHandle(STD_OUTPUT_HANDLE);  
  char *buff=(char *)alloca(strlen(message)+200);
  const char *type="Unknown";
  switch (msg_type)
  {
  case SCC_MSG_INFO: type="Note";break;
  case SCC_MSG_WARNING: type="Warning";break;
  case SCC_MSG_ERROR: type="Error";break;
  case SCC_MSG_STATUS: type="Status";break;
  }
  DWORD wrt;
  int chrs=sprintf(buff,"(SS) %s: %s\r\n",type,message);
  if (hndl)  WriteFile(hndl,buff,strlen(buff),&wrt,NULL);
  LogF(buff);
  return SCC_MSG_RTN_OK;
}

static HWND GetSccActiveWindow(const MsSccProjectBase *project)
{
  HWND hwnd=GetActiveWindow();
  if (hwnd==NULL) hwnd=HWND_MESSAGE;
  return hwnd;
}

void CObjektiv2App::InitScc(bool ask)
{
  if (SccProvider.IsLoaded())
  {
    MsSccProject::OnTextOut=MsSccTextOut;
    MsSccProject *proj=new MsSccProject(SccProvider,NULL,0);    
    proj->SetWindowCallback(GetSccActiveWindow);
    _scc=proj;
    if (SccOpenProject(ask)==false)
    {
      _scc.Free();
      _scc=NULL;
    }
    rocheck.SetScc(_scc);
  }
}

bool CObjektiv2App::SccOpenProject(bool ask)
{
  if (_scc==NULL) return false;
  char *databaseinfo=strcpy((char *)alloca(strlen(config->GetString(IDS_CFGSSDATABASE))+1),config->GetString(IDS_CFGSSDATABASE));
  char *database=databaseinfo;
  if (database[0]==0 && !ask) return false;
  char *userName=strchr(database,'*');
  if (userName) *userName++=0;
  char *projectName=userName?strchr(userName,'*'):NULL;
  if (projectName) *projectName++=0;
  char *localPath=projectName?strchr(projectName,'*'):NULL;
  if (localPath) *localPath++=0;

  if (!ask)
  {
    if (_scc->Open(projectName,localPath,database,userName)) return false;
  }
  else
  {
    if (_scc->ChooseProjectEx(projectName,localPath,database,userName)) return false;
  }
  return true;
}

void CObjektiv2App::LoadAllPlugins()
{
  Pathname dllpath;
  dllpath.SetDirectory(config->GetString(IDS_CFGOBJEKTIVDLLS));
  dllpath.SetFilename("*.dll");
  CFileFind fnd;
  BOOL ok=fnd.FindFile(dllpath);
  PluginGeneralAppSide *plug=new PluginGeneralAppSide;
  
  while (ok)
  {
    ok=fnd.FindNextFile();
    plug->InitFramework(reinterpret_cast<CMainFrame *>(this->m_pMainWnd));
    plug->LoadPlugin(fnd.GetFilePath());
    if (plug->IsLoaded())
    {
      _pluginList.Append(SRef<PluginGeneralAppSide>(plug));
      plug=new PluginGeneralAppSide;
    }
    else
      plug->UnloadPlugin();
  }
  
  delete plug;  
}

void *CObjektiv2App::FindPluginService(ServiceName serviceName)
{
  for (int i=0;i<_pluginList.Size();i++) 
  {
    void *service=_pluginList[i]->GetPlugin().GetService(serviceName);
    if (service) return service;
  }
  return 0;
}



class FunctorPluginPopupHook
{
  IPluginGeneralPluginSide::PopupMenuName mnuName; 
  HMENU hMenu; HWND hWnd;
public:
  FunctorPluginPopupHook(IPluginGeneralPluginSide::PopupMenuName name,HMENU hMenu, HWND hWnd):hMenu(hMenu),hWnd(hWnd),mnuName(name) {}
  bool operator()(SRef<PluginGeneralAppSide> &item) const
  {
    item->GetPlugin().InitPopupMenu(mnuName,hMenu,hWnd);
    return false;
  }
};

void CObjektiv2App::PluginPopupHook(IPluginGeneralPluginSide::PopupMenuName name, HMENU hMenu, HWND hWnd)
{  
  _popupMenus.Clear();
  ForEachPlugin(FunctorPluginPopupHook(name,hMenu,hWnd));
}

class PopupMenuUpdate:public IPluginMenuCommandUpdate
{
  HMENU mnu;
  int id;
public:
  PopupMenuUpdate(HMENU mnu, int id):mnu(mnu),id(id) {}
  virtual void Enable(bool enable)
  {
    EnableMenuItem(mnu,id,enable?MF_ENABLED:MF_GRAYED);
  }
  virtual void SetCheck(bool check)
  {
    CheckMenuItem(mnu,id,check?MF_CHECKED:MF_UNCHECKED);
  }
};

bool CObjektiv2App::AddToPopupMenu(HMENU mnu, const char *menuName, IPluginCommand *command)
{
  int id=_popupMenus.Size()+100;
  ::AppendMenu(mnu,menuName?MF_STRING:MF_SEPARATOR,id,menuName);
  _popupMenus.Append()=Ref<IPluginCommand>(command);
  command->Update(&PopupMenuUpdate(mnu,id));
  return true;
}

bool CObjektiv2App::HookMenuCommand(int cmd)
{
  bool processed;
  if (cmd>=100 && cmd-100<_popupMenus.Size())
  {
    processed=true;
    _popupMenus[cmd-100]->OnCommand();
  }
  else
    processed=false;
  _popupMenus.Clear();
  return processed;
}


void CObjektiv2App::ConsolePrintf(const char *format,...)
{
    HANDLE hndl=GetStdHandle(STD_OUTPUT_HANDLE);  
    if (hndl)
    {
      va_list args;
      va_start(args,format);
      CString text;
      text.FormatV(format,args);
      DWORD wr;
      WriteFile(hndl,text.GetString(),text.GetLength(),&wr,0);
    }
}

void CObjektiv2App::ConsolePrintf(const char *text)
{
  HANDLE hndl=GetStdHandle(STD_OUTPUT_HANDLE);  
  if (hndl)
  {
    DWORD wr;
    WriteFile(hndl,text,strlen(text),&wr,0);

  }
}
void CObjektiv2App::ConsolePrintf(int ids,...)
{
    HANDLE hndl=GetStdHandle(STD_OUTPUT_HANDLE);  
    if (hndl)
    {
      va_list args;
      va_start(args,ids);
      CString format;
      format.LoadString(ids);
      CString text;
      text.FormatV(format,args);
      DWORD wr;
      WriteFile(hndl,text.GetString(),text.GetLength(),&wr,0);
    }
}
static void SaveHistory(CWinApp *app, AutoArray<RString> &paths)
{
  char name[256];

  for (int i=0,cnt=__min(paths.Size(),20);i<cnt;i++)
  {
    sprintf(name,"Path%02d",i);
    app->WriteProfileString("HistoryPaths",name,paths[i]);
  }
}

static void LoadHistory(CWinApp *app, AutoArray<RString> &paths)
{
  char name[256];
  int i=0;

  paths.Clear();
  do 
  {
    sprintf(name,"Path%02d",i);
    i++;
    CString p=app->GetProfileString("HistoryPaths",name,0);
    if (p.GetLength()==0) return;
    paths.Add(RString(p));
  } 
  while(true);
}

void CObjektiv2App::StoreHistoryFolder(const char *folderName)
{
  LoadHistory(this,_historyFolders);
  _historyFolders.Insert(0,RString(folderName));
  for (int i=1;i<_historyFolders.Size();i++)
  {
    if (_stricmp(_historyFolders[i],folderName)==0)
    {
      _historyFolders.Delete(i);
      i--;
    }
  }
  SaveHistory(this,_historyFolders);
}

const char *CObjektiv2App::GetNextHistoryFolder(int index)
{
  if (_historyFolders.Size()==0)
    LoadHistory(this,_historyFolders);
  if (_historyFolders.Size()>index)
  {
    return _historyFolders[index].Data();
  }
  else
  {
    _historyFolders.Clear();
    return 0;
  }
}

HBITMAP CObjektiv2App::GetHistoryBitmap()
{
  AFX_MANAGE_STATE(::AfxGetAppModuleState());
  return LoadBitmap(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDB_FILEDIALOGEXHISTORY));
}
HBITMAP CObjektiv2App::GetHistoryMenuBitmap()
{
  AFX_MANAGE_STATE(::AfxGetAppModuleState());
  return LoadBitmap(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDB_FILEDIALOGEXHISTORYMENU));
}
