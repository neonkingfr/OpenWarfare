// IBackgrndMap.h: interface for the CIBackgrndMap class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IBACKGRNDMAP_H__3780B8E3_7F96_4022_A1E5_680D60C32CDB__INCLUDED_)
#define AFX_IBACKGRNDMAP_H__3780B8E3_7F96_4022_A1E5_680D60C32CDB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ComInt.h"

class CIBackgrndMap : public CICommand  
  {
  struct tuvmap
    {
    float tu[4];
    float tv[4];
    };
  char *texture;
  tuvmap *map;
  int mapsize;
  public:
    void Save(ostream &str);
    CIBackgrndMap(istream& str);
    void SetMapping(int index,int pt, float tu, float tv);
    CIBackgrndMap(int mapsize, const char *texname);
    virtual ~CIBackgrndMap();
    virtual int Execute(LODObject *obj);
    virtual int Tag() const;
  };

//--------------------------------------------------

#endif // !defined(AFX_IBACKGRNDMAP_H__3780B8E3_7F96_4022_A1E5_680D60C32CDB__INCLUDED_)
