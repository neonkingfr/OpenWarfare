// PolygonSelection.h: interface for the CPolygonSelection class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_POLYGONSELECTION_H__2C1AB68A_A28C_4D91_B507_C5475CD37534__INCLUDED_)
#define AFX_POLYGONSELECTION_H__2C1AB68A_A28C_4D91_B507_C5475CD37534__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MAXPOINTSINPOLYGON 300

class CPolygonSelection  
  {
  int count;
  CPoint points[MAXPOINTSINPOLYGON];
  int indexes[MAXPOINTSINPOLYGON];
  public:
    int GetCount() 
      {return count;}
    void DrawAsFan(CDC *pdc);
    void DrawAsStrip(CDC *pdc);
    void Draw(CDC *pdc, bool preview);
    void AddPoint(CPoint& pt,int index=-1);
    void OptimizePoints();
    CPolygonSelection() 
      {Reset();}
    void Reset() 
      {count=0;}
    bool IsEmpty() 
      {return count==0;}
    void ModifyLast(CPoint &pt, int index=-1) 
      {if (count) 
        {points[count-1]=pt;indexes[count-1]=index;}}	
    void RemoveDouble() 
      {while (count>2 && indexes[count-1]==indexes[count-2]) count--;}
    int operator[] (int index) 
      {return indexes[index];}
    operator const int *() {return indexes;}
    void Back()
    {if (count>0)
    {
      RemoveDouble();
      count--;
    }
    }
  protected:
    bool OptimizePointsOnePass();
    static bool PointIsNeed(CPoint *from, CPoint *to, CPoint *test);
  };

//--------------------------------------------------

#endif // !defined(AFX_POLYGONSELECTION_H__2C1AB68A_A28C_4D91_B507_C5475CD37534__INCLUDED_)
