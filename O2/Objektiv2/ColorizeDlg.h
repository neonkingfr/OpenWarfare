#if !defined(AFX_COLORIZEDLG_H__142E25F7_D66B_485E_AB5A_487FDAADA5C8__INCLUDED_)
#define AFX_COLORIZEDLG_H__142E25F7_D66B_485E_AB5A_487FDAADA5C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ColorizeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CColorizeDlg dialog
#define COC_COLORCOUNT 16
#define COC_WEIGHTCOUNT COC_COLORCOUNT

class CColorizeDlg : public CDialog
  {
  // Construction
  COLORREF fillcolor[COC_COLORCOUNT];
  COLORREF drawcolor[COC_COLORCOUNT];
  CPen pens[COC_COLORCOUNT];
  CColorDialog coldlg;
  static CPen weightPens[COC_WEIGHTCOUNT];
  static bool inited;
  bool ending;
  
  public:
    COLORREF _declspec(deprecated) GetColorizeColor(FaceT &fc)
    {
      return GetColorizeFillColor(fc);
    }
    COLORREF GetColorizeFillColor(FaceT &fc);
    COLORREF GetColorizeLineColor(FaceT &fc);
    CPen * GetColorizePen(FaceT &fc);
    static CPen *WeightColor(float weight);
    void ColorizeSelection(ObjectData *obj, int idx);
    CColorizeDlg(CWnd* pParent = NULL);   // standard constructor
    
    // Dialog Data
    //{{AFX_DATA(CColorizeDlg)
    enum 
      { IDD = IDD_COLORS };
    // NOTE: the ClassWizard will add data members here
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CColorizeDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CColorizeDlg)
    afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
    virtual BOOL OnInitDialog();
    afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
    afx_msg void OnColorClick(UINT cmd);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORIZEDLG_H__142E25F7_D66B_485E_AB5A_487FDAADA5C8__INCLUDED_)
