// ITexMerge.h: interface for the CITexMerge class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ITEXMERGE_H__829C1C25_B1F4_4701_B170_02641B2898A3__INCLUDED_)
#define AFX_ITEXMERGE_H__829C1C25_B1F4_4701_B170_02641B2898A3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ComInt.h"

class CITexMerge : public CICommand  
  {
  CString script;
  bool alllods;
  public:
    struct MatchInfo {

        RString _texture;
        bool _face;
        bool _rvmat;

        ClassIsMovable(MatchInfo);

        MatchInfo() {}
        MatchInfo (RString texture, bool face, bool rvmat):
        _texture(texture),_face(face), _rvmat(rvmat) {}
    };

    virtual int Execute(LODObject *lod);
    CITexMerge(const char *script, bool alllods,AutoArray<MatchInfo> *matchInfo = 0);// {};
    

  mutable AutoArray<MatchInfo> *matchInfo;
  
  };










#endif // !defined(AFX_ITEXMERGE_H__829C1C25_B1F4_4701_B170_02641B2898A3__INCLUDED_)
