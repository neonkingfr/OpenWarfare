#ifndef _express_hpp
#define _express_hpp

#define NVar ( 'z'-'a'+1 )

enum EvalError
  {
  EvalOK,
  EvalGen, // generic error
  EvalExpo, // exponent out of range or invalid
  EvalNum, // invalid number
  EvalVar, // undefined variable
  EvalDivZero, // zero divisor
  EvalTg90, // tg 90
  EvalOpenB, // missing (
  EvalCloseB, // missing )
  EvalOper, // unknown operator
  };

//--------------------------------------------------

const char *Evaluate( const char *source, EvalError &error, int index=NVar );
void SetVariable( int index, double value );

#endif

