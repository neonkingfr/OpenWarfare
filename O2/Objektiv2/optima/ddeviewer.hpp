#ifndef _DDEVIEWER_HPP
#define _DDEVIEWER_HPP

#include "../TraditionalInterface/LODObject.h"

bool StartViewer
  (
  HWND hWnd, LODObject *obj, const char *exePath, float lodBias, bool twoMon
  );
bool UpdateViewer( HWND hWnd, LODObject *obj, float lodBias );
void CloseViewer( HWND hWnd, bool closeapp );
bool IsViewerRunning();
void SwitchToViewer();
bool ViewerHasFocus();

bool ViewerStartScript(const char *scriptname);

#endif

