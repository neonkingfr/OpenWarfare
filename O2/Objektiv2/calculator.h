#if !defined(AFX_CALCULATOR_H__AAB7B8D0_3D8C_4973_B004_3360D6BCBC13__INCLUDED_)
#define AFX_CALCULATOR_H__AAB7B8D0_3D8C_4973_B004_3360D6BCBC13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Calculator.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCalculator dialog


class CSchemes
  {
  Pathname profname;
  public:
    struct HashEntry
      {
      char name[32];
      int type;
      CString calc[7];
      public:
        static int GetSizeOfKey();
        void Save(ostream& str);
        void Load(istream& str);
        void SetKey(int type, const char *name);
      };
    bool loaded;
    void RemoveScheme(int type, const char *name);
    HashEntry *GetScheme(int type, const char *name);
    HashEntry *Enum(HashEntry *last) 
      {return (HashEntry *)Hash_EnumData(&hash,(void *)last);}
    void AddScheme(HashEntry *scheme);
    static char HashDestructor(void *pt);
    THASHTABLE hash;
    CSchemes();
    bool LoadSchemes();
    void SaveSchemes();
    virtual ~CSchemes();
  };

//--------------------------------------------------

extern CSchemes schemes;

class CCalculator : public CDialog
  {
  
  // Construction
  public:
    virtual  ~CCalculator();
    CCalculator(CWnd* pParent = NULL);   // standard constructor
    CSize dlgsize;
    int curschemetype;	//scheme selector;
    
    // Dialog Data
    CStatic	wndValueName[7];
    CEdit	wndValue[7];
    float	Value[7];
    CString	Calc[7];
    //{{AFX_DATA(CCalculator)
    enum 
      { IDD = IDD_CALCULATOR };
    CListBox	list;
    CString	SchemeName;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CCalculator)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    void SelectScheme(const char *name);
    void LoadSchemesToLB(const char *active);
    
    // Generated message map functions
    //{{AFX_MSG(CCalculator)
    virtual BOOL OnInitDialog();
    afx_msg void OnDefine();
    afx_msg void OnSelchangeList();
    afx_msg void OnSave();
    afx_msg void OnNew();
    afx_msg void OnRemove();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALCULATOR_H__AAB7B8D0_3D8C_4973_B004_3360D6BCBC13__INCLUDED_)
