#include "StdAfx.h"
#include "gePlane.h"
#include "../ObjektivLib/Edges.h"
#include "resource.h"
#include "Objektiv2Doc.h"
#include "../ObjektivLib/ObjToolTopology.h"
#include "../ObjektivLib/ObjToolConvex.h"

#include ".\ifindnonconvexpoints.h"

CIFindNonConvexPoints::CIFindNonConvexPoints(void)
  {
  recordable=false;
  }

CIFindNonConvexPoints::~CIFindNonConvexPoints(void)
  {
  }


static void CreateFaceFaceGraph(ObjectData *obj, CEdges &edges)
  {
  CEdges helper;
  int i;
  helper.SetVertices(obj->NPoints());
  edges.SetVertices(obj->NFaces());
  for (i=0;i<obj->NFaces();i++) if (obj->FaceSelected(i))
    {
    FaceT fc(obj,i);    
    for (int a=0;a<fc.N();a++) helper.SetEdge(fc.GetPoint(a),i);
    }
  int a,b;
  for (i=0;i<obj->NFaces();i++) if (obj->FaceSelected(i))
    {
    FaceT fc(obj,i);
    b=fc.N()-1;
    for (a=0;a<fc.N();b=a++)
      {
      int j,sib,pta=fc.GetPoint(a),ptb=fc.GetPoint(b);
      for (j=0;(sib=helper.GetEdge(pta,j))!=-1;j++) if (sib!=i)
        {
        if (helper.IsEdgeInDirection(ptb,sib))
          if (!edges.IsEdge(i,sib)) edges.SetEdge(i,sib);
        }
      }
    }
  }


static void CreateNonConvexEdges(ObjectData *obj, CEdges &faceface, CEdges &nonconvex, bool selectedges)
  {
  int f1,f2,i;
  nonconvex.SetVertices(selectedges?obj->NPoints():obj->NFaces());
  for (f1=0;f1<obj->NFaces();f1++)
    {
    FaceT fc1(obj,f1);
    geVector4 pt(obj->Point(fc1.GetPoint(0)));
    gePlaneGen fc1plane(pt,geVector4(fc1.CalculateNormal()));
    for (i=0;(f2=faceface.GetEdge(f1,i))!=-1;i++) if (f1!=f2)
      {
      bool removeface=true;
      FaceT fc2(obj,f2);
      for (int j=0;j<fc2.N();j++) 
        {
        pt=obj->Point(fc2.GetPoint(j));
        if (fc1plane.Distance(pt)<-0.00001)
          {
          if (selectedges)
            {
            int a=-1,b=-1;
            for (int u=0;u<fc1.N();u++)
              for (int v=0;v<fc2.N();v++)
                if (fc1.GetPoint(u)==fc2.GetPoint(v))
                  if (a!=-1) 
                    {
                    b=fc1.GetPoint(u);
                    goto jumpout;
                    }
                  else
                    a=fc1.GetPoint(u);
  jumpout:  nonconvex.SetEdge(a,b);
            }
          else 
            if (!nonconvex.IsEdge(f1,f2)) 
              {
              nonconvex.SetEdge(f1,f2);
              break;
              }
          }
        }
      }
    }
  }

static void SetSharpEdges(ObjectData *obj,const CEdges &sharp)
  {
  obj->RemoveAllSharpEdges();
  obj->ClearSelection();
  for (int i=0;i<obj->NPoints();i++)
    {
    int a,b,j;
    a=i;
    for (j=0;(b=sharp.GetEdge(a,j))!=-1;j++)
      {
      obj->AddSharpEdge(a,b);
      obj->PointSelect(a);
      obj->PointSelect(b);
      }
    }
  }

static bool FaceTestSelOnly(ObjectData &obj, int faceindex, void *context)
  {
  return obj.FaceSelected(faceindex);
  }

int CIFindNonConvexPoints::Execute(LODObject * lodobj)
  {
  ObjectData *obj=lodobj->Active();
    /*
  CEdges edges(obj->NPoints());
  CEdges nonconvex(obj->NPoints());

  obj->BuildFaceGraph(edges,nonconvex,FaceTestSelOnly);
//  CreateFaceFaceGraph(obj,edges);

  CreateNonConvexEdges(obj,edges,nonconvex,true);

  SetSharpEdges(obj,nonconvex);
  */
  obj->GetTool<ObjToolTopology>().CheckConvexity(true);
  return 0;
  }

CIMakeConvex::CIMakeConvex(void)
  {
  recordable=false;
  }

CIMakeConvex::~CIMakeConvex(void)
  {
  }

static bool TurnEdges(ObjectData *obj, int a, int b)
  {
  FaceT fc1(obj,a),fc2(obj,b);
  int a1,a2,b1,b2;
  
  for(a1=2,a2=0;a2<3;a1=a2++)
    {
    for (b1=2,b2=0;b2<3;b1=b2++)
      {
      if (fc1.GetPoint(a1)==fc2.GetPoint(b2) && fc1.GetPoint(a2)==fc2.GetPoint(b1)) goto jumpout;
      }
    }
  return false;
jumpout:
  
  int a3=a2==2?0:a2+1;
  int b3=b2==2?0:b2+1;

  int pt1=fc1.GetPoint(a3);
  int pt2=fc2.GetPoint(b3);

  if (fc1.GetPoint(a1)==pt2 || fc1.GetPoint(a3)==pt2) return false;
  if (fc2.GetPoint(b1)==pt1 || fc2.GetPoint(b3)==pt1) return false;

  fc1.SetPoint(a2,pt2);
  fc2.SetPoint(b2,pt1);
  return true;
  }


static void Delete2sidedEdges(ObjectData *obj)
  {
  Selection sel(obj);
  CEdges etest;
  etest.SetVertices(obj->NPoints());
  int i;
  for (i=0;i<obj->NFaces();i++) if (obj->FaceSelected(i))
    {
    int j,k,cmp1,cmp2=-1;
    FaceT fc(obj,i);
    int pt1=fc.GetPoint(0);
    int pt2=fc.GetPoint(1);
    int pt3=fc.GetPoint(2);
    for (j=0;(cmp1=etest.GetEdge(pt1,j))!=-1;j++)
      {
      for (k=0;(cmp2=etest.GetEdge(pt2,k))!=-1;k++) 
        if(cmp1==cmp2) break;
      if (cmp2!=-1) break;
      }
    if (cmp1!=-1) 
      for (j=0;(cmp2=etest.GetEdge(pt3,j))!=-1;j++) 
        if (cmp1==cmp2) break;
    if (cmp2!=-1)
      {
      sel.FaceSelect(cmp2);
      sel.FaceSelect(i);
      }
    else
      {
      etest.SetEdge(pt1,i);
      etest.SetEdge(pt2,i);
      etest.SetEdge(pt3,i);
      }
    }
  for (i=obj->NFaces()-1;i>=0;i--)
    if (sel.FaceSelected(i)) obj->DeleteFace(i);
  }

static void DeleteIsolatedPoints(ObjectData *obj)
  {
  Selection sel(obj);
  int i;
  for (i=0;i<obj->NFaces();i++)
    {
    FaceT fc(obj,i);
    for(int j=0;j<fc.N();j++) sel.PointSelect(fc.GetPoint(j),true);
    }
  for (i=obj->NPoints()-1;i>=0;i--)
    if (!sel.PointSelected(i)) obj->DeletePoint(i);
  }

int CIMakeConvex::Execute(LODObject *lod)
  {
           /*
      ObjectData *obj = lod->Active();
  obj->Triangulate();
  CEdges edges;
  CEdges nonconvex;
  bool anyedge;
  do
    {
    edges.Clear();
    nonconvex.Clear();

    CreateFaceFaceGraph(obj,edges);

    CreateNonConvexEdges(obj,edges,nonconvex,false);

    anyedge=false;
    int a,b,i;
    for (a=0;a<obj->NFaces();a++)
      for (i=0;(b=nonconvex.GetEdge(a,i))!=-1;i++)
        {    
        if (TurnEdges(obj,a,b)==true)
          {
          anyedge=true;          
          }
        }
    if (anyedge) 
      {
      Delete2sidedEdges(obj);
   /*   doc->UpdateAllViews(NULL);
      AfxMessageBox("Step");      *//*
      }
    }
  while (anyedge);
  obj->Squarize();
  DeleteIsolatedPoints(obj);
  RecalcNormals2(obj,false);
  */
  return 0;
  }