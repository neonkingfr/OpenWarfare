#if !defined(AFX_TRANSFORM2D_H__DE142406_5E97_4C8F_8565_F62454C611B6__INCLUDED_)
#define AFX_TRANSFORM2D_H__DE142406_5E97_4C8F_8565_F62454C611B6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Transform2D.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTransform2D dialog

class CObjektiv2View;
class CTransform2D : public CDialog
  {
  // Construction
  public:
    enum emode 
      {em_rotate,em_scale};
    int idctext;	
    emode mode;
    CObjektiv2View *toview;
    CTransform2D(CWnd* pParent = NULL);   // standard constructor
    // Dialog Data
    //{{AFX_DATA(CTransform2D)
    enum 
      { IDD = IDD_2DTRANSFORM };
    BOOL	UsePin;
    float	Value;
    //}}AFX_DATA
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CTransform2D)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    
    // Generated message map functions
    //{{AFX_MSG(CTransform2D)
    virtual BOOL OnInitDialog();
    afx_msg void OnPreview();
    afx_msg void OnDestroy();
    afx_msg void OnTimer(UINT nIDEvent);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRANSFORM2D_H__DE142406_5E97_4C8F_8565_F62454C611B6__INCLUDED_)
