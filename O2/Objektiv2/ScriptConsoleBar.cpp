// ScriptConsoleBar.cpp : implementation file
//

#include "stdafx.h"
#include "Objektiv2.h"
#include "ScriptConsoleBar.h"
#include ".\scriptconsolebar.h"

#define SC_INCOMEDATA (WM_APP+10)

// CScriptConsoleBar

IMPLEMENT_DYNAMIC(CScriptConsoleBar, CDialogBar)
CScriptConsoleBar::CScriptConsoleBar()
{
memset(&textfont,0,sizeof(textfont));
}

CScriptConsoleBar::~CScriptConsoleBar()
{
_inputEnabled=false;
}


BEGIN_MESSAGE_MAP(CScriptConsoleBar, CDialogBar)
  ON_WM_SIZE()
  ON_WM_DESTROY()
  ON_MESSAGE(SC_INCOMEDATA,OnIncomeData)
END_MESSAGE_MAP()



// CScriptConsoleBar message handlers

UINT CALLBACK CScriptConsoleBar::ConsoleReadThread(LPVOID self)
{
  CScriptConsoleBar *thisptr=(CScriptConsoleBar *)self;
  char buff[256];
  static CString defText;
  if (defText.GetLength()==0) defText.LoadString(IDS_CONSOLEDATAHASBEENLOST);
  DWORD readed;
  do
  {
    ReadFile(thisptr->_pipeIn,buff,sizeof(buff)-2,&readed,NULL);
    if (readed)
    {
       buff[readed]=0;
       buff[readed+1]=0;       
       DWORD_PTR result;
       if (!SendMessageTimeout(*thisptr,SC_INCOMEDATA,readed,(LPARAM)(buff),SMTO_ABORTIFHUNG|SMTO_BLOCK,500,&result))
        thisptr->PostMessage(SC_INCOMEDATA,readed,(LPARAM)(strdup(buff)));
    }
  }
  while (readed!=0);  
  return 0;
}

UINT CALLBACK CScriptConsoleBar::ConsoleWriteThread(LPVOID self)
{
  CScriptConsoleBar *thisptr=(CScriptConsoleBar *)self;
  DWORD wr;
  thisptr->_writePipe.Lock();
  WriteFile(thisptr->_pipeOut,(void *)((LPCTSTR)(thisptr->_writeText)),thisptr->_writeText.GetLength(),&wr,NULL);
  thisptr->_writePipe.Unlock();
  return 0;
}



void CScriptConsoleBar::AttachIO()
{
  CreatePipe(&_stdIn,&_pipeOut,NULL,0);
  CreatePipe(&_pipeIn,&_stdOut,NULL,0);
  SetStdHandle(STD_INPUT_HANDLE,_stdIn);
  SetStdHandle(STD_OUTPUT_HANDLE,_stdOut);
  SetStdHandle(STD_ERROR_HANDLE,_stdOut);
  AfxBeginThread((AFX_THREADPROC)ConsoleReadThread,this);
}

void CScriptConsoleBar::DetachIO()
{
  if (GetStdHandle(STD_INPUT_HANDLE)==_stdIn) SetStdHandle(STD_INPUT_HANDLE,0);
  if (GetStdHandle(STD_OUTPUT_HANDLE)==_stdOut) SetStdHandle(STD_OUTPUT_HANDLE,0);
  if (GetStdHandle(STD_ERROR_HANDLE)==_stdOut) SetStdHandle(STD_ERROR_HANDLE,0);
  CloseHandle(_stdIn);
  CloseHandle(_stdOut);
  CloseHandle(_pipeOut);
  CloseHandle(_pipeIn);
  _pipeIn=0;
  _pipeOut=0;
  _stdIn=0;
  _stdOut=0;
}

void CScriptConsoleBar::Create(CWnd *parent, int menuid)
{
  CDialogBar::Create(parent,IDD,CBRS_RIGHT,menuid);
  SetBarStyle(GetBarStyle()| CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
  EnableDocking(CBRS_ALIGN_ANY);
  CString top;top.LoadString(IDS_SCRIPTCONSOLE);
  SetWindowText(top);
  _consoleWindow.SubclassDlgItem(IDC_OUTCONSOLE,this);
  _consoleWindow.LimitText(0xFFFF);
  LoadBarSize(*this,0,0);
  AttachIO();
}



void CScriptConsoleBar::OnSize(UINT nType, int cx, int cy)
{
  CDialogBar::OnSize(nType, cx, cy);
  if (_consoleWindow.GetSafeHwnd())
    _consoleWindow.MoveWindow(5,5,cx-10,cy-10);
}



BOOL CScriptConsoleBar::PreTranslateMessage(MSG* pMsg)
{
  if (_inputEnabled && pMsg->hwnd==_consoleWindow && pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN)
  {    
    if (TryEnterCriticalSection(&_writePipe.m_sect)==TRUE)
    {
      int from,to;      
      _consoleWindow.GetSel(from,to);
      if (from==to)
      {
        if (from>=_lastOutput)
        {
          from=_lastOutput;
          to=_consoleWindow.GetWindowTextLength();
        }
        else
        {
          from=_consoleWindow.LineIndex();
          to=from+_consoleWindow.LineLength();
        }
      }
      char eoln[]="\r\n";
      if (from<to) 
        {
        CString text;
        _consoleWindow.GetWindowText(text);
        _writeText=text.Mid(from,to-from);
        if (text[to-1]!='\n') _writeText+=eoln;
        }
      else
        _writeText=eoln;
      OnIncomeData(2,(LPARAM)eoln);
      AfxBeginThread((AFX_THREADPROC)ConsoleWriteThread,(LPVOID)this);
      _writePipe.Unlock();
      return TRUE;
    }
    else
      AfxMessageBox(IDS_INPUTPIPEFULL);
  }
  if (IsDialogMessage(pMsg)) return TRUE;
  return CDialogBar::PreTranslateMessage(pMsg);
}

void CScriptConsoleBar::OnDestroy()
{
  DetachIO();
  SaveBarSize(*this);
  CDialogBar::OnDestroy();
}

LRESULT CScriptConsoleBar::OnIncomeData(WPARAM wParam, LPARAM lParam)
{
  bool freemsg=!InSendMessage();
  int l=_consoleWindow.GetWindowTextLength();
  while (l+wParam>65000)
  {
    int cntlines=_consoleWindow.GetLineCount();
    int li=_consoleWindow.LineIndex(cntlines/3+1);
    _consoleWindow.SetSel(0,li,TRUE);
    _consoleWindow.ReplaceSel("");
    l=_consoleWindow.GetWindowTextLength();    
  }
  char *consoleText=(char *)lParam;
  _consoleWindow.SetSel(l,l);
  _consoleWindow.ReplaceSel(consoleText);
  if (_inputEnabled && !IsVisible()) ((CFrameWnd *)theApp.m_pMainWnd)->ShowControlBar(this,TRUE,FALSE);
  _lastOutput=_consoleWindow.GetWindowTextLength();
  if (freemsg) delete [] consoleText;
  return 0;
}

void CScriptConsoleBar::SetTextFont(const LOGFONT &font)
{
  if (curfont.m_hObject) 
  {
    _consoleWindow.SetFont(NULL,FALSE);
    curfont.DeleteObject();
  }
  curfont.CreateFontIndirect(&font);
  _consoleWindow.SetFont(&curfont,TRUE);
  textfont=font;
}
void CScriptConsoleBar::EnableInput(bool enable)
{
  if (enable==false && _inputEnabled==true)
  {
    DWORD avail;
    PeekNamedPipe(_stdIn,NULL,NULL,NULL,&avail,NULL);
    while (avail)
    {
      char *buff=new char[avail];
      ReadFile(_stdIn,buff,avail,&avail,NULL);
      delete [] buff;
      Sleep(200);
      PeekNamedPipe(_stdIn,NULL,NULL,NULL,&avail,NULL);
    }
  }
  _inputEnabled=enable;
}
