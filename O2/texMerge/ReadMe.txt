Priklad postupu:

X:\data3d\dum_mesto2.p3d

Texture folder:
x:\data (kde jsou textury, ktere se budou slucovat)

Merged folder
x:\data (kam se ulozi sloucena textura)

File new Ctrl N.

Save as:
x:\data\merge\dum_mesto2.ptm

Nahazet vsechny textury rucne bez opakovacich, pro alphove zvlastni sada (Set -> New).

F7 auto arrange

Rucne zmensit textury, Zvolim vybranou, dam Item -> Shrink

--
Save

--
Export:
x:\data
---

.paa -> dum_mesto2_me.pac
