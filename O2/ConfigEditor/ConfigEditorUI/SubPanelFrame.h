#pragma once


// SubPanelFrame

class SubPanelFrame : public CWnd
{
	DECLARE_DYNAMIC(SubPanelFrame)

  COLORREF _bgcolor;
  COLORREF _brdcolor;
  COLORREF _shadowcolor;  
  int _shadowAlpha;
  int _shadowSize;
public:
	SubPanelFrame();
	virtual ~SubPanelFrame();


  void SetBkColor(IN COLORREF col) {_bgcolor=col;}
  void SetBorderColor(IN COLORREF col) {_brdcolor=col;}
  void SetShadowColor(IN COLORREF col) {_shadowcolor=col;}
protected:
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnPaint();  
};



