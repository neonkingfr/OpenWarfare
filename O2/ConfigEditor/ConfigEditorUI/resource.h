//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ConfigEditorUI.rc
//
#define IDS_OPENCONFIGFILTER            1
#define IDS_OPENCONFIGTITLE             2
#define IDS_CONFIGLOADERROR             3
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_ConfigEditorUITYPE          129
#define IDR_GRAPHMENU                   130
#define IDB_HISTORYMENUBITMAP           131
#define IDB_HISTORYBITMAP               132
#define ID_GRAPHMENU_SWAPAXIS           32771
#define ID_FILE_SAVEAS                  32775
#define ID_NEW_EMPTYFRAME               32777
#define ID_VIEW_NEWWINDOW               32778
#define ID_FILE_SOURCECONTROL           32779
#define ID_SOURCECONTROL_OPENFROMSOURCECONTROL 32780
#define ID_SOURCECONTROL_CHECKOUT       32781
#define ID_SOURCECONTROL_CHECKIN        32782
#define ID_SOURCECONTROL_GETLATESTVERSION 32783
#define ID_SOURCECONTROL_COMPARE        32784
#define ID_SOURCECONTROL_HISTORY        32785
#define ID_SOURCECONTROL_UNDOCHECKOUT   32786
#define ID_EDIT_RELOADDEPENDENCIES      32787
#define ID_VIEW_EXTERNALPARTS           32788
#define ID_VIEW_SHOWDERIVEDITEMS        32789
#define ID_VIEW_REFRESH                 32790
#define ID_VIEW_RELOADDEPENDENCIES      32791

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32792
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
