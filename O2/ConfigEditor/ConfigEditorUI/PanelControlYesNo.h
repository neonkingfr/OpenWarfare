#pragma once
#include "panelcontrolwithlabel.h"
#include "ButtonWithNotify.h"

namespace LindaStudio
{

  class IPanelControlYesNoNotify
  {
  public:
    virtual void OnValueChange(bool newValue)=0;
    virtual void DeleteControl()=0;
  };

  class PanelControlYesNo :  public PanelControlWithLabel, public IButtonNotify
  {
    CButtonWithNotify _button;
    IPanelControlYesNoNotify *_notify;
  public:
    PanelControlYesNo(DocumentBase<IPanelControlMsgs> *document, const char *labelText,IPanelControlYesNoNotify *notify);
    ~PanelControlYesNo(void);

    void OnDocumentDropped();

    virtual bool GetPreferredSize(int &xs, int &ys);
    virtual bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys);
    virtual bool SetEnable(IPanelControlMsgs *what, bool newState);
    virtual bool SetVisible(IPanelControlMsgs *what, bool newState);
    virtual bool GoNextControl(IPanelControlMsgs *from, bool previous=false);  
    virtual HWND GetWindow() const;
 
    virtual void OnButtonPressed();
    virtual void ArrowPressed(int code);
    virtual bool SetFont(HFONT font) {return PanelControlWithLabel::SetFont(font);}    

    void SetCheck(bool check)
    {
      _button.SetCheck(check?1:0);
    }
    bool GetCheck() const
    {
      return _button.GetCheck()!=0;
    }

    virtual IPanelControlMsgs *WhoseWindow(HWND window)
    {
      if (window==_button) return this;
      else return PanelControlWithLabel::WhoseWindow(window);
    }


  };

}