#include "StdAfx.h"
#include ".\panelcontrolcolor.h"
#include "PanelContainer.h"

namespace LindaStudio
{
  PanelControlColor::PanelControlColor(IPanelControlColor *notify,DocumentBase<IPanelControlMsgs> *document, const char *labelText):
    _notify(notify),PanelControlWithLabel(document,labelText),_button(this)
  {
    _button.Create("",WS_CHILD|WS_VISIBLE|BS_OWNERDRAW,CRect(),CWnd::FromHandle(document->GetWindow()),0);
  }

  PanelControlColor::~PanelControlColor(void)
  {
  }


  bool PanelControlColor::GetPreferredSize(int &xs, int &ys)
  {
    CSize sz=PanelControlWithLabel::GetTextExtent(&_button,"W");
    ys=2*sz.cy+GetSystemMetrics(SM_CYEDGE);
    return true;
  }

  bool PanelControlColor::SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys)
  {
    int xx=xs;
    PanelControlWithLabel::GetPreferredSize(xs,ys);
    PanelControlWithLabel::SetNewLocation(mw,x,y,xs,ys);
    mw.SetNewPosition(_button,x+xs,y,xx-xs,ys);
    return true;
  }

  bool PanelControlColor::SetEnable(IPanelControlMsgs *what, bool newState)
  {
    bool res;
    if ((res=PanelControlWithLabel::SetEnable(what,newState))==true)
      _button.EnableWindow(res);
    return res;
  }

  bool PanelControlColor::SetVisible(IPanelControlMsgs *what, bool newState)
  {
    bool res;
    if ((res=PanelControlWithLabel::SetVisible(what,newState))==true)
      _button.ShowWindow(res?SW_SHOW:SW_HIDE);
    return res;
  }

  bool PanelControlColor::GoNextControl(IPanelControlMsgs *from, bool previous)
  {
    _button.SetFocus();
    return true;
  }

  HWND PanelControlColor::GetWindow() const
  {
    return _button;
  }

  bool PanelControlColor::SetFont(HFONT font)
  {
    PanelControlWithLabel::SetFont(font);
    _button.SetFont(CFont::FromHandle(font));
    return true;
  }

  void PanelControlColor::OnDocumentDropped()
  {
    delete this;
  }


  void PanelControlColor::OnColorButtonClick()
  {
    CColorDialog dlg(_button.GetColor(),CC_ANYCOLOR|CC_FULLOPEN|CC_RGBINIT,&_button);
    if (dlg.DoModal()==IDOK)
    {
      _button.SetColor(dlg.GetColor());
      if (_notify) _notify->OnColorChanged(dlg.GetColor());
    }
  }
}
