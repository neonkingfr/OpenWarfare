// PanelContainer.cpp : implementation file
//

#include "stdafx.h"
//#include "LindaStudio.h"
#include "PanelContainer.h"
#include "PanelControlWithLabel.h"



namespace LindaStudio
{


  class PanelContainerPimpl: public IPanelControlWithLabelContainerInfo
  {
    float _labelSize;
  public:
    PanelContainerPimpl():_labelSize(0.5) {}

    virtual float GetLabelSize() const {return _labelSize;}
    virtual void SetLabelSize(float f) {_labelSize=f;}
  };

  PanelContainer::PanelContainer():_asView(this)
  { 
    _prefXAsk=-1;
    _prefYAsk=-1;
    _space=2;
    _minheight=20;
    _lastSizeX=-1;
    _lastSizeY=-1;
    _pimpl=new PanelContainerPimpl();
  }

  PanelContainer::~PanelContainer()
  {
    PrepareDestroy();
    delete _pimpl;
  }


  BEGIN_MESSAGE_MAP(PanelContainer, CWnd)
    ON_WM_DESTROY()
    ON_WM_VSCROLL()
    ON_WM_HSCROLL()
    ON_WM_PAINT()
    ON_WM_SETFOCUS()
  END_MESSAGE_MAP()

  BOOL PanelContainer ::Create(DWORD style,CWnd *pParent, UINT nID)
  {
    BOOL res=(style & WS_CHILD?
        CWnd::Create(AfxRegisterWndClass(0,::LoadCursor(0,IDC_ARROW),(HBRUSH)(COLOR_BTNFACE+1),0),"",style,CRect(0,0,0,0),pParent,nID):
        CWnd::CreateEx(0,AfxRegisterWndClass(0,::LoadCursor(0,IDC_ARROW),(HBRUSH)(COLOR_BTNFACE+1),0),"",style,CRect(0,0,0,0),pParent,nID));
   if (res==FALSE) return false;
    return true;
  }


  void PanelContainer::GetPreferredSizeClient(int &width, int &height, int xs, int ys)
  {
    for(EachView it(this);it;)
    {
      int xxs=xs;
      int yys=ys;
      it->GetPreferredSize(xxs,yys);
      if (xxs>width) width=xxs;
      if (yys<_minheight) yys=_minheight;
      height+=yys+_space;
    }
  }
  void PanelContainer::GetPreferredSizeClient(int &xs, int &ys)
  {
    if (_prefXAsk==xs && _prefYAsk==ys)
    {
      xs=_prefXRep;
      ys=_prefYRep;
      return;
    }
    int height=0;
    int width=0;
    GetPreferredSizeClient(width,height,xs,ys);
    _prefXAsk=xs;
    _prefYAsk=ys;
    xs=width;
    ys=height;
    _prefXRep=xs;
    _prefYRep=ys;
  }

  bool PanelContainer::GetPreferredSize(int &xs, int &ys)
  {
    GetPreferredSizeClient(xs,ys);
    CRect rc(0,0,xs,ys);
    AdjustWindowRectEx(rc,GetWindowLong(*this,GWL_STYLE),GetMenu()!=0,GetWindowLong(*this,GWL_EXSTYLE));
    xs=rc.Size().cx;
    ys=rc.Size().cy;
    return true;
  }

  void PanelContainer::RecalcLayoutClient(bool force)
  {
    CRect rc,newrc;
    GetClientRect(&newrc);
    if (force || newrc.right!=_lastSizeX || newrc.bottom!=_lastSizeY)
    {    
      do 
      {         
        rc=newrc;
        int rx=rc.right;
        int ry=rc.bottom;
        GetPreferredSizeClient(rx,ry);

        SCROLLINFO nfo;
        nfo.cbSize=sizeof(nfo);
        nfo.fMask=SIF_PAGE|SIF_RANGE;
        nfo.nMin=0;
        nfo.nMax=ry-1;
        nfo.nPage=rc.bottom;
        SetScrollInfo(SB_VERT,&nfo,TRUE);

        nfo.nMax=rx-1;
        nfo.nPage=rc.right;
        SetScrollInfo(SB_HORZ,&nfo,TRUE);
        GetClientRect(&newrc);
      }
      while (newrc.right!=rc.right || newrc.bottom!=rc.bottom);

      int ypos=-GetScrollPos(SB_VERT);
      int xpos=-GetScrollPos(SB_HORZ);

      _linePos.Clear();
      _colors.Clear();
      RepositionItems(xpos, ypos, rc);
      _lastSizeX=newrc.right;
      _lastSizeY=newrc.bottom;
    }
  }
  void PanelContainer::RepositionItems(int xpos, int ypos, CRect &rc)
  {
      ScrollWinManipulator wlist;
      for(EachView it(this);it;)
      {
        int xx=rc.right;
        int yy=rc.bottom;
        it->GetPreferredSize(xx,yy);
        int h=yy,m=0;
        if (h<_minheight) {h=_minheight;m=(_minheight-yy)/2;}
        it->SetNewLocation(wlist,xpos,ypos+m,rc.right,yy);
        _linePos.Add(ypos+h+_space/2);
        _colors.Add(it->GetControlBgrColor());
        ypos+=h+_space;
      }
  }

  bool PanelContainer::SetNewLocation(IPanelWindowManipulator &mw,int x, int y, int xs, int ys)
  {
    mw.SetNewPosition(*this,x,y,xs,ys);
    RecalcLayoutClient(true);
    return true;
  }

  bool PanelContainer::SetEnable(IPanelControlMsgs *what, bool newState)
  {
    if (what==this)
    {
      EnableWindow(newState);
    }
    for(EachView it(this);it;it->SetEnable(what,newState));

    return true;
  }

  bool PanelContainer::SetVisible(IPanelControlMsgs *what, bool newState)
  {
    if (what==this)
    {
      ShowWindow(newState?SW_SHOW:SW_HIDE);
    }
    for(EachView it(this);it;it->SetVisible(what,newState));

    return true;
  }

  IPanelControlMsgs *PanelContainer::GetPreviousControl(IPanelControlMsgs *cur) const
  {
    IPanelControlMsgs *last=0; 
    for(EachView it(const_cast<PanelContainer *>(this));it;last=it)
      if (it==cur) return last;
    if (cur==0) return last;
    else return 0;
  }
  
  IPanelControlMsgs *PanelContainer::GetNextControl(IPanelControlMsgs *cur)  const
  {
    if (cur==0)
    {
      EachView it(const_cast<PanelContainer *>(this));
      if (it) return it;
      return 0;
    }
    for(EachView it(const_cast<PanelContainer *>(this));it;)
      if (it==cur)
      {
        if (it) return it;
        return 0;
      }

    return 0;    
  }


  bool PanelContainer::IncomeFocus(IPanelControlMsgs *from, bool previous)
  {
    if (previous)
    {    
      IPanelControlMsgs *prev=GetPreviousControl(0);
      while (prev && prev->GoNextControl(from,previous)==false)
        prev=GetPreviousControl(prev);
      EnsureVisible(prev);
      return prev!=0;
    }
    else
    {
      IPanelControlMsgs *next=GetNextControl(0);
      while (next && next->GoNextControl(from,previous)==false)
        next=GetNextControl(next);
      EnsureVisible(next);
      return next!=0;
    }

  }

  bool PanelContainer::GoNextControl(IPanelControlMsgs *from, bool previous)
  {
    IPanelControlMsgs *x;
    if (previous)
    {
      x=GetPreviousControl(from);
      while (x && x->GoNextControl(from,previous)==false)
        x=GetPreviousControl(x);
    }
    else
    {
      x=GetNextControl(from);
      while (x && x->GoNextControl(from,previous)==false)
        x=GetNextControl(x);
    }

    if (x==0)
    {
      IPanelControlMsgs *parent=_asView.GetParentContainer();
      bool gn;
      if (parent) gn=_asView.GetParentContainer()->GoNextControl(from,previous);
      else gn=false;

      if (gn==false)
      {
        return IncomeFocus(from,previous);          
      }       
    }
    else
      EnsureVisible(x);
    return true;
  }

  void PanelContainer::ReLayout()
  {
    _prefXAsk=_prefYAsk=-1;
    RecalcLayoutClient(true);
    Invalidate(TRUE);   
  }

  void PanelContainer::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
  {
    int cpos=GetScrollPos(SB_VERT);
    int opos=cpos;
    CRect rc;
    GetClientRect(&rc);
    switch (nSBCode)
    {
    case SB_LINEUP: cpos-=16;break;
    case SB_LINEDOWN: cpos+=16;break;
    case SB_PAGEUP: cpos-=rc.bottom*8/10;break;
    case SB_PAGEDOWN: cpos+=rc.bottom*8/10;break;
    case SB_TOP: cpos=0;break;
    case SB_BOTTOM: cpos=65535;break;
    case SB_THUMBPOSITION:
    case SB_THUMBTRACK: cpos=nPos;break;
    case SB_ENDSCROLL: return;
    }
    SetScrollPos(SB_VERT,cpos,TRUE);
    cpos=GetScrollPos(SB_VERT);
    RecalcLayoutClient(true);
    if (opos!=cpos) 
    {
      CRect rc;
      CRect wnd;
      GetClientRect(&wnd);
      CClientDC dc(this);
      dc.ScrollDC(0,opos-cpos,&wnd,0,0,&rc);
      InvalidateRect(&rc,TRUE);
      UpdateWindow();
    }
    
  }


  void PanelContainer::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
  {
    int cpos=GetScrollPos(SB_HORZ);
    int opos=cpos;
    CRect rc;
    GetClientRect(&rc);
    switch (nSBCode)
    {
    case SB_LINELEFT: cpos-=16;break;
    case SB_LINERIGHT: cpos+=16;break;
    case SB_PAGELEFT: cpos-=rc.bottom*8/10;break;
    case SB_PAGERIGHT: cpos+=rc.bottom*8/10;break;
    case SB_LEFT: cpos=0;break;
    case SB_RIGHT: cpos=65535;break;
    case SB_THUMBPOSITION:
    case SB_THUMBTRACK: cpos=nPos;break;
    case SB_ENDSCROLL: return;
    }
    SetScrollPos(SB_HORZ,cpos,TRUE);
    cpos=GetScrollPos(SB_VERT);
    RecalcLayoutClient(true);
    if (opos!=cpos) 
    {
      CRect rc;
      CRect wnd;
      GetClientRect(&wnd);
      CClientDC dc(this);
      dc.ScrollDC(opos-cpos,0,&wnd,0,0,&rc);
      InvalidateRect(&rc,TRUE);
      UpdateWindow();
    }
  }

  void PanelContainer::EnsureVisible(IPanelControlMsgs *what)
  {
    if (what==0) return;
    CRect rc;
    bool scroll=false;
    ::GetWindowRect(what->GetActiveWindow(),&rc);
    CRect client;
    GetClientRect(&client);
    ScreenToClient(&rc);
    int xpos=GetScrollPos(SB_HORZ);
    int ypos=GetScrollPos(SB_VERT);
    if (rc.top<0)    
    {
      SetScrollPos(SB_VERT,GetScrollPos(SB_VERT)+rc.top);
      scroll=true;
    }
    if (rc.bottom>client.bottom)    
    {
      SetScrollPos(SB_VERT,GetScrollPos(SB_VERT)+rc.bottom-client.bottom);
      scroll=true;
    }
    if (rc.left<0)    
    {
      SetScrollPos(SB_HORZ,GetScrollPos(SB_HORZ)+rc.left);
      scroll=true;
    }
    if (rc.right>client.right)    
    {
      SetScrollPos(SB_HORZ,GetScrollPos(SB_HORZ)+rc.right-client.right);
      scroll=true;
    }
    if (scroll) 
    {
      RecalcLayoutClient(true);
      CRect rc;
      CRect wnd;
      GetClientRect(&wnd);
      CClientDC dc(this);
      dc.ScrollDC(xpos-GetScrollPos(SB_HORZ),ypos-GetScrollPos(SB_VERT),&wnd,0,0,&rc);
      InvalidateRect(&rc,TRUE);
      UpdateWindow();
    }
  }

  bool PanelContainer::SetFont(HFONT font)
  {
    for(EachView it(this);it;it->SetFont(font));
    return true;
  }

  void PanelContainer::OnPaint()
  {
    CPaintDC dc(this);
    CRect rc;
    GetClientRect(&rc);
  
    LOGBRUSH lb;
    lb.lbColor=GetSysColor(COLOR_WINDOWTEXT);
    lb.lbStyle=BS_SOLID;
    CPen pen(PS_COSMETIC|PS_ALTERNATE,1,&lb);

    CPen *old=dc.SelectObject(&pen);


    int lasty=0;

    for (int i=0;i<_linePos.Size();i++)
    {
      int nx=_linePos[i]+1;
      if (nx>0 && (_colors[i] & 0xFF000000)==0)
      {
        dc.FillSolidRect(rc.left,lasty,rc.right,nx,_colors[i]);
        lasty=nx;
      }
      dc.MoveTo(rc.left,_linePos[i]);
      dc.LineTo((rc.right+rc.left)/2,_linePos[i]);
    }

    dc.SelectObject(old);

  }

  IPanelControlMsgs *PanelContainer::WhoseWindow(HWND window)
  {
    for(EachView it(this);it;)
    {
      IPanelControlMsgs *res=it->WhoseWindow(window);
      if (res) return res;
    }
    return 0;
  }

  void PanelContainer::RecalcLayout()
  {
    int xs=_lastSizeX;
    int ys=_lastSizeY;
    int savexs=xs;
    int saveys=ys;
    ForceRecalcNext();
    GetPreferredSize(xs,ys);
    if (xs!=savexs || ys!=saveys)
    { 
      IPanelControlMsgs *pp=_asView.GetParentContainer();
      if (pp) pp->RecalcLayout();
      else ReLayout();
    }
  }
  void PanelContainer::ForceRecalcNext()
  {
    _prefXAsk=-1;
    _prefYAsk=-1;
  }

  BOOL PanelContainer::PreTranslateMessage(MSG* pMsg)
  {
    if (pMsg->message==WM_MOUSEWHEEL)
    {    
      SCROLLINFO nfo;
      nfo.cbSize=sizeof(nfo);
      GetScrollInfo(SB_VERT,&nfo,SIF_ALL);
      if (nfo.nMax-nfo.nMin+1>nfo.nPage || nfo.nPos!=0)
      {
        short delta=(short)(HIWORD(pMsg->wParam));
        int shift;
        if (delta>0) shift=-1;else shift=1;
        nfo.nPos+=shift*50;
//        SetScrollPos(SB_VERT,nfo.nPos+shift*50,TRUE);  
        OnVScroll(SB_THUMBPOSITION,nfo.nPos,0);
        OnVScroll(SB_ENDSCROLL,0,0);
        return TRUE;

      }
    }
    return CWnd::PreTranslateMessage(pMsg);

  }



  void *PanelContainer::GetInterfacePtr(unsigned int ifcuid)
  {
    if (ifcuid==IPanelControlWithLabelContainerInfo::IFCUID)
      return static_cast<IPanelControlWithLabelContainerInfo *>(_pimpl);
    else
      return __super::GetInterfacePtr(ifcuid);
         
  }
  
}


