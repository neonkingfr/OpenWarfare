#pragma once
#include "PanelControlBase.h"
#include "PanelContainer.h"
#include "ButtonWithNotify.h"
#include "SubPanelFrame.h"
#include "TranspStatic.h"

class IPanelSubcontainer
{
public:
  virtual bool OnBeforeOpenClose(bool open)=0;
  virtual bool OnAfterOpenClose(bool open)=0;
};


class PanelSubcontainer : public LindaStudio::PanelControlBase, public IButtonNotify
{
public:
  class SlaveContainer: public LindaStudio::PanelContainer
  {
    PanelSubcontainer *outer;
    bool _repos;
  public:
//    virtual bool GetPreferredSize(int &xs, int &ys) {abort();}    

//    virtual bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys)=0;

    ///Informs all views (including target) that enable state has been changed
//    virtual bool SetEnable(IPanelControlMsgs *what, bool newState)
//      {


    ///Informs all views (including target) that visible state has been changed
//    virtual bool SetVisible(IPanelControlMsgs *what, bool newState)=0;

    ///Call to raise next control in list
    /**
    * Caller informs window, that it wants to give focus to next control.
    * Window informs control, that it gets focus
    */
//    virtual bool GoNextControl(IPanelControlMsgs *from, bool previous=false);  

//    virtual HWND GetWindow() const {return 0;}

//  virtual bool SetFont(HFONT)=0;

//    virtual IPanelControlMsgs *WhoseWindow(HWND window)=0;
//    virtual COLORREF GetControlBgrColor() const {return 0xFF000000;}
    virtual void RecalcLayout();

    SlaveContainer(PanelSubcontainer *outer):outer(outer) {}
    void OnPaint();
    BOOL OnEraseBkgnd(CDC* pDC);
    void SetOwner(PanelSubcontainer *outer) {this->outer=outer;}
    bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys)
    {
      _repos=true;      
      return __super::SetNewLocation(mw,x,y,xs,ys);
    }
    virtual void EnsureVisible(IPanelControlMsgs *what);
    bool SlaveContainer::GoNextControl(IPanelControlMsgs *from, bool previous);
  protected:
    DECLARE_MESSAGE_MAP();

  };
protected:
  CButtonWithNotify _titleButton;
  SubPanelFrame _frame;
  CTranspStatic _text;
  CTranspStatic _typetext;
  IPanelSubcontainer *_notify;
  SRef<PanelContainer> _container;
  DWORD _bgcolor;
  CFont _titleFont;
public:
  PanelSubcontainer(IPanelSubcontainer *notify,DocumentBase<IPanelControlMsgs> *document, const char *title,const char *type, bool closed,PanelContainer *container=0);
  ~PanelSubcontainer(void);

  PanelContainer *GetContainer() {return _container;}

  virtual bool GetPreferredSize(int &xs, int &ys);
  bool GetPreferredSize(int &xs, int &ys, int &cntxs, int &cntys);

  virtual bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys);
  virtual bool SetEnable(IPanelControlMsgs *what, bool newState);

  virtual bool SetVisible(IPanelControlMsgs *what, bool newState);

  virtual bool GoNextControl(IPanelControlMsgs *from, bool previous=false);  

  virtual HWND GetWindow() const {return _container->GetWindow();}
  virtual HWND GetActiveWindow() const {return GetFocus();}

  virtual bool SetFont(HFONT);

  virtual IPanelControlMsgs *WhoseWindow(HWND window);

  void ArrowPressed(int code);
  void OnButtonPressed();
  void SetBkColor(IN COLORREF col) {_bgcolor=col;}

  bool IsOpened() const;
};
