#pragma once
#include "panelcontrolwithlabel.h"
#include "ButtonWithNotify.h"

namespace LindaStudio
{

class IPanelControlSingleButtonNotify
{
public:
  virtual void ButtonClicked()=0;
};

class PanelControlSingleButton :  public PanelControlWithLabel,public IButtonNotify
{
protected:
  IPanelControlSingleButtonNotify *_notify;
  CButtonWithNotify _button;
  CSize _buttSize;

  void CalcButtonSize();
public:
  PanelControlSingleButton(IPanelControlSingleButtonNotify *notify, 
                           const char *text, 
                           DocumentBase<IPanelControlMsgs> *container, 
                           const char *labelText);

  ~PanelControlSingleButton(void);

  virtual bool GetPreferredSize(int &xs, int &ys);
  virtual bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys);
  virtual bool SetEnable(IPanelControlMsgs *what, bool newState);
  virtual bool SetVisible(IPanelControlMsgs *what, bool newState);
  virtual bool GoNextControl(IPanelControlMsgs *from, bool previous=false);  
  virtual HWND GetWindow() const;
  virtual bool SetFont(HFONT font);
  void OnDocumentDropped();

  virtual IPanelControlMsgs *WhoseWindow(HWND window)
  {
    if (window==_button) return this;
    else return PanelControlWithLabel::WhoseWindow(window);
  }

  virtual void OnButtonPressed();
  virtual void ArrowPressed(int code);

};

}