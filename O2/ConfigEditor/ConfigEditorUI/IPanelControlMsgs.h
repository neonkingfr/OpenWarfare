#pragma once
#include <el/Interfaces/IMultiInterfaceBase.hpp>

namespace LindaStudio
{

  class IPanelWindowManipulator
  {
  public:
    virtual void SetNewPosition(HWND hWnd, const RECT &rc)=0;
    virtual void SetNewPosition(HWND hWnd, int x, int y, int xs, int ys)=0;
    void SetNewPositionRel(HWND hWnd, int x, int y, int xs, int ys)
    {
      RECT rc;
      GetWindowRect(hWnd,&rc);
      HWND parent=GetParent(hWnd);
      ScreenToClient(parent,(LPPOINT)&rc);
      ScreenToClient(parent,((LPPOINT)&rc)+1);
      SetNewPosition(hWnd,x+rc.left,y+rc.top,(rc.right-rc.left)+xs,(rc.bottom-rc.top)+ys);
    }
  };


  class IPanelControlMsgs: public IMultiInterfaceBase
  {
  public:

    ///Obtains preferred size for the control
    /**
    * @param xs width of the window - return preferred width
    * @param ys height of the window - return preferred height
    */
    virtual bool GetPreferredSize(int &xs, int &ys)=0;

    /**
    * @param x left position of the control
    * @param y top position of the control
    * @param xs width of the control
    * @param ys height of the control
    */
    virtual bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys)=0;

    ///Informs all views (including target) that enable state has been changed
    virtual bool SetEnable(IPanelControlMsgs *what, bool newState)=0;


    ///Informs all views (including target) that visible state has been changed
    virtual bool SetVisible(IPanelControlMsgs *what, bool newState)=0;

    ///Call to raise next control in list
    /**
    * Caller informs window, that it wants to give focus to next control.
    * Window informs control, that it gets focus
    */
    virtual bool GoNextControl(IPanelControlMsgs *from, bool previous=false)=0;  

    virtual HWND GetWindow() const {return 0;}
    virtual HWND GetActiveWindow() const {return GetWindow();}

    virtual bool SetFont(HFONT)=0;

    virtual IPanelControlMsgs *WhoseWindow(HWND window)=0;
    virtual COLORREF GetControlBgrColor() const {return 0xFF000000;}
    virtual void RecalcLayout() {}


  };

  


}