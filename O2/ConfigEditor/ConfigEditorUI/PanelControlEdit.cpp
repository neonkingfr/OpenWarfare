// PanelControlEdit.cpp : implementation file
//

#include "stdafx.h"
//#include "LindaStudio.h"
#include "PanelControlEdit.h"
#include "PanelContainer.h"


namespace LindaStudio
{

  // PanelControlEdit

  PanelControlEdit::PanelControlEdit(DocumentBase<IPanelControlMsgs> *document, const char *labelText, IPanelControlEditNotify *ntf):
    PanelControlWithLabel(document,labelText),notify(ntf),_lastAsk(0,0),_lastReply(0,0),_modify(false)
  {
    CreateEx(WS_EX_STATICEDGE,"EDIT","",WS_CHILD|WS_VISIBLE|WS_TABSTOP|ES_AUTOHSCROLL,CRect(0,0,0,0),
      CWnd::FromHandle(GetDocument()->GetWindow()),0);

  }

  PanelControlEdit::~PanelControlEdit()
  {
    if (GetSafeHwnd()) DestroyWindow();
  }

  void PanelControlEdit::OnDocumentDropped()
  {
    DetachDocument();
    if (notify) notify->DeleteControl();
    delete this;
  }


  BEGIN_MESSAGE_MAP(PanelControlEdit, CEdit)  
    ON_CONTROL_REFLECT(EN_CHANGE,OnChangedText)
    ON_WM_KEYDOWN()
    ON_CONTROL_REFLECT(EN_SETFOCUS,OnSetFocus)
    ON_CONTROL_REFLECT(EN_KILLFOCUS,OnKillFocus)
  END_MESSAGE_MAP()

  void PanelControlEdit::OnChangedText()
  {
    _modify=true;
  }

  bool PanelControlEdit::GetPreferredSize(int &xs, int &ys)
  {
    if (xs==_lastAsk.cx && ys==_lastAsk.cy)
    {
      xs=_lastReply.cx;
      ys=_lastReply.cy;
      return true;
    }

    CClientDC dc(this);
    CFont *prev=dc.SelectObject(GetFont());
    CSize sz=dc.GetTextExtent("W");
    dc.SelectObject(prev);
    ys=sz.cy+1*GetSystemMetrics(SM_CYEDGE);

    int ax=xs;
    PanelControlWithLabel::GetPreferredSize(ax,ys);
    _lastReply.cx=xs;
    _lastReply.cy=ys;
    return true;
  }

  bool PanelControlEdit::SetNewLocation(IPanelWindowManipulator &mw,int x, int y, int xs, int ys)
  {
    int ax=xs;
    int ay=ys;
    PanelControlWithLabel::GetPreferredSize(ax,ay);
    PanelControlWithLabel::SetNewLocation(mw,x,y,ax,ay);
    x+=ax;
    xs-=ax;
    mw.SetNewPosition(m_hWnd,x,y,xs,ys);
    return true;
  }


  bool PanelControlEdit::SetEnable(IPanelControlMsgs *what, bool newState)
  {
    if (what==this)
    {
      EnableWindow(newState);
      PanelControlWithLabel::SetEnable(what,newState);
      return true;
    }
    else
      return false;
  }


 bool PanelControlEdit::SetVisible(IPanelControlMsgs *what, bool newState)
 {
   if (what==this)
   {
     ShowWindow(newState?SW_SHOW:SW_HIDE);
     PanelControlWithLabel::SetVisible(what,newState);
     return true;
   }
   else
     return false;
 }

 bool PanelControlEdit::GoNextControl(IPanelControlMsgs *from, bool previous)
 {
    SetFocus();
    SetSel(0,-1);
    return true;
 }
 
 HWND PanelControlEdit::GetWindow() const
 {
   return GetSafeHwnd();
 }

  void PanelControlEdit::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
  {
    if (nChar==VK_DOWN)
      GetDocument()->GoNextControl(this,false);
    else if (nChar==VK_UP)
      GetDocument()->GoNextControl(this,true);
    else
      CEdit::OnKeyDown(nChar,nRepCnt,nFlags);
  }

  void PanelControlEdit::OnSetFocus()
  {
    _modify=false;
  }
  void PanelControlEdit::OnKillFocus()
  {
    if (_modify)
    {
      CString txt;
      GetWindowText(txt);
      TextChanged(txt);
   }
    _modify=false;
  }

  bool PanelControlEdit::SetFont(HFONT font)
  {
    CEdit::SetFont(CFont::FromHandle(font));
    return PanelControlWithLabel::SetFont(font);
  }

  CString PanelControlEdit::GetText() const
  {
    CString text;
    GetWindowText(text);
    return text;
  }

  BEGIN_MESSAGE_MAP(PanelControlMultilineEdit, PanelControlEdit)  
    ON_WM_KEYDOWN()
  END_MESSAGE_MAP()
  PanelControlMultilineEdit::PanelControlMultilineEdit(DocumentBase<IPanelControlMsgs> *document, const char *labelText, IPanelControlEditNotify *ntf, int lines, bool wrap):
  PanelControlEdit(document,labelText,ntf),
    _lines(lines),
    _wrap(wrap)
  {
    DestroyWindow();
    CreateEx(WS_EX_STATICEDGE,"EDIT","",WS_CHILD|WS_VISIBLE|WS_TABSTOP|ES_MULTILINE|WS_VSCROLL|ES_AUTOVSCROLL|(!_wrap?ES_AUTOHSCROLL|WS_HSCROLL:0),CRect(0,0,0,0),
      CWnd::FromHandle(GetDocument()->GetWindow()),0);
  }

  bool PanelControlMultilineEdit::GetPreferredSize(int &xs, int &ys)
  {
    if (xs==_lastAsk.cx)
    {
      ys=_lastReply.cy;
      return true;
    }

    _lastAsk.cx=xs;
    int ax=xs;
    PanelControlWithLabel::GetPreferredSize(ax,ys);

    SetWindowPos(0,0,0,xs-ax,100,SWP_NOMOVE|SWP_NOZORDER);
    int lines=GetLineCount();
  
    CSize sz=PanelControlWithLabel::GetTextExtent(this,"W");    

    ys=sz.cy*lines+1*GetSystemMetrics(SM_CYEDGE)+(!_wrap?GetSystemMetrics(SM_CYHSCROLL):0);
    _lastReply.cy=ys;

    return true;
  }

  void PanelControlMultilineEdit::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
  {
    if (nChar==VK_UP || nChar==VK_DOWN)
    {
      int curSel;
      GetSel(curSel,curSel);
      int line=LineFromChar(curSel);
      int cnt=GetLineCount();
      if (nChar==VK_DOWN && line+1==cnt)
        GetDocument()->GoNextControl(this,false);
      else if (nChar==VK_UP && line==0)
        GetDocument()->GoNextControl(this,true);
      else
        CEdit::OnKeyDown(nChar,nRepCnt,nFlags);
    }
    else
      CEdit::OnKeyDown(nChar,nRepCnt,nFlags);
  }
}
