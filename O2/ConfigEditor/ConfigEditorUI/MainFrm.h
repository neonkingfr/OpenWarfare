// MainFrm.h : interface of the MainFrame class
//


#pragma once


#include "ConfigEditorWithUndo.h"
#include "PanelMyContainer.h"
#include "PanelView.h"
#include <es/Types/UniRef.hpp>

class MainFrame : public CFrameWnd, public MVC::View<ConfigMVC>
{
	UniRef<ConfigEditorWithUndo> _document;
  bool _sizing;
public:
	MainFrame(const UniRef<ConfigEditorWithUndo> &document);
protected: 
	DECLARE_DYNAMIC(MainFrame)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL LoadFrame(UINT nIDResource, DWORD dwDefaultStyle = WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, CWnd* pParentWnd = NULL, CCreateContext* pContext = NULL);

// Implementation
public:
	virtual ~MainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
	CToolBar    m_wndToolBar;
  PanelMyContainer _panel;
  PanelView _panelView;

// Generated message map functions
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnFileClose();
	afx_msg void OnClose();
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnFileNew();
  afx_msg void OnViewNewwindow();
  afx_msg void OnFileOpen();
  afx_msg void OnFileSave();
  afx_msg void OnFileSaveas();
  afx_msg void OnViewRefresh();
  afx_msg void OnViewReloaddependencies();
  afx_msg void OnAppExit();

  virtual bool UpdateMe(ConfigMVC::CurModel *model);
  virtual void SetConfigName(const RStringI &name);
  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg LRESULT OnEnterSizeMove(WPARAM wParam, LPARAM lParam); 
  afx_msg LRESULT OnExitSizeMove(WPARAM wParam, LPARAM lParam);
};


