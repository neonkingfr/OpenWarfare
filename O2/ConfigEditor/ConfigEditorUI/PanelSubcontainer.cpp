#include "StdAfx.h"
#include ".\panelsubcontainer.h"
#include "PanelControlWithLabel.h"

#define ANIMATION_SPEED 150

BEGIN_MESSAGE_MAP(PanelSubcontainer::SlaveContainer,LindaStudio::PanelContainer)
  ON_WM_PAINT()
  ON_WM_ERASEBKGND()
END_MESSAGE_MAP();

void PanelSubcontainer::SlaveContainer::OnPaint()
{
  CPaintDC dc(this);

/*  CRect rc;
  dc.GetClipBox(&rc);
  dc.FillSolidRect(&rc,outer->_bgcolor);*/
}


BOOL PanelSubcontainer::SlaveContainer::OnEraseBkgnd(CDC* pDC)
{
  return FALSE;
}

PanelSubcontainer::PanelSubcontainer(IPanelSubcontainer *notify,DocumentBase<IPanelControlMsgs> *document, const char *title,const char *type,bool closed,PanelContainer *container)
  :_titleButton(this), _notify(notify)
{
  if (container==0) _container=new SlaveContainer(this);
  else _container=container;
  CWnd *hWnd=CWnd::FromHandle(document->GetWindow());
  _titleButton.Create("+",WS_VISIBLE|WS_CHILD|BS_CENTER|BS_VCENTER|BS_AUTOCHECKBOX|BS_PUSHLIKE,CRect(),CWnd::FromHandle(document->GetWindow()),0);
  _frame.Create("STATIC","",WS_CHILD|WS_VISIBLE|WS_CLIPSIBLINGS,CRect(),hWnd,0);
  _typetext.Create(type,WS_VISIBLE|WS_CHILD|ES_RIGHT,CRect(),hWnd);
  _text.Create(title,WS_VISIBLE|WS_CHILD,CRect(),hWnd);
  _container->Create(WS_VISIBLE|WS_CHILD,hWnd,0);
  if (!closed) _titleButton.SetCheck(1);
  AttachDocument(document);
  _bgcolor=GetSysColor(COLOR_WINDOW);
}

PanelSubcontainer::~PanelSubcontainer(void)
{
}

bool PanelSubcontainer::GetPreferredSize(int &xs, int &ys)
{
  int d;  
  return GetPreferredSize(xs,ys,d,d);
}

bool PanelSubcontainer::GetPreferredSize(int &xs, int &ys, int &cntxs, int &cntys)
{
  CSize sz=PanelControlWithLabel::GetTextExtent(&_titleButton,"W",false); 
  if (_titleButton.GetCheck()==0)
  {
    ys=sz.cy+GetSystemMetrics(SM_CYEDGE)+4;
    cntxs=xs;
    cntys=0;
  }
  else
  {
    int xx=xs-sz.cx*2-GetSystemMetrics(SM_CYEDGE)-8;
    int yy=0x7fff;
    _container->GetPreferredSize(xx,yy);
    cntxs=xs;
    cntys=yy;
    yy+=sz.cy+GetSystemMetrics(SM_CYEDGE)+16;
    ys=yy;
    if (xs<xx) xs=xx;
  }
  return true;
}

bool PanelSubcontainer::SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys)
{
  CSize sz=PanelControlWithLabel::GetTextExtent(&_titleButton,"W",false);  
  int offsety=sz.cy+GetSystemMetrics(SM_CYEDGE)+3;
  int offsetx=sz.cx*2+GetSystemMetrics(SM_CYEDGE)+8;
  mw.SetNewPosition(_titleButton,x,y,sz.cx*2,sz.cy+GetSystemMetrics(SM_CYEDGE));
  mw.SetNewPosition(_frame,x+sz.cx*2,y,xs-sz.cx*2,ys);
  mw.SetNewPosition(_text,x+sz.cx*2+10,y,xs-sz.cx*2-20,sz.cy);
  mw.SetNewPosition(_typetext,x+sz.cx*2+10,y,xs-sz.cx*2-20,sz.cy);
  if (_titleButton.GetCheck()!=0)
  {
    _container->ShowWindow(SW_SHOW);
  }
  else
  {
    _container->ShowWindow(SW_HIDE);
  }
  _frame.SetBkColor(_bgcolor);

  CRect rc;
  _container->GetWindowRect(&rc);
  ys-=offsety+12;
  xs-=offsetx+8;
  if (xs!=rc.right-rc.left || ys!=rc.bottom-rc.top)
    _container->SetNewLocation(mw,offsetx+x,offsety+y,xs,ys);
  else
    mw.SetNewPosition(*_container,offsetx+x,offsety+y,xs,ys);    
  return true;
}
bool PanelSubcontainer::SetEnable(IPanelControlMsgs *what, bool newState)
{
  if (what==this)
  {
    _titleButton.EnableWindow(newState);
    _container->EnableWindow(newState);
    return true;
  }
  return false;
}
bool PanelSubcontainer::SetVisible(IPanelControlMsgs *what, bool newState)
{
  return false;
}
bool PanelSubcontainer::GoNextControl(IPanelControlMsgs *from, bool previous)
{
  if (previous)
  {
//    GetDocument()->GoNextControl(this,previous);
    if (IsOpened())
      _container->IncomeFocus(this,previous); 
    else
      _titleButton.SetFocus();
  }
  else
  {
    _titleButton.SetFocus();
  }
  return true;
}
bool PanelSubcontainer::SetFont(HFONT fnt)
{
  _titleButton.SetFont(CFont::FromHandle(fnt));
  _container->SetFont(fnt);
  LOGFONT lg;
  GetObject(fnt,sizeof(lg),&lg);
  lg.lfWeight=FW_BOLD;
//  lg.lfUnderline=TRUE;
  if (_titleFont.m_hObject) _titleFont.DeleteObject();
  _titleFont.CreateFontIndirect(&lg);
  _text.SetFont(&_titleFont,TRUE);
  _typetext.SetFont(CFont::FromHandle(fnt),TRUE);

  return true;
}

IPanelControlMsgs *PanelSubcontainer::WhoseWindow(HWND window)
{
  if (window==_titleButton) return this;
  if (window==*_container) return this;
  return _container->WhoseWindow(window);
}

void PanelSubcontainer::OnButtonPressed()
{
  bool openit=_titleButton.GetCheck()!=0;
  if (_notify) _notify->OnBeforeOpenClose(openit);
  CRect rc;
  CRect frc;
  _frame.GetWindowRect(&frc);
  _container->GetParent()->ScreenToClient(&frc);
  _container->ShowWindow(SW_SHOW);
  _container->GetWindowRect(&rc);
  _container->GetParent()->ScreenToClient(&rc);
  CRect parentRc;
  _container->GetParent()->GetClientRect(&parentRc);
  int h1,v1,h2,v2,h3,v3;
  h1=rc.bottom-rc.top;
  v1=rc.right-rc.left;
  h2=h1;
  v2=v1;
  GetPreferredSize(v2,h2,v3,h3); 
  DWORD ttime=ANIMATION_SPEED;
  DWORD btime=GetTickCount();
  DWORD ctime=btime;
  int lasth=h1;
  _container->SetScrollRange(SB_VERT,0,0,FALSE);
  if (openit)
  {  
    CRect scrrect(0,frc.bottom,parentRc.right,parentRc.bottom);
    _container->GetParent()->ScrollWindow(0,rc.top-frc.bottom+16,&scrrect,&scrrect);
    _frame.SetWindowPos(&CWnd::wndTop,0,0,frc.right-frc.left,rc.top-frc.top+16 ,SWP_NOZORDER|SWP_NOMOVE); 
    _frame.Invalidate();
    _text.Invalidate();
    _frame.UpdateWindow(); 
    _text.UpdateWindow();    
  }
  while (ctime<btime+ttime)
  {
    float f=(float)(ctime-btime)/(float)(ttime);
    int h=toLargeInt(h1+(h3-h1)*f);
    int v=toLargeInt(v1+(v3-v1)*f);
    _container->SetWindowPos(0,0,0,v,h,SWP_NOMOVE|SWP_NOZORDER);
///    _container->Invalidate();
    _container->UpdateWindow();
    int dy=h-lasth; 
    lasth=h;
    CRect scrrect(0,rc.top+h+(dy<0?dy:0),parentRc.right,parentRc.bottom);
    _container->GetParent()->ValidateRect(&rc);
    _container->GetParent()->ScrollWindow(0,dy,&scrrect,&scrrect);
    ctime=GetTickCount();
  }
  _container->RecalcLayout(); 
  if (_notify) _notify->OnAfterOpenClose(_titleButton.GetCheck()!=0);
}
void PanelSubcontainer::ArrowPressed(int code)
{
  if (code==VK_UP) GetDocument()->GoNextControl(this,true);
  if (code==VK_DOWN) 
    if (_titleButton.GetCheck())            
      _container->GetThisView().GoNextControl(this,false);
    else
      GetDocument()->GoNextControl(this,false);

}

void PanelSubcontainer::SlaveContainer::RecalcLayout()
{  
  ForceRecalcNext();  
  _repos=false;
  outer->GetDocument()->RecalcLayout();
  if (!_repos) 
    __super::RecalcLayout();
}

void PanelSubcontainer::SlaveContainer::EnsureVisible(IPanelControlMsgs *what)
{
  static_cast<PanelContainer *>(outer->GetDocument())->EnsureVisible(what);
}

bool PanelSubcontainer::SlaveContainer::GoNextControl(IPanelControlMsgs *from, bool previous)
{
  IPanelControlMsgs *x;
  if (previous)
  {
    x=GetPreviousControl(from);
    while (x && x->GoNextControl(from,previous)==false)
      x=GetPreviousControl(x);
  }
  else
  {
    x=GetNextControl(from);
    while (x && x->GoNextControl(from,previous)==false)
      x=GetNextControl(x);
  }

  if (x==0)
  {
    if (previous) 
    {
      outer->_titleButton.SetFocus(); 
      EnsureVisible(this);
    }
    else  
      outer->GetDocument()->GoNextControl(outer,previous);
  }
  else
    EnsureVisible(x);
  return true;
}


bool PanelSubcontainer::IsOpened() const
{
  return _titleButton.GetCheck()!=0;
}

