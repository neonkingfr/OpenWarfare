#pragma once

#include "PanelSubcontainer.h"


class SlaveContainerGrid: public PanelSubcontainer::SlaveContainer
{
  int _avgItemWidth;  
  int _lastMaxCol;
public:
  SlaveContainerGrid(PanelSubcontainer *outer,int avgItemWidth);
  ~SlaveContainerGrid(void);

  void GetPreferredSizeClient(int &width, int &height, int xs, int ys);
  void RepositionItems(int xpos, int ypos, CRect &rc);


};
