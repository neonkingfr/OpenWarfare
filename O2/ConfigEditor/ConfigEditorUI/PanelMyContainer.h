#pragma once
#include "panelcontainer.h"

class PanelMyContainer :public PanelContainer
{
public:
  PanelMyContainer(void);
  ~PanelMyContainer(void);

  afx_msg void OnPaint();

protected:
  DECLARE_MESSAGE_MAP();
};
