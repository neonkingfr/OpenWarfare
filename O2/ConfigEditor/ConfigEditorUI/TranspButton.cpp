// TranspButton.cpp : implementation file
//

#include "stdafx.h"
//#include "LindaStudio.h"
#include "TranspButton.h"
#include ".\transpbutton.h"


// CTranspButton

IMPLEMENT_DYNAMIC(CTranspButton, CButton)
CTranspButton::CTranspButton()
{
}

CTranspButton::~CTranspButton()
{
}


BEGIN_MESSAGE_MAP(CTranspButton, CButton)
  ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()



// CTranspButton message handlers


HBRUSH CTranspButton::CtlColor(CDC* pDC, UINT /*nCtlColor*/)
{
  // TODO:  Change any attributes of the DC here

  // TODO:  Return a non-NULL brush if the parent's handler should not be called
  pDC->SetBkMode(TRANSPARENT);
  return (HBRUSH)GetStockObject(HOLLOW_BRUSH);
}
