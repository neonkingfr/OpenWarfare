#pragma once

#include "PanelControlWithLabel.h"

namespace LindaStudio
{

  class IPanelControlComboBoxNotify
  {
  public:
    virtual void DataChanged(int itemIndex, const char *itemText)=0;
  };

  class PanelControlComboBox : public CComboBox, public PanelControlWithLabel
  {
    IPanelControlComboBoxNotify *_notify;
    bool _modify;
    static const int CHeight=24;
  public:
    PanelControlComboBox(IPanelControlComboBoxNotify *notify, DWORD style, DocumentBase<IPanelControlMsgs> *document, const char *labelText);
    ~PanelControlComboBox();

  protected:
    virtual bool GetPreferredSize(int &xs, int &ys);
    virtual bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys);
    virtual bool SetEnable(IPanelControlMsgs *what, bool newState);
    virtual bool SetVisible(IPanelControlMsgs *what, bool newState);
    virtual bool GoNextControl(IPanelControlMsgs *from, bool previous=false);  
    virtual HWND GetWindow() const;
    virtual bool SetFont(HFONT font);
    virtual void OnDocumentDropped() {delete this;}

    afx_msg void OnEditChange() {_modify=true;}
    afx_msg void OnSelChange() {_modify=true;}
    afx_msg void OnControlSetFocus() {_modify=false;}
    afx_msg void OnControlKillFocus();

    afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    virtual IPanelControlMsgs *WhoseWindow(HWND window)
    {
      if (window=*this) return this;
      else return PanelControlWithLabel::WhoseWindow(window);
    }
  protected:
    DECLARE_MESSAGE_MAP()

  public:
  };



}