#pragma once

#include "TranspButton.h"

// CButtonWithNotify

class IButtonNotify
{
public:
  virtual void OnButtonPressed()=0;
  virtual void ArrowPressed(int code)=0;
  virtual void OnDraw() {}
};

class CButtonWithNotify : public CTranspButton
{

  IButtonNotify *notify;
public:
	CButtonWithNotify(IButtonNotify *ntf);
	virtual ~CButtonWithNotify();

protected:
	DECLARE_MESSAGE_MAP()

  afx_msg void OnPress();
  afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
};


