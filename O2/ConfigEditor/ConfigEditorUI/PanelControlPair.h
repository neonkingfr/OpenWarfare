#pragma once

#include "PanelControlWithLabel.h"
#include "TranspStatic.h"

namespace LindaStudio
{

  class IPanelControlPairNotify
  {
  public:
    virtual void DataChanged(float value, float random)=0;
  };

  class PanelControlPair : public CWnd, public PanelControlWithLabel
  {
    static const int PHeight=20;
    static const int PWidth=40;
    static const int MSldWidth=40;
    CTranspStatic _titleval;
    CTranspStatic _titlerand;
    CEdit _wVal;
    CEdit _wRand;
    CSliderCtrl _wValSld;
    CSliderCtrl _wRandSld;
    float _factor;    
    IPanelControlPairNotify *_notify;
    int _labelLen;
    bool _modify;
    bool _srcchange;
    CBrush _sldBgr;
  public:

    PanelControlPair(IPanelControlPairNotify *notify, const char *titleVal, const char *titleRand, float min, float max,DocumentBase<IPanelControlMsgs> *document, const char *labelText);

  protected:
    DECLARE_MESSAGE_MAP()
  
  protected:
    virtual bool GetPreferredSize(int &xs, int &ys);
    virtual bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys);
    virtual bool SetEnable(IPanelControlMsgs *what, bool newState);
    virtual bool SetVisible(IPanelControlMsgs *what, bool newState);
    virtual bool GoNextControl(IPanelControlMsgs *from, bool previous=false);  
    virtual HWND GetWindow() const;
    virtual bool SetFont(HFONT font);

    afx_msg void OnSize(UINT nType, int cx, int cy);
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnChangeVal();
    afx_msg void OnChangeRand();
    afx_msg void OnEditSetFocus();
    afx_msg void OnEditKillFocus();
    afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
    virtual void OnDocumentDropped()
    {
      DestroyWindow();
      delete this;
    }
    virtual IPanelControlMsgs *WhoseWindow(HWND window);


    void OnValueChange();
  public:
    void SetValues(float value, float random);
  };



}