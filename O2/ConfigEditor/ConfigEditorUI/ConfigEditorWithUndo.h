#pragma once
#include "..\configeditormodel\configeditor.h"

class ConfigEditorUndoContainer: public MVC::UndoRedo<ConfigMVC>
{
public:
  ConfigEditorUndoContainer(ConfigEditor *owner):MVC::UndoRedo<ConfigMVC>(*owner) {}
  virtual IConfigEntry::ServiceResult SetBase(const IConfigEntry *entry, const RStringI &baseName);
  virtual IConfigEntry::ServiceResult SetName(const IConfigEntry *entry, const RStringI &newName);
  virtual IConfigEntry::ServiceResult SetValue(const IConfigEntry *entry, const RString &value);
  virtual IConfigEntry::ServiceResult EditSymbolStream(int atPos, int toDelete, const Array<LexSymbDesc> &symbols);
  virtual IConfigEntry::ServiceResult Delete(const IConfigEntry *what=0);
  virtual IConfigActions::CombinedResult Move(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before=0);
};

class ConfigEditorWithUndo : public ConfigEditor
{
  ConfigEditorUndoContainer _undo;
public:
  ConfigEditorWithUndo():_undo(this) {}
  IConfigEntry::ServiceResult SetBase(const IConfigEntry *entry, const RStringI &baseName);
  IConfigEntry::ServiceResult SetName(const IConfigEntry *entry, const RStringI &newName);
  IConfigEntry::ServiceResult SetValue(const IConfigEntry *entry, const RString &value);
  CombinedResult AddValue(const IConfigEntry *subj, const IConfigEntry *before=0, bool newLine=false);
  CombinedResult AddArray(const IConfigEntry *subj, const IConfigEntry *before=0, bool newLine=true);
  CombinedResult AddValue(const IConfigEntry *subj, RStringI name, const IConfigEntry *before=0);
  CombinedResult AddArray(const IConfigEntry *subj, RStringI name, const IConfigEntry *before=0, bool newLine=true);
  CombinedResult AddClass(const IConfigEntry *subj,RStringI name, const IConfigEntry *before=0);
  CombinedResult AddClassLink(const IConfigEntry *subj,RStringI name, const IConfigEntry *before=0);
  IConfigEntry::ServiceResult Delete(const IConfigEntry *what=0);
  IConfigEntry::ServiceResult EditSymbolStream(int atPos, int toDelete, const Array<LexSymbDesc> &symbols);
  CombinedResult Move(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before=0);
  CombinedResult Copy(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before=0, bool onlyStructure=false); 
  virtual void BeginBatch()
  {
    _undo.OpenGroup();
  }
  virtual void EndGroup()
  {
    _undo.CloseGroup();
  }


};
