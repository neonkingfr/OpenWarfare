#include "StdAfx.h"
#include ".\panelcontainercloseable.h"
#include "PanelControlWithLabel.h"

#define ANIMATION_SPEED 200 

namespace LindaStudio
{
  PanelContainerCloseable::PanelContainerCloseable(IPanelContainerCloseable *notify,DocumentBase<IPanelControlMsgs> *document, const char *title,bool closed)
    :_titleButton(this),_server(this), _notify(notify)
  {
    _titleButton.Create("+",WS_VISIBLE|WS_CHILD|BS_CENTER|BS_VCENTER|BS_AUTOCHECKBOX|BS_PUSHLIKE,CRect(),CWnd::FromHandle(document->GetWindow()),0);
    _container.Create(WS_VISIBLE|WS_CHILD,CWnd::FromHandle(document->GetWindow()),0);
    _frameWithName.Create(title,WS_VISIBLE|WS_CHILD|BS_GROUPBOX,CRect(),CWnd::FromHandle(document->GetWindow()),-1);
    if (!closed) _titleButton.SetCheck(1);
    _container.AttachViewToParent(&_server);
    AttachDocument(document);
  }

  PanelContainerCloseable::~PanelContainerCloseable(void)
  {
  }

  bool PanelContainerCloseable::GetPreferredSize(int &xs, int &ys)
  {
    CSize sz=PanelControlWithLabel::GetTextExtent(&_titleButton,"W",false); 
    if (_titleButton.GetCheck()==0)
    {
      ys=sz.cy+GetSystemMetrics(SM_CYEDGE);
    }
    else
    {
      int xx=xs;
      int yy=0x7fff;
      _container.GetPreferredSize(xx,yy);
      yy+=sz.cy+GetSystemMetrics(SM_CYEDGE);
      ys=yy;
      if (xs<xx) xs=xx;
    }
    return true;
  }
  bool PanelContainerCloseable::SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys)
  {

    CSize sz=PanelControlWithLabel::GetTextExtent(&_titleButton,"W",false);
    int offsety=sz.cy+GetSystemMetrics(SM_CYEDGE);
    mw.SetNewPosition(_titleButton,0,y,sz.cx*2,sz.cy+GetSystemMetrics(SM_CYEDGE));
    mw.SetNewPosition(_frameWithName,sz.cx*2,y,xs-sz.cx*2,sz.cy+GetSystemMetrics(SM_CYEDGE));
    if (_titleButton.GetCheck()!=0)
    {
      _container.ShowWindow(SW_SHOW);
    }
    else
    {
      _container.ShowWindow(SW_HIDE);
    }

      CRect rc;
      _container.GetWindowRect(&rc);
      ys-=offsety;
      if (xs!=rc.right-rc.left || ys!=rc.bottom-rc.top)
        _container.SetNewLocation(mw,x,offsety+y,xs,ys);
      else
        mw.SetNewPosition(_container,x,offsety+y,xs,ys);    
    return true;
  }
  bool PanelContainerCloseable::SetEnable(IPanelControlMsgs *what, bool newState)
  {
    if (what==this)
    {
      _titleButton.EnableWindow(newState);
      _container.EnableWindow(newState);
      return true;
    }
    return false;
  }
  bool PanelContainerCloseable::SetVisible(IPanelControlMsgs *what, bool newState)
  {
    return false;
  }
  bool PanelContainerCloseable::GoNextControl(IPanelControlMsgs *from, bool previous)
  {
    if (previous)
    {
      _container.GetThisView().GoNextControl(from,previous);
    }
    else
    {
      _titleButton.SetFocus();
    }
    return true;
  }
  HWND PanelContainerCloseable::GetWindow() const
  {
    return _container;
  }
  bool PanelContainerCloseable::SetFont(HFONT fnt)
  {
    _titleButton.SetFont(CFont::FromHandle(fnt));
    _frameWithName.SetFont(CFont::FromHandle(fnt));
    _container.SetFont(fnt);
    return true;
  }

  IPanelControlMsgs *PanelContainerCloseable::WhoseWindow(HWND window)
  {
    if (window==_titleButton) return this;
    if (window==_container) return this;
    return _container.WhoseWindow(window);
  }


  bool PanelContainerCloseable::InsideGetPreferredSize(int &xs, int &ys)
  {
    return GetDocument()->GetPreferredSize(xs,ys);
  }
  bool PanelContainerCloseable::InsideSetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys)
  {
    return GetDocument()->SetNewLocation(mw,x,y,xs,ys);
  }
  bool PanelContainerCloseable::InsideSetEnable(IPanelControlMsgs *what, bool newState)
  {
    return GetDocument()->SetEnable(what,newState);
  }
  bool PanelContainerCloseable::InsideSetVisible(IPanelControlMsgs *what, bool newState)
  {
    return GetDocument()->SetVisible(what,newState);
  }
  bool PanelContainerCloseable::InsideGoNextControl(IPanelControlMsgs *from, bool previous)
  {  
    if (previous && _titleButton.GetCheck())
    {
      _titleButton.SetFocus();
    }
    else
    {
      GetDocument()->GoNextControl(this,previous);
    }
    return true;
  }
  HWND PanelContainerCloseable::InsideGetWindow() const
  {
    return _container;
  }
  bool PanelContainerCloseable::InsideSetFont(HFONT fnt)
  {
    return GetDocument()->SetFont(fnt);
  }
  IPanelControlMsgs *PanelContainerCloseable::InsideWhoseWindow(HWND window)
  {
    return GetDocument()->WhoseWindow(window);
  }

  void PanelContainerCloseable::OnButtonPressed()
  {
      CRect rc;
      _container.ShowWindow(SW_SHOW);
      _container.GetWindowRect(&rc);
      _container.GetParent()->ScreenToClient(&rc);
      CRect parentRc;
      _container.GetParent()->GetClientRect(&parentRc);
      int h1,v1,h2,v2;
      h1=rc.bottom-rc.top;
      v1=rc.right-rc.left;
      h2=h1;
      v2=v1;
      GetPreferredSize(v2,h2);
      DWORD ttime=ANIMATION_SPEED;
      DWORD btime=GetTickCount();
      DWORD ctime=btime;
      int lasth=h1;
      while (ctime<btime+ttime)
      {
        float f=(float)(ctime-btime)/(float)(ttime);
        int h=toLargeInt(h1+(h2-h1)*f);
        int v=toLargeInt(v1+(v2-v1)*f);
        _container.SetWindowPos(0,0,0,v,h,SWP_NOMOVE|SWP_NOZORDER);
        _container.UpdateWindow();
        int dy=h-lasth; 
        lasth=h;
        CRect scrrect(0,rc.top+h+(dy<0?dy:0),parentRc.right,parentRc.bottom);
        _container.GetParent()->ValidateRect(scrrect);
        _container.GetParent()->ScrollWindow(0,dy,&scrrect,&scrrect);
        ctime=GetTickCount();
      }
    GetDocument()->RecalcLayout();
    if (_notify) _notify->OpenCloseGroup(_titleButton.GetCheck()==0);
  }
  void PanelContainerCloseable::ArrowPressed(int code)
  {
    if (code==VK_UP) GetDocument()->GoNextControl(this,true);
    if (code==VK_DOWN) 
      if (_titleButton.GetCheck())            
        _container.GetThisView().GoNextControl(this,false);
      else
        GetDocument()->GoNextControl(this,false);
      
  }
  void PanelContainerCloseable::InsideRecalcLayout()
  {

  }

}