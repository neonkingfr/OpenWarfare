#include "StdAfx.h"
#include ".\panelview.h"
#include "PanelSubContainer.h"
#include "PanelControlEdit.h"
#include "SlaveContainerGrid.h"
#include "../ConfigEditorModel/ConfigClass.h"
#include "../ConfigEditorModel/ConfigValue.h"
#include "../ConfigEditorModel/ConfigNamedArray.h"
#include "../ConfigEditorModel/ConfigArrayValue.h"
#include "../ConfigEditorModel/ConfigDefinition.h"

PanelView::PanelView(PanelContainer &panel):_panel(panel)
{  
}

PanelView::~PanelView(void)
{
}

bool PanelView::UpdateMe(ConfigMVC::CurModel *model)
{
  _panel.SetSpace(0);
  _panel.SetMinHeight(0);
  _panel.Clear();
  _containers.Clear();
  _panelItems.Clear();
  LoadSubpanel(&_panel,GetModel()->GetRoot());
  _panel.SetFont((HFONT)GetStockObject(DEFAULT_GUI_FONT));
  _panel.ReLayout();
  
  return true;
}

/*IPanelControlMsgs *PanelView::GetPanelToItem(const RStringI &item)
{
  PanelItemInfo search(item,0,0);
  PPanelItemInfo *found=_panelItems.Find(PPanelItemInfo(&search));
  if (found) return found->GetRef()->GetItem();
  else return 0;
}*/

/*PanelContainer *PanelView::GetItemContainer(const IConfigEntry *subj)
{
  IConfigEntry *parent=subj->GetParent();
  RStringI parname=parent->GetFullName();
  if (parname==RStringI("$")) return &_panel;
  PanelContainer *papan;
  IPanelControlMsgs *item=GetPanelToItem(parname);
  if (item) papan=static_cast<PanelSubcontainer *>(item)->GetContainer();
  else  papan=0;
  return papan;
}*/

/*bool PanelView::IsAlreadyLoaded(const IConfigEntry *subj)
{
  return GetPanelToItem(subj->GetFullName())==0;
}*/


PanelView::CombinedResult PanelView::AddValue(const IConfigEntry *subj, const IConfigEntry *before, bool newLine)
{

  return IConfigEntry::srOk;
}
PanelView::CombinedResult PanelView::AddArray(const IConfigEntry *subj, const IConfigEntry *before, bool newLine)
{

  return IConfigEntry::srOk;
}
PanelView::CombinedResult PanelView::AddValue(const IConfigEntry *subj, RStringI name, const IConfigEntry *before)
{
  /*
  PanelContainer *cont=GetItemContainer(subj);
  if (cont==0)
  { 
    PanelControlEdit *i=new PanelControlEdit(cont,name,0);
    _panelItems.Add(new PanelItemInfo(subj->GetFullName(),static_cast<ConfigEditor *>(GetModel()),i));
  }*/
  return IConfigEntry::srOk;
}
PanelView::CombinedResult PanelView::AddArray(const IConfigEntry *subj,RStringI name, const IConfigEntry *before, bool newLine)
{

  return IConfigEntry::srOk;
}
PanelView::CombinedResult PanelView::AddClass(const IConfigEntry *subj,RStringI name, const IConfigEntry *before)
{  
  /*
  PanelContainer *cont=GetItemContainer(subj);
  if (cont==0)
  { 
      SubPanelInfo *subPanel=new SubPanelInfo(this,subj->GetFullName(),static_cast<ConfigEditor *>(GetModel()),0);
      PanelSubcontainer *group=new PanelSubcontainer(subPanel,cont,name,true);  
      subPanel->SetItem(group);
      _panelItems.Add(subPanel);
      group->SetBkColor(RGB(rand()*64/RAND_MAX+192,rand()*64/RAND_MAX+192,rand()*64/RAND_MAX+192));
  }*/
  return IConfigEntry::srOk;
}
PanelView::CombinedResult PanelView::AddClassLink(const IConfigEntry *subj,RStringI name, const IConfigEntry *before)
{ 
  return AddClass(subj,name,before);
}
IConfigEntry::ServiceResult PanelView::Delete(const IConfigEntry *what)
{
  
  return IConfigEntry::srOk;
}
PanelView::CombinedResult PanelView::Copy(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before, bool onlyStructure)
{

  return IConfigEntry::srOk;
}
PanelView::CombinedResult PanelView::Move(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before)
{

  return IConfigEntry::srOk;
}
IConfigEntry::ServiceResult PanelView::EditSymbolStream(int atPos, int toDelete, const Array<LexSymbDesc> &symbols)
{

  return IConfigEntry::srOk;
}

void PanelView::LoadSubpanel(PanelContainer *subPanel)
{
  if (subPanel->GetCountRegistredViews()==0)
  {
    RStringI itemName=FindItemByContaner(subPanel);
    if (itemName.GetLength())
    {
      LoadSubpanel(subPanel,GetModel()->Find(itemName));
      subPanel->ForceRecalcNext();
    }
  }
}



void PanelView::LoadSubpanel(PanelContainer *subPanel,const IConfigEntry *entry)
{
  class LoadPanelEnumerator: public IConfigSubItemEnumerator
  {
    mutable PanelContainer *container;
    mutable PanelView &owner;
  public:
    LoadPanelEnumerator(PanelContainer *container, PanelView &owner):container(container),owner(owner) {}

    bool operator()(const IConfigEntry *entry) const
    {
      owner.AddClassAndItems(entry,container);
      return false;
    }
  };

  if (entry)
    entry->ForEach(LoadPanelEnumerator(subPanel,*this));
  subPanel->SetFont((HFONT)GetStockObject(DEFAULT_GUI_FONT));
}

const RStringI PanelView::FindItemByContaner(PanelContainer *cont)
{
  const SubPanelInfo **nfo=_containers.Find(&SubPanelInfo(this,0,0,0,cont));
  if (nfo) return (*nfo)->GetName();
  else
    return 0;
}

static RString DOSLines(const RString &lines)
{
  int ln=0;
  for (int i=0,cnt=lines.GetLength();i<cnt;i++) if (lines[i]=='\n') ln++;
  RString buff;
  char *k=buff.CreateBuffer(lines.GetLength()+ln);
  for (int i=0,cnt=lines.GetLength();i<cnt;i++) {if (lines[i]=='\n') *k++='\r';*k++=lines[i];}
  return buff;  
}

void PanelView::AddClassAndItems(const IConfigEntry *entry, PanelContainer *container)
{
  if (dynamic_cast<const ConfigClass *>(entry))
  {
    PanelView::SubPanelInfo *subPanel=new PanelView::SubPanelInfo(this,entry->GetFullName(),static_cast<ConfigEditor *>(GetModel()),0);
    PanelSubcontainer *group=new PanelSubcontainer(subPanel,container,entry->GetName(),"class",true);  
    subPanel->SetItem(group);
    _panelItems.Add(subPanel);  
    _containers.Add(subPanel);
    group->SetBkColor(RGB(rand()*64/RAND_MAX+192,rand()*64/RAND_MAX+192,rand()*64/RAND_MAX+192));
    group->GetContainer()->SetSpace(0);
    group->GetContainer()->SetMinHeight(0);
  }
  else if (dynamic_cast<const ConfigValue *>(entry)) 
  {
    PanelControlEdit *i=new PanelControlEdit(container,entry->GetName(),0);
    _panelItems.Add(new PanelItemInfo(entry->GetFullName(),static_cast<ConfigEditor *>(GetModel()),i));
    i->SetWindowText(entry->GetValue());
  }
  else if (dynamic_cast<const ConfigArrayValue *>(entry))
  {
    PanelControlEdit *i=new PanelControlEdit(container,"",0);
    _panelItems.Add(new PanelItemInfo(entry->GetFullName(),static_cast<ConfigEditor *>(GetModel()),i));
    i->SetWindowText(entry->GetValue());
  }
  else if (dynamic_cast<const ConfigNamedArray *>(entry))
  { 
    SlaveContainerGrid *cont=new SlaveContainerGrid(0,150);
    PanelView::SubPanelInfo *subPanel=new PanelView::SubPanelInfo(this,entry->GetFullName(),static_cast<ConfigEditor *>(GetModel()),0);
    PanelSubcontainer *group=new PanelSubcontainer(subPanel,container,entry->GetName()+"  {...}","array",true,cont);  
    cont->SetOwner(group);
    subPanel->SetItem(group);
    _panelItems.Add(subPanel);
    _containers.Add(subPanel);
    group->SetBkColor(RGB(rand()*64/RAND_MAX+192,rand()*64/RAND_MAX+192,rand()*64/RAND_MAX+192));
    group->GetContainer()->IMultiInterfaceBase::GetInterface<IPanelControlWithLabelContainerInfo>()->SetLabelSize(0);
    group->GetContainer()->SetSpace(0);
    group->GetContainer()->SetMinHeight(0);
  }
  else if (dynamic_cast<const ConfigArray *>(entry))
  {
    SlaveContainerGrid *cont=new SlaveContainerGrid(0,150);
    PanelView::SubPanelInfo *subPanel=new PanelView::SubPanelInfo(this,entry->GetFullName(),static_cast<ConfigEditor *>(GetModel()),0);
    PanelSubcontainer *group=new PanelSubcontainer(subPanel,container,"{...}","array",true,cont);  
    subPanel->SetItem(group);
    cont->SetOwner(group);
    _panelItems.Add(subPanel);
    _containers.Add(subPanel);
    group->SetBkColor(RGB(rand()*64/RAND_MAX+192,rand()*64/RAND_MAX+192,rand()*64/RAND_MAX+192));
    group->GetContainer()->IMultiInterfaceBase::GetInterface<IPanelControlWithLabelContainerInfo>()->SetLabelSize(0);
    group->GetContainer()->SetSpace(0);
    group->GetContainer()->SetMinHeight(0);
 }
  else if (dynamic_cast<const ConfigDefinition *>(entry))
  {
    PanelControlEdit *i=new PanelControlMultilineEdit(container,entry->GetName(),0,5,false);
    _panelItems.Add(new PanelItemInfo(entry->GetFullName(),static_cast<ConfigEditor *>(GetModel()),i));
    i->SetWindowText(DOSLines(entry->GetValue()));

  }

}