#include "StdAfx.h"
#include ".\panelcontrolyesno.h"
#include "PanelContainer.h"

namespace LindaStudio
{


  PanelControlYesNo::PanelControlYesNo(DocumentBase<IPanelControlMsgs> *document, const char *labelText,IPanelControlYesNoNotify *notify):
    _notify(notify),_button(this),PanelControlWithLabel(document,labelText)
  {
    _button.Create("     ",BS_AUTOCHECKBOX|WS_CHILD|WS_VISIBLE,CRect(0,0,0,0),
      CWnd::FromHandle(GetDocument()->GetWindow()),0);
  }

  PanelControlYesNo::~PanelControlYesNo(void)
  {    
  }

  void PanelControlYesNo::OnDocumentDropped()
  {
    if (_notify) _notify->DeleteControl();
    delete this;
  }

  bool PanelControlYesNo::GetPreferredSize(int &xs, int &ys)
  {
    CSize sz;
    if (_button.GetIdealSize(&sz)==FALSE)
    {
      sz.cx=0;
      sz.cy=0;
    }
    ys=sz.cy;
    int ax=xs;
    PanelControlWithLabel::GetPreferredSize(ax,ys);
    return true;
  }
  bool PanelControlYesNo::SetNewLocation(IPanelWindowManipulator &mw,int x, int y, int xs, int ys)
  {
    int ax=xs;
    int ay=ys;
    PanelControlWithLabel::GetPreferredSize(ax,ay);
    PanelControlWithLabel::SetNewLocation(mw,x,y,ax,ay);
    x+=ax;
    xs-=ax;
    mw.SetNewPosition(_button,x,y,xs,ys);
    return true;
  }
  bool PanelControlYesNo::SetEnable(IPanelControlMsgs *what, bool newState)
  {
    if (what==this)
    {
      _button.EnableWindow(newState);
      PanelControlWithLabel::SetEnable(what,newState);
      return true;
    }
    else
      return false;
  }
  bool PanelControlYesNo::SetVisible(IPanelControlMsgs *what, bool newState)
  {
    if (what==this)
    {
      _button.ShowWindow(newState?SW_SHOW:SW_HIDE);
      PanelControlWithLabel::SetVisible(what,newState);
      return true;
    }
    else
      return false;
  }
  bool PanelControlYesNo::GoNextControl(IPanelControlMsgs *from, bool previous)
  {
    _button.SetFocus();
    return true;
  }
  HWND PanelControlYesNo::GetWindow() const
  {
    return _button;   
  }

  void PanelControlYesNo::OnButtonPressed()
  {
    if (_notify) _notify->OnValueChange(_button.GetCheck()!=0);
  }
  void PanelControlYesNo::ArrowPressed(int code)
  {
    if (code==VK_UP) GetDocument()->GoNextControl(this,true);
    else if (code==VK_DOWN) GetDocument()->GoNextControl(this,false);
  }


  /*
  void PanelControlYesNo::RecalulateMinHeight()
  {
    __super::RecalculateLabelSize();
    CClientDC dc(&_button);
    CFont *fnd=dc.SelectObject(_button.GetFont());
    CSize sz=dc.GetTextExtent("W",1);
    dc.SelectObject(fnd);
    _minHeight=sz.cy+2*GetSystemMetrics(SM_CYEDGE);    
  }
  */

}