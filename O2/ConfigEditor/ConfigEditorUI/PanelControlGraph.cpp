#include "StdAfx.h"
#include ".\panelcontrolgraph.h"
#include "LindaEdit.h"
#include "PanelContainer.h"



namespace LindaStudio
{

  RString ReadGraphValues(const Simple2DGraph &_graph);
  RString ReadGraphValuesPoly(const Simple2DGraph &_graph);

  PanelControlGraph::PanelControlGraph(IPanelControlGraph *notify, 
    const char *text, 
    DocumentBase<IPanelControlMsgs> *container, 
    const char *labelText):PanelControlSingleButton(notify,text,container,labelText),
    _graph(this)
  {
    _graph.Create(0,WS_CHILD|WS_VISIBLE,CWnd::FromHandle(container->GetWindow()),0);
    _graph.SetMargin(CRect(2,2,2,2));

  }


  PanelControlGraph::~PanelControlGraph(void)
  {

  }


  bool PanelControlGraph::GetPreferredSize(int &xs, int &ys)
  {
    int xx=xs;
    PanelControlSingleButton::GetPreferredSize(xs,ys);
    if (ys<64) ys=64;
    if (xx-xs<50) xs=xx+50;
    return true;
  }

  bool PanelControlGraph::SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys)
  {
    int xx=xs,yy=ys;
    PanelControlSingleButton::GetPreferredSize(xx,yy);
    PanelControlSingleButton::SetNewLocation(mw,x,y+ys-yy,xs,yy);
    mw.SetNewPosition(_graph,xx,y,xs-xx,ys);
    return true;
  }

  void PanelControlGraph::UpdateGraph(const RString &from)
  {
    AutoArray<float,MemAllocLocal<float,256> > pole;
    CScriptParser::ParseArrayOfFloats(from,pole);

    AutoArray<float,MemAllocLocal<float,256> > interleaved;
    for (int i=1;i<pole.Size()-1;i+=3)
    {
      interleaved.Add(pole[i]);
      interleaved.Add(pole[i+1]);
    }

    _graph.LoadPointsXY(interleaved);
  }

  bool PanelControlGraph::SetEnable(IPanelControlMsgs *what, bool newState)
  {
    if (PanelControlSingleButton::SetEnable(what,newState))
    {
      _graph.EnableWindow(newState);
      return true;
    }
    else
     return false;
  }
  bool PanelControlGraph::SetVisible(IPanelControlMsgs *what, bool newState)
  {
    if (PanelControlSingleButton::SetVisible(what,newState))
    {
      _graph.ShowWindow(newState?SW_SHOW:SW_HIDE);
      return true;
    }
    else
      return false;
  }

  bool PanelControlGraph::SetFont(HFONT font)
  {
    if (PanelControlSingleButton::SetFont(font))
    {
      _graph.SetFont(CFont::FromHandle(font),TRUE);
      return true;
    }
    else
      return false;
  }

  void PanelControlGraph::GraphChanged()
  {
    RString str=_graph.ReadGraphValues(_graph);
    static_cast<IPanelControlGraph *>(_notify)->GraphChanged(str);
  }


  void PanelControlGraphPoly::UpdateGraph(const RString &from)
  {
    AutoArray<float,MemAllocLocal<float,256> > pole;
    CScriptParser::ParseArrayOfFloats(from,pole);

    AutoArray<float,MemAllocLocal<float,256> > interleaved;
    for (int i=0;i<pole.Size();i++)
    {
      interleaved.Add((float)i);
      interleaved.Add(pole[i]);
    }

    _graph.LoadPointsXY(interleaved);
  }

  void PanelControlGraphPoly::GraphChanged()
  {
    RString str=_graph.ReadGraphValuesPoly(_graph);
    static_cast<IPanelControlGraph *>(_notify)->GraphChanged(str);
  }
}
