// TranspStatic.cpp : implementation file
//

#include "stdafx.h"
//#include "LindaStudio.h"
#include "TranspStatic.h"
#include ".\transpstatic.h"


// CTranspStatic

IMPLEMENT_DYNAMIC(CTranspStatic, CStatic)
CTranspStatic::CTranspStatic()
{
}

CTranspStatic::~CTranspStatic()
{
}


BEGIN_MESSAGE_MAP(CTranspStatic, CStatic)
  ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()



// CTranspStatic message handlers


HBRUSH CTranspStatic::CtlColor(CDC* pDC, UINT /*nCtlColor*/)
{

  // TODO:  Change any attributes of the DC here

  // TODO:  Return a non-NULL brush if the parent's handler should not be called
  pDC->SetBkMode(TRANSPARENT);
  return (HBRUSH)GetStockObject(HOLLOW_BRUSH);
}
