#pragma once

#include "../ConfigEditorModel/ConfigEditor.h"
#include "PanelSubcontainer.h"



class PanelItemInfo
{
protected:
  RStringI _name;
  ConfigEditor *_editor;
  IPanelControlMsgs *_registredControl;
public:
  virtual void Update() {};
  virtual ~PanelItemInfo() {}

  PanelItemInfo(RStringI itemName, ConfigEditor *editorRef, IPanelControlMsgs  *registredControl):_name(itemName),_editor(editorRef),_registredControl(registredControl)
  {

  }

  void operator delete(void *p)
  {
    if (reinterpret_cast<PanelItemInfo *>(p)->_editor!=0) ::operator delete(p);    
  }

  int Compare(const PanelItemInfo &other) const
  {
    int c=_name.Cmp(other._name);
    if (c==0)    
      return (_registredControl>other._registredControl)-(_registredControl<other._registredControl);
    else
      return c;
  }

  RStringI GetName() const {return _name;}
  IPanelControlMsgs *GetItem() const {return _registredControl;}

  void SetItem(IPanelControlMsgs *registredControl) {_registredControl=registredControl;}
};


typedef SRef<PanelItemInfo> PPanelItemInfo;

TypeIsMovable(PPanelItemInfo);

class PanelView : public MVC::View<ConfigMVC>
{
  PanelContainer &_panel;

  class SubPanelInfo: public PanelItemInfo, public IPanelSubcontainer
  {
    PanelView *outer;
    PanelContainer *container;
  public:
    SubPanelInfo(PanelView *outer,RStringI itemName, ConfigEditor *editorRef, IPanelControlMsgs  *registredControl,PanelContainer *container=0):
      PanelItemInfo(itemName,editorRef,registredControl),outer(outer),container(container) {}

      virtual bool OnBeforeOpenClose(bool open) {if (open) outer->LoadSubpanel(static_cast<PanelSubcontainer *>(GetItem())->GetContainer());return true;}
      virtual bool OnAfterOpenClose(bool open) {return false;}

      int Compare(const SubPanelInfo &other) const
      {
        return (container>other.container)-(container<other.container);
      }


      void SetItem(PanelSubcontainer *control) 
      {
        _registredControl=control;
        container=control->GetContainer();
      }
  };


  BTree<PPanelItemInfo,BTreeRefCompare> _panelItems;
  BTree<const SubPanelInfo *,BTreeRefCompare> _containers;
//  IPanelControlMsgs *GetPanelToItem(const RStringI &item);
  PanelContainer *_loadContainer;

protected:
//  PanelContainer *GetItemContainer(const IConfigEntry *subj);//
  const RStringI FindItemByContaner(PanelContainer *cont);
//  bool IsAlreadyLoaded(const IConfigEntry *subj);
  void LoadSubpanel(PanelContainer *subPanel);
  void LoadSubpanel(PanelContainer *subPanel,const IConfigEntry *entry);
public:
  PanelView(PanelContainer &panel);
  ~PanelView(void);



  friend class LoadPanelEnumerator;
  friend class SubPanelInfo;

  virtual bool UpdateMe(ConfigMVC::CurModel *model);

  virtual CombinedResult AddValue(const IConfigEntry *subj, const IConfigEntry *before=0, bool newLine=false);
  virtual CombinedResult AddArray(const IConfigEntry *subj, const IConfigEntry *before=0, bool newLine=true);
  virtual CombinedResult AddValue(const IConfigEntry *subj, RStringI name, const IConfigEntry *before=0);
  virtual CombinedResult AddArray(const IConfigEntry *subj,RStringI name, const IConfigEntry *before=0, bool newLine=true);
  virtual CombinedResult AddClass(const IConfigEntry *subj,RStringI name, const IConfigEntry *before=0);
  virtual CombinedResult AddClassLink(const IConfigEntry *subj,RStringI name, const IConfigEntry *before=0);
  virtual IConfigEntry::ServiceResult Delete(const IConfigEntry *what=0);
  virtual CombinedResult Copy(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before=0, bool onlyStructure=false);
  virtual CombinedResult Move(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before=0);
  virtual IConfigEntry::ServiceResult EditSymbolStream(int atPos, int toDelete, const Array<LexSymbDesc> &symbols);


  void AddClassAndItems(const IConfigEntry *cls, PanelContainer *container);
};
