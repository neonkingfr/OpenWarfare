// Simple2DGraph.cpp : implementation file
//

#include "stdafx.h"
//#include "LindaStudio.h"
#include "Simple2DGraph.h"
#include ".\simple2dgraph.h"
#include <es/algorithms/qsort.hpp>
#include "resource.h"


using namespace std;

// Simple2DGraph

Simple2DGraph::Simple2DGraph(ISimple2DGraphNotify *notify):
  _swappedAxis(false),_xMin(0),_xMax(1),_yMin(0),_yMax(1),_notify(notify)    
{
  LOGBRUSH lgb;
  lgb.lbHatch=0;
  lgb.lbStyle=BS_SOLID;
  _lineColor.CreatePen(PS_SOLID,1,GetSysColor(COLOR_WINDOWTEXT));
  _pointColor.CreatePen(PS_SOLID,1,GetSysColor(COLOR_WINDOWTEXT));
  _pointSelFill.CreateSolidBrush(GetSysColor(COLOR_HIGHLIGHT));
  _pointSelColor.CreatePen(PS_SOLID,1,GetSysColor(COLOR_HIGHLIGHTTEXT));
  lgb.lbColor=GetSysColor(COLOR_GRAYTEXT);
  _axisColor.CreatePen(PS_ALTERNATE,1,&lgb,0,0); 
  _pointSize=2 ;
   _margin=CRect(16,8,8,16);
   _fontColor=GetSysColor(COLOR_GRAYTEXT);
   lgb.lbColor=GetSysColor(COLOR_HIGHLIGHT);
   _selectionRect.CreatePen(PS_ALTERNATE,1,&lgb);
   _constStep=false;
   
}

Simple2DGraph::~Simple2DGraph()
{

}


BEGIN_MESSAGE_MAP(Simple2DGraph, CWnd)
  ON_WM_PAINT()
  ON_WM_SIZE()
  ON_MESSAGE(WM_SETFONT,OnSetFont)
  ON_WM_LBUTTONDOWN()
  ON_WM_LBUTTONUP()
  ON_WM_MOUSEMOVE()
  ON_WM_CONTEXTMENU()
  ON_WM_LBUTTONDBLCLK()
  ON_WM_SETCURSOR()
  ON_WM_MBUTTONDOWN()
  ON_WM_MOUSEWHEEL()
  ON_WM_RBUTTONDOWN()
  ON_WM_RBUTTONUP()
  ON_COMMAND(ID_GRAPHMENU_SWAPAXIS,OnSwapAxis)
END_MESSAGE_MAP()



// Simple2DGraph message handlers

namespace Functors
{
  class SortPointsComp
  {
    Array<Simple2DGraph::PointXY> _points;
  public:
    SortPointsComp(const Array<Simple2DGraph::PointXY> points):_points(points) {}
    int operator()(const int *a,const int *b) const
    {
      return (_points[*a].x>_points[*b].x)-(_points[*a].x<_points[*b].x); 
    }
  };
}

static void CalculateNaturalStep(float fmin, float fmax, float startMin, int axisSize, float &start, float &step)
{
  int steps=axisSize/30;
  if (steps<4) steps=4;
  float diff=fmax-fmin;
  float dlog=floor(log10(diff))-1;
  float d1step=(float)step=pow(10.0f,dlog);
  if (diff/step>steps) 
  {
    step=d1step*2;
    if (diff/step>steps) 
    {
      step=d1step*5;
      if (diff/step>steps) 
      {
        step=d1step*10;
        if (diff/step>steps) step=d1step*20;
      }
    }
  }

  start=(float)ceil(startMin/step)*step;


}

static CPoint ClipTextByRect(CRect rect, CPoint pt)
{
  if (rect.PtInRect(pt)==TRUE) return pt;
  if (pt.x<rect.left && pt.y<rect.bottom && pt.y>rect.top) 
  {
    return CPoint(rect.left,pt.y);
  }
  if (pt.y>rect.bottom && pt.x>rect.left && pt.x<rect.right)
  {
    return CPoint(pt.x,rect.bottom);
  }
  return pt;
}

void Simple2DGraph::Redraw(bool force)
{
  CRect rc;
  GetClientRect(&rc);
  if (rc.bottom<1 || rc.right<1)
  {
    rc.right=16;
    rc.bottom=16;
  }
  
  CClientDC dc(this);
  CDC draw;
  draw.CreateCompatibleDC(&dc);
  if (_doubleBuff.GetSafeHandle()!=0)
  {
    _doubleBuff.DeleteObject();
  }
  _doubleBuff.CreateCompatibleBitmap(&dc,rc.right,rc.bottom);
  CBitmap *old=draw.SelectObject(&_doubleBuff);

  CPen *savepen=draw.GetCurrentPen();  
  CBrush *savebrush=draw.GetCurrentBrush();
  CFont *savefont=draw.GetCurrentFont();

  draw.SelectObject(CFont::FromHandle(_font));

  Transform trns=GetCurrentTransform(); 

  draw.FillRect(&rc,&CBrush(GetSysColor(COLOR_WINDOW)));

  draw.SelectObject(&_axisColor);
  draw.SetTextColor(_fontColor);
  draw.SetBkMode(TRANSPARENT);
  draw.SetTextAlign(TA_BASELINE|TA_LEFT);

  Transform zoomBack=_zoomTransform.Inverted();

  PointXY minPt=zoomBack.Trns(PointXY(_xMin,_yMin)),
          maxPt=zoomBack.Trns(PointXY(_xMax,_yMax));
  

  float aStart,aStep,aStop;
  CalculateNaturalStep(minPt.x,maxPt.x,__max(_xMin,minPt.x),_swappedAxis?(rc.bottom-rc.top):(rc.right-rc.left),aStart,aStep);
  char buff[50];
  
  aStop=__min(_xMax,maxPt.x)+0.001f;

  if (aStep>0.00001)
  for (float z=aStart;z<=aStop;z+=aStep)
  {
    CPoint pos=trns(z,_yMin);
    CPoint pos2=trns(z,_yMax);
    draw.MoveTo(pos.x,pos.y);
    draw.LineTo(pos2.x,pos2.y);
    _snprintf(buff,sizeof(buff),"%g",z);
    pos=ClipTextByRect(rc,pos);
    draw.TextOut(pos.x,pos.y,buff,strlen(buff));
  }

  CalculateNaturalStep(minPt.y,maxPt.y,__max(_yMin,minPt.y),_swappedAxis?(rc.right-rc.left):(rc.bottom-rc.top),aStart,aStep);

  aStop=__min(_yMax,maxPt.y)+0.001f;
  
  if (aStep>0.00001)
  for (float z=aStart;z<=aStop;z+=aStep)
  {
    CPoint pos=trns(_xMin,z);
    CPoint pos2=trns(_xMax,z);
    draw.MoveTo(pos.x,pos.y);
    draw.LineTo(pos2.x,pos2.y);
    _snprintf(buff,sizeof(buff),"%g",z); 
    pos=ClipTextByRect(rc,pos);
    draw.TextOut(pos.x,pos.y,buff,strlen(buff));
  }

  draw.SetTextColor(GetSysColor(COLOR_WINDOWTEXT)); 
  draw.SetTextAlign(TA_BOTTOM|TA_CENTER);
  draw.TextOut((rc.right+rc.left)/2,rc.bottom,_swappedAxis?_yTitle:_xTitle);  
  draw.SelectObject(&_rotatedFont);
  draw.SetTextAlign(TA_TOP|TA_CENTER);
  draw.TextOut(rc.left,(rc.bottom+rc.top)/2,_swappedAxis?_xTitle:_yTitle);  
  draw.SelectObject(CFont::FromHandle(_font));

  if (_points.Size()>0)
  {   
    int *order=(int *)alloca(_points.Size()*sizeof(int));
    for (int i=0;i<_points.Size();i++) order[i]=i;
    QSort(order,_points.Size(),Functors::SortPointsComp(_points));
    draw.SelectObject(&_lineColor);

    draw.MoveTo(trns(_xMin,_points[order[0]].y));
    for (int i=0;i<_points.Size();i++)
    {
      draw.LineTo(trns(_points[order[i]]));
    }
    draw.LineTo(trns(_xMax,_points[order[_points.Size()-1]].y));
  }

  draw.SelectObject(&_pointColor);
  draw.SelectStockObject(HOLLOW_BRUSH);
  for (int i=0;i<_points.Size();i++) if (!_points[i].selected)
  {
    CPoint pos=trns(_points[i]);
    draw.Ellipse(pos.x-_pointSize,pos.y-_pointSize,pos.x+_pointSize+1,pos.y+_pointSize+1);
  }
  draw.SelectStockObject(NULL_PEN); 
  draw.SelectObject(&_pointSelFill);
  for (int i=0;i<_points.Size();i++) if (_points[i].selected)
  {
    CPoint pos=trns(_points[i]);
    draw.Ellipse(pos.x-_pointSize-2,pos.y-_pointSize-2,pos.x+_pointSize+3,pos.y+_pointSize+3 );
  }
  draw.SelectObject(savepen);
  draw.SelectObject(savebrush);
  draw.SelectObject(old);
  draw.SelectObject(savefont);
  Invalidate();
  if (force) UpdateWindow();
}


void Simple2DGraph::OnPaint()
{
  CPaintDC dc(this); 

  if (_doubleBuff.GetSafeHandle()==0)
    Redraw();

  CDC buff;
  buff.CreateCompatibleDC(&dc);
  CBitmap *old=buff.SelectObject(&_doubleBuff);
  
  CRect rc;
  GetClientRect(&rc);
  dc.BitBlt(rc.left,rc.top,rc.Size().cx,rc.Size().cy,&buff,0,0,SRCCOPY);
  buff.SelectObject(old);

  if (GetCapture()==this && _dragPt==-1)
  {
    CPen *oldPen=dc.SelectObject(&_selectionRect);
    dc.SelectStockObject(HOLLOW_BRUSH);
    dc.Rectangle(_dragRect);
    dc.SelectObject(oldPen);
  }
}

void Simple2DGraph::LoadPointsXY(const Array<float> &pointsInterleaved)
{
  _points.Clear();
  for (int i=0;i<pointsInterleaved.Size();i+=2)
  {
    float x=pointsInterleaved[i];
    float y=pointsInterleaved[i+1];
    _points.Add(PointXY(x,y));
  }
  CheckPointRanges();
  Redraw();  
}

void Simple2DGraph::ReplacePointsXY(const Array<float> &pointsInterleaved)
{    
  for (int i=0;i<pointsInterleaved.Size();i+=2)
  {
    float x=pointsInterleaved[i];
    float y=pointsInterleaved[i+1];
    int index=i/2;
    if (index<_points.Size())
    {
      _points[index].x=x;
      _points[index].y=y;
    }
    else
      _points.Add(PointXY(x,y));
    
  }
  if (pointsInterleaved.Size()/2<_points.Size())
  {
    _points.Resize(pointsInterleaved.Size()/2);
  }
  CheckPointRanges();
  Redraw();  
}


void Simple2DGraph::LoadPointsY(const Array<float> &points)
{
  _points.Clear();
  for (int i=0;i<points.Size();i++)
  {
    float x=(points.Size()?(float)i/(float)(points.Size()-1):0)*(_xMax-_xMin)+_xMin;
    float y=points[i];
    if (y<_yMin) y=_yMin;
    if (y>_yMax) y=_yMax;
    _points.Add(PointXY(x,y));
  }
  Redraw();  
}

void Simple2DGraph::SavePoints(Array<float> &_pointsInterleaved, bool sorted) const
{
  int *order=(int *)alloca(_points.Size()*sizeof(int));
  for (int i=0;i<_points.Size();i++) order[i]=i;
  if (sorted) QSort(order,_points.Size(),Functors::SortPointsComp(_points));
  for (int i=0;i<_points.Size();i++)
  {
    _pointsInterleaved[i*2]=(_points[order[i]].x);
    _pointsInterleaved[i*2+1]=(_points[order[i]].y);
  }
}

Simple2DGraph::Transform::Transform(float xMin, float xMax, float yMin, float yMax, int canvasWidth, int canvasHeight,int offsetX, int offsetY,bool swapAxis)
{
  if (swapAxis)
  {
    a21=canvasWidth/(yMax-yMin);
    a12=canvasHeight/(xMin-xMax);
    a11=0;
    a22=0;
    a31=-yMin*a21+offsetX;
    a32=-xMax*a12+offsetY;
  }
  else
  {  
    a11=canvasWidth/(xMax-xMin);
    a22=canvasHeight/(yMin-yMax);
    a12=0;
    a21=0;
    a31=-xMin*a11+offsetX;
    a32=-yMax*a22+offsetY;
  }

}

Simple2DGraph::PointXY Simple2DGraph::Transform::Trns(const PointXY &point)
{
  return PointXY(point.x*a11+point.y*a21+a31,point.x*a12+point.y*a22+a32);
}


CPoint Simple2DGraph::Transform::operator()(float x, float y) const
{
  float xi=x*a11+y*a21+a31;
  float yi=x*a12+y*a22+a32;
  return CPoint(toLargeInt(xi),toLargeInt(yi));
}


CPoint Simple2DGraph::Transform::operator()(const PointXY &point) const
{
  float xi=point.x*a11+point.y*a21+a31;
  float yi=point.x*a12+point.y*a22+a32;
  return CPoint(toLargeInt(xi),toLargeInt(yi));
}

Simple2DGraph::PointXY Simple2DGraph::Transform::operator()(const CPoint &point) const
{
  float xi=point.x*a11+point.y*a21+a31;
  float yi=point.x*a12+point.y*a22+a32;
  return Simple2DGraph::PointXY(xi,yi);
}

BOOL Simple2DGraph::Create(DWORD ex_styles, DWORD styles, CWnd *parent, int nID)
{
  return CreateEx(ex_styles,AfxRegisterWndClass(CS_DBLCLKS|CS_HREDRAW|CS_VREDRAW),"",styles|WS_CHILD|WS_VISIBLE,CRect(0,0,0,0),parent,nID);
}

void Simple2DGraph::OnSize(UINT nType, int cx, int cy)
{
  Redraw();
}

LRESULT Simple2DGraph::OnSetFont(WPARAM wParam, LPARAM lParam)
{
  _font=(HFONT)wParam;
  LOGFONT lg;

  CFont::FromHandle(_font)->GetLogFont(&lg);
  if (_rotatedFont.GetSafeHandle()!=0) _rotatedFont.DeleteObject();
  lg.lfOrientation=900;
  lg.lfEscapement=900;
  _rotatedFont.CreateFontIndirect(&lg);
  if (LOWORD(lParam)) Redraw();
  return 0;
}

Simple2DGraph::Transform Simple2DGraph::GetCurrentTransform() const
{
  CRect rc;
  GetClientRect(&rc);
  return _zoomTransform*Transform(_xMin,_xMax,_yMin,_yMax,rc.right-(_margin.left+_margin.right),rc.bottom-(_margin.top+_margin.bottom),_margin.left,_margin.top,_swappedAxis);
}

int Simple2DGraph::FindPointFromXY(CPoint &pt)
{
  Transform trns=GetCurrentTransform();
    float dist=_pointSize*_pointSize*4;
  int choose=-1;
  for (int i=0;i<_points.Size();i++)
  {
    CPoint pp=trns(_points[i]);
    CPoint diff=pp-pt;
    float d=(float)diff.x*diff.x+(float)diff.y*diff.y;
    
    if (d<dist) {choose=i;dist=d;}    
  }
  return choose;
}


void Simple2DGraph::OnLButtonDown(UINT nFlags, CPoint point)
{
  _moveAny=false;
  _dragPt=FindPointFromXY(point);
  _dragRect=CRect(point,point);
  if (_dragPt>=0 && !IsPointSelected(_dragPt))
  {
    if (~nFlags & MK_CONTROL)
    {
      SetSelectionAll(false);
    }
    SelectPoint(_dragPt,true);
    Redraw();
    _deselect=false;
    _notify->SelectionChanged();
  }
  else
    _deselect=true;
  SetCapture();
}

void Simple2DGraph::OnLButtonUp(UINT nFlags, CPoint point)
{
  if (GetCapture()==this)
  {

    if (!_moveAny)
    {
      if (_dragPt>=0 && _deselect)
      {
        if (~nFlags & MK_CONTROL)
          SetSelectionAll(false);
        else
          SelectPoint(_dragPt,false);
        _notify->SelectionChanged();
      }
    }
    else
    {
      if (_dragPt!=-1)
      {
        /*      Transform trn=GetCurrentTransform().Inverted();
        PointXY start=trn(CPoint(_dragRect.left,_dragRect.top));
        PointXY end=trn(CPoint(_dragRect.right,_dragRect.bottom));
        PointXY diff(end.x-start.x,end.y-start.y);

        for (int i=0;i<_points.Size();i++) if (_points[i].selected)
        {
        _points[i].x+=diff.x;
        _points[i].y+=diff.y;
        }*/

        CheckPointRanges();
        _notify->GraphChanged();
      }
      else
      {
        if (~nFlags & MK_CONTROL)
        {
          SetSelectionAll(false);
        }
        SelectRectangle(_dragRect,true);
        _notify->SelectionChanged();
      }
    }
    ReleaseCapture();
    Redraw();
  }
}

void Simple2DGraph::CheckPointRanges()
{
  if (_constStep && _points.Size()>1)
  {
    int *order=(int *)alloca(_points.Size()*sizeof(int));
    for (int i=0;i<_points.Size();i++) order[i]=i;
    QSort(order,_points.Size(),Functors::SortPointsComp(_points));
    for (int i=0;i<_points.Size();i++)
      _points[order[i]].x=((float)i/(float)(_points.Size()-1))*(_xMax-_xMin)+_xMin;
  }
  for (int i=0;i<_points.Size();i++)
  {
    PointXY &pt=_points[i];
    if (pt.x>_xMax) pt.x=_xMax;
    if (pt.x<_xMin) pt.x=_xMin;
    if (pt.y>_yMax) pt.y=_yMax;
    if (pt.y<_yMin) pt.y=_yMin;
  }
}
void Simple2DGraph::OnMouseMove(UINT nFlags, CPoint point)
{
  if (GetCapture())
  {
    if (nFlags & MK_LBUTTON)
    {    
      _dragRect.bottom=point.y;
      _dragRect.right=point.x;
      if (_dragRect.right!=_dragRect.left || _dragRect.top!=_dragRect.bottom)
      {
        _moveAny=true;
      }
      if (_dragPt!=-1)
      {
        Transform trn=GetCurrentTransform().Inverted();
        PointXY start=trn(CPoint(_dragRect.left,_dragRect.top));
        PointXY end=trn(CPoint(_dragRect.right,_dragRect.bottom));
        PointXY diff(end.x-start.x,end.y-start.y);

        float rmin=_xMax,rmax=_xMin;

        for (int i=0;i<_points.Size();i++) if (_points[i].selected)
        {
          if (rmax<_points[i].x) rmax=_points[i].x;
          if (rmin>_points[i].x) rmin=_points[i].x;
        }
        float dn=_points[_dragPt].x-rmin;
        float dx=rmax-_points[_dragPt].x;   

        for (int i=0;i<_points.Size();i++) if (_points[i].selected)
        {
          if (nFlags & MK_SHIFT)
          {
            float factor;
            if (_points[i].x<_points[_dragPt].x)          
              factor=dn<0.000001f?1:((_points[i].x-rmin)/dn);
            else
              factor=dx<0.000001f?1:((rmax-_points[i].x)/dx);
            _points[i].x+=diff.x*factor;
            _points[i].y+=diff.y*factor;
          }
          else
          {        
            _points[i].x+=diff.x;
            _points[i].y+=diff.y;
          }
        }
        
        _dragRect.left=_dragRect.right;
        _dragRect.top=_dragRect.bottom;      
        Redraw(true);
      }
      else
      {
        Invalidate(FALSE);
      }
    }
    else
    {
      if (!_noContext)
      {
        if (abs(_dragRect.left-point.x)>8 || abs(_dragRect.top-point.y)>8)
        {
          _noContext=true;
          SetCursor(LoadCursor(0,IDC_SIZEALL));
        }
        else
        {
          return;
        }
      }
      Transform curTrn=GetCurrentTransform().Inverted();
      PointXY fpt=curTrn(_dragRect.TopLeft());
      PointXY fpt2=curTrn(point);
      _zoomTransform.a31+=(fpt2.x-fpt.x)*_zoomTransform.a11;
      _zoomTransform.a32+=(fpt2.y-fpt.y)*_zoomTransform.a22;
      Redraw(true);
      _dragRect=CRect(point,point);
    }
  }
}


void Simple2DGraph::SetSelectionAll(bool selectAll)
{
  for (int i=0;i<_points.Size();i++) _points[i].selected=selectAll;
}

void Simple2DGraph::SelectPoint(int point, bool sel)
{
  _points[point].selected=sel;
}

void Simple2DGraph::SelectRectangle(const CRect &rc, bool sel)
{
  CRect crc=rc;
  crc.NormalizeRect();
  Transform trns=GetCurrentTransform();
  for (int i=0;i<_points.Size();i++)
  {
    CPoint pt=trns(_points[i]);
    if (crc.PtInRect(pt)) _points[i].selected=sel;   
  }
}

Simple2DGraph::Transform Simple2DGraph::Transform::Inverted() const
{
  Matrix3P mx(a11,a21,a31,a12,a22,a32,0,0,1);
  Matrix3P inv=mx.InverseGeneral();  
  Transform res;
  res.a11=inv(0,0);
  res.a12=inv(1,0);
  res.a21=inv(0,1);
  res.a22=inv(1,1);
  res.a31=inv(0,2);
  res.a32=inv(1,2);
  return res;
}
void Simple2DGraph::OnContextMenu(CWnd* pWnd, CPoint point)
{
  CMenu mnu;
  mnu.LoadMenu(IDR_GRAPHMENU);
  CMenu *popup=mnu.GetSubMenu(0);  
  popup->TrackPopupMenu(0,point.x,point.y,pWnd);
}

void Simple2DGraph::OnLButtonDblClk(UINT nFlags, CPoint point)
{
  int pt=FindPointFromXY(point);
  if (pt==-1)
  {  
    Transform trns=GetCurrentTransform().Inverted();
    _points.Add(trns(point));
  }
  else
  {
    _points.Delete(pt);
  }
  CheckPointRanges();
  _notify->GraphChanged();
  Redraw();
}

void Simple2DGraph::DeleteSelected()
{
  bool cc=false;
  for (int i=0;i<_points.Size();i++) 
  {
    if (_points[i].selected) 
    {
      _points.Delete(i--);
      cc=true;
    }
  }
  if (cc) _notify->GraphChanged();
}

BOOL Simple2DGraph::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
  CPoint pt(GetMessagePos());
  ScreenToClient(&pt);
  int k=FindPointFromXY(pt);
  HCURSOR cursor;
  if (k==-1) cursor=LoadCursor(0,IDC_CROSS);
  else cursor=LoadCursor(0,IDC_HAND); 
  SetCursor(cursor);
  return TRUE;
}


void Simple2DGraph::OnMButtonDown(UINT nFlags, CPoint point)
{
  _zoomTransform=Transform();
  Redraw();

  CWnd::OnMButtonDown(nFlags, point);
}

BOOL Simple2DGraph::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
  pt=CPoint(GetMessagePos());
  ScreenToClient(&pt);
  Transform inv=GetCurrentTransform().Inverted();
  PointXY fpt=inv(pt);
  float lasta11=_zoomTransform.a11;
  float lasta22=_zoomTransform.a22;
  _zoomTransform.a11*=1.0f+0.001f*zDelta;
  _zoomTransform.a22*=1.0f+0.001f*zDelta;
  Transform inv2=GetCurrentTransform().Inverted();
  PointXY fpt2=inv2(pt);
  _zoomTransform.a31=((_zoomTransform.a31/_zoomTransform.a11)+(fpt2.x-fpt.x))*_zoomTransform.a11;
  _zoomTransform.a32=((_zoomTransform.a32/_zoomTransform.a22)+(fpt2.y-fpt.y))*_zoomTransform.a22;
  Redraw(true);


  return CWnd::OnMouseWheel(nFlags, zDelta, pt);
}


void Simple2DGraph::OnRButtonDown(UINT nFlags, CPoint point)
{
  _dragRect=CRect(point,point);
  SetCapture();
  _noContext=false;
}
void Simple2DGraph::OnRButtonUp(UINT nFlags, CPoint point)
{
  if (GetCapture())
  {
    ReleaseCapture();
    if (!_noContext)
    {
      __super::OnRButtonUp(nFlags,point);
    }
  }
}

void Simple2DGraph::OnSwapAxis()
{
  Transform cur=_zoomTransform;
  float aspect=(_yMax-_yMin)/(_xMax-_xMin);
  DWORD t1=GetTickCount();
  DWORD t2=t1;
  while (t1+250>t2)
  {
    float pos=(t2-t1)/250.0f;        
    _zoomTransform.a11=cur.a11*(1.0f-pos);
    _zoomTransform.a12=cur.a11*pos*aspect;
    _zoomTransform.a22=cur.a22*(1.0f-pos);
    _zoomTransform.a21=cur.a22*pos/aspect;
    _zoomTransform.a32=cur.a31*aspect+(cur.a32-cur.a31*aspect)*(1-pos);
    _zoomTransform.a31=cur.a32/aspect+(cur.a31-cur.a32/aspect)*(1-pos);
    Redraw(true);
    t2=GetTickCount();
  }  
  _swappedAxis=!_swappedAxis;
  _zoomTransform=cur;
  Redraw(true);
}


RString Simple2DGraph::ReadGraphValues(const Simple2DGraph &_graph)
{
  AutoArray<float,MemAllocLocal<float,256> > interleaved;
  interleaved.Resize(_graph.GetPointCount()*2);
  _graph.SavePoints(interleaved,true);
  ostrstream str;
  str<<"[3";
  for (int i=0;i<interleaved.Size();i+=2)
  {
    str<<","<<interleaved[i]<<","<<interleaved[i+1]<<",0";
  }
  str<<"]"<<'\0';
  RString res=str.str();
  str.freeze(false);
  return res;
}

RString Simple2DGraph::ReadGraphValuesPoly(const Simple2DGraph &_graph)
{
  AutoArray<float,MemAllocLocal<float,256> > interleaved;
  interleaved.Resize(_graph.GetPointCount()*2);
  _graph.SavePoints(interleaved,true);
  ostrstream str;
  str<<"[";
  for (int i=0;i<interleaved.Size();i+=2)
  {
    if (i) str<<",";
    str<<interleaved[i+1];
  }
  str<<"]"<<'\0';
  RString res=str.str();
  str.freeze(false);
  return res;
}
