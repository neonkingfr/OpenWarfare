#pragma once


// CTranspStatic

class CTranspStatic : public CStatic
{
	DECLARE_DYNAMIC(CTranspStatic)

public:
	CTranspStatic();
	virtual ~CTranspStatic();

protected:
	DECLARE_MESSAGE_MAP()
public:
  afx_msg HBRUSH CtlColor(CDC* /*pDC*/, UINT /*nCtlColor*/);
};


