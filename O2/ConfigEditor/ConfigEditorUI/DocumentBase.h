#pragma once

template<class IDocViewInterface, class DocLockImpl=DocumentBase_EmptyLockClass>
class DocumentBase;


///Template declaration for view as implementation of common DV interface
/**
Best way to create new view for a document is to use this template as base

@param IDocViewInterface common DV interface type - this must be the same for document corresponding view.
@param DocLockImpl Document lock implementation class - class that implements MT safety in document. Must be the same for document and corresponding view
*/
template<class IDocViewInterface, class DocLockImpl=DocumentBase_EmptyLockClass>
class ViewBase: public IDocViewInterface
{
protected:

    ///Pointer to document, on which this view is attached
    DocumentBase<IDocViewInterface,DocLockImpl> *_curDoc;

public:

    /// Constructor
    /**
    * Constructs view not assigned to any document 
    */
    ViewBase():_curDoc(0) {}
    ///Destructor
    /**
    * Destructor automatically detaches the document 
    */
    virtual ~ViewBase() {DetachDocument();}

    ///Returns current document
    /**
    * Best for access document commands or information interface  
    * @return current document or NULL, if view is not attached to document
    */
    DocumentBase<IDocViewInterface,DocLockImpl> *GetDocument() const {return _curDoc;}

    /// Returns true, if view is attached to document
    bool IsActiveDocument() const {return _curDoc!=0;}
    /// Detaches the view from document
    /**
    * After view is detaches, no mor messages arrives from document
    */
    void DetachDocument();
    /// Attaches the view to a specified document
    /**
    * @param doc document to attach on it
    * @note After view is attached, it can receive messages. In MT environment, use proper
    * synchronization to prevent receive message until view is not fully updated from document
    */
    bool AttachDocument(DocumentBase<IDocViewInterface,DocLockImpl> *doc);
    bool AttachDocument(IDocViewInterface *doc)
    {
        return AttachDocument(static_cast<DocumentBase<IDocViewInterface> *>(doc));
    }

    /// Called when document drops the view
    /**
    * When this message is processed, view is still attached. You should not call DetachDocument
    * in this function, because view will be detached automatically by document. Derived
    * implementation @b must call base.
    */
    virtual void OnDocumentDropped() {_curDoc=0;}  


};

/// Empty lock class - default class for DocLockImpl. Use it in ST environment
class DocumentBase_EmptyLockClass
{
public:
    ///Lock the resource
    /**
    * This function is doing nothing in this class.
    */
    bool Lock() const {return true;}
    ///Unlock the resource
    /**
    * This function is doing nothing in this class.
    */
    void Unlock() const {}
};

///Template declaration for document as implementation of common DV interface
/**
* All documents that using common DV interface should derive this class
*
* @param IDocViewInterface common DV interface type - this must be the same for document corresponding view.
* @param DocLockImpl Document lock implementation class - class that implements MT safety in document. Must be the same for document and corresponding view
*/
template<class IDocViewInterface, class DocLockImpl>
class DocumentBase: public IDocViewInterface
{
    friend class ViewBase<IDocViewInterface>;
protected:
    typedef ViewBase<IDocViewInterface> View;  

    ///Master lock
    /**
    * Master lock should be used to ensure MT safety during processing requests from views -
    * if there is possibility for MT collision
    *
    * To lock document, use DocLock class
    * @see DocLock
    */
    mutable DocLockImpl _lockDocument;

    ///List ov registred views
    /**
    * Document can access this array directly, but this is not the best programming practice 
    */
    AutoArray<View *,MemAllocLocal<View *,16> > _views;
    ///Current view enum
    /**
    * Document tracks current enumeration position for EachView object
    * This is only one position, it means there can be only one enumeration cycle at the time.
    * In other words, only one thread at time can broadcast the changes. Recursive broadcasting
    * is not allowed. All broadcasts are disabled, until currently is not finished.
    * Broadcasts from other threads are delayed. During broadcast, document tracks newle added
    * or removed views
    */
    mutable int _curViewEnum;

    friend class DocLock;

    ///Lock manipulation class
    /**
    * During lifetime of instance of this class master lock is held. 
    */
    class DocLock
    {
        const DocLockImpl &_lock;
    public:
        ///Holds the lock. Lock is dropped at destructor
        /**
        * @param doc pointer to document - there will be @b this very often.
        */
        DocLock(const DocumentBase<IDocViewInterface,DocLockImpl> *doc):_lock(doc->_lockDocument)
        {_lock.Lock();}
        ~DocLock() 
        {_lock.Unlock();}    
    };


    template<class CastType>
    friend class EachViewEx;

    ///Iterator through views
    /**
    * Base class for EachView - you can use it directly, if you want cast each view
    * to specific derived type. Specify the type as template argument
    *
    * @param CastType Type that will be returns for each item - @c static_cast is used.
    *
    * @note It is hightly recomennded to use this iterator to enumerate all view.
    * Can detect and handleall situations with adding and removing
    * views during enumeration thats normaly can lead to unexcepted behaviour.
    * For example, if enumerated view removes itself from list, or removes oter view(s) from list
    *
    * @par Example: 
    * Enumerating all views
    * @par
    * <tt>for(EachViewEx<T> x(this);x;x->ViewMemberFn(params);</tt>
    *
    * @see EachView 
    *
    */
    template<class CastType>   
    class EachViewEx
    {
        DocumentBase<IDocViewInterface,DocLockImpl> &_document;
        int *pos;    
        DocLock lk;;
    public:
        ///Construction of enumerator
        /**
        * @param doc document of which views will be enumerated 
        */
        EachViewEx(DocumentBase<IDocViewInterface,DocLockImpl> *doc):
          _document(*doc),pos(&doc->_curViewEnum),lk(doc) 
          {
              ///There is running enumeration. Mark this enumerator invalid
              if (*pos!=-1) pos=0;
          }
          ~EachViewEx()
          {
              ///Mark enumeration finished
              if (pos) pos[0]=-1;
          }
    public:
        /// advance iterator to next view
        /**
        * Use in cycle's test condition. You can advance and test result at one time
        * @retval true next view is prepared
        * @retval false no more views - leave the cycle
        */
        bool GetNext()
        {
            if (pos==0) return false;
            pos[0]++;
            return (pos[0]<_document._views.Size());
        }
        ///returns pointer to active view
        /**
        *  @return pointer to active view, or NULL, if there is no view available
        */
        CastType *GetItem()
        {
            if (pos==0) return 0;
            if (pos[0]>=0 && pos[0]<=_document._views.Size()) 
            {
                CastType *res=static_cast<CastType *>(_document._views[pos[0]]);
                return res;
            }
            return 0;       
        }
    public:
        /// operator advanced the position and returns state
        /**
        @copydoc GetNext();
        */
        operator bool() {return GetNext();}
        /// Performs reference directly to view's member function
        /**
        * @par Example:
        *  iterator->ViewMemberFn();
        */
        CastType *operator->() {return GetItem();}

        operator CastType *() {return GetItem();}

    };

    ///Iterator through views
    /**
    * This class simplifies EachViewEx. You don't need to specify a @c CastType.
    * This iterator returns views as pointers to DV interface. This is mostly enought for
    * all operations
    *
    * See EachViewEx for some notes and an example.
    *
    * @see EachViewEx
    */
    class EachView: public EachViewEx<IDocViewInterface>
    {
    public:
        EachView(DocumentBase<IDocViewInterface,DocLockImpl> *doc):EachViewEx<IDocViewInterface>(doc) {}
    };

    ///Add view into view list
    /**
    * Direct call is not recomended. This function is called from ViewBase::AttachDocument function.
    * So, to register view into document, call view's member function @b AttachDocument(this)
    * @retval true view has been added.
    * @retval false error - view cannot be added.
    */
    virtual bool AddView(View *view)
    {
        DocLock lk(this);
        if (view->IsActiveDocument()) return false;
        _views.Append(view);
        return true;
    }

    ///Remove view from view list
    /**
    * Direct call is not recomended. This function is called from ViewBase::DetachDocument function.
    * So, to unregister view from document, call view's member function @b DetachDocument(this)
    * @retval true view has been removed
    * @retval false error - view cannot be removed.
    */
    virtual bool RemoveView(View *view)
    {
        DocLock lk(this);
        for (int i=0,cnt=_views.Size();i<cnt;i++)
        {
            if (_views[i]==view) 
            {
                _views.Delete(i);
                if (_curViewEnum>=i) _curViewEnum--;
                return true;
            }      
        }
        return false;
    }

    ///Prepares document for destroying
    /**
    * Ensures, that all views are removed, before destruction process is started.
    * Best places for this function is first command in document destructor. It 
    * should be at most derived document class. Unregistred views can call pure virtual 
    * member function, or function of destructed parts of document
    *
    * It good practice to call this function in each destructor of all derived classes.
    * Calling this function twice or more times is not the error. Destructor of this
    * class is also processing this function.
    */
    void PrepareDestroy()
    {
        for(EachViewEx<ViewBase<IDocViewInterface> >x(this);x;) x->OnDocumentDropped();
        _views.Clear();
    }

public:

    DocumentBase(void):_curViewEnum(-1)  {}

    virtual ~DocumentBase(void)
    {
        PrepareDestroy();
    }  

    int GetCountRegistredViews() const
    {
        DocLock lk(this);
        int res=_views.Size();
        return res;
    }


};


//#define DocView_UpdateAllViews() for (int i=0,cnt=_views.Size();i<cnt;i++) _views[i]

template<class IDocViewInterface,class DocLockImpl>
void ViewBase<IDocViewInterface, DocLockImpl>::DetachDocument()
{
    if (_curDoc)
    {
        _curDoc->RemoveView(this);
        _curDoc=0;
    }
}

template<class IDocViewInterface,class DocLockImpl>
bool ViewBase<IDocViewInterface,DocLockImpl>::AttachDocument(DocumentBase<IDocViewInterface,DocLockImpl> *doc)
{
    if (_curDoc) return false;
    if (!doc->AddView(this)) return false;
    _curDoc=doc;
    return true;
}

///Template useful to construct UNDO/REDO history container 
/**
* UNDO/REDO history container is view, that can receive and process all messages as
any other view. There is one exception. Document must process this container separately and
call reveresed versions of actions. Document have to reverse the action, this is not done by
container (because document know exactly, how to reverse the action).

For example, if document processes "add" request, it has to call "remove" request on
this container first. Container contains two stacks, the one for @c the undo history and one to @c the redo
history. When container is requested for the undo command, it pops first item from the undo stack
and invokes stored command at the document. The same behavior is with the redo command, but from the redo
stack

@anchor actions
@par How to store commands?
Undo view must receive and process reversed commands. Each command must create an instance
of class derived from UndoItem (defined in this class). This class must implement UndoItem::Run 
command. You can use prepared templates UndoAction1,UndoAction2..UndoAction5, to easy
build this class. Here is an example:

<pre>
void AddItem(int id, const Location &where, const String &name)
{
class ActAddItem: public UndoAction3<int,Location,String> 
{
public:
ActAddItem(int id, const Location &where, const String &name):
UndoAction3<int,Location,String>(id,where,name)
void Run(DocViewInterface *ifc) {ifc->AddItem(p1,p2,p3); 
//you have to use p1..px for action derived from UndoActionX
};

PushAction(new ActAddItem(id,where,name));
}
</pre>

Good practice is to use an unique class declared directly in the function. Don't use pointers
and references as parameters of the stored action, if are not shore, that
pointers or references will be valid later. (you can use counted references)
*/
template<class IDocViewInterface, class DocLockImpl=DocumentBase_EmptyLockClass>
class DocumentUndo:  public ViewBase<IDocViewInterface>
{
    typedef ViewBase<IDocViewInterface> Super;
protected:

    ///Base class for undo item
    /**
    * You must implement run command 
    */
    class UndoItem
    {
        UndoItem *_nextItem;
    public:
        UndoItem():_nextItem(0) {}
        virtual ~UndoItem()
        {
            UndoItem *x=_nextItem;
            while (x)
            {
                UndoItem *nx=x->_nextItem;
                x->_nextItem=0;
                delete x;
                x=nx;
            }
        }

        virtual bool IsChainChange(const UndoItem *cmpWith) const
        {
            return false;
        }

        void PushTo(UndoItem **queue)
        {
            _nextItem=*queue;
            *queue=this;
        }

        static UndoItem *Pop(UndoItem **queue)
        {        
            UndoItem *ret=*queue;
            if (ret==0) return ret;
            *queue=ret->_nextItem;
            ret->_nextItem=0;
            return ret;
        }

        virtual void Run(IDocViewInterface *ifc)=0;
        virtual void Inspect(IDocViewInterface *ifc)=0;
        UndoItem *GetNext() const {return _nextItem;}
    };

    friend class UndoItemGroup;

    ///Undo item describes a group of actions
    class UndoItemGroup: public UndoItem
    {
        DocumentUndo *_control;
        int _count;
    public:
        UndoItemGroup(DocumentUndo *control, int groupSz):_control(control),_count(groupSz) {}
        void Run(IDocViewInterface *ifc)
        {
            //We are currently playing action above "redo queue"
            //okay, start group on "redo queue"
            _control->BeginGroup();
            //now, we must process undo step on "undo queue" - so swap queues
            _control->SwapQueues();
            //"undo queue" is active, normal state - we can play group of undo steps
            int cnt=_count;
            while (cnt--) _control->UndoOneStep();
            //"undo queue" is still active. We need close group on "redo queue" and restore the state
            _control->SwapQueues();
            //finish group on "redo queue". We are above "redo queue" this state is the same as begin state
            _control->EndGroup();
        }
        void Inspect(IDocViewInterface *ifc) {}
        int GetGroupSz() const {return _count;}
    };

    UndoItem *_undoQueue; ///<Undo stack
    UndoItem *_redoQueue; ///<Redo stack
    int _undoRedoProgress;///<count of recursions during undo/redo progress (some features are disabled during undo/redo progress)
    int _groupRecursion; ///<count of opened groups
    UndoItem *_groupItem; ///<beginning of the first opened group
    int _undoStackLen;
    int _dirtyPos;


    ///Stores the action on undo stack
    /**
    * @param item newly created item that need to be pushed into undo stack. Action should
    be allocated on heap.
    */
    void PushAction(UndoItem *item)
    { 
        if (_undoRedoProgress==0) 
        {
            delete _redoQueue;
            _redoQueue=0;
            if (_dirtyPos>_undoStackLen) _dirtyPos=0;
            _undoStackLen++;
            if (_undoQueue && _undoQueue->IsChainChange(item))
            {
                delete item;
                return;
            }
        }
        item->PushTo(&_undoQueue);
    }    

    ///Swaps undo and redo queue.
    /**
    * Queue is swapped during undo processing, so newly pushed actions are placed into redo stack 
    */
    void SwapQueues()
    {  
        UndoItem *tmp=_undoQueue;
        _undoQueue=_redoQueue;
        _redoQueue=tmp;
    }


    ///Template for undo action with one parameter
    template<class P1>
    class UndoAction1: public UndoItem
    {
    protected:
        P1 p1;
    public:
        UndoAction1(P1 p1):p1(p1) {}
    };

    ///Template for undo action with two parameters
    template<class P1,class P2>
    class UndoAction2: public UndoAction1<P1>
    {
    protected:
        P2 p2;
    public:
        UndoAction2(P1 p1,P2 p2):UndoAction1<P1>(p1),p2(p2) {}
    };

    ///Template for undo action with three parameters
    template<class P1,class P2, class P3>
    class UndoAction3: public UndoAction2<P1,P2>
    {
    protected:
        P3 p3;
    public:
        UndoAction3(P1 p1,P2 p2,P3 p3):UndoAction2<P1,P2>(p1,p2),p3(p3) {}
    };

    ///Template for undo action with four parameters
    template<class P1,class P2, class P3, class P4>
    class UndoAction4: public UndoAction3<P1,P2,P3>
    {
    protected:
        P4 p4;
    public:
        UndoAction4(P1 p1,P2 p2,P3 p3,P4 p4):UndoAction3<P1,P2,P3>(p1,p2,p3),p4(p4) {}
    };

    ///Template for undo action with five parameters
    template<class P1,class P2, class P3, class P4, class P5>
    class UndoAction5: public UndoAction4<P1,P2,P3,P4>
    {
    protected:
        P5 p5;
    public:
        UndoAction5(P1 p1,P2 p2,P3 p3,P4 p4,P5 p5):UndoAction4<P1,P2,P3,P4>(p1,p2,p3,p4),p5(p5) {}
    };

public:
    ///Construction of undo container
    DocumentUndo(void):_undoQueue(0),_redoQueue(0),_undoRedoProgress(0),_groupRecursion(0),_groupItem(0),_undoStackLen(0),_dirtyPos(0) {}
    ///Destruction of undo container
    ~DocumentUndo(void)
    {
        delete _undoQueue;
        delete _redoQueue;
    }


    ///Undoes one action
    /**
    *  Undoes one action, plays stored action in undo stack on document, and places all reversed actions
    into redo stack

    @retval true successfully processed
    @retval false no action on stack
    */
    bool UndoOneStep()
    {
        _undoRedoProgress++;
        UndoItem *itmToPlay=_undoQueue->Pop(&_undoQueue);
        IDocViewInterface *ifc=GetDocument();
        SwapQueues();
        if (itmToPlay)
        {
            _undoStackLen--;
            itmToPlay->Run(ifc);
            delete itmToPlay;
        }
        SwapQueues();
        _undoRedoProgress--;
        return itmToPlay!=0;
    }

    ///Undoes one action
    /**
    *  Plays stored action in redo stack on document, and places all reversed actions
    into undo stack

    @retval true successfully processed
    @retval false no action on stack
    */
    bool RedoOneStep()
    {
        _undoRedoProgress++;    
        _undoStackLen+=2;
        SwapQueues();
        bool res=UndoOneStep();
        SwapQueues();
        _undoRedoProgress--;
        return res;
    }

    ///Removes the both undo and redo queue
    void Clear()
    {
        delete _undoQueue;
        delete _redoQueue;
        _undoQueue=0;
        _redoQueue=0;
    }


    ///Returns state of the undo queue
    /**
    * @retval true there is at least one item in undo queue 
    * @retval false there is no item in undo queue
    */
    bool CanUndo() const {return _undoQueue!=0;}

    ///Returns state of the redo queue
    /**
    * @retval true there is at least one item in the redo queue 
    * @retval false there is no item in the redo queue
    */
    bool CanRedo() const {return _redoQueue!=0;}

    ///Starts group
    /**
    * Group of action is played at once. 
    * Each action is stored separately, but group is processed as single step
    * You can start group multiple times. Each BeginGroup must have corresponding EndGroup
    * Group is not closed, until final EndGroup is called. Opened group will not processed as single step
    */
    virtual void BeginGroup()
    {
        OpenGroup();
    }

    bool OpenGroup() 
    {
        if (_groupRecursion==0) 
        {
            _groupItem=_undoQueue;
        }
        return _groupRecursion++==0;
    }
    ///Ends group
    /**
    * When group is closed, a special item is placed to top of the undo stack
    * This item allows process group items as single item
    * Group item itself is placed into redo stack, so redo is also processed as single step.   
    * If the closing group is empty, or contains a single item, function will not place the special
    * item, because it is not needed for this case.
    * 
    */
    virtual void EndGroup()
    {
        CloseGroup();
    }

    bool CloseGroup()
    {
        ASSERT(_groupRecursion!=0);
        if (--_groupRecursion==0)
        {
            int cnt=0;
            UndoItem *p=_undoQueue;
            while (p!=_groupItem && p!=0) {cnt++;p=p->GetNext();}
            if (cnt>1 && p==_groupItem)
            {
                UndoItem *grp=new UndoItemGroup(this,cnt);
                PushAction(grp);
            }
        }
        return _groupRecursion==0;
    }

    void SetNewDirtyPos()
    {
        _dirtyPos=_undoStackLen;
    }

    bool IsDirty() const
    {
        return _dirtyPos!=_undoStackLen;
    }

    class IfcWithGroup: public IDocViewInterface {
    public:
        virtual void BeginGroup()=0;
        virtual void EndGroup()=0;
    };

    static void PlayUndoRedo(UndoItem *from, IfcWithGroup *ifc) {

        int grpcnt = 0;
        while (from != 0) {
            UndoItemGroup *isgrp = dynamic_cast<UndoItemGroup *>(from);
            if (isgrp) {
                ifc->BeginGroup();
                grpcnt = isgrp->GetGroupSz();
            } else {
                from->Inspect(ifc);
                if (grpcnt>0)
                    if (--grpcnt == 0) ifc->EndGroup();
            }
            from = from->GetNext();
        }
    }

    void PlayUndoOn(IfcWithGroup *ifc) {
        PlayUndoRedo(_undoQueue,ifc);
    }

    void PlayRedoOn(IfcWithGroup *ifc) {
        PlayUndoRedo(_redoQueue,ifc);
    }
};
