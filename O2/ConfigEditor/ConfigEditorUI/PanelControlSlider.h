#pragma once

#include "PanelControlWithLabel.h"




namespace LindaStudio
{


  class IPanelControlSliderNotify
  {
  public:
    virtual void DataChanged(float value)=0;
  };

  class PanelControlSlider : public CWnd, public PanelControlWithLabel
  {
    float _min;
    float _max;
    float _step;
    float _tick;
    int _fprec;
    IPanelControlSliderNotify *_notify;
    CSliderCtrl _slider;
    CEdit _val;

    static const int PHeight=20;
    static const int PWidth=40;
    bool _change;
    bool _scrchange;
    CBrush _sldBgr;


  public:
    PanelControlSlider(IPanelControlSliderNotify *_notify,
      float min,
      float max,
      float step,
      float tick,
      int fprec,
      DocumentBase<IPanelControlMsgs> *document, 
      const char *labelText);
      

    virtual bool GetPreferredSize(int &xs, int &ys);
    virtual bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys);
    virtual bool SetEnable(IPanelControlMsgs *what, bool newState);
    virtual bool SetVisible(IPanelControlMsgs *what, bool newState);
    virtual bool GoNextControl(IPanelControlMsgs *from, bool previous=false);  
    virtual HWND GetWindow() const;
    virtual bool SetFont(HFONT font);

    afx_msg void OnControlSetFocus();
    afx_msg void OnControlKillFocus();
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnEditChange();
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);


    void SetValue(float val);
    void OnDocumentDropped()
    {
      delete this;
   }

    virtual BOOL PreTranslateMessage(MSG* pMsg);
    virtual IPanelControlMsgs *WhoseWindow(HWND window);


    void SetValueToControl(float val);
    bool GetValueFromControl(float &val);


  protected:
    DECLARE_MESSAGE_MAP()

    void ReadValue();
  };


}