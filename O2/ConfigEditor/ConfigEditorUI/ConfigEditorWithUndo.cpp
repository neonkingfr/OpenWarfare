#include "StdAfx.h"
#include ".\configeditorwithundo.h"





IConfigEntry::ServiceResult ConfigEditorWithUndo::SetBase(const IConfigEntry *entry, const RStringI &baseName)
{
  IConfigEntry::ServiceResult res;
  RStringI oldBase=entry->GetBaseName(&res);
  if (res!=IConfigEntry::srOk) return res;
  res=ConfigEditor::SetBase(entry,baseName);
  if (res==IConfigEntry::srOk) _undo.SetBase(entry,oldBase);
  return res;
}

IConfigEntry::ServiceResult ConfigEditorWithUndo::SetName(const IConfigEntry *entry, const RStringI &newName)
{
  IConfigEntry::ServiceResult res;
  RStringI oldName=entry->GetName(&res);
  if (res!=IConfigEntry::srOk) return res;
  res=ConfigEditor::SetName(entry,newName);
  if (res==IConfigEntry::srOk) _undo.SetName(entry,oldName);
  return res;
}

IConfigEntry::ServiceResult ConfigEditorWithUndo::SetValue(const IConfigEntry *entry, const RString &value)
{
  IConfigEntry::ServiceResult res;
  RStringI oldvalue=entry->GetValue(&res);
  if (res!=IConfigEntry::srOk) return res;
  res=ConfigEditor::SetName(entry,value);
  if (res==IConfigEntry::srOk) _undo.SetName(entry,oldvalue);
  return res;
}

IConfigActions::CombinedResult ConfigEditorWithUndo::AddValue(const IConfigEntry *subj, const IConfigEntry *before, bool newLine)
{
  CombinedResult res=ConfigEditor::AddValue(subj,before,newLine);
  if (res==IConfigEntry::srOk) _undo.Delete(res);
  return res;
}

IConfigActions::CombinedResult ConfigEditorWithUndo::AddArray(const IConfigEntry *subj, const IConfigEntry *before, bool newLine)
{
  CombinedResult res=ConfigEditor::AddArray(subj,before,newLine);
  if (res==IConfigEntry::srOk) _undo.Delete(res);
  return res;
}

IConfigActions::CombinedResult ConfigEditorWithUndo::AddValue(const IConfigEntry *subj, RStringI name, const IConfigEntry *before)
{
  CombinedResult res=ConfigEditor::AddValue(subj,name,before);
  if (res==IConfigEntry::srOk) _undo.Delete(res);
  return res;
}

IConfigActions::CombinedResult ConfigEditorWithUndo::AddArray(const IConfigEntry *subj, RStringI name, const IConfigEntry *before, bool newLine)
{
  CombinedResult res=ConfigEditor::AddArray(subj,name,before,newLine);
  if (res==IConfigEntry::srOk) _undo.Delete(res);
  return res;
}

IConfigActions::CombinedResult ConfigEditorWithUndo::AddClass(const IConfigEntry *subj,RStringI name, const IConfigEntry *before)
{
  CombinedResult res=ConfigEditor::AddClass(subj,name,before);
  if (res==IConfigEntry::srOk) _undo.Delete(res);
  return res;
}

IConfigActions::CombinedResult ConfigEditorWithUndo::AddClassLink(const IConfigEntry *subj,RStringI name, const IConfigEntry *before)
{
  CombinedResult res=ConfigEditor::AddClass(subj,name,before);
  if (res==IConfigEntry::srOk) _undo.Delete(res);
  return res;

}

IConfigEntry::ServiceResult ConfigEditorWithUndo::Delete(const IConfigEntry *what)
{
  int pos=what->GetFilePosition();
  const Array<LexSymbDesc> &curStream=_struct.GetSymbols();
  int curSize=curStream.Size();
  AutoArray<LexSymbDesc> saveArr(Array<LexSymbDesc>(const_cast<LexSymbDesc *>(curStream.Data())+pos,curStream.Size()-pos));

  IConfigEntry::ServiceResult res=ConfigEditor::Delete(what);
  if (res==IConfigEntry::srOk)
  {  
    int finalSize=curStream.Size();
    saveArr.Delete(curSize-finalSize,saveArr.Size()-(curSize-finalSize));
    
    _undo.EditSymbolStream(pos,0,saveArr);
  }
  return res;
}

IConfigEntry::ServiceResult ConfigEditorWithUndo::EditSymbolStream(int atPos, int toDelete, const Array<LexSymbDesc> &symbols)
{
  const Array<LexSymbDesc> &curStream=_struct.GetSymbols();
  AutoArray<LexSymbDesc> saveArr(Array<LexSymbDesc>(const_cast<LexSymbDesc *>(curStream.Data())+atPos,toDelete));
  IConfigEntry::ServiceResult res=ConfigEditor::EditSymbolStream(atPos,toDelete,symbols);
  if (res==IConfigEntry::srOk)
    _undo.EditSymbolStream(atPos,symbols.Size(),saveArr);
  return res;
}

IConfigActions::CombinedResult ConfigEditorWithUndo::Move(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before)
{
  const Array<LexSymbDesc> &curStream=_struct.GetSymbols();
  AutoArray<LexSymbDesc> saveArr=curStream;
  IConfigActions::CombinedResult newEntry=ConfigEditor::Move(what,where,before);
  if (newEntry!=IConfigEntry::srOk) _undo.EditSymbolStream(0,curStream.Size(),saveArr);  
  return newEntry;
}

IConfigActions::CombinedResult ConfigEditorWithUndo::Copy(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before, bool onlyStructure)
{
  IConfigActions::CombinedResult newEntry=ConfigEditor::Copy(what,where,before,onlyStructure);
  if (newEntry==IConfigEntry::srOk)  _undo.Delete(newEntry);
  return newEntry;
}




IConfigEntry::ServiceResult ConfigEditorUndoContainer::SetBase(const IConfigEntry *entry, const RStringI &baseName)
{
  class Action: public ActionBase
  {
    RStringI name;
    RStringI newBase;
  public:
    Action(const IConfigEntry *entry, RStringI newBase):name(entry->GetFullName()),newBase(newBase) {}
    virtual void RunAction(ModelClass &model)
    {
      model.SetBase(model.Find(name),newBase);
    }
  };
  PushAction(new Action(entry,baseName));
  return IConfigEntry::srOk;
}

IConfigEntry::ServiceResult ConfigEditorUndoContainer::SetName(const IConfigEntry *entry, const RStringI &newName)
{
  class Action: public ActionBase
  {
    RStringI name;
    RStringI newName;
  public:
    Action(const IConfigEntry *entry, RStringI newName):name(entry->GetFullName()),newName(newName) {}
    virtual void RunAction(ModelClass &model)
    {
      model.SetName(model.Find(name),newName);
    }
  };
  PushAction(new Action(entry,newName));
  return IConfigEntry::srOk;
}
IConfigEntry::ServiceResult ConfigEditorUndoContainer::SetValue(const IConfigEntry *entry, const RString &value)
{
  class Action: public ActionBase
  {
    RStringI name;
    RStringI newValue;
  public:
    Action(const IConfigEntry *entry, RStringI newValue):name(entry->GetFullName()),newValue(newValue) {}
    virtual void RunAction(ModelClass &model)
    {
      model.SetValue(model.Find(name),newValue);
    }
  };
  PushAction(new Action(entry,value));
  return IConfigEntry::srOk;
}

IConfigEntry::ServiceResult ConfigEditorUndoContainer::EditSymbolStream(int atPos, int toDelete, const Array<LexSymbDesc> &symbols)
{
  PushAction(
    new StdAction3<
    IConfigEntry::ServiceResult (IConfigActions::*)(int,int,const Array<LexSymbDesc> &),
    &IConfigActions::EditSymbolStream,int,int,AutoArray<LexSymbDesc> >
     (atPos,toDelete,symbols) );
  return IConfigEntry::srOk;
}

IConfigEntry::ServiceResult ConfigEditorUndoContainer::Delete(const IConfigEntry *what)
{
  class Action: public ActionBase
  {
    RStringI name;    
  public:
    Action(const IConfigEntry *entry):name(entry->GetFullName()) {}
    virtual void RunAction(ModelClass &model)
    {
      model.Delete(model.Find(name));
    }
  };
  PushAction(new Action(what));
  return IConfigEntry::srOk;
}

IConfigActions::CombinedResult ConfigEditorUndoContainer::Move(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before)
{
  class Action: public ActionBase
  {
    RStringI what;
    RStringI where;
    RStringI before;
  public:
    Action(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before):
        what(what->GetFullName()),where(where->GetFullName()),before(before?before->GetFullName():0)
    {
    }
    virtual void RunAction(ModelClass &model)
    {
      model.Move(model.Find(what),model.Find(where),before.GetLength()?model.Find(before):0);
    }
  };
  PushAction(new Action(what,where,before));
  return IConfigEntry::srOk;
}
