#pragma once
#include "panelcontrolwithlabel.h"
#include "ColorButton.h"

namespace LindaStudio
{
  class IPanelControlColor
  {
  public:
    virtual void OnColorChanged(COLORREF ref);
  };
  
  class PanelControlColor :    public PanelControlWithLabel, public IColorButtonWithNotify
  {
    IPanelControlColor *_notify;
    CColorButtonWithNotify _button;
  public:
    PanelControlColor(IPanelControlColor *notify,DocumentBase<IPanelControlMsgs> *document, const char *labelText);
    ~PanelControlColor(void);
  
    virtual bool GetPreferredSize(int &xs, int &ys);
    virtual bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys);
    virtual bool SetEnable(IPanelControlMsgs *what, bool newState);
    virtual bool SetVisible(IPanelControlMsgs *what, bool newState);
    virtual bool GoNextControl(IPanelControlMsgs *from, bool previous=false);  
    virtual HWND GetWindow() const;
    virtual bool SetFont(HFONT font);
    void OnDocumentDropped();

    virtual IPanelControlMsgs *WhoseWindow(HWND window)
    {
      if (window==_button) return this;
      else return PanelControlWithLabel::WhoseWindow(window);
    }

    virtual void OnColorButtonClick();


    void SetColor(COLORREF color) {_button.SetColor(color,true);}
    COLORREF GetColor() const {return _button.GetColor();}
  };

}
