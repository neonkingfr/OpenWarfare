///Simulates LindaEdit environment

#include "resource.h"

namespace CScriptParser
{


  template<class Array>
  void ParseArrayOfFloats (const char *str,Array &vals )
  {
    if (*str=='[')
    {
      do 
      {               
        str++;
        char *out;
        float v=(float)strtod(str,&out);
        if (out==0 || out==str) return;
        vals.Add(v);
        str=out;
        while (*str && isspace(*str)) str++;
      }
      while (*str==',');
    }
  }

}