#pragma once

//#include "FrameBase.h"
#include "IPanelControlMsgs.h"
#include "DocumentBase.h"


namespace LindaStudio
{


  class DefWinManipulator: public IPanelWindowManipulator
  {
  public:
    virtual void SetNewPosition(HWND hWnd, int x, int y, int xs, int ys)
    {
      ::MoveWindow(hWnd,x,y,xs,ys,TRUE);
    }

    virtual void SetNewPosition(HWND hWnd, const RECT &rc)
    {
      ::MoveWindow(hWnd,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,TRUE);
    }

  };

  class ScrollWinManipulator: public IPanelWindowManipulator
  {
  public:
    virtual void SetNewPosition(HWND hWnd, int x, int y, int xs, int ys)
    {
      ::MoveWindow(hWnd,x,y,xs,ys,FALSE);
    }

    virtual void SetNewPosition(HWND hWnd, const RECT &rc)
    {
      ::MoveWindow(hWnd,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,FALSE);
    }
  };

  class MassWinManipulator: public IPanelWindowManipulator
  {
    HDWP dwp;
  public:
    MassWinManipulator(int count)
    {
      dwp=BeginDeferWindowPos(count);
    }
    virtual void SetNewPosition(HWND hWnd, int x, int y, int xs, int ys)
    {
      HDWP nw=DeferWindowPos(dwp,hWnd,0,x,y,xs,ys,SWP_NOZORDER);
      if (nw!=0) dwp=nw;
    }
    virtual void SetNewPosition(HWND hWnd, const RECT &rc)
    {
      HDWP nw=DeferWindowPos(dwp,hWnd,0,rc.left,rc.top,rc.right-rc.left,rc.bottom-rc.top,SWP_NOZORDER);
      if (nw!=0) dwp=nw;
    }
    ~MassWinManipulator()
    {
      EndDeferWindowPos(dwp);
    }

  };

  class PanelContainerPimpl;
  class PanelContainer : public CWnd, public DocumentBase<IPanelControlMsgs>
  {

    int _prefXAsk,_prefXRep;
    int _prefYAsk,_prefYRep;
    int _lastSizeX,_lastSizeY;
    int _space;
    int _minheight;    
    AutoArray<int> _linePos;
    AutoArray<COLORREF> _colors;
    PanelContainerPimpl *_pimpl;
    

    friend class ContainerAsView;
    class ContainerAsView: public ViewBase<IPanelControlMsgs>
    {
      PanelContainer *outer;      
    public:
      ContainerAsView(PanelContainer *outer):outer(outer) {}        

      virtual bool GetPreferredSize(int &xs, int &ys)
      {
        return outer->GetPreferredSize(xs,ys);
      }
      virtual bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys)
      {
        return outer->SetNewLocation(mw, x,y,xs,ys);
      }
      virtual bool SetEnable(IPanelControlMsgs *what, bool newState)
      {
        return outer->SetEnable(what,newState);
      }
      virtual bool SetVisible(IPanelControlMsgs *what, bool newState)
      {
        return outer->SetVisible(what,newState);
      }
      virtual bool GoNextControl(IPanelControlMsgs *from, bool previous)
      {
        return outer->IncomeFocus(from,previous);        
      }
      virtual bool SetFont(HFONT font)
      {
         return outer->SetFont(font);
      }
      IPanelControlMsgs *GetParentContainer() {return GetDocument();}      

      IPanelControlMsgs *WhoseWindow(HWND window) {return outer->WhoseWindow(window);}      
    };

    ContainerAsView _asView;

  protected:
    IPanelControlMsgs *GetPreviousControl(IPanelControlMsgs *cur) const;
    IPanelControlMsgs *GetNextControl(IPanelControlMsgs *cur) const;

  protected:
    virtual void RepositionItems(int xpos, int ypos, CRect &rc);
    virtual void GetPreferredSizeClient(int &width, int &height, int xs, int ys);
  public:
    PanelContainer();
    virtual ~PanelContainer();

    virtual bool GetPreferredSize(int &xs, int &ys);
    virtual bool SetNewLocation(IPanelWindowManipulator &mw, int x, int y, int xs, int ys);
    virtual bool SetEnable(IPanelControlMsgs *what, bool newState);
    virtual bool SetVisible(IPanelControlMsgs *what, bool newState);
    virtual bool GoNextControl(IPanelControlMsgs *from, bool previous=false);  
    virtual HWND GetWindow() const {return m_hWnd;}
    virtual bool SetFont(HFONT font);
    virtual void RecalcLayout();

    

    virtual void GetPreferredSizeClient(int &xs, int &ys);
    virtual void RecalcLayoutClient(bool force);

    bool IncomeFocus(IPanelControlMsgs *from, bool previous);

    BOOL Create(DWORD style,CWnd *pParent, UINT nID);

    void ReLayout();

    IPanelControlMsgs &GetThisView() {return _asView;}

    void AttachViewToParent(IPanelControlMsgs *parent) {_asView.AttachDocument(parent);}


    void Clear()
    {
      PrepareDestroy();
    }

    afx_msg void OnDestroy()
    {
      PrepareDestroy();
    }

    afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void OnPaint();
    afx_msg void OnSetFocus(CWnd* pOldWnd) {IncomeFocus(0,false);} 
    virtual BOOL PreTranslateMessage(MSG* pMsg);

    virtual void EnsureVisible(IPanelControlMsgs *what);

    IPanelControlMsgs *GetControl(int pos)
    {
      if (pos>=0 && pos<_views.Size()) return _views[pos];
      else return 0;
    }
    IPanelControlMsgs *WhoseWindow(HWND window);

    void ForceRecalcNext();

    virtual void *GetInterfacePtr(unsigned int ifcuid);


    void SetSpace(int space) {_space=space;}    
    void SetMinHeight(int minHeight) {_minheight=minHeight;}
  protected:
    DECLARE_MESSAGE_MAP()
  };

  typedef ViewBase<IPanelControlMsgs> *PPanelViewType;

}

TypeIsSimpleZeroed(ViewBase<LindaStudio::IPanelControlMsgs> *)