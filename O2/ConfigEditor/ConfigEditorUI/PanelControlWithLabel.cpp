#include "StdAfx.h"
#include ".\panelcontrolwithlabel.h"
#include "PanelContainer.h"


namespace LindaStudio
{


  float PanelControlWithLabel::labelPos=0.5f;



  PanelControlWithLabel::PanelControlWithLabel(DocumentBase<IPanelControlMsgs> *doc, const char *labelText)
  {
    AttachDocument(doc);
    CWnd *par=CWnd::FromHandle(doc->GetWindow());
    _label.CreateEx(0,"STATIC",labelText,WS_CHILD|WS_VISIBLE|SS_LEFT|SS_CENTERIMAGE|SS_WORDELLIPSIS, CRect(0,0,0,0),par,-1);
    RecalculateLabelSize();
  }

  void PanelControlWithLabel::RecalculateLabelSize()
  {
    CClientDC dc(&_label);
    CString labelText;
    _label.GetWindowText(labelText);
    CRect rc(0,0,0,0);
    CFont *prev=dc.SelectObject(_label.GetFont());
    dc.DrawText(labelText,&rc,DT_CALCRECT|DT_SINGLELINE);
    _lbminsize=rc.Size();
    dc.SelectObject(prev);
  }

  bool PanelControlWithLabel::GetPreferredSize(int &xs, int &ys)
  {
    IPanelControlWithLabelContainerInfo *ifc=GetDocument()->GetInterface<IPanelControlWithLabelContainerInfo>();
    float lbpos;
    if (ifc) lbpos=ifc->GetLabelSize();
    else lbpos=labelPos;
    xs=toInt(xs*lbpos);
    if (ys<_lbminsize.cy) ys=_lbminsize.cy;
    return true;
  }

  bool PanelControlWithLabel::SetNewLocation(IPanelWindowManipulator &mw,int x, int y, int xs, int ys)
  {
    mw.SetNewPosition(_label,x+5,y,xs-5,ys);
    _label.Invalidate(TRUE);
    return true;
  }

  bool PanelControlWithLabel::SetEnable(IPanelControlMsgs *what, bool newState)
  {
    if (what==this) 
    {
      _label.EnableWindow(newState);
      return true;
    }
    else
    {
      return false;
    } 
  }

  bool PanelControlWithLabel::SetVisible(IPanelControlMsgs *what, bool newState)
  {
    if (what==this) 
    {
      _label.ShowWindow(newState?SW_SHOW:SW_HIDE);
      return true;
    }
    else
    {
      return false;
    } 
  }

  bool PanelControlWithLabel::GoNextControl(IPanelControlMsgs *from, bool previous)
  {
    return false;
  }

  HWND PanelControlWithLabel::GetWindow() const {return _label;}

  bool PanelControlWithLabel::SetFont(HFONT font)
  {
    _label.SetFont(CFont::FromHandle(font));
    return true;
  }

  CSize PanelControlWithLabel::GetTextExtent(CWnd *wnd, const char *text, bool prefix, int mlinewith)
  {
    CClientDC dc(wnd);
    CRect rc(0,0,mlinewith,1);
    CFont *fnt=dc.SelectObject(wnd->GetFont());
    dc.DrawText(text,strlen(text),&rc,DT_CALCRECT|(prefix?0:DT_NOPREFIX)|(mlinewith<0?DT_SINGLELINE:0));
    dc.SelectObject(fnt);
    return rc.Size();
  }

}