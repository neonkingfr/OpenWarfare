#pragma once
#include "configitembase.h"

class ConfigArray : public ConfigItemBase
{
protected:
  int _values;


  int PerformAddChecks(IConfigEntry *before, ServiceResult *res);
public:
  ConfigArray(ConfigStructure &owner);
  ~ConfigArray(void);

  virtual bool LoadHeader(int index, int start, int &end, RStringI &subname);
  virtual bool LoadSubItems(int start, int &end);

  IConfigEntry *Get(int idx);
  const IConfigEntry *Get(int idx) const;

  bool InsertValue(int index, bool newline=false);
  bool InsertArray(int index, bool newline=true);
  void DeleteItem(int index);

  IConfigEntry *AddSymbols(const Array<LexSymbDesc> &symbs, IConfigEntry *before, bool newLine,ServiceResult *res =0 );

  IConfigEntry *AddArray(IConfigEntry *before =0 , bool newline =true ,ServiceResult *res =0 );
  IConfigEntry *AddValue(IConfigEntry *before =0 , bool newline =false ,ServiceResult *res =0 );
  ServiceResult Delete(IConfigEntry *what);

  virtual IConfigEntry *CloneTo(IConfigEntry *newParent, IConfigEntry *before) const;
};
