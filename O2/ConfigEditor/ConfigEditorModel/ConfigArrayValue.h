#pragma once
#include "configitembase.h"

class ConfigArrayValue :  public ConfigItemBase
{
public:
  ConfigArrayValue(ConfigStructure &owner);
  ~ConfigArrayValue(void);

  virtual bool LoadHeader(int index, int start, int &end, RStringI &subname);

  ServiceResult SetValue(RString val);
  RStringS GetValue(ServiceResult *res=0) const;

  virtual IConfigEntry *CloneTo(IConfigEntry *newParent, IConfigEntry *before) const;
};
