#include "StdAfx.h"
#include ".\Configitembase.h"

ConfigItemBase::ConfigItemBase(ConfigStructure &owner):_owner(owner),_itemBegin(-1),_itemEnd(-1)
{
}

ConfigItemBase::~ConfigItemBase(void)
{
}


int ConfigItemBase::LoadStructure(int index, int start,const IConfigEntry *parent)
{
  _loadIndex=index;
  _itemBegin=start;
  RStringI subname;
  bool res=LoadHeader(index, start,_itemEnd,subname);
  if (parent) 
    _name=parent->GetFullName()+"/"+subname;
  else
    _name=subname;
  if (res)
  {
    GetOwner().RegisterNewItem(this);
    LoadSubItems(_itemEnd,_itemEnd);
  }
  return res?_itemEnd:-1;
}

void ConfigItemBase::ShiftSymbolIndexes(int start, int shift)
{
  if (_itemBegin>=start) _itemBegin+=shift;
  if (_itemEnd>=start) _itemEnd+=shift;
}

IConfigEntry *ConfigItemBase::GetParent() const
{
  int cnt=_name.ReverseFind('/');
  if (cnt<1) return 0;
  RStringI parentName=_name.Mid(0,cnt);
  return _owner.FindEntry(parentName);
}

namespace Functors
{
  class EnumSubItems
  {
  protected:
    int seppos;
    const IConfigSubItemEnumerator &enumerator;

  public:
    EnumSubItems(int seppos, const IConfigSubItemEnumerator &enumerator):seppos(seppos),enumerator(enumerator) {}
    bool operator()(const IConfigEntry *entry) const
    {
      if (seppos>=0)
      {
        int pos=entry->GetFullName().ReverseFind('/');
        if (seppos==pos) return enumerator(entry);
      }
      else
      {
        return enumerator(entry);
      }
      return false;
    }
  };

  class EnumSubItemsNoDuplicate: public EnumSubItems
  {
    mutable FindArray<RStringI> _loadedItems;
  public:
    EnumSubItemsNoDuplicate(int seppos, const IConfigSubItemEnumerator &enumertor):
        EnumSubItems(seppos,enumertor) {}

    bool operator()(const IConfigEntry *entry) const
    {
      
      IConfigEntry::ServiceResult res;
      RStringI localName=entry->GetName(&res);
      if (res==IConfigEntry::srOk)
      {
        if (_loadedItems.Find(localName)==-1)
        {
          _loadedItems.Add(localName);
          return EnumSubItems::operator ()(entry);
        }
        else
          return false;
      }
      else
        return EnumSubItems::operator ()(entry);
    }
    void SetNewSepPos(int pos)
    {
      seppos=pos;
    }
  };
}

bool ConfigItemBase::ForEachNoInherit(const IConfigSubItemEnumerator &enumerator, bool recursive) const
{
  RStringI enumMask(_name,"/");
  return _owner.ForEachItem(enumMask,Functors::EnumSubItems(recursive?-1:enumMask.GetLength()-1,enumerator));
}

bool ConfigItemBase::ForEach(const IConfigSubItemEnumerator &enumerator) const
{
  RStringI enumMask(_name,"/");
  Functors::EnumSubItemsNoDuplicate enumer(enumMask.GetLength()-1,enumerator);
  if (_owner.ForEachItem(enumMask,enumer)==true) return true;
  const IConfigEntry *baseCls=GetBase();
  while (baseCls)
  {
    RStringI enumMask(baseCls->GetFullName(),"/"); 
    enumer.SetNewSepPos(enumMask.GetLength()-1);
    if (_owner.ForEachItem(enumMask,enumer)==true) return true;
    baseCls=baseCls->GetBase();
  }
  return false;
}

RStringI ConfigItemBase::GetFullName() const
{
  return _name;
}

IConfigEntry *ConfigItemBase::FindSubEntry(const RStringI &name, bool processBaseClass, ServiceResult *res) const
{
  RStringI subEntryName=RStringI(RStringI(_name,"/"),name);
  IConfigEntry *r=_owner.FindEntry(subEntryName);
  if (r==0 && processBaseClass)
  {
    const IConfigEntry *baseItem=GetBase();
    if (baseItem) 
      return baseItem->FindSubEntry(name,processBaseClass,res);
  }
  return Res(res,srOk),r;
}

int ConfigItemBase::GetIdent() const
{
  int i=0;
  const char *z=_name;
  while (*z)
  {
    if (*z=='/') i++;
    z++;
  }
  return i;
}


void ConfigItemBase::Reload()
{
  _owner.UnregisterItem(this);
  LoadStructure(_loadIndex,_itemBegin,GetParent());
}


void ConfigItemBase::Delete(bool reloadParent)
{
  IConfigEntry *parent=0;
  if (reloadParent) parent=GetParent();
  _owner.UnregisterItem(this);
  _owner.DeleteSymbol(_itemBegin,_itemEnd-_itemBegin);
  Release();
  if (parent) parent->Reload();
}