#pragma once



class IConfigActions
{
public:
  struct CombinedResult
  {
    const IConfigEntry *entry;
    IConfigEntry::ServiceResult sres;
  
    CombinedResult(const IConfigEntry *entry,IConfigEntry::ServiceResult sres):entry(entry),sres(sres) {}
    CombinedResult(const IConfigEntry::ServiceResult sres=IConfigEntry::srOk):entry(0),sres(sres) {}
    CombinedResult(const IConfigEntry *entry):entry(entry),sres(IConfigEntry::srOk) {}

    operator const IConfigEntry *() const {return entry;}
    const IConfigEntry * operator->() const {return entry;}
    operator IConfigEntry::ServiceResult() const {return sres;}

  };

  ///Changes base class of the specified item
  /**
   * @param entry entry which base have to be change
   * @param baseName new base name. Function searches for specified base and makes connection
   * @return Result of operation, see IConfigEntry::ServiceResult
   */
  virtual IConfigEntry::ServiceResult SetBase(const IConfigEntry *entry, const RStringI &baseName) {return IConfigEntry::srNotSupported;}
  ///Changes name of the specified item
  /**
  * @param entry entry which's name have to be change
  * @param newName new name. 
  * @return Result of operation, see IConfigEntry::ServiceResult
  */
  virtual IConfigEntry::ServiceResult SetName(const IConfigEntry *entry, const RStringI &newName) {return IConfigEntry::srNotSupported;}
  ///Changes value of the specified item
  /**
  * @param entry entry which's value have to be change
  * @param newName new value as text. Text should be quoted. Function adds quotes when it is necessary
  * @return Result of operation, see IConfigEntry::ServiceResult
  */
  virtual IConfigEntry::ServiceResult SetValue(const IConfigEntry *entry, const RString &value) {return IConfigEntry::srNotSupported;}
  ///Adds new unnamed value. This function is supported for arrays only
  /**
   * @param subj Parameter has two meanings. If this command is called by a view for the document, subj have to
        contain pointer to parent object for new item. If this command is called by the document for any view,
        parameter contains pointer to newly added item. If view need parent object, use GetParent function.
   * @param before Pointer to object before new values should be placed. Use NULL to place at the end of array 
   * @param newLine specifies formating of new value. If it is true, new value is placed at the new line in file.
   * @param res This parameter must be zero, if it is used as command to document. Document will use this parameter to distribute pointer of new added item.
   * @return Result of operation, see IConfigEntry::ServiceResult
   */
  virtual CombinedResult AddValue(const IConfigEntry *subj, const IConfigEntry *before=0, bool newLine=false) {return IConfigEntry::srNotSupported;}
  ///Adds new unnamed array. This function is supported for arrays only
  /**
  * @param subj Parameter has two meanings. If this command is called by a view for the document, subj have to
        contain pointer to parent object for new item. If this command is called by the document for any view,
        parameter contains pointer to newly added item. If view need parent object, use GetParent function.
  * @param before Pointer to object before new values should be placed. Use NULL to place at the end of array 
  * @param newLine specifies formating of new array. If it is true, new array is placed at the new line in file.
  * @return Result of operation, see IConfigEntry::ServiceResult
  * @note Pointer to before is invalidated after adding. Document informs all views with its new value.
  */
  virtual CombinedResult AddArray(const IConfigEntry *subj, const IConfigEntry *before=0, bool newLine=true) {return IConfigEntry::srNotSupported;}
  ///Adds new named value. This function is supported for classes only.
  /**
  * @param subj Parameter has two meanings. If this command is called by a view for the document, subj have to
        contain pointer to parent object for new item. If this command is called by the document for any view,
        parameter contains pointer to newly added item. If view need parent object, use GetParent function.
  * @param name Name of new item
  * @param before Pointer to object before new values should be placed. Use NULL to place at the end of class 
  * @return Result of operation, see IConfigEntry::ServiceResult
  * @note Pointer to before is invalidated after adding. Document informs all views with its new value.
  */
  virtual CombinedResult AddValue(const IConfigEntry *subj, RStringI name, const IConfigEntry *before=0) {return IConfigEntry::srNotSupported;}
  ///Adds new named array. This function is supported for arrays only
  /**
  * @param subj Parameter has two meanings. If this command is called by a view for the document, subj have to
          contain pointer to parent object for new item. If this command is called by the document for any view,
          parameter contains pointer to newly added item. If view need parent object, use GetParent function.
  * @param name Name of new item
  * @param before Pointer to object before new array should be placed. Use NULL to place at the end of class 
  * @param newLine specifies formating of new array. If it is true, new array is placed at the new line in file.
  * @return Result of operation, see IConfigEntry::ServiceResult
  * @note Pointer to before is invalidated after adding. Document informs all views with its new value.
  */
  virtual CombinedResult AddArray(const IConfigEntry *subj,RStringI name, const IConfigEntry *before=0, bool newLine=true) {return IConfigEntry::srNotSupported;}
  ///Adds new class.
  /**
  * @param subj Parameter has two meanings. If this command is called by a view for the document, subj have to
          contain pointer to parent object for new item. If this command is called by the document for any view,
          parameter contains pointer to newly added item. If view need parent object, use GetParent function.
  * @param name Name of new item
  * @param before Pointer to object before new class should be placed. Use NULL to place at the end of class 
  * @return Result of operation, see IConfigEntry::ServiceResult
  * @note Pointer to before is invalidated after adding. Document informs all views with its new value.
  */
  virtual CombinedResult AddClass(const IConfigEntry *subj,RStringI name, const IConfigEntry *before=0) {return IConfigEntry::srNotSupported;}
  ///Adds new class link. Class link is a class definition without { }.
  /**
  * @param subj Parameter has two meanings. If this command is called by a view for the document, subj have to
          contain pointer to parent object for new item. If this command is called by the document for any view,
          parameter contains pointer to newly added item. If view need parent object, use GetParent function.
  * @param name Name of new item
  * @param before Pointer to object before new class should be placed. Use NULL to place at the end of class 
  * @return Result of operation, see IConfigEntry::ServiceResult
  * @note Pointer to before is invalidated after adding. Document informs all views with its new value.
  */
  virtual CombinedResult AddClassLink(const IConfigEntry *subj,RStringI name, const IConfigEntry *before=0) {return IConfigEntry::srNotSupported;}
  ///Removes an item from structure
  /**
   * @param what Item to remove 
   * @return Result of operation, see IConfigEntry::ServiceResult
   * @note Pointer to before is invalidated after adding. Document informs all views with its new value.
   */
  virtual IConfigEntry::ServiceResult Delete(const IConfigEntry *what=0) {return IConfigEntry::srNotSupported;}

  ///Makes copy of specified class
  /**
   * Copy is made including all sub items, comments and all definitions inside
   * @param what pointer to object what should be copied
   * @param where pointer to object where should be copied
   * @param before pointer to object inside of where before 'what' should be copied. If NULL, object is placed at the end of class
   * @return Result of operation, see IConfigEntry::ServiceResult
   * @note All pointers can be invalidated after copy. Document notifies all views with valid pointers.
   */
  virtual CombinedResult Copy(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before=0, bool onlyStructure=false)  {return IConfigEntry::srNotSupported;}

  ///Moves specified class
  /**
  * Move is made including all sub items, comments and all definitions inside
  * @param what pointer to object what should be moved
  * @param where pointer to object where should be moved
  * @param before pointer to object inside of where before 'what' should be moved. If NULL, object is placed at the end of class
  * @return Result of operation, see IConfigEntry::ServiceResult
  * @note All pointers can be invalidated after move. Document notifies all views with valid pointers.
  */
  virtual CombinedResult Move(const IConfigEntry *what, const IConfigEntry *where, const IConfigEntry *before=0)  {return IConfigEntry::srNotSupported;}

  virtual IConfigEntry::ServiceResult EditSymbolStream(int atPos, int toDelete, const Array<LexSymbDesc> &symbols) {return IConfigEntry::srNotSupported;}

  virtual void SetConfigName(const RStringI &name) {}
  
};

class IConfigEnumCallback
{
public:
  virtual bool operator()(const IConfigEntry *item) const=0;
};

class IConfigModel: public IConfigActions
{
public:

  typedef IConfigActions Actions;

  ///Function returns pointer to root item
  virtual const IConfigEntry *GetRoot() const=0;
  ///Finds an item by the name.
  virtual const IConfigEntry *Find(const RStringI &fullName) const=0;
  ///Gets name of the item.
  virtual const RStringI GetFullName(const IConfigEntry *entry) const=0;  
  ///Gets an item from an array referenced by the index
  virtual const IConfigEntry *Find(const IConfigEntry *ent, int index) const=0;
  ///
  virtual bool ForEach(const RStringI &mask,const IConfigEnumCallback &functor)=0;
};

