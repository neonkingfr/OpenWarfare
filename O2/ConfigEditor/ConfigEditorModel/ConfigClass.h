#pragma once
#include "configitembase.h"

class ConfigClass :
  public ConfigItemBase
{
  int _values;  
  IConfigEntry *_base;
  int _baseSymbolId;
  int _baseEndId;
  int _nameId;
  RString _searchBase;
protected:
  IConfigEntry *SearchBase(const RStringI &baseName) const;
  void LinkToFullClass();
  int PerformAddChecks(const RStringI &name, IConfigEntry *before, ServiceResult *res);
public:
  ConfigClass(ConfigStructure &owner);

  virtual bool LoadHeader(int index, int start, int &end, RStringI &subname);
  virtual bool LoadSubItems(int start, int &end);


  IConfigEntry *GetBase(ServiceResult *res=0) {return _base;}
  const IConfigEntry *GetBase(ServiceResult *res=0) const {return _base;}
  RStringI GetBaseName(ServiceResult *res=0) const;
  ServiceResult SetBase(RStringI baseName);
  void ShiftSymbolIndexes(int start, int shift);
  virtual IConfigEntry *AddClass(RStringI name, IConfigEntry *before =0 ,ServiceResult *res =0 );
  virtual IConfigEntry *AddValue(RStringI name, IConfigEntry *before =0 ,ServiceResult *res =0 );
  virtual IConfigEntry *AddArray(RStringI name, IConfigEntry *before =0 , bool newline =true,ServiceResult *res =0 );
  virtual IConfigEntry *AddClassLink(RString name, IConfigEntry *before =0 ,ServiceResult *res =0 );

  virtual RStringI GetName(ServiceResult *res=0) const ;
  virtual ServiceResult SetName(RStringI name) ;
  ServiceResult Delete(IConfigEntry *what);

  ///Moves or copies whole item (include all subitems)
  /**
   * @param what subject to move
   * @param before item before new item should be placed. Item must be inside of current class
   *  if NULL passed, new item is placet at end of class
   * @param copy if true, item is copied. This is allowed only when item is moved from another class
   * @retval srOk success
   * @retval srError couldn't handle
   * @retval srNotSupported operation is not supported by this object
   * @retval srReadOnly source or target class is read only, and cannot be modified
   * @retval srDuplicate class with the same name is already exists in target class.
   * @note after move (not copy), pointer to what is invalidated. You have to use FindSubEntry to obtain
   *   valid pointer to moved object. 
   * @note function fails, if moving item is parent of this object
   */
  ServiceResult MoveItem(IConfigEntry *what, IConfigEntry *before=0, bool copy=false);


  virtual IConfigEntry *CloneTo(IConfigEntry *newParent, IConfigEntry *before) const;
};


class ConfigRootClass: public ConfigClass
{
public:
  ConfigRootClass(ConfigStructure &owner):ConfigClass(owner)  {}
  bool LoadHeader(int index, int start, int &end, RStringI &subname);  
};
