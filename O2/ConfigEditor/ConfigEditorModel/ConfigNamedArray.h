#pragma once
#include "configarray.h"

class ConfigNamedArray :
  public ConfigArray
{
public:
  ConfigNamedArray(ConfigStructure &owner);

  virtual bool LoadHeader(int index, int start, int &end, RStringI &subname);
  virtual bool LoadSubItems(int start, int &end);

  ServiceResult SetName(const RStringI &str);
  RStringI GetName(ServiceResult *res=0) const;
  virtual IConfigEntry *CloneTo(IConfigEntry *newParent, IConfigEntry *before) const;
};
