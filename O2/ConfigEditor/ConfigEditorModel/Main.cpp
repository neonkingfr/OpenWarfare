// ConfigEditorModel.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ConfigEditor.h"


int _tmain(int argc, _TCHAR* argv[])
{
  ConfigEditor editor;
  editor.Load("p:\\bin\\cfgUI.hpp");
  const IConfigEntry *root=editor.GetRoot();
  const IConfigEntry *sub=root->FindSubEntry("PreloadTextures/CfgInGameUI/GroupDir");
  const IConfigEntry *mytest=editor.AddClass(sub,"MyTest");  
  {  
  const IConfigEntry *myval=editor.AddValue(mytest,"MyVal");  
  editor.SetValue(myval,"Ahoj cau");
  const IConfigEntry *myarr=editor.AddArray(mytest,"MyArr",myval,true);  
  const IConfigEntry *val=editor.AddValue(myarr,0,true);  
  editor.SetValue(val,"3");
  editor.AddValue(myarr,val,false);
  editor.AddValue(myarr,0,true);
  editor.AddValue(myarr,0,false);
  val=editor.Find(myarr,1);
  editor.SetValue(val,"10");
  }
  editor.Save("t:\\test.hpp");
}

