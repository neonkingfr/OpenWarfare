#include "StdAfx.h"
#include ".\configdefaultfactory.h"

#include "ConfigArray.h"
#include "ConfigClass.h"
#include "ConfigNamedArray.h"
#include "ConfigArrayValue.h"
#include "ConfigValue.h"
#include "ConfigDefinition.h"

IConfigFactory &ConfigDefaultFactory::Instance()
{
  static ConfigDefaultFactory SConfigDefaultFactory;
  return SConfigDefaultFactory;
}

IConfigEntry *ConfigDefaultFactory::CreateValue(ConfigStructure &owner) const
{
  return new ConfigValue(owner);
}
IConfigEntry *ConfigDefaultFactory::CreateArray(ConfigStructure &owner) const
{
  return new ConfigArray(owner);
}
IConfigEntry *ConfigDefaultFactory::CreateNamedArray(ConfigStructure &owner) const
{
  return new ConfigNamedArray(owner);
}
IConfigEntry *ConfigDefaultFactory::CreateClass(ConfigStructure &owner) const
{
  return new ConfigClass(owner);
}
IConfigEntry *ConfigDefaultFactory::CreateArrayValue(ConfigStructure &owner) const
{
  return new ConfigArrayValue(owner);
}
IConfigEntry *ConfigDefaultFactory::CreateRoot(ConfigStructure &owner) const
{
  return new ConfigRootClass(owner);
}
IConfigEntry *ConfigDefaultFactory::CreateDefinition(ConfigStructure &owner) const
{
  return new ConfigDefinition(owner);
}
