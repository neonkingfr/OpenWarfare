#pragma once
#include "LexParser.h"
#include "IConfigEntry.h"
#include "IConfigFactory.h"


class ConfigStructure
{
  LexParser &_dataSrc;
  typedef IConfigEntry *Item;
  typedef BTree<Item, BTreeRefCompare> Database;
  typedef BTreeIterator<Item, BTreeRefCompare> Iter;

  Database _data;

  class SearchClass: public IConfigEntry
  {
    const RStringI &fullname;
  public:
    SearchClass(const RStringI &item):fullname(item) {}
    virtual RStringI GetFullName() const {return fullname;}
    virtual IConfigEntry *GetParent() const {return 0;}
    virtual bool ForEachItem(const IConfigSubItemEnumerator &enumerator, bool recursive) const {return false;}
    virtual void ShiftSymbolIndexes(int start, int shift) {}
    virtual void Reload() {}
    virtual int GetFilePosition() const {return 0;}
    virtual IConfigEntry *CloneTo(IConfigEntry *newParent, IConfigEntry *before) const {return 0;}
    virtual int GetFileEndPosition() const {return 0;}
    virtual bool ForEachNoInherit(const IConfigSubItemEnumerator &enumerator, bool recursive) const {return false;}
    virtual bool ForEach(const IConfigSubItemEnumerator &enumerator) const {return false;}

  };

  IConfigFactory &factory;

protected:
  void ShiftSymbolIndexes(int start, int shift);
public:
  ConfigStructure(LexParser &dataSrc, IConfigFactory &factory);
  ~ConfigStructure(void);

  const LexSymbDesc &GetSymbol(int index) const;
  const Array<LexSymbDesc> &GetSymbols() const;
  void ReplaceSymbol(int index, const LexSymbDesc &symb);
  void InsertSymbol(int index, const Array<LexSymbDesc> &symb);
  void DeleteSymbol(int index, int count);
  int CountSymbols() const;

  IConfigEntry *FindEntry(const RStringI &fullname) const;
  void Clear();

  template<class Functor>
  bool ForEachItem(const RStringI &mask, const Functor &functor) const
  {
    Iter iter(_data);
    Item *item;
    int masklen=mask.GetLength();
    SearchClass srch(mask);
    iter.BeginFrom(&srch);
    while ((item=iter.Next())!=0 && strnicmp((*item)->GetFullName().Data(),mask.Data(),masklen)==0)
    {
      if (functor(*item)==true) return true;
    }
    return false;
  }

  ///Packs symbols into plaintText symbol
  /**@copydoc LexParser::PackToPlainText */
  int PackToPlainText(int symbolId,SymbolType termSymbol, bool trim);
  ///Packs symbols into plaintText symbol
  /**@copydoc LexParser::PackToPlainText */
  int PackToPlainText(int symbolId,const Array<SymbolType> &termSymbol, bool trim);


  static RString QuoteText(const RString &text, bool preventDoubleQuote=true);
  static bool NeedQuote(const RString &);
  static RString Unquote(const RString &);
  static bool IsQuoted(const RString &);
  static bool IsAValidIdentifier(const RString &);

  const IConfigFactory &Factory() const {return factory;}

  void RegisterNewItem(IConfigEntry *entry);

  static RString IndexToName(int idx);
  static RString CreateIdent(int n);

  ///Removes all object inside of entry.
  /**
   * It unregisters and removes all entries inside specified entry. Data are not deleted.
   * Deleted structure can be restored by entry->Reload()
   * @param entry entry that contains entries to delete
   */
  void DeleteSubitems(IConfigEntry *entry);
  ///Removes entry and all entries inside
  /**
   * It unregisters a entry and removes all sub entries. When entry is unregistered,
   * object still exists, but it is not accessible by ConfigStructure class.
   * @param entry to unregister.
   */
  void UnregisterItem(IConfigEntry *entry);

  int SkipBlindSymbols(int index,bool skipdef=true);

  IConfigEntry *CreateRoot();

};
