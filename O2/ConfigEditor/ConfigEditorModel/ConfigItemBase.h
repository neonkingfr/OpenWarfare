#pragma once
#include "iconfigentry.h"
#include "ConfigStructure.h"

class ConfigItemBase : public IConfigEntry
{
  RStringI _name;
  int _itemBegin;
  int _itemEnd;
  int _loadIndex;
  ConfigStructure &_owner;

protected:
  ///Loades header into the structure
  /**
   * @param index Index of item in parent structure. It identifies position. 
   *   Items without the name can use this value to calculate unique name.
   * @param start Index of first symbol item in symbol stream
   * @param end It receives first unused symbol index after structure is loaded. This 
   *  value will be used as index of next item
   * @param subname receives item name. Loader will use this name to generate full qualified
   *  name
   * @retval true, structure has been fully or partially loaded
   * @retval false, structure cannot be loaded, object cannot recognize data format
   * @note during loading, object is not yet registered. To load data that depends on it
   *  rederive LoadSubItems
   */
  virtual bool LoadHeader(int index, int start, int &end, RStringI &subname)=0;
  
  virtual bool LoadSubItems(int start, int &end) {end=start; return true;}


public:
  ConfigItemBase(ConfigStructure &owner);
  ~ConfigItemBase(void);

  ConfigStructure &GetOwner() {return _owner;}  
  const ConfigStructure &GetOwner() const {return _owner;}  
  
  ///Loades structure into the object
  /**
   * @param index Index of the item in parent structure. In other words - position 
   * @param start Index of the first symbol item in the symbol stream
   * @param parent Pointer to parent object.
   * @return index of the next symbol in the symbol stream. Function returns -1
   *  when it cannot load the structure for given object.
   */
  int LoadStructure(int index, int start,const IConfigEntry *parent);


  void ShiftSymbolIndexes(int start, int shift);

  IConfigEntry *GetParent() const;
  bool ForEachNoInherit(const IConfigSubItemEnumerator &enumerator, bool recursive) const;
  bool ForEach(const IConfigSubItemEnumerator &enumerator) const;
  virtual RStringI GetFullName() const;
  virtual IConfigEntry *FindSubEntry(const RStringI &name, bool processBaseClass=true, ServiceResult *res=0) const;
  void Reload();  
  int GetIdent() const;
  
  int GetFirstSymbol() const
  {
    return _itemBegin;
  }  
  int GetTermSymbol() const
  {
    return _itemEnd;
  }  
  
  ///Psychicaly deletes an item
  /**
  * Function deletes object and the data. 
  * @param reloadParent when true, automatically reloads parent structure. Use
  *  false when you destroying items in batch. Then you have to reload parent manually
  */
  void Delete(bool reloadParent=true);

  virtual int GetFilePosition() const
  {
    return _itemBegin;
  }

  virtual int GetFileEndPosition() const
  {
    return _itemEnd;
  }
};
