#pragma once

enum SymbolType
{
  symLineComment,     // 
  symBlockComment,    /*  */
  symWord,           //word, token, number, anything surrounded by other symbols
  symLeftBracket,    //[
  symRightBracket,    //]
  symLeftBrace,       //{
  symRightBrace,      //}
  symLeftParenthesis, //(
  symRightParenthesis,//)
  symSemicolon,       //;
  symComma,            //,
  symColon,            //:
  symQuote,            //"
  symAssign,            //=
  symQuotedText,       //text in ""
  symPlainText,        //general text
  symNewLine,          //newline separator
  symWhiteSpaces,      //one or more white spaces
  symDoubleSharp,      //#
  symEof,              //end if the file
  symBof,               //begin of the file
  symInclude,           //include, next element contains filename
  symDefine,            //define followed by name, parameters, content is inside symDefinitionContent
  symDefineContent,      //content of definition
  symOtherPreproc       //any other preprocesor command
};

class LexFunctor
{
  mutable SymbolType _symbType;  
public:
  LexFunctor(void);
  ~LexFunctor(void);


  template<class Iter>
  bool operator()(Iter iter, int *subexpbeg, int *subexpend, int maxSubExp) const;

  SymbolType GetSymbolType() const {return _symbType;}

};
