#pragma once
#include <el/Interfaces/IMultiInterfaceBase.hpp>
#include "LexSymbDesc.h"

class IConfigEntry;
class IConfigSubItemEnumerator
{
public:
  virtual bool operator()(const IConfigEntry *entry) const=0;
};

class IConfigEntry: public IMultiInterfaceBase
{
public:

  virtual void Release() {delete this;}
  virtual ~IConfigEntry() {}

  ///returns full qualified name. Use / as path separator
  virtual RStringI GetFullName() const=0;

  int Compare(const IConfigEntry &other)
  {
    return GetFullName().Cmp(other.GetFullName());
  }  
  virtual IConfigEntry *GetParent() const=0;

  virtual bool ForEachNoInherit(const IConfigSubItemEnumerator &enumerator, bool recursive) const=0;
  virtual bool ForEach(const IConfigSubItemEnumerator &enumerator) const=0;


  virtual void ShiftSymbolIndexes(int start, int shift)=0;
  virtual void Reload()=0;  


  class CloneFunctor: public IConfigSubItemEnumerator
  {
    IConfigEntry *target;
  public:
    CloneFunctor(IConfigEntry *target):target(target) {}
    virtual bool operator()(const IConfigEntry *entry) const
    {
      return entry->CloneTo(target,0)==0;
    }
  };


  enum ServiceResult
  {
     srOk, ///<Operation processed successful
     srError,///<an unspecified error, or error in parameters prevents function to carry out the operation
     srNotSupported,///<Operation is not supported for given parameters
     srReadOnly, ///<Operation cannot be carried out. Target item is marked read only and cannot be modified
     srNotValidName, ///<Name of class or item has not been in acceptable form.
     srDuplicate ///<Cannot use specified name, there is another item with the same name.
  };

  virtual ServiceResult SetValue(RString value) {return srNotSupported;}
  virtual ServiceResult SetName(RStringI name) {return srNotSupported;}
  virtual ServiceResult SetBase(RStringI baseName) {return srNotSupported;}
  virtual IConfigEntry *AddValue(IConfigEntry *before=0, bool newline=false,ServiceResult *res=0) {if (res) *res=srNotSupported;return 0;}
  virtual IConfigEntry *AddArray(IConfigEntry *before=0, bool newline=true,ServiceResult *res=0) {if (res) *res=srNotSupported;return 0;}
  virtual IConfigEntry *AddValue(RStringI name, IConfigEntry *before=0,ServiceResult *res=0) {if (res) *res=srNotSupported;return 0;}
  virtual IConfigEntry *AddArray(RStringI name, IConfigEntry *before=0, bool newline=true,ServiceResult *res=0) {if (res) *res=srNotSupported;return 0;}
  virtual IConfigEntry *AddClass(RStringI name, IConfigEntry *before=0,ServiceResult *res=0) {if (res) *res=srNotSupported;return 0;}
  virtual IConfigEntry *AddClassLink(RStringI name, IConfigEntry *before=0,ServiceResult *res=0) {if (res) *res=srNotSupported;return 0;}
  virtual ServiceResult Delete(IConfigEntry *what) {return srNotSupported;}
  virtual IConfigEntry *FindSubEntry(const RStringI &name, bool processBaseClass=true, ServiceResult *res=0) const {if (res) *res=srNotSupported;return 0;}

  virtual RStringI GetName(ServiceResult *res=0) const  {if (res) *res=srNotSupported;return 0;}
  virtual RStringS GetValue(ServiceResult *res=0) const {if (res) *res=srNotSupported;return 0;}
  virtual IConfigEntry *GetBase(ServiceResult *res=0) {if (res) *res=srNotSupported;return 0;}
  virtual RStringI GetBaseName(ServiceResult *res=0) const {if (res) *res=srNotSupported;return 0;}
  virtual int GetFilePosition() const=0;
  virtual int GetFileEndPosition() const=0;
  
  virtual IConfigEntry *CloneTo(IConfigEntry *newParent, IConfigEntry *before) const=0;

  static void Res(ServiceResult *res, ServiceResult val)
  {
    if (res) *res=val;
  }

  const IConfigEntry *GetBase(ServiceResult *res=0) const
  {
    return const_cast<IConfigEntry *>(this)->GetBase(res);
  }

};


