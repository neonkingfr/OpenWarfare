#ifndef _GEOMETRY_OBJECT_H
#define _GEOMETRY_OBJECT_H

#define USE_SPAWN_POINTS 1  // spawn points are used only in alpha in engine so enabling this can cause CTD in beta and release!

#include <stdio.h>

// Structures intended to hold information about geometry of one game object (or terrain). It can also hold information about geometry of several game objects merged together by setNavmeshBlockers script command.
// Warning: all data copy, add and set operators and methods (as well as constructors and destructor) are implemented only on engine side (they are not needed here, plugins are supposed just to read data).

// One vertex:
struct GeometryVertex
{
  float x; 
  float y; 
  float z;
};

// One face (max 4 vertices per face)
struct GeometryFace
{
  int nIndices;     
  int indices[4];   // index to GeometryObject vertex array

  GeometryFace(){nIndices = 0;}
};

// Structure holds additional vertex arrays for GeometryObject.
struct GeometryObjectVerticesList
{
  int nVertices;
  GeometryVertex* vertices;

  GeometryObjectVerticesList(){nVertices = 0; vertices = NULL;}
};

// Geometry object declaration:
struct GeometryObject
{
  enum Type
  {
    House = 1,
    Tree = 2,
    Landscape = 4,
    Road = 8
  };
  Type type;                // type of object
  long long realObjectId;   // VBS id of object
  float transform[4][3];    // world transformation of the object
  GeometryVertex* vertices; // array of object's vertices
  int nVertices;            // number of vertices in 'vertices' array
  GeometryFace* faces;      // array of object's faces
  int nFaces;               // number of faces in 'faces' array
#if USE_SPAWN_POINTS
  GeometryObjectVerticesList spawnPoints; // points in geometry objects which define accessible areas for AI
#endif
  GeometryObject(){nVertices = 0; nFaces = 0; vertices = NULL; faces = NULL;}
};

#endif
