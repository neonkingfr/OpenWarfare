#include "BicubicFilter.h"

#include <d3dx9.h>
#include <memory.h>
#include <iostream>
#include <fstream>

/*
	Class constructor, IDirect3DTexture represent object for bicubic filtering.
*/
BicubicFilter::BicubicFilter(IDirect3DTexture9 *sourceTexture)
: _srcTexture(sourceTexture), _lookUpTexture(0), _constTable(0), _vsShader(0), _psShader(0), _vb(0), _decl(0)
{
	D3DSURFACE_DESC desc;

	sourceTexture->GetLevelDesc(0, &desc);

	_srcTexSize = D3DXVECTOR4(float(desc.Width), float(desc.Height), 0.0f, 0.0f);

	_texelSize = D3DXVECTOR4(1.0f / (float) desc.Width, 0.0f, 0.0f, 1.0f / (float) desc.Height);
}

/*
	Class destructor, release all allocated objects by class.
*/
BicubicFilter::~BicubicFilter(void)
{
	Release();
}

/*
	All resources initialization. If all resources initialized successfully	then return TRUE else FALSE.
 */
bool BicubicFilter::Initialize(IDirect3DDevice9 *device)
{
	if (!CompileShaders(device)) { return false; }

	if (!CreateVertexBuffer(device)) { return false; }

	// 128 - define number of samples and texture width
	if (!CreateLookupTexture(device, 128)) { return false; }

	// vertex declaration
	const D3DVERTEXELEMENT9 elements[3] = 
	{ 
		{ 0, 0,  D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0 },
		{ 0, 12, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0 },
		D3DDECL_END()
	};

	if ( FAILED(device->CreateVertexDeclaration(elements, &_decl)) )
		return false;

	return true;
}

/*
	Release of all allocated resources.
*/
void BicubicFilter::Release(void)
{
	_srcTexture = 0;

	if (_lookUpTexture) _lookUpTexture->Release();
	if (_constTable) _constTable->Release();
	if (_vsShader) _vsShader->Release();
	if (_psShader) _psShader->Release();
	if (_vb) _vb->Release();
	if (_decl) _decl->Release();

	_lookUpTexture = 0;
	_constTable = 0;
	_vsShader = 0;
	_psShader = 0;
	_vb = 0;
	_decl = 0;
}

/*
	Bicubic filtering excution.
*/
bool BicubicFilter::Render(IDirect3DDevice9 *device)
{
	device->Clear(0, 0, D3DCLEAR_TARGET, 0, 1.0f, 0);

	if ( SUCCEEDED(device->BeginScene()) )
	{
		device->SetRenderState(D3DRS_ZENABLE, false);

		// sampler 0 - texture to filtering
		device->SetTexture(0, _srcTexture);
		device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
		device->SetSamplerState(0, D3DSAMP_MIPFILTER, 0);
		device->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
		device->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

		// sampler 1 - lookup texture
		device->SetTexture(1, _lookUpTexture);
		device->SetSamplerState(1, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
		device->SetSamplerState(1, D3DSAMP_MINFILTER, D3DTEXF_POINT);
		device->SetSamplerState(1, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);

		_constTable->SetVector(device, "sourceSize", &_srcTexSize);
		_constTable->SetVector(device, "texelSize", &_texelSize);

		device->SetVertexDeclaration(_decl);
		device->SetStreamSource(0, _vb, 0, sizeof(Vertex));

		device->SetVertexShader(_vsShader);
		device->SetPixelShader(_psShader);

		device->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

		device->SetVertexShader(0);
		device->SetPixelShader(0);

		device->EndScene();
	}

	device->Present(0, 0, 0, 0);

	return false;
}

/*
	Lookup texture filling by coefficients.
*/
bool BicubicFilter::CreateLookupTexture(IDirect3DDevice9 *device, int width)
{
	IDirect3DTexture9 *tmpTexture;

	// temporary texture created in system pool used for offsets and weight filling
	if ( FAILED(D3DXCreateTexture(
		device, width, 1, 1, D3DUSAGE_DYNAMIC, D3DFMT_A16B16G16R16F, D3DPOOL_SYSTEMMEM, &tmpTexture
		)) ) { return false; }

	const int lookupTextureSize = width * 4;

	std::vector<float> *vec = new std::vector<float>();
	vec->reserve(lookupTextureSize);

	//fill vector by coefficients h0, h1, g0 & g1
	GenCoeefficients(vec, width);

	D3DXFLOAT16 *vec16 = new D3DXFLOAT16[lookupTextureSize];

	// convert to float16
	D3DXFloat32To16Array(&vec16[0], &(*vec)[0], lookupTextureSize);

	delete vec;

	D3DLOCKED_RECT lockRect;

	if ( FAILED(tmpTexture->LockRect(0, &lockRect, 0, D3DLOCK_DISCARD)) ) { return false; }

	// fill texture by coefficients
	memcpy(lockRect.pBits, vec16, sizeof(D3DXFLOAT16) * lookupTextureSize);

	tmpTexture->UnlockRect(0);

	if ( FAILED(D3DXCreateTexture(
		device, width, 1, 1, D3DUSAGE_DYNAMIC, D3DFMT_A16B16G16R16F, D3DPOOL_DEFAULT, &_lookUpTexture
		)) ) { return false; }

	// update texture
	if ( FAILED(device->UpdateTexture(tmpTexture, _lookUpTexture)) ) { return false; }

	tmpTexture->Release();

	return true;
}

/*
	Generate coefficients used in filtering process. Coefficients define offsets and weights for neighbouring
	samples used for sample determination. More information about coefficients can be found in GPU Gems 2,
	page 316.
*/
void BicubicFilter::GenCoeefficients(std::vector<float> *vec, int itemsNum)
{
	const float wMult = 1.0f / 6.0f;
	float alpha, alpha2, alpha3;
	float w0, w1, w2, w3;
	float g0, g1, h0, h1;

	float divider = (float) (itemsNum - 1);

	for (int i = 0; i < itemsNum; i++)
	{
		alpha = i / divider;	// alpha [0, 1], fractional part of texel coords
		alpha2 = alpha * alpha;
		alpha3 = alpha2 * alpha;

		// B-spline kernel - Coons cubics
		w0 =		-alpha3 + 3.0f * alpha2 - 3.0f * alpha + 1.0f;		// w0 = -1*a^3 + 3*a^2 - 3*a + 1
		w1 =  3.0f * alpha3 - 6.0f * alpha2 + 4.0f;						// w1 =  3*a^3 - 6*a^2 + 0*a + 4
		//w1 =  3.0f * alpha3 - 6.0f * alpha2 + 3.0f * alpha;
		w2 = -3.0f * alpha3 + 3.0f * alpha2 + 3.0f * alpha + 1.0f;		// w2 = -3*a^3 + 3*a^2 + 3*a + 0
		//w2 = - 3.0f * alpha3 + 3.0f * alpha2;
		w3 =		 alpha3;											// w3 =  1*a^3 + 0*a^2 + 0*a + 0

		w0 *= wMult;
		w1 *= wMult;
		w2 *= wMult;
		w3 *= wMult;

		// weights
		g0 = w0 + w1;
		g1 = w2 + w3;

		// offsets
		h0 = 1 - (w1 / g0) + alpha;
		h1 = 1 + (w3 / g1) - alpha;

		//std::cout << h1 << ", " << h0 << ", " << g1 << ", " << g0 << std::endl;

		vec->push_back(h1);
		vec->push_back(h0);
		vec->push_back(g1);
		vec->push_back(g0);
	}
}

/*
	Compilation of vertex shader and pixel shader. Return TRUE if there is no error during shaders compilation, 
	else return FALSE.
*/
bool BicubicFilter::CompileShaders(IDirect3DDevice9 *device)
{
	std::ifstream fxStream;
	
	// HLSL shader file located in same directory as *.exe
	fxStream.open("BicubicFilter.fx");

	if (fxStream.bad()) { fxStream.close(); std::cout << "File not found" << std::endl; }

	// lenght of compiled file
	fxStream.seekg(0, std::ios::end);
	int length = fxStream.tellg();
	fxStream.seekg(0, std::ios::beg);

	char *buffer = new char[length];

	// read *.fx file into buffer
	fxStream.read(buffer, length);

	fxStream.close();

	ID3DXBuffer *shader = 0;
	ID3DXBuffer *errors = 0;

	if ( FAILED(D3DXCompileShader(
		buffer, (UINT)strlen(buffer), 0, 0, "bicubic_filter_vs", "vs_3_0", D3DXSHADER_DEBUG, &shader, &errors, 0)) )
	{
		std::cout << (char *) errors->GetBufferPointer() << std::endl;

		errors->Release();

		return false;
	}

	// create vertex shader
	if ( FAILED(device->CreateVertexShader((const DWORD *)shader->GetBufferPointer(), &_vsShader)) ) 
	{ 
		return false; 
	}

	shader->Release();
	shader = 0;

	if ( FAILED(D3DXCompileShader(
		buffer, (UINT)strlen(buffer), 0, 0, "bicubic_filter_ps", "ps_3_0", D3DXSHADER_DEBUG, &shader, &errors, &_constTable)) )
	{
		std::cout << (char *) errors->GetBufferPointer() << std::endl;

		errors->Release();

		return false;
	}

	// create pixel shader
	if ( FAILED(device->CreatePixelShader((const DWORD *) shader->GetBufferPointer(), &_psShader)) )
	{ 
		return false; 
	}

	if (shader) shader->Release();
	if (errors) errors->Release();

	return true;
}

/* 
	Vertex buffer creation and intitialization. Vertex buffer is used for full screen quad. Return FALSE if creation fail, else TRUE.
 */
bool BicubicFilter::CreateVertexBuffer(IDirect3DDevice9 *device)
{
	// full screen aligned quad
	if ( FAILED(device->CreateVertexBuffer(
		sizeof(Vertex) * 4, D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, &_vb, 0)) )
	{ 
		return false; 
	}

	Vertex *ptr = 0;

	// vertices initialization
	if ( FAILED(_vb->Lock(0, sizeof(Vertex) * 4, (void **)&ptr, 0)) ) 
	{ 
		return false; 
	}

	const Vertex vertices[4] = {
		D3DXVECTOR3(-1.0f, 1.0f, 0.0f), D3DXVECTOR2(0, 0),
		D3DXVECTOR3( 1.0f, 1.0f, 0.0f), D3DXVECTOR2(1, 0),
		D3DXVECTOR3(-1.0f,-1.0f, 0.0f), D3DXVECTOR2(0, 1),
		D3DXVECTOR3( 1.0f,-1.0f, 0.0f), D3DXVECTOR2(1, 1)
	};

	memcpy(ptr, vertices, sizeof(Vertex) * 4);

	_vb->Unlock();

	return true;
}
