
#include "multiPassMat.hpp"

#include <Es/Strings/bString.hpp>
#include <Es/Algorithms/qSort.hpp>

LSError SinglePassInfo::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("material",material,0))
    CHECK(ar.Serialize("texture",texture,0))
    CHECK(ar.Serialize("tlAlpha",tlAlpha,0,0.0f))
    CHECK(ar.Serialize("trAlpha",trAlpha,0,0.0f))
    CHECK(ar.Serialize("blAlpha",blAlpha,0,0.0f))
    CHECK(ar.Serialize("brAlpha",brAlpha,0,0.0f))
    return LSOK;
}

MultipassMaterial::MultipassMaterial()
{
}
LSError MultipassMaterial::Serialize(ParamArchive &ar)
{
  if (ar.IsSaving())
  {
    for (int i=0; i<_passes.Size(); i++)
    {
      BString<64> name;
      sprintf(name,"Pass%d",i);
      SinglePassInfo &value = _passes[i];
      LSError err = ar.SerializeItem(RStringB(name),value,0);
    }
  }
  return LSOK;
}

void MultipassMaterial::AddPass(const SinglePassInfo &pass)
{
  // check if the same material is already present
  for (int i=0; i<_passes.Size(); i++)
  {
    SinglePassInfo &old = _passes[i];
    if (!strcmpi(old.material,pass.material))
    {
      // merge together - easy task
      old.tlAlpha += pass.tlAlpha;
      old.trAlpha += pass.trAlpha;
      old.blAlpha += pass.blAlpha;
      old.brAlpha += pass.brAlpha;
      return;
    }
  }
  // a new material - append it
  _passes.Add(pass);
}

static int CmpPass(const SinglePassInfo *pass1, const SinglePassInfo *pass2)
{
  float p1Alpha = pass1->tlAlpha+pass1->trAlpha+pass1->brAlpha+pass1->blAlpha;
  float p2Alpha = pass2->tlAlpha+pass2->trAlpha+pass2->brAlpha+pass2->blAlpha;
  if (p2Alpha<p1Alpha) return -1;
  if (p2Alpha>p1Alpha) return +1;
  return 0;
}

void MultipassMaterial::Close()
{
  // sort by importance - importance is given by total area covered, i.e. alpha sum
  QSort(_passes.Data(),_passes.Size(),CmpPass);
}