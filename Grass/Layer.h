#ifndef _LAYER_H_
#define _LAYER_H_

#include "GrassPrimitiveStream.h"
#include "Texture.h"
#include "HeightMap.h"

#define D3DFVF_LAYERCLOSEVERTEX (\
  D3DFVF_XYZ|\
  D3DFVF_NORMAL|\
  D3DFVF_DIFFUSE|\
  D3DFVF_TEX3|\
  D3DFVF_TEXCOORDSIZE2(0)|\
  D3DFVF_TEXCOORDSIZE3(1)|\
  D3DFVF_TEXCOORDSIZE3(2))

//! Vertex the landscape close to player is built from.
struct SLayerCloseVertex {
  // Vertex Position.
  Vector3 Position;
  // Vertex Normal.
  Vector3 Normal;
  // Diffuse
  D3DCOLOR Diffuse;
  // Base texture coordinates
  float U0, V0;
  // S coordinate of the normal map
  Vector3 S;
  // T coordinate of the normal map
  Vector3 T;
};

DWORD dwLayerCloseDecl[];

class CLayer {
public:
  virtual void AddLayer2(int X, int Y, int HalfSize, IDirect3DTexture8 *pTexture, D3DXMATRIX &MatView, D3DXMATRIX &MatProj) = 0;
  //! Adding of a layer into final screen itself.
  /*!
    This method is used to generate a close neighbourhood of a player.
    Neigbourhood which doesn't vary too much - to reduce number of textures
    used in it and therefore number of nacessary walkthroughs.
    \param PSLayer Primitives to render in each walkthrough. Structure of the vertices must be known.
    \param X X coordinate in the heigth map rounded to the closest integer value.
    \param Y Y coordinate in the heigth map rounded to the closest integer value.
    \param HalfSize Half size of the player neighbourhood.
    \param MatView View matrix.
    \param MatProj Projection matrix.
  */
  virtual void AddLayerToScreen(
    CGrassPrimitiveStream *PSLayer,
    int X,
    int Y,
    D3DXVECTOR3 &LightVector,
    D3DXMATRIX &MatView,
    D3DXMATRIX &MatProj) = 0;
  //! Drawing of layer components into world
  virtual void DrawLayer(D3DXMATRIX &MatView, D3DXMATRIX &MatProj, D3DXVECTOR3 &LightVector, CHeightMap &HeightMap, Vector3Par Pos) = 0;
};

#endif