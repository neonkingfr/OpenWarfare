#ifndef _grassengine_h_
#define _grassengine_h_

#include "debugLog.hpp"
#include "math3d.hpp"
#include "pointers.hpp"
#include "grassprimitivestream.h"

#define GRASSPATCH_SIZE 5.0f
#define D3DFVF_GRASSBUNCHVERTEX (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_DIFFUSE|D3DFVF_TEX1|D3DFVF_TEXCOORDSIZE2(0))

// Grass bunch
struct SGrassBunchVertex {
  // Vertex Position.
  Vector3 Position;
  // Vertex Normal.
  Vector3 Normal;
  // Diffuse color.
  D3DCOLOR Diffuse;
  // Coordinates in the normal map and at the same time coordinates in the RGB map.
  float U1, V1;
};

class CGrassEngine : public RefCount {
private:
  //! 3D Device.
  ComRef<IDirect3DDevice8> _pD3DDevice;
  //! Stream of grass bunch.
  Ref<CGrassPrimitiveStream> _pPSGrassBunch;
  //! Vertical grass texture.
  ComRef<IDirect3DTexture8> _pTVertical;
  ComRef<IDirect3DTexture8> _pTLayer0;
  ComRef<IDirect3DTexture8> _pTLayer1;
  ComRef<IDirect3DTexture8> _pTLayer2;
  //! Generate vertical grass bunch.
  void GenerateVerticalGrassBunch(CGrassPrimitiveStream &PSVertical, Matrix4Par Origin);
  //! Generate a layer.
  void GenerateLayer(CGrassPrimitiveStream &PS, int Stage, IDirect3DTexture8 *pTexture, Matrix4Par Origin, float Height);
  //! Generate vertical grass.
  void GenerateVerticalGrass(CGrassPrimitiveStream &PSVertical);
public:
  //! Initialization.
  void Init(IDirect3DDevice8 *pD3DDevice);
  //! Drawing of a square covered with grass.
  void DrawGrass(
    D3DXMATRIX &MatView,
    D3DXMATRIX &MatProj,
    int Seed,
    float Detail,
    Vector3Par Position);
};

#endif