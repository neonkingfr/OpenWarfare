#include "LayerGround.h"
#include "ARGB.h"

struct SGroundVertex {
  // Vertex Position.
  Vector3 Position;
  // Texture coordinates.
  float U1, V1;
  float U2, V2;
};

#define D3DFVF_GROUNDVERTEX (D3DFVF_XYZ|D3DFVF_TEX2|D3DFVF_TEXCOORDSIZE2(0)|D3DFVF_TEXCOORDSIZE2(1))

int Log2(int Number) {
  int Result = 0;
  while (Number > 0) {
    Number >>= 1;
    Result++;
  }
  return Result;
}

void CLayerGround::Init(IDirect3DDevice8 *pD3DDevice) {
  // Save device
  _pD3DDevice = pD3DDevice; pD3DDevice->AddRef();


  _TEnv.Init(pD3DDevice, "Ground.tga");
  //_TA.Init(pD3DDevice, "A0128.tga");
  //_TB.Init(pD3DDevice, "B0128.tga");
  //_TC.Init(pD3DDevice, "C0128.tga");
  _TA.Init(pD3DDevice, "A", Size1024x1024, 9);
  _TB.Init(pD3DDevice, "B", Size1024x1024, 9);
  _TC.Init(pD3DDevice, "C", Size1024x1024, 9);

/*
  D3DXCreateTextureFromFile(pD3DDevice, "A0512.tga", _pTBaseA.Init());
  D3DXCreateTextureFromFile(pD3DDevice, "B0512.tga", _pTBaseB.Init());
  D3DXCreateTextureFromFile(pD3DDevice, "C0512.tga", _pTBaseC.Init());

  D3DXCreateTextureFromFile(pD3DDevice, "AN0512.tga", _pTBaseAN.Init());
  D3DXCreateTextureFromFile(pD3DDevice, "BN0512.tga", _pTBaseBN.Init());
  D3DXCreateTextureFromFile(pD3DDevice, "CN0512.tga", _pTBaseCN.Init());
*/

  D3DXCreateTextureFromFileEx(
    pD3DDevice, "AJehlici.tga", D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
    D3DPOOL_DEFAULT, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_DEFAULT,
    D3DX_DEFAULT, 0, NULL, NULL, _pTBaseA.Init());
  D3DXCreateTextureFromFileEx(
    pD3DDevice, "BSuchaTrava.tga", D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
    D3DPOOL_DEFAULT, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_DEFAULT,
    D3DX_DEFAULT, 0, NULL, NULL, _pTBaseB.Init());
  D3DXCreateTextureFromFileEx(
    pD3DDevice, "CSkala.tga", D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
    D3DPOOL_DEFAULT, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_DEFAULT,
    D3DX_DEFAULT, 0, NULL, NULL, _pTBaseC.Init());


  D3DXCreateTextureFromFileEx(
    pD3DDevice, "AJehliciN.tga", D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
    D3DPOOL_DEFAULT, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_DEFAULT,
    D3DX_DEFAULT, 0, NULL, NULL, _pTBaseAN.Init());
  D3DXCreateTextureFromFileEx(
    pD3DDevice, "BSuchaTravaN.tga", D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
    D3DPOOL_DEFAULT, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_DEFAULT,
    D3DX_DEFAULT, 0, NULL, NULL, _pTBaseBN.Init());
  D3DXCreateTextureFromFileEx(
    pD3DDevice, "CSkalaN.tga", D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
    D3DPOOL_DEFAULT, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_DEFAULT,
    D3DX_DEFAULT, 0, NULL, NULL, _pTBaseCN.Init());



/*
  CTexture TTemp;
  TTemp.Init(pD3DDevice, "AJehlici.tga");
  TTemp.Normalize2();
  TTemp.CreateTexture(pD3DDevice, _pTBaseAN);
  D3DXSaveTextureToFile("AJehliciN.bmp", D3DXIFF_BMP, _pTBaseAN, NULL);
  TTemp.Init(pD3DDevice, "BSuchaTrava.tga");
  TTemp.Normalize2();
  TTemp.CreateTexture(pD3DDevice, _pTBaseBN);
  D3DXSaveTextureToFile("BSuchaTravaN.bmp", D3DXIFF_BMP, _pTBaseBN, NULL);
  TTemp.Init(pD3DDevice, "CSkala.tga");
  TTemp.Normalize2();
  TTemp.CreateTexture(pD3DDevice, _pTBaseCN);
  D3DXSaveTextureToFile("CSkalaN.bmp", D3DXIFF_BMP, _pTBaseCN, NULL);
*/





  // Create vertex buffer
  _pD3DDevice->CreateVertexBuffer(6 * sizeof(SGroundVertex), D3DUSAGE_WRITEONLY, D3DFVF_GROUNDVERTEX, D3DPOOL_DEFAULT, _pVB.Init());

  // Create shaders
  ID3DXBuffer *pCode;
  D3DXAssembleShaderFromFile("LayerGround.vsh", 0, NULL, &pCode, NULL);
  pD3DDevice->CreateVertexShader(dwLayerCloseDecl, (DWORD*)pCode->GetBufferPointer(), &_hVS, 0);
  pCode->Release();
  D3DXAssembleShaderFromFile("LayerGround.psh", 0, NULL, &pCode, NULL );
  pD3DDevice->CreatePixelShader((DWORD*)pCode->GetBufferPointer(),&_hPS);
  pCode->Release();

  // Create textures
  _LocalHalfSize = 16;
  int FullSize = _LocalHalfSize * 2;
  _Texture.Init(FullSize, FullSize, 0);
  _Texture.CreateTexture(pD3DDevice, _pTAlfaA);
  _Texture.CreateTexture(pD3DDevice, _pTAlfaB);
  _Texture.CreateTexture(pD3DDevice, _pTAlfaC);

}

void CLayerGround::AddLayer2(int X, int Y, int HalfSize, IDirect3DTexture8 *pTexture, D3DXMATRIX &MatView, D3DXMATRIX &MatProj) {

  // Prepare vertex vertices
  SGroundVertex GV[6];
  GV[0].Position = Vector3(HalfSize, 0.0f, HalfSize);
  GV[0].U1 = 1.0f;
  GV[0].V1 = 0.0f;
  GV[0].U2 = X + HalfSize;
  GV[0].V2 = Y + HalfSize;
  GV[1].Position = Vector3(HalfSize, 0.0f, -HalfSize);
  GV[1].U1 = 1.0f;
  GV[1].V1 = 1.0f;
  GV[1].U2 = X + HalfSize;
  GV[1].V2 = Y - HalfSize;
  GV[2].Position = Vector3(-HalfSize, 0.0f, -HalfSize);
  GV[2].U1 = 0.0f;
  GV[2].V1 = 1.0f;
  GV[2].U2 = X - HalfSize;
  GV[2].V2 = Y - HalfSize;
  
  GV[3].Position = Vector3(HalfSize, 0.0f, HalfSize);
  GV[3].U1 = 1.0f;
  GV[3].V1 = 0.0f;
  GV[3].U2 = X + HalfSize;
  GV[3].V2 = Y + HalfSize;
  GV[4].Position = Vector3(-HalfSize, 0.0f, -HalfSize);
  GV[4].U1 = 0.0f;
  GV[4].V1 = 1.0f;
  GV[4].U2 = X - HalfSize;
  GV[4].V2 = Y - HalfSize;
  GV[5].Position = Vector3(-HalfSize, 0.0f, +HalfSize);
  GV[5].U1 = 0.0f;
  GV[5].V1 = 0.0f;
  GV[5].U2 = X - HalfSize;
  GV[5].V2 = Y + HalfSize;

  // Fill VB
  BYTE *pData;
  _pVB->Lock(0, 6 * sizeof(SGroundVertex), &pData, 0);
  memcpy(pData, GV, 6 * sizeof(SGroundVertex));
  _pVB->Unlock();

  // Set VB
  _pD3DDevice->SetStreamSource(0, _pVB, sizeof(SGroundVertex));
  _pD3DDevice->SetVertexShader(D3DFVF_GROUNDVERTEX);
  _pD3DDevice->SetPixelShader(0);

  // Set render state 
  _pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
  _pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
  _pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
  _pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);


  
  //_pD3DDevice->SetRenderState(D3DRS_ZENABLE, FALSE);
  //_pD3DDevice->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);

  // Set texture stages
  _pD3DDevice->SetTextureStageState(0, D3DTSS_ADDRESSU, D3DTADDRESS_WRAP);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_ADDRESSV, D3DTADDRESS_WRAP);
  _pD3DDevice->SetTextureStageState(1, D3DTSS_ADDRESSU, D3DTADDRESS_WRAP);
  _pD3DDevice->SetTextureStageState(1, D3DTSS_ADDRESSV, D3DTADDRESS_WRAP);

	_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP,   D3DTOP_SELECTARG1);
	_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAOP,   D3DTOP_SELECTARG1);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_MINFILTER, D3DTEXF_LINEAR);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_MAGFILTER, D3DTEXF_LINEAR);
	_pD3DDevice->SetTextureStageState(1, D3DTSS_COLORARG1, D3DTA_CURRENT);
	_pD3DDevice->SetTextureStageState(1, D3DTSS_COLORARG2, D3DTA_TEXTURE);
	_pD3DDevice->SetTextureStageState(1, D3DTSS_COLOROP,   D3DTOP_MODULATEALPHA_ADDCOLOR);
	_pD3DDevice->SetTextureStageState(1, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	_pD3DDevice->SetTextureStageState(1, D3DTSS_ALPHAOP,   D3DTOP_SELECTARG1);
  _pD3DDevice->SetTextureStageState(1, D3DTSS_MINFILTER, D3DTEXF_LINEAR);
  _pD3DDevice->SetTextureStageState(1, D3DTSS_MAGFILTER, D3DTEXF_LINEAR);

  // Zero surface
  _pD3DDevice->Clear(0L, NULL, D3DCLEAR_TARGET, 0xFF000000, 1.0f, 0L);


  int FullSize = HalfSize * 2;

  // Create texture
  CTexture Texture;
  ComRef<IDirect3DTexture8> _pTTemp;
  Texture.Init(FullSize, FullSize, 0);

  _pD3DDevice->BeginScene();

  // R -------------------------------------------
  // Load it with values for R as alpha
  for (int j = 0; j < FullSize; j++) {
    for (int i = 0; i < FullSize; i++) {
      Texture.SetPixel(i, j, (_TEnv.GetPixel(X + i - HalfSize, Y + j - HalfSize) << 8) & 0xFF000000);
    }
  }
  Texture.CreateTexture(_pD3DDevice, _pTTemp);

  // SetTexture
  _pD3DDevice->SetTexture(0, NULL);
  _pD3DDevice->SetTexture(0, _pTTemp);
  _pD3DDevice->SetTexture(1, NULL);
  _pD3DDevice->SetTexture(1, _pTBaseA);

  // Draw primitive
  _pD3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);

  // G -------------------------------------------
  // Load it with values for R as alpha
  for (j = 0; j < FullSize; j++) {
    for (int i = 0; i < FullSize; i++) {
      Texture.SetPixel(i, j, (_TEnv.GetPixel(X + i - HalfSize, Y + j - HalfSize) << 16) & 0xFF000000);
    }
  }
  Texture.PrepareTexture(_pD3DDevice, _pTTemp);

  // SetTexture
  _pD3DDevice->SetTexture(1, NULL);
  _pD3DDevice->SetTexture(1, _pTBaseB);

  // Draw primitive
  _pD3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);

  // B -------------------------------------------
  // Load it with values for R as alpha
  for (j = 0; j < FullSize; j++) {
    for (int i = 0; i < FullSize; i++) {
      Texture.SetPixel(i, j, (_TEnv.GetPixel(X + i - HalfSize, Y + j - HalfSize) << 24) & 0xFF000000);
    }
  }
  Texture.PrepareTexture(_pD3DDevice, _pTTemp);

  // SetTexture
  _pD3DDevice->SetTexture(1, NULL);
  _pD3DDevice->SetTexture(1, _pTBaseC);

  // Draw primitive
  _pD3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);

  _pD3DDevice->EndScene();

}

void CLayerGround::AddLayerToScreen(
  CGrassPrimitiveStream *PSLayer,
  int X,
  int Y,
  D3DXVECTOR3 &LightVector,
  D3DXMATRIX &MatView,
  D3DXMATRIX &MatProj) {

  // Prepare alpha textures
  int FullSize = _LocalHalfSize * 2;

  // ------------------------------------------------------------------------
  // Prepare textures
  int i, j;
  
  // Alfa A texture
  for (j = 0; j < FullSize; j++) {
    for (i = 0; i < FullSize; i++) {
      _Texture.SetPixel(i, j, (_TEnv.GetPixel(X + i - _LocalHalfSize, Y + j - _LocalHalfSize) << 8) & 0xFF000000);
    }
  }
  _Texture.PrepareTexture(_pD3DDevice, _pTAlfaA);

  // Alfa B texture
  for (j = 0; j < FullSize; j++) {
    for (i = 0; i < FullSize; i++) {
      _Texture.SetPixel(i, j, (_TEnv.GetPixel(X + i - _LocalHalfSize, Y + j - _LocalHalfSize) << 16) & 0xFF000000);
    }
  }
  _Texture.PrepareTexture(_pD3DDevice, _pTAlfaB);

  // Alfa C texture
  for (j = 0; j < FullSize; j++) {
    for (i = 0; i < FullSize; i++) {
      _Texture.SetPixel(i, j, (_TEnv.GetPixel(X + i - _LocalHalfSize, Y + j - _LocalHalfSize) << 24) & 0xFF000000);
    }
  }
  _Texture.PrepareTexture(_pD3DDevice, _pTAlfaC);

  // ------------------------------------------------------------------------
  // Set shaders

  // Set texture stages
  //_pD3DDevice->SetTextureStageState(0, D3DTSS_ADDRESSU, D3DTADDRESS_WRAP);
  //_pD3DDevice->SetTextureStageState(0, D3DTSS_ADDRESSV, D3DTADDRESS_WRAP);

  _pD3DDevice->SetTextureStageState(0, D3DTSS_ADDRESSU, D3DTADDRESS_BORDER);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_ADDRESSV, D3DTADDRESS_BORDER);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_BORDERCOLOR, 0x00000000);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_MIPFILTER, D3DTEXF_POINT);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_MINFILTER, D3DTEXF_LINEAR);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_MAGFILTER, D3DTEXF_LINEAR);

  _pD3DDevice->SetTextureStageState(1, D3DTSS_ADDRESSU, D3DTADDRESS_WRAP);
  _pD3DDevice->SetTextureStageState(1, D3DTSS_ADDRESSV, D3DTADDRESS_WRAP);
  _pD3DDevice->SetTextureStageState(1, D3DTSS_MIPFILTER, D3DTEXF_POINT);
  _pD3DDevice->SetTextureStageState(1, D3DTSS_MINFILTER, D3DTEXF_LINEAR);
  _pD3DDevice->SetTextureStageState(1, D3DTSS_MAGFILTER, D3DTEXF_LINEAR);

  // Set shaders
  _pD3DDevice->SetVertexShader(_hVS);
  _pD3DDevice->SetPixelShader(_hPS);
  
  // Light vector
  D3DXVECTOR3 TLV;
  D3DXVec3TransformNormal(&TLV, &LightVector, &MatView);

  // Set constants of the vertex shader
  D3DXMATRIX MatTemp;
  D3DXMatrixTranspose(&MatTemp, &MatView);
  _pD3DDevice->SetVertexShaderConstant(0, &MatTemp, 4);
  D3DXMatrixTranspose(&MatTemp, &MatProj);
  _pD3DDevice->SetVertexShaderConstant(4, &MatTemp, 4);
  float C8[] = {0.5f, 1.0f, 0.0f, 0.0f};
  _pD3DDevice->SetVertexShaderConstant(8, C8, 1);
  float C9[] = {(float)(X - _LocalHalfSize), (float)(Y - _LocalHalfSize), 1.0f/(_LocalHalfSize * 2.0f), 0.0f};
  _pD3DDevice->SetVertexShaderConstant(9, C9, 1);
  float C10[] = {TLV.x, TLV.y, TLV.z, 0.0f};
  _pD3DDevice->SetVertexShaderConstant(10, C10, 1);
  float C11[] = {1.0f, 1.0f, 1.0f, 0.0f}; // Direction light color
  _pD3DDevice->SetVertexShaderConstant(11, C11, 1);

  // Set constants of the pixel shader
  float pC0[] = {0.4f, 0.4f, 0.4f, 0.0f}; // Ambient light color
  _pD3DDevice->SetPixelShaderConstant(0, pC0, 1);

  // ------------------------------------------------------------------------
  // A layer
  
  // Set textures
  PSLayer->RegisterTextures(0, _pTAlfaA, _pTBaseA, _pTBaseAN);

  // Set proper blending for the first walkthrough
  _pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

  // Draw
  PSLayer->Draw();

  // ------------------------------------------------------------------------
  // B layer
  
  // Set textures
  PSLayer->RegisterTextures(0, _pTAlfaB, _pTBaseB, _pTBaseBN);

  // Set proper blending for the rest of walkthroughs
  _pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
  _pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
  _pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
  _pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);

  // Draw
  PSLayer->Draw();

  // ------------------------------------------------------------------------
  // C layer
  
  // Set textures
  PSLayer->RegisterTextures(0, _pTAlfaC, _pTBaseC, _pTBaseCN);

  // Draw
  PSLayer->Draw();

}

void CLayerGround::DrawLayer(D3DXMATRIX &MatView, D3DXMATRIX &MatProj, D3DXVECTOR3 &LightVector, CHeightMap &HeightMap, Vector3Par Pos) {
}
