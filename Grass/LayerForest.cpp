#include "LayerForest.h"

#define POSX 60.0f
#define POSZ 45.0f

void CLayerForest::Init(IDirect3DDevice8 *pD3DDevice) {
  TreeEngine.Init(pD3DDevice);
  TreeEngine.Load("pokus.txt");
  //TreeEngine.Load("flyman2.txt");

  // Prepare the model
/*
  Matrix4 Origin;
  Origin.SetDirectionAndUp(Vector3(0, 1, 0), Vector3(1, 0, 0));
  Origin.SetPosition(Vector3(POSX, 9.5, POSZ));
  TreeEngine.PrepareTreeModel(0, 1.0f, Origin, 1.0f);
*/
}

void CLayerForest::AddLayer2(int X, int Y, int HalfSize, IDirect3DTexture8 *pTexture, D3DXMATRIX &MatView, D3DXMATRIX &MatProj) {

  Matrix4 Origin;
  Origin.SetDirectionAndUp(Vector3(0.75, 0.75, 0), Vector3(1, 0, 0));
  Origin.SetPosition(Vector3(POSX - X - 0.5f, 0.0f, -POSZ + Y + 0.5f));
  TreeEngine.DrawTree(MatView, MatProj, 0, 1.0f, Origin, 2.0f, Vector3(1, 0, 0), Vector3(1, 0, 0), 1.0f);

}

void CLayerForest::AddLayerToScreen(
  CGrassPrimitiveStream *PSLayer,
  int X,
  int Y,
  D3DXVECTOR3 &LightVector,
  D3DXMATRIX &MatView,
  D3DXMATRIX &MatProj) {
}

void CLayerForest::DrawLayer(D3DXMATRIX &MatView, D3DXMATRIX &MatProj, D3DXVECTOR3 &LightVector, CHeightMap &HeightMap, Vector3Par Pos) {
	// Transform light vector to be in model view coordinates (because light is moving with rotation)
	D3DXVECTOR3 tld;
	D3DXVec3TransformNormal(&tld, &LightVector, &MatView);
  
  //TreeEngine.DrawTreeModel(MatView, MatProj, Vector3(tld.x, tld.y, tld.z), Vector3(1, 0, 0), 1.0f);
/*
  Matrix4 Origin;
  Origin.SetDirectionAndUp(Vector3(0, 1, 0), Vector3(1, 0, 0));
  Origin.SetPosition(Vector3(POSX, 9.5, POSZ));
  TreeEngine.DrawTree(MatView, MatProj, 0, 1.0f, Origin, 1.0f, Vector3(tld.x, tld.y, tld.z), Vector3(1, 0, 0), 1.0f);
*/

/*
  for (int j = 30; j < 50; j++) {
    for (int i = 50; i < 70; i++) {
      Matrix4 Origin = HeightMap.GetTangentPlane(i, j);
      float Detail = 1.0f - min(Pos.Distance(Origin.Position()) * 0.1f, 1.0f);
      TreeEngine.DrawTree(
        MatView,
        MatProj,
        0,
        Detail,
        Origin,
        1.0f,
        Vector3(tld.x, tld.y, tld.z),
        Vector3(1, 0, 0),
        1.0f);

    }
  }
*/

  srand(0);
  for (int x = 0; x < 100; x++) {
    float I = 50.0f + ((float)rand() * 10.0f)/((float)RAND_MAX);
    float J = 30.0f + ((float)rand() * 10.0f)/((float)RAND_MAX);
    Matrix4 Origin = HeightMap.GetTangentPlane(I, J);
    float Detail = 1.0f - min(Pos.Distance(Origin.Position()) * 0.1f, 1.0f);
    TreeEngine.DrawTree(
      MatView,
      MatProj,
      0,
      Detail,
      Origin,
      1.0f,
      Vector3(tld.x, tld.y, tld.z),
      Vector3(1, 0, 0),
      1.0f);

  }
}
