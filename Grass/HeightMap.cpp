#include <stdlib.h>
#include "HeightMap.h"

void CHeightMap::Init(const char *FileName) {
  FILE * f = fopen(FileName,"r");
  if (f != NULL) {
	  for(unsigned int i = 0; i < HEIGHTMAP_SIZE * HEIGHTMAP_SIZE; i ++)
	  {
		  float fTempX, fTempY, fTempZ;
		  int nTemp;
		  fscanf(f,"*MESH_VERTEX %d %f %f %f\n",&nTemp, &fTempX, &fTempY, &fTempZ);
      _Data[i] = fTempZ/10.0f + ((float)rand() / RAND_MAX) * 0.2f;
	  }
	  fclose(f);
  }
}

float CHeightMap::GetHeight(int I, int J) {
  return _Data[J * HEIGHTMAP_SIZE + I];
}

float CHeightMap::GetHeight(float I, float J) {
  int IntPosI = (int)I;
  int IntPosJ = (int)J;
  float FractPosI = I - (float)IntPosI;
  float FractPosJ = J - (float)IntPosJ;
  float H00 = _Data[(IntPosJ    ) * HEIGHTMAP_SIZE + (IntPosI    )];
  float H10 = _Data[(IntPosJ    ) * HEIGHTMAP_SIZE + (IntPosI + 1)];
  float H01 = _Data[(IntPosJ + 1) * HEIGHTMAP_SIZE + (IntPosI    )];
  float H11 = _Data[(IntPosJ + 1) * HEIGHTMAP_SIZE + (IntPosI + 1)];
  float Coef0001 = 1.0f - FractPosI;
  float Coef1011 = FractPosI;
  float Coef0010 = 1.0f - FractPosJ;
  float Coef0111 = FractPosJ;
  float Coef00 = Coef0001 * Coef0010;
  float Coef01 = Coef0001 * Coef0111;
  float Coef10 = Coef1011 * Coef0010;
  float Coef11 = Coef1011 * Coef0111;
  return H00 * Coef00 + H10 * Coef10 + H01 * Coef01 + H11 * Coef11;
}

Matrix4 CHeightMap::GetTangentPlane(float I, float J) {
  Matrix4 Result;
  Vector3 Position = Vector3(I, GetHeight(I, J), J);
  Vector3 Aside = Vector3(1, GetHeight(I + 0.5f, J) - GetHeight(I - 0.5f, J), 0).Normalized();
  Vector3 Up = -Vector3(0, GetHeight(I, J + 0.5f) - GetHeight(I, J - 0.5f), 1).Normalized();
  Result.SetPosition(Position);
  Result.SetUpAndAside(Up, Aside);
  return Result;
}

Vector3 CHeightMap::GetNormal(float I, float J) {

  return Vector3(0, 0, 0);
}