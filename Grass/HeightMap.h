#ifndef _HEIGHTMAP_H_
#define _HEIGHTMAP_H_

#include "debugLog.hpp"
#include "math3d.hpp"
#include "pointers.hpp"

#define HEIGHTMAP_SIZE 129

class CHeightMap {
  float _Data[HEIGHTMAP_SIZE * HEIGHTMAP_SIZE];
public:
  void Init(const char *FileName);
  float GetHeight(int I, int J);
  float GetHeight(float I, float J);
  Matrix4 GetTangentPlane(float I, float J);
  Vector3 GetNormal(float I, float J);
};

#endif