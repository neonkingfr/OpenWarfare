#include "NeighbourLayer.h"

void CNeighbourLayer::Init(IDirect3DDevice8 *pD3DDevice, int HalfSize) {
  // Save device
  _pD3DDevice = pD3DDevice; pD3DDevice->AddRef();
  // Init layer texture
  _pD3DDevice->CreateTexture(NLTEXTURESIZE, NLTEXTURESIZE, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, _pTLayer.Init());
  _pTLayer->GetSurfaceLevel(0, _pSLayer.Init());
  // Init size
  _HalfSize = HalfSize;
  // Set rest values to default
  _PosX = 0;
  _PosY = 0;
}

void CNeighbourLayer::UpdatePos(int PosX, int PosY) {
  _PosX = PosX;
  _PosY = PosY;
}

void CNeighbourLayer::BeginRendering() {

  // Set Projection matrix to be orthogonal
  D3DXMatrixOrthoRH(&_MatProj, _HalfSize * 2.0f, _HalfSize * 2.0f, -100.0f, 100.0f);
  _pD3DDevice->SetTransform(D3DTS_PROJECTION, &_MatProj);

  // Set View matrix to identity
  //D3DXMatrixIdentity(&MatView);
  D3DXMatrixLookAtLH(&_MatView, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, -1, 0), &D3DXVECTOR3(0, 0, 1));
  _pD3DDevice->SetTransform(D3DTS_VIEW, &_MatView);

  // Set new target and remember the old one
	_pD3DDevice->GetRenderTarget(_pOldRenderTarget.Init());
	_pD3DDevice->GetDepthStencilSurface(_pOldZStencilSurface.Init());
  _pD3DDevice->SetRenderTarget(_pSLayer, NULL);

  // Start rendering the scene
  //_pD3DDevice->BeginScene();
}

void CNeighbourLayer::EndRendering() {
  // Restore former render target
  _pD3DDevice->SetRenderTarget(_pOldRenderTarget, _pOldZStencilSurface);

  // End rendering the scene
  //_pD3DDevice->EndScene();
}

void CNeighbourLayer::AddLayer2(CLayer *pLayer) {
  pLayer->AddLayer2(_PosX, _PosY, _HalfSize, _pTLayer, _MatView, _MatProj);
}

void CNeighbourLayer::AddLayerClear(CLayer *pLayer) {
  _pD3DDevice->Clear(0L, NULL, D3DCLEAR_TARGET, 0xFF004400, 1.0f, 0L);
}

IDirect3DTexture8 *CNeighbourLayer::GetTexture() {
  return _pTLayer;
}

void CNeighbourLayer::GetTextureCoord(int X, int Y, float &u, float &v) {
  float OneMeterInTextureSpace = 1.0f / ((float)_HalfSize * 2.0f);
  u = ((float)(X - _PosX + _HalfSize)) * OneMeterInTextureSpace;
  v = ((float)(Y - _PosY + _HalfSize)) * OneMeterInTextureSpace;
}

int CNeighbourLayer::NeedUpdate(float EyeX, float EyeY) {
  if ((fabs((float)_PosX + 0.5f - EyeX) > ((float)_HalfSize + 0.5f) * 0.5f) ||
      (fabs((float)_PosY + 0.5f - EyeY) > ((float)_HalfSize + 0.5f) * 0.5f)) {
    return 1;
  }
  else {
    return 0;
  }
}
