vs.1.1

; Shader for rendering of the ground neighbourhood of the player
;
; Input:
;
;  c0-c3  - World * View matrix (4x4)
;  c4-c7  - Projection matrix (4x4)
;  c8     - Vector of useful constants (0.5, 1.0, 0, 0)
;  c9.x   - PosX - Halfsize
;  c9.y   - PosY - Halfsize
;  c9.z   - 1.0f / (HalfSize * 2 + 1)
;  c10    - Vector of light in world coordinates
;
;  v0     - Vertex position (x, y, z, 1) related to W*V*P matrix
;  v3     - Vertex normal (x, y, z, 1) related to W*V matrix
;  v5     - Diffuse color
;  v7     - Base texture coordiante
;  v8     - S coordiante
;  v9     - T coordiante
;
; Output:
;
;  oPos.xyz - Transformed position (related to V+P matrix)
;  oD0      - Light color of the vertex
;  oD1      - Transformed vector of light (related to normal, S and T).
;  oT0.xy   - Alpha coordinate
;  oT1.xy   - Base coordinate
;  oT2.xy   - Base normal coordinate

; Position transformation
m4x4 r0, v0, c0     ; W * V matrix transformation
m4x4 oPos, r0, c4   ; Projection matrix transformation

; Diffuse color. In future shader will accumulate list of lights into this register.
mov oD0.xyz, v5

; Determine Alpha coordinates
sub r0.xy, v0.xz, c9.xy
mul oT0.xy, r0.xy, c9.zz

; Determine Base coordinates
mov oT1.xy, v7.xy

; Determine Base normal coordinates
mov oT2.xy, v7.xy

; Transformation texture mapping coordinates into world coordiantes and creating of matrix
; which will convert vector in world coordinates (light) to texture mapping coordinates.
; V will be negative because of the fact texture has 0,0 coordinate at the top whereas
; render coordinate 0, 0, 0 is in the bottom.
; Normal will be negative because of expressing orientation of the polygon in standard way.
; (Z coordinate points away)
m3x3 r0, v8, c0  ; S
m3x3 r1, v9, c0  ; T
m3x3 r2, -v3, c0 ; -Normal

; Transformation of light vector in world coordinates into texture mapping coordiantes.
; Negative value is here in order the dot product in ps will return proper value.
m3x3 r3, -c10, r0
mad oD1.xyz, r3, c8.xxxx, c8.xxxx
