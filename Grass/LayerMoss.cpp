#include "LayerMoss.h"

void CLayerMoss::Init(IDirect3DDevice8 *pD3DDevice) {
  // Save device
  _pD3DDevice = pD3DDevice; pD3DDevice->AddRef();

  // Load ground
  _TEnv.Init(pD3DDevice, "Moss.tga");

  // Load base texture
  //D3DXCreateTextureFromFile(pD3DDevice, "Mech.tga", _pTBase.Init());

  D3DXCreateTextureFromFileEx(
    pD3DDevice, "Mech.tga", D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
    D3DPOOL_DEFAULT, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_DEFAULT,
    D3DX_DEFAULT, 0, NULL, NULL, _pTBase.Init());

  D3DXCreateTextureFromFileEx(
    pD3DDevice, "MechN.tga", D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
    D3DPOOL_DEFAULT, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_DEFAULT,
    D3DX_DEFAULT, 0, NULL, NULL, _pTBaseN.Init());


/*
  CTexture TTemp;
  TTemp.Init(pD3DDevice, "Mech.tga");
  TTemp.Normalize2();
  TTemp.CreateTexture(pD3DDevice, _pTBaseN);
  D3DXSaveTextureToFile("MechN.bmp", D3DXIFF_BMP, _pTBaseN, NULL);
*/

  //D3DXCreateTextureFromFile(pD3DDevice, "ListN.tga", _pTBaseN.Init());


  // Create shaders
  ID3DXBuffer *pCode;
  D3DXAssembleShaderFromFile("LayerMoss.vsh", 0, NULL, &pCode, NULL);
  pD3DDevice->CreateVertexShader(dwLayerCloseDecl, (DWORD*)pCode->GetBufferPointer(), &_hVS, 0);
  pCode->Release();
  D3DXAssembleShaderFromFile("LayerMoss.psh", 0, NULL, &pCode, NULL );
  pD3DDevice->CreatePixelShader((DWORD*)pCode->GetBufferPointer(),&_hPS);
  pCode->Release();
  
  // Create textures
  _LocalHalfSize = 16;
  int FullSize = _LocalHalfSize * 2;
  _Texture.Init(FullSize, FullSize, 0);
  _Texture.CreateTexture(pD3DDevice, _pTAlfa);

}

void CLayerMoss::AddLayer2(int X, int Y, int HalfSize, IDirect3DTexture8 *pTexture, D3DXMATRIX &MatView, D3DXMATRIX &MatProj) {

}

void CLayerMoss::AddLayerToScreen(
  CGrassPrimitiveStream *PSLayer,
  int X,
  int Y,
  D3DXVECTOR3 &LightVector,
  D3DXMATRIX &MatView,
  D3DXMATRIX &MatProj) {

  // Prepare alpha textures
  int FullSize = _LocalHalfSize * 2;

  // ------------------------------------------------------------------------
  // Prepare textures
  int i, j;
  
  // Alfa A texture
  for (j = 0; j < FullSize; j++) {
    for (i = 0; i < FullSize; i++) {
      _Texture.SetPixel(i, j, (_TEnv.GetPixel(X + i - _LocalHalfSize, Y + j - _LocalHalfSize) << 8) & 0xFF000000);
    }
  }
  _Texture.PrepareTexture(_pD3DDevice, _pTAlfa);

  // Set texture stages
  _pD3DDevice->SetTextureStageState(0, D3DTSS_ADDRESSU, D3DTADDRESS_BORDER);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_ADDRESSV, D3DTADDRESS_BORDER);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_BORDERCOLOR, 0x00000000);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_MIPFILTER, D3DTEXF_POINT);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_MINFILTER, D3DTEXF_POINT);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_MAGFILTER, D3DTEXF_POINT);


  _pD3DDevice->SetTextureStageState(1, D3DTSS_ADDRESSU, D3DTADDRESS_WRAP);
  _pD3DDevice->SetTextureStageState(1, D3DTSS_ADDRESSV, D3DTADDRESS_WRAP);
  _pD3DDevice->SetTextureStageState(1, D3DTSS_MIPFILTER, D3DTEXF_POINT);

  // Set shaders
  _pD3DDevice->SetVertexShader(_hVS);
  _pD3DDevice->SetPixelShader(_hPS);
  
  // Light vector
  D3DXVECTOR3 TLV;
  D3DXVec3TransformNormal(&TLV, &LightVector, &MatView);

  // Set constants of the vertex shader
  D3DXMATRIX MatTemp;
  D3DXMatrixTranspose(&MatTemp, &MatView);
  _pD3DDevice->SetVertexShaderConstant(0, &MatTemp, 4);
  D3DXMatrixTranspose(&MatTemp, &MatProj);
  _pD3DDevice->SetVertexShaderConstant(4, &MatTemp, 4);
  float C8[] = {0.5f, 1.0f, 0.0f, 0.0f};
  _pD3DDevice->SetVertexShaderConstant(8, C8, 1);
  float C9[] = {(float)(X - _LocalHalfSize), (float)(Y - _LocalHalfSize), 1.0f/(_LocalHalfSize * 2.0f), 0.0f};
  _pD3DDevice->SetVertexShaderConstant(9, C9, 1);
  float C10[] = {TLV.x, TLV.y, TLV.z, 0.0f};
  _pD3DDevice->SetVertexShaderConstant(10, C10, 1);

  // ------------------------------------------------------------------------
  // The layer
  
  // Set textures
  PSLayer->RegisterTextures(0, _pTAlfa, _pTBase, _pTBaseN);

  // Set proper blending for the walkthrough
  _pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
  _pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
  _pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
  _pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);

  // Draw
  PSLayer->Draw();
}

void CLayerMoss::DrawLayer(D3DXMATRIX &MatView, D3DXMATRIX &MatProj, D3DXVECTOR3 &LightVector, CHeightMap &HeightMap, Vector3Par Pos) {
  
}
