# Microsoft Developer Studio Project File - Name="Grass" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Grass - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Grass.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Grass.mak" CFG="Grass - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Grass - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Grass - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Grass", XPJAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Grass - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GR /GX /O2 /I "..\suma" /I "..\suma\Class" /I "..\Element" /I "..\hierarchy" /I "..\Treeditor\TreeEngine" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_RELEASE" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 dsound.lib dinput8.lib dxerr8.lib d3dx8dt.lib d3d8.lib d3dxof.lib dxguid.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 dsound.lib dinput8.lib dxerr8.lib d3dx8dt.lib d3d8.lib d3dxof.lib dxguid.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "Grass - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GR /ZI /Od /I "..\suma" /I "..\suma\Class" /I "..\Element" /I "..\hierarchy" /I "..\Treeditor\TreeEngine" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 dsound.lib dinput8.lib dxerr8.lib d3dx8dt.lib d3d8.lib d3dxof.lib dxguid.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 dsound.lib dinput8.lib dxerr8.lib d3dx8dt.lib d3d8.lib d3dxof.lib dxguid.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "Grass - Win32 Release"
# Name "Grass - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\ARGB.CPP
# End Source File
# Begin Source File

SOURCE=.\d3dapp.cpp
# End Source File
# Begin Source File

SOURCE=.\d3dutil.cpp
# End Source File
# Begin Source File

SOURCE=.\dxutil.cpp
# End Source File
# Begin Source File

SOURCE=.\Ecosystem.cpp
# End Source File
# Begin Source File

SOURCE=.\Grass.cpp
# End Source File
# Begin Source File

SOURCE=.\Grass.rc
# End Source File
# Begin Source File

SOURCE=.\GrassEngine.cpp
# End Source File
# Begin Source File

SOURCE=.\grassprimitivestream.cpp
# End Source File
# Begin Source File

SOURCE=.\HeightMap.cpp
# End Source File
# Begin Source File

SOURCE=.\Layer.cpp
# End Source File
# Begin Source File

SOURCE=.\LayerForest.cpp
# End Source File
# Begin Source File

SOURCE=.\LayerGround.cpp
# End Source File
# Begin Source File

SOURCE=.\LayerMoss.cpp
# End Source File
# Begin Source File

SOURCE=.\math3dP.cpp
# End Source File
# Begin Source File

SOURCE=.\mathOpt.cpp
# End Source File
# Begin Source File

SOURCE=.\NeighbourLayer.cpp
# End Source File
# Begin Source File

SOURCE=.\randomFlyman.cpp
# End Source File
# Begin Source File

SOURCE=.\Texture.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\ARGB.H
# End Source File
# Begin Source File

SOURCE=.\d3dapp.h
# End Source File
# Begin Source File

SOURCE=.\d3dres.h
# End Source File
# Begin Source File

SOURCE=.\d3dutil.h
# End Source File
# Begin Source File

SOURCE=.\dxutil.h
# End Source File
# Begin Source File

SOURCE=.\Ecosystem.h
# End Source File
# Begin Source File

SOURCE=.\Grass.h
# End Source File
# Begin Source File

SOURCE=.\GrassEngine.h
# End Source File
# Begin Source File

SOURCE=.\grassprimitivestream.h
# End Source File
# Begin Source File

SOURCE=.\HeightMap.h
# End Source File
# Begin Source File

SOURCE=.\Layer.h
# End Source File
# Begin Source File

SOURCE=.\LayerForest.h
# End Source File
# Begin Source File

SOURCE=.\LayerGround.h
# End Source File
# Begin Source File

SOURCE=.\LayerMoss.h
# End Source File
# Begin Source File

SOURCE=.\math3d.hpp
# End Source File
# Begin Source File

SOURCE=.\math3dP.hpp
# End Source File
# Begin Source File

SOURCE=.\mathDefs.hpp
# End Source File
# Begin Source File

SOURCE=.\mathOpt.hpp
# End Source File
# Begin Source File

SOURCE=.\NeighbourLayer.h
# End Source File
# Begin Source File

SOURCE=.\randomFlyman.h
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\Texture.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\DirectX.ico
# End Source File
# Begin Source File

SOURCE=.\LayerGround.psh
# End Source File
# Begin Source File

SOURCE=.\LayerGround.vsh
# End Source File
# Begin Source File

SOURCE=.\LayerMoss.psh
# End Source File
# Begin Source File

SOURCE=.\LayerMoss.vsh
# End Source File
# End Group
# Begin Group "Suma-Class"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\suma\Class\appFrame.cpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\appFrame.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\array.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\bankArray.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\bankInitArray.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\bigArray.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\boolArray.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\cachelist.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\compactBuf.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\consoleBase.h
# End Source File
# Begin Source File

SOURCE=..\suma\Class\debugLog.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\debugNew.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\defNew.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\defNormNew.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\fastAlloc.cpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\fastAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\filenames.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\fltopts.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\hashMap.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\list.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\lLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\lzcompr.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\mathND.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\memAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\memtype.h
# End Source File
# Begin Source File

SOURCE=..\suma\Class\normalNew.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\optDefault.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\optEnable.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\pointers.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\qsort.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\removeLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\rString.cpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\rString.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\scopeLock.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\smallArray.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\staticArray.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\streamArray.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\typeDefines.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\typeGenTemplates.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\typeOpts.hpp
# End Source File
# Begin Source File

SOURCE=..\suma\Class\typeTemplates.hpp
# End Source File
# End Group
# Begin Group "Element"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Element\fileCompress.cpp
# End Source File
# Begin Source File

SOURCE=..\Element\fileCompress.hpp
# End Source File
# Begin Source File

SOURCE=..\Element\fileinfo.h
# End Source File
# Begin Source File

SOURCE=..\Element\fileMapping.cpp
# End Source File
# Begin Source File

SOURCE=..\Element\fileMapping.hpp
# End Source File
# Begin Source File

SOURCE=..\Element\paramFile.cpp
# End Source File
# Begin Source File

SOURCE=..\Element\paramFile.hpp
# End Source File
# Begin Source File

SOURCE=..\Element\perfLog.hpp
# End Source File
# Begin Source File

SOURCE=..\Element\qbstream.cpp
# End Source File
# Begin Source File

SOURCE=..\Element\QBStream.hpp
# End Source File
# Begin Source File

SOURCE=..\Element\qstream.cpp
# End Source File
# Begin Source File

SOURCE=..\Element\QStream.hpp
# End Source File
# Begin Source File

SOURCE=..\Element\serializeBin.cpp
# End Source File
# Begin Source File

SOURCE=..\Element\serializeBin.hpp
# End Source File
# Begin Source File

SOURCE=..\Element\ssCompress.cpp
# End Source File
# Begin Source File

SOURCE=..\Element\win.h
# End Source File
# Begin Source File

SOURCE=..\Element\wpch.hpp
# End Source File
# End Group
# Begin Group "TreeEngineSource"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\auxiliary.cpp
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\auxiliary.h
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\bbox.h
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\branch.cpp
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\branch.h
# End Source File
# Begin Source File

SOURCE=.\branch.psh
# End Source File
# Begin Source File

SOURCE=.\branch.vsh
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\branchBunch.cpp
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\branchBunch.h
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\branchLeaf.cpp
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\branchLeaf.h
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\branchSimpleGutter.cpp
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\branchSimpleGutter.h
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\branchStem.cpp
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\branchStem.h
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\branchTree.cpp
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\branchTree.h
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\mytree.cpp
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\mytree.h
# End Source File
# Begin Source File

SOURCE=.\normal.vsh
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\Polyplane.cpp
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\Polyplane.h
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\PolyplaneBlock.cpp
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\PolyplaneBlock.h
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\PolyplaneBunch.cpp
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\PolyplaneBunch.h
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\primitivestream.cpp
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\primitivestream.h
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\search.hpp
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\triplane.cpp
# End Source File
# Begin Source File

SOURCE=..\Treeditor\TreeEngine\triplane.h
# End Source File
# Begin Source File

SOURCE=.\triplane.psh
# End Source File
# Begin Source File

SOURCE=.\triplane.vsh
# End Source File
# End Group
# Begin Source File

SOURCE=.\todo.txt
# End Source File
# End Target
# End Project
