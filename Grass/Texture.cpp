#include "Texture.h"

CTexture::CTexture() {
  _pData = NULL;
}

CTexture::~CTexture() {
  delete _pData;
}

void CTexture::Init(IDirect3DDevice8 *pD3DDevice, const char *TextureName) {
  ComRef<IDirect3DTexture8> pTexture;
  D3DXCreateTextureFromFile(pD3DDevice, TextureName, pTexture.Init());
  D3DLOCKED_RECT LR;
  pTexture->LockRect(0, &LR, NULL, 0);
  D3DSURFACE_DESC SD;
  pTexture->GetLevelDesc(0, &SD);
  _Width = SD.Width;
  _Height = SD.Height;
  _Resolution = 1024;
  _pData = new int[_Width * _Height];
  memcpy(_pData, LR.pBits, _Width * _Height * sizeof(int));
  pTexture->UnlockRect(0);
}

void CTexture::Init(int Width, int Height, int Resolution) {
  _Width = Width;
  _Height = Height;
  _Resolution = Resolution;
  _pData = new int[_Width * _Height];
}

void CTexture::SetPixel(int X, int Y, int Value) {
  X = X%_Width; if (X < 0) X += _Width;
  Y = Y%_Height; if (Y < 0) Y += _Height;
  _pData[Y * _Width + X] = Value;
}

int CTexture::GetPixel(int X, int Y) {
  X = X%_Width; if (X < 0) X += _Width;
  Y = Y%_Height; if (Y < 0) Y += _Height;
  return _pData[Y * _Width + X];
}

CARGB CTexture::GetInterpolatedPixel(float X, float Y) {

  //  Neni to osetrene pro zaporne hodnoty X a Y

  float TexX = (X * _Width + 0.5f);
  float TexY = (Y * _Height + 0.5f);

  int IntTexX = (int)TexX;
  int IntTexY = (int)TexY;

  float FractTexX = TexX - (float)IntTexX;
  float FractTexY = TexY - (float)IntTexY;

  CARGB C00(GetPixel(IntTexX - 1, IntTexY - 1));
  CARGB C10(GetPixel(IntTexX    , IntTexY - 1));
  CARGB C01(GetPixel(IntTexX - 1, IntTexY    ));
  CARGB C11(GetPixel(IntTexX    , IntTexY    ));
  float Coef0001 = 1.0f - FractTexX;
  float Coef1011 = FractTexX;
  float Coef0010 = 1.0f - FractTexY;
  float Coef0111 = FractTexY;
  float Coef00 = Coef0001 * Coef0010;
  float Coef01 = Coef0001 * Coef0111;
  float Coef10 = Coef1011 * Coef0010;
  float Coef11 = Coef1011 * Coef0111;
  return C00 * Coef00 + C10 * Coef10 + C01 * Coef01 + C11 * Coef11;

}

void CTexture::Normalize() {
  for (int i = 0; i < _Width * _Height; i++) {
    int Color = _pData[i];
    Vector3 V(
      ((float)((Color >> 16) & 0xFF)) * (2.0f / 255.0f) - 1,
      ((float)((Color >> 8) & 0xFF)) * (2.0f / 255.0f) - 1,
      ((float)((Color >> 0) & 0xFF)) * (2.0f / 255.0f) - 1);
    V[2] = fabs(V.Z()) + 10.5f;
    V.Normalize();
    _pData[i] = 
      (((int)((V.X() + 1.0f) * 255.0f)) << 16) |
      (((int)((V.Y() + 1.0f) * 255.0f)) << 8) |
      (((int)((V.Z() + 1.0f) * 255.0f)) << 0);
  }
}

void CTexture::Normalize2() {
  for (int j = 0; j < _Height - 1; j++) {
    for (int i = 0; i < _Width - 1; i++) {
      int Color;
      Color = _pData[j * _Width + i];
      float AvgColor00 = 
      ((((float)((Color >> 16) & 0xFF)) +
        ((float)((Color >> 8) & 0xFF)) +
        ((float)((Color >> 0) & 0xFF))) / 3.0f) / 255.0f;

      Color = _pData[j * _Width + (i + 1)];
      float AvgColor10 = 
      ((((float)((Color >> 16) & 0xFF)) +
        ((float)((Color >> 8) & 0xFF)) +
        ((float)((Color >> 0) & 0xFF))) / 3.0f) / 255.0f;

      Color = _pData[(j + 1) * _Width + i];
      float AvgColor01 = 
      ((((float)((Color >> 16) & 0xFF)) +
        ((float)((Color >> 8) & 0xFF)) +
        ((float)((Color >> 0) & 0xFF))) / 3.0f) / 255.0f;

      Vector3 P00(0, 0, AvgColor00 * PIXEL_MAXHEIGHT);
      Vector3 P10(PIXEL_DISTANCE, 0, AvgColor10 * PIXEL_MAXHEIGHT);
      Vector3 P01(0, PIXEL_DISTANCE, AvgColor01 * PIXEL_MAXHEIGHT);
      Vector3 V0010 = (P10 - P00).Normalized();
      Vector3 V0001 = (P01 - P00).Normalized();
      Vector3 Normal = V0010.CrossProduct(V0001);

      _pData[j * _Width + i] = 
        (((int)((Normal.X() + 1.0f) * 255.0f*0.5f)) << 16) |
        (((int)((Normal.Y() + 1.0f) * 255.0f*0.5f)) << 8) |
        (((int)((Normal.Z() + 1.0f) * 255.0f*0.5f)) << 0);

    }
  }
}

void CTexture::CreateTexture(IDirect3DDevice8 *pD3DDevice, ComRef<IDirect3DTexture8> &pTexture) {
  pD3DDevice->CreateTexture(_Width, _Height, 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, pTexture.Init());
  // Lock and fill data
  D3DLOCKED_RECT LR;
  pTexture->LockRect(0, &LR, NULL, 0);
  memcpy(LR.pBits, _pData, _Width * _Height * sizeof(int));
  pTexture->UnlockRect(0);
}

void CTexture::PrepareTexture(IDirect3DDevice8 *pD3DDevice, ComRef<IDirect3DTexture8> &pTexture) {
  // Lock and fill data
  D3DLOCKED_RECT LR;
  pTexture->LockRect(0, &LR, NULL, 0);
  memcpy(LR.pBits, _pData, _Width * _Height * sizeof(int));
  pTexture->UnlockRect(0);
}

//---------------------------------------------------------------------------

int CFlyTexture::_LevelIndex[] = {
  /*   1*/ 0,
  /*   2*/ 1,
  /*   4*/ 5,
  /*   8*/ 21,
  /*  16*/ 85,
  /*  32*/ 341,
  /*  64*/ 1365,
  /* 128*/ 5461,
  /* 256*/ 21845,
  /* 512*/ 87381,
  /*1024*/ 349525,
  /*2048*/ 1398101,
  /*4096*/ 5592405,
  /*8192*/ 22369621
};

int CFlyTexture::_LevelSize[] = {
  /*   1*/ 1,
  /*   2*/ 2,
  /*   4*/ 4,
  /*   8*/ 8,
  /*  16*/ 16,
  /*  32*/ 32,
  /*  64*/ 64,
  /* 128*/ 128,
  /* 256*/ 256,
  /* 512*/ 512,
  /*1024*/ 1024,
  /*2048*/ 2048,
  /*4096*/ 4096
};

char *CFlyTexture::_LevelName[] = {
  /*   1*/ "0001",
  /*   2*/ "0002",
  /*   4*/ "0004",
  /*   8*/ "0008",
  /*  16*/ "0016",
  /*  32*/ "0032",
  /*  64*/ "0064",
  /* 128*/ "0128",
  /* 256*/ "0256",
  /* 512*/ "0512",
  /*1024*/ "1024",
  /*2048*/ "2048",
  /*4096*/ "4096"
};


int CFlyTexture::GetLevelPixel(int Level, float X, float Y) {
  int iLevelSize = _LevelSize[Level];
  float fLevelSizeMO = iLevelSize - 1.0f;
  int iX = ((int)(X * fLevelSizeMO + 0.5f)) & (_LevelSize[Level] - 1);
  int iY = ((int)(Y * fLevelSizeMO + 0.5f)) & (_LevelSize[Level] - 1);
  return _pData[_LevelIndex[Level] + iY * iLevelSize + iX];
}

CFlyTexture::CFlyTexture() {
  _pData = NULL;
}

CFlyTexture::~CFlyTexture() {
  delete _pData;
}

void CFlyTexture::Init(IDirect3DDevice8 *pD3DDevice, const char *TextureName, EFlyTextureSize Size, int Log2Resolution) {
  Init(Size, Log2Resolution);
  ComRef<IDirect3DTexture8> pTexture;
  for (int i = 0; i <= Size ; i++) {
    char TempString[1024];
    sprintf(TempString, "%s%s.tga", TextureName, _LevelName[i]);
    D3DXCreateTextureFromFile(pD3DDevice, TempString, pTexture.Init());
    D3DLOCKED_RECT LR;
    pTexture->LockRect(0, &LR, NULL, 0);
    memcpy(&_pData[_LevelIndex[i]], LR.pBits, _LevelSize[i] * _LevelSize[i] * sizeof(int));
    pTexture->UnlockRect(0);
  }
}

void CFlyTexture::Init(EFlyTextureSize Size, int Log2Resolution) {
  _LastLevel = Size;
  _Log2Resolution = Log2Resolution;
  _pData = new int[_LevelIndex[_LastLevel + 1]];
}

int CFlyTexture::GetPixel(int Log2Resolution, float X, float Y) {
  return GetLevelPixel(_LastLevel - _Log2Resolution + Log2Resolution, X, Y);
}

CARGB CFlyTexture::GetARGBPixel(int Log2Resolution, float X, float Y) {
  return CARGB(GetPixel(Log2Resolution, X, Y));
}
