#ifndef _ECOSYSTEM_H_
#define _ECOSYSTEM_H_

#include <d3d8.h>
#include <d3dx8.h>
#include "debugLog.hpp"
#include "math3d.hpp"
#include "pointers.hpp"
#include "grassprimitivestream.h"
#include "Texture.h"
#include "NeighbourLayer.h"
#include "HeightMap.h"

#define D3DFVF_MYVERTEX (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_DIFFUSE|D3DFVF_TEX3|D3DFVF_TEXCOORDSIZE2(0)|D3DFVF_TEXCOORDSIZE2(1)|D3DFVF_TEXCOORDSIZE2(2))

// Grass bunch
struct SMyVertex {
  // Vertex Position.
  Vector3 Position;
  // Vertex Normal.
  Vector3 Normal;
  // Diffuse color.
  D3DCOLOR Diffuse;
  // Coordinates in the normal map and at the same time coordinates in the RGB map.
  float U1, V1;
  float U2, V2;
  float U3, V3;
};

DWORD dwMyVertexDecl[];

#define LOCALNEIGHBOURHOOD_SIZE 16

class CEcosystem : public RefCount {
private:
  //! 3D device.
  ComRef<IDirect3DDevice8> _pD3DDevice;
  //! Stream of grass bunch.
  Ref<CGrassPrimitiveStream> _pPS;
  Ref<CGrassPrimitiveStream> _pPSLayerClose;
  //! Rendered layers in the neighborhood.
  CNeighbourLayer _NeighbourLayer0;
  CNeighbourLayer _NeighbourLayer1;
  CNeighbourLayer _NeighbourLayer2;
  //! Ground describing layer.
  CLayerGround _LayerGround;
  CLayerMoss _LayerMoss;
  CLayerForest _LayerForest;
  //! Height data from file.
  CHeightMap _HeightMap;
public:
  //! Eye position.
  D3DXVECTOR3 _EyePosition;
  //! Eye rotation around X axis.
  float _EyeRotX;
  //! Eye rotation around Y axis.
  float _EyeRotY;
  //! Heigh of the eye.
  float _EyeHeight;
  //! Sun position (rotation around Z axis).
  float _SunAngle;

  void Init(IDirect3DDevice8 *pD3DDevice, const char *GLBaseAName);
  void Draw(D3DXMATRIX &MatView, D3DXMATRIX &MatProj, int Seed, float Detail, Vector3Par Position);
  void DrawLand(D3DXMATRIX &MatProj);
};


#endif