#ifndef _LAYERFOREST_H_
#define _LAYERFOREST_H_

#include "Layer.h"
#include "mytree.h"

class CLayerForest : public CLayer {
  CTreeEngine TreeEngine;
public:
  void Init(IDirect3DDevice8 *pD3DDevice);
  virtual void AddLayer2(int X, int Y, int HalfSize, IDirect3DTexture8 *pTexture, D3DXMATRIX &MatView, D3DXMATRIX &MatProj);
  virtual void AddLayerToScreen(
    CGrassPrimitiveStream *PSLayer,
    int X,
    int Y,
    D3DXVECTOR3 &LightVector,
    D3DXMATRIX &MatView,
    D3DXMATRIX &MatProj);
  virtual void DrawLayer(D3DXMATRIX &MatView, D3DXMATRIX &MatProj, D3DXVECTOR3 &LightVector, CHeightMap &HeightMap, Vector3Par Pos);
};

#endif