#ifndef _NEIGHBOURLAYER_H_
#define _NEIGHBOURLAYER_H_

#include <d3d8.h>
#include <d3dx8.h>
#include "debugLog.hpp"
#include "math3d.hpp"
#include "pointers.hpp"
#include "grassprimitivestream.h"
#include "Texture.h"
#include "LayerGround.h"
#include "LayerMoss.h"
#include "LayerForest.h"

#define NLTEXTURESIZE 1024

class CNeighbourLayer {
private:
  //! 3D device.
  ComRef<IDirect3DDevice8> _pD3DDevice;
  //! Rendered d3d texture.
  ComRef<IDirect3DTexture8> _pTLayer;
  ComRef<IDirect3DSurface8> _pSLayer;


  D3DXMATRIX _MatProj;
  D3DXMATRIX _MatView;


  //! Old staff
  ComRef<IDirect3DSurface8> _pOldRenderTarget;
  ComRef<IDirect3DSurface8> _pOldZStencilSurface;
  //! X position of the centre of the NeighbourLayer.
  int _PosX;
  //! Y position of the centre of the NeighbourLayer.
  int _PosY;
  //! Size of the NeighbourLayer in meters.
  int _HalfSize;
public:
  void Init(IDirect3DDevice8 *pD3DDevice, int HalfSize);
  void UpdatePos(int PosX, int PosY);
  void BeginRendering();
  void EndRendering();
  void AddLayer2(CLayer *pLayer);
  void AddLayerClear(CLayer *pLayer);
  IDirect3DTexture8 *GetTexture();
  //! Returns coordinates of the vertex at coordinates X and Y in meters.
  /*!
    X and Y is from range <0, HAIGHTMAP_SIZE>
  */
  void GetTextureCoord(int X, int Y, float &u, float &v);
  int NeedUpdate(float EyeX, float EyeY);
};

#endif