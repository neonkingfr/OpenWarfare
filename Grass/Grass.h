//-----------------------------------------------------------------------------
// File: Grass.h
//
// Desc: Header file Grass sample app
//-----------------------------------------------------------------------------
#ifndef AFX_GRASS_H__78FF2110_0F89_40A4_86C7_CB075BF965CD__INCLUDED_
#define AFX_GRASS_H__78FF2110_0F89_40A4_86C7_CB075BF965CD__INCLUDED_

#include "GrassEngine.h"
#include "Ecosystem.h"

//-----------------------------------------------------------------------------
// Defines, and constants
//-----------------------------------------------------------------------------
// TODO: change "DirectX AppWizard Apps" to your name or the company name
#define DXAPP_KEY        TEXT("Software\\DirectX AppWizard Apps\\Grass")

// Struct to store the current input state
struct UserInput
{
    BYTE diks[256];   // DirectInput keyboard state buffer 

    BOOL bRotateUp;
    BOOL bRotateDown;
    BOOL bRotateLeft;
    BOOL bRotateRight;
    BOOL bMoveForward;
    BOOL bMoveBackward;
    BOOL bMoveUp;
    BOOL bMoveDown;
    BOOL bSunUp;
    BOOL bSunDown;
};

//-----------------------------------------------------------------------------
// Name: class CMyD3DApplication
// Desc: Application class. The base class (CD3DApplication) provides the 
//       generic functionality needed in all Direct3D samples. CMyD3DApplication 
//       adds functionality specific to this sample program.
//-----------------------------------------------------------------------------
class CMyD3DApplication : public CD3DApplication
{
    BOOL                    m_bLoadingApp;          // TRUE, if the app is loading
    ID3DXFont*              m_pD3DXFont;            // D3DX font    

    LPDIRECTINPUT8          m_pDI;                  // DirectInput object
    LPDIRECTINPUTDEVICE8    m_pKeyboard;            // DirectInput keyboard device
    UserInput               m_UserInput;            // Struct for storing user input 

    D3DXVECTOR3             m_CameraPosition;
    FLOAT                   m_CameraRotationX;
    FLOAT                   m_CameraRotationY;
    D3DXMATRIX              m_matView;
    D3DXMATRIX              m_matProjection;
    Ref<CGrassEngine>       m_pGrassEngine;
    Ref<CEcosystem>         m_Ecosystem;

protected:
    HRESULT OneTimeSceneInit();
    HRESULT InitDeviceObjects();
    HRESULT RestoreDeviceObjects();
    HRESULT InvalidateDeviceObjects();
    HRESULT DeleteDeviceObjects();
    HRESULT Render();
    HRESULT FrameMove();
    HRESULT FinalCleanup();
    HRESULT ConfirmDevice( D3DCAPS8*, DWORD, D3DFORMAT );

    HRESULT RenderText();

    HRESULT InitInput( HWND hWnd );
    void    UpdateInput( UserInput* pUserInput );
    void    CleanupDirectInput();

    VOID    ReadSettings();
    VOID    WriteSettings();

public:
    LRESULT MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );
    CMyD3DApplication();
};


#endif // !defined(AFX_GRASS_H__78FF2110_0F89_40A4_86C7_CB075BF965CD__INCLUDED_)
