#ifndef _LAYERGROUND_H_
#define _LAYERGROUND_H_

#include "Layer.h"

class CLayerGround : public CLayer {
private:
  //! 3D device.
  ComRef<IDirect3DDevice8> _pD3DDevice;
  ComRef<IDirect3DVertexBuffer8> _pVB;


  CTexture _TEnv;
  //CTexture _TA;
  //CTexture _TB;
  //CTexture _TC;
  CFlyTexture _TA;
  CFlyTexture _TB;
  CFlyTexture _TC;
  ComRef<IDirect3DTexture8> _pTBaseA;
  ComRef<IDirect3DTexture8> _pTBaseB;
  ComRef<IDirect3DTexture8> _pTBaseC;
  ComRef<IDirect3DTexture8> _pTBaseAN;
  ComRef<IDirect3DTexture8> _pTBaseBN;
  ComRef<IDirect3DTexture8> _pTBaseCN;
  
  ComRef<IDirect3DTexture8> _pTAlfaA;
  ComRef<IDirect3DTexture8> _pTAlfaB;
  ComRef<IDirect3DTexture8> _pTAlfaC;

  CTexture _Texture;

  int _LocalHalfSize;


  DWORD _hVS;
  DWORD _hPS;

public:
  void Init(IDirect3DDevice8 *pD3DDevice);
  virtual void AddLayer2(int X, int Y, int HalfSize, IDirect3DTexture8 *pTexture, D3DXMATRIX &MatView, D3DXMATRIX &MatProj);
  virtual void AddLayerToScreen(
    CGrassPrimitiveStream *PSLayer,
    int X,
    int Y,
    D3DXVECTOR3 &LightVector,
    D3DXMATRIX &MatView,
    D3DXMATRIX &MatProj);
  virtual void DrawLayer(D3DXMATRIX &MatView, D3DXMATRIX &MatProj, D3DXVECTOR3 &LightVector, CHeightMap &HeightMap, Vector3Par Pos);
};

#endif