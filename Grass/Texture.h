#ifndef _TEXTURE_H
#define _TEXTURE_H

#include <d3d8.h>
#include <d3dx8.h>
#include "debugLog.hpp"
#include "math3d.hpp"
#include "pointers.hpp"
#include "ARGB.h"

#define PIXEL_DISTANCE 0.002f
#define PIXEL_MAXHEIGHT 0.01f

class CTexture {
public:
  //! Data of the texture - row by row.
  int *_pData;
  //! Width in texels.
  int _Width;
  //! Height in texels.
  int _Height;
  //! Resolution in Dots Per Meter.
  int _Resolution;
  //! Default constructor.
  CTexture();
  //! Destructor.
  ~CTexture();
  //! Initialization - loading from texture.
  void Init(IDirect3DDevice8 *pD3DDevice, const char *TextureName);
  //! Initialization - creating according to size.
  void Init(int Width, int Height, int Resolution);
  //! Setting the pixel.
  void SetPixel(int X, int Y, int Value);
  //! Getting the pixel.
  int GetPixel(int X, int Y);
  //! Getting the interpolated pixel.
  /*!
    X and Y are values from interval <0,1>
  */
  CARGB GetInterpolatedPixel(float X, float Y);
  //! Normalizes the texture.
  void Normalize();
  void Normalize2();
  //! Creates D3D texture according to this texture.
  void CreateTexture(IDirect3DDevice8 *pD3DDevice, ComRef<IDirect3DTexture8> &pTexture);
  void PrepareTexture(IDirect3DDevice8 *pD3DDevice, ComRef<IDirect3DTexture8> &pTexture);
};

enum EFlyTextureSize {
  Size1x1       = 0,
  Size2x2       = 1,
  Size4x4       = 2,
  Size8x8       = 3,
  Size16x16     = 4,
  Size32x32     = 5,
  Size64x64     = 6,
  Size128x128   = 7,
  Size256x256   = 8,
  Size512x512   = 9,
  Size1024x1024 = 10,
  Size2048x2048 = 11,
  Size4096x4096 = 12
};

//! Texture with mip levels.
/*!
  This structure is capable to load textures with size 2^n only.
*/
class CFlyTexture {
private:
  //! Index of level to _pData array.
  static int _LevelIndex[];
  //! Size of level.
  static int _LevelSize[];
  //! Names for each level.
  static char *_LevelName[];
  //! Data of the texture - row by row, all mipmap levels starting with the smallest one.
  int *_pData;
  //! Index of last level of mipmaps.
  int _LastLevel;
  //! Resolution in Dots Per Meter.
  int _Log2Resolution;
  //! Getting the pixel.
  int GetLevelPixel(int Level, float X, float Y);
public:
  //! Default constructor.
  CFlyTexture();
  //! Destructor.
  ~CFlyTexture();
  //! Initialization - loading from texture.
  void Init(IDirect3DDevice8 *pD3DDevice, const char *TextureName, EFlyTextureSize Size, int Log2Resolution);
  //! Initialization of texture.
  void Init(EFlyTextureSize Size, int Log2Resolution);
  //! Getting the pixel.
  int GetPixel(int Log2Resolution, float X, float Y);
  //! Getting the RGB pixel.
  CARGB GetARGBPixel(int Log2Resolution, float X, float Y);
};

#endif