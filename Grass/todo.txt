- Zamyslet se nad hybanim triplanu ve vetru (VS)...

- Precist si sireni signalu od Prinskinievicze

- Barvy do L-systemu - variace, podzimni barvy, ztmavovani smerem do centra kmene

- Koeficient pro roztazeni textury (abychom byli schopni otexturovat travu)

- Upravit triplanes - vubec navrhnout jejich strukturu

- Materialy - vlastnici sekci?

- Ambientni svetlo, jeho barva, barva smeroveho svetla
    - Hotovy jednoduchy jednopruchodovy model.
    - Do budoucna 2 pruchody (dominantni svetlo a ostatni svetla) a nebo najit zpusob jak predat dalsi vektor do PS
    - Vubec by bylo dobre stanovit svetelny a materialovy model

- Otaceni textur

- Okraje textur (souvisi s otacenim)

- Shadery pro kombinaci neighbour vrstev a pro nasvetlovani
    - prozkoumat moznost pouziti Cg - nejlepe to v Cg implementovat
    - myslet na rozsireni o normalove mapy

- Vice vrstev pro ekosystem (spoluprace s L-systemem)
    - Lesy v dalce budou vrstva, jedince budu vykreslovat pouze na kraji lesa
      a v lokalnich vyskovych maximech (oba udaje se zaznamenaji v ekosystemu
      ve forme koeficientu kterym vynasobim Detail). Ve vetsi vzdalenosti
      uz pouze vrstva.

- Simulace ekosystemu
    - vybrat si nastroj (bud napsat vlastni jednoduchy a nebo neco jako "powersim")
    - najit vhodny ekosystemovy model

- Finalizace:
    - napsat templaty jako datove struktury pro vrstvy (Quadtree, Hasovani, cos transformace)
    - psat jednotlive vrstvy
    - presun do engine

- Podivat se na spravce textur a na shape do Engine a porozumet mu kompletne