#include "randomFlyman.h"
#include "GrassEngine.h"

#define GRASSBUNCH_SIZE 0.5f

void CGrassEngine::GenerateVerticalGrassBunch(CGrassPrimitiveStream &PSVertical, Matrix4Par Origin) {

  WORD Index;
  SGrassBunchVertex GBV;

  GBV.Position = Origin.Position() + Origin.DirectionAside() * GRASSBUNCH_SIZE + Origin.DirectionUp() * 0.5f;
  GBV.Normal = Origin.Direction();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 1;
  GBV.V1 = 0;
  Index = PSVertical.AddVertex(&GBV);
  PSVertical.AddIndex(10, Index);

  GBV.Position = Origin.Position() + Origin.DirectionAside() * GRASSBUNCH_SIZE;
  GBV.Normal = Origin.Direction();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 1;
  GBV.V1 = 1;
  Index = PSVertical.AddVertex(&GBV);
  PSVertical.AddIndex(10, Index);

  GBV.Position = Origin.Position() - Origin.DirectionAside() * GRASSBUNCH_SIZE;
  GBV.Normal = Origin.Direction();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 0;
  GBV.V1 = 1;
  Index = PSVertical.AddVertex(&GBV);
  PSVertical.AddIndex(10, Index);

  


  GBV.Position = Origin.Position() + Origin.DirectionAside() * GRASSBUNCH_SIZE + Origin.DirectionUp() * 0.5f;
  GBV.Normal = Origin.Direction();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 1;
  GBV.V1 = 0;
  Index = PSVertical.AddVertex(&GBV);
  PSVertical.AddIndex(10, Index);

  GBV.Position = Origin.Position() - Origin.DirectionAside() * GRASSBUNCH_SIZE;
  GBV.Normal = Origin.Direction();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 0;
  GBV.V1 = 1;
  Index = PSVertical.AddVertex(&GBV);
  PSVertical.AddIndex(10, Index);

  GBV.Position = Origin.Position() - Origin.DirectionAside() * GRASSBUNCH_SIZE + Origin.DirectionUp() * 0.5f;
  GBV.Normal = Origin.Direction();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 0;
  GBV.V1 = 0;
  Index = PSVertical.AddVertex(&GBV);
  PSVertical.AddIndex(10, Index);





  GBV.Position = Origin.Position() + Origin.DirectionAside() * GRASSBUNCH_SIZE + Origin.DirectionUp() * 0.5f;
  GBV.Normal = -Origin.Direction();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 1;
  GBV.V1 = 0;
  Index = PSVertical.AddVertex(&GBV);
  PSVertical.AddIndex(10, Index);

  GBV.Position = Origin.Position() - Origin.DirectionAside() * GRASSBUNCH_SIZE;
  GBV.Normal = -Origin.Direction();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 0;
  GBV.V1 = 1;
  Index = PSVertical.AddVertex(&GBV);
  PSVertical.AddIndex(10, Index);

  GBV.Position = Origin.Position() + Origin.DirectionAside() * GRASSBUNCH_SIZE;
  GBV.Normal = -Origin.Direction();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 1;
  GBV.V1 = 1;
  Index = PSVertical.AddVertex(&GBV);
  PSVertical.AddIndex(10, Index);


  GBV.Position = Origin.Position() + Origin.DirectionAside() * GRASSBUNCH_SIZE + Origin.DirectionUp() * 0.5f;
  GBV.Normal = -Origin.Direction();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 1;
  GBV.V1 = 0;
  Index = PSVertical.AddVertex(&GBV);
  PSVertical.AddIndex(10, Index);

  GBV.Position = Origin.Position() - Origin.DirectionAside() * GRASSBUNCH_SIZE + Origin.DirectionUp() * 0.5f;
  GBV.Normal = -Origin.Direction();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 0;
  GBV.V1 = 0;
  Index = PSVertical.AddVertex(&GBV);
  PSVertical.AddIndex(10, Index);

  GBV.Position = Origin.Position() - Origin.DirectionAside() * GRASSBUNCH_SIZE;
  GBV.Normal = -Origin.Direction();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 0;
  GBV.V1 = 1;
  Index = PSVertical.AddVertex(&GBV);
  PSVertical.AddIndex(10, Index);


}

void CGrassEngine::GenerateLayer(CGrassPrimitiveStream &PS, int Stage, IDirect3DTexture8 *pTexture, Matrix4Par Origin, float Height) {

  WORD Index;
  SGrassBunchVertex GBV;

  PS.RegisterTextures(Stage, pTexture, NULL, NULL);

  GBV.Position = Origin.Position() + (Origin.DirectionAside() + Origin.Direction()) * 0.5f * GRASSPATCH_SIZE + Origin.DirectionUp() * Height;
  GBV.Normal = Origin.DirectionUp();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 1;
  GBV.V1 = 0;
  Index = PS.AddVertex(&GBV);
  PS.AddIndex(Stage, Index);

  GBV.Position = Origin.Position() + (Origin.DirectionAside() - Origin.Direction()) * 0.5f * GRASSPATCH_SIZE + Origin.DirectionUp() * Height;
  GBV.Normal = Origin.DirectionUp();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 1;
  GBV.V1 = 1;
  Index = PS.AddVertex(&GBV);
  PS.AddIndex(Stage, Index);

  GBV.Position = Origin.Position() + (-Origin.DirectionAside() - Origin.Direction()) * 0.5f * GRASSPATCH_SIZE + Origin.DirectionUp() * Height;
  GBV.Normal = Origin.DirectionUp();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 0;
  GBV.V1 = 1;
  Index = PS.AddVertex(&GBV);
  PS.AddIndex(Stage, Index);




  GBV.Position = Origin.Position() + (Origin.DirectionAside() + Origin.Direction()) * 0.5f * GRASSPATCH_SIZE + Origin.DirectionUp() * Height;
  GBV.Normal = Origin.DirectionUp();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 1;
  GBV.V1 = 0;
  Index = PS.AddVertex(&GBV);
  PS.AddIndex(Stage, Index);

  GBV.Position = Origin.Position() + (-Origin.DirectionAside() - Origin.Direction()) * 0.5f * GRASSPATCH_SIZE + Origin.DirectionUp() * Height;
  GBV.Normal = Origin.DirectionUp();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 0;
  GBV.V1 = 1;
  Index = PS.AddVertex(&GBV);
  PS.AddIndex(Stage, Index);

  GBV.Position = Origin.Position() + (-Origin.DirectionAside() + Origin.Direction()) * 0.5f * GRASSPATCH_SIZE + Origin.DirectionUp() * Height;
  GBV.Normal = Origin.DirectionUp();
  GBV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  GBV.U1 = 0;
  GBV.V1 = 0;
  Index = PS.AddVertex(&GBV);
  PS.AddIndex(Stage, Index);


}

void CGrassEngine::GenerateVerticalGrass(CGrassPrimitiveStream &PSVertical) {
  
  CRandomFlyman RF;

  // Register texture
  PSVertical.RegisterTextures(10, _pTVertical, NULL, NULL);

  
  for (int i = 0;  i < 500; i++) {
    Vector3 P((RF.Random(i * 4 + 0) - 0.5f) * GRASSPATCH_SIZE, 0, (RF.Random(i * 4 + 1) - 0.5f) * GRASSPATCH_SIZE);
    Vector3 V(RF.Random(i * 4 + 2) - 0.5f, 0, RF.Random(i * 4 + 3) - 0.5f);
    Matrix4 Origin;
    Origin.SetPosition(P);
    Origin.SetUpAndAside(Vector3(0, 1, 0), V);
    GenerateVerticalGrassBunch(PSVertical, Origin);
  }
  

/*
  for (float i = 0.0f;  i < 1.0f; i += 0.1f) {
    for (float j = 0.0f; j < 1.0f; j += 0.1f) {
      Vector3 P;
      Vector3 V;
      Matrix4 Origin;

      P = Vector3((i - 0.5f) * GRASSPATCH_SIZE, 0, (j - 0.5f) * GRASSPATCH_SIZE);
      V = Vector3(0, 0, 1);
      Origin.SetPosition(P);
      Origin.SetUpAndAside(Vector3(0, 1, 0), V);
      GenerateVerticalGrassBunch(PSVertical, Origin);

      V = Vector3(1, 0, 0);
      Origin.SetUpAndAside(Vector3(0, 1, 0), V);
      GenerateVerticalGrassBunch(PSVertical, Origin);
    }
  }
*/

}

void CGrassEngine::Init(IDirect3DDevice8 *pD3DDevice) {
  // Save device
  _pD3DDevice = pD3DDevice; pD3DDevice->AddRef();

  // Create stream for the bunch of grass
  _pPSGrassBunch = new CGrassPrimitiveStream(pD3DDevice);
  _pPSGrassBunch->Init(sizeof(SGrassBunchVertex), D3DFVF_GRASSBUNCHVERTEX);

  // Load required textures
  D3DXCreateTextureFromFile(pD3DDevice, "vertical1.tga", _pTVertical.Init());
  //D3DXCreateTextureFromFile(pD3DDevice, "Layer0.tga", _pTLayer0.Init());
  D3DXCreateTextureFromFile(pD3DDevice, "trava1.tga", _pTLayer0.Init());
  D3DXCreateTextureFromFile(pD3DDevice, "Layer1.tga", _pTLayer1.Init());
  D3DXCreateTextureFromFile(pD3DDevice, "Layer2.tga", _pTLayer2.Init());
}

void CGrassEngine::DrawGrass(
                      D3DXMATRIX &MatView,
                      D3DXMATRIX &MatProj,
                      int Seed,
                      float Detail,
                      Vector3Par Position) {

  // Clearing
  _pPSGrassBunch->Clear();

  // Generate layers
  Matrix4 Matrix;
  Matrix.SetIdentity();
  GenerateLayer(*_pPSGrassBunch, 1, _pTLayer0, Matrix, 0.0f);
/*
  GenerateLayer(*_pPSGrassBunch, 2, _pTLayer1, Matrix, 0.1f);
  GenerateLayer(*_pPSGrassBunch, 2, _pTLayer1, Matrix, 0.11f);
  GenerateLayer(*_pPSGrassBunch, 2, _pTLayer1, Matrix, 0.12f);
  GenerateLayer(*_pPSGrassBunch, 2, _pTLayer1, Matrix, 0.13f);
*/

  //GenerateLayer(*_pPSGrassBunch, 3, _pTLayer2, Matrix, 0.4f);



  // Generate vertical grass
  ///GenerateVerticalGrass(*_pPSGrassBunch);


  // Prepare VB and IB of Branches and triplanes
  _pPSGrassBunch->Prepare();

  // Drawing
	_pD3DDevice->BeginScene();

  // Branch rendering
  _pD3DDevice->SetVertexShader(D3DFVF_GRASSBUNCHVERTEX);
  _pD3DDevice->SetPixelShader(0);
  _pD3DDevice->SetTransform(D3DTS_PROJECTION, &MatProj);
  _pD3DDevice->SetTransform(D3DTS_VIEW, &MatView);
  _pPSGrassBunch->Draw();

  // Finalizing
  _pD3DDevice->EndScene();

  
}
