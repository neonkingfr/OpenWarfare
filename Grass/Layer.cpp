#include "Layer.h"

DWORD dwLayerCloseDecl[] =
{
    D3DVSD_STREAM(0),
    D3DVSD_REG(D3DVSDE_POSITION,  D3DVSDT_FLOAT3),      // position
    D3DVSD_REG(D3DVSDE_NORMAL,    D3DVSDT_FLOAT3),      // normal
    D3DVSD_REG(D3DVSDE_DIFFUSE,   D3DVSDT_D3DCOLOR),    // diffuse
    D3DVSD_REG(D3DVSDE_TEXCOORD0, D3DVSDT_FLOAT2),      // base texture coordiantes
    D3DVSD_REG(D3DVSDE_TEXCOORD1, D3DVSDT_FLOAT3),      // S coordinate
    D3DVSD_REG(D3DVSDE_TEXCOORD2, D3DVSDT_FLOAT3),      // T coordinate
    D3DVSD_END()
};
