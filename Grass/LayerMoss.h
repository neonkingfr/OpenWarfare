#ifndef _LAYERMOSS_H_
#define _LAYERMOSS_H_

#include "Layer.h"

class CLayerMoss : public CLayer {
private:
  ComRef<IDirect3DDevice8> _pD3DDevice;
  CTexture _TEnv;
  ComRef<IDirect3DTexture8> _pTBase;
  ComRef<IDirect3DTexture8> _pTBaseN;
  ComRef<IDirect3DTexture8> _pTAlfa;
  CTexture _Texture;
  int _LocalHalfSize;
  DWORD _hVS;
  DWORD _hPS;
public:
  void Init(IDirect3DDevice8 *pD3DDevice);
  virtual void AddLayer2(int X, int Y, int HalfSize, IDirect3DTexture8 *pTexture, D3DXMATRIX &MatView, D3DXMATRIX &MatProj);
  virtual void AddLayerToScreen(
    CGrassPrimitiveStream *PSLayer,
    int X,
    int Y,
    D3DXVECTOR3 &LightVector,
    D3DXMATRIX &MatView,
    D3DXMATRIX &MatProj);
  virtual void DrawLayer(D3DXMATRIX &MatView, D3DXMATRIX &MatProj, D3DXVECTOR3 &LightVector, CHeightMap &HeightMap, Vector3Par Pos);
};


#endif