#include "Ecosystem.h"

DWORD dwMyVertexDecl[] =
{
    D3DVSD_STREAM(0),
    D3DVSD_REG(D3DVSDE_POSITION,  D3DVSDT_FLOAT3),      // position
    D3DVSD_REG(D3DVSDE_NORMAL,    D3DVSDT_FLOAT3),      // normal
    D3DVSD_REG(D3DVSDE_DIFFUSE,   D3DVSDT_D3DCOLOR),    // diffuse color
    D3DVSD_REG(D3DVSDE_TEXCOORD0, D3DVSDT_FLOAT2),      // 1 TC
    D3DVSD_REG(D3DVSDE_TEXCOORD1, D3DVSDT_FLOAT2),      // 2 TC
    D3DVSD_REG(D3DVSDE_TEXCOORD2, D3DVSDT_FLOAT2),      // 3 TC
    D3DVSD_END()
};

void CEcosystem::Init(IDirect3DDevice8 *pD3DDevice, const char *GLBaseAName) {
  // Save device
  _pD3DDevice = pD3DDevice; pD3DDevice->AddRef();
  // Init stream
  _pPS = new CGrassPrimitiveStream(pD3DDevice);
  _pPS->Init(sizeof(SMyVertex), D3DFVF_MYVERTEX);
  _pPSLayerClose = new CGrassPrimitiveStream(pD3DDevice);
  _pPSLayerClose->Init(sizeof(SLayerCloseVertex), D3DFVF_LAYERCLOSEVERTEX);
  // Load height data
  _HeightMap.Init("Krajina.ase");
  // Init eye's position and orientation
  _EyePosition = D3DXVECTOR3((float)HEIGHTMAP_SIZE/2.0f, 10.0f, (float)HEIGHTMAP_SIZE/2.0f);
  _EyeRotX = 0.0f;
  _EyeRotY = 0.0f;
  _EyeHeight = 1.5f;
  // Init the sun angle
  _SunAngle = H_PI * 0.5f;
  // Layer initialization
  _LayerGround.Init(pD3DDevice);
  _LayerMoss.Init(pD3DDevice);
  _LayerForest.Init(pD3DDevice);
  // Init the NeighbourLayer
  _NeighbourLayer0.Init(_pD3DDevice, 16);
  _NeighbourLayer0.UpdatePos((int)_EyePosition.x, (int)_EyePosition.z);
  _NeighbourLayer0.BeginRendering();
  _NeighbourLayer0.AddLayer2(&_LayerGround);
  //_NeighbourLayer0.AddLayer2(&_LayerForest);
  _NeighbourLayer0.EndRendering();

  _NeighbourLayer1.Init(_pD3DDevice, 32);
  _NeighbourLayer1.UpdatePos((int)_EyePosition.x, (int)_EyePosition.z);
  _NeighbourLayer1.BeginRendering();
  _NeighbourLayer1.AddLayer2(&_LayerGround);
  //_NeighbourLayer1.AddLayer2(&_LayerForest);
  _NeighbourLayer1.EndRendering();

  _NeighbourLayer2.Init(_pD3DDevice, 64);
  _NeighbourLayer2.UpdatePos((int)_EyePosition.x, (int)_EyePosition.z);
  _NeighbourLayer2.BeginRendering();
  _NeighbourLayer2.AddLayer2(&_LayerGround);
  //_NeighbourLayer2.AddLayer2(&_LayerForest);
  _NeighbourLayer2.EndRendering();

  //-------------------------------------------------------------------------

  // Clear primitive streams
  _pPS->Clear();
  _pPSLayerClose->Clear();

  // Generate vertices
  SMyVertex MV;
  SLayerCloseVertex LCV;
  srand(0);
  for (int j = 0; j < HEIGHTMAP_SIZE; j++) {
    for (int i = 0; i < HEIGHTMAP_SIZE; i++) {
      MV.Position = Vector3((float)i, _HeightMap.GetHeight(i, j), (float)j);
      MV.Normal = Vector3(0, 0, -1);
      MV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
      _NeighbourLayer0.GetTextureCoord(i, j, MV.U1, MV.V1);
      _NeighbourLayer1.GetTextureCoord(i, j, MV.U2, MV.V2);
      _NeighbourLayer2.GetTextureCoord(i, j, MV.U3, MV.V3);
      _pPS->AddVertex(&MV);

      // Fill out layer vertices
      Vector3 P00 = Vector3(i, _HeightMap.GetHeight(i, j), j);
      Vector3 VI;
      Vector3 VJ;
      if ((i < (HEIGHTMAP_SIZE - 1)) && (j < (HEIGHTMAP_SIZE - 1))) {
        Vector3 P10 = Vector3(i + 1, _HeightMap.GetHeight(i + 1, j), j);
        Vector3 P01 = Vector3(i, _HeightMap.GetHeight(i, j + 1), j + 1);
        VI = (P10 - P00).Normalized();
        VJ = (P01 - P00).Normalized();
      }
      else {
        VI = Vector3(1, 0, 0);
        VJ = Vector3(0, 0, 1);
      }

      // Position
      LCV.Position = P00;

      // Normal
      LCV.Normal = VJ.CrossProduct(VI);
      
      // Diffuse color
      //int c = (rand() * 150) / RAND_MAX;
      //LCV.Diffuse = D3DCOLOR_XRGB(255 - c, 255 - c, 255 - c);
      
      //LCV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
      
      LCV.Diffuse = D3DCOLOR_XRGB(255, 128, 0);
      
      // Base texture coordinates
      LCV.U0 = (float)i + (((float)rand()) * 0.4f) / ((float)RAND_MAX) - 0.2f;
      LCV.V0 = (float)j + (((float)rand()) * 0.4f) / ((float)RAND_MAX) - 0.2f;

      // S coordinate
      LCV.S = VI;

      // T coordinate
      LCV.T = VJ;

      // Add vertex at last
      _pPSLayerClose->AddVertex(&LCV);
    }
  }

  // Generate indices
  for (j = 0; j < (HEIGHTMAP_SIZE - 1); j++) {
    for (int i = 0; i < (HEIGHTMAP_SIZE - 1); i++) {
      // Add first triangle of the square
      _pPS->AddIndex(0, (j    ) * HEIGHTMAP_SIZE + (i    ));
      _pPS->AddIndex(0, (j + 1) * HEIGHTMAP_SIZE + (i    ));
      _pPS->AddIndex(0, (j    ) * HEIGHTMAP_SIZE + (i + 1));
      // Add second triangle of the square
      _pPS->AddIndex(0, (j    ) * HEIGHTMAP_SIZE + (i + 1));
      _pPS->AddIndex(0, (j + 1) * HEIGHTMAP_SIZE + (i    ));
      _pPS->AddIndex(0, (j + 1) * HEIGHTMAP_SIZE + (i + 1));

      // Add first triangle of the square
      _pPSLayerClose->AddIndex(0, (j    ) * HEIGHTMAP_SIZE + (i    ));
      _pPSLayerClose->AddIndex(0, (j + 1) * HEIGHTMAP_SIZE + (i    ));
      _pPSLayerClose->AddIndex(0, (j    ) * HEIGHTMAP_SIZE + (i + 1));
      // Add second triangle of the square
      _pPSLayerClose->AddIndex(0, (j    ) * HEIGHTMAP_SIZE + (i + 1));
      _pPSLayerClose->AddIndex(0, (j + 1) * HEIGHTMAP_SIZE + (i    ));
      _pPSLayerClose->AddIndex(0, (j + 1) * HEIGHTMAP_SIZE + (i + 1));
    }
  }

  // Prepare primitive streams
  _pPS->Prepare();
  _pPSLayerClose->Prepare();



}

void CEcosystem::Draw(D3DXMATRIX &MatView, D3DXMATRIX &MatProj, int Seed, float Detail, Vector3Par Position) {

  _pPS->Clear();
  
  //_pPS->RegisterTextures(0, _pTexture[0], NULL);
  SMyVertex MV;
  WORD Index;

  MV.Position = Vector3(-1, 1, 0);
  MV.Normal = Vector3(0, 0, -1);
  MV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  MV.U1 = 0;
  MV.V1 = 0;
  Index = _pPS->AddVertex(&MV);
  _pPS->AddIndex(0, Index);

  MV.Position = Vector3(1, 1, 0);
  MV.Normal = Vector3(0, 0, -1);
  MV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  MV.U1 = 1;
  MV.V1 = 0;
  Index = _pPS->AddVertex(&MV);
  _pPS->AddIndex(0, Index);

  MV.Position = Vector3(-1, -1, 0);
  MV.Normal = Vector3(0, 0, -1);
  MV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  MV.U1 = 0;
  MV.V1 = 1;
  Index = _pPS->AddVertex(&MV);
  _pPS->AddIndex(0, Index);

  MV.Position = Vector3(-1, -1, 0);
  MV.Normal = Vector3(0, 0, -1);
  MV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  MV.U1 = 0;
  MV.V1 = 1;
  Index = _pPS->AddVertex(&MV);
  _pPS->AddIndex(0, Index);

  MV.Position = Vector3(1, 1, 0);
  MV.Normal = Vector3(0, 0, -1);
  MV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  MV.U1 = 1;
  MV.V1 = 0;
  Index = _pPS->AddVertex(&MV);
  _pPS->AddIndex(0, Index);

  MV.Position = Vector3(1, -1, 0);
  MV.Normal = Vector3(0, 0, -1);
  MV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
  MV.U1 = 1;
  MV.V1 = 1;
  Index = _pPS->AddVertex(&MV);
  _pPS->AddIndex(0, Index);


  _pPS->Prepare();
	_pD3DDevice->BeginScene();
  _pD3DDevice->SetVertexShader(D3DFVF_MYVERTEX);
  _pD3DDevice->SetPixelShader(0);
  _pD3DDevice->SetTransform(D3DTS_PROJECTION, &MatProj);
  _pD3DDevice->SetTransform(D3DTS_VIEW, &MatView);
  _pPS->Draw();
  _pD3DDevice->EndScene();

}

void CEcosystem::DrawLand(D3DXMATRIX &MatProj) {

  // Set height of the eye
  if ((_EyePosition.x > 0.0f) && 
      (_EyePosition.x < (HEIGHTMAP_SIZE - 1)) &&
      (_EyePosition.z > 0.0f) && 
      (_EyePosition.z < (HEIGHTMAP_SIZE - 1))) {
    _EyePosition.y = _HeightMap.GetHeight(_EyePosition.x, _EyePosition.z);
  }

  // Update textures if naccessary
  if (_NeighbourLayer0.NeedUpdate(_EyePosition.x, _EyePosition.z)) {
    _NeighbourLayer0.UpdatePos((int)_EyePosition.x, (int)_EyePosition.z);
    _NeighbourLayer0.BeginRendering();
    _NeighbourLayer0.AddLayer2(&_LayerGround);
    //_NeighbourLayer0.AddLayer2(&_LayerForest);
    _NeighbourLayer0.EndRendering();
  }
  if (_NeighbourLayer1.NeedUpdate(_EyePosition.x, _EyePosition.z)) {
    _NeighbourLayer1.UpdatePos((int)_EyePosition.x, (int)_EyePosition.z);
    _NeighbourLayer1.BeginRendering();
    _NeighbourLayer1.AddLayer2(&_LayerGround);
    //_NeighbourLayer1.AddLayer2(&_LayerForest);
    _NeighbourLayer1.EndRendering();
  }
  if (_NeighbourLayer2.NeedUpdate(_EyePosition.x, _EyePosition.z)) {
    _NeighbourLayer2.UpdatePos((int)_EyePosition.x, (int)_EyePosition.z);
    _NeighbourLayer2.BeginRendering();
    _NeighbourLayer2.AddLayer2(&_LayerGround);
    //_NeighbourLayer2.AddLayer2(&_LayerForest);
    _NeighbourLayer2.EndRendering();
  }
  
  // -----------------------------------------------------------------------------
  // Fill the primitive stream with values from neighbourhood

  // Clear primitive streams
  _pPSLayerClose->Clear();

  // Generate vertices
  SLayerCloseVertex LCV;
  int XPos = (int)(_EyePosition.x + 0.5f);
  int YPos = (int)(_EyePosition.z + 0.5f);
  int XMin = max(XPos - LOCALNEIGHBOURHOOD_SIZE, 0);
  int XMax = min(XPos + LOCALNEIGHBOURHOOD_SIZE, HEIGHTMAP_SIZE - 1);
  int YMin = max(YPos - LOCALNEIGHBOURHOOD_SIZE, 0);
  int YMax = min(YPos + LOCALNEIGHBOURHOOD_SIZE, HEIGHTMAP_SIZE - 1);
  for (int j = YMin; j <= YMax; j++) {
    for (int i = XMin; i <= XMax; i++) {
      // Fill out layer vertices
      Vector3 P00 = Vector3(i, _HeightMap.GetHeight(i, j), j);
      Vector3 VI;
      Vector3 VJ;
      if ((i < (HEIGHTMAP_SIZE - 1)) && (j < (HEIGHTMAP_SIZE - 1))) {
        Vector3 P10 = Vector3(i + 1, _HeightMap.GetHeight(i + 1, j), j);
        Vector3 P01 = Vector3(i, _HeightMap.GetHeight(i, j + 1), j + 1);
        VI = (P10 - P00).Normalized();
        VJ = (P01 - P00).Normalized();
      }
      else {
        VI = Vector3(1, 0, 0);
        VJ = Vector3(0, 0, 1);
      }

      // Position
      LCV.Position = P00;

      // Normal
      LCV.Normal = VJ.CrossProduct(VI);
      
      // Diffuse color
      int c = (rand() * 150) / RAND_MAX;
      LCV.Diffuse = D3DCOLOR_XRGB(255 - c, 255 - c, 255 - c);
      //LCV.Diffuse = D3DCOLOR_XRGB(255, 255, 255);
      //LCV.Diffuse = D3DCOLOR_XRGB(255, 128, 0);
      
      // Base texture coordinates
      srand(j * HEIGHTMAP_SIZE + i);
      LCV.U0 = (float)i + (((float)rand()) * 0.4f) / ((float)RAND_MAX) - 0.2f;
      LCV.V0 = (float)j + (((float)rand()) * 0.4f) / ((float)RAND_MAX) - 0.2f;
      //LCV.U0 *= 0.5f;
      //LCV.V0 *= 0.5f;

      // S coordinate
      LCV.S = VI;

      // T coordinate
      LCV.T = VJ;

      // Add vertex at last
      _pPSLayerClose->AddVertex(&LCV);
    }
  }

  // Generate indices
  int XSize = XMax - XMin + 1;
  for (j = YMin; j < YMax; j++) {
    for (int i = XMin; i < XMax; i++) {
      // Add first triangle of the square
      _pPSLayerClose->AddIndex(0, (j - YMin    ) * XSize + (i - XMin    ));
      _pPSLayerClose->AddIndex(0, (j - YMin + 1) * XSize + (i - XMin    ));
      _pPSLayerClose->AddIndex(0, (j - YMin    ) * XSize + (i - XMin + 1));
      // Add second triangle of the square
      _pPSLayerClose->AddIndex(0, (j - YMin    ) * XSize + (i - XMin + 1));
      _pPSLayerClose->AddIndex(0, (j - YMin + 1) * XSize + (i - XMin    ));
      _pPSLayerClose->AddIndex(0, (j - YMin + 1) * XSize + (i - XMin + 1));
    }
  }

  // Prepare primitive streams
  _pPSLayerClose->Prepare();

  // -----------------------------------------------------------------------------

  _pPS->RegisterTextures(0,
    _NeighbourLayer0.GetTexture(),
    _NeighbourLayer1.GetTexture(),
    _NeighbourLayer2.GetTexture());

  // Set the view matrix
  D3DXMATRIX MatView;
  D3DXMATRIX MatCamera;
  D3DXMATRIX MatTransl;
  D3DXMATRIX MatRotX;
  D3DXMATRIX MatRotY;
  D3DXMatrixTranslation(&MatTransl, _EyePosition.x, _EyePosition.y + _EyeHeight, _EyePosition.z);
  D3DXMatrixRotationX(&MatRotX, _EyeRotX);
  D3DXMatrixRotationY(&MatRotY, _EyeRotY);
  MatCamera = MatRotX * MatRotY * MatTransl;
  D3DXMatrixInverse(&MatView, NULL, &MatCamera);

  // Set the sun vector
  D3DXVECTOR3 SunVector;
  SunVector.x = cos(_SunAngle);
  SunVector.y = sin(_SunAngle);
  SunVector.z = 0.0f;



  // Render states for layers

/*
  _pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
  _pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
  _pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO);
  _pD3DDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
  _pD3DDevice->SetRenderState(D3DRS_ZENABLE, TRUE);

  // Set stage states
	_pD3DDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	_pD3DDevice->SetTextureStageState(0, D3DTSS_COLOROP,   D3DTOP_SELECTARG1);
	_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAOP,   D3DTOP_SELECTARG1);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_MINFILTER, D3DTEXF_LINEAR);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_MAGFILTER, D3DTEXF_LINEAR);

	_pD3DDevice->SetTextureStageState(1, D3DTSS_COLORARG1, D3DTA_CURRENT);
	_pD3DDevice->SetTextureStageState(1, D3DTSS_COLORARG2, D3DTA_TEXTURE);
	_pD3DDevice->SetTextureStageState(1, D3DTSS_COLOROP,   D3DTOP_BLENDCURRENTALPHA);
	_pD3DDevice->SetTextureStageState(1, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	_pD3DDevice->SetTextureStageState(1, D3DTSS_ALPHAOP,   D3DTOP_SELECTARG1);
  _pD3DDevice->SetTextureStageState(1, D3DTSS_MINFILTER, D3DTEXF_LINEAR);
  _pD3DDevice->SetTextureStageState(1, D3DTSS_MAGFILTER, D3DTEXF_LINEAR);

	_pD3DDevice->SetTextureStageState(2, D3DTSS_COLORARG1, D3DTA_CURRENT);
	_pD3DDevice->SetTextureStageState(2, D3DTSS_COLORARG2, D3DTA_TEXTURE);
	_pD3DDevice->SetTextureStageState(2, D3DTSS_COLOROP,   D3DTOP_BLENDCURRENTALPHA);
	_pD3DDevice->SetTextureStageState(2, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	_pD3DDevice->SetTextureStageState(2, D3DTSS_ALPHAOP,   D3DTOP_SELECTARG1);
  _pD3DDevice->SetTextureStageState(2, D3DTSS_MINFILTER, D3DTEXF_LINEAR);
  _pD3DDevice->SetTextureStageState(2, D3DTSS_MAGFILTER, D3DTEXF_LINEAR);

  _pD3DDevice->SetTextureStageState(0, D3DTSS_ADDRESSU, D3DTADDRESS_BORDER);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_ADDRESSV, D3DTADDRESS_BORDER);
  _pD3DDevice->SetTextureStageState(0, D3DTSS_BORDERCOLOR, 0x00000000);
  _pD3DDevice->SetTextureStageState(1, D3DTSS_ADDRESSU, D3DTADDRESS_BORDER);
  _pD3DDevice->SetTextureStageState(1, D3DTSS_ADDRESSV, D3DTADDRESS_BORDER);
  _pD3DDevice->SetTextureStageState(1, D3DTSS_BORDERCOLOR, 0x00000000);
  _pD3DDevice->SetTextureStageState(2, D3DTSS_ADDRESSU, D3DTADDRESS_BORDER);
  _pD3DDevice->SetTextureStageState(2, D3DTSS_ADDRESSV, D3DTADDRESS_BORDER);
  _pD3DDevice->SetTextureStageState(2, D3DTSS_BORDERCOLOR, 0x00000000);

  _pPS->Prepare();
	_pD3DDevice->BeginScene();
  _pD3DDevice->SetVertexShader(D3DFVF_MYVERTEX);
  _pD3DDevice->SetPixelShader(0);
  _pD3DDevice->SetTransform(D3DTS_PROJECTION, &MatProj);
  _pD3DDevice->SetTransform(D3DTS_VIEW, &MatView);
  _pPS->Draw();
  _pD3DDevice->EndScene();
*/



	// Set up the textures
	_pD3DDevice->SetTextureStageState(0, D3DTSS_MINFILTER, D3DTEXF_POINT);
	_pD3DDevice->SetTextureStageState(0, D3DTSS_MAGFILTER, D3DTEXF_POINT);

	// Enable alpha testing (skips pixels with less than a certain alpha.)
	_pD3DDevice->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	_pD3DDevice->SetRenderState(D3DRS_ALPHAREF,        0x08);
	_pD3DDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);
	_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);

	D3DXVECTOR3 TSunVector;
	D3DXVec3TransformNormal(&TSunVector, &SunVector, &MatView);
  Vector3 Eye(_EyePosition.x, _EyePosition.y + _EyeHeight, _EyePosition.z);
  _LayerForest.DrawLayer(MatView, MatProj, TSunVector, _HeightMap, Eye);


	_pD3DDevice->BeginScene();
  
  // Draw close layers
  _LayerGround.AddLayerToScreen(_pPSLayerClose, (int)(_EyePosition.x + 0.5f), (int)(_EyePosition.z + 0.5f), SunVector, MatView, MatProj);
  _LayerMoss.AddLayerToScreen(_pPSLayerClose, (int)(_EyePosition.x + 0.5f), (int)(_EyePosition.z + 0.5f), SunVector, MatView, MatProj);
  _pD3DDevice->EndScene();



}

//--------------------------------------------------------------------------------------
