// LandGrabberView.cpp : implementation of the CLandGrabberView class
//

#include "stdafx.h"
#include "LandGrabber.h"

#include "MainFrm.h"
#include "LandGrabberDoc.h"
#include "LandGrabberView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLandGrabberView

IMPLEMENT_DYNCREATE(CLandGrabberView, CView)

BEGIN_MESSAGE_MAP(CLandGrabberView, CView)
	//{{AFX_MSG_MAP(CLandGrabberView)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_COMMAND(ID_FILE_EXPORT, OnFileExport)
	ON_UPDATE_COMMAND_UI(ID_FILE_EXPORT, OnUpdateFileExport)
	ON_CBN_SELCHANGE(IDC_AREASIZE, OnSelchangeAreasize)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLandGrabberView construction/destruction

CLandGrabberView::CLandGrabberView()
{
  _FramePosition.x = 0;
  _FramePosition.y = 0;
  _FrameSizeX = 64;
  _FrameSizeY = 64;
}

CLandGrabberView::~CLandGrabberView()
{
}

BOOL CLandGrabberView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CLandGrabberView drawing

void CLandGrabberView::OnDraw(CDC* pDC)
{
	CLandGrabberDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

  // Draw the land
  pDoc->_Land.Draw(pDC->GetSafeHdc());

  // Draw the cursor
  int x = _FramePosition.x;
  int y = _FramePosition.y;
  int HalfSizeX = _FrameSizeX / 2;
  int HalfSizeY = _FrameSizeY / 2;
  pDC->MoveTo(x - HalfSizeX,     y - HalfSizeY);
  pDC->LineTo(x + HalfSizeX - 1, y - HalfSizeY);
  pDC->LineTo(x + HalfSizeX - 1, y + HalfSizeY - 1);
  pDC->LineTo(x - HalfSizeX,     y + HalfSizeY - 1);
  pDC->LineTo(x - HalfSizeX,     y - HalfSizeY);
}

/////////////////////////////////////////////////////////////////////////////
// CLandGrabberView printing

BOOL CLandGrabberView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CLandGrabberView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CLandGrabberView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CLandGrabberView diagnostics

#ifdef _DEBUG
void CLandGrabberView::AssertValid() const
{
	CView::AssertValid();
}

void CLandGrabberView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CLandGrabberDoc* CLandGrabberView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CLandGrabberDoc)));
	return (CLandGrabberDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLandGrabberView message handlers

void CLandGrabberView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default

	CView::OnLButtonDown(nFlags, point);
}

void CLandGrabberView::OnMouseMove(UINT nFlags, CPoint point) 
{
  if (nFlags & MK_LBUTTON) {
    _FramePosition.x = point.x;
    _FramePosition.y = point.y;
	  RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_UPDATENOW);
  }
	
	CView::OnMouseMove(nFlags, point);
}

void CLandGrabberView::OnFileExport() 
{
	CString Name;
  CDocManager DM;
	if (!DM.DoPromptFileName(Name, AFX_IDS_SAVEFILE, OFN_HIDEREADONLY, TRUE, NULL)) return;

  CLandGrabberDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

  //pDoc->_Land.Export(Name, _FramePosition.x - _FrameSizeX/2, _FramePosition.y - _FrameSizeY/2, _FrameSizeX + 1, _FrameSizeY + 1);
  pDoc->_Land.Export(Name, _FramePosition.x - _FrameSizeX/2, _FramePosition.y - _FrameSizeY/2, _FrameSizeX, _FrameSizeY);
}

void CLandGrabberView::OnUpdateFileExport(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
}

void CLandGrabberView::OnSelchangeAreasize() 
{
  CMainFrame *pFrame = (CMainFrame *)AfxGetMainWnd();
  CWnd *Item = pFrame->m_wndDialogBar.GetDlgItem(IDC_AREASIZE);
  if (Item != NULL) {
    CComboBox *Combo = (CComboBox*) Item;
    switch (Combo->GetCurSel()) {
      case 0:
        _FrameSizeX = 64;
        _FrameSizeY = 64;
        break;
      case 1:
        _FrameSizeX = 128;
        _FrameSizeY = 128;
        break;
      case 2:
        _FrameSizeX = 256;
        _FrameSizeY = 256;
        break;
      case 3:
        _FrameSizeX = 512;
        _FrameSizeY = 512;
        break;
      case 4:
        _FrameSizeX = 1024;
        _FrameSizeY = 1024;
        break;
      case 5:
        _FrameSizeX = 2048;
        _FrameSizeY = 2048;
        break;
    }
	  RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_UPDATENOW);
  }
}
