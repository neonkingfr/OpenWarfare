// LandGrabber.h : main header file for the LANDGRABBER application
//

#if !defined(AFX_LANDGRABBER_H__104C4B65_B8EC_4BAE_B945_0D29B9884B40__INCLUDED_)
#define AFX_LANDGRABBER_H__104C4B65_B8EC_4BAE_B945_0D29B9884B40__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CLandGrabberApp:
// See LandGrabber.cpp for the implementation of this class
//

class CLandGrabberApp : public CWinApp
{
public:
	CLandGrabberApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLandGrabberApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CLandGrabberApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LANDGRABBER_H__104C4B65_B8EC_4BAE_B945_0D29B9884B40__INCLUDED_)
