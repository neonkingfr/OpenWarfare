// LandGrabberView.h : interface of the CLandGrabberView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_LANDGRABBERVIEW_H__298CB91E_92FD_4C72_B42D_2FC5F3052AE4__INCLUDED_)
#define AFX_LANDGRABBERVIEW_H__298CB91E_92FD_4C72_B42D_2FC5F3052AE4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CLandGrabberView : public CView
{
protected: // create from serialization only
	CLandGrabberView();
	DECLARE_DYNCREATE(CLandGrabberView)

// Attributes
public:
	CLandGrabberDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLandGrabberView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLandGrabberView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

  POINT _FramePosition;
  int _FrameSizeX;
  int _FrameSizeY;

// Generated message map functions
protected:
	//{{AFX_MSG(CLandGrabberView)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnFileExport();
	afx_msg void OnUpdateFileExport(CCmdUI* pCmdUI);
	afx_msg void OnSelchangeAreasize();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in LandGrabberView.cpp
inline CLandGrabberDoc* CLandGrabberView::GetDocument()
   { return (CLandGrabberDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LANDGRABBERVIEW_H__298CB91E_92FD_4C72_B42D_2FC5F3052AE4__INCLUDED_)
