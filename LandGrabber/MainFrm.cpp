// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "LandGrabber.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

  if (!m_wndDialogBar.Create(this, IDD_AREASELECTION, CBRS_TOP, 9999)) {
		TRACE0("Failed to create dialogbar\n");
		return -1;      // fail to create
  }
  CWnd *Item = m_wndDialogBar.GetDlgItem(IDC_AREASIZE);
  if (Item != NULL) {
    CComboBox *Combo = (CComboBox*) Item;
    Combo->SetCurSel(0);
  }

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
  m_wndDialogBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);
  CRect rect1, rect2;
  m_wndToolBar.GetClientRect(rect1);
  m_wndDialogBar.GetClientRect(rect2);
  int w = rect2.Width();
  rect2.left = rect1.right + 1;
  rect2.top = rect1.top;
  rect2.right = rect2.left + w;
  rect2.bottom = rect1.bottom;
	DockControlBar(&m_wndDialogBar, AFX_IDW_DOCKBAR_TOP, rect2);
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
/*
void CMainFrame::OnSelchangeAreasize() 
{
  CWnd *Item = m_wndDialogBar.GetDlgItem(IDC_AREASIZE);
  if (Item != NULL) {
    CComboBox *Combo = (CComboBox*) Item;
    int Sel = Combo->GetCurSel();
    int a;
    switch (Combo->GetCurSel()) {
      case 0:
        a = 1;
        break;

      case 1:
        a = 2;
        break;
    }
  }

  class CLandGrabberView;
  CLandGrabberView *LGView = (CLandGrabberView*) GetActiveView();
  LGView->SetFrameSize(10, 10);

}
*/