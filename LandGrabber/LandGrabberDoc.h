// LandGrabberDoc.h : interface of the CLandGrabberDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_LANDGRABBERDOC_H__50A33D45_BAB3_48E7_A4C6_BA16A2B31CEA__INCLUDED_)
#define AFX_LANDGRABBERDOC_H__50A33D45_BAB3_48E7_A4C6_BA16A2B31CEA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Land.h"

class CLandGrabberDoc : public CDocument
{
protected: // create from serialization only
	CLandGrabberDoc();
	DECLARE_DYNCREATE(CLandGrabberDoc)

// Attributes
public:

  CLand _Land;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLandGrabberDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLandGrabberDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CLandGrabberDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LANDGRABBERDOC_H__50A33D45_BAB3_48E7_A4C6_BA16A2B31CEA__INCLUDED_)
