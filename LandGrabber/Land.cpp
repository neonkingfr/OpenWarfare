#include "stdafx.h"
#include "float.h"
#include "Land.h"

#define COLOR_ARGB(a,r,g,b) ((COLORREF)((((a)&0xff)<<24)|(((b)&0xff)<<16)|(((g)&0xff)<<8)|((r)&0xff)))

SDimension CLand::GetDimensions(FILE *f) {
  SDimension Dimension;

  // Move the cursor to the beginning
  fseek(f, 0, SEEK_SET);

  // This step requires data in *.XYZ to be sequenced line by line. First item must be the X
  // coordinate, whereas the second one Y.
  /*float OldX, NewX, CurrY, CurrZ;
  fscanf(f, "%f %f %f", &OldX, &CurrY, &CurrZ);
  int Counter = 1;
  while (fscanf(f, "%f %f %f", &NewX, &CurrY, &CurrZ) == 3) {
    Counter++;
    if (NewX != OldX) break;
  }
  Dimension.SizeX = Counter - 1;
  while (fscanf(f, "%f %f %f", &NewX, &CurrY, &CurrZ) == 3) {
    Counter++;
  }
  Dimension.SizeY = Counter / Dimension.SizeX;
  
  // Move the cursor to the beginning
  fseek(f, 0, SEEK_SET);*/

  // Initialize dimension
  Dimension.MinX = FLT_MAX;
  Dimension.MaxX = -FLT_MAX;
  Dimension.MinY = FLT_MAX;
  Dimension.MaxY = -FLT_MAX;
  Dimension.MinZ = FLT_MAX;
  Dimension.MaxZ = -FLT_MAX;
  Dimension.StepX = 0.0f;
  Dimension.StepY = 0.0f;

  // Read each line and get values
  float X, Y, Z;
  float SecondMaxX = -FLT_MAX;
  float SecondMaxY = -FLT_MAX;
  while (fscanf(f, "%f %f %f", &X, &Y, &Z) == 3) {
    if (X < Dimension.MinX) Dimension.MinX = X;
    if (X > Dimension.MaxX) {
      SecondMaxX = Dimension.MaxX;
      Dimension.MaxX = X;
    }
    else if ((X > SecondMaxX) && (X != Dimension.MaxX)) {
      SecondMaxX = X;
    }
    if (Y < Dimension.MinY) Dimension.MinY = Y;
    if (Y > Dimension.MaxY) {
      SecondMaxY = Dimension.MaxY;
      Dimension.MaxY = Y;
    }
    else if ((Y > SecondMaxY) && (Y != Dimension.MaxY)){
      SecondMaxY = Y;
    }
    if (Z < Dimension.MinZ) Dimension.MinZ = Z;
    if (Z > Dimension.MaxZ) Dimension.MaxZ = Z;
  }
  Dimension.StepX = Dimension.MaxX - SecondMaxX;
  Dimension.StepY = Dimension.MaxY - SecondMaxY;

  Dimension.SizeX = (Dimension.MaxX - Dimension.MinX) / Dimension.StepX;
  Dimension.SizeY = (Dimension.MaxY - Dimension.MinY) / Dimension.StepY;

  return Dimension;
}

CLand::CLand() {
  _Data = NULL;
  _RGBData = NULL;
}

CLand::~CLand() {
  if (_Data != NULL) delete _Data;
  if (_RGBData != NULL) delete _RGBData;
}

/*
void CLand::Load(const char *FileName) {
  FILE *f;
  if ((f = fopen(FileName, "rt")) != NULL) {
    // Save the name of the file
    strcpy(_SourceFileName, FileName);

    // Retrieve the dimensions
    _Dimension = GetDimensions(f);

    // Allocate array
    if (_Data != NULL) delete _Data;
    _SizeX = (int)((_Dimension.MaxX - _Dimension.MinX) / _Dimension.StepX) + 1;
    _SizeY = (int)((_Dimension.MaxY - _Dimension.MinY) / _Dimension.StepY) + 1;
    _Data = new float[_SizeX * _SizeY];

    // Fill the data with constant value
    for (int i = 0; i < _SizeX * _SizeY; i++) {
      _Data[i] = -1.0f;
    }
    
    // Read data
    fseek(f, 0, SEEK_SET);
    float X, Y, Z;
    while (fscanf(f, "%f %f %f", &X, &Y, &Z) == 3) {
      // Convert it to integer range 0..SizeXY
      int OffsetX = (int)((X - _Dimension.MinX) / _Dimension.StepX);
      int OffsetY = (int)((Y - _Dimension.MinY) / _Dimension.StepY);
      // Fill the array
      _Data[OffsetY * _SizeX + OffsetX] = Z;
    }

    // Close file
    fclose(f);

    // Fill up the bitmap data
    if (_RGBData != NULL) delete _RGBData;
    _RGBData = new unsigned int[_SizeX * _SizeY];
    for (i = 0; i < _SizeX * _SizeY; i++) {
      int C = (int)(_Data[i]/4.0f);
      _RGBData[i] = COLOR_ARGB(0, C, C, C);
    }
  }
}
*/

void CLand::Load(const char *FileName) {
  FILE *f;
  if ((f = fopen(FileName, "rt")) != NULL) {
    // Save the name of the file
    strcpy(_SourceFileName, FileName);

    // Retrieve the dimensions
    _Dimension = GetDimensions(f);

    // Allocate array
    delete _Data;
    _SizeX = _Dimension.SizeX;
    _SizeY = _Dimension.SizeY;
    _Data = new float[_SizeX * _SizeY];

    // Fill the data with constant value
    for (int i = 0; i < _SizeX * _SizeY; i++) {
      _Data[i] = -1.0f;
    }
    
    // Read data
    fseek(f, 0, SEEK_SET);
    float X, Y, Z;    
    while(fscanf(f, "%f %f %f", &X, &Y, &Z) == 3)
    {
      int xx = (X - _Dimension.MinX) / _Dimension.StepX;
      if (xx >= _Dimension.SizeX)
        xx = _Dimension.SizeX - 1;

      int yy = (Y - _Dimension.MinY) / _Dimension.StepY;
      if (yy >= _Dimension.SizeY)
        yy = _Dimension.SizeY - 1;

      _Data[yy * _SizeX + xx] = Z;
    }

    // Close file
    fclose(f);

    // Fill up the bitmap data
    if (_RGBData != NULL) delete _RGBData;
    _RGBData = new unsigned int[_SizeX * _SizeY];
    for (i = 0; i < _SizeX * _SizeY; i++) {
      int C = (int)(_Data[i]/4.0f);
      _RGBData[i] = COLOR_ARGB(0, C, C, C);
    }
  }
}

void CLand::Export(const char *FileName, int OffsetX, int OffsetY, int SizeX, int SizeY) {
  float MaxZ = -FLT_MAX;
  float MinZ = FLT_MAX;
  char String[256];
  FILE *f;
  // Write the data file
  strcpy(String, FileName);
  strcat(String, ".ase");
  if ((f = fopen(String, "wt+")) != NULL) {
    int LineId = 0;
    for (int y = OffsetY; y < OffsetY + SizeY; y++) {
      for (int x = OffsetX; x < OffsetX + SizeX; x++) {
        float CoordX = (x - OffsetX) * _Dimension.StepX;
        float CoordY = (y - OffsetY) * _Dimension.StepY;
        float CoordZ = _Data[y * _SizeX + x];
        if (MaxZ < CoordZ) MaxZ = CoordZ;
        if (MinZ > CoordZ) MinZ = CoordZ;
        fprintf(f, "*MESH_VERTEX %d %f %f %f\n", LineId, CoordX, CoordY, CoordZ);
        LineId++;
      }
    }
    fclose(f);
  }
  // Write the description file
  strcpy(String, FileName);
  strcat(String, ".dim");
  if ((f = fopen(String, "wt+")) != NULL) {
    fprintf(f, "Source file:       %s\n", _SourceFileName);
    fprintf(f, "Source size X:     %d\n", _SizeX);
    fprintf(f, "Source size Y:     %d\n", _SizeY);
    fprintf(f, "Source min height: %f\n", _Dimension.MinZ);
    fprintf(f, "Source max height: %f\n", _Dimension.MaxZ);
    fprintf(f, "Offset X:          %d\n", OffsetX);
    fprintf(f, "Offset Y:          %d\n", OffsetY);
    fprintf(f, "Size X:            %d\n", SizeX);
    fprintf(f, "Size Y:            %d\n", SizeY);
    fprintf(f, "Min height:        %f\n", MinZ);
    fprintf(f, "Max height:        %f\n", MaxZ);
    fprintf(f, "Step X:            %f\n", _Dimension.StepX);
    fprintf(f, "Step Y:            %f\n", _Dimension.StepY);
    fclose(f);
  }
}

BOOL CLand::GetRGBMap(unsigned int *&RGBData, int &SizeX, int &SizeY) {
  if (_Data != NULL) {
    RGBData = _RGBData;
    SizeX = _SizeX;
    SizeY = _SizeY;
    return TRUE;
  }
  return FALSE;
}

void CLand::Draw(HDC hdc) {
  if (_RGBData != NULL) {
    BITMAPINFOHEADER BIH;
	  BIH.biSize=sizeof(BITMAPINFOHEADER);
	  BIH.biWidth=_SizeX;
	  BIH.biHeight=-_SizeY;
	  BIH.biPlanes=1;
	  BIH.biBitCount=32;
	  BIH.biCompression=BI_RGB;
	  BIH.biSizeImage=0;
	  BIH.biXPelsPerMeter=5000;
	  BIH.biYPelsPerMeter=5000;
	  BIH.biClrUsed=0;
	  BIH.biClrImportant=0;

    BITMAPINFO BI;
    BI.bmiHeader = BIH;

	  HBITMAP hbmp = CreateDIBitmap(hdc, &BIH, CBM_INIT, _RGBData, &BI, DIB_RGB_COLORS);

 	  if (hbmp) {
		  // copy bitmap into the canvas
		  HDC hdcBits = CreateCompatibleDC(hdc);
		  if(hdcBits) {
			  HGDIOBJ prevObj = SelectObject(hdcBits, hbmp);
			  BitBlt(hdc,	0, 0, _SizeX, _SizeY, hdcBits, 0, 0, SRCCOPY);
			  SelectObject(hdcBits,prevObj);
			  DeleteDC(hdcBits);
		  }
		  DeleteObject(hbmp);
	  }
  }
}
