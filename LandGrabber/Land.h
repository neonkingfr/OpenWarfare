#ifndef _land_h_
#define _land_h_

struct SDimension {
  float MinX, MaxX, MinY, MaxY, MinZ, MaxZ;
  float StepX, StepY;
  int SizeX, SizeY;
};

class CLand {
private:
  float *_Data;
  unsigned int *_RGBData;
  int _SizeX;
  int _SizeY;
  SDimension _Dimension;
  char _SourceFileName[256];
  SDimension GetDimensions(FILE *f);
public:
  //! Default constructor.
  CLand();
  //! Destructor.
  ~CLand();
  void Load(const char *FileName);
  void Export(const char *FileName, int OffsetX, int OffsetY, int SizeX, int SizeY);
  BOOL GetRGBMap(unsigned int *&RGBData, int &SizeX, int &SizeY);
  void Draw(HDC hdc);
};


#endif