// LandGrabberDoc.cpp : implementation of the CLandGrabberDoc class
//

#include "stdafx.h"
#include "LandGrabber.h"

#include "LandGrabberDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define COLOR_ARGB(a,r,g,b) ((COLORREF)((((a)&0xff)<<24)|(((b)&0xff)<<16)|(((g)&0xff)<<8)|((r)&0xff)))

/////////////////////////////////////////////////////////////////////////////
// CLandGrabberDoc

IMPLEMENT_DYNCREATE(CLandGrabberDoc, CDocument)

BEGIN_MESSAGE_MAP(CLandGrabberDoc, CDocument)
	//{{AFX_MSG_MAP(CLandGrabberDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLandGrabberDoc construction/destruction

CLandGrabberDoc::CLandGrabberDoc()
{
	// TODO: add one-time construction code here
}

CLandGrabberDoc::~CLandGrabberDoc()
{
}

BOOL CLandGrabberDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CLandGrabberDoc serialization

void CLandGrabberDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
    _Land.Load(ar.m_strFileName);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CLandGrabberDoc diagnostics

#ifdef _DEBUG
void CLandGrabberDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLandGrabberDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLandGrabberDoc commands
