// CDKeyId.cpp : Defines the entry point for the console application.
//

#include <El/elementpch.hpp>
#include <El/CDKey/serial.hpp>

const unsigned char CDKeyPublicKey[] =
{
  0x11, 0x00, 0x00, 0x00,
  0x9F, 0x01, 0x51, 0x75, 0xBB, 0x99, 0xA8, 0xD7, 0x1B, 0xA1, 0x26, 0x3D, 0x40, 0x83, 0xAF
};
const char CDKeyFixed[] = "ArmAssault";
const int CDKeyIdLength = (sizeof(CDKeyPublicKey) / sizeof(*CDKeyPublicKey) - 4) - strlen(CDKeyFixed);

enum DistributionId
{
  DistInternal,
  DistBetaTest,
  DistRussian,
  DistCzech,
  DistPolish,
  DistGerman,
  DistUS,
  DistInternational,
  DistEnglishOnline,
};

int main(int argc, char* argv[])
{
  if (argc != 2)
  {
    printf("Usage:\n");
    printf("   CDKeyId <CD Key string>\n");
    return 0;		
  }

  CDKey cdkey;

  unsigned char buffer[KEY_BYTES];
  cdkey.DecodeMsg(buffer, argv[1]);

  cdkey.Init(buffer, CDKeyPublicKey);

  if (!cdkey.Check(CDKeyIdLength, CDKeyFixed))
  {
    printf("Invalid CD key\n");
    return 0;		
  }

  const char *distribution = "Unknown";
  long long int distId = cdkey.GetValue(0, 1);
  switch (distId)
  {
  case DistInternal:
    distribution = "Internal";
    break;
  case DistBetaTest:
    distribution = "Beta";
    break;
  case DistRussian:
    distribution = "Russian";
    break;
  case DistCzech:
    distribution = "Czech";
    break;
  case DistPolish:
    distribution = "Polish";
    break;
  case DistGerman:
    distribution = "German";
    break;
  case DistUS:
    distribution = "US";
    break;
  case DistInternational:
    distribution = "International";
    break;
  case DistEnglishOnline:
    distribution = "English Online";
    break;
  }

  __int64 val = cdkey.GetValue(0, CDKeyIdLength);
  printf("Distribution %s (%I64d), id %I64d\n", distribution, distId, val);

  if (val > INT_MAX)
  {
    printf("Id out of integer range.");
    return 0;
  }

	return (int)val;
}

