
#if !defined(AFX_PREFERENCES_H__8F1B9D79_9949_4262_9D0F_4A8292E640BF__INCLUDED_)
#define AFX_PREFERENCES_H__8F1B9D79_9949_4262_9D0F_4A8292E640BF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//! Preferences of the whole application.
class CPreferences : public CObject  
{
public:
  //! Registry key to the path of the external editor for class code.
	static LPCTSTR EDITOR_EXTERN;

  //! Registry key to the window class code of the buldozer.
	static LPCTSTR BULDOZER_WINDOW_CLASS;

  //! Registry key prefix of the global favourites.
	static LPCTSTR GLOBAL_FAVOURITES;

  //! Registry key to the guide unit value.
	static LPCTSTR GUIDE_UNIT;

  //! Registry key to the guide snap value.
	static LPCTSTR GUIDE_SNAP;

  //! Registry key to the grid's horizontal unit value.
	static LPCTSTR GRID_HORZ_UNIT;

  //! Registry key to the grid's vertical unit value.
	static LPCTSTR GRID_VERT_UNIT;

  //! Registry key - snap items to the guides?.
	static LPCTSTR GUIDES_SWITCH;

  //! Registry key - snap items to the grid?.
	static LPCTSTR GRID_SWITCH;

  //! Registry key to the show/hide guides value.
	static LPCTSTR SHOW_GUIDES;

  //! Registry key to the show/hide grid value.
	static LPCTSTR SHOW_GRID;

  //! Unwrap macros in opened files? (Registry key)
	static LPCTSTR UNWRAP_INCLUDED_MACROS;

  //! Show message box about removing comments which parser is not able to remember. (Registry key)
	static LPCTSTR SHOW_REMOVE_COMMENT_BOX;

  //! Tabulator char. (Registry key)
	static LPCTSTR TABULATOR;

  //! If this number of chars was exceeded by array code then array is wraped to more lines. (Registry key)
	static LPCTSTR ARRAY_TEXT_WRAP_LIMIT;

  //! Snap items? (Registry key)
	static LPCTSTR ITEM_SNAP_SWITCH;

  //! Value for the find/replace dialog. (Registry key)
	static LPCTSTR EDITOR_REPLACEWITH;

  //! Value for the find/replace dialog. (Registry key)
	static LPCTSTR EDITOR_FINDWHAT;

  //! If true then store new items in class 'controls', otherwise in array 'controls'.
	static LPCTSTR CONTROL_CLASSES;

	CPreferences();
	virtual ~CPreferences();

  //! Return path to the *.ini file.
	bool GetPrefFileName (CString &name);
	void Load ();
	void Save ();

	bool IsValue (LPCTSTR key);
	
	bool GetValue (LPCTSTR key, CString &str);
	bool GetValue (LPCTSTR key, LPTSTR str, int n);
	bool GetValue (LPCTSTR key, float &f);
	bool GetValue (LPCTSTR key, double &f);
	bool GetValue (LPCTSTR key, int &i);
	bool GetValue (LPCTSTR key, bool &b);

	void SetValue (LPCTSTR key, LPCTSTR str, bool save = true);
	void SetValue (LPCTSTR key, float f, bool save = true);
	void SetValue (LPCTSTR key, double f, bool save = true);
	void SetValue (LPCTSTR key, int i, bool save = true);
	void SetValue (LPCTSTR key, bool b, bool save = true);

protected:
	CArray<CString, LPCTSTR>		_keys;
	CArray<CString, LPCTSTR>		_values;
};

#endif // !defined(AFX_PREFERENCES_H__8F1B9D79_9949_4262_9D0F_4A8292E640BF__INCLUDED_)
