
#include "stdafx.h"
#include "rsceditor.h"

#include "RscEditorItems.h"
#include "RscClassSpace.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"
#include "RscMacro.h"
#include "RscEditorDoc.h"
#include "RscEditorPreview.h"
#include "RscEditorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CRscClassSpace::CRscClassSpace()
	: _view (NULL), _rButtonDown (false), _lastDropEffect (DROPEFFECT_NONE), _itemClick (NULL)
{
}

CRscClassSpace::~CRscClassSpace()
{
}


BEGIN_MESSAGE_MAP(CRscClassSpace, CTreeCtrl)
	//{{AFX_MSG_MAP(CRscClassSpace)
	ON_WM_RBUTTONDOWN()
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
	ON_NOTIFY_REFLECT(TVN_BEGINDRAG, OnBeginDrag)
	ON_NOTIFY_REFLECT(TVN_SELCHANGED, OnSelChanged)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_DESTROY()
	ON_WM_LBUTTONDOWN()
	ON_NOTIFY_REFLECT(NM_CLICK, OnClick)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CRscClassSpace::OnRButtonDown(UINT nFlags, CPoint point) 
{
	_rButtonDown = true;
	SetFocus ();
}

void CRscClassSpace::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	if (!_rButtonDown)
	{
		HTREEITEM item = GetSelectedItem ();
		if (item != NULL)
		{
			RECT r;
			if (GetItemRect (item, &r, TRUE))
			{
				point.x = (r.right + r.left) >> 1;
				point.y = (r.bottom + r.top) >> 1;
				ClientToScreen (&point);
				_view->DisplayContexMenu (2, point);
			}
		}
	}
}

void CRscClassSpace::OnRButtonUp(UINT nFlags, CPoint point) 
{
	_rButtonDown = false;
	UINT flags;
	HTREEITEM ti = HitTest (point, &flags);
	if (ti != NULL &&
		(flags & TVHT_ONITEM) != 0)
	{
		SelectItem (ti);
	}
	ClientToScreen (&point);
	_view->DisplayContexMenu (2, point);
}

void CRscClassSpace::DropRegister ()
{
	if (_dropTarget._control == NULL)
	{
		_dropTarget._control = this;
		_dropTarget.Register (this);
	}
}

DROPEFFECT CRscClassSpace::OnDragEnter(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point)
{
	ASSERT(_lastDropEffect == DROPEFFECT_NONE);

	_lastInsertMarkItem = NULL;
	LockWindowUpdate ();
	return OnDragOver(pWnd, pDataObject, dwKeyState, point);
}

DROPEFFECT CRscClassSpace::OnDragOver(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point) 
{
	UnlockWindowUpdate ();
	
	CRect dropRect;
	GetClientRect (&dropRect);
	CClientDC dc(this);
	bool scroll = false;
	
	if (point.y < SCROLL_AREA)
	{
		HTREEITEM item = GetFirstVisibleItem ();
		if (item != NULL)
		{
			item = GetPrevVisibleItem (item);
			if (item != NULL)
			{
				DWORD ct = timeGetTime ();
				if (ct - _lastDragTime > SCROLL_TIME)
				{
					if (_lastDropEffect != DROPEFFECT_NONE)
					{
						// erase previous focus rect
						dc.DrawFocusRect(_lastDropRect);
						SetInsertMark (NULL);
						_lastDropEffect = DROPEFFECT_NONE;
					}
					SelectSetFirstVisible (item);
					UpdateWindow ();
					_lastDragTime = ct;
				}
				scroll = true;
			}
		}
	}
	else if (point.y >= dropRect.bottom - SCROLL_AREA)
	{
		HTREEITEM item = GetFirstVisibleItem ();
		if (item != NULL)
		{
			item = GetNextVisibleItem (item);
			if (item != NULL)
			{
				DWORD ct = timeGetTime ();
				if (ct - _lastDragTime > SCROLL_TIME)
				{
					if (_lastDropEffect != DROPEFFECT_NONE)
					{
						// erase previous focus rect
						dc.DrawFocusRect(_lastDropRect);
						SetInsertMark (NULL);
						_lastDropEffect = DROPEFFECT_NONE;
					}
					SelectSetFirstVisible (item);
					UpdateWindow ();
					_lastDragTime = ct;
				}
				scroll = true;
			}
		}
	}

	if (!scroll)
	{
		UINT cfClass = RegisterClipboardFormat (_T ("class.rsceditor.bi"));
		UINT cfAttr = RegisterClipboardFormat (_T ("attribute.rsceditor.bi"));
		DROPEFFECT de = DROPEFFECT_NONE;
		UINT flags;
		HTREEITEM hitItem = HitTest (point, &flags);
		BOOL isClass = pDataObject->IsDataAvailable (cfClass);
		
		if (isClass || pDataObject->IsDataAvailable (cfAttr))
		{
			CRscWorkTreeItem *dropParam = NULL;
			if (hitItem != NULL)
			{
				dropParam = (CRscWorkTreeItem*) GetItemData (hitItem);
			}

			CRscWorkTreeItem *dragParam = NULL;
			if (_view->_dragItem != NULL)
			{
				dragParam = (CRscWorkTreeItem*) _view->_dragSpaceCtrl->GetItemData (_view->_dragItem);
			}

			if (dragParam != NULL && dragParam->IsParam () &&
				(dropParam == NULL || (dropParam->IsParam () && ((CRscParamBase*) dropParam)->IsDropable (_lastInsertMarkAfter))))
			{
				// move or copy or derive parameter to the new position
				CRscEditorDoc *doc = _view->GetDocument ();
				bool after = _lastInsertMarkAfter;
				if (dropParam == NULL)
				{
					int size = doc->_params.GetSize ();
					if (size > 0)
					{
						dropParam = doc->_params[size - 1];
					}
					after = true;
				} 
				else if (!after)
				{
					CRscClass *parent = (dropParam != NULL ? ((CRscParamBase*) dropParam)->_parent : NULL);
					dropParam = doc->FindPrevParam (parent, (CRscParamBase*) dropParam);
					after = true;
				}

				if ((dwKeyState & (MK_CONTROL | MK_SHIFT)) == (MK_CONTROL | MK_SHIFT) &&
					((CRscParamBase*) dragParam)->IsClass () && 
					(!((CRscClass*) dragParam)->IsIncluded () || ((CRscClass*) dragParam)->_parent == NULL) &&
					doc->FindBaseClassDefinition (((CRscClass*) dragParam)->_name, (CRscParamBase*) dropParam, dropParam != NULL ? ((CRscParamBase*) dropParam)->_parent : NULL, false, true) == dragParam)
				{
					de = DROPEFFECT_LINK;
				}
				else if ((dwKeyState & MK_CONTROL) == MK_CONTROL || ((CRscParamBase*) dragParam)->IsIncluded ())
				{
					de = DROPEFFECT_COPY;
				}
				else
				{
					de = DROPEFFECT_MOVE;
				}
			}
			else if (dropParam == NULL || 
				(dropParam->IsParam () && ((CRscParamBase*) dropParam)->IsDropable (_lastInsertMarkAfter)))
			{
				de = DROPEFFECT_COPY;
			}
		}
		
		if (point == _dragPoint && de == _lastDropEffect)
		{
			if (hitItem != NULL && (flags & TVHT_ONITEM) != 0)
			{
				DWORD ct = timeGetTime ();
				if (ct - _lastDragTime > 1000)
				{
					if (ItemHasChildren (hitItem))
					{
						LockWindowUpdate ();
						OnDragLeave (pWnd);
						Expand (hitItem, TVE_TOGGLE);
						UpdateWindow ();
					}
					_lastDragTime = ct;
				}
				LockWindowUpdate ();
				return _lastDropEffect;
			}
			else
			{
				LockWindowUpdate ();
				return _lastDropEffect;
			}
		}
		
		_lastDragTime = timeGetTime ();
		_dragPoint = point;
		
		// otherwise, cursor has moved -- need to update the drag feedback
		if (_lastDropEffect != DROPEFFECT_NONE)
		{
			// erase previous focus rect
			dc.DrawFocusRect(_lastDropRect);
		}
		_lastDropEffect = de;
		if (_lastDropEffect != DROPEFFECT_NONE)
		{
			if (hitItem != NULL && (flags & TVHT_ONITEMROW) != 0)
			{
				_lastInsertMarkItem = hitItem;
				RECT r;
				GetItemRect (hitItem, &r, FALSE);
				_lastInsertMarkAfter = point.y > ((r.top + r.bottom) >> 1);
				SetInsertMark (_lastInsertMarkItem, _lastInsertMarkAfter);
			}
			else
			{
				_lastInsertMarkItem = NULL;
				SetInsertMark (NULL);
			}
			
			UpdateWindow ();
			_lastDropRect = dropRect;
			dc.DrawFocusRect(dropRect);
		}
		else
		{
			SetInsertMark (NULL);
			UpdateWindow ();
		}
	}
	LockWindowUpdate ();
	return _lastDropEffect;
}

void CRscClassSpace::OnDragLeave(CWnd *pWnd) 
{
	UnlockWindowUpdate ();
	SetInsertMark (NULL);
	CClientDC dc(this);
	if (_lastDropEffect != DROPEFFECT_NONE)
	{
		dc.DrawFocusRect(_lastDropRect); // erase previous focus rect
		_lastDropEffect = DROPEFFECT_NONE;
	}
	UpdateWindow ();
}

BOOL CRscClassSpace::OnDrop (CWnd *pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point)
{
	ASSERT_VALID(this);

	// clean up focus rect
	OnDragLeave(pWnd);

	UINT cfClass = RegisterClipboardFormat (_T ("class.rsceditor.bi"));
	UINT cfAttr = RegisterClipboardFormat (_T ("attribute.rsceditor.bi"));

	if (pDataObject->IsDataAvailable (cfClass))
	{
		CString str;
		HGLOBAL data = pDataObject->GetGlobalData (cfClass);
		if (data != NULL)
		{
			BOOL res = FALSE;
			LPVOID ptr = ::GlobalLock (data);
			if (ptr != NULL)
			{
				str = (LPCTSTR) ptr;
				::GlobalUnlock (data);
				BOOL res = ProcessParamDropInWorkSpace (str, true, dropEffect);
			}
			_view->_dragItem = NULL;
			_view->_dragSpaceCtrl = NULL;
			return res;
		}
	}
	else if (pDataObject->IsDataAvailable (cfAttr))
	{
		CString str;
		HGLOBAL data = pDataObject->GetGlobalData (cfAttr);
		if (data != NULL)
		{
			BOOL res = FALSE;
			LPVOID ptr = ::GlobalLock (data);
			if (ptr != NULL)
			{
				str = (LPCTSTR) ptr;
				::GlobalUnlock (data);
				BOOL res = ProcessParamDropInWorkSpace (str, false, dropEffect);
			}
			_view->_dragItem = NULL;
			_view->_dragSpaceCtrl = NULL;
			return res;
		}
	}

	return FALSE;
}

void CRscClassSpace::OnBeginDrag(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;

	*pResult = 0;

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) GetItemData (pNMTreeView->itemNew.hItem);
	if (wtItem == NULL ||
		!wtItem->IsParam ())
	{
		return;
	}

	CString tab ("\t");
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::TABULATOR, tab);
	CString str;
	int size = ((CRscParamBase*) wtItem)->GetTextLength (tab);
	LPTSTR buf = str.GetBuffer (size);
	((CRscParamBase*) wtItem)->GetText (buf, tab);
	str.ReleaseBuffer (size);
	size = (size + 1) * sizeof (TCHAR);
	HGLOBAL data1 = ::GlobalAlloc (GMEM_MOVEABLE, size);
	if (data1 == NULL)
	{
		return;
	}
	LPVOID ptr = ::GlobalLock (data1);
	if (ptr == NULL)
	{
		::GlobalFree (data1);
		return;
	}
	CopyMemory (ptr, str, size);
	::GlobalUnlock (data1);

	size = ((CRscParamBase*) wtItem)->GetFullTextLength (tab);
	buf = str.GetBuffer (size);
	((CRscParamBase*) wtItem)->GetFullText (buf, tab);
	str.ReleaseBuffer (size);
	CString dir;
	if (((CRscParamBase*) wtItem)->_parent != NULL)
	{
		((CRscParamBase*) wtItem)->_parent->GetFullName (dir);
	}
	str = dir + _T (",") + ((CRscParamBase*) wtItem)->_name + _T (";\r\n") + str;
	size = (str.GetLength () + 1) * sizeof (TCHAR);
	HGLOBAL data2 = ::GlobalAlloc (GMEM_MOVEABLE, size);
	if (data2 == NULL)
	{
		::GlobalFree (data1);
		return;
	}
	ptr = ::GlobalLock (data2);
	if (ptr == NULL)
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		return;
	}
	CopyMemory (ptr, str, size);
	::GlobalUnlock (data2);

	UINT cfClass = RegisterClipboardFormat (_T ("class.rsceditor.bi"));
	UINT cfAttr = RegisterClipboardFormat (_T ("attribute.rsceditor.bi"));

	COleDataSource src;
	src.CacheGlobalData (CF_TEXT, data1);
	src.CacheGlobalData (((CRscParamBase*) wtItem)->IsClass () ? cfClass : cfAttr,
		data2);

	_view->_dragItem = pNMTreeView->itemNew.hItem;
	_view->_dragSpaceCtrl = this;
	if (src.DoDragDrop () == DROPEFFECT_MOVE && 
		_view->_dragItem != NULL)
	{
		/*
		CRscWorkTreeItem *wti = (CRscWorkTreeItem*) GetItemData (_view->_dragItem);
		if (wti != NULL &&
			wti->IsParam ())
		{
			_view->GetDocument ()->DeleteParam ((CRscParamBase*) wti);
		}
		*/
	}
	_view->_dragItem = NULL;
	_view->_dragSpaceCtrl = NULL;
	src.Empty ();
}

BOOL CRscClassSpace::ProcessParamDropInWorkSpace (CString &fullCode, bool isClass, DROPEFFECT dropEffect)
{
	CRscWorkTreeItem *dropParam = NULL;
	if (_lastInsertMarkItem != NULL)
	{
		dropParam = (CRscWorkTreeItem*) GetItemData (_lastInsertMarkItem);
	}

	CRscWorkTreeItem *dragParam = NULL;
	if (_view->_dragItem != NULL)
	{
		dragParam = (CRscWorkTreeItem*) _view->_dragSpaceCtrl->GetItemData (_view->_dragItem);
	}

	if (dragParam != NULL && dragParam->IsParam () &&
		(dropParam == NULL || (dropParam->IsParam () && ((CRscParamBase*) dropParam)->IsDropable (_lastInsertMarkAfter))))
	{
		// move or copy or derive parameter to the new position
		CRscEditorDoc *doc = _view->GetDocument ();
		bool after = _lastInsertMarkAfter;
		if (dropParam == NULL)
		{
			int size = doc->_params.GetSize ();
			if (size > 0)
			{
				dropParam = doc->_params[size - 1];
			}
			after = true;
		}
		CRscClass *in = (dropParam != NULL ? ((CRscParamBase*) dropParam)->_parent : NULL);
		CRscParamBase *from = (CRscParamBase*) dropParam;
		if (!after)
		{
			from = doc->FindPrevParam (in, (CRscParamBase*) dropParam);
		}
		
		if (dropEffect == DROPEFFECT_LINK &&
			((CRscParamBase*) dragParam)->IsClass () &&
			(!((CRscClass*) dragParam)->IsIncluded () || ((CRscClass*) dragParam)->_parent == NULL) &&
			doc->FindBaseClassDefinition (((CRscClass*) dragParam)->_name, from, dropParam != NULL ? ((CRscParamBase*) dropParam)->_parent : NULL, false, true) == dragParam)
		{
			// derive
			if (doc->OpenCommand (_T ("New Derived Class"), true, UNDO_NEW_DERIVED_CLASS))
			{
				CRscClass *nc = doc->NewDerivedClass ((CRscClass*) dragParam, dropParam != NULL ? ((CRscParamBase*) dropParam)->_parent : NULL, (CRscParamBase*) dropParam, true, false);
				if (nc == NULL)
				{
					doc->DisposeCommand ();
					return FALSE;
				}
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
				doc->CloseCommand ();
			}
		}
		else if (dropEffect == DROPEFFECT_COPY || ((CRscParamBase*) dragParam)->IsIncluded ())
		{
			// copy
			if (doc->OpenCommand (_T ("Copy Class"), true, UNDO_COPY_CLASS))
			{
				CRscParamBase *op;
				if (!doc->MoveCopyParam ((CRscParamBase*) dragParam, in, (CRscParamBase*) dropParam, fullCode, after, false, isClass, op))
				{
					doc->DisposeCommand ();
					return FALSE;
				}
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
				doc->CloseCommand ();
			}
		}
		else
		{
			// move
			if (doc->OpenCommand (_T ("Move Class"), true, UNDO_MOVE_CLASS))
			{
				CRscParamBase *op;
				if (!doc->MoveCopyParam ((CRscParamBase*) dragParam, in, (CRscParamBase*) dropParam, fullCode, after, true, isClass, op))
				{
					doc->DisposeCommand ();
					return FALSE;
				}
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
				doc->CloseCommand ();
			}
		}
		return TRUE;
	}
	else if (dropParam != NULL &&
		dropParam->IsParam () && ((CRscParamBase*) dropParam)->IsDropable (_lastInsertMarkAfter))
	{
		// insert new param from other application
		CRscEditorDoc *doc = _view->GetDocument ();
		if (doc->OpenCommand (_T ("Insert Class"), true, UNDO_INSERT_CLASS))
		{
			CRscParamBase *op;
			doc->MoveCopyParam (NULL, ((CRscParamBase*) dropParam)->_parent, 
				(CRscParamBase*) dropParam, fullCode, _lastInsertMarkAfter, false, isClass, op);
			doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			doc->CloseCommand ();
			return TRUE;
		}
		doc->DisposeCommand ();
	}
	else if (dropParam == NULL)
	{
		// insert new param at the end from other application
		CRscEditorDoc *doc = _view->GetDocument ();
		if (doc->OpenCommand (_T ("Insert Class"), true, UNDO_INSERT_CLASS))
		{
			CRscParamBase *op;
			if (doc->InsertParam (fullCode, isClass, op))
			{
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
				doc->CloseCommand ();
				return TRUE;
			}
			doc->DisposeCommand ();
		}
	}
	return FALSE;
}

void CRscClassSpace::OnSelChanged(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;

	*pResult = 0;
	HTREEITEM item = GetSelectedItem ();
	if (item == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wti = (CRscWorkTreeItem*) GetItemData (item);
	if (wti == NULL ||
		!wti->IsParam ())
	{
		return;
	}

	_view->UpdateClassEditor ((CRscParamBase*) wti, false);
	CRect r;
	if (GetItemRect (item, r, TRUE))
	{
		InvalidateRect (r, FALSE);
	}
}

void CRscClassSpace::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	UINT flags;
	HTREEITEM item = HitTest (point, &flags);
	if (item != NULL)
	{
		if ((flags & TVHT_ONITEM) != 0)
		{
			CRscWorkTreeItem *wti = (CRscWorkTreeItem*) GetItemData (item);
			if (wti != NULL &&
				wti->IsParam ())
			{
				_view->LaunchClassEditor ((CRscParamBase*) wti, false);
				return;
			}
		}
	}

	CTreeCtrl::OnLButtonDblClk(nFlags, point);
}

void CRscClassSpace::OnDestroy() 
{
	CTreeCtrl::OnDestroy();
	
	if (_dropTarget._control != NULL)
	{
		_dropTarget.Revoke ();
		_dropTarget._control = NULL;
	}
}

void CRscClassSpace::OnEditCopy() 
{
	HTREEITEM selItem = GetSelectedItem ();
	if (selItem == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) GetItemData (selItem);
	if (wtItem == NULL ||
		!wtItem->IsParam ())
	{
		return;
	}

	CString tab ("\t");
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::TABULATOR, tab);
	CString str;
	int size = ((CRscParamBase*) wtItem)->GetTextLength (tab);
	LPTSTR buf = str.GetBuffer (size);
	((CRscParamBase*) wtItem)->GetText (buf, tab);
	str.ReleaseBuffer (size);
	size = (size + 1) * sizeof (TCHAR);
	HGLOBAL data1 = ::GlobalAlloc (GMEM_MOVEABLE, size);
	if (data1 == NULL)
	{
		return;
	}
	LPVOID ptr = ::GlobalLock (data1);
	if (ptr == NULL)
	{
		::GlobalFree (data1);
		return;
	}
	CopyMemory (ptr, str, size);
	::GlobalUnlock (data1);

	size = ((CRscParamBase*) wtItem)->GetFullTextLength (tab);
	buf = str.GetBuffer (size);
	((CRscParamBase*) wtItem)->GetFullText (buf, tab);
	str.ReleaseBuffer (size);
	CString dir;
	if (((CRscParamBase*) wtItem)->_parent != NULL)
	{
		((CRscParamBase*) wtItem)->_parent->GetFullName (dir);
	}
	str = dir + _T (",") + ((CRscParamBase*) wtItem)->_name + _T (";\r\n") + str;
	size = (str.GetLength () + 1) * sizeof (TCHAR);
	HGLOBAL data2 = ::GlobalAlloc (GMEM_MOVEABLE, size);
	if (data2 == NULL)
	{
		::GlobalFree (data1);
		return;
	}
	ptr = ::GlobalLock (data2);
	if (ptr == NULL)
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		return;
	}
	CopyMemory (ptr, str, size);
	::GlobalUnlock (data2);

	UINT cfClass = RegisterClipboardFormat (_T ("class.rsceditor.bi"));
	UINT cfAttr = RegisterClipboardFormat (_T ("attribute.rsceditor.bi"));

	if (!OpenClipboard ())
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		return;
	}

	if (!::EmptyClipboard ())
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		::CloseClipboard ();
		return;
	}

	if (::SetClipboardData (CF_TEXT, data1) == NULL)
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		::CloseClipboard ();
		return;
	}

	if (::SetClipboardData (((CRscParamBase*) wtItem)->IsClass () ? cfClass : cfAttr, data2) == NULL)
	{
		::GlobalFree (data2);
		::CloseClipboard ();
		return;
	}

	::CloseClipboard ();
	return;
}

void CRscClassSpace::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	HTREEITEM selItem = GetSelectedItem ();
	if (selItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) GetItemData (selItem);
	if (wtItem == NULL ||
		!wtItem->IsParam ())
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}

void CRscClassSpace::OnEditCut() 
{
	HTREEITEM selItem = GetSelectedItem ();
	if (selItem == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) GetItemData (selItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () ||
		!((CRscParamBase*) wtItem)->IsEditable ())
	{
		// I can not remove class from macro call
		return;
	}

	CString tab ("\t");
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::TABULATOR, tab);
	CString str;
	int size = ((CRscParamBase*) wtItem)->GetTextLength (tab);
	LPTSTR buf = str.GetBuffer (size);
	((CRscParamBase*) wtItem)->GetText (buf, tab);
	str.ReleaseBuffer (size);
	size = (size + 1) * sizeof (TCHAR);
	HGLOBAL data1 = ::GlobalAlloc (GMEM_MOVEABLE, size);
	if (data1 == NULL)
	{
		return;
	}
	LPVOID ptr = ::GlobalLock (data1);
	if (ptr == NULL)
	{
		::GlobalFree (data1);
		return;
	}
	CopyMemory (ptr, str, size);
	::GlobalUnlock (data1);

	size = ((CRscParamBase*) wtItem)->GetFullTextLength (tab);
	buf = str.GetBuffer (size);
	((CRscParamBase*) wtItem)->GetFullText (buf, tab);
	str.ReleaseBuffer (size);
	CString dir;
	if (((CRscParamBase*) wtItem)->_parent != NULL)
	{
		((CRscParamBase*) wtItem)->_parent->GetFullName (dir);
	}
	str = dir + _T (",") + ((CRscParamBase*) wtItem)->_name + _T (";\r\n") + str;
	size = (str.GetLength () + 1) * sizeof (TCHAR);
	HGLOBAL data2 = ::GlobalAlloc (GMEM_MOVEABLE, size);
	if (data2 == NULL)
	{
		::GlobalFree (data1);
		return;
	}
	ptr = ::GlobalLock (data2);
	if (ptr == NULL)
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		return;
	}
	CopyMemory (ptr, str, size);
	::GlobalUnlock (data2);

	UINT cfClass = RegisterClipboardFormat (_T ("class.rsceditor.bi"));
	UINT cfAttr = RegisterClipboardFormat (_T ("attribute.rsceditor.bi"));

	if (!OpenClipboard ())
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		return;
	}

	if (!::EmptyClipboard ())
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		::CloseClipboard ();
		return;
	}

	if (::SetClipboardData (CF_TEXT, data1) == NULL)
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		::CloseClipboard ();
		return;
	}

	if (::SetClipboardData (((CRscParamBase*) wtItem)->IsClass () ? cfClass : cfAttr, data2) == NULL)
	{
		::GlobalFree (data2);
		::CloseClipboard ();
		return;
	}

	::CloseClipboard ();

	CRscEditorDoc *doc = _view->GetDocument ();
	if (doc->OpenCommand (_T ("Cut"), true, UNDO_CUT))
	{
		if (((CRscParamBase*) wtItem)->IsClass ())
		{
			doc->AddCommandEntry (new CUndoSelectClass ((CRscClass*) wtItem, false, ((CRscClass*) wtItem)->_selected));
			doc->DeleteItem ((CRscClass*) wtItem, &_view->_preview->_itemGroupTags[0], false);
		}
		doc->DeleteParam ((CRscParamBase*) wtItem);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
	return;
}

void CRscClassSpace::OnUpdateEditCut(CCmdUI* pCmdUI) 
{
	HTREEITEM selItem = GetSelectedItem ();
	if (selItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) GetItemData (selItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () ||
		!((CRscParamBase*) wtItem)->IsEditable ())
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}

void CRscClassSpace::OnEditPaste() 
{
	UINT cfClass = RegisterClipboardFormat (_T ("class.rsceditor.bi"));
	UINT cfAttr = RegisterClipboardFormat (_T ("attribute.rsceditor.bi"));

	bool isClass = (::IsClipboardFormatAvailable (cfClass) != FALSE ? true : false);
	if (!isClass && !::IsClipboardFormatAvailable (cfAttr))
	{
		return;
	}

	if (!OpenClipboard ())
	{
		return;
	}
	
	HANDLE data = ::GetClipboardData (isClass ? cfClass : cfAttr);
	if (data == NULL)
	{
		::CloseClipboard ();
		return;
	}
	
	LPVOID ptr = ::GlobalLock (data);
	if (data == NULL)
	{
		::CloseClipboard ();
		return;
	}
	
	CString fullCode;
	fullCode = (LPCTSTR) ptr;
	::GlobalUnlock (data);
	::CloseClipboard ();
	
	bool after = false;
	CRscParamBase *pasteParam = NULL;
	if (_view->_curSpaceCtrl == this)
	{
		HTREEITEM selItem = GetSelectedItem ();
		if (selItem != NULL)
		{
			CRscWorkTreeItem *wti = (CRscWorkTreeItem*) GetItemData (selItem);
			if (wti != NULL &&
				wti->IsParam () && ((CRscParamBase*) wti)->IsDropable (true))
			{
				pasteParam = (CRscParamBase*) wti;
			}
		}
	}
	if (pasteParam == NULL)
	{
		CRscEditorDoc *doc = _view->GetDocument ();
		int size = doc->_params.GetSize ();
		if (size > 0)
		{
			pasteParam = doc->_params[size - 1];
			after = true;
		}
	}
	CRscClass *in = (pasteParam != NULL ? pasteParam->_parent : NULL);

	CRscEditorDoc *doc = _view->GetDocument ();
	if (doc->OpenCommand (_T ("Paste Class"), true, UNDO_PASTE_CLASS))
	{
		CRscParamBase *op;
		if (doc->MoveCopyParam (NULL, in, pasteParam, fullCode, after, false, isClass, op))
		{
			_view->SelectItemClass (op);
			doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			doc->CloseCommand ();
		}
		else
		{
			doc->DisposeCommand ();
		}
	}
}

void CRscClassSpace::OnUpdateEditPaste(CCmdUI* pCmdUI) 
{
	UINT cfClass = RegisterClipboardFormat (_T ("class.rsceditor.bi"));
	UINT cfAttr = RegisterClipboardFormat (_T ("attribute.rsceditor.bi"));

	if (!::IsClipboardFormatAvailable (cfClass) && !::IsClipboardFormatAvailable (cfAttr))
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
	return;
}

void CRscClassSpace::OnLButtonDown(UINT nFlags, CPoint point) 
{
	UINT flags;
	_itemClick = HitTest (point, &flags);
	if (_itemClick != NULL && (flags & TVHT_ONITEM) != 0)
	{
		SelectItem (_itemClick);
	}
	if ((flags & TVHT_ONITEMLABEL) == 0)
	{
		_itemClick = NULL;
	}

	CTreeCtrl::OnLButtonDown(nFlags, point);
}

void CRscClassSpace::OnClick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	*pResult = 0;

	HTREEITEM tcItem = GetSelectedItem ();
	if (tcItem != NULL)
	{
		CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) GetItemData (tcItem);
		if (wtItem != NULL &&
			wtItem->IsParam () &&
			((CRscParamBase*) wtItem)->IsClass () &&
			((CRscClass*) wtItem)->_displayed)
		{
			CRscEditorDoc *doc = _view->GetDocument ();
			doc->SelectAllClasses (false);
			doc->SelectClass ((CRscClass*) wtItem, true);

			_view->_preview->_workViewCtrl.Invalidate (FALSE);
			_view->_preview->_workViewCtrl.UpdateWindow ();
			return;
		}
	}

	if (_itemClick != NULL)
	{
		CRscWorkTreeItem *wti = (CRscWorkTreeItem*) GetItemData (_itemClick);
		if (wti != NULL &&
			wti->IsParam () &&
			((CRscParamBase*) wti)->IsClass ())
		{
			CRscEditorDoc *doc = _view->GetDocument ();
			if (doc->_curUndoType != UNDO_SET_MONITOR_CLASS)
			{
				if (!doc->OpenCommand (_T ("Set Monitor Class"), true, UNDO_SET_MONITOR_CLASS))
				{
					return;
				}
			}

			if (doc->SetMonitorClass ((CRscClass*) wti))
			{
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
				// doc->CloseCommand ();
			}
		}
	}
}

void CRscClassSpace::OnPaint() 
{
	// Do not call CTreeCtrl::OnPaint() for painting messages
	
	CTreeCtrl::OnPaint ();
	{
		CClientDC dc (this);
		CRscEditorDoc *doc = _view->GetDocument ();
		if (doc->_monitorClass != NULL)
		{
			CRect r;
			if (GetItemRect (doc->_monitorClass->_treeItem, r, TRUE))
			{
				CBrush br;
				if (br.CreateSolidBrush (RGB (0, 128, 192)))
				{
					dc.FrameRect (r, &br);
					r.DeflateRect (1, 1);
					dc.FrameRect (r, &br);
				}
			}
		}
	}
}
