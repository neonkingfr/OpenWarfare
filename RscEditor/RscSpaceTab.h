
#if !defined(AFX_RSCSPACETAB_H__60E5E9F8_D991_4407_A52E_37E1E164F956__INCLUDED_)
#define AFX_RSCSPACETAB_H__60E5E9F8_D991_4407_A52E_37E1E164F956__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CRscEditorView;

//! Tab to switch among item groups (objects/controls/background controls).
class CRscSpaceTab : public CTabCtrl
{
public:
  //! Parent view.
	CRscEditorView              *_view;

  //! Auxiliary object for drag&drop.
	CDropTarget<CRscSpaceTab>		_dropTarget;

  //! Last mouse drag time.
	DWORD							          _dragTime;

  //! Last mouse drag point.
	CPoint							        _dragPoint;

public:
	CRscSpaceTab();
	virtual ~CRscSpaceTab();

  //! Method of drag and drop event.
	void DropRegister ();

  //! Method of drag and drop event.
	DROPEFFECT OnDragEnter(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);

  //! Method of drag and drop event.
	DROPEFFECT OnDragOver(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);

  //! Method of drag and drop event.
	void OnDragLeave(CWnd *pWnd);

  //! Method of drag and drop event.
	BOOL OnDrop (CWnd *pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);

	//{{AFX_VIRTUAL(CRscSpaceTab)
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CRscSpaceTab)
	afx_msg void OnSelChange(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RSCSPACETAB_H__60E5E9F8_D991_4407_A52E_37E1E164F956__INCLUDED_)
