#if !defined(AFX_RSCWORKTAB_H__F2BE0F5E_6F4F_429D_9F77_28D94F00E83C__INCLUDED_)
#define AFX_RSCWORKTAB_H__F2BE0F5E_6F4F_429D_9F77_28D94F00E83C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CRscEditorPreview;

//! Tab to switch among work spaces (classes/favourites/included).
class CRscWorkTab : public CTabCtrl
{
public:
  //! Preview pane.
	CRscEditorPreview           *_preview;

  //! Auxiliary object for drag&drop.
	CDropTarget<CRscWorkTab>		_dropTarget;

  //! Last mouse drag time.
	DWORD							          _dragTime;

  //! Last mouse drag point.
	CPoint							        _dragPoint;

public:
	CRscWorkTab();
	virtual ~CRscWorkTab();

  //! Method of drag and drop event.
	void DropRegister ();

  //! Method of drag and drop event.
	DROPEFFECT OnDragEnter(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);

  //! Method of drag and drop event.
	DROPEFFECT OnDragOver(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);

  //! Method of drag and drop event.
	void OnDragLeave(CWnd *pWnd);

  //! Method of drag and drop event.
	BOOL OnDrop (CWnd *pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);

	//{{AFX_VIRTUAL(CRscWorkTab)
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CRscWorkTab)
	afx_msg void OnSelChange(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RSCWORKTAB_H__F2BE0F5E_6F4F_429D_9F77_28D94F00E83C__INCLUDED_)
