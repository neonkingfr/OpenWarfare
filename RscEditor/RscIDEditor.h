
#if !defined(AFX_RSCIDEDITOR_H__89029E66_1A3B_4ABD_9B3F_66CA9ED1DA34__INCLUDED_)
#define AFX_RSCIDEDITOR_H__89029E66_1A3B_4ABD_9B3F_66CA9ED1DA34__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//! Editor of macros in the document.
class CRscMacroEditor : public CDialog
{
public:
	CArray<CRscMacro*, CRscMacro*>		_tempMacros;

  //! Dirty flag.
	bool							_codeEdited;

  //! Document.
	CRscEditorDoc					*_doc;

  //! Used font.
	CFont							*_font;

  //! Is CTRL key down?
	bool							_ctrlDown;

	//{{AFX_DATA(CRscMacroEditor)
	enum { IDD = IDD_RSCIDEDITOR_DIALOG };
	CEdit	_idsTextCtrl;
	//}}AFX_DATA

	CRscMacroEditor(CRscEditorDoc *doc, CWnd* pParent = NULL);   // standard constructor
	~CRscMacroEditor();   // standard destructor

	//{{AFX_VIRTUAL(CRscMacroEditor)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	bool Compile ();

protected:
	//{{AFX_MSG(CRscMacroEditor)
	afx_msg void OnChangeIDsText();
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RSCIDEDITOR_H__89029E66_1A3B_4ABD_9B3F_66CA9ED1DA34__INCLUDED_)
