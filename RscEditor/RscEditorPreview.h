
#if !defined(AFX_RSCEDITORPREVIEW_H__7656E1CE_3DA1_47FF_B93B_783B6625FDF9__INCLUDED_)
#define AFX_RSCEDITORPREVIEW_H__7656E1CE_3DA1_47FF_B93B_783B6625FDF9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "RscWorkView.h"
#include "RscWorkTab.h"

class CRscEditorView;

//! Preview pane.
class CRscEditorPreview : public CFormView
{
protected:
	CRscEditorPreview();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CRscEditorPreview)

public:
	//{{AFX_DATA(CRscEditorPreview)
	enum { IDD = IDD_RSCPREVIEW_FORM };
	CStatic	_controlTabCtrlRect;
	CStatic	_workViewCtrlRect;
	CStatic	_itemLabel;
	//}}AFX_DATA

  //! Parent view.
	CRscEditorView			*_view;

  //! Flags for item groups to be displayed or not.
	bool								_itemGroupTags[rscEditorItemGroupNumber];

  //! Selected item group.
	int									_itemGroup;

  //! Drawing area.
	CRscWorkView				_workViewCtrl;

  //! Control to switch among item groups.
	CRscWorkTab					_controlTabCtrl;

public:
	CRscEditorView *GetView ();

	//{{AFX_VIRTUAL(CRscEditorPreview)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

protected:
	virtual ~CRscEditorPreview();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	//{{AFX_MSG(CRscEditorPreview)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RSCEDITORPREVIEW_H__7656E1CE_3DA1_47FF_B93B_783B6625FDF9__INCLUDED_)
