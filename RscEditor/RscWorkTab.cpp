
#include "stdafx.h"
#include "rsceditor.h"

#include "RscEditorItems.h"
#include "RscWorkTab.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"
#include "RscEditorView.h"
#include "RscEditorPreview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CRscWorkTab::CRscWorkTab()
: _preview (NULL)
{
}

CRscWorkTab::~CRscWorkTab()
{
}


BEGIN_MESSAGE_MAP(CRscWorkTab, CTabCtrl)
	//{{AFX_MSG_MAP(CRscWorkTab)
	ON_NOTIFY_REFLECT(TCN_SELCHANGE, OnSelChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CRscWorkTab::DropRegister ()
{
	if (_dropTarget._control == NULL)
	{
		_dropTarget._control = this;
		_dropTarget.Register (this);
	}
}

DROPEFFECT CRscWorkTab::OnDragEnter(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point)
{
	return OnDragOver(pWnd, pDataObject, dwKeyState, point);
}

DROPEFFECT CRscWorkTab::OnDragOver(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point) 
{
	if (_dragPoint == point)
	{
		TCHITTESTINFO hitInfo;
		hitInfo.pt = point;
		int hit = HitTest (&hitInfo);
		if (hit >= 0)
		{
			DWORD ct = timeGetTime ();
			if (ct - _dragTime > 500)
			{
				SetCurSel (hit);
				_preview->GetView ()->SetControlTab (hit);
				_dragTime = ct;
			}
		}
	}
	else
	{
		_dragPoint = point;
		_dragTime = timeGetTime ();
	}

	return DROPEFFECT_NONE;
}

void CRscWorkTab::OnDragLeave(CWnd *pWnd) 
{
}

BOOL CRscWorkTab::OnDrop (CWnd *pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point)
{
	ASSERT_VALID(this);

	// clean up focus rect
	OnDragLeave(pWnd);

	return FALSE;
}

void CRscWorkTab::OnSelChange(NMHDR* pNMHDR, LRESULT* pResult) 
{
	*pResult = 0;

	_preview->GetView ()->SetControlTab (GetCurSel ());
}
