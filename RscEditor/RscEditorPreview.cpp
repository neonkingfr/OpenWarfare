
#include "stdafx.h"
#include "RscEditor.h"
#include "RscEditorItems.h"
#include "RscMacro.h"
#include "RscEditorDoc.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"
#include "RscEditorView.h"
#include "RscEditorPreview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CRscEditorPreview, CFormView)

CRscEditorPreview::CRscEditorPreview()
	: CFormView(CRscEditorPreview::IDD), _view (NULL), 
	_itemGroup (rscEditorItemGroupControls)
{
	//{{AFX_DATA_INIT(CRscEditorPreview)
	//}}AFX_DATA_INIT

	for (int i = 0; i < rscEditorItemGroupNumber; ++i)
	{
		_itemGroupTags[i] = true;
	}
	_workViewCtrl._preview = this;
	_controlTabCtrl._preview = this;
}

CRscEditorPreview::~CRscEditorPreview()
{
}

void CRscEditorPreview::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRscEditorPreview)
	DDX_Control(pDX, IDC_CONTROLTABRECT, _controlTabCtrlRect);
	DDX_Control(pDX, IDC_WORKVIEWRECT, _workViewCtrlRect);
	DDX_Control(pDX, IDC_ITEMLABEL, _itemLabel);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRscEditorPreview, CFormView)
	//{{AFX_MSG_MAP(CRscEditorPreview)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG
//! Diagnostics.
void CRscEditorPreview::AssertValid() const
{
	CFormView::AssertValid();
}

//! Diagnostics.
void CRscEditorPreview::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

void CRscEditorPreview::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	RECT r;
	if (!::IsWindow (_workViewCtrl.m_hWnd))
	{
		_workViewCtrlRect.GetWindowRect (&r);
		ScreenToClient (&r);
		_workViewCtrl.Create (NULL, "", WS_CHILD | WS_VISIBLE, r, this, IDC_WORKVIEW, NULL);
	}

	if (!::IsWindow (_controlTabCtrl.m_hWnd))
	{
		_controlTabCtrlRect.GetWindowRect (&r);
		ScreenToClient (&r);
		_controlTabCtrl.Create (WS_CHILD | WS_VISIBLE | TCS_RIGHTJUSTIFY | TCS_MULTILINE | TCS_BOTTOM, r, 
			this, IDC_CONTROLTAB);
		_controlTabCtrl.SetFont (_view->_spaceTabCtrl.GetFont ());
	}

	_workViewCtrl.DropRegister ();
	_controlTabCtrl.DropRegister ();

	_controlTabCtrl.DeleteAllItems ();
	for (int i = 0; i < rscEditorItemGroupNumber; ++i)
	{
		_controlTabCtrl.InsertItem (i, CRscEditorView::_itemGroups[i]);
	}
	_controlTabCtrl.SetCurSel (_itemGroup);

	GetClientRect (&r);
	OnSize (0, r.right, r.bottom);
}

CRscEditorView *CRscEditorPreview::GetView ()
{
	return _view;
}

void CRscEditorPreview::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	
	if (!::IsWindow (_controlTabCtrl.m_hWnd))
	{
		return;
	}
	if (!::IsWindow (_itemLabel.m_hWnd))
	{
		return;
	}
	RECT r1;
	_itemLabel.GetWindowRect (&r1);

	RECT r;
	r.left = UI_SPACE;
	r.right = cx - UI_SPACE;
	r.top = UI_SPACE;
	r.bottom = UI_SPACE + r1.bottom - r1.top;
	_itemLabel.MoveWindow (&r, TRUE);

	r.left = 0;
	r.right = cx;
	r.top = UI_SPACE + r1.bottom - r1.top + UI_SPACE;
	r.bottom = cy;
	_controlTabCtrl.MoveWindow (&r, TRUE);

	if (!::IsWindow (_workViewCtrl.m_hWnd))
	{
		return;
	}
	_controlTabCtrl.AdjustRect (FALSE, &r);
	_workViewCtrl.MoveWindow (&r, TRUE);

	ShowScrollBar (SB_BOTH, FALSE);
}

void CRscEditorPreview::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
}
