
#include "stdafx.h"
#include "RscEditor.h"
#include "RscEditorItems.h"
#include "RscMacro.h"
#include "RscEditorDoc.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"
#include "RscEditorREDParser.h"
#include "RscEditorView.h"

LPCTSTR const CRscAttribute::_editables[] = 
{
	_T ("idd"),
	_T ("idc"),
	_T ("x"),
	_T ("y"),
	_T ("w"),
	_T ("h"),
};
const int CRscAttribute::_editablesSize = sizeof (CRscAttribute::_editables) / sizeof (CRscAttribute::_editables[0]);

bool CRscClassEnumerator::EnumerateValue (CRscClass *topClass, CRscParamBase *curParam, CRscAttributeValueBase *val, int itemGroup, int recursiveLevel)
{
	if (val->IsArray ())
	{
		int n = ((CRscAttributeValueArray*) val)->_elements.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			if (EnumerateValue (topClass, curParam, ((CRscAttributeValueArray*) val)->_elements[i], itemGroup, recursiveLevel))
			{
				return true;
			}
		}
	} 
	else if (val->IsElement ())
	{
		CRscClass *p = _doc->FindClassDefinition (((CRscAttributeValueElement*) val)->_value, topClass, _useDisabled);
		
		if (p != NULL)
		{
			_curItemGroup = itemGroup;
			if (EnumerateClass (p, (CRscAttributeValueElement*) val, recursiveLevel + 1))
			{
				return true;
			}
		}
	}
	
	return false;
}

bool CRscClassEnumerator::EnumerateClass (CRscClass *topClass, CRscAttributeValueElement *val, int recursiveLevel)
{
	if (topClass == NULL)
	{
		return false;
	}

	_readOnly = false;
	bool readOnlyPart;
	if (topClass->GetFloatParamInBases ("x", _x, true, &readOnlyPart, _useDisabled))
	{
		_readOnly |= readOnlyPart;
		if (topClass->GetFloatParamInBases ("y", _y, true, &readOnlyPart, _useDisabled))
		{
			_readOnly |= readOnlyPart;
			if (topClass->GetFloatParamInBases ("w", _w, true, &readOnlyPart, _useDisabled))
			{
				_readOnly |= readOnlyPart;
				if (topClass->GetFloatParamInBases ("h", _h, true, &readOnlyPart, _useDisabled))
				{
					_readOnly |= readOnlyPart;

					if (!topClass->GetStringParamInBases ("text", _text, true, NULL, _useDisabled))
					{
						_text = _T ("");
					}

					// test rect
					if (EnumerateFce (topClass, val))
					{
						return true;
					}
				}
			}
		}
	}
	
	if (!_recursive || recursiveLevel == _recursiveLimit)
	{
		return false;
	}
	
	for (int i = 0; i < rscEditorItemGroupNumber; ++i)
	{
		if (_tags == NULL || _tags[i])
		{
			CRscParamBase *par = topClass->FindParamInBases (CRscEditorView::_itemGroups[i], true, _useDisabled);
			if (par != NULL)
			{
				if (par->IsAttribute ())
				{
					CRscAttributeValueBase *val = ((CRscAttribute*) par)->_value;
					if (val != NULL)
					{
						if (EnumerateValue (topClass, par, val, i, recursiveLevel))
						{
							return true;
						}
					}
				}
				else if (par->IsClass ())
				{
					_curItemGroup = i;
					CArray<CRscParamBase*, CRscParamBase*> &inArr = ((CRscClass*) par)->_innerParams;
					int inSize = inArr.GetSize ();
					for (int inI = 0; inI < inSize; ++inI)
					{
						CRscParamBase *inP = inArr[inI];
						if (inP->IsClass ())
						{
							if (EnumerateClass ((CRscClass*) inP, val, recursiveLevel + 1))
							{
								return true;
							}
						}
					}
				}
			}
		}
	}
	
	return false;
}

bool CRscClass::SetFloatParamInBases (const char *name, double val, bool inBases, bool useDisabled, CRscEditorDoc *doc, bool canUndo, bool forceUndo)
{
	if (!IsInsideEditable ())
	{
		return false;
	}
	
	CRscParamBase *par = FindParamInBases (name, inBases, useDisabled);
	if (par != NULL && 
		(!par->IsAttribute () || (!par->IsInsideEditable () && par->_parent == this)))
	{
		return false;
	}
	
	if (par == NULL || !par->IsInsideEditable ())
	{
		CString str;
		str.Format (_T ("%g"), val);
		// Macro: don't create attribute inside of the macro!
		CRscAttribute *newParam = new CRscAttribute (name, NULL, false, NULL, 0, NULL);
		if (newParam == NULL)
		{
			return false;
		}
		CRscAttributeValueElement *val = new CRscAttributeValueElement (str, NULL, newParam);
		if (val == NULL)
		{
			delete newParam;
			return false;
		}
		newParam->_value = val;
		newParam->_parent = this;
		int index = _innerParams.GetSize ();
		_innerParams.Add (newParam);
		doc->SetModifiedFlag (TRUE);
		
		if (!doc->_undoMode)
		{
			CUndoNewParam *undo = new CUndoNewParam (newParam, index, false, -1);
			doc->AddCommandEntry (undo);
		}
		
		CUpdateDescriptor dscr;
		dscr._newParam._param = newParam;
		dscr._newParam._inFavourites = false;
		doc->UpdateAllViews (NULL, UH_NEW_PARAM, &dscr);
		return true;
	}
	
	CRscAttributeValueBase *attrVal = ((CRscAttribute*) par)->_value;
	if (attrVal == NULL || !attrVal->IsElement ())
	{
		return false;
	}

	CRscAttributeValueElement *newElem = (CRscAttributeValueElement*) attrVal->Copy (attrVal->_inArray, attrVal->_inParam);
	if (newElem == NULL)
	{
		return false;
	}
	newElem->_value.Format (_T ("%g"), val);
	if (!forceUndo && newElem->_value == ((CRscAttributeValueElement*) attrVal)->_value)
	{
		delete newElem;
		return false;
	}
	((CRscAttribute*) par)->_value = newElem;
	doc->SetModifiedFlag (TRUE);

	if (!doc->_undoMode && canUndo)
	{
		CUndoChangeParamValue *undo = new CUndoChangeParamValue (par, 
			&((CRscAttribute*) par)->_value, attrVal);
		doc->AddCommandEntry (undo);
	}
	else
	{
		delete attrVal;
	}

	CUpdateDescriptor dscr;
	dscr._newValue._param = par;
	doc->UpdateAllViews (NULL, UH_NEW_VALUE, &dscr);
	return true;
}

bool CRscClass::SetArrayStringParamInBases (const char *name, LPCTSTR val, bool inBases, bool useDisabled, CRscEditorDoc *doc, bool canUndo)
{
	if (!IsInsideEditable ())
	{
		return false;
	}
	
	CRscParamBase *par = FindParamInBases (name, inBases, useDisabled);
	if (par != NULL && (!par->IsAttribute () || (!par->IsInsideEditable () && par->_parent == this)))
	{
		return false;
	}
	
	if (par == NULL || !par->IsInsideEditable ())
	{
		// Macro: don't create attribute inside of the macro!
		CRscAttribute *newParam = new CRscAttribute (name, NULL, false, NULL, 0, NULL);
		if (newParam == NULL)
		{
			return false;
		}
		CString str;
		CRscAttributeValueArray *newVal = new CRscAttributeValueArray (NULL, newParam);
		if (newVal == NULL)
		{
			delete newParam;
			return false;
		}
		CRscAttributeValueElement *newElem = new CRscAttributeValueElement (val, newVal, newParam);
		if (newElem == NULL)
		{
			delete newVal;
			delete newParam;
			return false;
		}
		newVal->_elements.Add (newElem);

		newParam->_value = newVal;
		newParam->_parent = this;
		int index = _innerParams.GetSize ();
		_innerParams.Add (newParam);
		doc->SetModifiedFlag (TRUE);
		
		if (!doc->_undoMode)
		{
			CUndoNewParam *undo = new CUndoNewParam (newParam, index, false, -1);
			doc->AddCommandEntry (undo);
		}
		
		CUpdateDescriptor dscr;
		dscr._newParam._param = newParam;
		dscr._newParam._inFavourites = false;
		doc->UpdateAllViews (NULL, UH_NEW_PARAM, &dscr);
		return true;
	}
	
	CRscAttributeValueBase *attrVal = ((CRscAttribute*) par)->_value;
	if (attrVal == NULL || !attrVal->IsArray ())
	{
		return false;
	}
	
	CRscAttributeValueElement *newElem = new CRscAttributeValueElement (val, (CRscAttributeValueArray*) attrVal, ((CRscAttributeValueArray*) attrVal)->_inParam);
	if (newElem == NULL)
	{
		return false;
	}
	int index = ((CRscAttributeValueArray*) attrVal)->_elements.GetSize ();
	((CRscAttributeValueArray*) attrVal)->_elements.Add (newElem);
	doc->SetModifiedFlag (TRUE);
	
	if (!doc->_undoMode && canUndo)
	{
		CUndoAddParamValue *undo = new CUndoAddParamValue (par, 
			&((CRscAttributeValueArray*) attrVal)->_elements,
			index, newElem);
		doc->AddCommandEntry (undo);
	}

	CUpdateDescriptor dscr;
	dscr._newValue._param = par;
	doc->UpdateAllViews (NULL, UH_NEW_VALUE, &dscr);
	return true;
}

void CRscGroup::Draw (CDC &dc, RECT *view, bool drawDragBoxes)
{
	double x, y, w, h;
	bool bx, by, bw, bh;
	GetXYWH (x, y, w, h, bx, by, bw, bh);
	if (bx && by && bh && bw)
	{
		RECT r;
		double dx = (double) (view->right - view->left - 1);
		double dy = (double) (view->bottom - view->top - 1);
		CALC_CTRL_RECT (r, view->left, view->top, dx, dy, x, y, w, h)
			
		CPen p (PS_SOLID, 1, RGB (255, 128, 0));
		dc.SelectObject (&p);
		dc.SelectStockObject (NULL_BRUSH);
		dc.Rectangle (&r);
		
		if (drawDragBoxes)
		{
			CBrush br (RGB (255, 192, 96));
			dc.SelectObject (&br);
			RECT rr;
			CRect rf;
			DRAW_DRAG_BOX ((&dc), r.left, r.top, view, rr, rf)
			DRAW_DRAG_BOX ((&dc), r.right - 1, r.top, view, rr, rf)
			DRAW_DRAG_BOX ((&dc), r.left, r.bottom - 1, view, rr, rf)
			DRAW_DRAG_BOX ((&dc), r.right - 1, r.bottom - 1, view, rr, rf)
			DRAW_DRAG_BOX ((&dc), (r.left + r.right - 1) >> 1, r.top, view, rr, rf)
			DRAW_DRAG_BOX ((&dc), (r.left + r.right - 1) >> 1, r.bottom - 1, view, rr, rf)
			DRAW_DRAG_BOX ((&dc), r.left, (r.top + r.bottom - 1) >> 1, view, rr, rf)
			DRAW_DRAG_BOX ((&dc), r.right - 1, (r.top + r.bottom - 1) >> 1, view, rr, rf)
		}
	}
}

bool CRscLayoutGroup::SetX (double x, bool inBases, bool canUndo)
{
	if (_bx && x != _x && x >= 0.0 && x <= 1.0)
	{
		double mx;
		int n = _group->_members.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CRscClass *cl = _group->_members[i];
			if (cl->GetFloatParamInBases ("x", mx, true, NULL, false))
			{
				mx = mx + x - _x;
				if (mx < 0.0)
				{
					mx = 0.0;
				}
				if (mx > 1.0)
				{
					mx = 1.0;
				}
				cl->SetFloatParamInBases ("x", mx, inBases, false, _doc, canUndo, false);
			}
		}
		return true;
	}
	return false;
}

bool CRscLayoutGroup::SetY (double y, bool inBases, bool canUndo)
{
	if (_by && y != _y && y >= 0.0 && y <= 1.0)
	{
		double my;
		int n = _group->_members.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CRscClass *cl = _group->_members[i];
			if (cl->GetFloatParamInBases ("y", my, true, NULL, false))
			{
				my = my + y - _y;
				if (my < 0.0)
				{
					my = 0.0;
				}
				if (my > 1.0)
				{
					my = 1.0;
				}
				cl->SetFloatParamInBases ("y", my, inBases, false, _doc, canUndo, false);
			}
		}
		return true;
	}
	return false;
}

bool CRscLayoutGroup::SetW (double w, bool inBases, bool canUndo)
{
	if (_bw && w != _w && w > 0.0)
	{
		double mw, mx;
		int n = _group->_members.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CRscClass *cl = _group->_members[i];
			if (cl->GetFloatParamInBases ("w", mw, true, NULL, false) &&
				cl->GetFloatParamInBases ("x", mx, true, NULL, false))
			{
				mw = mw * w / _w;
				if (mw < 0.0)
				{
					mw = 0.0;
				}
				mx = (mx - _x) * w / _w + _x;
				if (mx < 0.0)
				{
					mx = 0.0;
				}
				if (mx > 1.0)
				{
					mx = 1.0;
				}
				cl->SetFloatParamInBases ("w", mw, inBases, false, _doc, canUndo, false);
				cl->SetFloatParamInBases ("x", mx, inBases, false, _doc, canUndo, false);
			}
		}
		return true;
	}
	return false;
}

bool CRscLayoutGroup::SetH (double h, bool inBases, bool canUndo)
{
	if (_bh && h != _h && h > 0.0)
	{
		double mh, my;
		int n = _group->_members.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CRscClass *cl = _group->_members[i];
			if (cl->GetFloatParamInBases ("h", mh, true, NULL, false) &&
				cl->GetFloatParamInBases ("y", my, true, NULL, false))
			{
				mh = mh * h / _h;
				if (mh < 0.0)
				{
					mh = 0.0;
				}
				my = (my - _y) * h / _h + _y;
				if (my < 0.0)
				{
					my = 0.0;
				}
				if (my > 1.0)
				{
					my = 1.0;
				}
				cl->SetFloatParamInBases ("h", mh, inBases, false, _doc, canUndo, false);
				cl->SetFloatParamInBases ("y", my, inBases, false, _doc, canUndo, false);
			}
		}
		return true;
	}
	return false;
}

void CRscLayoutGroup::TransformDone ()
{
	_group->GetXYWH (_x, _y, _w, _h, _bx, _by, _bw, _bh);
}

void CRscGuide::Draw (CDC &dc, RECT *h, RECT *v, RECT *preview)
{
	CPen *oldPen = dc.GetCurrentPen ();
	CPen p1 (PS_SOLID, 1, RGB (64, 128, 128));
	int coord;
	int n = _horz.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		CALC_GUIDE_POINT (coord, h->left, h->right - h->left - 1, _horz[i])
		dc.SelectStockObject (BLACK_PEN);
		dc.MoveTo (coord, h->top);
		dc.LineTo (coord, h->bottom - 1);
		dc.SelectObject (&p1);
		dc.MoveTo (coord, preview->top);
		dc.LineTo (coord, preview->bottom - 1);
		_horzPxl[i] = coord;
	}
	n = _vert.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		CALC_GUIDE_POINT (coord, v->top, v->bottom - v->top - 1, _vert[i])
		dc.SelectStockObject (BLACK_PEN);
		dc.MoveTo (v->left, coord);
		dc.LineTo (v->right - 1, coord);
		dc.SelectObject (&p1);
		dc.MoveTo (preview->left, coord);
		dc.LineTo (preview->right - 1, coord);
		_vertPxl[i] = coord;
	}
	dc.SelectObject (oldPen);
}

void CRscGuide::HighlightVert (CDC &dc, RECT *v, RECT *preview, int pxl, int &index)
{
	CPen *oldPen = dc.GetCurrentPen ();
	CPen p1 (PS_SOLID, 1, RGB (255, 0, 0));
	CPen p2 (PS_SOLID, 1, RGB (64, 128, 128));
	int mdg = 3;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDE_SNAP, mdg);
	index = -1;
	int n = _vertPxl.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		int td = ::abs (_vertPxl[i] - pxl);
		if (td <= mdg)
		{
			index = i;
			mdg = td;
		}
	}
	for (int i = 0; i < n; ++i)
	{
		if (i == index)
		{
			dc.SelectObject (&p1);
			dc.MoveTo (v->left, _vertPxl[i]);
			dc.LineTo (v->right - 1, _vertPxl[i]);
			dc.MoveTo (preview->left, _vertPxl[i]);
			dc.LineTo (preview->right - 1, _vertPxl[i]);
			index = i;
		}
		else
		{
			dc.SelectStockObject (BLACK_PEN);
			dc.MoveTo (v->left, _vertPxl[i]);
			dc.LineTo (v->right - 1, _vertPxl[i]);
			dc.SelectObject (&p2);
			dc.MoveTo (preview->left, _vertPxl[i]);
			dc.LineTo (preview->right - 1, _vertPxl[i]);
		}
	}
	dc.SelectObject (oldPen);
}

void CRscGuide::HighlightHorz (CDC &dc, RECT *h, RECT *preview, int pxl, int &index)
{
	CPen *oldPen = dc.GetCurrentPen ();
	CPen p1 (PS_SOLID, 1, RGB (255, 0, 0));
	CPen p2 (PS_SOLID, 1, RGB (64, 128, 128));
	int mdg = 3;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDE_SNAP, mdg);
	index = -1;
	int n = _horzPxl.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		int td = ::abs (_horzPxl[i] - pxl);
		if (td <= mdg)
		{
			index = i;
			mdg = td;
		}
	}
	for (int i = 0; i < n; ++i)
	{
		if (i == index)
		{
			dc.SelectObject (&p1);
			dc.MoveTo (_horzPxl[i], h->top);
			dc.LineTo (_horzPxl[i], h->bottom - 1);
			dc.MoveTo (_horzPxl[i], preview->top);
			dc.LineTo (_horzPxl[i], preview->bottom - 1);
			index = i;
		}
		else
		{
			dc.SelectStockObject (BLACK_PEN);
			dc.MoveTo (_horzPxl[i], h->top);
			dc.LineTo (_horzPxl[i], h->bottom - 1);
			dc.SelectObject (&p2);
			dc.MoveTo (_horzPxl[i], preview->top);
			dc.LineTo (_horzPxl[i], preview->bottom - 1);
		}
		dc.MoveTo (_horzPxl[i], h->top);
		dc.LineTo (_horzPxl[i], h->bottom - 1);
		dc.MoveTo (_horzPxl[i], preview->top);
		dc.LineTo (_horzPxl[i], preview->bottom - 1);
	}
	dc.SelectObject (oldPen);
}

bool CRscAttributeValueArray::IsSingleLine (const CString &prefix, LPCTSTR tab) const
{
	int n = _elements.GetSize ();
	if (n > 0)
	{
		for (int i = 0; i < n; ++i)
		{
			if (_elements[i]->_preComments.GetSize () != 0 ||
				_elements[i]->_postComment.GetLength () != 0)
			{
				return false;
			}
		}

		int maxLen = 80;
		((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::ARRAY_TEXT_WRAP_LIMIT, maxLen);
		return GetTextLengthSingleLine (prefix, tab) <= maxLen;
	}

	return _preComments.GetSize () == 0;
}

int CRscAttributeValueArray::GetTextLengthMultiLine (const CString &prefix, LPCTSTR tab) const
{
	int nl = _tcslen (_T ("\r\n"));
	int pl = prefix.GetLength ();
	CString prefix2 = prefix + tab;
	int pl2 = prefix2.GetLength ();

	int rl = nl + pl + _tcslen (_T ("{")) + nl;
	int n = _elements.GetSize ();
	if (n != 0)
	{
		--n;
		for (int i = 0; ; ++i)
		{
			if (i < n)
			{
				rl += pl2 + _elements[i]->GetTextLength (prefix2, tab) + _tcslen (_T (", ")) + 
					_elements[i]->_postComment.GetLength () + nl;
				continue;
			}
			rl += pl2 + _elements[i]->GetTextLength (prefix2, tab) + 
				_elements[i]->_postComment.GetLength () + nl;
			break;
		}
	}
	else
	{
		n = _preComments.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			rl += pl2 + _preComments[i].GetLength () + nl;
		}
	}
	rl += pl + _tcslen (_T ("}"));

	return rl;
}

LPTSTR CRscAttributeValueArray::GetTextMultiLine (LPTSTR text, const CString &prefix, LPCTSTR tab) const
{
	CString prefix2 = prefix + tab;

	_tcscat (text, _T ("\r\n"));
	_tcscat (text, prefix);
	_tcscat (text, _T ("{\r\n"));
	int n = _elements.GetSize ();
	if (n != 0)
	{
		--n;
		for (int i = 0; ; ++i)
		{
			_tcscat (text, prefix2);
			_elements[i]->GetText (text, prefix2, tab);
			if (i < n)
			{
				_tcscat (text, _T (", "));
				_tcscat (text, _elements[i]->_postComment);
				_tcscat (text, _T ("\r\n"));
				continue;
			}
			_tcscat (text, _elements[i]->_postComment);
			_tcscat (text, _T ("\r\n"));
			break;
		}
	}
	else
	{
		n = _preComments.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_tcscat (text, prefix2);
			_tcscat (text, _preComments[i]);
			_tcscat (text, _T ("\r\n"));
		}
	}
	_tcscat (text, prefix);
	_tcscat (text, _T ("}"));
	return text;
}

int CRscAttributeValueArray::GetTextLengthSingleLine (const CString &prefix, LPCTSTR tab) const
{
	int pl = prefix.GetLength ();

	int rl = _tcslen (_T ("{"));
	int n = _elements.GetSize ();
	
	if (n != 0)
	{
		--n;
		for (int i = 0; ; ++i)
		{
			rl += _elements[i]->GetTextLength (prefix, tab);
			if (i < n)
			{
				rl += _tcslen (_T (", "));
				continue;
			}
			break;
		}
	}

	rl += _tcslen (_T ("}"));

	return rl;
}

LPTSTR CRscAttributeValueArray::GetTextSingleLine (LPTSTR text, const CString &prefix, LPCTSTR tab) const
{
	_tcscat (text, _T ("{"));
	int n = _elements.GetSize ();

	if (n != 0)
	{
		--n;
		for (int i = 0; ; ++i)
		{
			_elements[i]->GetText (text, prefix, tab);
			if (i < n)
			{
				_tcscat (text, _T (", "));
				continue;
			}
			break;
		}
	}

	_tcscat (text, _T ("}"));
	return text;
}
