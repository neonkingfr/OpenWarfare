
class CRscEditorLex;

//! Structure describing token.
struct CRscEditorToken
{
  //! Token type.
	int type;

  //! Int value if token is of that type.
	long number;

  //! Float value if token is of that type.
	double fnumber;

  //! String value if token is of that type.
	TCHAR string[1024];
};

//! Abstract class of the parser.
class CRscEditorParser
{
public:
  //! Parse "//" comments. (callback from lexical analyzer)
	virtual bool ParseSlashSlash (CRscEditorLex *lex);

  //! Parse "/*" comments. (callback from lexical analyzer)
	virtual bool ParseSlashStar (CRscEditorLex *lex);

  //! Parse "#" command. (callback from lexical analyzer)
	virtual bool ParseHash (CRscEditorLex *lex, CRscEditorToken *tok, unsigned short flags);

  //! Parse macro. (callback from lexical analyzer)
	virtual bool ProcessMacroId (CRscEditorLex *lex, CRscEditorToken *tok, unsigned short flags);
};
