
#if !defined(AFX_RSCEDITORVIEW_H__55C9EB8D_CD40_4E9A_BCCA_424A45890097__INCLUDED_)
#define AFX_RSCEDITORVIEW_H__55C9EB8D_CD40_4E9A_BCCA_424A45890097__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "RscSpaceTab.h"
#include "ClassEditor.h"
#include "RscInclFileSpace.h"
#include "RscClassSpace.h"
#include "RscFavouriteSpace.h"

//! Code of the drag to box for changing control item (or group of them) properties.
enum
{
	rscEditorDragBoxNone,
	rscEditorDragBoxLeft,
	rscEditorDragBoxTop,
	rscEditorDragBoxRight,
	rscEditorDragBoxBottom,
	rscEditorDragBoxTopLeft,
	rscEditorDragBoxTopRight,
	rscEditorDragBoxBottomLeft,
	rscEditorDragBoxBottomRight,
};

//! Code of the work space (included files/classes/favourites).
enum
{
	rscEditorInclFileSpace = 0,
	rscEditorClassSpace,
	rscEditorFavouriteSpace,
};


class CRscEditorPreview;

//! Main view of the document.
class CRscEditorView : public CFormView
{
protected:
	CRscEditorView(); // create from serialization only
	DECLARE_DYNCREATE(CRscEditorView)

public:
	//{{AFX_DATA(CRscEditorView)
	enum { IDD = IDD_RSCEDITOR_FORM };
	CRscFavouriteSpace	_favouriteSpaceCtrl;
	CRscClassSpace	_classSpaceCtrl;
	CRscInclFileSpace	_inclFileSpaceCtrl;
	CRscSpaceTab	_spaceTabCtrl;
	CStatic	_monitorLabel;
	//}}AFX_DATA

public:
	CRscEditorDoc* GetDocument();

  //! Names of the items groups (objects/controls/background controls).
	static const char			*_itemGroups[rscEditorItemGroupNumber];

  //! Image list of the work spaces.
	CImageList						*_workSpaceImageList;

  //! Instance of the inner class editor.
	CClassEditor					_classEditor;

  //! Current work space tree control.
	CTreeCtrl							*_curSpaceCtrl;

  //! From which work space is item dragged.
	CTreeCtrl							*_dragSpaceCtrl;

  //! Dragged item.
	HTREEITEM							_dragItem;
  
  //! Change attributes in base classes or create copy of them in current item class and then change it.
	bool								  _changeInherited;

  //! Preview pane.
	CRscEditorPreview			*_preview;

  //! Lock X coord.
	bool								  _lockX;

  //! Lock Y coord.
	bool								  _lockY;

  //! Text for find dialog.
	TCHAR								  _findWhat[255];

  //! Text for find dialog.
	TCHAR								  _replaceWith[255];

  //! Standard find dialog.
	CFindReplaceDialog		*_findReplaceDialog;

public:
	void AddFavouriteIntoSpace (HTREEITEM parent, HTREEITEM after, CRscClass *cl);
	void AddClassIntoSpace (HTREEITEM parent, HTREEITEM after, CRscParamBase *par);
	void AddClassesIntoSpace (HTREEITEM parent, HTREEITEM after, CArray<CRscParamBase*, CRscParamBase*> &arr);
	void AddClassesIntoSpace (HTREEITEM parent, HTREEITEM after, CArray<CRscParamBase*, CRscParamBase*> &arr, int from, int to);
	void AddInclFileIntoSpace (HTREEITEM parent, CRscInclude *which);
	void UpdateClassEditor (CRscParamBase *cl, bool inFavourites);
	void UpdateClassEditor ();
	void LaunchClassEditor (CRscParamBase *cl, bool inFavourites);
	void DisplayContexMenu (int n, CPoint &point);
	void SelectItemClass ();
	void SelectItemClass (CRscParamBase *cls, bool inFavourites = false);
	void SetMonitorLabel ();
	void SetItemLabel (CRscClass *cl);
	void SetItemLabel (LPCTSTR str);
	void SetSpaceTab (int tab, bool focus = true);
	int GetImageNumber (CRscWorkTreeItem *wsi);
	void SetControlTab (int i);
	void SelectItemGroup ();
	bool MoveItem (CRscClass *item, double x, double y);
	bool LaunchExternClassEditor (CRscParamBase *param, bool inFavourites);
	void FindNext (DWORD flags, HWND owner);

	//{{AFX_VIRTUAL(CRscEditorView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

public:
	virtual ~CRscEditorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CRscEditorView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClassBase();
	afx_msg void OnUpdateClassBase(CCmdUI* pCmdUI);
	afx_msg void OnClassNewInner();
	afx_msg void OnUpdateClassNewInner(CCmdUI* pCmdUI);
	afx_msg void OnClassNewDerived();
	afx_msg void OnUpdateClassNewDerived(CCmdUI* pCmdUI);
	afx_msg void OnClassFavouritesAdd();
	afx_msg void OnUpdateClassFavouritesAdd(CCmdUI* pCmdUI);
	afx_msg void OnClassFavouritesRemove();
	afx_msg void OnUpdateClassFavouritesRemove(CCmdUI* pCmdUI);
	afx_msg void OnClassSelectOriginal();
	afx_msg void OnUpdateClassSelectOriginal(CCmdUI* pCmdUI);
	afx_msg void OnClassEdit();
	afx_msg void OnUpdateClassEdit(CCmdUI* pCmdUI);
	afx_msg void OnClassSelect();
	afx_msg void OnUpdateClassSelect(CCmdUI* pCmdUI);
	afx_msg void OnClassSelectAdd();
	afx_msg void OnUpdateClassSelectAdd(CCmdUI* pCmdUI);
	afx_msg void OnFileInclude();
	afx_msg void OnLayoutAlignBottom();
	afx_msg void OnUpdateLayoutAlignBottom(CCmdUI* pCmdUI);
	afx_msg void OnLayoutAlignRight();
	afx_msg void OnUpdateLayoutAlignRight(CCmdUI* pCmdUI);
	afx_msg void OnLayoutAlignLeft();
	afx_msg void OnUpdateLayoutAlignLeft(CCmdUI* pCmdUI);
	afx_msg void OnLayoutAlignTop();
	afx_msg void OnUpdateLayoutAlignTop(CCmdUI* pCmdUI);
	afx_msg void OnLayoutAlignHorizCenter();
	afx_msg void OnUpdateLayoutAlignHorizCenter(CCmdUI* pCmdUI);
	afx_msg void OnLayoutAlignVertCenter();
	afx_msg void OnUpdateLayoutAlignVertCenter(CCmdUI* pCmdUI);
	afx_msg void OnLayoutMakeSameSizeWidth();
	afx_msg void OnUpdateLayoutMakeSameSizeWidth(CCmdUI* pCmdUI);
	afx_msg void OnLayoutMakeSameSizeHeight();
	afx_msg void OnUpdateLayoutMakeSameSizeHeight(CCmdUI* pCmdUI);
	afx_msg void OnLayoutMakeSameSizeBoth();
	afx_msg void OnUpdateLayoutMakeSameSizeBoth(CCmdUI* pCmdUI);
	afx_msg void OnClassDelete();
	afx_msg void OnUpdateClassDelete(CCmdUI* pCmdUI);
	afx_msg void OnItemsShowControls();
	afx_msg void OnUpdateItemsShowControls(CCmdUI* pCmdUI);
	afx_msg void OnItemsShowBackgroundControls();
	afx_msg void OnUpdateItemsShowBackgroundControls(CCmdUI* pCmdUI);
	afx_msg void OnItemsEdit();
	afx_msg void OnUpdateItemsEdit(CCmdUI* pCmdUI);
	afx_msg void OnItemsNewCopy();
	afx_msg void OnUpdateItemsNewCopy(CCmdUI* pCmdUI);
	afx_msg void OnItemsNewDerived();
	afx_msg void OnUpdateItemsNewDerived(CCmdUI* pCmdUI);
	afx_msg void OnItemsMakeIndependent();
	afx_msg void OnUpdateItemsMakeIndependent(CCmdUI* pCmdUI);
	afx_msg void OnLayoutCenterInDialogHorizontal();
	afx_msg void OnUpdateLayoutCenterInDialogHorizontal(CCmdUI* pCmdUI);
	afx_msg void OnLayoutCenterInDialogVertical();
	afx_msg void OnUpdateLayoutCenterInDialogVertical(CCmdUI* pCmdUI);
	afx_msg void OnLayoutSpaceEvenlyAcross();
	afx_msg void OnUpdateLayoutSpaceEvenlyAcross(CCmdUI* pCmdUI);
	afx_msg void OnLayoutSpaceEvenlyDown();
	afx_msg void OnUpdateLayoutSpaceEvenlyDown(CCmdUI* pCmdUI);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnItemsShowObjects();
	afx_msg void OnUpdateItemsShowObjects(CCmdUI* pCmdUI);
	afx_msg void OnMonitorLabel();
	afx_msg void OnItemsNew();
	afx_msg void OnUpdateItemsNew(CCmdUI* pCmdUI);
	afx_msg void OnItemsChangeInherited();
	afx_msg void OnUpdateItemsChangeInherited(CCmdUI* pCmdUI);
	afx_msg void OnItemsRemove();
	afx_msg void OnUpdateItemsRemove(CCmdUI* pCmdUI);
	afx_msg void OnItemsDelete();
	afx_msg void OnUpdateItemsDelete(CCmdUI* pCmdUI);
	afx_msg void OnClassNew();
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnEditCut();
	afx_msg void OnUpdateEditCut(CCmdUI* pCmdUI);
	afx_msg void OnEditPaste();
	afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);
	afx_msg void OnItemsExtEdit();
	afx_msg void OnUpdateItemsExtEdit(CCmdUI* pCmdUI);
	afx_msg void OnFileBuldozer();
	afx_msg void OnUpdateFileBuldozer(CCmdUI* pCmdUI);
	afx_msg void OnEditUndo();
	afx_msg void OnUpdateEditUndo(CCmdUI* pCmdUI);
	afx_msg void OnEditRedo();
	afx_msg void OnUpdateEditRedo(CCmdUI* pCmdUI);
	afx_msg void OnEditClearHistory();
	afx_msg void OnItemsBackward();
	afx_msg void OnUpdateItemsBackward(CCmdUI* pCmdUI);
	afx_msg void OnItemsForward();
	afx_msg void OnUpdateItemsForward(CCmdUI* pCmdUI);
	afx_msg void OnItemsBack();
	afx_msg void OnUpdateItemsBack(CCmdUI* pCmdUI);
	afx_msg void OnItemsFront();
	afx_msg void OnUpdateItemsFront(CCmdUI* pCmdUI);
	afx_msg void OnEditSelectAll();
	afx_msg void OnUpdateEditSelectAll(CCmdUI* pCmdUI);
	afx_msg void OnItemsGroup();
	afx_msg void OnUpdateItemsGroup(CCmdUI* pCmdUI);
	afx_msg void OnLayoutShowGrid();
	afx_msg void OnUpdateLayoutShowGrid(CCmdUI* pCmdUI);
	afx_msg void OnLayoutShowGuides();
	afx_msg void OnUpdateLayoutShowGuides(CCmdUI* pCmdUI);
	afx_msg void OnLayoutSnapToGrid();
	afx_msg void OnUpdateLayoutSnapToGrid(CCmdUI* pCmdUI);
	afx_msg void OnLayoutSnapToGuides();
	afx_msg void OnUpdateLayoutSnapToGuides(CCmdUI* pCmdUI);
	afx_msg void OnEditPreferences();
	afx_msg void OnLayoutDeleteGuides();
	afx_msg void OnUpdateLayoutDeleteGuides(CCmdUI* pCmdUI);
	afx_msg void OnLayoutToggleGridGuides();
	afx_msg void OnUpdateLayoutToggleGridGuides(CCmdUI* pCmdUI);
	afx_msg void OnClassEditMacros();
	afx_msg void OnClassExtEdit();
	afx_msg void OnUpdateClassExtEdit(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLayoutMoveLeft(CCmdUI* pCmdUI);
	afx_msg void OnLayoutMoveLeft();
	afx_msg void OnUpdateLayoutMoveRight(CCmdUI* pCmdUI);
	afx_msg void OnLayoutMoveRight();
	afx_msg void OnUpdateLayoutMoveUp(CCmdUI* pCmdUI);
	afx_msg void OnLayoutMoveUp();
	afx_msg void OnUpdateLayoutMoveDown(CCmdUI* pCmdUI);
	afx_msg void OnLayoutMoveDown();
	afx_msg void OnUpdateLayoutFreeX(CCmdUI* pCmdUI);
	afx_msg void OnLayoutFreeX();
	afx_msg void OnUpdateLayoutFreeY(CCmdUI* pCmdUI);
	afx_msg void OnLayoutFreeY();
	afx_msg void OnUpdateClassSetMonitorClass(CCmdUI* pCmdUI);
	afx_msg void OnClassSetMonitorClass();
	afx_msg void OnLayoutSpaceEvenlyAcrossSelect();
	afx_msg void OnUpdateLayoutSpaceEvenlyAcrossSelect(CCmdUI* pCmdUI);
	afx_msg void OnLayoutSpaceEvenlyDownSelect();
	afx_msg void OnUpdateLayoutSpaceEvenlyDownSelect(CCmdUI* pCmdUI);
	afx_msg void OnLayoutSnapToItem();
	afx_msg void OnUpdateLayoutSnapToItem(CCmdUI* pCmdUI);
	afx_msg void OnLayoutSideBySideTop();
	afx_msg void OnUpdateLayoutSideBySideTop(CCmdUI* pCmdUI);
	afx_msg void OnLayoutSideBySideLeft();
	afx_msg void OnUpdateLayoutSideBySideLeft(CCmdUI* pCmdUI);
	afx_msg void OnLayoutSideBySideRight();
	afx_msg void OnUpdateLayoutSideBySideRight(CCmdUI* pCmdUI);
	afx_msg void OnLayoutSideBySideBottom();
	afx_msg void OnUpdateLayoutSideBySideBottom(CCmdUI* pCmdUI);
	afx_msg void OnEditFind();
	afx_msg void OnEditFindNext();
	afx_msg LONG OnFindMsg(WPARAM wParam, LPARAM lParam);
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	afx_msg void OnEditUndoToSave();
	afx_msg void OnUpdateEditUndoToSave(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in RscEditorView.cpp
inline CRscEditorDoc* CRscEditorView::GetDocument()
   { return (CRscEditorDoc*)m_pDocument; }
#endif

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RSCEDITORVIEW_H__55C9EB8D_CD40_4E9A_BCCA_424A45890097__INCLUDED_)
