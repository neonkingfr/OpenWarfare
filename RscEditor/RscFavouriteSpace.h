
#if !defined(AFX_RSCFAVOURITESPACE_H__7E70D484_F374_44A9_8853_208AFEA89785__INCLUDED_)
#define AFX_RSCFAVOURITESPACE_H__7E70D484_F374_44A9_8853_208AFEA89785__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CRscEditorView;

//! Tree control of the favourite list.
class CRscFavouriteSpace : public CTreeCtrl
{
public:
  //! Parent view.
	CRscEditorView					*_view;

  //! Is right button donw? (for context menu)
	bool							_rButtonDown;

  //! Auxiliary object for drag&drop.
	CDropTarget<CRscFavouriteSpace>	_dropTarget;

  //! Last drop effect.
	DROPEFFECT						_lastDropEffect;

  //! Last drop rectangle.
	CRect							_lastDropRect;

  //! Last mouse drag point.
	CPoint							_dragPoint;

  //! Last drop tree item.
	HTREEITEM						_lastInsertMarkItem;

  //! Last flag for drop before or behind.
	bool							_lastInsertMarkAfter;

  //! Last mouse drag time.
	DWORD							_lastDragTime;

  //! Tree item clicked.
	HTREEITEM						_itemClick;

public:
	CRscFavouriteSpace();
	virtual ~CRscFavouriteSpace();

  //! Register drop target.
	void DropRegister ();

  //! Method of drag and drop event.
	DROPEFFECT OnDragEnter(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);

  //! Method of drag and drop event.
	DROPEFFECT OnDragOver(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);

  //! Method of drag and drop event.
	void OnDragLeave(CWnd *pWnd);

  //! Method of drag and drop event.
	BOOL OnDrop (CWnd *pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);

  //! Insert droppped class or attribute into appropriate place in tree.
	BOOL ProcessParamDropInWorkSpace (CString &fullCode, bool isClass, DROPEFFECT dropEffect);

  //! Method of menu item command.
	void OnEditCopy();

  //! Method of menu item command UI update.
	void OnUpdateEditCopy(CCmdUI* pCmdUI);

  //! Method of menu item command.
	void OnEditCut();

  //! Method of menu item command UI update.
	void OnUpdateEditCut(CCmdUI* pCmdUI);

	//{{AFX_VIRTUAL(CRscFavouriteSpace)
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CRscFavouriteSpace)
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnBeginDrag(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnClick(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RSCFAVOURITESPACE_H__7E70D484_F374_44A9_8853_208AFEA89785__INCLUDED_)
