
#if !defined(AFX_RSCINCLFILESPACE_H__33206F91_6CCA_4800_9B50_204C1013FCEE__INCLUDED_)
#define AFX_RSCINCLFILESPACE_H__33206F91_6CCA_4800_9B50_204C1013FCEE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CRscEditorView;

//! Tree control of the included file list.
class CRscInclFileSpace : public CTreeCtrl
{
public:
  //! Parent view.
	CRscEditorView					*_view;

public:
	CRscInclFileSpace();
	virtual ~CRscInclFileSpace();

	//{{AFX_VIRTUAL(CRscInclFileSpace)
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CRscInclFileSpace)
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RSCINCLFILESPACE_H__33206F91_6CCA_4800_9B50_204C1013FCEE__INCLUDED_)
