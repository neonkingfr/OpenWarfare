
#include "stdafx.h"
#include "RscEditor.h"
#include "RscEditorItems.h"
#include "RscMacro.h"
#include "RscEditorDoc.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"
#include "RscEditorREDParser.h"
#include "RscIDEditor.h"
#include "RscEditorDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CRscMacroEditor::CRscMacroEditor(CRscEditorDoc *doc, CWnd* pParent /*=NULL*/)
	: CDialog(CRscMacroEditor::IDD, pParent), _codeEdited (false), _doc (doc), _font (NULL),
	_ctrlDown (false)
{
	//{{AFX_DATA_INIT(CRscMacroEditor)
	//}}AFX_DATA_INIT
}


CRscMacroEditor::~CRscMacroEditor ()
{
	if (_font != NULL)
	{
		delete _font;
		_font = NULL;
	}
}


void CRscMacroEditor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRscMacroEditor)
	DDX_Control(pDX, IDC_IDSTEXT, _idsTextCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRscMacroEditor, CDialog)
	//{{AFX_MSG_MAP(CRscMacroEditor)
	ON_EN_CHANGE(IDC_IDSTEXT, OnChangeIDsText)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CRscMacroEditor::OnOK() 
{
	if (_codeEdited)
	{
		if (Compile ())
		{
			_codeEdited = false;
			
			int n = _doc->_macros.GetSize ();
			for (int i = 0; i < n; )
			{
				CRscMacro *m = _doc->_macros[i];
				if (!m->IsIncluded ())
				{
					delete m;
					_doc->_macros.RemoveAt (i);
					--n;
				}
				else
				{
					++i;
				}
			}
			_doc->_macros.Append (_tempMacros);

			CDialog::OnOK ();
		}
	}
	else
	{
		CDialog::OnOK ();
	}
}

bool CRscMacroEditor::Compile ()
{
	int n = _tempMacros.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		delete _tempMacros[i];
	}
	_tempMacros.RemoveAll ();

	CString str;
	_idsTextCtrl.GetWindowText (str);

	CRscEditorStringInput in (str);
	CRscEditorLex lex (&in, CRscEditorLex::rscEditorKeywords, CRscEditorLex::rscEditorKeywordsLength);
	CRscEditorToken tok;
	bool showRemoveCommentBox = true;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_REMOVE_COMMENT_BOX, showRemoveCommentBox);
	CRscEditorCodeParser parser (NULL, _doc, NULL, true, false, true, showRemoveCommentBox, NULL);
	lex.GetNextToken (&tok, &parser, parser._basicLexFlags);
	if (!parser.ParseMacros (lex, tok, _tempMacros))
	{
		TCHAR buf[64];
		_stprintf (buf, _T ("Error at line %u."), parser._errorFileLine);
		AfxMessageBox (buf, MB_OK);

		int n = _tempMacros.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			delete _tempMacros[i];
		}
		_tempMacros.RemoveAll ();
		return false;
	}

	return true;
}

void CRscMacroEditor::OnChangeIDsText() 
{
	_codeEdited = true;
}

BOOL CRscMacroEditor::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CString str;
	_doc->GetMacrosText (str);
	_idsTextCtrl.SetWindowText (str);

	if (_font == NULL)
	{
		_font = new CFont ();
		if (_font != NULL)
		{
			if (_font->CreatePointFont (100, _T ("Courier")))
			{
				_idsTextCtrl.SetFont (_font);
			}
			else
			{
				delete _font;
				_font = NULL;
			}
		}
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CRscMacroEditor::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	CWnd *ctrl;
	ctrl = GetDlgItem (IDOK);
	if (ctrl == NULL)
	{
		return;
	}
	RECT r;
	ctrl->GetWindowRect (&r);
	ScreenToClient (&r);
	int dy = r.bottom - r.top;
	r.left = cx - UI_SPACE - (r.right - r.left);
	r.right = cx - UI_SPACE;
	r.top = UI_SPACE;
	r.bottom = UI_SPACE + dy;
	ctrl->MoveWindow (&r, TRUE);

	ctrl = GetDlgItem (IDCANCEL);
	if (ctrl == NULL)
	{
		return;
	}
	r.top = r.bottom + UI_SPACE;
	r.bottom = r.top + dy;
	ctrl->MoveWindow (&r, TRUE);

	ctrl = GetDlgItem (IDC_IDSTEXT);
	if (ctrl == NULL)
	{
		return;
	}
	r.right = r.left - UI_SPACE;
	r.left = UI_SPACE;
	r.top = UI_SPACE;
	r.bottom = cy - UI_SPACE;
	ctrl->MoveWindow (&r, TRUE);
}

BOOL CRscMacroEditor::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->hwnd == _idsTextCtrl.m_hWnd)
	{
		if (pMsg->message == WM_KEYDOWN)
		{
			switch (pMsg->wParam)
			{
			case VK_TAB:
				_idsTextCtrl.SendMessage (WM_CHAR, _T ('\t'), pMsg->lParam);
				return TRUE;
				
			case VK_RETURN:
				if (_ctrlDown)
				{
					SendMessage (WM_COMMAND, IDOK, NULL);
					return TRUE;
				}
				break;

			case VK_CONTROL:
				_ctrlDown = true;
				break;
			}
		}
		else if (pMsg->message == WM_KEYUP)
		{
			switch (pMsg->wParam)
			{
			case VK_CONTROL:
				_ctrlDown = false;
				break;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}
