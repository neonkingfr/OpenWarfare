
#include "stdafx.h"
#include "RscEditor.h"

#include "MainFrm.h"
#include "RscEditorItems.h"
#include "RscMacro.h"
#include "RscEditorDoc.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"
#include "RscEditorView.h"
#include "RscEditorPreview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BEGIN_MESSAGE_MAP(CRscEditorApp, CWinApp)
	//{{AFX_MSG_MAP(CRscEditorApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()

CRscEditorApp::CRscEditorApp()
: _lastFlags (0), _cursorHand (::GetCursor ()), _cursorArrow (::GetCursor ()), _cursorHorzArrow (::GetCursor ()), 
_cursorVertArrow (::GetCursor ()), _cursorLeftTopArrow (::GetCursor ()), _cursorRightTopArrow (::GetCursor ()),
_cursorEraser (::GetCursor ())
{
}

CRscEditorApp theApp;

BOOL CRscEditorApp::InitInstance()
{
	_cursorArrow = ::LoadCursor (NULL, IDC_ARROW);
	_cursorHand = ::LoadCursor (NULL, IDC_SIZEALL);
	_cursorHorzArrow = ::LoadCursor (NULL, IDC_SIZEWE);
	_cursorVertArrow = ::LoadCursor (NULL, IDC_SIZENS);
	_cursorLeftTopArrow = ::LoadCursor (NULL, IDC_SIZENWSE);
	_cursorRightTopArrow = ::LoadCursor (NULL, IDC_SIZENESW);
	_cursorEraser = ::LoadCursor (NULL, IDC_UPARROW);

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CRscEditorDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CRscEditorView));
	AddDocTemplate(pDocTemplate);

	// Enable DDE Execute open
	EnableShellOpen();
	RegisterShellFileTypes(TRUE);

	_preferences.Load ();

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it.
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();

	// Enable drag/drop open
	m_pMainWnd->DragAcceptFiles();

	return TRUE;
}


class CAboutDlg : public CDialog
{
public:
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

public:
	CAboutDlg();

	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CRscEditorApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

int CRscEditorApp::ExitInstance() 
{
	int n = _tempFiles.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		_tempFiles[i].Delete ();
	}
	_tempFiles.RemoveAll ();

	return CWinApp::ExitInstance();
}

BOOL CRscEditorApp::PreTranslateMessage(MSG* pMsg) 
{
	switch (pMsg->message)
	{
	case WM_KEYUP:
	case WM_KEYDOWN:
		if (pMsg->wParam == VK_CONTROL || pMsg->wParam == VK_SHIFT)
		{
			POINT pt;
			::GetCursorPos (&pt);
			CWnd *ch = CWnd::WindowFromPoint (pt);
			if (ch != NULL)
			{
				ch->SendMessage (WM_USER_UPDATECURSOR, 0, 0);
			}
		}
		break;
	}

	return CWinApp::PreTranslateMessage(pMsg);
}
