#if !defined(AFX_CLASSEDITOR_H__A48BC1D8_357E_4198_9A53_C11E6A1F5413__INCLUDED_)
#define AFX_CLASSEDITOR_H__A48BC1D8_357E_4198_9A53_C11E6A1F5413__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//! Class of modeless dialog of text editor.
class CClassEditor : public CDialog
{
public:
	//{{AFX_DATA(CClassEditor)
	enum { IDD = IDD_CLASSEDITOR_DIALOG };
	CEdit	_classTextCtrl;
	//}}AFX_DATA

  //! Font used to display text of resource classes.
	CFont				*_font;

  //! Was code in editor changed?
	bool				_codeEdited;

  //! Parent view;
	CRscEditorView		*_view;

  //! Text containing compiled code or code to be compiled.
	CString				_compileCode;

  //! Pointer to attribute or class which is edited.
	CRscParamBase		*_param;

  //! Is displayed class among favourits?
	bool				_inFavourites;

  //! Is CTRL key down? (for detection of RETURN+CTRL to close the editor)
	bool				_ctrlDown;


public:
	CClassEditor(CWnd* pParent = NULL);   // standard constructor
	~CClassEditor();   // standard destructor

	void InitEditor (CRscParamBase *pb, bool inFavourites);

	//{{AFX_VIRTUAL(CClassEditor)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CClassEditor)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnChangeClassText();
	afx_msg void OnApply();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLASSEDITOR_H__A48BC1D8_357E_4198_9A53_C11E6A1F5413__INCLUDED_)
