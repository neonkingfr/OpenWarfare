
#include "stdafx.h"
#include "RscEditor.h"

#include "MainFrm.h"
#include "RscEditorItems.h"
#include "RscMacro.h"
#include "RscEditorDoc.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"
#include "RscEditorView.h"
#include "RscEditorPreview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_VIEW_LAYOUTBAR, OnViewLayoutbar)
	ON_UPDATE_COMMAND_UI(ID_VIEW_LAYOUTBAR, OnUpdateViewLayoutbar)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

CMainFrame::CMainFrame()
: _editorView (NULL), _editorPreview (NULL)
{
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndLayoutBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndLayoutBar.LoadToolBar(IDR_LAYOUTBAR))
	{
		TRACE0("Failed to create layoutbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndLayoutBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar, AFX_IDW_DOCKBAR_TOP);
	DockControlBar(&m_wndLayoutBar, AFX_IDW_DOCKBAR_BOTTOM);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;

	return TRUE;
}

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
	if (!m_wndSplitter.CreateStatic (this, 1, 2))
	{
		return FALSE;
	}

	if (!m_wndSplitter.CreateView (0, 0, RUNTIME_CLASS (CRscEditorView), CSize (300, 0),
		pContext))
	{
		return FALSE;
	}
	_editorView = (CRscEditorView*) m_wndSplitter.GetPane (0, 0);

	if (!m_wndSplitter.CreateView (0, 1, RUNTIME_CLASS (CRscEditorPreview), CSize (0, 0),
		pContext))
	{
		return FALSE;
	}
	_editorPreview = (CRscEditorPreview*) m_wndSplitter.GetPane (0, 1);

	_editorView->_preview = _editorPreview;
	_editorPreview->_view = _editorView;

	return TRUE;
}

void CMainFrame::OnViewLayoutbar() 
{
	BOOL bVisible = ((m_wndLayoutBar.GetStyle() & WS_VISIBLE) != 0);

	ShowControlBar(&m_wndLayoutBar, !bVisible, FALSE);
	RecalcLayout();
}

void CMainFrame::OnUpdateViewLayoutbar(CCmdUI* pCmdUI) 
{
	BOOL bVisible = ((m_wndLayoutBar.GetStyle() & WS_VISIBLE) != 0);
	pCmdUI->SetCheck(bVisible);
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo) 
{
	if (CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
	{
		return TRUE;
	}

	CRscEditorDoc *doc = (CRscEditorDoc*) GetActiveDocument ();
	if (doc != NULL)
	{
		return doc->RouteCmdToAllViews (GetActiveView (), nID, nCode, pExtra, pHandlerInfo);
	}

	return FALSE;
}
