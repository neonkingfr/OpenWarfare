
#include "stdafx.h"
#include "rsceditor.h"
#include "RscEditorItems.h"
#include "RscInclFileSpace.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CRscInclFileSpace::CRscInclFileSpace()
: _view (NULL)
{
}

CRscInclFileSpace::~CRscInclFileSpace()
{
}


BEGIN_MESSAGE_MAP(CRscInclFileSpace, CTreeCtrl)
	//{{AFX_MSG_MAP(CRscInclFileSpace)
	ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CRscInclFileSpace::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	UINT flags;
	HTREEITEM item = HitTest (point, &flags);
	if (item != NULL && (flags & TVHT_ONITEMLABEL) != 0)
	{
		CRscInclude *incl = (CRscInclude*) GetItemData (item);
		if (incl != NULL)
		{
			CString editor;
			if (((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::EDITOR_EXTERN, editor))
			{
				STARTUPINFO stInfo;
				memset (&stInfo, 0, sizeof (stInfo));
				stInfo.cb = sizeof(stInfo);
				
				PROCESS_INFORMATION prInfo;
				memset (&prInfo, 0, sizeof (prInfo));
				CString cmdLine;
				cmdLine.Format (_T ("\"%s\" \"%s\""), editor, incl->_name);
				::CreateProcess (editor, 
					cmdLine.GetBuffer (cmdLine.GetLength ()), NULL, NULL,
					FALSE, CREATE_DEFAULT_ERROR_MODE | NORMAL_PRIORITY_CLASS,
					NULL, NULL, &stInfo, &prInfo);
			}
		}
	}
	else
	{
		CTreeCtrl::OnLButtonDblClk(nFlags, point);
	}
}
