#if !defined(AFX_RSCCLASSSPACE_H__270E53CC_42B5_479C_B5B9_5E46F86F165E__INCLUDED_)
#define AFX_RSCCLASSSPACE_H__270E53CC_42B5_479C_B5B9_5E46F86F165E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CRscEditorView;

//! Tree control containing class hierarchy.
class CRscClassSpace : public CTreeCtrl
{
public:
	CRscClassSpace();

public:
  //! Parent view.
	CRscEditorView *_view;

  //! Is right mouse button down? (Context menu)
	bool _rButtonDown;

  //! Drop target for this control.
	CDropTarget<CRscClassSpace> _dropTarget;

  //! Last drop effect code.
	DROPEFFECT _lastDropEffect;

  //! Last drop rectangle.
	CRect _lastDropRect;

  //! Last mouse point during dragging.
	CPoint _dragPoint;

  //! Last insert tree item during dragging.
	HTREEITEM _lastInsertMarkItem;

  //! Insert drgged item before of behind last insert tree item?
	bool _lastInsertMarkAfter;

  //! Last time of drag over event. (For tree item expansion.)
	DWORD _lastDragTime;

  //! Clicked item in the tree.
	HTREEITEM _itemClick;

public:
	virtual ~CRscClassSpace();

  //! Register drop target.
	void DropRegister ();

  //! Method of drag and drop event.
	DROPEFFECT OnDragEnter(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);

  //! Method of drag and drop event.
	DROPEFFECT OnDragOver(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);

  //! Method of drag and drop event.
	void OnDragLeave(CWnd *pWnd);

  //! Method of drag and drop event.
	BOOL OnDrop (CWnd *pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);

  //! Insert droppped class or attribute into appropriate place in tree.
	BOOL ProcessParamDropInWorkSpace (CString &fullCode, bool isClass, DROPEFFECT dropEffect);

  //! Method of menu item command.
	void OnEditCopy();

  //! Method of menu item command UI update.
	void OnUpdateEditCopy(CCmdUI* pCmdUI);

  //! Method of menu item command.
	void OnEditCut();
	
  //! Method of menu item command UI update.
  void OnUpdateEditCut(CCmdUI* pCmdUI);
	
  //! Method of menu item command.
  void OnEditPaste();
	
  //! Method of menu item command UI update.
  void OnUpdateEditPaste(CCmdUI* pCmdUI);

	//{{AFX_VIRTUAL(CRscClassSpace)
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CRscClassSpace)
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnBeginDrag(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnDestroy();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RSCCLASSSPACE_H__270E53CC_42B5_479C_B5B9_5E46F86F165E__INCLUDED_)
