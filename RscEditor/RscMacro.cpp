
#include "stdafx.h"

#include "RscEditorItems.h"
#include "RscMacro.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"

LPCTSTR CRscMacro::_idTypes[] = {_T ("idc"), _T ("idd"), NULL};

CRscMacro::CRscMacro (LPCTSTR macro, CRscInclude *inInclude)
: _macro (macro), _paramList (NULL), _inInclude (inInclude)
{
}

CRscMacro::~CRscMacro ()
{
	if (_paramList != NULL)
	{
		delete _paramList;
		_paramList = NULL;
	}
}

bool CRscMacro::Apply (CString &out, CRscMacroParamList *src)
{
	if (_paramList == NULL)
	{
		if (src == NULL)
		{
			out = _value;
			return true;
		}

		return false;
	}

	int n;
	if ((n = _paramList->_params.GetSize ()) != src->_params.GetSize ())
	{
		return false;
	}

	if (n == 0)
	{
		out = _value;
		return true;
	}

	out.Empty ();
	CRscEditorStringInput si (_value);
	CRscEditorLex lex (&si, NULL, 0);
	CRscEditorToken tok;

	lex.GetNextToken (&tok, NULL, 0);
	while (tok.type != rscEditorLexEof)
	{
		if (tok.type == rscEditorLexError)
		{
			return false;
		}
		if (tok.type == rscEditorLexId)
		{
			for (int i = 0; ; ++i)
			{
				if (i == n)
				{
					out += tok.string;
					break;
				}
				if (_paramList->_params[i] == tok.string)
				{
					out += src->_params[i];
					break;
				}
			}
		}
		else
		{
			out += tok.string;
		}
		lex.GetNextToken (&tok, NULL, 0);
	}

	return true;
}