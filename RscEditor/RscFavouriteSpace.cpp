
#include "stdafx.h"
#include "rsceditor.h"

#include "RscEditorItems.h"
#include "RscFavouriteSpace.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"
#include "RscMacro.h"
#include "RscEditorDoc.h"
#include "RscEditorPreview.h"
#include "RscEditorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CRscFavouriteSpace::CRscFavouriteSpace()
	: _view (NULL), _rButtonDown (false), _lastDropEffect (DROPEFFECT_NONE), _itemClick (NULL)
{
}

CRscFavouriteSpace::~CRscFavouriteSpace()
{
}


BEGIN_MESSAGE_MAP(CRscFavouriteSpace, CTreeCtrl)
	//{{AFX_MSG_MAP(CRscFavouriteSpace)
	ON_WM_RBUTTONDOWN()
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
	ON_NOTIFY_REFLECT(TVN_BEGINDRAG, OnBeginDrag)
	ON_NOTIFY_REFLECT(TVN_SELCHANGED, OnSelChanged)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_DESTROY()
	ON_WM_LBUTTONDOWN()
	ON_NOTIFY_REFLECT(NM_CLICK, OnClick)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CRscFavouriteSpace::OnRButtonDown(UINT nFlags, CPoint point) 
{
	_rButtonDown = true;
	SetFocus ();
}

void CRscFavouriteSpace::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	if (!_rButtonDown)
	{
		HTREEITEM item = GetSelectedItem ();
		if (item != NULL)
		{
			RECT r;
			if (GetItemRect (item, &r, TRUE))
			{
				point.x = (r.right + r.left) >> 1;
				point.y = (r.bottom + r.top) >> 1;
				ClientToScreen (&point);
				_view->DisplayContexMenu (2, point);
			}
		}
	}
}

void CRscFavouriteSpace::OnRButtonUp(UINT nFlags, CPoint point) 
{
	_rButtonDown = false;
	UINT flags;
	HTREEITEM ti = HitTest (point, &flags);
	if (ti != NULL &&
		(flags & TVHT_ONITEM) != 0)
	{
		SelectItem (ti);
	}
	ClientToScreen (&point);
	_view->DisplayContexMenu (2, point);
}

void CRscFavouriteSpace::DropRegister ()
{
	if (_dropTarget._control == NULL)
	{
		_dropTarget._control = this;
		_dropTarget.Register (this);
	}
}

DROPEFFECT CRscFavouriteSpace::OnDragEnter(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point)
{
	ASSERT(_lastDropEffect == DROPEFFECT_NONE);

	_lastInsertMarkItem = NULL;
	LockWindowUpdate ();
	return OnDragOver(pWnd, pDataObject, dwKeyState, point);
}

DROPEFFECT CRscFavouriteSpace::OnDragOver(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point) 
{
	UnlockWindowUpdate ();
	
	CRect dropRect;
	GetClientRect (&dropRect);
	CClientDC dc(this);
	bool scroll = false;
	
	if (point.y < SCROLL_AREA)
	{
		HTREEITEM item = GetFirstVisibleItem ();
		if (item != NULL)
		{
			item = GetPrevVisibleItem (item);
			if (item != NULL)
			{
				DWORD ct = timeGetTime ();
				if (ct - _lastDragTime > SCROLL_TIME)
				{
					if (_lastDropEffect != DROPEFFECT_NONE)
					{
						// erase previous focus rect
						dc.DrawFocusRect(_lastDropRect);
						SetInsertMark (NULL);
						_lastDropEffect = DROPEFFECT_NONE;
					}
					SelectSetFirstVisible (item);
					UpdateWindow ();
					_lastDragTime = ct;
				}
				scroll = true;
			}
		}
	}
	else if (point.y >= dropRect.bottom - SCROLL_AREA)
	{
		HTREEITEM item = GetFirstVisibleItem ();
		if (item != NULL)
		{
			item = GetNextVisibleItem (item);
			if (item != NULL)
			{
				DWORD ct = timeGetTime ();
				if (ct - _lastDragTime > SCROLL_TIME)
				{
					if (_lastDropEffect != DROPEFFECT_NONE)
					{
						// erase previous focus rect
						dc.DrawFocusRect(_lastDropRect);
						SetInsertMark (NULL);
						_lastDropEffect = DROPEFFECT_NONE;
					}
					SelectSetFirstVisible (item);
					UpdateWindow ();
					_lastDragTime = ct;
				}
				scroll = true;
			}
		}
	}

	if (!scroll)
	{
		UINT cfClass = RegisterClipboardFormat (_T ("class.rsceditor.bi"));
		UINT cfAttr = RegisterClipboardFormat (_T ("attribute.rsceditor.bi"));
		DROPEFFECT de = DROPEFFECT_NONE;
		UINT flags;
		HTREEITEM hitItem = HitTest (point, &flags);
		BOOL isClass = pDataObject->IsDataAvailable (cfClass);
		
		if (isClass || pDataObject->IsDataAvailable (cfAttr))
		{
			CRscWorkTreeItem *dropParam = NULL;
			if (hitItem != NULL)
			{
				dropParam = (CRscWorkTreeItem*) GetItemData (hitItem);
			}

			CRscWorkTreeItem *dragParam = NULL;
			if (_view->_dragItem != NULL)
			{
				dragParam = (CRscWorkTreeItem*) _view->_dragSpaceCtrl->GetItemData (_view->_dragItem);
			}

			if (dragParam != NULL &&
				dragParam->IsParam () &&
				((CRscParamBase*) dragParam)->IsClass () &&
				((CRscClass*) dragParam)->_favouriteTreeItem == _view->_dragItem)
			{
				// change order of favourite items
				de = DROPEFFECT_MOVE;
			}
			else if (dragParam != NULL &&
				dragParam->IsParam () &&
				((CRscParamBase*) dragParam)->IsClass () &&
				((CRscClass*) dragParam)->_treeItem == _view->_dragItem)
			{
				// put class among favourite items
				de = DROPEFFECT_LINK;
			}
		}
		
		if (point == _dragPoint && de == _lastDropEffect)
		{
			if (hitItem != NULL && (flags & TVHT_ONITEM) != 0)
			{
				DWORD ct = timeGetTime ();
				if (ct - _lastDragTime > 1000)
				{
					if (ItemHasChildren (hitItem))
					{
						LockWindowUpdate ();
						OnDragLeave (pWnd);
						Expand (hitItem, TVE_TOGGLE);
						UpdateWindow ();
					}
					_lastDragTime = ct;
				}
				LockWindowUpdate ();
				return _lastDropEffect;
			}
			else
			{
				LockWindowUpdate ();
				return _lastDropEffect;
			}
		}
		
		_lastDragTime = timeGetTime ();
		_dragPoint = point;
		
		// otherwise, cursor has moved -- need to update the drag feedback
		if (_lastDropEffect != DROPEFFECT_NONE)
		{
			// erase previous focus rect
			dc.DrawFocusRect(_lastDropRect);
		}
		_lastDropEffect = de;
		if (_lastDropEffect != DROPEFFECT_NONE)
		{
			if (hitItem != NULL && (flags & TVHT_ONITEMROW) != 0)
			{
				_lastInsertMarkItem = hitItem;
				RECT r;
				GetItemRect (hitItem, &r, FALSE);
				_lastInsertMarkAfter = point.y > ((r.top + r.bottom) >> 1);
				SetInsertMark (_lastInsertMarkItem, _lastInsertMarkAfter);
			}
			else
			{
				_lastInsertMarkItem = NULL;
				SetInsertMark (NULL);
			}
			
			UpdateWindow ();
			_lastDropRect = dropRect;
			dc.DrawFocusRect(dropRect);
		}
		else
		{
			SetInsertMark (NULL);
			UpdateWindow ();
		}
	}
	LockWindowUpdate ();
	return _lastDropEffect;
}

void CRscFavouriteSpace::OnDragLeave(CWnd *pWnd) 
{
	UnlockWindowUpdate ();
	SetInsertMark (NULL);
	CClientDC dc(this);
	if (_lastDropEffect != DROPEFFECT_NONE)
	{
		dc.DrawFocusRect(_lastDropRect); // erase previous focus rect
		_lastDropEffect = DROPEFFECT_NONE;
	}
	UpdateWindow ();
}

BOOL CRscFavouriteSpace::OnDrop (CWnd *pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point)
{
	ASSERT_VALID(this);

	// Clean up focus rect.
	OnDragLeave(pWnd);

	UINT cfClass = RegisterClipboardFormat (_T ("class.rsceditor.bi"));
	UINT cfAttr = RegisterClipboardFormat (_T ("attribute.rsceditor.bi"));

	if (pDataObject->IsDataAvailable (cfClass))
	{
		CString str;
		HGLOBAL data = pDataObject->GetGlobalData (cfClass);
		if (data != NULL)
		{
			BOOL res = FALSE;
			LPVOID ptr = ::GlobalLock (data);
			if (ptr != NULL)
			{
				str = (LPCTSTR) ptr;
				::GlobalUnlock (data);
				BOOL res = ProcessParamDropInWorkSpace (str, true, dropEffect);
			}
			// ::GlobalFree (data);
			_view->_dragItem = NULL;
			_view->_dragSpaceCtrl = NULL;
			return res;
		}
	}
	else if (pDataObject->IsDataAvailable (cfAttr))
	{
		CString str;
		HGLOBAL data = pDataObject->GetGlobalData (cfAttr);
		if (data != NULL)
		{
			BOOL res = FALSE;
			LPVOID ptr = ::GlobalLock (data);
			if (ptr != NULL)
			{
				str = (LPCTSTR) ptr;
				::GlobalUnlock (data);
				BOOL res = ProcessParamDropInWorkSpace (str, false, dropEffect);
			}
			// ::GlobalFree (data);
			_view->_dragItem = NULL;
			_view->_dragSpaceCtrl = NULL;
			return res;
		}
	}

	return FALSE;
}

void CRscFavouriteSpace::OnBeginDrag(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;

	*pResult = 0;

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) GetItemData (pNMTreeView->itemNew.hItem);
	if (wtItem == NULL ||
		!wtItem->IsParam ())
	{
		return;
	}

	CString tab ("\t");
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::TABULATOR, tab);
	CString str;
	int size = ((CRscParamBase*) wtItem)->GetTextLength (tab);
	LPTSTR buf = str.GetBuffer (size);
	((CRscParamBase*) wtItem)->GetText (buf, tab);
	str.ReleaseBuffer (size);
	size = (size + 1) * sizeof (TCHAR);
	HGLOBAL data1 = ::GlobalAlloc (GMEM_MOVEABLE, size);
	if (data1 == NULL)
	{
		return;
	}
	LPVOID ptr = ::GlobalLock (data1);
	if (ptr == NULL)
	{
		::GlobalFree (data1);
		return;
	}
	CopyMemory (ptr, str, size);
	::GlobalUnlock (data1);

	size = ((CRscParamBase*) wtItem)->GetFullTextLength (tab);
	buf = str.GetBuffer (size);
	((CRscParamBase*) wtItem)->GetFullText (buf, tab);
	str.ReleaseBuffer (size);
	CString dir;
	if (((CRscParamBase*) wtItem)->_parent != NULL)
	{
		((CRscParamBase*) wtItem)->_parent->GetFullName (dir);
	}
	str = dir + _T (",") + ((CRscParamBase*) wtItem)->_name + _T (";\r\n") + str;
	size = (str.GetLength () + 1) * sizeof (TCHAR);
	HGLOBAL data2 = ::GlobalAlloc (GMEM_MOVEABLE, size);
	if (data2 == NULL)
	{
		::GlobalFree (data1);
		return;
	}
	ptr = ::GlobalLock (data2);
	if (ptr == NULL)
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		return;
	}
	CopyMemory (ptr, str, size);
	::GlobalUnlock (data2);

	UINT cfClass = RegisterClipboardFormat (_T ("class.rsceditor.bi"));
	UINT cfAttr = RegisterClipboardFormat (_T ("attribute.rsceditor.bi"));

	COleDataSource src;
	src.CacheGlobalData (CF_TEXT, data1);
	src.CacheGlobalData (((CRscParamBase*) wtItem)->IsClass () ? cfClass : cfAttr,
		data2);

	_view->_dragItem = pNMTreeView->itemNew.hItem;
	_view->_dragSpaceCtrl = this;
	if (src.DoDragDrop () == DROPEFFECT_MOVE && 
		_view->_dragItem != NULL)
	{
		/*
		CRscWorkTreeItem *wti = (CRscWorkTreeItem*) GetItemData (_view->_dragItem);
		if (wti != NULL &&
			wti->IsParam ())
		{
			_view->GetDocument ()->DeleteParam ((CRscParamBase*) wti);
		}
		*/
	}
	_view->_dragItem = NULL;
	_view->_dragSpaceCtrl = NULL;
	src.Empty ();
}

BOOL CRscFavouriteSpace::ProcessParamDropInWorkSpace (CString &fullCode, bool isClass, DROPEFFECT dropEffect)
{
	CRscWorkTreeItem *dropParam = NULL;
	if (_lastInsertMarkItem != NULL)
	{
		dropParam = (CRscWorkTreeItem*) GetItemData (_lastInsertMarkItem);
	}

	CRscWorkTreeItem *dragParam = NULL;
	if (_view->_dragItem != NULL)
	{
		dragParam = (CRscWorkTreeItem*) _view->_dragSpaceCtrl->GetItemData (_view->_dragItem);
	}

	if (dragParam != NULL &&
		dragParam->IsParam () &&
		((CRscParamBase*) dragParam)->IsClass () &&
		((CRscClass*) dragParam)->_favouriteTreeItem == _view->_dragItem)
	{
		// change order of favourite items
		CRscEditorDoc *doc = _view->GetDocument ();
		if (doc->OpenCommand (_T ("Move Favourite"), false, UNDO_MOVE_FAVOURITE))
		{
			doc->MoveFavouriteParam ((CRscClass*) dragParam, (CRscClass*) dropParam, _lastInsertMarkAfter);
			doc->CloseCommand ();
		}
		return TRUE;
	}
	else if (dragParam != NULL &&
		dragParam->IsParam () &&
		((CRscParamBase*) dragParam)->IsClass () &&
		((CRscClass*) dragParam)->_treeItem == _view->_dragItem)
	{
		// put class among favourite items
		CRscEditorDoc *doc = _view->GetDocument ();
		if (doc->OpenCommand (_T ("Add Favourite"), false, UNDO_ADD_FAVOURITE))
		{
			CRscClass *prev = (CRscClass*) dropParam;
			if (prev == NULL)
			{
				int size = doc->_favouriteClasses.GetSize ();
				prev = (size != 0 ? doc->_favouriteClasses[size - 1] : NULL);
				_lastInsertMarkAfter = true;
			}
			else if (!_lastInsertMarkAfter)
			{
				prev = doc->FindPrevFavourite (prev);
				_lastInsertMarkAfter = true;
			}
			doc->AddFavourite ((CRscClass*) dragParam, prev);
			doc->CloseCommand ();
		}
		return TRUE;
	}

	return FALSE;
}

void CRscFavouriteSpace::OnSelChanged(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;

	*pResult = 0;
	HTREEITEM item = GetSelectedItem ();
	if (item == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wti = (CRscWorkTreeItem*) GetItemData (item);
	if (wti == NULL ||
		!wti->IsParam ())
	{
		return;
	}

	_view->UpdateClassEditor ((CRscParamBase*) wti, true);
}

void CRscFavouriteSpace::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	UINT flags;
	HTREEITEM item = HitTest (point, &flags);
	if (item != NULL)
	{
		if ((flags & TVHT_ONITEM) != 0)
		{
			CRscWorkTreeItem *wti = (CRscWorkTreeItem*) GetItemData (item);
			if (wti != NULL &&
				wti->IsParam ())
			{
				_view->LaunchClassEditor ((CRscParamBase*) wti, true);
				return;
			}
		}
	}

	CTreeCtrl::OnLButtonDblClk(nFlags, point);
}

void CRscFavouriteSpace::OnDestroy() 
{
	CTreeCtrl::OnDestroy();
	
	if (_dropTarget._control != NULL)
	{
		_dropTarget.Revoke ();
		_dropTarget._control = NULL;
	}
}

void CRscFavouriteSpace::OnEditCopy() 
{
	HTREEITEM selItem = GetSelectedItem ();
	if (selItem == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) GetItemData (selItem);
	if (wtItem == NULL ||
		!wtItem->IsParam ())
	{
		return;
	}

	CString tab ("\t");
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::TABULATOR, tab);
	CString str;
	int size = ((CRscParamBase*) wtItem)->GetTextLength (tab);
	LPTSTR buf = str.GetBuffer (size);
	((CRscParamBase*) wtItem)->GetText (buf, tab);
	str.ReleaseBuffer (size);
	size = (size + 1) * sizeof (TCHAR);
	HGLOBAL data1 = ::GlobalAlloc (GMEM_MOVEABLE, size);
	if (data1 == NULL)
	{
		return;
	}
	LPVOID ptr = ::GlobalLock (data1);
	if (ptr == NULL)
	{
		::GlobalFree (data1);
		return;
	}
	CopyMemory (ptr, str, size);
	::GlobalUnlock (data1);

	size = ((CRscParamBase*) wtItem)->GetFullTextLength (tab);
	buf = str.GetBuffer (size);
	((CRscParamBase*) wtItem)->GetFullText (buf, tab);
	str.ReleaseBuffer (size);
	CString dir;
	if (((CRscParamBase*) wtItem)->_parent != NULL)
	{
		((CRscParamBase*) wtItem)->_parent->GetFullName (dir);
	}
	str = dir + _T (",") + ((CRscParamBase*) wtItem)->_name + _T (";\r\n") + str;
	size = (str.GetLength () + 1) * sizeof (TCHAR);
	HGLOBAL data2 = ::GlobalAlloc (GMEM_MOVEABLE, size);
	if (data2 == NULL)
	{
		::GlobalFree (data1);
		return;
	}
	ptr = ::GlobalLock (data2);
	if (ptr == NULL)
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		return;
	}
	CopyMemory (ptr, str, size);
	::GlobalUnlock (data2);

	UINT cfClass = RegisterClipboardFormat (_T ("class.rsceditor.bi"));
	UINT cfAttr = RegisterClipboardFormat (_T ("attribute.rsceditor.bi"));

	if (!OpenClipboard ())
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		return;
	}

	if (!::EmptyClipboard ())
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		::CloseClipboard ();
		return;
	}

	if (::SetClipboardData (CF_TEXT, data1) == NULL)
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		::CloseClipboard ();
		return;
	}

	if (::SetClipboardData (((CRscParamBase*) wtItem)->IsClass () ? cfClass : cfAttr, data2) == NULL)
	{
		::GlobalFree (data2);
		::CloseClipboard ();
		return;
	}

	::CloseClipboard ();
	return;
}

void CRscFavouriteSpace::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	HTREEITEM selItem = GetSelectedItem ();
	if (selItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) GetItemData (selItem);
	if (wtItem == NULL ||
		!wtItem->IsParam ())
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}

void CRscFavouriteSpace::OnEditCut() 
{
	HTREEITEM selItem = GetSelectedItem ();
	if (selItem == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) GetItemData (selItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () ||
		!((CRscParamBase*) wtItem)->IsEditable ())
	{
		return;
	}

	CString tab ("\t");
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::TABULATOR, tab);
	CString str;
	int size = ((CRscParamBase*) wtItem)->GetTextLength (tab);
	LPTSTR buf = str.GetBuffer (size);
	((CRscParamBase*) wtItem)->GetText (buf, tab);
	str.ReleaseBuffer (size);
	size = (size + 1) * sizeof (TCHAR);
	HGLOBAL data1 = ::GlobalAlloc (GMEM_MOVEABLE, size);
	if (data1 == NULL)
	{
		return;
	}
	LPVOID ptr = ::GlobalLock (data1);
	if (ptr == NULL)
	{
		::GlobalFree (data1);
		return;
	}
	CopyMemory (ptr, str, size);
	::GlobalUnlock (data1);

	size = ((CRscParamBase*) wtItem)->GetFullTextLength (tab);
	buf = str.GetBuffer (size);
	((CRscParamBase*) wtItem)->GetFullText (buf, tab);
	str.ReleaseBuffer (size);
	CString dir;
	if (((CRscParamBase*) wtItem)->_parent != NULL)
	{
		((CRscParamBase*) wtItem)->_parent->GetFullName (dir);
	}
	str = dir + _T (",") + ((CRscParamBase*) wtItem)->_name + _T (";\r\n") + str;
	size = (str.GetLength () + 1) * sizeof (TCHAR);
	HGLOBAL data2 = ::GlobalAlloc (GMEM_MOVEABLE, size);
	if (data2 == NULL)
	{
		::GlobalFree (data1);
		return;
	}
	ptr = ::GlobalLock (data2);
	if (ptr == NULL)
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		return;
	}
	CopyMemory (ptr, str, size);
	::GlobalUnlock (data2);

	UINT cfClass = RegisterClipboardFormat (_T ("class.rsceditor.bi"));
	UINT cfAttr = RegisterClipboardFormat (_T ("attribute.rsceditor.bi"));

	if (!OpenClipboard ())
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		return;
	}

	if (!::EmptyClipboard ())
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		::CloseClipboard ();
		return;
	}

	if (::SetClipboardData (CF_TEXT, data1) == NULL)
	{
		::GlobalFree (data1);
		::GlobalFree (data2);
		::CloseClipboard ();
		return;
	}

	if (::SetClipboardData (((CRscParamBase*) wtItem)->IsClass () ? cfClass : cfAttr, data2) == NULL)
	{
		::GlobalFree (data2);
		::CloseClipboard ();
		return;
	}

	::CloseClipboard ();

	CRscEditorDoc *doc = _view->GetDocument ();
	if (doc->OpenCommand (_T ("Cut"), true, UNDO_CUT))
	{
		if (((CRscParamBase*) wtItem)->IsClass ())
		{
			doc->AddCommandEntry (new CUndoSelectClass ((CRscClass*) wtItem, false, ((CRscClass*) wtItem)->_selected));
			doc->DeleteItem ((CRscClass*) wtItem, &_view->_preview->_itemGroupTags[0], false);
		}
		doc->DeleteParam ((CRscParamBase*) wtItem);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
	return;
}

void CRscFavouriteSpace::OnUpdateEditCut(CCmdUI* pCmdUI) 
{
	HTREEITEM selItem = GetSelectedItem ();
	if (selItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) GetItemData (selItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () ||
		!((CRscParamBase*) wtItem)->IsEditable ())
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}

void CRscFavouriteSpace::OnLButtonDown(UINT nFlags, CPoint point) 
{
	UINT flags;
	_itemClick = HitTest (point, &flags);
	if (_itemClick != NULL && (flags & TVHT_ONITEM) != 0)
	{
		SelectItem (_itemClick);
	}
	if ((flags & TVHT_ONITEMLABEL) == 0)
	{
		_itemClick = NULL;
	}

	CTreeCtrl::OnLButtonDown(nFlags, point);
}

void CRscFavouriteSpace::OnClick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	*pResult = 0;

	HTREEITEM tcItem = GetSelectedItem ();
	if (tcItem != NULL)
	{
		CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) GetItemData (tcItem);
		if (wtItem != NULL &&
			wtItem->IsParam () &&
			((CRscParamBase*) wtItem)->IsClass () &&
			((CRscClass*) wtItem)->_displayed)
		{
			CRscEditorDoc *doc = _view->GetDocument ();
			doc->SelectAllClasses (false);
			doc->SelectClass ((CRscClass*) wtItem, true);

			_view->_preview->_workViewCtrl.Invalidate (FALSE);
			_view->_preview->_workViewCtrl.UpdateWindow ();
			return;
		}
	}

	if (_itemClick != NULL)
	{
		CRscWorkTreeItem *wti = (CRscWorkTreeItem*) GetItemData (_itemClick);
		if (wti != NULL &&
			wti->IsParam () &&
			((CRscParamBase*) wti)->IsClass ())
		{
			CRscEditorDoc *doc = _view->GetDocument ();
			if (doc->_curUndoType != UNDO_SET_MONITOR_CLASS)
			{
				if (!doc->OpenCommand (_T ("Set Monitor Class"), true, UNDO_SET_MONITOR_CLASS))
				{
					return;
				}
			}

			if (doc->SetMonitorClass ((CRscClass*) wti))
			{
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
				// doc->CloseCommand ();
			}
		}
	}
}
