
//! Parameters of some macro.
class CRscMacroParamList
{
public:
	CStringArray _params;
};

//! Class of document's macro.
class CRscMacro
{
public:
  //! Array of attribute names representing control item ids.
	static LPCTSTR _idTypes[];

  //! Macro name.
	CString _macro;

  //! Macro text.
	CString _value;

  //! Macro parameters.
	CRscMacroParamList *_paramList;

  //! Is this macro stored in some included file?
	CRscInclude *_inInclude;

  //! Prefix comments.
	CStringArray _preComments;

	CRscMacro (LPCTSTR macro, CRscInclude *inInclude);
	~CRscMacro ();
	
  //! Apply list of parameter to the macro.
	bool Apply (CString &out, CRscMacroParamList *src);

  //! Is macro included.
	bool IsIncluded () const
	{
		return _inInclude != NULL;
	}
};