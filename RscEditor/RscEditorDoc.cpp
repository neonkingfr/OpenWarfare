
#include "stdafx.h"

#include "RscEditor.h"
#include "RscEditorItems.h"
#include "RscMacro.h"
#include "RscEditorDoc.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"
#include "RscEditorREDParser.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//! Structure of control item class and its position in some dialog (used in item sorting from back to front).
struct CRscItemPosition
{
  //! Control item class.
	CRscClass *_class;

  //! Control item position in dialog.
	int _pos;
};

//! Class of control item removing from some dialog.
class CRscItemEraser : public CRscClassEnumerator
{
public:
  //! Control item class to find in dialog class.
	CRscClass		*_findItem;

public:
	CRscItemEraser (CRscEditorDoc *doc, CRscClass *findItem, bool *tags)
		: CRscClassEnumerator (doc, true, tags, false), _findItem (findItem)
	{
	}

	virtual bool EnumerateFce (CRscClass *cl, CRscAttributeValueElement *val)
	{
		return false;
	}

  /*! Method finding the control item class in dialog class and removing it from the dialog. 
      Proper CUndoEntry object is added into undo/redo system - that means some command 
      must be opened before using object of this class.
  */
	virtual bool EnumerateValue (CRscClass *topClass, CRscParamBase *curParam, CRscAttributeValueBase *val, int itemGroup, int recursiveLevel)
	{
		if (val->IsArray ())
		{
			int n = ((CRscAttributeValueArray*) val)->_elements.GetSize ();
			for (int i = 0; i < n; ++i)
			{
				CRscAttributeValueBase *subVal = ((CRscAttributeValueArray*) val)->_elements[i];
				if (subVal->IsElement ())
				{
					CRscClass *p = _doc->FindClassDefinition (((CRscAttributeValueElement*) subVal)->_value, topClass, false);
					
					if (p != NULL)
					{
						if (p == _findItem)
						{
							((CRscAttributeValueArray*) val)->_elements.RemoveAt (i);
							_doc->SetModifiedFlag (TRUE);

							if (!_doc->_undoMode)
							{
								CUndoDeleteParamValue *undo = new CUndoDeleteParamValue (curParam, 
									&((CRscAttributeValueArray*) val)->_elements, i, subVal);
								_doc->AddCommandEntry (undo);
							}

							CUpdateDescriptor dscr;
							dscr._newValue._param = curParam;
							_doc->UpdateAllViews (NULL, UH_NEW_VALUE, &dscr);
							return true;
						}
						else if (EnumerateClass (p, (CRscAttributeValueElement*) subVal, recursiveLevel + 1))
						{
							return true;
						}
					}
				}
				else if (EnumerateValue (topClass, curParam, subVal, itemGroup, recursiveLevel))
				{
					return true;
				}
			}
		} 
		else if (val->IsElement ())
		{
			CRscClass *p = _doc->FindClassDefinition (((CRscAttributeValueElement*) val)->_value, topClass, false);
			
			if (p != NULL)
			{
				_curItemGroup = itemGroup;
				if (EnumerateClass (p, (CRscAttributeValueElement*) val, recursiveLevel + 1))
				{
					return true;
				}
			}
		}

		return false;
	}
};

//! Construction of CUndoEntry object.
CUndoEntry::CUndoEntry ()
: _done (true)
{
}

//! Destruction of CUndoEntry object.
CUndoEntry::~CUndoEntry ()
{
}

//! Redo action represented by this CUndoEntry.
void CUndoEntry::Redo (CRscEditorDoc *doc)
{
	ASSERT (!_done);
	_done = true;
	doc->SetModifiedFlag (TRUE);
}

//! Undo action represented by this CUndoEntry.
void CUndoEntry::Undo (CRscEditorDoc *doc)
{
	ASSERT (_done);
	_done = false;
	doc->SetModifiedFlag (TRUE);
}

//! Construction of CUndoNewParam object - particular action for undo/redo system.
CUndoNewParam::CUndoNewParam (CRscParamBase *param, int index, bool inFavourites, int favouriteIndex)
: _param (param), _index (index), _inFavourites (inFavourites), _favouriteIndex (favouriteIndex)
{
}

//! Destruction of CUndoNewParam object - particular action for undo/redo system.
CUndoNewParam::~CUndoNewParam ()
{
	if (!_done)
	{
		delete _param;
	}
	_param = NULL;
}

//! Undo action represented by this object.
void CUndoNewParam::Undo (CRscEditorDoc *doc)
{
	CUpdateDescriptor dscr;
	CArray<CRscParamBase*, CRscParamBase*> &arr = (_param->_parent != NULL ? _param->_parent->_innerParams : doc->_params);
	arr.RemoveAt (_index);
	if (_param->IsClass ())
	{
		if (((CRscClass*) _param)->_baseClass != NULL)
		{
			((CRscClass*) _param)->_baseClass->RemoveDerivedClass ((CRscClass*) _param);
		}
		if (_inFavourites)
		{
			doc->_favouriteClasses.RemoveAt (_favouriteIndex);
			dscr._remFavourite._class = (CRscClass*) _param;
			doc->UpdateAllViews (NULL, UH_REMOVE_FAVOURITE, &dscr);
		}
	}

	dscr._delParam._param = _param;
	doc->UpdateAllViews (NULL, UH_DEL_PARAM, &dscr);
	
	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoNewParam::Redo (CRscEditorDoc *doc)
{
	CUpdateDescriptor dscr;
	CArray<CRscParamBase*, CRscParamBase*> &arr = (_param->_parent != NULL ? _param->_parent->_innerParams : doc->_params);
	arr.InsertAt (_index, _param);
	dscr._newParam._param = _param;
	dscr._newParam._inFavourites = false;
	if (_param->IsClass ())
	{
		if (((CRscClass*) _param)->_baseClass != NULL)
		{
			((CRscClass*) _param)->_baseClass->_derivedClasses.Add ((CRscClass*) _param);
		}
		if (_inFavourites)
		{
			doc->_favouriteClasses.InsertAt (_favouriteIndex, (CRscClass*) _param);
			dscr._newParam._inFavourites = true;
		}
	}
	doc->UpdateAllViews (NULL, UH_NEW_PARAM, &dscr);

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoSelectClass object - particular action for undo/redo system.
CUndoSelectClass::CUndoSelectClass (CRscClass *cl, bool selFlag, bool oldSelFlag)
: _class (cl), _selFlag (selFlag), _oldSelFlag (oldSelFlag)
{
}

//! Destruction of CUndoSelectClass object - particular action for undo/redo system.
CUndoSelectClass::~CUndoSelectClass ()
{
}

//! Undo action represented by this object.
void CUndoSelectClass::Undo (CRscEditorDoc *doc)
{
	doc->SelectClass (_class, _oldSelFlag);
	// doc->UpdateAllViews (NULL, UH_DEL_PARAM, &dscr);
	
	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoSelectClass::Redo (CRscEditorDoc *doc)
{
	doc->SelectClass (_class, _selFlag);
	// doc->UpdateAllViews (NULL, UH_NEW_PARAM, &dscr);

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoDeleteParamValue object - particular action for undo/redo system.
CUndoDeleteParamValue::CUndoDeleteParamValue (CRscParamBase *param, 
	  CArray<CRscAttributeValueBase*, CRscAttributeValueBase*> *array,
	  int index, CRscAttributeValueBase *value)
	  : _param (param), _array (array), _index (index), _value (value)
{
}

//! Destruction of CUndoDeleteParamValue object - particular action for undo/redo system.
CUndoDeleteParamValue::~CUndoDeleteParamValue ()
{
	if (_done)
	{
		delete _value;
	}
	_value = NULL;
}

//! Undo action represented by this object.
void CUndoDeleteParamValue::Undo (CRscEditorDoc *doc)
{
	_array->InsertAt (_index, _value);
	CUpdateDescriptor dscr;
	dscr._newValue._param = _param;
	doc->UpdateAllViews (NULL, UH_NEW_VALUE, &dscr);

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoDeleteParamValue::Redo (CRscEditorDoc *doc)
{
	_array->RemoveAt (_index);
	CUpdateDescriptor dscr;
	dscr._newValue._param = _param;
	doc->UpdateAllViews (NULL, UH_NEW_VALUE, &dscr);

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoAddParamValue object - particular action for undo/redo system.
CUndoAddParamValue::CUndoAddParamValue (CRscParamBase *param, 
	  CArray<CRscAttributeValueBase*, CRscAttributeValueBase*> *array,
	  int index, CRscAttributeValueBase *value)
	  : _param (param), _array (array), _index (index), _value (value)
{
}

//! Destruction of CUndoAddParamValue object - particular action for undo/redo system.
CUndoAddParamValue::~CUndoAddParamValue ()
{
	if (!_done)
	{
		delete _value;
	}
	_value = NULL;
}

//! Undo action represented by this object.
void CUndoAddParamValue::Undo (CRscEditorDoc *doc)
{
	_array->RemoveAt (_index);
	CUpdateDescriptor dscr;
	dscr._newValue._param = _param;
	doc->UpdateAllViews (NULL, UH_NEW_VALUE, &dscr);

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoAddParamValue::Redo (CRscEditorDoc *doc)
{
	_array->InsertAt (_index, _value);
	CUpdateDescriptor dscr;
	dscr._newValue._param = _param;
	doc->UpdateAllViews (NULL, UH_NEW_VALUE, &dscr);

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoChangeParamValue object - particular action for undo/redo system.
CUndoChangeParamValue::CUndoChangeParamValue (CRscParamBase *param, 
		CRscAttributeValueBase **change, CRscAttributeValueBase *backupValue)
	  : _param (param), _change (change), _backupValue (backupValue)
{
}

//! Destruction of CUndoChangeParamValue object - particular action for undo/redo system.
CUndoChangeParamValue::~CUndoChangeParamValue ()
{
	delete _backupValue;
	_backupValue = NULL;
}

//! Undo action represented by this object.
void CUndoChangeParamValue::Undo (CRscEditorDoc *doc)
{
	CRscAttributeValueBase *v = *_change;
	*_change = _backupValue;
	_backupValue = v;
	CUpdateDescriptor dscr;
	dscr._newValue._param = _param;
	doc->UpdateAllViews (NULL, UH_NEW_VALUE, &dscr);

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoChangeParamValue::Redo (CRscEditorDoc *doc)
{
	CRscAttributeValueBase *v = *_change;
	*_change = _backupValue;
	_backupValue = v;
	CUpdateDescriptor dscr;
	dscr._newValue._param = _param;
	doc->UpdateAllViews (NULL, UH_NEW_VALUE, &dscr);

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoChangeBase object - particular action for undo/redo system.
CUndoChangeBase::CUndoChangeBase (CRscClass *cl, CRscClass *oldBase, CRscClass *newBase)
: _class (cl), _oldBase (oldBase), _newBase (newBase)
{
}

//! Destruction of CUndoChangeBase object - particular action for undo/redo system.
CUndoChangeBase::~CUndoChangeBase ()
{
}

//! Undo action represented by this object.
void CUndoChangeBase::Undo (CRscEditorDoc *doc)
{
	if (_class->_baseClass != NULL)
	{
		_class->_baseClass->RemoveDerivedClass (_class);
	}
	_class->_baseClass = _oldBase;
	if (_oldBase != NULL)
	{
		_oldBase->_derivedClasses.Add (_class);
	}

	CUpdateDescriptor dscr;
	dscr._newBase._class = _class;
	doc->UpdateAllViews (NULL, UH_NEW_BASE, &dscr);

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoChangeBase::Redo (CRscEditorDoc *doc)
{
	if (_class->_baseClass != NULL)
	{
		_class->_baseClass->RemoveDerivedClass (_class);
	}
	_class->_baseClass = _newBase;
	if (_newBase != NULL)
	{
		_newBase->_derivedClasses.Add (_class);
	}

	CUpdateDescriptor dscr;
	dscr._newBase._class = _class;
	doc->UpdateAllViews (NULL, UH_NEW_BASE, &dscr);

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoChangeMonitor object - particular action for undo/redo system.
CUndoChangeMonitor::CUndoChangeMonitor (CRscClass *oldClass, CRscClass *newClass)
: _oldMonitorClass (oldClass), _newMonitorClass (newClass)
{
}

//! Destruction of CUndoChangeMonitor object - particular action for undo/redo system.
CUndoChangeMonitor::~CUndoChangeMonitor ()
{
}

//! Undo action represented by this object.
void CUndoChangeMonitor::Undo (CRscEditorDoc *doc)
{
	doc->SetMonitorClass (_oldMonitorClass);

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoChangeMonitor::Redo (CRscEditorDoc *doc)
{
	doc->SetMonitorClass (_newMonitorClass);

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoRemoveFavourite object - particular action for undo/redo system.
CUndoRemoveFavourite::CUndoRemoveFavourite (CRscClass *cl, int index)
: _class (cl), _index (index)
{
}

//! Destruction of CUndoRemoveFavourite object - particular action for undo/redo system.
CUndoRemoveFavourite::~CUndoRemoveFavourite ()
{
}

//! Undo action represented by this object.
void CUndoRemoveFavourite::Undo (CRscEditorDoc *doc)
{
	doc->_favouriteClasses.InsertAt (_index, _class);
	CUpdateDescriptor dscr;
	dscr._insertFavourite._class = _class;
	doc->UpdateAllViews (NULL, UH_INSERT_FAVOURITE, &dscr);

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoRemoveFavourite::Redo (CRscEditorDoc *doc)
{
	doc->_favouriteClasses.RemoveAt (_index);
	CUpdateDescriptor dscr;
	dscr._remFavourite._class = _class;
	doc->UpdateAllViews (NULL, UH_REMOVE_FAVOURITE, &dscr);

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoAddFavourite object - particular action for undo/redo system.
CUndoAddFavourite::CUndoAddFavourite (CRscClass *cl, int index)
: _class (cl), _index (index)
{
}

//! Destruction of CUndoAddFavourite object - particular action for undo/redo system.
CUndoAddFavourite::~CUndoAddFavourite ()
{
}

//! Undo action represented by this object.
void CUndoAddFavourite::Undo (CRscEditorDoc *doc)
{
	doc->_favouriteClasses.RemoveAt (_index);
	CUpdateDescriptor dscr;
	dscr._remFavourite._class = _class;
	doc->UpdateAllViews (NULL, UH_REMOVE_FAVOURITE, &dscr);

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoAddFavourite::Redo (CRscEditorDoc *doc)
{
	doc->_favouriteClasses.InsertAt (_index, _class);
	CUpdateDescriptor dscr;
	dscr._insertFavourite._class = _class;
	doc->UpdateAllViews (NULL, UH_INSERT_FAVOURITE, &dscr);

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoMoveFavourite object - particular action for undo/redo system.
CUndoMoveFavourite::CUndoMoveFavourite (CRscClass *cl, int oldIndex, int newIndex)
: _class (cl), _oldIndex (oldIndex), _newIndex (newIndex)
{
}

//! Destruction of CUndoMoveFavourite object - particular action for undo/redo system.
CUndoMoveFavourite::~CUndoMoveFavourite ()
{
}

//! Undo action represented by this object.
void CUndoMoveFavourite::Undo (CRscEditorDoc *doc)
{
	doc->_favouriteClasses.RemoveAt (_newIndex);
	doc->_favouriteClasses.InsertAt (_oldIndex, _class);
	CUpdateDescriptor dscr;
	dscr._moveFavourite._class = _class;
	dscr._moveFavourite._index = _oldIndex;
	doc->UpdateAllViews (NULL, UH_MOVE_FAVOURITE, &dscr);

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoMoveFavourite::Redo (CRscEditorDoc *doc)
{
	doc->_favouriteClasses.RemoveAt (_oldIndex);
	doc->_favouriteClasses.InsertAt (_newIndex, _class);
	CUpdateDescriptor dscr;
	dscr._moveFavourite._class = _class;
	dscr._moveFavourite._index = _newIndex;
	doc->UpdateAllViews (NULL, UH_MOVE_FAVOURITE, &dscr);

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoChangeFavourite object - particular action for undo/redo system.
CUndoChangeFavourite::CUndoChangeFavourite (int index, CRscClass *oldClass, CRscClass *newClass)
: _index (index), _oldClass (oldClass), _newClass (newClass)
{
}

//! Destruction of CUndoChangeFavourite object - particular action for undo/redo system.
CUndoChangeFavourite::~CUndoChangeFavourite ()
{
}

//! Undo action represented by this object.
void CUndoChangeFavourite::Undo (CRscEditorDoc *doc)
{
	doc->_favouriteClasses[_index] = _oldClass;
	CUpdateDescriptor dscr;
	dscr._changeFavourite._oldClass = _newClass;
	dscr._changeFavourite._newClass = _oldClass;
	doc->UpdateAllViews (NULL, UH_CHANGE_FAVOURITE, &dscr);

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoChangeFavourite::Redo (CRscEditorDoc *doc)
{
	doc->_favouriteClasses[_index] = _newClass;
	CUpdateDescriptor dscr;
	dscr._changeFavourite._oldClass = _oldClass;
	dscr._changeFavourite._newClass = _newClass;
	doc->UpdateAllViews (NULL, UH_CHANGE_FAVOURITE, &dscr);

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoDeleteParam object - particular action for undo/redo system.
CUndoDeleteParam::CUndoDeleteParam (CRscParamBase *param, int index)
: _param (param), _index (index)
{
}

//! Destruction of CUndoDeleteParam object - particular action for undo/redo system.
CUndoDeleteParam::~CUndoDeleteParam ()
{
	if (_done)
	{
		delete _param;
	}
	_param = NULL;
}

//! Undo action represented by this object.
void CUndoDeleteParam::Undo (CRscEditorDoc *doc)
{
	CArray<CRscParamBase*, CRscParamBase*> &arr = (_param->_parent != NULL ? _param->_parent->_innerParams : doc->_params);
	arr.InsertAt (_index, _param);
	CUpdateDescriptor dscr;
	dscr._newParam._param = _param;
	dscr._newParam._inFavourites = false;
	doc->UpdateAllViews (NULL, UH_NEW_PARAM, &dscr);

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoDeleteParam::Redo (CRscEditorDoc *doc)
{
	CArray<CRscParamBase*, CRscParamBase*> &arr = (_param->_parent != NULL ? _param->_parent->_innerParams : doc->_params);
	arr.RemoveAt (_index);
	CUpdateDescriptor dscr;
	dscr._delParam._param = _param;
	doc->UpdateAllViews (NULL, UH_DEL_PARAM, &dscr);

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoChangeClassCode object - particular action for undo/redo system.
CUndoChangeClassCode::CUndoChangeClassCode (CRscParamBase *oldCode, CRscParamBase *newCode, int index)
: _oldCode (oldCode), _newCode (newCode), _index (index)
{
}

//! Destruction of CUndoChangeClassCode object - particular action for undo/redo system.
CUndoChangeClassCode::~CUndoChangeClassCode ()
{
	if (_done)
	{
		delete _oldCode;
	}
	else
	{
		delete _newCode;
	}
	_newCode = NULL;
	_oldCode = NULL;
}

//! Undo action represented by this object.
void CUndoChangeClassCode::Undo (CRscEditorDoc *doc)
{
	CArray<CRscParamBase*, CRscParamBase*> &newArr = (_newCode->_parent != NULL ? _newCode->_parent->_innerParams : doc->_params);
	newArr.RemoveAt (_index);
	CUpdateDescriptor dscr;
	dscr._delParam._param = _newCode;
	doc->UpdateAllViews (NULL, UH_DEL_PARAM, &dscr);

	CArray<CRscParamBase*, CRscParamBase*> &oldArr = (_oldCode->_parent != NULL ? _oldCode->_parent->_innerParams : doc->_params);
	oldArr.InsertAt (_index, _oldCode);
	dscr._newParam._param = _oldCode;
	dscr._newParam._inFavourites = false;
	doc->UpdateAllViews (NULL, UH_NEW_PARAM, &dscr);

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoChangeClassCode::Redo (CRscEditorDoc *doc)
{
	CArray<CRscParamBase*, CRscParamBase*> &oldArr = (_oldCode->_parent != NULL ? _oldCode->_parent->_innerParams : doc->_params);
	oldArr.RemoveAt (_index);
	CUpdateDescriptor dscr;
	dscr._delParam._param = _oldCode;
	doc->UpdateAllViews (NULL, UH_DEL_PARAM, &dscr);

	CArray<CRscParamBase*, CRscParamBase*> &newArr = (_newCode->_parent != NULL ? _newCode->_parent->_innerParams : doc->_params);
	newArr.InsertAt (_index, _newCode);
	dscr._newParam._param = _newCode;
	dscr._newParam._inFavourites = false;
	doc->UpdateAllViews (NULL, UH_NEW_PARAM, &dscr);

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoAddInclude object - particular action for undo/redo system.
CUndoAddInclude::CUndoAddInclude (CRscInclude *incl, int from)
: _incl (incl), _from (from)
{
}

//! Destruction of CUndoAddInclude object - particular action for undo/redo system.
CUndoAddInclude::~CUndoAddInclude ()
{
	if (!_done)
	{
		int n = _incl->_params.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			delete _incl->_params[i];
		}
		_incl->_params.RemoveAll ();
		delete _incl;
	}
	_incl = NULL;
}

//! Undo action represented by this object.
void CUndoAddInclude::Undo (CRscEditorDoc *doc)
{
	int n = doc->_includedFiles.GetSize ();
	if (n == 0)
	{
		return;
	}

	CUpdateDescriptor dscr;
	dscr._removeInclude._include = _incl;
	dscr._removeInclude._from = _from;
	doc->UpdateAllViews (NULL, UH_REMOVE_INCLUDE, &dscr);

	doc->_includedFiles.RemoveAt (n - 1);
	n = _incl->_params.GetSize ();
	for (int i = n; i > 0; )
	{
		--i;
		doc->_params.RemoveAt (i);
	}

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoAddInclude::Redo (CRscEditorDoc *doc)
{
	doc->_includedFiles.Add (_incl);

	int n = _incl->_params.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		doc->_params.InsertAt (_from + i, _incl->_params[i]);
	}

	CUpdateDescriptor dscr;
	dscr._addInclude._include = _incl;
	dscr._addInclude._from = _from;
	doc->UpdateAllViews (NULL, UH_ADD_INCLUDE, &dscr);

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoDelGroup object - particular action for undo/redo system.
CUndoDelGroup::CUndoDelGroup (CRscGroup *group, int index)
: _group (group), _index (index)
{
}

//! Destruction of CUndoDelGroup object - particular action for undo/redo system.
CUndoDelGroup::~CUndoDelGroup ()
{
	if (_done)
	{
		delete _group;
	}
	_group = NULL;
}

//! Undo action represented by this object.
void CUndoDelGroup::Undo (CRscEditorDoc *doc)
{
	doc->_groups.InsertAt (_index, _group);
	_group->Attach ();

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoDelGroup::Redo (CRscEditorDoc *doc)
{
	doc->_groups.RemoveAt (_index);
	_group->Detach ();

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoNewGroup object - particular action for undo/redo system.
CUndoNewGroup::CUndoNewGroup (CRscGroup *group, int index)
: _group (group), _index (index)
{
}

//! Destruction of CUndoNewGroup object - particular action for undo/redo system.
CUndoNewGroup::~CUndoNewGroup ()
{
	if (!_done)
	{
		delete _group;
	}
	_group = NULL;
}

//! Undo action represented by this object.
void CUndoNewGroup::Undo (CRscEditorDoc *doc)
{
	doc->_groups.RemoveAt (_index);
	_group->Detach ();

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoNewGroup::Redo (CRscEditorDoc *doc)
{
	doc->_groups.InsertAt (_index, _group);
	_group->Attach ();

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoRemoveFromGroup object - particular action for undo/redo system.
CUndoRemoveFromGroup::CUndoRemoveFromGroup (CRscClass *cl, CRscGroup *group, int index)
: _class (cl), _group (group), _index (index)
{
}

//! Destruction of CUndoRemoveFromGroup object - particular action for undo/redo system.
CUndoRemoveFromGroup::~CUndoRemoveFromGroup ()
{
}

//! Undo action represented by this object.
void CUndoRemoveFromGroup::Undo (CRscEditorDoc *doc)
{
	_group->_members.InsertAt (_index, _class);
	_class->_group = _group;

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoRemoveFromGroup::Redo (CRscEditorDoc *doc)
{
	_group->_members.RemoveAt (_index);
	_class->_group = NULL;

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoCreateGuide object - particular action for undo/redo system.
CUndoCreateGuide::CUndoCreateGuide (CRscGuide *guide)
: _guide (guide)
{
}

//! Destruction of CUndoCreateGuide object - particular action for undo/redo system.
CUndoCreateGuide::~CUndoCreateGuide ()
{
	if (!_done)
	{
		delete _guide;
	}
	_guide = NULL;
}

//! Undo action represented by this object.
void CUndoCreateGuide::Undo (CRscEditorDoc *doc)
{
	int n = doc->_guides.GetSize ();
	if (n > 0)
	{
		doc->_guides.RemoveAt (n - 1);
	}

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoCreateGuide::Redo (CRscEditorDoc *doc)
{
	doc->_guides.Add (_guide);

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoAddGuide object - particular action for undo/redo system.
CUndoAddGuide::CUndoAddGuide (CRscGuide *guide, int type, double value)
: _guide (guide), _type (type), _value (value)
{
}

//! Destruction of CUndoAddGuide object - particular action for undo/redo system.
CUndoAddGuide::~CUndoAddGuide ()
{
}

//! Undo action represented by this object.
void CUndoAddGuide::Undo (CRscEditorDoc *doc)
{
	switch (_type)
	{
	case GUIDE_VERT:
		{
			int n = _guide->_vert.GetSize ();
			_guide->_vert.RemoveAt (n - 1);
			_guide->_vertPxl.RemoveAt (n - 1);
		}
		break;

	case GUIDE_HORZ:
		{
			int n = _guide->_horz.GetSize ();
			_guide->_horz.RemoveAt (n - 1);
			_guide->_horzPxl.RemoveAt (n - 1);
		}
		break;
	}
	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoAddGuide::Redo (CRscEditorDoc *doc)
{
	switch (_type)
	{
	case GUIDE_VERT:
		_guide->_vert.Add (_value);
		_guide->_vertPxl.Add (-1);
		break;

	case GUIDE_HORZ:
		_guide->_horz.Add (_value);
		_guide->_horzPxl.Add (-1);
		break;
	}
	CUndoEntry::Redo (doc);
}

//! Construction of CUndoMoveGuide object - particular action for undo/redo system.
CUndoMoveGuide::CUndoMoveGuide (CRscGuide *guide, int type, int index, double oldValue, double newValue)
: _guide (guide), _type (type), _index (index), _oldValue (oldValue), _newValue (newValue)
{
}

//! Destruction of CUndoMoveGuide object - particular action for undo/redo system.
CUndoMoveGuide::~CUndoMoveGuide ()
{
}

//! Undo action represented by this object.
void CUndoMoveGuide::Undo (CRscEditorDoc *doc)
{
	switch (_type)
	{
	case GUIDE_HORZ:
		_guide->_horz[_index] = _oldValue;
		break;

	case GUIDE_VERT:
		_guide->_vert[_index] = _oldValue;
		break;
	}

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoMoveGuide::Redo (CRscEditorDoc *doc)
{
	switch (_type)
	{
	case GUIDE_HORZ:
		_guide->_horz[_index] = _newValue;
		break;

	case GUIDE_VERT:
		_guide->_vert[_index] = _newValue;
		break;
	}

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoRemoveGuide object - particular action for undo/redo system.
CUndoRemoveGuide::CUndoRemoveGuide (CRscGuide *guide, int type, int index, double value)
: _guide (guide), _type (type), _index (index), _value (value)
{
}

//! Destruction of CUndoRemoveGuide object - particular action for undo/redo system.
CUndoRemoveGuide::~CUndoRemoveGuide ()
{
}

//! Undo action represented by this object.
void CUndoRemoveGuide::Undo (CRscEditorDoc *doc)
{
	switch (_type)
	{
	case GUIDE_VERT:
		_guide->_vert.InsertAt (_index, _value);
		_guide->_vertPxl.InsertAt (_index, -1);
		break;

	case GUIDE_HORZ:
		_guide->_horz.InsertAt (_index, _value);
		_guide->_horzPxl.InsertAt (_index, -1);
		break;
	}

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoRemoveGuide::Redo (CRscEditorDoc *doc)
{
	switch (_type)
	{
	case GUIDE_VERT:
		_guide->_vert.RemoveAt (_index);
		_guide->_vertPxl.RemoveAt (_index);
		break;

	case GUIDE_HORZ:
		_guide->_horz.RemoveAt (_index);
		_guide->_horzPxl.RemoveAt (_index);
		break;
	}

	CUndoEntry::Redo (doc);
}

//! Construction of CUndoDeleteGuide object - particular action for undo/redo system.
CUndoDeleteGuide::CUndoDeleteGuide (CRscGuide *guide, int index)
: _guide (guide), _index (index)
{
}

//! Destruction of CUndoDeleteGuide object - particular action for undo/redo system.
CUndoDeleteGuide::~CUndoDeleteGuide ()
{
	if (_done)
	{
		delete _guide;
	}
	_guide = NULL;
}

//! Undo action represented by this object.
void CUndoDeleteGuide::Undo (CRscEditorDoc *doc)
{
	doc->_guides.InsertAt (_index, _guide);

	CUndoEntry::Undo (doc);
}

//! Redo action represented by this object.
void CUndoDeleteGuide::Redo (CRscEditorDoc *doc)
{
	doc->_guides.RemoveAt (_index);

	CUndoEntry::Redo (doc);
}

//! Construction of command for undo/redo system (it is a list of entries=actions).
CUndoCommand::CUndoCommand (LPCTSTR name, bool postPreviewUpdate)
: _name (name), _postPreviewUpdate (postPreviewUpdate)
{
}

//! Descruction of command for undo/redo system.
CUndoCommand::~CUndoCommand ()
{
	int size = _entries.GetSize ();
	for (int i = 0; i < size; ++i)
	{
		delete _entries[i];
	}
	_entries.RemoveAll ();
}

IMPLEMENT_DYNCREATE(CRscEditorDoc, CDocument)

BEGIN_MESSAGE_MAP(CRscEditorDoc, CDocument)
	//{{AFX_MSG_MAP(CRscEditorDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CRscEditorDoc::CRscEditorDoc()
	: _monitorClass (NULL), _curUndo (NULL), _undoMode (false), _curUndoType (UNDO_NONE), _lastSavedPos (NULL),
	_everSavedFlag (FALSE)
{
}

CRscEditorDoc::~CRscEditorDoc()
{
	DeleteContents ();
}

BOOL CRscEditorDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// Reinitialization code here. (SDI documents will reuse this document)

	_isSaved = FALSE;
	_everSavedFlag = FALSE;
	_lastSavedPos = NULL;
	return TRUE;
}

void CRscEditorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// Storing code here.

		CString tab ("\t");
		((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::TABULATOR, tab);
		
		// Save macro definitions
		CString str;
		GetMacrosText (str);
		ar.Write (str, str.GetLength () * sizeof (TCHAR));

		// Save included files
		int size = _includedFiles.GetSize ();
		for (int i = 0; i < size; ++i)
		{
			int n = _includedFiles[i]->GetTextLength ();
			LPTSTR text = str.GetBuffer (n);
			_includedFiles[i]->GetText (text);
			str.ReleaseBuffer (n);
			ar.Write (str, n * sizeof (TCHAR));
		}

		// Save enums
		size = _enums.GetSize ();
		for (int i = 0; i < size; ++i)
		{
			CRscEnum *e = _enums[i];
			if (!e->IsIncluded ())
			{
				int n = e->GetTextLength ();
				LPTSTR text = str.GetBuffer (n);
				e->GetText (text);
				str.ReleaseBuffer (n);
				ar.Write (str, n * sizeof (TCHAR));
			}
		}

		// Save class and attributes definitions
		size = _params.GetSize ();
		for (int i = 0; i < size; ++i)
		{
			if (!_params[i]->IsIncluded ())
			{
				int n = _params[i]->GetTextLength (tab);
				LPTSTR text = str.GetBuffer (n);
				_params[i]->GetText (text, tab);
				str.ReleaseBuffer (n);
				ar.Write (str, n * sizeof (TCHAR));
			}
		}

		// Save favourtite param names
		size = _favouriteClasses.GetSize ();
		for (int i = 0; i < size; ++i)
		{
			CRscClass *cl = _favouriteClasses[i];
			cl->GetFullName (str);
			ar.Write (_T ("//__F "), _tcslen (_T ("//__F ")) * sizeof (TCHAR));
			ar.Write (str, str.GetLength () * sizeof (TCHAR));
			ar.Write (_T ("\r\n"), _tcslen (_T ("\r\n")) * sizeof (TCHAR));
		}

		// Save monitor class name
		if (_monitorClass != NULL)
		{
			_monitorClass->GetFullName (str);
			ar.Write (_T ("//__M "), _tcslen (_T ("//__M ")) * sizeof (TCHAR));
			ar.Write (str, str.GetLength () * sizeof (TCHAR));
			ar.Write (_T ("\r\n"), _tcslen (_T ("\r\n")) * sizeof (TCHAR));
		}

		// Save guide definitions
		size = _guides.GetSize ();
		for (int i = 0; i < size; ++i)
		{
			CRscGuide *guide = _guides[i];
			ar.Write (_T ("//__GUIDE "), _tcslen (_T ("//__GUIDE ")) * sizeof (TCHAR));
			guide->_owner->GetFullName (str);
			ar.Write (str, str.GetLength () * sizeof (TCHAR));
			int m = guide->_vert.GetSize ();
			for (int j = 0; j < m; ++j)
			{
				ar.Write (_T (" V "), _tcslen (_T (" V ")) * sizeof (TCHAR));
				str.Format (_T ("%g"), guide->_vert[j]);
				ar.Write (str, str.GetLength () * sizeof (TCHAR));
			}
			m = guide->_horz.GetSize ();
			for (int j = 0; j < m; ++j)
			{
				ar.Write (_T (" H "), _tcslen (_T (" H ")) * sizeof (TCHAR));
				str.Format (_T ("%g"), guide->_horz[j]);
				ar.Write (str, str.GetLength () * sizeof (TCHAR));
			}
			ar.Write (_T ("\r\n"), _tcslen (_T ("\r\n")) * sizeof (TCHAR));
		}

		// Save group definitions
		size = _groups.GetSize ();
		for (int i = 0; i < size; ++i)
		{
			CRscGroup *group = _groups[i];
			ar.Write (_T ("//__GROUP "), _tcslen (_T ("//__GROUP ")) * sizeof (TCHAR));
			group->_owner->GetFullName (str);
			ar.Write (str, str.GetLength () * sizeof (TCHAR));
			int m = group->_members.GetSize ();
			for (int j = 0; j < m; ++j)
			{
				ar.Write (_T (" "), _tcslen (_T (" ")) * sizeof (TCHAR));
				group->_members[j]->GetFullName (str);
				ar.Write (str, str.GetLength () * sizeof (TCHAR));
			}
			ar.Write (_T ("\r\n"), _tcslen (_T ("\r\n")) * sizeof (TCHAR));
		}
	}
	else
	{
		// Loading code here.

		CFile *docFile = ar.GetFile ();
		CString curDir = docFile->GetFilePath ();
		curDir = curDir.Left (curDir.GetLength () - docFile->GetFileName ().GetLength ());
		SetCurrentDirectory (curDir);
		CRscEditorArchiveInput in (&ar);
		CRscEditorLex lex (&in, CRscEditorLex::rscEditorKeywords, CRscEditorLex::rscEditorKeywordsLength);
		CRscEditorToken tok;
		_docModified = false;
		bool showRemoveCommentBox = true;
		((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_REMOVE_COMMENT_BOX, showRemoveCommentBox);
		CRscEditorREDParser parser (ar.GetFile ()->GetFilePath (), this, &_docModified, true, false, true, showRemoveCommentBox, NULL, _includedFiles);
		// Remember Macro
		lex.GetNextToken (&tok, &parser, parser._basicSaveCommentsHashLexFlags);

		if (!parser.ParseDOC (lex, tok))
		{
			TCHAR buf[512];
			_stprintf (buf, _T ("Error during parsing file \"%s\" at line %u."), parser._errorFileName, parser._errorFileLine);
			AfxMessageBox (buf, MB_OK);
			//throw new CArchiveException ();
		}
	}
}

#ifdef _DEBUG
// Diagnostics.
void CRscEditorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

// Diagnostics.
void CRscEditorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

void CRscEditorDoc::NewClassName (CString &name, CRscClass *parent)
{
	static int counter = 0;
	CRscParamBase *cc;
	do {
		name.Format (_T ("RscClass%d"), ++counter);
		cc = FindParamInClass (name, parent, false);
	} while (cc != NULL);
}

int CRscEditorDoc::AddClass (CRscClass *cl, CRscClass *in, CRscParamBase *where, bool after)
{
	if (where == NULL)
	{
		cl->_parent = in;
		CArray<CRscParamBase*, CRscParamBase*> &arr = (in != NULL ? in->_innerParams : _params);
		arr.Add (cl);
		return arr.GetSize () - 1;
	}
	else
	{
		in = where->_parent;
		cl->_parent = in;
		CArray<CRscParamBase*, CRscParamBase*> &arr = (in != NULL ? in->_innerParams : _params);
		int n = arr.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			if (arr[i] == where)
			{
				if (after)
				{
					++i;
				}
				arr.InsertAt (i, cl);
				return i;
			}
		}
		arr.Add (cl);
		return arr.GetSize () - 1;
	}
}

CRscClass *CRscEditorDoc::NewItemCopy (CRscClass *cl, CRscClass *monitor, const char *group)
{
	bool ctrlClasses = true;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::CONTROL_CLASSES, ctrlClasses);

	CRscClass *parent;
	if (ctrlClasses)
	{
		CRscParamBase *p = FindParamInClass (group, monitor, false);
		if (p == NULL)
		{
			parent = NewInnerClass (group, monitor, false);
			if (parent == NULL)
			{
				return NULL;
			}
		}
		else if (p->IsClass ())
		{
			parent = (CRscClass*) p;
		}
		else
		{
			return NULL;
		}
	}
	else
	{
		parent = monitor;
	}

	if (cl->_baseClass != NULL)
	{
		int size = parent->_innerParams.GetSize ();
		CRscParamBase *from = (size != 0 ? parent->_innerParams[size - 1] : NULL);
		CRscClass *bc = FindBaseClassDefinition (cl->_baseClass->_name, 
			from, parent, false, true);
		if (bc != cl->_baseClass)
		{
			return NULL;
		}
	}
	
	CRscParamBase *fc = FindParamInClass (cl->_name, parent, false);
	CRscClass *ncl;
	if (fc == NULL)
	{
		ncl = cl->Copy (cl->_name, false);
	}
	else
	{
		CString name;
		NewClassName (name, parent);
		ncl = cl->Copy (name, false);
	}
	
	if (ncl == NULL)
	{
		return NULL;
	}
	int index = AddClass (ncl, parent, NULL, false);
	SetModifiedFlag (TRUE);

	if (!_undoMode)
	{
		CUndoNewParam *undo = new CUndoNewParam (ncl, index, false, -1);
		AddCommandEntry (undo);
	}
	
	CUpdateDescriptor dscr;
	dscr._newParam._param = ncl;
	dscr._newParam._inFavourites = false;
	UpdateAllViews (NULL, UH_NEW_PARAM, &dscr);

	if (!ctrlClasses)
	{
		SetArrayStringParamInBases (parent, group, ncl->_name, true, false, true);
	}
	SelectClass (ncl, true);
	if (!_undoMode)
	{
		CUndoSelectClass *undo = new CUndoSelectClass (ncl, true, false);
		AddCommandEntry (undo);
	}
	return ncl;
}

void CRscEditorDoc::SelectClass (CRscClass *cl, bool flag)
{
	if (_monitorClass != NULL)
	{
		if (flag && !cl->_selected)
		{
			if (cl->_group != NULL)
			{
				SelectGroup (cl->_group, true);
			}
			else
			{
				cl->_selected = true;
				_selectedClasses.Add (cl);
			}
		}
		else if (!flag && cl->_selected)
		{
			if (cl->_group != NULL)
			{
				SelectGroup (cl->_group, false);
			}
			else
			{
				cl->_selected = false;
				int m = _selectedClasses.GetSize ();
				for (int j = 0; j < m; ++j)
				{
					if (_selectedClasses[j] != cl)
					{
						continue;
					}
					
					_selectedClasses.RemoveAt (j);
					break;
				}
			}
		}
	}
}

void CRscEditorDoc::SelectGroup (CRscGroup *gr, bool flag)
{
	int n = gr->_members.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		CRscClass *cl = gr->_members[i];
		if (flag && !cl->_selected)
		{
			cl->_selected = true;
			_selectedClasses.Add (cl);
		}
		else if (!flag && cl->_selected)
		{
			cl->_selected = false;
			int m = _selectedClasses.GetSize ();
			for (int j = 0; j < m; ++j)
			{
				if (_selectedClasses[j] != cl)
				{
					continue;
				}
				
				_selectedClasses.RemoveAt (j);
				break;
			}
		}
	}
}

CRscClass *CRscEditorDoc::NewClass (LPCTSTR name, bool readOnly, LPCTSTR parentFullName,
									bool &inFavourites, int &favouriteIndex, CRscInclude *inInclude, 
									CRscMacroCall *inMacroCall)
{
	if (!inFavourites)
	{
		TCHAR fullName[256];
		_tcscpy (fullName, parentFullName);
		_tcscat (fullName, _T ("::"));
		_tcscat (fullName, name);
		CString val;
		TCHAR str[256];
		_tcscpy (str, CPreferences::GLOBAL_FAVOURITES);
		TCHAR *num = &str[0] + _tcslen (str);
		for (int fc = 1; ; ++fc)
		{
			_stprintf (num, _T ("%d"), fc);
			if (((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (str, val))
			{
				if (_tcscmp (fullName, (LPCTSTR) val) == 0)
				{
					inFavourites = true;
					break;
				}
			}
			else
			{
				break;
			}
		}
	}

	// Macro: koser?
	CRscClass *cl = new CRscClass (name, readOnly, inMacroCall, 0, inInclude);
	if (cl != NULL)
	{
		if (inFavourites)
		{
			favouriteIndex = _favouriteClasses.GetSize ();
			_favouriteClasses.Add (cl);
		}
	}
	return cl;
}

CRscClass *CRscEditorDoc::NewInnerClass (LPCTSTR newName, CRscClass *parent, bool inFavourites)
{
	int favouriteIndex = -1;
	CString parentFullName;
	if (parent != NULL)
	{
		parent->GetFullName (parentFullName);
	}
	CRscClass *cl = NewClass (newName, false, parentFullName, inFavourites, favouriteIndex, NULL, NULL);
	if (cl != NULL)
	{
		int index = AddClass (cl, parent, NULL, false);
		SetModifiedFlag (TRUE);

		if (!_undoMode)
		{
			CUndoNewParam *undo = new CUndoNewParam (cl, index, inFavourites, favouriteIndex);
			AddCommandEntry (undo);
		}

		CUpdateDescriptor dscr;
		dscr._newParam._param = cl;
		dscr._newParam._inFavourites = inFavourites;
		UpdateAllViews (NULL, UH_NEW_PARAM, &dscr);
	}

	return cl;
}

CRscClass *CRscEditorDoc::NewInnerClass (CRscClass *parent, bool inFavourites)
{
	CString newName;
	NewClassName (newName, parent);
	return NewInnerClass (newName, parent, inFavourites);
}

CRscParamBase* CRscEditorDoc::FindNextForward (const char *name, CRscParamBase *last, DWORD flags) const
{
	CString s = name;
	if (!(flags & FR_MATCHCASE))
	{
		s.MakeLower ();
	}
	return FindNextForwardSelf (_params, s, last, flags);
}

CRscParamBase* CRscEditorDoc::FindNextForwardSelf (const CArray<CRscParamBase*, CRscParamBase*> &arr, const CString &name, CRscParamBase *&last, DWORD flags) const
{
	int i = 0;
	int n = arr.GetSize ();
	while (last != NULL && i < n)
	{
		CRscParamBase *p = arr[i++];
		if (p == last)
		{
			last = NULL;
			break;
		}

		if (p->IsClass ())
		{
			CRscParamBase *fp = FindNextForwardSelf (((CRscClass*) p)->_innerParams, name, last, flags);
			if (fp != NULL)
			{
				return fp;
			}
		}
	}

	while (i < n)
	{
		CRscParamBase *p = arr[i++];
		CString lowerName = p->_name;
		if (!(flags & FR_MATCHCASE))
		{
			lowerName.MakeLower ();
		}

		DWORD wholeWord = flags & FR_WHOLEWORD;
		if (!wholeWord && lowerName.Find (name) != -1 || wholeWord && lowerName == name)
		{
			return p;
		}

		if (p->IsClass ())
		{
			CRscParamBase *fp = FindNextForwardSelf (((CRscClass*) p)->_innerParams, name, last, flags);
			if (fp != NULL)
			{
				return fp;
			}
		}
	}

	return NULL;
}

CRscParamBase* CRscEditorDoc::FindNextBackward (const char *name, CRscParamBase *last, DWORD flags) const
{
	CString s = name;
	if (!(flags & FR_MATCHCASE))
	{
		s.MakeLower ();
	}
	return FindNextBackwardSelf (_params, s, last, flags);
}

CRscParamBase* CRscEditorDoc::FindNextBackwardSelf (const CArray<CRscParamBase*, CRscParamBase*> &arr, const CString &name, CRscParamBase *&last, DWORD flags) const
{
	int n = arr.GetSize ();
	int i = n;
	while (last != NULL && i > 0)
	{
		CRscParamBase *p = arr[--i];
		if (p == last)
		{
			last = NULL;
			break;
		}

		if (p->IsClass ())
		{
			CRscParamBase *fp = FindNextBackwardSelf (((CRscClass*) p)->_innerParams, name, last, flags);
			if (fp != NULL)
			{
				return fp;
			}
		}
	}

	while (i > 0)
	{
		CRscParamBase *p = arr[--i];
		CString lowerName = p->_name;
		if (!(flags & FR_MATCHCASE))
		{
			lowerName.MakeLower ();
		}

		DWORD wholeWord = flags & FR_WHOLEWORD;
		if (!wholeWord && lowerName.Find (name) != -1 || wholeWord && lowerName == name)
		{
			return p;
		}

		if (p->IsClass ())
		{
			CRscParamBase *fp = FindNextBackwardSelf (((CRscClass*) p)->_innerParams, name, last, flags);
			if (fp != NULL)
			{
				return fp;
			}
		}
	}

	return NULL;
}

CRscParamBase* CRscEditorDoc::FindParamInChildren (const char *name, CRscParamBase *from, bool inChildren, bool useDisabled)
{
	CArray<CRscParamBase*, CRscParamBase*> &arr = (from->_parent == NULL ? _params : from->_parent->_innerParams);
	int n = arr.GetSize ();
	int i = 0;
	for (; ; ++i)
	{
		if (i == n)
		{
			return NULL;
		}

		if (arr[i] == from)
		{
			break;
		}
	}

	for (; i < n; ++i)
	{
		CRscParamBase *p = arr[i]->FindParamInChildren (name, inChildren, useDisabled);
		if (p == NULL)
		{
			continue;
		}
		
		return p;
	}
	
	return NULL;
}

CRscClass *CRscEditorDoc::FindBaseClassDefinition (const char *name, CRscParamBase *from, CRscClass *in, bool useDisabled, bool inclFrom)
{
	if (from != NULL)
	{
		in = from->_parent;
		CArray<CRscParamBase*, CRscParamBase*> &arr = (in != NULL ? in->_innerParams : _params);
		int m = arr.GetSize ();
		int n = 0;
		for (; ; ++n)
		{
			if (m == n)
			{
				return NULL;
			}
			if (arr[n] == from)
			{
				break;
			}
		}
		for (int i = (inclFrom ? n : n - 1); i >= 0; --i)
		{
			if ((arr[i]->_enabled || useDisabled) && arr[i]->_name.CompareNoCase (name) == 0)
			{
				return arr[i]->IsClass () ? (CRscClass*) arr[i] : NULL;
			}
		}
	}

	if (in != NULL)
	{
		CRscParamBase *p = NULL;
		if (in->_baseClass != NULL)
		{
			p = in->_baseClass->FindParamInBases (name, true, useDisabled);
		}

		if (p == NULL || !p->IsClass ())
		{
			if (in->_parent != NULL)
			{
				p = FindBaseClassDefinition (name, in, in->_parent, useDisabled, false);
			}
			else
			{
				p = FindParamInParents (in, name, useDisabled); // old version
			}

			if (p == NULL || !p->IsClass ())
			{
				return NULL;
			}
		}
		
		return (CRscClass*) p;
	}

	return NULL;
}

CRscClass *CRscEditorDoc::NewDerivedClass (CRscClass *base, CRscClass *in, CRscParamBase *where, bool after, bool inFavourites)
{
	if (in != NULL && !in->IsInsideEditable ())
	{
		return NULL;
	}

	CRscParamBase *from = where;
	if (from == NULL)
	{
		CArray<CRscParamBase*, CRscParamBase*> &arr = (in == NULL ? _params : in->_innerParams);
		int m = arr.GetSize ();
		if (m != 0)
		{
			from = arr[m - 1];
		}
		after = true;
	}

	CRscClass *bc = FindBaseClassDefinition (base->_name, from, in, false, after);
	if (bc != base)
	{
		return NULL;
	}

	CString newName;
	NewClassName (newName, in);
	int favouriteIndex = -1;
	CString parentFullName;
	if (in != NULL)
	{
		in->GetFullName (parentFullName);
	}
	CRscClass *cl = NewClass (newName, false, parentFullName, inFavourites, favouriteIndex, NULL, NULL);
	if (cl != NULL)
	{
		int index = AddClass (cl, in, where, after);
		if (base != NULL)
		{
			cl->_baseClass = base;
			base->_derivedClasses.Add (cl);
		}
		SetModifiedFlag (TRUE);

		if (!_undoMode)
		{
			CUndoNewParam *undo = new CUndoNewParam (cl, index, inFavourites, favouriteIndex);
			AddCommandEntry (undo);
		}

		CUpdateDescriptor dscr;
		dscr._newParam._param = cl;
		dscr._newParam._inFavourites = inFavourites;
		UpdateAllViews (NULL, UH_NEW_PARAM, &dscr);
	}

	return cl;
}

CRscParamBase* CRscEditorDoc::FindParamInClass (const char *name, CRscClass *where, bool useDisabled)
{
	CRscParamBase *result = NULL;
	CArray<CRscParamBase*, CRscParamBase*> &arr = (where != NULL ? where->_innerParams : _params);
	int n = arr.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if ((arr[i]->_enabled || useDisabled) && arr[i]->_name == name)
		{
			result = arr[i];
		}
	}
	return result;
}

void CRscEditorDoc::VerifyBaseClassesInChildren (CRscParamBase *from, bool repair, CArray<CRscClass*, CRscClass*> &arr, CRscClass *defBaseClass, bool useDisabled)
{
	if (from != NULL)
	{
		CArray<CRscParamBase*, CRscParamBase*> &outPars = (from->_parent != NULL ? from->_parent->_innerParams : _params);
		int n = outPars.GetSize ();
		int i = 0;
		for (; ; ++i)
		{
			if (i == n)
			{
				return;
			}
			if (outPars[i] == from)
			{
				break;
			}
		}

		for (; i < n; ++i)
		{
			from = outPars[i];
			if (from->IsClass ())
			{
				if (((CRscClass*) from)->_baseClass != NULL && ((CRscClass*) from)->_enabled)
				{
					CRscClass *bc = FindBaseClassDefinition (((CRscClass*) from)->_baseClass->_name, (CRscClass*) from, ((CRscClass*) from)->_parent, useDisabled, false);
					if (bc == NULL)
					{
						bc = defBaseClass;
					}
					if (bc != ((CRscClass*) from)->_baseClass)
					{
						int n = arr.GetSize ();
						for (int i = 0; ; ++i)
						{
							if (i == n)
							{
								arr.Add ((CRscClass*) from);
								arr.Add (bc);
								break;
							}
							if (arr[i] == from)
							{
								break;
							}
						}
						if (repair)
						{
							((CRscClass*) from)->_baseClass->RemoveDerivedClass ((CRscClass*) from);
							((CRscClass*) from)->_baseClass = bc;
						}
					}
				}
				CArray<CRscParamBase*, CRscParamBase*> &inPars = ((CRscClass*) from)->_innerParams;
				if (inPars.GetSize () != 0)
				{
					VerifyBaseClassesInChildren (inPars[0], repair, arr, defBaseClass, useDisabled);
				}
			}
		}
	}
}

void CRscEditorDoc::VerifyBaseClassesInDeriveds (CRscClass *from, bool repair, CArray<CRscClass*, CRscClass*> &arr, CRscClass *defBaseClass, bool useDisabled)
{
	if (from->_innerParams.GetSize () != 0)
	{
		VerifyBaseClassesInChildren (from->_innerParams[0], repair, arr, defBaseClass, useDisabled);
	}

	int n = from->_derivedClasses.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		VerifyBaseClassesInDeriveds (from->_derivedClasses[i], repair, arr, defBaseClass, useDisabled);
	}
}

void CRscEditorDoc::VerifyBaseClasses (CRscParamBase *from, bool repair, CArray<CRscClass*, CRscClass*> &arr, CRscClass *defBaseClass, bool useDisabled)
{
	VerifyBaseClassesInChildren (from, repair, arr, defBaseClass, useDisabled);

	CRscClass *in = (from != NULL ? from->_parent : NULL);
	if (in != NULL)
	{
		int n = in->_derivedClasses.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			VerifyBaseClassesInDeriveds (in->_derivedClasses[i], repair, arr, defBaseClass, useDisabled);
		}
	}
}

CRscClass *CRscEditorDoc::FindClassDefinition (const char *name, CRscClass *in, bool useDisabled)
{
	if (in != NULL)
	{
		CRscParamBase *p = in->FindParamInBases (name, true, useDisabled);
		if (p == NULL || !p->IsClass ())
		{
			p = FindParamInParents (in, name, useDisabled);
			
			if (p == NULL || !p->IsClass ())
			{
				return NULL;
			}
		}
		
		return (CRscClass*) p;
	}
	else
	{
		if (_params.GetSize () != 0)
		{
			CRscParamBase *p = FindParamInChildren (name, _params[0], false, useDisabled);
			if (p == NULL || !p->IsClass ())
			{
				return NULL;
			}
			return (CRscClass*) p;
		}
		return NULL;
	}
}

void CRscEditorDoc::DeleteContents() 
{
	if (_curUndo != NULL)
	{
		CloseCommand ();
	}
	
	_monitorClass = NULL;
	UpdateAllViews (NULL, UH_DELETE_CONTENTS, NULL);

	int n = _includedFiles.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		delete _includedFiles[i];
	}
	_includedFiles.RemoveAll ();

	n = _params.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		delete _params[i];
	}
	_params.RemoveAll ();

	n = _macros.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		delete _macros[i];
	}
	_macros.RemoveAll ();

	n = _macroCalls.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		delete _macroCalls[i];
	}
	_macroCalls.RemoveAll ();

	n = _enums.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		delete _enums[i];
	}
	_enums.RemoveAll ();

	n = _undos.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		delete _undos[i];
	}
	_undos.RemoveAll ();

	n = _redos.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		delete _redos[i];
	}
	_redos.RemoveAll ();

	n = _groups.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		delete _groups[i];
	}
	_groups.RemoveAll ();

	n = _guides.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		delete _guides[i];
	}
	_guides.RemoveAll ();

	_selectedClasses.RemoveAll ();
	_displayedClasses.RemoveAll ();
	_favouriteClasses.RemoveAll ();

	CDocument::DeleteContents();
}

int CRscEditorDoc::GetNewIdValue ()
{
	CRscMacro *m;

	if (_macros.GetSize () > 0 && (m = _macros[0])->_macro == _T ("__ID_COUNTER"))
	{
		LPCTSTR beg = (LPCTSTR) m->_value;
		LPTSTR end;
		int r = _tcstol (beg, &end, 0);
		if (beg != end)
		{
			m->_value.Format (_T ("%d"), ++r);
			return r;
		}

		m->_value = _T ("1");
		return 1;
	}

	m = new CRscMacro (_T ("__ID_COUNTER"), NULL);
	if (m == NULL)
	{
		return 0;
	}
	m->_value = _T ("1");
	_macros.InsertAt (0, m);
	return 1;
}

CRscMacro *CRscEditorDoc::FindMacro (LPCTSTR id) const
{
	int n = _macros.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (_macros[i]->_macro == id)
		{
			return _macros[i];
		}
	}
	return NULL;
}

bool CRscEditorDoc::IsRscID (LPCTSTR id) const
{
	int n = _macros.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (_macros[i]->_macro == id)
		{
			return true;
		}
	}
	n = _enums.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (_enums[i]->Contains (id))
		{
			return true;
		}
	}
	return false;
}

void CRscEditorDoc::GetMacrosText (CString &text)
{
	text.Empty ();
	int n = _macros.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		CRscMacro *mac = _macros[i];
		if (!mac->IsIncluded ())
		{
			int m = mac->_preComments.GetSize ();
			for (int j = 0; j < m; ++j)
			{
				text += mac->_preComments[j];
				text += _T ("\r\n");
			}
			text += _T ("#define ");
			text += mac->_macro;
			if (mac->_paramList != NULL)
			{
				text += _T ('(');
				int m = mac->_paramList->_params.GetSize ();
				if (m > 0)
				{
					text += mac->_paramList->_params[0];
					int j = 1;
					while (j < m)
					{
						text += _T (", ");
						text += mac->_paramList->_params[j++];
					}
				}
				text += _T (')');
			}
			
			text += _T (' ');
			text += mac->_value;
			text += _T ("\r\n");
		}
	}
}

BOOL CRscEditorDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	
	if (_docModified)
	{
		SetModifiedFlag (TRUE);
	}
	
	_isSaved = TRUE;
	_everSavedFlag = TRUE;
	int n = _undos.GetSize ();
	_lastSavedPos = n != 0 ? _undos[n - 1] : NULL;
	return TRUE;
}

CRscClass *CRscEditorDoc::GetClassFromFullName (CString &fullName, CString *oldClass, CString *newClass)
{
	if (oldClass != NULL && newClass != NULL)
	{
		int len = fullName.GetLength ();
		int oldLen = oldClass->GetLength ();
		if (oldLen <= len)
		{
			for (int i = 0; ; ++i)
			{
				if (i == oldLen)
				{
					fullName = *newClass + fullName.Right (len - oldLen);
					break;
				}

				if (fullName[i] != (*oldClass)[i])
				{
					break;
				}
			}
		}
	}

	CRscEditorStringInput in (fullName);
	CRscEditorLex lex (&in, CRscEditorLex::rscEditorKeywords, CRscEditorLex::rscEditorKeywordsLength);
	CRscEditorToken tok;
	lex.GetNextToken (&tok, NULL, CRscEditorLex::flagSkipWhiteChars);
	
	CRscClass *rc = NULL;
	while (tok.type == ':')
	{
		lex.GetNextToken (&tok, NULL, CRscEditorLex::flagSkipWhiteChars);
		
		if (tok.type != ':')
		{
			return NULL;
		}
		lex.GetAlphaWord (&tok); // lex.GetNextToken (&tok, NULL, CRscEditorLex::flagSkipWhiteChars);
		
		if (tok.type != rscEditorLexId) // class name can be an integer
		{
			return NULL;
		}
		CRscParamBase *pb = FindParamInClass (tok.string, rc, false);
		if (pb == NULL || !pb->IsClass ())
		{
			return NULL;
		}
		rc = (CRscClass*) pb;
		lex.GetNextToken (&tok, NULL, CRscEditorLex::flagSkipWhiteChars);
	}

	return rc;
}

BOOL CRscEditorDoc::RouteCmdToAllViews (CView *pView, UINT nID, int nCode, void *pExtra, AFX_CMDHANDLERINFO *pHandlerInfo)
{
	POSITION poz = GetFirstViewPosition ();
	while (poz != NULL)
	{
		CView *pNextView = GetNextView (poz);
		if (pNextView != pView)
		{
			if (pNextView->OnCmdMsg (nID, nCode, pExtra, pHandlerInfo))
			{
				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CRscEditorDoc::IsSaved()
{
	return IsModified () ? FALSE : _isSaved;
}

void CRscEditorDoc::SetSaved (BOOL isSaved, BOOL isModified)
{
	_isSaved = isSaved;
	SetModifiedFlag (isModified);
}

BOOL CRscEditorDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	BOOL r = CDocument::OnSaveDocument(lpszPathName);
	if (r)
	{
		_isSaved = TRUE;
	}
	_everSavedFlag = TRUE;
	int n = _undos.GetSize ();
	_lastSavedPos = n != 0 ? _undos[n - 1] : NULL;
	return r;
}

CRscParamBase* CRscEditorDoc::FindPrevParam (CRscClass *parent, CRscParamBase *param)
{
	if (param != NULL)
	{
		parent = param->_parent;
	}
	else
	{
		CArray<CRscParamBase*, CRscParamBase*> &arr = (parent != NULL ? parent->_innerParams : _params);
		int size = arr.GetSize ();
		return size > 0 ? arr[size - 1] : NULL;
	}
	
	CRscParamBase *res = NULL;
	CArray<CRscParamBase*, CRscParamBase*> &arr = (param->_parent != NULL ? param->_parent->_innerParams : _params);
	int n = arr.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (arr[i] == param)
		{
			return res;
		}
		res = arr[i];
	}
	return NULL;
}

CRscClass* CRscEditorDoc::FindPrevFavourite (CRscClass *cl)
{
	int size = _favouriteClasses.GetSize ();
	CRscClass *res = NULL;
	for (int i = 0; i < size; ++i)
	{
		if (_favouriteClasses[i] == cl)
		{
			return res;
		}
		res = _favouriteClasses[i];
	}
	return NULL;
}

CRscParamBase* CRscEditorDoc::FindParamInParents (CRscParamBase *from, const char *name, bool useDisabled)
{
	if (from == NULL)
	{
		return NULL;
	}

	CRscClass *cur = from->_parent;
	CArray<CRscParamBase*, CRscParamBase*> &arr = (cur != NULL ? cur->_innerParams : _params);
	int m = arr.GetSize ();
	int n = 0;
	for (; ; ++n)
	{
		if (m == n)
		{
			return NULL;
		}
		if (arr[n] == from)
		{
			break;
		}
	}
	for (int i = n - 1; i >= 0; --i)
	{
		if ((arr[i]->_enabled || useDisabled) && arr[i]->_name.CompareNoCase (name) == 0)
		{
			return arr[i];
		}
	}

	while (cur != NULL)
	{
		cur = cur->_parent;
		CArray<CRscParamBase*, CRscParamBase*> &arr = (cur != NULL ? cur->_innerParams : _params);
		n = arr.GetSize ();
		for (int i = n - 1; i >= 0; --i)
		{
			if ((arr[i]->_enabled || useDisabled) && arr[i]->_name.CompareNoCase (name) == 0)
			{
				return arr[i];
			}
		}
	}
	
	return NULL;
}

bool CRscEditorDoc::SetArrayStringParamInBases (CRscClass *cl, const char *name, LPCTSTR val, bool inBases, bool useDisabled, bool canUndo)
{
	if (cl->SetArrayStringParamInBases (name, val, inBases, useDisabled, this, canUndo))
	{
		SetModifiedFlag (TRUE);
		return true;
	}
	return false;
}

bool CRscEditorDoc::SetFloatParamInBases (CRscClass *cl, const char *name, double val, bool inBases, bool useDisabled, bool canUndo, bool forceUndo)
{
	if (cl->SetFloatParamInBases (name, val, inBases, useDisabled, this, canUndo, forceUndo))
	{
		SetModifiedFlag (TRUE);
		return true;
	}
	return false;
}

bool CRscEditorDoc::OpenCommand (LPCTSTR name, bool postPreviewUpdate, EUndoType curUndoType)
{
	if (_curUndo != NULL)
	{
		CloseCommand ();
	}

	ASSERT (_curUndo == NULL);
	
	_curUndo = new CUndoCommand (name, postPreviewUpdate);
	if (_curUndo == NULL)
	{
		return false;
	}

	_curUndoType = curUndoType;
	return true;
}

void CRscEditorDoc::CloseCommand ()
{
	ASSERT (_curUndo != NULL);

	if (_curUndo->_entries.GetSize () == 0)
	{
		delete _curUndo;
		_curUndo = NULL;
		return;
	}

	int size = _redos.GetSize ();
	for (int i = 0; i < size; ++i)
	{
		delete _redos[i];
	}
	_redos.RemoveAll ();

	_undos.Add (_curUndo);
	_curUndo = NULL;
	_curUndoType = UNDO_NONE;
}

void CRscEditorDoc::DisposeCommand ()
{
	ASSERT (_curUndo != NULL);

	delete _curUndo;
	_curUndo = NULL;
}

void CRscEditorDoc::AddCommandEntry (CUndoEntry *undo)
{
	ASSERT (_curUndo != NULL);

	if (undo != NULL)
	{
		_curUndo->_entries.Add (undo);
	}
}

void CRscEditorDoc::SetUndoMode ()
{
	_undoMode = true;
}

void CRscEditorDoc::SetNormalMode ()
{
	_undoMode = false;
}

void CRscEditorDoc::DeleteItem (CRscClass *cl, bool *tags, bool displayMsg)
{
	CRscItemEraser er (this, cl, tags);
	if (!er.EnumerateClass (_monitorClass, NULL) && displayMsg)
	{
		CString str;
		str.Format (_T ("Item \"%s\"could not be removed."), cl->_name);
		AfxMessageBox (str, MB_OK);
	}

	/*??
	if (redraw)
	{
		_curSpaceCtrl->Invalidate (FALSE);
		_curSpaceCtrl->UpdateWindow ();
		_preview->_workViewCtrl.Invalidate (FALSE);
		_preview->_workViewCtrl.UpdateWindow ();
	}
	*/
}

bool CRscEditorDoc::AskUserAboutBaseClasses (CArray<CRscClass*, CRscClass*> &arr)
{
	int n = arr.GetSize ();
	if (n > 0)
	{
		CString fullName;
		CString msg (_T ("This operations has these side effects. Do you want to continue?"));
		for (int i = 0; i < n; i += 2)
		{
			msg += _T ("\r\nBase class of \"");
			msg += arr[i]->GetFullName (fullName);
			if (arr[i + 1] != NULL)
			{
				msg += _T ("\" will be changed.");
			}
			else
			{
				msg += _T ("\" will be removed.");
			}
		}
		switch (AfxMessageBox (msg, MB_YESNO))
		{
		case IDYES:
			{
				CString str;
				for (int i = 0; i < n; i += 2)
				{
					CRscClass *change = arr[i];
					CRscClass *oldBase = change->_baseClass;
					CRscClass *newBase = arr[i + 1];
					oldBase->RemoveDerivedClass (change);
					change->_baseClass = newBase;
					if (newBase != NULL)
					{
						newBase->_derivedClasses.Add (change);
					}
					SetModifiedFlag (TRUE);

					if (!_undoMode)
					{
						CUndoChangeBase *undo = new CUndoChangeBase (change, oldBase, newBase);
						AddCommandEntry (undo);
					}

					CUpdateDescriptor dscr;
					dscr._newBase._class = change;
					UpdateAllViews (NULL, UH_NEW_BASE, &dscr);

					double val;
					if (!change->GetFloatParamInBases ("x", val, true, NULL, false) &&
						oldBase->GetFloatParamInBases ("x", val, false, NULL, true))
					{
						SetFloatParamInBases (change, "x", val, false, false, true, true);
					}
					if (!change->GetFloatParamInBases ("y", val, true, NULL, false) &&
						oldBase->GetFloatParamInBases ("y", val, false, NULL, true))
					{
						SetFloatParamInBases (change, "y", val, false, false, true, true);
					}
					if (!change->GetFloatParamInBases ("w", val, true, NULL, false) &&
						oldBase->GetFloatParamInBases ("w", val, false, NULL, true))
					{
						SetFloatParamInBases (change, "w", val, false, false, true, true);
					}
					if (!change->GetFloatParamInBases ("h", val, true, NULL, false) &&
						oldBase->GetFloatParamInBases ("h", val, false, NULL, true))
					{
						SetFloatParamInBases (change, "h", val, false, false, true, true);
					}
				}
			}
			return true;
			
		default:
			return false;
		}
	}
	return true;
}

void CRscEditorDoc::RemoveFromGroup (CRscClass *cl)
{
	CRscGroup *gr = cl->_group;
	if (gr != NULL)
	{
		int n = gr->_members.GetSize ();
		if (n < 3)
		{
			Ungroup (gr);
		}
		else
		{
			for (int i = 0; i < n; )
			{
				if (gr->_members[i] == cl)
				{
					gr->_members.RemoveAt (i);
					((CRscClass*) cl)->_group = NULL;
					SetModifiedFlag (TRUE);
					
					if (!_undoMode)
					{
						CUndoRemoveFromGroup *undo = new CUndoRemoveFromGroup ((CRscClass*) cl, gr, i);
						AddCommandEntry (undo);
					}
					--n;
				}
				else
				{
					++i;
				}
			}
		}
	}
}

void CRscEditorDoc::PrepareParamForDelete (CRscParamBase *cl, CRscClass *newClass, CRscClass *newBaseClass)
{
	if (_monitorClass == cl)
	{
		CRscClass *oldClass = _monitorClass;
		_monitorClass = newClass;
		SetModifiedFlag (TRUE);

		if (!_undoMode)
		{
			CUndoChangeMonitor *undo = new CUndoChangeMonitor (oldClass, newClass);
			AddCommandEntry (undo);
		}

		CUpdateDescriptor dscr;
		dscr._changeMonitor._oldOne = oldClass;
		dscr._changeMonitor._newOne = newClass;
		UpdateAllViews (NULL, UH_CHANGE_MONITOR, &dscr);
	}

	if (cl->IsClass ())
	{
		DeleteGuide ((CRscClass*) cl);
		Ungroup ((CRscClass*) cl);
		RemoveFromGroup ((CRscClass*) cl);
		if (((CRscClass*) cl)->_selected)
		{
			int n = _selectedClasses.GetSize ();
			for (int i = 0; i < n; ++i)
			{
				if (_selectedClasses[i] != cl)
				{
					continue;
				}
				_selectedClasses.RemoveAt (i);
				break;
			}
			((CRscClass*) cl)->_selected = false;
		}

		if (((CRscClass*) cl)->_displayed)
		{
			int n = _displayedClasses.GetSize ();
			for (int i = 0; i < n; ++i)
			{
				if (_displayedClasses[i] != cl)
				{
					continue;
				}
				_displayedClasses.RemoveAt (i);
				break;
			}
			((CRscClass*) cl)->_displayed = false;
		}

		if (((CRscClass*) cl)->_baseClass != NULL)
		{
			CRscClass *old = ((CRscClass*) cl)->_baseClass;
			CArray<CRscClass*, CRscClass*> &arr = old->_derivedClasses;
			int n = arr.GetSize ();
			for (int i = 0; i < n; ++i)
			{
				if (arr[i] != cl)
				{
					continue;
				}
				
				arr.RemoveAt (i);
				break;
			}
			((CRscClass*) cl)->_baseClass = NULL;
			SetModifiedFlag (TRUE);

			if (!_undoMode)
			{
				CUndoChangeBase *undo = new CUndoChangeBase ((CRscClass*) cl, old, NULL);
				AddCommandEntry (undo);
			}

			// I don't have to update text of this class, 
			// because this one is about to be deleted
		}

		int n = ((CRscClass*) cl)->_derivedClasses.GetSize ();
		CString str;
		for (int i = 0; i < n; ++i)
		{
			CRscClass *dc = ((CRscClass*) cl)->_derivedClasses[i];
			dc->_baseClass = newBaseClass;
			if (newBaseClass != NULL)
			{
				newBaseClass->_derivedClasses.Add (dc);
			}
			SetModifiedFlag (TRUE);

			if (!_undoMode)
			{
				CUndoChangeBase *undo = new CUndoChangeBase (dc, (CRscClass*) cl, newBaseClass);
				AddCommandEntry (undo);
			}

			CUpdateDescriptor dscr;
			dscr._newBase._class = dc;
			UpdateAllViews (NULL, UH_NEW_BASE, &dscr);
		}
		((CRscClass*) cl)->_derivedClasses.RemoveAll ();
		
		if (((CRscClass*) cl)->_favouriteTreeItem != NULL)
		{
			if (newClass == NULL)
			{
				n = _favouriteClasses.GetSize ();
				for (int i = 0; i < n; ++i)
				{
					if (_favouriteClasses[i] == cl)
					{
						_favouriteClasses.RemoveAt (i);
						SetModifiedFlag (TRUE);

						if (!_undoMode)
						{
							CUndoRemoveFavourite *undo = new CUndoRemoveFavourite ((CRscClass*) cl, i);
							AddCommandEntry (undo);
						}

						CUpdateDescriptor dscr;
						dscr._remFavourite._class = (CRscClass*) cl;
						UpdateAllViews (NULL, UH_REMOVE_FAVOURITE, &dscr);
						break;
					}
				}
			}
			else
			{
				n = _favouriteClasses.GetSize ();
				for (int i = 0; i < n; ++i)
				{
					if (_favouriteClasses[i] == cl)
					{
						_favouriteClasses[i] = newClass;
						SetModifiedFlag (TRUE);

						if (!_undoMode)
						{
							CUndoChangeFavourite *undo = new CUndoChangeFavourite (i, (CRscClass*) cl, newClass);
							AddCommandEntry (undo);
						}

						CUpdateDescriptor dscr;
						dscr._changeFavourite._oldClass = (CRscClass*) cl;
						dscr._changeFavourite._newClass = newClass;
						UpdateAllViews (NULL, UH_CHANGE_FAVOURITE, &dscr);
						break;
					}
				}
			}
		}

		int innerN = ((CRscClass*) cl)->_innerParams.GetSize ();
		for (int innerI = 0; innerI < innerN; ++innerI)
		{
			PrepareParamForDelete (((CRscClass*) cl)->_innerParams[innerI], NULL, NULL);
		}
	}

	CUpdateDescriptor dscr;
	dscr._delParam._param = cl;
	UpdateAllViews (NULL, UH_DEL_PARAM, &dscr);
}

void CRscEditorDoc::DeleteParam (CRscParamBase *param)
{
	param->Enable (false);
	CArray<CRscClass*, CRscClass*> arr;
	VerifyBaseClasses (param, false, arr, !param->IsClass () ? NULL : ((CRscClass*) param)->_baseClass, false);
	
	if (!AskUserAboutBaseClasses (arr))
	{
		param->Enable (true);
		return;
	}

	CArray<CRscParamBase*, CRscParamBase*> &outPars = (param->_parent != NULL ? param->_parent->_innerParams : _params);
	int size = outPars.GetSize ();
	int i = 0;
	for (; ; ++i)
	{
		if (i == size)
		{
			param->Enable (true);
			return;
		}
		if (outPars[i] == param)
		{
			break;
		}
	}

	PrepareParamForDelete (param, NULL, NULL);
	outPars.RemoveAt (i);
	SetModifiedFlag (TRUE);

	param->Enable (true);
	if (!_undoMode)
	{
		CUndoDeleteParam *undo = new CUndoDeleteParam (param, i);
		AddCommandEntry (undo);
	}
	// Views all already updated ... this happened in PrepareParamForDelete
}

bool CRscEditorDoc::ApplyClassCode (CString &fullCode, CRscParamBase *dst, bool inFavourites, CString *outCode)
{
	CRscEditorStringInput in (fullCode);
	CRscEditorLex lex (&in, &CRscEditorLex::rscEditorKeywords[0], CRscEditorLex::rscEditorKeywordsLength);
	CRscEditorToken tok;
	bool showRemoveCommentBox = true;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_REMOVE_COMMENT_BOX, showRemoveCommentBox);
	CRscEditorCodeParser parser (NULL, this, NULL, true, false, true, showRemoveCommentBox, NULL);
	// Remember Macro
	lex.GetNextToken (&tok, &parser, parser._basicSaveCommentsLexFlags);
	dst->Enable (false);
	CArray<CRscParamBase*, CRscParamBase*> &arr = (dst->_parent != NULL ? dst->_parent->_innerParams: _params);
	int size = arr.GetSize ();
	int index = 0;
	for (; ; ++index)
	{
		if (index == size)
		{
			return false;
		}
		if (arr[index] == dst)
		{
			break;
		}
	}

	int outIndex = index;
	CString *rename = NULL;
	// Macro: koser?
	if (!parser.ParsePARAMETER (lex, tok, arr, outIndex, dst->_parent, false, rename, NULL, NULL) ||
		tok.type != rscEditorLexEof || outIndex != index + 1)
	{
		TCHAR buf[64];
		_stprintf (buf, _T ("Error at line %u."), parser._errorFileLine);
		AfxMessageBox (buf, MB_OK);
		while (arr.GetSize () > size)
		{
			delete arr[index];
			arr.RemoveAt (index);
		}
		dst->Enable (true);
		return false;
	}

	if (arr.GetSize () == size)
	{
		AfxMessageBox (_T ("No class compiled."), MB_OK);
		dst->Enable (true);
		return false;
	}

	if ((arr[index]->IsClass () ^ dst->IsClass ()) != 0)
	{
		AfxMessageBox (_T ("Output code is not compatible to input code."), MB_OK);
		delete arr[index];
		arr.RemoveAt (index);
		dst->Enable (true);
		return false;
	}

	if (outCode != NULL)
	{
		CString tab ("\t");
		((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::TABULATOR, tab);
		LPTSTR text = outCode->GetBuffer (arr[index]->GetTextLength (tab));
		arr[index]->GetText (text, tab);
		outCode->ReleaseBuffer ();
	}

	if (dst->IsClass () && arr[index]->IsClass ())
	{
		int n = ((CRscClass*) dst)->_valueReferences.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CRscAttributeValueElement *elem = ((CRscClass*) dst)->_valueReferences[i];
			if (elem != NULL)
			{
				elem->_value = arr[index]->_name;
			}
		}
		if (((CRscClass*) dst)->_selected)
		{
			SelectClass ((CRscClass*) arr[index], true);
			AddCommandEntry (new CUndoSelectClass ((CRscClass*) dst, false, true));
			AddCommandEntry (new CUndoSelectClass ((CRscClass*) arr[index], true, false));
		}
		PrepareParamForDelete (dst, (CRscClass*) arr[index], (CRscClass*) arr[index]);
	}
	else
	{
		PrepareParamForDelete (dst, NULL, NULL);
	}
	arr.RemoveAt (outIndex);
	SetModifiedFlag (TRUE);

	dst->Enable (true);
	if (!_undoMode)
	{
		CUndoChangeClassCode *undo = new CUndoChangeClassCode (dst, arr[index], index);
		AddCommandEntry (undo);
	}
	// Everything is updated in PrepareParamForDelete ... 
	// ... link in favourite class tree is changed
	// .... what else? ... link in class tree!

	CUpdateDescriptor dscr;
	dscr._changeCode._param = arr[index];
	dscr._changeCode._inFavourites = inFavourites;
	UpdateAllViews (NULL, UH_CHANGE_CODE, &dscr);
	return true;
}

bool CRscEditorDoc::InsertParam (CString &fullCode, bool isClass, CRscParamBase *&outParam)
{
	outParam = NULL;

	int comma = fullCode.Find (_T (','));
	if (comma < 0)
	{
		return false;
	}
	CString srcDir = fullCode.Left (comma);

	++comma;
	int nameEnd = fullCode.Find (_T (';'), comma);
	if (nameEnd < 0)
	{
		return false;
	}
	CString srcName = fullCode.Mid (comma, nameEnd - comma);
	
	bool rename;
	int renameIndex = 0;
	CString newName;
	do {
		rename = false;
		if (renameIndex > 0)
		{
			newName.Format (_T ("CopyOf_%s_%d"), srcName, renameIndex);
		}
		else
		{
			newName = srcName;
		}
		
		if (FindParamInClass (newName, NULL, false) != NULL)
		{
			rename = true;
			++renameIndex;
		}
	} while (rename);

	CRscEditorStringInput in (fullCode, nameEnd + 1);
	CRscEditorLex lex (&in, CRscEditorLex::rscEditorKeywords, CRscEditorLex::rscEditorKeywordsLength);
	CRscEditorToken tok;
	bool showRemoveCommentBox = true;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_REMOVE_COMMENT_BOX, showRemoveCommentBox);
	CRscEditorCodeParser parser (NULL, this, NULL, false, false, true, showRemoveCommentBox, NULL);
	// Remember Macro
	lex.GetNextToken (&tok, &parser, parser._basicSaveCommentsLexFlags);
	CString newClass;
	CString oldClass;
	if (isClass)
	{
		newClass += _T ("::") + newName;
		oldClass = srcDir + _T ("::") + srcName;
		parser._oldClass = &oldClass;
		parser._newClass = &newClass;
	}
	int index = _params.GetSize ();
	int outIndex = index;
	// Macro: koser?
	if (!parser.ParsePARAMETERS (lex, tok, _params, 
		outIndex, NULL, false, &newName, NULL, NULL) || _params.GetSize () != index + 1)
	{
		while (_params.GetSize () > index)
		{
			delete _params[index];
			_params.RemoveAt (index);
		}
		return false;
	}
	else
	{
		_params[index]->_name = newName;
		
		CArray<CRscClass*, CRscClass*> arr;
		VerifyBaseClasses (_params[index], false, arr, !_params[index]->IsClass () ? NULL : ((CRscClass*) _params[index])->_baseClass, false);
		
		if (!AskUserAboutBaseClasses (arr))
		{
			delete _params[index];
			_params.RemoveAt (index);
			return false;
		}
		outParam = _params[index];
		SetModifiedFlag (TRUE);

		if (!_undoMode)
		{
			CUndoNewParam *undo = new CUndoNewParam (_params[index], index, false, -1);
			AddCommandEntry (undo);
		}

		CUpdateDescriptor dscr;
		dscr._newParam._param = _params[index];
		dscr._newParam._inFavourites = false;
		UpdateAllViews (NULL, UH_NEW_PARAM, &dscr);
		return true;
	}
}

bool CRscEditorDoc::MoveCopyParam (CRscParamBase *src, CRscClass *parent, 
								   CRscParamBase *where, CString &fullCode, 
								   bool after, bool move, bool isClass, CRscParamBase *&outParam)
{
	outParam = NULL;
	if (where != NULL)
	{
		parent = where->_parent;
	}

	if (src != NULL)
	{
		move &= src->IsEditable ();
	}
	else
	{
		move = false;
	}

	int comma = fullCode.Find (_T (','));
	if (comma < 0)
	{
		return false;
	}
	CString srcDir = fullCode.Left (comma);

	++comma;
	int nameEnd = fullCode.Find (_T (';'), comma);
	if (nameEnd < 0)
	{
		return false;
	}
	CString srcName = fullCode.Mid (comma, nameEnd - comma);
	
	CRscParamBase *prev = (after ? where : FindPrevParam (parent, where));
	if (move)
	{
		if (prev == src)
		{
			outParam = src;
			return true;
		}
		src->Enable (false);
	}

	bool rename;
	int renameIndex = 0;
	CString newName;
	do {
		rename = false;
		if (renameIndex > 0)
		{
			newName.Format (_T ("CopyOf_%s_%d"), srcName, renameIndex);
		}
		else
		{
			newName = srcName;
		}
		
		if (FindParamInClass (newName, parent, false) != NULL)
		{
			rename = true;
			++renameIndex;
		}
	} while (rename);

	CRscEditorStringInput in (fullCode, nameEnd + 1);
	CRscEditorLex lex (&in, CRscEditorLex::rscEditorKeywords, CRscEditorLex::rscEditorKeywordsLength);
	CRscEditorToken tok;
	bool showRemoveCommentBox = true;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_REMOVE_COMMENT_BOX, showRemoveCommentBox);
	CRscEditorCodeParser parser (NULL, this, NULL, false, false, true, showRemoveCommentBox, NULL);
	// Remember Macro
	lex.GetNextToken (&tok, &parser, parser._basicSaveCommentsLexFlags);
	CString newClass;
	CString oldClass;
	if (isClass)
	{
		if (parent != NULL)
		{
			parent->GetFullName (newClass);
		}
		newClass += _T ("::") + newName;
		oldClass = srcDir + _T ("::") + srcName;
		parser._oldClass = &oldClass;
		parser._newClass = &newClass;
	}
	CArray<CRscParamBase*, CRscParamBase*> &outPars = (parent != NULL ? parent->_innerParams : _params);
	int size = outPars.GetSize ();
	int index = 0;
	if (prev != NULL)
	{
		for (; ; ++index)
		{
			if (index == size)
			{
				if (move)
				{
					src->Enable (true);
				}
				return false;
			}
			if (outPars[index] == prev)
			{
				++index;
				break;
			}
		}
	}
	int outIndex = index;
	// Macro: koser?
	if (!parser.ParsePARAMETERS (lex, tok, outPars, outIndex, parent, false, &newName, NULL, NULL) ||
		outPars.GetSize () != size + 1)
	{
		while (outPars.GetSize () > size)
		{
			delete outPars[index];
			outPars.RemoveAt (index);
		}
		if (move)
		{
			src->Enable (true);
		}
		return false;
	}
	else
	{
		CArray<CRscParamBase*, CRscParamBase*> *srcPars = NULL;
		int srcSize = 0;
		int srcIndex = 0;
		if (src != NULL)
		{
			srcPars = (src->_parent != NULL ? &src->_parent->_innerParams : &_params);
			srcSize = srcPars->GetSize ();
			for (; ; ++srcIndex)
			{
				if (srcIndex == srcSize)
				{
					if (move)
					{
						src->Enable (true);
					}
					return false;
				}
				if ((*srcPars)[srcIndex] == src)
				{
					break;
				}
			}
		}

		outPars[index]->_name = newName;
		
		if (move && src->IsClass () && outPars[index]->IsClass ())
		{
			int n = ((CRscClass*) src)->_derivedClasses.GetSize ();
			for (int i = 0; i < n; ++i)
			{
				CRscClass *dc = ((CRscClass*) src)->_derivedClasses[i];
				dc->_baseClass = (CRscClass*) outPars[index];
				((CRscClass*) outPars[index])->_derivedClasses.Add (dc);
			}
			((CRscClass*) src)->_derivedClasses.RemoveAll ();
		}
		CArray<CRscClass*, CRscClass*> arr;
		VerifyBaseClasses (outPars[index], false, arr, 
			!outPars[index]->IsClass () ? NULL : ((CRscClass*) outPars[index])->_baseClass, false);
		if (src != NULL && srcIndex + 1 < srcSize)
		{
			VerifyBaseClasses ((*srcPars)[srcIndex + 1], false, arr, 
				!(*srcPars)[srcIndex + 1]->IsClass () ? NULL : ((CRscClass*) (*srcPars)[srcIndex + 1])->_baseClass,
				false);
		}
		
		if (!AskUserAboutBaseClasses (arr))
		{
			if (move)
			{
				if (src->IsClass () && outPars[index]->IsClass ())
				{
					int n = ((CRscClass*) outPars[index])->_derivedClasses.GetSize ();
					for (int i = 0; i < n; ++i)
					{
						CRscClass *dc = ((CRscClass*) outPars[index])->_derivedClasses[i];
						dc->_baseClass = (CRscClass*) src;
						((CRscClass*) src)->_derivedClasses.Add (dc);
					}
					((CRscClass*) outPars[index])->_derivedClasses.RemoveAll ();
				}
				src->Enable (true);
			}
			delete outPars[index];
			outPars.RemoveAt (index);
			return false;
		}
		
		SetModifiedFlag (TRUE);
		outParam = outPars[index];

		if (!_undoMode)
		{
			CUndoNewParam *undo = new CUndoNewParam (outParam, index, false, -1);
			AddCommandEntry (undo);
		}

		CUpdateDescriptor dscr;
		dscr._newParam._param = outParam;
		dscr._newParam._inFavourites = false;
		UpdateAllViews (NULL, UH_NEW_PARAM, &dscr);

		if (move)
		{
			PrepareParamForDelete (src, outPars[index]->IsClass () ? (CRscClass*) outPars[index] : NULL, NULL);
			srcPars->RemoveAt (srcIndex);

			src->Enable (true);
			if (!_undoMode)
			{
				CUndoDeleteParam *undo = new CUndoDeleteParam (src, srcIndex);
				AddCommandEntry (undo);
			}
			// Views all already updated ... this happened in PreparParamForDelete
		}
		return true;
	}
}

bool CRscEditorDoc::CopyItem (const char *group, CString &fullCode, double x, double y)
{
	if (_monitorClass == NULL || !_monitorClass->IsInsideEditable ())
	{
		return false;
	}
	
	int size = _monitorClass->_innerParams.GetSize ();
	CRscParamBase *last = (size != 0 ? _monitorClass->_innerParams[size - 1] : NULL);
	CRscParamBase *rc;
	if (!MoveCopyParam (NULL, _monitorClass, last, fullCode, last != NULL, false, true, rc))
	{
		return false;
	}
	SetModifiedFlag (TRUE);

	SetArrayStringParamInBases (_monitorClass, group, rc->_name, true, false, true);
	if (rc->IsClass ())
	{
		SetFloatParamInBases ((CRscClass*) rc, "x", x, false, false, true, false);
		SetFloatParamInBases ((CRscClass*) rc, "y", y, false, false, true, false);
	}
	return true;
}

bool CRscEditorDoc::SetMonitorClass (CRscClass *cl)
{
	if (_monitorClass == cl)
	{
		return false;
	}

	SelectAllClasses (false);
	int n = _displayedClasses.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		_displayedClasses[i]->_displayed = false;
	}
	_displayedClasses.RemoveAll ();

	CRscClass *old = _monitorClass;
	_monitorClass = cl;
	SetModifiedFlag (TRUE);
	
	if (!_undoMode)
	{
		CUndoChangeMonitor *undo = new CUndoChangeMonitor (old, cl);
		AddCommandEntry (undo);
	}

	CUpdateDescriptor dscr;
	dscr._changeMonitor._oldOne = old;
	dscr._changeMonitor._newOne = cl;
	UpdateAllViews (NULL, UH_CHANGE_MONITOR, &dscr);
	return true;
}

void CRscEditorDoc::AddFavourite (CRscClass *cl, CRscClass *prev)
{
	int index = 0;
	int n = _favouriteClasses.GetSize ();
	if (prev != NULL)
	{
		for (; ; ++index)
		{
			if (index == n)
			{
				return;
			}
			if (_favouriteClasses[index] == prev)
			{
				++index;
				break;
			}
		}
	}
	_favouriteClasses.InsertAt (index, cl);
	SetModifiedFlag (TRUE);

	if (!_undoMode)
	{
		CUndoAddFavourite *undo = new CUndoAddFavourite (cl, index);
		AddCommandEntry (undo);
	}

	CUpdateDescriptor dscr;
	dscr._insertFavourite._class = cl;
	UpdateAllViews (NULL, UH_INSERT_FAVOURITE, &dscr);
}

bool CRscEditorDoc::RemoveFavourite (CRscClass *cl)
{
	int size = _favouriteClasses.GetSize ();
	for (int index = 0; index < size; ++index)
	{
		if (_favouriteClasses[index] == cl)
		{
			_favouriteClasses.RemoveAt (index);
			SetModifiedFlag (TRUE);

			if (!_undoMode)
			{
				CUndoRemoveFavourite *undo = new CUndoRemoveFavourite (cl, index);
				AddCommandEntry (undo);
			}

			CUpdateDescriptor dscr;
			dscr._remFavourite._class = cl;
			UpdateAllViews (NULL, UH_REMOVE_FAVOURITE, &dscr);
			return true;
		}
	}
	return false;
}

void CRscEditorDoc::MakeClassIndependent (CRscClass *cl)
{
	if (cl == NULL || !cl->IsInsideEditable ())
	{
		return;
	}

	double val;
	// x
	if (cl->GetFloatParamInBases ("x", val, true, NULL, false))
	{
		SetFloatParamInBases (cl, "x", val, false, false, true, false);
	}
	// y
	if (cl->GetFloatParamInBases ("y", val, true, NULL, false))
	{
		SetFloatParamInBases (cl, "y", val, false, false, true, false);
	}
	// w
	if (cl->GetFloatParamInBases ("w", val, true, NULL, false))
	{
		SetFloatParamInBases (cl, "w", val, false, false, true, false);
	}
	// h
	if (cl->GetFloatParamInBases ("h", val, true, NULL, false))
	{
		SetFloatParamInBases (cl, "h", val, false, false, true, false);
	}

	int nn = cl->_derivedClasses.GetSize ();
	for (int ii = 0; ii < nn; ++ii)
	{
		MakeClassIndependent (cl->_derivedClasses[ii]);
	}
}

void CRscEditorDoc::ClearHistory ()
{
	int n = _undos.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		delete _undos[i];
	}
	_undos.RemoveAll ();

	n = _redos.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		delete _redos[i];
	}
	_redos.RemoveAll ();
}

bool CRscEditorDoc::IncludeFile (CString &fileName)
{
	CRscInclude *incl = new CRscInclude (fileName);
	if (incl == NULL)
	{
		ASSERT (0);
		return false;
	}

	CFile ar;
	if (!ar.Open (fileName, CFile::modeRead | CFile::shareDenyWrite))
	{
		TCHAR buf[512];
		_stprintf (buf, _T ("File \"%s\" can not be opened for reading."), fileName);
		ASSERT (0);
		AfxMessageBox (buf, MB_OK);
		return false;
	}

	bool unwrapIncludedMacros = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::UNWRAP_INCLUDED_MACROS, unwrapIncludedMacros);
	CRscEditorDocParser parser (fileName, this, NULL, true, unwrapIncludedMacros, false, false, incl, incl->_subInclude);
	CRscEditorFileInput in (&ar);
	CRscEditorLex lex (&in, CRscEditorLex::rscEditorKeywords, CRscEditorLex::rscEditorKeywordsLength);
	CRscEditorToken tok;
	// Remember Macro
	lex.GetNextToken (&tok, &parser, parser._basicSaveCommentsHashLexFlags);
	int size = _params.GetSize ();
	int index = 0;
	for (; index < size && _params[index]->IsIncluded (); ++index);
	int favouriteIndex = _favouriteClasses.GetSize ();
	int outIndex = index;
	// Macro: koser?
	if (!parser.ParseDOC (lex, tok, outIndex, true))
	{
		ar.Close ();
		delete incl;
		TCHAR buf[512];
		_stprintf (buf, _T ("Error during parsing include file \"%s\" at line %u."), parser._errorFileName, parser._errorFileLine);
		AfxMessageBox (buf, MB_OK);
		return false;
	}
	ar.Close ();
	SetModifiedFlag (TRUE);

	_includedFiles.Add (incl);
	size = _favouriteClasses.GetSize ();
	if (!_undoMode)
	{
		CUndoAddInclude *undo = new CUndoAddInclude (incl, index);
		AddCommandEntry (undo);
		for (int i = favouriteIndex; i < size; ++i)
		{
			CUndoAddFavourite *undo = new CUndoAddFavourite (_favouriteClasses[i], i);
			AddCommandEntry (undo);
		}
	}

	CUpdateDescriptor dscr;
	dscr._addInclude._include = incl;
	dscr._addInclude._from = index;
	UpdateAllViews (NULL, UH_ADD_INCLUDE, &dscr);
	for (int i = favouriteIndex; i < size; ++i)
	{
		CUpdateDescriptor dscr;
		dscr._insertFavourite._class = _favouriteClasses[i];
		UpdateAllViews (NULL, UH_INSERT_FAVOURITE, &dscr);
	}
	return true;
}

void CRscEditorDoc::MoveFavouriteParam (CRscClass *move, CRscClass *where, bool after)
{
	int n = _favouriteClasses.GetSize ();
	int ri = 0;
	for (; ; ++ri)
	{
		if (ri == n)
		{
			return;
		}
		if (_favouriteClasses[ri] == move)
		{
			break;
		}
	}
	_favouriteClasses.RemoveAt (ri);
	int ii = _favouriteClasses.GetSize ();
	if (where != NULL)
	{
		n = _favouriteClasses.GetSize ();
		for (ii = 0; ii < n; ++ii)
		{
			if (_favouriteClasses[ii] == where)
			{
				if (after)
				{
					++ii;
				}
				break;
			}
		}
	}
	_favouriteClasses.InsertAt (ii, move);

	if (ri != ii)
	{
		SetModifiedFlag (TRUE);
		
		if (!_undoMode)
		{
			CUndoMoveFavourite *undo = new CUndoMoveFavourite (move, ri, ii);
			AddCommandEntry (undo);
		}
	}

	CUpdateDescriptor dscr;
	dscr._moveFavourite._class = move;
	dscr._moveFavourite._index = ii;
	UpdateAllViews (NULL, UH_MOVE_FAVOURITE, &dscr);
}

bool CRscEditorDoc::SortFromBackToFront (CArray<CRscClass*, CRscClass*> &arr)
{
	CRscItemPosition tmpEntry;
	CArray<CRscItemPosition, CRscItemPosition&> sortArr;
	int n = arr.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		CRscClass *cl = arr[i];
		tmpEntry._class = cl;
		tmpEntry._pos = -1;
		int m = cl->_valueReferences.GetSize ();
		for (int j = 0; j < m; ++j)
		{
			CRscAttributeValueElement *av = cl->_valueReferences[j];
			if (av != NULL && av->_inArray != NULL)
			{
				int l = av->_inArray->_elements.GetSize ();
				int k = 0;
				for (; k < l; ++k)
				{
					if (k == l)
					{
						return false;
					}
					if (av == av->_inArray->_elements[k])
					{
						break;
					}
				}
				if (k > tmpEntry._pos)
				{
					tmpEntry._pos = k;
				}
			}
		}
		sortArr.Add (tmpEntry);
	}

	::qsort (sortArr.GetData (), sortArr.GetSize (), sizeof (CRscItemPosition), CompareFromBackToFront);
	arr.RemoveAll ();
	n = sortArr.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		arr.Add (sortArr[i]._class);
	}

	return true;
}

int __cdecl CRscEditorDoc::CompareFromBackToFront (const void *elem1, const void *elem2)
{
	CRscItemPosition *p1 = (CRscItemPosition*) elem1;
	CRscItemPosition *p2 = (CRscItemPosition*) elem2;
	return p1->_pos - p2->_pos;
}

void CRscEditorDoc::BringForward (CRscClass *cl)
{
	int n = cl->_valueReferences.GetSize ();
	if (n == 0)
	{
		CString fullCode;
		int textSize = cl->GetFullTextLength (_T (""));
		LPTSTR text = fullCode.GetBuffer (textSize);
		cl->GetFullText (text, _T (""));
		fullCode.ReleaseBuffer (textSize);

		CString dir;
		cl->_parent->GetFullName (dir);
		
		fullCode = dir + _T (",") + cl->_name + _T (";") + fullCode;

		CRscParamBase *op;
		int b = cl->_parent->_innerParams.GetSize ();
		int a = 0;
		for (;;)
		{
			if (a == b)
			{
				return;
			}
			if (cl->_parent->_innerParams[a++] != cl)
			{
				continue;
			}
			break;
		}
		if (a == b)
		{
			return;
		}

		if (!MoveCopyParam (cl, cl->_parent, cl->_parent->_innerParams[a], fullCode, true, true, true, op))
		{
			return;
		}
		UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
	}
	else
	{
		for (int i = 0; i < n; ++i)
		{
			CRscAttributeValueElement *av = cl->_valueReferences[i];
			if (av != NULL && av->_inArray != NULL)
			{
				CRscAttributeValueBase **change;
				if (av->_inArray->_inArray == NULL)
				{
					change = &av->_inArray->_inParam->_value;
				}
				else
				{
					int size = av->_inArray->_inArray->_elements.GetSize ();
					for (int j = 0; ; ++j)
					{
						if (j == size)
						{
							return;
						}
						if (av->_inArray->_inArray->_elements[j] == av->_inArray)
						{
							change = &av->_inArray->_inArray->_elements[j];
						}
					}
				}
				int size = av->_inArray->_elements.GetSize ();
				int j = 0;
				for (; ; ++j)
				{
					if (j == size)
					{
						return;
					}
					CRscAttributeValueBase *vb = av->_inArray->_elements[j];
					if (vb->IsElement () && ((CRscAttributeValueElement*) vb)->_value == cl->_name)
					{
						break;
					}
				}
				if (j < size - 1)
				{
					CRscAttributeValueBase *na = av->_inArray->Copy (av->_inArray->_inArray, av->_inArray->_inParam);
					if (na != NULL && na->IsArray ())
					{
						CRscAttributeValueBase *a = av->_inArray->_elements[j];
						av->_inArray->_elements[j] = av->_inArray->_elements[j + 1];
						av->_inArray->_elements[j + 1] = a;
						SetModifiedFlag (TRUE);
						
						if (!_undoMode)
						{
							CUndoChangeParamValue *undo = new CUndoChangeParamValue (av->_inParam, 
								change, na);
							AddCommandEntry (undo);
						}
						
						CUpdateDescriptor dscr;
						dscr._newValue._param = av->_inParam;
						UpdateAllViews (NULL, UH_NEW_VALUE, &dscr);
					}
				}
			}
		}
	}
}

void CRscEditorDoc::SendBackward (CRscClass *cl)
{
	int n = cl->_valueReferences.GetSize ();
	if (n == 0)
	{
		CString fullCode;
		int textSize = cl->GetFullTextLength (_T (""));
		LPTSTR text = fullCode.GetBuffer (textSize);
		cl->GetFullText (text, _T (""));
		fullCode.ReleaseBuffer (textSize);

		CString dir;
		cl->_parent->GetFullName (dir);
		
		fullCode = dir + _T (",") + cl->_name + _T (";") + fullCode;

		CRscParamBase *op;
		int b = cl->_parent->_innerParams.GetSize ();
		int a = b;
		for (;;)
		{
			if (a == 0)
			{
				return;
			}
			if (cl->_parent->_innerParams[--a] != cl)
			{
				continue;
			}
			break;
		}
		if (a == 0)
		{
			return;
		}
		--a;

		if (!MoveCopyParam (cl, cl->_parent, cl->_parent->_innerParams[a], fullCode, false, true, true, op))
		{
			return;
		}
		UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
	}
	else
	{
		for (int i = 0; i < n; ++i)
		{
			CRscAttributeValueElement *av = cl->_valueReferences[i];
			if (av != NULL && av->_inArray != NULL)
			{
				CRscAttributeValueBase **change;
				if (av->_inArray->_inArray == NULL)
				{
					change = &av->_inArray->_inParam->_value;
				}
				else
				{
					int size = av->_inArray->_inArray->_elements.GetSize ();
					for (int j = 0; ; ++j)
					{
						if (j == size)
						{
							return;
						}
						if (av->_inArray->_inArray->_elements[j] == av->_inArray)
						{
							change = &av->_inArray->_inArray->_elements[j];
						}
					}
				}
				int size = av->_inArray->_elements.GetSize ();
				int j = 0;
				for (; ; ++j)
				{
					if (j == size)
					{
						return;
					}
					CRscAttributeValueBase *vb = av->_inArray->_elements[j];
					if (vb->IsElement () && ((CRscAttributeValueElement*) vb)->_value == cl->_name)
					{
						break;
					}
				}
				if (j > 0)
				{
					CRscAttributeValueBase *na = av->_inArray->Copy (av->_inArray->_inArray, av->_inArray->_inParam);
					if (na != NULL && na->IsArray ())
					{
						CRscAttributeValueBase *a = av->_inArray->_elements[j];
						av->_inArray->_elements[j] = av->_inArray->_elements[j - 1];
						av->_inArray->_elements[j - 1] = a;
						SetModifiedFlag (TRUE);
						
						if (!_undoMode)
						{
							CUndoChangeParamValue *undo = new CUndoChangeParamValue (av->_inParam, 
								change, na);
							AddCommandEntry (undo);
						}
						
						CUpdateDescriptor dscr;
						dscr._newValue._param = av->_inParam;
						UpdateAllViews (NULL, UH_NEW_VALUE, &dscr);
					}
				}
			}
		}
	}
}

void CRscEditorDoc::BringToFront (CRscClass *cl)
{
	int n = cl->_valueReferences.GetSize ();
	if (n == 0)
	{
		CString fullCode;
		int textSize = cl->GetFullTextLength (_T (""));
		LPTSTR text = fullCode.GetBuffer (textSize);
		cl->GetFullText (text, _T (""));
		fullCode.ReleaseBuffer (textSize);

		CString dir;
		cl->_parent->GetFullName (dir);
		
		fullCode = dir + _T (",") + cl->_name + _T (";") + fullCode;

		CRscParamBase *op;
		if (!MoveCopyParam (cl, cl->_parent, cl->_parent->_innerParams[cl->_parent->_innerParams.GetUpperBound ()], 
			fullCode, true, true, true, op))
		{
			return;
		}
		UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
	}
	else
	{
		for (int i = 0; i < n; ++i)
		{
			CRscAttributeValueElement *av = cl->_valueReferences[i];
			if (av != NULL && av->_inArray != NULL)
			{
				CRscAttributeValueBase **change;
				if (av->_inArray->_inArray == NULL)
				{
					change = &av->_inArray->_inParam->_value;
				}
				else
				{
					int size = av->_inArray->_inArray->_elements.GetSize ();
					for (int j = 0; ; ++j)
					{
						if (j == size)
						{
							return;
						}
						if (av->_inArray->_inArray->_elements[j] == av->_inArray)
						{
							change = &av->_inArray->_inArray->_elements[j];
						}
					}
				}
				int size = av->_inArray->_elements.GetSize ();
				int j = 0;
				for (; ; ++j)
				{
					if (j == size)
					{
						return;
					}
					CRscAttributeValueBase *vb = av->_inArray->_elements[j];
					if (vb->IsElement () && ((CRscAttributeValueElement*) vb)->_value == cl->_name)
					{
						break;
					}
				}
				if (j < size - 1)
				{
					CRscAttributeValueBase *na = av->_inArray->Copy (av->_inArray->_inArray, av->_inArray->_inParam);
					if (na != NULL && na->IsArray ())
					{
						CRscAttributeValueBase *a = av->_inArray->_elements[j];
						for (int k = j; k < size - 1; ++k)
						{
							av->_inArray->_elements[k] = av->_inArray->_elements[k + 1];
						}
						av->_inArray->_elements[size - 1] = a;
						SetModifiedFlag (TRUE);
						
						if (!_undoMode)
						{
							CUndoChangeParamValue *undo = new CUndoChangeParamValue (av->_inParam, 
								change, na);
							AddCommandEntry (undo);
						}
						
						CUpdateDescriptor dscr;
						dscr._newValue._param = av->_inParam;
						UpdateAllViews (NULL, UH_NEW_VALUE, &dscr);
					}
				}
			}
		}
	}
}

void CRscEditorDoc::SendToBack (CRscClass *cl)
{
	int n = cl->_valueReferences.GetSize ();
	if (n == 0)
	{
		CString fullCode;
		int textSize = cl->GetFullTextLength (_T (""));
		LPTSTR text = fullCode.GetBuffer (textSize);
		cl->GetFullText (text, _T (""));
		fullCode.ReleaseBuffer (textSize);

		CString dir;
		cl->_parent->GetFullName (dir);
		
		fullCode = dir + _T (",") + cl->_name + _T (";") + fullCode;

		CRscParamBase *op;
		if (!MoveCopyParam (cl, cl->_parent, cl->_parent->_innerParams[0], fullCode, false, true, true, op))
		{
			return;
		}
		UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
	}
	else
	{
		// controls[] = {...}
		for (int i = 0; i < n; ++i)
		{
			CRscAttributeValueElement *av = cl->_valueReferences[i];
			if (av != NULL && av->_inArray != NULL)
			{
				CRscAttributeValueBase **change;
				if (av->_inArray->_inArray == NULL)
				{
					change = &av->_inArray->_inParam->_value;
				}
				else
				{
					int size = av->_inArray->_inArray->_elements.GetSize ();
					for (int j = 0; ; ++j)
					{
						if (j == size)
						{
							return;
						}
						if (av->_inArray->_inArray->_elements[j] == av->_inArray)
						{
							change = &av->_inArray->_inArray->_elements[j];
						}
					}
				}
				int size = av->_inArray->_elements.GetSize ();
				int j = 0;
				for (; ; ++j)
				{
					if (j == size)
					{
						return;
					}
					CRscAttributeValueBase *vb = av->_inArray->_elements[j];
					if (vb->IsElement () && ((CRscAttributeValueElement*) vb)->_value == cl->_name)
					{
						break;
					}
				}
				if (j > 0)
				{
					CRscAttributeValueBase *na = av->_inArray->Copy (av->_inArray->_inArray, av->_inArray->_inParam);
					if (na != NULL && na->IsArray ())
					{
						CRscAttributeValueBase *a = av->_inArray->_elements[j];
						for (int k = j; k > 0; --k)
						{
							av->_inArray->_elements[k] = av->_inArray->_elements[k - 1];
						}
						av->_inArray->_elements[0] = a;
						SetModifiedFlag (TRUE);
						
						if (!_undoMode)
						{
							CUndoChangeParamValue *undo = new CUndoChangeParamValue (av->_inParam, 
								change, na);
							AddCommandEntry (undo);
						}
						
						CUpdateDescriptor dscr;
						dscr._newValue._param = av->_inParam;
						UpdateAllViews (NULL, UH_NEW_VALUE, &dscr);
					}
				}
			}
		}
	}
}

bool CRscEditorDoc::GetGroupsOfClass (CRscClass *owner, CArray<CRscGroup*, CRscGroup*> &grs)
{
	int a = grs.GetSize ();
	int n = _groups.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (_groups[i]->_owner == owner)
		{
			grs.Add (_groups[i]);
		}
	}
	return grs.GetSize () != a;
}

int CRscEditorDoc::GetGroups (CArray<CRscClass*, CRscClass*> &cls, 
							  CArray<CRscGroup*, CRscGroup*> *grArr,
							  CArray<CRscClass*, CRscClass*> *clArr)
{
	int m = cls.GetSize ();
	if (m == 0)
	{
		return SELECTED_NONE_ITEM;
	}
	if (m == 1)
	{
		if (clArr != NULL)
		{
			clArr->Add (cls[0]);
		}
		return SELECTED_ONE_ITEM;
	}

	CRscGroup *gr = cls[0]->_group;
	bool someOut = false;
	for (int j = 0; j < m; ++j)
	{
		if (cls[j]->_group != NULL)
		{
			if (grArr != NULL)
			{
				int n = grArr->GetSize ();
				for (int i = 0; ; ++i)
				{
					if (i == n)
					{
						grArr->Add (cls[j]->_group);
						break;
					}
					if ((*grArr)[i] == cls[j]->_group)
					{
						break;
					}
				}
			}
		}
		else
		{
			if (clArr != NULL)
			{
				clArr->Add (cls[j]);
			}
		}
		if (cls[j]->_group != gr)
		{
			someOut = true;
		}
	}
	if (!someOut)
	{
		if (gr == NULL)
		{
			return SELECTED_NONE_GROUP;
		}
		if (gr->_members.GetSize () == m)
		{
			return SELECTED_ONE_GROUP;
		}
	}
	return SELECTED_PART_GROUPS;
}

int CRscEditorDoc::GetSelectedGroups (CArray<CRscGroup*, CRscGroup*> *arr)
{
	if (_monitorClass == NULL)
	{
		return SELECTED_NONE_ITEM;
	}
	return GetGroups (_selectedClasses, arr, NULL);
}

void CRscEditorDoc::Group ()
{
	int index = _groups.GetSize ();
	CRscGroup *gr = new CRscGroup (_monitorClass);
	if (gr == NULL)
	{
		return;
	}
	gr->_members.Copy (_selectedClasses);
	gr->Attach ();
	_groups.Add (gr);
	SetModifiedFlag (TRUE);

	if (!_undoMode)
	{
		CUndoNewGroup *undo = new CUndoNewGroup (gr, index);
		AddCommandEntry (undo);
	}
}

void CRscEditorDoc::Ungroup (CRscClass *owner)
{
	int n = _groups.GetSize ();
	for (int i = 0; i < n; )
	{
		CRscGroup *gr = _groups[i];
		if (gr->_owner == owner)
		{
			_groups.RemoveAt (i);
			--n;
			gr->Detach ();
			SetModifiedFlag (TRUE);
			
			if (!_undoMode)
			{
				CUndoDelGroup *undo = new CUndoDelGroup (gr, i);
				AddCommandEntry (undo);
			}
		}
		else
		{
			++i;
		}
	}
}

void CRscEditorDoc::Ungroup (CRscGroup *gr)
{
	int n = _groups.GetSize ();
	int i = 0;
	for (; ; ++i)
	{
		if (i == n)
		{
			return;
		}
		if (_groups[i] == gr)
		{
			break;
		}
	}
	_groups.RemoveAt (i);
	gr->Detach ();
	SetModifiedFlag (TRUE);

	if (!_undoMode)
	{
		CUndoDelGroup *undo = new CUndoDelGroup (gr, i);
		AddCommandEntry (undo);
	}
}

void CRscEditorDoc::Regroup (CArray<CRscGroup*, CRscGroup*> &grs)
{
	int n = grs.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		Ungroup (grs[i]);
	}
	Group ();
}

void CRscEditorDoc::SelectAllClasses (bool flag)
{
	if (flag)
	{
		int n = _displayedClasses.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_displayedClasses[i]->_selected = true;
		}
		_selectedClasses.Copy (_displayedClasses);
	}
	else
	{
		int n = _selectedClasses.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_selectedClasses[i]->_selected = false;
		}
		_selectedClasses.RemoveAll ();
	}
}

int __cdecl CRscEditorDoc::SortLeft2Right (const void *elem1, const void *elem2)
{
	CRscLayoutUnit *lu1 = *(CRscLayoutUnit**) elem1;
	CRscLayoutUnit *lu2 = *(CRscLayoutUnit**) elem2;

	double x1, x2;
	if (lu1->GetX (x1))
	{
		if (lu2->GetX (x2))
		{
			return x1 < x2 ? -1 : (x1 > x2 ? 1 : 0);
		}
		else
		{
			return -1;
		}
	}
	else if (lu2->GetX (x2))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int __cdecl CRscEditorDoc::SortTop2Down (const void *elem1, const void *elem2)
{
	CRscLayoutUnit *lu1 = *(CRscLayoutUnit**) elem1;
	CRscLayoutUnit *lu2 = *(CRscLayoutUnit**) elem2;

	double y1, y2;
	if (lu1->GetY (y1))
	{
		if (lu2->GetY (y2))
		{
			return y1 < y2 ? -1 : (y1 > y2 ? 1 : 0);
		}
		else
		{
			return -1;
		}
	}
	else if (lu2->GetY (y2))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int __cdecl CRscEditorDoc::SortRight2Left (const void *elem1, const void *elem2)
{
	return -SortLeft2Right (elem1, elem2);
}

int __cdecl CRscEditorDoc::SortDown2Top (const void *elem1, const void *elem2)
{
	return -SortTop2Down (elem1, elem2);
}

void CRscEditorDoc::AlignRightCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
	if (x + w > n[0])
	{
		n[0] = x + w;
	}
}

void CRscEditorDoc::AlignRightSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	x = n[0] - w;
}

void CRscEditorDoc::AlignBottomCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
	if (y + h > n[0])
	{
		n[0] = y + h;
	}
}

void CRscEditorDoc::AlignBottomSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	y = n[0] - h;
}

void CRscEditorDoc::AlignLeftCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
	if (x < n[0])
	{
		n[0] = x;
	}
}

void CRscEditorDoc::AlignLeftSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	x = n[0];
}

void CRscEditorDoc::AlignTopCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
	if (y < n[0])
	{
		n[0] = y;
	}
}

void CRscEditorDoc::AlignTopSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	y = n[0];
}

void CRscEditorDoc::AlignHorizCenterCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
	if (n[0] < 0.0)
	{
		n[0] = x + x + w;
	}
	else
	{
		if (n[0] < w)
		{
			n[0] = w;
		}
		else if (n[0] + w > 2.0)
		{
			n[0] = 2.0 - w;
		}
	}
}

void CRscEditorDoc::AlignHorizCenterSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	x = (n[0] - w) / 2.0;
}

void CRscEditorDoc::AlignVertCenterCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
	if (n[0] < 0.0)
	{
		n[0] = y + y + h;
	}
	else
	{
		if (n[0] < h)
		{
			n[0] = h;
		}
		else if (n[0] + h > 2.0)
		{
			n[0] = 2.0 - h;
		}
	}
}

void CRscEditorDoc::AlignVertCenterSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	y = (n[0] - h) / 2.0;
}

void CRscEditorDoc::SideBySideTopCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
}

void CRscEditorDoc::SideBySideTopSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	if (n[0] != 0.0)
	{
		n[1] = y + h;
		n[0] = 0.0;
	}
	else
	{
		y = n[1];
		n[1] += h;
	}
}

void CRscEditorDoc::SideBySideLeftCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
}

void CRscEditorDoc::SideBySideLeftSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	if (n[0] != 0.0)
	{
		n[1] = x + w;
		n[0] = 0.0;
	}
	else
	{
		x = n[1];
		n[1] += w;
	}
}

void CRscEditorDoc::SideBySideBottomCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
}

void CRscEditorDoc::SideBySideBottomSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	if (n[0] != 0.0)
	{
		n[1] = y;
		n[0] = 0.0;
	}
	else
	{
		n[1] -= h;
		y = n[1];
	}
}

void CRscEditorDoc::SideBySideRightCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
}

void CRscEditorDoc::SideBySideRightSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	if (n[0] != 0.0)
	{
		n[1] = x;
		n[0] = 0.0;
	}
	else
	{
		n[1] -= w;
		x = n[1];
	}
}

void CRscEditorDoc::MakeSameSizeWidthCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
	if (n[0] < w)
	{
		n[0] = w;
	}
}

void CRscEditorDoc::MakeSameSizeWidthSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	w = n[0];
	if (x + w > 1.0)
	{
		x = 1.0 - w;
	}
}

void CRscEditorDoc::MakeSameSizeHeightCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
	if (n[0] < h)
	{
		n[0] = h;
	}
}

void CRscEditorDoc::MakeSameSizeHeightSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	h = n[0];
	if (y + h > 1.0)
	{
		y = 1.0 - h;
	}
}

void CRscEditorDoc::MakeSameSizeBothCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
	if (n[0] < w)
	{
		n[0] = w;
	}
	if (n[1] < h)
	{
		n[1] = h;
	}
}

void CRscEditorDoc::MakeSameSizeBothSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	w = n[0];
	if (x + w > 1.0)
	{
		x = 1.0 - w;
	}
	h = n[1];
	if (y + h > 1.0)
	{
		y = 1.0 - h;
	}
}

void CRscEditorDoc::CenterInDialogHorizCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
	if (x < n[0])
	{
		n[0] = x;
	}
	if (x + w > n[1])
	{
		n[1] = x + w;
	}
}

void CRscEditorDoc::CenterInDialogHorizSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	x += n[2] - (n[0] + n[1]) / 2.0;
}

void CRscEditorDoc::CenterInDialogVerticalCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
	if (y < n[0])
	{
		n[0] = y;
	}
	if (y + h > n[1])
	{
		n[1] = y + h;
	}
}

void CRscEditorDoc::CenterInDialogVerticalSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	y += n[2] - (n[0] + n[1]) / 2.0;
}

void CRscEditorDoc::SpaceEvenlyAcrossDialogCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
	n[2] += 1.0;
	n[1] -= w;
}

void CRscEditorDoc::SpaceEvenlyAcrossDialogSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	n[0] += n[1] / n[2];
	x = n[0];
	n[0] += w;
}

void CRscEditorDoc::SpaceEvenlyAcrossSelectionCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
	if (n[0] > x)
	{
		n[4] = n[0] = x;
	}
	if (n[1] < x + w)
	{
		n[1] = x + w;
	}
	n[2] += 1.0;
	n[3] += w;
}

void CRscEditorDoc::SpaceEvenlyAcrossSelectionSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	x = n[4];
	n[4] += w;
	n[4] += (n[1] - n[0] - n[3]) / (n[2] - 1.0);
}

void CRscEditorDoc::SpaceEvenlyDownDialogCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
	n[2] += 1.0;
	n[1] -= h;
}

void CRscEditorDoc::SpaceEvenlyDownDialogSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	n[0] += n[1] / n[2];
	y = n[0];
	n[0] += h;
}

void CRscEditorDoc::SpaceEvenlyDownSelectionCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h)
{
	if (n[0] > y)
	{
		n[4] = n[0] = y;
	}
	if (n[1] < y + h)
	{
		n[1] = y + h;
	}
	n[2] += 1.0;
	n[3] += h;
}

void CRscEditorDoc::SpaceEvenlyDownSelectionSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h)
{
	y = n[4];
	n[4] += h;
	n[4] += (n[1] - n[0] - n[3]) / (n[2] - 1.0);
}

int CRscEditorDoc::CreateLayoutUnits (CArray<CRscLayoutUnit*, CRscLayoutUnit*> *lus,
									   CArray<CRscClass*, CRscClass*> &sel)
{
	CArray<CRscGroup*, CRscGroup*> grs;
	CArray<CRscClass*, CRscClass*> cls;
	(void) GetGroups (sel, &grs, &cls);
	if (lus != NULL)
	{
		int size = grs.GetSize ();
		for (int i = 0; i < size; ++i)
		{
			CRscLayoutGroup *lu = new CRscLayoutGroup (grs[i], this);
			if (lu != NULL)
			{
				lus->Add (lu);
			}
		}
		size = cls.GetSize ();
		for (int i = 0; i < size; ++i)
		{
			CRscLayoutClass *lu = new CRscLayoutClass (cls[i], this);
			if (lu != NULL)
			{
				lus->Add (lu);
			}
		}
	}
	return grs.GetSize () + cls.GetSize ();
}

void CRscEditorDoc::DisposeLayoutUnits (CArray<CRscLayoutUnit*, CRscLayoutUnit*> &lus)
{
	int n = lus.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		delete lus[i];
	}
	lus.RemoveAll ();
}

void CRscEditorDoc::OnLayoutGeneric (double (&n)[LAYOUT_PARAMS_SIZE], bool changeBases,
	void CompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h), 
	void SetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h),
	int (__cdecl *sort) (const void *elem1, const void *elem2))
{
	CArray<CRscLayoutUnit*, CRscLayoutUnit*> arr;
	CreateLayoutUnits (&arr, _selectedClasses);

	int size = arr.GetSize ();
	if (sort != NULL)
	{
		qsort (arr.GetData (), size, sizeof (CRscLayoutUnit*), sort);
	}

	for (int i = 0; i < size; ++i)
	{
		double x, y, w, h;

		CRscLayoutUnit *lu = arr[i];
		if (lu->GetX (x) &&
			lu->GetY (y) &&
			lu->GetW (w) &&
			lu->GetH (h))
		{
			CompFce (n, x, y, w, h);
		}
	}
	for (int i = 0; i < size; ++i)
	{
		double x, y, w, h;

		CRscLayoutUnit *lu = arr[i];
		if (lu->GetX (x) &&
			lu->GetY (y) &&
			lu->GetW (w) &&
			lu->GetH (h))
		{
			double x2 = x;
			double y2 = y;
			double w2 = w;
			double h2 = h;
			SetFce (n, x2, y2, w2, h2);
			if (x != x2)
			{
				// SetFloatParamInBases (cl, "x", x2, changeBases, false, true, false);
				if (lu->SetX (x2, changeBases, true))
				{
					SetModifiedFlag (TRUE);
				}
			}
			if (y != y2)
			{
				//SetFloatParamInBases (cl, "y", y2, changeBases, false, true, false);
				if (lu->SetY (y2, changeBases, true))
				{
					SetModifiedFlag (TRUE);
				}
			}
			if (w != w2)
			{
				//SetFloatParamInBases (cl, "w", w2, changeBases, false, true, false);
				if (lu->SetW (w2, changeBases, true))
				{
					SetModifiedFlag (TRUE);
				}
			}
			if (h != h2)
			{
				//SetFloatParamInBases (cl, "h", h2, changeBases, false, true, false);
				if (lu->SetH (h2, changeBases, true))
				{
					SetModifiedFlag (TRUE);
				}
			}
			lu->TransformDone ();
		}
	}
	DisposeLayoutUnits (arr);
}

CRscGuide *CRscEditorDoc::GetGuide (CRscClass *cl, bool create)
{
	int n = _guides.GetSize ();
	for (int i = 0; ; ++i)
	{
		if (i == n)
		{
			if (create)
			{
				CRscGuide *guide = new CRscGuide (cl);
				if (guide == NULL)
				{
					return NULL;
				}
				_guides.Add (guide);
				SetModifiedFlag (TRUE);
				
				if (!_undoMode)
				{
					CUndoCreateGuide *undo = new CUndoCreateGuide (guide);
					AddCommandEntry (undo);
				}
				return guide;
			}
			else
			{
				return NULL;
			}
		}
		if (_guides[i]->_owner == cl)
		{
			return _guides[i];
		}
	}
}

CRscGuide* CRscEditorDoc::AddGuide (CRscClass *cl, int type, double f, int *index)
{
	CRscGuide *guide = GetGuide (cl, true);
	if (guide == NULL)
	{
		return NULL;
	}

	switch (type)
	{
	case GUIDE_VERT:
		if (index != NULL)
		{
			*index = guide->_vert.GetSize ();
		}
		guide->_vert.Add (f);
		guide->_vertPxl.Add (-1);
		break;

	case GUIDE_HORZ:
		if (index != NULL)
		{
			*index = guide->_horz.GetSize ();
		}
		guide->_horz.Add (f);
		guide->_horzPxl.Add (-1);
		break;
	}
	SetModifiedFlag (TRUE);

	if (!_undoMode)
	{
		CUndoAddGuide *undo = new CUndoAddGuide (guide, type, f);
		AddCommandEntry (undo);
	}
	return guide;
}

void CRscEditorDoc::MoveGuide (CRscGuide *guide, int type, int index, double value, bool canUndo)
{
	switch (type)
	{
	case GUIDE_VERT:
		if (canUndo && !_undoMode)
		{
			CUndoMoveGuide *undo = new CUndoMoveGuide (guide, type, index, guide->_vert[index], value);
			AddCommandEntry (undo);
		}
		guide->_vert[index] = value;
		SetModifiedFlag (TRUE);
		break;

	case GUIDE_HORZ:
		if (canUndo && !_undoMode)
		{
			CUndoMoveGuide *undo = new CUndoMoveGuide (guide, type, index, guide->_horz[index], value);
			AddCommandEntry (undo);
		}
		guide->_horz[index] = value;
		SetModifiedFlag (TRUE);
		break;
	}
}

void CRscEditorDoc::RemoveGuide (CRscGuide *guide, int type, int index)
{
	double v;
	switch (type)
	{
	case GUIDE_VERT:
		v = guide->_vert[index];
		guide->_vert.RemoveAt (index);
		guide->_vertPxl.RemoveAt (index);
		break;

	// case GUIDE_HORZ:
	default:
		v = guide->_horz[index];
		guide->_horz.RemoveAt (index);
		guide->_horzPxl.RemoveAt (index);
		break;
	}
	SetModifiedFlag (TRUE);

	if (!_undoMode)
	{
		CUndoRemoveGuide *undo = new CUndoRemoveGuide (guide, type, index, v);
		AddCommandEntry (undo);
	}

	if (guide->_vert.GetSize () == 0 && guide->_horz.GetSize () == 0)
	{
		DeleteGuide (guide);
	}
}

void CRscEditorDoc::DeleteGuide (CRscClass *owner)
{
	int n = _guides.GetSize ();
	for (int i = 0; i < n;)
	{
		CRscGuide *guide = _guides[i];
		if (guide->_owner == owner)
		{
			_guides.RemoveAt (i);
			--n;
			SetModifiedFlag (TRUE);
			if (!_undoMode)
			{
				CUndoDeleteGuide *undo = new CUndoDeleteGuide (guide, i);
				AddCommandEntry (undo);
			}
		}
		else
		{
			++i;
		}
	}
}

void CRscEditorDoc::DeleteGuide (CRscGuide *guide)
{
	int n = _guides.GetSize ();
	for (int i = 0; i < n;)
	{
		if (_guides[i] == guide)
		{
			_guides.RemoveAt (i);
			SetModifiedFlag (TRUE);
			if (!_undoMode)
			{
				CUndoDeleteGuide *undo = new CUndoDeleteGuide (guide, i);
				AddCommandEntry (undo);
			}
			--n;
		}
		else
		{
			++i;
		}
	}
}

bool CRscEditorDoc::FindGuidedVertValue (double y, double &d, double *gv, int *gvp, RECT *view)
{
	bool r = false;
	bool snap = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GRID_SWITCH, snap);
	if (snap)
	{
		double grid = 0.1;
		((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GRID_VERT_UNIT, grid);
		double t = ::fmod (y, grid);
		if (t < d)
		{
			r = true;
			d = t;
			double tt = ::floor (y / grid) * grid;
			if (gv != NULL)
			{
				*gv = tt;
			}
			if (gvp != NULL && view != NULL)
			{
				CALC_GUIDE_POINT (*gvp, view->top, view->bottom - view->top - 1, tt);
			}
		}
		t = grid - t;
		if (t < d)
		{
			r = true;
			d = t;
			double tt = (::floor (y / grid) + 1.0) * grid;
			if (gv != NULL)
			{
				*gv = tt;
			}
			if (gvp != NULL)
			{
				CALC_GUIDE_POINT (*gvp, view->top, view->bottom - view->top - 1, tt);
			}
		}
	}

	snap = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDES_SWITCH, snap);
	if (snap)
	{
		CRscGuide *guide = GetGuide (_monitorClass, false);
		if (guide == NULL)
		{
			return r;
		}
		
		int n = guide->_vert.GetSize ();
		if (n == 0)
		{
			return r;
		}
		
		for (int i = 0; i < n; ++i)
		{
			double t = ::fabs (y - guide->_vert[i]);
			if (t < d)
			{
				r = true;
				d = t;
				if (gv != NULL)
				{
					*gv = guide->_vert[i];
				}
				if (gvp != NULL)
				{
					*gvp = guide->_vertPxl[i];
				}
			}
		}
	}
	return r;
}

bool CRscEditorDoc::FindGuidedHorzValue (double x, double &d, double *gv, int *gvp, RECT *view)
{
	bool r = false;
	bool snap = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GRID_SWITCH, snap);
	if (snap)
	{
		double grid = 0.1;
		((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GRID_HORZ_UNIT, grid);
		double t = ::fmod (x, grid);
		if (t < d)
		{
			r = true;
			d = t;
			double tt = ::floor (x / grid) * grid;
			if (gv != NULL)
			{
				*gv = tt;
			}
			if (gvp != NULL && view != NULL)
			{
				CALC_GUIDE_POINT (*gvp, view->left, view->right - view->left - 1, tt);
			}
		}
		t = grid - t;
		if (t < d)
		{
			r = true;
			d = t;
			double tt = (::floor (x / grid) + 1.0) * grid;
			if (gv != NULL)
			{
				*gv = tt;
			}
			if (gvp != NULL)
			{
				CALC_GUIDE_POINT (*gvp, view->left, view->right - view->left - 1, tt);
			}
		}
	}

	snap = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDES_SWITCH, snap);
	if (snap)
	{
		CRscGuide *guide = GetGuide (_monitorClass, false);
		if (guide == NULL)
		{
			return r;
		}
		
		int n = guide->_horz.GetSize ();
		if (n == 0)
		{
			return r;
		}
		
		for (int i = 0; i < n; ++i)
		{
			double t = ::fabs (x - guide->_horz[i]);
			if (t < d)
			{
				r = true;
				d = t;
				if (gv != NULL)
				{
					*gv = guide->_horz[i];
				}
				if (gvp != NULL)
				{
					*gvp = guide->_horzPxl[i];
				}
			}
		}
	}
	return r;
}

bool CRscEditorDoc::FindItemSnapVertValue (double y, double &d, double *gv, int *gvp, RECT *view)
{
	bool r = false;
	bool snap = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::ITEM_SNAP_SWITCH, snap);
	if (snap)
	{
		int n = _displayedClasses.GetSize ();
		if (n == 0)
		{
			return r;
		}
		
		for (int i = 0; i < n; ++i)
		{
			CRscClass *cl = _displayedClasses[i];
			double cly, clh;
			if (!cl->_selected && 
				cl->GetFloatParamInBases ("y", cly, true, NULL, false) &&
				cl->GetFloatParamInBases ("h", clh, true, NULL, false))
			{
				double t = ::fabs (y - cly);
				if (t < d)
				{
					r = true;
					d = t;
					if (gv != NULL)
					{
						*gv = cly;
					}
					if (gvp != NULL)
					{
						CALC_GUIDE_POINT (*gvp, view->top, view->bottom - view->top - 1, cly);
					}
				}
				t = ::fabs (y - cly - clh);
				if (t < d)
				{
					r = true;
					d = t;
					if (gv != NULL)
					{
						*gv = cly + clh;
					}
					if (gvp != NULL)
					{
						CALC_GUIDE_POINT (*gvp, view->top, view->bottom - view->top - 1, cly + clh);
					}
				}
			}
		}
	}
	return r;
}

bool CRscEditorDoc::FindItemSnapHorzValue (double x, double &d, double *gv, int *gvp, RECT *view)
{
	bool r = false;
	bool snap = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::ITEM_SNAP_SWITCH, snap);
	if (snap)
	{
		int n = _displayedClasses.GetSize ();
		if (n == 0)
		{
			return r;
		}
		
		for (int i = 0; i < n; ++i)
		{
			CRscClass *cl = _displayedClasses[i];
			double clx, clw;
			if (!cl->_selected && 
				cl->GetFloatParamInBases ("x", clx, true, NULL, false) &&
				cl->GetFloatParamInBases ("w", clw, true, NULL, false))
			{
				double t = ::fabs (x - clx);
				if (t < d)
				{
					r = true;
					d = t;
					if (gv != NULL)
					{
						*gv = clx;
					}
					if (gvp != NULL)
					{
						CALC_GUIDE_POINT (*gvp, view->left, view->right - view->left - 1, clx);
					}
				}
				t = ::fabs (x - clx - clw);
				if (t < d)
				{
					r = true;
					d = t;
					if (gv != NULL)
					{
						*gv = clx + clw;
					}
					if (gvp != NULL)
					{
						CALC_GUIDE_POINT (*gvp, view->left, view->right - view->left - 1, clx + clw);
					}
				}
			}
		}
	}
	return r;
}

void CRscEditorDoc::ApplyGuidesToPosition (CArray<CRscLayoutUnit*, CRscLayoutUnit*> &arr, 
									bool inBases, RECT *view)
{
	int m = arr.GetSize ();
	if (m == 0)
	{
		return;
	}

	int gsnap = 3;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDE_SNAP, gsnap);

	CRscLayoutUnit *nearest = NULL;
	double nearestX = -1, nearestW = -1;
	double nearestY = -1, nearestH = -1;
	bool topLeft = true;
	double d = 1.0;
	double displX, displW;
	double displY, displH;
	for (int j = 0; j < m; ++j)
	{
		double guidedValue = -1.0;
		int guidedValuePxl = -1;
		CRscLayoutUnit *lu = arr[j];
		if (lu->GetX (displX) &&
			lu->GetY (displY) &&
			lu->GetW (displW) &&
			lu->GetH (displH))
		{
			if (FindGuidedVertValue (displY, d, &guidedValue, &guidedValuePxl, view))
			{
				nearest = lu;
				nearestX = displX;
				nearestW = displW;
				nearestY = displY;
				nearestH = displH;
				topLeft = true;
			}
			if (FindGuidedVertValue (displY + displH, d, &guidedValue, &guidedValuePxl, view))
			{
				nearest = lu;
				nearestX = displX;
				nearestW = displW;
				nearestY = displY;
				nearestH = displH;
				topLeft = false;
			}
		}
		if (nearest != NULL)
		{
			RECT ctrl;
			double vdx = (double) (view->right - view->left - 1);
			double vdy = (double) (view->bottom - view->top - 1);
			CALC_CTRL_RECT (ctrl, view->left, view->top, vdx, vdy, 
				nearestX, nearestY, nearestW, nearestH);
			bool closeEnough = false;
			double guidedDeltaY = -1.0;
			if (topLeft)
			{
				closeEnough = ::abs (ctrl.top - guidedValuePxl) <= gsnap;
				if (closeEnough)
				{
					guidedDeltaY = guidedValue - nearestY;
					nearest->SetY (guidedValue, inBases, false);
					nearest->TransformDone ();
				}
			}
			else
			{
				closeEnough = ::abs (ctrl.bottom - 1 - guidedValuePxl) <= gsnap;
				if (closeEnough)
				{
					guidedDeltaY = guidedValue - nearestY - nearestH;
					nearest->SetY (guidedValue - nearestH, inBases, false);
					nearest->TransformDone ();
				}
			}
			if (closeEnough)
			{
				for (j = 0; j < m; ++j)
				{
					CRscLayoutUnit *lu = arr[j];
					if (lu != nearest)
					{
						double y;
						if (lu->GetY (y))
						{
							lu->SetY (y + guidedDeltaY, inBases, false);
							lu->TransformDone ();
						}
					}
				}
			}
		}
	}

	nearest = NULL;
	topLeft = true;
	d = 1.0;
	for (int j = 0; j < m; ++j)
	{
		double guidedValue = -1.0;
		int guidedValuePxl = -1;
		CRscLayoutUnit *lu = arr[j];
		if (lu->GetX (displX) &&
			lu->GetY (displY) &&
			lu->GetW (displW) &&
			lu->GetH (displH))
		{
			if (FindGuidedHorzValue (displX, d, &guidedValue, &guidedValuePxl, view))
			{
				nearest = lu;
				nearestX = displX;
				nearestW = displW;
				nearestY = displY;
				nearestH = displH;
				topLeft = true;
			}
			if (FindGuidedHorzValue (displX + displW, d, &guidedValue, &guidedValuePxl, view))
			{
				nearest = lu;
				nearestX = displX;
				nearestW = displW;
				nearestY = displY;
				nearestH = displH;
				topLeft = false;
			}
		}
		if (nearest != NULL)
		{
			RECT ctrl;
			double vdx = (double) (view->right - view->left - 1);
			double vdy = (double) (view->bottom - view->top - 1);
			CALC_CTRL_RECT (ctrl, view->left, view->top, vdx, vdy, 
				nearestX, nearestY, nearestW, nearestH);
			bool closeEnough = false;
			double guidedDeltaX = -1.0;
			if (topLeft)
			{
				closeEnough = ::abs (ctrl.left - guidedValuePxl) <= gsnap;
				if (closeEnough)
				{
					guidedDeltaX = guidedValue - nearestX;
					nearest->SetX (guidedValue, inBases, false);
					nearest->TransformDone ();
				}
			}
			else
			{
				closeEnough = ::abs (ctrl.right - 1 - guidedValuePxl) <= gsnap;
				if (closeEnough)
				{
					guidedDeltaX = guidedValue - nearestX - nearestW;
					nearest->SetX (guidedValue - nearestW, inBases, false);
					nearest->TransformDone ();
				}
			}
			if (closeEnough)
			{
				for (j = 0; j < m; ++j)
				{
					CRscLayoutUnit *lu = arr[j];
					if (lu != nearest)
					{
						double x;
						if (lu->GetX (x))
						{
							lu->SetX (x + guidedDeltaX, inBases, false);
							lu->TransformDone ();
						}
					}
				}
			}
		}
	}
}

void CRscEditorDoc::ApplyGuidesToLeft (CArray<CRscLayoutUnit*, CRscLayoutUnit*> &arr, 
									bool inBases, RECT *view)
{
	int m = arr.GetSize ();
	if (m == 0)
	{
		return;
	}

	int gsnap = 3;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDE_SNAP, gsnap);

	CRscLayoutUnit *nearest = NULL;
	double nearestX = -1.0, nearestW = -1.0;
	double nearestY = -1.0, nearestH = -1.0;
	double d = 1.0;
	double displX, displW;
	double displY, displH;
	double guidedValue = -1.0;
	int guidedValuePxl = -1;
	for (int j = 0; j < m; ++j)
	{
		CRscLayoutUnit *lu = arr[j];
		if (lu->GetX (displX) &&
			lu->GetY (displY) &&
			lu->GetW (displW) &&
			lu->GetH (displH))
		{
			if (FindGuidedHorzValue (displX, d, &guidedValue, &guidedValuePxl, view))
			{
				nearest = lu;
				nearestX = displX;
				nearestW = displW;
				nearestY = displY;
				nearestH = displH;
			}
		}
	}
	if (nearest != NULL)
	{
		RECT ctrl;
		double vdx = (double) (view->right - view->left - 1);
		double vdy = (double) (view->bottom - view->top - 1);
		CALC_CTRL_RECT (ctrl, view->left, view->top, vdx, vdy,
			nearestX, nearestY, nearestW, nearestH);
		bool closeEnough = ::abs (ctrl.left - guidedValuePxl) <= gsnap;
		if (closeEnough)
		{
			double guidedDeltaX = guidedValue - nearestX;
			nearest->SetX (guidedValue, inBases, false);
			nearest->SetW (nearestW - guidedDeltaX, inBases, false);
			nearest->TransformDone ();
			
			for (int j = 0; j < m; ++j)
			{
				CRscLayoutUnit *lu = arr[j];
				if (lu != nearest)
				{
					double x, w;
					if (lu->GetX (x) &&
						lu->GetW (w))
					{
						lu->SetX (x + guidedDeltaX, inBases, false);
						lu->SetW (w - guidedDeltaX, inBases, false);
						lu->TransformDone ();
					}
				}
			}
		}
	}
}

void CRscEditorDoc::ApplyGuidesToRight (CArray<CRscLayoutUnit*, CRscLayoutUnit*> &arr, 
									bool inBases, RECT *view)
{
	int m = arr.GetSize ();
	if (m == 0)
	{
		return;
	}

	int gsnap = 3;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDE_SNAP, gsnap);
	
	CRscLayoutUnit *nearest = NULL;
	double nearestX = -1.0, nearestW = -1.0;
	double nearestY = -1.0, nearestH = -1.0;
	double d = 1.0;
	double displX, displW;
	double displY, displH;
	double guidedValue = -1.0;
	int guidedValuePxl = -1;
	for (int j = 0; j < m; ++j)
	{
		CRscLayoutUnit *lu = arr[j];
		if (lu->GetX (displX) &&
			lu->GetY (displY) &&
			lu->GetW (displW) &&
			lu->GetH (displH))
		{
			if (FindGuidedHorzValue (displX + displW, d, &guidedValue, &guidedValuePxl, view))
			{
				nearest = lu;
				nearestX = displX;
				nearestW = displW;
				nearestY = displY;
				nearestH = displH;
			}
		}
	}
	if (nearest != NULL)
	{
		RECT ctrl;
		double vdx = (double) (view->right - view->left - 1);
		double vdy = (double) (view->bottom - view->top - 1);
		CALC_CTRL_RECT (ctrl, view->left, view->top, vdx, vdy,
			nearestX, nearestY, nearestW, nearestH);
		bool closeEnough = ::abs (ctrl.right - 1 - guidedValuePxl) <= gsnap;
		if (closeEnough)
		{
			double guidedDeltaW = guidedValue - nearestX - nearestW;
			nearest->SetW (guidedValue - nearestX, inBases, false);
			nearest->TransformDone ();
			
			for (int j = 0; j < m; ++j)
			{
				CRscLayoutUnit *lu = arr[j];
				if (lu != nearest)
				{
					double w;
					if (lu->GetW (w))
					{
						lu->SetW (w + guidedDeltaW, inBases, false);
						lu->TransformDone ();
					}
				}
			}
		}
	}
}

void CRscEditorDoc::ApplyGuidesToTop (CArray<CRscLayoutUnit*, CRscLayoutUnit*> &arr, 
									bool inBases, RECT *view)
{
	int m = arr.GetSize ();
	if (m == 0)
	{
		return;
	}

	int gsnap = 3;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDE_SNAP, gsnap);
	
	CRscLayoutUnit *nearest = NULL;
	double nearestX = -1.0, nearestW = -1.0;
	double nearestY = -1.0, nearestH = -1.0;
	double d = 1.0;
	double displX, displW;
	double displY, displH;
	double guidedValue = -1.0;
	int guidedValuePxl = -1;
	for (int j = 0; j < m; ++j)
	{
		CRscLayoutUnit *lu = arr[j];
		if (lu->GetX (displX) &&
			lu->GetY (displY) &&
			lu->GetW (displW) &&
			lu->GetH (displH))
		{
			if (FindGuidedVertValue (displY, d, &guidedValue, &guidedValuePxl, view))
			{
				nearest = lu;
				nearestX = displX;
				nearestW = displW;
				nearestY = displY;
				nearestH = displH;
			}
		}
	}
	if (nearest != NULL)
	{
		RECT ctrl;
		double vdx = (double) (view->right - view->left - 1);
		double vdy = (double) (view->bottom - view->top - 1);
		CALC_CTRL_RECT (ctrl, view->left, view->top, vdx, vdy,
			nearestX, nearestY, nearestW, nearestH);
		bool closeEnough = ::abs (ctrl.top - guidedValuePxl) <= gsnap;
		if (closeEnough)
		{
			double guidedDeltaY = guidedValue - nearestY;
			nearest->SetY (guidedValue, inBases, false);
			nearest->SetH (nearestH - guidedDeltaY, inBases, false);
			nearest->TransformDone ();
			
			for (int j = 0; j < m; ++j)
			{
				CRscLayoutUnit *lu = arr[j];
				if (lu != nearest)
				{
					double y, h;
					if (lu->GetX (y) &&
						lu->GetW (h))
					{
						lu->SetX (y + guidedDeltaY, inBases, false);
						lu->SetW (h - guidedDeltaY, inBases, false);
						lu->TransformDone ();
					}
				}
			}
		}
	}
}

void CRscEditorDoc::ApplyGuidesToBottom (CArray<CRscLayoutUnit*, CRscLayoutUnit*> &arr, 
									bool inBases, RECT *view)
{
	int m = arr.GetSize ();
	if (m == 0)
	{
		return;
	}

	int gsnap = 3;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDE_SNAP, gsnap);

	CRscLayoutUnit *nearest = NULL;
	double nearestX = -1.0, nearestW = -1.0;
	double nearestY = -1.0, nearestH = -1.0;
	double d = 1.0;
	double displX, displW;
	double displY, displH;
	double guidedValue = -1.0;
	int guidedValuePxl = -1;
	for (int j = 0; j < m; ++j)
	{
		CRscLayoutUnit *lu = arr[j];
		if (lu->GetX (displX) &&
			lu->GetY (displY) &&
			lu->GetW (displW) &&
			lu->GetH (displH))
		{
			if (FindGuidedVertValue (displY + displH, d, &guidedValue, &guidedValuePxl, view))
			{
				nearest = lu;
				nearestX = displX;
				nearestW = displW;
				nearestY = displY;
				nearestH = displH;
			}
		}
	}
	if (nearest != NULL)
	{
		RECT ctrl;
		double vdx = (double) (view->right - view->left - 1);
		double vdy = (double) (view->bottom - view->top - 1);
		CALC_CTRL_RECT (ctrl, view->left, view->top, vdx, vdy, 
			nearestX, nearestY, nearestW, nearestH);
		bool closeEnough = ::abs (ctrl.bottom - 1 - guidedValuePxl) <= gsnap;
		if (closeEnough)
		{
			double guidedDeltaH = guidedValue - nearestY - nearestH;
			nearest->SetH (guidedValue - nearestY, inBases, false);
			nearest->TransformDone ();
			for (int j = 0; j < m; ++j)
			{
				CRscLayoutUnit *lu = arr[j];
				if (lu != nearest)
				{
					double h;
					if (lu->GetH (h))
					{
						lu->SetH (h + guidedDeltaH, inBases, false);
						lu->TransformDone ();
					}
				}
			}
		}
	}
}

void CRscEditorDoc::ApplyItemSnapToPosition (CArray<CRscLayoutUnit*, CRscLayoutUnit*> &arr, 
									bool inBases, RECT *view)
{
	int m = arr.GetSize ();
	if (m == 0)
	{
		return;
	}

	int gsnap = 3;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDE_SNAP, gsnap);

	CRscLayoutUnit *nearest = NULL;
	double nearestX = -1, nearestW = -1;
	double nearestY = -1, nearestH = -1;
	bool topLeft = true;
	double d = 1.0;
	double displX, displW;
	double displY, displH;
	for (int j = 0; j < m; ++j)
	{
		double guidedValue = -1.0;
		int guidedValuePxl = -1;
		CRscLayoutUnit *lu = arr[j];
		if (lu->GetX (displX) &&
			lu->GetY (displY) &&
			lu->GetW (displW) &&
			lu->GetH (displH))
		{
			if (FindItemSnapVertValue (displY, d, &guidedValue, &guidedValuePxl, view))
			{
				nearest = lu;
				nearestX = displX;
				nearestW = displW;
				nearestY = displY;
				nearestH = displH;
				topLeft = true;
			}
			if (FindItemSnapVertValue (displY + displH, d, &guidedValue, &guidedValuePxl, view))
			{
				nearest = lu;
				nearestX = displX;
				nearestW = displW;
				nearestY = displY;
				nearestH = displH;
				topLeft = false;
			}
		}
		if (nearest != NULL)
		{
			RECT ctrl;
			double vdx = (double) (view->right - view->left - 1);
			double vdy = (double) (view->bottom - view->top - 1);
			CALC_CTRL_RECT (ctrl, view->left, view->top, vdx, vdy, 
				nearestX, nearestY, nearestW, nearestH);
			bool closeEnough = false;
			double guidedDeltaY = -1.0;
			if (topLeft)
			{
				closeEnough = ::abs (ctrl.top - guidedValuePxl) <= gsnap;
				if (closeEnough)
				{
					guidedDeltaY = guidedValue - nearestY;
					nearest->SetY (guidedValue, inBases, false);
					nearest->TransformDone ();
				}
			}
			else
			{
				closeEnough = ::abs (ctrl.bottom - 1 - guidedValuePxl) <= gsnap;
				if (closeEnough)
				{
					guidedDeltaY = guidedValue - nearestY - nearestH;
					nearest->SetY (guidedValue - nearestH, inBases, false);
					nearest->TransformDone ();
				}
			}
			if (closeEnough)
			{
				for (j = 0; j < m; ++j)
				{
					CRscLayoutUnit *lu = arr[j];
					if (lu != nearest)
					{
						double y;
						if (lu->GetY (y))
						{
							lu->SetY (y + guidedDeltaY, inBases, false);
							lu->TransformDone ();
						}
					}
				}
			}
		}
	}

	nearest = NULL;
	topLeft = true;
	d = 1.0;
	for (int j = 0; j < m; ++j)
	{
		double guidedValue = -1.0;
		int guidedValuePxl = -1;
		CRscLayoutUnit *lu = arr[j];
		if (lu->GetX (displX) &&
			lu->GetY (displY) &&
			lu->GetW (displW) &&
			lu->GetH (displH))
		{
			if (FindItemSnapHorzValue (displX, d, &guidedValue, &guidedValuePxl, view))
			{
				nearest = lu;
				nearestX = displX;
				nearestW = displW;
				nearestY = displY;
				nearestH = displH;
				topLeft = true;
			}
			if (FindItemSnapHorzValue (displX + displW, d, &guidedValue, &guidedValuePxl, view))
			{
				nearest = lu;
				nearestX = displX;
				nearestW = displW;
				nearestY = displY;
				nearestH = displH;
				topLeft = false;
			}
		}
		if (nearest != NULL)
		{
			RECT ctrl;
			double vdx = (double) (view->right - view->left - 1);
			double vdy = (double) (view->bottom - view->top - 1);
			CALC_CTRL_RECT (ctrl, view->left, view->top, vdx, vdy, 
				nearestX, nearestY, nearestW, nearestH);
			bool closeEnough = false;
			double guidedDeltaX = -1.0;
			if (topLeft)
			{
				closeEnough = ::abs (ctrl.left - guidedValuePxl) <= gsnap;
				if (closeEnough)
				{
					guidedDeltaX = guidedValue - nearestX;
					nearest->SetX (guidedValue, inBases, false);
					nearest->TransformDone ();
				}
			}
			else
			{
				closeEnough = ::abs (ctrl.right - 1 - guidedValuePxl) <= gsnap;
				if (closeEnough)
				{
					guidedDeltaX = guidedValue - nearestX - nearestW;
					nearest->SetX (guidedValue - nearestW, inBases, false);
					nearest->TransformDone ();
				}
			}
			if (closeEnough)
			{
				for (j = 0; j < m; ++j)
				{
					CRscLayoutUnit *lu = arr[j];
					if (lu != nearest)
					{
						double x;
						if (lu->GetX (x))
						{
							lu->SetX (x + guidedDeltaX, inBases, false);
							lu->TransformDone ();
						}
					}
				}
			}
		}
	}
}

void CRscEditorDoc::SetReferenceDelta (CArray<CRscLayoutUnit*, CRscLayoutUnit*> &arr, const CPoint &point, const RECT *view)
{
	int n = arr.GetSize ();
	double fx = (double) (point.x - view->left) / (double) (view->right - view->left - 1);
	double fy = (double) (point.y - view->top) / (double) (view->bottom - view->top - 1);
	for (int i = 0; i < n; ++i)
	{
		arr[i]->SetReferenceDelta (fx, fy);
	}
}

void CRscEditorDoc::SetPathName (LPCTSTR lpszPathName, BOOL bAddToMRU)
{
	CDocument::SetPathName(lpszPathName, bAddToMRU);

	SetTitle(m_strPathName);
}
