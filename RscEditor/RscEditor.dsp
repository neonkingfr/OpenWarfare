# Microsoft Developer Studio Project File - Name="RscEditor" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=RscEditor - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "RscEditor.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "RscEditor.mak" CFG="RscEditor - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "RscEditor - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "RscEditor - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "RscEditor"
# PROP Scc_LocalPath ".."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "RscEditor - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GR /O2 /I "w:\c" /D "NDEBUG" /D _RELEASE=1 /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D MFC_NEW=1 /YX"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 winmm.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "RscEditor - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GR /Zi /Od /I "w:\c" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D MFC_NEW=1 /FR /YX"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x405 /d "_DEBUG"
# ADD RSC /l 0x405 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 winmm.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "RscEditor - Win32 Release"
# Name "RscEditor - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\src\ClassEditor.cpp
# End Source File
# Begin Source File

SOURCE=.\src\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\src\Preferences.cpp
# End Source File
# Begin Source File

SOURCE=.\src\PreferencesDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscClassSpace.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscEditor.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscEditor.rc
# End Source File
# Begin Source File

SOURCE=.\src\RscEditorDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscEditorItems.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscEditorLex.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscEditorParser.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscEditorPreview.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscEditorREDParser.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscEditorView.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscEditorViewLayout.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscFavouriteSpace.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscIDEditor.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscInclFileSpace.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscMacro.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscSpaceTab.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscWorkTab.cpp
# End Source File
# Begin Source File

SOURCE=.\src\RscWorkView.cpp
# End Source File
# Begin Source File

SOURCE=.\src\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\src\ClassEditor.h
# End Source File
# Begin Source File

SOURCE=.\src\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\src\Preferences.h
# End Source File
# Begin Source File

SOURCE=.\src\PreferencesDialog.h
# End Source File
# Begin Source File

SOURCE=.\src\Resource.h
# End Source File
# Begin Source File

SOURCE=.\src\RscClassSpace.h
# End Source File
# Begin Source File

SOURCE=.\src\RscEditor.h
# End Source File
# Begin Source File

SOURCE=.\src\RscEditorDoc.h
# End Source File
# Begin Source File

SOURCE=.\src\RscEditorItems.h
# End Source File
# Begin Source File

SOURCE=.\src\RscEditorLex.h
# End Source File
# Begin Source File

SOURCE=.\src\RscEditorParser.h
# End Source File
# Begin Source File

SOURCE=.\src\RscEditorPreview.h
# End Source File
# Begin Source File

SOURCE=.\src\RscEditorREDParser.h
# End Source File
# Begin Source File

SOURCE=.\src\RscEditorView.h
# End Source File
# Begin Source File

SOURCE=.\src\RscFavouriteSpace.h
# End Source File
# Begin Source File

SOURCE=.\src\RscIDEditor.h
# End Source File
# Begin Source File

SOURCE=.\src\RscInclFileSpace.h
# End Source File
# Begin Source File

SOURCE=.\src\RscMacro.h
# End Source File
# Begin Source File

SOURCE=.\src\RscSpaceTab.h
# End Source File
# Begin Source File

SOURCE=.\src\RscWorkTab.h
# End Source File
# Begin Source File

SOURCE=.\src\RscWorkView.h
# End Source File
# Begin Source File

SOURCE=.\src\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\src\res\about.bmp
# End Source File
# Begin Source File

SOURCE=.\src\res\layoutbar.bmp
# End Source File
# Begin Source File

SOURCE=.\src\res\RscEditor.ico
# End Source File
# Begin Source File

SOURCE=.\src\res\RscEditor.rc2
# End Source File
# Begin Source File

SOURCE=.\src\res\RscEditorDoc.ico
# End Source File
# Begin Source File

SOURCE=.\src\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\src\res\workspac.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\src\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\src\RscEditor.reg
# End Source File
# End Target
# End Project
