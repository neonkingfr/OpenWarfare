
#include "stdafx.h"
#include "rsceditor.h"

#include "RscEditorItems.h"
#include "RscSpaceTab.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"
#include "RscEditorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CRscSpaceTab::CRscSpaceTab()
: _view (NULL)
{
}

CRscSpaceTab::~CRscSpaceTab()
{
}


BEGIN_MESSAGE_MAP(CRscSpaceTab, CTabCtrl)
	//{{AFX_MSG_MAP(CRscSpaceTab)
	ON_NOTIFY_REFLECT(TCN_SELCHANGE, OnSelChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CRscSpaceTab::DropRegister ()
{
	if (_dropTarget._control == NULL)
	{
		_dropTarget._control = this;
		_dropTarget.Register (this);
	}
}

DROPEFFECT CRscSpaceTab::OnDragEnter(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point)
{
	return OnDragOver(pWnd, pDataObject, dwKeyState, point);
}

DROPEFFECT CRscSpaceTab::OnDragOver(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point) 
{
	if (_dragPoint == point)
	{
		TCHITTESTINFO hitInfo;
		hitInfo.pt = point;
		int hit = HitTest (&hitInfo);
		if (hit >= 0)
		{
			DWORD ct = timeGetTime ();
			if (ct - _dragTime > 500)
			{
				SetCurSel (hit);
				_view->SetSpaceTab (hit);
				_dragTime = ct;
			}
		}
	}
	else
	{
		_dragPoint = point;
		_dragTime = timeGetTime ();
	}

	return DROPEFFECT_NONE;
}

void CRscSpaceTab::OnDragLeave(CWnd *pWnd) 
{
}

BOOL CRscSpaceTab::OnDrop (CWnd *pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point)
{
	ASSERT_VALID(this);

	// clean up focus rect
	OnDragLeave(pWnd);

	return FALSE;
}

void CRscSpaceTab::OnSelChange(NMHDR* pNMHDR, LRESULT* pResult) 
{
	*pResult = 0;

	_view->SetSpaceTab (GetCurSel ());
}
