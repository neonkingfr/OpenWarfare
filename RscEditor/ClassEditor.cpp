
#include "stdafx.h"
#include "RscEditor.h"

#include "RscEditorItems.h"
#include "RscMacro.h"
#include "RscEditorDoc.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"
#include "RscEditorREDParser.h"
#include "RscEditorView.h"
#include "ClassEditor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CClassEditor::CClassEditor(CWnd* pParent /*=NULL*/)
	: CDialog(CClassEditor::IDD, pParent), _font (NULL), _codeEdited (false),
	_view (NULL), _param (NULL), _ctrlDown (false), _inFavourites (false)
{
	//{{AFX_DATA_INIT(CClassEditor)
	//}}AFX_DATA_INIT
}


CClassEditor::~CClassEditor ()
{
	if (_font != NULL)
	{
		delete _font;
		_font = NULL;
	}
}


void CClassEditor::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CClassEditor)
	DDX_Control(pDX, IDC_CLASSTEXT, _classTextCtrl);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CClassEditor, CDialog)
	//{{AFX_MSG_MAP(CClassEditor)
	ON_WM_SIZE()
	ON_EN_CHANGE(IDC_CLASSTEXT, OnChangeClassText)
	ON_BN_CLICKED(IDC_APPLY, OnApply)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CClassEditor::InitEditor (CRscParamBase *pb, bool inFavourites)
{
	_param = pb;
	_inFavourites = inFavourites;
	if (::IsWindow (_classTextCtrl.m_hWnd))
	{
		if (pb != NULL)
		{
			CString tab ("\t");
			((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::TABULATOR, tab);
			LPTSTR text = _compileCode.GetBuffer (pb->GetTextLength (tab));
			pb->GetText (text, tab);
			_compileCode.ReleaseBuffer ();
			_classTextCtrl.SetWindowText (_compileCode);
			_classTextCtrl.SetReadOnly (pb->IsEditable () ? FALSE : TRUE);
		}
		else
		{
			_classTextCtrl.SetWindowText (_T (""));
			_classTextCtrl.SetReadOnly (TRUE);
		}
	}
}

BOOL CClassEditor::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if (_font == NULL)
	{
		_font = new CFont ();
		if (_font != NULL)
		{
			if (_font->CreatePointFont (100, _T ("Courier")))
			{
				_classTextCtrl.SetFont (_font);
			}
			else
			{
				delete _font;
				_font = NULL;
			}
		}
	}
	InitEditor (NULL, false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CClassEditor::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	CWnd *ctrl;
	ctrl = GetDlgItem (IDOK);
	if (ctrl == NULL)
	{
		return;
	}
	RECT r;
	ctrl->GetWindowRect (&r);
	ScreenToClient (&r);
	int dy = r.bottom - r.top;
	r.left = cx - UI_SPACE - (r.right - r.left);
	r.right = cx - UI_SPACE;
	r.top = UI_SPACE;
	r.bottom = UI_SPACE + dy;
	ctrl->MoveWindow (&r, TRUE);

	ctrl = GetDlgItem (IDCANCEL);
	if (ctrl == NULL)
	{
		return;
	}
	r.top = r.bottom + UI_SPACE;
	r.bottom = r.top + dy;
	ctrl->MoveWindow (&r, TRUE);

	ctrl = GetDlgItem (IDC_APPLY);
	if (ctrl == NULL)
	{
		return;
	}
	r.top = r.bottom + UI_SPACE;
	r.bottom = r.top + dy;
	ctrl->MoveWindow (&r, TRUE);

	ctrl = GetDlgItem (IDC_CLASSTEXT);
	if (ctrl == NULL)
	{
		return;
	}
	r.right = r.left - UI_SPACE;
	r.left = UI_SPACE;
	r.top = UI_SPACE;
	r.bottom = cy - UI_SPACE;
	ctrl->MoveWindow (&r, TRUE);
}

void CClassEditor::OnChangeClassText() 
{
	_codeEdited = true;
}

void CClassEditor::OnApply() 
{
	if (_codeEdited)
	{
		_classTextCtrl.GetWindowText (_compileCode);
		CRscEditorDoc *doc = _view->GetDocument ();
		if (doc->OpenCommand (_T ("Edit Code"), true, UNDO_EDIT_CODE))
		{
			if (doc->ApplyClassCode (_compileCode, _param, _inFavourites, &_compileCode))
			{
				_classTextCtrl.SetWindowText (_compileCode);
				_codeEdited = false;
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
				doc->CloseCommand ();
			}
			else
			{
				doc->DisposeCommand ();
			}
		}
	}
}

void CClassEditor::OnOK ()
{
	if (_codeEdited)
	{
		_classTextCtrl.GetWindowText (_compileCode);
		CRscEditorDoc *doc = _view->GetDocument ();
		if (doc->OpenCommand (_T ("Edit Code"), true, UNDO_EDIT_CODE))
		{
			if (doc->ApplyClassCode (_compileCode, _param, _inFavourites, &_compileCode))
			{
				_classTextCtrl.SetWindowText (_compileCode);
				_codeEdited = false;
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
				doc->CloseCommand ();
				CDialog::OnOK ();
			}
			else
			{
				doc->DisposeCommand ();
			}
		}
	}
	else
	{
		CDialog::OnOK ();
	}
}

BOOL CClassEditor::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->hwnd == _classTextCtrl.m_hWnd)
	{
		if (pMsg->message == WM_KEYDOWN)
		{
			switch (pMsg->wParam)
			{
			case VK_TAB:
				// _classTextCtrl.SendMessage (WM_CHAR, _T ('\t'), pMsg->lParam);
				_classTextCtrl.ReplaceSel (_T ("\t"));
				return TRUE;
				
			case VK_RETURN:
				if (_ctrlDown)
				{
					SendMessage (WM_COMMAND, IDOK, NULL);
					_ctrlDown = false;
					return TRUE;
				}
				break;

			case VK_CONTROL:
				_ctrlDown = true;
				break;
			}
		}
		else if (pMsg->message == WM_KEYUP)
		{
			switch (pMsg->wParam)
			{
			case VK_CONTROL:
				_ctrlDown = false;
				break;
			}
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}
