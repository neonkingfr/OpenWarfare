
#if !defined(AFX_RSCEDITORDOC_H__3B472C5A_14BB_46D0_B52C_491294476912__INCLUDED_)
#define AFX_RSCEDITORDOC_H__3B472C5A_14BB_46D0_B52C_491294476912__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//! Number of available parameters for layout functions.
#define LAYOUT_PARAMS_SIZE 16

//! Enumeration of what is just now selected.
enum
{
  //! Nothing is selected.
	SELECTED_NONE_ITEM = 0,

  //! Right one item is selectedd.
	SELECTED_ONE_ITEM,

  //! A few ungroupped items.
	SELECTED_NONE_GROUP,

  //! Just items of one group.
	SELECTED_ONE_GROUP,

  //! Items from several groups or not all items of one group.
	SELECTED_PART_GROUPS,
};

//! Enumeration of update hints for calling UpdateAllViews method of CDocument class.
enum
{
	UH_NEW_PARAM = 32001,
	//UH_NEW_PARAM_2,
	UH_NEW_VALUE,
	UH_DEL_PARAM,
	UH_NEW_BASE,
	UH_CHANGE_MONITOR,
	UH_REMOVE_FAVOURITE,
	UH_CHANGE_FAVOURITE,
	UH_INSERT_FAVOURITE,
	UH_MOVE_FAVOURITE,
	UH_CHANGE_CODE,
	UH_ADD_INCLUDE,
	UH_REMOVE_INCLUDE,
	UH_DELETE_CONTENTS,
	UH_UPDATE_PREVIEW,
};

class CRscEditorDoc;

//! Class of hint object for calling UpdateAllViews method of CDocument class.
struct CUpdateDescriptor : public CObject
{
	union
	{
		struct // UH_NEW_PARAM
		{
			CRscParamBase *_param;
			bool _inFavourites;
		} _newParam;
		/*
		struct // UH_NEW_PARAM_2
		{
			CRscParamBase *_param;
		} _newParam2;
		*/
		struct // UH_NEW_VALUE
		{
			CRscParamBase *_param;
		} _newValue;
		struct // UH_DEL_PARAM
		{
			CRscParamBase *_param;
		} _delParam;
		struct // UH_NEW_BASE
		{
			CRscClass *_class;
		} _newBase;
		struct // UH_CHANGE_MONITOR
		{
			CRscClass *_oldOne;
			CRscClass *_newOne;
		} _changeMonitor;
		struct // UH_REMOVE_FAVOURITE
		{
			CRscClass *_class;
		} _remFavourite;
		struct // UH_CHANGE_FAVOURITE
		{
			CRscClass *_oldClass;
			CRscClass *_newClass;
		} _changeFavourite;
		struct // UH_INSERT_FAVOURITE
		{
			CRscClass *_class;
		} _insertFavourite;
		struct // UH_MOVE_FAVOURITE
		{
			CRscClass *_class;
			int _index;
		} _moveFavourite;
		struct // UH_CHANGE_CODE
		{
			CRscParamBase *_param;
			bool _inFavourites;
		} _changeCode;
		struct // UH_ADD_INCLUDE
		{
			CRscInclude *_include;
			int _from;
		} _addInclude;
		struct // UH_REMOVE_INCLUDE
		{
			CRscInclude *_include;
			int _from;
		} _removeInclude;
	};
};

//! Base class of ementary entry for undo/redo actions.
class CUndoEntry
{
public:
  //! Flag value - was this action done? (or undone? = false)
	bool _done;

public:
	CUndoEntry ();
	virtual ~CUndoEntry ();
	
	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - new parameter (class or attribute) added.
class CUndoNewParam : public CUndoEntry
{
public:
	CRscParamBase *_param;
	int _index;
	bool _inFavourites;
	int _favouriteIndex;

public:
	CUndoNewParam (CRscParamBase *param, int index, bool inFavourites, int favouriteIndex);
	virtual ~CUndoNewParam ();
	
	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - selected another class.
class CUndoSelectClass : public CUndoEntry
{
public:
	CRscClass *_class;
	bool _selFlag;
	bool _oldSelFlag;

public:
	CUndoSelectClass (CRscClass *cl, bool selFlag, bool oldSelFlag);
	virtual ~CUndoSelectClass ();
	
	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - value deleted from parameter array.
class CUndoDeleteParamValue : public CUndoEntry
{
public:
	CRscParamBase *_param;
	CArray<CRscAttributeValueBase*, CRscAttributeValueBase*> *_array;
	int _index;
	CRscAttributeValueBase *_value;

public:
	CUndoDeleteParamValue (CRscParamBase *param, 
		CArray<CRscAttributeValueBase*, CRscAttributeValueBase*> *array,
		int index, CRscAttributeValueBase *value);
	virtual ~CUndoDeleteParamValue ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - value added in parameter array.
class CUndoAddParamValue : public CUndoEntry
{
public:
	CRscParamBase *_param;
	CArray<CRscAttributeValueBase*, CRscAttributeValueBase*> *_array;
	int _index;
	CRscAttributeValueBase *_value;

public:
	CUndoAddParamValue (CRscParamBase *param, 
		CArray<CRscAttributeValueBase*, CRscAttributeValueBase*> *array,
		int index, CRscAttributeValueBase *value);
	virtual ~CUndoAddParamValue ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - parameter value changed.
class CUndoChangeParamValue : public CUndoEntry
{
public:
	CRscParamBase *_param;
	CRscAttributeValueBase **_change;
	CRscAttributeValueBase *_backupValue;

public:
	CUndoChangeParamValue (CRscParamBase *param, 
		CRscAttributeValueBase **change, CRscAttributeValueBase *backupValue);
	virtual ~CUndoChangeParamValue ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - base class changed.
class CUndoChangeBase : public CUndoEntry
{
public:
	CRscClass *_class;
	CRscClass *_oldBase;
	CRscClass *_newBase;

public:
	CUndoChangeBase (CRscClass *cl, CRscClass *oldBase, CRscClass *newBase);
	virtual ~CUndoChangeBase ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - monitor class changed
class CUndoChangeMonitor : public CUndoEntry
{
public:
	CRscClass *_oldMonitorClass;
	CRscClass *_newMonitorClass;

public:
	CUndoChangeMonitor (CRscClass *oldClass, CRscClass *newClass);
	virtual ~CUndoChangeMonitor ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - parameter removed from favourite list.
class CUndoRemoveFavourite : public CUndoEntry
{
public:
	CRscClass *_class;
	int _index;

public:
	CUndoRemoveFavourite (CRscClass *cl, int index);
	virtual ~CUndoRemoveFavourite ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - item in favourite list changed.
class CUndoChangeFavourite : public CUndoEntry
{
public:
	int _index;
	CRscClass *_oldClass;
	CRscClass *_newClass;

public:
	CUndoChangeFavourite (int index, CRscClass *oldClass, CRscClass *newClass);
	virtual ~CUndoChangeFavourite ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - parameter added into favourite list.
class CUndoAddFavourite : public CUndoEntry
{
public:
	CRscClass *_class;
	int _index;

public:
	CUndoAddFavourite (CRscClass *cl, int index);
	virtual ~CUndoAddFavourite ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - order of tems in favourite list changed.
class CUndoMoveFavourite : public CUndoEntry
{
public:
	CRscClass *_class;
	int _oldIndex;
	int _newIndex;

public:
	CUndoMoveFavourite (CRscClass *cl, int oldIndex, int newIndex);
	virtual ~CUndoMoveFavourite ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - parameter deleted.
class CUndoDeleteParam : public CUndoEntry
{
public:
	CRscParamBase *_param;
	int _index;

public:
	CUndoDeleteParam (CRscParamBase *param, int index);
	virtual ~CUndoDeleteParam ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - class has been changed (e.g. in class text editor).
class CUndoChangeClassCode : public CUndoEntry
{
public:
	CRscParamBase *_oldCode;
	CRscParamBase *_newCode;
	int _index;

public:
	CUndoChangeClassCode (CRscParamBase *oldCode, CRscParamBase *newCode, int index);
	virtual ~CUndoChangeClassCode ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - new file included.
class CUndoAddInclude : public CUndoEntry
{
public:
	CRscInclude *_incl;
	int _from;

public:
	CUndoAddInclude (CRscInclude *incl, int from);
	virtual ~CUndoAddInclude ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - included file removed.
class CUndoRemoveInclude : public CUndoEntry
{
public:
	CArray<CRscParamBase*, CRscParamBase*> _params;
	CRscInclude *_incl;
	int _from;

public:
	CUndoRemoveInclude (CRscInclude *incl, int from, int to, CRscEditorDoc *doc);
	virtual ~CUndoRemoveInclude ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - group of items deleted.
class CUndoDelGroup : public CUndoEntry
{
public:
	CRscGroup	*_group;
	int			_index;

public:
	CUndoDelGroup (CRscGroup *group, int index);
	virtual ~CUndoDelGroup ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - group of items created.
class CUndoNewGroup : public CUndoEntry
{
public:
	CRscGroup	*_group;
	int			_index;

public:
	CUndoNewGroup (CRscGroup *group, int index);
	virtual ~CUndoNewGroup ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - item removed from a group.
class CUndoRemoveFromGroup : public CUndoEntry
{
public:
	CRscClass	*_class;
	CRscGroup	*_group;
	int			_index;

public:
	CUndoRemoveFromGroup (CRscClass *cl, CRscGroup *group, int index);
	~CUndoRemoveFromGroup ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - new guide created (vertical or horizontal).
class CUndoCreateGuide : public CUndoEntry
{
public:
	CRscGuide *_guide;

public:
	CUndoCreateGuide (CRscGuide *guide);
	virtual ~CUndoCreateGuide ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - new value added into guide.
class CUndoAddGuide : public CUndoEntry
{
public:
	CRscGuide *_guide;
	int _type;
	double _value;

public:
	CUndoAddGuide (CRscGuide *guide, int type, double value);
	virtual ~CUndoAddGuide ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - value in guide changed.
class CUndoMoveGuide : public CUndoEntry
{
public:
	CRscGuide *_guide;
	int _type;
	int _index;
	double _oldValue;
	double _newValue;

public:
	CUndoMoveGuide (CRscGuide *guide, int type, int index, double oldValue, double newValue);
	virtual ~CUndoMoveGuide ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - value removed from guide.
class CUndoRemoveGuide : public CUndoEntry
{
public:
	CRscGuide *_guide;
	int _type;
	int _index;
	double _value;

public:
	CUndoRemoveGuide (CRscGuide *guide, int type, int index, double value);
	virtual ~CUndoRemoveGuide ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Action for undo/redo system - guide (vertical or horizontal) deleted.
class CUndoDeleteGuide : public CUndoEntry
{
public:
	CRscGuide *_guide;
	int _index;

public:
	CUndoDeleteGuide (CRscGuide *guide, int index);
	virtual ~CUndoDeleteGuide ();

	virtual void Undo (CRscEditorDoc *doc);
	virtual void Redo (CRscEditorDoc *doc);
};

//! Command of undo/redo system - list of individual actions.
class CUndoCommand
{
public:
	CArray<CUndoEntry*, CUndoEntry*> _entries;
	CString _name;
	bool _postPreviewUpdate;

public:
	CUndoCommand (LPCTSTR name, bool postPreviewUpdate);
	~CUndoCommand ();
};

//! Control item iterator drawing items into preview pane.
class CRscClassPainter : public CRscClassEnumerator
{
public:
  //! Preview pane rectangle.
	RECT	*_view;

  //! Precalculated value - preview pane width.
	double	_dx;

  //! Precalculated value - preview pane height.
	double	_dy;

  //! Displayed classes.
	CArray<CRscClass*, CRscClass*>	&_displayed;

  //! Current selected item group (objects/controls/background controls). The control item color depends on it.
	int		_selItemGroup;

public:
	CRscClassPainter (CRscEditorDoc *doc, RECT *view, 
		CArray<CRscClass*, CRscClass*> &displayed, bool *tags, int selItemGroup)
		: CRscClassEnumerator (doc, true, tags, true), _view (view),
		_dx ((double) (view->right - view->left - 1)), 
		_dy ((double) (view->bottom - view->top - 1)),
		_displayed (displayed), _selItemGroup (selItemGroup)
	{
		int n = _displayed.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_displayed[i]->_displayed = false;
			_displayed[i]->_itemGroup = rscEditorItemGroupNone;
			_displayed[i]->_valueReferences.RemoveAll ();
		}
		_displayed.RemoveAll ();
	}

  //! Draw control items stored in '_displayed' w/o drag boxes.
	void Draw (CDC *dc, bool drawDragBoxes)
	{
		int n = _displayed.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CRscClass *cl = _displayed[i];
			CPen p (PS_SOLID, 1, cl->_displayedColor);
			dc->SelectObject (&p);
			dc->SelectStockObject (NULL_BRUSH);
			RECT r = cl->_displayedRect;
			dc->Rectangle (&r);
			if (cl->_selected)
			{
				++r.left;
				++r.top;
				--r.right;
				--r.bottom;
				dc->Rectangle (&r);
				--r.left;
				--r.top;
				++r.right;
				++r.bottom;
				if (drawDragBoxes)
				{
					dc->SelectStockObject (BLACK_PEN);
					dc->SelectStockObject (LTGRAY_BRUSH);
					RECT rr;
					CRect rf;
					DRAW_DRAG_BOX (dc, r.left, r.top, _view, rr, rf)
					DRAW_DRAG_BOX (dc, r.right - 1, r.top, _view, rr, rf)
					DRAW_DRAG_BOX (dc, r.left, r.bottom - 1, _view, rr, rf)
					DRAW_DRAG_BOX (dc, r.right - 1, r.bottom - 1, _view, rr, rf)
					DRAW_DRAG_BOX (dc, (r.left + r.right - 1) >> 1, r.top, _view, rr, rf)
					DRAW_DRAG_BOX (dc, (r.left + r.right - 1) >> 1, r.bottom - 1, _view, rr, rf)
					DRAW_DRAG_BOX (dc, r.left, (r.top + r.bottom - 1) >> 1, _view, rr, rf)
					DRAW_DRAG_BOX (dc, r.right - 1, (r.top + r.bottom - 1) >> 1, _view, rr, rf)
				}
			}
			CFont font;
			if (font.CreatePointFont (80, _T ("Arial"), dc))
			{
				COLORREF oldc = dc->SetTextColor (cl->_displayedColor);
				int oldBkMode = dc->SetBkMode (TRANSPARENT);
				CFont *oldFont = dc->SelectObject (&font);
				dc->DrawText (cl->_displayedText, &r, DT_LEFT | DT_WORDBREAK);
				dc->SelectObject (oldFont);
				dc->SetBkMode (oldBkMode);
				dc->SetTextColor (oldc);
				font.DeleteObject ();
			}
		}
	}

protected:
  //! Process each control item reference. Calculate control item color, its rectangle, etc.
	virtual bool EnumerateFce (CRscClass *cl, CRscAttributeValueElement *val)
	{
		if (!cl->_displayed)
		{
			_displayed.Add (cl);
			cl->_displayed = true;
			cl->_itemGroup = _curItemGroup;
		}
		
		if (val != NULL)
		{
			cl->_valueReferences.Add (val);
		}

		if (cl->_selected)
		{
			cl->_displayedColor = RGB (0, 0, 0);
		}
		else if (_curItemGroup == _selItemGroup)
		{
			cl->_displayedColor = RGB (0, 255, 0);
		}
		else
		{
			cl->_displayedColor = RGB (0, 0, 255);
		}
		CALC_CTRL_RECT (cl->_displayedRect, _view->left, _view->top, _dx, _dy, _x, _y, _w, _h)
		cl->_displayedText = _text;

		return false;
	}
};

//! Code of the currently opened undo command in document.
enum EUndoType
{
	UNDO_NONE,
	UNDO_EDIT_CODE,
	UNDO_NEW_DERIVED_CLASS,
	UNDO_COPY_CLASS,
	UNDO_MOVE_CLASS,
	UNDO_INSERT_CLASS,
	UNDO_CUT,
	UNDO_PASTE_CLASS,
	UNDO_SET_MONITOR_CLASS,
	UNDO_NEW_INNER_CLASS,
	UNDO_ADD_FAVOURITE,
	UNDO_REMOVE_FAVOURITE,
	UNDO_INCLUDE_FILE,
	UNDO_DELETE_CLASS,
	UNDO_DELETE_ITEMS,
	UNDO_REMOVE_ITEMS,
	UNDO_COPY_ITEMS,
	UNDO_NEW_DERIVED_ITEMS,
	UNDO_MAKE_INDEPENDENT,
	UNDO_NEW_ITEM,
	UNDO_NEW_CLASS,
	UNDO_EDIT_CLASS,
	UNDO_EDIT_ITEM,
	UNDO_SEND_BACKWARD,
	UNDO_BRING_FORWARD,
	UNDO_SEND_TO_BACK,
	UNDO_BRING_TO_FRONT,
	UNDO_GROUP_ITEMS,
	UNDO_UNGROUP_ITEMS,
	UNDO_REGROUP_ITEMS,
	UNDO_DELETE_GUIDE,
	UNDO_ALIGN_BOTTOM,
	UNDO_ALIGN_RIGHT,
	UNDO_ALIGN_LEFT,
	UNDO_ALIGN_TOP,
	UNDO_ALIGN_HOR_CENTER,
	UNDO_ALIGN_VER_CENTER,
	UNDO_SAME_WIDTH,
	UNDO_SAME_HEIGHT,
	UNDO_SAME_SIZE,
	UNDO_CENTER_HOR,
	UNDO_CENTER_VER,
	UNDO_SPACE_EVENLY_ACROSS_DIALOG,
	UNDO_SPACE_EVENLY_DOWN_DIALOG,
	UNDO_SPACE_EVENLY_ACROSS_SELECTION,
	UNDO_SPACE_EVENLY_DOWN_SELECTION,
	UNDO_MOVE_FAVOURITE,
	UNDO_REMOVE_VERT_GUIDE,
	UNDO_ADD_VERT_GUIDE,
	UNDO_REMOVE_HORZ_GUIDE,
	UNDO_ADD_HORZ_GUIDE,
	UNDO_MOVE_VERT_GUIDE,
	UNDO_MOVE_HORZ_GUIDE,
	UNDO_RESIZE_ITEM,
	UNDO_MOVE_ITEM,
	UNDO_ADD_ITEMS,
	UNDO_SIDE_BY_SIDE_TOP,
	UNDO_SIDE_BY_SIDE_LEFT,
	UNDO_SIDE_BY_SIDE_BOTTOM,
	UNDO_SIDE_BY_SIDE_RIGHT,
};

//! Class of the main document type.
class CRscEditorDoc : public CDocument
{
protected:
	CRscEditorDoc(); // create from serialization only
	DECLARE_DYNCREATE(CRscEditorDoc)

public:
  //{ Document structure

  //! List of included files into this document.
	CArray<CRscInclude*, CRscInclude*>		_includedFiles;

  //! List of params (classes or attributes) of this document.
	CArray<CRscParamBase*, CRscParamBase*>	_params;

  //! List of macros used in this document.
	CArray<CRscMacro*, CRscMacro*>			_macros;

  //! List of enums used in this document.
	CArray<CRscEnum*, CRscEnum*>			_enums;

  //}

  //{ Actions

  //! Document's undos.
	CArray<CUndoCommand*, CUndoCommand*>	_undos;

  //! Document's redos.
	CArray<CUndoCommand*, CUndoCommand*>	_redos;

  //! Pointer to the current undo command.
	CUndoCommand							*_curUndo;

  //! Code of the current (opened) command.
  EUndoType								_curUndoType;

  //}

  //{ Helper elements

  //! List of macro calls used in this document.
  CArray<CRscMacroCall*, CRscMacroCall*>	_macroCalls;

  //! List of groups used in this document.
	CArray<CRscGroup*, CRscGroup*>			_groups;

  //! Guides of this document.
	CArray<CRscGuide*, CRscGuide*>			_guides;

  //}

  //{ Selection

  //! Currently selected classes.
	CArray<CRscClass*, CRscClass*>			_selectedClasses;

  //! Displayed classes.
	CArray<CRscClass*, CRscClass*>			_displayedClasses;

  //! Favourite classes.
	CArray<CRscClass*, CRscClass*>			_favouriteClasses;

  //! Current monitor class.
	CRscClass								*_monitorClass;

  //}

  //! Document modified? (used when document is opened - true -> parser had to modify it)
	bool									_docModified;

  //! Is saved?
	BOOL									_isSaved;

  //! Is command undoing?
	bool									_undoMode;

  //! Last position when document was saved.
	CUndoCommand							*_lastSavedPos;

  //! Has the document ever been saved?
	BOOL									_everSavedFlag;

public:
  //! Return new name for new class.
	void NewClassName (CString &name, CRscClass *parent);

  //! Add new class.
	int AddClass (CRscClass *cl, CRscClass *in, CRscParamBase *where, bool after);

  //! New inner class.
	CRscClass* NewInnerClass (LPCTSTR newName, CRscClass *parent, bool inFavourites);

  //! New inner class.
	CRscClass* NewInnerClass (CRscClass *cl, bool inFavourites);

  //! New derived class.
	CRscClass* NewDerivedClass (CRscClass *cl, CRscClass *parent, CRscParamBase *where, bool after, bool inFavourites);

  //! Find class.
	CRscClass* FindClassDefinition (const char *name, CRscClass *in, bool useDisabled);

  //! Find base class.
	CRscClass* FindBaseClassDefinition (const char *name, CRscParamBase *from, CRscClass *in, bool useDisabled, bool inclFrom);

  //! Find param in class.
	CRscParamBase* FindParamInClass (const char *name, CRscClass *where, bool useDisabled);

  //! Find next (for Find dialog).
	CRscParamBase* FindNextForward (const char *name, CRscParamBase *last, DWORD flags) const;

  //! Find next (for Find dialog).
	CRscParamBase* FindNextBackward (const char *name, CRscParamBase *last, DWORD flags) const;

  //! Detect collisions (e.g. when some class deleted).
	void VerifyBaseClassesInChildren (CRscParamBase *from, bool repair, CArray<CRscClass*, CRscClass*> &arr, CRscClass *defBaseClass, bool useDisabled);

  //! Detect collisions (e.g. when some class deleted).
	void VerifyBaseClassesInDeriveds (CRscClass *from, bool repair, CArray<CRscClass*, CRscClass*> &arr, CRscClass *defBaseClass, bool useDisabled);

  //! Detect collisions (e.g. when some class deleted).
	void VerifyBaseClasses (CRscParamBase *from, bool repair, CArray<CRscClass*, CRscClass*> &arr, CRscClass *defBaseClass, bool useDisabled);

  //! Returns id for new control ('id' member of the control class).
	int GetNewIdValue ();

  //! Find macro.
	CRscMacro *FindMacro (LPCTSTR id) const;

  //! Returns true if some macro or enum defines this id.
	bool IsRscID (LPCTSTR id) const;

  //! Fill text with macro definitions code.
	void GetMacrosText (CString &text);

  /*! Returns pointer to the class determined by the full name.
      Starting string 'oldClass' in fullName is replaced by 'newClass'.
  */
	CRscClass *GetClassFromFullName (CString &fullName, CString *oldClass, CString *newClass);

  //! Send command to all views of this document.
	BOOL RouteCmdToAllViews (CView *pView, UINT nID, int nCode, void *pExtra, AFX_CMDHANDLERINFO *pHandlerInfo);

  //! Return previous parameter.
	CRscParamBase* FindPrevParam (CRscClass *parent, CRscParamBase *param);

  //! Returns previous favourite parameter.
	CRscClass* FindPrevFavourite (CRscClass *cl);

  //! Find parameter name in children. 
	CRscParamBase* FindParamInChildren (const char *name, CRscParamBase *from, bool inChildren, bool useDisabled);

  //! Find parameter name in parents.
	CRscParamBase* FindParamInParents (CRscParamBase *from, const char *name, bool useDisabled);

  //! Set array parameter value.
	bool SetArrayStringParamInBases (CRscClass *cl, const char *name, LPCTSTR val, bool inBases, bool useDisabled, bool canUndo);

  //! Set float parameter value.
	bool SetFloatParamInBases (CRscClass *cl, const char *name, double val, bool inBases, bool useDisabled, bool canUndo, bool forceUndo);

  //! Open new command (in undo/redo system).
	bool OpenCommand (LPCTSTR name, bool postPreviewUpdate, EUndoType curUndoType);

  //! Close opened command (in undo/redo system).
	void CloseCommand ();

  //! Dispose opened command (in undo/redo system).
	void DisposeCommand ();

  //! Add new action into last opened command.
	void AddCommandEntry (CUndoEntry *undo);

  //! Set document into undo mode.
	void SetUndoMode ();

  //! Set document into normal mode.
	void SetNormalMode ();

  //! Delete control item.
	void DeleteItem (CRscClass *cl, bool *tags, bool displayMsg);

  //! Delete parameter.
	void DeleteParam (CRscParamBase *param);

  //! Ask user to change base classes affected be some other class delete.
	bool AskUserAboutBaseClasses (CArray<CRscClass*, CRscClass*> &arr);

  //! Remove class from group.
	void RemoveFromGroup (CRscClass *cl);

  /*! Prepare parameter (class or attribute) to be deleted.
      Base class of the derived classes will be changed to 'newBaseClass'.
      Using of this class is replaced by 'newClass' (e.g. monitor class).
  */
	void PrepareParamForDelete (CRscParamBase *cl, CRscClass *newClass, CRscClass *newBaseClass);

  //! Apply class code.
	bool ApplyClassCode (CString &fullCode, CRscParamBase *dst, bool inFavourites, CString *outCode);

  //! Insert code (e.g. from clipboard).
	bool InsertParam (CString &fullCode, bool isClass, CRscParamBase *&outParam);

  //! Move/copy parameter.
	bool MoveCopyParam (CRscParamBase *src, CRscClass *in, CRscParamBase *where, CString &fullCode, 
		bool after, bool move, bool isClass, CRscParamBase *&outParam);

  //! Copy control item.
	bool CopyItem (const char *name, CString &fullCode, double x, double y);

  //! Set new monitor class.
	bool SetMonitorClass (CRscClass *cl);

  //! Add class among favourites.
	void AddFavourite (CRscClass *cl, CRscClass *prev);

  //! Remove class from favourite list.
	bool RemoveFavourite (CRscClass *cl);

  //! Create new copy of control item (in monitor class).
	CRscClass *NewItemCopy (CRscClass *cl, CRscClass *monitor, const char *name);

  //! Make the attributes of the class independent on the base class.
	void MakeClassIndependent (CRscClass *cl);

  //! Clear undo/redo system.
	void ClearHistory ();

  //! Include file into this document.
	bool IncludeFile (CString &fileName);

  //! Move parameter in favourite list.
	void MoveFavouriteParam (CRscClass *move, CRscClass *where, bool after);

  //! Bring control item forward.
	void BringForward (CRscClass *cl);

  //! Bring control item to front.
	void BringToFront (CRscClass *cl);

  //! Send control item backward.
	void SendBackward (CRscClass *cl);

  //! Send control item to back.
	void SendToBack (CRscClass *cl);

  //! Create new class.
	CRscClass *NewClass (LPCTSTR name, bool readOnly, LPCTSTR parentFullName, bool &inFavourites, int &favouriteIndex, CRscInclude *inInclude, CRscMacroCall *inMacroCall);

  //! Fill 'grs' with groups owned by the 'owner' (their monitor class). Returns true if 'grs' not empty.
	bool GetGroupsOfClass (CRscClass *owner, CArray<CRscGroup*, CRscGroup*> &grs);

  /*! Returns the selection code of 'cls' (e.g. SELECTED_NONE_ITEM, SELECTED_ONE_ITEM, SELECTED_PART_GROUPS, ...) and
      'agArr' will contain groups of classes in 'cls' and 'clArr' will contain classes which doesn't belong to any group.
  */
	int GetGroups (CArray<CRscClass*, CRscClass*> &cls, CArray<CRscGroup*, CRscGroup*> *grArr, CArray<CRscClass*, CRscClass*> *clArr);

  //! Fill 'arr' with groups of selected items. Returns the selection code.
	int GetSelectedGroups (CArray<CRscGroup*, CRscGroup*> *arr);

  //! Group selected classes.
	void Group ();

  //! Ungroup group.
	void Ungroup (CRscGroup *gr);

  //! Ungroup all groups owned by 'owner'.
	void Ungroup (CRscClass *owner);

  //! Ungroup these groups and group selected items.
	void Regroup (CArray<CRscGroup*, CRscGroup*> &grs);

  //! Select class.
	void SelectClass (CRscClass *cl, bool flag);

  //! Select group.
	void SelectGroup (CRscGroup *gr, bool flag);

  //! Select/deselect all classes.
	void SelectAllClasses (bool flag);

  //! Create references to the coordinates of the selected control items or selected groups.
	int CreateLayoutUnits (CArray<CRscLayoutUnit*, CRscLayoutUnit*> *lus, CArray<CRscClass*, CRscClass*> &sel);

  //! Destroy references to the coordinates of control items or groups of them.
	void DisposeLayoutUnits (CArray<CRscLayoutUnit*, CRscLayoutUnit*> &lus);

  //! Add guide value into (monitor) class 'cl'.
	CRscGuide *AddGuide (CRscClass *cl, int type, double f, int *index);

  //! Get guide of class 'cl'. You can force the guide to be created.
	CRscGuide *GetGuide (CRscClass *cl, bool create);

  //! Change the guide value.
	void MoveGuide (CRscGuide *guide, int type, int index, double value, bool canUndo);

  //! Remove guide value.
	void RemoveGuide (CRscGuide *guide, int type, int index);

  //! Delete guide.
	void DeleteGuide (CRscGuide *guide);

  //! Delete guide of the appropriate class.
	void DeleteGuide (CRscClass *owner);

  //! Find the nearest vertical guide value.
	bool FindGuidedVertValue (double y, double &d, double *gv, int *gvp, RECT *view);

  //! Find the nearest horizontal guide value.
	bool FindGuidedHorzValue (double x, double &d, double *gv, int *gvp, RECT *view);

  //! Find the nearest vertical snap value.
	bool FindItemSnapVertValue (double y, double &d, double *gv, int *gvp, RECT *view);

  //! Find the nearest horizontal snap value.
	bool FindItemSnapHorzValue (double x, double &d, double *gv, int *gvp, RECT *view);

  //! Apply guides to position coordinates (x,y) in 'arr'.
	void ApplyGuidesToPosition (CArray<CRscLayoutUnit*, CRscLayoutUnit*> &arr, bool inBases, RECT *view);

  //! Apply guides to right coordinates in 'arr'.
	void ApplyGuidesToRight (CArray<CRscLayoutUnit*, CRscLayoutUnit*> &arr, bool inBases, RECT *view);

  //! Apply guides to bottom coordinates in 'arr'.
	void ApplyGuidesToBottom (CArray<CRscLayoutUnit*, CRscLayoutUnit*> &arr, bool inBases, RECT *view);

  //! Apply guides to left coordinates in 'arr'.
	void ApplyGuidesToLeft (CArray<CRscLayoutUnit*, CRscLayoutUnit*> &arr, bool inBases, RECT *view);

  //! Apply guides to top coordinates in 'arr'.
	void ApplyGuidesToTop (CArray<CRscLayoutUnit*, CRscLayoutUnit*> &arr, bool inBases, RECT *view);

  //! Apply snap to position coordinates (x,y) in 'arr'.
	void ApplyItemSnapToPosition (CArray<CRscLayoutUnit*, CRscLayoutUnit*> &arr, bool inBases, RECT *view);

  //! Set delta to the reference point in all members of the array.
	void SetReferenceDelta (CArray<CRscLayoutUnit*, CRscLayoutUnit*> &arr, const CPoint &pt, const RECT *view);

  //! Sort classes in 'arr' in order from back to front.
	bool SortFromBackToFront (CArray<CRscClass*, CRscClass*> &arr);

  //! Compare position in two CRscItemPosition objects. Used in ::qsort calling from SortFromBackToFront method.
	static int __cdecl CompareFromBackToFront (const void *elem1, const void *elem2);

public:
  /*! Function that iterates through all selected items/groups and call compare and set function.
      Items/groups can be also sort before processing be 'sort' function (e.g. from left to right, top to bottom, ...).
  */
	void OnLayoutGeneric (double (&n)[LAYOUT_PARAMS_SIZE], bool changeBases,
				void CompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h), 
				void SetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h),
				int (__cdecl *sort) (const void *elem1, const void *elem2));

  //! Compare function for alligning to the right.
	static void AlignRightCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function for alligning to the right.
	static void AlignRightSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function for alligning to the bottom.
	static void AlignBottomCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function for alligning to the bottom.
	static void AlignBottomSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function for alligning to the left.
	static void AlignLeftCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function for alligning to the left.
	static void AlignLeftSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function for alligning to the top.
	static void AlignTopCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function for alligning to the top.
	static void AlignTopSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function for alligning to the horizontal center.
	static void AlignHorizCenterCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function for alligning to the horizontal center.
	static void AlignHorizCenterSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function for alligning to the vertical center.
	static void AlignVertCenterCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function for alligning to the vertical center.
	static void AlignVertCenterSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function for alligning side by side vertically.
	static void SideBySideTopCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function for alligning side by side vertically.
	static void SideBySideTopSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function for alligning side by side horizontally.
	static void SideBySideLeftCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function for alligning side by side horizontally.
	static void SideBySideLeftSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function for alligning side by side vertically.
	static void SideBySideBottomCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function for alligning side by side vertically.
	static void SideBySideBottomSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function for alligning side by side horizontally.
	static void SideBySideRightCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function for alligning side by side horizontally.
	static void SideBySideRightSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function for making the same width.
  static void MakeSameSizeWidthCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function for making the same width.
	static void MakeSameSizeWidthSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function for making the same height.
	static void MakeSameSizeHeightCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function for making the same height.
	static void MakeSameSizeHeightSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function for making the same width and height.
	static void MakeSameSizeBothCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function for making the same width and height.
	static void MakeSameSizeBothSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function for horizontal centering in dialog.
	static void CenterInDialogHorizCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function for horizontal centering in dialog.
	static void CenterInDialogHorizSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function for vertical centering in dialog.
	static void CenterInDialogVerticalCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function for vertical centering in dialog.
	static void CenterInDialogVerticalSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function to position items space evenly across dialog.
	static void SpaceEvenlyAcrossDialogCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function to position items space evenly across dialog.
	static void SpaceEvenlyAcrossDialogSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function to position items space evenly across selection.
	static void SpaceEvenlyAcrossSelectionCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function to position items space evenly across selection.
	static void SpaceEvenlyAcrossSelectionSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function to position items space evenly downward dialog.
	static void SpaceEvenlyDownDialogCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function to position items space evenly downward dialog.
	static void SpaceEvenlyDownDialogSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function to position items space evenly downward selection.
	static void SpaceEvenlyDownSelectionCompFce (double (&n)[LAYOUT_PARAMS_SIZE], double x, double y, double w, double h);

  //! Set function to position items space evenly downward selection.
	static void SpaceEvenlyDownSelectionSetFce (double (&n)[LAYOUT_PARAMS_SIZE], double &x, double &y, double &w, double &h);

  //! Compare function for sorting items from left to right.
	static int __cdecl SortLeft2Right (const void *elem1, const void *elem2);

  //! Compare function for sorting items from top to down.
	static int __cdecl SortTop2Down (const void *elem1, const void *elem2);

  //! Compare function for sorting items from right to left.
	static int __cdecl SortRight2Left (const void *elem1, const void *elem2);

  //! Compare function for sorting items from down to top.
	static int __cdecl SortDown2Top (const void *elem1, const void *elem2);

private:
  //! Private method for finding containing recursive calling.
	CRscParamBase* FindNextForwardSelf (const CArray<CRscParamBase*, CRscParamBase*> &arr, const CString &name, CRscParamBase *&last, DWORD flags) const;

  //! Private method for finding containing recursive calling.
	CRscParamBase* FindNextBackwardSelf (const CArray<CRscParamBase*, CRscParamBase*> &arr, const CString &name, CRscParamBase *&last, DWORD flags) const;

	//{{AFX_VIRTUAL(CRscEditorDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual void DeleteContents();
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	virtual void SetPathName(LPCTSTR lpszPathName, BOOL bAddToMRU = TRUE);
	//}}AFX_VIRTUAL

public:
	virtual ~CRscEditorDoc();

	BOOL IsSaved ();

	void SetSaved (BOOL isSaved, BOOL isModified);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CRscEditorDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RSCEDITORDOC_H__3B472C5A_14BB_46D0_B52C_491294476912__INCLUDED_)
