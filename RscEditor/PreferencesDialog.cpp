
#include "stdafx.h"
#include "rsceditor.h"
#include "PreferencesDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CPreferencesDialog::CPreferencesDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CPreferencesDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPreferencesDialog)
	//}}AFX_DATA_INIT
}


void CPreferencesDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPreferencesDialog)
	DDX_Control(pDX, IDC_UNWRAP_INCLUDED_MACROS, _unwrapIncludedMacros);
	DDX_Control(pDX, IDC_SHOWREMOVECOMMENTBOX, _showRemoveCommentBox);
	DDX_Control(pDX, IDC_DOCUMENT_TABULATOR, _tabulator);
	DDX_Control(pDX, IDC_CONTROL_CLASSES, _controlClasses);
	DDX_Control(pDX, IDC_ARRAY_TEXT_WRAP_LIMIT, _arrayTextWrapLimit);
	DDX_Control(pDX, IDC_EXTERNAL_VIEWER, _externalViewer);
	DDX_Control(pDX, IDC_EXTERNAL_EDITOR, _externalEditor);
	DDX_Control(pDX, IDC_GUIDE_UNIT, _guideUnit);
	DDX_Control(pDX, IDC_GUIDE_SNAP, _guideSnap);
	DDX_Control(pDX, IDC_GRID_VERT_UNIT, _gridVertUnit);
	DDX_Control(pDX, IDC_GRID_HORZ_UNIT, _gridHorzUnit);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPreferencesDialog, CDialog)
	//{{AFX_MSG_MAP(CPreferencesDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CPreferencesDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CString str;
	double unit = 0.1;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GRID_HORZ_UNIT, unit);
	str.Format (_T ("%g"), unit);
	_gridHorzUnit.SetWindowText (str);

	unit = 0.1;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GRID_VERT_UNIT, unit);
	str.Format (_T ("%g"), unit);
	_gridVertUnit.SetWindowText (str);

	unit = 0.05;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDE_UNIT, unit);
	str.Format (_T ("%g"), unit);
	_guideUnit.SetWindowText (str);

	int snap = 3;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDE_SNAP, snap);
	str.Format (_T ("%d"), snap);
	_guideSnap.SetWindowText (str);

	str = _T ("");
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::EDITOR_EXTERN, str);
	_externalEditor.SetWindowText (str);

	str = _T ("Operation Flashpoint");
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::BULDOZER_WINDOW_CLASS, str);
	_externalViewer.SetWindowText (str);

	str = _T ("\t");
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::TABULATOR, str);
	str.Replace (_T ("\t"), _T ("\\t"));
	_tabulator.SetWindowText (str);

	str = _T ("80");
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::ARRAY_TEXT_WRAP_LIMIT, str);
	_arrayTextWrapLimit.SetWindowText (str);

	bool b = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::UNWRAP_INCLUDED_MACROS, b);
	_unwrapIncludedMacros.SetCheck (b ? 1 : 0);

	b = true;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_REMOVE_COMMENT_BOX, b);
	_showRemoveCommentBox.SetCheck (b ? 1 : 0);

	b = true;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::CONTROL_CLASSES, b);
	_controlClasses.SetCheck (b ? 1 : 0);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CPreferencesDialog::OnOK() 
{
	bool ok = true;
	CString str;
	_gridHorzUnit.GetWindowText (str);
	TCHAR *end;
	LPCTSTR start = (LPCTSTR) str;
	double unit = _tcstod (start, &end);
	if (start == end)
	{
		ok = false;
		MessageBox (_T ("Grid horizontal unit must be float number."), _T ("Error"), MB_OK);
	}
	else
	{
		((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::GRID_HORZ_UNIT, unit, false);
	}

	_gridVertUnit.GetWindowText (str);
	start = (LPCTSTR) str;
	unit = _tcstod (start, &end);
	if (start == end)
	{
		ok = false;
		MessageBox (_T ("Grid vertical unit must be float number."), _T ("Error"), MB_OK);
	}
	else
	{
		((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::GRID_VERT_UNIT, unit, false);
	}

	_guideUnit.GetWindowText (str);
	start = (LPCTSTR) str;
	unit = _tcstod (start, &end);
	if (start == end)
	{
		ok = false;
		MessageBox (_T ("Guide unit must be float number."), _T ("Error"), MB_OK);
	}
	else
	{
		((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::GUIDE_UNIT, unit, false);
	}

	_guideSnap.GetWindowText (str);
	start = (LPCTSTR) str;
	int snap = (int) _tcstol (start, &end, 0);
	if (start == end || snap < 0)
	{
		ok = false;
		MessageBox (_T ("Guide snap value must be unsigned integer number."), _T ("Error"), MB_OK);
	}
	else
	{
		((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::GUIDE_SNAP, snap, false);
	}

	_externalEditor.GetWindowText (str);
	CFileStatus status;
	if (str.GetLength () > 0 && (!CFile::GetStatus (str, status) || (status.m_attribute & CFile::directory) != 0))
	{
		ok = false;
		MessageBox (_T ("External editor value must be path to a file or an empty string."), _T ("Error"), MB_OK);
	}
	else
	{
		((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::EDITOR_EXTERN, str, false);
	}

	_externalViewer.GetWindowText (str);
	((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::BULDOZER_WINDOW_CLASS, str, false);

	_tabulator.GetWindowText (str);
	str.Replace (_T ("\\t"), _T ("\t"));
	((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::TABULATOR, str, false);

	_arrayTextWrapLimit.GetWindowText (str);
	start = (LPCTSTR) str;
	snap = (int) _tcstol (start, &end, 0);
	if (start == end || snap < 0)
	{
		ok = false;
		MessageBox (_T ("Array text wrap limit must be unsigned integer number."), _T ("Error"), MB_OK);
	}
	else
	{
		((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::ARRAY_TEXT_WRAP_LIMIT, snap, false);
	}

	bool b = _unwrapIncludedMacros.GetCheck () != 0;
	((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::UNWRAP_INCLUDED_MACROS, b, false);

	b = _showRemoveCommentBox.GetCheck () != 0;
	((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::SHOW_REMOVE_COMMENT_BOX, b, false);

	b = _controlClasses.GetCheck () != 0;
	((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::CONTROL_CLASSES, b, false);

	((CRscEditorApp*) AfxGetApp ())->_preferences.Save ();
	if (ok)
	{
		CDialog::OnOK();
	}
}
