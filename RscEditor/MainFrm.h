
#if !defined(AFX_MAINFRM_H__6753636E_8916_40B4_A371_A3BCE30C9DC8__INCLUDED_)
#define AFX_MAINFRM_H__6753636E_8916_40B4_A371_A3BCE30C9DC8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CRscEditorView;
class CRscEditorPreview;

//! Main frame of the application.
class CMainFrame : public CFrameWnd
{
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

protected:
  //! Splitter of this frame.
	CSplitterWnd m_wndSplitter;

  //! Status bar of this frame.
	CStatusBar   m_wndStatusBar;

  //! Standard toolbar.
	CToolBar     m_wndToolBar;

  //! Layout toolbar.
	CToolBar     m_wndLayoutBar;

public:
  //! Pointer to the editor view = tree view of the classes and attributes.
	CRscEditorView			*_editorView;

  //! Pointer to the preview view.
	CRscEditorPreview		*_editorPreview;

	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	//}}AFX_VIRTUAL

public:
	virtual ~CMainFrame();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnViewLayoutbar();
	afx_msg void OnUpdateViewLayoutbar(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__6753636E_8916_40B4_A371_A3BCE30C9DC8__INCLUDED_)
