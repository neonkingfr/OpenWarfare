
#if !defined(AFX_RSCEDITOR_H__7DF000DB_ED56_474C_B603_A1262D228FDE__INCLUDED_)
#define AFX_RSCEDITOR_H__7DF000DB_ED56_474C_B603_A1262D228FDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "Preferences.h"

//! User message to update control. (Used during controling by key UP and DOWN.)
#define WM_USER_UPDATECURSOR (WM_USER + 1)

//! Space between two UI controls.
#define UI_SPACE 7

//! Scroll area size while dragging.
#define SCROLL_AREA 10

//! Scroll delay time while dragging (in milliseconds).
#define SCROLL_TIME 50

//! Class of temporary file.
class CRscTempFile
{
protected:
  //! Filename.
	TCHAR _fileName[MAX_PATH];

  //! Unique numeric value used in the filename.
	UINT _unique;

  //! File handle of this file.
	HANDLE _fileHandle;

public:
	CRscTempFile ()
		: _unique (0), _fileHandle (INVALID_HANDLE_VALUE)
	{
	}

	CRscTempFile (const CRscTempFile &src)
	{
		operator = (src);
	}

	~CRscTempFile ()
	{
	}

	LPCTSTR GetPathName () const
	{
		return _fileName;
	}

	void operator = (const CRscTempFile &src)
	{
		_tcscpy (_fileName, src._fileName);
		_unique = src._unique;
		_fileHandle = src._fileHandle;
	}

	bool Open ()
	{
		_unique = GetTempFileName (_T ("."), _T ("t_r"), 0, _fileName);
		if (_unique == 0)
		{
			return false;
		}
		_fileHandle = CreateFile (_fileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY, NULL);
		if (_fileHandle == INVALID_HANDLE_VALUE)
		{
			return false;
		}
		LPSTR filePart;
		DWORD err = GetFullPathName (_fileName, sizeof (_fileName) / sizeof (TCHAR), _fileName, &filePart);
		if (err == 0 || err > sizeof (_fileName) / sizeof (TCHAR))
		{
			CloseHandle (_fileHandle);
			_fileHandle = INVALID_HANDLE_VALUE;
			return false;
		}
		return true;
	}

	bool Write (CString &str)
	{
		if (_fileHandle == INVALID_HANDLE_VALUE)
		{
			return false;
		}

		DWORD written;
		DWORD size = str.GetLength () * sizeof (TCHAR);
		if (!WriteFile (_fileHandle, (LPCTSTR) str, size, &written, NULL))
		{
			return false;
		}

		return size == written;
	}

	void Close ()
	{
		if (_fileHandle != INVALID_HANDLE_VALUE)
		{
			CloseHandle (_fileHandle);
			_fileHandle = INVALID_HANDLE_VALUE;
		}
	}

	void Delete ()
	{
		Close ();
		if (_unique != 0)
		{
			DeleteFile (_fileName);
			_unique = 0;
		}
	}
};

//! Application class.
class CRscEditorApp : public CWinApp
{
public:
  //! Instance of application's preferences store.
	CPreferences		_preferences;

  //! Array of temporary files, will be deleted at application's exit.
	CArray<CRscTempFile, const CRscTempFile&> _tempFiles;

  //! Flags for 'find' command.
	DWORD				  _lastFlags;

  //! Arrow cursor.
	HCURSOR				_cursorArrow;

  //! Hand cursor.
	HCURSOR				_cursorHand;

  //! Horizontal arrow cursor.
	HCURSOR				_cursorHorzArrow;

  //! Vertical arrow cursor.
	HCURSOR				_cursorVertArrow;

  //! Left-top right-bottom arrow cursor.
	HCURSOR				_cursorLeftTopArrow;

  //! Right-top left-bottom arrow cursor.
	HCURSOR				_cursorRightTopArrow;

  //! Eraser mouse cursor.
	HCURSOR				_cursorEraser;

	CRscEditorApp();

	//{{AFX_VIRTUAL(CRscEditorApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CRscEditorApp)
	afx_msg void OnAppAbout();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RSCEDITOR_H__7DF000DB_ED56_474C_B603_A1262D228FDE__INCLUDED_)
