// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__E00BBEAC_A856_4AF4_BA73_F74B913A7B39__INCLUDED_)
#define AFX_STDAFX_H__E00BBEAC_A856_4AF4_BA73_F74B913A7B39__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxole.h>
#include <afxtempl.h>
#include <Mmsystem.h>
#include <Math.h>

#if _MSC_VER>=1300
	#pragma conform(forScope,on)
	#pragma warning(disable:4258)
#else
	#define for if( false ) {} else for
#endif

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__E00BBEAC_A856_4AF4_BA73_F74B913A7B39__INCLUDED_)
