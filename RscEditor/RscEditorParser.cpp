
#include "StdAfx.h"

#include "RscEditorItems.h"
#include "RscMacro.h"
#include "RscEditorDoc.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"

/*! Parse "//" comments (callback from lexical analyzer).
    Standard implementation - skip comment to the end of the line.
*/
bool CRscEditorParser::ParseSlashSlash (CRscEditorLex *lex)
{
	CRscEditorToken tok;

	lex->GetNextToken (&tok, NULL, 0);
	
	while (tok.type != rscEditorLexEof)
	{
		if (tok.type == rscEditorLexError)
		{
			return false;
		}
		if (tok.type == '\n')
		{
			return true;
		}
		lex->GetNextToken (&tok, NULL, 0);
	}

	return true;
}

/*! Parse "/*" comments (callback from lexical analyzer).
    Standard implementation.
*/
bool CRscEditorParser::ParseSlashStar (CRscEditorLex *lex)
{
	CRscEditorToken tok;

	lex->GetNextToken (&tok, NULL, 0);
	
	while (tok.type != rscEditorLexEof)
	{
		if (tok.type == rscEditorLexError)
		{
			return false;
		}
		if (tok.type == rscEditorLex_starSlash)
		{
			return true;
		}
		lex->GetNextToken (&tok, NULL, 0);
	}

	return false;
}

/*! Parse "#" command (callback from lexical analyzer).
    Default implementation - no hash command is supported.
*/
bool CRscEditorParser::ParseHash (CRscEditorLex *lex, CRscEditorToken *tok, unsigned short flags)
{
	return true;
}

/*! Parse macro (callback from lexical analyzer).
    Default implementation - no macros are supported.
*/
bool CRscEditorParser::ProcessMacroId (CRscEditorLex *lex, CRscEditorToken *tok, unsigned short flags)
{
	return true;
}