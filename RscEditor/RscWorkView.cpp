
#include "stdafx.h"
#include "RscEditor.h"

#include "RscEditorItems.h"
#include "RscWorkView.h"
#include "RscMacro.h"
#include "RscEditorDoc.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"
#include "RscEditorREDParser.h"
#include "RscEditorView.h"
#include "RscEditorPreview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//! Code of the action performed by mouse dragging.
enum
{
  //! Nothing.
	DRAG_NONE = 0,

  //! Control item(s) moving.
	DRAG_MOVE,

  //! Control item(s) resizing.
	DRAG_RESIZE,

  //! Control item(s) selecting.
	DRAG_SELECT,

  //! Horizontal guide value moving.
	DRAG_HORZ_GUIDE,

  //! Vertical guide value moving.
	DRAG_VERT_GUIDE,
};

//! Control item iterator finding item at given point.
class CRscClassFinder : public CRscClassEnumerator
{
public:
  //! Point where I'm looking for some control item.
	CPoint			_point;

  //! Preview pane rectangle.
	RECT			*_view;

  //! Class of the control item found at the given point.
	CRscClass		*_foundClass;

  //! Area of the found control item (the smallest one is the right one).
	unsigned int	_size;

  //! Precalculated value - preview pane width.
	double			_dx;

  //! Precalculated value - preview pane height.
	double			_dy;

public:
	CRscClassFinder (CRscEditorDoc *doc, CPoint pt, RECT *view, bool *tags)
		: CRscClassEnumerator (doc, true, tags, false), _point (pt), _view (view), _foundClass (NULL), 
		_size (UINT_MAX), _dx (view->right - view->left - 1), 
		_dy (view->bottom - view->top - 1)
	{
	}

  //! Main method testing if current control item is under the point and is smaller than the last one.
	virtual bool EnumerateFce (CRscClass *cl, CRscAttributeValueElement *val)
	{
		CRect r;
		CALC_CTRL_RECT (r, _view->left, _view->top, _dx, _dy, _x, _y, _w, _h)
		if (r.PtInRect (_point))
		{
			unsigned int ps = (r.right - r.left) * (r.bottom - r.top);
			if (ps < _size || ps == _size && cl->_selected)
			{
				_size = ps;
				_foundClass = cl;
			}
		}
		return false;
	}
};

//! Control item iterator finding items inside given rectangle.
class CRscClassesInRect : public CRscClassEnumerator
{
public:
  //! Rectangle where I'm looking for some control item(s).
	CRect			&_rect;

  //! Preview pane rectangle.
	RECT			*_view;

  //! Class(es) of the control item(s) found inside the given rectangle.
	CArray<CRscClass*, CRscClass*>	&_result;

  //! Precalculated value - preview pane width.
	double			_dx;

  //! Precalculated value - preview pane height.
	double			_dy;

public:
	CRscClassesInRect (CRscEditorDoc *doc, CRect &r, RECT *view, bool *tags, CArray<CRscClass*, CRscClass*> &result)
		: CRscClassEnumerator (doc, true, tags, false), _rect (r), _view (view), _result (result), 
		_dx (view->right - view->left - 1), 
		_dy (view->bottom - view->top - 1)
	{
		_rect.NormalizeRect ();
	}

	virtual bool EnumerateFce (CRscClass *cl, CRscAttributeValueElement *val)
	{
		CRect r;
		CALC_CTRL_RECT (r, _view->left, _view->top, _dx, _dy, _x, _y, _w, _h)
		if (r.IntersectRect (r, _rect))
		{
			_result.Add (cl);
		}
		return false;
	}
};

//! Control item iterator finding drag box of any control item at given coordinates.
class CRscDragBoxFinder : public CRscClassEnumerator
{
public:
  //! Point where I am looking for the control item drag box.
	CPoint			_point;

  //! Preview pane rectangle.
	RECT			*_view;

  //! Class of the control item of the drag box found at the given point.
	CRscClass		*_foundClass;

  //! Precalculated value - preview pane width.
	double			_dx;

  //! Precalculated value - preview pane height.
	double			_dy;

  //! Code of the drag box (left, top, right, bottom-left, etc.) found at given coordinates.
	int				_dragBox;

public:
	CRscDragBoxFinder (CRscEditorDoc *doc, CPoint pt, RECT *view, bool *tags)
		: CRscClassEnumerator (doc, true, tags, false), _point (pt), _view (view), _foundClass (NULL), 
		_dx (view->right - view->left - 1), _dy (view->bottom - view->top - 1),
		_dragBox (rscEditorDragBoxNone)
	{
	}

	virtual bool EnumerateFce (CRscClass *cl, CRscAttributeValueElement *val)
	{
		RECT r;
		CALC_CTRL_RECT (r, _view->left, _view->top, _dx, _dy, _x, _y, _w, _h)

		RECT rr;
		CRect rf;

#define TEST_DRAG_BOX(x, y, dragBoxCode)			\
		DRAG_BOX ((x), (y), (_view), rr, rf)\
		if (rf.PtInRect (_point))\
		{\
			_dragBox = dragBoxCode;\
			_foundClass = cl;\
			return true;\
		}

		--r.right;
		--r.bottom;
		TEST_DRAG_BOX (r.left, r.top, rscEditorDragBoxTopLeft)
		TEST_DRAG_BOX (r.right, r.top, rscEditorDragBoxTopRight)
		TEST_DRAG_BOX (r.left, r.bottom, rscEditorDragBoxBottomLeft)
		TEST_DRAG_BOX (r.right, r.bottom, rscEditorDragBoxBottomRight)
		TEST_DRAG_BOX ((r.left + r.right) >> 1, r.top, rscEditorDragBoxTop)
		TEST_DRAG_BOX ((r.left + r.right) >> 1, r.bottom, rscEditorDragBoxBottom)
		TEST_DRAG_BOX (r.left, (r.top + r.bottom) >> 1, rscEditorDragBoxLeft)
		TEST_DRAG_BOX (r.right, (r.top + r.bottom) >> 1, rscEditorDragBoxRight)

#undef TEST_DRAG_BOX

		return false;
	}
};

CRscWorkView::CRscWorkView()
	: _preview (NULL), _rButtonDown (false), _pressedDragBox (rscEditorDragBoxNone), 
	_lastDropEffect (DROPEFFECT_NONE), _commandOpened (false), _offBmp (NULL), 
	_dragState (DRAG_NONE), _dragRect (0, 0, 0, 0), _highlightedGuideIndex (-1)
{
}

CRscWorkView::~CRscWorkView()
{
	if (_offBmp != NULL)
	{
		delete _offBmp;
		_offBmp = NULL;
	}

	int n = _dragLayoutUnits.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		delete _dragLayoutUnits[i];
	}
	_dragLayoutUnits.RemoveAll ();
}


BEGIN_MESSAGE_MAP(CRscWorkView, CWnd)
	//{{AFX_MSG_MAP(CRscWorkView)
	ON_WM_PAINT()
	ON_WM_RBUTTONUP()
	ON_WM_RBUTTONDOWN()
	ON_WM_CONTEXTMENU()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_DESTROY()
	ON_WM_SETCURSOR()
	ON_MESSAGE (WM_USER_UPDATECURSOR, OnUserUpdateCursor)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CRscWorkView::OnPaint() 
{
	CPaintDC dc(this); // device context for painting

	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();

	RECT r1;
	GetClientRect (&r1);
	
	CDC offdc;
	if (!offdc.CreateCompatibleDC (&dc))
	{
		return;
	}
	if (_offBmp == NULL || _offBmpWidth != r1.right || _offBmpHeight != r1.bottom)
	{
		if (_offBmp != NULL)
		{
			delete _offBmp;
			_offBmp = NULL;
		}
		_offBmp = new CBitmap ();
		if (_offBmp == NULL)
		{
			return;
		}
		_offBmp->CreateCompatibleBitmap (&dc, r1.right, r1.bottom);
		_offBmpWidth = r1.right;
		_offBmpHeight = r1.bottom;
	}

	CBitmap *oldBmp = offdc.SelectObject (_offBmp);

	// Do not call CStatic::OnPaint() for painting messages
	if (_preview == NULL)
	{
		return;
	}

	offdc.FillSolidRect (&r1, RGB (255, 255, 255));
	offdc.DrawEdge (&r1, EDGE_SUNKEN, BF_RECT);
	
	if (doc->_monitorClass != NULL)
	{
		RECT h, v;
		CRect view;
		CalcPreviewRect (view, r1);
		CalcHorzGuideRect (h, r1);
		CalcVertGuideRect (v, r1);
		CPen *oldPen = offdc.GetCurrentPen ();
		CBrush *oldBrush = offdc.GetCurrentBrush ();

		int n, i;
		bool show = false;
		((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_GRID, show);
		if (show)
		{
			CPen pen (PS_SOLID, 1, RGB (224, 224, 224));
			offdc.SelectObject (&pen);		
			offdc.SelectStockObject (NULL_BRUSH);
			double grid = 0.1;
			((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GRID_HORZ_UNIT, grid);
			double step = grid;
			while (grid < 1.0)
			{
				CALC_GUIDE_POINT (n, view.left, view.right - view.left - 1, grid);
				offdc.MoveTo (n, view.top);
				offdc.LineTo (n, view.bottom - 1);
				grid += step;
			}
			
			grid = 0.1;
			((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GRID_VERT_UNIT, grid);
			step = grid;
			while (grid < 1.0)
			{
				CALC_GUIDE_POINT (n, view.top, view.bottom - view.top - 1, grid);
				offdc.MoveTo (view.left, n);
				offdc.LineTo (view.right - 1, n);
				grid += step;
			}
		}

		show = false;
		((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_GUIDES, show);
		if (show)
		{
			offdc.SelectStockObject (BLACK_PEN);		
			offdc.SelectStockObject (NULL_BRUSH);
			offdc.Rectangle (&h);
			offdc.Rectangle (&v);
			n = doc->_guides.GetSize ();
			for (i = 0; i < n; ++i)
			{
				if (doc->_guides[i]->_owner == doc->_monitorClass)
				{
					doc->_guides[i]->Draw (offdc, &h, &v, &view);
				}
			}
		}

		offdc.IntersectClipRect (&view);
		CRscClassPainter painter (doc, &view, doc->_displayedClasses, 
			&_preview->_itemGroupTags[0], _preview->_itemGroup);
		painter.EnumerateClass (doc->_monitorClass, NULL);
		n = doc->_selectedClasses.GetSize ();
		for (i = 0; i < n; )
		{
			if (!doc->_selectedClasses[i]->_displayed)
			{
				// this should never happen
				ASSERT (FALSE);
				doc->RemoveFromGroup (doc->_selectedClasses[i]);
				doc->_selectedClasses[i]->_selected = false;
				doc->_selectedClasses.RemoveAt (i);
				--n;
			}
			else
			{
				++i;
			}
		}
		painter.Draw (&offdc, doc->_selectedClasses.GetSize () == 1);
		CArray<CRscGroup*, CRscGroup*> grs;
		bool drawDragBoxes = doc->GetGroups (doc->_selectedClasses, &grs, NULL) == SELECTED_ONE_GROUP;
		n = grs.GetSize ();
		for (i = 0; i < n; ++i)
		{
			grs[i]->Draw (offdc, &view, drawDragBoxes);
		}
		if (!_dragRect.IsRectEmpty ())
		{
			view.IntersectRect (view, _dragRect);
			offdc.DrawDragRect (&view, CSize (1, 1), NULL, CSize (0, 0), NULL, NULL);
		}

		offdc.SelectObject (oldBrush);
		offdc.SelectObject (oldPen);
	}
	dc.BitBlt (r1.left, r1.top, r1.right - r1.left, r1.bottom - r1.top, &offdc,
		r1.left, r1.top, SRCCOPY);
	offdc.SelectObject (oldBmp);
	ValidateRect (&r1);
}

void CRscWorkView::OnRButtonUp(UINT nFlags, CPoint point) 
{
	_rButtonDown = false;

	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	if (doc->_monitorClass != NULL)
	{
		_pressedDragBox = rscEditorDragBoxNone;
		SelectClass (point, doc->_monitorClass, false);
		_preview->GetView ()->SelectItemClass ();
		_preview->GetView ()->SelectItemGroup ();
		Invalidate (FALSE);
		UpdateWindow ();
	}
	
	ClientToScreen (&point);
	_preview->GetView ()->DisplayContexMenu (3, point);
}

void CRscWorkView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	_rButtonDown = true;
	SetFocus ();
}

void CRscWorkView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	if (!_rButtonDown)
	{
		CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
		if (doc->_selectedClasses.GetSize () > 0)
		{
			CRscClass *cl = doc->_selectedClasses[0];
			double x, y, w, h;
			if (cl->GetFloatParamInBases ("x", x, true, NULL, false) &&
				cl->GetFloatParamInBases ("y", y, true, NULL, false) &&
				cl->GetFloatParamInBases ("w", w, true, NULL, false) &&
				cl->GetFloatParamInBases ("h", h, true, NULL, false))
			{
				RECT r;
				GetWindowRect (&r);
				point.x = (int) (r.left + (x + w / 2.0) * (r.right - r.left));
				point.y = (int) (r.top + (y + h / 2.0) * (r.bottom - r.top));
				_preview->GetView ()->DisplayContexMenu (3, point);
			}
		}
	}
}

void CRscWorkView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	SetFocus ();
	_dragState = DRAG_NONE;
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	if (doc->_monitorClass != NULL)
	{
		RECT ctrlRect;
		CRect previewRect;
		CRect vertGuideRect;
		CRect horzGuideRect;

		GetClientRect (&ctrlRect);
		CalcPreviewRect (previewRect, ctrlRect);
		CalcHorzGuideRect (horzGuideRect, ctrlRect);
		CalcVertGuideRect (vertGuideRect, ctrlRect);

		doc->DisposeLayoutUnits (_dragLayoutUnits);
		doc->CreateLayoutUnits (&_dragLayoutUnits, doc->_selectedClasses);
		
		if (previewRect.PtInRect (point))
		{
			_pressedDragBox = FindDragBoxAtPt (point, &_pressedDragBoxLU);
			if (_pressedDragBox == rscEditorDragBoxNone)
			{
				if (SelectClass (point, doc->_monitorClass, (nFlags & (MK_CONTROL | MK_SHIFT)) != 0))
				{
					_preview->GetView ()->SelectItemClass ();
					_preview->GetView ()->SelectItemGroup ();
					doc->DisposeLayoutUnits (_dragLayoutUnits);
					doc->CreateLayoutUnits (&_dragLayoutUnits, doc->_selectedClasses);
					doc->SetReferenceDelta (_dragLayoutUnits, point, &previewRect);
					Invalidate (FALSE);
					UpdateWindow ();
					_dragState = DRAG_MOVE;
				}
				else
				{
					_dragState = DRAG_SELECT;
				}
			}
			else
			{
				_dragState = DRAG_RESIZE;
			}
		}
		else if (vertGuideRect.PtInRect (point))
		{
			_dragGuideIndex = -1;
			_dragGuide = doc->GetGuide (doc->_monitorClass, false);
			if (_dragGuide != NULL)
			{
				int mdg = 3;
				((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDE_SNAP, mdg);
				int n = _dragGuide->_vertPxl.GetSize ();
				for (int i = 0; i < n; ++i)
				{
					int td = ::abs (point.y - _dragGuide->_vertPxl[i]);
					if (td <= mdg)
					{
						_dragGuideIndex = i;
						mdg = td;
					}
				}
			}
			if ((nFlags & (MK_CONTROL | MK_SHIFT)) != 0)
			{
				if (_dragGuideIndex != -1)
				{
					if (doc->OpenCommand (_T ("Remove Vert. Guide"), true, UNDO_REMOVE_VERT_GUIDE))
					{
						doc->RemoveGuide (_dragGuide, GUIDE_VERT, _dragGuideIndex);
						doc->CloseCommand ();
						doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
					}
				}
				return;
			}
			else if (_dragGuideIndex == -1)
			{
				_commandOpened = doc->OpenCommand (_T ("Add Vert. Guide"), true, UNDO_ADD_VERT_GUIDE);
				if (_commandOpened)
				{
					double gunit = 0.01;
					((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDE_UNIT, gunit);
					RECT r;
					GetClientRect (&r);
					CalcVertGuideRect (r, r);
					_dragGuideValue = (double) (point.y - r.top) / (double) (r.bottom - r.top - 1);
					_dragGuideValue = (double) ::floor ((_dragGuideValue + gunit / 2.0) / gunit) * gunit;
					LIMIT (_dragGuideValue, 0.0, 1.0)
					_dragGuide = doc->AddGuide (doc->_monitorClass, GUIDE_VERT, _dragGuideValue, &_dragGuideIndex);
					if (_dragGuide == NULL)
					{
						doc->DisposeCommand ();
						_commandOpened = false;
					}
				}
				if (!_commandOpened)
				{
					_dragState = DRAG_NONE;
					return;
				}
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			}
			_dragState = DRAG_VERT_GUIDE;
		}
		else if (horzGuideRect.PtInRect (point))
		{
			_dragGuideIndex = -1;
			_dragGuide = doc->GetGuide (doc->_monitorClass, false);
			if (_dragGuide != NULL)
			{
				int mdg = 3;
				((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDE_SNAP, mdg);
				int n = _dragGuide->_horzPxl.GetSize ();
				for (int i = 0; i < n; ++i)
				{
					int td = ::abs (point.x - _dragGuide->_horzPxl[i]);
					if (td <= mdg)
					{
						_dragGuideIndex = i;
						mdg = td;
					}
				}
			}
			if ((nFlags & (MK_CONTROL | MK_SHIFT)) != 0)
			{
				if (_dragGuideIndex != -1)
				{
					if (doc->OpenCommand (_T ("Remove Horz. Guide"), true, UNDO_REMOVE_HORZ_GUIDE))
					{
						doc->RemoveGuide (_dragGuide, GUIDE_HORZ, _dragGuideIndex);
						doc->CloseCommand ();
						doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
					}
				}
				return;
			}
			else if (_dragGuideIndex == -1)
			{
				_commandOpened = doc->OpenCommand (_T ("Add Horz. Guide"), true, UNDO_ADD_HORZ_GUIDE);
				if (_commandOpened)
				{
					double gunit = 0.01;
					((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDE_UNIT, gunit);
					RECT r;
					GetClientRect (&r);
					CalcHorzGuideRect (r, r);
					_dragGuideValue = (double) (point.x - r.left) / (double) (r.right - r.left - 1);
					_dragGuideValue = ::floor ((_dragGuideValue + gunit / 2.0) / gunit) * gunit;
					LIMIT (_dragGuideValue, 0.0, 1.0)
					_dragGuide = doc->AddGuide (doc->_monitorClass, GUIDE_HORZ, _dragGuideValue, &_dragGuideIndex);
					if (_dragGuide == NULL)
					{
						doc->DisposeCommand ();
						_commandOpened = false;
					}
				}
				if (!_commandOpened)
				{
					_dragState = DRAG_NONE;
					return;
				}
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			}
			_dragState = DRAG_HORZ_GUIDE;
		}

		_dragPoint = point;
		_dragCorner = point;
		SetCapture ();
		
		switch (_dragState)
		{
		case DRAG_SELECT:
			{
				_originallySelectedClasses.Copy (doc->_selectedClasses);
				
				_dragRect.left = _dragCorner.x;
				_dragRect.top = _dragCorner.y;
				_dragRect.right = _dragPoint.x + 1;
				_dragRect.bottom = _dragPoint.y + 1;
				_dragRect.NormalizeRect ();
				Invalidate (FALSE);
				UpdateWindow ();
			}
			break;
		}
	}
}

void CRscWorkView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	switch (_dragState)
	{
	case DRAG_MOVE:
		if (_commandOpened)
		{
			bool inBases = _preview->GetView ()->_changeInherited;
			int n = doc->_selectedClasses.GetSize ();
			for (int i = 0; i < n; ++i)
			{
				CRscClass *cl = doc->_selectedClasses[i];
				double val = 0.0;
				cl->GetFloatParamInBases ("x", val, true, NULL, false);
				doc->SetFloatParamInBases (cl, "x", val, inBases, false, true, true);
				val = 0.0;
				cl->GetFloatParamInBases ("y", val, true, NULL, false);
				doc->SetFloatParamInBases (cl, "y", val, inBases, false, true, true);
			}
			doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			doc->CloseCommand ();
			_commandOpened = false;
		}
		break;

	case DRAG_RESIZE:
		if (_commandOpened)
		{
			bool inBases = _preview->GetView ()->_changeInherited;
			int n = doc->_selectedClasses.GetSize ();
			for (int i = 0; i < n; ++i)
			{
				CRscClass *cl = doc->_selectedClasses[i];
				double val = 0.0;
				cl->GetFloatParamInBases ("x", val, true, NULL, false);
				doc->SetFloatParamInBases (cl, "x", val, inBases, false, true, true);
				val = 0.0;
				cl->GetFloatParamInBases ("y", val, true, NULL, false);
				doc->SetFloatParamInBases (cl, "y", val, inBases, false, true, true);
				val = 0.0;
				cl->GetFloatParamInBases ("w", val, true, NULL, false);
				doc->SetFloatParamInBases (cl, "w", val, inBases, false, true, true);
				val = 0.0;
				cl->GetFloatParamInBases ("h", val, true, NULL, false);
				doc->SetFloatParamInBases (cl, "h", val, inBases, false, true, true);
			}
			doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			doc->CloseCommand ();
			_commandOpened = false;
		}
		break;

	case DRAG_SELECT:
		{
			_dragRect.SetRectEmpty ();
			Invalidate (FALSE);
			UpdateWindow ();
			_preview->GetView ()->SelectItemClass ();
			_preview->GetView ()->SelectItemGroup ();
		}
		break;

	case DRAG_HORZ_GUIDE:
		if (_commandOpened)
		{
			doc->MoveGuide (_dragGuide, GUIDE_HORZ, _dragGuideIndex, _dragGuideValue, true);
			doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			doc->CloseCommand ();
			_commandOpened = false;
		}
		_preview->GetView ()->SetItemLabel (_T (""));
		break;

	case DRAG_VERT_GUIDE:
		if (_commandOpened)
		{
			doc->MoveGuide (_dragGuide, GUIDE_VERT, _dragGuideIndex, _dragGuideValue, true);
			doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			doc->CloseCommand ();
			_commandOpened = false;
		}
		_preview->GetView ()->SetItemLabel (_T (""));
		break;
	}
	_dragState = DRAG_NONE;
	ReleaseCapture ();
}

void CRscWorkView::OnMouseMove(UINT nFlags, CPoint point) 
{
	if (_dragPoint == point)
	{
		return;
	}
	
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	if ((nFlags & MK_LBUTTON) != 0 && doc->_monitorClass != NULL)
	{
		switch (_dragState)
		{
		case DRAG_MOVE:
			DragMove (point);
			break;

		case DRAG_RESIZE:
			DragResize (point);
			break;

		case DRAG_SELECT:
			DragSelect (point, nFlags);
			break;

		case DRAG_VERT_GUIDE:
			{
				double gunit = 0.01;
				((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDE_UNIT, gunit);
				RECT r;
				GetClientRect (&r);
				CalcVertGuideRect (r, r);
				_dragGuideValue = (double) (point.y - r.top) / (double) (r.bottom - r.top);
				_dragGuideValue = ::floor ((_dragGuideValue + gunit / 2.0) / gunit) * gunit;
				LIMIT (_dragGuideValue, 0.0, 1.0)
				if (!_commandOpened && _dragGuideIndex != -1)
				{
					_commandOpened = doc->OpenCommand (_T ("Move Vert. Guide"), true, UNDO_MOVE_VERT_GUIDE);
					if (_commandOpened)
					{
						doc->MoveGuide (_dragGuide, GUIDE_VERT, _dragGuideIndex, _dragGuide->_vert[_dragGuideIndex], true);
					}
					else
					{
						_dragState = DRAG_NONE;
						ReleaseCapture ();
						return;
					}
				}
				doc->MoveGuide (_dragGuide, GUIDE_VERT, _dragGuideIndex, _dragGuideValue, false);
				CString str;
				str.Format (_T ("Vertical Guide: %g"), _dragGuideValue);
				_preview->GetView ()->SetItemLabel (str);
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			}
			break;
			
		case DRAG_HORZ_GUIDE:
			{
				double gunit = 0.01;
				((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDE_UNIT, gunit);
				RECT r;
				GetClientRect (&r);
				CalcHorzGuideRect (r, r);
				_dragGuideValue = (double) (point.x - r.left) / (double) (r.right - r.left);
				_dragGuideValue = ::floor ((_dragGuideValue + gunit / 2.0) / gunit) * gunit;
				LIMIT (_dragGuideValue, 0.0, 1.0)
				if (!_commandOpened && _dragGuideIndex != -1)
				{
					_commandOpened = doc->OpenCommand (_T ("Move Horz. Guide"), true, UNDO_MOVE_HORZ_GUIDE);
					if (_commandOpened)
					{
						doc->MoveGuide (_dragGuide, GUIDE_HORZ, _dragGuideIndex, _dragGuide->_horz[_dragGuideIndex], true);
					}
					else
					{
						_dragState = DRAG_NONE;
						ReleaseCapture ();
						return;
					}
				}
				doc->MoveGuide (_dragGuide, GUIDE_HORZ, _dragGuideIndex, _dragGuideValue, false);
				CString str;
				str.Format (_T ("Horizontal Guide: %g"), _dragGuideValue);
				_preview->GetView ()->SetItemLabel (str);
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			}
			break;
		}
	}

	_dragPoint = point;
}

void CRscWorkView::DragSelect (CPoint &point, UINT nFlags)
{
	_dragRect.left = _dragCorner.x;
	_dragRect.top = _dragCorner.y;
	_dragRect.right = point.x + 1;
	_dragRect.bottom = point.y + 1;
	_dragRect.NormalizeRect ();

	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	SelectClass (_dragRect, doc->_monitorClass, (nFlags & (MK_CONTROL | MK_SHIFT)) != 0);
	Invalidate (FALSE);
	UpdateWindow ();

	_dragPoint = point;
}

void CRscWorkView::DragResize (CPoint &point)
{
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	if (!_commandOpened)
	{
		_commandOpened = doc->OpenCommand (_T ("Resize Item"), true, UNDO_RESIZE_ITEM);
		if (!_commandOpened)
		{
			return;
		}
		bool inBases = _preview->GetView ()->_changeInherited;
		int n = doc->_selectedClasses.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CRscClass *cl = doc->_selectedClasses[i];
			double val = 0.0;
			cl->GetFloatParamInBases ("x", val, true, NULL, false);
			doc->SetFloatParamInBases (cl, "x", val, inBases, false, true, true);
			val = 0.0;
			cl->GetFloatParamInBases ("y", val, true, NULL, false);
			doc->SetFloatParamInBases (cl, "y", val, inBases, false, true, true);
			val = 0.0;
			cl->GetFloatParamInBases ("w", val, true, NULL, false);
			doc->SetFloatParamInBases (cl, "w", val, inBases, false, true, true);
			val = 0.0;
			cl->GetFloatParamInBases ("h", val, true, NULL, false);
			doc->SetFloatParamInBases (cl, "h", val, inBases, false, true, true);
		}
	}
	RECT r;
	GetClientRect (&r);
	CalcPreviewRect (r, r);
	
	LONG ay = point.y - r.top;
	LONG ax = point.x - r.left;
	LONG dx = r.right - r.left - 1;
	LONG dy = r.bottom - r.top - 1;
	
	switch (_pressedDragBox)
	{
	case rscEditorDragBoxTopLeft:
		SetDraggedTopBoxAbs (ay, dy);
		SetDraggedLeftBoxAbs (ax, dx);
		doc->ApplyGuidesToTop (_dragLayoutUnits, 
			_preview->GetView ()->_changeInherited, &r);
		doc->ApplyGuidesToLeft (_dragLayoutUnits, 
			_preview->GetView ()->_changeInherited, &r);
		break;
		
	case rscEditorDragBoxTopRight:
		SetDraggedTopBoxAbs (ay, dy);
		SetDraggedRightBoxAbs (ax, dx);
		doc->ApplyGuidesToTop (_dragLayoutUnits, 
			_preview->GetView ()->_changeInherited, &r);
		doc->ApplyGuidesToRight (_dragLayoutUnits, 
			_preview->GetView ()->_changeInherited, &r);
		break;
		
	case rscEditorDragBoxBottomLeft:
		SetDraggedBottomBoxAbs (ay, dy);
		SetDraggedLeftBoxAbs (ax, dx);
		doc->ApplyGuidesToBottom (_dragLayoutUnits, 
			_preview->GetView ()->_changeInherited, &r);
		doc->ApplyGuidesToLeft (_dragLayoutUnits, 
			_preview->GetView ()->_changeInherited, &r);
		break;
		
	case rscEditorDragBoxBottomRight:
		SetDraggedBottomBoxAbs (ay, dy);
		SetDraggedRightBoxAbs (ax, dx);
		doc->ApplyGuidesToBottom (_dragLayoutUnits, 
			_preview->GetView ()->_changeInherited, &r);
		doc->ApplyGuidesToRight (_dragLayoutUnits, 
			_preview->GetView ()->_changeInherited, &r);
		break;
		
	case rscEditorDragBoxLeft:
		SetDraggedLeftBoxAbs (ax, dx);
		doc->ApplyGuidesToLeft (_dragLayoutUnits, 
			_preview->GetView ()->_changeInherited, &r);
		break;
		
	case rscEditorDragBoxTop:
		SetDraggedTopBoxAbs (ay, dy);
		doc->ApplyGuidesToTop (_dragLayoutUnits, 
			_preview->GetView ()->_changeInherited, &r);
		break;
		
	case rscEditorDragBoxRight:
		SetDraggedRightBoxAbs (ax, dx);
		doc->ApplyGuidesToRight (_dragLayoutUnits, 
			_preview->GetView ()->_changeInherited, &r);
		break;
		
	case rscEditorDragBoxBottom:
		SetDraggedBottomBoxAbs (ay, dy);
		doc->ApplyGuidesToBottom (_dragLayoutUnits, 
			_preview->GetView ()->_changeInherited, &r);
		break;
	}
	doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
}

void CRscWorkView::DragMove (CPoint &point)
{
	CRscEditorView *view = _preview->GetView ();
	CRscEditorDoc *doc = view->GetDocument ();
	if (!_commandOpened)
	{
		_commandOpened = doc->OpenCommand (_T ("Move Item"), true, UNDO_MOVE_ITEM);
		if (!_commandOpened)
		{
			return;
		}
		bool inBases = view->_changeInherited;
		int n = doc->_selectedClasses.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CRscClass *cl = doc->_selectedClasses[i];
			double val = 0.0;
			cl->GetFloatParamInBases ("x", val, true, NULL, false);
			doc->SetFloatParamInBases (cl, "x", val, inBases, false, true, true);
			val = 0.0;
			cl->GetFloatParamInBases ("y", val, true, NULL, false);
			doc->SetFloatParamInBases (cl, "y", val, inBases, false, true, true);
		}
	}
	RECT r;
	GetClientRect (&r);
	CalcPreviewRect (r, r);
	
	double fy = (double) point.y - r.top;
	double fx = (double) point.x - r.left;
	int n = _dragLayoutUnits.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!view->_lockY)
		{
			CheckTopCoordAbs (_dragLayoutUnits[i], &fy, r.bottom - r.top - 1);
		}
		if (!view->_lockX)
		{
			CheckLeftCoordAbs (_dragLayoutUnits[i], &fx, r.right - r.left - 1);
		}
	}
	for (int i = 0; i < n; ++i)
	{
		if (!view->_lockY)
		{
			SetTopCoordAbs (_dragLayoutUnits[i], fy, r.bottom - r.top - 1);
		}
		if (!view->_lockX)
		{
			SetLeftCoordAbs (_dragLayoutUnits[i], fx, r.right - r.left - 1);
		}
	}
	doc->ApplyGuidesToPosition (_dragLayoutUnits, view->_changeInherited, &r);
	doc->ApplyItemSnapToPosition (_dragLayoutUnits, view->_changeInherited, &r);
	doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
}

int CRscWorkView::FindDragBoxAtPt (CPoint &pt, CRscLayoutUnit **dragBoxLU)
{
	*dragBoxLU = NULL;

	RECT view;
	GetClientRect (&view);
	CalcPreviewRect (view, view);

	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	if (_dragLayoutUnits.GetSize () == 1)
	{
		CRscLayoutUnit *lu = _dragLayoutUnits[0];
		RECT r;
		double dx = (double) (view.right - view.left - 1);
		double dy = (double) (view.bottom - view.top - 1);
		double x, y, w, h;
		if (lu->GetX (x) && lu->GetY (y) && lu->GetW (w) && lu->GetH (h))
		{
			CALC_CTRL_RECT (r, view.left, view.top, dx, dy, x, y, w, h)

			RECT rr;
			CRect rf;

#define TEST_DRAG_BOX(x, y, dragBoxCode)			\
			DRAG_BOX ((x), (y), (&view), rr, rf)\
			if (rf.PtInRect (pt))\
			{\
				*dragBoxLU = lu;\
				return dragBoxCode;\
			}

			--r.right;
			--r.bottom;
			TEST_DRAG_BOX (r.left, r.top, rscEditorDragBoxTopLeft)
			TEST_DRAG_BOX (r.right, r.top, rscEditorDragBoxTopRight)
			TEST_DRAG_BOX (r.left, r.bottom, rscEditorDragBoxBottomLeft)
			TEST_DRAG_BOX (r.right, r.bottom, rscEditorDragBoxBottomRight)
			TEST_DRAG_BOX ((r.left + r.right) >> 1, r.top, rscEditorDragBoxTop)
			TEST_DRAG_BOX ((r.left + r.right) >> 1, r.bottom, rscEditorDragBoxBottom)
			TEST_DRAG_BOX (r.left, (r.top + r.bottom) >> 1, rscEditorDragBoxLeft)
			TEST_DRAG_BOX (r.right, (r.top + r.bottom) >> 1, rscEditorDragBoxRight)
			if (::PtInRect (&r, pt))
			{
				*dragBoxLU = lu;
				return rscEditorDragBoxNone;
			}

#undef TEST_DRAG_BOX

		}
	}
	return rscEditorDragBoxNone;

	/*
	CRscDragBoxFinder dragFinder (doc, pt, &view, &_preview->_itemGroupTags[0]);
	if (doc->_selectedClasses.GetSize () == 1)
	{
		if (dragFinder.EnumerateClass (doc->_selectedClasses[0], NULL))
		{
			*dragBoxClass = dragFinder._foundClass;
			return dragFinder._dragBox;
		}
	}
	return rscEditorDragBoxNone;
	*/
}

void CRscWorkView::SelectClass (CRect &r, CRscClass *topClass, bool append)
{
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	CArray<CRscClass*, CRscClass*> arr;
	FindClassesInRect (r, topClass, arr);

	doc->SelectAllClasses (false);
	
	int n, i;
	if (append)
	{
		n = _originallySelectedClasses.GetSize ();
		for (i = 0; i < n; ++i)
		{
			_originallySelectedClasses[i]->_selected = true;
		}
		doc->_selectedClasses.Copy (_originallySelectedClasses);
	}

	n = arr.GetSize ();
	CArray<CRscGroup*, CRscGroup*> grs;
	CArray<CRscClass*, CRscClass*> cls;
	switch (doc->GetGroups (arr, &grs, &cls))
	{
	case SELECTED_ONE_ITEM:
	case SELECTED_NONE_GROUP:
		for (i = 0; i < n; ++i)
		{
			CRscClass *cl = arr[i];
			if (cl != NULL)
			{
				doc->SelectClass (cl, !cl->_selected);
			}
		}
		break;

	case SELECTED_ONE_GROUP:
		{
			int m = grs.GetSize ();
			for (int j = 0; j < m; ++j)
			{
				CRscGroup *gr = grs[j];
				if (gr != NULL)
				{
					doc->SelectGroup (gr, !gr->_members[0]->_selected);
				}
			}
		}
		break;

	case SELECTED_PART_GROUPS:
		{
			int m = grs.GetSize ();
			for (int j = 0; j < m; ++j)
			{
				CRscGroup *gr = grs[j];
				if (gr != NULL)
				{
					doc->SelectGroup (gr, !gr->_members[0]->_selected);
				}
			}
			m = cls.GetSize ();
			for (int j = 0; j < m; ++j)
			{
				CRscClass *cl = cls[j];
				if (cl != NULL)
				{
					doc->SelectClass (cl, !cl->_selected);
				}
			}
		}
	}
}

bool CRscWorkView::SelectClass (CPoint &pt, CRscClass *topClass, bool append)
{
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	CRscClass *cl = FindClassAtPt (pt, topClass);
	if (cl != NULL && cl->_selected)
	{
		if (append)
		{
			doc->SelectClass (cl, false);
		}
		return true;
	}

	if (!append)
	{
		doc->SelectAllClasses (false);
	}
	
	if (cl != NULL)
	{
		doc->SelectClass (cl, true);
		return true;
	}
	return false;
}

void CRscWorkView::SetDraggedRightBoxAbs (LONG dpa, LONG pb)
{
	double a, b;
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();

	if (_pressedDragBoxLU->GetX (a) &&
		_pressedDragBoxLU->GetW (b))
	{
		b = (double) dpa / (double) pb - a;
		if (b < 0)
		{
			/*
			a += b;
			if (a < 0.0)
			{
				a = 0.0;
			}
			if (_pressedDragBoxLU->SetX (a, _preview->GetView ()->_changeInherited, false))
			{
				doc->SetModifiedFlag (TRUE);
			}
			*/
			b = 0;
		}
		if (a + b > 1.0)
		{
			b = 1.0 - a;
		}
		if (_pressedDragBoxLU->SetW (b, _preview->GetView ()->_changeInherited, false))
		{
			doc->SetModifiedFlag (TRUE);
		}
		_pressedDragBoxLU->TransformDone ();
	}
}

void CRscWorkView::SetDraggedBottomBoxAbs (LONG dpa, LONG pb)
{
	double a, b;
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();

	if (_pressedDragBoxLU->GetY (a) &&
		_pressedDragBoxLU->GetH (b))
	{
		b = (double) dpa / (double) pb - a;
		if (b < 0)
		{
			/*
			a += b;
			if (a < 0.0)
			{
				a = 0.0;
			}
			if (_pressedDragBoxLU->SetY (a, _preview->GetView ()->_changeInherited, false))
			{
				doc->SetModifiedFlag (TRUE);
			}
			*/
			b = 0;
		}
		if (a + b > 1.0)
		{
			b = 1.0 - a;
		}
		if (_pressedDragBoxLU->SetH (b, _preview->GetView ()->_changeInherited, false))
		{
			doc->SetModifiedFlag (TRUE);
		}
		_pressedDragBoxLU->TransformDone ();
	}
}

void CRscWorkView::SetDraggedLeftBoxAbs (LONG dpa, LONG pb)
{
	double a, b;
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();

	if (_pressedDragBoxLU->GetX (a) &&
		_pressedDragBoxLU->GetW (b))
	{
		double na = (double) dpa / (double) pb;
		if (na > 1.0)
		{
			na = 1.0;
		}
		else if (na < 0.0) 
		{
			na = 0.0;
		}
		double nb = a + b - na;
		if (nb < 0.0)
		{
			na = a;
			nb = b;
		}
		if (na + nb > 1.0)
		{
			nb = 1.0 - na;
		}

		if (_pressedDragBoxLU->SetW (nb, _preview->GetView ()->_changeInherited, false))
		{
			doc->SetModifiedFlag (TRUE);
			_pressedDragBoxLU->SetX (na, _preview->GetView ()->_changeInherited, false);
		}
		_pressedDragBoxLU->TransformDone ();
	}
}

void CRscWorkView::SetDraggedTopBoxAbs (LONG dpa, LONG pb)
{
	double a, b;
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();

	if (_pressedDragBoxLU->GetY (a) &&
		_pressedDragBoxLU->GetH (b))
	{
		double na = (double) dpa / (double) pb;
		if (na < 0.0) 
		{
			na = 0.0;
		}
		else if (na > 1.0) 
		{
			na = 1.0;
		}
		double nb = a + b - na;
		if (nb < 0.0)
		{
			na = a;
			nb = b;
		}
		if (na + nb > 1.0)
		{
			nb = 1.0 - na;
		}

		if (_pressedDragBoxLU->SetH (nb, _preview->GetView ()->_changeInherited, false))
		{
			doc->SetModifiedFlag (TRUE);
			_pressedDragBoxLU->SetY (na, _preview->GetView ()->_changeInherited, false);
		}
		_pressedDragBoxLU->TransformDone ();
	}
}

void CRscWorkView::CheckTopCoordAbs (CRscLayoutUnit *lu, double *top, LONG height)
{
	double min, max, h;

	if (lu->GetH (h))
	{
		max = (1.0 - h + lu->_refDeltaY) * (double) height;
		min = lu->_refDeltaY * (double) height;
		if (*top < min)
		{
			*top = min;
		}
		else if (*top > max)
		{
			*top = max;
		}
	}
}

void CRscWorkView::CheckLeftCoordAbs (CRscLayoutUnit *lu, double *left, LONG width)
{
	double min, max, w;

	if (lu->GetW (w))
	{
		max = (1.0 - w + lu->_refDeltaX) * (double) width;
		min = lu->_refDeltaX * (double) width;
		if (*left < min)
		{
			*left = min;
		}
		else if (*left > max)
		{
			*left = max;
		}
	}
}

void CRscWorkView::SetTopCoordAbs (CRscLayoutUnit *lu, double top, LONG height)
{
	double y, h;
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();

	if (lu->GetY (y) &&
		lu->GetH (h))
	{
		y = top / (double) height - lu->_refDeltaY;
		if (y < 0.0)
		{
			y = 0.0;
		}
		else if (y + h > 1.0)
		{
			y = 1.0 - h;
		}

		lu->SetY (y, _preview->GetView ()->_changeInherited, false);
		lu->TransformDone ();
	}
}

void CRscWorkView::SetLeftCoordAbs (CRscLayoutUnit *lu, double left, LONG width)
{
	double x, w;
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();

	if (lu->GetX (x) &&
		lu->GetW (w))
	{
		x = left / (double) width - lu->_refDeltaX;
		if (x < 0.0)
		{
			x = 0.0;
		}
		else if (x + w > 1.0)
		{
			x = 1.0 - w;
		}

		lu->SetX (x, _preview->GetView ()->_changeInherited, false);
		lu->TransformDone ();
	}
}

void CRscWorkView::FindClassesInRect (CRect &r, CRscClass *topClass, CArray<CRscClass*, CRscClass*> &arr)
{
	RECT view;
	GetClientRect (&view);
	CalcPreviewRect (view, view);

	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	CRscClassesInRect cf (doc, r, &view, &_preview->_itemGroupTags[0], arr);
	cf.EnumerateClass (topClass, NULL);
}

CRscClass *CRscWorkView::FindClassAtPt (CPoint &pt, CRscClass *topClass)
{
	RECT view;
	GetClientRect (&view);
	CalcPreviewRect (view, view);

	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	CRscClassFinder cf (doc, pt, &view, &_preview->_itemGroupTags[0]);
	cf.EnumerateClass (topClass, NULL);
	return cf._foundClass;
}

void CRscWorkView::DropRegister ()
{
	if (_dropTarget._control == NULL)
	{
		_dropTarget._control = this;
		_dropTarget.Register (this);
	}
}

DROPEFFECT CRscWorkView::OnDragEnter(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point)
{
	ASSERT(_lastDropEffect == DROPEFFECT_NONE);

	LockWindowUpdate ();
	return OnDragOver(pWnd, pDataObject, dwKeyState, point);
}

DROPEFFECT CRscWorkView::OnDragOver(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point) 
{
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable ())
	{
		return DROPEFFECT_NONE;
	}

	UnlockWindowUpdate ();
	UINT cfClass = RegisterClipboardFormat (_T ("class.rsceditor.bi"));

	CRect dropRect;
	GetClientRect (&dropRect);
	DROPEFFECT de = DROPEFFECT_NONE;
	if (dropRect.PtInRect (point) && pDataObject->IsDataAvailable (cfClass))
	{
		CRscWorkTreeItem *dragParam = NULL;
		if (_preview->GetView ()->_dragItem != NULL)
		{
			dragParam = (CRscWorkTreeItem*) _preview->GetView ()->_dragSpaceCtrl->GetItemData (_preview->GetView ()->_dragItem);
		}

		int size = doc->_monitorClass->_innerParams.GetSize ();
		CRscParamBase *from = (size != 0 ? doc->_monitorClass->_innerParams[size - 1] : NULL);

		if ((dwKeyState & (MK_SHIFT | MK_CONTROL)) == (MK_SHIFT | MK_CONTROL) &&
			dragParam != NULL &&
			dragParam->IsParam () &&
			((CRscParamBase*) dragParam)->IsClass () &&
			doc->FindBaseClassDefinition (((CRscClass*) dragParam)->_name, from, doc->_monitorClass, false, true) == dragParam)
		{
			de = DROPEFFECT_LINK;
		}
		else if ((dwKeyState & MK_CONTROL) == 0 &&
			dragParam != NULL &&
			dragParam->IsParam () &&
			((CRscParamBase*) dragParam)->IsClass () &&
			!((CRscClass*) dragParam)->_displayed &&
			doc->FindClassDefinition (((CRscClass*) dragParam)->_name, doc->_monitorClass, false) == dragParam)
		{
			de = DROPEFFECT_MOVE;
		}
		else
		{
			de = DROPEFFECT_COPY;
		}
	}

	if (point == _dragPoint && de == _lastDropEffect)
		return _lastDropEffect;

	_dragPoint = point;

	// otherwise, cursor has moved -- need to update the drag feedback
	CClientDC dc(this);
	if (_lastDropEffect != DROPEFFECT_NONE)
	{
		// erase previous focus rect
		dc.DrawFocusRect(_lastDropRect);
	}
	_lastDropEffect = de;
	if (_lastDropEffect != DROPEFFECT_NONE)
	{
		_lastDropRect = dropRect;
		dc.DrawFocusRect(dropRect);
	}

	LockWindowUpdate ();
	return _lastDropEffect;
}

void CRscWorkView::OnDragLeave(CWnd *pWnd) 
{
	UnlockWindowUpdate ();
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	if (doc->_monitorClass == NULL)
	{
		return;
	}

	CClientDC dc(this);
	if (_lastDropEffect != DROPEFFECT_NONE)
	{
		dc.DrawFocusRect(_lastDropRect); // erase previous focus rect
		_lastDropEffect = DROPEFFECT_NONE;
	}
	UpdateWindow ();
}

BOOL CRscWorkView::OnDrop (CWnd *pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point)
{
	ASSERT_VALID(this);

	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable ())
	{
		return FALSE;
	}

	// clean up focus rect
	OnDragLeave(pWnd);

	UINT cfClass = RegisterClipboardFormat (_T ("class.rsceditor.bi"));
	if (pDataObject->IsDataAvailable (cfClass))
	{
		CString str;
		HGLOBAL data = pDataObject->GetGlobalData (cfClass);
		if (data != NULL)
		{
			LPVOID ptr = ::GlobalLock (data);
			if (ptr != NULL)
			{
				str = (LPCTSTR) ptr;
				::GlobalUnlock (data);
				(void) ProcessClassDropInWorkView (str, point, dropEffect);
			}
			// ::GlobalFree (data);
			return FALSE;
		}
	}

	return FALSE;
}

BOOL CRscWorkView::ProcessClassDropInWorkView (CString &fullCode, CPoint &point, DROPEFFECT dropEffect)
{
	CRect view;
	GetClientRect (&view);
	CalcPreviewRect (view, view);
	if (!view.PtInRect (point))
	{
		return FALSE;
	}

	double x = (double) (point.x - view.left) / (double) (view.right - view.left - 1);
	double y = (double) (point.y - view.top) / (double) (view.bottom - view.top - 1);
	
	CRscWorkTreeItem *dragParam = NULL;
	if (_preview->GetView ()->_dragItem != NULL)
	{
		dragParam = (CRscWorkTreeItem*) _preview->GetView ()->_dragSpaceCtrl->GetItemData (_preview->GetView ()->_dragItem);
	}
	
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	
	int size = doc->_monitorClass->_innerParams.GetSize ();
	CRscParamBase *from = (size != 0 ? doc->_monitorClass->_innerParams[size - 1] : NULL);
	
	if (dropEffect == DROPEFFECT_LINK &&
		dragParam != NULL &&
		dragParam->IsParam () &&
		((CRscParamBase*) dragParam)->IsClass () &&
		doc->FindBaseClassDefinition (((CRscClass*) dragParam)->_name, from, doc->_monitorClass, false, true) == dragParam)
	{
		// derive
		if (doc->OpenCommand (_T ("New Derived Item"), true, UNDO_NEW_DERIVED_ITEMS))
		{
			CRscClass *cl = doc->NewDerivedClass ((CRscClass*) dragParam, doc->_monitorClass, NULL, false, false);
			if (cl != NULL)
			{
				doc->SelectAllClasses (false);
				doc->SelectClass (cl, true);
				
				doc->SetArrayStringParamInBases (doc->_monitorClass, CRscEditorView::_itemGroups[_preview->_itemGroup], cl->_name, true, false, true);
				doc->SetFloatParamInBases (cl, "x", x, false, false, true, false);
				doc->SetFloatParamInBases (cl, "y", y, false, false, true, false);
				
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
				doc->CloseCommand ();
				return TRUE;
			}
			doc->DisposeCommand ();
		}
	}
	else if (dropEffect == DROPEFFECT_MOVE &&
		dragParam != NULL &&
		dragParam->IsParam () &&
		((CRscParamBase*) dragParam)->IsClass () &&
		!((CRscClass*) dragParam)->_displayed &&
		doc->FindClassDefinition (((CRscClass*) dragParam)->_name, doc->_monitorClass, false) == dragParam)
	{
		// move
		if (doc->OpenCommand (_T ("Add Item"), true, UNDO_ADD_ITEMS))
		{
			if (_preview->GetView ()->MoveItem ((CRscClass*) dragParam, x, y))
			{
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
				doc->CloseCommand ();
				return TRUE;
			}
			doc->DisposeCommand ();
		}
	}
	else if (dropEffect == DROPEFFECT_COPY)
	{
		// copy
		if (doc->OpenCommand (_T ("Copy Item"), true, UNDO_COPY_ITEMS))
		{
			if (_preview->GetView ()->GetDocument ()->CopyItem (CRscEditorView::_itemGroups[_preview->_itemGroup], fullCode, x, y))
			{
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
				doc->CloseCommand ();
				return TRUE;
			}
			doc->DisposeCommand ();
		}
	}

	return FALSE;
}

void CRscWorkView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	if (doc->_selectedClasses.GetSize () != 1)
	{
		return;
	}

	_preview->GetView ()->LaunchClassEditor (doc->_selectedClasses[0], false);
}

void CRscWorkView::OnDestroy() 
{
	CWnd::OnDestroy();
	
	if (_dropTarget._control != NULL)
	{
		_dropTarget.Revoke ();
		_dropTarget._control = NULL;
	}
}

void CRscWorkView::CalcPreviewRect (RECT &prev, const RECT &ctrl)
{
	bool show = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_GUIDES, show);
	if (show)
	{
		prev.left = ctrl.left + 12;
		prev.top = ctrl.top + 12;
		prev.right = ctrl.right - 2;
		prev.bottom = ctrl.bottom - 2;
	}
	else
	{
		prev.left = ctrl.left + 2;
		prev.top = ctrl.top + 2;
		prev.right = ctrl.right - 2;
		prev.bottom = ctrl.bottom - 2;
	}
}

void CRscWorkView::CalcVertGuideRect (RECT &vert, const RECT &ctrl)
{
	bool show = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_GUIDES, show);
	if (show)
	{
		vert.left = ctrl.left + 2;
		vert.top = ctrl.top + 12;
		vert.right = vert.left + 10;
		vert.bottom = ctrl.bottom - 2;
	}
	else
	{
		vert.left = vert.top = vert.right = vert.bottom = 0;
	}
}

void CRscWorkView::CalcHorzGuideRect (RECT &horz, const RECT &ctrl)
{
	bool show = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_GUIDES, show);
	if (show)
	{
		horz.left = ctrl.left + 12;
		horz.top = ctrl.top + 2;
		horz.right = ctrl.right - 2;
		horz.bottom = horz.top + 10;
	}
	else
	{
		horz.left = horz.top = horz.right = horz.bottom = 0;
	}
}

void CRscWorkView::MoveSelectedItems (double dpx, double dpy) 
{
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();
	if (doc->_curUndoType != UNDO_MOVE_ITEM)
	{
		if (!doc->OpenCommand (_T ("Move Item"), true, UNDO_MOVE_ITEM))
		{
			return;
		}
	}
	bool inBases = _preview->GetView ()->_changeInherited;
	int n = doc->_selectedClasses.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		CRscClass *cl = doc->_selectedClasses[i];
		double val = 0.0;
		cl->GetFloatParamInBases ("x", val, true, NULL, false);
		doc->SetFloatParamInBases (cl, "x", val, inBases, false, true, true);
		val = 0.0;
		cl->GetFloatParamInBases ("y", val, true, NULL, false);
		doc->SetFloatParamInBases (cl, "y", val, inBases, false, true, true);
	}

	RECT r;
	GetClientRect (&r);
	CalcPreviewRect (r, r);
	
	int height = r.bottom - r.top - 1;
	int width = r.right - r.left - 1;

	doc->DisposeLayoutUnits (_dragLayoutUnits);
	doc->CreateLayoutUnits (&_dragLayoutUnits, doc->_selectedClasses);

	double x, y;
	_dragLayoutUnits[0]->GetX (x);
	_dragLayoutUnits[0]->GetY (y);
	
	double fy = y * (double) height + dpy;
	double fx = x * (double) width + dpx;
	
	n = _dragLayoutUnits.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		CheckTopCoordAbs (_dragLayoutUnits[i], &fy, height);
		CheckLeftCoordAbs (_dragLayoutUnits[i], &fx, width);
	}
	for (int i = 0; i < n; ++i)
	{
		SetTopCoordAbs (_dragLayoutUnits[i], fy, height);
		SetLeftCoordAbs (_dragLayoutUnits[i], fx, width);
	}
	doc->ApplyGuidesToPosition (_dragLayoutUnits, inBases, &r);
	doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);

	// doc->CloseCommand ();
}

BOOL CRscWorkView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	// return CWnd::OnSetCursor(pWnd, nHitTest, message);

	UpdateCursor ();
	return TRUE;
}

void CRscWorkView::UpdateCursor ()
{
	UINT flags = 0;
	flags |= ((::GetKeyState (VK_CONTROL) & 0x8000) != 0) ? MK_CONTROL : 0;
	flags |= ((::GetKeyState (VK_SHIFT) & 0x8000) != 0) ? MK_SHIFT : 0;
	UpdateCursor (flags);
}

void CRscWorkView::UpdateCursor (UINT flags)
{
	CPoint pt;
	GetCursorPos (&pt);
	ScreenToClient (&pt);
	UpdateCursor (flags, pt);
}

void CRscWorkView::UpdateCursor (UINT flags, CPoint &point)
{
	CRscEditorDoc *doc = _preview->GetView ()->GetDocument ();

	RECT ctrlRect;
	CRect previewRect;
	CRect vertGuideRect;
	CRect horzGuideRect;

	GetClientRect (&ctrlRect);
	CalcPreviewRect (previewRect, ctrlRect);
	CalcHorzGuideRect (horzGuideRect, ctrlRect);
	CalcVertGuideRect (vertGuideRect, ctrlRect);

	doc->DisposeLayoutUnits (_dragLayoutUnits);
	doc->CreateLayoutUnits (&_dragLayoutUnits, doc->_selectedClasses);

	if (_highlightedGuideIndex != -1)
	{
		Invalidate (FALSE);
		UpdateWindow ();
		_highlightedGuideIndex = -1;
	}

	if (previewRect.PtInRect (point))
	{
		CRscClass *cl = FindClassAtPt (point, doc->_monitorClass);
		_preview->GetView ()->SetItemLabel (cl);

		_pressedDragBox = FindDragBoxAtPt (point, &_pressedDragBoxLU);
		if (_pressedDragBoxLU != NULL)
		{
			switch (_pressedDragBox)
			{
			case rscEditorDragBoxNone:
				SetCursor (cl != NULL ? ((CRscEditorApp*) AfxGetApp ())->_cursorHand : ((CRscEditorApp*) AfxGetApp ())->_cursorArrow);
				// TRACE (cl 1= NULL ? _T ("Cursor: Hand\n") : _T ("Cursor: Arrow\n"));
				break;

			case rscEditorDragBoxLeft:
			case rscEditorDragBoxRight:
				SetCursor (((CRscEditorApp*) AfxGetApp ())->_cursorHorzArrow);
				// TRACE (_T ("Cursor: H-Arrow\n"));
				break;

			case rscEditorDragBoxTop:
			case rscEditorDragBoxBottom:
				SetCursor (((CRscEditorApp*) AfxGetApp ())->_cursorVertArrow);
				// TRACE (_T ("Cursor: V-Arrow\n"));
				break;

			case rscEditorDragBoxTopLeft:
			case rscEditorDragBoxBottomRight:
				SetCursor (((CRscEditorApp*) AfxGetApp ())->_cursorLeftTopArrow);
				// TRACE (_T ("Cursor: NWSE-Arrow\n"));
				break;

			case rscEditorDragBoxTopRight:
			case rscEditorDragBoxBottomLeft:
				SetCursor (((CRscEditorApp*) AfxGetApp ())->_cursorRightTopArrow);
				// TRACE (_T ("Cursor: NESW-Arrow\n"));
				break;
			}
		}
		else
		{
			SetCursor (((CRscEditorApp*) AfxGetApp ())->_cursorArrow);
		}
	}
	else if (horzGuideRect.PtInRect (point))
	{
		CRscGuide *guide = doc->GetGuide (doc->_monitorClass, false);
		if (guide != NULL)
		{
			CClientDC dc (this);
			guide->HighlightHorz (dc, &horzGuideRect, &previewRect, point.x, _highlightedGuideIndex);
		}
		if (guide == NULL || _highlightedGuideIndex == -1)
		{
			SetCursor (((CRscEditorApp*) AfxGetApp ())->_cursorArrow);
		}
		else
		{
			SetCursor ((flags & (MK_CONTROL | MK_SHIFT)) != 0 ? ((CRscEditorApp*) AfxGetApp ())->_cursorEraser : ((CRscEditorApp*) AfxGetApp ())->_cursorHorzArrow);
		}
	}
	else if (vertGuideRect.PtInRect (point))
	{
		CRscGuide *guide = doc->GetGuide (doc->_monitorClass, false);
		if (guide != NULL)
		{
			CClientDC dc (this);
			guide->HighlightVert (dc, &vertGuideRect, &previewRect, point.y, _highlightedGuideIndex);
		}
		if (guide == NULL || _highlightedGuideIndex == -1)
		{
			SetCursor (((CRscEditorApp*) AfxGetApp ())->_cursorArrow);
		}
		else
		{
			SetCursor ((flags & (MK_CONTROL | MK_SHIFT)) != 0 ? ((CRscEditorApp*) AfxGetApp ())->_cursorEraser : ((CRscEditorApp*) AfxGetApp ())->_cursorVertArrow);
		}
	}
	else
	{
		SetCursor (((CRscEditorApp*) AfxGetApp ())->_cursorArrow);
	}
}

LRESULT CRscWorkView::OnUserUpdateCursor (WPARAM wParam, LPARAM lParam)
{
	UpdateCursor ();
	return S_OK;
}
