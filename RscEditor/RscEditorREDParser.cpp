
#include "StdAfx.h"

#include "RscEditor.h"
#include "RscEditorItems.h"
#include "RscMacro.h"
#include "RscEditorDoc.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"
#include "RscEditorREDParser.h"

const short CRscEditorCodeParser::_saveMacroSaveHashLexFlags = CRscEditorLex::flagSkipWhiteChars | CRscEditorLex::flagAllComments;

const short CRscEditorCodeParser::_saveMacroLexFlags = CRscEditorLex::flagSkipWhiteChars | CRscEditorLex::flagAllComments | CRscEditorLex::flagHashCallback;
const short CRscEditorCodeParser::_saveMacroSaveCharsLexFlags = CRscEditorLex::flagAllComments | CRscEditorLex::flagHashCallback;
const short CRscEditorCodeParser::_saveMacroSaveCommentsCharsLexFlags = CRscEditorLex::flagHashCallback;

const short CRscEditorCodeParser::_unwrapMacroLexFlags = CRscEditorLex::flagSkipWhiteChars | CRscEditorLex::flagParserCallbacks;
const short CRscEditorCodeParser::_unwrapMacroSaveCommentsLexFlags = CRscEditorLex::flagSkipWhiteChars | CRscEditorLex::flagMacroCallback | CRscEditorLex::flagHashCallback;
const short CRscEditorCodeParser::_unwrapMacroSaveCommentsNlsLexFlags = CRscEditorLex::flagSkipWhiteSpaces | CRscEditorLex::flagMacroCallback | CRscEditorLex::flagHashCallback;

/*! Skip "//" comments. 
    More advanced code to the default version (error handling and message about comment removing).
*/
bool CRscEditorCodeParser::ParseSlashSlash (CRscEditorLex *lex)
{
	CString cmt(_T ("//"));
	CRscEditorToken tok;

	lex->GetNextToken (&tok, NULL, 0);

	while (tok.type != rscEditorLexEof)
	{
		if (tok.type == rscEditorLexError)
		{
			_errorFileLine = lex->GetLineNumber ();
			ASSERT (0);
			return false;
		}
		if (tok.type == '\n')
		{
			break;
		}
		if (tok.type != '\r')
		{
			cmt += tok.string;
		}
		lex->GetNextToken (&tok, NULL, 0);
	}

	if (_showRemoveCommentBox && _inInclude == NULL)
	{
		//ASSERT (0);
		CString msg;
		msg.Format (_T ("Removed Comment:\n%s(%d):\n%s"), _errorFileName, lex->GetLineNumber (), cmt);
		AfxMessageBox (msg, MB_OK);
		if (_docModified != NULL)
		{
			*_docModified = true;
			ASSERT (0);
		}
	}
	return true;
}

/*! Skip "/*" comments. 
    More advanced code to the default version (error handling and message about comment removing).
*/
bool CRscEditorCodeParser::ParseSlashStar (CRscEditorLex *lex)
{
	CString cmt(_T ("/*"));
	CRscEditorToken tok;

	lex->GetNextToken (&tok, NULL, 0);
	
	while (tok.type != rscEditorLexEof)
	{
		if (tok.type == rscEditorLexError)
		{
			_errorFileLine = lex->GetLineNumber ();
			ASSERT (0);
			return false;
		}
		if (tok.type == rscEditorLex_starSlash)
		{
			cmt += tok.string;
			if (_showRemoveCommentBox && _inInclude == NULL)
			{
				//ASSERT (0);
				CString msg;
				msg.Format (_T ("Removed Comment:\n%s(%d):\n%s"), _errorFileName, lex->GetLineNumber (), cmt);
				AfxMessageBox (msg, MB_OK);
				if (_docModified != NULL)
				{
					*_docModified = true;
					ASSERT (0);
				}
			}
			return true;
		}
		cmt += tok.string;
		lex->GetNextToken (&tok, NULL, 0);
	}

	return false;
}

/*! This method parse comment and put the comment to the 'comment' and return true.
    This method is used to remember comments at some specific places in the code.
*/
bool CRscEditorCodeParser::ParseCOMMENT (CRscEditorLex &lex, CRscEditorToken &tok, CString &comment, short lexFlags)
{
	comment.Empty ();

	if (tok.type == rscEditorLex_slashSlash)
	{
		comment += tok.string;
		lex.GetNextToken (&tok, NULL, 0);

		while (tok.type != rscEditorLexEof)
		{
			if (tok.type == rscEditorLexError)
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}
			if (tok.type == '\n')
			{
				lex.GetNextToken (&tok, this, lexFlags);
				return true;
			}
			if (tok.type != '\r')
			{
				comment += tok.string;
			}
			lex.GetNextToken (&tok, NULL, 0);
		}
	}
	else if (tok.type == rscEditorLex_slashStar)
	{
		comment += tok.string;
		lex.GetNextToken (&tok, NULL, 0);
		
		while (tok.type != rscEditorLexEof)
		{
			if (tok.type == rscEditorLexError)
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}
			if (tok.type == rscEditorLex_starSlash)
			{
				comment += tok.string;
				lex.GetNextToken (&tok, this, lexFlags);
				return true;
			}
			comment += tok.string;
			lex.GetNextToken (&tok, NULL, 0);
		}
	}

	return true;
}

/*! This method parse multiple comments and put them to the 'comments' and return true.
    This method is used to remember comments at some specific places in the code.
*/
bool CRscEditorCodeParser::ParseCOMMENTS (CRscEditorLex &lex, CRscEditorToken &tok, CStringArray &comments, short lexFlags)
{
	comments.RemoveAll ();

	for (;;)
	{
		CString comment;

		if (!ParseCOMMENT (lex, tok, comment, lexFlags & ~CRscEditorLex::flagAllComments))
		{
			return false;
		}

		if (comment.IsEmpty ())
		{
			return true;
		}

		comments.Add (comment);
	}
}

/*! Parse source code of the class. New object put into 'arr' at 'index' and update the 'index' for next use.
*/
bool CRscEditorCodeParser::ParseCLASS (CRscEditorLex &lex, CRscEditorToken &tok, 
								   CArray<CRscParamBase*, CRscParamBase*> &arr, 
								   int &index, CRscClass *parent, bool readOnly,
								   CString *&rename, CRscMacroCall *inMacroCall, 
								   const CStringArray &comments)
{
	if (tok.type != rscEditorLex_class)
	{
		_errorFileLine = lex.GetLineNumber ();
		ASSERT (0);
		return false;
	}
	lex.GetAlphaWord (&tok); // lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);

	if (tok.type != rscEditorLexId) // class name can be an integer
	{
		_errorFileLine = lex.GetLineNumber ();
		ASSERT (0);
		return false;
	}
	if (rename != NULL)
	{
		_tcscpy (&tok.string[0], (LPCTSTR) *rename);
		rename = NULL;
	}
	if (_checkClassName)
	{
		CRscParamBase *cc = _doc->FindParamInClass (tok.string, parent, false);
		if (cc != NULL)
		{
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;
		}
	}
	bool inFavourites = false;
	int favouriteIndex = -1;
	CString parentFullName;
	if (parent != NULL)
	{
		parent->GetFullName (parentFullName);
	}
	CRscClass *cl = _doc->NewClass (tok.string, readOnly, parentFullName, inFavourites, favouriteIndex, _inInclude, inMacroCall);
	if (cl == NULL)
	{
		_errorFileLine = lex.GetLineNumber ();
		ASSERT (0);
		return false;
	}
	arr.InsertAt (index, cl);
	cl->_parent = parent;
	cl->_preComments.Append (comments);
	lex.GetNextToken (&tok, this, _unwrapMacroSaveCommentsNlsLexFlags);

	if (tok.type == ':')
	{
		lex.GetAlphaWord (&tok); // lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);

		if (tok.type != rscEditorLexError)
		{
			if (tok.type != rscEditorLexId) // class name can be an integer
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				delete cl;
				arr.RemoveAt (index);
				return false;
			}
			// find the base class structure
			CRscClass *base = _doc->FindBaseClassDefinition (tok.string, cl, cl->_parent, false, false);
			if (base == NULL)
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				delete cl;
				arr.RemoveAt (index);
				return false;
			}
			cl->_baseClass = base;
			base->_derivedClasses.Add (cl);
			lex.GetNextToken (&tok, this, _unwrapMacroSaveCommentsNlsLexFlags);
		}
		else
		{
			lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);

			CString baseClassName;
			if (!ParseExactFULLCLASSNAME (lex, tok, cl->_baseClass, _oldClass, _newClass, &baseClassName))
			{
				int n = _invalidClasses.GetSize ();
				for (int i = 0; ; ++i)
				{
					if (i == n)
					{
						CRscClass *ic = new CRscClass (baseClassName, true, inMacroCall, 0, _inInclude);
						if (ic == NULL)
						{
							_errorFileLine = lex.GetLineNumber ();
							ASSERT (0);
							delete cl;
							arr.RemoveAt (index);
							return false;
						}
						_invalidClasses.Add (ic);
						cl->_baseClass = ic;
						break;
					}

					if (baseClassName == _invalidClasses[i]->_name)
					{
						cl->_baseClass = _invalidClasses[i];
						break;
					}
				}
			}
			cl->_baseClass->_derivedClasses.Add (cl);
			lex.GetNextToken (&tok, this, _unwrapMacroSaveCommentsNlsLexFlags);
		}
	}

	if (tok.type == '\n')
	{
		lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
	}
	else
	{
		if (!ParseCOMMENT (lex, tok, cl->_postComment, _unwrapMacroLexFlags))
		{
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			delete cl;
			arr.RemoveAt (index);
			return false;
		}
	}
	
	if (tok.type != '{')
	{
		_errorFileLine = lex.GetLineNumber ();
		ASSERT (0);
		delete cl;
		arr.RemoveAt (index);
		return false;
	}
	// Remember Macro
	lex.GetNextToken (&tok, this, _basicSaveCommentsLexFlags);

	int outIndex = 0;
	if (!ParsePARAMETERS (lex, tok, cl->_innerParams, outIndex, cl, readOnly, NULL, inMacroCall, NULL))
	{
		delete cl;
		arr.RemoveAt (index);
		return false;
	}
	if (tok.type != '}')
	{
		_errorFileLine = lex.GetLineNumber ();
		ASSERT (0);
		delete cl;
		arr.RemoveAt (index);
		return false;
	}
	// Remember Macro
	lex.GetNextToken (&tok, this, parent == NULL ? _basicSaveCommentsHashLexFlags : _basicSaveCommentsLexFlags);

	// Remember Macro
	while (tok.type == ';')
	{
		lex.GetNextToken (&tok, this, parent == NULL ? _basicSaveCommentsHashLexFlags : _basicSaveCommentsLexFlags);
	}

	++index;
	if (_inInclude != NULL)
	{
		_inInclude->_params.Add (cl);
	}
	return true;
}

/*! Parse parameters - classes, attributes, comments and enums.
*/
bool CRscEditorCodeParser::ParsePARAMETERS (CRscEditorLex &lex, CRscEditorToken &tok, 
										CArray<CRscParamBase*, CRscParamBase*> &arr,
										int &index, CRscClass *parent, bool readOnly,
										CString *rename, CRscMacroCall *inMacroCall,
										CStringArray *preComments)
{
	while (tok.type == rscEditorLex_class || tok.type == rscEditorLexId || 
		tok.type == rscEditorLex_enum || tok.type == ';' ||
		tok.type == rscEditorLex_slashSlash || tok.type == rscEditorLex_slashStar)
	{
		switch (tok.type)
		{
		case ';':
			// Remember Macro
			lex.GetNextToken (&tok, this, _basicSaveCommentsLexFlags);
			break;

		case rscEditorLex_class:
		case rscEditorLex_enum:
		case rscEditorLexId:
		case rscEditorLex_slashStar:
		case rscEditorLex_slashSlash:
			if (!ParsePARAMETER (lex, tok, arr, index, parent, readOnly, rename, inMacroCall, preComments))
			{
				return false;
			}
			ASSERT(arr.GetSize () >= index);
			break;

		default:
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;
		}
	}
	return true;
}

/*! Parse just one parameter (with comments) - a class, an attribute, an enum or a macro call.
*/
bool CRscEditorCodeParser::ParsePARAMETER (CRscEditorLex &lex, CRscEditorToken &tok, 
									   CArray<CRscParamBase*, CRscParamBase*> &arr, 
									   int &index, CRscClass *parent, bool readOnly,
									   CString *&rename, CRscMacroCall *inMacroCall, 
									   CStringArray *preComments)
{
	CStringArray comments;
	if (!ParseCOMMENTS (lex, tok, comments, parent == NULL ? _basicSaveCommentsHashLexFlags : _basicSaveCommentsLexFlags))
	{
		return false;
	}

	if (tok.type == rscEditorLexId)
	{
		if (_tcscmp (tok.string, _T ("__EXEC")) == 0)
		{
			lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
			
			if (tok.type != '(')
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}
			lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
			
			CString exprStr;
			if (!ParseEXEC (lex, tok, &exprStr))
			{
				return false;
			}
			
			if (tok.type != ')')
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}
			// Remember Macro
			lex.GetNextToken (&tok, this, parent == NULL ? _basicSaveCommentsHashLexFlags : _basicSaveCommentsLexFlags);

			if (!_unwrapMacros)
			{
				//ASSERT (inMacroCall == NULL);
				CRscMacroCall *mc = new CRscMacroCall (_T (""), _inInclude, inMacroCall);
				if (mc == NULL)
				{
					_errorFileLine = lex.GetLineNumber ();
					ASSERT (0);
					return false;
				}

				mc->_preComments.Append (comments);
				mc->_textUse.Format (_T ("__EXEC (%s)"), exprStr);
				mc->_semicolon = true;
				_doc->_macroCalls.Add (mc);
				if (_inInclude != NULL)
				{
					_inInclude->_macroCalls.Add (mc);
				}

				// Generates __EXEC macro calling in output document
				if (inMacroCall == NULL)
				{
					CRscParamDummy *pd = new CRscParamDummy (mc, CRscParamBase::_macroBegin | CRscParamBase::_macroEnd, _inInclude);
					if (pd == NULL)
					{
						_errorFileLine = lex.GetLineNumber ();
						ASSERT (0);
						return false;
					}
					arr.InsertAt (index, pd);
					++index;
					if (_inInclude != NULL)
					{
						_inInclude->_params.Add (pd);
					}
				}
			}
			return true;
		}

		CRscMacro *mac;
		if ((mac = _doc->FindMacro (tok.string)) != NULL)
		{
			CRscMacroCall *mc = new CRscMacroCall (_T (""), _inInclude, inMacroCall);
			if (mc == NULL)
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}

			mc->_preComments.Append (comments);
			if (!ParseMACROCALLING (lex, tok, mc->_textUse, mac, arr, index, parent, readOnly, mc))
			{
				delete mc;
				return false;
			}

			if (tok.type == _T (';'))
			{
				mc->_semicolon = true;
				// Remember Macro
				lex.GetNextToken (&tok, this, parent == NULL ? _basicSaveCommentsHashLexFlags : _basicSaveCommentsLexFlags);
			}

			_doc->_macroCalls.Add (mc);
			if (_inInclude != NULL)
			{
				_inInclude->_macroCalls.Add (mc);
			}
			return true;
		}
	}

	switch (tok.type)
	{
	case rscEditorLex_class:
		if (!ParseCLASS (lex, tok, arr, index, parent, readOnly, rename, inMacroCall, comments))
		{
			return false;
		}
		return true;

	case rscEditorLex_enum:
		{
			lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
			CRscEnum *rscEnum = new CRscEnum (_inInclude);
			if (rscEnum == NULL)
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}
			rscEnum->_preComments.Append (comments);
			
			if (tok.type == rscEditorLexId)
			{
				rscEnum->_name = tok.string;
				lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
			}

			if (tok.type != '{')
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				delete rscEnum;
				return false;
			}
			lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);

			do {
				if (tok.type != rscEditorLexId)
				{
					_errorFileLine = lex.GetLineNumber ();
					ASSERT (0);
					delete rscEnum;
					return false;
				}
				CRscEnumItem *rscEnumItem = rscEnum->Add (tok.string);
				if (rscEnumItem == NULL)
				{
					_errorFileLine = lex.GetLineNumber ();
					ASSERT (0);
					delete rscEnum;
					return false;
				}
				lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
				
				if (tok.type == '=')
				{
					// Remember Macro
					lex.GetNextToken (&tok, this, _basicLexFlags);
					
					const static int endTokens[] = {_T (','), _T ('}')};
					const static int endTokensSize = sizeof (endTokens) / sizeof (int);
					
					CRscAttributeValueElement *sub;
					if (!ParseELEMENT (lex, tok, &sub, NULL, rscEnumItem, endTokens, endTokensSize, 0))
					{
						ASSERT (0);
						delete rscEnum;
						return false;
					}
					rscEnumItem->_value = sub;
				}

				if (tok.type != ',')
				{
					if (tok.type != '}')
					{
						_errorFileLine = lex.GetLineNumber ();
						ASSERT (0);
						delete rscEnum;
						return false;
					}
					lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
					_doc->_enums.Add (rscEnum);
					return true;
				}
				lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);

			} while (tok.type != '}');
			// Remember Macro
			lex.GetNextToken (&tok, this, parent == NULL ? _basicSaveCommentsHashLexFlags : _basicSaveCommentsLexFlags);

			rscEnum->_preComments.RemoveAll ();
			rscEnum->_preComments.Append (comments);

			_doc->_enums.Add (rscEnum);
		}
		return true;

	case rscEditorLexId:
		{
			if (rename != NULL)
			{
				_tcscpy (&tok.string[0], (LPCTSTR) *rename);
				rename = NULL;
			}
			CRscParamBase *cc = _doc->FindParamInClass (tok.string, parent, false);
			if (cc != NULL)
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}
			CRscAttribute *attr = new CRscAttribute (tok.string, NULL, readOnly, inMacroCall, 0, _inInclude);
			if (attr == NULL)
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}
			attr->_parent = parent;
			lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
			
			switch (tok.type)
			{
			case '[':
				lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
				
				if (tok.type != ']')
				{
					_errorFileLine = lex.GetLineNumber ();
					ASSERT (0);
					delete attr;
					return false;
				}
				lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);

				if (tok.type != '=')
				{
					_errorFileLine = lex.GetLineNumber ();
					ASSERT (0);
					delete attr;
					return false;
				}
				// Remember Macro
				lex.GetNextToken (&tok, this, _basicLexFlags);

				if (!ParseARRAYELEMENT (lex, tok, &attr->_value, NULL, attr))
				{
					delete attr;
					return false;
				}

				if (tok.type != ';')
				{
					_errorFileLine = lex.GetLineNumber ();
					ASSERT (0);
					delete attr;
					return false;
				}
				// Remember Macro
				lex.GetNextToken (&tok, this, parent == NULL ? _basicSaveCommentsNlsHashLexFlags : _basicSaveCommentsNlsLexFlags);

				if (tok.type == '\n')
				{
					lex.GetNextToken (&tok, this, parent == NULL ? _basicSaveCommentsHashLexFlags : _basicSaveCommentsLexFlags);
				}
				else
				{
					if (!ParseCOMMENT (lex, tok, attr->_postComment, parent == NULL ? _basicSaveCommentsHashLexFlags : _basicSaveCommentsLexFlags))
					{
						delete attr;
						return false;
					}
				}

				attr->_preComments.RemoveAll ();
				attr->_preComments.Append (comments);

				arr.InsertAt (index, attr);
				++index;
				if (_inInclude != NULL)
				{
					_inInclude->_params.Add (attr);
				}
				return true;
				
			case '=':
				{
					// Remember Macro
					lex.GetNextToken (&tok, this, _basicLexFlags);
					
					const static int endTokens[] = {_T (';'), _T ('\r'), _T ('\n'), rscEditorLex_slashSlash};
					const static int endTokensSize = sizeof (endTokens) / sizeof (int);
					
					CRscAttributeValueElement *sub;
					if (!ParseELEMENT (lex, tok, &sub, NULL, attr, endTokens, endTokensSize, CRscEditorLex::flagSlashStarComment))
					{
						delete attr;
						return false;
					}
					attr->_value = sub;
					// Remember Macro
					if (tok.type != rscEditorLex_slashSlash)
					{
						lex.GetNextToken (&tok, this, parent == NULL ? _basicSaveCommentsNlsHashLexFlags : _basicSaveCommentsNlsLexFlags);
					}
			
					if (tok.type == '\n')
					{
						lex.GetNextToken (&tok, this, parent == NULL ? _basicSaveCommentsHashLexFlags : _basicSaveCommentsLexFlags);
					}
					else
					{
						if (!ParseCOMMENT (lex, tok, attr->_postComment, parent == NULL ? _basicSaveCommentsHashLexFlags : _basicSaveCommentsLexFlags))
						{
							delete attr;
							return false;
						}
					}

					if (!readOnly && attr->_value->IsOnlyMacro ())
					{
						for (int i = 0; CRscMacro::_idTypes[i] != NULL; ++i)
						{
							if (attr->_name == CRscMacro::_idTypes[i])
							{
								LPCTSTR idVal = attr->_value->GetIdValue ();
								if (idVal != NULL)
								{
									if (!_doc->IsRscID (idVal))
									{
										CRscMacro *id = new CRscMacro (idVal, NULL);
										if (id == NULL)
										{
											_errorFileLine = lex.GetLineNumber ();
											ASSERT (0);
											delete attr;
											return false;
										}
										id->_value.Format (_T ("%d"), _doc->GetNewIdValue ());
										_doc->_macros.Add (id);
										if (NULL != _docModified)
										{
											*_docModified = true;
											ASSERT (0);
										}

										CRscAttributeValueMacro *vm = new CRscAttributeValueMacro (idVal, NULL, attr, false);
										if (vm == NULL)
										{
											_errorFileLine = lex.GetLineNumber ();
											ASSERT (0);
											delete attr;
											return false;
										}
										delete attr->_value;
										attr->_value = vm;
									}
								}
								break;
							}
						}
					}

					attr->_preComments.RemoveAll ();
					attr->_preComments.Append (comments);

					arr.InsertAt (index, attr);
					++index;
					if (_inInclude != NULL)
					{
						_inInclude->_params.Add (attr);
					}
					return true;
				}

			default:
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				delete attr;
				return false;
			}
		}

	default:
		/*
		_errorFileLine = lex.GetLineNumber ();
		ASSERT (0);
		return false;
		*/
		if (preComments != NULL)
		{
			preComments->Append (comments);
		}
		return true;
	}
}

//! Parse exactly full class name (e.g. BigDialog::cotrols::okBtn) and do not read nothing more from input analyzer.
bool CRscEditorCodeParser::ParseExactFULLCLASSNAME (CRscEditorLex &lex, CRscEditorToken &tok, CRscClass *&rc, CString *oldClass, CString *newClass, CString *className)
{
	CString fullName;

	do {
		if (tok.type != ':')
		{
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;
		}
		fullName += tok.string;
		lex.GetNextToken (&tok, NULL, 0);
		
		if (tok.type != ':')
		{
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;
		}
		fullName += tok.string;
		lex.GetAlphaWord (&tok); // lex.GetNextToken (&tok, NULL, 0);
		
		if (tok.type != rscEditorLexId) // class name can be an integer
		{
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;
		}
		fullName += tok.string;
		if (className != NULL)
		{
			*className = tok.string;
		}
		lex.GetNextToken (&tok, NULL, 0);
	} while (tok.type == ':');

	rc = _doc->GetClassFromFullName (fullName, oldClass, newClass);
	return rc != NULL;
}

//! Parse full class name (e.g. BigDialog::cotrols::okBtn) and read next token from the input analyzer.
bool CRscEditorCodeParser::ParseFULLCLASSNAME (CRscEditorLex &lex, CRscEditorToken &tok, CRscClass *&rc, CString *oldClass, CString *newClass, CString *className)
{
	if (!ParseExactFULLCLASSNAME (lex, tok, rc, oldClass, newClass, className))
	{
		return false;
	}

	lex.GetNextToken (&tok, this, _unwrapMacroSaveCommentsLexFlags); // originally _unwrapMacroLexFlags
	return true;
}

/*! Parse macro calling (also with parameters). And calculate the string of the unwrapped macro.
*/
bool CRscEditorCodeParser::ParseMACROCALLING (CRscEditorLex &lex, CRscEditorToken &tok, CString &macroCall, CRscMacro *macro,
											 CArray<CRscParamBase*, CRscParamBase*> &arr, int &index, CRscClass *parent, 
											 bool readOnly, CRscMacroCall *inMacroCall)
{
	CRscMacroParamList pl;
	if (!ParseMACROCALLING (lex, tok, macroCall, macro->_paramList != NULL, parent == NULL, &pl))
	{
		return false;
	}

	bool showRemoveCommentBox = true;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_REMOVE_COMMENT_BOX, showRemoveCommentBox);

#if 0
	CRscEditorCodeParser macParser (NULL, _doc, _docModified, _checkClassName, true, false, showRemoveCommentBox, _inInclude);
	CRscEditorStringInput macInput (macroCall);
#else
	CString unwrapped;
	if (!macro->Apply (unwrapped, macro->_paramList != NULL ? &pl : NULL))
	{
		_errorFileLine = lex.GetLineNumber ();
		ASSERT (0);
		return false;
	}

	CRscEditorCodeParser macParser (NULL, _doc, _docModified, _checkClassName, false, true, showRemoveCommentBox, _inInclude);
	CRscEditorStringInput macInput (unwrapped);
#endif

	CRscEditorLex macLex (&macInput, CRscEditorLex::rscEditorKeywords, CRscEditorLex::rscEditorKeywordsLength);
	CRscEditorToken macTok;
	macLex.GetNextToken (&macTok, &macParser, parent == NULL ? macParser._basicSaveCommentsHashLexFlags : macParser._basicSaveCommentsLexFlags);
	int macroBeginIndex = index;
	if (!macParser.ParsePARAMETERS (macLex, macTok, arr, index, parent, true, NULL, inMacroCall, NULL))
	{
		_errorFileLine = lex.GetLineNumber ();
		ASSERT (0);
		return false;
	}
	if (index > macroBeginIndex)
	{
		arr[macroBeginIndex]->_macroFlags |= CRscParamBase::_macroBegin;
		arr[index - 1]->_macroFlags |= CRscParamBase::_macroEnd;
	}
	else
	{
		CRscParamDummy *pd = new CRscParamDummy (inMacroCall, CRscParamBase::_macroBegin | CRscParamBase::_macroEnd, _inInclude);
		if (pd == NULL)
		{
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;
		}
		arr.InsertAt (index, pd);
		++index;
		if (_inInclude != NULL)
		{
			_inInclude->_params.Add (pd);
		}
	}

	return true;
}

//! Parse macro calling (also with parameters).
bool CRscEditorCodeParser::ParseMACROCALLING (CRscEditorLex &lex, CRscEditorToken &tok, CString &macroCall, 
											  bool hasParams, bool saveHash, CRscMacroParamList *pl)
{
	if (tok.type != rscEditorLexId)
	{
		_errorFileLine = lex.GetLineNumber ();
		ASSERT (0);
		return false;
	}
	macroCall = tok.string;
	if (!hasParams)
	{
		lex.GetNextToken (&tok, this, saveHash ? _basicSaveCommentsHashLexFlags : _basicSaveCommentsLexFlags);
	}
	else
	{
		lex.GetNextToken (&tok, this, _saveMacroSaveHashLexFlags);

		if (tok.type != _T ('('))
		{
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;
		}
		macroCall += _T ("(");
		lex.GetNextToken (&tok, this, _saveMacroSaveHashLexFlags);

		if (tok.type != _T (')'))
		{
			CString par;
			if (!ParseMACROCALLINGPARAM (lex, tok, par))
			{
				return false;
			}
			if (pl != NULL)
			{
				pl->_params.Add (par);
			}
			macroCall += par;

			while (tok.type != _T (')'))
			{
				if (tok.type != _T (','))
				{
					return false;
				}
				macroCall += _T (", ");
				lex.GetNextToken (&tok, this, _saveMacroSaveHashLexFlags);

				par.Empty ();
				if (!ParseMACROCALLINGPARAM (lex, tok, par))
				{
					return false;
				}
				if (pl != NULL)
				{
					pl->_params.Add (par);
				}
				macroCall += par;
			}
		}
		macroCall += _T (")");
		// Remember Macro
		lex.GetNextToken (&tok, this, saveHash ? _basicSaveCommentsHashLexFlags : _basicSaveCommentsLexFlags);
	}

	return true;
}

//! Parse parameters of the macro calling.
bool CRscEditorCodeParser::ParseMACROCALLINGPARAM (CRscEditorLex &lex, CRscEditorToken &tok, CString &param)
{
	int bracketCounter = 0;
	while (tok.type != _T (',') && (bracketCounter != 0 || tok.type != _T (')')))
	{
		switch (tok.type)
		{
		case rscEditorLexEof:
		case rscEditorLexError:
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;

		case _T ('('):
			++bracketCounter;
			param += tok.string;
			break;

		case _T (')'):
			--bracketCounter;
			param += tok.string;
			break;

		default:
			param += tok.string;
			break;
		}

		lex.GetNextToken (&tok, this, CRscEditorLex::flagAllComments);
	}

	return true;
}

//! Parse array (e.g. "{1,2,3}"). It can be also an array inside other array.
bool CRscEditorCodeParser::ParseARRAYELEMENT (CRscEditorLex &lex, CRscEditorToken &tok, CRscAttributeValueBase **val,
										  CRscAttributeValueArray *inArray, CRscAttribute *inParam)
{
	// Remember Macro
	switch (tok.type)
	{
	case _T ('{'):
		{
			CRscAttributeValueArray *arr = new CRscAttributeValueArray (inArray, inParam);
			if (arr == NULL)
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}
			// Remember Macro
			lex.GetNextToken (&tok, this, _basicSaveCommentsLexFlags);

			CStringArray comments;
			CRscAttributeValueBase *first;
			if (!ParseARRAY (lex, tok, arr->_elements, arr, inParam, &comments, first))
			{
				delete arr;
				return false;
			}
			if (first == NULL)
			{
				arr->_preComments.Append (comments);
			}

			*val = arr;
			return true;
		}

	case rscEditorLexId:
		{
			CRscMacro *m = _doc->FindMacro (tok.string);
			if (m == NULL)
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}

			CRscAttributeValueMacro *vm = new CRscAttributeValueMacro (NULL, inParam, true);
			if (vm == NULL)
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}

			if (!ParseMACROCALLING (lex, tok, vm->_calling, m->_paramList != NULL, false))
			{
				delete vm;
				return false;
			}

			*val = vm;
			return true;
		}

	default:
		_errorFileLine = lex.GetLineNumber ();
		ASSERT (0);
		return false;
	}
}

//! Parse the elements of an array.
bool CRscEditorCodeParser::ParseARRAY (CRscEditorLex &lex, CRscEditorToken &tok, 
									   CArray<CRscAttributeValueBase*, CRscAttributeValueBase*> &vals,
								       CRscAttributeValueArray *inArray, CRscAttribute *inParam, 
									   CStringArray *outComments, CRscAttributeValueBase *&sub)
{
	sub = NULL;

	switch (tok.type)
	{
	case '{':
		{
#if 1 // Support arrays inside array
			if( !ParseARRAYELEMENT( lex, tok, &sub, inArray, inParam ) )
			{
				return false;
			}
			vals.Add( sub );

			if (!ParseCOMMENT (lex, tok, sub->_postComment, _basicLexFlags))
			{
				return false;
			}

			// Remember Macro
			if (tok.type == ',')
			{
				lex.GetNextToken (&tok, this, _basicSaveCommentsNlsLexFlags);

				if (tok.type == '\n')
				{
					lex.GetNextToken (&tok, this, _basicSaveCommentsLexFlags);
				}
				else if (!ParseCOMMENT (lex, tok, sub->_postComment, _basicSaveCommentsLexFlags))
				{
					return false;
				}
			}
			else if (tok.type != '}')
			{
				return false;
			}

			CRscAttributeValueBase *first;
			if (!ParseARRAY (lex, tok, vals, inArray, inParam, NULL, first))
			{
				return false;
			}
			return true;
#else
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;
#endif
		}

	case '}':
		// Remember Macro
		lex.GetNextToken (&tok, this, _basicLexFlags);
		return true;

	case ',':
		{
			sub = new CRscAttributeValueElement (_T (""), inArray, inParam);
			if (sub == NULL)
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}
			vals.Add (sub);

			// Remember Macro
			lex.GetNextToken (&tok, this, _basicSaveCommentsLexFlags);

			CRscAttributeValueBase *first;
			if (!ParseARRAY (lex, tok, vals, inArray, inParam, NULL, first))
			{
				return false;
			}

			return true;
		}

	case rscEditorLex_slashSlash:
	case rscEditorLex_slashStar:
		{
			CStringArray comments;
			if (!ParseCOMMENTS (lex, tok, comments, _basicSaveCommentsLexFlags))
			{
				return false;
			}
			if (!ParseARRAY (lex, tok, vals, inArray, inParam, NULL, sub))
			{
				return false;
			}
			if (sub != NULL)
			{
				sub->_preComments.Append (comments);
			}
			else if (outComments != NULL)
			{
				outComments->Append (comments);
			}
			return true;
		}

	default:
		{
			const static int endTokens[] = {_T (','), _T ('}'), rscEditorLex_slashSlash, rscEditorLex_slashStar};
			const static int endTokensSize = 4;

      CRscAttributeValueElement *subElem;
			if (!ParseELEMENT (lex, tok, &subElem, inArray, inParam, endTokens, endTokensSize, 0))
			{
				return false;
			}
      sub = subElem;
			vals.Add (sub);

			if (!ParseCOMMENT (lex, tok, sub->_postComment, _basicLexFlags))
			{
				return false;
			}

			// Remember Macro
			if (tok.type == ',')
			{
				lex.GetNextToken (&tok, this, _basicSaveCommentsNlsLexFlags);

				if (tok.type == '\n')
				{
					lex.GetNextToken (&tok, this, _basicSaveCommentsLexFlags);
				}
				else if (!ParseCOMMENT (lex, tok, sub->_postComment, _basicSaveCommentsLexFlags))
				{
					return false;
				}
			}
			else if (tok.type != '}')
			{
				return false;
			}

			CRscAttributeValueBase *first;
			if (!ParseARRAY (lex, tok, vals, inArray, inParam, NULL, first))
			{
				return false;
			}
			return true;
		}
	}
}

//! Parse the token of the array elment (handles quotes in strings and no macro calling).
bool CRscEditorCodeParser::ParseELEMENT_Token (CRscEditorLex &lex, CRscEditorToken &tok, CRscAttributeValueElement *elem, const short flags)
{
	switch (tok.type)
	{
	case rscEditorLexEof:
	case rscEditorLexError:
		_errorFileLine = lex.GetLineNumber ();
		ASSERT (0);
		return false;
		
	case '"':
		{
			lex.GetNextToken (&tok, NULL, 0);
			
			bool insideString = true;
			do {
				switch (tok.type)
				{
				case rscEditorLexEof:
				case rscEditorLexError:
					_errorFileLine = lex.GetLineNumber ();
					ASSERT (0);
					return false;

				case '"':
					{
						lex.GetNextToken (&tok, NULL, 0);
						if (tok.type == '"')
						{
							lex.GetNextToken (&tok, NULL, 0);
							elem->_value += _T ("\"\"");
							break;
						}
						
						elem->_value += _T ('"');
						insideString = false;
						lex.Unread (&tok.string[0], _tcslen (tok.string) * sizeof (TCHAR));
						lex.GetNextToken (&tok, this, flags);
					}
					break;

				default:
					elem->_value += tok.string;
					lex.GetNextToken (&tok, NULL, 0);
					break;
				}
			} while (insideString);
			break;
		}

	default:
		elem->_value += tok.string;
		lex.GetNextToken (&tok, this, flags);
		break;
	}

	return true;
}

//! Parse the token of the array elment (can be also a macro calling).
bool CRscEditorCodeParser::ParseELEMENT (CRscEditorLex &lex, CRscEditorToken &tok, CRscAttributeValueElement **val, 
										CRscAttributeValueArray *inArray, CRscAttribute *inParam, 
										const int *endTokens, const int endTokensSize, const short removeCommentFlagsForUnwrap)
{
	bool unwrap = false;
	if (inParam != NULL)
	{
    //! Is parameter name of this array among editables? If yes then it is editable, so unwrap this array element value.
		for (int i = 0; i < CRscAttribute::_editablesSize; ++i)
		{
			if (inParam->_name.CompareNoCase (CRscAttribute::_editables[i]) == 0)
			{
				unwrap = true;
				break;
			}
		}
	}

	CRscAttributeValueElement *elem = new CRscAttributeValueElement (_T (""), inArray, inParam);
	if (elem == NULL)
	{
		_errorFileLine = lex.GetLineNumber ();
		ASSERT (0);
		return false;
	}

	if (!unwrap)
	{
		for (;;)
		{
			for (int toki = 0; toki < endTokensSize; ++toki)
			{
				if (tok.type == endTokens[toki])
				{
					elem->_value.TrimRight ();
					elem->_unwrappedValue = elem->_value;
					*val = elem;
					return true;
				}
			}
			if (!ParseELEMENT_Token (lex, tok, elem, _saveMacroSaveCommentsCharsLexFlags))
			{
				delete elem;
				return false;
			}
		}
	}

	for (;;)
	{
		for (int toki = 0; toki < endTokensSize; ++toki)
		{
			if (tok.type == endTokens[toki])
			{
				elem->_value.TrimRight ();
				{
					CRscEditorCodeParser exprElemParser (NULL, _doc, NULL, false, false, false, false, NULL);
					CRscEditorStringInput exprElemInput (elem->_value);
					CRscEditorLex exprElemLex (&exprElemInput, NULL, 0);
					CRscEditorToken exprElemTok;
					exprElemLex.GetNextToken (&exprElemTok, &exprElemParser, exprElemParser._basicLexFlags);
					double result;
					static const int endTokens[] = {rscEditorLexEof};
					static const int endTokensSize = 1;
					elem->_hasMacro = false;
					elem->_isOnlyMacro = true;
					if (exprElemParser.ParseEXPR (exprElemLex, exprElemTok, result, false, NULL, &elem->_hasMacro, &elem->_isOnlyMacro, endTokens, endTokensSize) && exprElemTok.type == rscEditorLexEof)
					{
						if (result == (unsigned long) result)
						{
							elem->_unwrappedValue.Format (_T ("%lu"), (unsigned long) result);
						}
						else if (result == (long) result)
						{
							elem->_unwrappedValue.Format (_T ("%ld"), (long) result);
						}
						else
						{
							elem->_unwrappedValue.Format (_T ("%g"), result);
						}
						if (!elem->HasMacro ())
						{
							elem->_value = elem->_unwrappedValue;
						}
					}
					else
					{
						CRscEditorCodeParser strElemParser (NULL, _doc, NULL, false, true, false, false, NULL);
						CRscEditorStringInput strElemInput (elem->_value);
						CRscEditorLex strElemLex (&strElemInput, NULL, 0);
						CRscEditorToken strElemTok;
						strElemLex.GetNextToken (&strElemTok, &strElemParser, strElemParser._basicLexFlags);
						while (strElemTok.type != rscEditorLexEof)
						{
							if (strElemTok.type == rscEditorLexError)
							{
								return false;
							}
							elem->_unwrappedValue += strElemTok.string;
							strElemLex.GetNextToken (&strElemTok, &strElemParser, strElemParser._basicLexFlags);
						}
					}
				}
				*val = elem;
				return true;
			}
		}
		
		if (!ParseELEMENT_Token (lex, tok, elem, _saveMacroSaveCommentsCharsLexFlags | removeCommentFlagsForUnwrap))
		{
			delete elem;
			return false;
		}
	}
}

bool CRscEditorCodeParser::ParseMacro (CRscEditorLex *lex, CRscEditorToken *outTok, CRscMacro *&macro)
{
	if (outTok->type != rscEditorLexId)
	{
		_errorFileLine = lex->GetLineNumber ();
		ASSERT (0);
		return false;
	}
	macro = new CRscMacro (outTok->string, _inInclude);
	if (macro == NULL)
	{
		_errorFileLine = lex->GetLineNumber ();
		ASSERT (0);
		return false;
	}
	lex->GetNextToken (outTok, this, 0);

	if (outTok->type == _T ('('))
	{
		macro->_paramList = new CRscMacroParamList ();
		if (macro->_paramList == NULL)
		{
			_errorFileLine = lex->GetLineNumber ();
			ASSERT (0);
			delete macro;
			return false;
		}
		lex->GetNextToken (outTok, this, CRscEditorLex::flagSkipWhiteSpaces);

		if (outTok->type != _T (')'))
		{
			if (outTok->type != rscEditorLexId)
			{
				_errorFileLine = lex->GetLineNumber ();
				ASSERT (0);
				delete macro;
				return false;
			}
			macro->_paramList->_params.Add (outTok->string);

			lex->GetNextToken (outTok, this, CRscEditorLex::flagSkipWhiteSpaces);
			while (outTok->type != _T (')'))
			{
				if (outTok->type != _T (','))
				{
					_errorFileLine = lex->GetLineNumber ();
					ASSERT (0);
					delete macro;
					return false;
				}
				lex->GetNextToken (outTok, this, CRscEditorLex::flagSkipWhiteSpaces);

				if (outTok->type != rscEditorLexId)
				{
					_errorFileLine = lex->GetLineNumber ();
					ASSERT (0);
					delete macro;
					return false;
				}
				macro->_paramList->_params.Add (outTok->string);
				lex->GetNextToken (outTok, this, CRscEditorLex::flagSkipWhiteSpaces);
			}
		}
	}
	else if (outTok->type != _T (' ') && outTok->type != _T ('\t') && 
		outTok->type != _T ('\r') && outTok->type != _T ('\n'))
	{
		_errorFileLine = lex->GetLineNumber ();
		ASSERT (0);
		delete macro;
		return false;
	}
	lex->GetNextToken (outTok, this, CRscEditorLex::flagSkipWhiteSpaces);

	while (outTok->type != '\n')
	{
		switch (outTok->type)
		{
		case rscEditorLexEof:
		case rscEditorLexError:
			_errorFileLine = lex->GetLineNumber ();
			ASSERT (0);
			delete macro;
			return false;

		default:
			macro->_value += outTok->string;
			break;
		}
		lex->GetNextToken (outTok, this, 0);
	}

	macro->_value.TrimRight ();
	return true;
}

bool CRscEditorCodeParser::ParseMacros (CRscEditorLex &lex, CRscEditorToken &tok, CArray<CRscMacro*, CRscMacro*> &arr)
{
	while (tok.type == '#')
	{
		lex.GetNextToken (&tok, this, 0);

		if (tok.type != rscEditorLex_define)
		{
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;
		}
		lex.GetNextToken (&tok, this, CRscEditorLex::flagSkipWhiteSpaces | CRscEditorLex::flagSlashStarComment);

		CRscMacro *macro;
		if (!ParseMacro (&lex, &tok, macro))
		{
			return false;
		}
		arr.Add (macro);
		lex.GetNextToken (&tok, this, CRscEditorLex::flagSkipWhiteChars | CRscEditorLex::flagAllComments);
	}

	if (tok.type != rscEditorLexEof)
	{
		_errorFileLine = lex.GetLineNumber ();
		ASSERT (0);
		return false;
	}

	return true;
}

bool CRscEditorCodeParser::ParseMacroParamList (CRscEditorLex *lex, CRscEditorToken *tok, CRscMacroParamList &pl)
{
	if (tok->type != _T ('('))
	{
		return false;
	}
	lex->GetNextToken (tok, this, _saveMacroSaveHashLexFlags);

	if (tok->type != _T (')'))
	{
		CString param;
		if (!ParseMACROCALLINGPARAM (*lex, *tok, param))
		{
			return false;
		}
		pl._params.Add (param);

		while (tok->type != _T (')'))
		{
			if (tok->type != _T (','))
			{
				return false;
			}
			lex->GetNextToken (tok, this, _saveMacroSaveHashLexFlags);

			param.Empty ();
			if (!ParseMACROCALLINGPARAM (*lex, *tok, param))
			{
				return false;
			}
			pl._params.Add (param);
		}
	}
	return true;
}

bool CRscEditorCodeParser::ProcessMacroId (CRscEditorLex *lex, CRscEditorToken *tok, unsigned short flags)
{
	CRscMacro *m = _doc->FindMacro (tok->string);
	if (m != NULL)
	{
		_hasMacro = true;
		
		CString str;
		if (m->_paramList == NULL)
		{
			if (!m->Apply (str, NULL))
			{
				return false;
			}
		}
		else
		{
			lex->GetNextToken (tok, this, _saveMacroSaveHashLexFlags);

			CRscMacroParamList pl;
			if (!ParseMacroParamList (lex, tok, pl))
			{
				return false;
			}
			if (!m->Apply (str, &pl))
			{
				return false;
			}
		}

		CRscEditorStringBufferInput *inp = new CRscEditorStringBufferInput (str);
		if (inp == NULL) { return false; }

		if (_showUnwrapMacroBox)
		{
			TCHAR msg[512];
			_stprintf (msg, _T ("Macro \"%s\" at line %d will be unwrapped.\nShow this message again?"), m->_macro, lex->GetLineNumber ());

#ifdef _DEBUG
			switch (AfxMessageBox (msg, MB_YESNOCANCEL))
#else
			switch (AfxMessageBox (msg, MB_YESNO))
#endif
			{
			case IDNO:
				_showUnwrapMacroBox = false;
				break;
				
			case IDCANCEL:
				ASSERT (0);
				break;
			}
		}
		
		if (!lex->PushInput (inp))
		{
			return false;
		}

		lex->GetNextToken (tok, this, flags);
		return true;
	}

	return true;
}

bool CRscEditorDocParser::PreprocCondition (bool condition, CRscEditorLex *lex, CRscEditorToken *outTok, unsigned short flags)
{
	if (condition)
	{
		_preprocConditions.Add (preConditionSuccess);
		lex->GetNextToken (outTok, this, flags);
	}
	else
	{
		lex->GetNextToken (outTok, this, CRscEditorLex::flagSkipWhiteChars | CRscEditorLex::flagAllComments);
		for (;;)
		{
			if (outTok->type == _T ('#'))
			{
				lex->GetNextToken (outTok, this, 0);
				if (outTok->type == rscEditorLex_else)
				{
					_preprocConditions.Add (preConditionFailed);
					lex->GetNextToken (outTok, this, flags);
					return true;
				}
				else if (outTok->type == rscEditorLex_endif)
				{
					lex->GetNextToken (outTok, this, flags);
					return true;
				}
			}
			else if (outTok->type == rscEditorLexEof || outTok->type == rscEditorLexError)
			{
				_errorFileLine = lex->GetLineNumber ();
				ASSERT (0);
				return false;
			}
			else
			{
				lex->GetNextToken (outTok, this, CRscEditorLex::flagSkipWhiteChars | CRscEditorLex::flagAllComments);
			}
		}
	}
	return true;
}

bool CRscEditorDocParser::ParseHash (CRscEditorLex *lex, CRscEditorToken *outTok, unsigned short flags)
{
	CRscEditorToken tok2;
	lex->GetNextToken (&tok2, this, 0);

	switch (tok2.type)
	{
	case rscEditorLex_include:
		lex->Unread (tok2.string, _tcslen (tok2.string) * sizeof (TCHAR));
		return true;

	case rscEditorLex_define:
		{
			lex->GetNextToken (outTok, this, CRscEditorLex::flagSkipWhiteSpaces | CRscEditorLex::flagSlashStarComment);
			CRscMacro *m;
			if (!ParseMacro (lex, outTok, m))
			{
				return false;
			}
			_doc->_macros.Add (m);
			lex->GetNextToken (outTok, this, flags);
		}
		return true;

	case rscEditorLexEof:
	case rscEditorLexError:
		_errorFileLine = lex->GetLineNumber ();
		ASSERT (0);
		return false;

	case rscEditorLex_ifndef:
		if (_inInclude == NULL)
		{
			_errorFileLine = lex->GetLineNumber ();
			ASSERT (0);
			return false;
		}
		lex->GetNextToken (&tok2, this, CRscEditorLex::flagSkipWhiteSpaces | CRscEditorLex::flagSlashStarComment);
		if (tok2.type != rscEditorLexId)
		{
			_errorFileLine = lex->GetLineNumber ();
			ASSERT (0);
			return false;
		}
		return PreprocCondition (_doc->FindMacro (tok2.string) == NULL, lex, outTok, flags);

	case rscEditorLex_ifdef:
		if (_inInclude == NULL)
		{
			_errorFileLine = lex->GetLineNumber ();
			ASSERT (0);
			return false;
		}
		lex->GetNextToken (&tok2, this, CRscEditorLex::flagSkipWhiteSpaces | CRscEditorLex::flagSlashStarComment);
		if (tok2.type != rscEditorLexId)
		{
			_errorFileLine = lex->GetLineNumber ();
			ASSERT (0);
			return false;
		}
		return PreprocCondition (_doc->FindMacro (tok2.string) != NULL, lex, outTok, flags);

	case rscEditorLex_else:
		{
			if (_inInclude == NULL)
			{
				_errorFileLine = lex->GetLineNumber ();
				ASSERT (0);
				return false;
			}
			int n = _preprocConditions.GetUpperBound ();
			if (n < 0)
			{
				_errorFileLine = lex->GetLineNumber ();
				ASSERT (0);
				return false;
			}
			
			if (_preprocConditions[n] != preConditionSuccess)
			{
				_errorFileLine = lex->GetLineNumber ();
				ASSERT (0);
				return false;
			}
			_preprocConditions.RemoveAt (n);

			lex->GetNextToken (outTok, this, CRscEditorLex::flagSkipWhiteChars | CRscEditorLex::flagAllComments);
			for (;;)
			{
				if (outTok->type == _T ('#'))
				{
					lex->GetNextToken (outTok, this, 0);
					if (outTok->type == rscEditorLex_endif)
					{
						lex->GetNextToken (outTok, this, flags);
						return true;
					}
				}
				else if (outTok->type == rscEditorLexEof || outTok->type == rscEditorLexError)
				{
					_errorFileLine = lex->GetLineNumber ();
					ASSERT (0);
					return false;
				}
				else
				{
					lex->GetNextToken (outTok, this, CRscEditorLex::flagSkipWhiteChars | CRscEditorLex::flagAllComments);
				}
			}
		}

	case rscEditorLex_endif:
		{
			if (_inInclude == NULL)
			{
				_errorFileLine = lex->GetLineNumber ();
				ASSERT (0);
				return false;
			}
			int n = _preprocConditions.GetUpperBound ();
			if (n < 0)
			{
				_errorFileLine = lex->GetLineNumber ();
				ASSERT (0);
				return false;
			}
			_preprocConditions.RemoveAt (n);

			lex->GetNextToken (outTok, this, flags);
		}
		return true;

	default:
		{
			TCHAR delims[] = {';', '\r', '\n'};
			lex->Unread (tok2.string, _tcslen (tok2.string) * sizeof (TCHAR));
			lex->GetNextWord (&tok2, delims, sizeof (delims) / sizeof (TCHAR));

			outTok->type = rscEditorLexId;
			_stprintf (outTok->string, _T ("#%s"), tok2.string);
			return true;
		}
	}

	return true;
}

bool CRscEditorDocParser::ParseDOCHash (CRscEditorLex &lex, CRscEditorToken &tok, int *index, CStringArray &preComments)
{
	if (tok.type == '#')
	{
		lex.GetNextToken (&tok, this, 0);

		switch (tok.type)
		{
		case rscEditorLex_include:
			{
				lex.GetNextToken (&tok, this, CRscEditorLex::flagSkipWhiteSpaces | CRscEditorLex::flagSlashStarComment | 
					CRscEditorLex::flagMacroCallback);
				
				if (tok.type != rscEditorLexString)
				{
					_errorFileLine = lex.GetLineNumber ();
					ASSERT (0);
					return false;
				}
				tok.string[_tcslen (tok.string) - 1] = _T ('\0'); // remove quotation mark
				
				CRscInclude *incl = new CRscInclude (&tok.string[1]);
				if (incl == NULL)
				{
					ASSERT (0);
					return false;
				}

				incl->_preComments.Append (preComments);
				preComments.RemoveAll ();

				CFile ar;
				if (!ar.Open (&tok.string[1], CFile::modeRead | CFile::shareDenyWrite))
				{
					TCHAR buf[512];
					_stprintf (buf, _T ("File \"%s\" can not be opened for reading."), &tok.string[1]);
					ASSERT (0);
					AfxMessageBox (buf, MB_OK);
					delete incl;
					_errorFileLine = lex.GetLineNumber ();
					return false;
				}

				bool unwrapIncludedMacros = false;
				((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::UNWRAP_INCLUDED_MACROS, unwrapIncludedMacros);
				CRscEditorDocParser parser (&tok.string[1], _doc, NULL, true, unwrapIncludedMacros, false, false, _inInclude == NULL ? incl : _inInclude, incl->_subInclude);
				CRscEditorFileInput in (&ar);
				CRscEditorLex inclLex (&in, CRscEditorLex::rscEditorKeywords, CRscEditorLex::rscEditorKeywordsLength);
				CRscEditorToken inclTok;
				// Remember Macro
				inclLex.GetNextToken (&inclTok, &parser, parser._basicSaveCommentsHashLexFlags);
				int n = _doc->_params.GetSize ();
				int i = 0;
				for (; i < n; ++i)
				{
					if (!_doc->_params[i]->IsIncluded ())
					{
						break;
					}
				}
				int outIndex = i;
				// Macro: koser?
				if (!parser.ParseDOC (inclLex, inclTok, outIndex, true))
				{
					ar.Close ();
					delete incl;
					_tcscpy (_errorFileName, parser._errorFileName);
					_errorFileLine = parser._errorFileLine;
					return false;
				}
				ar.Close ();
				_includings.Add (incl);
				lex.GetNextToken (&tok, this, _basicSaveCommentsHashLexFlags);
				if (index != NULL)
				{
					*index += outIndex - i;
				}
				break;
			}

		case rscEditorLex_define:
			{
				lex.GetNextToken (&tok, this, CRscEditorLex::flagSkipWhiteSpaces | CRscEditorLex::flagSlashStarComment);
				CRscMacro *m;
				if (!ParseMacro (&lex, &tok, m))
				{
					return false;
				}
				m->_preComments.Append (preComments);
				preComments.RemoveAll ();
				_doc->_macros.Add (m);
				lex.GetNextToken (&tok, this, _basicSaveCommentsHashLexFlags);
			}
			break;

		default:
			lex.Unread (tok.string, _tcslen (tok.string) * sizeof (TCHAR));
			lex.Unread (_T ("#"), sizeof (TCHAR));
			lex.GetNextToken (&tok, this, _basicSaveCommentsHashLexFlags);
			if (!ParseHash (&lex, &tok, _basicSaveCommentsHashLexFlags))
			{
				return false;
			}
			break;
		}
	}

	return true;
}

bool CRscEditorDocParser::ParseDOC (CRscEditorLex &lex, CRscEditorToken &tok)
{
	CStringArray preComments;

	while (tok.type != rscEditorLexEof)
	{
		switch (tok.type)
		{
		case rscEditorLexError:
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;

		case '#':
			if (!ParseDOCHash (lex, tok, NULL, preComments))
			{
				return false;
			}
			break;
			
		default:
			{
				int outIndex = _doc->_params.GetSize ();
				if (!ParsePARAMETERS (lex, tok, _doc->_params, outIndex, NULL, false, NULL, NULL, &preComments))
				{
					return false;
				}
			}
		}
	}

	return true;
}

bool CRscEditorDocParser::ParseDOC (CRscEditorLex &lex, CRscEditorToken &tok, int &index, bool readOnly)
{
	CStringArray preComments;

	while (tok.type != rscEditorLexEof)
	{
		switch (tok.type)
		{
		case rscEditorLexError:
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;

		case '#':
			if (!ParseDOCHash (lex, tok, &index, preComments))
			{
				return false;
			}
			break;
			
		default:
			{
				if (!ParsePARAMETERS (lex, tok, _doc->_params, index, NULL, readOnly, NULL, NULL, &preComments))
				{
					return false;
				}
			}
		}
	}

	return true;
}

bool CRscEditorREDParser::ParseCOMMENT (CRscEditorLex &lex, CRscEditorToken &tok, CString &comment, short lexFlags)
{
	comment.Empty ();

	if (tok.type == rscEditorLex_slashSlash)
	{
		comment += tok.string;
		lex.GetNextToken (&tok, NULL, 0);

		while (tok.type != rscEditorLexEof)
		{
			switch (tok.type)
			{
			case rscEditorLexError:
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;

			case _T ('\n'):
				lex.GetNextToken (&tok, this, lexFlags);
				return true;

			case rscEditorLex_favourite:
			case rscEditorLex_monitor:
			case rscEditorLex_group:
			case rscEditorLex_guide:
				comment.Empty ();
				return ParseREDOptions (lex, tok);

			case _T ('\r'):
				break;

			default:
				comment += tok.string;
				break;
			}
			lex.GetNextToken (&tok, NULL, 0);
		}
	}
	else if (tok.type == rscEditorLex_slashStar)
	{
		comment += tok.string;
		lex.GetNextToken (&tok, NULL, 0);
		
		while (tok.type != rscEditorLexEof)
		{
			if (tok.type == rscEditorLexError)
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}
			if (tok.type == rscEditorLex_starSlash)
			{
				comment += tok.string;
				lex.GetNextToken (&tok, this, lexFlags);
				return true;
			}
			comment += tok.string;
			lex.GetNextToken (&tok, NULL, 0);
		}
	}

	return true;
}

bool CRscEditorREDParser::ParseREDOptions (CRscEditorLex &lex, CRscEditorToken &tok)
{
	switch (tok.type)
	{
	case rscEditorLex_favourite:
		{
			lex.GetNextToken (&tok, this, CRscEditorLex::flagSkipWhiteSpaces);

			CRscClass *fc;
			if (!ParseFULLCLASSNAME (lex, tok, fc, NULL, NULL, NULL))
			{
				return false;
			}
			
			_doc->_favouriteClasses.Add (fc);
		}
		break;

	case rscEditorLex_monitor:
		{
			lex.GetNextToken (&tok, this, CRscEditorLex::flagSkipWhiteSpaces);

			if (!ParseFULLCLASSNAME (lex, tok, _doc->_monitorClass, NULL, NULL, NULL))
			{
				return false;
			}
		}
		break;

	case rscEditorLex_group:
		{
			lex.GetNextToken (&tok, this, CRscEditorLex::flagSkipWhiteSpaces);

			CRscClass *fc;
			if (!ParseFULLCLASSNAME (lex, tok, fc, NULL, NULL, NULL))
			{
				return false;
			}

			CRscGroup *group = new CRscGroup (fc);
			if (group == NULL)
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}

			while (tok.type == ':')
			{
				if (!ParseFULLCLASSNAME (lex, tok, fc, NULL, NULL, NULL))
				{
					delete group;
					return false;
				}

				group->_members.Add (fc);
			}

			group->Attach ();
			_doc->_groups.Add (group);
		}
		break;

	case rscEditorLex_guide:
		{
			lex.GetNextToken (&tok, this, CRscEditorLex::flagSkipWhiteSpaces);

			CRscClass *fc;
			if (!ParseFULLCLASSNAME (lex, tok, fc, NULL, NULL, NULL))
			{
				return false;
			}

			CRscGuide *guide = new CRscGuide (fc);
			if (guide == NULL)
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}

			while (tok.type == rscEditorLexId)
			{
				if (_tcscmp (tok.string, _T ("H")) == 0)
				{
					lex.GetNextToken (&tok, this, CRscEditorLex::flagSkipWhiteSpaces);

					if (tok.type != rscEditorLexLong && tok.type != rscEditorLexFixedFloat)
					{
						_errorFileLine = lex.GetLineNumber ();
						ASSERT (0);
						delete guide;
						return false;
					}
					guide->_horz.Add (tok.fnumber);
					guide->_horzPxl.Add (-1);
					lex.GetNextToken (&tok, this, CRscEditorLex::flagSkipWhiteSpaces);
				}
				else if (_tcscmp (tok.string, _T ("V")) == 0)
				{
					lex.GetNextToken (&tok, this, CRscEditorLex::flagSkipWhiteSpaces);

					if (tok.type != rscEditorLexLong && tok.type != rscEditorLexFixedFloat)
					{
						_errorFileLine = lex.GetLineNumber ();
						ASSERT (0);
						delete guide;
						return false;
					}
					guide->_vert.Add (tok.fnumber);
					guide->_vertPxl.Add (-1);
					lex.GetNextToken (&tok, this, CRscEditorLex::flagSkipWhiteSpaces);
				}
				else
				{
					_errorFileLine = lex.GetLineNumber ();
					ASSERT (0);
					delete guide;
					return false;
				}
			}

			_doc->_guides.Add (guide);
			lex.GetNextToken (&tok, this, CRscEditorLex::flagSkipWhiteChars);
		}
		break;

	default:
		_errorFileLine = lex.GetLineNumber ();
		ASSERT (0);
		return false;
	}

	return true;
}
