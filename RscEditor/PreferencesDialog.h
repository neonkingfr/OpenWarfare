#if !defined(AFX_PREFERENCESDIALOG_H__3C1CDA5D_4C51_4E81_97E5_9AB50C853E3A__INCLUDED_)
#define AFX_PREFERENCESDIALOG_H__3C1CDA5D_4C51_4E81_97E5_9AB50C853E3A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//! Class of preferences dialog.
class CPreferencesDialog : public CDialog
{
public:
	CPreferencesDialog(CWnd* pParent = NULL);   // standard constructor

	//{{AFX_DATA(CPreferencesDialog)
	enum { IDD = IDD_PREFERENCES_DIALOG };
	CButton	_unwrapIncludedMacros;
	CButton	_showRemoveCommentBox;
	CEdit	_tabulator;
	CButton	_controlClasses;
	CEdit	_arrayTextWrapLimit;
	CEdit	_externalViewer;
	CEdit	_externalEditor;
	CEdit	_guideUnit;
	CEdit	_guideSnap;
	CEdit	_gridVertUnit;
	CEdit	_gridHorzUnit;
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CPreferencesDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:
	//{{AFX_MSG(CPreferencesDialog)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PREFERENCESDIALOG_H__3C1CDA5D_4C51_4E81_97E5_9AB50C853E3A__INCLUDED_)
