
#include "stdafx.h"
#include "rsceditor.h"
#include "Preferences.h"

#include "RscEditorParser.h"
#include "RscEditorLex.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// Already in preferences dialog
LPCTSTR CPreferences::GRID_HORZ_UNIT = _T ("grid_horz_unit");
LPCTSTR CPreferences::GRID_VERT_UNIT = _T ("grid_vert_unit");
LPCTSTR CPreferences::GUIDE_UNIT = _T ("guide_unit");
LPCTSTR CPreferences::GUIDE_SNAP = _T ("guide_snap");
LPCTSTR CPreferences::EDITOR_EXTERN = _T ("editor_extern");
LPCTSTR CPreferences::BULDOZER_WINDOW_CLASS = _T ("buldozer_window_class");

// In progress
LPCTSTR CPreferences::TABULATOR = _T ("tabulator");
LPCTSTR CPreferences::ARRAY_TEXT_WRAP_LIMIT = _T ("arrayTextWrapLimit");
LPCTSTR CPreferences::UNWRAP_INCLUDED_MACROS = _T ("unwrap_included_macros");
LPCTSTR CPreferences::SHOW_REMOVE_COMMENT_BOX = _T ("editor_showRemoveCommentBox");
LPCTSTR CPreferences::CONTROL_CLASSES = _T ("control_classes");

// Should be in preferences dialog

// Needn't be in preferences dialog
LPCTSTR CPreferences::ITEM_SNAP_SWITCH = _T ("item_snap_switch");
LPCTSTR CPreferences::GUIDES_SWITCH = _T ("guides_switch");
LPCTSTR CPreferences::SHOW_GUIDES = _T ("show_guides");
LPCTSTR CPreferences::GRID_SWITCH = _T ("grid_switch");
LPCTSTR CPreferences::SHOW_GRID = _T ("show_grid");
LPCTSTR CPreferences::EDITOR_FINDWHAT = _T ("editor_findWhat");
LPCTSTR CPreferences::EDITOR_REPLACEWITH = _T ("editor_replaceWith");
LPCTSTR CPreferences::GLOBAL_FAVOURITES = _T ("global_favourites_");

CPreferences::CPreferences()
{

}

CPreferences::~CPreferences()
{

}

void CPreferences::Save ()
{
	CString prefName;
	if (!GetPrefFileName (prefName))
	{
		return;
	}

	CFile ar;
	if (!ar.Open (prefName, CFile::modeCreate | CFile::modeWrite | CFile::shareExclusive))
	{
		return;
	}

	int size = _keys.GetSize ();
	for (int i = 0; i < size; ++i)
	{
		ar.Write (_keys[i], _keys[i].GetLength () * sizeof (TCHAR));
		ar.Write (_T ("=\""), _tcslen (_T ("=\"")) * sizeof (TCHAR));
		ar.Write (_values[i], _values[i].GetLength () * sizeof (TCHAR));
		ar.Write (_T ("\"\r\n"), _tcslen (_T ("\"\r\n")) * sizeof (TCHAR));
	}
	ar.Close ();
}

void CPreferences::Load ()
{
	CString prefName;
	if (!GetPrefFileName (prefName))
	{
		return;
	}

	CFile ar;
	if (!ar.Open (prefName, CFile::modeRead | CFile::shareDenyWrite))
	{
		return;
	}

	CRscEditorFileInput in (&ar);
	CRscEditorLex lex (&in, NULL, 0);
	CRscEditorToken tok;
	lex.GetNextToken (&tok, NULL, CRscEditorLex::flagSkipWhiteChars);
	while (tok.type == rscEditorLexId)
	{
		CString key (tok.string);
		lex.GetNextToken (&tok, NULL, CRscEditorLex::flagSkipWhiteChars);
		if (tok.type != _T ('='))
		{
			break;
		}
		lex.GetNextToken (&tok, NULL, CRscEditorLex::flagSkipWhiteChars);
		if (tok.type != rscEditorLexString)
		{
			break;
		}
		tok.string[_tcslen (tok.string) - 1] = _T ('\0');
		_keys.Add (key);
		_values.Add (&tok.string[1]);
		lex.GetNextToken (&tok, NULL, CRscEditorLex::flagSkipWhiteChars);
	}
	ar.Close ();
}

bool CPreferences::GetValue (LPCTSTR key, CString &str)
{
	int size = _keys.GetSize ();
	for (int i = 0; i < size; ++i)
	{
		if (_keys[i] == key)
		{
			str = _values[i];
			return true;
		}
	}
	return false;
}

bool CPreferences::GetValue (LPCTSTR key, LPTSTR str, int n)
{
	int size = _keys.GetSize ();
	for (int i = 0; i < size; ++i)
	{
		if (_keys[i] == key)
		{
			_tcsncpy (str, _values[i], n);
			return true;
		}
	}
	return false;
}

bool CPreferences::IsValue (LPCTSTR key)
{
	int size = _keys.GetSize ();
	for (int i = 0; i < size; ++i)
	{
		if (_keys[i] == key)
		{
			return true;
		}
	}
	return false;
}

bool CPreferences::GetPrefFileName(CString &name)
{
	LPTSTR instName = name.GetBuffer (MAX_PATH);
	bool ok = GetModuleFileName (AfxGetInstanceHandle (), instName, MAX_PATH) != 0;
	name.ReleaseBuffer ();

	if (ok)
	{
		int i = name.ReverseFind (_T ('.'));
		if (i > 0)
		{
			name = name.Left (i + 1);
			name += _T ("ini");
			return true;
		}
	}
	return false;
}

bool CPreferences::GetValue (LPCTSTR key, float &f)
{
	CString str;
	if (GetValue (key, str))
	{
		LPCTSTR s = (LPCTSTR) str;
		LPTSTR e;
		double d;
		d = _tcstod (s, &e);
		if (s != e)
		{
			f = (float) d;
			return true;
		}
	}
	return false;
}

bool CPreferences::GetValue (LPCTSTR key, double &f)
{
	CString str;
	if (GetValue (key, str))
	{
		LPCTSTR s = (LPCTSTR) str;
		LPTSTR e;
		double d;
		d = _tcstod (s, &e);
		if (s != e)
		{
			f = d;
			return true;
		}
	}
	return false;
}

bool CPreferences::GetValue (LPCTSTR key, int &i)
{
	CString str;
	if (GetValue (key, str))
	{
		LPCTSTR s = (LPCTSTR) str;
		LPTSTR e;
		long l;
		l = _tcstol (s, &e, 0);
		if (s != e)
		{
			i = (int) l;
			return true;
		}
	}
	return false;
}

bool CPreferences::GetValue (LPCTSTR key, bool &b)
{
	CString str;
	if (GetValue (key, str))
	{
		if (str.CompareNoCase (_T ("true")) == 0)
		{
			b = true;
			return true;
		}
		else if (str.CompareNoCase (_T ("false")) == 0)
		{
			b = false;
			return true;
		}
	}
	return false;
}

void CPreferences::SetValue (LPCTSTR key, LPCTSTR str, bool save)
{
	int size = _keys.GetSize ();
	for (int i = 0; i < size; ++i)
	{
		if (_keys[i] == key)
		{
			_values[i] = str;

			if (save)
			{
				Save ();
			}
			return;
		}
	}
	_keys.Add (key);
	_values.Add (str);

	if (save)
	{
		Save ();
	}
	return;
}

void CPreferences::SetValue (LPCTSTR key, float f, bool save)
{
	CString str;
	str.Format (_T ("%g"), f);
	SetValue (key, str, save);
}

void CPreferences::SetValue (LPCTSTR key, double f, bool save)
{
	CString str;
	str.Format (_T ("%g"), f);
	SetValue (key, str, save);
}

void CPreferences::SetValue (LPCTSTR key, int i, bool save)
{
	CString str;
	str.Format (_T ("%d"), i);
	SetValue (key, str, save);
}

void CPreferences::SetValue (LPCTSTR key, bool b, bool save)
{
	SetValue (key, b ? _T ("true") : _T ("false"), save);
}
