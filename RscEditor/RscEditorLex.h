
#ifndef _RSCEDITORLEX_H_
#define _RSCEDITORLEX_H_

//! Enum codes of possible lexical tokens.
enum ERscEditorTokenType
{
	rscEditorLexLong = 0x00010000,
	rscEditorLexFixedFloat,
	rscEditorLexString,

	rscEditorLexKeyword,
	rscEditorLexId,
	rscEditorLexEof,
	rscEditorLexError,

	rscEditorLex_or = 0x00020000,
	rscEditorLex_and,
	rscEditorLex_equal,

	rscEditorLex_include = 0x00030000,
	rscEditorLex_define,
	rscEditorLex_class,
	rscEditorLex_enum,
	rscEditorLex_favourite,
	rscEditorLex_monitor,
	rscEditorLex_group,
	rscEditorLex_guide,

	rscEditorLex_undef,
	rscEditorLex_ifdef,
	rscEditorLex_ifndef,
	rscEditorLex_else,
	rscEditorLex_endif,
	
	rscEditorLex_slashSlash,
	rscEditorLex_slashStar,
	rscEditorLex_starSlash,

	rscEditorLex_w,
	rscEditorLex_h,
	rscEditorLex_x,
	rscEditorLex_y,
};

//! Structure describing keyword.
struct CRscEditorKeyword
{
	LPCTSTR token;
	int id;
};

//! Abstract class describing input for lexical analyze.
class CRscEditorInput
{
public:
  //! Members for input buffer.
	TCHAR buffer[1024];
	unsigned int bufferIndex;
	unsigned int currentIndex;
	unsigned int lineNumber;

	CRscEditorInput ()
		: bufferIndex (0), currentIndex (0), lineNumber (1)
	{
	}

	virtual ~CRscEditorInput ()
	{
	}

	unsigned int Read (void *buf, unsigned int max)
	{
		if (bufferIndex > 0)
		{
			unsigned char *b = (unsigned char*) buf;

			if (bufferIndex >= max)
			{
				for (unsigned int i = 0; i < max; ++i)
				{
					if ((b[i] = buffer[bufferIndex - 1 - i]) == '\n')
					{
						++lineNumber;
					}
				}
				bufferIndex -= max;
				currentIndex += max;
				return max;
			}
			else
			{
				unsigned int i = 0;
				for (; i < bufferIndex; ++i)
				{
					if ((b[i] = buffer[bufferIndex - 1 - i]) == '\n')
					{
						++lineNumber;
					}
				}
				bufferIndex = 0;
				int r = i + ReadFromSource (&b[i], max - i);
				currentIndex += r;
				return r;
			}
		}
		else
		{
			int r = ReadFromSource (buf, max);
			currentIndex += r;
			for (int i = 0; i < r; ++i)
			{
				if (((char*) buf)[i] == '\n')
				{
					++lineNumber;
				}
			}
			return r;
		}
	}

	void Unread (const void *buf, unsigned int num)
	{
		const unsigned char *b = (const unsigned char*) buf;

		while (num > 0)
		{
			--num;
			if ((buffer[bufferIndex++] = b[num]) == '\n')
			{
				--lineNumber;
			}
		}

		currentIndex -= num;
	}

	unsigned int GetLineNumber () const
	{
		return lineNumber;
	}

protected:
  //! This method must be overriden in each derived class.
	virtual unsigned int ReadFromSource (void *buf, unsigned int max) = 0;
};

//! Input from MFC file.
class CRscEditorFileInput : public CRscEditorInput
{
protected:
	CFile *file;

public:
	CRscEditorFileInput (CFile *ar)
		: file (ar)
	{
	}

protected:
	virtual unsigned int ReadFromSource (void *buf, unsigned int max)
	{
		return file->Read (buf, max);
	}
};

//! Input from MFC archive.
class CRscEditorArchiveInput : public CRscEditorInput
{
protected:
	CArchive *archive;

public:
	CRscEditorArchiveInput (CArchive *ar)
		: archive (ar)
	{
	}

protected:
	virtual unsigned int ReadFromSource (void *buf, unsigned int max)
	{
		return archive->Read (buf, max);
	}
};

//! Input from string.
class CRscEditorStringInput : public CRscEditorInput
{
protected:
	const char *string;
	const char *p;
	unsigned int index;
	unsigned int length;

public:
	CRscEditorStringInput (const char *str)
		: string (str), p (str), index (0), length (strlen (str))
	{
	}

	CRscEditorStringInput (const char *str, unsigned int offset)
		: string (str), p (str + offset), index (offset), length (strlen (str))
	{
	}

	CRscEditorStringInput (const char *str, unsigned int offset, unsigned int len)
		: string (str), p (str + offset), index (offset), length (len)
	{
	}

protected:
	virtual unsigned int ReadFromSource (void *buf, unsigned int max)
	{
		unsigned int i = length - index;
		if (i < max)
		{
			max = i;
		}

		char *dst = (char*) buf;
		for (i = 0; i < max; ++i)
		{
			*dst++ = *p++;
		}

		index += max;
		return max;
	}
};

//! Input from MFC string.
class CRscEditorStringBufferInput : public CRscEditorInput
{
protected:
	CString string;
	unsigned int index;
	unsigned int length;

public:
	CRscEditorStringBufferInput (const char *str)
		: string (str), index (0), length (strlen (str))
	{
	}

	CRscEditorStringBufferInput (const char *str, unsigned int offset)
		: string (str), index (offset), length (strlen (str))
	{
	}

	CRscEditorStringBufferInput (const char *str, int unsigned offset, unsigned int len)
		: string (str), index (offset), length (len)
	{
	}

	virtual ~CRscEditorStringBufferInput ()
	{
	}

protected:
	virtual unsigned int ReadFromSource (void *buf, unsigned int max)
	{
		unsigned int i = length - index;
		if (i < max)
		{
			max = i;
		}

		char *dst = (char*) buf;
		for (i = 0; i < max; ++i)
		{
			*dst++ = string.GetAt (index++);
		}

		return max;
	}
};

//! Lexical analyzer. (Handle also preprocessor commands as #include, #define, #if, etc.)
class CRscEditorLex
{
public:
  //! Standard keywords.
	static const CRscEditorKeyword rscEditorKeywords[];

  //! Number of standard keywords.
	static const unsigned int rscEditorKeywordsLength;

protected:
  //! If some other file is included, number of inputs increases.
	enum 
	{
		maxInputs = 256,
	};

  //! Describes the state of input (included file).
	struct CRscEditorLexCfg
	{
		CRscEditorInput *_input;
		TCHAR _lastChar;
		bool _isNotEof;
	};

  //! Array of available inputs.
	CRscEditorLexCfg cfgs[maxInputs];

  //! Current input index.
	unsigned int cfgPtr;

  //! Used array of keywords.
	const CRscEditorKeyword *keywords;

  //! Number of used keywords.
	const unsigned int keywordsLength;

public:
  //! Flags to customize next token reading (e.g. commants are supported only at some places).
	enum
	{
    //! Flag saying the lexical analyzer to skip spaces.
		flagSkipWhiteSpaces			= 1 << 0,

    //! Flag saying the lexical analyzer to skip new line characters.
		flagSkipNewlines			= 1 << 1,

    //! Flag saying the lexical analyzer to skip spaces and new line characters.
		flagSkipWhiteChars = flagSkipWhiteSpaces | flagSkipNewlines,

    //! Flag saying the lexical analyzer to skip "//" comments.
		flagSlashSlashComment		= 1 << 2,

    //! Flag saying the lexical analyzer to skip "/*" comments.
		flagSlashStarComment		= 1 << 3,

    //! Flag saying the lexical analyzer to call parser's callback for each identifier to test macro calling.
		flagMacroCallback			= 1 << 4,

    //! Flag saying the lexical analyzer to call parser's callback for each '#' character.
		flagHashCallback			= 1 << 5,

    //! Flag saying the lexical analyzer to skip all comments.
		flagAllComments = flagSlashSlashComment | flagSlashStarComment,

    //! Flag saying the lexical analyzer to use all parser callbacks.
		flagParserCallbacks = flagAllComments | flagMacroCallback | flagHashCallback,
	};

	CRscEditorLex (CRscEditorInput *in, const CRscEditorKeyword *keys, 
		const unsigned int keysLength)
		: cfgPtr(0), keywords (keys), keywordsLength (keysLength)
	{
    // Read first character.
		cfgs[0]._input = in;
		cfgs[0]._isNotEof = in->Read (&cfgs[0]._lastChar, sizeof (cfgs[0]._lastChar)) == sizeof (cfgs[0]._lastChar);
	}

	~CRscEditorLex ()
	{
    // Destroy oll input except the first one - passed into this analyzer.
		while (cfgPtr > 0)
		{
			delete cfgs[cfgPtr]._input;
			cfgs[cfgPtr--]._input = NULL;
		}
	}

  //! Get the next word.
	void GetNextWord (CRscEditorToken *token, TCHAR *delims, int numOfDelims)
	{
		int i = 0;
		TCHAR *lastChar = &cfgs[cfgPtr]._lastChar;
		bool *isNotEof = &cfgs[cfgPtr]._isNotEof;
		while (*isNotEof)
		{
			for (int j = 0; j < numOfDelims; ++j)
			{
				if (delims[j] == *lastChar)
				{
					goto end;
				}
			}
			token->string[i++] = *lastChar;
			AdvanceLastChar (isNotEof, lastChar);
		}
end:
		token->string[i] = _T ('\0');
		token->type = i == 0 ? rscEditorLexError : rscEditorLexId;
	}

  //! Get the next alpha-numerical word.
	void GetAlphaWord (CRscEditorToken *token)
	{
		TCHAR *lastChar = &cfgs[cfgPtr]._lastChar;
		bool *isNotEof = &cfgs[cfgPtr]._isNotEof;

		while (*isNotEof && _istspace(*lastChar))
		{
			AdvanceLastChar (isNotEof, lastChar);
		}

		int i = 0;
		while (*isNotEof && (_istalnum(*lastChar) || *lastChar == _T ('_')))
		{
			token->string[i++] = *lastChar;
			AdvanceLastChar (isNotEof, lastChar);
		}

		token->string[i] = _T ('\0');
		token->type = i == 0 ? rscEditorLexError : rscEditorLexId;
	}
	
  //! Get the next character.
	inline void GetNextChar (CRscEditorToken *token)
	{
		if (GetNextChar (token->string[0]))
		{
			token->string[1] = _T ('\0');
			token->type = token->string[0];
			return;
		}

		token->type = rscEditorLexEof;
		return;
	}

  //! Get the next character.
	inline bool GetNextChar (TCHAR &chr)
	{
		if (cfgs[cfgPtr]._isNotEof || AdvanceLastChar ())
		{
			TCHAR &lastChar = cfgs[cfgPtr]._lastChar;
			chr = lastChar;
			AdvanceLastChar ();
			return true;
		}

		return false;
	}

  //! Go to the next char.
	inline bool AdvanceLastChar (bool *&isNotEof, TCHAR *&lastChar)
	{
		bool r = AdvanceLastChar ();
		lastChar = &cfgs[cfgPtr]._lastChar;
		isNotEof = &cfgs[cfgPtr]._isNotEof;
		return r;
	}

  //! Go to the next char.
	inline bool AdvanceLastChar ()
	{
		bool &isNotEof = cfgs[cfgPtr]._isNotEof;
		if (!isNotEof)
		{
			if (cfgPtr == 0)
			{
				return false;
			}

			delete cfgs[cfgPtr]._input;
			cfgs[cfgPtr--]._input = NULL;
			return AdvanceLastChar ();
		}

		CRscEditorInput *&input = cfgs[cfgPtr]._input;
		TCHAR &lastChar = cfgs[cfgPtr]._lastChar;
		if (!(isNotEof = input->Read (&lastChar, sizeof (TCHAR)) == sizeof (TCHAR)))
		{
			return AdvanceLastChar ();
		}

		return true;
	}

  //! Get the next token in the input.
	void GetNextToken (CRscEditorToken *token, CRscEditorParser *parser, unsigned short flags)
	{
		TCHAR *lastChar = &cfgs[cfgPtr]._lastChar;
		bool *isNotEof = &cfgs[cfgPtr]._isNotEof;

		if (*isNotEof)
		{
			if ((flags & flagSkipWhiteChars) != 0)
			{
				SkipWhiteSpaces (flags, isNotEof, lastChar, NULL);
			}

			if (*isNotEof)
			{
				char *p;

				switch (*lastChar)
				{
				case _T ('/'):
					token->string[0] = *lastChar;
					if (AdvanceLastChar (isNotEof, lastChar))
					{
						switch (*lastChar)
						{
						case _T ('/'):
							token->string[1] = *lastChar;
							token->string[2] = _T ('\0');
							token->type = rscEditorLex_slashSlash;
							AdvanceLastChar (isNotEof, lastChar);
							if ((flags & flagSlashSlashComment) != 0 && parser != NULL)
							{
								if (!parser->ParseSlashSlash (this))
								{
									ASSERT (0);
									token->type = rscEditorLexError;
									return;
								}
								if ((flags & flagSkipWhiteSpaces) != 0)
								{
									GetNextToken (token, parser, flags);
								}
								else
								{
									token->string[0] = token->type = _T (' ');
									token->string[1] = _T ('\0');
								}
							}
							return;

						case _T ('*'):
							token->string[1] = *lastChar;
							token->string[2] = _T ('\0');
							token->type = rscEditorLex_slashStar;
							AdvanceLastChar (isNotEof, lastChar);
							if ((flags & flagSlashStarComment) != 0 && parser != NULL)
							{
								if (!parser->ParseSlashStar (this))
								{
									ASSERT (0);
									token->type = rscEditorLexError;
									return;
								}
								if ((flags & flagSkipWhiteSpaces) != 0)
								{
									GetNextToken (token, parser, flags);
								}
								else
								{
									token->string[0] = token->type = _T (' ');
									token->string[1] = _T ('\0');
								}
							}
							return;
						}
					}
					token->string[1] = _T ('\0');
					token->type = _T ('/');
					return;

				case _T ('*'):
					token->string[0] = *lastChar;
					AdvanceLastChar (isNotEof, lastChar);
					if (*isNotEof && *lastChar == _T ('/'))
					{
						token->string[1] = *lastChar;
						token->string[2] = _T ('\0');
						token->type = rscEditorLex_starSlash;
						AdvanceLastChar (isNotEof, lastChar);
						return;
					}
					token->string[1] = _T ('\0');
					token->type = _T ('*');
					return;

				case _T ('\\'):
					{
						AdvanceLastChar (isNotEof, lastChar);
						CString str;
						SkipWhiteSpaces (flags | flagSkipWhiteSpaces, isNotEof, lastChar, &str);
						if (*lastChar == '\r' || *lastChar == '\n')
						{
							SkipWhiteSpaces (flags | flagSkipWhiteChars, isNotEof, lastChar, NULL);
							if ((flags & flagSkipWhiteSpaces) == 0)
							{
								token->string[0] = _T (' ');
								token->string[1] = _T ('\0');
								token->type = _T (' ');
							}
							else
							{
								GetNextToken (token, parser, flags);
							}
						}
						else
						{
							Unread ((LPCTSTR) str, str.GetLength () * sizeof (TCHAR));
							token->type = '\\';
							_tcscpy (token->string, _T ("\\"));
						}
					}
					return;

				case _T ('#'):
					token->string[0] = *lastChar;
					token->string[1] = _T ('\0');
					token->type = *lastChar;
					AdvanceLastChar (isNotEof, lastChar);
					if ((flags & flagHashCallback) != 0 && parser != NULL)
					{
						if (!parser->ParseHash (this, token, flags))
						{
							ASSERT (0);
							token->type = rscEditorLexError;
							return;
						}
					}
					return;

				case _T ('{'):
				case _T ('}'):
				case _T (','):
				case _T (';'):
				case _T (':'):
				case _T ('?'):
				case _T ('['):
				case _T (']'):
				case _T ('<'):
				case _T ('>'):
				case _T ('$'):
				case _T ('@'):
				case _T ('+'):
				case _T ('-'):
				case _T ('%'):
				case _T ('!'):
				case _T ('^'):
				case _T ('~'):
				case _T ('('):
				case _T (')'):
				case _T (' '):
				case _T ('\t'):
				case _T ('\r'):
				case _T ('\n'):
				case _T ('\''):
				case _T ('`'):
					token->string[0] = *lastChar;
					token->string[1] = _T ('\0');
					token->type = *lastChar;
					AdvanceLastChar (isNotEof, lastChar);
					return;
					
				case _T ('='):
					token->string[0] = *lastChar;
					AdvanceLastChar (isNotEof, lastChar);
					if (*isNotEof && *lastChar == _T ('='))
					{
						token->string[1] = *lastChar;
						token->string[2] = _T ('\0');
						token->type = rscEditorLex_equal;
						AdvanceLastChar (isNotEof, lastChar);
						return;
					}
					token->string[1] = _T ('\0');
					token->type = _T ('=');
					return;

				case _T ('&'):
					token->string[0] = *lastChar;
					AdvanceLastChar (isNotEof, lastChar);
					if (*isNotEof && *lastChar == _T ('&'))
					{
						token->string[1] = *lastChar;
						token->string[2] = _T ('\0');
						token->type = rscEditorLex_and;
						AdvanceLastChar (isNotEof, lastChar);
						return;
					}
					token->string[1] = _T ('\0');
					token->type = _T ('&');
					return;

				case _T ('|'):
					token->string[0] = *lastChar;
					AdvanceLastChar (isNotEof, lastChar);
					if (*isNotEof && *lastChar == _T ('|'))
					{
						token->string[1] = *lastChar;
						token->string[2] = _T ('\0');
						token->type = rscEditorLex_or;
						AdvanceLastChar (isNotEof, lastChar);
						return;
					}
					token->string[1] = _T ('\0');
					token->type = _T ('|');
					return;

				case _T ('.'):
					ParseNumber (token);
					return;

				case _T ('"'):
					p = &token->string[0];
					*p++ = *lastChar;
					token->type = rscEditorLexString;
					AdvanceLastChar (isNotEof, lastChar);
					while (*isNotEof)
					{
						if (*lastChar != _T ('"'))
						{
							*p++ = *lastChar;
							AdvanceLastChar (isNotEof, lastChar);
							continue;
						}
						*p++ = *lastChar;
						*p = _T ('\0');
						AdvanceLastChar (isNotEof, lastChar);
						return;
					}
					ASSERT (0);
					token->type = rscEditorLexError;
					return;
					
				default:
					if (*lastChar >= _T ('0') && *lastChar <= _T ('9'))
					{
						ParseNumber (token);
						return;
					}
					
					if ((*lastChar >= _T ('a') && *lastChar <= _T ('z')) || 
						(*lastChar >= _T ('A') && *lastChar <= _T ('Z')) ||
						*lastChar == _T ('_'))
					{
						p = &token->string[0];
						*p++ = *lastChar;
						AdvanceLastChar (isNotEof, lastChar);
						while (*isNotEof && ((*lastChar >= _T ('a') && *lastChar <= _T ('z')) ||
							(*lastChar >= _T ('A') && *lastChar <= _T ('Z')) ||
							(*lastChar >= _T ('0') && *lastChar <= _T ('9')) ||
							*lastChar == _T ('_')))
						{
							*p++ = *lastChar;
							AdvanceLastChar (isNotEof, lastChar);
						}
						*p = 0x00;
						FindKeyword (token, parser, flags);
						return;
					}

					// ASSERT (0);
					// token->type = rscEditorLexError;
					token->string[0] = *lastChar;
					token->string[1] = _T ('\0');
					token->type = *lastChar;
					AdvanceLastChar (isNotEof, lastChar);
					return;
				}
			}
		}

		if (cfgPtr > 0)
		{
			delete cfgs[cfgPtr]._input;
			cfgs[cfgPtr--]._input = NULL;
			GetNextToken (token, parser, flags);
			return;
		}
		token->type = rscEditorLexEof;
		return;
	}

	inline void SkipWhiteSpaces (unsigned short flags, bool *&isNotEof, TCHAR *&lastChar, CString *str)
	{
		while (*isNotEof)
		{
			if (*lastChar == _T (' ') || *lastChar == _T ('\r') || *lastChar == _T ('\t') ||
				(*lastChar == _T ('\n') && (flags & flagSkipNewlines) != 0))
			{
				if (str != NULL)
				{
					*str += *lastChar;
				}
				AdvanceLastChar (isNotEof, lastChar);
				continue;
			}
			
			return;
		}
	}

	void Unread (const void *buf, unsigned int num)
	{
		CRscEditorInput *&input = cfgs[cfgPtr]._input;
		TCHAR &lastChar = cfgs[cfgPtr]._lastChar;
		bool &isNotEof = cfgs[cfgPtr]._isNotEof;

		if (isNotEof)
		{
			input->Unread (&lastChar, sizeof (TCHAR));
		}
		input->Unread (buf, num);
		isNotEof |= (num > 0);
		AdvanceLastChar ();
	}

	unsigned int GetLineNumber () const
	{
		return cfgs[0]._input->GetLineNumber ();
	}

  //! Add new input into the stack of inputs.
	bool PushInput (CRscEditorInput *input)
	{
		if (++cfgPtr < maxInputs)
		{
			cfgs[cfgPtr]._input = input;
			cfgs[cfgPtr]._isNotEof = input->Read (&cfgs[cfgPtr]._lastChar, sizeof (cfgs[0]._lastChar)) == sizeof (cfgs[0]._lastChar);
			return true;
		}

		--cfgPtr;
		return false;
	}

protected:
  //! Check if the token is not a keyword.
	void FindKeyword (CRscEditorToken *token, CRscEditorParser *parser, unsigned short flags)
	{
		const CRscEditorKeyword *key = keywords;
		for (int i = keywordsLength; i > 0; --i)
		{
			if (0 == strcmp (key->token, &token->string[0]))
			{
				token->type = key->id;
				return;
			}

			++key;
		}
		token->type = rscEditorLexId;

		if ((flags & flagMacroCallback) != 0 && parser != NULL)
		{
			if (!parser->ProcessMacroId (this, token, flags))
			{
				ASSERT (0);
				token->type = rscEditorLexError;
				return;
			}
		}
	}

  //! Parse number (hex or dec or float).
	inline void ParseNumber (CRscEditorToken *token)
	{
		TCHAR *lastChar = &cfgs[cfgPtr]._lastChar;
		bool *isNotEof = &cfgs[cfgPtr]._isNotEof;

		token->number = 0;
		TCHAR *p = &token->string[0];
		while (*isNotEof && *lastChar >= _T ('0') && *lastChar <= _T ('9'))
		{
			*p++ = *lastChar;
			token->number *= 10;
			token->number += *lastChar - _T ('0');
			AdvanceLastChar (isNotEof, lastChar);
		}

		if (!*isNotEof || *lastChar != _T ('.'))
		{
			*p = _T ('\0');
			token->fnumber = (double) token->number;
			token->type = rscEditorLexLong;
			return;
		}

		token->fnumber = (double) token->number;
		token->number <<= 16;
		token->type = rscEditorLexFixedFloat;

		*p++ = *lastChar;
		AdvanceLastChar (isNotEof, lastChar);
		long part = 0;
		long exp = 1;
		while (*isNotEof && *lastChar >= _T ('0') && *lastChar <= _T ('9'))
		{
			*p++ = *lastChar;
			part *= 10;
			part += *lastChar - _T ('0');
			exp *= 10;
			AdvanceLastChar (isNotEof, lastChar);
		}

		*p = _T ('\0');
		token->fnumber += ((double) part / (double) exp);
		CalcDecimalPart (token, part, exp);
	}

  //! Calculate decimal part of a float value.
	inline void CalcDecimalPart (CRscEditorToken *token, long num, long exp)
	{
		long bit = 0x10000;
		int i = 16;

		while (num > 0 && i > 0)
		{
			num *= 2;
			bit >>= 1;
			if (num >= exp)
			{
				token->number |= bit;
				num %= exp;
			}
			--i;
		}
	}
};


#endif // #ifndef _RSCEDITORLEX_H_
