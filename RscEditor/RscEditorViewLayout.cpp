
#include "stdafx.h"

#include "RscEditor.h"
#include "RscEditorItems.h"
#include "RscMacro.h"
#include "RscEditorDoc.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"
#include "RscEditorREDParser.h"
#include "RscEditorView.h"
#include "RscEditorPreview.h"
#include "RscIDEditor.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

void CRscEditorView::OnLayoutAlignBottom() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 2)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Align Bottom"), true, UNDO_ALIGN_BOTTOM))
	{
		double n[LAYOUT_PARAMS_SIZE] = {0.0, 0.0, 0.0, 0.0};
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::AlignBottomCompFce, &CRscEditorDoc::AlignBottomSetFce, NULL);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutAlignBottom(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size < 2)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutAlignRight() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 2)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Align Right"), true, UNDO_ALIGN_RIGHT))
	{
		double n[LAYOUT_PARAMS_SIZE] = {0.0, 0.0, 0.0, 0.0};
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::AlignRightCompFce, &CRscEditorDoc::AlignRightSetFce, NULL);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutAlignRight(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size < 2)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutAlignLeft() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 2)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Align Left"), true, UNDO_ALIGN_LEFT))
	{
		double n[LAYOUT_PARAMS_SIZE] = {1.0, 1.0, 1.0, 1.0};
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::AlignLeftCompFce, &CRscEditorDoc::AlignLeftSetFce, NULL);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutAlignLeft(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size < 2)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutAlignTop() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 2)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Align Top"), true, UNDO_ALIGN_TOP))
	{
		double n[LAYOUT_PARAMS_SIZE] = {1.0, 1.0, 1.0, 1.0};
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::AlignTopCompFce, &CRscEditorDoc::AlignTopSetFce, NULL);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutAlignTop(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size < 2)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutAlignHorizCenter() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 2)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Align Hor. Center"), true, UNDO_ALIGN_HOR_CENTER))
	{
		double n[LAYOUT_PARAMS_SIZE] = {-1.0, -1.0, -1.0, -1.0};
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::AlignHorizCenterCompFce, &CRscEditorDoc::AlignHorizCenterSetFce, NULL);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutAlignHorizCenter(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size < 2)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutAlignVertCenter() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 2)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Align Ver. Center"), true, UNDO_ALIGN_VER_CENTER))
	{
		double n[LAYOUT_PARAMS_SIZE] = {-1.0, -1.0, -1.0, -1.0};
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::AlignVertCenterCompFce, &CRscEditorDoc::AlignVertCenterSetFce, NULL);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutAlignVertCenter(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size < 2)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutMakeSameSizeWidth() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 2)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Same Width"), true, UNDO_SAME_WIDTH))
	{
		double n[LAYOUT_PARAMS_SIZE] = {-1.0, -1.0, -1.0, -1.0};
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::MakeSameSizeWidthCompFce, &CRscEditorDoc::MakeSameSizeWidthSetFce, NULL);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutMakeSameSizeWidth(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size < 2)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutMakeSameSizeHeight() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 2)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Same Height"), true, UNDO_SAME_HEIGHT))
	{
		double n[LAYOUT_PARAMS_SIZE] = {-1.0, -1.0, -1.0, -1.0};
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::MakeSameSizeHeightCompFce, &CRscEditorDoc::MakeSameSizeHeightSetFce, NULL);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutMakeSameSizeHeight(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size < 2)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutMakeSameSizeBoth() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 2)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Same Size"), true, UNDO_SAME_SIZE))
	{
		double n[LAYOUT_PARAMS_SIZE] = {-1.0, -1.0, -1.0, -1.0};
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::MakeSameSizeBothCompFce, &CRscEditorDoc::MakeSameSizeBothSetFce, NULL);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutMakeSameSizeBoth(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size < 2)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutCenterInDialogHorizontal() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 1)
	{
		return;
	}

	double n[LAYOUT_PARAMS_SIZE] = {1.0, 0.0, 0.5, 0.0};
	double x, w;
	if (doc->_monitorClass != NULL &&
		doc->_monitorClass->GetFloatParamInBases ("x", x, true, NULL, false) &&
		doc->_monitorClass->GetFloatParamInBases ("w", w, true, NULL, false))
	{
		n[2] = (x + x + w) / 2.0;
	}
	if (doc->OpenCommand (_T ("Center Hor."), true, UNDO_CENTER_HOR))
	{
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::CenterInDialogHorizCompFce, &CRscEditorDoc::CenterInDialogHorizSetFce, NULL);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutCenterInDialogHorizontal(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size == 0)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutCenterInDialogVertical() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 1)
	{
		return;
	}

	double n[LAYOUT_PARAMS_SIZE] = {1.0, 0.0, 0.5, 0.0};
	double y, h;
	if (doc->_monitorClass != NULL &&
		doc->_monitorClass->GetFloatParamInBases ("y", y, true, NULL, false) &&
		doc->_monitorClass->GetFloatParamInBases ("h", h, true, NULL, false))
	{
		n[2] = (y + y + h) / 2.0;
	}
	if (doc->OpenCommand (_T ("Center Ver."), true, UNDO_CENTER_VER))
	{
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::CenterInDialogVerticalCompFce, &CRscEditorDoc::CenterInDialogVerticalSetFce, NULL);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutCenterInDialogVertical(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size == 0)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutSpaceEvenlyAcross() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 1)
	{
		return;
	}

	double n[LAYOUT_PARAMS_SIZE] = {0.0, 1.0, 1.0, 0.0};
	double x, w;
	if (doc->_monitorClass != NULL &&
		doc->_monitorClass->GetFloatParamInBases ("x", x, true, NULL, false) &&
		doc->_monitorClass->GetFloatParamInBases ("w", w, true, NULL, false))
	{
		n[0] = x;
		n[1] = w;
	}
	if (doc->OpenCommand (_T ("Space Evenly Across Dialog"), true, UNDO_SPACE_EVENLY_ACROSS_DIALOG))
	{
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::SpaceEvenlyAcrossDialogCompFce, &CRscEditorDoc::SpaceEvenlyAcrossDialogSetFce, &CRscEditorDoc::SortLeft2Right);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutSpaceEvenlyAcross(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size == 0)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutSpaceEvenlyDown() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 1)
	{
		return;
	}

	double n[LAYOUT_PARAMS_SIZE] = {0.0, 1.0, 1.0, 0.0};
	double y, h;
	if (doc->_monitorClass != NULL &&
		doc->_monitorClass->GetFloatParamInBases ("y", y, true, NULL, false) &&
		doc->_monitorClass->GetFloatParamInBases ("h", h, true, NULL, false))
	{
		n[0] = y;
		n[1] = h;
	}
	if (doc->OpenCommand (_T ("Space Evenly Down Dialog"), true, UNDO_SPACE_EVENLY_DOWN_DIALOG))
	{
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::SpaceEvenlyDownDialogCompFce, &CRscEditorDoc::SpaceEvenlyDownDialogSetFce, &CRscEditorDoc::SortTop2Down);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutSpaceEvenlyDown(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size == 0)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutSpaceEvenlyAcrossSelect() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 3)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Space Evenly Across Selection"), true, UNDO_SPACE_EVENLY_ACROSS_SELECTION))
	{
		double n[LAYOUT_PARAMS_SIZE] = {1.0, 0.0, 0.0, 0.0, 1.0};
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::SpaceEvenlyAcrossSelectionCompFce, &CRscEditorDoc::SpaceEvenlyAcrossSelectionSetFce, &CRscEditorDoc::SortLeft2Right);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutSpaceEvenlyAcrossSelect(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size < 3)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutSpaceEvenlyDownSelect() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 3)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Space Evenly Down Selection"), true, UNDO_SPACE_EVENLY_DOWN_SELECTION))
	{
		double n[LAYOUT_PARAMS_SIZE] = {1.0, 0.0, 0.0, 0.0, 1.0};
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::SpaceEvenlyDownSelectionCompFce, &CRscEditorDoc::SpaceEvenlyDownSelectionSetFce, &CRscEditorDoc::SortTop2Down);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutSpaceEvenlyDownSelect(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size < 3)
	{
		pCmdUI->Enable (FALSE);
	}
}


void CRscEditorView::OnLayoutSideBySideTop() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 2)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Align Side By Side Top"), true, UNDO_SIDE_BY_SIDE_TOP))
	{
		double n[LAYOUT_PARAMS_SIZE] = {1.0, 0.0};
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::SideBySideTopCompFce, &CRscEditorDoc::SideBySideTopSetFce, &CRscEditorDoc::SortTop2Down);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutSideBySideTop(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size < 2)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutSideBySideLeft() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 2)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Align Side By Side Left"), true, UNDO_SIDE_BY_SIDE_LEFT))
	{
		double n[LAYOUT_PARAMS_SIZE] = {1.0, 0.0};
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::SideBySideLeftCompFce, &CRscEditorDoc::SideBySideLeftSetFce, &CRscEditorDoc::SortLeft2Right);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutSideBySideLeft(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size < 2)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutSideBySideRight() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 2)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Align Side By Side Right"), true, UNDO_SIDE_BY_SIDE_RIGHT))
	{
		double n[LAYOUT_PARAMS_SIZE] = {1.0, 0.0};
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::SideBySideRightCompFce, &CRscEditorDoc::SideBySideRightSetFce, &CRscEditorDoc::SortRight2Left);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutSideBySideRight(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size < 2)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnLayoutSideBySideBottom() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () || 
		size < 2)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Align Side By Side Bottom"), true, UNDO_SIDE_BY_SIDE_BOTTOM))
	{
		double n[LAYOUT_PARAMS_SIZE] = {1.0, 0.0};
		doc->OnLayoutGeneric (n, _changeInherited, &CRscEditorDoc::SideBySideBottomCompFce, &CRscEditorDoc::SideBySideBottomSetFce, &CRscEditorDoc::SortDown2Top);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateLayoutSideBySideBottom(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->CreateLayoutUnits (NULL, doc->_selectedClasses);
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		size < 2)
	{
		pCmdUI->Enable (FALSE);
	}
}
