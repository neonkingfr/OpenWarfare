// RscEditorView.cpp : implementation of the CRscEditorView class
//

#include "stdafx.h"

#include "RscEditor.h"
#include "MainFrm.h"
#include "RscEditorItems.h"
#include "RscMacro.h"
#include "RscEditorDoc.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"
#include "RscEditorREDParser.h"
#include "RscEditorView.h"
#include "RscEditorPreview.h"
#include "RscIDEditor.h"
#include "PreferencesDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

UINT WM_FINDREPLACE = ::RegisterWindowMessage(FINDMSGSTRING);;
const char *CRscEditorView::_itemGroups[rscEditorItemGroupNumber] = {"objects", "controls", "controlsBackground"};

IMPLEMENT_DYNCREATE(CRscEditorView, CFormView)

BEGIN_MESSAGE_MAP(CRscEditorView, CFormView)
	//{{AFX_MSG_MAP(CRscEditorView)
	ON_WM_SIZE()
	ON_COMMAND(ID_CLASS_BASE, OnClassBase)
	ON_UPDATE_COMMAND_UI(ID_CLASS_BASE, OnUpdateClassBase)
	ON_COMMAND(ID_CLASS_NEW_INNER, OnClassNewInner)
	ON_UPDATE_COMMAND_UI(ID_CLASS_NEW_INNER, OnUpdateClassNewInner)
	ON_COMMAND(ID_CLASS_NEW_DERIVED, OnClassNewDerived)
	ON_UPDATE_COMMAND_UI(ID_CLASS_NEW_DERIVED, OnUpdateClassNewDerived)
	ON_COMMAND(ID_CLASS_FAVOURITES_ADD, OnClassFavouritesAdd)
	ON_UPDATE_COMMAND_UI(ID_CLASS_FAVOURITES_ADD, OnUpdateClassFavouritesAdd)
	ON_COMMAND(ID_CLASS_FAVOURITES_REMOVE, OnClassFavouritesRemove)
	ON_UPDATE_COMMAND_UI(ID_CLASS_FAVOURITES_REMOVE, OnUpdateClassFavouritesRemove)
	ON_COMMAND(ID_CLASS_SELECT_ORIGINAL, OnClassSelectOriginal)
	ON_UPDATE_COMMAND_UI(ID_CLASS_SELECT_ORIGINAL, OnUpdateClassSelectOriginal)
	ON_COMMAND(ID_CLASS_EDIT, OnClassEdit)
	ON_UPDATE_COMMAND_UI(ID_CLASS_EDIT, OnUpdateClassEdit)
	ON_COMMAND(ID_CLASS_SELECT, OnClassSelect)
	ON_UPDATE_COMMAND_UI(ID_CLASS_SELECT, OnUpdateClassSelect)
	ON_COMMAND(ID_CLASS_SELECT_ADD, OnClassSelectAdd)
	ON_UPDATE_COMMAND_UI(ID_CLASS_SELECT_ADD, OnUpdateClassSelectAdd)
	ON_COMMAND(ID_FILE_INCLUDE, OnFileInclude)
	ON_COMMAND(ID_LAYOUT_ALIGN_BOTTOM, OnLayoutAlignBottom)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_ALIGN_BOTTOM, OnUpdateLayoutAlignBottom)
	ON_COMMAND(ID_LAYOUT_ALIGN_RIGHT, OnLayoutAlignRight)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_ALIGN_RIGHT, OnUpdateLayoutAlignRight)
	ON_COMMAND(ID_LAYOUT_ALIGN_LEFT, OnLayoutAlignLeft)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_ALIGN_LEFT, OnUpdateLayoutAlignLeft)
	ON_COMMAND(ID_LAYOUT_ALIGN_TOP, OnLayoutAlignTop)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_ALIGN_TOP, OnUpdateLayoutAlignTop)
	ON_COMMAND(ID_LAYOUT_ALIGN_HORIZCENTER, OnLayoutAlignHorizCenter)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_ALIGN_HORIZCENTER, OnUpdateLayoutAlignHorizCenter)
	ON_COMMAND(ID_LAYOUT_ALIGN_VERTCENTER, OnLayoutAlignVertCenter)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_ALIGN_VERTCENTER, OnUpdateLayoutAlignVertCenter)
	ON_COMMAND(ID_LAYOUT_MAKESAMESIZE_WIDTH, OnLayoutMakeSameSizeWidth)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_MAKESAMESIZE_WIDTH, OnUpdateLayoutMakeSameSizeWidth)
	ON_COMMAND(ID_LAYOUT_MAKESAMESIZE_HEIGHT, OnLayoutMakeSameSizeHeight)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_MAKESAMESIZE_HEIGHT, OnUpdateLayoutMakeSameSizeHeight)
	ON_COMMAND(ID_LAYOUT_MAKESAMESIZE_BOTH, OnLayoutMakeSameSizeBoth)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_MAKESAMESIZE_BOTH, OnUpdateLayoutMakeSameSizeBoth)
	ON_COMMAND(ID_CLASS_DELETE, OnClassDelete)
	ON_UPDATE_COMMAND_UI(ID_CLASS_DELETE, OnUpdateClassDelete)
	ON_COMMAND(ID_ITEMS_SHOWCONTROLS, OnItemsShowControls)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_SHOWCONTROLS, OnUpdateItemsShowControls)
	ON_COMMAND(ID_ITEMS_SHOWBACKGROUNDCONTROLS, OnItemsShowBackgroundControls)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_SHOWBACKGROUNDCONTROLS, OnUpdateItemsShowBackgroundControls)
	ON_COMMAND(ID_ITEMS_EDIT, OnItemsEdit)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_EDIT, OnUpdateItemsEdit)
	ON_COMMAND(ID_ITEMS_NEWCOPY, OnItemsNewCopy)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_NEWCOPY, OnUpdateItemsNewCopy)
	ON_COMMAND(ID_ITEMS_NEWDERIVED, OnItemsNewDerived)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_NEWDERIVED, OnUpdateItemsNewDerived)
	ON_COMMAND(ID_ITEMS_MAKEINDEPENDENT, OnItemsMakeIndependent)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_MAKEINDEPENDENT, OnUpdateItemsMakeIndependent)
	ON_COMMAND(ID_LAYOUT_CENTERINDIALOG_HORIZONTAL, OnLayoutCenterInDialogHorizontal)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_CENTERINDIALOG_HORIZONTAL, OnUpdateLayoutCenterInDialogHorizontal)
	ON_COMMAND(ID_LAYOUT_CENTERINDIALOG_VERTICAL, OnLayoutCenterInDialogVertical)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_CENTERINDIALOG_VERTICAL, OnUpdateLayoutCenterInDialogVertical)
	ON_COMMAND(ID_LAYOUT_SPACEEVENLY_ACROSS, OnLayoutSpaceEvenlyAcross)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_SPACEEVENLY_ACROSS, OnUpdateLayoutSpaceEvenlyAcross)
	ON_COMMAND(ID_LAYOUT_SPACEEVENLY_DOWN, OnLayoutSpaceEvenlyDown)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_SPACEEVENLY_DOWN, OnUpdateLayoutSpaceEvenlyDown)
	ON_WM_MOUSEMOVE()
	ON_COMMAND(ID_ITEMS_SHOWOBJECTS, OnItemsShowObjects)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_SHOWOBJECTS, OnUpdateItemsShowObjects)
	ON_BN_CLICKED(IDC_MONITORLABEL, OnMonitorLabel)
	ON_COMMAND(ID_ITEMS_NEW, OnItemsNew)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_NEW, OnUpdateItemsNew)
	ON_COMMAND(ID_ITEMS_CHANGEINHERITED, OnItemsChangeInherited)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_CHANGEINHERITED, OnUpdateItemsChangeInherited)
	ON_COMMAND(ID_ITEMS_REMOVE, OnItemsRemove)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_REMOVE, OnUpdateItemsRemove)
	ON_COMMAND(ID_ITEMS_DELETE, OnItemsDelete)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_DELETE, OnUpdateItemsDelete)
	ON_COMMAND(ID_CLASS_NEW, OnClassNew)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateEditCut)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)
	ON_COMMAND(ID_ITEMS_EXTEDIT, OnItemsExtEdit)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_EXTEDIT, OnUpdateItemsExtEdit)
	ON_COMMAND(ID_FILE_BULDOZER, OnFileBuldozer)
	ON_UPDATE_COMMAND_UI(ID_FILE_BULDOZER, OnUpdateFileBuldozer)
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
	ON_COMMAND(ID_EDIT_REDO, OnEditRedo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_REDO, OnUpdateEditRedo)
	ON_COMMAND(ID_EDIT_CLEARHISTORY, OnEditClearHistory)
	ON_COMMAND(ID_ITEMS_BACKWARD, OnItemsBackward)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_BACKWARD, OnUpdateItemsBackward)
	ON_COMMAND(ID_ITEMS_FORWARD, OnItemsForward)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_FORWARD, OnUpdateItemsForward)
	ON_COMMAND(ID_ITEMS_BACK, OnItemsBack)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_BACK, OnUpdateItemsBack)
	ON_COMMAND(ID_ITEMS_FRONT, OnItemsFront)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_FRONT, OnUpdateItemsFront)
	ON_COMMAND(ID_EDIT_SELECT_ALL, OnEditSelectAll)
	ON_UPDATE_COMMAND_UI(ID_EDIT_SELECT_ALL, OnUpdateEditSelectAll)
	ON_COMMAND(ID_ITEMS_GROUP, OnItemsGroup)
	ON_UPDATE_COMMAND_UI(ID_ITEMS_GROUP, OnUpdateItemsGroup)
	ON_COMMAND(ID_LAYOUT_SHOWGRID, OnLayoutShowGrid)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_SHOWGRID, OnUpdateLayoutShowGrid)
	ON_COMMAND(ID_LAYOUT_SHOWGUIDES, OnLayoutShowGuides)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_SHOWGUIDES, OnUpdateLayoutShowGuides)
	ON_COMMAND(ID_LAYOUT_SNAPTOGRID, OnLayoutSnapToGrid)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_SNAPTOGRID, OnUpdateLayoutSnapToGrid)
	ON_COMMAND(ID_LAYOUT_SNAPTOGUIDES, OnLayoutSnapToGuides)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_SNAPTOGUIDES, OnUpdateLayoutSnapToGuides)
	ON_COMMAND(ID_EDIT_PREFERENCES, OnEditPreferences)
	ON_COMMAND(ID_LAYOUT_DELETEGUIDES, OnLayoutDeleteGuides)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_DELETEGUIDES, OnUpdateLayoutDeleteGuides)
	ON_COMMAND(ID_LAYOUT_TOGGLE_GRID_GUIDES, OnLayoutToggleGridGuides)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_TOGGLE_GRID_GUIDES, OnUpdateLayoutToggleGridGuides)
	ON_COMMAND(ID_CLASS_EDITMACROS, OnClassEditMacros)
	ON_COMMAND(ID_CLASS_EXTEDIT, OnClassExtEdit)
	ON_UPDATE_COMMAND_UI(ID_CLASS_EXTEDIT, OnUpdateClassExtEdit)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_MOVELEFT, OnUpdateLayoutMoveLeft)
	ON_COMMAND(ID_LAYOUT_MOVELEFT, OnLayoutMoveLeft)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_MOVERIGHT, OnUpdateLayoutMoveRight)
	ON_COMMAND(ID_LAYOUT_MOVERIGHT, OnLayoutMoveRight)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_MOVEUP, OnUpdateLayoutMoveUp)
	ON_COMMAND(ID_LAYOUT_MOVEUP, OnLayoutMoveUp)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_MOVEDOWN, OnUpdateLayoutMoveDown)
	ON_COMMAND(ID_LAYOUT_MOVEDOWN, OnLayoutMoveDown)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_FREEX, OnUpdateLayoutFreeX)
	ON_COMMAND(ID_LAYOUT_FREEX, OnLayoutFreeX)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_FREEY, OnUpdateLayoutFreeY)
	ON_COMMAND(ID_LAYOUT_FREEY, OnLayoutFreeY)
	ON_UPDATE_COMMAND_UI(ID_CLASS_SETMONITORCLASS, OnUpdateClassSetMonitorClass)
	ON_COMMAND(ID_CLASS_SETMONITORCLASS, OnClassSetMonitorClass)
	ON_COMMAND(ID_LAYOUT_SPACEEVENLY_ACROSSSELECT, OnLayoutSpaceEvenlyAcrossSelect)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_SPACEEVENLY_ACROSSSELECT, OnUpdateLayoutSpaceEvenlyAcrossSelect)
	ON_COMMAND(ID_LAYOUT_SPACEEVENLY_DOWNSELECT, OnLayoutSpaceEvenlyDownSelect)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_SPACEEVENLY_DOWNSELECT, OnUpdateLayoutSpaceEvenlyDownSelect)
	ON_COMMAND(ID_LAYOUT_SNAPTOITEM, OnLayoutSnapToItem)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_SNAPTOITEM, OnUpdateLayoutSnapToItem)
	ON_COMMAND(ID_LAYOUT_SIDEBYSIDE_TOP, OnLayoutSideBySideTop)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_SIDEBYSIDE_TOP, OnUpdateLayoutSideBySideTop)
	ON_COMMAND(ID_LAYOUT_SIDEBYSIDE_LEFT, OnLayoutSideBySideLeft)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_SIDEBYSIDE_LEFT, OnUpdateLayoutSideBySideLeft)
	ON_COMMAND(ID_LAYOUT_SIDEBYSIDE_RIGHT, OnLayoutSideBySideRight)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_SIDEBYSIDE_RIGHT, OnUpdateLayoutSideBySideRight)
	ON_COMMAND(ID_LAYOUT_SIDEBYSIDE_BOTTOM, OnLayoutSideBySideBottom)
	ON_UPDATE_COMMAND_UI(ID_LAYOUT_SIDEBYSIDE_BOTTOM, OnUpdateLayoutSideBySideBottom)
	ON_COMMAND(ID_EDIT_FIND, OnEditFind)
	ON_COMMAND(ID_EDIT_FINDNEXT, OnEditFindNext)
	ON_REGISTERED_MESSAGE( WM_FINDREPLACE, OnFindMsg )
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_COMMAND(ID_EDIT_UNDOTOSAVE, OnEditUndoToSave)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDOTOSAVE, OnUpdateEditUndoToSave)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CRscEditorView::CRscEditorView()
	: CFormView(CRscEditorView::IDD), _workSpaceImageList (NULL), 
	_classEditor (NULL),
	_curSpaceCtrl (NULL), _dragItem (NULL), _dragSpaceCtrl (NULL),
	_changeInherited (false), _preview (NULL), _lockY (false), _lockX (false)
{
	//{{AFX_DATA_INIT(CRscEditorView)
	//}}AFX_DATA_INIT

	_inclFileSpaceCtrl._view = this;
	_classSpaceCtrl._view = this;
	_favouriteSpaceCtrl._view = this;
	_classEditor._view = this;
	_spaceTabCtrl._view = this;

	_tcscpy (_findWhat, _T (""));
	_tcscpy (_replaceWith, _T (""));
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::EDITOR_FINDWHAT, _findWhat, 255);
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::EDITOR_REPLACEWITH, _replaceWith, 255);
}

CRscEditorView::~CRscEditorView()
{
	if (_workSpaceImageList != NULL)
	{
		delete _workSpaceImageList;
		_workSpaceImageList = NULL;
	}
}

void CRscEditorView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRscEditorView)
	DDX_Control(pDX, IDC_FAVOURITE_SPACE, _favouriteSpaceCtrl);
	DDX_Control(pDX, IDC_CLASS_SPACE, _classSpaceCtrl);
	DDX_Control(pDX, IDC_INCL_FILES_SPACE, _inclFileSpaceCtrl);
	DDX_Control(pDX, IDC_SPACETAB, _spaceTabCtrl);
	DDX_Control(pDX, IDC_MONITORLABEL, _monitorLabel);
	//}}AFX_DATA_MAP
}

BOOL CRscEditorView::PreCreateWindow(CREATESTRUCT& cs)
{
	return CFormView::PreCreateWindow(cs);
}

void CRscEditorView::AddInclFileIntoSpace (HTREEITEM parent, CRscInclude* which)
{
	int image = GetImageNumber (which);
	HTREEITEM child = _inclFileSpaceCtrl.InsertItem (which->_name, image, image, parent);
	if (child == NULL)
	{
		return;
	}
	_inclFileSpaceCtrl.SetItemData (child, (DWORD) which);
	which->_treeItem = child;
	
	int n = which->_subInclude.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		AddInclFileIntoSpace (child, which->_subInclude[i]);
	}
}

void CRscEditorView::AddClassesIntoSpace (HTREEITEM parent, HTREEITEM after, 
										  CArray<CRscParamBase*, CRscParamBase*> &arr)
{
	AddClassesIntoSpace (parent, after, arr, 0, arr.GetSize ());
}

void CRscEditorView::AddClassesIntoSpace (HTREEITEM parent, HTREEITEM after, 
										  CArray<CRscParamBase*, CRscParamBase*> &arr,
										  int from, int to)
{
	for (int i = from; i < to; ++i)
	{
		AddClassIntoSpace (parent, after, arr[i]);
		after = arr[i]->_treeItem;
	}
}

void CRscEditorView::AddClassIntoSpace (HTREEITEM parent, HTREEITEM after, CRscParamBase *par)
{
	CString title;
	par->GetTitle (title);
	
	HTREEITEM child;
	int image = GetImageNumber (par);

	child = _classSpaceCtrl.InsertItem (title, image, image, parent, after);
	if (child == NULL)
	{
		return;
	}
	_classSpaceCtrl.SetItemData (child, (DWORD) par);
	par->_treeItem = child;
	
	if (par->IsClass ())
	{
		AddClassesIntoSpace (par->_treeItem, TVI_LAST, ((CRscClass*) par)->_innerParams);
	}

	return;
}

void CRscEditorView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	if (!::IsWindow (_classEditor.m_hWnd))
	{
		_classEditor.Create (IDD_CLASSEDITOR_DIALOG, NULL);
	}

	// _inclFileSpaceCtrl.DropRegister ();
	// _inclClassSpaceCtrl.DropRegister ();
	_classSpaceCtrl.DropRegister ();
	_favouriteSpaceCtrl.DropRegister ();
	_spaceTabCtrl.DropRegister ();

	CRscEditorDoc *doc = GetDocument ();
	if (_workSpaceImageList == NULL)
	{
		_curSpaceCtrl = &_classSpaceCtrl;
		_inclFileSpaceCtrl.ShowWindow (SW_HIDE);
		_classSpaceCtrl.ShowWindow (SW_SHOW);
		_classSpaceCtrl.SetFocus ();
		_favouriteSpaceCtrl.ShowWindow (SW_HIDE);
	
		_spaceTabCtrl.DeleteAllItems ();
		_spaceTabCtrl.InsertItem (rscEditorInclFileSpace, _T ("Files"));
		_spaceTabCtrl.InsertItem (rscEditorClassSpace, _T ("Classes"));
		_spaceTabCtrl.InsertItem (rscEditorFavouriteSpace, _T ("Favourites"));
		_spaceTabCtrl.SetCurSel (rscEditorClassSpace);
		_spaceTabCtrl.Invalidate (FALSE);
		_spaceTabCtrl.UpdateWindow ();

		_workSpaceImageList = new CImageList ();
		if (_workSpaceImageList != NULL)
		{
			if (_workSpaceImageList->Create (IDR_WORKSPACE, 16, 0, RGB (0xFF, 0xFF, 0xFF)))
			{
				_inclFileSpaceCtrl.SetImageList (_workSpaceImageList, TVSIL_NORMAL);
				_classSpaceCtrl.SetImageList (_workSpaceImageList, TVSIL_NORMAL);
				_favouriteSpaceCtrl.SetImageList (_workSpaceImageList, TVSIL_NORMAL);
			}
			else
			{
				delete _workSpaceImageList;
				_workSpaceImageList = NULL;
			}
		}
	}
	_inclFileSpaceCtrl.SelectItem (NULL);
	_classSpaceCtrl.SelectItem (NULL);
	_favouriteSpaceCtrl.SelectItem (NULL);

	_inclFileSpaceCtrl.DeleteAllItems ();
	_classSpaceCtrl.DeleteAllItems ();
	_favouriteSpaceCtrl.DeleteAllItems ();

	int n = doc->_includedFiles.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		AddInclFileIntoSpace (TVI_ROOT, doc->_includedFiles[i]);
	}
	AddClassesIntoSpace (TVI_ROOT, TVI_LAST, doc->_params);
	n = doc->_favouriteClasses.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		AddFavouriteIntoSpace (TVI_ROOT, TVI_LAST, doc->_favouriteClasses[i]);
	}

	_inclFileSpaceCtrl.Invalidate (FALSE);
	_inclFileSpaceCtrl.UpdateWindow ();
	_classSpaceCtrl.Invalidate (FALSE);
	_classSpaceCtrl.UpdateWindow ();
	_favouriteSpaceCtrl.Invalidate (FALSE);
	_favouriteSpaceCtrl.UpdateWindow ();

	if (doc->_monitorClass != NULL)
	{
		SetSpaceTab (rscEditorClassSpace);
		_classSpaceCtrl.SelectItem (doc->_monitorClass->_treeItem);
		_classSpaceCtrl.SelectSetFirstVisible (doc->_monitorClass->_treeItem);
		if (::IsWindow (_preview->_workViewCtrl.m_hWnd))
		{
			_preview->_workViewCtrl.Invalidate (FALSE);
			_preview->_workViewCtrl.UpdateWindow ();
		}
	}
	SetMonitorLabel ();

	GetParentFrame()->RecalcLayout();
	// ResizeParentToFit();
}

#ifdef _DEBUG
// Diagnostics.
void CRscEditorView::AssertValid() const
{
	CFormView::AssertValid();
}

// Diagnostics.
void CRscEditorView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CRscEditorDoc* CRscEditorView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CRscEditorDoc)));
	return (CRscEditorDoc*)m_pDocument;
}
#endif //_DEBUG

void CRscEditorView::AddFavouriteIntoSpace (HTREEITEM parent, HTREEITEM after, CRscClass *cl)
{
	CString title;
	cl->GetTitle (title);
	
	HTREEITEM favouriteChild;
	int image = GetImageNumber (cl);

	favouriteChild = _favouriteSpaceCtrl.InsertItem (title, image, image, parent, after);
	if (favouriteChild == NULL)
	{
		return;
	}
	_favouriteSpaceCtrl.SetItemData (favouriteChild, (DWORD) cl);
	cl->_favouriteTreeItem = favouriteChild;

	return;
}

void CRscEditorView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	
	if (!::IsWindow (_spaceTabCtrl.m_hWnd))
	{
		return;
	}
	if (!::IsWindow (_monitorLabel.m_hWnd))
	{
		return;
	}
	RECT r1;
	_monitorLabel.GetWindowRect (&r1);

	RECT r;
	r.left = UI_SPACE;
	r.right = cx - UI_SPACE;
	r.top = UI_SPACE;
	r.bottom = UI_SPACE + r1.bottom - r1.top;
	_monitorLabel.MoveWindow (&r, TRUE);

	r.left = 0;
	r.top = UI_SPACE + r1.bottom - r1.top + UI_SPACE;
	r.right = cx;
	r.bottom = cy;
	_spaceTabCtrl.MoveWindow (&r, TRUE);

	if (!::IsWindow (_inclFileSpaceCtrl.m_hWnd) ||
		!::IsWindow (_classSpaceCtrl.m_hWnd) ||
		!::IsWindow (_favouriteSpaceCtrl.m_hWnd))
	{
		return;
	}
	_spaceTabCtrl.AdjustRect (FALSE, &r);
	_inclFileSpaceCtrl.MoveWindow (&r, TRUE);
	_classSpaceCtrl.MoveWindow (&r, TRUE);
	_favouriteSpaceCtrl.MoveWindow (&r, TRUE);

	ShowScrollBar (SB_BOTH, FALSE);

	/*
	if (!::IsWindow (_spaceTabCtrl.m_hWnd))
	{
		return;
	}
	RECT r;
	_spaceTabCtrl.GetWindowRect (&r);
	ScreenToClient (&r);
	if (!::IsWindow (_monitorLabel.m_hWnd))
	{
		return;
	}
	RECT r1;
	_monitorLabel.GetWindowRect (&r1);
	ScreenToClient (&r1);

	r.left = UI_SPACE;
	r.top = UI_SPACE;
	r.bottom = cy - UI_SPACE - UI_SPACE + r1.top - r1.bottom;
	_spaceTabCtrl.MoveWindow (&r, FALSE);

	if (!::IsWindow (_inclFileSpaceCtrl.m_hWnd) ||
		!::IsWindow (_inclClassSpaceCtrl.m_hWnd) ||
		!::IsWindow (_classSpaceCtrl.m_hWnd) ||
		!::IsWindow (_favouriteSpaceCtrl.m_hWnd))
	{
		return;
	}
	RECT r2 = r;
	_spaceTabCtrl.AdjustRect (FALSE, &r2);
	_inclFileSpaceCtrl.MoveWindow (&r2, FALSE);
	_inclClassSpaceCtrl.MoveWindow (&r2, FALSE);
	_classSpaceCtrl.MoveWindow (&r2, FALSE);
	_favouriteSpaceCtrl.MoveWindow (&r2, FALSE);

	r.top = cy - UI_SPACE + r1.top - r1.bottom;
	r.bottom = cy - UI_SPACE;
	_monitorLabel.MoveWindow (&r, FALSE);

	if (!::IsWindow (_dividerCtrl.m_hWnd))
	{
		return;
	}
	r.left = r.right;
	r.right = r.left + UI_SPACE;
	r.top = 0;
	r.bottom = cy;
	_dividerCtrl.MoveWindow (&r, FALSE);

	if (!::IsWindow (_preview->_controlTabCtrl.m_hWnd))
	{
		return;
	}
	if (!::IsWindow (_preview->_itemLabel.m_hWnd))
	{
		return;
	}
	_preview->_itemLabel.GetWindowRect (&r1);
	r.left = r.right;
	r.right = cx - UI_SPACE;
	r.top = UI_SPACE;
	r.bottom = cy - UI_SPACE - UI_SPACE + r1.top - r1.bottom;
	_preview->_controlTabCtrl.MoveWindow (&r, FALSE);

	if (!::IsWindow (_workViewCtrl.m_hWnd))
	{
		return;
	}
	r2 = r;
	_preview->_controlTabCtrl.AdjustRect (FALSE, &r2);
	_workViewCtrl.MoveWindow (&r2, FALSE);

	r.top = cy - UI_SPACE + r1.top - r1.bottom;
	r.bottom = cy - UI_SPACE;
	_preview->_itemLabel.MoveWindow (&r, FALSE);

	ShowScrollBar (SB_BOTH, FALSE);
	*/
}

void CRscEditorView::OnClassBase() 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () || 
		!((CRscParamBase*) wtItem)->IsClass () ||
		((CRscClass*) wtItem)->_baseClass == NULL)
	{
		return;
	}

	CRscClass *bc = ((CRscClass*) wtItem)->_baseClass;
	SetSpaceTab (rscEditorClassSpace);
	_classSpaceCtrl.SelectItem (bc->_treeItem);
}

void CRscEditorView::OnUpdateClassBase(CCmdUI* pCmdUI) 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () || 
		!((CRscParamBase*) wtItem)->IsClass () ||
		((CRscClass*) wtItem)->_baseClass == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}

void CRscEditorView::OnClassNewInner() 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL)
	{
		return;
	}

	if (wtItem->IsParam () && ((CRscParamBase*) wtItem)->IsClass () && ((CRscParamBase*) wtItem)->IsInsideEditable ())
	{
		CRscEditorDoc *doc = GetDocument ();
		if (doc->OpenCommand (_T ("New Inner Class"), true, UNDO_NEW_INNER_CLASS))
		{
			if (doc->NewInnerClass ((CRscClass*) wtItem, ((CRscClass*) wtItem)->_favouriteTreeItem == tcItem) != NULL)
			{
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
				doc->CloseCommand ();
			}
			else
			{
				doc->DisposeCommand ();
			}
		}
	}
}

void CRscEditorView::OnUpdateClassNewInner(CCmdUI* pCmdUI) 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	if (wtItem->IsParam () && ((CRscParamBase*) wtItem)->IsClass () && ((CRscParamBase*) wtItem)->IsInsideEditable ())
	{
		pCmdUI->Enable (TRUE);
		return;
	}

	/*
	if (wtItem->IsClassesFolder ())
	{
		pCmdUI->Enable (TRUE);
		return;
	}

	if (wtItem->IsFavouritesFolder ())
	{
		pCmdUI->Enable (TRUE);
		return;
	}
	*/

	pCmdUI->Enable (FALSE);
}

void CRscEditorView::OnClassNewDerived() 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () || 
		!((CRscParamBase*) wtItem)->IsClass () ||
		((CRscClass*) wtItem)->IsIncluded () && ((CRscClass*) wtItem)->_parent != NULL)
	{
		return;
	}

	CRscEditorDoc *doc = GetDocument ();
	CRscParamBase *where = (CRscParamBase*) wtItem;
	if (where->IsIncluded ())
	{
		where = doc->_params[doc->_params.GetSize () - 1];
	}
	if (doc->OpenCommand (_T ("New Derived Class"), true, UNDO_NEW_DERIVED_CLASS))
	{
		doc->NewDerivedClass ((CRscClass*) wtItem, ((CRscClass*) wtItem)->_parent, where, true, ((CRscClass*) wtItem)->_favouriteTreeItem == tcItem);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateClassNewDerived(CCmdUI* pCmdUI) 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () || 
		!((CRscParamBase*) wtItem)->IsClass () ||
		((CRscClass*) wtItem)->IsIncluded () && ((CRscClass*) wtItem)->_parent != NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}

void CRscEditorView::OnClassFavouritesAdd() 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () || 
		!((CRscParamBase*) wtItem)->IsClass () || 
		((CRscClass*) wtItem)->_favouriteTreeItem != NULL)
	{
		return;
	}

	CRscEditorDoc *doc = GetDocument ();
	if (doc->OpenCommand (_T ("Add Favourite"), false, UNDO_ADD_FAVOURITE))
	{
		int size = doc->_favouriteClasses.GetSize ();
		doc->AddFavourite ((CRscClass*) wtItem, size != 0 ? doc->_favouriteClasses[size - 1] : NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateClassFavouritesAdd(CCmdUI* pCmdUI) 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () || 
		!((CRscParamBase*) wtItem)->IsClass () || 
		((CRscClass*) wtItem)->_favouriteTreeItem != NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}

void CRscEditorView::OnClassFavouritesRemove() 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () || 
		!((CRscParamBase*) wtItem)->IsClass () || 
		((CRscClass*) wtItem)->_favouriteTreeItem == NULL)
	{
		return;
	}

	CRscEditorDoc *doc = GetDocument ();
	if (doc->OpenCommand (_T ("Remove Favourite"), false, UNDO_REMOVE_FAVOURITE))
	{
		if (doc->RemoveFavourite ((CRscClass*) wtItem))
		{
			doc->CloseCommand ();
		}
		else
		{
			doc->DisposeCommand ();
		}
	}
}

void CRscEditorView::OnUpdateClassFavouritesRemove(CCmdUI* pCmdUI) 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () || 
		!((CRscParamBase*) wtItem)->IsClass () || 
		((CRscClass*) wtItem)->_favouriteTreeItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}

void CRscEditorView::OnClassSelectOriginal() 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () || 
		!((CRscParamBase*) wtItem)->IsClass () || 
		((CRscClass*) wtItem)->_favouriteTreeItem != tcItem)
	{
		return;
	}

	SetSpaceTab (rscEditorClassSpace);
	_classSpaceCtrl.SelectItem (((CRscClass*) wtItem)->_treeItem);
}

void CRscEditorView::OnUpdateClassSelectOriginal(CCmdUI* pCmdUI) 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () || 
		!((CRscParamBase*) wtItem)->IsClass () || 
		((CRscClass*) wtItem)->_favouriteTreeItem != tcItem)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}

void CRscEditorView::OnClassEdit() 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam ())
	{
		return;
	}

	LaunchClassEditor ((CRscParamBase*) wtItem, _curSpaceCtrl == &_favouriteSpaceCtrl);
}

void CRscEditorView::UpdateClassEditor (CRscParamBase *cl, bool inFavourites)
{
	_classEditor.InitEditor (cl, inFavourites);
}

void CRscEditorView::UpdateClassEditor ()
{
	_classEditor.InitEditor (_classEditor._param, _classEditor._inFavourites);
}

void CRscEditorView::LaunchClassEditor (CRscParamBase *cl, bool inFavourites)
{
	UpdateClassEditor (cl, inFavourites);
	_classEditor.ShowWindow (SW_SHOW);
	_classEditor.SetActiveWindow ();
}

void CRscEditorView::OnUpdateClassEdit(CCmdUI* pCmdUI) 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam ())
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}

void CRscEditorView::OnClassSelect() 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () || 
		!((CRscParamBase*) wtItem)->IsClass () ||
		!((CRscClass*) wtItem)->_displayed)
	{
		return;
	}

	CRscEditorDoc *doc = GetDocument ();
	doc->SelectAllClasses (false);
	doc->SelectClass ((CRscClass*) wtItem, true);

	_preview->_workViewCtrl.Invalidate (FALSE);
	_preview->_workViewCtrl.UpdateWindow ();
}

void CRscEditorView::OnUpdateClassSelect(CCmdUI* pCmdUI) 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () || 
		!((CRscParamBase*) wtItem)->IsClass () ||
		!((CRscClass*) wtItem)->_displayed)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}

void CRscEditorView::OnClassSelectAdd() 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () || 
		!((CRscParamBase*) wtItem)->IsClass () ||
		!((CRscClass*) wtItem)->_displayed)
	{
		return;
	}

	if (!((CRscClass*) wtItem)->_selected)
	{
		GetDocument ()->SelectClass ((CRscClass*) wtItem, true);
		
		_preview->_workViewCtrl.Invalidate (FALSE);
		_preview->_workViewCtrl.UpdateWindow ();
	}
}

void CRscEditorView::OnUpdateClassSelectAdd(CCmdUI* pCmdUI) 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () || 
		!((CRscParamBase*) wtItem)->IsClass () ||
		!((CRscClass*) wtItem)->_displayed)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}

void CRscEditorView::OnFileInclude() 
{
	static LPCTSTR filter = _T ("All Files (*.*)\0*.*\0"
	                            "RscEditor Files (*.red)\0*.red\0"
		                        "Header Files (*.h;*.hpp)\0*.h;*.hpp\0"
		                        "C++ Files (*.h;*.hpp;*.cpp;*.c)\0*.h;*.hpp;*.cpp;*.c\0");
	CString fileName;
	CFileDialog fileDlg (TRUE, NULL, NULL, OFN_HIDEREADONLY, NULL, this);
	fileDlg.m_ofn.lStructSize = sizeof (fileDlg.m_ofn);
	fileDlg.m_ofn.hwndOwner = this->m_hWnd;
	fileDlg.m_ofn.hInstance = NULL;
	fileDlg.m_ofn.lpstrFilter = filter;
	fileDlg.m_ofn.lpstrCustomFilter = NULL;
	fileDlg.m_ofn.nMaxCustFilter = 0;
	fileDlg.m_ofn.nFilterIndex = 1;
	fileDlg.m_ofn.lpstrFile = fileName.GetBuffer (_MAX_PATH);
	fileDlg.m_ofn.nMaxFile = _MAX_PATH;
	fileDlg.m_ofn.lpstrFileTitle = NULL;
	fileDlg.m_ofn.nMaxFileTitle = 0;
	fileDlg.m_ofn.lpstrInitialDir = NULL;
	fileDlg.m_ofn.lpstrTitle = _T ("Select file to include");
	fileDlg.m_ofn.nFileOffset = 0;
	fileDlg.m_ofn.nFileExtension = 0;
	fileDlg.m_ofn.lpstrDefExt = NULL;
	fileDlg.m_ofn.lCustData = 0;
	fileDlg.m_ofn.lpTemplateName = NULL;

	int id = fileDlg.DoModal ();
	fileName.ReleaseBuffer ();

	if (id != IDOK)
	{
		return;
	}

	CRscEditorDoc *doc = GetDocument ();
	if (doc->OpenCommand (_T ("Include File"), true, UNDO_INCLUDE_FILE))
	{
		if (doc->IncludeFile (fileName))
		{
			doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			doc->CloseCommand ();
		}
		else
		{
			doc->DisposeCommand ();
		}
	}
}

void CRscEditorView::OnClassDelete() 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () || 
		!((CRscParamBase*) wtItem)->IsEditable ())
	{
		return;
	}

	CRscEditorDoc *doc = GetDocument ();
	if (doc->OpenCommand (_T ("Delete Class"), true, UNDO_DELETE_CLASS))
	{
		if (((CRscParamBase*) wtItem)->IsClass ())
		{
			doc->AddCommandEntry (new CUndoSelectClass ((CRscClass*) wtItem, false, ((CRscClass*) wtItem)->_selected));
			doc->DeleteItem ((CRscClass*) wtItem, &_preview->_itemGroupTags[0], false);
		}
		doc->DeleteParam ((CRscParamBase*) wtItem);
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateClassDelete(CCmdUI* pCmdUI) 
{
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam () || 
		!((CRscParamBase*) wtItem)->IsEditable ())
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}

void CRscEditorView::OnItemsShowObjects() 
{
	_preview->_itemGroupTags[rscEditorItemGroupObjects] = !_preview->_itemGroupTags[rscEditorItemGroupObjects];
	_preview->_workViewCtrl.Invalidate (FALSE);
	_preview->_workViewCtrl.UpdateWindow ();
}

void CRscEditorView::OnUpdateItemsShowObjects(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck (_preview->_itemGroupTags[rscEditorItemGroupObjects] ? TRUE : FALSE);
}

void CRscEditorView::OnItemsShowControls() 
{
	_preview->_itemGroupTags[rscEditorItemGroupControls] = !_preview->_itemGroupTags[rscEditorItemGroupControls];
	_preview->_workViewCtrl.Invalidate (FALSE);
	_preview->_workViewCtrl.UpdateWindow ();
}

void CRscEditorView::OnUpdateItemsShowControls(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck (_preview->_itemGroupTags[rscEditorItemGroupControls] ? TRUE : FALSE);
}

void CRscEditorView::OnItemsShowBackgroundControls() 
{
	_preview->_itemGroupTags[rscEditorItemGroupControlsBackground] = !_preview->_itemGroupTags[rscEditorItemGroupControlsBackground];
	_preview->_workViewCtrl.Invalidate (FALSE);
	_preview->_workViewCtrl.UpdateWindow ();
}

void CRscEditorView::OnUpdateItemsShowBackgroundControls(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck (_preview->_itemGroupTags[rscEditorItemGroupControlsBackground] ? TRUE : FALSE);
}

void CRscEditorView::OnItemsEdit() 
{
	if (GetDocument ()->_selectedClasses.GetSize () == 1)
	{
		LaunchClassEditor (GetDocument ()->_selectedClasses[0], false);
	}
}

void CRscEditorView::OnUpdateItemsEdit(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable (GetDocument ()->_selectedClasses.GetSize () == 1 ? TRUE : FALSE);
}

void CRscEditorView::SelectItemClass ()
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_selectedClasses.GetSize () == 1)
	{
		SelectItemClass (doc->_selectedClasses[0]);
	}
}

void CRscEditorView::SelectItemClass (CRscParamBase *cls, bool inFavourites /* = false */)
{
	if (inFavourites && cls->IsClass () && ((CRscClass*) cls)->_favouriteTreeItem != NULL)
	{
		SetSpaceTab (rscEditorFavouriteSpace);
		_favouriteSpaceCtrl.SelectItem (((CRscClass*) cls)->_favouriteTreeItem);
		UpdateClassEditor (cls, true);
	}
	else
	{
		SetSpaceTab (rscEditorClassSpace);
		_classSpaceCtrl.SelectItem (cls->_treeItem);
		UpdateClassEditor (cls, false);
	}
}

void CRscEditorView::OnItemsDelete() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_selectedClasses.GetSize () == 0 ||
		doc->_monitorClass == NULL ||
		!doc->_monitorClass->IsInsideEditable ())
	{
		return;
	}

	if (doc->OpenCommand (_T ("Delete Items"), true, UNDO_DELETE_ITEMS))
	{
		CArray<CRscClass*, CRscClass*> arr;
		arr.Copy (doc->_selectedClasses);
		
		int n = arr.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			doc->AddCommandEntry (new CUndoSelectClass (arr[i], false, arr[i]->_selected));
			if (arr[i]->_valueReferences.GetSize () > 0)
			{
				doc->DeleteItem (arr[i], &_preview->_itemGroupTags[0], true);
			}
			doc->DeleteParam (arr[i]);
		}
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateItemsDelete(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_selectedClasses.GetSize () == 0 ||
		doc->_monitorClass == NULL ||
		!doc->_monitorClass->IsInsideEditable ())
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnItemsRemove() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_selectedClasses.GetSize () == 0 ||
		doc->_monitorClass == NULL ||
		!doc->_monitorClass->IsInsideEditable ())
	{
		return;
	}

	if (doc->OpenCommand (_T ("Remove Items"), true, UNDO_REMOVE_ITEMS))
	{
		int n = doc->_selectedClasses.GetSize ();
		int i = 0;
		while (i < n)
		{
			CRscClass *cl = doc->_selectedClasses[i];
			if (cl->_valueReferences.GetSize () > 0)
			{
				doc->_selectedClasses.RemoveAt (i);
				cl->_selected = false;
				doc->AddCommandEntry (new CUndoSelectClass (cl, false, true));
				doc->RemoveFromGroup (cl);
				doc->DeleteItem (cl, &_preview->_itemGroupTags[0], true);
				--n;
			}
			else
			{
				++i;
			}
		}
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateItemsRemove(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_selectedClasses.GetSize () == 0 ||
		doc->_monitorClass == NULL ||
		!doc->_monitorClass->IsInsideEditable ())
	{
		pCmdUI->Enable (FALSE);
	}

	int n = doc->_selectedClasses.GetSize ();
	int i = 0;
	while (i < n)
	{
		CRscClass *cl = doc->_selectedClasses[i];
		if (cl->_valueReferences.GetSize () > 0)
		{
			return;
		}
		++i;
	}

	pCmdUI->Enable (FALSE);
}

void CRscEditorView::OnItemsNewCopy() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable ())
	{
		return;
	}
	int n = doc->_selectedClasses.GetSize ();
	if (n == 0)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Copy Items"), true, UNDO_COPY_ITEMS))
	{
		CArray<CRscClass*, CRscClass*> arr;
		arr.Copy (doc->_selectedClasses);
		doc->SelectAllClasses (false);
		bool res = false;
		for (int i = 0; i < n; ++i)
		{
			res |= (doc->NewItemCopy (arr[i], doc->_monitorClass, _itemGroups[_preview->_itemGroup]) != NULL);
		}
		if (res)
		{
			doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			doc->CloseCommand ();
		}
		else
		{
			doc->DisposeCommand ();
		}
	}
}

void CRscEditorView::OnUpdateItemsNewCopy(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable ())
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	bool ctrlClasses = true;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::CONTROL_CLASSES, ctrlClasses);

	CRscClass *parent;
	if (ctrlClasses)
	{
		CRscParamBase *p = doc->FindParamInClass (_itemGroups[_preview->_itemGroup], doc->_monitorClass, false);
		if (p == NULL)
		{
			parent = doc->_monitorClass;
		}
		else if (p->IsClass ())
		{
			parent = (CRscClass*) p;
		}
		else
		{
			pCmdUI->Enable (FALSE);
			return;
		}
	}
	else
	{
		parent = doc->_monitorClass;
	}

	int n = doc->_selectedClasses.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		CRscClass *cl = doc->_selectedClasses[i];
		
		if (cl->_baseClass != NULL)
		{
			int size = parent->_innerParams.GetSize ();
			CRscParamBase *from = (size != 0 ? parent->_innerParams[size - 1] : NULL);
			CRscClass *bc = doc->FindBaseClassDefinition (cl->_baseClass->_name, 
				from, parent, false, true);
			if (bc != cl->_baseClass)
			{
				continue;
			}
		}
		pCmdUI->Enable (TRUE);
		return;
	}

	pCmdUI->Enable (FALSE);
}

void CRscEditorView::OnItemsNewDerived() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable ())
	{
		return;
	}
	if (doc->_selectedClasses.GetSize () != 1)
	{
		return;
	}

	bool ctrlClasses = true;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::CONTROL_CLASSES, ctrlClasses);

	CRscClass *parent;
	if (ctrlClasses)
	{
		CRscParamBase *p = doc->FindParamInClass (_itemGroups[_preview->_itemGroup], doc->_monitorClass, false);
		if (p == NULL)
		{
			parent = doc->NewInnerClass (_itemGroups[_preview->_itemGroup], doc->_monitorClass, false);
			if (parent == NULL)
			{
				doc->DisposeCommand ();
				return;
			}
		}
		else if (p->IsClass ())
		{
			parent = (CRscClass*) p;
		}
		else
		{
			doc->DisposeCommand ();
			return;
		}
	}
	else
	{
		parent = doc->_monitorClass;
	}

	CRscClass *cls = doc->_selectedClasses[0];
	int size = parent->_innerParams.GetSize ();
	CRscParamBase *from = (size != 0 ? parent->_innerParams[size - 1] : NULL);
	CRscClass *bc = doc->FindBaseClassDefinition (cls->_name, from, parent, false, true);

	if (bc == NULL || bc != cls)
	{
		return;
	}

	if (doc->OpenCommand (_T ("New Derived Item"), true, UNDO_NEW_DERIVED_ITEMS))
	{
		CRscClass *cl = doc->NewDerivedClass (cls, parent, NULL, false, false);
		if (cl == NULL)
		{
			doc->DisposeCommand ();
			return;
		}
		
		if (!ctrlClasses)
		{
			doc->SetArrayStringParamInBases (doc->_monitorClass, _itemGroups[_preview->_itemGroup], cl->_name, true, false, true);
		}
		doc->SelectAllClasses (false);
		doc->SelectClass (cl, true);
		doc->AddCommandEntry (new CUndoSelectClass (cl, true, false));

		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateItemsNewDerived(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable () ||
		doc->_selectedClasses.GetSize () != 1)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	bool ctrlClasses = true;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::CONTROL_CLASSES, ctrlClasses);

	CRscClass *parent;
	if (ctrlClasses)
	{
		CRscParamBase *p = doc->FindParamInClass (_itemGroups[_preview->_itemGroup], doc->_monitorClass, false);
		if (p == NULL)
		{
			parent = doc->_monitorClass;
		}
		else if (p->IsClass ())
		{
			parent = (CRscClass*) p;
		}
		else
		{
			pCmdUI->Enable (FALSE);
			return;
		}
	}
	else
	{
		parent = doc->_monitorClass;
	}

	CRscClass *cls = doc->_selectedClasses[0];
	int size = parent->_innerParams.GetSize ();
	CRscParamBase *from = (size != 0 ? parent->_innerParams[size - 1] : NULL);
	CRscClass *bc = doc->FindBaseClassDefinition (cls->_name, from, parent, false, true);

	if (bc == NULL || bc != cls)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}

/*
void CRscEditorView::OnItemsAdd() 
{
	CRscEditorDoc *doc = GetDocument ();
	HTREEITEM ti = _curSpaceCtrl->GetSelectedItem ();
	CRscWorkTreeItem *wi = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (ti);
	if (doc->_monitorClass == NULL ||
		doc->_monitorClass->_readOnly ||
		wi == NULL ||
		!wi->IsParam () ||
		!((CRscParamBase*) wi)->IsClass ())
	{
		return;
	}

	CRscClass *cd = doc->FindClassDefinition (((CRscClass*) wi)->_name, doc->_monitorClass, false);
	if (cd != wi)
	{
		return;
	}

	CRscParamBase *newParam;
	if (doc->_monitorClass->SetArrayStringParamInBases (_itemGroups[_preview->_itemGroup],
		((CRscClass*) wi)->_name, true, &newParam, false))
	{
		if (newParam->_treeItem == NULL)
		{
			AddClassIntoSpace (newParam->_parent->_treeItem, 
				newParam->_prev != NULL ? newParam->_prev->_treeItem : TVI_FIRST, newParam);
			_classSpaceCtrl.Invalidate (FALSE);
			_classSpaceCtrl.UpdateWindow ();
		}
		doc->SetModifiedFlag (TRUE);
		_preview->_workViewCtrl.Invalidate (FALSE);
		_preview->_workViewCtrl.UpdateWindow ();
	}
}

void CRscEditorView::OnUpdateItemsAdd(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	HTREEITEM ti = _curSpaceCtrl->GetSelectedItem ();
	CRscWorkTreeItem *wi = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (ti);
	if (doc->_monitorClass == NULL ||
		doc->_monitorClass->_readOnly ||
		wi == NULL ||
		!wi->IsParam () ||
		!((CRscParamBase*) wi)->IsClass ())
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscClass *cd = doc->FindClassDefinition (((CRscClass*) wi)->_name, doc->_monitorClass, false);
	if (cd != wi)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}
*/

void CRscEditorView::OnItemsMakeIndependent() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_selectedClasses.GetSize () <= 0 || doc->_monitorClass == NULL)
	{
		return;
	}

	if (doc->OpenCommand (_T ("Make Independent"), true, UNDO_MAKE_INDEPENDENT))
	{
		int n = doc->_selectedClasses.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CRscClass *cl = doc->_selectedClasses[i];
			doc->MakeClassIndependent (cl);
		}
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateItemsMakeIndependent(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	int n = doc->_selectedClasses.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!doc->_selectedClasses[i]->IsInsideEditable ())
		{
			pCmdUI->Enable (TRUE);
			return;
		}
	}

	pCmdUI->Enable (FALSE);
}

void CRscEditorView::OnClassEditMacros() 
{
	CRscEditorDoc *doc = GetDocument ();
	CRscMacroEditor dlg (doc);
	if (dlg.DoModal () == IDOK)
	{
		doc->SetModifiedFlag (TRUE);
	}
}

void CRscEditorView::DisplayContexMenu (int n, CPoint &point)
{
	CMenu *menubar = AfxGetMainWnd ()->GetMenu ();
	if (menubar == NULL)
	{
		return;
	}
	
	CMenu *menu = menubar->GetSubMenu (n);
	if (menu == NULL)
	{
		return;
	}
	SendMessage (WM_INITMENUPOPUP, (WPARAM) (menu->m_hMenu), n);
	
	menu->TrackPopupMenu (TPM_LEFTALIGN | TPM_LEFTBUTTON, point.x, point.y, AfxGetMainWnd ());
}

bool CRscEditorView::MoveItem (CRscClass *item, double x, double y)
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL ||
		!doc->_monitorClass->IsInsideEditable ())
	{
		return false;
	}

	doc->SetArrayStringParamInBases (doc->_monitorClass,
		_itemGroups[_preview->_itemGroup],
		item->_name, true, false, true);
	doc->SetFloatParamInBases (item, "x", x, false, false, true, false);
	doc->SetFloatParamInBases (item, "y", y, false, false, true, false);

	return true;
}

void CRscEditorView::SetMonitorLabel ()
{
	CString str;
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass != NULL)
	{
		doc->_monitorClass->GetFullName (str);
	}
	
	_monitorLabel.SetWindowText (str);
}

void CRscEditorView::SetItemLabel (CRscClass *cl)
{
	CString str;
	if (cl != NULL)
	{
		cl->GetFullName (str);
	}
	
	_preview->_itemLabel.SetWindowText (str);
}

void CRscEditorView::SetItemLabel (LPCTSTR str)
{
	_preview->_itemLabel.SetWindowText (str);
}

void CRscEditorView::OnMouseMove(UINT nFlags, CPoint point) 
{
	CFormView::OnMouseMove(nFlags, point);

	SetItemLabel ((CRscClass*) NULL);
}

void CRscEditorView::SetSpaceTab (int tab, bool focus /* = true */)
{
	_spaceTabCtrl.SetCurSel (tab);

	switch (tab)
	{
	case rscEditorInclFileSpace:
		_curSpaceCtrl->ShowWindow (SW_HIDE);
		_curSpaceCtrl = &_inclFileSpaceCtrl;
		break;

	case rscEditorClassSpace:
		_curSpaceCtrl->ShowWindow (SW_HIDE);
		_curSpaceCtrl = &_classSpaceCtrl;
		break;

	case rscEditorFavouriteSpace:
		_curSpaceCtrl->ShowWindow (SW_HIDE);
		_curSpaceCtrl = &_favouriteSpaceCtrl;
		break;

	default:
		return;
	}

	_curSpaceCtrl->ShowWindow (SW_SHOW);
	if (focus)
	{
		_curSpaceCtrl->SetFocus ();
	}
}

int CRscEditorView::GetImageNumber (CRscWorkTreeItem *wsi)
{
	if (wsi->IsParam ())
	{
		if (wsi == GetDocument ()->_monitorClass)
		{
			if (!((CRscParamBase*) wsi)->IsEditable ())
			{
				return 1;
			}
			else
			{
				return 2;
			}
		}
		else if (((CRscParamBase*) wsi)->IsClass ())
		{
			int image = 3;
			if (((CRscClass*) wsi)->FindParamInBases ("idd", true, false) != 0)
			{
				image = 7;
			}
			else if (((CRscClass*) wsi)->FindParamInBases ("idc", true, false) != 0)
			{
				image = 9;
			}

			if (!((CRscParamBase*) wsi)->IsEditable ())
			{
				return image;
			}
			else
			{
				return image + 1;
			}
		}
		else
		{
			if (!((CRscParamBase*) wsi)->IsEditable ())
			{
				return 5;
			}
			else
			{
				return 6;
			}
		}
	}

	// include
	return 0;
}

void CRscEditorView::SetControlTab (int i)
{
	_preview->_itemGroup = i;
	_preview->_workViewCtrl.Invalidate (FALSE);
	_preview->_workViewCtrl.UpdateWindow ();
}

void CRscEditorView::SelectItemGroup ()
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_selectedClasses.GetSize () == 1)
	{
		int ig = doc->_selectedClasses[0]->_itemGroup;
		if (ig != rscEditorItemGroupNone)
		{
			_preview->_itemGroup = ig;
			_preview->_controlTabCtrl.SetCurSel (ig);
		}
	}
}

void CRscEditorView::OnMonitorLabel() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL)
	{
		return;
	}

	SetSpaceTab (rscEditorClassSpace);
	_classSpaceCtrl.SelectItem (doc->_monitorClass->_treeItem);
	_classSpaceCtrl.SelectSetFirstVisible (doc->_monitorClass->_treeItem);
}

void CRscEditorView::OnItemsNew() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable ())
	{
		return;
	}
	
	if (doc->OpenCommand (_T ("New Item"), true, UNDO_NEW_ITEM))
	{
		CRscParamBase *list = doc->FindParamInClass (_itemGroups[_preview->_itemGroup], doc->_monitorClass, false);
		bool b;
		if (list == NULL)
		{
			b = true;
			((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::CONTROL_CLASSES, b);
		}
		else
		{
			b = list->IsClass ();
		}

		if (b)
		{
			if (list == NULL)
			{
				list = doc->NewInnerClass (_itemGroups[_preview->_itemGroup], doc->_monitorClass, false);
				if (list == NULL)
				{
					doc->DisposeCommand ();
					return;
				}
			}

			CRscClass *cl = doc->NewInnerClass ((CRscClass*) list, false);
			if (cl != NULL)
			{
				doc->SetFloatParamInBases (cl, "x", 0.45f, false, false, true, false);
				doc->SetFloatParamInBases (cl, "y", 0.45f, false, false, true, false);
				doc->SetFloatParamInBases (cl, "w", 0.1f, false, false, true, false);
				doc->SetFloatParamInBases (cl, "h", 0.1f, false, false, true, false);

				doc->SelectAllClasses (false);
				doc->SelectClass (cl, true);
				doc->AddCommandEntry (new CUndoSelectClass (cl, true, false));

				SelectItemClass ();
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
				doc->CloseCommand ();
			}
			else
			{
				doc->DisposeCommand ();
				return;
			}
		}
		else
		{
			CRscClass *cl = doc->NewInnerClass (doc->_monitorClass, false);
			if (cl != NULL)
			{
				doc->SetArrayStringParamInBases (doc->_monitorClass, _itemGroups[_preview->_itemGroup], 
					cl->_name, true, false, true);
				doc->SetFloatParamInBases (cl, "x", 0.45f, false, false, true, false);
				doc->SetFloatParamInBases (cl, "y", 0.45f, false, false, true, false);
				doc->SetFloatParamInBases (cl, "w", 0.1f, false, false, true, false);
				doc->SetFloatParamInBases (cl, "h", 0.1f, false, false, true, false);

				doc->SelectAllClasses (false);
				doc->SelectClass (cl, true);
				doc->AddCommandEntry (new CUndoSelectClass (cl, true, false));

				SelectItemClass ();
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
				doc->CloseCommand ();
			}
			else
			{
				doc->DisposeCommand ();
				return;
			}
		}
	}
}

void CRscEditorView::OnUpdateItemsNew(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	pCmdUI->Enable ((doc->_monitorClass == NULL || !doc->_monitorClass->IsInsideEditable ()) ? FALSE : TRUE);
}

void CRscEditorView::OnItemsChangeInherited() 
{
	_changeInherited = !_changeInherited;
}

void CRscEditorView::OnUpdateItemsChangeInherited(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck (_changeInherited ? TRUE : FALSE);
}

void CRscEditorView::OnClassNew() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->OpenCommand (_T ("New Class"), true, UNDO_NEW_CLASS))
	{
		if (doc->NewInnerClass (NULL, false) != NULL)
		{
			doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			doc->CloseCommand ();
		}
		else
		{
			doc->DisposeCommand ();
		}
	}
}

void CRscEditorView::OnEditCopy() 
{
	if (_curSpaceCtrl == &_inclFileSpaceCtrl)
	{
		return;
	}
	else if (_curSpaceCtrl == &_classSpaceCtrl)
	{
		_classSpaceCtrl.OnEditCopy ();
		return;
	}
	else if (_curSpaceCtrl == &_favouriteSpaceCtrl)
	{
		_favouriteSpaceCtrl.OnEditCopy ();
		return;
	}

	return;
}

void CRscEditorView::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	if (_curSpaceCtrl == &_inclFileSpaceCtrl)
	{
		pCmdUI->Enable (FALSE);
		return;
	}
	else if (_curSpaceCtrl == &_classSpaceCtrl)
	{
		_classSpaceCtrl.OnUpdateEditCopy (pCmdUI);
		return;
	}
	else if (_curSpaceCtrl == &_favouriteSpaceCtrl)
	{
		_favouriteSpaceCtrl.OnUpdateEditCopy (pCmdUI);
		return;
	}

	pCmdUI->Enable (FALSE);
	return;
}

void CRscEditorView::OnEditCut() 
{
	if (_curSpaceCtrl == &_inclFileSpaceCtrl)
	{
		return;
	}
	else if (_curSpaceCtrl == &_classSpaceCtrl)
	{
		_classSpaceCtrl.OnEditCut ();
		return;
	}
	else if (_curSpaceCtrl == &_favouriteSpaceCtrl)
	{
		_favouriteSpaceCtrl.OnEditCut ();
		return;
	}

	return;
}

void CRscEditorView::OnUpdateEditCut(CCmdUI* pCmdUI) 
{
	if (_curSpaceCtrl == &_inclFileSpaceCtrl)
	{
		pCmdUI->Enable (FALSE);
		return;
	}
	else if (_curSpaceCtrl == &_classSpaceCtrl)
	{
		_classSpaceCtrl.OnUpdateEditCut (pCmdUI);
		return;
	}
	else if (_curSpaceCtrl == &_favouriteSpaceCtrl)
	{
		_favouriteSpaceCtrl.OnUpdateEditCut (pCmdUI);
		return;
	}

	pCmdUI->Enable (FALSE);
	return;
}

void CRscEditorView::OnEditPaste() 
{
	_classSpaceCtrl.OnEditPaste ();
}

void CRscEditorView::OnUpdateEditPaste(CCmdUI* pCmdUI) 
{
	_classSpaceCtrl.OnUpdateEditPaste (pCmdUI);
	return;
}

void CRscEditorView::OnClassExtEdit() 
{
	if (!((CRscEditorApp*) AfxGetApp ())->_preferences.IsValue (CPreferences::EDITOR_EXTERN))
	{
		return;
	}

	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam ())
	{
		return;
	}

	CRscEditorDoc *doc = GetDocument ();
	if (doc->OpenCommand (_T ("Edit Class"), true, UNDO_EDIT_CLASS))
	{
		if (LaunchExternClassEditor ((CRscParamBase*) wtItem, tcItem != wtItem->_treeItem))
		{
			doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			doc->CloseCommand ();
		}
		else
		{
			doc->DisposeCommand ();
		}
	}
}

void CRscEditorView::OnUpdateClassExtEdit(CCmdUI* pCmdUI) 
{
	if (!((CRscEditorApp*) AfxGetApp ())->_preferences.IsValue (CPreferences::EDITOR_EXTERN))
	{
		pCmdUI->Enable (FALSE);
		return;
	}
	
	HTREEITEM tcItem = _curSpaceCtrl->GetSelectedItem ();
	if (tcItem == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscWorkTreeItem *wtItem = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (tcItem);
	if (wtItem == NULL ||
		!wtItem->IsParam ())
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}

bool CRscEditorView::LaunchExternClassEditor (CRscParamBase *param, bool inFavourites)
{
	TCHAR tempPath[MAX_PATH];
	TCHAR tempFile[MAX_PATH];
	GetTempPath (MAX_PATH, tempPath);
	GetTempFileName (tempPath, _T ("RSCF_"), 0, tempFile);
	
	CString tab ("\t");
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::TABULATOR, tab);
	CString code;
	int size = param->GetTextLength (tab);
	LPTSTR text = code.GetBuffer (size);
	param->GetText (text, tab);
	code.ReleaseBuffer ();

	CString editor;
	if (!((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::EDITOR_EXTERN, editor))
	{
		return false;
	}

	bool res = false;
	bool ok;
	do {
		CFile cTempFile;
		if (!cTempFile.Open (tempFile, CFile::modeCreate | CFile::modeWrite | CFile::shareExclusive))
		{
			return false;
		}
		cTempFile.Write ((LPCTSTR) code, size);
		cTempFile.Close ();
		if (!param->IsEditable ())
		{
			CFileStatus fs;
			CFile::GetStatus (tempFile, fs);
			fs.m_attribute |= CFile::readOnly;
			CFile::SetStatus (tempFile, fs);
		}

		STARTUPINFO stInfo;
		memset (&stInfo, 0, sizeof (stInfo));
		stInfo.cb = sizeof(stInfo);
		
		PROCESS_INFORMATION prInfo;
		memset (&prInfo, 0, sizeof (prInfo));
		CString cmdLine;
		cmdLine.Format (_T ("\"%s\" \"%s\""), editor, tempFile);
		if (::CreateProcess (editor, 
			cmdLine.GetBuffer (cmdLine.GetLength ()), NULL, NULL,
			FALSE, CREATE_DEFAULT_ERROR_MODE | NORMAL_PRIORITY_CLASS,
			NULL, NULL, &stInfo, &prInfo))
		{
			DWORD dwExitCode;
			while (::GetExitCodeProcess(prInfo.hProcess, &dwExitCode) && dwExitCode == STILL_ACTIVE)
			{
				::Sleep(100);
			}
		}

		if (!param->IsEditable ())
		{
			CFileStatus fs;
			CFile::GetStatus (tempFile, fs);
			fs.m_attribute &= ~CFile::readOnly;
			CFile::SetStatus (tempFile, fs);
			CFile::Remove (tempFile);
			ok = true;
		}
		else
		{
			if (!cTempFile.Open (tempFile, CFile::modeRead | CFile::shareDenyWrite))
			{
				CFile::Remove (tempFile);
				return false;
			}
			
			size = (cTempFile.GetLength () + sizeof (TCHAR) - 1) / sizeof (TCHAR);
			text = code.GetBuffer (size);
			cTempFile.Read (text, cTempFile.GetLength ());
			code.ReleaseBuffer (size);
			cTempFile.Close ();
			CFile::Remove (tempFile);
			ok = GetDocument ()->ApplyClassCode (code, param, inFavourites, NULL);
			
			if (!ok)
			{
				if (AfxMessageBox (_T ("Compile error. Do you want the code to be reloaded?"), MB_YESNO) != IDYES)
				{
					ok = true;
				}
			}
			else
			{
				res = true;
			}
		}
	} while (!ok);

	return res;
}

void CRscEditorView::OnItemsExtEdit() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_selectedClasses.GetSize () == 1 &&
		((CRscEditorApp*) AfxGetApp ())->_preferences.IsValue (CPreferences::EDITOR_EXTERN))
	{
		if (doc->OpenCommand (_T ("Edit Item"), true, UNDO_EDIT_ITEM))
		{
			if (LaunchExternClassEditor (GetDocument ()->_selectedClasses[0], false))
			{
				doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
				doc->CloseCommand ();
			}
			else
			{
				doc->DisposeCommand ();
			}
		}
	}
}

void CRscEditorView::OnUpdateItemsExtEdit(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable (GetDocument ()->_selectedClasses.GetSize () == 1 &&
		((CRscEditorApp*) AfxGetApp ())->_preferences.IsValue (CPreferences::EDITOR_EXTERN)
		? TRUE : FALSE);
}

void CRscEditorView::OnFileBuldozer() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc == NULL || doc->_monitorClass == NULL || doc->_monitorClass->_parent != NULL)
	{
		return;
	}

	CString buldozerWindowClass (_T ("Operation Flashpoint"));
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::BULDOZER_WINDOW_CLASS, buldozerWindowClass);

	HWND hwnd = ::FindWindow (buldozerWindowClass, NULL);
	if (hwnd == NULL)
	{
		TCHAR msg[512];
		_stprintf (msg, _T ("No window of the class \"%s\" was found."), (LPCTSTR) buldozerWindowClass);
		AfxMessageBox (msg, MB_OK);
		return;
	}

	CRscTempFile tmpFile;
	if (!tmpFile.Open ())
	{
		return;
	}

	CString tab ("\t");
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::TABULATOR, tab);

	// Save Macros
	CString str;
	doc->GetMacrosText (str);
	tmpFile.Write (str);
	// Save included files
	int size = doc->_includedFiles.GetSize ();
	for (int i = 0; i < size; ++i)
	{
		int n = doc->_includedFiles[i]->GetTextLength ();
		LPTSTR text = str.GetBuffer (n);
		doc->_includedFiles[i]->GetText (text);
		str.ReleaseBuffer (n);
		tmpFile.Write (str);
	}
	// Save enums
	size = doc->_enums.GetSize ();
	for (int i = 0; i < size; ++i)
	{
		CRscEnum *e = doc->_enums[i];
		if (!e->IsIncluded ())
		{
			int n = e->GetTextLength ();
			LPTSTR text = str.GetBuffer (n);
			e->GetText (text);
			str.ReleaseBuffer (n);
			tmpFile.Write (str);
		}
	}
	// Save class and attributes definitions
	size = doc->_params.GetSize ();
	for (int i = 0; i < size; ++i)
	{
		CRscParamBase *p = doc->_params[i];
		if (!p->IsIncluded ())
		{
			int n = p->GetTextLength (tab);
			LPTSTR text = str.GetBuffer (n);
			p->GetText (text, tab);
			str.ReleaseBuffer (n);
			tmpFile.Write (str);
		}
	}

	tmpFile.Close ();
	((CRscEditorApp*) AfxGetApp ())->_tempFiles.Add (tmpFile);

	CString msgParam;
	msgParam.Format (_T ("%s;%s"), tmpFile.GetPathName (), doc->_monitorClass->_name);
	ATOM atom = ::GlobalAddAtom (msgParam);
	if (atom == 0)
	{
		AfxMessageBox (_T ("Atom couldn't be created."), MB_OK);
		return;
	}

	::PostMessage (hwnd, WM_APP + 7, 0, (LPARAM) atom);
	//::SetForegroundWindow (hwnd);
}

void CRscEditorView::OnUpdateFileBuldozer(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc == NULL || doc->_monitorClass == NULL || doc->_monitorClass->_parent != NULL)
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	switch (lHint)
	{
	case UH_NEW_PARAM:
		{
			CUpdateDescriptor *dscr = (CUpdateDescriptor*) pHint;
			if (dscr->_newParam._param != NULL)
			{
				CRscEditorDoc *doc = GetDocument ();
				CRscParamBase *prev = doc->FindPrevParam (dscr->_newParam._param->_parent, dscr->_newParam._param);
				AddClassIntoSpace (dscr->_newParam._param->_parent != NULL ? dscr->_newParam._param->_parent->_treeItem : TVI_ROOT, 
					prev != NULL ? prev->_treeItem : TVI_FIRST, dscr->_newParam._param);
				if (dscr->_newParam._inFavourites && dscr->_newParam._param->IsClass ())
				{
					CRscClass *prev = doc->FindPrevFavourite ((CRscClass*) dscr->_newParam._param);
					AddFavouriteIntoSpace (TVI_ROOT, prev != NULL ? prev->_favouriteTreeItem : TVI_FIRST, (CRscClass*) dscr->_newParam._param);
					SelectItemClass (dscr->_newParam._param, true);
				}
				else
				{
					SelectItemClass (dscr->_newParam._param);
				}
				
				//_curSpaceCtrl->Invalidate (FALSE);
				//_curSpaceCtrl->UpdateWindow ();
				//_preview->_workViewCtrl.Invalidate (FALSE);
				//_preview->_workViewCtrl.UpdateWindow ();
				//UpdateClassEditor ();
			}
		}
		break;
	case UH_NEW_VALUE:
		{
			CUpdateDescriptor *dscr = (CUpdateDescriptor*) pHint;
			if (dscr->_newValue._param != NULL && dscr->_newValue._param->IsAttribute ())
			{
				if (dscr->_newValue._param->_treeItem == NULL)
				{
					ASSERT (FALSE);
					CRscEditorDoc *doc = GetDocument ();
					CRscParamBase *prev = doc->FindPrevParam (dscr->_newValue._param->_parent, dscr->_newValue._param);
					AddClassIntoSpace (dscr->_newValue._param->_parent != NULL ? dscr->_newValue._param->_parent->_treeItem : TVI_ROOT, 
						prev != NULL ? prev->_treeItem : TVI_FIRST, dscr->_newValue._param);
					//_curSpaceCtrl->Invalidate (FALSE);
					//_curSpaceCtrl->UpdateWindow ();
				}
				else if (((CRscAttribute*) dscr->_newValue._param)->_value->IsElement ())
				{
					CString str;
					_classSpaceCtrl.SetItemText (dscr->_newValue._param->_treeItem, dscr->_newValue._param->GetTitle (str));
					//_curSpaceCtrl->Invalidate (FALSE);
					//_curSpaceCtrl->UpdateWindow ();
				}
				//_preview->_workViewCtrl.Invalidate (FALSE);
				//_preview->_workViewCtrl.UpdateWindow ();
				//UpdateClassEditor ();
			}
		}
		break;
	case UH_UPDATE_PREVIEW:
		_preview->_workViewCtrl.Invalidate (FALSE);
		_preview->_workViewCtrl.UpdateWindow ();
		UpdateClassEditor ();
		break;
	case UH_DEL_PARAM:
		{
			CUpdateDescriptor *dscr = (CUpdateDescriptor*) pHint;
			if (dscr->_delParam._param->_treeItem != NULL)
			{
				_classSpaceCtrl.DeleteItem (dscr->_delParam._param->_treeItem);
				dscr->_delParam._param->_treeItem = NULL;
			}
			//_preview->_workViewCtrl.Invalidate (FALSE);
			//_preview->_workViewCtrl.UpdateWindow ();
			if (_classEditor._param != NULL &&
				_classEditor._param == dscr->_delParam._param)
			{
				UpdateClassEditor (NULL, false);
			}
			//else
			//{
			//	UpdateClassEditor ();
			//}
		}
		break;
	/*
	case UH_NEW_PARAM_2:
		{
			CUpdateDescriptor *dscr = (CUpdateDescriptor*) pHint;
			if (dscr->_newParam2._param->_treeItem != NULL)
			{
				CString str;
				dscr->_newParam2._param->GetTitle (str);

				HTREEITEM child;
				int image = GetImageNumber (par);

				HTREEITEM parent = (dscr->_newParam2._param->_parent != NULL ? dscr->_newParam2._param->_parent->_treeItem : TVI_ROOT);
				CRscParamBase *prev = GetDocument ()->FindPrevParam (dscr->_newParam2._param);
				HTREEITEM after = (prev != NULL ? prev->_treeItem : TVI_FIRST);
				child = _classSpaceCtrl.InsertItem (title, image, image, parent, after);
				if (child != NULL)
				{
					_classSpaceCtrl.SetItemData (child, (DWORD) dscr->_newParam2._param);
					dscr->_newParam2._param->_treeItem = child;
				}
			}
		}
		break;
	*/
	case UH_NEW_BASE:
		{
			CUpdateDescriptor *dscr = (CUpdateDescriptor*) pHint;
			CString str;
			LPCTSTR title = dscr->_newBase._class->GetTitle (str);
			if (dscr->_newBase._class->_treeItem != NULL)
			{
				_classSpaceCtrl.SetItemText (dscr->_newBase._class->_treeItem, title);
			}
			if (dscr->_newBase._class->_favouriteTreeItem != NULL)
			{
				_favouriteSpaceCtrl.SetItemText (dscr->_newBase._class->_favouriteTreeItem, title);
			}
			//_preview->_workViewCtrl.Invalidate (FALSE);
			//_preview->_workViewCtrl.UpdateWindow ();
			//UpdateClassEditor ();
		}
		break;
	case UH_CHANGE_MONITOR:
		{
			CUpdateDescriptor *dscr = (CUpdateDescriptor*) pHint;
			if (dscr->_changeMonitor._oldOne != NULL)
			{
				int imageNumber = GetImageNumber (dscr->_changeMonitor._oldOne);
				_classSpaceCtrl.SetItemImage (dscr->_changeMonitor._oldOne->_treeItem, imageNumber, imageNumber);
				if (dscr->_changeMonitor._oldOne->_favouriteTreeItem != NULL)
				{
					_favouriteSpaceCtrl.SetItemImage (dscr->_changeMonitor._oldOne->_favouriteTreeItem, imageNumber, imageNumber);
				}
			}
			if (dscr->_changeMonitor._newOne != NULL)
			{
				int imageNumber = GetImageNumber (dscr->_changeMonitor._newOne);
				_classSpaceCtrl.SetItemImage (dscr->_changeMonitor._newOne->_treeItem, imageNumber, imageNumber);
				if (dscr->_changeMonitor._newOne->_favouriteTreeItem != NULL)
				{
					_favouriteSpaceCtrl.SetItemImage (dscr->_changeMonitor._newOne->_favouriteTreeItem, imageNumber, imageNumber);
				}
			}

			//_curSpaceCtrl->Invalidate (FALSE);
			//_curSpaceCtrl->UpdateWindow ();
			//_preview->_workViewCtrl.Invalidate (FALSE);
			//_preview->_workViewCtrl.UpdateWindow ();
			SetMonitorLabel ();
		}
		break;
	case UH_REMOVE_FAVOURITE:
		{
			CUpdateDescriptor *dscr = (CUpdateDescriptor*) pHint;
			_favouriteSpaceCtrl.DeleteItem (dscr->_remFavourite._class->_favouriteTreeItem);
			dscr->_remFavourite._class->_favouriteTreeItem = NULL;
		}
		break;
	case UH_CHANGE_FAVOURITE:
		{
			CUpdateDescriptor *dscr = (CUpdateDescriptor*) pHint;
			_favouriteSpaceCtrl.SetItemData (dscr->_changeFavourite._oldClass->_favouriteTreeItem, (DWORD) dscr->_changeFavourite._newClass);
			CString title;
			_favouriteSpaceCtrl.SetItemText (dscr->_changeFavourite._oldClass->_favouriteTreeItem, dscr->_changeFavourite._newClass->GetTitle (title));
			dscr->_changeFavourite._newClass->_favouriteTreeItem = dscr->_changeFavourite._oldClass->_favouriteTreeItem;
			dscr->_changeFavourite._oldClass->_favouriteTreeItem = NULL;
		}
		break;
	case UH_INSERT_FAVOURITE:
		{
			CUpdateDescriptor *dscr = (CUpdateDescriptor*) pHint;
			CRscClass *prev = GetDocument ()->FindPrevFavourite ((CRscClass*) dscr->_newParam._param);
			AddFavouriteIntoSpace (TVI_ROOT, prev != NULL ? prev->_favouriteTreeItem : TVI_FIRST, (CRscClass*) dscr->_newParam._param);
			SelectItemClass (dscr->_newParam._param, true);
		}
		break;
	case UH_CHANGE_CODE:
		{
			CUpdateDescriptor *dscr = (CUpdateDescriptor*) pHint;
			if (dscr->_changeCode._param != NULL)
			{
				CRscEditorDoc *doc = GetDocument ();
				CRscParamBase *prev = doc->FindPrevParam (dscr->_changeCode._param->_parent, dscr->_changeCode._param);
				AddClassIntoSpace (dscr->_changeCode._param->_parent != NULL ? dscr->_changeCode._param->_parent->_treeItem : TVI_ROOT, 
					prev != NULL ? prev->_treeItem : TVI_FIRST, dscr->_changeCode._param);
				SelectItemClass (dscr->_changeCode._param, dscr->_changeCode._inFavourites);
				
				//_curSpaceCtrl->Invalidate (FALSE);
				//_curSpaceCtrl->UpdateWindow ();
				//_preview->_workViewCtrl.Invalidate (FALSE);
				//_preview->_workViewCtrl.UpdateWindow ();
				//UpdateClassEditor ();
			}
		}
		break;
	case UH_ADD_INCLUDE:
		{
			CUpdateDescriptor *dscr = (CUpdateDescriptor*) pHint;
			CRscEditorDoc *doc = GetDocument ();
			AddInclFileIntoSpace (TVI_ROOT, dscr->_addInclude._include);
			AddClassesIntoSpace (TVI_ROOT, 
				dscr->_addInclude._from > 0 ? doc->_params[dscr->_addInclude._from - 1]->_treeItem : TVI_FIRST, 
				doc->_params, dscr->_addInclude._from, dscr->_addInclude._from + dscr->_addInclude._include->_params.GetSize ());
			
			//_curSpaceCtrl->Invalidate (FALSE);
			//_curSpaceCtrl->UpdateWindow ();
			//_preview->_workViewCtrl.Invalidate (FALSE);
			//_preview->_workViewCtrl.UpdateWindow ();
			//UpdateClassEditor ();
		}
		break;
	case UH_REMOVE_INCLUDE:
		{
			CUpdateDescriptor *dscr = (CUpdateDescriptor*) pHint;
			CRscEditorDoc *doc = GetDocument ();
			_inclFileSpaceCtrl.DeleteItem (dscr->_removeInclude._include->_treeItem);
			int to = dscr->_removeInclude._from + dscr->_removeInclude._include->_params.GetSize ();
			for (int i = dscr->_removeInclude._from; i < to; ++i)
			{
				CRscParamBase *p = doc->_params[i];
				if (p->_treeItem != NULL)
				{
					_classSpaceCtrl.DeleteItem (p->_treeItem);
					p->_treeItem = NULL;
				}
			}
			//_preview->_workViewCtrl.Invalidate (FALSE);
			//_preview->_workViewCtrl.UpdateWindow ();
			//UpdateClassEditor ();
		}
		break;
	case UH_MOVE_FAVOURITE:
		{
			CUpdateDescriptor *dscr = (CUpdateDescriptor*) pHint;
			CRscEditorDoc *doc = GetDocument ();
			_favouriteSpaceCtrl.DeleteItem (dscr->_moveFavourite._class->_favouriteTreeItem);
			dscr->_moveFavourite._class->_favouriteTreeItem = NULL;
			AddFavouriteIntoSpace (TVI_ROOT, 
				dscr->_moveFavourite._index == 0 ? TVI_FIRST : doc->_favouriteClasses[dscr->_moveFavourite._index - 1]->_favouriteTreeItem, 
				dscr->_moveFavourite._class);
			
			SelectItemClass (dscr->_moveFavourite._class, true);
			//_favouriteSpaceCtrl.Invalidate (FALSE);
			//_favouriteSpaceCtrl.UpdateWindow ();
		}
		break;
	case UH_DELETE_CONTENTS:
		if (::IsWindow (_inclFileSpaceCtrl.m_hWnd))
		{
			_inclFileSpaceCtrl.SelectItem (NULL);
			_inclFileSpaceCtrl.DeleteAllItems ();
		}
		if (::IsWindow (_classSpaceCtrl.m_hWnd))
		{
			_classSpaceCtrl.SelectItem (NULL);
			_classSpaceCtrl.DeleteAllItems ();
		}
		if (::IsWindow (_favouriteSpaceCtrl.m_hWnd))
		{
			_favouriteSpaceCtrl.SelectItem (NULL);
			_favouriteSpaceCtrl.DeleteAllItems ();
		}
		if (::IsWindow (_preview->_workViewCtrl.m_hWnd))
		{
			_preview->_workViewCtrl.Invalidate (FALSE);
			_preview->_workViewCtrl.UpdateWindow ();
		}
		UpdateClassEditor (NULL, false);
		break;
	}
}

void CRscEditorView::OnEditUndo() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->_undos.GetSize ();
	if (size == 0 && doc->_curUndo == NULL)
	{
		return;
	}

	if (doc->_curUndo != NULL)
	{
		doc->CloseCommand ();
		size = doc->_undos.GetSize ();
	}
	doc->SetUndoMode ();

	CUndoCommand *cmd = doc->_undos[size - 1];
	doc->_undos.RemoveAt (size - 1);

	size = cmd->_entries.GetSize ();
	for (int i = size - 1; i >= 0; --i)
	{
		cmd->_entries[i]->Undo (doc);
	}

	doc->_redos.InsertAt (0, cmd);

	if (cmd->_postPreviewUpdate)
	{
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
	}

	doc->SetNormalMode ();

	size = doc->_undos.GetSize ();
	cmd = size != 0 ? doc->_undos[size - 1] : NULL;
	doc->SetSaved (doc->_everSavedFlag ? cmd == doc->_lastSavedPos : FALSE, cmd != doc->_lastSavedPos);
}

void CRscEditorView::OnUpdateEditUndo(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->_undos.GetSize ();
	if (size == 0 && doc->_curUndo == NULL)
	{
		pCmdUI->SetText (_T ("Undo\tCtrl+Z"));
		pCmdUI->Enable (FALSE);
		return;
	}
	CString text (_T ("Undo: "));
	text += doc->_curUndo == NULL ? doc->_undos[size - 1]->_name : doc->_curUndo->_name;
	text += _T ("\tCtrl+Z");
	pCmdUI->SetText (text);
}

void CRscEditorView::OnEditRedo() 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->_redos.GetSize ();
	if (size == 0 || doc->_curUndo != NULL)
	{
		return;
	}

	doc->SetUndoMode ();

	CUndoCommand *cmd = doc->_redos[0];
	doc->_redos.RemoveAt (0);

	size = cmd->_entries.GetSize ();
	for (int i = 0; i < size; ++i)
	{
		cmd->_entries[i]->Redo (doc);
	}

	doc->_undos.Add (cmd);

	if (cmd->_postPreviewUpdate)
	{
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
	}

	doc->SetNormalMode ();

	size = doc->_undos.GetSize ();
	cmd = size != 0 ? doc->_undos[size - 1] : NULL;
	doc->SetSaved (doc->_everSavedFlag ? cmd == doc->_lastSavedPos : FALSE, cmd != doc->_lastSavedPos);
}

void CRscEditorView::OnUpdateEditRedo(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	int size = doc->_redos.GetSize ();
	if (size == 0 || doc->_curUndo != NULL)
	{
		pCmdUI->SetText (_T ("Redo\tCtrl+Shift+Z"));
		pCmdUI->Enable (FALSE);
		return;
	}
	CString text (_T ("Redo: "));
	text += doc->_redos[0]->_name;
	text += _T ("\tCtrl+Shift+Z");
	pCmdUI->SetText (text);
}

void CRscEditorView::OnEditClearHistory() 
{
	GetDocument ()->ClearHistory ();
}

void CRscEditorView::OnItemsBackward() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL || doc->_selectedClasses.GetSize () == 0 ||
		!doc->_monitorClass->IsInsideEditable ())
	{
		return;
	}

	CArray<CRscClass*, CRscClass*> cp;
	cp.Copy (doc->_selectedClasses);
	doc->SortFromBackToFront (cp);
	if (doc->OpenCommand (_T ("Send Backward"), true, UNDO_SEND_BACKWARD))
	{
		int n = cp.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			doc->SendBackward (cp[i]);
		}
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateItemsBackward(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL || doc->_selectedClasses.GetSize () == 0 ||
		!doc->_monitorClass->IsInsideEditable ())
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnItemsForward() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL || doc->_selectedClasses.GetSize () == 0 ||
		!doc->_monitorClass->IsInsideEditable ())
	{
		return;
	}

	CArray<CRscClass*, CRscClass*> cp;
	cp.Copy (doc->_selectedClasses);
	doc->SortFromBackToFront (cp);
	if (doc->OpenCommand (_T ("Bring Forward"), true, UNDO_BRING_FORWARD))
	{
		int n = cp.GetSize ();
		for (int i = n; i > 0; )
		{
			--i;
			doc->BringForward (cp[i]);
		}
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateItemsForward(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL || doc->_selectedClasses.GetSize () == 0 ||
		!doc->_monitorClass->IsInsideEditable ())
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnItemsBack() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL || doc->_selectedClasses.GetSize () == 0 ||
		!doc->_monitorClass->IsInsideEditable ())
	{
		return;
	}

	CArray<CRscClass*, CRscClass*> cp;
	cp.Copy (doc->_selectedClasses);
	doc->SortFromBackToFront (cp);
	if (doc->OpenCommand (_T ("Send To Back"), true, UNDO_SEND_TO_BACK))
	{
		int n = cp.GetSize ();
		for (int i = n; i > 0; )
		{
			--i;
			doc->SendToBack (cp[i]);
		}
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateItemsBack(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL || doc->_selectedClasses.GetSize () == 0 ||
		!doc->_monitorClass->IsInsideEditable ())
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnItemsFront() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL || doc->_selectedClasses.GetSize () == 0 ||
		!doc->_monitorClass->IsInsideEditable ())
	{
		return;
	}

	CArray<CRscClass*, CRscClass*> cp;
	cp.Copy (doc->_selectedClasses);
	doc->SortFromBackToFront (cp);
	if (doc->OpenCommand (_T ("Bring To Front"), true, UNDO_BRING_TO_FRONT))
	{
		int n = cp.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			doc->BringToFront (cp[i]);
		}
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		doc->CloseCommand ();
	}
}

void CRscEditorView::OnUpdateItemsFront(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL || doc->_selectedClasses.GetSize () == 0 ||
		!doc->_monitorClass->IsInsideEditable ())
	{
		pCmdUI->Enable (FALSE);
	}
}

void CRscEditorView::OnEditSelectAll() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL)
	{
		return;
	}

	doc->SelectAllClasses (true);
	_preview->_workViewCtrl.Invalidate (FALSE);
	_preview->_workViewCtrl.UpdateWindow ();
}

void CRscEditorView::OnUpdateEditSelectAll(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}
}

void CRscEditorView::OnItemsGroup() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL)
	{
		return;
	}

	CArray<CRscGroup*, CRscGroup*> grs;
	switch (doc->GetSelectedGroups (&grs))
	{
	case SELECTED_NONE_GROUP:
		if (doc->OpenCommand (_T ("Group Items"), true, UNDO_GROUP_ITEMS))
		{
			doc->Group ();
			doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			doc->CloseCommand ();
		}
		break;

	case SELECTED_ONE_GROUP:
		if (doc->OpenCommand (_T ("Ungroup Items"), true, UNDO_UNGROUP_ITEMS))
		{
			doc->Ungroup (grs[0]);
			doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			doc->CloseCommand ();
		}
		break;

	case SELECTED_PART_GROUPS:
		if (doc->OpenCommand (_T ("Regroup Items"), true, UNDO_REGROUP_ITEMS))
		{
			doc->Regroup (grs);
			doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
			doc->CloseCommand ();
		}
		break;

	case SELECTED_NONE_ITEM:
	case SELECTED_ONE_ITEM:
		break;
	}
}

void CRscEditorView::OnUpdateItemsGroup(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	switch (doc->GetSelectedGroups (NULL))
	{
	case SELECTED_NONE_GROUP:
		pCmdUI->SetText (_T ("&Group\tCtrl+G"));
		break;

	case SELECTED_ONE_GROUP:
		pCmdUI->SetText (_T ("Un&group\tCtrl+G"));
		break;

	case SELECTED_PART_GROUPS:
		pCmdUI->SetText (_T ("Re&group\tCtrl+G"));
		break;

	case SELECTED_NONE_ITEM:
	case SELECTED_ONE_ITEM:
		pCmdUI->Enable (FALSE);
		break;
	}
}

void CRscEditorView::OnLayoutShowGrid() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL)
	{
		return;
	}

	bool show = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_GRID, show);
	show = !show;
	((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::SHOW_GRID, show);

	_preview->Invalidate (FALSE);
	_preview->UpdateWindow ();
}

void CRscEditorView::OnUpdateLayoutShowGrid(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	bool show = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_GRID, show);
	pCmdUI->SetCheck (show ? TRUE : FALSE);
}

void CRscEditorView::OnLayoutShowGuides() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL)
	{
		return;
	}

	bool show = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_GUIDES, show);
	show = !show;
	((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::SHOW_GUIDES, show);

	_preview->Invalidate (FALSE);
	_preview->UpdateWindow ();
}

void CRscEditorView::OnUpdateLayoutShowGuides(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	bool show = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_GUIDES, show);
	pCmdUI->SetCheck (show ? TRUE : FALSE);
}

void CRscEditorView::OnLayoutSnapToGrid() 
{
	bool show = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GRID_SWITCH, show);
	show = !show;
	((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::GRID_SWITCH, show);

	_preview->Invalidate (FALSE);
	_preview->UpdateWindow ();
}

void CRscEditorView::OnUpdateLayoutSnapToGrid(CCmdUI* pCmdUI) 
{
	bool show = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GRID_SWITCH, show);
	pCmdUI->SetCheck (show ? TRUE : FALSE);
}

void CRscEditorView::OnLayoutSnapToGuides() 
{
	bool show = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDES_SWITCH, show);
	show = !show;
	((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::GUIDES_SWITCH, show);

	_preview->Invalidate (FALSE);
	_preview->UpdateWindow ();
}

void CRscEditorView::OnUpdateLayoutSnapToGuides(CCmdUI* pCmdUI) 
{
	bool show = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDES_SWITCH, show);
	pCmdUI->SetCheck (show ? TRUE : FALSE);
}

void CRscEditorView::OnEditPreferences() 
{
	CPreferencesDialog dlg;
	if (dlg.DoModal () == IDOK)
	{
		GetDocument ()->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
	}
}

void CRscEditorView::OnLayoutDeleteGuides() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL)
	{
		return;
	}

	CRscGuide *g = doc->GetGuide (doc->_monitorClass, false);
	if (g != NULL)
	{
		if (doc->OpenCommand (_T ("Delete Guide"), true, UNDO_DELETE_GUIDE))
		{
			doc->DeleteGuide (g);
			doc->CloseCommand ();
			doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		}
	}
}

void CRscEditorView::OnUpdateLayoutDeleteGuides(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscGuide *g = doc->GetGuide (doc->_monitorClass, false);
	if (g == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}
}

void CRscEditorView::OnLayoutToggleGridGuides() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL)
	{
		return;
	}

	bool showGuides = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_GUIDES, showGuides);
	bool showGrid = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::SHOW_GRID, showGrid);
	bool switchGuides = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GUIDES_SWITCH, switchGuides);
	bool switchGrid = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::GRID_SWITCH, switchGrid);

	bool show = showGuides && showGrid && switchGuides && switchGrid;
	show = !show;
	((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::SHOW_GUIDES, show);
	((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::SHOW_GRID, show);
	((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::GUIDES_SWITCH, show);
	((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::GRID_SWITCH, show);

	_preview->Invalidate (FALSE);
	_preview->UpdateWindow ();
}

void CRscEditorView::OnUpdateLayoutToggleGridGuides(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_monitorClass == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}
}

void CRscEditorView::OnUpdateLayoutMoveLeft(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_selectedClasses.GetSize () == 0)
	{
		pCmdUI->Enable (FALSE);
		return;
	}
}

void CRscEditorView::OnLayoutMoveLeft() 
{
	_preview->_workViewCtrl.MoveSelectedItems (-1.0, 0.0);
}

void CRscEditorView::OnUpdateLayoutMoveRight(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_selectedClasses.GetSize () == 0)
	{
		pCmdUI->Enable (FALSE);
		return;
	}
}

void CRscEditorView::OnLayoutMoveRight() 
{
	_preview->_workViewCtrl.MoveSelectedItems (1.0, 0.0);
}

void CRscEditorView::OnUpdateLayoutMoveUp(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_selectedClasses.GetSize () == 0)
	{
		pCmdUI->Enable (FALSE);
		return;
	}
}

void CRscEditorView::OnLayoutMoveUp() 
{
	_preview->_workViewCtrl.MoveSelectedItems (0.0, -1.0);
}

void CRscEditorView::OnUpdateLayoutMoveDown(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	if (doc->_selectedClasses.GetSize () == 0)
	{
		pCmdUI->Enable (FALSE);
		return;
	}
}

void CRscEditorView::OnLayoutMoveDown() 
{
	_preview->_workViewCtrl.MoveSelectedItems (0.0, 1.0);
}

void CRscEditorView::OnUpdateLayoutFreeX(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck (!_lockX  ? 1 : 0);
}

void CRscEditorView::OnLayoutFreeX() 
{
	_lockX = !_lockX;
	if (_lockX && _lockY)
	{
		_lockY = false;
	}
}

void CRscEditorView::OnUpdateLayoutFreeY(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck (!_lockY ? 1 : 0);
}

void CRscEditorView::OnLayoutFreeY() 
{
	_lockY = !_lockY;
	if (_lockX && _lockY)
	{
		_lockX = false;
	}
}

void CRscEditorView::OnUpdateClassSetMonitorClass(CCmdUI* pCmdUI) 
{
	HTREEITEM ti = _curSpaceCtrl->GetSelectedItem ();
	if (ti == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	CRscWorkTreeItem *wti = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (ti);
	if (wti == NULL ||
		!wti->IsParam () ||
		!((CRscParamBase*) wti)->IsClass ())
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable (TRUE);
}

void CRscEditorView::OnClassSetMonitorClass() 
{
	HTREEITEM ti = _curSpaceCtrl->GetSelectedItem ();
	if (ti == NULL)
	{
		return;
	}

	CRscWorkTreeItem *wti = (CRscWorkTreeItem*) _curSpaceCtrl->GetItemData (ti);
	if (wti == NULL ||
		!wti->IsParam () ||
		!((CRscParamBase*) wti)->IsClass ())
	{
		return;
	}

	CRscEditorDoc *doc = GetDocument ();
	if (doc->_curUndoType != UNDO_SET_MONITOR_CLASS)
	{
		if (!doc->OpenCommand (_T ("Set Monitor Class"), true, UNDO_SET_MONITOR_CLASS))
		{
			return;
		}
	}

	if (doc->SetMonitorClass ((CRscClass*) wti))
	{
		doc->UpdateAllViews (NULL, UH_UPDATE_PREVIEW, NULL);
		// doc->CloseCommand ();
	}
}

void CRscEditorView::OnLayoutSnapToItem() 
{
	bool show = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::ITEM_SNAP_SWITCH, show);
	show = !show;
	((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::ITEM_SNAP_SWITCH, show);

	_preview->Invalidate (FALSE);
	_preview->UpdateWindow ();
}

void CRscEditorView::OnUpdateLayoutSnapToItem(CCmdUI* pCmdUI) 
{
	bool show = false;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::ITEM_SNAP_SWITCH, show);
	pCmdUI->SetCheck (show ? TRUE : FALSE);
}

void CRscEditorView::OnEditFind() 
{
	_findReplaceDialog = new CFindReplaceDialog ();
	CString findWhat;
	CString replaceWith;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::EDITOR_FINDWHAT, findWhat);
	((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::EDITOR_REPLACEWITH, replaceWith);

	_findReplaceDialog->Create (TRUE, findWhat, replaceWith, FR_DOWN, NULL);
	_findReplaceDialog->m_fr.lStructSize = sizeof (_findReplaceDialog->m_fr);
	_findReplaceDialog->m_fr.hwndOwner = this->m_hWnd;
	_findReplaceDialog->m_fr.hInstance = AfxGetInstanceHandle ();
	_findReplaceDialog->m_fr.Flags = FR_DOWN;
	_findReplaceDialog->m_fr.lpstrFindWhat = _findWhat;
	_findReplaceDialog->m_fr.lpstrReplaceWith = _replaceWith;
	_findReplaceDialog->m_fr.wFindWhatLen = 255;
	_findReplaceDialog->m_fr.wReplaceWithLen = 255;
	_findReplaceDialog->m_fr.lCustData = 0;
	_findReplaceDialog->m_fr.lpfnHook = NULL;
	_findReplaceDialog->m_fr.lpTemplateName = NULL;
}

void CRscEditorView::FindNext (DWORD flags, HWND owner)
{
	CRscEditorDoc *doc = GetDocument ();

	CString findWhat;
	((CRscEditorApp*) AfxGetApp ())->_preferences.GetValue (CPreferences::EDITOR_FINDWHAT, findWhat);

	HTREEITEM ti = _classSpaceCtrl.GetSelectedItem ();
	CRscParamBase *lastFound = ti == NULL ? NULL : (CRscParamBase*) _classSpaceCtrl.GetItemData (ti);
	lastFound = flags & FR_DOWN ? doc->FindNextForward (findWhat, lastFound, flags) : 
					doc->FindNextBackward (findWhat, lastFound, flags);
	if (lastFound != NULL)
	{
		SetSpaceTab (rscEditorClassSpace, false);
		_classSpaceCtrl.SelectItem (lastFound->_treeItem);
		_classSpaceCtrl.SelectSetFirstVisible (lastFound->_treeItem);
	}
	else
	{
		::MessageBox (owner, _T ("Finished searching the document."), _T ("RscEditor"), MB_OK);
	}
}

void CRscEditorView::OnEditFindNext() 
{
	FindNext (((CRscEditorApp*) AfxGetApp ())->_lastFlags, m_hWnd);
}

LONG CRscEditorView::OnFindMsg (WPARAM wParam, LPARAM lParam)
{
	FINDREPLACE *fr = (FINDREPLACE*) lParam;
	((CRscEditorApp*) AfxGetApp ())->_lastFlags = fr->Flags;
	if (fr->Flags & FR_FINDNEXT)
	{
		((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::EDITOR_FINDWHAT, fr->lpstrFindWhat);
		((CRscEditorApp*) AfxGetApp ())->_preferences.SetValue (CPreferences::EDITOR_REPLACEWITH, fr->lpstrReplaceWith);
		FindNext (fr->Flags, _findReplaceDialog->m_hWnd);
	}
	return 1;
}

void CRscEditorView::OnUpdateFileSave(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	pCmdUI->Enable (doc->IsSaved () ? FALSE : TRUE);
}

void CRscEditorView::OnEditUndoToSave() 
{
	CRscEditorDoc *doc = GetDocument ();
	if (!doc->IsModified ())
	{
		return;
	}

	if (doc->_lastSavedPos == NULL)
	{
		while (doc->_undos.GetSize () != 0)
		{
			OnEditUndo ();
		}
		return;
	}

	int n = doc->_undos.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (doc->_undos[i] == doc->_lastSavedPos)
		{
			for (++i; i < n; ++i)
			{
				OnEditUndo ();
			}
			return;
		}
	}

	n = doc->_redos.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (doc->_redos[i] == doc->_lastSavedPos)
		{
			for (++i; i != 0; --i)
			{
				OnEditRedo ();
			}
			return;
		}
	}
}

void CRscEditorView::OnUpdateEditUndoToSave(CCmdUI* pCmdUI) 
{
	CRscEditorDoc *doc = GetDocument ();
	pCmdUI->Enable (doc->IsModified () ? TRUE : FALSE);
}
