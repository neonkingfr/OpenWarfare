#if !defined(AFX_RSCWORKVIEW_H__7BC95766_7059_4E1C_A78E_F4BF77132811__INCLUDED_)
#define AFX_RSCWORKVIEW_H__7BC95766_7059_4E1C_A78E_F4BF77132811__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CRscEditorPreview;

/*! Class of drawing area of the preview pane also containing tabs for switching among item groups 
    (controls/objects/background controls).
*/
class CRscWorkView : public CWnd
{
public:
  //! Parent preview pane.
	CRscEditorPreview *_preview;

  //! Is right button down (for context menu).
	bool _rButtonDown;

  //! Auxiliary object for drag&drop.
	CDropTarget<CRscWorkView> _dropTarget;

  //! Last drop effect.
	DROPEFFECT _lastDropEffect;

  //! Last drop rectangle.
	CRect _lastDropRect;

  //! Last mouse drag point.
	CPoint _dragPoint;

  //! Point where the left mouse button was pressed down.
	CPoint _dragCorner;

  //! Code of pressed dragged box to change properties of control item (size and position).
	int _pressedDragBox;

  //! References to layout coordinates of selected control items or groups of them.
	CArray<CRscLayoutUnit*, CRscLayoutUnit*> _dragLayoutUnits;

  //! References to layout coordinates of the control item of the currently pressed drag box.
	CRscLayoutUnit *_pressedDragBoxLU;

  //! Is some command opened? (store control item dragging as one command)
	bool _commandOpened;

  //! Offscrren bitmap for smooth redrawing while dragging.
	CBitmap *_offBmp;

  //! Offscreen bitmap width.
	int _offBmpWidth;

  //! Offscreen bitmap height.
	int _offBmpHeight;

  //! What kind of dragging is acting - item moving, resizing, selecting, vert. guide moving, horz. guide moving?
	int _dragState;

  //! Rectangle for item selection by mouse dragging.
	CRect _dragRect;

  //! Originally selected classes before adding new ones.
	CArray<CRscClass*, CRscClass*> _originallySelectedClasses;

  //! Pointer to the currently dragged guide (if any).
	CRscGuide *_dragGuide;

  //! Index of the guide value which is changing (if is any dragged).
	int _dragGuideIndex;

  //! The new guide value (if is any dragged).
	double _dragGuideValue;

  //! Index of the guide value which is highlighted (mouse pointer is over it).
	int _highlightedGuideIndex;

public:
	CRscWorkView();
	virtual ~CRscWorkView();

	//{{AFX_VIRTUAL(CRscWorkView)
	//}}AFX_VIRTUAL

  //! Find drag box of some control item at given point and return the code of the drag box type.
	int FindDragBoxAtPt (CPoint &pt, CRscLayoutUnit **dragBoxLU);

  //! Select control item class (or append it to the current selection).
	bool SelectClass (CPoint &pt, CRscClass *topClass, bool append);

  //! Select control item class(es) (or append it/them to the current selection).
	void SelectClass (CRect &r, CRscClass *topClass, bool append);

  //! Find control item class at given point.
	CRscClass *FindClassAtPt (CPoint &pt, CRscClass *topClass);

  //! Find control item classes inside some rectangle.
	void FindClassesInRect (CRect &r, CRscClass *topClass, CArray<CRscClass*, CRscClass*> &arr);

  //! Register drop target.
	void DropRegister ();

  //! Method of drag and drop event.
	DROPEFFECT OnDragEnter(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);

  //! Method of drag and drop event.
	DROPEFFECT OnDragOver(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);

  //! Method of drag and drop event.
	void OnDragLeave(CWnd *pWnd);

  //! Method of drag and drop event.
	BOOL OnDrop (CWnd *pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);

  //! Insert droppped class or attribute into monitor class and register it as control item.
	BOOL ProcessClassDropInWorkView (CString &fullCode, CPoint &point, DROPEFFECT dropEffect);

  //! Calculate preview pane rectangle based on the preferences.
	static void CalcPreviewRect (RECT &prev, const RECT &ctrl);

  //! Calculate vertical guide rectangle based on the preferences.
	static void CalcVertGuideRect (RECT &vert, const RECT &ctrl);

  //! Calculate horizontal guide rectangle based on the preferences.
	static void CalcHorzGuideRect (RECT &horz, const RECT &ctrl);

  //! Move selected control items.
	void MoveSelectedItems (double dpx, double dpy);

  //! Update mouse cursor shape based on its position and pressed keys.
	void UpdateCursor (UINT flags, CPoint &point);

  //! Update mouse cursor shape based on its position and pressed keys.
	void UpdateCursor (UINT flags);

  //! Update mouse cursor shape based on its position and pressed keys.
	void UpdateCursor ();

protected:
  //! Resize left coordinate by dragging.
	void SetDraggedLeftBoxAbs (LONG dpa, LONG pb);

  //! Resize top coordinate by dragging.
	void SetDraggedTopBoxAbs (LONG dpa, LONG pb);

  //! Resize right coordinate by dragging.
	void SetDraggedRightBoxAbs (LONG dpa, LONG pb);

  //! Resize bottom coordinate by dragging.
	void SetDraggedBottomBoxAbs (LONG dpa, LONG pb);

  //! Move top coordinate.
	void SetTopCoordAbs (CRscLayoutUnit *lu, double top, LONG height);

  //! Move left coordinate.
	void SetLeftCoordAbs (CRscLayoutUnit *lu, double left, LONG width);

  //! Check if the top coordinate doesn't go over the limits.
	void CheckTopCoordAbs (CRscLayoutUnit *lu, double *top, LONG height);

  //! Check if the left coordinate doesn't go over the limits.
	void CheckLeftCoordAbs (CRscLayoutUnit *lu, double *left, LONG width);

  //! Move control item(s) by mouse draggig.
	void DragMove (CPoint &point);

  //! Resize control item(s) by mouse dragging.
	void DragResize (CPoint &point);

  //! Select control item(s) by mouse dragging.
	void DragSelect (CPoint &point, UINT nFlags);

protected:
	//{{AFX_MSG(CRscWorkView)
	afx_msg void OnPaint();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnDestroy();
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg LRESULT OnUserUpdateCursor (WPARAM wParam, LPARAM lParam);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RSCWORKVIEW_H__7BC95766_7059_4E1C_A78E_F4BF77132811__INCLUDED_)
