
//! Calculate pixel coord of the guide value in pixel view rect.
#define CALC_GUIDE_POINT(pt, viewLeftTop, viewWidthHeight, coord)	\
	(pt) = (viewLeftTop) + (int) ((viewWidthHeight) * (coord) + 0.5);

//! Calculate control item rectangle from relative coords and pixel view rectangle.
#define CALC_CTRL_RECT(rect,viewLeft,viewTop,viewWidth,viewHeight,ctrlLeft,ctrlTop,ctrlWidth,ctrlHeight)	\
	(rect).left = (viewLeft) + (int) ((viewWidth) * (ctrlLeft) + 0.5);\
	(rect).top = (viewTop) + (int) ((viewHeight) * (ctrlTop) + 0.5);\
	(rect).right = (viewLeft) + (int) ((viewWidth) * ((ctrlLeft) + (ctrlWidth)) + 1.5);\
	(rect).bottom = (viewTop) + (int) ((viewHeight) * ((ctrlTop) + (ctrlHeight)) + 1.5);

//! Size of the drag box for resizing of control items.
#define DRAG_BOX_SIZE	4

//! Draw drag box for control item resizing.
#define DRAW_DRAG_BOX(dc, x, y, view, rr, rf)		\
	DRAG_BOX ((x), (y), (view), (rr), (rf))\
	(dc)->Rectangle (rf);

//! Calculate rectangle of the drag box for control item resizing.
#define DRAG_BOX(x, y, view, rr, rf)		\
	(rr).left = (x) - DRAG_BOX_SIZE;\
	(rr).top = (y) - DRAG_BOX_SIZE;\
	(rr).right = (x) + DRAG_BOX_SIZE + 1;\
	(rr).bottom = (y) + DRAG_BOX_SIZE + 1;\
	(rf).IntersectRect ((view), &(rr));

//! Flags if click was on a row of tree control item.
#define TVHT_ONITEMROW (TVHT_ONITEMICON | TVHT_ONITEMLABEL | TVHT_ONITEMINDENT | \
		TVHT_ONITEMBUTTON | TVHT_ONITEMRIGHT | TVHT_ONITEMSTATEICON)

//! Cut value into min and max.
#define LIMIT(var,min,max)		\
	if (var < min)\
	{\
		var = min;\
	}\
	else if (var > max)\
	{\
		var = max;\
	}

//! Enumeration of item groups.
enum
{
	rscEditorItemGroupNone = -1,
	rscEditorItemGroupObjects = 0,
	rscEditorItemGroupControls,
	rscEditorItemGroupControlsBackground,
	rscEditorItemGroupNumber,
};

class CRscEditorDoc;

//! Template class for drag and drop virtual methods.
template<typename ctrlType>
class CDropTarget : public COleDropTarget
{
public:
	ctrlType	*_control;

public:
	CDropTarget () : _control (NULL)
	{
	}

	virtual DROPEFFECT OnDragEnter(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point)
	{
		if (_control != NULL)
		{
			return _control->OnDragEnter (pWnd, pDataObject, dwKeyState, point);
		}
		else
		{
			return COleDropTarget::OnDragEnter (pWnd, pDataObject, dwKeyState, point);
		}
	}

	virtual DROPEFFECT OnDragOver(CWnd *pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point)
	{
		if (_control != NULL)
		{
			return _control->OnDragOver (pWnd, pDataObject, dwKeyState, point);
		}
		else
		{
			return COleDropTarget::OnDragOver (pWnd, pDataObject, dwKeyState, point);
		}
	}

	virtual void OnDragLeave(CWnd *pWnd)
	{
		if (_control != NULL)
		{
			_control->OnDragLeave (pWnd);
		}
		else
		{
			COleDropTarget::OnDragLeave (pWnd);
		}
	}

	virtual BOOL OnDrop(CWnd *pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point)
	{
		if (_control != NULL)
		{
			return _control->OnDrop (pWnd, pDataObject, dropEffect, point);
		}
		else
		{
			return COleDropTarget::OnDrop (pWnd, pDataObject, dropEffect, point);
		}
	}

	virtual DROPEFFECT OnDragScroll(CWnd *pWnd, DWORD dwKeyState, CPoint point)
	{
		return COleDropTarget::OnDragScroll (pWnd, dwKeyState, point);
	}
};

/*! Encapsulates general item of tree control (e.g. CRscClassSpace, CRscFavouriteSpace, CRscInclFileSpace) 
    for drag and drop, context menu etc.
*/
class CRscWorkTreeItem
{
public:
	HTREEITEM		_treeItem;

public:
	CRscWorkTreeItem ()
		: _treeItem (NULL)
	{
	}

	virtual ~CRscWorkTreeItem ()
	{
	}

	virtual bool IsInclude () const
	{
		return false;
	}

	virtual bool IsParam () const
	{
		return false;
	}
};

class CRscParamBase;
class CRscMacroCall;

//! Tree item of CRscInclFileSpace.
class CRscInclude : public CRscWorkTreeItem
{
public:
  //! Path to the file of this item.
	CString		_name;

  //! Items of files included into this file.
	CArray<CRscInclude*, CRscInclude*> _subInclude;

  //! Parameters (classes or attributes) stored in this include file.
	CArray<CRscParamBase*, CRscParamBase*> _params;

  //! Macro calls used in this include file.
	CArray<CRscMacroCall*, CRscMacroCall*> _macroCalls;

  //! Prefix comments of '#include "this_file"'.
	CStringArray _preComments;

public:
	CRscInclude (LPCTSTR name)
		: _name (name)
	{
	}

	virtual ~CRscInclude ()
	{
		int size = _subInclude.GetSize ();
		for (int i = 0; i < size; ++i)
		{
			delete _subInclude[i];
		}
		_subInclude.RemoveAll ();
	}

	virtual bool IsInclude () const
	{
		return true;
	}

	virtual int GetTextLength () const
	{
		int n = 0;
		int m = _preComments.GetSize ();
		for (int i = 0; i < m; ++i)
		{
			n += _preComments[i].GetLength ();
			n += _tcslen (_T ("\r\n"));
		}
		n += _tcslen (_T ("#include \""));
		n += _name.GetLength ();
		n += _tcslen (_T ("\"\r\n"));
		return n;
	}

	virtual LPTSTR GetText (LPTSTR text) const
	{
		_tcscpy (text, _T (""));
		int m = _preComments.GetSize ();
		for (int i = 0; i < m; ++i)
		{
			_tcscat (text, (LPCTSTR) _preComments[i]);
			_tcscat (text, _T ("\r\n"));
		}
		_tcscat (text, _T ("#include \""));
		_tcscat (text, _name);
		_tcscat (text, _T ("\"\r\n"));
		return text;
	}
};

class CRscAttributeValueArray;
class CRscAttribute;

//! Base class for attribute value.
class CRscAttributeValueBase
{
public:
  //! In which array? If is not in array then it is NULL.
	CRscAttributeValueArray	*_inArray;

  //! In which parameter?
	CRscAttribute			*_inParam;

  //! Postfix comments. Used only in array value element.
	CString					_postComment;

  //! Prefix comments. Used only in array value element and value array.
	CStringArray			_preComments;

public:
	CRscAttributeValueBase (CRscAttributeValueArray *inArray, CRscAttribute *inParam)
		: _inArray (inArray), _inParam (inParam)
	{
	}

	virtual ~CRscAttributeValueBase ()
	{
	}

	virtual bool HasMacro () const = 0;

	virtual bool IsOnlyMacro () const = 0;

	virtual bool IsArray () const
	{
		return false;
	}

	virtual bool IsElement () const
	{
		return false;
	}

	virtual int GetTextLength (const CString &prefix, LPCTSTR tab) const = 0;

	virtual LPTSTR GetText (LPTSTR text, const CString &prefix, LPCTSTR tab) const = 0;

	virtual CRscAttributeValueBase *Copy (CRscAttributeValueArray *inArray, CRscAttribute *inParam) = 0;

	virtual LPCTSTR GetIdValue () const
	{
		return NULL;
	}
};

//! Value of attribute defined by macro calling.
class CRscAttributeValueMacro : public CRscAttributeValueBase
{
public:
  //! Text of the macro calling.
	CString		_calling;

  //! Defines this macro calling an array value? (e.g. arr[] = ARR_IN_MACRO;)
	bool		_isArray;

public:
	CRscAttributeValueMacro (CRscAttributeValueArray *inArray, 
		CRscAttribute *inParam, bool isArray)
		: CRscAttributeValueBase (inArray, inParam), _isArray (isArray)
	{
	}

	CRscAttributeValueMacro (LPCTSTR calling, CRscAttributeValueArray *inArray, 
		CRscAttribute *inParam, bool isArray)
		: CRscAttributeValueBase (inArray, inParam), _calling (calling), _isArray (isArray)
	{
	}

	CRscAttributeValueMacro (const CString &calling, CRscAttributeValueArray *inArray, 
		CRscAttribute *inParam, bool isArray)
		: CRscAttributeValueBase (inArray, inParam), _calling (calling), _isArray (isArray)
	{
	}

	virtual bool HasMacro () const
	{
		return true;
	}

	virtual bool IsOnlyMacro () const
	{
		return true;
	}

	virtual int GetTextLength (const CString &prefix, LPCTSTR tab) const
	{
		return _calling.GetLength ();
	}

	virtual LPTSTR GetText (LPTSTR text, const CString &prefix, LPCTSTR tab) const
	{
		_tcscat (text, _calling);
		return text;
	}

	virtual CRscAttributeValueBase *Copy (CRscAttributeValueArray *inArray, CRscAttribute *inParam)
	{
		CRscAttributeValueMacro *ne = new CRscAttributeValueMacro (_calling, inArray, inParam, _isArray);
		return ne;
	}

	virtual bool IsArray () const
	{
		return _isArray;
	}
};

//! Elementary value of the attribute or array.
class CRscAttributeValueElement : public CRscAttributeValueBase
{
public:
  //! Value (number or text).
	CString _value;

  //! Value without macros.
	CString _unwrappedValue;

  //! Contains macro.
	bool _hasMacro;

  //! Nothing more than macro.
	bool _isOnlyMacro;

public:
	CRscAttributeValueElement (LPCTSTR value, CRscAttributeValueArray *inArray, 
		CRscAttribute *inParam, bool hasMacro = false, bool isOnlyMacro = false)
		: CRscAttributeValueBase (inArray, inParam), _value (value), _hasMacro (hasMacro), _isOnlyMacro (isOnlyMacro)
	{
	}

	bool virtual HasMacro () const
	{
		return _hasMacro;
	}

	bool virtual IsOnlyMacro () const
	{
		return _hasMacro && _isOnlyMacro;
	}

	virtual bool IsElement () const
	{
		return true;
	}

	virtual int GetTextLength (const CString &prefix, LPCTSTR tab) const
	{
		int nl = _tcslen (_T ("\r\n"));
		int pl = prefix.GetLength ();
		int r = 0;
		int n = _preComments.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			r += _preComments[i].GetLength () + nl + pl;
		}
		r += _value.GetLength ();

		return r;
	}

	virtual LPTSTR GetText (LPTSTR text, const CString &prefix, LPCTSTR tab) const
	{
		int n = _preComments.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_tcscat (text, _preComments[i]);
			_tcscat (text, _T ("\r\n"));
			_tcscat (text, prefix);
		}
		_tcscat (text, _value);
		return text;
	}

	virtual CRscAttributeValueBase *Copy (CRscAttributeValueArray *inArray, CRscAttribute *inParam)
	{
		CRscAttributeValueElement *ne = new CRscAttributeValueElement (_value, inArray, inParam, _hasMacro, _isOnlyMacro);
		return ne;
	}

	virtual LPCTSTR GetIdValue () const
	{
		LPCTSTR start = (LPCTSTR) _value;
		TCHAR *end;
		_tcstol (start, &end, 0);
		return start == end ? start : NULL;
	}
};

//! Array of values.
class CRscAttributeValueArray : public CRscAttributeValueBase
{
public:
  //! Members of this array.
	CArray<CRscAttributeValueBase*, CRscAttributeValueBase*> _elements;

public:
	CRscAttributeValueArray (CRscAttributeValueArray *inArray, CRscAttribute *inParam)
		: CRscAttributeValueBase (inArray, inParam)
	{
	}

	virtual ~CRscAttributeValueArray ()
	{
		int n = _elements.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			delete _elements[i];
		}
		_elements.RemoveAll ();
	}

	virtual bool HasMacro () const
	{
		int n = _elements.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			if (_elements[i]->HasMacro ())
			{
				return true;
			}
		}
		return false;
	}

	virtual bool IsOnlyMacro () const
	{
		return false;
	}

	virtual CRscAttributeValueBase *Copy (CRscAttributeValueArray *inArray, CRscAttribute *inParam)
	{
		CRscAttributeValueArray *na = new CRscAttributeValueArray (inArray, inParam);
		if (na == NULL)
		{
			return NULL;
		}

		int n = _elements.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CRscAttributeValueBase *nv = _elements[i]->Copy (na, inParam);
			if (nv != NULL)
			{
				na->_elements.Add (nv);
			}	
			else
			{
				return na;
			}
		}
		return na;
	}

	virtual bool IsArray () const
	{
		return true;
	}

	int GetSize () const
	{
		return _elements.GetSize ();
	}

	CRscAttributeValueBase*& operator [] (int i)
	{
		return _elements[i];
	}
	
	bool IsSingleLine (const CString &prefix, LPCTSTR tab) const;

	virtual int GetTextLength (const CString &prefix, LPCTSTR tab) const
	{
		return IsSingleLine (prefix, tab) ? GetTextLengthSingleLine (prefix, tab) : 
			GetTextLengthMultiLine (prefix, tab);
	}

	int GetTextLengthMultiLine (const CString &prefix, LPCTSTR tab) const;

	int GetTextLengthSingleLine (const CString &prefix, LPCTSTR tab) const;

	virtual LPTSTR GetText (LPTSTR text, const CString &prefix, LPCTSTR tab) const
	{
		return IsSingleLine (prefix, tab) ? GetTextSingleLine (text, prefix, tab) : 
			GetTextMultiLine (text, prefix, tab);
	}

	LPTSTR GetTextSingleLine (LPTSTR text, const CString &prefix, LPCTSTR tab) const;

	LPTSTR GetTextMultiLine (LPTSTR text, const CString &prefix, LPCTSTR tab) const;
};

//! Record of macro calling to know where are macros used from.
class CRscMacroCall
{
public:
  //! Text of this macro call.
	CString _textUse;

  //! Has semicolon at the end.
	bool _semicolon;

  //! Pointer to the included file containing it.
	CRscInclude *_inInclude;

  //! Can be part of another macro calling.
	CRscMacroCall *_inMacroCall;

  //! Prefix comments.
	CStringArray _preComments;

public:
	CRscMacroCall (LPCTSTR textUse, CRscInclude *inInclude, CRscMacroCall *inMacroCall)
		: _textUse (textUse), _semicolon (false), _inInclude (inInclude), _inMacroCall (inMacroCall)
	{
	}

	virtual ~CRscMacroCall ()
	{
	}

	virtual int GetTextLength (const CString &prefix) const
	{
		if (_inMacroCall != NULL)
		{
			return _inMacroCall->GetTextLength (prefix);
		}

		int len = 0;
		int nl = _tcslen (_T ("\r\n"));
		int pl = prefix.GetLength ();
		int n = _preComments.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			len += pl + _preComments[i].GetLength () + nl;
		}
		len += pl + _textUse.GetLength () + (_semicolon ? _tcslen (_T (";\r\n")) : nl);

		return len;
	}

	virtual LPTSTR GetText (LPTSTR text, const CString &prefix) const
	{
		if (_inMacroCall != NULL)
		{
			return _inMacroCall->GetText (text, prefix);
		}

		int n = _preComments.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_tcscat (text, prefix);
			_tcscat (text, _preComments[i]);
			_tcscat (text, _T ("\r\n"));
		}
		_tcscat (text, prefix);
		_tcscat (text, _textUse);
		_tcscat (text, _semicolon ? _T (";\r\n") : _T ("\r\n"));
		return text;
	}

	virtual int GetFullTextLength (const CString &prefix) const
	{
		return GetTextLength (prefix);
	}

	virtual LPTSTR GetFullText (LPTSTR text, const CString &prefix) const
	{
		return GetText (text, prefix);
	}
};

class CRscClass;

//! Item of the CRscClassSpace tree view.
class CRscParamBase : public CRscWorkTreeItem
{
public:
	enum
	{
		_macroBegin			= 1 << 0,
		_macroEnd			= 1 << 1,
	};

	CString				_name;
	CRscClass			*_parent;
	bool				_enabled;
	CRscMacroCall		*_inMacroCall;
	short				_macroFlags;
	CRscInclude			*_inInclude;

public:
	CRscParamBase (LPCTSTR name, bool readOnly, CRscMacroCall *inMacroCall, short macroFlags, CRscInclude *inInclude)
		: _name (name), _parent (NULL), _enabled (true), _inMacroCall (inMacroCall), _macroFlags (macroFlags), 
		_inInclude (inInclude)
	{
	}

	virtual ~CRscParamBase ()
	{
	}

  //! Is this whole item editable.
	virtual bool IsEditable () const
	{
		return _inInclude == NULL && (_inMacroCall == NULL || (~_macroFlags & (_macroBegin | _macroEnd)) == 0);
	}

  //! Is its part editable? (its value)
	virtual bool IsInsideEditable () const
	{
		return _inInclude == NULL && _inMacroCall == NULL;
	}

	virtual bool IsIncluded () const
	{
		return _inInclude != NULL;
	}

  //! Can be something dropped before or behind.
	virtual bool IsDropable (bool after) const
	{
		return _inInclude == NULL && 
			(_inMacroCall == NULL || 
			!after && (_macroFlags & _macroBegin != 0) ||
			after && (_macroFlags & _macroEnd != 0));
	}
	
	virtual void Enable (bool flag)
	{
		_enabled = flag;
	}

	virtual bool IsParam () const
	{
		return true;
	}

	virtual bool IsClass () const = 0;

	virtual bool IsAttribute () const = 0;

	virtual LPCTSTR GetTitle (CString &text) const
	{
		text = _name;
		return text;
	}

	int GetTextLength (LPCTSTR tab) const
	{
		return GetTextLength (_T (""), tab);
	}

	LPTSTR GetText (LPTSTR text, LPCTSTR tab) const
	{
		*text = _T ('\0');
		return GetText (text, _T (""), tab);
	}

	int GetTextLength (const CString &prefix, LPCTSTR tab) const
	{
		if (_inMacroCall != NULL)
		{
			if ((_macroFlags & _macroBegin) != 0)
			{
				return _inMacroCall->GetTextLength (prefix);
			}
			return 0;
		}
		return GetUnwrappedTextLength (prefix, tab);
	}

	LPTSTR GetText (LPTSTR text, const CString &prefix, LPCTSTR tab) const
	{
		if (_inMacroCall != NULL)
		{
			if ((_macroFlags & _macroBegin) != 0)
			{
				return _inMacroCall->GetText (text, prefix);
			}
			return text;
		}
		return GetUnwrappedText (text, prefix, tab);
	}

  //! Version used in drag&drop or copy/paste.
	int GetFullTextLength (LPCTSTR tab) const
	{
		return GetFullTextLength (_T (""), tab);
	}

  //! Version used in drag&drop or copy/paste.
	LPTSTR GetFullText (LPTSTR text, LPCTSTR tab) const
	{
		*text = _T ('\0');
		return GetFullText (text, _T (""), tab);
	}

  //! Version used in drag&drop or copy/paste.
	int GetFullTextLength (const CString &prefix, LPCTSTR tab) const
	{
		if (_inMacroCall != NULL)
		{
			if ((_macroFlags & _macroBegin) != 0)
			{
				return _inMacroCall->GetFullTextLength (prefix);
			}
			return 0;
		}
		return GetUnwrappedFullTextLength (prefix, tab);
	}

  //! Version used in drag&drop or copy/paste.
	LPTSTR GetFullText (LPTSTR text, const CString &prefix, LPCTSTR tab) const
	{
		if (_inMacroCall != NULL)
		{
			if ((_macroFlags & _macroBegin) != 0)
			{
				return _inMacroCall->GetFullText (text, prefix);
			}
			return text;
		}
		return GetUnwrappedFullText (text, prefix, tab);
	}

	virtual CRscParamBase *Copy (CRscInclude *inInclude) = 0;

	virtual CRscParamBase *FindParamInChildren (const char *name, bool inChildren, bool useDisabled)
	{
		if ((_enabled || useDisabled) && _name == name)
		{
			return this;
		}

		return NULL;
	}

protected:
	virtual int GetUnwrappedTextLength (const CString &prefix, LPCTSTR tab) const = 0;
	virtual LPTSTR GetUnwrappedText (LPTSTR text, const CString &prefix, LPCTSTR tab) const = 0;

  //! Version used in drag&drop or copy/paste.
	virtual int GetUnwrappedFullTextLength (const CString &prefix, LPCTSTR tab) const = 0;

  //! Version used in drag&drop or copy/paste.
	virtual LPTSTR GetUnwrappedFullText (LPTSTR text, const CString &prefix, LPCTSTR tab) const = 0;
};

//! Dummy parameter (used while parsing empty macros or __EXEC).
class CRscParamDummy : public CRscParamBase
{
public:
	CRscParamDummy (CRscMacroCall *inMacroCall, short macroFlags, CRscInclude *inInclude)
		: CRscParamBase (_T ("Dummy"), true, inMacroCall, macroFlags, inInclude)
	{
	}

	virtual bool IsClass () const
	{
		return false;
	}

	virtual bool IsAttribute () const
	{
		return false;
	}

	virtual int GetUnwrappedTextLength (const CString &prefix, LPCTSTR tab) const
	{
		return 0;
	}

	virtual LPTSTR GetUnwrappedText (LPTSTR text, const CString &prefix, LPCTSTR tab) const
	{
		return text;
	}

	virtual int GetUnwrappedFullTextLength (const CString &prefix, LPCTSTR tab) const
	{
		return GetUnwrappedTextLength (prefix, tab);
	}

	virtual LPTSTR GetUnwrappedFullText (LPTSTR text, const CString &prefix, LPCTSTR tab) const
	{
		return GetUnwrappedText (text, prefix, tab);
	}

	virtual CRscParamBase *Copy (CRscInclude *inInclude)
	{
		// Macro: koser?
		ASSERT ((_inMacroCall != NULL) && (_macroFlags == (CRscParamBase::_macroBegin | CRscParamBase::_macroEnd)));
		return new CRscParamDummy (_inMacroCall, _macroFlags, inInclude);
	}
};

//! Attribute - can describe some control item property (position, size, id, etc.).
class CRscAttribute : public CRscParamBase
{
public:
  //! Value of the attribute.
	CRscAttributeValueBase *_value;

  //! Prefix comments.
	CStringArray _preComments;

  //! Postfix comments.
	CString _postComment;

  //! List of editable attributes of control item.
	static LPCTSTR const _editables[];

  //! Number of the editable attributes.
	static const int _editablesSize;

public:
	CRscAttribute (LPCTSTR name, CRscAttributeValueBase *value, bool readOnly, CRscMacroCall *inMacroCall, short macroFlags, CRscInclude *inInclude)
		: CRscParamBase (name, readOnly, inMacroCall, macroFlags, inInclude), _value (value)
	{
	}

	virtual ~CRscAttribute ()
	{
		if (_value != NULL)
		{
			delete _value;
			_value = NULL;
		}
	}

	virtual bool IsAttribute () const
	{
		return true;
	}

	virtual bool IsClass () const
	{
		return false;
	}

	virtual bool IsInsideEditable () const
	{
		if (!CRscParamBase::IsInsideEditable ())
		{
			return false;
		}
		if (_value->HasMacro ())
		{
			return false;
		}
		for (int i = 0; ; ++i)
		{
			if (i == _editablesSize)
			{
				return false;
			}
			if (_name.CompareNoCase (_editables[i]) == 0)
			{
				break;
			}
		}
		return true;
	}

	virtual LPCTSTR GetTitle (CString &text) const
	{
		text = _name;

		if (_value == NULL)
		{
			return text;
		}
		if (_value->IsArray ())
		{
			text += _T ("[]");
			return text;
		}

		text += _T (" = ");
		text += ((CRscAttributeValueElement*) _value)->_value;
		return text;
	}

	virtual int GetUnwrappedTextLength (const CString &prefix, LPCTSTR tab) const
	{
		int rl = 0;
		const int nl = _tcslen (_T ("\r\n"));
		const int pl = prefix.GetLength ();
		
		int n = _preComments.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			rl += pl + _preComments[i].GetLength () + nl;
		}
		
		rl += pl + _name.GetLength ();
		
		if (_value == NULL)
		{
			return rl;
		}

		if (_value->IsArray ())
		{
			rl += _tcslen (_T ("[]"));
		}
		rl += _tcslen (_T (" = "));
		rl += _value->GetTextLength (prefix, tab);

		rl += _tcslen (_T (";"));
		if (!_postComment.IsEmpty ())
		{
			rl += _tcslen (_T (" "));
			rl += _postComment.GetLength ();
		}
		rl += nl;

		return rl;
	}

	virtual LPTSTR GetUnwrappedText (LPTSTR text, const CString &prefix, LPCTSTR tab) const 
	{
		int n = _preComments.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_tcscat (text, prefix);
			_tcscat (text, (LPCTSTR) _preComments[i]);
			_tcscat (text, _T ("\r\n"));
		}
		
		_tcscat (text, prefix);
		_tcscat (text, _name);

		if (_value == NULL)
		{
			return text;
		}

		if (_value->IsArray ())
		{
			_tcscat (text, _T ("[]"));
		}
		_tcscat (text, _T (" = "));
		_value->GetText (text, prefix, tab);

		_tcscat (text, _T (";"));
		if (!_postComment.IsEmpty ())
		{
			_tcscat (text, _T (" "));
			_tcscat (text, (LPCTSTR) _postComment);
		}
		_tcscat (text, _T ("\r\n"));
		return text;
	}

	virtual int GetUnwrappedFullTextLength (const CString &prefix, LPCTSTR tab) const
	{
		return GetUnwrappedTextLength (prefix, tab);
	}

	virtual LPTSTR GetUnwrappedFullText (LPTSTR text, const CString &prefix, LPCTSTR tab) const
	{
		return GetUnwrappedText (text, prefix, tab);
	}

	virtual CRscParamBase *Copy (CRscInclude *inInclude)
	{
		CRscAttribute *na = new CRscAttribute (_name, NULL, inInclude != NULL, _inMacroCall, _macroFlags, inInclude);
		if (na == NULL)
		{
			return NULL;
		}
		CRscAttributeValueBase *nv = _value->Copy (NULL, na);
		if (nv == NULL)
		{
			delete na;
			return NULL;
		}
		na->_value = nv;

		return na;
	}
};

//! Class of the enum value object.
class CRscEnumItem : public CRscAttribute
{
public:
	CRscEnumItem (LPCTSTR name, CRscAttributeValueBase *value, bool readOnly, CRscInclude *inInclude)
		: CRscAttribute (name, value, readOnly, NULL, 0, inInclude)
	{
	}

	virtual int GetUnwrappedTextLength (const CString &prefix, LPCTSTR tab) const
	{
		int rl = 0;
		rl += _name.GetLength ();
		
		if (_value == NULL)
		{
			return rl;
		}

		rl += _tcslen (_T (" = "));
		rl += _value->GetTextLength (prefix, tab);

		return rl;
	}

	virtual LPTSTR GetUnwrappedText (LPTSTR text, const CString &prefix, LPCTSTR tab) const 
	{
		_tcscat (text, _name);

		if (_value == NULL)
		{
			return text;
		}

		_tcscat (text, _T (" = "));
		_value->GetText (text, prefix, tab);

		return text;
	}
};

//! Class of the enum object.
class CRscEnum
{
protected:
  //! In included file...
	CRscInclude *_inInclude;

  //! Vlaues of this enumeration.
	CArray<CRscEnumItem*, CRscEnumItem*> _items;

public:
  //! Prefix comments.
	CStringArray _preComments;

  //! Enum name.
	CString _name;

public:
	CRscEnum (CRscInclude *inInclude)
		: _inInclude (inInclude)
	{
	}

	virtual ~CRscEnum ()
	{
		int n = _items.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			delete _items[i];
		}
		_items.RemoveAll ();
	}
	
	bool IsIncluded () const
	{
		return _inInclude != NULL;
	}

	int GetTextLength () const
	{
		int nl = _tcslen (_T ("\r\n"));
		int r = 0;
		
		int n = _preComments.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			r += _preComments[i].GetLength () + nl;
		}

		r += _tcslen (_T ("enum"));
		if (!_name.IsEmpty ())
		{
			r += _tcslen (_T (" "));
			r += _name.GetLength ();
		}
		r += _tcslen (_T ("\r\n{\r\n"));
		n = _items.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			r += _items[i]->GetUnwrappedTextLength (_T (""), _T (""));
			r += _tcslen (_T (",\r\n"));
		}
		r += _tcslen (_T ("};\r\n"));
		return r;
	}

	void GetText (LPTSTR text) const
	{
		_tcscpy (text, _T (""));

		int n = _preComments.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_tcscat (text, (LPCTSTR) _preComments[i]);
			_tcscat (text, _T ("\r\n"));
		}

		_tcscat (text, _T ("enum"));
		if (!_name.IsEmpty ())
		{
			_tcscat (text, _T (" "));
			_tcscat (text, _name);
		}
		_tcscat (text, _T ("\r\n{\r\n"));
		n = _items.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_items[i]->GetUnwrappedText (text, _T (""), _T (""));
			_tcscat (text, _T (",\r\n"));
		}
		_tcscat (text, _T ("};\r\n"));
	}

	bool Contains (LPCTSTR id) const
	{
		int n = _items.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			if (_items[i]->_name.CompareNoCase (id) == 0)
			{
				return true;
			}
		}
		return false;
	}

	CRscEnumItem* Add (LPCTSTR id)
	{
		CRscEnumItem *enumItem = new CRscEnumItem (id, NULL, false, _inInclude);
		if (enumItem == NULL)
		{
			return NULL;
		}
		_items.Add (enumItem);
		return enumItem;
	}
};

class CRscGroup;

//! Class parameter of the resource document.
class CRscClass : public CRscParamBase
{
public:
  //! Base class.
	CRscClass								*_baseClass;

  //! Derived class from this class.
	CArray<CRscClass*, CRscClass*>			_derivedClasses;

  //! Parameters inside this class.
	CArray<CRscParamBase*, CRscParamBase*>	_innerParams;

  //! Item of this class in favourite tree control.
	HTREEITEM								_favouriteTreeItem;

  //! Is selected (in preview)?
	bool									_selected;

  //! Is displyed in preview?
	bool									_displayed;

  //! Rectangle in preview pane.
	RECT									_displayedRect;

  //! Used color for displaying.
	COLORREF								_displayedColor;

  //! Displyed text in preview with the rectangle.
	CString									_displayedText;

  //! Enum code of the item group (objects/controls/background objects).
	int										_itemGroup;

  /*! Pointers to the elemntary attribute values containing reference to this item class. 
      (e.g. from controls[]={item1}; class item1 {}; )
  */
	CArray<CRscAttributeValueElement*,CRscAttributeValueElement*>	_valueReferences;

  //! Group, if any. (Each item can be only in one group.)
	CRscGroup								*_group;

  //! Prefix comments.
	CStringArray						_preComments;

  //! Postfix comments.
	CString									_postComment;

public:
	CRscClass (LPCTSTR name, bool readOnly, CRscMacroCall *inMacroCall, short macroFlags, CRscInclude *inInclude)
		: CRscParamBase (name, readOnly, inMacroCall, macroFlags, inInclude), _baseClass (NULL),
		_favouriteTreeItem (NULL), _selected (false), _displayed (false), 
		_itemGroup (rscEditorItemGroupNone), _group (NULL)
	{
	}

	virtual ~CRscClass ()
	{
		int size = _innerParams.GetSize ();
		for (int i = 0; i < size; ++i)
		{
			delete _innerParams[i];
		}
		_innerParams.RemoveAll ();
	}

	virtual CRscParamBase *Copy (CRscInclude *inInclude)
	{
		return Copy (NULL, inInclude);
	}

	CRscClass *Copy (LPCTSTR name, CRscInclude *inInclude)
	{
		// Macro: koser?
		CRscClass *nc = new CRscClass (name == NULL ? _name : name, inInclude != NULL, _inMacroCall, _macroFlags, inInclude);
		if (nc == NULL)
		{
			return NULL;
		}

		nc->_baseClass = _baseClass;
		int n = _innerParams.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CRscParamBase *dstPar = _innerParams[i]->Copy (inInclude);
			if (dstPar == NULL)
			{
				return nc;
			}
			nc->_innerParams.Add (dstPar);
		}

		return nc;
	}

	virtual void Enable (bool flag)
	{
		_enabled = flag;

		int n = _innerParams.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_innerParams[i]->Enable (flag);
		}
	}

	virtual bool IsClass () const
	{
		return true;
	}

	virtual bool IsAttribute () const
	{
		return false;
	}

	virtual LPCTSTR GetTitle (CString &text) const
	{
		text = _name;
		if (_baseClass != NULL)
		{
			text += _T (" : ");
			text += _baseClass->_name;
		}
		return text;
	}

	virtual int GetUnwrappedTextLength (const CString &prefix, LPCTSTR tab) const
	{
		const int pl = prefix.GetLength ();
		const int nl = _tcslen (_T ("\r\n"));
		int rl = 0;

		int n = _preComments.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			rl += pl + _preComments[i].GetLength () + nl;
		}

		rl += pl + _tcslen (_T ("class "));
		rl += _name.GetLength ();
		if (_baseClass != NULL)
		{
			rl += _tcslen (_T (" : "));
			rl += _baseClass->_name.GetLength ();
		}
		if (!_postComment.IsEmpty ())
		{
			rl += _tcslen (_T (" "));
			rl += _postComment.GetLength ();
		}
		rl += nl;
		rl += pl;
		rl += _tcslen (_T ("{\r\n"));
		n = _innerParams.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			rl += _innerParams[i]->GetTextLength (prefix + tab, tab);
		}
		rl += pl;
		rl += _tcslen (_T ("};"));
		rl += nl;

		return rl;
	}

	virtual LPTSTR GetUnwrappedText (LPTSTR text, const CString &prefix, LPCTSTR tab) const
	{
		int n = _preComments.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_tcscat (text, prefix);
			_tcscat (text, (LPCTSTR) _preComments[i]);
			_tcscat (text, _T ("\r\n"));
		}

		_tcscat (text, prefix);
		_tcscat (text, _T ("class "));
		_tcscat (text, _name);
		if (_baseClass != NULL)
		{
			_tcscat (text, _T (" : "));
			_tcscat (text, _baseClass->_name);
		}
		if (!_postComment.IsEmpty ())
		{
			_tcscat (text, _T (" "));
			_tcscat (text, (LPCTSTR) _postComment);
		}
		_tcscat (text, _T ("\r\n"));
		_tcscat (text, prefix);
		_tcscat (text, _T ("{\r\n"));
		n = _innerParams.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_innerParams[i]->GetText (text, prefix + tab, tab);
		}
		_tcscat (text, prefix);
		_tcscat (text, _T ("};\r\n"));
		return text;
	}

  //! Version used in drag&drop or copy/paste.
	virtual int GetUnwrappedFullTextLength (const CString &prefix, LPCTSTR tab) const
	{
		CString fullName;
		int pl = prefix.GetLength ();
		int nl = _tcslen (_T ("\r\n"));
		int rl = 0;

		int n = _preComments.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			rl += pl + _preComments[i].GetLength () + nl;
		}

		rl += pl + _tcslen (_T ("class "));
		//GetFullName (fullName);
		//rl += fullName.GetLength ();
		rl += _name.GetLength ();
		if (_baseClass != NULL)
		{
			rl += _tcslen (_T (" : "));
			_baseClass->GetFullName (fullName);
			rl += fullName.GetLength ();
		}
		if (!_postComment.IsEmpty ())
		{
			rl += _tcslen (_T (" "));
			rl += _postComment.GetLength ();
		}
		rl += nl;
		rl += pl;
		rl += _tcslen (_T ("{\r\n"));
		n = _innerParams.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			rl += _innerParams[i]->GetFullTextLength (prefix + tab, tab);
		}
		rl += pl;
		rl += _tcslen (_T ("};"));
		rl += nl;

		return rl;
	}

  //! Version used in drag&drop or copy/paste.
	virtual LPTSTR GetUnwrappedFullText (LPTSTR text, const CString &prefix, LPCTSTR tab) const
	{
		int n = _preComments.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_tcscat (text, prefix);
			_tcscat (text, (LPCTSTR) _preComments[i]);
			_tcscat (text, _T ("\r\n"));
		}

		CString fullName;
		_tcscat (text, prefix);
		_tcscat (text, _T ("class "));
		//_tcscat (text, GetFullName (fullName));
		_tcscat (text, _name);
		if (_baseClass != NULL)
		{
			_tcscat (text, _T (" : "));
			_tcscat (text, _baseClass->GetFullName (fullName));
		}
		if (!_postComment.IsEmpty ())
		{
			_tcscat (text, _T (" "));
			_tcscat (text, (LPCTSTR) _postComment);
		}
		_tcscat (text, _T ("\r\n"));
		_tcscat (text, prefix);
		_tcscat (text, _T ("{\r\n"));
		n = _innerParams.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_innerParams[i]->GetFullText (text, prefix + tab, tab);
		}
		_tcscat (text, prefix);
		_tcscat (text, _T ("};\r\n"));
		return text;
	}

	CRscParamBase* FindParamInBases (const char *name, bool inBases, bool useDisabled)
	{
		int n = _innerParams.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CRscParamBase *cur = _innerParams[i];
			if ((cur->_enabled || useDisabled) && cur->_name.CompareNoCase (name) == 0)
			{
				return cur;
			}
		}

		if (inBases && _baseClass != NULL)
		{
			return _baseClass->FindParamInBases (name, inBases, useDisabled);
		}
		
		return NULL;
	}

	bool GetIntParamInBases (const char *name, int &val, bool inBases, bool *notInsideEditable, bool useDisabled)
	{
		CRscParamBase *par = FindParamInBases (name, inBases, useDisabled);
		if (par == NULL ||
			!par->IsAttribute ())
		{
			return false;
		}

		CRscAttributeValueBase *attrVal = ((CRscAttribute*) par)->_value;
		if (attrVal == NULL ||
			!attrVal->IsElement ())
		{
			return false;
		}

		TCHAR *endPtr;
		LPCTSTR str = ((CRscAttributeValueElement*) attrVal)->_value;
		val = _tcstol (str, &endPtr, 0);
		if (notInsideEditable != NULL)
		{
			*notInsideEditable = !par->IsInsideEditable ();
		}
		return str != endPtr;
	}

	bool GetStringParamInBases (const char *name, CString &str, bool inBases, bool *notInsideEditable, bool useDisabled)
	{
		CRscParamBase *par = FindParamInBases (name, inBases, useDisabled);
		if (par == NULL ||
			!par->IsAttribute ())
		{
			return false;
		}

		CRscAttributeValueBase *attrVal = ((CRscAttribute*) par)->_value;
		if (attrVal == NULL ||
			!attrVal->IsElement ())
		{
			return false;
		}

		str = ((CRscAttributeValueElement*) attrVal)->_value;
		if (notInsideEditable != NULL)
		{
			*notInsideEditable = !par->IsInsideEditable ();
		}
		return true;
	}

	bool GetFloatParamInBases (const char *name, double &val, bool inBases, bool *notInsideEditable, bool useDisabled)
	{
		CRscParamBase *par = FindParamInBases (name, inBases, useDisabled);
		if (par == NULL ||
			!par->IsAttribute ())
		{
			return false;
		}

		CRscAttributeValueBase *attrVal = ((CRscAttribute*) par)->_value;
		if (attrVal == NULL ||
			!attrVal->IsElement ())
		{
			return false;
		}

		TCHAR *endPtr;
		LPCTSTR str = ((CRscAttributeValueElement*) attrVal)->_value;
		val = _tcstod (str, &endPtr);
		if (notInsideEditable != NULL)
		{
			*notInsideEditable = !par->IsInsideEditable ();
		}
		return str != endPtr;
	}

	bool SetFloatParamInBases (const char *name, double val, bool inBases, bool useDisabled, CRscEditorDoc *doc, bool canUndo, bool forceUndo);

	bool SetArrayStringParamInBases (const char *name, LPCTSTR val, bool inBases, bool useDisabled, CRscEditorDoc *doc, bool canUndo);

	void RemoveDerivedClass (CRscClass *derClass)
	{
		int n = _derivedClasses.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			if (_derivedClasses[i] == derClass)
			{
				_derivedClasses.RemoveAt (i);
				break;
			}
		}
	}

  //! Returns full absolute name. Used in drag&drop or copy/paste.
	LPCTSTR GetFullName (CString &str) const
	{
		str = _T ("::") + _name;
		CRscClass *cl = _parent;
		while (cl != NULL)
		{
			str = _T ("::") + cl->_name + str;
			cl = cl->_parent;
		}
		return str;
	}

	virtual CRscParamBase *FindParamInChildren (const char *name, bool inChildren, bool useDisabled)
	{
		if ((_enabled || useDisabled) && _name == name)
		{
			return this;
		}

		if (inChildren)
		{
			int n = _innerParams.GetSize ();
			for (int i = 0; i < n; ++i)
			{
				CRscParamBase *fp = _innerParams[i]->FindParamInChildren (name, inChildren, useDisabled);
				if (fp == NULL)
				{
					continue;
				}

				return fp;
			}
		}

		return NULL;
	}
};

class CRscEditorDoc;

//! Base class for iteration through item groups (objects/controls/background controls) of given monitor class.
class CRscClassEnumerator
{
public:
  //! Recursive limit. In OFP is nott deeper than 1, so here it is the same way (see constructor)
	int				_recursiveLimit;
	
protected:
  //! Coordinates of the current item. Set in this class, used in derived.
	double			_x, _y, _w, _h;

  //! Text of the current item.
	CString			_text;

  //! Pointer to the current document.
	CRscEditorDoc	*_doc;

  //! To switch on/off recursion.
	bool			_recursive;

  //! Are elements (x, y, w, h) of the current item editable/read only.
	bool			_readOnly;

  /*! Array of flags of the same size as is number of different item groups. 
      for each item group one bool, if the bool is true then items of the group are displayed.
  */
	bool			*_tags;

  //! Iterate also through disabled items?
	bool			_useDisabled;

  //! Enum code of the chosen item group.
	int				_curItemGroup;

protected:
  //! This method must be overriden in the derived classes.
	virtual bool EnumerateFce (CRscClass *cl, CRscAttributeValueElement *val) = 0;

public:
	CRscClassEnumerator (CRscEditorDoc *doc, bool recursive, bool *tags, bool useDisabled)
		: _doc (doc), _recursive (recursive), _tags (&tags[0]), _useDisabled (useDisabled),
		_curItemGroup (rscEditorItemGroupNone), _recursiveLimit (1)
	{
	}

  //! Main iteration function.
	virtual bool EnumerateClass (CRscClass *topClass, CRscAttributeValueElement *val, int recursiveLevel = 0);

  //! Iterate through some value.
	virtual bool EnumerateValue (CRscClass *topClass, CRscParamBase *curParam, CRscAttributeValueBase *val, int itemGroup, int recursiveLevel);
};

//! Group of control items.
class CRscGroup
{
public:
  //! Owner of this group (this class must be monitor class to be this group useable).
	CRscClass	*_owner;

  //! Members of this group.
	CArray<CRscClass*, CRscClass*> _members;

public:
	CRscGroup (CRscClass *owner)
		: _owner (owner)
	{
	}

  //! Attach this group to its members.
	void Attach ()
	{
		int n = _members.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_members[i]->_group = this;
		}
	}

  //! Detach this group from its members.
	void Detach ()
	{
		int n = _members.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			_members[i]->_group = NULL;
		}
	}

  //! Get the coordinates of this group.
	void GetXYWH (double &px, double &py, double &pw, double &ph,
		bool &bx, bool &by, bool &bw, bool &bh)
	{
		px = 1.0;
		py = 1.0;
		pw = 0.0;
		ph = 0.0;
		bx = by = bw = bh = false;
		double x, y, w, h;
		int n = _members.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			if (_members[i]->GetFloatParamInBases ("x", x, true, NULL, false))
			{
				if (x < px)
				{
					px = x;
					bx = true;
				}
				if (_members[i]->GetFloatParamInBases ("y", y, true, NULL, false))
				{
					if (y < py)
					{
						py = y;
						by = true;
					}
					if (_members[i]->GetFloatParamInBases ("w", w, true, NULL, false))
					{
						if (x + w > pw)
						{
							pw = x + w;
							bw = true;
						}
						if (_members[i]->GetFloatParamInBases ("h", h, true, NULL, false))
						{
							if (y + h > ph)
							{
								ph = y + h;
								bh = true;
							}
						}
					}
				}
			}
		}
		pw -= px;
		ph -= py;
	}

  //! Draw rectangle of this group into rectangle view.
	void Draw (CDC &dc, RECT *view, bool drawDragBoxes);
};

/*! Base class to encapsulate position and size of control item or item group and 
    relative offset to the reference point for chnging properties by mouse dragging.
*/
class CRscLayoutUnit
{
public:
  //! Horizontal delta to the reference point.
	double	_refDeltaX;

  //! Vertical delta to the reference point.
	double	_refDeltaY;

public:
	CRscLayoutUnit () : _refDeltaX (0.0), _refDeltaY (0.0)
	{
	}

	virtual void TransformDone () = 0;
	virtual bool GetX (double &x) = 0;
	virtual bool GetY (double &y) = 0;
	virtual bool GetW (double &w) = 0;
	virtual bool GetH (double &h) = 0;
	virtual bool SetX (double x, bool inBases, bool canUndo) = 0;
	virtual bool SetY (double y, bool inBases, bool canUndo) = 0;
	virtual bool SetW (double w, bool inBases, bool canUndo) = 0;
	virtual bool SetH (double h, bool inBases, bool canUndo) = 0;

  //! Set reference delta to the passed point.
	virtual void SetReferenceDelta (double fx, double fy)
	{
		double x, y;
		if (GetX (x) &&
			GetY (y))
		{
			_refDeltaX = fx - x;
			_refDeltaY = fy - y;
		}
	}
};

//! To change properties of control item stored in resource class.
class CRscLayoutClass : public CRscLayoutUnit
{
public:
  //! Pointer to the particular resource class.
	CRscClass *_class;

  //! Actual document.
	CRscEditorDoc *_doc;

public:
	CRscLayoutClass (CRscClass *cl, CRscEditorDoc *doc)
		: _class (cl), _doc (doc)
	{
	}

	virtual void TransformDone ()
	{
	}

	virtual bool GetX (double &x)
	{
		return _class->GetFloatParamInBases ("x", x, true, NULL, false);
	}

	virtual bool GetY (double &y)
	{
		return _class->GetFloatParamInBases ("y", y, true, NULL, false);
	}

	virtual bool GetW (double &w)
	{
		return _class->GetFloatParamInBases ("w", w, true, NULL, false);
	}

	virtual bool GetH (double &h)
	{
		return _class->GetFloatParamInBases ("h", h, true, NULL, false);
	}

	virtual bool SetX (double x, bool inBases, bool canUndo)
	{
		return _class->SetFloatParamInBases ("x", x, inBases, false, _doc, canUndo, false);
	}

	virtual bool SetY (double y, bool inBases, bool canUndo)
	{
		return _class->SetFloatParamInBases ("y", y, inBases, false, _doc, canUndo, false);
	}

	virtual bool SetW (double w, bool inBases, bool canUndo)
	{
		return _class->SetFloatParamInBases ("w", w, inBases, false, _doc, canUndo, false);
	}

	virtual bool SetH (double h, bool inBases, bool canUndo)
	{
		return _class->SetFloatParamInBases ("h", h, inBases, false, _doc, canUndo, false);
	}
};

//! To change properties of control item group.
class CRscLayoutGroup : public CRscLayoutUnit
{
public:
  //! Pointer to the group.
	CRscGroup *_group;

  //! Actual document.
	CRscEditorDoc *_doc;

  //! Group's properties.
	double _x, _y, _w, _h;

  //! Validity of the group's properties.
	bool _bx, _by, _bw, _bh;

public:
	CRscLayoutGroup (CRscGroup *gr, CRscEditorDoc *doc)
		: _group (gr), _doc (doc)
	{
		_group->GetXYWH (_x, _y, _w, _h, _bx, _by, _bw, _bh);
	}

	virtual void TransformDone ();

	virtual bool GetX (double &x)
	{
		x = _x;
		return _bx;
	}

	virtual bool GetY (double &y)
	{
		y = _y;
		return _by;
	}

	virtual bool GetW (double &w)
	{
		w = _w;
		return _bw;
	}

	virtual bool GetH (double &h)
	{
		h = _h;
		return _bh;
	}

	virtual bool SetX (double x, bool inBases, bool canUndo);
	virtual bool SetY (double y, bool inBases, bool canUndo);
	virtual bool SetW (double w, bool inBases, bool canUndo);
	virtual bool SetH (double h, bool inBases, bool canUndo);
};

//! Each monitor class can contain two different kinds of guides.
enum
{
	GUIDE_VERT = 0,
	GUIDE_HORZ,
};

//! Guide of some monitor class. It contains values of all guide lines.
class CRscGuide
{
public:
  //! Monitor class owning this guide.
	CRscClass*				_owner;

  //! Relative values of vertical guide lines.
	CArray<double, double>	_vert;

  //! Relative values of horizontal guide lines.
	CArray<double, double>	_horz;

  //! Pixel values of vertical guide lines.
	CArray<int, int>		_vertPxl;

  //! Pixel values of horizontal guide lines.
	CArray<int, int>		_horzPxl;

public:
	CRscGuide (CRscClass *owner)
		: _owner (owner)
	{
	}

	void Draw (CDC &dc, RECT *h, RECT *v, RECT *preview);
	void HighlightVert (CDC &dc, RECT *v, RECT *preview, int pxl, int &index);
	void HighlightHorz (CDC &dc, RECT *h, RECT *preview, int pxl, int &index);
};
