
//! Variables defined by __EXEC macro.
class CRscEditorFloatVar
{
public:
  //! It is an item of a list so this is a pointer to the next one.
	CRscEditorFloatVar	*_next;

  //! Variable name.
	CString				_name;

  //! Variable value.
	double				_value;

public:
	CRscEditorFloatVar (LPCTSTR name)
		: _name (name), _next (NULL), _value (0.0)
	{
	}
};

//! Parser of resource code. Handles also __EXEC and __EVAL macro.
class CRscEditorCodeParser : public CRscEditorParser
{
public:
  //! Pointer to the document.
	CRscEditorDoc		*_doc;

  //! List of variables defined by __EXEC macro.
	CRscEditorFloatVar	*_vars;

  //! Pointer to the dirty flag of the document.
	bool				*_docModified;

  //! Flag whether to check class names or not.
	bool				_checkClassName;

  //! Pointer to the old full (absolute) class name.
	CString									*_oldClass;

  //! Pointer to the new full (absolute) class name.
	CString									*_newClass;

  //! Invalid temporary classes.
	CArray<CRscClass*, CRscClass*>			_invalidClasses;

  //! Name of the file for error message.
	TCHAR				_errorFileName[MAX_PATH];

  //! Line number in the file for error message.
	unsigned int		_errorFileLine;

  //! Unwrap macros or remain them untouched?
	bool				_unwrapMacros;

  /*! Flags of this parser. Used for taking the next token in particular situations. 
      Their values are set in constructor based on configuration parameters.
  */
	short				_basicLexFlags;
	short				_basicSaveCharsLexFlags;
	short				_basicSaveCommentsLexFlags;
	short				_basicSaveCommentsHashLexFlags;
	short				_basicSaveCommentsNlsLexFlags;
	short				_basicSaveCommentsNlsHashLexFlags;

  //! If you want to save macro callings and preprocessor hash commands (and remove white characters and all comments).
	static const short	_saveMacroSaveHashLexFlags;

  //! If you want to save macro callings (and remove white characters and all comments and process hash commands).
	static const short	_saveMacroLexFlags;

  //! If you want to save macro callings and white characters (and remove all comments and process all hash commands).
	static const short	_saveMacroSaveCharsLexFlags;

  //! If you want to save macro callings and white characters and all comments (and process all hash commands).
	static const short	_saveMacroSaveCommentsCharsLexFlags;
	
  //! If you want to process macro callings (and remove white characters and all comments and process hash commands).
	static const short	_unwrapMacroLexFlags;

  //! If you want to process macro callings and save all comments (and remove white characters and process all hash commands).
	static const short	_unwrapMacroSaveCommentsLexFlags;

  //! If you want to process macro callings and save all comments and new line characters (and remove white spaces and process all hash commands).
	static const short	_unwrapMacroSaveCommentsNlsLexFlags;

  //! Contains the input any macro?
	bool				_hasMacro;

  //! Is input only macro calling.
	bool				_isOnlyMacro;

  //! If the input is included file then this is pointer to its item.
	CRscInclude			*_inInclude;

protected:
  //! Display message box about unwrapping the macro.
	bool				_showUnwrapMacroBox;

  //! Display message box about removing comments.
	bool				_showRemoveCommentBox;
	
public:
	CRscEditorCodeParser (LPCTSTR srcName, CRscEditorDoc *doc, bool *docModified,
		bool checkClassName, bool unwrapMacros, bool showUnwrapMacroBox, bool showRemoveCommentBox, 
		CRscInclude *inInclude)
		: _doc (doc), _vars (NULL), _docModified (docModified), _checkClassName (checkClassName), 
		_showUnwrapMacroBox (showUnwrapMacroBox), _unwrapMacros (unwrapMacros), _showRemoveCommentBox (showRemoveCommentBox),
		_basicLexFlags (unwrapMacros ? _unwrapMacroLexFlags : _saveMacroLexFlags), 
		_basicSaveCharsLexFlags (_basicLexFlags & ~CRscEditorLex::flagSkipWhiteChars),
		_basicSaveCommentsLexFlags (_basicLexFlags & ~CRscEditorLex::flagAllComments),
		_basicSaveCommentsHashLexFlags (_basicSaveCommentsLexFlags & ~CRscEditorLex::flagHashCallback),
		_basicSaveCommentsNlsLexFlags (_basicSaveCommentsLexFlags & ~CRscEditorLex::flagSkipNewlines),
		_basicSaveCommentsNlsHashLexFlags (_basicSaveCommentsNlsLexFlags & ~CRscEditorLex::flagHashCallback),
		_hasMacro (false), _isOnlyMacro (true), _oldClass (NULL), _newClass (NULL), _inInclude (inInclude)
	{
		_tcscpy (_errorFileName, srcName != NULL ? srcName : _T (""));
	}

	virtual ~CRscEditorCodeParser ()
	{
		int n = _invalidClasses.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			delete _invalidClasses[i];
		}
		_invalidClasses.RemoveAll ();

		CRscEditorFloatVar *var;
		while (_vars != NULL)
		{
			var = _vars->_next;
			delete _vars;
			_vars = var;
		}
		_vars = NULL;
	}

	CRscEditorFloatVar *GetFloatVar (LPCTSTR name) const
	{
		for (CRscEditorFloatVar *v = _vars; v != NULL; v = v->_next)
		{
			if (v->_name != name)
			{
				continue;
			}

			return v;
		}

		return NULL;
	}

	CRscEditorFloatVar *NewFloatVal (LPCTSTR name)
	{
		CRscEditorFloatVar **v = &_vars;
		while (*v != NULL)
		{
			v = &(**v)._next;
		}
		return *v = new CRscEditorFloatVar (name);
	}

	void Execute (const char *expr)
	{
		CRscEditorStringInput in (expr);
		CRscEditorLex lex (&in, NULL, 0);
		CRscEditorToken tok;
		lex.GetNextToken (&tok, this, CRscEditorLex::flagSkipWhiteChars | CRscEditorLex::flagParserCallbacks);
		ParseEXEC (lex, tok, NULL);
	}

	bool ParseEXEC (CRscEditorLex &lex, CRscEditorToken &tok, CString *exprStr)
	{
		if (tok.type != rscEditorLexId)
		{
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;
		}

		CRscEditorFloatVar *v = GetFloatVar (tok.string);

		if (v == NULL)
		{
			v = NewFloatVal (tok.string);
			if (v == NULL)
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}
		}
		if (exprStr != NULL)
		{
			*exprStr += tok.string;
			*exprStr += _T (" = ");
		}

		lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
		if (tok.type != '=')
		{
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;
		}

		// Don't Remember Macro
		lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
		double result = 0.0;
		static const int endTokens[] = {rscEditorLexEof, _T (')'), _T (';')};
		static const int endTokensSize = 3;
		if (!ParseEXPR (lex, tok, result, true, exprStr, NULL, NULL, endTokens, endTokensSize))
		{
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;
		}

		v->_value = result;
		return true;
	}

	bool Evaluate (const char *expr, double &result)
	{
		CRscEditorStringInput in (expr);
		CRscEditorLex lex (&in, NULL, 0);
		CRscEditorToken tok;
		// Don't Remember Macro
		lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
		static const int endTokens[] = {rscEditorLexEof, _T (')'), _T (';')};
		static const int endTokensSize = 3;
		if (!ParseEXPR (lex, tok, result, true, NULL, NULL, NULL, endTokens, endTokensSize))
		{
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;
		}

		return true;
	}

	bool ParseEXPR (CRscEditorLex &lex, CRscEditorToken &tok, double &result, bool initUndeclVars, CString *exprStr, 
		bool *hasMacro, bool *isOnlyMacro, const int* endTokens, const int endTokensSize)
	{
		int braCount = 0;
		CString myExprStr;

		for (;;)
		{
			if (braCount == 0)
			{
				for (int toki = 0; toki < endTokensSize; ++toki)
				{
					if (tok.type == endTokens[toki])
					{
						CRscEditorCodeParser exprParser (NULL, _doc, NULL, false, true, false, false, NULL);
						CRscEditorStringInput exprInput (myExprStr);
						CRscEditorLex exprLex (&exprInput, NULL, 0);
						CRscEditorToken exprTok;
						exprLex.GetNextToken (&exprTok, &exprParser, exprParser._basicLexFlags);
						if (exprParser.ParseExactEXPR (exprLex, exprTok, result, initUndeclVars) && exprTok.type == rscEditorLexEof)
						{
							if (exprStr != NULL)
							{
								*exprStr += myExprStr;
							}
							if (hasMacro != NULL)
							{
								*hasMacro = exprParser._hasMacro;
							}
							if (isOnlyMacro != NULL)
							{
								*isOnlyMacro = exprParser._isOnlyMacro;
							}
							return true;
						}
						return false;
					}
				}
			}

			switch (tok.type)
			{
			case rscEditorLexEof:
			case rscEditorLexError:
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;

			case _T ('('):
				++braCount;
				break;

			case _T (')'):
				--braCount;
				break;
			}

			myExprStr += tok.string;
			lex.GetNextToken (&tok, this, _saveMacroSaveCharsLexFlags);
		}
	}

	bool ParseExactEXPR (CRscEditorLex &lex, CRscEditorToken &tok, double &result, bool initUndeclVars)
	{
		double fpn;

		switch (tok.type)
		{
		case _T ('-'):
		case _T ('+'):
		case rscEditorLexId:
		case rscEditorLexFixedFloat:
		case rscEditorLexLong:
		case '(':
			if (!ParseMUL (lex, tok, result, initUndeclVars))
			{
				return false;
			}
			for (;;)
			{
				switch (tok.type)
				{
				case '+':
					_isOnlyMacro = false;
					lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
					switch (tok.type)
					{
					case rscEditorLexId:
					case rscEditorLexFixedFloat:
					case rscEditorLexLong:
					case '(':
						if (!ParseMUL (lex, tok, fpn, initUndeclVars))
						{
							return false;
						}
						result += fpn;
						break;

					default:
						_errorFileLine = lex.GetLineNumber ();
						ASSERT (0);
						return false;
					}
					break;
					
				case '-':
					_isOnlyMacro = false;
					lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
					switch (tok.type)
					{
					case rscEditorLexId:
					case rscEditorLexFixedFloat:
					case rscEditorLexLong:
					case '(':
						if (!ParseMUL (lex, tok, fpn, initUndeclVars))
						{
							return false;
						}
						result -= fpn;
						break;
						
					default:
						_errorFileLine = lex.GetLineNumber ();
						ASSERT (0);
						return false;
					}
					break;
					
				case rscEditorLexEof:
				case ')':
					return true;

				default:
					_errorFileLine = lex.GetLineNumber ();
					ASSERT (0);
					return false;
				}
			}
			break;

		default:
			_errorFileLine = lex.GetLineNumber ();
			//ASSERT (0);
			return false;
		}
	}

	bool ParseMUL (CRscEditorLex &lex, CRscEditorToken &tok, double &result, bool initUndeclVars)
	{
		double fpn;

		switch (tok.type)
		{
		case _T ('-'):
		case _T ('+'):
		case rscEditorLexId:
		case rscEditorLexFixedFloat:
		case rscEditorLexLong:
		case '(':
			if (!ParseVAL (lex, tok, result, initUndeclVars))
			{
				return false;
			}
			for (;;)
			{
				switch (tok.type)
				{
				case '*':
					_isOnlyMacro = false;
					lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
					switch (tok.type)
					{
					case rscEditorLexId:
					case rscEditorLexFixedFloat:
					case rscEditorLexLong:
					case '(':
						if (!ParseVAL (lex, tok, fpn, initUndeclVars))
						{
							return false;
						}
						result *= fpn;
						break;
						
					default:
						_errorFileLine = lex.GetLineNumber ();
						ASSERT (0);
						return false;
					}
					break;
					
				case '/':
					_isOnlyMacro = false;
					lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
					switch (tok.type)
					{
					case rscEditorLexId:
					case rscEditorLexFixedFloat:
					case rscEditorLexLong:
					case '(':
						if (!ParseVAL (lex, tok, fpn, initUndeclVars))
						{
							return false;
						}
						result /= fpn;
						break;
						
					default:
						_errorFileLine = lex.GetLineNumber ();
						ASSERT (0);
						return false;
					}
					break;
					
				case rscEditorLexEof:
				case ')':
				case '+':
				case '-':
					return true;

				default:
					_errorFileLine = lex.GetLineNumber ();
					//ASSERT (0);
					return false;
				}
			}
			break;

		default:
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;
		}
	}

	bool ParseVAL (CRscEditorLex &lex, CRscEditorToken &tok, double &result, bool initUndeclVars)
	{
		switch (tok.type)
		{
		case _T ('-'):
			_isOnlyMacro = false;
			lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
			if (!ParseVAL (lex, tok, result, initUndeclVars))
			{
				return false;
			}
			result = -result;
			return true;

		case _T ('+'):
			_isOnlyMacro = false;
			lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
			if (!ParseVAL (lex, tok, result, initUndeclVars))
			{
				return false;
			}
			return true;

		case rscEditorLexId:
			{
				if (_tcscmp (tok.string, _T ("__EVAL")) == 0)
				{
					lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
					
					if (tok.type != '(')
					{
						_errorFileLine = lex.GetLineNumber ();
						ASSERT (0);
						return false;
					}
					// Don't Remember Macro
					lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
					
					if (!ParseExactEXPR (lex, tok, result, initUndeclVars))
					{
						return false;
					}
					
					if (tok.type != ')')
					{
						_errorFileLine = lex.GetLineNumber ();
						ASSERT (0);
						return false;
					}
					lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);

					_hasMacro = true;
					return true;
				}

				// find value of this var
				for (CRscEditorFloatVar *v = _vars; v != NULL; v = v->_next)
				{
					if (v->_name != tok.string)
					{
						continue;
					}
					
					_isOnlyMacro = false;
					result = v->_value;
					lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
					return true;
				}

				if (initUndeclVars)
				{
					_isOnlyMacro = false;
					result = 0.0;
					lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
					return true;
				}
				_errorFileLine = lex.GetLineNumber ();
				//ASSERT (0);
				return false;
			}
			
		case rscEditorLexFixedFloat:
		case rscEditorLexLong:
			// return the value
			_isOnlyMacro = false;
			result = tok.fnumber;
			lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
			return true;
			
		case '(':
			// Remember Macro
			_isOnlyMacro = false;
			lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
			if (!ParseExactEXPR (lex, tok, result, initUndeclVars))
			{
				return false;
			}
			if (tok.type != ')')
			{
				_errorFileLine = lex.GetLineNumber ();
				ASSERT (0);
				return false;
			}
			lex.GetNextToken (&tok, this, _unwrapMacroLexFlags);
			return true;
			
		default:
			_errorFileLine = lex.GetLineNumber ();
			ASSERT (0);
			return false;
		}
	}

	virtual bool ParseCOMMENT (CRscEditorLex &lex, CRscEditorToken &tok, CString &comment, short lexFlags);
	bool ParseCOMMENTS (CRscEditorLex &lex, CRscEditorToken &tok, CStringArray &comments, short lexFlags);
	bool ParseExactFULLCLASSNAME (CRscEditorLex &lex, CRscEditorToken &tok, CRscClass *&rc, CString *oldClass, CString *newClass, CString *className);
	bool ParseFULLCLASSNAME (CRscEditorLex &lex, CRscEditorToken &tok, CRscClass *&rc, CString *oldClass, CString *newClass, CString *className);
	bool ParseCLASS (CRscEditorLex &lex, CRscEditorToken &tok, CArray<CRscParamBase*, CRscParamBase*> &arr, int &index, CRscClass *parent, bool readOnly, CString *&rename, CRscMacroCall *inMacroCall, const CStringArray &comments);
	bool ParsePARAMETERS (CRscEditorLex &lex, CRscEditorToken &tok, CArray<CRscParamBase*, CRscParamBase*> &arr, int &index, CRscClass *parent, bool readOnly, CString *rename, CRscMacroCall *inMacroCall, CStringArray *preComments);
	bool ParsePARAMETER (CRscEditorLex &lex, CRscEditorToken &tok, CArray<CRscParamBase*, CRscParamBase*> &arr, int &index, CRscClass *parent, bool readOnly, CString *&rename, CRscMacroCall *inMacroCall, CStringArray *preComments);
	bool ParseMACROCALLING (CRscEditorLex &lex, CRscEditorToken &tok, CString &macroCall, bool hasParams, bool saveHash, CRscMacroParamList *pl = NULL);
	bool ParseMACROCALLINGPARAM (CRscEditorLex &lex, CRscEditorToken &tok, CString &param);
	bool ParseMACROCALLING (CRscEditorLex &lex, CRscEditorToken &tok, CString &macroCall, CRscMacro *macro, CArray<CRscParamBase*, CRscParamBase*> &arr, int &index, CRscClass *parent, bool readOnly, CRscMacroCall *inMacroCall);
	bool ParseARRAYELEMENT (CRscEditorLex &lex, CRscEditorToken &tok, CRscAttributeValueBase **val, CRscAttributeValueArray *inArray, CRscAttribute *inParam);
	bool ParseARRAY (CRscEditorLex &lex, CRscEditorToken &tok, CArray<CRscAttributeValueBase*, CRscAttributeValueBase*> &vals, CRscAttributeValueArray *inArray, CRscAttribute *inParam, CStringArray *outComments, CRscAttributeValueBase *&sub);
	bool ParseELEMENT (CRscEditorLex &lex, CRscEditorToken &tok, CRscAttributeValueElement **val, CRscAttributeValueArray *inArray, CRscAttribute *inParam, const int *endTokens, const int endTokensSize, const short removeCommentFlagsForUnwrap);
	bool ParseELEMENT_Token (CRscEditorLex &lex, CRscEditorToken &tok, CRscAttributeValueElement *elem, const short flags);
	bool ParseMacro (CRscEditorLex *lex, CRscEditorToken *outTok, CRscMacro *&macro);
	bool ParseMacros (CRscEditorLex &lex, CRscEditorToken &tok, CArray<CRscMacro*, CRscMacro*> &arr);

	bool ParseMacroParamList (CRscEditorLex *lex, CRscEditorToken *tok, CRscMacroParamList &pl);
	virtual bool ProcessMacroId (CRscEditorLex *lex, CRscEditorToken *tok, unsigned short flags);
	virtual bool ParseSlashSlash (CRscEditorLex *lex);
	virtual bool ParseSlashStar (CRscEditorLex *lex);
};

/*! Document praser. 
    Parse and process also preprocessor symbols: #include, #define, #ifndef, #ifdef, #else, #endif.
*/
class CRscEditorDocParser : public CRscEditorCodeParser
{
public:
	enum
	{
		preConditionSuccess = 1,
		preConditionFailed = 2,
	};

	CArray<CRscInclude*, CRscInclude*> &_includings;

	CArray<short, short> _preprocConditions;

	CRscEditorDocParser (LPCTSTR srcName, CRscEditorDoc *doc, bool *docModified,
		bool checkClassName, bool unwrapMacros, bool showUnwrapMacroBox, bool showRemoveCommentBox,
		CRscInclude *inInclude, CArray<CRscInclude*, CRscInclude*> &includings)
		: CRscEditorCodeParser (srcName, doc, docModified, checkClassName, unwrapMacros, showUnwrapMacroBox, 
		showRemoveCommentBox, inInclude), _includings (includings)
	{
	}

	bool PreprocCondition (bool condition, CRscEditorLex *lex, CRscEditorToken *outTok, unsigned short flags);
	virtual bool ParseHash (CRscEditorLex *lex, CRscEditorToken *outTok, unsigned short flags);

	bool ParseDOCHash (CRscEditorLex &lex, CRscEditorToken &tok, int *index, CStringArray &preComments);
	bool ParseDOC (CRscEditorLex &lex, CRscEditorToken &tok);
	bool ParseDOC (CRscEditorLex &lex, CRscEditorToken &tok, int &index, bool readOnly);
};

/*! Parser of RED = Resource Editor Documents.
    Parse some information stored in comments (sych as monitor class, group of control items, control item guides, etc.).
*/
class CRscEditorREDParser : public CRscEditorDocParser
{
public:
	CRscEditorREDParser (LPCTSTR srcName, CRscEditorDoc *doc, bool *docModified,
		bool checkClassName, bool unwrapMacros, bool showUnwrapMacroBox, bool showRemoveCommentBox,
		CRscInclude *inInclude, CArray<CRscInclude*, CRscInclude*> &includings)
		: CRscEditorDocParser (srcName, doc, docModified, checkClassName, unwrapMacros, showUnwrapMacroBox, 
		showRemoveCommentBox, inInclude, includings)
	{
	}

	virtual bool ParseCOMMENT (CRscEditorLex &lex, CRscEditorToken &tok, CString &comment, short lexFlags);

	bool ParseREDOptions (CRscEditorLex &lex, CRscEditorToken &tok);
};