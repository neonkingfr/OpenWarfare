
#include "stdafx.h"
#include "RscEditorParser.h"
#include "RscEditorLex.h"


const CRscEditorKeyword CRscEditorLex::rscEditorKeywords[] =
{
	{_T ("include"), rscEditorLex_include},
	{_T ("define"), rscEditorLex_define},
	{_T ("class"), rscEditorLex_class},
	{_T ("enum"), rscEditorLex_enum},
	{_T ("ifndef"), rscEditorLex_ifndef},
	{_T ("ifdef"), rscEditorLex_ifdef},
	{_T ("else"), rscEditorLex_else},
	{_T ("endif"), rscEditorLex_endif},
	{_T ("__F"), rscEditorLex_favourite},
	{_T ("__M"), rscEditorLex_monitor},
	{_T ("__GROUP"), rscEditorLex_group},
	{_T ("__GUIDE"), rscEditorLex_guide},
};

const unsigned int CRscEditorLex::rscEditorKeywordsLength = sizeof (rscEditorKeywords) / sizeof (CRscEditorKeyword);
