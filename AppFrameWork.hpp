#ifndef _MYAPPFRAMEWORK
#define _MYAPPFRAMEWORK

#include <string>

// Global format, where standard template library does not work
std::string Format(const char *format,...);

//escapes all special characters so that *str can be used as a string literal in C++ code
std::string Escape(const char *str);

// Normal message
void LogF(const char* format, ...);

// Warning message dialog box appearance.
void WarningMessage(const char* format, ...);

// Error message program cannot continue operation.
void ErrorMessage(const char* format, ...);

class wxTextCtrl;
class AppFrameWorkBase
{
public:
  virtual void SetTextControl(wxTextCtrl *ctrl) = 0;
  virtual void LogF(const char *msg) = 0;
  virtual void WarningMessage(const char *msg) = 0;
  virtual void ErrorMessage(const char *msg) = 0;
};

#ifndef lenof
#define lenof(x) (sizeof(x)/sizeof(*(x)))
#endif

extern AppFrameWorkBase* GFrameWork;

#if _DEBUG

#define Assert( EXPRESION ) \
if ( !(EXPRESION) ) \
{\
  LogF("%s(%d) : Assertion failed '%s'",__FILE__,__LINE__, #EXPRESION ); \
  __debugbreak();\
}

#else

#define Assert( EXPRESION ) (EXPRESION)

#endif

#define Verify( EXPRESION ) \
if ( !(EXPRESION) ) \
{\
  LogF("%s(%d) : Verify failed '%s'",__FILE__,__LINE__, #EXPRESION ); \
  Assert(false);\
}

#define Fail( EXPRESION ) \
if ( !(EXPRESION) ) \
{\
  ErrorMessage("%s(%d) : Critical failure '%s'",__FILE__,__LINE__, #EXPRESION ); \
}

#endif