#pragma once

#include <El/elementpch.hpp>
#include <El/Evaluator/express.hpp>
#include "../poseidoneditor/DlgScriptOutput.hpp"
#include "../poseidoneditor/PoseidonDoc.h"


class MyGameState : public GameState
{
protected:
  CScriptOutputDlg * _outputDlg;
  CPoseidonDoc * _activeDoc;

public:
  MyGameState() : _outputDlg(NULL),_activeDoc(NULL), GameState() {};

  void SetOutputWnd(CScriptOutputDlg * outDlg) {_outputDlg = outDlg;};
  void SetDoc(CPoseidonDoc * doc) {_activeDoc = doc;};
  CPoseidonDoc * GetDoc() const {return _activeDoc;};

  virtual int MaxIterations() const {return 1000000;};

  void OnEcho(const char *text) {if (_outputDlg) _outputDlg->AddText(CString(text));};
};