#pragma once

#include <El/elementpch.hpp>
#include <el\evaluator\express.hpp>
#include "gdType.hpp"

#define TYPES_EDOBJECT(XX, Category) \
  XX("PosEdObject",EvalType_PosEdObject,CreatePosEdObject,"@PosEdObject","PosEdObject","This type represents PosEdObject.",Category)

TYPES_EDOBJECT(DECLARE_TYPE, Category)

class gdPosEdObject : public GameData
{
protected:
  CPosEdObject * _posEdObj;
public:
  gdPosEdObject() : _posEdObj(NULL) {};
  gdPosEdObject(CPosEdObject * posEdObj) : _posEdObj(posEdObj) {};
  gdPosEdObject(const gdPosEdObject& src) : _posEdObj(src._posEdObj) {};

  CPosEdObject * GetObject() const {return _posEdObj;}

  const GameType &GetType() const {return EvalType_PosEdObject;}
  RString GetText() const {return RString("document");}

  bool IsEqualTo(const GameData *data) const ;
  const char *GetTypeName() const {return "Document";}
  GameData *Clone() const {return new gdPosEdObject (*this);}

  bool GetNil() const {return (_posEdObj == NULL);}

  void SetNil() {_posEdObj = NULL;};
  
  static void RegisterToGameState(GameState *gState);
};