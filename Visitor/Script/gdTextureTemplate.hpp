#include <El/elementpch.hpp>
#include <el\evaluator\express.hpp>
#include "gdType.hpp"
#include "..\poseidoneditor\PoseidonDoc.h"

#define TYPES_TEXTEMPLATE(XX, Category) \
  XX("Texture Template",EvalType_TextureTemplate,CreateTextureTemplate,"@Texture Template","Texture Template","This type represents Texture Template.",Category)

TYPES_TEXTEMPLATE(DECLARE_TYPE, Category)

class gdTextureTemplate :  public GameData
{
protected:
  Ref<CPosTextureTemplate> _templ;    

public:
  gdTextureTemplate(CPosTextureTemplate * templ) : GameData(), _templ(templ) {};
  gdTextureTemplate(const gdTextureTemplate& gTempl) : GameData(), _templ(gTempl._templ) {};

  CPosTextureTemplate * GetObject() const {return _templ;};
  const GameType &GetType() const {return EvalType_TextureTemplate;}
  RString GetText() const{ return _templ ? (LPCSTR)_templ->Name(): "NULL";};

  bool IsEqualTo(const GameData *data) const {return data && data->GetType() == EvalType_TextureTemplate 
    && _templ == (static_cast<const gdTextureTemplate*>(data))->GetObject();};

  const char *GetTypeName() const {return "Texture Template";}
  GameData *Clone() const {return new gdTextureTemplate (*this);}

  //USE_FAST_ALLOCATOR;
  static void RegisterToGameState(GameState *gState);
};

