#include <El/elementpch.hpp>
#include <el\evaluator\express.hpp>
#include "gdType.hpp"
#include "..\poseidoneditor\PoseidonDoc.h"

#define TYPES_TEMPLATE(XX, Category) \
  XX("Object Template",EvalType_ObjectTemplate,CreateObjectTemplate,"@Object Template","Object Template","This type represents Object Template.",Category)\

TYPES_TEMPLATE(DECLARE_TYPE, Category)

class gdObjectTemplate :  public GameData
{
protected:
	Ref<CPosObjectTemplate> _templ;    

public:
	gdObjectTemplate(CPosObjectTemplate* templ) 
	: GameData()
	, _templ(templ) 
	{
	}

	gdObjectTemplate(const gdObjectTemplate& gTempl) 
	: GameData()
	, _templ(gTempl._templ) 
	{
	}

	CPosObjectTemplate* GetObject() const 
	{
		return _templ;
	}

	const GameType &GetType() const 
	{
		return EvalType_ObjectTemplate;
	}

	RString GetText() const
	{ 
		return _templ ? (LPCSTR)_templ->GetName(): "NULL";
	}

	bool IsEqualTo(const GameData* data) const 
	{
		return data && data->GetType() == EvalType_ObjectTemplate && 
			   _templ == (static_cast<const gdObjectTemplate*>(data))->GetObject();
	}

	const char* GetTypeName() const 
	{
		return "Object Template";
	}

	GameData* Clone() const 
	{
		return new gdObjectTemplate (*this);
	}

	//USE_FAST_ALLOCATOR;
	static void RegisterToGameState(GameState* gState);
};


