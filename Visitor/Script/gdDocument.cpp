
#include <stdafx.h>
#include "gdDocument.hpp"
#include "gdPosEdObject.hpp"
#include "MyGameState.hpp"
#include "gdObject.hpp"
#include "gdNet.hpp"
#include <Es/Algorithms/qsort.hpp>

#define Category "Visitor::Document"

static GameData *CreateDocument(ParamArchive *ar) {return new gdDocument(NULL);}

TYPES_DOCUMENT(DEFINE_TYPE, Category)

// null operator getActiveDocument
static GameValue getActiveDoc(const GameState *gs)
{
  gdDocument *data=new gdDocument(((MyGameState *)gs)->GetDoc());
  return GameValue(data);
}

#define NULARS_DEFAULT(XX, Category) \
  XX(EvalType_Document, "getActiveDoc",getActiveDoc , "Returns active document. ", "_doc = getActiveDoc;", "", "", "", Category) 

static GameNular NularyFuncts[]=
{
  NULARS_DEFAULT(REGISTER_NULAR, Category)
};

static GameValue createPosEdObject( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();
  RString templateName = (GameStringType) oper2;

  if (doc)
  { 
    Ref<CPosObjectTemplate> teml = doc->GetObjectTemplateByName(CString(templateName.Data()));
    if (teml.IsNull())
    {
      state->SetError(EvalForeignError,"template does not exist");
      return GameValue(new gdPosEdObject());
    }

    CPosObjectAction * act = (CPosObjectAction *)Action_CreatePoseidonObject(CPoint(0,0),teml, doc, NULL);
    CPosEdObject * obj = doc->m_PoseidonMap.GetPoseidonObject( act->m_pCurValue->GetID());
    return GameValue(new gdPosEdObject(obj));
  }

  state->SetError(EvalForeignError,"empty document");
  return GameValue(new gdPosEdObject());
}

static GameValue getPosEdObject( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();
  int objID = toInt((GameScalarType) oper2);

  if (doc)
  {
    CPosEdObject * obj = doc->m_PoseidonMap.GetPoseidonObject(objID);
    return GameValue(new gdPosEdObject(obj));
  }

  state->SetError(EvalForeignError,"empty document");
  return GameValue(new gdPosEdObject());
}

static GameValue getNet( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();
  int netID = toInt((GameScalarType) oper2);

  if (doc)
  {
    CPosNetObjectBase * net = doc->m_PoseidonMap.GetPoseidonNet(netID);
    return GameValue(new gdNet(net));
  }

  state->SetError(EvalForeignError,"empty document");
  return GameValue(new gdNet());
}

static GameValue selectObjectsInDoc( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper2.GetData())->GetObject();
  const GameArrayType& objs = oper1;

    if (doc)      
    {
      for(int i = 0; i < objs.Size(); i++)
      {
        if (objs[i].GetData()->GetType() == EvalType_PosEdObject)      
        {
          CPosEdObject* obj = static_cast<gdPosEdObject *>(objs[i].GetData())->GetObject();
          doc->AddToObjectSelection(obj, false);
          continue;
        }

        if (objs[i].GetData()->GetType() == EvalType_Net)
        {
          CPosNetObjectBase* obj = static_cast<gdNet *>(objs[i].GetData())->GetObject();
          doc->AddToObjectSelection(obj, false);
          continue;
        }
      }

      doc->UpdateAllViewsSelection();
      return GameValue();
    }

    state->SetError(EvalForeignError,"empty document");
    return GameValue();
}

static GameValue selectObjectInDoc( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper2.GetData())->GetObject();
  CPosEdObject * obj = static_cast<gdPosEdObject *>(oper1.GetData())->GetObject();

  if (!doc)
  {
    state->SetError(EvalForeignError,"empty document");
    return GameValue();
  }

  if (!obj)
  {
    state->SetError(EvalForeignError,"empty object");
    return GameValue();
  }

  doc->AddToObjectSelection(obj, true);
  doc->UpdateAllViewsSelection();
  return GameValue();
}


static GameValue selectAreaInDoc( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper2.GetData())->GetObject();
  CPosAreaObject * area = static_cast<gdAreaObject *>(oper1.GetData())->GetObject();
 
  if (!area)
  {  
    state->SetError(EvalForeignError,"empty area");
    return GameValue();
  }

  if (!doc)
  {  
    state->SetError(EvalForeignError,"empty document");
    return GameValue();
  }
  
  Action_SetPoseidonCurrArea(*area, doc);
  return GameValue();
}

// Information about landscape
static GameValue getLandHeight( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();
  GameArrayType& pos = (oper2.GetData())->GetArray();  

  if (doc)
  {  
    REALPOS realPos;
    realPos.x = pos[0];
    realPos.z = pos[1];
    
    float rdX,rdY;
    float height = doc->m_PoseidonMap.GetLandscapeHeight(realPos, &rdX, &rdY);

    GameArrayType ret;
    ret.Reserve(3,3);
    ret.Add(height);
    ret.Add(rdX);
    ret.Add(rdY);

    return GameValue(ret);
  }

  state->SetError(EvalForeignError,"empty document");
  return GameValue();
}


//information about water
static GameValue getWaterHeight( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();
  GameArrayType& pos = (oper2.GetData())->GetArray();  

  if (doc)
  {  
    REALPOS realPos;
    realPos.x = pos[0];
    realPos.z = pos[1];

    float rdX,rdY;
    float height = doc->m_PoseidonMap.GetWaterLayer().GetHeight(realPos, &rdX, &rdY);

    GameArrayType ret;
    ret.Reserve(3,3);
    ret.Add(height);
    ret.Add(rdX);
    ret.Add(rdY);

    return GameValue(ret);
  }

  state->SetError(EvalForeignError,"empty document");
  return GameValue();
}

static GameValue IsWater( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();
  GameArrayType& pos = (oper2.GetData())->GetArray();  

  if (doc)
  {  
    REALPOS realPos;
    realPos.x = pos[0];
    realPos.z = pos[1];
   
    float waterHeight = doc->m_PoseidonMap.GetWaterLayer().GetHeight(realPos);
    float terrainHeight = doc->m_PoseidonMap.GetLandscapeHeight(realPos);

    if (waterHeight > terrainHeight)
      return true;
    else
      return false;
  }

  state->SetError(EvalForeignError,"empty document");
  return GameValue();
}


// Information about close objects
static GameValue getObjectsIn( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();
  CPosAreaObject * area = static_cast<gdAreaObject *>(oper2.GetData())->GetObject();  

  GameArrayType ret;
  if (doc)
  { 

    for(PosEdObjectIterator iter(doc); iter; ++iter)
    {
      CPosEdObject * obj = iter;
      if (area->IsPointAtAreaReal(obj->GetRealPos()))
        ret.Add(GameValue(new gdPosEdObject(obj)));
    }    

    return GameValue(ret);
  }

  state->SetError(EvalForeignError,"empty document");
  return GameValue();
}

// get Objects created with some template
static GameValue getObjectsWithTemplate( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();
  RString templateName = (GameStringType) oper2; 

  GameArrayType ret;
  if (doc)
  { 

    for(PosEdObjectIterator iter(doc); iter; ++iter)
    {
      CPosEdObject * obj = iter;
      Ref<CPosObjectTemplate> tmpl = obj->GetTemplate();       
      if (tmpl.NotNull() && templateName == (LPCSTR) tmpl->GetName())
        ret.Add(GameValue(new gdPosEdObject(obj)));
    }    

    return GameValue(ret);
  }

  state->SetError(EvalForeignError,"empty document");
  return GameValue();
}
/// sort array... 
class CompareExp
{
protected:
  const GameState& _state;
  RString _expression;
  mutable bool _exited; // if expresion exited, true. 
public:
  CompareExp(const GameState& state, RString& expression) : _state(state), _expression(expression), _exited(false)  {};
  int operator () (const GameValue *a, const GameValue *b) const
  {
    if (_exited)
      return 0;

    GameVarSpace local(_state.GetContext());
    _state.BeginContext(&local);
       
    // set local varible - will be deleted on EndContext
    _state.VarSetLocal("_x",*a,true);
    _state.VarSetLocal("_y",*b,true);
    GameScalarType ret = _state.EvaluateMultiple(_expression);
    
    if( _state.GetLastError() || local._blockExit)
    {
      _exited = true;
      _state.EndContext();
      return 0;
    }       

    _state.EndContext();
    return toInt(ret);
  }
};

static GameValue SortBy( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{  
  RString expression=oper2;
  GameArrayType &array=(oper1.GetData())->GetArray();  

  CompareExp comp(*state,expression);
  QSort<GameValue, CompareExp>(array.Data(), array.Size(), comp);

  return GameValue(array);
}

// Compare strings... 
static GameValue StringCmp( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  RString str1=oper1;
  RString str2=oper2;

  return GameValue((GameScalarType) strcmp(str1, str2));
}

/////////////////////////////////////////////////////
// Manipulate with landscape

static GameValue GetVertexesInArea( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();
  CPosAreaObject * area = static_cast<gdAreaObject *>(oper2.GetData())->GetObject();  

  if (doc == NULL)
  {  
    state->SetError(EvalForeignError,"empty document");
    return GameValue();
  }

  if (area == NULL)
  { 
    state->SetError(EvalForeignError,"empty area");
    return GameValue();
  }
  
  CPoseidonMap& map = doc->m_PoseidonMap;

  UNITPOS	lftTop,rghBtm;
  CRect	rLogHull;
  area->GetObjectHull(rLogHull);
  map.GetUnitPosHull(lftTop,rghBtm,rLogHull);

  int nDimX = (rghBtm.x-lftTop.x)+1;
  int nDimZ = (lftTop.z-rghBtm.z)+1;

  // chybn� dimenze ?
  if ((nDimX <= 0)||(nDimZ <= 0))
  {	
    ASSERT(FALSE);
    return GameValue();
  };

  // na�tu data z mapy
  UNITPOS pos;

  GameArrayType ret;
  for(pos.z=lftTop.z; pos.z>=rghBtm.z; pos.z--)
  {
    for(pos.x=lftTop.x; pos.x<=rghBtm.x; pos.x++)
    {    
      if (area->IsPointAtArea(map.FromUnitToViewPos(pos)))
      {
        // add to ret
        GameArrayType vert;
        REALPOS realPos = map.FromUnitToRealPos(pos);
        vert.Add(realPos.x);
        vert.Add(realPos.z);
        vert.Add(map.GetLandscapeHeight(pos.x,pos.z));

        ret.Add(GameValue(vert));
      }      
    }
  }

  return ret;
}

static GameValue ChangeVertexes( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  GameArrayType &array=(oper1.GetData())->GetArray();  
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper2.GetData())->GetObject();

  if (doc == NULL)
  {  
    state->SetError(EvalForeignError,"empty document");
    return GameValue();
  }

  AutoArray<float> vert;
  vert.Reserve(array.Size()*3, array.Size()*3);

  for(int i = 0; i < array.Size(); i++)
  {
    vert.Add(((GameArrayType&)array[i])[0]);
    vert.Add(((GameArrayType&)array[i])[1]);
    vert.Add(((GameArrayType&)array[i])[2]);
  }

  Action_HeightPoseidonLand(vert, doc);
  return GameValue();
}

// get vertices under object
static GameValue GetVertexesUnderObject( const GameState *state, GameValuePar oper1)
{  
  CPosEdObject * obj = static_cast<gdPosEdObject *>(oper1.GetData())->GetObject();

  if (obj == NULL)
  {  
    state->SetError(EvalForeignError,"empty object");
    return GameValue();
  }

  CPosEdMainDoc * doc = (CPosEdMainDoc *) obj->GetDocument();

  AutoArray<UNITPOS> vertices;
  doc->m_PoseidonMap.GetVertexesInvolved(vertices, *obj->GetLogObjectPosition());

  GameArrayType ret;
  for(int i = 0; i < vertices.Size(); i++)
  {    
    // add to ret
    GameArrayType vert;
    REALPOS realPos = doc->m_PoseidonMap.FromUnitToRealPos(vertices[i]);
    vert.Add(realPos.x);
    vert.Add(realPos.z);
    vert.Add(doc->m_PoseidonMap.GetLandscapeHeight(vertices[i]));

    ret.Add(GameValue(vert));
  }
  
  return ret;
}

// get vertices under Net
static GameValue GetVertexesUnderNet( const GameState *state, GameValuePar oper1)
{  
  CPosNetObjectBase * net = static_cast<gdNet *>(oper1.GetData())->GetObject();

  if (net == NULL)
  {  
    state->SetError(EvalForeignError,"empty net");
    return GameValue();
  }

  CPosEdMainDoc * doc = (CPosEdMainDoc *) net->GetDocument();

  AutoArray<UNITPOS> vertices;
  doc->m_PoseidonMap.GetVertexesInvolved(vertices, *net->GetLogObjectPosition());

  GameArrayType ret;
  for(int i = 0; i < vertices.Size(); i++)
  {    
    // add to ret
    GameArrayType vert;
    REALPOS realPos = doc->m_PoseidonMap.FromUnitToRealPos(vertices[i]);
    vert.Add(realPos.x);
    vert.Add(realPos.z);
    vert.Add(doc->m_PoseidonMap.GetLandscapeHeight(vertices[i]));

    ret.Add(GameValue(vert));
  }

  return ret;
}

#define GET_DOCUMENT(oper) (static_cast<gdDocument *>(oper.GetData())->GetObject())

/// Save/Load passage

static GameValue Save( const GameState *state, GameValuePar oper1)
{
  CPoseidonDoc * doc = GET_DOCUMENT(oper1);
  if (doc == NULL)
  {  
    state->SetError(EvalForeignError,"empty document");
    return GameValue(false);
  }
  return  GameValue(doc->OnSaveDocument(doc->GetPathName()) == TRUE);
}

static GameValue Save( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = GET_DOCUMENT(oper1);
  if (doc == NULL)
  {  
    state->SetError(EvalForeignError,"empty document");
    return GameValue(false);
  }
  return GameValue(doc->OnSaveDocument((LPCSTR)(GameStringType) oper2) == TRUE);
}

static GameValue Load( const GameState *state, GameValuePar oper1)
{
  CPoseidonDoc * doc = GET_DOCUMENT(oper1);
  if (doc == NULL)
  {  
    state->SetError(EvalForeignError,"empty document");
    return GameValue(false);
  }
  return GameValue(doc->ReloadDocument(TRUE) == TRUE);
}

static GameValue Load( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = GET_DOCUMENT(oper1);
  if (doc == NULL)
  {  
    state->SetError(EvalForeignError,"empty document");
    return GameValue(false);
  }
  return GameValue(doc->OnOpenDocument((LPCSTR)(GameStringType) oper2) == TRUE);
}

static GameValue FastLoad( const GameState *state, GameValuePar oper1)
{
  CPoseidonDoc * doc = GET_DOCUMENT(oper1);
  if (doc == NULL)
  {  
    state->SetError(EvalForeignError,"empty document");
    return GameValue(false);
  }
  return  GameValue(doc->ReloadDocument(FALSE) == TRUE);
}

static GameValue FastLoad( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = GET_DOCUMENT(oper1);
  if (doc == NULL)
  {  
    state->SetError(EvalForeignError,"empty document");
    return GameValue(false);
  }
  g_bLoadLODShapes = FALSE;
  BOOL ret = doc->OnOpenDocument((LPCSTR)(GameStringType) oper2);
  g_bLoadLODShapes = TRUE;

  return GameValue(ret == TRUE);
}

static GameValue SetFileName( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = GET_DOCUMENT(oper1);
  if (doc == NULL)
  {  
    state->SetError(EvalForeignError,"empty document");
    return GameValue();
  }

  doc->SetPathName((LPCSTR)(GameStringType) oper2);
  return GameValue();
}


static GameValue GetFileName( const GameState *state, GameValuePar oper1)
{
  CPoseidonDoc * doc = GET_DOCUMENT(oper1);
  if (doc == NULL)
  {  
    state->SetError(EvalForeignError,"empty document");
    return GameValue();
  }

  const CString name = doc->GetPathName();
  return GameValue((LPCSTR)name);
}


#define OPERATORS_DEFAULT(XX, Category) \
  XX(EvalType_PosEdObject, "createPosEdObject",function, createPosEdObject, EvalType_Document , GameString, "Document","name of the object template", "Creates object from object template.", "_object = _document createPosEdObject \"small_house\";", "Reference to the object created from template small_house", "", "", Category) \
  XX(EvalType_Net, "getNet",function, getNet, EvalType_Document , GameScalar, "Document","NetID", "Returns a net with ID equal to NetID.", "_net = _document getNet _ID;", "reference to net", "", "", Category) \
  XX(EvalType_PosEdObject, "getPosEdObject",function, getPosEdObject, EvalType_Document , GameScalar, "Document","ObjectID", "Returns an object with ID equal to ObjectID.", "_object = _document getPosEdObject _ID;", "reference to object", "", "", Category) \
  XX(GameNothing, "selectInDoc",function, selectObjectsInDoc, GameArray, EvalType_Document , "Array of objects","Document", "Selects objects and nets in document. The previously selected objects are not deselected", "_objects = [_document getPosEdObject 123,  _document getNet 321];\n  _objects selectInDoc _document", "", "", "", Category) \
  XX(GameNothing, "selectInDoc",function, selectObjectInDoc, EvalType_PosEdObject, EvalType_Document , "Object","Document", "Selects object in document. The previously selected objects are not deselected", " _object selectInDoc _document", "", "", "", Category) \
  XX(GameNothing, "selectInDoc",function, selectAreaInDoc, EvalType_AreaObject, EvalType_Document, "Area","Document", "Selects area in document.", "_area selectAreaInDoc _document", "", "", "", Category) \
  XX(GameArray, "getLandHeight",function, getLandHeight, EvalType_Document, GameArray, "Document","Position", "Returns heigh and slope of the landscape.", "_res = _document getLandHeight [_xpos, _zpos];", "array, [height, x-slope, z-slope]", "", "", Category) \
  XX(GameArray, "getWaterHeight",function, getWaterHeight, EvalType_Document, GameArray, "Document","Position", "Returns heigh and slope of the water surface.", "_res = _document getWaterHeight [_xpos, _zpos];", "array, [height, x-slope, z-slope]", "", "", Category) \
  XX(GameBool, "isWaterAt",function, IsWater, EvalType_Document, GameArray, "Document","Position", "Returns true if at position is water surface higher than terrain.", "_res = _document isWaterAt [_xpos, _zpos];", "true - if water is over terrain, false otherwise.", "", "", Category) \
  XX(GameArray, "getObjectsIn",function, getObjectsIn, EvalType_Document, EvalType_AreaObject, "Document","Area", "Returns objects, thats position is in the area.", "_objects = _document getObjectsIn _area;", "array with objects (PosEdObjects).", "", "", Category) \
  XX(GameArray, "getObjectsWithTemplate",function, getObjectsWithTemplate, EvalType_Document, GameString, "Document","name of the object template", "Returns objects created from the template with given name.", "_objects = _document getObjectsIn _area;", "array with objects (PosEdObjects).", "", "", Category) \
  XX(GameArray, "sortBy",function, SortBy, GameArray, GameString, "Array","Block", "Sorts the array. The block defines a compare function, it has two parameters _x, _y and must return negative number -- if _x is less than _y, 0 -- if _x == _y and positive number ->if _x is bigger than _y. The sortBy function modifies input array.", "_arr = [3,2,1]; _arr sortBy {private [\"_ret\"];  _ret = 0; if (_x < _y) then {_ret = -1;}; if (_x > _y) then {_ret = 1;};  _ret};", "_arr is [1,2,3]", "", "", "Visitor::Misc") \
  XX(GameScalar, "strCmp",function, StringCmp, GameString, GameString, "String","String", "Compares two strings. Returns a negative number if the first string is less than the second string, 0 if the strings are identical and a positive number otherwise.", "[\"a\" strCmp \"b\",\"a\" strCmp \"a\",\"b\" strCmp \"a\" ", "[-1,0,1]", "", "", "Visitor::Misc") \
  XX(GameArray, "getVerticesUnder",function, GetVertexesInArea, EvalType_Document, EvalType_AreaObject, "Document","Area", "Returns all land vertices in the area. A vertex is represented by an array: index 0 - x coord, index 1 - z coord, index 2 - height.", "_vertices = _doc getVerticesUnder _area;", "array of vertices", "", "", Category) \
  XX(GameNothing, "setVerticesTo",function, ChangeVertexes, GameArray, EvalType_Document, "Vertexes","Document", "Changes height of the given vertices. A vertex is represented by an array: index 0 - x coord, index 1 - z coord, index 2 - height.", "_vertices setVerticesIn _doc;", "","", "", Category) \
  XX(GameBool, "save",function, Save, EvalType_Document, GameString, "Document","Path", "Saves document into the file defined by path.", "_document save \"c:\new_island.pew\";", "true - if it succeeded, false otherwise","", "", Category) \
  XX(GameBool, "load",function, Load, EvalType_Document, GameString, "Document","Path", "Loads document from the file defined by path.", "_document load \"c:\new_island.pew\";", "true - if it succeeded, false otherwise","", "", Category) \
  XX(GameBool, "fastLoad",function, FastLoad, EvalType_Document, GameString, "Document","Path", "Loads document from the file defined by path. It does not updates the object templates from p3d files. ", "_document load \"c:\new_island.pew\";", "true - if it succeeded, false otherwise","", "", Category) \
  XX(GameNothing, "setFileName",function, SetFileName, EvalType_Document, GameString, "Document","Path", "Changes document file name, but the document is not saved.", "_document setFileName \"c:\new_island.pew\";", "","", "", Category)

static GameOperator BinaryFuncts[]=
{
  OPERATORS_DEFAULT(REGISTER_OPERATOR, Category)
};

static GameValue getSelectedArea( const GameState *state, GameValuePar oper1)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();

  if (doc)
  {
    CPosAreaObject * area = &doc->GetSelectedArea();
    return GameValue(new gdAreaObject(area));
  }

  state->SetError(EvalForeignError,"empty document");
  return GameValue(new gdAreaObject());
}

static GameValue getNetCount( const GameState *state, GameValuePar oper1)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();

  if (doc)      
    return GameValue((float) doc->m_PoseidonMap.GetPoseidonNetsCount());
  
  state->SetError(EvalForeignError,"empty document");
  return GameValue((float) 0);
}

static GameValue getPosEdObjectCount( const GameState *state, GameValuePar oper1)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();

  if (doc)      
    return GameValue((float) doc->m_PoseidonMap.GetPoseidonObjectCount());

  state->SetError(EvalForeignError,"empty document");
  return GameValue((float) 0);
}

static GameValue getPosEdObjectIter( const GameState *state, GameValuePar oper1)
{
  PosEdObjectIterator & iter = static_cast<gdDocument *>(oper1.GetData())->GetPosEdObjectIterator();

  return GameValue(new gdPosEdObject((CPosEdObject *) iter));
}

static GameValue resetPosEdObjectIter( const GameState *state, GameValuePar oper1)
{
  PosEdObjectIterator & iter = static_cast<gdDocument *>(oper1.GetData())->GetPosEdObjectIterator();
  iter.Reset();

  return GameValue();
}

static GameValue nextPosEdObjectIter( const GameState *state, GameValuePar oper1)
{
  PosEdObjectIterator & iter = static_cast<gdDocument *>(oper1.GetData())->GetPosEdObjectIterator();
  ++iter;

  return GameValue();
}

static GameValue isPosEdObjectIter( const GameState *state, GameValuePar oper1)
{
  PosEdObjectIterator & iter = static_cast<gdDocument *>(oper1.GetData())->GetPosEdObjectIterator();

  return GameValue((bool) iter);
}

static GameValue getNetIter( const GameState *state, GameValuePar oper1)
{
  PosNetObjectBaseIterator & iter = static_cast<gdDocument *>(oper1.GetData())->GetPosNetObjectBaseIterator();

  return GameValue(new gdNet((CPosNetObjectBase *) iter));
}

static GameValue resetNetIter( const GameState *state, GameValuePar oper1)
{
  PosNetObjectBaseIterator & iter = static_cast<gdDocument *>(oper1.GetData())->GetPosNetObjectBaseIterator();
  iter.Reset();

  return GameValue();
}

static GameValue nextNetIter( const GameState *state, GameValuePar oper1)
{
  PosNetObjectBaseIterator & iter = static_cast<gdDocument *>(oper1.GetData())->GetPosNetObjectBaseIterator();
  ++iter;

  return GameValue();
}

static GameValue isNetIter( const GameState *state, GameValuePar oper1)
{
  PosNetObjectBaseIterator & iter = static_cast<gdDocument *>(oper1.GetData())->GetPosNetObjectBaseIterator();

  return GameValue((bool) iter);
}

static GameValue getSelectedPosEdObjects( const GameState *state, GameValuePar oper1)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();

  if (!doc)      
  {
    state->SetError(EvalForeignError,"empty document");
    return GameValue();
  }

  const CPosGroupObject& objs = doc->GetSelectedObjects();

  // choose only PosEdObject
  GameArrayType ret; 
  for(int i = 0; i < objs.GetGroupSize(); i++)
  {
    if (objs[i]->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
    {
      ret.Add(GameValue(new gdPosEdObject((CPosEdObject *) objs[i])));
    }
  }

  return ret;
}

static GameValue getSelectedNets( const GameState *state, GameValuePar oper1)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();

  if (!doc)      
  {
    state->SetError(EvalForeignError,"empty document");
    return GameValue();
  }

  const CPosGroupObject& objs = doc->GetSelectedObjects();

  // choose only PosEdObject
  GameArrayType ret; 
  for(int i = 0; i < objs.GetGroupSize(); i++)
  {
    if (objs[i]->IsKindOf(RUNTIME_CLASS(CPosNetObjectBase)))
    {
      ret.Add(GameValue(new gdNet((CPosNetObjectBase *) objs[i])));
    }
  }

  return GameValue(ret);
}

static GameValue deselectObjects( const GameState *state, GameValuePar oper1)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();

  if (doc)      
  {
    doc->ClearObjectSelection();
    doc->UpdateAllViewsSelection();
    return GameValue();
  }

  state->SetError(EvalForeignError,"empty document");
  return GameValue();
}

/// get info about terrain
static GameValue getTerrainSize( const GameState *state, GameValuePar oper1)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();

  if (doc)      
  {
    UNITPOS pos = doc->m_PoseidonMap.GetSize();
    GameArrayType ret;
    ret.Add(GameValue((float)pos.x));
    ret.Add(GameValue((float)pos.z));
    ret.Add(GameValue(doc->m_nRealSizeUnit));

    return ret;

  };

  state->SetError(EvalForeignError,"empty document");
  return GameValue();
}

static GameValue Sleep( const GameState *state, GameValuePar oper1)
{
  Sleep((GameScalarType) oper1 * 1000);
  return GameValue();
}


#define FUNCTIONS_DEFAULT(XX, Category) \
  XX(EvalType_AreaObject, "getSelectedArea", getSelectedArea, EvalType_Document, "Document", "Returns currently selected area.", "_area = getSelectedArea _document;", "reference to selected area", "", "", Category) \
  XX(GameScalar, "getNetCount", getNetCount, EvalType_Document, "Document", "Returns the maximal net ID. Not all IDs are used.", "_n = getNetCount _document;", "","", "", Category) \
  XX(GameScalar, "getPosEdObjectCount", getPosEdObjectCount, EvalType_Document, "Document", "Returns the maximal object ID. Not all IDs are used.", "_n = getPosEdObjectCount _document;", "", "", "", Category) \
  XX(EvalType_PosEdObject, "getPosEdObjectIter", getPosEdObjectIter, EvalType_Document, "Document", "Returns an object actually referenced by the object iterator in the document.", "_obj = getPosEdObjectIter _object", "reference to object", "", "", Category) \
  XX(GameNothing, "resetPosEdObjectIter", resetPosEdObjectIter, EvalType_Document, "Document", "Resets the object iterator in the document. The iterator now pointing to the first object (one with lowest ID).", "resetPosEdObjectIter _document;", "", "", "", Category) \
  XX(GameNothing, "nextPosEdObjectIter", nextPosEdObjectIter, EvalType_Document, "Document", "Increments the object iterator in the document.", "nextPosEdObjectIter _document;", "", "", "", Category) \
  XX(GameBool, "isPosEdObjectIter", isPosEdObjectIter, EvalType_Document, "Document", "Returns true if the object iterator in the document points to valid object.", " resetPosEdObjectIter _doc; \n while {isPosEdObjectIter _doc} do {\n _obj = getPosEdObjectIter;\n // ... process object... \n nextPosEdObjectIter _doc;\n};", "", "", "", Category) \
  XX(EvalType_Net, "getNetIter", getNetIter, EvalType_Document, "Document", "Returns an net actually referenced by the net iterator in the document. ", "_net = getNetIter _object;", "reference to net", "", "", Category) \
  XX(GameNothing, "resetNetIter", resetNetIter, EvalType_Document, "Document", "Resets the net iterator in the document. The iterator now pointing to the first net (one with lowest ID).", "resetNetIter _document;", "", "", "", Category) \
  XX(GameNothing, "nextNetIter", nextNetIter, EvalType_Document, "Document", "Increments the net iterator in the document.", "nextNetIter _document", "", "", "", Category) \
  XX(GameBool, "isNetIter", isNetIter, EvalType_Document, "Document", "Returns true if the net iterator in the document points to valid net.", " resetNetIter _doc; \n while {isNetIter _doc} do {\n _net = getNetIter;\n // ... process object... \n nextNetIter _doc;\n};", "", "", "", Category) \
  XX(GameArray, "getSelectedPosEdObjects", getSelectedPosEdObjects, EvalType_Document, "Document", "Returns array filled with currently selected objects in document.", "_objects = getSelectedPosEdObjects _document;", "Array with selected objects.", "", "", Category) \
  XX(GameArray, "getSelectedNets", getSelectedNets, EvalType_Document, "Document", "Returns array filled with currently selected nets in document.", "_nets = getSelectedNets _document;", "Array with selected nets.", "", "", Category) \
  XX(GameNothing, "deselectObjects", deselectObjects, EvalType_Document, "Document", "Cancels object selection in the document.", "deselectObjects _doc;", "", "", "", Category) \
  XX(GameArray, "getVerticesUnder", GetVertexesUnderObject, EvalType_PosEdObject, "Object", "Returns all land vertices belonging to the triangles under object. A vertex is represented by an array: index 0 - x coord, index 1 - z coord, index 2 - height.", "_vertices = getVerticesUnder _object;", "array of vertices", "", "", Category) \
  XX(GameArray, "getVerticesUnder", GetVertexesUnderNet, EvalType_Net, "Net", "Returns all land vertices belonging to the triangles under object. A vertex is represented by an array: index 0 - x coord, index 1 - z coord, index 2 - height.", "_vertices = getVerticesUnder _object;", "array of vertices", "", "", Category) \
  XX(GameArray, "getTerrainSize", getTerrainSize, EvalType_Document, "Document", "Returns dimension of the terrain and the grid.", "_size = getTerrainSize _document;", "array: index 0 = dim in x, index 1 - dim, index 2 - grid", "", "", Category) \
  XX(GameBool, "save", Save, EvalType_Document, "Document", "Saves document.", "save _document;", "true - if it succeeded, false otherwise", "", "", Category) \
  XX(GameBool, "load", Load, EvalType_Document, "Document", "Reloads document.", "load _document;", "true - if it succeeded, false otherwise", "", "", Category) \
  XX(GameBool, "fastLoad", FastLoad, EvalType_Document, "Document", "Reloads document, but do not updates the object templates content.", "fastLoad _document;", "true - if it succeeded, false otherwise", "", "", Category) \
  XX(GameString, "getFileName", GetFileName, EvalType_Document, "Document", "Returns document file name.", "_fileName = getFileName _document;", "document file name", "", "", Category) \
  XX(GameNothing, "~", Sleep, GameScalar, "time in s", "Script is stopped for given time. ", "~ 10;", "", "", "", Category)

static GameFunction UnaryFuncts[] = 
{
  FUNCTIONS_DEFAULT(REGISTER_FUNCTION, Category)
};

#if DOCUMENT_COMREF
static ComRefFunc DefaultComRefFunc[] =
{
  NULARS_DEFAULT(COMREF_NULAR, Category)
  FUNCTIONS_DEFAULT(COMREF_FUNCTION, Category)
  OPERATORS_DEFAULT(COMREF_OPERATOR, Category)
};

static ComRefType DefaultComRefType[] =
{
  TYPES_DOCUMENT(COMREF_TYPE, Category)
  /*  TYPES_DEFAULT_COMB(COMREF_TYPE, "Default")*/
};
#endif

void gdDocument::RegisterToGameState(GameState *gState)
{
  GameState &state = *gState; // helper to make macro works
  TYPES_DOCUMENT(REGISTER_TYPE, Category)

  gState->NewOperators(BinaryFuncts,lenof(BinaryFuncts));
  gState->NewFunctions(UnaryFuncts,lenof(UnaryFuncts));
  gState->NewNularOps(NularyFuncts,lenof(NularyFuncts));
#if DOCUMENT_COMREF
  gState->AddComRefFunctions(DefaultComRefFunc,lenof(DefaultComRefFunc));
  gState->AddComRefTypes(DefaultComRefType,lenof(DefaultComRefType));
#endif
}
