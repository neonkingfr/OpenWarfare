#include <stdafx.h>

#include "script.hpp"
#include <El/Evaluator/evaluatorExpress.hpp>
#include <El/PreprocC/Preproc.h>
#include <Es/Strings/bstring.hpp>
#include <El/Evaluator/Addons/Dialogs/dialogs.h>
#include <El/Evaluator/Addons/StringsArrays/StringsArrays.h>
#include <El/Evaluator/Addons/Streams/gdIOStream.h>
#include <El/Evaluator/Addons/Matrix/gdMatrix.h>
#include "../poseidoneditor/PoseidonDoc.h"
#include "gdDocument.hpp"
#include "gdPosEdObject.hpp"
#include "gdObject.hpp"
#include "gdNet.hpp"
#include "gdObjectTemplate.hpp"
#include "gdTextureTemplate.hpp"
#include "..\poseidoneditor\LogToFunction.hpp"
#include <projects/ObjektivLib/UIDebugger/DllDebuggingSession.h>
#include <el/Pathname/pathname.h>
#include "DlgStopScript.h"


/*#include "..\LODObject.h"
#include "ExampleScriptDebugger.h"

#include "..\o2scriptlib\gdlodobject.h"
#include "..\o2scriptlib\gdSelection.h"
#include "..\o2scriptlib\misc.h"
#include "..\o2scriptlib\gdMatrix.h"
#include "..\o2scriptlib\dialogs.h"
#include "..\o2scriptlib\gdIOStream.h"
#include "ScriptLoader.h"
#include "MassProcess.h"
#include "conio.h"
#include "..\UIDebugger\DllDebuggingSession.h"
#include "..\o2scriptlib\globalState.h"*/


static DllDebuggerProvider SDebugger;

class MinimalDebugger:public ScriptDebuggerBase
{
public:
  virtual void OnTrace() {}
  virtual void OnVariableBreakpoint(const char *name, const GameValue &val) {}
};

class ScriptLoader :public Preproc
{
public:    
  virtual QIStream *OnEnterInclude(const char *filename);
  virtual void OnExitInclude(QIStream *stream);
};

QIStream *ScriptLoader::OnEnterInclude(const char *filename)
{
  QIFStream *stream=new QIFStream();
  //stream->restoreScriptName=curScriptName;
  //curScriptName=Pathname(filename,curScriptName);
  //if (curScriptName.IsNull()) curScriptName=filename;
  stream->open(filename);
  if (stream->fail()) 
  {
    //curScriptName=stream->restoreScriptName;
    delete stream;
    return NULL;
  }  
  return stream;
}

void ScriptLoader::OnExitInclude(QIStream *str)
{
  delete str;
}

void LogToWindow(void * context, const char *format, va_list argptr)
{
  BString<512> buf;    
  vsprintf(buf,format,argptr);
  strcat(buf,"\n");

  ((CScriptOutputDlg *) context)->AddText(CString((const char *)buf));
}

/////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////

ProcessScript::ProcessScript()
{
  _gState.BeginContext(&_gSpace);
}

bool ProcessScript::operator()(CString& name, CPoseidonDoc& doc, CScriptOutputDlg * outDlg)
{
 //GameState gState;

 //GameVarSpace space;
 //gState.BeginContext(&space);
 //ExampleScriptDebugger console_debugger;
 //DllDebuggerProvider dbgprovider;
 //dbgprovider.CreateProvider("UIDebugger.dll");
 //DllDebuggingSession session(dbgprovider);
 //ScriptDebuggerBase *debugger=session;
 //if (debugger==NULL) debugger=&console_debugger;
 //gState.AttachDebugger(debugger);
  
  SRef<DllDebuggingSession> debugsession;
  SRef<ScriptDebuggerBase> simpledebug;
  ScriptDebuggerBase *debugger;

  if (!SDebugger.IsLoaded())
  {
    Pathname dllpath=Pathname::GetExePath();
    dllpath.SetFilename("UIDebugger.dll");
    if (dllpath.BottomTopSearchExist())
    {
      SDebugger.CreateProvider(dllpath);
    }
  }


  _gState.SetOutputWnd(outDlg);
  _gState.SetDoc(&doc);

  AppFrameFunctions* old = CurrentAppFrameFunctions;
  ToFunctionAppFrameFunctions appFrameFn(LogToWindow, (void *) outDlg);
  CurrentAppFrameFunctions = &appFrameFn;

  gdDocument::RegisterToGameState(&_gState);
  gdPosEdObject::RegisterToGameState(&_gState);
  gdAreaObject::RegisterToGameState(&_gState);
  gdNet::RegisterToGameState(&_gState);
  GdDialogs::RegisterToGameState(&_gState);
  gdObjectTemplate::RegisterToGameState(&_gState);
  gdTextureTemplate::RegisterToGameState(&_gState);  
  GdStringsArrays::RegisterToGameState(&_gState);
  GdIOStreamV::RegisterToGameState(&_gState);
  GdDialogs::SetParentWindow(&_gState,(HWND)(*AfxGetMainWnd()));
  GdMatrix::RegisterToGameState(&_gState);
  
  CWaitCursor wait;
  

#if DOCUMENT_COMREF 
  _gState.ComRefDocument();
#endif

  QOStrStream script;
  ScriptLoader scriptLoader;
  if (scriptLoader.Process(&script,name)==false)
  {
    printf("%s(%d) error: Preprocessor error: %d \n",scriptLoader.filename.Data(),scriptLoader.curline,scriptLoader.error);
    return false;
  }

  script.put(0);

  if (SDebugger.IsLoaded()) 
  {
    debugsession=new DllDebuggingSession(SDebugger);    
    debugger=*debugsession;
  }
  else
  {
    simpledebug=new MinimalDebugger;
    debugger=simpledebug;
  }

  _gState.AttachDebugger(debugger);

  DlgStopScript dlg(debugger,Pathname::GetNameFromPath(name),CWnd::GetDesktopWindow());
  

  printf("Running script:%s\n",name);
  GameValue val = _gState.EvaluateMultiple(script.str());
  RString text = val.GetText();
  printf("Result: %s",text.Data());



  _gState.AttachDebugger(0);

  _gState.EndContext();

  CurrentAppFrameFunctions = old;  

  return true;
}
