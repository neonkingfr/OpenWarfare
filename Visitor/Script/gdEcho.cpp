#include <stdafx.h>

#include "gdEcho.hpp"
#include "MyGameState.hpp"


static GameValue Echo( const GameState *state, GameValuePar oper1 )
{
  MyGameState * myState = (MyGameState *) state;
  myState->Echo(oper1.GetText());

  return GameValue();
}

#define FUNCTIONS_DEFAULT(XX, Category) \
  XX(GameNothing, "myecho", Echo, GameString, "text", "Sends any text into the debugger console or the logfile.","echo \"Text in logfile\"", "", "2.00", "", Category)

static GameFunction UnaryFuncts[]=
{
  FUNCTIONS_DEFAULT(REGISTER_FUNCTION, "Fechs")
};

void gdMisc::RegisterToGameState(GameState *gState)
{
  //gState->NewType(TypeDefs[0]);
  //gState->NewOperators(BinaryFuncts,lenof(BinaryFuncts));
  gState->NewFunctions(UnaryFuncts,lenof(UnaryFuncts));
}



