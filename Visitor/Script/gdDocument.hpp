#pragma once

#include <El/elementpch.hpp>
#include <el\evaluator\express.hpp>
#include "gdType.hpp"
#include "..\poseidoneditor\PoseidonDoc.h"

template<class Type, class Traits>
class ObjIterator 
{
protected: 
  CPoseidonDoc * _doc;
  int _lastID;
public:
  ObjIterator(CPoseidonDoc* doc) : _doc(doc),_lastID(-1) {operator++(); };
  void Reset()
  {
    _lastID = -1;
    operator++();    
  }

  operator bool() {return Traits::IsObject(_doc, _lastID);};
  operator typename Type*() {return Traits::GetObject(_doc, _lastID);};
  ObjIterator& operator++() 
  {     
    while (++_lastID < Traits::NObjects(_doc) && !Traits::IsObject(_doc, _lastID) );
    return *this;
  }
};

class PosEdObjectIterTraits
{
public:
  static CPosEdObject * GetObject(CPoseidonDoc* doc, int i) {return doc->m_PoseidonMap.GetPoseidonObject(i);};
  static int NObjects(CPoseidonDoc* doc) {return doc->m_PoseidonMap.GetPoseidonObjectCount();};
  static bool IsObject(CPoseidonDoc* doc, int i) 
  {
    CPosEdObject * obj = doc->m_PoseidonMap.GetPoseidonObject(i);    
    return obj && obj->m_nMasterNet < 0 && obj->m_nMasterWood < 0;
  };
};

class PosNetObjectBaseIterTraits
{
public:
  static CPosNetObjectBase * GetObject(CPoseidonDoc* doc, int i) {return doc->m_PoseidonMap.GetPoseidonNet(i);};
  static bool IsObject(CPoseidonDoc* doc, int i)  {return doc->m_PoseidonMap.GetPoseidonNet(i) != NULL;};
  static int NObjects(CPoseidonDoc* doc) {return doc->m_PoseidonMap.GetPoseidonNetsCount();};
};

typedef ObjIterator<CPosEdObject, PosEdObjectIterTraits> PosEdObjectIterator;
typedef ObjIterator<CPosNetObjectBase, PosNetObjectBaseIterTraits> PosNetObjectBaseIterator;

#define TYPES_DOCUMENT(XX, Category) \
  XX("Document",EvalType_Document,CreateDocument,"@Document","Document","This type represents Visitor document (island).",Category)\

TYPES_DOCUMENT(DECLARE_TYPE, Category)

class gdDocument :  public GameData
{
protected:
  CPoseidonDoc * _doc;  
  PosEdObjectIterator _objIterator;
  PosNetObjectBaseIterator _netIterator;

public:
  gdDocument(CPoseidonDoc * doc) : GameData(), _doc(doc), _objIterator(doc), _netIterator(doc) {};
  gdDocument(const gdDocument& gDoc) : GameData(), _doc(gDoc._doc), _objIterator(gDoc._doc), _netIterator(gDoc._doc) {};

  CPoseidonDoc * GetObject() const {return _doc;};
  const GameType &GetType() const {return EvalType_Document;}
  RString GetText() const {return RString("document");}

  bool IsEqualTo(const GameData *data) const {return false;};
  const char *GetTypeName() const {return "Document";}
  GameData *Clone() const {return new gdDocument (*this);}

  //USE_FAST_ALLOCATOR;

  static void RegisterToGameState(GameState *gState);

  PosEdObjectIterator&  GetPosEdObjectIterator() {return _objIterator;};  
  PosNetObjectBaseIterator& GetPosNetObjectBaseIterator() {return _netIterator;};
};

