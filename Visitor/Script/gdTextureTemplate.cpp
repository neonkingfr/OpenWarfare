#include <stdafx.h>
#include "gdDocument.hpp"
#include "gdPosEdObject.hpp"
#include "gdTextureTemplate.hpp"
#include "MyGameState.hpp"

#define Category "Visitor::TextureTemplate"

#define GET_DOCUMENT(oper) (static_cast<gdDocument *>(oper.GetData())->GetObject())
#define GET_TEXTURETEMPLATE(oper) (static_cast<gdTextureTemplate *>(oper.GetData())->GetObject())

static GameData *CreateTextureTemplate(ParamArchive *ar) {return new gdTextureTemplate(NULL);}

TYPES_TEXTEMPLATE(DEFINE_TYPE, Category)

/// Texture Templates
static GameValue getLandTextureAt( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = GET_DOCUMENT(oper1);
  GameArrayType pos = (GameArrayType) oper2;

  if (pos.Size() < 2)
  {
    state->SetError(EvalForeignError,"empty object");
    return GameValue(new gdTextureTemplate(NULL));
  }

  REALPOS posR((GameScalarType) pos[0], (GameScalarType) pos[1]);
  UNITPOS posU = doc->m_PoseidonMap.FromRealToTextureUnitPos(posR);

 return GameValue(new gdTextureTemplate(doc->m_PoseidonMap.GetLandTextureAt(posU)));
}

static GameValue getBaseTextureAt( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = GET_DOCUMENT(oper1);
  GameArrayType pos = (GameArrayType) oper2;

  if (pos.Size() < 2)
  {
    state->SetError(EvalForeignError,"empty object");
    return GameValue(new gdTextureTemplate(NULL));
  }

  REALPOS posR((GameScalarType) pos[0], (GameScalarType) pos[1]);
  UNITPOS posU = doc->m_PoseidonMap.FromRealToTextureUnitPos(posR);

  return GameValue(new gdTextureTemplate(doc->m_PoseidonMap.GetBaseTextureAt(posU)));
}

static GameValue getPrimTextureAt( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPoseidonDoc * doc = GET_DOCUMENT(oper1);
  GameArrayType pos = (GameArrayType) oper2;

  if (pos.Size() < 2)
  {
    state->SetError(EvalForeignError,"empty object");
    return GameValue(new gdTextureTemplate(NULL));
  }

  REALPOS posR((GameScalarType) pos[0], (GameScalarType) pos[1]);
  UNITPOS posU = doc->m_PoseidonMap.FromRealToTextureUnitPos(posR);

  return GameValue(new gdTextureTemplate(doc->m_PoseidonMap.GetPrimTextureAt(posU)));
}



#define OPERATORS_DEFAULT(XX, Category) \
  XX(GameArray, "getLandTexture",function, getLandTextureAt, EvalType_Document, GameArray, "Document", "position", "Returns the texture template under given position.", "_texture = _doc getLandTexture [_xpos,_zpos];", "texture", "", "", Category) \
  XX(GameArray, "getBaseTexture",function, getBaseTextureAt, EvalType_Document, GameArray, "Document", "position", "Returns the template of the base texture under given position.", "_texture = _doc getBaseTexture [_xpos,_zpos];", "texture", "", "", Category) \
  XX(GameArray, "getPrimTexture",function, getPrimTextureAt, EvalType_Document, GameArray, "Document", "position", "Returns the primary texture template of the texture under given position.", "_texture = _doc getPrimaryTexture [_xpos,_zpos];", "texture", "", "", Category)

static GameOperator BinaryFuncts[]=
{
  OPERATORS_DEFAULT(REGISTER_OPERATOR, Category)
};



static GameValue getName( const GameState *state, GameValuePar oper1)
{
  CPosTextureTemplate * templ = GET_TEXTURETEMPLATE(oper1);
  if (templ)
    return GameValue((const char *)templ->Name());
  else
    state->SetError(EvalForeignError,"empty object");
  return GameValue();
}

static GameValue getTextureFileName( const GameState *state, GameValuePar oper1)
{
  CPosTextureTemplate * templ = GET_TEXTURETEMPLATE(oper1);
  if (templ)
  {
    CString text;
    templ->GetBuldozerTextureFileName(text);

    return GameValue((const char *)text);
  }
  else
    state->SetError(EvalForeignError,"empty object");
  return GameValue();
}

static GameValue getRvMatFileName( const GameState *state, GameValuePar oper1)
{
  CPosTextureTemplate * templ = GET_TEXTURETEMPLATE(oper1);
  if (templ)
  {
    CString text;
    templ->GetBuldozerRvMatFileName(text);

    return GameValue((const char *)text);
  }   
  else
    state->SetError(EvalForeignError,"empty object");
  return GameValue();
}

static GameValue getBimPasFileName( const GameState *state, GameValuePar oper1)
{
  CPosTextureTemplate * templ = GET_TEXTURETEMPLATE(oper1);
  if (templ)
  {
    CString text;
    templ->GetBuldozerBimPasFileName(text);

    return GameValue((const char *)text);
  }
  else
    state->SetError(EvalForeignError,"empty object");
  return GameValue();
}

static GameValue getZoneName( const GameState *state, GameValuePar oper1)
{
  CPosTextureTemplate * templ = GET_TEXTURETEMPLATE(oper1);
  if (templ)
  {
    
    CPosTextureZone * zone = templ->GetZone();
    if (zone)
      return GameValue((const char *)zone->GetName());
    else
      return GameValue("");
  }
  else
    state->SetError(EvalForeignError,"empty object");
  return GameValue();
}

static GameValue getLandName( const GameState *state, GameValuePar oper1)
{
  CPosTextureTemplate * templ = GET_TEXTURETEMPLATE(oper1);
  if (templ)
  {
    CPosTextureZone * zone = templ->GetZone();
    if (zone)
    {
      CPosTextureLand * land = zone->GetLand();
      if (land)
        return GameValue((const char *)land->GetName());
    }
    
    return GameValue("");
  }
  else
    state->SetError(EvalForeignError,"empty object");
  return GameValue();
}

#define FUNCTIONS_DEFAULT(XX, Category) \
  XX(GameString, "getName", getName, EvalType_TextureTemplate, "Texture template", "Returns texture template name.", "_name = getName _texture;", "texture template name", "", "", Category) \
  XX(GameString, "getTextureFileName", getTextureFileName, EvalType_TextureTemplate, "Texture template", "Returns texture file name.", "_name = getTextureFileName _texture;", "texture file name", "", "", Category) \
  XX(GameString, "getRvMatFileName", getRvMatFileName, EvalType_TextureTemplate, "Texture template", "Returns RvMat file name.", "_name = getRvMatFileName _texture;", "RvMat file name", "", "", Category) \
  XX(GameString, "getBimPassFileName", getBimPasFileName, EvalType_TextureTemplate, "Texture template", "Returns BimPass file name.", "_name = getBimPassFileName _texture;", "BimPass file name", "", "", Category) \
  XX(GameString, "getSurfaceName", getZoneName, EvalType_TextureTemplate, "Texture template", "Returns name of a surface that contains current texture.", "_name = getSurfaceName _texture;", "Surface file name", "", "", Category) \
  XX(GameString, "getTerrainName", getLandName, EvalType_TextureTemplate, "Texture template", "Returns name of a terrain that contains current texture.", "_name = getTerrainName _texture;", "Terrain file name", "", "", Category)
  

static GameFunction UnaryFuncts[] = 
{
  FUNCTIONS_DEFAULT(REGISTER_FUNCTION, Category)
};


#if DOCUMENT_COMREF
static ComRefFunc DefaultComRefFunc[] =
{
  //NULARS_DEFAULT(COMREF_NULAR, Category)
  FUNCTIONS_DEFAULT(COMREF_FUNCTION, Category)
    OPERATORS_DEFAULT(COMREF_OPERATOR, Category)
};

static ComRefType DefaultComRefType[] =
{
  TYPES_TEXTEMPLATE(COMREF_TYPE, Category)
    /*  TYPES_DEFAULT_COMB(COMREF_TYPE, "Default")*/
};
#endif

void gdTextureTemplate::RegisterToGameState(GameState *gState)
{
  GameState &state = *gState; // helper to make macro works
  TYPES_TEXTEMPLATE(REGISTER_TYPE, Category)

  gState->NewOperators(BinaryFuncts,lenof(BinaryFuncts));
  gState->NewFunctions(UnaryFuncts,lenof(UnaryFuncts));
  //gState->NewNularOps(NularyFuncts,lenof(NularyFuncts));
#if DOCUMENT_COMREF
  gState->AddComRefFunctions(DefaultComRefFunc,lenof(DefaultComRefFunc));
  gState->AddComRefTypes(DefaultComRefType,lenof(DefaultComRefType));
#endif
}
