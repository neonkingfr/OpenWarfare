#include <stdafx.h>
#include "gdDocument.hpp"
#include "gdPosEdObject.hpp"
#include "gdObjectTemplate.hpp"
#include "MyGameState.hpp"

//#include "gdObject.hpp"
//#include "gdNet.hpp"
//#include <Es/Algorithms/qsort.hpp>

#define Category "Visitor::ObjectTemplate"

#define GET_DOCUMENT(oper) (static_cast<gdDocument *>(oper.GetData())->GetObject())
#define GET_OBJECTTEMPLATE(oper) (static_cast<gdObjectTemplate *>(oper.GetData())->GetObject())

static GameData* CreateObjectTemplate(ParamArchive *ar) {return new gdObjectTemplate(NULL);}

TYPES_TEMPLATE(DEFINE_TYPE, Category)

//#define NULARS_DEFAULT(XX, Category) \
  //XX(EvalType_Document, "getActiveDoc",getActiveDoc , "Returns active document. ", "_doc = getActiveDoc;", "", "", "", Category) 

/*static GameNular NularyFuncts[]=
{
  NULARS_DEFAULT(REGISTER_NULAR, Category)
};*/

/// Object Templates
static GameValue getObjectTemplates(const GameState* state, GameValuePar oper1, GameValuePar oper2)
{
	CPoseidonDoc* doc = static_cast<gdDocument*>(oper1.GetData())->GetObject();
	RStringI type = (GameStringType) oper2;

	int itype;
	if (type == RStringI("natural"))
		itype = CPosObjectTemplate::objTpNatural;
	else if (type == RStringI("artificial"))
		itype = CPosObjectTemplate::objTpPeople;
	else if (type == RStringI("road"))
		itype = CPosObjectTemplate::objTpNewRoads;
	else if (type == RStringI("net"))
		itype = CPosObjectTemplate::objTpNet;
	else if (type == RStringI("wood"))
		itype = CPosObjectTemplate::objTpWood;
	else if (type == RStringI("undef"))
		itype = CPosObjectTemplate::objTpUndef;
	else if (type == RStringI("all"))
		itype = -1;
	else 
	{
		state->SetError(EvalForeignError, "wrong template type");
		return GameValue();
	}

	if (doc)      
	{
		GameArrayType ret;
		for (int i = 0; i < doc->ObjectTemplatesSize(); ++i)
		{
			CPosObjectTemplate* templ = (CPosObjectTemplate*)(doc->GetIthObjectTemplate(i));
			if (templ)
			{
				if (itype < 0 || templ->m_nObjType == itype)
				{                         
					ret.Add(GameValue(new gdObjectTemplate(templ)));
				}         
			}
		}
		return GameValue(ret);
	}

	state->SetError(EvalForeignError, "empty document");
	return GameValue();
}

static GameValue setName( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	CPosObjectTemplate* templ = GET_OBJECTTEMPLATE(oper1);
	if (templ)
	{
		templ->SetName((const char*)(GameStringType) oper2); // TODO: undo/redo
		if (templ->GetDocument()) templ->GetDocument()->SetModifiedFlag();
	}
	else
	{
		state->SetError(EvalForeignError, "empty object");
	}
	return GameValue();
}

static GameValue setFileName(const GameState* state, GameValuePar oper1, GameValuePar oper2)
{
	CPosObjectTemplate* templ = GET_OBJECTTEMPLATE(oper1);
	if (templ)      
	{
		templ->SetFileName((const char*)(GameStringType) oper2); // TODO: undo/redo
		if (templ->GetDocument()) templ->GetDocument()->SetModifiedFlag();
	}
	else
	{
		state->SetError(EvalForeignError, "empty object");
	}

	return GameValue();
}

#define OPERATORS_DEFAULT(XX, Category) \
  XX(GameArray, "getObjectTemplates",function, getObjectTemplates, EvalType_Document, GameString, "Document", "Template type", "Returns array filled by the object templates. Object type can be one of the values: natural, artificial, net, wood, undef, all.", "_templates = _doc getObjectTemplates \"all\";", "array of object templates.", "", "", Category) \
  XX(GameNothing, "setName",function, setName, EvalType_ObjectTemplate, GameString, "Object template", "Name", "Changes object template name.", "_template setName \"sandokan\";", "", "", "", Category) \
  XX(GameNothing, "setModelFile",function, setFileName, EvalType_ObjectTemplate, GameString, "Object template", "File name", "Changes object template p3d file","_template setModelFile \"sandokan.p3d\";", "", "", "", Category)

static GameOperator BinaryFuncts[]=
{
	OPERATORS_DEFAULT(REGISTER_OPERATOR, Category)
};

// object templates
static GameValue getNatureObjectTemplates(const GameState* state, GameValuePar oper1)
{
	return getObjectTemplates(state, oper1, GameValue("natural"));  
}

static GameValue getArtificialObjectTemplates(const GameState* state, GameValuePar oper1)
{
	return getObjectTemplates(state, oper1, GameValue("artificial"));  
}

static GameValue getRoadObjectTemplates(const GameState* state, GameValuePar oper1)
{
	return getObjectTemplates(state, oper1, GameValue("road"));  
}

static GameValue getName(const GameState* state, GameValuePar oper1)
{
	CPosObjectTemplate* templ = GET_OBJECTTEMPLATE(oper1);
	if (templ)
	{
		return GameValue((const char*)templ->GetName());
	}
	else
	{
		state->SetError(EvalForeignError, "empty object");
	}
	return GameValue();
}

static GameValue getFileName(const GameState* state, GameValuePar oper1)
{
	CPosObjectTemplate* templ = GET_OBJECTTEMPLATE(oper1);
	if (templ)
	{
		return GameValue((const char*)templ->GetFileName());
	}
	else
	{
		state->SetError(EvalForeignError, "empty object");
	}
	return GameValue();
}

#define FUNCTIONS_DEFAULT(XX, Category) \
  XX(GameArray, "getNatureObjectTemplates", getNatureObjectTemplates, EvalType_Document, "Document", "Returns array filled by the nature object templates.", "_templates = getNatureObjectTemplates _doc;", "", "", "", Category) \
  XX(GameArray, "getArtificialObjectTemplates", getArtificialObjectTemplates, EvalType_Document, "Document", "Returns array filled by the artificial object template templates.", "_templates = getArtificialObjectTemplates _doc;", "", "", "", Category) \
  XX(GameArray, "getRoadObjectTemplates", getRoadObjectTemplates, EvalType_Document, "Document", "Returns array filled by the road object template templates.", "_templates = getRoadObjectTemplates _doc;", "", "", "", Category) \
  XX(GameString, "getName", getName, EvalType_ObjectTemplate, "Object template", "Returns object template name;", "_name = getName _template;", "", "", "", Category) \
  XX(GameString, "getModelFile", getFileName, EvalType_ObjectTemplate, "Object template", "Returns object template model file name.", "_name = getModelFile _template;", "", "", "", Category)

static GameFunction UnaryFuncts[] = 
{
	FUNCTIONS_DEFAULT(REGISTER_FUNCTION, Category)
};

#if DOCUMENT_COMREF
static ComRefFunc DefaultComRefFunc[] =
{
  //NULARS_DEFAULT(COMREF_NULAR, Category)
  FUNCTIONS_DEFAULT(COMREF_FUNCTION, Category)
  OPERATORS_DEFAULT(COMREF_OPERATOR, Category)
};

static ComRefType DefaultComRefType[] =
{
  TYPES_TEMPLATE(COMREF_TYPE, Category)
    /*  TYPES_DEFAULT_COMB(COMREF_TYPE, "Default")*/
};
#endif

void gdObjectTemplate::RegisterToGameState(GameState *gState)
{
  GameState &state = *gState; // helper to make macro works
  TYPES_TEMPLATE(REGISTER_TYPE, Category)

	gState->NewOperators(BinaryFuncts, lenof(BinaryFuncts));
	gState->NewFunctions(UnaryFuncts, lenof(UnaryFuncts));
  //gState->NewNularOps(NularyFuncts,lenof(NularyFuncts));
#if DOCUMENT_COMREF
	gState->AddComRefFunctions(DefaultComRefFunc,lenof(DefaultComRefFunc));
	gState->AddComRefTypes(DefaultComRefType,lenof(DefaultComRefType));
#endif
}