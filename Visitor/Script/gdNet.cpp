#include <stdafx.h>
#include "gdNet.hpp"
#include "gdPosEdObject.hpp"
#include "MyGameState.hpp"

#define Category "Visitor::Net"

bool gdNet::IsEqualTo(const GameData *data) const 
{
  if (data->GetType() != EvalType_Net)
    return false;

  gdNet * net = (gdNet *) data;
  return _net == net->_net;
}


static GameValue doesObjectIntersect( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPosEdObject * obj = static_cast<gdPosEdObject *>(oper1.GetData())->GetObject();
  CPosNetObjectBase * net = static_cast<gdNet *>(oper2.GetData())->GetObject();
  
  if (!obj)
  {
    state->SetError(EvalForeignError,"empty object");
    return GameValue(false);
  }

  if (!net)
  {
    state->SetError(EvalForeignError,"empty net");
    return GameValue(false);
  }

  return GameValue(net->IsIntersection(*obj));
}

// Walking on nets,
static GameValue getWalkPoints( const GameState *state, GameValuePar oper1)
{
  CPosNetObjectBase * net = static_cast<gdNet *>(oper1.GetData())->GetObject();

  if (net)
  {
    AutoArray<NetWalkPoint> pnts = net->GetWalkPoints();
    GameArrayType ret;

    for(int i = 0; i < pnts.Size(); i++)
    {
      NetWalkPoint& nwp = pnts[i];
      GameArrayType array;
      array.Add(GameValue(nwp._pos.x));
      array.Add(GameValue(nwp._pos.z));
      array.Add(GameValue(nwp._grad));
      array.Add(GameValue((float) nwp._partIndx));
      array.Add(GameValue((float) nwp._dirIndx));

      ret.Add(GameValue(array));
    }

    return GameValue(ret);
  }

  state->SetError(EvalForeignError,"empty net");
  return GameValue();
}

static GameValue advanceWalkPoint( const GameState *state, GameValuePar oper1,  GameValuePar oper2)
{
  CPosNetObjectBase * net = static_cast<gdNet *>(oper1.GetData())->GetObject();
  GameArrayType& arr = (oper2.GetData())->GetArray();

  if (net)
  {
    float dist = (float)  arr[1];
    if (dist < 0)
    {
      state->SetError(EvalForeignError,"distance must be positive");
      return GameValue(false);
    }

    GameArrayType& wptS = arr[0];
    NetWalkPoint wpt;
    wpt._pos.x = (float) wptS[0];
    wpt._pos.z = (float) wptS[1];
    wpt._grad = (float) wptS[2];
    wpt._partIndx = toInt(wptS[3]);
    wpt._dirIndx = toInt(wptS[4]);

    bool ret = net->AdvanceWalkPoint(wpt,(float)  arr[1]);
    
    wptS[0] = GameValue(wpt._pos.x);
    wptS[1] = GameValue(wpt._pos.z);
    wptS[2] = GameValue(wpt._grad);
    wptS[3] = GameValue((float) wpt._partIndx);         

    return GameValue(ret);
  }

  state->SetError(EvalForeignError,"empty net");
  return GameValue(false);
}

// positions and orientations 
static GameValue getNetPos( const GameState *state, GameValuePar oper1 )
{
  CPosNetObjectBase * net = static_cast<gdNet *>(oper1.GetData())->GetObject();

  if (net)
  {
    REALPOS pos = net->GetNetRealPos().nPos;
    GameArrayType array;

    array.Add(pos.x);
    array.Add(pos.z);

    return GameValue(array);
  }

  state->SetError(EvalForeignError,"empty net");
  return GameValue();
}


static GameValue setNetPos( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPosNetObjectBase * net = static_cast<gdNet *>(oper1.GetData())->GetObject();
  GameArrayType& array = (GameArrayType) oper2;

  if (net)
  {
    REALPOS pos;
    pos.x = (float) array[0];
    pos.z = (float) array[1];    

    Action_SetPosRealNet(net, pos);

    return GameValue();
  }

  state->SetError(EvalForeignError,"empty object");
  return GameValue();
}

static GameValue getNetOrientationXZ( const GameState *state, GameValuePar oper1 )
{
  CPosNetObjectBase * net = static_cast<gdNet *>(oper1.GetData())->GetObject();

  if (net)
  {   
    return GameValue((GameScalarType) net->GetNetRealPos().nGra);
  }

  state->SetError(EvalForeignError,"empty net");
  return GameValue();
}


static GameValue setNetOrientationXZ( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPosNetObjectBase * net = static_cast<gdNet *>(oper1.GetData())->GetObject();
  int grad = toInt((GameScalarType) oper2);

  if (net)
  {
    grad -= net->GetNetRealPos().nGra;
    Action_RotateNet(net, grad);

    return GameValue();
  }

  state->SetError(EvalForeignError,"empty object");
  return GameValue();
}

static GameValue lockNet( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{  
  CPosNetObjectBase * obj = static_cast<gdNet *>(oper1.GetData())->GetObject();

  if (!obj)
  {
    state->SetError(EvalForeignError,"empty object");
    return GameValue(false);
  }

  Action_LockPoseidonObject(obj, (GameBoolType) oper2,*(CPosEdMainDoc *) obj->GetDocument());

  return GameValue();
}

#define OPERATORS_DEFAULT(XX, Category) \
  XX(GameBool, "doesIntersect",function, doesObjectIntersect, EvalType_PosEdObject, EvalType_Net, "PosEdObject","Net", "Returns true, if the object and road overlaps.", "_object doesObjectIntersect _net;", "true - in case of overlap, false - otherwise.", "", "", Category) \
  XX(GameBool, "advanceWalkPoint",function, advanceWalkPoint, EvalType_Net, GameArray, "Net","Array [Walk point, distance].", "Moves walk point away from net key point by distance.Use walkpoints returned by getWalkPoints.", "_net advanceWalkPoint [_walkPoint, 100];", "true is succeeded, false if the end of the net was reached, in this case _walkPoint points to the end of the road.", "", "", Category) \
  XX(GameBool, "setPos",function, setNetPos, EvalType_Net, GameArray, "Net","2Dposition", "Sets net position.", "_net setPos [_xcoord, _zcoord];", "", "", "", Category) \
  XX(GameBool, "setDir",function, setNetOrientationXZ, EvalType_Net, GameScalar, "Net","Orientation", "Set net orientation in plane XZ. The orientation is in degrees. ", "_net setDir 10;", "", "", "", Category) \
  XX(GameNothing, "lock",function, lockNet, EvalType_Net, GameBool, "Net","Orientation", "Set net orientation in plane XZ. The orientation is in degrees. ", "_net setDir 10;", "", "", "", Category) 

static GameOperator BinaryFuncts[]=
{
  OPERATORS_DEFAULT(REGISTER_OPERATOR, Category)
};

#define FUNCTIONS_DEFAULT(XX, Category) \
  XX(GameArray, "getWalkPoints", getWalkPoints, EvalType_Net, "Net", "Returns array of walk points. Walk points can be then used for advancing on the net. For each direction of the net key point with at least one part is returned one walk point.", "_points = getWalkPoints _net;", "Array with walk points. Walk point is represented by an array. Meaning of array members: Index 0 -> x, 1 -> z coord, 2 -> orientation, 3 -> net part index, 4 -> net dir index.", "", "", Category) \
  XX(GameArray, "getPos", getNetPos, EvalType_Net, "Net", "Returns net position.", "_pos = getPos _net.", "Array with pos. Index 0 -> x,1 -> z coord", "", "", Category) \
  XX(GameScalar, "getDir", getNetOrientationXZ, EvalType_Net , "Net", "Returns net orientation in plane XZ. Orientation is an angle of rotation in degrees.", "_rot = getDir _net;", "", "", "", Category)

static GameFunction UnaryFuncts[]=
{
  FUNCTIONS_DEFAULT(REGISTER_FUNCTION, Category)
};


static GameData *CreateNet(ParamArchive *ar) {return new gdNet();}

TYPES_NET(DEFINE_TYPE, Category)

#if DOCUMENT_COMREF
static ComRefFunc DefaultComRefFunc[] =
{
  //NULARS_DEFAULT(COMREF_NULAR, Category)
  FUNCTIONS_DEFAULT(COMREF_FUNCTION, Category)
  OPERATORS_DEFAULT(COMREF_OPERATOR, Category)
};

static ComRefType DefaultComRefType[] =
{
  TYPES_NET(COMREF_TYPE, Category)
  /*  TYPES_DEFAULT_COMB(COMREF_TYPE, "Default")*/
};
#endif

void gdNet::RegisterToGameState(GameState *gState)
{
  GameState &state = *gState; // helper to make macro works
  TYPES_NET(REGISTER_TYPE, Category)

  gState->NewOperators(BinaryFuncts,lenof(BinaryFuncts));
  gState->NewFunctions(UnaryFuncts,lenof(UnaryFuncts));
  //gState->NewNularOps(NularyFuncts,lenof(NularyFuncts));
#if DOCUMENT_COMREF
  gState->AddComRefFunctions(DefaultComRefFunc,lenof(DefaultComRefFunc));
  gState->AddComRefTypes(DefaultComRefType,lenof(DefaultComRefType));
#endif
}