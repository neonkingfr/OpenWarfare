
#pragma once

#include <El/elementpch.hpp>
#include <El/Evaluator/express.hpp>
#include "MyGameState.hpp"

class CPoseidonDoc;

class ProcessScript
{
protected:
	MyGameState  _gState;
	GameVarSpace _gSpace;

public:
	ProcessScript();

	bool operator() (CString& name, CPoseidonDoc& doc, CScriptOutputDlg* outDlg = NULL);
};
