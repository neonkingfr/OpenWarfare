#include <stdafx.h>

#include "gdPosEdObject.hpp"

#define Category "Visitor::PosEdObject"


bool gdPosEdObject::IsEqualTo(const GameData *data) const 
{
  if (data->GetType() != EvalType_PosEdObject)
    return false;

  gdPosEdObject * obj = (gdPosEdObject *) data;
  return _posEdObj == obj->_posEdObj;
}

static GameValue getPosition( const GameState *state, GameValuePar oper1 )
{
  CPosEdObject * obj = static_cast<gdPosEdObject *>(oper1.GetData())->GetObject();

  if (obj)
  {
    REALPOS pos = obj->GetRealPos();
    GameArrayType array;
    
    array.Add(pos.x);
    array.Add(pos.z);
    
    return GameValue(array);
  }

  state->SetError(EvalForeignError,"empty object");
  return GameValue();
}

static GameValue getZeroPosition( const GameState *state, GameValuePar oper1 )
{
  CPosEdObject * obj = static_cast<gdPosEdObject *>(oper1.GetData())->GetObject();

  if (obj)
  {
    REALPOS pos = obj->GetRealZeroPos();
    GameArrayType array;

    array.Add(pos.x);
    array.Add(pos.z);

    return GameValue(array);
  }

  state->SetError(EvalForeignError,"empty object");
  return GameValue();
}

static GameValue setPosition( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPosEdObject * obj = static_cast<gdPosEdObject *>(oper1.GetData())->GetObject();
  GameArrayType& array = (GameArrayType) oper2;

  if (obj)
  {
    REALPOS pos;
    pos.x = (float) array[0];
    pos.z = (float) array[1];    

    Action_SetPosRealPoseidonObject(obj, pos);

    return GameValue();
  }

  state->SetError(EvalForeignError,"empty object");
  return GameValue();
}

static GameValue getOrientationXZ( const GameState *state, GameValuePar oper1 )
{
  CPosEdObject * obj = static_cast<gdPosEdObject *>(oper1.GetData())->GetObject();

  if (obj)
  {   
    return GameValue((float) obj->GetGradRotation());
  }

  state->SetError(EvalForeignError,"empty object");
  return GameValue((float) 0);
}

static GameValue setOrientationXZ( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPosEdObject * obj = static_cast<gdPosEdObject *>(oper1.GetData())->GetObject();
  float grad = (float) oper2;

  if (obj)
  {    
    grad -= obj->GetGradRotation();
    Action_RotatePoseidonObject(obj, grad);
    return GameValue();
  }

  state->SetError(EvalForeignError,"empty object");
  return GameValue();
}

static GameValue getRelHeight( const GameState *state, GameValuePar oper1 )
{
  CPosEdObject * obj = static_cast<gdPosEdObject *>(oper1.GetData())->GetObject();

  if (obj)
  {   
    return GameValue((float) obj->GetRelHeight());
  }

  state->SetError(EvalForeignError,"empty object");
  return GameValue((float) 0);
}

static GameValue setRelHeight( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPosEdObject * obj = static_cast<gdPosEdObject *>(oper1.GetData())->GetObject();
  float relHeight = (float) oper2;

  if (obj)
  {            
    Action_SetRelHeightPoseidonObject(obj, relHeight);
    return GameValue();
  }

  state->SetError(EvalForeignError,"empty object");
  return GameValue();
}

static GameValue getTemplateName( const GameState *state, GameValuePar oper1 )
{
  CPosEdObject * obj = static_cast<gdPosEdObject *>(oper1.GetData())->GetObject();

  if (obj)
  {
    CPosObjectTemplate* pTempl = obj->GetTemplate();
    if (pTempl)
    {    
      CString name = pTempl->GetName();           
      return GameValue(name);
    }
    else
      return  GameValue("");
  }

  state->SetError(EvalForeignError,"empty object");
  return GameValue((float) 0);
}

static GameValue deleteObject( const GameState *state, GameValuePar oper1 )
{
  CPosEdObject * obj = static_cast<gdPosEdObject *>(oper1.GetData())->GetObject();

  if (obj)
  {   
    Action_DeletePoseidonObject(obj, (CPosEdMainDoc *) obj->GetDocument(), NULL);
    static_cast<gdPosEdObject *>(oper1.GetData())->SetNil();

    return GameValue();
  }

  state->SetError(EvalForeignError,"empty object");
  return GameValue();
}

static GameValue doesObjectIntersect( const GameState *state, GameValuePar oper1)
{  
  CPosEdObject * obj = static_cast<gdPosEdObject *>(oper1.GetData())->GetObject();

  if (!obj)
  {
    state->SetError(EvalForeignError,"empty object");
    return GameValue(false);
  }

  CPoseidonMap& map = ((CPosEdMainDoc *) obj->GetDocument())->m_PoseidonMap;    
  for(int i=0; i < map.GetPoseidonObjectCount();i++)
  {
    CPosEdObject* obj2 = map.GetPoseidonObject(i);
    if (obj2 && obj2 != obj && obj2->m_nMasterWood < 0 && obj2->m_nMasterNet < 0 
      && obj->IsIntersection(*obj2))
    {			           
      return GameValue(true);
    };
  };

  return GameValue(false);
}

static GameValue lockObject( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{  
  CPosEdObject * obj = static_cast<gdPosEdObject *>(oper1.GetData())->GetObject();

  if (!obj)
  {
    state->SetError(EvalForeignError,"empty object");
    return GameValue(false);
  }

  Action_LockPoseidonObject(obj, (GameBoolType) oper2,*(CPosEdMainDoc *) obj->GetDocument());

  return GameValue();
}

#define OPERATORS_DEFAULT(XX, Category) \
  XX(GameNothing, "setPos",function, setPosition, EvalType_PosEdObject , GameArray, "PosEdObject","2Dpos", "Sets object position.", "_object setPos [_xCoord,_zCoord];", "", "", "", Category) \
  XX(GameNothing, "setDir",function, setOrientationXZ, EvalType_PosEdObject , GameScalar, "PosEdObject","Orientation", "Set object orientation in plane XZ. The orientation is in degrees. ", "_object setDir 10;", "", "", "", Category) \
  XX(GameNothing, "setRelHeight",function, setRelHeight, EvalType_PosEdObject , GameScalar, "PosEdObject","Relative height", "Set object height relative to the landscape height (the height over the landscape). ", "_object setRelHeight 10;", "", "", "", Category) \
  XX(GameNothing, "lock",function, lockObject, EvalType_PosEdObject , GameBool, "PosEdObject","bool", "Set object height relative to the landscape height (the height over the landscape). ", "_object setRelHeight 10;", "", "", "", Category) 

static GameOperator BinaryFuncts[]=
{
  OPERATORS_DEFAULT(REGISTER_OPERATOR, Category)
};

#define FUNCTIONS_DEFAULT(XX, Category) \
  XX(GameArray, "getPos", getPosition, EvalType_PosEdObject, "PosEdObject", "Returns object position.", "_pos = getPos _object", "Array with pos. Index 0 -> x,1 -> z coord", "", "", Category) \
  XX(GameArray, "getZeroPos", getZeroPosition, EvalType_PosEdObject, "PosEdObject", "Returns position in world coord of the point [0,0,0] from model.", "_pos = getZeroPos _object", "Array with pos. Index 0 -> x,1 -> z coord", "", "", Category) \
  XX(GameScalar, "getDir", getOrientationXZ, EvalType_PosEdObject , "PosEdObject", "Returns object orientation in plane XZ. Orientation is an angle of rotation in degrees.", "_rot = getDir _object;", "", "", "", Category) \
  XX(GameScalar, "getRelHeight", getRelHeight, EvalType_PosEdObject , "PosEdObject", "Returns object height relative to the landscape height (the height over the landscape)", "_relHeight = getRelHeight _object;", "", "", "", Category) \
  XX(GameString, "getTemplateName", getTemplateName, EvalType_PosEdObject , "PosEdObject", "Returns the name of object template.", "_tempName = getTemplateName _object;", "", "", "", Category) \
  XX(GameNothing, "delete", deleteObject, EvalType_PosEdObject , "PosEdObject", "Deletes object.", "delete _object;", "", "", "", Category) \
  XX(GameBool, "doesIntersect", doesObjectIntersect, EvalType_PosEdObject , "PosEdObject", "Returns true if the object overlaps some other object in map. It checks overlap only with PosEdObjects.", "_is = doesIntersect _object;", "true - in case of overlap, false - otherwise.", "", "", Category) 

static GameFunction UnaryFuncts[]=
{
  FUNCTIONS_DEFAULT(REGISTER_FUNCTION, Category)
};

static GameData *CreatePosEdObject(ParamArchive *ar) {return new gdPosEdObject;}

TYPES_EDOBJECT(DEFINE_TYPE, Category)

#if DOCUMENT_COMREF
static ComRefFunc DefaultComRefFunc[] =
{
  //NULARS_DEFAULT(COMREF_NULAR, Category)
    FUNCTIONS_DEFAULT(COMREF_FUNCTION, Category)
    OPERATORS_DEFAULT(COMREF_OPERATOR, Category)
};

static ComRefType DefaultComRefType[] =
{
  TYPES_EDOBJECT(COMREF_TYPE, Category)
    /*  TYPES_DEFAULT_COMB(COMREF_TYPE, "Default")*/
};
#endif


void gdPosEdObject::RegisterToGameState(GameState *gState)
{
  GameState &state = *gState; // helper to make macro works
  TYPES_EDOBJECT(REGISTER_TYPE, Category)

  gState->NewOperators(BinaryFuncts,lenof(BinaryFuncts));
  gState->NewFunctions(UnaryFuncts,lenof(UnaryFuncts));
  //gState->NewNularOps(NularyFuncts,lenof(NularyFuncts));
#if DOCUMENT_COMREF
  gState->AddComRefFunctions(DefaultComRefFunc,lenof(DefaultComRefFunc));
  gState->AddComRefTypes(DefaultComRefType,lenof(DefaultComRefType));
#endif
}



