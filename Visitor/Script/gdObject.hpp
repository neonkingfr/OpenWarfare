#pragma once

#include <El/elementpch.hpp>
#include <el\evaluator\express.hpp>
#include "gdType.hpp"
#include "..\poseidonedmaps\PoseidonEdMaps.h"

#define TYPES_OBJECT(XX, Category) \
  XX("AreaObject",EvalType_AreaObject,CreateAreaObject,"@AreaObject","AreaObject","This type represent object containig 2D area build from rects.",Category)

TYPES_OBJECT(DECLARE_TYPE, Category)

class gdAreaObject : public GameData
{
protected:
  CPosAreaObject * _area;

public:
  gdAreaObject() : GameData(), _area(NULL) {};
  gdAreaObject(CPosAreaObject * area) : GameData(), _area(area) {};
  gdAreaObject(const gdAreaObject& gArea) : GameData(), _area(gArea._area) {};

  CPosAreaObject * GetObject() const {return _area;};
  const GameType &GetType() const {return EvalType_AreaObject;}
  RString GetText() const {return RString("Area");}

  bool IsEqualTo(const GameData *data) const {return false;};
  const char *GetTypeName() const {return "AreObject";}
  GameData *Clone() const {return new gdAreaObject (*this);}

  //USE_FAST_ALLOCATOR;

  static void RegisterToGameState(GameState *gState);
};

