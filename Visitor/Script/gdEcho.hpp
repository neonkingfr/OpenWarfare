#pragma once

#include <El/elementpch.hpp>
#include <el\evaluator\express.hpp>

class gdMisc
{
public:
  static void RegisterToGameState(GameState *gState);
};