#pragma once

#include <El/elementpch.hpp>
#include <el\evaluator\express.hpp>
#include "gdType.hpp"
#include "..\poseidonedmaps\PoseidonEdMaps.h"

#define TYPES_NET(XX, Category) \
  XX("Net",EvalType_Net,CreateNet,"@Net","Net","Road object.",Category)

TYPES_NET(DECLARE_TYPE, Category)

class gdNet : public GameData
{
protected:
	CPosNetObjectBase * _net;

public:
	gdNet() 
	: GameData()
	, _net(NULL) 
	{
	}

	gdNet(CPosNetObjectBase* area) 
	: GameData()
	, _net(area) 
	{
	}

	gdNet(const gdNet& gNet)
	: GameData()
	, _net(gNet._net) 
	{
	}

	CPosNetObjectBase* GetObject() const 
	{
		return _net;
	}

	const GameType &GetType() const 
	{
		return EvalType_Net;
	}

	RString GetText() const 
	{
		return RString("Net");
	}

	bool IsEqualTo(const GameData* data) const;

	const char* GetTypeName() const 
	{
		return "Net";
	}

	GameData* Clone() const 
	{
		return new gdNet (*this);
	}

  //USE_FAST_ALLOCATOR;

	static void RegisterToGameState(GameState* gState);
};