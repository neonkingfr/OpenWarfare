#include <stdafx.h>
#include "gdObject.hpp"
#include "gdDocument.hpp"
#include "MyGameState.hpp"

#define Category "Visitor::AreaObject"

static GameValue isPointAtArea(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPosAreaObject * area = static_cast<gdAreaObject *>(oper2.GetData())->GetObject();
  GameArrayType& array = (GameArrayType) oper1;

  if (area)
  {
    REALPOS pos;
    pos.x = array[0];
    pos.z = array[1];

    return GameValue(area->IsPointAtAreaReal(pos));
  }

  state->SetError(EvalForeignError,"empty area");
  return GameValue(false);
}

static GameValue addRectToArea(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPosAreaObject * area = static_cast<gdAreaObject *>(oper1.GetData())->GetObject();
  GameArrayType& array = (GameArrayType) oper2;

  if (area)
  {
    if (array.Size() < 4)
    {
      state->SetError(EvalForeignError,"not rect on input");
      return GameValue();
    }
    FltRect rec;
    rec[0] = array[0];
    rec[1] = array[1];
    rec[2] = array[2];
    rec[3] = array[3];

    // is selected area from document? if yes change through undo/redo
    CPosEdMainDoc * doc = (CPosEdMainDoc *) area->GetDocument();
    if (&doc->GetSelectedArea() == area)
    {
      Action_AddPoseidonCurrArea(doc->m_PoseidonMap.FromRealToViewRect(rec),FALSE,FALSE, doc);
    }
    else
      area->AddRectReal(rec);

    return GameValue();
  }

  state->SetError(EvalForeignError,"empty area");
  return GameValue();
}

static GameValue removeRectFromArea(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CPosAreaObject * area = static_cast<gdAreaObject *>(oper1.GetData())->GetObject();
  GameArrayType& array = (GameArrayType) oper2;

  if (area)
  {
    if (array.Size() < 4)
    {
      state->SetError(EvalForeignError,"not rect on input");
      return GameValue();
    }
    FltRect rec;
    rec[0] = array[0];
    rec[1] = array[1];
    rec[2] = array[2];
    rec[3] = array[3];

    // is selected area from document? if yes change through undo/redo
    CPosEdMainDoc * doc = (CPosEdMainDoc *) area->GetDocument();
    if (&doc->GetSelectedArea() == area)
    {      
      Action_AddPoseidonCurrArea(doc->m_PoseidonMap.FromRealToViewRect(rec),FALSE,TRUE, doc);
    }
    else
      area->RemoveRectReal(rec);

    return GameValue();
  }

  state->SetError(EvalForeignError,"empty area");
  return GameValue();
}

//  not used because there si no way how to get name areas from script
static GameValue lockArea( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{  
  CPosAreaObject * obj = static_cast<gdAreaObject *>(oper1.GetData())->GetObject();

  if (!obj)
  {
    state->SetError(EvalForeignError,"empty object");
    return GameValue(false);
  }

  Action_LockPoseidonObject(obj, (GameBoolType) oper2,*(CPosEdMainDoc *) obj->GetDocument());

  return GameValue();
}


static GameValue lockTexture( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{  
  CPosAreaObject * obj = static_cast<gdAreaObject *>(oper1.GetData())->GetObject();

  if (!obj)
  {
    state->SetError(EvalForeignError,"empty object");
    return GameValue();
  }

  Action_TextureLock(*obj->GetLogObjectPosition(), (GameBoolType) oper2,*(CPosEdMainDoc *) obj->GetDocument());  
  return GameValue();
}

static GameValue lockVerticesInArea( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{  
  CPosAreaObject * obj = static_cast<gdAreaObject *>(oper1.GetData())->GetObject();

  if (!obj)
  {
    state->SetError(EvalForeignError,"empty object");
    return GameValue();
  }

  Action_LockHeight(*obj->GetLogObjectPosition(), (GameBoolType) oper2,*(CPosEdMainDoc *) obj->GetDocument());  
  return GameValue();
}

static GameValue lockVertices( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{  
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();
  GameArrayType& array = (GameArrayType) oper2; // array with vertices's. create area selecting them

  if (array.Size() >= 2 && GameArray != array[0].GetType())
  {
    state->SetError(EvalForeignError,"wrong input array");
    return GameValue();
  }

  GameArrayType& arrayWithVertices = (GameArrayType) array[0];

  CPositionArea area;
  int n = arrayWithVertices.Size();
  for(int i = 0; i < n; i++)
  {
    REALPOS realPos;
    realPos.x = (float) ((GameArrayType&)arrayWithVertices[i])[0];
    realPos.z = (float) ((GameArrayType&)arrayWithVertices[i])[1];    

    CPoint viewPos = doc->m_PoseidonMap.FromRealToViewPos(realPos);
    area.AddRect(CRect(viewPos.x - VIEW_LOG_UNIT_SIZE / 2, viewPos.y - VIEW_LOG_UNIT_SIZE / 2 ,viewPos.x + VIEW_LOG_UNIT_SIZE / 2, viewPos.y + VIEW_LOG_UNIT_SIZE / 2));
  }  

  Action_LockHeight(area, (GameBoolType) array[1],*doc);  
  return GameValue();
}


#define OPERATORS_DEFAULT(XX, Category) \
  XX(GameBool, "isPointAtArea",function, isPointAtArea, GameArray, EvalType_AreaObject, "2d Point","Area", "Returns true if point lies inside the area.", "[_xCoord, _zCoord] isPointAtArea _area;", "true - if point is in area, false - otherwise", "", "", Category) \
  XX(GameNothing, "+=",function, addRectToArea, EvalType_AreaObject,GameArray, "AreaObject","Rectangle", "Appends rectangle to area.", "_area += [_left, _top, _right, _botom];", "", "", "", Category) \
  XX(GameNothing, "-=",function, removeRectFromArea, EvalType_AreaObject,GameArray, "AreaObject","Rectangle", "Removes rectangle from area.", "_area -= [_left, _top, _right, _botom];", "", "", "", Category) \
  XX(GameNothing, "lockTextures",function, lockTexture, EvalType_AreaObject,GameBool, "AreaObject","lock", "Locks all textures in area, if lock is true, otherwise it unlocks them. ", "_area lockTextures true;", "", "", "", Category) \
  XX(GameNothing, "lockVertices",function, lockVerticesInArea, EvalType_AreaObject,GameBool, "AreaObject","lock", "Locks all vertices in area, if lock is true, otherwise it unlocks them.", "_area lockVertices true;", "", "", "", Category) \
  XX(GameNothing, "lockVertices",function, lockVertices, EvalType_Document,GameArray, "Document","[Vertices,lock]", "Locks all vertices, if lock is true, otherwise it unlocks them. Vertices must be in format defined in getVerticesUnder", "_vertices = getVerticesUnder _obj; _document lockVertices [_vertices, true];", "", "", "", Category) 
  //XX(GameNothing, "lock",function, lockArea, EvalType_AreaObject,GameBool, "AreaObject","Rectangle", "Removes rectangle from area.", "_area -= [_left, _top, _right, _botom];", "", "", "", Category) 

static GameOperator BinaryFuncts[]=
{
  OPERATORS_DEFAULT(REGISTER_OPERATOR, Category)
};

static GameValue getBoundingRect(const GameState *state, GameValuePar oper1)
{
  CPosAreaObject * area = static_cast<gdAreaObject *>(oper1.GetData())->GetObject();
 
  if (area)
  {    
    FltRect rect;
    area->GetObjectHullReal(rect);

    GameArrayType outRect; 
    outRect.Resize(4);
    outRect[0] = rect[0];
    outRect[1] = rect[1];
    outRect[2] = rect[2];
    outRect[3] = rect[3];

    return GameValue(outRect);
  }

  state->SetError(EvalForeignError,"empty area");
  return GameValue(GameArrayType());
}

// functions remove members that are several times in array
/*static GameValue removeReplication(const GameState *state, GameValuePar oper1)
{
  GameArrayType& array = oper1->GetData()->GetArray();

  for(int i = 0; i < array.Size(); i++)
  {
    GameData * gd = array[i].GetData();
    for (int j = 0; j < i; j++)
    {
      if (array[j].GetData()->IsEqualTo(gd))
      {
        array.Delete(j);
        i--;
        break;
      }
    }
  }

  return GameValue();
}*/

static GameValue resetArea(const GameState *state, GameValuePar oper1)
{
  CPosAreaObject * area = static_cast<gdAreaObject *>(oper1.GetData())->GetObject();
  if (area)  
  {
    // is selected area from document? if yes change through undo/redo
    CPosEdMainDoc * doc = (CPosEdMainDoc *) area->GetDocument();
    if (&doc->GetSelectedArea() == area)
    {
      Action_DelPoseidonCurrArea(doc, NULL, FALSE);
    }
    else
      area->ResetArea();    
    return GameValue();
  }

  state->SetError(EvalForeignError,"empty area");
  return GameValue();
}

static GameValue newArea(const GameState *state, GameValuePar oper1)
{
  CPoseidonDoc * doc = static_cast<gdDocument *>(oper1.GetData())->GetObject();

  if (doc)  
    return GameValue(new gdAreaObject(new CPosAreaObject(doc->m_PoseidonMap)));  

  state->SetError(EvalForeignError,"empty doc");
  return GameValue(new gdAreaObject());
}


#define FUNCTIONS_DEFAULT(XX, Category) \
  XX(GameArray, "getBoundingRect", getBoundingRect, EvalType_AreaObject, "area", "Returns bounding box of the area."," getBoundingRect _area;", "GameArray with bounding rect (left, top, right, bottom) ", "", "", Category) \
  XX(EvalType_AreaObject, "newArea", newArea, EvalType_Document, "Document", "Alloc new area object. Area is not connected with document, but can be use in function like: isPointAtArea, selectInDoc...","_area = newArea _document;","reference to created area object.", "", "", Category) \
  XX(GameNothing, "reset", resetArea, EvalType_AreaObject, "Area", "Reset content of area.","reset _area;","", "", "", Category) 
  //XX(GameNothing, "removeReplication", removeReplication, GameArray(), "area", "Retturns bounding box of the area."," getBoundingRect _area;", "GameArray with bounding rect (left, top, right, bottom) ", "1.00", "", Category) 

static GameFunction UnaryFuncts[]=
{
  FUNCTIONS_DEFAULT(REGISTER_FUNCTION, Category)
};


static GameData *CreateAreaObject(ParamArchive *ar) {return new gdAreaObject;}

TYPES_OBJECT(DEFINE_TYPE, Category)

#if DOCUMENT_COMREF
static ComRefFunc DefaultComRefFunc[] =
{
  //NULARS_DEFAULT(COMREF_NULAR, Category)
  FUNCTIONS_DEFAULT(COMREF_FUNCTION, Category)
  OPERATORS_DEFAULT(COMREF_OPERATOR, Category)
};

static ComRefType DefaultComRefType[] =
{
  TYPES_OBJECT(COMREF_TYPE, Category)
    /*  TYPES_DEFAULT_COMB(COMREF_TYPE, "Default")*/
};
#endif

void gdAreaObject::RegisterToGameState(GameState *gState)
{
  GameState &state = *gState; // helper to make macro works
  TYPES_OBJECT(REGISTER_TYPE, Category)

  gState->NewOperators(BinaryFuncts,lenof(BinaryFuncts));
  gState->NewFunctions(UnaryFuncts,lenof(UnaryFuncts));
  //gState->NewNularOps(NularyFuncts,lenof(NularyFuncts));
#if DOCUMENT_COMREF
  gState->AddComRefFunctions(DefaultComRefFunc,lenof(DefaultComRefFunc));
  gState->AddComRefTypes(DefaultComRefType,lenof(DefaultComRefType));
#endif
}

