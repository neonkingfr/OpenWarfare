#pragma once
// #include "afxwin.h"

class CTextureLayerDlg : public CDialog
{
	DECLARE_DYNAMIC(CTextureLayerDlg)
public:
	CString m_Name;
	CString m_NameInit;
	int     m_nTexSizeInSqLog;

	CPosEdMainDoc* m_pDoc;

public:
	CTextureLayerDlg(CPosEdMainDoc* pDoc, CWnd* pParentWnd = NULL) : CDialog(IDD, pParentWnd), m_nTexSizeInSqLog(0), m_pDoc(pDoc) {};
  
	// Dialog Data
	enum { IDD = IDD_EDIT_TEXTURE_LAYER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
};
