/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "math.h"
#include "AutoNetGener.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// je to stejn� poloha (v toleranci ?)

bool IsSameNetPosition(const REALNETPOS& p1,const REALNETPOS& p2,const AutoNetGenParams& Params,BOOL bFinal)
{
	// rozd�l sm�ru
	int nDGra = abs(p1.nGra-p2.nGra);
	if (nDGra > Params.nSameGraTol)
		return FALSE;
	// rozd�l v��ky
	double  nDist = fabs(p1.nHgt - p2.nHgt);
	if (nDist > Params.nSamePosHgt)
		return FALSE;
	// vzd�lenost bod�
	nDist = GetDistancePoints(p1.nPos,p2.nPos);
	/*if (!bFinal)
	{
		if (nDist > 0.01)
			return FALSE;
	} else
	{*/
		if (nDist > Params.nSamePosTol)
			return FALSE;
	//};
	return TRUE;
};

BOOL DoAutoNetGenerObjects2(CObArray&   FinalPath,CPosNetTemplate* pNetTemplate,
                            AutoNetGenParams& Params,const CPoseidonMap* pMap);

BOOL DoAutoNetGenerateFunction(CNetPartsList* pListFrom,CNetPartsList* pListTo,CPosNetTemplate* pNetTemplate,AutoNetGenParams& Params,CPosEdMainDoc* pDocument)
{
	// c�lov� objekty
	CObArray   FinalPath;	// cesta z objekt� CNetPartSIMP
	TRY
	{
		// po�adovan� polohy
		Params.nBgnPosition = pListFrom->GetNetPartsEndRealPos();
		Params.nEndPosition = pListTo->GetNetPartsEndRealPos();
			 // obr�cen� c�lov�ho sm�ru (kon��m v protism�ru koncov�ho �seku)
		Params.nEndPosition.nGra = GetNormalizedGrad(Params.nEndPosition.nGra + 180);
		// min. d�lka trati (|nBgnPosition,nEndPosition|)
		Params.nMinLength	= GetDistancePoints(Params.nBgnPosition.nPos,Params.nEndPosition.nPos);
    if (pListFrom->m_NetParts.GetSize() > 0)
		  Params.nBgnSlope	= pListFrom->GetEndNetSlope();
    else
      Params.nBgnSlope = -100.0; 

    if (pListFrom->m_NetParts.GetSize() > 0)
		  Params.nEndSlope	= pListTo->GetEndNetSlope();
    else
      Params.nEndSlope = -100.0; 

    Params.bFitEndAngle = TRUE;

		// generuji
		if (!DoAutoNetGenerObjects2(FinalPath,pNetTemplate,Params,&(pDocument->m_PoseidonMap)))
		{
			DestroyArrayObjects(&FinalPath);
			return FALSE;
		}
		// na�el jsem cestu -> udelam z toho usek site
		if (FinalPath.GetSize()>0)
		{
			CPosNetObjectNor* pNetObj = new CPosNetObjectNor(pDocument->m_PoseidonMap);
			// list podle s�t�
			pNetObj->m_PartList.CreateFromNetTemplate(pNetTemplate);
			// po��te�n� poloha
			pNetObj->m_nBaseRealPos = Params.nBgnPosition;
			// napln�n� seznamu objekt�
			for(int i=0; i<FinalPath.GetSize(); i++)
			{
				CNetPartSIMP* pNetPart = (CNetPartSIMP*)(FinalPath[i]);
				if (pNetPart != NULL)
				{
					pNetObj->m_PartList.m_NetParts.Add(pNetPart);
				}
			};
			// vyjmu objekty, aby je nesmazal
			FinalPath.RemoveAll();

			// generuji akci
			Action_CreatePoseidonNorNet(pNetObj,pNetTemplate,pDocument,NULL);
		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		DestroyArrayObjects(&FinalPath);
		return FALSE;
	}
	END_CATCH_ALL
	// ru��m objekty, kdyz jsem je nepouzil
	DestroyArrayObjects(&FinalPath);
	return TRUE;
};

BOOL DoAutoNetGenerateFunction2(CPosNetObjectKey* pMainObject, CNetPartsList* pListFrom,REALNETPOS	rPosTo,CPosNetTemplate* pNetTemplate,AutoNetGenParams& Params,CPosEdMainDoc* pDocument)
{
	// c�lov� objekty
	CObArray   FinalPath;	// cesta z objekt� CNetPartSIMP
	TRY
	{
		// po�adovan� polohy
		Params.nBgnPosition = pListFrom->GetNetPartsEndRealPos();
		Params.nEndPosition = rPosTo;
			 // obr�cen� c�lov�ho sm�ru (kon��m v protism�ru koncov�ho �seku)
		Params.nEndPosition.nGra = GetNormalizedGrad(Params.nEndPosition.nGra/*+ 180*/);
		// min. d�lka trati (|nBgnPosition,nEndPosition|)
		Params.nMinLength	= GetDistancePoints(Params.nBgnPosition.nPos,Params.nEndPosition.nPos);
    if (pListFrom->m_NetParts.GetSize() > 0)
		  Params.nBgnSlope	= pListFrom->GetEndNetSlope();
    else
      Params.nBgnSlope	= -100; // -100 means undef

		Params.nEndSlope	= 0.;
		// generuji
		if (!DoAutoNetGenerObjects2(FinalPath,pNetTemplate,Params,&(pDocument->m_PoseidonMap)))
		{
			DestroyArrayObjects(&FinalPath);
			return FALSE;
		}
		// na�el jsem cestu -> udelam z toho usek site
		if (FinalPath.GetSize()>0)
		{
			// generuji akci
			{
				CPosNetObjectKey CopyObj(pDocument->m_PoseidonMap);
				CopyObj.CopyFrom(pMainObject);
				// index seznamu
				int nIndx = -1;
				for(int i=0;;i++)
				{
					if (pMainObject->m_PartLists[i] == pListFrom)
					{
						nIndx = i;
						break;
					}
				}

				if ((nIndx >= 0)&&(nIndx<CopyObj.m_PartLists.GetSize()))
				{
					CNetPartsList* pList = (CNetPartsList*)(CopyObj.m_PartLists[nIndx]);
					if (pList)
					{
						// napln�n� seznamu objekt�
						for(int i=0; i<FinalPath.GetSize(); i++)
						{
							CNetPartSIMP* pNetPart = (CNetPartSIMP*)(FinalPath[i]);
							if (pNetPart != NULL)
							{
								pList->m_NetParts.Add(pNetPart);
							}
						};
						// vyjmu objekty, aby je nesmazal
						FinalPath.RemoveAll();

						Action_EditPoseidonNetKey(&CopyObj,pDocument,NULL,FALSE);
					}
				}
			}

		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		DestroyArrayObjects(&FinalPath);
		return FALSE;
	}
	END_CATCH_ALL
	// ru��m objekty, kdyz jsem je nepouzil
	DestroyArrayObjects(&FinalPath);
	return TRUE;
};
/////////////////////////////////////////////////////////////////////////////////////////
// A*


// Precision in determination, that two states are diffrent.
#define POS_PREC 2.0f
#define ANGLE_PREC 1.0f
#define HEIGHT_PREC 2.0f

unsigned int g_SavedPos = 0;

int REALNETPOSKEY::Cmp(const REALNETPOSKEY &pos) const
{  

  if (fabs(nPos.x - pos.nPos.x) > POS_PREC)
  {  
    if (nPos.x < pos.nPos.x)
      return -1;
    else
      if (nPos.x > pos.nPos.x)
        return 1;
  }

  if (fabs(nPos.z - pos.nPos.z) > POS_PREC)
  {
    if (nPos.z < pos.nPos.z)
      return -1;
    else
      if (nPos.z > pos.nPos.z)
        return 1;
  }

  if (fabs(nGra - pos.nGra) > ANGLE_PREC)
  {
    if (nGra < pos.nGra)
      return -1;
    else
      if (nGra > pos.nGra)
        return 1;
  }

  if (fabs(nHgt - pos.nHgt) > HEIGHT_PREC)
  {  
    if (nHgt < pos.nHgt)
      return -1;
    else
      if (nHgt > pos.nHgt)
        return 1;
  }

  if (nPos.x != pos.nPos.x || 
    nPos.z != pos.nPos.z || 
    nGra != pos.nGra ||
    nHgt != pos.nHgt)
    g_SavedPos ++; 

  return 0;
}

NetField::NetField(const NetField& field)
{    
  m_pNetPart = (field.m_pNetPart) ? (CNetPartSIMP *) field.m_pNetPart->CreateCopyObject() : NULL;
  m_nEndPos = field.m_nEndPos;
  m_nSlope = field.m_nSlope;
  m_pParams = field.m_pParams;
}

const NetField& NetField::operator=(const NetField& field) 
{
  delete m_pNetPart;
  m_pNetPart = (field.m_pNetPart) ? (CNetPartSIMP *) field.m_pNetPart->CreateCopyObject() : NULL;
  m_nEndPos = field.m_nEndPos;
  m_nSlope = field.m_nSlope;
  m_pParams = field.m_pParams;

  return *this;
};


struct NetContext
{
  CPosNetTemplate * _netTemplate;
  AutoNetGenParams * _params;
  const CPoseidonMap* _map;
};

class NetIterator
{
protected:
	NetContext* _context;
	int _index;
	NetField _createdField;
	const NetField& _parent;

	void DoGenerateSTRAParts(REALNETPOS BgnPos, CNetPartSTRA* pStra);
	void DoGenerateBENDPart(REALNETPOS BgnPos, CNetPartBEND* pBend);
public:

	// added to let Visitor compile after changes in AStar.hpp
	NetIterator()
	: _index(0)
	, _parent(NetField())
	, _context(0)
	{	
	}

	NetIterator(const NetField & parent, void *context) 
	: _index(0)
	, _parent(parent) 
	{ 
		_context = (NetContext*) context; 
		operator ++ ();
	}

	operator bool () const 
	{
		return _index <= (_context->_netTemplate->m_NetSTRA.GetSize() 
						  + 2 * _context->_netTemplate->m_NetBEND.GetSize());
	}

	void operator ++ ();

	operator NetField () 
	{
		return _createdField;
	}

	// added to let Visitor compile after changes in AStar.hpp
	NetIterator &operator= (NetIterator &it)
	{ 
		return *this; 
	}
};

void NetIterator::DoGenerateSTRAParts(REALNETPOS BgnPos,CNetPartSTRA* pStra)
{
  // spo��t�m polohu
  REALNETPOS EndPos= pStra->CalcABasePosition(BgnPos,_context->_map);
  
  // v��ka tak, aby byla standardn�
  //pStra->m_nRelaHeight = _context->_netTemplate->m_nStdHeight; 
  //pStra->m_nBaseRealPos = EndPos;

  _createdField.m_nEndPos = EndPos;
  _createdField.m_nSlope = pStra->GetCurrentSlopeRad();
  _createdField.m_pParams = _context->_params;
  _createdField.m_pNetPart = pStra;
};

void NetIterator::DoGenerateBENDPart(REALNETPOS BgnPos,CNetPartBEND* pBend)
{
  // spo��t�m polohu
  REALNETPOS EndPos= pBend->CalcABasePosition(BgnPos,_context->_map);

  // ur��m v��ku, relativn� tak, aby zat��ka navazovala 
  //pBend->m_nRelaHeight = _context->_netTemplate->m_nStdHeight; // standardn� v��ka
  //pBend->m_nBaseRealPos = EndPos;

  _createdField.m_nEndPos = EndPos;
  _createdField.m_nSlope = pBend->GetCurrentSlopeRad();
  _createdField.m_pParams = _context->_params;
  _createdField.m_pNetPart = pBend;  
};

void NetIterator::operator++()
{
  //
  CNetPartsList list; list.CreateFromNetTemplate(_context->_netTemplate);

  // po��te�n� polohy pro useky od konce a od za��tku
  REALNETPOS	BgnPos;  
  BgnPos = _parent.m_nEndPos; 

  if (_createdField.m_pNetPart)
  {
    delete _createdField.m_pNetPart;
    _createdField.m_pNetPart = NULL;
  }

  // absolutn� v��ka 
  float nAbsHeight = _context->_map->GetLandscapeHeight(BgnPos.nPos);
  float nRelHeight = BgnPos.nHgt - nAbsHeight;	// relativn� v��ka po��tku �seku
  
/*  if (!pNetTemplate->m_bAutoHeight)
  {
    if ((nRelHeight1 < (float)pNetTemplate->m_nMinHeight)||
      (nRelHeight1 > (float)pNetTemplate->m_nMaxHeight))
    {	// od tohoto d�lu nemohu prov�st expanzi,
      // proto�e konec p�edchoz�ho je mimo povolen� parametry !!
      return;
    }
    if ((nRelHeight2 < (float)pNetTemplate->m_nMinHeight)||
      (nRelHeight2 > (float)pNetTemplate->m_nMaxHeight))
    {	// od tohoto d�lu nemohu prov�st expanzi,
      // proto�e konec p�edchoz�ho je mimo povolen� parametry !!
      return;
    }
  }	*/  
  // generuji rovinky
  if (_index < _context->_netTemplate->m_NetSTRA.GetSize())  
  {
    CNetSTRA* pSTRA = (CNetSTRA*)(_context->_netTemplate->m_NetSTRA[_index]);
    if (pSTRA)
    {
      ASSERT_KINDOF(CNetSTRA,pSTRA);
      // vodorovn�
      CNetPartSTRA* pNew = new CNetPartSTRA();
      pNew->CreateFromTemplateSTRA(pSTRA,/*&list*/ NULL);
      DoGenerateSTRAParts(BgnPos,  pNew);
    }
    _index ++;
    return;
  }

  // Rigth bends
  int i = _index - _context->_netTemplate->m_NetSTRA.GetSize();
  
  if (i < _context->_netTemplate->m_NetBEND.GetSize())
  {
    CNetBEND* pBEND = (CNetBEND*)(_context->_netTemplate->m_NetBEND[i]);
    if (pBEND)
    {
      ASSERT_KINDOF(CNetBEND,pBEND);
      // lev�
      CNetPartBEND* pNew = new CNetPartBEND();
      pNew->CreateFromTemplateBEND(pBEND,TRUE,/*&list*/ NULL);
      DoGenerateBENDPart(BgnPos, pNew);      
    }
    _index ++;
    return;
  }

  // Left bends
  i -= _context->_netTemplate->m_NetBEND.GetSize();
  if (i < _context->_netTemplate->m_NetBEND.GetSize())
  {
    CNetBEND* pBEND = (CNetBEND*)(_context->_netTemplate->m_NetBEND[i]);
    if (pBEND)
    {
      ASSERT_KINDOF(CNetBEND,pBEND);
      // lev�
      CNetPartBEND* pNew = new CNetPartBEND();
      pNew->CreateFromTemplateBEND(pBEND,FALSE,/*&list*/ NULL);
      DoGenerateBENDPart(BgnPos, pNew);      
    }
    _index ++;
    return;
  }
   _index ++;
};

#define UNACCESSIBLE 1000000.0f
//#define PI 3.1415f    
#define GRADTORAD(x) ((x)/180.0f * PI)

#define COMP_CHANGE_PUNISHMENT 10.0f;

class NetCostFunction
{
protected:
  const AutoNetGenParams& _params;

public:
  NetCostFunction(const AutoNetGenParams& params) : _params(params) {};

  float operator () (const NetField &field1, const NetField &field2) const 
  {
    float fDistOrg = (float) GetDistancePoints(field2.m_pNetPart->m_nConnRealPos.nPos, field2.m_pNetPart->m_nBaseRealPos.nPos);    
    float fDist = fDistOrg * 0.9f + 2.5f;

    if (_params.bAvoidHills)
    {    
      float slope = (float) field2.m_nSlope;    
      float fDistToEnd = (float) GetDistancePoints(field2.m_pNetPart->m_nConnRealPos.nPos, _params.nEndPosition.nPos);
      FMntVector2 vec(fDistToEnd , (_params.nEndPosition.nHgt - field2.m_pNetPart->m_nConnRealPos.nHgt));
      vec.Normalize();

      FMntVector2 dir(fDistOrg, field2.m_pNetPart->m_nBaseRealPos.nHgt - field2.m_pNetPart->m_nConnRealPos.nHgt);
      fDist += (float) _params.fAvoidHillsCoef * (dir.Size() - vec * dir);
    }    
   
    // check see level.
    if (field2.m_pNetPart->m_nConnRealPos.nHgt < SEA_LAND_HEIGHT || 
      field2.m_pNetPart->m_nBaseRealPos.nHgt < SEA_LAND_HEIGHT)
      return UNACCESSIBLE;

    if (_params.bPunishCompChange &&     
      field1.m_pNetPart != NULL && (field2.m_pNetPart->m_nComponentName != field1.m_pNetPart->m_nComponentName || 
      (field2.m_pNetPart->IsKindOf(RUNTIME_CLASS(CNetPartBEND)) && field1.m_pNetPart->IsKindOf(RUNTIME_CLASS(CNetPartBEND)) &&
      ((CNetPartBEND *)field2.m_pNetPart)->m_bLeftBend != ((CNetPartBEND *)field1.m_pNetPart)->m_bLeftBend)))   
      fDist += (float) _params.fPunishCompChangeCoef * COMP_CHANGE_PUNISHMENT;

    if (_params.bMaxSlope && field2.m_nSlope != -100 && field2.m_nSlope > GRADTORAD(_params.nMaxSlope))
      return UNACCESSIBLE;

    if (_params.bMaxSlopeChange && field2.m_nSlope != -100 && field1.m_nSlope != -100 && fabs(field2.m_nSlope - field1.m_nSlope) > GRADTORAD(_params.nMaxSlopeChange))
      return UNACCESSIBLE;

    return fDist;
  };
};


#define MIN_RAD 25.0f // minimal bend radius.

class NetHeuristicFunction
{
protected:
  const AutoNetGenParams& _params;

  float GetArcDistance(float fDist, const FMntVector2& vec, const FMntVector2& dir, float minRad) const
  {
    FMntVector2 axe(-vec[1], vec[0]);      
    //axe.Normalize();

    float fangl = dir * axe;
    fangl = fabs(fangl);

    if (fangl < 0.01)
    {
      if (vec * dir > 0)
        return fDist;
      else
        return (PI * minRad + fDist + 2 * minRad);
    }
    else
    {    
      float radius = fDist / 2.0f / fangl;  

      if (radius < MIN_RAD)
        return 2 * UNACCESSIBLE;

      float angleRad = asin(fangl);

      if (dir * vec > 0)
        return 2.0f * angleRad * radius;
      else
        return 2.0f * (PI - angleRad) * radius;
    }    
  }

public:
  NetHeuristicFunction(const AutoNetGenParams& params): _params(params) {};
  float operator () (const NetField &field1, const NetField &field2) const 
  {    
    float fDist = (float) GetDistancePoints(field1.m_nEndPos.nPos,field2.m_nEndPos.nPos);
    float hills = 0;

    if (_params.bAvoidHills)
    {
      FMntVector2 vec(fDist, 
        field2.m_nEndPos.nHgt - field1.m_nEndPos.nHgt);
      float fNewDist = vec.Size();
      float invNewDist = 1.0f/ ((fNewDist == 0) ? 1 : fNewDist);
      vec *= invNewDist;

      FMntVector2 dir(1, tan((float) field1.m_nSlope));
      dir.Normalize();

      hills = (float) (_params.fBaseHeuristicCoef * _params.fAvoidHillsCoef *(GetArcDistance(fNewDist,vec,dir,0) - fNewDist));
    }
    if (!_params.bFitEndAngle)
    {                
      FMntVector2 vec(field2.m_nEndPos.nPos.x - field1.m_nEndPos.nPos.x, 
        field2.m_nEndPos.nPos.z - field1.m_nEndPos.nPos.z);
      vec.Normalize();

      FMntVector2 dir; dir.FromAngle(450 - field1.m_nEndPos.nGra);

      return hills + ((float) _params.fBaseHeuristicCoef) * GetArcDistance(fDist, vec, dir, MIN_RAD);      
    }
    else
    {
     // float fFirst = 0;
      
      FMntVector2 vec(field2.m_nEndPos.nPos.x - field1.m_nEndPos.nPos.x, 
        field2.m_nEndPos.nPos.z - field1.m_nEndPos.nPos.z);
      vec.Normalize();

      FMntVector2 dir1; dir1.FromAngle(450 - field1.m_nEndPos.nGra);
      FMntVector2 dir2; dir2.FromAngle(450 - field2.m_nEndPos.nGra);
      FMntVector2 axe(-vec[1], vec[0]);

      BOOL bSimilar = ((axe * dir1) * (axe * dir2) > 0);
      if (bSimilar)
      {
        fDist *= 0.5f; 
      }

      float fFirst = GetArcDistance(fDist, vec, dir1, MIN_RAD); 
      float fSecond = GetArcDistance(fDist, -vec, -dir2, MIN_RAD);

      return hills +  ((float)_params.fBaseHeuristicCoef) * (fFirst + fSecond) * ((bSimilar) ? 1 : 0.5f);
    }
  }
};

class DummyNetReadyFunction
{
public:
  bool operator () (const NetField &field1) {return true;};
};

class NETParams
{
public:
  static float LimitCost() {return UNACCESSIBLE;}
  static float BestNodeCoef() {return 0.0f;}
};



typedef AStarEx<NetField, NetCostFunction, NetHeuristicFunction, DummyNetReadyFunction, NetIterator, NETParams> NetAStar;

BOOL DoAutoNetGenerObjects2(CObArray& FinalPath,CPosNetTemplate* pNetTemplate,
                           AutoNetGenParams& Params,const CPoseidonMap* pMap)
{	

  AfxGetApp()->BeginWaitCursor();

  NetField start,end;

  start.m_nEndPos = Params.nBgnPosition;
  Params.nBgnPosition.nHgt = pMap->GetLandscapeHeight(Params.nBgnPosition.nPos);
  start.m_nEndPos.nHgt = Params.nBgnPosition.nHgt;
  start.m_pNetPart =  NULL;
  start.m_nSlope = Params.nBgnSlope;
  start.m_pParams = &Params;

  end.m_nEndPos = Params.nEndPosition;
  Params.nEndPosition.nHgt = pMap->GetLandscapeHeight(Params.nEndPosition.nPos);
  end.m_nEndPos.nHgt = Params.nEndPosition.nHgt;
  end.m_pNetPart =  NULL;
  end.m_nSlope = 0;
  end.m_pParams = &Params;

  NetCostFunction costFunction(Params);
  NetHeuristicFunction heuristicFunction(Params);
  DummyNetReadyFunction readyFunction;
  ASInterruptFunction interruptFunction(Params.nVarisLimit);

  NetAStar astar(start, end, costFunction, heuristicFunction,readyFunction, interruptFunction);
  NetContext netCont;
  netCont._map = pMap;
  netCont._netTemplate = pNetTemplate;
  netCont._params = &Params;
  
  g_SavedPos = 0;
  unsigned int nIters = astar.Process((void *) &netCont);
  LogF("nIters: %d, nSavePos: %d", nIters, g_SavedPos);

  if (astar.IsFound() || AfxMessageBox(IDS_AUTONET_GENER_NOT_SUCCESS,MB_OK|MB_YESNOCANCEL)==IDYES)
  {
    const AStarNode<NetField> * node = astar.IsFound() ? astar.GetLastNode() : astar.GetBestNode();    
    CObArray cRevFinal;
    while (node->_parent != NULL) 
    {
      if (node->_field.m_pNetPart != NULL)
        cRevFinal.Add(node->_field.m_pNetPart->CreateCopyObject());

      node = node->_parent; 
    };

    for(int i = cRevFinal.GetSize() - 1; i >= 0; i--)
    {
      FinalPath.Add(cRevFinal[i]);
    }
    AfxGetApp()->EndWaitCursor();
    return TRUE;
  }

  AfxGetApp()->EndWaitCursor();
  return FALSE;
}

#if  DEBUG_BEST_NODE
void DebugFirst(AStarNode<NetField> * node)
{
  LogF("New: f %f, g %f, ", node->_f, node->_g);
  while (node->_parent != NULL) 
  {
    if (node->_field.m_pNetPart != NULL)
      LogF("%s,",node->_field.m_pNetPart->m_nComponentName);

    node = node->_parent; 
  };
  LogF("\n");
}
#endif