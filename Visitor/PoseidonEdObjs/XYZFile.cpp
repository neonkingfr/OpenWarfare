#include "stdafx.h"
#include "XYZFile.h"
#include <float.h>

BOOL XYZFile::LoadDimension(SDimension & dim) 
{

  // Move the cursor to the beginning
  FILE * f = fopen(_sourceFileName,"rt");
  if (f == NULL)
    return FALSE;

  fseek(f, 0, SEEK_SET);

  // Initialize dimension
  _dimension.MinX = FLT_MAX;
  _dimension.MaxX = -FLT_MAX;
  _dimension.MinY = FLT_MAX;
  _dimension.MaxY = -FLT_MAX;
  _dimension.MinZ = FLT_MAX;
  _dimension.MaxZ = -FLT_MAX;
  _dimension.StepX = 0.0f;
  _dimension.StepY = 0.0f;

  // Read each line and get values
  float X, Y, Z;
  float SecondMaxX = -FLT_MAX;
  float SecondMaxY = -FLT_MAX;
  while (fscanf(f, "%f %f %f", &X, &Y, &Z) == 3) 
  {
    if (X < _dimension.MinX) 
      _dimension.MinX = X;

    if (X > _dimension.MaxX) 
    {
      SecondMaxX = _dimension.MaxX;
      _dimension.MaxX = X;
    }
    else if ((X > SecondMaxX) && (X != _dimension.MaxX)) 
    {
      SecondMaxX = X;
    }
    if (Y < _dimension.MinY) _dimension.MinY = Y;
    if (Y > _dimension.MaxY) {
      SecondMaxY = _dimension.MaxY;
      _dimension.MaxY = Y;
    }
    else if ((Y > SecondMaxY) && (Y != _dimension.MaxY)){
      SecondMaxY = Y;
    }
    if (Z < _dimension.MinZ) _dimension.MinZ = Z;
    if (Z > _dimension.MaxZ) _dimension.MaxZ = Z;
  }
  _dimension.StepX = _dimension.MaxX - SecondMaxX;
  _dimension.StepY = _dimension.MaxY - SecondMaxY;

  _dimension.SizeX = (int) ((_dimension.MaxX - _dimension.MinX) / _dimension.StepX);
  _dimension.SizeY = (int) ((_dimension.MaxY - _dimension.MinY) / _dimension.StepY);

  dim = _dimension;

  fclose(f);

  return TRUE;
}

BOOL XYZFile::Load(CPoseidonMap& map,const SMapSize& mapSize, bool keepObjects) 
{
  FILE *f;
  if ((f = fopen(_sourceFileName, "rt")) == NULL) 
    return FALSE;

  // The sizes must be equal and power of 2
  int size = mapSize.size[0];

  int log = 0;
  int orig = size;
  for(; size > 0; log++)
    size = size >> 1;

  if (orig == 1 << (log - 1))
    size = 1 << (log - 1);
  else
    size = 1 << log;

  if (keepObjects) map.NewTerrain(size,size);
  else map.OnNewMap(size, size);    

  // Read data
  fseek(f, 0, SEEK_SET);
  float X, Y, Z;    
  while(fscanf(f, "%f %f %f", &X, &Y, &Z) == 3)
  {
    int xx = (int) ((X - mapSize.offset[0]) / mapSize.step);
    if (xx >= size || xx < 0)
      continue;

    int yy = (int) ((Y - mapSize.offset[1]) / mapSize.step);
    if (yy >= size || yy < 0)
      continue;

    map.SetLandscapeHeight(xx,yy,Z, true,false);
  }


  // Close file
  fclose(f);
  return TRUE;
}