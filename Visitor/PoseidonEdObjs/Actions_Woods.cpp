/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Akce s lesy

// editace parametr� pro zalesn�n� plochy
extern BOOL DoCreateWoodParamsDlg(CPosWoodTemplate* pWoodTempl,
	BOOL& bTrjKonvex,BOOL& bTrjKonkav,BOOL& bGenMlazi,
	BOOL& bClearArea);



/////////////////////////////////////////////////////////////////////////////
// vytvo�en� akce pro vytvo�en� lesa

// generov�n� z�kladn�ch objekt� lesa
#define WOOD_OBJ_RECT	0
#define WOOD_OBJ_TRJ_N	1
#define WOOD_OBJ_TRJ_E	2
#define WOOD_OBJ_TRJ_S	3
#define WOOD_OBJ_TRJ_W	4	
#define WOOD_OBJ_MLZ_N	5
#define WOOD_OBJ_MLZ_E	6
#define WOOD_OBJ_MLZ_S	7
#define WOOD_OBJ_MLZ_W	8	

void CreateBaseWoodObject(CPosWoodObject* pWood,CPosWoodTemplate* pTempl,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup,
						  POINT ptAt,int nType)
{
	CPosObjectAction* pAction  = NULL;
	switch(nType)
	{
	case WOOD_OBJ_TRJ_N:
		pAction = (CPosObjectAction*)Action_CreatePoseidonObject(CPoint(ptAt),pTempl->m_pWoodObjTEntir,pDocument,pToGroup);
		break;
	case WOOD_OBJ_TRJ_E:
		pAction = (CPosObjectAction*)Action_CreatePoseidonObject(CPoint(ptAt),pTempl->m_pWoodObjTEntir,pDocument,pToGroup);
		if ((pAction)&&(pAction->m_pOldValue))
		{	ASSERT_KINDOF(CPosEdObject,pAction->m_pOldValue);
			((CPosEdObject*)(pAction->m_pOldValue))->CalcRotationPosition(90.);
		}
		break;
	case WOOD_OBJ_TRJ_S:
		pAction = (CPosObjectAction*)Action_CreatePoseidonObject(CPoint(ptAt),pTempl->m_pWoodObjTEntir,pDocument,pToGroup);
		if ((pAction)&&(pAction->m_pOldValue))
		{	ASSERT_KINDOF(CPosEdObject,pAction->m_pOldValue);
			((CPosEdObject*)(pAction->m_pOldValue))->CalcRotationPosition(180.);
		}
		break;
	case WOOD_OBJ_TRJ_W:
		pAction = (CPosObjectAction*)Action_CreatePoseidonObject(CPoint(ptAt),pTempl->m_pWoodObjTEntir,pDocument,pToGroup);
		if ((pAction)&&(pAction->m_pOldValue))
		{	ASSERT_KINDOF(CPosEdObject,pAction->m_pOldValue);
			((CPosEdObject*)(pAction->m_pOldValue))->CalcRotationPosition(270.);
		}
		break;
	case WOOD_OBJ_MLZ_N:
		pAction = (CPosObjectAction*)Action_CreatePoseidonObject(CPoint(ptAt),pTempl->m_pWoodObjRFrame,pDocument,pToGroup);
		break;
	case WOOD_OBJ_MLZ_E:
		pAction = (CPosObjectAction*)Action_CreatePoseidonObject(CPoint(ptAt),pTempl->m_pWoodObjRFrame,pDocument,pToGroup);
		if ((pAction)&&(pAction->m_pOldValue))
		{	ASSERT_KINDOF(CPosEdObject,pAction->m_pOldValue);
			((CPosEdObject*)(pAction->m_pOldValue))->CalcRotationPosition(90.);
		}
		break;
	case WOOD_OBJ_MLZ_S:
		pAction = (CPosObjectAction*)Action_CreatePoseidonObject(CPoint(ptAt),pTempl->m_pWoodObjRFrame,pDocument,pToGroup);
		if ((pAction)&&(pAction->m_pOldValue))
		{	ASSERT_KINDOF(CPosEdObject,pAction->m_pOldValue);
			((CPosEdObject*)(pAction->m_pOldValue))->CalcRotationPosition(180.);
		}
		break;
	case WOOD_OBJ_MLZ_W:
		pAction = (CPosObjectAction*)Action_CreatePoseidonObject(CPoint(ptAt),pTempl->m_pWoodObjRFrame,pDocument,pToGroup);
		if ((pAction)&&(pAction->m_pOldValue))
		{	ASSERT_KINDOF(CPosEdObject,pAction->m_pOldValue);
			((CPosEdObject*)(pAction->m_pOldValue))->CalcRotationPosition(270.);
		}
		break;
	default:
		// WOOD_OBJ_RECT
		pAction = (CPosObjectAction*)Action_CreatePoseidonObject(CPoint(ptAt),pTempl->m_pWoodObjREntir,pDocument,pToGroup);
	};
	if (pAction)
	{
		ASSERT_KINDOF(CPosObjectAction,pAction);
		// nebude se m�nit selekce
		pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
		// nebude se prov�d�t update
		pAction->m_bUpdateView	  = FALSE;
		// objekt je pod��zen lesu
		ASSERT(pAction->m_pOldValue != NULL);
		ASSERT_KINDOF(CPosEdObject,pAction->m_pOldValue);
		((CPosEdObject*)(pAction->m_pOldValue))->m_nMasterWood = pWood->GetID();
	}
};

BOOL IsWoodTriangleAtPos(CPointArray& Points,UNITPOS Pos)
{
	for(int i=0; i<Points.Size(); i++)
	{
		if ((Points[i].x == Pos.x)&&
			(Points[i].y == Pos.z))
			return TRUE;
	}
	return FALSE;
};

void GenerBaseWoodObjects(CPosWoodObject* pWood,CPosWoodTemplate* pTempl,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup,
						  BOOL bTrjKonvex,BOOL bTrjKonkav,BOOL bGenMlazi)
{
	CPoseidonMap* pMap = &pDocument->m_PoseidonMap;
	float   nUnitSz2   = ((float)pDocument->m_nRealSizeUnit)/2.f;

	CRect	rLogHull;
	UNITPOS LftTop,RghBtm;
	// obal pro vytv��en� lesa
	pWood->GetObjectHull(rLogHull);
	GetUnitPosHull(LftTop,RghBtm,rLogHull,&pDocument->m_PoseidonMap);

	// poloha lesa p�i generov�n�
	CPointArray		UnitsForMlazi;
	CPointArray		UnitsTriangle;
	CPositionArea	WoodPosition;
	WoodPosition.CopyFrom(pWood->GetLogObjectPosition());

	ASSERT(pWood != NULL);
	for(int x=LftTop.x; x<=RghBtm.x; x++)
	for(int z=LftTop.z; z>=RghBtm.z; z--)
	{
		// testovan� unit
		UNITPOS nUnit = { x, z },nUnPom;
		REALPOS nReal = pMap->FromUnitToRealPos(nUnit);
		nReal.x += nUnitSz2;	// um�st�m na st�ed
		nReal.z += nUnitSz2;
		POINT	nLog  = pMap->FromRealToViewPos(nReal);
		// testuji obsazen� bod�
		BOOL bCt,bPt1,bPt2,bPt3,bPt4;
		BOOL bTrg = FALSE;
		bCt	   = IsUnitCenterAtArea(nUnit,&WoodPosition,&pDocument->m_PoseidonMap);
		nUnPom = nUnit; nUnPom.z++;
		bPt1   = IsUnitCenterAtArea(nUnPom,&WoodPosition,&pDocument->m_PoseidonMap);
		if (!bTrg) bTrg = IsWoodTriangleAtPos(UnitsTriangle,nUnPom);
		nUnPom = nUnit; nUnPom.x++;
		bPt2   = IsUnitCenterAtArea(nUnPom,&WoodPosition,&pDocument->m_PoseidonMap);
		if (!bTrg) bTrg = IsWoodTriangleAtPos(UnitsTriangle,nUnPom);
		nUnPom = nUnit; nUnPom.z--;
		bPt3   = IsUnitCenterAtArea(nUnPom,&WoodPosition,&pDocument->m_PoseidonMap);
		if (!bTrg) bTrg = IsWoodTriangleAtPos(UnitsTriangle,nUnPom);
		nUnPom = nUnit; nUnPom.x--;
		bPt4   = IsUnitCenterAtArea(nUnPom,&WoodPosition,&pDocument->m_PoseidonMap);
		if (!bTrg) bTrg = IsWoodTriangleAtPos(UnitsTriangle,nUnPom);
		// po�et sousedn�ch pol�
		int    nCount = 0;
		if (bPt1) nCount++; if (bPt2) nCount++;
		if (bPt3) nCount++; if (bPt4) nCount++;
		
		BOOL bCreated = FALSE;

		if (!bTrg)
		{	// nen� sousedn� troj�heln�k)
			if (((!bCt)&&(bTrjKonkav)&&(nCount==2))||
				 ((bCt)&&(bTrjKonvex)&&(nCount==2)))
			{	// dopln�n� konkavn�ho troj�heln�ka
				// dopln�n� konvexn�ho troj�heln�ka
				if (bPt1 && bPt2)
					CreateBaseWoodObject(pWood,pTempl,pDocument,pToGroup,nLog,WOOD_OBJ_TRJ_E);
				if (bPt2 && bPt3)
					CreateBaseWoodObject(pWood,pTempl,pDocument,pToGroup,nLog,WOOD_OBJ_TRJ_S);
				if (bPt3 && bPt4)
					CreateBaseWoodObject(pWood,pTempl,pDocument,pToGroup,nLog,WOOD_OBJ_TRJ_W);
				if (bPt4 && bPt1)
					CreateBaseWoodObject(pWood,pTempl,pDocument,pToGroup,nLog,WOOD_OBJ_TRJ_N);
				// vytvo�en troj�heln�k
				bCreated = TRUE;
				UnitsTriangle.Add(CPoint(x,z));
				// mus�m p�idat �tverec do obalu lesa
				if ((!bCt)&&(bTrjKonkav)&&(nCount==2))
				{
					CRect rPos;
					REALPOS nRel = pMap->FromUnitToRealPos(nUnit);
					POINT	nLog = pMap->FromRealToViewPos(nRel);
					rPos.left	= nLog.x;
					rPos.bottom = nLog.y;
					nRel.x += 2*nUnitSz2;	
					nRel.z += 2*nUnitSz2;
					nLog  = pMap->FromRealToViewPos(nRel);
					rPos.right	= nLog.x;
					rPos.top    = nLog.y;
					pWood->AddRect(rPos);
				}
			}
		};
		// nebyl-li vytvo�en objekt->zkusim �tverec
		if ((bCt)&&(!bCreated))
		{
			CreateBaseWoodObject(pWood,pTempl,pDocument,pToGroup,nLog,WOOD_OBJ_RECT);
			if (bGenMlazi && (nCount < 4))
			{	// aspirant na generov�n� ml�z�
				UnitsForMlazi.Add(CPoint(x,z));
			}
		};
	};
	// generuji ml�z�
	if (bGenMlazi)
	for(int i=0;i<UnitsForMlazi.Size();i++)
	{
		POINT	ptUnit = UnitsForMlazi[i];

		// testovan� unit
		UNITPOS nUnit = { ptUnit.x, ptUnit.y },nUnPom;
		REALPOS nReal = pMap->FromUnitToRealPos(nUnit);
		nReal.x += nUnitSz2;	// um�st�m na st�ed
		nReal.z += nUnitSz2;
		POINT	nLog  = pMap->FromRealToViewPos(nReal);
		nReal.x  = (float)nUnitSz2;
		POINT	nDif  = pMap->FromRealToViewPos(nReal);
		// testuji obsazen� bod�
		BOOL bPt1,bPt2,bPt3,bPt4;
		ASSERT(IsUnitCenterAtArea(nUnit,pWood->GetLogObjectPosition(),&pDocument->m_PoseidonMap));
		nUnPom = nUnit; nUnPom.z++;
		bPt1   = IsUnitCenterAtArea(nUnPom,pWood->GetLogObjectPosition(),&pDocument->m_PoseidonMap);
		nUnPom = nUnit; nUnPom.x++;
		bPt2   = IsUnitCenterAtArea(nUnPom,pWood->GetLogObjectPosition(),&pDocument->m_PoseidonMap);
		nUnPom = nUnit; nUnPom.z--;
		bPt3   = IsUnitCenterAtArea(nUnPom,pWood->GetLogObjectPosition(),&pDocument->m_PoseidonMap);
		nUnPom = nUnit; nUnPom.x--;
		bPt4   = IsUnitCenterAtArea(nUnPom,pWood->GetLogObjectPosition(),&pDocument->m_PoseidonMap);
		// generuji ml�z�
		if (!bPt1)
			CreateBaseWoodObject(pWood,pTempl,pDocument,pToGroup,CPoint(nLog.x,nLog.y-nDif.x),WOOD_OBJ_MLZ_N);
		if (!bPt2)
			CreateBaseWoodObject(pWood,pTempl,pDocument,pToGroup,CPoint(nLog.x+nDif.x,nLog.y),WOOD_OBJ_MLZ_E);
		if (!bPt3)
			CreateBaseWoodObject(pWood,pTempl,pDocument,pToGroup,CPoint(nLog.x,nLog.y+nDif.x),WOOD_OBJ_MLZ_S);
		if (!bPt4)
			CreateBaseWoodObject(pWood,pTempl,pDocument,pToGroup,CPoint(nLog.x-nDif.x,nLog.y),WOOD_OBJ_MLZ_W);
	}
};


CPosAction* Action_CreatePoseidonWood(CPosWoodTemplate* pTempl,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pTempl != NULL)
	{
		BOOL bTrjKonvex,bTrjKonkav,bGenMlazi,bClearArea;
		// parametry pro vkl�d�n� lesa
		if (!DoCreateWoodParamsDlg(pTempl,bTrjKonvex,bTrjKonkav,bGenMlazi,bClearArea))
			return NULL;

		CPosWoodObject*		pWood		= NULL;
		CPosActionGroup*	pGrpAction  = NULL;
		TRY
		{
			// vytvo�en� nov�ho lesa
			pWood = new CPosWoodObject(pDocument->m_PoseidonMap);
			pWood->CreateFromTemplateAt(pTempl,&(pDocument->m_CurrentArea),pDocument->m_PoseidonMap.GetFreeWoodID());
	
			// vytvo�en� skupinov� ud�losti
			pGrpAction = new CPosActionGroup(IDA_WOOD_CREATE);
			pGrpAction->m_bFromBuldozer = FALSE;

			// vytvo�en� objektu lesa
			CPosObjectAction* pWoodAct = (CPosObjectAction*)(pDocument->GenerNewObjectAction(IDA_WOOD_CREATE,
					NULL,pWood,FALSE,pGrpAction));
			// generov�n�/vytvo�en� z�kladn�ch objekt� lesa
			GenerBaseWoodObjects(pWood,pTempl,pDocument,pGrpAction,bTrjKonvex,bTrjKonkav,bGenMlazi);

			// uvoln�n� vybran� plochy
			if (bClearArea)
			{
				CPosAction* pAreaAct = Action_DelPoseidonCurrArea(pDocument,pGrpAction);
				if (pAreaAct)	// neprov�d�m update - les je v�t��
					pAreaAct->m_bUpdateView = FALSE;
			}
			// provedu akci
			if (pToGroup)
				pToGroup->AddAction(pGrpAction);
			else
				pDocument->ProcessEditAction(pGrpAction);
		}
		CATCH_ALL(e)
		{
			MntReportMemoryException();
			if (pGrpAction)
			{
				delete pGrpAction;
				pGrpAction = NULL;
			} else
			{	// sma�u d�l�� objekty
				if (pWood)
				{
					delete pWood;
					pWood = NULL;
				}
			};
		}
		END_CATCH_ALL
		return pGrpAction;
	}
	return NULL;
};

/////////////////////////////////////////////////////////////////////////////
// vytvo�en� akce pro smaz�n� lesa

CPosAction* Action_DeletePoseidonWood(CPosWoodObject* pToDel,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pToDel != NULL)
	{
		CPosObject* pDocObj = pDocument->GetDocumentObjectFromValue(pToDel);
		if (pDocObj)
		{
			ASSERT_KINDOF(CPosWoodObject,pDocObj);
			CPosActionGroup*	pGrpAction  = NULL;
			TRY
			{
				// vytvo�en� skupinov� ud�losti
				pGrpAction = new CPosActionGroup(IDA_WOOD_DEL);
				pGrpAction->m_bFromBuldozer = FALSE;
				// smaz�n� objektu lesa
				pDocument->GenerNewObjectAction(IDA_WOOD_DEL,pDocObj,NULL,FALSE,pGrpAction);
				// generov�n� zru�en� objekt�
				for(int i=0; i<pDocument->m_PoseidonMap.GetPoseidonObjectCount(); i++)
				{
					CPosEdObject* pObj = pDocument->m_PoseidonMap.GetPoseidonObject(i);
					if ((pObj != NULL)&&(pObj->m_nMasterWood==pToDel->GetID()))
					{	// toto je objekt spr�vn�ho lesa
						CPosObjectAction* pAction = (CPosObjectAction*)Action_DeletePoseidonObject(pObj,pDocument,pGrpAction);
						if (pAction)
						{
							// nebude se m�nit selekce
							pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
							// nebude se prov�d�t update
							pAction->m_bUpdateView	  = FALSE;
						}
					}
					
				}
				// provedu akci
				if (pToGroup)
					pToGroup->AddAction(pGrpAction);
				else
					pDocument->ProcessEditAction(pGrpAction);
			}
			CATCH_ALL(e)
			{
				MntReportMemoryException();
				if (pGrpAction)
				{
					delete pGrpAction;
					pGrpAction = NULL;
				}
			}
			END_CATCH_ALL
			return pGrpAction;
		}
	}
	return NULL;
};

/////////////////////////////////////////////////////////////////////////////
// vytvo�en� akce pro dekompozici lesa

CPosAction* Action_DekompPoseidonWood(CPosWoodObject* pWood,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pWood != NULL)
	{
		CPosObject* pDocObj = pDocument->GetDocumentObjectFromValue(pWood);
		if (pDocObj)
		{
			ASSERT_KINDOF(CPosWoodObject,pDocObj);
			CPosActionGroup*	pGrpAction  = NULL;
			TRY
			{
				// vytvo�en� skupinov� ud�losti
				pGrpAction = new CPosActionGroup(IDA_OBJECT_DEKOMP);
				pGrpAction->m_bFromBuldozer = FALSE;
				// smaz�n� objektu lesa
				pDocument->GenerNewObjectAction(IDA_WOOD_DEL,pDocObj,NULL,FALSE,pGrpAction);
				// generov�n� zm�n objekt�
				for(int i=0; i<pDocument->m_PoseidonMap.GetPoseidonObjectCount(); i++)
				{
					CPosEdObject* pObj = pDocument->m_PoseidonMap.GetPoseidonObject(i);
					if ((pObj != NULL)&&(pObj->m_nMasterWood==pWood->GetID()))
					{	// toto je objekt spr�vn�ho lesa
						CPosEdObject ObjToAction(pDocument->m_PoseidonMap);
						ObjToAction.CopyFrom(pObj);
						ObjToAction.m_nMasterWood = UNDEF_REG_ID;
						CPosObjectAction* pAction = (CPosObjectAction*)Action_EditPoseidonObject(&ObjToAction,pDocument,pGrpAction);
						if (pAction)
						{
							// nebude se m�nit selekce
							pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
						}
					}
					
				}
				// provedu akci
				if (pToGroup)
					pToGroup->AddAction(pGrpAction);
				else
					pDocument->ProcessEditAction(pGrpAction);
			}
			CATCH_ALL(e)
			{
				MntReportMemoryException();
				if (pGrpAction)
				{
					delete pGrpAction;
					pGrpAction = NULL;
				}
			}
			END_CATCH_ALL
			return pGrpAction;
		}
	}
	return NULL;
};

/////////////////////////////////////////////////////////////////////////////
// vytvo�en� akce pro p�esun lesa

CPosAction* Action_MovePoseidonWood(CPosWoodObject* pWood,int xAmount,int yAmount,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pWood != NULL)
	{
		CPosObject* pDocObj = pDocument->GetDocumentObjectFromValue(pWood);
		ASSERT(pDocObj != NULL);
		ASSERT(pDocObj != pWood);

		if (pDocObj)
		{
			ASSERT_KINDOF(CPosWoodObject,pDocObj);
			CPosActionGroup*	pGrpAction  = NULL;
			CPosWoodObject*		pAct		= NULL;
			TRY
			{
				// vytvo�en� skupinov� ud�losti
				pGrpAction = new CPosActionGroup(IDA_WOOD_MOVE);
				pGrpAction->m_bFromBuldozer = FALSE;
				// smaz�n� objektu lesa
				if ((pAct = (CPosWoodObject*)pWood->CreateCopyObject())==NULL) AfxThrowMemoryException();
				pAct->GetLogObjectPosition()->OffsetPosition(xAmount,yAmount);
				pDocument->GenerNewObjectAction(IDA_WOOD_MOVE,pDocObj,pAct,FALSE,pGrpAction);
				// generov�n� zm�n objekt�
				for(int i=0; i<pDocument->m_PoseidonMap.GetPoseidonObjectCount(); i++)
				{
					CPosEdObject* pObj = pDocument->m_PoseidonMap.GetPoseidonObject(i);
					if ((pObj != NULL)&&(pObj->m_nMasterWood==pWood->GetID()))
					{	// toto je objekt spr�vn�ho lesa
						CPosObjectAction* pAction = (CPosObjectAction*)Action_MovePoseidonObject(pObj,xAmount,yAmount,pDocument,pGrpAction);
						if (pAction)
						{
							// nebude se m�nit selekce
							pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
							// nebude se prov�d�t update
							pAction->m_bUpdateView	  = FALSE;
						}
					}
					
				}
				// provedu akci
				if (pToGroup)
					pToGroup->AddAction(pGrpAction);
				else
					pDocument->ProcessEditAction(pGrpAction);
			}
			CATCH_ALL(e)
			{
				MntReportMemoryException();
				if (pGrpAction)
				{
					delete pGrpAction;
					pGrpAction = NULL;
				}
			}
			END_CATCH_ALL
			return pGrpAction;
		}
	}
	return NULL;
};

CPosAction* Action_RotatePoseidonWood(CPosWoodObject* pWood,float grad, CPointVal origin,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  if (pWood != NULL)
  {
    CPosObject* pDocObj = pDocument->GetDocumentObjectFromValue(pWood);
    ASSERT(pDocObj != NULL);
    ASSERT(pDocObj != pWood);

    if (pDocObj)
    {
      ASSERT_KINDOF(CPosWoodObject,pDocObj);
      CPosActionGroup*	pGrpAction  = NULL;
      CPosWoodObject*		pAct		= NULL;
      TRY
      {
        // vytvo�en� skupinov� ud�losti
        pGrpAction = new CPosActionGroup(IDA_WOOD_MOVE);
        pGrpAction->m_bFromBuldozer = FALSE;
        // smaz�n� objektu lesa
        if ((pAct = (CPosWoodObject*) pWood->CreateCopyObject())==NULL) AfxThrowMemoryException();

        FMntMatrix2 trans;
        trans.Rotation(grad);
        CPoint offset = pAct->GetLogObjectPosition()->GetCenter();
        pAct->GetLogObjectPosition()->TransformPosition(trans,origin);
        offset = pAct->GetLogObjectPosition()->GetCenter() - offset;

        pDocument->GenerNewObjectAction(IDA_WOOD_MOVE,pDocObj,pAct,FALSE,pGrpAction);
        // generov�n� zm�n objekt�
        for(int i=0; i<pDocument->m_PoseidonMap.GetPoseidonObjectCount(); i++)
        {
          CPosEdObject* pObj = pDocument->m_PoseidonMap.GetPoseidonObject(i);
          if ((pObj != NULL)&&(pObj->m_nMasterWood==pWood->GetID()))
          {	
            // toto je objekt spr�vn�ho lesa
            // the wood objects must not be rotated
            CPosObjectAction* pAction = (CPosObjectAction*)Action_MovePoseidonObject(pObj,offset.x,offset.y,pDocument,pGrpAction);
            if (pAction)
            {
              // nebude se m�nit selekce
              pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
              // nebude se prov�d�t update
              pAction->m_bUpdateView	  = FALSE;
            }

          }

        }
        // provedu akci
        if (pToGroup)
          pToGroup->AddAction(pGrpAction);
        else
          pDocument->ProcessEditAction(pGrpAction);
      }
      CATCH_ALL(e)
      {
        MntReportMemoryException();
        if (pGrpAction)
        {
          delete pGrpAction;
          pGrpAction = NULL;
        }
      }
      END_CATCH_ALL
        return pGrpAction;
    }
  }
  return NULL;
};

CPosAction* Action_RotateMovePoseidonWood(CPosWoodObject* pWood,CPointVal offset, float grad,CPointVal origin,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  if (pWood != NULL)
  {
    CPosObject* pDocObj = pDocument->GetDocumentObjectFromValue(pWood);
    ASSERT(pDocObj != NULL);
    ASSERT(pDocObj != pWood);

    if (pDocObj)
    {
      ASSERT_KINDOF(CPosWoodObject,pDocObj);
      CPosActionGroup*	pGrpAction  = NULL;
      CPosWoodObject*		pAct		= NULL;
      TRY
      {
        // vytvo�en� skupinov� ud�losti
        pGrpAction = new CPosActionGroup(IDA_WOOD_MOVE);
        pGrpAction->m_bFromBuldozer = FALSE;
        // smaz�n� objektu lesa
        if ((pAct = (CPosWoodObject*) pWood->CreateCopyObject())==NULL) AfxThrowMemoryException();

        FMntMatrix2 trans;
        trans.Rotation(grad);
        CPoint newOffset = pAct->GetLogObjectPosition()->GetCenter();
        pAct->GetLogObjectPosition()->TransformPosition(trans,origin);
        pAct->GetLogObjectPosition()->OffsetPosition(offset.x, offset.y);
        newOffset = pAct->GetLogObjectPosition()->GetCenter() - newOffset;

        pDocument->GenerNewObjectAction(IDA_WOOD_MOVE,pDocObj,pAct,FALSE,pGrpAction);
        // generov�n� zm�n objekt�
        for(int i=0; i<pDocument->m_PoseidonMap.GetPoseidonObjectCount(); i++)
        {
          CPosEdObject* pObj = pDocument->m_PoseidonMap.GetPoseidonObject(i);
          if ((pObj != NULL)&&(pObj->m_nMasterWood==pWood->GetID()))
          {	
            // toto je objekt spr�vn�ho lesa
            // the wood objects must not be rotated
            CPosObjectAction* pAction = (CPosObjectAction*)Action_MovePoseidonObject(pObj,newOffset.x,newOffset.y,pDocument,pGrpAction);
            if (pAction)
            {
              // nebude se m�nit selekce
              pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
              // nebude se prov�d�t update
              pAction->m_bUpdateView	  = FALSE;
            }
          }
        }
        // provedu akci
        if (pToGroup)
          pToGroup->AddAction(pGrpAction);
        else
          pDocument->ProcessEditAction(pGrpAction);
      }
      CATCH_ALL(e)
      {
        MntReportMemoryException();
        if (pGrpAction)
        {
          delete pGrpAction;
          pGrpAction = NULL;
        }
      }
      END_CATCH_ALL
        return pGrpAction;
    }
  }
  return NULL;
}

