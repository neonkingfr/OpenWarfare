/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CManagementObjectsDlg dialog

class CManagementObjectsDlg : public CDialog
{
// Construction
public:
	CManagementObjectsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CManagementObjectsDlg)
	enum { IDD = IDD_MANAGEMENT_OBJECTS };
	CComboBox	m_cRecalcHgType;
	BOOL	m_bRecalcHeight;
	BOOL	m_bUpdateFromFile;
	//}}AFX_DATA
	int		m_nRecalcHgType;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CManagementObjectsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CManagementObjectsDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CManagementObjectsDlg::CManagementObjectsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CManagementObjectsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CManagementObjectsDlg)
	m_bRecalcHeight = FALSE;
	m_bUpdateFromFile = FALSE;
	//}}AFX_DATA_INIT
	m_nRecalcHgType = 0;
}

BOOL CManagementObjectsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	return TRUE;
}

void CManagementObjectsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CManagementObjectsDlg)
	DDX_Control(pDX, IDC_COMBO_HGTYPE, m_cRecalcHgType);
	DDX_Check(pDX, IDC_OBJS_RECALC, m_bRecalcHeight);
	DDX_Check(pDX, IDC_OBJS_UPDFILE, m_bUpdateFromFile);
	//}}AFX_DATA_MAP
	if (SAVEDATA)
		m_nRecalcHgType = m_cRecalcHgType.GetCurSel();
	else
		m_cRecalcHgType.SetCurSel(m_nRecalcHgType);
}


BEGIN_MESSAGE_MAP(CManagementObjectsDlg, CDialog)
	//{{AFX_MSG_MAP(CManagementObjectsDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// update hodnot podle souboru

void DoUpdateObjectParams(CPosObjectTemplate* pTempl)
{
	if (pTempl!=NULL)
	{
		ASSERT_KINDOF(CPosObjectTemplate,pTempl);
		CString sFile,sText,sName;
		pTempl->GetFullFileName(sFile);
		if (!MntExistFile(sFile))
		{
			pTempl->GetBuldozerFileName(sName);
			AfxFormatString1(sText,IDS_OBJECT_FILE_NOT_FOUND,sName);
			sText += sFile;
			AfxMessageBox(sText,MB_OK|MB_ICONEXCLAMATION);
		} else
		{
			pTempl->UpdateParamsFromSourceFile();
		};
	}
};


void DoUpdateDefinedObjectParams(CPosEdBaseDoc* pDoc,CPoseidonMap* pMap)
{
	int i=0;
	for(i=0; i<pDoc->ObjectTemplatesSize(); i++)
	{
		CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(pDoc->GetIthObjectTemplate(i));
		DoUpdateObjectParams(pTempl);
	}
};

void DoUpdateDefinedWoodsObjectParams(CPosEdBaseDoc* pDoc,CPoseidonMap* pMap)
{
	int i;
	for(i=0; i<pDoc->m_MgrWoods.m_WoodTemplates.GetSize(); i++)
	{
		CPosWoodTemplate* pWood = (CPosWoodTemplate*)(pDoc->m_MgrWoods.m_WoodTemplates[i]);
		if (pWood)
		{
			DoUpdateObjectParams(pWood->m_pWoodObjREntir);
			DoUpdateObjectParams(pWood->m_pWoodObjRFrame);
			DoUpdateObjectParams(pWood->m_pWoodObjTEntir);
		}
	}
};

void DoUpdateDefinedNetsObjectParams(CPosEdBaseDoc* pDoc,CPoseidonMap* pMap)
{
	int i;
	for(i=0; i<pDoc->m_MgrNets.m_NetTemplates.GetSize(); i++)
	{
		CPosNetTemplate* pNet = (CPosNetTemplate*)(pDoc->m_MgrNets.m_NetTemplates[i]);
		if (pNet)
		{
			ASSERT_KINDOF(CPosNetTemplate,pNet);
			int j;
			for(j=0; j<pNet->m_NetSTRA.GetSize(); j++)
			{
				CNetComponent* pComp = (CNetComponent*)(pNet->m_NetSTRA[j]);
				if (pComp)
				{
					ASSERT_KINDOF(CNetComponent,pComp);
					DoUpdateObjectParams(pComp->GetObjectTemplate());
					if (pComp->GetObjectTemplate() != NULL)
						pComp->CreateFromObjectTemplate(pComp->GetObjectTemplate());
				}
			}
		}
	}
	for(i=0; i<pDoc->m_MgrNets.m_CrossTemplates.GetSize(); i++)
	{
		CPosCrossTemplate* pCross = (CPosCrossTemplate*)(pDoc->m_MgrNets.m_CrossTemplates[i]);
		if (pCross)
		{
			ASSERT_KINDOF(CPosCrossTemplate,pCross);
			DoUpdateObjectParams(pCross->GetObjectTemplate());
		}
	}
};


/////////////////////////////////////////////////////////////////////////////
// p�epo��t�n� v��ky

void DoUpdateObjectHeight(CPoseidonMap* pMap,BOOL bResetRel)
{
	for(int i=0; i<pMap->m_InstanceObjs.GetSize(); i++)
	{
		CPosEdObject* pObj = (CPosEdObject*)(pMap->m_InstanceObjs[i]);
		if (pObj)
		{
			ASSERT_KINDOF(CPosEdObject,pObj);
			ASSERT(pObj->m_pTemplate != NULL);
			if (bResetRel)
			{
				pObj->m_nRelHeight = 0.f;
			}
			pObj->RepairMatrix();
			pObj->CalcObjectPoseidonHeight();
		}
	}
};

/////////////////////////////////////////////////////////////////////////////
// Spr�va objekt�

void CPosEdMainDoc::OnObjectsManagement()
{
	CManagementObjectsDlg dlg;
	if (dlg.DoModal() != IDOK)
		return;

	AfxGetApp()->BeginWaitCursor();	
	if (dlg.m_bUpdateFromFile)
	{	// update hodnot podle souboru
		DoUpdateDefinedObjectParams(this,&m_PoseidonMap);
		DoUpdateDefinedWoodsObjectParams(this,&m_PoseidonMap);
		DoUpdateDefinedNetsObjectParams(this,&m_PoseidonMap);	
	}
	if (dlg.m_bRecalcHeight)
	{	// p�epo��t�n� v��ky
		DoUpdateObjectHeight(&m_PoseidonMap,dlg.m_nRecalcHgType!=0);
	}

	// provedu-li n�co -> reset UNDO
	if ((dlg.m_bRecalcHeight)||(dlg.m_bUpdateFromFile))
	{
		m_ActionManager.ClearUndoRedoBuffers();
		SetModifiedFlag();
		UpdateAllViews(NULL,0,NULL);
    m_PoseidonMap.TransferMapToBuldozer();
	}

	AfxGetApp()->EndWaitCursor();
};

void CPosEdMainDoc::OnUpdateObjectsManagement(CCmdUI* pCmdUI)
{	pCmdUI->Enable(TRUE); };

/////////////////////////////////////////////////////////////////////////////
// CManageObjChanges dialog

class CManageObjChanges : public CDialog
{
// Construction
public:
	CManageObjChanges(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	CPoseidonMap*	m_pMap;

	CPosObjectTemplate* pTempl1;
	CPosObjectTemplate* pTempl2;

	//{{AFX_DATA(CManageObjChanges)
	enum { IDD = IDD_MANAGCHANGE_OBJECTS };
	CListBox	m_List2;
	CListBox	m_List1;
	int		m_nActiontype;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CManageObjChanges)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CManageObjChanges)
	afx_msg void OnRadio1();
	afx_msg void OnRadio2();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// CManageObjChanges dialog


CManageObjChanges::CManageObjChanges(CWnd* pParent /*=NULL*/)
	: CDialog(CManageObjChanges::IDD, pParent)
{
	pTempl1 = pTempl2 = NULL;
	//{{AFX_DATA_INIT(CManageObjChanges)
	m_nActiontype = 0;
	//}}AFX_DATA_INIT
}

BOOL CManageObjChanges::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// napln�n� seznam�
  int n = m_pMap->GetDocument()->ObjectTemplatesSize();
	for(int i=0; i<n; i++)
	{
		CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(m_pMap->GetDocument()->GetIthObjectTemplate(i));
    if (pTempl != NULL && (pTempl->m_nObjType == CPosObjectTemplate::objTpNatural ||
      pTempl->m_nObjType == CPosObjectTemplate::objTpPeople))
		{
      if (pTempl->m_nTimesUsed > 0)
			  LBAddString(m_List1.m_hWnd,pTempl->GetName(),(DWORD)pTempl);

			LBAddString(m_List2.m_hWnd,pTempl->GetName(),(DWORD)pTempl);
		}
	}

	// ukryt� n�hrady
	m_List2.ShowWindow(SW_HIDE);
	return TRUE; 
}

void CManageObjChanges::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CManageObjChanges)
	DDX_Control(pDX, IDC_LIST2, m_List2);
	DDX_Control(pDX, IDC_LIST1, m_List1);
	DDX_Radio(pDX, IDC_RADIO1, m_nActiontype);
	//}}AFX_DATA_MAP
	if (SAVEDATA)
	{
		DWORD dwData;
		pTempl1 = pTempl2 = NULL;

		MDDV_LBValidSelItem(pDX, IDC_LIST1);
		VERIFY(LBGetCurSelData(m_List1.m_hWnd,dwData));
		pTempl1 = (CPosObjectTemplate*)dwData;
		
		if (m_nActiontype != 0)
		{
			MDDV_LBValidSelItem(pDX, IDC_LIST2);
			VERIFY(LBGetCurSelData(m_List2.m_hWnd,dwData));
			pTempl2 = (CPosObjectTemplate*)dwData;
		};
	}
}


BEGIN_MESSAGE_MAP(CManageObjChanges, CDialog)
	//{{AFX_MSG_MAP(CManageObjChanges)
	ON_BN_CLICKED(IDC_RADIO1, OnRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnRadio2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CManageObjChanges::OnRadio1() 
{	// ukryt� n�hrady
	m_List2.ShowWindow(SW_HIDE);
}

void CManageObjChanges::OnRadio2() 
{	// zobrazen� n�hrady
	m_List2.ShowWindow(SW_SHOW);
}

/////////////////////////////////////////////////////////////////////////////
// CManageObjChanges message handlers

void CPosEdMainDoc::OnObjectsManageChange()
{
	CManageObjChanges dlg;
	dlg.m_pMap = &(m_PoseidonMap);

	if (dlg.DoModal() != IDOK)
		return;

	// proch�z�m objekty
  if (dlg.pTempl2 != NULL)
  {	// nahradim
    CPosGroupObject groupDel(m_PoseidonMap);
    CPosGroupObject groupAdd(m_PoseidonMap);

	  for(int i=0;i<m_PoseidonMap.m_InstanceObjs.GetSize();i++)
	  {
		  CPosEdObject* pEdObj = (CPosEdObject*)(m_PoseidonMap.m_InstanceObjs[i]);
		  if (pEdObj != NULL)
		  {
			  if (pEdObj->m_nTemplateID == dlg.pTempl1->m_ID)
			  {	// to je spravny objekt	
          groupDel.AddObjectGroup(pEdObj, false);
          CPosEdObject * pNewObj = new CPosEdObject(m_PoseidonMap);

          pNewObj->CreateFromTemplateAt(dlg.pTempl2, *pEdObj);
          groupAdd.AddObjectGroup(pNewObj, false);					
				} 
      }
    }

    CPosObjectGroupAction* pGrpAction = new CPosObjectGroupAction(IDA_GROUPOBJ_REPLACE);
    if (pGrpAction)
    {
      Action_DeletePoseidonObjects(&groupDel, this, pGrpAction);
      Action_CreatePoseidonObjects(&groupAdd, this, pGrpAction);
      ProcessEditAction(pGrpAction);
    }
  } 
  else
  {	
    CPosGroupObject group(m_PoseidonMap);
    for(int i=0;i<m_PoseidonMap.m_InstanceObjs.GetSize();i++)
    {
      CPosEdObject* pEdObj = (CPosEdObject*)(m_PoseidonMap.m_InstanceObjs[i]);
      if (pEdObj != NULL)
      {
        if (pEdObj->m_nTemplateID == dlg.pTempl1->m_ID)
        {	// to je spravny objekt			
          // vymazu          
          group.AddObjectGroup(pEdObj, false);          
        }
      };			
		}
    Action_DeletePoseidonObjects(&group, this, NULL);
	}

	//m_ActionManager.ClearUndoRedoBuffers();
	//SetModifiedFlag();
	//UpdateAllViews(NULL,0,NULL);
};


