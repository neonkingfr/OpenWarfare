/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#if !defined(_POSEDOBJS_MAIN_H_)
#define _POSEDOBJS_MAIN_H_

/////////////////////////////////////////////////////////////////////////////
//  PoseidonEdObjs.h : header file
//	resource >> 16000

class CPosEdMainDoc;
class CPosObjectsPanel;
#include "PosClipboard.h"
#include "VisSelectionList.h"
#include "IAdditionalEditor.h"

// MouseModes for scroller
#define MM_AREA_ADD				100
#define MM_AREA_CRT				101
#define MM_VERTEX_AREA_ADD 202
#define MM_BGIMAGE_AREA_ADD 303

// standardn� krok vrstevnic
#define COUNTLN_STEP_NORMAL		10.


// maximal allowed move to handle click like single click
#define MAX_MOUSE_CLICK_MOVE    4

// koncovka souboru pro pojmenovan� skupiny objektu
#define SEL_OBJ_FILE_EXT		_T("pso")

/////////////////////////////////////////////////////////////////////////////
// CPosEdCursor

class /*AFX_EXT_CLASS*/ CPosEdCursor : public CObject
{
DECLARE_DYNAMIC(CPosEdCursor)
public:
				CPosEdCursor();
				~CPosEdCursor();

	// nastaven� polohy cursoru
		void	SetCursorLogPosition(POINT ptPos,BOOL bFromBuldozer);
		void	SetCursorRealPosition(REALPOS ptPos,BOOL bFromBuldozer);

	// update polohy mezi aplikacemi
		void	SendPositionToBuldozer() const;

	// vykreslov�n� kurzoru
		void	OnDrawCursorInDP(CDC* pDC,const POINT& Pos,const CDrawPar* pParams) const;


// data
public:
	CPosEdMainDoc*	m_pDocument;
CPosEdEnvironment*	m_pEnvironment;

	// logick� poloha cursoru
	POINT			m_ptCurrentPos;	// aktu�ln� poloha (v log. sou�adnic�ch)
	POINT			m_ptLastLogPos;	// minul� poloha v log sou�adnic�ch
	// re�ln� poloha cursoru
	REALPOS			m_rpCurrentPos;	// aktu�ln� (odpov�daj�c� - m_ptCurrentPos)
	REALPOS			m_rpBuldozrPos;	// posledn� z buldozeru

};

/////////////////////////////////////////////////////////////////////////////
// CPosEdMainView view

// Parametry pro podporu pr�ce se scrollerem
class /*AFX_EXT_CLASS*/ CEditObjsParams : public CFMntScrollEditParams
{
  DECLARE_DYNAMIC(CEditObjsParams)
public:
  CEditObjsParams();
  virtual			~CEditObjsParams();

  void	SetSelObject(const CPosObject*);
  void	ClearSelObject();
};

// Parametry pro lok�ln� podporu editace atributu
class /*AFX_EXT_CLASS*/ CEditObjsInfo : public CFMntScrollEditInfo
{
DECLARE_DYNCREATE(CEditObjsInfo)
public:
					CEditObjsInfo();
	virtual			~CEditObjsInfo();

	// TRACKER pro objekt
			// podle parametr� objektu nastav� tracker
	virtual void CreateTracker(CFMntScrollerView*,BOOL bDraw = TRUE); 
  virtual void UpdateObjectChanges(CFMntScrollerView*, BOOL bDelete, BOOL bDraw = TRUE);
  virtual void OnChangeDP(CFMntScrollerView* pScroller);
  

// Attributes
public:
	CEditObjsParams* m_pEditParams;
};



class /*AFX_EXT_CLASS*/ CPosEdMainView : public CFMntScrollerView, public IExtraEditorServer
{
	DECLARE_DYNAMIC(CPosEdMainView)

	IExtraEditorClient* _activeExEditor;
  
	SRef<IExtraEditorClient> _roadVectorEditor;

protected:
	CPosEdMainView();      

	COleDataSource* m_ClipboardDataSource; // data source for clipboard  

	// Attributes
public:
	// mapa poseidon
	CPoseidonMap*	m_pPoseidonMap; 

	// objects panel. It sets editing mode
	CPosObjectsPanel* m_pObjsPanel;

	// editace objekt� 
	CEditObjsParams	m_EditObjsParams;

	enum SelMode 
	{ // selection modes
		smNew          = 0,
		smAdd          = 1,
		smAddDontCheck = 2,
		smDel          = 3
	};  

	//CPosSelAreaObject m_BGImageArea;
	CString m_SelBGImageID;

	// Operations
public:
	// Connection/disconnection to buldozer
	void OnBuldConnect();
	void OnBuldDisconnect();

	CPosEdMainDoc* GetDocument() const 
	{
		return (CPosEdMainDoc*) m_pDocument;
	}  
  
	virtual void CancelMouseMode();

	// aktu�ln� m���tko (0,5>
	double GetCurrentViewScale() const;
	LANDHEIGHT GetCountLnStep() const;

	// nastaven� metriky
	virtual void InitialUpdateMetrics();

	// nastaven� a vykreslen� m��ky
	virtual void EnableGrid(BOOL bEnable);
	virtual void DrawGrid(CDC* pDC);

	// nastaven� styl� zobrazen�
	void OnChangeViewStyle();
	afx_msg void OnBaseStyle0();
	afx_msg void OnUpdateBaseStyle0(CCmdUI* pCmdUI);
	afx_msg void OnBaseStyle1();
	afx_msg void OnUpdateBaseStyle1(CCmdUI* pCmdUI);
	afx_msg void OnBaseStyle2();
	afx_msg void OnUpdateBaseStyle2(CCmdUI* pCmdUI);
	afx_msg void OnBaseStyle3();
	afx_msg void OnUpdateBaseStyle3(CCmdUI* pCmdUI);
	afx_msg void OnBaseStyle4();
	afx_msg void OnUpdateBaseStyle4(CCmdUI* pCmdUI);
	afx_msg void OnBaseStyle5();
	afx_msg void OnUpdateBaseStyle5(CCmdUI* pCmdUI);
	afx_msg void OnBaseStyle6();
	afx_msg void OnUpdateBaseStyle6(CCmdUI* pCmdUI);

	afx_msg void OnNaturObjcs();
	afx_msg void OnUpdateNaturObjcs(CCmdUI* pCmdUI);
	afx_msg void OnPeopleObjcs();
	afx_msg void OnUpdatePeopleObjcs(CCmdUI* pCmdUI);

	afx_msg void OnNewRoadsObjcs();
	afx_msg void OnUpdateNewRoadsObjcs(CCmdUI* pCmdUI);

	afx_msg void OnUnknowObjcs();
	afx_msg void OnUpdateUnknowObjcs(CCmdUI* pCmdUI);
	afx_msg void OnShowWoods();
	afx_msg void OnUpdateShowWoods(CCmdUI* pCmdUI);
	afx_msg void OnShowNets();
	afx_msg void OnUpdateShowNets(CCmdUI* pCmdUI);
	afx_msg void OnAreasBorder();
	afx_msg void OnUpdateAreasBorder(CCmdUI* pCmdUI);
	afx_msg void OnAreasEntire();
	afx_msg void OnUpdateAreasEntire(CCmdUI* pCmdUI);
	afx_msg void OnKeyPtBorder();
	afx_msg void OnUpdateKeyPtBorder(CCmdUI* pCmdUI);
	afx_msg void OnKeyPtEntire();
	afx_msg void OnUpdateKeyPtEntire(CCmdUI* pCmdUI);

	afx_msg void OnEnableCntLn();
	afx_msg void OnUpdateEnableCntLn(CCmdUI* pCmdUI);
	afx_msg void OnEnableGrid();
	afx_msg void OnUpdateEnableGrid(CCmdUI* pCmdUI);
	afx_msg void OnEnableScndTxtr();
	afx_msg void OnUpdateEnableScndTxtr(CCmdUI* pCmdUI);
	afx_msg void OnEnableShowSea();
	afx_msg void OnUpdateEnableShowSea(CCmdUI* pCmdUI);
	afx_msg void OnEnableShowRivers();
	afx_msg void OnUpdateEnableShowRivers(CCmdUI* pCmdUI);
	afx_msg void OnEnableShowBGImages();
	afx_msg void OnUpdateEnableShowBGImages(CCmdUI* pCmdUI);
	afx_msg void OnEnableShowShadowing();
	afx_msg void OnUpdateEnableShowShadowing(CCmdUI* pCmdUI);
	afx_msg void OnEnableShowLock();
	afx_msg void OnUpdateEnableShowLock(CCmdUI* pCmdUI);
	afx_msg void OnEditCrop();

	// podpora editace
	virtual BOOL EOSearchEditObject(CPoint ptAt);	// pr�ce s m_pEditParams
	virtual BOOL EOSelectObject();					// v�b�r objektu podle m_pEditParams
	virtual BOOL EOUnselectObject();				// deselekce objektu
	virtual BOOL EOUpdateInfo();					// �daje o objektu (vrac� p��znak zm�ny �daj�)
	virtual BOOL EOObjectFromTracker();				// objekt byl zm�n�n trackerem->p�izp�sob� se
	virtual BOOL EOEditObject();					// implementace p��m� editace (TRUE->zm�na)
	virtual BOOL EOCanEditObject();

	virtual BOOL OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult);

	void CreateObjectOnMouse(UINT nFlags, CPoint point); // Create object on mouse position  
	void CreateLandObjectOnMouse(UINT nFlags, CPoint point); 

	// STATUS bar
	afx_msg void OnUpdateStatusInfoObject(CCmdUI* pCmdUI);

	// Update rulers.
	virtual void OnScrollScaleChanged();

	// selekce objektu
	void DoSelectPosObject(CPosObject*, SelMode iSelMode = smNew, BOOL bUpdate = TRUE, BOOL bUpdateBuldozer = FALSE);
	void DoUnselectPosObject();

	void DoSelectPosObjects(CObArray&, SelMode iSelMode = smNew, BOOL bUpdateBuldozer = FALSE);
	void DoSelectPosObjsInRect(CRect& rArea, const EnabledObjects& enabled, SelMode iSelMode = smNew, BOOL bUpdateBuldozer = FALSE);
	void DoSelectPosObjsInRgn(CRgn& rArea, const EnabledObjects& enabled, SelMode iSelMode = smNew, BOOL bUpdateBuldozer = FALSE);
	void DoSelectPosObjsAll(const EnabledObjects& enabled, BOOL bUpdateBuldozer = FALSE);
	void SendSelectionToBuldozer();
  //void CollectPosEdObjsInterfereIntoArea(const CRect& area, const EnabledObjects& enabled, AutoArray<CPosEdObject *> dest);

	void GetSelObjectInAction(CObArray&, const CPosAction*) const;
	void SendMovedObjectsToBuldozer(CObArray&);

	// editace a zru�en� objektu
	BOOL CanEditObject();
	afx_msg void OnEditObject();
	afx_msg void OnUpdateEditObject(CCmdUI* pCmdUI);

	BOOL CanDeleteObject();
	afx_msg void OnDeleteObject();
	afx_msg void OnUpdateDeleteObject(CCmdUI* pCmdUI);

	// lock & unlock
	afx_msg void OnLock();
	afx_msg void OnUpdateLock(CCmdUI* pCmdUI);

	afx_msg void OnUnlock();
	afx_msg void OnUpdateUnlock(CCmdUI* pCmdUI);

	// pr�ce s kurzorem
	afx_msg void OnGotoCursorObject();
	afx_msg void OnUpdateGotoCursorObject(CCmdUI* pCmdUI);

	// pr�ce s plochou (blokem)  
	afx_msg void OnSelObjAreaRect();
	afx_msg void OnUpdateSelObjAreaRect(CCmdUI* pCmdUI);  
  
	// pr�ce s v��kou
	afx_msg void OnErozeLandscape();
	afx_msg void OnUpdateErozeLandscape(CCmdUI* pCmdUI);
	afx_msg void OnHeightLandscape();
	afx_msg void OnUpdateHeightLandscape(CCmdUI* pCmdUI);
    
	// Drawing functions
	void OnInitDrawParams(CDrawPar*);
	void OnInitDrawParamsForBk(CDrawPar*);
	virtual void OnDrawObjects(CDC* pDC);

	// vykreslen� podkladu na bodu 
	void DoDrawBkUnit(CDC* pDC, CRect&, int nX, int nZ, int nXCnt, int nZCnt, CDrawPar*);
	void DoDrawCountLn(CDC* pDC, CRect&, int nX, int nZ, int nXCnt, int nZCnt, CDrawPar*);
	void DoDrawHeightScale(CDC* pDC, CRect&, int nX, int nZ, int nXCnt, int nZCnt, float colorCoef, SScaleItem *scale, CDrawPar*);
	void DoDrawWater(CDC* pDC, CRect&, int nX, int nZ, int nXCnt, int nZCnt, CDrawPar*);
	void DoShadowBkUnit(CDC* pDC, CRect& rUnit);

	// vypln�n� ploch texturami
	afx_msg void OnFillAreaByTxtr();
	afx_msg void OnUpdateFillAreaByTxtr(CCmdUI* pCmdUI);
	afx_msg void OnFillAreaByZone();
	afx_msg void OnUpdateFillAreaByZone(CCmdUI* pCmdUI);
	afx_msg void OnFillAreaByLand();
	afx_msg void OnUpdateFillAreaByLand(CCmdUI* pCmdUI);
	afx_msg void OnFillAreaByChange();
	afx_msg void OnUpdateFillAreaByChange(CCmdUI* pCmdUI);

	afx_msg void OnToolsAnalyseTxtrs();
	afx_msg void OnUpdateToolsAnalyseTxtrs(CCmdUI* pCmdUI);

	// random placement of objects
	afx_msg void OnRandomPlacer();
	afx_msg void OnUpdateRandomPlacer(CCmdUI* pCmdUI);

	// Snap together selected objects
	afx_msg void OnSnapObjects();
	afx_msg void OnUpdateSnapObjects(CCmdUI* pCmdUI);

	// generov�n� lesu do plochy
	afx_msg void OnCreateWood();
	afx_msg void OnUpdateCreateWood(CCmdUI* pCmdUI);

	// generov�n� s�t�
	afx_msg void OnGenerateNet();
	afx_msg void OnUpdateGenerateNet(CCmdUI* pCmdUI);

	// pojmenovan� oblast
	afx_msg void OnCreateNameArea();
	afx_msg void OnUpdateCreateNameArea(CCmdUI* pCmdUI);
	afx_msg void OnGoToNameArea();
	afx_msg void OnUpdateGoToNameArea(CCmdUI* pCmdUI);
	afx_msg void OnEditNameArea();
	afx_msg void OnUpdateEditNameArea(CCmdUI* pCmdUI);
	afx_msg void OnSelectNameArea();
	afx_msg void OnUpdateSelectNameArea(CCmdUI* pCmdUI);
	afx_msg void OnSelObjNameArea();
	afx_msg void OnUpdateSelObjNameArea(CCmdUI* pCmdUI);

	// klic. plocha
	afx_msg void OnGoToKeyPoint();
	afx_msg void OnUpdateGoToKeyPoint(CCmdUI* pCmdUI);
	afx_msg void OnEditKeyPoint();
	afx_msg void OnUpdateEditKeyPoint(CCmdUI* pCmdUI);
	afx_msg void OnSelectKeyPoint();
	afx_msg void OnUpdateSelectKeyPoint(CCmdUI* pCmdUI);
	afx_msg void OnSelObjKeyPoint();
	afx_msg void OnUpdateSelObjKeyPoint(CCmdUI* pCmdUI);

	// pojmenovan� objekt
	afx_msg void OnGoToNameObject();
	afx_msg void OnUpdateGoToNameObject(CCmdUI* pCmdUI);

	// background images
	afx_msg void OnCreateBgImage();
	afx_msg void OnRemoveBgImage(); 
	afx_msg void OnEditBgImage();
	afx_msg void OnUpdateOperBgImage(CCmdUI* pCmdUI);

	// texture layer
	afx_msg void OnAddTextureLayer();
	afx_msg void OnUpdateAddTextureLayer(CCmdUI* pCmdUI);
	afx_msg void OnRemoveTextureLayer();
	afx_msg void OnUpdateRemoveTextureLayer(CCmdUI* pCmdUI);
	afx_msg void OnEditTextureLayer();
	afx_msg void OnUpdateEditTextureLayer(CCmdUI* pCmdUI);

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPosEdMainView)
public:
	virtual void OnInitialUpdate();
protected:
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL
	void OnUpdateAction(const CPosAction* pAction);
	void OnUpdateCursor(const POINT&);
	void OnUpdateSelObjectBuldozer(CPosObject* pObj);
	void OnUpdateSelEdObjectBuldozer(CPosEdObject* pObj);
	void OnUpdateSelLandBuldozer();

	// Implementation
protected:
	virtual ~CPosEdMainView();

//  void HandleRealMessage(const SPosMessage&, int);

  // Generated message map functions
protected:
	// nastaven� m���tka
	afx_msg void OnZoomIn();
	afx_msg void OnUpdateZoomIn(CCmdUI* pCmdUI);
	afx_msg void OnZoomOut();
	afx_msg void OnUpdateZoomOut(CCmdUI* pCmdUI);
	afx_msg void OnViewZoomFit();
	afx_msg void OnUpdateViewZoomFit(CCmdUI* pCmdUI);
	afx_msg void OnViewScale10();
	afx_msg void OnUpdateViewScale10(CCmdUI* pCmdUI);
	afx_msg void OnViewScale25();
	afx_msg void OnUpdateViewScale25(CCmdUI* pCmdUI);
	afx_msg void OnViewScale50();
	afx_msg void OnUpdateViewScale50(CCmdUI* pCmdUI);
	afx_msg void OnViewScale75();
	afx_msg void OnUpdateViewScale75(CCmdUI* pCmdUI);
	afx_msg void OnViewScale100();
	afx_msg void OnUpdateViewScale100(CCmdUI* pCmdUI);
	afx_msg void OnViewScale150();
	afx_msg void OnUpdateViewScale150(CCmdUI* pCmdUI);
	afx_msg void OnViewScale200();
	afx_msg void OnUpdateViewScale200(CCmdUI* pCmdUI);
	afx_msg void OnViewScale250();
	afx_msg void OnUpdateViewScale250(CCmdUI* pCmdUI);
	afx_msg void OnViewScale500();
	afx_msg void OnUpdateViewScale500(CCmdUI* pCmdUI);
	afx_msg void OnViewScaleUser();
	afx_msg void OnUpdateViewScaleUser(CCmdUI* pCmdUI);

	afx_msg void OnViewScaleSpecIn();
	afx_msg void OnViewScaleSpecOut();
	afx_msg void OnUpdateViewScaleSpec(CCmdUI* pCmdUI);

	afx_msg void OnViewAreaMoveLeft();
	afx_msg void OnViewAreaMoveRight();
	afx_msg void OnViewAreaMoveTop();
	afx_msg void OnViewAreaMoveBottom();
	afx_msg void OnUpdateViewAreaMove(CCmdUI* pCmdUI);

	//{{AFX_MSG(CPosEdMainView)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	//}}AFX_MSG

	void EraseBkgndToDC(CDC* pDC, const CRect& rClip, const CRect& rArea);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);  
	afx_msg void OnPickTexture();
	afx_msg void OnPickObject();

	afx_msg void OnMouseMove(UINT nFlags, CPoint point);  

	void ChangeSelBGImage(const CString& selBGImageID);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);

	//Clipboard
	afx_msg void OnClipboardCopy();
	afx_msg void OnClipboardPaste();
	afx_msg void OnUpdateClipboardCopy(CCmdUI* pCmdUI);
	afx_msg void OnEditSelectAll();
	afx_msg void OnEditDeselect();
	afx_msg void OnEditPasteAbsolut();
	afx_msg void OnNewRoadEditor();
	afx_msg void OnNewRoadEditorUpdate(CCmdUI* pCmdUI);

	void CursorPositionSet(const SMoveObjectPosMessData& msgData);
	afx_msg void OnProjectExportmapasimage();

	IExtraEditorClient* ActivateExEditor(IExtraEditorClient* editor);
	bool ExEditor_MouseEvent(IExtraEditorClient::MouseEvent ev, const CPoint& pt, UINT flags);

	virtual CWnd* GetExtraEditorWindow() const 
	{
		return const_cast<CWnd*>(static_cast<const CWnd*>(this));
	}

	virtual void TransformWorldToScreen(const Array<const Vector3>& inpoints, Array<CPoint>& outpoints) const;
	virtual void TransformScreenToWorld(const Array<const CPoint>& inpoints, Array<Vector3>& outpoints) const;

	virtual CPoint GetGlobalMsPosition(const CPoint& pt) const
	{
		return WindowToScrollerPosition(pt);
	}

	virtual void PrepareDC(CDC* pDC) const
	{
		const_cast<CPosEdMainView*>(this)->OnPrepareDC(pDC);
	}
};

/////////////////////////////////////////////////////////////////////////////
// Akce dokumentu (MainActions.cpp)

// CPosObjectAction
class CPosObjectAction : public CPosAction
{
DECLARE_DYNAMIC(CPosObjectAction)
public:
	CPosObjectAction(int nType);
	~CPosObjectAction();

			// v�m�na hodnot m_CurValue a m_OldValue
	virtual BOOL ChangeValues();

// data
public:
	CPosObject*		m_pCurValue;
	CPosObject*		m_pOldValue;

	// selekce objektu
	enum {
		ida_SelectNone  = 0,
		ida_SelectExclu = 1,
		ida_SelectGroup = 2,
	};
	int				m_nSelectObjType;
};

// CPosObjectGroupAction
class CPosObjectGroupAction : public CPosActionGroup
{
DECLARE_DYNAMIC(CPosObjectGroupAction)
public:
	CPosObjectGroupAction(int nType);

					// proveden� akce v dokumentu
	virtual BOOL	RealizeAction(CDocument*);			
};

// CPosTextureAction
class CPosTextureAction : public CPosAction
{
DECLARE_DYNAMIC(CPosTextureAction)
public:
	CPosTextureAction(int nType,UNITPOS);
	~CPosTextureAction();

			// v�m�na hodnot m_CurValue a m_OldValue
	virtual BOOL ChangeValues();

// data
public:
  CString m_TextureLayerName;
	int	m_pCurValue;
	int	m_pOldValue;
	UNITPOS					m_Position;

	// aktualizovat sekund�rn� texturu ?
	BOOL					m_bUpdateTxtr;
};

// CPosTxtrAreaAction
/*class  CPosTxtrAreaAction : public CPosActionGroup
{
DECLARE_DYNAMIC(CPosTxtrAreaAction)
public:
	CPosTxtrAreaAction(int nType,const CPositionArea&);
	~CPosTxtrAreaAction();

					// proveden� akce v dokumentu
	virtual BOOL	RealizeAction(CDocument*);			
	
// data
public:
	CPositionArea	m_Position;
	BOOL			m_bUpdateTxtr;
};*/

class CPosTxtrAreaActionBase: public CPosAction
{
  DECLARE_DYNAMIC(CPosTxtrAreaActionBase)
public:
  CPosTxtrAreaActionBase(int nType,const CPositionArea&);
  virtual ~CPosTxtrAreaActionBase() {};
  
  virtual void SetInMap(CPoseidonMap&) = 0;

  // data
public:
  CString m_TextureLayerName;

  CPositionArea	m_Position;  
  BOOL			    m_bUpdateTxtr;

  UNITPOS			m_LftTop;
  UNITPOS			m_RghBtm;
  int				  m_nDimX;
  int				  m_nDimZ;
};

class CPosTxtrAreaAction : public CPosTxtrAreaActionBase
{
  DECLARE_DYNAMIC(CPosTxtrAreaAction)
public:
  CPosTxtrAreaAction(int nType,const CPositionArea&);
  virtual ~CPosTxtrAreaAction();

  //BOOL CreateActionData(const CPoseidonMap*, int newTextID);
  virtual void SetInMap(CPoseidonMap&);
  virtual BOOL ChangeValues();

protected:
  void SetUnitTexture(int * data, int x, int z, int nTextID) const {data[x - m_LftTop.x + (z - m_RghBtm.z) * m_nDimX] =  nTextID;};
  int GetUnitTexture(int * data, int x, int z) const {return data[x - m_LftTop.x + (z - m_RghBtm.z) * m_nDimX];};

  // data
public:  
  //BOOL        m_bUndo; // if true take m_pOldValue as new IDs else take m_pCurValue

  int *       m_pCurValue;
  int *       m_pOldValue;
};

class CPosZoneTxtrAreaAction : public CPosTxtrAreaAction
{
  DECLARE_DYNAMIC(CPosZoneTxtrAreaAction)
public:
  CPosZoneTxtrAreaAction(int nType,const CPositionArea&);
  virtual ~CPosZoneTxtrAreaAction() {};

  BOOL CreateActionData(const CPoseidonMap*, const CPosTextureZone*);
};

#define TEXTURE_UNDER_SEA 0x01
#define TEXTURE_GROUND 0x02
#define TEXTURE_RANDOM 0x04

class CPosLandTxtrAreaAction : public CPosTxtrAreaAction
{
  DECLARE_DYNAMIC(CPosLandTxtrAreaAction)

public:
  CPosLandTxtrAreaAction(int nType,const CPositionArea&);
  virtual ~CPosLandTxtrAreaAction() {};

  BOOL CreateActionData(const CPoseidonMap*, const CPosTextureLand*, WORD flags);
};

class CPosTxtrAreaAction2 : public CPosTxtrAreaActionBase
{
  DECLARE_DYNAMIC(CPosTxtrAreaAction2)
public:
  CPosTxtrAreaAction2(int nType,const CPositionArea&);
  virtual ~CPosTxtrAreaAction2();
 
  BOOL CreateActionData(const CPoseidonMap*, int newTextID);
  virtual void SetInMap(CPoseidonMap&);
  
  virtual BOOL ChangeValues();

private:
  void SetUnitTexture(int x, int z, int nTextID) const;
  int GetUnitTexture(int x, int z) const {return m_pOldValue[x - m_LftTop.x + (z - m_RghBtm.z) * m_nDimX];};

 
  // data
public:  
  BOOL        m_bUndo; // if true take m_pOldValue as new IDs else take m_pCurValue

  int         m_pCurValue;
  int *       m_pOldValue;
};

class CPosChngTxtrAreaAction : public CPosTxtrAreaActionBase
{
  DECLARE_DYNAMIC(CPosChngTxtrAreaAction)
public:
  CPosChngTxtrAreaAction(int nType,const CPositionArea&);
  virtual ~CPosChngTxtrAreaAction();

  BOOL CreateActionData(const CPoseidonMap*, int oldTextID, int newTextID);
  virtual void SetInMap(CPoseidonMap&); 		

  virtual BOOL ChangeValues();

private:
  void SetUnitTexture(int x, int z, int nTextID) const;
  int GetUnitTexture(int x, int z) const {return m_pOldValue[x - m_LftTop.x + (z - m_RghBtm.z) * m_nDimX];};


  // data
public:
  BOOL        m_bUndo; // if true take m_pOldValue as new IDs else take m_pCurValue

  int         m_oldTextID;
  int         m_newTextID;

  int *       m_pOldValue;
};

class CPosClipboardTxtrAreaAction : public CPosTxtrAreaActionBase
{
  DECLARE_DYNAMIC(CPosClipboardTxtrAreaAction)
public:
  CPosClipboardTxtrAreaAction(int nType,const CPositionArea&);
  virtual ~CPosClipboardTxtrAreaAction();

  BOOL CreateActionData(const CPoseidonMap*, PosClipBoardTexture *, const REALPOS& );
  virtual void SetInMap(CPoseidonMap&);

  virtual BOOL ChangeValues();

private:
  void SetUnitTexture(int x, int z, int nTextID) const;
  int GetUnitTexture(int x, int z) const {return m_pOldValue[x - m_LftTop.x + (z - m_RghBtm.z) * m_nDimX];};
public:
  BOOL        m_bUndo; // if true take m_pOldValue as new IDs else take m_pCurValue

  int *           m_pOldValue;
  TextureElem *   m_pCurValue;

  int m_numberOfElems;
};

// Action for locking/unlocking textures
class CPosTxtrAreaLockAction : public CPosTxtrAreaActionBase
{
  DECLARE_DYNAMIC(CPosTxtrAreaLockAction)
public:
  CPosTxtrAreaLockAction(int nType,const CPositionArea& area) : CPosTxtrAreaActionBase(nType, area) {};
  virtual ~CPosTxtrAreaLockAction() {};

  virtual BOOL ChangeValues() {_lock = !_lock; return TRUE;};
  virtual void SetInMap(CPoseidonMap&);

  BOOL CreateActionData(const CPoseidonMap&, bool lock = true);

protected:
  bool _lock;
};


// Zm�na v��ky v bloku
class CPosHeightAction : public CPosAction
{
DECLARE_DYNAMIC(CPosHeightAction)
public:
  CPosHeightAction(int nType,const CPositionArea&);
  ~CPosHeightAction();

  // v�m�na hodnot m_CurValue a m_OldValue
  virtual BOOL ChangeValues();

  // podpora pro pr�ci s blokem v��ek
  BOOL CreateActionData(const CPoseidonMap*);

  // nastaven� v��ky - m_pOldValue
  LANDHEIGHT GetUnitLandHeight(int x, int z) const;
  void SetUnitLandHeight(int x, int z, LANDHEIGHT) const;

// data
public:
	CPositionArea	m_Position;
	// obal polohy
	UNITPOS			m_LftTop;
	UNITPOS			m_RghBtm;
	int				m_nDimX;
	int				m_nDimZ;
	// data
	LANDHEIGHT*		m_pCurValue;
	LANDHEIGHT*		m_pOldValue;
};

class CPosHeightLockAction : public CPosTxtrAreaActionBase
{
  DECLARE_DYNAMIC(CPosHeightLockAction)
public:
  CPosHeightLockAction(int nType,const CPositionArea& area) : CPosTxtrAreaActionBase(nType, area) {};
  virtual ~CPosHeightLockAction() {};

  virtual BOOL ChangeValues() {_lock = !_lock; return TRUE;};
  virtual void SetInMap(CPoseidonMap&);

  BOOL CreateActionData(const CPoseidonMap&, bool lock = true);

protected:
  bool _lock;
};


class CPosTextureLayerAction : public CPosAction
{
DECLARE_DYNAMIC(CPosTextureLayerAction)
public:
  CPosTextureLayerAction(int nType);
  ~CPosTextureLayerAction();

  // v�m�na hodnot m_CurValue a m_OldValue
  virtual BOOL ChangeValues();

  void SetDataIntoLayer(Ref<CTextureLayer> layer);
  bool CreateOldData(Ref<CTextureLayer> layer);

public: 
  CString m_OldName;
  int m_OldTextureSizeInSqLog;
  int * m_pOldBaseTextures;
  
  CString m_CurrName;
  int m_CurrTextureSizeInSqLog;
  bool m_bUndo;
};


/////////////////////////////////////////////////////////////////////////////
// CPosEdMainDoc - hlavn� dokument projektu

#define WM_RENAME_DOC (WM_APP + 1)

class /*AFX_EXT_CLASS*/ CPosEdMainDoc : public CPosEdBaseDoc
{
	DECLARE_DYNAMIC(CPosEdMainDoc)
	typedef CPosEdBaseDoc base;
protected:
	CPosEdMainDoc();           

	// Attributes
public:
	// mapa poseidon
	CPoseidonMap m_PoseidonMap;
	CString      m_sLastImportFile;

	// kurzor Buldozeru
	CPosEdCursor m_CursorObject;

  //CPosVertexAreaObject		m_VertexSelectionArea;

	// vybran� objekty
	CPosGroupObject m_GroupObjects;

	// panel pro praci s objekty
	CPosObjectsPanel* m_pObjsPanel;

	// Selected rectangles
	CPosSelAreaObject m_CurrentArea;

	// Named selection
	VisSelectionList m_SelectionList;

	// Operations
public:
	// connect/disconnect buldozer
	virtual void OnBuldConnect(IVisViewerExchange* realViewer);
	virtual void OnBuldDisconnect();

	virtual void DoSerialize(CArchive& ar, int nVer);

	// Overrides
//  afx_msg void OnMakeDocReal(IVisViewerExchange *realViewer);	

	afx_msg void OnUpdateBuldozer();
	afx_msg void OnUpdateUpdateBuldozer(CCmdUI* pCmdUI);

	afx_msg void OnImportRealDoc();
	afx_msg void OnUpdateImportRealDoc(CCmdUI* pCmdUI);
	afx_msg void OnExportRealDoc();
	afx_msg void OnUpdateExportRealDoc(CCmdUI* pCmdUI);

	// spr�va textur a objekt� 
	afx_msg void OnObjectsManagement();
	afx_msg void OnObjectsManageChange();
	afx_msg void OnUpdateObjectsManagement(CCmdUI* pCmdUI);

	// STATUS bar
	afx_msg void OnUpdateStatusInfoCurpos(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStatusInfoHeight(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStatusInfoPrimtxtr(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStatusInfoScndtxtr(CCmdUI* pCmdUI);

	// pr�ce s animac�
	afx_msg void OnAnimationNew();
	afx_msg void OnAnimationOpen();
	afx_msg void OnAnimationSave();
	afx_msg void OnUpdateAnimationSave(CCmdUI* pCmdUI);
	afx_msg void OnAnimationSaveAs();
	afx_msg void OnUpdateAnimationSaveAs(CCmdUI* pCmdUI);
	afx_msg void OnAnimationClose();
	afx_msg void OnUpdateAnimationClose(CCmdUI* pCmdUI);

	afx_msg void OnAnimationParams();
	afx_msg void OnUpdateAnimationParams(CCmdUI* pCmdUI);
	afx_msg void OnAnimationShot();
	afx_msg void OnUpdateAnimationShot(CCmdUI* pCmdUI);
	afx_msg void OnAnimationGener();
	afx_msg void OnUpdateAnimationGener(CCmdUI* pCmdUI);

	// podpora pr�ce s animac�
	BOOL IsAnimationActive() const;
	BOOL CloseCurrentAnimation();
	BOOL SaveCurrentAnimation(LPCSTR pFile);

	// zpracov�n� zpr�v z preview
//  void InvalidRealMessage(const SPosMessage& sMsg) const;
//  virtual void HandleRealMessage(const SPosMessage&,int nMode);

	// obnova panelu
	void UpdateDocKPlaObjects();
	void UpdateDocNameObjects();
	void UpdateDocNObjObjects();
	void UpdateDocNObjObject(CPosEdObject*, CPosEdObject*);	
	void UpdateAnimationShots();
	void ActualizeAnimationShot();

	// change of square size
	bool ChangeRealSizeUnit(float newRealSizeUnit, bool recalcLandHeight = false);

	/// change satellite grid
	bool ChangeSatGrid(int newSatGrid);

	virtual CTextureLayer* GetActiveTextureLayer()
	{
		return m_PoseidonMap.GetActiveTextureLayer();
	}

	// pr�ce se spr�vcem akc�
	CPosObject*  GetDocumentObjectFromValue(const CPosObject*);

	CPosEdObject* CreateObjectFromObjectValue(CPosEdObject* pValue);
	void DeleteObjectFromObjectValue(CPosEdObject* pValue);

	CPosWoodObject* CreateWoodFromWoodValue(CPosWoodObject* pValue);
	void DeleteWoodFromWoodValue(CPosWoodObject* pValue);

	CPosNetObjectBase* CreateNetFromNetValue(CPosNetObjectBase* pValue);
	void DeleteNetFromNetValue(CPosNetObjectBase* pValue);

	CPosNameAreaObject* CreateNameAreaFromNameAreaValue(CPosNameAreaObject* pValue);
	void DeleteNameAreaFromNameAreaValue(CPosNameAreaObject* pValue);

	CPosKeyPointObject* CreateKeyPointFromKeyPointValue(CPosKeyPointObject* pValue);
	void DeleteKeyPointFromKeyPointValue(CPosKeyPointObject* pValue);

	CPosBackgroundImageObject* CreateBGImageFromBGImageValue(CPosBackgroundImageObject* pValue);
	void DeleteBGImageFromBGImageValue(CPosBackgroundImageObject* pValue);  

	virtual BOOL RealizeEditAction(CPosAction*);			
	CPosAction* GenerNewObjectAction(int nType, CPosObject* pDocObj, CPosObject* pNewObj,
									 bool bFromBuldozer = false, CPosActionGroup* pToGroup = NULL, 
									 bool select = true);

	// Align current area to mach correct grid
	void AlignCurrentArea();

	CPosSelAreaObject& GetSelectedArea() 
	{
		return m_CurrentArea;
	}

	const CPosGroupObject& GetSelectedObjects() const 
	{
		return m_GroupObjects;
	}

	void ClearObjectSelection();
	void AddToObjectSelection(CPosObject* obj, bool exclusive);
	void UpdateAllViewsSelection();

	///(Re)defines (new) named selection
	/**
	* @param selName name of existing or new selection. If selection not exists, creates new one
	* @param source Source of selection, if NULL, current selection is used;
	*/
	void DefineNamedSelection(const RStringI& selName, const VisSelection* source = 0);

	///Uses named selection. 
	/**
	* @param  selName name of selection to use
	* @param ctrl function for ctrl in effect
	* @param shift function for shift in effect
	* @note Normally, current selection is replaced. When ctrl is true and shift is false,
	* current selection if combined with new selection. When both ctrl and shift is true,
	* new selection defines items that should be removed from current selection. When
	* ctrl is false and shift is true, only items selected in both selection remains.
	*/
	bool UseNamedSelection(const RStringI& selName, bool ctrl, bool shift)
	{
		const VisSelection* sel = GetNamedSel(selName);
		if (sel) 
		{
			UseSelection(*sel, ctrl, shift);
			return true;
		}
		else 
		{
			return false;
		}
	}

	///Uses named selection. 
	/**
	* @param sel named selection object
	* @param ctrl function for ctrl in effect
	* @param shift function for shift in effect
	* @note Normally, current selection is replaced. When ctrl is true and shift is false,
	* current selection if combined with new selection. When both ctrl and shift is true,
	* new selection defines items that should be removed from current selection. When
	* ctrl is false and shift is true, only items selected in both selection remains.
	*/
	void UseSelection(const VisSelection& sel, bool ctrl, bool shift);

	///returns selection for given name
	const VisSelection* GetNamedSel(const RStringI& name) const 
	{
		return m_SelectionList.GetSelection(name);
	}

	VisSelection* GetNamedSel(const RStringI& name) 
	{
		return m_SelectionList.GetSelection(name);
	}

	///removes selection of given name
	bool DeleteSelection(const RStringI& name);

	///Processes all selection in the selection list.
	template<class Functor>
	int ForEachNamedSel(const Functor& functor) const
	{
		return m_SelectionList.ForEach(functor);
	}

	///returns total count of selections
	int GetSelectionCount() const;  
  
	///removes selection
	void RenameSelection(const RStringI& oldName, const RStringI& newName);

	void DefineNamedSelectionAction(const RStringI& selName, const VisSelection* source = 0);
	void DeleteSelectionAction(const RStringI& selName);
	bool RenameSelectionAction(const RStringI& oldName, const RStringI& newName);

	void HideObjects(bool selected = true, bool hide = true);
	void InvertSelection();
  
	///Viewer actions:

	void SetLandTextureAt(int x, int z, int id, BOOL bFromBuldozer);
	void SetLandHeight(int x, int z, float height);
	void SetBlockLandHeight(const Array<STexturePosMessData>& data);
	void MoveObject(const SMoveObjectPosMessData& msgData);
	void BlockMoveObject(const Array<SMoveObjectPosMessData >& data);
	void SelectionObjectAdd(const SObjectPosMessData& data);
	void SelectionObjectClear();
	void OnCursorPositionSet(const SMoveObjectPosMessData& data);
	void BlockSelectionObject(const Array<SMoveObjectPosMessData>& msgData);
	void SelectionLandAdd(const SLandSelPosMessData& msgData);
	void SelectionLandClear();
	void BlockSelectionLand(const Array<SLandSelPosMessData>& msgData);

	void CropLand(const CRect& rc);
  
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPosEdMainDoc)
public:
	virtual void OnCloseDocument();
protected:
	virtual BOOL OnNewDocument();
	virtual void DeleteContents();
  
	//}}AFX_VIRTUAL

	// Implementation
public:
	virtual ~CPosEdMainDoc();

	// Generated message map functions
protected:
	//{{AFX_MSG(CPosEdMainDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
  
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
//  afx_msg void OnSccIntegCheckOut();
//  afx_msg void OnUpdateSccIntegCheckOut(CCmdUI* pCmdUI);
};

/////////////////////////////////////////////////////////////////////////////
// UpdatePar for view updating
class CPosMessageUpdate : public CFMntScrollUpdate
{
protected:
  const SPosMessage& _msg;
  int _nMode;

public:
  enum {
    uvfPosMessage = 0x00030001
  };

  CPosMessageUpdate(const SPosMessage& msg, int nMode) : _msg(msg), _nMode(nMode),
    CFMntScrollUpdate(uvfPosMessage, NULL) {};

  const SPosMessage& Message() const {return _msg;};
  int Mode() const {return _nMode;};
};


/////////////////////////////////////////////////////////////////////////////
//	CPosObjectsPanel	(ObjectsBar.cpp)

class CPosObjectsPanel : public CDialogBar
{
	// Construction
public:
	CPosObjectsPanel();

	BOOL CreatePanel(CWnd* pParentWnd,UINT nID);

	// Sizes for resizing dialogbar
	CSize m_sizeDocked;
	CSize m_sizeFloating;

	// dialog - controls
	CVariantComboBox	m_cTxtrLand;	// typy krajiny
	CVariantComboBox	m_cTxtrZone;	// veget. p�smo
	CVariantListBox		m_cTextures;	// textury

	CVariantListBox		m_cWoods;		// lesy
	CVariantListBox		m_cNets;		// s�t�

	CFMntMenuListBox    m_cTextureLayers; 
	CListBox			m_cNameAreas;	// pojmenovan� plochy
	CListBox			m_cKeyPoints;	// kl��ov� body
	CListBox			m_cNameObjcs;	// pojmenovan� objekty

	CVariantListBox		m_cNatObjs;		// p��rodn� objekty
	CVariantListBox		m_cPplObjs;		// lidsk� objekty

	CVariantListBox		m_cNewRoadsObjs;		

	CListBox            m_cBgImages;  // Images at background

	// current and last type
	int m_iPreviousType;
	int m_iCurrentType;

	// Operations
public:
	CPosEdMainDoc*		m_pCurrentDoc;

#ifdef __ONLY_FOR_PUBLIC__
	enum 
	{
		grTypePlOb    =  0, 
		grTypeNtOb    =  1, 
		grTypeNets    =  2, 
		grTypeKPla    =  3, 
		grTypeLand    =  4, 
		grTypeBgImage =  5,
		grTypeName    =  6, 
		grTypeNObj    =  7, 
		grTypeTxtr    =  8, 
		grTypeWood    =  9 
	};
#else
	enum 
	{
		grTypePlOb    =  0, //0,
		grTypeNtOb    =  1, //1,
		grTypeNROb    =  2,
		grTypeNets    =  3, //2,
		grTypeKPla    =  4, //3,
		grTypeLand    =  5, //4,
		grTypeBgImage =  6, //5,
		grTypeName    =  7, //6,
		grTypeNObj    =  8, //7,
		grTypeTxtr    =  9, //8,
		grTypeWood    = 10  //9,
	};
#endif

	int GetPreviousTypeObjects() const 
	{
		return m_iPreviousType;
	}

	int GetTypeObjects() const;
	void SetTypeObjects(int nType);

	// Overrides
	void UpdateDocKPlaObjects();
	void UpdateDocNameObjects();

	void UpdateDocNObjObjects();
	void UpdateDocNObjObject(CPosEdObject*, CPosEdObject*);	

	void UpdateTextures() 
	{ 
		m_cTextures.Invalidate();
	}

	void UpdateDocBgImages();
	void UpdateDocTxtrLayers();

	void UpdateDocData(CPosEdMainDoc*, BOOL bAlways = FALSE);
	void OnChangeTypeObjects();

	// aktu�ln� objekty pro vlo�en�
	CPosTextureTemplate* GetCurrentTexture();
	void SetCurrentTexture(CPosTextureTemplate* text);
	CPosObjectTemplate*  GetCurrentObject();
	void SetCurrentObject(const CPosObjectTemplate* temp);
	CPosWoodTemplate*	 GetCurrentWood();
	void SetCurrentWood(CPosWoodTemplate* temp);
	CPosNetTemplate*	 GetCurrentNet();
	void SetCurrentNet(const CString& name);

	// textury pro vypln�n� plochy
	CPosTextureTemplate* GetFillTextureTxtr();
	CPosTextureZone*	 GetFillTextureZone();
	CPosTextureLand*	 GetFillTextureLand();

	// pojmen. plochy a klic. body
	CPosNameAreaObject*	 GetCurrentNameArea();
	void SetCurrentNameArea(CPosNameAreaObject* keyPoint);
	CPosKeyPointObject*  GetCurrentKeyPoint();
	void SetCurrentKeyPoint(CPosKeyPointObject* keyPoint);
	CPosEdObject*		 GetCurrentNameObjc();

	void SetCurrentBGImage(const CString& bgImageID);
	CPosBackgroundImageObject* GetCurrentBGImage();

	CTextureLayer* GetCurrentTextureLayer();

	virtual CSize CalcDynamicLayout(int nLength, DWORD dwMode);
	virtual CSize CalcDynamicDim(int nLength, DWORD dwMode);
	void RecalcChildWindowPos(const CSize& size);

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEbisAttribPanel)
	//}}AFX_VIRTUAL
protected:
	// Generated message map functions
	//{{AFX_MSG(CEbisAttribPanel)
	afx_msg void OnSelchangeListLand();
	afx_msg void OnSelchangeListZone();
	afx_msg void OnDblSelectListNaAr();
	afx_msg void OnDblSelectListKePt();
	afx_msg void OnDblSelectListNmOb();
	afx_msg void OnDblSelectTextureLayers();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
//  afx_msg void OnSizing(UINT fwSide, LPRECT pRect);
//  afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};

/////////////////////////////////////////////////////////////////////////////
//	Akce s objekty

// vytvo�en� akce pro vlo�en� objektu
CPosAction* Action_CreatePoseidonObject(CPoint ptLog, CPosObjectTemplate* pTempl, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);
CPosAction* Action_CreatePoseidonObjectMulti(CPoint ptLog, CPosObjectTemplate* pTempl, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);
CPosAction* Action_CreatePoseidonObjects(CPosGroupObject*, CPosEdMainDoc*, CPosActionGroup*);

CPosAction* Action_InsertPoseidonObject(CPosEdObject* pToInsert, CPosObjectTemplate* pTempl, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);

// vytvo�en� akce pro smaz�n� objektu
CPosAction* Action_DeletePoseidonObject(CPosEdObject* pToDelete, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);

// vytvo�en� akce pro p�esun objektu
//CPosAction* Action_MovePoseidonObject(CPosEdObject* pNewPosObj, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);
CPosAction* Action_EditPoseidonObject(CPosEdObject* pNewPosObj, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);
CPosAction* Action_SetPosRealPoseidonObject(CPosEdObject* pNewPosObj, const REALPOS& pos, CPosActionGroup* pToGroup = NULL);

// vytvo�en� akce pro p�esun objektu
CPosAction* Action_MovePoseidonObject(CPosEdObject* pNewPosObj, int xAmount, int yAmount, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);
CPosAction* Action_RotatePoseidonObject(CPosGroupObject* pGroup, float grad, CPointVal origin,  CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);
CPosAction* Action_RotateMovePoseidonObject(CPosEdObject* pNewPosObj, CPointVal offset, float grad, CPointVal origin, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);

// simply rotate
CPosAction* Action_RotatePoseidonObject(CPosEdObject* obj, float grad, CPosActionGroup* pToGroup = NULL);

// change Relative Height
CPosAction* Action_SetRelHeightPoseidonObject(CPosEdObject* obj, float relHeight, CPosActionGroup* pToGroup = NULL);

// skupiny objekt�
CPosAction* Action_DeletePoseidonObjects(CPosGroupObject*, CPosEdMainDoc*, CPosActionGroup*);
//CPosAction* Action_MovePoseidonObjects(CPosGroupObject*, int xAmount, int yAmount, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);
//CPosAction* Action_RotatePoseidonObjects(CPosGroupObject* pGroup, float grad, CPointVal origin, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);
CPosAction* Action_RotateMovePoseidonObjects(CPosGroupObject*, CPointVal offset, float grad, CPointVal origin, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);


CPosAction* Action_PasteClipboardObject(PosClipBoardObject* clipData, CPosEdMainDoc* pDocument, const CPoint& cursorLog, CPosActionGroup* pToGroup = NULL);
CPosAction* Action_PasteAbsolutClipboardObject(PosClipBoardObject* clipData, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup = NULL);

// random placement
CPosAction* Action_RandomPlacementObject(CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup = NULL);

// Snap objects
CPosAction* Action_SnapObjects(CPosGroupObject& objGrp, CPosEdMainDoc& doc, CPosActionGroup* pToGroup = NULL);

// Lock/Unlock
CPosAction* Action_LockPoseidonObject(CPosObject* obj, bool lock, CPosEdMainDoc& doc, CPosActionGroup* toGrp = NULL);
CPosAction* Action_LockPoseidonObjects(CPosGroupObject* grp, bool lock, CPosEdMainDoc& doc, CPosActionGroup* toGrp = NULL);

/////////////////////////////////////////////////////////////////////////////
//	Akce s texturami

// vytvo�en� akce pro zm�nu textury
CPosAction* Action_CreatePoseidonTexture(UNITPOS ptUn, const CPosTextureTemplate* pTempl, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);
CPosAction* Action_CreatePoseidonTexture(CPoint ptLog, const CPosTextureTemplate* pTempl, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);

// vytvo�en� akce pro texturov�n� plochy - texturou
CPosAction* Action_TexturePoseidonTxtr2(const CPositionArea&, const CPosTextureTemplate* pSource, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);

// vytvo�en� akce pro texturov�n� plochy - p�smem
CPosAction* Action_TexturePoseidonZone(const CPositionArea&, const CPosTextureZone* pSource, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);

// vytvo�en� akce pro texturov�n� plochy - krajinou
CPosAction* Action_TexturePoseidonLand(const CPositionArea&,const CPosTextureLand* pSource, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);

// vytvo�en� akce pro texturov�n� plochy - krajinou
CPosAction* Action_TexturePoseidonChng(const CPositionArea&,const CPosTextureTemplate* pSource, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup);

CPosAction* Action_PasteClipboardTexture(PosClipBoardTexture* clipData, CPosEdMainDoc* pDocument, const CPoint& cursorLog, CPosActionGroup* pToGroup = NULL);
CPosAction* Action_PasteAbsolutClipboardTexture(PosClipBoardTexture* clipData, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup = NULL);

// Lock/ unlock
CPosAction* Action_TextureLock(const CPositionArea& Area, bool lock, CPosEdMainDoc& doc,CPosActionGroup * pToGroup = NULL);

/////////////////////////////////////////////////////////////////////////////
//	Akce s lesy

// vytvo�en� akce pro vytvo�en� lesa
CPosAction* Action_CreatePoseidonWood(CPosWoodTemplate* pTempl,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

// vytvo�en� akce pro smaz�n� lesa
CPosAction* Action_DeletePoseidonWood(CPosWoodObject* pToDel,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

// vytvo�en� akce pro p�esun lesa
CPosAction* Action_MovePoseidonWood(CPosWoodObject* pWood,int xAmount,int yAmount,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);
CPosAction* Action_RotatePoseidonWood(CPosWoodObject* pWood,float grad,CPointVal origin,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);
CPosAction* Action_RotateMovePoseidonWood(CPosWoodObject* pWood,CPointVal offset, float grad,CPointVal origin,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

// vytvo�en� akce pro dekompozici lesa
CPosAction* Action_DekompPoseidonWood(CPosWoodObject* pWood,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

/////////////////////////////////////////////////////////////////////////////
//	Akce se s�t�mi

// vytvo�en� akce pro vytvo�en� kl��ov�ho bodu
CPosAction* Action_CreatePoseidonKeyNet(CPoint ptLog,CPosNetTemplate* pTempl,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);
CPosAction* Action_CreatePoseidonNextKeyNet(REALNETPOS ptNext,CPosNetTemplate* pTempl,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

// akce pro vytvo�en� �seku s�t�
CPosAction* Action_CreatePoseidonNorNet(CPosNetObjectNor*,CPosNetTemplate* pTempl,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

// vytvo�en� akce pro smaz�n� �seku s�t�
CPosAction* Action_DeletePoseidonNet(CPosNetObjectBase* pToDel,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

// vytvo�en� akce pro dekompozici s�t�
CPosAction* Action_DekompPoseidonNet(CPosNetObjectBase* pNet,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

// vytvo�en� akce pro p�esun sit
CPosAction* Action_MovePoseidonNet(CPosNetObjectBase* pNet,int xAmount,int yAmount,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);
CPosAction* Action_RotatePoseidonNet(CPosNetObjectBase* pNet,float grad, CPointVal origin,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);
CPosAction* Action_RotateMovePoseidonNet(CPosNetObjectBase* pNet,CPointVal offset, float grad, CPointVal origin,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

CPosAction* Action_SetPosRealNet(CPosNetObjectBase* pNewPosObj,const REALPOS& pos, CPosActionGroup* pToGroup = NULL);
CPosAction* Action_RotateNet(CPosNetObjectBase* pNet,int grad, CPosActionGroup* pToGroup = NULL);

// editace �seku
CPosAction* Action_EditPoseidonNetNor(CPosNetObjectNor* pNet,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

// editace kl��ov�ho bodu
CPosAction* Action_EditPoseidonNetKey(CPosNetObjectKey* pNet,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup,BOOL bEdit);

// update poseidn objekt�
BOOL DoUpdateNetPoseidonObjects(CPosNetObjectBase* pNet,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

// generov�n� s�t� mezi �seky - parametry & v�b�r �sek�
BOOL DoPreGenerateNetBetween(CPosNetObjectKey* pFrom,CPosNetObjectKey* pTo,CPosEdMainDoc* pDocument);

/////////////////////////////////////////////////////////////////////////////
//	Akce s plochami

// vytvo�en� akce pro increment aktu�ln� plochy
CPosAction* Action_AddPoseidonCurrArea(CRect& rLogArea,BOOL bExlusive, BOOL bRemove, CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup = NULL, BOOL bFromBuldzer = FALSE);

// vytvo�en� akce pro increment aktu�ln� plochy
CPosAction* Action_AddPoseidonCurrArea(CRectArray& rLogArea,BOOL bExlusive, BOOL bRemove, CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup = NULL, BOOL bFromBuldzer = FALSE);

// vytvo�en� akce pro reset aktu�ln� plochy
CPosAction* Action_DelPoseidonCurrArea(CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup = NULL, BOOL bFromBuldzer = FALSE);

// vytvo�en� akce pro reset aktu�ln� plochy
CPosAction* Action_SetPoseidonCurrArea(const CPosAreaObject& area, CPosEdMainDoc * pDocument, CPosActionGroup* pToGroup = NULL, BOOL bFromBuldzer = FALSE);

// vytvo�en� akce pro selekci pod objektem
CPosAction* Action_SelPoseidonCurrArea(const CPositionBase*,BOOL bExlusive,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);
CPosAction* Action_SelPoseidonCurrArea(CPosObject*,BOOL bExlusive,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);


/////////////////////////////////////////////////////////////////////////////
//	Akce s v��kami

// vytvo�en� akce pro Erozi krajiny
CPosAction* Action_ErozePoseidonArea(const CPositionArea&,CPosEdMainDoc* pDocument);
CPosAction* Action_ErozePoseidonLand(CRect& rLogArea,CPosEdMainDoc* pDocument);

CPosAction* Action_HeightPoseidonLand(const CPositionArea&,CPosEdMainDoc* pDocument);
CPosAction* Action_HeightPoseidonLand(const AutoArray<float>&,CPosEdMainDoc* pDocument);

// kop�rov�n� relliefu
CPosAction*  Action_PasteClipboardHeight(PosClipBoardHeight * clipData,CPosEdMainDoc* pDocument,const CPoint& cursorLog, CPosActionGroup* pToGroup = NULL);
CPosAction*  Action_PasteAbsolutClipboardHeight(PosClipBoardHeight * clipData,CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup = NULL);

// lock / unlock
CPosAction*  Action_LockHeight(const CPositionArea& Area,bool lock, CPosEdMainDoc& doc,CPosActionGroup* toGroup = NULL);

/////////////////////////////////////////////////////////////////////////////
// Akce s pojmenovan�mi plochami

BOOL  DoEditNameArea(CPosNameAreaObject* pArea,BOOL bNew,BOOL* bClearSel = NULL);

// vytvo�en� pojmenovan� plochy
CPosAction* Action_CreatePoseidonNameArea(CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

// smaz�n� pojmenovan� plochy
CPosAction* Action_DeletePoseidonNameArea(CPosNameAreaObject* pArea,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

// p�esun pojmenovan� plochy
CPosAction* Action_MovePoseidonNameArea(CPosNameAreaObject* pArea,int xAmount,int yAmount,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);
CPosAction* Action_RotatePoseidonNameArea(CPosNameAreaObject* pArea,float grad,CPointVal origin,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);
CPosAction* Action_RotateMovePoseidonNameArea(CPosNameAreaObject* pArea,CPointVal offset, float grad,CPointVal origin,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);
// editace pojmenovan� plochy
CPosAction* Action_EditPoseidonNameArea(CPosNameAreaObject* pArea,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);


/////////////////////////////////////////////////////////////////////////////
// Akce s kl��ov�mi body

BOOL  DoEditKeyPoint(CPosKeyPointObject* pKeyPt,BOOL bNew);

// vytvo�en� pojmenovan� plochy
CPosAction* Action_CreatePoseidonKeyPoint(CPoint ptLog,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

// smaz�n� pojmenovan� plochy
CPosAction* Action_DeletePoseidonKeyPoint(CPosKeyPointObject* pKeyPt,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

// p�esun pojmenovan� plochy
CPosAction* Action_MovePoseidonKeyPoint(CPosKeyPointObject* pKeyPt,int xAmount,int yAmount,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);
CPosAction* Action_RotatePoseidonKeyPoint(CPosKeyPointObject* pKeyPt,float grad,CPointVal origin,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);
CPosAction* Action_RotateMovePoseidonKeyPoint(CPosKeyPointObject* pKeyPt,CPointVal offset,float grad,CPointVal origin,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

// editace pojmenovan� plochy
CPosAction* Action_EditPoseidonKeyPoint(CPosKeyPointObject* pKeyPt,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup);

/////////////////////////////////////////////////////////////////////////////
// Akce s background images

BOOL DoCreateBgImage(CPosBackgroundImageObject * pObj, CPosEdMainDoc * pDoc);

CPosAction* Action_CreateBGImage(CPosEdMainDoc* pDocument, FltRect * pRealRect = NULL, CPosActionGroup* pToGroup = NULL);
CPosAction* Action_RemoveBGImage(CPosBackgroundImageObject * pObj, CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup = NULL);
CPosAction* Action_EditBGImage(CPosBackgroundImageObject* pBGImage, CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup = NULL);
CPosAction* Action_EditBGImage(CPosBackgroundImageObject* pBGImage, const FltRect& newRect, CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup = NULL);

CPosAction* Action_PasteClipboardBackgroundImage(PosClipBoardBackgroundImage& data,CPosEdMainDoc& doc,
                                                 const CPoint& cursorLog, CPosActionGroup * pToGroup = NULL);
CPosAction* Action_PasteAbsolutClipboardBackgroundImage(PosClipBoardBackgroundImage& data,CPosEdMainDoc& doc,
                                                        CPosActionGroup * pToGroup = NULL);

/////////////////////////////////////////////////////////////////////////////
// Pomocn� funkce

// obal unit bod� pro rect log. sou�adnic�ch
void GetUnitPosHull(UNITPOS& LftTop,UNITPOS& RghBtm,const CRect& rArea,const CPoseidonMap*);

// je st�ed UNIT �tverce v plo�e
BOOL IsUnitCenterAtArea(UNITPOS Pos,const CPositionArea*,const CPoseidonMap*);

// p�ekr�v� se UNIT �tverec s plochou
BOOL IsUnitRectAtArea(UNITPOS Pos,const CPositionArea*,const CPoseidonMap*);

// provede update sekund�rn�ch textur p�i zm�n� textury na UNITPOS Pos
void DoUpdateSecndTexturesByChangeOb(UNITPOS Pos,CPosEdMainDoc* pDocument, Ref<CTextureLayer>);

// provede update sekund�rn�ch textur p�i zm�n� textury na UNITPOS Pos
void DoUpdateSecndTexturesByChange(const CPositionArea*,CPosEdMainDoc* pDocument, Ref<CTextureLayer>);

// provede update/generov�n� sekund�rn� textury pro pole UNITPOS Pos
// function regenerates texture only if bRegenerate = TRUE && its CPosTextureTemplate::m_nTxtrCounter == 0
void DoUpdateSecndTexturesAt(UNITPOS Pos,CPosEdMainDoc* pDocument,BOOL bRegenerate = FALSE);

/////////////////////////////////////////////////////////////////////////////
// Texture layer action

CPosAction * Action_CreateTextureLayer(CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup = NULL);
CPosAction * Action_RemoveTextureLayer(CTextureLayer * layer, CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup = NULL);
CPosAction * Action_EditTextureLayer(CTextureLayer * layer, CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup = NULL);

/////////////////////////////////////////////////////////////////////////////
// XYZ file format reader
#include "XYZFile.h"
/////////////////////////////////////////////////////////////////////////////
#endif // !defined(_POSEDOBJS_MAIN_H_)

