/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Akce s animacemi

CPosAction* Action_EditPosAnimation(CPosAnimation* pNewAnim,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pNewAnim != NULL)
	{
			CPosActionGroup*  pGrpAction  = NULL;

			// vytvo�en� skupinov� ud�losti
			if ((pGrpAction = new CPosActionGroup(IDA_ANIM_CHANGE))==NULL) AfxThrowMemoryException();
			pGrpAction->m_bFromBuldozer = FALSE;

			// generuji ud�lost pro editaci animce
			CPosAnimAction* pAction = new CPosAnimAction(IDA_ANIM_CHANGE);
			pAction->m_bFromBuldozer = FALSE;
			// p�i�ad�m akci hodnoty (obr�cen�, budou teprve provedeny)
			pAction->m_pCurValue = new CPosAnimation();
			pAction->m_pCurValue->CopyFrom(&(pDocument->m_PosAnimation));
			pAction->m_pOldValue = pNewAnim;
			pGrpAction->AddAction(pAction);

			// generov�n�/update z�kladn�ch objekt�
			if(!DoUpdatePosAnimationObjects(pNewAnim,pDocument,pGrpAction))
			{
				if (pGrpAction)
					delete pGrpAction;
				else
				if (pNewAnim)
					delete pNewAnim;
				return NULL;
			};

			// provedu akci
			if (pToGroup)
				pToGroup->AddAction(pGrpAction);
			else
				pDocument->ProcessEditAction(pGrpAction);
			return pGrpAction;
	}
	return NULL;
};

// update objektu podle aktu�lni polohy a animace
/*AFX_EXT_API*/ void RealizeAction_UpdatePosAnimation(CPosEdMainDoc* pDocument)
{
	CPosAnimation* pNewAnim = new CPosAnimation();
	if (pNewAnim != NULL)
	{
			// animace je shodna s aktualni
			pNewAnim->CopyFrom(&(pDocument->m_PosAnimation));
			// vytvo�en� skupinov� ud�losti
			CPosActionGroup*  pGrpAction  = NULL;
			if ((pGrpAction = new CPosActionGroup(IDA_ANIM_CHANGE))==NULL) AfxThrowMemoryException();
			pGrpAction->m_bFromBuldozer = FALSE;

			// generuji ud�lost pro editaci animce
			CPosAnimAction* pAction = new CPosAnimAction(IDA_ANIM_CHANGE);
			pAction->m_bFromBuldozer = FALSE;
			// p�i�ad�m akci hodnoty (obr�cen�, budou teprve provedeny)
			pAction->m_pCurValue = new CPosAnimation();
			pAction->m_pCurValue->CopyFrom(&(pDocument->m_PosAnimation));
			pAction->m_pOldValue = pNewAnim;
			pGrpAction->AddAction(pAction);

			// generov�n�/update z�kladn�ch objekt�
			if(!DoUpdatePosAnimationObjects(pNewAnim,pDocument,pGrpAction))
			{
				if (pGrpAction)
					delete pGrpAction;
				else
				if (pNewAnim)
					delete pNewAnim;
				return;
			};

			// provedu akci
			pDocument->ProcessEditAction(pGrpAction);
			// reset undo/redo
			pDocument->m_ActionManager.ClearUndoRedoBuffers();				
	}
};

// update poseidn objekt�
BOOL DoUpdatePosAnimationObjects(CPosAnimation* pNewAnim,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	int i;
	TRY
	{
		// najdu existuj�c� objekty vzta�en� k s�ti
		CObArray	ExistObjects,CreateObjects;
		for(i=0; i<pDocument->m_PoseidonMap.m_InstanceObjs.GetSize(); i++)
		{
			CPosEdObject* pObj = (CPosEdObject*)(pDocument->m_PoseidonMap.m_InstanceObjs[i]);
			if (pObj)
			{
				ASSERT_KINDOF(CPosEdObject,pObj);
				if (pObj->m_bAnimation)
				{	// je to objekt s�t� -> vlo��m kopii se kterou budu pracovat
					ExistObjects.Add(pObj->CreateCopyObject());
				}
			}
		}
		// existuj�c�m objekt�m nuluji p��znak pou�it�
		for(i=0; i<ExistObjects.GetSize(); i++)
		{
			CPosEdObject* pObj = (CPosEdObject*)(ExistObjects[i]);
			pObj->m_nTempObjCounter = 0;
		}

		int nNewObjID = pDocument->m_PoseidonMap.m_InstanceObjs.GetSize();

		// update objekt� mapy
		for(int obj=0; obj<pNewAnim->m_AnimObjects.GetSize(); obj++)
		{
			CPosAnimObject* pAniObj = (CPosAnimObject*)(pNewAnim->m_AnimObjects[obj]);
			if (pAniObj)
			{
					BOOL bExist = FALSE;
					for(i=0; i<ExistObjects.GetSize(); i++)
					{
						CPosEdObject* pObj = (CPosEdObject*)(ExistObjects[i]);
						if (lstrcmp(pAniObj->m_sObjName,pObj->m_sObjName)==0)
						{
							ASSERT(pObj->m_nTempObjCounter == 0);
							pObj->m_nTempObjCounter++;
							bExist = TRUE;
							break;
						}
					}
					if (!bExist)
					{	// mus� se vytvo�it nov�
						CPosEdObject* pObj = NULL;
						{
							// poloha objektu
							REALPOS rPos = pAniObj->m_rPosCreate;
							// vytvo��m nov� objekt
							pObj = new CPosEdObject();
							if (pObj == NULL)
								AfxThrowMemoryException();
							pObj->CreateFromTemplateAt(pAniObj->m_pObjTemplate,rPos,&pDocument->m_PoseidonMap);

							if ((rPos.x < 0)||(rPos.z < 0))
							{	// podle current pozice
								pAniObj->m_CurrentPosition.SetPositionToObject(pObj);
							} else
							{	// poloha podle aktu�ln� pozice
								pAniObj->m_CurrentPosition.UpdatePositionFromObject(pObj);
								// upravim pozice pro snimky
								for(int i=0; i<pAniObj->m_ShotsPositions.GetSize(); i++)
								{
									pAniObj->UpdateShot(i);
								}
							};
							// nen� ur�eno ID objektu !
							ASSERT(pObj->m_nObjectID == UNDEF_REG_ID);
							pObj->m_sObjName  = pAniObj->m_sObjName;
							pObj->m_bAnimation= TRUE;
							pObj->m_nObjectID = nNewObjID;
							nNewObjID++;
							CreateObjects.Add(pObj);
						}
					}
			}
		}

		// update polohy (nastaven�) existuj�c�hc objekt�
		if (ExistObjects.GetSize()>0)
		{
			// vytvo�en� akc� pro update a smaz�n� objekt�
			for(i=0; i<ExistObjects.GetSize(); i++)
			{
				CPosEdObject* pObj = (CPosEdObject*)(ExistObjects[i]);
				ASSERT(pObj != NULL);
				if (pObj != NULL)
				{
					if (pObj->m_nTempObjCounter == 0)
					{	// objekt je nepou�it -> bude smaz�n
						CPosEdObject* pDelObj = pDocument->m_PoseidonMap.GetPoseidonObject(pObj->m_nObjectID);
						if (pDelObj != NULL)
						{
							ASSERT(pDelObj->m_nObjectID == pObj->m_nObjectID);
							ASSERT(pDelObj->m_bAnimation);
							CPosObjectAction* pAction = (CPosObjectAction*)Action_DeletePoseidonObject(pDelObj,pDocument,pToGroup);
							if (pAction)
							{
								// nebude se m�nit selekce
								// pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
								// nebude se prov�d�t update
								pAction->m_bUpdateView	  = TRUE;
							}
						};
					} else
					{	// objekt je pou�it -> bude updatov�n
						CPosEdObject* pEditObj = pDocument->m_PoseidonMap.GetPoseidonObject(pObj->m_nObjectID);
						if (pEditObj != NULL)
						{
							ASSERT(pEditObj->m_nObjectID == pObj->m_nObjectID);
							ASSERT(pEditObj->m_bAnimation);
							// update polohy podle current objektu
							CPosAnimObject*	pAnimObj = pNewAnim->GetPosAnimObject(pObj->m_sObjName);
							if (pAnimObj)
							{
								pAnimObj->m_CurrentPosition.SetPositionToObject(pObj);
							}

							CPosObjectAction* pAction = (CPosObjectAction*)Action_EditPoseidonObject(pObj,pDocument,pToGroup);
							if (pAction)
							{
								// nebude se m�nit selekce
								// pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
								// nebude se prov�d�t update
								pAction->m_bUpdateView	  = TRUE;
							}
						};
					};
				}
			}
			// ru��m zpracovan� objekty
			DestroyArrayObjects(&ExistObjects);
		}
		// vytvo�en� nov�ch (neexistuj�c�ch) objekt�
		{
			// generuji akce pro nov� objekty
			for(i=0; i<CreateObjects.GetSize(); i++)
			{
				CPosEdObject* pNewObj = (CPosEdObject*)(CreateObjects[i]);
				if (pNewObj)
				{
					ASSERT_KINDOF(CPosEdObject,pNewObj);
					CPosObjectAction* pAction  = (CPosObjectAction*)pDocument->GenerNewObjectAction(IDA_OBJECT_ADD,NULL,pNewObj,FALSE,pToGroup);
					if (pAction)
					{
						// objekt ji� nepot�ebuji
						CreateObjects[i] = NULL;
						// nebude se m�nit selekce
						// pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
						// nebude se prov�d�t update
						pAction->m_bUpdateView	  = TRUE;
						// objekt je pod��zen s�ti
						ASSERT(pAction->m_pOldValue != NULL);
						ASSERT_KINDOF(CPosEdObject,pAction->m_pOldValue);
						((CPosEdObject*)(pAction->m_pOldValue))->m_bAnimation = TRUE;
					};
				}
			}
		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		return FALSE;
	}
	END_CATCH_ALL
	// v�e je OK
	return TRUE;
};

