/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// poloha objektu

IMPLEMENT_DYNAMIC(CPosAnimObjPosition,CObject)

CPosAnimObjPosition::CPosAnimObjPosition()
{
	m_nRelHeight = 0.;
};

CPosAnimObjPosition::~CPosAnimObjPosition()
{	Destroy(); };

// z�kladn� funkce objektu
void  CPosAnimObjPosition::CopyFrom(const CPosAnimObjPosition* pSrc)
{
	for(int i=0; i<4; i++)
		for(int j=0; j<4; j++)
			m_Position[i][j] = pSrc->m_Position[i][j];

	m_nRelHeight = pSrc->m_nRelHeight;
};

void	CPosAnimObjPosition::Destroy()
{};

void	CPosAnimObjPosition::DoSerialize(CArchive& ar, DWORD nVer)
{
	for(int i=0; i<4; i++)
		for(int j=0; j<4; j++)
			MntSerializeFloat(ar,m_Position[i][j]);

	MntSerializeFloat(ar,m_nRelHeight);
};

// nastaven� polohy pro objekt
void  CPosAnimObjPosition::UpdatePositionFromObject(const CPosEdObject* pObj)
{
	ASSERT(pObj != NULL);
	for(int i=0; i<4; i++)
		for(int j=0; j<4; j++)
			m_Position[i][j] = pObj->m_Position[i][j];

	// rel. v��ka
	m_nRelHeight = pObj->m_nRelHeight;
};

void  CPosAnimObjPosition::SetPositionToObject(CPosEdObject* pObj)
{
	ASSERT(pObj != NULL);
	for(int i=0; i<4; i++)
		for(int j=0; j<4; j++)
			pObj->m_Position[i][j] = m_Position[i][j];

	// rel. v��ka
	pObj->m_nRelHeight = m_nRelHeight;
};

/////////////////////////////////////////////////////////////////////////////
// CPosAnimObject - objekt animace

IMPLEMENT_DYNAMIC(CPosAnimObject,CObject)

CPosAnimObject::CPosAnimObject()
{		
	m_pObjTemplate = NULL;

	// nen� vytv��en
	m_rPosCreate.x =
	m_rPosCreate.z = -1.f;
};
CPosAnimObject::~CPosAnimObject()
{
	Destroy();
};

BOOL  CPosAnimObject::IsCamera() const
{
	if (m_pObjTemplate != NULL)
	{
		if (lstrcmpi(ANIME_CAMERA_NAME_IDENTIFY,m_pObjTemplate->m_sObjFile)==0)
			return TRUE;
	}
	return FALSE;
};

// z�kladn� funkce objektu
void  CPosAnimObject::CopyFrom(const CPosAnimObject* pSrc)
{
	Destroy();
	m_sObjName = pSrc->m_sObjName;
	if (pSrc->m_pObjTemplate != NULL)
	{
		m_pObjTemplate = new CPosObjectTemplate(pSrc->m_pObjTemplate->m_pOwner);
		m_pObjTemplate->CopyFrom(pSrc->m_pObjTemplate);
	}
	m_rPosCreate	= pSrc->m_rPosCreate;
	// kopie polohy
	m_CurrentPosition.CopyFrom(&(pSrc->m_CurrentPosition));
	for(int i=0; i<pSrc->m_ShotsPositions.GetSize(); i++)
	{
		CPosAnimObjPosition* pPosOld = (CPosAnimObjPosition*)(pSrc->m_ShotsPositions[i]);
		if (pPosOld != NULL)
		{
			CPosAnimObjPosition* pPosNew = new CPosAnimObjPosition();
			pPosNew->CopyFrom(pPosOld);
			m_ShotsPositions.Add(pPosNew);
		}
	}
};

void	CPosAnimObject::Destroy()
{
	if (m_pObjTemplate != NULL)
	{
		delete m_pObjTemplate;
		m_pObjTemplate = NULL;
	}
	DestroyArrayObjects(&m_ShotsPositions);
};

void	CPosAnimObject::DoSerialize(CArchive& ar, DWORD nVer,CPosEdBaseDoc*	m_pDocument)
{
	MntSerializeString(ar,m_sObjName);
	if (ar.IsStoring())
	{
		ASSERT(m_pObjTemplate != NULL);
		m_pObjTemplate->DoSerialize(ar,nVer);
	} else
	{
		ASSERT(m_pObjTemplate == NULL);
		m_pObjTemplate = new CPosObjectTemplate(m_pDocument);
		if (m_pObjTemplate == NULL)
			AfxThrowMemoryException();
		m_pObjTemplate->DoSerialize(ar,nVer);
	};
	// ulo�en� polohy
	m_CurrentPosition.DoSerialize(ar,nVer);
	// serializace snimku
	LONG nCount = m_ShotsPositions.GetSize();
	MntSerializeLong(ar,nCount);
	for(int i=0; i<nCount; i++)
	{
		CPosAnimObjPosition* pPosition = NULL;
		if (ar.IsStoring())
			pPosition = (CPosAnimObjPosition*)(m_ShotsPositions[i]);
		else
		{
			pPosition = new CPosAnimObjPosition();
			m_ShotsPositions.Add(pPosition);
		};
		if (pPosition == NULL)
			AfxThrowMemoryException();
		pPosition->DoSerialize(ar,nVer);
	}
};

// update polohy objektu
CPosEdObject*	CPosAnimObject::GetPoseidonAnimObject(const CPoseidonMap* pMap)
{
	for(int i=0; i<pMap->m_InstanceObjs.GetSize(); i++)
	{
		CPosEdObject* pObj = (CPosEdObject*)(pMap->m_InstanceObjs[i]);
		if (pObj != NULL)
		{
			ASSERT_KINDOF(CPosEdObject,pObj);
			if ((pObj->m_bAnimation)&&(lstrcmp(pObj->m_sObjName,m_sObjName)==0))
			{	// je to hledan� objekt
				return pObj;
			}
		}
	}
	ASSERT(FALSE);
	return NULL;
};

BOOL	CPosAnimObject::UpdateObjectPosition(const CPoseidonMap* pMap)
{
	CPosEdObject*	pObj = GetPoseidonAnimObject(pMap);
	ASSERT(pObj != NULL);
	if (pObj == NULL)
		return FALSE;
	// update polohy
	m_CurrentPosition.UpdatePositionFromObject(pObj);
	return TRUE;
};

// prace se snimky
// novy snimek
void CPosAnimObject::AddNewShot()	
{
	CPosAnimObjPosition* pNewPos = new CPosAnimObjPosition();
	if (pNewPos != NULL)
		{
		pNewPos->CopyFrom(&m_CurrentPosition);
		m_ShotsPositions.Add(pNewPos);
		}
};
// smzani snimku
void CPosAnimObject::DeleteShot(int nIndx)
{
	ASSERT((nIndx >= 0)&&(nIndx<m_ShotsPositions.GetSize()));
	CPosAnimObjPosition* pShot = (CPosAnimObjPosition*)(m_ShotsPositions[nIndx]);
	if (pShot)
	{
		m_ShotsPositions.RemoveAt(nIndx);	
		delete pShot;
	}
};
// snimek podle m_CurrentPosition
void CPosAnimObject::UpdateShot(int nIndx)
{
	ASSERT((nIndx >= 0)&&(nIndx<m_ShotsPositions.GetSize()));
	CPosAnimObjPosition* pShot = (CPosAnimObjPosition*)(m_ShotsPositions[nIndx]);
	if (pShot)
	{
		pShot->CopyFrom(&m_CurrentPosition);
	}
};
// poloha podle snimku
void CPosAnimObject::SetPosShot(int nIndx)
{
	ASSERT((nIndx >= 0)&&(nIndx<m_ShotsPositions.GetSize()));
	CPosAnimObjPosition* pShot = (CPosAnimObjPosition*)(m_ShotsPositions[nIndx]);
	if (pShot)
	{
		m_CurrentPosition.CopyFrom(pShot);
	}
};

/////////////////////////////////////////////////////////////////////////////
// sn�mek animace

IMPLEMENT_DYNAMIC(CPosAnimShot,CObject)

CPosAnimShot::CPosAnimShot()
{
	m_nShotTime = 0;
	m_sShotName.Empty();
};

CPosAnimShot::~CPosAnimShot()
{	Destroy();
};

// z�kladn� funkce objektu
void CPosAnimShot::CopyFrom(const CPosAnimShot* pSrc)
{
	m_nShotTime = pSrc->m_nShotTime;
	m_sShotName = pSrc->m_sShotName;
};

void CPosAnimShot::Destroy()
{
	m_nShotTime = 0;
	m_sShotName.Empty();
};

void CPosAnimShot::DoSerialize(CArchive& ar, DWORD nVer)
{
	MntSerializeDouble(ar,m_nShotTime);
	MntSerializeString(ar,m_sShotName);
};

/////////////////////////////////////////////////////////////////////////////
// CPosAnimation   (PosAnimation.cpp)

IMPLEMENT_DYNAMIC(CPosAnimation,CMntFileObject)


CPosAnimation::CPosAnimation()
{
	m_bActiveAnimation = FALSE;

	static LPCTSTR fileAnimID = _T("POANI");
//	for(int i=0; i<MNT_FILE_ID_LEN; i++)
	//	m_cFileID[i] = fileAnimID[i];
	SetParams(CMntFileObject::fileID|CMntFileObject::fileVer,fileAnimID);
};

CPosAnimation::~CPosAnimation()
{	// zru�en� animace
	DestroyAnimation();
};

// kopie objektu
void  CPosAnimation::CopyFrom(const CPosAnimation* pSrc)
{
	m_bActiveAnimation	= pSrc->m_bActiveAnimation;
	m_sAnimFileName			= pSrc->m_sAnimFileName;
	m_pDocument					= pSrc->m_pDocument;
	// kopie objekt�
	CopyAnimObject(pSrc);
};
void  CPosAnimation::CopyAnimObject(const CPosAnimation* pSrc)
{
	// objekty
	DestroyArrayObjects(&m_AnimObjects);
	for(int i=0; i<pSrc->m_AnimObjects.GetSize();i++)
	{
		CPosAnimObject* pOld = (CPosAnimObject*)(pSrc->m_AnimObjects[i]);
		if (pOld != NULL)
		{
			CPosAnimObject* pNew = new CPosAnimObject();
			if (pNew)
			{
				pNew->CopyFrom(pOld);
				m_AnimObjects.Add(pNew);
			}
		}
	}
	//sn�mky
	DestroyArrayObjects(&m_AnimShots);
	for(i=0; i<pSrc->m_AnimShots.GetSize();i++)
	{
		CPosAnimShot* pOld = (CPosAnimShot*)(pSrc->m_AnimShots[i]);
		if (pOld != NULL)
		{
			CPosAnimShot* pNew = new CPosAnimShot();
			if (pNew)
			{
				pNew->CopyFrom(pOld);
				m_AnimShots.Add(pNew);
			}
		}
	}
};

// zru�en� animace
void	CPosAnimation::DestroyAnimation()
{
	DestroyArrayObjects(&m_AnimObjects);
	DestroyArrayObjects(&m_AnimShots);

	// deaktivace
	m_bActiveAnimation = FALSE;

	m_sAnimFileName.Empty();	// nen� ur�en soubor
	SetModified(FALSE);
};

// inicializace nov� animace
void CPosAnimation::OnNewAnimation()
{
	m_sAnimFileName.Empty();	// nen� ur�en soubor

	// aktivace
	m_bActiveAnimation = TRUE;
	SetModified(FALSE);
};

// serializace objektu (implementuj� potomci)
void CPosAnimation::OpenFile(LPCTSTR pFileName)
{
	m_sAnimFileName = pFileName;
	// soubor objektu
	SetFile(m_sAnimFileName,0);
	// na�ten�
	if (!OnLoadObject(NULL,POSED_FILE_VER))
	{
		ASSERT(!m_bActiveAnimation);
		m_bActiveAnimation = FALSE;
	} else
	{
		ASSERT(m_bActiveAnimation);
		m_bActiveAnimation = TRUE;
	};
};
BOOL CPosAnimation::SaveFile(LPCTSTR pFileName)
{
	m_sAnimFileName = pFileName;
	// soubor objektu
	SetFile(m_sAnimFileName,0);
	// ulo�en�
	return OnSaveObject(NULL,POSED_FILE_VER);
};

void CPosAnimation::DoSerialize(CArchive& ar, DWORD nVer, void* pParams)
{
	WORD nTest = 100;
	MntSerializeWord(ar,nTest);
	// serializace objektu animace
	LONG nCount = m_AnimObjects.GetSize();
	MntSerializeLong(ar,nCount);

	for(int i=0; i<nCount; i++)
	{
		BOOL bExist;
		CPosAnimObject* pObj = NULL;
		if (ar.IsStoring())
		{
			pObj		= (CPosAnimObject*)(m_AnimObjects[i]);
			bExist	= (pObj != NULL);
		}
		MntSerializeBool(ar,bExist);
		if (!ar.IsStoring())
		{
			if (bExist)
			{
				if ((pObj = new CPosAnimObject())==NULL)
					AfxThrowMemoryException();
				m_AnimObjects.Add(pObj);
			}
		}
		if (pObj != NULL)
		{
			ASSERT(bExist);
			pObj->DoSerialize(ar,nVer,m_pDocument);
		};
	}
	// serializace sn�mk�
	nCount = m_AnimShots.GetSize();
	MntSerializeLong(ar,nCount);

	for(i=0; i<nCount; i++)
	{
		BOOL bExist;
		CPosAnimShot* pObj = NULL;
		if (ar.IsStoring())
		{
			pObj		= (CPosAnimShot*)(m_AnimShots[i]);
			bExist	= (pObj != NULL);
		}
		MntSerializeBool(ar,bExist);
		if (!ar.IsStoring())
		{
			if (bExist)
			{
				if ((pObj = new CPosAnimShot())==NULL)
					AfxThrowMemoryException();
				m_AnimShots.Add(pObj);
			}
		}
		if (pObj != NULL)
		{
			ASSERT(bExist);
			pObj->DoSerialize(ar,nVer);
		};
	}

	// aktivace
	if (ar.IsStoring())
	{
		ASSERT(m_bActiveAnimation);
	} else
	{
		ASSERT(!m_bActiveAnimation);
		m_bActiveAnimation = TRUE;
	};
};

// podpora
BOOL CPosAnimation::GetAnimationFile(CString& sFile, BOOL bOpen)
{
	CString sFilter,sExt,sDir,sTitle;
	VERIFY(sFilter.LoadString(IDS_POS_ANIMATION_FILE));

	VERIFY(sTitle.LoadString(IDS_POS_ANIMATION_FILE_TITLE));
	sDir = GetPosEdEnvironment()->m_optSystem.m_sObjWorldPath;
	MakeShortDirStr(sDir,sDir);
	// p��prava CFileDialog
	CFileDialog dlg(bOpen);
	if (bOpen)
		dlg.m_ofn.Flags |= OFN_FILEMUSTEXIST;
	else
		dlg.m_ofn.Flags |= OFN_PATHMUSTEXIST;
	sFilter += (TCHAR)(_T('\0'));
	sFilter += _T("*.");
	sFilter += ANIM_FILE_EXT;
	sFilter += (TCHAR)(_T('\0'));
	sFilter += (TCHAR)(_T('\0'));
	dlg.m_ofn.nMaxCustFilter = 1;
	dlg.m_ofn.lpstrFilter = sFilter;
	dlg.m_ofn.lpstrDefExt = ANIM_FILE_EXT;
	dlg.m_ofn.lpstrTitle  = sTitle;
	dlg.m_ofn.lpstrFile = sFile.GetBuffer(_MAX_PATH);
	dlg.m_ofn.lpstrInitialDir = sDir;
	BOOL bRes = (dlg.DoModal()==IDOK)?TRUE:FALSE;
	sFile.ReleaseBuffer();
  if (bRes)
  {
    SeparateDirFromFileName(sDir, (LPCTSTR) sFile);
    GetPosEdEnvironment()->m_optSystem.m_sObjWorldPath = sDir;
    GetPosEdEnvironment()->m_optSystem.SaveParams();
  }
	return bRes;
};

BOOL CPosAnimation::GetAnimExportFile(CString& sFile, BOOL bOpen)
{
	CString sFilter,sExt,sDir,sTitle;
	VERIFY(sFilter.LoadString(IDS_POS_ANIMEXPORT_FILE));

	VERIFY(sTitle.LoadString(IDS_POS_ANIMEXPORT_FILE_TITLE));
	sDir = GetPosEdEnvironment()->m_optSystem.m_sObjWorldPath;
	MakeShortDirStr(sDir,sDir);
	// default file
	CString sDefFile;
	sDefFile = m_sAnimFileName;
	if (sDefFile.GetLength()>0)
	{
		int nDot = sDefFile.ReverseFind(_T('.'));
		if (nDot > 0)
		{
			CString sFileExp =  sDefFile.Left(nDot+1);
					sFileExp += ANIMEXPORT_FILE_EXT;
			sDefFile = sFileExp;
		};
		// nastav�m def. soubor
		sFile = sDefFile;
	}

	// p��prava CFileDialog
	CFileDialog dlg(bOpen);
	if (bOpen)
		dlg.m_ofn.Flags |= OFN_FILEMUSTEXIST;
	else
		dlg.m_ofn.Flags |= OFN_PATHMUSTEXIST;
	sFilter += (TCHAR)(_T('\0'));
	sFilter += _T("*.");
	sFilter += ANIMEXPORT_FILE_EXT;
	sFilter += (TCHAR)(_T('\0'));
	sFilter += (TCHAR)(_T('\0'));
	dlg.m_ofn.nMaxCustFilter = 1;
	dlg.m_ofn.lpstrFilter = sFilter;
	dlg.m_ofn.lpstrDefExt = ANIMEXPORT_FILE_EXT;
	dlg.m_ofn.lpstrTitle  = sTitle;
	dlg.m_ofn.lpstrFile = sFile.GetBuffer(_MAX_PATH);
	dlg.m_ofn.lpstrInitialDir = sDir;
	BOOL bRes = (dlg.DoModal()==IDOK)?TRUE:FALSE;
	sFile.ReleaseBuffer();
  if (bRes)
  {
    SeparateDirFromFileName(sDir, (LPCTSTR) sFile);
    GetPosEdEnvironment()->m_optSystem.m_sObjWorldPath = sDir;
    GetPosEdEnvironment()->m_optSystem.SaveParams();
  }
	return bRes;
};

/////////////////////////////////////////////////////////////////////////////
// export animace

void CPosAnimation::DoExportAnimationData(CString& sFile)
{
	// EXPORT pojmenovan�ch oblast�
	CFile save;
	TRY
	{
		if (!save.Open(sFile,CFile::modeCreate|CFile::modeWrite))
			AfxThrowUserException();

		// indexy �as� 
		CWordArray			arIndexes;
		{
			CObArray sortShots;
			for(int i=0; i<m_AnimShots.GetSize(); i++)
			{
				CPosAnimShot* pShot = (CPosAnimShot*)(m_AnimShots[i]);

				BOOL bAdd = FALSE;
				for(int j=0; j<sortShots.GetSize(); j++)
				{
					CPosAnimShot* pShotOld = (CPosAnimShot*)(sortShots[j]);
					if (pShotOld->m_nShotTime > pShot->m_nShotTime)
					{
						sortShots.InsertAt(j,pShot);
						bAdd = TRUE;
						break;
					}
				}
				if (!bAdd)
					sortShots.Add(pShot);
			}
			// vytvo��m pole index�
			ASSERT(sortShots.GetSize() == m_AnimShots.GetSize());
			for(i=0; i<sortShots.GetSize(); i++)
			{
				CPosAnimShot* pShot = (CPosAnimShot*)(sortShots[i]);
				WORD nIndx = (WORD)(GetShotIndex(pShot));
				arIndexes.Add(nIndx);
			}
		}
		// nalezen� kamery
		CPosAnimObject*		pCamera = NULL;
		{
			for(int i=0; i<m_AnimObjects.GetSize(); i++)
			{
				CPosAnimObject* pObj = (CPosAnimObject*)(m_AnimObjects[i]);
				if ((pObj != NULL)&&(pObj->IsCamera()))
				{	// nalezena kamera
					pCamera = pObj;
					break;
				}
			}
		}
	
		// z�hlav�
		SaveAnimExportHeader(save);

		// kamera
		if (pCamera != NULL)
		{
			SaveAnimExportCamera(save,pCamera,&arIndexes);
		};
		// objekty
		CStringArray Descriptors;
		SaveAnimExportObjects(save,&arIndexes,&Descriptors);
		if (Descriptors.GetSize()>0)
			SaveAnimExportDescriptors(save,&Descriptors);

		// konec
		SaveAnimExportFooter(save);

		save.Close();
	}
	CATCH_ALL(e)
	{
		AfxMessageBox(IDS_ERROR_EXPORTANIM_NAMES,MB_OK|MB_ICONEXCLAMATION);
		save.Abort();
	}
	END_CATCH_ALL
};

void CPosAnimation::SaveExportFileLine(CFile& save,LPCTSTR pText)
{
	static LPCTSTR pNewLn = _T("\r\n");
	CString sText;
	if (pText != NULL)
		sText = pText;
	// ulo�en� do souboru
	if (sText.GetLength()>0)
	{
		save.Write(((LPCTSTR)sText),sText.GetLength());
	}
	save.Write(pNewLn,lstrlen(pNewLn));
};

void CPosAnimation::SaveAnimExportHeader(CFile& save)
{
	SaveExportFileLine(save,_T("//////////////////////////////////////////////////////////////////////"));
	SaveExportFileLine(save,_T("// Export parametru ANIMACE - POSEIDON"));
	SaveExportFileLine(save,NULL);
	SaveExportFileLine(save,NULL);
};

void CPosAnimation::SaveAnimExportFooter(CFile& save)
{
	SaveExportFileLine(save,NULL);
	SaveExportFileLine(save,_T("//////////////////////////////////////////////////////////////////////"));
};

void CPosAnimation::SaveAnimExportObject(CFile& save,CPosAnimObject* pObj,CWordArray* pIndexes,BOOL bCamera,CStringArray* pDescriptors)
{
	SaveExportFileLine(save,NULL);
	// jm�no objektu v pozn�mce
	CString sObjName = pObj->m_sObjName,sText,sNum;
	if (bCamera)
	{	// pevne jmeno
		sObjName = ANIMEXPORT_CAMERA_NAME;
		// jmeno neukladam
	} else
	{	// ulozim jmeno
		sText  = _T("// ");
		sText += sObjName;
		SaveExportFileLine(save,sText);
		SaveExportFileLine(save,NULL);
	};

	// jm�na prom�nn�ch
	CString sObjPathSize = sObjName; sObjPathSize += _T("PathSize");
	CString sObjTimes	 = sObjName; sObjTimes	  += _T("Times");
	CString sObjPath	 = sObjName; sObjPath	  += _T("Path");
	CString sObjDesc	 = sObjName; sObjDesc	  += _T("Desc");
	CString sShotCount;	 NumToStandardString(sShotCount,(LONG)(m_AnimShots.GetSize()));

	// konstanta pro pocet snimku
	{
		sText  = _T("const int ");
		sText += sObjPathSize;
		sText += _T("=");
		sText += sShotCount;
		sText += _T(";");
		SaveExportFileLine(save,sText);
	};

	// pole pro casy
	{
		sText  = _T("static float ");
		sText += sObjTimes;
		sText += _T("[");
		sText += sObjPathSize;
		sText += _T("]=");
		SaveExportFileLine(save,sText);
		SaveExportFileLine(save,_T("{"));
		// hodnoty �asu
		ASSERT(pIndexes->GetSize() == m_AnimShots.GetSize());

		int nLnCount = 0;
		for(int i=0; i<pIndexes->GetSize(); i++)
		{
			int nIndx = (*pIndexes)[i];
			CPosAnimShot* pShot = (CPosAnimShot*)(m_AnimShots[nIndx]);
			if (pShot != NULL)
			{
				NumToStandardString(sNum,pShot->m_nShotTime,5);
				if (nLnCount == 0)
					sText = _T("\t");
				// p�id�m hodnotu
				sText += sNum;
				// p�id�m ��rku
				if (i < (pIndexes->GetSize()-1))
					sText += _T(",");
				// dal�� �daj
				nLnCount++;
				// ukon��m ��dek
				if ((nLnCount == ANIMEXPORT_TIMES_AT_LINE)||(i == (pIndexes->GetSize()-1)))
				{
					SaveExportFileLine(save,sText);
					nLnCount = 0;
				}
			};
		}
		SaveExportFileLine(save,_T("};"));
	};

	// pole pro polohy
	{
		sText  = _T("static const Matrix4 ");
		sText += sObjPath;
		sText += _T("[");
		sText += sObjPathSize;
		sText += _T("]=");
		SaveExportFileLine(save,sText);
		SaveExportFileLine(save,_T("{"));
		// export pozic
		ASSERT(pIndexes->GetSize() == pObj->m_ShotsPositions.GetSize());
		for(int i=0; i<pIndexes->GetSize(); i++)
		{
			int nIndx = (*pIndexes)[i];
			CPosAnimObjPosition* pPosition = (CPosAnimObjPosition*)(pObj->m_ShotsPositions[nIndx]);
			if (pPosition != NULL)
			{
				SaveExportFileLine(save,_T("\tMatrix4"));
				SaveExportFileLine(save,_T("\t("));
				// data matice
				for(int ln=0; ln <3; ln++)
				{
					CString sText = _T("\t\t");
					for(int cl=0; cl <4; cl++)
					{
						NumToStandardString(sNum,(double)(pPosition->m_Position[ln][cl]),5);
						sText += sNum;
						if ((cl < 3)||(ln < 2))
							sText += _T(", ");

					}
					SaveExportFileLine(save,sText);
				}
				SaveExportFileLine(save,_T("\t),"));
			}
		};
		SaveExportFileLine(save,_T("};"));
	};

	// deskriptor objektu
	if (!bCamera)
	{
		sText  = _T("static const AnimationDescriptor ");
		sText += sObjDesc;
		sText += _T("=");
		SaveExportFileLine(save,sText);
		SaveExportFileLine(save,_T("{"));
		// ID objektu
		sText  = _T("\t\"");
		if (pObj->m_pObjTemplate)
			pObj->m_pObjTemplate->GetBuldozerName(sNum);
		for(int c=0; c<sNum.GetLength(); c++)
		{
			TCHAR buff[2]; buff[0] = sNum[c]; buff[1] = 0;
			sText += buff;
			if (buff[0] == _T('\\'))
				sText += buff;
		};
		sText += _T("\",");
		SaveExportFileLine(save,sText);
		// promenne objektu
		sText  = _T("\t");
		sText += sObjPathSize;
		sText += _T(",");
		sText += sObjTimes;
		sText += _T(",");
		sText += sObjPath;
		SaveExportFileLine(save,sText);
		SaveExportFileLine(save,_T("};"));
		// vlo��m do deskriptoru
		if (pDescriptors != NULL)
			pDescriptors->Add(sObjDesc);
	};

	SaveExportFileLine(save,NULL);
};

void CPosAnimation::SaveAnimExportObjects(CFile& save,CWordArray* pIndexes,CStringArray* pDescriptors)
{
	SaveExportFileLine(save,_T("//////////////////////////////////////////////////////////////////////"));
	SaveExportFileLine(save,_T("// Popis trajektorie jednotlivych objektu"));
	// jednotliv� objekty
	for(int i=0; i<m_AnimObjects.GetSize(); i++)
	{
		CPosAnimObject* pObj = (CPosAnimObject*)(m_AnimObjects[i]);
		if ((pObj != NULL)&&(!pObj->IsCamera()))
		{
			SaveAnimExportObject(save,pObj,pIndexes,FALSE,pDescriptors);
		}
	}
};

void CPosAnimation::SaveAnimExportDescriptors(CFile& save,CStringArray* pDescriptors)
{
	SaveExportFileLine(save,_T("//////////////////////////////////////////////////////////////////////"));
	SaveExportFileLine(save,_T("// Seznam objektu"));
	SaveExportFileLine(save,NULL);
	SaveExportFileLine(save,_T("static const AnimationDescriptor* Descriptors[]="));
	SaveExportFileLine(save,_T("{"));
	// seznam objektu
	CString sText;
	int nLnCount = 0;
	for(int i=0; i<=pDescriptors->GetSize(); i++)
	{
		if (nLnCount == 0)
			sText = _T("\t");
		nLnCount++;
		// p�id�m objekt 
		CString sName;
		if (i == pDescriptors->GetSize())
			sName = _T("NULL");
		else
		{
			sName = (*pDescriptors)[i];
			// pridam ukazatel
			sText	+= _T("&");
		};
		sText	+= sName;
		// oddelim objekt
		if (i < pDescriptors->GetSize())
			sText += _T(",");
		if ((nLnCount == ANIMEXPORT_DESCS_AT_LINE)||(i == pDescriptors->GetSize()))
		{
			SaveExportFileLine(save,sText);
			nLnCount = 0;
		}
	}
	SaveExportFileLine(save,_T("};"));
	SaveExportFileLine(save,NULL);
};

void CPosAnimation::SaveAnimExportCamera(CFile& save,CPosAnimObject* pCamera,CWordArray* pIndexes)
{
	SaveExportFileLine(save,_T("//////////////////////////////////////////////////////////////////////"));
	SaveExportFileLine(save,_T("// Popis trajektorie kamery"));
	SaveAnimExportObject(save,pCamera,pIndexes,TRUE,NULL);
};


/////////////////////////////////////////////////////////////////////////////
// pr�ce s objekty

extern BOOL DoEditAnimObject(CPosAnimObject* pObject,CPosAnimation* pAnim,BOOL bNew);

CPosAnimObject* CPosAnimation::CreateNewAnimObject(REALPOS rPos,CPosObjectTemplate* pTemplate)
{
	CPosAnimObject* pNewObj = NULL;

	TRY
	{
		if ((pNewObj = new CPosAnimObject())==NULL)
			AfxThrowMemoryException();
		// parametry objektu
		pNewObj->m_sObjName.Empty();
		/*
		pNewObj->m_pObjTemplate = new CPosObjectTemplate(m_pDocument);
		pNewObj->m_pObjTemplate->CopyFrom(pTemplate);
		*/
		pNewObj->m_pObjTemplate = pTemplate;
		pNewObj->m_rPosCreate   = rPos;

		// edituji objekt
		if (!DoEditAnimObject(pNewObj,this,TRUE))
		{
			delete pNewObj;
			return NULL;
		};
		// vytvo��m pozice pro snimky
		for(int i=0; i<m_AnimShots.GetSize(); i++)
		{
			pNewObj->AddNewShot();
		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pNewObj != NULL)
			delete pNewObj;
		pNewObj = NULL;
	}
	END_CATCH_ALL

	return pNewObj;
};

void CPosAnimation::AddNewObject(CPosAnimObject* pObj)
{
	m_AnimObjects.Add(pObj);
};

void CPosAnimation::DeleteObject(LPCTSTR pName)
{
	for(int i=0; i<m_AnimObjects.GetSize(); i++)
	{
		CPosAnimObject* pObj = (CPosAnimObject*)(m_AnimObjects[i]);
		if (lstrcmp(pName,pObj->m_sObjName)==0)
		{
			m_AnimObjects.RemoveAt(i);
			delete pObj;
			return;
		}
	}
};

CPosAnimObject*	CPosAnimation::GetPosAnimObject(LPCTSTR pName)
{
	for(int i=0; i<m_AnimObjects.GetSize(); i++)
	{
		CPosAnimObject* pObj = (CPosAnimObject*)(m_AnimObjects[i]);
		if (lstrcmp(pName,pObj->m_sObjName)==0)
		{
			return pObj;
		}
	}
	return NULL;
};

// update polohy objektu
BOOL CPosAnimation::UpdateAllObjectPosition(const CPoseidonMap* pMap)
{
	for(int i=0; i<m_AnimObjects.GetSize(); i++)
	{
		CPosAnimObject* pObj = (CPosAnimObject*)(m_AnimObjects[i]);
		if (pObj != NULL)
			VERIFY(pObj->UpdateObjectPosition(pMap));
	}
	return TRUE;
};

// pr�ce se sn�mky
extern BOOL DoEditAnimShot(CPosAnimShot* pShot,CPosAnimation* pAnim,BOOL bNew);

BOOL CPosAnimation::AddNewShotFromCurrPos()
{
	CPosAnimShot* pNewObj = NULL;

	TRY
	{
		if ((pNewObj = new CPosAnimShot())==NULL)
			AfxThrowMemoryException();

		// edituji objekt
		if (!DoEditAnimShot(pNewObj,this,TRUE))
		{
			delete pNewObj;
			return FALSE;
		};
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pNewObj != NULL)
			delete pNewObj;
		pNewObj = NULL;
		return FALSE;
	}
	END_CATCH_ALL
	// sn�mek �spe�ne vytvo�en -> vlozim snimek
	m_AnimShots.Add(pNewObj);

	// vkladam pozice
	for(int i=0; i<m_AnimObjects.GetSize(); i++)
	{
		CPosAnimObject* pObj = (CPosAnimObject*)(m_AnimObjects[i]);
		if (pObj != NULL)
		{
			pObj->AddNewShot();
			ASSERT(pObj->m_ShotsPositions.GetSize() == m_AnimShots.GetSize());
		}
	}

	// vse je OK
	return TRUE;
};

int	CPosAnimation::GetShotIndex(const CPosAnimShot* pFindShot)
{
	int nIndx = -1;
	for(int i=0; i<m_AnimShots.GetSize(); i++)
	{
		CPosAnimShot* pShot = (CPosAnimShot*)(m_AnimShots[i]);
		if ((pShot != NULL)&&(pShot == pFindShot))
		{
			nIndx = i;
			break;
		}
	};
	return nIndx;
};

void CPosAnimation::DeleteShot(int nIndx)
{
	if ((nIndx < 0)||(nIndx >= m_AnimShots.GetSize()) )
		return;
	// vyjmu snimek
	CPosAnimShot* pShot = (CPosAnimShot*)(m_AnimShots[nIndx]);
	m_AnimShots.RemoveAt(nIndx);
	// mazu pozice
	for(int i=0; i<m_AnimObjects.GetSize(); i++)
	{
		CPosAnimObject* pObj = (CPosAnimObject*)(m_AnimObjects[i]);
		if (pObj != NULL)
		{
			pObj->DeleteShot(nIndx);
			ASSERT(pObj->m_ShotsPositions.GetSize() == m_AnimShots.GetSize());
		}
	}
	// zrusim snimek
	delete pShot;
};

void CPosAnimation::UpdateShotFromCurrent(int nShot)
{
	for(int i=0; i<m_AnimObjects.GetSize(); i++)
	{
		CPosAnimObject* pObj = (CPosAnimObject*)(m_AnimObjects[i]);
		if (pObj != NULL)
		{
			ASSERT(pObj->m_ShotsPositions.GetSize() == m_AnimShots.GetSize());
			pObj->UpdateShot(nShot);
		}
	}
};

void CPosAnimation::UpdateCurrentFromShot(int nShot)
{
	for(int i=0; i<m_AnimObjects.GetSize(); i++)
	{
		CPosAnimObject* pObj = (CPosAnimObject*)(m_AnimObjects[i]);
		if (pObj != NULL)
		{
			ASSERT(pObj->m_ShotsPositions.GetSize() == m_AnimShots.GetSize());
			pObj->SetPosShot(nShot);
		}
	}
};







