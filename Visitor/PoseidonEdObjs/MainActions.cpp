/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPosObjectAction

IMPLEMENT_DYNAMIC(CPosObjectAction,CPosAction)

CPosObjectAction::CPosObjectAction(int nType):
	CPosAction(nType)
{
	m_pCurValue		 =
	m_pOldValue		 = NULL;
	// nem�n�m selekci objektu
	m_nSelectObjType = ida_SelectExclu;
};

CPosObjectAction::~CPosObjectAction()
{
	if (m_pCurValue) delete m_pCurValue;
	if (m_pOldValue) delete m_pOldValue;
	m_pCurValue =
	m_pOldValue = NULL;
};

// v�m�na hodnot m_CurValue a m_OldValue
BOOL CPosObjectAction::ChangeValues()
{
	CPosObject*   pTemp = m_pCurValue;
	m_pCurValue = m_pOldValue;
	m_pOldValue = pTemp;
	return TRUE;
};

/////////////////////////////////////////////////////////////////////////////
// CPosObjectGroupAction

IMPLEMENT_DYNAMIC(CPosObjectGroupAction, CPosActionGroup)

CPosObjectGroupAction::CPosObjectGroupAction(int nType)
: CPosActionGroup(nType)
{
}

// proveden� akce v dokumentu
BOOL CPosObjectGroupAction::RealizeAction(CDocument* pDoc)
{
	// provedu d�l�� akce pro zm�nu textur
	BOOL bChange = CPosActionGroup::RealizeAction(pDoc);
	// provedu akci - reaguji na n� updatem plochy
	bChange |= CPosAction::RealizeAction(pDoc);
	return bChange;
}

/////////////////////////////////////////////////////////////////////////////
// CPosTextureAction

IMPLEMENT_DYNAMIC(CPosTextureAction, CPosAction)

CPosTextureAction::CPosTextureAction(int nType, UNITPOS Pos)
: CPosAction(nType)
{	
	m_Position		 = Pos;
	m_bUpdateTxtr	 = TRUE;
};
	
CPosTextureAction::~CPosTextureAction()
{	
};

// v�m�na hodnot m_CurValue a m_OldValue
BOOL CPosTextureAction::ChangeValues()
{
	int  pTemp = m_pCurValue;
	m_pCurValue = m_pOldValue;
	m_pOldValue = pTemp;
	return TRUE;
};

/////////////////////////////////////////////////////////////////////////////
// CPosTxtrAreaAction

/*IMPLEMENT_DYNAMIC(CPosTxtrAreaAction,CPosActionGroup)

CPosTxtrAreaAction::CPosTxtrAreaAction(int nType,const CPositionArea& Area):
	CPosActionGroup(nType)
{	// kop�ruji oblast
	m_Position.CopyFrom(&Area);
	m_bUpdateTxtr = TRUE;
};
CPosTxtrAreaAction::~CPosTxtrAreaAction()
{
	m_Position.ResetPosition();
};

// proveden� akce v dokumentu
BOOL CPosTxtrAreaAction::RealizeAction(CDocument* pDoc)
{
	// provedu d�l�� akce pro zm�nu textur
	BOOL bChange = CPosActionGroup::RealizeAction(pDoc);
	// provedu akci - reaguji na n� updatem plochy
	bChange |= CPosAction::RealizeAction(pDoc);
	return  bChange;
};*/

/////////////////////////////////////////////////////////////////////////////
// CPosTxtrAreaActionBase

IMPLEMENT_DYNAMIC(CPosTxtrAreaActionBase,CPosAction)

CPosTxtrAreaActionBase::CPosTxtrAreaActionBase(int nType,const CPositionArea& Area):
CPosAction(nType)
{	// kop�ruji oblast
  m_Position.CopyFrom(&Area);
  m_bUpdateTxtr = TRUE; 
};

/////////////////////////////////////////////////////////////////////////////
// CPosTxtrAreaAction
IMPLEMENT_DYNAMIC(CPosTxtrAreaAction,CPosTxtrAreaActionBase)

CPosTxtrAreaAction::CPosTxtrAreaAction(int nType,const CPositionArea& Area):
CPosTxtrAreaActionBase(nType, Area)
{
  m_pCurValue = NULL;
  m_pOldValue = NULL;
};

CPosTxtrAreaAction::~CPosTxtrAreaAction()
{
  delete m_pCurValue;
  delete m_pOldValue;
}

void CPosTxtrAreaAction::SetInMap(CPoseidonMap& map)
{ 
  // undo operation
  Ref<CTextureLayer> layer = map.GetTextureLayer(m_TextureLayerName);
  if (layer.IsNull())
  {
    ASSERT(FALSE);
    return;
  }

  TextureBank& textures = map.m_pDocument->m_Textures;
  UNITPOS pos;
  for(pos.z=m_LftTop.z; pos.z>=m_RghBtm.z; pos.z--)
    for(pos.x=m_LftTop.x; pos.x<=m_RghBtm.x; pos.x++)
    {
      if (layer->IsTextureUnitCenterAtArea(pos, &m_Position))
      {        
        int nTextID = GetUnitTexture(m_pCurValue, pos.x,pos.z);
        PTRTEXTURETMPLT text = textures.FindEx(nTextID);         
        layer->SetBaseTextureAt(pos, text);
      }
    }
}

BOOL CPosTxtrAreaAction::ChangeValues()
{
  int * temp = m_pCurValue;
  m_pCurValue = m_pOldValue;
  m_pOldValue = temp;

  return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CPosZoneTxtrAreaAction

IMPLEMENT_DYNAMIC(CPosZoneTxtrAreaAction,CPosTxtrAreaAction)

CPosZoneTxtrAreaAction::CPosZoneTxtrAreaAction(int nType,const CPositionArea& Area):
CPosTxtrAreaAction(nType, Area)
{
};

BOOL CPosZoneTxtrAreaAction::CreateActionData(const CPoseidonMap* pMap, const CPosTextureZone* pSource)
{
  if (m_pOldValue || m_pCurValue)
  {	// ji� bylo vytvo�eno v�e pot�ebn�
    ASSERT(FALSE);
    return FALSE;
  };

  // dimenze dat
  CRect	rLogHull;
  rLogHull = m_Position.GetPositionHull();
  pMap->GetTextureUnitPosHull(m_LftTop,m_RghBtm,rLogHull);
  m_nDimX = (m_RghBtm.x-m_LftTop.x)+1;
  m_nDimZ = (m_LftTop.z-m_RghBtm.z)+1;
  // chybn� dimenze ?
  if ((m_nDimX <= 0)||(m_nDimZ <= 0))
  {	
    ASSERT(FALSE);
    return FALSE;
  };
  // vytvo��m data
  m_pCurValue = new int[m_nDimX*m_nDimZ];
  m_pOldValue = new int[m_nDimX*m_nDimZ];
  if (m_pOldValue==NULL || m_pCurValue == NULL)
  {	// nedostatek pam�ti
    delete m_pOldValue;
    delete m_pCurValue;

    return FALSE;
  };

  // na�tu data z mapy	
  bool notLocked = false;
  for(int z=m_LftTop.z; z>=m_RghBtm.z; z--)
    for(int x=m_LftTop.x; x<=m_RghBtm.x; x++)
    {
      UNITPOS pos;
      pos.x = x; pos.z = z; 
      if (pMap->IsTextureUnitCenterAtArea(pos, &m_Position))
      {
        PTRTEXTURETMPLT text = pMap->GetBaseTextureAt(pos);
        SetUnitTexture(m_pOldValue, x, z,(text == NULL)? -1 : text->m_nTextureID);

        if (pMap->LockedTexture(pos))
        {
          // locked do not change
          SetUnitTexture(m_pCurValue, x, z,(text == NULL)? -1 : text->m_nTextureID);
        }
        else
        {
          notLocked = true; 

          PTRTEXTURETMPLT pTxtr = ((CPosTextureZone*)pSource)->GetRandomTexture();
          SetUnitTexture(m_pCurValue, x, z,(pTxtr == NULL)? -1 : pTxtr->m_nTextureID);
        }
      }
    }; 

  return notLocked;
}

/////////////////////////////////////////////////////////////////////////////
// CPosLandTxtrAreaAction

IMPLEMENT_DYNAMIC(CPosLandTxtrAreaAction,CPosTxtrAreaAction)

CPosLandTxtrAreaAction::CPosLandTxtrAreaAction(int nType,const CPositionArea& Area):
CPosTxtrAreaAction(nType, Area)
{
};

BOOL CPosLandTxtrAreaAction::CreateActionData(const CPoseidonMap* pMap, const CPosTextureLand* pSource, WORD flags)
{
  if (m_pOldValue || m_pCurValue)
  {	// ji� bylo vytvo�eno v�e pot�ebn�
    ASSERT(FALSE);
    return FALSE;
  };

  // dimenze dat
  CRect	rLogHull;
  rLogHull = m_Position.GetPositionHull();
  pMap->GetTextureUnitPosHull(m_LftTop,m_RghBtm,rLogHull);
  m_nDimX = (m_RghBtm.x-m_LftTop.x)+1;
  m_nDimZ = (m_LftTop.z-m_RghBtm.z)+1;
  // chybn� dimenze ?
  if ((m_nDimX <= 0)||(m_nDimZ <= 0))
  {	
    ASSERT(FALSE);
    return FALSE;
  };
  // vytvo��m data
  m_pCurValue = new int[m_nDimX*m_nDimZ];
  m_pOldValue = new int[m_nDimX*m_nDimZ];
  if (m_pOldValue==NULL || m_pCurValue == NULL)
  {	// nedostatek pam�ti
    delete m_pOldValue;
    delete m_pCurValue;
    return FALSE;
  };

  // na�tu data z mapy	
  bool notLocked = false;
  for(int z=m_LftTop.z; z>=m_RghBtm.z; z--)
    for(int x=m_LftTop.x; x<=m_RghBtm.x; x++)
    {
      UNITPOS pos;
      pos.x = x; pos.z = z; 
      if (pMap->IsTextureUnitCenterAtArea(pos,&m_Position))
      {
        PTRTEXTURETMPLT text = pMap->GetBaseTextureAt(pos);
        SetUnitTexture(m_pOldValue, x, z,(text == NULL)? -1 : text->m_nTextureID);

        if (pMap->LockedTexture(pos))
        {
          // locked texture -> do not change
          SetUnitTexture(m_pCurValue, x, z,(text == NULL)? -1 : text->m_nTextureID);
        }
        else
        {
          notLocked = true;

          // pr�m�rn� v��ka
          UNITPOS terrainPos;
          terrainPos.x = pos.x * pMap->GetActiveTextureLayer()->m_nTexSizeInSq;
          terrainPos.z = pos.z * pMap->GetActiveTextureLayer()->m_nTexSizeInSq;

          float nMaxGrad;
          float nHeight = pMap->GetBalancedHeight(terrainPos,&nMaxGrad);
          // je to mo�e ?
          BOOL bSea = FALSE;
          if (nHeight <= SEA_LAND_HEIGHT)
            bSea = TRUE;

          // um�st�m texturu
          if (bSea)
          {	// mohu modifikovat mo�e ?
            if (flags & TEXTURE_UNDER_SEA)
            {
              // PTRTEXTURETMPLT pTxtr = pDocument->m_PoseidonMap.GetStdSeaTexture();
              PTRTEXTURETMPLT pTxtr = ((CPosTextureLand*)pSource)->GetTextureForHeight(nHeight,nMaxGrad,(flags & TEXTURE_RANDOM));
              SetUnitTexture(m_pCurValue, x, z,(pTxtr == NULL)? -1 : pTxtr->m_nTextureID);
            }
          } 
          else
          {	// mohu modifikovat pevninu ?
            if (flags & TEXTURE_GROUND)
            {
              PTRTEXTURETMPLT pTxtr = ((CPosTextureLand*)pSource)->GetTextureForHeight(nHeight,nMaxGrad,(flags & TEXTURE_RANDOM));
              SetUnitTexture(m_pCurValue, x, z,(pTxtr == NULL)? -1 : pTxtr->m_nTextureID);
            }
          }
        }
      }
    }; 

  return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CPosTxtrAreaAction2

IMPLEMENT_DYNAMIC(CPosTxtrAreaAction2,CPosTxtrAreaActionBase)

CPosTxtrAreaAction2::CPosTxtrAreaAction2(int nType,const CPositionArea& Area):
CPosTxtrAreaActionBase(nType, Area)
{	// kop�ruji oblast
  m_bUndo = FALSE;
  m_pCurValue = -1;
  m_pOldValue = NULL;
};

CPosTxtrAreaAction2::~CPosTxtrAreaAction2()
{
  delete m_pOldValue;
  m_Position.ResetPosition();
};



BOOL CPosTxtrAreaAction2::CreateActionData(const CPoseidonMap* pMap, int newTextID)
{
  if (m_pOldValue)
  {	// ji� bylo vytvo�eno v�e pot�ebn�
    ASSERT(FALSE);
    return FALSE;
  };
  m_pCurValue = newTextID;

  // dimenze dat
  CRect	rLogHull;
  rLogHull = m_Position.GetPositionHull();
  pMap->GetTextureUnitPosHull(m_LftTop,m_RghBtm,rLogHull);
  m_nDimX = (m_RghBtm.x-m_LftTop.x)+1;
  m_nDimZ = (m_LftTop.z-m_RghBtm.z)+1;
  // chybn� dimenze ?
  if ((m_nDimX <= 0)||(m_nDimZ <= 0))
  {	
    ASSERT(FALSE);
    return FALSE;
  };
  // vytvo��m data
  //m_pCurValue = new int[m_nDimX*m_nDimZ];
  m_pOldValue = new int[m_nDimX*m_nDimZ];
  if (m_pOldValue==NULL)
  {	// nedostatek pam�ti
    return FALSE;
  };

  // na�tu data z mapy	
  bool notLocked = false;
  for(int z=m_LftTop.z; z>=m_RghBtm.z; z--)
    for(int x=m_LftTop.x; x<=m_RghBtm.x; x++)
    {
      UNITPOS pos;
      pos.x = x; pos.z = z; 
      if (pMap->LockedTexture(pos))
        SetUnitTexture(x, z, -2 ); // locked texture do not edit
      else
      {
        notLocked = true;
        PTRTEXTURETMPLT text = pMap->GetBaseTextureAt(pos);
        SetUnitTexture(x, z,(text == NULL)? -1 : text->m_nTextureID);
      }
    }; 
  return notLocked;
}

void CPosTxtrAreaAction2::SetInMap(CPoseidonMap& map)
{
  Ref<CTextureLayer> layer = map.GetTextureLayer(m_TextureLayerName);
  if (layer.IsNull())
  {
    ASSERT(FALSE);
    return;
  }

  TextureBank& textures = map.m_pDocument->m_Textures;
  if (m_bUndo)
  {
    // undo operation
    UNITPOS pos;
    for(pos.z=m_LftTop.z; pos.z>=m_RghBtm.z; pos.z--)
      for(pos.x=m_LftTop.x; pos.x<=m_RghBtm.x; pos.x++)
      {
        if (layer->IsTextureUnitCenterAtArea(pos, &m_Position))
        {        
          int nTextID = GetUnitTexture(pos.x,pos.z);
          if (nTextID != -2) // not locked
          {
            PTRTEXTURETMPLT text = textures.FindEx(nTextID);         
            layer->SetBaseTextureAt(pos, text);
          }
        }
      }
  }
  else
  {
    // redo operation
    PTRTEXTURETMPLT text = textures.FindEx(m_pCurValue);
    ASSERT(text.NotNull());
    UNITPOS pos;
    
    for( pos.z=m_LftTop.z; pos.z>=m_RghBtm.z; pos.z--)
      for( pos.x=m_LftTop.x; pos.x<=m_RghBtm.x; pos.x++)
      {        
        if(layer->IsTextureUnitCenterAtArea(pos, &m_Position))
        {        
          if (GetUnitTexture(pos.x,pos.z) != -2)
            layer->SetBaseTextureAt(pos, text);
        }
      }
  }
}


void CPosTxtrAreaAction2::SetUnitTexture(int x, int z, int nTextID) const
{
  ASSERT(m_pOldValue!=NULL);
  if ((x < m_LftTop.x)||(x > m_RghBtm.x))
  {
    ASSERT(FALSE);
    return;
  };
  if ((z < m_RghBtm.z)||(z > m_LftTop.z))
  {
    ASSERT(FALSE);
    return;
  };
  int nX   = x - m_LftTop.x;
  int nZ   = z - m_RghBtm.z;
  int nPos = nX+nZ*m_nDimX;
  m_pOldValue[nPos] = nTextID;
};

// nastaven� v��ky - m_pOldValue

BOOL CPosTxtrAreaAction2::ChangeValues()
{
  m_bUndo = !m_bUndo;
  return TRUE;
}

////////////////////////////////////////////////////////////////////////////
// CPosTxtrAreaAction3

IMPLEMENT_DYNAMIC(CPosChngTxtrAreaAction,CPosTxtrAreaActionBase)

CPosChngTxtrAreaAction::CPosChngTxtrAreaAction(int nType,const CPositionArea& Area):
CPosTxtrAreaActionBase(nType, Area)
{	// kop�ruji oblast
  m_bUndo = FALSE; 
  m_pOldValue = NULL;
};

CPosChngTxtrAreaAction::~CPosChngTxtrAreaAction()
{
  delete m_pOldValue;
  m_Position.ResetPosition();
};

BOOL CPosChngTxtrAreaAction::CreateActionData(const CPoseidonMap* pMap, int oldTextID, int newTextID)
{
  if (m_pOldValue)
  {	// ji� bylo vytvo�eno v�e pot�ebn�
    ASSERT(FALSE);
    return FALSE;
  };

  m_newTextID = newTextID;
  m_oldTextID = oldTextID;
  
  // dimenze dat
  CRect	rLogHull;
  rLogHull = m_Position.GetPositionHull();
  pMap->GetTextureUnitPosHull(m_LftTop,m_RghBtm,rLogHull);
  m_nDimX = (m_RghBtm.x-m_LftTop.x)+1;
  m_nDimZ = (m_LftTop.z-m_RghBtm.z)+1;
  // chybn� dimenze ?
  if ((m_nDimX <= 0)||(m_nDimZ <= 0))
  {	
    ASSERT(FALSE);
    return FALSE;
  };
  // vytvo��m data
  //m_pCurValue = new int[m_nDimX*m_nDimZ];
  m_pOldValue = new int[m_nDimX*m_nDimZ];
  if (m_pOldValue==NULL)
  {	// nedostatek pam�ti
    return FALSE;
  };

  // na�tu data z mapy	
  bool notLocked = false; 
  for(int z=m_LftTop.z; z>=m_RghBtm.z; z--)
    for(int x=m_LftTop.x; x<=m_RghBtm.x; x++)
    {
      UNITPOS pos;
      pos.x = x; pos.z = z; 
      PTRTEXTURETMPLT text = pMap->GetBaseTextureAt(pos);

      if (pMap->LockedTexture(pos))
      {
         SetUnitTexture(x, z, -2);
      }
      else
      {
        notLocked = true; 
        SetUnitTexture(x, z,(text == NULL)? -1 : text->m_nTextureID);
      }
    }; 
  return notLocked;
}

void CPosChngTxtrAreaAction::SetInMap(CPoseidonMap& map)
{
  Ref<CTextureLayer> layer = map.GetTextureLayer(m_TextureLayerName);
  if (layer.IsNull())
  {
    ASSERT(FALSE);
    return;
  }

  TextureBank& textures = map.m_pDocument->m_Textures;
  if (m_bUndo)
  {
    // undo operation
    UNITPOS pos;
    for(pos.z=m_LftTop.z; pos.z>=m_RghBtm.z; pos.z--)
      for(pos.x=m_LftTop.x; pos.x<=m_RghBtm.x; pos.x++)
      {
        if (layer->IsTextureUnitCenterAtArea(pos, &m_Position))
        {        
          int nTextID = GetUnitTexture(pos.x,pos.z);
          if (nTextID != -2) // not locked
          {
            PTRTEXTURETMPLT text = textures.FindEx(nTextID);              
            layer->SetBaseTextureAt(pos, text);
          }
        }
      }
  }
  else
  {
    // redo operation
    PTRTEXTURETMPLT text = textures.FindEx(m_newTextID);    
    ASSERT(text.NotNull());
    UNITPOS pos;

    for( pos.z=m_LftTop.z; pos.z>=m_RghBtm.z; pos.z--)
      for( pos.x=m_LftTop.x; pos.x<=m_RghBtm.x; pos.x++)
      {        
        if(layer->IsTextureUnitCenterAtArea(pos, &m_Position))
        {               
          int nTextID = GetUnitTexture(pos.x,pos.z);
          if (nTextID != -2)
          {
            if (nTextID == m_oldTextID)
            {
              layer->SetBaseTextureAt(pos, text);
            }
            else
            {          
              PTRTEXTURETMPLT text2 = textures.FindEx(nTextID);         
              layer->SetBaseTextureAt(pos, text2); 
            }
          }
        }
      }
  }
}


void CPosChngTxtrAreaAction::SetUnitTexture(int x, int z, int nTextID) const
{
#ifdef _DEBUG
  ASSERT(m_pOldValue!=NULL);
  if ((x < m_LftTop.x)||(x > m_RghBtm.x))
  {
    ASSERT(FALSE);
    return;
  };
  if ((z < m_RghBtm.z)||(z > m_LftTop.z))
  {
    ASSERT(FALSE);
    return;
  };
#endif

  int nX   = x - m_LftTop.x;\
  int nZ   = z - m_RghBtm.z;
  int nPos = nX+nZ*m_nDimX;
  m_pOldValue[nPos] = nTextID;
};

// nastaven� v��ky - m_pOldValue

BOOL CPosChngTxtrAreaAction::ChangeValues()
{
  m_bUndo = !m_bUndo;
  return TRUE;
}
//////////////////////////////////////////////////////////////////////////////
//

IMPLEMENT_DYNAMIC(CPosClipboardTxtrAreaAction,CPosTxtrAreaActionBase)

CPosClipboardTxtrAreaAction::CPosClipboardTxtrAreaAction(int nType,const CPositionArea& area) :
CPosTxtrAreaActionBase(nType, area)
{
  m_bUndo = FALSE; 
  m_pOldValue = NULL;
  m_pCurValue = NULL;
  m_numberOfElems = 0;
}

CPosClipboardTxtrAreaAction::~CPosClipboardTxtrAreaAction()
{
  delete [] m_pCurValue;
  delete [] m_pOldValue;
}

BOOL CPosClipboardTxtrAreaAction::CreateActionData(const CPoseidonMap* pMap, PosClipBoardTexture * data, const REALPOS& cursorReal)
{
  if (m_pOldValue)
  {	// ji� bylo vytvo�eno v�e pot�ebn�
    ASSERT(FALSE);
    return FALSE;
  };

  // dimenze dat
  CRect	rLogHull;
  rLogHull = m_Position.GetPositionHull();
  pMap->GetTextureUnitPosHull(m_LftTop,m_RghBtm,rLogHull);
  m_nDimX = (m_RghBtm.x-m_LftTop.x)+1;
  m_nDimZ = (m_LftTop.z-m_RghBtm.z)+1;
  // chybn� dimenze ?
  if ((m_nDimX <= 0)||(m_nDimZ <= 0))
  {	
    ASSERT(FALSE);
    return FALSE;
  };
  // vytvo��m data  
  m_numberOfElems = data->numberOfElems;
  m_pOldValue = new int[m_nDimX*m_nDimZ];
  m_pCurValue = new TextureElem[m_numberOfElems];
  if (m_pOldValue == NULL || m_pCurValue == NULL)
  {
  	// nedostatek pam�ti
    delete [] m_pOldValue;
    delete [] m_pCurValue;
    return FALSE;  
  }

  // na�tu data z mapy	
  bool notLocked = false;
  for(int z=m_LftTop.z; z>=m_RghBtm.z; z--) for(int x=m_LftTop.x; x<=m_RghBtm.x; x++)
  {
    UNITPOS pos;
    pos.x = x; pos.z = z; 
    if (pMap->LockedTexture(pos))
      SetUnitTexture(x, z, -2);
    else
    {
      notLocked = true; 
      PTRTEXTURETMPLT text = pMap->GetBaseTextureAt(pos);
      SetUnitTexture(x, z,(text == NULL)? -1 : text->m_nTextureID);
    }
  }; 

  memcpy(m_pCurValue, data->data, m_numberOfElems * sizeof(*m_pCurValue));
  REALPOS diff;
  diff.x = -data->rectreal[0] + cursorReal.x;
  diff.z = -data->rectreal[3] + cursorReal.z;

  for(int i = 0; i < m_numberOfElems; i++)
  {
    m_pCurValue[i].rectreal[0] += diff.x;
    m_pCurValue[i].rectreal[1] += diff.z;
    m_pCurValue[i].rectreal[2] += diff.x;
    m_pCurValue[i].rectreal[3] += diff.z;
  }
  return TRUE;
}

void CPosClipboardTxtrAreaAction::SetInMap(CPoseidonMap& map)
{
  Ref<CTextureLayer> layer = map.GetTextureLayer(m_TextureLayerName);
  if (layer.IsNull())
  {
    ASSERT(FALSE);
    return;
  }

  TextureBank& textures = map.m_pDocument->m_Textures;
  if (m_bUndo)
  {
    // undo operation
    UNITPOS pos;
    for(pos.z=m_LftTop.z; pos.z>=m_RghBtm.z; pos.z--) for(pos.x=m_LftTop.x; pos.x<=m_RghBtm.x; pos.x++)
    {                
      int nTextID = GetUnitTexture(pos.x,pos.z);
      if (nTextID != -2) // not locked
      {
        PTRTEXTURETMPLT text = textures.FindEx(nTextID);         
        layer->SetBaseTextureAt(pos, text);        
      }
    }
  }
  else
  {
    // redo operation
    for(int i = 0; i < m_numberOfElems ; i++)
    {
      TextureElem& elem = m_pCurValue[i];
      FltRect realRect;
      realRect[0] = elem.rectreal[0];
      realRect[1] = elem.rectreal[1];
      realRect[2] = elem.rectreal[2];
      realRect[3] = elem.rectreal[3];
      CRect logRect = map.FromRealToViewRect(realRect);
      UNITPOS LftTop, RghBtm;
      map.GetTextureUnitPosHull(LftTop,RghBtm,logRect);

      PTRTEXTURETMPLT text = textures.FindEx(elem.textID); 

      UNITPOS pos;
      for( pos.z=LftTop.z; pos.z>=RghBtm.z; pos.z--) for( pos.x=LftTop.x; pos.x<=RghBtm.x; pos.x++)  
      {
        if (GetUnitTexture(pos.x,pos.z) != -2) // not locked
          layer->SetBaseTextureAt(pos, text);   
      }
    }
  }
}

void CPosClipboardTxtrAreaAction::SetUnitTexture(int x, int z, int nTextID) const
{
#ifdef _DEBUG
  ASSERT(m_pOldValue!=NULL);
  if ((x < m_LftTop.x)||(x > m_RghBtm.x))
  {
    ASSERT(FALSE);
    return;
  };
  if ((z < m_RghBtm.z)||(z > m_LftTop.z))
  {
    ASSERT(FALSE);
    return;
  };
#endif

  int nX   = x - m_LftTop.x;
  int nZ   = z - m_RghBtm.z;
  int nPos = nX+nZ*m_nDimX;
  m_pOldValue[nPos] = nTextID;
};

BOOL CPosClipboardTxtrAreaAction::ChangeValues()
{
  m_bUndo = !m_bUndo;
  return TRUE;
}
//////////////////////////////////////////////////////////////////////////////
//
IMPLEMENT_DYNAMIC(CPosTxtrAreaLockAction, CPosTxtrAreaActionBase)

void CPosTxtrAreaLockAction::SetInMap(CPoseidonMap& map)
{
  Ref<CTextureLayer> layer = map.GetTextureLayer(m_TextureLayerName);
  if (layer.IsNull())
  {
    ASSERT(FALSE);
    return;
  }

  UNITPOS pos;
  for(pos.z=m_LftTop.z; pos.z>=m_RghBtm.z; pos.z--)
    for(pos.x=m_LftTop.x; pos.x<=m_RghBtm.x; pos.x++)
    {
      if(layer->IsTextureUnitCenterAtArea(pos, &m_Position))
      {
        layer->Lock(pos, _lock);
      }
    }
}

BOOL CPosTxtrAreaLockAction::CreateActionData(const CPoseidonMap& map, bool lock )
{
  // dimenze dat
  CRect	rLogHull;
  rLogHull = m_Position.GetPositionHull();
  map.GetTextureUnitPosHull(m_LftTop,m_RghBtm,rLogHull);
  m_nDimX = (m_RghBtm.x-m_LftTop.x)+1;
  m_nDimZ = (m_LftTop.z-m_RghBtm.z)+1;
  // chybn� dimenze ?
  if ((m_nDimX <= 0)||(m_nDimZ <= 0))
  {	
    ASSERT(FALSE);
    return FALSE;
  };

  Ref<CTextureLayer> layer = map.GetTextureLayer(m_TextureLayerName);
  if (layer.IsNull())
  {
    ASSERT(FALSE);
    return FALSE;
  }
 
  // na�tu data z mapy	
  _lock = lock; 
  for(int z=m_LftTop.z; z>=m_RghBtm.z; z--)
    for(int x=m_LftTop.x; x<=m_RghBtm.x; x++)
    {
      UNITPOS pos;
      pos.x = x; pos.z = z; 
      if(layer->IsTextureUnitCenterAtArea(pos, &m_Position) && _lock != map.LockedTexture(pos))        
        return TRUE;
    }; 
  return FALSE;
};

/////////////////////////////////////////////////////////////////////////////
// Zm�na v��ky v bloku

IMPLEMENT_DYNAMIC(CPosHeightAction,CPosAction)

CPosHeightAction::CPosHeightAction(int nType,const CPositionArea& Area):
	CPosAction(nType)
{
	m_Position.CopyFrom(&Area);
	m_nDimX = 
	m_nDimZ = 0;

	m_pCurValue =
	m_pOldValue = NULL;
};
CPosHeightAction::~CPosHeightAction()
{
	m_Position.ResetPosition();
	if (m_pCurValue)
		delete [] m_pCurValue;
	if (m_pOldValue)
		delete [] m_pOldValue;
};

// v�m�na hodnot m_CurValue a m_OldValue
BOOL CPosHeightAction::ChangeValues()
{
	if ((m_pCurValue)&&(m_pOldValue))
	{	
		int nDim = m_nDimX*m_nDimZ;
		for(int i=0; i<nDim; i++)
		{
			LANDHEIGHT nTemp = m_pCurValue[i];
			m_pCurValue[i]   = m_pOldValue[i];
			m_pOldValue[i]	 = nTemp;
		}
	};
	return TRUE;
};

// podpora pro pr�ci s blokem v��ek
BOOL CPosHeightAction::CreateActionData(const CPoseidonMap* pMap)
{
	if ((m_pCurValue)||(m_pOldValue))
	{	// ji� bylo vytvo�eno v�e pot�ebn�
		ASSERT(FALSE);
		return FALSE;
	};
	// dimenze dat
	CRect	rLogHull;
	rLogHull = m_Position.GetPositionHull();
	pMap->GetUnitPosHull(m_LftTop,m_RghBtm,rLogHull);
	m_nDimX = (m_RghBtm.x-m_LftTop.x)+1;
	m_nDimZ = (m_LftTop.z-m_RghBtm.z)+1;
	// chybn� dimenze ?
	if ((m_nDimX <= 0)||(m_nDimZ <= 0))
	{	
		ASSERT(FALSE);
		return FALSE;
	};
	// vytvo��m data
	m_pCurValue = new LANDHEIGHT[m_nDimX*m_nDimZ];
	m_pOldValue = new LANDHEIGHT[m_nDimX*m_nDimZ];
	if ((m_pCurValue==NULL)||(m_pOldValue==NULL))
	{	// nedostatek pam�ti		
		return FALSE;
	};
	// na�tu data z mapy	
	for(int z=m_LftTop.z; z>=m_RghBtm.z; z--)
	for(int x=m_LftTop.x; x<=m_RghBtm.x; x++)
	{
		SetUnitLandHeight(x, z, pMap->GetLandscapeHeight(x,z));
	};
	// kp�ruji do Current
	int nDim = m_nDimX*m_nDimZ;
	for(int i=0; i<nDim; i++)
		m_pCurValue[i]   = m_pOldValue[i];
	return TRUE;
};

// nastaven� v��ky - m_pOldValue
LANDHEIGHT CPosHeightAction::GetUnitLandHeight(int x, int z) const
{
	ASSERT(m_pOldValue!=NULL);
	if ((x < m_LftTop.x)||(x > m_RghBtm.x))
	{
		ASSERT(FALSE);
		return 0.;
	};
	if ((z < m_RghBtm.z)||(z > m_LftTop.z))
	{
		ASSERT(FALSE);
		return 0.;
	};
	int nX   = x - m_LftTop.x;
	int nZ   = z - m_RghBtm.z;
	int nPos = nX+nZ*m_nDimX;
	return m_pOldValue[nPos];
};
void CPosHeightAction::SetUnitLandHeight(int x, int z, LANDHEIGHT nHeight) const
{
	ASSERT(m_pOldValue!=NULL);
	if ((x < m_LftTop.x)||(x > m_RghBtm.x))
	{
		ASSERT(FALSE);
		return;
	};
	if ((z < m_RghBtm.z)||(z > m_LftTop.z))
	{
		ASSERT(FALSE);
		return;
	};
	int nX   = x - m_LftTop.x;
	int nZ   = z - m_RghBtm.z;
	int nPos = nX+nZ*m_nDimX;
	m_pOldValue[nPos] = nHeight;
};

/////////////////////////////////////////////////////////////////////////////
// Lock vertexes
IMPLEMENT_DYNAMIC(CPosHeightLockAction, CPosTxtrAreaActionBase)

void CPosHeightLockAction::SetInMap(CPoseidonMap& map)
{ 
  UNITPOS pos;
  for(pos.z=m_LftTop.z; pos.z>=m_RghBtm.z; pos.z--)
    for(pos.x=m_LftTop.x; pos.x<=m_RghBtm.x; pos.x++)
    {
      if(m_Position.IsPointAtArea(map.FromUnitToViewPos(pos)))
      {
        map.LockVertex(pos, _lock);
      }
    }
}

BOOL CPosHeightLockAction::CreateActionData(const CPoseidonMap& map, bool lock )
{
  // dimenze dat
  CRect	rLogHull;
  rLogHull = m_Position.GetPositionHull();
  map.GetUnitPosHull(m_LftTop,m_RghBtm,rLogHull);  

  // na�tu data z mapy	
  _lock = lock; 
  for(int z=m_LftTop.z; z>=m_RghBtm.z; z--)
    for(int x=m_LftTop.x; x<=m_RghBtm.x; x++)
    {
      UNITPOS pos;
      pos.x = x; pos.z = z; 
      if(m_Position.IsPointAtArea(map.FromUnitToViewPos(pos)) && _lock != map.LockedVertex(pos))
        return TRUE;
    }; 

  return FALSE;
}


