/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"
#include "AutoNetGener.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef Fail
#undef Fail
#endif

/////////////////////////////////////////////////////////////////////////////
// CCreateNetPage0 dialog

class CCreateNetPage0 : public CMntPropertyPage
{
	// Construction
public:
	CCreateNetPage0();
	~CCreateNetPage0();

	// Dialog Data
	CNetPartKEY*     m_pKeyObj;
	CPosNetTemplate* m_pNet;

	//{{AFX_DATA(CCreateNetPage0)
	enum { IDD = IDD_CREATE_NET_PAGE0 };
	//}}AFX_DATA

	// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CCreateNetPage0)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CCreateNetPage0)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CCreateNetPage0::CCreateNetPage0() 
: CMntPropertyPage(CCreateNetPage0::IDD)
{
	m_pKeyObj	= NULL;
	m_pNet		= NULL;
	//{{AFX_DATA_INIT(CCreateNetPage0)
	//}}AFX_DATA_INIT
}

CCreateNetPage0::~CCreateNetPage0()
{
}

BOOL CCreateNetPage0::OnInitDialog() 
{
	CMntPropertyPage::OnInitDialog();
	// typ kl��ov�ho �seku
	if (m_pNet != NULL)
	{
		::SetWindowText(DLGCTRL(IDC_NET_NAME1), m_pNet->m_sNetName);
		::SetWindowText(DLGCTRL(IDC_NET_NAME2), m_pNet->m_sNetName);
		::SetWindowText(DLGCTRL(IDC_NET_NAME3), m_pNet->m_sNetName);
	} 
	else
	{
		::ShowWindow(DLGCTRL(IDC_NET_NAME1), SW_HIDE);
		::ShowWindow(DLGCTRL(IDC_NET_NAME2), SW_HIDE);
		::ShowWindow(DLGCTRL(IDC_NET_NAME3), SW_HIDE);
		::EnableWindow(DLGCTRL(IDC_TYPE_STRA), FALSE);
		::EnableWindow(DLGCTRL(IDC_TYPE_SPEC), FALSE);
		::EnableWindow(DLGCTRL(IDC_TYPE_TERM), FALSE);
	}
	return TRUE;
}

void CCreateNetPage0::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCreateNetPage0)
	//}}AFX_DATA_MAP
	DDX_Radio(pDX, IDC_TYPE_CROSS, m_pKeyObj->m_nKeyPartType);
}

BEGIN_MESSAGE_MAP(CCreateNetPage0, CMntPropertyPage)
	//{{AFX_MSG_MAP(CCreateNetPage0)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCreateNetPage1 dialog

class CCreateNetPage1 : public CMntPropertyPage
{
	// Construction
public:
	CCreateNetPage1();
	~CCreateNetPage1();

	// Dialog Data
	CNetPartKEY*     m_pKeyObj;
	CPosNetTemplate* m_pNet;
	CMgrNets*        m_pMgrNets;
	int              m_nLastType;
	BOOL             m_bVerifyCrossNet;
	BOOL             m_bVerifySaveData;

	//{{AFX_DATA(CCreateNetPage1)
	enum { IDD = IDD_CREATE_NET_PAGE1 };
	CListBox m_cList;
	//}}AFX_DATA

	// Overrides
	void OnUpdateType();
	BOOL OnCreateObject(CObject*);

	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CCreateNetPage1)
public:
	virtual BOOL OnSetActive();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	virtual LRESULT OnWizardBack();
	virtual LRESULT OnWizardNext();

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CCreateNetPage1)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CCreateNetPage1::CCreateNetPage1() 
: CMntPropertyPage(CCreateNetPage1::IDD)
{
	m_nLastType = -1;

	m_pKeyObj	= NULL;
	m_pNet		= NULL;
	m_pMgrNets	= NULL;

	m_bVerifyCrossNet = FALSE;
	m_bVerifySaveData = TRUE;
	//{{AFX_DATA_INIT(CCreateNetPage1)
	//}}AFX_DATA_INIT
}

CCreateNetPage1::~CCreateNetPage1()
{
}

void CCreateNetPage1::OnUpdateType()
{
	CString sTitle;
	// reset seznamu
	m_cList.ResetContent();
	// typ objektu
	m_nLastType = m_pKeyObj->m_nKeyPartType;
	switch(m_nLastType)
	{
	case CNetPartKEY::typeCross:
		{
			sTitle.LoadString(IDS_KEY_OBJ_TYPE_CROSS);
			for (int i = 0; i < m_pMgrNets->m_CrossTemplates.GetSize(); ++i)
			{
				CPosCrossTemplate* pNet = (CPosCrossTemplate*)(m_pMgrNets->m_CrossTemplates[i]);
				LBAddString(m_cList.m_hWnd, pNet->m_sCrossName, (DWORD)pNet);
			}
		}  
		break;
	case CNetPartKEY::typeStra:
		{
			ASSERT(m_pNet != NULL);
			AfxFormatString1(sTitle, IDS_KEY_OBJ_TYPE_STRA, m_pNet->m_sNetName);
			for (int i = 0; i < m_pNet->m_NetSTRA.GetSize(); ++i)
			{
				CNetComponent* pObj = (CNetComponent*)(m_pNet->m_NetSTRA[i]);
				if (pObj)
				{
					LBAddString(m_cList.m_hWnd, pObj->m_sCompName, (DWORD)pObj);
				}
			}
		}	
		break;
	case CNetPartKEY::typeSpec:
		{
			ASSERT(m_pNet != NULL);
			AfxFormatString1(sTitle, IDS_KEY_OBJ_TYPE_SPEC, m_pNet->m_sNetName);
			for (int i = 0; i < m_pNet->m_NetSPEC.GetSize(); ++i)
			{
				CNetComponent* pObj = (CNetComponent*)(m_pNet->m_NetSPEC[i]);
				if (pObj)
				{
					LBAddString(m_cList.m_hWnd, pObj->m_sCompName, (DWORD)pObj);
				}
			}
		}  
		break;
	case CNetPartKEY::typeTerm:
		{
			ASSERT(m_pNet != NULL);
			AfxFormatString1(sTitle, IDS_KEY_OBJ_TYPE_TERM, m_pNet->m_sNetName);
			for (int i = 0; i < m_pNet->m_NetTERM.GetSize(); ++i)
			{
				CNetComponent* pObj = (CNetComponent*)(m_pNet->m_NetTERM[i]);
				if (pObj)
				{
					LBAddString(m_cList.m_hWnd, pObj->m_sCompName, (DWORD)pObj);
				}
			}
		}  
		break;
	default:
		ASSERT(FALSE);
	}
	// v�b�r prvku
	if (m_cList.GetCount() > 0) m_cList.SetCurSel(0);
	// nastaven� titulku
	::SetWindowText(DLGCTRL(IDC_OBJECT_TYPE), sTitle);
}

BOOL CCreateNetPage1::OnCreateObject(CObject* pSource)
{
	switch(m_nLastType)
	{
	case CNetPartKEY::typeCross:
		{	// vytvo��m podle k�i�ovatky
			ASSERT_KINDOF(CPosCrossTemplate, pSource);
			if (m_bVerifyCrossNet && (m_pNet != NULL))
			{	// k�i�ovatka mus� m�t �sek dan�ho typu
				BOOL bFound = FALSE;
				for (int i = 0; i < CPosCrossTemplate::netCROSS_Parts; ++i)
				{
					CPosNetTemplate* pCrossNet = ((CPosCrossTemplate*)pSource)->GetPartNetTemplate((WORD)i,m_pMgrNets);
					if (pCrossNet != NULL)
					{	
						if (lstrcmp(pCrossNet->m_sNetName, m_pNet->m_sNetName) == 0)
						{
							bFound = TRUE;
							break;
						}
					}
				}
				if (!bFound)
				{	// k�i�ovatka nem� ��dn� �sek dan�ho typu s�t�
					AfxMessageBox(IDS_CROSS_HAS_NOT_NET_TYPE, MB_OK | MB_ICONEXCLAMATION);
					return FALSE;
				}
			}
			return m_pKeyObj->CreateFromTemplateCross((CPosCrossTemplate*)pSource);
		}  
	case CNetPartKEY::typeStra:
		{	// vytvo��m podle rovinky
			ASSERT_KINDOF(CNetSTRA, pSource);
			return m_pKeyObj->CreateFromTemplateStra(m_pNet, (CNetSTRA*)pSource);
		}
	case CNetPartKEY::typeSpec:
		{	// vytvo��m podle spec. d�lu
			ASSERT_KINDOF(CNetSPEC, pSource);
			return m_pKeyObj->CreateFromTemplateSpec(m_pNet, (CNetSPEC*)pSource);
		}  
	case CNetPartKEY::typeTerm:
		{	// vytvo��m podle koncov�ho d�lu
			ASSERT_KINDOF(CNetTERM, pSource);
			return m_pKeyObj->CreateFromTemplateTerm(m_pNet, (CNetTERM*)pSource);
		}
	}
	return FALSE;
}

BOOL CCreateNetPage1::OnInitDialog() 
{
	CMntPropertyPage::OnInitDialog();
	m_bVerifySaveData = TRUE;
	// update typu
	OnUpdateType();
	return TRUE;
}

void CCreateNetPage1::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCreateNetPage1)
	DDX_Control(pDX, IDC_LIST, m_cList);
	//}}AFX_DATA_MAP
	if (SAVEDATA)
	{
		if (m_bVerifySaveData) MDDV_LBValidSelItem(pDX, IDC_LIST, IDS_NOT_SEL_KEY_NET_OBJ);
		// vytvo�en� objektu
		DWORD dwData;
		if (LBGetCurSelData(m_cList.m_hWnd, dwData))
		{
			CObject* pObj = (CObject*)dwData;
			if (pObj != NULL)
			{
				if (!OnCreateObject(pObj))
				{
					AfxMessageBox(IDS_NO_VALID_KEY_CREATE, MB_OK | MB_ICONEXCLAMATION);
					pDX->Fail();
				}
			}
		}
	}
}

LRESULT CCreateNetPage1::OnWizardBack()
{
	m_bVerifySaveData = FALSE;
	return CMntPropertyPage::OnWizardBack();
}

LRESULT CCreateNetPage1::OnWizardNext()
{
	m_bVerifySaveData = TRUE;
	return CMntPropertyPage::OnWizardNext();
}

BEGIN_MESSAGE_MAP(CCreateNetPage1, CMntPropertyPage)
	//{{AFX_MSG_MAP(CCreateNetPage1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CCreateNetPage1::OnSetActive() 
{
	if (m_nLastType != m_pKeyObj->m_nKeyPartType) OnUpdateType();
	return CMntPropertyPage::OnSetActive();
}

/////////////////////////////////////////////////////////////////////////////
// CCreateNetPage2 dialog

class CCreateNetPage2 : public CMntPropertyPage
{
	// Construction
public:
	CCreateNetPage2();
	~CCreateNetPage2();

	// Dialog Data
	//{{AFX_DATA(CCreateNetPage2)
	enum { IDD = IDD_CREATE_NET_PAGE2 };
	CComboBox m_cOrientation;
	//}}AFX_DATA

	// Overrides
	CNetPartKEY*     m_pKeyObj;
	CPosNetTemplate* m_pNet;
	CMgrNets*        m_pMgrNets;
	CPoseidonMap*    m_pMap;

	// ClassWizard generate virtual function overrides

	void OnUpdateShowData();

	//{{AFX_VIRTUAL(CCreateNetPage2)
public:
	virtual BOOL OnSetActive();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CCreateNetPage2)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CCreateNetPage2::CCreateNetPage2() 
: CMntPropertyPage(CCreateNetPage2::IDD)
{
	//{{AFX_DATA_INIT(CCreateNetPage2)
	//}}AFX_DATA_INIT
}

CCreateNetPage2::~CCreateNetPage2()
{
}

void CCreateNetPage2::OnUpdateShowData()
{
	CString sTitle;
	// typ objektu
	switch(m_pKeyObj->m_nKeyPartType)
	{
	case CNetPartKEY::typeCross:
		{
			sTitle.LoadString(IDS_KEY_OBJ_TYPE_CROSS);
		}  
		break;
	case CNetPartKEY::typeStra:
		{
			ASSERT(m_pNet != NULL);
			AfxFormatString1(sTitle, IDS_KEY_OBJ_TYPE_STRA, m_pNet->m_sNetName);
		}
		break;
	case CNetPartKEY::typeSpec:
		{
			ASSERT(m_pNet != NULL);
			AfxFormatString1(sTitle, IDS_KEY_OBJ_TYPE_SPEC, m_pNet->m_sNetName);
		}
		break;
	case CNetPartKEY::typeTerm:
		{
			ASSERT(m_pNet != NULL);
			AfxFormatString1(sTitle, IDS_KEY_OBJ_TYPE_TERM, m_pNet->m_sNetName);
		}
		break;
	default:
		ASSERT(FALSE);
	}
	// nastaven� titulku
	::SetWindowText(DLGCTRL(IDC_OBJECT_TYPE), sTitle);
	::SetWindowText(DLGCTRL(IDC_OBJECT_NAME), m_pKeyObj->m_nComponentName);
}

void CCreateNetPage2::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCreateNetPage2)
	DDX_Control(pDX, IDC_ORIENTATION, m_cOrientation);
	//}}AFX_DATA_MAP

	if (!SAVEDATA)
	{	// update viditeln�ch dat
		OnUpdateShowData();
		// orientace
		// new code
		//*********************************************
		if (m_pKeyObj->m_nBaseRealPos.nGra == 90.0)
		{
			m_cOrientation.SetCurSel(1); 
		}
		else if (m_pKeyObj->m_nBaseRealPos.nGra == 180.0)
		{
			m_cOrientation.SetCurSel(2); 
		}
		else if (m_pKeyObj->m_nBaseRealPos.nGra == 270.0)
		{
			m_cOrientation.SetCurSel(3); 
		}
		else
		{
			m_cOrientation.SetCurSel(0);
		}
		//*********************************************

		// old code
		//*********************************************
		/*
		switch(m_pKeyObj->m_nBaseRealPos.nGra)
		{
		case 90:  m_cOrientation.SetCurSel(1); break;
		case 180: m_cOrientation.SetCurSel(2); break;
		case 270: m_cOrientation.SetCurSel(3); break;
		default:  m_cOrientation.SetCurSel(0);
		}
		*/
		//*********************************************
	} 
	else
	{	// orientace
		switch(m_cOrientation.GetCurSel())
		{
		case 1: m_pKeyObj->m_nBaseRealPos.nGra = 90;  break;
		case 2: m_pKeyObj->m_nBaseRealPos.nGra = 180; break;
		case 3: m_pKeyObj->m_nBaseRealPos.nGra = 270; break;
		default: m_pKeyObj->m_nBaseRealPos.nGra = 0;
		}
	}
	// min. a max zabo�en�
//	double nMin,nMax,nVal = Round(m_pKeyObj->m_nRelaHeight,3);
//	if (m_pKeyObj->m_nKeyPartType == CNetPartKEY::typeCross)
//	{	// podle k�i�ovatky
//		CPosCrossTemplate* pCross = (CPosCrossTemplate*)(m_pMgrNets->GetCrossTemplate(m_pKeyObj->m_nComponentName));
//		ASSERT(pCross != NULL);
//		ASSERT_KINDOF(CPosCrossTemplate,pCross);
//		nMin = pCross->m_nMinHeight;
//		nMax = pCross->m_nMaxHeight;
//	} else
//	{	// podle s�t�
//		ASSERT(m_pNet != NULL);
//		nMin = m_pNet->m_nMinHeight;
//		nMax = m_pNet->m_nMaxHeight;
//	};
//	MDDX_Text(pDX, IDC_EDIT_POSY, nVal);
//	MDDV_MinMaxDouble(pDX, nVal, nMin, nMax, IDC_EDIT_POSY, 0.1);
//	if (SAVEDATA)
//	{
//		m_pKeyObj->m_nRelaHeight = (float)nVal;
//	};

	// poloha objektu
	double nMaxX = (float)(m_pMap->GetSize().x * m_pMap->GetDocument()->m_nRealSizeUnit);
	double nMaxZ = (float)(m_pMap->GetSize().z * m_pMap->GetDocument()->m_nRealSizeUnit);
	double nVal = Round(m_pKeyObj->m_nBaseRealPos.nPos.x, 3);
	MDDX_Text(pDX, IDC_EDIT_POSX, nVal);
	MDDV_MinMaxDouble(pDX, nVal, 0., nMaxX, IDC_EDIT_POSX, NET_ALIGN_GRID);
	if (SAVEDATA) m_pKeyObj->m_nBaseRealPos.nPos.x = (float)nVal;
	nVal = Round(m_pKeyObj->m_nBaseRealPos.nPos.z, 3);
	MDDX_Text(pDX, IDC_EDIT_POSZ, nVal);
	MDDV_MinMaxDouble(pDX, nVal, 0., nMaxZ, IDC_EDIT_POSZ, NET_ALIGN_GRID);
	if (SAVEDATA)
	{
		m_pKeyObj->m_nBaseRealPos.nPos.z = (float)nVal;
	}
}

BEGIN_MESSAGE_MAP(CCreateNetPage2, CMntPropertyPage)
	//{{AFX_MSG_MAP(CCreateNetPage2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CCreateNetPage2::OnSetActive() 
{
	UpdateData(FALSE);
	return CMntPropertyPage::OnSetActive();
}

/////////////////////////////////////////////////////////////////////////////
// CCreateNetPage2_2 dialog

class CCreateNetPage2_2 : public CMntPropertyPage
{
	// Construction
public:
	CCreateNetPage2_2();
	~CCreateNetPage2_2();

	// Dialog Data
	CNetPartKEY*     m_pKeyObj;
	CPosNetTemplate* m_pNet;
	CMgrNets*        m_pMgrNets;
	CPoseidonMap*    m_pMap;

	CShowNetKeyCtrl  m_cShowType;

	//{{AFX_DATA(CCreateNetPage2_2)
	enum { IDD = IDD_CREATE_NET_PAGE2_2 };
	//}}AFX_DATA
	BOOL       m_bValidButton[CNetPartKEY::maxSections];
	int        m_nConnectPart;
	REALNETPOS m_nConnectPos;

	// Overrides
	void OnUpdateShowData();
	void UpdateValidButtons();

	// p�ipojen�� k dan�mu �seku
	void DoConnectNetPart(int nSection);

	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CCreateNetPage2_2)
	public:
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CCreateNetPage2_2)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CCreateNetPage2_2::CCreateNetPage2_2() 
: CMntPropertyPage(CCreateNetPage2_2::IDD)
{
	m_nConnectPart = 0;	// def. p�ipojeno na A

	//{{AFX_DATA_INIT(CCreateNetPage2_2)
	//}}AFX_DATA_INIT
}

CCreateNetPage2_2::~CCreateNetPage2_2()
{
}

void CCreateNetPage2_2::OnUpdateShowData()
{
	CString sTitle;
	// typ objektu
	switch(m_pKeyObj->m_nKeyPartType)
	{
	case CNetPartKEY::typeCross:
		{
			sTitle.LoadString(IDS_KEY_OBJ_TYPE_CROSS);
		}
		break;
	case CNetPartKEY::typeStra:
		{
			ASSERT(m_pNet != NULL);
			AfxFormatString1(sTitle, IDS_KEY_OBJ_TYPE_STRA, m_pNet->m_sNetName);
		}
		break;
	case CNetPartKEY::typeSpec:
		{
			ASSERT(m_pNet != NULL);
			AfxFormatString1(sTitle, IDS_KEY_OBJ_TYPE_SPEC, m_pNet->m_sNetName);
		}
		break;
	case CNetPartKEY::typeTerm:
		{
			ASSERT(m_pNet != NULL);
			AfxFormatString1(sTitle, IDS_KEY_OBJ_TYPE_TERM, m_pNet->m_sNetName);
		}
		break;
	default:
		ASSERT(FALSE);
	}
	// nastaven� titulku
	::SetWindowText(DLGCTRL(IDC_OBJECT_TYPE), sTitle);
	::SetWindowText(DLGCTRL(IDC_OBJECT_NAME), m_pKeyObj->m_nComponentName);
}

static int CtrlButtons[] =
{	
	IDC_RADIO_LIST_A, 
	IDC_RADIO_LIST_B, 
	IDC_RADIO_LIST_C, 
	IDC_RADIO_LIST_D, 
};

void CCreateNetPage2_2::UpdateValidButtons()
{
	int i;
	// reset validace tla��tek
	for (i = 0; i < CNetPartKEY::maxSections; ++i)
	{
		m_bValidButton[i] = FALSE;
	}
	// validace tla��tek dle typu
	for (i = 0; i < CNetPartKEY::maxSections; ++i)
	{
		m_bValidButton[i] = (i < m_pKeyObj->m_nCountSections);
		// viditelnost tla��tka
		::EnableWindow(DLGCTRL(CtrlButtons[i]), TRUE);
		::ShowWindow(DLGCTRL(CtrlButtons[i]), (m_bValidButton[i]) ? SW_SHOW : SW_HIDE);
	}
	// z�m�na "C" za "D"
	if ((m_pKeyObj->m_nKeyPartType == CNetPartKEY::typeCross) &&
		(m_pKeyObj->m_nKeyPartSubtype == CPosCrossTemplate::netCROSS_P))
	{
		ASSERT(m_pKeyObj->m_nCountSections == 3);
		TCHAR buff[100];
		::GetWindowText(DLGCTRL(IDC_RADIO_LIST_D), buff, 99);
		::SetWindowText(DLGCTRL(IDC_RADIO_LIST_C), buff);
	}
	// validace typu s�t�
	for (i = 0; i < CNetPartKEY::maxSections; ++i)
	{
		if (m_bValidButton[i])
		{
			if (lstrcmp(m_pKeyObj->m_SectionNetType[i], m_pNet->m_sNetName) != 0)
			{	// jin� s�
				m_bValidButton[i] = FALSE;
				::EnableWindow(DLGCTRL(CtrlButtons[i]), FALSE);
			}
		}
	}
	// check p��slu�n�ho tla��tka
	ASSERT((m_nConnectPart >= 0) && (m_nConnectPart < CNetPartKEY::maxSections));
	if (!m_bValidButton[m_nConnectPart])
	{	// vybeur prvn� p��pustn�
		for (i = 0; i < CNetPartKEY::maxSections; ++i)
		{
			if (m_bValidButton[i])
			{
				m_nConnectPart = i;
				break;
			}
		}
	}
	// check tal��tek
	for (i = 0; i < CNetPartKEY::maxSections; ++i)
	{
		::CheckDlgButton(m_hWnd, CtrlButtons[i], (m_nConnectPart == i) ? 1 : 0);
	}

	// typ objektu
	m_cShowType.SetNetKeyType(m_pKeyObj->m_nKeyPartType, m_pKeyObj->m_nKeyPartSubtype, 0);
	m_cShowType.Invalidate();
}

void CCreateNetPage2_2::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCreateNetPage2_2)
	DDX_Control(pDX, IDC_SHOW_TYPE, m_cShowType);
	//}}AFX_DATA_MAP
	if (!SAVEDATA)
	{
		OnUpdateShowData();
		UpdateValidButtons();
	}
	else
	{
		BOOL bValidButton = FALSE;
		for (int i = 0; i < CNetPartKEY::maxSections; ++i)
		{
			if ((m_bValidButton[i]) && (::IsDlgButtonChecked(m_hWnd, CtrlButtons[i]) != 0))
			{
				m_nConnectPart = i;
				bValidButton   = TRUE;
				break;
			}
		}
		if (!bValidButton)
		{
			AfxMessageBox(IDS_NOT_VALID_CONN_PART_SEL, MB_OK | MB_ICONEXCLAMATION);
			pDX->Fail();
		}
		// p�ipojuji sekc� m_nConnectPart
		DoConnectNetPart(m_nConnectPart);
	}
}

BEGIN_MESSAGE_MAP(CCreateNetPage2_2, CMntPropertyPage)
	//{{AFX_MSG_MAP(CCreateNetPage2_2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CCreateNetPage2_2::OnSetActive() 
{
	UpdateData(FALSE);
	return CMntPropertyPage::OnSetActive();
}

// p�ipojen�� k dan�mu �seku
void CCreateNetPage2_2::DoConnectNetPart(int nSection)
{
	InitRealNetPos(m_pKeyObj->m_nBaseRealPos);

	if (nSection == 0)
	{	// code is wrong, but terminators are wrongly mapped. So this code solves that bug. Will be removed after XBOX- release

		// poloha pro p�ipojen� = A
		m_pKeyObj->m_nBaseRealPos.nPos = m_nConnectPos.nPos;
		// �hel obr�cen�, ne� jak kon�� silnice
		m_pKeyObj->m_nBaseRealPos.nGra = GetNormalizedGrad(m_nConnectPos.nGra + 180);
	} 
	else
	{
		// correct code
		ASSERT(nSection < m_pKeyObj->m_nCountSections);
		REALNETPOS	nRelNet = m_pKeyObj->m_SectionBegins[nSection];

		// poloha pro p�ipojen� = A
		m_pKeyObj->m_nBaseRealPos.nPos = m_nConnectPos.nPos;
		// �hel obr�cen�, ne� jak kon�� silnice + relativn� �hel k A
		m_pKeyObj->m_nBaseRealPos.nGra = GetNormalizedGrad(m_nConnectPos.nGra + 180 - nRelNet.nGra);
		// poloha tak, aby navazoval jinou sekc�
		REALNETPOS rSecPos = m_pKeyObj->GetRealPosForSection(nSection, m_pMap);
		m_pKeyObj->m_nBaseRealPos.nPos.x += (m_pKeyObj->m_nBaseRealPos.nPos.x - rSecPos.nPos.x);
		m_pKeyObj->m_nBaseRealPos.nPos.z += (m_pKeyObj->m_nBaseRealPos.nPos.z - rSecPos.nPos.z);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCreateNetPage3 dialog

class CCreateNetPage3 : public CMntPropertyPage
{
	// Construction
public:
	CCreateNetPage3();
	~CCreateNetPage3();

	// Dialog Data
	CNetPartKEY*     m_pKeyObj;
	CPosNetTemplate* m_pNet;

	//{{AFX_DATA(CCreateNetPage3)
	enum { IDD = IDD_CREATE_NET_PAGE3 };
	//}}AFX_DATA

	// Overrides
	// ClassWizard generate virtual function overrides

	void OnUpdateShowData();

	//{{AFX_VIRTUAL(CCreateNetPage3)
public:
	virtual BOOL OnSetActive();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CCreateNetPage3)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CCreateNetPage3::CCreateNetPage3() 
: CMntPropertyPage(CCreateNetPage3::IDD)
{
	//{{AFX_DATA_INIT(CCreateNetPage3)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CCreateNetPage3::~CCreateNetPage3()
{
}

void CCreateNetPage3::OnUpdateShowData()
{
	CString sTitle;
	// typ objektu
	switch(m_pKeyObj->m_nKeyPartType)
	{
	case CNetPartKEY::typeCross:
		{
			sTitle.LoadString(IDS_KEY_OBJ_TYPE_CROSS);
		}
		break;
	case CNetPartKEY::typeStra:
		{
			ASSERT(m_pNet != NULL);
			AfxFormatString1(sTitle, IDS_KEY_OBJ_TYPE_STRA, m_pNet->m_sNetName);
		}
		break;
	case CNetPartKEY::typeSpec:
		{
			ASSERT(m_pNet != NULL);
			AfxFormatString1(sTitle, IDS_KEY_OBJ_TYPE_SPEC, m_pNet->m_sNetName);
		}
		break;
	case CNetPartKEY::typeTerm:
		{
			ASSERT(m_pNet != NULL);
			AfxFormatString1(sTitle, IDS_KEY_OBJ_TYPE_TERM, m_pNet->m_sNetName);
		}
		break;
	default:
		ASSERT(FALSE);
	}
	// nastaven� titulku
	::SetWindowText(DLGCTRL(IDC_OBJECT_TYPE), sTitle);
	::SetWindowText(DLGCTRL(IDC_OBJECT_NAME), m_pKeyObj->m_nComponentName);
}

void CCreateNetPage3::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCreateNetPage3)
	//}}AFX_DATA_MAP

	if (!SAVEDATA)
	{	// update viditeln�ch dat
		OnUpdateShowData();
	}
}

BEGIN_MESSAGE_MAP(CCreateNetPage3, CMntPropertyPage)
	//{{AFX_MSG_MAP(CCreateNetPage3)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CCreateNetPage3::OnSetActive() 
{
	UpdateData(FALSE);
	return CMntPropertyPage::OnSetActive();
}

/////////////////////////////////////////////////////////////////////////////
// DoCreateNetKeyObject

CNetPartKEY* DoCreateNetKeyObject(CPoint ptLog, CPosNetTemplate* pTempl, CPosEdMainDoc* pDocument)
{
	// objekt
	CNetPartKEY* pKeyObj = NULL;
	TRY
	{
		// vytvo�en� objektu
		pKeyObj = new CNetPartKEY();
		// sou�adnice objektu & z�kladn� orientace
		REALPOS rPos = pDocument->m_PoseidonMap.FromViewToRealPos(ptLog);

		InitRealNetPos(pKeyObj->m_nBaseRealPos);
		pKeyObj->m_nBaseRealPos.nPos.x = rPos.x;
		pKeyObj->m_nBaseRealPos.nPos.z = rPos.z;
			
		// dialog
		CMntPropertySheet dlg(IDS_CREATE_NET_TITLE, PSH_NOAPPLYNOW);
		// str�nky
		CCreateNetPage0 page0; 
		page0.m_pKeyObj = pKeyObj; 
		dlg.AddPageIntoWizard(&page0);
		page0.m_pNet = pTempl;
		CCreateNetPage1 page1; 
		page1.m_pKeyObj = pKeyObj; 
		dlg.AddPageIntoWizard(&page1);
		page1.m_pNet = pTempl;
		page1.m_pMgrNets = &(pDocument->m_MgrNets);
		//page1.m_nBaseRealPos
		CCreateNetPage2 page2; 
		page2.m_pKeyObj = pKeyObj; 
		dlg.AddPageIntoWizard(&page2);
		page2.m_pNet = pTempl;
		page2.m_pMgrNets = &(pDocument->m_MgrNets);
		page2.m_pMap	 = &(pDocument->m_PoseidonMap);
		CCreateNetPage3 page3; 
		page3.m_pKeyObj = pKeyObj; 
		dlg.AddPageIntoWizard(&page3);
		page3.m_pNet = pTempl;

		dlg.SetWizardMode();
		if (dlg.DoModal() != ID_WIZFINISH)
		{	// canceled
			delete pKeyObj; 
			return NULL;
		}
		// OK je nadefinov�n kl��ov� objekt
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pKeyObj != NULL)
		{
			delete pKeyObj;
			pKeyObj = NULL;
		}
	}
	END_CATCH_ALL
	return pKeyObj;
}

/////////////////////////////////////////////////////////////////////////////
// DoCreateNextNetKeyObject

CNetPartKEY* DoCreateNextNetKeyObject(REALNETPOS ptNext, CPosNetTemplate* pTempl, CPosEdMainDoc* pDocument)
{
	// objekt
	CNetPartKEY* pKeyObj = NULL;
	TRY
	{
		// vytvo�en� objektu
		pKeyObj = new CNetPartKEY();
			
		// dialog
		CMntPropertySheet dlg(IDS_CREATE_NEXT_NET_TITLE, PSH_NOAPPLYNOW);
		// str�nky
		CCreateNetPage0 page0; 
		page0.m_pKeyObj = pKeyObj; 
		dlg.AddPageIntoWizard(&page0);						
		page0.m_pNet = pTempl;
		CCreateNetPage1 page1; 
		page1.m_pKeyObj = pKeyObj; 
		dlg.AddPageIntoWizard(&page1);
		page1.m_pNet = pTempl;
		page1.m_bVerifyCrossNet = TRUE;
		page1.m_pMgrNets = &(pDocument->m_MgrNets);
		CCreateNetPage2_2 page2; 
		page2.m_pKeyObj = pKeyObj; 
		dlg.AddPageIntoWizard(&page2);
		page2.m_nConnectPos = ptNext;
		page2.m_pNet = pTempl;
		page2.m_pMgrNets = &(pDocument->m_MgrNets);
		page2.m_pMap	 = &(pDocument->m_PoseidonMap);
		CCreateNetPage3 page3; 
		page3.m_pKeyObj = pKeyObj; 
		dlg.AddPageIntoWizard(&page3);
		page3.m_pNet = pTempl;

		dlg.SetWizardMode();
		if (dlg.DoModal() != ID_WIZFINISH)
		{	// canceled
			delete pKeyObj; 
			return NULL;
		}
		// OK je nadefinov�n kl��ov� objekt
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pKeyObj != NULL)
		{
			delete pKeyObj;
			pKeyObj = NULL;
		}
	}
	END_CATCH_ALL
	return pKeyObj;
}