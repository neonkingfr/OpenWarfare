/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCreateWoodDlg dialog

class CCreateWoodDlg : public CDialog
{
// Construction
public:
	CCreateWoodDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	CPosWoodTemplate*	m_pWoodTempl;

	//{{AFX_DATA(CCreateWoodDlg)
	enum { IDD = IDD_CREATE_WOOD_PARAMS };
	BOOL	m_bClearSelArea;
	BOOL	m_bTorjKonkav;
	BOOL	m_bTorjKonvex;
	BOOL	m_bGenMlazi;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCreateWoodDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCreateWoodDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CCreateWoodDlg::CCreateWoodDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCreateWoodDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCreateWoodDlg)
	m_bTorjKonvex   = TRUE;
	m_bTorjKonkav   = TRUE;
	m_bGenMlazi	    = TRUE;
	m_bClearSelArea = TRUE;
	//}}AFX_DATA_INIT
}

BOOL CCreateWoodDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// jm�no lesa	
	::SetWindowText(DLGCTRL(IDC_SHOW_WOOD_NAME),m_pWoodTempl->m_sWoodName);
	
	return TRUE;
}

void CCreateWoodDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCreateWoodDlg)
	DDX_Check(pDX, IDC_CLEAR_SEL_AREA, m_bClearSelArea);
	DDX_Check(pDX, IDC_TROJ_KONKAV, m_bTorjKonkav);
	DDX_Check(pDX, IDC_TROJ_KONVEX, m_bTorjKonvex);
	DDX_Check(pDX, IDC_GENER_MLAZI, m_bGenMlazi);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCreateWoodDlg, CDialog)
	//{{AFX_MSG_MAP(CCreateWoodDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DoCreateWoodParamsDlg

BOOL DoCreateWoodParamsDlg(CPosWoodTemplate* pWoodTempl,
	BOOL& bTrjKonvex,BOOL& bTrjKonkav,BOOL& bGenMlazi,
	BOOL& bClearArea)
{
	CCreateWoodDlg dlg;
	dlg.m_pWoodTempl = pWoodTempl;

	if (dlg.DoModal()!=IDOK)
		return FALSE;
	// parametry pro vytvo�en� lesa
	bTrjKonvex = dlg.m_bTorjKonvex;
	bTrjKonkav = dlg.m_bTorjKonkav;
	bGenMlazi  = dlg.m_bGenMlazi;
	bClearArea = dlg.m_bClearSelArea;

	return TRUE;
};


