/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Akce se s�t�mi

extern CNetPartKEY* DoCreateNetKeyObject(CPoint ptLog, CPosNetTemplate* pTempl, CPosEdMainDoc* pDocument);

// vytvo�en� akce pro vytvo�en� kl��ov�ho bodu
CPosAction* Action_CreatePoseidonKeyNet(CPoint ptLog, CPosNetTemplate* pTempl, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup)
{
	// kl��ov� objekt
	CNetPartKEY* pKeyObj = DoCreateNetKeyObject(ptLog, pTempl, pDocument);
	if (pKeyObj == NULL) return NULL;

	CPosNetObjectKey* pNetKey     = NULL;
	CPosActionGroup*  pGrpAction  = NULL;
	TRY
	{
		// vytvo�en� nov�ho kl��ov�ho bodu
		pNetKey = new CPosNetObjectKey(pDocument->m_PoseidonMap);
		pNetKey->CreateFromNetKeyPart(pKeyObj, &(pDocument->m_MgrNets));

		// update dat nov�ho objektu		
		// update z�kladn�ch re�ln�ch sou�adnic objekt�
		pNetKey->DoUpdateNetPartsBaseRealPos();
		// update logick�ch sou�adnic objekt� s�t�
		pNetKey->DoUpdateNetPartsLogPosition();

		// vytvo�en� skupinov� ud�losti
		pGrpAction = new CPosObjectGroupAction(IDA_NET_KEY_CREATE_GROUP);
		pGrpAction->m_bFromBuldozer = FALSE;
		// vytvo�en� objektu s�t�
		CPosObjectAction* pNetAct = (CPosObjectAction*)(pDocument->GenerNewObjectAction(IDA_NET_KEY_CREATE, 
																						NULL,
																						pNetKey,
																						FALSE,
																						pGrpAction));

		// generov�n�/update z�kladn�ch objekt� s�t�
		if (!DoUpdateNetPoseidonObjects(pNetKey, pDocument, pGrpAction))
		{
			if (pGrpAction)
			{
				delete pGrpAction;
			}
			else if (pNetKey)
			{
				delete pNetKey;
			}
			return NULL;
		}
		// provedu akci
		if (pToGroup)
		{
			pToGroup->AddAction(pGrpAction);
		}
		else
		{
			pDocument->ProcessEditAction(pGrpAction);
		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pGrpAction)
		{
			delete pGrpAction;
			pGrpAction = NULL;
		} 
		else
		{	// sma�u d�l�� objekty
			if (pNetKey)
			{
				delete pNetKey;
				pNetKey = NULL;
			}
		}
	}
	END_CATCH_ALL
	return pGrpAction;
}

extern CNetPartKEY* DoCreateNextNetKeyObject(REALNETPOS ptNext, CPosNetTemplate* pTempl, CPosEdMainDoc* pDocument);

CPosAction* Action_CreatePoseidonNextKeyNet(REALNETPOS ptNext, CPosNetTemplate* pTempl, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup)
{
	// kl��ov� objekt
	CNetPartKEY* pKeyObj = DoCreateNextNetKeyObject(ptNext, pTempl, pDocument);
	if (pKeyObj == NULL) return NULL;

	CPosNetObjectKey* pNetKey    = NULL;
	CPosActionGroup*  pGrpAction = NULL;
	TRY
	{
		// vytvo�en� nov�ho kl��ov�ho bodu
		pNetKey = new CPosNetObjectKey(pDocument->m_PoseidonMap);
		pNetKey->CreateFromNetKeyPart(pKeyObj,&(pDocument->m_MgrNets));

		// update dat nov�ho objektu
		
		// update z�kladn�ch re�ln�ch sou�adnic objekt�
		pNetKey->DoUpdateNetPartsBaseRealPos();
		// update logick�ch sou�adnic objekt� s�t�
		pNetKey->DoUpdateNetPartsLogPosition();

		// vytvo�en� skupinov� ud�losti
		pGrpAction = new CPosObjectGroupAction(IDA_NET_KEY_CREATE_GROUP);
		pGrpAction->m_bFromBuldozer = FALSE;
		// vytvo�en� objektu s�t�
		CPosObjectAction* pNetAct = (CPosObjectAction*)(pDocument->GenerNewObjectAction(IDA_NET_KEY_CREATE,
				NULL,pNetKey,FALSE,pGrpAction));

		// generov�n�/update z�kladn�ch objekt� s�t�
		if(!DoUpdateNetPoseidonObjects(pNetKey,pDocument,pGrpAction))
		{
			if (pGrpAction)
				delete pGrpAction;
			else
			if (pNetKey)
				delete pNetKey;
			return NULL;
		};
		// provedu akci
		if (pToGroup)
			pToGroup->AddAction(pGrpAction);
		else
			pDocument->ProcessEditAction(pGrpAction);
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pGrpAction)
		{
			delete pGrpAction;
			pGrpAction = NULL;
		} else
		{	// sma�u d�l�� objekty
			if (pNetKey)
			{
				delete pNetKey;
				pNetKey = NULL;
			}
		};
	}
	END_CATCH_ALL
	return pGrpAction;
};

// akce pro vytvo�en� �seku s�t�
CPosAction* Action_CreatePoseidonNorNet(CPosNetObjectNor* pNetObj,CPosNetTemplate* pTempl,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	CPosActionGroup*	pGrpAction  = NULL;
	TRY
	{
		// update dat nov�ho objektu
		pNetObj->SetFreeID();
		// update z�kladn�ch re�ln�ch sou�adnic objekt�
		pNetObj->DoUpdateNetPartsBaseRealPos();
		// update logick�ch sou�adnic objekt� s�t�
		pNetObj->DoUpdateNetPartsLogPosition();

		// vytvo�en� skupinov� ud�losti
		pGrpAction = new CPosObjectGroupAction(IDA_NET_KEY_CREATE_GROUP);
		pGrpAction->m_bFromBuldozer = FALSE;
		// vytvo�en� objektu s�t�
		CPosObjectAction* pNetAct = (CPosObjectAction*)(pDocument->GenerNewObjectAction(IDA_NET_KEY_CREATE,
				NULL,pNetObj,FALSE,pGrpAction));

		// generov�n�/update z�kladn�ch objekt� s�t�
		if(!DoUpdateNetPoseidonObjects(pNetObj,pDocument,pGrpAction))
		{
			if (pGrpAction)
				delete pGrpAction;
			else
			if (pNetObj)
				delete pNetObj;
			return NULL;
		};
		// provedu akci
		if (pToGroup)
			pToGroup->AddAction(pGrpAction);
		else
			pDocument->ProcessEditAction(pGrpAction);
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pGrpAction)
		{
			delete pGrpAction;
			pGrpAction = NULL;
		} else
		{	// sma�u d�l�� objekty
			if (pNetObj)
			{
				delete pNetObj;
				pNetObj = NULL;
			}
		};
	}
	END_CATCH_ALL
	return pGrpAction;
};


// vytvo�en� akce pro smaz�n� �seku s�t�
CPosAction* Action_DeletePoseidonNet(CPosNetObjectBase* pToDel,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pToDel != NULL)
	{
		CPosObject* pDocObj = pDocument->GetDocumentObjectFromValue(pToDel);
		if (pDocObj)
		{
			ASSERT_KINDOF(CPosNetObjectBase,pDocObj);
			CPosObjectGroupAction*	pGrpAction  = NULL;
			TRY
			{
				// vytvo�en� skupinov� ud�losti
				pGrpAction = new CPosObjectGroupAction(IDA_GROUPOBJ_DELETE);
				pGrpAction->m_bFromBuldozer = FALSE;
				// smaz�n� objektu lesa
				pDocument->GenerNewObjectAction(IDA_NET_KEY_DELETE,pDocObj,NULL,FALSE,pGrpAction);
				// generov�n� zru�en� objekt�
				for(int i=0; i<pDocument->m_PoseidonMap.GetPoseidonObjectCount(); i++)
				{
					CPosEdObject* pObj = pDocument->m_PoseidonMap.GetPoseidonObject(i);
					if ((pObj != NULL)&&(pObj->m_nMasterNet==pToDel->GetID()))
					{	// toto je objekt spr�vn�ho lesa
						CPosObjectAction* pAction = (CPosObjectAction*)Action_DeletePoseidonObject(pObj,pDocument,pGrpAction);
						if (pAction)
						{
							// nebude se m�nit selekce
							pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
							// nebude se prov�d�t update
							pAction->m_bUpdateView	  = FALSE;
						}
					}
					
				}
				// provedu akci
				if (pToGroup)
					pToGroup->AddAction(pGrpAction);
				else
					pDocument->ProcessEditAction(pGrpAction);
			}
			CATCH_ALL(e)
			{
				MntReportMemoryException();
				if (pGrpAction)
				{
					delete pGrpAction;
					pGrpAction = NULL;
				}
			}
			END_CATCH_ALL
			return pGrpAction;
		}
	}
	return NULL;
};

// vytvo�en� akce pro dekompozici s�t�
CPosAction* Action_DekompPoseidonNet(CPosNetObjectBase* pNet,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pNet != NULL)
	{
		CPosObject* pDocObj = pDocument->GetDocumentObjectFromValue(pNet);
		if (pDocObj)
		{
			ASSERT_KINDOF(CPosNetObjectBase,pDocObj);
			CPosActionGroup*	pGrpAction  = NULL;
			TRY
			{
				// vytvo�en� skupinov� ud�losti
				pGrpAction = new CPosActionGroup(IDA_OBJECT_DEKOMP);
				pGrpAction->m_bFromBuldozer = FALSE;
				// smaz�n� objektu lesa
				CPosObjectAction* pAction = (CPosObjectAction*)pDocument->GenerNewObjectAction(IDA_NET_KEY_DELETE,pDocObj,NULL,FALSE,pGrpAction);
        pAction->m_nSelectObjType = CPosObjectAction::ida_SelectExclu; // undo selection
				// generov�n� zm�n objekt�
				for(int i=0; i<pDocument->m_PoseidonMap.GetPoseidonObjectCount(); i++)
				{
					CPosEdObject* pObj = pDocument->m_PoseidonMap.GetPoseidonObject(i);
					if ((pObj != NULL)&&(pObj->m_nMasterNet==pNet->GetID()))
					{	// toto je objekt spr�vn�ho lesa
						CPosEdObject ObjToAction(pDocument->m_PoseidonMap);
						ObjToAction.CopyFrom(pObj);
						ObjToAction.m_nMasterNet  = UNDEF_REG_ID;
						CPosObjectAction* pAction = (CPosObjectAction*)Action_EditPoseidonObject(&ObjToAction,pDocument,pGrpAction);
						if (pAction)
						{
							// nebude se m�nit selekce
							pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
						}
					}
					
				}
				// provedu akci
				if (pToGroup)
					pToGroup->AddAction(pGrpAction);
				else
					pDocument->ProcessEditAction(pGrpAction);
			}
			CATCH_ALL(e)
			{
				MntReportMemoryException();
				if (pGrpAction)
				{
					delete pGrpAction;
					pGrpAction = NULL;
				}
			}
			END_CATCH_ALL
			return pGrpAction;
		}
	}
	return NULL;
};

// vytvo�en� akce pro p�esun site
CPosAction* Action_MovePoseidonNet(CPosNetObjectBase* pNet,int xAmount,int yAmount,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pNet != NULL)
	{
		CPosObject* pDocObj = pDocument->GetDocumentObjectFromValue(pNet);
		if (pDocObj)
		{
			ASSERT_KINDOF(CPosNetObjectBase,pDocObj);
			CPosActionGroup*	pGrpAction  = NULL;
			TRY
			{
				// vytvo�en� skupinov� ud�losti
				pGrpAction = new CPosActionGroup(IDA_NET_KEY_MOVE);
				pGrpAction->m_bFromBuldozer = FALSE;
				// posun objektu s�t�
				CPosNetObjectBase* pAct = (CPosNetObjectKey*)(pNet->CreateCopyObject());

				// sou�adnice objektu & z�kladn� orientace
				REALPOS rPos = pAct->m_nBaseRealPos.nPos;
				POINT ptLog  = pDocument->m_PoseidonMap.FromRealToViewPos(rPos);
				ptLog.x += xAmount;
				ptLog.y += yAmount;
				rPos = pDocument->m_PoseidonMap.FromViewToRealPos(ptLog);
				pAct->m_nBaseRealPos.nPos.x = rPos.x;
				pAct->m_nBaseRealPos.nPos.z = rPos.z;

				// update z�kladn�ch re�ln�ch sou�adnic objekt�
				pAct->DoUpdateNetPartsBaseRealPos();
				// update logick�ch sou�adnic objekt� s�t�
				pAct->DoUpdateNetPartsLogPosition();
				
				pDocument->GenerNewObjectAction(IDA_NET_KEY_MOVE,pDocObj,pAct,FALSE,pGrpAction);

				// generov�n�/update z�kladn�ch objekt� s�t�
				if(!DoUpdateNetPoseidonObjects(pAct,pDocument,pGrpAction))
				{
					if (pGrpAction)
						delete pGrpAction;
					else
					if (pAct)
						delete pAct;
					return NULL;
				};

				// provedu akci
				if (pToGroup)
					pToGroup->AddAction(pGrpAction);
				else
					pDocument->ProcessEditAction(pGrpAction);
			}
			CATCH_ALL(e)
			{
				MntReportMemoryException();
				if (pGrpAction)
				{
					delete pGrpAction;
					pGrpAction = NULL;
				}
			}
			END_CATCH_ALL
			return pGrpAction;
		}
	}
	return NULL;
};

CPosAction* Action_RotatePoseidonNet(CPosNetObjectBase* pNet, float grad, CPointVal origin, CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  if (pNet != NULL)
  {
    CPosObject* pDocObj = pDocument->GetDocumentObjectFromValue(pNet);
    if (pDocObj)
    {
      ASSERT_KINDOF(CPosNetObjectBase,pDocObj);
      CPosActionGroup*	pGrpAction  = NULL;
      TRY
      {
        // vytvo�en� skupinov� ud�losti
        pGrpAction = new CPosActionGroup(IDA_NET_KEY_MOVE);
        pGrpAction->m_bFromBuldozer = FALSE;
        // posun objektu s�t�
        CPosNetObjectBase* pAct = (CPosNetObjectKey*)(pNet->CreateCopyObject());

        // sou�adnice objektu & z�kladn� orientace       
        pAct->m_nBaseRealPos.nGra += toInt(grad);     

        REALPOS realOrigin = pDocument->m_PoseidonMap.FromViewToRealPos(origin);
        FMntVector2 vecOrigin(realOrigin.x, realOrigin.z);
        FMntVector2 vecBase(pAct->m_nBaseRealPos.nPos.x, pAct->m_nBaseRealPos.nPos.z);

        FMntMatrix2 trans;
        trans.Rotation(360 - (float) grad);

        vecBase = trans * (vecBase - vecOrigin) + vecOrigin;

        pAct->m_nBaseRealPos.nPos.x = vecBase[0];
        pAct->m_nBaseRealPos.nPos.z = vecBase[1];

        // update z�kladn�ch re�ln�ch sou�adnic objekt�
        pAct->DoUpdateNetPartsBaseRealPos();
        // update logick�ch sou�adnic objekt� s�t�
        pAct->DoUpdateNetPartsLogPosition();

        pDocument->GenerNewObjectAction(IDA_NET_KEY_MOVE,pDocObj,pAct,FALSE,pGrpAction);

        // generov�n�/update z�kladn�ch objekt� s�t�
        if(!DoUpdateNetPoseidonObjects(pAct,pDocument,pGrpAction))
        {
          if (pGrpAction)
            delete pGrpAction;
          else
            if (pAct)
              delete pAct;
          return NULL;
        };

        // provedu akci
        if (pToGroup)
          pToGroup->AddAction(pGrpAction);
        else
          pDocument->ProcessEditAction(pGrpAction);
      }
      CATCH_ALL(e)
      {
        MntReportMemoryException();
        if (pGrpAction)
        {
          delete pGrpAction;
          pGrpAction = NULL;
        }
      }
      END_CATCH_ALL
        return pGrpAction;
    }
  }
  return NULL;
};

CPosAction* Action_RotateMovePoseidonNet(CPosNetObjectBase* pNet,CPointVal offset, float grad, CPointVal origin,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  if (pNet != NULL)
  {
    CPosObject* pDocObj = pDocument->GetDocumentObjectFromValue(pNet);
    if (pDocObj)
    {
      ASSERT_KINDOF(CPosNetObjectBase,pDocObj);
      CPosActionGroup*	pGrpAction  = NULL;
      TRY
      {
        // vytvo�en� skupinov� ud�losti
        pGrpAction = new CPosActionGroup(IDA_NET_KEY_MOVE);
        pGrpAction->m_bFromBuldozer = FALSE;
        // posun objektu s�t�
        CPosNetObjectBase* pAct = (CPosNetObjectKey*)(pNet->CreateCopyObject());

        // sou�adnice objektu & z�kladn� orientace       
        pAct->m_nBaseRealPos.nGra -= (int) grad;  // nGra is in logical coordinates !!!   

        REALPOS realOrigin = pDocument->m_PoseidonMap.FromViewToRealPos(origin);
        FMntVector2 vecOrigin(realOrigin.x, realOrigin.z);
        FMntVector2 vecBase(pAct->m_nBaseRealPos.nPos.x, pAct->m_nBaseRealPos.nPos.z);

        FMntMatrix2 trans;
        trans.Rotation(grad);

        vecBase = trans * (vecBase - vecOrigin) + vecOrigin;

        // offset
        CPoint ptLog  = pDocument->m_PoseidonMap.FromRealToViewPos(REALPOS(vecBase[0],vecBase[1]));
        ptLog += offset;        
        pAct->m_nBaseRealPos.nPos = pDocument->m_PoseidonMap.FromViewToRealPos(ptLog);         

        // update z�kladn�ch re�ln�ch sou�adnic objekt�
        pAct->DoUpdateNetPartsBaseRealPos();
        // update logick�ch sou�adnic objekt� s�t�
        pAct->DoUpdateNetPartsLogPosition();

        pDocument->GenerNewObjectAction(IDA_NET_KEY_MOVE,pDocObj,pAct,FALSE,pGrpAction);

        // generov�n�/update z�kladn�ch objekt� s�t�
        if(!DoUpdateNetPoseidonObjects(pAct,pDocument,pGrpAction))
        {
          if (pGrpAction)
            delete pGrpAction;
          else
            if (pAct)
              delete pAct;
          return NULL;
        };

        // provedu akci
        if (pToGroup)
          pToGroup->AddAction(pGrpAction);
        else
          pDocument->ProcessEditAction(pGrpAction);
      }
      CATCH_ALL(e)
      {
        MntReportMemoryException();
        if (pGrpAction)
        {
          delete pGrpAction;
          pGrpAction = NULL;
        }
      }
      END_CATCH_ALL
        return pGrpAction;
    }
  }
  return NULL;
}

CPosAction* Action_SetPosRealNet(CPosNetObjectBase* pNet,const REALPOS& pos, CPosActionGroup* pToGroup)
{
  if (pNet != NULL)
	{
    CPosEdMainDoc * doc = (CPosEdMainDoc *) pNet->GetDocument();
		CPosObject* pDocObj = doc->GetDocumentObjectFromValue(pNet);
		if (pDocObj)
		{
			ASSERT_KINDOF(CPosNetObjectBase,pDocObj);
			CPosObjectGroupAction*	pGrpAction  = NULL;
			TRY
			{
				// vytvo�en� skupinov� ud�losti
				pGrpAction = new CPosObjectGroupAction(IDA_GROUPOBJ_MOVE);
				pGrpAction->m_bFromBuldozer = FALSE;
				// posun objektu s�t�
        CPosNetObjectBase* pAct = (CPosNetObjectBase*)(pNet->CreateCopyObject());
        REALNETPOS netPos = pAct->GetNetRealPos();
        netPos.nPos = pos;

        pAct->SetNetRealPos(netPos);

        // update z�kladn�ch re�ln�ch sou�adnic objekt�
        pAct->DoUpdateNetPartsBaseRealPos();
        // update logick�ch sou�adnic objekt� s�t�
        pAct->DoUpdateNetPartsLogPosition();

        doc->GenerNewObjectAction(IDA_NET_KEY_MOVE,pDocObj,pAct,FALSE,pGrpAction);

        // generov�n�/update z�kladn�ch objekt� s�t�
        if(!DoUpdateNetPoseidonObjects(pAct,doc,pGrpAction))
        {
          if (pGrpAction)
            delete pGrpAction;
          else
            if (pAct)
              delete pAct;
          return NULL;
        };

        // provedu akci
        if (pToGroup)
          pToGroup->AddAction(pGrpAction);
        else
          doc->ProcessEditAction(pGrpAction);
      }
      CATCH_ALL(e)
      {
        MntReportMemoryException();
        if (pGrpAction)
        {
          delete pGrpAction;
          pGrpAction = NULL;
        }
      }
      END_CATCH_ALL
        return pGrpAction;
    }
  }
  return NULL;
};

CPosAction* Action_RotateNet(CPosNetObjectBase* pNet,int grad, CPosActionGroup* pToGroup)
{
  if (pNet != NULL)
  {
    CPosEdMainDoc * doc = (CPosEdMainDoc *) pNet->GetDocument();
    CPosObject* pDocObj = doc->GetDocumentObjectFromValue(pNet);
    if (pDocObj)
    {
      ASSERT_KINDOF(CPosNetObjectBase,pDocObj);
      CPosObjectGroupAction*	pGrpAction  = NULL;
      TRY
      {
        // vytvo�en� skupinov� ud�losti
        pGrpAction = new CPosObjectGroupAction(IDA_GROUPOBJ_MOVE);
        pGrpAction->m_bFromBuldozer = FALSE;
        // posun objektu s�t�
        CPosNetObjectBase* pAct = (CPosNetObjectBase*)(pNet->CreateCopyObject());

        REALNETPOS netPos = pAct->GetNetRealPos();
        netPos.nGra += grad;   
        pAct->SetNetRealPos(netPos);

        // update z�kladn�ch re�ln�ch sou�adnic objekt�
        pAct->DoUpdateNetPartsBaseRealPos();
        // update logick�ch sou�adnic objekt� s�t�
        pAct->DoUpdateNetPartsLogPosition();

        doc->GenerNewObjectAction(IDA_NET_KEY_MOVE,pDocObj,pAct,FALSE,pGrpAction);

        // generov�n�/update z�kladn�ch objekt� s�t�
        if(!DoUpdateNetPoseidonObjects(pAct,doc,pGrpAction))
        {
          if (pGrpAction)
            delete pGrpAction;
          else
            if (pAct)
              delete pAct;
          return NULL;
        };

        // provedu akci
        if (pToGroup)
          pToGroup->AddAction(pGrpAction);
        else
          doc->ProcessEditAction(pGrpAction);
      }
      CATCH_ALL(e)
      {
        MntReportMemoryException();
        if (pGrpAction)
        {
          delete pGrpAction;
          pGrpAction = NULL;
        }
      }
      END_CATCH_ALL
        return pGrpAction;
    }
  }
  return NULL;
};

