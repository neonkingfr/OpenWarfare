/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "math.h"
#include "AutoNetHeight.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAutoNetHeightPage0 dialog

class CAutoNetHeightPage0 : public CMntPropertyPage
{
// Construction
public:
	CAutoNetHeightPage0();

// Dialog Data
	BOOL	m_bFixedEndDirectly;
	//{{AFX_DATA(CAutoNetHeightPage0)
	enum { IDD = IDD_AUTO_NET_HEIGHT_MAIN };
	BOOL	m_nShowResults;
	BOOL	m_bEnableAutoTest;
	BOOL	m_bFixEndHeight;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CAutoNetHeightPage0)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CAutoNetHeightPage0)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

CAutoNetHeightPage0::CAutoNetHeightPage0() : CMntPropertyPage(CAutoNetHeightPage0::IDD)
{
	m_bFixedEndDirectly = FALSE;
	//{{AFX_DATA_INIT(CAutoNetHeightPage0)
	m_bFixEndHeight	  = FALSE;
	m_nShowResults	  = TRUE;
	m_bEnableAutoTest = TRUE;
	//}}AFX_DATA_INIT
}

void CAutoNetHeightPage0::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAutoNetHeightPage0)
	DDX_Check(pDX, IDC_SHOW_RESULTS, m_nShowResults);
	DDX_Check(pDX, IDC_ENABLE_AUTOTEST, m_bEnableAutoTest);
	DDX_Check(pDX, IDC_FIX_END_HEIGHT, m_bFixEndHeight);
	//}}AFX_DATA_MAP
	if (!SAVEDATA)
	{
		if (m_bFixedEndDirectly)
		{
			DDX_Check(pDX, IDC_FIX_END_HEIGHT, m_bFixedEndDirectly);
			::EnableWindow(DLGCTRL(IDC_FIX_END_HEIGHT),FALSE);
		}

	}
}


BEGIN_MESSAGE_MAP(CAutoNetHeightPage0, CMntPropertyPage)
	//{{AFX_MSG_MAP(CAutoNetHeightPage0)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAutoNetHeightPage1 dialog

class CAutoNetHeightPage1 : public CMntPropertyPage
{
// Construction
public:
	CAutoNetHeightPage1();

// Dialog Data
	AutoNetHgParams	m_Params;
	//{{AFX_DATA(CAutoNetHeightPage1)
	enum { IDD = IDD_AUTO_NET_HEIGHT_PARA };
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CAutoNetHeightPage1)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CAutoNetHeightPage1)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

CAutoNetHeightPage1::CAutoNetHeightPage1() : CMntPropertyPage(CAutoNetHeightPage1::IDD)
{
	//{{AFX_DATA_INIT(CAutoNetHeightPage1)
	//}}AFX_DATA_INIT
}

void CAutoNetHeightPage1::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAutoNetHeightPage1)
	//}}AFX_DATA_MAP
	MDDX_Text(pDX, IDC_EDIT_STEP_HGHT, m_Params.nHeightStep);
	MDDV_MinMaxDouble(pDX, m_Params.nHeightStep, 0.01, 1., IDC_EDIT_STEP_HGHT, 0.1);

	MDDX_Text(pDX, IDC_EDIT_GRAD_ABS, m_Params.nPriceSlope);
	MDDV_MinMaxLong(pDX, m_Params.nPriceSlope, 0, 1000, IDC_EDIT_GRAD_ABS, 1);
	MDDX_Text(pDX, IDC_EDIT_GRAD_REL, m_Params.nPricePrvSl);
	MDDV_MinMaxLong(pDX, m_Params.nPricePrvSl, 0, 1000, IDC_EDIT_GRAD_REL, 1);
	MDDX_Text(pDX, IDC_EDIT_STD_DIFER, m_Params.nPriceStand);
	MDDV_MinMaxLong(pDX, m_Params.nPriceStand, 0, 1000, IDC_EDIT_STD_DIFER, 1);

	MDDX_Text(pDX, IDC_EDIT_ERR_NOT_NET, m_Params.nErrBadPrvHg);
	MDDV_MinMaxLong(pDX, m_Params.nErrBadPrvHg, 0, 10000000, IDC_EDIT_ERR_NOT_NET, 1000);
	MDDX_Text(pDX, IDC_EDIT_ERR_FLYING, m_Params.nErrBadNetUp);
	MDDV_MinMaxLong(pDX, m_Params.nErrBadNetUp, 0, 10000000, IDC_EDIT_ERR_FLYING, 1000);
	MDDX_Text(pDX, IDC_EDIT_ERR_DOWN, m_Params.nErrBadNetDn);
	MDDV_MinMaxLong(pDX, m_Params.nErrBadNetDn, 0, 10000000, IDC_EDIT_ERR_DOWN, 1000);
	MDDX_Text(pDX, IDC_EDIT_ERR_GRAD_ABS, m_Params.nErrBadSlope);
	MDDV_MinMaxLong(pDX, m_Params.nErrBadSlope, 0, 10000000, IDC_EDIT_ERR_GRAD_ABS, 1000);
	MDDX_Text(pDX, IDC_EDIT_ERR_GRAD_REL, m_Params.nErrBadPrvSl);
	MDDV_MinMaxLong(pDX, m_Params.nErrBadPrvSl, 0, 10000000, IDC_EDIT_ERR_GRAD_REL, 1000);

}


BEGIN_MESSAGE_MAP(CAutoNetHeightPage1, CMntPropertyPage)
	//{{AFX_MSG_MAP(CAutoNetHeightPage1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// uzel pro nastaven� v��ky - CKnotNetPart

CKnotNetPart::CKnotNetPart(CKnotNetPart* pParent)
{
	m_pParent = pParent;
};

/////////////////////////////////////////////////////////////////////////////
// D�l�� kroky algoritmu

// kalkulace ceny pro uzel
LONG DoCalcPriceForKnot(CKnotNetPart* pKnot,CObArray& PartList,
						const CNetPartsList* pList,const CPoseidonMap* pMap,
						AutoNetHgParams& AutoParams)
{
	ASSERT(pKnot != NULL);
	// st�vaj�c� d�l
	CNetPartSIMP* pCurrent = (CNetPartSIMP*)(PartList[pKnot->m_nPartNumber]);
	ASSERT(pCurrent != NULL);
	// d�l pro v�po�et
	CNetPartSIMP* pTempNet = (CNetPartSIMP*)(pCurrent->CreateCopyObject());
	if (pTempNet == NULL) AfxThrowMemoryException();
	ASSERT_KINDOF(CNetPartSIMP,pTempNet);
	// cena p�edchoz�ho d�lu
	LONG   nPrice = (pKnot->m_pParent != NULL)?(pKnot->m_pParent->m_nPrice):0;
	// p�edchoz� d�l
	double nPrvSl = (pKnot->m_pParent != NULL)?(pKnot->m_pParent->m_nSlope):0.;	// �hel p�edchoz�ho d�lu
	// koncov� poloha p�edchoz�ho d�lu
	REALNETPOS nPrvHg;
	if (pKnot->m_pParent != NULL)
	{
		nPrvHg.nPos = pTempNet->m_nConnRealPos.nPos;
		nPrvHg.nGra = pTempNet->m_nConnRealPos.nGra;
		// v��ak je pou�ita jako v��ka p�edchoz�ho
		nPrvHg.nHgt = pTempNet->m_nConnRealPos.nHgt = pKnot->m_pParent->m_nHeight;
	} else
	{	// je to po��tek seznamu
		nPrvHg = pList->m_nBaseRealPos;
	};
	// nastav�m temporary d�l tak, ja m� vypadat
	pTempNet->m_nBaseRealPos.nHgt = pKnot->m_nHeight;
	// u objekt�, kter� nemohou m�nit v��ku = konstanta
	if ((!pTempNet->CanChangeSlope())||(pTempNet->DisChangeSlope()))
		pTempNet->m_nConnRealPos.nHgt = pTempNet->m_nBaseRealPos.nHgt;
	// nasatv�m aktu�ln� sklon
	pKnot->m_nSlope = pTempNet->GetCurrentSlopeRad();

	// spo��t�m chyby v pTempNet
	pTempNet->m_nTestState = pTempNet->DoTestNetPart_All(nPrvHg,nPrvSl,pList,pMap);

	// spo��t�m cenu za chyby
	LONG nLocalPrice = 0;
	if (pTempNet->m_nTestState & CNetPartSIMP::tstErrBadPrvHg)
	{
		nLocalPrice += AutoParams.nErrBadPrvHg;
	}
	if (pTempNet->m_nTestState & CNetPartSIMP::tstErrBadNetUp)
	{
		nLocalPrice += AutoParams.nErrBadNetUp;
	}
	if (pTempNet->m_nTestState & CNetPartSIMP::tstErrBadNetDn)
	{
		nLocalPrice += AutoParams.nErrBadNetDn;
	}
	if (pTempNet->m_nTestState & CNetPartSIMP::tstErrBadSlope)
	{
		double nSlope = fabs(pKnot->m_nSlope);
		double nMaxSl = ((6.28318530718*pList->m_nMaxSlope)/360.);
		if (nSlope > nMaxSl)
		{
			nSlope -= nMaxSl;
			// p�evedu na 0.1�  (nSlope*3600.)/6.28318530718
			nSlope  = nSlope*572.9577951308;
			nLocalPrice += (LONG)(nSlope*AutoParams.nErrBadSlope);
		}
	}
	if (pTempNet->m_nTestState & CNetPartSIMP::tstErrBadPrvSl)
	{
		double nSlope = fabs(nPrvSl - pKnot->m_nSlope);
		double nMaxSl = ((6.28318530718*pList->m_nPartSlope)/360.);
		if (nSlope > nMaxSl)
		{
			nSlope -= nMaxSl;
			// p�evedu na 0.1�  (nSlope*3600.)/6.28318530718
			nSlope  = nSlope*572.9577951308;
			nLocalPrice += (LONG)(nSlope*AutoParams.nErrBadPrvSl);
		}
	}
	delete pTempNet; pTempNet = NULL;

	nPrice += nLocalPrice;
	// spo��t�m cenu za d�l
	nLocalPrice = 0;
	{	// sklon k sousedn�mu
		double nSlope = fabs(nPrvSl - pKnot->m_nSlope);
		// p�evedu na 0.1�  (nSlope*3600.)/6.28318530718
		nSlope  = nSlope*572.9577951308;
		nLocalPrice += (LONG)(nSlope*AutoParams.nPricePrvSl);
	}
	{	// sklon d�lu
		double nSlope = fabs(pKnot->m_nSlope);
		// p�evedu na 0.1�  (nSlope*3600.)/6.28318530718
		nSlope  = nSlope*572.9577951308;
		nLocalPrice += (LONG)(nSlope*AutoParams.nPriceSlope);
	}
	{	// odchylka od standardn� v��ky
		double nDiffer= fabs(pKnot->m_nRelHg/* - pList->m_nStdHeight*/);
		nDiffer *= 10.;
		nLocalPrice += (LONG)(nDiffer*AutoParams.nPriceStand);
	}

	nPrice += nLocalPrice;
	return nPrice;
};

// za�azen� uzlu do seznamu
void DoInsertKnotToOpenList(CKnotNetPart* pKnot,CObList& KnotsOpen)
{
	if (KnotsOpen.GetCount()==0)
	{
		KnotsOpen.AddHead(pKnot);
		return;
	}


	POSITION pos, posOld, posInsert = NULL;
	for(pos = KnotsOpen.GetHeadPosition(); pos != NULL; )
	{
		posOld = pos;
		CKnotNetPart* pOld = (CKnotNetPart*)(KnotsOpen.GetNext(pos));
		ASSERT(pOld != NULL);
		// je stejn� ?
		BOOL bSame = ((pOld->m_nPartNumber == pKnot->m_nPartNumber)&&(pOld->m_nHeight == pKnot->m_nHeight));
		if  (bSame)
		{
			if (pOld->m_nPrice <= pKnot->m_nPrice)
			{	// pou�iji star�, tenhle je na nic
				delete pKnot;
				return;
			} else
			{	// vyjmu star� a pou�iji nov�
				ASSERT(pKnot->m_nPrice < pOld->m_nPrice);
				KnotsOpen.RemoveAt(posOld);
				delete pOld;
				// posInsert - je ur�ena a nen� to ta co vyj�m�m
				break;
			};
		}
		if (pOld->m_nPrice < pKnot->m_nPrice)
			posInsert = posOld;
	};
	// vlo�en� do seznamu
	if (posInsert == NULL)
		KnotsOpen.AddHead(pKnot);
	else
		KnotsOpen.InsertAfter(posInsert,pKnot);
};

// expanze potomk�
void DoCreateKnotsForParent(CKnotNetPart* pParent,CObList& KnotsOpen,
		CObArray& PartList,const CNetPartsList* pList,const CPoseidonMap* pMap,
		REALNETPOS BgnPos,REALNETPOS EndPos,BOOL bFixEnd,AutoNetHgParams& Params)
{
	// vyb�r�m �sek d�lu, pro kter� budu vyr�b�t varianty
	int nIndexNetPart   = (pParent == NULL)?0:(pParent->m_nPartNumber+1);
	ASSERT(nIndexNetPart < PartList.GetSize());
	CNetPartSIMP* pPart = (CNetPartSIMP*)(PartList[nIndexNetPart]);
	ASSERT(pPart != NULL); ASSERT_KINDOF(CNetPartSIMP,pPart);
	// pro pPart generuji v��kov� varianty a ur��m jejich cenu
	CObArray NewKnots;
	float    nAbsHeight = pMap->GetLandscapeHeight(pPart->m_nBaseRealPos.nPos);
	float    nRelHeight;
	// za�ad�m uzel se stejnou v��kou jako p�edchoz�
	{
		nRelHeight = (pParent != NULL)?(pParent->m_nHeight - nAbsHeight):(pList->m_nBaseRealPos.nHgt - nAbsHeight);
		/*if (nRelHeight < (float)pList->m_nMinHeight)
			nRelHeight = (float)pList->m_nMinHeight;
		if (nRelHeight > (float)pList->m_nMaxHeight)
			nRelHeight = (float)pList->m_nMaxHeight;*/
		// vlo��m uzel
		CKnotNetPart* pNewKnot  = new CKnotNetPart(pParent);
		NewKnots.Add(pNewKnot);
		// p�i�ad�m uzlu p��slu�n� parametry
		pNewKnot->m_nPartNumber = nIndexNetPart;
		pNewKnot->m_nRelHg		= nRelHeight;
		pNewKnot->m_nHeight		= nAbsHeight + nRelHeight;
		// spo��t�m cenu
		pNewKnot->m_nPrice		= DoCalcPriceForKnot(pNewKnot,PartList,pList,pMap,Params);
	}

	if (pPart->CanChangeSlope()&&(!pPart->DisChangeSlope()))
	{
		for(nRelHeight = 0; /*(float)pList->m_nMinHeight;*/;)
		{	// pro dan� nRelHeight vytvo��m uzel
			CKnotNetPart* pNewKnot = new CKnotNetPart(pParent);
			
			NewKnots.Add(pNewKnot);
			// p�i�ad�m uzlu p��slu�n� parametry
			pNewKnot->m_nPartNumber = nIndexNetPart;
			pNewKnot->m_nRelHg		= nRelHeight;
			pNewKnot->m_nHeight		= nAbsHeight + nRelHeight;
			// spo��t�m cenu
			pNewKnot->m_nPrice		= DoCalcPriceForKnot(pNewKnot,PartList,pList,pMap,Params);
			// nov� v��ka
			double nNewHeight = nRelHeight + Params.nHeightStep;
			/*if (nNewHeight <= pList->m_nMaxHeight)
				nRelHeight =  (float)nNewHeight;
			else
			if (nRelHeight <  (float)pList->m_nMaxHeight)
				nRelHeight =  (float)pList->m_nMaxHeight;
			else
			{
				//ASSERT(nRelHeight == (float)pList->m_nMaxHeight);
				break;	// konec generov�n�
			}*/
		}
	};
	// za�azen� jednotliv�ch uzl� do KnotsOpen
	for(int i=0; i<NewKnots.GetSize(); i++)
	{
		CKnotNetPart* pKnot = (CKnotNetPart*)(NewKnots[i]);
		ASSERT(pKnot != NULL);
		DoInsertKnotToOpenList(pKnot,KnotsOpen);
	}
};


/////////////////////////////////////////////////////////////////////////////
// Automatick� nastaven� v��ky v �seku s�t�

void DoAutoNetHeightSetup(CObArray& PartList,const CNetPartsList* pList,const CPoseidonMap* pMap,
						  REALNETPOS BgnPos,REALNETPOS EndPos,BOOL bFixEnd,AutoNetHgParams& Params)
{
	AfxGetApp()->BeginWaitCursor();

	CObList	 KnotsOpen;
	CObArray KnotsClose; // otev�en� a uzav�en� uzly

	TRY
	{
		int		nLastKnot = PartList.GetSize()-1;	// u tohoto uzlu kon��m

		CKnotNetPart* pFinal = NULL;	// v�sledek
		// vygeneruji po��te�n� uzly v bod� 0
		DoCreateKnotsForParent(NULL,KnotsOpen,PartList,pList,pMap,BgnPos,EndPos,bFixEnd,Params);
		// prov�d�m algoritmus
		while(pFinal == NULL)
		{
			// vyjmu nejlep�� prvek
			ASSERT(KnotsOpen.GetCount()>0);
			CKnotNetPart* pBest = (CKnotNetPart*)(KnotsOpen.RemoveHead());
			ASSERT(pBest != NULL);
			// vlo��m do uzav�en�ch
			KnotsClose.Add(pBest);
			// je-li posledn� -> konec
			if (pBest->m_nPartNumber ==  nLastKnot)	
			{
				ASSERT(pFinal == NULL);
				pFinal = pBest;
			} else
			{	// pokra�uji expanz� uzlu pBest
				ASSERT(pBest->m_nPartNumber < nLastKnot);
				DoCreateKnotsForParent(pBest,KnotsOpen,PartList,pList,pMap,BgnPos,EndPos,bFixEnd,Params);
			};
		};
		if (pFinal != NULL)
		{	// nastaven� relativn�ch v��ek objektu
			while(pFinal != NULL)
			{
				int nIndx = pFinal->m_nPartNumber;
				CNetPartSIMP* pNet = (CNetPartSIMP*)(PartList[nIndx]);
				//if (pNet)
				//{
				//	pNet->m_nRelaHeight = pFinal->m_nRelHg;
				//}
				pFinal = pFinal->m_pParent;
			}
		};
		// ru��m seznamy uzl�
		DestroyListObjects(&KnotsOpen);
		DestroyArrayObjects(&KnotsClose);
	}
	CATCH_ALL(e)
	{	// z�ejm� nedostatek pam�ti
		AfxGetApp()->EndWaitCursor();
		DestroyListObjects(&KnotsOpen);
		DestroyArrayObjects(&KnotsClose);
		THROW_LAST();
	}
	END_CATCH_ALL
	AfxGetApp()->EndWaitCursor();
};

BOOL DoAutoNetHeightFunction(const CNetPartsList* pList,const CPoseidonMap* pMap,BOOL bFixEnd,BOOL& bAutoTest)
{
	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	// nastaven� parametr� algoritmu
	AutoNetHgParams		AutoParams = pEnvir->m_optPosEd.m_NetHgtParams;
	/*
	AutoParams.nHeightStep	= 0.2;	// krok nastavov�n� v��ky
	// ocen�n� chyb
	AutoParams.nErrBadPrvHg = 100000;// nenavazuje na p�edchoz� d�l
	AutoParams.nErrBadNetUp = 50000;// s� "plave" na povrchem
	AutoParams.nErrBadNetDn = 50000;// si� "zabo�ena" pod povrch
	AutoParams.nErrBadSlope = 500;	// �patn� sklon d�lu	(cena za kazdy 0.1� navic)
	AutoParams.nErrBadPrvSl = 700;	// �patn� sklon k p�edchoz�mu d�lu (cena za kazdy 0.1� navic)
	// ocen�n� nastaven� d�lu
	AutoParams.nPricePrvSl = 10;	// cena za sklon k p�edchoz�mu d�lu (cena za kazdy 0.1�)
	AutoParams.nPriceSlope = 7;		// cena za sklon d�lu	(cena za kazdy 0.1�)
	AutoParams.nPriceStand = 2;		// cena za odchylku od standardn�ho vyno�en� (cena za 0.1 m)
	*/

	// nastaven� parametr� dialogu
	CMntPropertySheet	dlg(IDS_AUTO_NET_HEIGHT,PSH_NOAPPLYNOW);
	CAutoNetHeightPage0 page0; 
	CAutoNetHeightPage1 page1; 
	page0.m_bFixedEndDirectly = bFixEnd;
	page1.m_Params = AutoParams;

	dlg.AddPageIntoWizard(&page0);
	dlg.AddPageIntoWizard(&page1);
	if (dlg.DoModal()!=IDOK)
		return FALSE;
	bAutoTest  = page0.m_bEnableAutoTest;
	AutoParams = page1.m_Params;
	// ulo�en� parametr�
	pEnvir->m_optPosEd.m_NetHgtParams = AutoParams;

	TRY
	{
		// vyb�r�m �seky, pro kter� nastavuji samostatn�
		REALNETPOS	m_nBgnPos = pList->m_nBaseRealPos;
		CObArray	m_SubList;
		for(int nIndx  = 0; nIndx < pList->m_NetParts.GetSize(); nIndx++)
		{
			CNetPartSIMP* pPart = (CNetPartSIMP*)(pList->m_NetParts[nIndx ]);
			if (pPart != NULL)
			{	// m� tento d�l �anci m�nit v��ku
				/*if ((!pPart->m_bAutoHeight)&&((!pPart->CanChangeSlope())||(pPart->DisChangeSlope())))
				{	// tento d�l nem��e m�nit v��ku
					if (m_SubList.GetSize()>0)
					{	// nastav�m objekty tak, aby po��tek byl v m_nBgnPos
						// a konec v bod� B tohoto d�lu
						REALNETPOS EndPos = pPart->m_nConnRealPos;
						DoAutoNetHeightSetup(m_SubList,pList,pMap,
							  m_nBgnPos,EndPos,TRUE,AutoParams);
						// pokra�uji dal��m d�lem
						m_nBgnPos = pPart->m_nBaseRealPos;
						m_SubList.RemoveAll();
					}
				} else*/
				{	// tento d�l m��e m�nit v��ku
					m_SubList.Add(pPart);
				};
			}
		}
		if (m_SubList.GetSize()>0)
		{	// nastav�m objekty tak, aby po��tek byl v m_nBgnPos
			// a konec kdekoliv nebo v posledn�m d�le, je-li fixn�
			CNetPartSIMP* pEndPart = (CNetPartSIMP*)(pList->m_NetParts[pList->m_NetParts.GetSize()-1]);
			ASSERT(pEndPart);
			REALNETPOS EndPos = pEndPart->m_nBaseRealPos;
			DoAutoNetHeightSetup(m_SubList,pList,pMap,
				  m_nBgnPos,EndPos,bFixEnd||page0.m_bFixEndHeight,AutoParams);
		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		return FALSE;
	}
	END_CATCH_ALL
	return TRUE;
};


