/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CPosEdCursor

IMPLEMENT_DYNAMIC(CPosEdCursor,CObject)

CPosEdCursor::CPosEdCursor()
{
	m_pDocument		 = NULL;
	m_pEnvironment	 = GetPosEdEnvironment();

	// logick� poloha kamsi
	m_ptCurrentPos.x =
	m_ptCurrentPos.y = 500;
	m_ptLastLogPos.x = 
	m_ptLastLogPos.y = -1;
};

CPosEdCursor::~CPosEdCursor()
{};


// nastaven� polohy cursoru
void CPosEdCursor::SetCursorLogPosition(POINT ptPos,BOOL bFromBuldozer)
{
	ASSERT(m_pDocument != NULL);

	if ((m_ptCurrentPos.x != ptPos.x)||(m_ptCurrentPos.y != ptPos.y))
	{	// nov� logick� poloha
		m_ptLastLogPos = m_ptCurrentPos;
		m_ptCurrentPos = ptPos;
		// update zobrazen�
		m_pDocument->DoUpdateAllViews(CUpdatePar::uvfChngCursor);
	};
	if (!bFromBuldozer)
	{	// spo��t�m re�lnou polohu
		m_rpCurrentPos = m_pDocument->m_PoseidonMap.FromViewToRealPos(m_ptCurrentPos);
		// update v Buldozeru
		//if (m_pEnvironment->m_optPosEd.m_bBldSynchrAction)
		{
			m_rpBuldozrPos = m_rpCurrentPos;
			SendPositionToBuldozer();
		}
	}
};

void CPosEdCursor::SetCursorRealPosition(REALPOS ptPos,BOOL bFromBuldozer)
{
	ASSERT(m_pDocument != NULL);
	POINT ptLog = m_pDocument->m_PoseidonMap.FromRealToViewPos(ptPos);
	SetCursorLogPosition(ptLog,bFromBuldozer);
};

// update polohy mezi aplikacemi
void CPosEdCursor::SendPositionToBuldozer() const
{
	
    SMoveObjectPosMessData data;

	for(int i=0;i<4;i++)
		for(int j=0;j<4;j++)
		{
			data.Position[i][j] = 0.;
		};
	data.Position[0][3] = m_rpCurrentPos.x;
	data.Position[1][3] = m_pDocument->m_PoseidonMap.GetLandscapeHeight(m_rpCurrentPos);
	data.Position[2][3] = m_rpCurrentPos.z;
	data.Position[0][0] = 1.;
	data.Position[1][1] = 1.;
	data.Position[2][2] = 1.;
	data.Position[3][3] = 1.;
	if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->CursorPositionSet(data);
};

// vykreslov�n� kurzoru
void CPosEdCursor::OnDrawCursorInDP(CDC* pDC,const POINT& Pos,const CDrawPar* pParams) const
{
	int	  nWidth2 = pParams->m_pCfg->m_nCursorDevSz/2;
	CRect rPos(Pos.x-nWidth2,Pos.y-nWidth2,
			   Pos.x+nWidth2,Pos.y+nWidth2);

	CPen  pnCursor(PS_SOLID,pParams->m_pCfg->m_nCursorDevWd,pParams->m_pCfg->m_cCursorColor);
	CPen* pOldPen = pDC->SelectObject(&pnCursor);
	CBrush* pOldBr = (CBrush*)(pDC->SelectStockObject(NULL_BRUSH));
	// XOR or INV
	if (pParams->m_pCfg->m_nCursorDraw == CPosEdCfg::cursDrXor)
		pDC->SetROP2(R2_XORPEN);
	else
	if (pParams->m_pCfg->m_nCursorDraw == CPosEdCfg::cursDrInv)
		pDC->SetROP2(R2_NOT);
	 else
		pDC->SetROP2(R2_COPYPEN);
	// Draw
	switch(pParams->m_pCfg->m_nCursorType)
	{
	case CPosEdCfg::cursCross:
		pDC->MoveTo(rPos.left,rPos.top);
		pDC->LineTo(rPos.right,rPos.bottom);
		pDC->MoveTo(rPos.right,rPos.top);
		pDC->LineTo(rPos.left,rPos.bottom);
		break;
	case CPosEdCfg::cursRect:
		pDC->Rectangle(rPos);
		break;
	case CPosEdCfg::cursFull:
		{
		pDC->Rectangle(rPos);
		int nCross = min(rPos.Width()/3,pParams->m_pCfg->m_nCursorDevWd);
		rPos.InflateRect(-nCross,-nCross);
		pDC->MoveTo(rPos.left,rPos.top);
		pDC->LineTo(rPos.right,rPos.bottom);
		pDC->MoveTo(rPos.right,rPos.top);
		pDC->LineTo(rPos.left,rPos.bottom);
		}; break;
	};
	pDC->SelectObject(pOldBr);
	pDC->SelectObject(pOldPen);
};


