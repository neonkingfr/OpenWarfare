/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"
#include ".\poseidonedobjs.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Mapa kontrol� pro zobrazen�

typedef struct
{
	int  nSection;
	int  nCtrlID;
	bool resize;
} SCtrlMapItem;

SCtrlMapItem ObjBarCtrlMap[] = 
{
	// textury
	{ CPosObjectsPanel::grTypeTxtr, IDC_COMBO_TYPE, false },
	{ CPosObjectsPanel::grTypeTxtr, IDC_TXTR_LAND_TXT, false },
	{ CPosObjectsPanel::grTypeTxtr, IDC_TXTR_LAND, false },
	{ CPosObjectsPanel::grTypeTxtr, IDC_TXTR_ZONE_TXT, false },
	{ CPosObjectsPanel::grTypeTxtr, IDC_TXTR_ZONE, false },
	{ CPosObjectsPanel::grTypeTxtr, IDC_TEXTURES_TXT, false },
	{ CPosObjectsPanel::grTypeTxtr, IDC_TEXTURES, true },
	{ CPosObjectsPanel::grTypeTxtr, IDC_FILLAREA_TXTR, false },
	{ CPosObjectsPanel::grTypeTxtr, IDC_FILLAREA_ZONE, false },
	{ CPosObjectsPanel::grTypeTxtr, IDC_FILLAREA_LAND, false },
	{ CPosObjectsPanel::grTypeTxtr, IDC_FILLAREA_CHNG, false },  
	{ CPosObjectsPanel::grTypeTxtr, IDC_TOOLS_ANALYSE_TXTRS, false },

	// ---------------------------------------------------------- //
	// these lines are no more required after the introduction of //
	// the new project params dialog                              //
	// ---------------------------------------------------------- //
/*
	{ CPosObjectsPanel::grTypeTxtr, IDC_LAYERS_TXT, false },
	{ CPosObjectsPanel::grTypeTxtr, IDC_LAYERS, true },
*/

	// p��rodn� objekty
	{ CPosObjectsPanel::grTypeNtOb, IDC_COMBO_TYPE, false },
	{ CPosObjectsPanel::grTypeNtOb, IDC_LIST_NATOBJ, true },

 
	// lidsk� objekty
	{ CPosObjectsPanel::grTypePlOb, IDC_COMBO_TYPE, false },
	{ CPosObjectsPanel::grTypePlOb, IDC_LIST_PPLOBJ, true },

#ifndef __ONLY_FOR_PUBLIC__
	{ CPosObjectsPanel::grTypeNROb, IDC_COMBO_TYPE, false },
	{ CPosObjectsPanel::grTypeNROb, IDC_LIST_NEWROADSOBJ, true },
#endif

	// lesy
	{ CPosObjectsPanel::grTypeWood, IDC_COMBO_TYPE, false },
	{ CPosObjectsPanel::grTypeWood, IDC_LIST_WOODS, true },
	{ CPosObjectsPanel::grTypeWood, IDC_CREATE_WOOD, false },

	// s�t�
	{ CPosObjectsPanel::grTypeNets, IDC_COMBO_TYPE, false },
	{ CPosObjectsPanel::grTypeNets, IDC_LIST_NETS, true },

	// pojmenovan� oblasti
	{ CPosObjectsPanel::grTypeName, IDC_COMBO_TYPE, false },
	{ CPosObjectsPanel::grTypeName, IDC_LIST_NAMEAREAS, true },
	{ CPosObjectsPanel::grTypeName, IDC_GOTO_NAMEAREA, false  },
	{ CPosObjectsPanel::grTypeName, IDC_EDIT_NAMEAREA, false  },
	{ CPosObjectsPanel::grTypeName, IDC_SELECT_NAMEAREA, false},
	{ CPosObjectsPanel::grTypeName, IDC_SELOBJ_NAMEAREA, false},

	// kl��ov� body
	{ CPosObjectsPanel::grTypeKPla, IDC_COMBO_TYPE, false },
	{ CPosObjectsPanel::grTypeKPla, IDC_LIST_KEYPOINTS, true },
	{ CPosObjectsPanel::grTypeKPla, IDC_GOTO_KEYPOINT, false  },
	{ CPosObjectsPanel::grTypeKPla, IDC_EDIT_KEYPOINT, false  },
	{ CPosObjectsPanel::grTypeKPla, IDC_SELECT_KEYPOINT, false},
	{ CPosObjectsPanel::grTypeKPla, IDC_SELOBJ_KEYPOINT, false},

	// pojmenovan� objekty
	{ CPosObjectsPanel::grTypeNObj, IDC_COMBO_TYPE, false },
	{ CPosObjectsPanel::grTypeNObj, IDC_LIST_OBJNAMES, true },
	{ CPosObjectsPanel::grTypeNObj, IDC_GOTO_OBJNAME, false },

	//Editace krajiny  
	{ CPosObjectsPanel::grTypeLand, IDC_COMBO_TYPE, false },
	{ CPosObjectsPanel::grTypeLand, IDC_BTN_CHANGE_HEIGHT, false},
	{ CPosObjectsPanel::grTypeLand, IDC_BTN_EROSION, false},

	//Background images  
	{ CPosObjectsPanel::grTypeBgImage, IDC_COMBO_TYPE, false },
	{ CPosObjectsPanel::grTypeBgImage, IDC_BG_IMAGES, true},
	{ CPosObjectsPanel::grTypeBgImage, IDC_ADD_BG_IMAGE, false},
	{ CPosObjectsPanel::grTypeBgImage, IDC_BG_IMAGE_PROP, false},
	{ CPosObjectsPanel::grTypeBgImage, IDC_BG_IMAGE_DEL, false},
  
	// konec
	{ -1, -1, false },
};


/////////////////////////////////////////////////////////////////////////////
// CPosObjectsPanel

BEGIN_MESSAGE_MAP(CPosObjectsPanel, CDialogBar)
	//{{AFX_MSG_MAP(CPosObjectsPanel)
	ON_CBN_SELCHANGE(IDC_TXTR_LAND, OnSelchangeListLand)
	ON_CBN_SELCHANGE(IDC_TXTR_ZONE, OnSelchangeListZone)
	ON_LBN_DBLCLK(IDC_LIST_NAMEAREAS, OnDblSelectListNaAr)
	ON_LBN_DBLCLK(IDC_LIST_KEYPOINTS, OnDblSelectListKePt)
	ON_LBN_DBLCLK(IDC_LIST_OBJNAMES,  OnDblSelectListNmOb)

	// ---------------------------------------------------------- //
	// these lines are no more required after the introduction of //
	// the new project params dialog                              //
	// ---------------------------------------------------------- //
	ON_LBN_DBLCLK(IDC_LAYERS,		 OnDblSelectTextureLayers)

	//}}AFX_MSG_MAP
//  ON_WM_SIZING()
//  ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()


void CPosObjectsPanel::OnDblSelectListNaAr()
{
	AfxGetMainWnd()->SendMessage(WM_COMMAND,IDC_GOTO_NAMEAREA,0);
}

void CPosObjectsPanel::OnDblSelectListKePt()
{
	AfxGetMainWnd()->SendMessage(WM_COMMAND,IDC_GOTO_KEYPOINT,0);
}

void CPosObjectsPanel::OnDblSelectListNmOb()
{
	AfxGetMainWnd()->SendMessage(WM_COMMAND,IDC_GOTO_OBJNAME,0);
}

CPosObjectsPanel::CPosObjectsPanel()
{
	m_pCurrentDoc = NULL;
	m_iCurrentType = m_iPreviousType = 0;
}

BOOL CPosObjectsPanel::CreatePanel(CWnd* pParentWnd,UINT nID)
{
	BOOL bRes = Create(pParentWnd, IDD_OBJECT_DLGBAR, CBRS_TOP | CBRS_SIZE_DYNAMIC, nID);  
	if (bRes)
	{
		m_sizeFloating = m_sizeDocked = GetPosEdEnvironment()->m_appDockState.m_objectPanelSize;

		CComboBox* pTypeObj = (CComboBox*)GetDlgItem(IDC_COMBO_TYPE);
		pTypeObj->ResetContent();
		pTypeObj->AddString("Artificial objects");
		pTypeObj->AddString("Natural objects");
#ifndef __ONLY_FOR_PUBLIC__
		pTypeObj->AddString("New roads objects");
#endif
		pTypeObj->AddString("Road networks");
		pTypeObj->AddString("Key points");
		pTypeObj->AddString("Terrain vertices");
		pTypeObj->AddString("Background images");
		pTypeObj->AddString("Named zones");
		pTypeObj->AddString("Named objects");
		pTypeObj->AddString("Surfaces (OFP)");
		pTypeObj->AddString("Woods (OFP)");

		if (pTypeObj)
		{
			pTypeObj->SetCurSel(grTypePlOb);
			OnChangeTypeObjects();
		}

		// subclass controls
		m_cTxtrLand.m_nVariant = CVariantComboBox::itmVariantLand;
		m_cTxtrLand.SubclassDlgItem(IDC_TXTR_LAND, this);
		m_cTxtrZone.m_nVariant = CVariantComboBox::itmVariantZone;
		m_cTxtrZone.SubclassDlgItem(IDC_TXTR_ZONE, this);
		m_cTextures.m_nVariant = CVariantListBox::itmVariantTxtr;
		m_cTextures.SubclassDlgItem(IDC_TEXTURES, this);

		m_cTextureLayers.Init(IDR_LAYERS, IDR_LAYER, pParentWnd);
		m_cTextureLayers.SubclassDlgItem(IDC_LAYERS, this);
		
		m_cWoods.m_nVariant = CVariantListBox::itmVariantWood;
		m_cWoods.SubclassDlgItem(IDC_LIST_WOODS, this);

		m_cNets.m_nVariant = CVariantListBox::itmVariantNets;
		m_cNets.SubclassDlgItem(IDC_LIST_NETS, this);

		m_cNameAreas.SubclassDlgItem(IDC_LIST_NAMEAREAS, this);
		m_cKeyPoints.SubclassDlgItem(IDC_LIST_KEYPOINTS, this);
		m_cNameObjcs.SubclassDlgItem(IDC_LIST_OBJNAMES,  this);
		m_cBgImages.SubclassDlgItem(IDC_BG_IMAGES, this);

		m_cNatObjs.SubclassDlgItem(IDC_LIST_NATOBJ, this);
		m_cNatObjs.m_nVariant = CVariantListBox::itmVariantNObj;
		m_cPplObjs.SubclassDlgItem(IDC_LIST_PPLOBJ, this);
		m_cPplObjs.m_nVariant = CVariantListBox::itmVariantPObj;

#ifndef __ONLY_FOR_PUBLIC__
		m_cNewRoadsObjs.SubclassDlgItem(IDC_LIST_NEWROADSOBJ, this);
		m_cNewRoadsObjs.m_nVariant = CVariantListBox::itmVariantNRObj;
#endif
	}
	return bRes;
}

int	 CPosObjectsPanel::GetTypeObjects() const
{
#if _DEBUG  
	/*CComboBox*	pTypeObj = (CComboBox*)GetDlgItem(IDC_COMBO_TYPE);
	if (pTypeObj)
	{
		int nIndx = pTypeObj->GetCurSel();
		ASSERT(nIndx == m_iCurrentType);
	};*/
#endif

	return m_iCurrentType;
}

void CPosObjectsPanel::SetTypeObjects(int nType)
{
	CComboBox* pTypeObj = (CComboBox*)GetDlgItem(IDC_COMBO_TYPE);
	if (pTypeObj)
	{
		pTypeObj->SetCurSel(nType);
		OnChangeTypeObjects();
	}
}

void CPosObjectsPanel::OnChangeTypeObjects()
{
	CComboBox*	pTypeObj = (CComboBox*)GetDlgItem(IDC_COMBO_TYPE);
	if (pTypeObj)
	{
		m_iPreviousType = m_iCurrentType;
		m_iCurrentType = pTypeObj->GetCurSel();

		for (int i = 0; ObjBarCtrlMap[i].nSection >= 0; ++i)
		{
			if (ObjBarCtrlMap[i].nCtrlID > 0 && ObjBarCtrlMap[i].nCtrlID != IDC_COMBO_TYPE)
			{
				MntShowWindow(DLGCTRL(ObjBarCtrlMap[i].nCtrlID), (ObjBarCtrlMap[i].nSection == m_iCurrentType) ? SW_SHOW : SW_HIDE);
			}
		}

		CUpdatePar cUpdate(m_iCurrentType, NULL);
		
		if (m_pCurrentDoc) m_pCurrentDoc->UpdateAllViews(NULL, CUpdatePar::uvfEditMode, &cUpdate);
	}

	//Resize childs
	RecalcChildWindowPos(m_sizeFloating);
}

/////////////////////////////////////////////////////////////////////////////
// CPosObjectsPanel

void CPosObjectsPanel::OnSelchangeListLand()
{
	// ma�u obsah p�sem
	m_cTxtrZone.ResetContent();
	// aktu�ln� typ krajiny
	DWORD dwData;
	if (CBGetCurSelData(m_cTxtrLand.m_hWnd,dwData))
	{
		CPosTextureLand* pLand = (CPosTextureLand*)dwData;
		ASSERT_KINDOF(CPosTextureLand,pLand);
		// napln�n� seznamu p�sem
		for(int i=0; i<pLand->m_LandZones.GetSize(); i++)
		{
			CPosTextureZone* pZone = (CPosTextureZone*)(pLand->m_LandZones[i]);
      if (pZone)
        m_cTxtrZone.AddStringEx(pZone->m_sZoneName, (DWORD)pZone);
      //CBAddString(m_cTxtrZone.m_hWnd, pZone->m_sZoneName, (DWORD)pZone);
		}
		if (m_cTxtrZone.GetCount()>0)
			m_cTxtrZone.SetCurSel(0);
	}
	OnSelchangeListZone();
};

void CPosObjectsPanel::OnSelchangeListZone()
{
	// ma�u obsah textur
	m_cTextures.ResetContent();
	// aktu�ln� p�smo
	DWORD dwData;
	if (CBGetCurSelData(m_cTxtrZone.m_hWnd,dwData))
	{
		CPosTextureZone* pZone = (CPosTextureZone*)dwData;
		ASSERT_KINDOF(CPosTextureZone,pZone);
		// n�hodn� v�b�r
		LBAddStringID(m_cTextures.m_hWnd, IDS_RANDOM_TEXTURE_SEL, (DWORD)(NULL));
		// prim�rn� textura
		if (pZone->m_pPrimarTxtr)
    {
      CString name;
      /*if (pZone->m_pPrimarTxtr->m_nTimesUsedInLandActive > 0)
        name.Format(_T("%s %d"), pZone->m_pPrimarTxtr->m_sTextureName, pZone->m_pPrimarTxtr->m_nTimesUsedInLandActive);
      else*/
        name = pZone->m_pPrimarTxtr->m_sTextureName;
    
			LBAddString(m_cTextures.m_hWnd, name, (DWORD)(pZone->m_pPrimarTxtr.GetRef()));
    }
		// napln�n� seznamu variantn�ch textur
		for(int i=0; i<pZone->m_VarTxtTmpl.Size(); i++)
		{
			CPosTextureTemplate* pTxtr = (CPosTextureTemplate*)(pZone->m_VarTxtTmpl[i]);
      CString name;
      /*if (pTxtr->m_nTimesUsedInLandActive > 0)
        name.Format(_T("%s %d"), pTxtr->m_sTextureName, pTxtr->m_nTimesUsedInLandActive);
      else*/
        name = pTxtr->m_sTextureName;

			if (pTxtr)
				LBAddString(m_cTextures.m_hWnd, name, (DWORD)pTxtr);
		}
		if (m_cTextures.GetCount()>0)
			m_cTextures.SetCurSel(0);
	}
};


void CPosObjectsPanel::UpdateDocKPlaObjects()
{
	if (m_cKeyPoints.m_hWnd == NULL)
		return;

	DWORD dwData;
	BOOL  bLastSel = LBGetCurSelData(m_cKeyPoints.m_hWnd,dwData);
	m_cKeyPoints.ResetContent();
	if (m_pCurrentDoc != NULL)
	{
		for(int i=0; i<m_pCurrentDoc->m_PoseidonMap.GetPoseidonKeyPtCount();i++)
		{
			CPosKeyPointObject*	pArea = m_pCurrentDoc->m_PoseidonMap.GetPoseidonKeyPt(i);
			if (pArea)
			{
				LBAddString(m_cKeyPoints.m_hWnd, pArea->m_sName, (DWORD)pArea);
			}
		};
		if (m_cKeyPoints.GetCount()>0)
		{
			m_cKeyPoints.SetCurSel(0);
			if (bLastSel)
				LBSetCurSelData(m_cKeyPoints.m_hWnd,dwData);
		}
	}
};

void CPosObjectsPanel::UpdateDocNameObjects()
{
	if (m_cNameAreas.m_hWnd == NULL)
		return;

	DWORD dwData;
	BOOL  bLastSel = LBGetCurSelData(m_cNameAreas.m_hWnd,dwData);
	m_cNameAreas.ResetContent();
	if (m_pCurrentDoc != NULL)
	{
		for(int i=0; i<m_pCurrentDoc->m_PoseidonMap.GetPoseidonNameAreaCount();i++)
		{
			CPosNameAreaObject*	pArea = m_pCurrentDoc->m_PoseidonMap.GetPoseidonNameArea(i);
			if (pArea)
			{
				LBAddString(m_cNameAreas.m_hWnd, ((CPosObject *)pArea)->GetObjectName(), (DWORD)pArea);
			}
		};
		if (m_cNameAreas.GetCount()>0)
		{
			m_cNameAreas.SetCurSel(0);
			if (bLastSel)
				LBSetCurSelData(m_cNameAreas.m_hWnd,dwData);
		}
	}
};

void CPosObjectsPanel::UpdateDocNObjObjects()
{
	if (m_cNameObjcs.m_hWnd == NULL)
		return;

	DWORD dwData;
	BOOL  bLastSel = LBGetCurSelData(m_cNameObjcs.m_hWnd,dwData);
	m_cNameObjcs.ResetContent();
	if (m_pCurrentDoc != NULL)
	{
		for(int i=0; i<m_pCurrentDoc->m_PoseidonMap.GetPoseidonObjectCount();i++)
		{
			CPosEdObject*	pObj = m_pCurrentDoc->m_PoseidonMap.GetPoseidonObject(i);
			if ((pObj != NULL)&&(!((CPosObject*)pObj)->GetObjectName().IsEmpty()))
			{
				LBAddString(m_cNameObjcs.m_hWnd, ((CPosObject*)pObj)->GetObjectName(), (DWORD)pObj);
			}
		};
		if (m_cNameObjcs.GetCount()>0)
		{
			m_cNameObjcs.SetCurSel(0);
			if (bLastSel)
				LBSetCurSelData(m_cNameObjcs.m_hWnd,dwData);
		}
	}
};

void CPosObjectsPanel::UpdateDocBgImages()
{
  if (m_cBgImages.m_hWnd == NULL)
    return;

  DWORD dwData;
  BOOL  bLastSel = LBGetCurSelData(m_cBgImages.m_hWnd,dwData);
  m_cBgImages.ResetContent();
  if (m_pCurrentDoc != NULL)
  {
    for(int i=0; i<m_pCurrentDoc->m_PoseidonMap.GetBgImagesCount();i++)
    {
      CPosBackgroundImageObject *	pObj = m_pCurrentDoc->m_PoseidonMap.GetBgImage(i);
      if ((pObj != NULL)&&(pObj->m_sAreaName.GetLength()>0))
      {
        LBAddString(m_cBgImages.m_hWnd, pObj->m_sAreaName, (DWORD)pObj);
      }
    };
    if (m_cBgImages.GetCount()>0)
    {
      m_cBgImages.SetCurSel(0);
      if (bLastSel)
        LBSetCurSelData(m_cBgImages.m_hWnd,dwData);
    }
  }
};


void CPosObjectsPanel::UpdateDocTxtrLayers()
{
  if (m_cTextureLayers.m_hWnd == NULL)
    return;

  DWORD dwData;
  BOOL  bLastSel = LBGetCurSelData(m_cTextureLayers.m_hWnd,dwData);
  m_cTextureLayers.ResetContent();
  if (m_pCurrentDoc != NULL)
  {
    for(int i=0; i<m_pCurrentDoc->m_PoseidonMap.GetTextureLayersCount();i++)
    {
      Ref<CTextureLayer> layer = m_pCurrentDoc->m_PoseidonMap.GetTextureLayer(i);
      if ((layer != NULL)&&(layer->Name().GetLength()>0))
      {
        if (layer != m_pCurrentDoc->m_PoseidonMap.GetActiveTextureLayer())
          LBAddString(m_cTextureLayers.m_hWnd, layer->Name(), (DWORD)layer.GetRef());
        else
        {
          CString layerName;
          layerName = layer->Name() + _T(" (active)");
          LBAddString(m_cTextureLayers.m_hWnd, layerName, (DWORD)layer.GetRef());
        }
      }
    };
    if (m_cTextureLayers.GetCount()>0)
    {
      m_cTextureLayers.SetCurSel(0);
      if (bLastSel)
        LBSetCurSelData(m_cTextureLayers.m_hWnd,dwData);
    }
  }
};


void CPosObjectsPanel::UpdateDocNObjObject(CPosEdObject* pObjOld, CPosEdObject* pObjNew)
{
	if (m_cNameObjcs.m_hWnd == NULL)
		return;

	// Delete from list box. 
	if ((pObjOld != NULL)&&(!((CPosObject*)pObjOld)->GetObjectName().IsEmpty()))
	{
		LBDeleteString(m_cNameObjcs.m_hWnd, ((CPosObject*)pObjOld)->GetObjectName());			
	}

	// Add to list box
	if ((pObjNew != NULL)&&(!((CPosObject*)pObjNew)->GetObjectName().IsEmpty()))
	{
		LBAddString(m_cNameObjcs.m_hWnd, ((CPosObject*)pObjNew)->GetObjectName(), (DWORD)pObjNew);		
	}
}

void CPosObjectsPanel::UpdateDocData(CPosEdMainDoc* pDoc, BOOL bAlways)
{
	if ((pDoc != m_pCurrentDoc) || (bAlways))
	{
		m_pCurrentDoc = pDoc;
		if (pDoc != NULL)
		{	// propojen� dokumentu s panelem
			ASSERT((pDoc->m_pObjsPanel == NULL) || (pDoc->m_pObjsPanel == this));
			pDoc->m_pObjsPanel = this;
			CUpdatePar cUpdate(CUpdatePar::uvfEditMode, NULL);
			pDoc->UpdateAllViews(NULL, CUpdatePar::uvfEditMode, &cUpdate);
		};

		int i;
		///////////////////////////////////////////////////
		// TEXTURY
		if (m_pCurrentDoc != NULL)
		{
			// typ krajiny
			ASSERT(m_cTxtrLand.m_hWnd != NULL);
			m_cTxtrLand.ResetContent();
			for (i = 0; i < m_pCurrentDoc->m_MgrTextures.m_TextureLands.GetSize(); ++i)
			{
				CPosTextureLand* pLand = (CPosTextureLand*)(m_pCurrentDoc->m_MgrTextures.m_TextureLands[i]);
				if (pLand) m_cTxtrLand.AddStringEx(pLand->m_sLandName, (DWORD)pLand);
			  //CBAddString(m_cTxtrLand.m_hWnd, pLand->m_sLandName, (DWORD)pLand);
			}

			if (m_cTxtrLand.GetCount() > 0) m_cTxtrLand.SetCurSel(0);

			// layers
			UpdateDocTxtrLayers();
		} 
		else
		{
			// typ krajiny
			ASSERT(m_cTxtrLand.m_hWnd != NULL);
			m_cTxtrLand.ResetContent();

			ASSERT(m_cTextureLayers.m_hWnd != NULL);
			m_cTextureLayers.ResetContent();
		}
   
		OnSelchangeListLand();

		///////////////////////////////////////////////////
		// OBJEKTY
		m_cNatObjs.ResetContent();
		m_cPplObjs.ResetContent();
#ifndef __ONLY_FOR_PUBLIC__
		m_cNewRoadsObjs.ResetContent();
#endif
		if (m_pCurrentDoc != NULL)
		{
			for (int i = 0; i < m_pCurrentDoc->ObjectTemplatesSize(); ++i)
			{
				CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(m_pCurrentDoc->GetIthObjectTemplate(i));
				if (pTempl)
				{
					switch (pTempl->m_nObjType)
					{
						case CPosObjectTemplate::objTpNatural:
							LBAddString(m_cNatObjs.m_hWnd, pTempl->GetName(), (DWORD)pTempl);
							break;
						case CPosObjectTemplate::objTpPeople:
							LBAddString(m_cPplObjs.m_hWnd, pTempl->GetName(), (DWORD)pTempl);
							break;
#ifndef __ONLY_FOR_PUBLIC__
						case CPosObjectTemplate::objTpNewRoads:
							LBAddString(m_cNewRoadsObjs.m_hWnd, pTempl->GetName(), (DWORD)pTempl);
							break;
#endif
					}
				}
			}

			if (m_cNatObjs.GetCount() > 0) m_cNatObjs.SetCurSel(0);	
			if (m_cPplObjs.GetCount() > 0) m_cPplObjs.SetCurSel(0);
#ifndef __ONLY_FOR_PUBLIC__
			if (m_cNewRoadsObjs.GetCount() > 0) m_cNewRoadsObjs.SetCurSel(0);
#endif
		}

		///////////////////////////////////////////////////
		// LESY

		if (m_pCurrentDoc != NULL)
		{
			// typy les�
			ASSERT(m_cWoods.m_hWnd != NULL);
			m_cWoods.ResetContent();
			for(i = 0; i < m_pCurrentDoc->m_MgrWoods.m_WoodTemplates.GetSize(); ++i)
			{
				CPosWoodTemplate* pWood = (CPosWoodTemplate*)(m_pCurrentDoc->m_MgrWoods.m_WoodTemplates[i]);
				if (pWood) LBAddString(m_cWoods.m_hWnd, pWood->m_sWoodName, (DWORD)pWood);
			}
		} 
		else
		{
			// typ lesa
			ASSERT(m_cWoods.m_hWnd != NULL);
			m_cWoods.ResetContent();
		}

		if (m_cWoods.GetCount() > 0) m_cWoods.SetCurSel(0);

		///////////////////////////////////////////////////
		// S�T�

		if (m_pCurrentDoc != NULL)
		{
			// typy s�t�
			ASSERT(m_cNets.m_hWnd != NULL);
			m_cNets.ResetContent();
			for(i = 0; i < m_pCurrentDoc->m_MgrNets.m_NetTemplates.GetSize(); ++i)
			{
				CPosNetTemplate* pNet = (CPosNetTemplate*)(m_pCurrentDoc->m_MgrNets.m_NetTemplates[i]);
				if (pNet) LBAddString(m_cNets.m_hWnd, pNet->m_sNetName, (DWORD)pNet);
			}
		} 
		else
		{
			// typ s�t�
			ASSERT(m_cNets.m_hWnd != NULL);
			m_cNets.ResetContent();
		}

		if (m_cNets.GetCount() > 0) m_cNets.SetCurSel(0);

		///////////////////////////////////////////////////
		// POJMENOVAN� OBJEKTY a KL��OV� BODY
		UpdateDocNameObjects();
		UpdateDocKPlaObjects();
		UpdateDocNObjObjects();    
		// bg images
		UpdateDocBgImages();
	}
}

/////////////////////////////////////////////////////////////////////////////
// aktu�ln� objekty pro vlo�en�

CPosTextureTemplate* CPosObjectsPanel::GetCurrentTexture()
{
	CPosTextureZone* pZone = CPosObjectsPanel::GetFillTextureZone();
	if (pZone)
	{
		DWORD dwData;
		if (LBGetCurSelData(m_cTextures.m_hWnd,dwData))
		{	// textura
			CPosTextureTemplate* pTxtr = (CPosTextureTemplate*)dwData;
			if (pTxtr != NULL)
			{	// konkr�tn� textura
				ASSERT_KINDOF(CPosTextureTemplate,pTxtr);
				return pTxtr;
			}
			// n�hodn� v�b�r
			return pZone->GetRandomTexture();
		}
	};
	return NULL;
}

void CPosObjectsPanel::SetCurrentTexture(CPosTextureTemplate* text)
{
  SetTypeObjects(grTypeTxtr);
  if (text == NULL || text->m_pLandZone == NULL || text->m_pLandZone->m_pOwner == NULL)
    return;

  CPosTextureZone * txtZone = text->m_pLandZone;
  CBSetCurSelData(m_cTxtrLand.m_hWnd, (DWORD)txtZone->m_pOwner);
  OnSelchangeListLand();
  CBSetCurSelData(m_cTxtrZone.m_hWnd, (DWORD)txtZone);
  OnSelchangeListZone();

  // Find textuure by ID
  int nItems = m_cTextures.GetCount();  
  for(int i = 0; i < nItems; i++)
  {
    DWORD ItmData2 = m_cTextures.GetItemData(i);
    if (ItmData2 != 0 && ItmData2 != LB_ERR && ((CPosTextureTemplate *) ItmData2)->m_nTextureID  == text->m_nTextureID)
    {
      m_cTextures.SetCurSel(i);
      return;
    }
  }
  
}

void CPosObjectsPanel::SetCurrentObject(const CPosObjectTemplate* temp)
{
	if (temp == NULL) return;

	CVariantListBox* pList; 
	switch (temp->m_nObjType) 
	{
	case CPosObjectTemplate::objTpNatural:
		{
			SetTypeObjects(grTypeNtOb);
			pList = &m_cNatObjs;
		}
		break;
	case CPosObjectTemplate::objTpPeople:
		{
			SetTypeObjects(grTypePlOb);
			pList = &m_cPplObjs;
		}
		break;
#ifndef __ONLY_FOR_PUBLIC__
	case CPosObjectTemplate::objTpNewRoads:
		{
			SetTypeObjects(grTypeNROb);
			pList = &m_cNewRoadsObjs;
		}
		break;
#endif
	default:
		return;
	}

	int nItems = pList->GetCount();  
	for(int i = 0; i < nItems; ++i)
	{
		DWORD ItmData2 = pList->GetItemData(i);
		if (ItmData2 != 0 && ItmData2 != LB_ERR && ((CPosObjectTemplate*) ItmData2)->GetName() == temp->GetName())
		{
			pList->SetCurSel(i);
			return;
		}
	}
}

CPosObjectTemplate* CPosObjectsPanel::GetCurrentObject()
{
	CComboBox* pTypeObj = (CComboBox*)GetDlgItem(IDC_COMBO_TYPE);
	if (pTypeObj)
	{
		int nIndx = pTypeObj->GetCurSel();
		if (nIndx == grTypeNtOb)
		{	// p��rodn� objekty
			nIndx = m_cNatObjs.GetCurSel();
			if (nIndx != LB_ERR)
			{
				CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(m_cNatObjs.GetItemData(nIndx));
				return pTempl;
			}
		} 
		else if (nIndx == grTypePlOb)
		{	// lidsk� objekty
			nIndx = m_cPplObjs.GetCurSel();
			if (nIndx != LB_ERR)
			{
				CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(m_cPplObjs.GetItemData(nIndx));
				return pTempl;
			}
		}
#ifndef __ONLY_FOR_PUBLIC__
		else if (nIndx == grTypeNROb)
		{
			nIndx = m_cNewRoadsObjs.GetCurSel();
			if (nIndx != LB_ERR)
			{
				CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(m_cNewRoadsObjs.GetItemData(nIndx));
				return pTempl;
			}
		}
#endif
	}
	return NULL;
}

void CPosObjectsPanel::SetCurrentWood(CPosWoodTemplate* temp)
{
  SetTypeObjects(grTypeWood);

  int nItems = m_cWoods.GetCount();  
  for(int i = 0; i < nItems; i++)
  {
    DWORD ItmData2 = m_cWoods.GetItemData(i);
    if (ItmData2 != 0 && ItmData2 != LB_ERR && ((CPosWoodTemplate *) ItmData2)->m_sWoodName == temp->m_sWoodName)
    {
      m_cWoods.SetCurSel(i);
      return;
    }
  }
}

CPosWoodTemplate* CPosObjectsPanel::GetCurrentWood()
{
	CComboBox*	pTypeObj = (CComboBox*)GetDlgItem(IDC_COMBO_TYPE);
	if (pTypeObj)
	{
		int nIndx = pTypeObj->GetCurSel();
		if (nIndx == grTypeWood)
		{	// p��rodn� objekty
			int nIndx = m_cWoods.GetCurSel();
			if (nIndx != LB_ERR)
			{
				CPosWoodTemplate* pTempl = (CPosWoodTemplate*)(m_cWoods.GetItemData(nIndx));
				if (pTempl)
				{
					ASSERT_KINDOF(CPosWoodTemplate,pTempl);
					return pTempl; 
				}
			}
		}
	}
	return NULL;
}

void CPosObjectsPanel::SetCurrentNet(const CString& name)
{
	SetTypeObjects(grTypeNets);

	int nItems = m_cNets.GetCount();  
	for (int i = 0; i < nItems; ++i)
	{
		DWORD ItmData2 = m_cNets.GetItemData(i);
		if (ItmData2 != 0 && ItmData2 != LB_ERR && ((CPosNetTemplate *) ItmData2)->m_sNetName == name)
		{
			m_cNets.SetCurSel(i);
			return;
		}
	}
}

CPosNetTemplate* CPosObjectsPanel::GetCurrentNet()
{
	CComboBox* pTypeObj = (CComboBox*)GetDlgItem(IDC_COMBO_TYPE);
	if (pTypeObj)
	{
		int nIndx = pTypeObj->GetCurSel();
		if (nIndx == grTypeNets)
		{	// p��rodn� objekty
			int nIndx = m_cNets.GetCurSel();
			if (nIndx != LB_ERR)
			{
				CPosNetTemplate* pTempl = (CPosNetTemplate*)(m_cNets.GetItemData(nIndx));
				if (pTempl)
				{
					ASSERT_KINDOF(CPosNetTemplate, pTempl);
					return pTempl; 
				}
			}
		}
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////
// textury pro vypln�n� plochy

CPosTextureTemplate* CPosObjectsPanel::GetFillTextureTxtr()
{
	CPosTextureZone* pZone = CPosObjectsPanel::GetFillTextureZone();
	if (pZone)
	{
		DWORD dwData;
		if (LBGetCurSelData(m_cTextures.m_hWnd,dwData))
		{	// textura
			CPosTextureTemplate* pTxtr = (CPosTextureTemplate*)dwData;
			if (pTxtr != NULL)
			{	// konkr�tn� textura
				ASSERT_KINDOF(CPosTextureTemplate,pTxtr);
				return pTxtr;
			}
			if ((!pZone->m_bEnRandomVar)||(pZone->m_VarTxtTmpl.Size()==0))
			{	// nelze d�lat n�hodnou volbu
				return pZone->m_pPrimarTxtr;
			}
			// n�hodn� v�b�r pro v�pl� nem� smysl
			return NULL;
		}
	};
	return NULL;
}

CPosTextureZone*	 CPosObjectsPanel::GetFillTextureZone()
{
	CPosTextureLand* pLand = GetFillTextureLand();
	if (pLand != NULL)
	{
		DWORD dwData;
		if (CBGetCurSelData(m_cTxtrZone.m_hWnd,dwData))
		{
			CPosTextureZone* pZone = (CPosTextureZone*)dwData;
			if (pZone != NULL)
			{
				ASSERT_KINDOF(CPosTextureZone,pZone);
				return pZone;
			}
		}
	}
	return NULL;
}

CPosTextureLand*	 CPosObjectsPanel::GetFillTextureLand()
{
	CComboBox*	pTypeObj = (CComboBox*)GetDlgItem(IDC_COMBO_TYPE);
	if (pTypeObj)
	{
		int nIndx = pTypeObj->GetCurSel();
		if (nIndx == grTypeTxtr)
		{	// aktu�ln� je str�nka textura
			DWORD dwData;
			if (CBGetCurSelData(m_cTxtrLand.m_hWnd,dwData))
			{
				CPosTextureLand* pLand = (CPosTextureLand*)dwData;
				if (pLand != NULL)
				{
					ASSERT_KINDOF(CPosTextureLand,pLand);
					return pLand;
				}
			}
		}
	};
	return NULL;
}

void CPosObjectsPanel::SetCurrentNameArea(CPosNameAreaObject* name)
{
  SetTypeObjects(grTypeName);

  int nItems = m_cNameAreas.GetCount();  
  for(int i = 0; i < nItems; i++)
  {
    DWORD ItmData2 = m_cNameAreas.GetItemData(i);
    if (ItmData2 != 0 && ItmData2 != LB_ERR && ((CPosNameAreaObject *) ItmData2)->GetID() == name->GetID())
    {
      m_cNameAreas.SetCurSel(i);
      return;
    }
  }
}

// pojmen. plochy a klic. body
CPosNameAreaObject*	CPosObjectsPanel::GetCurrentNameArea()
{
	if (m_cNameAreas.m_hWnd == NULL)
		return NULL;
	DWORD dwData;
	if (LBGetCurSelData(m_cNameAreas.m_hWnd,dwData))
		return ((CPosNameAreaObject*)dwData);
	return NULL;
}

void CPosObjectsPanel::SetCurrentKeyPoint(CPosKeyPointObject* keyPoint)
{
  SetTypeObjects(grTypeKPla);

  int nItems = m_cKeyPoints.GetCount();  
  for(int i = 0; i < nItems; i++)
  {
    DWORD ItmData2 = m_cKeyPoints.GetItemData(i);
    if (ItmData2 != 0 && ItmData2 != LB_ERR && ((CPosKeyPointObject *) ItmData2)->m_sName == keyPoint->m_sName)
    {
      m_cKeyPoints.SetCurSel(i);
      return;
    }
  }
}

CPosKeyPointObject* CPosObjectsPanel::GetCurrentKeyPoint()
{
	if (m_cKeyPoints.m_hWnd == NULL)
		return NULL;
	DWORD dwData;
	if (LBGetCurSelData(m_cKeyPoints.m_hWnd,dwData))
		return ((CPosKeyPointObject*)dwData);
	return NULL;
}

void CPosObjectsPanel::SetCurrentBGImage(const CString& bgImageID)
{
  int nItems = m_cBgImages.GetCount();  
  for(int i = 0; i < nItems; i++)
  {
    DWORD ItmData2 = m_cBgImages.GetItemData(i);
    if (ItmData2 != 0 && ItmData2 != LB_ERR && ((CPosBackgroundImageObject *) ItmData2)->m_sAreaName == bgImageID)
    {
      m_cBgImages.SetCurSel(i);
      return;
    }
  }
}

CPosBackgroundImageObject* CPosObjectsPanel::GetCurrentBGImage()
{
  if (m_cBgImages.m_hWnd == NULL)
    return NULL;
  DWORD dwData;
  if (LBGetCurSelData(m_cBgImages.m_hWnd,dwData))
    return ((CPosBackgroundImageObject*)dwData);
  return NULL;
}

CTextureLayer* CPosObjectsPanel::GetCurrentTextureLayer()
{
	if (m_cTextureLayers.m_hWnd == NULL) return NULL;

	DWORD dwData;
	if (LBGetCurSelData(m_cTextureLayers.m_hWnd, dwData)) return ((CTextureLayer *)dwData);

	return NULL;
}

CPosEdObject* CPosObjectsPanel::GetCurrentNameObjc()
{
	if (m_cNameObjcs.m_hWnd == NULL) return NULL;

	DWORD dwData;
	if (LBGetCurSelData(m_cNameObjcs.m_hWnd, dwData)) return ((CPosEdObject*)dwData);

	return NULL;
}

CSize CPosObjectsPanel::CalcDynamicDim(int nLength, DWORD dwMode)
{
  // Return default if it is being docked or floated
  if ((dwMode & LM_VERTDOCK) || (dwMode & LM_HORZDOCK))
  {
    if (dwMode & LM_STRETCH) // if not docked stretch to fit
      return CSize((dwMode & LM_HORZ) ? 32767 : m_sizeDocked.cx,
      (dwMode & LM_HORZ) ? m_sizeDocked.cy : 32767);
    else
      return m_sizeDocked;
  }
  if (dwMode & LM_MRUWIDTH)
    return m_sizeFloating;
  // In all other cases, accept the dynamic length
  if (dwMode & LM_LENGTHY)
    return CSize(m_sizeFloating.cx, /*(m_bChangeDockedSize) ?*/
    m_sizeFloating.cy = m_sizeDocked.cy = nLength /*:
                                                  m_sizeFloating.cy = nLength*/);
  else
  return CSize(/*(m_bChangeDockedSize) ?*/
  m_sizeFloating.cx = m_sizeDocked.cx = nLength /*:
                                                m_sizeFloating.cx = nLength*/, m_sizeFloating.cy);
}

#define MIN_OBJECTSPANEL_SIZE_X 100
#define MIN_OBJECTSPANEL_SIZE_Y 300 

CSize CPosObjectsPanel::CalcDynamicLayout(int nLength, DWORD dwMode)
{
  CSize size = CalcDynamicDim(nLength, dwMode);
  if (size.cx < MIN_OBJECTSPANEL_SIZE_X)
    size.cx = MIN_OBJECTSPANEL_SIZE_X;

  if (size.cy < MIN_OBJECTSPANEL_SIZE_Y)
    size.cy = MIN_OBJECTSPANEL_SIZE_Y;

  RecalcChildWindowPos(size);
  return size;
}

#define SPACE_X 7
#define SPACE_Y 5
#define START_AT 11

void CPosObjectsPanel::RecalcChildWindowPos(const CSize& size)
{
  // Find whole size and size of items, that can be resized
  int baseSize = 0;
  int resizeSize = 0; 
  for(int i=0; ObjBarCtrlMap[i].nSection >= 0; i++)
  {
    if (ObjBarCtrlMap[i].nCtrlID > 0 && (ObjBarCtrlMap[i].nSection == m_iCurrentType))
    {
      CWnd * wnd = GetDlgItem(ObjBarCtrlMap[i].nCtrlID);
      CRect rect;
      wnd->GetWindowRect(rect);

      baseSize += SPACE_Y + max(rect.Height(),5);
      if (ObjBarCtrlMap[i].resize)
        resizeSize += max(rect.Height(),5);
    }
  } 
  baseSize -= resizeSize;

  int resultResizeSize = size.cy - baseSize - START_AT;
  if (resultResizeSize <= 0)
    return; 

  float coef = resultResizeSize * 1.0f / resizeSize;

  // Resize items
  int width = size.cx - 2 * SPACE_X;
  int y = START_AT;
  for(int i=0; ObjBarCtrlMap[i].nSection >= 0; i++)
  {
    if (ObjBarCtrlMap[i].nCtrlID > 0 && (ObjBarCtrlMap[i].nSection == m_iCurrentType))
    {
      CWnd * wnd = GetDlgItem(ObjBarCtrlMap[i].nCtrlID);
      CRect rect;
      wnd->GetWindowRect(rect);
      ScreenToClient(rect);

      int height = max(rect.Height(),5);

      rect.top = y;
      rect.left = SPACE_X;
      rect.right = rect.left + width;

      if (ObjBarCtrlMap[i].resize)
        rect.bottom = rect.top + (LONG) (height * coef);
      else
        rect.bottom = rect.top + height;

      y = rect.bottom + SPACE_Y;

      wnd->MoveWindow(rect,TRUE);
    }
  }
}

void CPosObjectsPanel::OnDblSelectTextureLayers()
{
	if ((m_cTextureLayers.m_hWnd == NULL)||(m_pCurrentDoc == NULL)) return;

	DWORD dwData;
	if (!LBGetCurSelData(m_cTextureLayers.m_hWnd, dwData)) return;

	Ref<CTextureLayer> layer = (CTextureLayer *) dwData;
	m_pCurrentDoc->m_PoseidonMap.SwitchActiveTextureLayer(layer);

	UpdateDocTxtrLayers();
}