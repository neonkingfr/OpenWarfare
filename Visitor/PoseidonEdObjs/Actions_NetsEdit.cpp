/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"
#include "AutoNetGener.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef Fail
#undef Fail
#endif

/////////////////////////////////////////////////////////////////////////////
// CShowNetKeyCtrl - v�b�r atribut�

#define BMP_PATTERN 90

CShowNetKeyCtrl::CShowNetKeyCtrl()
{
	m_bmpTypes.LoadBitmap(IDB_NETKEY_TYPE);
}

CShowNetKeyCtrl::~CShowNetKeyCtrl()
{
	if (m_bmpTypes.m_hObject) m_bmpTypes.DeleteObject();
}

void CShowNetKeyCtrl::SetNetKeyType(int KeyPartType, WORD KeyPartSubtype, float nGrad)
{
	m_nKeyPartType		= KeyPartType;
	m_nKeyPartSubtype	= KeyPartSubtype;
	m_nKeyPartGrad		= nGrad;

//****************************************
/*
	// uprv�m na standardn� �hly
	if ((nGrad >= 45) && (nGrad < 135))
	{
		m_nKeyPartGrad	= 90;
	}
	else if ((nGrad >= 135) && (nGrad < 225))
	{
		m_nKeyPartGrad	= 180;
	}
	else if ((nGrad >= 225) && (nGrad < 315))
	{
		m_nKeyPartGrad	= 270;
	}
	else
	{
		m_nKeyPartGrad	= 0;
	}
*/
//****************************************

	if (m_hWnd != NULL) Invalidate();
}

BEGIN_MESSAGE_MAP(CShowNetKeyCtrl, CButton)
	//{{AFX_MSG_MAP(CShowNetKeyCtrl)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

WNDPROC* CShowNetKeyCtrl::GetSuperWndProcAddr()
{
	static WNDPROC NEAR pfnSuperListAttrCtrl;
	return &pfnSuperListAttrCtrl;
}

void CShowNetKeyCtrl::DrawItem(LPDRAWITEMSTRUCT)
{
}

BOOL CShowNetKeyCtrl::OnEraseBkgnd(CDC* pDC)
{
	CRect rClip, rClient;	
	// v�pl�
	pDC->GetClipBox(&rClip);
	GetClientRect(rClient);
	rClip.IntersectRect(rClip, rClient);
	pDC->DrawFrameControl(rClip, DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_PUSHED);
	rClip.DeflateRect(2, 2);

	::FillRect(pDC->m_hDC, &rClip, (HBRUSH)::GetStockObject(WHITE_BRUSH));
	return TRUE;
}

void CShowNetKeyCtrl::OnPaint()
{
	CPaintDC dc(this); 
	if (m_bmpTypes.m_hObject == NULL) return;
	// vykresl�m BITMAPU DLE TYPU
	int nBmpX = 0, nBmpY = 0; // sektor bitmapy
	
	// index podle typu
	switch(m_nKeyPartType)
	{
	case CNetPartKEY::typeTerm:	// A		0
		nBmpX = 0; 	break;
	case CNetPartKEY::typeStra:	// A,B		1
	case CNetPartKEY::typeSpec:
		nBmpX = 1;  break;
	case CNetPartKEY::typeCross:
		{
			switch(m_nKeyPartSubtype)
			{
			case CPosCrossTemplate::netCROSS_I:	// I
				nBmpX = 2;  break;
			case CPosCrossTemplate::netCROSS_L:	// L
				nBmpX = 3;  break;
			case CPosCrossTemplate::netCROSS_P:	// P
				nBmpX = 4;  break;
			case CPosCrossTemplate::netCROSS_K:	// K
				nBmpX = 5;  break;
			}
		}
		break;
	}

	//**************************************************
	/*
	// index Y pole orientace
	switch(m_nKeyPartGrad)
	{
	case  90: nBmpY = 1; break;
	case 180: nBmpY = 2; break;
	case 270: nBmpY = 3; break;
	}
	*/
	//**************************************************

	CRect rPos, rClient;	
	GetClientRect(rClient);
	PaintSpecBitmapRect(dc.m_hDC, (HBITMAP)(m_bmpTypes.m_hObject), rClient,
						CPoint(nBmpX * BMP_PATTERN, nBmpY * BMP_PATTERN), 
						CSize(BMP_PATTERN, BMP_PATTERN), SRCCOPY);
}

/////////////////////////////////////////////////////////////////////////////
// CEditNetKeyObjectDlg dialog

class CEditNetKeyObjectDlg : public CDialog
{
	// Construction
public:
	CEditNetKeyObjectDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
	CPosNetObjectKey*	m_pKeyObj;
	CPoseidonMap*		m_pMap;
	CMgrNets*			m_pNetMgr;
	CPosEdMainDoc*		m_pDocument;

	BOOL				m_bGenerNextKey;
	REALNETPOS			m_nGenerNextKey;
	CPosNetTemplate*	m_pGenerNextNet;

	//{{AFX_DATA(CEditNetKeyObjectDlg)
	enum { IDD = IDD_EDIT_NETKEY_OBJECT };
	CShowNetKeyCtrl		m_cShowType;
	//}}AFX_DATA

	// Overrides
	void DoUpdateInfoData();      
	void DoEditNetList(int nList);
	void DoGenerNext(int nList);
	void DoExportImportEditNetList(int nList, std::istream* in, std::ostream* out);

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditNetKeyObjectDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEditNetKeyObjectDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeOrientation();
	afx_msg void OnNetList1();
	afx_msg void OnNetList2();
	afx_msg void OnNetList3();
	afx_msg void OnNetList4();
	afx_msg void OnGenerNext1();
	afx_msg void OnGenerNext2();
	afx_msg void OnGenerNext3();
	afx_msg void OnGenerNext4();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedExportnet();
	afx_msg void OnBnClickedImportnet();
	void ImportNet(std::istream& in);
	void ExportNet(std::ostream& out);
};

CEditNetKeyObjectDlg::CEditNetKeyObjectDlg(CWnd* pParent /*=NULL*/)
: CDialog(CEditNetKeyObjectDlg::IDD, pParent)
{
	m_bGenerNextKey = FALSE;
	m_pGenerNextNet = NULL;
	m_pKeyObj = NULL;
	//{{AFX_DATA_INIT(CEditNetKeyObjectDlg)
	//}}AFX_DATA_INIT
}

static int CtrlsListBtn[] = 
{ 
	IDC_NETLIST_1,
	IDC_NETLIST_2,
	IDC_NETLIST_3,
	IDC_NETLIST_4 
};

static int CtrlsListTxt[] = 
{ 
	IDC_NET_TXT_A,
	IDC_NET_TXT_B,
	IDC_NET_TXT_C,
	IDC_NET_TXT_D 
};

static int CtrlsListNet[] = 
{ 
	IDC_NET_NAME_A,
	IDC_NET_NAME_B,
	IDC_NET_NAME_C,
	IDC_NET_NAME_D 
};

static int CtrlsListGen[] = 
{ 
	IDC_GENER_NEXT_1,
	IDC_GENER_NEXT_2,
	IDC_GENER_NEXT_3,
	IDC_GENER_NEXT_4 
};

BOOL CEditNetKeyObjectDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// update informativn�ch dat
	DoUpdateInfoData();
	// validace tla��tek �sek�
	for (int i = 0; i < CNetPartKEY::maxSections; ++i)
	{
		BOOL bIsValid = (i < m_pKeyObj->m_pBasePartKEY->m_nCountSections);
		::ShowWindow(DLGCTRL(CtrlsListBtn[i]), bIsValid ? SW_SHOW : SW_HIDE);
		::ShowWindow(DLGCTRL(CtrlsListNet[i]), bIsValid ? SW_SHOW : SW_HIDE);
		::ShowWindow(DLGCTRL(CtrlsListTxt[i]), bIsValid ? SW_SHOW : SW_HIDE);
		::ShowWindow(DLGCTRL(CtrlsListGen[i]), bIsValid ? SW_SHOW : SW_HIDE);
		// vlo��m jm�no s�t�
		if (bIsValid)
		{
			CNetPartsList* pList = (CNetPartsList*)(m_pKeyObj->m_PartLists[i]);
			if (pList) ::SetWindowText(DLGCTRL(CtrlsListNet[i]), pList->m_nTemplateName);
		}
	}
	// z�m�na "C" za "D"
	if ((m_pKeyObj->m_pBasePartKEY->m_nKeyPartType == CNetPartKEY::typeCross) &&
		(m_pKeyObj->m_pBasePartKEY->m_nKeyPartSubtype == CPosCrossTemplate::netCROSS_P))
	{
		ASSERT(m_pKeyObj->m_pBasePartKEY->m_nCountSections == 3);
		TCHAR buff[100];
		::GetWindowText(DLGCTRL(IDC_NETLIST_4), buff, 99);
		::SetWindowText(DLGCTRL(IDC_NETLIST_3), buff);
	}
	// update orientace
	OnChangeOrientation();
	return TRUE;
}

void CEditNetKeyObjectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditNetKeyObjectDlg)
	DDX_Control(pDX, IDC_NET_TYPE, m_cShowType);
	//}}AFX_DATA_MAP
	if (!SAVEDATA)
	{	// orientace
		MDDX_Text(pDX, IDC_ORIENTATION, m_pKeyObj->m_nBaseRealPos.nGra);

		// new code
		//*******************************************************************
		MDDV_MinMaxFloat(pDX, m_pKeyObj->m_nBaseRealPos.nGra, -360.0, 360.0, IDC_ORIENTATION, .01);
		//*******************************************************************

		// old code
		//*******************************************************************
		//MDDV_MinMaxInt(pDX, m_pKeyObj->m_nBaseRealPos.nGra, -360, 360, IDC_ORIENTATION, 10);
		//*******************************************************************
	} 
	else
	{	
		// orientace
		// new code
		//********************
		float nPom;
		//********************

		// old code
		//********************
		//int nPom;
		//********************

		MDDX_Text(pDX, IDC_ORIENTATION, nPom);

		// new code
		//*******************************************************************
		MDDV_MinMaxFloat(pDX, nPom, -360.0, 360.0, IDC_ORIENTATION, .01);
		m_pKeyObj->m_nBaseRealPos.nGra = GetNormalizedGrad((float)SpecRound(nPom, SR_001));
		//*******************************************************************

		// old code
		//*******************************************************************
		//MDDV_MinMaxInt(pDX, nPom, -360, 360, IDC_ORIENTATION, 10);
		//m_pKeyObj->m_nBaseRealPos.nGra = GetNormalizedGrad((int)SpecRound(nPom, SR_10));
		//*******************************************************************
	}

	// min. a max zabo�en�
//	double nMin,nMax,nVal = m_pKeyObj->m_pBasePartKEY->m_nRelaHeight;
//	nMin = m_pKeyObj->m_pBasePartKEY->m_nMinPartHg;
//	nMax = m_pKeyObj->m_pBasePartKEY->m_nMaxPartHg;
//
//	MDDX_Text(pDX, IDC_EDIT_POSY, nVal);
//	MDDV_MinMaxDouble(pDX, nVal, nMin, nMax, IDC_EDIT_POSY, 0.1);
//	if (SAVEDATA)
//	{
//		m_pKeyObj->m_pBasePartKEY->m_nRelaHeight = (float)nVal;
//	};
	// poloha objektu
	double nMaxX = (float)(m_pMap->GetSize().x * m_pMap->GetDocument()->m_nRealSizeUnit);
	double nMaxZ = (float)(m_pMap->GetSize().z * m_pMap->GetDocument()->m_nRealSizeUnit);
	double nVal = m_pKeyObj->m_nBaseRealPos.nPos.x;
	MDDX_Text(pDX, IDC_EDIT_POSX, nVal);
	MDDV_MinMaxDouble(pDX, nVal, 0., nMaxX, IDC_EDIT_POSX, NET_ALIGN_GRID);
	if (SAVEDATA) m_pKeyObj->m_nBaseRealPos.nPos.x = (float)nVal;
	nVal = m_pKeyObj->m_nBaseRealPos.nPos.z;
	MDDX_Text(pDX, IDC_EDIT_POSZ, nVal);
	MDDV_MinMaxDouble(pDX, nVal, 0., nMaxZ, IDC_EDIT_POSZ, NET_ALIGN_GRID);
	if (SAVEDATA) m_pKeyObj->m_nBaseRealPos.nPos.z = (float)nVal;
}

BEGIN_MESSAGE_MAP(CEditNetKeyObjectDlg, CDialog)
	//{{AFX_MSG_MAP(CEditNetKeyObjectDlg)
	ON_EN_CHANGE(IDC_ORIENTATION, OnChangeOrientation)
	ON_BN_CLICKED(IDC_NETLIST_1, OnNetList1)
	ON_BN_CLICKED(IDC_NETLIST_2, OnNetList2)
	ON_BN_CLICKED(IDC_NETLIST_3, OnNetList3)
	ON_BN_CLICKED(IDC_NETLIST_4, OnNetList4)
	ON_BN_CLICKED(IDC_GENER_NEXT_1, OnGenerNext1)
	ON_BN_CLICKED(IDC_GENER_NEXT_2, OnGenerNext2)
	ON_BN_CLICKED(IDC_GENER_NEXT_3, OnGenerNext3)
	ON_BN_CLICKED(IDC_GENER_NEXT_4, OnGenerNext4)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_EXPORTNET, OnBnClickedExportnet)
	ON_BN_CLICKED(IDC_IMPORTNET, OnBnClickedImportnet)
END_MESSAGE_MAP()

// update informativn�ch dat
void CEditNetKeyObjectDlg::DoUpdateInfoData()
{
	ASSERT(m_pKeyObj != NULL);

	CString sKeyObjType;
	// typ objektu
	switch(m_pKeyObj->m_pBasePartKEY->m_nKeyPartType)
	{
	case CNetPartKEY::typeCross:
		{
			sKeyObjType.LoadString(IDS_KEY_OBJ_TYPE_CROSS);
		}  
		break;
	case CNetPartKEY::typeStra:
		{
			AfxFormatString1(sKeyObjType, IDS_KEY_OBJ_TYPE_STRA, m_pKeyObj->m_pBasePartKEY->m_nTemplateName);
		}	
		break;
	case CNetPartKEY::typeSpec:
		{
			AfxFormatString1(sKeyObjType, IDS_KEY_OBJ_TYPE_SPEC, m_pKeyObj->m_pBasePartKEY->m_nTemplateName);
		}  
		break;
	case CNetPartKEY::typeTerm:
		{
			AfxFormatString1(sKeyObjType, IDS_KEY_OBJ_TYPE_TERM, m_pKeyObj->m_pBasePartKEY->m_nTemplateName);
		}
		break;
	default:
		ASSERT(FALSE);
	}
	// nastaven� titulku
	::SetWindowText(DLGCTRL(IDC_OBJECT_TYPE), sKeyObjType);
	::SetWindowText(DLGCTRL(IDC_OBJECT_NAME), m_pKeyObj->m_pBasePartKEY->m_nComponentName);
}

void CEditNetKeyObjectDlg::OnChangeOrientation() 
{
	// new code
	//**********************
	float nGrad;
	//**********************

	// old code
	//**********************
	//int nGrad;
	//**********************

	if (GetEditFloat(DLGCTRL(IDC_ORIENTATION), nGrad))
	{
		nGrad = GetNormalizedGrad(nGrad);
		// update zobrazen�
		m_cShowType.SetNetKeyType(m_pKeyObj->m_pBasePartKEY->m_nKeyPartType, m_pKeyObj->m_pBasePartKEY->m_nKeyPartSubtype, nGrad);
	}
}

void CEditNetKeyObjectDlg::OnNetList1() 
{	
	DoEditNetList(0); 
}

void CEditNetKeyObjectDlg::OnNetList2() 
{	
	DoEditNetList(1); 
}

void CEditNetKeyObjectDlg::OnNetList3() 
{	
	DoEditNetList(2); 
}

void CEditNetKeyObjectDlg::OnNetList4() 
{	
	DoEditNetList(3); 
}

extern BOOL DoEditNetList(CNetPartsList* pList, CPosNetTemplate* pNet, CPosEdMainDoc* pDocument, CPosNetObjectBase* pMainObject);

void CEditNetKeyObjectDlg::DoEditNetList(int nList)
{
	if (!UpdateData(TRUE)) return;
	// list
	CNetPartsList* pList = (CNetPartsList*)(m_pKeyObj->m_PartLists[nList]);
	if (pList == NULL)
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	// mus�m naj�t �ablonu s�t�
	CPosNetTemplate* pNet = m_pNetMgr->GetNetTemplate(pList->m_nTemplateName);
	if (pNet == NULL)
	{
		CString sText;
		AfxFormatString1(sText, IDS_UNKNOWN_NET_DEFINITION, pList->m_nTemplateName);
		AfxMessageBox(sText, MB_OK | MB_ICONEXCLAMATION);
		return;
	}
	// provedu nastaven� sou�adnic objektu
	m_pKeyObj->DoUpdateNetPartsBaseRealPos();
	// provedu editace �seku
	::DoEditNetList(pList, pNet, m_pDocument, m_pKeyObj);
}

BOOL DoExportImportEditNetList(CNetPartsList* pList, CPosNetTemplate* pNet, CPosEdMainDoc* pDocument,
                               CPosNetObjectBase* pMainObject, std::istream* in, std::ostream* out);

void CEditNetKeyObjectDlg::DoExportImportEditNetList(int nList, std::istream* in, std::ostream* out)
{
	if (nList >= m_pKeyObj->m_PartLists.GetCount()) return;
	CNetPartsList* pList = (CNetPartsList*)(m_pKeyObj->m_PartLists[nList]);
	if (pList == NULL) return;
	CPosNetTemplate* pNet = m_pNetMgr->GetNetTemplate(pList->m_nTemplateName);
	if (pNet == NULL)
	{
		CString sText;
		AfxFormatString1(sText, IDS_UNKNOWN_NET_DEFINITION, pList->m_nTemplateName);
		AfxMessageBox(sText, MB_OK | MB_ICONEXCLAMATION);
		return;
	}
	::DoExportImportEditNetList(pList, pNet, m_pDocument, m_pKeyObj, in, out);
	if (out) *out << "-\n";
}

void CEditNetKeyObjectDlg::DoGenerNext(int nList)
{
	CNetPartsList* pList = (CNetPartsList*)(m_pKeyObj->m_PartLists[nList]);
	if (pList == NULL)
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	ASSERT(m_pNetMgr != NULL);
	m_pGenerNextNet = m_pNetMgr->GetNetTemplate(pList->m_nTemplateName);
	if (m_pGenerNextNet == NULL)
	{
		CString sText;
		AfxFormatString1(sText, IDS_NET_NOT_FOUND, pList->m_nTemplateName);
		AfxMessageBox(sText, MB_OK | MB_ICONEXCLAMATION);
		return;
	}

	if (UpdateData(TRUE))
	{
		if (AfxMessageBox(IDS_GENER_NEXT_KEY,MB_YESNO | MB_ICONQUESTION) != IDYES) return;
		EndDialog(IDOK);
	}
	m_bGenerNextKey = TRUE;
	m_nGenerNextKey = pList->GetNetPartsEndRealPos();
}

void CEditNetKeyObjectDlg::OnGenerNext1() 
{	
	DoGenerNext(0); 
}

void CEditNetKeyObjectDlg::OnGenerNext2() 
{	
	DoGenerNext(1); 
}

void CEditNetKeyObjectDlg::OnGenerNext3() 
{	
	DoGenerNext(2); 
}

void CEditNetKeyObjectDlg::OnGenerNext4() 
{	
	DoGenerNext(3); 
}

/////////////////////////////////////////////////////////////////////////////
// editace kl��ov�ho bodu

static BOOL bInDoEditNetKeyObject = FALSE;

BOOL DoEditNetKeyObject(CPosNetObjectKey* pKeyObj, CPosEdMainDoc* pDocument)
{
	if (bInDoEditNetKeyObject) return TRUE;
	bInDoEditNetKeyObject = TRUE;

	CEditNetKeyObjectDlg dlg;
	// kl��ov� objekt
	ASSERT(pKeyObj != NULL);
	ASSERT(pKeyObj->m_pBasePartKEY != NULL);
	// nastaven� parametr�
	dlg.m_pKeyObj = pKeyObj;
	dlg.m_pMap	 = &(pDocument->m_PoseidonMap);
	dlg.m_pNetMgr= &(pDocument->m_MgrNets);
	dlg.m_pDocument = pDocument;

	// update registrovan�ch objekt� podle definovan�ch
	pKeyObj->UpdateNetsObjectsFromDefObjects(&(pDocument->m_MgrNets));

	if (dlg.DoModal() != IDOK)
	{
		bInDoEditNetKeyObject = FALSE;
		return FALSE;
	}
	// potvrzena editace

	// update pozic
	pKeyObj->DoUpdateNetPartsBaseRealPos();
	pKeyObj->DoUpdateNetPartsLogPosition();

	bInDoEditNetKeyObject = FALSE;

	if (dlg.m_bGenerNextKey)
	{
		Action_CreatePoseidonNextKeyNet(dlg.m_nGenerNextKey, dlg.m_pGenerNextNet, pDocument, NULL);
	}
	return TRUE;
}

static BOOL bInDoEditNetNorObject = FALSE;

BOOL DoEditNetNorObject(CPosNetObjectNor* pNetObj, CPosEdMainDoc* pDocument)
{
	if (bInDoEditNetNorObject) return TRUE;
	bInDoEditNetNorObject = TRUE;

	CEditNetKeyObjectDlg dlg;
	// kl��ov� objekt
	ASSERT(pNetObj != NULL);

	// list
	CNetPartsList* pList = &(pNetObj->m_PartList);
	if (pList == NULL)
	{
		MessageBeep(MB_DEFAULT);
		bInDoEditNetNorObject = FALSE;
		return FALSE;
	}
	// mus�m naj�t �ablonu s�t�
	CPosNetTemplate* pNet = pDocument->m_MgrNets.GetNetTemplate(pList->m_nTemplateName);
	if (pNet == NULL)
	{
		CString sText;
		AfxFormatString1(sText, IDS_UNKNOWN_NET_DEFINITION, pList->m_nTemplateName);
		AfxMessageBox(sText, MB_OK | MB_ICONEXCLAMATION);
		bInDoEditNetNorObject = FALSE;
		return FALSE;
	}
	// provedu nastaven� sou�adnic objektu
	pNetObj->DoUpdateNetPartsBaseRealPos();
	// provedu editace �seku
	if (::DoEditNetList(pList, pNet, pDocument, pNetObj))
	{
		// update pozic
		pNetObj->DoUpdateNetPartsBaseRealPos();
		pNetObj->DoUpdateNetPartsLogPosition();
		bInDoEditNetNorObject = FALSE;
		return TRUE;
	}
	bInDoEditNetNorObject = FALSE;
	return FALSE;
}

CPosAction* Action_EditPoseidonNetKey(CPosNetObjectKey* pNet, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup, BOOL bEdit)
{
	if (pNet != NULL)
	{
		CPosActionGroup*  pGrpAction  = NULL;
		CPosNetObjectKey* pAct = (CPosNetObjectKey*)(pNet->CreateCopyObject());
		if (pAct != NULL)
		{	// objekt dokumentu, se kter�m hejbu
			CPosNetObjectKey* pDocObj = (CPosNetObjectKey*)pDocument->GetDocumentObjectFromValue(pAct);
			ASSERT(pDocObj != NULL);
			ASSERT_KINDOF(CPosNetObjectKey, pAct);
			ASSERT_KINDOF(CPosNetObjectKey, pDocObj);
			// editace objektu
			if (bEdit)
			{
				if (!DoEditNetKeyObject(pAct, pDocument))
				{
					delete pAct;
					return NULL;
				}
			}
			// update z�kladn�ch re�ln�ch sou�adnic objekt�
			pAct->DoUpdateNetPartsBaseRealPos();
			// update logick�ch sou�adnic objekt� s�t�
			pAct->DoUpdateNetPartsLogPosition();

			// vytvo�en� skupinov� ud�losti
			pGrpAction = new CPosActionGroup(IDA_NET_KEY_EDIT);
			pGrpAction->m_bFromBuldozer = FALSE;

			// generuji ud�lost pro editaci atributu
			CPosObjectAction* pKeyAction = (CPosObjectAction*) pDocument->GenerNewObjectAction(IDA_NET_KEY_EDIT,pDocObj,pAct,FALSE,pGrpAction);
			pKeyAction->m_nSelectObjType = CPosObjectAction::ida_SelectExclu;

			// generov�n�/update z�kladn�ch objekt� s�t�
			if (!DoUpdateNetPoseidonObjects(pAct, pDocument, pGrpAction))
			{
				if (pGrpAction)
				{
					delete pGrpAction;
				}
				else if (pAct)
				{
					delete pAct;
				}
				return NULL;
			}

			// provedu akci
			if (pToGroup)
			{
				pToGroup->AddAction(pGrpAction);
			}
			else
			{
				pDocument->ProcessEditAction(pGrpAction);
			}
		}
		return pGrpAction;
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////
// editace �seku

CPosAction* Action_EditPoseidonNetNor(CPosNetObjectNor* pNet,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pNet != NULL)
	{
		CPosActionGroup*  pGrpAction  = NULL;
		CPosNetObjectNor* pAct = (CPosNetObjectNor*)(pNet->CreateCopyObject());
		if (pAct != NULL)
		{	// objekt dokumentu, se kter�m hejbu
			CPosNetObjectNor* pDocObj = (CPosNetObjectNor*)pDocument->GetDocumentObjectFromValue(pAct);
			ASSERT(pDocObj != NULL);
			ASSERT_KINDOF(CPosNetObjectNor, pAct);
			ASSERT_KINDOF(CPosNetObjectNor, pDocObj);
			// editace objektu
			if (!DoEditNetNorObject(pAct, pDocument))
			{
				delete pAct;
				return NULL;
			}
			// update z�kladn�ch re�ln�ch sou�adnic objekt�
			pAct->DoUpdateNetPartsBaseRealPos();
			// update logick�ch sou�adnic objekt� s�t�
			pAct->DoUpdateNetPartsLogPosition();

			// vytvo�en� skupinov� ud�losti
			pGrpAction = new CPosActionGroup(IDA_NET_KEY_EDIT);
			pGrpAction->m_bFromBuldozer = FALSE;

			// generuji ud�lost pro editaci atributu
			pDocument->GenerNewObjectAction(IDA_NET_KEY_EDIT, pDocObj, pAct, FALSE, pGrpAction);

			// generov�n�/update z�kladn�ch objekt� s�t�
			if(!DoUpdateNetPoseidonObjects(pAct, pDocument, pGrpAction))
			{
				if (pGrpAction)
				{
					delete pGrpAction;
				}
				else if (pAct)
				{
					delete pAct;
				}
				return NULL;
			}

			// provedu akci
			if (pToGroup)
			{
				pToGroup->AddAction(pGrpAction);
			}
			else
			{
				pDocument->ProcessEditAction(pGrpAction);
			}
		}
		return pGrpAction;
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////
// CGenerNetPage0 dialog

class CGenerNetPage0 : public CMntPropertyPage
{
	// Construction
public:
	CGenerNetPage0();
	~CGenerNetPage0();

	// Dialog Data
	CPosEdMainDoc*	  m_pDocument;
	CPosNetObjectKey* m_pKeyFrom;
	CPosNetObjectKey* m_pKeyTo;
	REALPOS			  m_rPtTo;

	// v�sledky
	CNetPartsList*	  m_pListFrom;
	CNetPartsList*	  m_pListTo;
	CPosNetTemplate*  m_pNetTemplate;

	BOOL			  m_bAutoClose;
	AutoNetGenParams* m_Params;

	//{{AFX_DATA(CGenerNetPage0)
	enum { IDD = IDD_GENER_NET_PAGE0 };
	CComboBox	m_cComboNetEnd;
	CComboBox	m_cComboNetBgn;
	CShowNetKeyCtrl	m_cShowTypeEnd;
	CShowNetKeyCtrl	m_cShowTypeBgn;	
	//}}AFX_DATA

	// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CGenerNetPage0)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CGenerNetPage0)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedFitEndGrad();
};

CGenerNetPage0::CGenerNetPage0() 
: CMntPropertyPage(CGenerNetPage0::IDD)
{
	m_pListFrom = NULL;
	m_pListTo   = NULL;
	m_pNetTemplate = NULL;
	m_bAutoClose   = FALSE;
	m_Params = NULL;	
}

CGenerNetPage0::~CGenerNetPage0()
{
}

BOOL CGenerNetPage0::OnInitDialog() 
{
	CMntPropertyPage::OnInitDialog();
	// update obr�zk�
	m_cShowTypeBgn.SetNetKeyType(m_pKeyFrom->m_pBasePartKEY->m_nKeyPartType, m_pKeyFrom->m_pBasePartKEY->m_nKeyPartSubtype, m_pKeyFrom->m_nBaseRealPos.nGra);

	if (m_pKeyTo != NULL)
	{
		m_cShowTypeEnd.SetNetKeyType(m_pKeyTo->m_pBasePartKEY->m_nKeyPartType, m_pKeyTo->m_pBasePartKEY->m_nKeyPartSubtype, m_pKeyTo->m_nBaseRealPos.nGra);
		// ukryt� konce
		::ShowWindow(DLGCTRL(IDC_TEXT_END_POSITION),SW_HIDE); 
		::ShowWindow(DLGCTRL(IDC_END_POSITION),SW_HIDE);
		::ShowWindow(DLGCTRL(IDC_COMBO_END_GRA_TXT),SW_HIDE);
		::ShowWindow(DLGCTRL(IDC_COMBO_END_GRA),SW_HIDE);
		::ShowWindow(DLGCTRL(IDC_FIT_END_GRAD),SW_HIDE);
	}
	else
	{
		m_cShowTypeEnd.ShowWindow(SW_HIDE);
		m_cComboNetEnd.ShowWindow(SW_HIDE);
		// text
		CString sText,sPom;
		NumToStandardString(sText,(double)m_rPtTo.x, 3);
		NumToStandardString(sPom,(double)m_rPtTo.z, 3);
		sText += _T(" : ");
		sText += sPom;
		::SetWindowText(DLGCTRL(IDC_END_POSITION),sText);

    if (!m_Params->bFitEndAngle)
    {
      ::EnableWindow(DLGCTRL(IDC_COMBO_END_GRA_TXT), FALSE);
      ::EnableWindow(DLGCTRL(IDC_COMBO_END_GRA), FALSE);
    }
	}
	
	// validace tla��tek �sek�
	for(int i=0; i<CNetPartKEY::maxSections; i++)
	{
		BOOL bIsValid = (i < m_pKeyFrom->m_pBasePartKEY->m_nCountSections);
		if (bIsValid)
		{
			TCHAR sName[100]; lstrcpy(sName,_T("A"));
			sName[0] += i;
			// z�m�na "C" za "D"
			if ((m_pKeyFrom->m_pBasePartKEY->m_nKeyPartType == CNetPartKEY::typeCross)&&
				(m_pKeyFrom->m_pBasePartKEY->m_nKeyPartSubtype == CPosCrossTemplate::netCROSS_P)&&
				(i==2))
			{
				ASSERT(m_pKeyFrom->m_pBasePartKEY->m_nCountSections == 3);
				sName[0] += 1;
			}
			lstrcat(sName,_T("   ["));
			// prid�m jm�no s�t�
			CNetPartsList* pList = (CNetPartsList*)(m_pKeyFrom->m_PartLists[i]);
			if (pList)
				lstrcat(sName,pList->m_nTemplateName);
			lstrcat(sName,_T("]"));
			m_cComboNetBgn.AddString(sName);
		}
		if (m_pKeyTo!=NULL)
		{
			bIsValid = (i < m_pKeyTo->m_pBasePartKEY->m_nCountSections);
			if (bIsValid)
			{
				TCHAR sName[100]; lstrcpy(sName,_T("A"));
				sName[0] += i;
				// z�m�na "C" za "D"
				if ((m_pKeyTo->m_pBasePartKEY->m_nKeyPartType == CNetPartKEY::typeCross)&&
					(m_pKeyTo->m_pBasePartKEY->m_nKeyPartSubtype == CPosCrossTemplate::netCROSS_P)&&
					(i==2))
				{
					ASSERT(m_pKeyTo->m_pBasePartKEY->m_nCountSections == 3);
					sName[0] += 1;
				}
				lstrcat(sName,_T("   ["));
				// prid�m jm�no s�t�
				CNetPartsList* pList = (CNetPartsList*)(m_pKeyTo->m_PartLists[i]);
				if (pList)
					lstrcat(sName,pList->m_nTemplateName);
				lstrcat(sName,_T("]"));
				m_cComboNetEnd.AddString(sName);
			}
		}
	}
	m_cComboNetBgn.SetCurSel(0);
	m_cComboNetEnd.SetCurSel(0);
	// v�b�r nejbli���ho bodu
	if (m_pKeyTo == NULL)
	{
		int nBest = 0;
		double nMinSize = 0.;
		for (int i = 0; i < CNetPartKEY::maxSections; ++i)
		{
			if (i < m_pKeyFrom->m_pBasePartKEY->m_nCountSections)
			{
				//REALNETPOS	rPos = m_pKeyFrom->m_pBasePartKEY->GetRealPosForSection(i,&(m_pDocument->m_PoseidonMap));
				REALNETPOS rPos = m_pKeyFrom->GetRealPosForSection(i, &(m_pDocument->m_PoseidonMap));
				if (i == 0)
				{
					nMinSize = GetDistancePoints(m_rPtTo, rPos.nPos);
				}
				else
				{
					double nDist = GetDistancePoints(m_rPtTo, rPos.nPos);
					if (nDist < nMinSize)
					{
						nBest = i;
						nMinSize = nDist;
					}
				}
			}
		}
		m_cComboNetBgn.SetCurSel(nBest);
		// c�lov�� �hel
		//REALNETPOS	rPos = m_pKeyFrom->m_pBasePartKEY->GetRealPosForSection(0,&(m_pDocument->m_PoseidonMap));
		REALNETPOS rPos = m_pKeyFrom->GetRealPosForSection(nBest, &(m_pDocument->m_PoseidonMap));

		//new code
		//*******************************************************
		double nGrad = 90.0 - GetPointsRad(m_rPtTo, rPos.nPos); // nGra is in logical coordinates angle from vertical up line in clockwise
		m_Params->nEndPosition.nGra = GetNormalizedGrad((float)SpecRound(GetGradFromRad(nGrad), SR_001)); // 
		//*******************************************************

		//old code
		//*******************************************************
		//double nGrad = 90 - GetPointsRad(m_rPtTo, rPos.nPos); // nGra is in logical coordinates angle from vertical up line in clockwise
		//m_Params->nEndPosition.nGra = GetNormalizedGrad((int)SpecRound(GetIntGradFromRad(nGrad), SR_10)); // 
		//*******************************************************

		UpdateData(FALSE);
	}

	if (m_bAutoClose)
	{
		if (UpdateData(TRUE))
		{
			((CPropertySheet*)GetParent())->EndDialog(IDOK);
		}
	}
	return TRUE;
}

void CGenerNetPage0::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGenerNetPage0)
	DDX_Control(pDX, IDC_COMBO_NET_END, m_cComboNetEnd);
	DDX_Control(pDX, IDC_COMBO_NET_BGN, m_cComboNetBgn);
	DDX_Control(pDX, IDC_SHOW_TYPE_END, m_cShowTypeEnd);
	DDX_Control(pDX, IDC_SHOW_TYPE_BGN, m_cShowTypeBgn);
	//}}AFX_DATA_MAP
	if (m_pKeyTo==NULL)
	{
		MDDX_Text(pDX, IDC_COMBO_END_GRA, m_Params->nEndPosition.nGra);
		MDDV_MinMaxLong(pDX, m_Params->nEndPosition.nGra, -360, 360, IDC_COMBO_END_GRA, 10);
		DDX_Check(pDX, IDC_FIT_END_GRAD, m_Params->bFitEndAngle);
	}

	if (SAVEDATA)
	{
		CNetPartsList* pListFrom = NULL;
		CNetPartsList* pListTo   = NULL;

		MDDV_CBValidSelItem(pDX, IDC_COMBO_NET_BGN, IDS_NO_VALID_NET_LIST);
		int nBgnIndx = m_cComboNetBgn.GetCurSel();
		if ((nBgnIndx >= 0)&&(nBgnIndx <= m_pKeyFrom->m_PartLists.GetSize()))
			pListFrom = (CNetPartsList*)(m_pKeyFrom->m_PartLists[nBgnIndx]);
		if (m_pKeyTo !=NULL)
		{
			MDDV_CBValidSelItem(pDX, IDC_COMBO_NET_END, IDS_NO_VALID_NET_LIST);
			int nEndIndx = m_cComboNetEnd.GetCurSel();
			if ((nEndIndx >= 0)&&(nEndIndx <= m_pKeyTo->m_PartLists.GetSize()))
				pListTo   = (CNetPartsList*)(m_pKeyTo->m_PartLists[nEndIndx]);
		}
		if ((pListFrom == NULL)||((pListTo == NULL)&&(m_pKeyTo!=NULL)))
		{
			AfxMessageBox(IDS_NO_VALID_NET_LIST,MB_OK|MB_ICONEXCLAMATION);
			pDX->Fail();
		}
		// stejn� sit ?
		if (pListTo != NULL)
		{
			if (lstrcmp(pListFrom->m_nTemplateName,pListTo->m_nTemplateName)!=0)
			{
				AfxMessageBox(IDS_NO_SAME_NET_TYPE_LISTS,MB_OK|MB_ICONEXCLAMATION);
				pDX->Fail();
			}
		}
		// bude nata�en mezi �seky pListFrom & pListTo
		m_pListFrom = pListFrom;
		m_pListTo   = pListTo;
		m_pNetTemplate = m_pDocument->m_MgrNets.GetNetTemplate(pListFrom->m_nTemplateName);
		if (m_pNetTemplate == NULL)
		{	// s� ji� neexistuje
			CString sText;
			AfxFormatString1(sText,IDS_NET_NOT_FOUND,pListFrom->m_nTemplateName);
			AfxMessageBox(sText,MB_OK|MB_ICONEXCLAMATION);
			sText.Empty();
			pDX->Fail();
		}
	}
}

void CGenerNetPage0::OnBnClickedFitEndGrad()
{
  CButton * pCheck = (CButton *) GetDlgItem(IDC_FIT_END_GRAD);
  if (pCheck->GetCheck() == BST_CHECKED)
  {
    ::EnableWindow(DLGCTRL(IDC_COMBO_END_GRA_TXT), TRUE);
    ::EnableWindow(DLGCTRL(IDC_COMBO_END_GRA), TRUE);
  }
  else
  {
    ::EnableWindow(DLGCTRL(IDC_COMBO_END_GRA_TXT), FALSE);
    ::EnableWindow(DLGCTRL(IDC_COMBO_END_GRA), FALSE);
  }
}



BEGIN_MESSAGE_MAP(CGenerNetPage0, CMntPropertyPage)
	//{{AFX_MSG_MAP(CGenerNetPage0)
	//}}AFX_MSG_MAP
  ON_BN_CLICKED(IDC_FIT_END_GRAD, OnBnClickedFitEndGrad)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGenerNetPage1 dialog

class CGenerNetPage1 : public CMntPropertyPage
{
// Construction
public:
	CGenerNetPage1();
	~CGenerNetPage1();

// Dialog Data
	AutoNetGenParams	* m_Params;

	//{{AFX_DATA(CGenerNetPage1)
	enum { IDD = IDD_GENER_NET_PAGE1 };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CGenerNetPage1)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CGenerNetPage1)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

CGenerNetPage1::CGenerNetPage1() : m_Params(NULL), CMntPropertyPage(CGenerNetPage1::IDD)
{
	//{{AFX_DATA_INIT(CGenerNetPage1)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CGenerNetPage1::~CGenerNetPage1()
{
}

void CGenerNetPage1::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGenerNetPage1)
	//}}AFX_DATA_MAP
/* 	
  MDDX_Text(pDX, IDC_EDIT_STEP_HGHT, m_Params.nHeightStep);
	MDDV_MinMaxDouble(pDX, m_Params.nHeightStep, 0.01, 1., IDC_EDIT_STEP_HGHT, 0.1);

	MDDX_Text(pDX, IDC_EDIT_GRAD_ABS, m_Params.nPriceSlope);
	MDDV_MinMaxLong(pDX, m_Params.nPriceSlope, 0, 1000, IDC_EDIT_GRAD_ABS, 1);
	MDDX_Text(pDX, IDC_EDIT_GRAD_REL, m_Params.nPricePrvSl);
	MDDV_MinMaxLong(pDX, m_Params.nPricePrvSl, 0, 1000, IDC_EDIT_GRAD_REL, 1);
	MDDX_Text(pDX, IDC_EDIT_STD_DIFER, m_Params.nPriceStand);
	MDDV_MinMaxLong(pDX, m_Params.nPriceStand, 0, 1000, IDC_EDIT_STD_DIFER, 1);

	MDDX_Text(pDX, IDC_EDIT_STRA0, m_Params.nSTRA[0]);
	MDDV_MinMaxLong(pDX, m_Params.nSTRA[0], 0, 1000, IDC_EDIT_STRA0, 1);
	MDDX_Text(pDX, IDC_EDIT_STRA1, m_Params.nSTRA[1]);
	MDDV_MinMaxLong(pDX, m_Params.nSTRA[1], 0, 1000, IDC_EDIT_STRA1, 1);
	MDDX_Text(pDX, IDC_EDIT_STRA2, m_Params.nSTRA[2]);
	MDDV_MinMaxLong(pDX, m_Params.nSTRA[2], 0, 1000, IDC_EDIT_STRA2, 1);

	MDDX_Text(pDX, IDC_EDIT_BEND0, m_Params.nBEND[0]);
	MDDV_MinMaxLong(pDX, m_Params.nBEND[0], 0, 1000, IDC_EDIT_BEND0, 1);
	MDDX_Text(pDX, IDC_EDIT_BEND1, m_Params.nBEND[1]);
	MDDV_MinMaxLong(pDX, m_Params.nBEND[1], 0, 1000, IDC_EDIT_BEND1, 1);
	MDDX_Text(pDX, IDC_EDIT_BEND2, m_Params.nBEND[2]);
	MDDV_MinMaxLong(pDX, m_Params.nBEND[2], 0, 1000, IDC_EDIT_BEND2, 1);
	MDDX_Text(pDX, IDC_EDIT_BEND3, m_Params.nBEND[3]);
	MDDV_MinMaxLong(pDX, m_Params.nBEND[3], 0, 1000, IDC_EDIT_BEND3, 1);
*/
  MDDX_Text(pDX, IDC_EDIT_HEURISTIC_COEF, m_Params->fBaseHeuristicCoef);
  MDDV_MinMaxDouble(pDX, m_Params->fBaseHeuristicCoef, 0, 20, IDC_EDIT_HEURISTIC_COEF, 0.2);

  DDX_Check(pDX, IDC_CHCK_AVOID_HILLS, m_Params->bAvoidHills);
  MDDX_Text(pDX, IDC_EDIT_AVOID_HILLS_COEF, m_Params->fAvoidHillsCoef);
  MDDV_MinMaxDouble(pDX, m_Params->fAvoidHillsCoef, 0, 20, IDC_EDIT_AVOID_HILLS_COEF, 1);

  DDX_Check(pDX, IDC_CHCK_PUNISH_CMPCHNG, m_Params->bPunishCompChange);
  MDDX_Text(pDX, IDC_EDIT_PUNISH_CMPCHNG_COEF, m_Params->fPunishCompChangeCoef);
  MDDV_MinMaxDouble(pDX, m_Params->fPunishCompChangeCoef, 0, 20, IDC_EDIT_PUNISH_CMPCHNG_COEF, 1);

  DDX_Check(pDX, IDC_CHCK_MAX_SLOPE, m_Params->bMaxSlope);
  MDDX_Text(pDX, IDC_EDIT_MAX_SLOPE, m_Params->nMaxSlope);
  MDDV_MinMaxInt(pDX, m_Params->nMaxSlope, 0, 90, IDC_EDIT_MAX_SLOPE, 1);

  DDX_Check(pDX, IDC_CHCK_MAX_SLOPE_CHANGE, m_Params->bMaxSlopeChange);
  MDDX_Text(pDX, IDC_EDIT_MAX_SLOPE_CHANGE, m_Params->nMaxSlopeChange);
  MDDV_MinMaxInt(pDX, m_Params->nMaxSlopeChange, 0, 90, IDC_EDIT_MAX_SLOPE_CHANGE, 1);

  MDDX_Text(pDX, IDC_EDIT_TOL_POS, m_Params->nSamePosTol);
  MDDV_MinMaxDouble(pDX, m_Params->nSamePosTol, 0., 100., IDC_EDIT_TOL_POS, 0.1);
  //MDDX_Text(pDX, IDC_EDIT_TOL_HGT, m_Params->nSamePosHgt);
  //MDDV_MinMaxDouble(pDX, m_Params->nSamePosHgt, 0., 100., IDC_EDIT_TOL_HGT, 0.1);
  MDDX_Text(pDX, IDC_EDIT_TOL_GRA, m_Params->nSameGraTol);
  MDDV_MinMaxLong(pDX, m_Params->nSameGraTol, 0, 360, IDC_EDIT_TOL_GRA, 1);
  
  MDDX_Text(pDX, IDC_EDIT_LIM_VAR, m_Params->nVarisLimit);
  MDDV_MinMaxLong(pDX, m_Params->nVarisLimit, 0, 10000000, IDC_EDIT_LIM_VAR, 1000);
}

BEGIN_MESSAGE_MAP(CGenerNetPage1, CMntPropertyPage)
	//{{AFX_MSG_MAP(CGenerNetPage1)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()




/////////////////////////////////////////////////////////////////////////////
// generov�n� s�t� mezi �seky - parametry & v�b�r �sek�

extern BOOL DoAutoNetGenerateFunction(CNetPartsList* pListFrom,CNetPartsList* pListTo,CPosNetTemplate* pNetTemplate,AutoNetGenParams& Params,CPosEdMainDoc* pDocument);
extern BOOL DoAutoNetGenerateFunction2(CPosNetObjectKey* pMainObject,CNetPartsList* pListFrom,REALNETPOS	rPosTo,CPosNetTemplate* pNetTemplate,AutoNetGenParams& Params,CPosEdMainDoc* pDocument);

BOOL DoPreGenerateNetBetween(CPosNetObjectKey* pFrom,CPosNetObjectKey* pTo,CPosEdMainDoc* pDocument)
{
	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	// glob�ln� parametry generov�n�
	AutoNetGenParams	Params= pEnvir->m_optPosEd.m_NetGenParams;
	// inicializace parametr�
/*
	/// TEMP - INICIALIZACE PARAMETRU
	Params.nSamePosTol = 0.01;	// tolerance pro pova�ov�n� bodu za stejn� (v m)
	Params.nSamePosHgt = 0.005;	// tolerance pro pova�ov�n� v��ky za stejnou (v m)
	Params.nSameGraTol = 0;		// tolerance pro povazovan� �hlu za stejn� (ve �)

	Params.nCountLimit = 3;		// limit po�tu d�l�
	Params.nHeightStep = 0.2;	// krok testovan� v��ky pro generov�n� uzl�

	// cena d�lu
	Params.nSTRA[0]	= 80;		// cena za rovinku
	Params.nSTRA[1] = 50;
	Params.nSTRA[2] = 30;

	Params.nBEND[0] =			// cena za zat��ku
	Params.nBEND[1] =
	Params.nBEND[2] =
	Params.nBEND[3] = 100;

	// ocen�n� nastaven� d�lu
	Params.nPricePrvSl = 7;		// cena za sklon k p�edchoz�mu d�lu (cena za kazdy 0.1�)
	Params.nPriceSlope = 5;		// cena za sklon d�lu	(cena za kazdy 0.1�)
	Params.nPriceStand = 3;		// cena za odchylku od standardn�ho vyno�en� (cena za 0.1 m)

	// ocen�n� trati
	Params.nLenPrice   = 0;		// cena za jeden metr trati
	Params.nBendPrice  = 0;		// cena za % (0-100x) zat��ek

	// odhad ceny trati
	Params.nDodLenPrice= 4;		// cena za 1 m vzdu�n� vzd�lenosti do c�le
	Params.nDodKoefGrad= 10000;	// cena za odchylku �hlu  (za 1�) se �tvercem vzd�lenosti do c�le (0-1x)
	Params.nDodKoefSlop= 1000;	// cena za odchylku sklonu (za 0.1�) se �tvercem vzd�lenosti do c�le (0-1x)
	Params.nDodKoefHght= 1000;	// cena za odchylku v��ky (za 0.1m) se �tvercem vzd�lenosti do c�le (0-1x)
*/

	CMntPropertySheet	dlg(IDS_GENER_NET_TITLE,PSH_NOAPPLYNOW);
	CGenerNetPage0		page0;
	CGenerNetPage1		page1; page1.m_Params = &Params;
	//CGenerNetPage2		page2; page2.m_Params = &Params;
	// vlo�en� str�nek & parametry
	page0.m_pDocument = pDocument;
	page0.m_pKeyFrom  = pFrom;
	page0.m_pKeyTo	  = pTo;

	dlg.AddPageIntoWizard(&page0);
	dlg.AddPageIntoWizard(&page1);
	//dlg.AddPageIntoWizard(&page2);

	// realizace generov�n�
	if (dlg.DoModal()!=IDOK) return FALSE;

	pEnvir->m_optPosEd.m_NetGenParams = Params;
  pEnvir->m_optPosEd.SaveParams();

	// vol�m generov�n� �seku
	DoAutoNetGenerateFunction(page0.m_pListFrom,page0.m_pListTo,page0.m_pNetTemplate,Params,pDocument);
	return TRUE;
};


BOOL DoPreGenerateNetFrom(CPosNetObjectKey* pFrom, CPoint ptTo, CPosEdMainDoc* pDocument, BOOL bParams)
{
	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	REALPOS rPtTo = pDocument->m_PoseidonMap.FromViewToRealPos(ptTo);
	// glob�ln� parametry generov�n�
	AutoNetGenParams	Params= pEnvir->m_optPosEd.m_NetGenParams;
	// inicializace parametr�
	CMntPropertySheet	dlg(IDS_GENER_NET_TITLE, PSH_NOAPPLYNOW);
	CGenerNetPage0		page0; 
	page0.m_Params = &Params;
	CGenerNetPage1		page1; 
	page1.m_Params = &Params;
	//CGenerNetPage2		page2; page2.m_Params = &Params;
	// vlo�en� str�nek & parametry
	page0.m_pDocument = pDocument;
	page0.m_pKeyFrom  = pFrom;
	page0.m_pKeyTo	  = NULL;
	page0.m_rPtTo     = rPtTo;

	dlg.AddPageIntoWizard(&page0);
	dlg.AddPageIntoWizard(&page1);
	//dlg.AddPageIntoWizard(&page2);

	// realizace generov�n�
	page0.m_bAutoClose = !bParams;
	if (dlg.DoModal() != IDOK) return FALSE;

	pEnvir->m_optPosEd.m_NetGenParams = Params;
	pEnvir->m_optPosEd.SaveParams();

	if (!Params.bFitEndAngle)
	{ //It does not have sence to test end angle.
		Params.nSameGraTol = 360;
	}
	// vol�m generov�n� �seku
	REALNETPOS	rPosTo;
	rPosTo.nPos = rPtTo;

	// new code
	// ************************************************************
	rPosTo.nGra = GetNormalizedGrad((float)SpecRound(page0.m_Params->nEndPosition.nGra, SR_001));
	// ************************************************************

	// old code
	// ************************************************************
	//rPosTo.nGra = GetNormalizedGrad((int)SpecRound(page0.m_Params->nEndPosition.nGra,SR_10));
	// ************************************************************

	rPosTo.nHgt = pDocument->m_PoseidonMap.GetLandscapeHeight(rPosTo.nPos);
	//rPosTo.nHgt+= (float)(page0.m_pNetTemplate->m_nStdHeight);

	DoAutoNetGenerateFunction2(pFrom, page0.m_pListFrom, rPosTo, page0.m_pNetTemplate, Params, pDocument);
	return TRUE;
}

void CEditNetKeyObjectDlg::OnBnClickedExportnet()
{
  using namespace std;
  CFileDialog fdlg(FALSE);
  if (fdlg.DoModal()==IDOK)
  {
    CString fname=fdlg.GetPathName();
    ofstream out(fname,ios::out|ios::trunc);
    if (!out)
    {
      AfxMessageBox("Error creating the file");
      return;
    }
    ExportNet(out);
  }
}

void CEditNetKeyObjectDlg::OnBnClickedImportnet()
{
  using namespace std;
  CFileDialog fdlg(TRUE,0,0,OFN_HIDEREADONLY|OFN_FILEMUSTEXIST);
  if (fdlg.DoModal()==IDOK)
  {
    CString fname=fdlg.GetPathName();
    ifstream in(fname,ios::in);
    if (!in)
    {
      AfxMessageBox("Error opening the file");
      return;
    }
    ImportNet(in);
  }
}

void CEditNetKeyObjectDlg::ImportNet(std::istream &in)
{
  for (int i=0;i<4;i++)
  {
    DoExportImportEditNetList(i,&in,0);
  }
}

void CEditNetKeyObjectDlg::ExportNet(std::ostream &out)
{
  for (int i=0;i<4;i++)
  {
    DoExportImportEditNetList(i,0,&out);
  }
}
