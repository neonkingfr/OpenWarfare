/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"
#include ".\poseidonedobjs.h"
#include <el/Pathname/Pathname.h>
#include "PosMainFrm.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPosEdMainDoc


IMPLEMENT_DYNAMIC(CPosEdMainDoc, CPosEdBaseDoc)

CPosEdMainDoc::CPosEdMainDoc() : m_GroupObjects(m_PoseidonMap), m_CurrentArea(m_PoseidonMap)
{	
	m_pObjsPanel = NULL;	// nen� ur�en
	m_PoseidonMap.m_pDocument = this;
	m_CursorObject.m_pDocument= this;
}

CPosEdMainDoc::~CPosEdMainDoc()
{
}

BEGIN_MESSAGE_MAP(CPosEdMainDoc, CPosEdBaseDoc)
	//{{AFX_MSG_MAP(CPosEdMainDoc)  
	//}}AFX_MSG_MAP
//  ON_COMMAND(ID_SCC_INTEG_CHECK_OUT, OnSccIntegCheckOut)
//  ON_UPDATE_COMMAND_UI(ID_SCC_INTEG_CHECK_OUT, OnUpdateSccIntegCheckOut)
END_MESSAGE_MAP()

BOOL CPosEdMainDoc::OnNewDocument()
{
	if (!CPosEdBaseDoc::OnNewDocument())
		return FALSE;

	return TRUE;
}

void CPosEdMainDoc::OnCloseDocument() 
{
	// ukon��m re�ln� b�h
	if (IsRealDocument())
    OnBuldDisconnect();

  // likviduji n�vaznost na panel objekt�
	if ((m_pObjsPanel)&&(m_pObjsPanel->m_pCurrentDoc == this))
		m_pObjsPanel->UpdateDocData(NULL,TRUE);
	
	CPosEdBaseDoc::OnCloseDocument();
}

/////////////////////////////////////////////////////////////////////////////
// serializace dat dokumentu

void CPosEdMainDoc::DoSerialize(CArchive& ar,int nVer)
{	// serializuji p�edka
	CPosEdBaseDoc::DoSerialize(ar,nVer);
	// mapa poseidon
	m_PoseidonMap.DoSerialize(ar,nVer);

  if (nVer>=58)
  {
    if (ar.IsStoring())
    {
      std::ostrstream out;
      m_SelectionList.Save(out);
      unsigned long size=out.pcount();
      MntSerializeDWord(ar,size);
      ar.Write(out.str(),out.pcount());
      out.freeze(false);
    }
    else
    {
      unsigned long size;
      MntSerializeDWord(ar,size);
      char *buff=new char[size];
      ar.Read(buff,size);
      std::istrstream in(buff,size);
      m_SelectionList.Load(in);
      delete [] buff;
    }
  }
	
	if (!ar.IsStoring())
	{
		// update registrovan�ch textur
		//m_PoseidonMap.UpdateRegTexturesFromDefTextures(&m_MgrTextures);
		// update registrovan�ch objekt�
		//m_PoseidonMap.UpdateRegObjectsFromDefObjects(&m_MgrObjects);
		// update polohy curzoru
		m_CursorObject.SetCursorLogPosition(m_CursorObject.m_ptCurrentPos,FALSE);
	}
};

/////////////////////////////////////////////////////////////////////////////
// zpracov�n� zpr�v z preview
/*
void CPosEdMainDoc::InvalidRealMessage(const SPosMessage& sMsg) const
{
	CString sText,sNum;
	NumToStandardString(sNum,(LONG)sMsg._iMsgType);
	AfxFormatString1(sText,IDS_INVALID_REAL_MSG,sNum);
	AfxMessageBox(sText,MB_OK|MB_ICONSTOP);
	return;
};

void CPosEdMainDoc::HandleRealMessage(const SPosMessage& sMsg,int nMode)
{
	switch(sMsg._iMsgType)
	{
	///////////////////////////////////////
	// Akceptovan� ud�losti od BULDOZERU

	case LAND_TEXTURE_CHANGE:
		{	// zm�na textury			
      MESSAGE_DATA_CONST_VAR(LAND_TEXTURE_CHANGE, &sMsg, msgData);
			UNITPOS Pos = { msgData.nX, msgData.nZ };
			Ref<CPosTextureTemplate> pTempl = m_Textures.FindEx(msgData.nTextureID);
			ASSERT(pTempl != NULL);
			if (pTempl)
			{
				m_PoseidonMap.SetLandTextureAt(Pos,pTempl,TRUE);
				// if (nMode != CPosEdBaseDoc::sndModeImport)
				//	m_PoseidonMap.UpdateBaseTexturesFromLandTextures();
				// else - najednou pro cel� import
			}
		}; break;

	case LAND_HEIGHT_CHANGE:
		{	// zm�na v��ky
      MESSAGE_DATA_CONST_VAR(LAND_HEIGHT_CHANGE, &sMsg, msgData);
			UNITPOS Pos = { msgData.nX, msgData.nZ };
			
			// ud�lost pro zmenu v��ky v bode
			{
        if (m_PoseidonMap.LockedVertex(Pos))
        {
          // vertex is locked send message back
          MESSAGE_VAR(LAND_HEIGHT_CHANGE, sMsg);		

          sMsg._data.nX	= Pos.x;
          sMsg._data.nZ	= Pos.z;
          sMsg._data.Y	= m_PoseidonMap.GetLandscapeHeight(Pos);
          SendRealMessage(sMsg); 
          break;
        }

				REALPOS rPos = m_PoseidonMap.FromUnitToRealPos(Pos);
				POINT	ptPos= m_PoseidonMap.FromRealToViewPos(rPos);
        
				CRect   rRect( ptPos.x, ptPos.y-VIEW_LOG_UNIT_SIZE, ptPos.x+VIEW_LOG_UNIT_SIZE, ptPos.y);
				CPositionArea Area;
				Area.AddRect(rRect);

				CPosHeightAction* pAction = new CPosHeightAction(IDA_HEIGHT_CHANGE,Area);
				pAction->m_bFromBuldozer = TRUE;
				// vytvo�en� dat
				pAction->CreateActionData(&m_PoseidonMap);
				// nastaven� v��ky
				pAction->SetUnitLandHeight(Pos.x, Pos.z, msgData.Y);
				// realizace akce
				ProcessEditAction(pAction);
			}
		}; break;
  case BLOCK_LAND_HEIGHT_CHANGE:
    {	// zm�na v��ky
      MESSAGE_DATA_CONST_VAR(BLOCK_LAND_HEIGHT_CHANGE, &sMsg, msgData);
            
      // ud�lost pro zmenu v��ky v bode
      CPositionArea Area;
      MESSAGE_VAR(BLOCK_LAND_HEIGHT_CHANGE, msgBack);
      MESSAGE_DATA_VAR(BLOCK_LAND_HEIGHT_CHANGE, &msgBack, msgDataBack);
    
      for(int i = 0; i < msgData.Size(); i++)
      {
        UNITPOS Pos = { msgData[i].nX, msgData[i].nZ };

        if (m_PoseidonMap.LockedVertex(Pos))
        {
           // vertex is locked send message back
          STexturePosMessData& data = msgDataBack.Append();
          data.nX = Pos.x;
          data.nZ = Pos.z;
          data.Y = m_PoseidonMap.GetLandscapeHeight(Pos);                
        }
        else
        {        
          REALPOS rPos = m_PoseidonMap.FromUnitToRealPos(Pos);
          POINT	ptPos= m_PoseidonMap.FromRealToViewPos(rPos);        
          CRect rRect( ptPos.x - VIEW_LOG_UNIT_SIZE/2, ptPos.y - VIEW_LOG_UNIT_SIZE/2,ptPos.x + VIEW_LOG_UNIT_SIZE/2, ptPos.y  + VIEW_LOG_UNIT_SIZE/2);        
          Area.AddRect(rRect);
        }
      }

      if (msgDataBack.Size() > 0)
      {
        // send back message some editer height cannot be changed.
         SendRealMessage(msgBack);
      }      

      if (Area.IsValidPosition())
      {
        CPosHeightAction* pAction = new CPosHeightAction(IDA_HEIGHT_CHANGE,Area);
        pAction->m_bFromBuldozer = TRUE;
        // vytvo�en� dat
        pAction->CreateActionData(&m_PoseidonMap);
        // nastaven� v��ky

        for(int i = 0; i < msgData.Size(); i++)
        {
          pAction->SetUnitLandHeight( msgData[i].nX, msgData[i].nZ, msgData[i].Y);             
        }

        // realizace akce 
        ProcessEditAction(pAction);
      }
    }; break;	
	case OBJECT_MOVE:
		{	// p�esun objektu - NE IMPORT !
			ASSERT(nMode != CPosEdBaseDoc::sndModeImport);
      MESSAGE_DATA_CONST_VAR(OBJECT_MOVE, &sMsg, msgData);      
			CPosEdObject* pObj = (CPosEdObject*)(m_PoseidonMap.GetPoseidonObject(msgData.nID));
      if (pObj->Locked())
      {
        // send message back object cannot be moved
        MESSAGE_VAR(OBJECT_MOVE, sMsgBack);

        pObj->SetObjectDataToMessage(sMsgBack._data);
        SendRealMessage(sMsg);
        break;
      }

			CPosEdObject* pAct = (CPosEdObject*)(pObj->CreateCopyObject());
			if ((pObj)&&(pAct))
			{	// posun objektu
				for(int i=0; i<4;i++)
					for(int j=0; j<4;j++)
				pAct->m_Position(i,j) = msgData.Position[i][j];
				// relativn� v��ka vzhledem k povrchu
				pAct->m_nRelHeight = pAct->CalcRelHeightFromCurrPos();
				pAct->CalcObjectLogPosition();
				// generuji ud�lost pro p�esun atributu
				GenerNewObjectAction(IDA_OBJECT_MOVE,pObj,pAct,TRUE,NULL);
			};
		}; break; 
  case BLOCK_MOVE:
    {
      CPosObjectGroupAction * groupAct = new CPosObjectGroupAction(IDA_GROUPOBJ_MOVE);
      MESSAGE_DATA_CONST_VAR(BLOCK_MOVE, &sMsg, msgData);

      MESSAGE_VAR(BLOCK_MOVE, msgBack); // send back message if locked object was moved
      MESSAGE_DATA_VAR(BLOCK_MOVE, &msgBack, msgDataBack);
    

      for(int iO = 0; iO < msgData.Size(); iO++)
      {
        CPosEdObject* pObj = (CPosEdObject*)(m_PoseidonMap.GetPoseidonObject(msgData[iO].nID));
        if (!pObj)
          continue;

        if (pObj->Locked())
        {
          // send back it cannot be moved
          SMoveObjectPosMessData& data = msgDataBack.Append();
          pObj->SetObjectDataToMessage(data);
        }
        else
        {
          CPosEdObject* pAct = (CPosEdObject*)(pObj->CreateCopyObject());
          if (pAct)
          {	// posun objektu
            for(int i=0; i<4;i++)
              for(int j=0; j<4;j++)
                pAct->m_Position(i,j) = msgData[iO].Position[i][j];
            // relativn� v��ka vzhledem k povrchu
            pAct->m_nRelHeight = pAct->CalcRelHeightFromCurrPos();
            pAct->CalcObjectLogPosition();
            // generuji ud�lost pro p�esun atributu
            GenerNewObjectAction(IDA_OBJECT_MOVE,pObj,pAct,TRUE,groupAct);
          };
        };
      }

      if (msgDataBack.Size() > 0)
      {
        SendRealMessage(msgBack);
      }
      
      groupAct->m_bFromBuldozer = TRUE;
      ProcessEditAction(groupAct);
      break;
    };
	default:
		// zpracuje z�kladn� dokument
		CPosEdBaseDoc::HandleRealMessage(sMsg,nMode);

    // send the rest messages to views
    CPosMessageUpdate cUpdate(sMsg, nMode);
    UpdateAllViews(NULL, CPosMessageUpdate::uvfPosMessage, &cUpdate);
	};
};

/////////////////////////////////////////////////////////////////////////////
// obnova panelu

*/
void CPosEdMainDoc::UpdateDocKPlaObjects()
{
	if (m_pObjsPanel != NULL) m_pObjsPanel->UpdateDocKPlaObjects();
}

void CPosEdMainDoc::UpdateDocNameObjects()
{
	if (m_pObjsPanel != NULL) m_pObjsPanel->UpdateDocNameObjects();
}

void CPosEdMainDoc::UpdateDocNObjObjects()
{
	if (m_pObjsPanel != NULL) m_pObjsPanel->UpdateDocNObjObjects();
}

void CPosEdMainDoc::UpdateDocNObjObject(CPosEdObject* pObjBefore,CPosEdObject* pObjAfter)
{
	if (m_pObjsPanel != NULL) m_pObjsPanel->UpdateDocNObjObject(pObjBefore, pObjAfter);
}

/////////////////////////////////////////////////////////////////////////////
// CPosEdMainDoc commands
/*
// pr�ce s re�ln�m dokumentem
void CPosEdMainDoc::OnMakeDocReal()
{
	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	if (IsRealDocument())
	{	// ru��m re�ln� status
		pEnvir->m_pRealDoc = NULL;
    OnBuldDisconnect();
		
	} else
	{	// nastav�m re�ln� status
		ASSERT(pEnvir->m_pRealDoc == NULL);
		pEnvir->m_pRealDoc = this;
    
    OnBuldConnect();
	};
};
*/

void CPosEdMainDoc::OnUpdateBuldozer()
{	
	m_PoseidonMap.TransferMapToBuldozer();
}

void CPosEdMainDoc::OnUpdateUpdateBuldozer(CCmdUI* pCmdUI)
{	
	pCmdUI->Enable(IsRealDocument()); 
}

BOOL GetWorldFile(CString& sFile, BOOL bOpen)
{
	CString sFilter, sExt, sDir, sTitle;
	VERIFY(sFilter.LoadString(IDS_BULDOZER_WORLD_FILE));
	VERIFY(sTitle.LoadString(bOpen ? IDS_BULDOZER_WORD_IMP_TITLE : IDS_BULDOZER_WORD_EXP_TITLE));
	sDir = GetPosEdEnvironment()->m_optSystem.m_sObjWorldPath;
	MakeShortDirStr(sDir, sDir);
	// p��prava CFileDialog
	CFileDialog dlg(bOpen);
	if (bOpen)
	{
		dlg.m_ofn.Flags |= OFN_FILEMUSTEXIST;
	}
	else
	{
		dlg.m_ofn.Flags |= OFN_PATHMUSTEXIST;
	}
	sFilter += (TCHAR)(_T('\0'));
	sFilter += _T("*.wrp");
	sFilter += (TCHAR)(_T('\0'));
	sFilter += (TCHAR)(_T('\0'));
	dlg.m_ofn.nMaxCustFilter = 1;
	dlg.m_ofn.lpstrFilter = sFilter;
	dlg.m_ofn.lpstrDefExt = _T("wrp");
	dlg.m_ofn.lpstrTitle  = sTitle;
	dlg.m_ofn.lpstrFile = sFile.GetBuffer(_MAX_PATH);
	dlg.m_ofn.lpstrInitialDir = sDir;
	BOOL bRes = (dlg.DoModal() == IDOK) ? TRUE : FALSE;
	sFile.ReleaseBuffer();
	if (bRes)
	{  
		SeparateDirFromFileName(sDir, (LPCTSTR) sFile);

		GetPosEdEnvironment()->m_optSystem.m_sObjWorldPath = sDir;
		GetPosEdEnvironment()->m_optSystem.SaveParams();
	}

	return bRes;
}

void CPosEdMainDoc::OnImportRealDoc()
{
/*	if (!IsRealDocument())
		return;

	CString sFile;
	if (!GetWorldFile(sFile, TRUE))
		return;
	// po�lu ud�lost buldozeru
  {
	  
      MESSAGE_VAR(SYSTEM_INIT, sMsg); 	
	  SendRealMessage(sMsg);
  }
  {
    MESSAGE_VAR(FILE_IMPORT, sMsg); 
    sMsg._size = sizeof(*sMsg._data) + sFile.GetLength();
    sMsg._data = (PSLoadSavePosMessData) new unsigned char[sMsg._size];

    strcpy(sMsg._data->szFileName,sFile);	  
	  SendRealMessage(sMsg);
  }
	m_sLastImportFile = sFile;*/
}

void CPosEdMainDoc::OnUpdateImportRealDoc(CCmdUI* pCmdUI)
{	
	pCmdUI->Enable(IsRealDocument()); 
}

void CPosEdMainDoc::OnExportRealDoc()
{
	if (!IsRealDocument()) return;

    if (RealViewer())
    {
		CString sFile = m_sLastImportFile;
		if (!GetWorldFile(sFile, FALSE)) return;
	
		// po�lu ud�lost buldozeru
		RealViewer()->FileExport(sFile);
		m_sLastImportFile = sFile;
	
		// EXPORT pojmenovan�ch oblast�

		Pathname target = sFile;
		target.SetExtension(_T(".hpp"));

		using namespace std;
		ofstream out(target.GetFullPath(), ios::out | ios::trunc);
		if (!out)
		{
			AfxMessageBox(IDS_ERROR_EXPORT_NAMES, MB_OK | MB_ICONEXCLAMATION);
			return;
		}

		for (int i = 0; i < m_PoseidonMap.m_KeyPtInMap.GetSize(); ++i)
		{
			CPosKeyPointObject* pKey = (CPosKeyPointObject*)(m_PoseidonMap.m_KeyPtInMap[i]);
			if (pKey != NULL)
			{
				out << "class " << pKey->m_sName.GetString() << "\n"
					<< "{\n"
					<< "\tname=\"" << pKey->m_Text.GetString() << "\";\n"
					<< "\tposition[]={" << pKey->m_ptRealPos.x << "," << pKey->m_ptRealPos.z << "};\n"
					<< "\ttype=\"" << pKey->m_Type.GetString() << "\";\n"
					<< "\tradiusA=" << pKey->m_nRealWidth << ";\n"
					<< "\tradiusB=" << pKey->m_nRealHeight << ";\n";
				CString extra = pKey->m_otherProp;
				if (extra.GetLength())
				{     
					int i = extra.Find('\n');
					while (i >= 0)
					{
						extra.Delete(i - 1, 1);
						extra.Insert(i, "\t");
						i = extra.Find('\n', i + 1);
					}
					out << "\t" << extra.GetString() << "\n";
				}
				out << "};\n";        
			}
		}

/*
	static LPCTSTR pNewLn = _T("\r\n");

	int nDot = sFile.ReverseFind(_T('.'));
	if (nDot <= 0)
		return;

	CString sFileExp =  sFile.Left(nDot+1);
			sFileExp += _T("hpp");
	CFile save;
	TRY
	{
		if (!save.Open(sFileExp,CFile::modeCreate|CFile::modeWrite))
			AfxThrowUserException();
		save.Write(pNewLn,lstrlen(pNewLn));
		// export pojmenovan�ch objekt
		for (int i=0; i<m_PoseidonMap.m_InstanceObjs.GetSize(); i++)
		{
			CPosEdObject* pObj = (CPosEdObject*)(m_PoseidonMap.GetPoseidonObject(i));
			if ((pObj != NULL)&&(!((CPosObject*)pObj)->GetObjectName().IsEmpty()))
			{
				CString sNum,sText = _T("#define ");
				sText += ((CPosObject*)pObj)->GetObjectName();
				sText += _T("\t{");
				NumToStandardString(sNum,(double)(pObj->m_Position(0,3)));
				sText += sNum;
				sText += _T(",");
				NumToStandardString(sNum,(double)(pObj->m_Position(2,3)));
				sText += sNum;
				sText += _T("}");
				// ulo�en� do souboru
				save.Write(((LPCTSTR)sText),sText.GetLength());
				save.Write(pNewLn,lstrlen(pNewLn));
			}
		};
		save.Write(pNewLn,lstrlen(pNewLn));
		// export pojmenovan�ch oblast� (kl��ov�ch bod�)
		for (int i=0; i<m_PoseidonMap.m_KeyPtInMap.GetSize(); i++)
		{
			CPosKeyPointObject* pKey = (CPosKeyPointObject*)(m_PoseidonMap.m_KeyPtInMap[i]);
			if (pKey != NULL)
			{
				CString sNum,sText = _T("#define ");
				sText += pKey->m_sName;
				sText += _T("\t{");
				NumToStandardString(sNum,(double)(pKey->m_ptRealPos.x));
				sText += sNum;
				sText += _T(",");
				NumToStandardString(sNum,(double)(pKey->m_ptRealPos.z));
				sText += sNum;
				sText += _T(",");
				NumToStandardString(sNum,(double)(max(pKey->m_nRealWidth,pKey->m_nRealHeight)));
				sText += sNum;
				sText += _T("}");
				// ulo�en� do souboru
				save.Write(((LPCTSTR)sText),sText.GetLength());
				save.Write(pNewLn,lstrlen(pNewLn));
			}
		};

		save.Close();
	}
	CATCH_ALL(e)
	{
		AfxMessageBox(IDS_ERROR_EXPORT_NAMES,MB_OK|MB_ICONEXCLAMATION);
		save.Abort();
	}
	END_CATCH_ALL
    }
    */
  
  }
}

void CPosEdMainDoc::OnUpdateExportRealDoc(CCmdUI* pCmdUI)
{	
	pCmdUI->Enable(IsRealDocument()); 
}

/////////////////////////////////////////////////////////////////////////////
// STATUS bar

void CPosEdMainDoc::OnUpdateStatusInfoCurpos(CCmdUI* pCmdUI)
{	pCmdUI->Enable(TRUE);
	// poloha kurz�ru (re�ln� sou�adnice)
	CString sXTxt, sZTxt;
	if ((m_CursorObject.m_rpCurrentPos.x >= 0.f)&&
		(m_CursorObject.m_rpCurrentPos.z >= 0.f))
	{
		NumToLocalString(sXTxt,(double)(m_CursorObject.m_rpCurrentPos.x),0);
		NumToLocalString(sZTxt,(double)(m_CursorObject.m_rpCurrentPos.z),0);
		sXTxt += _T(" : ");
		sXTxt += sZTxt;
	} else
		sXTxt = _T(" ");
	pCmdUI->SetText(sXTxt);
};
void CPosEdMainDoc::OnUpdateStatusInfoHeight(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
	if ((m_CursorObject.m_rpCurrentPos.x >= 0.f)&&
		(m_CursorObject.m_rpCurrentPos.z >= 0.f))
	{
		if (m_PoseidonMap.IsAtWorld(m_CursorObject.m_rpCurrentPos))
		{
			CString sText;
			double  nHeight = m_PoseidonMap.GetLandscapeHeight(m_CursorObject.m_rpCurrentPos);
			NumToLocalString(sText,nHeight,3);
			pCmdUI->SetText(sText);
			return;
		}
	};
	pCmdUI->SetText(_T(" "));
};
void CPosEdMainDoc::OnUpdateStatusInfoPrimtxtr(CCmdUI* pCmdUI)
{	pCmdUI->Enable(TRUE);
	// zaokrouhl�m na 50 m
	REALPOS rPos  = m_CursorObject.m_rpCurrentPos;
	//rPos.x		  = (float)(((int)(rPos.x / m_nRealSizeUnit))*m_nRealSizeUnit);
	//rPos.z		  = (float)(((int)(rPos.z / m_nRealSizeUnit))*m_nRealSizeUnit);
	UNITPOS rUnit = m_PoseidonMap.GetActiveTextureLayer()->FromRealToTextureUnitPos(rPos);	
	// prim�rn�/variantn� textura
	PTRTEXTURETMPLT pBase =	m_PoseidonMap.GetBaseTextureAt(rUnit);
	if (pBase == NULL)
	{	// mo�e
		pCmdUI->SetText(_T("[ *** ]"));
		return;
	}
	CString sText = pBase->m_sTextureName;
	sText += _T(" (");
	sText += pBase->m_sTextureFile;
	sText += _T(")");
	pCmdUI->SetText(sText);
};
void CPosEdMainDoc::OnUpdateStatusInfoScndtxtr(CCmdUI* pCmdUI)
{	pCmdUI->Enable(TRUE);
	// zaokrouhl�m na 50 m
	REALPOS rPos  = m_CursorObject.m_rpCurrentPos;
	//rPos.x		  = (float)(((int)(rPos.x / m_nRealSizeUnit))*m_nRealSizeUnit);
	//rPos.z		  = (float)(((int)(rPos.z / m_nRealSizeUnit))*m_nRealSizeUnit);
	UNITPOS rUnit = m_PoseidonMap.GetActiveTextureLayer()->FromRealToTextureUnitPos(rPos);	
	// sekund�rn� textura
	PTRTEXTURETMPLT pBase =	m_PoseidonMap.GetLandTextureAt(rUnit);
	if (pBase == NULL)
	{	// mo�e
		pCmdUI->SetText(_T("[ *** ]"));
		return;
	}
	CString sText = pBase->m_sTextureName;
	sText += _T(" (");
	sText += pBase->m_sTextureFile;
	sText += _T(")");
	pCmdUI->SetText(sText);
};

/////////////////////////////////////////////////////////////////////////////
// realizace akc� - objekty

CPosObject*  CPosEdMainDoc::GetDocumentObjectFromValue(const CPosObject* pValue)
{
	// kop�ruji data objektu
	if (pValue->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
	{
		CPosEdObject* pDestObj = m_PoseidonMap.GetPoseidonObject(pValue->GetID());
		ASSERT(pDestObj != NULL);
		return pDestObj;
	} else
	if (pValue->IsKindOf(RUNTIME_CLASS(CPosWoodObject)))
	{
		CPosWoodObject* pDestObj = m_PoseidonMap.GetPoseidonWood(pValue->GetID());
		ASSERT(pDestObj != NULL);
		return pDestObj;
	} else
	if (pValue->IsKindOf(RUNTIME_CLASS(CPosNetObjectBase)))
	{
		CPosNetObjectBase* pDestObj = m_PoseidonMap.GetPoseidonNet(pValue->GetID());
		ASSERT(pDestObj != NULL);
		return pDestObj;
	} else
	if (pValue->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)))
	{
		CPosNameAreaObject* pDestObj = m_PoseidonMap.GetPoseidonNameArea(pValue->GetID());
		ASSERT(pDestObj != NULL);
		return pDestObj;
	} else
	if (pValue->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))
	{
		CPosKeyPointObject* pDestObj = m_PoseidonMap.GetPoseidonKeyPt(pValue->GetID());
		ASSERT(pDestObj != NULL);
		return pDestObj;
	}
  if (pValue->IsKindOf(RUNTIME_CLASS(CPosBackgroundImageObject)))
  {
    CPosBackgroundImageObject* pDestObj = m_PoseidonMap.GetBgImage(((CPosBackgroundImageObject*)pValue)->m_sAreaName);
    ASSERT(pDestObj != NULL);
    return pDestObj;
  }
	// nezn�m� typ objektu
	ASSERT(FALSE);
	return NULL;
};

CPosEdObject* CPosEdMainDoc::CreateObjectFromObjectValue(CPosEdObject* pValue)
{
	CPosEdObject* pCreate = NULL;
	ASSERT_KINDOF(CPosEdObject,pValue);

	// re�ln� poloha objektu
	REALPOS rPos = pValue->GetRealPos();

	// synchronizuji kurzor
	m_CursorObject.SetCursorRealPosition(rPos,FALSE);

	// registrace objektu
  Ref<CPosObjectTemplate> pRegTempl;
  if (pValue->m_pTemplate.IsNull())
	  pRegTempl = GetObjectTemplate(pValue->m_nTemplateID);
  else
    pRegTempl = pValue->m_pTemplate;

	if (pRegTempl.IsNull())
	{	// objekt nen� registrov�n
    LogF("Error: Object creation failed. Template does not exist.id =  %s.", pValue->m_nTemplateID);
    return NULL;
	}

	// vlo�en� registrovaneho objektu
	if (pRegTempl != NULL)
	{
		// vytvo�en� objektu
		pCreate = new CPosEdObject(m_PoseidonMap);
		pCreate->CreateFromTemplateAt(pRegTempl,rPos);
		pCreate->CopyFrom(pValue);
    pCreate->m_pTemplate = pRegTempl;  
    pCreate->m_nTemplateID = pRegTempl->m_ID;

		// vlo��m objekt
		m_PoseidonMap.AddNewObject(pCreate,FALSE);
	};
	return pCreate;
};

void CPosEdMainDoc::DeleteObjectFromObjectValue(CPosEdObject* pValue)
{
	// synchronizuji kurzor
	//if (GetPosEdEnvironment()->m_optPosEd.m_bBldSynchrAction)
	{
		REALPOS rPos = pValue->GetRealPos();
		m_CursorObject.SetCursorRealPosition(rPos,FALSE);
	};
	// ma�u objekt
	m_PoseidonMap.DeleteObject(pValue->GetID(),FALSE);
};

/////////////////////////////////////////////////////////////////////////////
// realizace akc� - lesy

CPosWoodObject* CPosEdMainDoc::CreateWoodFromWoodValue(CPosWoodObject* pValue)
{
	CPosWoodObject* pCreate = NULL;
	ASSERT_KINDOF(CPosWoodObject,pValue);
  // check if template exists
  if (pValue->GetWoodTemplate() == NULL)
  {
    LogF("Error: Wood template %s is missing.", pValue->GetTemplateName());
    return NULL;
  }

	// synchronizuji kurzor
	//if (GetPosEdEnvironment()->m_optPosEd.m_bBldSynchrAction)
	{
		CRect  rHull; pValue->GetObjectHull(rHull);
		CPoint ptCenter((rHull.left+rHull.right)/2,(rHull.top+rHull.bottom)/2);
		m_CursorObject.SetCursorLogPosition(ptCenter,FALSE);
	}
	// vlo�en� lesa
	pCreate = new CPosWoodObject(m_PoseidonMap);
	pCreate->CopyFrom(pValue);
	// vlo��m objekt
	m_PoseidonMap.AddNewWood(pCreate);
	return pCreate;
};
void CPosEdMainDoc::DeleteWoodFromWoodValue(CPosWoodObject* pValue)
{
	// synchronizuji kurzor
	//if (GetPosEdEnvironment()->m_optPosEd.m_bBldSynchrAction)
	{
		CRect  rHull; pValue->GetObjectHull(rHull);
		CPoint ptCenter((rHull.left+rHull.right)/2,(rHull.top+rHull.bottom)/2);
		m_CursorObject.SetCursorLogPosition(ptCenter,FALSE);
	}
	// ma�u objekt
	m_PoseidonMap.DeleteWood(pValue->GetID());
};

/////////////////////////////////////////////////////////////////////////////
// realizace akc� - s�t�


CPosNetObjectBase* CPosEdMainDoc::CreateNetFromNetValue(CPosNetObjectBase* pValue)
{
	CPosNetObjectBase* pCreate = NULL;
	ASSERT_KINDOF(CPosNetObjectBase, pValue);
	// synchronizuji kurzor
	//if (GetPosEdEnvironment()->m_optPosEd.m_bBldSynchrAction)
	{
		m_CursorObject.SetCursorRealPosition(pValue->m_nBaseRealPos.nPos, FALSE);
	}
	// vlo�en� lesa
	pCreate = (CPosNetObjectBase*)(pValue->CreateCopyObject());
	ASSERT_KINDOF(CPosNetObjectBase, pCreate);
	// vlo��m objekt
	m_PoseidonMap.AddNewNet(pCreate);
	return pCreate;
}

void CPosEdMainDoc::DeleteNetFromNetValue(CPosNetObjectBase* pValue)
{
	// synchronizuji kurzor
	//if (GetPosEdEnvironment()->m_optPosEd.m_bBldSynchrAction)
	{
		m_CursorObject.SetCursorRealPosition(pValue->m_nBaseRealPos.nPos,FALSE);
	}
	// ma�u objekt
	m_PoseidonMap.DeleteNet(pValue->GetID());
};


/////////////////////////////////////////////////////////////////////////////
// realizace akc� - pojmenovan� oblasti

CPosNameAreaObject* CPosEdMainDoc::CreateNameAreaFromNameAreaValue(CPosNameAreaObject* pValue)
{
	CPosNameAreaObject* pCreate = NULL;
	ASSERT_KINDOF(CPosNameAreaObject,pValue);
	// synchronizuji kurzor
	//if (GetPosEdEnvironment()->m_optPosEd.m_bBldSynchrAction)
	{
		CRect  rHull; pValue->GetObjectHull(rHull);
		CPoint ptCenter((rHull.left+rHull.right)/2,(rHull.top+rHull.bottom)/2);
		m_CursorObject.SetCursorLogPosition(ptCenter,FALSE);
	}
	// vlo�en� lesa
	pCreate = new CPosNameAreaObject(m_PoseidonMap);
	pCreate->CopyFrom(pValue);
	// vlo��m objekt
	m_PoseidonMap.AddNewNameArea(pCreate);
	return pCreate;
};
void CPosEdMainDoc::DeleteNameAreaFromNameAreaValue(CPosNameAreaObject* pValue)
{
	// synchronizuji kurzor
	//if (GetPosEdEnvironment()->m_optPosEd.m_bBldSynchrAction)
	{
		CRect  rHull; pValue->GetObjectHull(rHull);
		CPoint ptCenter((rHull.left+rHull.right)/2,(rHull.top+rHull.bottom)/2);
		m_CursorObject.SetCursorLogPosition(ptCenter,FALSE);
	}
	// ma�u objekt
	m_PoseidonMap.DeleteArea(pValue->GetID());
};

/////////////////////////////////////////////////////////////////////////////
// realizace akc� - kl��ov� body

CPosKeyPointObject* CPosEdMainDoc::CreateKeyPointFromKeyPointValue(CPosKeyPointObject* pValue)
{
	CPosKeyPointObject* pCreate = NULL;
	ASSERT_KINDOF(CPosKeyPointObject,pValue);
	// synchronizuji kurzor
	//if (GetPosEdEnvironment()->m_optPosEd.m_bBldSynchrAction)
	{
		CRect  rHull; pValue->GetObjectHull(rHull);
		CPoint ptCenter((rHull.left+rHull.right)/2,(rHull.top+rHull.bottom)/2);
		m_CursorObject.SetCursorLogPosition(ptCenter,FALSE);
	}
	// vlo�en� lesa
	pCreate = new CPosKeyPointObject(m_PoseidonMap);
	pCreate->CopyFrom(pValue);
	// vlo��m objekt
	m_PoseidonMap.AddNewKeyPt(pCreate);
	return pCreate;
};
void CPosEdMainDoc::DeleteKeyPointFromKeyPointValue(CPosKeyPointObject* pValue)
{
	// synchronizuji kurzor
	//if (GetPosEdEnvironment()->m_optPosEd.m_bBldSynchrAction)
	{
		CRect  rHull; pValue->GetObjectHull(rHull);
		CPoint ptCenter((rHull.left+rHull.right)/2,(rHull.top+rHull.bottom)/2);
		m_CursorObject.SetCursorLogPosition(ptCenter,FALSE);
	}
	// ma�u objekt
	m_PoseidonMap.DeleteKeyPt(pValue->GetID());
};

/////////////////////////////////////////////////////////////////////////////
// realizace akc� - background images

CPosBackgroundImageObject* CPosEdMainDoc::CreateBGImageFromBGImageValue(CPosBackgroundImageObject* pValue)
{
  CPosBackgroundImageObject* pCreate = NULL;
  ASSERT_KINDOF(CPosBackgroundImageObject,pValue);
  // synchronizuji kurzor
  //if (GetPosEdEnvironment()->m_optPosEd.m_bBldSynchrAction)
  {
    CRect  rHull; pValue->GetObjectHull(rHull);    
    m_CursorObject.SetCursorLogPosition(rHull.CenterPoint(),FALSE);
  }
  // vlo�en� lesa
  pCreate = new CPosBackgroundImageObject(m_PoseidonMap);
  pCreate->CopyFrom(pValue);
  pCreate->CalcObjectLogPosition();
  // vlo��m objekt
  m_PoseidonMap.AddBgImage(pCreate);
  return pCreate;
};

void CPosEdMainDoc::DeleteBGImageFromBGImageValue(CPosBackgroundImageObject* pValue)
{  
  if (pValue != NULL)
  {
    ASSERT_KINDOF(CPosBackgroundImageObject,pValue);
    m_PoseidonMap.DeleteBgImage(pValue->m_sAreaName);
  }
};



/////////////////////////////////////////////////////////////////////////////
// pr�ce se spr�vcem akc�

CPosAction* CPosEdMainDoc::GenerNewObjectAction(int nType, CPosObject* pDocObj, CPosObject* pNewObj, bool bFromBuldozer, CPosActionGroup* pToGroup, bool select)
{
	CPosObject*			pOldObj = NULL;
	CPosObjectAction*	pAction  = NULL;
	TRY
	{
		// uchov�m starou hodnotu atributu
		if (pDocObj != NULL)
		{
			if ((pOldObj = pDocObj->CreateCopyObject()) == NULL) AfxThrowMemoryException();
		}
		// vytvo��m akci
		pAction = new CPosObjectAction(nType);

		pAction->m_bFromBuldozer = bFromBuldozer;

		/*if (pAction->m_bFromBuldozer)
		{	// nemenim selekci
		//		pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
		}*/

		if (pToGroup != NULL)
		{
			pAction->m_nSelectObjType = CPosObjectAction::ida_SelectGroup;
		}

		if (!select) pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;

		// p�i�ad�m akci hodnoty (obr�cen�, budou teprve provedeny)
		pAction->m_pCurValue = pOldObj;
		pAction->m_pOldValue = pNewObj;
		// provedu akci
		if (pToGroup)
		{
			pToGroup->AddAction(pAction);
		}
		else
		{
			ProcessEditAction(pAction);
		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pAction != NULL)
		{
			delete pAction; // sma�e se i vlastni attribut
		} 
		else
		{	// attributy nebylo kam za�adit
			if (pNewObj != NULL) delete pNewObj;
			if (pOldObj != NULL) delete pOldObj;
		}
		return NULL;
	}
	END_CATCH_ALL
	return pAction;
}

BOOL CPosEdMainDoc::RealizeEditAction(CPosAction* pAction)
{
	if (CPosEdBaseDoc::RealizeEditAction(pAction)) return TRUE;

	switch(pAction->m_nActType)
	{
		////////////////
		// OBJEKTY
	case IDA_OBJECT_ADD:
		{	// vlo�en� objektu
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			if (pObjAction->m_pCurValue == NULL)
			{	// objekt bude vytvo�en
				ASSERT(pObjAction->m_pOldValue != NULL);
				CreateObjectFromObjectValue((CPosEdObject*)(pObjAction->m_pOldValue));
				UpdateDocNObjObject(NULL, (CPosEdObject*)(pObjAction->m_pOldValue)); 
			} 
			else
			{	// objekt bude smaz�n
				ASSERT(pObjAction->m_pOldValue == NULL);
				DeleteObjectFromObjectValue((CPosEdObject*)(pObjAction->m_pCurValue));
				UpdateDocNObjObject((CPosEdObject*)(pObjAction->m_pCurValue), NULL);
			}		
			return TRUE;
		}
	case IDA_OBJECT_DEL:
		{	// smaz�n� objektu
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			if (pObjAction->m_pCurValue != NULL)
			{	// objekt bude smaz�n
				ASSERT(pObjAction->m_pOldValue == NULL);
				DeleteObjectFromObjectValue((CPosEdObject*)(pObjAction->m_pCurValue));
				UpdateDocNObjObject((CPosEdObject*)(pObjAction->m_pCurValue), NULL);			
			} 
			else
			{	// objekt bude vytvo�en
				ASSERT(pObjAction->m_pOldValue != NULL);
				CreateObjectFromObjectValue((CPosEdObject*)(pObjAction->m_pOldValue));
				UpdateDocNObjObject(NULL, (CPosEdObject*)(pObjAction->m_pOldValue)); 
			}		
			return TRUE;
		}
	case IDA_OBJECT_MOVE:
	case IDA_OBJECT_EDIT:
		{	// p�esun nebo �pravy objektu
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			ASSERT(pObjAction->m_pCurValue != NULL);
			ASSERT(pObjAction->m_pOldValue != NULL);
			// kop�ruji data objektu
			if (pObjAction->m_pOldValue->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
			{
				ASSERT(((CPosEdObject*)(pObjAction->m_pCurValue))->GetID() == ((CPosEdObject*)(pObjAction->m_pOldValue))->GetID());
				CPosEdObject* pDestObj = (CPosEdObject*)GetDocumentObjectFromValue(pObjAction->m_pOldValue);
				ASSERT(pDestObj != NULL);
				if (pDestObj != NULL)
				{
					pDestObj->CopyFrom(pObjAction->m_pOldValue);
					// synchronizuji kurzor
					REALPOS rPos = pDestObj->GetRealPos();
					if ((!pObjAction->m_bFromBuldozer)/*&&(GetPosEdEnvironment()->m_optPosEd.m_bBldSynchrAction)*/)
						m_CursorObject.SetCursorRealPosition(rPos, FALSE);
					// posun v map�
					m_PoseidonMap.MoveObjectToPos(pDestObj, rPos, pObjAction->m_bFromBuldozer);
					UpdateDocNObjObject((CPosEdObject*)(pObjAction->m_pCurValue), pDestObj); 
				}				
			}
			return TRUE;
		}
	case IDA_OBJECT_LOCK:
		{	// p�esun nebo �pravy objektu
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;

			ASSERT(pObjAction->m_pCurValue != NULL);
			ASSERT(pObjAction->m_pOldValue != NULL);
	    
			CPosObject* pDestObj = GetDocumentObjectFromValue(pObjAction->m_pOldValue);

			ASSERT(pDestObj != NULL);
			if (pDestObj != NULL)
			{
				pDestObj->CopyFrom(pObjAction->m_pOldValue); // copy it just to be sure

				// synchronizuji kurzor          
				if (!pObjAction->m_bFromBuldozer) m_CursorObject.SetCursorLogPosition(pDestObj->GetLogObjectPosition()->GetCenter(),FALSE);
			}				      
			return TRUE;
		}
		////////////////
		// LESY
	case IDA_WOOD_CREATE:
		{	// vlo�en� lesa
			ASSERT_KINDOF(CPosObjectAction,pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			if (pObjAction->m_pCurValue == NULL)
			{	// les bude vytvo�en
				ASSERT(pObjAction->m_pOldValue != NULL);
				CreateWoodFromWoodValue((CPosWoodObject*)(pObjAction->m_pOldValue));
			} 
			else
			{	// les bude smaz�n
				ASSERT(pObjAction->m_pOldValue == NULL);
				DeleteWoodFromWoodValue((CPosWoodObject*)(pObjAction->m_pCurValue));
			}
			return TRUE;
		}
	case IDA_WOOD_DEL:
		{	// smaz�n� lesa
			ASSERT_KINDOF(CPosObjectAction,pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			if (pObjAction->m_pCurValue != NULL)
			{	// les bude smaz�n
				ASSERT(pObjAction->m_pOldValue == NULL);
				DeleteWoodFromWoodValue((CPosWoodObject*)(pObjAction->m_pCurValue));
			} 
			else
			{	// les bude vytvo�en
				ASSERT(pObjAction->m_pOldValue != NULL);
				CreateWoodFromWoodValue((CPosWoodObject*)(pObjAction->m_pOldValue));
			}
			return TRUE;
		}
	case IDA_WOOD_MOVE:
		{	// p�esun lesa
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			ASSERT(pObjAction->m_pCurValue != NULL);
			ASSERT(pObjAction->m_pOldValue != NULL);
			// kop�ruji data objektu
			if (pObjAction->m_pOldValue->IsKindOf(RUNTIME_CLASS(CPosWoodObject)))
			{
				ASSERT(((CPosWoodObject*)(pObjAction->m_pCurValue))->GetID() == ((CPosWoodObject*)(pObjAction->m_pOldValue))->GetID());
				CPosWoodObject* pDestObj = (CPosWoodObject*)GetDocumentObjectFromValue(pObjAction->m_pOldValue);
				ASSERT(pDestObj != NULL);
				if (pDestObj != NULL)
				{
					ASSERT_KINDOF(CPosWoodObject, pDestObj);
					pDestObj->CopyFrom(pObjAction->m_pOldValue);
					// synchronizuji kurzor
					//if (GetPosEdEnvironment()->m_optPosEd.m_bBldSynchrAction)
					{
						CRect  rHull; pDestObj->GetObjectHull(rHull);
						CPoint ptCenter((rHull.left + rHull.right) / 2, (rHull.top + rHull.bottom) / 2);
						m_CursorObject.SetCursorLogPosition(ptCenter, FALSE);
					}
				}
			}
			return TRUE;
		}
		////////////////
		// S�T�
	case IDA_NET_KEY_CREATE:
		{	// vlo�en� s�t�
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			if (pObjAction->m_pCurValue == NULL)
			{	// s� bude vytvo�ena
				ASSERT(pObjAction->m_pOldValue != NULL);
				CreateNetFromNetValue((CPosNetObjectBase*)(pObjAction->m_pOldValue));
			} 
			else
			{	// s� bude smaz�na
				ASSERT(pObjAction->m_pOldValue == NULL);
				DeleteNetFromNetValue((CPosNetObjectBase*)(pObjAction->m_pCurValue));
			}
			return TRUE;
		}
	case IDA_NET_KEY_DELETE:
		{	// smaz�n� lesa
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			if (pObjAction->m_pCurValue != NULL)
			{	// s� bude smaz�na
				ASSERT(pObjAction->m_pOldValue == NULL);
				DeleteNetFromNetValue((CPosNetObjectBase*)(pObjAction->m_pCurValue));
			} 
			else
			{	// s� bude vytvo�ena
				ASSERT(pObjAction->m_pOldValue != NULL);
				CreateNetFromNetValue((CPosNetObjectBase*)(pObjAction->m_pOldValue));
			}
			return TRUE;
		}
	case IDA_NET_KEY_EDIT:
	case IDA_NET_KEY_MOVE:
		{	// p�esun nebo �pravy objektu
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			ASSERT(pObjAction->m_pCurValue != NULL);
			ASSERT(pObjAction->m_pOldValue != NULL);
			// kop�ruji data objektu
			if (pObjAction->m_pOldValue->IsKindOf(RUNTIME_CLASS(CPosNetObjectBase)))
			{
				ASSERT(((CPosNetObjectBase*)(pObjAction->m_pCurValue))->GetID() == ((CPosNetObjectBase*)(pObjAction->m_pOldValue))->GetID());
				CPosNetObjectBase* pDestObj = (CPosNetObjectBase*)GetDocumentObjectFromValue(pObjAction->m_pOldValue);
				ASSERT(pDestObj != NULL);
				if (pDestObj != NULL)
				{
					pDestObj->CopyFrom(pObjAction->m_pOldValue);
					// synchronizuji kurzor
					//if (GetPosEdEnvironment()->m_optPosEd.m_bBldSynchrAction)
					{
						m_CursorObject.SetCursorRealPosition(pDestObj->m_nBaseRealPos.nPos,FALSE);
					}
				}
			}
			return TRUE;
		}
		////////////////
		// POJMENOVAN� PLOCHY
	case IDA_NAMEAREA_CREATE:
		{	// vlo�en� lesa
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			if (pObjAction->m_pCurValue == NULL)
			{	// les bude vytvo�en
				ASSERT(pObjAction->m_pOldValue != NULL);
				CreateNameAreaFromNameAreaValue((CPosNameAreaObject*)(pObjAction->m_pOldValue));
			} 
			else
			{	// les bude smaz�n
				ASSERT(pObjAction->m_pOldValue == NULL);
				DeleteNameAreaFromNameAreaValue((CPosNameAreaObject*)(pObjAction->m_pCurValue));
			}
			UpdateDocNameObjects();
			return TRUE;
		}
	case IDA_NAMEAREA_DELETE:
		{	// smaz�n� lesa
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			if (pObjAction->m_pCurValue != NULL)
			{	// les bude smaz�n
				ASSERT(pObjAction->m_pOldValue == NULL);
				DeleteNameAreaFromNameAreaValue((CPosNameAreaObject*)(pObjAction->m_pCurValue));
			} 
			else
			{	// les bude vytvo�en
				ASSERT(pObjAction->m_pOldValue != NULL);
				CreateNameAreaFromNameAreaValue((CPosNameAreaObject*)(pObjAction->m_pOldValue));
			}
			UpdateDocNameObjects();
			return TRUE;
		}
	case IDA_NAMEAREA_MOVE:
	case IDA_NAMEAREA_EDIT:
		{	// p�esun nebo �pravy objektu
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			ASSERT(pObjAction->m_pCurValue != NULL);
			ASSERT(pObjAction->m_pOldValue != NULL);
			// kop�ruji data objektu
			if (pObjAction->m_pOldValue->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)))
			{
				ASSERT(((CPosNameAreaObject*)(pObjAction->m_pCurValue))->GetID() == ((CPosNameAreaObject*)(pObjAction->m_pOldValue))->GetID());
				CPosNameAreaObject* pDestObj = (CPosNameAreaObject*)GetDocumentObjectFromValue(pObjAction->m_pOldValue);
				ASSERT(pDestObj != NULL);
				if (pDestObj != NULL)
				{
					pDestObj->CopyFrom(pObjAction->m_pOldValue);
					// synchronizuji kurzor
					//if (GetPosEdEnvironment()->m_optPosEd.m_bBldSynchrAction)
					{
						CRect rHull; 
						pDestObj->GetObjectHull(rHull);
						CPoint ptCenter((rHull.left + rHull.right) / 2, (rHull.top + rHull.bottom) / 2);
						m_CursorObject.SetCursorLogPosition(ptCenter, FALSE);
					}
				}
			}
			if ((pAction->m_nActType == IDA_NAMEAREA_EDIT) &&
				(pObjAction->m_pCurValue->GetObjectName().CompareNoCase(pObjAction->m_pOldValue->GetObjectName()) != 0))
			{
				UpdateDocNameObjects();
			}
			return TRUE;
		}
		////////////////
		// KL��OV� BODY
	case IDA_KEYPOINT_CREATE:
		{	// vlo�en� lesa
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			if (pObjAction->m_pCurValue == NULL)
			{	// les bude vytvo�en
				ASSERT(pObjAction->m_pOldValue != NULL);
				CreateKeyPointFromKeyPointValue((CPosKeyPointObject*)(pObjAction->m_pOldValue));
			} 
			else
			{	// les bude smaz�n
				ASSERT(pObjAction->m_pOldValue == NULL);
				DeleteKeyPointFromKeyPointValue((CPosKeyPointObject*)(pObjAction->m_pCurValue));
			}
			UpdateDocKPlaObjects();
			return TRUE;
		}
	case IDA_KEYPOINT_DELETE:
		{	// smaz�n� lesa
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			if (pObjAction->m_pCurValue != NULL)
			{	// les bude smaz�n
				ASSERT(pObjAction->m_pOldValue == NULL);
				DeleteKeyPointFromKeyPointValue((CPosKeyPointObject*)(pObjAction->m_pCurValue));
			} else
			{	// les bude vytvo�en
				ASSERT(pObjAction->m_pOldValue != NULL);
				CreateKeyPointFromKeyPointValue((CPosKeyPointObject*)(pObjAction->m_pOldValue));
			}
			UpdateDocKPlaObjects();
			return TRUE;
		}
	case IDA_KEYPOINT_MOVE:
	case IDA_KEYPOINT_EDIT:
		{	// p�esun nebo �pravy objektu
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			ASSERT(pObjAction->m_pCurValue != NULL);
			ASSERT(pObjAction->m_pOldValue != NULL);
			// kop�ruji data objektu
			if (pObjAction->m_pOldValue->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))
			{
				ASSERT(((CPosKeyPointObject*)(pObjAction->m_pCurValue))->GetID() == ((CPosKeyPointObject*)(pObjAction->m_pOldValue))->GetID());
				CPosKeyPointObject* pDestObj = (CPosKeyPointObject*)GetDocumentObjectFromValue(pObjAction->m_pOldValue);
				ASSERT(pDestObj != NULL);
				if (pDestObj != NULL)
				{
					pDestObj->CopyFrom(pObjAction->m_pOldValue);
					// synchronizuji kurzor
					//if (GetPosEdEnvironment()->m_optPosEd.m_bBldSynchrAction)
					{
						CRect rHull; 
						pDestObj->GetObjectHull(rHull);
						CPoint ptCenter((rHull.left + rHull.right) / 2,(rHull.top + rHull.bottom) / 2);
						m_CursorObject.SetCursorLogPosition(ptCenter, FALSE);
					}
				}
			}
			if ((pAction->m_nActType == IDA_KEYPOINT_EDIT) &&
				(lstrcmp( ((CPosKeyPointObject*)(pObjAction->m_pCurValue))->m_sName, ((CPosKeyPointObject*)(pObjAction->m_pOldValue))->m_sName ) != 0))
			{
				UpdateDocKPlaObjects();
			}
			return TRUE;
		}
		///////////////////////////////
		// background images
	case IDA_BG_IMAGE_CREATE:
		{	// vlo�en�
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			if (pObjAction->m_pCurValue == NULL)
			{	// bude vytvo�en
				ASSERT(pObjAction->m_pOldValue != NULL);
				CreateBGImageFromBGImageValue((CPosBackgroundImageObject *)(pObjAction->m_pOldValue));
			} 
			else
			{	// bude smaz�n
				ASSERT(pObjAction->m_pOldValue == NULL);
				DeleteBGImageFromBGImageValue((CPosBackgroundImageObject*)(pObjAction->m_pCurValue));
			}
			if (m_pObjsPanel != NULL) m_pObjsPanel->UpdateDocBgImages();
			return TRUE;
		}
	case IDA_BG_IMAGE_DELETE:
		{	// smaz�n� 
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			if (pObjAction->m_pCurValue != NULL)
			{	// les bude smaz�n
				ASSERT(pObjAction->m_pOldValue == NULL);
				DeleteBGImageFromBGImageValue((CPosBackgroundImageObject*)(pObjAction->m_pCurValue));
			} 
			else
			{	// les bude vytvo�en
				ASSERT(pObjAction->m_pOldValue != NULL);
				CreateBGImageFromBGImageValue((CPosBackgroundImageObject*)(pObjAction->m_pOldValue));
			}
			if (m_pObjsPanel != NULL) m_pObjsPanel->UpdateDocBgImages();
			return TRUE;
		}
	case IDA_BG_IMAGE_EDIT:
		{	// vlo�en�
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			ASSERT(pObjAction->m_pCurValue != NULL);
			ASSERT(pObjAction->m_pOldValue != NULL);

			// kop�ruji data objektu
			ASSERT(((CPosBackgroundImageObject*)(pObjAction->m_pCurValue))->m_sAreaName == ((CPosBackgroundImageObject*)(pObjAction->m_pOldValue))->m_sAreaName);
			CPosBackgroundImageObject* pDestObj = (CPosBackgroundImageObject*) GetDocumentObjectFromValue(pObjAction->m_pOldValue);
			ASSERT(pDestObj != NULL);
			if (pDestObj != NULL)
			{
				pDestObj->CopyFrom(pObjAction->m_pOldValue);
				pDestObj->CalcObjectLogPosition();
			}

			if (m_pObjsPanel != NULL) m_pObjsPanel->UpdateDocBgImages();
			return TRUE;
		}
		////////////////
		// PLOCHA
	case IDA_AREA_ADD:
	case IDA_AREA_REMOVE:
	case IDA_AREA_DEL:
		{
			ASSERT_KINDOF(CPosObjectAction, pAction);
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			ASSERT(pObjAction->m_pCurValue != NULL);
			ASSERT(pObjAction->m_pOldValue != NULL);
			ASSERT_KINDOF(CPosAreaObject,pObjAction->m_pOldValue);
			// kopie dat
   
			// invalidate rect of old m_CurrentArea
			bool valid;
			CFMntScrollUpdate cUpdate(CFMntScrollerView::uvfEOInvalRect, NULL);
			if (valid = m_CurrentArea.IsValidArea() ? true : false)
			{      
				cUpdate.m_rPlace =  m_CurrentArea.GetObjectHull();;
				cUpdate.m_nParam = 1;
			}

			m_CurrentArea.CopyFrom((CPosSelAreaObject*)(pObjAction->m_pOldValue));    
			AlignCurrentArea();
    
			if (valid) UpdateAllViews(NULL, cUpdate.m_nType, &cUpdate);

			return TRUE;
		}  
		////////////////
		// TEXTURY
	case IDA_TEXTURE_SET:
		{	// zm�na textury
			AfxGetApp()->BeginWaitCursor();
			ASSERT_KINDOF(CPosTextureAction, pAction);
			CPosTextureAction* pTxtrAction = (CPosTextureAction*)pAction;

			Ref<CTextureLayer> layer = m_PoseidonMap.GetTextureLayer(pTxtrAction->m_TextureLayerName);
			if (layer.IsNull())
			{
				ASSERT(FALSE);
				return TRUE;
			}
			// synchronizuji kurzor
			REALPOS rPos = layer->FromTextureUnitToRealPos(pTxtrAction->m_Position);
			float nUnitSz2 = layer->GetTextureRealSize() / 2.f;
			rPos.x += nUnitSz2;	// um�st�m na st�ed
			rPos.z += nUnitSz2;
			//if ((!pAction->m_bFromBuldozer)&&(m_PoseidonMap.m_pEnvironment->m_optPosEd.m_bBldSynchrAction))
			m_CursorObject.SetCursorRealPosition(rPos, FALSE);
			// zm�n�m prim�rn� texturu doumentu
			Ref<CPosTextureTemplate> text =  m_Textures.FindEx(pTxtrAction->m_pOldValue);
			layer->SetBaseTextureAt(pTxtrAction->m_Position, text);
			if (pTxtrAction->m_bUpdateTxtr) DoUpdateSecndTexturesByChangeOb(pTxtrAction->m_Position, this, layer);

			AfxGetApp()->EndWaitCursor();

			return TRUE;
		}
	case IDA_TEXTURE_AREA:
	case IDA_TEXTURE_AREA2:
	case IDA_TEXTURE_CHNG_AREA:
		{	// texturov�n� plochy
			AfxGetApp()->BeginWaitCursor();
			ASSERT_KINDOF(CPosTxtrAreaActionBase, pAction);
			CPosTxtrAreaActionBase* pTxtrAction = (CPosTxtrAreaActionBase*)pAction;
    
			pTxtrAction->SetInMap(m_PoseidonMap);

			// update sekund�rn�ch textur
			if (pTxtrAction->m_bUpdateTxtr)
			{
				Ref<CTextureLayer> layer = m_PoseidonMap.GetTextureLayer(pTxtrAction->m_TextureLayerName);
				if (layer.NotNull()) DoUpdateSecndTexturesByChange(&(pTxtrAction->m_Position), this, layer);
			}

			m_pObjsPanel->UpdateTextures();
			AfxGetApp()->EndWaitCursor();
			return TRUE;
		}
	case IDA_TEXTURE_LOCK_AREA:
		{	// texturov�n� plochy
			AfxGetApp()->BeginWaitCursor();
			ASSERT_KINDOF(CPosTxtrAreaActionBase, pAction);
			CPosTxtrAreaActionBase* pTxtrAction = (CPosTxtrAreaActionBase*)pAction;

			pTxtrAction->SetInMap(m_PoseidonMap);     

			m_pObjsPanel->UpdateTextures();
			AfxGetApp()->EndWaitCursor();
			return TRUE;
		}
	/////////////////
	// Texture layer
	case IDA_TEXTURE_LAYER_CREATE:
		{
			AfxGetApp()->BeginWaitCursor();
			ASSERT_KINDOF(CPosTextureLayerAction, pAction);
			CPosTextureLayerAction * layerAction = (CPosTextureLayerAction *) pAction;

			if (layerAction->m_bUndo)
			{
				// delete layer
				m_PoseidonMap.DeleteTextureLayer(layerAction->m_CurrName);
			}
			else
			{
				// create layer
				Ref<CTextureLayer> layer = new CTextureLayer(m_PoseidonMap);
				if (layer.NotNull())
				{          
					layer->OnNewLayer(m_PoseidonMap.GetSize().x, m_PoseidonMap.GetSize().z, layerAction->m_CurrTextureSizeInSqLog,
					layerAction->m_CurrName);

					m_PoseidonMap.AddTextureLayer(layer);
				}
			}
			m_pObjsPanel->UpdateDocTxtrLayers();
			AfxGetApp()->EndWaitCursor();
			return TRUE;
		}
	case IDA_TEXTURE_LAYER_REMOVE:
		{
			AfxGetApp()->BeginWaitCursor();
			ASSERT_KINDOF(CPosTextureLayerAction, pAction);
			CPosTextureLayerAction* layerAction = (CPosTextureLayerAction*) pAction;

			if (!layerAction->m_bUndo)
			{
				// delete layer
				m_PoseidonMap.DeleteTextureLayer(layerAction->m_OldName);
			}
			else
			{
				// create layer
				Ref<CTextureLayer> layer = new CTextureLayer(m_PoseidonMap);
				if (layer.NotNull())                  
				{
					layerAction->SetDataIntoLayer(layer);
					m_PoseidonMap.AddTextureLayer(layer);
				}
			}
			m_pObjsPanel->UpdateDocTxtrLayers();
			AfxGetApp()->EndWaitCursor();
			return TRUE;
		}
	case IDA_TEXTURE_LAYER_EDIT:
		{
			AfxGetApp()->BeginWaitCursor();
			ASSERT_KINDOF(CPosTextureLayerAction, pAction);
			CPosTextureLayerAction* layerAction = (CPosTextureLayerAction*) pAction;

			if (layerAction->m_bUndo)
			{
				// replace layer
				Ref<CTextureLayer> layer = m_PoseidonMap.GetTextureLayer(layerAction->m_CurrName);
				if (layer.NotNull())
				{ 
					layerAction->SetDataIntoLayer(layer);
					if (layer == m_PoseidonMap.GetActiveTextureLayer())
					{
						m_PoseidonMap.TransferMapToBuldozer();          
						UpdateAllViews(NULL);
					}
				}
			}
			else
			{
				// edit layer
				Ref<CTextureLayer> layer = m_PoseidonMap.GetTextureLayer(layerAction->m_OldName);

				if (layer.NotNull())
				{
					layer->m_Name = layerAction->m_CurrName;
					if (layerAction->m_CurrTextureSizeInSqLog != layer->m_nTexSizeInSqLog)
					{          
						layer->ChangeTextureLog(layerAction->m_CurrTextureSizeInSqLog);
						if (layer == m_PoseidonMap.GetActiveTextureLayer())
						{
							m_PoseidonMap.TransferMapToBuldozer();
							UpdateAllViews(NULL);
						}
					}
				}
			}
			m_pObjsPanel->UpdateDocTxtrLayers();
			AfxGetApp()->EndWaitCursor();
			return TRUE;
		}
		////////////////
		// V݊KA a EROZE
	case IDA_HEIGHT_EROZE:
	case IDA_HEIGHT_CHANGE:
		{
			ASSERT_KINDOF(CPosHeightAction, pAction);
			CPosHeightAction* pHeightAction = (CPosHeightAction*)pAction;
			if ((pHeightAction->m_pCurValue != NULL) &&
				(pHeightAction->m_pOldValue != NULL))
			{
				// update v��ky v bloku
				AfxGetApp()->BeginWaitCursor();
				if (pHeightAction->m_bFromBuldozer)
				{
					for (int z = pHeightAction->m_LftTop.z; z >= pHeightAction->m_RghBtm.z; --z)
					{
						for (int x = pHeightAction->m_LftTop.x; x <= pHeightAction->m_RghBtm.x; ++x)
						{
							LANDHEIGHT nHeight = pHeightAction->GetUnitLandHeight(x, z);
							m_PoseidonMap.SetLandscapeHeight(x, z, nHeight, true, false);
						}
					}
				}
				else     
				{
					AutoArray<STexturePosMessData> msgData;

					for (int z = pHeightAction->m_LftTop.z; z >= pHeightAction->m_RghBtm.z; --z)
					{
						for (int x = pHeightAction->m_LftTop.x; x <= pHeightAction->m_RghBtm.x; ++x)
						{
							LANDHEIGHT nHeight = pHeightAction->GetUnitLandHeight(x, z);
							m_PoseidonMap.SetLandscapeHeight(x, z, nHeight, true, false);

							STexturePosMessData& data = msgData.Append();
							data.nX = x;
							data.nZ = z;
							data.Y = nHeight;
						}
					}
					if (RealViewer()) RealViewer()->BlockLandHeightChange(msgData);
				}
				// update v��ky v map�
				m_PoseidonMap.UpdateMaxHeight();

				// update object
				CRect rHull = pHeightAction->m_Position.GetPositionHull();
				int nUpSz = VIEW_LOG_UNIT_SIZE;
				rHull.left   = rHull.left   - nUpSz;
				rHull.right  = rHull.right  + nUpSz;
				rHull.top    = rHull.top    - nUpSz;
				rHull.bottom = rHull.bottom + nUpSz;

				m_PoseidonMap.UpdateObjectsHeight(rHull);

				AfxGetApp()->EndWaitCursor();
			}
			return TRUE;
		}
	case IDA_HEIGHT_LOCK_CHANGE:
		{
			ASSERT_KINDOF(CPosHeightLockAction, pAction);
			CPosHeightLockAction* heightAction = (CPosHeightLockAction*)pAction;

			heightAction->SetInMap(m_PoseidonMap);
			return TRUE;
		}

		// Derived action - action already processed by derived class, assume update only
	case IDA_DERIVED_ACTION:
		////////////////
		// SKUPINY OBJEKTU
	case IDA_GROUPOBJ_DELETE:
	case IDA_GROUPOBJ_MOVE:
	case IDA_GROUPOBJ_PASTE:
	case IDA_GROUPOBJ_REPLACE:
	case IDA_NET_KEY_CREATE_GROUP:
		{	// nic ned�l� - pouze pro update selekce objekt�
			return TRUE;
		}
	}
	
	// nezn�m� akce
	ASSERT(FALSE);
	return FALSE;
}

void CPosEdMainDoc::OnBuldConnect(IVisViewerExchange* realViewer)
{  
	SetCurrentRealViewer(realViewer);
	if (realViewer) OnUpdateBuldozer();
}

void CPosEdMainDoc::OnBuldDisconnect()
{
	SLandscapePosMessData data;
	ZeroMemory(&data, sizeof(data));
	data.landGridX = 128;
	data.landGridY = 128;
	data.landRangeX = 128;
	data.landRangeY = 128;
	data.textureRangeX = 128;
	data.textureRangeY = 128;
	if (RealViewer()) RealViewer()->SystemInit(data, "");
	SetCurrentRealViewer(0);
}

bool CPosEdMainDoc::ChangeRealSizeUnit(float newRealSizeUnit, bool recalcLandHeight)
{  
	float changeFrac = newRealSizeUnit / m_nRealSizeUnit;

	if (changeFrac < 1 && m_PoseidonMap.m_NetsInMap.GetSize() > 0)
	{
		// roads can be outside map.
		if (IDNO == AfxMessageBox(IDS_WARN_SQ_SIZE_SMALLER, MB_YESNO)) return false;
	}

	m_nRealSizeUnit = newRealSizeUnit;
	m_PoseidonMap.OnRealSizeUnitChange(changeFrac, recalcLandHeight);

	/// TODO: UNDO/REDO operation. 
	m_ActionManager.ClearUndoRedoBuffers(); 
  
	UpdateAllViews(NULL);
	return true;
}

bool CPosEdMainDoc::ChangeSatGrid(int newSatGrid)
{
  m_satelliteGridG = newSatGrid;
  return true;
}

BOOL CPosEdMainDoc::OnOpenDocument(LPCTSTR lpszPathName)
{  
	return CDocument::OnOpenDocument(lpszPathName);
}

void CPosEdMainDoc::AlignCurrentArea()
{
  int iCurrMode = m_pObjsPanel->GetTypeObjects();

  if (iCurrMode == CPosObjectsPanel::grTypeLand)
    m_CurrentArea.SetSelAreaType(CPosSelAreaObject::satVertex); 
  else
    m_CurrentArea.SetSelAreaType(CPosSelAreaObject::satSquare); 

  //Align
  switch(iCurrMode)
  {
  case CPosObjectsPanel::grTypeLand:
    m_CurrentArea.AlignToGrid(CPoint(VIEW_LOG_UNIT_SIZE,VIEW_LOG_UNIT_SIZE),
      CPoint(VIEW_LOG_UNIT_SIZE/2,VIEW_LOG_UNIT_SIZE/2));
    break;
  case CPosObjectsPanel::grTypeWood:
  case CPosObjectsPanel::grTypeName:
    m_CurrentArea.AlignToGrid(CPoint(VIEW_LOG_UNIT_SIZE,VIEW_LOG_UNIT_SIZE),
      CPoint(0,0));
    break;
  case CPosObjectsPanel::grTypeTxtr:
    {
      int textLogSize = m_PoseidonMap.GetActiveTextureLayer()->GetTextureLogSize();
      m_CurrentArea.AlignToGrid(CPoint(textLogSize,textLogSize),
        CPoint(0,0));
      break;
    }
  default:;
  }
}

void CPosEdMainDoc::DeleteContents()
{
  m_PoseidonMap.DestroyMap();    
  m_GroupObjects.DestroyGroup();   
  m_CurrentArea.ResetArea();

  if (m_pObjsPanel)
    m_pObjsPanel->UpdateDocData(NULL, TRUE);

  base::DeleteContents();

  CUpdatePar update(CUpdatePar::uvfResetView, NULL);
  DoUpdateAllViews(&update);
}

void CPosEdMainDoc::ClearObjectSelection()
{
  m_GroupObjects.DestroyGroup();   
}

void CPosEdMainDoc::AddToObjectSelection(CPosObject * obj, bool exclusive)
{
  m_GroupObjects.AddObjectGroup(obj, exclusive);
}

void CPosEdMainDoc::UpdateAllViewsSelection()
{
  CUpdatePar update(CUpdatePar::uvfSelectionObjects, NULL);
  DoUpdateAllViews(&update);
}



void CPosEdMainDoc::SetLandTextureAt(int x, int z, int id, BOOL bFromBuldozer)
{
  UNITPOS Pos = { x, z};
  Ref<CPosTextureTemplate> pTempl = m_Textures.FindEx(id);
  ASSERT(pTempl != NULL);
  return m_PoseidonMap.SetLandTextureAt(Pos, pTempl, bFromBuldozer);
}

void CPosEdMainDoc::SetLandHeight(int x, int z, float height)
{
  UNITPOS Pos = { x, z};

  // ud�lost pro zmenu v��ky v bode
  {
    if (m_PoseidonMap.LockedVertex(Pos))
    {      
      if (RealViewer()) RealViewer()->LandHeightChange(STexturePosMessData(x,z,m_PoseidonMap.GetLandscapeHeight(Pos),0));
    }
    else
    {
      REALPOS rPos = m_PoseidonMap.FromUnitToRealPos(Pos);
      POINT	ptPos= m_PoseidonMap.FromRealToViewPos(rPos);

      CRect   rRect( ptPos.x, ptPos.y-VIEW_LOG_UNIT_SIZE, ptPos.x+VIEW_LOG_UNIT_SIZE, ptPos.y);
      CPositionArea Area;
      Area.AddRect(rRect);

      CPosHeightAction* pAction = new CPosHeightAction(IDA_HEIGHT_CHANGE,Area);
      pAction->m_bFromBuldozer = TRUE;
      // vytvo�en� dat
      pAction->CreateActionData(&m_PoseidonMap);
      // nastaven� v��ky
      pAction->SetUnitLandHeight(x, z, height);
      // realizace akce
      ProcessEditAction(pAction);
    }
  }
}

void CPosEdMainDoc::SetBlockLandHeight(const Array<STexturePosMessData> &msgData)
{
      AutoArray<STexturePosMessData> msgDataBack;
      CPositionArea Area;
      for(int i = 0; i < msgData.Size(); i++)
      {
        UNITPOS Pos = { msgData[i].nX, msgData[i].nZ };

        if (m_PoseidonMap.LockedVertex(Pos))
        {
          msgDataBack.Append()=STexturePosMessData(msgData[i]);
        }
        else
        {        
          REALPOS rPos = m_PoseidonMap.FromUnitToRealPos(Pos);
          POINT	ptPos= m_PoseidonMap.FromRealToViewPos(rPos);        
          CRect rRect( ptPos.x - VIEW_LOG_UNIT_SIZE/2, ptPos.y - VIEW_LOG_UNIT_SIZE/2,ptPos.x + VIEW_LOG_UNIT_SIZE/2, ptPos.y  + VIEW_LOG_UNIT_SIZE/2);        
          Area.AddRect(rRect);
        }
      }

      if (msgDataBack.Size() > 0)
      {
        if (RealViewer()) RealViewer()->BlockLandHeightChange(msgDataBack);
      }      

      if (Area.IsValidPosition())
      {
        CPosHeightAction* pAction = new CPosHeightAction(IDA_HEIGHT_CHANGE,Area);
        pAction->m_bFromBuldozer = TRUE;
        // vytvo�en� dat
        pAction->CreateActionData(&m_PoseidonMap);
        // nastaven� v��ky

        for(int i = 0; i < msgData.Size(); i++)
        {
          pAction->SetUnitLandHeight( msgData[i].nX, msgData[i].nZ, msgData[i].Y);             
        }

        // realizace akce 
        ProcessEditAction(pAction);
      }
}

void CPosEdMainDoc::MoveObject(const SMoveObjectPosMessData &msgData)
{
  CPosEdObject* pObj = (CPosEdObject*)(m_PoseidonMap.GetPoseidonObject(msgData.nID));
  if (pObj->Locked())
  {
    // send message back object cannot be moved
    SMoveObjectPosMessData sMsgBack;
    pObj->SetObjectDataToMessage(sMsgBack);
    if (RealViewer()) RealViewer()->ObjectMove(sMsgBack);
  }
  else
  {
    CPosEdObject* pAct = (CPosEdObject*)(pObj->CreateCopyObject());
    if ((pObj)&&(pAct))
    {	// posun objektu
      for(int i=0; i<4;i++)
        for(int j=0; j<4;j++)
          pAct->m_Position(i,j) = msgData.Position[i][j];
      // relativn� v��ka vzhledem k povrchu
      pAct->m_nRelHeight = pAct->CalcRelHeightFromCurrPos();
      pAct->CalcObjectLogPosition();
      // generuji ud�lost pro p�esun atributu
      GenerNewObjectAction(IDA_OBJECT_MOVE,pObj,pAct,TRUE,NULL);
    };
  }
}

void CPosEdMainDoc::BlockMoveObject(const Array<SMoveObjectPosMessData > &msgData)
    {
      AutoArray<SMoveObjectPosMessData> msgDataBack;
      CPosObjectGroupAction * groupAct = new CPosObjectGroupAction(IDA_GROUPOBJ_MOVE);
  
      for(int iO = 0; iO < msgData.Size(); iO++)
      {
        CPosEdObject* pObj = (CPosEdObject*)(m_PoseidonMap.GetPoseidonObject(msgData[iO].nID));
        if (!pObj)
          continue;

        if (pObj->Locked())
        {
          // send back it cannot be moved
          SMoveObjectPosMessData& data = msgDataBack.Append();
          pObj->SetObjectDataToMessage(data);
        }
        else
        {
          CPosEdObject* pAct = (CPosEdObject*)(pObj->CreateCopyObject());
          if (pAct)
          {	// posun objektu
            for(int i=0; i<3;i++)
              for(int j=0; j<4;j++)
                pAct->m_Position(i,j) = msgData[iO].Position[i][j];
            // relativn� v��ka vzhledem k povrchu
            pAct->m_nRelHeight = pAct->CalcRelHeightFromCurrPos();
            pAct->CalcObjectLogPosition();
            // generuji ud�lost pro p�esun atributu
            GenerNewObjectAction(IDA_OBJECT_MOVE,pObj,pAct,TRUE,groupAct);
          };
        };
      }

      if (msgDataBack.Size() > 0)
      {
        if (RealViewer()) RealViewer()->BlockMove(msgDataBack);
      }
      
      groupAct->m_bFromBuldozer = TRUE;
      ProcessEditAction(groupAct);
    };

void CPosEdMainDoc::SelectionObjectAdd(const SObjectPosMessData &msgData)
{
  POSITION pos=GetFirstViewPosition();
  while (pos)
  {
    CPosEdMainView *w=dynamic_cast<CPosEdMainView *>(GetNextView(pos));
    if (w) w->DoSelectPosObject(m_PoseidonMap.GetPoseidonObject(msgData.nID),msgData.bState ? CPosEdMainView::smAdd : CPosEdMainView::smDel);
  }
}

void CPosEdMainDoc::SelectionObjectClear()
{
  POSITION pos=GetFirstViewPosition();
  while (pos)
  {
    CPosEdMainView *w=dynamic_cast<CPosEdMainView *>(GetNextView(pos));
    if (w) w->DoUnselectPosObject();
  }
}

void CPosEdMainDoc::BlockSelectionObject(const Array<SMoveObjectPosMessData> &msgData)
{
  POSITION pos=GetFirstViewPosition();
  while (pos)
  {
    CPosEdMainView *w=dynamic_cast<CPosEdMainView *>(GetNextView(pos));
    if (w) 
    {
      for(int i = 0; i < msgData.Size(); i++)
      {     
        w->DoSelectPosObject(m_PoseidonMap.GetPoseidonObject(msgData[i].nID),CPosEdMainView::smAdd, i ==  msgData.Size() - 1);
      }
    }
  }
}

void CPosEdMainDoc::OnCursorPositionSet(const SMoveObjectPosMessData &msgData)
{
  POSITION pos=GetFirstViewPosition();
  while (pos)
  {
    CPosEdMainView *w=dynamic_cast<CPosEdMainView *>(GetNextView(pos));
    if (w) 
    {
      w->CursorPositionSet(msgData);
    }
  }
}

void CPosEdMainDoc::SelectionLandClear()
{
  if (m_CurrentArea.IsValidArea())
    Action_DelPoseidonCurrArea(this,NULL, TRUE);
  
}

void CPosEdMainDoc::SelectionLandAdd(const SLandSelPosMessData& msgData)
{
      REALPOS leftTop;
      leftTop.x = msgData.xs;
      leftTop.z = msgData.zs;

      REALPOS rightBottom;
      rightBottom.x = msgData.xe;
      rightBottom.z = msgData.ze;

      POINT leftTopView = m_PoseidonMap.FromRealToViewPos(leftTop);
      POINT rightBottomView = m_PoseidonMap.FromRealToViewPos(rightBottom);
      
      //Align to step
      int nMaxX = m_PoseidonMap.GetSize().x;
      int nMaxY = m_PoseidonMap.GetSize().z;
      leftTopView.x = min(max(0,(leftTopView.x / VIEW_LOG_UNIT_SIZE)), nMaxX) * VIEW_LOG_UNIT_SIZE;
      leftTopView.y = min(max(0,(leftTopView.y / VIEW_LOG_UNIT_SIZE)), nMaxY) * VIEW_LOG_UNIT_SIZE;
      rightBottomView.x = min(max(0,(rightBottomView.x / VIEW_LOG_UNIT_SIZE)), nMaxX) * VIEW_LOG_UNIT_SIZE;
      rightBottomView.y = min(max(0,(rightBottomView.y / VIEW_LOG_UNIT_SIZE)), nMaxY) * VIEW_LOG_UNIT_SIZE;      

      CRect rect(leftTopView.x, rightBottomView.y, rightBottomView.x, leftTopView.y);
      if (m_CurrentArea.GetSelAreaType() == CPosSelAreaObject::satVertex)
      {
        rect += CPoint(VIEW_LOG_UNIT_SIZE/2, VIEW_LOG_UNIT_SIZE/2);       
      }

      if (rect.Width() != 0 && rect.Height() != 0)
        Action_AddPoseidonCurrArea(rect,FALSE,FALSE,this,NULL, TRUE);
  
}

void CPosEdMainDoc::BlockSelectionLand(const Array<SLandSelPosMessData> &msgData)
{
      CRectArray area;
      for(int i = 0; i < msgData.Size(); i++)
      {

        const SLandSelPosMessData& data = msgData[i];
        REALPOS leftTop;
        leftTop.x = data.xs;
        leftTop.z = data.zs;

        REALPOS rightBottom;
        rightBottom.x = data.xe;
        rightBottom.z = data.ze;

        POINT leftTopView = m_PoseidonMap.FromRealToViewPos(leftTop);
        POINT rightBottomView = m_PoseidonMap.FromRealToViewPos(rightBottom);

        //Align to step
        int nMaxX = m_PoseidonMap.GetSize().x;
        int nMaxY = m_PoseidonMap.GetSize().z;
        leftTopView.x = min(max(0,(leftTopView.x / VIEW_LOG_UNIT_SIZE)), nMaxX) * VIEW_LOG_UNIT_SIZE;
        leftTopView.y = min(max(0,(leftTopView.y / VIEW_LOG_UNIT_SIZE)), nMaxY) * VIEW_LOG_UNIT_SIZE;
        rightBottomView.x = min(max(0,(rightBottomView.x / VIEW_LOG_UNIT_SIZE)), nMaxX) * VIEW_LOG_UNIT_SIZE;
        rightBottomView.y = min(max(0,(rightBottomView.y / VIEW_LOG_UNIT_SIZE)), nMaxY) * VIEW_LOG_UNIT_SIZE;      

        CRect rect(leftTopView.x, rightBottomView.y, rightBottomView.x, leftTopView.y);
        if (m_CurrentArea.GetSelAreaType() == CPosSelAreaObject::satVertex)
        {
          rect += CPoint(VIEW_LOG_UNIT_SIZE/2, VIEW_LOG_UNIT_SIZE/2);       
        }

        if (rect.Width() != 0 && rect.Height() != 0)
          area.Add(rect);          
      }
      Action_AddPoseidonCurrArea(area,TRUE,FALSE,this,NULL, TRUE);
}

static void IncludeToSelection(const CObArray& array, const BTree<CObject*>& selObj, VisSelection* selection, NamSelItem::ObjectType objType)
{
	unsigned int startRange = -1;
	int i;
	for (i = 0; i < array.GetSize(); ++i)
	{
		CObject* obj = array[i];
		if (selObj.Find(obj) != 0)
		{
			if (startRange == -1) startRange = i;
		}
		else
		{
			if (startRange != -1) 
			{
				selection->Include(objType, startRange, i - 1);
				startRange = -1;
			}
		}
	}
	if (startRange != -1) 
	{
		selection->Include(objType, startRange, i - 1);
	}
}

void CPosEdMainDoc::DefineNamedSelection(const RStringI& selName, const VisSelection* source/* =0 */)
{
	VisSelection* newSel = m_SelectionList.CreateSelection(selName);
	if (source)
	{
		*newSel = *source;
	}
	else
	{
		BTree<CObject*> selObject;
		///store all selected object to temporaly tree to fast search
		for (int i = 0; i < m_GroupObjects.GetGroupSize(); ++i)
		{
			selObject.Add(m_GroupObjects[i]);
		}
		///process all objects in poseidon map;
		IncludeToSelection(m_PoseidonMap.m_InstanceObjs, selObject, newSel, NamSelItem::objInstance);
		IncludeToSelection(m_PoseidonMap.m_WoodsInMap, selObject, newSel, NamSelItem::objWood);
		IncludeToSelection(m_PoseidonMap.m_NetsInMap, selObject, newSel, NamSelItem::objNet);
		IncludeToSelection(m_PoseidonMap.m_AreasInMap, selObject, newSel, NamSelItem::objArea);
		IncludeToSelection(m_PoseidonMap.m_KeyPtInMap, selObject, newSel, NamSelItem::objKeyPoint);
	}
	((CMainFrame *)AfxGetMainWnd())->_namedSelectionBar.SelectionAdded(selName);
}

namespace Functors
{
	template<class ArrayAllocator>
	class SelectByRange
	{
		mutable CPosEdMainDoc& doc;
		mutable AutoArray<CObject*, ArrayAllocator>& curSel;
	public:
		SelectByRange(CPosEdMainDoc& doc, AutoArray<CObject*, ArrayAllocator>& curSel) 
		: doc(doc)
		, curSel(curSel) 
		{
		}

		int operator () (NamSelItem::ObjectType objType, unsigned int from, unsigned int to) const
		{
			CObject* obj;
			for (unsigned int i = from ; i <= to; ++i)
			{      
				switch(objType) 
				{
				case NamSelItem::objInstance: obj = doc.m_PoseidonMap.m_InstanceObjs[i]; break;      	  
				case NamSelItem::objWood:     obj = doc.m_PoseidonMap.m_WoodsInMap[i];   break;      	  
				case NamSelItem::objNet:      obj = doc.m_PoseidonMap.m_NetsInMap[i];    break;      	  
				case NamSelItem::objArea:     obj = doc.m_PoseidonMap.m_AreasInMap[i];   break;      	  
				case NamSelItem::objKeyPoint: obj = doc.m_PoseidonMap.m_KeyPtInMap[i];   break;      	  
				default: break;
				}
				if (obj) 
				{
					curSel.Add(obj);
				}
			}
			return 0;
		}
	};
}

void CPosEdMainDoc::UseSelection(const VisSelection& sel, bool ctrl, bool shift)
{
	AutoArray<CObject*, MemAllocStack<CObject*, 256> > selObj;

	sel.ForEachSelected(Functors::SelectByRange<MemAllocStack<CObject*, 256> >(*this, selObj));
  
	BTree<CObject*> selObject;
	///store all selected object to temporally tree to fast search
	if (ctrl || shift)
	{
		for (int i = 0; i < m_GroupObjects.GetGroupSize(); ++i)
		{
			selObject.Add(m_GroupObjects[i]);
		}
	}

	ClearObjectSelection();
	for (int i = 0; i < selObj.Size(); ++i)
	{
		if (!shift) 
		{
			selObject.Add(selObj[i]);
		}
		else
		{
			if (ctrl) 
			{
				selObject.Remove(selObj[i]);
			}
			else
			{
				CObject* rem = selObject.Remove(selObj[i]);
				if (rem) AddToObjectSelection(static_cast<CPosObject*>(rem), false);
			}
		}
	}

	BTreeIterator<CObject*>iter(selObject);
	CObject** nx;
	while ((nx = iter.Next()) != 0)
	{
		AddToObjectSelection(static_cast<CPosObject*>(*nx), false);    
	}
	UpdateAllViewsSelection();
}

bool CPosEdMainDoc::DeleteSelection(const RStringI& name)
{
	m_SelectionList.DeleteSelection(name);
	((CMainFrame *)AfxGetMainWnd())->_namedSelectionBar.SelectionRemoved(name);
	return true;
}

int CPosEdMainDoc::GetSelectionCount() const
{
	return m_SelectionList.Size();
}

class NamedSelAction: public CPosAction
{
protected:
	SRef<VisSelection> selCopy;
	RStringI selName;
	bool remosel;
public:
	NamedSelAction(const RStringI& selName, const VisSelection* selCopy, bool remosel = false)
	: selName(selName)
	, selCopy( selCopy ? new VisSelection(*selCopy) : 0)
	, CPosAction(IDA_DERIVED_ACTION)
	, remosel(remosel) 
	{	
	}

	virtual BOOL RealizeAction(CDocument* doc)
	{
		CPosEdMainDoc* maindoc = static_cast<CPosEdMainDoc*>(doc);
		if (remosel) 
		{
			maindoc->DeleteSelection(selName);
		}
		else 
		{
			maindoc->DefineNamedSelection(selName, selCopy);
		}
		return CPosAction::RealizeAction(doc);
	}
	
	virtual BOOL ChangeValues() 
	{
		remosel = !remosel;
		return TRUE;
	}
};

class NamedSelActionRedefine : public NamedSelAction
{
public:
	NamedSelActionRedefine(const RStringI& selName, const VisSelection* selCopy, bool remosel=false)
	: NamedSelAction(selName, selCopy, remosel) 
	{
	}

	virtual BOOL RealizeAction(CDocument* doc)
	{
		CPosEdMainDoc* maindoc = static_cast<CPosEdMainDoc *>(doc);
		VisSelection* cur = maindoc->GetNamedSel(selName);
		VisSelection* cpy = new VisSelection(*cur);
		maindoc->DefineNamedSelection(selName, selCopy);
		selCopy = cpy;
		return CPosAction::RealizeAction(doc);
	}
};

void CPosEdMainDoc::DefineNamedSelectionAction(const RStringI& selName, const VisSelection* source)
{
	VisSelection* cur = GetNamedSel(selName);
	if (cur == 0)
	{
		ProcessEditAction(new NamedSelAction(selName, source, false));
	}
	else
	{
		ProcessEditAction(new NamedSelActionRedefine(selName, source, false));
	}
}

void CPosEdMainDoc::DeleteSelectionAction(const RStringI& selName)
{
	VisSelection* sel = m_SelectionList.GetSelection(selName);
	if (sel == 0) return;
	ProcessEditAction(new NamedSelAction(selName, sel, true));
}

void CPosEdMainDoc::RenameSelection(const RStringI& oldName, const RStringI& newName)
{
	const VisSelection* sel = m_SelectionList.GetSelection(oldName);
	VisNamedSelection* newSel = m_SelectionList.CreateSelection(newName);
	*static_cast<VisSelection*>(newSel) = *sel;
	m_SelectionList.DeleteSelection(oldName);
	((CMainFrame*)AfxGetMainWnd())->_namedSelectionBar.SelectionRemoved(oldName);
	((CMainFrame*)AfxGetMainWnd())->_namedSelectionBar.SelectionAdded(newName);
}

bool CPosEdMainDoc::RenameSelectionAction(const RStringI& oldName, const RStringI& newName)
{
	const VisNamedSelection* sel = m_SelectionList.GetSelection(oldName);
	const VisNamedSelection* newsel = m_SelectionList.GetSelection(newName);
	if (newsel != 0 || sel == 0) return false;

	class RenameSel : public CPosAction
	{
		RString oldName;
		RString newName;
  public:
		RenameSel(RString oldName, RString newName)
		: oldName(oldName)
		, newName(newName)
		, CPosAction(IDA_DERIVED_ACTION) 
		{
		}

		virtual BOOL RealizeAction(CDocument* doc)
		{
			static_cast<CPosEdMainDoc*>(doc)->RenameSelection(oldName, newName);
			return TRUE;
		}

		virtual BOOL ChangeValues() 
		{
			RString x = oldName; 
			oldName = newName; 
			newName = x;      
			return TRUE;
		}
	};
  
	ProcessEditAction(new RenameSel(oldName, newName));
	return true;
}

static void HideObjectOnContainer(const CObArray& array, bool hide)
{
	for (int i = 0; i < array.GetSize(); ++i)
	{
		CPosObject* obj = static_cast<CPosObject*>(array[i]);
		if (obj) obj->Hide(hide);
	}
}

void CPosEdMainDoc::HideObjects(bool selected/* =true */, bool hide/* =true */)
{
	if (selected)
	{
		for (int i = 0; i < m_GroupObjects.GetGroupSize(); ++i)
		{
			CPosObject* obj = static_cast<CPosObject*>(m_GroupObjects[i]);
			if (obj) obj->Hide(hide);
		}
	}
	else
	{
		HideObjectOnContainer(m_PoseidonMap.m_WoodsInMap, hide);
		HideObjectOnContainer(m_PoseidonMap.m_AreasInMap, hide);
		HideObjectOnContainer(m_PoseidonMap.m_NetsInMap, hide);
		HideObjectOnContainer(m_PoseidonMap.m_InstanceObjs, hide);
		HideObjectOnContainer(m_PoseidonMap.m_KeyPtInMap, hide);
	}
}

static void BuildInvertedSel(const CObArray& array, BTree<CObject*>& objsel, CPosGroupObject& newgroup)
{
	for (int i = 0; i < array.GetSize(); ++i)
	{
		if (array[i])
		{
			if (!objsel.Find(array[i])) newgroup.AddObjectGroup(array[i], false);    
		}
	}
}

void CPosEdMainDoc::InvertSelection()
{
	BTree<CObject*> selobj;
	for (int i = 0; i < m_GroupObjects.GetGroupSize(); ++i)
	{
		selobj.Add(m_GroupObjects[i]);
	}
  
	m_GroupObjects.DestroyGroup();

	BuildInvertedSel(m_PoseidonMap.m_WoodsInMap, selobj, m_GroupObjects);
	BuildInvertedSel(m_PoseidonMap.m_AreasInMap, selobj, m_GroupObjects);
	BuildInvertedSel(m_PoseidonMap.m_NetsInMap, selobj, m_GroupObjects);
	BuildInvertedSel(m_PoseidonMap.m_InstanceObjs, selobj, m_GroupObjects);
	BuildInvertedSel(m_PoseidonMap.m_KeyPtInMap, selobj, m_GroupObjects);
}

void CPosEdMainDoc::CropLand(const CRect& rc)
{
	m_PoseidonMap.CropLand(rc);
	UpdateAllViews(0);
}

TypeIsSimpleZeroed(CObject*);