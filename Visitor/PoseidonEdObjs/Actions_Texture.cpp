/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright © Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#ifdef Fail
#undef Fail
#endif

/////////////////////////////////////////////////////////////////////////////
// Akce s texturami

/////////////////////////////////////////////////////////////////////////////
// vytvoření akce pro změnu textury

CPosAction* Action_CreatePoseidonTexture(UNITPOS ptUn,const CPosTextureTemplate* pTempl,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pTempl != NULL && !pDocument->m_PoseidonMap.LockedTexture(ptUn))
	{
		CPosTextureAction*		pAction = NULL;
		TRY
		{
			// vytvořím akci
			pAction = new CPosTextureAction(IDA_TEXTURE_SET,ptUn);

      pAction->m_TextureLayerName = pDocument->m_PoseidonMap.GetActiveTextureLayer()->Name();
			pAction->m_bFromBuldozer = FALSE;
			// update sekundáru a view jen není-li skupinová operace
			pAction->m_bUpdateTxtr   = (pToGroup == NULL);
			pAction->m_bUpdateView   = (pToGroup == NULL);

			// jako nová bude použita kopie vzorové šablony
			pAction->m_pOldValue = pTempl->m_nTextureID;						

			// uchovám stávající hodnotu textury
			Ref<CPosTextureTemplate> pCurr = pDocument->m_PoseidonMap.GetBaseTextureAt(ptUn);
      pAction->m_pCurValue = pCurr.NotNull() ? pCurr->m_nTextureID : -1;
			
			// realizuji akci ?
			if (pToGroup)
				pToGroup->AddAction(pAction);
			else
				pDocument->ProcessEditAction(pAction);
		}
		CATCH_ALL(e)
		{
			MntReportMemoryException();
			if (pAction != NULL)
				delete pAction; // smaže se i pOldTxtr a pNewTxtr
		}
		END_CATCH_ALL
		return pAction;
	};
	return NULL;
};

CPosAction* Action_CreatePoseidonTexture(CPoint ptLog,const CPosTextureTemplate* pTempl,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	// určím unit pro log. bod
	REALPOS rPos  = pDocument->m_PoseidonMap.FromViewToRealPos(ptLog);
	// zaokrouhlím na 50 m
	rPos.x = (float)(((int)(rPos.x / pDocument->m_nRealSizeUnit))*pDocument->m_nRealSizeUnit);
	rPos.z = (float)(((int)(rPos.z / pDocument->m_nRealSizeUnit))*pDocument->m_nRealSizeUnit);
	UNITPOS rUnit = pDocument->m_PoseidonMap.FromRealToTextureUnitPos(rPos);	
	// vytvořím texturu na UNIT pozici
	return Action_CreatePoseidonTexture(rUnit,pTempl,pDocument,pToGroup);
};

/////////////////////////////////////////////////////////////////////////////
// vytvoření akce pro texturování plochy - texturou

CPosAction* Action_TexturePoseidonTxtr2(const CPositionArea& Area,const CPosTextureTemplate* pSource,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  if (pSource != NULL)
  {
    CPosTxtrAreaAction2* pAction  = NULL;
    TRY
    {
      // vytvoření akce
      pAction = new CPosTxtrAreaAction2(IDA_TEXTURE_AREA2,Area);

      // texturování plochy
      pAction->m_TextureLayerName = pDocument->m_PoseidonMap.GetActiveTextureLayer()->Name();
      CPoseidonMap* pMap = &pDocument->m_PoseidonMap;

      if (!pAction->CreateActionData(pMap, pSource->m_nTextureID))
      {
        delete pAction;
        return NULL;
      }
      
      // provedu akci
      if (pToGroup)
        pToGroup->AddAction(pAction);
      else
        pDocument->ProcessEditAction(pAction);
      AfxGetApp()->EndWaitCursor();
    }
    CATCH_ALL(e)
    {
      MntReportMemoryException();
      if (pAction)
      {
        delete pAction;
        pAction = NULL;
      }
    }
    END_CATCH_ALL
      return pAction;
  }
  return NULL;
};

/////////////////////////////////////////////////////////////////////////////
// vytvoření akce pro texturování plochy - pásmem

CPosAction* Action_TexturePoseidonZone(const CPositionArea& Area,const CPosTextureZone* pSource,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pSource != NULL)
	{
		CPosZoneTxtrAreaAction*	pAction  = NULL;
		TRY
		{
			// vytvoření akce
			pAction = new CPosZoneTxtrAreaAction(IDA_TEXTURE_AREA,Area);

			// texturování plochy
      pAction->m_TextureLayerName = pDocument->m_PoseidonMap.GetActiveTextureLayer()->Name();
      if (!pAction->CreateActionData(&pDocument->m_PoseidonMap, pSource))
      {
        delete pAction;
        return NULL;
      }
			
			// provedu akci
			if (pToGroup)
				pToGroup->AddAction(pAction);
			else
				pDocument->ProcessEditAction(pAction);
			AfxGetApp()->EndWaitCursor();
		}
		CATCH_ALL(e)
		{
			MntReportMemoryException();
			if (pAction)
			{
				delete pAction;
				pAction = NULL;
			}
		}
		END_CATCH_ALL
		return pAction;
	}
	return NULL;
};

/////////////////////////////////////////////////////////////////////////////
// CCreateLandParamsDlg dialog

class CCreateLandParamsDlg : public CDialog
{
// Construction
public:
	CCreateLandParamsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	const CPosTextureLand* m_pLand;
	//{{AFX_DATA(CCreateLandParamsDlg)
	enum { IDD = IDD_CREATE_LAND_PARAMS };
	BOOL	m_bRandomVariants;
	BOOL	m_bModifGround;
	BOOL	m_bModifSea;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCreateLandParamsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCreateLandParamsDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CCreateLandParamsDlg::CCreateLandParamsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCreateLandParamsDlg::IDD, pParent)
{
	m_pLand = NULL;
	//{{AFX_DATA_INIT(CCreateLandParamsDlg)
	m_bRandomVariants = TRUE;
	m_bModifGround = TRUE;
	m_bModifSea	   = TRUE;
	//}}AFX_DATA_INIT
}

BOOL CCreateLandParamsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	if (m_pLand)
		::SetWindowText(DLGCTRL(IDC_SHOW_LAND_NAME),m_pLand->m_sLandName);
	return TRUE;
}

void CCreateLandParamsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCreateLandParamsDlg)
	DDX_Check(pDX, IDC_RANDOMIZE_VARIABLES, m_bRandomVariants);
	DDX_Check(pDX, IDC_MODIF_TEXTRS_GROUD, m_bModifGround);
	DDX_Check(pDX, IDC_MODIF_TEXTRS_SEA, m_bModifSea);
	//}}AFX_DATA_MAP
	if (SAVEDATA)
	{
		if ((!m_bModifGround)&&(!m_bModifSea))
		{
			AfxMessageBox(IDS_NO_MODIF_SET,MB_OK|MB_ICONEXCLAMATION);
			pDX->Fail();
		}
	}
}


BEGIN_MESSAGE_MAP(CCreateLandParamsDlg, CDialog)
	//{{AFX_MSG_MAP(CCreateLandParamsDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// vytvoření akce pro texturování plochy - krajinou

CPosAction* Action_TexturePoseidonLand(const CPositionArea& Area,const CPosTextureLand* pSource,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pSource != NULL)
	{
		if (pSource->m_LandZones.GetSize() <= 0)
		{	// není to korektní typ krajiny
			AfxMessageBox(IDS_INVALID_LAND_TYPE,MB_OK|MB_ICONEXCLAMATION);
			return NULL;
		}
		
		// parametry pro texturování
		CCreateLandParamsDlg dlg;
		dlg.m_pLand = pSource;
		if (pSource->m_bIsSea)
			dlg.m_bModifGround = FALSE;
		else
		{
			CPosTextureZone* pZone = (CPosTextureZone*)(pSource->m_LandZones[0]);
			if ((pZone != NULL)&&(pZone->m_nHeightMin >= SEA_LAND_HEIGHT))
				dlg.m_bModifSea = FALSE;
		}

		if (dlg.DoModal()!=IDOK) 
      return NULL;
		
		////////////////////////////////////////////////////////
		// texturování
		CPosLandTxtrAreaAction*	pAction  = NULL;
		TRY
		{
			// vytvoření akce
			pAction = new CPosLandTxtrAreaAction(IDA_TEXTURE_AREA,Area);
			// texturování plochy

      WORD flags = 0;
      if (dlg.m_bModifSea)
        flags = TEXTURE_UNDER_SEA;

      if (dlg.m_bModifGround)
        flags |= TEXTURE_GROUND;

      if (dlg.m_bRandomVariants)
        flags |= TEXTURE_RANDOM;

      pAction->m_TextureLayerName = pDocument->m_PoseidonMap.GetActiveTextureLayer()->Name();
      if (!pAction->CreateActionData(&pDocument->m_PoseidonMap, pSource, flags))
      {
        delete pAction;
        return NULL;
      }
			
			// provedu akci
			if (pToGroup)
				pToGroup->AddAction(pAction);
			else
				pDocument->ProcessEditAction(pAction);
			AfxGetApp()->EndWaitCursor();
		}
		CATCH_ALL(e)
		{
			MntReportMemoryException();
			if (pAction)
			{
				delete pAction;
				pAction = NULL;
			}
		}
		END_CATCH_ALL
		return pAction;
	}
	return NULL;
};


/////////////////////////////////////////////////////////////////////////////
// CChangeTxtrParamsDlg dialog

class CChangeTxtrParamsDlg : public CDialog
{
// Construction
public:
	CChangeTxtrParamsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	CPosEdMainDoc *				m_pDocument;
	const CPosTextureTemplate *	m_pNew;
	Ref<CPosTextureTemplate>  m_pOld;
	const CPositionArea *		m_pArea;

	//{{AFX_DATA(CChangeTxtrParamsDlg)
	enum { IDD = IDD_TXTR_SELCHNG_TEXTURE };
	CListBox	m_List;
	//}}AFX_DATA

			void UpdateTextureList();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChangeTxtrParamsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CChangeTxtrParamsDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CChangeTxtrParamsDlg::CChangeTxtrParamsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CChangeTxtrParamsDlg::IDD, pParent)
{
	m_pOld = NULL;
	//{{AFX_DATA_INIT(CChangeTxtrParamsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

void CChangeTxtrParamsDlg::UpdateTextureList()
{
 	m_List.ResetContent();

  AfxGetApp()->BeginWaitCursor();
	// obal pro texturování plochy
	CRect	rLogHull;
	UNITPOS LftTop,RghBtm;
	float   nUnitSz2   = ((float)m_pDocument->m_nRealSizeUnit)/2.f;

	rLogHull = m_pArea->GetPositionHull();
	m_pDocument->m_PoseidonMap.GetTextureUnitPosHull(LftTop,RghBtm,rLogHull);

	// nuluji očítadla
	for(int i= 0; i<m_pDocument->m_Textures.Size(); i++)
	{
		PTRTEXTURETMPLT pTxtr = m_pDocument->m_Textures[i];
		if (pTxtr)
			pTxtr->m_nTxtCounter = 0;
	}
	// texturování
	
	for(int x=LftTop.x; x<=RghBtm.x; x++)
	for(int z=LftTop.z; z>=RghBtm.z; z--)
	{
		// testovaný unit
		UNITPOS nUnit         = { x, z };	
		if (m_pDocument->m_PoseidonMap.IsTextureUnitCenterAtArea(nUnit,m_pArea))
		{
			PTRTEXTURETMPLT pTxtr = m_pDocument->m_PoseidonMap.GetBaseTextureAt(nUnit);
			if (pTxtr.NotNull())
				pTxtr->m_nTxtCounter++;
		}
	};
	for(int i= 0; i < m_pDocument->m_Textures.Size(); i++)
	{
		PTRTEXTURETMPLT pTxtr = m_pDocument->m_Textures[i];
		if ((pTxtr.NotNull())&&(pTxtr->m_nTxtCounter > 0))
		{
			// vložím texturu
			CString sText;
			sText  = pTxtr->m_sTextureName;
			sText += _T(" (");
			sText += pTxtr->m_sTextureFile;
			sText += _T(")");
			LBAddString(m_List.m_hWnd, sText, (DWORD)pTxtr.GetRef());
		}
	}
	
	AfxGetApp()->EndWaitCursor();

	if (m_List.GetCount()>0)
		m_List.SetCurSel(0);    
};

BOOL CChangeTxtrParamsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CString sText;
	sText  = m_pNew->m_sTextureName;
	sText += _T(" (");
	sText += m_pNew->m_sTextureFile;
	sText += _T(")");
	::SetWindowText(DLGCTRL(IDC_TXTR_NEW),sText);

	UpdateTextureList();
	return TRUE;
}

void CChangeTxtrParamsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChangeTxtrParamsDlg)
	DDX_Control(pDX, IDC_LIST, m_List);
	//}}AFX_DATA_MAP
	if (SAVEDATA)
	{
		m_pOld = NULL;
		DWORD dwData;
		if (LBGetCurSelData(m_List.m_hWnd,dwData))
			m_pOld = (CPosTextureTemplate*)dwData;

		if (m_pOld == NULL)
		{
			AfxMessageBox(IDS_NO_CHANGE_TEXTURE_SELECT,MB_OK|MB_ICONEXCLAMATION);
			pDX->Fail();
		}
	}
}


BEGIN_MESSAGE_MAP(CChangeTxtrParamsDlg, CDialog)
	//{{AFX_MSG_MAP(CChangeTxtrParamsDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// vytvoření akce pro texturování plochy - krajinou

CPosAction* Action_TexturePoseidonChng(const CPositionArea& Area,const CPosTextureTemplate* pSource,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	CChangeTxtrParamsDlg dlg;
	dlg.m_pDocument = pDocument;
	dlg.m_pNew  = pSource;
	dlg.m_pArea = &Area;

	if (dlg.DoModal() != IDOK)
		return NULL;
	// provedení záměny
	{
		////////////////////////////////////////////////////////
		// texturování
		CPosChngTxtrAreaAction*	pAction  = NULL;
		TRY
		{
			// vytvoření akce
			pAction = new CPosChngTxtrAreaAction(IDA_TEXTURE_CHNG_AREA,Area);

      pAction->m_TextureLayerName = pDocument->m_PoseidonMap.GetActiveTextureLayer()->Name();
      if (!pAction->CreateActionData(&pDocument->m_PoseidonMap, dlg.m_pOld.IsNull() ? -1 : dlg.m_pOld->m_nTextureID ,(pSource ? pSource->m_nTextureID:-1)))
      {
        delete pAction;
        return NULL;
      }
		
			// provedu akci
			if (pToGroup)
				pToGroup->AddAction(pAction);
			else
				pDocument->ProcessEditAction(pAction);
			AfxGetApp()->EndWaitCursor();
		}
		CATCH_ALL(e)
		{
			MntReportMemoryException();
			if (pAction)
			{
				delete pAction;
				pAction = NULL;
			}
		}
		END_CATCH_ALL
		return pAction;
	}
	return NULL;
};

//////////////////////////////////////////////////////////////////////////////////


CPosAction* Action_PasteClipboardTexture(PosClipBoardTexture * clipData,CPosEdMainDoc* pDocument,const CPoint& cursorLog, CPosActionGroup* pToGroup)
{  
  if (!clipData)
    return NULL;

  CPosClipboardTxtrAreaAction * pAction;
  TRY
  {
    AfxGetApp()->BeginWaitCursor();

    REALPOS cursorPos = pDocument->m_PoseidonMap.FromViewToRealPos(cursorLog);
    float realHeigth = clipData->rectreal[3] -  clipData->rectreal[1];
    float realWidth = clipData->rectreal[2] -  clipData->rectreal[0];
    FltRect realRect(cursorPos.x, cursorPos.z - realHeigth, cursorPos.x + realWidth, cursorPos.z);
    CRect logRect = pDocument->m_PoseidonMap.FromRealToViewRect(realRect);
    CPositionArea area;
    area.AddRect(logRect);

    pAction = new CPosClipboardTxtrAreaAction(IDA_TEXTURE_AREA, area);

    pAction->m_TextureLayerName = pDocument->m_PoseidonMap.GetActiveTextureLayer()->Name();    
    
    if (!pAction->CreateActionData(&pDocument->m_PoseidonMap, clipData, cursorPos))
    {
      delete pAction;
      return NULL;
    }

    // provedu akci
    if (pToGroup)
      pToGroup->AddAction(pAction);
    else
      pDocument->ProcessEditAction(pAction);

    AfxGetApp()->EndWaitCursor();
  }
  CATCH_ALL(e)
  {
    MntReportMemoryException();
    if (pAction)
    {
      delete pAction;
      pAction = NULL;
    }
  }
  END_CATCH_ALL

  return pAction;  
}

CPosAction* Action_PasteAbsolutClipboardTexture(PosClipBoardTexture * clipData,CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup)
{
  if (!clipData)
    return NULL;

  CPosClipboardTxtrAreaAction * pAction;
  TRY
  {
    AfxGetApp()->BeginWaitCursor();

    FltRect realRect(clipData->rectreal[0], clipData->rectreal[1], clipData->rectreal[2], clipData->rectreal[3]);
    CRect logRect = pDocument->m_PoseidonMap.FromRealToViewRect(realRect);

    CPositionArea area;
    area.AddRect(logRect);

    pAction = new CPosClipboardTxtrAreaAction(IDA_TEXTURE_AREA, area);

    pAction->m_TextureLayerName = pDocument->m_PoseidonMap.GetActiveTextureLayer()->Name();
    REALPOS cursorPos;
    cursorPos.x = clipData->rectreal[0];
    cursorPos.z = clipData->rectreal[3];

    if (!pAction->CreateActionData(&pDocument->m_PoseidonMap, clipData, cursorPos))
    {
      delete pAction;
      return NULL;
    }

    // provedu akci
    if (pToGroup)
      pToGroup->AddAction(pAction);
    else
      pDocument->ProcessEditAction(pAction);

    AfxGetApp()->EndWaitCursor();
  }
  CATCH_ALL(e)
  {
    MntReportMemoryException();
    if (pAction)
    {
      delete pAction;
      pAction = NULL;
    }
  }
  END_CATCH_ALL

  return pAction;
}

CPosAction* Action_TextureLock(const CPositionArea& Area,bool lock, CPosEdMainDoc& doc,CPosActionGroup * pToGroup)
{  
  CPosTxtrAreaLockAction * pAction  = NULL;
  TRY
  {
    // vytvoření akce
    pAction = new CPosTxtrAreaLockAction(IDA_TEXTURE_LOCK_AREA,Area);

    // texturování plochy
    pAction->m_TextureLayerName = doc.m_PoseidonMap.GetActiveTextureLayer()->Name();

    if (!pAction->CreateActionData(doc.m_PoseidonMap, lock))
    {
      delete pAction;
      return NULL;
    }

    // provedu akci
    if (pToGroup)
      pToGroup->AddAction(pAction);
    else
      doc.ProcessEditAction(pAction);
    AfxGetApp()->EndWaitCursor();
  }
  CATCH_ALL(e)
  {
    MntReportMemoryException();
    if (pAction)
    {
      delete pAction;
      pAction = NULL;
    }
  }
  END_CATCH_ALL
    return pAction;    
};

