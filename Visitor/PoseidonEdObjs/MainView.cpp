/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#include <float.h>
#include ".\poseidonedobjs.h"
#include "PosChildFrm.h"
#include "PosClipboard.h"
#include "DlgCropLand.h"
#include "RoadEditorView.h"

TypeIsSimple(HeightElem);

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Parametry pro podporu pr�ce se scrollerem

IMPLEMENT_DYNAMIC(CEditObjsParams, CFMntScrollEditParams)

CEditObjsParams::CEditObjsParams()
{
}

CEditObjsParams::~CEditObjsParams()
{	
	ClearSelObject(); 
}

void CEditObjsParams::SetSelObject(const CPosObject* pObject)
{
	ASSERT(m_pSelObject== NULL);	
	// kopie objektu pro editaci
	TRY
	{
		m_pSelObject = pObject->CreateCopyObject();
		ASSERT(m_pSelObject != NULL);
		if (m_pSelObject == NULL) AfxThrowMemoryException();
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		ClearSelObject();
	}
	END_CATCH_ALL
}

void CEditObjsParams::ClearSelObject()
{
	// sma�u edita�n� atribut
	if (m_pSelObject) delete m_pSelObject;
	m_pSelObject = NULL;
}

/*BOOL CEditObjsParams::IsObjSelected(const CPosObject& obj)
{
  if (m_pSelObject == NULL || !m_pSelObject->IsKindOf(RUNTIME_CLASS(CPosObject)))
    return FALSE;

  if (!m_pSelObject->IsKindOf(RUNTIME_CLASS(CPosGroupObject)))
    return ((CPosObject *) m_pSelObject)->IsSameObject(obj);

  return FALSE;
}
*/

/////////////////////////////////////////////////////////////////////////////
// Parametry pro lok�ln� podporu editace atributu

IMPLEMENT_DYNCREATE(CEditObjsInfo, CFMntScrollEditInfo)

CEditObjsInfo::CEditObjsInfo()
{
	m_pEditParams = NULL;
};

CEditObjsInfo::~CEditObjsInfo()
{
	if (m_pEditParams)
		m_pEditParams->ClearSelObject();
};

// TRACKER pro objekt
// podle parametr� objektu nastav� tracker
void CEditObjsInfo::CreateTracker(CFMntScrollerView* pScroller,BOOL bDraw)
{
	ASSERT(m_pSelObject != NULL);
	ASSERT_KINDOF(CPosObject,m_pSelObject);
	// najdu odpov�daj�c� parametry editace
	CEditObjsParams* pEdParams = ((CEditObjsParams*)(pScroller->m_pEditParams));
	ASSERT(pEdParams);
	ASSERT_KINDOF(CEditObjsParams,pEdParams);
	// vytvo��m tracker podle objektu
	{	// z�kladn� tracker

		m_pSelTracker =  new CTrackerBase((CPosGroupObject*)(m_pSelObject),((CPosEdMainView*)pScroller)->m_pPoseidonMap,pScroller);

		// nastav�m parametry trackeru - m_logPos
		((CPosObject*)m_pSelObject)->CalcObjectTrackerPosition(((CTrackerBase*)m_pSelTracker)->m_logPos); 	
	
		// p�evedu na DP
		((CTrackerBase*)m_pSelTracker)->m_devPos.CopyFrom(&(((CTrackerBase*)m_pSelTracker)->m_logPos));
		((CTrackerBase*)m_pSelTracker)->m_devPos.ScrolltoDP(pScroller);

		// vykreslen�
		CFMntScrollEditInfo::CreateTracker(pScroller,bDraw);
	}
};

// Just refresh tracker
void CEditObjsInfo::UpdateObjectChanges(CFMntScrollerView*pScroller, BOOL bDelete, BOOL bDraw)
{
  if (!bDelete)
  {  
    ((CTrackerBase*)m_pSelTracker)->m_logPos.ResetPosition();
    ((CPosObject*)m_pSelObject)->CalcObjectTrackerPosition(((CTrackerBase*)m_pSelTracker)->m_logPos); 
    
    OnChangeDP(pScroller);    

    if (bDraw)    
      ((CTrackerBase*)m_pSelTracker)->Draw(pScroller->GetDC());    
  }
  else
  {
    DestroyTracker(pScroller, bDraw);
  }
}

void CEditObjsInfo::OnChangeDP(CFMntScrollerView* pScroller)
{  
  m_pSelTracker->OnChangeDP();
}

/////////////////////////////////////////////////////////////////////////////
// CPosEdMainView

IMPLEMENT_DYNAMIC(CPosEdMainView, CFMntScrollerView)

CPosEdMainView::CPosEdMainView()
{
	m_ClipboardDataSource = NULL;
	m_pPoseidonMap = NULL;
	m_pObjsPanel = NULL;
	// napojen� na parametry editace
	m_pEInfoClass = RUNTIME_CLASS(CEditObjsInfo);  
	_activeExEditor = 0;
	_roadVectorEditor = new RoadEditorView(this);
}

CPosEdMainView::~CPosEdMainView()
{
	ActivateExEditor(0);
	if (m_ClipboardDataSource) delete m_ClipboardDataSource;
}

BEGIN_MESSAGE_MAP(CPosEdMainView, CFMntScrollerView)
	//{{AFX_MSG_MAP(CPosEdMainView)
	ON_WM_ERASEBKGND()
	ON_WM_SETCURSOR()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_MBUTTONDOWN()	
	ON_WM_MOUSEWHEEL()  
	//}}AFX_MSG_MAP
	ON_WM_MOUSEMOVE()
	ON_WM_ACTIVATE()
	ON_COMMAND(ID_EDIT_SELECT_ALL, OnEditSelectAll)
	ON_COMMAND(ID_EDIT_DESELECT, OnEditDeselect)
	ON_COMMAND(ID_EDIT_PASTE_ABSOLUT, OnEditPasteAbsolut)
	ON_COMMAND(ID_PROJECT_EXPORTMAPASIMAGE, OnProjectExportmapasimage)
	ON_COMMAND(ID_TOOLS_NEWROADEDITOR, OnNewRoadEditor)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_NEWROADEDITOR, OnNewRoadEditorUpdate)
END_MESSAGE_MAP()

// aktu�ln� m���tko (0,5>
double CPosEdMainView::GetCurrentViewScale() const
{
	if ((m_nScale == SS_ISOTROPICFIT)||
		(m_nScale == SS_ANISOTROPICFIT))
	{	// zarovn�v�n� do okna
		CSize  sz = GetLogInMM();
		double sc = (double)min(sz.cx,sz.cy);
		if (sc == 0)
			sc  = 0.1;
		// ve 100 % = MM_LOMETRIC -> 10 bod� na mm
		sc = 10./sc;
		if (sc < 0.1)
			sc = 0.1;
		if (sc > 5.)
			sc = 5.;
		return sc;
	};
	// podle m���tka
	double sc = (double)m_nScale;
	sc /= SCALE_UNIT;
	return sc;
};

LANDHEIGHT CPosEdMainView::GetCountLnStep() const
{
	double nStep = COUNTLN_STEP_NORMAL;
	double nScale= GetCurrentViewScale();
	if (nScale < 0.8)
	{
		nStep = 20.;
		if (nScale < 0.5)
		{
			nStep = 30.;
			if (nScale < 0.35)
			{
				nStep = 40.;
				if (nScale < 0.20)
					nStep = 50.;
			}
		}
	}
	return (LANDHEIGHT)nStep;
}

/////////////////////////////////////////////////////////////////////////////
// Update view

// nastaven� metriky
void CPosEdMainView::InitialUpdateMetrics()
{	// nastaven� obsahu pro nov� okno
	CFMntScrollerView::InitialUpdateMetrics();
	// napojen� na data dokumentu
	ASSERT(m_pDocument != NULL);
	ASSERT_KINDOF(CPosEdMainDoc,m_pDocument);
	m_pPoseidonMap = &GetDocument()->m_PoseidonMap;

	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	ASSERT(pEnvir != NULL);

	// velikost plochy podle mapy
	m_rAreaLog.SetRect(-VIEW_LOG_UNIT_SIZE,0,VIEW_LOG_UNIT_SIZE*m_pPoseidonMap->GetSize().x,VIEW_LOG_UNIT_SIZE*(m_pPoseidonMap->GetSize().z + 1));
	SetScrollSizes(MM_TEXT);

	// men�� zobrazen� nen� centrov�no
	m_bCenterInView = FALSE;
	// po��te�n� m���tko
	OnSetScale(pEnvir->m_optPosEd.m_nProjScale);
	if (pEnvir->m_optPosEd.m_bFitProjWnd)
		OnSetScale(SS_ISOTROPICFIT);
	// pozad� podle barvy frame ve Window
	m_wndColor = GetSysColor(COLOR_3DFACE);
};

void CPosEdMainView::OnInitialUpdate() 
{
  static_cast<RoadEditorView *>(_roadVectorEditor.GetRef())->AttachModel(&GetDocument()->m_PoseidonMap.m_roadControl);
	CFMntScrollerView::OnInitialUpdate();
  EnableGrid(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowGrid);
	// nastaven� parametr� editace
	m_bEditSupport = TRUE;
	m_pEditParams  = &m_EditObjsParams; 	// parmetry lokalne pro view

  CChildFrame* m_pParent = ((CChildFrame*)GetParentFrame());
  m_pParent->ShowRulers(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowGrid);

  OnScrollScaleChanged();
}

void CPosEdMainView::OnScrollScaleChanged() 
{
  if (m_pPoseidonMap == NULL)
    return;

  stRULER_INFO rulInfo;
  rulInfo.DocSize = m_pPoseidonMap->GetRealSize()[0];
  rulInfo.fZoomFactor = m_szTotalDev.cx * 1.0f / rulInfo.DocSize;
  CPoint pt = GetScrollPosition();
  REALPOS real = m_pPoseidonMap->FromViewToRealPos( pt);
  rulInfo.ScrollPos  = real.x;
  CPoint mousePt;
  GetCursorPos(&mousePt);
  REALPOS mouseReal = m_pPoseidonMap->FromViewToRealPos( WindowToScrollerPosition(mousePt));

  rulInfo.Pos  = mouseReal.x;
  rulInfo.uMessage = RW_SCROLL;
  rulInfo.bInverted = FALSE;

  CChildFrame* m_pParent = ((CChildFrame*)GetParentFrame());
  m_pParent->UpdateRulersInfo(rulInfo, RT_HORIZONTAL);

  rulInfo.DocSize = m_pPoseidonMap->GetRealSize()[1];
  rulInfo.fZoomFactor = m_szTotalDev.cy * 1.0f / rulInfo.DocSize;  
  rulInfo.ScrollPos  = real.z;
  rulInfo.Pos  = mouseReal.z;
  rulInfo.uMessage = RW_SCROLL;
  rulInfo.bInverted = TRUE;
 
  m_pParent->UpdateRulersInfo(rulInfo, RT_VERTICAL);
};

void CPosEdMainView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	if ((lHint == 0)||(lHint > uvfFirstUpdate))
		CFMntScrollerView::OnUpdate(pSender, lHint, pHint);
	else
	{
		ASSERT(pHint != NULL);
		//ASSERT_KINDOF(CUpdatePar, pHint);
		switch(lHint)
		{
		case CUpdatePar::uvfCloseView:
			if (((CUpdatePar*)pHint)->m_pView != this) 
				break;
		case CUpdatePar::uvfCloseAll:		// zav�en� okna
			{// zav�en� okna
			CFrameWnd* pFrame = GetParentFrame();
			GetDocument()->PreCloseFrame(pFrame);
			pFrame->DestroyWindow();
			break;}
    case CUpdatePar::uvfResetView:
      {
        // ResetView document can be reloaded
        
        DoUnselectObject();
        m_SelBGImageID.Empty();        
        break;
      }
    case CUpdatePar::uvfSelectionObjects:
      {
        const CPosGroupObject& objs = GetDocument()->GetSelectedObjects();
        if (objs.GetGroupSize() == 0)
        {
          DoUnselectObject();
          m_EditObjsParams.ClearSelObject();
          break;
        }

        // ru��m p�vodn�
        DoUnselectObject();

        // nov� selekce
        m_EditObjsParams.SetSelObject(&objs);
        CFMntScrollUpdate cUpdate(uvfEOSelObject,this);
        UpdateAllViews(&cUpdate);
        
        SendSelectionToBuldozer();

        break;
      }
		case CUpdatePar::uvfChngOpts:	// zm�na options
			break;
		case CUpdatePar::uvfChngConfig:	// zm�na konfigurace
			Invalidate(TRUE);
			break;
		case CUpdatePar::uvfChngCursor:	// zm�na kurzoru
			//Invalidate(TRUE);
			// p�vodn� poloha
      if (GetDocument()->m_CursorObject.m_ptLastLogPos.x >= 0 && 
        GetPosEdEnvironment()->m_cfgCurrent.m_bCursorVisible)
				OnUpdateCursor(GetDocument()->m_CursorObject.m_ptLastLogPos);
		case CUpdatePar::uvfVisiCursor:	// zm�na kurzoru
			// aktu�ln� poloha
      if (GetPosEdEnvironment()->m_cfgCurrent.m_bCursorVisible)
			  OnUpdateCursor(GetDocument()->m_CursorObject.m_ptCurrentPos);
			break;
		// proveden� akce dokumentu
		case CUpdatePar::uvfRealizeAction:
			OnUpdateAction((CPosAction*)(((CUpdatePar*)pHint)->m_pAction));
			break;
/*    case CPosMessageUpdate::uvfPosMessage:
      HandleRealMessage(((CPosMessageUpdate *) pHint)->Message(), ((CPosMessageUpdate *) pHint)->Mode());
      break;*/
    case CUpdatePar::uvfBuldConnect:
      OnBuldConnect();
      break;
    case CUpdatePar::uvfBuldDisconnect:
      OnBuldDisconnect();
      break;
    case CUpdatePar::uvfEditMode:
      {
        CPosEdMainDoc * pDoc = (CPosEdMainDoc *) m_pDocument;
        //if (pDoc->m_CurrentArea.IsValidArea())
        if (m_pObjsPanel == NULL)
          break;


        int iCurrMode = m_pObjsPanel->GetTypeObjects();
        int iPreviousMode = m_pObjsPanel->GetPreviousTypeObjects();

        if (pDoc->m_CurrentArea.IsValidArea())
        {
          pDoc->AlignCurrentArea();
          if (iCurrMode == CPosObjectsPanel::grTypeLand && 
            iPreviousMode != CPosObjectsPanel::grTypeLand)
          {
            CRect rHull;
            pDoc->m_CurrentArea.GetObjectHull(rHull);
            rHull.left -= min(rHull.left,VIEW_LOG_UNIT_SIZE/2);
            rHull.top -= min(rHull.top,VIEW_LOG_UNIT_SIZE/2);
            InvalidateLogRect(rHull,TRUE);           
          }
          else
          {
            if (iCurrMode != CPosObjectsPanel::grTypeLand && 
              iPreviousMode == CPosObjectsPanel::grTypeLand)            
            {
              CRect rHull;
              pDoc->m_CurrentArea.GetObjectHull(rHull);
              rHull.right += VIEW_LOG_UNIT_SIZE/2;
              rHull.bottom += VIEW_LOG_UNIT_SIZE/2;
              InvalidateLogRect(rHull,TRUE);   
            }
          }

          OnUpdateSelLandBuldozer();
        }

        CancelMouseMode();
        break;
      };
    case CUpdatePar::uvfChngTextLayer:
      {
        CPosEdMainDoc * pDoc = (CPosEdMainDoc *) m_pDocument;
        //if (pDoc->m_CurrentArea.IsValidArea())
        int iCurrMode = m_pObjsPanel->GetTypeObjects();        
        if(iCurrMode == CPosObjectsPanel::grTypeTxtr)
        {
          int textLogSize = m_pPoseidonMap->GetActiveTextureLayer()->GetTextureLogSize();
          pDoc->m_CurrentArea.AlignToGrid(CPoint(textLogSize,textLogSize),
            CPoint(0,0));
          OnUpdateSelLandBuldozer();
        }    
        Invalidate();
        break;
      };
    };
  };
	// update selekce v buldozeru
	/*if (lHint == uvfEOSelObject)
	{
		MESSAGE_VAR(SELECTION_OBJECT_CLEAR, sMsg);
		// clear selekce		
		GetDocument()->SendRealMessage(sMsg);
		// nastaven� selekce
		CPosObject* pSrc = (CPosObject*)(m_pEditParams->m_pSelObject);
		if (pSrc == NULL)
			return;
		ASSERT(m_EditObjsParams.m_pCurrObj == pSrc);
		if (pSrc->IsKindOf(RUNTIME_CLASS(CPosGroupObject)))
		{	// vlo��m objekty ze skupiny
			CPosGroupObject* pGroup = (CPosGroupObject*)pSrc;
      //ASSERT(pGroup->m_SelObjects.GetSize() > 5 || pGroup->m_SelObjects.GetSize() == 2);
			for(int i=0; i<pGroup->m_SelObjects.GetSize(); i++)
			{
				CPosObject* pObj = (CPosObject*)(pGroup->m_SelObjects[i]);
				if (pObj != NULL)
				{
					ASSERT_KINDOF(CPosObject,pObj);
					if (pObj->IsKindOf(RUNTIME_CLASS(CPosObject)))
						OnUpdateSelObjectBuldozer(pObj);
				}
			}
		} else
		{	// vlo��m vlastn� objekt
			OnUpdateSelObjectBuldozer(pSrc);
		};
	};*/
}

void CPosEdMainView::OnUpdateAction(const CPosAction* pAction)
{
	// P�ekresl�m podle akce
	switch(pAction->m_nActType)
	{
	case IDA_OBJECT_MOVE:
	case IDA_OBJECT_EDIT:
  case IDA_OBJECT_LOCK:
	case IDA_WOOD_MOVE:
	case IDA_NAMEAREA_MOVE:
	case IDA_NAMEAREA_EDIT:
	case IDA_KEYPOINT_MOVE:
	case IDA_KEYPOINT_EDIT:
	case IDA_NET_KEY_EDIT:
	case IDA_NET_KEY_MOVE:
		{
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			ASSERT_KINDOF(CPosObjectAction,pObjAction);
			ASSERT((pObjAction->m_pCurValue != NULL)&&(pObjAction->m_pOldValue != NULL));
			CRect rHull;
			pObjAction->m_pCurValue->GetObjectHull(rHull);
      rHull.InflateRect(rHull.Width() / 10, rHull.Height() / 10);
			InvalidateLogRect(rHull,TRUE);
			pObjAction->m_pOldValue->GetObjectHull(rHull);
      rHull.InflateRect(rHull.Width() / 10, rHull.Height() / 10);
			InvalidateLogRect(rHull,TRUE);
		}; break;
	case IDA_OBJECT_ADD:
	case IDA_OBJECT_DEL:  
	case IDA_WOOD_CREATE:
	case IDA_WOOD_DEL:
	case IDA_NET_KEY_CREATE:
	case IDA_NET_KEY_DELETE:
	case IDA_NAMEAREA_CREATE:
	case IDA_NAMEAREA_DELETE:
	case IDA_KEYPOINT_CREATE:
	case IDA_KEYPOINT_DELETE:
      {
        CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
        ASSERT_KINDOF(CPosObjectAction,pObjAction);
        ASSERT((pObjAction->m_pCurValue != NULL)||(pObjAction->m_pOldValue != NULL));
        //CPosObject* pObj = (pObjAction->m_pCurValue != NULL)?(pObjAction->m_pCurValue):(pObjAction->m_pOldValue);
        CRect rHull;
        if (pObjAction->m_pCurValue != NULL)
        {      
          pObjAction->m_pCurValue->GetObjectHull(rHull);
          rHull.InflateRect(rHull.Width() / 10, rHull.Height() / 10);
          InvalidateLogRect(rHull,TRUE);
        }; 
        if (pObjAction->m_pOldValue != NULL)
        {      
          pObjAction->m_pOldValue->GetObjectHull(rHull);
          rHull.InflateRect(rHull.Width() / 10, rHull.Height() / 10);
          InvalidateLogRect(rHull,TRUE);
        }; 
        break;
      }
  case IDA_BG_IMAGE_CREATE:
  case IDA_BG_IMAGE_DELETE:
  case IDA_BG_IMAGE_EDIT:
		{
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			ASSERT_KINDOF(CPosObjectAction,pObjAction);
			ASSERT((pObjAction->m_pCurValue != NULL)||(pObjAction->m_pOldValue != NULL));
			//CPosObject* pObj = (pObjAction->m_pCurValue != NULL)?(pObjAction->m_pCurValue):(pObjAction->m_pOldValue);
			CRect rHull;
      if (pObjAction->m_pCurValue != NULL)
      {   
        ((CPosBackgroundImageObject *) pObjAction->m_pCurValue)->CalcObjectLogPosition();
			  pObjAction->m_pCurValue->GetObjectHull(rHull);
			  InvalidateLogRect(rHull,TRUE);
      }; 
      if (pObjAction->m_pOldValue != NULL)
      {      
        ((CPosBackgroundImageObject *) pObjAction->m_pOldValue)->CalcObjectLogPosition();
        pObjAction->m_pOldValue->GetObjectHull(rHull);
        InvalidateLogRect(rHull,TRUE);
        ChangeSelBGImage(((CPosBackgroundImageObject *) pObjAction->m_pOldValue)->m_sAreaName);
      }
      else
        ChangeSelBGImage(CString());
      break;
    }
	case IDA_AREA_ADD:
  case IDA_AREA_REMOVE:
	case IDA_AREA_DEL: 
		{
			CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
			ASSERT_KINDOF(CPosObjectAction,pObjAction);			
						
			if (GetDocument()->m_CurrentArea.IsValidArea())
			{
        CRect rHull;
				GetDocument()->m_CurrentArea.GetObjectHull(rHull);
        rHull.InflateRect(VIEW_LOG_UNIT_SIZE/2,VIEW_LOG_UNIT_SIZE/2);
				InvalidateLogRect(rHull,TRUE);
			};
      if (!pObjAction->m_bFromBuldozer)
      {
        OnUpdateSelLandBuldozer();
      }
		}; return;	// n�vrat - selekce se nem�n�
	case IDA_TEXTURE_SET:
		{
			ASSERT_KINDOF(CPosTextureAction,pAction);
			CPosTextureAction* pTxtrAction = (CPosTextureAction*)pAction;
			CRect	rHull;
			// lev� doln�
			float   nUnitSz = ((float)GetDocument()->m_nRealSizeUnit);
			REALPOS rPos	= GetDocument()->m_PoseidonMap.FromTextureUnitToRealPos(pTxtrAction->m_Position);
			POINT	ptLog	= GetDocument()->m_PoseidonMap.FromRealToViewPos(rPos);
			int		nUpSz	= VIEW_LOG_UNIT_SIZE/2;
      if (GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowScndTxtr)
					nUpSz  += m_pPoseidonMap->GetTextureLogSize();

			rHull.left  = ptLog.x - nUpSz;
			rHull.right = ptLog.x + m_pPoseidonMap->GetTextureLogSize() + nUpSz;
			rHull.bottom= ptLog.y + nUpSz;
			rHull.top   = ptLog.y -(m_pPoseidonMap->GetTextureLogSize() + nUpSz);
			InvalidateLogRect(rHull,TRUE);
		}; return;	// n�vrat - selekce se nem�n�
	case IDA_TEXTURE_AREA:
  case IDA_TEXTURE_AREA2:
  case IDA_TEXTURE_CHNG_AREA:
  case IDA_TEXTURE_LOCK_AREA: 
		{
			ASSERT_KINDOF(CPosTxtrAreaActionBase,pAction);
			CPosTxtrAreaActionBase* pTxtrAction = (CPosTxtrAreaActionBase*)pAction;
			// obal zm�n
			CRect rHull; rHull = pTxtrAction->m_Position.GetPositionHull();
			// zv�t��m rect
			int		nUpSz	= VIEW_LOG_UNIT_SIZE/2;
			if (GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowScndTxtr)
					nUpSz  += m_pPoseidonMap->GetTextureLogSize();
			rHull.left  = rHull.left   - nUpSz;
			rHull.right = rHull.right  + nUpSz;
			rHull.top   = rHull.top	   - nUpSz;
			rHull.bottom= rHull.bottom + nUpSz;
			InvalidateLogRect(rHull,TRUE);
		}; return;	// n�vrat - selekce se nem�n�  
  case IDA_HEIGHT_LOCK_CHANGE:
    {
      ASSERT_KINDOF(CPosTxtrAreaActionBase,pAction);
      CPosTxtrAreaActionBase* action = (CPosTxtrAreaActionBase*)pAction;
      // obal zm�n
      CRect rHull; rHull = action->m_Position.GetPositionHull();
      // zv�t��m rect
      int		nUpSz	= VIEW_LOG_UNIT_SIZE;      
      rHull.left  = rHull.left   - nUpSz;
      rHull.right = rHull.right  + nUpSz;
      rHull.top   = rHull.top	   - nUpSz;
      rHull.bottom= rHull.bottom + nUpSz;
      InvalidateLogRect(rHull,TRUE);
    }; return;	// n�vrat - selekce se nem�n�  
	case IDA_HEIGHT_EROZE:
	case IDA_HEIGHT_CHANGE:

		{	// zm�na v��ky v bloku
			ASSERT_KINDOF(CPosHeightAction,pAction);
			CPosHeightAction* pHeightAction = (CPosHeightAction*)pAction;
			// obal zm�n
			CRect rHull; rHull = pHeightAction->m_Position.GetPositionHull();
			// zv�t��m rect
			int		nUpSz	= VIEW_LOG_UNIT_SIZE;
			rHull.left  = rHull.left   - nUpSz;
			rHull.right = rHull.right  + nUpSz;
			rHull.top   = rHull.top	   - nUpSz;
			rHull.bottom= rHull.bottom + nUpSz;
			InvalidateLogRect(rHull,TRUE);
		}; return;	// n�vrat - selekce se nem�n�
	case IDA_GROUPOBJ_DELETE:	
	case IDA_GROUPOBJ_PASTE:
  case IDA_NET_KEY_CREATE_GROUP:
		{	// update skupiny objekt� po proveden� akci 
			// update objekt� je proveden d�l��mi instrukcemi
			CObArray	ObjToSelect;
			GetSelObjectInAction(ObjToSelect,pAction);
			// selekce objekt�
			DoSelectPosObjects(ObjToSelect,smNew,TRUE);
		}; return;	// n�vrat - vlastn� selekce
  case IDA_GROUPOBJ_MOVE:
    {	// update skupiny objekt� po proveden� akci 
      // update objekt� je proveden d�l��mi instrukcemi
      CObArray	ObjToSelect;
      GetSelObjectInAction(ObjToSelect,pAction);
      // selekce objekt�
      DoSelectPosObjects(ObjToSelect,smNew,FALSE);
      if (!pAction->m_bFromBuldozer)
        SendMovedObjectsToBuldozer(ObjToSelect);
    }; return;	// n�vrat - vlastn� selekce
  case IDA_TEXTURE_LAYER_CREATE:
  case IDA_TEXTURE_LAYER_REMOVE:
  case IDA_TEXTURE_LAYER_EDIT:
  case IDA_GROUPOBJ_REPLACE:
  case IDA_DERIVED_ACTION:
    return;
	default:
		// nezn�m� akce - p�ekresl�m v�e
		ASSERT(FALSE);
		Invalidate(TRUE);
	};

	// Selekce/deselekce objekt�
	if (pAction->IsKindOf(RUNTIME_CLASS(CPosObjectAction)))
	{	// akce s objektem
		CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
		ASSERT((pObjAction->m_pCurValue != NULL)||(pObjAction->m_pOldValue != NULL));
		// mohu m�nit selekci
		if (pObjAction->m_nSelectObjType != CPosObjectAction::ida_SelectExclu)
		{			
      return;      
		}

		BOOL bExlSel = (pObjAction->m_nSelectObjType == CPosObjectAction::ida_SelectExclu);
		// deselekce objektu
		if (pObjAction->m_pOldValue != NULL)
		{	
			// z�kladn� objekt ?
			if (pObjAction->m_pOldValue->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
			{
        DoSelectPosObject(m_pPoseidonMap->GetPoseidonObject(pObjAction->m_pOldValue->GetID()),bExlSel ? smNew : smAdd, TRUE, TRUE);
				return;
			};
			if (pObjAction->m_pOldValue->IsKindOf(RUNTIME_CLASS(CPosWoodObject)))
			{
				DoSelectPosObject(m_pPoseidonMap->GetPoseidonWood(pObjAction->m_pOldValue->GetID()),bExlSel ? smNew : smAdd, TRUE, TRUE);
				return;
			};
			if (pObjAction->m_pOldValue->IsKindOf(RUNTIME_CLASS(CPosNetObjectBase)))
			{
				DoSelectPosObject(m_pPoseidonMap->GetPoseidonNet(pObjAction->m_pOldValue->GetID()),bExlSel ? smNew : smAdd,TRUE, TRUE);
				return;
			};
			if (pObjAction->m_pOldValue->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)))
			{
				DoSelectPosObject(m_pPoseidonMap->GetPoseidonNameArea(pObjAction->m_pOldValue->GetID()),bExlSel ? smNew : smAdd, TRUE, TRUE);
				return;
			};
			if (pObjAction->m_pOldValue->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))
			{
				DoSelectPosObject(m_pPoseidonMap->GetPoseidonKeyPt(((CPosKeyPointObject*)(pObjAction->m_pOldValue))->GetID()),bExlSel ? smNew : smAdd, TRUE, TRUE);
				return;
			};
			DoUnselectPosObject();
			if ((pObjAction->m_pCurValue != NULL)&&
				(m_pEditInfo != NULL)&&(m_pEditInfo->m_pSelObject != NULL))
				m_pEditInfo->UpdateObjectChanges(this, FALSE, TRUE);
		} else
			DoUnselectPosObject();
	}
};

void CPosEdMainView::OnUpdateCursor(const POINT& ptLog)
{
	CPoint ptDev = FromScrollToDevicePosition(ptLog);
		   ptDev-= GetDeviceScrollPosition();
	// rect kolem kurozoru
	CPosEdCfg*	pCfg = &(GetPosEdEnvironment()->m_cfgCurrent);
	int	nWidth2 = (pCfg->m_nCursorDevSz+1)/2;
	int nPen2	= (pCfg->m_nCursorDevWd+3)/2;
	CRect rPos(ptDev.x-nWidth2,ptDev.y-nWidth2,
			   ptDev.x+nWidth2,ptDev.y+nWidth2);
	// p�id�m ���ku pera
	rPos.InflateRect(nPen2,nPen2);
	//CRect client;GetClientRect(client);
	//if (rPos.IntersectRect(rPos,client))
	::InvalidateRect(m_hWnd,rPos,TRUE);
};

void CPosEdMainView::OnUpdateSelObjectBuldozer(CPosObject* pObj)
{
	if (pObj == NULL)
		return;
	if (pObj->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
		OnUpdateSelEdObjectBuldozer((CPosEdObject*)pObj);
	else
	{	// nezn�my typ objektu
		// ASSERT(FALSE);
	};
};

void CPosEdMainView::OnUpdateSelEdObjectBuldozer(CPosEdObject* pObj)
{
        SObjectPosMessData data;
        data.bState=TRUE;
        pObj->SetObjectDataToMessage(data);
        if (GetDocument()->RealViewer()) GetDocument()->RealViewer()->SelectionObjectAdd(data);
};

/*

void CPosEdMainView::HandleRealMessage(const SPosMessage& msg, int nMode)
{
  switch (msg._iMsgType)
  {
  case SELECTION_OBJECT_ADD:
    {    
      MESSAGE_DATA_CONST_VAR(SELECTION_OBJECT_ADD, &msg, msgData);      
      DoSelectPosObject(m_pPoseidonMap->GetPoseidonObject(msgData.nID),msgData.bState ? smAdd : smDel);
      break;
    }
  case SELECTION_OBJECT_CLEAR:
    DoUnselectPosObject();   
    break; 
  case BLOCK_SELECTION_OBJECT:
    {    
      MESSAGE_DATA_CONST_VAR(BLOCK_SELECTION_OBJECT, &msg, msgData);
      
      for(int i = 0; i < msgData.Size(); i++)
      {     
        DoSelectPosObject(m_pPoseidonMap->GetPoseidonObject(msgData[i].nID),smAdd, i ==  msgData.Size() - 1);
      }
      break;
    }

  case CURSOR_POSITION_SET:
    {	// zm�na polohy curzoru
      // zaznamen�m polohu z buldozeru
      MESSAGE_DATA_CONST_VAR(CURSOR_POSITION_SET, &msg, msgData)
      CPosEdMainDoc * pDocument = (CPosEdMainDoc *) m_pDocument;
      if (pDocument == NULL)
        break;

      CPosEdCursor & cursor = pDocument->m_CursorObject;
      cursor.m_rpBuldozrPos.x = msgData.Position[0][3];
      cursor.m_rpBuldozrPos.z = msgData.Position[2][3];
      // update polohy ve view - je-li aktu�ln� nastaven
      //if (GetPosEdEnvironment()->m_optPosEd.m_nBldCursorUpdate == CPosEdOptions::upBlCurFull)
        cursor.SetCursorRealPosition(cursor.m_rpBuldozrPos,TRUE);

      KeepVisibleLog(cursor.m_ptCurrentPos);
    }; break;
  case SELECTION_LAND_CLEAR:
    {
      if (GetDocument()->m_CurrentArea.IsValidArea())
        Action_DelPoseidonCurrArea((CPosEdMainDoc*)m_pDocument,NULL, TRUE);
      break;
    }
  case SELECTION_LAND_ADD:
    {
      MESSAGE_DATA_CONST_VAR(SELECTION_LAND_ADD, &msg, msgData);

      REALPOS leftTop;
      leftTop.x = msgData.xs;
      leftTop.z = msgData.zs;

      REALPOS rightBottom;
      rightBottom.x = msgData.xe;
      rightBottom.z = msgData.ze;

      POINT leftTopView = m_pPoseidonMap->FromRealToViewPos(leftTop);
      POINT rightBottomView = m_pPoseidonMap->FromRealToViewPos(rightBottom);
      
      //Align to step
      int nMaxX = ((CPosEdMainDoc *) m_pDocument)->m_PoseidonMap.GetSize().x;
      int nMaxY = ((CPosEdMainDoc *) m_pDocument)->m_PoseidonMap.GetSize().z;
      leftTopView.x = min(max(0,(leftTopView.x / VIEW_LOG_UNIT_SIZE)), nMaxX) * VIEW_LOG_UNIT_SIZE;
      leftTopView.y = min(max(0,(leftTopView.y / VIEW_LOG_UNIT_SIZE)), nMaxY) * VIEW_LOG_UNIT_SIZE;
      rightBottomView.x = min(max(0,(rightBottomView.x / VIEW_LOG_UNIT_SIZE)), nMaxX) * VIEW_LOG_UNIT_SIZE;
      rightBottomView.y = min(max(0,(rightBottomView.y / VIEW_LOG_UNIT_SIZE)), nMaxY) * VIEW_LOG_UNIT_SIZE;      

      CRect rect(leftTopView.x, rightBottomView.y, rightBottomView.x, leftTopView.y);
      if (((CPosEdMainDoc *) m_pDocument)->m_CurrentArea.GetSelAreaType() == CPosSelAreaObject::satVertex)
      {
        rect += CPoint(VIEW_LOG_UNIT_SIZE/2, VIEW_LOG_UNIT_SIZE/2);       
      }

      if (rect.Width() != 0 && rect.Height() != 0)
        Action_AddPoseidonCurrArea(rect,FALSE,FALSE,(CPosEdMainDoc*)m_pDocument,NULL, TRUE);

      break;
    }
  case BLOCK_SELECTION_LAND:
    {
      MESSAGE_DATA_CONST_VAR(BLOCK_SELECTION_LAND, &msg, msgData);
      CRectArray area;
      for(int i = 0; i < msgData.Size(); i++)
      {

        const SLandSelPosMessData& data = msgData[i];
        REALPOS leftTop;
        leftTop.x = data.xs;
        leftTop.z = data.zs;

        REALPOS rightBottom;
        rightBottom.x = data.xe;
        rightBottom.z = data.ze;

        POINT leftTopView = m_pPoseidonMap->FromRealToViewPos(leftTop);
        POINT rightBottomView = m_pPoseidonMap->FromRealToViewPos(rightBottom);

        //Align to step
        int nMaxX = ((CPosEdMainDoc *) m_pDocument)->m_PoseidonMap.GetSize().x;
        int nMaxY = ((CPosEdMainDoc *) m_pDocument)->m_PoseidonMap.GetSize().z;
        leftTopView.x = min(max(0,(leftTopView.x / VIEW_LOG_UNIT_SIZE)), nMaxX) * VIEW_LOG_UNIT_SIZE;
        leftTopView.y = min(max(0,(leftTopView.y / VIEW_LOG_UNIT_SIZE)), nMaxY) * VIEW_LOG_UNIT_SIZE;
        rightBottomView.x = min(max(0,(rightBottomView.x / VIEW_LOG_UNIT_SIZE)), nMaxX) * VIEW_LOG_UNIT_SIZE;
        rightBottomView.y = min(max(0,(rightBottomView.y / VIEW_LOG_UNIT_SIZE)), nMaxY) * VIEW_LOG_UNIT_SIZE;      

        CRect rect(leftTopView.x, rightBottomView.y, rightBottomView.x, leftTopView.y);
        if (((CPosEdMainDoc *) m_pDocument)->m_CurrentArea.GetSelAreaType() == CPosSelAreaObject::satVertex)
        {
          rect += CPoint(VIEW_LOG_UNIT_SIZE/2, VIEW_LOG_UNIT_SIZE/2);       
        }

        if (rect.Width() != 0 && rect.Height() != 0)
          area.Add(rect);          
      }
      Action_AddPoseidonCurrArea(area,TRUE,FALSE,(CPosEdMainDoc*)m_pDocument,NULL, TRUE);

      break;
    }



  }
}

*/
/////////////////////////////////////////////////////////////////////////////
// nastaven� m��ky

void CPosEdMainView::EnableGrid(BOOL bEnable)
{
	CSize sz(VIEW_LOG_UNIT_SIZE,VIEW_LOG_UNIT_SIZE);

	double nPom = GetCurrentViewScale();
	if (nPom >0.)
	// v�dy zarovn�v�m na m��ku
	SetGrid( bEnable, TRUE, sz);
};

void CPosEdMainView::DrawGrid(CDC* pDC)
{
	if (!m_bShowGrid) return;
	// paint grid to clip rect or full rect
	CRect rClip; pDC->GetClipBox(&rClip);
	rClip.InflateRect(m_currGridStep);
	if (!rClip.IntersectRect(rClip,m_rAreaLog)) return;
	rClip = AlignRectToGrid(rClip,rClip);
	// vykreslen� m��ky do rClip
	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	// nastaven� xor
	int nOldROP = pDC->SetROP2(pEnvir->m_cfgCurrent.m_bGridXor?R2_XORPEN:R2_COPYPEN);
	//LogF("Draw: m_currGridStep: %ld, %ld", m_currGridStep.cx, m_currGridStep.cy);
  int texGridSize = m_pPoseidonMap->GetActiveTextureLayer()->GetTextureLogSize();
  COLORREF gridColor = pEnvir->m_cfgCurrent.m_nGridColor;   
  COLORREF lowImportance = (~gridColor) & (0x00ffffff);
	if(pEnvir->m_cfgCurrent.m_nGridStyle == CPosEdCfg::gridStPoint)
	{	// body    
		for(int x = rClip.left; x <= rClip.right; x += m_currGridStep.cx)
		{
			for(int y = rClip.top; y <= rClip.bottom; y += m_currGridStep.cy)
			{
        if (x % texGridSize == 0 && y  % texGridSize == 0)
				  pDC->SetPixel(x,y,pEnvir->m_cfgCurrent.m_nGridColor);
        else
          pDC->SetPixel(x,y,lowImportance);

			}
		}
	} else
	{	// ��ra
		CPen	penGrid((pEnvir->m_cfgCurrent.m_nGridStyle == CPosEdCfg::gridStDottLn)?PS_DOT:PS_SOLID,0,gridColor);
		CPen*	pOldPen = pDC->SelectObject(&penGrid);
		pDC->SetBkMode(TRANSPARENT);
		// vykreslen� m��ky    
		for(int x = rClip.left; x <= rClip.right; x += m_currGridStep.cx)
		{
      if (x % texGridSize == 0 )
      {      
			  pDC->MoveTo(x,rClip.top);
			  pDC->LineTo(x,rClip.bottom);
      }
    }
		for(int y = rClip.top; y <= rClip.bottom; y += m_currGridStep.cy)
		{
      if (y % texGridSize == 0 )
      {    
        pDC->MoveTo(rClip.left,y);
        pDC->LineTo(rClip.right,y);
      }
		}
    CPen	penGridLow((pEnvir->m_cfgCurrent.m_nGridStyle == CPosEdCfg::gridStDottLn)?PS_DOT:PS_SOLID,0,lowImportance);
    pDC->SelectObject(&penGridLow);
    for(int x = rClip.left; x <= rClip.right; x += m_currGridStep.cx)
    {
      if (x % texGridSize != 0 )
      {      
        pDC->MoveTo(x,rClip.top);
        pDC->LineTo(x,rClip.bottom);
      }
    }
    for(int y = rClip.top; y <= rClip.bottom; y += m_currGridStep.cy)
    {
      if (y % texGridSize != 0 )
      {    
        pDC->MoveTo(rClip.left,y);
        pDC->LineTo(rClip.right,y);
      }
    }
		pDC->SelectObject(&pOldPen);
	};
	pDC->SetROP2(nOldROP);
};


/////////////////////////////////////////////////////////////////////////////
// nastaven� styl� zobrazen�

void CPosEdMainView::OnChangeViewStyle()
{
	EnableGrid(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowGrid);
	CChildFrame* pFrame= (CChildFrame *) GetParentFrame();
	pFrame->ShowRulers(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowRuler);

	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.SaveParams();
	Invalidate(TRUE);
}

void CPosEdMainView::OnBaseStyle0()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_nBaseStyle = 0;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateBaseStyle0(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_nBaseStyle == 0); };
void CPosEdMainView::OnBaseStyle1()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_nBaseStyle = 1;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateBaseStyle1(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_nBaseStyle == 1); };
void CPosEdMainView::OnBaseStyle2()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_nBaseStyle = 2;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateBaseStyle2(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_nBaseStyle == 2); };
void CPosEdMainView::OnBaseStyle3()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_nBaseStyle = 3;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateBaseStyle3(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_nBaseStyle == 3); };
void CPosEdMainView::OnBaseStyle4()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_nBaseStyle = 4;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateBaseStyle4(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_nBaseStyle == 4); };
void CPosEdMainView::OnBaseStyle5()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_nBaseStyle = 5;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateBaseStyle5(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_nBaseStyle == 5); };
void CPosEdMainView::OnBaseStyle6()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_nBaseStyle = CViewStyle::tvsHgFromTo;
OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateBaseStyle6(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_nBaseStyle == CViewStyle::tvsHgFromTo); };


void CPosEdMainView::OnNaturObjcs()
{	
	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bNatureObjcs = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bNatureObjcs;
	OnChangeViewStyle(); 
}

void CPosEdMainView::OnUpdateNaturObjcs(CCmdUI* pCmdUI)
{	
	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bNatureObjcs); 
}

void CPosEdMainView::OnPeopleObjcs()
{	
	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bPeopleObjcs = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bPeopleObjcs;
	OnChangeViewStyle(); 
}

void CPosEdMainView::OnUpdatePeopleObjcs(CCmdUI* pCmdUI)
{	
	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bPeopleObjcs); 
}

void CPosEdMainView::OnNewRoadsObjcs()
{	
	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bNewRoadsObjcs = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bNewRoadsObjcs;
	OnChangeViewStyle(); 
}

void CPosEdMainView::OnUpdateNewRoadsObjcs(CCmdUI* pCmdUI)
{	
	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bNewRoadsObjcs); 
}


void CPosEdMainView::OnUnknowObjcs()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bUnknowObjcs = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bUnknowObjcs;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateUnknowObjcs(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bUnknowObjcs); };

void CPosEdMainView::OnShowWoods()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowWoods= !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowWoods;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateShowWoods(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowWoods); };
void CPosEdMainView::OnShowNets()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowNets = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowNets;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateShowNets(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowNets); };

void CPosEdMainView::OnAreasBorder()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bAreasBorder = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bAreasBorder;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateAreasBorder(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bAreasBorder); };
void CPosEdMainView::OnAreasEntire()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bAreasEntire = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bAreasEntire;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateAreasEntire(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bAreasEntire); };

void CPosEdMainView::OnKeyPtBorder()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bKeyPtBorder = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bKeyPtBorder;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateKeyPtBorder(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bKeyPtBorder); };
void CPosEdMainView::OnKeyPtEntire()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bKeyPtEntire = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bKeyPtEntire;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateKeyPtEntire(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bKeyPtEntire); };

void CPosEdMainView::OnEnableCntLn()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowCountLn = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowCountLn;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateEnableCntLn(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowCountLn); };

void CPosEdMainView::OnEnableGrid()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowGrid = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowGrid;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateEnableGrid(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowGrid); };

void CPosEdMainView::OnEnableScndTxtr()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowScndTxtr = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowScndTxtr;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateEnableScndTxtr(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowScndTxtr); };

void CPosEdMainView::OnEnableShowSea()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowSea = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowSea;
	OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateEnableShowSea(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowSea); };

void CPosEdMainView::OnEnableShowRivers()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bWaterLayer = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bWaterLayer;
OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateEnableShowRivers(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bWaterLayer); };

void CPosEdMainView::OnEnableShowBGImages()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowBGImage = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowBGImage;
OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateEnableShowBGImages(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowBGImage); };

void CPosEdMainView::OnEnableShowShadowing()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_Shadowing = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_Shadowing;
OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateEnableShowShadowing(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_Shadowing); };

void CPosEdMainView::OnEnableShowLock()
{	GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_ShowLock = !GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_ShowLock;
OnChangeViewStyle(); };
void CPosEdMainView::OnUpdateEnableShowLock(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_ShowLock); };

/////////////////////////////////////////////////////////////////////////////
// Drawing functions

void CPosEdMainView::OnInitDrawParams(CDrawPar* pParams)
{
	// p��prava parametr� pro vykreslov�n�
	pParams->m_pDocument = m_pDocument;
	// parametry pro vykreslov�n� objekt�
	pParams->CreateTempObjects();
	pParams->m_VwStyle.CopyFrom(&GetPosEdEnvironment()->m_optPosEd.m_ViewStyle);
}

void SetColorScale(int nMinInx,COLORREF nMinColor,int nMaxInx,COLORREF nMaxColor,SScaleItem* pScale)
{
	if ((nMinInx < 0)||(nMaxInx < 0)||(nMinInx > nMaxInx))
		return;
	if (nMinInx == nMaxInx)
	{
		pScale[nMinInx].m_cColor = nMinColor;
		return;
	};
	// v�ce barev
	int nCount = nMaxInx-nMinInx;

	int R0 = GetRValue(nMinColor);
	int R1 = GetRValue(nMaxColor);
	int G0 = GetGValue(nMinColor);
	int G1 = GetGValue(nMaxColor);
	int B0 = GetBValue(nMinColor);
	int B1 = GetBValue(nMaxColor);

	double nStepR = (R1-R0)/(double)nCount;
	double nStepG = (G1-G0)/(double)nCount;
	double nStepB = (B1-B0)/(double)nCount;

	pScale[nMinInx].m_cColor = nMinColor;
	for(int i=1;i<nCount;i++)
	{
		int nR = (int)Round(R0 + (i-1)*nStepR,0);
		if (nR > 255) nR = 255;
		if (nR < 0)   nR = 0;
		int nG = (int)Round(G0 + (i-1)*nStepG,0);
		if (nG > 255) nG = 255;
		if (nG < 0)   nG = 0;
		int nB = (int)Round(B0 + (i-1)*nStepB,0);
		if (nB > 255) nB = 255;
		if (nB < 0)   nB = 0;
		pScale[nMinInx+i].m_cColor = RGB(nR,nG,nB);
	};
	pScale[nMaxInx].m_cColor = nMaxColor;
}

void CPosEdMainView::OnInitDrawParamsForBk(CDrawPar* pParams)
{
	// p��prava parametr� pro vykreslov�n�
	pParams->m_pDocument = m_pDocument;
	// parametry pro vykreslov�n� pozad�
	pParams->CreateTempBackground();

  int nBaseStyle = GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_nBaseStyle;
	// krok vrstevnic
  if (nBaseStyle == CViewStyle::tvsHgFromTo)
    pParams->m_nStpCountLn = pParams->m_pCfg->m_HgStep;
  else
	  pParams->m_nStpCountLn = GetCountLnStep();

  // get view params but not all of them are really needed.
  pParams->m_VwStyle.CopyFrom(&GetPosEdEnvironment()->m_optPosEd.m_ViewStyle);
	// barevn� �k�la podle kroku
	if (nBaseStyle == CViewStyle::tvsHgColor)
	{
		ASSERT(pParams->m_cColorHg[ZERO_SCALE_COLOR_HEIGHT].m_nHeightTo == SEA_LAND_HEIGHT);
		// nastaven� z�porn� mapy
		int   nIndx   = 1;
		//FLYfloat nHeight = MIN_SCALE_COLOR_HEIGHT;
    float nHeight = m_pPoseidonMap->GetMinHeight();
		while(nHeight < SEA_LAND_HEIGHT)
		{
			// najdu nejbli��� vy���
			for(int i=0; i<MAX_SCALE_COLOR_ITEMS; i++)
			{
				if (pParams->m_pCfg->m_cColorHg[i].m_nHeightTo >= nHeight)
				{
					pParams->m_cColorHg[nIndx].m_nHeightTo = pParams->m_pCfg->m_cColorHg[i].m_nHeightTo;
					pParams->m_cColorHg[nIndx].m_cColor	   = pParams->m_pCfg->m_cColorHg[i].m_cColor;
					break;
				}
			}
			// dal�� v��ka
			nHeight += (float)pParams->m_nStpCountLn;
			nIndx++;
		}
		// hladina mo�e
		pParams->m_cColorHg[nIndx].m_nHeightTo = pParams->m_pCfg->m_cColorHg[ZERO_SCALE_COLOR_HEIGHT].m_nHeightTo;
		pParams->m_cColorHg[nIndx].m_cColor	   = pParams->m_pCfg->m_cColorHg[ZERO_SCALE_COLOR_HEIGHT].m_cColor;
		nIndx++;
		// kladn� hodnoty
		nHeight = (float)pParams->m_nStpCountLn;
		//FLYwhile((nHeight < MAX_SCALE_COLOR_HEIGHT)&&((nIndx+1)<MAX_SCALE_COLOR_ITEMS))
    LANDHEIGHT maxHeight =(LANDHEIGHT) ( m_pPoseidonMap->GetMaxHeight() + pParams->m_nStpCountLn);
    int i = 1;
    while((nHeight < maxHeight)&&((nIndx+1)<MAX_SCALE_COLOR_ITEMS))
		{
			// najdu nejbli��� vy���
			for(; i<MAX_SCALE_COLOR_ITEMS; i++)
			{
				if (pParams->m_pCfg->m_cColorHg[i].m_nHeightTo >= nHeight)
				{
					pParams->m_cColorHg[nIndx].m_nHeightTo = pParams->m_pCfg->m_cColorHg[i].m_nHeightTo;
					pParams->m_cColorHg[nIndx].m_cColor	   = pParams->m_pCfg->m_cColorHg[i].m_cColor;
					break;
				}
			}
			// dal�� v��ka
			nHeight += (float)pParams->m_nStpCountLn;
			nIndx++;
		}
		pParams->m_cColorHg[nIndx].m_nHeightTo = pParams->m_pCfg->m_cColorHg[MAX_SCALE_COLOR_ITEMS-1].m_nHeightTo;
		pParams->m_cColorHg[nIndx].m_cColor	   = pParams->m_pCfg->m_cColorHg[MAX_SCALE_COLOR_ITEMS-1].m_cColor;
	}
	// monochromatick� barevn� �k�la
	if (nBaseStyle == CViewStyle::tvsHgMono)
	{
		ASSERT(pParams->m_cColorHg[ZERO_SCALE_COLOR_HEIGHT].m_nHeightTo == SEA_LAND_HEIGHT);
		// nastaven� z�porn� mapy
		int   nIndx   = 1;
		float nHeight = m_pPoseidonMap->GetMinHeight();
    int i=0;
		while(nHeight < SEA_LAND_HEIGHT)
		{
			// najdu nejbli��� vy���
			for(; i<MAX_SCALE_COLOR_ITEMS; i++)
			{
				if (pParams->m_pCfg->m_cColorHg[i].m_nHeightTo >= nHeight)
				{
					pParams->m_cColorHg[nIndx].m_nHeightTo = pParams->m_pCfg->m_cColorHg[i].m_nHeightTo;
					break;
				}
			}
			// dal�� v��ka
			nHeight += (float)pParams->m_nStpCountLn;
			nIndx++;
		}
		// hladina mo�e
		pParams->m_cColorHg[nIndx].m_nHeightTo = pParams->m_pCfg->m_cColorHg[ZERO_SCALE_COLOR_HEIGHT].m_nHeightTo;
		nIndx++;
		// kladn� hodnoty
		nHeight = (float)pParams->m_nStpCountLn;
    i=1;
		while((nHeight < m_pPoseidonMap->GetMaxHeight())&&((nIndx+1)<MAX_SCALE_COLOR_ITEMS))
		{
			// najdu nejbli��� vy���
			for(; i<MAX_SCALE_COLOR_ITEMS; i++)
			{
				if (pParams->m_pCfg->m_cColorHg[i].m_nHeightTo >= nHeight)
				{
					pParams->m_cColorHg[nIndx].m_nHeightTo = pParams->m_pCfg->m_cColorHg[i].m_nHeightTo;
					break;
				}
			}
			// dal�� v��ka
			nHeight += (float)pParams->m_nStpCountLn;
			nIndx++;
		}
		pParams->m_cColorHg[nIndx].m_nHeightTo = pParams->m_pCfg->m_cColorHg[MAX_SCALE_COLOR_ITEMS-1].m_nHeightTo;

		// nastaven� monochromaick�ch barev
		int nZeroIndx = -1;
		for(int i=0; i<nIndx; i++)
		{
			if (pParams->m_cColorHg[i].m_nHeightTo == SEA_LAND_HEIGHT)
			{
				nZeroIndx = i;
				break;
			}
		}
		// nastaven� barevn� �k�ly
		SetColorScale(0,pParams->m_pCfg->m_cMinSeaHg,nZeroIndx,pParams->m_pCfg->m_cMaxSeaHg,pParams->m_cColorHg);
		SetColorScale(nZeroIndx+1,pParams->m_pCfg->m_cMinMonoHg,nIndx,pParams->m_pCfg->m_cMaxMonoHg,pParams->m_cColorHg);
	}

  // fromto monochromatic height scale
  if (nBaseStyle == CViewStyle::tvsHgFromTo)
  {   
    float nHeight = pParams->m_pCfg->m_HgFrom;    
    for(int i=0; (i + 1) < MAX_SCALE_COLOR_ITEMS; i++)
    {
      pParams->m_cColorHg[i].m_nHeightTo = nHeight;
      // dal�� v��ka
      nHeight += (float)pParams->m_nStpCountLn;      
    }    
    pParams->m_cColorHg[MAX_SCALE_COLOR_ITEMS-1].m_nHeightTo = 1e6;
    
    // nastaven� barevn� �k�ly   
    SetColorScale(0,pParams->m_pCfg->m_cMinMonoHg,MAX_SCALE_COLOR_ITEMS-1,pParams->m_pCfg->m_cMaxMonoHg,pParams->m_cColorHg);
  }
}

void CPosEdMainView::OnDrawObjects(CDC* pDC)
{
	// parametry pro vykreslov�n�
	CDrawPar DrawParams;
	OnInitDrawParams(&DrawParams);

	CPosEdMainDoc* pDoc = (CPosEdMainDoc*)m_pDocument;
	// vykreslen� les� & objekt�	
	CRect rectLog = GetLogClientRect();
	int i;
	if (pDoc)
	{
		EnabledObjects enabled = GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.GetEnableObjects();
		AutoArray<CPosNameAreaObject*> nameAreas;
		for(i = 0; i < pDoc->m_PoseidonMap.GetPoseidonNameAreaCount(); ++i)
		{
			CPosNameAreaObject*	pArea = pDoc->m_PoseidonMap.GetPoseidonNameArea(i);

			if ((pArea == NULL) || (!pArea->IsEnabledObject(enabled))) continue;

			CRect hull = pArea->GetObjectHull();
			if (pDC->RectVisible(&hull))
			{      
				//pArea->OnDrawObject(pDC,pArea->m_LogPosition,&DrawParams);
				pArea->RecalcDevObjectPositionFromLP(pDC);
				nameAreas.Add(pArea);
			}
		}

		AutoArray<CPosKeyPointObject*> keyPoints;
		for(i = 0; i < pDoc->m_PoseidonMap.GetPoseidonKeyPtCount(); ++i)
		{
			CPosKeyPointObject* pKeyPt = pDoc->m_PoseidonMap.GetPoseidonKeyPt(i);

			if ((pKeyPt == NULL) || (!pKeyPt->IsEnabledObject(enabled))) continue;

			CRect hull = pKeyPt->GetObjectHull();
			if (pDC->RectVisible(&hull))
			{      
				//pKeyPt->OnDrawObject(pDC,pKeyPt->m_LogPosition,&DrawParams);
				pKeyPt->RecalcDevObjectPositionFromLP(pDC);
				keyPoints.Add(pKeyPt);
			}
		}

		AutoArray<CPosWoodObject*> woods;
		for(i = 0; i < pDoc->m_PoseidonMap.GetPoseidonWoodsCount(); ++i)
		{
			CPosWoodObject* pWood = pDoc->m_PoseidonMap.GetPoseidonWood(i);

			if ((pWood == NULL) || (!pWood->IsEnabledObject(enabled))) continue;

			CRect hull = pWood->GetObjectHull();
			if (pDC->RectVisible(&hull))
			{      
				//pWood->OnDrawObject(pDC,pWood->m_LogPosition,&DrawParams);
				pWood->RecalcDevObjectPositionFromLP(pDC);
				woods.Add(pWood);
			}
		}

		AutoArray<CPosNetObjectBase*> nets;
		for(i = 0; i < pDoc->m_PoseidonMap.GetPoseidonNetsCount(); ++i)
		{
			CPosNetObjectBase* pNet = pDoc->m_PoseidonMap.GetPoseidonNet(i);   

			if ((pNet == NULL) || (!pNet->IsEnabledObject(enabled))) continue;

			CRect hull = pNet->GetLogObjectPosition()->GetPositionHull();
			if (pDC->RectVisible(&hull))
			{      
				//pNet->OnDrawObject(pDC,&DrawParams);
				pNet->RecalcDevObjectPositionFromLP(pDC);
				nets.Add(pNet);
			}
		}

		AutoArray<CPosEdObject*> objects;
		for(i = 0; i < pDoc->m_PoseidonMap.GetPoseidonObjectCount(); ++i)
		{
			CPosEdObject* pObject = pDoc->m_PoseidonMap.GetPoseidonObject(i);

			if ((pObject == NULL) || (!pObject->IsEnabledObject(enabled))) continue;

			CRect hull = pObject->GetObjectHull();
			if (pDC->RectVisible(&hull))
			{      
				//pObject->OnDrawObject(pDC,pObject->m_LogPosition,&DrawParams);
				pObject->RecalcDevObjectPositionFromLP(pDC);
				objects.Add(pObject);
			}
		}

		int nSaveDC = pDC->SaveDC();
		ASSERT(nSaveDC != 0);
		pDC->SetMapMode(MM_TEXT);
		pDC->SetViewportOrg(0, 0);
		pDC->SetWindowOrg(0, 0);

		for(i = 0; i < nameAreas.Size(); ++i)
		{
			CPosNameAreaObject* pArea = nameAreas[i];		
			if (pArea->Hidden()) continue;
			pArea->OnDrawObject(pDC, &DrawParams);
			pArea->GetDevObjectPosition()->ResetPosition();		
		}

		for(i = 0; i < keyPoints.Size(); ++i)
		{
			CPosKeyPointObject* pKeyPt = keyPoints[i];			    
			if (pKeyPt->Hidden()) continue;
			pKeyPt->OnDrawObject(pDC,&DrawParams);
			pKeyPt->GetDevObjectPosition()->ResetPosition();			
		}

		for(i = 0; i < woods.Size(); ++i)
		{
			CPosWoodObject* pWood = woods[i];			    
			if (pWood->Hidden()) continue;
			pWood->OnDrawObject(pDC, &DrawParams);
			pWood->GetDevObjectPosition()->ResetPosition();			
		}

		for(i = 0; i < nets.Size(); ++i)
		{
			CPosNetObjectBase* pNet = nets[i];			    
			if (pNet->Hidden()) continue;
			pNet->OnDrawObject(pDC, &DrawParams);
			pNet->ResetDevObjectPosition();			
		}

		for(i = 0; i < objects.Size(); ++i)
		{     
			CPosEdObject* pObject = objects[i];			        
			if (pObject->Hidden()) continue;
			pObject->OnDrawObject(pDC, &DrawParams);
			pObject->GetDevObjectPosition()->ResetPosition();			
		}

		VERIFY(pDC->RestoreDC(nSaveDC));

		// spo��t�m DP pro objekty
  /*  CObArray nameAreas;
		for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonNameAreaCount();i++)
		{
			CPosNameAreaObject*	pArea = pDoc->m_PoseidonMap.GetPoseidonNameArea(i);
      
			if ((pArea)&&(pArea->IsEnabledObject(&GetPosEdEnvironment()->m_optPosEd.m_ViewStyle)) && IsIntersectRect(rectLog,pArea->m_LogPosition.GetPositionHull()))
      {      
				pArea->RecalcDevObjectPositionFromLP(pDC);
        nameAreas.Add(pArea);
      }
		};
    CObArray keyPoints;
		for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonKeyPtCount();i++)
		{
			CPosKeyPointObject*	pKeyPt = pDoc->m_PoseidonMap.GetPoseidonKeyPt(i);
     
			if ((pKeyPt)&&(pKeyPt->IsEnabledObject(&GetPosEdEnvironment()->m_optPosEd.m_ViewStyle)) && IsIntersectRect(rectLog,pKeyPt->m_LogPosition.GetPositionHull()))
      {      
				pKeyPt->RecalcDevObjectPositionFromLP(pDC);
        keyPoints.Add(pKeyPt);
      }
		};
    CObArray woods;
		for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonWoodsCount();i++)
		{
			CPosWoodObject*	pWood = pDoc->m_PoseidonMap.GetPoseidonWood(i);
      
			if ((pWood)&&(pWood->IsEnabledObject(&GetPosEdEnvironment()->m_optPosEd.m_ViewStyle)) && IsIntersectRect(rectLog,pWood->m_LogPosition.GetPositionHull()))
      {      
				pWood->RecalcDevObjectPositionFromLP(pDC);
        woods.Add(pWood);
      }
    }
    CObArray nets;
		for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonNetsCount();i++)
		{
			CPosNetObjectBase*	pNet = pDoc->m_PoseidonMap.GetPoseidonNet(i);      
			if ((pNet)&&(pNet->IsEnabledObject(&GetPosEdEnvironment()->m_optPosEd.m_ViewStyle)) && IsIntersectRect(rectLog,pNet->m_rGlobalHull))
      {      
				pNet->RecalcDevObjectPositionFromLP(pDC);
        nets.Add(pNet);
      }
    }
    CObArray objects;
		for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonObjectCount();i++)
		{
			CPosEdObject* pObject = pDoc->m_PoseidonMap.GetPoseidonObject(i);
     
			if ((pObject)&&(pObject->m_pTemplate)&&(pObject->IsEnabledObject(&GetPosEdEnvironment()->m_optPosEd.m_ViewStyle)) && IsIntersectRect(rectLog,pObject->m_LogPosition.GetPositionHull()))
      {      
        pObject->OnDrawObject(pDC,pObject->m_LogPosition,&DrawParams);
				pObject->RecalcDevObjectPositionFromLP(pDC);
        objects.Add(pObject);
      }
    };
		// vykresl�m jednotliv� objekty
		
		for(i=0; i< nameAreas.GetSize();i++)
		{
			CPosNameAreaObject*	pArea = (CPosNameAreaObject*) nameAreas[i];
      CRect rHull = pArea->m_DevPosition.GetPositionHull();
      if (pDC->RectVisible(rHull))
        pArea->OnDrawObject(pDC,pArea->m_DevPosition,&DrawParams);
      pArea->GetDevObjectPosition()->ResetPosition();			
		};
		for(i=0; i<keyPoints.GetSize();i++)
		{
			CPosKeyPointObject*	pKeyPt = (CPosKeyPointObject*	) keyPoints[i];

			CRect rHull = pKeyPt->m_DevPosition.GetPositionHull();
			if (pDC->RectVisible(rHull))
				pKeyPt->OnDrawObject(pDC,pKeyPt->m_DevPosition,&DrawParams);
			pKeyPt->GetDevObjectPosition()->ResetPosition();			
		};
		for(i=0; i<woods.GetSize();i++)
		{
			CPosWoodObject*	pWood = (CPosWoodObject*	) woods[i];
			
			CRect rHull = pWood->m_DevPosition.GetPositionHull();
			if (pDC->RectVisible(rHull))
				pWood->OnDrawObject(pDC,pWood->m_DevPosition,&DrawParams);
			pWood->GetDevObjectPosition()->ResetPosition();
			
		};
		for(i=0; i<nets.GetSize();i++)
		{
			CPosNetObjectBase*	pNet =  (CPosNetObjectBase*	) nets[i];			
			if (pDC->RectVisible(pNet->m_rGlobalHullDP))
				pNet->OnDrawObject(pDC,&DrawParams);
			pNet->ResetDevObjectPosition();			
		};
		for(i=0; i<objects.GetSize();i++)
		{
			CPosEdObject* pObject = (CPosEdObject*	) objects[i];
			if (pObject->m_pTemplate)
			{
				CRect rHull = pObject->m_DevPosition.GetPositionHull();
				//if (pDC->RectVisible(rHull))
				//	pObject->OnDrawObject(pDC,pObject->m_DevPosition,&DrawParams);
				pObject->GetDevObjectPosition()->ResetPosition();
			};
		};*/
		//VERIFY(pDC->RestoreDC(nSaveDC));
	};
	// vykreslen� vybran� plochy

	// mode for editing background images is special. 
	bool bBGImageMode = m_pObjsPanel && m_pObjsPanel->GetTypeObjects() == CPosObjectsPanel::grTypeBgImage;

	if (!bBGImageMode && pDoc->m_CurrentArea.IsValidArea())
	{
		pDoc->m_CurrentArea.RecalcDevObjectPositionFromLP(pDC);

		int nSaveDC = pDC->SaveDC();
		ASSERT(nSaveDC != 0);
		pDC->SetMapMode(MM_TEXT);
		pDC->SetViewportOrg(0, 0);
		pDC->SetWindowOrg(0, 0);
		// vykreslen�
		if (!bBGImageMode && pDoc->m_CurrentArea.IsValidArea())
		{
			pDoc->m_CurrentArea.OnDrawObject(pDC, &DrawParams);
		}

		VERIFY(pDC->RestoreDC(nSaveDC));
	}  
  
	if (bBGImageMode && !m_SelBGImageID.IsEmpty())
	{
		CPosBackgroundImageObject* pBGImage = m_pPoseidonMap->GetBgImage(m_SelBGImageID);
		if (pBGImage)
		{
			CPosAreaObject area(*m_pPoseidonMap);      
			area.AddRect(pBGImage->m_LogPosition.m_Position);    
			area.RecalcDevObjectPositionFromLP(pDC);

			int nSaveDC = pDC->SaveDC();
			ASSERT(nSaveDC != 0);
			pDC->SetMapMode(MM_TEXT);
			pDC->SetViewportOrg(0, 0);
			pDC->SetWindowOrg(0, 0);

			// vykreslen�
			area.OnDrawObject(pDC, &DrawParams);

			VERIFY(pDC->RestoreDC(nSaveDC));
		}
		else
		{
			m_SelBGImageID.Empty();
		}
	}  

	if (_roadVectorEditor.NotNull())  _roadVectorEditor->OnDraw(*pDC);

	// vykreslen� kurzoru
	if ((pDoc) && (DrawParams.m_pCfg->m_bCursorVisible))
	{
		CPoint ptDevPos = pDoc->m_CursorObject.m_ptCurrentPos;
		pDC->LPtoDP(&ptDevPos);
		int nSaveDC = pDC->SaveDC();
		ASSERT(nSaveDC != 0);
		pDC->SetMapMode(MM_TEXT);
		pDC->SetViewportOrg(0, 0);
		pDC->SetWindowOrg(0, 0);
		// vykreslen�
		pDoc->m_CursorObject.OnDrawCursorInDP(pDC, ptDevPos, &DrawParams);
		VERIFY(pDC->RestoreDC(nSaveDC));
	}
}

BOOL CPosEdMainView::OnEraseBkgnd(CDC* pDCsrc) 
{
	CRect rClip,rArea,rMap;	
	pDCsrc->GetClipBox(&rClip);

	CPoint ptPom = FromScrollToWindowPosition(m_rAreaLog.TopLeft());
	rArea.left = ptPom.x;
	rArea.top = ptPom.y;
	ptPom = FromScrollToWindowPosition(m_rAreaLog.BottomRight());
	rArea.right = ptPom.x;
	rArea.bottom = ptPom.y;
  EraseBkgndToDC(pDCsrc,rClip,rArea);
  return TRUE;
}

void CPosEdMainView::EraseBkgndToDC(CDC *pDC, const CRect &rClip, const CRect &rArea)
{
  CRect rMap;	
// vykresl�m pozad� mimo plochu mapy
	if ((rClip.right >= rArea.right)||(rClip.bottom >= rArea.bottom))
	{
		CBrush  bkBrush(m_wndColor),frBrush(GetSysColor(COLOR_3DHILIGHT));
		CBrush *pOldBrush = pDC->SelectObject(&bkBrush);       
		if (rClip.right >= rArea.right)
		{	// dopln�m z prav� strany
			pDC->PatBlt(rArea.right,rClip.top,rClip.right - rArea.right,rClip.Height(),PATCOPY);
			// 3D frame
			pDC->SelectObject(&frBrush);
			pDC->PatBlt(rArea.right+1,rClip.top,1,rArea.bottom-rClip.top+1,PATCOPY);
			pDC->SelectObject(&bkBrush);
		};
		if (rClip.bottom >= rArea.bottom)
		{	// dopln�m ze spodn� strany
			pDC->PatBlt(rClip.left,rArea.bottom,rArea.right - rClip.left,rClip.bottom - rArea.bottom,PATCOPY);
			// 3D frame
			pDC->SelectObject(&frBrush);
			pDC->PatBlt(rClip.left,rArea.bottom+1,rArea.right - rClip.left+2,1,PATCOPY);
		};
		pDC->SelectObject(pOldBrush);
	};
	// v�pl� pozad� mapy
	if (rMap.IntersectRect(rArea,rClip))
	{
		if (m_pPoseidonMap == NULL)
		{	// nen� dosud asociov�na mapa
			pDC->FillSolidRect(rMap,GetSysColor(COLOR_WINDOW));
		} else
		{
			// o��znut� na mapu 
			CRgn rClip;rClip.CreateRectRgn(rMap.left,rMap.top,rMap.right,rMap.bottom);
			pDC->SelectClipRgn(&rClip,RGN_AND);
			double xStep = (double)(rArea.Width());
			double yStep = (double)(rArea.Height());
			xStep /= (double)(m_pPoseidonMap->GetSize().x + 1);
			yStep /= (double)(m_pPoseidonMap->GetSize().z + 1);
			if ((xStep > 0)&&(yStep > 0))
			{
				// kde za��t vykreslovat
				int xFirst, yFirst, xUnit = 1, yUnit = 1;
				{	// X
					double nBegin   = (rMap.left - rArea.left);
						   nBegin  /= xStep;
					xFirst = (int)nBegin;
					while (rMap.left < (rArea.left+(int)Round(xFirst*xStep,0)))
						xFirst--;
					ASSERT((xFirst >= 0)&&(xFirst < m_pPoseidonMap->GetSize().x + 1));
				}
				{	// Y
					double nBegin   = (rMap.top - rArea.top);
						   nBegin  /= yStep;
					yFirst = (int)nBegin;
					while (rMap.top < (rArea.top+(int)Round(yFirst*yStep,0)))
						yFirst--;
					ASSERT((yFirst >= 0)&&(yFirst < m_pPoseidonMap->GetSize().z + 1));
				}
				// po kolika unitech vykresluji ?
				CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
				double MinUnitDp = (double)(pEnvir->m_cfgCurrent.m_nMinTextSize);
				if (xStep < MinUnitDp)
				{
					xUnit = 1+(int)(MinUnitDp/xStep);
				}
				if (yStep < MinUnitDp)
				{
					yUnit = 1+(int)(MinUnitDp/yStep);
				}

				// parametry pro vykreslov�n�
				CDrawPar DrawParams;
				OnInitDrawParamsForBk(&DrawParams);
        // drawing of background images...
        for (int i = 0; i < m_pPoseidonMap->m_BGImagesInMap.Size(); i++)
        {
          CPosBackgroundImageObject * pImage = m_pPoseidonMap->m_BGImagesInMap[i];
          //pImage->RecalcDevObjectPositionFromLP(pDC);
          pImage->m_DevPosition.m_Position = FromScrollToWindowPosition(pImage->m_LogPosition.m_Position);          
        }

				// vykresluji jednotliv� �tverce po �ad�ch
				CRect	rUnit;	  // vykreslovan� bod v DP
				int		x,y = yFirst;
				for(;y<m_pPoseidonMap->GetSize().z + 1;y+= yUnit)
				{
					x = xFirst;
					rUnit.top	 = rArea.top+(int)Round(y*yStep,0);
					rUnit.bottom = rArea.top+(int)Round((y+yUnit)*yStep,0);
          rUnit.top = max(rUnit.top, rMap.top);
          rUnit.bottom = min(rUnit.bottom, rMap.bottom);

					if (rUnit.top < rMap.bottom)
					{
						for(;x<m_pPoseidonMap->GetSize().x + 1;x+= xUnit)
						{
							rUnit.left  = rArea.left+(int)Round(x*xStep,0);
							rUnit.right = rArea.left+(int)Round((x+xUnit)*xStep,0);
              rUnit.left = max(rUnit.left, rMap.left);
              rUnit.right = min(rUnit.right, rMap.right);

							if (rUnit.left < rMap.right)
							{	// tohle se z�ejm� bude vykreslovat
                BOOL bDraw = TRUE;
                /*for (int i = 0; i < m_pPoseidonMap->m_BGImagesInMap.Size(); i++)
                {
                  CPosBackgroundImageObject * pImage = m_pPoseidonMap->m_BGImagesInMap[i];                    
                  if (pImage->m_Transparency == 100 && pImage->IsRectAtAreaDP(rUnit,TRUE))
                  {
                    bDraw = FALSE;
                    break;
                  }                  
                }*/
								if (bDraw && pDC->RectVisible(rUnit))
                {
									DoDrawBkUnit(pDC,rUnit, x - 1, (m_pPoseidonMap->GetSize().z-y - 1), xUnit, yUnit, &DrawParams);
                  if (DrawParams.m_VwStyle.m_bWaterLayer)
                    DoDrawWater(pDC,rUnit,  x - 1, (m_pPoseidonMap->GetSize().z-y - 1), xUnit, yUnit, &DrawParams);

                  /*if (DrawParams.m_VwStyle.m_Shadowing)
                    DoShadowBkUnit(pDC,rUnit);*/
                }

							} else
								break;	// dal�� ��dek
						}
					} else 
						break;	// vykresleno v�e
				}


        EnabledObjects enabled = GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.GetEnableObjects();
        for (int i = 0; i < m_pPoseidonMap->m_BGImagesInMap.Size(); i++)
        {
          CPosBackgroundImageObject * pImage = m_pPoseidonMap->m_BGImagesInMap[i];  
          if (pImage->IsEnabledObject(enabled))
          {
            pImage->LoadBitmap(*GetDocument());
            pImage->OnDrawObject(pDC, pImage->m_DevPosition, rMap, NULL);
          }
        }

				// vykresl�m vrstevnice
				if (GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowCountLn)
				{
					CPen pnCntLn(PS_SOLID, 0, DrawParams.m_cCountLnColor);
					CPen *penOld = pDC->SelectObject(&pnCntLn);
					x,y = yFirst;
					for(;y<m_pPoseidonMap->GetSize().z+1;y+= yUnit)
					{
						x = xFirst;
						rUnit.top	 = rArea.top+(int)Round(y*yStep,0);
						rUnit.bottom = rArea.top+(int)Round((y+yUnit)*yStep,0);
						if (rUnit.top < rMap.bottom)
						{
							for(;x<m_pPoseidonMap->GetSize().x + 1;x+= xUnit)
							{
								rUnit.left  = rArea.left+(int)Round(x*xStep,0);
								rUnit.right = rArea.left+(int)Round((x+xUnit)*xStep,0);
								if (rUnit.left < rMap.right)
								{	// tohle se z�ejm� bude vykreslovat                 
									if (pDC->RectVisible(rUnit))
										DoDrawCountLn(pDC,rUnit, x - 1, (m_pPoseidonMap->GetSize().z-y - 1), xUnit, yUnit, &DrawParams);
								} else
									break;	// dal�� ��dek
							}
						} else 
							break;	// vykresleno v�e
					}
					pDC->SelectObject(penOld);
				}
			} 
      else
				// p��li� mal� m���tko
				pDC->FillSolidRect(rMap,GetSysColor(COLOR_WINDOW));     
		};		
	};    
}


/////////////////////////////////////////////////////////////////////////////
// vykreslen� podkladu na bodu 

inline COLORREF ColorTimesReal(COLORREF color, float coef)
{
  return RGB(toInt(GetRValue(color) * coef),toInt(GetGValue(color) * coef),toInt(GetBValue(color) * coef));
}

void CPosEdMainView::DoShadowBkUnit(CDC* pDC,CRect& rUnit)//, int nX, int nZ,int nXCnt,int nZCnt)
{
  
  Vector3 light(-0.5, -1, -0.5);
  light.Normalize();

  CRect rect = WindowToScrollerPosition(rUnit);
  FltRect realRect =  m_pPoseidonMap->FromViewToRealRect(rect);
  float stepx = (realRect.right - realRect.left) / rUnit.Width();
  float stepy = (realRect.bottom - realRect.top) / rUnit.Height();  

  
  for(int i = rUnit.top;  i < rUnit.bottom; i++)
  {
    for(int j = rUnit.left;  j < rUnit.right; j++)
    {
      
      REALPOS pos; 
      pos.x = (j - rUnit.left) * stepx + realRect.left;
      pos.z = realRect.bottom - (i - rUnit.top) * stepy;
      float x,z;
      m_pPoseidonMap->GetLandscapeHeight(pos,&x,&z);

      Vector3 norm(-x,1,-z);
      

      float coef = - norm.Normalized() * light;
      coef = max(coef,0.1f);

      COLORREF pix = pDC->GetPixel(j,i);      
      pDC->SetPixel(j,i, ColorTimesReal(pix, coef));
    }
  }


}



void CPosEdMainView::DoDrawBkUnit(CDC* pDC,CRect& rUnit, int nX, int nZ,int nXCnt,int nZCnt,CDrawPar* pParams)
{
	// nX, nY = sou�adnice lev�ho horn�ho uniti v PoseidonSou�ednic�ch
	// nXCnt,nZCnt - po�e unit� ve vykreslovan�m rectu
	ASSERT((nX >= -1)&&(nX <= m_pPoseidonMap->GetSize().x));
	ASSERT((nZ >= -1)&&(nZ <= m_pPoseidonMap->GetSize().z));

  /// find slope in the middle, and enable shadowing 
  float coefColor = 1; 
  if (pParams->m_VwStyle.m_Shadowing)
  {
    CPoint center = WindowToScrollerPosition(rUnit.CenterPoint());
    REALPOS pos =  m_pPoseidonMap->FromViewToRealPos(center);

    Vector3 light(-0.5, -1, -0.5);
    light.Normalize();    

    float x,z;
    m_pPoseidonMap->GetLandscapeHeight(pos,&x,&z);
    Vector3 norm(-x,1,-z);

    coefColor = - norm.Normalized() * light;
    coefColor = max(coefColor,0.1f);
  }

  if (nX < 0 || nX >= m_pPoseidonMap->GetSize().x || nZ< 0 || nZ >= m_pPoseidonMap->GetSize().z)
  {
    pDC->FillSolidRect(rUnit,ColorTimesReal(pParams->m_cStdSeaColor, coefColor));
    return;
  }

	switch (pParams->m_VwStyle.m_nBaseStyle)
	{
	////////////////////////////////////////////////////
	// Korektn� vykreslen� styly zobrazen�
	case CViewStyle::tvsGround:
		{      
			UNITPOS Pos = { nX, nZ };
			PTRTEXTURETMPLT pTexture = NULL;

			if (pParams->m_VwStyle.m_bShowScndTxtr)
				pTexture = m_pPoseidonMap->GetLandTextureAtTerrainGrid(Pos);
			else
				pTexture = m_pPoseidonMap->GetBaseTextureAtTerrainGrid(Pos);
			// kresl�m mo�e nebo pevninu
			if (GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowSea)
			{
				LANDHEIGHT nHeight = m_pPoseidonMap->GetBalancedHeight(Pos,NULL);
				if (nHeight <= SEA_LAND_HEIGHT)
				{
					pDC->FillSolidRect(rUnit,ColorTimesReal(pParams->m_cStdSeaColor, coefColor));
					return;
				}
			}
			if ((pTexture == NULL)||(pTexture->m_bIsSea))
				pDC->FillSolidRect(rUnit,ColorTimesReal(pParams->m_cStdSeaColor, coefColor));
			else
				pDC->FillSolidRect(rUnit,ColorTimesReal(pParams->m_cStdGndColor, coefColor));
		}; break;

	case CViewStyle::tvsLands:
	case CViewStyle::tvsZones:
		{
			UNITPOS Pos = { nX, nZ };
			PTRTEXTURETMPLT pTexture = m_pPoseidonMap->GetBaseTextureAtTerrainGrid(Pos);
			if (pTexture == NULL)
				pTexture =  m_pPoseidonMap->GetStdSeaTexture();

			if (pTexture == NULL)
			{	// barva textury mo�e
				pDC->FillSolidRect(rUnit,ColorTimesReal(pParams->m_cStdSeaColor, coefColor));
			} else
			{	// textura existuje
				if (pParams->m_VwStyle.m_bShowSea)
				{
					LANDHEIGHT nHeight = m_pPoseidonMap->GetBalancedHeight(Pos,NULL);
					if (nHeight <= SEA_LAND_HEIGHT)
					{
						pDC->FillSolidRect(rUnit,ColorTimesReal(pParams->m_cRegSeaColor, coefColor));
						return;
					}
				}

				if (pParams->m_VwStyle.m_nBaseStyle == CViewStyle::tvsZones)
				{	// podle vegeta�n�ho p�sma
					if (pTexture->m_pLandZone != NULL)
						pDC->FillSolidRect(rUnit,ColorTimesReal(pTexture->m_pLandZone->m_pPrimarTxtr->m_cTxtrColor, coefColor));
					else
						pDC->FillSolidRect(rUnit,ColorTimesReal(pParams->m_cUndefLColor,coefColor));
				} else
				{	// podle typu krajiny
					if (pTexture->m_pLandZone != NULL)
						pDC->FillSolidRect(rUnit,ColorTimesReal(pTexture->m_pLandZone->m_pOwner->m_cLandColor,coefColor));
					else
						pDC->FillSolidRect(rUnit,ColorTimesReal(pParams->m_cUndefLColor,coefColor));
				};
			};
		}
		break;
	case CViewStyle::tvsTextur:
		{
			UNITPOS Pos = { nX, nZ };
			PTRTEXTURETMPLT pTexture = NULL;
			if (pParams->m_VwStyle.m_bShowScndTxtr)
				pTexture = m_pPoseidonMap->GetLandTextureAtTerrainGrid(Pos);
			else
				pTexture = m_pPoseidonMap->GetBaseTextureAtTerrainGrid(Pos);
			if (pTexture == NULL)
				pTexture =  m_pPoseidonMap->GetStdSeaTexture();

			if (pTexture == NULL)
			{	// barva textury mo�e
				pDC->FillSolidRect(rUnit,ColorTimesReal(pParams->m_cRegSeaColor, coefColor));
			} else
			{	// textura existuje
				if (pParams->m_VwStyle.m_bShowSea)
				{
					LANDHEIGHT nHeight = m_pPoseidonMap->GetBalancedHeight(Pos,NULL);
					if (nHeight <= SEA_LAND_HEIGHT)
					{
						pDC->FillSolidRect(rUnit,ColorTimesReal(pParams->m_cRegSeaColor, coefColor));
						return;
					}
				}
        
        if (pParams->m_VwStyle.m_ShowLock && m_pPoseidonMap->LockedTexture(m_pPoseidonMap->FromUnitToTextureUnitPos(Pos)))
          pDC->FillSolidRect(rUnit,ColorTimesReal(RGBToY(pTexture->m_cTxtrColor), coefColor));
        else
				  pDC->FillSolidRect(rUnit,ColorTimesReal(pTexture->m_cTxtrColor, coefColor));
			};
		}
		break;
	case CViewStyle::tvsHgColor:
	case CViewStyle::tvsHgMono:
  case CViewStyle::tvsHgFromTo:
		{
			CPen *penOld = (CPen*)pDC->SelectStockObject(NULL_PEN);
			DoDrawHeightScale(pDC, rUnit, nX, nZ, nXCnt, nZCnt, coefColor, pParams->m_cColorHg, pParams);
			pDC->SelectObject(penOld);
		};  break;
	}
};

void CPosEdMainView::DoDrawCountLn(CDC* pDC,CRect& rUnit, int nX, int nZ,int nXCnt,int nZCnt,CDrawPar* pParams)
{
	int i, t;
	LANDHEIGHT to;
	LANDHEIGHT step = (float)pParams->m_nStpCountLn;
 
  LANDHEIGHT hgFrom;
  LANDHEIGHT hgTo;

  if(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_nBaseStyle == CViewStyle::tvsHgFromTo)
  {
    hgFrom = pParams->m_pCfg->m_HgFrom;
    hgTo = hgFrom + MAX_SCALE_COLOR_ITEMS * pParams->m_pCfg->m_HgStep;
  }
  else
  {
    hgFrom = 0;
    hgTo = 1e6;
  }

	UNITPOS pos[4];
	pos[0].x = nX;
	pos[0].z = nZ;
	pos[1].x = nX + nXCnt;
	pos[1].z = nZ;
	pos[2].x = nX;
	pos[2].z = nZ + nZCnt;
	pos[3].x = nX + nXCnt;
	pos[3].z = nZ + nZCnt;
	UNITPOS posLast;
	
	LANDHEIGHT y[4];
	for (i=0; i<4; i++)
		y[i] = m_pPoseidonMap->GetLandscapeHeight(pos[i]);
	
	POINT ptBorder[4];
	ptBorder[0].x = rUnit.left;
	ptBorder[0].y = rUnit.bottom;
	ptBorder[1].x = rUnit.right;
	ptBorder[1].y = rUnit.bottom;
	ptBorder[2].x = rUnit.left;
	ptBorder[2].y = rUnit.top;
	ptBorder[3].x = rUnit.right;
	ptBorder[3].y = rUnit.top;

	int n0, n1, n2, np;
	POINT ptBeg, ptEnd;

	for (t=0; t<2; t++)
	{
		// trinagle t, t + 1, t + 2
		n0 = t;
		n1 = t + 1;
		n2 = t + 2;
		if (y[n0] > y[n1])
		{
			np = n0;
			n0 = n1;
			n1 = np;
		}
		if (y[n0] > y[n2])
		{
			np = n0;
			n0 = n2;
			n2 = np;
		}
		if (y[n1] > y[n2])
		{
			np = n1;
			n1 = n2;
			n2 = np;
		}

		for (to=hgFrom; y[n0] > to && hgTo >= to; to+=step);

		for (; y[n2] > to && hgTo >= to; to+=step)
		{
			ptEnd.x = ptBorder[n0].x + int((to - y[n0]) * (ptBorder[n2].x - ptBorder[n0].x) / (y[n2] - y[n0]));
			ptEnd.y = ptBorder[n0].y + int((to - y[n0]) * (ptBorder[n2].y - ptBorder[n0].y) / (y[n2] - y[n0]));
			if (to < y[n1])
			{
				ptBeg.x = ptBorder[n0].x + int((to - y[n0]) * (ptBorder[n1].x - ptBorder[n0].x) / (y[n1] - y[n0]));
				ptBeg.y = ptBorder[n0].y + int((to - y[n0]) * (ptBorder[n1].y - ptBorder[n0].y) / (y[n1] - y[n0]));
			}
			else if (to == y[n1])
			{
				ptBeg = ptBorder[n1];
			}
			else
			{
				ptBeg.x = ptBorder[n1].x + int((to - y[n1]) * (ptBorder[n2].x - ptBorder[n1].x) / (y[n2] - y[n1]));
				ptBeg.y = ptBorder[n1].y + int((to - y[n1]) * (ptBorder[n2].y - ptBorder[n1].y) / (y[n2] - y[n1]));
			}

			pDC->MoveTo(ptBeg);
			pDC->LineTo(ptEnd);
			//pDC->SetPixel(ptEnd, colorCountLn);
		}
		if (t == 0)
		{
			if (nX >= nXCnt && y[0] == to && y[2] == to)
			{
				posLast.x = nX - nXCnt;
				posLast.z = nZ + nZCnt;
				if (m_pPoseidonMap->GetLandscapeHeight(posLast) > to)
				{
					pDC->MoveTo(ptBorder[0]);
					pDC->LineTo(ptBorder[2]);
					//pDC->SetPixel(ptBorder[2], colorCountLn);
				}
			}
		}
		else
		{
			if (nZ + 2 * nZCnt < m_pPoseidonMap->GetSize().z && y[2] == to && y[3] == to)
			{
				posLast.x = nX;
				posLast.z = nZ + 2 * nZCnt;
				if (m_pPoseidonMap->GetLandscapeHeight(posLast) > to)
				{
					pDC->MoveTo(ptBorder[2]);
					pDC->LineTo(ptBorder[3]);
					//pDC->SetPixel(ptBorder[3], colorCountLn);
				}
			}
		}
	}
}

void CPosEdMainView::DoDrawHeightScale(CDC* pDC,CRect& rUnit, int nX, int nZ,int nXCnt,int nZCnt,float colorCoef, SScaleItem *scale,CDrawPar* pParams)
{
	int nScaleItems = sizeof(*scale) / sizeof(SScaleItem);
	
	int i,  t;
	LANDHEIGHT to;
	UNITPOS pos[4];
	pos[0].x = nX;
	pos[0].z = nZ;
	pos[1].x = nX + nXCnt;
	pos[1].z = nZ;
	pos[2].x = nX;
	pos[2].z = nZ + nZCnt;
	pos[3].x = nX + nXCnt;
	pos[3].z = nZ + nZCnt;

  bool locked = pParams->m_VwStyle.m_ShowLock && m_pPoseidonMap->LockedVertex(pos[0]);
	
	LANDHEIGHT y[4];
	for (i=0; i<4; i++)
		y[i] = m_pPoseidonMap->GetLandscapeHeight(pos[i]);
    //y[i] = 600;

	// optimalizuji stejnou v��ku
	if ((y[0] == y[1])&&(y[1] == y[2])&& (y[2] == y[3]))
	{
		if (y[0] == 0.f)
		{	// mo�e
			pDC->FillSolidRect(rUnit,ColorTimesReal(locked ? RGBToY(scale[0].m_cColor) :scale[0].m_cColor, colorCoef));
		} else
		{	// v��ka
			for (i=0; y[0] > scale[i].m_nHeightTo; i++);      
      pDC->FillSolidRect(rUnit,ColorTimesReal(locked ? RGBToY(scale[i].m_cColor) : scale[i].m_cColor, colorCoef) );
		};
		return;
	}
	// optimalizuji stejn� p�smo
	{
    int i0=0;
    int i1=0;
		for ( ; y[0] > scale[i0].m_nHeightTo; i0++);
		for ( ;y[1] > scale[i1].m_nHeightTo; i1++);
		if (i0==i1)
		{
      int i2=0;
			for (; y[2] > scale[i2].m_nHeightTo; i2++);
			if (i1==i2)
			{
        int i3=0;
				for (; y[3] > scale[i3].m_nHeightTo; i3++);
				if  (i2==i3)
				{
					pDC->FillSolidRect(rUnit,ColorTimesReal(locked ? RGBToY(scale[i0].m_cColor) : scale[i0].m_cColor, colorCoef));
            
					return;
				}
			}
		};
	}
	// standardn� kreslen�
	{
		POINT ptBorder[4];
		ptBorder[0].x = rUnit.left;
		ptBorder[0].y = rUnit.bottom;
		ptBorder[1].x = rUnit.right;
		ptBorder[1].y = rUnit.bottom;
		ptBorder[2].x = rUnit.left;
		ptBorder[2].y = rUnit.top;
		ptBorder[3].x = rUnit.right;
		ptBorder[3].y = rUnit.top;

		int n0, n1, n2, np;
		POINT ptPoly[4];

		CBrush brush, *brushOld;
		for (t=0; t<2; t++)
		{
			// trinagle t, t + 1, t + 2
			n0 = t;
			n1 = t + 1;
			n2 = t + 2;
			if (y[n0] > y[n1])
			{
				np = n0;
				n0 = n1;
				n1 = np;
			}
			if (y[n0] > y[n2])
			{
				np = n0;
				n0 = n2;
				n2 = np;
			}
			if (y[n1] > y[n2])
			{
				np = n1;
				n1 = n2;
				n2 = np;
			}

			for (i=0; y[n0] > scale[i].m_nHeightTo; i++);
			to = scale[i].m_nHeightTo;
			if (y[n0] == to && y[n2] > to)
				i++;

			if (y[n2] <= scale[i].m_nHeightTo)
			{
				ptPoly[0] = ptBorder[n0];
				ptPoly[1] = ptBorder[n1];
				ptPoly[2] = ptBorder[n2];
				brush.CreateSolidBrush(ColorTimesReal(locked ? RGBToY(scale[i].m_cColor) : scale[i].m_cColor, colorCoef));
				brushOld = pDC->SelectObject(&brush);
				pDC->Polygon(ptPoly, 3);
				pDC->SelectObject(brushOld);
				brush.DeleteObject();
				continue;
			}
			if (y[n1] < scale[i].m_nHeightTo)
			{
				ptPoly[0] = ptBorder[n0];
				ptPoly[1] = ptBorder[n1];
			}
			else
			{
				ptPoly[0] = ptPoly[1] = ptBorder[n0];
			}
      int i0 = i;
			for (; y[n2] > scale[i].m_nHeightTo; i++)
			{
				to = scale[i].m_nHeightTo;
				ptPoly[3].x = ptBorder[n0].x + int((to - y[n0]) * (ptBorder[n2].x - ptBorder[n0].x) / (y[n2] - y[n0]) + 0.5);
				ptPoly[3].y = ptBorder[n0].y + int((to - y[n0]) * (ptBorder[n2].y - ptBorder[n0].y) / (y[n2] - y[n0]) + 0.5);
				if (to < y[n1])
				{
					ptPoly[2].x = ptBorder[n0].x + int((to - y[n0]) * (ptBorder[n1].x - ptBorder[n0].x) / (y[n1] - y[n0]) + 0.5);
					ptPoly[2].y = ptBorder[n0].y + int((to - y[n0]) * (ptBorder[n1].y - ptBorder[n0].y) / (y[n1] - y[n0]) + 0.5);
				}
				else if (to == y[n1])
				{
					ptPoly[2] = ptBorder[n1];
				}
				else
				{
					ptPoly[2].x = ptBorder[n1].x + int((to - y[n1]) * (ptBorder[n2].x - ptBorder[n1].x) / (y[n2] - y[n1]) + 0.5);
					ptPoly[2].y = ptBorder[n1].y + int((to - y[n1]) * (ptBorder[n2].y - ptBorder[n1].y) / (y[n2] - y[n1]) + 0.5);
				}

				brush.CreateSolidBrush(ColorTimesReal(locked ? RGBToY(scale[i].m_cColor) : scale[i].m_cColor, colorCoef));
				brushOld = pDC->SelectObject(&brush);
				pDC->Polygon(ptPoly, 4);
				if (i > i0 && y[n1] < to && y[n1] > scale[i - 1].m_nHeightTo)
				{
					POINT ptPoly2[3];
					ptPoly2[0] = ptPoly[1];
					ptPoly2[1] = ptPoly[2];
					ptPoly2[2] = ptBorder[n1];
					pDC->Polygon(ptPoly2, 3);
				}
				pDC->SelectObject(brushOld);
				brush.DeleteObject();

				ptPoly[0] = ptPoly[3];
				ptPoly[1] = ptPoly[2];
			}

			if (y[n1] > scale[i - 1].m_nHeightTo)
			{
				ptPoly[2] = ptBorder[n1];
				ptPoly[3] = ptBorder[n2];
			}
			else
			{
				ptPoly[2] = ptPoly[3] = ptBorder[n2];
			}

			brush.CreateSolidBrush(ColorTimesReal(locked ? RGBToY(scale[i].m_cColor) : scale[i].m_cColor, colorCoef));
			brushOld = pDC->SelectObject(&brush);
			pDC->Polygon(ptPoly, 4);
			pDC->SelectObject(brushOld);
			brush.DeleteObject();
		}
	};
}

void CPosEdMainView::DoDrawWater(CDC* pDC,CRect&rUnit, int nX, int nZ,int nXCnt,int nZCnt,CDrawPar*pParams)
{
  const WaterLayer& water = m_pPoseidonMap->GetWaterLayer();
  if (!water.IsCreated())
    return;

  // nX, nY = sou�adnice lev�ho horn�ho uniti v PoseidonSou�ednic�ch
  // nXCnt,nZCnt - po�e unit� ve vykreslovan�m rectu
  ASSERT((nX >= -1)&&(nX <= m_pPoseidonMap->GetSize().x));
  ASSERT((nZ >= -1)&&(nZ <= m_pPoseidonMap->GetSize().z));

  if (nX < 0 || nX >= m_pPoseidonMap->GetSize().x || nZ< 0 || nZ >= m_pPoseidonMap->GetSize().z)      
    return;

  UNITPOS Pos = { nX, nZ }; 
  REALPOS realPos = m_pPoseidonMap->FromUnitToRealPos(Pos);

  if (water.GetHeight(realPos) > m_pPoseidonMap->GetLandscapeHeight(Pos) + 0.05f)
  {
    pDC->FillSolidRect(rUnit,pParams->m_cStdSeaColor);
  }
}

/////////////////////////////////////////////////////////////////////////////
// podpora editace

// pr�ce s m_pEditParams
BOOL CPosEdMainView::EOSearchEditObject(CPoint ptAt)
{
	if (m_pPoseidonMap == NULL)
		return FALSE;
	// vyhled�n� objektu
	CSize szDif = GetLogInMM();
	CPosObject* pObject = m_pPoseidonMap->GetObjectAt(ptAt,max(5,szDif.cx),GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.GetEnableObjects());
	if (pObject)
	{
		m_EditObjsParams.SetSelObject(pObject);
		return TRUE;
	}
	return FALSE;
};

// v�b�r objektu podle m_pEditParams
BOOL CPosEdMainView::EOSelectObject()
{
	ASSERT(m_pEditParams != NULL);
	ASSERT(m_pEditParams->m_pSelObject != NULL);
	ASSERT_KINDOF(CPosObject,m_pEditParams->m_pSelObject);
	// v�dy potvrzen v�b�r objektu
	return TRUE; };
// deselekce objektu
BOOL CPosEdMainView::EOUnselectObject()
{	// vzdy mohu zru�it v�b�r objektu
	return TRUE; };

// �daje o objektu (vrac� p��znak zm�ny �daj�)
BOOL CPosEdMainView::EOUpdateInfo()
{
	ASSERT(m_pEditParams != NULL);
	ASSERT(m_pEditInfo	 != NULL);
	ASSERT_KINDOF(CEditObjsParams,m_pEditParams);
	ASSERT_KINDOF(CEditObjsInfo,m_pEditInfo);
	// update informac� o objektu podle parametr� editace
	if ((m_pEditParams)&&(m_pEditInfo))
	{
		m_pEditInfo->m_pSelObject = m_pEditParams->m_pSelObject;
		// m_rPosition a� v CreateTracker - �et��m �as
		((CEditObjsInfo*)m_pEditInfo)->m_pEditParams= (CEditObjsParams*)m_pEditParams;
		return TRUE;
	}
	return FALSE;
};

// objekt byl zm�n�n trackerem->p�izp�sob� se
BOOL CPosEdMainView::EOObjectFromTracker()
{
	if ((m_pEditParams)&&(m_pEditInfo)&&
		(m_pEditParams->m_pSelObject)&&(m_pEditInfo->m_pSelTracker))
	{
		ASSERT_KINDOF(CPosObject,m_pEditParams->m_pSelObject);
		ASSERT_KINDOF(CEditObjsParams,m_pEditParams);
		CEditObjsParams* pEditParams = (CEditObjsParams*)m_pEditParams;
		// podle trackeru
		if (m_pEditInfo->m_pSelTracker->IsKindOf(RUNTIME_CLASS(CTrackerBase)))
		{	// tracker na dn� n�bojnice
			CPosObject* pSrc = (CPosObject*)(m_pEditParams->m_pSelObject);			
			if (pSrc->IsKindOf(RUNTIME_CLASS(CPosGroupObject)))
			{	// offset
				CPosGroupObject* pGroup= (CPosGroupObject*)pSrc;
				CTrackerBase* pTracker = (CTrackerBase*)(m_pEditInfo->m_pSelTracker);

        // update polohy skupiny objekt�
        Action_RotateMovePoseidonObjects((CPosGroupObject*)pSrc,pTracker->m_offset,pTracker->m_rotatedGrad, pTracker->m_logPos.m_ptCenter,(CPosEdMainDoc*)m_pDocument,NULL);         
        /*if (pTracker->m_offset != CPoint(0,0))
        {        
				  Action_MovePoseidonObjects((CPosGroupObject*)pSrc,pTracker->m_offset.x,pTracker->m_offset.y,(CPosEdMainDoc*)m_pDocument,NULL);         
        }
        else if (pTracker->m_rotatedGrad != 0)
          Action_RotatePoseidonObjects((CPosGroupObject*)pSrc, pTracker->m_rotatedGrad, pTracker->m_logPos.m_ptCenter, (CPosEdMainDoc*)m_pDocument,NULL);
          */
			} 
      else
      {
        ASSERT(FALSE);
      }
		}
	}
	return FALSE;
};

/////////////////////////////////////////////////////////////////////////////
// selekce objektu

void CPosEdMainView::DoSelectPosObject(CPosObject* pObj, SelMode iSelMode,BOOL bUpdate, BOOL bUpdateBuldozer)
{
  if (pObj == NULL)
    return;

  CPosEdMainDoc* pDoc = GetDocument();

  switch (iSelMode )
  {  
  case smNew:
    // odselektuji    
    pDoc->m_GroupObjects.DestroyGroup();
    pDoc->m_GroupObjects.AddObjectGroup(pObj);    
    break;
  case smDel:
    pDoc->m_GroupObjects.DelObjectGroup(pObj);
    break;
  case smAdd:
    pDoc->m_GroupObjects.AddObjectGroup(pObj);
    break;          
  case smAddDontCheck:
    pDoc->m_GroupObjects.AddObjectGroup(pObj, false);
    break;          
  };

  if (bUpdate)
  {
    // ru��m p�vodn�
    DoUnselectObject();

    // nov� selekce
    m_EditObjsParams.SetSelObject(&(pDoc->m_GroupObjects));
    CFMntScrollUpdate cUpdate(uvfEOSelObject,this);
    UpdateAllViews(&cUpdate);

    if (bUpdateBuldozer)
      SendSelectionToBuldozer();
  }
};
void CPosEdMainView::DoUnselectPosObject()
{	
  DoUnselectObject();
  GetDocument()->m_GroupObjects.DestroyGroup();
}

void CPosEdMainView::SendSelectionToBuldozer()
{  
  if (GetDocument()->RealViewer()) GetDocument()->RealViewer()->SelectionObjectClear();

  if ((m_pEditInfo!=NULL)&&(m_pEditInfo->m_pSelObject!=NULL))
  {	// je n�co vybr�no
    if (!m_pEditInfo->m_pSelObject->IsKindOf(RUNTIME_CLASS(CPosGroupObject)))
      return;

    CPosGroupObject* group = (CPosGroupObject*)(m_pEditInfo->m_pSelObject);   

    if (group->GetGroupSize() > 1)
    {	// vlo��m objekty ze skupiny
      AutoArray<SMoveObjectPosMessData> msgData;
      for(int i=0; i<group->m_SelObjects.GetSize(); i++)
      {        
        CPosObject* pObj = (CPosObject*)(group->m_SelObjects[i]);
        if (pObj != NULL)
        {
          if (pObj->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
          {
            SMoveObjectPosMessData& data = msgData.Append();

            ((CPosEdObject *)pObj)->SetObjectDataToMessage(data);          
          }
          else
          {
            // selection of other objects is not synchronized...
          }
        }
      }
      if (GetDocument()->RealViewer()) GetDocument()->RealViewer()->BlockSelectionObject(msgData);
    } else
    {	// vlo��m vlastn� objekt
      if (group->GetGroupSize() > 0)
        OnUpdateSelObjectBuldozer((CPosObject *) group->m_SelObjects[0]);
    };
  } 
}

void CPosEdMainView::DoSelectPosObjects(CObArray& Objects, SelMode iSelMode,BOOL bUpdateBuldozer)
{
	if (Objects.GetSize() <= 0)
	{
		if (iSelMode == smNew)
    {
      DoUnselectPosObject();	// odselektuji
    }

    CFMntScrollUpdate cUpdate(uvfEOSelObject,this);
    UpdateAllViews(&cUpdate);

    if (bUpdateBuldozer)
      SendSelectionToBuldozer();

		// else - zadna zmena
	} else
	{
		if (iSelMode == smNew)
    {    
			DoUnselectPosObject();	// odselektuji      
      iSelMode = smAddDontCheck;
    }
		// neexclusivn� selekce objekt�
		for(int i=0; i<Objects.GetSize(); i++)
		{
			CPosObject* pObj = (CPosObject*)(Objects[i]);
			if (pObj != NULL)
			{
				DoSelectPosObject(pObj, iSelMode, (i==(Objects.GetSize()-1)), bUpdateBuldozer && (i==(Objects.GetSize()-1)));
			}
		}
	};
};

void CPosEdMainView::DoSelectPosObjsInRect(CRect& rArea, const EnabledObjects& enabled, SelMode iSelMode, BOOL bUpdateBuldozer)
{
  AfxGetApp()->BeginWaitCursor();
	CObArray	   ObjGroup;
	CPosEdMainDoc* pDoc = (CPosEdMainDoc*)m_pDocument;
	// v�b�r objekt�
	int i;
	for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonNameAreaCount();i++)
	{
		CPosNameAreaObject*	pArea = pDoc->m_PoseidonMap.GetPoseidonNameArea(i);
		if ((pArea)&&(pArea->IsEnabledObject(enabled)))
		{
			if (pArea->IsObjectInRect(rArea))
				ObjGroup.Add(pArea);
		}
	};
	for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonKeyPtCount();i++)
	{
		CPosKeyPointObject*	pKeyPt = pDoc->m_PoseidonMap.GetPoseidonKeyPt(i);
		if ((pKeyPt)&&(pKeyPt->IsEnabledObject(enabled)))
		{
			if (pKeyPt->IsObjectInRect(rArea))
				ObjGroup.Add(pKeyPt);
		}
	};
	for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonWoodsCount();i++)
	{
		CPosWoodObject*	pWood = pDoc->m_PoseidonMap.GetPoseidonWood(i);
		if ((pWood)&&(pWood->IsEnabledObject(enabled)))
		{
			if (pWood->IsObjectInRect(rArea))
				ObjGroup.Add(pWood);
		}
	};
	for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonNetsCount();i++)
	{
		CPosNetObjectBase*	pNet = pDoc->m_PoseidonMap.GetPoseidonNet(i);
		if ((pNet)&&(pNet->IsEnabledObject(enabled)))
		{
			if (pNet->IsObjectInRect(rArea))
				ObjGroup.Add(pNet);
		}
	};
	for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonObjectCount();i++)
	{
		CPosEdObject* pObject = pDoc->m_PoseidonMap.GetPoseidonObject(i);
    if ((pObject)&&pObject->IsObjectInRect(rArea))
		  if ((pObject)&&(pObject->IsEnabledObject(enabled)))
		  {			
				ObjGroup.Add(pObject);
		  };
	};
	// v�b�r objekt�
	DoSelectPosObjects(ObjGroup,iSelMode, bUpdateBuldozer);
  AfxGetApp()->EndWaitCursor();
};
void CPosEdMainView::DoSelectPosObjsAll(const EnabledObjects& enabled, BOOL bUpdateBuldozer)
{
  AfxGetApp()->BeginWaitCursor();
  CObArray	   ObjGroup;
  CPosEdMainDoc* pDoc = (CPosEdMainDoc*)m_pDocument;
  // v�b�r objekt�
  int i;
  for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonNameAreaCount();i++)
  {
    CPosNameAreaObject*	pArea = pDoc->m_PoseidonMap.GetPoseidonNameArea(i);
    if ((pArea)&&(pArea->IsEnabledObject(enabled)))          
      ObjGroup.Add(pArea);    
  };
  for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonKeyPtCount();i++)
  {
    CPosKeyPointObject*	pKeyPt = pDoc->m_PoseidonMap.GetPoseidonKeyPt(i);
    if ((pKeyPt)&&(pKeyPt->IsEnabledObject(enabled)))
        ObjGroup.Add(pKeyPt);
  };
  for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonWoodsCount();i++)
  {
    CPosWoodObject*	pWood = pDoc->m_PoseidonMap.GetPoseidonWood(i);
    if ((pWood)&&(pWood->IsEnabledObject(enabled)))
      ObjGroup.Add(pWood);
  };
  for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonNetsCount();i++)
  {
    CPosNetObjectBase*	pNet = pDoc->m_PoseidonMap.GetPoseidonNet(i);
    if ((pNet)&&(pNet->IsEnabledObject(enabled)))
      ObjGroup.Add(pNet);    
  };
  for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonObjectCount();i++)
  {
    CPosEdObject* pObject = pDoc->m_PoseidonMap.GetPoseidonObject(i);   
    if ((pObject)&&(pObject->IsEnabledObject(enabled)))      			
      ObjGroup.Add(pObject);      
  };
  // v�b�r objekt�
  DoSelectPosObjects(ObjGroup,smNew, bUpdateBuldozer);
  AfxGetApp()->EndWaitCursor();
};

void CPosEdMainView::DoSelectPosObjsInRgn(CRgn& rArea, const EnabledObjects& enabled, SelMode iSelMode, BOOL bUpdateBuldozer)
{
  
	CObArray	   ObjGroup;
	CPosEdMainDoc* pDoc = (CPosEdMainDoc*)m_pDocument;

	AfxGetApp()->BeginWaitCursor();
	// v�b�r objekt�
	int i;
	for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonNameAreaCount();i++)
	{
		CPosNameAreaObject*	pArea = pDoc->m_PoseidonMap.GetPoseidonNameArea(i);
		if ((pArea)&&(pArea->IsEnabledObject(enabled)))
		{
			if (pArea->IsObjectInRgn(rArea))
				ObjGroup.Add(pArea);
		}
	};
	for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonKeyPtCount();i++)
	{
		CPosKeyPointObject*	pKeyPt = pDoc->m_PoseidonMap.GetPoseidonKeyPt(i);
		if ((pKeyPt)&&(pKeyPt->IsEnabledObject(enabled)))
		{
			if (pKeyPt->IsObjectInRgn(rArea))
				ObjGroup.Add(pKeyPt);
		}
	};
	for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonWoodsCount();i++)
	{
		CPosWoodObject*	pWood = pDoc->m_PoseidonMap.GetPoseidonWood(i);
		if ((pWood)&&(pWood->IsEnabledObject(enabled)))
		{
			if (pWood->IsObjectInRgn(rArea))
				ObjGroup.Add(pWood);
		}
	};
	for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonNetsCount();i++)
	{
		CPosNetObjectBase*	pNet = pDoc->m_PoseidonMap.GetPoseidonNet(i);
		if ((pNet)&&(pNet->IsEnabledObject(enabled)))
		{
			if (pNet->IsObjectInRgn(rArea))
				ObjGroup.Add(pNet);
		}
	};
  for(i=0; i<pDoc->m_PoseidonMap.GetPoseidonObjectCount();i++)
  {
    CPosEdObject* pObject = pDoc->m_PoseidonMap.GetPoseidonObject(i);
    if ((pObject)&&(pObject->IsEnabledObject(enabled)))
    {
      if (pObject->IsObjectInRgn(rArea))
        ObjGroup.Add(pObject);
    };
  };
	// v�b�r objekt�
	DoSelectPosObjects(ObjGroup,iSelMode, bUpdateBuldozer);

	AfxGetApp()->EndWaitCursor();
};

void CPosEdMainView::GetSelObjectInAction(CObArray& SelObjs,const CPosAction* pAction) const
{
	if (pAction->IsKindOf(RUNTIME_CLASS(CPosActionGroup)))
	{
		CPosActionGroup* pGrpAction = (CPosActionGroup*)pAction;
		for(int i=0; i<pGrpAction->m_Actions.GetSize(); i++)
		{
			CPosAction* pObjAction = (CPosObjectAction*)(pGrpAction->m_Actions[i]);
			if (pObjAction)
			{
				GetSelObjectInAction(SelObjs,pObjAction);
			}
		}
	} else
	if (pAction->IsKindOf(RUNTIME_CLASS(CPosObjectAction)))
	{
		CPosObjectAction* pObjAction = (CPosObjectAction*)pAction;
		if (pObjAction->m_nSelectObjType != CPosObjectAction::ida_SelectNone)
		{
			ASSERT_KINDOF(CPosObjectAction,pObjAction);
			CPosObject* pDocObject = NULL;
			if (pObjAction->m_pOldValue != NULL)
				pDocObject = GetDocument()->GetDocumentObjectFromValue(pObjAction->m_pOldValue);
			// vlo��m objekt
			if (pDocObject != NULL)
			{
				ASSERT_KINDOF(CPosObject,pDocObject);
				SelObjs.Add(pDocObject);
			}
		};
	};
};

/* NOT USED CORRECTLY
void CPosEdMainView::CollectPosEdObjsInterfereIntoArea(const CRect& logArea, const EnabledObjects& enabled, AutoArray<CPosEdObject *> dest)
{
  CPoseidonMap& map = GetDocument()->m_PoseidonMap;
  CRect hull;
  for(int i=0; i < map.GetPoseidonObjectCount();i++)
  {
    CPosEdObject* pObject = map.GetPoseidonObject(i);
   
    if (pObject && pObject->IsEnabledObject(enabled))      
    {			
      pObject->GetObjectHull(hull);
      if (IsIntersectRect(hull, logArea))
        dest.Add(pObject);
    };
  };
}*/
/////////////////////////////////////////////////////////////////////////////
// implementace p��m� editace (TRUE->zm�na)
BOOL CPosEdMainView::EOEditObject()
{
	if (EOCanEditObject())
	{
		ASSERT(m_EditObjsParams.m_pSelObject != NULL);
		
		ASSERT_KINDOF(CPosGroupObject,m_pEditInfo->m_pSelObject);
		CPosGroupObject* group = (CPosGroupObject *) m_pEditInfo->m_pSelObject;
		if (group->GetGroupSize() != 1) return FALSE;

		CPosObject* pSrc = (CPosObject*)(*group)[0];
   
		// editace objektu
		if (pSrc->CanEditObject())
		{
			CPosObject* pMod = pSrc->CreateCopyObject();
			if (pMod->DoEditObject())
			{	// generuji zm�nu
				if (pSrc->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
				{
					Action_EditPoseidonObject((CPosEdObject*)pMod, (CPosEdMainDoc*)m_pDocument, NULL);
				}
				else if (pSrc->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)))
				{
					Action_EditPoseidonNameArea((CPosNameAreaObject*)pMod, (CPosEdMainDoc*)m_pDocument, NULL);
				}
				else if (pSrc->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))
				{
					Action_EditPoseidonKeyPoint((CPosKeyPointObject*)pMod, (CPosEdMainDoc*)m_pDocument, NULL);
				}
				else if (pSrc->IsKindOf(RUNTIME_CLASS(CPosNetObjectNor)))
				{
					Action_EditPoseidonNetNor((CPosNetObjectNor*)pMod, (CPosEdMainDoc*)m_pDocument, NULL);
				}
				else if (pSrc->IsKindOf(RUNTIME_CLASS(CPosNetObjectKey)))
				{
					Action_EditPoseidonNetKey((CPosNetObjectKey*)pMod, (CPosEdMainDoc*)m_pDocument, NULL, TRUE);
				}
			}
			delete pMod;
		}
	}
	// ��dn� zm�na zp�soben� editac�
	return FALSE;
}

BOOL CPosEdMainView::EOCanEditObject()
{	// mohu prov�st editaci ?
	return CanEditObject();
}

// editace a zru�en� objektu
BOOL CPosEdMainView::CanEditObject()
{
	if (!CFMntScrollerView::EOCanEditObject()) return FALSE;
	// mus� b�t vybr�n objekt
	if (m_EditObjsParams.m_pSelObject == NULL) return FALSE;
	// mohu prov�st editaci
	ASSERT_KINDOF(CPosGroupObject, m_pEditInfo->m_pSelObject);
	CPosGroupObject * group = (CPosGroupObject *) m_pEditInfo->m_pSelObject;
	if (group->GetGroupSize() == 0) return FALSE;

	ASSERT_KINDOF(CPosObject,(*group)[0]);
	return ((CPosObject*)(*group)[0])->CanEditObject();
}

void CPosEdMainView::OnEditObject()
{
	if (CanEditObject()) DoEditObject(NULL);
}

void CPosEdMainView::OnUpdateEditObject(CCmdUI* pCmdUI)
{	
	pCmdUI->Enable(CanEditObject()); 
}

BOOL CPosEdMainView::CanDeleteObject()
{	
	// je to les
	if (!m_pEditInfo || !m_pEditInfo->m_pSelObject) return FALSE;

	ASSERT_KINDOF(CPosGroupObject,m_pEditInfo->m_pSelObject);
	CPosGroupObject* group = (CPosGroupObject*)(m_pEditParams->m_pSelObject);
	if (group == NULL) return FALSE;
	
	if (group->GetGroupSize() > 1 || group->GetGroupSize() == 0) return TRUE;

	if ((*group)[0]->IsKindOf(RUNTIME_CLASS(CPosWoodObject))) return TRUE;
	if ((*group)[0]->IsKindOf(RUNTIME_CLASS(CPosNetObjectBase))) return TRUE;
	// editovateln� objekt ?
	return CanEditObject(); 
}

void CPosEdMainView::OnDeleteObject()
{
	if (!CanDeleteObject()) return;
	// vybran� objekt
	ASSERT_KINDOF(CPosGroupObject, m_pEditInfo->m_pSelObject);
	CPosGroupObject* pSrc = (CPosGroupObject*)(m_pEditParams->m_pSelObject);	

	Action_DeletePoseidonObjects(pSrc, (CPosEdMainDoc*)m_pDocument, NULL);

	// sm�u objekt dle typu
	/*if (pSrc->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
		Action_DeletePoseidonObject((CPosEdObject*)pSrc,(CPosEdMainDoc*)m_pDocument,NULL);
	else
	if (pSrc->IsKindOf(RUNTIME_CLASS(CPosGroupObject)))
		Action_DeletePoseidonObjects((CPosGroupObject*)pSrc,(CPosEdMainDoc*)m_pDocument,NULL);
	else
	if (pSrc->IsKindOf(RUNTIME_CLASS(CPosWoodObject)))
		Action_DeletePoseidonWood((CPosWoodObject*)pSrc,(CPosEdMainDoc*)m_pDocument,NULL);
	else
	if (pSrc->IsKindOf(RUNTIME_CLASS(CPosNetObjectBase)))
		Action_DeletePoseidonNet((CPosNetObjectBase*)pSrc,(CPosEdMainDoc*)m_pDocument,NULL);
	else
	if (pSrc->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)))
		Action_DeletePoseidonNameArea((CPosNameAreaObject*)pSrc,(CPosEdMainDoc*)m_pDocument,NULL);
	else
	if (pSrc->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))
		Action_DeletePoseidonKeyPoint((CPosKeyPointObject*)pSrc,(CPosEdMainDoc*)m_pDocument,NULL);
    */
}

void CPosEdMainView::OnUpdateDeleteObject(CCmdUI* pCmdUI)
{	
	pCmdUI->Enable(CanDeleteObject()); 
}

// pr�ce s kurzorem
void CPosEdMainView::OnGotoCursorObject()
{
	CPosEdCursor* pCursor = &(GetDocument()->m_CursorObject);
	if ((pCursor->m_rpBuldozrPos.x != pCursor->m_rpCurrentPos.x)||
		(pCursor->m_rpBuldozrPos.z != pCursor->m_rpCurrentPos.z))
	{
		//if (GetPosEdEnvironment()->m_optPosEd.m_nBldCursorUpdate == CPosEdOptions::upBlCurGoto)
		{	// update re�ln� polohy p�i p�echodu
			pCursor->SetCursorRealPosition(pCursor->m_rpBuldozrPos,FALSE);
		}
	}
	// p�echod na polohu kurzoru
	CRect rPos(pCursor->m_ptCurrentPos.x-1,pCursor->m_ptCurrentPos.y-1,
			   pCursor->m_ptCurrentPos.x+1,pCursor->m_ptCurrentPos.y+1);
	CenterToRect(&rPos);
};

void CPosEdMainView::OnUpdateGotoCursorObject(CCmdUI* pCmdUI)
{	pCmdUI->Enable(TRUE); };

/////////////////////////////////////////////////////////////////////////////
// STATUS bar

void CPosEdMainView::OnUpdateStatusInfoObject(CCmdUI* pCmdUI)
{	
  pCmdUI->Enable(TRUE);
	CString sText = _T(" ");
	// jm�no objektu
	CPosGroupObject* pSrc = (CPosGroupObject*)(m_pEditParams->m_pSelObject);
	if (pSrc != NULL)
  {
    if ( pSrc->GetGroupSize() == 1)
      sText += ((CPosObject *)(*pSrc)[0])->GetStatusInfo();
    else
		  pSrc->GetObjectName(sText);
  }

	pCmdUI->SetText(sText);
};

/////////////////////////////////////////////////////////////////////////////
// pr�ce s blokem
void CPosEdMainView::OnSelObjAreaRect()
{
	BOOL bEnable = FALSE;
	CPosObject* pSrc = (CPosObject*)(m_pEditParams->m_pSelObject);
	if (pSrc != NULL)
	{
		if (pSrc->IsKindOf(RUNTIME_CLASS(CPosWoodObject)))
			bEnable = TRUE;
		else
		if (pSrc->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)))
			bEnable = TRUE;
		else
		if (pSrc->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))
			bEnable = TRUE;
	}
	if (bEnable)
		Action_SelPoseidonCurrArea(pSrc,TRUE,(CPosEdMainDoc*)m_pDocument,NULL);
};
void CPosEdMainView::OnUpdateSelObjAreaRect(CCmdUI* pCmdUI)
{	
	BOOL bEnable = FALSE;
	CPosObject* pSrc = (CPosObject*)(m_pEditParams->m_pSelObject);
	if (pSrc != NULL)
	{
		if (pSrc->IsKindOf(RUNTIME_CLASS(CPosWoodObject)))
			bEnable = TRUE;
		else
		if (pSrc->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)))
			bEnable = TRUE;
		else
		if (pSrc->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))
			bEnable = TRUE;
	}
	pCmdUI->Enable(bEnable);
};

/////////////////////////////////////////////////////////////////////////////
// pr�ce s v��kou

void CPosEdMainView::OnErozeLandscape()
{	Action_ErozePoseidonLand(m_rAreaLog,(CPosEdMainDoc*)m_pDocument); };
void CPosEdMainView::OnUpdateErozeLandscape(CCmdUI* pCmdUI)
{	pCmdUI->Enable((GetDocument()->m_CurrentArea.IsValidArea())); };	// mohu v�dy prov�st erozi

void CPosEdMainView::OnHeightLandscape()
{	
	if (GetDocument()->m_CurrentArea.IsValidArea())
		Action_HeightPoseidonLand(*(GetDocument()->m_CurrentArea.GetLogObjectPosition()),(CPosEdMainDoc*)m_pDocument); 
};
void CPosEdMainView::OnUpdateHeightLandscape(CCmdUI* pCmdUI)
{	pCmdUI->Enable((GetDocument()->m_CurrentArea.IsValidArea())); };	

/////////////////////////////////////////////////////////////////////////////
// vytvo�en� objekt�

// vypln�n� ploch texturami
void CPosEdMainView::OnFillAreaByTxtr()
{
  CTextureLayer *layer = GetDocument()->GetActiveTextureLayer();
  if (!layer || layer->CheckExplicitBimpas()) return;
	Ref<CPosTextureTemplate> pTxtr = m_pObjsPanel->GetFillTextureTxtr();  
	if ((pTxtr!=NULL)&&(GetDocument()->m_CurrentArea.IsValidArea()))
		Action_TexturePoseidonTxtr2(*(GetDocument()->m_CurrentArea.GetLogObjectPosition()),pTxtr,(CPosEdMainDoc*)m_pDocument,NULL);
};
void CPosEdMainView::OnUpdateFillAreaByTxtr(CCmdUI* pCmdUI)
{
  CTextureLayer *layer = GetDocument()->GetActiveTextureLayer();
  bool enabled = layer && !layer->CheckExplicitBimpas();
	CPosTextureTemplate* pTxtr = m_pObjsPanel->GetFillTextureTxtr();
	pCmdUI->Enable(enabled && (pTxtr!=NULL)&&(GetDocument()->m_CurrentArea.IsValidArea()));
};
void CPosEdMainView::OnFillAreaByZone()
{
  CTextureLayer *layer = GetDocument()->GetActiveTextureLayer();
  if (!layer || layer->CheckExplicitBimpas()) return;
	CPosTextureZone*  pZone = m_pObjsPanel->GetFillTextureZone();
	if ((pZone!=NULL)&&(GetDocument()->m_CurrentArea.IsValidArea()))
		Action_TexturePoseidonZone(*(GetDocument()->m_CurrentArea.GetLogObjectPosition()),pZone,(CPosEdMainDoc*)m_pDocument,NULL);
};
void CPosEdMainView::OnUpdateFillAreaByZone(CCmdUI* pCmdUI)
{
  CTextureLayer *layer = GetDocument()->GetActiveTextureLayer();
  bool enabled = layer && !layer->CheckExplicitBimpas();
	CPosTextureZone*  pZone = m_pObjsPanel->GetFillTextureZone();
	pCmdUI->Enable(enabled && (pZone!=NULL)&&(GetDocument()->m_CurrentArea.IsValidArea()));
};
void CPosEdMainView::OnFillAreaByLand()
{
  CTextureLayer *layer = GetDocument()->GetActiveTextureLayer();
  if (!layer || layer->CheckExplicitBimpas()) return;
	CPosTextureLand*  pLand = m_pObjsPanel-> GetFillTextureLand();
	if ((pLand!=NULL)&&(GetDocument()->m_CurrentArea.IsValidArea()))
		Action_TexturePoseidonLand(*(GetDocument()->m_CurrentArea.GetLogObjectPosition()),pLand,(CPosEdMainDoc*)m_pDocument,NULL);
};
void CPosEdMainView::OnUpdateFillAreaByLand(CCmdUI* pCmdUI)
{
  CTextureLayer *layer = GetDocument()->GetActiveTextureLayer();
  bool enabled = layer && !layer->CheckExplicitBimpas();
	CPosTextureLand*  pLand = m_pObjsPanel-> GetFillTextureLand();
	pCmdUI->Enable(enabled && (pLand!=NULL)&&(GetDocument()->m_CurrentArea.IsValidArea()));
};

void CPosEdMainView::OnFillAreaByChange()
{
  CTextureLayer *layer = GetDocument()->GetActiveTextureLayer();
  if (!layer || layer->CheckExplicitBimpas()) return;
	CPosTextureTemplate* pTxtr = m_pObjsPanel->GetFillTextureTxtr();
	if ((pTxtr!=NULL)&&(GetDocument()->m_CurrentArea.IsValidArea()))
		Action_TexturePoseidonChng(*(GetDocument()->m_CurrentArea.GetLogObjectPosition()),pTxtr,(CPosEdMainDoc*)m_pDocument,NULL);
};

void CPosEdMainView::OnUpdateFillAreaByChange(CCmdUI* pCmdUI)
{
  CTextureLayer *layer = GetDocument()->GetActiveTextureLayer();
  bool enabled = layer && !layer->CheckExplicitBimpas();
	CPosTextureTemplate* pTxtr = m_pObjsPanel->GetFillTextureTxtr();
	pCmdUI->Enable(enabled && (pTxtr!=NULL)&&(GetDocument()->m_CurrentArea.IsValidArea()));
};

void CPosEdMainView::OnToolsAnalyseTxtrs() 
{
  CTextureLayer *layer = GetDocument()->GetActiveTextureLayer();
  if (!layer || layer->CheckExplicitBimpas()) return;
  CPosEdMainDoc* pDoc = (CPosEdMainDoc*)m_pDocument;
  REALPOS		   GoTo;
  if (DoTextureAnalyse(pDoc,GoTo))
  {	// p�ejdu na kurzor
    pDoc->m_CursorObject.SetCursorRealPosition(GoTo,FALSE);
    OnGotoCursorObject();
  }
}

void CPosEdMainView::OnUpdateToolsAnalyseTxtrs(CCmdUI* pCmdUI)
{  
  CTextureLayer *layer = GetDocument()->GetActiveTextureLayer();
  bool enabled = layer && !layer->CheckExplicitBimpas();
  pCmdUI->Enable(enabled && GetDocument()->m_CurrentArea.IsValidArea());
};

void CPosEdMainView::OnCreateWood()
{
	// vlo��m les
	CPosWoodTemplate* pWoodTmpl = m_pObjsPanel->GetCurrentWood();
	if ((pWoodTmpl!=NULL)&&(GetDocument()->m_CurrentArea.IsValidArea()))
		Action_CreatePoseidonWood(pWoodTmpl,(CPosEdMainDoc*)m_pDocument,NULL);
};

void CPosEdMainView::OnUpdateCreateWood(CCmdUI* pCmdUI)
{
	CPosWoodTemplate* pWood = m_pObjsPanel->GetCurrentWood();
	pCmdUI->Enable((pWood!=NULL)&&(GetDocument()->m_CurrentArea.IsValidArea()));
};

// generov�n� s�t�
void CPosEdMainView::OnGenerateNet()
{
	CPosObject* pSrc = (CPosObject*)(m_pEditParams->m_pSelObject);
	if ((pSrc!=NULL)&&(pSrc->IsKindOf(RUNTIME_CLASS(CPosGroupObject))))
	{		
		if (((CPosGroupObject*)pSrc)->m_SelObjects.GetSize()==2)
		{
			CPosObject* pNet1 = (CPosObject*)(((CPosGroupObject*)pSrc)->m_SelObjects[0]);
			CPosObject* pNet2 = (CPosObject*)(((CPosGroupObject*)pSrc)->m_SelObjects[1]);
			if ((pNet1 != NULL)&&(pNet2 != NULL)&&
				(pNet1->IsKindOf(RUNTIME_CLASS(CPosNetObjectKey)))&&
				(pNet2->IsKindOf(RUNTIME_CLASS(CPosNetObjectKey))))
			{	// dv� kl��ov� s�t�
				DoPreGenerateNetBetween((CPosNetObjectKey*)pNet1,(CPosNetObjectKey*)pNet2,(CPosEdMainDoc*)m_pDocument);
			}
		}
	}
};
void CPosEdMainView::OnUpdateGenerateNet(CCmdUI* pCmdUI)
{
	CPosObject* pSrc = (CPosObject*)(m_pEditParams->m_pSelObject);
	if ((pSrc!=NULL)&&(pSrc->IsKindOf(RUNTIME_CLASS(CPosGroupObject))))
	{		
		if (((CPosGroupObject*)pSrc)->m_SelObjects.GetSize()==2)
		{
			CPosObject* pNet1 = (CPosObject*)(((CPosGroupObject*)pSrc)->m_SelObjects[0]);
			CPosObject* pNet2 = (CPosObject*)(((CPosGroupObject*)pSrc)->m_SelObjects[1]);
			if ((pNet1 != NULL)&&(pNet2 != NULL)&&
				(pNet1->IsKindOf(RUNTIME_CLASS(CPosNetObjectKey)))&&
				(pNet2->IsKindOf(RUNTIME_CLASS(CPosNetObjectKey))))
			{	// dv� kl��ov� s�t�

				pCmdUI->Enable(TRUE);
				return;
			}
		}
	}
	pCmdUI->Enable(FALSE);
};

// pojmenovan� oblast
void CPosEdMainView::OnCreateNameArea()
{
	if (GetDocument()->m_CurrentArea.IsValidArea())
	{
		Action_CreatePoseidonNameArea((CPosEdMainDoc*)m_pDocument,NULL);
	};
};
void CPosEdMainView::OnUpdateCreateNameArea(CCmdUI* pCmdUI)
{	pCmdUI->Enable(GetDocument()->m_CurrentArea.IsValidArea()); };

void CPosEdMainView::OnGoToNameArea()
{
	if (m_pObjsPanel != NULL)
	{
		CPosNameAreaObject* pArea = m_pObjsPanel->GetCurrentNameArea();
		if (pArea)
		{
			// prechod na polohu
			CRect  rHull; pArea->GetObjectHull(rHull);
			CPoint ptCenter;
			ptCenter.x = (rHull.left + rHull.right)/2;
			ptCenter.y = (rHull.top  + rHull.bottom)/2;
			rHull.SetRect(ptCenter.x-1,ptCenter.y-1, ptCenter.x+1,ptCenter.y+1);
			CenterToRect(&rHull);
			// vyber plochy
			DoSelectPosObject(pArea, smNew, TRUE, TRUE);
		}
	}
}
void CPosEdMainView::OnUpdateGoToNameArea(CCmdUI* pCmdUI)
{
	if (m_pObjsPanel != NULL)
	{
		CPosNameAreaObject* pArea = m_pObjsPanel->GetCurrentNameArea();
		pCmdUI->Enable(pArea!=NULL);
	} else
		pCmdUI->Enable(FALSE); 
}
void CPosEdMainView::OnEditNameArea()
{
  if (m_pEditInfo == NULL || m_pEditInfo->m_pSelObject == NULL)
    return;

  CPosGroupObject* group = (CPosGroupObject*)(m_pEditInfo->m_pSelObject);   
  if (group->GetGroupSize() != 1 )
    return;

  if ((*group)[0]->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)))
  {  
    CPosNameAreaObject* pArea = (CPosNameAreaObject*) (*group)[0];
    if (pArea)    
			Action_EditPoseidonNameArea(pArea,(CPosEdMainDoc*)m_pDocument,NULL);
	}
}
void CPosEdMainView::OnUpdateEditNameArea(CCmdUI* pCmdUI)
{	OnUpdateSelObjNameArea(pCmdUI); }
void CPosEdMainView::OnSelectNameArea()
{

  if (m_pEditInfo == NULL || m_pEditInfo->m_pSelObject == NULL)
    return;

  CPosGroupObject* group = (CPosGroupObject*)(m_pEditInfo->m_pSelObject);   
  if (group->GetGroupSize() != 1 )
    return;

  AfxGetApp()->BeginWaitCursor();
  if ((*group)[0]->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)))
  {  
    CPosNameAreaObject* pArea = (CPosNameAreaObject*) (*group)[0];
    if (pArea)    
			Action_SelPoseidonCurrArea(pArea,TRUE,(CPosEdMainDoc*)m_pDocument,NULL);
	}
  AfxGetApp()->EndWaitCursor();
}
void CPosEdMainView::OnUpdateSelectNameArea(CCmdUI* pCmdUI)
{	OnUpdateSelObjNameArea(pCmdUI); }
void CPosEdMainView::OnSelObjNameArea()
{	
  if (m_pEditInfo == NULL || m_pEditInfo->m_pSelObject == NULL)
    return;

  CPosGroupObject* group = (CPosGroupObject*)(m_pEditInfo->m_pSelObject);   
  if (group->GetGroupSize() != 1 )
    return;

  if ((*group)[0]->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)))
  {  
		CPosNameAreaObject* pArea = (CPosNameAreaObject*) (*group)[0];
		if (pArea)
		{
			CRgn rgn;
			pArea->GetLogObjectPosition()->CreateAreaRgn(rgn);
      EnabledObjects enabled = GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.GetEnableObjects();
			DoSelectPosObjsInRgn(rgn,enabled,smNew, TRUE);
		}
	}
}
void CPosEdMainView::OnUpdateSelObjNameArea(CCmdUI* pCmdUI)
{	
  if (m_pEditInfo == NULL || m_pEditInfo->m_pSelObject == NULL)
  {
    pCmdUI->Enable(FALSE);
    return;
  }

  CPosGroupObject* group = (CPosGroupObject*)(m_pEditInfo->m_pSelObject);   
  if (group->GetGroupSize() != 1 )
  {
    pCmdUI->Enable(FALSE);
    return;
  }   

  pCmdUI->Enable((*group)[0]->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)));  
}

// klic. plocha
void CPosEdMainView::OnGoToKeyPoint()
{
	if (m_pObjsPanel != NULL)
	{
		CPosKeyPointObject* pArea = m_pObjsPanel->GetCurrentKeyPoint();
		if (pArea)
		{
			// prechod na polohu
			CRect  rHull; pArea->GetObjectHull(rHull);
			CPoint ptCenter;
			ptCenter.x = (rHull.left + rHull.right)/2;
			ptCenter.y = (rHull.top  + rHull.bottom)/2;
			rHull.SetRect(ptCenter.x-1,ptCenter.y-1, ptCenter.x+1,ptCenter.y+1);
			CenterToRect(&rHull);
			// vyber plochy
			DoSelectPosObject(pArea, smNew, TRUE, TRUE);
		}
	}
}
void CPosEdMainView::OnUpdateGoToKeyPoint(CCmdUI* pCmdUI)
{
	if (m_pObjsPanel != NULL)
	{
		CPosKeyPointObject* pArea = m_pObjsPanel->GetCurrentKeyPoint();
		pCmdUI->Enable(pArea!=NULL);
	} else
		pCmdUI->Enable(FALSE); 
}
void CPosEdMainView::OnEditKeyPoint()
{
  if (m_pEditInfo == NULL || m_pEditInfo->m_pSelObject == NULL)
    return;

  CPosGroupObject* group = (CPosGroupObject*)(m_pEditInfo->m_pSelObject);   
  if (group->GetGroupSize() != 1 )
    return;

  if ((*group)[0]->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))  
  {
    CPosKeyPointObject* pArea = (CPosKeyPointObject*) (*group)[0];
		if (pArea)
			Action_EditPoseidonKeyPoint(pArea,(CPosEdMainDoc*)m_pDocument,NULL);
	}
}
void CPosEdMainView::OnUpdateEditKeyPoint(CCmdUI* pCmdUI)
{	OnUpdateSelObjKeyPoint(pCmdUI); }
void CPosEdMainView::OnSelectKeyPoint()
{  
  if (m_pEditInfo == NULL || m_pEditInfo->m_pSelObject == NULL)
    return;

  CPosGroupObject* group = (CPosGroupObject*)(m_pEditInfo->m_pSelObject);   
  if (group->GetGroupSize() != 1 )
    return;

  AfxGetApp()->BeginWaitCursor();
  if ((*group)[0]->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))  
  {
    CPosKeyPointObject* pArea = (CPosKeyPointObject*) (*group)[0];
    if (pArea)
			Action_SelPoseidonCurrArea(pArea,TRUE,(CPosEdMainDoc*)m_pDocument,NULL);
	}
  AfxGetApp()->EndWaitCursor();
}
void CPosEdMainView::OnUpdateSelectKeyPoint(CCmdUI* pCmdUI)
{	OnUpdateSelObjKeyPoint(pCmdUI); }
void CPosEdMainView::OnSelObjKeyPoint()
{	
  if (m_pEditInfo == NULL || m_pEditInfo->m_pSelObject == NULL)
    return;

  CPosGroupObject* group = (CPosGroupObject*)(m_pEditInfo->m_pSelObject);   
  if (group->GetGroupSize() != 1 )
    return;

  if ((*group)[0]->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))  
	{
		CPosKeyPointObject* pArea = (CPosKeyPointObject*) (*group)[0];
		if (pArea)
		{
			CRgn rgn;
      EnabledObjects enabled = GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.GetEnableObjects();
			rgn.CreateEllipticRgnIndirect(&(pArea->GetObjectHull()));
			DoSelectPosObjsInRgn(rgn,enabled,smNew, TRUE);
		}
	}
}
void CPosEdMainView::OnUpdateSelObjKeyPoint(CCmdUI* pCmdUI)
{	
  if (m_pEditInfo == NULL || m_pEditInfo->m_pSelObject == NULL)
  {
    pCmdUI->Enable(FALSE);
    return;
  }

  CPosGroupObject* group = (CPosGroupObject*)(m_pEditInfo->m_pSelObject);   
  if (group->GetGroupSize() != 1 )
  {
    pCmdUI->Enable(FALSE);
    return;
  }   

  pCmdUI->Enable((*group)[0]->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)));  
}

// pojmenovan� objekt
void CPosEdMainView::OnGoToNameObject()
{
	if (m_pObjsPanel != NULL)
	{
		CPosEdObject* pObj = m_pObjsPanel->GetCurrentNameObjc();
		if (pObj)
		{
			// prechod na polohu
			CRect  rHull; pObj->GetObjectHull(rHull);
			CPoint ptCenter;
			ptCenter.x = (rHull.left + rHull.right)/2;
			ptCenter.y = (rHull.top  + rHull.bottom)/2;
			rHull.SetRect(ptCenter.x-1,ptCenter.y-1, ptCenter.x+1,ptCenter.y+1);
			CenterToRect(&rHull);
			// vyber plochy
			DoSelectPosObject(pObj, smNew, TRUE, TRUE);
		}
	}
};

void CPosEdMainView::OnUpdateGoToNameObject(CCmdUI* pCmdUI)
{
	if (m_pObjsPanel != NULL)
	{
		CPosEdObject* pObj = m_pObjsPanel->GetCurrentNameObjc();
		pCmdUI->Enable(pObj!=NULL);
	} else
		pCmdUI->Enable(FALSE); 
};


/////////////////////////////////////////////////////////////////////////////
// CPosEdMainView message handlers

/////////////////////////////////////////////////////////////////////////////
// menu VIEW - nastaven� m���tka
// nastaven� m���tka
void CPosEdMainView::OnZoomIn()
{
  if (CanZoomIn())
    SetMouseMode(MM_ZOOM_IN);
};
void CPosEdMainView::OnUpdateZoomIn(CCmdUI* pCmdUI)
{	pCmdUI->Enable(CanZoomIn()); }

void CPosEdMainView::OnZoomOut()
{
  OnViewScaleSpecOut();
};
void CPosEdMainView::OnUpdateZoomOut(CCmdUI* pCmdUI)
{	pCmdUI->Enable(CanZoomOut()); }



void CPosEdMainView::OnViewZoomFit() 
{	OnSetScale(SS_ISOTROPICFIT); }
void CPosEdMainView::OnUpdateViewZoomFit(CCmdUI* pCmdUI) 
{	OnUpdateSetScale(SS_ISOTROPICFIT,pCmdUI); }

void CPosEdMainView::OnViewScale10() 
{	OnSetScale(10,TRUE); }
void CPosEdMainView::OnUpdateViewScale10(CCmdUI* pCmdUI) 
{	OnUpdateSetScale(10,pCmdUI); }

void CPosEdMainView::OnViewScale25() 
{	OnSetScale(25,TRUE); }
void CPosEdMainView::OnUpdateViewScale25(CCmdUI* pCmdUI) 
{	OnUpdateSetScale(25,pCmdUI); }

void CPosEdMainView::OnViewScale50() 
{	OnSetScale(50,TRUE); }
void CPosEdMainView::OnUpdateViewScale50(CCmdUI* pCmdUI) 
{	OnUpdateSetScale(50,pCmdUI); }

void CPosEdMainView::OnViewScale75() 
{	OnSetScale(75,TRUE); }
void CPosEdMainView::OnUpdateViewScale75(CCmdUI* pCmdUI) 
{	OnUpdateSetScale(75,pCmdUI); }

void CPosEdMainView::OnViewScale100() 
{	OnSetScale(100,TRUE); }
void CPosEdMainView::OnUpdateViewScale100(CCmdUI* pCmdUI) 
{	OnUpdateSetScale(100,pCmdUI); }

void CPosEdMainView::OnViewScale150() 
{	OnSetScale(150,TRUE); }
void CPosEdMainView::OnUpdateViewScale150(CCmdUI* pCmdUI) 
{	OnUpdateSetScale(150,pCmdUI); }

void CPosEdMainView::OnViewScale200() 
{	OnSetScale(200,TRUE); }
void CPosEdMainView::OnUpdateViewScale200(CCmdUI* pCmdUI) 
{	OnUpdateSetScale(200,pCmdUI); }

void CPosEdMainView::OnViewScale250() 
{	OnSetScale(250,TRUE); }
void CPosEdMainView::OnUpdateViewScale250(CCmdUI* pCmdUI) 
{	OnUpdateSetScale(250,pCmdUI); }

void CPosEdMainView::OnViewScale500() 
{	OnSetScale(500,TRUE); }
void CPosEdMainView::OnUpdateViewScale500(CCmdUI* pCmdUI) 
{	OnUpdateSetScale(500,pCmdUI); }

void CPosEdMainView::OnViewScaleUser() 
{	OnUserSetScale(); }
void CPosEdMainView::OnUpdateViewScaleUser(CCmdUI* pCmdUI) 
{	pCmdUI->Enable(TRUE); }

void CPosEdMainView::OnViewScaleSpecIn()
{
  POINT CursorPos; 
  CPoint cCursorPos1;
  BOOL bSucc = GetCursorPos(&CursorPos);
  ScreenToClient(&CursorPos);

  CRect rClient;
  GetClientRect(&rClient);

  BOOL bCenterToMouse = TRUE;
  if (!rClient.PtInRect(CursorPos))
  {
    // center to screen center
    rClient = GetLogClientRect();
    bCenterToMouse = FALSE;
  }
  else
  {
    // center to mouse point
    cCursorPos1 = CursorPos;
    cCursorPos1 += GetDeviceScrollPosition();
    cCursorPos1 = FromDeviceToScrollPosition(cCursorPos1);
  }
  
	double nScale = (double)m_nScale;
	if (m_nScale == SS_ISOTROPICFIT)
	{	
		nScale = max(m_szTotalDev.cx* SCALE_UNIT/m_szTotalRDev.cx , 1);    
	};
	nScale *= GetPosEdEnvironment()->m_optPosEd.m_nInOutScaleStep;
	if (m_nMaxScale < (int)Round(nScale,0))
  {
    if (m_nScale >= m_nMaxScale)
      return;
		nScale = m_nMaxScale;
  }

	OnSetScale((int)Round(nScale,0),TRUE);
	
  if (bCenterToMouse)
  {
    CPoint cCursorPos2(CursorPos);
    cCursorPos2 += GetDeviceScrollPosition();
    cCursorPos2 = FromDeviceToScrollPosition(cCursorPos2);
    CPoint cSrollCenter = GetScrollPosition() - cCursorPos2 + cCursorPos1;

    ScrollToPosition(cSrollCenter);
  }
  else
  {
	  rClient.left = (rClient.left+rClient.right)/2-1;
	  rClient.right= rClient.left+2;
	  rClient.top  = (rClient.top+rClient.bottom)/2-1;
	  rClient.bottom= rClient.top+2;
	  CenterToRect(&rClient);
  }
}
void CPosEdMainView::OnViewScaleSpecOut()
{	
  POINT CursorPos; 
  CPoint cCursorPos1;
  BOOL bSucc = GetCursorPos(&CursorPos);
  ScreenToClient(&CursorPos);

  CRect rClient;
  GetClientRect(&rClient);

  BOOL bCenterToMouse = TRUE;
  if (!rClient.PtInRect(CursorPos))
  {
    // center to screen center
    rClient = GetLogClientRect();
    bCenterToMouse = FALSE;
  }
  else
  {
    // center to mouse point
    cCursorPos1=CursorPos;
    cCursorPos1 += GetDeviceScrollPosition();
    cCursorPos1 = FromDeviceToScrollPosition(cCursorPos1);
  }

	double nScale = (double)m_nScale;
	if (m_nScale == SS_ISOTROPICFIT)
	  return;

	nScale /= GetPosEdEnvironment()->m_optPosEd.m_nInOutScaleStep;

  int nMinScale = GetMinScale();
	if (nMinScale >= (int)Round(nScale,0))
  {
		//nScale = SS_ISOTROPICFIT;
    OnSetScale(SS_ISOTROPICFIT,TRUE);
    return;
  }

	OnSetScale((int)Round(nScale,0),TRUE);
	// st�ed
  if (bCenterToMouse)
  {
    CPoint cCursorPos2(CursorPos);
    cCursorPos2 += GetDeviceScrollPosition();
    cCursorPos2 = FromDeviceToScrollPosition(cCursorPos2);
    CPoint cSrollCenter = GetScrollPosition() - cCursorPos2 + cCursorPos1;

    ScrollToPosition(cSrollCenter);
  }
  else
  {
    rClient.left = (rClient.left+rClient.right)/2-1;
    rClient.right= rClient.left+2;
    rClient.top  = (rClient.top+rClient.bottom)/2-1;
    rClient.bottom= rClient.top+2;
    CenterToRect(&rClient);
  }
}
void CPosEdMainView::OnUpdateViewScaleSpec(CCmdUI* pCmdUI)
{	pCmdUI->Enable(TRUE); }

void CPosEdMainView::OnViewAreaMoveLeft()
{	
	CRect rClient = GetLogClientRect();
	if (rClient.left <= m_rAreaLog.left)
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	// posun
	double nPom = (double)rClient.Width();
		   nPom*= (double)(GetPosEdEnvironment()->m_optPosEd.m_nMoveWindowCoef); 
		   nPom/= 100.;
	CPoint pt   = GetScrollPosition();
	pt.x = max(m_rAreaLog.left,pt.x-(int)nPom);
	ScrollToPosition(pt);
}
void CPosEdMainView::OnViewAreaMoveRight()
{	
	CRect rClient = GetLogClientRect();
	if (rClient.right >= m_rAreaLog.right)
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	// posun
	double nPom = (double)rClient.Width();
		   nPom*= (double)(GetPosEdEnvironment()->m_optPosEd.m_nMoveWindowCoef); 
		   nPom/= 100.;
	CPoint pt   = GetScrollPosition();
	pt.x = min(m_rAreaLog.right,pt.x+(int)nPom);
	ScrollToPosition(pt);
}
void CPosEdMainView::OnViewAreaMoveTop()
{	
	CRect rClient = GetLogClientRect();
	if (rClient.top <= m_rAreaLog.top)
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	// posun
	double nPom = (double)rClient.Height();
		   nPom*= (double)(GetPosEdEnvironment()->m_optPosEd.m_nMoveWindowCoef); 
		   nPom/= 100.;
	CPoint pt   = GetScrollPosition();
	pt.y = max(m_rAreaLog.top,pt.y-(int)nPom);
	ScrollToPosition(pt);
}
void CPosEdMainView::OnViewAreaMoveBottom()
{	
	CRect rClient = GetLogClientRect();
	if (rClient.bottom >= m_rAreaLog.bottom)
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	// posun
	double nPom = (double)rClient.Height();
		   nPom*= (double)(GetPosEdEnvironment()->m_optPosEd.m_nMoveWindowCoef); 
		   nPom/= 100.;
	CPoint pt   = GetScrollPosition();
	pt.y = min(m_rAreaLog.bottom,pt.y+(int)nPom);
	ScrollToPosition(pt);
}
void CPosEdMainView::OnUpdateViewAreaMove(CCmdUI* pCmdUI)
{	pCmdUI->Enable(TRUE); }

/////////////////////////////////////////////////////////////////////////////
// pr�ce s my��

void CPosEdMainView::OnMButtonDown(UINT nFlags, CPoint point) 
{
	// synchronizace kurzoru
	CPoint ptMouse = WindowToScrollerPosition( point );
	if (m_rAreaLog.PtInRect(ptMouse))
		GetDocument()->m_CursorObject.SetCursorLogPosition(ptMouse,FALSE);
	else
		MessageBeep(MB_DEFAULT);
	
	CFMntScrollerView::OnMButtonDown(nFlags, point);
}

void CPosEdMainView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	// show info about object
	switch(GetMouseMode())
	{
	case MM_NORMAL:
		{
			// synchronizace kurzoru
			CPoint ptMouse = WindowToScrollerPosition(point);
			CSize szDif = GetLogInMM();
			CPosObject* pObject = m_pPoseidonMap->GetObjectAt(ptMouse, max(5, szDif.cx), GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.GetEnableObjects());

			if (pObject != NULL) DoEditObject(&point);
			return;
		}
		case MM_BGIMAGE_AREA_ADD:
		{
			CPoint logMouse = WindowToScrollerPosition(point);
      
			CPosBackgroundImageObject* pSelBGImage;
			if (!m_SelBGImageID.IsEmpty() && (pSelBGImage =  m_pPoseidonMap->GetBgImage(m_SelBGImageID)))
			{
				CRect rAreaAdd = pSelBGImage->m_LogPosition.m_Position;
        
				if (rAreaAdd.PtInRect(logMouse))
				{
					Action_EditBGImage(pSelBGImage, GetDocument());
					return;
				}
			}

			CSize szDif = GetLogInMM();
			CPosBackgroundImageObject* pBGImage = m_pPoseidonMap->GetBgImageAt(logMouse,max(5,szDif.cx),&GetPosEdEnvironment()->m_optPosEd.m_ViewStyle);
			Action_EditBGImage(pBGImage, GetDocument());
			return;
		}
	default:
		CFMntScrollerView::OnLButtonDblClk(nFlags, point);
	}
}

BOOL CPosEdMainView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	if ((pWnd==this)&&(nHitTest==HTCLIENT))
	{ 
    if (_activeExEditor)
    {
      CPoint pt(GetMessagePos());
      ScreenToClient(&pt);
      HCURSOR c=_activeExEditor->OnSetCursor(pt);
      if (c!=0)
      {
        SetCursor(c);
        return TRUE;
      }
    }
    // upravuji pouze v Client plo�e
		HCURSOR hCursor = NULL;
		switch (GetMouseMode())
    {
    case MM_AREA_ADD:
    case MM_AREA_CRT:
    case MM_VERTEX_AREA_ADD:
      {	// v�b�r plochy
        hCursor = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_MSCR_RECT));
        ::SetCursor(hCursor);
        return TRUE;;
      }
    case MM_BGIMAGE_AREA_ADD:
      {
        if (m_SelBGImageID.IsEmpty())
        {
          hCursor = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_MSCR_RECT));
          ::SetCursor(hCursor);
          return TRUE;;
        }

        CPosBackgroundImageObject * pSelBGImage;
        if (!m_SelBGImageID.IsEmpty() && (pSelBGImage = m_pPoseidonMap->GetBgImage(m_SelBGImageID)))
        {
          CPoint point; GetCursorPos(&point);
          pWnd->ScreenToClient(&point);            
          CRect rect = FromScrollToDevicePosition(pSelBGImage->m_LogPosition.m_Position);

          if (UserTrackRectOnSetCursor(rect, point))
            return TRUE;
        }
        
        hCursor = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_MSCR_RECT));
        ::SetCursor(hCursor);          
        
        return TRUE;;
      }
    default:
      return CFMntScrollerView::OnSetCursor(pWnd, nHitTest, message);
    }
  }
  return CFMntScrollerView::OnSetCursor(pWnd, nHitTest, message);
}

void CPosEdMainView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	switch (GetMouseMode())
	{
	case MM_AREA_ADD:
	case MM_AREA_CRT:
		{	
			// v�b�r plochy (blok)
			BOOL bControl = (nFlags & MK_CONTROL) != 0;
			BOOL bShift = (nFlags & MK_SHIFT) != 0;

			CRect rAreaAdd;
			BOOL  bOldAlignGrid = m_bAlignGrid;
			m_bAlignGrid  = TRUE;

			CPoint allignSize;
			if (m_pObjsPanel->GetTypeObjects() == CPosObjectsPanel::grTypeTxtr)
			{
				allignSize.SetPoint(m_pPoseidonMap->GetTextureLogSize(), m_pPoseidonMap->GetTextureLogSize());
			}
			else
			{
				allignSize.SetPoint(VIEW_LOG_UNIT_SIZE, VIEW_LOG_UNIT_SIZE);
			}

			if (GetUserTrackRectNew(rAreaAdd, point, TRUE, TRUE, fstAlignSquare, allignSize, CPoint(0, 0), TRUE))
			{	
				if ((rAreaAdd.Width() != 0) && (rAreaAdd.Height() != 0))
				{	          
					// vytvo��m akci
					Action_AddPoseidonCurrArea(rAreaAdd, !bControl, bShift, (CPosEdMainDoc*)m_pDocument, NULL);          
				}
				else
				{
					if (GetDocument()->m_CurrentArea.IsValidArea())
					{
						CPoint cMouse = WindowToScrollerPosition(point);	
						if (GetDocument()->m_CurrentArea.IsPointAtArea(cMouse))
						{
							CreateLandObjectOnMouse(nFlags, point);
						}
						else
						{
							Action_DelPoseidonCurrArea((CPosEdMainDoc*)m_pDocument, NULL);
						}
					}
					else
					{
						CreateObjectOnMouse(nFlags, point);
					}
				}
			}

			m_bAlignGrid = bOldAlignGrid;      
			//OnCancelMouseMode();
			break;
		}
	case MM_VERTEX_AREA_ADD:  
		{	
			// v�b�r plochy (blok)
			BOOL bControl = (nFlags & MK_CONTROL) != 0;
			BOOL bShift = (nFlags & MK_SHIFT) != 0;

			CRect rAreaAdd;
			BOOL bOldAlignGrid = m_bAlignGrid;
			m_bAlignGrid  = TRUE;
			if (GetUserTrackRectNew(rAreaAdd, point, TRUE, TRUE, fstAlignSquare, CPoint(VIEW_LOG_UNIT_SIZE, VIEW_LOG_UNIT_SIZE), CPoint(VIEW_LOG_UNIT_SIZE / 2, VIEW_LOG_UNIT_SIZE / 2), TRUE))
			{	
				if ((rAreaAdd.Width() != 0) && (rAreaAdd.Height() != 0))
				{	
					// vytvo��m akci
					Action_AddPoseidonCurrArea(rAreaAdd, !bControl, bShift, (CPosEdMainDoc*)m_pDocument, NULL);          
				}
				else
				{
					if (GetDocument()->m_CurrentArea.IsValidArea())
					{
						CPoint cMouse = WindowToScrollerPosition(point);	
						if (GetDocument()->m_CurrentArea.IsPointAtArea(cMouse))
						{
							CreateLandObjectOnMouse(nFlags, point);
						}
						else
						{
							Action_DelPoseidonCurrArea((CPosEdMainDoc*)m_pDocument, NULL);
						}
					}
					else
					{
						CreateObjectOnMouse(nFlags, point);
					}
				}
			}

			m_bAlignGrid = bOldAlignGrid;      
			//OnCancelMouseMode();
			break;
		}
	case MM_NORMAL:
		{  		
			if (nFlags & MK_CONTROL || nFlags & MK_SHIFT)
			{	
				// kombinovan� v�b�r objekt�
				CPoint ptMouse = WindowToScrollerPosition(point);
				CSize szDif    = GetLogInMM();

				CPosObject* pObject = m_pPoseidonMap->GetObjectAt(ptMouse, max(5, szDif.cx), GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.GetEnableObjects());
				if (pObject)
				{     
					if (m_pEditInfo != NULL && m_pEditInfo->m_pSelTracker && 
						m_pEditInfo->m_pSelTracker->HitTest(point) != CFMntScrollerTracker::hitNothing && (nFlags & MK_CONTROL))
					{
						CFMntScrollerView::OnLButtonDown(nFlags, point);
					}
					else
					{
						((CPosEdMainDoc *) m_pDocument)->m_CursorObject.SetCursorLogPosition(ptMouse, FALSE);
						DoSelectPosObject(pObject,(nFlags & MK_CONTROL) ? smAdd : smDel, TRUE, TRUE);
					}
				} 
				else
				{	
					// v�b�r v rectu
					CRect rArea;
					if (GetUserTrackRectNew(rArea, point, TRUE, FALSE))
					{
						if ((rArea.Width( )> MAX_MOUSE_CLICK_MOVE) && (rArea.Height() > MAX_MOUSE_CLICK_MOVE))
						{   
							// v�b�r objekt� v rectu
							((CPosEdMainDoc *) m_pDocument)->m_CursorObject.SetCursorLogPosition(rArea.CenterPoint(), FALSE);
							EnabledObjects enabled = GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.GetEnableObjects();
							DoSelectPosObjsInRect(rArea, enabled, (nFlags & MK_CONTROL) ? smAdd : smDel, TRUE);
							return;
						}
					}
	          
					CreateObjectOnMouse(nFlags, point);
				}
			}
			else        
			{
				CPoint ptMouse = WindowToScrollerPosition(point);
				CSize szDif    = GetLogInMM();

				CPosObject* pObject = m_pPoseidonMap->GetObjectAt(ptMouse, max(5, szDif.cx), GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.GetEnableObjects());

				if (pObject != NULL)	
				{	
					// If selected move objects
					//if (m_EditObjsParams.IsObjSelected(*pObject))
					if (m_pEditInfo != NULL && m_pEditInfo->m_pSelTracker && 
						m_pEditInfo->m_pSelTracker->HitTest(point) != CFMntScrollerTracker::hitNothing)
					{
						CFMntScrollerView::OnLButtonDown(nFlags, point);
					}
					else
					{
						((CPosEdMainDoc *) m_pDocument)->m_CursorObject.SetCursorLogPosition(ptMouse, FALSE);
						DoSelectPosObject(pObject, smNew, TRUE, TRUE); //CFMntScrollerView::OnLButtonDown(nFlags, point);
					}
					break;
				}            

				CRect rArea;
				if (GetUserTrackRectNew(rArea, point, TRUE, FALSE))
				{
					if ((rArea.Width() > MAX_MOUSE_CLICK_MOVE) && (rArea.Height() > MAX_MOUSE_CLICK_MOVE))
					{   
						// v�b�r objekt� v rectu
						((CPosEdMainDoc *) m_pDocument)->m_CursorObject.SetCursorLogPosition(rArea.CenterPoint(), FALSE);
						EnabledObjects enabled = GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.GetEnableObjects();
						DoSelectPosObjsInRect(rArea, enabled, smNew, TRUE);
					}
					else
					{
						if (m_pEditInfo != NULL && m_pEditInfo->m_pSelTracker)
						{
							// deselect object
							DoUnselectPosObject();
						}
						else              
						{
							CreateObjectOnMouse(nFlags, point);
						}
					}
				}
			}         
			break;
		}
	case MM_BGIMAGE_AREA_ADD:
		{	
			// v�b�r plochy (blok)
			BOOL bControl = (nFlags & MK_CONTROL) != 0;
			BOOL bShift = (nFlags & MK_SHIFT) != 0;

			CRect rAreaAdd(0, 0, 0, 0);
			BOOL bOldAlignGrid = m_bAlignGrid;
			m_bAlignGrid = TRUE;

			CPosBackgroundImageObject* pSelBGImage;
			if (!m_SelBGImageID.IsEmpty() && (pSelBGImage =  m_pPoseidonMap->GetBgImage(m_SelBGImageID)))
			{
				rAreaAdd = pSelBGImage->m_LogPosition.m_Position;

				CPoint logMouse  = WindowToScrollerPosition(point);
				if (rAreaAdd.PtInRect(logMouse))
				{
					if (GetUserTrackRect(rAreaAdd, point, TRUE, TRUE, fstAlignNone, TRUE))
					{
						//Edit bitmap according to new rect
						FltRect rect = m_pPoseidonMap->FromViewToRealRect(rAreaAdd);
						Action_EditBGImage(pSelBGImage, rect, GetDocument());

					}
					m_bAlignGrid = bOldAlignGrid;      
					//OnCancelMouseMode();
					break;
				}
			}

			if (GetUserTrackRectNew(rAreaAdd, point, TRUE, TRUE, fstAlignNone, TRUE))
			{	
				CRect rDevArea = FromScrollToDevicePosition(rAreaAdd);

				if ((rDevArea.Width() <= MAX_MOUSE_CLICK_MOVE) && (rDevArea.Height() <= MAX_MOUSE_CLICK_MOVE))
				{
					// if there is a BGImage area and we are in one create or edit image
					CPoint ptMouse = WindowToScrollerPosition(point);
					CSize szDif = GetLogInMM();

					CPosBackgroundImageObject* pBGImage = m_pPoseidonMap->GetBgImageAt(ptMouse,max(5,szDif.cx),&GetPosEdEnvironment()->m_optPosEd.m_ViewStyle);
					if (pBGImage != NULL)
					{
						ChangeSelBGImage(pBGImage->m_sAreaName);                        
					}
					else
					{
						if (!m_SelBGImageID.IsEmpty())
						{
							ChangeSelBGImage(CString());
						}
						else
						{          
							REALPOS rP = m_pPoseidonMap->FromViewToRealPos(ptMouse);  
							FltRect rect;
#define DEFUALT_BGIMAGE_RECT_SIZE 300;
							rect.left = rP.x - DEFUALT_BGIMAGE_RECT_SIZE;
							rect.left = max(rect.left, 0);
							rect.top = rP.z - DEFUALT_BGIMAGE_RECT_SIZE;
							rect.top = max(rect.top, 0);

							FMntVector2 size = m_pPoseidonMap->GetRealSize();
							rect.right = rP.x + DEFUALT_BGIMAGE_RECT_SIZE;
							rect.right = min(rect.right, size[0]);
							rect.bottom = rP.z + DEFUALT_BGIMAGE_RECT_SIZE;
							rect.bottom = min(rect.bottom, size[1]);            

							Action_CreateBGImage((CPosEdMainDoc*) m_pDocument, &rect);
						}
					}
				}      
				else
				{
					FltRect rect = m_pPoseidonMap->FromViewToRealRect(rAreaAdd);
					Action_CreateBGImage((CPosEdMainDoc*) m_pDocument, &rect);          
				}
			}

			m_bAlignGrid = bOldAlignGrid;      
			//OnCancelMouseMode();
			break;
		}
	default:
		CFMntScrollerView::OnLButtonDown(nFlags, point);
	}
}

void CPosEdMainView::CreateLandObjectOnMouse(UINT nFlags, CPoint point) 
{
  CPoint ptMouse = WindowToScrollerPosition( point );	

  // je na vybran�m objektu ?
  if (m_rAreaLog.PtInRect(ptMouse) && m_pObjsPanel &&
    GetDocument()->m_CurrentArea.IsValidArea() &&    
    GetDocument()->m_CurrentArea.IsPointAtArea(ptMouse))      
  {
    switch( m_pObjsPanel->GetTypeObjects())
    {
    case CPosObjectsPanel::grTypeTxtr:
      // vlo��m texturu
      //OnFillAreaByTxtr(); // removed on Rasta request
      return;
    case CPosObjectsPanel::grTypeWood:
      OnCreateWood();
      return;
    case CPosObjectsPanel::grTypeName:
      OnCreateNameArea();
      return;      
    default:;
    };
  }	
}

extern BOOL DoPreGenerateNetFrom(CPosNetObjectKey* pFrom,CPoint ptTo,CPosEdMainDoc* pDocument,BOOL bParams);

void CPosEdMainView::CreateObjectOnMouse(UINT nFlags, CPoint point) 
{
	CPoint ptMouse = WindowToScrollerPosition(point);	
	// je na vybran�m objektu ?
	if (m_EditObjsParams.m_pSelObject != NULL)
	{		
		// sit�
		if(m_pEditParams->m_pSelObject->IsKindOf(RUNTIME_CLASS(CPosGroupObject)))
		{
			CPosGroupObject * group = (CPosGroupObject *) m_pEditParams->m_pSelObject;
			if (group->GetGroupSize() == 1 && (*group)[0]->IsKindOf(RUNTIME_CLASS(CPosNetObjectKey)))
			{  
				if (((CPosNetObjectKey *)(*group)[0])->Locked()) return;

				BOOL bParams = ((nFlags & MK_SHIFT) != 0) && ((nFlags & MK_CONTROL) != 0);
				DoPreGenerateNetFrom((CPosNetObjectKey*)((*group)[0]), ptMouse, (CPosEdMainDoc*)m_pDocument, bParams);
				return;
			}
		}
	}

	if ((m_rAreaLog.PtInRect(ptMouse)) && (m_pObjsPanel))
	{
		switch( m_pObjsPanel->GetTypeObjects())
		{		
		case CPosObjectsPanel::grTypeTxtr:
			{	// vlo��m texturu
				CPosTextureTemplate* pTxtr = m_pObjsPanel->GetCurrentTexture();
				if (pTxtr != NULL)
				{
					Action_CreatePoseidonTexture(ptMouse, pTxtr, (CPosEdMainDoc*)m_pDocument, NULL);
					return;
				}
			}
			break;
		case CPosObjectsPanel::grTypeNtOb:
		case CPosObjectsPanel::grTypePlOb:
#ifndef __ONLY_FOR_PUBLIC__
		case CPosObjectsPanel::grTypeNROb:
#endif
			{	// vlo��m objekt
				CPosObjectTemplate* pTempl = m_pObjsPanel->GetCurrentObject();
				if (pTempl)
				{
					if (nFlags & MK_SHIFT) 
					{
						Action_CreatePoseidonObjectMulti(ptMouse, pTempl, (CPosEdMainDoc*)m_pDocument, NULL);
					}
					else
					{
						Action_CreatePoseidonObject(ptMouse, pTempl, (CPosEdMainDoc*)m_pDocument, NULL);
					}
					return;
				}
			}
			break;
		case CPosObjectsPanel::grTypeNets:
			{	// vytvo�en� kl��ov�ho bodu s�t�
				CPosNetTemplate* pTempl = m_pObjsPanel->GetCurrentNet();
				// vytv���m i kdy� je NULL
				Action_CreatePoseidonKeyNet(ptMouse, pTempl, (CPosEdMainDoc*)m_pDocument, NULL);
				return;
			}
			break;		
		case CPosObjectsPanel::grTypeKPla:
			{
				Action_CreatePoseidonKeyPoint(ptMouse, (CPosEdMainDoc*)m_pDocument, NULL);
				return;
			}
			break;
		default:;      
		}
	}	
}

BOOL CPosEdMainView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
  if (zDelta < 0)
    OnViewScaleSpecIn();
  else
    OnViewScaleSpecOut();

  return CFMntScrollerView::OnMouseWheel(nFlags, zDelta, pt);
}

void CPosEdMainView::OnBuldConnect()
{
  CRect client = GetLogClientRect();

  ((CPosEdMainDoc *) m_pDocument)->m_CursorObject.SetCursorLogPosition( client.CenterPoint(), FALSE);
}

void CPosEdMainView::OnBuldDisconnect()
{
}

void CPosEdMainView::CancelMouseMode()
{
	int iType = m_pObjsPanel->GetTypeObjects();

	switch (iType)
	{
	case CPosObjectsPanel::grTypeTxtr:
	case CPosObjectsPanel::grTypeWood:
	case CPosObjectsPanel::grTypeName:
		{
			SetMouseMode(MM_AREA_CRT);
		}
		break;
	case CPosObjectsPanel::grTypeNtOb:
	case CPosObjectsPanel::grTypePlOb:  
#ifndef __ONLY_FOR_PUBLIC__
	case CPosObjectsPanel::grTypeNROb:  
#endif
	case CPosObjectsPanel::grTypeNets:
  
	case CPosObjectsPanel::grTypeKPla:
	case CPosObjectsPanel::grTypeNObj:
		{
			SetMouseMode(MM_NORMAL);
		} 
		break;
	case CPosObjectsPanel::grTypeLand:
		{
			SetMouseMode(MM_VERTEX_AREA_ADD);
		}
		break;
	case CPosObjectsPanel::grTypeBgImage:
		{
			SetMouseMode(MM_BGIMAGE_AREA_ADD);      
		}
		break;
	} 

	// show/hide land selection and bgImage selection

	CPosSelAreaObject& area = GetDocument()->m_CurrentArea;
	if (area.IsValidArea())
	{
		CRect hull;
		area.GetObjectHull(hull);
		InvalidateLogRect(hull);
	}

	CPosBackgroundImageObject * pBGImage; 
	if (!m_SelBGImageID.IsEmpty() && (pBGImage = m_pPoseidonMap->GetBgImage(m_SelBGImageID)))
	{
		InvalidateLogRect(pBGImage->m_LogPosition.m_Position);
	}
}

void CPosEdMainView::OnPickTexture()
{
  POINT pt;
  if (!GetCursorPos(&pt))
    return;

  ScreenToClient(&pt);
  CRect rect;
  GetClientRect(rect);
  if(!rect.PtInRect(pt))
    return;

  CPoint point = WindowToScrollerPosition(CPoint(pt));
  REALPOS rPos  = m_pPoseidonMap->FromViewToRealPos(point);
  UNITPOS rUnit = m_pPoseidonMap->FromRealToTextureUnitPos(rPos);	

  CPosTextureTemplate* pCurr = m_pPoseidonMap->GetBaseTextureAt(rUnit);
  if (pCurr != NULL)
    m_pObjsPanel->SetCurrentTexture(pCurr);
}

void CPosEdMainView::OnPickObject()
{
	POINT pt;
  
	if (!GetCursorPos(&pt)) return;

	ScreenToClient(&pt);
	CRect rect;
	GetClientRect(rect);
	if(!rect.PtInRect(pt)) return;

	CPoint point = WindowToScrollerPosition(CPoint(pt));

	CSize  szDif   = GetLogInMM();
	CPosObject* pObject = m_pPoseidonMap->GetObjectAt(point, max(5, szDif.cx), GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.GetEnableObjects());

	if (pObject == NULL) return;

	if (pObject->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
	{
		const Ref<CPosObjectTemplate> pTemplate = GetDocument()->GetObjectTemplate(((CPosEdObject *)pObject)->m_nTemplateID);

		if (pTemplate != NULL) m_pObjsPanel->SetCurrentObject(pTemplate);
		return;
	}

	if (pObject->IsKindOf(RUNTIME_CLASS(CPosWoodObject)))
	{
		CPosWoodTemplate * pTemplate = ((CPosWoodObject *)pObject)->GetWoodTemplate();
		
		if (pTemplate != NULL) m_pObjsPanel->SetCurrentWood(pTemplate);
		return;
	}

	if (pObject->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))
	{    
		m_pObjsPanel->SetCurrentKeyPoint((CPosKeyPointObject *) pObject);
		return;
	}

	if (pObject->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)))
	{    
		m_pObjsPanel->SetCurrentNameArea((CPosNameAreaObject *) pObject);
		return;
	}

	if (pObject->IsKindOf(RUNTIME_CLASS(CPosNetObjectKey)))
	{  
		CPosNetObjectKey * key = (CPosNetObjectKey *) pObject;

		if (key->m_pBasePartKEY == NULL) return;

		if (!key->m_pBasePartKEY->m_nTemplateName.IsEmpty())
		{
			m_pObjsPanel->SetCurrentNet(key->m_pBasePartKEY->m_nTemplateName);
			return;
		}

		// Crossroads has different template for each part list.
		int n = key->m_PartLists.GetCount();
		for(int i = 0; i < n; ++i)
		{
			CNetPartsList* pList = (CNetPartsList*)(key->m_PartLists[i]);
			if (pList->IsPointAtArea(point))
			{
				m_pObjsPanel->SetCurrentNet(pList->m_nTemplateName);
				return;
			}
		}
		return;
	}
}

void CPosEdMainView::OnUpdateSelLandBuldozer()
{
  if (GetDocument()->RealViewer()) GetDocument()->RealViewer()->SelectionLandClear();

  CPosAreaObject & areaObj = ((CPosEdMainDoc *) m_pDocument)->m_CurrentArea;
  

  AutoArray<SLandSelPosMessData> msgData;

  BOOL bFromLandToVertex = m_pObjsPanel->GetTypeObjects() != CPosObjectsPanel::grTypeLand;
  for(int i = 0; i < areaObj.NRects(); i++)
  {
    CRect rect = areaObj.Rect(i);
    if (bFromLandToVertex)    
      rect += CPoint(VIEW_LOG_UNIT_SIZE/2, VIEW_LOG_UNIT_SIZE/2); 
    
    REALPOS leftTop = m_pPoseidonMap->FromViewToRealPos(rect.TopLeft());
    REALPOS rightBottom = m_pPoseidonMap->FromViewToRealPos(rect.BottomRight());
    float gridLength = GetDocument()->m_nRealSizeUnit/2;

    SLandSelPosMessData& data = msgData.Append();
    data.xs = leftTop.x;
    data.xe = rightBottom.x;
    data.zs = rightBottom.z;
    data.ze = leftTop.z;
   
  }
  if (GetDocument()->RealViewer()) GetDocument()->RealViewer()->BlockSelectionLand(msgData);
}

void CPosEdMainView::SendMovedObjectsToBuldozer(CObArray& ObjToSelect)
{
  AutoArray<SMoveObjectPosMessData> msgData;
  
  for(int i = 0; i < ObjToSelect.GetSize(); i++)
  {
    if (ObjToSelect[i]->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
    {
      SMoveObjectPosMessData& data = msgData.Append();
      ((CPosEdObject *)ObjToSelect[i])->SetObjectDataToMessage(data);
    }  
  }

  if (GetDocument()->RealViewer()) GetDocument()->RealViewer()->BlockMove(msgData);
}

void CPosEdMainView::OnMouseMove(UINT nFlags, CPoint point)
{  
  stRULER_INFO rulInfo;
  rulInfo.DocSize = m_pPoseidonMap->GetRealSize()[0];
  rulInfo.fZoomFactor = m_szTotalDev.cx * 1.0f / rulInfo.DocSize;
  CPoint pt = GetScrollPosition();
  REALPOS real = m_pPoseidonMap->FromViewToRealPos(pt);

  rulInfo.ScrollPos  = real.x;
  CPoint mouseScrollPos = WindowToScrollerPosition(point);
  REALPOS mouseRealPos = m_pPoseidonMap->FromViewToRealPos(mouseScrollPos);

  rulInfo.Pos  = mouseRealPos.x;
  rulInfo.uMessage = RW_POSITION;
  rulInfo.bInverted = FALSE;

  CChildFrame* m_pParent = ((CChildFrame*)GetParentFrame());
  m_pParent->UpdateRulersInfo(rulInfo, RT_HORIZONTAL);

  rulInfo.DocSize = m_pPoseidonMap->GetRealSize()[1];
  rulInfo.fZoomFactor = m_szTotalDev.cy * 1.0f / rulInfo.DocSize;  
  rulInfo.ScrollPos  = real.z;
  rulInfo.Pos  = mouseRealPos.z;
  rulInfo.uMessage = RW_POSITION;
  rulInfo.bInverted = TRUE;

  m_pParent->UpdateRulersInfo(rulInfo, RT_VERTICAL);

  CFMntScrollerView::OnMouseMove(nFlags, point);
}

void CPosEdMainView::OnCreateBgImage()
{  
	Action_CreateBGImage((CPosEdMainDoc*) m_pDocument);
}

void CPosEdMainView::OnRemoveBgImage()
{
  CPosBackgroundImageObject * image = m_pObjsPanel->GetCurrentBGImage();
  if (image != NULL)
  {
    CPosBackgroundImageObject * image2 = (CPosBackgroundImageObject *) image->CreateCopyObject();
    Action_RemoveBGImage(image2, (CPosEdMainDoc*) m_pDocument);
  }
}

void CPosEdMainView::OnEditBgImage()
{
  CPosBackgroundImageObject * image = m_pObjsPanel->GetCurrentBGImage();
  if (image != NULL)
  {    
    Action_EditBGImage(image, (CPosEdMainDoc*) m_pDocument);
  }
}

void CPosEdMainView::OnUpdateOperBgImage(CCmdUI* pCmdUI)
{
  pCmdUI->Enable(m_pObjsPanel->GetCurrentBGImage() != NULL);
}

void CPosEdMainView::ChangeSelBGImage(const CString& selBGImageID)
{
  // Invalide rectangles
  CPosBackgroundImageObject * pBGImage;
  if (!selBGImageID.IsEmpty())
  {
    pBGImage = m_pPoseidonMap->GetBgImage(m_SelBGImageID);
    if (pBGImage)
      InvalidateLogRect(pBGImage->m_LogPosition.m_Position);
    else
      Invalidate();
  }

  m_SelBGImageID = selBGImageID;
  if (!m_SelBGImageID.IsEmpty() && (pBGImage = m_pPoseidonMap->GetBgImage(m_SelBGImageID)) && pBGImage->m_Visible)
    InvalidateLogRect(pBGImage->m_LogPosition.m_Position);
  else
  {
    m_SelBGImageID.Empty();
    Invalidate();
  }

  if (!m_SelBGImageID.IsEmpty())
    m_pObjsPanel->SetCurrentBGImage(m_SelBGImageID);
}

void CPosEdMainView::OnAddTextureLayer()
{
  Action_CreateTextureLayer(GetDocument());
}

void CPosEdMainView::OnUpdateAddTextureLayer(CCmdUI* pCmdUI)
{
  pCmdUI->Enable(TRUE);
}

void CPosEdMainView::OnRemoveTextureLayer()
{
  Action_RemoveTextureLayer(m_pObjsPanel->GetCurrentTextureLayer(),GetDocument());
}

void CPosEdMainView::OnUpdateRemoveTextureLayer(CCmdUI* pCmdUI)
{
  pCmdUI->Enable(m_pPoseidonMap->GetTextureLayersCount() > 1);
}
  
void CPosEdMainView::OnEditTextureLayer()
{
  Action_EditTextureLayer(m_pObjsPanel->GetCurrentTextureLayer(),GetDocument());
}

void CPosEdMainView::OnUpdateEditTextureLayer(CCmdUI* pCmdUI)
{
  pCmdUI->Enable(TRUE);
}

void CPosEdMainView::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CFMntScrollerView::OnActivate(nState, pWndOther, bMinimized);

	m_pObjsPanel->UpdateDocData(GetDocument(), TRUE);
}

// ClipBoard

TypeIsSimple(TextureElem);

void CPosEdMainView::OnClipboardCopy()
{
  AfxGetApp()->BeginWaitCursor();
  int mode = m_pObjsPanel->GetTypeObjects(); 
  //m_ClipboardDataSource.Empty();
 
  switch(mode)
  {
  case CPosObjectsPanel::grTypeLand:
    {
      CPosSelAreaObject& sel = GetDocument()->m_CurrentArea;
      if (!sel.IsValidArea())
      {
        AfxGetApp()->EndWaitCursor();
        return;
      }

      CRect	rLogHull;
      UNITPOS m_LftTop, m_RghBtm;
      rLogHull = sel.GetObjectHull();
      m_pPoseidonMap->GetUnitPosHull(m_LftTop,m_RghBtm,rLogHull);

      // how many points are there
      AutoArray<HeightElem> elems;
      UNITPOS pos;
      REALPOS lefttop;
      lefttop.x = FLT_MAX;
      lefttop.z = FLT_MAX;
      REALPOS rightbottom;
      rightbottom.x = FLT_MIN;
      rightbottom.z = FLT_MIN;

      for(pos.z=m_LftTop.z; pos.z>=m_RghBtm.z; pos.z--)
        for(pos.x=m_LftTop.x; pos.x<=m_RghBtm.x; pos.x++)
        {
          if (sel.IsPointAtArea(m_pPoseidonMap->FromUnitToViewPos(pos)))
          {
            HeightElem& elem = elems.Append();
            elem.height = m_pPoseidonMap->GetLandscapeHeight(pos);
            REALPOS realpos = m_pPoseidonMap->FromUnitToRealPos(pos);
            elem.relrealPos[0] = realpos.x;
            elem.relrealPos[1] = realpos.z;
            lefttop.x = min(realpos.x, lefttop.x);
            lefttop.z = min(realpos.z, lefttop.z);
            rightbottom.x = max(realpos.x, rightbottom.x);
            rightbottom.z = max(realpos.z, rightbottom.z);
          }
        }

      for(int i = 0; i < elems.Size(); i++)
      {
        elems[i].relrealPos[0] -= lefttop.x;
        elems[i].relrealPos[1] -= lefttop.z;
      }

      HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE,sizeof(PosClipBoardHeight) + sizeof(HeightElem) * (elems.Size() - 1));
      PosClipBoardHeight * clipData = (PosClipBoardHeight *) GlobalLock(hGlobal);
      clipData->realGrid = GetDocument()->m_nRealSizeUnit;
      clipData->rectreal[0] = lefttop.x;
      clipData->rectreal[1] = lefttop.z;
      clipData->rectreal[2] = rightbottom.x;
      clipData->rectreal[3] = rightbottom.z;
      clipData->numberOfElems = elems.Size();
      memcpy(clipData->data, elems.Data(), sizeof(HeightElem) * elems.Size());
      GlobalUnlock(hGlobal);
      int iFormat = RegisterClipboardFormat(HEIGHT_FORMAT_NAME);
      COleDataSource * source = new COleDataSource();
      source->CacheGlobalData(iFormat,hGlobal);
      source->SetClipboard();
    
      // no need for delete dataSource is deleted by source->SetClipboard();
      //delete m_ClipboardDataSource;
      m_ClipboardDataSource = source;
    }    
    AfxGetApp()->EndWaitCursor();
    return;  
  case CPosObjectsPanel::grTypeNtOb:
  case CPosObjectsPanel::grTypePlOb:
#ifndef __ONLY_FOR_PUBLIC__
  case CPosObjectsPanel::grTypeNROb:
#endif
  case CPosObjectsPanel::grTypeNets:
  case CPosObjectsPanel::grTypeKPla:
  case CPosObjectsPanel::grTypeNObj:
  case CPosObjectsPanel::grTypeWood:
  case CPosObjectsPanel::grTypeName:     
    {
      int n;
      if ((n = GetDocument()->m_GroupObjects.GetGroupSize()) == 0)
      {
        AfxGetApp()->EndWaitCursor();
        return;
      }

      // first store all files in memory
      CMemFile file;
      CArchive ar(&file, CArchive::store);

      int nNameAreas = 0;
      int nObjs = 0;
      int nNets = 0;
      int nKeys = 0; 
      int nWoods = 0;
      float realHull[4];
      realHull[0] = realHull[1] = FLT_MAX;
      realHull[2] = realHull[3] = FLT_MIN;

      for(int i = 0; i < n; i++)
      {
        CObject * obj = GetDocument()->m_GroupObjects[i];
        if (obj->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)))
        {
          CPosNameAreaObject * nameObj = (CPosNameAreaObject *) obj;          
          nNameAreas++;
          nameObj->PreSerializeStoring(*m_pPoseidonMap);
          nameObj->DoSerialize(ar, POSED_FILE_VER);
          CRect rect;
          nameObj->GetObjectHull(rect);
          FltRect realRect = m_pPoseidonMap->FromViewToRealRect(rect);
          realHull[0] = min(realRect[0], realHull[0]);
          realHull[2] = max(realRect[2], realHull[2]);
          realHull[1] = min(realRect[1], realHull[1]);
          realHull[3] = max(realRect[3], realHull[3]);    
        }
      }

      for(int i = 0; i < n; i++)
      {
        CObject * obj = GetDocument()->m_GroupObjects[i];
        if (obj->IsKindOf(RUNTIME_CLASS(CPosWoodObject)))
        {
          CPosWoodObject * woodObj = (CPosWoodObject *) obj;          
          nWoods++;
          woodObj->DoSerialize(ar, POSED_FILE_VER);
          CRect rect;
          woodObj->GetObjectHull(rect);
          FltRect realRect = m_pPoseidonMap->FromViewToRealRect(rect);
          realHull[0] = min(realRect[0], realHull[0]);
          realHull[2] = max(realRect[2], realHull[2]);
          realHull[1] = min(realRect[1], realHull[1]);
          realHull[3] = max(realRect[3], realHull[3]);    

          // store also all his objects
          int nWoodItems = 0;
          AutoArray<CPosEdObject *> woodItems;
          for(int j = 0; j < m_pPoseidonMap->GetPoseidonObjectCount(); j++)
          {
            CPosEdObject * pObj = m_pPoseidonMap->GetPoseidonObject(j);
            if (pObj && pObj->m_nMasterWood == woodObj->GetID())
            {
              nWoodItems++;
              woodItems.Add(pObj);
            }
          }

          MntSerializeInt(ar, nWoodItems);

          for(int j = 0; j < woodItems.Size(); j++)
          {
            CPosEdObject * pObj = woodItems[j];
            pObj->DoSerialize(ar,POSED_FILE_VER);
          }
        }
      }

      for(int i = 0; i < n; i++)
      {
        CObject * obj = GetDocument()->m_GroupObjects[i];
        if (obj->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))
        {
          CPosKeyPointObject * keyObj = (CPosKeyPointObject *) obj;          
          nKeys++;
          keyObj->DoSerialize(ar, POSED_FILE_VER);
          CRect rect;
          keyObj->GetObjectHull(rect);
          FltRect realRect = m_pPoseidonMap->FromViewToRealRect(rect);
          realHull[0] = min(realRect[0], realHull[0]);
          realHull[2] = max(realRect[2], realHull[2]);
          realHull[1] = min(realRect[1], realHull[1]);
          realHull[3] = max(realRect[3], realHull[3]);                   
        }
      }

      for(int i = 0; i < n; i++)
      {
        CObject * obj = GetDocument()->m_GroupObjects[i];
        if (obj->IsKindOf(RUNTIME_CLASS(CPosNetObjectKey)))
        {
          CPosNetObjectKey * netObj = (CPosNetObjectKey *) obj;          
          nNets++;
          netObj->DoSerialize(ar, POSED_FILE_VER);
          CRect rect;
          netObj->GetObjectHull(rect);
          FltRect realRect = m_pPoseidonMap->FromViewToRealRect(rect);
          realHull[0] = min(realRect[0], realHull[0]);
          realHull[2] = max(realRect[2], realHull[2]);
          realHull[1] = min(realRect[1], realHull[1]);
          realHull[3] = max(realRect[3], realHull[3]);                   
        }
      }

		for (int i = 0; i < n; ++i)
		{
			CObject* obj = GetDocument()->m_GroupObjects[i];
			if (obj->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
			{
				CPosEdObject* edObj = (CPosEdObject*) obj;
				if (edObj->m_pTemplate.NotNull() && 
					(edObj->m_pTemplate->m_nObjType == CPosObjectTemplate::objTpNatural || 
					 edObj->m_pTemplate->m_nObjType == CPosObjectTemplate::objTpPeople ||
					 edObj->m_pTemplate->m_nObjType == CPosObjectTemplate::objTpNewRoads))
				{
					nObjs++;
					CString tempName = edObj->GetTemplate()->GetName();
					MntSerializeString(ar, tempName);
					edObj->DoSerialize(ar, POSED_FILE_VER);
					realHull[0] = min(edObj->m_Position(0, 3), realHull[0]);
					realHull[2] = max(edObj->m_Position(0, 3), realHull[2]);
					realHull[1] = min(edObj->m_Position(2, 3), realHull[1]);
					realHull[3] = max(edObj->m_Position(2, 3), realHull[3]);          
				}
			}
		}     

		ar.Close();

      if (nObjs == 0 && nNets == 0 && nKeys == 0 && nWoods == 0 && nNameAreas == 0)
      {
        AfxGetApp()->EndWaitCursor();
        return;
      }

      int size = (int) file.GetLength();
      BYTE * mem = file.Detach();
      
      HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE,sizeof(PosClipBoardObject) + size - 1);
      PosClipBoardObject * clipData = (PosClipBoardObject *) GlobalLock(hGlobal);
      memcpy(clipData->rectreal,realHull, sizeof(realHull[0]) * 4);
      clipData->nnameareas = nNameAreas;
      clipData->nkey = nKeys;
      clipData->nobjs = nObjs;
      clipData->nets = nNets;
      clipData->nwoods = nWoods;
      clipData->dataSize = size;
      memcpy(clipData->data, mem, size);
      free(mem);

      GlobalUnlock(hGlobal);

      int iFormat = RegisterClipboardFormat(OBJECT_FORMAT_NAME);
      COleDataSource * source = new COleDataSource();
      source->CacheGlobalData(iFormat,hGlobal);
      source->SetClipboard();

      // no need for delete dataSource is deleted by source->SetClipboard();
      //delete m_ClipboardDataSource;

      m_ClipboardDataSource = source;

      AfxGetApp()->EndWaitCursor();
      return;
    }
  case CPosObjectsPanel::grTypeTxtr:
    {
      CPosSelAreaObject& sel = GetDocument()->m_CurrentArea;
      if (!sel.IsValidArea())
      {
        AfxGetApp()->EndWaitCursor();
        return;
      }

      CRect	rLogHull;
      sel.GetObjectHull(rLogHull);
      UNITPOS m_LftTop, m_RghBtm;
      m_pPoseidonMap->GetTextureUnitPosHull(m_LftTop,m_RghBtm,rLogHull);

      AutoArray<TextureElem> textures;      
      // na�tu data z mapy	

      float realSizeUnit =  GetDocument()->m_nRealSizeUnit;
      UNITPOS pos;
      for(pos.z=m_LftTop.z; pos.z>=m_RghBtm.z; pos.z--) for(pos.x=m_LftTop.x; pos.x<=m_RghBtm.x; pos.x++)        
      {
        if (m_pPoseidonMap->IsTextureUnitCenterAtArea(pos, sel.GetLogObjectPosition()))
        {
          TextureElem& elem = textures.Append();
          PTRTEXTURETMPLT texture = m_pPoseidonMap->GetBaseTextureAt(pos);
          if (texture.IsNull())
            elem.textID = 0;
          else
            elem.textID = texture->m_nTextureID;

          REALPOS realPos = m_pPoseidonMap->FromTextureUnitToRealPos(pos);
          elem.rectreal[0] = realPos.x;
          elem.rectreal[1] = realPos.z;

          elem.rectreal[2] = realPos.x + m_pPoseidonMap->GetTextureRealSize();
          elem.rectreal[3] = realPos.z + m_pPoseidonMap->GetTextureRealSize();
        }            
      }

      HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE,sizeof(PosClipBoardTexture) + (textures.Size() - 1) * sizeof(TextureElem));
      PosClipBoardTexture * clipData = (PosClipBoardTexture *) GlobalLock(hGlobal);

      FltRect realRect = m_pPoseidonMap->FromViewToRealRect(rLogHull);

      clipData->rectreal[0] = realRect[0];
      clipData->rectreal[1] = realRect[1];
      clipData->rectreal[2] = realRect[2];
      clipData->rectreal[3] = realRect[3];

      clipData->realGrid = realSizeUnit;

      clipData->numberOfElems = textures.Size();

      memcpy(clipData->data, textures.Data(), textures.Size() * sizeof(TextureElem));
      GlobalUnlock(hGlobal);

      int iFormat = RegisterClipboardFormat(TEXTURE_FORMAT_NAME);
      COleDataSource * source = new COleDataSource();
      source->CacheGlobalData(iFormat,hGlobal);
      source->SetClipboard();

      m_ClipboardDataSource = source;

      AfxGetApp()->EndWaitCursor();
      return;
    }
  case CPosObjectsPanel::grTypeBgImage:
    {
      CPosBackgroundImageObject * bgImage = m_pPoseidonMap->GetBgImage(m_SelBGImageID);
      if (!bgImage)
      {
        AfxGetApp()->EndWaitCursor();
        return;
      }

      HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE,sizeof(PosClipBoardBackgroundImage) + 
        bgImage->m_sFileName.GetLength() + bgImage->m_sAreaName.GetLength()- 1);
      PosClipBoardBackgroundImage * clipData = (PosClipBoardBackgroundImage *) GlobalLock(hGlobal);
      clipData->transparency = bgImage->m_Transparency;

      clipData->rectreal[0] = bgImage->m_RealRect[0];
      clipData->rectreal[1] = bgImage->m_RealRect[1];
      clipData->rectreal[2] = bgImage->m_RealRect[2];
      clipData->rectreal[3] = bgImage->m_RealRect[3];

      clipData->nameLength = bgImage->m_sAreaName.GetLength();
      clipData->fileNameLength =  bgImage->m_sFileName.GetLength();

      memcpy(clipData->data,(LPCSTR) bgImage->m_sAreaName, clipData->nameLength);
      memcpy(clipData->data + clipData->nameLength,(LPCSTR) bgImage->m_sFileName, clipData->fileNameLength);

      GlobalUnlock(hGlobal); 

      int iFormat = RegisterClipboardFormat(BACKGROUND_IMAGE_FORMAT_NAME);
      COleDataSource * source = new COleDataSource();
      source->CacheGlobalData(iFormat,hGlobal);
      source->SetClipboard();

      m_ClipboardDataSource = source;

      AfxGetApp()->EndWaitCursor();
      return;
    }
  
  default:
    AfxGetApp()->EndWaitCursor();
    return;
  }

}

void CPosEdMainView::OnClipboardPaste()
{  
  AfxGetApp()->BeginWaitCursor();

  COleDataObject clipboardSource;
  clipboardSource.AttachClipboard();

  CPoint cursor;
  GetCursorPos(&cursor);
  ScreenToClient(&cursor);
  CPoint cursorLog = WindowToScrollerPosition(cursor);

  int iFormat = RegisterClipboardFormat(HEIGHT_FORMAT_NAME);
  if (clipboardSource.IsDataAvailable(iFormat))
  {
    HGLOBAL hGlobal = clipboardSource.GetGlobalData(iFormat);
    PosClipBoardHeight * clipData = (PosClipBoardHeight *) GlobalLock(hGlobal);
    
    Action_PasteClipboardHeight(clipData,GetDocument(), cursorLog);
    GlobalUnlock(hGlobal);
  }
  else if (clipboardSource.IsDataAvailable(iFormat = RegisterClipboardFormat(OBJECT_FORMAT_NAME)))
  {
    // do not allow copy from different visitor
    if (NULL != m_ClipboardDataSource->GetClipboardOwner())
    {    
      HGLOBAL hGlobal = clipboardSource.GetGlobalData(iFormat);
      PosClipBoardObject * clipData = (PosClipBoardObject *) GlobalLock(hGlobal);

      Action_PasteClipboardObject(clipData,GetDocument(), cursorLog);
      GlobalUnlock(hGlobal);
    }
  }
  else if (clipboardSource.IsDataAvailable(iFormat = RegisterClipboardFormat(TEXTURE_FORMAT_NAME)))
  {
    // do not allow copy from different visitor
    if (NULL != m_ClipboardDataSource->GetClipboardOwner())
    {    
      HGLOBAL hGlobal = clipboardSource.GetGlobalData(iFormat);
      PosClipBoardTexture * clipData = (PosClipBoardTexture *) GlobalLock(hGlobal);

      Action_PasteClipboardTexture(clipData,GetDocument(), cursorLog);
    }
  }
  else if (clipboardSource.IsDataAvailable(iFormat = RegisterClipboardFormat(BACKGROUND_IMAGE_FORMAT_NAME))) 
  {
    // do not allow copy from different visitor
    if (NULL != m_ClipboardDataSource->GetClipboardOwner())
    {    
      HGLOBAL hGlobal = clipboardSource.GetGlobalData(iFormat);
      PosClipBoardBackgroundImage * clipData = (PosClipBoardBackgroundImage *) GlobalLock(hGlobal);

      Action_PasteClipboardBackgroundImage(*clipData,*GetDocument(), cursorLog);
    }
  }

  AfxGetApp()->EndWaitCursor();
}

void CPosEdMainView::OnEditPasteAbsolut()
{
  AfxGetApp()->BeginWaitCursor();

  COleDataObject clipboardSource;
  clipboardSource.AttachClipboard();

  int iFormat = RegisterClipboardFormat(HEIGHT_FORMAT_NAME);
  if (clipboardSource.IsDataAvailable(iFormat))
  {
    HGLOBAL hGlobal = clipboardSource.GetGlobalData(iFormat);
    PosClipBoardHeight * clipData = (PosClipBoardHeight *) GlobalLock(hGlobal);

    Action_PasteAbsolutClipboardHeight(clipData,GetDocument());
  }
  else if (clipboardSource.IsDataAvailable(iFormat = RegisterClipboardFormat(OBJECT_FORMAT_NAME)))
  {
    // do not allow copy from different visitor
    if (NULL != m_ClipboardDataSource->GetClipboardOwner())
    {    
      HGLOBAL hGlobal = clipboardSource.GetGlobalData(iFormat);
      PosClipBoardObject * clipData = (PosClipBoardObject *) GlobalLock(hGlobal);

      Action_PasteAbsolutClipboardObject(clipData,GetDocument());
    }
  }
  else if (clipboardSource.IsDataAvailable(iFormat = RegisterClipboardFormat(TEXTURE_FORMAT_NAME)))
  {
    // do not allow copy from different visitor
    if (NULL != m_ClipboardDataSource->GetClipboardOwner())
    {    
      HGLOBAL hGlobal = clipboardSource.GetGlobalData(iFormat);
      PosClipBoardTexture * clipData = (PosClipBoardTexture *) GlobalLock(hGlobal);

      Action_PasteAbsolutClipboardTexture(clipData,GetDocument());
    }
  } else if (clipboardSource.IsDataAvailable(iFormat = RegisterClipboardFormat(BACKGROUND_IMAGE_FORMAT_NAME))) 
  {
    // do not allow copy from different visitor
    if (NULL != m_ClipboardDataSource->GetClipboardOwner())
    {    
      HGLOBAL hGlobal = clipboardSource.GetGlobalData(iFormat);
      PosClipBoardBackgroundImage * clipData = (PosClipBoardBackgroundImage *) GlobalLock(hGlobal);

      Action_PasteAbsolutClipboardBackgroundImage(*clipData,*GetDocument());
    }
  }

  AfxGetApp()->EndWaitCursor();
}


void CPosEdMainView::OnUpdateClipboardCopy(CCmdUI* pCmdUI)
{
  int mode = m_pObjsPanel->GetTypeObjects();

  switch(mode)
  {
  case CPosObjectsPanel::grTypeTxtr:
  case CPosObjectsPanel::grTypeLand:  
  case CPosObjectsPanel::grTypeWood:  
    {
      CPosSelAreaObject& sel = GetDocument()->m_CurrentArea;
      pCmdUI->Enable(sel.IsValidArea());
      return;      
    }    
  case CPosObjectsPanel::grTypeNtOb:
  case CPosObjectsPanel::grTypePlOb:
#ifndef __ONLY_FOR_PUBLIC__
  case CPosObjectsPanel::grTypeNROb:
#endif
  case CPosObjectsPanel::grTypeNets:
  case CPosObjectsPanel::grTypeKPla:
  case CPosObjectsPanel::grTypeNObj:
  case CPosObjectsPanel::grTypeName:
    {      
      pCmdUI->Enable(GetDocument()->m_GroupObjects.GetGroupSize() > 0);
      return;      
    }   
  case CPosObjectsPanel::grTypeBgImage:
    {    
      pCmdUI->Enable(!m_SelBGImageID.IsEmpty());
      return;
    }  
  default:
    pCmdUI->Enable(FALSE);
    return;
  }
}
void CPosEdMainView::OnEditSelectAll()
{
  int mode = m_pObjsPanel->GetTypeObjects();

  switch(mode)
  {
  case CPosObjectsPanel::grTypeTxtr:
  case CPosObjectsPanel::grTypeBgImage:
  case CPosObjectsPanel::grTypeWood:
  case CPosObjectsPanel::grTypeName:
    {
      CRect rLogArea(0,0,m_pPoseidonMap->GetSize().x * VIEW_LOG_UNIT_SIZE,m_pPoseidonMap->GetSize().z * VIEW_LOG_UNIT_SIZE);
      Action_AddPoseidonCurrArea(rLogArea,TRUE, FALSE, GetDocument());
      return;      
    } 
  case CPosObjectsPanel::grTypeLand:
    {
      CRect rLogArea(-VIEW_LOG_UNIT_SIZE/2, +VIEW_LOG_UNIT_SIZE/2, 
        (int) ((m_pPoseidonMap->GetSize().x - 0.5f) * VIEW_LOG_UNIT_SIZE), 
        (int) ((m_pPoseidonMap->GetSize().z + 0.5f) * VIEW_LOG_UNIT_SIZE));
        Action_AddPoseidonCurrArea(rLogArea,TRUE, FALSE, GetDocument());
      return;      
    } 
  case CPosObjectsPanel::grTypeNtOb:
  case CPosObjectsPanel::grTypePlOb:
#ifndef __ONLY_FOR_PUBLIC__
  case CPosObjectsPanel::grTypeNROb:
#endif
  case CPosObjectsPanel::grTypeNets:
  case CPosObjectsPanel::grTypeKPla:
  case CPosObjectsPanel::grTypeNObj:
    {
      //Select all objects in map
      EnabledObjects enabled = GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.GetEnableObjects();
      DoSelectPosObjsAll(enabled, TRUE);
      return;
    }   
  default:   
    return;
  }
}

void CPosEdMainView::OnEditDeselect()
{
  if (GetDocument()->m_CurrentArea.IsValidArea())
    Action_DelPoseidonCurrArea(GetDocument(), NULL, FALSE);

  DoUnselectPosObject();
}

// nahodny rozmistnovac
void CPosEdMainView::OnRandomPlacer()
{
  Action_RandomPlacementObject(GetDocument());
}

void CPosEdMainView::OnUpdateRandomPlacer(CCmdUI* pCmdUI)
{	
  pCmdUI->Enable(GetDocument() != NULL && GetDocument()->m_CurrentArea.IsValidArea());
}

// nahodny rozmistnovac
void CPosEdMainView::OnSnapObjects()
{
  if (m_EditObjsParams.m_pSelObject != NULL)
  {  
    ASSERT(m_EditObjsParams.m_pSelObject->IsKindOf(RUNTIME_CLASS(CPosGroupObject)));
    Action_SnapObjects(*((CPosGroupObject *)m_EditObjsParams.m_pSelObject), *GetDocument());
  }
}

void CPosEdMainView::OnUpdateSnapObjects(CCmdUI* pCmdUI)
{	
  pCmdUI->Enable(m_EditObjsParams.m_pSelObject && ((CPosGroupObject *) m_EditObjsParams.m_pSelObject)->GetGroupSize() > 0);
}

// lock & unlock
void CPosEdMainView::OnLock()
{

  int mode = m_pObjsPanel->GetTypeObjects();

  switch(mode)
  {
  case CPosObjectsPanel::grTypeTxtr:
    {
      if(GetDocument()->m_CurrentArea.IsValidArea())      
        Action_TextureLock(*GetDocument()->m_CurrentArea.GetLogObjectPosition(), true, *GetDocument(), NULL);             
      break;
    }
    // case CPosObjectsPanel::grTypeBgImage:
    // case CPosObjectsPanel::grTypeWood:
    // case CPosObjectsPanel::grTypeName:     
  case CPosObjectsPanel::grTypeLand:
    {
      Action_LockHeight(*GetDocument()->m_CurrentArea.GetLogObjectPosition(), true, *GetDocument(), NULL);      
      break;      
    } 
  case CPosObjectsPanel::grTypeNtOb:
  case CPosObjectsPanel::grTypePlOb:
#ifndef __ONLY_FOR_PUBLIC__
  case CPosObjectsPanel::grTypeNROb:
#endif
  case CPosObjectsPanel::grTypeNets:
  case CPosObjectsPanel::grTypeKPla:
  case CPosObjectsPanel::grTypeNObj:
    {
      //Select all objects in map
      Action_LockPoseidonObjects((CPosGroupObject *) m_EditObjsParams.m_pSelObject, true, *GetDocument(), NULL);

      break;
    }   
  default:   
    break;
  }  
};

void CPosEdMainView::OnUpdateLock(CCmdUI* pCmdUI)
{
  int mode = m_pObjsPanel->GetTypeObjects();

  switch(mode)
  {
  case CPosObjectsPanel::grTypeTxtr:
  case CPosObjectsPanel::grTypeLand:
    {
      pCmdUI->Enable(GetDocument()->m_CurrentArea.IsValidArea());
      return;
    }
  case CPosObjectsPanel::grTypeBgImage:
  case CPosObjectsPanel::grTypeWood:
  case CPosObjectsPanel::grTypeName:      
    {
      pCmdUI->Enable(false);
      return;  
    } 
  case CPosObjectsPanel::grTypeNtOb:
  case CPosObjectsPanel::grTypePlOb:
#ifndef __ONLY_FOR_PUBLIC__
  case CPosObjectsPanel::grTypeNROb:
#endif
  case CPosObjectsPanel::grTypeNets:
  case CPosObjectsPanel::grTypeKPla:
  case CPosObjectsPanel::grTypeNObj:
    {
      //Select all objects in map
      pCmdUI->Enable(m_EditObjsParams.m_pSelObject && ((CPosGroupObject *) m_EditObjsParams.m_pSelObject)->GetGroupSize() > 0);
      return;
    }   
  default:  
    pCmdUI->Enable(false);
    return;
  }    
};

void CPosEdMainView::OnUnlock()
{
  int mode = m_pObjsPanel->GetTypeObjects();

  switch(mode)
  {
  case CPosObjectsPanel::grTypeTxtr:
    {
      if(GetDocument()->m_CurrentArea.IsValidArea())      
        Action_TextureLock(*GetDocument()->m_CurrentArea.GetLogObjectPosition(), false, *GetDocument(), NULL);             
      break;
    }
 // case CPosObjectsPanel::grTypeBgImage:
 // case CPosObjectsPanel::grTypeWood:
 // case CPosObjectsPanel::grTypeName:     
  case CPosObjectsPanel::grTypeLand:
    {
      Action_LockHeight(*GetDocument()->m_CurrentArea.GetLogObjectPosition(), false, *GetDocument(), NULL);      
      break;      
    } 
  case CPosObjectsPanel::grTypeNtOb:
  case CPosObjectsPanel::grTypePlOb:
#ifndef __ONLY_FOR_PUBLIC__
  case CPosObjectsPanel::grTypeNROb:
#endif
  case CPosObjectsPanel::grTypeNets:
  case CPosObjectsPanel::grTypeKPla:
  case CPosObjectsPanel::grTypeNObj:
    {
      //Select all objects in map
      Action_LockPoseidonObjects((CPosGroupObject *) m_EditObjsParams.m_pSelObject, false, *GetDocument(), NULL);
     
      break;
    }   
  default:   
    break;
  }  
};

void CPosEdMainView::OnUpdateUnlock(CCmdUI* pCmdUI)
{
  OnUpdateLock(pCmdUI);
};


void CPosEdMainView::CursorPositionSet(const SMoveObjectPosMessData &msgData)
{
      CPosEdMainDoc * pDocument = (CPosEdMainDoc *) m_pDocument;
      if (pDocument == NULL) return;

      CPosEdCursor & cursor = pDocument->m_CursorObject;
      cursor.m_rpBuldozrPos.x = msgData.Position[0][3];
      cursor.m_rpBuldozrPos.z = msgData.Position[2][3];
      // update polohy ve view - je-li aktu�ln� nastaven
      //if (GetPosEdEnvironment()->m_optPosEd.m_nBldCursorUpdate == CPosEdOptions::upBlCurFull)
        cursor.SetCursorRealPosition(cursor.m_rpBuldozrPos,TRUE);

      KeepVisibleLog(cursor.m_ptCurrentPos);

}

class CDlgAskForExportEmf: public CFileDialog
{
  int xs;
  int ys;
public:
  CDlgAskForExportEmf(int xs, int ys, LPCTSTR lpszDefExt = NULL , LPCTSTR lpszFileName = NULL , DWORD dwFlags  = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT , LPCTSTR lpszFilter  = NULL , CWnd* pParentWnd = NULL ):
      CFileDialog(FALSE,lpszDefExt,lpszFileName,dwFlags | OFN_ENABLETEMPLATE,lpszFilter),xs(xs),ys(ys) 
      {
        m_ofn.lpTemplateName=MAKEINTRESOURCE(IDD_EXPORT_EMF);        
      }
  void OnInitDone()
  {
    SetDlgItemInt(IDC_X,xs);
    SetDlgItemInt(IDC_Y,ys);
  }
  BOOL OnFileNameOK()
  {
    xs=GetDlgItemInt(IDC_X);
    ys=GetDlgItemInt(IDC_Y);
    return FALSE;
  }
  int GetXS() const {return xs;}
  int GetYS() const {return ys;}
};

static PBITMAPINFO CreateBitmapInfoStruct( HBITMAP hBmp)
{ 
  BITMAP bmp; 
  PBITMAPINFO pbmi; 
  WORD    cClrBits; 

  // Retrieve the bitmap color format, width, and height. 
  if (!GetObjectA(hBmp, sizeof(BITMAP), (LPSTR)&bmp)) return 0;

  // Convert the color format to a count of bits. 
  cClrBits = (WORD)(bmp.bmPlanes * bmp.bmBitsPixel); 
  if (cClrBits == 1) 
    cClrBits = 1; 
  else if (cClrBits <= 4) 
    cClrBits = 4; 
  else if (cClrBits <= 8) 
    cClrBits = 8; 
  else if (cClrBits <= 16) 
    cClrBits = 16; 
  else if (cClrBits <= 24) 
    cClrBits = 24; 
  else cClrBits = 32; 

  // Allocate memory for the BITMAPINFO structure. (This structure 
  // contains a BITMAPINFOHEADER structure and an array of RGBQUAD 
  // data structures.) 

  if (cClrBits != 24) 
    pbmi = (PBITMAPINFO) malloc( 
    sizeof(BITMAPINFOHEADER) + 
    sizeof(RGBQUAD) * (1<< cClrBits)); 

  // There is no RGBQUAD array for the 24-bit-per-pixel format. 

  else 
    pbmi = (PBITMAPINFO) malloc( 
    sizeof(BITMAPINFOHEADER)); 

  // Initialize the fields in the BITMAPINFO structure. 

  pbmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER); 
  pbmi->bmiHeader.biWidth = bmp.bmWidth; 
  pbmi->bmiHeader.biHeight = bmp.bmHeight; 
  pbmi->bmiHeader.biPlanes = bmp.bmPlanes; 
  pbmi->bmiHeader.biBitCount = bmp.bmBitsPixel; 
  if (cClrBits < 24) 
    pbmi->bmiHeader.biClrUsed = (1<<cClrBits); 

  // If the bitmap is not compressed, set the BI_RGB flag. 
  pbmi->bmiHeader.biCompression = BI_RGB; 

  // Compute the number of bytes in the array of color 
  // indices and store the result in biSizeImage. 
  // For Windows NT, the width must be DWORD aligned unless 
  // the bitmap is RLE compressed. This example shows this. 
  // For Windows 95/98/Me, the width must be WORD aligned unless the 
  // bitmap is RLE compressed.
  pbmi->bmiHeader.biSizeImage = ((pbmi->bmiHeader.biWidth * cClrBits +31) & ~31) /8
    * pbmi->bmiHeader.biHeight; 
  // Set biClrImportant to 0, indicating that all of the 
  // device colors are important. 
  pbmi->bmiHeader.biClrImportant = 0; 
  return pbmi; 
} 

static bool CreateBMPFile(LPCTSTR pszFile, PBITMAPINFO pbi, HBITMAP hBMP, HDC hDC) 
{
  HANDLE hf;                 // file handle 
  BITMAPFILEHEADER hdr;       // bitmap file-header 
  PBITMAPINFOHEADER pbih;     // bitmap info-header 
  LPBYTE lpBits;              // memory pointer 
  DWORD dwTotal;              // total count of bytes 
  DWORD cb;                   // incremental count of bytes 
  BYTE *hp;                   // byte pointer 
  DWORD dwTmp; 

  pbih = (PBITMAPINFOHEADER) pbi; 
  lpBits = (LPBYTE) malloc(pbih->biSizeImage);

  if (lpBits) 
  {
    // Retrieve the color table (RGBQUAD array) and the bits 
    // (array of palette indices) from the DIB. 
    if (GetDIBits(hDC, hBMP, 0, (WORD) pbih->biHeight, lpBits, pbi, 
      DIB_RGB_COLORS)) 
    {

      // Create the .BMP file. 
      hf = CreateFile(pszFile, 
        GENERIC_READ | GENERIC_WRITE, 
        (DWORD) 0, 
        NULL, 
        CREATE_ALWAYS, 
        FILE_ATTRIBUTE_NORMAL, 
        (HANDLE) NULL); 
      if (hf != INVALID_HANDLE_VALUE) 
      {  
        hdr.bfType = 0x4d42;        // 0x42 = "B" 0x4d = "M" 
        // Compute the size of the entire file. 
        hdr.bfSize = (DWORD) (sizeof(BITMAPFILEHEADER) + 
          pbih->biSize + pbih->biClrUsed 
          * sizeof(RGBQUAD) + pbih->biSizeImage); 
        hdr.bfReserved1 = 0; 
        hdr.bfReserved2 = 0; 

        // Compute the offset to the array of color indices. 
        hdr.bfOffBits = (DWORD) sizeof(BITMAPFILEHEADER) + 
          pbih->biSize + pbih->biClrUsed 
          * sizeof (RGBQUAD); 

        // Copy the BITMAPFILEHEADER into the .BMP file. 
        if (WriteFile(hf, (LPVOID) &hdr, sizeof(BITMAPFILEHEADER), 
          (LPDWORD) &dwTmp,  NULL)) 
        {

          // Copy the BITMAPINFOHEADER and RGBQUAD array into the file. 
          if (WriteFile(hf, (LPVOID) pbih, sizeof(BITMAPINFOHEADER) 
            + pbih->biClrUsed * sizeof (RGBQUAD), 
            (LPDWORD) &dwTmp, ( NULL)) )
          {


            // Copy the array of color indices into the .BMP file. 
            dwTotal = cb = pbih->biSizeImage; 
            hp = lpBits; 
            if (WriteFile(hf, (LPSTR) hp, (int) cb, (LPDWORD) &dwTmp,NULL)) 
            {
              free(lpBits);
              CloseHandle(hf);
              return true;

            }


          }
        }
        CloseHandle(hf);
      }
    }

    // Free memory. 
  }
  free(lpBits);
  return false;
}


void CPosEdMainView::OnProjectExportmapasimage()
{
	static float saveFactorx = 1, saveFactory = 1;

	UNITPOS mapsz = this->m_pPoseidonMap->GetSize();

//  Export of bmp disabled
//	CDlgAskForExportEmf askf(toLargeInt(mapsz.x * saveFactory), toLargeInt(mapsz.z * saveFactory), ".BMP", 0, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST,
//							 "BMP|*.BMP|EMF|*.EMF|", this);

	CDlgAskForExportEmf askf(toLargeInt(mapsz.x * saveFactory), toLargeInt(mapsz.z * saveFactory), ".EMF", 0, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST,
							 "EMF|*.EMF|", this);

	if (askf.DoModal() == IDOK)
	{
		saveFactorx = askf.GetXS() / (float)mapsz.x;
		saveFactory = askf.GetYS() / (float)mapsz.z;

		CRect dcsz(0, 0, askf.GetXS(), askf.GetYS());

		CRect curWindow;

		GetWindowRect(&curWindow);
		ScreenToClient(&curWindow);
		CRect nextWindow = dcsz;
		AdjustWindowRect(&nextWindow, GetWindowLong(*this, GWL_STYLE), false);
		MoveWindow(&nextWindow, FALSE);

		OnViewZoomFit();

		CWaitCursor wt;

		{
			CString fname = askf.GetPathName();
			CString ext = askf.GetFileExt();

			CClientDC dc(this);

			//  Export of bmp disabled
//			if (_stricmp(ext, "BMP") == 0)
//			{
//				CDC draw;
//				draw.CreateCompatibleDC(&dc);
//				CBitmap bmp;
//				if (bmp.CreateCompatibleBitmap(&dc, dcsz.right, dcsz.bottom) == FALSE)
//				{
//					AfxMessageBox("Cannot allocate bitmap...", MB_OK | MB_ICONERROR);
//					return;
//				}
//				CBitmap* old = draw.SelectObject(&bmp);
//
//				CRgn allBox;
//				allBox.CreateRectRgn(dcsz.left, dcsz.top, dcsz.right, dcsz.bottom);
//				draw.SelectClipRgn(&allBox, RGN_COPY);
//
//				EraseBkgndToDC(&draw, dcsz, dcsz);
//				OnPrepareDC(&draw);
//				OnDraw(&draw);
//
//				draw.SelectObject(old);
//
//				BITMAPINFO* binfo = CreateBitmapInfoStruct(bmp);
//				if (binfo != 0)
//				{
//					if (CreateBMPFile(fname, binfo, bmp, dc) == false)
//					{
//						AfxMessageBox("Cannot save the bitmap.");
//					}
//					free(binfo);
//				}
//			}
//			else
//			{      
				CRect bound;
				int iWidthMM = GetDeviceCaps(dc, HORZSIZE); 
				int iHeightMM = GetDeviceCaps(dc, VERTSIZE); 
				int iWidthPels = GetDeviceCaps(dc, HORZRES); 
				int iHeightPels = GetDeviceCaps(dc, VERTRES); 

				bound.left = (dcsz.left * iWidthMM * 100) / iWidthPels; 
				bound.top = (dcsz.top * iHeightMM * 100) / iHeightPels; 
				bound.right = (dcsz.right * iWidthMM * 100) / iWidthPels; 
				bound.bottom = (dcsz.bottom * iHeightMM * 100) / iHeightPels; 

				CMetaFileDC metafl;
				if (metafl.CreateEnhanced(&dc, askf.GetPathName(), &bound, "\0") == FALSE)
				{
					MoveWindow(&curWindow, FALSE);
					AfxMessageBox("Cannot open metafile for writing");
					return;
				}
				metafl.m_hAttribDC = metafl.m_hDC;
				int err = GetLastError();

				CRgn allBox;
				allBox.CreateRectRgn(dcsz.left, dcsz.top, dcsz.right, dcsz.bottom);

				metafl.SelectClipRgn(&allBox, RGN_COPY);
        
				EraseBkgndToDC(&metafl, dcsz, dcsz);
				OnPrepareDC(&metafl);
				OnDraw(&metafl);
				metafl.m_hAttribDC = 0;
				metafl.CloseEnhanced();
//			}
		}
		MoveWindow(&curWindow, FALSE);
		AfxGetMainWnd()->SetFocus();
	}
}


IExtraEditorClient *CPosEdMainView::ActivateExEditor(IExtraEditorClient *editor)
{
  IExtraEditorClient *curEdit=_activeExEditor;
  if (_activeExEditor) _activeExEditor->OnActivateEditor(false);
  _activeExEditor=editor;
  if (_activeExEditor) _activeExEditor->OnActivateEditor(true);
  return curEdit;
}

bool CPosEdMainView::ExEditor_MouseEvent(IExtraEditorClient::MouseEvent ev, const CPoint &pt, UINT flags)
{
  if (_activeExEditor==0) return false;
  bool lockMouse=GetCapture()==this;
  bool prevLock=lockMouse;
  bool res=_activeExEditor->OnMouseEvent(ev,pt,flags,lockMouse);
  if (lockMouse!=prevLock)
  {
    if (lockMouse) SetCapture();
    else ReleaseCapture();
  }
  return res;
}

void CPosEdMainView::TransformWorldToScreen(const Array<const Vector3> &inpoints, Array<CPoint> &outpoints) const
{
  ASSERT(inpoints.Size()==outpoints.Size());
  for (int i=0;i<inpoints.Size();i++)
  {
    outpoints[i]=GetDocument()->m_PoseidonMap.FromRealToViewPos(REALPOS(inpoints[i][0],inpoints[i][2]));
  }
}
void CPosEdMainView::TransformScreenToWorld(const Array<const CPoint> &inpoints, Array<Vector3> &outpoints)const
{ 
  ASSERT(inpoints.Size()==outpoints.Size());
  for (int i=0;i<inpoints.Size();i++)
  {
    REALPOS pos=GetDocument()->m_PoseidonMap.FromViewToRealPos(inpoints[i]);
    outpoints[i][0]=pos.x;
    outpoints[i][2]=pos.z;
    outpoints[i][1]=0;
  }
}

BOOL CPosEdMainView::OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
  if (_activeExEditor)
  { 
    if (_activeExEditor->OnWndMsg(message,wParam,lParam,pResult)) return TRUE;
    if (message>=WM_MOUSEFIRST && message<=WM_MOUSELAST)
    {
      IExtraEditorClient::MouseEvent ev;
      switch(message)
      {
      case WM_MOUSEMOVE: ev=IExtraEditorClient::meMove;break;
      case WM_LBUTTONDOWN: ev=IExtraEditorClient::meLClick;break;
      case WM_MBUTTONDOWN: ev=IExtraEditorClient::meMClick;break;
      case WM_RBUTTONDOWN: ev=IExtraEditorClient::meRClick;break;
      case WM_LBUTTONUP: ev=IExtraEditorClient::meLRelease;break;
      case WM_MBUTTONUP: ev=IExtraEditorClient::meMRelease;break;
      case WM_RBUTTONUP: ev=IExtraEditorClient::meRRelease;break;
      case WM_LBUTTONDBLCLK: ev=IExtraEditorClient::meLDblClick;break;
      case WM_MBUTTONDBLCLK: ev=IExtraEditorClient::meMDblClick;break;
      case WM_RBUTTONDBLCLK: ev=IExtraEditorClient::meRDblClick;break;
      case WM_MOUSEWHEEL: ev=IExtraEditorClient::meWheel;break;
      }
      bool lockMouse=GetCapture()==this;
      bool prevLock=lockMouse;
      bool res=_activeExEditor->OnMouseEvent(ev,CPoint(lParam),wParam,lockMouse);
      if (lockMouse!=prevLock)
      {
        if (lockMouse) SetCapture();
        else ReleaseCapture();
      }
      if (res) return TRUE;
    }
    else if (message>=WM_KEYFIRST && message<=WM_KEYLAST)
    {
      IExtraEditorClient::KeyEvent ev;
      switch(message)
      {
      case WM_KEYDOWN: if (lParam & (1<<30)) ev=IExtraEditorClient::keRepeat;
                       else ev=IExtraEditorClient::keDown;
                       break;
      case WM_KEYUP: ev=IExtraEditorClient::keUp;break;
      case WM_CHAR: ev=IExtraEditorClient::keChar;break;
      default: ev=IExtraEditorClient::keUnknown;break;
      }
      if (_activeExEditor->OnKeyboardEvent(ev,wParam,lParam & 0xFFFF,lParam)) return TRUE;
    }    
    else if (message==WM_COMMAND)
    {
      switch(LOWORD(wParam))
      {
      case ID_EDIT_COPY: if (_activeExEditor->OnCopy()) return TRUE;break;
      case ID_EDIT_PASTE: if (_activeExEditor->OnPaste()) return TRUE;break;
      case ID_EDIT_OBJECT_DEL: if (_activeExEditor->OnDelete()) return TRUE;break;
      case ID_EDIT_CUT: if (_activeExEditor->OnCopy()) 
                        {
                          _activeExEditor->OnDelete();
                          return TRUE;
                        }
                        break;
      case ID_EDIT_SELECT_ALL:if (_activeExEditor->OnSelectAll())  return TRUE;break;
      case ID_EDIT_UNDO: if (_activeExEditor->OnUndo()) return TRUE;break;
      case ID_EDIT_REDO: if (_activeExEditor->OnRedo()) return TRUE;break;
      }
    }
  }
  return CFMntScrollerView::OnWndMsg(message,wParam,lParam,pResult);
}

void CPosEdMainView::OnEditCrop()
{
  float sqx=GetDocument()->m_nRealSizeUnit,sqy=GetDocument()->m_nRealSizeUnit;
  UNITPOS us=GetDocument()->m_PoseidonMap.GetSize();
  
  DlgCropLand dlg;
  dlg.vRight=(dlg.vWidth=us.x)-1;
  dlg.vTop=(dlg.vHeight=us.z)-1;
  dlg.vUnits=1;
  if (dlg.DoModal()==IDOK)
  {
    if (AfxMessageBox("Warning: Operation is not undoable.\r\n\r\n. It is HARDLY recomended to save result into new file and reopen it.",MB_OKCANCEL)==IDOK)
    {    
      if (dlg.vUnits==0)
      {
        dlg.vLeft=toInt(floor(dlg.vLeft/sqx));
        dlg.vBottom=toInt(floor(dlg.vBottom/sqx));
        dlg.vRight=toInt(ceil(dlg.vRight/sqx));
        dlg.vTop=toInt(ceil(dlg.vTop/sqx));
      }
      GetDocument()->CropLand(CRect(dlg.vLeft,dlg.vTop,dlg.vRight,dlg.vBottom));
      AfxGetMainWnd()->SendMessage(WM_COMMAND,ID_FILE_SAVE_AS,0);
      AfxGetMainWnd()->PostMessage(WM_COMMAND,ID_FILE_CLOSE,0);
      AfxGetMainWnd()->PostMessage(WM_COMMAND,ID_FILE_OPEN,0);
    }
  }
}

void CPosEdMainView::OnNewRoadEditor()
{
  if (_activeExEditor==_roadVectorEditor)
    ActivateExEditor(0);
  else
    ActivateExEditor(_roadVectorEditor);
}

void CPosEdMainView::OnNewRoadEditorUpdate(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_roadVectorEditor->CanActivate());
  pCmdUI->SetCheck(_roadVectorEditor==_activeExEditor);
}
