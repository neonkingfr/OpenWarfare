#pragma once

// Files defines structures used in clipboard
#define HEIGHT_FORMAT_NAME _T("PosClipBoardHeight")

typedef struct  
{
  float relrealPos[2];
  float height;
} HeightElem;

typedef struct 
{
  float realGrid;
  float rectreal[4]; //(left,top,right,bottom)
  int numberOfElems;
  HeightElem data[1];
} PosClipBoardHeight;


#define OBJECT_FORMAT_NAME _T("PosClipBoardObject")

typedef struct 
{  
  float rectreal[4]; //(left,top,right,bottom)

  int nnameareas;
  int nwoods;
  int nkey;
  int nets;
  int nobjs;

  int dataSize;
  BYTE data[1];
} PosClipBoardObject;

#define TEXTURE_FORMAT_NAME _T("PosClipBoardTexture")

typedef struct  
{
  float rectreal[4];
  int textID;
} TextureElem;

typedef struct 
{
  float realGrid;
  float rectreal[4]; //(left,top,right,bottom)
  int numberOfElems;
  TextureElem data[1];
} PosClipBoardTexture;

#define BACKGROUND_IMAGE_FORMAT_NAME _T("PosClipBoardBackgroundImage")

typedef struct
{
  float rectreal[4]; //(left,top,right,bottom)
  int transparency;

  int fileNameLength;
  int nameLength;

#if ((defined _UNICODE) || (defined UNICODE))
#error I assume that CString holds chars
#endif

  char data[1];

} PosClipBoardBackgroundImage;



