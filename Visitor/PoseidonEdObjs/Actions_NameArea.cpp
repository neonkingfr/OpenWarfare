/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditNameAreaObjectDlg dialog

class CEditNameAreaObjectDlg : public CDialog
{
// Construction
public:
	CEditNameAreaObjectDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	BOOL	m_bNew;

	//{{AFX_DATA(CEditNameAreaObjectDlg)
	enum { IDD = IDD_EDIT_NAMEAREA_OBJECT };
	CComboBox		m_cComboFrame;
	CComboBox		m_cComboEntire;
	CFMntColorButton	m_cColorFrame;
	CFMntColorButton	m_cColorEntire;
	CString			m_sName;
	int				m_nStyle;
	BOOL	m_bClearSel;
	BOOL	m_bIsVisible;
	//}}AFX_DATA
	COLORREF		m_cAreaEntire;
	COLORREF		m_cAreaFrame;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditNameAreaObjectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEditNameAreaObjectDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnColorEntire();
	afx_msg void OnColorFrame();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CEditNameAreaObjectDlg::CEditNameAreaObjectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEditNameAreaObjectDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditNameAreaObjectDlg)
	m_sName  = _T("");
	m_nStyle = 0;
	m_bClearSel = TRUE;
	m_bIsVisible = TRUE;
	//}}AFX_DATA_INIT
}

BOOL CEditNameAreaObjectDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	if (!m_bNew)
	{
		::ShowWindow(DLGCTRL(IDC_CHECK_CLEAR_SEL),SW_HIDE);
	}
	return TRUE;
}

void CEditNameAreaObjectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditNameAreaObjectDlg)
	DDX_Control(pDX, IDC_COMBO_COLOR_FRAME, m_cComboFrame);
	DDX_Control(pDX, IDC_COMBO_COLOR_ENTIRE, m_cComboEntire);
	DDX_Control(pDX, IDC_COLOR_FRAME, m_cColorFrame);
	DDX_Control(pDX, IDC_COLOR_ENTIRE, m_cColorEntire);
	DDX_Check(pDX, IDC_CHECK_CLEAR_SEL, m_bClearSel);
	DDX_Check(pDX, IDC_CHECK_VISIBLE, m_bIsVisible);
	//}}AFX_DATA_MAP
	MDDX_Text(pDX, IDC_EDIT_NAME, m_sName);
	MDDV_MaxChars(pDX, m_sName, MAX_AREA_NAME_LEN, IDC_EDIT_NAME);
	if (SAVEDATA)
		MDDV_MinChars(pDX, m_sName, 1);
	// barvy
	if (SAVEDATA)
	{
		if (m_cComboFrame.GetCurSel()==0)
			m_cAreaFrame = DEFAULT_COLOR;
		else
			m_cAreaFrame = m_cColorFrame.m_cColor;

		if (m_cComboEntire.GetCurSel()==0)
			m_cAreaEntire= DEFAULT_COLOR;
		else
			m_cAreaEntire= m_cColorEntire.m_cColor;
	} else
	{
		if (m_cAreaFrame == DEFAULT_COLOR)
		{
			m_cComboFrame.SetCurSel(0);
			m_cColorFrame.m_cColor = GetPosEdEnvironment()->m_cfgCurrent.m_cDefColors[CPosEdCfg::defcolFNAre];
		} else
		{
			m_cComboFrame.SetCurSel(1);
			m_cColorFrame.m_cColor = m_cAreaFrame;
		};
		if (m_cAreaEntire == DEFAULT_COLOR)
		{
			m_cComboEntire.SetCurSel(0);
			m_cColorEntire.m_cColor = GetPosEdEnvironment()->m_cfgCurrent.m_cDefColors[CPosEdCfg::defcolNArea];
		} else
		{
			m_cComboEntire.SetCurSel(1);
			m_cColorEntire.m_cColor = m_cAreaEntire;
		};
	};
	// styl
	DDX_CBIndex(pDX, IDC_COMBO_STYLE, m_nStyle);
}

BEGIN_MESSAGE_MAP(CEditNameAreaObjectDlg, CDialog)
	//{{AFX_MSG_MAP(CEditNameAreaObjectDlg)
	ON_BN_CLICKED(IDC_COLOR_ENTIRE, OnColorEntire)
	ON_BN_CLICKED(IDC_COLOR_FRAME, OnColorFrame)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CEditNameAreaObjectDlg::OnColorEntire() 
{
	if (m_cColorEntire.DoEditColor())
		m_cComboEntire.SetCurSel(1);
}

void CEditNameAreaObjectDlg::OnColorFrame() 
{
	if (m_cColorFrame.DoEditColor())
		m_cComboFrame.SetCurSel(1);
}

/////////////////////////////////////////////////////////////////////////////
// Editace pojmenovan� plochy

BOOL  DoEditNameArea(CPosNameAreaObject* pArea,BOOL bNew,BOOL* bClearSel)
{
	CEditNameAreaObjectDlg dlg;
	// kopie parametr�
	dlg.m_bNew  = bNew;
	dlg.m_sName = pArea->m_sAreaName;
	dlg.m_cAreaEntire	= pArea->m_cAreaEntire;
	dlg.m_cAreaFrame	= pArea->m_cAreaFrame;
	dlg.m_nStyle		= pArea->m_nAreaStyle;
	dlg.m_bIsVisible	= pArea->m_bAreaVisible;

	// relizace 
	if (dlg.DoModal() != IDOK)
		return FALSE;

	// li�� se parametry ?
	if ( bNew || (dlg.m_sName.Compare(pArea->m_sAreaName)!=0)||
		(dlg.m_cAreaEntire != pArea->m_cAreaEntire)||
		(dlg.m_cAreaFrame  != pArea->m_cAreaFrame)||
		(dlg.m_nStyle	   != pArea->m_nAreaStyle)||
		(dlg.m_bIsVisible  != pArea->m_bAreaVisible))
	{
		// kopie dat
		pArea->m_sAreaName	  = dlg.m_sName;
		pArea->m_cAreaEntire  = dlg.m_cAreaEntire;
		pArea->m_cAreaFrame	  = dlg.m_cAreaFrame;
		pArea->m_nAreaStyle	  = dlg.m_nStyle;
		pArea->m_bAreaVisible = dlg.m_bIsVisible;
		if (bClearSel != NULL)
			(*bClearSel) = dlg.m_bClearSel;
		return TRUE;
	}
	// ��dn� zm�na
	return FALSE;
};

/////////////////////////////////////////////////////////////////////////////
// Akce s pojmenovan�mi plochami

// vytvo�en� pojmenovan� plochy
CPosAction* Action_CreatePoseidonNameArea(CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pDocument->m_CurrentArea.IsValidArea())
	{
		CPosNameAreaObject*	pNameArea = NULL;
		CPosAction*			pAction = NULL;
		TRY
		{
			// vytvo�en� pojmenovan� skupiny
			pNameArea = new CPosNameAreaObject(pDocument->m_PoseidonMap);
			// parametry pro pojmenovanou skupini
			BOOL bClearSel;
			if (!DoEditNameArea(pNameArea,TRUE,&bClearSel))
			{
				delete pNameArea;
				return NULL;
			};
			// nov� ID
			pNameArea->SetFreeID();
			pNameArea->GetLogObjectPosition()->CopyFrom(pDocument->m_CurrentArea.GetLogObjectPosition());
			// generov�n� akce
			pAction = pDocument->GenerNewObjectAction(IDA_NAMEAREA_CREATE,NULL,pNameArea,FALSE,pToGroup);
			// uvoln�n� plochy
			if (bClearSel)
			{
				CPosAction* pAreaAct = Action_DelPoseidonCurrArea(pDocument,pToGroup);
			}
		}
		CATCH_ALL(e)
		{
			MntReportMemoryException();
			if (pNameArea)
			{
				delete pNameArea;
				pNameArea = NULL;
			} 
		}
		END_CATCH_ALL
		return pAction;
	}
	return NULL;
};

// smaz�n� pojmenovan� plochy
CPosAction* Action_DeletePoseidonNameArea(CPosNameAreaObject* pToDel,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pToDel != NULL)
	{
		CPosObject* pDocObj = pDocument->GetDocumentObjectFromValue(pToDel);
		if (pDocObj)
		return pDocument->GenerNewObjectAction(IDA_NAMEAREA_DELETE,pDocObj,NULL,FALSE,pToGroup);
	}
	return NULL;
};

// p�esun pojmenovan� plochy
CPosAction* Action_MovePoseidonNameArea(CPosNameAreaObject* pArea,int xAmount,int yAmount,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pArea != NULL)
	{
		CPosNameAreaObject* pAct = (CPosNameAreaObject*)pArea->CreateCopyObject();
		if (pAct != NULL)
		{	// objekt dokumentu, se kter�m hejbu
			CPosNameAreaObject* pDocObj = (CPosNameAreaObject*)pDocument->GetDocumentObjectFromValue(pAct);
			ASSERT(pDocObj != NULL);
			ASSERT_KINDOF(CPosNameAreaObject,pAct);
			ASSERT_KINDOF(CPosNameAreaObject,pDocObj);
			// offset objektu
			pAct->GetLogObjectPosition()->OffsetPosition(xAmount,yAmount);
			// generuji ud�lost pro p�esun atributu
			return pDocument->GenerNewObjectAction(IDA_NAMEAREA_MOVE,pDocObj,pAct,FALSE,pToGroup);
		}
	}
	return NULL;
};

CPosAction* Action_RotatePoseidonNameArea(CPosNameAreaObject* pArea,float grad,CPointVal origin,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  if (pArea != NULL)
  {
    CPosNameAreaObject* pAct = (CPosNameAreaObject*)pArea->CreateCopyObject();
    if (pAct != NULL)
    {	// objekt dokumentu, se kter�m hejbu
      CPosNameAreaObject* pDocObj = (CPosNameAreaObject*)pDocument->GetDocumentObjectFromValue(pAct);
      ASSERT(pDocObj != NULL);
      ASSERT_KINDOF(CPosNameAreaObject,pAct);
      ASSERT_KINDOF(CPosNameAreaObject,pDocObj);
      // offset objektu
      FMntMatrix2 trans;
      trans.Rotation(grad);

      pAct->GetLogObjectPosition()->TransformPosition(trans,origin);
      // generuji ud�lost pro p�esun atributu
      return pDocument->GenerNewObjectAction(IDA_NAMEAREA_MOVE,pDocObj,pAct,FALSE,pToGroup);
    }
  }
  return NULL;
};


CPosAction* Action_RotateMovePoseidonNameArea(CPosNameAreaObject* pArea,CPointVal offset, float grad,CPointVal origin,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  if (pArea != NULL)
  {
    CPosNameAreaObject* pAct = (CPosNameAreaObject*)pArea->CreateCopyObject();
    if (pAct != NULL)
    {	// objekt dokumentu, se kter�m hejbu
      CPosNameAreaObject* pDocObj = (CPosNameAreaObject*)pDocument->GetDocumentObjectFromValue(pAct);
      ASSERT(pDocObj != NULL);
      ASSERT_KINDOF(CPosNameAreaObject,pAct);
      ASSERT_KINDOF(CPosNameAreaObject,pDocObj);
      // offset objektu
      FMntMatrix2 trans;
      trans.Rotation(grad);

      pAct->GetLogObjectPosition()->TransformPosition(trans,origin);
      pAct->GetLogObjectPosition()->OffsetPosition(offset.x, offset.y);
      // generuji ud�lost pro p�esun atributu
      return pDocument->GenerNewObjectAction(IDA_NAMEAREA_MOVE,pDocObj,pAct,FALSE,pToGroup);
    }
  }
  return NULL;

}

// editace pojmenovan� plochy
CPosAction* Action_EditPoseidonNameArea(CPosNameAreaObject* pArea,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pArea != NULL)
	{
		CPosNameAreaObject* pAct = (CPosNameAreaObject*)(pArea->CreateCopyObject());
		if (pAct != NULL)
		{	// objekt dokumentu, se kter�m hejbu
			CPosNameAreaObject* pDocObj = (CPosNameAreaObject*)pDocument->GetDocumentObjectFromValue(pAct);
			ASSERT(pDocObj != NULL);
			ASSERT_KINDOF(CPosNameAreaObject,pAct);
			ASSERT_KINDOF(CPosNameAreaObject,pDocObj);
			// editace objektu
			if (!DoEditNameArea(pAct,FALSE))
			{
				delete pAct;
				return NULL;
			};
			// generuji ud�lost pro editaci atributu
			return pDocument->GenerNewObjectAction(IDA_NAMEAREA_EDIT,pDocObj,pAct,FALSE,pToGroup);
		}
	}
	return NULL;
};



