#include "stdafx.h"
#include "resource.h"
#include "float.h"

// ------------------------------------------------------------ //
// added to give access to the class definition which has been //
// moved from this file                                        //
// ------------------------------------------------------------ //
#include "Actions_TextureLayers.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// --------------------------------------------------------- //
// the following class definition has been moved in the file //
// Actions_TextureLayers.h                                   //
// --------------------------------------------------------- //
/*
class CTextureLayerDlg : public CDialog
{
	DECLARE_DYNAMIC(CTextureLayerDlg)
public:
	CString m_Name;
	CString m_NameInit;
	int m_nTexSizeInSqLog;

	CPosEdMainDoc * m_pDoc;
public:
	CTextureLayerDlg(CPosEdMainDoc * pDoc, CWnd * pParentWnd = NULL) : CDialog(IDD, pParentWnd), m_nTexSizeInSqLog(0), m_pDoc(pDoc) {};
  
	// Dialog Data
	enum { IDD = IDD_EDIT_TEXTURE_LAYER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
};
*/

IMPLEMENT_DYNAMIC(CTextureLayerDlg, CDialog)

BEGIN_MESSAGE_MAP(CTextureLayerDlg, CDialog)
END_MESSAGE_MAP()

void CTextureLayerDlg::DoDataExchange(CDataExchange* pDX)
{
  DDX_Text(pDX, IDC_TEXTURE_LAYER_NAME, m_Name);
  MDDV_MinChars(pDX, m_Name, 1, IDC_TEXTURE_LAYER_NAME);

  if (m_Name != m_NameInit && m_pDoc && m_pDoc->m_PoseidonMap.GetTextureLayer(m_Name) != NULL)
  {
    AfxMessageBox(IDS_TEXTURE_LAYER_NAME_EXIST);
#ifdef Fail
#undef Fail
#endif
    pDX->Fail();
  }

  DDX_CBIndex(pDX, IDC_TEXTURE_LAYER_SIZE, m_nTexSizeInSqLog);  
}

BOOL CTextureLayerDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  CComboBox * combo = (CComboBox *) GetDlgItem(IDC_TEXTURE_LAYER_SIZE);
  if (combo != NULL)
  {

#ifdef _LIMIT_LAYERSIZE
    //don't allow layercount > 1024
    int size = m_pDoc->m_PoseidonMap.GetSize().x;
    int minValue = 1;
    while(size > 1024)
    {
      minValue++;
      size >>= 1;
    }
#endif

    for(int i = 1; i < m_pDoc->m_PoseidonMap.GetSize().x; i *= 2)
    {
      CString text;
#ifdef _LIMIT_LAYERSIZE
      if(i<minValue)
        text = _T("invalid");
      else
#endif
        text.Format(_T("%.1fx%.1fm"), m_pDoc->m_nRealSizeUnit * i,m_pDoc->m_nRealSizeUnit * i);
      combo->AddString(text);
    }
    combo->SetCurSel(m_nTexSizeInSqLog);
  }

  return TRUE;  
}

/////////////////////////////////////////////////////////////
// Create/Remove/Edit texture layer action

IMPLEMENT_DYNAMIC(CPosTextureLayerAction, CPosAction);

CPosTextureLayerAction::CPosTextureLayerAction(int nType) :
CPosAction(nType), m_bUndo(false)
{};

CPosTextureLayerAction::~CPosTextureLayerAction()
{};

BOOL CPosTextureLayerAction::ChangeValues()
{
  m_bUndo = !m_bUndo;
  return TRUE;
}

bool CPosTextureLayerAction::CreateOldData(Ref<CTextureLayer> layer)
{
  if (layer.IsNull())
    return false;

  TRY
  {
    m_OldName = layer->Name();
    m_OldTextureSizeInSqLog = layer->m_nTexSizeInSqLog;
   
    if (!layer->m_layered) 
    {
      int texSize = layer->m_nTexWidth * layer->m_nTexHeight;
      m_pOldBaseTextures = new int[texSize];

      for(int i = 0; i < texSize; i++)
      {
        Ref<CPosTextureTemplate> & tex = layer->m_pBaseTextures[i];
        m_pOldBaseTextures[i] = (tex.NotNull()) ? tex->m_nTextureID : 0;  
      }
      return true;

    }
    else
    {
      // TODO: backup old satellite mapping
      m_pOldBaseTextures = NULL;
    }
  }
  CATCH_ALL(e)
  {
    MntReportMemoryException(); 
    return false;
  }
  END_CATCH_ALL
    return false;
}

// Buldozer must be refreshed after this step.
void CPosTextureLayerAction::SetDataIntoLayer(Ref<CTextureLayer> layer)
{  
  layer->DestroyLayer();
  layer->OnNewLayer(layer->m_map.GetSize().x, layer->m_map.GetSize().z, m_OldTextureSizeInSqLog, m_OldName);

  int texSize = layer->m_nTexWidth * layer->m_nTexHeight;
  UNITPOS pos;
  TextureBank& textures = layer->m_map.m_pDocument->m_Textures;
  for(pos.z = 0; pos.z < layer->m_nTexHeight; pos.z++)
    for(pos.x = 0; pos.x < layer->m_nTexWidth; pos.x++)
    { 
      Ref<CPosTextureTemplate> tex;
      int id = m_pOldBaseTextures[pos.x + layer->m_nTexWidth * pos.z];
      if (id > 0)
        tex = textures.FindEx(id);

      layer->SetBaseTextureAt(pos, tex);
    }

  for(pos.z = 0; pos.z < layer->m_nTexHeight; pos.z++)
    for(pos.x = 0; pos.x < layer->m_nTexWidth; pos.x++)
    { 
      layer->DoUpdateSecndTexturesAt(pos, TRUE);
    }
}

/////////////////////////////////////////////////////////////
// Create texture layer action

CPosAction * Action_CreateTextureLayer(CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  CTextureLayerDlg dlg(pDocument);

  if (dlg.DoModal() != IDOK)
    return NULL;

  TRY
  {  
    CPosTextureLayerAction * pAction;
    pAction = new CPosTextureLayerAction(IDA_TEXTURE_LAYER_CREATE);

    pAction->m_CurrName = dlg.m_Name;
    pAction->m_CurrTextureSizeInSqLog = dlg.m_nTexSizeInSqLog;

    // realizuji akci ?
    if (pToGroup)
      pToGroup->AddAction(pAction);
    else
      pDocument->ProcessEditAction(pAction);

    return pAction;
  }
  CATCH_ALL(e)
  {
    MntReportMemoryException(); 
    return NULL;
  }
  END_CATCH_ALL

  return NULL;
};

////////////////////////////////////////////////////////////
// Remove texture layer action

CPosAction * Action_RemoveTextureLayer(CTextureLayer * layer, CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  TRY
  {  
    CPosTextureLayerAction * pAction;
    pAction = new CPosTextureLayerAction(IDA_TEXTURE_LAYER_REMOVE);

    pAction->CreateOldData(layer);

    // realizuji akci ?
    if (pToGroup)
      pToGroup->AddAction(pAction);
    else
      pDocument->ProcessEditAction(pAction);

    return pAction;
  }
  CATCH_ALL(e)
  {
    MntReportMemoryException(); 
    return NULL;
  }
  END_CATCH_ALL

    return NULL;
};

/////////////////////////////////////////////////////////////
// Edit texture layer action

CPosAction * Action_EditTextureLayer(CTextureLayer * layer, CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  CTextureLayerDlg dlg(pDocument);
  dlg.m_Name = dlg.m_NameInit = layer->Name();
  dlg.m_nTexSizeInSqLog = layer->m_nTexSizeInSqLog;

  if (dlg.DoModal() != IDOK)
    return NULL;

  TRY
  {  
    CPosTextureLayerAction * pAction;
    pAction = new CPosTextureLayerAction(IDA_TEXTURE_LAYER_EDIT);
    
    pAction->CreateOldData(layer);
    pAction->m_CurrName = dlg.m_Name;
    pAction->m_CurrTextureSizeInSqLog = dlg.m_nTexSizeInSqLog;

    // realizuji akci ?
    if (pToGroup)
      pToGroup->AddAction(pAction);
    else
      pDocument->ProcessEditAction(pAction);

    return pAction;
  }
  CATCH_ALL(e)
  {
    MntReportMemoryException(); 
    return NULL;
  }
  END_CATCH_ALL

    return NULL;
};



