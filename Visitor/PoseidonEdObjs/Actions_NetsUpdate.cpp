/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Akce se s�t�mi - update poseidon objekt�

BOOL DoUpdateNetPoseidonObjects(CPosNetObjectBase* pNet,CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup)
{
	int i;
	TRY
	{
		// najdu existuj�c� objekty vzta�en� k s�ti
		CObArray ExistObjects, CreateObjects;
		for (i = 0; i < pDocument->m_PoseidonMap.m_InstanceObjs.GetSize(); ++i)
		{
			CPosEdObject* pObj = (CPosEdObject*)(pDocument->m_PoseidonMap.m_InstanceObjs[i]);
			if (pObj)
			{
				ASSERT_KINDOF(CPosEdObject,pObj);
				if (pObj->m_nMasterNet == pNet->GetID())
				{	// je to objekt s�t� -> vlo��m kopii se kterou budu pracovat
					ExistObjects.Add(pObj->CreateCopyObject());
				}
			}
		}

		// existuj�c�m objekt�m nuluji p��znak pou�it�
		for (i = 0; i < ExistObjects.GetSize(); ++i)
		{
			CPosEdObject* pObj = (CPosEdObject*)(ExistObjects[i]);
			pObj->m_nTempObjCounter = 0;
		}
		// update polohy (nastaven�) existuj�c�hc objekt�
		if (ExistObjects.GetSize() > 0)
		{
			// TODO the following text allways fails why?
			/*VERIFY*/(pNet->DoUpdatePoseidonNetObjects(&ExistObjects, &(pDocument->m_PoseidonMap)));
			// vytvo�en� akc� pro update a smaz�n� objekt�
			for (i = 0; i < ExistObjects.GetSize(); ++i)
			{
				CPosEdObject* pObj = (CPosEdObject*)(ExistObjects[i]);
				ASSERT(pObj != NULL);
				if (pObj != NULL)
				{
					if (pObj->m_nTempObjCounter == 0)
					{	// objekt je nepou�it -> bude smaz�n
						CPosEdObject* pDelObj = pDocument->m_PoseidonMap.GetPoseidonObject(pObj->GetID());
						if (pDelObj != NULL)
						{
							ASSERT(pDelObj->GetID() == pObj->GetID());
							ASSERT(pDelObj->m_nMasterNet== pNet->GetID());
							CPosObjectAction* pAction = (CPosObjectAction*)Action_DeletePoseidonObject(pDelObj,pDocument,pToGroup);
							if (pAction)
							{
								// nebude se m�nit selekce
								pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
								// nebude se prov�d�t update
								pAction->m_bUpdateView	  = FALSE;
							}
						}
					} 
					else
					{	// objekt je pou�it -> bude updatov�n
						CPosEdObject* pEditObj = pDocument->m_PoseidonMap.GetPoseidonObject(pObj->GetID());
						if (pEditObj != NULL)
						{
							ASSERT(pEditObj->GetID() == pObj->GetID());
							ASSERT(pEditObj->m_nMasterNet== pNet->GetID());
							CPosObjectAction* pAction = (CPosObjectAction*)Action_EditPoseidonObject(pObj,pDocument,pToGroup);
							if (pAction)
							{
								// nebude se m�nit selekce
								pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
								// nebude se prov�d�t update
								pAction->m_bUpdateView	  = FALSE;
							}
						}
					}
				}
			}
			// ru��m zpracovan� objekty
			DestroyArrayObjects(&ExistObjects);
		}
		// vytvo�en� nov�ch (neexistuj�c�ch) objekt�
		{		
			VERIFY(pNet->DoCreatePoseidonNetObjects(&CreateObjects, &(pDocument->m_PoseidonMap)));      
			// generuji akce pro nov� objekty
			for (i = 0; i < CreateObjects.GetSize(); ++i)
			{
				CPosEdObject* pNewObj = (CPosEdObject*)(CreateObjects[i]);
				if (pNewObj)
				{
					ASSERT_KINDOF(CPosEdObject,pNewObj);
					CPosObjectAction* pAction  = (CPosObjectAction*)pDocument->GenerNewObjectAction(IDA_OBJECT_ADD,NULL,pNewObj,FALSE,pToGroup);
					if (pAction)
					{
						// objekt ji� nepot�ebuji
						CreateObjects[i] = NULL;
						// nebude se m�nit selekce
						pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
						// nebude se prov�d�t update
						pAction->m_bUpdateView = FALSE;
						// objekt je pod��zen s�ti
						ASSERT(pAction->m_pOldValue != NULL);
						ASSERT_KINDOF(CPosEdObject,pAction->m_pOldValue);
						((CPosEdObject*)(pAction->m_pOldValue))->m_nMasterNet = pNet->GetID();
					}
				}
			}
		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		return FALSE;
	}
	END_CATCH_ALL
	// v�e je OK
	return TRUE;
}