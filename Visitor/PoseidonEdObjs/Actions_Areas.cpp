/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define BOOLTobool(x) ((x)? true : false)
/////////////////////////////////////////////////////////////////////////////
// Akce s plochami

/////////////////////////////////////////////////////////////////////////////
// vytvo�en� akce pro increment aktu�ln� plochy

CPosAction* Action_AddPoseidonCurrArea(CRect& rLogArea,BOOL bExlusive, BOOL bRemove, CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup, BOOL bFromBuldzer)
{
	// vytvo��m nov� objekt
	CPosSelAreaObject* pObj = new CPosSelAreaObject(pDocument->m_PoseidonMap);
	if (bExlusive && !bRemove)
  {  
		pObj->ResetArea();
    pObj->SetSelAreaType(pDocument->m_CurrentArea.GetSelAreaType());
  }
	else
		pObj->CopyFrom(&pDocument->m_CurrentArea);
  if (bRemove)
  {
    pObj->RemoveRect(rLogArea);
    return pDocument->GenerNewObjectAction(IDA_AREA_REMOVE,
      &(pDocument->m_CurrentArea),pObj,BOOLTobool(bFromBuldzer),pToGroup);
  }
  else
  {
	  pObj->AddRect(rLogArea);
    return pDocument->GenerNewObjectAction(IDA_AREA_ADD,
      &(pDocument->m_CurrentArea),pObj,BOOLTobool(bFromBuldzer),pToGroup);
  }
	// generov�n� akce
}

CPosAction* Action_AddPoseidonCurrArea(CRectArray& rLogArea,BOOL bExlusive, BOOL bRemove, CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup, BOOL bFromBuldzer)
{
  // vytvo��m nov� objekt
  CPosSelAreaObject* pObj = new CPosSelAreaObject(pDocument->m_PoseidonMap);
  if (bExlusive && !bRemove)
  {  
    pObj->ResetArea();
    pObj->SetSelAreaType(pDocument->m_CurrentArea.GetSelAreaType());
  }
  else
    pObj->CopyFrom(&pDocument->m_CurrentArea);

  if (bRemove)
  {
    for(int i = 0; i < rLogArea.GetSize(); i++)
    {    
      pObj->RemoveRect(rLogArea[i]);
    }

    return pDocument->GenerNewObjectAction(IDA_AREA_REMOVE,
      &(pDocument->m_CurrentArea),pObj,BOOLTobool(bFromBuldzer) ,pToGroup);
  }
  else
  {
     for(int i = 0; i < rLogArea.GetSize(); i++)
    {    
      pObj->AddRect(rLogArea[i]);
    }
    return pDocument->GenerNewObjectAction(IDA_AREA_ADD,
      &(pDocument->m_CurrentArea),pObj,BOOLTobool(bFromBuldzer),pToGroup);
  }
  // generov�n� akce

};

/////////////////////////////////////////////////////////////////////////////
// vytvo�en� akce pro reset aktu�ln� plochy

CPosAction* Action_DelPoseidonCurrArea(CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup, BOOL bFromBuldzer)
{
	// vytvo��m nov� objekt
	CPosSelAreaObject* pObj = new CPosSelAreaObject(pDocument->m_PoseidonMap);
	pObj->ResetArea();
  pObj->SetSelAreaType(pDocument->m_CurrentArea.GetSelAreaType());

	// generov�n� akce
	return pDocument->GenerNewObjectAction(IDA_AREA_DEL,
			&(pDocument->m_CurrentArea),pObj,BOOLTobool(bFromBuldzer),pToGroup);
};

/////////////////////////////////////////////////////////////////////////////
// vytvo�en� akce set aktualny polohy

CPosAction* Action_SetPoseidonCurrArea(const CPosAreaObject& area, CPosEdMainDoc * pDocument,CPosActionGroup* pToGroup, BOOL bFromBuldzer)
{
  CPosSelAreaObject* pObj = new CPosSelAreaObject(pDocument->m_PoseidonMap);  
  pObj->ResetArea();
  pObj->SetSelAreaType(pDocument->m_CurrentArea.GetSelAreaType());

  pObj->CPosAreaObject::CopyFrom(&area);

  // generov�n� akce
  return pDocument->GenerNewObjectAction(IDA_AREA_DEL,
    &(pDocument->m_CurrentArea),pObj,BOOLTobool(bFromBuldzer),pToGroup);
}

/////////////////////////////////////////////////////////////////////////////
// CSelObjectAreaParamsDlg dialog

class CSelObjectAreaParamsDlg : public CDialog
{
// Construction
public:
	CSelObjectAreaParamsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSelObjectAreaParamsDlg)
	enum { IDD = IDD_OBJECT_SELAREA_PARAMS };
	int		m_nType;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelObjectAreaParamsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSelObjectAreaParamsDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CSelObjectAreaParamsDlg::CSelObjectAreaParamsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelObjectAreaParamsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelObjectAreaParamsDlg)
	m_nType = 0;
	//}}AFX_DATA_INIT
}


void CSelObjectAreaParamsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelObjectAreaParamsDlg)
	DDX_Radio(pDX, IDC_RADIO1, m_nType);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelObjectAreaParamsDlg, CDialog)
	//{{AFX_MSG_MAP(CSelObjectAreaParamsDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// vytvo�en� akce pro selekci pod objektem

void DoCreateObjectHullArea(CPosAreaObject* pObj,const CPositionBase* pPos,/*BOOL bExtended,*/CPosEdMainDoc* pDocument)
{
	CPoseidonMap* pMap = &pDocument->m_PoseidonMap;

	CRect	rLogHull = pPos->GetPositionHull();
	UNITPOS LftTop,RghBtm,nUnit;
	// obal pro logick� recty
	GetUnitPosHull(LftTop,RghBtm,rLogHull,&pDocument->m_PoseidonMap);
	// p�id�v�m rect
	CRect	rToAdd;
	BOOL	bExist = FALSE;

	for(int x=LftTop.x-1; x<=RghBtm.x+1; x++)
	{	// po sloupc�ch
		if (bExist)
		{
			pObj->AddRect(rToAdd);
			bExist = FALSE;
		};
		if ((x >= 0)&&(x < pDocument->m_PoseidonMap.GetSize().x))
		{
			nUnit.x = x; nUnit.z = 0;
			CPoint ptLog = pMap->FromRealToViewPos(pMap->FromUnitToRealPos(nUnit));
			rToAdd.left = ptLog.x;
			rToAdd.right= rToAdd.left + VIEW_LOG_UNIT_SIZE;
			
			for(int z=LftTop.z+1; z>=RghBtm.z-1; z--)
			{	
				if ((z >= 0)&&(z < pDocument->m_PoseidonMap.GetSize().z))
				{
					nUnit.x = x; nUnit.z = z;
					ptLog = pMap->FromRealToViewPos(pMap->FromUnitToRealPos(nUnit));
					
					CRect rLog; 
					rLog.left   = rToAdd.left; rLog.right = rToAdd.right;
					rLog.bottom = ptLog.y;
					rLog.top	= rLog.bottom - VIEW_LOG_UNIT_SIZE;
					// je rect v obalu objektu ?
					BOOL bIsOkRect = FALSE;
					bIsOkRect = pPos->IsRectAtArea(rLog,TRUE);
					
					/*if ((!bIsOkRect)&&(bExtended))
					{	// roz���eno otexturu ?
						CRect rPom;
						if (!bIsOkRect)
						{
							rPom = rLog; rPom.OffsetRect(-VIEW_LOG_UNIT_SIZE,0);
							bIsOkRect = pPos->IsRectAtArea(rPom,TRUE);
						}
						if (!bIsOkRect)
						{
							rPom = rLog; rPom.OffsetRect(0,+VIEW_LOG_UNIT_SIZE);
							bIsOkRect = pPos->IsRectAtArea(rPom,TRUE);
						}
						if (!bIsOkRect)
						{
							rPom = rLog; rPom.OffsetRect(-VIEW_LOG_UNIT_SIZE,+VIEW_LOG_UNIT_SIZE);
							bIsOkRect = pPos->IsRectAtArea(rPom,TRUE);
						}
					}*/

					// zpracuji rect
					if (bIsOkRect)
					{
						if (bExist)
						{	// p�id�m k p�vodn�mu
							rToAdd.bottom = rLog.bottom;
						} else
						{	// vytvo��m nov�
							rToAdd = rLog;
							bExist = TRUE;
						};
					} else
					{	// ukon��m p�vodn� rect
						if (bExist)
						{
							pObj->AddRect(rToAdd);
							bExist = FALSE;
						};
					};
				}
			}
		}
	};
	if (bExist)
	{
		pObj->AddRect(rToAdd);
		bExist = FALSE;
	};
};

CPosAction* Action_SelPoseidonCurrArea(const CPositionBase* pPos,BOOL bExlusive,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	// vytvo��m nov� objekt
	CPosSelAreaObject* pObj = new CPosSelAreaObject(pDocument->m_PoseidonMap);
	if (bExlusive)
		pObj->ResetArea();
	else
		pObj->CopyFrom(&pDocument->m_CurrentArea);
	// parametry pro v�b�r bloku
	/*CSelObjectAreaParamsDlg dlg;
	if (dlg.DoModal() != IDOK)
	{
		delete pObj;
		return NULL;
	};*/

	// v�b�r obalu objektu
	//DoCreateObjectHullArea(pObj,pPos,(dlg.m_nType!=0),pDocument);
  DoCreateObjectHullArea(pObj,pPos,pDocument);

	// generov�n� akce
	return pDocument->GenerNewObjectAction(IDA_AREA_ADD,
			&(pDocument->m_CurrentArea),pObj,FALSE,pToGroup);
};
CPosAction* Action_SelPoseidonCurrArea(CPosObject* pObj,BOOL bExlusive,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{	return Action_SelPoseidonCurrArea(pObj->GetLogObjectPosition(),bExlusive,pDocument,pToGroup); } 
