/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef Fail
#undef Fail
#endif

/////////////////////////////////////////////////////////////////////////////
// CShotSelectDlg dialog

class CShotSelectDlg : public CDialog
{
// Construction
public:
	CShotSelectDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	CPosAnimation*	m_pAnim;
	CString			m_sObjName;

	CObArray		m_SelShots;

	//{{AFX_DATA(CShotSelectDlg)
	enum { IDD = IDD_ANIM_SHOT_SELECT };
	CListBox	m_cList;
	//}}AFX_DATA


			void  OnUpdateList(DWORD dwSelect);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShotSelectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CShotSelectDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CShotSelectDlg::CShotSelectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CShotSelectDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CShotSelectDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CShotSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CShotSelectDlg)
	DDX_Control(pDX, IDC_LIST, m_cList);
	//}}AFX_DATA_MAP
	if (!SAVEDATA)
	{
		OnUpdateList(0);
		// popis objektu
		CString sText;
		AfxFormatString1(sText,IDS_SELECT_OBJ_SHOTS,m_sObjName);
		::SetWindowText(DLGCTRL(IDC_TEXT),sText);
	} else
	{
		m_SelShots.RemoveAll();

		int nCount = m_cList.GetCount();
		for(int i=0; i<nCount; i++)
		{
			if (m_cList.GetSel(i) != 0)
			{
				m_SelShots.Add((CObject*)(m_cList.GetItemData(i)));
			}
		}
	};
}

void CShotSelectDlg::OnUpdateList(DWORD dwSelect)
{
	if ((dwSelect == 0)&&(m_cList.GetCurSel()!=LB_ERR))
		LBGetCurSelData(m_cList.m_hWnd,dwSelect);

	m_cList.ResetContent();
	// sroruji sn�mky
	CObArray sortShots;
	for(int i=0; i<m_pAnim->m_AnimShots.GetSize(); i++)
	{
		CPosAnimShot* pShot = (CPosAnimShot*)(m_pAnim->m_AnimShots[i]);

		BOOL bAdd = FALSE;
		for(int j=0; j<sortShots.GetSize(); j++)
		{
			CPosAnimShot* pShotOld = (CPosAnimShot*)(sortShots[j]);
			if (pShotOld->m_nShotTime > pShot->m_nShotTime)
			{
				sortShots.InsertAt(j,pShot);
				bAdd = TRUE;
				break;
			}
		}
		if (!bAdd)
			sortShots.Add(pShot);
	}
		
	
	for(i=0; i<sortShots.GetSize(); i++)
	{
		CPosAnimShot* pShot = (CPosAnimShot*)(sortShots[i]);
		if (pShot)
		{
			CString sText;
			NumToLocalString(sText,pShot->m_nShotTime,3);
			sText += _T("\t : ");
			sText += pShot->m_sShotName;
			LBAddString(m_cList.m_hWnd, sText, (DWORD)pShot);
		}
	}

	if (m_cList.GetCount()>0)
	{
		m_cList.SetCurSel(0);
		if (dwSelect != 0)
			LBSetCurSelData(m_cList.m_hWnd,dwSelect);
	}
};

BEGIN_MESSAGE_MAP(CShotSelectDlg, CDialog)
	//{{AFX_MSG_MAP(CShotSelectDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CEditAnimObjectDlg dialog

class CEditAnimObjectDlg : public CDialog
{
// Construction
public:
	CEditAnimObjectDlg(CWnd* pParent = NULL);   // standard constructor

	CString					m_sOldName;
	CPosAnimObject* m_pObject;
	CPosAnimation*	m_pAnim;

// Dialog Data
	//{{AFX_DATA(CEditAnimObjectDlg)
	enum { IDD = IDD_ANIM_OBJS_PARAMS };
	CString	m_sObjName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditAnimObjectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEditAnimObjectDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CEditAnimObjectDlg::CEditAnimObjectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEditAnimObjectDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditAnimObjectDlg)
	m_sObjName = _T("");
	//}}AFX_DATA_INIT
}


void CEditAnimObjectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditAnimObjectDlg)
	//}}AFX_DATA_MAP
	if (SAVEDATA)
	{
		MDDX_Text(pDX, IDC_EDIT_OBJ_NAME, m_sObjName);
		MDDV_MaxChars(pDX, m_sObjName, 20, IDC_EDIT_OBJ_NAME);
		MDDV_MinChars(pDX, m_sObjName, 1);
		// jm�no mus� b�t jednozna�n�
		if (lstrcmp(m_sObjName,m_sOldName)!=0)
		{
			for(int i=0; i<m_pAnim->m_AnimObjects.GetSize(); i++)
			{
				CPosAnimObject* pObj = (CPosAnimObject*)(m_pAnim->m_AnimObjects[i]);
				if (pObj != NULL)
				{
					if (lstrcmp(m_sObjName,pObj->m_sObjName)==0)
					{
						AfxMessageBox(IDS_NOT_EXTRA_ANIM_NAME,MB_OK|MB_ICONEXCLAMATION);
						pDX->Fail();
					}
				}
			}
		}
	} else
	{
		MDDX_Text(pDX, IDC_EDIT_OBJ_NAME, m_sObjName);
		MDDV_MaxChars(pDX, m_sObjName, 20, IDC_EDIT_OBJ_NAME);
		// zobraz�m typ objektu
		if ((m_pObject != NULL)&&(m_pObject->m_pObjTemplate != NULL))
			::SetWindowText(DLGCTRL(IDC_SHOW_OBJ_TYPE),m_pObject->m_pObjTemplate->m_sObjFile);
	};
}


BEGIN_MESSAGE_MAP(CEditAnimObjectDlg, CDialog)
	//{{AFX_MSG_MAP(CEditAnimObjectDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Editace objektu animace

BOOL DoEditAnimObject(CPosAnimObject* pObject,CPosAnimation* pAnim,BOOL bNew)
{
	CEditAnimObjectDlg dlg;
	dlg.m_sObjName  =
	dlg.m_sOldName	= pObject->m_sObjName;
	dlg.m_pObject		= pObject;
	dlg.m_pAnim			= pAnim;

	if (dlg.DoModal()!=IDOK)
		return FALSE;

	if((!bNew)&&
		 (lstrcmp(dlg.m_sObjName,dlg.m_sOldName)==0)
		)
		return FALSE;	// zadna zmena

	// kopie data
	pObject->m_sObjName = dlg.m_sObjName;

	return TRUE;
};

/////////////////////////////////////////////////////////////////////////////
// CAnimShotParamsDlg dialog

class CAnimShotParamsDlg : public CDialog
{
// Construction
public:
	CAnimShotParamsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAnimShotParamsDlg)
	enum { IDD = IDD_ANIM_SHOT_PARAMS };
	CString	m_sObjName;
	double	m_nAnimTime;
	//}}AFX_DATA
	CString					m_sOldName;
	CPosAnimShot*		m_pShot;
	CPosAnimation*	m_pAnim;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAnimShotParamsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAnimShotParamsDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// CAnimShotParamsDlg dialog


CAnimShotParamsDlg::CAnimShotParamsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAnimShotParamsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAnimShotParamsDlg)
	m_nAnimTime = 0.0;
	//}}AFX_DATA_INIT
}


void CAnimShotParamsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAnimShotParamsDlg)
	//}}AFX_DATA_MAP
	MDDX_Text(pDX, IDC_EDIT_TIME, m_nAnimTime);
	MDDV_MinMaxDouble(pDX, m_nAnimTime, 0., 10000., IDC_EDIT_TIME, 1.);

	if (SAVEDATA)
	{
		MDDX_Text(pDX, IDC_EDIT_OBJ_NAME, m_sObjName);
		MDDV_MaxChars(pDX, m_sObjName, 20, IDC_EDIT_OBJ_NAME);
//		MDDV_MinChars(pDX, m_sObjName, 1);
		// jm�no mus� b�t jednozna�n�
		if (m_sObjName.GetLength()>0)
		{
			if (lstrcmp(m_sObjName,m_sOldName)!=0)
			{
				for(int i=0; i<m_pAnim->m_AnimShots.GetSize(); i++)
				{
					CPosAnimShot* pObj = (CPosAnimShot*)(m_pAnim->m_AnimShots[i]);
					if (pObj != NULL)
					{
						if (lstrcmp(m_sObjName,pObj->m_sShotName)==0)
						{
							AfxMessageBox(IDS_NOT_EXTRA_SHOT_NAME,MB_OK|MB_ICONEXCLAMATION);
							pDX->Fail();
						}
					}
				}
			}
		}
	} else
	{
		MDDX_Text(pDX, IDC_EDIT_OBJ_NAME, m_sObjName);
		MDDV_MaxChars(pDX, m_sObjName, 20, IDC_EDIT_OBJ_NAME);
	};
}


BEGIN_MESSAGE_MAP(CAnimShotParamsDlg, CDialog)
	//{{AFX_MSG_MAP(CAnimShotParamsDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Editace sn�mku animace

BOOL DoEditAnimShot(CPosAnimShot* pShot,CPosAnimation* pAnim,BOOL bNew)
{
	CAnimShotParamsDlg dlg;
	dlg.m_sObjName  =
	dlg.m_sOldName	= pShot->m_sShotName;
	dlg.m_nAnimTime = pShot->m_nShotTime;
	dlg.m_pShot		  = pShot;
	dlg.m_pAnim			= pAnim;
	

	if (dlg.DoModal()!=IDOK)
		return FALSE;

	if((!bNew)&&
		 (lstrcmp(dlg.m_sObjName,dlg.m_sOldName)==0)&&
		 (dlg.m_nAnimTime == pShot->m_nShotTime)
		)
		return FALSE;	// zadna zmena

	// kopie data
	pShot->m_sShotName = dlg.m_sObjName;
	pShot->m_nShotTime = dlg.m_nAnimTime;
	return TRUE;
};

/////////////////////////////////////////////////////////////////////////////
// CAnimParamsPage0 dialog

class CAnimParamsPage0 : public CMntPropertyPage
{
// Construction
public:
	CAnimParamsPage0();
	~CAnimParamsPage0();

// Dialog Data
	CPosAnimation		m_Anim;

	//{{AFX_DATA(CAnimParamsPage0)
	enum { IDD = IDD_ANIM_PARAMS_PAGE0 };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CAnimParamsPage0)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CAnimParamsPage0)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

CAnimParamsPage0::CAnimParamsPage0() : CMntPropertyPage(CAnimParamsPage0::IDD)
{
	//{{AFX_DATA_INIT(CAnimParamsPage0)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CAnimParamsPage0::~CAnimParamsPage0()
{
}

void CAnimParamsPage0::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAnimParamsPage0)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
	if (!SAVEDATA)
	{
		::SetWindowText(DLGCTRL(IDC_FILE_NAME),m_Anim.m_sAnimFileName);
	} else
	{
	};
}


BEGIN_MESSAGE_MAP(CAnimParamsPage0, CMntPropertyPage)
	//{{AFX_MSG_MAP(CAnimParamsPage0)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAnimParamsPage1 dialog

class CAnimParamsPage1 : public CMntPropertyPage
{
// Construction
public:
	CAnimParamsPage1();
	~CAnimParamsPage1();

// Dialog Data
	CPosAnimation		m_Anim;

	//{{AFX_DATA(CAnimParamsPage1)
	enum { IDD = IDD_ANIM_PARAMS_PAGE1 };
	CListBox	m_cList;
	//}}AFX_DATA


// Overrides
					void OnUpdateList(DWORD dwSelect);

	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CAnimParamsPage1)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CAnimParamsPage1)
	afx_msg void OnDelete();
	afx_msg void OnEdit();
	afx_msg void OnUpdateObjPos();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

CAnimParamsPage1::CAnimParamsPage1() : CMntPropertyPage(CAnimParamsPage1::IDD)
{
	//{{AFX_DATA_INIT(CAnimParamsPage1)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CAnimParamsPage1::~CAnimParamsPage1()
{
}

void CAnimParamsPage1::OnUpdateList(DWORD dwSelect)
{
	if ((dwSelect == 0)&&(m_cList.GetCurSel()!=LB_ERR))
		LBGetCurSelData(m_cList.m_hWnd,dwSelect);
		
	m_cList.ResetContent();
	for(int i=0; i<m_Anim.m_AnimObjects.GetSize(); i++)
	{
		CPosAnimObject* pObj = (CPosAnimObject*)(m_Anim.m_AnimObjects[i]);
		if (pObj)
		{
			CString sName = pObj->m_sObjName;
			sName += _T(" ( ");
			sName += pObj->m_pObjTemplate->m_sObjFile;
			sName += _T(" )");
			LBAddString(m_cList.m_hWnd, sName, (DWORD)pObj);
		}
	}

	if (m_cList.GetCount()>0)
	{
		m_cList.SetCurSel(0);
		if (dwSelect != 0)
			LBSetCurSelData(m_cList.m_hWnd,dwSelect);
	}
};

void CAnimParamsPage1::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAnimParamsPage1)
	DDX_Control(pDX, IDC_LIST, m_cList);
	//}}AFX_DATA_MAP

	if (!SAVEDATA)
	{
		OnUpdateList(0);
	}
}


BEGIN_MESSAGE_MAP(CAnimParamsPage1, CMntPropertyPage)
	//{{AFX_MSG_MAP(CAnimParamsPage1)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_EDIT, OnEdit)
	ON_LBN_DBLCLK(IDC_LIST, OnEdit)
	ON_BN_CLICKED(IDC_UPDATE_OBJPOS, OnUpdateObjPos)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CAnimParamsPage1::OnEdit() 
{
	DWORD dwData;
	if (LBGetCurSelData(m_cList.m_hWnd,dwData))
	{
		for(int i=0; i<m_Anim.m_AnimObjects.GetSize(); i++)
		{
			CPosAnimObject* pObj = (CPosAnimObject*)(m_Anim.m_AnimObjects[i]);
			if ((pObj!=NULL)&&(pObj == (CPosAnimObject*)dwData))
			{
				if (DoEditAnimObject(pObj,&m_Anim,FALSE))
				{
					OnUpdateList((DWORD)pObj);
				}
			}
		}
	}
}

void CAnimParamsPage1::OnDelete() 
{
	DWORD dwData;
	if (LBGetCurSelData(m_cList.m_hWnd,dwData))
	{
		for(int i=0; i<m_Anim.m_AnimObjects.GetSize(); i++)
		{
			CPosAnimObject* pObj = (CPosAnimObject*)(m_Anim.m_AnimObjects[i]);
			if ((pObj!=NULL)&&(pObj == (CPosAnimObject*)dwData))
			{
				m_Anim.m_AnimObjects.RemoveAt(i);
				delete pObj;
				OnUpdateList(0);
			}
		}
	}
}

void CAnimParamsPage1::OnUpdateObjPos() 
{
	// akut�ln� objekt
	CPosAnimObject* pObj = NULL;
	DWORD dwData;
	if (LBGetCurSelData(m_cList.m_hWnd,dwData))
		pObj = (CPosAnimObject*)dwData;
	if (pObj == NULL)
	{
		MessageBeep(MB_DEFAULT);
		return;
	};

	// v�b�r sn�mk�
	CShotSelectDlg	dlg;
	dlg.m_pAnim = &m_Anim;
	dlg.m_sObjName = pObj->m_sObjName;
	if (dlg.DoModal()!=IDOK)
		return;
	// realizace propagace
	for(int i=0; i<dlg.m_SelShots.GetSize(); i++)
	{
		CPosAnimShot* pShot = (CPosAnimShot*)(dlg.m_SelShots[i]);
		if (pShot != NULL)
		{
			ASSERT_KINDOF(CPosAnimShot,pShot);
			// index sn�mku
			int nIndx = m_Anim.GetShotIndex(pShot);
			if ((nIndx >= 0)&&(nIndx < pObj->m_ShotsPositions.GetSize()))
			{
				CPosAnimObjPosition* pPos = (CPosAnimObjPosition*)(pObj->m_ShotsPositions[nIndx]);
				if (pPos != NULL)
				{
					pPos->CopyFrom(&(pObj->m_CurrentPosition));
				}
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CAnimParamsPage2 dialog

class CAnimParamsPage2 : public CMntPropertyPage
{
// Construction
public:
	CAnimParamsPage2();
	~CAnimParamsPage2();

// Dialog Data
	CPosAnimation*	m_pAnim;

	//{{AFX_DATA(CAnimParamsPage2)
	enum { IDD = IDD_ANIM_PARAMS_PAGE2 };
	CListBox	m_cList;
	//}}AFX_DATA

						void  OnUpdateList(DWORD dwSelect);

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CAnimParamsPage2)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CAnimParamsPage2)
	afx_msg void OnEdit();
	afx_msg void OnDelete();
	afx_msg void OnNew();
	afx_msg void OnUpdateObjects();
	afx_msg void OnUpdateShot();
	afx_msg void OnDblclkList();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

CAnimParamsPage2::CAnimParamsPage2() : CMntPropertyPage(CAnimParamsPage2::IDD)
{
	//{{AFX_DATA_INIT(CAnimParamsPage2)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CAnimParamsPage2::~CAnimParamsPage2()
{
}

void CAnimParamsPage2::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAnimParamsPage2)
	DDX_Control(pDX, IDC_LIST, m_cList);
	//}}AFX_DATA_MAP
	if (!SAVEDATA)
	{
		OnUpdateList(0);
	}
}

void CAnimParamsPage2::OnUpdateList(DWORD dwSelect)
{
	if ((dwSelect == 0)&&(m_cList.GetCurSel()!=LB_ERR))
		LBGetCurSelData(m_cList.m_hWnd,dwSelect);

	m_cList.ResetContent();
	// sroruji sn�mky
	CObArray sortShots;
	for(int i=0; i<m_pAnim->m_AnimShots.GetSize(); i++)
	{
		CPosAnimShot* pShot = (CPosAnimShot*)(m_pAnim->m_AnimShots[i]);

		BOOL bAdd = FALSE;
		for(int j=0; j<sortShots.GetSize(); j++)
		{
			CPosAnimShot* pShotOld = (CPosAnimShot*)(sortShots[j]);
			if (pShotOld->m_nShotTime > pShot->m_nShotTime)
			{
				sortShots.InsertAt(j,pShot);
				bAdd = TRUE;
				break;
			}
		}
		if (!bAdd)
			sortShots.Add(pShot);
	}
		
	
	for(i=0; i<sortShots.GetSize(); i++)
	{
		CPosAnimShot* pShot = (CPosAnimShot*)(sortShots[i]);
		if (pShot)
		{
			CString sText;
			NumToLocalString(sText,pShot->m_nShotTime,3);
			sText += _T("\t : ");
			sText += pShot->m_sShotName;
			LBAddString(m_cList.m_hWnd, sText, (DWORD)pShot);
		}
	}

	if (m_cList.GetCount()>0)
	{
		m_cList.SetCurSel(0);
		if (dwSelect != 0)
			LBSetCurSelData(m_cList.m_hWnd,dwSelect);
	}
};

BEGIN_MESSAGE_MAP(CAnimParamsPage2, CMntPropertyPage)
	//{{AFX_MSG_MAP(CAnimParamsPage2)
	ON_BN_CLICKED(IDC_EDIT, OnEdit)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_NEW, OnNew)
	ON_BN_CLICKED(IDC_UPDATE_OBJECTS, OnUpdateObjects)
	ON_BN_CLICKED(IDC_UPDATE_SHOT, OnUpdateShot)
	ON_LBN_DBLCLK(IDC_LIST, OnDblclkList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CAnimParamsPage2::OnNew() 
{
	if (m_pAnim->AddNewShotFromCurrPos())
	{
		CPosAnimShot* pShot = (CPosAnimShot*)(m_pAnim->m_AnimShots[m_pAnim->m_AnimShots.GetSize()-1]);
		OnUpdateList((DWORD)pShot);
	}
}

void CAnimParamsPage2::OnEdit() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_cList.m_hWnd,dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	};
	CPosAnimShot* pShot = (CPosAnimShot*)dwData;
	if (pShot == NULL)
	{
		MessageBeep(MB_DEFAULT);
		return;
	};
	// upravy snimku
	if (DoEditAnimShot(pShot,m_pAnim,FALSE))
	{
		OnUpdateList((DWORD)pShot);
	}
}

void CAnimParamsPage2::OnDelete() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_cList.m_hWnd,dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	};
	CPosAnimShot* pShot = (CPosAnimShot*)dwData;
	if (pShot == NULL)
	{
		MessageBeep(MB_DEFAULT);
		return;
	};
	// smazat ?
	if (AfxMessageBox(IDS_VERIFY_DELETE_SHOT,MB_YESNO)!=IDYES)
		return;
	// najdu index
	int nIndx = m_pAnim->GetShotIndex((CPosAnimShot*)dwData);
	if (nIndx >= 0)
		m_pAnim->DeleteShot(nIndx);
	OnUpdateList(0);
}

void CAnimParamsPage2::OnUpdateObjects() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_cList.m_hWnd,dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	};
	CPosAnimShot* pShot = (CPosAnimShot*)dwData;
	if (pShot == NULL)
	{
		MessageBeep(MB_DEFAULT);
		return;
	};
	// najdu index snimku
	int nIndx = m_pAnim->GetShotIndex((CPosAnimShot*)dwData);
	if (nIndx >= 0)
	{
		m_pAnim->UpdateCurrentFromShot(nIndx);
	}
}

void CAnimParamsPage2::OnUpdateShot() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_cList.m_hWnd,dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	};
	CPosAnimShot* pShot = (CPosAnimShot*)dwData;
	if (pShot == NULL)
	{
		MessageBeep(MB_DEFAULT);
		return;
	};
	// najdu index snimku
	int nIndx = m_pAnim->GetShotIndex((CPosAnimShot*)dwData);
	if (nIndx >= 0)
	{
		m_pAnim->UpdateShotFromCurrent(nIndx);
	}
}

void CAnimParamsPage2::OnDblclkList() 
{
	OnUpdateObjects();	
	EndDialog(IDOK);
}

/////////////////////////////////////////////////////////////////////////////
// Parametry animace

BOOL  CPosAnimation::DoEditParams()
{
		CAnimParamsPage0	page0;
		CAnimParamsPage1	page1;
		CAnimParamsPage2	page2;
		CMntPropertySheet	dlg(IDS_ANIM_PARAMS_TITLE,PSH_NOAPPLYNOW);
		dlg.AddPageIntoWizard(&page2);
		dlg.AddPageIntoWizard(&page1);
		dlg.AddPageIntoWizard(&page0);

		// kopie dat do str�nek
		page0.m_Anim.CopyFrom(this);
		page1.m_Anim.CopyFrom(this);
		page2.m_pAnim = &(page1.m_Anim);

		if (dlg.DoModal()!=IDOK)
			return FALSE;
		// kopie dat ze str�nek do animace
		CopyFrom(&(page1.m_Anim));
		//CopyAnimObject(&(page1.m_Anim));

		return TRUE;
};












