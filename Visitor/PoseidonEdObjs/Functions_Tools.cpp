/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Pomocn� funkce

// obal unit bod� pro rect log. sou�adnic�ch
void GetUnitPosHull(UNITPOS& LftTop,UNITPOS& RghBtm,const CRect& rArea,const CPoseidonMap* pMap)
{
	pMap->GetUnitPosHull(LftTop,RghBtm,rArea);
/*
	REALPOS rPos = pMap->FromViewToRealPos(rArea.TopLeft());
	// zaokrouhl�m na 50 m
	rPos.x = (float)(((int)(rPos.x / pMap->m_pEnvironment->m_optSystem.m_nRealSizeUnit))*pMap->m_pEnvironment->m_optSystem.m_nRealSizeUnit);
	rPos.z = (float)(((int)((rPos.z-0.01) / pMap->m_pEnvironment->m_optSystem.m_nRealSizeUnit))*pMap->m_pEnvironment->m_optSystem.m_nRealSizeUnit);
	LftTop = pMap->FromRealToUnitPos(rPos);	

	rPos = pMap->FromViewToRealPos(rArea.BottomRight());
	rPos.x = (float)(((int)((rPos.x-0.01) / pMap->m_pEnvironment->m_optSystem.m_nRealSizeUnit))*pMap->m_pEnvironment->m_optSystem.m_nRealSizeUnit);
	rPos.z = (float)(((int)(rPos.z / pMap->m_pEnvironment->m_optSystem.m_nRealSizeUnit))*pMap->m_pEnvironment->m_optSystem.m_nRealSizeUnit);
	RghBtm = pMap->FromRealToUnitPos(rPos);	
*/
};

// je st�ed UNIT �tverce v plo�e
BOOL IsUnitCenterAtArea(UNITPOS nUnit,const CPositionArea* pArea,const CPoseidonMap* pMap)
{
	return pMap->IsUnitCenterAtArea(nUnit,pArea);
/*
	if (!pMap->IsAtWorld(nUnit))
		return FALSE;

	float   nUnitSz2= ((float)pMap->m_pEnvironment->m_optSystem.m_nRealSizeUnit)/2.f;
	REALPOS nReal = pMap->FromUnitToRealPos(nUnit);
	nReal.x += nUnitSz2;	// um�st�m na st�ed
	nReal.z += nUnitSz2;
	// p�evedu na log.
	POINT	nLog  = pMap->FromRealToViewPos(nReal);
	return  pArea->IsPointAtArea(nLog);
*/
};

// p�ekr�v� se UNIT �tverec s plochou
BOOL IsUnitRectAtArea(UNITPOS Pos,const CPositionArea*,const CPoseidonMap*)
{
	return FALSE;
};

/////////////////////////////////////////////////////////////////////////////
// provede update sekund�rn�ch textur p�i zm�n� textury na UNITPOS Pos

void DoUpdateSecndTexturesByChangeOb(UNITPOS Pos,CPosEdMainDoc* pDocument, Ref<CTextureLayer> layer)
{
	UNITPOS ptAt;
	// update posti�en�ch textur
  for(ptAt.x = Pos.x - 1; ptAt.x <= Pos.x + 1; ptAt.x++)
    for(ptAt.z = Pos.z - 1; ptAt.z <= Pos.z + 1; ptAt.z++)
    {
      layer->DoUpdateSecndTexturesAt(ptAt);
    }

	/*ptAt = Pos;
	layer->DoUpdateSecndTexturesAt(ptAt);
	ptAt = Pos; ptAt.x--; 
	layer->DoUpdateSecndTexturesAt(ptAt);
	ptAt = Pos; ptAt.x--; ptAt.z--;
	layer->DoUpdateSecndTexturesAt(ptAt);
	ptAt = Pos; ptAt.z--;
	layer->DoUpdateSecndTexturesAt(ptAt);
  ptAt = Pos; ptAt.z--;
  layer->DoUpdateSecndTexturesAt(ptAt);
  ptAt = Pos; ptAt.z--;
  layer->DoUpdateSecndTexturesAt(ptAt);
  ptAt = Pos; ptAt.z--;
  layer->DoUpdateSecndTexturesAt(ptAt);*/
};

// provede update sekund�rn�ch textur p�i zm�n� textury na UNITPOS Pos
void DoUpdateSecndTexturesByChange(const CPositionArea* pArea,CPosEdMainDoc* pDocument, Ref<CTextureLayer> layer)
{
	// texturov�n� plochy
	CPoseidonMap* pMap = &pDocument->m_PoseidonMap;
	float   nUnitSz2   = layer->GetTextureRealSize() /2.f;
	CRect	rLogHull;
	UNITPOS LftTop,RghBtm;
	// obal pro texturov�n� plochy
	rLogHull = pArea->GetPositionHull();
	layer->GetTextureUnitPosHull(LftTop,RghBtm,rLogHull);
	// texturov�n�
	for(int x=LftTop.x; x<=RghBtm.x; x++)
	for(int z=LftTop.z; z>=RghBtm.z; z--)
	{
		// testovan� unit
		UNITPOS nUnit = { x, z };
		REALPOS nReal = pMap->FromUnitToRealPos(nUnit);
		nReal.x += nUnitSz2;	// um�st�m na st�ed
		nReal.z += nUnitSz2;
		if (layer->IsTextureUnitCenterAtArea(nUnit,pArea))
		{	// zde se to zm�nilo
			DoUpdateSecndTexturesByChangeOb(nUnit,pDocument, layer);			
		};
	};
};


void SetSecundarTextureToMap(UNITPOS Pos,PTRTEXTURETMPLT pTxtr,CPosEdMainDoc* pDocument)
{	
	if (pTxtr.NotNull())
	{
    if (pTxtr->m_nTextureID < 0)
    {
      pDocument->m_Textures.AddUnigue(pTxtr);         
    }   
	};

	// vlo��m sekund�rn� texturu
	pDocument->m_PoseidonMap.SetLandTextureAt(Pos,pTxtr,FALSE);
};



