/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"
#include <fstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define MAX_SHOW_PART_ERRORS 3	// max. po�et zobrazen�ch chyb d�lu

/////////////////////////////////////////////////////////////////////////////
// Pomocn� funkce

void DoClearNetPartTestStates(CNetPartsList* pList, int nFrom = 0)
{
	for (int i = nFrom; i < pList->m_NetParts.GetSize(); ++i)
	{
		CNetPartSIMP* pPart = (CNetPartSIMP*)(pList->m_NetParts[i]);
		if (pPart) pPart->m_nTestState = CNetPartSIMP::tstStateUNDEF;
	}
}

void DoUpdateNetPartTestStates(CNetPartsList* pList,const CPoseidonMap* pMap)
{
	REALNETPOS nPrvHg = pList->m_nBaseRealPos;	// p�edchoz� poloha
	double	   nPrvSl = 0.;						// p�edchoz� sklon

	for (int i = 0; i < pList->m_NetParts.GetSize(); ++i)
	{
		CNetPartSIMP* pPart = (CNetPartSIMP*)(pList->m_NetParts[i]);
		if (pPart)	
		{
			pPart->m_nTestState = pPart->DoTestNetPart_All(nPrvHg, nPrvSl, pList, pMap);

      if (i == 0 && (pPart->m_nTestState & CNetPartSIMP::tstErrBadPrvSl))
      {
        pPart->m_nTestState  ^= CNetPartSIMP::tstErrBadPrvSl;
      }
			// p�edchoz� poloha a sklon podle tohoto d�lu
			nPrvHg = pPart->m_nBaseRealPos;
			nPrvSl = pPart->GetCurrentSlopeRad();
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CNetPartListBox

class /*AFX_EXT_CLASS*/ CNetPartListBox : public CFMntListBox
{
public:
	CNetPartListBox();
	~CNetPartListBox();

	virtual int  GetItemImageWidth(LPDRAWITEMSTRUCT lpDIS);
	virtual void OnDrawItemImage(CRect&, LPDRAWITEMSTRUCT lpDIS);
	virtual void OnDrawItemText(CRect&, LPDRAWITEMSTRUCT lpDIS);

	// data
	CString	m_sEndPartName;
	CBitmap m_bmpType;
	CBitmap m_bmpTest;
};

CNetPartListBox::CNetPartListBox()
{
	Init( MNTLST_USER);
	//m_nSelectType = lbSelTypeText;
	// text ro konec �seku
	m_sEndPartName.LoadString(IDS_END_NET_LIST);
	// na�ten� bitmapy
	m_bmpType.LoadBitmap(IDB_LB_NETPART_TYPE);
	m_bmpTest.LoadBitmap(IDB_LB_NETPART_TEST);
}

CNetPartListBox::~CNetPartListBox()
{
}

int	CNetPartListBox::GetItemImageWidth(LPDRAWITEMSTRUCT lpDIS)
{
	int nHg = lpDIS->rcItem.bottom - lpDIS->rcItem.top;
	return 3 * nHg;
}

#define nTypePtrns 5
#define nTestPtrns 2

void CNetPartListBox::OnDrawItemImage(CRect& rPos, LPDRAWITEMSTRUCT lpDIS)
{
	CNetPartSIMP* pPart = (CNetPartSIMP*)(lpDIS->itemData);
	// zobrazen� typu �seku
	int nType = 0;
	if (pPart == NULL)
	{	
		nType = 4;	
	} 
	else
	{
		// zobrazen� p��slu�n�ho d�lu
		ASSERT_KINDOF(CNetPartSIMP,pPart);
		if (pPart->GetNetPartType() == CNetPartBase::netPartSTRA)
			nType = 1;
		else
		if (pPart->GetNetPartType() == CNetPartBase::netPartSPEC)
			nType = 4;
		else
		if (pPart->GetNetPartType() == CNetPartBase::netPartBEND)
		{
			if ( ((CNetPartBEND*)pPart)->m_bLeftBend )
				nType = 2;
			else
				nType = 0;
		};
	};
	CSize szBmp  = GetBitmapSize((HBITMAP)(m_bmpType.m_hObject));
	CSize szPtrn(  szBmp.cx/nTypePtrns, szBmp.cy );
	POINT ptFrom;
	ptFrom.x = nType * szPtrn.cx;
	ptFrom.y = 0;   
	PaintSpecBitmapRect(lpDIS->hDC,(HBITMAP)(m_bmpType.m_hObject),
			2,rPos.top,
			ptFrom,szPtrn,SRCCOPY);
	// zobrazen� testov�n�
	if ((pPart == NULL)||(pPart->m_nTestState == CNetPartSIMP::tstStateUNDEF))
		return;
	szBmp  = GetBitmapSize((HBITMAP)(m_bmpTest.m_hObject));
	szPtrn.cx = szBmp.cx/nTestPtrns;
	szPtrn.cy = szBmp.cy;
	ptFrom.x = ((pPart->m_nTestState == CNetPartSIMP::tstStateOK)?0:1)* szPtrn.cx;
	ptFrom.y = 0;   
	PaintSpecBitmapRect(lpDIS->hDC,(HBITMAP)(m_bmpTest.m_hObject),
			rPos.left + rPos.Width()/2,rPos.top,
			ptFrom,szPtrn,SRCCOPY);
}

void CNetPartListBox::OnDrawItemText(CRect& rPos, LPDRAWITEMSTRUCT lpDIS)
{	
	CNetPartSIMP* pPart = (CNetPartSIMP*)(lpDIS->itemData);
	if (pPart == NULL)
	{	// konec �seku
		CRect rText = rPos; rText.left += 2;
		::DrawTextA(lpDIS->hDC, m_sEndPartName,-1,&rText,DT_LEFT|DT_NOPREFIX|DT_SINGLELINE|DT_VCENTER|DT_TABSTOP);
		return;
	} 
	// zobrazen� p��slu�n�ho d�lu
	ASSERT_KINDOF(CNetPartSIMP,pPart);
	CRect rText = rPos; rText.left += 2;
	::DrawTextA(lpDIS->hDC, pPart->m_nComponentName,-1,&rText,DT_LEFT|DT_NOPREFIX|DT_SINGLELINE|DT_VCENTER|DT_TABSTOP);
}

/////////////////////////////////////////////////////////////////////////////
// CEditNetListDlg dialog

class CEditNetListDlg : public CDialog
{
	// Construction
public:
	CEditNetListDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
	CNetPartsList    m_PartList;
	CPosNetTemplate* m_pNet;
	CPosEdMainDoc*   m_pDocument;

	CPosNetObjectBase* m_pMainObject;
	CNetPartsList*     m_pMainpList;

	 //{{AFX_DATA(CEditNetListDlg)
	enum { IDD = IDD_EDIT_NETLIST_OBJECT };

	CNetPartListBox m_cListMAIN;
	CListBox m_cListBENDL;
	CListBox m_cListBENDR;
	CListBox m_cListSPEC;
	CListBox m_cListSTRA;
	//}}AFX_DATA
	BOOL  m_bInterPartUpdate;
	DWORD m_LastMainSel;

	// Overrides
	void DoUpdateList(DWORD dwSel);
	void DoValidButtons();

	void OnChangePartList();
	void InsertNewComponent(CNetPartSIMP*);

	BOOL GetNetListExpImpFile(CString& sFile, BOOL bOpen);
	CNetComponent* GetNetPartSource(WORD nType, LPCTSTR pName);

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditNetListDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	void OnUpdateListMainItem(BOOL bEdit);

	// Generated message map functions
	//{{AFX_MSG(CEditNetListDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeListStra();
	afx_msg void OnSelchangeListSpec();
	afx_msg void OnSelchangeListBendL();
	afx_msg void OnSelchangeListBendR();
	afx_msg void OnInsertStra();
	afx_msg void OnInsertBendL();
	afx_msg void OnInsertBendR();
	afx_msg void OnInsertSpec();
	afx_msg void OnDelete();
	afx_msg void OnSelchangeListMain();
	afx_msg void OnDisabledSnope();
	afx_msg void OnRadioHeight();
	afx_msg void OnChangeEditHeight();
	afx_msg void OnAutoSetupHeight();
	afx_msg void OnAutoTestHeight();
	afx_msg void OnApplyList();
	//afx_msg void OnLoad();
	//afx_msg void OnSave();
	//}}AFX_MSG
  
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedExporttxt();
	afx_msg void OnBnClickedImporttxt();
	void ImportTxt(std::istream& in);
	void ExportTxt(std::ostream& out);
};

CEditNetListDlg::CEditNetListDlg(CWnd* pParent /*=NULL*/)
: CDialog(CEditNetListDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditNetListDlg)
	//}}AFX_DATA_INIT
	m_LastMainSel = 0;
	m_bInterPartUpdate = FALSE;
}

BOOL CEditNetListDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	int i;

	///////////////////////////////////////
	// po��te�n� parametry �seku

	// typ s�t�
	::SetWindowText(DLGCTRL(IDC_SHOW_NET_TYPE), m_pNet->m_sNetName);
	// sou�adnice X,Y
	CString sNum;
	NumToLocalString(sNum, Round(m_PartList.m_nBaseRealPos.nPos.x, 2));
	::SetWindowText(DLGCTRL(IDC_SHOW_BGN_X), sNum);
	NumToLocalString(sNum, Round(m_PartList.m_nBaseRealPos.nPos.z, 2));
	::SetWindowText(DLGCTRL(IDC_SHOW_BGN_Z), sNum);
	NumToLocalString(sNum, (LONG)m_PartList.m_nBaseRealPos.nGra);
	sNum += _T("�");
	::SetWindowText(DLGCTRL(IDC_SHOW_BGN_G), sNum);
	// v��ka (abs.)
	NumToLocalString(sNum, (double)(m_PartList.m_nBaseRealPos.nHgt), 3);
	sNum += _T(" m");
	::SetWindowText(DLGCTRL(IDC_SHOW_BGN_H), sNum);

	///////////////////////////////////////
	// koncov� parametry �seku

	OnChangePartList();

	///////////////////////////////////////
	// kontroly pro editaci

	// p�vodn� seznam
	DoUpdateList(0);
	// update seznamu STRA
	for (i = 0; i < m_pNet->m_NetSTRA.GetSize(); ++i)
	{
		CNetComponent* pObj = (CNetComponent*)(m_pNet->m_NetSTRA[i]);
		if (pObj) LBAddString(m_cListSTRA.m_hWnd, pObj->m_sCompName, (DWORD)pObj);
	}

	if (m_cListSTRA.GetCount() > 0) m_cListSTRA.SetCurSel(0);

	// update seznamu BEND
	for (i = 0; i < m_pNet->m_NetBEND.GetSize(); ++i)
	{
		CNetComponent* pObj = (CNetComponent*)(m_pNet->m_NetBEND[i]);
		if (pObj)
		{
			LBAddString(m_cListBENDL.m_hWnd, pObj->m_sCompName, (DWORD)pObj);
			LBAddString(m_cListBENDR.m_hWnd, pObj->m_sCompName, (DWORD)pObj);
		}
	}
	if (m_cListBENDL.GetCount() > 0) m_cListBENDL.SetCurSel(0);
	if (m_cListBENDR.GetCount() > 0) m_cListBENDR.SetCurSel(0);

	// update seznamu SPEC
	for (i = 0; i < m_pNet->m_NetSPEC.GetSize(); ++i)
	{
		CNetComponent* pObj = (CNetComponent*)(m_pNet->m_NetSPEC[i]);
		if (pObj) LBAddString(m_cListSPEC.m_hWnd, pObj->m_sCompName, (DWORD)pObj);
	}

	if (m_cListSPEC.GetCount()>0) m_cListSPEC.SetCurSel(0);
/*
	if (m_PartList.m_bAutoHeight)
	{
		::ShowWindow(DLGCTRL(),SW_HIDE);
	}
*/
	// update tla��tek
	DoValidButtons();
	return TRUE;
}

void CEditNetListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditNetListDlg)
	DDX_Control(pDX, IDC_LIST_MAIN, m_cListMAIN);
	DDX_Control(pDX, IDC_LIST_BENDL, m_cListBENDL);
	DDX_Control(pDX, IDC_LIST_BENDR, m_cListBENDR);
	DDX_Control(pDX, IDC_LIST_SPEC, m_cListSPEC);
	DDX_Control(pDX, IDC_LIST_STRA, m_cListSTRA);
	//}}AFX_DATA_MAP
	if (SAVEDATA)
	{
		// ukl�d�m parametry ?
		if (m_LastMainSel != 0)
		{
			CNetPartSIMP* pPart = (CNetPartSIMP*)m_LastMainSel;
			ASSERT_KINDOF(CNetPartSIMP, pPart);
			// zm�na parametr� pro pPart
			if (pPart->CanChangeSlope())
			{
				BOOL bDisableSnope;
				DDX_Check(pDX, IDC_DISABLED_SNOPE, bDisableSnope);
				if (pPart->IsKindOf(RUNTIME_CLASS(CNetPartSTRA)))
				{
					((CNetPartSTRA*)pPart)->m_bDisChangeSlope = bDisableSnope;
				}
				else if (pPart->IsKindOf(RUNTIME_CLASS(CNetPartSPEC)))
				{
					((CNetPartSPEC*)pPart)->m_bDisChangeSlope = bDisableSnope;
				}
			}
			// zm�na stylu zad�n� v��ky
			/*int nHeightType;
			DDX_Radio(pDX, IDC_RADIO_HGHT_AUTO, nHeightType);
			pPart->m_bAutoHeight = (nHeightType == 0);
			// zm�na hodnoty v��ky
			if (!m_PartList.m_bAutoHeight)
			{
				double nHeight;
				MDDX_Text(pDX, IDC_EDIT_HEIGHT, nHeight);
				MDDV_MinMaxDouble(pDX, nHeight, m_pNet->m_nMinHeight, m_pNet->m_nMaxHeight, IDC_EDIT_HEIGHT, 0.1);
				pPart->m_nRelaHeight = nHeight;
			} else
				pPart->m_nRelaHeight = m_PartList.m_nStdHeight;
        */
		}
	}	
	else
	{
		m_bInterPartUpdate = TRUE;
		//MDDX_Text(pDX, IDC_EDIT_HEIGHT, m_pNet->m_nMinHeight);
		//MDDV_MinMaxDouble(pDX, m_pNet->m_nMinHeight, m_pNet->m_nMinHeight, m_pNet->m_nMaxHeight, IDC_EDIT_HEIGHT, 0.1);
		m_bInterPartUpdate = FALSE;
	}
}

BEGIN_MESSAGE_MAP(CEditNetListDlg, CDialog)
	//{{AFX_MSG_MAP(CEditNetListDlg)
	ON_LBN_SELCHANGE(IDC_LIST_STRA, OnSelchangeListStra)
	ON_LBN_SELCHANGE(IDC_LIST_SPEC, OnSelchangeListSpec)
	ON_LBN_SELCHANGE(IDC_LIST_BENDL, OnSelchangeListBendL)
	ON_LBN_SELCHANGE(IDC_LIST_BENDR, OnSelchangeListBendR)
	ON_BN_CLICKED(IDC_INSERT_STRA, OnInsertStra)
	ON_BN_CLICKED(IDC_INSERT_BENDL, OnInsertBendL)
	ON_BN_CLICKED(IDC_INSERT_BENDR, OnInsertBendR)
	ON_BN_CLICKED(IDC_INSERT_SPEC, OnInsertSpec)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	ON_LBN_SELCHANGE(IDC_LIST_MAIN, OnSelchangeListMain)
	ON_BN_CLICKED(IDC_DISABLED_SNOPE, OnDisabledSnope)
	//ON_BN_CLICKED(IDC_RADIO_HGHT_AUTO, OnRadioHeight)
	ON_EN_CHANGE(IDC_EDIT_HEIGHT, OnChangeEditHeight)
	ON_BN_CLICKED(IDC_AUTO_SETUP_HEIGHT, OnAutoSetupHeight)
	ON_BN_CLICKED(IDC_AUTO_TEST_HEIGHT, OnAutoTestHeight)
	ON_BN_CLICKED(ID_APPLY_LIST, OnApplyList)
	ON_LBN_DBLCLK(IDC_LIST_STRA, OnInsertStra)
	ON_LBN_DBLCLK(IDC_LIST_BENDL, OnInsertBendL)
	ON_LBN_DBLCLK(IDC_LIST_BENDR, OnInsertBendR)
	ON_LBN_DBLCLK(IDC_LIST_SPEC, OnInsertSpec)
	//ON_BN_CLICKED(IDC_RADIO_HGHT_MANU, OnRadioHeight)
	//ON_BN_CLICKED(IDC_LOAD, OnLoad)
	//ON_BN_CLICKED(IDC_SAVE, OnSave)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_EXPORTTXT, OnBnClickedExporttxt)
	ON_BN_CLICKED(IDC_IMPORTTXT, OnBnClickedImporttxt)
END_MESSAGE_MAP()

void CEditNetListDlg::DoValidButtons()
{
	DWORD dwData;
	::EnableWindow(DLGCTRL(IDC_INSERT_STRA), LBGetCurSelData(m_cListSTRA.m_hWnd, dwData));
	::EnableWindow(DLGCTRL(IDC_INSERT_BENDL), LBGetCurSelData(m_cListBENDL.m_hWnd, dwData));
	::EnableWindow(DLGCTRL(IDC_INSERT_BENDR), LBGetCurSelData(m_cListBENDR.m_hWnd, dwData));
	::EnableWindow(DLGCTRL(IDC_INSERT_SPEC), LBGetCurSelData(m_cListSPEC.m_hWnd, dwData));
}

/////////////////////////////////////////////////////////////////////////////
// Zm�nil se seznam component

void CEditNetListDlg::OnChangePartList()
{
	// provedu update z�kladn�ch sou�ednic
	m_PartList.DoUpdateNetPartsBaseRealPos(m_PartList.m_nBaseRealPos, &(m_pDocument->m_PoseidonMap));
	// zobraz�m koncov� stav
	int nCount = m_PartList.m_NetParts.GetSize();
	if (nCount == 0)
	{	// bez objekt�
		CString sEmpty;
		::SetWindowText(DLGCTRL(IDC_SHOW_END_X), sEmpty);
		::SetWindowText(DLGCTRL(IDC_SHOW_END_Z), sEmpty);
		::SetWindowText(DLGCTRL(IDC_SHOW_END_G), sEmpty);
		::SetWindowText(DLGCTRL(IDC_SHOW_END_H), sEmpty);
	} 
	else
	{	// koncov� objekt
		CNetPartSIMP* pEndObj = (CNetPartSIMP*)(m_PartList.m_NetParts[nCount - 1]);
		ASSERT(pEndObj != NULL);
		if (pEndObj != NULL)
		{
			// sou�adnice X,Y
			CString sNum;
			NumToLocalString(sNum, Round(pEndObj->m_nBaseRealPos.nPos.x, 2));
			::SetWindowText(DLGCTRL(IDC_SHOW_END_X), sNum);
			NumToLocalString(sNum, Round(pEndObj->m_nBaseRealPos.nPos.z, 2));
			::SetWindowText(DLGCTRL(IDC_SHOW_END_Z), sNum);
			NumToLocalString(sNum, (LONG)pEndObj->m_nBaseRealPos.nGra);
			sNum += _T("�");
			::SetWindowText(DLGCTRL(IDC_SHOW_END_G), sNum);
			// v��ka (abs.)
			NumToLocalString(sNum, (double)(pEndObj->m_nBaseRealPos.nHgt), 3);
			sNum += _T(" m");
			::SetWindowText(DLGCTRL(IDC_SHOW_END_H), sNum);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// Zm�ny v seznamu STRA,BEND a SPEC

void CEditNetListDlg::OnUpdateListMainItem(BOOL bEdit)
{
	m_bInterPartUpdate = TRUE;

	DWORD dwData;
	CString sNum;

	if ((!LBGetCurSelData(m_cListMAIN.m_hWnd, dwData)) || (dwData == 0))
	{	// nen� vybr�n ��dn� prvek
		::EnableWindow(DLGCTRL(IDC_DELETE), FALSE);
		// nen� umo�n�n sklon
		CheckDlgButton(IDC_DISABLED_SNOPE, 0);
		::EnableWindow(DLGCTRL(IDC_DISABLED_SNOPE), FALSE);
		// v��ka nad povrchem
		//CheckRadioButton(IDC_RADIO_HGHT_AUTO,IDC_RADIO_HGHT_MANU,IDC_RADIO_HGHT_AUTO);
		::SetWindowText(DLGCTRL(IDC_EDIT_HEIGHT), _T(""));
		::SetWindowText(DLGCTRL(IDC_SHOW_PART_HEIGHT), _T(""));
		::SetWindowText(DLGCTRL(IDC_SHOW_PART_SLOPE), _T(""));
		::SetWindowText(DLGCTRL(IDC_SHOW_PART_STATE), _T(""));
		//::EnableWindow(DLGCTRL(IDC_EDIT_HEIGHT),FALSE);
		//::EnableWindow(DLGCTRL(IDC_RADIO_HGHT_AUTO),FALSE);
		//::EnableWindow(DLGCTRL(IDC_RADIO_HGHT_MANU),FALSE);
		m_LastMainSel = 0;
	} 
	else
	{	// vybr�n prvek
		::EnableWindow(DLGCTRL(IDC_DELETE), TRUE);
		CNetPartSIMP* pPart = (CNetPartSIMP*)dwData;
		ASSERT_KINDOF(CNetPartSIMP, pPart);
		// je umo�n�n sklon ?
		//if (!m_PartList.m_bAutoHeight)
		{
			if (pPart->CanChangeSlope())
			{
				::EnableWindow(DLGCTRL(IDC_DISABLED_SNOPE), TRUE);
				CheckDlgButton(IDC_DISABLED_SNOPE, pPart->DisChangeSlope() ? 1 : 0);
			} 
			else
			{	// nen� umo�n�n sklon -> horizont�ln�
				CheckDlgButton(IDC_DISABLED_SNOPE, 1);
				::EnableWindow(DLGCTRL(IDC_DISABLED_SNOPE), FALSE);
			}
			// v��ka nad povrchem
			::EnableWindow(DLGCTRL(IDC_EDIT_HEIGHT), TRUE);
			//::EnableWindow(DLGCTRL(IDC_RADIO_HGHT_AUTO),TRUE);
			//::EnableWindow(DLGCTRL(IDC_RADIO_HGHT_MANU),TRUE);
			//CheckRadioButton(IDC_RADIO_HGHT_AUTO,IDC_RADIO_HGHT_MANU,(pPart->m_bAutoHeight)?IDC_RADIO_HGHT_AUTO:IDC_RADIO_HGHT_MANU);

			//if (bEdit)
			//{
			//	NumToLocalString(sNum,pPart->m_nRelaHeight,5);
			//	::SetWindowText(DLGCTRL(IDC_EDIT_HEIGHT),sNum);
			//};
			// zobrazen  absolutn� v��ka a sklonu
			NumToLocalString(sNum, (double)(pPart->m_nBaseRealPos.nHgt), 3);
			::SetWindowText(DLGCTRL(IDC_SHOW_PART_HEIGHT), sNum);
			m_LastMainSel = dwData;
		}

		// sklon
		double nSlope = pPart->GetCurrentSlopeRad();
		nSlope = Round(nSlope * (360. / 6.28318530718), 3);
		while(nSlope < 0.)		nSlope += 360.;
		while(nSlope >= 360.)	nSlope -= 360.;
		if (nSlope  > 180.) nSlope -= 360.;
		NumToLocalString(sNum, nSlope, 3);
		sNum += _T("�");
		::SetWindowText(DLGCTRL(IDC_SHOW_PART_SLOPE), sNum);

		// stav d�lu
		CString sPartState;
		if (pPart->m_nTestState == CNetPartSIMP::tstStateOK)
		{
			VERIFY(sPartState.LoadString(IDS_NETPART_TEST_OK));
		} 
		else if (pPart->m_nTestState == CNetPartSIMP::tstStateUNDEF)
		{
			VERIFY(sPartState.LoadString(IDS_NETPART_TEST_UNDEF));
		} 
		else
		{
			CString	sPom;
			int     nErrCount = 0; // po�et chyb (max. MAX_SHOW_PART_ERRORS)

			if ((pPart->m_nTestState & CNetPartSIMP::tstErrBadPrvHg) && (nErrCount < MAX_SHOW_PART_ERRORS))
			{	// zobrazen� chyby
				if (nErrCount != 0)	// nov� ��dek
					sPartState += _T("\r\n");
				// vlatn� text
				VERIFY(sPom.LoadString(IDS_NETPART_TEST_PRVHG));
				sPartState += sPom;
				// chyba zaps�na
				nErrCount++;
			}
			if ((pPart->m_nTestState & CNetPartSIMP::tstErrBadNetUp) && (nErrCount < MAX_SHOW_PART_ERRORS))
			{	// zobrazen� chyby
				if (nErrCount != 0)	// nov� ��dek
					sPartState += _T("\r\n");
				// vlatn� text
				VERIFY(sPom.LoadString(IDS_NETPART_TEST_NETUP));
				sPartState += sPom;
				// chyba zaps�na
				nErrCount++;
			}
			if ((pPart->m_nTestState & CNetPartSIMP::tstErrBadNetDn) && (nErrCount < MAX_SHOW_PART_ERRORS))
			{	// zobrazen� chyby
				if (nErrCount != 0)	// nov� ��dek
					sPartState += _T("\r\n");
				// vlatn� text
				VERIFY(sPom.LoadString(IDS_NETPART_TEST_NETDN));
				sPartState += sPom;
				// chyba zaps�na
				nErrCount++;
			}
			if ((pPart->m_nTestState & CNetPartSIMP::tstErrBadSlope) && (nErrCount < MAX_SHOW_PART_ERRORS))
			{	// zobrazen� chyby
				if (nErrCount != 0)	// nov� ��dek
					sPartState += _T("\r\n");
				// vlatn� text
				VERIFY(sPom.LoadString(IDS_NETPART_TEST_SLOPE));
				sPartState += sPom;
				// chyba zaps�na
				nErrCount++;
			}
			if ((pPart->m_nTestState & CNetPartSIMP::tstErrBadPrvSl) && (nErrCount < MAX_SHOW_PART_ERRORS))
			{	// zobrazen� chyby
				if (nErrCount != 0)	// nov� ��dek
					sPartState += _T("\r\n");
				// vlatn� text
				VERIFY(sPom.LoadString(IDS_NETPART_TEST_PRVSL));
				sPartState += sPom;
				// chyba zaps�na
				nErrCount++;
			}
		}
		::SetWindowText(DLGCTRL(IDC_SHOW_PART_STATE), sPartState);
	}
	m_bInterPartUpdate = FALSE;
}

void CEditNetListDlg::OnSelchangeListMain() 
{
	if (!UpdateData(TRUE))
	{
		LBSetCurSelData(m_cListMAIN.m_hWnd, m_LastMainSel);
		return;
	}
	OnUpdateListMainItem(TRUE);
}

void CEditNetListDlg::OnSelchangeListStra() 
{
}

void CEditNetListDlg::OnSelchangeListSpec() 
{
}

void CEditNetListDlg::OnSelchangeListBendL() 
{
}

void CEditNetListDlg::OnSelchangeListBendR() 
{
}

/////////////////////////////////////////////////////////////////////////////
// vkl�d�n� objekt�

void CEditNetListDlg::InsertNewComponent(CNetPartSIMP* pNewPart)
{
	DWORD dwData;
	if (!LBGetCurSelData(m_cListMAIN.m_hWnd, dwData))
	{	
		ASSERT(FALSE);
		MessageBeep(MB_DEFAULT);
		return;
	}
	// vkl�d�m do seznamu
	if (dwData == 0)
	{	// na konec
		m_PartList.m_NetParts.Add(pNewPart);
		// reset testov�n�
		DoClearNetPartTestStates(&m_PartList, m_PartList.m_NetParts.GetSize() - 1);
	} 
	else
	{	// p�ed dwData
		for (int i = 0; i < m_PartList.m_NetParts.GetSize(); ++i)
		{
			DWORD dwFound = (DWORD)((CObject*)(m_PartList.m_NetParts[i]));
			if (dwFound == dwData)
			{
				m_PartList.m_NetParts.InsertAt(i, pNewPart);
				// reset testov�n�
				DoClearNetPartTestStates(&m_PartList, i);
				break;
			}
		}
	}
	// update parametr�
	OnChangePartList();
	// update seznamu
	DoUpdateList(dwData);
}

void CEditNetListDlg::OnInsertStra() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_cListSTRA.m_hWnd, dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	CNetSTRA* pSTRA = (CNetSTRA*)dwData;
	ASSERT(pSTRA != NULL);
	ASSERT_KINDOF(CNetSTRA, pSTRA);
	// vytvo�en� podle pSTRA
	CNetPartSTRA* pNew = new CNetPartSTRA();
	if (pNew == NULL) return;
	pNew->CreateFromTemplateSTRA(pSTRA, &m_PartList);
	// vlo�en� komponenty
	InsertNewComponent(pNew);
}

void CEditNetListDlg::OnInsertSpec() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_cListSPEC.m_hWnd, dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	CNetSPEC* pSPEC = (CNetSPEC*)dwData;
	ASSERT(pSPEC != NULL);
	ASSERT_KINDOF(CNetSPEC, pSPEC);
	// vytvo�en� podle pSPEC
	CNetPartSPEC* pNew = new CNetPartSPEC();
	if (pNew == NULL) return;
	pNew->CreateFromTemplateSPEC(pSPEC, &m_PartList);
	// vlo�en� komponenty
	InsertNewComponent(pNew);
}

void CEditNetListDlg::OnInsertBendL() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_cListBENDL.m_hWnd, dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	CNetBEND* pBEND = (CNetBEND*)dwData;
	ASSERT(pBEND != NULL);
	ASSERT_KINDOF(CNetBEND, pBEND);
	// vytvo�en� podle pBEND
	CNetPartBEND* pNew = new CNetPartBEND();
	if (pNew == NULL) return;
	pNew->CreateFromTemplateBEND(pBEND, TRUE/*bLeft*/, &m_PartList);
	// vlo�en� komponenty
	InsertNewComponent(pNew);
}

void CEditNetListDlg::OnInsertBendR() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_cListBENDR.m_hWnd, dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	CNetBEND* pBEND = (CNetBEND*)dwData;
	ASSERT(pBEND != NULL);
	ASSERT_KINDOF(CNetBEND, pBEND);
	// vytvo�en� podle pBEND
	CNetPartBEND* pNew = new CNetPartBEND();
	if (pNew == NULL) return;
	pNew->CreateFromTemplateBEND(pBEND, FALSE/*bLeft*/, &m_PartList);
	// vlo�en� komponenty
	InsertNewComponent(pNew);
}

void CEditNetListDlg::OnDelete() 
{
	DWORD dwData;
	if ((!LBGetCurSelData(m_cListMAIN.m_hWnd, dwData)) || (dwData == 0))
	{	
		MessageBeep(MB_DEFAULT);
		return;
	}
	// najdu objekt ke smaz�n�
	for (int i = 0; i < m_PartList.m_NetParts.GetSize(); ++i)
	{
		DWORD dwFound = (DWORD)((CObject*)(m_PartList.m_NetParts[i]));
		if (dwFound == dwData)
		{
			CNetPartSIMP* pObjDel = (CNetPartSIMP*)(m_PartList.m_NetParts[i]);
			ASSERT(pObjDel != NULL);
			if (pObjDel != NULL)
			{
				delete pObjDel;
				m_PartList.m_NetParts.RemoveAt(i);
				// reset testov�n�
				DoClearNetPartTestStates(&m_PartList, i);
				// nov� v�b�r
				DWORD dwSel = 0;
				if (i < m_PartList.m_NetParts.GetSize()) dwSel = (DWORD)(m_PartList.m_NetParts[i]);
				// to co bylo vybr�no ji� nen�
				m_LastMainSel = 0;
				// update parametr�
				OnChangePartList();
				// update seznamu
				DoUpdateList(dwSel);
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// zm�ny v parametrech PART

void CEditNetListDlg::OnDisabledSnope() 
{
	if (!m_bInterPartUpdate)
	{
		if (UpdateData(TRUE))
		{
			// reset testov�n�
			for (int i = 0; i < m_PartList.m_NetParts.GetSize(); ++i)
			{
				DWORD dwFound = (DWORD)((CObject*)(m_PartList.m_NetParts[i]));
				if (dwFound == m_LastMainSel)
				{
					DoClearNetPartTestStates(&m_PartList, i);
					m_cListMAIN.Invalidate();	
					break;
				}
			}
			// update parametr�
			OnChangePartList();
			OnUpdateListMainItem(FALSE);
		}
	}
}

void CEditNetListDlg::OnRadioHeight() 
{
	if (!m_bInterPartUpdate)
	{
		if (UpdateData(TRUE))
		{
			// reset testov�n�
			for (int i = 0; i < m_PartList.m_NetParts.GetSize(); ++i)
			{
				DWORD dwFound = (DWORD)((CObject*)(m_PartList.m_NetParts[i]));
				if (dwFound == m_LastMainSel)
				{
					DoClearNetPartTestStates(&m_PartList, i);
					m_cListMAIN.Invalidate();	
					break;
				}
			}
			// update parametr�
			OnChangePartList();
			OnUpdateListMainItem(FALSE);
		}
	}
}

void CEditNetListDlg::OnChangeEditHeight() 
{
	if (!m_bInterPartUpdate)
	{
		// nastav�m manu�l
		// CheckRadioButton(IDC_RADIO_HGHT_AUTO,IDC_RADIO_HGHT_MANU,IDC_RADIO_HGHT_MANU);
		if (UpdateData(TRUE))
		{
			// reset testov�n�
			for (int i = 0; i < m_PartList.m_NetParts.GetSize(); ++i)
			{
				DWORD dwFound = (DWORD)((CObject*)(m_PartList.m_NetParts[i]));
				if (dwFound == m_LastMainSel)
				{
					DoClearNetPartTestStates(&m_PartList, i);
					m_cListMAIN.Invalidate();	
					break;
				}
			}
			// update parametr�
			OnChangePartList();
			OnUpdateListMainItem(FALSE);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// zm�ny v seznamu MAIN

void CEditNetListDlg::DoUpdateList(DWORD dwSel)
{
	// reset seznamu
	m_cListMAIN.ResetContent();
	// vlo�en� objekt�
	for (int i = 0; i < m_PartList.m_NetParts.GetSize(); ++i)
	{
		CNetPartSIMP* pPart = (CNetPartSIMP*)(m_PartList.m_NetParts[i]);
		if (pPart) LBAddString(m_cListMAIN.m_hWnd, pPart->m_nComponentName, (DWORD)pPart);		
	}
	// vlo�en� koncov�ho prvku
	CString sEndList; 
	VERIFY(sEndList.LoadString(IDS_END_NET_LIST));
	LBAddString(m_cListMAIN.m_hWnd, sEndList, 0L);
	// v�b�r prvku
	m_cListMAIN.SetCurSel(0);
	LBSetCurSelData(m_cListMAIN.m_hWnd, dwSel);
	// update dat prvku
	OnSelchangeListMain();
}

/////////////////////////////////////////////////////////////////////////////
// Testov�n� a nastaven� �seku

extern BOOL DoAutoNetHeightFunction(const CNetPartsList* pList, const CPoseidonMap*, BOOL bFixEnd, BOOL& bAutoTest);

void CEditNetListDlg::OnAutoSetupHeight() 
{
	if (m_PartList.m_NetParts.GetSize() < 1)
	{	// nen� co nastavovat
		MessageBeep(MB_DEFAULT);
		return;
	}

	BOOL bAutoTest = FALSE;
	if (!DoAutoNetHeightFunction(&m_PartList, &(m_pDocument->m_PoseidonMap), FALSE, bAutoTest)) return;
	// update zm�n
	OnChangePartList();
	// auto test
	if (bAutoTest) DoUpdateNetPartTestStates(&m_PartList, &(m_pDocument->m_PoseidonMap));
	// update parametru
	m_cListMAIN.Invalidate();	
	OnSelchangeListMain();
}

void CEditNetListDlg::OnAutoTestHeight() 
{
	DoUpdateNetPartTestStates(&m_PartList,&(m_pDocument->m_PoseidonMap));
	m_cListMAIN.Invalidate();	
	OnSelchangeListMain();
}

/////////////////////////////////////////////////////////////////////////////
// Apply do projektu

void CEditNetListDlg::OnApplyList() 
{
	if (m_pMainObject == NULL) return;
	if (!UpdateData(TRUE)) return;

	if (m_pMainObject->IsKindOf(RUNTIME_CLASS(CPosNetObjectNor)))
	{
		CPosNetObjectNor CopyObj(m_pDocument->m_PoseidonMap);
		CopyObj.CopyFrom((CPosNetObjectNor*)m_pMainObject);
		CopyObj.m_PartList.CopyFrom(&m_PartList);
		Action_EditPoseidonNetNor(&CopyObj, m_pDocument, NULL);
	} 
	else if (m_pMainObject->IsKindOf(RUNTIME_CLASS(CPosNetObjectKey)))
	{
		CPosNetObjectKey CopyObj(m_pDocument->m_PoseidonMap);
		CopyObj.CopyFrom((CPosNetObjectKey*)m_pMainObject);
		// index seznamu
		int nIndx = -1;
		for (int i = 0;; ++i)
		{
			if (((CPosNetObjectKey*)m_pMainObject)->m_PartLists[i] == m_pMainpList)
			{
				nIndx = i;
				break;
			}
		}

		if ((nIndx >= 0) && (nIndx < CopyObj.m_PartLists.GetSize()))
		{
			CNetPartsList* pList = (CNetPartsList*)(CopyObj.m_PartLists[nIndx]);
			if (pList)
			{
				pList->CopyFrom(&m_PartList);
				Action_EditPoseidonNetKey(&CopyObj, m_pDocument, NULL, TRUE);
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// DoEditNetList

BOOL DoEditNetList(CNetPartsList* pList, CPosNetTemplate* pNet, CPosEdMainDoc* pDocument,
				   CPosNetObjectBase* pMainObject)
{
	ASSERT(pList != NULL);
	ASSERT(pNet  != NULL);
	CEditNetListDlg dlg;
	// incializace dialogu
	dlg.m_pNet        = pNet;
	dlg.m_pDocument	  = pDocument;
	dlg.m_pMainObject = pMainObject;
	dlg.m_pMainpList  = pList;
	// kopie star�ch dat
	dlg.m_PartList.CopyFrom(pList);
	DoClearNetPartTestStates(&dlg.m_PartList);
	if (dlg.DoModal() != IDOK) return FALSE;
	// kopie nov�ch dat
	pList->CopyFrom(&dlg.m_PartList);

	return TRUE;
}

BOOL DoExportImportEditNetList(CNetPartsList* pList,CPosNetTemplate* pNet,CPosEdMainDoc* pDocument,
                               CPosNetObjectBase* pMainObject, std::istream* in, std::ostream* out)
{
	ASSERT(pList != NULL);
	ASSERT(pNet  != NULL);
	CEditNetListDlg dlg;
	// incializace dialogu
	dlg.m_pNet        = pNet;
	dlg.m_pDocument	  = pDocument;
	dlg.m_pMainObject = pMainObject;
	dlg.m_pMainpList  = pList;
	// kopie star�ch dat
	dlg.m_PartList.CopyFrom(pList);
	DoClearNetPartTestStates(&dlg.m_PartList);
	dlg.Create(dlg.IDD);
	if (in) dlg.ImportTxt(*in);
	if (out) dlg.ExportTxt(*out);
	// kopie nov�ch dat
	dlg.SendMessage(WM_COMMAND, IDOK);
	dlg.DestroyWindow();
	if (in) pList->CopyFrom(&dlg.m_PartList);

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// Na��t�n� a ukl�d�n� �seku

/////////////////////////////////////////////////////////////////////////////
// CSaveNetPartType dialog

class CSaveNetPartType : public CDialog
{
// Construction
public:
	CSaveNetPartType(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSaveNetPartType)
	enum { IDD = IDD_SAVE_NET_PART };
	int		m_nType;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSaveNetPartType)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSaveNetPartType)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
CSaveNetPartType::CSaveNetPartType(CWnd* pParent /*=NULL*/)
	: CDialog(CSaveNetPartType::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSaveNetPartType)
	m_nType = 0;
	//}}AFX_DATA_INIT
}


void CSaveNetPartType::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSaveNetPartType)
	DDX_Radio(pDX, IDC_RADIO1, m_nType);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSaveNetPartType, CDialog)
	//{{AFX_MSG_MAP(CSaveNetPartType)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


#define IDS_NET_PART_FILE_EXT	_T("pnp")

#define IDS_NET_PART_TYPE_NONE	0
#define IDS_NET_PART_TYPE_STRA	1
#define IDS_NET_PART_TYPE_SPEC	2
#define IDS_NET_PART_TYPE_LBND	3
#define IDS_NET_PART_TYPE_RBND	4

BOOL CEditNetListDlg::GetNetListExpImpFile(CString& sFile, BOOL bOpen)
{
	CString sFilter,sExt,sDir,sTitle;
	VERIFY(sFilter.LoadString(IDS_POS_NET_PART_FILE));

	VERIFY(sTitle.LoadString(IDS_POS_NET_PART_FILE_TITLE));
	sDir = GetPosEdEnvironment()->m_optSystem.m_sObjWorldPath;
	MakeShortDirStr(sDir,sDir);

	// p��prava CFileDialog
	CFileDialog dlg(bOpen);
	if (bOpen)
		dlg.m_ofn.Flags |= OFN_FILEMUSTEXIST;
	else
		dlg.m_ofn.Flags |= OFN_PATHMUSTEXIST;
	sFilter += (TCHAR)(_T('\0'));
	sFilter += _T("*.");
	sFilter += IDS_NET_PART_FILE_EXT;
	sFilter += (TCHAR)(_T('\0'));
	sFilter += (TCHAR)(_T('\0'));
	dlg.m_ofn.nMaxCustFilter = 1;
	dlg.m_ofn.lpstrFilter = sFilter;
	dlg.m_ofn.lpstrDefExt = IDS_NET_PART_FILE_EXT;
	dlg.m_ofn.lpstrTitle  = sTitle;
	dlg.m_ofn.lpstrFile = sFile.GetBuffer(_MAX_PATH);
	dlg.m_ofn.lpstrInitialDir = sDir;
	BOOL bRes = (dlg.DoModal()==IDOK)?TRUE:FALSE;
	sFile.ReleaseBuffer();
  if (bRes)
  {
    SeparateDirFromFileName(sDir, (LPCTSTR) sFile);
    GetPosEdEnvironment()->m_optSystem.m_sObjWorldPath = sDir;
    GetPosEdEnvironment()->m_optSystem.SaveParams();
  }
	return bRes;
};

CNetComponent* CEditNetListDlg::GetNetPartSource(WORD nType,LPCTSTR pName)
{
	if (nType == IDS_NET_PART_TYPE_STRA)
	{
		for(int i=0; i<m_pNet->m_NetSTRA.GetSize(); i++)
		{
			CNetComponent* pObj = (CNetComponent*)(m_pNet->m_NetSTRA[i]);
			if ((pObj!=NULL)&&(lstrcmpi(pObj->m_sCompName,pName)==0))
				return pObj;
		}
	} else
	if (nType == IDS_NET_PART_TYPE_SPEC)
	{
		for(int i=0; i<m_pNet->m_NetSPEC.GetSize(); i++)
		{
			CNetComponent* pObj = (CNetComponent*)(m_pNet->m_NetSPEC[i]);
			if ((pObj!=NULL)&&(lstrcmpi(pObj->m_sCompName,pName)==0))
				return pObj;
		}
	} else
	{	// 	IDS_NET_PART_TYPE_LBND,IDS_NET_PART_TYPE_RBND
		for(int i=0; i<m_pNet->m_NetBEND.GetSize(); i++)
		{
			CNetComponent* pObj = (CNetComponent*)(m_pNet->m_NetBEND[i]);
			if ((pObj!=NULL)&&(lstrcmpi(pObj->m_sCompName,pName)==0))
				return pObj;
		}
	};
	return NULL;
};

/*void CEditNetListDlg::OnLoad() 
{
	CString sFileName;
	if (!GetNetListExpImpFile(sFileName, TRUE))
		return;
	// na�ten� d�l� �seku
	CWordArray		arTypes;
	CStringArray	arParts;

	// load object
	CFile file;
	CFileException fe;
	if (!file.Open(sFileName, CFile::modeRead | CFile::shareDenyWrite, &fe))
	{
		AfxMessageBox(IDS_FAILED_TO_READ_NET_PART_FILE,MB_OK|MB_ICONSTOP);
		return;
	}
	CArchive loadArchive(&file, CArchive::load | CArchive::bNoFlushOnDelete);
	AfxGetApp()->BeginWaitCursor();
	// uchov�m p�vodn� soubor a nastv�m aktu�ln�
	TRY
	{
		// serializace dat
		for(;;)
		{
			WORD	nType;
			CString sName;

			MntSerializeWord(loadArchive,nType);
			if (nType != IDS_NET_PART_TYPE_NONE)
			{
				MntSerializeString(loadArchive,sName);
				arTypes.Add(nType);
				arParts.Add(sName);

			} else
				break;	// konec souboru
		};
		loadArchive.Close();
		file.Close();
	}
	CATCH_ALL(e)
	{
		file.Abort(); 
		AfxGetApp()->EndWaitCursor();
		AfxMessageBox(IDS_FAILED_TO_READ_NET_PART_FILE,MB_OK|MB_ICONSTOP);
		return;
	}
	END_CATCH_ALL
	AfxGetApp()->EndWaitCursor();
	// verifikuji existenci v�ech d�l�
	ASSERT(arTypes.GetSize() == arParts.GetSize());
	for(int i=0; i<arTypes.GetSize(); i++)
	{
		WORD	nType = arTypes[i];
		CString	sName = arParts[i];
		CNetComponent* pComp = GetNetPartSource(nType,sName);
		if (pComp == NULL)
		{
			CString sText;
			AfxFormatString1(sText,IDS_NO_EXIST_SAVE_NET_COMPONENT,sName);
			AfxMessageBox(sText,MB_OK|MB_ICONEXCLAMATION);
			return;
		}
	};
	// vytvo��m a vlo��m v�echny d�ly
	for(int i=0; i<arTypes.GetSize(); i++)
	{
		WORD	nType = arTypes[i];
		CString	sName = arParts[i];
		CNetComponent* pComp = GetNetPartSource(nType,sName);
		if (pComp != NULL)
		{
			if (nType == IDS_NET_PART_TYPE_STRA)
			{	// vlo�en� rovinky
				CNetSTRA* pSTRA = (CNetSTRA*)pComp;
				ASSERT_KINDOF(CNetSTRA,pSTRA);
				// vytvo�en� podle pSTRA
				CNetPartSTRA* pNew = new CNetPartSTRA();
				if (pNew == NULL) return;
				pNew->CreateFromTemplateSTRA(pSTRA,&m_PartList);
				// vlo�en� komponenty
				InsertNewComponent(pNew);
			} else
			if (nType == IDS_NET_PART_TYPE_SPEC)
			{	// vlo�en� spec d�lu
				CNetSPEC* pSPEC = (CNetSPEC*)pComp;
				ASSERT_KINDOF(CNetSPEC,pSPEC);
				// vytvo�en� podle pSPEC
				CNetPartSPEC* pNew = new CNetPartSPEC();
				if (pNew == NULL) return;
				pNew->CreateFromTemplateSPEC(pSPEC,&m_PartList);
				// vlo�en� komponenty
				InsertNewComponent(pNew);
			} else
			{	// 	IDS_NET_PART_TYPE_LBND,IDS_NET_PART_TYPE_RBND
				// vlo�en� zat��ky
				BOOL bLeft = (nType == IDS_NET_PART_TYPE_LBND);

				CNetBEND* pBEND = (CNetBEND*)pComp;
				ASSERT_KINDOF(CNetBEND,pBEND);
				// vytvo�en� podle pBEND
				CNetPartBEND* pNew = new CNetPartBEND();
				if (pNew == NULL) return;
				pNew->CreateFromTemplateBEND(pBEND,bLeft,&m_PartList);
				// vlo�en� komponenty
				InsertNewComponent(pNew);
			};
		}
	};
}

void CEditNetListDlg::OnSave() 
{
	CSaveNetPartType dlg;
	if (dlg.DoModal() != IDOK)
		return;
	// napln�m d�ly, kter� budu ukl�dat
	CWordArray		arTypes;
	CStringArray	arParts;

	// vlo�en� objekt�
	int nCurSel = m_cListMAIN.GetCurSel();
	for(int i=0; i<m_PartList.m_NetParts.GetSize(); i++)
	{
		CNetPartSIMP* pPart = (CNetPartSIMP*)(m_PartList.m_NetParts[i]);
		if (pPart!=NULL)
		{
			if ((dlg.m_nType == 0)||				// v�echny d�ly
				((dlg.m_nType == 1)&&(i<nCurSel))||	// po��te�n� d�ly
				((dlg.m_nType == 2)&&(i>nCurSel)))	// koncov� d�ly
			{
				WORD nType = IDS_NET_PART_TYPE_NONE;
				if (pPart->GetNetPartType() == CNetPartBase::netPartSTRA)
					nType = IDS_NET_PART_TYPE_STRA;
				else
				if (pPart->GetNetPartType() == CNetPartBase::netPartSPEC)
					nType = IDS_NET_PART_TYPE_SPEC;
				else
				if (pPart->GetNetPartType() == CNetPartBase::netPartBEND)
				{
					if (((CNetPartBEND*)pPart)->m_bLeftBend)
						nType = IDS_NET_PART_TYPE_LBND;
					else
						nType = IDS_NET_PART_TYPE_RBND;
				};
				// vlo��m
				if (nType != IDS_NET_PART_TYPE_NONE)
				{
					arTypes.Add(nType);
					arParts.Add(pPart->m_nComponentName);
				}

			}
		};
	};
	if (arTypes.GetSize() == 0)
	{
		AfxMessageBox(IDS_NO_NET_PART_TO_SAVE,MB_OK|MB_ICONEXCLAMATION);
		return;
	}

	// soubor kam budu ukl�dat
	CString sFileName;
	if (!GetNetListExpImpFile(sFileName, FALSE))
		return;

	// load object
	CFile file;
	CFileException fe;
	if (!file.Open(sFileName, CFile::modeCreate |
	  CFile::modeReadWrite | CFile::shareExclusive, &fe))
	{
		AfxMessageBox(IDS_FAILED_TO_SAVE_NET_PART_FILE,MB_OK|MB_ICONEXCLAMATION);
		return;
	}
	CArchive saveArchive(&file, CArchive::store | CArchive::bNoFlushOnDelete);
	AfxGetApp()->BeginWaitCursor();
	// uchov�m p�vodn� soubor a nastv�m aktu�ln�
	TRY
	{
		// serializace dat
		ASSERT(arTypes.GetSize() == arParts.GetSize());
		for(int i=0; i<arTypes.GetSize(); i++)
		{
			WORD	nType = arTypes[i];
			CString	sName = arParts[i];
			MntSerializeWord(saveArchive,nType);
			MntSerializeString(saveArchive,sName);
		};
		WORD	nType = IDS_NET_PART_TYPE_NONE;
		MntSerializeWord(saveArchive,nType);

		saveArchive.Close();
		file.Close();
	}
	CATCH_ALL(e)
	{
		file.Abort(); 
		AfxGetApp()->EndWaitCursor();
		AfxMessageBox(IDS_FAILED_TO_SAVE_NET_PART_FILE,MB_OK|MB_ICONEXCLAMATION);
		return;
	}
	END_CATCH_ALL
	AfxGetApp()->EndWaitCursor();
	return;
}
*/

void CEditNetListDlg::ImportTxt(std::istream &in)
{
  int i=in.get();
  while (i!=EOF)
  {
    ws(in);
    if (i=='-') break;
    char buff[512];
    in.getline(buff,sizeof(buff));
    CListBox *chlivek=0;
    switch (i)
    {
    case '|': chlivek=&m_cListSTRA;break;
    case ')': chlivek=&m_cListBENDL;break;
    case '(': chlivek=&m_cListBENDR;break;
    case '+': chlivek=&m_cListSPEC;break;
    }
    if (chlivek)
    {      
      int pos=chlivek->FindStringExact(-1,buff);
      if (pos!=-1)
      {
        chlivek->SetCurSel(pos);
        if (chlivek==&m_cListSTRA) OnInsertStra();
        else if (chlivek==&m_cListSPEC) OnInsertSpec();
        else if (chlivek==&m_cListBENDL) OnInsertBendL();
        else if (chlivek==&m_cListBENDR) OnInsertBendR();
      }
      else
      {
        CString s;
        AfxFormatString1(s,IDS_IMPORTTXT_UNKNOWN_COMPONENT,buff);
        if (AfxMessageBox(s,MB_OKCANCEL)==IDCANCEL) return;
      }
    }
    else
    {
      CString s;
      buff[0]=i;
      buff[1]=0;
      AfxFormatString1(s,IDS_IMPORTTXT_UNKNOWN_COMPONENTTYPE,buff);
      if (AfxMessageBox(s,MB_OKCANCEL)==IDCANCEL) return;
    }
    i=in.get();
  }
}

void CEditNetListDlg::ExportTxt(std::ostream &out)
{
  for (int i=0,cnt=m_cListMAIN.GetCount();i<cnt;i++)
  {
    CNetPartSIMP* pPart = (CNetPartSIMP*)m_cListMAIN.GetItemData(i);
    if (pPart !=NULL)
    {      
      if (pPart->GetNetPartType() == CNetPartBase::netPartSTRA)
        out<<"|";
      else
        if (pPart->GetNetPartType() == CNetPartBase::netPartSPEC)
          out<<"+";
        else
          if (pPart->GetNetPartType() == CNetPartBase::netPartBEND)
          {
            if ( ((CNetPartBEND*)pPart)->m_bLeftBend )
              out<<")";
            else
              out<<"(";

          }
          out<<" "<<pPart->m_nComponentName<<"\n";
    }
  }
}
void CEditNetListDlg::OnBnClickedExporttxt()
{
  using namespace std;
  CFileDialog fdlg(FALSE);
  if (fdlg.DoModal()==IDOK)
  {
    CString fname=fdlg.GetPathName();
    ofstream out(fname,ios::out|ios::trunc);
    if (!out)
    {
      AfxMessageBox("Error creating the file");
      return;
    }
    ExportTxt(out);
  }
}

void CEditNetListDlg::OnBnClickedImporttxt()
{
  using namespace std;
  CFileDialog fdlg(TRUE,0,0,OFN_HIDEREADONLY|OFN_FILEMUSTEXIST);
  if (fdlg.DoModal()==IDOK)
  {
    CString fname=fdlg.GetPathName();
    ifstream in(fname,ios::in);
    if (!in)
    {
      AfxMessageBox("Error opening the file");
      return;
    }
    ImportTxt(in);
  }
}
