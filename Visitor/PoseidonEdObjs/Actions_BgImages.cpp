#include "stdafx.h"
#include "resource.h"
#include "float.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//////////////////////////////////////////////////////////////////////
// CCreateBgImageDlg dialog

class CCreateBgImageDlg : public CDialog
{
  DECLARE_DYNAMIC(CCreateBgImageDlg)
public:
  CPosBackgroundImageObject m_imgObj;
  //float m_MaxSizeX;
  //float m_MaxSizeY;
  const CPosEdMainDoc& m_Doc;
  BOOL m_Create; // if true create new, otherwise edit old  

public:
  CCreateBgImageDlg( const CPosEdMainDoc& doc, BOOL create, CWnd* pParent = NULL);   // standard constructor
  virtual ~CCreateBgImageDlg();

  // Dialog Data
  enum { IDD = IDD_EDIT_BG_IMAGE };

  //Message handlers
  afx_msg void OnBGIBrowse();

protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

  DECLARE_MESSAGE_MAP()
public:
  virtual BOOL OnInitDialog();
};

// CCreateBgImageDlg dialog

IMPLEMENT_DYNAMIC(CCreateBgImageDlg, CDialog)
CCreateBgImageDlg::CCreateBgImageDlg( const CPosEdMainDoc& doc, BOOL create, CWnd* pParent /*=NULL*/)
	: m_Doc(doc), m_Create(create), m_imgObj(doc.m_PoseidonMap), CDialog(CCreateBgImageDlg::IDD, pParent)
{
  
}

CCreateBgImageDlg::~CCreateBgImageDlg()
{
}

void CCreateBgImageDlg::DoDataExchange(CDataExchange* pDX)
{
  DDX_Text(pDX, IDC_BGI_NAME, m_imgObj.m_sAreaName);
  MDDV_MinChars(pDX, m_imgObj.m_sAreaName, 1, IDC_BGI_NAME);
  if (m_Create && NULL != m_Doc.m_PoseidonMap.GetBgImage(m_imgObj.m_sAreaName))
  {
    AfxMessageBox(IDS_NAME_EXIST, MB_OK);
#ifdef Fail
#undef Fail
#endif
    pDX->Fail();    
  }

  DDX_Text(pDX, IDC_BGI_PATH, m_imgObj.m_sFileName);
  MDDV_FileExist(pDX, m_imgObj.m_sFileName);

  DDX_Text(pDX, IDC_BGI_X, m_imgObj.m_RealRect.left);
  //MDDV_MinMaxFloat( pDX, m_imgObj.m_RealRect.left, 0, m_MaxSizeX, IDC_BGI_X, 100);

  DDX_Text(pDX, IDC_BGI_Y, m_imgObj.m_RealRect.top);
  //MDDV_MinMaxFloat( pDX, m_imgObj.m_RealRect.top, 0, m_MaxSizeY, IDC_BGI_Y, 100);

  DDX_Text(pDX, IDC_BGI_WIDTH, m_imgObj.m_RealRect.right);
  //MDDV_MinMaxFloat( pDX, m_imgObj.m_RealRect.right, 0, m_MaxSizeX - m_imgObj.m_RealRect.left, IDC_BGI_WIDTH, 100);
  MDDV_MinMaxFloat( pDX, m_imgObj.m_RealRect.right, 0, FLT_MAX, IDC_BGI_WIDTH, 100);

  DDX_Text(pDX, IDC_BGI_HEIGHT, m_imgObj.m_RealRect.bottom);
  //MDDV_MinMaxFloat( pDX, m_imgObj.m_RealRect.bottom, 0, m_MaxSizeY - m_imgObj.m_RealRect.top, IDC_BGI_HEIGHT, 100);
  MDDV_MinMaxFloat( pDX, m_imgObj.m_RealRect.bottom, 0, FLT_MAX, IDC_BGI_HEIGHT, 100);

  DDX_Text(pDX, IDC_BGI_TRANSPARENCY, m_imgObj.m_Transparency);
  MDDV_MinMaxInt( pDX, m_imgObj.m_Transparency, 0, 100, IDC_BGI_TRANSPARENCY, 10);

  DDX_Check(pDX, IDC_BGI_VISIBLE, m_imgObj.m_Visible);

	CDialog::DoDataExchange(pDX);
}

void CCreateBgImageDlg::OnBGIBrowse()
{
  
  // Open file 
  CString sFilter,sExt,sDir,sFile;

  sDir = GetPosEdEnvironment()->m_optSystem.m_sObjWorldPath;
  MakeShortDirStr(sDir,sDir);
  // default file  

  // příprava CFileDialog
  CFileDialog dlg(true);
  
  dlg.m_ofn.Flags |= OFN_FILEMUSTEXIST;
  
  sFilter = _T("Bmp Files(*.bmp)");
  sFilter += (TCHAR)(_T('\0'));
  sFilter += _T("*.");
  sFilter += _T("bmp");
  sFilter += (TCHAR)(_T('\0'));
  sFilter += (TCHAR)(_T('\0'));

  dlg.m_ofn.nMaxCustFilter = 1;
  dlg.m_ofn.lpstrFilter = sFilter;
  dlg.m_ofn.lpstrDefExt = _T("bmp");
  //dlg.m_ofn.lpstrTitle  = sTitle;
  dlg.m_ofn.lpstrFile = sFile.GetBuffer(_MAX_PATH);
  dlg.m_ofn.lpstrInitialDir = sDir;
  BOOL bRes = (dlg.DoModal()==IDOK)?TRUE:FALSE;
  sFile.ReleaseBuffer();
  sFile.MakeLower();
  if (bRes)
  {
    SeparateDirFromFileName(sDir, (LPCTSTR) sFile);
    GetPosEdEnvironment()->m_optSystem.m_sObjWorldPath = sDir;
    GetPosEdEnvironment()->m_optSystem.SaveParams();

    m_imgObj.m_sFileName = sFile;
    CWnd * pWnd = GetDlgItem(IDC_BGI_PATH);
    pWnd->SetWindowText(sFile);
  }
}

BOOL CCreateBgImageDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  if (!m_Create)
  {
    // disable window with name
    CWnd * pWnd = GetDlgItem(IDC_BGI_NAME);
    pWnd->EnableWindow(FALSE);

  }

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

BEGIN_MESSAGE_MAP(CCreateBgImageDlg, CDialog)
  ON_COMMAND(IDC_BGI_BROWSE, OnBGIBrowse)
END_MESSAGE_MAP()


// CCreateBgImageDlg message handlers
//////////////////////////////////////////////////////////////////////
//

BOOL DoCreateEditBgImage(CPosBackgroundImageObject * pObj, CPosEdMainDoc * pDoc, BOOL create, FltRect * pRealRect = NULL)
{
  CCreateBgImageDlg dlg(*pDoc, create);
  //pObj->m_RealRect.right = pObj->m_RealRect.Width();
  //pObj->m_RealRect.bottom = pObj->m_RealRect.Height();
  CString projectFile, sDir;
  projectFile = pDoc->GetPathName();
  if (projectFile.IsEmpty())
  {
    AfxMessageBox(IDS_ERROR_SAVE_PROJECT, MB_OK);
    return FALSE;
  }
  projectFile.MakeLower();

  SeparateDirFromFileName(sDir, projectFile);
  MakeLongDirStr(sDir, sDir);

  if (!create)
  {
    dlg.m_imgObj.CopyFrom(pObj);
    dlg.m_imgObj.m_sFileName = sDir + dlg.m_imgObj.m_sFileName;

    dlg.m_imgObj.m_RealRect.right -= dlg.m_imgObj.m_RealRect.left;
    dlg.m_imgObj.m_RealRect.bottom -= dlg.m_imgObj.m_RealRect.top;
  }

  //dlg.m_MaxSizeX = pDoc->m_PoseidonMap.GetRealSize()[0];
  //dlg.m_MaxSizeY = pDoc->m_PoseidonMap.GetRealSize()[1];

  if (pRealRect)
  {
    dlg.m_imgObj.m_RealRect = *pRealRect;
    dlg.m_imgObj.m_RealRect.right -= pRealRect->left;
    dlg.m_imgObj.m_RealRect.bottom -= pRealRect->top;
  }

  if (dlg.DoModal() != IDOK) 
    return FALSE;

  // Copy bitmap into project directory 

  CString sFileName;
  SeparateNameFromFileName(sFileName,  dlg.m_imgObj.m_sFileName);

  if (_tcsncmp(sDir, dlg.m_imgObj.m_sFileName, sDir.GetLength()) != 0)
  {
    
    
    CString sNewFile = sDir + sFileName;

    //CString sQuestion;
    //sQuestion.Format(IDS_COPY_TEXTURE, (LPCTSTR) sDir, (LPCTSTR) sNewFile);

    //if (AfxMessageBox(sQuestion,MB_YESNO|MB_ICONQUESTION)!=IDYES)
    // return FALSE;

    // check if file exist
    if (MntExistFile(sNewFile))
    {
      CString sQuestion;
      sQuestion.Format(IDS_OVERWRITE, (LPCTSTR) sNewFile);

      int ret = AfxMessageBox(sQuestion,MB_YESNOCANCEL|MB_ICONQUESTION); 
      if (ret == IDCANCEL)
        return FALSE;

      if (ret == IDYES)
        CopyFile(dlg.m_imgObj.m_sFileName, sNewFile, FALSE);
    }
    else
      CopyFile(dlg.m_imgObj.m_sFileName, sNewFile, FALSE);    
  }

  dlg.m_imgObj.m_sFileName = sFileName;  
  if (dlg.m_imgObj.m_RealRect.IsRectNull())
  {
    // empty rect means the whole map
    dlg.m_imgObj.m_RealRect.right = pDoc->m_PoseidonMap.GetRealSize()[0];
    dlg.m_imgObj.m_RealRect.bottom = pDoc->m_PoseidonMap.GetRealSize()[1];
  }
  else
  {
    // in dlg right is Width and bottom Height change it
    dlg.m_imgObj.m_RealRect.right += dlg.m_imgObj.m_RealRect.left;
    dlg.m_imgObj.m_RealRect.bottom += dlg.m_imgObj.m_RealRect.top;
  }

  pObj->CopyFrom(&dlg.m_imgObj);

  return TRUE;
}

//////////////////////////////////////////////////////////////////////
// Create background image
CPosAction* Action_CreateBGImage(CPosEdMainDoc* pDocument, FltRect * pRealRect, CPosActionGroup* pToGroup )
{
  CPosBackgroundImageObject* pBGImage  = NULL;
  CPosAction*			pAction = NULL;

  TRY
  {
    // vytvoření 
    pBGImage = new CPosBackgroundImageObject(pDocument->m_PoseidonMap);
    
    if (!DoCreateEditBgImage(pBGImage, pDocument, TRUE, pRealRect))
    {
      delete pBGImage;
      return NULL;
    };

    // nové ID
    //pKeyPt->m_nKeyPtID = pDocument->m_PoseidonMap.GetFreeKeyPtID();
    // poloha
    //pKeyPt->m_ptRealPos= pDocument->m_PoseidonMap.FromViewToRealPos(ptLog);
    pBGImage->CalcObjectLogPosition();
    // generování akce
    pAction = pDocument->GenerNewObjectAction(IDA_BG_IMAGE_CREATE,NULL,pBGImage,FALSE,pToGroup);
    // uvolnění plochy
  }
  CATCH_ALL(e)
  {
    MntReportMemoryException();
    if (pBGImage)
    {
      delete pBGImage;
      //pKeyPt = NULL;
    } 
  }
  END_CATCH_ALL
    return pAction;
};

//////////////////////////////////////////////////////////////////////
// Remove background image
CPosAction* Action_RemoveBGImage(CPosBackgroundImageObject* pBGImage, CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  if (pBGImage != NULL)
  {    
    return pDocument->GenerNewObjectAction(IDA_BG_IMAGE_DELETE,pBGImage,NULL,FALSE,pToGroup);
  }
  return NULL;
};


//////////////////////////////////////////////////////////////////////
// Edit background image
CPosAction* Action_EditBGImage(CPosBackgroundImageObject* pBGImage, CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  if (pBGImage == NULL)
    return NULL; 

  CPosAction*			pAction = NULL;
  TRY
  {
    // vytvoření 
    CPosBackgroundImageObject* pObj;
    if ((pObj = (CPosBackgroundImageObject*) pBGImage->CreateCopyObject())==NULL) AfxThrowMemoryException();

    if (!DoCreateEditBgImage(pObj, pDocument, FALSE))
    {
      delete pObj;
      return NULL;
    };

    CPosBackgroundImageObject* pDocObj = (CPosBackgroundImageObject*) pDocument->GetDocumentObjectFromValue(pBGImage);
    CPosBackgroundImageObject* pDocObj2 = (CPosBackgroundImageObject*) pDocObj->CreateCopyObject();
   
    pBGImage->CalcObjectLogPosition();
    // generování akce
    pAction = pDocument->GenerNewObjectAction(IDA_BG_IMAGE_EDIT,pDocObj2,pObj,FALSE,pToGroup);
    // uvolnění plochy
  }
  CATCH_ALL(e)
  {
    MntReportMemoryException();    
  }
  END_CATCH_ALL
    return pAction;
};

//////////////////////////////////////////////////////////////////////
// Edit background image
CPosAction* Action_EditBGImage(CPosBackgroundImageObject* pBGImage, const FltRect& newRect, CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  if (pBGImage == NULL)
    return NULL; 

  CPosAction*			pAction = NULL;
  TRY
  {
    // vytvoření 
    CPosBackgroundImageObject* pObj;
    if ((pObj = (CPosBackgroundImageObject*) pBGImage->CreateCopyObject())==NULL) AfxThrowMemoryException();

    pObj->m_RealRect = newRect; 
    pObj->CalcObjectLogPosition();

    CPosBackgroundImageObject* pDocObj = (CPosBackgroundImageObject*) pDocument->GetDocumentObjectFromValue(pBGImage);
    CPosBackgroundImageObject* pDocObj2 = (CPosBackgroundImageObject*) pDocObj->CreateCopyObject();

    pBGImage->CalcObjectLogPosition();
    // generování akce
    pAction = pDocument->GenerNewObjectAction(IDA_BG_IMAGE_EDIT,pDocObj2,pObj,FALSE,pToGroup);
    // uvolnění plochy
  }
  CATCH_ALL(e)
  {
    MntReportMemoryException();    
  }
  END_CATCH_ALL
    return pAction;
};

//////////////////////////////////////////////////////////////////////
// Paste background image
CPosAction* Action_PasteClipboardBackgroundImage(PosClipBoardBackgroundImage& data,CPosEdMainDoc& doc,
                                                 const CPoint& cursorLog, CPosActionGroup * pToGroup)
{
  CPosBackgroundImageObject* pBGImage  = NULL;
  CPosAction*			pAction = NULL;

  TRY
  {
    // vytvoření 
    pBGImage = new CPosBackgroundImageObject(doc.m_PoseidonMap);    

    pBGImage->m_Transparency = data.transparency;

    LPSTR pAreaName = pBGImage->m_sAreaName.GetBuffer(data.nameLength);
    memcpy(pAreaName, data.data, data.nameLength);
    pBGImage->m_sAreaName.ReleaseBuffer(data.nameLength);

    LPSTR pFileName = pBGImage->m_sFileName.GetBuffer(data.fileNameLength);
    memcpy(pFileName, data.data + data.nameLength, data.fileNameLength);
    pBGImage->m_sFileName.ReleaseBuffer(data.fileNameLength);

    REALPOS realDiff = doc.m_PoseidonMap.FromViewToRealPos(cursorLog);
    realDiff.x -= data.rectreal[0];
    realDiff.z -= data.rectreal[3];

    pBGImage->m_RealRect[0] = data.rectreal[0] + realDiff.x;
    pBGImage->m_RealRect[1] = data.rectreal[1] + realDiff.z;
    pBGImage->m_RealRect[2] = data.rectreal[2] + realDiff.x;
    pBGImage->m_RealRect[3] = data.rectreal[3] + realDiff.z;

    pBGImage->CalcObjectLogPosition();

    // Probably object with the same name aleready exist create new
    CString newName = pBGImage->m_sAreaName;
    int i = 1;
    while (NULL != doc.m_PoseidonMap.GetBgImage(newName))    
      newName.Format("%s_%d",pBGImage->m_sAreaName,i++); 

    pBGImage->m_sAreaName = newName;

    // generování akce
    pAction = doc.GenerNewObjectAction(IDA_BG_IMAGE_CREATE,NULL,pBGImage,FALSE,pToGroup);
    // uvolnění plochy
  }
  CATCH_ALL(e)
  {
    MntReportMemoryException();
    if (pBGImage)    
      delete pBGImage;          
  }
  END_CATCH_ALL
    return pAction;
}

CPosAction* Action_PasteAbsolutClipboardBackgroundImage(PosClipBoardBackgroundImage& data,CPosEdMainDoc& doc,
                                                 CPosActionGroup * pToGroup)
{
  CPosBackgroundImageObject* pBGImage  = NULL;
  CPosAction*			pAction = NULL;

  TRY
  {
    // vytvoření 
    pBGImage = new CPosBackgroundImageObject(doc.m_PoseidonMap);    

    pBGImage->m_Transparency = data.transparency;

    LPSTR pAreaName = pBGImage->m_sAreaName.GetBuffer(data.nameLength);
    memcpy(pAreaName, data.data, data.nameLength);
    pBGImage->m_sAreaName.ReleaseBuffer(data.nameLength);

    LPSTR pFileName = pBGImage->m_sFileName.GetBuffer(data.fileNameLength);
    memcpy(pFileName, data.data + data.nameLength, data.fileNameLength);
    pBGImage->m_sFileName.ReleaseBuffer(data.fileNameLength);    

    pBGImage->m_RealRect[0] = data.rectreal[0];
    pBGImage->m_RealRect[1] = data.rectreal[1];
    pBGImage->m_RealRect[2] = data.rectreal[2];
    pBGImage->m_RealRect[3] = data.rectreal[3];

    pBGImage->CalcObjectLogPosition();

    // Probably object with the same name aleready exist create new
    CString newName = pBGImage->m_sAreaName;
    int i = 1;
    while (NULL != doc.m_PoseidonMap.GetBgImage(newName))    
      newName.Format("%s_%d",pBGImage->m_sAreaName,i++); 

    pBGImage->m_sAreaName = newName;

    // generování akce
    pAction = doc.GenerNewObjectAction(IDA_BG_IMAGE_CREATE,NULL,pBGImage,FALSE,pToGroup);
    // uvolnění plochy
  }
  CATCH_ALL(e)
  {
    MntReportMemoryException();
    if (pBGImage)    
      delete pBGImage;          
  }
  END_CATCH_ALL
    return pAction;
}


