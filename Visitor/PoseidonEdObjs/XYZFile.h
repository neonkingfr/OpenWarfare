
#pragma once

struct SDimension 
{
  float MinX, MaxX, MinY, MaxY, MinZ, MaxZ;
  float StepX, StepY;
  int SizeX, SizeY;
};

struct SMapSize
{
  int size[2];
  float step;
  float offset[2];
};

class XYZFile 
{
private:    
  SDimension _dimension;
  CString _sourceFileName;
  
public:
  //! Default constructor.
  XYZFile() {};
  //! Destructor.
  ~XYZFile() {};
  
  void SetSourceFile(const char *fileName) {_sourceFileName = fileName;};
  BOOL LoadDimension(SDimension& dim);
  BOOL Load(CPoseidonMap& map, const SMapSize& mapSize, bool keepObjects=false);

  const SDimension& GetDimension() const {return _dimension;};
  
  //void Export(const char *FileName, int OffsetX, int OffsetY, int SizeX, int SizeY);
  //BOOL GetRGBMap(unsigned int *&RGBData, int &SizeX, int &SizeY);
  //void Draw(HDC hdc);
};
