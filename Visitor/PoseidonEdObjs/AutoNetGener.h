/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#if !defined(_AUTONETGENER_MAIN_H_)
#define _AUTONETGENER_MAIN_H_

/////////////////////////////////////////////////////////////////////////////
//  CShowNetKeyCtrl - zobrazen� kl��ov�ho bodu s�t�

class /*AFX_EXT_CLASS*/ CShowNetKeyCtrl : public CButton
{
// Construction
public:
		CShowNetKeyCtrl();
		~CShowNetKeyCtrl();

// Attributes
public:
	CBitmap			m_bmpTypes;			// bitmapa pro atributy
	
	int				m_nKeyPartType;		// zobrazovan� �sek
	WORD			m_nKeyPartSubtype;
	float				m_nKeyPartGrad;
	
// Operations
public:
	WNDPROC* GetSuperWndProcAddr();

// Overrides
			 void SetNetKeyType(int KeyPartType, WORD KeyPartSubtype, float nGrad);
	virtual  void DrawItem(LPDRAWITEMSTRUCT);
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShowNetKeyCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	//{{AFX_MSG(CShowNetKeyCtrl)
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
bool IsSameNetPosition(const REALNETPOS& p1,const REALNETPOS& p2,const AutoNetGenParams& Params,BOOL bFinal);

struct REALNETPOSKEY : public REALNETPOS
{  
	unsigned int CalculateHashValue() const 
	{
		// new code
		//***************************************
		return (int)nGra + (int) nPos.x * (int) nPos.z;
		//***************************************

		// old code
		//***************************************
		//return nGra + (int) nPos.x * (int) nPos.z;
		//***************************************
	}
	int Cmp(const REALNETPOSKEY& pos) const;
};

// uzel pro nastaven� v��ky
class NetField /*: public CObject*/
{
public:
  typedef REALNETPOSKEY KeyType;

  //int				m_nPartNum;	// index �seku (po�ad� v seznamu od za��tku)
  CNetPartSIMP*	m_pNetPart;	// komponenta �seku od za��tku
  REALNETPOS		m_nEndPos;		// koncov� poloha d�lu (v �seku od za��tku)		
  double			 m_nSlope;		// �hel  vlo�en�ho d�lu
  AutoNetGenParams * m_pParams;

  NetField() : m_pNetPart(NULL) {};
  NetField(const NetField& field);
  ~NetField() {delete m_pNetPart;};  

  const NetField& operator=(const NetField& field);
  bool operator == (const NetField &with) const 
  {return IsSameNetPosition(m_nEndPos, with.m_nEndPos,*m_pParams,false);};
  REALNETPOSKEY GetKey() const { REALNETPOSKEY out; *(REALNETPOS *)(&out) = m_nEndPos; return out;};
  float Distance(const NetField &x) const
  {
    return sqrt((m_nEndPos.nPos.x-x.m_nEndPos.nPos.x)*(m_nEndPos.nPos.x-x.m_nEndPos.nPos.x)+
                  (m_nEndPos.nPos.z-x.m_nEndPos.nPos.z)*(m_nEndPos.nPos.z-x.m_nEndPos.nPos.z));
  }
};


/////////////////////////////////////////////////////////////////////////////
// A* + its storage data structure.
#include <float.h>
#include <Es/Algorithms/aStar.hpp>



#define DEBUG_BEST_NODE 0
#if  DEBUG_BEST_NODE
void DebugBest(AStarNode<NetField> * node);
#endif

class ASInterruptFunction
{
protected:
  int _iter;
  int _maxIters;

public:
  ASInterruptFunction(int maxIters) {_maxIters = maxIters;}
  void Start() {_iter = 0;}
  bool operator () () {return _iter++ >= _maxIters;}
  void Iteration() {}
};


template<class NodePtr>
struct ASOpenListTraits
{
  static bool IsLess(const NodePtr a, const NodePtr b) {return a->_f < b->_f;}
  static bool IsLessOrEqual(const NodePtr a, const NodePtr b){return a->_f <= b->_f;}
};

template<class NodePtr>
class ASOpenList : public HeapArray<NodePtr, MemAllocD, ASOpenListTraits<NodePtr> >
{
  typedef HeapArray<NodePtr, MemAllocD, ASOpenListTraits<NodePtr> > base;
public:
  void UpdateUp(NodePtr node) {base::HeapUpdateUp(node);}
  void Add(NodePtr node) {base::HeapInsert(node);}
  bool RemoveFirst(NodePtr &node) {
    if (base::HeapRemoveFirst(node))
    {
#if  DEBUG_BEST_NODE
      DebugFirst(node);
#endif
      return true;}
    else return false;}
  const NodePtr GetFirst() const {return Size() > 0 ? Get(0) : NULL;}
};

template<class Node, class Field>
class ASNodeRef : public SRef<Node>
{
  typedef SRef<Node> base;

public:
  ASNodeRef() {};
  ASNodeRef(Node *node) : base(node) {};
  typename Field::KeyType GetKey() const {return (*this)->_field.GetKey();};
};

template<class Node, class Field>
struct ASClosedListTraits: public DefMapClassTraits<Node>
{
  //! key type
  typedef typename Field::KeyType KeyType;
  //! calculate hash value
  static unsigned int CalculateHashValue(KeyType key)
  {
    return key.CalculateHashValue();
  }

  //! compare keys, return negative when k1<k2, positive when k1>k2, zero when equal
  static int CmpKey(KeyType k1, KeyType k2)
  {
    return k1.Cmp(k2);
  }

  static KeyType GetKey(const ASNodeRef<Node, Field> &item) {return item.GetKey();}
};

template<class Node, class Field>
class ASClosedList : public MapStringToClass<ASNodeRef<Node, Field>, AutoArray<ASNodeRef<Node, Field> >, ASClosedListTraits<Node, Field> >
{
	typedef MapStringToClass<ASNodeRef<Node, Field>, AutoArray<ASNodeRef<Node, Field> >, ASClosedListTraits<Node, Field> > base;

public:
	int Add(Node* node) 
	{
		return base::Add(node);
	}

	// added to let Visitor compile after changes in AStar.hpp
	Node* AddNew(const Field& field, Node* parent, float g, float f)
	{
		Node* node = new Node(field, parent, g, f);
		base::Add(node);
		return node;
	}
};

template <class Field, class CostF, class HeuristicF>
struct AStarExCostFunctions: public AStarCostFunctionsDef<Field>
{
  typedef CostF CostFunction;
  typedef HeuristicF HeuristicFunction;
};

#define ASTAR_EX_TEMPLATE \
  template \
  < \
class Field, \
class CostFunction, class HeuristicFunction, class ReadyFunction, \
class Iterator, \
class Params \
  >

ASTAR_EX_TEMPLATE 
class AStarEx : public AStar
  <
  Field,
  AStarExCostFunctions<Field,CostFunction,HeuristicFunction>, ReadyFunction, ASInterruptFunction, AStarEndDefault<Field>,
  Iterator,
  ASClosedList<AStarNode<Field>,Field>, ASOpenList<AStarNode<Field> *>,
  Params
  >
{
  typedef AStar
    <
    Field,
    AStarExCostFunctions<Field,CostFunction,HeuristicFunction>, ReadyFunction, ASInterruptFunction, AStarEndDefault<Field>,
    Iterator,
    ASClosedList<AStarNode<Field>,Field>, ASOpenList<AStarNode<Field> *>,
    Params
    > base;

public:
  AStarEx
    (
    const Field &start, const Field &destination,
    const CostFunction &cost, const HeuristicFunction &heuristic, const ReadyFunction &ready, 
    const ASInterruptFunction &interrupt
    ) : base(
      start, destination,
      AStarCostFunctionsWrapped< AStarExCostFunctions<Field,CostFunction,HeuristicFunction> >(cost,heuristic),
      ready, interrupt, AStarEndDefault<Field>(destination)
    ) {};
};


typedef ASNodeRef<AStarNode<NetField>, NetField> NetNodeRef;
TypeIsSimple(AStarNode<NetField>*)
DEFINE_FAST_ALLOCATOR(AStarNode<NetField>)
TypeIsMovableZeroed(NetNodeRef)

#endif // !defined(_AUTONETGENER_MAIN_H_)

