/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#if !defined(_AUTONETHEIGHT_MAIN_H_)
#define _AUTONETHEIGHT_MAIN_H_



/////////////////////////////////////////////////////////////////////////////
//  AutoNetHeight.h : header file

// uzel pro nastaven� v��ky
class CKnotNetPart : public CObject
{
public:
					CKnotNetPart(CKnotNetPart*);

// data
public:
	int				m_nPartNumber;	// index uzlu v �seku (po�ad� NetPart v seznamu)
	LANDHEIGHT		m_nHeight;		// v��ka na konci tohoto uzlu
	float			m_nRelHg;		// relativn� v��ka
	double			m_nSlope;		// �hel  vlo�en�ho d�lu

	LONG			m_nPrice;		// cena  a� do tohoto uzlu

	CKnotNetPart*	m_pParent;
};


/////////////////////////////////////////////////////////////////////////////
#endif // !defined(_AUTONETHEIGHT_MAIN_H_)

