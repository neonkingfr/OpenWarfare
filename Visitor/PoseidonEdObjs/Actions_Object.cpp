/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"
#include "../PoseidonEdDlgs/Progress.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCreateObjectMultiDlg dialog

class CCreateObjectMultiDlg : public CDialog
{
// Construction
public:
	CCreateObjectMultiDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCreateObjectMultiDlg)
	enum { IDD = IDD_INS_OBJS_MULTI };
	int		m_nCount;
	int		m_nOrient;
	//}}AFX_DATA


// Overrides
	CPosObjectTemplate* m_pTempl;

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCreateObjectMultiDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCreateObjectMultiDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CCreateObjectMultiDlg::CCreateObjectMultiDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCreateObjectMultiDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCreateObjectMultiDlg)
	m_nCount  = 2;
	m_nOrient = 0;
	//}}AFX_DATA_INIT
}


void CCreateObjectMultiDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCreateObjectMultiDlg)
	DDX_Radio(pDX, IDC_RADIO1, m_nOrient);
	//}}AFX_DATA_MAP
	MDDX_Text(pDX, IDC_EDIT_COUNT, m_nCount);
	MDDV_MinMaxInt(pDX, m_nCount, 2, 100, IDC_EDIT_COUNT);

	if (!SAVEDATA)
	{	// zobrazen� typu objektu
		if (m_pTempl != NULL)
		{
			::SetWindowText(DLGCTRL(IDC_SHOW_OBJ_TYPE),m_pTempl->GetName());
		}
	}
}


BEGIN_MESSAGE_MAP(CCreateObjectMultiDlg, CDialog)
	//{{AFX_MSG_MAP(CCreateObjectMultiDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Akce s objekty

/////////////////////////////////////////////////////////////////////////////
// vytvo�en� akce pro vlo�en� objektu

CPosAction* Action_CreatePoseidonObject(CPoint ptLog,CPosObjectTemplate* pTempl,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pTempl != NULL)
	{
		// poloha objektu
		REALPOS rPos = pDocument->m_PoseidonMap.FromViewToRealPos(ptLog);
		// vytvo��m nov� objekt
		CPosEdObject* pObj = new CPosEdObject(pDocument->m_PoseidonMap);
		pObj->CreateFromTemplateAt(pTempl,rPos);
    pObj->CalcObjectLogPosition();
    pObj->m_pTemplate = NULL; // in undo/redo should not be referncies to banks	
		pObj->SetFreeID();    

		// generov�n� akce
		CPosObjectAction * action = (CPosObjectAction * ) pDocument->GenerNewObjectAction(IDA_OBJECT_ADD,NULL,pObj,false,pToGroup, false);    
    return action;
	};
	return NULL;
};

CPosAction* Action_CreatePoseidonObjectMulti(CPoint ptLog,CPosObjectTemplate* pTempl,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pTempl == NULL)
		return NULL;

	CCreateObjectMultiDlg dlg;
	dlg.m_pTempl = pTempl;
	if (dlg.DoModal() != IDOK)
		return NULL;
	// generovani akce
	CPosObjectGroupAction* pGrpAction = new CPosObjectGroupAction(IDA_GROUPOBJ_PASTE);
	if (pGrpAction != NULL)
	{
		// vlo�en� akc� s objekty
		for(int i=0;i<dlg.m_nCount;i++)
			Action_CreatePoseidonObject(ptLog,pTempl,pDocument,pGrpAction);
		// nastav�m polohu objektu
		ASSERT(pGrpAction->m_Actions.GetSize()==dlg.m_nCount);
		float		MasterPosition[4][4];	
		float		MasterOffset = (float)(pTempl->GetWidth());
		if (dlg.m_nOrient != 0)
			MasterOffset = (float)(pTempl->GetHeight());

    FindSnapping snapping; 

		for(int i=0;i<dlg.m_nCount;i++)
		{
			CPosObjectAction* pAction = (CPosObjectAction*)(pGrpAction->m_Actions[i]);
			if ((pAction != NULL)&&(pAction->m_pOldValue != NULL))
			{
				ASSERT(pAction->m_pOldValue != NULL);
				ASSERT_KINDOF(CPosObjectAction,pAction);
				CPosEdObject* pObj = (CPosEdObject*)(pAction->m_pOldValue);

				
				if (i == 0)
				{	// nastav�m prvn� objekt
          pObj->m_Position = M4Identity;

					// poloha objektu
					REALPOS rPos = pDocument->m_PoseidonMap.FromViewToRealPos(ptLog);
					pObj->m_Position(0,3) = rPos.x;
					pObj->m_Position(2,3) = rPos.z;
					pObj->CalcObjectPoseidonHeight();
					
					if (dlg.m_nOrient != 0)
					{	// objekt vertik�lne
						pObj->CalcRotationPosition(90.);
					}
					
				} else
				{	// nastav�m dal�� objekt
					for(int ii=0;ii<3;ii++)
						for(int j=0;j<4;j++)
						{
							pObj->m_Position(ii,j) = MasterPosition[ii][j];
						};
					// offset
					pObj->m_Position(0,3) += MasterOffset;
					// vyska
					pObj->CalcObjectPoseidonHeight();          
				};

        pObj->CalcObjectLogPosition();

        CPosGroupObject group(pDocument->m_PoseidonMap);
        group.AddObjectGroup(pObj);

        CPoint offset(0,0);
        float rotatedGrad = 0;         

        snapping(offset, rotatedGrad, CPoint(pObj->GetLogObjectPosition()->GetCenter()), group, pDocument->m_PoseidonMap);

        // apply found values
        pObj->CalcRotationPosition(rotatedGrad,CPoint(pObj->GetLogObjectPosition()->GetCenter()));
        pObj->CalcOffsetLogPosition(offset.x, offset.y);

        snapping.AddSnapToObj(pObj);

        for(int ii=0;ii<3;ii++)
          for(int j=0;j<4;j++)
          {
            MasterPosition[ii][j] = pObj->m_Position(ii,j);
          };          
			}
		};
		// provedu akci
		pDocument->ProcessEditAction(pGrpAction);
	}
	return pGrpAction;
};

CPosAction* Action_CreatePoseidonObjects(CPosGroupObject*objs,CPosEdMainDoc* doc,CPosActionGroup* actGroup)
{
  if (!objs || !doc)
    return actGroup;

  CPosObjectGroupAction* pGrpAction = new CPosObjectGroupAction(IDA_GROUPOBJ_PASTE);
  if (pGrpAction != NULL)
  {
    for(int i = 0; i < objs->GetGroupSize(); i++)   
    {
      CPosObject * obj = static_cast<CPosObject *>((*objs)[i]);
      obj->SetFreeID();
      doc->GenerNewObjectAction(IDA_OBJECT_ADD,NULL,obj,FALSE,pGrpAction);
    }

    if (actGroup)
      actGroup->AddAction(pGrpAction);
    else
      doc->ProcessEditAction(pGrpAction);    
  }

  return actGroup;
}

CPosAction* Action_InsertPoseidonObject(CPosEdObject* pToInsert,CPosObjectTemplate* pTempl,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pTempl != NULL)
	{
		// poloha objektu

		REALPOS rPos; rPos.x = rPos.z = 100.f;
		// vytvo��m nov� objekt
		CPosEdObject* pObj = new CPosEdObject(pDocument->m_PoseidonMap);
		pObj->CreateFromTemplateAt(pTempl,rPos);
		pObj->CopyFrom(pToInsert);
    pObj->m_pTemplate = NULL;
		// nepouziji stare ID a jemno objektu !!!
		pObj->MakeUndef();  

		// nen� ur�eno ID objektu !
		ASSERT(pObj->GetID() == UNDEF_REG_ID);
		// generov�n� akce
		return pDocument->GenerNewObjectAction(IDA_OBJECT_ADD,NULL,pObj,FALSE,pToGroup);
	};
	return NULL;
};

/////////////////////////////////////////////////////////////////////////////
// vytvo�en� akce pro smaz�n� objektu

CPosAction* Action_DeletePoseidonObject(CPosEdObject* pToDelete,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	// vytvo��m kopii objektu
	if (pToDelete != NULL && !pToDelete->Locked())
	{	// generuji ud�lost pro p�esun atributu
		CPosObject* pDocObj = pDocument->GetDocumentObjectFromValue(pToDelete);
		if (pDocObj)
		return pDocument->GenerNewObjectAction(IDA_OBJECT_DEL,pDocObj,NULL,FALSE,pToGroup);
	}
	return NULL;
};


/////////////////////////////////////////////////////////////////////////////
// vytvo�en� akce pro p�esun objektu

CPosAction* Action_SetPosRealPoseidonObject(CPosEdObject* pNewPosObj,const REALPOS& pos, CPosActionGroup* pToGroup)
{
  if (pNewPosObj != NULL && !pNewPosObj->Locked())
  {
    CPosEdObject* pAct = (CPosEdObject*)pNewPosObj->CreateCopyObject();
    // objekt dokumentu, se kter�m hejbu
    CPosEdMainDoc * doc = (CPosEdMainDoc *) pNewPosObj->GetDocument();
    CPosEdObject* pDocObj = (CPosEdObject*)doc->GetDocumentObjectFromValue(pAct);
    ASSERT(pDocObj != NULL);
    ASSERT_KINDOF(CPosEdObject,pAct);
    // offset objektu
    pAct->SetRealPos(pos);
    // generuji ud�lost pro p�esun atributu
    return doc->GenerNewObjectAction(IDA_OBJECT_MOVE,pDocObj,pAct, pToGroup != NULL,pToGroup);
  }  
  return NULL;
};

// vytvo�en� akce pro p�esun objektu
CPosAction* Action_MovePoseidonObject(CPosEdObject* pNewPosObj,int xAmount,int yAmount,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pNewPosObj != NULL && !pNewPosObj->Locked())
	{
		CPosEdObject* pAct = (CPosEdObject*)pNewPosObj->CreateCopyObject();
		if (pAct != NULL)
		{	// objekt dokumentu, se kter�m hejbu
			CPosEdObject* pDocObj = (CPosEdObject*)pDocument->GetDocumentObjectFromValue(pAct);
			ASSERT(pDocObj != NULL);
			ASSERT_KINDOF(CPosEdObject,pAct);
			// offset objektu
			pAct->CalcOffsetLogPosition(xAmount,yAmount);
			// generuji ud�lost pro p�esun atributu
			return pDocument->GenerNewObjectAction(IDA_OBJECT_MOVE,pDocObj,pAct, pToGroup != NULL,pToGroup);
		}
	}
	return NULL;
};

/*CPosAction* Action_RotatePoseidonObject(CPosEdObject* pNewPosObj,float grad, CPointVal origin,  CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  if (pNewPosObj != NULL)
  {
    CPosEdObject* pAct = (CPosEdObject*)pNewPosObj->CreateCopyObject();
    if (pAct != NULL)
    {	// objekt dokumentu, se kter�m hejbu
      CPosEdObject* pDocObj = (CPosEdObject*)pDocument->GetDocumentObjectFromValue(pAct);
      ASSERT(pDocObj != NULL);
      ASSERT_KINDOF(CPosEdObject,pAct);
      // offset objektu
      pAct->CalcRotationPosition(grad,origin);      
      // generuji ud�lost pro p�esun atributu
      return pDocument->GenerNewObjectAction(IDA_OBJECT_MOVE,pDocObj,pAct, pToGroup != NULL,pToGroup);
    }
  }
  return NULL;
}

CPosAction* Action_MovePoseidonObject(CPosEdObject* pNewPosObj,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pNewPosObj != NULL)
	{
		CPosObject* pAct = pNewPosObj->CreateCopyObject();
		if (pAct != NULL)
		{	// objekt dokumentu, se kter�m hejbu
			CPosObject* pDocObj = pDocument->GetDocumentObjectFromValue(pAct);
			ASSERT(pDocObj != NULL);
			// generuji ud�lost pro p�esun atributu
			return pDocument->GenerNewObjectAction(IDA_OBJECT_MOVE,pDocObj,pAct, pToGroup != NULL,pToGroup);
		}
	}
	return NULL;
};*/

CPosAction* Action_RotateMovePoseidonObject(CPosEdObject* pNewPosObj,CPointVal offset,float grad, CPointVal origin,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  if (pNewPosObj != NULL && !pNewPosObj->Locked())
  {
    CPosEdObject* pAct = (CPosEdObject*)pNewPosObj->CreateCopyObject();
    if (pAct != NULL)
    {	// objekt dokumentu, se kter�m hejbu
      CPosEdObject* pDocObj = (CPosEdObject*)pDocument->GetDocumentObjectFromValue(pAct);
      ASSERT(pDocObj != NULL);
      ASSERT_KINDOF(CPosEdObject,pAct);
      // offset objektu
      pAct->CalcRotationPosition( grad, origin); 
      pAct->CalcOffsetLogPosition( offset.x, offset.y);
      // generuji ud�lost pro p�esun atributu
      return pDocument->GenerNewObjectAction(IDA_OBJECT_MOVE,pDocObj,pAct, pToGroup != NULL,pToGroup);
    }
  }
  return NULL;
};

CPosAction* Action_EditPoseidonObject(CPosEdObject* pNewPosObj, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup)
{
	if (pNewPosObj != NULL && !pNewPosObj->Locked())
	{
		CPosObject* pAct = pNewPosObj->CreateCopyObject();
		if (pAct != NULL)
		{	// objekt dokumentu, se kter�m hejbu
			CPosObject* pDocObj = pDocument->GetDocumentObjectFromValue(pAct);
			ASSERT(pDocObj != NULL);
			// generuji ud�lost pro editaci objektu
			return pDocument->GenerNewObjectAction(IDA_OBJECT_EDIT, pDocObj, pAct, FALSE, pToGroup);
		}
	}
	return NULL;
}

CPosAction* Action_RotatePoseidonObject(CPosEdObject * obj, float grad, CPosActionGroup* pToGroup)
{
  if (obj != NULL && !obj->Locked())
  {
    CPosEdObject* pAct = (CPosEdObject*)obj->CreateCopyObject();
    CPosEdMainDoc * doc = (CPosEdMainDoc *) obj->GetDocument();
    if (pAct != NULL)
    {	// objekt dokumentu, se kter�m hejbu
      CPosEdObject* pDocObj = (CPosEdObject*)doc->GetDocumentObjectFromValue(pAct);
      ASSERT(pDocObj != NULL);
      ASSERT_KINDOF(CPosEdObject,pAct);
      // rotate objectu
      pAct->CalcRotationPosition(grad);
      // generuji ud�lost pro p�esun atributu
      return doc->GenerNewObjectAction(IDA_OBJECT_MOVE,pDocObj,pAct, pToGroup != NULL,pToGroup);
    }
  }
  return NULL;
}

CPosAction* Action_SetRelHeightPoseidonObject(CPosEdObject * obj, float relHeight, CPosActionGroup* pToGroup)
{
  if (obj != NULL && !obj->Locked())
  {
    CPosEdObject* pAct = (CPosEdObject*)obj->CreateCopyObject();
    CPosEdMainDoc * doc = (CPosEdMainDoc *) obj->GetDocument();
    if (pAct != NULL)
    {	// objekt dokumentu, se kter�m hejbu
      CPosEdObject* pDocObj = (CPosEdObject*)doc->GetDocumentObjectFromValue(pAct);
      ASSERT(pDocObj != NULL);
      ASSERT_KINDOF(CPosEdObject,pAct);
      // rotate objectu
      pAct->SetRelHeight(relHeight);
      // generuji ud�lost pro p�esun atributu
      return doc->GenerNewObjectAction(IDA_OBJECT_MOVE,pDocObj,pAct, pToGroup != NULL,pToGroup);
    }
  }
  return NULL;
}


/////////////////////////////////////////////////////////////////////////////
// skupiny objekt�

CPosAction* Action_DeletePoseidonObjects(CPosGroupObject* pGroup,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pGroup != NULL)
	{
		CPosObjectGroupAction* pGrpAction = new CPosObjectGroupAction(IDA_GROUPOBJ_DELETE);
		if (pGrpAction != NULL)
		{
			for(int i=0; i<pGroup->m_SelObjects.GetSize(); i++)
			{
				CPosObject* pObj = (CPosObject*)(pGroup->m_SelObjects[i]);
				if (pObj != NULL && !pObj->Locked())
				{
					// generuji akci podle typu objektu
					// if (pObj->IsKindOf(RUNTIME_CLASS()))
					if (pObj->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
					{						
						Action_DeletePoseidonObject((CPosEdObject *) pObj,pDocument,pGrpAction);
					} else
					if (pObj->IsKindOf(RUNTIME_CLASS(CPosWoodObject)))
					{						
						Action_DeletePoseidonWood((CPosWoodObject *) pObj,pDocument,pGrpAction);
					} else
					if (pObj->IsKindOf(RUNTIME_CLASS(CPosNetObjectBase)))
					{						
						Action_DeletePoseidonNet((CPosNetObjectBase*) pObj, pDocument,pGrpAction);
					} else
					if (pObj->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)))
					{						
						Action_DeletePoseidonNameArea((CPosNameAreaObject*) pObj,pDocument,pGrpAction);
					} else
					if (pObj->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))
					{						
						Action_DeletePoseidonKeyPoint((CPosKeyPointObject*) pObj,pDocument,pGrpAction);
					};
				}
			}
			// provedu akci
			if (pToGroup)
				pToGroup->AddAction(pGrpAction);
			else
				pDocument->ProcessEditAction(pGrpAction);
		}
		return pGrpAction;
	}
	return NULL;
};

/* Replaced by Action_RotateMovePoseidonObjects 


CPosAction* Action_MovePoseidonObjects(CPosGroupObject* pGroup,int xAmount,int yAmount,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pGroup != NULL)
	{
		CPosObjectGroupAction* pGrpAction = new CPosObjectGroupAction(IDA_GROUPOBJ_MOVE);
    pGrpAction->m_bFromBuldozer = FALSE;

		if (pGrpAction != NULL)
		{
			for(int i=0; i<pGroup->m_SelObjects.GetSize(); i++)
			{
				CPosObject* pObj = (CPosObject*)(pGroup->m_SelObjects[i]);
				if (pObj != NULL)
				{
					// generuji akci podle typu objektu
					// if (pObj->IsKindOf(RUNTIME_CLASS()))
					if (pObj->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
					{
						CPosEdObject* pNewPosObj = (CPosEdObject*)(pObj->CreateCopyObject());				
						Action_MovePoseidonObject(pNewPosObj,xAmount,yAmount,pDocument,pGrpAction);		
					} else
					if (pObj->IsKindOf(RUNTIME_CLASS(CPosWoodObject)))
					{
						CPosWoodObject* pNewPosObj = (CPosWoodObject*)(pObj->CreateCopyObject());
						if (pNewPosObj)
							Action_MovePoseidonWood(pNewPosObj,xAmount,yAmount,pDocument,pGrpAction);
					} else
					if (pObj->IsKindOf(RUNTIME_CLASS(CPosNetObjectBase)))
					{	// offset polohy
						CPosNetObjectBase* pNewPosObj  = (CPosNetObjectBase*)(pObj->CreateCopyObject());
						// generov�n� p�esunu s�t�
						if (pNewPosObj)
							Action_MovePoseidonNet(pNewPosObj,xAmount,yAmount,pDocument,pGrpAction);
					} else
					if (pObj->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)))
					{
						CPosNameAreaObject* pNewPosObj = (CPosNameAreaObject*)(pObj->CreateCopyObject());
						if (pNewPosObj)
							Action_MovePoseidonNameArea(pNewPosObj,xAmount,yAmount,pDocument,pGrpAction);
					} else
					if (pObj->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))
					{
						CPosKeyPointObject* pNewPosObj = (CPosKeyPointObject*)(pObj->CreateCopyObject());
						if (pNewPosObj)
							Action_MovePoseidonKeyPoint(pNewPosObj,xAmount,yAmount,pDocument,pGrpAction);
					};
				}
			}
			// provedu akci
			if (pToGroup)
				pToGroup->AddAction(pGrpAction);
			else
				pDocument->ProcessEditAction(pGrpAction);
		}
		return pGrpAction;
	}
	return NULL;
};

CPosAction* Action_RotatePoseidonObjects(CPosGroupObject* pGroup,float grad, CPointVal origin,  CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  if (pGroup != NULL)
  {
    CPosObjectGroupAction* pGrpAction = new CPosObjectGroupAction(IDA_GROUPOBJ_MOVE);
    pGrpAction->m_bFromBuldozer = FALSE;

    if (pGrpAction != NULL)
    {
      for(int i=0; i<pGroup->m_SelObjects.GetSize(); i++)
      {
        CPosObject* pObj = (CPosObject*)(pGroup->m_SelObjects[i]);
        if (pObj != NULL)
        {
          // generuji akci podle typu objektu
          // if (pObj->IsKindOf(RUNTIME_CLASS()))
          if (pObj->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
          {
            CPosEdObject* pNewPosObj = (CPosEdObject*)(pObj->CreateCopyObject());            
            Action_RotatePoseidonObject(pNewPosObj,grad,origin,pDocument,pGrpAction);
            //delete pNewPosObj;
          } else if (pObj->IsKindOf(RUNTIME_CLASS(CPosWoodObject)))
          {
            CPosWoodObject* pNewPosObj = (CPosWoodObject*)(pObj->CreateCopyObject());
            Action_RotatePoseidonWood(pNewPosObj,grad,origin,pDocument,pGrpAction);
          } else if (pObj->IsKindOf(RUNTIME_CLASS(CPosNetObjectBase)))
          {	// offset polohy
            CPosNetObjectBase* pNewPosObj  = (CPosNetObjectBase*)(pObj->CreateCopyObject());
            // generov�n� p�esunu s�t�
            Action_RotatePoseidonNet(pNewPosObj,grad,origin,pDocument,pGrpAction);
          } else if (pObj->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)))
          {
            CPosNameAreaObject* pNewPosObj = (CPosNameAreaObject*)(pObj->CreateCopyObject());            
            Action_RotatePoseidonNameArea(pNewPosObj,grad,origin,pDocument,pGrpAction);
          } else if (pObj->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))
          {
            CPosKeyPointObject* pNewPosObj = (CPosKeyPointObject*)(pObj->CreateCopyObject());
            Action_RotatePoseidonKeyPoint(pNewPosObj,grad,origin,pDocument,pGrpAction);
          };
        }
      }
      // provedu akci
      if (pToGroup)
        pToGroup->AddAction(pGrpAction);
      else
        pDocument->ProcessEditAction(pGrpAction);
    }
    return pGrpAction;
  }
  return NULL;
};
*/
CPosAction* Action_RotateMovePoseidonObjects(CPosGroupObject* pGroup,CPointVal offset,float grad, CPointVal origin, CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup)
{
  if (pGroup != NULL)
  {
    CPosObjectGroupAction* pGrpAction = new CPosObjectGroupAction(IDA_GROUPOBJ_MOVE);
    pGrpAction->m_bFromBuldozer = FALSE;

    if (pGrpAction != NULL)
    {
      for(int i=0; i<pGroup->m_SelObjects.GetSize(); i++)
      {
        CPosObject* pObj = (CPosObject*)(pGroup->m_SelObjects[i]);
        if (pObj != NULL && !pObj->Locked())
        {
          // generuji akci podle typu objektu
          // if (pObj->IsKindOf(RUNTIME_CLASS()))
          if (pObj->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
          {
            CPosEdObject* pNewPosObj = (CPosEdObject*)(pObj->CreateCopyObject());				
            Action_RotateMovePoseidonObject(pNewPosObj,offset,grad,origin,pDocument,pGrpAction);		
          } else if (pObj->IsKindOf(RUNTIME_CLASS(CPosWoodObject)))
          {
            CPosWoodObject* pNewPosObj = (CPosWoodObject*)(pObj->CreateCopyObject());
            Action_RotateMovePoseidonWood(pNewPosObj,offset,grad,origin,pDocument,pGrpAction);
          } else if (pObj->IsKindOf(RUNTIME_CLASS(CPosNetObjectBase)))
          {	// offset polohy
            CPosNetObjectBase* pNewPosObj  = (CPosNetObjectBase*)(pObj->CreateCopyObject());
            // generov�n� p�esunu s�t�
            Action_RotateMovePoseidonNet(pNewPosObj,offset,grad,origin,pDocument,pGrpAction);
          } else if (pObj->IsKindOf(RUNTIME_CLASS(CPosNameAreaObject)))
          {
            CPosNameAreaObject* pNewPosObj = (CPosNameAreaObject*)(pObj->CreateCopyObject());                  
            Action_RotateMovePoseidonNameArea(pNewPosObj,offset,grad,origin,pDocument,pGrpAction);
          } else if (pObj->IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))
          {
            CPosKeyPointObject* pNewPosObj = (CPosKeyPointObject*)(pObj->CreateCopyObject());                    
            Action_RotateMovePoseidonKeyPoint(pNewPosObj,offset,grad,origin,pDocument,pGrpAction);
          };
        }
      }
      // provedu akci
      if (pToGroup)
        pToGroup->AddAction(pGrpAction);
      else
        pDocument->ProcessEditAction(pGrpAction);
    }
    return pGrpAction;
  }
  return NULL;
};


/////////////////////////////////////////////////////////////////////////////
// Objecty z clipboardu

CPosAction* Action_PasteClipboardObject(PosClipBoardObject * clipData,CPosEdMainDoc* pDocument,const CPoint& cursorLog, CPosActionGroup* pToGroup)
{
  CPosObjectGroupAction* pGrpAction = new CPosObjectGroupAction(IDA_GROUPOBJ_PASTE);
  if (pGrpAction != NULL)
  {
    REALPOS realDiff = pDocument->m_PoseidonMap.FromViewToRealPos(cursorLog);
    realDiff.x -= clipData->rectreal[0];
    realDiff.z -= clipData->rectreal[3];

    CPoint logDiff = CPoint(pDocument->m_PoseidonMap.FromRealToViewPos(realDiff)) 
      - pDocument->m_PoseidonMap.FromRealToViewPos(REALPOS(0,0));

    CMemFile file;
    file.Attach(clipData->data, clipData->dataSize);
    CArchive ar(&file, CArchive::load);

    for(int i = 0; i < clipData->nnameareas; i++)
    {
      CPosNameAreaObject * nameObj = new CPosNameAreaObject(pDocument->m_PoseidonMap);
      if (nameObj)
      {
        nameObj->DoSerialize(ar, POSED_FILE_VER);
        nameObj->CalcObjectLogPosition();
        nameObj->GetLogObjectPosition()->OffsetPosition(logDiff.x, logDiff.y);
        nameObj->SetFreeID();        
        // generov�n� akce
        pDocument->GenerNewObjectAction(IDA_NAMEAREA_CREATE,NULL,nameObj,FALSE,pGrpAction);
      }
    }

    for(int i = 0; i < clipData->nwoods; i++)
    {
      CPosWoodObject * woodObj = new CPosWoodObject(pDocument->m_PoseidonMap);
      if (woodObj)
      {
        woodObj->DoSerialize(ar, POSED_FILE_VER);
        woodObj->CalcObjectLogPosition();

        woodObj->GetLogObjectPosition()->OffsetPosition(logDiff.x,logDiff.y);

        if (NULL == woodObj->GetWoodTemplate())
        {         
          LogF("Cannot paste CPosWoodObject %s. Template not found)", woodObj->GetTemplateName());
          delete woodObj;

          int nWoodItems;
          MntSerializeInt(ar,nWoodItems);

          CPosEdObject obj(pDocument->m_PoseidonMap);
          for(int j = 0; j < nWoodItems; j++)          
            obj.DoSerialize(ar, POSED_FILE_VER);

          continue;
        }

        woodObj->SetFreeID(); 

        int nWoodItems;
        MntSerializeInt(ar,nWoodItems);

        //CPosActionGroup * pGrpAction = new CPosActionGroup(IDA_WOOD_CREATE);
        //pGrpAction->m_bFromBuldozer = FALSE;

        (CPosObjectAction*)(pDocument->GenerNewObjectAction(IDA_WOOD_CREATE,
          NULL,woodObj,FALSE,pGrpAction));

         for(int j = 0; j < nWoodItems; j++)
         {
           CPosEdObject * pObj = new CPosEdObject(pDocument->m_PoseidonMap);
           if (pObj)
           {
             pObj->DoSerialize(ar, POSED_FILE_VER);
             pObj->SetFreeID();
             pObj->m_nMasterWood = woodObj->GetID();
             pObj->CalcOffsetLogPosition(logDiff.x, logDiff.y);
             pObj->CalcObjectPoseidonHeight();

             CPosObjectAction * pAction = (CPosObjectAction *) pDocument->GenerNewObjectAction(IDA_OBJECT_ADD,NULL,pObj,FALSE, pGrpAction);

             pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
             // nebude se prov�d�t update
             pAction->m_bUpdateView	  = FALSE;
             // objekt je pod��zen lesu
             /*ASSERT(pAction->m_pOldValue != NULL);
             ASSERT_KINDOF(CPosEdObject,pAction->m_pOldValue);
             ((CPosEdObject*)(pAction->m_pOldValue))->m_nMasterWood = pWood->m_nWoodID;*/
           }     
         }
      }
    }

    for(int i = 0; i < clipData->nkey; i++)
    {
      CPosKeyPointObject * keyObj = new CPosKeyPointObject(pDocument->m_PoseidonMap);
      if (keyObj)
      {
        keyObj->DoSerialize(ar, POSED_FILE_VER);
        keyObj->SetFreeID();
        // poloha       
        keyObj->m_ptRealPos.x  += realDiff.x;
        keyObj->m_ptRealPos.z  += realDiff.z;

        keyObj->CalcObjectLogPosition();
        // generov�n� akce
        pDocument->GenerNewObjectAction(IDA_KEYPOINT_CREATE,NULL,keyObj,FALSE,pGrpAction);
      }
    }

    for(int i = 0; i < clipData->nets; i++)
    {
      CPosNetObjectKey * netObj = new CPosNetObjectKey(pDocument->m_PoseidonMap);      
      if (netObj)
      {
        netObj->DoSerialize(ar, POSED_FILE_VER);
        // reserve ID //TODO: some intelgent ID reserve...
        netObj->SetFreeID();        
        if (!netObj->PrepareForPaste(*pDocument))
        {
          LogF("Cannot paste CPosNetObjectKey %s. Templates not found", netObj->m_pBasePartKEY->m_nComponentName);
          continue;
        }

        netObj->m_nBaseRealPos.nPos.x += realDiff.x;
        netObj->m_nBaseRealPos.nPos.z += realDiff.z;

        netObj->DoUpdateNetPartsBaseRealPos();
        netObj->DoUpdateNetPartsLogPosition();
        
        // vytvo�en� objektu s�t�
        CPosObjectAction* pNetAct = (CPosObjectAction*)(pDocument->GenerNewObjectAction(IDA_NET_KEY_CREATE,
          NULL,netObj,FALSE,pGrpAction));

        // generov�n�/update z�kladn�ch objekt� s�t�
        if(!DoUpdateNetPoseidonObjects(netObj,pDocument,pGrpAction))
        {
          if (pGrpAction)
            delete pGrpAction;
          else
            if (netObj)
              delete netObj;
          return NULL;
        };
      }
    }

    CProgressDialog progress;
    progress.Create();
    progress.SetRange(clipData->nobjs);
    for(int i = 0; i < clipData->nobjs; i++)
    {
      progress.DoStep();
      bool askinvalidtemplate=true;
      CPosEdObject* pObject = new CPosEdObject(pDocument->m_PoseidonMap);
      if (pObject)
      {
        CString templateName;
        MntSerializeString(ar,templateName);
        pObject->DoSerialize(ar, POSED_FILE_VER);
        Ref<CPosObjectTemplate> templatePtr=pDocument->GetObjectTemplateByName(templateName);
        if (templatePtr!=0)        
        {
          pObject->m_nTemplateID=templatePtr->m_ID;                    
          pObject->m_Position(0,3) += realDiff.x;
          pObject->m_Position(2,3) += realDiff.z;
          pObject->AllocPositions();
          pObject->SetFreeID();

          pObject->CalcObjectPoseidonHeight();
          pObject->CalcObjectLogPosition();

          pDocument->GenerNewObjectAction(IDA_OBJECT_ADD,NULL,pObject,FALSE, pGrpAction);
        }
        else
        {
          delete pObject;
          if (askinvalidtemplate)
          {
            CString t;
            AfxFormatString1(t,IDS_PASTE_INVALID_TEMPLATE,templateName);
            int i=AfxMessageBox(t,MB_ABORTRETRYIGNORE|MB_ICONINFORMATION);
            if (i==IDABORT) break;
            if (i==IDIGNORE) askinvalidtemplate=false;
          }
        }
      }
    }
  
    ar.Close();

    if (pToGroup)
      pToGroup->AddAction(pGrpAction);
    else
      pDocument->ProcessEditAction(pGrpAction);
  }
  return pGrpAction;
}

CPosAction* Action_PasteAbsolutClipboardObject(PosClipBoardObject * clipData,CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup)
{
  CPosObjectGroupAction* pGrpAction = new CPosObjectGroupAction(IDA_GROUPOBJ_PASTE);
  if (pGrpAction != NULL)
  {    
    CMemFile file;
    file.Attach(clipData->data, clipData->dataSize);
    CArchive ar(&file, CArchive::load);

    for(int i = 0; i < clipData->nnameareas; i++)
    {
      CPosNameAreaObject * nameObj = new CPosNameAreaObject(pDocument->m_PoseidonMap);
      
      nameObj->DoSerialize(ar, POSED_FILE_VER);      
      nameObj->CalcObjectLogPosition();        
      nameObj->SetFreeID();        
      // generov�n� akce
      pDocument->GenerNewObjectAction(IDA_NAMEAREA_CREATE,NULL,nameObj,FALSE,pGrpAction);           
    }

    for(int i = 0; i < clipData->nwoods; i++)
    {
      CPosWoodObject * woodObj = new CPosWoodObject(pDocument->m_PoseidonMap);
      if (woodObj)
      {
        woodObj->DoSerialize(ar, POSED_FILE_VER);
        woodObj->CalcObjectLogPosition();

        if (NULL == woodObj->GetWoodTemplate())
        {         
          LogF("Cannot paste CPosWoodObject %s. Template not found)", woodObj->GetTemplateName());
          delete woodObj;

          int nWoodItems;
          MntSerializeInt(ar,nWoodItems);

          CPosEdObject obj(pDocument->m_PoseidonMap);
          for(int j = 0; j < nWoodItems; j++)          
            obj.DoSerialize(ar, POSED_FILE_VER);

          continue;
        }

        woodObj->SetFreeID(); 

        int nWoodItems;
        MntSerializeInt(ar,nWoodItems);

        //CPosActionGroup * pGrpAction = new CPosActionGroup(IDA_WOOD_CREATE);
        //pGrpAction->m_bFromBuldozer = FALSE;

        (CPosObjectAction*)(pDocument->GenerNewObjectAction(IDA_WOOD_CREATE,
          NULL,woodObj,FALSE,pGrpAction));

        for(int j = 0; j < nWoodItems; j++)
        {
          CPosEdObject * pObj = new CPosEdObject(pDocument->m_PoseidonMap);
          if (pObj)
          {
            pObj->DoSerialize(ar, POSED_FILE_VER);
            pObj->SetFreeID();
            pObj->m_nMasterWood = woodObj->GetID();
            pObj->CalcObjectPoseidonHeight();

            CPosObjectAction * pAction = (CPosObjectAction *) pDocument->GenerNewObjectAction(IDA_OBJECT_ADD,NULL,pObj,FALSE, pGrpAction);

            pAction->m_nSelectObjType = CPosObjectAction::ida_SelectNone;
            // nebude se prov�d�t update
            pAction->m_bUpdateView	  = FALSE;
            /*
            // objekt je pod��zen lesu
            ASSERT(pAction->m_pOldValue != NULL);
            ASSERT_KINDOF(CPosEdObject,pAction->m_pOldValue);
            ((CPosEdObject*)(pAction->m_pOldValue))->m_nMasterWood = pWood->m_nWoodID;*/
          }     
        }
      }
    }


    for(int i = 0; i < clipData->nkey; i++)
    {
      CPosKeyPointObject * keyObj = new CPosKeyPointObject(pDocument->m_PoseidonMap);
      if (keyObj)
      {
        keyObj->DoSerialize(ar, POSED_FILE_VER);
        keyObj->SetFreeID();
        // poloha              
        keyObj->CalcObjectLogPosition();
        // generov�n� akce
        pDocument->GenerNewObjectAction(IDA_KEYPOINT_CREATE,NULL,keyObj,FALSE,pGrpAction);
      }
    }

    for(int i = 0; i < clipData->nets; i++)
    {
      CPosNetObjectKey * netObj = new CPosNetObjectKey(pDocument->m_PoseidonMap);      
      if (netObj)
      {
        netObj->DoSerialize(ar, POSED_FILE_VER);
        // reserve ID //TODO: some intelgent ID reserve...
        netObj->SetFreeID();
        if (!netObj->PrepareForPaste(*pDocument))
        {
          LogF("Cannot paste CPosNetObjectKey %s. Templates not found", netObj->m_pBasePartKEY->m_nComponentName);
          continue;
        }        

        netObj->DoUpdateNetPartsBaseRealPos();
        netObj->DoUpdateNetPartsLogPosition();

        // vytvo�en� objektu s�t�
        CPosObjectAction* pNetAct = (CPosObjectAction*)(pDocument->GenerNewObjectAction(IDA_NET_KEY_CREATE,
          NULL,netObj,FALSE,pGrpAction));

        // generov�n�/update z�kladn�ch objekt� s�t�
        if(!DoUpdateNetPoseidonObjects(netObj,pDocument,pGrpAction))
        {
          if (pGrpAction)
            delete pGrpAction;
          else
            if (netObj)
              delete netObj;
          return NULL;
        };
      }
    }

    CProgressDialog progress;
    progress.Create();
    progress.SetRange(clipData->nobjs);
    for(int i = 0; i < clipData->nobjs; i++)
    {
      progress.DoStep();
      bool askinvalidtemplate=true;
      CPosEdObject* pObject = new CPosEdObject(pDocument->m_PoseidonMap);
      if (pObject)
      {
        CString templateName;
        MntSerializeString(ar,templateName);
        pObject->DoSerialize(ar, POSED_FILE_VER);
        Ref<CPosObjectTemplate> templatePtr=pDocument->GetObjectTemplateByName(templateName);
        if (templatePtr!=0)        
        {
          pObject->m_nTemplateID=templatePtr->m_ID;                    
          pObject->m_pTemplate=0;
          pObject->AllocPositions();
          pObject->SetFreeID();
        
          pObject->CalcObjectPoseidonHeight();
          pObject->CalcObjectLogPosition();

          pDocument->GenerNewObjectAction(IDA_OBJECT_ADD,NULL,pObject,FALSE, pGrpAction);
        }
        else
        {
          delete pObject;
          if (askinvalidtemplate)
          {
            CString t;
            AfxFormatString1(t,IDS_PASTE_INVALID_TEMPLATE,templateName);
            int i=AfxMessageBox(t,MB_ABORTRETRYIGNORE|MB_ICONINFORMATION);
            if (i==IDABORT) break;
            if (i==IDIGNORE) askinvalidtemplate=false;
          }
        }
      }
    }

    ar.Close();

    if (pToGroup)
      pToGroup->AddAction(pGrpAction);
    else
      pDocument->ProcessEditAction(pGrpAction);
  }
  return pGrpAction;
}

////////////////////////////////////////////////
// Snap objects

CPosAction* Action_SnapObjects(CPosGroupObject& objGrp, CPosEdMainDoc & doc, CPosActionGroup* pToGroup)
{ 
  if (objGrp.GetGroupSize() < 2)
    return NULL;

  CPosObjectGroupAction* pGrpAction = new CPosObjectGroupAction(IDA_GROUPOBJ_MOVE);
  pGrpAction->m_bFromBuldozer = FALSE;

  FindSnapping snapping;

  snapping.AddSnapToObj((CPosEdObject *) objGrp[0]);
  CPosGroupObject objs = objGrp;
  objs.DelObjectGroup(objGrp[0]);

  // dummy replacement
  Action_MovePoseidonObject((CPosEdObject *) objGrp[0], 0,0, &doc, pGrpAction);

  for(; 0 < objs.GetGroupSize();)
  {    
    CPoint offsetLog(0,0);
    float rotatedGrad(0);
    CPoint originLog(0,0);
    CPosEdObject * snapped;

    if (snapping(snapped, offsetLog, rotatedGrad, originLog, objs, doc.m_PoseidonMap))
    {
      objs.DelObjectGroup(snapped);      
      // create actionand then use shifted object from action
      CPosObjectAction * action = ( CPosObjectAction *) Action_RotateMovePoseidonObject(snapped, offsetLog, rotatedGrad, originLog, &doc, pGrpAction);
      snapping.AddSnapToObj((CPosEdObject *) action->m_pOldValue);
    }
    else
    {
      snapped = (CPosEdObject *) objs[0];
      objs.DelObjectGroup(snapped);
      snapping.AddSnapToObj(snapped);

      // dummy replacement
      Action_MovePoseidonObject(snapped, 0,0, &doc, pGrpAction);
    }

  }

  if (pToGroup)
    pToGroup->AddAction(pGrpAction);
  else
    doc.ProcessEditAction(pGrpAction);

  return pGrpAction;
}

////////////////////////////////////////////////
// Lock & unlock object

CPosAction* Action_LockPoseidonObject(CPosObject* obj, bool lock, CPosEdMainDoc & doc, CPosActionGroup* toGrp)
{    
  if (obj != NULL && ((lock && !obj->Locked()) || (!lock && obj->Locked())))
  {
    CPosObject* pNewPosObj = (CPosObject*)(obj->CreateCopyObject());		
    pNewPosObj->Lock(lock);

    CPosAction * action = doc.GenerNewObjectAction(IDA_OBJECT_LOCK,obj,pNewPosObj, TRUE, toGrp); 
      
    return action; 
  }
  return NULL;
};

CPosAction* Action_LockPoseidonObjects(CPosGroupObject* grp, bool lock, CPosEdMainDoc & doc, CPosActionGroup* toGrp)
{
  if (grp != NULL)
  {
    CPosObjectGroupAction* grpAction = new CPosObjectGroupAction(IDA_GROUPOBJ_MOVE);
    grpAction->m_bFromBuldozer = FALSE;

    if (grpAction != NULL)
    {
      for(int i=0; i<grp->m_SelObjects.GetSize(); i++)
      {
        CPosObject* obj = (CPosObject*)(grp->m_SelObjects[i]);
        Action_LockPoseidonObject(obj, lock, doc, grpAction);        
      }    
        
      // provedu akci
      if (toGrp)
        toGrp->AddAction(grpAction);
      else
        doc.ProcessEditAction(grpAction);
    }

    return grpAction;
  }
  return NULL;
};
