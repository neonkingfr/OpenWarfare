/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"
#include <el/Pathname/Pathname.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditKeyPointObjectDlg dialog

#define MIN_RAD_SIZE	3.
#define MAX_RAD_SIZE	7000.
class CEditKeyPointObjectDlg : public CDialog
{
// Construction
public:
	CEditKeyPointObjectDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEditKeyPointObjectDlg)
	enum { IDD = IDD_EDIT_KEYPOINT_OBJECT };
	CComboBox		m_cComboFrame;
	CComboBox		m_cComboEntire;
	CFMntColorButton	m_cColorFrame;
	CFMntColorButton	m_cColorEntire;
	CString			m_sName;
	int				m_nStyle;
	BOOL			m_bIsVisible;
	double			m_nRadA;
	double			m_nRadB;
	//}}AFX_DATA
	COLORREF		m_cAreaEntire;
	COLORREF		m_cAreaFrame;

  CString m_Text;
  CString m_Type;
  CString m_Properties;
  CComboBox m_wType;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditKeyPointObjectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEditKeyPointObjectDlg)
	afx_msg void OnColorEntire();
	afx_msg void OnColorFrame();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
  virtual BOOL OnInitDialog();
};

CEditKeyPointObjectDlg::CEditKeyPointObjectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEditKeyPointObjectDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditKeyPointObjectDlg)
	m_sName  = _T("");
	m_nStyle = 0;
	m_bIsVisible = TRUE;
	//}}AFX_DATA_INIT
}


void CEditKeyPointObjectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditKeyPointObjectDlg)
	DDX_Control(pDX, IDC_COMBO_COLOR_FRAME, m_cComboFrame);
	DDX_Control(pDX, IDC_COMBO_COLOR_ENTIRE, m_cComboEntire);
	DDX_Control(pDX, IDC_COLOR_FRAME, m_cColorFrame);
	DDX_Control(pDX, IDC_COLOR_ENTIRE, m_cColorEntire);
	DDX_Check(pDX, IDC_CHECK_VISIBLE, m_bIsVisible);
  DDX_Text(pDX,IDC_DESCRIPTION,m_Text);
  DDX_Text(pDX,IDC_TYPE,m_Type);
  DDX_Control(pDX,IDC_TYPE,m_wType);
  DDX_Text(pDX,IDC_PROPERTIES,m_Properties);
	//}}AFX_DATA_MAP
	MDDX_Text(pDX, IDC_EDIT_NAME, m_sName);
	MDDV_MaxChars(pDX, m_sName, MAX_AREA_NAME_LEN, IDC_EDIT_NAME);
	if (SAVEDATA)
		MDDV_MinChars(pDX, m_sName, 1);

	MDDX_Text(pDX, IDC_EDIT_RADA, m_nRadA);
	MDDV_MinMaxDouble(pDX, m_nRadA, MIN_RAD_SIZE, MAX_RAD_SIZE, IDC_EDIT_RADA, 1.);
	MDDX_Text(pDX, IDC_EDIT_RADB, m_nRadB);
	MDDV_MinMaxDouble(pDX, m_nRadB, MIN_RAD_SIZE, MAX_RAD_SIZE, IDC_EDIT_RADB, 1.);

	// barvy
	if (SAVEDATA)
	{
		if (m_cComboFrame.GetCurSel()==0)
			m_cAreaFrame = DEFAULT_COLOR;
		else
			m_cAreaFrame = m_cColorFrame.m_cColor;

		if (m_cComboEntire.GetCurSel()==0)
			m_cAreaEntire= DEFAULT_COLOR;
		else
			m_cAreaEntire= m_cColorEntire.m_cColor;
	} else
	{
		if (m_cAreaFrame == DEFAULT_COLOR)
		{
			m_cComboFrame.SetCurSel(0);
			m_cColorFrame.m_cColor = GetPosEdEnvironment()->m_cfgCurrent.m_cDefColors[CPosEdCfg::defcolFKeyP];
		} else
		{
			m_cComboFrame.SetCurSel(1);
			m_cColorFrame.m_cColor = m_cAreaFrame;
		};
		if (m_cAreaEntire == DEFAULT_COLOR)
		{
			m_cComboEntire.SetCurSel(0);
			m_cColorEntire.m_cColor = GetPosEdEnvironment()->m_cfgCurrent.m_cDefColors[CPosEdCfg::defcolKeyPt];
		} else
		{
			m_cComboEntire.SetCurSel(1);
			m_cColorEntire.m_cColor = m_cAreaEntire;
		};
	};
	// styl
	DDX_CBIndex(pDX, IDC_COMBO_STYLE, m_nStyle);
}


BEGIN_MESSAGE_MAP(CEditKeyPointObjectDlg, CDialog)
	//{{AFX_MSG_MAP(CEditKeyPointObjectDlg)
	ON_BN_CLICKED(IDC_COLOR_ENTIRE, OnColorEntire)
	ON_BN_CLICKED(IDC_COLOR_FRAME, OnColorFrame)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CEditKeyPointObjectDlg::OnColorEntire() 
{
	if (m_cColorEntire.DoEditColor())
		m_cComboEntire.SetCurSel(1);
}

void CEditKeyPointObjectDlg::OnColorFrame() 
{
	if (m_cColorFrame.DoEditColor())
		m_cComboFrame.SetCurSel(1);
}

/////////////////////////////////////////////////////////////////////////////
// Editace kl��ov�ho bodu

BOOL  DoEditKeyPoint(CPosKeyPointObject* pKeyPt,BOOL bNew)
{
	CEditKeyPointObjectDlg dlg;
	// kopie parametr�
	//dlg.m_bNew  = bNew;
	dlg.m_sName			= pKeyPt->m_sName;
	dlg.m_cAreaEntire	= pKeyPt->m_cEntire;
	dlg.m_cAreaFrame	= pKeyPt->m_cFrame;
	dlg.m_nStyle		= pKeyPt->m_nStyle;
	dlg.m_bIsVisible	= pKeyPt->m_bVisible;
	dlg.m_nRadA			= pKeyPt->m_nRealWidth;
	dlg.m_nRadB			= pKeyPt->m_nRealHeight;
  dlg.m_Type      = pKeyPt->m_Type;
  dlg.m_Text      = pKeyPt->m_Text;
  dlg.m_Properties = pKeyPt->m_otherProp;

	// relizace 
	if (dlg.DoModal() != IDOK)
		return FALSE;

	// li�� se parametry ?
	if ( bNew || (lstrcmp(dlg.m_sName,pKeyPt->m_sName)!=0)||
		(dlg.m_cAreaEntire != pKeyPt->m_cEntire)||
		(dlg.m_cAreaFrame  != pKeyPt->m_cFrame)||
		(dlg.m_nStyle	   != pKeyPt->m_nStyle)||
		(dlg.m_bIsVisible  != pKeyPt->m_bVisible)||
		(dlg.m_nRadA	!= (double)pKeyPt->m_nRealWidth)||
		(dlg.m_nRadB	!= (double)pKeyPt->m_nRealHeight)||
    (dlg.m_Text!=pKeyPt->m_Text)||
    (dlg.m_Type!=pKeyPt->m_Type)||
    (dlg.m_Properties!=pKeyPt->m_otherProp))
	{
		// kopie dat
		pKeyPt->m_sName			= dlg.m_sName;
		pKeyPt->m_cEntire		= dlg.m_cAreaEntire;
		pKeyPt->m_cFrame		= dlg.m_cAreaFrame;
		pKeyPt->m_nStyle		= dlg.m_nStyle;
		pKeyPt->m_bVisible		= dlg.m_bIsVisible;
    pKeyPt->m_Text      =dlg.m_Text;
    pKeyPt->m_Type      =dlg.m_Type;
		pKeyPt->m_nRealWidth	= (float)(dlg.m_nRadA);
		pKeyPt->m_nRealHeight	= (float)(dlg.m_nRadB);
    pKeyPt->m_otherProp=dlg.m_Properties;
		return TRUE;
	}
	// ��dn� zm�na
	return FALSE;
};

/////////////////////////////////////////////////////////////////////////////
// Akce s kl��ov�mi body

// vytvo�en� pojmenovan� plochy
CPosAction* Action_CreatePoseidonKeyPoint(CPoint ptLog,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	CPosKeyPointObject* pKeyPt  = NULL;
	CPosAction*			pAction = NULL;
	TRY
	{
		// vytvo�en� pojmenovan� skupiny
		if ((pKeyPt = new CPosKeyPointObject(pDocument->m_PoseidonMap))==NULL) AfxThrowMemoryException();
		// parametry pro pojmenovanou skupini
		if (!DoEditKeyPoint(pKeyPt,TRUE))
		{
			delete pKeyPt;
			return NULL;
		};
		// nov� ID
		pKeyPt->SetFreeID();
		// poloha
		pKeyPt->m_ptRealPos= pDocument->m_PoseidonMap.FromViewToRealPos(ptLog);
		pKeyPt->CalcObjectLogPosition();
		// generov�n� akce
		pAction = pDocument->GenerNewObjectAction(IDA_KEYPOINT_CREATE,NULL,pKeyPt,FALSE,pToGroup);
		// uvoln�n� plochy
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pKeyPt)
		{
			delete pKeyPt;
			pKeyPt = NULL;
		} 
	}
	END_CATCH_ALL
	return pAction;
};

// smaz�n� pojmenovan� plochy
CPosAction* Action_DeletePoseidonKeyPoint(CPosKeyPointObject* pKeyPt,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pKeyPt != NULL)
	{
		CPosObject* pDocObj = pDocument->GetDocumentObjectFromValue(pKeyPt);
		if (pDocObj)
		return pDocument->GenerNewObjectAction(IDA_KEYPOINT_DELETE,pDocObj,NULL,FALSE,pToGroup);
	}
	return NULL;
};

// p�esun pojmenovan� plochy
CPosAction* Action_MovePoseidonKeyPoint(CPosKeyPointObject* pKeyPt,int xAmount,int yAmount,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pKeyPt != NULL)
	{
		CPosKeyPointObject* pAct = (CPosKeyPointObject*)pKeyPt->CreateCopyObject();
		if (pAct != NULL)
		{	// objekt dokumentu, se kter�m hejbu
			CPosKeyPointObject* pDocObj = (CPosKeyPointObject*)pDocument->GetDocumentObjectFromValue(pAct);
			ASSERT(pDocObj != NULL);
			ASSERT_KINDOF(CPosKeyPointObject,pAct);
			ASSERT_KINDOF(CPosKeyPointObject,pDocObj);
			// offset objektu
			pAct->CalcOffsetLogPosition(xAmount,yAmount);
			// generuji ud�lost pro p�esun atributu
			return pDocument->GenerNewObjectAction(IDA_KEYPOINT_MOVE,pDocObj,pAct,FALSE,pToGroup);
		}
	}
	return NULL;
};

CPosAction* Action_RotatePoseidonKeyPoint(CPosKeyPointObject* pKeyPt,float grad,CPointVal origin,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  if (pKeyPt != NULL)
  {
    CPosKeyPointObject* pAct = (CPosKeyPointObject*)pKeyPt->CreateCopyObject();
    if (pAct != NULL)
    {	// objekt dokumentu, se kter�m hejbu
      CPosKeyPointObject* pDocObj = (CPosKeyPointObject*)pDocument->GetDocumentObjectFromValue(pAct);
      ASSERT(pDocObj != NULL);
      ASSERT_KINDOF(CPosKeyPointObject,pAct);
      ASSERT_KINDOF(CPosKeyPointObject,pDocObj);
      // offset objektu
      pAct->CalcRotateLogPosition(grad,origin);
      // generuji ud�lost pro p�esun atributu
      return pDocument->GenerNewObjectAction(IDA_KEYPOINT_MOVE,pDocObj,pAct,FALSE,pToGroup);
    }
  }
  return NULL;
};

CPosAction* Action_RotateMovePoseidonKeyPoint(CPosKeyPointObject* pKeyPt,CPointVal offset,float grad,CPointVal origin,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  if (pKeyPt != NULL)
  {
    CPosKeyPointObject* pAct = (CPosKeyPointObject*)pKeyPt->CreateCopyObject();
    if (pAct != NULL)
    {	// objekt dokumentu, se kter�m hejbu
      CPosKeyPointObject* pDocObj = (CPosKeyPointObject*)pDocument->GetDocumentObjectFromValue(pAct);
      ASSERT(pDocObj != NULL);
      ASSERT_KINDOF(CPosKeyPointObject,pAct);
      ASSERT_KINDOF(CPosKeyPointObject,pDocObj);
      // offset objektu
      pAct->CalcRotateLogPosition(grad,origin);
      pAct->CalcOffsetLogPosition(offset.x,offset.y);

      // generuji ud�lost pro p�esun atributu
      return pDocument->GenerNewObjectAction(IDA_KEYPOINT_MOVE,pDocObj,pAct,FALSE,pToGroup);
    }
  }
  return NULL;
};


// editace pojmenovan� plochy
CPosAction* Action_EditPoseidonKeyPoint(CPosKeyPointObject* pKeyPt,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
	if (pKeyPt != NULL)
	{
		CPosKeyPointObject* pAct = (CPosKeyPointObject*)(pKeyPt->CreateCopyObject());
		if (pAct != NULL)
		{	// objekt dokumentu, se kter�m hejbu
			CPosKeyPointObject* pDocObj = (CPosKeyPointObject*)pDocument->GetDocumentObjectFromValue(pAct);
			ASSERT(pDocObj != NULL);
			ASSERT_KINDOF(CPosKeyPointObject,pAct);
			ASSERT_KINDOF(CPosKeyPointObject,pDocObj);
			// editace objektu
			if (!DoEditKeyPoint(pAct,FALSE))
			{
				delete pAct;
				return NULL;
			};
			// nov� poloha ?
			pAct->CalcObjectLogPosition();
			// generuji ud�lost pro editaci atributu
			return pDocument->GenerNewObjectAction(IDA_KEYPOINT_EDIT,pDocObj,pAct,FALSE,pToGroup);
		}
	}
	return NULL;
};

static void LoadTypeList(CComboBox &cb)
{
  Pathname listPath=Pathname::GetExePath();
  listPath.SetFilename("KPTypes.dat");

  using namespace std;
  ifstream in(listPath,ios::in);
  if (!in) return;

  char buff[256];
  while (!(!in) && !in.eof())
  {
    in.getline(buff,sizeof(buff),'\n');
    cb.AddString(buff);
  }
}


BOOL CEditKeyPointObjectDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  LoadTypeList(m_wType);
  

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}
