/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"
#include "PosClipboard.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Akce s v��kou

/////////////////////////////////////////////////////////////////////////////
// Eroze krajiny - CWashLandscapeDlg

class CWashLandscapeDlg : public CDialog
{
// Construction
public:
	CWashLandscapeDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	WashParams	m_WashParams;

	//{{AFX_DATA(CWashLandscapeDlg)
	enum { IDD = IDD_EROZE_LAND_PARAMS };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWashLandscapeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CWashLandscapeDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CWashLandscapeDlg::CWashLandscapeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWashLandscapeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CWashLandscapeDlg)
	//}}AFX_DATA_INIT

	// default values pro m_WashParams
    m_WashParams.nGens		= 32;
    m_WashParams.washCoef	= 0.05;
    m_WashParams.sedimCoef	= 80;
    m_WashParams.sedimBase	= 0.1;
    m_WashParams.sedimEff	= 0.9;
    m_WashParams.initRain	= 0.1;
    m_WashParams.steadyRain	= 0.05;
    m_WashParams.finalBlur	= TRUE;
}

BOOL CWashLandscapeDlg::OnInitDialog() 
{	
	CDialog::OnInitDialog();
	return TRUE;
}

void CWashLandscapeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWashLandscapeDlg)
	
	//}}AFX_DATA_MAP
	
	// update parametr�

    // m_WashParams.nGens
	MDDX_Text(pDX, IDC_EDIT_GENS, m_WashParams.nGens);
	MDDV_MinMaxInt(pDX, m_WashParams.nGens, 0, 128, IDC_EDIT_GENS);

    // m_WashParams.washCoef	= 0.05f;
	MDDX_Text(pDX, IDC_EDIT_WASH_COEF, m_WashParams.washCoef);
	MDDV_MinMaxDouble(pDX, m_WashParams.washCoef, 0., 1., IDC_EDIT_WASH_COEF, 0.1);
    // m_WashParams.sedimCoef	= 80;
	MDDX_Text(pDX, IDC_EDIT_SEDIM_COEF, m_WashParams.sedimCoef);
	MDDV_MinMaxDouble(pDX, m_WashParams.sedimCoef, 50., 300., IDC_EDIT_SEDIM_COEF, 5.);
    // m_WashParams.sedimBase	= 0.1f;
	MDDX_Text(pDX, IDC_EDIT_SEDIM_BASE, m_WashParams.sedimBase);
	MDDV_MinMaxDouble(pDX, m_WashParams.sedimBase, 0., 1., IDC_EDIT_SEDIM_BASE, 0.1);
    // m_WashParams.sedimEff	= 0.9f;
	MDDX_Text(pDX, IDC_EDIT_SEDIM_EFF, m_WashParams.sedimEff);
	MDDV_MinMaxDouble(pDX, m_WashParams.sedimEff, 0., 1., IDC_EDIT_SEDIM_EFF, 0.1);
    // m_WashParams.initRain	= 0.1f;
	MDDX_Text(pDX, IDC_EDIT_INIT_RAIN, m_WashParams.initRain);
	MDDV_MinMaxDouble(pDX, m_WashParams.initRain, 0., 1., IDC_EDIT_INIT_RAIN, 0.1);
    // m_WashParams.steadyRain	= 0.05f;
	MDDX_Text(pDX, IDC_EDIT_STEADY_RAIN, m_WashParams.steadyRain);
	MDDV_MinMaxDouble(pDX, m_WashParams.steadyRain, 0., 1., IDC_EDIT_STEADY_RAIN, 0.1);
    //m_WashParams.finalBlur	= TRUE;
	DDX_Check(pDX, IDC_CHECK_FINAL_BLUR, m_WashParams.finalBlur);
}


BEGIN_MESSAGE_MAP(CWashLandscapeDlg, CDialog)
	//{{AFX_MSG_MAP(CWashLandscapeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// vytvo�en� akce pro Erozi krajiny

CPosAction* Action_ErozePoseidonArea(WashParams& Params,CPositionArea& Area,CPosEdMainDoc* pDocument)
{
	CPosHeightAction* pAction = NULL;
	TRY
	{
		pAction = new CPosHeightAction(IDA_HEIGHT_EROZE,Area);
		
		// vytvo�en� dat akce
		if (!pAction->CreateActionData(&(pDocument->m_PoseidonMap)))
			AfxThrowMemoryException();
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pAction != NULL)
		{
			delete pAction;
			pAction = NULL;
		}
	}
	END_CATCH_ALL

	if (pAction != NULL)
	{
		// provedu erozi
		pDocument->m_PoseidonMap.DoLandEroze(Params,Area);
		// na�tu data z mapy a vr�t�m j� p�vodn� hodnotu
		for(int z=pAction->m_LftTop.z; z>=pAction->m_RghBtm.z; z--)
		for(int x=pAction->m_LftTop.x; x<=pAction->m_RghBtm.x; x++)
		{
			LANDHEIGHT nHeight = pAction->GetUnitLandHeight(x, z);
			pAction->SetUnitLandHeight(x, z, pDocument->m_PoseidonMap.GetLandscapeHeight(x,z));
			pDocument->m_PoseidonMap.SetLandscapeHeight(x,z,nHeight,true);
		};
		// provedu akci
		pDocument->ProcessEditAction(pAction);
	}
	return pAction;
};

CPosAction* Action_ErozePoseidonLand(CRect& rLogArea,CPosEdMainDoc* pDocument)
{
	CPosEdEnvironment*	pEnvironment = GetPosEdEnvironment();
	
	CWashLandscapeDlg dlg;
	dlg.m_WashParams	 = pEnvironment->m_optPosEd.m_WashParams;	
	if (dlg.DoModal() != IDOK)
		return NULL;
	// uchov�m parametry
	pEnvironment->m_optPosEd.m_WashParams = dlg.m_WashParams;
	// provedu
	
  pDocument->m_CurrentArea.SetSelAreaType(CPosSelAreaObject::satVertex);    

  CPosAction* pAction = Action_ErozePoseidonArea(dlg.m_WashParams,*pDocument->m_CurrentArea.GetLogObjectPosition(),pDocument);
    
  if (pDocument->m_pObjsPanel->GetTypeObjects() != CPosObjectsPanel::grTypeLand)
    pDocument->m_CurrentArea.SetSelAreaType(CPosSelAreaObject::satSquare);

  return pAction;

};

/////////////////////////////////////////////////////////////////////////////
// Parametry pro zm�nu v��ky

#define LAND_TYPE_LAND	0
#define LAND_TYPE_SEA	1

#define HGMODE_MIN		0
#define HGMODE_MID		1
#define HGMODE_MAX		2
#define HGMODE_AVE		3
#define HGMODE_VAL		4
#define HGMODE_TRA		5
#define HGMODE_FOR		6

typedef struct
{
	int			nType;		// LAND_TYPE_
	int			nCount;		// po�et pol� v bloku

	double		nMinHg;		// min. a max. hodnota
	double		nMaxHg;
	double		nMidle;		// st�edn� hodnota
	double		nAvera;		// pr�m�rn� hodnota

	// nov� hodnoty
	int			nHgMode;
	double		nHeight;
	double		nHgFrom;
	double		nHgTo;

}	SLandHeightParams;

void DoInitLandHeightParams(SLandHeightParams& sLand,SLandHeightParams& sSea,CPosHeightAction* pAction,CPosEdMainDoc* pDocument)
{
	// typ krajiny
	sLand.nType = LAND_TYPE_LAND;
	sSea.nType  = LAND_TYPE_SEA;
	// inicializace hodnot pro v�po�et
	sLand.nCount	= sSea.nCount	= 0;
	sLand.nMinHg	= sSea.nMinHg	= MAX_LAND_HEIGHT;
	sLand.nMaxHg	= sSea.nMaxHg	= MIN_LAND_HEIGHT;
	sLand.nMidle	= sSea.nMidle	= 0.;
	sLand.nAvera	= sSea.nAvera	= 0.;

	// nov� hodnoty
	sLand.nHgMode	= sSea.nHgMode	= HGMODE_MIN;
	sLand.nHeight	= sSea.nHeight	= 0.;
	sLand.nHgFrom	= sSea.nHgFrom	= 0.;
	sLand.nHgTo		= sSea.nHgTo	= 0.;

	
	// na�ten� parametr�
	for(int z=pAction->m_LftTop.z; z>=pAction->m_RghBtm.z; z--)
	for(int x=pAction->m_LftTop.x; x<=pAction->m_RghBtm.x; x++)
	{
		LANDHEIGHT nHeight = pDocument->m_PoseidonMap.GetLandscapeHeight(x,z);
		if (nHeight < SEA_LAND_HEIGHT)
		{	// mo�e 
			sSea.nCount++;
			if (sSea.nMinHg > nHeight)
				sSea.nMinHg = nHeight;
			if (sSea.nMaxHg < nHeight)
				sSea.nMaxHg = nHeight;
			sSea.nAvera    += nHeight;
		} else
		{	// pevnina
			sLand.nCount++;
			if (sLand.nMinHg > nHeight)
				sLand.nMinHg = nHeight;
			if (sLand.nMaxHg < nHeight)
				sLand.nMaxHg = nHeight;
			sLand.nAvera    += nHeight;
		};
	};
	// v�po�et pr�m�r�
	if (sSea.nCount > 0)
		sSea.nAvera = sSea.nAvera / (double)(sSea.nCount);
	if (sLand.nCount > 0)
		sLand.nAvera = sLand.nAvera / (double)(sLand.nCount);
	// v�po�et st�edn�ch hodnot
	sSea.nMidle		= (sSea.nMinHg  + sSea.nMaxHg)/2.;
	sLand.nMidle	= (sLand.nMinHg + sLand.nMaxHg)/2.;
};

void DoCalcHeightValues(SLandHeightParams& sParams,CPosHeightAction* pAction,CPosEdMainDoc* pDocument)
{
	// na�ten� parametr�
	for(int z=pAction->m_LftTop.z; z>=pAction->m_RghBtm.z; z--)
	for(int x=pAction->m_LftTop.x; x<=pAction->m_RghBtm.x; x++)
	{
    UNITPOS upos;
    upos.x = x;
    upos.z = z;
    POINT pos = pDocument->m_PoseidonMap.FromUnitToViewPos(upos);
		if (pAction->m_Position.IsPointAtArea(pos))
		{
			LANDHEIGHT nHeight = pDocument->m_PoseidonMap.GetLandscapeHeight(x,z);
			if (((nHeight < SEA_LAND_HEIGHT)&&(sParams.nType == LAND_TYPE_SEA))||
				((nHeight >=SEA_LAND_HEIGHT)&&(sParams.nType == LAND_TYPE_LAND)))
			{	// m�n�m toto pole
				switch(sParams.nHgMode)
				{
				case HGMODE_MIN:
					nHeight = (float)(sParams.nMinHg);
					break;
				case HGMODE_MID:
					nHeight = (float)(sParams.nMidle);
					break;
				case HGMODE_MAX:
					nHeight = (float)(sParams.nMaxHg);
					break;
				case HGMODE_AVE:
					nHeight = (float)(sParams.nAvera);
					break;
				case HGMODE_VAL:
					nHeight = (float)(sParams.nHeight);
					break;
				case HGMODE_TRA:
					{
						if (sParams.nMinHg >= sParams.nMaxHg)
						{
							nHeight = (float)(sParams.nHgFrom);
						} else
						{
							double nPom = (nHeight-sParams.nMinHg)/(sParams.nMaxHg-sParams.nMinHg);
							double nCoe = nPom*(sParams.nHgTo - sParams.nHgFrom);
							nHeight = (float)(sParams.nHgFrom + nCoe);
						};
					};
					break;
				case HGMODE_FOR:
					ASSERT(FALSE);
					break;
				};
				pAction->SetUnitLandHeight(x, z, nHeight);
			}
		};
	};
};

/////////////////////////////////////////////////////////////////////////////
// Dialog pro zm�nu v��ky

class CChangeLandHeightDlg : public CDialog
{
// Construction
public:
	CChangeLandHeightDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	SLandHeightParams		m_sLand;
	SLandHeightParams		m_sSea;
	SLandHeightParams		m_sFinal;

	//{{AFX_DATA(CChangeLandHeightDlg)
	enum { IDD = IDD_HEIGHT_LAND_PARAMS };
	CComboBox	m_cLandType;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChangeLandHeightDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CChangeLandHeightDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeComboLandType();
	afx_msg void OnChangeEditHeight();
	afx_msg void OnChangeEditHeightFromTo();
	afx_msg void OnChangeEditHeightFormul();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CChangeLandHeightDlg::CChangeLandHeightDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CChangeLandHeightDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CChangeLandHeightDlg)
	//}}AFX_DATA_INIT
}

BOOL CChangeLandHeightDlg::OnInitDialog() 
{
	if (m_sSea.nCount > m_sLand.nCount)
		m_sFinal = m_sSea;
	else
		m_sFinal = m_sLand;

	CDialog::OnInitDialog();
	// po�ty pol�
	CString sNum;
	NumToLocalString(sNum,(LONG)(m_sLand.nCount+m_sSea.nCount));
	::SetWindowText(DLGCTRL(IDC_COUNTER_ALL),sNum);
	NumToLocalString(sNum,(LONG)(m_sLand.nCount));
	::SetWindowText(DLGCTRL(IDC_COUNTER_LAND),sNum);
	NumToLocalString(sNum,(LONG)(m_sSea.nCount));
	::SetWindowText(DLGCTRL(IDC_COUNTER_SEA),sNum);
	// selekce typu krajiny
	if (m_sSea.nCount > m_sLand.nCount)
		m_cLandType.SetCurSel(LAND_TYPE_SEA);
	else
		m_cLandType.SetCurSel(LAND_TYPE_LAND);
	// povolen� typu krajiny
	if ((m_sSea.nCount == 0)||(m_sLand.nCount == 0))
		m_cLandType.EnableWindow(FALSE);
	// aktu�ln� hodnoty
	OnSelchangeComboLandType();
	return TRUE;
}

void CChangeLandHeightDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChangeLandHeightDlg)
	DDX_Control(pDX, IDC_COMBO_LAND_TYPE, m_cLandType);
	//}}AFX_DATA_MAP
	DDX_Radio(pDX, IDC_RADIO1, m_sFinal.nHgMode);

	MDDX_Text(pDX, IDC_EDIT_HEIGHT, m_sFinal.nHeight);
	MDDV_MinMaxDouble(pDX, m_sFinal.nHeight, MIN_LAND_HEIGHT, MAX_LAND_HEIGHT, IDC_EDIT_HEIGHT, 1.);
	MDDX_Text(pDX, IDC_EDIT_HEIGHT_FROM, m_sFinal.nHgFrom);
	MDDV_MinMaxDouble(pDX, m_sFinal.nHgFrom, MIN_LAND_HEIGHT, MAX_LAND_HEIGHT, IDC_EDIT_HEIGHT_FROM, 1.);
	MDDX_Text(pDX, IDC_EDIT_HEIGHT_TO, m_sFinal.nHgTo);
	MDDV_MinMaxDouble(pDX, m_sFinal.nHgTo, MIN_LAND_HEIGHT, MAX_LAND_HEIGHT, IDC_EDIT_HEIGHT_TO, 1.);
}


BEGIN_MESSAGE_MAP(CChangeLandHeightDlg, CDialog)
	//{{AFX_MSG_MAP(CChangeLandHeightDlg)
	ON_CBN_SELCHANGE(IDC_COMBO_LAND_TYPE, OnSelchangeComboLandType)
	ON_EN_CHANGE(IDC_EDIT_HEIGHT, OnChangeEditHeight)
	ON_EN_CHANGE(IDC_EDIT_HEIGHT_FROM, OnChangeEditHeightFromTo)
	ON_EN_CHANGE(IDC_EDIT_HEIGHT_TO, OnChangeEditHeightFromTo)
	ON_EN_CHANGE(IDC_EDIT_HEIGHT_FORMUL, OnChangeEditHeightFormul)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CChangeLandHeightDlg::OnSelchangeComboLandType() 
{
	int nLandType = m_cLandType.GetCurSel();

	if (nLandType == LAND_TYPE_SEA)
		m_sFinal = m_sSea;
	else
		m_sFinal = m_sLand;
	// zobrazen� parametr�
	CString sNum;
	NumToLocalString(sNum,m_sFinal.nMinHg,3);
	::SetWindowText(DLGCTRL(IDC_SHOW_MIN),sNum);
	NumToLocalString(sNum,m_sFinal.nMidle,3);
	::SetWindowText(DLGCTRL(IDC_SHOW_MIDLE),sNum);
	NumToLocalString(sNum,m_sFinal.nMaxHg,3);
	::SetWindowText(DLGCTRL(IDC_SHOW_MAX),sNum);
	NumToLocalString(sNum,m_sFinal.nAvera,3);
	::SetWindowText(DLGCTRL(IDC_SHOW_AVERAGE),sNum);
}

void CChangeLandHeightDlg::OnChangeEditHeight() 
{
	CheckRadioButton(IDC_RADIO1,IDC_RADIO7,IDC_RADIO5);
}

void CChangeLandHeightDlg::OnChangeEditHeightFromTo() 
{
	CheckRadioButton(IDC_RADIO1,IDC_RADIO7,IDC_RADIO6);
}

void CChangeLandHeightDlg::OnChangeEditHeightFormul() 
{
	CheckRadioButton(IDC_RADIO1,IDC_RADIO7,IDC_RADIO7);
}

/////////////////////////////////////////////////////////////////////////////
// vytvo�en� akce pro zm�nu v��ky

CPosAction* Action_HeightPoseidonLand(const CPositionArea& Area,CPosEdMainDoc* pDocument)
{
	// p��prava akce
	CPosHeightAction* pAction = NULL;
  // do vertex selection
  pDocument->m_CurrentArea.SetSelAreaType(CPosSelAreaObject::satVertex);

	TRY
	{
	  pAction = new CPosHeightAction(IDA_HEIGHT_CHANGE,Area);
		// vytvo�en� dat akce
		if (!pAction->CreateActionData(&(pDocument->m_PoseidonMap)))
			AfxThrowMemoryException();
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pAction != NULL)
		{
			delete pAction;
			pAction = NULL;
		}
	}
	END_CATCH_ALL

	if (pAction != NULL)
	{
		// p��prava dat
		CPosEdEnvironment*		pEnvironment = GetPosEdEnvironment();
		CChangeLandHeightDlg	dlg;
		// inicializace struktur (podle bloku)
		DoInitLandHeightParams(dlg.m_sLand,dlg.m_sSea,pAction,pDocument);
		// inicializace parametr�

		// realizace okna
		if (dlg.DoModal() != IDOK)
		{
			delete pAction;
      if (pDocument->m_pObjsPanel->GetTypeObjects() != CPosObjectsPanel::grTypeLand)
        pDocument->m_CurrentArea.SetSelAreaType(CPosSelAreaObject::satSquare);
			return NULL;
		};
		// �schova parametr�

		// p�epo��t�n� v��ky
		DoCalcHeightValues(dlg.m_sFinal,pAction,pDocument);
		// provedu akci
		pDocument->ProcessEditAction(pAction);
	};
  
  if (pDocument->m_pObjsPanel->GetTypeObjects() != CPosObjectsPanel::grTypeLand)
    pDocument->m_CurrentArea.SetSelAreaType(CPosSelAreaObject::satSquare);

	return pAction;
};

///////////////////////////////////////////////////////////
// vytvo�en� akce pro zm�nu v��ky
void CreateAreaFromVertexes(CPositionArea& area, const AutoArray<float>& vertexes,CPosEdMainDoc* doc)
{
  FltRect rect;
  rect[0] = vertexes[0];
  rect[1] = vertexes[1];
  rect[2] = vertexes[0];
  rect[3] = vertexes[1];

  int n = vertexes.Size() / 3;
  for(int i = 1; i < n; i++)
  {
    saturateMin(rect[0], vertexes[3 * i]);
    saturateMin(rect[1], vertexes[3 * i + 1]);
    saturateMax(rect[2], vertexes[3 * i]);
    saturateMax(rect[3], vertexes[3 * i + 1]);
  }

  CRect r = doc->m_PoseidonMap.FromRealToViewRect(rect);
  r.InflateRect(VIEW_LOG_UNIT_SIZE/2,VIEW_LOG_UNIT_SIZE/2);

  area.AddRect(r);
}

CPosAction* Action_HeightPoseidonLand(const AutoArray<float>& vertexes,CPosEdMainDoc* doc)
{
  if (vertexes.Size() == 0)
    return NULL;

  // Create simple area
  CPositionArea area;
  CreateAreaFromVertexes( area, vertexes, doc);

  CPosHeightAction* action = new CPosHeightAction(IDA_HEIGHT_CHANGE,area);

  // vytvo�en� dat akce
  if (!action->CreateActionData(&(doc->m_PoseidonMap)))
  {
    delete action;
    return NULL;
  }

  // set up moved vertexes
  int n = vertexes.Size() / 3;
  for(int i = 0; i < n; i++)
  {
    REALPOS realPos;
    realPos.x = vertexes[3*i];
    realPos.z = vertexes[3*i+1];

    UNITPOS unitPos = doc->m_PoseidonMap.FromRealToUnitPos(realPos);
    action->SetUnitLandHeight(unitPos.x, unitPos.z, vertexes[3*i+2]);
  }
  
  doc->ProcessEditAction(action);

  return action;
}

///////////////////////////////////////////////////////////
// Clipboard actions
CPosAction*  Action_PasteClipboardHeight(PosClipBoardHeight * clipData,CPosEdMainDoc* pDocument,const CPoint& cursorLog, CPosActionGroup* pToGroup)
{
  // p��prava akce  
  REALPOS realpos;
  realpos.x = clipData->rectreal[0];
  realpos.z = clipData->rectreal[1];
  CPoint leftbottom = pDocument->m_PoseidonMap.FromRealToViewPos(realpos);  
  realpos.x = clipData->rectreal[2];
  realpos.z = clipData->rectreal[3];
  CPoint righttop = pDocument->m_PoseidonMap.FromRealToViewPos(realpos);
  CPoint diff(cursorLog.x - leftbottom.x, cursorLog.y - righttop.y);

  CPositionArea area;
  area.AddRect(CRect(cursorLog.x - VIEW_LOG_UNIT_SIZE, cursorLog.y  - VIEW_LOG_UNIT_SIZE, righttop.x + diff.x + VIEW_LOG_UNIT_SIZE , leftbottom.y + diff.y + VIEW_LOG_UNIT_SIZE));

  CPosHeightAction* pAction = NULL;
  TRY
  {
    pAction = new CPosHeightAction(IDA_HEIGHT_CHANGE,area);
      
    // vytvo�en� dat akce
    if (!pAction->CreateActionData(&(pDocument->m_PoseidonMap)))
      AfxThrowMemoryException();
  }
  CATCH_ALL(e)
  {
    MntReportMemoryException();
    if (pAction != NULL)
    {
      delete pAction;
      pAction = NULL;
    }
  }
  END_CATCH_ALL;

  // add data to action
  REALPOS realDiff = pDocument->m_PoseidonMap.FromViewToRealPos(cursorLog);
  //realDiff.x -= clipData->rectreal[0];
  realDiff.z -= clipData->rectreal[3] - clipData->rectreal[1];

  for(int i=0; i<clipData->numberOfElems; i++)
  {
    const HeightElem & elem = clipData->data[i];
    realpos.x = elem.relrealPos[0] + realDiff.x + pDocument->m_nRealSizeUnit;
    realpos.z = elem.relrealPos[1] + realDiff.z;

    UNITPOS unitPos = pDocument->m_PoseidonMap.FromRealToUnitPos(realpos);
    pAction->SetUnitLandHeight(unitPos.x, unitPos.z, elem.height);
  }

  if (pToGroup)
    pToGroup->AddAction(pAction);
  else
    pDocument->ProcessEditAction(pAction);

  return pAction;
}

CPosAction*  Action_PasteAbsolutClipboardHeight(PosClipBoardHeight * clipData,CPosEdMainDoc* pDocument,CPosActionGroup* pToGroup)
{
  // p��prava akce  
  REALPOS realpos;
  realpos.x = clipData->rectreal[0];
  realpos.z = clipData->rectreal[1];
  CPoint leftbottom = pDocument->m_PoseidonMap.FromRealToViewPos(realpos);  
  realpos.x = clipData->rectreal[2];
  realpos.z = clipData->rectreal[3];
  CPoint righttop = pDocument->m_PoseidonMap.FromRealToViewPos(realpos);

  CPositionArea area;
  area.AddRect(CRect(leftbottom.x - VIEW_LOG_UNIT_SIZE, righttop.y  - VIEW_LOG_UNIT_SIZE, righttop.x + VIEW_LOG_UNIT_SIZE , leftbottom.y + VIEW_LOG_UNIT_SIZE));

  CPosHeightAction* pAction = NULL;
  TRY
  {
    pAction = new CPosHeightAction(IDA_HEIGHT_CHANGE,area);

    // vytvo�en� dat akce
    if (!pAction->CreateActionData(&(pDocument->m_PoseidonMap)))
      AfxThrowMemoryException();
  }
  CATCH_ALL(e)
  {
    MntReportMemoryException();
    if (pAction != NULL)
    {
      delete pAction;
      pAction = NULL;
    }
  }
  END_CATCH_ALL;

  // add data to action
  for(int i=0; i<clipData->numberOfElems; i++)
  {
    const HeightElem & elem = clipData->data[i];
    realpos.x = elem.relrealPos[0] + clipData->rectreal[0];
    realpos.z = elem.relrealPos[1] + clipData->rectreal[1];

    UNITPOS unitPos = pDocument->m_PoseidonMap.FromRealToUnitPos(realpos);
    pAction->SetUnitLandHeight(unitPos.x, unitPos.z, elem.height);
  }

  if (pToGroup)
    pToGroup->AddAction(pAction);
  else
    pDocument->ProcessEditAction(pAction);

  return pAction;
}

CPosAction*  Action_LockHeight(const CPositionArea& Area,bool lock, CPosEdMainDoc& doc,CPosActionGroup* toGroup)
{
  CPosHeightLockAction* action = new CPosHeightLockAction(IDA_HEIGHT_LOCK_CHANGE,Area);
	 
  // vytvo�en� dat akce
  if (!action || !action->CreateActionData(doc.m_PoseidonMap, lock))			
  {
    delete action;
    return NULL;
  }
  if (toGroup)
    toGroup->AddAction(action);
  else
    doc.ProcessEditAction(action);

  return action;
}


