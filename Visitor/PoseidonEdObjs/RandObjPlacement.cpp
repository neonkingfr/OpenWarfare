#include "stdafx.h"
#include "resource.h"
#include <float.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

////////////////////////////////////////////
// Random objects placement

#define PLACER_FILE_VER 1
#define PLACER_FILE_HEAD "PLACER"
#define PLACER_FILE_HEAD_LEN 6


//-----------------------------------------------------------
// CObjectsListBox
//-----------------------------------------------------------

class /*AFX_EXT_CLASS*/ CObjectsListBox : public CFMntListBox
{
public:
  CObjectsListBox();

  virtual int	 GetItemImageWidth(LPDRAWITEMSTRUCT lpDIS) {return 0;};
  virtual void	OnDrawItemImage(CRect&, LPDRAWITEMSTRUCT lpDIS) {};
  virtual void	OnDrawItemText(CRect&, LPDRAWITEMSTRUCT lpDIS);
  virtual BOOL	GetItemColor(COLORREF&, LPDRAWITEMSTRUCT lpDIS);
};

CObjectsListBox::CObjectsListBox()
{
  Init(MNTLST_COLOR);
};

void CObjectsListBox::OnDrawItemText(CRect& rPos, LPDRAWITEMSTRUCT lpDIS)
{
  CPosObjectTemplate* pObj = (CPosObjectTemplate*)(lpDIS->itemData);
  if (pObj != NULL)
  {
    ASSERT_KINDOF(CPosObjectTemplate,pObj);
    CDC dc;
    dc.Attach(lpDIS->hDC);
    pObj->DrawLBItem(&dc,rPos);
    dc.Detach();
  } 
};

BOOL CObjectsListBox::GetItemColor(COLORREF& cColor, LPDRAWITEMSTRUCT lpDIS)
{
  CPosObjectTemplate* pObj = (CPosObjectTemplate*)(lpDIS->itemData);
  if (pObj != NULL)
  {
    ASSERT_KINDOF(CPosObjectTemplate,pObj);
    cColor = pObj->GetLBItemColor();
  }
  return TRUE;
};

//-----------------------------------------------------------
// CUsedObjsListCtrl
//-----------------------------------------------------------

class CUsedObjsListCtrl : public CMntEditableListCtrl
{
protected:
  bool CanEditSubItem(int iItem, int iSubItem) {return iSubItem == 1;};
  /*CEdit * ConstructInPlaceEdit(int iItem, int iSubItem, CString sInitText) 
  {return new CInPlaceEditFloat(iItem, iSubItem, sInitText);};*/
public:
  DECLARE_MESSAGE_MAP()
  afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
};

BEGIN_MESSAGE_MAP(CUsedObjsListCtrl, CMntEditableListCtrl)
  ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()

void CUsedObjsListCtrl::OnLButtonDblClk(UINT nFlags, CPoint point)
{
  int iSubItem;
  int iItem = HitTestEx(point, iSubItem);

  if (iItem == -1)
    return;

  if (iSubItem != 2)
    return CMntEditableListCtrl::OnLButtonDblClk(nFlags, point);

  // change value Collision free
  CString str = GetItemText( iItem, 2);

  LVITEM item;
  item.mask = LVIF_TEXT;
  item.iItem = iItem;
  item.iSubItem = 2;  
  if (str == _T("Y"))
    item.pszText = _T("N");
  else
    item.pszText = _T("Y");

  SetItem(&item);
}

//-----------------------------------------------------------
// RandomObjectTemplate
//-----------------------------------------------------------

struct RandomObjectTemplate
{
  CPosObjectTemplate * _template;
  float _density;
  bool _collide;
};

TypeIsSimple(RandomObjectTemplate);

struct ObjectRandomPlacerConfig
{
  CString _name;
  AutoArray<RandomObjectTemplate> _RandomTemplates;
};

TypeIsMovable(ObjectRandomPlacerConfig);

//-----------------------------------------------------------
// RandomObjectTemplate
//-----------------------------------------------------------

class CObjectRandomPlacementDlg : public CDialog
{
  // Construction
public:
  CObjectRandomPlacementDlg(CPosEdMainDoc & doc, ObjectRandomPlacerConfig & config, bool newConfig = true, CWnd* pParent = NULL);   // standard constructor

  // Dialog Data 
  enum { IDD = IDD_OBJECT_RANDOM_PLACEMENT };

protected:
  CUsedObjsListCtrl m_UsedObjs;
  CObjectsListBox m_AviableObjs;

  CPosEdMainDoc & m_Document;
  ObjectRandomPlacerConfig&  m_RandomConfig;
  bool m_New;
  //AutoArray<RandomObjectTemplate> m_RandomTemplates;

protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual BOOL OnInitDialog();

public:
  //const ObjectRandomPlacerConfig& GetData() const {return m_RandomConfig;};
  //void DoSerializeUsedObjs(bool save);

  DECLARE_MESSAGE_MAP()
  afx_msg void OnBnClickedRandomAdd();
  afx_msg void OnBnClickedRandomRem();
  afx_msg void OnBnClickedOk();
};

CObjectRandomPlacementDlg::CObjectRandomPlacementDlg(CPosEdMainDoc & doc, ObjectRandomPlacerConfig & config, bool newConfig /*= true*/, CWnd* pParent/* = NULL*/) :
CDialog(CObjectRandomPlacementDlg::IDD, pParent), m_Document(doc), m_RandomConfig(config), m_New(newConfig)
{
}

BOOL CObjectRandomPlacementDlg::OnInitDialog()
{
  //DoSerializeUsedObjs(false);

  UpdateData(FALSE);

  // set up object templates
  for(int i=0; i<m_Document.ObjectTemplatesSize(); i++)
  {
    CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(m_Document.GetIthObjectTemplate(i));
    if (pTempl && (pTempl->m_nObjType == CPosObjectTemplate::objTpNatural || pTempl->m_nObjType == CPosObjectTemplate::objTpPeople))
      LBAddString(m_AviableObjs.m_hWnd, pTempl->GetName(), (DWORD)pTempl);          
  }

  if (m_AviableObjs.GetCount()>0)
    m_AviableObjs.SetCurSel(0);

  // 
  LVCOLUMN  column;
  column.mask = LVCF_FMT | LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH;
  column.fmt = LVCFMT_LEFT;
  column.iSubItem = 0;
  column.pszText = _T("Template");
  column.cx = 100;

  m_UsedObjs.InsertColumn(0, &column);

  column.fmt = LVCFMT_LEFT;
  column.iSubItem = 1;
  column.pszText = _T("Density (items per rect)");
  column.cx = 50;

  m_UsedObjs.InsertColumn(1, &column);

  column.fmt = LVCFMT_CENTER;
  column.iSubItem = 2;
  column.pszText = _T("Collision free");
  column.cx = 50;

  m_UsedObjs.InsertColumn(2, &column);

  // add values from 
  for(int i = 0; i < m_RandomConfig._RandomTemplates.Size(); i++)
  {
    CPosObjectTemplate * templ = m_RandomConfig._RandomTemplates[i]._template;

    LVITEM item;
    item.mask = LVIF_TEXT;
    item.iItem = m_UsedObjs.GetItemCount();
    item.iSubItem = 0;    
    item.pszText = const_cast<LPTSTR>((LPCTSTR) templ->GetName());
    m_UsedObjs.InsertItem(&item);
    templ->GetName();

    item.iSubItem = 1;
    CString text;
    text.Format("%.2f", m_RandomConfig._RandomTemplates[i]._density);
    item.pszText = text.GetBuffer();
    m_UsedObjs.SetItem(&item);  
    text.ReleaseBuffer();

    item.iSubItem = 2; 
    if (m_RandomConfig._RandomTemplates[i]._collide)
      item.pszText = _T("Y");
    else
      item.pszText = _T("N");

    m_UsedObjs.SetItem(&item);      

    m_UsedObjs.SetItemData(item.iItem, (DWORD) templ);
  }
  
  UpdateData(FALSE);

  return TRUE;
}

void CObjectRandomPlacementDlg::DoDataExchange(CDataExchange* pDX)
{
  DDX_Control(pDX, IDC_AVIABLE_OBJS, m_AviableObjs);
  DDX_Control(pDX, IDC_USED_OBJS, m_UsedObjs);
  DDX_Text(pDX, IDC_RANDOM_CONFIG_NAME, m_RandomConfig._name);
  MDDV_MinChars(pDX, m_RandomConfig._name, 1);

  /*
  if (pDX->m_bSaveAndValidate)
  {
    for(int i = 0; i < m_UsedObjs.GetItemCount(); i++)
    {
      CString densityStr = m_UsedObjs.GetItemText( i, 1);
      float density;
#ifdef Fail
#undef Fail
#endif
      if (0 ==_stscanf(densityStr, _T("%f"), &density))
        AfxMessageBox(IDS_NOT_NUMBER);
        pDX->Fail();
      return;
    }
  }
  */
}

BEGIN_MESSAGE_MAP(CObjectRandomPlacementDlg, CDialog)
  ON_BN_CLICKED(IDC_RANDOM_ADD, OnBnClickedRandomAdd)
  ON_BN_CLICKED(IDC_RANDOM_REM, OnBnClickedRandomRem)
  ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()

void CObjectRandomPlacementDlg::OnBnClickedRandomAdd()
{
  DWORD ItmData;

  if (LBGetCurSelData(m_AviableObjs.m_hWnd, ItmData))
  {
    //RandomObjectTemplate * templ = new RandomObjectTemplate;
    //templ->_template = (CPosObjectTemplate *) ItmData;
    //templ->_density = 20.0f;
    CPosObjectTemplate * templ = (CPosObjectTemplate *) ItmData;

    LVITEM item;
    item.mask = LVIF_TEXT;
    item.iItem = m_UsedObjs.GetItemCount();
    item.iSubItem = 0;
    item.pszText = const_cast<LPTSTR>((LPCTSTR)templ->GetName());
    m_UsedObjs.InsertItem(&item);    

    item.iSubItem = 1;
    CString text;
    text.Format("%.2f", 10.0f);
    item.pszText = text.GetBuffer();
    m_UsedObjs.SetItem(&item);  
    text.ReleaseBuffer();

    item.iSubItem = 2;    
    item.pszText = _T("Y");
    m_UsedObjs.SetItem(&item);      

    m_UsedObjs.SetItemData(item.iItem, (DWORD) ItmData);    
  }
}

void CObjectRandomPlacementDlg::OnBnClickedRandomRem()
{
  POSITION pos = m_UsedObjs.GetFirstSelectedItemPosition();
  if (pos == NULL)
    return;
  else
  {
    while (pos)
    {
      int nItem = m_UsedObjs.GetNextSelectedItem(pos);
      m_UsedObjs.DeleteItem(nItem);
    }
  }
}

void CObjectRandomPlacementDlg::OnBnClickedOk()
{
  m_RandomConfig._RandomTemplates.Resize(0);
  m_RandomConfig._RandomTemplates.Reserve(m_UsedObjs.GetItemCount(),m_UsedObjs.GetItemCount());
  for(int i = 0; i < m_UsedObjs.GetItemCount(); i++)
  {
    RandomObjectTemplate& templ =  m_RandomConfig._RandomTemplates.Append();
    templ._template = (CPosObjectTemplate *)  m_UsedObjs.GetItemData(i);
    CString densityStr = m_UsedObjs.GetItemText( i, 1);
    _stscanf(densityStr, _T("%f"), &templ._density);
    CString collide = m_UsedObjs.GetItemText( i, 2);
    if (collide == _T("Y"))
      templ._collide = true;
    else
      templ._collide = false;
  }

  //DoSerializeUsedObjs(true);
  OnOK();
}
  
/*void CObjectRandomPlacementDlg::DoSerializeUsedObjs(bool save)
{
  if (!save)
  {
    m_RandomTemplates.Clear();

    CFile file; 
    if (file.Open(m_Document.GetPathName(), CFile::modeRead))
    {
      char fname[7];
      file.Read(fname,7);
      // verze
      int nVer = (fname[5] - '0')*10 + (fname[6] - '0');
      if ((nVer < 10)||(nVer > POSED_FILE_VER))
      {	// chybn� verze
        MntInvalidFileFormat();
        return;
      }

      if (nVer <= 37)
      {
        // no data
        return;
      }

      ULONGLONG dataPos;
      file.Read(&dataPos, sizeof(dataPos));
      file.Seek(dataPos, CFile::begin);

      ULONGLONG endPos = file.GetLength();
      if (endPos - dataPos < sizeof(int))
      { 
        //nodata;
        file.Close();
        return;
      }

      CArchive ar(&file, CArchive::load);

      int n = 0; 
      MntSerializeInt(ar, n);
      for(int i = 0; i < n; i++)
      {        
        int ID; 
        MntSerializeInt(ar, ID);
        Ref<CPosObjectTemplate> objTempl = m_Document.GetObjectTemplate(ID);
        if (objTempl.NotNull())
        {
          RandomObjectTemplate& templ = m_RandomTemplates.Append();
          templ._template = objTempl;
          MntSerializeFloat(ar, templ._density);
          BOOL coll; 
          MntSerializeBOOL(ar, coll);
          templ._collide = coll; 
        }
        else
        {
          // just read and ignore values
          float dent;
          MntSerializeFloat(ar, dent);
          BOOL coll; 
          MntSerializeBOOL(ar, coll);
        }
      }
      ar.Close();
      file.Close();
    }
  }
  else
  {
    CFile file; 
    if (file.Open(m_Document.GetPathName(), CFile::modeRead))
    {
      char fname[7];
      file.Read(fname,7);
      // verze
      int nVer = (fname[5] - '0')*10 + (fname[6] - '0');
      if ((nVer < 10)||(nVer > POSED_FILE_VER))
      {	// chybn� verze
        MntInvalidFileFormat();
        return;
      }

      if (nVer <= 37)
      {
        // Old file format cannot save data
        AfxMessageBox(IDS_OLD_FILEFORMAT_CONF);
        return;
      }

      ULONGLONG dataPos;
      file.Read(&dataPos, sizeof(dataPos));      
      ULONGLONG endPos = file.GetLength();

      file.Close();

      CFMntFromToFile fromToFile;

      if (fromToFile.Open(m_Document.GetPathName(), CFile::modeWrite | CFile::modeCreate))
      {
        RptF("CObjectRandomPlacementDlg::DoSerializeUsedObjs: Open of FromTo file (%s ) failed.",m_Document.GetPathName());
        return;
      }

      fromToFile.StartFromToSession(dataPos, endPos, m_Document.GetPathName());

      CArchive ar(&fromToFile, CArchive::store);
      int n = m_RandomTemplates.Size();
      MntSerializeInt(ar, n);
      for(int i = 0; i < n; i++)
      {        
        
        MntSerializeInt(ar, m_RandomTemplates[i]._template->m_ID);        
        MntSerializeFloat(ar, m_RandomTemplates[i]._density);
        BOOL coll = m_RandomTemplates[i]._collide; 
        MntSerializeBOOL(ar, coll);        
      }
      ar.Close();
      fromToFile.EndFromToSession(endPos);
      fromToFile.Close();
    }
    else    
      RptF("CObjectRandomPlacementDlg::DoSerializeUsedObjs: Load of document file (%s ) failed.",m_Document.GetPathName());     
  }
}*/

/////////////////////////////////////
// 

class CObjectRandomPlacerConfigDlg : public CDialog
{
protected:  
  CListBox m_AviableConfigs;

  CPosEdMainDoc & m_Document;
  //AutoArray<ObjectRandomPlacerConfig> m_RandomConfigs;
  ObjectRandomPlacerConfig m_CurConfig;

public:
  CObjectRandomPlacerConfigDlg(CPosEdMainDoc & doc, CWnd* pParent = NULL);   // standard constructor

  // Dialog Data 
  enum { IDD = IDD_OBJECT_RANDOM_PLACER};

  ObjectRandomPlacerConfig& GetCurConfig() {return m_CurConfig;};
  
protected: 
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual BOOL OnInitDialog();
  void DeleteConfigs();

  void DoSerialize(bool save = true);

  DECLARE_MESSAGE_MAP()
  afx_msg void OnBnClickedRandomAdd();
  afx_msg void OnBnClickedRandomRem();
  afx_msg void OnBnClickedRandomEdit();
  afx_msg void OnBnClickedOk();
  virtual void OnCancel();
};

BEGIN_MESSAGE_MAP(CObjectRandomPlacerConfigDlg, CDialog)
  ON_BN_CLICKED(IDC_OBJ_RANDOM_PLACER_ADD, OnBnClickedRandomAdd)
  ON_BN_CLICKED(IDC_OBJ_RANDOM_PLACER_EDIT, OnBnClickedRandomEdit)
  ON_BN_CLICKED(IDC_OBJ_RANDOM_PLACER_REM, OnBnClickedRandomRem)
  //ON_BN_CLICKED(IDC_OBJ_RANDOM_PLACER_EDIT, OnBnClickedRandomRem)
  ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()

CObjectRandomPlacerConfigDlg::CObjectRandomPlacerConfigDlg(CPosEdMainDoc & doc, CWnd* pParent) :
CDialog(IDD, pParent), m_Document(doc)
{
}

void CObjectRandomPlacerConfigDlg::DoDataExchange(CDataExchange* pDX)
{
  DDX_Control(pDX, IDC_OBJ_RANDOM_PLACER_LIST, m_AviableConfigs);
}

void CObjectRandomPlacerConfigDlg::OnBnClickedRandomAdd()
{
  ObjectRandomPlacerConfig * config = new ObjectRandomPlacerConfig();

  CObjectRandomPlacementDlg dlg(m_Document, *config, true);

  if (dlg.DoModal() != IDOK)
  {
    delete config;
    return;
  }

  int item = m_AviableConfigs.AddString(config->_name);  
  if (item == LB_ERR  || item == LB_ERRSPACE)
  {
    delete config;
    return;
  }

  m_AviableConfigs.SetItemDataPtr(item, (void *) config);
  m_AviableConfigs.SetCurSel(item);
}

void CObjectRandomPlacerConfigDlg::OnBnClickedRandomEdit()
{
  int item = m_AviableConfigs.GetCurSel();
  if (item == LB_ERR  || item == LB_ERRSPACE)   
    return;

  ObjectRandomPlacerConfig * configOld = (ObjectRandomPlacerConfig *) m_AviableConfigs.GetItemDataPtr(item);
  if (!configOld)
  {
    ASSERT(configOld != NULL);
    return;
  }
  ObjectRandomPlacerConfig * config = new ObjectRandomPlacerConfig();
  *config = *configOld;

  CObjectRandomPlacementDlg dlg(m_Document, *config, false);

  if (dlg.DoModal() != IDOK)
  {
    delete config;
    return;
  }

  if (config->_name == configOld->_name)
    *configOld = *config;
  else
  {
    *configOld = *config;
    m_AviableConfigs.DeleteString(item);
    item = m_AviableConfigs.AddString(configOld->_name);
    if (item == LB_ERR  || item == LB_ERRSPACE)       
      delete configOld;    
    else
      m_AviableConfigs.SetItemDataPtr(item, (void *) configOld);
  }

  delete config;

  m_AviableConfigs.SetCurSel(item);
}

void CObjectRandomPlacerConfigDlg::OnBnClickedRandomRem()
{
  int item = m_AviableConfigs.GetCurSel();  
  if (item == LB_ERR  || item == LB_ERRSPACE)   
    return;

  delete m_AviableConfigs.GetItemDataPtr(item);
  m_AviableConfigs.DeleteString(item);
}

void CObjectRandomPlacerConfigDlg::OnBnClickedOk()
{
  if (0 == m_AviableConfigs.GetCount())
  {
    DoSerialize();
    OnCancel();
    return;
  }

  int item = m_AviableConfigs.GetCurSel();  
  if (item == LB_ERR  || item == LB_ERRSPACE)   
  {
    OnCancel();
    return;
  }

  ObjectRandomPlacerConfig * configOld = (ObjectRandomPlacerConfig *) m_AviableConfigs.GetItemDataPtr(item);
  if (!configOld)
  {
    OnCancel();
    return;
  }

  m_CurConfig = *configOld;

  DoSerialize();

  DeleteConfigs();
  OnOK();  
}

BOOL CObjectRandomPlacerConfigDlg::OnInitDialog()
{
  UpdateData(FALSE);
  DoSerialize(false);
  if (m_AviableConfigs.GetCount() > 0)
    m_AviableConfigs.SetCurSel(0);

  return TRUE;
}

void CObjectRandomPlacerConfigDlg::DeleteConfigs()
{
  for(int i = 0; i < m_AviableConfigs.GetCount(); i++)  
    delete (ObjectRandomPlacerConfig *) m_AviableConfigs.GetItemDataPtr(i);  

  m_AviableConfigs.ResetContent();
}

void CObjectRandomPlacerConfigDlg::OnCancel()
{
  m_AviableConfigs.ResetContent();
  CDialog::OnCancel();
}


void CObjectRandomPlacerConfigDlg::DoSerialize(bool save)
{
  if (!save)
  {   
    CFile file; 
    if (file.Open(m_Document.GetPathName(), CFile::modeRead))
    {
      TRY
      {
        char fname[7];
        file.Read(fname,7);
        // verze
        int nVer = (fname[5] - '0')*10 + (fname[6] - '0');
        if ((nVer < 10)||(nVer > POSED_FILE_VER))
        {	// chybn� verze
          MntInvalidFileFormat();
          return;
        }

        if (nVer <= 38)
        {
          // no data
          file.Close();
          return;
        }

        ULONGLONG dataPos;
        file.Read(&dataPos, sizeof(dataPos));
        file.Seek(dataPos, CFile::begin);

        ULONGLONG endPos = file.GetLength();
        if (endPos - dataPos < sizeof(int))
        { 
          //nodata;
          file.Close();
          return;
        }

        CArchive ar(&file, CArchive::load);
        
        char head[PLACER_FILE_HEAD_LEN + 2];        
        ar.Read(head,PLACER_FILE_HEAD_LEN + 2);

        int nPlacerVer = (head[PLACER_FILE_HEAD_LEN] - '0')*10 + (head[PLACER_FILE_HEAD_LEN + 1] - '0');
        if (strncmp(head, PLACER_FILE_HEAD,  PLACER_FILE_HEAD_LEN))
        {	// chybn� verze
          RptF("CObjectRandomPlacerConfigDlg::DoSerialize : wrong random placer header.");
          ar.Close();
          file.Close();
          return;
        }


        int n = 0; 
        MntSerializeInt(ar, n);
        for(int i = 0; i < n; i++)
        {    
          ObjectRandomPlacerConfig * config = new ObjectRandomPlacerConfig();
          MntSerializeString(ar, config->_name);

          int nn = 0; 
          MntSerializeInt(ar, nn);
          for(int j = 0; j < nn; j++)
          {
            int ID; 
            MntSerializeInt(ar, ID);
            Ref<CPosObjectTemplate> objTempl = m_Document.GetObjectTemplate(ID);
            if (objTempl.NotNull())
            {
              RandomObjectTemplate& templ = config->_RandomTemplates.Append();
              templ._template = objTempl;
              MntSerializeFloat(ar, templ._density);
              BOOL coll; 
              MntSerializeBOOL(ar, coll);
              templ._collide = (coll == TRUE); 
            }
            else
            {
              // just read and ignore values
              float dent;
              MntSerializeFloat(ar, dent);
              BOOL coll; 
              MntSerializeBOOL(ar, coll);
            }
          }

          int item = m_AviableConfigs.AddString(config->_name);
          if (item == LB_ERR && item == LB_ERRSPACE)
            delete config;
          else
            m_AviableConfigs.SetItemDataPtr(item, (void *) config);

        }

        ar.Close();
        file.Close();        
      }
      CATCH_ALL(e)
        file.Close();     
      END_CATCH_ALL
    }
  }
  else
  {
    CFile file; 
    if (file.Open(m_Document.GetPathName(), CFile::modeRead))
    {
      TRY
      {      
        char fname[7];
        file.Read(fname,7);
        // verze
        int nVer = (fname[5] - '0')*10 + (fname[6] - '0');
        if ((nVer < 10)||(nVer > POSED_FILE_VER))
        {	// chybn� verze
          MntInvalidFileFormat();
          return;
        }

        if (nVer <= 38)
        {
          // Old file format cannot save data
          AfxMessageBox(IDS_OLD_FILEFORMAT_CONF);
          file.Close();
          return;
        }

        ULONGLONG dataPos;
        file.Read(&dataPos, sizeof(dataPos));      
        ULONGLONG endPos = file.GetLength();


        file.Close();

        CFMntFromToFile fromToFile;

        TRY
        {
          if (!fromToFile.Open(m_Document.GetPathName(), CFile::modeWrite | CFile::modeCreate))
          {
            RptF("CObjectRandomPlacerConfigDlg::DoSerialize: Open of FromTo file (%s ) failed.",m_Document.GetPathName());
            return;
          }

          fromToFile.StartFromToSession(dataPos, endPos, m_Document.GetPathName());

          CArchive ar(&fromToFile, CArchive::store);
          
          char head[PLACER_FILE_HEAD_LEN + 2];
          strcpy(head,PLACER_FILE_HEAD);
          ASSERT(PLACER_FILE_VER <= 99);
          head[PLACER_FILE_HEAD_LEN] = '0' + (PLACER_FILE_VER / 10);
          head[PLACER_FILE_HEAD_LEN + 1] = '0' + (PLACER_FILE_VER % 10);
          ar.Write(head,PLACER_FILE_HEAD_LEN + 2);

          int n = m_AviableConfigs.GetCount();
          MntSerializeInt(ar, n);
          for(int i = 0; i < n; i++)
          {        
            ObjectRandomPlacerConfig * config = (ObjectRandomPlacerConfig *) m_AviableConfigs.GetItemDataPtr(i);
            if (!config)
              continue;

            MntSerializeString(ar, config->_name);
            int nn = config->_RandomTemplates.Size();
            MntSerializeInt(ar, nn);

            for(int j = 0; j < nn; j++)
            {
              MntSerializeInt(ar,config->_RandomTemplates[j]._template->m_ID);        
              MntSerializeFloat(ar, config->_RandomTemplates[j]._density);
              BOOL coll = config->_RandomTemplates[j]._collide; 
              MntSerializeBOOL(ar, coll);        
            }       
          }

          ar.Close();
          fromToFile.EndFromToSession(endPos);
        }
        CATCH_ALL(e) END_CATCH_ALL;

        fromToFile.Close();
      }
      CATCH_ALL(e);
      file.Close();
      END_CATCH_ALL;

    }
    else    
      RptF("CObjectRandomPlacerConfigDlg::DoSerialize: Load of document file (%s ) failed.",m_Document.GetPathName());     
  }
}




//////////////////////////////////////////////
// PlaceObjectsRandomaly

//TypeIsSimple(CPosEdObject *);

class CRandomObjectPlacer
{
protected:
  CPosEdMainDoc& _doc;
  CPosSelAreaObject * _area;
  CPosObjectGroupAction * _pGrpAction;
  const AutoArray<RandomObjectTemplate> * _data;
  AutoArray<CPosEdObject *> _objs;
  AutoArray<CPosEdObject *> _collideObjs;
  AutoArray<CPosEdObject *> _collideObjsTemp;

public:
  CRandomObjectPlacer(CPosEdMainDoc & doc): _doc(doc) {};
  void operator()(CPosObjectGroupAction * pGrpAction, CPosSelAreaObject & area, const AutoArray<RandomObjectTemplate>& data);

protected:
  void CollectPosEdObjsInterfereIntoArea(const CRect& logArea);
  void CollectCollideObjsInterfereIntoArea(const CRect& logArea);
  void PlaceObjectsRandomalyEx(const CRect& logArea);
};

void CRandomObjectPlacer::CollectCollideObjsInterfereIntoArea(const CRect& logArea)
{  
  CRect hull;
  for(int i=0; i < _collideObjs.Size();i++)
  {
    CPosEdObject* pObject = _collideObjs[i];

    if (pObject)      
    {			
      pObject->GetObjectHull(hull);
      if (IsIntersectRect(hull, logArea))
        _collideObjsTemp.Add(pObject);
    };
  };
};

void CRandomObjectPlacer::CollectPosEdObjsInterfereIntoArea(const CRect& logArea)
{
  CPoseidonMap& map = _doc.m_PoseidonMap;
  CRect hull;
  for(int i=0; i < map.GetPoseidonObjectCount();i++)
  {
    CPosEdObject* pObject = map.GetPoseidonObject(i);

    if (pObject && pObject->m_nMasterWood < 0 && pObject->m_nMasterNet < 0)      
    {			
      pObject->GetObjectHull(hull);
      if (IsIntersectRect(hull, logArea))
        _collideObjsTemp.Add(pObject);
    };
  };
}

void CRandomObjectPlacer::operator()(CPosObjectGroupAction * pGrpAction, CPosSelAreaObject & area, const AutoArray<RandomObjectTemplate>& data)
{
  _pGrpAction = pGrpAction;
  _area = &area;
  _data = &data;
  
  CRect logHull;
  area.GetObjectHull(logHull);

  int height = logHull.Height();
  int width = logHull.Width();
  
  // calculate maximal possibly needed amount of objects
  int nMaxObjs = 0;
  int nMaxCollideObjs = 0;
  float maxSize = FLT_MIN;
  float minDensity = FLT_MAX;
  for(int i = 0; i < data.Size(); i++)
  {
    Ref<CPosObjectTemplate> objTempl = data[i]._template;
    float maxSizeObj = (float) max(objTempl->GetDrawWidth(), objTempl->GetDrawHeight());
    if (objTempl->m_bRandomSize)    
      maxSizeObj *=  (float) (1 + objTempl->m_nMaxRanSize / 100.0f);

    if (maxSizeObj > maxSize)
      maxSize = maxSizeObj;

    // I will not calculate exactly maximal length with sqew + rotate, lets say coeficient by  2 for this effect.
    // On the other hand I am interesting in half maximal size that is why I will let result untouched.    
    if (data[i]._collide)    
      nMaxCollideObjs += (int) (data[i]._density * (height / (float) VIEW_LOG_UNIT_SIZE) * (width / (float) VIEW_LOG_UNIT_SIZE));      
    else
      nMaxObjs += (int) (data[i]._density * (height / (float) VIEW_LOG_UNIT_SIZE) * (width / (float) VIEW_LOG_UNIT_SIZE));    

    if (data[i]._density < minDensity)
      minDensity = data[i]._density;

  }

  _objs.Resize(0);
  _objs.Reserve(nMaxObjs,nMaxObjs); 

  if (nMaxCollideObjs > 0)
  {  
    _collideObjs.Reserve(nMaxCollideObjs,nMaxCollideObjs); // I do not how much objects there will be. but lets guest...
     maxSize *= VIEW_LOG_UNIT_SIZE / _doc.m_nRealSizeUnit + 1;

#define MAX_COLLIDE_PER_STEP 1000
    if (nMaxCollideObjs <= MAX_COLLIDE_PER_STEP)
    {
      _collideObjsTemp.Reserve(nMaxCollideObjs,nMaxCollideObjs);
     
      CRect temp = logHull;
      temp.InflateRect((int) maxSize, (int) maxSize);

      CollectPosEdObjsInterfereIntoArea(temp);  
      int iStart = _collideObjsTemp.Size();
      PlaceObjectsRandomalyEx(logHull); 
      for(int k = iStart; k < _collideObjsTemp.Size(); k++)
        _collideObjs.Add(_collideObjsTemp[k]);
    }
    else
    {
      int tiles = (int) sqrt(nMaxCollideObjs / MAX_COLLIDE_PER_STEP) + 1;
      
      int xStep = width / tiles;
      int yStep = height / tiles;
      for(int i = 0; i < tiles; i++)
      {
        CRect tile;
        tile.left = i * xStep + logHull.left;
        tile.right = tile.left + xStep;

        for(int j = 0; j < tiles; j++)
        {
          tile.top = j * yStep +  + logHull.top;
          tile.bottom = tile.top + yStep;

          CRect temp = tile;
          temp.InflateRect((int) maxSize, (int) maxSize);

          _collideObjsTemp.Resize(0);

          CollectPosEdObjsInterfereIntoArea(temp);
          CollectCollideObjsInterfereIntoArea(temp);
          int iStart = _collideObjsTemp.Size();
          PlaceObjectsRandomalyEx(tile);

          for(int k = iStart; k < _collideObjsTemp.Size(); k++)
            _collideObjs.Add(_collideObjsTemp[k]);

        }
      }
    }
  }
  else  
    PlaceObjectsRandomalyEx(logHull);  


  for(int i = 0; i < _objs.Size(); i++)  
  {
    _objs[i]->SetFreeID();    
    _doc.GenerNewObjectAction(IDA_OBJECT_ADD,NULL,_objs[i],FALSE,_pGrpAction);
  }

  for(int i = 0; i < _collideObjs.Size(); i++)  
  {
    _collideObjs[i]->SetFreeID();
    _doc.GenerNewObjectAction(IDA_OBJECT_ADD,NULL,_collideObjs[i],FALSE,_pGrpAction);
  }
}

void CRandomObjectPlacer::PlaceObjectsRandomalyEx(const CRect& logBounding)
{
  int height = logBounding.Height();
  int width = logBounding.Width();

  for(int i = 0; i < _data->Size(); i++)
  {
    Ref<CPosObjectTemplate> objTempl = (*_data)[i]._template;
    int nObjs =(int) ((*_data)[i]._density * (height / (float) VIEW_LOG_UNIT_SIZE) * (width / (float) VIEW_LOG_UNIT_SIZE));
    if (!(*_data)[i]._collide)
    {    
      for(int j = 0; j < nObjs; j++)
      {
        CPoint pos(logBounding.left + (int) (width * (rand()/(float) RAND_MAX)),logBounding.top + (int) (height * (rand()/(float) RAND_MAX)));
        if (_area->IsPointAtArea(pos))
        {
          REALPOS rPos = _doc.m_PoseidonMap.FromViewToRealPos(pos);
          // vytvo��m nov� objekt
          CPosEdObject* pObj = new CPosEdObject(_doc.m_PoseidonMap);
          pObj->CreateFromTemplateAt(objTempl,rPos);
          pObj->m_pTemplate = NULL; // in undo/redo should not be referncies to banks
          // nen� ur�eno ID objektu !
          ASSERT(pObj->GetID() == UNDEF_REG_ID); 
          pObj->CalcObjectLogPosition();

          _objs.Add(pObj);
        }
      }
    }  
    else
    {
      for(int j = 0; j < nObjs; j++)
      {
        CPoint pos(logBounding.left + (int) (width * (rand()/(float) RAND_MAX)),logBounding.top + (int) (height * (rand()/(float) RAND_MAX)));
        if (_area->IsPointAtArea(pos))
        {
          // find save position
          CPosEdObject* pObj = new CPosEdObject(_doc.m_PoseidonMap);
          int k = 0;
#define TRIES 20
          for(; k < TRIES; )
          {        
            if (_area->IsPointAtArea(pos))
            {
              k++;
              REALPOS rPos = _doc.m_PoseidonMap.FromViewToRealPos(pos);
              // vytvo��m nov� objekt              
              pObj->CreateFromTemplateAt(objTempl,rPos);
              pObj->m_pTemplate = NULL; // in undo/redo should not be referncies to banks
              // nen� ur�eno ID objektu !
              ASSERT(pObj->GetID() == UNDEF_REG_ID); 
              pObj->CalcObjectLogPosition();

              //CRect newHull;
              //pObj->GetObjectHull(newHull);

              int l = 0;
              //CRect oldHull;
              for(; l < _collideObjsTemp.Size(); l++)
              {
                //objs[l]->GetObjectHull(oldHull);
                if (pObj->IsIntersection(*_collideObjsTemp[l]))
                  break;
              }

              if (l == _collideObjsTemp.Size())
                break;              
            }

            pos = CPoint(logBounding.left + (int) (width * (rand()/(float) RAND_MAX)),logBounding.top + (int) (height * (rand()/(float) RAND_MAX)));
          }

          if (k < TRIES)
          {                     
            _collideObjsTemp.Add(pObj);
          }
          else
            delete pObj;
        }
      }
    }
  }
}

/////////////////////////////////////////////////////////////////
// Action

CPosAction* Action_RandomPlacementObject(CPosEdMainDoc* pDocument, CPosActionGroup* pToGroup /*= NULL*/)
{
  CObjectRandomPlacerConfigDlg dlg(*pDocument);
  if (dlg.DoModal() != IDOK)
    return NULL;

  ObjectRandomPlacerConfig& config = dlg.GetCurConfig();
 
  AfxGetMainWnd()->BeginWaitCursor();
  CPosObjectGroupAction* pGrpAction = new CPosObjectGroupAction(IDA_GROUPOBJ_PASTE);

  CRandomObjectPlacer placer(*pDocument);
  placer(pGrpAction, pDocument->m_CurrentArea, config._RandomTemplates);

  if (pToGroup)
    pToGroup->AddAction(pGrpAction);
  else
    pDocument->ProcessEditAction(pGrpAction);

  AfxGetMainWnd()->EndWaitCursor();

  return pGrpAction;
}


