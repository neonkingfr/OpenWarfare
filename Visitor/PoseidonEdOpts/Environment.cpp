/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include <atlconv.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Prost�ed� aplikace


IMPLEMENT_DYNAMIC(CPosEdEnvironment, CObject)

// constructor
CPosEdEnvironment::CPosEdEnvironment()
{
	m_pRealDoc = NULL;
};

CPosEdEnvironment::~CPosEdEnvironment()
{
};

// inicializace prost�ed� (dle typu programu)
BOOL CPosEdEnvironment::DoInitEnvironment(const TCHAR * pszConfFileName)
{
	static TCHAR strPosEd[] = _T("Poseidon");

  USES_CONVERSION;
  m_szConfFileName = T2CA (pszConfFileName);
  m_cConfFile.Parse(m_szConfFileName);
    
	// parametry syst�mu
  
	m_optSystem.SetInOutParamFile("SystemOptions", &m_cConfFile);
	m_optSystem.LoadParams();

	// jm�no u�ivatele
	//CString sUser = strPosEd;
	// asociace options se soubory	
        
	m_optPosEd.SetInOutParamFile("EditorOptions",  &m_cConfFile);
	m_optPosEd.LoadParams();

	// asociace konfigurace se soubory
	/*GetPoseidonDirectory(sRoot, dirOpts);

	static TCHAR cfgFileExt[] = _T(".cfg");
	sRoot += sUser;		sRoot += cfgFileExt;
    m_cfgPosEdMgr.SetStyle(CMntOptions::optFile, sRoot, sRoot);
    */
   
	m_cfgCurrent.SetInOutParamFile("Configuration", &m_cConfFile);       
	m_cfgCurrent.LoadParams();

  m_appDockState.SetInOutParamFile("DockState", &m_cConfFile); 
  m_appDockState.LoadParams();

	return TRUE;
};

void CPosEdEnvironment::DoDoneEnvironment()
{
    
    m_optSystem.ReleaseInOutParamFile();
    m_optPosEd.ReleaseInOutParamFile();
    m_cfgCurrent.ReleaseInOutParamFile();
    
    m_cConfFile.Clear();
}



// p��stup k adres���m
/*void CPosEdEnvironment::GetPoseidonDirectory(CString& sDir, int nDir) const
{
	switch(nDir)
	{
	case dirPosEd:
		MakeLongDirStr(sDir,m_optSystem.m_sPosEdDirectory); break;
	case dirOpts:
		MakeLongDirStr(sDir,m_optSystem.m_sOptionsDirectory); break;
	case dirObjTxt:	// textury
		MakeLongDirStr(sDir,m_optSystem.m_sTexturesPath); break;
	case dirObjNat:	// p��roda
		MakeLongDirStr(sDir,m_optSystem.m_sObjNaturPath); break;
	case dirObjPeo:	// lid�
		MakeLongDirStr(sDir,m_optSystem.m_sObjPeoplPath); break;
	case dirObjNet:	// s�t�
		MakeLongDirStr(sDir,m_optSystem.m_sObjOtherPath); break;
	case dirObjWoo:	// lesy
		MakeLongDirStr(sDir,m_optSystem.m_sObjOtherPath); break;
	case dirObjWrd:	// sv�ty
		MakeLongDirStr(sDir,m_optSystem.m_sObjWorldPath); break;
	};
};*/

// p��stup k soubor�m
void CPosEdEnvironment::GetPoseidonFile(CString& sFile, int nFile) const
{

};

/////////////////////////////////////////////////////////////////////////////
// �prava glob�ln�ch dat z�vosl�ch na konfiguraci

void CPosEdEnvironment::OnChangeCurrentConfig()
{
};

// Flush file with configuration
void CPosEdEnvironment::FlushConfFile()
{
    m_cConfFile.Save(m_szConfFileName);    
}
