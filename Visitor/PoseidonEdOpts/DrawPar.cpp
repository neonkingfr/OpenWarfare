/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDrawPar - parametry vykreslov�n�

IMPLEMENT_DYNAMIC(CDrawPar, CObject)

// constructor
CDrawPar::CDrawPar()
{
	m_pEnvir = GetPosEdEnvironment();
	ASSERT(m_pEnvir != NULL);
	m_pCfg	 = &(m_pEnvir->m_cfgCurrent);
	// nen� zn�m dokument
	m_pDocument = NULL;
	// inicializace temp pro objekty
	for(int i=0; i<stdObjectCount; i++)
	{
		m_pStdObjPen[i] = NULL;
		m_pStdObjBrush[i] = NULL;
	}
	// inicializace temp pro pozad�
};

CDrawPar::~CDrawPar()
{
	DestroyTempObjects();
	DestroyTempBackground();
};


/////////////////////////////////////////////////////////////////////////////
// parametry pro vykreslov�n� objekt�

void CDrawPar::CreateTempObjects()
{
	ASSERT(m_pCfg != NULL);
	// standardn� barvy objektu
	m_cStdObjFrame[stdObjectNone]	= m_pCfg->m_cDefColors[CPosEdCfg::defcolUObFrm];
	m_cStdObjFrame[stdObjectNatur]	= m_pCfg->m_cDefColors[CPosEdCfg::defcolNObFrm];
	m_cStdObjFrame[stdObjectPeople]	= m_pCfg->m_cDefColors[CPosEdCfg::defcolPObFrm];
	m_cStdObjEntir[stdObjectNone]	= m_pCfg->m_cDefColors[CPosEdCfg::defcolUObEnt];
	m_cStdObjEntir[stdObjectNatur]	= m_pCfg->m_cDefColors[CPosEdCfg::defcolNObEnt];
	m_cStdObjEntir[stdObjectPeople]	= m_pCfg->m_cDefColors[CPosEdCfg::defcolPObEnt];
	// pera a brush pro objekty
	for(int i=0; i<stdObjectCount; i++)
	{
		m_pStdObjPen[i]		= new CPen(PS_SOLID,1,m_cStdObjFrame[i]);
		m_pStdObjBrush[i]	= new CBrush(m_cStdObjEntir[i]);
	}
};

void CDrawPar::DestroyTempObjects()
{
	for(int i=0; i<stdObjectCount; i++)
	{
		if (m_pStdObjPen[i]) 
		{
			delete (m_pStdObjPen[i]);
			m_pStdObjPen[i] = NULL;
		};
		if (m_pStdObjBrush[i])
		{
			delete (m_pStdObjBrush[i]);
			m_pStdObjBrush[i] = NULL;
		};
	}
};

/////////////////////////////////////////////////////////////////////////////
// parametry pro vykreslov�n� pozad�

void CDrawPar::CreateTempBackground()
{
	// dafultov� hodnoty
	m_cStdSeaColor = m_pCfg->m_cDefColors[CPosEdCfg::defcolSea];
	m_cStdGndColor = m_pCfg->m_cDefColors[CPosEdCfg::defcolGround];
	m_cRegSeaColor = m_pCfg->m_cDefColors[CPosEdCfg::defcolSea];
	m_cUndefLColor = m_pCfg->m_cDefColors[CPosEdCfg::defcolTextur];

	// vrstevnice
	m_nStpCountLn  = STD_SCALE_COLOR_STEP;
	m_cCountLnColor= m_pCfg->m_cCountLns;

	// vytvo�en� objekt�
	for(int i=0; i<MAX_SCALE_COLOR_ITEMS;i++)
		m_cColorHg[i] = m_pCfg->m_cColorHg[i];
};

void CDrawPar::DestroyTempBackground()
{
};




