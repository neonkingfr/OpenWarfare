/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "PoseidonEdOptsDll.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Options SYSTEM - CSystemOptions

BEGIN_OPTIONS_MAP( CSystemOptions )
	//MNTOPT_STRING( m_sPosEdDirectory,   _T("C:\\Poseidon"),"MainDir" )
	//MNTOPT_STRING( m_sOptionsDirectory, _T("C:\\Poseidon"),"OptsDir" )
	MNTOPT_STRING( m_sPreviewAppPath, _T(""),"ViewAppWorkDir" )
  MNTOPT_STRING( m_sPreviewAppCommand, _T(""),"ViewAppCommand" )
	//MNTOPT_STRING( m_sTexturesPath,	  _T(""),"DirOTxt" )
	MNTOPT_STRING( m_sObjNaturPath,	  _T(""),"DirONat" )
	MNTOPT_STRING( m_sObjPeoplPath,	  _T(""),"DirOPeo" )
	//MNTOPT_STRING( m_sObjOtherPath,	  _T(""),"DirOOth" )
	MNTOPT_STRING( m_sObjWorldPath,	  _T(""),"DirWrld" )
  MNTOPT_STRING( m_sScriptPath,	  _T(""),"DirScript" )
  MNTOPT_STRING( m_sNetPath,	  _T(""),"DirNet" )
	//MNTOPT_STRING( m_sBankMapsPath,	  _T(""),"BankPth" )
	//MNTOPT_STRING( m_sBankTexture,	  _T(""),"BankTxt" )
	//MNTOPT_INT(	   m_nStdWorldWidth,  256, "StdWidth" )
	//MNTOPT_INT(	   m_nStdWorldHeight, 256, "StdHeight" )
	//MNTOPT_BOOL(   m_bStdSizeOnly,	  TRUE,"StdOnly" )
	//MNTOPT_INT(	   m_nRealSizeUnit,   50,  "RealSz" )  
  MNTOPT_STRING( m_sccServerName,	  _T(""),"SccServerName" )
  MNTOPT_STRING( m_sccProjectName,	  _T(""),"SccProjectName" )
  MNTOPT_STRING( m_sccLocalPath,	  _T(""),"SccLocalPath" )
END_OPTIONS_MAP()

void CSystemOptions::Flush()  const
{
    g_PosEdEnvironment.FlushConfFile();
}

/////////////////////////////////////////////////////////////////////////////
// Options PosEd  - CPosEdOptions

IMPLEMENT_DYNAMIC(CViewStyle,CFMntOptions)

CViewStyle::CViewStyle()
{	/*Deafult();*/ 
}

CViewStyle::~CViewStyle()
{
}

void CViewStyle::Flush() const
{
	g_PosEdEnvironment.FlushConfFile();
}

EnabledObjects CViewStyle::GetEnableObjects() const
{ 
	EnabledObjects ret;
	ret.m_bPeopleObjcs = m_bPeopleObjcs;
	ret.m_bNatureObjcs = m_bNatureObjcs;

	ret.m_bNewRoadsObjcs = m_bNewRoadsObjcs;

	ret.m_bNets = m_bShowNets;
	ret.m_bWoods = m_bShowWoods;
	ret.m_bNameArea = m_bAreasBorder || m_bAreasEntire;
	ret.m_bKeyPt = m_bKeyPtBorder || m_bKeyPtEntire;
	ret.m_bBGImage = m_bShowBGImage;

	return ret;
}

BEGIN_OPTIONS_MAP( CViewStyle )
  MNTOPT_INT(  m_nBaseStyle    ,  tvsTextur, "BaseStyle")
  MNTOPT_BOOL( m_bShowCountLn  ,  FALSE    , "ShowCountLn")
  MNTOPT_BOOL( m_bNatureObjcs  ,  TRUE     , "NatureObjcs")
  MNTOPT_BOOL( m_bPeopleObjcs  ,  TRUE     , "PeopleObjcs")

  MNTOPT_BOOL( m_bNewRoadsObjcs,  TRUE     , "NewRoadsObjcs")

  MNTOPT_BOOL( m_bUnknowObjcs  ,  TRUE     , "UnknowObjcs")
  MNTOPT_BOOL( m_bShowWoods    ,  TRUE     , "ShowWoods")
  MNTOPT_BOOL( m_bShowNets     ,  TRUE     , "ShowNets")
  MNTOPT_BOOL( m_bShowCountLn  ,  FALSE    , "ShowCountLn")
  MNTOPT_BOOL( m_bShowGrid     ,  TRUE     , "ShowGrid")
  MNTOPT_BOOL( m_bShowScndTxtr ,  FALSE    , "ShowScndTxtr")
  MNTOPT_BOOL( m_bShowSea      ,  TRUE     , "ShowSea")
  MNTOPT_BOOL( m_bAreasBorder  ,  TRUE     , "AreasBorder")
  MNTOPT_BOOL( m_bAreasEntire  ,  FALSE    , "AreasEntire")
  MNTOPT_BOOL( m_bKeyPtBorder  ,  TRUE     , "KeyPtBorder")
  MNTOPT_BOOL( m_bKeyPtEntire  ,  TRUE     , "KeyPtEntire")
  MNTOPT_BOOL( m_bShowRuler    ,  TRUE     , "ShowRuler")
  MNTOPT_BOOL( m_bShowBGImage  ,  TRUE     , "ShowBGImages")
  MNTOPT_BOOL( m_bWaterLayer   ,  TRUE     , "ShowWaterLayer")
  MNTOPT_BOOL( m_Shadowing     ,  FALSE    , "Shadowing")
  MNTOPT_BOOL( m_ShowLock      ,  FALSE    , "ShowLocks")
END_OPTIONS_MAP()

/*

void CViewStyle::CopyFrom(const CViewStyle* pSrc)
{
	m_nBaseStyle	= pSrc->m_nBaseStyle;

	m_bShowCountLn	= pSrc->m_bShowCountLn;
	m_bShowGrid		= pSrc->m_bShowGrid;
	m_bShowScndTxtr = pSrc->m_bShowScndTxtr;
	m_bShowSea		= pSrc->m_bShowSea;

	m_bNatureObjcs	= pSrc->m_bNatureObjcs;
	m_bPeopleObjcs	= pSrc->m_bPeopleObjcs;
	m_bUnknowObjcs	= pSrc->m_bUnknowObjcs;

	m_bShowWoods	= pSrc->m_bShowWoods;
	m_bShowNets		= pSrc->m_bShowNets;

	m_bAreasBorder	= pSrc->m_bAreasBorder;
	m_bAreasEntire	= pSrc->m_bAreasEntire;

	m_bKeyPtBorder	= pSrc->m_bKeyPtBorder;
	m_bKeyPtEntire	= pSrc->m_bKeyPtEntire;
};

void CViewStyle::Serialize(CArchive& ar)
{
	MntSerializeInt( ar,m_nBaseStyle);

	MntSerializeBool(ar,m_bShowCountLn);
	MntSerializeBool(ar,m_bShowGrid);
	MntSerializeBool(ar,m_bShowScndTxtr);
	MntSerializeBool(ar,m_bShowSea);

	MntSerializeBool(ar,m_bNatureObjcs);
	MntSerializeBool(ar,m_bPeopleObjcs);
	MntSerializeBool(ar,m_bUnknowObjcs);

	MntSerializeBool(ar,m_bShowWoods);
	MntSerializeBool(ar,m_bShowNets);

	MntSerializeBool(ar,m_bAreasBorder);
	MntSerializeBool(ar,m_bAreasEntire);
	
	MntSerializeBool(ar,m_bKeyPtBorder);
	MntSerializeBool(ar,m_bKeyPtEntire);
};*/

BEGIN_OPTIONS_MAP( CPosEdOptions )
	MNTOPT_BOOL( m_bMaximizeApp,  TRUE, "MaxApp"  )
	MNTOPT_INT(  m_nMoveWindowCoef, 60,	"MovCoe")
	MNTOPT_DOUBLE(m_nInOutScaleStep, 1.6,"ScaStp")
	MNTOPT_BOOL( m_bLastMaximize, TRUE, "FrmMax"  )
	MNTOPT_INT(  m_nLastXPos, 0,		"FrmPosX" )
	MNTOPT_INT(  m_nLastYPos, 0,		"FrmPosY" )
	MNTOPT_INT(  m_nLastCXPos,300,		"FrmPosCX")
	MNTOPT_INT(  m_nLastCYPos,300,		"FrmPosCY")
	// recent files
	MNTOPT_STRING( m_sRecentFile[0], _T(""),"RecFile1" )
	MNTOPT_STRING( m_sRecentFile[1], _T(""),"RecFile2" )
	MNTOPT_STRING( m_sRecentFile[2], _T(""),"RecFile3" )
	MNTOPT_STRING( m_sRecentFile[3], _T(""),"RecFile4" )
	// z�kladn� rozm�ry dokumentu
	MNTOPT_INT(  m_nBaseWorldWidth, 256,"BaseWdth")
	MNTOPT_INT(  m_nBaseWorldHeight,256,"BaseHght")
	// form�t p�echodov� textury
	MNTOPT_INT(  m_nTextureFormat,   0, "TxtrType")
	// okno projektu - po��te�n� stav
	MNTOPT_BOOL( m_bMaxProjWnd, TRUE,	"MaxWnd")
	MNTOPT_BOOL( m_bFitProjWnd, FALSE,	"FitWnd")
	MNTOPT_INT(  m_nProjScale,	100,	"BgnScl")
	// Buldozer - varianty komunikace
	//MNTOPT_INT(  m_nBldCursorUpdate,	1,		"BldCur")
	//MNTOPT_BOOL( m_bBldSynchrAction,	TRUE,	"BldSyn")
	// eroze krajiny
	MNTOPT_INT(  m_WashParams.nGens, 32,		"WshGen")
	MNTOPT_DOUBLE(m_WashParams.washCoef, 0.05,	"WshCoe")
	MNTOPT_DOUBLE(m_WashParams.sedimCoef,80.,	"WshSCo")
	MNTOPT_DOUBLE(m_WashParams.sedimBase,0.1,	"WshSBa")
	MNTOPT_DOUBLE(m_WashParams.sedimEff, 0.9,	"WshEff")
	MNTOPT_DOUBLE(m_WashParams.initRain, 0.1,	"WshIRn")
	MNTOPT_DOUBLE(m_WashParams.steadyRain,0.05,	"WshSRn")
	MNTOPT_BOOL( m_WashParams.finalBlur, TRUE,	"WshBlr")
	// parametry pro generov�n� s�t�
	MNTOPT_DOUBLE(m_NetGenParams.nSamePosTol,.01,"NGPpst")
	MNTOPT_DOUBLE(m_NetGenParams.nSamePosHgt,.005,"NGPhgt")
	MNTOPT_INT(m_NetGenParams.nSameGraTol,0,     "NGPgrt")
	//MNTOPT_LONG(m_NetGenParams.nCountLimit,20,   "NGPlim")
	MNTOPT_LONG(m_NetGenParams.nVarisLimit,50000, "NGPvar")  
  MNTOPT_DOUBLE(m_NetGenParams.fBaseHeuristicCoef,1.2,"fBaseHeuristicCoef")
  MNTOPT_BOOL( m_NetGenParams.bFitEndAngle, FALSE,	"FitEndAngle")
  MNTOPT_BOOL( m_NetGenParams.bAvoidHills, FALSE,	"AvoidHills")
	MNTOPT_DOUBLE(m_NetGenParams.fAvoidHillsCoef,5.0,"AvoidHillsCoef")
  MNTOPT_BOOL( m_NetGenParams.bPunishCompChange, FALSE,	"PunishCompChange")
	MNTOPT_DOUBLE(m_NetGenParams.fPunishCompChangeCoef,5.0,"PunishCompChangeCoef")
  MNTOPT_BOOL( m_NetGenParams.bMaxSlope, TRUE,	"CheckMaxSlope")
	MNTOPT_INT(m_NetGenParams.nMaxSlope,30,"MaxSlope")
  MNTOPT_BOOL( m_NetGenParams.bMaxSlopeChange, TRUE,	"CheckMaxSlopeChange")
	MNTOPT_INT(m_NetGenParams.nMaxSlopeChange,10,"MaxSlopeChange")
	//MNTOPT_DOUBLE(m_NetGenParams.nHeightStep,0.2,"NGPstp")
	//MNTOPT_LONG(m_NetGenParams.nSTRA[0], 30,    "NGPst0")
	//MNTOPT_LONG(m_NetGenParams.nSTRA[1], 50,    "NGPst1")
	//MNTOPT_LONG(m_NetGenParams.nSTRA[2], 80,    "NGPst2")
	//MNTOPT_LONG(m_NetGenParams.nBEND[0],100,	"NGPbn0")
	//MNTOPT_LONG(m_NetGenParams.nBEND[1],100,	"NGPbn1")
	//MNTOPT_LONG(m_NetGenParams.nBEND[2],100,	"NGPbn2")
	//MNTOPT_LONG(m_NetGenParams.nBEND[3],100,	"NGPbn3")
	//MNTOPT_LONG(m_NetGenParams.nPricePrvSl, 4,	"NGPprv")
	//MNTOPT_LONG(m_NetGenParams.nPriceSlope, 2,	"NGPslp")
	//MNTOPT_LONG(m_NetGenParams.nPriceStand, 0,	"NGPstd")
	//MNTOPT_LONG(m_NetGenParams.nLenPrice,	0,	"NGPlen")
	//MNTOPT_LONG(m_NetGenParams.nBendPrice,	0,	"NGPbnd")
	//MNTOPT_LONG(m_NetGenParams.nDodLenPrice,100,"NGPddl")
	//MNTOPT_LONG(m_NetGenParams.nDodKoefGrad,100,"NGPddg")
	//MNTOPT_LONG(m_NetGenParams.nDodKoefSlop,100,"NGPdds")
	//MNTOPT_LONG(m_NetGenParams.nDodKoefHght,500,"NGPddh")
	//MNTOPT_LONG(m_NetGenParams.nDodIdealDif,1000,"NGPddi")
	// parametry pro nastaven� v��ky s�t�
	MNTOPT_DOUBLE(m_NetHgtParams.nHeightStep,0.2,	"NGHstp")
	MNTOPT_LONG(m_NetHgtParams.nErrBadPrvHg,100000,	"NGHerh")
	MNTOPT_LONG(m_NetHgtParams.nErrBadNetUp,50000,	"NGHeru")
	MNTOPT_LONG(m_NetHgtParams.nErrBadNetDn,50000,	"NGHerd")
	MNTOPT_LONG(m_NetHgtParams.nErrBadSlope,500,	"NGHers")
	MNTOPT_LONG(m_NetHgtParams.nErrBadPrvSl,700,	"NGHerp")
	MNTOPT_LONG(m_NetHgtParams.nPricePrvSl,10,		"NGHprs")
	MNTOPT_LONG(m_NetHgtParams.nPriceSlope,7,		"NGHslp")
	MNTOPT_LONG(m_NetHgtParams.nPriceStand,2,		"NGHstd")
  MNTOPT_CLASS(m_ViewStyle,"ViewStyle")
END_OPTIONS_MAP()

CPosEdOptions::CPosEdOptions()
{
};

CPosEdOptions::~CPosEdOptions()
{	
};

// nastaven� def. hodnot
/*void CPosEdOptions::SetDefaultValues()
{
	CFMntOptions::SetDefaultValues();
};

// kopie hodnot
void CPosEdOptions::CopyFrom(CFMntOptions* pSrc)
{
	CFMntOptions::CopyFrom(pSrc);
};
*/

// update hodnot do souboru (funkce nesm� b�t vol�na p��mo)
/*void CPosEdOptions::DoSerialize(CArchive& ar, DWORD nVer, void* pParams)
{
	CFMntOptions::DoSerialize(ar, nVer, pParams);
	// serialize ViewStyle
	m_ViewStyle.Serialize(ar);
	// serializace informac� o DockBars
	m_appDockState.Serialize(ar);
};*/

void CPosEdOptions::Flush() const
{
    g_PosEdEnvironment.FlushConfFile();
}

////////////////////////////////////////////////
// DockState

IMPLEMENT_DYNAMIC(CPosDockState,CFMntDockState)

void CPosDockState::Flush() const
{
  g_PosEdEnvironment.FlushConfFile();
}

#define LOAD_INT(name, value, defvalue) pEntry = pClass->FindEntry(name);\
  if (pEntry.IsNull()) \
  value = defvalue; \
  else \
  value = (int) (*pEntry);

BOOL CPosDockState::LoadParams(BOOL bReportErr)
{
  if (m_pParentClass.IsNull())
    return FALSE;

  if (!CFMntDockState::LoadParams(bReportErr))
    return FALSE;

  ParamEntryPtr pEntry = m_pParentClass->FindEntry(m_cClassName);
  if (pEntry.IsNull() || !pEntry->IsClass())
    return FALSE;

  ParamClassPtr pClass = pEntry->GetClassInterface();
  
  LOAD_INT("ObjPanelX", m_objectPanelSize.cx, 150);
  LOAD_INT("ObjPanelY", m_objectPanelSize.cy, 300);
  LOAD_INT("ObjScriptX", m_scriptPanelSize.cx, 200);
  LOAD_INT("ObjScriptY", m_scriptPanelSize.cy, 300);
  LOAD_INT("ObjNamedSelX", m_namSelPanelSize.cx, 150);
  LOAD_INT("ObjNamedSelY", m_namSelPanelSize.cy, 300);

  return TRUE;
}

BOOL CPosDockState::SaveParams(BOOL bReportErr)
{
  if (m_pParentClass.IsNull())
    return FALSE;

  if (!CFMntDockState::SaveParams( bReportErr))
    return FALSE;

  ParamEntryPtr pEntry = m_pParentClass->FindEntry(m_cClassName);
  if (pEntry.IsNull() || !pEntry->IsClass())
    return FALSE;

  ParamClassPtr pClass = pEntry->GetClassInterface();
 
  CSize size = GetScreenSize();
  pClass->Add( "ObjPanelX", m_objectPanelSize.cx);
  pClass->Add( "ObjPanelY", m_objectPanelSize.cy);
  pClass->Add( "ObjScriptX", m_scriptPanelSize.cx);
  pClass->Add( "ObjScriptY", m_scriptPanelSize.cy);
  pClass->Add( "ObjNamedSelX", m_namSelPanelSize.cx);
  pClass->Add( "ObjNamedSelY", m_namSelPanelSize.cy);

  return TRUE;
}



