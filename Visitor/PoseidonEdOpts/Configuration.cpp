/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"
#include "PoseidonEdOptsDll.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Konfigurace

IMPLEMENT_DYNAMIC(CPosEdCfg,CObject)

// constructor
CPosEdCfg::CPosEdCfg()
{	// nastaven� def. hodnot
    m_tDefColorsOptMapEntry.nArrayItemType = CFMntOptions::optColor;
    m_tDefColorsOptMapEntry.nSize = defcolCount;
    m_tDefColorsOptMapEntry.pArray = m_cDefColors;

    m_tColorHgOptMapEntry.nArrayItemType = CFMntOptions::optClass;
    m_tColorHgOptMapEntry.nStep = sizeof(SScaleItem);
    m_tColorHgOptMapEntry.nSize = MAX_SCALE_COLOR_ITEMS;
    m_tColorHgOptMapEntry.pArray = m_cColorHg;

	SetDefaultValues();
};

CPosEdCfg::~CPosEdCfg()
{};

// nastaven� def. hodnot
static COLORREF defcolValues[CPosEdCfg::defcolCount] = 
{
	RGB(  0,   0, 255),		// mo�e
	RGB(192, 192, 192),		// pevnina
	RGB( 64,  64,  64),		// textura (neident.)
	RGB(  0,   0,   0),		// obrys nedef. objektu
	RGB(128, 128, 128),		// v�pl� nedef. objektu
	RGB(  0,   0,   0),		// obrys p�ir. objektu
	RGB(  0, 255,   0),		// v�pl� p�ir. objektu
	RGB(  0,   0,   0),		// obrys lids. objektu
	RGB(255,   0,   0),		// v�pl� lids. objektu
	RGB(  0,   0, 128),		// s�t� - kl��ov�
	RGB(  0,   0,   0),		// s�t� - norm�ln�
	RGB(  0,   0, 255),		// s�t� - k�i�ovatka
	RGB(  0, 128,   0),		// les�
	RGB(  0,  64,   0),		// r�m - les�
	RGB(255, 128,   0),		// pojm. plocha
	RGB(128,  64,   0),		// r�m - pojm. plocha
	RGB(255,   0, 128),		// kl��ov� bod
	RGB(128,   0,   0),		// r�m - kl��ov� bod
	RGB(192, 192, 192),		// vybran� plocha
	RGB(255, 255, 255),		// vybran� plocha - r�m

	RGB(  0,   0,   0),		// new roads
	RGB(128, 128, 128),		// new roads
};

/*
static SScaleItem  defColorHg[MAX_SCALE_COLOR_ITEMS] =
{
	{ -250.f, RGB(0,0,92) },	// odst�n modr�
	{ -240.f, RGB(6,10,98) },
	{ -230.f, RGB(13,20,104) },
	{ -220.f, RGB(20,29,111) },
	{ -210.f, RGB(27,39,117) },
	{ -200.f, RGB(34,49,130) },
	{ -190.f, RGB(41,59,136) },
	{ -180.f, RGB(47,69,142) },
	{ -170.f, RGB(54,78,148) },
	{ -160.f, RGB(61,88,155) },
	{ -150.f, RGB(67,98,161) },
	{ -140.f, RGB(74,108,167) },
	{ -130.f, RGB(81,118,173) },
	{ -120.f, RGB(87,128,180) },
	{ -110.f, RGB(94,137,186) },
	{ -100.f, RGB(101,147,192) },
	{ -90.f,  RGB(108,157,198) },
	{ -80.f,  RGB(115,167,205) },
	{ -70.f,  RGB(122,177,211) },
	{ -60.f,  RGB(128,186,217) },
	{ -50.f,  RGB(135,196,224) },	
	{ -40.f,  RGB(142,206,230) },
	{ -30.f,  RGB(149,216,236) },
	{ -20.f,  RGB(156,226,242) },
	{ -10.f,  RGB(165,240,248) },
	{ 0.f,	  RGB(176,255,255) },		
	{ 10.f, RGB(0,64,0) },		// odst�n zelen�
	{ 20.f, RGB(0,72,0) },
	{ 30.f, RGB(0,85,0) },
	{ 40.f, RGB(0,97,0) },
	{ 50.f, RGB(0,110,0) },
	{ 60.f, RGB(0,128,0) },
	{ 70.f, RGB(30,139,0) },
	{ 80.f, RGB(40,150,1) },
	{ 90.f, RGB(70,163,2) },
	{ 100.f, RGB(110, 175,2) },
	{ 110.f, RGB(139, 190, 3) },
	{ 120.f, RGB(152, 201, 3) },
	{ 130.f, RGB(165, 218, 3) },
	{ 140.f, RGB(175, 232, 4) },
	{ 150.f, RGB(182, 240, 4) },
	{ 160.f, RGB(227, 240, 5) },// odst�n �lut�
	{ 170.f, RGB(235, 243,0) },
	{ 180.f, RGB(241, 246,0) },
	{ 190.f, RGB(249, 250,0) },
	{ 200.f, RGB(255, 255,0) },
	{ 210.f, RGB(252, 235,0) },
	{ 220.f, RGB(249, 210,0) },
	{ 230.f, RGB(245, 192,0) },
	{ 240.f, RGB(241, 181,0) },
	{ 250.f, RGB(237, 170,0) },
	{ 260.f, RGB(233, 157,0) },
	{ 270.f, RGB(230, 148,0) },
	{ 280.f, RGB(227, 138,0) },
	{ 290.f, RGB(226, 128,0) },
	{ 300.f, RGB(224, 112,0) },// odst�n hn�d�
	{ 310.f, RGB(216, 109,3) },
	{ 320.f, RGB(207, 104,7) },
	{ 330.f, RGB(197, 101,10) },
	{ 340.f, RGB(187, 98, 15) },
	{ 350.f, RGB(177, 94, 20) },
	{ 360.f, RGB(164, 90, 25) },
	{ 370.f, RGB(152, 86, 31) },
	{ 380.f, RGB(139, 83, 37) },
	{ 390.f, RGB(127, 80, 44) },

	{ 400.f, RGB(115, 77, 39) },	// ostrn �ed�
	{ 410.f, RGB(112, 68, 31) },
	{ 420.f, RGB(108, 64, 23) },
	{ 430.f, RGB(102, 57, 17) },
	{ 440.f, RGB(92, 52,   8) },
	{ 450.f, RGB(82, 47,   4) },
	{ 460.f, RGB(72, 42,   1) },
	{ 470.f, RGB(64, 37,  0)  },
	{ 480.f, RGB(57, 33,  0)  },
	{ 490.f, RGB(48, 29,  0)  },
	{ 500.f, RGB(32, 26,  0)  },
	{ 1e6f,  RGB(0,0,0) },
};
*/
static SScaleItemDef  defColorHg[MAX_SCALE_COLOR_ITEMS] =
{
	{ -1250.f, RGB(0,0,92) },	// odst�n modr�
	{ -1200.f, RGB(6,10,98) },
	{ -1150.f, RGB(13,20,104) },
	{ -1100.f, RGB(20,29,111) },
	{ -1050.f, RGB(27,39,117) },
	{ -1000.f, RGB(34,49,130) },
	{ -950.f, RGB(41,59,136) },
	{ -900.f, RGB(47,69,142) },
	{ -850.f, RGB(54,78,148) },
	{ -800.f, RGB(61,88,155) },
	{ -750.f, RGB(67,98,161) },
	{ -700.f, RGB(74,108,167) },
	{ -650.f, RGB(81,118,173) },
	{ -600.f, RGB(87,128,180) },
	{ -550.f, RGB(94,137,186) },
	{ -500.f, RGB(101,147,192) },
	{ -450.f,  RGB(108,157,198) },
	{ -400.f,  RGB(115,167,205) },
	{ -350.f,  RGB(122,177,211) },
	{ -300.f,  RGB(128,186,217) },
	{ -250.f,  RGB(135,196,224) },	
	{ -200.f,  RGB(142,206,230) },
	{ -150.f,  RGB(149,216,236) },
	{ -100.f,  RGB(156,226,242) },
	{ -50.f,  RGB(165,240,248) },
	{ 0.f,	  RGB(176,255,255) },		
	{ 50.f, RGB(0,64,0) },		// odst�n zelen�
	{ 100.f, RGB(0,72,0) },
	{ 150.f, RGB(0,85,0) },
	{ 200.f, RGB(0,97,0) },
	{ 250.f, RGB(0,110,0) },
	{ 300.f, RGB(0,128,0) },
	{ 350.f, RGB(30,139,0) },
	{ 400.f, RGB(40,150,1) },
	{ 450.f, RGB(70,163,2) },
	{ 500.f, RGB(110, 175,2) },
	{ 550.f, RGB(139, 190, 3) },
	{ 600.f, RGB(152, 201, 3) },
	{ 650.f, RGB(165, 218, 3) },
	{ 700.f, RGB(175, 232, 4) },
	{ 750.f, RGB(182, 240, 4) },
	{ 800.f, RGB(227, 240, 5) },// odst�n �lut�
	{ 850.f, RGB(235, 243,0) },
	{ 900.f, RGB(241, 246,0) },
	{ 950.f, RGB(249, 250,0) },
	{ 1000.f, RGB(255, 255,0) },
	{ 1050.f, RGB(252, 235,0) },
	{ 1100.f, RGB(249, 210,0) },
	{ 1150.f, RGB(245, 192,0) },
	{ 1200.f, RGB(241, 181,0) },
	{ 1250.f, RGB(237, 170,0) },
	{ 1300.f, RGB(233, 157,0) },
	{ 1350.f, RGB(230, 148,0) },
	{ 1400.f, RGB(227, 138,0) },
	{ 1450.f, RGB(226, 128,0) },
	{ 1500.f, RGB(224, 112,0) },// odst�n hn�d�
	{ 1550.f, RGB(216, 109,3) },
	{ 1600.f, RGB(207, 104,7) },
	{ 1650.f, RGB(197, 101,10) },
	{ 1700.f, RGB(187, 98, 15) },
	{ 1750.f, RGB(177, 94, 20) },
	{ 1800.f, RGB(164, 90, 25) },
	{ 1850.f, RGB(152, 86, 31) },
	{ 1900.f, RGB(139, 83, 37) },
	{ 1950.f, RGB(127, 80, 44) },

	{ 2000.f, RGB(115, 77, 39) },	// ostrn �ed�
	{ 2050.f, RGB(112, 68, 31) },
	{ 2100.f, RGB(108, 64, 23) },
	{ 2150.f, RGB(102, 57, 17) },
	{ 2200.f, RGB(92, 52,   8) },
	{ 2250.f, RGB(82, 47,   4) },
	{ 2300.f, RGB(72, 42,   1) },
	{ 2350.f, RGB(64, 37,  0)  },
	{ 2400.f, RGB(57, 33,  0)  },
	{ 2450.f, RGB(48, 29,  0)  },
	{ 2500.f, RGB(32, 26,  0)  },
	{ 1e6f,  RGB(0,0,0) },
};

void CPosEdCfg::SetDefaultValues()
{
	// def. velikost 12
/*	memset(&m_CfgDefFont, 0, sizeof(m_CfgDefFont));
	m_CfgDefFont.lfHeight = -70;
	m_CfgDefFont.lfWeight = FW_NORMAL;
	m_CfgDefFont.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
	static TCHAR BASED_CODE szCfgDefFont[] = _T("Arial");
	lstrcpy(m_CfgDefFont.lfFaceName, szCfgDefFont );*/
	
	// nen� def. jm�no
	m_bDefault	= FALSE;

	// min. velikost textury v pixlech
	m_nMinTextSize	= 6;
	// nastaven� m��ky
	m_nGridStyle	= 0;				
	m_nGridColor	= RGB(255, 255, 255);		
	m_bGridXor		= TRUE;

	// zobrazen� kurzoru
	m_bCursorVisible= TRUE;
	m_nCursorType	= cursRect;
	m_nCursorDraw	= cursDrNor;
	m_cCursorColor	= RGB(255, 255, 255);
	m_nCursorDevWd	= 3;
	m_nCursorDevSz	= 16;

	// def. barvy
	for (int i = 0; i < defcolCount; ++i)
	{
		m_cDefColors[i]	= defcolValues[i];
	}

	// v�pln� objektu
	m_nFillWoods	= 0;
	m_nFillNArea	= 3;
	m_nFillKeyPt	= 6;
	m_nFillSArea	= 6;

	// barvy pro zobrazen� v��ky
	m_cCountLns		= RGB(168,  84,   0);	// vrstevnice
	m_cMinSeaHg		= RGB(  0,   0,  92);		// minim�ln� v��ka mo�e
	m_cMaxSeaHg		= RGB(176, 255, 255);	// maxim�ln� v��ka mo�e
	m_cMinMonoHg	= RGB(210, 255, 210);	// minim�ln� v��ka ter�nu
	m_cMaxMonoHg	= RGB(  0,  55,   0);		// maxim�ln� v��ka ter�nu

	m_HgFrom        = MIN_SCALE_COLOR_HEIGHT;
	m_HgStep        = STD_SCALE_COLOR_STEP;
										// pole barev pro v��ku
	for (int i = 0; i < MAX_SCALE_COLOR_ITEMS; ++i)
	{
		m_cColorHg[i] = defColorHg[i];
	}
};

// serializace objektu (implementuj� potomci)
/*void CPosEdCfg::DoSerialize(CArchive& ar, DWORD nVer, void* pParams)
{
	MntSerializeString(ar,m_sName,(LPCTSTR)pParams);
	MntSerializeBOOL(ar,m_bDefault,(LPCTSTR)pParams);

	MntSerializeInt(ar,m_nMinTextSize,(LPCTSTR)pParams);

	MntSerializeInt( ar,m_nGridStyle,(LPCTSTR)pParams);
	MntSerializeColor(ar,m_nGridColor,(LPCTSTR)pParams);
	MntSerializeBOOL(ar,m_bGridXor,  (LPCTSTR)pParams);

	MntSerializeBOOL(ar,m_bCursorVisible,(LPCTSTR)pParams);
	MntSerializeInt( ar,m_nCursorType	,(LPCTSTR)pParams);
	MntSerializeInt( ar,m_nCursorDraw	,(LPCTSTR)pParams);
	MntSerializeColor(ar,m_cCursorColor	,(LPCTSTR)pParams);
	MntSerializeInt( ar,m_nCursorDevWd	,(LPCTSTR)pParams);
	MntSerializeInt( ar,m_nCursorDevSz	,(LPCTSTR)pParams);

	// def. barvy
	for(int i=0; i<defcolCount; i++)
	{
		MntSerializeColor(ar,m_cDefColors[i],(LPCTSTR)pParams);
	};
	// v�pln� objektu
	MntSerializeInt( ar,m_nFillWoods,(LPCTSTR)pParams);
	MntSerializeInt( ar,m_nFillNArea,(LPCTSTR)pParams);
	MntSerializeInt( ar,m_nFillKeyPt,(LPCTSTR)pParams);
	MntSerializeInt( ar,m_nFillSArea,(LPCTSTR)pParams);
	// barvy pro zobrazen� v��ky
	MntSerializeColor(ar,m_cCountLns,(LPCTSTR)pParams);
	MntSerializeColor(ar,m_cMinSeaHg,(LPCTSTR)pParams);
	MntSerializeColor(ar,m_cMaxSeaHg,(LPCTSTR)pParams);
	MntSerializeColor(ar,m_cMinMonoHg,(LPCTSTR)pParams);
	MntSerializeColor(ar,m_cMaxMonoHg,(LPCTSTR)pParams);
	for(i=0; i<MAX_SCALE_COLOR_ITEMS; i++)
	{
		MntSerializeColor(ar,m_cColorHg[i].m_cColor,(LPCTSTR)pParams);
	};
};*/

// kopie hodnot
/*void CPosEdCfg::CopyFrom(const CPosEdCfg* pCfg)
{
	m_sName			= pCfg->m_sName;
	m_bDefault		= pCfg->m_bDefault;

	m_nMinTextSize	= pCfg->m_nMinTextSize;
	// nastaven� m��ky
	m_nGridStyle	= pCfg->m_nGridStyle;				
	m_nGridColor	= pCfg->m_nGridColor;		
	m_bGridXor		= pCfg->m_bGridXor;

	// zobrazen� kurzoru
	m_bCursorVisible= pCfg->m_bCursorVisible;
	m_nCursorType	= pCfg->m_nCursorType;
	m_nCursorDraw	= pCfg->m_nCursorDraw;
	m_cCursorColor	= pCfg->m_cCursorColor;
	m_nCursorDevWd	= pCfg->m_nCursorDevWd;
	m_nCursorDevSz	= pCfg->m_nCursorDevSz;

	// def. barvy
	for(int i=0; i<defcolCount; i++)
	{
		m_cDefColors[i]	= pCfg->m_cDefColors[i];
	};
	// v�pln� objektu
	m_nFillWoods	= pCfg->m_nFillWoods;
	m_nFillNArea	= pCfg->m_nFillNArea;
	m_nFillKeyPt	= pCfg->m_nFillKeyPt;
	m_nFillSArea	= pCfg->m_nFillSArea;
	// barvy pro zobrazen� v��ky
	m_cCountLns		= pCfg->m_cCountLns;
	m_cMinSeaHg		= pCfg->m_cMinSeaHg;
	m_cMaxSeaHg		= pCfg->m_cMaxSeaHg;
	m_cMinMonoHg	= pCfg->m_cMinMonoHg;
	m_cMaxMonoHg	= pCfg->m_cMaxMonoHg;
	for(i=0; i<MAX_SCALE_COLOR_ITEMS; i++)
	{
		m_cColorHg[i] = pCfg->m_cColorHg[i];
	};
};
*/

static int ColorNameStringID[] = {
  IDS_DEFCOL_NAME0,
  IDS_DEFCOL_NAME1,
  IDS_DEFCOL_NAME2,
  IDS_DEFCOL_NAME3,
  IDS_DEFCOL_NAME4,
  IDS_DEFCOL_NAME5,
  IDS_DEFCOL_NAME6,
  IDS_DEFCOL_NAME7,
  IDS_DEFCOL_NAME8,
  IDS_DEFCOL_NAME9,
  IDS_DEFCOL_NAME10,
  IDS_DEFCOL_NAME11,
  IDS_DEFCOL_NAME12,
  IDS_DEFCOL_NAME13,
  IDS_DEFCOL_NAME14,
  IDS_DEFCOL_NAME15,
  IDS_DEFCOL_NAME16,
  IDS_DEFCOL_NAME17,
  IDS_DEFCOL_NAME18,
  IDS_DEFCOL_NAME19,  
 // IDS_DEFCOL_NAME22,
 // IDS_DEFCOL_NAME23
};

void CPosEdCfg::GetDefColorName(CString& sName,int nColor)
{
	ASSERT((nColor>=0)&&(nColor<defcolCount));
	VERIFY(sName.LoadString(ColorNameStringID[nColor]));
  
};

void CPosEdCfg::GetHeightColorName(CString& sString,int nIndx)
{
	ASSERT((nIndx>=0) &&(nIndx <MAX_SCALE_COLOR_ITEMS));
	float nHeight = m_cColorHg[nIndx].m_nHeightTo;
/*
	if (nHeight == 0.f)
	{
		VERIFY(sString.LoadString(IDS_ZERO_HEIGHT_COLOR));
	} else
*/
	if (nHeight > MAX_SCALE_COLOR_HEIGHT)
	{
		CString sNum;
		NumToLocalString(sNum,(double)MAX_SCALE_COLOR_HEIGHT,0);
		AfxFormatString1(sString,IDS_HEIGHT_COLOR_FROM,sNum);
	} else
	{
		CString sNum;
		NumToLocalString(sNum,(double)nHeight,0);
		AfxFormatString1(sString,IDS_HEIGHT_COLOR_TO,sNum);
	};
};

void CPosEdCfg::Flush() const
{
  g_PosEdEnvironment.FlushConfFile();
}

BEGIN_OPTIONS_MAP( CPosEdCfg )
  MNTOPT_STRING(m_sName, _T(""), "ConfigurationName")
  MNTOPT_BOOL(m_bDefault, FALSE, "Default")	
  MNTOPT_INT(m_nMinTextSize, 16, "MinTextureSizeInPixels")
  MNTOPT_INT(m_nGridStyle, 0, "GridStyle")	
  MNTOPT_COLOR(m_nGridColor,0, "GridColor")
  MNTOPT_BOOL(m_bGridXor, FALSE, "XORstyle")
  MNTOPT_BOOL(m_bCursorVisible, TRUE, "CursorIsVisible")
  MNTOPT_INT(m_nCursorType, 0, "CursorStyle")
  MNTOPT_INT(m_nCursorDraw, 0, "CursorDraw")
  MNTOPT_COLOR(m_cCursorColor,0, "CursorColor")
  MNTOPT_INT(m_nCursorDevWd, 10, "CursorDevWidth")
  MNTOPT_INT(m_nCursorDevSz, 10, "CursorDevSize")
  MNTOPT_ARRAY(m_tDefColorsOptMapEntry, "DefColors")
  MNTOPT_INT(m_nFillWoods, 0, "FillWoods")
  MNTOPT_INT(m_nFillNArea, 0, "FillNArea")
  MNTOPT_INT(m_nFillKeyPt, 0, "FillKeyPt")
  MNTOPT_INT(m_nFillSArea, 0, "FillSArea")	
  MNTOPT_COLOR(m_cCountLns,0, "CountLnsColor")
  MNTOPT_COLOR(m_cMinSeaHg,0, "MinSeaHgColor")
  MNTOPT_COLOR(m_cMaxSeaHg,0, "MaxSeaHgColor")
  MNTOPT_COLOR(m_cMinMonoHg,0, "MinMonoHgColor")
  MNTOPT_COLOR(m_cMaxMonoHg,0, "MaxMonoHgColor")	
  MNTOPT_FLOAT(m_HgFrom,0, "HgFrom")
  MNTOPT_FLOAT(m_HgStep,20, "HgStep")
  MNTOPT_ARRAY(m_tColorHgOptMapEntry, "HeightColors")
END_OPTIONS_MAP()

/////////////////////////////////////////////////////////////////////////////
// Spr�vce konfigurac�



CFMntOptions * NewPosEdCfgObj()
{
    return (CFMntOptions *) new CPosEdCfg();
}

/////////////////////////////////////////////////////////////////////////////
// 

IMPLEMENT_DYNAMIC(CObjArrayMntOpt,CFMntOptions)

BOOL CObjArrayMntOpt::LoadParams(BOOL /*bReportErr = TRUE*/)
{
    

    if (m_pParentClass.IsNull())
    {
        return FALSE;
    }

    ParamEntryPtr pEntry = m_pParentClass->FindEntry(m_cClassName);
    if (pEntry.IsNull() || !pEntry->IsClass())
    {        
        return FALSE;
    }

    ParamClassPtr pClass = pEntry->GetClassInterface();

    char pszTemp[256];

    for(int i = 0;TRUE;i++)
    {
        sprintf(pszTemp,"%s_%d",_pszObjClassName.Data(), i);
        
        pEntry = pClass->FindEntry(pszTemp);
        if (pEntry.IsNull())
        {
            // everything is loaded
            break;
        }

        CFMntOptions * pNew = _fcnNewObject();
        
        pNew->SetInOutParamFile(pszTemp, pClass);
        pNew->LoadParams();
        pNew->ReleaseInOutParamFile();

        _pObjArray->Add(pNew);
    }

    return TRUE;
}

BOOL CObjArrayMntOpt::SaveParams(BOOL /*bReportErr = TRUE*/)
{    
    if (m_pParentClass.IsNull())
        return FALSE;

    m_pParentClass->Delete(m_cClassName);    

    ParamClassPtr pClass = m_pParentClass->AddClass(m_cClassName);    

    char pszTemp[256];

    for(int i = 0;i < _pObjArray->GetSize();i++)
    {
        sprintf(pszTemp,"%s_%d",_pszObjClassName.Data(), i);
        CFMntOptions * pObj = dynamic_cast<CFMntOptions *>(_pObjArray->GetAt(i));
        if (pObj != NULL)
        {            
            pObj->SetInOutParamFile(pszTemp, pClass);
            pObj->SaveParams(); 
            pObj->ReleaseInOutParamFile();
        }
    }

    return TRUE;
}
/////////////////////////////////////////////////////////////////////////////
// 

//IMPLEMENT_DYNAMIC(SScaleItem,CFMntOptions)

BEGIN_OPTIONS_MAP( SScaleItem )
	MNTOPT_FLOAT( m_nHeightTo,0,"HeightTo")
    MNTOPT_COLOR( m_cColor, 0x0, "Color")
END_OPTIONS_MAP()

