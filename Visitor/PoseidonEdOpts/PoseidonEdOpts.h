/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/


#if !defined(_POSEDOPTS_MAIN_H_)
#define _POSEDOPTS_MAIN_H_

#include <El\ParamFile\paramFile.hpp>
#include <El\Math\math3d.hpp>

/////////////////////////////////////////////////////////////////////////////
//  PoseidonEdOpts.h : header file
//	resource >> 10000


/////////////////////////////////////////////////////////////////////////////
// definice sou�adnic
typedef struct
{
	int			x;
	int			z;
}	UNITPOS;					// jednotkov� sou�adnice

TypeIsSimple(UNITPOS);

typedef struct _REALPOS
{
	float x;
	float z;

	_REALPOS(float xx, float zz) 
	 : x(xx)
	 , z(zz) 
	{
	}

	_REALPOS() 
	{
	}

} REALPOS;					// re�ln� sou�adnice

typedef float	LANDHEIGHT;		// re�ln� v��ka krajiny

// poloha s�t�
struct REALNETPOS
{
	REALPOS nPos;			// po��te�n� bod (A)

	// new code
	//*****************
	float   nGra;
	//*****************

	// old code
	//*****************
	//int     nGra;			// �hel (sm�r s�t�) je to uhel ve smeru hodinovych rucicek od kladnej osi Z v reqalnych suradniciach,
	//*****************
	float   nHgt;			// v��ka v bod� (absolutn�)
};

// vektor - poseidon
typedef Vector3 Vector3; 
// poloha objektu v log. sou�adnic�ch view
//#define OBJPOS_PTCOUNT		4

typedef AutoArray<POINT> OBJPOSITION;	


/////////////////////////////////////////////////////////////////////////////
// parametry sv�ta
#define MIN_WORLD_SIZE			16
#define MAX_WORLD_SIZE			4096

#define MIN_CURSOR_SIZE			5
#define MAX_CURSOR_SIZE			50

#define MIN_REAL_SIZE			1
#define MAX_REAL_SIZE			1000

#define MIN_SATGRID_SIZE			1
#define MAX_SATGRID_SIZE			10000

class CPosEdCfg;
class CDrawPar;

// definice �ablon pro pole a mapy objekt�
typedef	CArray<float,float>					CFltArray;
typedef	CArray<RECT,RECT>					CRectArray;

typedef CMap<int,int,CObject*,CObject*&>	CMapIntToObj;		
typedef CMap<int,int,CString, CString&> 	CMapIntToStr;		

#define MIN_SCALE_COLOR_HEIGHT	-1250.f
#define MAX_SCALE_COLOR_HEIGHT	 2500.f
#define STD_SCALE_COLOR_STEP	 50.f
#define ZERO_SCALE_COLOR_HEIGHT	 25		// index 0 hladiny

#define MAX_SCALE_COLOR_ITEMS  (((int)(MAX_SCALE_COLOR_HEIGHT/STD_SCALE_COLOR_STEP))+2+ZERO_SCALE_COLOR_HEIGHT)

// Eroze krajiny
// default values, value range
typedef struct washParams
{
    int    nGens;		// 32 (0-128);
    double washCoef;		// 0.05 (0-1)
    double sedimCoef;	// 80 (50-300)
    double sedimBase;	// 0.1 (0.0 - 1.0)
    double sedimEff;		// 0.9 (0-1)
    double initRain;		// 0.1 (0-1)
    double steadyRain;   // 0.05 (0-1)
    BOOL   finalBlur;	// true
}	WashParams;

struct SScaleItemDef // for default values
{
	float		m_nHeightTo;
	COLORREF	m_cColor;
};
	
struct SScaleItem : public CFMntOptions // class with support CFMntOptions
{
	float		m_nHeightTo;
	COLORREF	m_cColor;

    void operator=(const SScaleItemDef& item) {m_nHeightTo = item.m_nHeightTo; m_cColor = item.m_cColor;};
    void operator=(const SScaleItem& item) {m_nHeightTo = item.m_nHeightTo; m_cColor = item.m_cColor; CFMntOptions::operator=(item);};

    DECLARE_OPTIONS_MAP(2)    
};

// parametry pro generov�n� s�t�
typedef struct
{
	double		nSamePosTol;		// tolerance pro pova�ov�n� bodu za stejn� (v m)
	double		nSamePosHgt;		// tolerance pro pova�ov�n� v��ky za stejnou (v m)
	int			nSameGraTol;		// tolerance pro povazovan� �hlu za stejn� (ve �)

	//LONG		nCountLimit;		// limit po�tu d�l�
	LONG		nVarisLimit;		// limit po�tu variant (OpenKnots)

	//double		nHeightStep;		// krok testovan� v��ky pro generov�n� uzl�

	// cena typu d�lu
	//LONG		nSTRA[3];			// cena za rovinku
	//LONG		nBEND[4];			// cena za zat��ku

	// ocen�n� nastaven� d�lu
	//LONG		nPricePrvSl;		// cena za sklon k p�edchoz�mu d�lu (cena za kazdy 0.1�)
	//LONG		nPriceSlope;		// cena za sklon d�lu	(cena za kazdy 0.1�)
	//LONG		nPriceStand;		// cena za odchylku od standardn�ho vyno�en� (cena za 0.1 m)

	// ocen�n� trati
	//LONG		nLenPrice;			// cena za jeden metr trati
	//LONG		nBendPrice;			// cena za % (0-100x) zat��ek

	// odhad ceny trati
	//LONG		nDodLenPrice;		// cena za 1 m vzdu�n� vzd�lenosti do c�le
	//LONG		nDodKoefGrad;		// cena za odchylku �hlu  (za 1�) se �tvercem vzd�lenosti do c�le (0-1x)
	//LONG		nDodKoefSlop;		// cena za odchylku sklonu (za 0.1�) se �tvercem vzd�lenosti do c�le (0-1x)
	//LONG		nDodKoefHght;		// cena za odchylku v��ky (za 0.1m) se �tvercem vzd�lenosti do c�le (0-1x)
	//LONG		nDodIdealDif;		// cena odchylky s�t� od ide�ln� p��mky (za 1 m)

	// p�ed�van� parametry !!!
	// nejsou modifikov�ny, slou�� pouze pro p�ed�v�n� do d�l��ch funkce
	REALNETPOS	nBgnPosition;
	REALNETPOS	nEndPosition;
	double		nMinLength;			// min. d�lka trati (|nBgnPosition,nEndPosition|)
	double		nBgnSlope;			// po��te�n� sklon trati
	double		nEndSlope;			// kone�n� sklon trati

  double fBaseHeuristicCoef; 
  BOOL bFitEndAngle; //Solution must have end angle defined in nEndPosition.nGra
  BOOL bAvoidHills; // Try to avoid hills
  double fAvoidHillsCoef;

  BOOL bPunishCompChange; // Punish if component is changed
  double fPunishCompChangeCoef;

  BOOL bMaxSlope;  // Maximal allowed road slope
  int nMaxSlope;

  BOOL bMaxSlopeChange; // MAximal change in slope between two roads. 
  int nMaxSlopeChange;
}	AutoNetGenParams;

// parametry pro nastaven� v��ky s�t�
typedef struct
{
	double		nHeightStep;		// krok testovan� v��ky pro generov�n� uzl�

	// ocen�n� chyb
	LONG		nErrBadPrvHg;		// nenavazuje na p�edchoz� d�l
	LONG		nErrBadNetUp;		// s� "plave" na povrchem
	LONG		nErrBadNetDn;		// si� "zabo�ena" pod povrch
	LONG		nErrBadSlope;		// �patn� sklon d�lu	(cena za kazdy 0.1� navic)
	LONG		nErrBadPrvSl;		// �patn� sklon k p�edchoz�mu d�lu (cena za kazdy 0.1� navic)
	// ocen�n� nastaven� d�lu
	LONG		nPricePrvSl;		// cena za sklon k p�edchoz�mu d�lu (cena za kazdy 0.1�)
	LONG		nPriceSlope;		// cena za sklon d�lu	(cena za kazdy 0.1�)
	LONG		nPriceStand;		// cena za odchylku od standardn�ho vyno�en� (cena za 0.1 m)

}	AutoNetHgParams;


/////////////////////////////////////////////////////////////////////////////
// Funkce - tools (MoreTools.cpp)

/*AFX_EXT_API*/	void DoSerializeStringArray(CArchive& ar, DWORD nVer, CStringArray&);

/////////////////////////////////////////////////////////////////////////////
// Options syst�mu a aplikace (Options.cpp)

// Options syst�mu
class /*AFX_EXT_CLASS*/ CSystemOptions : public CFMntOptions
{
public:			// constructor & destructor
				CSystemOptions() {};

protected:
    //virtual ParamClass * GetParentParamClass() const {return m_pParentClass;};
    //virtual const RStringB& GetClassName() const {return m_sClassName;};
    virtual void Flush() const ;

// data options
public:
    RStringB    m_sClassName;       // Name of the class in paramfile
    ParamClass* m_pParentClass;     // ParentClass in paramfile

	/*CString		m_sPosEdDirectory;	// adres�� pro editor
	CString		m_sOptionsDirectory;// adres�� pro options*/

	CString		m_sPreviewAppPath;	// working dir for preview app
  CString   m_sPreviewAppCommand; // command launching working preview app

	/*CString		m_sTexturesPath;	// cesta k textur�m*/
	CString		m_sObjNaturPath;	// cesta k obj. p��rodn�m
	CString		m_sObjPeoplPath;	// cesta k obj. lidsk�m

	CString		m_sObjNewRoadsPath;	

  CString   m_sNetPath;  //cesta k obj se silnicema
	//CString		m_sObjOtherPath;	// cesta k les�m a s�t�m*/
	CString		m_sObjWorldPath;	// cesta k sv�t�m
  CString   m_sScriptPath;    // cesta k scriptum

	
	/*CString		m_sBankMapsPath;	// cesta k mask�m p�ehu
	CString		m_sBankTexture;		// textura pro mix. b�ehu a mo�e*/

	//int			m_nStdWorldWidth;	// standarn� rozm�ry sv�ta
	//int			m_nStdWorldHeight;
	//BOOL		m_bStdSizeOnly;		// pou��t preview pouze na std. rom�ry

	//int			m_nRealSizeUnit;	// re�ln� ���ka unitu v m
  CString   m_sccServerName;
  CString   m_sccProjectName;
  CString   m_sccLocalPath;


	// nastaven� datab�ze
	DECLARE_OPTIONS_MAP(10);
};

#define MAX_RECENT_FILES	4

typedef struct 
{
	BOOL m_bNatureObjcs;	// p��rodn� objekty
	BOOL m_bPeopleObjcs;	// lidsk� objekty

	BOOL m_bNewRoadsObjcs;

	BOOL m_bUnknowObjcs;	// neur�en� objekty

	BOOL m_bWoods;	// lesy
	BOOL m_bNets;	// s�te  

	BOOL m_bKeyPt;
	BOOL m_bNameArea;
	BOOL m_bBGImage;
} EnabledObjects;

class /*AFX_EXT_CLASS*/ CViewStyle : public CFMntOptions
{
DECLARE_DYNAMIC(CViewStyle)
public:
	CViewStyle();
	virtual ~CViewStyle();

protected:
  virtual void Flush() const;

			//void	Deafult();		// standardn� hodnoty
			//void	CopyFrom(const CViewStyle*);

			//void	Serialize(CArchive& ar);
  

// parametry zobrazen�
public:
	EnabledObjects GetEnableObjects() const;

	enum 
	{	
		tvsGround   = 0,	// mo�e/pevina
		tvsLands    = 1,	// kategorie textur
		tvsZones    = 2,	// kategorie textur
		tvsTextur   = 3,	// prim�rn� textury
		tvsHgColor  = 4,	// v��ka barevnou �k�lou
		tvsHgMono   = 5,	// v��ka monochromaticky
		tvsHgFromTo = 6,//  
	};

	int m_nBaseStyle;	// z�kladn� styl - tvs???

	BOOL m_bShowCountLn;	// zobrazen� vrstevnic
	BOOL m_bShowGrid;	// zobrazen� m��ky
	BOOL m_bShowScndTxtr;// pou��vat sekund�rn� textury
	BOOL m_bShowSea;		// zobrazovat mo�e

	BOOL m_bNatureObjcs;	// p��rodn� objekty
	BOOL m_bPeopleObjcs;	// lidsk� objekty

	BOOL m_bNewRoadsObjcs;	

	BOOL m_bUnknowObjcs;	// neur�en� objekty

	BOOL m_bShowWoods;	// zobrazen� les�
	BOOL m_bShowNets;	// zobrazen� s�t�

	BOOL m_bAreasBorder;	// hranice pojmenovan�ch oblast�
	BOOL m_bAreasEntire;	// plochy pojmenovan�ch oblast�

	BOOL m_bKeyPtBorder;	// hranice kl��ov�ch bod�
	BOOL m_bKeyPtEntire;	// plochy kl��ov�ch bod�

	BOOL m_bShowRuler; //show rulers..
	BOOL m_bShowBGImage; //show background image

	BOOL m_bWaterLayer; //show water layer
	BOOL m_Shadowing;// show shadows on map
	BOOL m_ShowLock; // show locks by grayscale color

	DECLARE_OPTIONS_MAP(21);
};

class /*AFX_EXT_CLASS*/ CPosEdOptions : public CFMntOptions
{
public:				
	// constructor & destructor
	CPosEdOptions();
	~CPosEdOptions();

	// nastaven� def. hodnot
	//virtual void	SetDefaultValues();
	// kopie hodnot
	//virtual void	CopyFrom(CFMntOptions*);
	// update hodnot do souboru (funkce nesm� b�t vol�na p��mo)
	//virtual	void	DoSerialize(CArchive& ar, DWORD nVer, void* pParams);

protected:
    virtual void Flush() const;

// data options
public:
	BOOL		m_bMaximizeApp;
	int			m_nMoveWindowCoef;
	double		m_nInOutScaleStep;

	// posledn� stav aplikace
	BOOL		m_bLastMaximize;
	int			m_nLastXPos;		// velikost okna aplikace p�i ukon�en�
	int			m_nLastYPos;
	int			m_nLastCXPos;
	int			m_nLastCYPos;

	// recent files
	CString		m_sRecentFile[MAX_RECENT_FILES];

	// z�kladn� rozm�ry dokumentu
	int			m_nBaseWorldWidth;
	int			m_nBaseWorldHeight;

	// form�t p�echodov� textury
	enum 
	{ 
		txtrPAC = 0, 
		txtrPAA = 1 
	};

	int			m_nTextureFormat;

	// okno projektu - po��te�n� stav
	BOOL		m_bMaxProjWnd;
	BOOL		m_bFitProjWnd;
	int			m_nProjScale;
	
	CViewStyle	m_ViewStyle;

	// Buldozer - varianty komunikace
	enum 
	{  
		upBlCurNone = 0,	// - kurzor z buldozeru se nep�esouv�
		upBlCurGoto = 1,	// - p�i p�echodu na cursor
		upBlCurFull = 2,    // - v�dy - akceptuji ka�dou ud�lost
	};	

	//int			m_nBldCursorUpdate;	// zp�sob update kurzoru
	//BOOL		m_bBldSynchrAction;	// p�i akc�ch v Editoru synchr. kurzor Buldozeru

	// eroze krajiny
	WashParams			m_WashParams;
	// parametry pro generov�n� s�t�
	AutoNetGenParams	m_NetGenParams;
	// parametry pro nastaven� v��ky s�t�
	AutoNetHgParams		m_NetHgtParams;

	DECLARE_OPTIONS_MAP(29+23+7+1);
public:
	// data, kter� nejsou ukl�d�na
};

/////////////////////////////////////////////////////////////////////////////
// Konfigurace (Configuration.cpp)

#define CFG_FILE_VERSION	2

class /*AFX_EXT_CLASS*/ CPosEdCfg : public CFMntOptions
{
	DECLARE_DYNAMIC(CPosEdCfg)
public:
	// constructor
	CPosEdCfg();
	~CPosEdCfg();

	// nastaven� def. hodnot
	virtual void SetDefaultValues();
	virtual void Flush() const;
	
// data konfigurace
public:
	CString		m_sName;			// jm�no konfigurace
	BOOL		m_bDefault;			// jde o def. konfiguraci

	//LOGFONT		m_CfgDefFont;		// def. text

	// parametry konfigurace
	int			m_nMinTextSize;		// min. velikost textury v pixlech

	enum 
	{ 
		gridStPoint  = 0, 
		gridStDottLn = 1, 
		gridStFullLn = 2, 
	};

	int			m_nGridStyle;		// styl m��ky (bod,te�ky,��ra)
	COLORREF	m_nGridColor;		// barva m��ky
	BOOL		m_bGridXor;			// XOR styl

	// zobrazen� kurzoru
	BOOL		m_bCursorVisible;	// kurzor je vid�t
	
	enum 
	{ 
		cursCross = 0, 
		cursRect  = 1, 
		cursFull  = 2 
	};

	int			m_nCursorType;

	enum 
	{ 
		cursDrNor = 0, 
		cursDrXor = 1, 
		cursDrInv = 2 
	};

	int			m_nCursorDraw;
	COLORREF	m_cCursorColor;		// barva kurzoru
	int			m_nCursorDevWd;		// velikost ��ry kurzoru v dev. sou�adnic�ch
	int			m_nCursorDevSz;		// velikost kurzoru v dev. sou�adnic�ch

	enum 
	{ 
		defcolSea     =  0,		// mo�e
		defcolGround  =  1,		// pevnina
		defcolTextur  =  2,		// textura (neident.)
		defcolUObFrm  =  3,		// obrys nedef. objektu
		defcolUObEnt  =  4,		// v�pl� nedef. objektu
		defcolNObFrm  =  5,		// obrys p�ir. objektu
		defcolNObEnt  =  6,		// v�pl� p�ir. objektu
		defcolPObFrm  =  7,		// obrys lids. objektu
		defcolPObEnt  =  8,		// v�pl� lids. objektu
		defcolNetKey  =  9,		// s�t� - kl��ov�
		defcolNetNor  = 10,		// s�t� - norm�ln�
		defcolNetCrs  = 11,		// s�t� - k�i�ovatka
		defcolWoods   = 12,		// les�
		defcolFWood   = 13,		// r�m - les�
		defcolNArea   = 14,		// pojm. plocha
		defcolFNAre   = 15,		// r�m - pojm. plocha
		defcolKeyPt   = 16,		// kl��ov� bod
		defcolFKeyP   = 17,		// r�m - kl��ov� bod
		defcolAreaE   = 18,		// vybran� plocha
		defcolAreaF   = 19,		// vybran� plocha - r�m

		defcolNRObFrm = 20,     // new roads
		defcolNRObEnt = 21,     // new roads

		defcolCount   = 22		// po�et def. barev
	};

	COLORREF	m_cDefColors[defcolCount];
	static void GetDefColorName(CString&, int nColor);        
    FMT_OPTMAP_ENTRY_ARRAY m_tDefColorsOptMapEntry;

	// v�pln� objektu
	int			m_nFillWoods;
	int			m_nFillNArea;
	int			m_nFillKeyPt;
	int			m_nFillSArea;

	// barvy pro zobrazen� v��ky
	COLORREF	m_cCountLns;		// vrstevnice
	COLORREF	m_cMinSeaHg;		// minim�ln� v��ka mo�e
	COLORREF	m_cMaxSeaHg;		// maxim�ln� v��ka mo�e
	COLORREF	m_cMinMonoHg;		// minim�ln� v��ka ter�nu
	COLORREF	m_cMaxMonoHg;		// maxim�ln� v��ka ter�nu

	float m_HgFrom;           // draw countlines etc from
	float m_HgStep;             // draw countlines etc to
									// pole barev pro v��ku
	SScaleItem  m_cColorHg[MAX_SCALE_COLOR_ITEMS];
    FMT_OPTMAP_ENTRY_ARRAY m_tColorHgOptMapEntry;

	void GetHeightColorName(CString&,int nIndx);

    DECLARE_OPTIONS_MAP(14+1+4+6+1);
};


//////////////////////////////////////////////////////////////////////////////////
// Auxiliary class. It implements serialization through CFMntOptions for CObArray
typedef CFMntOptions * (* NEWOBJECT)();

class CObjArrayMntOpt : public CFMntOptions
{
    DECLARE_DYNAMIC(CObjArrayMntOpt)
protected:
    CObArray * _pObjArray;
    RString _pszObjClassName;
    NEWOBJECT _fcnNewObject;

public:
    CObjArrayMntOpt(CObArray * pObjArray, const LPCSTR pszObjClassName, NEWOBJECT fcnNewObject)
      : _pObjArray(pObjArray),
        _pszObjClassName(pszObjClassName), 
        _fcnNewObject(fcnNewObject) {};

    virtual BOOL LoadParams(BOOL bReportErr = TRUE);
    virtual BOOL SaveParams(BOOL bReportErr = TRUE);
};
/////////////////////////////////////////////////////////////////////////////
// DockState serialization into paramfile
class CPosDockState : public CFMntDockState
{
  DECLARE_DYNAMIC(CPosDockState)
public:
  CSize m_objectPanelSize;
  CSize m_scriptPanelSize;
  CSize m_namSelPanelSize;
public:
  virtual BOOL	LoadParams(BOOL bReportErr = TRUE);
  virtual BOOL	SaveParams(BOOL bReportErr = TRUE); 
  virtual void Flush() const;
};
/////////////////////////////////////////////////////////////////////////////
// Prost�ed� aplikace (Environment.cpp)

class /*AFX_EXT_CLASS*/ CPosEdEnvironment : public CObject
{
	DECLARE_DYNAMIC(CPosEdEnvironment)

protected:
	ParamFile m_cConfFile;
	RString m_szConfFileName;

public:
	// constructor
	CPosEdEnvironment();
	~CPosEdEnvironment();

	// inicializace prost�ed� (dle typu programu)
	BOOL	DoInitEnvironment(const TCHAR * pszConfFileName);
	void    DoDoneEnvironment();

	// �prava glob�ln�ch dat z�vosl�ch na konfiguraci
	void	OnChangeCurrentConfig();


  // p��stup k adres���m
  /*enum {
  dirPosEd = 0, dirOpts = 1,
  dirObjTxt= 2,	// textury
  dirObjNat= 3,	// p��roda
  dirObjPeo= 4,	// lid�
  dirObjNet= 5,	// s�t�
  dirObjWoo= 6,	// lesy
  dirObjWrd= 7,	// sv�t exp/imp
  };*/

  //void	GetPoseidonDirectory(CString& sDir, int nDir) const;

	// p��stup k soubor�m
	void GetPoseidonFile(CString& sFile, int nFile) const;

	void FlushConfFile();

	  // parametry
public:
	// options syst�mu a aplikac�
	CSystemOptions m_optSystem;	// parametry syst�mu
	CPosEdOptions  m_optPosEd;		// parametry aplikace

	CPosEdCfg     m_cfgCurrent;	// aktu�ln�  konfigurace
	CPosDockState m_appDockState;		// stav docbars
  //CPosEdCfgManager	m_cfgPosEdMgr;	// konfigurace
  
	// re�ln� dokument
	CDocument* m_pRealDoc;
};

/////////////////////////////////////////////////////////////////////////////
// UpdateAllViews parametry (UpdatePar.cpp)

class /*AFX_EXT_CLASS*/ CUpdatePar : public CFMntScrollUpdate
{
DECLARE_DYNAMIC(CUpdatePar)
public:
				// constructor
				CUpdatePar(DWORD nType,CView* pView = NULL);
				CUpdatePar(CAction*);


// p��znaky pro update
	enum {
		uvfCloseAll		= 0x00010000,	// zav��t v�echna okna
		uvfCloseView	= 0x00010001,	// zav�en� okna m_pView
    uvfResetView = 0x00010002,  // document was reloaded, reset view

		uvfChngOpts		= 0x00010005,	// zm�na options programu
		uvfChngConfig	= 0x00010006,	// zm�na aktu�ln� konfigurace

		uvfSaveDoc		= 0x00010008,	// dokument byl ulo�en

		uvfChngCursor	= 0x00010010,	// zm�na polohy kurzoru
		uvfVisiCursor	= 0x00010011,	// zm�na viditelnosti kurzoru
		
		// proveden� akce dokumentu
		uvfRealizeAction= 0x00020001,	// provedena akce
    uvfBuldConnect = 0x00020002, // Buldozer se pripojil k documentu
    uvfBuldDisconnect = 0x00020003, // Buldozer se odpojil od documentu
    uvfEditMode = 0x00020005, // Edit mode changed
    uvfChngTextLayer = 0x00020006, // Edit mode changed

    // selection
    uvfSelectionObjects = 0x00030007,
	};

// data informuj�c� o ud�losti
public:
	BOOL		m_bDocUpdate;	// TRUE -> m� se vola UpdateDocument()
	CAction*	m_pAction;		// realizov�na akce dokumentu
};

/////////////////////////////////////////////////////////////////////////////
// CDrawPar - parametry vykreslov�n� (DrawPar.cpp)

#define DRAW_TEMP_PARAMS	2	// po�et temp. parametr�

class /*AFX_EXT_CLASS*/ CDrawPar : public CObject
{
DECLARE_DYNAMIC(CDrawPar)
public:
			// constructor
			CDrawPar();
			~CDrawPar();

			// parametry pro vykreslov�n� objekt�
	void	CreateTempObjects();
	void	DestroyTempObjects();
			// parametry pro vykreslov�n� pozad�
	void	CreateTempBackground();
	void	DestroyTempBackground();

// parametry
public:
	CPosEdEnvironment*		m_pEnvir;	// prost�ed�
	CPosEdCfg*				m_pCfg;		// konfigurace pro kreslen�
	CViewStyle				m_VwStyle;	// styl zobrazen�
	
	// aktu�ln� vykreslovan� dokument
	CDocument*				m_pDocument;

	// temp. parametry pro vykreslov�n� objekt�
	enum { stdObjectCount = 3,
	stdObjectNone	= 0, stdObjectNatur	= 1, stdObjectPeople= 2 };
	
	COLORREF				m_cStdObjFrame[stdObjectCount];
	COLORREF				m_cStdObjEntir[stdObjectCount];
	CPen*					m_pStdObjPen[stdObjectCount];
	CBrush*					m_pStdObjBrush[stdObjectCount];

	// temp. parametry pro vykreslov�n� pozad�
	COLORREF				m_cStdSeaColor;	// mo�e z cfg
	COLORREF				m_cStdGndColor; // zem� z cfg

	COLORREF				m_cRegSeaColor;	// barva 0 textury
	COLORREF				m_cUndefLColor;	// nedefinovan� krajina/p�smo

	double					m_nStpCountLn;	// krok vrstevnic
	COLORREF				m_cCountLnColor;// barva vrstevnic
	SScaleItem				m_cColorHg[MAX_SCALE_COLOR_ITEMS];
};

/////////////////////////////////////////////////////////////////////////////
// Inicializace syst�mu & glob�ln� data (PosEdOptsDll.cpp)

/*AFX_EXT_API*/	BOOL	DoInitPosEdSystem(LPCTSTR confFileName);
/*AFX_EXT_API*/	BOOL	DoDonePosEdSystem();

// aktu�ln� prost�ed� aplikace
/*AFX_EXT_API*/ CPosEdEnvironment* GetPosEdEnvironment();


/////////////////////////////////////////////////////////////////////////////
#endif // !defined(_POSEDOPTS_MAIN_H_)

