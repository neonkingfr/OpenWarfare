/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// 

void DoSerializeStringArray(CArchive& ar, DWORD nVer, CStringArray& Array)
{
	if (!ar.IsStoring())
		Array.RemoveAll();
	// po�et prvk�
	int nCount = Array.GetSize();
	MntSerializeInt(ar,nCount);
	// jednotliv� �et�zce
	for(int i=0;i<nCount;i++)
	{
		CString sText;
		if (ar.IsStoring())
		{
			sText = Array[i];
			MntSerializeString(ar,sText);
		} else
		{
			MntSerializeString(ar,sText);
			Array.Add(sText);
		};
	};
};

