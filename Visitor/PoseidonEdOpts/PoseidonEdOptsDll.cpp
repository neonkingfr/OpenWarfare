/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
//#include "afximpl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CPosEdEnvironment	g_PosEdEnvironment;

/////////////////////////////////////////////////////////////////////////////
// Inicializace syst�mu & glob�ln� data

BOOL DoInitPosEdSystem(LPCTSTR confFileName)
{	
	if (!g_PosEdEnvironment.DoInitEnvironment(confFileName))
		return FALSE;

	// v�e prob�hlo OK
	return TRUE;
};

BOOL DoDonePosEdSystem()
{	
	// nevol�m pLib->DoneLibrary() !!!
	// nech� se a� na destructoru, aby bylo mo�n� pou��vat
	// options p�i ukon�en� aplikace

	// v�e prob�hlo OK
    g_PosEdEnvironment.DoDoneEnvironment();
	return TRUE;
};

CPosEdEnvironment* GetPosEdEnvironment()
{
	return &g_PosEdEnvironment;
};

