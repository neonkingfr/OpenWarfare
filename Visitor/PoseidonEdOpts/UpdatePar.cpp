/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UpdateAllViews parametry

IMPLEMENT_DYNAMIC(CUpdatePar,CFMntScrollUpdate)

// constructor
CUpdatePar::CUpdatePar(DWORD nType,CView* pView):
	CFMntScrollUpdate(nType,pView)
{
	m_bDocUpdate = FALSE;
	m_pAction	 = NULL;
};

CUpdatePar::CUpdatePar(CAction* pAction):
	CFMntScrollUpdate(CUpdatePar::uvfRealizeAction,NULL)
{
	m_bDocUpdate = FALSE;
	m_pAction	 = pAction;
};

