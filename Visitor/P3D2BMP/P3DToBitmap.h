#pragma once

#include <ObjectData.h>
using namespace ObjektivLib;

struct P3DToBitmapParams
{
  Pathname _basePath;
  Pathname _objectName;
  float _level; 
  float _zenit; ///<in degrees
  float _azimut;///<in degrees
  int _steps; 
  int _xres;
  int _yres;
  Pathname _output;
  int _masklevel;
  unsigned long _maskcolor;
  bool _paletted;
  bool _rgb24;
  bool _groundcenter;
  bool _speherebounding;
  bool _ignoreProxies;
  char _excludeSelection[512];
  bool _topDown;
  P3DToBitmapParams():_level(0),_azimut(0),_zenit(90),_steps(0),_basePath(PathNull),_objectName(PathNull),_xres(64),_yres(64),_output(PathNull),
  _masklevel(0x80),_maskcolor(0xFF00FF),_paletted(false),_rgb24(false),_groundcenter(false),_speherebounding(false) 
  , _topDown(false)
  {
    _excludeSelection[0]=0;
  }
};

struct TextureData
{
  RStringB _textureName;
  ComRef<IDirect3DTexture9> _texture;
  TextureData(const char *name):_textureName(name) {}
  bool LoadTexture(IDirect3DDevice9 *device, const char *filename);
  TextureData() {}
};

struct ModelInfo
{
  Vector3 center;
  float radius;
  float maxSide;
  ModelInfo(ObjectData *object, bool ground, bool spehered);
  ModelInfo() {}
};

TypeIsMovable(TextureData);

class P3DToBitmap
{
public:
  P3DToBitmap(void);
  ~P3DToBitmap(void);

enum Result
{
  Ok,GenError,UnableToCreateRenderTarget, UnableToLoadObject, VertexBufferCreationFailed,
  UnableToLockResult,UnableToCreateOutputFile, UnabletoWriteOutputFile
};

struct RenderInfo
{
  DrawingDevice *device;
  int width;
  int height;
  ObjectData *object;
  RGBQUAD *output;
  ModelInfo *modelInfo;
  float azimut;
  float zenit;
  bool topDown;
  AutoArray<TextureData> *textureData;
};

static ObjectData *LoadP3DLevel(Pathname &name, float level, const char *basePath, int recursion, bool ignoreProxies, P3DToBitmapParams* params);

static Result Run(P3DToBitmapParams &params, DrawingDevice &device);
static Result RenderBitmap(RenderInfo &renderInfo);
static void RemoveUnwantedSelections(ObjectData* object, P3DToBitmapParams &params);

};
