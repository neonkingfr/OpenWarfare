#include "StdAfx.h"
#include "DrawingDevice.h"
#include <ObjectData.h>
#include <ObjToolProxy.h>
#include <ObjToolsMatLib.h>
#include ".\p3dtobitmap.h"
#include <fstream>
using namespace std;

ModelInfo::ModelInfo(ObjectData *object, bool ground, bool spehered)
{
  Vector3 min,max;

  if (object->NPoints()==0) 
  {
    center=Vector3(0,0,0);
    radius=1.0f;
  }
  else
  {
    min=object->Point(0);
    max=object->Point(0);
    for (int i=1;i<object->NPoints();i++)
    {
      PosT pos=object->Point(i);
      if (pos.flags & POINT_SPECIAL_HIDDEN) continue;
      if (pos[0]<min[0]) min[0]=pos[0];
      if (pos[1]<min[1]) min[1]=pos[1];
      if (pos[2]<min[2]) min[2]=pos[2];
      if (pos[0]>max[0]) max[0]=pos[0];
      if (pos[1]>max[1]) max[1]=pos[1];
      if (pos[2]>max[2]) max[2]=pos[2];      
    }
    center=(min+max)/2;
    if (ground) center[1]=0.0f;
    radius=0;    
    for (int i=1;i<object->NPoints();i++)
    {
      PosT pos=object->Point(i);
      if (pos.flags & POINT_SPECIAL_HIDDEN) continue;
      Vector3 diff=(pos-center);
      if (!spehered) diff[1]=0;
      float distance=diff.SquareSize();
      if (distance>radius) radius=distance;
    }

    if(max[0]-min[0] > max[2]-min[2])
      maxSide = max[0]-min[0];
    else
      maxSide = max[2]-min[2];

    radius=sqrt(radius);
    if (radius==0) radius=1.0f;
  }
}

P3DToBitmap::P3DToBitmap(void)
{
}

P3DToBitmap::~P3DToBitmap(void)
{
}


//ObjectData *P3DToBitmap::LoadP3DLevel(Pathname &name, float level, const char *basePath, int recursion, bool ignoreProxies)

ObjectData *P3DToBitmap::LoadP3DLevel(Pathname &name, float level, const char *basePath, int recursion, bool ignoreProxies, P3DToBitmapParams* params)
{
  for (int i=0;i<recursion;i++) printf("..");
  printf("Loading object: %s\r\n",name.GetFilename());
  LODObject lodobj;
  if (lodobj.Load(name,0,0)!=0) return 0;

  int lev=lodobj.FindLevel(level);
  if (lev==-1) return 0;

  ObjectData *found=lodobj.Level(lev);
  ObjectData *res=new ObjectData(*found);
  if(params)
	  RemoveUnwantedSelections(res, *params);
  Selection proxyToDel(res);

  if (recursion<16)
  {
    ObjToolProxy &proxyTool=res->GetTool<ObjToolProxy>();
    
    NamedSelection **proxies=(NamedSelection **)alloca(sizeof(NamedSelection *)*proxyTool.EnumProxies());
    int proxCount=proxyTool.EnumProxies(proxies);
    
    for (int i=0; i<proxCount;i++)
    {
      RString proxname;
      Pathname ldproxy(PathNull);
      proxyTool.GetProxyFilename(proxies[i]->Name(),proxname.CreateBuffer(strlen(proxies[i]->Name())));
      int face=proxyTool.FindProxyFace(proxies[i]);
      proxyToDel.FaceSelect(face);
	  if(!ignoreProxies)
	  {

		  if (proxname[0]=='\\') proxname=proxname.Data()+1;
		  proxname=proxname+".p3d";
		  if (basePath)
		  {
			ldproxy.SetDirectory(basePath);
			ldproxy.SetFilename(proxname);
		  }
		  else
		  {
			ldproxy.SetDirectory(name.GetDirectoryWithDrive());
			ldproxy.SetFilename(proxname);
			if (ldproxy.BottomTopSearchExist()==false) continue;
		  }
		  ObjectData *obj=LoadP3DLevel(ldproxy,level,basePath,recursion+1, ignoreProxies, params);
		  if (obj==0)
		  {
			for (int i=0;i<recursion;i++) printf("..");
			printf("Note: Unable to load proxy: \r\n\t%s\r\n",ldproxy.GetFullPath());
		  }
		  else if (face!=-1)
		  {
			FaceT fc(res,face);
			Matrix4 proxmx;
			proxyTool.GetProxyTransform(fc,proxmx);
			for (int i=0;i<obj->NPoints();i++)
			{
			  PosT &ps=obj->Point(i);
			  VecT vx;
			  vx.SetFastTransform(proxmx,ps);
			  ps.SetPoint(vx);
			}
			res->Merge(*obj,0,false);
			delete obj;
		  }
		}
	 }
  }
  res->SelectionDelete(&proxyToDel);
  for (int i=0;i<recursion;i++) printf("..");
  puts("ok");
  return res;
}


void P3DToBitmap::RemoveUnwantedSelections(ObjectData* object, P3DToBitmapParams &params)
{
  AutoArray <RString> RemoveSel;
  if(params._excludeSelection[0])
  {
    printf("Open Exclude file\r\n");
    FILE* file = fopen(cc_cast(params._excludeSelection), "rt");
    if(file)
    {
      printf("Opened\r\n");

      fseek( file, 0L, SEEK_SET );
      char buf[256];
      while(!feof(file))
      {
        fscanf(file, "%s\r\n", buf);
        int indx = RemoveSel.Add(buf);
      }
    }
  }


  for(int i=0; i<RemoveSel.Size(); ++i)
  {
    NamedSelection* sel = object->GetNamedSel(cc_cast(RemoveSel[i]));
    bool res = false;
    if(sel)
    {
      res = object->SelectionDeleteFaces(sel);
      printf("removed selection: %s\r\n", cc_cast(RemoveSel[i]));
    }
  }
}

P3DToBitmap::Result P3DToBitmap::Run(P3DToBitmapParams &params, DrawingDevice &device)
{

  ObjectData *object=LoadP3DLevel(params._objectName,params._level,params._basePath, 0, params._ignoreProxies, &params);
  if (object==0) return UnableToLoadObject;

  object->RecalcNormals();

  ModelInfo modelInfo(object,params._groundcenter,params._speherebounding);


  ComRef<IDirect3DSurface9> renderTarget;
  if (device->CreateRenderTarget(params._xres*2,params._yres*2,D3DFMT_A8R8G8B8,D3DMULTISAMPLE_NONE,0,TRUE,renderTarget.Init(),0)!=S_OK)
  {
    return UnableToCreateRenderTarget;
  }

  if (device->SetRenderTarget(0,renderTarget)!=S_OK)
  {
    return UnableToCreateRenderTarget;
  }
  ComRef<IDirect3DSurface9> depth;

  device->GetDepthStencilSurface(depth.Init());
  D3DSURFACE_DESC Zeroed(desc);
  depth->GetDesc(&desc);  

  if (device->CreateDepthStencilSurface(params._xres*2,params._yres*2,desc.Format,D3DMULTISAMPLE_NONE,0,TRUE,depth.Init(),0)!=S_OK)
  {
    return UnableToCreateRenderTarget;
  }
  if (device->SetDepthStencilSurface(depth)!=S_OK)
  {
    return UnableToCreateRenderTarget;
  }

  RGBQUAD *bitmapData;
  Result res=Ok;
  DWORD bitmapSize=0;

  ObjToolMatLib &matLib=object->GetTool<ObjToolMatLib>();
  BTree<ObjMatLibItem> texlib;
  matLib.ReadMatLib(texlib,ObjToolMatLib::ReadTextures);
  AutoArray<TextureData> textureList;

  BTreeIterator<ObjMatLibItem> iter(texlib);
  ObjMatLibItem *texInfo;
  while (texInfo=iter.Next())
  {
    if (texInfo->name.Data()[0]==0)
    {
      textureList.Append()=TextureData(texInfo->name);
    }
    else
    {
      Pathname search(PathNull);
      if (params._basePath.IsNull()) search=params._objectName;else search.SetDirectory(params._basePath);
      search.SetFilename(texInfo->name.Data());
      if (search.BottomTopSearchExist()==false)
      {
        textureList.Append()=TextureData(texInfo->name);
      }
      else
      {      
        TextureData txt(texInfo->name);
        printf("Loading texture: %s\r\n",search.GetFullPath());
        if (txt.LoadTexture(device.operator *(), search)==false)        
          puts("FAILED!");        
        textureList.Append()=txt;
      }
    }
  }
  if (params._steps==0)  
    bitmapSize=params._xres*params._yres;  
  else  
    bitmapSize=params._xres*params._yres*params._steps;


  puts("Rendering to bitmap...");
  bitmapData=new RGBQUAD[bitmapSize];

  RenderInfo renderInfo;
  renderInfo.device=&device;
  renderInfo.width=params._xres;
  renderInfo.height=params._yres;
  renderInfo.object=object;
  renderInfo.output=bitmapData;
  renderInfo.modelInfo=&modelInfo;
  renderInfo.azimut=params._azimut;
  renderInfo.zenit=params._zenit;
  renderInfo.textureData=&textureList;
  renderInfo.topDown = params._topDown;
  if (params._steps)
  {
    for (int i=0;i<params._steps;i++)
    {
      renderInfo.azimut=params._azimut+360*i/params._steps;
      renderInfo.output=bitmapData+params._yres*params._xres*i;
      res=RenderBitmap(renderInfo);
      if (res!=Ok) break;
    }
  }
  else
     res=RenderBitmap(renderInfo);
  
 


  if (res==Ok)
  {
    Pathname outFile=params._output;
    if (outFile.IsNull()) 
    {
      outFile=params._objectName;
      outFile.SetExtension(".bmp");
    }

    printf("Saving as '%s'\r\n",outFile.GetFullPath());
    ofstream out(outFile,ios::binary|ios::out|ios::trunc);

    if (!out)
    {
      puts("FAILED TO OPEN!");
      res=UnableToCreateOutputFile;
    }

    else
    {
      if (params._rgb24)
      { ;;
        BITMAPFILEHEADER Zeroed(file_nfo);
        file_nfo.bfType='B'+'M'*256;
        file_nfo.bfOffBits=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER);
        file_nfo.bfSize=file_nfo.bfOffBits+bitmapSize*3;
        BITMAPINFOHEADER ZeroedSz(hdr,biSize);
        hdr.biWidth=params._xres;
        hdr.biHeight=bitmapSize/params._xres;
        hdr.biPlanes=1;
        hdr.biBitCount=24;
        hdr.biCompression=BI_RGB;
        out.write(reinterpret_cast<char *>(&file_nfo),sizeof(file_nfo));
        out.write(reinterpret_cast<char *>(&hdr),sizeof(hdr));
        for (int i=hdr.biHeight;i-->0;)
        {
          RGBQUAD *line=bitmapData+hdr.biWidth*i;
          for (int j=0;j<hdr.biWidth;j++)
          {
            if (line->rgbReserved<params._masklevel)
              out.write(reinterpret_cast<char *>(&params._maskcolor),3);
            else
              out.write(reinterpret_cast<char *>(line),3);
            line++;
          }
          int padding=((((hdr.biWidth*3)+3)/4)*4)-hdr.biWidth*3;
          if (padding>0) out.write("\0\0\0\0\0\0",padding);          
        }
      }
      else if (params._paletted)
      {
        BITMAPFILEHEADER Zeroed(file_nfo);
        file_nfo.bfType='B'+'M'*256;
        file_nfo.bfOffBits=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER)+sizeof(RGBQUAD)*256;
        file_nfo.bfSize=file_nfo.bfOffBits+bitmapSize;
        BITMAPINFOHEADER ZeroedSz(hdr,biSize);
        hdr.biWidth=params._xres;
        hdr.biHeight=bitmapSize/params._xres;
        hdr.biPlanes=1;
        hdr.biBitCount=8;
        hdr.biCompression=BI_RGB;
        RGBQUAD palette[256];
        for (int i=0;i<256;i++)
        {
          palette[i].rgbRed=(i & 0xE0)+((i & 0xE0)>>3)+((i & 0xE0)>>6);
          palette[i].rgbGreen=((i & 0x1C)<<3)+((i & 0x1C))+((i & 0x1C)>>3);
          palette[i].rgbBlue=((i & 0x3)<<6)+((i & 0x3)<<4)+((i & 0x3)<<2)+((i & 0x3));


          memcpy(palette,&params._maskcolor,4);
          palette[4].rgbGreen=0;
        }
        out.write(reinterpret_cast<char *>(&file_nfo),sizeof(file_nfo));
        out.write(reinterpret_cast<char *>(&hdr),sizeof(hdr));
        out.write(reinterpret_cast<char *>(&palette),sizeof(palette));
        for (int i=hdr.biHeight;i-->0;)
        {
          RGBQUAD *line=bitmapData+hdr.biWidth*i;
          for (int j=0;j<hdr.biWidth;j++)
          {
            if (line->rgbReserved<params._masklevel)
              out.write("\0",1);
            else
            {
              unsigned char res=(line->rgbRed & 0xE0)+((line->rgbGreen & 0xE0)>>3)+((line->rgbBlue & 0xC0)>>6);
              if (res==0) res=4;
              out.write((char *)&res,1);
            }
            line++;
          }
          int padding=(((hdr.biWidth+3)/4)*4)-hdr.biWidth*3;
          if (padding>0) out.write("\0\0\0\0\0\0",padding);          
        }
      }
      else
      {
        BITMAPFILEHEADER Zeroed(file_nfo);
        file_nfo.bfType='B'+'M'*256;
        file_nfo.bfOffBits=sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFOHEADER);
        file_nfo.bfSize=file_nfo.bfOffBits+bitmapSize*sizeof(RGBQUAD);
        BITMAPINFOHEADER ZeroedSz(hdr,biSize);
        hdr.biWidth=params._xres;
        hdr.biHeight=bitmapSize/params._xres;
        hdr.biPlanes=1;
        hdr.biBitCount=32;
        hdr.biCompression=BI_RGB;
        out.write(reinterpret_cast<char *>(&file_nfo),sizeof(file_nfo));
        out.write(reinterpret_cast<char *>(&hdr),sizeof(hdr));
        for (int i=hdr.biHeight;i-->0;)
        {
          RGBQUAD *line=bitmapData+hdr.biWidth*i;
          out.write(reinterpret_cast<char *>(line),sizeof(RGBQUAD)*hdr.biWidth);
        }
      }
      if (!out)
      {
        puts("FAILED!");
        res=UnabletoWriteOutputFile;
      }
    }
    
  }

  delete [] bitmapData;
  delete object;
  return res;
}

#pragma pack(4)
struct P3DVertex
{
  float x,y,z;  
  float xn,yn,zn;
  DWORD color1;
  float u,v;
  static const DWORD FVF=D3DFVF_XYZ|D3DFVF_DIFFUSE|D3DFVF_NORMAL|D3DFVF_TEX1;
};

#pragma pack()

#define VERTEX_BUFFER_SIZE (5000*3)

P3DToBitmap::Result P3DToBitmap::RenderBitmap(RenderInfo &renderInfo)
{
  DrawingDevice &device=*renderInfo.device;
  ObjectData &object=*renderInfo.object;
  ModelInfo &modelInfo=*renderInfo.modelInfo;
  AutoArray<TextureData> &textureData=*renderInfo.textureData;

  device->Clear(0,0,D3DCLEAR_ZBUFFER|D3DCLEAR_TARGET,0,1.0f,0);
  device->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
  device->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);
  device->SetRenderState(D3DRS_CULLMODE,D3DCULL_NONE);
  device->SetRenderState(D3DRS_ZFUNC,D3DCMP_LESSEQUAL);
  device->SetRenderState(D3DRS_ALPHAREF,0x10);
  device->SetRenderState(D3DRS_ALPHAFUNC,D3DCMP_GREATEREQUAL);
  device->SetRenderState(D3DRS_ALPHABLENDENABLE,TRUE);
//  device->SetRenderState(D3DRS_WRAP0,D3DWRAP_U|D3DWRAP_V);
  device->SetRenderState(D3DRS_LIGHTING,TRUE);
  device->SetRenderState(D3DRS_AMBIENT,0x808080);
  device->SetFVF(P3DVertex::FVF);

  D3DVIEWPORT9 vp;
  vp.X=0;
  vp.Y=0;
  vp.Width=renderInfo.width*2;
  vp.Height=renderInfo.height*2;
  vp.MinZ=0.0f;
  vp.MaxZ=1.0f;
  device->SetViewport(&vp);

  ComRef<IDirect3DVertexBuffer9> vertexBuffer;
  if (device->CreateVertexBuffer(VERTEX_BUFFER_SIZE*sizeof(P3DVertex),D3DUSAGE_DYNAMIC|D3DUSAGE_WRITEONLY,P3DVertex::FVF,D3DPOOL_DEFAULT ,vertexBuffer.Init(),0)!=S_OK)
    return VertexBufferCreationFailed;

  device->SetStreamSource(0,vertexBuffer,0,sizeof(P3DVertex));
  
  D3DLIGHT9 Zeroed(basicLight);
  
  basicLight.Type=D3DLIGHT_DIRECTIONAL;
  basicLight.Ambient.r=basicLight.Ambient.g=basicLight.Ambient.b=0.1f;
  basicLight.Diffuse.r=basicLight.Diffuse.g=basicLight.Diffuse.b=1.0f;
  if(renderInfo.topDown)
  {
    basicLight.Direction.x=0; basicLight.Direction.y=-1.0f;basicLight.Direction.z=-1.0f;
  }
  else
  {
    basicLight.Direction.x=basicLight.Direction.y=0;basicLight.Direction.z=-1.0f;
  }

  basicLight.Attenuation0=1;


  device->SetLight(0,&basicLight);
  device->LightEnable(0,TRUE);

  D3DMATRIX Zeroed(projection);
  D3DMATRIX Zeroed(view);
  D3DMATRIX Zeroed(world);
  
  if(renderInfo.topDown)
  {
    renderInfo.zenit = 90;
    modelInfo.center[1] = 0;
  }

  float azimut=-(renderInfo.azimut)*3.14159265/180;
  float zenit=-renderInfo.zenit*3.14159265/180;

  Matrix4 mres;
  Matrix4 mx1(MTranslation,-modelInfo.center);
  Matrix4 mx2(MRotationY,azimut);
  Matrix4 mx3(MRotationX,zenit);

  if(!renderInfo.topDown)
  {
    projection._11=1.0f;
    projection._22=1.0f;
    projection._33=0.1f;
    projection._44=1.0f;
    projection._41=0;
    projection._42=0;
    projection._43=0.5f;

    view._11=1.0/modelInfo.radius;
    view._22=view._11;
    view._33=view._11/2;
    view._44=1.0f;

    mres=mx3*mx2*mx1;
  }
  else //TopDown projection
  {
    
    projection._11=1.0f;
    projection._22=1.0f;
    projection._33=0.1f;
    projection._44=1.0f;
    projection._41=0;
    projection._42=0;
    projection._43=0.5f;
/*
    projection._11=1.0f;
    projection._22=1.0f;
    projection._33=0.0f;
    projection._44=1.0f;
    projection._41=0;
    projection._42=0;
    projection._43=0;
*/
    view._11=1.0/(modelInfo.maxSide/2.0f);
    view._22=view._11;
    view._33=view._11/2;
    view._44=1.0f;

    mres = mx3* mx1;
  }


  for (int i=0;i<4;i++)
    for (int j=0;j<3;j++)
    {
      world.m[i][j]=mres(j,i);
    }
    world._44=1.0f;

  device->SetTransform(D3DTS_PROJECTION,&projection);
  device->SetTransform(D3DTS_VIEW,&view);
  device->SetTransform(D3DTS_WORLD,&world);

  device->BeginScene();

  int indexes[2][3]=
      {
        {0,1,2},
        {0,2,3}
      };
  
  for (int i=0;i<textureData.Size();i++)
  {
    int vxpos=0;
    P3DVertex *buffer;
    device->SetTexture(0,textureData[i]._texture);    
    vertexBuffer->Lock(0,0,(void **)&buffer,D3DLOCK_DISCARD);
    for (int j=0;j<object.NFaces();j++)
    {
      FaceT fc(object,j);
      if (fc.GetTexture(0).Data()==textureData[i]._textureName.Data())
      {
        for (int i=2;i<fc.N();i++)   
        {
          int id=i-2;          
          for (int i=0;i<3;i++)
          {
            int vs=indexes[id][i];
            Vector3 &ps=fc.GetPointVector<Vector3>(vs);
            Vector3 &nm=fc.GetNormalVector<Vector3>(vs);
            buffer[vxpos].x=ps[0];
            buffer[vxpos].y=ps[1];
            buffer[vxpos].z=ps[2];
            buffer[vxpos].xn=nm[0];
            buffer[vxpos].yn=nm[1];
            buffer[vxpos].zn=nm[2];
            buffer[vxpos].color1=0xFFFFFFFF;
            buffer[vxpos].u=fc.GetU(vs);
            buffer[vxpos].v=fc.GetV(vs);
            vxpos++;
            if (vxpos>=VERTEX_BUFFER_SIZE)
            {
              vertexBuffer->Unlock();
              device->DrawPrimitive(D3DPT_TRIANGLELIST,0,vxpos/3);
              vertexBuffer->Lock(0,0,(void **)&buffer,D3DLOCK_DISCARD);
              vxpos=0;
            }
          }
        }
      }
    }
    Assert(vxpos>0);
    vertexBuffer->Unlock();
    device->DrawPrimitive(D3DPT_TRIANGLELIST,0,vxpos/3);
  }
  device->EndScene();

  ComRef<IDirect3DSurface9> resultData;
  if (device->GetRenderTarget(0,resultData.Init())!=S_OK) return UnableToLockResult;
  D3DLOCKED_RECT locked;
  if (resultData->LockRect(&locked,0,D3DLOCK_NOSYSLOCK|D3DLOCK_READONLY)!=S_OK) return UnableToLockResult;

  for (int y=0;y<renderInfo.height;y++)
  {
    DWORD *targetLine=reinterpret_cast<DWORD *>(renderInfo.output+y*renderInfo.width);
    DWORD *srcLine1=reinterpret_cast<DWORD *>(reinterpret_cast<char *>(locked.pBits)+locked.Pitch*(y*2));
    DWORD *srcLine2=reinterpret_cast<DWORD *>(reinterpret_cast<char *>(locked.pBits)+locked.Pitch*(y*2+1));
    for (int x=0;x<renderInfo.width;x++,srcLine1+=2,srcLine2+=2,targetLine++)
    {
      DWORD hv1=((srcLine1[0] & 0xFEFEFEFE)>>1)+((srcLine2[0] & 0xFEFEFEFE)>>1);
      DWORD hv2=((srcLine1[1] & 0xFEFEFEFE)>>1)+((srcLine2[1] & 0xFEFEFEFE)>>1);
      *targetLine=((hv1 & 0xFEFEFEFE)>>1)+((hv2 & 0xFEFEFEFE)>>1);
    }
  }
  resultData->UnlockRect();
  return Ok;
}

bool TextureData::LoadTexture(IDirect3DDevice9 *device, const char *filename)
{
  Picture picture;
  const char *ext=Pathname::GetExtensionFromPath(filename);
  int res;
  if (stricmp(ext,".tga")==0) res=picture.LoadTGA(filename);
  else if (stricmp(ext,".paa")==0) 
  {
    QIFStream in;
    in.open(filename);
    if (in.error()) return false;
    res=picture.LoadPAA(in,0);
  }
  else if (stricmp(ext,".pac")==0) 
  {
    QIFStream in;
    in.open(filename);
    if (in.error()) return false;
    res=picture.LoadPAC(in,0);
  }
  else return false;

  if (res!=0) return false;

  ComRef<IDirect3DTexture9> texture;
  if (device->CreateTexture(picture.W(),picture.H(),1,0,D3DFMT_A8R8G8B8,D3DPOOL_MANAGED,texture.Init(),0)!=S_OK) return false;
  D3DLOCKED_RECT Zeroed(lockInfo);
  if (texture->LockRect(0,&lockInfo,0,D3DLOCK_NOSYSLOCK)!=S_OK) return false;
  picture.ConvertARGB();
  for (int y=0;y<picture.H();y++)
  {
    unsigned long *argbpos=reinterpret_cast<unsigned long *>(reinterpret_cast<char *>(lockInfo.pBits)+y*lockInfo.Pitch);
    for (int x=0;x<picture.W();x++,argbpos++)
    {
      *argbpos=picture.GetPixelARGBRaw(x,y);
    }
  }
  texture->UnlockRect(0);
  _texture=texture;
  return true;
}