// P3D2BMP.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include ".\drawingdevice.h"
#include "P3DToBitmap.h"


bool ProcessParams(P3DToBitmapParams &pdata, int argc,  char **argv)
{
  int nonsw=0;
  while (argc>0)
  {
    const char *data=argv[0];
    while (*data)
    {
      if (*data=='-' || *data=='/') //switch
      {
        data++;
        const char *value=0;
        char *dataSwitches="azslobwhme";
        int code=*data;
        data++;
        if (strchr(dataSwitches,tolower(code))!=0)
        {      
          if (*data) value=data;
          else if (argc>1) {value=argv[1];argc--;argv++;}
          else return false;
          data=strchr(data,0);
          if (value==0) return false;
        }
        switch (tolower(code))
        {
        case 'a': 
          sscanf(value,"%f",&pdata._azimut);
          break;
        case 'c':
          pdata._speherebounding=true;
          break;
        case 'z':
          sscanf(value,"%f",&pdata._zenit);
          break;
        case 's':
          sscanf(value,"%d",&pdata._steps);
          break;
        case 'l':
          sscanf(value,"%f",&pdata._level);
          break;
        case 'o':
          pdata._output=value;
          break;
        case 'b':
          pdata._basePath=value;
          break;
        case 'w':
          sscanf(value,"%d",&pdata._xres);
          break;
        case 'h':
          sscanf(value,"%d",&pdata._yres);
          break;
        case 'p':
          pdata._paletted=true;
          break;
        case 'r':
          pdata._rgb24=true;
          break;
        case 'm':
          sscanf(value,"%d,%x",&pdata._masklevel,&pdata._maskcolor);
          break;
        case 'g':
          pdata._groundcenter=true;
          break;
        case 'x':
          pdata._ignoreProxies=true;
          break;
        case 'e':
          sscanf(value,"%s",pdata._excludeSelection);
          printf("Exclude Selection File: %s\r\n", pdata._excludeSelection);
          break;
        case 't':
          pdata._topDown = true;
          break;
        }
      }
      else
      {
        if (nonsw==0)
        {
          pdata._objectName=data;
          data=strchr(data,0);
          nonsw++;
        }
        else
          return false;
      }
    }
   argv++;
   argc--;
  }
  return true;
}

int _tmain(int argc, _TCHAR* argv[])
{
    P3DToBitmapParams params;
    ProcessParams(params,argc-1,argv+1);
    GDebugExceptionTrap.EnsureInitialized();

    if (params._objectName.IsNull())
    {
      puts("Invalid parameters\r\n"
           "\r\n"
           "Usage:\r\n"
           "------\r\n"
           "\r\n"
           "P3D2BMP <object_name> [-Aazimut] [-Zzenit] [-Ssteps] [-Llevel] [-Ooutput] [-Bbasepath] [-Wwidth] [-hHeight] [-MmaskLevel[,maskColor]] [-p|r] [-i] [-g] [-c] [-x] [-t] [-e""excludeSelFile""]\r\n"
           "\r\n"
           "<object_name>    Name of P3D to convert into BMP preview\r\n"
           "azimut           Azimut of object in degrees *optional* Default: 0.000\r\n"
           "zenit            Zenit of object in degrees *optional* Default: 90.000\r\n"
           "steps            Allows to generate bitmap with series sub-images\r\n"
           "                 Model is rotated about Y axis and each angle is rendered\r\n"
           "                 into image.\r\n"
           "                 Parameter specifies number of steps (Default: 0 - no rotation)\r\n"
           "level            LOD leve of object. Default is 0.0000\r\n"
           "output           Pathname of output file. Default: same as object with .BMP\r\n"
           "basepath         Specifies base path for textures.\r\n"
           "                 If not specified, renderer will try to search some textures\r\n"
           "with             Width of final image: Default 64.\r\n"
           "height           Height of final image: Default 64.\r\n"
           "maskLevel        Level for masking transparency - used with -p or -r\r\n"
           "maskColor        Color for masking transparency - used with -r \r\n"
           "                 Color used standard web-form withou # (ex. FFFF00 is yellow)\r\n"
           "-p               Create paletted bitmap - index zero is transparent color\r\n"
           "-r               Create standard 24 bit BMP with masked transparency\r\n"
           "-i               Append EXIF. This block contains additional informations\r\n"
           "                 about displayed object for calculations final size of bitmap\r\n"
           "                 Data contains center of object X,Y,Z, and radius of bounding\r\n"
           "                 circle (cylinder)\r\n"
           "-g               Ground center - no vertical centering\r\n"
           "-c               Creates spehere bounding instead cylinder\r\n"
           "-x               Renders no proxies\r\n"
           "-t               top down view\r\n"
           "-e               exclude selection file"
           );
      return -210;
    }

    HINSTANCE hInst=GetModuleHandle(0);
    HWND hWnd=CreateWindow("STATIC","Preview Window",WS_OVERLAPPEDWINDOW,0,0,400,430,0,0,hInst,0);

    ComRef<IDirect3D9> d3DService;
    d3DService << Direct3DCreate9(D3D_SDK_VERSION);
    if (d3DService.IsNull()) {puts("Failed to create 3D device");return 201;}

    D3DPRESENT_PARAMETERS Zeroed(pparm);
    pparm.BackBufferFormat=D3DFMT_UNKNOWN;
    pparm.SwapEffect=D3DSWAPEFFECT_COPY;
    pparm.Windowed=TRUE;
    pparm.hDeviceWindow=hWnd;
    pparm.EnableAutoDepthStencil=TRUE;
    pparm.AutoDepthStencilFormat=D3DFMT_D32;
    pparm.PresentationInterval=D3DPRESENT_INTERVAL_DEFAULT;
    
    DrawingDevice device;
    
    if (device.Create(d3DService,D3DADAPTER_DEFAULT,D3DDEVTYPE_REF,hWnd,D3DCREATE_MIXED_VERTEXPROCESSING,&pparm)!=S_OK)
    {
      pparm.AutoDepthStencilFormat=D3DFMT_D16;
      if (device.Create(d3DService,D3DADAPTER_DEFAULT,D3DDEVTYPE_REF,hWnd,D3DCREATE_MIXED_VERTEXPROCESSING,&pparm)!=S_OK)
      {
        puts("Unable to create D3D Device");
        return 202;
      }
    }


    P3DToBitmap::Result result=P3DToBitmap::Run(params,device);
    switch (result)
    {
    case P3DToBitmap::Ok:break;
    case P3DToBitmap::GenError:puts("Error: General error");break;
    case P3DToBitmap::UnableToCreateOutputFile:puts("Error: Unable to create output file");break;
    case P3DToBitmap::UnableToCreateRenderTarget:puts("Error: Unable to create render target");break;
    case P3DToBitmap::UnableToLoadObject:printf("Error: Unable to load object '%s'",params._objectName.GetFullPath());break;
    case P3DToBitmap::UnableToLockResult:puts("Error: Unable to lock result bitmap (DX9:LockRect failed)");break;
    case P3DToBitmap::UnabletoWriteOutputFile:puts("Error: Unable to write to output file");break;
    case P3DToBitmap::VertexBufferCreationFailed:puts("Error: Cannot create vertex buffer for rendering");break;
    default:puts("Error: Unknown error detected");break;
    }

    if (result!=P3DToBitmap::Ok) return 203+result;
	return 0;
}

