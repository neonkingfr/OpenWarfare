#pragma once

class DrawingDevice
{
  ComRef<IDirect3DDevice9> _device;
  D3DPRESENT_PARAMETERS _pparams;  
public:
  DrawingDevice();
  ~DrawingDevice(void);

  HRESULT Create(IDirect3D9 *serviceProvider,UINT Adapter, D3DDEVTYPE DeviceType, HWND hFocusWindow,  DWORD BehaviorFlags,  D3DPRESENT_PARAMETERS *pPresentationParameters);

  IDirect3DDevice9 *operator->() {return _device;}
  IDirect3DDevice9 *operator *() {return _device;}

};
