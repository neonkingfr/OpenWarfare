#include "StdAfx.h"
#include ".\drawingdevice.h"

DrawingDevice::DrawingDevice(void)
{
}

DrawingDevice::~DrawingDevice(void)
{
}

HRESULT DrawingDevice::Create(IDirect3D9 *serviceProvider,UINT Adapter, D3DDEVTYPE DeviceType, HWND hFocusWindow,  DWORD BehaviorFlags,  D3DPRESENT_PARAMETERS *pPresentationParameters)
{
  HRESULT res=serviceProvider->CreateDevice(Adapter,DeviceType,hFocusWindow,BehaviorFlags,pPresentationParameters,_device.Init());
  if (res==S_OK)
  {
    _pparams=*pPresentationParameters;
  }
  return res;
}