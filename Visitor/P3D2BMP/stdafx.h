// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once


#include <iostream>
#include <tchar.h>
#include <es/types/pointers.hpp>
#include <el/pathname/pathname.h>
#include <el/Debugging/imexhnd.h>
#include <malloc.h>
#include <El/BTree/BTree.h>
#include <es/strings/rstring.hpp>
#include <LODObject.h>
#include <Picture.hpp>
#include <windows.h>
#include <d3d9.h>



#define Zeroed(name) name;memset(&name,0,sizeof(name))
#define ZeroedSz(name,sz) name;memset(&name,0,sizeof(name));name.sz=sizeof(name)

// TODO: reference additional headers your program requires here
