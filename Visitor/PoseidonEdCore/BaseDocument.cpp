/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"
#include ".\poseidonedcore.h"
#include "..\PoseidonEditor\PosMainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CMgrTemplates
IMPLEMENT_DYNAMIC(CMgrTemplates,CMntOptions)

CMgrTemplates::CMgrTemplates()
{	m_pOwner = NULL; };

// update hodnot do souboru (funkce nesm� b�t vol�na p��mo)
BOOL CMgrTemplates::LoadTemplates()
{
	// na�ten� nov�ch �ablon
	BOOL   bResult = TRUE;
	//ASSERT(m_nStyle != optNone);
	//ASSERT(m_nStyle == optFile);
	// na�ten� dat
	{	// ulo�eno v souboru
		//ASSERT(m_sRoot.GetLength()!=0);
		SetFile(m_sRoot);
		// na�ten� dat
		//bResult  = OnLoadObject(NULL,10/*POSED_FILE_VER*/);
    bResult  = OnLoadObject(NULL, 28); // Is good version
	}
	// zobrazen� chyby
	if (!bResult)
	{	// report error
		AfxMessageBox(IDS_ERR_LOAD_REGTEMPLATES,MB_OK|MB_ICONEXCLAMATION);
	}
	return bResult;
};

BOOL CMgrTemplates::SaveTemplates()
{
	BOOL   bResult = TRUE;
	//ASSERT(m_nStyle != optNone);
	//ASSERT(m_nStyle == optFile);
	{	// ulo�eno v souboru
		//ASSERT(m_sRoot.GetLength()!=0);
		SetFile(m_sRoot);
		// ulo�en� dat
		bResult  = OnSaveObject(NULL,POSED_FILE_VER);
	}
	// zobrazen� chyby
	if (!bResult)
	{	// report error
		AfxMessageBox(IDS_ERR_SAVE_REGTEMPLATES,MB_OK|MB_ICONEXCLAMATION);
	}
	return bResult;
};

void CMgrTemplates::DoSerialize(CArchive& ar, DWORD nVer, void* pParams)
{
	if (m_pOwner == NULL) AfxThrowUserException();

	// ulo��m jednotliv� mana�ery
	m_pOwner->m_MgrTextures.DoSerialize(ar,nVer);
  if (nVer <= 36)
  {
    m_pOwner->m_pMgrObjects = new CMgrObjects();
    if (m_pOwner->m_pMgrObjects != NULL)
    {
      m_pOwner->m_pMgrObjects->m_pOwner = m_pOwner;
	    m_pOwner->m_pMgrObjects->DoSerialize(ar,nVer);
    }
  }

	m_pOwner->m_MgrWoods.DoSerialize(ar,nVer);
	m_pOwner->m_MgrNets.DoSerialize(ar,nVer);
};

/////////////////////////////////////////////////////////////////////////////
// CPosEdBaseDoc

IMPLEMENT_DYNAMIC(CPosEdBaseDoc, CDocument)

CPosEdBaseDoc::CPosEdBaseDoc()
{
	m_MgrTextures.m_pOwner =	
	m_MgrWoods.m_pOwner =
	m_MgrNets.m_pOwner = this;
	// nastaven� spr�vce akc� dokumentu
	m_ActionManager.m_pDocument = this;
  _curRealViewer=0;
}


CPosEdBaseDoc::~CPosEdBaseDoc()
{
}

BEGIN_MESSAGE_MAP(CPosEdBaseDoc, CDocument)
	//{{AFX_MSG_MAP(CPosEdBaseDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CPosEdBaseDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

BOOL CPosEdBaseDoc::IsRealDocument() const
{	// je toto re�ln� dokument ?
	return (GetPosEdEnvironment()->m_pRealDoc == (CDocument*)this);
};
/*
// zpracov�n� zpr�v z preview
void CPosEdBaseDoc::SendRealMessage(const SPosMessage&) const
{	ASSERT(FALSE);	// mus� b�t implementov�na v aplikaci !!!
};

void CPosEdBaseDoc::HandleRealMessage(const SPosMessage&,int nMode)
{	// zpracov�n� ud�losti z preview
};*/

/////////////////////////////////////////////////////////////////////////////
// CPosEdBaseDoc serialization

void CPosEdBaseDoc::Serialize(CArchive& ar)
{
	// parametry registrace �ablon
   
	if (ar.IsStoring())
	{
		// ulo��m verzi
		char fname[7];
		strcpy(fname, POSED_FILE_ID);
		ASSERT((POSED_FILE_VER >= 10) && (POSED_FILE_VER <= 99));
		fname[5] = '0' + (POSED_FILE_VER / 10);
		fname[6] = '0' + (POSED_FILE_VER % 10);
		ar.Write(fname, 7);
		ULONGLONG toolsDataChunkOffset = 0; // will be rewritten at the end
		ar.Write(&toolsDataChunkOffset, sizeof(toolsDataChunkOffset));

		// ulo��m data - adresare projektu
		MntSerializeString(ar, m_sTexturesPath);
		MntSerializeString(ar, m_sObjectsPath);
		MntSerializeFloat(ar, m_nRealSizeUnit);
		float satGrid = m_satelliteGridG * m_nRealSizeUnit;
		MntSerializeFloat(ar, satGrid);
		MntSerializeString(ar, m_configName);

		m_Textures.DoSerialize(ar, POSED_FILE_VER, this);
		m_ObjectTemplates.DoSerialize(ar, POSED_FILE_VER, this);

		// ulo��m registraci �ablon   
		m_MgrTextures.DoSerialize(ar, POSED_FILE_VER);
		m_MgrWoods.DoSerialize(ar, POSED_FILE_VER);
		m_MgrNets.DoSerialize(ar, POSED_FILE_VER);  
   
		DoSerialize(ar, POSED_FILE_VER);
	}
	else
	{
		// na�tu verzi
		char fname[7];
		ar.Read(fname, 7);
		// verze
		int nVer = (fname[5] - '0') * 10 + (fname[6] - '0');
		if ((nVer < 10) || (nVer > POSED_FILE_VER))
		{	// chybn� verze
			MntInvalidFileFormat();
		}

		// jm�no souboru
		fname[5] = 0;
		if (strcmp(fname, POSED_FILE_ID) != 0)
		{	// chybn� verze
			MntInvalidFileFormat();
		}

		if (nVer > 37)
		{    
			ULONGLONG toolsDataChunkOffset = 0;  // not needed now, just read them to move in file.
			ar.Read(&toolsDataChunkOffset, sizeof(toolsDataChunkOffset));
		}

		// na�tu data

		//  - adresare projektu
		if (nVer >= 25)
		{
			MntSerializeString(ar, m_sTexturesPath);
			m_sTexturesPath.MakeLower();
			MntSerializeString(ar,m_sObjectsPath);
			m_sObjectsPath.MakeLower();
		} 
		else
		{
			m_sTexturesPath.Empty();
			m_sObjectsPath.Empty();
		}

		if (nVer >= 31)
		{
			MntSerializeFloat(ar, m_nRealSizeUnit);    
		}
		else
		{
			if (nVer >= 29)
			{
				int temp;
				MntSerializeInt(ar, temp);   
				m_nRealSizeUnit = (float) temp;
			}
			else
			{
				m_nRealSizeUnit = 50.0f;
			}
		} 

		if (nVer >= 54)
		{
			float satGrid = 0;
			MntSerializeFloat(ar, satGrid);
			m_satelliteGridG = toInt(satGrid / m_nRealSizeUnit);
		}
		else
		{
			m_satelliteGridG = 7;
		}
    
		if (nVer >= 49)
		{
			MntSerializeString(ar, m_configName);
		}
		else
		{
			m_configName = "";
		}

		m_Textures.DoSerialize(ar, nVer, this);
		m_ObjectTemplates.DoSerialize(ar, nVer, this);    

		// nactu registraci �ablon
		if (nVer > 37)
		{
			// load from the same file
			m_MgrTextures.DoSerialize(ar, nVer);      
			m_MgrWoods.DoSerialize(ar, nVer);
			m_MgrNets.DoSerialize(ar, nVer); 
		}
		else
		{    
			CString sFileTempl = ar.GetFile()->GetFilePath();
			ASSERT(sFileTempl.GetLength() > 0);
			int nDot = sFileTempl.ReverseFind(_T('.'));
			if (nDot < 0) AfxThrowUserException();
			(sFileTempl.GetBuffer(0))[nDot+1] = _T(0);
			sFileTempl.ReleaseBuffer();
			sFileTempl += _T("per");
			CMgrTemplates templates;
			templates.m_pOwner = this;
			templates.SetStyle(CMntOptions::optFile, sFileTempl, sFileTempl);

			templates.LoadTemplates();
			if (nVer <= 36)
			{
				//move data from CMgrObjects into m_ObjectTemplates
				if (m_pMgrObjects != NULL)
				{      
					int n = m_pMgrObjects->m_ObjTemplNatural.GetSize();
					for (int i = 0; i < n; ++i)
					{
						Ref<CPosObjectTemplate> templ = new CPosObjectTemplate(*((CPosObjectTemplate*)m_pMgrObjects->m_ObjTemplNatural[i]));
						if (templ.NotNull()) AddObjectTemplateSetID(templ);
					}

					n = m_pMgrObjects->m_ObjTemplPeoples.GetSize();
					for (int i = 0; i < n; ++i)
					{
						Ref<CPosObjectTemplate> templ = new CPosObjectTemplate(*((CPosObjectTemplate*)m_pMgrObjects->m_ObjTemplPeoples[i]));
						if (templ.NotNull()) AddObjectTemplateSetID(templ);          
					}

					delete m_pMgrObjects;
					m_pMgrObjects = NULL;        
				}    
			}
		}

		// serialize the rest.
		DoSerialize(ar, nVer);
	}
}

void CPosEdBaseDoc::DoSerialize(CArchive& ar, int nVer)
{
}

// nastaven� absolutn�ch cest
void CPosEdBaseDoc::UpdateObjectsPaths()
{
}

void CPosEdBaseDoc::GetProjectDirectory(CString& sDir, ProjDir nDir,BOOL bRelativeToViewerDir /* = FALSE*/) const
{
  sDir.Empty();
  switch (nDir)
  {
    case CPosEdBaseDoc::dirText:
      if (!bRelativeToViewerDir)
      {
        CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
        MakeLongDirStr(sDir, pEnvir->m_optSystem.m_sPreviewAppPath);
        sDir += m_sTexturesPath;
        MakeLongDirStr(sDir,sDir);
      }
      else
      {
		    MakeLongDirStr(sDir,m_sTexturesPath);		
      }
      break;
	  case CPosEdBaseDoc::dirObj:
      if (!bRelativeToViewerDir)
      {
        CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
        MakeLongDirStr(sDir, pEnvir->m_optSystem.m_sPreviewAppPath);
        sDir += m_sObjectsPath;
        MakeLongDirStr(sDir,sDir);
      }
      else
      {
        MakeLongDirStr(sDir,m_sObjectsPath);		
      }
      break;
    case dirRoot:
      if (!bRelativeToViewerDir)
      {
        CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
        MakeLongDirStr(sDir, pEnvir->m_optSystem.m_sPreviewAppPath);
      }
      else
      {
		    MakeLongDirStr(sDir,"\\");		
      }
      break;
    default:
      ASSERT(FALSE);
      break;
  }
}

// update oken na z�klade parametr�
void CPosEdBaseDoc::DoUpdateAllViews(DWORD nType)
{
	CUpdatePar updParams(nType, NULL);
	DoUpdateAllViews(&updParams);
}

void CPosEdBaseDoc::DoUpdateAllViews(CAction* pAction)
{
	CUpdatePar updParams(pAction);
	DoUpdateAllViews(&updParams);
}

void CPosEdBaseDoc::DoUpdateAllViews(CUpdatePar* pPar)
{ 
	UpdateAllViews(pPar->m_pSender, (LPARAM)(pPar->m_nType), pPar);
}

/////////////////////////////////////////////////////////////////////////////
// pr�ce se spr�vcem akc�

BOOL CPosEdBaseDoc::RealizeEditAction(CPosAction* pAction)
{
	// na t�to �rovni neprov�d�m ��dn� akce
	return FALSE;
}

void CPosEdBaseDoc::ProcessEditAction(CPosAction* pAction)
{	
	m_ActionManager.DoNewAction(pAction); 
}

void CPosEdBaseDoc::OnEditUndoAction()
{	
	m_ActionManager.DoUndoAction(); 
}

void CPosEdBaseDoc::OnUpdateEditUndoAction(CCmdUI* pCmdUI)
{	
	m_ActionManager.OnUpdateUndoAction(pCmdUI); 
}

void CPosEdBaseDoc::OnEditRedoAction()
{	
	m_ActionManager.DoRedoAction(); 
}

void CPosEdBaseDoc::OnUpdateEditRedoAction(CCmdUI* pCmdUI)
{	
	m_ActionManager.OnUpdateRedoAction(pCmdUI); 
}

void CPosEdBaseDoc::OnDelUndoRedoActions()
{ 
	m_ActionManager.ClearUndoRedoBuffers();
}

void CPosEdBaseDoc::OnUpdateDelUndoRedoActions(CCmdUI* pCmdUI)
{	
	m_ActionManager.OnUpdateDelUndoRedoActions(pCmdUI); 
}

/////////////////////////////////////////////////////////////////////////////
// CPosEdBaseDoc commands

Ref<CPosObjectTemplate> CPosEdBaseDoc::GetObjectTemplate(int ID)
{
	return m_ObjectTemplates.FindEx(ID);    
}

Ref<CPosObjectTemplate> CPosEdBaseDoc::GetObjectTemplate(const CString& objFile)
{
	return m_ObjectTemplates.FindFileEx(objFile);  
}

Ref<CPosObjectTemplate> CPosEdBaseDoc::GetObjectTemplateByName(const CString& objName)
{
	return m_ObjectTemplates.FindNameEx(objName);  
}

void CPosEdBaseDoc::AddObjectTemplateSetID(Ref<CPosObjectTemplate>& templ)
{
	m_ObjectTemplates.AddID(templ);
}

void CPosEdBaseDoc::DeleteObjectTemplates(AutoArray<int> deleteIDs)
{
	for (int i = 0; i < deleteIDs.Size(); ++i)
	{
		int indx = m_ObjectTemplates.Find(deleteIDs[i]);
		if (indx >= 0) m_ObjectTemplates.Delete(indx);
	}
}

BOOL CPosEdBaseDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
	CFileException fe;
	CFMntFromToFile* pFile = new CFMntFromToFile;
	ASSERT(pFile != NULL);

	if (!pFile->Open(lpszPathName, CFile::modeCreate | CFile::modeReadWrite | CFile::shareExclusive, &fe))
	{
		delete pFile;
		pFile = NULL;
	}
  
	if (pFile == NULL)
	{
		ReportSaveLoadException(lpszPathName, &fe, TRUE, AFX_IDP_INVALID_FILENAME);
		return FALSE;
	}

	CFile oldFile;    
	ULONGLONG dataPos = _UI64_MAX;

	if (oldFile.Open(GetPathName(), CFile::modeRead))
	{
		// file exist found and opened
		char fname[7];
		oldFile.Read(fname,7);
		// verze
		int nVer = (fname[5] - '0') * 10 + (fname[6] - '0');    
		if (nVer > 37)
		  oldFile.Read( &dataPos, sizeof(ULONGLONG));
		else
		  dataPos = oldFile.GetLength();

		oldFile.Close();
	}

	pFile->StartFromToSession(0, dataPos, GetPathName());

	CArchive saveArchive(pFile, CArchive::store | CArchive::bNoFlushOnDelete);
	saveArchive.m_pDocument = this;
	saveArchive.m_bForceFlat = FALSE;
	TRY
	{
		CWaitCursor wait;
		Serialize(saveArchive);     // save me
		saveArchive.Close();

		pFile->EndFromToSession(dataPos);
		pFile->Seek(7, CFile::begin);
		pFile->Write(&dataPos, sizeof(ULONGLONG));

    /*// now update offset of data chunk.
    ULONGLONG endPos = pFile->GetLength();
    pFile->Seek( 7, CFile::begin);
    pFile->Write( &endPos, sizeof(ULONGLONG));

    // copy data chunk from the old file if exist
    CFile oldFile;    
    if (oldFile.Open(GetPathName(), CFile::modeRead))
    {
      // file exist found and opened
      oldFile.Seek( 7, CFile::begin);
      ULONGLONG dataPos;
      oldFile.Read( &dataPos, sizeof(ULONGLONG));
      
      ULONGLONG oldLength =  oldFile.GetLength();
      oldFile.Seek( dataPos, CFile::begin);

      UINT dataSize = (UINT) (oldLength - dataPos);

      // alloc buffer for move
      BYTE * buffer = new BYTE[dataSize];
      if (buffer)
      {
        oldFile.Read(buffer, dataSize);
        pFile->Seek(0, CFile::end);
        pFile->Write(buffer, dataSize);
        delete [] buffer;
      }

      oldFile.Close();
    }*/
		ReleaseFile(pFile, FALSE);
	}
	CATCH_ALL(e)
	{
		ReleaseFile(pFile, TRUE);
		ReportSaveLoadException(lpszPathName, e, TRUE, AFX_IDP_FAILED_TO_SAVE_DOC);    
		return FALSE;
	}
	END_CATCH_ALL

	SetModifiedFlag(FALSE);     // back to unmodified

	return TRUE;        // success*/
  //return CDocument::OnSaveDocument(lpszPathName);
}

void CPosEdBaseDoc::DeleteContents()
{
	m_ActionManager.ClearUndoRedoBuffers();
	m_MgrNets.Destroy();
	m_MgrTextures.Destroy();
	m_MgrWoods.Destroy();

	m_ObjectTemplates.Clear();
	m_Textures.Clear();

	base::DeleteContents();
}