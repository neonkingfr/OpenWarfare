/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPosWoodTemplate - �ablona lesa

IMPLEMENT_DYNAMIC( CPosWoodTemplate, CObject )

CPosWoodTemplate::CPosWoodTemplate()
{
	m_pOwner = NULL;
};

CPosWoodTemplate::CPosWoodTemplate(CPosEdBaseDoc*	pOwner)
{
	m_pOwner = pOwner;
};

CPosWoodTemplate::~CPosWoodTemplate()
{	DestroyValues(); };

void CPosWoodTemplate::MakeDefaultValues()
{
	m_sWoodName.Empty();
	m_clrFrame =
	m_clrEntire= DEFAULT_COLOR;
};
void CPosWoodTemplate::DestroyValues()
{	
	m_WoodObjBush.RemoveAll();
	m_WoodObjTree.RemoveAll();
};
void CPosWoodTemplate::CopyFrom(const CPosWoodTemplate* pSrc)
{
	DestroyValues();
	m_pOwner				= pSrc->m_pOwner;

	m_sWoodName			= pSrc->m_sWoodName;
	m_clrFrame			= pSrc->m_clrFrame;
	m_clrEntire			= pSrc->m_clrEntire;
	// kop�ruji �ablony
	m_pWoodObjREntir = pSrc->m_pWoodObjREntir;
	m_pWoodObjRFrame = pSrc->m_pWoodObjRFrame;
	m_pWoodObjTEntir = pSrc->m_pWoodObjTEntir;
	
	// kop�ruji pole
	for(int i=0; i<pSrc->m_WoodObjBush.GetSize(); i++)
		m_WoodObjBush.Add(pSrc->m_WoodObjBush[i]);
	for(int i=0; i<pSrc->m_WoodObjTree.GetSize(); i++)
		m_WoodObjTree.Add(pSrc->m_WoodObjTree[i]);
};

// serializace
void CPosWoodTemplate::DoSerialize(CArchive& ar, DWORD nVer)
{
	if (!ar.IsStoring())
		DestroyValues();	// vypr�zdn�n� 

	MntSerializeString(ar,m_sWoodName);
	MntSerializeColor(ar,m_clrFrame);
	MntSerializeColor(ar,m_clrEntire);

  if (ar.IsLoading())
  {  
    if (nVer < 37)
    {  
      // serializace �ablon
      BOOL bExist;      
      MntSerializeBOOL(ar,bExist);
      if (bExist)
      {
        m_pWoodObjREntir = new CPosObjectTemplate(m_pOwner);

        m_pWoodObjREntir->DoSerialize(ar, nVer);
        m_pWoodObjREntir->m_nObjType = CPosObjectTemplate::objTpWood;
        m_pOwner->AddObjectTemplateSetID(m_pWoodObjREntir);
      };

      MntSerializeBOOL(ar,bExist);
      if (bExist)
      {
        m_pWoodObjRFrame = new CPosObjectTemplate(m_pOwner);

        m_pWoodObjRFrame->DoSerialize(ar, nVer);
        m_pWoodObjRFrame->m_nObjType = CPosObjectTemplate::objTpWood;
        m_pOwner->AddObjectTemplateSetID(m_pWoodObjRFrame);        
      };

      MntSerializeBOOL(ar,bExist);
      if (bExist)
      {
        m_pWoodObjTEntir = new CPosObjectTemplate(m_pOwner);
 
        m_pWoodObjTEntir->DoSerialize(ar, nVer);
        m_pWoodObjTEntir->m_nObjType = CPosObjectTemplate::objTpWood;
        m_pOwner->AddObjectTemplateSetID(m_pWoodObjTEntir);  
      };
    }
    else
    {
      int templateID;
      MntSerializeInt(ar, templateID);
      m_pWoodObjREntir = m_pOwner->GetObjectTemplate(templateID);
      MntSerializeInt(ar, templateID);
      m_pWoodObjRFrame = m_pOwner->GetObjectTemplate(templateID);
      MntSerializeInt(ar, templateID);
      m_pWoodObjTEntir = m_pOwner->GetObjectTemplate(templateID);
    }
  }
  else
  {
    int iD = -1;
    if (m_pWoodObjREntir.NotNull())    
      MntSerializeInt(ar, m_pWoodObjREntir->m_ID);
    else    
      MntSerializeInt(ar, iD);

    if (m_pWoodObjRFrame.NotNull())
      MntSerializeInt(ar, m_pWoodObjRFrame->m_ID);
    else
      MntSerializeInt(ar, iD);

    if (m_pWoodObjTEntir.NotNull())
      MntSerializeInt(ar, m_pWoodObjTEntir->m_ID);
    else
      MntSerializeInt(ar, iD);
  }
	
	if (nVer < 13)
	{
    BOOL bExist; 
		MntSerializeBOOL(ar,bExist);
		if (bExist)
		{
			CPosObjectTemplate* pWoodObjTFrame = new CPosObjectTemplate(m_pOwner);			
			pWoodObjTFrame->DoSerialize(ar, nVer);
			delete pWoodObjTFrame;
		};
		CObArray temp;
		// serializace pol�
		CMgrObjects::DoSerializeTemplates(ar, nVer, temp, m_pOwner);
		DestroyArrayObjects(&temp);
		CMgrObjects::DoSerializeTemplates(ar, nVer, temp, m_pOwner);
		DestroyArrayObjects(&temp);
	} else
	{
		// serializace pol�
		DoSerializeStringArray(ar, nVer, m_WoodObjBush);
		DoSerializeStringArray(ar, nVer, m_WoodObjTree);
	};
};

// vykreslen� v ListBox
void CPosWoodTemplate::DrawLBItem(CDC* pDC,CRect& rPos) const
{
	CRect rText = rPos; rText.left += 2;
	// jm�no
	::DrawTextA(pDC->m_hDC,m_sWoodName,-1,&rText,DT_LEFT|DT_NOPREFIX|DT_SINGLELINE|DT_VCENTER|DT_TABSTOP);
};

COLORREF CPosWoodTemplate::GetLBItemColor() const
{
	return GetObjectEntireColor(&(GetPosEdEnvironment()->m_cfgCurrent));
};

COLORREF CPosWoodTemplate::GetObjectFrameColor(const CPosEdCfg* pCfg) const
{
	if (m_clrFrame != DEFAULT_COLOR)
		return m_clrFrame;
	ASSERT(pCfg!=NULL);
	return pCfg->m_cDefColors[CPosEdCfg::defcolFWood];
};

COLORREF CPosWoodTemplate::GetObjectEntireColor(const CPosEdCfg* pCfg) const
{
	if (m_clrEntire != DEFAULT_COLOR)
		return m_clrEntire;
	ASSERT(pCfg!=NULL);
	return pCfg->m_cDefColors[CPosEdCfg::defcolWoods];
};

void CPosWoodTemplate::CreateLocalObjectTemplates()
{
  if (m_pWoodObjREntir.NotNull())  
    m_pWoodObjREntir = new CPosObjectTemplate(*m_pWoodObjREntir);

  if (m_pWoodObjRFrame.NotNull())
    m_pWoodObjRFrame = new CPosObjectTemplate(*m_pWoodObjRFrame);
    
  if (m_pWoodObjTEntir.NotNull())
    m_pWoodObjTEntir = new CPosObjectTemplate(*m_pWoodObjTEntir);  
}

void CPosWoodTemplate::SyncLocalObjectTemplatesIntoDoc()
{
  Ref<CPosObjectTemplate> templ = m_pOwner->GetObjectTemplate(m_pWoodObjREntir->m_ID);
  if (templ.NotNull())  
  {
    templ->CopyFrom(m_pWoodObjREntir);  
    m_pWoodObjREntir = templ;
  }
  else
    m_pOwner->AddObjectTemplateSetID(m_pWoodObjREntir);

  templ = m_pOwner->GetObjectTemplate(m_pWoodObjRFrame->m_ID);
  if (templ.NotNull())  
  {
    templ->CopyFrom(m_pWoodObjRFrame);  
    m_pWoodObjRFrame = templ;
  }
  else
    m_pOwner->AddObjectTemplateSetID(m_pWoodObjRFrame);

  templ = m_pOwner->GetObjectTemplate(m_pWoodObjTEntir->m_ID);
  if (templ.NotNull())  
  {
    templ->CopyFrom(m_pWoodObjTEntir);  
    m_pWoodObjTEntir = templ;
  }
  else
    m_pOwner->AddObjectTemplateSetID(m_pWoodObjTEntir);
}

/////////////////////////////////////////////////////////////////////////////
// CMgrWoods - mana�er les�

CMgrWoods::CMgrWoods()
{
};

CMgrWoods::~CMgrWoods()
{	Destroy(); };

void CMgrWoods::Destroy()
{	DestroyArrayObjects(&m_WoodTemplates); };

void CMgrWoods::CopyFrom(CMgrWoods* pSrc)
{
	Destroy();
	for(int i=0;i<pSrc->m_WoodTemplates.GetSize();i++)
	{
		CPosWoodTemplate* pTempl = (CPosWoodTemplate*)(pSrc->m_WoodTemplates[i]);
		if (pTempl)
		{
			ASSERT_KINDOF(CPosWoodTemplate,pTempl);
			CPosWoodTemplate* pCopy = new CPosWoodTemplate(m_pOwner);
			
			// kop�ruji data
			pCopy->CopyFrom(pTempl);
			// vlo��m �ablonu
			m_WoodTemplates.Add(pCopy);
		}
	}
};

// serializace
void CMgrWoods::DoSerialize(CArchive& ar, DWORD nVer)
{
	BOOL bEmpty = (m_WoodTemplates.GetSize()==0);
	MntSerializeBOOL(ar,bEmpty);
	if (bEmpty) return;	// tento manazer je pr�zdn�

	if (!ar.IsStoring())
		Destroy();	// vypr�zdn�n� 

	// serializace les�
	int nCount = m_WoodTemplates.GetSize();
	MntSerializeInt(ar,nCount);
	for(int i=0; i<nCount; i++)
	{
		if (ar.IsStoring())
		{
			CPosWoodTemplate* pTempl = (CPosWoodTemplate*)(m_WoodTemplates[i]);
			ASSERT(pTempl != NULL);
			pTempl->DoSerialize(ar, nVer);
		} else
		{
			CPosWoodTemplate* pTempl = new CPosWoodTemplate(m_pOwner);
			m_WoodTemplates.Add(pTempl);
			pTempl->DoSerialize(ar, nVer);
		};
	}
};

// �ablona podle jm�na
CPosWoodTemplate* CMgrWoods::GetWoodTemplate(LPCTSTR pName) const
{
	for(int i=0;i<m_WoodTemplates.GetSize();i++)
	{
		CPosWoodTemplate* pTempl = (CPosWoodTemplate*)(m_WoodTemplates[i]);
		if (pTempl)
		{
			ASSERT_KINDOF(CPosWoodTemplate,pTempl);
			if (lstrcmpi(pTempl->m_sWoodName,pName)==0)
				return pTempl;
		}
	}
	return NULL;
};

void CMgrWoods::CreateLocalObjectTemplates()
{
  for(int i=0;i<m_WoodTemplates.GetSize();i++)
  {
    CPosWoodTemplate* pTempl = (CPosWoodTemplate*)(m_WoodTemplates[i]);
    if (pTempl)
    {
      ASSERT_KINDOF(CPosWoodTemplate,pTempl);
      pTempl->CreateLocalObjectTemplates();
    }
  }
}

void CMgrWoods::SyncLocalObjectTemplatesIntoDoc()
{
  for(int i=0;i<m_WoodTemplates.GetSize();i++)
  {
    CPosWoodTemplate* pTempl = (CPosWoodTemplate*)(m_WoodTemplates[i]);
    if (pTempl)
    {
      ASSERT_KINDOF(CPosWoodTemplate,pTempl);
      pTempl->SyncLocalObjectTemplatesIntoDoc();        
    }
  }
}
