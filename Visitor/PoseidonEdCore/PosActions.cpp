/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CPosAction

IMPLEMENT_DYNAMIC(CPosAction,CAction)

CPosAction::CPosAction(int nType):
	CAction(nType,NULL)
{
	ASSERT(m_sActName.GetLength() == 0);
	// jm�no akce je z parametrick�ho �et�zce
	if (m_nActType != 0)
	{
		CString sFull;
		VERIFY( sFull.LoadString(m_nActType) );
		VERIFY( MntGetSubstring(m_sActName, sFull, _T('#'),0));
	}
	m_bFromBuldozer = FALSE;
	m_bUpdateView	= TRUE;
}

// proveden� akce v dokumentu
BOOL CPosAction::RealizeAction(CDocument* pDoc)
{
	ASSERT(pDoc != NULL);
	ASSERT_KINDOF(CPosEdBaseDoc, pDoc);
	// mohu m�nit
	if (!((CPosEdBaseDoc*)pDoc)->RealizeEditAction(this)) return FALSE; // nic se nezm�nilo

	// provedu update oken po proveden� akce
	if (m_bUpdateView)
	{
		CUpdatePar parUpdate(this);
		((CPosEdBaseDoc*)pDoc)->DoUpdateAllViews(&parUpdate);
	}

	// je modifikov�n dokument
	pDoc->SetModifiedFlag();
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CPosActionGroup

IMPLEMENT_DYNAMIC(CPosActionGroup,CPosAction)

CPosActionGroup::CPosActionGroup(int nType)
: CPosAction(nType)
{
	// z�kladn� po�ad� prov�d�n� akc�
	m_bBaseDirection = TRUE;
}

CPosActionGroup::~CPosActionGroup()
{	
	DestroyAll(); 
}

// proveden� akce v dokumentu
BOOL CPosActionGroup::RealizeAction(CDocument* pDoc)
{
	// realizace z�kladn� akce
	BOOL bChange = FALSE;
	// realizace d�l��ch akc�
	if (m_bBaseDirection)
	{
		for (int i = 0; i < m_Actions.GetSize(); ++i)
		{
			CPosAction* pAction = (CPosAction*)(m_Actions[i]);
			if (pAction)
			{
				ASSERT_KINDOF(CPosAction, pAction);
				bChange |= pAction->RealizeAction(pDoc);
			} 
		}
	} 
	else
	{
		for (int i = m_Actions.GetSize() - 1; i >= 0; --i)
		{
			CPosAction* pAction = (CPosAction*)(m_Actions[i]);
			if (pAction)
			{
				ASSERT_KINDOF(CPosAction, pAction);
				bChange |= pAction->RealizeAction(pDoc);
			} 
		}
	}
	return bChange;
}

void CPosActionGroup::SetFromBuldozer(BOOL bFromBuldozer)
{
  CPosAction::SetFromBuldozer(bFromBuldozer);

  for(int i=0; i<m_Actions.GetSize(); i++)
  {
    CPosAction* pAction = (CPosAction*)(m_Actions[i]);
    if (pAction)
    {
      ASSERT_KINDOF(CPosAction,pAction);
      pAction->SetFromBuldozer(bFromBuldozer);
    } 
  };

}

// pr�ce se skupinou akc�
void CPosActionGroup::AddAction(CPosAction* pAction)
{	m_Actions.Add(pAction);	};
void CPosActionGroup::DestroyAll()
{	DestroyArrayObjects(&m_Actions); };

// v�m�na hodnot (z�m�na po�ad�)
BOOL CPosActionGroup::ChangeValues()
{
	m_bBaseDirection = !m_bBaseDirection;
	// z�m�na hodnot d�l��ch akc�
	for(int i=m_Actions.GetSize()-1; i>=0; i--)
	{
		CPosAction* pAction = (CPosAction*)(m_Actions[i]);
		if (pAction)
		{
			ASSERT_KINDOF(CPosAction,pAction);
			VERIFY(pAction->ChangeValues());
		} 
	};
	return TRUE;
};

