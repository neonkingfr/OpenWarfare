/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "math.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNetComponent

IMPLEMENT_DYNAMIC( CNetComponent, CObject )
					
CNetComponent::CNetComponent(CPosEdBaseDoc*	pOwner)
{
	m_pOwner  = pOwner;
	m_pNetObj = NULL;
}

CNetComponent::~CNetComponent()
{	//zru�en� dat
	Destroy();
}

void CNetComponent::MakeDefault()
{
	m_sCompName.Empty();
}

void CNetComponent::Destroy()
{
	m_sCompName.Empty();	
	m_pNetObj = NULL;	
}

// vytvo�en� kopie objektu
CNetComponent* CNetComponent::CreateCopyObject() const
{
	CObject* pObject = NULL;
	TRY
	{
		pObject = GetRuntimeClass()->CreateObject();
		if (pObject)
		{
			ASSERT_KINDOF(CNetComponent, pObject);
			((CNetComponent*)pObject)->CopyFrom(this);
		} 
		else
		{
			AfxThrowUserException();
		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pObject) delete pObject;
		pObject = NULL;
	}
	END_CATCH_ALL
	return ((CNetComponent*)pObject);
}

CNetComponent* CNetComponent::CreateObjectType(int nType, CPosEdBaseDoc*	pOwner)
{
	CNetComponent* pObj = NULL;
	switch(nType)
	{
	case netCmpSTRA: pObj = new CNetSTRA(pOwner); break;
	case netCmpBEND: pObj = new CNetBEND(pOwner); break;
	case netCmpSPEC: pObj = new CNetSPEC(pOwner); break;
	case netCmpTERM: pObj = new CNetTERM(pOwner); break;
	};
	return pObj;
}

void CNetComponent::CopyFrom(const CNetComponent* pSrc)
{
	// zru�en� dat
	Destroy();

	m_pOwner		 = pSrc->m_pOwner;
	// kopie data
	m_sCompName		= pSrc->m_sCompName;

	// kopie objektu
	ASSERT(m_pNetObj == NULL);
	if (pSrc->m_pNetObj != NULL)
	{
		m_pNetObj = new CPosObjectTemplate(pSrc->m_pNetObj->m_pOwner);
		if (m_pNetObj) m_pNetObj->CopyFrom(pSrc->m_pNetObj);
	}
};

// serializace
void CNetComponent::DoSerialize(CArchive& ar, DWORD nVer)
{
	// serializace dat
	MntSerializeString(ar, m_sCompName);

	// serializace objektu
	if (ar.IsLoading())
	{ 
		if (nVer < 37)
		{    
			BOOL bExist;    
			MntSerializeBOOL(ar, bExist);
			if (bExist)
			{
				m_pNetObj = new CPosObjectTemplate(m_pOwner);
        
				m_pNetObj->DoSerialize(ar, nVer);
				m_pNetObj->m_nObjType = CPosObjectTemplate::objTpNet;
				m_pOwner->AddObjectTemplateSetID(m_pNetObj);
			}
		}
		else
		{
			int templID;
			MntSerializeInt(ar, templID);
			m_pNetObj = m_pOwner->GetObjectTemplate(templID);
		}
	}
	else
	{
		if (m_pNetObj.NotNull())
		{
			MntSerializeInt(ar, m_pNetObj->m_ID);
		}
		else
		{
			int iD = -1;
			MntSerializeInt(ar, iD);
		}
	}
}

// propojen� s objektem poseidon
BOOL CNetComponent::CreateFromObjectTemplate(CPosObjectTemplate* pTempl)
{
	if (!pTempl->m_bIsNetObject) return FALSE;	// nen� to objekt s�t�

	// propoj�m s objektem
	ASSERT((m_pNetObj == NULL) || (m_pNetObj == pTempl));
	m_pNetObj = pTempl;

	// jm�no podle objektu
	if (m_sCompName.GetLength() == 0) m_pNetObj->GetNameFromFile(m_sCompName);

	// v�e je OK
	return TRUE;
}

void CNetComponent::CreateLocalObjectTemplates()
{
	if (m_pNetObj.NotNull()) m_pNetObj = new CPosObjectTemplate(*m_pNetObj);    
}

void CNetComponent::SyncLocalObjectTemplatesIntoDoc()
{
	if (m_pNetObj.NotNull())
	{
		Ref<CPosObjectTemplate> templ = m_pOwner->GetObjectTemplate(m_pNetObj->m_ID);
		if (templ.NotNull())
		{
			templ->CopyFrom(m_pNetObj);
			m_pNetObj = templ;
		}
		else
		{
			m_pOwner->AddObjectTemplateSetID(m_pNetObj);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// z�kladn� rovinka

IMPLEMENT_DYNAMIC( CNetBaseStra, CNetComponent )

CNetBaseStra::CNetBaseStra(CPosEdBaseDoc* pOwner)
: CNetComponent(pOwner)
{
}

// vytvo�en� kopie objektu
void CNetBaseStra::CopyFrom(const CNetComponent* pSrc)
{
	CNetComponent::CopyFrom(pSrc);
	// vlastn� data
	ASSERT_KINDOF(CNetBaseStra, pSrc);
	m_nSTRAType   = ((CNetBaseStra*)pSrc)->m_nSTRAType;
	m_bChngHeight = ((CNetBaseStra*)pSrc)->m_bChngHeight;
}

// serializace
void CNetBaseStra::DoSerialize(CArchive& ar, DWORD nVer)
{
	CNetComponent::DoSerialize(ar, nVer);
	// vlastn� data
	MntSerializeWord(ar, m_nSTRAType);
	if (nVer >= 22) MntSerializeBOOL(ar, m_bChngHeight);
}

// propojen� s objektem poseidon
BOOL CNetBaseStra::CreateFromObjectTemplate(CPosObjectTemplate* pTempl)
{
	if (!CNetComponent::CreateFromObjectTemplate(pTempl)) return FALSE;	// nespl�uje z�kladn� podm�nky

	// m� v�ech body LB, PB, LE, PE ?
	if (!pTempl->ExistNetPoint(CPosObjectTemplate::ptNetLB)) return FALSE;
	if (!pTempl->ExistNetPoint(CPosObjectTemplate::ptNetPB)) return FALSE;
	if (!pTempl->ExistNetPoint(CPosObjectTemplate::ptNetLE)) return FALSE;
	if (!pTempl->ExistNetPoint(CPosObjectTemplate::ptNetPE)) return FALSE;

	// typ podle d�lky
	Vector3 ptLB = pTempl->GetNetPoint(CPosObjectTemplate::ptNetLB);
	Vector3 ptLE = pTempl->GetNetPoint(CPosObjectTemplate::ptNetLE);
	float nSize = ptLE[2] - ptLB[2];
	if (nSize > 13.f)
	{
		m_nSTRAType = netSTRA_25;
	}
	else if (nSize > 7.f)
	{
		m_nSTRAType = netSTRA_12;
	}
	else
	{
		m_nSTRAType = netSTRA_6;
	}
	// v�e je OK
	return TRUE;
}

// d�lka v m podle typu
float CNetBaseStra::GetNormalizeLength(float nLen)
{
	nLen = (float)fabs(nLen);
	float nLenOrig = nLen;

	if (nLen > 13.f)
	{
		nLen = 25.f;
	}
	else if (nLen > 7.f)
	{
		nLen = 12.5f;
	}
	else
	{
		nLen = 6.25f;
	}

	if (fabs(nLenOrig - nLen) > 1)
	{
		LogF("Warning: CNetBaseStra::GetNormalizeLength -- big difference: nLenOrig %lf, nLen %lf", nLenOrig, nLen);
	}
	return nLen;
}

float CNetBaseStra::GetNormalizeLength(int nType)
{
	float nLen = 0.f;
	switch(nType)
	{
	case netSTRA_6:  nLen = 6.25f; break;
	case netSTRA_12: nLen = 12.5f; break;
	case netSTRA_25: nLen = 25.f;  break;
	default: ASSERT(FALSE);
	};
	return nLen;
}

float CNetBaseStra::GetStraLength() const
{
	float nLen = 0.f;
	switch(m_nSTRAType)
	{
	case netSTRA_6:  nLen = 6.25f; break;
	case netSTRA_12: nLen = 12.5f; break;
	case netSTRA_25: nLen = 25.f;  break;
	};
	return nLen;
}

/////////////////////////////////////////////////////////////////////////////
// rovinka - STRA

IMPLEMENT_DYNCREATE( CNetSTRA, CNetBaseStra )

CNetSTRA::CNetSTRA()
: CNetBaseStra(NULL)
{	
	m_bChngHeight = TRUE; 
}

CNetSTRA::CNetSTRA(CPosEdBaseDoc* pOwner)
: CNetBaseStra(pOwner)
{	
	m_bChngHeight = TRUE; 
}

// propojen� s objektem poseidon
BOOL CNetSTRA::CreateFromObjectTemplate(CPosObjectTemplate* pTempl)
{
	if (!CNetBaseStra::CreateFromObjectTemplate(pTempl)) return FALSE;	// nespl�uje z�kladn� podm�nky

	// v�e je OK
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// zat��ka - BEND

IMPLEMENT_DYNCREATE( CNetBEND, CNetComponent )

CNetBEND::CNetBEND()
: CNetComponent(NULL)
{
}

CNetBEND::CNetBEND(CPosEdBaseDoc* pOwner)
: CNetComponent(pOwner)
{
}
  
CNetBEND::~CNetBEND()
{
}

// vytvo�en� kopie objektu
void CNetBEND::CopyFrom(const CNetComponent* pSrc)
{
	CNetComponent::CopyFrom(pSrc);
	// vlastn� data
	ASSERT_KINDOF(CNetBEND,pSrc);
	m_nBENDType = ((CNetBEND*)pSrc)->m_nBENDType;
}

// serializace
void CNetBEND::DoSerialize(CArchive& ar, DWORD nVer)
{
	CNetComponent::DoSerialize(ar, nVer);
	// vlastn� data
	MntSerializeWord(ar, m_nBENDType);
}

// propojen� s objektem poseidon
BOOL CNetBEND::CreateFromObjectTemplate(CPosObjectTemplate* pTempl)
{
	if (!CNetComponent::CreateFromObjectTemplate(pTempl)) return FALSE;	// nespl�uje z�kladn� podm�nky

	// m� v�ech body LB, PB, LE, PE ?
	if (!pTempl->ExistNetPoint(CPosObjectTemplate::ptNetLB)) return FALSE;
	if (!pTempl->ExistNetPoint(CPosObjectTemplate::ptNetPB)) return FALSE;
	if (!pTempl->ExistNetPoint(CPosObjectTemplate::ptNetLE)) return FALSE;
	if (!pTempl->ExistNetPoint(CPosObjectTemplate::ptNetPE)) return FALSE;

	Vector3 ptLB = pTempl->GetNetPoint(CPosObjectTemplate::ptNetLB);
	Vector3 ptPB = pTempl->GetNetPoint(CPosObjectTemplate::ptNetPB);
	Vector3 ptLE = pTempl->GetNetPoint(CPosObjectTemplate::ptNetLE);

	// new code
	// Added to allow for variable angle
	// **********************************
	Vector3 ptPE = pTempl->GetNetPoint(CPosObjectTemplate::ptNetPE);
	Vector3 vB = ptPB - ptLB;
	Vector3 vE = ptPE - ptLE;
	vB.Normalize();
	vE.Normalize();
	float cosBE = vB.DotProduct(vE);
	float nRad = acos(cosBE);
	float nDeg = (float)GetGradFromRad(nRad);

	Vector3 cMidB = (ptPB + ptLB) / 2.0f;
	Vector3 cMidE = (ptPE + ptLE) / 2.0f;
	float dz = cMidB.Z() - cMidE.Z();
	float radius = -dz / sin(nRad);

	if (radius > 1990.0)
	{
		m_nBENDType = netBEND_0_2000;
	}
	else if (radius > 999.0)
	{
		m_nBENDType = netBEND_1_1000;
	}
	else if (radius > 99.0)
	{
		if (nDeg > 9.0)
		{
			m_nBENDType = netBEND_10_100;
		}
		else
		{
			m_nBENDType = netBEND_7_100;
		}
	}
	else if (radius > 74.0)
	{
		if (nDeg > 14.0)
		{
			m_nBENDType = netBEND_15_75;
		}
		else
		{
			m_nBENDType = netBEND_10_75;
		}
	}
	else if (radius > 49.0)
	{
		if (nDeg > 19.0)
		{
			m_nBENDType = netBEND_22_50;
		}
		else
		{
			m_nBENDType = netBEND_10_50;
		}
	}
	else if (radius > 24.0)
	{
		if (nDeg > 29.0)
		{
			m_nBENDType = netBEND_30_25;
		}
		else
		{
			m_nBENDType = netBEND_10_25;
		}
	}
	else
	{
		m_nBENDType = netBEND_60_10;
	}
	// **********************************

	// old code
	// **********************************
	/*
	double ptX = (ptLB[0] + ptPB[0]) / 2;
	double dist25, dist50, dist75, dist100, nR; 

	// �hel
	double  nRad = GetRadFromGrad(GetNormalizeGrad());

	REALPOS ptCalc, ptCenter, ptEnd; // spo��tan� bod
	//ptCenter.x =  ptLB.z; 
	ptCenter.x = 0;
	ptEnd.x = ptLE[0]; ptEnd.z = ptLE[2];

	// polom�r 100
	nR = ptX - ptLB[0] + 100.;
	ptCenter.z = (float)-nR;
	GetRadixPoint(ptCalc, ptCenter,(float)nR, nRad);
	ptCalc.z  = ptLB[0] - ptCalc.z;
	ptCalc.x  = ptLB[2] + ptCalc.x;
	dist100 = GetDistancePoints(ptCalc.z, ptCalc.x, ptEnd.x, ptEnd.z);
	// polom�r 75
	nR = ptX - ptLB[0] + 75.;
	ptCenter.z  = (float)-nR;
	GetRadixPoint(ptCalc, ptCenter, (float)nR, nRad);
	ptCalc.z  = ptLB[0] - ptCalc.z;
	ptCalc.x  = ptLB[2] + ptCalc.x;
	dist75  = GetDistancePoints(ptCalc.z, ptCalc.x, ptEnd.x, ptEnd.z);
	// polom�r 50
	nR = ptX - ptLB[0] + 50.;
	ptCenter.z  = (float)-nR;
	GetRadixPoint(ptCalc, ptCenter, (float)nR, nRad);
	ptCalc.z  = ptLB[0] - ptCalc.z;
	ptCalc.x  = ptLB[2] + ptCalc.x;
	dist50  = GetDistancePoints(ptCalc.z, ptCalc.x, ptEnd.x, ptEnd.z);
	// polom�r 25
	nR = ptX - ptLB[0] + 25.;
	ptCenter.z  = (float)-nR;
	GetRadixPoint(ptCalc, ptCenter,(float)nR, nRad);
	ptCalc.z  = ptLB[0] - ptCalc.z;
	ptCalc.x  = ptLB[2] + ptCalc.x;
	dist25  = GetDistancePoints(ptCalc.z, ptCalc.x, ptEnd.x, ptEnd.z);

	// typ podle d�lky
	if (dist100 < 0.5f)
	{
		m_nBENDType = netBEND_10_100;
	}
	else if (dist75 < 0.5f)
	{
		m_nBENDType = netBEND_10_75;
	}
	else if (dist50 < 0.5f)
	{
		m_nBENDType = netBEND_10_50;
	}
	else
	{
		m_nBENDType = netBEND_10_25;
	}
	*/
	// **********************************

	// v�e je OK
	return TRUE;
}

// new code
// Added to allow for variable angle
// **********************************
float CNetBEND::GetNormalizeGrad() const
{
	switch(m_nBENDType)
	{
	// new models
	case netBEND_0_2000: return 0.5;
	case netBEND_1_1000: return 1.0;
	case netBEND_7_100:  return 7.5;
	case netBEND_15_75:  return 15.0;
	case netBEND_22_50:  return 22.5;
	case netBEND_30_25:  return 30.0; 
	case netBEND_60_10:  return 60.0;
	// old models
	case netBEND_10_100:
	case netBEND_10_75:
	case netBEND_10_50:
	case netBEND_10_25:  return 10.0;
	default:             ASSERT(FALSE);
	}
	return 10.0;
}

float CNetBEND::GetNormalizeGrad(WORD nType)
{
	switch(nType)
	{
	// new models
	case netBEND_0_2000: return 0.5;
	case netBEND_1_1000: return 1.0;
	case netBEND_7_100:  return 7.5;
	case netBEND_15_75:  return 15.0;
	case netBEND_22_50:  return 22.5;
	case netBEND_30_25:  return 30.0; 
	case netBEND_60_10:  return 60.0;
	// old models
	case netBEND_10_100:
	case netBEND_10_75:
	case netBEND_10_50:
	case netBEND_10_25:  return 10.0;
	default:             ASSERT(FALSE);
	}
	return 10.0;
}

// **************************************


// d�lka (vzd�lenost A.B) podle typu
double CNetBEND::GetNormalizeLength(WORD nType)
{
	double nLen = 0.;
//	vzorec pro v�po�et
/*
	// polom�r
	double nRadix = GetNormalizeRadix(nType);
	// �hel
	double nRad	  = GetRadFromGrad(GetNormalizeGrad()/2);
	// v�po�et 1/2
	double nPom   = sin(nRad)*nRadix;
	// d�lka 
	nLen = 2*nPom;
*/
	// vypo��tan� hodnoty
	switch(nType)
	{
	// new models
	case netBEND_0_2000: nLen = 17.45323714; break;
	case netBEND_1_1000: nLen = 17.45307100; break;
	case netBEND_7_100:  nLen = 13.08062585; break;
	case netBEND_15_75:  nLen = 19.57892883; break;
	case netBEND_22_50:  nLen = 19.50903220; break;
	case netBEND_30_25:  nLen = 12.94095226; break; 
	case netBEND_60_10:  nLen = 10.00000000; break;
	// old models
	case netBEND_10_25:  nLen =  4.35778714/*73832*/; break;
	case netBEND_10_50:  nLen =  8.71557427/*47664*/; break;
	case netBEND_10_75:  nLen = 13.07336141/*2150*/; break;
	case netBEND_10_100: nLen = 17.43114855/*9533*/; break;
	default: ASSERT(FALSE);
	}
	return nLen;
}

// polomer podle typu
double CNetBEND::GetNormalizeRadix(WORD nType)
{
	double nRadix = 0.0;
	switch(nType)
	{
	// new models
	case netBEND_0_2000: nRadix = 2000.0; break;
	case netBEND_1_1000: nRadix = 1000.0; break;
	case netBEND_7_100:  nRadix = 100.0; break;
	case netBEND_15_75:  nRadix = 75.0; break;
	case netBEND_22_50:  nRadix = 50.0; break;
	case netBEND_30_25:  nRadix = 25.0; break; 
	case netBEND_60_10:  nRadix = 10.0; break;
	// old models
	case netBEND_10_25:  nRadix = 25.0; break;
	case netBEND_10_50:  nRadix = 50.0; break;
	case netBEND_10_75:  nRadix = 75.0; break;
	case netBEND_10_100: nRadix = 100.0; break;
	default: ASSERT(FALSE);
	}
	return nRadix;
}

/////////////////////////////////////////////////////////////////////////////
// spec. d�l - SPEC

IMPLEMENT_DYNCREATE( CNetSPEC, CNetBaseStra )

CNetSPEC::CNetSPEC()
: CNetBaseStra(NULL)
{	
	m_bChngHeight = FALSE; 
}

CNetSPEC::CNetSPEC(CPosEdBaseDoc* pOwner)
: CNetBaseStra(pOwner)
{	
	m_bChngHeight = FALSE; 
}

// propojen� s objektem poseidon
BOOL CNetSPEC::CreateFromObjectTemplate(CPosObjectTemplate* pTempl)
{
	if (!CNetBaseStra::CreateFromObjectTemplate(pTempl)) return FALSE;	// nespl�uje z�kladn� podm�nky

	// v�e je OK
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// koncov� d�l - TERM

IMPLEMENT_DYNCREATE( CNetTERM, CNetComponent )

CNetTERM::CNetTERM()
: CNetComponent(NULL)
{
}

CNetTERM::CNetTERM(CPosEdBaseDoc* pOwner)
: CNetComponent(pOwner)
{
}

// propojen� s objektem poseidon
BOOL CNetTERM::CreateFromObjectTemplate(CPosObjectTemplate* pTempl)
{
	if (!CNetComponent::CreateFromObjectTemplate(pTempl)) return FALSE;	// nespl�uje z�kladn� podm�nky

	// m� v�ech body LB, PB, LE, PE ?
	if (!pTempl->ExistNetPoint(CPosObjectTemplate::ptNetLE)) return FALSE;
	if (!pTempl->ExistNetPoint(CPosObjectTemplate::ptNetPE)) return FALSE;

	// v�e je OK
	return TRUE;
}