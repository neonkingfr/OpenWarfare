/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include <projects/objektivlib/LODObject.h>
#include <projects/ObjektivLib/ObjToolProxy.h>
//#include <Poseidon/lib/Shape/SpecLods.hpp>
#include <El/Pathname/Pathname.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace ObjektivLib;

BOOL g_bLoadLODShapes = TRUE;

/////////////////////////////////////////////////////////////////////////////
// CPosObjectTemplate - �ablona objektu

IMPLEMENT_DYNAMIC( CPosObjectTemplate, CObject )

CPosObjectTemplate::CPosObjectTemplate(CPosEdBaseDoc* pDoc)
{
	// vlastnik objektu
	m_pOwner = pDoc;

	m_ID = -1;

	m_posType = posTypePoly;
	// def. rozm�ry (nesmysln�)
	m_nWidth   = 12;
	m_nHeight  = 6;
	
	// def. randomizace
	m_bRandomSize	=
	m_bRandomVert	=
	m_bRandomRota	=  FALSE;

	m_nMinRanSize	=
	m_nMaxRanSize	= DEF_OBJRANDOM_SIZE;
	m_nMinRanVert	=
	m_nMaxRanVert	= DEF_OBJRANDOM_VERT;
	m_nMinRanRota	=
	m_nMaxRanRota	= DEF_OBJRANDOM_ROTA;

	m_nTimesUsed = 0;

	m_autoCenter = true;
	m_BoundingCenter = VZero;
  
	// body pro ur�en� s�t�
	m_bIsNetObject	= FALSE;
	for (int i = 0; i < ptNetCount; ++i)
	{
		m_ptNetPts[i] = VZero;
	}
}

CPosObjectTemplate::~CPosObjectTemplate()
{
}

void CPosObjectTemplate::MakeDefaultValues()
{
	m_nObjType = objTpUndef;

	m_clrFrame =
	m_clrEntire= DEFAULT_COLOR;
	m_BoundingCenter = VZero;

	// body pro ur�en� s�t�
	m_bIsNetObject	= FALSE;
	m_posType = posTypePoly;

	for (int i = 0; i < ptNetCount; ++i)
	{
		m_ptNetPts[i] = VZero;
	}
}

void CPosObjectTemplate::CopyFrom(const CPosObjectTemplate* pSrc)
{
	m_pOwner         = pSrc->m_pOwner;

	m_ID             = pSrc->m_ID;
	m_nTimesUsed     = pSrc->m_nTimesUsed;

	m_posType        = pSrc->m_posType;
	m_sObjFile       = pSrc->m_sObjFile;
	m_sObjName       = pSrc->m_sObjName;
	m_nObjType       = pSrc->m_nObjType;
	m_clrFrame       = pSrc->m_clrFrame;
	m_clrEntire      = pSrc->m_clrEntire;
	m_nWidth         = pSrc->m_nWidth;
	m_nHeight        = pSrc->m_nHeight;
	m_autoCenter     = pSrc->m_autoCenter;
	m_BoundingCenter = pSrc->m_BoundingCenter;

	// randomizace objektu
	m_bRandomSize    = pSrc->m_bRandomSize;
	m_nMinRanSize    = pSrc->m_nMinRanSize;
	m_nMaxRanSize    = pSrc->m_nMaxRanSize;
	m_bRandomVert    = pSrc->m_bRandomVert;
	m_nMinRanVert    = pSrc->m_nMinRanVert;
	m_nMaxRanVert    = pSrc->m_nMaxRanVert;
	m_bRandomRota    = pSrc->m_bRandomRota;
	m_nMinRanRota    = pSrc->m_nMinRanRota;
	m_nMaxRanRota    = pSrc->m_nMaxRanRota;

	// body pro ur�en� s�t�
	m_bIsNetObject   = pSrc->m_bIsNetObject;

	for (int i = 0; i < ptNetCount; ++i)
	{
		m_ptNetPts[i] = pSrc->m_ptNetPts[i];
	}
}

// serializace
void CPosObjectTemplate::DoSerialize(CArchive& ar, DWORD nVer)
{
	MntSerializeString(ar, m_sObjFile);
	MntSerializeString(ar, m_sObjName);
	MntSerializeInt(ar, m_nObjType);
	MntSerializeColor(ar, m_clrFrame);
	MntSerializeColor(ar, m_clrEntire);
	MntSerializeDouble(ar, m_nWidth);
	MntSerializeDouble(ar, m_nHeight);

	if (nVer > 36) MntSerializeInt(ar, m_ID);

	if (nVer < 11)
	{	// nezn� BoundingCenter
		ASSERT(!ar.IsStoring());
		//UpdateParamsFromSourceFile(); will be read before function end
	} 
	else
	{	// na�te BoundingCenter
		MntSerializeFloat(ar, m_BoundingCenter[0]);
		MntSerializeFloat(ar, m_BoundingCenter[1]);
		MntSerializeFloat(ar, m_BoundingCenter[2]);

		if (nVer >= 53)
		{
			MntSerializeDouble(ar, m_drawWidth);
			MntSerializeDouble(ar, m_drawHeight);

			MntSerializeFloat(ar, m_drawBoundingCenter[0]);
			MntSerializeFloat(ar, m_drawBoundingCenter[1]);
			MntSerializeFloat(ar, m_drawBoundingCenter[2]);   
		}

		if (nVer >= 40) MntSerializeBOOL(ar, m_autoCenter);
	}

	if (nVer >= 17)
	{	// randomizace objektu
		MntSerializeBOOL(ar, m_bRandomSize);
		MntSerializeDouble(ar, m_nMinRanSize);
		MntSerializeDouble(ar, m_nMaxRanSize);
		MntSerializeBOOL(ar, m_bRandomVert);
		MntSerializeDouble(ar, m_nMinRanVert);
		MntSerializeDouble(ar, m_nMaxRanVert);
		MntSerializeBOOL(ar, m_bRandomRota);
		MntSerializeDouble(ar, m_nMinRanRota);
		MntSerializeDouble(ar, m_nMaxRanRota);
	}

	if (nVer >= 19)
	{	// body pro ur�en� s�t�
		MntSerializeBOOL(ar, m_bIsNetObject);
		if (m_bIsNetObject)
		{
			for (int i = 0; i < ptNetCount; ++i)
			{
				MntSerializeFloat(ar, m_ptNetPts[i][0]);
				MntSerializeFloat(ar, m_ptNetPts[i][1]);
				MntSerializeFloat(ar, m_ptNetPts[i][2]);
			}
		}
	}

	if (nVer >= 40) // store/load snap points
	{
		int n = 0;
		if (ar.IsLoading())
		{      
			MntSerializeInt(ar, n);
			m_snapPlugs.Resize(n);
		}
		else
		{
			n = m_snapPlugs.Size();
			MntSerializeInt(ar,n);
		}

		for (int i = 0; i < n; ++i)
		{
			SnapPlug& plug = m_snapPlugs[i];
			MntSerializeString(ar, plug.name);
			MntSerializeInt(ar, plug.type);

			int nPoints = 0;
			if (ar.IsLoading())
			{      
				MntSerializeInt(ar, nPoints);
				plug.pos.Resize(nPoints);
			}
			else
			{
				nPoints = plug.pos.Size();
				MntSerializeInt(ar, nPoints);
			}

			for (int j = 0; j < nPoints; ++j)
			{      
				MntSerializeFloat(ar, plug.pos[j][0]);
				MntSerializeFloat(ar, plug.pos[j][1]);
				MntSerializeFloat(ar, plug.pos[j][2]);
			}
		}
	}

	// posType
	if (nVer > 50)
	{
		int temp = m_posType;
		MntSerializeInt(ar, temp);
		m_posType = (PosType) temp;
	}
	else
	{
		if (m_nObjType == objTpNatural)
		{
			m_posType = posTypeEllipse; 
		}
		else
		{
			m_posType = posTypePoly; 
		}
	}

	// !!! Added by GRALFIM 99/10/07 - fixed problems with flying objects
	if (ar.IsLoading() && (g_bLoadLODShapes || nVer < 53))
	{
		UpdateParamsFromSourceFile();
	}

#if 1  // check for infinities...
	if (m_bRandomSize && (!_finite(m_nMinRanSize) || !_finite(m_nMaxRanSize)))
	{
		LogF("Warning: Infinite randomization of size, template %s. Randomization is disabled", m_sObjName); 
		m_bRandomSize = false;
		m_nMinRanSize = 1;
		m_nMaxRanSize = 1;
	}

	if (m_bRandomVert && (!_finite(m_nMinRanVert) || !_finite(m_nMaxRanVert)))
	{
		LogF("Warning: Infinite randomization of angle, template %s. Randomization is disabled", m_sObjName); 
		m_bRandomVert = false;
		m_nMinRanVert = 0;
		m_nMaxRanVert = 0;
	}

	if (m_bRandomRota && (!_finite(m_nMinRanRota) || !_finite(m_nMaxRanRota)))
	{
		LogF("Warning: Infinite randomization of rotation, template %s. Randomization is disabled", m_sObjName); 
		m_bRandomRota = false;
		m_nMinRanRota = 0;
		m_nMaxRanRota = 0;
	}

	if (!_finite(m_BoundingCenter[0]) || !_finite(m_BoundingCenter[1]) || !_finite(m_BoundingCenter[2]))
	{
		LogF("Warning: Infinite bounding center, template %s. Bounding center changed to (0,0,0)", m_sObjName); 
		m_BoundingCenter = VZero;
	}

	if (!_finite(m_nWidth) || !_finite(m_nHeight))
	{
		LogF("Warning: Infinite template size, template %s. size changed to (1,1)", m_sObjName); 
		m_nWidth = 1;
		m_nHeight = 1; 
	}

#endif
}

COLORREF CPosObjectTemplate::GetObjectFrameColor(const CPosEdCfg* pCfg) const
{ 
	if (m_clrFrame != DEFAULT_COLOR) return m_clrFrame;
	ASSERT(pCfg != NULL);

	switch(m_nObjType)
	{
	case objTpNatural:   return pCfg->m_cDefColors[CPosEdCfg::defcolNObFrm];
	case objTpPeople:    return pCfg->m_cDefColors[CPosEdCfg::defcolPObFrm];
	case objTpNewRoads:  return pCfg->m_cDefColors[CPosEdCfg::defcolNRObFrm];
	}
	return pCfg->m_cDefColors[CPosEdCfg::defcolUObFrm];
}

COLORREF CPosObjectTemplate::GetObjectEntireColor(const CPosEdCfg* pCfg) const
{
	if (m_clrEntire != DEFAULT_COLOR) return m_clrEntire;
	ASSERT(pCfg!=NULL);

	switch(m_nObjType)
	{
	case objTpNatural:  return pCfg->m_cDefColors[CPosEdCfg::defcolNObEnt];
	case objTpPeople:   return pCfg->m_cDefColors[CPosEdCfg::defcolPObEnt];
	case objTpNewRoads: return pCfg->m_cDefColors[CPosEdCfg::defcolNRObEnt];
	}
	return pCfg->m_cDefColors[CPosEdCfg::defcolUObEnt];
}

// vykreslen� v ListBox
void CPosObjectTemplate::DrawLBItem(CDC* pDC,CRect& rPos) const
{
	CRect rText = rPos; 
	rText.left += 2;
	// jm�no
	::DrawTextA(pDC->m_hDC, m_sObjName, -1, &rText, DT_LEFT | DT_NOPREFIX | DT_SINGLELINE | DT_VCENTER | DT_TABSTOP);
}

COLORREF CPosObjectTemplate::GetLBItemColor() const
{	
	return GetObjectEntireColor(&(GetPosEdEnvironment()->m_cfgCurrent)); 
}

// update dat ze souboru p3d

void GetModelBoundingBox(LODObject& obj, const char* basePath, int recursion, int recursionDeep, bool allLods, Vector3& minPos, Vector3& maxPos)
{  
	// calculate object min max... 
	if (!allLods)
	{
		// update bounding box only from one lod
		int useOnlyLod = obj.FindLevel(0);
		if (useOnlyLod < 0) return; 

		obj.SelectLevel(useOnlyLod);
		ObjectData& geomLevel = obj;
		ObjToolProxy& proxyTool = geomLevel.GetTool<ObjToolProxy>();

	    Selection sel(&geomLevel); 

	    if (proxyTool.EnumProxies() > 0)
		{
			// without proxies
			int proxCount = proxyTool.EnumProxies();
			NamedSelection** proxies = (NamedSelection **)alloca(sizeof(NamedSelection *)*proxCount);
			proxyTool.EnumProxies(proxies);

			for (int i = 0; i < proxCount; ++i)
			{
				sel += *(proxies[i]);
			}

			sel = ~sel;
		}
		else
		{
			sel.SelectAll();
		}      

		int n = geomLevel.NPoints();    
		for (int i = 0; i < n; ++i)
		{
			if (!sel.PointSelected(i)) continue;

			Vector3& pos = geomLevel.Point(i);

			for (int j = 0; j < 3; ++j)
			{
				if (minPos[j] > pos[j]) minPos[j] = pos[j];
				if (maxPos[j] < pos[j]) maxPos[j] = pos[j];
			}
		}
	}

	int subPartLod = obj.NLevels() > 1 ? obj.FindLevelExact((float) LOD_SUB_PARTS) : -1;

	for (int lod = 0; lod < obj.NLevels(); ++lod)
	{
		// find bounding box 
		obj.SelectLevel(lod);
		ObjectData& geomLevel = obj;

		int n = geomLevel.NPoints();
		if (allLods)
		{
			for (int i = 0; i < n; ++i)
			{
				Vector3& pos = geomLevel.Point(i);

				for (int j = 0; j < 3; ++j)
				{
					if (minPos[j] > pos[j]) minPos[j] = pos[j];
					if (maxPos[j] < pos[j]) maxPos[j] = pos[j];
				}
			}
		}

		// Get proxies from model... 
		if (recursion == recursionDeep || basePath == NULL || subPartLod == lod) // do not take subparts it will not affect bb
		{
			continue;
		}

	    ObjToolProxy& proxyTool = geomLevel.GetTool<ObjToolProxy>();

		NamedSelection** proxies = (NamedSelection **)alloca(sizeof(NamedSelection *)*proxyTool.EnumProxies());
		int proxCount = proxyTool.EnumProxies(proxies);

		for (int i = 0; i < proxCount; ++i)
		{
			RString proxname;
			proxyTool.GetProxyFilename(proxies[i]->Name(), proxname.CreateBuffer(strlen(proxies[i]->Name())));
			int face = proxyTool.FindProxyFace(proxies[i]);
			if (face < 0) continue; 

			if (proxname[0] == '\\') proxname = proxname.Data() + 1;
			proxname = proxname + ".p3d";      
			proxname = RString(basePath, proxname);      

			LODObject lodObjProxy;
			if (0 > lodObjProxy.Load(Pathname(proxname), NULL, NULL))
			{
				RptF("Warning: Loading of proxy %s failed.", (LPCSTR) proxname);    
				continue;
			}

			Vector3 minPosProxy(FLT_MAX, FLT_MAX, FLT_MAX);
			Vector3 maxPosProxy(-FLT_MAX, -FLT_MAX, -FLT_MAX);

			GetModelBoundingBox(lodObjProxy, basePath, recursion + 1, recursionDeep, allLods, minPosProxy, maxPosProxy);

			FaceT fc(geomLevel, face);
			Matrix4 proxmx;
			proxyTool.GetProxyTransform(fc, proxmx);

			minPosProxy = proxmx * minPosProxy;
			maxPosProxy = proxmx * maxPosProxy;
			saturateMin(minPos[0], minPosProxy[0]);
			saturateMin(minPos[1], minPosProxy[1]);
			saturateMin(minPos[2], minPosProxy[2]);

			saturateMax(maxPos[0], maxPosProxy[0]);
			saturateMax(maxPos[1], maxPosProxy[1]);
			saturateMax(maxPos[2], maxPosProxy[2]);
		}
	}
}

void CPosObjectTemplate::UpdateParamsFromSourceFile()
{
	CString sObjFile;
	GetFullFileName(sObjFile);

	LODObject lodObj;
	if (0 > lodObj.Load(Pathname((LPCSTR) sObjFile), NULL, NULL))
	{
		RptF("Warning: Loading of %s failed.", (LPCSTR) sObjFile);    
		return;
	}
  
	//int geomIndex = lodObj.FindLevel(0);
	int memIndex = lodObj.FindLevelExact((float) LOD_MEMORY);
	int geometryIndex = lodObj.FindLevelExact((float) LOD_GEOMETRY);

	m_autoCenter = TRUE;
	bool propertyFound = false;
	if (geometryIndex >= 0)
	{
		// try to load property _autocenter
		lodObj.SelectLevel(geometryIndex);
		ObjectData& geometryLevel = lodObj;

		const char* val = geometryLevel.GetNamedProp("autocenter");
		if(val)
		{
			propertyFound = true;
			if (atoi(val) == 0) m_autoCenter = FALSE;
		}
		m_placement = geometryLevel.GetNamedProp("placement");
	}

	if (!propertyFound)  
	{
		// check also first lod

		lodObj.SelectLevel(0);
		ObjectData& geomLevel = lodObj;

		const char* val = geomLevel.GetNamedProp("autocenter");
		if (val && atoi(val) == 0) m_autoCenter = FALSE;
	}

	// calculate bounding center from all lods
	Vector3 minPos(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 maxPos(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	GetModelBoundingBox(lodObj, "p:\\", 0, 0, true, minPos, maxPos);

	Vector3 minPosWithProxies(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 maxPosWithProxies(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	GetModelBoundingBox(lodObj, "p:\\", 0, 10, false,  minPosWithProxies, maxPosWithProxies);   

	m_nWidth  = maxPos[0] - minPos[0];
	m_nHeight = maxPos[2] - minPos[2];
	m_BoundingCenter = (maxPos + minPos) / 2;

	m_drawWidth	         = maxPosWithProxies[0] - minPosWithProxies[0];
	m_drawHeight         = maxPosWithProxies[2] - minPosWithProxies[2];
	m_drawBoundingCenter = (maxPosWithProxies + minPosWithProxies) / 2;

	if (memIndex >= 0)
	{
		lodObj.SelectLevel(memIndex);
		ObjectData& memLevel = lodObj;

		// body pro ur�en� s�t�
		if (m_bIsNetObject)
		{
			for (int i = 0; i < ptNetCount; ++i)
			{
				CString sName;
				GetNetPtName(sName, i);
				NamedSelection* sel = memLevel.GetNamedSel(sName);
				if (sel != NULL && sel->NPoints() == 1)
				{
					int j = 0;
					for (; j < memLevel.NPoints() && !sel->PointSelected(j); ++j);
					m_ptNetPts[i] = memLevel.Point(j);

					if (m_autoCenter) m_ptNetPts[i] -= m_BoundingCenter; 
				}
				else
				{
					m_ptNetPts[i] = VZero;
				}
			}
		} 
		else
		{	// reset
			for (int i = 0; i < ptNetCount; ++i)
			{
				m_ptNetPts[i] = VZero;
			}
		}

		// load snap points
		// snap points starts with prefix
		m_snapPlugs.Resize(0);

#define SNAP_PREFIX ".sp_"
    int SNAP_PREFIX_LENGTH = strlen(SNAP_PREFIX);    
    char buffer[512];

		for (int i = 0; i < MAX_NAMED_SEL; ++i)
		{
			NamedSelection* sel = memLevel.GetNamedSel(i);
			if (!sel || sel->NPoints() != 1) continue;

			strcpy(buffer, sel->Name());
     
			_strlwr(buffer);
			if (strncmp(SNAP_PREFIX, buffer, SNAP_PREFIX_LENGTH) == 0)
			{
				// snap point  founded decompose it into name and 
				char* pointNum = strrchr(buffer, '_');
				if (!pointNum)
				{
					RptF("Warning: Wrong format of snap point %s in model %s.", buffer, sObjFile);
					continue;
				}

				pointNum++;

				int pointIndx = 0;
				sscanf(pointNum, "%d", &pointIndx);

				if (pointIndx > 99)
				{
					RptF("Warning: Wrong format of snap point %s in model %s.", buffer, sObjFile);
					continue;
				}         

				// find name 
				char* nameBg = buffer + SNAP_PREFIX_LENGTH;                    

				int nameLength = pointNum - nameBg;
				char name[512];
				strncpy(name, nameBg, nameLength);
				name[nameLength] = '\0';

				int indx = m_snapPlugs.FindKey(name);
				if (indx < 0)
				{
					SnapPlug& plug = m_snapPlugs.Append();

					plug.name = name;
					plug.pos.Access(pointIndx);

					int j = 0;
					for (; j < memLevel.NPoints() && !sel->PointSelected(j); ++j);
					plug.pos[pointIndx] = memLevel.Point(j); //TODO: Just for testing
				}
				else
				{
					m_snapPlugs[indx].pos.Access(pointIndx);
					int j = 0;
					for (; j < memLevel.NPoints() && !sel->PointSelected(j); ++j);
					m_snapPlugs[indx].pos[pointIndx] = memLevel.Point(j); //TODO: Just for testing         
				}
			}    
		}

		// clean names and set up plug type
		for (int i = 0; i < m_snapPlugs.Size(); ++i)
		{
			SnapPlug& plug = m_snapPlugs[i];
			int length = plug.name.Find(_T('_'));

			//SetUp Type
			if (plug.name[length + 1] == _T('a'))
			{
				plug.type = 1;
			}
			else
			{
				if (plug.name[length + 1] != _T('v'))
				{
					RptF("Warning: Wrong format of snap point %s in model %s.", buffer, sObjFile);
				}
				plug.type = 0;
			}

			plug.name = plug.name.Left(length);
			plug.pos.Compact();      

			if (m_autoCenter)
			{
				for (int j = 0; j < plug.pos.Size(); ++j)
				{
					plug.pos[j] -= m_BoundingCenter;
				}
			}
		}

		m_snapPlugs.Compact();
	}
}

bool CPosObjectTemplate::AreSameDataInSourceFile(CString sObjFile) const
{
	// data z VisitorShape]
	LODObject lodObj;
	if (0 > lodObj.Load(Pathname((LPCSTR) sObjFile), NULL, NULL))
	{
		RptF("Warning: Loading of %s failed.", (LPCSTR) sObjFile);    
		return false;
	}
	int memIndex = lodObj.FindLevelExact((float) LOD_MEMORY);
	int geometryIndex = lodObj.FindLevelExact((float) LOD_GEOMETRY);
 
	bool propertyFound = false;
	if (geometryIndex >= 0)
	{
		// try to load property _autocenter
		lodObj.SelectLevel(geometryIndex);
		ObjectData& geometryLevel = lodObj;

		const char* val = geometryLevel.GetNamedProp("autocenter");
		if(val)
		{
			propertyFound = true;
			if (atoi(val) == 0) 
			{
				//m_autoCenter=FALSE;
				if (m_autoCenter) return false;
			}
		}
	}

	if (!propertyFound)  
	{
		// check also first lod
		lodObj.SelectLevel(0);
		ObjectData& geomLevel = lodObj;

		const char* val = geomLevel.GetNamedProp("autocenter");
		if(val && atoi(val) == 0)
		{
			//m_autoCenter=FALSE;
			if (m_autoCenter) return false;
		}
	}
	else if (!m_autoCenter)
	{
		return false;
	}

	// calculate bounding center from all lods
	Vector3 minPos(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 maxPos(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	for (int lod = 0; lod < lodObj.NLevels(); ++lod)
	{
		// find bounding box 
		lodObj.SelectLevel(lod);
		ObjectData& geomLevel = lodObj;

		int n = geomLevel.NPoints();
		for (int i = 0; i < n; ++i)
		{
			Vector3& pos = geomLevel.Point(i);

			for (int j = 0; j < 3; ++j)
			{
				if (minPos[j] > pos[j]) minPos[j] = pos[j];
				if (maxPos[j] < pos[j]) maxPos[j] = pos[j];
			}
		}
	}

	const float maxDiff = 0.01f;

  //m_nWidth	= maxPos[0] - minPos[0];
	if (fabs(m_nWidth - (maxPos[0] - minPos[0])) > maxDiff) return false;
  
  //m_nHeight	= maxPos[2] - minPos[2];
	if (fabs(m_nHeight - (maxPos[2] - minPos[2])) > maxDiff) return false;

  //m_BoundingCenter = (maxPos + minPos) / 2;
	if (m_BoundingCenter.Distance2((maxPos + minPos) / 2) > maxDiff * maxDiff) return false;

	if (memIndex >= 0)
	{
		lodObj.SelectLevel(memIndex);
		ObjectData& memLevel = lodObj;

		// body pro ur�en� s�t�
		if (m_bIsNetObject)
		{
			for (int i = 0; i < ptNetCount; ++i)
			{
				CString sName;
				GetNetPtName(sName, i);
				NamedSelection* sel = memLevel.GetNamedSel(sName);
				Vector3 pt; 
				if (sel != NULL && sel->NPoints() == 1)
				{
					int j = 0;
					for(; j < memLevel.NPoints() && !sel->PointSelected(j); ++j);
					pt = memLevel.Point(j);         
				}
				else
				{
					pt = VZero;
				}

				if (m_autoCenter) pt -= m_BoundingCenter;  
				if (pt.Distance2(m_ptNetPts[i]) > maxDiff * maxDiff) return false;
			}
		} 
    
   /// DO NOT TEST SNAP POINTS 
/*
	// snap points starts with prefix
    const int SNAP_PREFIX_LENGTH = strlen(SNAP_PREFIX);    
    char buffer[512];

    for(int i = 0; i < MAX_NAMED_SEL; i++)
    {
      NamedSelection * sel = memLevel.GetNamedSel(i);
      if (!sel || sel->NPoints() != 1)
        continue;

      strcpy(buffer, sel->Name());

      _strlwr(buffer);
      if (strncmp(SNAP_PREFIX, buffer, SNAP_PREFIX_LENGTH) == 0)
      {
        // snap point  founded decompose it into name and 
        char * pointNum = strrchr(buffer,'_');
        if (!pointNum)
        {
          RptF("Warning: Wrong format of snap point %s in model %s.", buffer, sObjFile);
          continue;
        }

        pointNum++;

        int pointIndx = 0;
        sscanf(pointNum,"%d",&pointIndx);

        if (pointIndx > 99)
        {
          RptF("Warning: Wrong format of snap point %s in model %s.", buffer, sObjFile);
          continue;
        }         

        // find name 
        char * nameBg = buffer + SNAP_PREFIX_LENGTH;                    

        int nameLength = pointNum - nameBg;
        char name[512];
        strncpy(name, nameBg, nameLength);
        name[nameLength] = '\0';       

        int type;
        char * tmp = strchr(name,_T('_'));
        if (tmp == NULL)
        {  
          RptF("Warning: Wrong format of snap point %s in model %s.", buffer, sObjFile);
          continue;
        }         

        int pureNameLength = name - strchr(name,_T('_'));
        //Type
        if (name[pureNameLength + 1] == _T('a'))
          type = 1;
        else
        {
          if (name[pureNameLength + 1] != _T('v'))
            RptF("Warning: Wrong format of snap point %s in model %s.", buffer, sObjFile);

          type = 0;
        }
        name[pureNameLength] = '\0';

        // find pos
        int j = 0;
        for(; j < memLevel.NPoints() && !sel->PointSelected(j); j++);
        Vector3 pos = memLevel.Point(j); 
        if (m_autoCenter)
          pos -= m_BoundingCenter;
        
        for(; j < m_snapPlugs.Size(); j++)
        {
          const SnapPlug& plug = m_snapPlugs[j];
          if (plug.name == name && plug.type == type && plug.pos.Size() > pointIndx)
          {
            if (plug.pos[pointIndx].Distance2(pos) < maxDiff * maxDiff)
            {
              break;
            }
          }
        }

        if (j == m_snapPlugs.Size())
          return false;
      }
    }
*/
	}
	return true; 
}

// vytvo�en� z buldozeru
/*void CPosObjectTemplate::CreateFromBuldozerRegistr(const SPosMessage& sMsg)
{
	ASSERT(sMsg._iMsgType == REGISTER_OBJECT_TYPE);
	// objekt nem� def. typ
	m_nObjType = objTpUndef;
  MESSAGE_DATA_CONST_VAR(REGISTER_OBJECT_TYPE, &sMsg, msgData);
	// soubor ze souboru Buldozeru
	SetFileFromBuldozer(msgData.szName);
	// jm�no ze souboru
	GetNameFromFile(m_sObjName);
	// nejsou d�ny speci�ln� barvy
	m_clrFrame =
	m_clrEntire= DEFAULT_COLOR;

	// rozm�ry ze zdrojov�ho souboru
	UpdateParamsFromSourceFile();
};*/

// vytvo�en� ze souboru
void CPosObjectTemplate::CreateFromObjFile(LPCTSTR pFileName, LPCTSTR useName)
{
	// objekt nem� def. typ
	m_nObjType = objTpUndef;
	// soubor z pln� cesty
//	SetFileFromFullPath(pFileName);
	m_sObjFile  = pFileName;
	m_sObjFile.MakeLower();

    if (useName) 
	{
		m_sObjName = useName;
	}
    else
	{
	    // jm�no ze souboru
	    GetNameFromFile(m_sObjName);
	}

	// nejsou d�ny speci�ln� barvy
	m_clrFrame =
	m_clrEntire= DEFAULT_COLOR;

	// rozm�ry ze zdrojov�ho souboru
	UpdateParamsFromSourceFile();
}

// p�evod jmen a soubor�
// pl� cesta k souboru pac (m_sObjFile)
CString& CPosObjectTemplate::GetFullFileName(CString& sFile) const
{
	ASSERT(m_sObjFile.GetLength() > 0);
	ASSERT(m_pOwner != NULL);
	m_pOwner->GetProjectDirectory(sFile, CPosEdBaseDoc::dirObj);
	sFile += m_sObjFile;
	return sFile;
}

// realativn� cesta k souboru pac (m_sObjFile)
CString& CPosObjectTemplate::GetBuldozerFileName(CString& sFile) const
{
	ASSERT(m_sObjFile.GetLength() > 0);

	//CString sPom;
	ASSERT(m_pOwner != NULL);
	m_pOwner->GetProjectDirectory(sFile, CPosEdBaseDoc::dirObj, TRUE);	
	sFile += m_sObjFile;
	return sFile;
}

// jm�no podle m_sObjFile
void CPosObjectTemplate::GetNameFromFile(CString& sName) const 
{
	ASSERT(m_sObjFile.GetLength() > 0);
	sName = m_sObjFile;
	int nBack = sName.ReverseFind(_T('.'));
	if (nBack > 0)
	{
		(sName.GetBuffer(0))[nBack] = _T(0);
		 sName.ReleaseBuffer();
	}
}

// m_sObjFile z pln� cesty
void CPosObjectTemplate::SetFileFromFullPath(LPCTSTR pPath)	 
{
	CString sName = pPath;
	CString sDir;
	ASSERT(m_pOwner != NULL);
	m_pOwner->GetProjectDirectory(sDir, CPosEdBaseDoc::dirObj);
	MakeLongDirStr(sDir, sDir);
	// find relative path to sDir. pPath must start with sDir;
	ASSERT(sDir.GetLength() < sName.GetLength());
	
	m_sObjFile  = (LPCTSTR)(sName.GetBuffer(0) + sDir.GetLength());
	m_sObjFile.MakeLower();
}

// pojmenovan� body pro s�
void CPosObjectTemplate::GetNetPtName(CString& sName, int nPtNet)
{
	switch(nPtNet)
	{
	case ptNetLB: sName = _T("LB"); break;
	case ptNetPB: sName = _T("PB"); break;
	case ptNetLE: sName = _T("LE"); break;
	case ptNetPE: sName = _T("PE"); break;
	case ptNetLH: sName = _T("LH"); break;
	case ptNetLD: sName = _T("LD"); break;
	case ptNetPH: sName = _T("PH"); break;
	case ptNetPD: sName = _T("PD"); break;
	default: ASSERT(FALSE);
	}
}

BOOL CPosObjectTemplate::ExistNetPoint(int nPtNet) const
{
	Vector3 pt = GetNetPoint(nPtNet);

	if (pt == VZero) return FALSE;
	return TRUE;
}

Vector3 CPosObjectTemplate::GetNetPoint(int nPtNet) const
{
	ASSERT((nPtNet >= 0) && (nPtNet < ptNetCount));
	return m_ptNetPts[nPtNet];
}

// bod A v sou�adnic�ch objektu
Vector3 CPosObjectTemplate::GetNetPointA() const
{
	ASSERT(ExistNetPoint(ptNetLE));
	ASSERT(ExistNetPoint(ptNetPE));
	
	Vector3 ptA;
	ptA = (m_ptNetPts[ptNetLE] + m_ptNetPts[ptNetPE]) / 2.f;
	return ptA;
}

/////////////////////////////////////////////////////////////////////////////
// CMgrObjects - mana�er objekt�

CMgrObjects::CMgrObjects()
{
}

CMgrObjects::~CMgrObjects()
{	
	Destroy(); 
}

void CMgrObjects::Destroy()
{
	DestroyArrayObjects(&m_ObjTemplNatural);
	DestroyArrayObjects(&m_ObjTemplPeoples);
	DestroyArrayObjects(&m_ObjTemplNewRoads);
}

BOOL CMgrObjects::CopyTemplates(CObArray& dest,const CObArray& src)
{
	DestroyArrayObjects(&dest);
	TRY
	{
		for(int i = 0; i < src.GetSize(); ++i)
		{
			CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(src[i]);
			if (pTempl)
			{
				ASSERT_KINDOF(CPosObjectTemplate, pTempl);
				CPosObjectTemplate* pCopy = new CPosObjectTemplate(pTempl->m_pOwner);
				// kop�ruji data
				pCopy->CopyFrom(pTempl);
				// vlo��m �ablonu
				dest.Add(pCopy);
			}
		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		return FALSE;
	}
	END_CATCH_ALL
	return TRUE;
}

// serializace
void CMgrObjects::DoSerializeTemplates(CArchive& ar, DWORD nVer, CObArray& Data, CPosEdBaseDoc* pDoc)
{
	int nCount = Data.GetSize();
	MntSerializeInt(ar, nCount);
	for(int i = 0; i < nCount; ++i)
	{
		if (ar.IsStoring())
		{
			CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(Data[i]);
			ASSERT(pTempl != NULL);
			pTempl->DoSerialize(ar, nVer);
		} 
		else
		{
			CPosObjectTemplate* pTempl = new CPosObjectTemplate(pDoc);
			Data.Add(pTempl);
			pTempl->DoSerialize(ar, nVer);
		}
	}
}

void CMgrObjects::DoSerialize(CArchive& ar, DWORD nVer)
{
	BOOL bEmpty = (m_ObjTemplNatural.GetSize() == 0) && (m_ObjTemplPeoples.GetSize() == 0) && (m_ObjTemplNewRoads.GetSize() == 0);
	MntSerializeBOOL(ar, bEmpty);
	if (bEmpty) return;	// tento manazer je pr�zdn�

	if (!ar.IsStoring()) Destroy();	// vypr�zdn�n� 

	// serializace p��rodn�ch objekt�
	DoSerializeTemplates(ar, nVer, m_ObjTemplNatural, m_pOwner);
	// serializace lidsk�ch objekt�
	DoSerializeTemplates(ar, nVer, m_ObjTemplPeoples, m_pOwner);

	// serializace new roads
	DoSerializeTemplates(ar, nVer, m_ObjTemplNewRoads, m_pOwner);
}

/////////////////////////////////////////////////////////////////////
// ObjectBank

int ObjectBank::_lastUsedID = 0;

void ObjectBank::DoSerialize(CArchive& ar, int nVer, CPosEdBaseDoc* pDoc)
{
	if (nVer < 37) return;

	if (ar.IsStoring())
	{  
		int size = base::Size();
		MntSerializeInt(ar, size);
		for (int i = 0; i < size; ++i)
		{
			Get(i)->DoSerialize(ar, nVer);
		}
	}
	else
	{
		int size;
		MntSerializeInt(ar, size);
		Reserve(size, size);
		Resize(0);
		for (int i = 0; i < size; ++i)
		{
			Ref<CPosObjectTemplate> obj = new CPosObjectTemplate(pDoc);
 
			obj->DoSerialize(ar, nVer);
			if (obj->m_ID > _lastUsedID) _lastUsedID = obj->m_ID;

			if (obj->m_nObjType == CPosObjectTemplate::objTpUndef)
			{
				RptF("Undef object template removed. %s" , obj->GetFileName());
			}
			else
			{
				Add(obj);
			}
		}
	}
}

bool ObjectBank::HardCopyFrom(const ObjectBank& src, int type)
{
	Clear();  

	// copy all objects template from src into this
	for (int i = 0; i < src.Size(); ++i)  
	{
		if (src[i]->m_nObjType == type) base::Add(new CPosObjectTemplate(*src[i]));  
	}

	return true;
}

bool ObjectBank::SyncTarget(ObjectBank& dest, int type)
{  
	//Remove templates
	for (int i = 0; i < dest.Size(); ++i)  
	{
		if (dest[i]->m_nObjType == type &&  0 > FindKey(dest[i]->m_ID))
		{
			ASSERT(dest[i]->m_nTimesUsed < 1);
			dest.Delete(i);
			i--;
		}    
	}

	// copy all objects template from src into this
	for (int i = 0; i < base::Size(); ++i)  
	{
		Ref<CPosObjectTemplate> templ = Get(i);

		Ref<CPosObjectTemplate> templDest = dest.FindEx(templ->m_ID);
		if (templDest.IsNull())   
		{
			dest.AddID(templ);
		}
		else
		{
			templDest->CopyFrom(templ);
		}
	}

	return true;
}