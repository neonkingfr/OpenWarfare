/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// p�evod stup�� na radi�ny a zp�t

// p�evod stup�� na radi�ny a zp�t
double GetRadFromGrad(int nGra)
{	
	return ((6.28318530718 * (double)nGra) / 360.); 
}

double GetRadFromGrad(double nGra)
{	
	return ((6.28318530718 * nGra) / 360.); 
}

double GetNormalizedGrad(double nGra)
{
	while(nGra < 0.0)    nGra += 360.0;
	while(nGra >= 360.0) nGra -= 360.0;
	return nGra;
}

float GetNormalizedGrad(float nGra)
{
	while (nGra < 0.0f)    nGra += 360.0f;
	while (nGra >= 360.0f) nGra -= 360.0f;
	return nGra;
}

int	GetNormalizedGrad(int nGra)
{
	while (nGra < 0)    nGra += 360;
	while (nGra >= 360) nGra -= 360;
	return nGra;
}

int GetIntGradFromRad(double nRad)
{
	int	  nPom = (int)Round(nRad * (360. / 6.28318530718), 0);
	while (nPom < 0)    nPom += 360;
	while (nPom >= 360) nPom -= 360;
	return nPom;
}

double GetGradFromRad(double nRad)
{
	return (nRad * 360.0 / 6.28318530718);
}

/////////////////////////////////////////////////////////////////////////////
// vzd�lenost bod�

double  GetDistancePoints(double xx,double xy,double yx,double yy)
{
	double dist;
	if (xx==yx) dist = xy-yy;
	else
	if (xy==yy) dist = xx-yx;
	else
	{
		dist = (xx-yx);
		double d2 = (xy-yy);
		dist = dist*dist+d2*d2;
		dist = sqrt(dist);
	}
	return fabs(dist);
};

double  GetDistancePoints(REALPOS x,REALPOS y)
{
	return GetDistancePoints((double)(x.x),(double)(x.z),(double)(y.x),(double)(y.z));
};

/////////////////////////////////////////////////////////////////////////////
// uhel mezi dv�ma body				

double GetPointsRad(REALPOS x, REALPOS pCenter)
{
	double nDist = GetDistancePoints(x, pCenter);
	if (nDist == 0.) return 0;

	double nRad = acos(((double)(x.x-pCenter.x))/nDist);
	if (x.z < pCenter.z) 
		nRad = 2 * PI-nRad;
	return nRad;
};

/////////////////////////////////////////////////////////////////////////////
// poloha bodu - bod dan� �hlem nGra a polom�rem nR uhel je ve smeru hodinovych rucicek od horizontalneho smeru nahoru ve Visitoru

void GetRadixPoint(POINT& pt,POINT ptCenter, int nR, double nRad)
{
	pt.x = ((int)Round((sin(nRad)*(double)nR),0))+ptCenter.x;
	pt.y = ptCenter.y + ((int)Round((-cos(nRad)*(double)nR),0));
};

void GetRadixPoint(REALPOS& pt,REALPOS ptCenter, double nR, double nRad)
{
	pt.x = ((float)((sin(nRad)*nR)))+ptCenter.x;
	pt.z = ptCenter.z + ((float)((cos(nRad)*nR)));
};

/////////////////////////////////////////////////////////////////////////////
// relativn� posuny objektu s�t� vzhledem k jeho bodu A

// realtivn� posun k A
Vector3 GetRelativPointToA(Vector3 pt, Vector3 ptA)
{
	Vector3 ptR;
	ptR = pt - ptA;	
	return ptR;
}

// old code
//**********************************************************
// realtivn� rotace k A
Vector3 GetRelativPtRotToA(Vector3 pt, Vector3 ptA, int nGra)
{
	Vector3 ptR;
	ptR = pt - ptA;

	Matrix3 rot(MRotationY, GRADTORAD(nGra));
	return rot * ptR;
}
//**********************************************************

// new code
//**********************************************************
// realtivn� rotace k A
Vector3 GetRelativPtRotToA(Vector3 pt, Vector3 ptA, float nGra)
{
	Vector3 ptR;
	ptR = pt - ptA;

	Matrix3 rot(MRotationY, GRADTORAD(nGra));
	return rot * ptR;
}
//**********************************************************

// bod realtivn� k A (podle posunu)		
Vector3 GetPointFromRelativA(Vector3 ptR, Vector3 ptA)
{
	Vector3 pt;
	pt = ptR + ptA;
	return pt;
}

/////////////////////////////////////////////////////////////////////////////
// inicializace & serializace REALNETPOS

void InitRealNetPos(REALNETPOS& RealNet)
{
	RealNet.nPos.x =
	RealNet.nPos.z =
	RealNet.nHgt   = 0.;
	RealNet.nGra   = 0;
}

void MntSerializeNetPos(CArchive& ar, DWORD nVer, REALNETPOS& RealNet)
{
	MntSerializeFloat(ar, RealNet.nPos.x);
	MntSerializeFloat(ar, RealNet.nPos.z);
	MntSerializeFloat(ar, RealNet.nHgt);
	// required by the change in REALNETPOS
	if (nVer < 60)
	{
		int temp;
		if (ar.IsStoring())
		{
			temp = (int)RealNet.nGra;
		}
		MntSerializeInt(ar, temp);
		if (ar.IsLoading())
		{
			RealNet.nGra = (float)temp;
		}
	}
	else
	{
		MntSerializeFloat(ar, RealNet.nGra);
	}
}



