/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#if !defined(_POSEDCORE_MAIN_H_)
#define _POSEDCORE_MAIN_H_

// include ES/El

#include <Es/Containers/Array.hpp>
#include "../VisitorExchangeInterface/IVisViewerExchange.h"

class  CPosEdBaseDoc;
/////////////////////////////////////////////////////////////////////////////
//  PoseidonEdCore.h : header file
//	resource >> 12000

//#define POSED_FILE_VER		37
//#define POSED_FILE_VER		38 // per and pew in one file
//#define POSED_FILE_VER		39 // new format of data for random placer
//#define POSED_FILE_VER		40 // added snap points into objects def
//#define POSED_FILE_VER		41 // added Disable transition on surface into texture definition
//#define POSED_FILE_VER		42 // removed all info about height over surface from roads
//#define POSED_FILE_VER		43 //added m_sMaterialFile into CPosTextureTemplate
//#define POSED_FILE_VER		44 //added WaterLayer into map
//#define POSED_FILE_VER		45 //lock/unlock for objects
//#define POSED_FILE_VER		46 //lock/unlock for textures
//#define POSED_FILE_VER		47 //lock/unlock for vertexes
//#define POSED_FILE_VER		48 //delete CPosEdObject::m_sObjFile
//#define POSED_FILE_VER		49 //added config name to document
//#define POSED_FILE_VER		50 //no changes in file, use only to force relativeHeight replacement
//#define POSED_FILE_VER		51 //added marker type into object template
//#define POSED_FILE_VER		52 //variable number of points in CPositionPoly
//#define POSED_FILE_VER		53 //for object template save also sizes used for drawing (including proxy sizes)
//#define POSED_FILE_VER		54 //satellite grid
//#define POSED_FILE_VER		55 //explicit bimpas stored in the texture layer
//#define POSED_FILE_VER		56 //major pass stored in explicit bimpas
//#define POSED_FILE_VER		57 //Text and Type for keyPoint
//#define POSED_FILE_VER		58 //Named Selections
//#define POSED_FILE_VER		59 //New roads object templates
#define POSED_FILE_VER		60 //Changed angles from int to float

#define POSED_FILE_ID		"POSEW"

/// unregisted object / texture ID
#define UNDEF_REG_ID		(-1)
#define TEXTURE_SEA_ID		0 // reserved ID for sea texture. No texture in visitor will have this ID

#define TEXTURE_FILE_EXT	_T(".pac")
#define TEXTURE_FILE_EXT2	_T(".paa")
#define BIMPAS_FILE_EXT	_T(".bimpas")
#define RVMAT_FILE_EXT	_T(".rvmat")
#define MAX_FILENAME_LEN_TEXTURE_PRIMAR	7

#define DATA3D_FILE_EXT		_T(".p3d");


#define DEFAULT_COLOR		0xFFFFFFFF

#define MAX_OBJC_NAME_LEN	50

#define MAX_TXTR_NAME_LEN	20
#define MAX_LAND_NAME_LEN	30
#define MAX_ZONE_NAME_LEN	30
#define STD_STRING_LEN		100

#define MAX_AREA_NAME_LEN	30		// max. area/forest name length
#define MAX_PKEY_NAME_LEN	30		// max. key point name length


#define MAX_LAND_HEIGHT		8000.f
#define SEA_LAND_HEIGHT		   0.f
#define MIN_LAND_HEIGHT	   -1000.f

#define MIN_LAND_HGGRAD	       0.f
#define MAX_LAND_HGGRAD		  90.f

#define MIN_OBJRANDOM_SIZE		0.
#define MAX_OBJRANDOM_SIZE	  100.
#define DEF_OBJRANDOM_SIZE	   50.

#define MIN_OBJRANDOM_VERT	    0.	
#define MAX_OBJRANDOM_VERT	   60.
#define DEF_OBJRANDOM_VERT	   20.

#define MIN_OBJRANDOM_ROTA	    0.
#define MAX_OBJRANDOM_ROTA	  180.
#define DEF_OBJRANDOM_ROTA	  180.


#define NET_ALIGN_GRID		6.25	// network (roads) alignment grid

// unit width in logical coordinates
#define VIEW_LOG_UNIT_SIZE	50000


class   CMgrNets;


/////////////////////////////////////////////////////////////////////////////
// CMgrTextures - texture manager

class	CPosTextureTemplate;
typedef	Ref<CPosTextureTemplate>  PTRTEXTURETMPLT;

class	CPosTextureLand;
class	CPosTextureZone;

/// texture template
class /*AFX_EXT_CLASS*/ CPosTextureTemplate : public CObject, public RefCount
{
  DECLARE_DYNAMIC( CPosTextureTemplate )
public:
  CPosTextureTemplate(CPosEdBaseDoc*);
  ~CPosTextureTemplate();

  void	MakeDefaultValues();
  virtual void	CopyFrom(const CPosTextureTemplate*);

  // serialization
  virtual	void	DoSerialize(CArchive& ar, DWORD nVer);

  // rendering - ListBox
  void	DrawLBItem(CDC*,CRect& rPos,BOOL bProp=FALSE) const;
  COLORREF	GetLBItemColor() const;

  // data update from PAC file
  void	UpdateParamsFromSourceFile();
  // data created from a PAC file
  void	CreateFromPacFile(LPCTSTR pFileName);

  //@{ name/file conversions
  void	GetFullTextureFileName(CString&) const; // full texture file name (m_sTextureFile)
  void	GetBuldozerTextureFileName(CString&) const; // relative texture file name (m_sTextureFile)
  void  GetTextureFileName(CString&) const;

  void	GetFullBimPasFileName(CString&) const; // full bimpas file name (m_sTextureName)
  void	GetBuldozerBimPasFileName(CString&) const; // relative bimpas file name (m_sTextureName)
  void  GetBimPasFileName(CString&) const;

  void	GetFullRvMatFileName(CString&) const; //  full rvmat file name, created from m_sMaterialFile of primary texture
  void	GetBuldozerRvMatFileName(CString&) const; // relative rvmat file name, --||--
  void  GetRvMatFileName(CString&) const;

  void	GetNameFromFile(CString&) const; // name based on m_sTextureFile
  void	SetFileFromFullPath(LPCTSTR);	 // m_sTextureFile from full path
  void  SetRvMatFileNameFromFullPath(LPCTSTR); // m_sMaterialFile from full path

  void GetName(CString& name) const {name = m_sTextureName;};
  const CString& Name() const {return m_sTextureName;};
  void SetName(const CString& name) {m_sTextureName = name;};

  BOOL	GetScndTxtrNamePart(CString&) const; // part of the name for a secondary texture
  //@}
  
  CPosTextureTemplate * GetPrimarTexture() const;
  CPosTextureZone * GetZone() const {return m_pLandZone;};

  /// Get functions
  COLORREF GetColor() const {return m_cTxtrColor;};

  BOOL IsAllowedSecondary() const;

  // data
public:
  int	m_nTextureID;	// texture ID
  CString			m_sTextureFile;	// texture file *.paa for primary ones and *.bimpas for secondary ones 

private:
  CString     m_sMaterialFile; // path to the *.rvmat file...

public:
  CString			m_sTextureName;	// texture name

  int  m_nTimesUsedInLandGlobal; // number of use in Land (secondary)
  int  m_nTimesUsedInBaseGlobal; // number of use in base texture level 
  int  m_nTimesUsedInLandActive; // number of use in Land (secondary)
  int  m_nTimesUsedInBaseActive; // number of use in base texture level 

  // texture flags/types
  enum {    
    txtrTypePrimar		= 0,
    txtrTypeVariant		= 1,
    txtrTypeSecundar	= 2,
  };

  int				m_nTxtrType;	// texture type -  txtrType???
  BOOL			m_bIsSea;		// is this a sea (obsolete, sea is no longer texture based)
  float			m_nVarPorpab;	// variant texture probability
  bool      m_bUseOwnMaterial; // for variants textures. Use own material instead of primary one
  COLORREF		m_cTxtrColor;	// texture color

  // non-serialized data
  CPosTextureZone * m_pLandZone;	// vegetation zone

  // existence counter
  int				m_nTxtCounter;
  int				m_nTxtCounter2;

  // texture owner
  CPosEdBaseDoc * m_pOwner;
};

// texture bank
template <>
struct RefArrayKeyTraits<CPosTextureTemplate>
{
  typedef const int KeyType;
  static bool IsEqual(KeyType a, KeyType b)
  {
    return a==b;
  }
  static KeyType GetKey(const Ref<CPosTextureTemplate> &a) {return a->m_nTextureID;}
};

class TextureBank : public RefArray<CPosTextureTemplate>
{
  typedef RefArray<CPosTextureTemplate> base;

protected:
  int _lastUsedID;

public:
  TextureBank(): _lastUsedID(0) {};
  ~TextureBank(){};

  int AllocFreeID() {return ++_lastUsedID;};
  void SetLastUsedID(int lastUsedID) {_lastUsedID = lastUsedID;};
  int GetLastUsedID() const {return _lastUsedID;};

  int Add(Ref<CPosTextureTemplate>& src) 
  {    
    return base::Add(src);
  };

  int AddID(Ref<CPosTextureTemplate>& src) 
  {    
    src->m_nTextureID = ++_lastUsedID;
    return base::Add(src);
  };

  int AddUnigue(Ref<CPosTextureTemplate>& src)
  {
    int index = FindFile(src->m_sTextureFile);
    if (index >= 0)
    {
      src->m_nTextureID = base::Get(index)->m_nTextureID;
      return index;
    }
    else
    {
      src->m_nTextureID = ++_lastUsedID;
      return Add(src);
    }
  }

  int FindName(const CString& name) const
  {
    for(int i = 0; i < base::Size(); i++)
    {
      if (base::Get(i)->m_sTextureName == name)
        return i;
    }
    return -1;    
  }

  int FindFile(const CString& name) const
  {
    for(int i = 0; i < base::Size(); i++)
    {
      if (base::Get(i)->m_sTextureFile == name)
        return i;
    }
    return -1;    
  }

  Ref<CPosTextureTemplate> FindNameEx(const CString& name) const 
  {int i = FindName(name); return (i < 0)? NULL : base::Get(i);};

  Ref<CPosTextureTemplate> FindFileEx(const CString& name) const 
  {int i = FindFile(name); return (i < 0)? NULL : base::Get(i);};

  Ref<CPosTextureTemplate> FindEx(int ID) const 
  { int i = FindKey(ID); return (i < 0)? NULL : base::Get(i);};
    
  int Find(int ID) const {return base::FindKey(ID);};  

  void DoSerialize(CArchive& ar, int nVer, CPosEdBaseDoc * pDoc);
  void CleanUnUsed();
};

//typedef RefArray<CPosTextureTemplate> TextureBank;
typedef RefArray<CPosTextureTemplate> TextureRefBank;

/// zone - both primary and variant from given height
class /*AFX_EXT_CLASS*/ CPosTextureZone : public CObject
{
  DECLARE_DYNAMIC( CPosTextureZone )
public:
  CPosTextureZone();
  ~CPosTextureZone();

  void	CopyFrom(const CPosTextureZone*); // Be ware does not copies textures, just references to them

  void	Destroy();

  // update flags and owner in the definition hierarchy
  void	UpdateFlagsAndOwners();

  // serialization
  virtual	void	DoSerialize(CArchive& ar, DWORD nVer);

  // drawing a ListBox item
  void	DrawLBItem(CDC*,CRect& rPos) const;
  COLORREF	GetLBItemColor() const;

  PTRTEXTURETMPLT	GetRandomTexture();

  BOOL CreateLocalTextures(); // this function creates instances of the textures owned by this structure
                              // The textures are copies of instances from CPosEdBaseDec::m_Textures   
  void SyncLocalTexturesIntoDoc(); // This function copies local textures into CPosEdBaseDec::m_Textures. 

  bool ExistTextureName(const CString& name, const CPosTextureTemplate * ignore = NULL) const; // This function goes through all 
                                          // textures pointed by this zone and searches if exist one 
                                          // with given name.

  BOOL IsInUseBase();
  BOOL IsAllowedSecondary() const {return !m_bNoSecondary;};
  CPosTextureLand* GetLand() const {return m_pOwner;};
  CString GetName() const {return m_sZoneName;};

  // data
public:
  CString			m_sZoneName;	// zone name
  float			m_nHeightMin;	// basic zone height
  float			m_nHeightMax;
  float			m_nHgGradMin;
  float			m_nHgGradMax;

  PTRTEXTURETMPLT	m_pPrimarTxtr;	// primary texture for a zone

  BOOL			m_bPrimColOnly;	// use only primary texture name
  BOOL			m_bEnRandomVar;	// allow random variants
  BOOL      m_bNoSecondary; // never replace cell with this zone by secondary texture

  TextureRefBank		m_VarTxtTmpl;	// allow variant texture templates
  CPosTextureLand*m_pOwner;		// landscape owning this zone
};



/// landscape type - zones sorted by height
class /*AFX_EXT_CLASS*/ CPosTextureLand : public CObject
{
  DECLARE_DYNAMIC( CPosTextureLand )
public:
  CPosTextureLand(CPosEdBaseDoc*);
  ~CPosTextureLand();

  void	CopyFrom(const CPosTextureLand*);
  void	Destroy();
  BOOL CreateLocalTextures(); // this function creates instances of the textures owned by this structure
                              // The textures are copies of instaces from CPosEdBaseDec::m_Textures   
  void SyncLocalTexturesIntoDoc(); // This function copies local textures into CPosEdBaseDec::m_Textures. 

  bool ExistTextureName(const CString& name, const CPosTextureZone * doNotSearch, const CPosTextureTemplate * ignore = NULL) const; // This function goes through all 
                            // textures pointed by this land and searches if exist one 
                            //with given name.

  BOOL IsInUseBase();

  // update flags and owners in the definition hierarchy
  void	UpdateFlagsAndOwners();

  // serialization
  virtual	void	DoSerialize(CArchive& ar, DWORD nVer);

  // ListBox drawing
  void	DrawLBItem(CDC*,CRect& rPos) const;
  COLORREF	GetLBItemColor() const;

  // pr�ce s p�smy
  void	AddNewZone(CPosTextureZone* pZone);
  void	DeleteZone(CPosTextureZone* pZone,BOOL bDelObj=TRUE);

  // texture based on height
  CPosTextureZone* GetTextureZoneForHeight(LANDHEIGHT,float nHgGrad);
  PTRTEXTURETMPLT	GetTextureForHeight(LANDHEIGHT,float nHgGrad,BOOL bRandom = TRUE);

  CString GetName() const {return m_sLandName;};

  // data
public:
  CString			m_sLandName;	// type name
  CString			m_sLandHeight;	// equation for height recalculation
  BOOL			m_bIsSea;		// is this a sea (obsolete)
  COLORREF		m_cLandColor;	// color for Visitor display

  CObArray		m_LandZones;	// zones

  // texture owner
  CPosEdBaseDoc* m_pOwner;
};

class /*AFX_EXT_CLASS*/ CMgrTextures : public CObject
{
public:
  CMgrTextures();
  ~CMgrTextures();

  void	CopyFrom(const CMgrTextures*);
  void	Destroy();

  BOOL CreateLocalTextures(); // this function creates instances of the textures owned by this structure
                              // The textures are copies of instances from CPosEdBaseDec::m_Textures  

  void SyncLocalTexturesIntoDoc(); // This function copies local textures into CPosEdBaseDec::m_Textures. 

  bool ExistTextureName(const CString& name, const CPosTextureZone * doNotSearch, const CPosTextureTemplate * ignore = NULL) const; // This function goes through all 
                                          // textures pointed by this managers and searches if exist one 
                                          //with given name.

  void	UpdateFlagsAndOwners();

  virtual	void	DoSerialize(CArchive& ar, DWORD nVer);

  // data
public:
  CObArray  m_TextureLands;

  CPosEdBaseDoc*	m_pOwner;
};

/////////////////////////////////////////////////////////////////////////////
// CMgrObjects - object manager


typedef struct 
{
  CString name; //< name of the snap point. just snap points with identical names can be combined.
  int type; //< just snap points with different type can be combined. Types can be 0 or 1.
  AutoArray<Vector3> pos;//< in real model space
} SnapPlug; 
TypeIsMovable(SnapPlug);

struct SnapPlugFindArrayKeyTraits
{
	typedef const CString &KeyType;
	static bool IsEqual(KeyType a, KeyType b)
	{
		return a == b;
	}
	static KeyType GetKey(const SnapPlug &a) 
	{
		return a.name;
	}
};

class /*AFX_EXT_CLASS*/ CPosObjectTemplate : public CObject, public RefCount
{
	DECLARE_DYNAMIC( CPosObjectTemplate )

public:
	CPosObjectTemplate(CPosEdBaseDoc*);
  
	CPosObjectTemplate(const CPosObjectTemplate& src) 
	{
		CopyFrom(&src);
	}

	~CPosObjectTemplate();

	void MakeDefaultValues();
	virtual void CopyFrom(const CPosObjectTemplate*);

	// serialization
	virtual void DoSerialize(CArchive& ar, DWORD nVer);

	COLORREF GetObjectFrameColor(const CPosEdCfg*) const;
	COLORREF GetObjectEntireColor(const CPosEdCfg*) const;

	void DrawLBItem(CDC*, CRect& rPos) const;
	COLORREF GetLBItemColor() const;

	// data update from p3d file
	void UpdateParamsFromSourceFile();
	//! Verifies if the given source file has the same data...  BE WARE snap point data can be different
	bool AreSameDataInSourceFile(CString file) const; 

	// Load requested selections from memory level
	//bool LoadP3DNamedSel(AutoArray<P3DNamedSel>& namedSel);

	// create from p3d file
	void CreateFromObjFile(LPCTSTR pFileName, LPCTSTR useName = 0);

	/// name/file conversions
	CString& GetFullFileName(CString&) const; // full path (m_sObjFile)
	CString& GetBuldozerFileName(CString&) const; // relative path (m_sObjFile)
 
	CString GetBuldozerFileName() const 
	{
		CString temp; 
		return GetBuldozerFileName(temp); 
	}

	const CString GetFileName() const 
	{
		return m_sObjFile;
	}
 
	void SetFileName(CString name) 
	{
		m_sObjFile = name;
	}

	void GetNameFromFile(CString&) const; // name based on sObjFile
	void SetFileFromFullPath(LPCTSTR);	 // m_sObjFile based on full path

	const CString& GetName() const 
	{
		return m_sObjName;
	}

	void SetName(CString name) 
	{
		m_sObjName = name;
	}

	// network named points
	static void GetNetPtName(CString& sName, int nPtNet);
	BOOL ExistNetPoint(int nPtNet) const;
	Vector3 GetNetPoint(int nPtNet) const;

	Vector3 GetNetPointA() const;			// model coordinates point A

	CPosEdBaseDoc* GetDocument() const 
	{
		return m_pOwner;
	}

	typedef enum 
	{
		posTypePoly = 0,
		posTypeEllipse = 1,
		posType2D = 2,
	} PosType;

	PosType GetPosType() const 
	{
		return m_posType;
	}

	void SetPosType(PosType type) 
	{
		m_posType = type;
	}

	// Object sizes
	bool HasAutoCenter() const 
	{
		return m_autoCenter == TRUE;
	}

	const CString& GetPlacement() const 
	{
		return m_placement;
	}

	const Vector3& GetBoundingCenter() const 
	{
		return m_BoundingCenter;
	}

	double GetWidth() const 
	{
		return m_nWidth;
	}

	double GetHeight() const 
	{
		return m_nHeight;
	}

	// sizes valid only for drawing in Visitor
	const Vector3& GetDrawBoundingCenter() const 
	{
		return m_drawBoundingCenter;
	}

	double GetDrawWidth() const 
	{
		return m_drawWidth;
	}

	double GetDrawHeight() const 
	{
		return m_drawHeight;
	}

	// object data
public:
	int m_ID; // TemplateID;
 
protected:
	CString		m_sObjFile;			// filename
	CString		m_sObjName;			// name
  
	PosType m_posType; 
  
	// object size (m)
	double  m_nWidth;
	double  m_nHeight;
	Vector3 m_BoundingCenter;	// BoundingCenter 
	BOOL    m_autoCenter; // if true assume that object will be centered.
	CString m_placement;      // placement type

	// the following values will be used for drawing (for objects with proxies they can differ from m_BoundingCenter);
	Vector3 m_drawBoundingCenter; 
	double  m_drawWidth;
	double  m_drawHeight;

public:
	enum 
	{
		objTpUndef	  = 0,	// nedefinov�n / import
		objTpNatural  = 1,	// natural object
		objTpPeople	  = 2,	// artificial object
		objTpNet      = 3,  // road object
		objTpWood     = 4,  // forest
		objTpNewRoads = 5   // new roads object
	};

	int m_nObjType;	// object type - objTp???

	COLORREF m_clrFrame;	// object frame
	COLORREF m_clrEntire;// object fill

	// object randomization 
	BOOL		m_bRandomSize;
	double		m_nMinRanSize;  // - 5%
	double		m_nMaxRanSize;  // + 5%

	BOOL		m_bRandomVert;
	double		m_nMinRanVert;  // - 10�
	double		m_nMaxRanVert;  // + 10�

	BOOL		m_bRandomRota;
	double		m_nMinRanRota;  // - 180�
	double		m_nMaxRanRota;  // + 180�

	// net (road) named points
	enum 
	{
		ptNetCount = 8,
		ptNetLB = 0, 
		ptNetPB = 1,
		ptNetLE = 2, 
		ptNetPE = 3,
		ptNetLH = 4, 
		ptNetLD = 5,
		ptNetPH = 6, 
		ptNetPD = 7, 
	};

	BOOL	m_bIsNetObject;	// net (road) object
	Vector3	m_ptNetPts[ptNetCount];

	// snap points
	FindArrayKey<SnapPlug, SnapPlugFindArrayKeyTraits> m_snapPlugs;

	// TEMP
	int m_nTimesUsed;	// existence counter

	// object owner
	CPosEdBaseDoc* m_pOwner;
};

template <>
struct RefArrayKeyTraits<CPosObjectTemplate>
{
	typedef const int KeyType;

	static bool IsEqual(KeyType a, KeyType b)
	{
		return a == b;
	}

	static KeyType GetKey(const Ref<CPosObjectTemplate> &a) 
	{
		return a->m_ID;
	}
};

class ObjectBank : public RefArray<CPosObjectTemplate>
{
	typedef RefArray<CPosObjectTemplate> base;

protected:
	static int _lastUsedID;

public:
	ObjectBank() 
	{
	}

	~ObjectBank()
	{
	}

	int AllocFreeID() 
	{
		return ++_lastUsedID;
	}

	void SetLastUsedID(int lastUsedID) 
	{
		_lastUsedID = lastUsedID;
	}
 
	int GetLastUsedID() const 
	{
		return _lastUsedID;
	}

	int Add(Ref<CPosObjectTemplate>& src) 
	{    
		return base::Add(src);
	}

	int AddID(Ref<CPosObjectTemplate>& src) 
	{    
		src->m_ID = ++_lastUsedID;
		return base::Add(src);
	}
 
	int FindFile(const CString& name) const
	{
		for (int i = 0; i < base::Size(); ++i)
		{
			if (base::Get(i)->GetFileName() == name)
			return i;
		}
		return -1;    
	}
  
	int FindName(const CString& name) const
	{
		for (int i = 0; i < base::Size(); ++i)
		{
			if (base::Get(i)->GetName() == name) return i;
		}
		return -1;    
	}

	Ref<CPosObjectTemplate> FindFileEx(const CString& name) const 
	{
		int i = FindFile(name); 
		return (i < 0) ? NULL : base::Get(i);
	}

	Ref<CPosObjectTemplate> FindNameEx(const CString& name) const 
	{
		int i = FindName(name); 
		return (i < 0) ? NULL : base::Get(i);
	}

	Ref<CPosObjectTemplate> FindEx(int ID) const 
	{ 
		int i = FindKey(ID); 
		return (i < 0) ? NULL : base::Get(i);
	}

	int Find(int ID) const 
	{
		return base::FindKey(ID);
	}  

	void DoSerialize(CArchive& ar, int nVer, CPosEdBaseDoc* pDoc);
	bool HardCopyFrom(const ObjectBank& src, int type);
	bool SyncTarget(ObjectBank& dest, int type); 
};

class /*AFX_EXT_CLASS*/ CMgrObjects : public CObject
{
public:
	CMgrObjects();
	~CMgrObjects();

	void Destroy();
	static BOOL CopyTemplates(CObArray& dest, const CObArray& src);	

	// serialization
	static void DoSerializeTemplates(CArchive& ar, DWORD nVer, CObArray&, CPosEdBaseDoc*);
	virtual void DoSerialize(CArchive& ar, DWORD nVer);

// object templates
public:
	CObArray m_ObjTemplNatural;	// natural objects
	CObArray m_ObjTemplPeoples;	// artificial objects

	CObArray m_ObjTemplNewRoads;	

	// document
	CPosEdBaseDoc*	m_pOwner;
};

/////////////////////////////////////////////////////////////////////////////
// CMgrWoods - forest manager			(MgrWoods.cpp)

class /*AFX_EXT_CLASS*/ CPosWoodTemplate : public CObject
{
  DECLARE_DYNAMIC( CPosWoodTemplate )
public:

  CPosWoodTemplate();
  CPosWoodTemplate(CPosEdBaseDoc*	pOwner);
  ~CPosWoodTemplate();

  void	MakeDefaultValues();
  void	DestroyValues();
  virtual void	CopyFrom(const CPosWoodTemplate*);

  // serialization
  virtual	void	DoSerialize(CArchive& ar, DWORD nVer);

  void	DrawLBItem(CDC*,CRect& rPos) const;
  COLORREF	GetLBItemColor() const;

  COLORREF	GetObjectFrameColor(const CPosEdCfg*) const;
  COLORREF	GetObjectEntireColor(const CPosEdCfg*) const;

  void CreateLocalObjectTemplates();
  void SyncLocalObjectTemplatesIntoDoc();

public:
  CString		m_sWoodName;		// forest name
  COLORREF	m_clrFrame;			// frame color
  COLORREF	m_clrEntire;		// fill color

  // forest object templates
  Ref<CPosObjectTemplate>	m_pWoodObjREntir;	// square
  Ref<CPosObjectTemplate>	m_pWoodObjRFrame;	// square + border
  Ref<CPosObjectTemplate>	m_pWoodObjTEntir;	// triangle + border

  CStringArray		m_WoodObjBush;		// bush
  CStringArray		m_WoodObjTree;		// forest

  /// document
  CPosEdBaseDoc*	m_pOwner;
};


class /*AFX_EXT_CLASS*/ CMgrWoods : public CObject
{
public:
  CMgrWoods();
  ~CMgrWoods();

  void	Destroy();
  void	CopyFrom(CMgrWoods*);	

  /// serialization
  virtual	void	DoSerialize(CArchive& ar, DWORD nVer);
  /// named template
  CPosWoodTemplate* GetWoodTemplate(LPCTSTR) const;

  void CreateLocalObjectTemplates(); // this function creates instances of the templates owned by this structure
  // The templates are copies of instances from CPosEdBaseDec::m_ObjectTemplates  

  void SyncLocalObjectTemplatesIntoDoc(); // This function copies local templates into CPosEdBaseDec::m_ObjectTemplates. 

public:
  CObArray	m_WoodTemplates;	// forest templates

  // document
  CPosEdBaseDoc*	m_pOwner;
};

/////////////////////////////////////////////////////////////////////////////
// road parts						(NetComponent.cpp)

class /*AFX_EXT_CLASS*/ CNetComponent : public CObject
{
	DECLARE_DYNAMIC( CNetComponent )
public:
	enum 
	{
		netCmpSTRA = 0,	// rovinka
		netCmpBEND = 1, // zat��ka
		netCmpSPEC = 2, // speci�ln� d�l
		netCmpTERM = 3, // koncov� d�l
	};
					
	CNetComponent(CPosEdBaseDoc* pOwner);
	~CNetComponent();

	virtual void MakeDefault();
	virtual void Destroy();
	virtual WORD GetNetCmpType() const = 0;		

	// vytvo�en� kopie objektu
	CNetComponent* CreateCopyObject() const;
	static CNetComponent* CreateObjectType(int nType, CPosEdBaseDoc* pOwner);
	virtual	void CopyFrom(const CNetComponent*);

	// serializace
	virtual	void DoSerialize(CArchive& ar, DWORD nVer);

	// propojen� s objektem poseidon
	virtual BOOL CreateFromObjectTemplate(CPosObjectTemplate*);

	// parametry objektu // m��e m�nit v��ku (FALSE - polo�en pouze horizont�ln�)
	virtual BOOL CanChangedObjHeight() const 
	{ 
		return FALSE;  
	}

	virtual void CreateLocalObjectTemplates();
	virtual void SyncLocalObjectTemplatesIntoDoc();

	virtual bool IsUsed() const 
	{
		return m_pNetObj.NotNull() && m_pNetObj->m_nTimesUsed > 0;
	}

	CPosObjectTemplate* GetObjectTemplate() const 
	{
		return m_pNetObj;
	}

	CPosEdBaseDoc* GetDocument() const 
	{
		return m_pOwner;
	}

// data
protected:
	Ref<CPosObjectTemplate> m_pNetObj;		// objekt Poseidon
public:
	CString m_sCompName;	// n�zev komponenty

	// dokument
	CPosEdBaseDoc* m_pOwner;
};

// z�kladn� rovinka
class /*AFX_EXT_CLASS*/ CNetBaseStra : public CNetComponent
{
	DECLARE_DYNAMIC( CNetBaseStra )
public:
	CNetBaseStra(CPosEdBaseDoc*	pOwner);

	// vytvo�en� kopie objektu
	virtual	void CopyFrom(const CNetComponent*);
	// serializace
	virtual	void DoSerialize(CArchive& ar, DWORD nVer);

	// propojen� s objektem poseidon
	virtual BOOL CreateFromObjectTemplate(CPosObjectTemplate*);

	// parametry objektu // m��e m�nit v��ku (FALSE - polo�en pouze horizont�ln�)
	virtual BOOL CanChangedObjHeight() const 
	{ 
		return m_bChngHeight;  
	}

	// d�lka v m podle typu
	static float GetNormalizeLength(float nLen);
	static float GetNormalizeLength(int nType);
	       float GetStraLength() const;

	// data
public:
	enum 
	{
		netSTRA_6  = 0,
		netSTRA_12 = 1,
		netSTRA_25 = 2, 
	};					// typ rovinky

	WORD m_nSTRAType;				// netSTRA_???
	BOOL m_bChngHeight;				// m��e m�nit sklon
};

// komponenty s�t�

// rovinka - STRA
class /*AFX_EXT_CLASS*/ CNetSTRA : public CNetBaseStra
{
	DECLARE_DYNCREATE( CNetSTRA )
public:
	CNetSTRA();
	CNetSTRA(CPosEdBaseDoc*	pOwner);

	// typ objektu
	virtual WORD GetNetCmpType() const 
	{ 
		return CNetComponent::netCmpSTRA; 
	}		

	// propojen� s objektem poseidon
	virtual BOOL CreateFromObjectTemplate(CPosObjectTemplate*);
};

// zat��ka - BEND
class /*AFX_EXT_CLASS*/ CNetBEND : public CNetComponent
{
DECLARE_DYNCREATE( CNetBEND )
public:
	CNetBEND();
	CNetBEND(CPosEdBaseDoc*	pOwner);
	virtual ~CNetBEND();

	// typ objektu
	virtual WORD GetNetCmpType() const 
	{ 
		return CNetComponent::netCmpBEND; 
	}

	// vytvo�en� kopie objektu
	virtual	void CopyFrom(const CNetComponent*);
	// serializace
	virtual	void DoSerialize(CArchive& ar, DWORD nVer);

	// propojen� s objektem poseidon
	virtual BOOL CreateFromObjectTemplate(CPosObjectTemplate*);

// new code
// Added to allow for variable angle
// **********************************
	float GetNormalizeGrad() const;
	static float GetNormalizeGrad(WORD nType);
// **********************************

// old code
// **********************************
/*
	// d�lka a uhel podle typu
	static int GetNormalizeGrad() 
	{ 
		return 10; 
	}	// zat��ky maj� 10 
*/
// **********************************

	static double GetNormalizeLength(WORD nType);		// d�lka (vzd�lenost A.B) podle typu
	static double GetNormalizeRadix(WORD nType);		// polomer podle typu

// data
public:
	enum 
	{
		// old models, renamed
		netBEND_10_25  =  0,
		netBEND_10_50  =  1,
		netBEND_10_75  =  2,
		netBEND_10_100 =  3, 
		// newly added models
		netBEND_0_2000 =  4, 
		netBEND_1_1000 =  5, 
		netBEND_7_100  =  6, 
		netBEND_15_75  =  7, 
		netBEND_22_50  =  8, 
		netBEND_30_25  =  9, 
		netBEND_60_10  = 10, 
	};					// typ zat��ky

	WORD m_nBENDType;				// netBEND_???
};

// spec. d�l - SPEC
class /*AFX_EXT_CLASS*/ CNetSPEC : public CNetBaseStra
{
	DECLARE_DYNCREATE( CNetSPEC )
public:
	CNetSPEC();
	CNetSPEC(CPosEdBaseDoc*	pOwner);

	// typ objektu
	virtual WORD GetNetCmpType() const 
	{ 
		return CNetComponent::netCmpSPEC; 
	}		

	// propojen� s objektem poseidon
	virtual BOOL CreateFromObjectTemplate(CPosObjectTemplate*);
};

// koncov� d�l - TERM
class /*AFX_EXT_CLASS*/ CNetTERM : public CNetComponent
{
	DECLARE_DYNCREATE( CNetTERM )
public:
	CNetTERM();
	CNetTERM(CPosEdBaseDoc*	pOwner);

	// typ objektu
	virtual WORD GetNetCmpType() const 
	{ 
		return CNetComponent::netCmpTERM; 
	}		

	// propojen� s objektem poseidon
	virtual BOOL CreateFromObjectTemplate(CPosObjectTemplate*);
};

/////////////////////////////////////////////////////////////////////////////
// CMgrNets - mana�er s�t�			(MgrNets.cpp)

// �ablona s�t� - CPosNetTemplate
class /*AFX_EXT_CLASS*/ CPosNetTemplate : public CObject
{
	DECLARE_DYNAMIC( CPosNetTemplate )
public:
	CPosNetTemplate(CPosEdBaseDoc*);
	~CPosNetTemplate();

	void MakeDefaultValues();
	void DestroyValues();
	virtual void CopyFrom(const CPosNetTemplate*);

	// serializace
	virtual void DoSerialize(CArchive& ar, DWORD nVer);

	// vykreslen� v ListBox
	void DrawLBItem(CDC*, CRect& rPos) const;
	void DrawLBImage(CDC*, CRect& rPos) const;

	COLORREF GetObjectKeyColor(BOOL& bFill, const CPosEdCfg*) const;
	COLORREF GetObjectNorColor(BOOL& bFill, const CPosEdCfg*) const;

	void CreateLocalObjectTemplates();
	void SyncLocalObjectTemplatesIntoDoc();

	bool IsUsed() const;
	void GetObjectTemplates(AutoArray<int>& templ);
	CNetTERM* GetTermTemplate(CString name);
	CNetSPEC* GetSpecTemplate(CString name);
	CNetBEND* GetBendTemplate(CString name);
	CNetSTRA* GetStraTemplate(CString name);

	// data objektu
public:
	CString  m_sNetName;    // jm�no  s�t�
	COLORREF m_clrKey;      // barva - kl��ov�
	COLORREF m_clrNor;      // barva - norm�ln�
	BOOL     m_bFillEntire; // vypl� zobrazen�

protected:
  /*+++ Obsolete roads are placed by engine onto ground +++*/
  //double			m_nMinHeight;		// um�st�n� d�l� vzhledem k povrchu
  //double			m_nMaxHeight;
  //double			m_nStdHeight;	
  //BOOL			  m_bAutoHeight;		// automatick� p�ilnavost k povrchu
  /*--- Obsolete roads are placed by engine onto ground ---*/

public:
	double m_nMaxSlope;		// maxim�ln� sklon
	double m_nPartSlope;		// maxim�ln� �hle mezi d�ly

	// definovan� d�ly
	CObArray m_NetSTRA;			// rovinky
	CObArray m_NetBEND;			// zat��ky
	CObArray m_NetSPEC;			// spec. d�ly
	CObArray m_NetTERM;			// koncov� d�ly

	// vlastnik - projekt
	CPosEdBaseDoc* m_pOwner;
};

// �ablona k�i�ovatky - CPosCrossTemplate
class /*AFX_EXT_CLASS*/ CPosCrossTemplate : public CObject
{
	DECLARE_DYNAMIC( CPosCrossTemplate )
public:
	CPosCrossTemplate(CPosEdBaseDoc*);
	~CPosCrossTemplate();

	void MakeDefaultValues();
	void DestroyValues();
	virtual void CopyFrom(const CPosCrossTemplate*);

	// serializace
	virtual void DoSerialize(CArchive& ar, DWORD nVer);

	// propojen� s objektem poseidon
	void GetTypeString(CString& sType) const;
	virtual BOOL CreateFromObjectTemplate(CPosObjectTemplate*);

	// vykreslen� v ListBox
	COLORREF GetLBItemColor() const;
	void DrawLBItem(CDC*, CRect& rPos) const;
	COLORREF GetObjectNorColor(BOOL& bFill, const CPosEdCfg*) const;

	// �seky (napojen�) na s�t�
	static BOOL SupportCrossPart(WORD nPart, WORD nType);	// m� smysl dan� �sek ?
	BOOL SupportCrossPart(WORD nPart) const;
	void GetCrossPartName(CString& sName, WORD nPart) const;

	CPosNetTemplate* GetPartNetTemplate(WORD nPart, CMgrNets*);
	void RenameNetTemplate(LPCTSTR pOld, LPCTSTR pNew);

	void CreateLocalObjectTemplates();
	void SyncLocalObjectTemplatesIntoDoc();

	CPosObjectTemplate* GetObjectTemplate() const 
	{
		return m_pNetObj;
	}

	CPosEdBaseDoc* GetDocument() const 
	{
		return m_pOwner;
	}

	 // data objektu
public:
	CString m_sCrossName;	// jm�no  k�i�ovatky
protected:
	Ref<CPosObjectTemplate> m_pNetObj;		// objekt Poseidon
public:
	COLORREF m_clrNor;      // barva - norm�ln�
	BOOL     m_bFillEntire; // vypl� zobrazen�

protected:
  /*+++ Obsolete roads are placed by engine onto ground +++*/
  //double					m_nMinHeight;	// um�st�n� d�lu vzhledem k povrchu
  //double					m_nMaxHeight;
  //double					m_nStdHeight;	
  //BOOL					m_bAutoHeight;	// automatick� p�ilnavost k povrchu
  /*--- Obsolete roads are placed by engine onto ground ---*/
public:

	enum 
	{
		netCROSS_Types = 4,
		netCROSS_I	= 0,
		netCROSS_L	= 1,
		netCROSS_P  = 2,
		netCROSS_K	= 3, 
	};					// typ k�i�ovatky

	WORD m_nCrossType;	// netCROSS_???

	enum 
	{
		netCROSS_Parts = 4,
		netCROSS_A	= 0,
		netCROSS_B	= 1,
		netCROSS_C  = 2,
		netCROSS_D	= 3, 
	};					// typ �seku (napojen�)

	// n�zvy s�t� v ��stech k�i�ovatky
	CString m_sPartsNets[netCROSS_Parts];	

	// vlastnik - projekt
	CPosEdBaseDoc* m_pOwner;
};

class /*AFX_EXT_CLASS*/ CMgrNets : public CObject
{
public:
	CMgrNets();
	~CMgrNets();

	void Destroy();
	void CopyFrom(CMgrNets*);	

	// serializace
	virtual void DoSerialize(CArchive& ar, DWORD nVer);

	// �ablona podle jm�na
	CPosNetTemplate* GetNetTemplate(LPCTSTR) const;
	CPosCrossTemplate* GetCrossTemplate(LPCTSTR) const;

	void RenameNetTemplate(LPCTSTR pOld, LPCTSTR pNew);

	void CreateLocalObjectTemplates();
	void SyncLocalObjectTemplatesIntoDoc();

	// �ablony s�t�
public:
	CObArray m_NetTemplates;		// s�t�
	CObArray m_CrossTemplates;	// k�i�ovatky

	// dokument
	CPosEdBaseDoc* m_pOwner;
};

/////////////////////////////////////////////////////////////////////////////
// CVariantListBox - LB pro zobrazen� objekt�

class /*AFX_EXT_CLASS*/ CVariantListBox : public CFMntListBox
{
public:
	CVariantListBox();

	virtual int  GetItemImageWidth(LPDRAWITEMSTRUCT lpDIS);
	virtual void OnDrawItemImage(CRect&, LPDRAWITEMSTRUCT lpDIS);
	virtual void OnDrawItemText(CRect&, LPDRAWITEMSTRUCT lpDIS);
	virtual BOOL GetItemColor(COLORREF&, LPDRAWITEMSTRUCT lpDIS);

// data
	int		m_nVariant;				// typ prvk�
	enum 
	{
		itmVariantLand  = 0,			// typ krajiny
		itmVariantZone  = 1,			// p�smo krajiny
		itmVariantTxtr  = 2,			// textura
		itmVariantWood  = 3,			// les
		itmVariantNets  = 4,			// s�t�
		itmVariantCros  = 5,			// k�i�ovatky
		itmVariantNObj  = 6,			// p��rodn� objekty
		itmVariantPObj  = 7,			// lidsk� objekty
		itmVariantNRObj = 8			
	};
};

class /*AFX_EXT_CLASS*/ CVariantComboBox : public CFMntComboBox
{
public:
					CVariantComboBox();

	virtual void	OnDrawItemText(CRect&, LPDRAWITEMSTRUCT lpDIS);
	virtual BOOL	GetItemColor(COLORREF&, LPDRAWITEMSTRUCT lpDIS);

// data
	int		m_nVariant;				// typ prvk�
	enum {
		itmVariantLand = 0,			// typ krajiny
		itmVariantZone = 1,			// p�smo krajiny
		itmVariantTxtr = 2,			// textura
		itmVariantWood = 3,			// les
	};
};



/////////////////////////////////////////////////////////////////////////////
// CPosAction - z�kladn� akce prov�d�n� v dokumentu

class /*AFX_EXT_CLASS*/ CPosAction : public CAction
{
DECLARE_DYNAMIC(CPosAction)
public:
			CPosAction(int nType);

			// proveden� akce v dokumentu
	virtual BOOL RealizeAction(CDocument*);		
  virtual void SetFromBuldozer(BOOL bFromBuldozer) {m_bFromBuldozer = bFromBuldozer;};

// data
	BOOL	m_bFromBuldozer;
	BOOL	m_bUpdateView;
};

// skupinov� akce
class /*AFX_EXT_CLASS*/ CPosActionGroup : public CPosAction
{
DECLARE_DYNAMIC(CPosActionGroup)
public:
			CPosActionGroup(int nType);
			~CPosActionGroup();

			// proveden� akce v dokumentu
	virtual BOOL RealizeAction(CDocument*);	
  virtual void SetFromBuldozer(BOOL bFromBuldozer);

			// pr�ce se skupinou akc�
			void AddAction(CPosAction*);
			void DestroyAll();

			// v�m�na hodnot (z�m�na po�ad�)
	virtual BOOL ChangeValues();


// skupina akc�
public:
	BOOL		 m_bBaseDirection;	// po�ad� prov�d�n�
	CObArray	 m_Actions;
};

/////////////////////////////////////////////////////////////////////////////
// Poloha objekt� - PositionClass.cpp

void GetObjectHull(RECT& rHull,const OBJPOSITION&);
void GetPointsHull(RECT& rHull,const POINT*, int nPtCount);

// CPositionBase - z�kladn� objekt pro polohu
class /*AFX_EXT_CLASS*/ CPositionBase : public CObject
{
	DECLARE_DYNAMIC(CPositionBase)
public:
	CPositionBase();
	virtual ~CPositionBase();

	// vytvo�en� kopie objektu
	CPositionBase* CreateCopyObject() const;
	virtual void CopyFrom(const CPositionBase*);

	// serializace
	virtual void DoSerialize(CArchive& ar, DWORD nVer) = 0;

	// zru�en� v�ech �daj� polohy
	virtual void ResetPosition() = 0;
	// testov�n� podobnosti
	virtual BOOL IsSamePos(const CPositionBase*) const = 0;

	// testov�n� platnosti
	virtual BOOL IsValidPosition() const;
	// obal polohy
	virtual RECT GetPositionHull() const = 0;

	virtual void RecalcPositionHull() 
	{
	}

	CPoint GetCenter() const 
	{
		RECT rect = GetPositionHull(); 
		return CPoint((rect.left + rect.right) / 2, (rect.top + rect.bottom) / 2);
	}

	// je bod na plo�e ?
	virtual BOOL IsPointAtArea(const POINT& pt) const = 0;
	virtual BOOL IsRectAtArea(const RECT& r, BOOL bFull) const = 0;

	// je plocha v oblasti ?
	virtual BOOL IsAreaInRect(const CRect&) const = 0;
	virtual BOOL IsAreaInRgn(const CRgn&) const = 0;

	// posun plochy
	virtual void OffsetPosition(int xAmount, int yAmount) = 0;
	virtual void TransformPosition(FMntMatrix2Val trans, CPointVal origin ) = 0; 
	// p�evod na DP z LP
	virtual void LPtoDP(CDC* pDC) = 0;
	// p�evod na DP ze Scrolleru
	virtual void ScrolltoDP(CFMntScrollerView*) = 0;

	// vykreslov�n� trackeru
	virtual void OnDrawTrackFrameInDP(CDC* pDC, const CFMntScrollerTracker*) const = 0;
	virtual void OnDrawTrackHitsInDP(CDC* pDC, const CFMntScrollerTracker*) const = 0;

	virtual void OnDrawObject(CDC* pDC, COLORREF cFrame, int frameBrushStyle, COLORREF cEntir, int entireBrushStyle, const CDrawPar* pParams) const 
	{
	}

	virtual bool IsIntersection(const CPositionBase&) const = 0;
};

// CPositionRect - poloha - rect
  class /*AFX_EXT_CLASS*/ CPositionRect : public CPositionBase
  {
    DECLARE_DYNCREATE(CPositionRect)
  public:
    CPositionRect();

    void Create(const CRect& rect) {m_Position = rect;};

    // kopie objektu
    virtual void	CopyFrom(const CPositionBase*);
    // serializace
    virtual	void	DoSerialize(CArchive& ar, DWORD nVer);
    // zru�en� v�ech �daj� polohy
    virtual	void	ResetPosition();
    // testov�n� podobnosti
    virtual BOOL	IsSamePos(const CPositionBase*) const;
    // obal polohy
    virtual RECT	GetPositionHull() const;
    // je bod na plo�e ?
    virtual BOOL	IsPointAtArea(const POINT& pt) const;
    virtual BOOL	IsRectAtArea(const RECT& r,BOOL bFull) const;
    // je plocha v oblasti ?
    virtual BOOL	IsAreaInRect(const CRect&) const;
    virtual BOOL	IsAreaInRgn(const CRgn&) const;

    // posun plochy
    virtual void	OffsetPosition(int xAmount,int yAmount);
    virtual void  TransformPosition(FMntMatrix2Val trans, CPointVal origin);

    // p�evod na DP z LP
    virtual void	LPtoDP(CDC* pDC);
    // p�evod na DP ze Scrolleru
    virtual void	ScrolltoDP(CFMntScrollerView*);

    // vykreslov�n� trackeru
    virtual void	OnDrawTrackFrameInDP(CDC* pDC,const CFMntScrollerTracker*) const;
    virtual void	OnDrawTrackHitsInDP(CDC* pDC,const CFMntScrollerTracker*) const;
    virtual bool  IsIntersection(const CPositionBase& ) const {return false;};

    // data
  public:
    CRect			m_Position;
  };

  // CPositionEllipse - poloha - rect
  class /*AFX_EXT_CLASS*/ CPositionEllipse : public CPositionRect
  {
    typedef CPositionRect base;
    DECLARE_DYNCREATE(CPositionEllipse)
  public:
    CPositionEllipse();
    virtual ~CPositionEllipse();

    

    // je bod na plo�e ?
    virtual BOOL	IsPointAtArea(const POINT& pt) const;
    virtual BOOL	IsRectAtArea(const RECT& r,BOOL bFull) const;
    // je plocha v oblasti ?
    virtual BOOL	IsAreaInRect(const CRect&) const;
    virtual BOOL	IsAreaInRgn(const CRgn&) const;

    // vykreslov�n� trackeru
    virtual void	OnDrawTrackFrameInDP(CDC* pDC,const CFMntScrollerTracker*) const;
    virtual void	OnDrawObject(CDC* pDC, COLORREF cFrame, int frameBrushStyle, COLORREF cEntir, int entireBrushStyle, const CDrawPar* pParams) const;
  };

// CPositionPoly - poloha - polygon
class /*AFX_EXT_CLASS*/ CPositionPoly : public CPositionBase
{
DECLARE_DYNCREATE(CPositionPoly)
public:
					CPositionPoly();
	virtual ~CPositionPoly();

  void CreateBox(const CPoint& center, float sizeX, float sizeY, const Matrix3& orient);
  void CreateEllipse(const CPoint& center, float sizeX, float sizeY, const Matrix3& orient);

					// kopie objektu
	virtual void	CopyFrom(const CPositionBase*);
					// serializace
	virtual	void	DoSerialize(CArchive& ar, DWORD nVer);
					// zru�en� v�ech �daj� polohy
	virtual	void	ResetPosition();
					// testov�n� podobnosti
	virtual BOOL	IsSamePos(const CPositionBase*) const;
					// obal polohy
  virtual RECT	GetPositionHull() const {return m_hull;};
  void RecalcPositionHull();

					// vytvo�en� regionu
			void	CreatePolyRgn(CRgn&) const;

					// je bod na plo�e ?
	virtual BOOL	IsPointAtArea(const POINT& pt) const;
	virtual BOOL	IsRectAtArea(const RECT& r,BOOL bFull) const;
					// je plocha v oblasti ?
	virtual BOOL	IsAreaInRect(const CRect&) const;
	virtual BOOL	IsAreaInRgn(const CRgn&) const;

					// posun plochy
	virtual void	OffsetPosition(int xAmount,int yAmount);
  virtual void  TransformPosition(FMntMatrix2Val trans, CPointVal origin);
					// p�evod na DP z LP
	virtual void	LPtoDP(CDC* pDC);
					// p�evod na DP ze Scrolleru
	virtual void	ScrolltoDP(CFMntScrollerView*);

					// vykreslov�n� trackeru
	virtual void	OnDrawTrackFrameInDP(CDC* pDC,const CFMntScrollerTracker*) const;
	virtual void	OnDrawTrackHitsInDP(CDC* pDC,const CFMntScrollerTracker*) const;
  //vykreslovani object
  virtual void	OnDrawObject(CDC* pDC, COLORREF cFrame, int frameBrushStyle, COLORREF cEntir, int entireBrushStyle, const CDrawPar* pParams) const;

  virtual bool IsIntersection(const CPositionBase&) const;

  // world with Position
  const OBJPOSITION& GetPoly() const {return m_Position;};
  void AddPoint(const POINT& pt) {m_Position.Add(pt);RecalcPositionHull();};

private:
  //data
	OBJPOSITION		m_Position;
  RECT m_hull; // actual hull
};

// CPositionMulti - poloha - polygon s v�ce body
class /*AFX_EXT_CLASS*/ CPositionMulti : public CPositionBase
{
DECLARE_DYNCREATE(CPositionMulti)
public:
					CPositionMulti();
  virtual ~CPositionMulti();

					// kopie objektu
	virtual void	CopyFrom(const CPositionBase*);
					// serializace
	virtual	void	DoSerialize(CArchive& ar, DWORD nVer);
					// zru�en� v�ech �daj� polohy
	virtual	void	ResetPosition();
					// testov�n� podobnosti
	virtual BOOL	IsSamePos(const CPositionBase*) const;
					// testov�n� platnosti
	virtual	BOOL	IsValidPosition() const;
					// vlo�en� nov�ho bodu
			void	DoAddNewPoint(const POINT&,BOOL bUpdateHull);

					// obal polohy
	virtual RECT	GetPositionHull() const;
			void	RecalcPositionHull();

					// vytvo�en� regionu
			void	CreateMultiRgn(CRgn&) const;

					// je bod na plo�e ?
	virtual BOOL	IsPointAtArea(const POINT& pt) const;
	virtual BOOL	IsRectAtArea(const RECT& r,BOOL bFull) const;
					// je plocha v oblasti ?
	virtual BOOL	IsAreaInRect(const CRect&) const;
	virtual BOOL	IsAreaInRgn(const CRgn&) const;

					// posun plochy
	virtual void OffsetPosition(int xAmount,int yAmount);
  virtual void TransformPosition(FMntMatrix2Val trans, CPointVal origin);
					// p�evod na DP z LP
	virtual void	LPtoDP(CDC* pDC);
					// p�evod na DP ze Scrolleru
	virtual void	ScrolltoDP(CFMntScrollerView*);

					// vykreslov�n� trackeru
	virtual void	OnDrawTrackFrameInDP(CDC* pDC,const CFMntScrollerTracker*) const;
	virtual void	OnDrawTrackHitsInDP(CDC* pDC,const CFMntScrollerTracker*) const;

  virtual bool IsIntersection(const CPositionBase&) const;

// data
public:
	enum { maxPolygonPoints = 12 };
	
	int				m_nCount;					// po�et bod�
	POINT			m_Points[maxPolygonPoints];
	CRect			m_rHull;
};

// CPositionArea - poloha - kombinace rect�
class /*AFX_EXT_CLASS*/ CPositionArea : public CPositionBase
{
  DECLARE_DYNCREATE(CPositionArea)
public:
  CPositionArea();
  ~CPositionArea();

  // kopie objektu
  virtual void	CopyFrom(const CPositionBase*);
  // serializace
  virtual	void	DoSerialize(CArchive& ar, DWORD nVer);
  // zru�en� v�ech �daj� polohy
  virtual	void	ResetPosition();
  // testov�n� podobnosti
  virtual BOOL	IsSamePos(const CPositionBase*) const;

  // testov�n� platnosti
  virtual	BOOL	IsValidPosition() const;
  // vlo�en� nov�ho rectu
  void	AddRect(const RECT&);
  void  RemoveRect(const RECT&);
  int NRects() const {return m_Position.GetSize();};
  CRect Rect(int i) const {return m_Position[i];};
  
  // obal polohy
  virtual RECT	GetPositionHull() const;
  void	RecalcPositionHull();

  // je bod na plo�e ?
  virtual BOOL	IsPointAtArea(const POINT& pt) const;
  virtual BOOL	IsRectAtArea(const RECT& r,BOOL bFull) const;
  // je plocha v oblasti ?
  virtual BOOL	IsAreaInRect(const CRect&) const;
  virtual BOOL	IsAreaInRgn(const CRgn&) const;

  // posun plochy
  virtual void	OffsetPosition(int xAmount,int yAmount);
  virtual void  TransformPosition(FMntMatrix2Val trans, CPointVal origin);
  // p�evod na DP z LP
  virtual void	LPtoDP(CDC* pDC);
  // p�evod na DP ze Scrolleru
  virtual void	ScrolltoDP(CFMntScrollerView*);

  // vytvo�en� regionu
  void	CreateAreaRgn(CRgn&) const;

  // vykreslov�n� trackeru
  virtual void	OnDrawTrackFrameInDP(CDC* pDC,const CFMntScrollerTracker*) const;
  virtual void	OnDrawTrackHitsInDP(CDC* pDC,const CFMntScrollerTracker*) const;
  virtual void	OnDrawObject(CDC* pDC, COLORREF cFrame, int frameBrushStyle, COLORREF cEntir, int entireBrushStyle, const CDrawPar* pParams) const;

  virtual bool IsIntersection(const CPositionBase&) const {return false;};

  // data
protected:
  CRectArray		m_Position;
  CRect			m_rHull;
};

// CPositionGroup - poloha - kombinace poloh
class /*AFX_EXT_CLASS*/ CPositionGroup : public CPositionBase
{
DECLARE_DYNCREATE(CPositionGroup)
public:
					CPositionGroup();
					~CPositionGroup();

					// kopie objektu
	virtual void	CopyFrom(const CPositionBase*);
					// serializace
	virtual	void	DoSerialize(CArchive& ar, DWORD nVer);
					// zru�en� v�ech �daj� polohy
	virtual	void	ResetPosition();
					// testov�n� podobnosti
	virtual BOOL	IsSamePos(const CPositionBase*) const;

					// testov�n� platnosti
	virtual	BOOL	IsValidPosition() const;
					// vlo�en� nov� pozice
			void	DoAddNewPos(const CPositionBase*);
					// obal polohy
	virtual RECT	GetPositionHull() const;
			void	RecalcPositionHull();

					// je bod na plo�e ?
	virtual BOOL	IsPointAtArea(const POINT& pt) const;
	virtual BOOL	IsRectAtArea(const RECT& r,BOOL bFull) const;
					// je plocha v oblasti ?
	virtual BOOL	IsAreaInRect(const CRect&) const;
	virtual BOOL	IsAreaInRgn(const CRgn&) const;

					// posun plochy
	virtual void OffsetPosition(int xAmount,int yAmount);
  virtual void TransformPosition(FMntMatrix2Val trans, CPointVal origin);
					// p�evod na DP z LP
	virtual void	LPtoDP(CDC* pDC);
					// p�evod na DP ze Scrolleru
	virtual void	ScrolltoDP(CFMntScrollerView*);

					// vykreslov�n� trackeru
	virtual void	OnDrawTrackFrameInDP(CDC* pDC,const CFMntScrollerTracker*) const;
	virtual void	OnDrawTrackHitsInDP(CDC* pDC,const CFMntScrollerTracker*) const;

  virtual bool IsIntersection(const CPositionBase&) const;

  int NPositions() const {return m_Position.GetSize();};
  CPositionBase * Position(int i) const {return (CPositionBase *) m_Position[i];};
// data
protected:
	CObArray		m_Position;
	CRect			m_rHull;
};

/////////////////////////////////////////////////////////////////////////////
// CPosEdBaseDoc - z�kladn� dokument projektu
class CPosEdBaseDoc;

class  CMgrTemplates : public CMntOptions
//class  CMgrTemplates : public CMntOptions
//class  CMgrTemplates : public CMntFileObject
{
DECLARE_DYNAMIC(CMgrTemplates)
private:
  CString m_sRoot;
public:
					// constructor
					CMgrTemplates();

	// update hodnot do souboru (funkce nesm� b�t vol�na p��mo)
			BOOL	LoadTemplates();
			BOOL	SaveTemplates();
  void SetStyle(WORD nStyle,LPCTSTR pRoot,LPCTSTR pDef = NULL) {
    //m_nStyle = nStyle;
    m_sRoot = pRoot;
    //m_sDefault = pDef;
  };
	virtual	void	DoSerialize(CArchive& ar, DWORD nVer, void* pParams);

	// nastaven� def. hodnot
	virtual void	SetDefaultValues() {};
	virtual void	InitOptionsMap() {};

// data
	CPosEdBaseDoc*	m_pOwner;
};

class CTextureLayer;

class /*AFX_EXT_CLASS*/ CPosEdBaseDoc : public CDocument
{
	DECLARE_DYNAMIC(CPosEdBaseDoc)
	typedef CDocument base;
protected:
	CPosEdBaseDoc();           // protected constructor used by dynamic creation

	ObjectBank m_ObjectTemplates;

  // Attributes
public:  
	CMgrTextures m_MgrTextures;
	CMgrObjects*  m_pMgrObjects; // replaced by m_ObjectTemplates
                                 // used only during loading of the files in old format.
	CMgrWoods		m_MgrWoods;
	CMgrNets		m_MgrNets;

	TextureBank m_Textures;

	IVisViewerExchange* _curRealViewer;

	IVisViewerExchange* RealViewer() 
	{
		return _curRealViewer;
	}

	void SetCurrentRealViewer(IVisViewerExchange* viewer) 
	{
		_curRealViewer = viewer;
	}  

	/// document action manager
	CActionManager	m_ActionManager;

	/// project texture folder
	CString				m_sTexturesPath;
	/// project object folder
	CString				m_sObjectsPath;

	/// size of basic grid in meters
	float m_nRealSizeUnit;    

	/// Size of sat. texture grid in grid units
	int m_satelliteGridG;
  
	virtual CTextureLayer* GetActiveTextureLayer() = 0;

protected:
	CString   m_configName; 

	// Operations
public:
	BOOL IsRealDocument() const;

	// zpracov�n� zpr�v z preview
	enum 
	{
		sndModeReal	  = 0,	// ud�lost z Buldozeru
		sndModeImport = 1,  // ud�lost z Buldozeru - import
		sndModeEditor = 2,  // ud�lost generuje editor
	};

//  virtual void SendRealMessage(const SPosMessage&) const;
//  virtual void HandleRealMessage(const SPosMessage&,int nMode);

	void UpdateObjectsPaths();	// nastaven� absolutn�ch cest
	virtual	void DoSerialize(CArchive& ar, int nVer);

	enum ProjDir 
	{
		/// directory for textures (like p:\island\data\)
		dirText,
		/// directory for objects (like p:\island\data3d\)
		dirObj,
		/// root directory (like p:\)
		dirRoot
	};

	void GetProjectDirectory(CString& sDir, ProjDir nDir, BOOL bRelativeToViewerDir = FALSE) const;

	const CString& GetConfigName() const 
	{
		return m_configName;
	}

	void SetConfigName(const CString& name) 
	{
		m_configName = name; 
		SetModifiedFlag();
	}

	// update oken na z�klade parametr�
	void DoUpdateAllViews(DWORD nType);
	void DoUpdateAllViews(CAction*   );
	void DoUpdateAllViews(CUpdatePar*);

	// pr�ce se spr�vcem akc�
	virtual	BOOL RealizeEditAction(CPosAction*);			
	void ProcessEditAction(CPosAction*);

	void OnEditUndoAction();
	void OnUpdateEditUndoAction(CCmdUI* pCmdUI);
	void OnEditRedoAction();
	void OnUpdateEditRedoAction(CCmdUI* pCmdUI);
	void OnDelUndoRedoActions();
	void OnUpdateDelUndoRedoActions(CCmdUI* pCmdUI);

	// Objects Templates
	Ref<CPosObjectTemplate> GetObjectTemplate(int ID);
	Ref<CPosObjectTemplate> GetObjectTemplate(const CString&);
	Ref<CPosObjectTemplate> GetObjectTemplateByName(const CString&);
	void AddObjectTemplateSetID(Ref<CPosObjectTemplate>& templ);

	int ObjectTemplatesSize() const 
	{
		return m_ObjectTemplates.Size();
	}

	Ref<CPosObjectTemplate> GetIthObjectTemplate(int i) 
	{
		return m_ObjectTemplates[i];
	}

  void DeleteObjectTemplates(AutoArray<int> deleteIDs);
  ObjectBank& GetObjectTemplates() {return m_ObjectTemplates;};

  /// WoodTemplates
  CPosWoodTemplate * GetWoodTemplate(const CString& tempName) const {return m_MgrWoods.GetWoodTemplate(tempName);};

  // Overrides
  // ClassWizard generated virtual function overrides
  //{{AFX_VIRTUAL(CPosEdBaseDoc)
public:
  virtual void Serialize(CArchive& ar);   
protected:
  virtual BOOL OnNewDocument();
  virtual void DeleteContents( ); 
  //}}AFX_VIRTUAL

  // Implementation
public:
  virtual ~CPosEdBaseDoc();

  // Generated message map functions
protected:
  //{{AFX_MSG(CPosEdBaseDoc)
  //}}AFX_MSG
  DECLARE_MESSAGE_MAP()
public:
  virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
};

/////////////////////////////////////////////////////////////////////////////
// Matematick� funkce (PosEdMath.cpp)

// p�evod stup�� na radi�ny a zp�t
double GetRadFromGrad(int nGra);
double GetRadFromGrad(double nGra);
int    GetIntGradFromRad(double nRad);
double GetGradFromRad(double nRad);
int    GetNormalizedGrad(int nGra);
float  GetNormalizedGrad(float nGra);
double GetNormalizedGrad(double nGra);


// vzd�lenost bod�
double GetDistancePoints(double xx, double xy, double yx, double yy);
double GetDistancePoints(REALPOS x, REALPOS y);

// uhel mezi dv�ma body				
double GetPointsRad(REALPOS x, REALPOS pCenter);

// poloha bodu - bod dan� �hlem nRad a polom�rem nR
void GetRadixPoint(POINT& pt, POINT ptCenter, int nR, double nRad);
void GetRadixPoint(REALPOS& pt, REALPOS ptCenter, double nR, double nRad);

// rotace bodu
REALPOS GetRotationPoint(REALPOS pt, REALPOS ptCenter, int nGra);
REALPOS GetRotationPoint(REALPOS pt, REALPOS ptCenter, double nRad);

// relativn� posuny objektu s�t� vzhledem k jeho bodu A
Vector3 GetRelativPointToA(Vector3 pt, Vector3 ptA);		// realtivn� posun k A
//Vector3 GetRelativPtRotToA(Vector3 pt, Vector3 ptA, int nGra);// realtivn� rotace k A
Vector3 GetRelativPtRotToA(Vector3 pt, Vector3 ptA, float nGra);// realtivn� rotace k A

Vector3 GetPointFromRelativA(Vector3 pt, Vector3 ptA);	// bod realtivn� k A (podle posunu)

// inicializace & serializace REALNETPOS
void InitRealNetPos(REALNETPOS&);
void MntSerializeNetPos(CArchive& ar, DWORD nVer, REALNETPOS&);

/////////////////////////////////////////////////////////////////////////////
// TODO: just a hack to prevent loading LODShapes during serialization
extern BOOL g_bLoadLODShapes;

/////////////////////////////////////////////////////////////////////////////
#endif // !defined(_POSEDCORE_MAIN_H_)

