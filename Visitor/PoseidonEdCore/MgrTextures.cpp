/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "math.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CPosTextureTemplate - �ablona textur

IMPLEMENT_DYNAMIC( CPosTextureTemplate, CObject )

CPosTextureTemplate::CPosTextureTemplate(CPosEdBaseDoc* pDoc)
{
	m_pOwner = pDoc;
  m_nTimesUsedInLandActive = 0;
  m_nTimesUsedInBaseActive = 0;
  m_nTimesUsedInLandGlobal = 0;
  m_nTimesUsedInBaseGlobal = 0;
  m_nTextureID = -1;
  m_bUseOwnMaterial = false;
};

CPosTextureTemplate::~CPosTextureTemplate()
{
};

void CPosTextureTemplate::MakeDefaultValues()
{
/*
	// defaultov� hodnoty
	m_bIsSea		= FALSE;
	m_nVarPorpab	= 0.f;
	m_cTxtrColor	= RGB(0,0,0);
*/
	// nen� napojen� na definovan� textury
	//m_pPrimarTxtr	= NULL;
	m_pLandZone		= NULL;
};

void CPosTextureTemplate::CopyFrom(const CPosTextureTemplate* pSrc)
{
	m_pOwner      = pSrc->m_pOwner;

	m_nTextureID	= pSrc->m_nTextureID;	
	m_sTextureFile	= pSrc->m_sTextureFile;	
	m_sTextureName	= pSrc->m_sTextureName;
  m_sMaterialFile	= pSrc->m_sMaterialFile;

  m_bUseOwnMaterial = pSrc->m_bUseOwnMaterial;

	m_nTxtrType		= pSrc->m_nTxtrType;
	m_nVarPorpab	= pSrc->m_nVarPorpab;
	m_bIsSea		 = pSrc->m_bIsSea;		
	m_cTxtrColor	= pSrc->m_cTxtrColor;
  m_nTimesUsedInLandActive = pSrc->m_nTimesUsedInLandActive;
  m_nTimesUsedInBaseActive = pSrc->m_nTimesUsedInBaseActive;
  m_nTimesUsedInLandGlobal = pSrc->m_nTimesUsedInLandGlobal;
  m_nTimesUsedInBaseGlobal = pSrc->m_nTimesUsedInBaseGlobal;

	// apojen� na hierarchii definic
	m_pLandZone		= pSrc->m_pLandZone;
};

// serializace
void CPosTextureTemplate::DoSerialize(CArchive& ar, DWORD nVer)
{
	// serializace dat
	MntSerializeInt(ar,m_nTextureID);
	MntSerializeString(ar,m_sTextureFile);
	MntSerializeString(ar,m_sTextureName);

  if (ar.IsLoading() && nVer < 43)  
    m_sMaterialFile = m_sTextureName + RVMAT_FILE_EXT;  
  else
  {
    MntSerializeString(ar, m_sMaterialFile);
    BOOL temp = m_bUseOwnMaterial;
    MntSerializeBOOL(ar, temp);
    m_bUseOwnMaterial = temp ? true : false;
  }

	MntSerializeInt(ar,m_nTxtrType);
	MntSerializeInt(ar,m_bIsSea);
	MntSerializeFloat(ar,m_nVarPorpab);
	MntSerializeColor(ar,m_cTxtrColor);
};

// vykreslen� v ListBox
void CPosTextureTemplate::DrawLBItem(CDC* pDC,CRect& rPos,BOOL bProp) const
{
	CRect rText = rPos; rText.left += 2;
	// jm�no
  
  /*if (m_nTimesUsedInLandActive > 0)
  {
    CString name;
    name.Format(_T("%s %d"), m_sTextureName, m_nTimesUsedInLandActive);
    ::DrawText(pDC->m_hDC,name,-1,&rText,DT_LEFT|DT_NOPREFIX|DT_SINGLELINE|DT_VCENTER|DT_TABSTOP);
  }
  else*/
	  ::DrawTextA(pDC->m_hDC,m_sTextureName,-1,&rText,DT_LEFT|DT_NOPREFIX|DT_SINGLELINE|DT_VCENTER|DT_TABSTOP);
	// pravd�podobnost
	if (bProp)
	{
		double  nPop = Round((double)m_nVarPorpab,1);
		CString sText; NumToLocalString(sText,nPop,1);
		sText += _T(" %");
		::DrawTextA(pDC->m_hDC,sText,-1,&rText,DT_RIGHT|DT_NOPREFIX|DT_SINGLELINE|DT_VCENTER|DT_TABSTOP);
	};
};

COLORREF CPosTextureTemplate::GetLBItemColor() const
{	return m_cTxtrColor; };

// update dat ze souboru pac.
void CPosTextureTemplate::UpdateParamsFromSourceFile()
{
	/*CString sPacFile; // color will be not read from texture
	GetFullTextureFileName(sPacFile);
	COLORREF cColor = TextureColor( sPacFile );
	m_cTxtrColor	= cColor;*/
};

// vytvo�en� ze souboru
void CPosTextureTemplate::CreateFromPacFile(LPCTSTR pFileName)
{	
	MakeDefaultValues();
	// ID nen� ur�eno
	m_nTextureID = UNDEF_REG_ID;
	// soubor z pln� cesty
	SetFileFromFullPath(pFileName);
	// jm�no ze souboru
	GetNameFromFile(m_sTextureName);

  m_sMaterialFile = m_sTextureName + RVMAT_FILE_EXT;
	// textura je default. prim�rn�
	m_nTxtrType = txtrTypePrimar;
	// nen� to default. mo�e
	m_bIsSea = FALSE;
	// 0 pravd�podobnost
	m_nVarPorpab = 0.f;
	// barva nen� ur�ena 
	m_cTxtrColor = RGB(0,0,0);

	// update dat ze souboru pac
	UpdateParamsFromSourceFile();
};

// pl� cesta k souboru pac (m_sTextureFile)
void CPosTextureTemplate::GetFullTextureFileName(CString& sFile) const
{
	ASSERT(m_sTextureFile.GetLength()>0);
	ASSERT(m_pOwner != NULL);
	m_pOwner->GetProjectDirectory(sFile, CPosEdBaseDoc::dirText);
	sFile += m_sTextureFile;
};
// realativn� cesta k souboru pac (m_sTextureFile) 
void CPosTextureTemplate::GetBuldozerTextureFileName(CString& sFile) const
{
	ASSERT(m_sTextureFile.GetLength()>0);
	CString sPom;
	ASSERT(m_pOwner != NULL);
	m_pOwner->GetProjectDirectory(sPom, CPosEdBaseDoc::dirText, TRUE);
	// najdu posledn� adres��
	//int nLen = sPom.GetLength()-2;
	//while((nLen >= 0)&&(sPom[nLen]!=_T('\\'))&&(sPom[nLen]!=_T('/')))
	//	nLen--;
  MakeLongDirStr(sFile,(LPCTSTR) sPom);
	//sFile  = (LPCTSTR)(sPom.GetBuffer(0)+(nLen+1));
  //sFile = sPom;
	sFile += m_sTextureFile;
};

void CPosTextureTemplate::GetTextureFileName(CString& sFile) const
{
  sFile = m_sTextureFile;
}

void CPosTextureTemplate::GetBimPasFileName(CString& sFile) const
{
  CutExtFromFileName(sFile, m_sTextureName);
  sFile += BIMPAS_FILE_EXT;
}

void CPosTextureTemplate::GetFullBimPasFileName(CString& sFile) const
{
  ASSERT(m_pOwner != NULL);
  m_pOwner->GetProjectDirectory(sFile, CPosEdBaseDoc::dirText);
  CString bimPasFile;
  GetBimPasFileName(bimPasFile);  
  sFile += bimPasFile;
}
void	CPosTextureTemplate::GetBuldozerBimPasFileName(CString& sFile) const
{
  ASSERT(m_pOwner != NULL);
  m_pOwner->GetProjectDirectory(sFile, CPosEdBaseDoc::dirText, TRUE); 
  CString bimPasFile;
  GetBimPasFileName(bimPasFile);  
  sFile += bimPasFile;
}

void CPosTextureTemplate::GetRvMatFileName(CString& sFile) const
{
  if (m_nTxtrType == txtrTypePrimar || m_bUseOwnMaterial)
  {
    // is primary      
    sFile = m_sMaterialFile;
  }
  else
  {
    CPosTextureTemplate *prim = GetPrimarTexture();
    if (prim) GetPrimarTexture()->GetRvMatFileName(sFile);
    else sFile = m_sMaterialFile;
  }
}

void CPosTextureTemplate::GetFullRvMatFileName(CString& sFile) const
{
  if (m_nTxtrType == txtrTypePrimar || m_bUseOwnMaterial)
  {
    // is primary  
    ASSERT(m_pOwner != NULL);
    m_pOwner->GetProjectDirectory(sFile, CPosEdBaseDoc::dirText);
    CString rvMatFile;
    GetRvMatFileName(rvMatFile);  
    sFile += rvMatFile;
  }
  else if (m_nTxtrType == txtrTypeVariant)
    GetPrimarTexture()->GetFullRvMatFileName(sFile);
}

void	CPosTextureTemplate::GetBuldozerRvMatFileName(CString& sFile) const
{
  if (m_nTxtrType == txtrTypePrimar || m_bUseOwnMaterial)
  {
    // is primary  
    ASSERT(m_pOwner != NULL);
    m_pOwner->GetProjectDirectory(sFile, CPosEdBaseDoc::dirText, TRUE); 
    CString rvMatFile;
    GetRvMatFileName(rvMatFile);  
    sFile += rvMatFile;
  }
  else if (m_nTxtrType == txtrTypeVariant)
    GetPrimarTexture()->GetBuldozerRvMatFileName(sFile);
}

// jm�no podle m_sTextureFile
void CPosTextureTemplate::GetNameFromFile(CString& sName) const
{
	ASSERT(m_sTextureFile.GetLength()>0);
	sName = m_sTextureFile;
	int nBack = sName.ReverseFind(_T('.'));
	if (nBack>0)
	{
		(sName.GetBuffer(0))[nBack] = _T(0);
		 sName.ReleaseBuffer();
	};
};

// m_sTextureFile z pln� cesty
void CPosTextureTemplate::SetFileFromFullPath(LPCTSTR pPath)
{
  CString sName = pPath;
  CString sDir;
  ASSERT(m_pOwner != NULL);
  m_pOwner->GetProjectDirectory(sDir, CPosEdBaseDoc::dirText);
  MakeLongDirStr(sDir,sDir);
  // find relative path to sDir. pPath must start with sDir;
  ASSERT(sDir.GetLength() < sName.GetLength());

  m_sTextureFile  = (LPCTSTR)(sName.GetBuffer(0)+sDir.GetLength());
  m_sTextureFile.MakeLower();
};

void CPosTextureTemplate::SetRvMatFileNameFromFullPath(LPCTSTR pPath)
{
  CString sName = pPath;
  CString sDir;
  ASSERT(m_pOwner != NULL);
  m_pOwner->GetProjectDirectory(sDir, CPosEdBaseDoc::dirText);
  MakeLongDirStr(sDir,sDir);
  // find relative path to sDir. pPath must start with sDir;
  ASSERT(sDir.GetLength() < sName.GetLength());

  m_sMaterialFile  = (LPCTSTR)(sName.GetBuffer(0)+sDir.GetLength());
  m_sMaterialFile.MakeLower();
};

// ��st jm�na pro sekund�rn� texturu
BOOL CPosTextureTemplate::GetScndTxtrNamePart(CString& sName) const 
{
	if (m_sTextureName.GetLength()<2)
		return FALSE;
	if (m_sTextureName.GetLength()>MAX_FILENAME_LEN_TEXTURE_PRIMAR)
		return FALSE;
  sName = m_sTextureName;
	return TRUE; // je to dobr� jm�no
};

BOOL CPosTextureTemplate::IsAllowedSecondary() const
{
  return m_pLandZone->IsAllowedSecondary();
};

CPosTextureTemplate * CPosTextureTemplate::GetPrimarTexture() const 
{
  return m_pLandZone->m_pPrimarTxtr;
}

/////////////////////////////////////////////////////////////////////////////
// CPosTextureZone - p�smo = prim�rn� a variantn� textury od dan� v��ky ...

IMPLEMENT_DYNAMIC( CPosTextureZone, CObject )

CPosTextureZone::CPosTextureZone()
{
	m_nHeightMin  =	0.f;
	m_nHeightMax  = 0.f;
	m_nHgGradMin  = MIN_LAND_HGGRAD;
	m_nHgGradMax  = MAX_LAND_HGGRAD;
	m_pPrimarTxtr = NULL;
	m_bPrimColOnly=
	m_bEnRandomVar= TRUE;
	m_pOwner	  = NULL;
};
CPosTextureZone::~CPosTextureZone()
{	Destroy();	};

void CPosTextureZone::CopyFrom(const CPosTextureZone* pSrc)
{	// ma�u p�vodn� obsah
	Destroy();

	m_sZoneName		= pSrc->m_sZoneName;
	m_nHeightMin	= pSrc->m_nHeightMin;
	m_nHeightMax	= pSrc->m_nHeightMax;
	m_nHgGradMin	= pSrc->m_nHgGradMin;
	m_nHgGradMax	= pSrc->m_nHgGradMax;
	m_bPrimColOnly	= pSrc->m_bPrimColOnly;
	m_bEnRandomVar	= pSrc->m_bEnRandomVar;
  m_bNoSecondary = pSrc->m_bNoSecondary;

  m_pPrimarTxtr = pSrc->m_pPrimarTxtr;
  m_VarTxtTmpl = pSrc->m_VarTxtTmpl;
	
	
	ASSERT(m_VarTxtTmpl.Size()==pSrc->m_VarTxtTmpl.Size());
};
void CPosTextureZone::Destroy()
{
	/*if (m_pPrimarTxtr != NULL)
	{
		delete m_pPrimarTxtr;
		m_pPrimarTxtr = NULL;
	}*/
	//DestroyArrayObjects(&m_VarTxtTmpl);
};

// update p��znak� a vlastn�k� v hierarchii definic
void CPosTextureZone::UpdateFlagsAndOwners()
{
	ASSERT(m_pOwner != NULL);
	// p��znaky mo�e
	if (m_pPrimarTxtr != NULL)
	{
		m_pPrimarTxtr->m_bIsSea = m_pOwner->m_bIsSea;
		//m_pPrimarTxtr->m_pPrimarTxtr = NULL;
		m_pPrimarTxtr->m_pLandZone = this;
	}
	for(int i=0;i<m_VarTxtTmpl.Size(); i++)
	{
		CPosTextureTemplate* pTxtr = (CPosTextureTemplate*)(m_VarTxtTmpl[i]);
		if (pTxtr!=NULL)
		{
			pTxtr->m_bIsSea = m_pOwner->m_bIsSea;
			//pTxtr->m_pPrimarTxtr = m_pPrimarTxtr;
			pTxtr->m_pLandZone = this;
		}
	}
};

// serializace
void CPosTextureZone::DoSerialize(CArchive& ar, DWORD nVer)
{
	if (!ar.IsStoring())
		Destroy();	// vypr�zdn�n� 
	// serializace dat
	MntSerializeString(ar,m_sZoneName);
	if (nVer >= 15)
	{
		MntSerializeFloat(ar,m_nHeightMin);
		MntSerializeFloat(ar,m_nHeightMax);
		MntSerializeFloat(ar,m_nHgGradMin);
		MntSerializeFloat(ar,m_nHgGradMax);
	} else
	{
		MntSerializeFloat(ar,m_nHeightMin);
		m_nHeightMax = m_nHeightMin;
	}
	MntSerializeBOOL(ar,m_bPrimColOnly);
	MntSerializeBOOL(ar,m_bEnRandomVar);
  if (ar.IsStoring())
    MntSerializeBOOL(ar,m_bNoSecondary);
  else
  {
    if (nVer > 40)
      MntSerializeBOOL(ar,m_bNoSecondary);
    else
      m_bNoSecondary = FALSE;
  }

	// serializace prim�rn� textury
	BOOL bExistPrim = (m_pPrimarTxtr != NULL);
	MntSerializeBOOL(ar,bExistPrim);
	if (bExistPrim)
	{
		if (ar.IsStoring())
		{
			ASSERT(m_pPrimarTxtr != NULL);
      MntSerializeInt(ar,m_pPrimarTxtr->m_nTextureID);			
		} 
    else
		{
			ASSERT(m_pPrimarTxtr.IsNull());
      if (nVer < 35)
      {     
        m_pPrimarTxtr = new CPosTextureTemplate(m_pOwner->m_pOwner);
		
	  		m_pPrimarTxtr->DoSerialize(ar, nVer); 
        m_pPrimarTxtr->m_nTxtrType = CPosTextureTemplate::txtrTypePrimar;
        m_pOwner->m_pOwner->m_Textures.AddID(m_pPrimarTxtr);
      }
      else
      {
        int ID;
        MntSerializeInt(ar,ID);
        m_pPrimarTxtr = m_pOwner->m_pOwner->m_Textures.FindEx(ID);        
      }
		};
	}
	// serializace variantn�ch textur
	int nCount = m_VarTxtTmpl.Size();
	MntSerializeInt(ar,nCount);
	for(int i=0; i<nCount; i++)
	{
		if (ar.IsStoring())
		{
			CPosTextureTemplate* pTxtr = (CPosTextureTemplate*)(m_VarTxtTmpl[i]);
			ASSERT(pTxtr != NULL);
      MntSerializeInt(ar, pTxtr->m_nTextureID);			
		} else
		{
      if (nVer < 35)
      {      
        Ref<CPosTextureTemplate> pTxtr = new CPosTextureTemplate(m_pOwner->m_pOwner);
        m_VarTxtTmpl.Add(pTxtr);
        pTxtr->DoSerialize(ar, nVer);   
        pTxtr->m_nTxtrType = CPosTextureTemplate::txtrTypeVariant;
        m_pOwner->m_pOwner->m_Textures.AddID(pTxtr);
      }
      else
      {
        int ID;
        MntSerializeInt(ar,ID);
        Ref<CPosTextureTemplate> pTxtr = m_pOwner->m_pOwner->m_Textures.FindEx(ID);
        if (pTxtr.NotNull())
          m_VarTxtTmpl.Add(pTxtr);        
        else
          LogF("Zone %s, serialize. Texture %d, not found",m_sZoneName, ID);                 
      };
    }
	}
	if (nCount != m_VarTxtTmpl.Size())
		MntInvalidFileFormat();		
};

// vykreslen� v ListBox
void CPosTextureZone::DrawLBItem(CDC* pDC,CRect& rPos) const
{
	CRect rPom,rText = rPos; rText.left += 2;
	int	  nPos = rText.Width()/2;
	// jm�no
	rPom = rText; rPom.right = rPom.left + nPos;
	::DrawTextA(pDC->m_hDC,m_sZoneName,-1,&rPom,DT_LEFT|DT_NOPREFIX|DT_SINGLELINE|DT_VCENTER|DT_TABSTOP);
	// v��ka
	rPom = rText; rPom.left += nPos;
	CString sNum,sText; 
	NumToLocalString(sNum,(double)m_nHeightMin,0);
	sText += sNum;
	sText += _T(";");
	NumToLocalString(sNum,(double)m_nHeightMax,0);
	sText += sNum;
	sText += _T(";");
	NumToLocalString(sNum,(double)m_nHgGradMin,0);
	sText += sNum;
	sText += _T(";");
	NumToLocalString(sNum,(double)m_nHgGradMax,0);
	sText += sNum;
	::DrawTextA(pDC->m_hDC,sText,-1,&rPom,DT_LEFT|DT_NOPREFIX|DT_SINGLELINE|DT_VCENTER|DT_TABSTOP);
};

COLORREF CPosTextureZone::GetLBItemColor() const
{
	COLORREF cColor = RGB(0,0,0);
	if (m_pPrimarTxtr != NULL)
		cColor = m_pPrimarTxtr->m_cTxtrColor;
	return cColor;
};

// n�hodmn� zvolen� textura
PTRTEXTURETMPLT	CPosTextureZone::GetRandomTexture()
{
	// existuje alespo� prim�rn� textura
	ASSERT(m_pPrimarTxtr != NULL);
	// nen� z �eho volit, nebo je z�kaz n�dn� volby
	if ((!m_bEnRandomVar)||(m_VarTxtTmpl.Size()==0))
		return m_pPrimarTxtr;
	ASSERT(m_VarTxtTmpl.Size()>0);
	// n�hodn� ��slo
	int   nRanNum = rand();
	float nToNum  = (float)nRanNum;
		  nToNum  = nToNum/((float)RAND_MAX);
		  nToNum *= 100.f; 
	ASSERT((nToNum>=0.f)&&(nToNum<=100.f));
	// zn�m ��slo 
	float nSumPr  = 0.f;
	for(int i=0;i<m_VarTxtTmpl.Size();i++)
	{
		PTRTEXTURETMPLT pTxtr = (PTRTEXTURETMPLT)(m_VarTxtTmpl[i]);
		if (pTxtr)
		{
			nSumPr += pTxtr->m_nVarPorpab;
			if (nSumPr > nToNum)
				return pTxtr;
		}
	}
	// nebyla zvolena ��dn� variantn� -> vol�m prim�rn�
	return m_pPrimarTxtr;
};

BOOL CPosTextureZone::CreateLocalTextures()
{
  if (m_pPrimarTxtr.NotNull())
  {
    Ref<CPosTextureTemplate> pTextr = new CPosTextureTemplate(m_pOwner->m_pOwner);
    if (pTextr.IsNull())    
      return FALSE;

    pTextr->CopyFrom(m_pPrimarTxtr);
    pTextr->m_pLandZone = this;
    m_pPrimarTxtr = pTextr;
  }

  for(int i = 0; i < m_VarTxtTmpl.Size(); i++)
  {
    if (m_VarTxtTmpl[i].NotNull())
    {
      Ref<CPosTextureTemplate> pTextr = new CPosTextureTemplate(m_pOwner->m_pOwner);
      if (pTextr.IsNull())    
        return FALSE;

      pTextr->CopyFrom(m_VarTxtTmpl[i]);
      pTextr->m_pLandZone = this;
      m_VarTxtTmpl[i] = pTextr;    
    }
  }
  
  return TRUE;
}

void CPosTextureZone::SyncLocalTexturesIntoDoc()
{
  TextureBank & bank = m_pOwner->m_pOwner->m_Textures;
  if (m_pPrimarTxtr.NotNull())
  {
    Ref<CPosTextureTemplate> pTextr = bank.FindEx(m_pPrimarTxtr->m_nTextureID);
    if (pTextr.IsNull())
    {
      bank.AddID(m_pPrimarTxtr);    
    }
    else
    {
      pTextr->CopyFrom(m_pPrimarTxtr);
      m_pPrimarTxtr = pTextr;      
    }
    // create also bimpas file for the texture
    CString bimPasPath;
    m_pPrimarTxtr->GetFullBimPasFileName(bimPasPath);
    CPoseidonMap::CreateSquareSurfaceBimPasFile(bimPasPath,m_pPrimarTxtr);

  }

  for(int i = 0; i < m_VarTxtTmpl.Size(); i++)
  {
    if (m_VarTxtTmpl[i].NotNull())
    {
      Ref<CPosTextureTemplate> pTextr = bank.FindEx(m_VarTxtTmpl[i]->m_nTextureID);
      if (pTextr.IsNull())

        bank.AddID(m_VarTxtTmpl[i]);
      else
      {
        pTextr->CopyFrom(m_VarTxtTmpl[i]);
        m_VarTxtTmpl[i] = pTextr;
      }      
      // create also bimpas file for the texture
      CString bimPasPath;
      m_VarTxtTmpl[i]->GetFullBimPasFileName(bimPasPath);
      CPoseidonMap::CreateSquareSurfaceBimPasFile(bimPasPath,m_VarTxtTmpl[i]);
    }
  }
}

bool CPosTextureZone::ExistTextureName(const CString& name, const CPosTextureTemplate * ignore) const
{
  if (m_pPrimarTxtr.NotNull() && m_pPrimarTxtr != ignore && m_pPrimarTxtr->Name() == name)
    return true;

  for(int i = 0; i < m_VarTxtTmpl.Size(); i++)
  {
    if (m_VarTxtTmpl[i].NotNull() && m_VarTxtTmpl[i] != ignore && m_VarTxtTmpl[i]->Name() == name)
      return true;
  }

  return false;
}

BOOL CPosTextureZone::IsInUseBase()
{
  if (m_pPrimarTxtr.NotNull() && m_pPrimarTxtr->m_nTimesUsedInBaseGlobal > 0)
    return TRUE;

  for(int i = 0; i < m_VarTxtTmpl.Size(); i++)
  {
    if (m_VarTxtTmpl[i].NotNull() && m_VarTxtTmpl[i]->m_nTimesUsedInBaseGlobal > 0)
      return TRUE;
  }

  return FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// CPosTextureLand - typ krajiny = set��d�n� p�sma dle v��ky

IMPLEMENT_DYNAMIC( CPosTextureLand, CObject )

CPosTextureLand::CPosTextureLand(CPosEdBaseDoc* pOwner)
{	
	m_pOwner = pOwner;
	m_bIsSea	= FALSE;	
	m_cLandColor= 0L;
};
CPosTextureLand::~CPosTextureLand()
{	Destroy();	};

void CPosTextureLand::CopyFrom(const CPosTextureLand* pSrc)
{	// zru�en� obsahu
	Destroy();
	m_pOwner			= pSrc->m_pOwner;
	// kopie dat
	m_sLandName		= pSrc->m_sLandName;
	m_sLandHeight	= pSrc->m_sLandHeight;
	m_bIsSea		= pSrc->m_bIsSea;
	m_cLandColor	= pSrc->m_cLandColor;
	// kopie v��kov�ch oblast�
	for(int i=0;i<pSrc->m_LandZones.GetSize();i++)
	{
		CPosTextureZone* pOldZone = (CPosTextureZone*)(pSrc->m_LandZones[i]);
		CPosTextureZone* pNewZone = new CPosTextureZone();

		// kop�ruji data pro typ krajiny
		pNewZone->m_pOwner = this;
		pNewZone->CopyFrom(pOldZone); // do not copy textures
		m_LandZones.Add(pNewZone);
	}
	ASSERT(m_LandZones.GetSize()==pSrc->m_LandZones.GetSize());
};

void CPosTextureLand::Destroy()
{
	m_sLandName.Empty();
	m_sLandHeight.Empty();
	DestroyArrayObjects(&m_LandZones);
};

// update p��znak� a vlastn�k� v hierarchii definic
void CPosTextureLand::UpdateFlagsAndOwners()
{
	for(int i=0;i<m_LandZones.GetSize();i++)
	{
		CPosTextureZone* pZone = (CPosTextureZone*)(m_LandZones[i]);
		if (pZone)
		{
			pZone->m_pOwner = this;
			pZone->UpdateFlagsAndOwners();
		}
	};
};

// serializace
void CPosTextureLand::DoSerialize(CArchive& ar, DWORD nVer)
{
	if (!ar.IsStoring())
		Destroy();	// vypr�zdn�n� 
	// serializace jmen
	MntSerializeString(ar,m_sLandName);
	MntSerializeString(ar,m_sLandHeight);
	MntSerializeBOOL(ar,m_bIsSea);
	MntSerializeColor(ar,m_cLandColor);
	// serializace p�sem
	int nCount = m_LandZones.GetSize();
	MntSerializeInt(ar,nCount);
	for(int i=0; i<nCount; i++)
	{
		if (ar.IsStoring())
		{
			CPosTextureZone* pZone = (CPosTextureZone*)(m_LandZones[i]);
			ASSERT(pZone != NULL);
			pZone->DoSerialize(ar, nVer);
		} else
		{
			CPosTextureZone* pZone = new CPosTextureZone();
			pZone->m_pOwner = this;
			m_LandZones.Add(pZone);
			pZone->DoSerialize(ar, nVer);
		};
	}
	if (nCount != m_LandZones.GetSize())
		MntInvalidFileFormat();		
};

// vykreslen� v ListBox
void CPosTextureLand::DrawLBItem(CDC* pDC,CRect& rPos) const
{
	CRect rText = rPos; rText.left += 2;
  ::DrawTextA(pDC->m_hDC,m_sLandName,-1,&rText,DT_LEFT|DT_NOPREFIX|DT_SINGLELINE|DT_VCENTER|DT_TABSTOP);
};

COLORREF CPosTextureLand::GetLBItemColor() const
{	return m_cLandColor; };

// pr�ce s p�smy
void CPosTextureLand::AddNewZone(CPosTextureZone* pZone)
{
	ASSERT(pZone != NULL);
	// set��d�m podle nadmo�sk� v��ky
	int nInsAt = -1;

	for(int i=0;i<m_LandZones.GetSize();i++)
	{
		CPosTextureZone* pOldZone = (CPosTextureZone*)(m_LandZones[i]);
		if ((pOldZone != NULL) && (pOldZone->m_nHeightMin >= pZone->m_nHeightMin))
		{
			nInsAt = i;
			break;
		}
	}
	if (nInsAt >= 0)
		m_LandZones.InsertAt(nInsAt,pZone);
	else
		m_LandZones.Add(pZone);
};

void CPosTextureLand::DeleteZone(CPosTextureZone* pZone,BOOL bDelObj)
{
	for(int i=0;i<m_LandZones.GetSize();i++)
	{
		CPosTextureZone* pOldZone = (CPosTextureZone*)(m_LandZones[i]);
		if ((pOldZone == pZone)&&(pOldZone != NULL))
		{
			m_LandZones.RemoveAt(i);
			if (bDelObj)
				delete pOldZone;
			return;	// OK
		}
	}
	// musela b�t nalezena
	ASSERT(FALSE);
};

// textura podle v��ky
CPosTextureZone* CPosTextureLand::GetTextureZoneForHeight(LANDHEIGHT nHeight,float nHgGr)
{
	int   nCount = m_LandZones.GetSize();
	float nHgGrad= (float)atan((double)nHgGr);
		  nHgGrad= (nHgGrad / 3.1416f)*180.f;
	if (nHgGrad < MIN_LAND_HGGRAD) nHgGrad = MIN_LAND_HGGRAD;
	if (nHgGrad > MAX_LAND_HGGRAD) nHgGrad = MAX_LAND_HGGRAD;

	for(int i=nCount-1; i>=0; i--)
	{
		CPosTextureZone* pZone = (CPosTextureZone*)(m_LandZones[i]);
		if (i==0)
			return pZone;	// dafaultov� v�dy n�co vyhovuje
	
		if ((pZone->m_nHeightMin <= nHeight)&&(pZone->m_nHeightMax >= nHeight)&&
			(pZone->m_nHgGradMin <= nHgGrad)&&(pZone->m_nHgGradMax >= nHgGrad))
			return pZone;
	}
	return NULL;
};

PTRTEXTURETMPLT	CPosTextureLand::GetTextureForHeight(LANDHEIGHT nHeight,float nHgGrad,BOOL bRandom)
{
	CPosTextureZone* pZone = GetTextureZoneForHeight(nHeight,nHgGrad);
	if (pZone != NULL)
	{
		if (bRandom)
			return pZone->GetRandomTexture();
		return pZone->m_pPrimarTxtr;
	}
	return NULL;
};

BOOL CPosTextureLand::CreateLocalTextures()
{
  for(int i = 0; i < m_LandZones.GetSize(); i++)
  {
    CPosTextureZone * pZone = (CPosTextureZone *) m_LandZones[i];

    if (!pZone->CreateLocalTextures())
      return FALSE;
  }
  return TRUE;
}

void CPosTextureLand::SyncLocalTexturesIntoDoc()
{
  for(int i = 0; i < m_LandZones.GetSize(); i++)
  {
    CPosTextureZone * pZone = (CPosTextureZone *) m_LandZones[i];
    if (pZone)
      pZone->SyncLocalTexturesIntoDoc();
  }  
}

BOOL CPosTextureLand::IsInUseBase()
{
  for(int i = 0; i < m_LandZones.GetSize(); i++)
  {
    CPosTextureZone * pZone = (CPosTextureZone *) m_LandZones[i];
    if (pZone && pZone->IsInUseBase())
      return TRUE;
  }

  return FALSE;
}

bool CPosTextureLand::ExistTextureName(const CString& name, const CPosTextureZone * doNotSearch, const CPosTextureTemplate * ignore) const
{
  for(int i = 0; i < m_LandZones.GetSize(); i++)
  {
    CPosTextureZone * pZone = (CPosTextureZone *) m_LandZones[i];
    if (pZone != doNotSearch)
    {    
      if (pZone && pZone->ExistTextureName(name, ignore))
        return true;
    }
  }

  return false;
}

/////////////////////////////////////////////////////////////////////////////
// CMgrTextures - mana�er textur

CMgrTextures::CMgrTextures()
{
};

CMgrTextures::~CMgrTextures()
{	Destroy(); };

void CMgrTextures::CopyFrom(const CMgrTextures* pSrc)
{	// zru�en� obsahu
	Destroy();
	// kopie �ablon
	for(int i=0;i<pSrc->m_TextureLands.GetSize();i++)
	{
		CPosTextureLand* pOldLand = (CPosTextureLand*)(pSrc->m_TextureLands[i]);
		CPosTextureLand* pNewLand = new CPosTextureLand(m_pOwner);
		// kop�ruji data pro typ krajiny
		pNewLand->CopyFrom(pOldLand);
		m_TextureLands.Add(pNewLand);
	}
	ASSERT(m_TextureLands.GetSize()==pSrc->m_TextureLands.GetSize());
	// update vlastn�k� v nov� kopii
	UpdateFlagsAndOwners();
};

void CMgrTextures::Destroy()
{	DestroyArrayObjects(&m_TextureLands); };

// update p��znak� a vlastn�k� v hierarchii definic
void CMgrTextures::UpdateFlagsAndOwners()
{
	for(int i=0;i<m_TextureLands.GetSize();i++)
	{
		CPosTextureLand* pLand = (CPosTextureLand*)(m_TextureLands[i]);
		if (pLand)
			pLand->UpdateFlagsAndOwners();
	};
};

// serializace
void CMgrTextures::DoSerialize(CArchive& ar, DWORD nVer)
{
	BOOL bEmpty = (m_TextureLands.GetSize()==0);
	MntSerializeBOOL(ar,bEmpty);
	if (bEmpty) return;	// tento manazer je pr�zdn�

	if (!ar.IsStoring())
		Destroy();	// vypr�zdn�n� 
	
	// serializace typ� krajiny
	int nCount = m_TextureLands.GetSize();
	MntSerializeInt(ar,nCount);
	for(int i=0; i<nCount; i++)
	{
		if (ar.IsStoring())
		{
			CPosTextureLand* pLand = (CPosTextureLand*)(m_TextureLands[i]);
			ASSERT(pLand != NULL);
			pLand->DoSerialize(ar, nVer);
		} else
		{
			CPosTextureLand* pLand = new CPosTextureLand(m_pOwner);
			m_TextureLands.Add(pLand);
			pLand->DoSerialize(ar, nVer);
		};
	}
	if (nCount != m_TextureLands.GetSize())
		MntInvalidFileFormat();		

	// update vlastn�k� a p��znak�
	if (!ar.IsStoring())
		UpdateFlagsAndOwners();
};

BOOL CMgrTextures::CreateLocalTextures()
{
  for(int i = 0; i < m_TextureLands.GetSize(); i++)
  {
    CPosTextureLand * pLand = (CPosTextureLand *) m_TextureLands[i];

    if (!pLand->CreateLocalTextures())
      return FALSE;
  }
  return TRUE;
}

void CMgrTextures::SyncLocalTexturesIntoDoc()
{
  for(int i = 0; i < m_TextureLands.GetSize(); i++)
  {
    CPosTextureLand * pLand = (CPosTextureLand *) m_TextureLands[i];
    if (pLand)
      pLand->SyncLocalTexturesIntoDoc();
  }  
}

bool CMgrTextures::ExistTextureName(const CString& name, const CPosTextureZone * doNotSearch, const CPosTextureTemplate * ignore) const
{
  for(int i = 0; i < m_TextureLands.GetSize(); i++)
  {
    CPosTextureLand * pLand = (CPosTextureLand *) m_TextureLands[i];
    if (pLand && pLand->ExistTextureName(name, doNotSearch, ignore))
      return true;
  }
  return false;
}

///////////////////////////////////////////////////////////////
// Class for storage of global texture bank
void TextureBank::DoSerialize(CArchive& ar, int nVer, CPosEdBaseDoc * pDoc)
{
  if (nVer < 35)
    return;

  CleanUnUsed();

  if (ar.IsStoring())
  {  
    int size = base::Size();
    MntSerializeInt(ar, size);
    for(int i = 0; i < size; i++)
      Get(i)->DoSerialize(ar, nVer);
  }
  else
  {
    int size;
    MntSerializeInt(ar, size);
    Reserve(size,size);
    Resize(0);
    for(int i = 0; i < size; i++)
    {
      Ref<CPosTextureTemplate> text = new CPosTextureTemplate(pDoc);

      text->DoSerialize(ar,nVer);
      if (text->m_nTextureID > _lastUsedID)
        _lastUsedID = text->m_nTextureID;

      Add(text);
    }
  }
}

void TextureBank::CleanUnUsed()
{ 
  for(int i = 0; i < Size(); )
  {
    if (Get(i)->RefCounter() == 1)    
      Delete(i);    
    else
      i++;
  }
}


