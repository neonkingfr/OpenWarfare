/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// �ablona s�t� - CPosNetTemplate

IMPLEMENT_DYNAMIC( CPosNetTemplate, CObject )

CPosNetTemplate::CPosNetTemplate(CPosEdBaseDoc* pOwner)
{
	m_pOwner = pOwner;
	MakeDefaultValues();
}

CPosNetTemplate::~CPosNetTemplate()
{	
	DestroyValues(); 
}

void CPosNetTemplate::MakeDefaultValues()
{
	m_sNetName.Empty();
	m_clrKey = m_clrNor = DEFAULT_COLOR;
	m_bFillEntire	= TRUE;
	// um�st�n� d�l� vzhledem k povrchu
	//m_nMinHeight	= 0.;
	//m_nStdHeight	= 0.5;
	//m_nMaxHeight	= 3.;
	//m_bAutoHeight	= FALSE;
	// maxim�ln� sklon
	m_nMaxSlope		= 25.;		
	// maxim�ln� �hle mezi d�ly
	m_nPartSlope	= 5.;		
}

void CPosNetTemplate::DestroyValues()
{
	DestroyArrayObjects(&m_NetSTRA);
	DestroyArrayObjects(&m_NetBEND);
	DestroyArrayObjects(&m_NetSPEC);
	DestroyArrayObjects(&m_NetTERM);
}

void CPosNetTemplate::CopyFrom(const CPosNetTemplate* pSrc)
{
	DestroyValues();

	m_pOwner        = pSrc->m_pOwner;
	// kopie dat
	m_sNetName	    = pSrc->m_sNetName;
	m_clrKey		= pSrc->m_clrKey;
	m_clrNor		= pSrc->m_clrNor;
	m_bFillEntire	= pSrc->m_bFillEntire;
	// um�st�n� d�l� vzhledem k povrchu
	//m_nMinHeight	= pSrc->m_nMinHeight;
	//m_nStdHeight	= pSrc->m_nStdHeight;
	//m_nMaxHeight	= pSrc->m_nMaxHeight;
	//m_bAutoHeight	= pSrc->m_bAutoHeight;
	// maxim�ln� sklon & maxim�ln� �hle mezi d�ly
	m_nMaxSlope		= pSrc->m_nMaxSlope;	
	m_nPartSlope	= pSrc->m_nPartSlope;		

	// kopie objekt�
	int i;
	for (i = 0; i < pSrc->m_NetSTRA.GetSize(); ++i)
	{
		CNetSTRA* pSrcObj = (CNetSTRA*)(pSrc->m_NetSTRA[i]);
		if (pSrcObj) m_NetSTRA.Add(pSrcObj->CreateCopyObject());
	}
	for (i = 0; i < pSrc->m_NetBEND.GetSize(); ++i)
	{
		CNetBEND* pSrcObj = (CNetBEND*)(pSrc->m_NetBEND[i]);
		if (pSrcObj) m_NetBEND.Add(pSrcObj->CreateCopyObject());
	}
	for (i = 0; i < pSrc->m_NetSPEC.GetSize(); ++i)
	{
		CNetSPEC* pSrcObj = (CNetSPEC*)(pSrc->m_NetSPEC[i]);
		if (pSrcObj) m_NetSPEC.Add(pSrcObj->CreateCopyObject());
	}
	for (i = 0; i < pSrc->m_NetTERM.GetSize(); ++i)
	{
		CNetTERM* pSrcObj = (CNetTERM*)(pSrc->m_NetTERM[i]);
		if (pSrcObj) m_NetTERM.Add(pSrcObj->CreateCopyObject());
	}
}

// serializace
void CPosNetTemplate::DoSerialize(CArchive& ar, DWORD nVer)
{
	if (!ar.IsStoring()) DestroyValues();	// vypr�zdn�n� 

	MntSerializeString(ar, m_sNetName);
	MntSerializeColor(ar, m_clrKey);
	MntSerializeColor(ar, m_clrNor);
	MntSerializeBOOL(ar, m_bFillEntire);
	// um�st�n� d�l� vzhledem k povrchu
	if (nVer >= 22)
	{
		if (ar.IsLoading() && nVer < 42)
		{
			double nMinHeight;
			MntSerializeDouble(ar, nMinHeight);
			double nMaxHeight;
			MntSerializeDouble(ar, nMaxHeight);
			double nStdHeight;
			MntSerializeDouble(ar, nStdHeight);
			if (nVer >= 26)
			{
				BOOL bAutoHeight;
				MntSerializeBOOL(ar, bAutoHeight);
			}
		}
		// maxim�ln� sklon & maxim�ln� �hel mezi d�ly
		if (nVer >= 23)
		{
			MntSerializeDouble(ar, m_nMaxSlope);
			MntSerializeDouble(ar, m_nPartSlope);
		}
	}

	// serializace objekt�
	int nCount, i;

	nCount = m_NetSTRA.GetSize();
	MntSerializeInt(ar, nCount);
	for (i = 0; i < nCount; ++i)
	{
		if (ar.IsStoring())
		{
			CNetComponent* pObj = (CNetComponent*)(m_NetSTRA[i]);
			ASSERT(pObj != NULL);
			pObj->DoSerialize(ar, nVer);
		} 
		else
		{
			CNetSTRA* pObj = new CNetSTRA(m_pOwner);
			m_NetSTRA.Add(pObj);
			pObj->DoSerialize(ar, nVer);
		}
	}

	nCount = m_NetBEND.GetSize();
	MntSerializeInt(ar, nCount);
	for (i = 0; i < nCount; ++i)
	{
		if (ar.IsStoring())
		{
			CNetComponent* pObj = (CNetComponent*)(m_NetBEND[i]);
			ASSERT(pObj != NULL);
			pObj->DoSerialize(ar, nVer);
		} 
		else
		{
			CNetBEND* pObj = new CNetBEND(m_pOwner);
			m_NetBEND.Add(pObj);
			pObj->DoSerialize(ar, nVer);
		}
	}

	nCount = m_NetSPEC.GetSize();
	MntSerializeInt(ar, nCount);
	for (i = 0; i < nCount; ++i)
	{
		if (ar.IsStoring())
		{
			CNetComponent* pObj = (CNetComponent*)(m_NetSPEC[i]);
			ASSERT(pObj != NULL);
			pObj->DoSerialize(ar, nVer);
		} 
		else
		{
			CNetSPEC* pObj = new CNetSPEC(m_pOwner);
			m_NetSPEC.Add(pObj);
			pObj->DoSerialize(ar, nVer);
		}
	}

	nCount = m_NetTERM.GetSize();
	MntSerializeInt(ar, nCount);
	for (i = 0; i < nCount; ++i)
	{
		if (ar.IsStoring())
		{
			CNetComponent* pObj = (CNetComponent*)(m_NetTERM[i]);
			ASSERT(pObj != NULL);
			pObj->DoSerialize(ar, nVer);
		} 
		else
		{
			CNetTERM* pObj = new CNetTERM(m_pOwner);
			m_NetTERM.Add(pObj);
			pObj->DoSerialize(ar, nVer);
		}
	}
}

// vykreslen� v ListBox
void CPosNetTemplate::DrawLBItem(CDC* pDC, CRect& rPos) const
{
	CRect rText = rPos; rText.left += 2;
	// jm�no
	::DrawTextA(pDC->m_hDC, m_sNetName, -1, &rText, DT_LEFT | DT_NOPREFIX | DT_SINGLELINE | DT_VCENTER | DT_TABSTOP);
}

// vykreslen� v ListBox
void CPosNetTemplate::DrawLBImage(CDC* pDC, CRect& rPos) const
{
	CRect rPom = rPos; rPom.right = (rPos.left + rPos.right) / 2;
	BOOL  bFill;
	// key
    COLORREF bkColor = pDC->GetBkColor();
	COLORREF cColor = GetObjectKeyColor(bFill, &(GetPosEdEnvironment()->m_cfgCurrent));
	pDC->FillSolidRect(rPom, cColor);
	// nor
	rPom.left = rPom.right; rPom.right = rPos.right;
	cColor = GetObjectNorColor(bFill, &(GetPosEdEnvironment()->m_cfgCurrent));
	pDC->FillSolidRect(rPom, cColor);
    pDC->SetBkColor(bkColor);

	//PaintSpecBorder(pDC->m_hDC,rPos,PBS_EDDOWN);
}

COLORREF CPosNetTemplate::GetObjectKeyColor(BOOL& bFill, const CPosEdCfg* pCfg) const
{
	bFill = m_bFillEntire;

	if (m_clrKey != DEFAULT_COLOR) return m_clrKey;
	ASSERT(pCfg != NULL);
	return pCfg->m_cDefColors[CPosEdCfg::defcolNetKey];
}

COLORREF CPosNetTemplate::GetObjectNorColor(BOOL& bFill, const CPosEdCfg* pCfg) const
{
	bFill = m_bFillEntire;

	if (m_clrNor != DEFAULT_COLOR) return m_clrNor;
	ASSERT(pCfg != NULL);
	return pCfg->m_cDefColors[CPosEdCfg::defcolNetNor];
}

void CPosNetTemplate::CreateLocalObjectTemplates()
{
	// rovinky
	for (int i = 0; i < m_NetSTRA.GetSize(); ++i)
	{
		CNetComponent* net = (CNetComponent*) m_NetSTRA[i];
		if (net) net->CreateLocalObjectTemplates();    
	}

	// zat��ky
	for (int i = 0; i < m_NetBEND.GetSize(); ++i)
	{
		CNetComponent* net = (CNetComponent*) m_NetBEND[i];
		if (net) net->CreateLocalObjectTemplates();    
	}

	// spec. d�ly
	for (int i = 0; i < m_NetSPEC.GetSize(); ++i)
	{
		CNetComponent* net = (CNetComponent*) m_NetSPEC[i];
		if (net) net->CreateLocalObjectTemplates();    
	}

	// koncov� d�ly  
	for (int i = 0; i < m_NetTERM.GetSize(); ++i)
	{
		CNetComponent* net = (CNetComponent*) m_NetTERM[i];
		if (net) net->CreateLocalObjectTemplates();    
	}
}

void CPosNetTemplate::SyncLocalObjectTemplatesIntoDoc()
{
	// rovinky
	for(int i = 0; i < m_NetSTRA.GetSize(); ++i)
	{
		CNetComponent* net = (CNetComponent*) m_NetSTRA[i];
		if (net) net->SyncLocalObjectTemplatesIntoDoc();    
	}

	// zat��ky
	for (int i = 0; i < m_NetBEND.GetSize(); ++i)
	{
		CNetComponent* net = (CNetComponent*) m_NetBEND[i];
		if (net) net->SyncLocalObjectTemplatesIntoDoc();    
	}

	// spec. d�ly
	for (int i = 0; i < m_NetSPEC.GetSize(); ++i)
	{
		CNetComponent* net = (CNetComponent*) m_NetSPEC[i];
		if (net) net->SyncLocalObjectTemplatesIntoDoc();    
	}

	// koncov� d�ly  
	for (int i = 0; i < m_NetTERM.GetSize(); ++i)
	{
		CNetComponent* net = (CNetComponent*) m_NetTERM[i];
		if (net) net->SyncLocalObjectTemplatesIntoDoc();    
	}
}

bool CPosNetTemplate::IsUsed() const
{
	// rovinky
	for (int i = 0; i < m_NetSTRA.GetSize(); ++i)
	{
		CNetComponent* net = (CNetComponent*) m_NetSTRA[i];
		if (net && net->IsUsed()) return true;
	}

	// zat��ky
	for (int i = 0; i < m_NetBEND.GetSize(); ++i)
	{
		CNetComponent* net = (CNetComponent*) m_NetBEND[i];
		if (net && net->IsUsed()) return true;
	}

	// spec. d�ly
	for (int i = 0; i < m_NetSPEC.GetSize(); ++i)
	{
		CNetComponent* net = (CNetComponent*) m_NetSPEC[i];
		if (net && net->IsUsed()) return true;    
	}

	 // koncov� d�ly  
	for (int i = 0; i < m_NetTERM.GetSize(); ++i)
	{
		CNetComponent* net = (CNetComponent*) m_NetTERM[i];
		if (net && net->IsUsed()) return true;    
	}

	return false;
}

void CPosNetTemplate::GetObjectTemplates(AutoArray<int>& templ)
{
	// rovinky
	for (int i = 0; i < m_NetSTRA.GetSize(); ++i)
	{
		CNetComponent* net = (CNetComponent*) m_NetSTRA[i];
		if (net && net->GetObjectTemplate()) templ.Add(net->GetObjectTemplate()->m_ID);    
	}

	// zat��ky
	for (int i = 0; i < m_NetBEND.GetSize(); ++i)
	{
		CNetComponent* net = (CNetComponent*) m_NetBEND[i];
		if (net && net->GetObjectTemplate()) templ.Add(net->GetObjectTemplate()->m_ID);
	}

	// spec. d�ly
	for (int i = 0; i < m_NetSPEC.GetSize(); ++i)
	{
		CNetComponent* net = (CNetComponent*) m_NetSPEC[i];
		if (net && net->GetObjectTemplate()) templ.Add(net->GetObjectTemplate()->m_ID);    
	}

	// koncov� d�ly  
	for (int i = 0; i < m_NetTERM.GetSize(); ++i)
	{
		CNetComponent* net = (CNetComponent*) m_NetTERM[i];
		if (net && net->GetObjectTemplate()) templ.Add(net->GetObjectTemplate()->m_ID);   
	}
}

CNetTERM* CPosNetTemplate::GetTermTemplate(CString name)
{
	for (int i = 0; i < m_NetTERM.GetSize(); ++i)
	{
		CNetTERM* net = (CNetTERM*) m_NetTERM[i];

		// new code
		// *********************************************
		int slashPos = net->m_sCompName.ReverseFind('\\');
		CString compName1;
		if (slashPos == -1)
		{
			compName1 = net->m_sCompName;
		}
		else
		{
			compName1 = net->m_sCompName.Right(net->m_sCompName.GetLength() - (slashPos + 1));
		}
		slashPos = name.ReverseFind('\\');
		CString compName2;
		if (slashPos == -1)
		{
			compName2 = name;
		}
		else
		{
			compName2 = name.Right(name.GetLength() - (slashPos + 1));
		}
		if (net && compName1 == compName2) return net;
		// *********************************************
		
		// old code
		// *********************************************
		// if (net && net->m_sCompName == name) return net;
		// *********************************************
	}
	return NULL;
}

CNetSTRA* CPosNetTemplate::GetStraTemplate(CString name)
{
	for (int i = 0; i < m_NetSTRA.GetSize(); ++i)
	{
		CNetSTRA* net = (CNetSTRA*) m_NetSTRA[i];

		// new code
		// *********************************************
		int slashPos = net->m_sCompName.ReverseFind('\\');
		CString compName1;
		if (slashPos == -1)
		{
			compName1 = net->m_sCompName;
		}
		else
		{
			compName1 = net->m_sCompName.Right(net->m_sCompName.GetLength() - (slashPos + 1));
		}
		slashPos = name.ReverseFind('\\');
		CString compName2;
		if (slashPos == -1)
		{
			compName2 = name;
		}
		else
		{
			compName2 = name.Right(name.GetLength() - (slashPos + 1));
		}
		if (net && compName1 == compName2) return net;
		// *********************************************

		// old code
		// *********************************************
		// if (net && net->m_sCompName == name) return net;
		// *********************************************
	}
	return NULL;
}

CNetBEND* CPosNetTemplate::GetBendTemplate(CString name)
{
	for (int i = 0; i < m_NetBEND.GetSize(); ++i)
	{
		CNetBEND* net = (CNetBEND*) m_NetBEND[i];

		// new code
		// *********************************************
		int slashPos = net->m_sCompName.ReverseFind('\\');
		CString compName1;
		if (slashPos == -1)
		{
			compName1 = net->m_sCompName;
		}
		else
		{
			compName1 = net->m_sCompName.Right(net->m_sCompName.GetLength() - (slashPos + 1));
		}
		slashPos = name.ReverseFind('\\');
		CString compName2;
		if (slashPos == -1)
		{
			compName2 = name;
		}
		else
		{
			compName2 = name.Right(name.GetLength() - (slashPos + 1));
		}
		if (net && compName1 == compName2) return net;
		// *********************************************

		// old code
		// *********************************************
		// if (net && net->m_sCompName == name) return net;
		// *********************************************
	}
	return NULL;
}

CNetSPEC* CPosNetTemplate::GetSpecTemplate(CString name)
{
	for (int i = 0; i < m_NetSPEC.GetSize(); ++i)
	{
		CNetSPEC* net = (CNetSPEC*) m_NetSPEC[i];

		// new code
		// *********************************************
		int slashPos = net->m_sCompName.ReverseFind('\\');
		CString compName1;
		if (slashPos == -1)
		{
			compName1 = net->m_sCompName;
		}
		else
		{
			compName1 = net->m_sCompName.Right(net->m_sCompName.GetLength() - (slashPos + 1));
		}
		slashPos = name.ReverseFind('\\');
		CString compName2;
		if (slashPos == -1)
		{
			compName2 = name;
		}
		else
		{
			compName2 = name.Right(name.GetLength() - (slashPos + 1));
		}
		if (net && compName1 == compName2) return net;
		// *********************************************

		// old code
		// *********************************************
		// if (net && net->m_sCompName == name) return net;
		// *********************************************
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////
// �ablona k�i�ovatky - CPosCrossTemplate

IMPLEMENT_DYNAMIC( CPosCrossTemplate, CObject )

CPosCrossTemplate::CPosCrossTemplate(CPosEdBaseDoc* pOwner)
{
	m_pOwner     = pOwner;
	m_pNetObj    = NULL;
	m_nCrossType = 0;
	MakeDefaultValues();

}

CPosCrossTemplate::~CPosCrossTemplate()
{	
	DestroyValues(); 
}

void CPosCrossTemplate::MakeDefaultValues()
{
	m_sCrossName.Empty();
	m_clrNor        = DEFAULT_COLOR;
	m_bFillEntire	= TRUE;
	// um�st�n� d�lu vzhledem k povrchu
	//m_nMinHeight	= 0.;
	//m_nStdHeight	= 0.5;
	//m_nMaxHeight	= 3.;
	//m_bAutoHeight	= FALSE;

	// ru��m napojen� na s�t�
	for (int i = 0; i < netCROSS_Parts; ++i)
	{
		m_sPartsNets[i].Empty();
	}
}

void CPosCrossTemplate::DestroyValues()
{
	m_pNetObj = NULL;
	MakeDefaultValues();
}

void CPosCrossTemplate::CopyFrom(const CPosCrossTemplate* pSrc)
{
	DestroyValues();

	m_pOwner        = pSrc->m_pOwner;

	// kopie dat
	m_sCrossName	= pSrc->m_sCrossName;
	m_nCrossType	= pSrc->m_nCrossType;

	m_clrNor		= pSrc->m_clrNor;
	m_bFillEntire	= pSrc->m_bFillEntire;
	// um�st�n� d�lu vzhledem k povrchu
	//m_nMinHeight	= pSrc->m_nMinHeight;
	//m_nStdHeight	= pSrc->m_nStdHeight;
	//m_nMaxHeight	= pSrc->m_nMaxHeight;
	//m_bAutoHeight	= pSrc->m_bAutoHeight;

	// kopie objektu
	ASSERT(m_pNetObj == NULL);
	if (pSrc->m_pNetObj != NULL)
	{
		m_pNetObj = new CPosObjectTemplate(m_pOwner);
		if (m_pNetObj) m_pNetObj->CopyFrom(pSrc->m_pNetObj);
	}
	// napojen� na s�t�
	for (int i = 0; i < netCROSS_Parts; ++i)
	{
		m_sPartsNets[i] = pSrc->m_sPartsNets[i];
	}
}

// serializace
void CPosCrossTemplate::DoSerialize(CArchive& ar, DWORD nVer)
{
	if (!ar.IsStoring()) DestroyValues();	// vypr�zdn�n� 

	MntSerializeString(ar, m_sCrossName);
	MntSerializeWord(ar, m_nCrossType);

	MntSerializeColor(ar, m_clrNor);
	MntSerializeBOOL(ar, m_bFillEntire);

	// um�st�n� d�lu vzhledem k povrchu
	if (nVer >= 22)
	{
		if (ar.IsLoading() && nVer < 42)
		{    
			double nMinHeight;
			MntSerializeDouble(ar, nMinHeight);
			double nMaxHeight;
			MntSerializeDouble(ar, nMaxHeight);
			double nStdHeight;
			MntSerializeDouble(ar, nStdHeight);

			if (nVer >= 26)
			{
				BOOL bAutoHeight;
				MntSerializeBOOL(ar, bAutoHeight);
			}
		}
	}

	if (ar.IsLoading())
	{
		if (nVer <= 36)
		{
			// serializace objektu
			BOOL bExist;      
			MntSerializeBOOL(ar, bExist);
			if (bExist)
			{        
				m_pNetObj = new CPosObjectTemplate(m_pOwner);
   
				m_pNetObj->DoSerialize(ar, nVer);
				m_pNetObj->m_nObjType = CPosObjectTemplate::objTpNet;
				m_pOwner->AddObjectTemplateSetID(m_pNetObj);        
			}
		}
		else
		{
			int templID;
			MntSerializeInt(ar, templID);
			m_pNetObj = m_pOwner->GetObjectTemplate(templID);
		}
	}
	else
	{
		// saving
		if (m_pNetObj)
		{
			MntSerializeInt(ar, m_pNetObj->m_ID);
		}
		else
		{
			int i = -1;
			MntSerializeInt(ar, i);
		}
	}

	// napojen� na s�t�
	for (int i = 0; i < netCROSS_Parts; ++i)
	{
		MntSerializeString(ar, m_sPartsNets[i]);
	}
}

void CPosCrossTemplate::GetTypeString(CString& sText) const
{
	switch(m_nCrossType)
	{
	case netCROSS_I: sText = _T("I"); break;
	case netCROSS_L: sText = _T("L"); break;
	case netCROSS_P: sText = _T("P"); break;
	case netCROSS_K: sText = _T("K"); break;
	};
}

// propojen� s objektem poseidon
BOOL CPosCrossTemplate::CreateFromObjectTemplate(CPosObjectTemplate* pTempl)
{
	if (!pTempl->m_bIsNetObject) return FALSE;	// nen� to objekt s�t�

	// propoj�m s objektem
	ASSERT(m_pNetObj == NULL);
	m_pNetObj = pTempl;

	// jm�no podle objektu
	m_pNetObj->GetNameFromFile(m_sCrossName);

	// m� v�ech body LB, PB, LE, PE ? - mus� m�t ka�d� k�i�ovatka
	if (!pTempl->ExistNetPoint(CPosObjectTemplate::ptNetLB)) return FALSE;
	if (!pTempl->ExistNetPoint(CPosObjectTemplate::ptNetPB)) return FALSE;
	if (!pTempl->ExistNetPoint(CPosObjectTemplate::ptNetLE)) return FALSE;
	if (!pTempl->ExistNetPoint(CPosObjectTemplate::ptNetPE)) return FALSE;

	// standardn� typ I
	m_nCrossType = netCROSS_I;

	if ((pTempl->ExistNetPoint(CPosObjectTemplate::ptNetLH))&&
		(pTempl->ExistNetPoint(CPosObjectTemplate::ptNetLD)))
	{
		if ((pTempl->ExistNetPoint(CPosObjectTemplate::ptNetPH))&&
			(pTempl->ExistNetPoint(CPosObjectTemplate::ptNetPD)))
		{	// netCROSS_K
			m_nCrossType = netCROSS_K;
		} 
		else
		{	// netCROSS_L
			m_nCrossType = netCROSS_L;
		}
	} 
	else if ((pTempl->ExistNetPoint(CPosObjectTemplate::ptNetPH))&&
		     (pTempl->ExistNetPoint(CPosObjectTemplate::ptNetPD)))
	{	// netCROSS_P
		m_nCrossType = netCROSS_P;
	}

	// v�e je OK
	return TRUE;
}

// vykreslen� v ListBox
COLORREF CPosCrossTemplate::GetLBItemColor() const
{	
	BOOL bFill;
	return GetObjectNorColor(bFill, &(GetPosEdEnvironment()->m_cfgCurrent)); 
}

void CPosCrossTemplate::DrawLBItem(CDC* pDC, CRect& rPos) const
{
	CRect rText = rPos; rText.left += 2;
	// typ
	CString sText, sType;
	sText  = _T("[");
	GetTypeString(sType);
	sText += sType;
	sText += _T("] ");
	// jm�no
	sText += m_sCrossName;
	::DrawTextA(pDC->m_hDC, sText, -1, &rText, DT_LEFT | DT_NOPREFIX | DT_SINGLELINE | DT_VCENTER | DT_TABSTOP);
}

COLORREF CPosCrossTemplate::GetObjectNorColor(BOOL& bFill, const CPosEdCfg* pCfg) const
{
	bFill = m_bFillEntire;

	if (m_clrNor != DEFAULT_COLOR) return m_clrNor;
	ASSERT(pCfg != NULL);
	return pCfg->m_cDefColors[CPosEdCfg::defcolNetCrs];
}

// �seky (napojen�) na s�t�
BOOL CPosCrossTemplate::SupportCrossPart(WORD nPart, WORD nType)	// m� smysl dan� �sek ?
{
	switch(nType)
	{
	case netCROSS_I:
		if ((nPart == netCROSS_A) || (nPart == netCROSS_B)) return TRUE;
		break;
	case netCROSS_L:
		if ((nPart == netCROSS_A) || (nPart == netCROSS_B) || (nPart == netCROSS_C)) return TRUE;
		break;
	case netCROSS_P:
		if ((nPart == netCROSS_A) || (nPart == netCROSS_B) || (nPart == netCROSS_D)) return TRUE;
		break;
	case netCROSS_K:
		if ((nPart == netCROSS_A) || (nPart == netCROSS_B) || (nPart == netCROSS_C) || (nPart == netCROSS_D)) return TRUE;
		break;
	};
	return FALSE;
}

BOOL CPosCrossTemplate::SupportCrossPart(WORD nPart) const
{	
	return SupportCrossPart(nPart, m_nCrossType); 
}

void CPosCrossTemplate::GetCrossPartName(CString& sName, WORD nPart) const
{
	TCHAR buff[2]; lstrcpy(buff,_T("A"));
	buff[0] += nPart;
	sName = buff;
}

CPosNetTemplate* CPosCrossTemplate::GetPartNetTemplate(WORD nPart, CMgrNets* pMgr)
{
	if (!SupportCrossPart(nPart)) return NULL;
	// v�b�r �ablony
	CPosNetTemplate* pTempl = pMgr->GetNetTemplate(m_sPartsNets[nPart]);
	return pTempl;
}

void CPosCrossTemplate::RenameNetTemplate(LPCTSTR pOld, LPCTSTR pNew)
{
	for (int i = 0; i < netCROSS_Parts; ++i)
	{
		if (lstrcmp(m_sPartsNets[i], pOld) == 0) m_sPartsNets[i] = pNew;
	}
}

void CPosCrossTemplate::CreateLocalObjectTemplates()
{
	if (m_pNetObj) m_pNetObj = new CPosObjectTemplate(*m_pNetObj);    
}

void CPosCrossTemplate::SyncLocalObjectTemplatesIntoDoc()
{
	if (m_pNetObj)
	{
		Ref<CPosObjectTemplate> templ = m_pOwner->GetObjectTemplate(m_pNetObj->m_ID);
		if (templ.NotNull())
		{
			templ->CopyFrom(m_pNetObj);
			m_pNetObj = templ;
		}
		else
		{
			m_pOwner->AddObjectTemplateSetID(m_pNetObj);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMgrNets - mana�er s�t�

CMgrNets::CMgrNets()
{
}

CMgrNets::~CMgrNets()
{	
	Destroy();
}

void CMgrNets::Destroy()
{	
	DestroyArrayObjects(&m_NetTemplates); 
	DestroyArrayObjects(&m_CrossTemplates); 
}

void CMgrNets::CopyFrom(CMgrNets* pSrc)
{
	Destroy();
	for (int i = 0; i < pSrc->m_NetTemplates.GetSize(); ++i)
	{
		CPosNetTemplate* pTempl = (CPosNetTemplate*)(pSrc->m_NetTemplates[i]);
		if (pTempl)
		{
			ASSERT_KINDOF(CPosNetTemplate, pTempl);
			CPosNetTemplate* pCopy = new CPosNetTemplate(m_pOwner);

			// kop�ruji data
			pCopy->CopyFrom(pTempl);
			// vlo��m �ablonu
			m_NetTemplates.Add(pCopy);
		}
	}
	for (int i = 0; i < pSrc->m_CrossTemplates.GetSize(); ++i)
	{
		CPosCrossTemplate* pTempl = (CPosCrossTemplate*)(pSrc->m_CrossTemplates[i]);
		if (pTempl)
		{
			ASSERT_KINDOF(CPosCrossTemplate, pTempl);
			CPosCrossTemplate* pCopy = new CPosCrossTemplate(m_pOwner);

			// kop�ruji data
			pCopy->CopyFrom(pTempl);
			// vlo��m �ablonu
			m_CrossTemplates.Add(pCopy);
		}
	}
}

// serializace
void CMgrNets::DoSerialize(CArchive& ar, DWORD nVer)
{
	BOOL bEmpty = ((m_NetTemplates.GetSize() == 0) && (m_CrossTemplates.GetSize() == 0));
	MntSerializeBOOL(ar, bEmpty);
	if (bEmpty) return;	// tento manazer je pr�zdn�

	if (!ar.IsStoring()) Destroy();	// vypr�zdn�n� 

	// serializace s�t�
	int nCount = m_NetTemplates.GetSize();
	MntSerializeInt(ar, nCount);
	for (int i = 0; i < nCount; ++i)
	{
		if (ar.IsStoring())
		{
			CPosNetTemplate* pTempl = (CPosNetTemplate*)(m_NetTemplates[i]);
			ASSERT(pTempl != NULL);
			pTempl->DoSerialize(ar, nVer);
		} 
		else
		{
			CPosNetTemplate* pTempl = new CPosNetTemplate(m_pOwner);
			m_NetTemplates.Add(pTempl);
			pTempl->DoSerialize(ar, nVer);
		}
	}
	// serializace k�i�ovatek
	nCount = m_CrossTemplates.GetSize();
	MntSerializeInt(ar, nCount);
	for (int i = 0; i < nCount; ++i)
	{
		if (ar.IsStoring())
		{
			CPosCrossTemplate* pTempl = (CPosCrossTemplate*)(m_CrossTemplates[i]);
			ASSERT(pTempl != NULL);
			pTempl->DoSerialize(ar, nVer);
		} 
		else
		{
			CPosCrossTemplate* pTempl = new CPosCrossTemplate(m_pOwner);
			m_CrossTemplates.Add(pTempl);
			pTempl->DoSerialize(ar, nVer);
		}
	}
}

// �ablona podle jm�na
CPosNetTemplate* CMgrNets::GetNetTemplate(LPCTSTR pName) const
{
	for (int i = 0; i < m_NetTemplates.GetSize(); ++i)
	{
		CPosNetTemplate* pTempl = (CPosNetTemplate*)(m_NetTemplates[i]);
		if (pTempl)
		{
			ASSERT_KINDOF(CPosNetTemplate, pTempl);
			if (lstrcmpi(pTempl->m_sNetName, pName) == 0) return pTempl;
		}
	}
	return NULL;
}

CPosCrossTemplate* CMgrNets::GetCrossTemplate(LPCTSTR pName) const
{
	for (int i = 0; i < m_CrossTemplates.GetSize(); ++i)
	{
		CPosCrossTemplate* pTempl = (CPosCrossTemplate*)(m_CrossTemplates[i]);
		if (pTempl)
		{
			ASSERT_KINDOF(CPosCrossTemplate, pTempl);
			if (lstrcmpi(pTempl->m_sCrossName, pName) == 0) return pTempl;
		}
	}
	return NULL;
}

void CMgrNets::RenameNetTemplate(LPCTSTR pOld, LPCTSTR pNew)
{
	for (int i = 0; i < m_CrossTemplates.GetSize(); ++i)
	{
		CPosCrossTemplate* pTempl = (CPosCrossTemplate*)(m_CrossTemplates[i]);
		if (pTempl)
		{
			ASSERT_KINDOF(CPosCrossTemplate, pTempl);
			pTempl->RenameNetTemplate(pOld, pNew);
		}
	}
}

void CMgrNets::CreateLocalObjectTemplates()
{
	for (int i = 0; i < m_CrossTemplates.GetSize(); ++i)
	{
		CPosCrossTemplate* pTempl = (CPosCrossTemplate*)(m_CrossTemplates[i]);
		if (pTempl)
		{
			ASSERT_KINDOF(CPosCrossTemplate, pTempl);
			pTempl->CreateLocalObjectTemplates();
		}
	}

	for (int i = 0; i < m_NetTemplates.GetSize(); ++i)
	{
		CPosNetTemplate* pTempl = (CPosNetTemplate*)(m_NetTemplates[i]);
		if (pTempl)
		{
			ASSERT_KINDOF(CPosNetTemplate, pTempl);
			pTempl->CreateLocalObjectTemplates();
		}
	}
}

void CMgrNets::SyncLocalObjectTemplatesIntoDoc()
{
	for (int i = 0; i < m_CrossTemplates.GetSize(); ++i)
	{
		CPosCrossTemplate* pTempl = (CPosCrossTemplate*)(m_CrossTemplates[i]);
		if (pTempl)
		{
			ASSERT_KINDOF(CPosCrossTemplate, pTempl);
			pTempl->SyncLocalObjectTemplatesIntoDoc();
		}
	}
	for (int i = 0; i < m_NetTemplates.GetSize(); ++i)
	{
		CPosNetTemplate* pTempl = (CPosNetTemplate*)(m_NetTemplates[i]);
		if (pTempl)
		{
			ASSERT_KINDOF(CPosNetTemplate, pTempl);
			pTempl->SyncLocalObjectTemplatesIntoDoc();
		}
	}
}