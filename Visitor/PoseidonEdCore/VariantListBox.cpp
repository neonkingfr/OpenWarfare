/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CVariantListBox - LB pro zobrazen� objekt�

CVariantListBox::CVariantListBox()
{
	m_nVariant	  = -1;
	
	Init(MNTLST_COLOR);
};

int CVariantListBox::GetItemImageWidth(LPDRAWITEMSTRUCT)
{
    if (m_nVariant == itmVariantNets)
	{
        return 30;
    }
    return 0;
}

void CVariantListBox::OnDrawItemImage(CRect& rPos, LPDRAWITEMSTRUCT lpDIS)
{
	if (m_nVariant == itmVariantNets)
	{
		CPosNetTemplate* pNet = (CPosNetTemplate*)(lpDIS->itemData);
		if (pNet != NULL)
		{
			ASSERT_KINDOF(CPosNetTemplate, pNet);
			CDC dc;
			dc.Attach(lpDIS->hDC);
			pNet->DrawLBImage(&dc, rPos);
			dc.Detach();
		}
	} //else
	//	CFMntListBox::OnDrawItemImage(rPos, lpDIS);
};

void CVariantListBox::OnDrawItemText(CRect& rPos, LPDRAWITEMSTRUCT lpDIS)
{
	switch(m_nVariant)
	{
	case itmVariantLand: // typ krajiny
		{
			CPosTextureLand* pLand = (CPosTextureLand*)(lpDIS->itemData);
			if (pLand != NULL)
			{
				ASSERT_KINDOF(CPosTextureLand, pLand);
				CDC dc;
				dc.Attach(lpDIS->hDC);
				pLand->DrawLBItem(&dc, rPos);
				dc.Detach();
			}
		} 
		break;
	case itmVariantZone: // p�smo krajiny
		{
			CPosTextureZone* pZone = (CPosTextureZone*)(lpDIS->itemData);
			if (pZone != NULL)
			{
				ASSERT_KINDOF(CPosTextureZone, pZone);
				CDC dc;
				dc.Attach(lpDIS->hDC);
				pZone->DrawLBItem(&dc, rPos);
				dc.Detach();
			}
		}
		break;
	case itmVariantTxtr: // textura
		{
			CPosTextureTemplate* pTxtr = (CPosTextureTemplate*)(lpDIS->itemData);
			if (pTxtr != NULL)
			{
				ASSERT_KINDOF(CPosTextureTemplate, pTxtr);
				CDC dc;
				dc.Attach(lpDIS->hDC);
				pTxtr->DrawLBItem(&dc, rPos);
				dc.Detach();
			} 
			else
			{	// n�hodn� v�b�r
				CString sText;
				GetText(lpDIS->itemID, sText);
				// jm�no
				CRect rText = rPos; rText.left += 2;
				::DrawTextA(lpDIS->hDC, sText, -1, &rText, DT_LEFT | DT_NOPREFIX | DT_SINGLELINE | DT_VCENTER | DT_TABSTOP);
			}
		} 
		break;
	case itmVariantWood: // les
		{
			CPosWoodTemplate* pWood = (CPosWoodTemplate*)(lpDIS->itemData);
			if (pWood != NULL)
			{
				ASSERT_KINDOF(CPosWoodTemplate, pWood);
				CDC dc;
				dc.Attach(lpDIS->hDC);
				pWood->DrawLBItem(&dc, rPos);
				dc.Detach();
			}
		} 
		break;
	case itmVariantNObj:
	case itmVariantPObj:
	case itmVariantNRObj:
		{
			CPosObjectTemplate* pObj = (CPosObjectTemplate*)(lpDIS->itemData);
			if (pObj != NULL)
			{
				ASSERT_KINDOF(CPosObjectTemplate, pObj);
				CDC dc;
				dc.Attach(lpDIS->hDC);
				pObj->DrawLBItem(&dc, rPos);
				dc.Detach();
			}
		} 
		break;
	case itmVariantNets:
		{
			CPosNetTemplate* pNet = (CPosNetTemplate*)(lpDIS->itemData);
			if (pNet != NULL)
			{
				ASSERT_KINDOF(CPosNetTemplate, pNet);
				CDC dc;
				dc.Attach(lpDIS->hDC);
				pNet->DrawLBItem(&dc, rPos);
				dc.Detach();
			}
		}
		break;
	case itmVariantCros:
		{
			CPosCrossTemplate* pNet = (CPosCrossTemplate*)(lpDIS->itemData);
			if (pNet != NULL)
			{
				ASSERT_KINDOF(CPosCrossTemplate, pNet);
				CDC dc;
				dc.Attach(lpDIS->hDC);
				pNet->DrawLBItem(&dc, rPos);
				dc.Detach();
			}
		} 
		break;
	}
};

BOOL CVariantListBox::GetItemColor(COLORREF& cColor, LPDRAWITEMSTRUCT lpDIS)
{
	cColor = RGB(0, 0, 0);
	switch (m_nVariant)
	{
	case itmVariantLand: // typ krajiny
		{
			CPosTextureLand* pLand = (CPosTextureLand*)(lpDIS->itemData);
			if (pLand != NULL)
			{
				ASSERT_KINDOF(CPosTextureLand, pLand);
				cColor = pLand->GetLBItemColor();
			}
		}
		break;
	case itmVariantZone: // p�smo krajiny
		{
			CPosTextureZone* pZone = (CPosTextureZone*)(lpDIS->itemData);
			if (pZone != NULL)
			{
				ASSERT_KINDOF(CPosTextureZone, pZone);
				cColor = pZone->GetLBItemColor();
			}
		} 
		break;
	case itmVariantTxtr: // textura
		{
			CPosTextureTemplate* pTxtr = (CPosTextureTemplate*)(lpDIS->itemData);
			if (pTxtr != NULL)
			{
				ASSERT_KINDOF(CPosTextureTemplate, pTxtr);
				cColor = pTxtr->GetLBItemColor();
			} 
			else
			{
				cColor = RGB(255, 255, 255);
			}
		} 
		break;
	case itmVariantWood: // p�smo krajiny
		{
			CPosWoodTemplate* pWood = (CPosWoodTemplate*)(lpDIS->itemData);
			if (pWood != NULL)
			{
				ASSERT_KINDOF(CPosWoodTemplate, pWood);
				cColor = pWood->GetLBItemColor();
			}
		} 
		break;
	case itmVariantNObj:
	case itmVariantPObj:
	case itmVariantNRObj:
		{
			CPosObjectTemplate* pObj = (CPosObjectTemplate*)(lpDIS->itemData);
			if (pObj != NULL)
			{
				ASSERT_KINDOF(CPosObjectTemplate, pObj);
				cColor = pObj->GetLBItemColor();
			}
		} 
		break;
	case itmVariantCros:
		{
			CPosCrossTemplate* pNet = (CPosCrossTemplate*)(lpDIS->itemData);
			if (pNet != NULL)
			{
				ASSERT_KINDOF(CPosCrossTemplate, pNet);
				cColor = pNet->GetLBItemColor();
			}
		}
		break;
	default:
		return FALSE;
	}
	return TRUE;
};

/////////////////////////////////////////////////////////////////////////////
// CVariantComboBox - CB pro zobrazen� objekt�

CVariantComboBox::CVariantComboBox()
{
		m_nVariant	  = -1;      
		Init(MNTLST_COLOR);
};

void CVariantComboBox::OnDrawItemText(CRect& rPos, LPDRAWITEMSTRUCT lpDIS)
{
	switch(m_nVariant)
	{
	case itmVariantLand: // typ krajiny
		{
			CPosTextureLand* pLand = (CPosTextureLand*)(lpDIS->itemData);
			if (pLand != NULL)
			{
				ASSERT_KINDOF(CPosTextureLand,pLand);
				CDC dc;
				dc.Attach(lpDIS->hDC);
				pLand->DrawLBItem(&dc,rPos);
				dc.Detach();
			}
		}; break;
	case itmVariantZone: // p�smo krajiny
		{
			CPosTextureZone* pZone = (CPosTextureZone*)(lpDIS->itemData);
			if (pZone != NULL)
			{
				ASSERT_KINDOF(CPosTextureZone,pZone);
				CDC dc;
				dc.Attach(lpDIS->hDC);
				pZone->DrawLBItem(&dc,rPos);
				dc.Detach();
			}
		}; break;
	case itmVariantTxtr: // textura
		{
			CPosTextureTemplate* pTxtr = (CPosTextureTemplate*)(lpDIS->itemData);
			if (pTxtr != NULL)
			{
				ASSERT_KINDOF(CPosTextureTemplate,pTxtr);
				CDC dc;
				dc.Attach(lpDIS->hDC);
				pTxtr->DrawLBItem(&dc,rPos);
				dc.Detach();
			}
		}; break;
	case itmVariantWood: // les
		{
			CPosWoodTemplate* pWood = (CPosWoodTemplate*)(lpDIS->itemData);
			if (pWood != NULL)
			{
				ASSERT_KINDOF(CPosWoodTemplate,pWood);
				CDC dc;
				dc.Attach(lpDIS->hDC);
				pWood->DrawLBItem(&dc,rPos);
				dc.Detach();
			}
		}; break;
	};
};

BOOL CVariantComboBox::GetItemColor(COLORREF& cColor, LPDRAWITEMSTRUCT lpDIS)
{
	cColor = RGB(0,0,0);
	switch(m_nVariant)
	{
	case itmVariantLand: // typ krajiny
		{
			CPosTextureLand* pLand = (CPosTextureLand*)(lpDIS->itemData);
			if (pLand != NULL)
			{
				ASSERT_KINDOF(CPosTextureLand,pLand);
				cColor = pLand->GetLBItemColor();
			}
		}; break;
	case itmVariantZone: // p�smo krajiny
		{
			CPosTextureZone* pZone = (CPosTextureZone*)(lpDIS->itemData);
			if (pZone != NULL)
			{
				ASSERT_KINDOF(CPosTextureZone,pZone);
				cColor = pZone->GetLBItemColor();
			}
		}; break;
	case itmVariantTxtr: // textura
		{
			CPosTextureTemplate* pTxtr = (CPosTextureTemplate*)(lpDIS->itemData);
			if (pTxtr != NULL)
			{
				ASSERT_KINDOF(CPosTextureTemplate,pTxtr);
				cColor = pTxtr->GetLBItemColor();
			}
		}; break;
	case itmVariantWood: // p�smo krajiny
		{
			CPosWoodTemplate* pWood = (CPosWoodTemplate*)(lpDIS->itemData);
			if (pWood != NULL)
			{
				ASSERT_KINDOF(CPosWoodTemplate,pWood);
				cColor = pWood->GetLBItemColor();
			}
		}; break;
    default:
        return FALSE;
	};
	return TRUE;
};




