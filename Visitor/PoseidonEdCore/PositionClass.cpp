/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"
#include <Es\Containers\array.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// ���ka vybran�ho tracku
#define DP_TRACK_FRAME_SZ	3


/////////////////////////////////////////////////////////////////////////////
// obal polohy objektu

void GetPointsHull(RECT& rHull,const POINT* Pos, int nPtCount)
{
	::SetRect(&rHull,Pos[0].x,Pos[0].y,Pos[0].x + 1,Pos[0].y + 1);
	for(int i = 1; i < nPtCount; i++)
	{
		if (Pos[i].x < rHull.left)	rHull.left	= Pos[i].x;
		if (Pos[i].x > rHull.right)	rHull.right	= Pos[i].x;
		if (Pos[i].y < rHull.top)	rHull.top	= Pos[i].y;
		if (Pos[i].y > rHull.bottom)rHull.bottom= Pos[i].y;
	}
};
void GetObjectHull(RECT& rHull,const OBJPOSITION& Pos)
{  
  if (Pos.Size() == 0)
  {
    ::SetRect(&rHull,0,0,0,0);
    return;
  }

	::SetRect(&rHull,Pos[0].x,Pos[0].y,Pos[0].x + 1,Pos[0].y + 1);
	for(int i = 1; i < Pos.Size(); i++)
	{
		if (Pos[i].x < rHull.left)	rHull.left	= Pos[i].x;
		if (Pos[i].x > rHull.right)	rHull.right	= Pos[i].x;
		if (Pos[i].y < rHull.top)	rHull.top	= Pos[i].y;
		if (Pos[i].y > rHull.bottom)rHull.bottom= Pos[i].y;
	}
};


/////////////////////////////////////////////////////////////////////////////
// CPositionBase - z�kladn� objekt pro polohu

IMPLEMENT_DYNAMIC(CPositionBase, CObject)

CPositionBase::CPositionBase()
{
}

CPositionBase::~CPositionBase()
{
}

// vytvo�en� kopie objektu
CPositionBase* CPositionBase::CreateCopyObject() const
{
	CObject* pObject = NULL;
	TRY
	{
		pObject = GetRuntimeClass()->CreateObject();
		if (pObject)
		{
			ASSERT_KINDOF(CPositionBase, pObject);
			((CPositionBase*)pObject)->CopyFrom(this);
		} 
		else
		{
			AfxThrowUserException();
		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pObject) delete pObject;
		pObject = NULL;
	}
	END_CATCH_ALL
	return ((CPositionBase*)pObject);
}

// kopie objektu
void CPositionBase::CopyFrom(const CPositionBase*)
{	// reset polohy
	ResetPosition();
}

// testov�n� platnosti
BOOL CPositionBase::IsValidPosition() const
{	
	return TRUE; 
}

/////////////////////////////////////////////////////////////////////////////
// CPositionRect - poloha - rect

IMPLEMENT_DYNCREATE(CPositionRect, CPositionBase)

CPositionRect::CPositionRect()
{
}

// kopie objektu
void CPositionRect::CopyFrom(const CPositionBase* pSrc)
{
	ASSERT_KINDOF(CPositionRect, pSrc);
	CPositionBase::CopyFrom(pSrc);
	// kopie vlastn�ch dat
	m_Position = ((CPositionRect*)pSrc)->m_Position;
}

// serializace
void CPositionRect::DoSerialize(CArchive& ar, DWORD nVer)
{
	// reset dat p�ed na�ten�m
	if (!ar.IsStoring()) ResetPosition();	
	// serializace dat
	MntSerializeRect(ar, m_Position);
}

// zru�en� v�ech �daj� polohy
void CPositionRect::ResetPosition()
{	// destrukce vlastn�ch dat
}

// testov�n� podobnosti
BOOL CPositionRect::IsSamePos(const CPositionBase* pPos) const
{	// porovn�n� vlastn�ch dat
	ASSERT_KINDOF(CPositionRect, pPos);
	return m_Position.EqualRect(((CPositionRect*)pPos)->m_Position);
}

// obal polohy
RECT CPositionRect::GetPositionHull() const
{	
	if (m_Position.Width() == 0 || m_Position.Height() == 0)
	{
		CRect rect = m_Position;
		rect.right++;
		rect.bottom++;
	}

	return m_Position; 
}

// je bod na plo�e ?
BOOL CPositionRect::IsPointAtArea(const POINT& pt) const
{	
	return m_Position.PtInRect(pt); 
}

BOOL CPositionRect::IsRectAtArea(const RECT& r, BOOL bFull) const
{	
	return IsRectAInRectB(r, m_Position, bFull); 
}

// je plocha v oblasti ?
BOOL CPositionRect::IsAreaInRect(const CRect& rPos) const
{
	CRect rHull = GetPositionHull();
	CRect rPom; 
	rPom.IntersectRect(&rHull, &rPos);
	if (rPom.EqualRect(rHull))
	{	// hull je v obalu
		return TRUE;
	}
	return FALSE;
}

BOOL CPositionRect::IsAreaInRgn(const CRgn& rPos) const
{
	CRgn rMain; 
	rMain.CreateRectRgnIndirect(m_Position);
	CRgn rPom;  
	rPom.CreateRectRgn(0, 0, 100, 100);
	rPom.CombineRgn(&rMain, (CRgn*)(&rPos), RGN_AND);

	if (rPom.EqualRgn(&rMain)) return TRUE;

	return FALSE;
}

// posun plochy
void CPositionRect::OffsetPosition(int xAmount, int yAmount)
{	
	m_Position.OffsetRect(xAmount, yAmount); 
}

void CPositionRect::TransformPosition(FMntMatrix2Val trans, CPointVal origin)
{
	// rectangle can't be rotated just move it
	FMntMatrix2 logTrans = trans.NegativeY(); // beware trans must to be transformed into log coord where is opposite sign of y
 
	CPoint center = m_Position.CenterPoint() - origin;
	center = logTrans * center + origin;

	m_Position.OffsetRect(center - m_Position.CenterPoint());
}

// p�evod na DP z LP
void CPositionRect::LPtoDP(CDC* pDC)
{	
	pDC->LPtoDP(m_Position); 
}

// p�evod na DP ze Scrolleru
void CPositionRect::ScrolltoDP(CFMntScrollerView* pView)
{	
	m_Position = pView->FromScrollToWindowPosition(m_Position); 
}

// vykreslov�n� trackeru
void CPositionRect::OnDrawTrackFrameInDP(CDC* pDC,const CFMntScrollerTracker*) const
{	
	// vytvo�en� regionu
#define MIN_DRAW_SIZE 3
	if (m_Position.Width() < MIN_DRAW_SIZE && m_Position.Height() < MIN_DRAW_SIZE)
	{
		pDC->SetPixelV(m_Position.CenterPoint(), RGB(255, 255, 255));
		return;
	}
	CRgn rgn; 
	rgn.CreateRectRgnIndirect(m_Position);
	CBrush brFrame(HS_CROSS, RGB(0, 0, 0));
	pDC->FrameRgn(&rgn, &brFrame, DP_TRACK_FRAME_SZ, DP_TRACK_FRAME_SZ);
}

void CPositionRect::OnDrawTrackHitsInDP(CDC* pDC,const CFMntScrollerTracker*) const
{
}

/////////////////////////////////////////////////////////////////////////////
// CPositionEllipse - poloha - rect

IMPLEMENT_DYNCREATE(CPositionEllipse, CPositionRect)

CPositionEllipse::CPositionEllipse()
{
}

CPositionEllipse::~CPositionEllipse()
{
}

inline float sqr(float a) 
{
	return (float) a*a;
}

// je bod na plo�e ?
BOOL CPositionEllipse::IsPointAtArea(const POINT& pt) const
{
	if (!base::IsPointAtArea(pt)) return FALSE;  

	// check analytically x^2/a^2 + y^2/b^2 <= 1
	int x = pt.x - m_Position.CenterPoint().x;
	int y = pt.y - m_Position.CenterPoint().y;

	float res = sqr(x * 2.0f / m_Position.Width()) + sqr(y * 2.0f / m_Position.Height());

	return res <= 1;
	/*CRgn rgn; rgn.CreateEllipticRgnIndirect(m_Position);
	return rgn.PtInRegion(pt);*/
}

BOOL CPositionEllipse::IsRectAtArea(const RECT& r, BOOL bFull) const
{
	if (!base::IsRectAtArea(r, bFull)) return FALSE;  

	CRgn rgn; 
	rgn.CreateEllipticRgnIndirect(m_Position);
	return rgn.RectInRegion(&r);
}

// je plocha v oblasti ?
BOOL CPositionEllipse::IsAreaInRect(const CRect& rPos) const
{
	CRect rHull = GetPositionHull();
	CRect rPom; rPom.IntersectRect(&rHull, &rPos);
	if (rPom.EqualRect(rHull))
	{	// hull je v obalu -> OK
		return TRUE;
	} 
	else if (!rPom.IsRectEmpty())
	{	// ��st -> zkusim region
		CRgn regPos;
		regPos.CreateRectRgnIndirect(&rPos);
		return IsAreaInRgn(regPos);
	}
	return FALSE;
}

BOOL CPositionEllipse::IsAreaInRgn(const CRgn& rPos) const
{
	// testuji HUll
	CRgn rMain; 
	rMain.CreateRectRgnIndirect(&GetPositionHull());
	CRgn rPom;  
	rPom.CreateRectRgn(0, 0, 100, 100);
	rPom.CombineRgn(&rMain, (CRgn*)(&rPos), RGN_AND);

	if (rPom.EqualRgn(&rMain)) return TRUE;

	// testuji objekt
	rMain.DeleteObject();
	rMain.CreateEllipticRgnIndirect(m_Position);
	rPom.CombineRgn(&rMain, (CRgn*)(&rPos), RGN_AND);

	if (rPom.EqualRgn(&rMain)) return TRUE;

	return FALSE;
}

// vykreslov�n� trackeru
void CPositionEllipse::OnDrawTrackFrameInDP(CDC* pDC,const CFMntScrollerTracker*) const
{
	// vytvo�en� regionu
	//CRgn rgn; rgn.CreateEllipticRgnIndirect(m_Position);
	//CBrush	  brFrame(HS_CROSS,RGB(0,0,0));
	//pDC->FrameRgn(&rgn,&brFrame,DP_TRACK_FRAME_SZ,DP_TRACK_FRAME_SZ);

	if (m_Position.Width() < MIN_DRAW_SIZE && m_Position.Height() < MIN_DRAW_SIZE)
	{
		pDC->SetPixelV(m_Position.CenterPoint(), RGB(255, 255, 255));
		return;
	}

	CPen cPen(PS_SOLID, 3, RGB(255, 255, 255));
	CPen* pcPenOld = pDC->SelectObject(&cPen);
	pDC->Arc(m_Position, CPoint(0, 0), CPoint(0, 0));
	pDC->SelectObject(pcPenOld);
}

void CPositionEllipse::OnDrawObject(CDC* pDC, COLORREF cFrame, int frameBrush, COLORREF cEntir, int entireBrush, const CDrawPar* pParams) const
{
	if (frameBrush < -1 && entireBrush < -1) return;

	RECT posHull = GetPositionHull();

#define MIN_DRAW_SIZE 3
	if ((posHull.right - posHull.left) < MIN_DRAW_SIZE && (posHull.bottom - posHull.top) < MIN_DRAW_SIZE) 
	{    
		pDC->SetPixelV((int) ((posHull.right + posHull.left) / 2), (int) ((posHull.top + posHull.bottom) / 2), cEntir);    
		return;
	}

	if (entireBrush == -1 && frameBrush == -1)
	{
		for (int i=0; i<CDrawPar::stdObjectCount; ++i)
		{
			if ((pParams->m_cStdObjFrame[i] == cFrame) &&
				(pParams->m_cStdObjEntir[i] == cEntir))
			{	// vykresl�m standardn�mi prost�edky
				CBrush* pBr = pDC->SelectObject(pParams->m_pStdObjBrush[i]);
				CPen*   pPn = pDC->SelectObject(pParams->m_pStdObjPen[i]);
				pDC->Ellipse(m_Position);
				pDC->SelectObject(pBr);
				pDC->SelectObject(pPn);
				return;
			}
		}
	}

	if (entireBrush >= -1)
	{
		// vykresl�m vlastn�mi prost�edky
		CBrush	br;

		if (entireBrush == -1)
		{
			br.CreateSolidBrush(cEntir);
		}
		else
		{
			br.CreateHatchBrush(entireBrush, cEntir);    
		}

		CPen	pn(frameBrush < -1 ? PS_NULL : PS_SOLID, 1, cFrame);    
		CBrush* pBr = pDC->SelectObject(&br);
		CPen*   pPn = pDC->SelectObject(&pn);
		pDC->Ellipse(m_Position);
		pDC->SelectObject(pBr);
		pDC->SelectObject(pPn);
		return;
	}
	else
	{
		CPen	pn(PS_SOLID, 1, cFrame);    
		CPen*   pPn = pDC->SelectObject(&pn);
		pDC->Arc(m_Position, CPoint(0, 0), CPoint(0, 0));    
		pDC->SelectObject(pPn);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CPositionPoly - poloha - polygon

IMPLEMENT_DYNCREATE(CPositionPoly, CPositionBase)

CPositionPoly::CPositionPoly()
{  
	m_hull.left = m_hull.top = m_hull.right = m_hull.bottom = 0;
}

CPositionPoly::~CPositionPoly()
{
}

// kopie objektu
void CPositionPoly::CopyFrom(const CPositionBase* pSrc)
{
	ASSERT_KINDOF(CPositionPoly, pSrc);
	CPositionBase::CopyFrom(pSrc);
	// kopie vlastn�ch dat
	m_Position = ((CPositionPoly*)pSrc)->m_Position;	
	RecalcPositionHull();
}

// serializace
void CPositionPoly::DoSerialize(CArchive& ar, DWORD nVer)
{
	// reset dat p�ed na�ten�m
	if (!ar.IsStoring()) ResetPosition();	

	// serializace dat
	if (nVer > 51)
	{
		int nCount = m_Position.Size();
		MntSerializeInt(ar, nCount);
		if (ar.IsLoading()) m_Position.Resize(nCount);
	}
	else
	{
		m_Position.Resize(4);
	}

	for (int i = 0; i < m_Position.Size(); ++i)
	{
		MntSerializeLong(ar, m_Position[i].x);
		MntSerializeLong(ar, m_Position[i].y);
	}

	RecalcPositionHull();
}

void CPositionPoly::CreateBox(const CPoint& ptCenter, float sizeX, float sizeY, const Matrix3& orient)
{
	m_Position.Reserve(4, 4);
	m_Position.Resize(4);
	double X,Z;
	X = -sizeX; 
	Z = -sizeY;
	m_Position[0].x = ptCenter.x+
    +(int)Round(orient(0, 0) * X - orient(0, 2) * Z, 0);
	m_Position[0].y = ptCenter.y+
    +(int)Round(-orient(2, 0) * X + orient(2, 2) * Z, 0);

	X = +sizeX; 
	Z = -sizeY;
	m_Position[1].x = ptCenter.x+
    +(int)Round(orient(0, 0) * X - orient(0, 2) * Z, 0);
	m_Position[1].y = ptCenter.y+
    +(int)Round(-orient(2, 0) * X + orient(2, 2) * Z, 0);

	X = +sizeX; 
	Z = +sizeY;
	m_Position[2].x = ptCenter.x+
    +(int)Round(orient(0, 0) * X - orient(0, 2) * Z, 0);
	m_Position[2].y = ptCenter.y+
    +(int)Round(-orient(2, 0) * X + orient(2, 2) * Z, 0);

	X = -sizeX; 
	Z = +sizeY;
	m_Position[3].x = ptCenter.x+
    +(int)Round(orient(0, 0) * X - orient(0, 2) * Z, 0);
	m_Position[3].y = ptCenter.y+
    +(int)Round(-orient(2, 0) * X + orient(2, 2) * Z,0);

	m_Position.Compact();
	RecalcPositionHull();
}

void CPositionPoly::CreateEllipse(const CPoint& ptCenter, float sizeX, float sizeY, const Matrix3& orient)
{
	const int nPointsInSegment = 20;

	float angle = 2 * PI / nPointsInSegment;
	m_Position.Reserve(nPointsInSegment, nPointsInSegment);
	m_Position.Resize(nPointsInSegment);

	for(int i = 0; i < nPointsInSegment; ++i)
	{
		float x = sizeX * cosf(angle * i);
		float y = sizeY * sinf(angle * i); 
		m_Position[i].x = (LONG) (orient(0, 0) * x + orient(2, 0) * y + ptCenter.x);
		m_Position[i].y = (LONG) (orient(0, 2) * x + orient(2, 2) * y + ptCenter.y);
	}
	m_Position.Compact();
	RecalcPositionHull();
}

// zru�en� v�ech �daj� polohy
void CPositionPoly::ResetPosition()
{	// destrukce vlastn�ch dat
	m_Position.Resize(0);
}

// testov�n� podobnosti
BOOL CPositionPoly::IsSamePos(const CPositionBase* pPos) const
{	// porovn�n� vlastn�ch dat
	ASSERT_KINDOF(CPositionPoly,pPos);
	if (m_Position.Size() != ((CPositionPoly*)pPos)->m_Position.Size()) return FALSE;

	for (int i = 0;i < m_Position.Size(); ++i)
	{
		if ((m_Position[i].x != ((CPositionPoly*)pPos)->m_Position[i].x) ||
			(m_Position[i].y != ((CPositionPoly*)pPos)->m_Position[i].y))
			return FALSE;
	}
	return TRUE;
}

// obal polohy
void CPositionPoly::RecalcPositionHull() 
{
	GetObjectHull(m_hull, m_Position);
}

// vytvo�en� regionu
void CPositionPoly::CreatePolyRgn(CRgn& rgn) const
{
	rgn.CreatePolygonRgn((POINT*)m_Position.Data(), m_Position.Size(), ALTERNATE);
}

// je bod na plo�e ?
BOOL CPositionPoly::IsPointAtArea(const POINT& pt) const
{	
	CRect rHull = GetPositionHull();
	if (rHull.PtInRect(pt))
	{
		CRgn rgn; 
		CreatePolyRgn(rgn);
		return rgn.PtInRegion(pt);
	}
	return FALSE;
}

BOOL CPositionPoly::IsRectAtArea(const RECT& r,BOOL bFull) const
{
	CRgn rgn; 
	CreatePolyRgn(rgn);
	if (bFull)
	{
		// ASSERT(FALSE); TODO
		return rgn.RectInRegion(&r);
	} 
	else
	{
		return rgn.RectInRegion(&r);
	}
	return FALSE;
}

// je plocha v oblasti ?
BOOL CPositionPoly::IsAreaInRect(const CRect& rPos) const
{
	CRect rHull = GetPositionHull();
	ASSERT(rHull.Width() > 0 && rHull.Height() > 0);
	CRect rPom; 
	rPom.IntersectRect(&rHull, &rPos);
	if (rPom.EqualRect(rHull))
	{	// hull je v obalu -> OK
		return TRUE;
	} 
	else if (!rPom.IsRectEmpty())
	{	// ��st -> zkusim region
		CRgn regPos;
		regPos.CreateRectRgnIndirect(&rPos);
		return IsAreaInRgn(regPos);
	}
	return FALSE;
}

BOOL CPositionPoly::IsAreaInRgn(const CRgn& rPos) const
{
	// testuji HUll
	if (!rPos.RectInRegion(&GetPositionHull())) return FALSE;

	CRgn rMain; 
	CRgn rPom;  
	rPom.CreateRectRgn(0, 0, 100, 100);
	
	rMain.CreatePolygonRgn(const_cast<LPPOINT>(m_Position.Data()), m_Position.Size(), ALTERNATE);
	rPom.CombineRgn(&rMain, (CRgn*)(&rPos), RGN_AND);

	if (rPom.EqualRgn(&rMain)) return TRUE;

	return FALSE;
}

// posun plochy
void CPositionPoly::OffsetPosition(int xAmount, int yAmount)
{
	for(int i = 0; i < m_Position.Size(); ++i)
	{
		m_Position[i].x += xAmount;
		m_Position[i].y += yAmount;
	}
	RecalcPositionHull();
}

void CPositionPoly::TransformPosition(FMntMatrix2Val trans, CPointVal origin)
{
	FMntMatrix2 logTrans = trans.NegativeY(); // beware trans must to be transformed into log coord where is opposite sign of y
	for(int i = 0; i < m_Position.Size(); ++i)
	{
		m_Position[i] = logTrans * (CPoint(m_Position[i]) - origin) + origin;    
	}
	RecalcPositionHull();
}

// p�evod na DP z LP
void CPositionPoly::LPtoDP(CDC* pDC)
{
	for(int i = 0; i < m_Position.Size(); ++i)
	{
		pDC->LPtoDP(&(m_Position[i]));
	}
  RecalcPositionHull();
}

// p�evod na DP ze Scrolleru
void CPositionPoly::ScrolltoDP(CFMntScrollerView* pView)
{
	for(int i = 0; i < m_Position.Size(); ++i)
	{
		m_Position[i] = pView->FromScrollToWindowPosition(CPoint(m_Position[i]));
	}
	RecalcPositionHull();
}

// vykreslov�n� trackeru
void CPositionPoly::OnDrawTrackFrameInDP(CDC* pDC,const CFMntScrollerTracker*) const
{	
	//CRgn rgn; CreatePolyRgn(rgn);
	//CBrush	  brFrame(HS_CROSS,RGB(0,0,0));
	//pDC->FrameRgn(&rgn,&brFrame,DP_TRACK_FRAME_SZ,DP_TRACK_FRAME_SZ);
  //pDC->Polygon((POINT*)m_Position,OBJPOS_PTCOUNT);

	CRect rect = GetPositionHull();
	if (rect.Width() < MIN_DRAW_SIZE && rect.Height() < MIN_DRAW_SIZE)
	{
		pDC->SetPixelV(rect.CenterPoint(), RGB(255, 255, 255));
		return;
	}

	CPen cPen(PS_SOLID, 3, RGB(255, 255, 255));
	CPen* pcPenOld = pDC->SelectObject(&cPen);
	pDC->Polyline((POINT*)m_Position.Data(), m_Position.Size());
	pDC->MoveTo(m_Position[m_Position.Size() - 1]);
	pDC->LineTo(m_Position[0]);
	pDC->SelectObject(pcPenOld);
}

void CPositionPoly::OnDrawTrackHitsInDP(CDC* pDC,const CFMntScrollerTracker* pTracker) const
{
	for(int i = 0; i < m_Position.Size(); ++i)
	{
		CRect rHandle;
		// nastav�m rect dle bodu
		rHandle.left  = m_Position[i].x - (pTracker->GetTrackHandleSize() / 2); 
		rHandle.top	  = m_Position[i].y - (pTracker->GetTrackHandleSize() / 2);
		rHandle.right = rHandle.left + pTracker->GetTrackHandleSize();
		rHandle.bottom= rHandle.top  + pTracker->GetTrackHandleSize();
		pDC->FillSolidRect(rHandle, pTracker->m_pScroller->m_wndColor ^ 0xFFFFFFFF);
	}
}

void CPositionPoly::OnDrawObject(CDC* pDC, COLORREF cFrame, int frameBrush, COLORREF cEntir, int entireBrush, const CDrawPar* pParams) const
{
	// frameBrush, entireBrush not used... 

	RECT posHull = GetPositionHull();

#define MIN_DRAW_SIZE 3
	if ((posHull.right - posHull.left) < MIN_DRAW_SIZE && (posHull.bottom - posHull.top) < MIN_DRAW_SIZE) 
	{    
		pDC->SetPixelV((int) ((posHull.right + posHull.left) / 2), (int) ((posHull.top + posHull.bottom) / 2), cEntir);    
		return;
	}

	for(int i = 0; i < CDrawPar::stdObjectCount; ++i)
	{
		if ((pParams->m_cStdObjFrame[i] == cFrame) &&
			(pParams->m_cStdObjEntir[i] == cEntir))
		{	// vykresl�m standardn�mi prost�edky
			CBrush* pBr = pDC->SelectObject(pParams->m_pStdObjBrush[i]);
			CPen*   pPn = pDC->SelectObject(pParams->m_pStdObjPen[i]);
			pDC->Polygon((POINT*)&(m_Position[0]), m_Position.Size());
			pDC->SelectObject(pBr);
			pDC->SelectObject(pPn);
			return;
		}
	}

	// vykresl�m vlastn�mi prost�edky
	CBrush	br(cEntir);
	CPen	pn(PS_SOLID, 1, cFrame);
	CBrush* pBr = pDC->SelectObject(&br);
	CPen*   pPn = pDC->SelectObject(&pn);
	pDC->Polygon((POINT*)&(m_Position[0]), m_Position.Size());
	pDC->SelectObject(pBr);
	pDC->SelectObject(pPn);
}

/*bool CPositionPoly::IsIntersection(const CPositionPoly& second)
{ 
  CRect hull1;
  GetObjectHull(hull1, m_Position);
  CRect hull2;
  GetObjectHull(hull2, second.m_Position);

  if (!IsIntersectRect(hull1,hull2))
    return false;

  // search for separation line, I suppose polygons are convex
  // For convex polygons must exist separation line, that is equal to first or second polygon sides

  for( int i = 0; i < OBJPOS_PTCOUNT; i++)
  {
    FMntVectorLong2 dir(m_Position[i], m_Position[(i == 0) ?  OBJPOS_PTCOUNT - 1 : i - 1]);
    FMntVectorLong2 norm(dir[1], -dir[0]); 
    
    int itestpt = (i == OBJPOS_PTCOUNT - 1) ? 0 : i + 1;

    if (norm * (FMntVectorLong2(m_Position[i],m_Position[itestpt])) > 0)
    {
      norm *= -1;      
    }

    int j = 0;
    for(; j < OBJPOS_PTCOUNT; j++)
      if (norm * FMntVectorLong2(m_Position[i], second.m_Position[j]) < 0)
        break;

    if (j == OBJPOS_PTCOUNT)
      return false; //separation line found
  }

  for( int i = 0; i < OBJPOS_PTCOUNT; i++)
  {
    FMntVectorLong2 dir(second.m_Position[i], second.m_Position[(i == 0) ?  OBJPOS_PTCOUNT - 1 : i - 1]);
    FMntVectorLong2 norm(dir[1], -dir[0]); 
    
    int itestpt = (i == OBJPOS_PTCOUNT - 1) ? 0: i + 1;

    if (norm * FMntVectorLong2(second.m_Position[i], second.m_Position[itestpt]) > 0)
    {
      norm *= -1;
    }

    int j = 0;
    for(; j < OBJPOS_PTCOUNT; j++)
      if (norm * FMntVectorLong2(second.m_Position[i], m_Position[j]) < 0)
        break;

    if (j == OBJPOS_PTCOUNT)
      return false; //separation line found
  }

  return true;
}*/

bool CPositionPoly::IsIntersection(const CPositionBase& sec) const
{ 
  if (sec.IsKindOf(RUNTIME_CLASS(CPositionGroup)))
    return sec.IsIntersection(*this);

  if (sec.IsKindOf(RUNTIME_CLASS(CPositionMulti)))
    return sec.IsIntersection(*this);

  if (!sec.IsKindOf(RUNTIME_CLASS(CPositionPoly)))  
    return false;  

  const CPositionPoly& second = (const CPositionPoly&) sec;

  CRect hull1;
  GetObjectHull(hull1, m_Position);
  CRect hull2;
  GetObjectHull(hull2, second.m_Position);

  if (!IsIntersectRect(hull1,hull2))
    return false;

  // search for separation line, I suppose polygons are convex
  // For convex polygons must exist separation line, that is equal to first or second polygon sides

  OBJPOSITION pos;
  OBJPOSITION sec_pos;

  pos.Resize(m_Position.Size());
  
  for( int i = 0; i < m_Position.Size(); i++)
  { 
    pos[i].x = m_Position[i].x - m_Position[0].x;
    pos[i].y = m_Position[i].y - m_Position[0].y;
  }

  sec_pos.Resize(second.m_Position.Size());
  for( int i = 0; i < second.m_Position.Size(); i++)
  { 
    sec_pos[i].x = second.m_Position[i].x - m_Position[0].x;
    sec_pos[i].y = second.m_Position[i].y - m_Position[0].y;
  }

  for( int i = 0; i < pos.Size(); i++)
  {
    FMntVectorLong2 dir(pos[i], pos[(i == 0) ?  pos.Size() - 1 : i - 1]);
    FMntVector2 norm((float) dir[1], (float) -dir[0]); 
    norm.Normalize();

    int itestpt = (i == pos.Size() - 1) ? 0 : i + 1;

    if ((FMntVectorLong2(pos[i],pos[itestpt]) * norm) > 0)
    {
      norm *= -1;      
    }

    int j = 0;
    for(; j < sec_pos.Size(); j++)
      if (FMntVectorLong2(pos[i], sec_pos[j]) * norm < 0)
        break;

    if (j == sec_pos.Size())
      return false; //separation line found
  }

  for( int i = 0; i < sec_pos.Size(); i++)
  {
    FMntVectorLong2 dir(sec_pos[i], sec_pos[(i == 0) ?  sec_pos.Size() - 1 : i - 1]);
    FMntVector2 norm((float) dir[1],(float)  -dir[0]);
    norm.Normalize();

    int itestpt = (i == sec_pos.Size() - 1) ? 0: i + 1;

    if ((FMntVectorLong2(sec_pos[i], sec_pos[itestpt]) * norm) > 0)
    {
      norm *= -1;
    }

    int j = 0;
    for(; j < pos.Size(); j++)
      if ((FMntVectorLong2(sec_pos[i], pos[j]) * norm) < 0)
        break;

    if (j == pos.Size())
      return false; //separation line found
  }

  return true;
}
/////////////////////////////////////////////////////////////////////////////
// CPositionArea - poloha - kombinace rect�

IMPLEMENT_DYNCREATE(CPositionArea,CPositionBase)

CPositionArea::CPositionArea()
{};
CPositionArea::~CPositionArea()
{	ResetPosition(); };

// kopie objektu
void CPositionArea::CopyFrom(const CPositionBase* pSrc)
{
	ASSERT_KINDOF(CPositionArea,pSrc);
	CPositionBase::CopyFrom(pSrc);
	// kopie vlastn�ch dat
	m_rHull = ((CPositionArea*)pSrc)->m_rHull;
	for(int i=0; i<((CPositionArea*)pSrc)->m_Position.GetSize(); i++)
		m_Position.Add(((CPositionArea*)pSrc)->m_Position[i]);
};

// serializace
void CPositionArea::DoSerialize(CArchive& ar, DWORD nVer)
{
	// reset dat p�ed na�ten�m
	if (!ar.IsStoring())
		ResetPosition();	
	// serializace dat
	int nCount = m_Position.GetSize();
	MntSerializeInt(ar,nCount);
	for(int i=0; i<nCount; i++)
	{
		if (ar.IsStoring())
		{
			CRect rSave = m_Position[i];
			MntSerializeRect(ar,rSave);
		} else
		{
			CRect rLoad;
			MntSerializeRect(ar,rLoad);
			m_Position.Add(rLoad);
		};
	};
	// serializace dat
	if (!ar.IsStoring())
		RecalcPositionHull();
};

// zru�en� v�ech �daj� polohy
void CPositionArea::ResetPosition()
{	// destrukce vlastn�ch dat
	m_Position.RemoveAll();
};
// testov�n� podobnosti
BOOL CPositionArea::IsSamePos(const CPositionBase* pPos) const
{	// porovn�n� vlastn�ch dat
	ASSERT_KINDOF(CPositionArea,pPos);
	if (m_Position.GetSize()!=((CPositionArea*)pPos)->m_Position.GetSize())
		return FALSE;
	if (!m_rHull.EqualRect(((CPositionArea*)pPos)->m_rHull))
		return FALSE;
	for(int i=0; i<m_Position.GetSize(); i++)
	{
		CRect r1 = m_Position[i],r2 = ((CPositionArea*)pPos)->m_Position[i];
		if (!r1.EqualRect(r2))
			return FALSE;
	}
	return TRUE;
};

// testov�n� platnosti
BOOL CPositionArea::IsValidPosition() const
{	return (m_Position.GetSize()!=0); };
// vlo�en� nov�ho rectu
void CPositionArea::AddRect(const RECT& rArea)
{
	m_Position.Add(rArea); 
	// uprav�m obal
	if (m_Position.GetSize()==1)
		m_rHull = rArea;
	else
	{
		if (m_rHull.left   > rArea.left)
			m_rHull.left   = rArea.left;
		if (m_rHull.top    > rArea.top)
			m_rHull.top    = rArea.top;
		if (m_rHull.right  < rArea.right)
			m_rHull.right  = rArea.right;
		if (m_rHull.bottom < rArea.bottom)
			m_rHull.bottom = rArea.bottom;
	}
};



typedef struct 
{
  BOOL bFromRemove;
  LONG s,e;
} INTERVAL;

TypeIsSimple(INTERVAL);
/** Function removes rectangle from m_Position set.
  */
void  CPositionArea::RemoveRect(const RECT& rectRem)
{
  CRectArray cNewPosition;

  for(int i = 0; i < m_Position.GetSize(); i++)
  {
    CRect rect = m_Position[i];
    if( IsIntersectRect(rectRem, rect) )
    {
      if (!IsRectInRect(rect, rectRem))
      {
        if (IsRectInRect(rectRem, rect))
        {
          CRect rectRes;
          rectRes.left = rect.left;
          rectRes.right = rectRem.left;
          rectRes.bottom = rect.bottom;
          rectRes.top = rect.top;

          if (rectRes.Width() >= 0 && rectRes.Height() >= 0)
            cNewPosition.Add(rectRes);

          rectRes.left = rectRem.left;
          rectRes.right = rectRem.right;
          //rectRes.bottom = rect.bottom;
          rectRes.top = rectRem.bottom;

          if (rectRes.Width() >= 0 && rectRes.Height() >= 0)
            cNewPosition.Add(rectRes);

          //rectRes.left = rectRem.left;
          //rectRes.right = rectRem.right;
          rectRes.bottom = rectRem.top;
          rectRes.top = rect.top;

          if (rectRes.Width() >= 0 && rectRes.Height() >= 0)
            cNewPosition.Add(rectRes);

          rectRes.left = rectRem.right;
          rectRes.right = rect.right;
          rectRes.bottom = rect.bottom;
          rectRes.top = rect.top;

          if (rectRes.Width() >= 0 && rectRes.Height() >= 0)
            cNewPosition.Add(rectRes);
        }
        else
        {

          AutoArray<INTERVAL> cIntervalsX;
          cIntervalsX.Realloc(3);

          INTERVAL inter;

          inter.s = rect.left;
          if (rectRem.left > rect.left)
          {
            inter.e = rectRem.left;
            inter.bFromRemove = FALSE;

            cIntervalsX.Add(inter);

            inter.s = rectRem.left;
          }

          if (rect.right > rectRem.right)
          {
            inter.e = rectRem.right;
            inter.bFromRemove = TRUE;

            cIntervalsX.Add(inter);

            inter.s = rectRem.right;
            inter.e = rect.right;
            inter.bFromRemove = FALSE;

            cIntervalsX.Add(inter);
          }
          else
          {
            inter.e = rect.right;
            inter.bFromRemove = TRUE;

            cIntervalsX.Add(inter);
          }
          
          AutoArray<INTERVAL> cIntervalsY;
          cIntervalsY.Realloc(3);          

          inter.s = rect.top;
          if (rectRem.top > rect.top)
          {
            inter.e = rectRem.top;
            inter.bFromRemove = FALSE;

            cIntervalsY.Add(inter);

            inter.s = rectRem.top;
          }

          if (rect.bottom > rectRem.bottom)
          {
            inter.e = rectRem.bottom;
            inter.bFromRemove = TRUE;

            cIntervalsY.Add(inter);

            inter.s = rectRem.bottom;
            inter.e = rect.bottom;
            inter.bFromRemove = FALSE;

            cIntervalsY.Add(inter);
          }
          else
          {
            inter.e = rect.bottom;
            inter.bFromRemove = TRUE;

            cIntervalsY.Add(inter);
          }

          for(int j = 0; j < cIntervalsX.Size(); j++)
          {
            for(int k = 0; k < cIntervalsY.Size(); k++)
            {
              if (cIntervalsX[j].bFromRemove && cIntervalsY[k].bFromRemove)
                continue;

              CRect rectNew(cIntervalsX[j].s, cIntervalsY[k].s,cIntervalsX[j].e, cIntervalsY[k].e);
              cNewPosition.Add(rectNew);
            }
          }
        }
      }
    }
    else
      cNewPosition.Add(rect);
  }

  m_Position.RemoveAll();
  for(int i = 0; i < cNewPosition.GetSize(); i++)
  {
    m_Position.Add(cNewPosition[i]);
  }
  RecalcPositionHull();
}

// obal polohy
RECT CPositionArea::GetPositionHull() const
{	return m_rHull; };

void CPositionArea::RecalcPositionHull()
{
	if (m_Position.GetSize()>0)
	{
		m_rHull = m_Position[0];
		for(int i=m_Position.GetSize()-1;i>=1;i--)
		{
			CRect rArea = m_Position[i];
			if (m_rHull.left   > rArea.left)
				m_rHull.left   = rArea.left;
			if (m_rHull.top    > rArea.top)
				m_rHull.top    = rArea.top;
			if (m_rHull.right  < rArea.right)
				m_rHull.right  = rArea.right;
			if (m_rHull.bottom < rArea.bottom)
				m_rHull.bottom = rArea.bottom;
		}

    if (m_rHull.Width() == 0)
      m_rHull.right++;

    if (m_rHull.Height() == 0)
      m_rHull.top++;
	}
  
};

// je bod na plo�e ?
BOOL CPositionArea::IsPointAtArea(const POINT& pt) const
{
	if (!IsValidPosition()) return FALSE;
	if (m_rHull.PtInRect(pt))
	{
		for(int i=m_Position.GetSize()-1;i>=0;i--)
		{
			if (::PtInRect(&(m_Position[i]),pt))
				return TRUE;
		}
	}
	return FALSE;
};
BOOL CPositionArea::IsRectAtArea(const RECT& r,BOOL bFull) const
{
	CRgn rgn; CreateAreaRgn(rgn);
	if (bFull)
	{
		// ASSERT(FALSE);
		return rgn.RectInRegion(&r);
	} else
		return rgn.RectInRegion(&r);
}

// je plocha v oblasti ?
BOOL CPositionArea::IsAreaInRect(const CRect& rPos) const
{
	CRect rHull = GetPositionHull();
	CRect rPom; rPom.IntersectRect(&rHull,&rPos);
	if (rPom.EqualRect(rHull))
	{	// hull je v obalu -> OK
		return TRUE;
	} else
	if (!rPom.IsRectEmpty())
	{	// ��st -> zkusim region
		CRgn regPos;
		regPos.CreateRectRgnIndirect(&rPos);
		return IsAreaInRgn(regPos);
	}
	return FALSE;
};
BOOL CPositionArea::IsAreaInRgn(const CRgn& rPos) const
{
	// testuji HUll
	CRgn   rMain; rMain.CreateRectRgnIndirect(&GetPositionHull());
	CRgn   rPom;  rPom.CreateRectRgn(0,0,100,100);
	rPom.CombineRgn(&rMain,(CRgn*)(&rPos),RGN_AND);
	if (rPom.EqualRgn(&rMain))
		return TRUE;
	// testuji objekt
	rMain.DeleteObject();
	CreateAreaRgn(rMain);
	rPom.CombineRgn(&rMain,(CRgn*)(&rPos),RGN_AND);
	if (rPom.EqualRgn(&rMain))
		return TRUE;
	return FALSE;
};

// posun plochy
void CPositionArea::OffsetPosition(int xAmount,int yAmount)
{
	for(int i=m_Position.GetSize()-1;i>=0;i--)
		::OffsetRect(&(m_Position[i]),xAmount,yAmount);
	::OffsetRect(&m_rHull,xAmount,yAmount);
};

void CPositionArea::TransformPosition(FMntMatrix2Val trans, CPointVal origin)
{
  FMntMatrix2 logTrans = trans.NegativeY(); // beware trans must to be transformed into log coord where is opposite sign of y
  CPoint offset = m_rHull.CenterPoint() - origin;
  offset = logTrans * offset + origin;
  offset -= m_rHull.CenterPoint();
  m_rHull.OffsetRect(offset);

  for(int i=m_Position.GetSize()-1;i>=0;i--)
  {
    m_Position[i].left += offset.x;
    m_Position[i].top += offset.y;
    m_Position[i].right += offset.x;
    m_Position[i].bottom += offset.y;
  } 
};


// p�evod na DP z LP
void CPositionArea::LPtoDP(CDC* pDC)
{
	for(int i=m_Position.GetSize()-1;i>=0;i--)
		pDC->LPtoDP(&(m_Position[i]));
	RecalcPositionHull();
	//pDC->LPtoDP(m_rHull);
};
// p�evod na DP ze Scrolleru
void CPositionArea::ScrolltoDP(CFMntScrollerView* pView)
{
	for(int i=m_Position.GetSize()-1;i>=0;i--)
		m_Position[i] = pView->FromScrollToWindowPosition(CRect(m_Position[i]));
	RecalcPositionHull();
	//m_rHull = pView->FromScrollToWindowPosition(m_rHull);
};

// vytvo�en� regionu
void CPositionArea::CreateAreaRgn(CRgn& rgn) const
{
	ASSERT(m_Position.GetSize()>0);
	rgn.CreateRectRgnIndirect(&(m_Position[0]));
	for(int i=1; i<m_Position.GetSize(); i++)
	{
		CRgn rgn2;rgn2.CreateRectRgnIndirect(&(m_Position[i]));
		rgn.CombineRgn(&rgn,&rgn2,RGN_OR);
	}
};

// vykreslov�n� trackeru
void CPositionArea::OnDrawTrackFrameInDP(CDC* pDC,const CFMntScrollerTracker*) const
{
	// vytvo�en� regionu
	CRgn rgn; CreateAreaRgn(rgn);
	CBrush	  brFrame(RGB(255,255,255));
	pDC->FrameRgn(&rgn,&brFrame,DP_TRACK_FRAME_SZ,DP_TRACK_FRAME_SZ);
};
void CPositionArea::OnDrawTrackHitsInDP(CDC* pDC,const CFMntScrollerTracker*) const
{};

void CPositionArea::OnDrawObject(CDC* pDC, COLORREF cFrame, int frameBrushStyle, COLORREF cEntir, int entireBrushStyle, const CDrawPar* pParams) const
{
  // vytvo�en� regionu
  CRgn	rgn; 
  CreateAreaRgn(rgn);

  // v�pl� 
  pDC->SetBkMode(TRANSPARENT);
  if ((pParams==NULL) || (pParams->m_VwStyle.m_bAreasEntire) )
  {
    if (entireBrushStyle >0)
    {
      CBrush  brE(entireBrushStyle,cEntir);
      pDC->FillRgn(&rgn,&brE);
    }
    else
    {
      CBrush  brE(cEntir);
      pDC->FillRgn(&rgn,&brE);
    }
  }
  // or�mov�n�
  if ( (pParams==NULL) || (pParams->m_VwStyle.m_bAreasBorder) )
  {
    if (frameBrushStyle > 0)
    {    
      CBrush  brF(frameBrushStyle, cFrame);
      pDC->FrameRgn(&rgn,&brF,2,2);
    }
    else
    {
      CBrush  brF(cFrame);
      pDC->FrameRgn(&rgn,&brF,2,2);
    }
  };
}

/////////////////////////////////////////////////////////////////////////////
// CPositionMulti - poloha - polygon s v�ce body

IMPLEMENT_DYNCREATE(CPositionMulti,CPositionBase)

CPositionMulti::CPositionMulti()
{	ResetPosition(); };

CPositionMulti::~CPositionMulti()
{};

// kopie objektu
void CPositionMulti::CopyFrom(const CPositionBase* pSrc)
{
	ASSERT_KINDOF(CPositionMulti,pSrc);
	CPositionBase::CopyFrom(pSrc);
	// kopie vlastn�ch dat
	m_nCount= ((CPositionMulti*)pSrc)->m_nCount;
	for(int i=0; i<maxPolygonPoints; i++)
		m_Points[i] = ((CPositionMulti*)pSrc)->m_Points[i];
	m_rHull = ((CPositionMulti*)pSrc)->m_rHull;
};

// serializace
void CPositionMulti::DoSerialize(CArchive& ar, DWORD nVer)
{
	// serializace dat
	MntSerializeInt(ar,m_nCount);
	for(int i=0; i<m_nCount; i++)
	{
		MntSerializeLong(ar,m_Points[i].x);
		MntSerializeLong(ar,m_Points[i].y);
	};
	// update HULL
	if (!ar.IsStoring())
		RecalcPositionHull();
};

// zru�en� v�ech �daj� polohy
void CPositionMulti::ResetPosition()
{	m_nCount = 0; };

// testov�n� platnosti
BOOL CPositionMulti::IsValidPosition() const
{	return (m_nCount>=3); };

// vlo�en� nov�ho bodu
void CPositionMulti::DoAddNewPoint(const POINT& pt,BOOL bUpdateHull)
{
	ASSERT(m_nCount < maxPolygonPoints);
	m_Points[m_nCount] = pt;
	m_nCount++;
	// update hull ?
	if (bUpdateHull)
		RecalcPositionHull();
};

// testov�n� podobnosti
BOOL CPositionMulti::IsSamePos(const CPositionBase* pPos) const
{	// porovn�n� vlastn�ch dat
	ASSERT_KINDOF(CPositionMulti,pPos);
	if (m_nCount!=((CPositionMulti*)pPos)->m_nCount)
		return FALSE;
	if (!m_rHull.EqualRect(((CPositionMulti*)pPos)->m_rHull))
		return FALSE;
	for(int i=0; i<m_nCount; i++)
	{
		if ((m_Points[i].x != ((CPositionMulti*)pPos)->m_Points[i].x)||
			(m_Points[i].y != ((CPositionMulti*)pPos)->m_Points[i].y))
			return FALSE;
	}
	return TRUE;
};
// obal polohy
RECT CPositionMulti::GetPositionHull() const
{	return m_rHull; };
void CPositionMulti::RecalcPositionHull()
{
	if (IsValidPosition())
	{
		m_rHull.SetRect(m_Points[0].x,m_Points[0].y,m_Points[0].x + 1,m_Points[0].y + 1);
		for(int i=m_nCount-1;i>=1;i--)
		{
			if (m_rHull.left   > m_Points[i].x)
				m_rHull.left   = m_Points[i].x;
			if (m_rHull.top    > m_Points[i].y)
				m_rHull.top    = m_Points[i].y;
			if (m_rHull.right  < m_Points[i].x)
				m_rHull.right  = m_Points[i].x;
			if (m_rHull.bottom < m_Points[i].y)
				m_rHull.bottom = m_Points[i].y;
		}
	};
};

// vytvo�en� regionu
void CPositionMulti::CreateMultiRgn(CRgn& rgn) const
{
	if (IsValidPosition())
		rgn.CreatePolygonRgn((POINT*)(&(m_Points[0])),m_nCount,ALTERNATE);
};

// je bod na plo�e ?
BOOL CPositionMulti::IsPointAtArea(const POINT& pt) const
{
	if (!IsValidPosition()) return FALSE;
	if (m_rHull.PtInRect(pt))
	{
		CRgn rgn;  CreateMultiRgn(rgn);
		return rgn.PtInRegion(pt);
	}
	return FALSE;
};
BOOL CPositionMulti::IsRectAtArea(const RECT& r,BOOL bFull) const
{
	CRgn rgn;  CreateMultiRgn(rgn);
	if (bFull)
	{	// ASSERT(FALSE);
		return rgn.RectInRegion(&r);
	} else
		return rgn.RectInRegion(&r);
	return FALSE;
};

// je plocha v oblasti ?
BOOL CPositionMulti::IsAreaInRect(const CRect& rPos) const
{
	CRect rPom; rPom.IntersectRect(&m_rHull,&rPos);
	if (rPom.EqualRect(m_rHull))
	{	// hull je v obalu -> OK
		return TRUE;
	} else
	if (!rPom.IsRectEmpty())
	{	// ��st -> zkusim region
		CRgn regPos; CreateMultiRgn(regPos);
		return IsAreaInRgn(regPos);
	}
	return FALSE;
};
BOOL CPositionMulti::IsAreaInRgn(const CRgn& rPos) const
{
	// testuji HUll
	CRgn   rMain; rMain.CreateRectRgnIndirect(&m_rHull);
	CRgn   rPom;  rPom.CreateRectRgn(0,0,100,100);
	rPom.CombineRgn(&rMain,(CRgn*)(&rPos),RGN_AND);
	if (rPom.EqualRgn(&rMain))
		return TRUE;
	// testuji objekt
	rMain.DeleteObject();
	CreateMultiRgn(rMain);
	rPom.CombineRgn(&rMain,(CRgn*)(&rPos),RGN_AND);
	if (rPom.EqualRgn(&rMain))
		return TRUE;
	return FALSE;
};

// posun plochy
void CPositionMulti::OffsetPosition(int xAmount,int yAmount)
{
	for(int i=0; i<m_nCount; i++)
	{
		m_Points[i].x += xAmount;
		m_Points[i].y += yAmount;
	};
};

void CPositionMulti::TransformPosition(FMntMatrix2Val trans, CPointVal origin)
{
  FMntMatrix2 logTrans = trans.NegativeY(); // beware trans must to be transformed into log coord where is opposite sign of y
  for(int i=0; i<m_nCount; i++)
  {
    m_Points[i] = logTrans * (CPoint(m_Points[i]) - origin) + origin;    
  };

  CPoint center = m_rHull.CenterPoint() - origin;
  center = logTrans * center + origin;
  m_rHull.OffsetRect(center - m_rHull.CenterPoint());
};

// p�evod na DP z LP
void CPositionMulti::LPtoDP(CDC* pDC)
{
	for(int i=0; i<m_nCount; i++)
		pDC->LPtoDP(&(m_Points[i]));
};
// p�evod na DP ze Scrolleru
void CPositionMulti::ScrolltoDP(CFMntScrollerView* pView)
{
	for(int i=0; i<m_nCount; i++)
		m_Points[i] = pView->FromScrollToWindowPosition(CPoint(m_Points[i]));
};

// vykreslov�n� trackeru
void CPositionMulti::OnDrawTrackFrameInDP(CDC* pDC,const CFMntScrollerTracker*) const
{

  CRect rect = GetPositionHull();
  if (rect.Width() < MIN_DRAW_SIZE && rect.Height() < MIN_DRAW_SIZE)
  {
    pDC->SetPixelV(rect.CenterPoint(),RGB(255,255,255));
    return;
  }

	CRgn rgn; CreateMultiRgn(rgn);
	CBrush	  brFrame(HS_CROSS,RGB(0,0,0));
	pDC->FrameRgn(&rgn,&brFrame,DP_TRACK_FRAME_SZ,DP_TRACK_FRAME_SZ);
};

void CPositionMulti::OnDrawTrackHitsInDP(CDC* pDC,const CFMntScrollerTracker*) const
{};

bool CPositionMulti::IsIntersection(const CPositionBase& sec) const
{
  if (sec.IsKindOf(RUNTIME_CLASS(CPositionMulti)))
  {
    const CPositionMulti& second = (const CPositionMulti& ) sec;

    CRect hull1 = GetPositionHull();    
    CRect hull2 = second.GetPositionHull();    

    if (!IsIntersectRect(hull1,hull2))
      return false;

    CRgn rgn; CreateMultiRgn(rgn);
    CRgn secrgn; second.CreateMultiRgn(secrgn);

    CRgn   rPom; rPom.CreateRectRgn(0,0,1,1);
    int ret = rPom.CombineRgn(&rgn,&secrgn,RGN_AND);

    if (ret == NULLREGION)
      return false;
    else
      return true;
  }

  if (sec.IsKindOf(RUNTIME_CLASS(CPositionPoly)))
  {
    const CPositionPoly& second = (const CPositionPoly& ) sec;

    CRgn rgn; 
    CreateMultiRgn(rgn);
    CRgn secrgn; 
    secrgn.CreatePolygonRgn(const_cast<LPPOINT>(second.GetPoly().Data()), second.GetPoly().Size(), WINDING); 

    CRgn rPom; rPom.CreateRectRgn(0,0,1,1); // dummy initialization, but needed
    int ret = rPom.CombineRgn(&rgn,&secrgn,RGN_AND);

    if (ret == NULLREGION)
      return false;
    else
      return true;
  }

  ASSERT(FALSE);
  LogF("IsIntersection not supported, between current types.");
  return false;
}

/////////////////////////////////////////////////////////////////////////////
// CPositionGroup - poloha - kombinace poloh

IMPLEMENT_DYNCREATE(CPositionGroup,CPositionBase)

CPositionGroup::CPositionGroup()
{};
CPositionGroup::~CPositionGroup()
{	ResetPosition(); };

// kopie objektu
void CPositionGroup::CopyFrom(const CPositionBase* pSrc)
{
	ASSERT_KINDOF(CPositionGroup,pSrc);
	CPositionBase::CopyFrom(pSrc);
	// kopie vlastn�ch dat
	m_rHull = ((CPositionGroup*)pSrc)->m_rHull;
	for(int i=0; i<((CPositionGroup*)pSrc)->m_Position.GetSize(); i++)
	{
		CPositionBase* pSrcPos = (CPositionBase*)(((CPositionGroup*)pSrc)->m_Position[i]);
		ASSERT(pSrcPos != NULL);
		ASSERT_KINDOF(CPositionBase,pSrcPos);
		m_Position.Add(pSrcPos->CreateCopyObject());
	}
};

// serializace
void CPositionGroup::DoSerialize(CArchive& ar, DWORD nVer)
{	ASSERT(FALSE);
	// nen� serializov�no
};

// zru�en� v�ech �daj� polohy
void CPositionGroup::ResetPosition()
{	// destrukce vlastn�ch dat
	DestroyArrayObjects(&m_Position);
};
// testov�n� podobnosti
BOOL CPositionGroup::IsSamePos(const CPositionBase* pPos) const
{	// porovn�n� vlastn�ch dat
	ASSERT_KINDOF(CPositionGroup,pPos);
	if (m_Position.GetSize()!=((CPositionGroup*)pPos)->m_Position.GetSize())
		return FALSE;
	if (!m_rHull.EqualRect(((CPositionGroup*)pPos)->m_rHull))
		return FALSE;
	for(int i=0; i<m_Position.GetSize(); i++)
	{
		CPositionBase* pPos1 = (CPositionBase*)(((CPositionGroup*)pPos)->m_Position[i]);
		CPositionBase* pPos2 = (CPositionBase*)(m_Position[i]);
		ASSERT(pPos1 != NULL);
		ASSERT(pPos2 != NULL);
		ASSERT_KINDOF(CPositionBase,pPos1);
		ASSERT_KINDOF(CPositionBase,pPos2);
		if (!pPos1->IsSamePos(pPos2))
			return FALSE;
	}
	return TRUE;
};

// testov�n� platnosti
BOOL CPositionGroup::IsValidPosition() const
{	return (m_Position.GetSize()!=0); };

// vlo�en� nov� pozice
void CPositionGroup::DoAddNewPos(const CPositionBase* pPos)
{
	CPositionBase* pNewPos = pPos->CreateCopyObject();
	if (pNewPos == NULL) return;
	// vlo�en� 
	m_Position.Add(pNewPos);
	CRect rArea = pNewPos->GetPositionHull();
	// uprav�m obal
	if (m_Position.GetSize()==1)
		m_rHull = rArea;
	else
	{
		if (m_rHull.left   > rArea.left)
			m_rHull.left   = rArea.left;
		if (m_rHull.top    > rArea.top)
			m_rHull.top    = rArea.top;
		if (m_rHull.right  < rArea.right)
			m_rHull.right  = rArea.right;
		if (m_rHull.bottom < rArea.bottom)
			m_rHull.bottom = rArea.bottom;
	}
};

// obal polohy
RECT CPositionGroup::GetPositionHull() const
{	return m_rHull; };

void CPositionGroup::RecalcPositionHull()
{
	if (m_Position.GetSize()>0)
	{
		m_rHull = ((CPositionBase*)(m_Position[0]))->GetPositionHull();
		for(int i=m_Position.GetSize()-1;i>=1;i--)
		{
			CRect rArea = ((CPositionBase*)(m_Position[i]))->GetPositionHull();
			if (m_rHull.left   > rArea.left)
				m_rHull.left   = rArea.left;
			if (m_rHull.top    > rArea.top)
				m_rHull.top    = rArea.top;
			if (m_rHull.right  < rArea.right)
				m_rHull.right  = rArea.right;
			if (m_rHull.bottom < rArea.bottom)
				m_rHull.bottom = rArea.bottom;
		}
	}
};

// je bod na plo�e ?
BOOL CPositionGroup::IsPointAtArea(const POINT& pt) const
{
	if (!IsValidPosition()) return FALSE;
	if (m_rHull.PtInRect(pt))
	{
		for(int i=m_Position.GetSize()-1;i>=0;i--)
		{
			CPositionBase* pPos = (CPositionBase*)(m_Position[i]);
			if ((pPos)&&(pPos->IsPointAtArea(pt)))
				return TRUE;
		}
	}
	return FALSE;
};
BOOL CPositionGroup::IsRectAtArea(const RECT& r,BOOL bFull) const
{
	for(int i=m_Position.GetSize()-1;i>=0;i--)
	{
		CPositionBase* pPos = (CPositionBase*)(m_Position[i]);
		if (pPos->IsRectAtArea(r,bFull))
			return TRUE;
	}
	return FALSE;
};

// je plocha v oblasti ?
BOOL CPositionGroup::IsAreaInRect(const CRect& rPos) const
{
	for(int i=m_Position.GetSize()-1;i>=0;i--)
	{
		CPositionBase* pPos = (CPositionBase*)(m_Position[i]);
		if (!pPos->IsAreaInRect(rPos))
			return FALSE;
	}
	return TRUE;
};
BOOL CPositionGroup::IsAreaInRgn(const CRgn& rPos) const
{
	for(int i=m_Position.GetSize()-1;i>=0;i--)
	{
		CPositionBase* pPos = (CPositionBase*)(m_Position[i]);
		if (!pPos->IsAreaInRgn(rPos))
			return FALSE;
	}
	return TRUE;
};

// posun plochy
void CPositionGroup::OffsetPosition(int xAmount,int yAmount)
{
	for(int i=m_Position.GetSize()-1;i>=0;i--)
	{
		CPositionBase* pPos = (CPositionBase*)(m_Position[i]);
		pPos->OffsetPosition(xAmount,yAmount);
	}
	::OffsetRect(&m_rHull,xAmount,yAmount);
};

void CPositionGroup::TransformPosition(FMntMatrix2Val trans, CPointVal origin)
{
  for(int i=m_Position.GetSize()-1;i>=0;i--)
  {
    CPositionBase* pPos = (CPositionBase*)(m_Position[i]);
    pPos->TransformPosition(trans,origin);
  }

  FMntMatrix2 logTrans = trans.NegativeY(); // beware trans must to be transformed into log coord where is opposite sign of y
  CPoint center = m_rHull.CenterPoint() - origin;
  center = logTrans * center + origin;
  m_rHull.OffsetRect(center - m_rHull.CenterPoint());
}
// p�evod na DP z LP
void CPositionGroup::LPtoDP(CDC* pDC)
{
	for(int i=m_Position.GetSize()-1;i>=0;i--)
	{
		CPositionBase* pPos = (CPositionBase*)(m_Position[i]);
		pPos->LPtoDP(pDC);
	}
	RecalcPositionHull();
	//pDC->LPtoDP(m_rHull);
};
// p�evod na DP ze Scrolleru
void CPositionGroup::ScrolltoDP(CFMntScrollerView* pView)
{
	for(int i=m_Position.GetSize()-1;i>=0;i--)
	{
		CPositionBase* pPos = (CPositionBase*)(m_Position[i]);
		pPos->ScrolltoDP(pView);
	}
	RecalcPositionHull();
	//m_rHull = pView->FromScrollToWindowPosition(m_rHull);
};

// vykreslov�n� trackeru
void CPositionGroup::OnDrawTrackFrameInDP(CDC* pDC,const CFMntScrollerTracker* pTracker) const
{
	for(int i=m_Position.GetSize()-1;i>=0;i--)
	{
		CPositionBase* pPos = (CPositionBase*)(m_Position[i]);
		pPos->OnDrawTrackFrameInDP(pDC,pTracker);
	}
};
void CPositionGroup::OnDrawTrackHitsInDP(CDC* pDC,const CFMntScrollerTracker* pTracker) const
{
	for(int i=m_Position.GetSize()-1;i>=0;i--)
	{
		CPositionBase* pPos = (CPositionBase*)(m_Position[i]);
		pPos->OnDrawTrackHitsInDP(pDC,pTracker);
	}
};

bool CPositionGroup::IsIntersection(const CPositionBase& sec) const
{
  for(int i=m_Position.GetSize()-1;i>=0;i--)
  {
    CPositionBase* pPos = (CPositionBase*)(m_Position[i]);
    if (pPos->IsIntersection(sec))
      return true;
  }

  return false;
}
