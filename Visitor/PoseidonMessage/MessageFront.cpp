/*********************************************************************/
/*																	 */
/*	Poseidon Messages 												 */
/*									Copyright � Mentar, 1998    	 */
/*																	 */
/*********************************************************************/

#include <windows.h>
#include "MessageDll.h"
#include "MessageInter.h"

//////////////////////////////////////////////////////////////
// CMessageFront

CMessageFront::CMessageFront()
{
	m_pFirstToServer =
	m_pLastToServer	 =
	m_pFirstToClient =
	m_pLastToClient	 = NULL;
};

CMessageFront::~CMessageFront()
{
	// destrukce nezpracovan�ch ud�lost�
	while(m_pFirstToServer != NULL)
	{
		RemItem(m_pFirstToServer,m_pLastToServer);
	};
	while(m_pFirstToClient != NULL)
	{
		RemItem(m_pFirstToClient,m_pLastToClient);
	};
};

//////////////////////////////////////////////////////////////
// funkce pro server - Poseidon Editor

// vrac� po�et ud�lost� ve front�
int CMessageFront::GetServerMsgCount()
{
	return OnGetServerMsgCount();
};

// p�e�te ud�lost Serveru
BOOL CMessageFront::GetServerMessage(SPosMessage& sMsg)
{
	return OnGetServerMessage(sMsg);
};

// po�le ud�lost na Clienta
void CMessageFront::PostMessageToClient(SPosMessage& sMsg)
{
	AddItem(sMsg,m_pFirstToClient,m_pLastToClient);
	// zkus�m ud�lost vyslat
	TryToSendMessages();
};

// reset fronty ud�lost� pro client
void CMessageFront::ResetClientMessageFront()
{
	while(m_pFirstToClient != NULL)
	{
		RemItem(m_pFirstToClient,m_pLastToClient);
	};
	// reset memory bufferu
	OnResetClienFront();
};

// vrac� stav aplikace serveru
int	CMessageFront::GetServerAppState()
{	return OnGetServerAppState(); };

// nastav� stav aplikace serveru
void CMessageFront::SetServerAppState(int nState)
{	OnSetServerAppState(nState); };


//////////////////////////////////////////////////////////////
// funkce pro client - Poseidon View

// vrac� po�et ud�lost� ve front�
int	CMessageFront::GetClientMsgCount()
{
	return OnGetClientMsgCount();
};

// p�e�te ud�lost Clienta
BOOL CMessageFront::GetClientMessage(SPosMessage& sMsg)
{
	return OnGetClientMessage(sMsg);
};

// po�le ud�lost na Server
void CMessageFront::PostMessageToServer(SPosMessage& sMsg)
{
	AddItem(sMsg,m_pFirstToServer,m_pLastToServer);
	// zkus�m ud�lost vyslat
	TryToSendMessages();
};

// reset fronty ud�lost� pro server
void CMessageFront::ResetServerMessageFront()
{
	while(m_pFirstToServer != NULL)
	{
		RemItem(m_pFirstToServer,m_pLastToServer);
	};
	// reset memory bufferu
	OnResetServerFront();
};

// vrac� stav aplikace clienta
int	 CMessageFront::GetClientAppState()
{	return OnGetClientAppState(); };

// nastav� stav aplikace clienta
void CMessageFront::SetClientAppState(int nState)
{	OnSetClientAppState(nState); };

//////////////////////////////////////////////////////////////
// funkce pro komunikaci mezi aplikacemi
// funkce by mela b�t vol�na periodicky !!!

BOOL CMessageFront::TryToSendMessages()
{
	BOOL bOk = TRUE;
/*
	if (m_pFirstToServer != NULL)
	{	// zkus�m poslat ud�lost serveru
		if (OnAddServerMessage(m_pFirstToServer->sMsg))
		{	// poda�ilo se poslat ud�lost -> vyjmu ji
			RemItem(m_pFirstToServer,m_pLastToServer);
		}
	}
	if (m_pFirstToClient != NULL)
	{	// zkus�m poslat ud�lost clientu
		if (OnAddClientMessage(m_pFirstToClient->sMsg))
		{	// poda�ilo se poslat ud�lost -> vyjmu ji
			RemItem(m_pFirstToClient,m_pLastToClient);
		}
	}
*/
	while (m_pFirstToServer != NULL && OnAddServerMessage(m_pFirstToServer->sMsg))
	{	// zkus�m poslat ud�lost serveru
		RemItem(m_pFirstToServer,m_pLastToServer);
	}
	
	while (m_pFirstToClient != NULL && OnAddClientMessage(m_pFirstToClient->sMsg))
	{	// poda�ilo se poslat ud�lost -> vyjmu ji
		RemItem(m_pFirstToClient,m_pLastToClient);
	}

	return bOk;
};



//////////////////////////////////////////////////////////////
//	zpracov�n� fronty

void CMessageFront::AddItem(SPosMessage& sMsg,SFrontItem*& pFirst,SFrontItem*& pLast)
{
	SFrontItem*	pItem = new SFrontItem[1];
	if (pItem == NULL)
		return;
	// kopie dat
	pItem->sMsg = sMsg;
	pItem->pNext= NULL;

	// vlo��m prvek
	if (pLast == NULL)
	{
		pFirst	= pItem;
		pLast	= pItem;
	} else
	{
		pLast->pNext = pItem;
		pLast	= pItem;
	};
};

void CMessageFront::RemItem(SFrontItem*& pFirst,SFrontItem*& pLast)
{
	SFrontItem*	pItem = NULL;
	// vyjmu ud�lost
	if (pFirst != NULL)
	{
		pItem = pFirst;
		if (pItem->pNext == NULL)
		{
			pFirst	= NULL;
			pLast	= NULL;
		} else
		{
			pFirst	= (SFrontItem*)(pItem->pNext);
		};
	}
	// zpracuji prvek
	if (pItem == NULL)
		return;		// nebyla ��dn� ud�lost
	delete [] pItem;
};


