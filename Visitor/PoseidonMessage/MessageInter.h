/*********************************************************************/
/*																	 */
/*	Poseidon Messages 												 */
/*									Copyright � Mentar, 1998    	 */
/*																	 */
/*********************************************************************/

// komunika�n� knihovna pro editory Poseidon


//////////////////////////////////////////////////////////////
// struktura bufferu p�en�en�ch ud�lost�

typedef struct
{
	int			nCountItems;	// po�et prvk� v bufferu
	int			nFirstItem;		// prvn� prvek v bufferu
								// fronta ud�lost�
	int			nAppState;		// stav aplikace

	SPosMessage	sMessages[MSG_MEM_COUNT];

}	SMsgBuffer;


//////////////////////////////////////////////////////////////
// CMessageBuffer - t��da pro pr�ci s bufferem

class CMessageBuffer
{
public:
				CMessageBuffer();
	virtual		~CMessageBuffer();

	// inicializace bufferu
		void	InitBuffer(SMsgBuffer*	pBuffer,LPCTSTR pSemName);
		void	ResetBuffer();

	// uzam�en� a odem�en� bufferu
		void	LockBuffer();
		void	UnlockBuffer();

	// pr�ce s bufferem
		int		GetMsgCount();
		int		GetNewIndex();

	// pr�ce se stavem aplikace
		void	SetAppState(int nState);
		int		GetAppState();

	// p�e�ten� ud�losti
		BOOL	GetMessage(SPosMessage&);

	// vlo�en�  ud�losti
		BOOL	AddMessage(SPosMessage&);

// buffer ve sd�len� pam�ti
	SMsgBuffer*	m_pBuffer;
	HANDLE		m_hSemaphore;	// semafor k bufferu
};

//////////////////////////////////////////////////////////////
// pr�ce s frontou SERVERU

void OnInitServerFront(LPVOID lpvMem);
void OnResetServerFront();

// vrac� po�et ud�lost� ve front�
int  OnGetServerMsgCount();

// p�e�te ud�lost Serveru
BOOL OnGetServerMessage(SPosMessage&);

// vlo��  ud�lost Serveru
BOOL OnAddServerMessage(SPosMessage&);


// vrac� stav aplikace serveru
int	 OnGetServerAppState();
// nastav� stav aplikace serveru
void OnSetServerAppState(int nState);

//////////////////////////////////////////////////////////////
// pr�ce s frontou CLIENTA

void OnInitClientFront(LPVOID lpvMem);
void OnResetClienFront();

// vrac� po�et ud�lost� ve front�
int  OnGetClientMsgCount();

// p�e�te ud�lost Clienta
BOOL OnGetClientMessage(SPosMessage&);

// vlo��  ud�lost Clienta
BOOL OnAddClientMessage(SPosMessage&);

// vrac� stav aplikace clienta
int	 OnGetClientAppState();
// nastav� stav aplikace clienta
void OnSetClientAppState(int nState);
