/*********************************************************************/
/*																	 */
/*	Poseidon Messages 												 */
/*									Copyright � Mentar, 1998    	 */
/*																	 */
/*********************************************************************/

#include "windows.h"
#include "MessageDll.h"
#include "MessageInter.h"


/****************************************************************************
   FUNCTION: DllMain(HANDLE, DWORD, LPVOID)
*******************************************************************************/


#define SHMEMSIZE	(2*sizeof(SMsgBuffer))
static  LPVOID lpvMem = NULL;		// pointer to shared memory
static	HANDLE hMapObject = NULL;	// handle to file mapping

BOOL APIENTRY DllMain(HANDLE hInst, DWORD dwReason, LPVOID lpReserved)
{
	switch(dwReason)
	{
	case DLL_PROCESS_ATTACH:  
		{
			// Create a named file mapping object. 
			hMapObject = CreateFileMapping( 
				(HANDLE) 0xFFFFFFFF, // use paging file
				NULL,                // no security attributes
				PAGE_READWRITE,      // read/write access
				0,                   // size: high 32-bits
				SHMEMSIZE,           // size: low 32-bits
				"PosMessageMemMap"); // name of map object
			if (hMapObject == NULL)  
				return FALSE;  
			// The first process to attach initializes memory. 
			BOOL fInit = (GetLastError() != ERROR_ALREADY_EXISTS);  
			// Get a pointer to the file-mapped shared memory. 
			lpvMem = MapViewOfFile( 
				hMapObject,     // object to map view of
				FILE_MAP_WRITE, // read/write access
				0,              // high offset:  map from
				0,              // low offset:   beginning
				0);             // default: map entire file
			if (lpvMem == NULL)                 
				return FALSE;  
			// Initialize memory if this is the first process. 
			if (fInit)                 
			{
				memset(lpvMem, '\0', SHMEMSIZE);  
			};
			// inicializace front
			OnInitServerFront(lpvMem);
			OnInitClientFront(lpvMem);
		};	break;          
	case DLL_PROCESS_DETACH:  
		// Unmap shared memory from the process's address space. 
		UnmapViewOfFile(lpvMem);  
		// Close the process's handle to the file-mapping object. 
		CloseHandle(hMapObject);              
		break;  
	case DLL_THREAD_ATTACH:             
		break;  // The thread of the attached process terminates. 
	case DLL_THREAD_DETACH:             
		break;	// The DLL is unloading from a process due to 
				// process termination or a call to FreeLibrary.  
	default:
		break;      
	}      
	return TRUE; 
        UNREFERENCED_PARAMETER(hInst);
        UNREFERENCED_PARAMETER(lpReserved);
}


