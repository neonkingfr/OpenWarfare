/*********************************************************************/
/*																	 */
/*	Poseidon Messages 												 */
/*									Copyright � Mentar, 1998    	 */
/*																	 */
/*********************************************************************/

#include <windows.h>
#include "MessageDll.h"
#include "MessageInter.h"


//////////////////////////////////////////////////////////////
// CMessageBuffer

CMessageBuffer::CMessageBuffer()
{
	m_pBuffer		= NULL;
	m_hSemaphore	= NULL;
};

CMessageBuffer::~CMessageBuffer()
{
	if (m_hSemaphore != NULL)
	{
		CloseHandle(m_hSemaphore);
		m_hSemaphore = NULL;
	}
};

// inicializace bufferu
void CMessageBuffer::InitBuffer(SMsgBuffer*	pBuffer,LPCTSTR pSemName)
{
	m_pBuffer = pBuffer;

	// vytvo�en� semaforu
	m_hSemaphore = CreateSemaphore(NULL,   // no security attributes
								   1,1,pSemName);  
};

void CMessageBuffer::ResetBuffer()
{
	if (m_pBuffer != NULL)
	{
		LockBuffer();
		m_pBuffer->nCountItems = 0;
		UnlockBuffer();
	};
};

//////////////////////////////////////////////////////////////
// uzam�en� a odem�en� bufferu

void CMessageBuffer::LockBuffer()
{
	if (m_hSemaphore != NULL)
		WaitForSingleObject(m_hSemaphore,INFINITE);
};

void CMessageBuffer::UnlockBuffer()
{
	if (m_hSemaphore != NULL)
		ReleaseSemaphore(m_hSemaphore,1,NULL);
};

//////////////////////////////////////////////////////////////
// pr�ce s bufferem

int CMessageBuffer::GetMsgCount()
{
	int nCount = 0;
	if (m_pBuffer != NULL)
	{
		LockBuffer();
		nCount = m_pBuffer->nCountItems;
		UnlockBuffer();
	};
	return nCount;
};

int	CMessageBuffer::GetNewIndex()
{
	if (m_pBuffer->nCountItems == 0)
		return 0;	// hned na za��tek

	int nIndx = m_pBuffer->nFirstItem + m_pBuffer->nCountItems;
	if (nIndx >= MSG_MEM_COUNT)
		nIndx -= MSG_MEM_COUNT;

	return nIndx;
};

//////////////////////////////////////////////////////////////
// pr�ce se stavem aplikace

void CMessageBuffer::SetAppState(int nState)
{
	if (m_pBuffer != NULL)
	{
		LockBuffer();
		m_pBuffer->nAppState = nState;
		UnlockBuffer();
	};
};

int	 CMessageBuffer::GetAppState()
{
	int nState = APP_STATE_NOT_READY;
	if (m_pBuffer != NULL)
	{
		LockBuffer();
		nState = m_pBuffer->nAppState;
		UnlockBuffer();
	};
	return nState;
};

//////////////////////////////////////////////////////////////
// p�e�ten� ud�losti

BOOL CMessageBuffer::GetMessage(SPosMessage& sMsg)
{
	BOOL bRes = FALSE;
	if (m_pBuffer != NULL)
	{
		LockBuffer();
		if (m_pBuffer->nCountItems > 0)
		{	// p�e�tu ud�lost na nFirstItem
			sMsg = m_pBuffer->sMessages[m_pBuffer->nFirstItem];
			bRes = TRUE;
			// posun na dal�� ud�lost
			m_pBuffer->nCountItems--;

			if (m_pBuffer->nCountItems == 0)
				m_pBuffer->nFirstItem = 0;
			else
			{
				m_pBuffer->nFirstItem++;
				if (m_pBuffer->nFirstItem >= MSG_MEM_COUNT)
					m_pBuffer->nFirstItem =  0;
			}
		}
		UnlockBuffer();
	};
	return bRes;
};


//////////////////////////////////////////////////////////////
// vlo�en�  ud�losti

BOOL CMessageBuffer::AddMessage(SPosMessage& sMsg)
{
	BOOL bRes = FALSE;
	if (m_pBuffer != NULL)
	{
		LockBuffer();
		if (m_pBuffer->nCountItems < MSG_MEM_COUNT)
		{	// je�t� se vejde do bufferu
			int  nIndx = GetNewIndex();
			// vlo��m ud�lost
			m_pBuffer->sMessages[nIndx] = sMsg;
			m_pBuffer->nCountItems++;
			bRes = TRUE;
		}
		UnlockBuffer();
	};
	return bRes;
};






