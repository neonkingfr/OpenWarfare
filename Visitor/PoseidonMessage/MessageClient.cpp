/*********************************************************************/
/*																	 */
/*	Poseidon Messages 												 */
/*									Copyright � Mentar, 1998    	 */
/*																	 */
/*********************************************************************/

#include <windows.h>
#include "MessageDll.h"
#include "MessageInter.h"

//////////////////////////////////////////////////////////////
// fronta - CLIENT

class	CMessageBuffer	ClientFront;

void OnInitClientFront(LPVOID lpvMem)
{
	// inicializace fronty
	if (ClientFront.m_pBuffer == NULL)
	{
		ClientFront.InitBuffer(&(((SMsgBuffer*)lpvMem)[1]),"PosMsgClientSem");
	};
};

void OnResetClienFront()
{
	ClientFront.ResetBuffer();
};

// vrac� po�et ud�lost� ve front�
int  OnGetClientMsgCount()
{
	return ClientFront.GetMsgCount();
};

// p�e�te ud�lost Clienta
BOOL OnGetClientMessage(SPosMessage& sMsg)
{
	return ClientFront.GetMessage(sMsg);
};

// vlo��  ud�lost Clienta
BOOL OnAddClientMessage(SPosMessage& sMsg)
{
	return ClientFront.AddMessage(sMsg);
};

// vrac� stav aplikace clienta
int	 OnGetClientAppState()
{	return ClientFront.GetAppState(); }

// nastav� stav aplikace clienta
void OnSetClientAppState(int nState)
{	ClientFront.SetAppState(nState); };



