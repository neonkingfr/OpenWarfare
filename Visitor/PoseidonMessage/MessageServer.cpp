/*********************************************************************/
/*																	 */
/*	Poseidon Messages 												 */
/*									Copyright � Mentar, 1998    	 */
/*																	 */
/*********************************************************************/

#include "windows.h"
#include "MessageDll.h"
#include "MessageInter.h"


//////////////////////////////////////////////////////////////
// fronta - SERVER

class	CMessageBuffer	ServerFront;

void OnInitServerFront(LPVOID lpvMem)
{
	// inicializace fronty
	if (ServerFront.m_pBuffer == NULL)
	{
		ServerFront.InitBuffer(&(((SMsgBuffer*)lpvMem)[0]),"PosMsgServerSem");
	};
};

void OnResetServerFront()
{
	ServerFront.ResetBuffer();
};


// vrac� po�et ud�lost� ve front�
int  OnGetServerMsgCount()
{
	return ServerFront.GetMsgCount();
};

// p�e�te ud�lost Serveru
BOOL OnGetServerMessage(SPosMessage& sMsg)
{
	return ServerFront.GetMessage(sMsg);
};

// vlo��  ud�lost Serveru
BOOL OnAddServerMessage(SPosMessage& sMsg)
{
	return ServerFront.AddMessage(sMsg);
};

// vrac� stav aplikace serveru
int	 OnGetServerAppState()
{	return ServerFront.GetAppState(); }

// nastav� stav aplikace serveru
void OnSetServerAppState(int nState)
{	ServerFront.SetAppState(nState); };
