/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPosNetObjectBase - z�kladn� objekt s�t�

IMPLEMENT_DYNAMIC(CPosNetObjectBase, CPosObject)

CPosNetObjectBase::CPosNetObjectBase()
{
	m_nNetID = UNDEF_REG_ID;
	// z�kladn� poloha
	InitRealNetPos(m_nBaseRealPos);
}

CPosNetObjectBase::CPosNetObjectBase(const CPoseidonMap& map) 
: base(map)
{
	m_nNetID = UNDEF_REG_ID;
	// z�kladn� poloha
	InitRealNetPos(m_nBaseRealPos);
}

// vytvo�en� kopie objektu
void CPosNetObjectBase::CopyFrom(const CPosObject* pSrc)
{
	ASSERT_KINDOF(CPosNetObjectBase, pSrc);
	base::CopyFrom(pSrc);

	m_nNetID = ((CPosNetObjectBase*)pSrc)->m_nNetID;
	m_nBaseRealPos = ((CPosNetObjectBase*)pSrc)->m_nBaseRealPos;

	//m_rGlobalHull	= ((CPosNetObjectBase*)pSrc)->m_rGlobalHull;
	//m_rGlobalHullDP	= ((CPosNetObjectBase*)pSrc)->m_rGlobalHullDP;
}

// serializace
void CPosNetObjectBase::DoSerialize(CArchive& ar, DWORD nVer)
{
	base::DoSerialize(ar,nVer);
	MntSerializeInt(ar, m_nNetID);
	MntSerializeNetPos(ar, nVer, m_nBaseRealPos);
}

// informace o poloze objektu - nen� ur�ena
/*CPositionBase* CPosNetObjectBase::GetLogObjectPosition()
{	ASSERT(FALSE);
	return NULL;
};*/


BOOL CPosNetObjectBase::IsSameObject(const CPosObject& obj) const
{
	if (!obj.IsKindOf(RUNTIME_CLASS(CPosNetObjectBase))) return FALSE;

	return (((const CPosNetObjectBase&) obj).m_nNetID == m_nNetID);
}

// informace o objektu
BOOL CPosNetObjectBase::IsEnabledObject(const EnabledObjects& enabled) const
{	
	return enabled.m_bNets && !_hidden; 
}

/*void CPosNetObjectBase::GetObjectHull(CRect& rHull) const
{	rHull = m_rGlobalHull; };*/

// pr�ce se sou�adnicemi objektu

void CPosNetObjectBase::DoUpdateNetPartsBaseRealPos(REALNETPOS nPos)
{
	m_nBaseRealPos = nPos;
	DoUpdateNetPartsBaseRealPos();
}

/*void CPosNetObjectBase::RecalcDevObjectPositionFromLP(CDC* pDC)
{	// p�epo��t�m DP HULL
	m_rGlobalHullDP = m_rGlobalHull;
	pDC->LPtoDP(m_rGlobalHullDP);
};*/

int CPosNetObjectBase::SetFreeID()
{
	return m_nNetID = _map->GetFreeNetID();
}

/////////////////////////////////////////////////////////////////////////////
// CPosNetObjectNor - norm�ln� �sek s�t�

IMPLEMENT_DYNCREATE(CPosNetObjectNor, CPosNetObjectBase)

CPosNetObjectNor::CPosNetObjectNor()
{
	m_LogPosition = new CPositionGroup();
}

CPosNetObjectNor::CPosNetObjectNor(const CPoseidonMap& map) 
: base(map)
{
	m_LogPosition = new CPositionGroup();
}

CPosNetObjectNor::~CPosNetObjectNor()
{	
	Destroy(); 
}

void CPosNetObjectNor::Destroy()
{	
	// zru�en� �seku
	m_PartList.Destroy();
}

// vytvo�en� kopie objektu
void CPosNetObjectNor::CopyFrom(const CPosObject* pSrc)
{	
	// zru�en� dat
	Destroy();
	// z�kladn� data
	CPosNetObjectBase::CopyFrom(pSrc);
	// vlastn� data
	ASSERT_KINDOF(CPosNetObjectNor, pSrc);
	m_PartList.CopyFrom(&((CPosNetObjectNor*)pSrc)->m_PartList);
}

// vytvo�en� �seku podle s�t�
BOOL CPosNetObjectNor::CreateFromNetTemplate(const CPosNetTemplate* pNet)
{
	ASSERT(pNet != NULL);
	if (pNet != NULL)
	{
		// vytvo�en� listu podle s�t�
		m_PartList.CreateFromNetTemplate(pNet);
	}
	return TRUE;
}

// update registrovan�ch objekt� podle definovan�ch
void CPosNetObjectNor::UpdateNetsObjectsFromDefObjects(const CMgrNets* pMgr)
{
	m_PartList.UpdateNetsObjectsFromDefObjects(pMgr);
}

// serializace
void CPosNetObjectNor::DoSerialize(CArchive& ar, DWORD nVer)
{
	// z�kladn� data
	CPosNetObjectBase::DoSerialize(ar, nVer);
	// vlastn� data
	m_PartList.DoSerialize(ar, nVer);
}

void CPosNetObjectNor::DoUpdateFromTemplate(const CPoseidonMap* map)
{
	m_PartList.DoUpdateFromTemplate(map);
}

// informace o objektu
static CString sCPosNetObjectNorName;

void CPosNetObjectNor::GetObjectName(CString& sText) const
{
	if (sCPosNetObjectNorName.GetLength() == 0) VERIFY(sCPosNetObjectNorName.LoadString(IDS_BASE_NET_OBJ_NAME));
	sText = sCPosNetObjectNorName;
}

/*void CPosNetObjectNor::DoUpdateHull()
{	// hull �seku
	m_PartList.GetObjectHull(m_rGlobalHull);
};*/
/*BOOL CPosNetObjectNor::IsPointAtArea(const POINT& pt) const
{
	for(int ob = 0; ob < m_PartList.m_NetParts.GetSize(); ob++)
	{
		CNetPartBase* pPart = (CNetPartBase*)(m_PartList.m_NetParts[ob]);
		if (pPart)
		{
			if (pPart->IsPointAtArea(pt))
				return TRUE;
		}
	}
	return FALSE;
};*/

// pr�ce s objekty Poseidon (Update a Vytvo�en�)
BOOL CPosNetObjectNor::DoUpdatePoseidonNetObjects(CObArray* ExistObjs,const CPoseidonMap* pMap)
{	
	return m_PartList.DoUpdatePoseidonNetObjects(ExistObjs, pMap); 
}

BOOL CPosNetObjectNor::DoCreatePoseidonNetObjects(CObArray* CreateObjs,const CPoseidonMap* pMap)
{	
	return m_PartList.DoCreatePoseidonNetObjects(CreateObjs, pMap); 
}

// pr�ce se sou�adnicemi objektu
void CPosNetObjectNor::DoUpdateNetPartsBaseRealPos()
{	
	// m_nBaseRealPos - p�edstavuje po��te�n� bod �seku
	// provedu update �seku a to je v�e
	m_PartList.DoUpdateNetPartsBaseRealPos(m_nBaseRealPos, _map);
}

void CPosNetObjectNor::DoUpdateNetPartsLogPosition()
{	
	// provedu update logick�ch sou�adnic v �seku 
	m_PartList.DoUpdateLogPosition(_map);
	// obnova glob�ln�ho obalu
	//DoUpdateHull();

	// update logick� polohy
	GetLogObjectPosition()->ResetPosition();
	// poloha �sek�
	for (int ob = 0; ob < m_PartList.m_NetParts.GetSize(); ++ob)
	{
		CNetPartBase* pPart = (CNetPartBase*)(m_PartList.m_NetParts[ob]);
		if (pPart)
		{
			GetLogObjectPosition()->DoAddNewPos(pPart->GetLogObjectPosition());
		}
	}
	GetLogObjectPosition()->RecalcPositionHull();
}

void CPosNetObjectNor::RecalcDevObjectPositionFromLP(CDC* pDC)
{	
	m_PartList.RecalcDevObjectPositionFromLP(pDC);
}

void CPosNetObjectNor::ResetDevObjectPosition()
{
	m_PartList.ResetDevObjectPosition();
}

// vykreslov�n� objektu
void CPosNetObjectNor::OnDrawObject(CDC* pDC, const CDrawPar* pParams) const
{
	// vykreslen� seznamu
	m_PartList.OnDrawObject(pDC, pParams, pParams->m_VwStyle.m_ShowLock && Locked());
}

// informace o poloze objektu - nen� ur�ena

void CPosNetObjectNor::CalcObjectTrackerPosition(CPositionTracker& Pos)
{
	for (int ob = 0; ob < m_PartList.m_NetParts.GetSize(); ++ob)
	{
		CNetPartBase* pPart = (CNetPartBase*)(m_PartList.m_NetParts[ob]);
		if (pPart)
		{
			Pos.DoAddNewPos(pPart->GetLogObjectPosition());
		}
	}
	// spo��t�m st�ed
	Pos.CalcAutoPositionCenter();
}

/*bool CPosNetObjectNor::IsIntersection(const CPosObject& second) const
{  
  return m_PartList.IsIntersection(second);  
}*/

/////////////////////////////////////////////////////////////////////////////
// CPosNetObjectKey - kl��ov� bod s�t�

IMPLEMENT_DYNCREATE(CPosNetObjectKey, CPosNetObjectBase)

CPosNetObjectKey::CPosNetObjectKey(const CPoseidonMap& map) 
: base(map)
{	
	m_pBasePartKEY = NULL;
	m_LogPosition = new CPositionGroup();
}

CPosNetObjectKey::CPosNetObjectKey() 
: base()
{	
	m_pBasePartKEY = NULL;
	m_LogPosition = new CPositionGroup();

}

CPosNetObjectKey::~CPosNetObjectKey()
{	
	Destroy(); 
}

void CPosNetObjectKey::Destroy()
{
	if (m_pBasePartKEY != NULL)
	{
		delete m_pBasePartKEY;
		m_pBasePartKEY = NULL;
	}
	// likvidace seznamu list�
	DestroyArrayObjects(&m_PartLists);
}

// vytvo�en� kopie objektu
void CPosNetObjectKey::CopyFrom(const CPosObject* pSrc)
{	
	// zru�en� dat
	Destroy();
	// z�kladn� data
	CPosNetObjectBase::CopyFrom(pSrc);
	// vlastn� data
	ASSERT_KINDOF(CPosNetObjectKey, pSrc);
	ASSERT(m_pBasePartKEY == NULL);
	if (((CPosNetObjectKey*)pSrc)->m_pBasePartKEY != NULL) m_pBasePartKEY = (CNetPartKEY*)(((CPosNetObjectKey*)pSrc)->m_pBasePartKEY->CreateCopyObject());
	// kopie seznamu list�
	for (int i = 0; i < ((CPosNetObjectKey*)pSrc)->m_PartLists.GetSize(); ++i)
	{
		CNetPartsList* pOld = (CNetPartsList*)(((CPosNetObjectKey*)pSrc)->m_PartLists[i]);
		if (pOld)
		{
			CNetPartsList* pNew = new CNetPartsList();
			if (pNew)
			{
				m_PartLists.Add(pNew);
				pNew->CopyFrom(pOld);
			}
		}
	}	
}

// update registrovan�ch objekt� podle definovan�ch
void CPosNetObjectKey::UpdateNetsObjectsFromDefObjects(const CMgrNets* pMgr)
{
	ASSERT(m_pBasePartKEY != NULL);
	if (m_pBasePartKEY == NULL) return;
	// update kl��ov�ho bodu
	m_pBasePartKEY->UpdateNetsObjectsFromDefObjects(pMgr, NULL);
	for (int i = 0; i < m_pBasePartKEY->m_nCountSections; ++i)
	{
		CNetPartsList* pList = (CNetPartsList*)(m_PartLists[i]);
		if (pList) pList->UpdateNetsObjectsFromDefObjects(pMgr);
	}
}

// serializace
void CPosNetObjectKey::DoSerialize(CArchive& ar, DWORD nVer)
{
	// z�kladn� data
	CPosNetObjectBase::DoSerialize(ar, nVer);
	// vlastn� data
	BOOL bExist = (m_pBasePartKEY != NULL);
	MntSerializeBOOL(ar, bExist);
	if (bExist)
	{
		if (ar.IsStoring())
		{
			ASSERT(m_pBasePartKEY != NULL);
		} 
		else
		{
			ASSERT(m_pBasePartKEY == NULL);
			m_pBasePartKEY = new CNetPartKEY();
		}
		m_pBasePartKEY->DoSerialize(ar, nVer);
	}
	// serializace list�
	int nCount = m_PartLists.GetSize();
	MntSerializeInt(ar, nCount);
	for (int i = 0; i < nCount; ++i)
	{
		CNetPartsList* pList = NULL;
		if (ar.IsStoring())
		{
			pList = (CNetPartsList*)(m_PartLists[i]);
		} 
		else
		{
			pList = new CNetPartsList();
			m_PartLists.Add(pList);
		}
		if (pList) pList->DoSerialize(ar, nVer);
	}
}

void CPosNetObjectKey::DoUpdateFromTemplate(const CPoseidonMap* map)
{
	if (m_pBasePartKEY != NULL) m_pBasePartKEY->DoUpdateFromTemplate(map);

	int nCount = m_PartLists.GetSize();
	for (int i = 0; i < nCount; ++i)
	{
		CNetPartsList* pList = (CNetPartsList*)(m_PartLists[i]);
		pList->DoUpdateFromTemplate(map);
	}
}

// informace o objektu
static CString sCPosNetObjectKeyName;

void CPosNetObjectKey::GetObjectName(CString& sText) const
{
	if (sCPosNetObjectKeyName.GetLength() == 0) VERIFY(sCPosNetObjectKeyName.LoadString(IDS_KEY_NET_OBJ_NAME));
	sText = sCPosNetObjectKeyName;
}

/*void CPosNetObjectKey::DoUpdateHull()
{	// obnova glob�ln�ho obalu
	ASSERT(m_pBasePartKEY != NULL);
	if (m_pBasePartKEY == NULL) return;
	// z�klad je obal kl��ov�ho objektu
	m_rGlobalHull = m_pBasePartKEY->GetLogObjectPosition()->GetPositionHull();
	// obal dal��ch objekt�	
	for(int i=0; i<m_pBasePartKEY->m_nCountSections; i++)
	{
		// nastav�m seznamu
		CNetPartsList* pList = (CNetPartsList*)(m_PartLists[i]);
		if ((pList)&&(pList->m_NetParts.GetSize()>0))
		{
			CRect rHull; pList->GetObjectHull(rHull);
			if (m_rGlobalHull.left   > rHull.left)
				m_rGlobalHull.left   = rHull.left;
			if (m_rGlobalHull.right  < rHull.right)
				m_rGlobalHull.right  = rHull.right;
			if (m_rGlobalHull.top    > rHull.top)
				m_rGlobalHull.top    = rHull.top;
			if (m_rGlobalHull.bottom < rHull.bottom)
				m_rGlobalHull.bottom = rHull.bottom;
		}
	}
};*/
/*BOOL CPosNetObjectKey::IsPointAtArea(const POINT& pt) const
{
	ASSERT(m_pBasePartKEY != NULL);
	if (m_pBasePartKEY == NULL) return FALSE;
	// je na kl��ov�m objektu
	if (m_pBasePartKEY->IsPointAtArea(pt))
		return TRUE;
	// poloha �sek�
	for(int i=0; i<m_pBasePartKEY->m_nCountSections; i++)
	{
		CNetPartsList* pList = (CNetPartsList*)(m_PartLists[i]);
		if (pList)
		{
			for(int ob = 0; ob < pList->m_NetParts.GetSize(); ob++)
			{
				CNetPartBase* pPart = (CNetPartBase*)(pList->m_NetParts[ob]);
				if (pPart)
				{
					if (pPart->IsPointAtArea(pt))
						return TRUE;
				}
			}
		}
	}
	return FALSE;
};*/

// vytvo�en� kl��ov�ho objektu
BOOL CPosNetObjectKey::CreateFromNetKeyPart(CNetPartKEY* pKeyObj, CMgrNets* pMgr)
{
	ASSERT(pKeyObj != NULL);
	ASSERT_KINDOF(CNetPartKEY, pKeyObj);

	m_nNetID = _map->GetFreeNetID();
	// v�m�na objekt�
	if (m_pBasePartKEY != NULL) delete m_pBasePartKEY;
	m_pBasePartKEY = pKeyObj;
	// sou�adnice podle kl��ov�ho bodu
	m_nBaseRealPos = m_pBasePartKEY->m_nBaseRealPos;
	// vytvo��m odpov�daj�c� �seky
	for (int i = 0; i < pKeyObj->m_nCountSections; ++i)
	{
		CPosNetTemplate* pNet = pMgr->GetNetTemplate(pKeyObj->m_SectionNetType[i]);
		ASSERT(pNet != NULL);
		if (pNet != NULL)
		{
			// nov� list
			CNetPartsList* pList = new CNetPartsList();
			m_PartLists.Add(pList);
			// vytvo�en� listu podle s�t�
			pList->CreateFromNetTemplate(pNet);
		}
	}

	return TRUE;
}

// pr�ce s objekty Poseidon (Update a Vytvo�en�)
BOOL CPosNetObjectKey::DoUpdatePoseidonNetObjects(CObArray* ExistObjs, const CPoseidonMap* pMap)
{
	ASSERT(m_pBasePartKEY != NULL);
	if (m_pBasePartKEY == NULL) return FALSE;
	// z�kladn� objekt
	if (!m_pBasePartKEY->DoUpdatePoseidonNetObjects(ExistObjs, pMap)) return FALSE;
	// �seky
	for (int i = 0; i < m_pBasePartKEY->m_nCountSections; ++i)
	{
		CNetPartsList* pList = (CNetPartsList*)(m_PartLists[i]);
		if (pList)
		{
			if (!pList->DoUpdatePoseidonNetObjects(ExistObjs, pMap)) return FALSE;
		}
	}
	return TRUE;
}

BOOL CPosNetObjectKey::DoCreatePoseidonNetObjects(CObArray* CreateObjs, const CPoseidonMap* pMap)
{
	ASSERT(m_pBasePartKEY != NULL);
	if (m_pBasePartKEY == NULL) return FALSE;
	// z�kladn� objekt
	if (!m_pBasePartKEY->DoCreatePoseidonNetObject(CreateObjs, pMap)) return FALSE;
	// �seky
	for (int i = 0; i < m_pBasePartKEY->m_nCountSections; ++i)
	{
		CNetPartsList* pList = (CNetPartsList*)(m_PartLists[i]);
		if (pList)
		{
			if (!pList->DoCreatePoseidonNetObjects(CreateObjs, pMap)) return FALSE;
		}
	}
	return TRUE;
}

// pr�ce se sou�adnicemi objektu
void CPosNetObjectKey::DoUpdateNetPartsBaseRealPos()
{
	ASSERT(m_pBasePartKEY != NULL);
	if (m_pBasePartKEY == NULL) return;
	// update absolutn� v��ky podle krajiny
	m_nBaseRealPos.nHgt = _map->GetLandscapeHeight(m_nBaseRealPos.nPos);

	//m_nBaseRealPos.nHgt+= (float)m_pBasePartKEY->m_nRelaHeight;

	// kl��ov� bod z�sk� z�kladn� sou�adnice
	m_pBasePartKEY->m_nBaseRealPos = m_nBaseRealPos;
	// spo��t�m po��te�n� bod ka�d�ho �seku
	// a nastav�m polohu v�ech objekt� v �seku
	for (int i = 0; i < m_pBasePartKEY->m_nCountSections; ++i)
	{
		REALNETPOS rBasePos = m_pBasePartKEY->GetRealPosForSection(i, _map);
		// nastav�m seznamu
		CNetPartsList* pList = (CNetPartsList*)(m_PartLists[i]);
		if (pList) pList->DoUpdateNetPartsBaseRealPos(rBasePos, _map);
	}
}

void CPosNetObjectKey::DoUpdateNetPartsLogPosition()
{
	ASSERT(m_pBasePartKEY != NULL);
	if (m_pBasePartKEY == NULL) return;
	// update sou�adnic kl��ov�ho objektu
	m_pBasePartKEY->DoUpdateLogPosition(_map);
	// update sou�adnic ve v�ech �sec�ch !
	int nCount = m_PartLists.GetSize();
	for (int i = 0; i < nCount; ++i)
	{
		CNetPartsList* pList = (CNetPartsList*)(m_PartLists[i]);
		if (pList) pList->DoUpdateLogPosition(_map);
	}
	// obnova glob�ln�ho obalu
	//DoUpdateHull();

	// update logick� polohy
	GetLogObjectPosition()->ResetPosition();
	GetLogObjectPosition()->DoAddNewPos(m_pBasePartKEY->GetLogObjectPosition()); 
	// poloha �sek�
	for (int i = 0; i < m_pBasePartKEY->m_nCountSections; ++i)
	{
		CNetPartsList* pList = (CNetPartsList*)(m_PartLists[i]);
		if (pList)
		{
			for (int ob = 0; ob < pList->m_NetParts.GetSize(); ++ob)
			{
				CNetPartBase* pPart = (CNetPartBase*)(pList->m_NetParts[ob]);
				if (pPart)
				{
					GetLogObjectPosition()->DoAddNewPos(pPart->GetLogObjectPosition());
				}
			}
		}
	}
	GetLogObjectPosition()->RecalcPositionHull();
}

void CPosNetObjectKey::RecalcDevObjectPositionFromLP(CDC* pDC)
{	
	ASSERT(m_pBasePartKEY != NULL);
	if (m_pBasePartKEY == NULL) return;
	// p�epo��t�n� sou�adnic z�kladn�ho objektu
	m_pBasePartKEY->RecalcDevObjectPositionFromLP(pDC);

	for (int i = 0; i < m_pBasePartKEY->m_nCountSections; ++i)
	{
		CNetPartsList* pList = (CNetPartsList*)(m_PartLists[i]);
		if (pList) pList->RecalcDevObjectPositionFromLP(pDC);
	}
}

void CPosNetObjectKey::ResetDevObjectPosition()
{
	ASSERT(m_pBasePartKEY != NULL);
	if (m_pBasePartKEY == NULL) return;
	// reset kl��ov�ho bodu
	((CPositionBase*)(m_pBasePartKEY->GetDevObjectPosition()))->ResetPosition();
	for (int i = 0; i < m_pBasePartKEY->m_nCountSections; ++i)
	{
		CNetPartsList* pList = (CNetPartsList*)(m_PartLists[i]);
		if (pList) pList->ResetDevObjectPosition();
	}
}

void CPosNetObjectKey::CalcObjectTrackerPosition(CPositionTracker& Pos)
{
	ASSERT(m_pBasePartKEY != NULL);
	if (m_pBasePartKEY == NULL) return;
	// poloha kl��ov�ho bodu
	Pos.DoAddNewPos(m_pBasePartKEY->GetLogObjectPosition()); 
	// poloha �sek�
	for (int i = 0; i < m_pBasePartKEY->m_nCountSections; ++i)
	{
		CNetPartsList* pList = (CNetPartsList*)(m_PartLists[i]);
		if (pList)
		{
			for (int ob = 0; ob < pList->m_NetParts.GetSize(); ++ob)
			{
				CNetPartBase* pPart = (CNetPartBase*)(pList->m_NetParts[ob]);
				if (pPart)
				{
					Pos.DoAddNewPos(pPart->GetLogObjectPosition());
				}
			}
		}
	}
	// spo��t�m st�ed
	Pos.CalcAutoPositionCenter();
}

// vykreslov�n� objektu
void CPosNetObjectKey::OnDrawObject(CDC* pDC, const CDrawPar* pParams) const
{
	ASSERT(m_pBasePartKEY != NULL);
	if (m_pBasePartKEY == NULL) return;
	// vykreslen� kl��ov�ho bodu
	m_pBasePartKEY->OnDrawObject(pDC, pParams, pParams->m_VwStyle.m_ShowLock && Locked());
	// vykreslen� seznam�
	for (int i = 0; i < m_pBasePartKEY->m_nCountSections; ++i)
	{
		CNetPartsList* pList = (CNetPartsList*)(m_PartLists[i]);
		if (pList) pList->OnDrawObject(pDC, pParams, pParams->m_VwStyle.m_ShowLock && Locked());
	}
}

// pro Section vrac� odpov�daj�c� polohu (m_nBaseRealPos seznamu)
REALNETPOS CPosNetObjectKey::GetRealPosForSection(int nSection, const CPoseidonMap* pMap) const
{
	if ((nSection >= 0) && (nSection <= m_pBasePartKEY->m_nCountSections))
	{
		CNetPartsList* pList = (CNetPartsList*)(m_PartLists[nSection]);
		if (pList)
		{
			return pList->GetNetPartsEndRealPos();
		}
	}
	return m_pBasePartKEY->GetRealPosForSection(nSection, pMap);
}

bool CPosNetObjectKey::PrepareForPaste(const CPosEdBaseDoc& doc) 
{ 
	for (int i = 0; i < m_pBasePartKEY->m_nCountSections; ++i)
	{
		CNetPartsList* pList = (CNetPartsList*)(m_PartLists[i]);
		if (pList)
		{
			if (!pList->PrepareForPaste(doc)) return false;
		}
	}

	return m_pBasePartKEY->PrepareForPaste(doc);
}

/*bool CPosNetObjectKey::IsIntersection(const CPosObject& second) const
{
  CRect hull;
  GetObjectHull(hull);
  CRect objHull;
  second.GetObjectHull(objHull);

  if (!IsIntersectRect(hull, objHull))
    return false;

  if (m_pBasePartKEY && m_pBasePartKEY->IsIntersection(second))
    return true;
  for(int i = 0; i < m_PartLists.GetSize(); i++)
  {
    CNetPartsList* pList = (CNetPartsList*)(m_PartLists[i]);
    if (pList && pList->IsIntersection(second))
      return true;
  }

  return false;  
}*/

// walk on road
AutoArray<NetWalkPoint> CPosNetObjectKey::GetWalkPoints() const
{
	AutoArray<NetWalkPoint> ret;
	ret.Realloc(m_PartLists.GetSize(), m_PartLists.GetSize());

	for (int i = 0; i < m_PartLists.GetSize(); ++i)
	{
		CNetPartsList* pList = (CNetPartsList*)(m_PartLists[i]);

		NetWalkPoint pt ;
		if (pList->GetWalkPoint(pt))
		{
			pt._dirIndx = i;
			ret.Add(pt);              
		}   
	}

	return ret;
}

bool CPosNetObjectKey::AdvanceWalkPoint(NetWalkPoint& pt, float dist) const
{
	ASSERT(pt._dirIndx < m_PartLists.GetSize());
	CNetPartsList* pList = (CNetPartsList*)(m_PartLists[pt._dirIndx]);

	return pList->AdvanceWalkPoint(pt, dist);
}