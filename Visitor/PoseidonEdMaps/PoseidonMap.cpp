/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"
#include "math.h"
#include <Landscape/multiPassTex/multiPassMat.hpp>
#include <El/ParamArchive/paramArchiveDB.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
//	z�kladn� objekt pro mapu poseidon

IMPLEMENT_DYNAMIC(CPoseidonMap, CObject)


CPoseidonMap::CPoseidonMap()
{
	// prost�ed� aplikace obsahuje d�le�it� konstanty
	m_pEnvironment = GetPosEdEnvironment();
	ASSERT(m_pEnvironment != NULL);
	// napojen� na dokument
	m_pDocument	   = NULL;
	
	// inicializace prom�nn�ch
	m_pLandHeight	= NULL;

	// reset prom�nn�ch
	DestroyMap();
};

CPoseidonMap::~CPoseidonMap()
{	// likvidace mapy
	DestroyMap();
};

// likvidace struktur mapy
void CPoseidonMap::DestroyMap()
{
	// nem� ��dn� rozm�r
	m_nWidth	= 
	m_nHeight	= 0;
  
	// likvidace prom�nn�ch
	if (m_pLandHeight)	delete [] m_pLandHeight;
	m_pLandHeight = NULL;
	
	m_nMaxHeight  = 0.f;
	m_nMinHeight  = 0.f;

	// likvidace objekt�
	DestroyArrayObjects(&m_InstanceObjs);
  m_ObjectIDs.Init(); 
  m_WoodIDs.Init();
  m_NetIDs.Init();
  m_AreaIDs.Init();
  m_KeyPtIDs.Init();
	
	// likvidace textur
	m_ActualTextureLayer = NULL;
  m_TextureLayers.Clear();

	// likvidace registru textur
	//DestroyArrayObjects(&m_RegisterTexts);
	//m_RegTextsMap.RemoveAll();

	// likvidace les�, s�t� ... 
	DestroyArrayObjects(&m_WoodsInMap);
	DestroyArrayObjects(&m_NetsInMap);
	DestroyArrayObjects(&m_AreasInMap);
	DestroyArrayObjects(&m_KeyPtInMap);
  for(int i = 0; i < m_BGImagesInMap.Size(); i ++)
  {
    delete m_BGImagesInMap[i];
  }
  m_BGImagesInMap.Clear();

};


/////////////////////////////////////////////////////////////////////////////
// vytv��en� a serializace mapy

BOOL  CPoseidonMap::NewTerrain(int nWidth, int nHeight)
{
  if (m_pLandHeight)	delete [] m_pLandHeight;
  m_nWidth	= nWidth;
  m_nHeight	= nHeight;
  // dimeze pol�v v jednotkov�ch sou�adnic�ch
  int nLandDim = m_nWidth * m_nHeight;
  if ((m_pLandHeight = new LANDHEIGHT[nLandDim])==NULL)
    AfxThrowMemoryException();		
  memset(m_pLandHeight,0,sizeof(LANDHEIGHT)*nLandDim);
  m_landHeightLocked.Clear();
  m_landHeightLocked.Alloc(nLandDim);
  m_landHeightLocked.Neutralize();
  for (int i=0;i<m_TextureLayers.Size();i++)
    m_TextureLayers[i]->ResizeLayer(m_nWidth,m_nHeight);
  return TRUE;
}
BOOL CPoseidonMap::OnNewMap(int nWidth, int nHeight, int nTexSizeInSqLog)
{
	// likvidace mapy, kdyby existovala
	DestroyMap();
	// vytvo�en� nov� mapy
	TRY
	{
		// p�i�ad�m rozm�ry
		m_nWidth	= nWidth;
		m_nHeight	= nHeight;
   
		// dimeze pol�v v jednotkov�ch sou�adnic�ch
		int nLandDim = m_nWidth * m_nHeight;

#ifdef _LIMIT_LAYERSIZE
    //prevent TextureLayerSize >1024
    int minWidth = nWidth;
    while(minWidth>1024)
    {
      nTexSizeInSqLog++;
      minWidth >>= 1;
    }
#endif

    // pole pro v��ku krajiny
		if ((m_pLandHeight = new LANDHEIGHT[nLandDim])==NULL)
			AfxThrowMemoryException();		

    m_ActualTextureLayer = new CTextureLayer(*this);
    if (!m_ActualTextureLayer->OnNewLayer(nWidth, nHeight, nTexSizeInSqLog, CString(_T("Base"))))
      AfxThrowMemoryException();

    m_ActualTextureLayer->Activate();

    m_TextureLayers.Add(m_ActualTextureLayer);
    m_landHeightLocked.Alloc(nLandDim);
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		// likvidace nepoveden� mapy
		DestroyMap();
		return FALSE;
	}
	END_CATCH_ALL
	// inicializace hodnot
	OnInitMap();
	return TRUE;
};

void CPoseidonMap::OnInitMap()
{
	int nLandDim = m_nWidth * m_nHeight;

	// inicializace v��ky krajiny
	if (m_pLandHeight)
	{
		for(int i=0; i<nLandDim; i++)
			m_pLandHeight[i] = 0.f;
	}

  m_landHeightLocked.Neutralize();

	// likvidace objekt�
	DestroyArrayObjects(&m_InstanceObjs);
  m_ObjectIDs.Init(); 
  m_WoodIDs.Init();
  m_NetIDs.Init();
  m_AreaIDs.Init();
  m_KeyPtIDs.Init();

	// inicializace textur  
  m_TextureLayers.Clear();
  m_ActualTextureLayer->OnInitLayer();
  m_TextureLayers.Add(m_ActualTextureLayer);	

	// likvidace les�
	DestroyArrayObjects(&m_WoodsInMap);
	DestroyArrayObjects(&m_NetsInMap);
	DestroyArrayObjects(&m_AreasInMap);
	DestroyArrayObjects(&m_KeyPtInMap);
  for(int i = 0; i < m_BGImagesInMap.Size(); i ++)
  {
    delete m_BGImagesInMap[i];
  }
  m_BGImagesInMap.Clear();

  // recreate water
  _waterLayer.Destroy();
};

// Index mapping
struct DoubleInt
{
  int _org,_new;
};

TypeIsSimple(DoubleInt);

template<>
struct FindArrayKeyTraits<DoubleInt>
{
  typedef const int &KeyType;
  static bool IsEqual(KeyType a, KeyType b)
  {
    return a==b;
  }
  static KeyType GetKey(const DoubleInt &a) {return a._org;}
};

typedef FindArrayKey<DoubleInt> IndexMapping;

void CPoseidonMap::DoSerialize(CArchive& ar,int nVer)
{
	// serializuji rozm�ry mapy
	MntSerializeInt(ar, m_nWidth);
	MntSerializeInt(ar, m_nHeight);  

	int nCount;
	// dimenze krajiny
	int nLandDim = m_nWidth * m_nHeight;

	if (!ar.IsStoring())
	{	// p�i na��t�n� generuji novou mapu & inicializuji ji
		if (!OnNewMap(m_nWidth,m_nHeight)) AfxThrowUserException();
		// na�ten� v��ky krajiny
		ASSERT(m_pLandHeight != NULL);
		ar.Read((char*)m_pLandHeight, nLandDim * sizeof(LANDHEIGHT));
		UpdateMaxHeight();
		if (nVer >= 47) m_landHeightLocked.DoSerialize(ar);

		// nacteni vysky vody    
		_waterLayer.DoSerialize(ar, nVer);
      
		if (nVer <= 36)
		{    
			// na�ten� registru objekt�, not needed just for back compatibility
			int nCount = 0;
			MntSerializeInt(ar, nCount);
			for (int i = 0; i < nCount; ++i)
			{
				BOOL bExist = FALSE;
				MntSerializeBOOL(ar, bExist);
				CPosObjectTemplate cRegObj(m_pDocument);

				if (bExist)
				{	
					// serializace �ablony   
					cRegObj.DoSerialize(ar, nVer);
					Ref<CPosObjectTemplate> templ;
					templ = m_pDocument->GetObjectTemplate(cRegObj.GetFileName());
					if (templ.IsNull())
					{
						templ = new CPosObjectTemplate(cRegObj);
						if (templ.NotNull()) m_pDocument->AddObjectTemplateSetID(templ);
					}
				} 
			}     
		}

		// na�ten� instanc� objekt�
		if (nVer <= 36)
		{    
			int nCount = 0;
			MntSerializeInt(ar, nCount);
			for (int i = 0; i < nCount; ++i)
			{
				BOOL bExist = FALSE;
				MntSerializeBOOL(ar, bExist);
				if (bExist)
				{	// serializace objektu
					CPosEdObject* pObject = new CPosEdObject(*this);
					pObject->DoSerialize(ar,nVer);
                    
					// napojen� objektu na �ablonu
					Ref<CPosObjectTemplate> templ;                  
					templ = m_pDocument->GetObjectTemplate(pObject->m_tempString);          
					if (templ.NotNull())      
					{
						m_ObjectIDs.ReserveID(pObject->GetID());
						pObject->m_nTemplateID = templ->m_ID;  
						pObject->m_pTemplate = templ;
						templ->m_nTimesUsed++;
						m_InstanceObjs.Add(pObject);
						// v�po�et logick�ch sou�adnic
						pObject->AllocPositions();
						pObject->CalcObjectLogPosition();
					}
					else
					{
						RptF("Error: Serialize. Missing object template: %s, Object will be deleted", pObject->m_tempString);
						delete pObject; // object without template will make problems delete it, it is nonsence         
						m_InstanceObjs.Add(NULL);
					}                  
				} 
				else
				{
					m_InstanceObjs.Add(NULL);          
				}
			}
			if (m_InstanceObjs.GetSize() != nCount) AfxThrowUserException();
		}
		else
		{
			int nCount = 0;
			MntSerializeInt(ar, nCount);      
			for (int i = 0; i < nCount; ++i)
			{
				BOOL bExist = FALSE;
				MntSerializeBOOL(ar, bExist);
				if (bExist)
				{	
					// serializace objektu
					CPosEdObject* pObject = new CPosEdObject(*this);
					pObject->DoSerialize(ar, nVer);         
					// napojen� objektu na �ablonu
					Ref<CPosObjectTemplate> templ;                  
					templ = m_pDocument->GetObjectTemplate(pObject->m_nTemplateID);                   
					if (templ.NotNull())      
					{    
						m_ObjectIDs.ReserveID(pObject->GetID());
						pObject->m_pTemplate = templ;
						templ->m_nTimesUsed++;
						m_InstanceObjs.Add(pObject);
						// v�po�et logick�ch sou�adnic
						pObject->CalcObjectLogPosition();
					}
					else
					{
						RptF("Error: Serialize. Missing object template: id = %d , Object will be deleted", pObject->m_nTemplateID);
						delete pObject; // object without template will make problems delete it, it is nonsence         
						m_InstanceObjs.Add(NULL);
					}                           
				} 
				else
				{
					m_InstanceObjs.Add(NULL);       
				}
			}
			if (m_InstanceObjs.GetSize() != nCount) AfxThrowUserException();
		}

		//// T E X T U R E S

		// na�ten� registru textur
		if (nVer < 35)
		{
			int nCount = 0;
			MntSerializeInt(ar, nCount);

			// Maximal length for primary texture longer filenames are secondary textures
			int MaxNameLen = MAX_FILENAME_LEN_TEXTURE_PRIMAR + lstrlen(TEXTURE_FILE_EXT);
			IndexMapping map; 
			for (int i = 0; i < nCount; ++i)
			{
				BOOL bExist = FALSE;
				MntSerializeBOOL(ar, bExist);
				if (bExist)
				{	
					// serializace �ablony
					Ref<CPosTextureTemplate> pRegTxt = new CPosTextureTemplate(m_pDocument);
					pRegTxt->DoSerialize(ar, nVer); 
					DoubleInt mapItem;
					mapItem._org = pRegTxt->m_nTextureID;
					m_pDocument->m_Textures.AddUnigue(pRegTxt);
					mapItem._new = pRegTxt->m_nTextureID;

					// texture can be secondar, check it
					CString fileName;
					SeparateNameFromFileName(fileName, pRegTxt->m_sTextureFile);

					if (fileName.GetLength() > MaxNameLen)
					{
						Ref<CPosTextureTemplate> pTxtr = m_pDocument->m_Textures.FindEx(pRegTxt->m_nTextureID);
						pTxtr->m_nTxtrType = CPosTextureTemplate::txtrTypeSecundar;
					}

					map.Add(mapItem);
					//m_RegisterTexts.Add(pRegTxt);
				}
			}
			//if (m_RegisterTexts.GetSize()!=nCount)
			//  AfxThrowUserException();
			// update mapy registrovan�ch textur

			UNITPOS pos;
			for (pos.z = 0; pos.z < m_nHeight; ++pos.z)
			{
				for (pos.x = 0; pos.x < m_nWidth; ++pos.x)
				{
					int nTxtrID = 0;
					MntSerializeInt(ar, nTxtrID);
					int newID = map.FindKey(nTxtrID);
					if (newID != -1)
					{
						newID = map[newID]._new;        

						Ref<CPosTextureTemplate> tex = m_pDocument->m_Textures.FindEx(newID);
						m_ActualTextureLayer->SetBaseTextureAt(pos, tex);           
					}
				}
			}   
			// na�ten� sekund�rn�ch textur  
			for (pos.z = 0; pos.z < m_nHeight; ++pos.z)
			{
				for (pos.x = 0; pos.x < m_nWidth; ++pos.x)
				{
					int nTxtrID = 0;
					MntSerializeInt(ar, nTxtrID);
					int newID = map.FindKey(nTxtrID);
					if (newID != -1)
					{
						newID = map[newID]._new;        
						Ref<CPosTextureTemplate> tex = m_pDocument->m_Textures.FindEx(newID);
						m_ActualTextureLayer->SetLandTextureAt(pos, tex, TRUE);             
					}       
				}
			}
		}
		else
		{
			if (nVer == 35)
			{
				UNITPOS pos;
				for (pos.z = 0; pos.z < m_nHeight; ++pos.z)
				{
					for (pos.x = 0; pos.x < m_nWidth; ++pos.x)
					{
						int nTxtrID = 0;
						MntSerializeInt(ar, nTxtrID);            
						Ref<CPosTextureTemplate> tex = m_pDocument->m_Textures.FindEx(nTxtrID);
						m_ActualTextureLayer->SetBaseTextureAt(pos, tex);    
					}
				}

				// na�ten� sekund�rn�ch textur
				for (pos.z = 0; pos.z < m_nHeight; ++pos.z)
				{
					for (pos.x = 0; pos.x < m_nWidth; ++pos.x)
					{
						int nTxtrID = 0;
						MntSerializeInt(ar, nTxtrID);
						Ref<CPosTextureTemplate> tex = m_pDocument->m_Textures.FindEx(nTxtrID);
						m_ActualTextureLayer->SetLandTextureAt(pos, tex, TRUE);   
					}
				}
			}
			else
			{
				nCount = 0;
				MntSerializeInt(ar, nCount);
				CString actualTextureLayerName;
				MntSerializeString(ar, actualTextureLayerName);

				ASSERT(nCount > 0);
				m_ActualTextureLayer->DoSerialize(ar, nVer);
				for(int i = 1; i < nCount; ++i)
				{
					Ref<CTextureLayer> layer = new CTextureLayer(*this);
					layer->DoSerialize(ar, nVer);
					m_TextureLayers.Add(layer);
				}

				Ref<CTextureLayer> layer = GetTextureLayer(actualTextureLayerName);
				if (layer.NotNull())
				{
					m_ActualTextureLayer->Deactivate();
					m_ActualTextureLayer = layer; 
					m_ActualTextureLayer->Activate();
				}
			}
		}

		// na�ten� les�
		if (nVer >= 13)
		{
			MntSerializeInt(ar, nCount);
			for (int i = 0; i < nCount; ++i)
			{
				BOOL bExist = FALSE;
				MntSerializeBOOL(ar, bExist);
				if (bExist)
				{	// serializace �ablony
					CPosWoodObject* pWood = new CPosWoodObject(*this);
					if (pWood == NULL) AfxThrowMemoryException();
					pWood->DoSerialize(ar, nVer);
					m_WoodIDs.ReserveID(pWood->GetID());
					if (nVer >= 30) pWood->CalcObjectLogPosition();
					m_WoodsInMap.Add(pWood);
				} 
				else
				{
					m_WoodsInMap.Add(NULL);
				}
			}
		}

		// na�ten� s�t�
		if (nVer >= 20)
		{
			MntSerializeInt(ar, nCount);
			for (int i = 0; i < nCount; ++i)
			{
				BOOL bExist = FALSE;
				MntSerializeBOOL(ar, bExist);
				if (bExist)
				{	// serializace typu
					WORD nType; MntSerializeWord(ar, nType);
					// vytvo�en� objektu
					CPosNetObjectBase* pTempl = NULL;
					if (nType == CPosNetObjectBase::netObjTypeNor)
					{
						pTempl = new CPosNetObjectNor(*this);
					}
					else if (nType == CPosNetObjectBase::netObjTypeKey)
					{
						pTempl = new CPosNetObjectKey(*this);
					}

					// serializace objektu

					pTempl->DoSerialize(ar, nVer);
					m_NetIDs.ReserveID(pTempl->GetID());
					if (g_bLoadLODShapes) pTempl->DoUpdateFromTemplate(this);
					// update log. sou�adnic
					pTempl->DoUpdateNetPartsBaseRealPos();
					pTempl->DoUpdateNetPartsLogPosition();
					m_NetsInMap.Add(pTempl);
				} 
				else
				{
					m_NetsInMap.Add(NULL);
				}
			}

			if (ar.IsLoading())
			{
				for (int i = 0; i < m_InstanceObjs.GetSize(); ++i)
				{
					CPosEdObject* obj = (CPosEdObject*)m_InstanceObjs[i];
					if (obj)
					{          
						int net = obj->m_nMasterNet;
						if (net != UNDEF_REG_ID)
						{
							if (net >= m_NetsInMap.GetCount() || m_NetsInMap[net] == 0) 
							{
								delete obj;
								m_InstanceObjs[i] = 0;
							}
						}
					}
				}
			}
		}
		// na�ten� pojmenovan�ch oblast�
		if (nVer >= 16)
		{
			MntSerializeInt(ar, nCount);
			for (int i = 0; i < nCount; ++i)
			{
				BOOL bExist = FALSE;
				MntSerializeBOOL(ar, bExist);
				if (bExist)
				{	
					// serializace �ablony
					CPosNameAreaObject* pArea = new CPosNameAreaObject(*this);
					pArea->DoSerialize(ar, nVer);          
					m_AreaIDs.ReserveID(pArea->GetID());

					if (nVer >= 30) pArea->CalcObjectLogPosition();

					m_AreasInMap.Add(pArea);
				} 
				else
				{
					m_AreasInMap.Add(NULL);
				}
			}
		}

		// na�ten� kl��ov�ch bod�
		if (nVer >= 16)
		{
			MntSerializeInt(ar, nCount);
			for (int i = 0; i < nCount; ++i)
			{
				BOOL bExist = FALSE;
				MntSerializeBOOL(ar, bExist);
				if (bExist)
				{	
					// serializace �ablony
					CPosKeyPointObject* pKeyPt = new CPosKeyPointObject(*this);
					pKeyPt->DoSerialize(ar, nVer);
					m_KeyPtIDs.ReserveID(pKeyPt->GetID());
					pKeyPt->CalcObjectLogPosition();
					m_KeyPtInMap.Add(pKeyPt);

				} 
				else
				{
					m_KeyPtInMap.Add(NULL);
				}
			}
		}
    
		if (nVer >= 32)
		{
			MntSerializeInt(ar, nCount);
			for (int i = 0; i < nCount; ++i)
			{        
				CPosBackgroundImageObject* pBGImage = new CPosBackgroundImageObject(*this);
				pBGImage->DoSerialize(ar, nVer);
				pBGImage->CalcObjectLogPosition();        
				m_BGImagesInMap.Add(pBGImage);        
			}
		}
	} 
	else
	{
		// ulo�en� v��ky krajiny
		ASSERT(m_pLandHeight != NULL);
		ar.Write((char*)m_pLandHeight, nLandDim*sizeof(LANDHEIGHT));		
		m_landHeightLocked.DoSerialize(ar);
		// ulozeni vysky vody
		_waterLayer.DoSerialize(ar, nVer);
		// ulo�en� instanc� objekt�
		nCount = m_InstanceObjs.GetSize();
		MntSerializeInt(ar, nCount);
		for (int i = 0; i < nCount; ++i)
		{
			CPosEdObject* pObject = (CPosEdObject*)(m_InstanceObjs[i]);
			BOOL bExist = (pObject != NULL);
			MntSerializeBOOL(ar, bExist);
			if (bExist)
			{	
				// serializace objektu
				ASSERT_KINDOF(CPosEdObject, pObject);
				ASSERT(pObject->m_pTemplate != NULL);
				pObject->DoSerialize(ar, nVer);
			}
		}
		// ulo�en� registru textur
		/*nCount = m_RegisterTexts.GetSize();
		MntSerializeInt(ar, nCount);
		for(i=0;i<nCount;i++)
		{
			CPosTextureTemplate* pRegTxt = (CPosTextureTemplate*)(m_RegisterTexts[i]);
			BOOL bExist = (pRegTxt != NULL);
			MntSerializeBOOL(ar, bExist);
			if (bExist)
			{	// serializace �ablony textury
				ASSERT_KINDOF(CPosTextureTemplate,pRegTxt);
				pRegTxt->DoSerialize(ar,nVer);
			}
		}*/
		// ulo�en� textur		
		int layers = m_TextureLayers.Size();
		MntSerializeInt(ar, layers);
		MntSerializeString(ar, m_ActualTextureLayer->m_Name);
		for (int i = 0; i < m_TextureLayers.Size(); ++i)
		{
			m_TextureLayers[i]->DoSerialize(ar, nVer);
		}
		
		// ulo�en� les�
		nCount = m_WoodsInMap.GetSize(); 
		MntSerializeInt(ar, nCount);
		for (int i = 0; i < nCount; ++i)
		{
			CPosWoodObject* pWood = (CPosWoodObject*)(m_WoodsInMap[i]);
			BOOL bExist = (pWood != NULL);
			MntSerializeBOOL(ar, bExist);
			if (bExist)
			{	
				// serializace �ablony textury
				ASSERT_KINDOF(CPosWoodObject, pWood);
				if (nVer >= 30) pWood->PreSerializeStoring(*this);
				pWood->DoSerialize(ar, nVer);
			}
		}

		// ulo�en� s�t�
		nCount = m_NetsInMap.GetSize(); 
		MntSerializeInt(ar, nCount);
		for (int i = 0; i < nCount; ++i)
		{
			CPosNetObjectBase* pTempl = (CPosNetObjectBase*)(m_NetsInMap[i]);
			BOOL bExist = (pTempl != NULL);
			MntSerializeBOOL(ar, bExist);
			if (bExist)
			{	ASSERT_KINDOF(CPosNetObjectBase, pTempl);
				// serializace typu objektu
				WORD nType = (WORD)(pTempl->GetNetObjectType());
				MntSerializeWord(ar, nType);
				// serializace objektu
				pTempl->DoSerialize(ar, nVer);
			}
		}

		// ulo�en� pojmenovan�ch oblast�
		nCount = m_AreasInMap.GetSize(); 
		MntSerializeInt(ar, nCount);
		for (int i = 0; i < nCount; ++i)
		{
			CPosNameAreaObject* pArea = (CPosNameAreaObject*)(m_AreasInMap[i]);
			BOOL bExist = (pArea != NULL);
			MntSerializeBOOL(ar, bExist);
			if (bExist)
			{	
				// serializace �ablony textury
				ASSERT_KINDOF(CPosNameAreaObject, pArea);        
				pArea->PreSerializeStoring(*this);
				pArea->DoSerialize(ar, nVer);
			}
		}

		// ulo�en� kl��ov�ch bod�
		nCount = m_KeyPtInMap.GetSize(); 
		MntSerializeInt(ar, nCount);
		for (int i = 0; i < nCount; ++i)
		{
			CPosKeyPointObject* pKeyPt = (CPosKeyPointObject*)(m_KeyPtInMap[i]);
			BOOL bExist = (pKeyPt != NULL);
			MntSerializeBOOL(ar, bExist);
			if (bExist)
			{	
				// serializace �ablony textury
				ASSERT_KINDOF(CPosKeyPointObject, pKeyPt);
				pKeyPt->DoSerialize(ar, nVer);
			}
		}

		nCount = m_BGImagesInMap.Size(); 
		MntSerializeInt(ar, nCount);
		for (int i = 0; i < nCount; ++i)
		{
			CPosBackgroundImageObject* pBGImage = (CPosBackgroundImageObject*)(m_BGImagesInMap[i]);
			ASSERT(pBGImage != NULL);      
			pBGImage->DoSerialize(ar, nVer);     
		}
	}	

	/// repair relHeight
	if (ar.IsLoading() && nVer < 50)
	{
		for (int i = 0; i < GetPoseidonObjectCount(); ++i)
		{
			CPosEdObject* obj = GetPoseidonObject(i);
			if (obj) obj->RepairObjectPoseidonRelHeight();
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// pr�ce se sou�adnicemi

BOOL CPoseidonMap::IsAtWorld(const UNITPOS& Pos) const
{
	if ((Pos.x <0)||(Pos.z <0)) return FALSE;
	if ((Pos.x >=m_nWidth)||(Pos.z >=m_nHeight)) return FALSE;
	return TRUE;
};

BOOL CPoseidonMap::IsAtWorld(const REALPOS& Pos) const
{
	if ((Pos.x <0.f)||(Pos.z <0.f)) return FALSE;
	if (Pos.x > (float)(m_nWidth *m_pDocument->m_nRealSizeUnit))
		return FALSE;
	if (Pos.z > (float)(m_nHeight*m_pDocument->m_nRealSizeUnit))
		return FALSE;
	return TRUE;
};

UNITPOS	CPoseidonMap::FromRealToUnitPos(const REALPOS& PosFrom) const
{
	//ASSERT(IsAtWorld(PosFrom));
	UNITPOS PosTo;
	double nInvLandGrid = 1.f/((double)(m_pDocument->m_nRealSizeUnit));
	PosTo.x = (int)((double)(PosFrom.x * nInvLandGrid));
	PosTo.z = (int)((double)(PosFrom.z * nInvLandGrid));
	return PosTo;
};
REALPOS	CPoseidonMap::FromUnitToRealPos(const UNITPOS& PosFrom) const
{
	//ASSERT(IsAtWorld(PosFrom));
	REALPOS PosTo;
	PosTo.x = (float)(PosFrom.x * m_pDocument->m_nRealSizeUnit);
	PosTo.z = (float)(PosFrom.z * m_pDocument->m_nRealSizeUnit);
	return PosTo;
};

POINT CPoseidonMap::FromUnitToViewPos(const UNITPOS& PosFrom) const
{
  POINT PosTo;
  PosTo.x = PosFrom.x * VIEW_LOG_UNIT_SIZE;
  PosTo.y = (m_nHeight - PosFrom.z) * VIEW_LOG_UNIT_SIZE;
  return PosTo;
};


UNITPOS	CPoseidonMap::FromRealToTextureUnitPos(const REALPOS& PosFrom) const
{
  return m_ActualTextureLayer->FromRealToTextureUnitPos(PosFrom);
};
REALPOS	CPoseidonMap::FromTextureUnitToRealPos(const UNITPOS& PosFrom) const
{
  return m_ActualTextureLayer->FromTextureUnitToRealPos(PosFrom);
};

POINT CPoseidonMap::FromTextureUnitToViewPos(const UNITPOS& PosFrom) const
{
  return m_ActualTextureLayer->FromTextureUnitToViewPos(PosFrom);
};

UNITPOS CPoseidonMap::FromTextureUnitToUnitPos(const UNITPOS& PosFrom) const
{
  return m_ActualTextureLayer->FromTextureUnitToUnitPos(PosFrom);
};

UNITPOS CPoseidonMap::FromUnitToTextureUnitPos(const UNITPOS& PosFrom) const
{
  return m_ActualTextureLayer->FromUnitToTextureUnitPos(PosFrom);
};


POINT CPoseidonMap::FromRealToViewPos(const REALPOS& PosFrom) const
{
	POINT  PosTo;
	double nMSize = ((double)VIEW_LOG_UNIT_SIZE)/((double)(m_pDocument->m_nRealSizeUnit));
	PosTo.x = (int)Round(((double)(PosFrom.x))*nMSize,0);
	PosTo.y = (VIEW_LOG_UNIT_SIZE*m_nHeight)-(int)Round(((double)(PosFrom.z))*nMSize,0);
	return PosTo;
};

CRect	CPoseidonMap::FromRealToViewRect(const FltRect& realRect) const
{
  REALPOS realPos;
  realPos.x = realRect[0];
  realPos.z = realRect[3];
  POINT pt1 = FromRealToViewPos(realPos);
  realPos.x = realRect[2];
  realPos.z = realRect[1];
  POINT pt2 = FromRealToViewPos(realPos);

  return CRect(pt1, pt2);
}

REALPOS	CPoseidonMap::FromViewToRealPos(const POINT& PosFrom) const
{
	REALPOS PosTo;
	double	nMSize  = ((double)(m_pDocument->m_nRealSizeUnit))/((double)VIEW_LOG_UNIT_SIZE);
	PosTo.x = (float)((PosFrom.x) * nMSize);
	PosTo.z = (float)(((VIEW_LOG_UNIT_SIZE*(m_nHeight))-PosFrom.y)*nMSize);
	return  PosTo;
};

FltRect	CPoseidonMap::FromViewToRealRect(const CRect& PosFrom) const
{
  FltRect RectTo;
  REALPOS PosTo;
  PosTo = FromViewToRealPos(PosFrom.TopLeft());
  RectTo.left = PosTo.x;
  RectTo.bottom = PosTo.z;

  PosTo = FromViewToRealPos(PosFrom.BottomRight());
  RectTo.right = PosTo.x;
  RectTo.top = PosTo.z;

  return RectTo;
};


BOOL CPoseidonMap::IsUnitCenterAtArea(UNITPOS nUnit,const CPositionArea* pArea) const
{
	if (!IsAtWorld(nUnit))
		return FALSE;

	float   nUnitSz2= ((float)m_pDocument->m_nRealSizeUnit)/2.f;
	REALPOS nReal = FromUnitToRealPos(nUnit);
	nReal.x += nUnitSz2;	// um�st�m na st�ed
	nReal.z += nUnitSz2;
	// p�evedu na log.
	POINT	nLog  = FromRealToViewPos(nReal);
	return  pArea->IsPointAtArea(nLog);
};
BOOL CPoseidonMap::IsTextureUnitCenterAtArea(UNITPOS nUnit,const CPositionArea* pArea) const
{
  return m_ActualTextureLayer->IsTextureUnitCenterAtArea(nUnit, pArea);  
};

BOOL CPoseidonMap::IsUnitCenterAtArea(int X,int Y,const CPositionArea* pArea) const
{
	UNITPOS nUnit = { X, Y };
	return IsUnitCenterAtArea(nUnit,pArea);
};


void CPoseidonMap::GetUnitPosHull(UNITPOS& LftTop, UNITPOS& RghBtm, const CRect& rArea) const
{
	//REALPOS rPos = FromViewToRealPos(rArea.TopLeft());	
	if (rArea.left <= 0)
	{
		LftTop.x = (rArea.left + 1) / VIEW_LOG_UNIT_SIZE;
	}
  	else
	{
		LftTop.x = (rArea.left - 1) / VIEW_LOG_UNIT_SIZE + 1;
	}	

	if (rArea.top <= 0)
	{
		LftTop.z = (rArea.top + 1) / VIEW_LOG_UNIT_SIZE;
	}
	else
	{
		// this line triggers a bug when pressing "Change height..." button
		// the count of vertices is wrong
		// warning that changing this line will change also the answer
		// of the system to change in height field using buldozer
		LftTop.z = (rArea.top - 1) / VIEW_LOG_UNIT_SIZE;
	}

	if (rArea.right <= 0)
	{
		RghBtm.x = (rArea.right + 1) / VIEW_LOG_UNIT_SIZE - 1;
	}
	else
	{
		RghBtm.x = (rArea.right - 1) / VIEW_LOG_UNIT_SIZE;
	}

	if (rArea.bottom <= 0)
	{
		RghBtm.z = (rArea.bottom + 1) / VIEW_LOG_UNIT_SIZE - 1;
	}
	else
	{
		// this line triggers a bug when pressing "Change height..." button
		// the count of vertices is wrong
		// warning that changing this line will change also the answer
		// of the system to change in height field using buldozer
		RghBtm.z = (rArea.bottom - 1) / VIEW_LOG_UNIT_SIZE + 1;
	}

	LftTop.z = m_nHeight - LftTop.z /* - 1 */;
	RghBtm.z = m_nHeight - RghBtm.z /* - 1 */;
};

void CPoseidonMap::GetTextureUnitPosHull(UNITPOS& LftTop, UNITPOS& RghBtm, const CRect& rArea) const
{
	m_ActualTextureLayer->GetTextureUnitPosHull(LftTop, RghBtm, rArea);
};

/////////////////////////////////////////////////////////////////////////////
// pr�ce s v��kou krajiny

LANDHEIGHT CPoseidonMap::GetLandscapeHeight(const REALPOS& Pos, float* rdX, float* rdY) const
{
	UNITPOS UnPos = FromRealToUnitPos(Pos);

	float nInvLandGrid = 1.f / ((float)(m_pDocument->m_nRealSizeUnit));
	float xRel = Pos.x * nInvLandGrid;
	float zRel = Pos.z * nInvLandGrid;
	float xIn  = xRel - UnPos.x; // relative 0..1 in square
	float zIn  = zRel - UnPos.z;

	float y00 = GetLandscapeHeight(UnPos); UnPos.x++;
	float y10 = GetLandscapeHeight(UnPos); UnPos.z++;
	float y11 = GetLandscapeHeight(UnPos); UnPos.x--;
	float y01 = GetLandscapeHeight(UnPos);
	// each face is divided to two triangles
	// determine which triangle contains point
	LANDHEIGHT y;
	if (xIn <= 1 - zIn)
	{ // triangle 00,01,10
		if (rdX) *rdX = (y10 - y00) * nInvLandGrid;
		if (rdY) *rdY = (y01 - y00) * nInvLandGrid;
		y=y00 + (y01 - y00) * zIn + (y10 - y00) * xIn;
	}
	else
	{
		// triangle 01,10,11
		if (rdX) *rdX = (y11 - y01) * nInvLandGrid;
		if (rdY) *rdY = (y11 - y10) * nInvLandGrid;
		y = y01 + y10 - y11 - (y01 - y11) * xIn - (y10 - y11) * zIn;
	}
	return y;
};

LANDHEIGHT CPoseidonMap::GetLandscapeHeight(const UNITPOS& Pos)const
{
	if (!IsAtWorld(Pos))
		return m_nMinHeight/*0.f*/;	// nen� na map� sv�ta
	// vrac�m p��slu�nou hodnotu
	ASSERT(m_pLandHeight!=NULL);
	return m_pLandHeight[Pos.x+Pos.z*m_nWidth];
};
LANDHEIGHT CPoseidonMap::GetLandscapeHeight(int X,int Z)const
{
	UNITPOS Pos = { X, Z };
	return  GetLandscapeHeight(Pos);
};
// pr�m�rn� v��ka pro texturaci
LANDHEIGHT CPoseidonMap::GetBalancedHeight(const UNITPOS& nUnit,float* rdGradMax) const 
{
	UNITPOS nUnP;
	float   nGradX[4],nGradY[4];
	// pr�m�rn� v��ka
	LANDHEIGHT nHeight = GetLandscapeHeight(FromUnitToRealPos(nUnit),&(nGradX[0]),&(nGradY[0]));
	nUnP = nUnit; nUnP.x++;
	nHeight += GetLandscapeHeight(FromUnitToRealPos(nUnP),&(nGradX[1]),&(nGradY[1]));
	nUnP = nUnit; nUnP.z++;
	nHeight += GetLandscapeHeight(FromUnitToRealPos(nUnP),&(nGradX[2]),&(nGradY[2]));
	nUnP = nUnit; nUnP.x++; nUnP.z++;
	nHeight += GetLandscapeHeight(FromUnitToRealPos(nUnP),&(nGradX[3]),&(nGradY[3]));
	nHeight /= 4.f;
	// maximalni gradient
	if (rdGradMax != NULL)
	{
		(*rdGradMax) = (float)max(fabs(nGradX[0]),fabs(nGradY[0]));
	}
	return nHeight;
};
void CPoseidonMap::SetLandscapeHeight(int X,int Z,LANDHEIGHT nHeight,bool bFromBuldozer, bool recalcObjectHeight /* = true*/)
{
	UNITPOS Pos = { X, Z };
	SetLandscapeHeight(Pos,nHeight,bFromBuldozer,recalcObjectHeight);
};
void CPoseidonMap::SetLandscapeHeight(const UNITPOS& Pos,LANDHEIGHT nHeight,bool bFromBuldozer, bool recalcObjectHeight /* = true*/)
{
	if ((m_pLandHeight==NULL)||(!IsAtWorld(Pos)) || m_landHeightLocked[Pos.x+Pos.z*m_nWidth])
		return;	// nen� na map� sv�ta nebo zamceny
	// �prava v��ky v povolen�m rozsahu
	if (nHeight > MAX_LAND_HEIGHT)
		nHeight = MAX_LAND_HEIGHT;
	if (nHeight < MIN_LAND_HEIGHT)
		nHeight = MIN_LAND_HEIGHT;
	ASSERT(m_pLandHeight!=NULL);
	if (m_pLandHeight[Pos.x+Pos.z*m_nWidth] == nHeight)
		return;	// ��dn� zm�na

	m_pLandHeight[Pos.x+Pos.z*m_nWidth] = nHeight;
	// maxim�ln� v��ka
	if (m_nMaxHeight < nHeight)
		m_nMaxHeight = nHeight;
	if (m_nMinHeight > nHeight)
		m_nMinHeight = nHeight;
	// nen�-li z buldozera->provedu update
	if (!bFromBuldozer)
      if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->LandHeightChange(STexturePosMessData(Pos.x,Pos.z,nHeight,0));	

  if (recalcObjectHeight)
  {
    UNITPOS pt1 = Pos;
    pt1.x--;
    pt1.z--;
    UNITPOS pt2 = Pos;
    pt2.x++;
    pt2.z++;

    CPoint pt1Log = FromUnitToViewPos(pt1);
    CPoint pt2Log = FromUnitToViewPos(pt2);

    CRect updateRect(pt1Log.x, pt2Log.y, pt2Log.x, pt1Log.y);

    UpdateObjectsHeight(updateRect);
  }
};
void CPoseidonMap::SetGlobalHeight(LANDHEIGHT nHeight) 
{
	if (m_pLandHeight==NULL)
		return;	
	// inicializace v��ky krajiny
	int nLandDim = m_nWidth * m_nHeight;
	for(int i=0; i<nLandDim; i++)
  { if (! m_landHeightLocked[i])
		  m_pLandHeight[i] = nHeight;
  }
};

void CPoseidonMap::UpdateMaxHeight()
{
	m_nMaxHeight = 0.f;
	m_nMinHeight = 0.f;
	if (m_pLandHeight!=NULL)
	{
		int nLandDim= m_nWidth * m_nHeight;
		for(int i=0;i<nLandDim; i++)
		{
			float nHeight = m_pLandHeight[i];
			// maxim�ln� v��ka
			if (m_nMaxHeight < nHeight)
				m_nMaxHeight = nHeight;
			if (m_nMinHeight > nHeight)
				m_nMinHeight = nHeight;
		};
	};
};

/////////////////////////////////////////////////////////////////////////////
// pr�ce s objekty

void CPoseidonMap::UpdateObjectsHeight(const CRect& rect, bool keepRelHeight /*= true*/,bool repairMatrix /*=true*/)
{  
  for(int i = 0; i <  m_InstanceObjs.GetSize(); i++)
  {
    CPosEdObject* obj = (CPosEdObject*)(m_InstanceObjs[i]);
    if (obj)
    {
      ASSERT_KINDOF(CPosEdObject,obj);
      ASSERT(obj->m_pTemplate != NULL);

      CRect hull; 
      obj->GetObjectHull(hull);

      if (!IsIntersectRect(hull,rect)) // do it faster use only hull....
        continue; 

      if (repairMatrix) obj->RepairMatrix();
      obj->CalcObjectPoseidonHeight();

      SMoveObjectPosMessData dta;
      obj->SetObjectDataToMessage(dta);
      if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->ObjectMove(dta);
    }
  }
}

void CPoseidonMap::UpdateObjectsHeight(bool keepRelHeight /*= true*/,bool repairMatrix /*=true*/)
{
  CRect rect(0,0,m_nWidth * VIEW_LOG_UNIT_SIZE, m_nHeight * VIEW_LOG_UNIT_SIZE);
  UpdateObjectsHeight(rect, keepRelHeight,repairMatrix);
}



CPosEdObject* CPoseidonMap::GetPoseidonObject(int nObjID) const
{
	if ((nObjID<0)||(nObjID >= m_InstanceObjs.GetSize()))
		return NULL;
  if (m_InstanceObjs[nObjID] == NULL ||  !m_InstanceObjs[nObjID]->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
    return NULL; //TODO why is here a mistake?
 
	return ((CPosEdObject*)(m_InstanceObjs[nObjID]));
};

void CPoseidonMap::AddNewObject(CPosEdObject* pObj,BOOL bFromBuldozer)
{
	ASSERT(pObj->GetID()  != UNDEF_REG_ID);	

	// v�po�et logick�ch sou�adnic
	pObj->CalcObjectLogPosition();

	// vlo��m objekt do seznamu instanc�
	if (pObj->GetID() >= m_InstanceObjs.GetSize())
		m_InstanceObjs.SetSize(pObj->GetID()+100);

	ASSERT(m_InstanceObjs[pObj->GetID()] == NULL);
	m_InstanceObjs[pObj->GetID()]  = pObj;

  ASSERT(pObj->m_pTemplate.NotNull());
  pObj->m_pTemplate->m_nTimesUsed++;

	// aktualizuji Buldozer  
	if (!bFromBuldozer)
	{
      if (pObj->m_pTemplate->m_nTimesUsed == 1)
      {
        //Register template into buldozer
        CString fileName; 
        pObj->m_pTemplate->GetBuldozerFileName(fileName);
        if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->RegisterObjectType(fileName);
      }    

      CString fileName; 
      pObj->GetBuldozerFileName(fileName);

      SMoveObjectPosMessData dta;
  	  pObj->SetObjectDataToMessage(dta);    

      if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->ObjectCreate(dta,fileName);
	}
};

void CPoseidonMap::DeleteObject(int nObjID,BOOL bFromBuldozer)
{
	CPosEdObject* pObj = GetPoseidonObject(nObjID);
	if (pObj == NULL)
		return;
	// odstran�m objekt  
  if (pObj->m_pTemplate.NotNull())
    pObj->m_pTemplate->m_nTimesUsed--;

  m_ObjectIDs.ReleaseID(nObjID);    
	m_InstanceObjs[nObjID] = NULL;

	// optimalizuji pole
	int nIndx = m_InstanceObjs.GetSize()-1;
	while((nIndx>=0)&&(m_InstanceObjs[nIndx] == NULL))
	{		
		nIndx--;
	}

  if (m_InstanceObjs.GetSize() - nIndx > 100)
  {
    m_InstanceObjs.SetSize(nIndx + 1);
  }

	// aktualizuji Buldozer
	if (!bFromBuldozer)
	{
      SMoveObjectPosMessData dta;
      pObj->SetObjectDataToMessage(dta);
      if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->ObjectDestroy(dta);
	}
	// zru��m objekt
	delete pObj;
};

void CPoseidonMap::MoveObjectToPos(CPosEdObject* pObj,const REALPOS&,BOOL bFromBuldozer)
{
	ASSERT(pObj->GetID()  != UNDEF_REG_ID);
	ASSERT(pObj->m_pTemplate  != NULL);

	// v�po�et logick�ch sou�adnic objektu
	pObj->CalcObjectLogPosition();
	// aktualizuji polohu objektu
	//if (!bFromBuldozer)
	{
		pObj->CalcObjectPoseidonHeight();
	}
	// aktualizuji Buldozer
	if (!bFromBuldozer)
	{
        SMoveObjectPosMessData dta;
        pObj->SetObjectDataToMessage(dta);
        if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->ObjectMove(dta);
	}
};

/////////////////////////////////////////////////////////////////////////////
// pr�ce s texturami

PTRTEXTURETMPLT	CPoseidonMap::CreateSecundTextureFrom(PTRTEXTURETMPLT pXX,PTRTEXTURETMPLT pYY,PTRTEXTURETMPLT pUU,PTRTEXTURETMPLT pZZ,BOOL bRegener)
{
	CPosTextureTemplate* pScnd = NULL;
	TRY
	{
		pScnd = new CPosTextureTemplate(m_pDocument);

		// jm�no souboru do textury
		CreateSecundTextureName(pScnd->m_sTextureName, pXX,pYY,pUU,pZZ);	
    CreateTransitionSquareSurfaceBimPasFileName(pScnd->m_sTextureFile,pXX,pYY,pUU,pZZ);    

		// pln� jm�no k souboru bimpas
    CString sBimPasFile;
    
		pScnd->GetFullBimPasFileName(sBimPasFile);
		if ((bRegener)&&(MntExistFile(sBimPasFile)))
			MntDeleteFile(sBimPasFile);
		if (!MntExistFile(sBimPasFile))
		{
			ASSERT((pXX!=NULL)&&(pYY!=NULL)&&(pUU!=NULL)&&(pZZ!=NULL));
			CreateTransitionSquareSurfaceBimPasFile(sBimPasFile,pXX,pYY,pUU,pZZ);
		}
		// neexistuje-li soubor, m�m sm�lu
		if (!MntExistFile(sBimPasFile))
		{
			delete pScnd;
			return NULL;	// nelze nic vytvo�it
		};
		// set the rest values
    pScnd->MakeDefaultValues();
    // ID nen� ur�eno
    pScnd->m_nTextureID = UNDEF_REG_ID;        
    // textura je default. prim�rn�
    pScnd->m_nTxtrType = CPosTextureTemplate::txtrTypeSecundar;
    // nen� to default. mo�e
    pScnd->m_bIsSea = FALSE;
    // 0 pravd�podobnost
    pScnd->m_nVarPorpab = 0.f;
    // color will be average color from all textures
    
    float r = GetRValue(pXX->GetColor())/256.0f;
    float g = GetGValue(pXX->GetColor())/256.0f;
    float b = GetBValue(pXX->GetColor())/256.0f;

    r += GetRValue(pYY->GetColor())/256.0f;
    g += GetGValue(pYY->GetColor())/256.0f;
    b += GetBValue(pYY->GetColor())/256.0f;

    r += GetRValue(pUU->GetColor())/256.0f;
    g += GetGValue(pUU->GetColor())/256.0f;
    b += GetBValue(pUU->GetColor())/256.0f;

    r += GetRValue(pZZ->GetColor())/256.0f;
    g += GetGValue(pZZ->GetColor())/256.0f;
    b += GetBValue(pZZ->GetColor())/256.0f;

    pScnd->m_cTxtrColor = RGB((BYTE) (r *  64),(BYTE) (g *  64),(BYTE) (b *  64));
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pScnd)
		{
			delete pScnd;
			pScnd = NULL;
		}
	}
	END_CATCH_ALL 
	return pScnd;
};

void CPoseidonMap::CreateSecundTextureName(CString& sName,PTRTEXTURETMPLT pXX,PTRTEXTURETMPLT pYY,PTRTEXTURETMPLT pUU,PTRTEXTURETMPLT pZZ)
{
	// p�i chyb� vrac�m pr�zdn� jm�no
	sName.Empty();
	// je-li n�kter� textura NULL -> nem�m co vytv��et
	if ((pXX == NULL)||(pYY == NULL)||(pUU == NULL)||(pZZ == NULL))
		return;	// ERROR

	// d�l�� jm�na z textur
	CString sXX,sYY,sUU,sZZ;
	if (!pXX->GetScndTxtrNamePart(sXX))
		return; // ERROR
	if (!pYY->GetScndTxtrNamePart(sYY))
		return; // ERROR
	if (!pUU->GetScndTxtrNamePart(sUU))
		return; // ERROR
	if (!pZZ->GetScndTxtrNamePart(sZZ))
		return; // ERROR
	// v�echna jm�na jsou OK ! -> poskl�d�m je
	sName  = sXX;
	sName += sYY;
	sName += sUU;
	sName += sZZ;

  // The extension  is not more needed.
	// p�id�m koncovku souboru
	/*if (GetPosEdEnvironment()->m_optPosEd.m_nTextureFormat == CPosEdOptions::txtrPAA)
		sName += TEXTURE_FILE_EXT2;
	else
		sName += TEXTURE_FILE_EXT;
    */
};

void CPoseidonMap::CreateTransitionSquareSurfaceBimPasFileName(CString& sName,PTRTEXTURETMPLT pXX,PTRTEXTURETMPLT pYY,PTRTEXTURETMPLT pUU,PTRTEXTURETMPLT pZZ)
{
  CreateSecundTextureName(sName, pXX, pYY, pUU, pZZ);
  if (sName.GetLength())
    sName += BIMPAS_FILE_EXT;
}


static inline float Bilint(
                           float y00, float y01, float y10, float y11,
                           float xf, float zf
                           )
{
  // y00 top-left
  // y01 bottom-left
  // y10 top-right
  // y11 bottom-right
  float y0z = y00*(1-zf) + y01*zf;
  float y1z = y10*(1-zf) + y11*zf;
  return y0z*(1-xf) + y1z*xf;
}

void CPoseidonMap::CreateTransitionSquareSurfaceBimPasFile(const CString& sFileName,PTRTEXTURETMPLT pXX,PTRTEXTURETMPLT pYY,PTRTEXTURETMPLT pUU,PTRTEXTURETMPLT pZZ)
{
  CString s[4];
  if (pXX != NULL) pXX->GetBuldozerTextureFileName(s[0]);
  if (pYY != NULL) pYY->GetBuldozerTextureFileName(s[1]);
  if (pUU != NULL) pUU->GetBuldozerTextureFileName(s[2]);
  if (pZZ != NULL) pZZ->GetBuldozerTextureFileName(s[3]); 

  CString m[4];
  if (pXX != NULL) pXX->GetBuldozerRvMatFileName(m[0]);
  if (pYY != NULL) pYY->GetBuldozerRvMatFileName(m[1]);
  if (pUU != NULL) pUU->GetBuldozerRvMatFileName(m[2]);
  if (pZZ != NULL) pZZ->GetBuldozerRvMatFileName(m[3]);



  Ref<MultipassMaterial> mat = new MultipassMaterial;

  //Assert(len>=8);
  // TL TR BL BR
  static const float factorsAll[4][4]=
  {
    {1,0,0,0},
    {0,1,0,0},
    {0,0,1,0},
    {0,0,0,1}
  };
  for (int i=0; i<4; i++)
  {
    const float *factors = factorsAll[i];
   
    SinglePassInfo info;
    info.tlAlpha = Bilint(factors[0],factors[2],factors[1],factors[3],0,0);
    info.trAlpha = Bilint(factors[0],factors[2],factors[1],factors[3],1,0);
    info.blAlpha = Bilint(factors[0],factors[2],factors[1],factors[3],0,1);
    info.brAlpha = Bilint(factors[0],factors[2],factors[1],factors[3],1,1);

    info.texture = RString(s[i]);
    info.material = RString(m[i]);
    mat->AddPass(info);
  }

  mat->Close();
  ParamArchiveSave ar(0);
  mat->Serialize(ar);

  ar.Save(sFileName);  
};

void CPoseidonMap::CreateSquareSurfaceBimPasFile(const CString& sFileName,PTRTEXTURETMPLT text)
{  
  CString texturePath; // path to the texture
  text->GetBuldozerTextureFileName(texturePath);
  CString rvmatPath; // path to the bimpas
  text->GetBuldozerRvMatFileName(rvmatPath);

  SinglePassInfo info;
  info.texture = RString(texturePath);  
  info.material = RString(rvmatPath);;
  info.tlAlpha = 1;
  info.trAlpha = 1;
  info.blAlpha = 1;
  info.brAlpha = 1;

  Ref<MultipassMaterial> mat = new MultipassMaterial;
  mat->_passes.Add(info);

  mat->Close();
  ParamArchiveSave ar(0);
  mat->Serialize(ar);
  ar.Save(sFileName);  
};

// jsou dv� textury stejn� ?
BOOL CPoseidonMap::IsSameTextures(PTRTEXTURETMPLT pT1,PTRTEXTURETMPLT pT2)
{
	/*if ((pT1 != NULL)&&(pT1->m_nTextureID == TEXTURE_SEA_ID))
		 pT1  = NULL;	
	if ((pT2 != NULL)&&(pT2->m_nTextureID == TEXTURE_SEA_ID))
		 pT2  = NULL;	*/
	// jsou-li ob� mo�e -> OK
	if ((pT1 == NULL)&&(pT2 == NULL))
		return TRUE;
	// je-li jedna mo�e -> FALSE
	if ((pT1 == NULL)||(pT2 == NULL))
		return FALSE;
	// musej� b�t ze stejn�ho souboru
	return (lstrcmp(pT1->m_sTextureFile,pT2->m_sTextureFile)==0);
};

/////////////////////////////////////////////////////////////////////////////
// p�eps�n� mapy do Buldozeru

void CPoseidonMap::TransferMapToBuldozer() const
{
	if (!m_pDocument->IsRealDocument()) return;

	AfxGetApp()->BeginWaitCursor();
	ASSERT(m_pDocument != NULL);
	CString sBuldozerName;
	
	// reset obsahu buldozeru
	{    
		SLandscapePosMessData data;

		data.landRangeX = m_nWidth;
		data.landRangeY = m_nHeight;
		data.textureRangeX = m_ActualTextureLayer->m_nTexWidth;
		data.textureRangeY = m_ActualTextureLayer->m_nTexHeight;
		// land grid is in buldozer used like texture grid
		data.landGridX = m_ActualTextureLayer->GetTextureRealSize(); 
		data.landGridY = m_ActualTextureLayer->GetTextureRealSize();

		if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->SystemInit(data,m_pDocument->GetConfigName());
	}

	{
		STexturePosMessData data;
	
		// temporary use this variables to transfer number of messages
		data.nX = 0;
		data.nZ = 0;
		// find number of textures to register
		data.nTextureID = 0;
		int size = m_pDocument->m_Textures.Size();
		for (int i = 0; i < size; ++i)
		{
			Ref<CPosTextureTemplate> pTempl = m_pDocument->m_Textures[i];
			if (pTempl.NotNull() && pTempl->m_nTimesUsedInLandActive > 0) data.nTextureID++;
		}
		// find number of objects to register
		size = m_pDocument->ObjectTemplatesSize();
		for (int i = 0; i < size; ++i)
		{
			Ref<CPosObjectTemplate> pTempl = m_pDocument->GetIthObjectTemplate(i);
			if (pTempl.NotNull() && pTempl->m_nTimesUsed > 0) data.nX++;
		}    

		//find number of objects
		size = m_InstanceObjs.GetSize();
		for (int i = 0; i < size; ++i)
		{
			if (m_InstanceObjs[i]) data.nZ++;
		}

		if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->FileImportBegin(data);
	}

	if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->StartBuffer(1024 * 1024 * 10);

	//Ref<CTextureLayer> actualLayer = m_pDocument->GetActiveTextureLayer();
  
	Ref<CTextureLayer> actualLayer = m_ActualTextureLayer;
  
	if (actualLayer->CheckExplicitBimpas())
	{ // if explicit texture model is used, transfer explicit
		int size = actualLayer->m_explicitBimpasRegister.Size();
		for (int i = 0; i < size; ++i)
		{
			RString name = actualLayer->m_explicitBimpasRegister[i].name;
			// extend to project relative path

			CString projRelName;
			m_pDocument->GetProjectDirectory(projRelName, CPosEdBaseDoc::dirText, TRUE);
			projRelName += name;

			if (m_pDocument->RealViewer())
			{
				m_pDocument->RealViewer()->RegisterLandscapeTexture(SMoveObjectPosMessData(i),projRelName);
			}
		}    
	}
	else
	{ 
		// otherwise transfer implicit textures
		int size = m_pDocument->m_Textures.Size();
		for (int i = 0; i < size; ++i)
		{
			Ref<CPosTextureTemplate> pTempl = m_pDocument->m_Textures[i];
			if (pTempl.NotNull() && pTempl->m_nTimesUsedInLandActive > 0)
			{
				ASSERT_KINDOF(CPosTextureTemplate, pTempl);

				CString fileName; 
				pTempl->GetBuldozerBimPasFileName(fileName);
				if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->RegisterLandscapeTexture(SMoveObjectPosMessData(pTempl->m_nTextureID),fileName);
			}
		}
	}

	// transfer v��ky
	{
		AutoArray<float> msgData;

		for (int nX = 0; nX < m_nWidth; ++nX)
		{
			for (int nZ = 0; nZ < m_nHeight; ++nZ)
			{
				// v��ka
				UNITPOS Pos = { nX, nZ };
				LANDHEIGHT nHeight = GetLandscapeHeight(Pos);
				msgData.Append() = nHeight;
				if (msgData.Size() > 256000)
				{
					if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->BlockLandHeightChangeInit(msgData);
					msgData.Clear();
				}
			}
		}
		if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->BlockLandHeightChangeInit(msgData);
	}

	// transfer vody
	if (_waterLayer.IsCreated())
	{
		AutoArray<float> msgData;

		/// start data with dim of water height field and grid
		msgData.Append() = (float)_waterLayer.GetSizeX();
		msgData.Append() = (float)_waterLayer.GetSizeZ();
		msgData.Append() = _waterLayer.GetStep();    

		for (int nX = 0; nX < _waterLayer.GetSizeX(); ++nX)
		{
			for (int nZ = 0; nZ < _waterLayer.GetSizeZ(); ++nZ)
			{
				// v��ka    
				UNITPOS pos = { nX, nZ };     
				LANDHEIGHT nHeight = _waterLayer.GetHeight(pos);
				msgData.Append() = nHeight;          
				if (msgData.Size() > 256000)
				{
					if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->BlockWaterHeightChangeInit(msgData);
					msgData.Clear();
				}
			}
			if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->BlockWaterHeightChangeInit(msgData);
		}
	}
    
	if (actualLayer->CheckExplicitBimpas())
	{ 
		// transfer explicit texture grid
		AutoArray<int> msgData;
		for (int nX = 0; nX < m_ActualTextureLayer->m_nTexWidth; ++nX)
		{
			for (int nZ = 0; nZ < m_ActualTextureLayer->m_nTexWidth; ++nZ)
			{
				msgData.Add(m_ActualTextureLayer->m_explicitBimpas(nX, nZ));
			}
		}
    
		if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->BlockLandTextureChangeInit(msgData);
	}
	else
	{ 
		// transfer textur
		AutoArray<int> msgData;

		for (int nX = 0; nX < m_ActualTextureLayer->m_nTexWidth; ++nX)
		{
			for (int nZ = 0; nZ < m_ActualTextureLayer->m_nTexWidth; ++nZ)
			{
				UNITPOS Pos = { nX, nZ };
				CPosTextureTemplate* pLand = m_ActualTextureLayer->GetLandTextureAt(Pos);

				int& data = msgData.Append();

				if ((pLand != NULL) && (pLand->m_nTextureID != 0))
				{
					data = pLand->m_nTextureID;
				}
				else
				{
					data = 0;
				}
			}
		}
		if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->BlockLandTextureChangeInit(msgData);
	}

	// transfer registrovan�ch objekt�
	{
		int size = m_pDocument->ObjectTemplatesSize();
		for (int i = 0; i < size; ++i)
		{
			CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(m_pDocument->GetIthObjectTemplate(i));
			if (pTempl)
			{
				ASSERT_KINDOF(CPosObjectTemplate, pTempl);	

				CString fileName; 
				pTempl->GetBuldozerFileName(fileName);

				if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->RegisterObjectType(fileName);        
			}
		}    
	}
	
	// transfer instanc� objekt�
	{    
		for (int i = 0; i < m_InstanceObjs.GetSize(); ++i)
		{
			CPosEdObject* pObj = (CPosEdObject*)(m_InstanceObjs[i]);
			if (pObj)
			{
				ASSERT_KINDOF(CPosEdObject, pObj);

				SMoveObjectPosMessData data;
				CString fileName; 
				pObj->GetBuldozerFileName(fileName);
				pObj->SetObjectDataToMessage(data); 
				if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->ObjectCreate(data, fileName);
			}
		}
	}
  
	if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->EndBuffer();
	if (m_pDocument->RealViewer()) m_pDocument->RealViewer()->FileImportEnd();
	// transfer cursor position

	((CPosEdMainDoc*)m_pDocument)->m_CursorObject.SendPositionToBuldozer();
  
	AfxGetApp()->EndWaitCursor();
	//AfxMessageBox(IDS_END_TRANSFER_TO_BULD,MB_OK|MB_ICONINFORMATION);
}

/////////////////////////////////////////////////////////////////////////////
// pr�ce s lesy

CPosWoodObject*	CPoseidonMap::GetPoseidonWood(int nWoodID)
{
	if ((nWoodID<0)||(nWoodID >= m_WoodsInMap.GetSize()))
		return NULL;
	return ((CPosWoodObject*)(m_WoodsInMap[nWoodID]));
};

int CPoseidonMap::GetFreeWoodID() const
{	
	return m_WoodIDs.ReserveID();
};

void CPoseidonMap::AddNewWood(CPosWoodObject* pWood)
{	
	ASSERT(pWood->GetID()  != UNDEF_REG_ID);
	// vlo��m objekt do seznamu instanc� 

	if (pWood->GetID() >= m_WoodsInMap.GetSize())
		m_WoodsInMap.SetSize(pWood->GetID()+1);
	ASSERT(m_WoodsInMap[pWood->GetID()] == NULL);
	m_WoodsInMap[pWood->GetID()]	= pWood;

};

void CPoseidonMap::DeleteWood(int nWoodID)
{
	CPosWoodObject* pWood = GetPoseidonWood(nWoodID);
	if (pWood == NULL)
		return;
	ASSERT(pWood->GetID() == nWoodID);
	// odstran�m objekt
	m_WoodsInMap[nWoodID] = NULL;
  m_WoodIDs.ReleaseID(nWoodID);
	// optimalizuji pole
	int nIndx = m_WoodsInMap.GetSize()-1;
	while((nIndx>=0)&&(m_WoodsInMap[nIndx] == NULL))
	{
		m_WoodsInMap.SetSize(nIndx);
		nIndx--;
	}
	// zru��m objekt
	delete pWood;
};

/////////////////////////////////////////////////////////////////////////////
// pr�ce se s�t�mi

CPosNetObjectBase* CPoseidonMap::GetPoseidonNet(int nNetID)
{
	if ((nNetID < 0) || (nNetID >= m_NetsInMap.GetSize())) return NULL;
	return ((CPosNetObjectBase*)(m_NetsInMap[nNetID]));
}

int	CPoseidonMap::GetFreeNetID() const
{	
	return m_NetIDs.ReserveID();
}

void CPoseidonMap::AddNewNet(CPosNetObjectBase* pNet)
{	
	ASSERT(pNet->GetID() != UNDEF_REG_ID);
	// vlo��m objekt do seznamu instanc�
	if (pNet->GetID() >= m_NetsInMap.GetSize()) m_NetsInMap.SetSize(pNet->GetID() + 1);
	ASSERT(m_NetsInMap[pNet->GetID()] == NULL);
	m_NetsInMap[pNet->GetID()] = pNet;
}

void CPoseidonMap::DeleteNet(int nNetID)
{
	CPosNetObjectBase* pNet = GetPoseidonNet(nNetID);
	if (pNet == NULL) return;
	ASSERT(pNet->GetID() == nNetID);
	// odstran�m objekt
	m_NetsInMap[nNetID] = NULL;
	m_NetIDs.ReleaseID(nNetID);
	// optimalizuji pole
	int nIndx = m_NetsInMap.GetSize() - 1;
	while((nIndx >= 0) && (m_NetsInMap[nIndx] == NULL))
	{
		m_NetsInMap.SetSize(nIndx);
		nIndx--;
	}
	// zru��m objekt
	delete pNet;
}

// update registrovan�ch objekt� podle definovan�ch
void CPoseidonMap::UpdateNetsObjectsFromDefObjects(const CMgrNets* pMgr)
{
	for (int i = 0; i < m_NetsInMap.GetSize(); ++i)
	{
		CPosNetObjectBase* pNet = (CPosNetObjectBase*)(m_NetsInMap[i]);
		if (pNet != NULL) pNet->UpdateNetsObjectsFromDefObjects(pMgr);
	}
}

/////////////////////////////////////////////////////////////////////////////
// pr�ce s pojmenovan�mi plochami

CPosNameAreaObject*	CPoseidonMap::GetPoseidonNameArea(int nAreaID)
{
	if ((nAreaID<0)||(nAreaID >= m_AreasInMap.GetSize()))
		return NULL;
	return ((CPosNameAreaObject*)(m_AreasInMap[nAreaID]));
};

int CPoseidonMap::GetFreeNameAreaID() const
{	
	return m_AreaIDs.ReserveID();
};
void CPoseidonMap::AddNewNameArea(CPosNameAreaObject* pArea)
{	
	ASSERT(pArea->GetID() != UNDEF_REG_ID);
	// vlo��m objekt do seznamu instanc�
	if (pArea->GetID() >= m_AreasInMap.GetSize())
		m_AreasInMap.SetSize(pArea->GetID()+1);
	ASSERT(m_AreasInMap[pArea->GetID()] == NULL);
	m_AreasInMap[pArea->GetID()]	= pArea;	
};

void CPoseidonMap::DeleteArea(int nAreaID)
{
	CPosNameAreaObject* pArea = GetPoseidonNameArea(nAreaID);
	if (pArea == NULL)
		return;
	ASSERT(pArea->GetID() == nAreaID);
	// odstran�m objekt
	m_AreasInMap[nAreaID] = NULL;
  m_AreaIDs.ReleaseID(nAreaID);
	// optimalizuji pole
	int nIndx = m_AreasInMap.GetSize()-1;
	while((nIndx>=0)&&(m_AreasInMap[nIndx] == NULL))
	{
		m_AreasInMap.SetSize(nIndx);
		nIndx--;
	}
	// zru��m objekt
	delete pArea;
};

/////////////////////////////////////////////////////////////////////////////
// pr�ce s kl��ov�mi body

CPosKeyPointObject*	CPoseidonMap::GetPoseidonKeyPt(int nKeyPtID)
{
	if ((nKeyPtID<0)||(nKeyPtID >= m_KeyPtInMap.GetSize()))
		return NULL;
	return ((CPosKeyPointObject*)(m_KeyPtInMap[nKeyPtID]));
};

int	CPoseidonMap::GetFreeKeyPtID() const
{
	return m_KeyPtIDs.ReserveID();
};

void CPoseidonMap::AddNewKeyPt(CPosKeyPointObject* pKeyPt)
{
	ASSERT(pKeyPt->GetID()  != UNDEF_REG_ID);
	// vlo��m objekt do seznamu instanc�
	if (pKeyPt->GetID() >= m_KeyPtInMap.GetSize())
		m_KeyPtInMap.SetSize(pKeyPt->GetID()+1);
	ASSERT(m_KeyPtInMap[pKeyPt->GetID()] == NULL);
	m_KeyPtInMap[pKeyPt->GetID()]	= pKeyPt;
};
void CPoseidonMap::DeleteKeyPt(int nKeyPtID)
{
	CPosKeyPointObject* pKeyPt = GetPoseidonKeyPt(nKeyPtID);
	if (pKeyPt == NULL)
		return;
	ASSERT(pKeyPt->GetID() == nKeyPtID);
	// odstran�m objekt
	m_KeyPtInMap[nKeyPtID] = NULL;
  m_KeyPtIDs.ReleaseID(nKeyPtID);
	// optimalizuji pole
	int nIndx = m_KeyPtInMap.GetSize()-1;
	while((nIndx>=0)&&(m_KeyPtInMap[nIndx] == NULL))
	{
		m_KeyPtInMap.SetSize(nIndx);
		nIndx--;
	}
	// zru��m objekt
	delete pKeyPt;
};

/////////////////////////////////////////////////////////////////////////////
// vyhled�n� objekt� na map�

CPosObject* CPoseidonMap::GetObjectAt(CPoint ptAt,int nDif,const EnabledObjects& enabled)
{
	int i;
	// prohled�v�m klasick� objekty
	for(i=0; i<GetPoseidonObjectCount();i++)
	{
		CPosEdObject* pObject = GetPoseidonObject(i);
		if ( pObject!=NULL && pObject->IsEnabledObject(enabled))
		{
			if (pObject->IsPointAtArea(ptAt))
				return pObject;
		}
	}
	// prohled�v�m lesy
	for(i=0; i<GetPoseidonWoodsCount();i++)
	{
		CPosWoodObject* pObject = GetPoseidonWood(i);
		if ((pObject!=NULL)&&pObject->IsEnabledObject(enabled))
		{
			if (pObject->IsPointAtArea(ptAt))
				return pObject;
		};
	}
	// prohled�v�m s�t�
	for(i=0; i<GetPoseidonNetsCount();i++)
	{
		CPosNetObjectBase* pObject = GetPoseidonNet(i);
		if ((pObject!=NULL)&&pObject->IsEnabledObject(enabled))
		{
			if (pObject->IsPointAtArea(ptAt))
				return pObject;
		};
	}
	// prohled�v�m pojmenovan� plochy
	for(i=0; i<GetPoseidonNameAreaCount();i++)
	{
		CPosNameAreaObject* pObject = GetPoseidonNameArea(i);
		if ((pObject!=NULL)&&pObject->IsEnabledObject(enabled))
		{
			if (pObject->IsPointAtArea(ptAt))
				return pObject;
		};
	}
	// prohled�v�m kl��ov� body
	for(i=0; i<GetPoseidonKeyPtCount();i++)
	{
		CPosKeyPointObject* pObject = GetPoseidonKeyPt(i);
		if ((pObject!=NULL)&&pObject->IsEnabledObject(enabled))
		{
			if (pObject->IsPointAtArea(ptAt))
				return pObject;
		};
	}
	return NULL;
};

UNITPOS CPoseidonMap::GetSize() const
{
  UNITPOS unit;
  unit.x = m_nWidth;
  unit.z = m_nHeight;

  return unit;
}

FMntVector2 CPoseidonMap::GetRealSize() const
{
  UNITPOS unit;
  unit.x = m_nWidth;
  unit.z = m_nHeight;

  REALPOS real = FromUnitToRealPos(unit);
  return FMntVector2(real.x, real.z);
}
// Reposition objects to according to new RealSizeUnit
void CPoseidonMap::OnRealSizeUnitChange(float frac, bool recalcLandHeight)
{
  //Key points
  
  for(int i = 0; i < m_KeyPtInMap.GetSize(); i++)
  {
    CPosKeyPointObject * pKey = (CPosKeyPointObject *) m_KeyPtInMap[i];
    if (pKey == NULL)
      continue;

    pKey->CalcObjectPosPosition();
    pKey->CalcObjectLogPosition();
  }

  for(int i = 0; i < m_InstanceObjs.GetSize(); i++)
  {
    CPosEdObject * pObj = (CPosEdObject *) m_InstanceObjs[i];
    if (pObj == NULL)
      continue;

    pObj->m_Position(0,3) *= frac;
    pObj->m_Position(2,3) *= frac;

    pObj->CalcObjectLogPosition();
  }  

  for(int i = 0; i < m_NetsInMap.GetSize(); i++)
  {
    CPosNetObjectBase* pNet = (CPosNetObjectBase*)(m_NetsInMap[i]);
    if (pNet == NULL)
      continue;

    REALNETPOS pos = pNet->m_nBaseRealPos;

    pos.nPos.x *= frac;
    pos.nPos.z *= frac;

    pNet->DoUpdateNetPartsBaseRealPos(pos);  
    pNet->DoUpdateNetPartsLogPosition();
  }
}


CPosBackgroundImageObject * CPoseidonMap::GetBgImage(const CString &name) const
{
  for(int i = 0; i < m_BGImagesInMap.Size(); i++)
  {
    if (m_BGImagesInMap[i]->m_sAreaName == name)
      return m_BGImagesInMap[i];
  }

  return NULL;
}

CPosBackgroundImageObject * CPoseidonMap::GetBgImageAt(CPoint ptLogAt,int nDif,const CViewStyle* style) const
{
  if (style && style->m_bShowBGImage)  
    for(int i = 0; i < m_BGImagesInMap.Size(); i++)
    {
      if (m_BGImagesInMap[i]->m_Visible && m_BGImagesInMap[i]->IsPointAtArea(ptLogAt))
        return m_BGImagesInMap[i];
    }

  return NULL;
}

void CPoseidonMap::DeleteBgImage(const CString &name)
{
  for(int i = 0; i < m_BGImagesInMap.Size(); i++)
  {
    if (m_BGImagesInMap[i]->m_sAreaName == name)
    {
      delete m_BGImagesInMap[i];
      m_BGImagesInMap.Delete(i);
      return;
    }
  }
}


///////////////////////////////////////////////////////////////
// Texture can now have different step than land

BOOL CPoseidonMap::ChangeTextureLog(int nTexLog)
{
  AfxGetApp()->BeginWaitCursor();
  // alocate new memory for textures
  BOOL ret = m_ActualTextureLayer->ChangeTextureLog(nTexLog);
  AfxGetApp()->EndWaitCursor();
  return ret;
}

PTRTEXTURETMPLT	CPoseidonMap::GetPrimTextureAt(const UNITPOS& pos)const
{
  return m_ActualTextureLayer->GetPrimTextureAt(pos);
}
PTRTEXTURETMPLT	CPoseidonMap::GetBaseTextureAt(const UNITPOS& pos)const
{
  return m_ActualTextureLayer->GetBaseTextureAt(pos);
}

PTRTEXTURETMPLT	CPoseidonMap::GetBaseTextureAtTerrainGrid(const UNITPOS& pos)const
{
  return m_ActualTextureLayer->GetBaseTextureAtTerrainGrid(pos);
}

void CPoseidonMap::SetBaseTextureAt(const UNITPOS&pos ,PTRTEXTURETMPLT tex)
{
  return m_ActualTextureLayer->SetBaseTextureAt(pos, tex);
}

PTRTEXTURETMPLT	CPoseidonMap::GetLandTextureAt(const UNITPOS& pos)const
{
  return m_ActualTextureLayer->GetLandTextureAt(pos);
}

PTRTEXTURETMPLT	CPoseidonMap::GetLandTextureAtTerrainGrid(const UNITPOS& pos)const
{
  return m_ActualTextureLayer->GetLandTextureAtTerrainGrid(pos);
}

void CPoseidonMap::SetLandTextureAt(const UNITPOS& pos ,PTRTEXTURETMPLT tex,BOOL bFromBuldozer)
{
  return m_ActualTextureLayer->SetLandTextureAt(pos, tex, bFromBuldozer);
}


bool CPoseidonMap::LockedTexture(const UNITPOS& pos) const
{
  return m_ActualTextureLayer->Locked(pos);
}

void CPoseidonMap::LockTexture(const UNITPOS& pos, bool lock)
{
  m_ActualTextureLayer->Lock(pos, lock);
}

bool CPoseidonMap::LockedVertex(const UNITPOS& pos) const
{
  int p = pos.x+pos.z*m_nWidth;
  return m_landHeightLocked[p];
}

void CPoseidonMap::LockVertex(const UNITPOS& pos, bool lock )
{
  int p = pos.x+pos.z*m_nWidth;
  m_landHeightLocked[p] = lock;
}

void CPoseidonMap::DeleteTextureLayer(const CString &name)
{
  if (m_TextureLayers.Size() == 1)
  { // allways must be at least one layer
    ASSERT(FALSE); 
    return;
  }

  for(int i = 0; i < m_TextureLayers.Size(); i++)
  {
    if (m_TextureLayers[i]->m_Name == name)
    {
      if (m_TextureLayers[i] == m_ActualTextureLayer)
      {
        m_TextureLayers.Delete(i);
        SwitchActiveTextureLayer(m_TextureLayers[0]);
      }
      else
        m_TextureLayers.Delete(i);
      return;
    }
  }
}

void CPoseidonMap::SwitchActiveTextureLayer(Ref<CTextureLayer> layer)
{
  if (m_ActualTextureLayer != layer)
  {
    m_ActualTextureLayer->Deactivate();
    m_ActualTextureLayer = layer; 
    m_ActualTextureLayer->Activate();
    
    CUpdatePar cUpdate(CUpdatePar::uvfChngTextLayer, NULL);
    m_pDocument->UpdateAllViews(NULL, CUpdatePar::uvfChngTextLayer, &cUpdate);    
    TransferMapToBuldozer();
  }
}

CTextureLayer * CPoseidonMap::GetTextureLayer(const CString &name) const
{
  for(int i = 0; i < m_TextureLayers.Size(); i++)
    if (m_TextureLayers[i]->m_Name == name)
      return m_TextureLayers[i];

  return NULL;
}

int	CPoseidonMap::GetFreeObjectID() const
{  
  return m_ObjectIDs.ReserveID();
};

void CPoseidonMap::GetVertexesInvolved(AutoArray<UNITPOS>& vert, const CPositionBase& pos) const
{
  if (pos.IsKindOf(RUNTIME_CLASS(CPositionGroup)))
  {
    const CPositionGroup& grp = static_cast<const CPositionGroup&>(pos);
    for(int i = 0; i < grp.NPositions(); i++)
    {
      AutoArray<UNITPOS> vertTemp;
      GetVertexesInvolved(vertTemp, *grp.Position(i));

      // some vertexes from vertTemp can already exist in vert.
      int vertSize = vert.Size();
      for(int j = 0; j < vertTemp.Size(); j++)
      {
        UNITPOS& pos = vertTemp[j];
        int k = 0;
        for(; k < vertSize; k++)
        {
          if (vert[k].x == pos.x && vert[k].z == pos.z) 
            break;
        }

        if (k == vertSize)
          vert.Add(pos);      
      }
    }

    return;
  }

  // take big hull and then test triangles
  CRect rect = pos.GetPositionHull();

  // go through triangle
  int xs = rect.left / VIEW_LOG_UNIT_SIZE;
  int xe = rect.right / VIEW_LOG_UNIT_SIZE + 1; 

  int zs = rect.top / VIEW_LOG_UNIT_SIZE;
  int ze = rect.bottom / VIEW_LOG_UNIT_SIZE + 1; 

  int xSize = (xe - xs + 1);
  int zSize = (ze - zs + 1);

  bool * used = new bool[xSize * zSize];
  memset(used, 0x0, sizeof(bool) * xSize * zSize);
#define USED(xx,zz) used[((xx) - xs) * zSize + (zz) - zs]

  for(int x = xs; x < xe; x++)
    for(int z = zs; z < ze; z++)
    {
      CPositionMulti triangle;
      triangle.m_nCount = 3;
      triangle.m_Points[1] = CPoint(x * VIEW_LOG_UNIT_SIZE, z * VIEW_LOG_UNIT_SIZE);
      triangle.m_Points[0] = CPoint((x+1) * VIEW_LOG_UNIT_SIZE, z * VIEW_LOG_UNIT_SIZE);
      triangle.m_Points[2] = CPoint((x+1) * VIEW_LOG_UNIT_SIZE, (z + 1) * VIEW_LOG_UNIT_SIZE);

      triangle.RecalcPositionHull();

      if (pos.IsIntersection(triangle))
      {
        UNITPOS unitpos;
        if (!USED(x,z))
        {
          USED(x,z) = true;
          unitpos.x = x;
          unitpos.z = m_nHeight - z;
          vert.Add(unitpos);
        }

        if (!USED(x+1,z))
        {
          USED(x+1,z) = true;
          unitpos.x = x + 1;
          unitpos.z = m_nHeight - z;
          vert.Add(unitpos);
        }

        if (!USED(x+1,z+1))
        {
          USED(x+1,z+1) = true;
          unitpos.x = x+1;
          unitpos.z = m_nHeight - (z + 1);
          vert.Add(unitpos);
        }
      }

      triangle.m_Points[1] = CPoint(x* VIEW_LOG_UNIT_SIZE, z * VIEW_LOG_UNIT_SIZE);
      triangle.m_Points[0] = CPoint((x+1) * VIEW_LOG_UNIT_SIZE, (z+1) * VIEW_LOG_UNIT_SIZE);
      triangle.m_Points[2] = CPoint(x * VIEW_LOG_UNIT_SIZE, (z+1) * VIEW_LOG_UNIT_SIZE);

      triangle.RecalcPositionHull();

      if (pos.IsIntersection(triangle))
      {
        UNITPOS unitpos;
        if (!USED(x,z))
        {
          USED(x,z) = true;
          unitpos.x = x;
          unitpos.z = m_nHeight - z;
          vert.Add(unitpos);
        }

        if (!USED(x+1,z+1))
        {
          USED(x+1,z+1) = true;
          unitpos.x = x+1;
          unitpos.z = m_nHeight - (z+1);
          vert.Add(unitpos);
        }

        if (!USED(x,z+1))
        {
          USED(x,z+1) = true;
          unitpos.x = x;
          unitpos.z = m_nHeight - (z+1);
          vert.Add(unitpos);
        }
      }
    }

  delete [] used;
}

void CPoseidonMap::RecalculateObjectsSize()
{
  for(int i = 0; i < m_InstanceObjs.GetSize(); i++)
  {
    if (m_InstanceObjs[i])
      ((CPosEdObject *)m_InstanceObjs[i])->CalcObjectLogPosition();
  }
}

void CPoseidonMap::CropLand(const CRect &rc)
{
  int nTexSizeInSqLog=m_ActualTextureLayer->m_nTexSizeInSqLog;
  UNITPOS sz=GetSize();
  LANDHEIGHT *save=m_pLandHeight;
  m_pLandHeight=0;
  NewTerrain(rc.right-rc.left+1,rc.top-rc.bottom+1);
  for (int i=rc.left;i<=rc.right;i++)
    for (int j=rc.bottom;j<=rc.top;j++)
      SetLandscapeHeight(i-rc.left,j-rc.bottom,save[i+j*sz.x],true,false);
  
  delete [] save;
  m_TextureLayers.Clear();
  

  m_ActualTextureLayer = new CTextureLayer(*this);
  if (!m_ActualTextureLayer->OnNewLayer(rc.right-rc.left+1, rc.top-rc.bottom+1, nTexSizeInSqLog, CString(_T("Base"))))
    AfxThrowMemoryException();

  m_ActualTextureLayer->Activate();

  m_TextureLayers.Add(m_ActualTextureLayer);
  
  float left=rc.left*m_pDocument->m_nRealSizeUnit;
  float top=(rc.top+1)*m_pDocument->m_nRealSizeUnit;
  float right=(rc.right+1)*m_pDocument->m_nRealSizeUnit;
  float bottom=rc.bottom*m_pDocument->m_nRealSizeUnit;

  for (int i=0;i<m_InstanceObjs.GetSize();i++)
  {
    CPosEdObject *obj=(CPosEdObject *)(m_InstanceObjs[i]);
    if (obj)
    {    
      REALPOS rl=obj->GetRealPos();
      if (rl.x>=left && rl.x<right && rl.z>=bottom && rl.z<top)
      {
        rl.x-=left;
        rl.z-=bottom;
        obj->SetRealPos(rl);
        obj->CalcObjectLogPosition();
        obj->CalcRelHeightFromCurrPos();
      }
      else          
      {
        delete obj;
        m_InstanceObjs.SetAt(i,0);
      }
    }    
  }
  for (int i=0;i<m_NetsInMap.GetSize();i++)
  {
    CPosNetObjectBase *obj=(CPosNetObjectBase *)(m_NetsInMap[i]);
    if (obj)
    {
      REALNETPOS rl=obj->m_nBaseRealPos;
      if (rl.nPos.x>=left && rl.nPos.x<right && rl.nPos.z>=bottom && rl.nPos.z<top)
      {
        rl.nPos.x-=left;
        rl.nPos.z-=bottom;
        obj->m_nBaseRealPos=rl;
        obj->DoUpdateNetPartsBaseRealPos();
        
      }
      else
      {
        delete obj;
        m_NetsInMap.SetAt(i,0);
      }
    }
  }

  for (int i=0;i<m_KeyPtInMap.GetSize();i++)
  {
    CPosKeyPointObject* obj=(CPosKeyPointObject *)(m_KeyPtInMap[i]);
    if (obj)
    {    
      REALPOS rl=obj->m_ptRealPos;
      if (rl.x>=left && rl.x<right && rl.z>=bottom && rl.z<top)
      {
        rl.x-=left;
        rl.z-=bottom;
        obj->m_ptRealPos=rl;
        obj->CalcObjectLogPosition();
      }
      else
      {
        delete obj;
        m_KeyPtInMap.SetAt(i,0);
      }
    }
 }
}