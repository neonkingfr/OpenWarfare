/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPosObject

IMPLEMENT_DYNAMIC(CPosObject,CObject)

CPosObject::CPosObject() : _map(NULL), _locked(false),_hidden(false)
{};

CPosObject::CPosObject(const CPoseidonMap& map) : _map(&map), _locked(false),_hidden(false)
{};

CPosObject::~CPosObject()
{};

// vytvo�en� kopie objektu
CPosObject* CPosObject::CreateCopyObject() const
{
	CObject* pObject = NULL;
	TRY
	{
		pObject = GetRuntimeClass()->CreateObject();
		if (pObject)
		{
			ASSERT_KINDOF(CPosObject,pObject);
			((CPosObject*)pObject)->CopyFrom(this);
		} else
			AfxThrowUserException();
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pObject) delete pObject;
		pObject = NULL;
	}
	END_CATCH_ALL
	return ((CPosObject*)pObject);
};

void CPosObject::CopyFrom(const CPosObject* src)
{
  ASSERT( src->_map);
  _map = src->_map;
  _locked = src->_locked;

  if (src->m_LogPosition)
    m_LogPosition = src->m_LogPosition->CreateCopyObject();  
  else
    m_LogPosition = NULL;

  if (src->m_DevPosition)
    m_DevPosition = src->m_DevPosition->CreateCopyObject();
  else
    m_DevPosition = NULL;
}

void CPosObject::RecalcDevObjectPositionFromLP(CDC* pDC)
{
	CPositionBase* pLog = GetLogObjectPosition();
	CPositionBase* pDev = GetDevObjectPosition();
	ASSERT((pLog != NULL) && (pDev != NULL));
	if ((pLog != NULL) && (pDev != NULL))
	{
		pDev->CopyFrom(pLog);
		pDev->LPtoDP(pDC);
	}
}

BOOL CPosObject::IsObjectInRect(const CRect& rPos)
{
	CPositionBase* pPos = GetLogObjectPosition();
	if (pPos != NULL)
	{
		return pPos->IsAreaInRect(rPos);
	} 
	return FALSE;
};

BOOL CPosObject::IsObjectInRgn(const CRgn& rPos)
{
	CPositionBase* pPos = GetLogObjectPosition();
	if (pPos != NULL)
	{
		return pPos->IsAreaInRgn(rPos);
	} 
	return FALSE;
};

// pr�ce s trackerem
void CPosObject::CalcObjectTrackerPosition(CPositionTracker& Pos)
{	
	Pos.DoAddNewPos(GetLogObjectPosition()); 
	Pos.CalcAutoPositionCenter();
};


bool CPosObject::IsPointAtAreaReal(const REALPOS& pos) const
{
  return IsPointAtArea(_map->FromRealToViewPos(pos)) ? true : false;
}

void CPosObject::GetObjectHullReal(FltRect& rHull) const
{
  CRect rect;
  GetObjectHull(rect);

  rHull = _map->FromViewToRealRect(rect);
}

bool CPosObject::IsIntersection(const CPosObject& sec) const
{
  return GetLogObjectPosition()->IsIntersection(*sec.GetLogObjectPosition());
}

CPosEdBaseDoc * CPosObject::GetDocument() const 
{
  return _map->GetDocument();
};

void CPosObject::DoSerialize(CArchive& ar, DWORD nVer)
{ 
  if (ar.IsLoading() && nVer < 45) 
  {
    _locked = false;
    return;
  }

  MntSerializeBool(ar,_locked);
}

void CPosObject::GetObjectHull(CRect& rHull) const
{	
  rHull = m_LogPosition->GetPositionHull();
};

void CPosObject::OnDrawObject(CDC* pDC, const CDrawPar* pParams) const
{
	m_DevPosition->OnDrawObject(pDC, RGB(255, 255, 255), -1, RGB(0, 0, 0), -1, pParams);
}
