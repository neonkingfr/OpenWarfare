/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#if !defined(_POSEDMAPS_MAIN_H_)
#define _POSEDMAPS_MAIN_H_

#include <Es/Containers/array2D.hpp>
#include "StringList.h"

class CPoseidonMap;
class CPosObject;
class CPosEdObject; 
class CPosGroupObject;

/////////////////////////////////////////////////////////////////////////////
//  PoseidonEdMaps.h : header file
//	resource >> 14000


/////////////////////////////////////////////////////////////////////////////
//  FindSnapping

/** Functor finds neareast position where two objects are in touch according to snap points.
 */

class FindSnapping
{
protected:
  AutoArray<CPosEdObject *> m_SnapToObjects; // objects to which snaping has to be tested.
  float m_snapDist; // maximal allowed shift of objects.

  bool SnapObjects(CPoint& offsetLog, float& rotated, CPointVal originLog, 
    float & dist,  CPosEdObject& tracker, CPosEdObject& obj, CPoseidonMap& map);

  bool SnapPlugs(CPoint& offsetLog, 
    float& rotated, 
    CPointVal originLog, 
    float & dist,  
    CPosEdObject& tracker, 
    SnapPlug& trPlug, 
    CPosEdObject& obj, 
    SnapPlug& objPlug,
    CPoseidonMap& map);

public:

  FindSnapping(): m_snapDist(2.0f) {};

  const float& SetSnapDist(float snapDist) {return (m_snapDist = snapDist);};  
  void Reset() {m_SnapToObjects.Clear();};

  void AddSnapToObj(CPosEdObject * obj) {m_SnapToObjects.Add(obj);};

  /** Function finds the position, where are two objects (the first from m_SnapToObjects, the second from objs) in touch according to snap points (plugs). 
    * First is rotation aplied and then shift.
    * @param offsetLog - (in/out) shift in logical units
    * @param rotatedGrad - (in/out) rotation angle in Grad
    * @param originLog - (in) center of rotation in logical units
    * @param objs - (in) objects that will be shift and rotated. Functions
    * @param map - (in) map
    * @return true if position was found.
    */
  bool operator()(CPoint& offsetLog, float& rotatedGrad, CPointVal originLog, const CPosGroupObject& objs, CPoseidonMap& map);

  /** Function finds the position, where are two objects (the first from m_SnapToObjects, the second from objs) in touch according to snap points (plugs). 
  * First is rotation aplied and then shift.
  * @param snapped - (out) object from objs used for snapping
  * @param offsetLog - (in/out) shift in logical units
  * @param rotatedGrad - (in/out) rotation angle in Grad
  * @param originLog - (in) center of rotation in logical units
  * @param objs - (in) objects that will be shift and rotated. Functions
  * @param map - (in) map
  * @return true if position was found.
  */
  bool operator()(CPosEdObject *& snapped, CPoint& offsetLog, float& rotatedGrad, CPointVal originLog, const CPosGroupObject& objs, CPoseidonMap& map);
};
/////////////////////////////////////////////////////////////////////////////
// Poloha & Tracker

class CTrackerBase;
class CPosEdObject;

/////////////////////////////////////////////////////////////////////////////
// Rules for snapping

class /*AFX_EXT_CLASS*/ CPositionTracker : public CPositionGroup
{
  DECLARE_DYNAMIC(CPositionTracker);
public:
  CPositionTracker();

  // kopie objektu
  virtual void	CopyFrom(const CPositionBase*);

  // vykreslov�n� trackeru
  void	OnDrawObjectTrackInDP(CDC* pDC,const CTrackerBase*) const;
  void	OnDrawObjectTHitsInDP(CDC* pDC,const CTrackerBase*) const;

  // testov�n� polohy nad trackerem
  BOOL	IsPointAtTrackerPos(CPoint ptMouse,int nDif) const;
  int		GetHitPointsCount() const { return 0; };

  // v�po�et �chopov�ho boud
  void	CalcAutoPositionCenter();

  // posun plochy
  virtual void	OffsetPosition(int xAmount,int yAmount);
  virtual void  TransformPosition(FMntMatrix2Val trans, CPointVal origin);
  // p�evod na DP z LP
  virtual void	LPtoDP(CDC* pDC);
  // p�evod na DP ze Scrolleru
  virtual void	ScrolltoDP(CFMntScrollerView*);

  // data
public:
  CPoint	m_ptCenter;				// st�ed - �chopov� bod atributu
};

/////////////////////////////////////////////////////////////////////////////
// CBaseObjsTracker

#define MIN_MOUSE_DP_DIF	10	// posun my�i a� po pohybu o n bod�

class CPosGroupObject;

class /*AFX_EXT_CLASS*/ CTrackerBase : public CFMntScrollerTracker
{
  DECLARE_DYNAMIC(CTrackerBase)
public:
  CTrackerBase(CPosGroupObject*,CPoseidonMap*,CFMntScrollerView*);
  ~CTrackerBase();

  // Operations
  virtual void	Draw(CDC* pDC) ;
  void	DrawTrackerAttr(CDC* pDC,BOOL bErase);

  virtual int		HitTest(CPoint point) const;
  virtual int		HitTestHandles(CPoint point) const;
  virtual int		NormalizeHit(int nHandle) const;
  virtual void	GetTrueRect(LPRECT lpTrueRect) const;

  // track na vybran� plo�e
  enum TrackMode
  {
    trOffset = 0, 
    trRotate = 1
  };  

  void SetTrackMode(TrackMode mode) {m_trMode = mode;};
  //void EnableSnapMode(int snapDist, CSnapRulesContainer& rules);
  
  virtual BOOL	Track(CWnd* pWnd, CPoint * point);
  virtual BOOL	Track(CWnd* pWnd, CPoint point, BOOL bAllowInvert, CWnd* pWndClipTo = NULL);
  // p�esun atributu
  //virtual	BOOL	TrackAttrib(CWnd* pWnd, CPoint* ptFrom);
  virtual	BOOL	TrackHandle(int nHandle, CWnd* pWnd, CPoint* ptFrom);

  // return TRUE if continue
  virtual BOOL	OnMouseMessage(int nHandle,UINT nMsg, WPARAM wParam, LPARAM lParam, 
    CWnd* pWnd, CWnd* pWndClipTo); 
  void	SetTrackCursor();

  // modifications by scroller
  void	FromScrollToTracker(CPositionTracker* pDest,CPositionTracker* pSrc);
  virtual void	ScrollTracker(int xAmount,int yAmount);

  void OnChangeDP();

protected:
  void CollectSnapToObjects();

  FindSnapping m_snapping;
  void SnapTrackerToObjects();  
  // data
public:  
  CPoseidonMap*		m_pMap;

  TrackMode m_trMode;

  float m_rotatedGrad; // if tracker was rotated, this number shows how much...
  CPoint m_offset;     // if tracker was offseted, this vector shows how much...

  // parametry trackeru  
  BOOL m_bTrackVisible;	// tracker je viditeln�
  int m_nLastTrckCursor;	// kurzor my�i
  CPoint m_fixCenterOffset;	// difernce uchopen� od st�edu

  CPositionTracker	m_devPos;     // position of the selected objects in dev. coordinates
  CPositionTracker	m_logPos;			// position of the selected objects in log. coordinates
  CPositionTracker	m_finalDevPos;			// zpracov�van� poloha v dev. sou�ad.
  CPositionTracker	m_oldTrckPos;	// posledn� kreslen� poloha v DrawTrackerAttr  

  // Data for snapping
  bool m_bSnapping;
  //int m_SnapDistDev;
  AutoArray<CPosEdObject *> m_SnapToObjects;
  CPosGroupObject * m_grpObjects;   
};



/////////////////////////////////////////////////////////////////////////////
// CNetPartBase - z�kladn� ��st s�t�

// Will be used for walking on roads
typedef struct 
{
  REALPOS _pos;
  float   _grad;     // rotation of object in real coord (be ware it is opposite to rotations used in NetParts)
  int     _partIndx; // index of part in netlist
  int     _dirIndx;  // index of direction in road (defines netlist)
} NetWalkPoint;

TypeIsSimple(NetWalkPoint);

class CNetPartsList;

class /*AFX_EXT_CLASS*/ CNetPartBase : public CObject
{
	DECLARE_DYNAMIC(CNetPartBase)
public:
	enum 
	{
		netPartSTRA = 0, // rovinka
		netPartBEND = 1, // zat��ka
		netPartSPEC = 2, // spec. d�l
		netPartKEY  = 3, // kl��ov�
	};

	CNetPartBase();

	// nastaven� objektu
	virtual void MakeDefault();
	virtual void Destroy();
	virtual WORD GetNetPartType() const = 0;		

	// vytvo�en� kopie objektu
	CNetPartBase* CreateCopyObject() const;
	static CNetPartBase* CreateObjectType(int nType);
	virtual void CopyFrom(const CNetPartBase*);

	// update registrovan�ch objekt� podle definovan�ch
	virtual void UpdateNetsObjectsFromDefObjects(const CMgrNets*, const CNetPartsList*) 
	{
	}

	// serializace
	virtual void DoSerialize(CArchive& ar, DWORD nVer);
	virtual void DoUpdateFromTemplate(const CPoseidonMap* map) = 0;

	// informace o poloze objektu
	virtual const CPositionBase* GetLogObjectPosition() const = 0;
	virtual const CPositionBase* GetDevObjectPosition() const = 0;
	virtual void  RecalcDevObjectPositionFromLP(CDC*);

	// pr�ce s objekty Poseidon (Update a Vytvo�en�)
	virtual BOOL DoUpdatePoseidonNetObject(CPosEdObject*, const CPoseidonMap*);
	virtual BOOL DoUpdatePoseidonNetObjects(CObArray* ExistObjs, const CPoseidonMap*);
	virtual BOOL DoCreatePoseidonNetObject(CObArray* CreateObjs, const CPoseidonMap*);

	// pr�ce se sou�adnicemi objektu
	virtual BOOL IsPointAtArea(const POINT&) const;
	virtual bool IsIntersection(const CPosObject& sec) const;
	virtual void DoUpdateLogPosition(const CPoseidonMap*) = 0;
	REALPOS CalcRealPosFromRelativePos(REALPOS) const;
	REALPOS CalcRealPosFromRelativePos(Vector3) const;
	POINT   CalcLogPosFromRelativePoint(Vector3, const CPoseidonMap*) const;

	// pr�ce s v��kou
	virtual BOOL CanChangeSlope() const 
	{ 
		return FALSE; 
	} 	// lez m�nit sklon

	virtual BOOL DisChangeSlope() const 
	{ 
		return TRUE;  
	} 	// zak�z�na zm�na sklonu

	virtual double GetCurrentSlopeRad() const 
	{ 
		return 0.; 
	}   // sklon d�lu

	// vykreslov�n� objektu
	virtual void OnDrawObject(CDC* pDC, const CDrawPar* pParams, bool locked) const = 0;

	Ref<CPosObjectTemplate> GetObjectTemplate(const CPoseidonMap *map) const;

	// NetWalkPoint
	virtual void AdvanceWalkPoint(NetWalkPoint& wpt, float& dist) const {};

	// data
public:
	REALNETPOS m_nBaseRealPos;		// z�kladn� re�ln� poloha bodu A pro kl��ov� bod,

protected:
  /*+++ Obsolete roads are placed by engine onto ground +++*/
  //BOOL			  m_bAutoHeight;		// v��ku lze m�nit automaticky
  //double			m_nRelaHeight;		// relativn� v��ka s�t� v bod� (A) nad povrchem
  /*--- Obsolete roads are placed by engine onto ground ---*/
public:

	// identifikace objekt� Poseidon
	int                     m_nPosObjectID;  // ID objektu poseidon (nebo UNDEF_REG_ID)
	Ref<CPosObjectTemplate> m_pCreateObject; // �ablona pro nov� objekt nebo NULL
};

// CNetPartSIMP - ��st s�t� - rovinka v. zat��ka
class /*AFX_EXT_CLASS*/ CNetPartSIMP : public CNetPartBase
{
	DECLARE_DYNAMIC(CNetPartSIMP)
public:
	CNetPartSIMP();
	~CNetPartSIMP();

	// vytvo�en� kopie objektu
	virtual	void CopyFrom(const CNetPartBase*);

	// serializace
	virtual	void DoSerialize(CArchive& ar, DWORD nVer);

	// informace o poloze objektu
	virtual const CPositionBase* GetLogObjectPosition() const 
	{ 
		return (&m_LogPosition); 
	}

	virtual const CPositionBase* GetDevObjectPosition() const 
	{ 
		return (&m_DevPosition); 
	}

	// pr�ce se sou�adnicemi objektu
	virtual void DoUpdateLogPosition(const CPoseidonMap*);
	virtual REALNETPOS CalcABasePosition(REALNETPOS PosConn, const CPoseidonMap*) = 0;
	virtual REALNETPOS GetEndRealPos() const 
	{
		return m_nBaseRealPos;
	}

	virtual REALPOS CalcBankRealPosition(int nPt) const;  // poloha n�spu pro ptNetLB,ptNet...

	// pr�ce s v��kou
	virtual	BOOL CanChangeSlope() const 
	{ 
		return m_bCanChangePartSlope; 
	} 	// lez m�nit sklon
	
	virtual double GetCurrentSlopeRad() const;		// sklon d�lu

	// vykreslov�n� objektu
	virtual	void OnDrawObject(CDC* pDC, const CDrawPar* pParams, bool locked) const;
	virtual	void OnDrawObject(CDC* pDC, const CDrawPar* pParams, CBrush*, BOOL bFill) const;

	// testov�n� d�lu
	WORD DoTestNetPart_All(REALNETPOS nPrvHg, double nPrvSl, const CNetPartsList* pList, const CPoseidonMap*) const;
	BOOL DoTestNetPart_PrvHg(REALNETPOS nPrvHg, const CNetPartsList* pList) const;
	BOOL DoTestNetPart_NetUp(const CNetPartsList* pList, const CPoseidonMap*) const;
	BOOL DoTestNetPart_NetDn(const CNetPartsList* pList, const CPoseidonMap*) const;
	BOOL DoTestNetPart_Slope(const CNetPartsList* pList) const;
	BOOL DoTestNetPart_PrvSl(double nPrvSl, const CNetPartsList* pList) const;  

	// data
public:
	WORD    m_nPartSubtype;		// podtyp dle vzoru (m_nSTRAType,m_nBENDType)
	CString m_nComponentName;	// jm�no komponenty

	REALNETPOS m_SectionBeginA;	// relativn� poloha �sek� A,B
	REALNETPOS m_SectionBeginB;	
	double     m_nRelBankWidth;	// rel. vzd�lenost n�spu od st�edu vozovky

	// body s�ov�ho objektu relativn� k A
	enum 
	{
		ptNetCount = 4,
		ptNetLB = 0, 
		ptNetPB = 1,
		ptNetLE = 2, 
		ptNetPE = 3, 
	};

	Vector3 m_ptRelBoundingCenter;	// bounding center relativn� k A
	Vector3 m_ptRelNetObjPoints[ptNetCount];

	// sou�adnice objektu - nejsou serializov�na (obnovena po serializaci)
	CPositionPoly m_LogPosition;		// logick� sou�adnice objektu
	CPositionPoly m_DevPosition;		// device  sou�adnice

	REALNETPOS m_nConnRealPos;		// re�ln� poloha bodu B pro kl��ov� bod,

	WORD m_nTestState;		// testov�n� um�st�n� d�lu
	enum 
	{
		tstStateOK		= 0,
		tstStateUNDEF	= 0xFFFF,
		// chybov� k�dy
		tstErrBadPrvHg	= 0x0001,	// nenavazuje na p�edchoz� d�l
		tstErrBadNetUp	= 0x0010,	// s� "plave" na povrchem
		tstErrBadNetDn	= 0x0020,	// si� "zabo�ena" pod povrch
		tstErrBadSlope  = 0x0100,	// �patn� sklon d�lu
		tstErrBadPrvSl  = 0x1000,	// �patn� sklon k p�edchoz�mu d�lu
	};

	// lze m�nit sklon d�lu
	BOOL m_bCanChangePartSlope;		
};

// CNetPartSTRA - ��st s�t� - rovinka
class /*AFX_EXT_CLASS*/ CNetPartSTRA : public CNetPartSIMP
{
	DECLARE_DYNCREATE(CNetPartSTRA)
public:
	CNetPartSTRA();

	// nastaven� objektu
	virtual WORD GetNetPartType() const 
	{ 
		return CNetPartBase::netPartSTRA; 
	}		

	// vytvo�en� kopie objektu
	virtual	void CopyFrom(const CNetPartBase*);

	// update registrovan�ch objekt� podle definovan�ch
	virtual	void UpdateNetsObjectsFromDefObjects(const CMgrNets*, const CNetPartsList*);

	// serializace
	virtual	void DoSerialize(CArchive& ar, DWORD nVer);
	virtual	void DoUpdateFromTemplate(const CPoseidonMap* map);

	// vytvo�en� objektu
	void CreateFromTemplateSTRA(const CNetSTRA*, const CNetPartsList*);
	virtual REALNETPOS CalcABasePosition(REALNETPOS PosConn, const CPoseidonMap*);

	// pr�ce s objekty Poseidon (Update a Vytvo�en�)
	virtual BOOL DoUpdatePoseidonNetObject(CPosEdObject*, const CPoseidonMap*);

	// pr�ce s v��kou
	virtual BOOL DisChangeSlope() const 
	{ 
		return m_bDisChangeSlope;  
	} 	// zak�z�na zm�na sklonu

	// NetWalkPoint
	virtual void AdvanceWalkPoint(NetWalkPoint& wpt, float& dist) const;

	// data
public:
	BOOL m_bDisChangeSlope;		// zak�z�na zm�na sklonu
};

// CNetPartSPEC - ��st s�t� - spec. �sek
class /*AFX_EXT_CLASS*/ CNetPartSPEC : public CNetPartSIMP
{
	DECLARE_DYNCREATE(CNetPartSPEC)
public:
	CNetPartSPEC();

	// nastaven� objektu
	virtual WORD GetNetPartType() const 
	{ 
		return CNetPartBase::netPartSPEC; 
	}

	// vytvo�en� kopie objektu
	virtual	void CopyFrom(const CNetPartBase*);

	// update registrovan�ch objekt� podle definovan�ch
	virtual	void UpdateNetsObjectsFromDefObjects(const CMgrNets*, const CNetPartsList*);

	// serializace
	virtual	void DoSerialize(CArchive& ar, DWORD nVer);
	virtual	void DoUpdateFromTemplate(const CPoseidonMap *map);

	// vytvo�en� objektu
	void CreateFromTemplateSPEC(const CNetSPEC*, const CNetPartsList*);
	virtual REALNETPOS CalcABasePosition(REALNETPOS PosConn, const CPoseidonMap*);

	// pr�ce s objekty Poseidon (Update a Vytvo�en�)
	virtual BOOL DoUpdatePoseidonNetObject(CPosEdObject*, const CPoseidonMap*);

	// pr�ce s v��kou
	virtual BOOL DisChangeSlope() const 
	{ 
		return m_bDisChangeSlope;  
	} 	// zak�z�na zm�na sklonu

	// Net Walk Point
	void AdvanceWalkPoint(NetWalkPoint& wpt, float& dist) const;

	// data
public:
	BOOL m_bDisChangeSlope;		// zak�z�na zm�na sklonu
};

// CNetPartBEND - ��st s�t� - zat��ka
class /*AFX_EXT_CLASS*/ CNetPartBEND : public CNetPartSIMP
{
	DECLARE_DYNCREATE(CNetPartBEND)
public:
	CNetPartBEND();

	// nastaven� objektu
	virtual WORD GetNetPartType() const 
	{ 
		return CNetPartBase::netPartBEND; 
	}

	// vytvo�en� kopie objektu
	virtual	void CopyFrom(const CNetPartBase*);

	// update registrovan�ch objekt� podle definovan�ch
	virtual	void UpdateNetsObjectsFromDefObjects(const CMgrNets*, const CNetPartsList*);

	// serializace
	virtual	void DoSerialize(CArchive& ar, DWORD nVer);
	virtual	void DoUpdateFromTemplate(const CPoseidonMap* map);

	// vytvo�en� objektu
	void CreateFromTemplateBEND(const CNetBEND*, BOOL bLeft, const CNetPartsList*);
	virtual REALNETPOS CalcABasePosition(REALNETPOS PosConn, const CPoseidonMap*);
	virtual REALNETPOS GetEndRealPos() const;
	virtual REALPOS CalcBankRealPosition(int nPt) const;  // poloha n�spu pro ptNetLB,ptNet...

	// pr�ce s v��kou
	virtual BOOL DisChangeSlope() const 
	{ 
		return FALSE;  
	}	// zak�z�na zm�na sklonu

	// pr�ce s objekty Poseidon (Update a Vytvo�en�)
	virtual BOOL DoUpdatePoseidonNetObject(CPosEdObject*, const CPoseidonMap*);

	// NetWalkPart
	virtual void AdvanceWalkPoint(NetWalkPoint& wpt, float& dist) const;

	// data
public:
	BOOL m_bLeftBend;	// zat��ka doleva (else doprava)
};

// CNetPartKEY	- ��st s�t� - kl��ov� bod
class /*AFX_EXT_CLASS*/ CNetPartKEY : public CNetPartBase
{
	DECLARE_DYNCREATE(CNetPartKEY)
public:
	CNetPartKEY();

	// nastaven� objektu
	virtual void MakeDefault();
	virtual WORD GetNetPartType() const 
	{ 
		return CNetPartBase::netPartKEY; 
	}		

	// vytvo�en� kopie objektu
	virtual	void CopyFrom(const CNetPartBase*);
	// update registrovan�ch objekt� podle definovan�ch
	virtual	void UpdateNetsObjectsFromDefObjects(const CMgrNets*, const CNetPartsList*);

	// serializace
	virtual	void DoSerialize(CArchive& ar, DWORD nVer);
	virtual	void DoUpdateFromTemplate(const CPoseidonMap* map);

	// vytvo�en� podle �ablony
	BOOL CreateFromTemplateCross(const CPosCrossTemplate*);
	BOOL CreateFromTemplateStra(const CPosNetTemplate*, const CNetBaseStra*);
	BOOL CreateFromTemplateSpec(const CPosNetTemplate*, const CNetBaseStra*);
	BOOL CreateFromTemplateTerm(const CPosNetTemplate*, const CNetTERM*);

	// informace o poloze objektu
	virtual const CPositionBase* GetLogObjectPosition() const 
	{ 
		return (&m_LogPosition); 
	}

	virtual const CPositionBase* GetDevObjectPosition() const 
	{ 
		return (&m_DevPosition); 
	}

	bool IsIntersection(const CPosObject&) const;

	// pr�ce se sou�adnicemi objektu
	virtual void DoUpdateLogPosition(const CPoseidonMap*);
	// pro Section vrac� odpov�daj�c� polohu (m_nBaseRealPos seznamu)
	REALNETPOS GetRealPosForSection(int nSection, const CPoseidonMap*) const;

	// pr�ce s objekty Poseidon (Update a Vytvo�en�)
	virtual BOOL DoUpdatePoseidonNetObject(CPosEdObject*, const CPoseidonMap*);
	
	// vykreslov�n� objektu
	virtual	void OnDrawObject(CDC* pDC, const CDrawPar* pParams, bool locked) const;

	bool PrepareForPaste(const CPosEdBaseDoc &);

	// data
public:
	enum	
	{	
		typeCross = 0, 
		typeStra  = 1, 
		typeSpec  = 2, 
		typeTerm  = 3, 
		typeBend  = 4
	};

	int  m_nKeyPartType;		// typ kl��ov�ho bodu - type???
	WORD m_nKeyPartSubtype;	// podtyp dle vzoru (m_nSTRAType)

	CString	m_nTemplateName;	// jm�no s�t�( u k�i�ovatky empty )
	CString	m_nComponentName;	// jm�no objektu

	/*+++ Obsolete roads are placed by engine onto ground +++*/
	//double		m_nMinPartHg;		// min & max. v��ka vzhledem k povrchu
	//double		m_nMaxPartHg;
	/*--- Obsolete roads are placed by engine onto ground ---*/

	COLORREF m_clrNor;			// barva - norm�ln�
	BOOL     m_bFillEntire;		// vypl� zobrazen�

	// existence �sek�
	enum	
	{   
		maxSections = 4 
	};	// max. po�et �sek�

	int			m_nCountSections;	// po�et �sek�
	REALNETPOS	m_SectionBegins[maxSections];	// relativn� poloha �sek�
	CString		m_SectionNetType[maxSections];	// jm�no s�t� pro �sek

	// body s�ov�ho objektu relativn� k A
	enum 
	{
		ptNetCount = 8,
		ptNetLB = 0, 
		ptNetPB = 1,
		ptNetLE = 2, 
		ptNetPE = 3,
		ptNetLH = 4, 
		ptNetLD = 5,
		ptNetPH = 6, 
		ptNetPD = 7, 
	};

	Vector3	m_ptRelBoundingCenter;			// bounding center relativn� k A
	Vector3	m_ptRelNetObjPoints[ptNetCount];

	// sou�adnice objektu - nejsou serializov�na (obnovena po serializaci)
	CPositionMulti	m_LogPosition;		// logick� sou�adnice objektu
	CPositionMulti	m_DevPosition;		// device  sou�adnice
};

// CNetPartsList - posloupnost ��st� s�t� (CNetPartSTRA,CNetPartBEND)
class /*AFX_EXT_CLASS*/ CNetPartsList : public CObject
{
	DECLARE_DYNAMIC(CNetPartsList)
public:
	CNetPartsList();
	~CNetPartsList();

	// nastaven� objektu
	virtual void Destroy();

	// vytvo�en� kopie objektu
	virtual void CopyFrom(const CNetPartsList*);
	void CreateFromNetTemplate(const CPosNetTemplate*);

	// update registrovan�ch objekt� podle definovan�ch
	virtual void UpdateNetsObjectsFromDefObjects(const CMgrNets*);

	// serializace
	virtual void DoSerialize(CArchive& ar, DWORD nVer);
	virtual void DoUpdateFromTemplate(const CPoseidonMap *map);

	// informace o poloze objektu
	virtual void RecalcDevObjectPositionFromLP(CDC*);
	virtual void ResetDevObjectPosition();

	// pr�ce s objekty Poseidon (Update a Vytvo�en�)
	virtual BOOL DoUpdatePoseidonNetObjects(CObArray* ExistObjs, const CPoseidonMap*);
	virtual BOOL DoCreatePoseidonNetObjects(CObArray* CreateObjs, const CPoseidonMap*);

	// pr�ce se sou�adnicemi objektu
	void GetObjectHull(CRect& rHull) const;
	virtual BOOL IsPointAtArea(const POINT&) const;
	virtual bool IsIntersection(const CPosObject& sec) const;

	// pr�ce se sou�adnicemi objektu
	void DoUpdateNetPartsBaseRealPos(REALNETPOS, const CPoseidonMap*);
	REALNETPOS GetNetPartsEndRealPos();
	double GetEndNetSlope() const;

	virtual void DoUpdateLogPosition(const CPoseidonMap*);

	// vykreslov�n� objektu
	virtual void OnDrawObject(CDC* pDC, const CDrawPar* pParams, bool locked) const;

	bool PrepareForPaste(const CPosEdBaseDoc& doc);
  
	// Walk on net
	virtual bool GetWalkPoint(NetWalkPoint&) const;
	virtual bool AdvanceWalkPoint(NetWalkPoint&, float dist) const;

	// data
public:
	CObArray m_NetParts;
	CString  m_nTemplateName;	// jm�no s�t�

	REALNETPOS m_nBaseRealPos;		// z�kladn� re�ln� poloha n�vazn�ho bodu 1.objektu

	// barvy s�t�
	COLORREF m_clrKey;			// barva - kl��ov�
	COLORREF m_clrNor;			// barva - norm�ln�
	BOOL     m_bFillEntire;		// vypl� zobrazen�

protected:
	/*+++ Obsolete roads are placed by engine onto ground +++*/
	//double			m_nMinHeight;		// um�st�n� d�lu vzhledem k povrchu
	//double			m_nMaxHeight;
	//double			m_nStdHeight;	
	//BOOL			m_bAutoHeight;		// automatick� p�ilnavost k povrchu
	/*--- Obsolete roads are placed by engine onto ground ---*/

public:
	double m_nMaxSlope;		// maxim�ln� sklon
	double m_nPartSlope;		// maxim�ln� �hle mezi d�ly
};

/////////////////////////////////////////////////////////////////////////////
// CPosObject - z�kladn� viditeln� objekt
typedef AutoArray<float> RealRect;

// z�kladn� zobraziteln� objekt
class CPosObject : public CObject
{
	DECLARE_DYNAMIC(CPosObject)
protected:
	const CPoseidonMap* _map;
	bool _locked; 
	bool _hidden;

	// poloha objektu ve view
	SRef<CPositionBase> m_LogPosition;		// logick� sou�adnice objektu
	SRef<CPositionBase> m_DevPosition;		// dev. sou�adnice - temporary !!!

	CPosObject();

public:  

	CPosObject(const CPoseidonMap& map);
	virtual ~CPosObject();

	// vytvo�en� kopie objektu
	CPosObject* CreateCopyObject() const;
	virtual void CopyFrom(const CPosObject*) = 0;
	// serializace
	virtual void DoSerialize(CArchive& ar, DWORD nVer);

	// informace o poloze objektu
	virtual CPositionBase* GetLogObjectPosition() 
	{
		return m_LogPosition;
	}

	virtual const CPositionBase* GetLogObjectPosition() const 
	{
		return m_LogPosition;
	}

	virtual CPositionBase* GetDevObjectPosition()  
	{
		return m_DevPosition;
	}

	virtual void RecalcDevObjectPositionFromLP(CDC*);

	// informace o objektu
	virtual void GetObjectName(CString& sText) const = 0;		
	virtual CString GetObjectName() const 
	{
		CString sText; 
		GetObjectName(sText); 
		return sText;
	}

	// Info will be viewed in StatusBar... 
	virtual CString GetStatusInfo() const 
	{
		CString sText; 
		GetObjectName(sText); 
		return sText;
	}

	virtual void GetObjectHull(CRect& rHull) const;
	
	CRect GetObjectHull() const 
	{
		CRect rect; 
		GetObjectHull(rect); 
		return rect;
	}

	void GetObjectHullReal(FltRect& rHull) const;
	virtual bool IsIntersection(const CPosObject& sec) const; 

	virtual BOOL IsPointAtArea(const POINT& pt) const 
	{
		return m_LogPosition->IsPointAtArea(pt);
	}

	bool IsPointAtAreaReal(const REALPOS&) const;

	virtual BOOL IsSameObject(const CPosObject& obj) const = 0;

	virtual BOOL IsEnabledObject(const EnabledObjects&) const 
	{ 
		return !_hidden; 
	}

	BOOL IsObjectInRect(const CRect&);
	BOOL IsObjectInRgn(const CRgn&);

	// pr�ce s trackerem
	virtual void CalcObjectTrackerPosition(CPositionTracker& Pos);

	// editace objektu
	virtual BOOL DoEditObject()	
	{ 
		return !Locked(); 
	}

	virtual BOOL CanEditObject() const 
	{ 
		return FALSE; 
	}

	// locking status
	bool Locked() const 
	{
		return _locked;
	}

	void Lock(bool on = true) 
	{
		_locked = on;
	}

	bool Hidden() const 
	{
		return _hidden;
	}

	void Hide(bool on = true) 
	{ 
		_hidden = on;
	}

	// vykreslov�n� objektu
	virtual void OnDrawObject(CDC* pDC, const CDrawPar* pParams) const;

	CPosEdBaseDoc* GetDocument() const;  

	virtual int GetID() const  
	{
		return -1;
	}

	virtual int SetFreeID() 
	{
		return -1;
	}
};

// CPosEdObject - editovan� podoba instance objektu
class /*AFX_EXT_CLASS*/ CPosEdObject : public CPosObject
{
	DECLARE_DYNCREATE(CPosEdObject)
	typedef CPosObject base;
	CPosEdObject() 
	{
	}

public:
	CPosEdObject(const CPoseidonMap& map);

	// vytvo�en� kopie objektu
	virtual void CopyFrom(const CPosObject*);
	// serializace
	virtual void DoSerialize(CArchive& ar, DWORD nVer);

	// informace o objektu
	virtual void GetObjectName(CString& sText) const;	
	virtual CString GetStatusInfo() const;

	virtual BOOL IsEnabledObject(const EnabledObjects&) const;
	virtual BOOL IsSameObject(const CPosObject& obj) const;

	CString& GetBuldozerFileName(CString&) const; // realativn� cesta k souboru pac (m_sObjFile)
	
	CString  GetBuldozerFileName() const 
	{
		CString temp; 
		return GetBuldozerFileName(temp); 
	}

	// vlo�� ID,jm�no a polohu objektu do zpr�vy
	void SetObjectDataToMessage(SObjectPosMessData&) const;
	void SetObjectDataToMessage(SMoveObjectPosMessData&) const;

	// vytvo�en� z buldozeru
	void CreateFromBuldozerMsg(const SPosMessage&);
	// vytvo�en� ze �ablony na pozici
	void CreateFromTemplateAt(Ref<CPosObjectTemplate>, REALPOS);

	void CreateFromTemplateAt(Ref<CPosObjectTemplate>, CPosEdObject&);

	void AllocPositions();
protected:
	// randomizace objektu podle �ablony
	void RandomFromTemplateAt(Ref<CPosObjectTemplate>, REALPOS);

public:
	// v�po�et logick�ch sou�adnic objektu
	void CalcObjectLogPosition();  

	void  CalcObjectPoseidonHeight();
	float CalcRelHeightFromCurrPos() const;

	/** Function made just for one reason. The calculation of relative height was changed. 
	*  The function recalculates relative height according to new system
	*/
	void RepairObjectPoseidonRelHeight(); 

	// �prava polohy objekt�
	void CalcOffsetLogPosition(int xAmount, int yAmount);
	void CalcRotationPosition(double nGrad);
	void CalcRotationPosition(double nGrad, CPointVal originLog);
	void CalcVertRotXPosition(double nGrad);

	// editace objektu
	virtual BOOL DoEditObject();
	virtual BOOL CanEditObject() const 
	{ 
		return TRUE; 
	}

/*
  virtual BOOL	DoPositionObject(const CPoseidonMap* pMap);
  virtual BOOL	CanPositionObject() const { return TRUE; };
  virtual BOOL	DoRotationObject(const CPoseidonMap* pMap);
  virtual BOOL	CanRotationObject() const { return TRUE; };
*/

	Vector3 PositionModelToWorld(const Vector3& v) const;
	void RepairMatrix();

	Ref<CPosObjectTemplate> GetTemplate() const;

	REALPOS GetRealPos() const 
	{
		return REALPOS(m_Position(0, 3), m_Position(2, 3));
	}

	/// Finction returns position of the [0,0,0] point from model.
	REALPOS GetRealZeroPos() const;

	double GetGradRotation() const;
	double GetGradSkew() const;

	float GetRelHeight() const 
	{
		return m_nRelHeight;
	}

	void SetRelHeight(float relHeight) 
	{
		m_nRelHeight = relHeight; 
		CalcObjectPoseidonHeight();
	}

	void SetRealPos(const REALPOS& pos);

	/// clear object name and ID...
	void MakeUndef();

	// vykreslov�n� objektu
	virtual void	OnDrawObject(CDC* pDC, const CDrawPar* pParams) const;

	int GetID() const 
	{
		return m_nObjectID;
	}

	int SetFreeID();
  
protected:
	CString m_sObjName;			// jm�no objektu
	int     m_nObjectID;		// ID objektu  

public:

	Matrix4 m_Position;	// poloha objektu

	float  m_nRelHeight;		// relativn� v��ka (vzhledem k povrchu)
	double m_nSizeScale;		// velikost objektu (v %)

	// master objekty
	int m_nMasterWood;		// ��d�c� les
	int m_nMasterNet;		// ��d�c� s�

	// registrovan� �ablona objektu
	int m_nTemplateID; 
	Ref<CPosObjectTemplate> m_pTemplate;
  
	// p��znak pou��t� objektu - TEMPORARY !
	int m_nTempObjCounter;

	// Reserved -- for old formats
	CString m_tempString;
};

TypeIsSimple(CPosEdObject*)

// CPosAreaObject - editovan� plocha na map�
class /*AFX_EXT_CLASS*/ CPosAreaObject : public CPosObject
{
  DECLARE_DYNCREATE(CPosAreaObject)
  typedef CPosObject base;
protected:
   CPosAreaObject();
public:
  CPosAreaObject(const CPoseidonMap& map);
 
  ~CPosAreaObject();

  // vytvo�en� kopie objektu
  virtual	void	CopyFrom(const CPosObject*);
  // serializace
  void PreSerializeStoring(const CPoseidonMap& map);
  virtual	void	DoSerialize(CArchive& ar, DWORD nVer);
  void CalcObjectLogPosition(); // do not call this function, 
  // except serialization or when PreSerializeStoring was called

  virtual CPositionArea* GetLogObjectPosition() {return static_cast<CPositionArea*>(m_LogPosition.GetRef());};
  virtual const CPositionArea* GetLogObjectPosition() const {return static_cast<CPositionArea*>(m_LogPosition.GetRef());}; 
  
 
  // informace o objektu
  virtual void	GetObjectName(CString& sText) const;				 

  virtual BOOL IsSameObject(const CPosObject & obj) const;

  // vykreslov�n� objektu
protected:
  virtual void	GetDrawAreaColors(COLORREF& cEntire,COLORREF& cFrame,int& nStyle,const CDrawPar*) const;

public:
  void	OnDrawObject(CDC* pDC,const CDrawPar* pParams) const;

  // pr�ce s plochou
  BOOL	IsValidArea() const;

  void	RemoveRect(const RECT&);
  void	AddRect(const RECT&);
  void	RemoveRectReal(const FltRect&);
  void	AddRectReal(const FltRect&);
  void	ResetArea();

  int NRects() const;
  CRect Rect(int i) const;
  
  // data
protected:
  CString			m_sAreaName;		// jm�no pojmenovan� plochy
  COLORREF		m_cAreaEntire;		// barvy pro vykreslen�
  COLORREF		m_cAreaFrame;
  int				  m_nAreaStyle;		// styl vykreslen�

  enum {
    drStyleFull = 0,
    drStyleHori = 1,  drStyleVert = 2,  drStyleCros = 3,
    drStyleLfRg = 4,  drStyleRgLf = 5,  drStyleDgCr = 6, 
    drStyleDef	= 7 
  };

  AutoArray<float> m_RealPosition; // auxiliary data member, use only during serialization
                                   // contains rectangle coordinates... 
};	

// CPosWoodObject - kompletn� objekt lesa
class /*AFX_EXT_CLASS*/ CPosWoodObject : public CPosAreaObject
{
  typedef CPosAreaObject base;
  DECLARE_DYNCREATE(CPosWoodObject)
protected:
  CPosWoodObject();
public:
  CPosWoodObject(const CPoseidonMap & map);
  

  // vytvo�en� kopie objektu
  virtual	void	CopyFrom(const CPosObject*);
  // serializace
  virtual	void	DoSerialize(CArchive& ar, DWORD nVer);
protected:
  virtual void	GetDrawAreaColors(COLORREF& cEntire,COLORREF& cFrame,int& nStyle,const CDrawPar*) const;
public:
  // vytvo�en� ze �ablony na pozici
  void	CreateFromTemplateAt(const CPosWoodTemplate*,const CPosAreaObject*, int ID);
  CPosWoodTemplate* GetWoodTemplate() const;
  CString GetTemplateName() const {return m_sTemplName;};

  // informace o objektu
  virtual BOOL	IsEnabledObject(const EnabledObjects&) const;

  // editace objektu
  virtual BOOL	DoEditObject();
  virtual BOOL	CanEditObject() const { return FALSE; };

  int GetID() const {return m_nWoodID;};
  int SetFreeID(); 
  // data
protected:
  int			m_nWoodID;					// ID lesa
  CString		m_sTemplName;				// jm�no �ablony p�i vytvo�en� lesa
};	

TypeIsSimple(CPosWoodObject *)

/////////////////////////////////////////////////////////////////////////////
// CPosSelAreaObject - selection area
class CPosSelAreaObject : public CPosAreaObject
{
  DECLARE_DYNCREATE(CPosSelAreaObject)
  typedef CPosAreaObject base;
public:
  enum SelAreaType
  {
    satSquare = 0,
    satVertex = 1,    
  };

protected:
  SelAreaType _selType;
  void OffsetAreaLog(const CPoint& offset);
  CPosSelAreaObject(){ _selType = satSquare;};

public:

  CPosSelAreaObject(const CPoseidonMap& map) : base(map) { _selType = satSquare;};
  ~CPosSelAreaObject(){};

  // vytvo�en� kopie objektu
  virtual	void	CopyFrom(const CPosObject*); 
  SelAreaType GetSelAreaType() const {return _selType;};
  void SetSelAreaType(SelAreaType );
  void AlignToGrid(const CPoint& sizeLog, const CPoint& offsetLog);
};



// CPosNetObjectBase - z�kladn� objekt s�t�
class /*AFX_EXT_CLASS*/ CPosNetObjectBase : public CPosObject
{
	DECLARE_DYNAMIC(CPosNetObjectBase)
	typedef CPosObject base;
protected:
	CPosNetObjectBase();
public:
	CPosNetObjectBase(const CPoseidonMap& map);

	// typ objektu
	enum 
	{ 
		netObjTypeNor = 0, 
		netObjTypeKey = 1, 
	};

	virtual int GetNetObjectType() const = 0;

	// vytvo�en� kopie objektu
	virtual	void CopyFrom(const CPosObject*);
	// update registrovan�ch objekt� podle definovan�ch
	virtual	void UpdateNetsObjectsFromDefObjects(const CMgrNets*) = 0;

	// serializace
	virtual void DoSerialize(CArchive& ar, DWORD nVer);
	virtual void DoUpdateFromTemplate(const CPoseidonMap* map) = 0;

	// informace o poloze objektu - nen� ur�ena
	virtual CPositionGroup* GetLogObjectPosition() 
	{
		return static_cast<CPositionGroup*>(m_LogPosition.GetRef());
	}

	virtual const CPositionGroup* GetLogObjectPosition() const 
	{
		return static_cast<CPositionGroup*>(m_LogPosition.GetRef());
	} 

	// informace o objektu
	// virtual void	GetObjectHull(CRect& rHull) const;
	virtual BOOL IsEnabledObject(const EnabledObjects&) const;
	virtual BOOL IsSameObject(const CPosObject& obj) const;

	// pr�ce s objekty Poseidon (Update a Vytvo�en�)
	virtual BOOL DoUpdatePoseidonNetObjects(CObArray* ExistObjs, const CPoseidonMap*) = 0;
	virtual BOOL DoCreatePoseidonNetObjects(CObArray* CreateObjs, const CPoseidonMap*) = 0;

	// pr�ce se sou�adnicemi objektu
	void DoUpdateNetPartsBaseRealPos(REALNETPOS);
	virtual void DoUpdateNetPartsBaseRealPos() = 0;
	virtual void DoUpdateNetPartsLogPosition() = 0;
	//virtual void	RecalcDevObjectPositionFromLP(CDC*);
	virtual void ResetDevObjectPosition() = 0;
	//virtual bool IsIntersection(const CPosObject& second) const = 0;

	// pr�ce s trackerem
	virtual void CalcObjectTrackerPosition(CPositionTracker& Pos) = 0;

	// editace objektu
	virtual BOOL DoEditObject() 
	{ 
		return TRUE; 
	}

	virtual BOOL CanEditObject() const 
	{ 
		return TRUE; 
	}

	// vykreslov�n� objektu
	virtual void OnDrawObject(CDC* pDC, const CDrawPar* pParams) const = 0;

	// walk on road
	virtual AutoArray<NetWalkPoint> GetWalkPoints() const = 0;
	virtual bool AdvanceWalkPoint(NetWalkPoint&, float dist) const = 0;

	virtual REALNETPOS GetNetRealPos() const 
	{
		return m_nBaseRealPos;
	}

	virtual void SetNetRealPos(const REALNETPOS& pos) 
	{
		m_nBaseRealPos = pos;
	}

	int GetID() const 
	{
		return m_nNetID;
	}

	int SetFreeID();

	// data
protected:
	int m_nNetID;			// ID s�t�

public:
	REALNETPOS m_nBaseRealPos;		// z�kladn� re�ln� poloha
 
	// temporary - neserializov�no
	//CRect			m_rGlobalHull;		// celkov� obal v�ech objekt� 
	//CRect			m_rGlobalHullDP;	// celkov� obal v�ech objekt� v DP
};

TypeIsSimple(CPosNetObjectBase *)

// CPosNetObjectNor - norm�ln� �sek s�t�
class /*AFX_EXT_CLASS*/ CPosNetObjectNor : public CPosNetObjectBase
{
DECLARE_DYNCREATE(CPosNetObjectNor)
protected:
  typedef CPosNetObjectBase base;
  CPosNetObjectNor();
public:
  CPosNetObjectNor(const CPoseidonMap& map);
  ~CPosNetObjectNor();

					// typ objektu
	virtual int		GetNetObjectType() const { return CPosNetObjectBase::netObjTypeNor; };

					// vytvo�en� kopie objektu
	virtual	void	CopyFrom(const CPosObject*);
	virtual	void	Destroy();
					// vytvo�en� �seku podle s�t�
			BOOL	CreateFromNetTemplate(const CPosNetTemplate*);
					// update registrovan�ch objekt� podle definovan�ch
	virtual void	UpdateNetsObjectsFromDefObjects(const CMgrNets*);

					// serializace
	virtual	void	DoSerialize(CArchive& ar, DWORD nVer);
	virtual	void	DoUpdateFromTemplate(const CPoseidonMap *map);

					// informace o objektu
	virtual void	GetObjectName(CString& sText) const;				
	//		void	DoUpdateHull();
	//virtual	BOOL IsPointAtArea(const POINT&) const;
  //virtual bool IsIntersection(const CPosObject& second) const;

					// pr�ce s objekty Poseidon (Update a Vytvo�en�)
	virtual BOOL	DoUpdatePoseidonNetObjects(CObArray* ExistObjs,const CPoseidonMap*);
	virtual BOOL	DoCreatePoseidonNetObjects(CObArray* CreateObjs,const CPoseidonMap*);

					// pr�ce se sou�adnicemi objektu
	virtual void	DoUpdateNetPartsBaseRealPos();
	virtual void	DoUpdateNetPartsLogPosition();
	virtual void	RecalcDevObjectPositionFromLP(CDC*);
	virtual void	ResetDevObjectPosition();

					// informace o poloze objektu - nen� ur�ena
	
					// pr�ce s trackerem
	virtual void	CalcObjectTrackerPosition(CPositionTracker& Pos);

					// vykreslov�n� objektu
	virtual	void	OnDrawObject(CDC* pDC,const CDrawPar* pParams) const;

  // walk on road
  virtual AutoArray<NetWalkPoint> GetWalkPoints() const {return AutoArray<NetWalkPoint>();};
  virtual bool AdvanceWalkPoint(NetWalkPoint& , float dist) const {return false;};  

// data
public:
	CNetPartsList	m_PartList;

// TEMPORARY
	// poloha objektu ve view
	//CPositionGroup	m_LogPosition;		// logick� sou�adnice objektu
};

// CPosNetObjectKey - kl��ov� bod s�t�
class /*AFX_EXT_CLASS*/ CPosNetObjectKey : public CPosNetObjectBase
{
  DECLARE_DYNCREATE(CPosNetObjectKey)
  typedef CPosNetObjectBase base;
protected:
  CPosNetObjectKey();
public:
  CPosNetObjectKey(const CPoseidonMap& map);
  
  ~CPosNetObjectKey();

  // typ objektu
  virtual int		GetNetObjectType() const { return CPosNetObjectBase::netObjTypeKey; };

  // vytvo�en� kopie objektu
  virtual	void	CopyFrom(const CPosObject*);
  virtual void	Destroy();
  // update registrovan�ch objekt� podle definovan�ch
  virtual void	UpdateNetsObjectsFromDefObjects(const CMgrNets*);

  // serializace
  virtual	void	DoSerialize(CArchive& ar, DWORD nVer);
  virtual	void	DoUpdateFromTemplate(const CPoseidonMap *map);

  // informace o objektu
  virtual void	GetObjectName(CString& sText) const;				
  //void	DoUpdateHull();
  //virtual	BOOL	IsPointAtArea(const POINT&) const;
 // virtual bool IsIntersection(const CPosObject& second) const;

  // vytvo�en� kl��ov�ho objektu
  BOOL	CreateFromNetKeyPart(CNetPartKEY*,CMgrNets* pMgr);

  // pr�ce s objekty Poseidon (Update a Vytvo�en�)
  virtual BOOL	DoUpdatePoseidonNetObjects(CObArray* ExistObjs,const CPoseidonMap*);
  virtual BOOL	DoCreatePoseidonNetObjects(CObArray* CreateObjs,const CPoseidonMap*);

  // pr�ce se sou�adnicemi objektu
  virtual void	DoUpdateNetPartsBaseRealPos();
  virtual void	DoUpdateNetPartsLogPosition();
  virtual void	RecalcDevObjectPositionFromLP(CDC*);
  virtual void	ResetDevObjectPosition();

  // pr�ce s trackerem
  virtual void	CalcObjectTrackerPosition(CPositionTracker& Pos);

  // vykreslov�n� objektu
  virtual	void	OnDrawObject(CDC* pDC,const CDrawPar* pParams) const;

  // informace o poloze objektu - nen� ur�ena
  //virtual CPositionBase* GetLogObjectPosition();
  //virtual const CPositionBase* GetLogObjectPosition() const {return &m_LogPosition;};

  // pro Section vrac� odpov�daj�c� polohu (m_nBaseRealPos seznamu)
  REALNETPOS	GetRealPosForSection(int nSection,const CPoseidonMap*) const;

  bool PrepareForPaste(const CPosEdBaseDoc& doc);  

  // walk on road
  virtual AutoArray<NetWalkPoint> GetWalkPoints() const;
  virtual bool AdvanceWalkPoint(NetWalkPoint& , float dist) const;

  // data
public:
  CNetPartKEY*	m_pBasePartKEY;
  CObArray			m_PartLists;	// �seky

  // TEMPORARY
  // poloha objektu ve view
  //CPositionGroup	m_LogPosition;		// logick� sou�adnice objektu
};


// CPosNameAreaObject - pojmenovan� oblast
class /*AFX_EXT_CLASS*/ CPosNameAreaObject : public CPosAreaObject
{
  typedef CPosAreaObject base;
DECLARE_DYNCREATE(CPosNameAreaObject)
protected:
  CPosNameAreaObject();
public:
  CPosNameAreaObject(const CPoseidonMap& map);
					

					// vytvo�en� kopie objektu
	virtual	void	CopyFrom(const CPosObject*);
					// serializace
	virtual	void	DoSerialize(CArchive& ar, DWORD nVer);

					// informace o objektu
	virtual BOOL	IsEnabledObject(const EnabledObjects&) const;

					// editace objektu
	virtual BOOL	DoEditObject();
	virtual BOOL	CanEditObject() const { return TRUE; };

  int GetID() const {return m_nAreaID;};
  int SetFreeID();

// data
public:
	BOOL			m_bAreaVisible;				// povoleno zobrazn�
protected:
	int				m_nAreaID;					// ID oblasti

  friend BOOL DoEditNameArea(CPosNameAreaObject*,BOOL,BOOL*);
};	

TypeIsSimple(CPosNameAreaObject *)

// CPosKeyPointObject - kl��ov� bod
class /*AFX_EXT_CLASS*/ CPosKeyPointObject : public CPosObject
{
  DECLARE_DYNCREATE(CPosKeyPointObject)
  typedef CPosObject base;
protected:
  CPosKeyPointObject();
public:  
  CPosKeyPointObject(const CPoseidonMap& map);

  ~CPosKeyPointObject();

  // vytvo�en� kopie objektu
  virtual	void	CopyFrom(const CPosObject*);
  // serializace
  virtual	void	DoSerialize(CArchive& ar, DWORD nVer);
  

  // informace o objektu
  virtual void	GetObjectName(CString& sText) const;				

  // vykreslov�n� objektu
protected:
  virtual void	GetDrawAreaColors(COLORREF& cEntire,COLORREF& cFrame,int& nStyle,const CDrawPar*) const;
public:
  void	OnDrawObject(CDC* pDC,const CDrawPar* pParams) const;

  // informace o objektu
  virtual BOOL	IsEnabledObject(const EnabledObjects&) const;
  virtual BOOL IsSameObject(const CPosObject & obj) const;

  // editace objektu
  virtual BOOL	DoEditObject();
  virtual BOOL	CanEditObject() const { return TRUE; };

  // v�po�et logick�ch sou�adnic objektu
  void	CalcObjectLogPosition();
  void	CalcObjectPosPosition();

  // �prava polohy objekt�
  void	CalcOffsetLogPosition(int xAmount,int yAmount);
  void	CalcRotateLogPosition(float grad,CPointVal origin);

  int GetID() const {return m_nKeyPtID;};
  int SetFreeID();

  // data
protected:
   int				m_nKeyPtID;			// ID kl��ov�ho bodu

public:
  CString			m_sName;		// jm�no 
  COLORREF		m_cEntire;		// barvy pro vykreslen�
  COLORREF		m_cFrame;
  int				m_nStyle;		// styl vykreslen�
  BOOL			m_bVisible;		// viditeln� na map�

  REALPOS		m_ptRealPos;	// poloha objektu (re�ln�)
  float			m_nRealWidth;	// polom�r A
  float			m_nRealHeight;	// polom�r B

  CString   m_Text;       //key point text
  CString   m_Type;       //key point type
  CString   m_otherProp;  //other properties rendered directly to the output

  enum {
    drStyleFull = 0,
    drStyleHori = 1,  drStyleVert = 2,  drStyleCros = 3,
    drStyleLfRg = 4,  drStyleRgLf = 5,  drStyleDgCr = 6, 
    drStyleDef	= 7 };



    //CPositionEllipse	m_LogPosition;		// plocha v log. sou�adnic�ch
    //CPositionEllipse	m_DevPosition;		// dev. sou�adnice - temporary !!!
};	

TypeIsSimple(CPosKeyPointObject *)

// CPosGroupObject - skupina vybran�ch obejkt�
class /*AFX_EXT_CLASS*/ CPosGroupObject : public CPosObject
{
  DECLARE_DYNCREATE(CPosGroupObject)
  typedef CPosObject base;

protected:
  CPosGroupObject();

public:
  CPosGroupObject(const CPoseidonMap& map);
  CPosGroupObject(const CPosGroupObject& src);
  ~CPosGroupObject();

  // vytvo�en� kopie objektu
  virtual	void	CopyFrom(const CPosObject*);
  // serializace
  virtual	void	DoSerialize(CArchive& ar, DWORD nVer);

  // informace o poloze objektu
  virtual CPositionGroup* GetLogObjectPosition() {return static_cast<CPositionGroup*>(m_LogPosition.GetRef());};
  virtual const CPositionGroup* GetLogObjectPosition() const {return static_cast<CPositionGroup*>(m_LogPosition.GetRef());}; 
 
  // informace o objektu
  virtual void	GetObjectName(CString& sText) const;				
 

  virtual BOOL IsSameObject(const CPosObject & obj) const {return FALSE;};

  virtual BOOL	IsEnabledObject(const EnabledObjects&) const;

  // vykreslov�n� objektu
  void	OnDrawObject(CDC* pDC,const CDrawPar* pParams) const;

  // pr�ce s vybran�mi objekty
  void	DestroyGroup();
  int		GetGroupSize() const;

  BOOL	IsObjectAtGroup(const CObject*,int* pIndx = NULL) const;

  void	AddObjectGroup(CObject*, bool exclusive = true);
  void	DelObjectGroup(CObject*);

  void	UpdateLogPosition();

  CObject * operator[](int i) const {return m_SelObjects[i];}; 

  const CPosGroupObject& operator=(const CPosGroupObject& src);

  // data
public:
  CObArray		m_SelObjects;		// vybran� objekty
};

/////////////////////////////////////////////////////////////////////////////
// Background object

// CPosAreaObject - editovan� plocha na map�
class /*AFX_EXT_CLASS*/ CPosRectObject : public CPosObject
{
  DECLARE_DYNCREATE(CPosRectObject)
  typedef CPosObject base;
protected:
  CPosRectObject() {};
  CPosRectObject(const CPoseidonMap& map);
public:    
  ~CPosRectObject();

  // vytvo�en� kopie objektu
  virtual	void	CopyFrom(const CPosObject*);
  // serializace
  virtual	void	DoSerialize(CArchive& ar, DWORD nVer);

  // informace o poloze objektu
  virtual CPositionBase* GetLogObjectPosition() {return &m_LogPosition;};
  virtual const CPositionBase* GetLogObjectPosition() const {return &m_LogPosition;};
  virtual CPositionBase* GetDevObjectPosition() {return &m_DevPosition;};
  // informace o objektu
  virtual void	GetObjectName(CString& sText) const { sText = m_sAreaName;};				
  virtual void	GetObjectHull(CRect& rHull) const {rHull = m_LogPosition.m_Position;};
  virtual	BOOL	IsPointAtArea(const POINT& pt) const {return m_LogPosition.IsPointAtArea(pt);};
  virtual BOOL  IsRectAtArea(const CRect& rect, BOOL bFull) const {return m_LogPosition.IsRectAtArea(rect, bFull);};
  virtual BOOL  IsRectAtAreaDP(const CRect& rect, BOOL bFull) const {return m_DevPosition.IsRectAtArea(rect, bFull);};

  virtual BOOL IsSameObject(const CPosObject & obj) const ;

  // vykreslov�n� objektu
  //virtual void	GetDrawAreaColors(COLORREF& cEntire,COLORREF& cFrame,int& nStyle,const CDrawPar*) const;
  virtual void	OnDrawObject(CDC* pDC,const CPositionRect& Pos, const CRect& Clip, const CDrawPar* pParams){};

  // pr�ce s plochou
  //BOOL	IsValidArea() const;
  //BOOL	CanSetRect(const RECT&) const;
  //void	(const RECT&);
  //void	AddRect(const RECT&);
  //void	ResetArea();

  // data
public:
  CString			m_sAreaName;	

  CPositionRect	m_LogPosition;		// plocha v log. sou�adnic�ch
  CPositionRect	m_DevPosition;		// dev. sou�adnice - temporary !!!
};	

class CPosBackgroundImageObject : public CPosRectObject
{
  DECLARE_DYNCREATE(CPosBackgroundImageObject)
protected:
  CPosBackgroundImageObject();
public:
  typedef CPosRectObject base;

  CPosBackgroundImageObject(const CPoseidonMap & map);  
  ~CPosBackgroundImageObject();

  virtual	void	CopyFrom(const CPosObject*);
  // serializace
  virtual	void	DoSerialize(CArchive& ar, DWORD nVer);

  void OnDrawObject(CDC* pDC,const CPositionRect& Pos,  const CRect& Clip, const CDrawPar* pParams);
  void LoadBitmap(const CDocument& cDoc);
  void ReleaseBitmap();

  void CalcObjectLogPosition();

  virtual BOOL	IsEnabledObject(const EnabledObjects& enabled) const { return m_Visible && enabled.m_bBGImage; };

public:
  CString m_sFileName;  
  CBitmap m_cBitmap;

  FltRect m_RealRect;  
  int m_Transparency;
  BOOL m_Visible;
};

TypeIsSimple(CPosBackgroundImageObject *);

/// information common for all layers
struct CCommonLayer
{
  float ambient[4];
  float diffuse[4];
  float emissive[4];
  float forcedDiffuse[4];
  /// texture
  Matrix4 txTexgen;
  /// normal map
  Matrix4 noTexgen;
  /// detail map
  Matrix4 dtTexgen;
  
  void Load(CPosEdBaseDoc *doc, ParamEntryPar entry, QFileTime &newest);
};

/// information about a single masked layer
struct CMaskedLayer
{
  RString name;
  /// texture
  RString tx;
  /// normal map
  RString no;
  /// detail map
  RString dt;
  
  void Load(CPosEdBaseDoc *doc, ParamEntryPar entry, QFileTime &newest);
};

TypeIsMovable(CMaskedLayer)

/// information about a bimpas usage
struct BimpasUsed
{
  /// material file name
  RString name;
  /// major pass id
  int major;
  /// how many times is this bimpas used
  int useCount;
  
  BimpasUsed()
  {
    useCount = 0;
  }

  bool operator == (const BimpasUsed &b) const {return _strcmpi(name,b.name)==0 && major==b.major;}
  
};

TypeIsMovable(BimpasUsed)


/*
template <>
struct FindArrayKeyTraits<BimpasUsed>
{
  typedef RString KeyType;
  /// check if two keys are equal
  static bool IsEqual(KeyType a, KeyType b) {return !strcmpi(a,b) && a.major==b.major;}
  /// get a key from an item
  static KeyType GetKey(const BimpasUsed &a) {return a.name;}
};
*/

////////////////////////////////////////////////////////////////////////////
// Texture Layer
class CPoseidonMap;

class CTextureLayer : public CObject, public RefCount, private NoCopy
{
	DECLARE_DYNAMIC(CTextureLayer);
public:
	CString m_Name;

	int m_nTexWidth;    // rozmer krajiny v texturach. podil m_nWidth/m_nTexWidth musi byt mocnina 2
	int m_nTexHeight;   //                             podil m_nHeight/m_nTexHeight musi byt mocnina 2

	int m_nTexSizeInSqLog; //m_nWidth/m_nTexWidth vyjadrena jako mocnina 2
	int m_nTexSizeInSq; //m_nWidth/m_nTexWidth

	PTRTEXTURETMPLT*	m_pBaseTextures;// textury (prim. a variantn�)
	PTRTEXTURETMPLT*	m_pLandTextures;// textury (sekund�rn�)

	BitArray m_locked;

	bool m_bActive; 

	//CPosSelAreaObject m_SelArea;
	CPoseidonMap& m_map;
	//@{
	///@name layer + satellite representation
	/// when true, layers + satellite are valid
	bool m_layered;
	/// common for all layers
	CCommonLayer m_commonLayer;
	/// all layers
	AutoArray<CMaskedLayer> m_layers;
	/// time stamp of the source file for layers
	QFileTime m_layersTimeStamp;
  
	/// pattern for satellite texture name
	RString m_satellitePattern;
	/// pattern for layer mask texture name
	RString m_layerMaskPattern;

	/// bimpas names as a result of sat. map import
	Array2D<int> m_explicitBimpas;
	/// register referenced by m_explicitBimpas
	FindArrayKey<BimpasUsed> m_explicitBimpasRegister;
  
	//@}
public:

	CTextureLayer(CPoseidonMap& map);
	//CTextureLayer(const CTextureLayer& src);
	virtual ~CTextureLayer();

	const CString& Name() const 
	{
		return m_Name;
	}

	void SetName(const CString& name) 
	{
		m_Name = name;
	}

	void Activate(); 
	void Deactivate();

	BOOL OnNewLayer(int nWidth, int nHeight, int nTexSizeInSqLog, const CString& name);
	void OnInitLayer();
	void DestroyLayer();
	void ResizeLayer(int newWidth, int newHeight);

	virtual void DoSerialize(CArchive& ar, int nVer);
  
	BOOL IsAtLayerWorld(const UNITPOS&) const;
	BOOL IsTextureUnitCenterAtArea(UNITPOS Pos, const CPositionArea*) const;
	void GetTextureUnitPosHull(UNITPOS& LftTop, UNITPOS& RghBtm, const CRect& rArea) const;

	float GetTextureRealSize() const;

	int GetTextureLogSize() const 
	{
		return VIEW_LOG_UNIT_SIZE * m_nTexSizeInSq;
	}

	REALPOS FromTextureUnitToRealPos(const UNITPOS& PosFrom) const;
	POINT FromTextureUnitToViewPos(const UNITPOS& PosFrom) const;
	UNITPOS FromRealToTextureUnitPos(const REALPOS& PosFrom) const;
	UNITPOS FromTextureUnitToUnitPos(const UNITPOS& PosFrom) const;
	UNITPOS FromUnitToTextureUnitPos(const UNITPOS& PosFrom) const;

	BOOL ChangeTextureLog(int nTexLog);

	PTRTEXTURETMPLT GetPrimTextureAt(const UNITPOS&)const;
	/** Functions returns primary texture in node, which should be used in secondary textures creation*/ 
	PTRTEXTURETMPLT GetPrimTextureAtEx(const UNITPOS&)const;
	PTRTEXTURETMPLT GetBaseTextureAt(const UNITPOS&)const;
	PTRTEXTURETMPLT GetBaseTextureAtTerrainGrid(const UNITPOS&)const;

	bool Locked(const UNITPOS&) const;
	void Lock(const UNITPOS&, bool lock = true);

	void SetBaseTextureAt(const UNITPOS&, PTRTEXTURETMPLT);
	PTRTEXTURETMPLT GetLandTextureAt(const UNITPOS&)const;
	PTRTEXTURETMPLT GetLandTextureAtTerrainGrid(const UNITPOS&)const;
	void SetLandTextureAt(const UNITPOS&, PTRTEXTURETMPLT, BOOL bFromBuldozer);
	void DoUpdateSecndTexturesAt(const UNITPOS& pos, BOOL bRegenerate = FALSE);

	/// use explicit bimpas representation
	void SwitchToExplicitBimaps();

	/// clear explicit bimpas representation - use 0 everywhere
	void ClearExplicitBimaps();
  
	/// check if explicit is used
	bool CheckExplicitBimpas() const 
	{
		return m_explicitBimpas.GetXRange() > 0;
	}

	/// set explicit bimpas name at given coordinates
	void SetExplicitBimpasAt(const UNITPOS& pos, RString bimpasName, int major, bool fromBuldozer);
	/// update bimpas to represent satellite + layer mask
	void DoUpdateSatAndMaskAt(const UNITPOS& pos, int nLayers, const int* layers, int major, int segX, 
							  int segZ, float satOffU, float satOffV, float satTexU, float satTexV, 
							  float maskOffU, float maskOffV, float maskTexU, float maskTexV, 
							  bool saveAsBinary, StringList& detailTexturesList);
  
	/// maitain explicit bimpas register - increase use count
	void RegisterBimpas(int index);
	/// maitain explicit bimpas register - decrease use count
	void UnregisterBimpas(int index);
};

////////////////////////////////////////////////////////////////////////////
// Water layer it is quite unclear what it will really be, but now will be represented by heigh map

class WaterLayer : public CObject/*, public RefCount*/
{
  DECLARE_DYNAMIC(WaterLayer) 

protected:
  int					_sizeX;		// rozm�r krajiny (v bodech) - X
  int					_sizeZ;	//						               - Z
  float       _realSizeStep;
  float       _invRealSizeStep;

  LANDHEIGHT *			_heightField;	// v��ka krajiny  

  UNITPOS FromRealToUnitPos(REALPOS pos) const;
  bool IsAtWorld(UNITPOS pos) const { return (pos.x > 0 && pos.x < _sizeX && pos.z > 0 && pos.z < _sizeZ);};

public:

  WaterLayer() : _sizeX(0), _sizeZ(0), _realSizeStep(0),_invRealSizeStep(0), _heightField(NULL) {};
  virtual ~WaterLayer();

  bool Create(int sizeX, int sizeZ, float realSizeStep, float startHeight = 0);
  void Destroy();
  bool IsCreated() const {return _heightField != NULL;};

  void DoSerialize(CArchive& ar,int nVer);

  void SetHeight(UNITPOS pos, float height); 
  float GetHeight(UNITPOS pos) const; 
  float GetHeight(REALPOS pos, float * dX = NULL, float * dZ = NULL) const; 

  int GetSizeX() const {return _sizeX;};
  int GetSizeZ() const {return _sizeX;};
  float GetStep() const {return  _realSizeStep;};
};


/////////////////////////////////////////////////////////////////////////////
//	z�kladn� objekt pro mapu poseidon	(PoseidonMap.cpp)

#include "../PoseidonEditor/RoadEditorDoc.h"


class CPoseidonMap : public CObject
{
  DECLARE_DYNAMIC(CPoseidonMap)
public:
  CPoseidonMap();
  virtual			~CPoseidonMap();

  // likvidace struktur mapy
  void	DestroyMap();

  // vytv��en� a serializace mapy
  BOOL	OnNewMap(int nWidth, int nHeight, int nTexSizeInSqLog = 0);
  BOOL  NewTerrain(int nWidth, int nHeight);
  void	OnInitMap();
  virtual	void	DoSerialize(CArchive& ar,int nVer);

  // Prepinani rozmeru textur
  BOOL	ChangeTextureLog(int nTexLog);

  // pr�ce se sou�adnicemi
  BOOL	IsAtWorld(const UNITPOS&) const;  
  BOOL	IsAtWorld(const REALPOS&) const;

  UNITPOS	FromRealToUnitPos(const REALPOS&) const;
  REALPOS	FromUnitToRealPos(const UNITPOS&) const;

  UNITPOS	FromRealToTextureUnitPos(const REALPOS&) const;
  REALPOS	FromTextureUnitToRealPos(const UNITPOS&) const;

  POINT	FromRealToViewPos(const REALPOS&) const;
  CRect	FromRealToViewRect(const FltRect&) const;
  REALPOS	FromViewToRealPos(const POINT&) const;
  FltRect FromViewToRealRect(const CRect&) const;

  POINT FromUnitToViewPos(const UNITPOS&) const;
  POINT FromTextureUnitToViewPos(const UNITPOS&) const;

  UNITPOS FromTextureUnitToUnitPos(const UNITPOS& PosFrom) const;
  UNITPOS FromUnitToTextureUnitPos(const UNITPOS& PosFrom) const;


  BOOL IsUnitCenterAtArea(UNITPOS nUnit,const CPositionArea* pArea) const;
  BOOL IsUnitCenterAtArea(int X,int Y,const CPositionArea* pArea) const;
  BOOL IsTextureUnitCenterAtArea(UNITPOS Pos,const CPositionArea*) const;


  void	GetUnitPosHull(UNITPOS& LftTop,UNITPOS& RghBtm,const CRect& rArea) const;
  void	GetTextureUnitPosHull(UNITPOS& LftTop,UNITPOS& RghBtm,const CRect& rArea) const;

  // pr�ce s v��kou krajiny
  LANDHEIGHT GetLandscapeHeight(const REALPOS&,float *rdX=NULL, float *rdY=NULL)const;			
  LANDHEIGHT GetLandscapeHeight(const UNITPOS&)const;
  LANDHEIGHT GetLandscapeHeight(int X,int Z)const;

  LANDHEIGHT GetBalancedHeight(const UNITPOS&,float* rdGradMax)const; // pr�m�rn� v��ka pro texturaci


  void    SetLandscapeHeight(const UNITPOS&,LANDHEIGHT,bool bFromBuldozer,bool recalcObjectHeight = true);
  void    SetLandscapeHeight(int X,int Z,LANDHEIGHT,bool bFromBuldozer,bool recalcObjectHeight = true );
  void    SetGlobalHeight(LANDHEIGHT);
  void	UpdateMaxHeight();

  // Recalculates object height according to terrain, rect is in log-coordinates
  void UpdateObjectsHeight(const CRect& rect, bool keepRelHeight = true, bool repairMatrix=true);
  void UpdateObjectsHeight(bool keepRelHeight = true, bool repairMatrix=true);

  // eroze + pomocn� funkce
  void	DoLandEroze(WashParams& Params,CPositionArea& Area);
  void	DoBlur(CPositionArea& Area);

  // pr�ce s objekty 

  int		GetPoseidonObjectCount() const { return m_InstanceObjs.GetSize(); };
  CPosEdObject*	GetPoseidonObject(int nObjID) const;
  int		GetFreeObjectID() const;
  void	AddNewObject(CPosEdObject*,BOOL bFromBuldozer);
  void	DeleteObject(int nObjID,BOOL bFromBuldozer);
  void	MoveObjectToPos(CPosEdObject*,const REALPOS&,BOOL bFromBuldozer);

  // pr�ce s lesy
  int		GetPoseidonWoodsCount() const { return m_WoodsInMap.GetSize(); };
  CPosWoodObject*	GetPoseidonWood(int nWoodID);
  int		GetFreeWoodID() const;
  void	AddNewWood(CPosWoodObject*);
  void	DeleteWood(int nWoodID);

  // pr�ce se s�t�mi
  int		GetPoseidonNetsCount() const { return m_NetsInMap.GetSize(); };
  CPosNetObjectBase*	GetPoseidonNet(int nNetID);
  int		GetFreeNetID() const;
  void	AddNewNet(CPosNetObjectBase*);
  void	DeleteNet(int nNetID);
  // update registrovan�ch objekt� podle definovan�ch
  void	UpdateNetsObjectsFromDefObjects(const CMgrNets*);

  // pr�ce s pojmenovan�mi plochami
  int		GetPoseidonNameAreaCount() const { return m_AreasInMap.GetSize(); };
  CPosNameAreaObject*	GetPoseidonNameArea(int nAreaID);
  int		GetFreeNameAreaID() const;
  void	AddNewNameArea(CPosNameAreaObject*);
  void	DeleteArea(int nAreaID);

  // pr�ce s kl��ov�mi body
  int		GetPoseidonKeyPtCount() const { return m_KeyPtInMap.GetSize(); };
  CPosKeyPointObject*	GetPoseidonKeyPt(int nKeyPtID);
  int		GetFreeKeyPtID() const;
  void	AddNewKeyPt(CPosKeyPointObject*);
  void	DeleteKeyPt(int nKeyPtID);

  //int		GetFreeRegTextureID(BOOL bIsSea) const;
  PTRTEXTURETMPLT	GetStdSeaTexture() const {return NULL;}
  //PTRTEXTURETMPLT	GetRegisteredTexture(int nID) const;
  //PTRTEXTURETMPLT	GetRegisteredTexture(LPCTSTR pFile) const;

  PTRTEXTURETMPLT	GetPrimTextureAt(const UNITPOS&)const;
  PTRTEXTURETMPLT	GetBaseTextureAt(const UNITPOS&)const;
  PTRTEXTURETMPLT	GetBaseTextureAtTerrainGrid(const UNITPOS&)const;
  void    SetBaseTextureAt(const UNITPOS&,PTRTEXTURETMPLT);
  PTRTEXTURETMPLT	GetLandTextureAt(const UNITPOS&)const;
  PTRTEXTURETMPLT	GetLandTextureAtTerrainGrid(const UNITPOS&)const;
  void    SetLandTextureAt(const UNITPOS&,PTRTEXTURETMPLT,BOOL bFromBuldozer);

  bool LockedTexture(const UNITPOS&) const;
  void LockTexture(const UNITPOS&, bool lock = true);
  bool LockedVertex(const UNITPOS&) const;
  void LockVertex(const UNITPOS&, bool lock = true);

  // generov�n� a registrace slo�en�ch textur 
  PTRTEXTURETMPLT	CreateSecundTextureFrom(PTRTEXTURETMPLT,PTRTEXTURETMPLT,PTRTEXTURETMPLT,PTRTEXTURETMPLT,BOOL bRegener = FALSE);

  static void CreateSecundTextureName(CString&,PTRTEXTURETMPLT,PTRTEXTURETMPLT,PTRTEXTURETMPLT,PTRTEXTURETMPLT);
  static void CreateTransitionSquareSurfaceBimPasFile(const CString&,PTRTEXTURETMPLT,PTRTEXTURETMPLT,PTRTEXTURETMPLT,PTRTEXTURETMPLT);
  static void CreateTransitionSquareSurfaceBimPasFileName(CString&,PTRTEXTURETMPLT,PTRTEXTURETMPLT,PTRTEXTURETMPLT,PTRTEXTURETMPLT);

  static void CreateSquareSurfaceBimPasFile(const CString&,PTRTEXTURETMPLT);  
  

  // jsou dv� textury stejn� ?
  static BOOL	IsSameTextures(PTRTEXTURETMPLT,PTRTEXTURETMPLT);

  // p�eps�n� mapy do Buldozeru
  void	TransferMapToBuldozer() const;

  // vyhled�n� objekt� na map�
  CPosObject*   GetObjectAt(CPoint ptAt,int nDif,const EnabledObjects&);  

  FMntVector2 GetRealSize() const;
  UNITPOS GetSize() const;

  // change of realSizeUnit
  void OnRealSizeUnitChange(float frac, bool recalcLandHeight);

  // BackgroundImages
  CPosBackgroundImageObject * GetBgImage(int i) const {return m_BGImagesInMap[i];};
  CPosBackgroundImageObject * GetBgImage(const CString &name) const;
  CPosBackgroundImageObject * GetBgImageAt(CPoint ptAt,int nDif,const CViewStyle*) const;

  int GetBgImagesCount() const {return m_BGImagesInMap.Size();};
  void AddBgImage(CPosBackgroundImageObject * bgImage) {m_BGImagesInMap.Add(bgImage);};
  void DeleteBgImage(const CString &name);  

  // Texture size
  float GetTextureRealSize() const {return m_ActualTextureLayer->GetTextureRealSize();};
  int GetTextureLogSize() const {return m_ActualTextureLayer->GetTextureLogSize();};

  // Texture layers
  int GetTextureLayersCount() const {return m_TextureLayers.Size();};
  CTextureLayer * GetTextureLayer(int i) const {return m_TextureLayers[i];};
  CTextureLayer * GetTextureLayer(const CString &name) const;

  void AddTextureLayer(Ref<CTextureLayer> layer) {m_TextureLayers.Add(layer);};  
  void DeleteTextureLayer(const CString &name);

  void SwitchActiveTextureLayer(Ref<CTextureLayer> layer);
  Ref<CTextureLayer> GetActiveTextureLayer() const {return m_ActualTextureLayer;};

  /// returns array with UNITPOS of the land vertexes, which are involved on land under pos
  void GetVertexesInvolved(AutoArray<UNITPOS>& vert, const CPositionBase& pos) const; 

  // Water Layer functions. It is unclear what will be water layer so just very simple implementation
  WaterLayer& GetWaterLayer() {return _waterLayer;};
  const WaterLayer& GetWaterLayer()const  {return _waterLayer;};

  CPosEdBaseDoc * GetDocument() const {return m_pDocument;};

  LANDHEIGHT GetMaxHeight() const {return m_nMaxHeight;};
  LANDHEIGHT GetMinHeight() const {return m_nMinHeight;};

  void RecalculateObjectsSize(); // Recalculates all objects sizes

  void CropLand(const CRect &rc);


  // vlastn� data mapy
protected:
  int					m_nWidth;		// rozm�r krajiny (v bodech) - X
  int					m_nHeight;		//						   - Z
  
  BitArray m_landHeightLocked;

  LANDHEIGHT*			m_pLandHeight;	// v��ka krajiny
  LANDHEIGHT			m_nMaxHeight;	// max.v��ka krajiny
  LANDHEIGHT			m_nMinHeight;	// max.v��ka krajiny


  // textury
  RefArray<CTextureLayer> m_TextureLayers;
  Ref<CTextureLayer> m_ActualTextureLayer;
  CPosEdEnvironment*	m_pEnvironment;
  WaterLayer _waterLayer;

  mutable IDStorage m_ObjectIDs;
  mutable IDStorage m_WoodIDs;
  mutable IDStorage m_NetIDs;
  mutable IDStorage m_AreaIDs;
  mutable IDStorage m_KeyPtIDs;


public:
  // lesy, s�t�, pojmenovan� oblasti, kl��ov� body
  CObArray			m_InstanceObjs;	// instance objekt�
  CObArray			m_WoodsInMap;	// objekty les�
  CObArray			m_NetsInMap;	// objekty s�t�
  CObArray			m_AreasInMap;
  CObArray			m_KeyPtInMap;

  AutoArray<CPosBackgroundImageObject *> m_BGImagesInMap;
  CPosEdBaseDoc*		m_pDocument;

  RoadEditorDoc m_roadControl;


};

/////////////////////////////////////////////////////////////////////////////
#endif // !defined(_POSEDMAPS_MAIN_H_)



