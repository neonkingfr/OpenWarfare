#include "stdafx.h"
//#include "resource.h"
#include "PoseidonEdMaps.h"

IMPLEMENT_DYNAMIC(WaterLayer, CObject)

bool WaterLayer::Create(int sizeX, int sizeZ, float realSizeStep, float startHeight)
{
  _sizeX = sizeX;
  _sizeZ = sizeZ;
  _realSizeStep = realSizeStep;
  _invRealSizeStep = 1.0f / _realSizeStep;

  int size = _sizeX * _sizeZ;
  if (_heightField)
    delete [] _heightField;

  _heightField = new float[size];

  if (_heightField == NULL)
    return false;

  for(int i = 0; i < size; i++)  
    _heightField[i] = startHeight;

  return true;   
}

WaterLayer::~WaterLayer()
{
  Destroy();
}

void WaterLayer::Destroy()
{
  if (_heightField)
    delete [] _heightField;

  _heightField = NULL;
}

void WaterLayer::SetHeight(UNITPOS pos, float height)
{
  if (!IsAtWorld(pos))
    return;

  _heightField[pos.x + _sizeX * pos.z] = height;
};

float WaterLayer::GetHeight(UNITPOS pos) const
{
  if (!IsAtWorld(pos))
    return -100;

  return _heightField[pos.x + _sizeX * pos.z];
};

UNITPOS WaterLayer::FromRealToUnitPos(REALPOS pos) const
{
  UNITPOS ret;
  ret.x = toInt(pos.x * _invRealSizeStep);
  ret.z = toInt(pos.z * _invRealSizeStep);

  return ret;
}

// Return the real height at a point
float WaterLayer::GetHeight(REALPOS Pos, float *rdX, float *rdY) const
{
  UNITPOS UnPos = FromRealToUnitPos(Pos);

  if (!IsAtWorld(UnPos))
    return -100;

  float xRel = Pos.x*_invRealSizeStep;
  float zRel = Pos.z*_invRealSizeStep;
  float xIn  = xRel-UnPos.x; // relative 0..1 in square
  float zIn  = zRel-UnPos.z;

  float y00=GetHeight(UnPos); UnPos.x++;
  float y01=GetHeight(UnPos); UnPos.z++;
  float y11=GetHeight(UnPos); UnPos.x--;
  float y10=GetHeight(UnPos);
  // each face is divided to two triangles
  // determine which triangle contains point
  LANDHEIGHT y;
  if( xIn<=1-zIn )
  { // triangle 00,01,10
    if( rdX ) *rdX=(y01-y00)*_invRealSizeStep;
    if( rdY ) *rdY=(y10-y00)*_invRealSizeStep;
    y=y00+(y10-y00)*zIn+(y01-y00)*xIn;
  }
  else
  {
    // triangle 01,10,11
    if( rdX ) *rdX=(y11-y10)*_invRealSizeStep;
    if( rdY ) *rdY=(y11-y01)*_invRealSizeStep;
    y=y11+(y10-y11)*(1-xIn)+(y01-y11)*(1-zIn);
  }
  return y;
};
 

void WaterLayer::DoSerialize(CArchive& ar,int nVer)
{
  if (nVer < 44) // start in version 44
  {
    Destroy();
    return;
  }

  BOOL isCreated = IsCreated();
  MntSerializeBOOL(ar,isCreated);

  if (ar.IsStoring())
  {
    if (!isCreated)
      return;   

    MntSerializeInt(ar,_sizeX);
    MntSerializeInt(ar,_sizeZ);
    MntSerializeFloat(ar,_realSizeStep);
    ar.Write((char*)_heightField,_sizeX*_sizeZ*sizeof(LANDHEIGHT));
  }
  else
  {
    if (!isCreated)
    {
      Destroy();
      return;
    }

    MntSerializeInt(ar,_sizeX);
    MntSerializeInt(ar,_sizeZ);
    MntSerializeFloat(ar,_realSizeStep);

    Create(_sizeX, _sizeZ, _realSizeStep);
    ar.Read((char*)_heightField,_sizeX*_sizeZ*sizeof(LANDHEIGHT));
  }
}