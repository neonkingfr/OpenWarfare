#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CPosRectObject, CPosObject)

CPosRectObject::CPosRectObject(const CPoseidonMap & map) : base(map)
{
}

CPosRectObject::~CPosRectObject()
{
}

void CPosRectObject::CopyFrom(const CPosObject* pSrc)
{
  ASSERT_KINDOF(CPosRectObject,pSrc);
  base::CopyFrom(pSrc);
  // kopie dat	
  m_sAreaName		= ((CPosRectObject*)pSrc)->m_sAreaName;
    
  // souĝadnice
  m_LogPosition.CopyFrom(&(((CPosRectObject*)pSrc)->m_LogPosition));
  m_DevPosition.CopyFrom(&(((CPosRectObject*)pSrc)->m_DevPosition));  
}

void	CPosRectObject::DoSerialize(CArchive& ar, DWORD nVer)
{
  base::DoSerialize(ar,nVer);
  MntSerializeString(ar, m_sAreaName);
  m_LogPosition.DoSerialize(ar, nVer);
  
}

BOOL CPosRectObject::IsSameObject(const CPosObject & obj) const
{
  if (!obj.IsKindOf(RUNTIME_CLASS(CPosRectObject)))
    return FALSE;

  return (((const CPosRectObject&) obj).m_sAreaName == m_sAreaName);
}
///////////////////////////////////////////////////////////////////////////
//

IMPLEMENT_DYNCREATE(CPosBackgroundImageObject, CPosRectObject)

CPosBackgroundImageObject::CPosBackgroundImageObject() 
{  
  m_Transparency = 50;
  m_Visible = TRUE;
}

CPosBackgroundImageObject::CPosBackgroundImageObject(const CPoseidonMap & map) : base(map)
{  
  m_Transparency = 50;
  m_Visible = TRUE;
}

CPosBackgroundImageObject::~CPosBackgroundImageObject()
{
}

void CPosBackgroundImageObject::DoSerialize(CArchive& ar, DWORD nVer)
{
  MntSerializeString(ar,m_sFileName);
  MntSerializeString(ar, m_sAreaName);
  MntSerializeFltRect(ar, m_RealRect);
  MntSerializeInt(ar, m_Transparency);
  if (ar.IsLoading())
  {
    if (nVer >= 33)
      MntSerializeBOOL(ar, m_Visible);
    else
      m_Visible = TRUE;
  }
  else
    MntSerializeBOOL(ar, m_Visible);

  //base::DoSerialize(ar, nVer);
}

void CPosBackgroundImageObject::OnDrawObject(CDC* pDC,const CPositionRect& Pos, const CRect& Clip, const CDrawPar* pParams)
{
  BITMAP  bi;
  HBITMAP hBmp = (HBITMAP) m_cBitmap; 
  if (hBmp == INVALID_HANDLE_VALUE || hBmp == NULL)
    return;

  if ( 0 == m_cBitmap.GetBitmap(&bi))
    return;

  CDC dcCompatible;
  dcCompatible.CreateCompatibleDC(pDC);

  CBitmap * pOldBitmap = dcCompatible.SelectObject(&m_cBitmap);  

  CRect drawRect;
  drawRect.IntersectRect(&(Pos.m_Position), &Clip);
  if (drawRect.Width() == 0 || drawRect.Height() == 0)
    return;

  double widthFrag = bi.bmWidth / (double) Pos.m_Position.Width();
  double heightFrag = bi.bmHeight / (double) Pos.m_Position.Height();

  BLENDFUNCTION blendFunction;
  blendFunction.BlendOp = AC_SRC_OVER;
  blendFunction.BlendFlags = 0;
  blendFunction.AlphaFormat = 0;
  blendFunction.SourceConstantAlpha = 255 - m_Transparency * 255 / 100;

  int nSrcWidth = (int) (drawRect.Width() * widthFrag);
  nSrcWidth = max(nSrcWidth,1);
  int nSrcHeight = (int) (drawRect.Height() * heightFrag);
  nSrcHeight = max(nSrcHeight,1);

  int nSrcX = (int)((drawRect.left - Pos.m_Position.left) * widthFrag);
  int nSrcY = (int)((drawRect.top - Pos.m_Position.top) * heightFrag);

  // When AlphaBlend was called with nSrcWidth or nSrcHeight > 2048 windows crashed in graphic card driver
  // To avoid such crashes, call AlphaBlend several times on smaller src blocks... 
   
  const int maxSrcSize = 1024; 

  if (nSrcWidth <= maxSrcSize && nSrcHeight <= maxSrcSize)
  {  
    pDC->AlphaBlend(drawRect.left,drawRect.top,drawRect.Width(),drawRect.Height(),
      &dcCompatible,nSrcX, nSrcY, nSrcWidth, nSrcHeight, blendFunction);
  }
  else
  {
    int nSrcWidthBak = nSrcWidth;
    int leftBak = drawRect.left; 
    int nSrcXBak = nSrcX;

    do 
    {
      int doSrcHeight = min(maxSrcSize, nSrcHeight);
      int doDestHeight = drawRect.Height() * doSrcHeight / nSrcHeight;

      nSrcWidth = nSrcWidthBak;
      drawRect.left = leftBak; 
      nSrcX = nSrcXBak;
            
      do 
      {  
        int doSrcWidth = min(maxSrcSize, nSrcWidth);       
        int doDestWidth = drawRect.Width() * doSrcWidth / nSrcWidth;
        
        pDC->AlphaBlend(drawRect.left,drawRect.top,doDestWidth ,doDestHeight,
          &dcCompatible,nSrcX, nSrcY, doSrcWidth, doSrcHeight, blendFunction);

        nSrcWidth -= doSrcWidth;       
        drawRect.left += doDestWidth;
        nSrcX += doSrcWidth;
      } while  (nSrcWidth > 0);

      nSrcHeight -= doSrcHeight;
      drawRect.top += doDestHeight;
      nSrcY += doSrcHeight; 

    } while  (nSrcHeight > 0);
  }

  dcCompatible.SelectObject(pOldBitmap);
}

void CPosBackgroundImageObject::ReleaseBitmap()
{
  HBITMAP hBmp = (HBITMAP) m_cBitmap.Detach(); 
  DeleteObject(hBmp);
}

void CPosBackgroundImageObject::LoadBitmap(const CDocument& cDoc)
{
  HBITMAP hBmp = (HBITMAP) m_cBitmap; 
  if (hBmp != INVALID_HANDLE_VALUE && hBmp != NULL)
  {
    return;

    //m_cBitmap.Detach(); 
    //DeleteObject(hBmp);
  }


  if (m_sFileName.GetLength() > 0)
  {
    CString projectPath = cDoc.GetPathName();
    SeparateDirFromFileName(projectPath,projectPath);
    MakeLongDirStr(projectPath,projectPath);

    projectPath += m_sFileName;

    hBmp = (HBITMAP)::LoadImage(NULL,projectPath,
      IMAGE_BITMAP,0,0,
      LR_LOADFROMFILE|LR_CREATEDIBSECTION);
  }

  m_cBitmap.Attach(hBmp);
}

void	CPosBackgroundImageObject::CopyFrom(const CPosObject* pObj)
{
  if (pObj == NULL || !pObj->IsKindOf(RUNTIME_CLASS(CPosBackgroundImageObject)))
    return;

  const CPosBackgroundImageObject * pBGIObj = (const CPosBackgroundImageObject *) pObj;
 
  if (m_sFileName != pBGIObj->m_sFileName)
  {
    ReleaseBitmap();
    m_sFileName = pBGIObj->m_sFileName;
  }
  
  m_RealRect = pBGIObj->m_RealRect;
  m_Transparency = pBGIObj->m_Transparency;
  m_Visible = pBGIObj->m_Visible;

  base::CopyFrom(pBGIObj);
}

void CPosBackgroundImageObject::CalcObjectLogPosition()
{
  REALPOS real;
  real.x = m_RealRect.left;
  real.z = m_RealRect.top;
  CPoint ptlt = _map->FromRealToViewPos(real);

  real.x = m_RealRect.right;
  real.z = m_RealRect.bottom;
  CPoint ptrb = _map->FromRealToViewPos(real);

  m_LogPosition.m_Position.SetRect( ptlt.x, ptrb.y, ptrb.x, ptlt.y);
}


