/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define ATTR_LINE_EXTRA			10

/////////////////////////////////////////////////////////////////////////////
// Tracker objektu

IMPLEMENT_DYNAMIC(CTrackerBase,CFMntScrollerTracker)

CTrackerBase::CTrackerBase(CPosGroupObject* pObject,CPoseidonMap* pMap,CFMntScrollerView* pView):
	CFMntScrollerTracker(pView)
{
  m_trMode = trOffset;
	m_grpObjects		  = pObject;// vybr�n atribut
	m_pMap			  = pMap;
	m_nLastTrckCursor = IDC_MSCR_ARROW;
};

CTrackerBase::~CTrackerBase()
{ };

/////////////////////////////////////////////////////////////////////////////
// Drawing

// it draws white frames around selected objects 
void CTrackerBase::Draw(CDC* pDC) 
{
	// set initial DC state

	int nSaveDC = pDC->SaveDC();
	ASSERT(nSaveDC != 0);
	pDC->SetMapMode(MM_TEXT);
	pDC->SetViewportOrg(0, 0);
	pDC->SetWindowOrg(0, 0);

	pDC->SetBkMode(OPAQUE);
	pDC->SetBkColor(RGB(255,255,255));
	// Pen a Brush na obrys
	//CPen*	pOldPen   = pDC->SelectObject(CPen::FromHandle(GetTrackBlackPen()));
	//CBrush* pOldBrush = (CBrush*)pDC->SelectStockObject(NULL_BRUSH);
	int		nOldROP	  = pDC->SetROP2(R2_COPYPEN);

	// vykreslen� r�mu a hits
	m_devPos.OnDrawObjectTrackInDP(pDC,this);

	// cleanup pDC state
	pDC->SetROP2(nOldROP);
	//pDC->SelectObject(pOldPen);
	//pDC->SelectObject(pOldBrush);

  //if (m_bTracking)  
  //  DrawTrackerAttr(pDC, FALSE);     
  

	VERIFY(pDC->RestoreDC(nSaveDC));
};

void CTrackerBase::DrawTrackerAttr(CDC* pDC,BOOL bErase)
{
	//if ((bErase) && (!m_bTrackVisible)) return;
  // Now every object set his own pen
	//HPEN	hOldPen = (HPEN)::SelectObject(pDC->m_hDC,::GetStockObject(WHITE_PEN));
	//HBRUSH	hOldBr  = (HBRUSH)::SelectObject(pDC->m_hDC,::GetStockObject(NULL_BRUSH));
	int		nOldROP = pDC->SetROP2(R2_XORPEN);	
  pDC->SetBkMode(TRANSPARENT);	

	if (bErase)
	{	
		// vykreslen� v DP
    if (m_bTrackVisible)
    {    
		  m_oldTrckPos.OnDrawObjectTrackInDP(pDC,this);
      m_bTrackVisible = FALSE;
    }
  }
  else
	{	
		// zobraz�m
    if (!m_bTrackVisible)
    {    
      m_bTrackVisible = TRUE;
      m_oldTrckPos.CopyFrom(&m_finalDevPos);
      m_oldTrckPos.OnDrawObjectTrackInDP(pDC,this);
    }
  }

	// vr�cen� prost�edk�
	pDC->SetROP2(nOldROP);
	//::SelectObject(pDC->m_hDC,hOldPen);
	//::SelectObject(pDC->m_hDC,hOldBr);
};


/////////////////////////////////////////////////////////////////////////////
// Cursor

void CTrackerBase::SetTrackCursor()
{
	//int nCursor = (m_bValidPosition && m_bFreeMouseAct) ? (/*m_bInsertObject ? IDC_MSCR_INSERT : */IDC_MSCR_MOVE) : IDC_MSCR_ERROR;
  int nCursor = IDC_MSCR_MOVE;
	if (nCursor != m_nLastTrckCursor)
	{
		m_nLastTrckCursor = nCursor;
		::SetCursor(LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(m_nLastTrckCursor)));		
	}
};

/////////////////////////////////////////////////////////////////////////////
// HitTests & Hull

int	CTrackerBase::HitTest(CPoint point) const
{	return HitTestHandles(point); };

// implementation helpers
int	CTrackerBase::HitTestHandles(CPoint point) const
{
	// testuji polohu na spojnic�ch
	CPoint ptMouse = m_pScroller->WindowToScrollerPosition( point );
	// CSize  szDif   = m_pScroller->GetLogInMM();
	if (m_logPos.IsPointAtTrackerPos(ptMouse, 5/*max(5,szDif.cx/2)*/))
		return hitMiddle;
	return hitNothing;
};

int	CTrackerBase::NormalizeHit(int nHandle) const
{	return nHandle; };

void CTrackerBase::GetTrueRect(LPRECT lpTrueRect) const
{
	CRect rHull;
	// v�dy pouze z final rect
	if (m_bTracking) 
  {  
		rHull = m_finalDevPos.GetPositionHull();
    rHull = m_pScroller->FromDeviceToScrollPosition(rHull);
  }
	else 
		rHull = m_logPos.GetPositionHull();
	// roz����m o max. ���ku ��ry & hit bod
	CRect rHits = rHull;
	rHits.InflateRect(GetTrackHandleSize(),GetTrackHandleSize());
	rHull.InflateRect(ATTR_LINE_EXTRA,ATTR_LINE_EXTRA);
	rHull.UnionRect(rHull,rHits);
	// p�evod na DP	
	FromSrollerToTracker(rHull,rHull);
	// ur�en rect	
	CopyRect(lpTrueRect,rHull);
};

// modifications by scroller
void CTrackerBase::ScrollTracker(int xAmount,int yAmount)
{	
  m_devPos.OffsetPosition(xAmount,yAmount); 

  if (m_bTracking)
  {
    DrawTrackerAttr(m_pScroller->GetDC(), TRUE);
    m_finalDevPos.OffsetPosition(xAmount,yAmount);     
  }
};

void CTrackerBase::FromScrollToTracker(CPositionTracker* pDest,CPositionTracker* pSrc)
{
	pDest->CopyFrom(pSrc);
	pDest->ScrolltoDP(m_pScroller);
};

/////////////////////////////////////////////////////////////////////////////
// Tracking
BOOL CTrackerBase::Track(CWnd* pWnd, CPoint point, BOOL bAllowInvert, CWnd* pWndClipTo)
{	// mus� b�t pou�it jin� form�t funkce
	return Track(pWnd,&point);
};

// Move attribute
//BOOL CTrackerBase::TrackAttrib(CWnd* pWnd, CPoint* ptFrom)
BOOL CTrackerBase::Track(CWnd* pWnd, CPoint* ptFrom)
{
	//ASSERT(m_pOwnerObj != NULL);
	// provedu vlastn� track
	if (TrackHandle(hitMiddle, pWnd, ptFrom))
	{		
		// zm�nilo se n�co ?
		if (!m_devPos.IsSamePos(&m_finalDevPos)) 
    {
      m_finalDevPos.ResetPosition();      
			return TRUE; // bude zm�n�no v update
    }
  }
	//FromScrollToTracker(&m_devPos,&m_logPos);
  m_finalDevPos.ResetPosition();
  //m_finalPos.ResetPosition();
	return FALSE;
};

BOOL CTrackerBase::TrackHandle(int nHandle, CWnd* pWnd, CPoint* ptFrom)
{
	ASSERT(m_pScroller  == pWnd);
	//ASSERT(m_pOwnerObj != NULL);
	m_pClipWnd      = pWnd;		// nesm� opustit okno
	m_oldMouseParam = 0L;		// no auto-scroll before
  m_rotatedGrad = 0; 
  m_offset = CPoint(0,0);		
  m_fixCenterOffset = m_logPos.m_ptCenter - m_pScroller->WindowToScrollerPosition(*ptFrom);
  m_bTrackVisible = FALSE;

  //m_finalPos.CopyFrom(&m_logPos);
  m_finalDevPos.CopyFrom(&m_devPos);
  
	// don't handle if capture already set
	if (::GetCapture() != NULL)	return FALSE;
	//ASSERT(!m_bTrackVisible);
	// update cusoru
	m_nLastTrckCursor = IDC_MSCR_ARROW;
	SetTrackCursor();
	// set capture to the window which received this message
	pWnd->SetCapture();
	ASSERT(pWnd == CWnd::GetCapture());
	pWnd->UpdateWindow();
	
	// get DC for drawing
	m_pDrawDC = pWnd->GetDC();
	ASSERT_VALID(m_pDrawDC);

	m_bTracking = TRUE;

  m_bSnapping = TRUE;
  //m_SnapDistDev = 5000;

  if (m_bSnapping)
  {
    CollectSnapToObjects();    
  }

	BOOL bCancel= FALSE;
	// get messages until capture lost or cancelled/accepted
	for (;;)
	{
		MSG msg;
		VERIFY(::GetMessageA(&msg, NULL, 0, 0));
		if (CWnd::GetCapture() != pWnd)
			break;

    CPoint ptMouse;
    GetCursorPos(&ptMouse);
    m_pScroller->ScreenToClient(&ptMouse);
    OnAutoScroll(ptMouse);

		switch (msg.message)
		{
		// handle movement/accept messages
		case WM_LBUTTONUP:   
		case WM_MOUSEMOVE:
			if (!OnMouseMessage(nHandle,msg.message,msg.wParam, msg.lParam,pWnd,pWnd))
				goto ExitLoop;      
			break;
		// handle cancel messages
		case WM_KEYDOWN:
			if (msg.wParam != VK_ESCAPE)
				break;
		case WM_RBUTTONDOWN:
			DrawTrackerAttr(m_pDrawDC,TRUE);
			bCancel = TRUE;
			goto ExitLoop;
		case WM_SETCURSOR:
			if (LOWORD(msg.lParam) != HTCLIENT)
			{
				::SetCursor(LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_MSCR_ERROR)));
			} else
				::SetCursor(LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(/*bInsert?IDC_MSCR_INSERT:*/IDC_MSCR_MOVE)));
			break;
   // case WM_MOUSEWHEEL: // does not allow zoom in this mode
   //   break;
		// just dispatch rest of the messages
		default:
      {
        DispatchMessage(&msg);
        DrawTrackerAttr(m_pDrawDC,FALSE);
        break;
      }
    }
	}
ExitLoop:
	pWnd->ReleaseDC(m_pDrawDC);
	ReleaseCapture();
  if (m_bSnapping)
    m_snapping.Reset();    
  
	// result
	m_bTracking	= FALSE;	
  return !bCancel;
};


// return TRUE if continue
BOOL CTrackerBase::OnMouseMessage(int nHandle,UINT nMsg, WPARAM wParam, LPARAM lParam, CWnd* pWnd, CWnd* pWndClipTo)
{
	CFMntScrollerTracker::OnMouseMessage(nHandle,nMsg, wParam, lParam,pWnd,pWndClipTo);


	if (nMsg==WM_LBUTTONUP || nMsg==WM_MOUSEMOVE)
	{
		CPoint ptMouse((int)(short)LOWORD(lParam),
					  (int)(short)HIWORD(lParam));
		CPoint ptLogMouse = m_pScroller->WindowToScrollerPosition(ptMouse);

    
		// none autoscroll
		CPoint ptCtAttrib = ptLogMouse; 

		// update cursoru
		SetTrackCursor();
	
    int nXMove = ptCtAttrib.x - m_logPos.m_ptCenter.x + m_fixCenterOffset.x;
    int nYMove = ptCtAttrib.y - m_logPos.m_ptCenter.y + m_fixCenterOffset.y;
    
    //m_finalPos.ResetPosition();
    //m_finalPos.CopyFrom(&m_logPos);
    switch (m_trMode) 
    {
    case trOffset:
      {     
        CPoint newOffset(nXMove, nYMove);         
        CPoint diffOffset = newOffset - m_offset;
        //m_finalPos.OffsetPosition(diffOffset.x,diffOffset.y);  
        m_offset = newOffset;
        m_rotatedGrad = 0;

        if (m_bSnapping && GetAsyncKeyState(VK_MENU) == 0)
        {
          //CPoint oldOffset = m_offset;
          SnapTrackerToObjects();
          FMntMatrix2 trans;
          trans.Rotation(m_rotatedGrad);
          m_finalDevPos.CopyFrom(&m_devPos);
          m_finalDevPos.TransformPosition(trans, m_pScroller->FromScrollToWindowPosition(m_logPos.m_ptCenter));

          CPoint devDiffOffset = m_pScroller->FromScrollToWindowPosition(m_offset) - 
            m_pScroller->FromScrollToWindowPosition(CPoint(0,0));

          m_finalDevPos.OffsetPosition(devDiffOffset.x,devDiffOffset.y);  
        }
        else
        {
          CPoint devDiffOffset = m_pScroller->FromScrollToWindowPosition(diffOffset) - 
            m_pScroller->FromScrollToWindowPosition(CPoint(0,0));

          m_finalDevPos.OffsetPosition(devDiffOffset.x,devDiffOffset.y);  
        }
        break;
      }
    case trRotate:
      {
        FMntMatrix2 trans;
        CPoint wndOriginPos = m_pScroller->FromScrollToWindowPosition(m_logPos.m_ptCenter -  m_fixCenterOffset);

        m_rotatedGrad = (float) (ptMouse.y - wndOriginPos.y);
        trans.Rotation(m_rotatedGrad);
        m_finalDevPos.CopyFrom(&m_devPos);
        m_finalDevPos.TransformPosition(trans, m_pScroller->FromScrollToWindowPosition(m_logPos.m_ptCenter));

        //FromScrollToTracker(&m_finalDevPos,&m_finalPos);
        
        break;
      }
    default:
      ASSERT(FALSE);
    }

		// jde o korektn� polohu atributu ?		
    if (nMsg==WM_LBUTTONUP)
    { 
      // ukon��m p�esun - sma�u v�e
      DrawTrackerAttr(m_pDrawDC,TRUE);
      //m_devPos.CopyFrom(&m_finalDevPos);
      return FALSE;
    } 
    else
    {      
      // p�ekresl�m v�e
      DrawTrackerAttr(m_pDrawDC,TRUE);
      DrawTrackerAttr(m_pDrawDC,FALSE);
    }		
	};
	// pokra�ovat ve zpracov�n�
	return TRUE;
};

void CTrackerBase::OnChangeDP() // device unit was changed...
{
  // p�evedu na DP
  m_devPos.CopyFrom(&m_logPos);
  m_devPos.ScrolltoDP(m_pScroller);

  if (m_bTracking)
  {
    DrawTrackerAttr(m_pScroller->GetDC(),TRUE);  
    switch (m_trMode) 
    {
    case trOffset:
      {     
        m_finalDevPos.CopyFrom(&m_devPos);       
        CPoint devOffset = m_pScroller->FromScrollToWindowPosition(m_offset) - 
          m_pScroller->FromScrollToWindowPosition(CPoint(0,0));

        m_finalDevPos.OffsetPosition(devOffset.x,devOffset.y);  
        break;
      }
    case trRotate:
      {
        m_finalDevPos.CopyFrom(&m_devPos);   

        FMntMatrix2 trans;       
        trans.Rotation(m_rotatedGrad);        
        m_finalDevPos.TransformPosition(trans,m_pScroller->FromScrollToWindowPosition(m_logPos.m_ptCenter));        
        break;
      }
    default:
      ASSERT(FALSE);
    }    
  }
}

void CTrackerBase::CollectSnapToObjects()
{
  int n = m_pMap->GetPoseidonObjectCount();

  m_snapping.Reset();
  
  for(int i = 0; i < n; i++)
  {
    CPosEdObject * obj = m_pMap->GetPoseidonObject(i);
    if (!obj)
      continue;

    if (m_grpObjects->IsObjectAtGroup(obj))
      continue;

    bool taken = false;
    Ref<CPosObjectTemplate> objTemp = obj->GetTemplate();
    if (objTemp.IsNull())
      continue;

    for(int j = 0; j < objTemp->m_snapPlugs.Size(); j++)
    {
      SnapPlug & plug = objTemp->m_snapPlugs[j];
      for(int k = 0; k < m_grpObjects->GetGroupSize(); k++)
      {
        CPosObject * trObjTemp = (CPosObject *) (*m_grpObjects)[k];
        if (!trObjTemp->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
          continue;

        CPosEdObject * trObj = (CPosEdObject *) trObjTemp;
        Ref<CPosObjectTemplate> trTemp = trObj->GetTemplate();

        if (trTemp.IsNull())
          continue;

        for(int l = 0; l < trTemp->m_snapPlugs.Size(); l++)
        {
          SnapPlug & trPlug = trTemp->m_snapPlugs[l];
          if (plug.name == trPlug.name && plug.type != trPlug.type)
          {
            //yes object can be used for snapping
            m_snapping.AddSnapToObj(obj);
            taken = true;
            break;
          }
        }

        if (taken)
          break;
      }
      if (taken)
        break;
    }    
  }
}



void CTrackerBase::SnapTrackerToObjects()
{
  m_snapping(m_offset, m_rotatedGrad, m_logPos.m_ptCenter, *m_grpObjects, *m_pMap);
}

bool FindSnapping::operator()(CPoint& offset, float& rotatedGrad, CPointVal origin, const CPosGroupObject& objs, CPoseidonMap& map)
{
  CPosEdObject * snapped;
  return operator()(snapped, offset, rotatedGrad, origin, objs, map);  
}

bool FindSnapping::operator()(CPosEdObject *& snapped, CPoint& offset, float& rotatedGrad, CPointVal origin, const CPosGroupObject& objs, CPoseidonMap& map)
{
  if (m_SnapToObjects.Size() == 0 || objs.GetGroupSize() == 0)
    return false;  

  int nGrp = objs.GetGroupSize();

  float dist = m_snapDist;

  CPoint oldOffset = offset;

  bool ret = false;
  for(int i = 0; i < m_SnapToObjects.Size(); i++)
  {
    CPosEdObject * obj = m_SnapToObjects[i];

    for(int j = 0; j < nGrp; j++)
    {
      CPosObject * trObjTemp = (CPosObject *) (objs[j]);
      if (!trObjTemp->IsKindOf(RUNTIME_CLASS(CPosEdObject)))
        continue;

      CPosEdObject * trackerObj = (CPosEdObject *)trObjTemp;
      CPoint offset2 = oldOffset;

      if (SnapObjects( offset2, rotatedGrad, origin, dist, *trackerObj, *obj, map))
      {
        ret = true;
        snapped = trackerObj;
        offset = offset2;
      }
    }
  }
  return ret;
}


/** Functions tries to snap two objects
*/
bool FindSnapping::SnapObjects(CPoint& offset, float& rotated, CPointVal origin, float & dist, 
                               CPosEdObject& tracker, CPosEdObject& obj, CPoseidonMap& map)
{
  // find matching plugs
  CPoint oldOffset = offset;
  Ref<CPosObjectTemplate> trTemp = tracker.GetTemplate();
  Ref<CPosObjectTemplate> objTemp = obj.GetTemplate();

  bool ret = false;
  for(int i = 0; i < trTemp->m_snapPlugs.Size(); i++)
  {
    SnapPlug & trPlug = trTemp->m_snapPlugs[i];
    for(int j = 0; j < objTemp->m_snapPlugs.Size(); j++)
    {
      SnapPlug & objPlug = objTemp->m_snapPlugs[j];
      if (trPlug.name == objPlug.name && trPlug.type != objPlug.type)
      {
        CPoint newOffset = oldOffset;        

        if (SnapPlugs(newOffset, rotated, origin, dist, tracker, trPlug, obj, objPlug, map))
        {
          offset = newOffset;
          ret = true;
        }
      }
    }
  }

  return ret;
}

bool FindSnapping::SnapPlugs(CPoint& offset, float& rotated, CPointVal origin, float & dist,
                             CPosEdObject& tracker, SnapPlug& trPlug, CPosEdObject& obj, SnapPlug& objPlug, CPoseidonMap& map)
{
  // I suppose that plugs have two points
  if (trPlug.pos.Size() != 2 || objPlug.pos.Size() != 2)
    return false;

  FMntVector2 vRealObj[2];
  FMntVector2 vRealTracker[2];

  REALPOS realOffsetEnd = map.FromViewToRealPos(offset);
  REALPOS realOffsetBg = map.FromViewToRealPos(CPoint(0,0));

  FMntVector2 realOffset(realOffsetEnd.x - realOffsetBg.x, realOffsetEnd.z - realOffsetBg.z);

  for(int i = 0; i < 2; i++)
  {
    Vector3 vObj = obj.PositionModelToWorld(objPlug.pos[i]);
    Vector3 vTracker = tracker.PositionModelToWorld(trPlug.pos[i]);

    vRealObj[i] = FMntVector2(vObj[0], vObj[2]);
    vRealTracker[i] = FMntVector2(vTracker[0], vTracker[2]);
    vRealTracker[i] += realOffset;
  }

  // check if they can be combinated (the two points have the same distance)
  FMntVector2 diff[2];
  diff[0] = vRealTracker[1] - vRealTracker[0];
  diff[1] = vRealObj[1] - vRealObj[0];

  
  float diffSize = diff[0].Size();
  if (abs(diff[0].Size() - diff[1].Size()) > 0.01f * diffSize) // maximal difference 1cm
  {
    // cannot match such points
    return false;
  }

  // check if matching distance isn't to long
  FMntVector2 matchDiff = vRealTracker[0] - vRealObj[0];  
  float newmatchdist = matchDiff.Size();

  if (dist <= newmatchdist)
    return false;

  // yes matching must be done. To determine a rotation means to determine an angle between diff vectors
  float grad = diff[0].GradAngleTo(diff[1]);

  rotated = grad;

  // rotation matched find move
  FMntMatrix2 trans;
  trans.Rotation(rotated);

  REALPOS realOrigin =  map.FromViewToRealPos(origin);
  FMntVector2 realVOrigin(realOrigin.x, realOrigin.z);

  vRealTracker[0] -= realOffset;
  vRealTracker[0] = trans * (vRealTracker[0] - realVOrigin) + realVOrigin;
  vRealTracker[0] += realOffset;

  realOffset += vRealObj[0] - vRealTracker[0];

  // transform real to log offset
  offset = map.FromRealToViewPos(REALPOS(realOffset[0], realOffset[1]));
  offset -= map.FromRealToViewPos(REALPOS(0,0));
  dist = newmatchdist;
  return true;
}