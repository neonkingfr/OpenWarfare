/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright © Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPosEdObject

IMPLEMENT_DYNCREATE(CPosAreaObject,CPosObject)

CPosAreaObject::CPosAreaObject(const CPoseidonMap& map) : base(map)
{
  m_cAreaEntire =
  m_cAreaFrame  = DEFAULT_COLOR;
  m_nAreaStyle  = drStyleDef;

  m_LogPosition = new CPositionArea();
  m_DevPosition = new CPositionArea();
};

CPosAreaObject::CPosAreaObject()
{
	m_cAreaEntire =
	m_cAreaFrame  = DEFAULT_COLOR;
	m_nAreaStyle  = drStyleDef;

  m_LogPosition = new CPositionArea();
  m_DevPosition = new CPositionArea();
};

CPosAreaObject::~CPosAreaObject()
{	ResetArea(); };

void CPosAreaObject::CopyFrom(const CPosObject* pSrc)
{
	ASSERT_KINDOF(CPosAreaObject,pSrc);
  base::CopyFrom(pSrc);
	// kopie dat	
	m_sAreaName		= ((CPosAreaObject*)pSrc)->m_sAreaName;
	m_cAreaEntire	= ((CPosAreaObject*)pSrc)->m_cAreaEntire;
	m_cAreaFrame	= ((CPosAreaObject*)pSrc)->m_cAreaFrame;
	m_nAreaStyle	= ((CPosAreaObject*)pSrc)->m_nAreaStyle;
};

// serializace
void CPosAreaObject::DoSerialize(CArchive& ar, DWORD nVer)
{
  base::DoSerialize(ar,nVer);
	MntSerializeString(ar, m_sAreaName);
	MntSerializeColor( ar, m_cAreaEntire);
	MntSerializeColor( ar, m_cAreaFrame);
	if (nVer >= 16)
		MntSerializeInt(   ar, m_nAreaStyle);
	
  // Saveit in real corrdinates
  if (ar.IsStoring())
  {
    int n = m_RealPosition.Size();
    MntSerializeInt(ar,n);
    if (n > 0)
      ar.Write(m_RealPosition.Data(), sizeof(*m_RealPosition.Data()) * n);
  }
  else
  {
    // reading according to version
    if (nVer < 30)
    {
      m_LogPosition->DoSerialize(ar, nVer);
      
      //there was changed VIEW_LOG_UNIT_SIZE from 50
      float frac = VIEW_LOG_UNIT_SIZE/50.0f;
      int n = NRects();
      CRectArray result;       
      for(int i = 0; i < n; i++)
      {
        CRect rect = Rect(i);
        rect.bottom = (LONG) Round(rect.bottom * frac,0);
        rect.top = (LONG) Round(rect.top * frac,0);
        rect.left = (LONG) Round(rect.left * frac,0);
        rect.right = (LONG) Round(rect.right * frac,0);
        result.Add(rect);
      }
      ResetArea();
      for(int i = 0; i < n; i++)
      {
        AddRect(result[i]);
      }

      m_LogPosition->RecalcPositionHull();
    }
    else
    {
      int n;
      MntSerializeInt(ar,n);
      m_RealPosition.Resize(n);

      if (n > 0)
        ar.Read(m_RealPosition.Data(), sizeof(*m_RealPosition.Data()) * m_RealPosition.Size());
    }
  }
};

void CPosAreaObject::PreSerializeStoring(const CPoseidonMap& map)
{
  int n = NRects();
  m_RealPosition.Resize(0);
  m_RealPosition.Reserve(n * 4, n * 4);

  for(int i = 0; i < n; i++)
  {
    CRect rect = Rect(i);
    REALPOS realPos = map.FromViewToRealPos(rect.TopLeft());
    m_RealPosition.Add(realPos.x);
    m_RealPosition.Add(realPos.z);

    realPos = map.FromViewToRealPos(rect.BottomRight());
    m_RealPosition.Add(realPos.x);
    m_RealPosition.Add(realPos.z);
  }
}

void CPosAreaObject::CalcObjectLogPosition()
{
  int n = m_RealPosition.Size() / 4;
  for(int i = 0; i < n; i++)
  {
    RECT rect;
    REALPOS realPos;
    realPos.x = m_RealPosition[4 * i];
    realPos.z = m_RealPosition[4 * i + 1];

    POINT pt = _map->FromRealToViewPos(realPos);
    rect.top = pt.y;
    rect.left = pt.x;

    realPos.x = m_RealPosition[4 * i + 2];
    realPos.z = m_RealPosition[4 * i + 3];

    pt = _map->FromRealToViewPos(realPos);
    rect.bottom = pt.y;
    rect.right = pt.x;

    AddRect(rect);
  }
  m_LogPosition->RecalcPositionHull();
}

// informace o objektu
void CPosAreaObject::GetObjectName(CString& sText) const
{	sText = m_sAreaName; };


BOOL CPosAreaObject::IsSameObject(const CPosObject & obj) const
{
  if (!obj.IsKindOf(RUNTIME_CLASS(CPosAreaObject)))
    return FALSE;

  return (((const CPosAreaObject&) obj).m_sAreaName == m_sAreaName);
}
// vykreslování objektu
void CPosAreaObject::GetDrawAreaColors(COLORREF& cEntire,COLORREF& cFrame,int& nStyle,const CDrawPar* pParams) const
{
	if (m_sAreaName.GetLength()==0)
	{	// barvy pro standardní plochu
		cEntire = pParams->m_pCfg->m_cDefColors[CPosEdCfg::defcolAreaE];
		cFrame  = pParams->m_pCfg->m_cDefColors[CPosEdCfg::defcolAreaF];
		nStyle	= pParams->m_pCfg->m_nFillSArea;
		return;
	}
	// standardní barvy
	cEntire = m_cAreaEntire;
	cFrame  = m_cAreaFrame;
	nStyle	= m_nAreaStyle;

	if (cEntire == DEFAULT_COLOR)
		cEntire = pParams->m_pCfg->m_cDefColors[CPosEdCfg::defcolNArea];
	if (cEntire == DEFAULT_COLOR)
		cFrame  = pParams->m_pCfg->m_cDefColors[CPosEdCfg::defcolFNAre];
	if (nStyle  == drStyleDef)
		nStyle	= pParams->m_pCfg->m_nFillNArea;
};

void CPosAreaObject::OnDrawObject(CDC* pDC,const CDrawPar* pParams) const
{
	// barvy pro vykreslování
	COLORREF cAreaEntire,cAreaFrame;
	int		 nStyle,nBrIndex = -1;
	GetDrawAreaColors(cAreaEntire,cAreaFrame,nStyle,pParams);
  if (pParams->m_VwStyle.m_ShowLock && Locked())
  {
    cAreaEntire = RGBToY(cAreaEntire);
    cAreaFrame = RGBToY(cAreaFrame);    
  }

	switch(nStyle)
	{
	case drStyleHori:
		nBrIndex = HS_HORIZONTAL; break;
	case drStyleVert:
		nBrIndex = HS_VERTICAL; break;
	case drStyleCros:
		nBrIndex = HS_CROSS; break;
	case drStyleLfRg:
		nBrIndex = HS_BDIAGONAL; break;
	case drStyleRgLf:
		nBrIndex = HS_FDIAGONAL; break;
	case drStyleDgCr:
		nBrIndex = HS_DIAGCROSS; break;
	default:
		nStyle = 0;
	};


  if (m_sAreaName.IsEmpty())
    m_DevPosition->OnDrawObject(pDC, cAreaFrame, -1, cAreaEntire, nBrIndex,NULL); // selection draw allways
  else
    m_DevPosition->OnDrawObject(pDC, cAreaFrame, -1, cAreaEntire, nBrIndex,pParams);

};

/////////////////////////////////////////////////////////////////////////////
// práce s plochou

BOOL CPosAreaObject::IsValidArea() const
{	return m_LogPosition->IsValidPosition(); };
void CPosAreaObject::ResetArea()
{	m_LogPosition->ResetPosition(); };


void CPosAreaObject::AddRect(const RECT& rArea)
{	static_cast<CPositionArea*>(m_LogPosition.GetRef())->AddRect(rArea); };
void CPosAreaObject::RemoveRect(const RECT& rArea)
{	static_cast<CPositionArea*>(m_LogPosition.GetRef())->RemoveRect(rArea); };

int CPosAreaObject::NRects() const
{	return static_cast<CPositionArea*>(m_LogPosition.GetRef())->NRects(); };
CRect CPosAreaObject::Rect(int i) const
{return static_cast<CPositionArea*>(m_LogPosition.GetRef())->Rect(i); };


void	CPosAreaObject::RemoveRectReal(const FltRect& rect)
{RemoveRect(_map->FromRealToViewRect(rect));}
void	CPosAreaObject::AddRectReal(const FltRect& rect)
{AddRect(_map->FromRealToViewRect(rect));}

/////////////////////////////////////////////////////////////////////
//
IMPLEMENT_DYNCREATE(CPosSelAreaObject, CPosAreaObject)

void CPosSelAreaObject::OffsetAreaLog(const CPoint& offset)
{
  m_LogPosition->OffsetPosition(offset.x, offset.y);
}

void CPosSelAreaObject::SetSelAreaType(SelAreaType type)
{
  if (type == _selType)
    return;

  if (type == satVertex)
  {
    OffsetAreaLog(CPoint(VIEW_LOG_UNIT_SIZE/2,VIEW_LOG_UNIT_SIZE/2));
  }
  else
    OffsetAreaLog(CPoint(-VIEW_LOG_UNIT_SIZE/2,-VIEW_LOG_UNIT_SIZE/2));

  _selType = type;
}

void	CPosSelAreaObject::CopyFrom(const CPosObject* pSrc)
{
  ASSERT_KINDOF(CPosSelAreaObject,pSrc);
  CPosAreaObject::CopyFrom(pSrc);
  _selType = ((CPosSelAreaObject *)pSrc)->_selType;
}

void CPosSelAreaObject::AlignToGrid(const CPoint& size, const CPoint& offset)
{
  int n = NRects();
  CRectArray result; 
  for(int i = 0; i < n; i++)
  {
    const CRect& rect = Rect(i);
    CRect res;
    res.left = (rect.left - offset.x)/size.x * size.x + offset.x;
    res.top = (rect.top - offset.y)/size.y * size.y + offset.y;
    res.right = ((rect.right - offset.x)/size.x) * size.x + offset.x;
    res.bottom = ((rect.bottom - offset.y)/size.y) * size.y + offset.y;

    if (rect.right > res.right)
      res.right += size.x;

    if (rect.bottom > res.bottom)
      res.bottom += size.x;

    result.Add(res);
  }

  ResetArea();
  for(int i = 0; i < n; i++)  
    AddRect(result[i]);

  m_LogPosition->RecalcPositionHull();
  
}

