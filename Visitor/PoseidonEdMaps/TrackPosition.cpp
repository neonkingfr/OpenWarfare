/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Poloha objektu

IMPLEMENT_DYNAMIC(CPositionTracker, CPositionGroup)

CPositionTracker::CPositionTracker()
{};

// kopie objektu
void CPositionTracker::CopyFrom(const CPositionBase* pSrc)
{
	CPositionGroup::CopyFrom(pSrc);
	ASSERT_KINDOF(CPositionTracker,pSrc);
	m_ptCenter = ((CPositionTracker*)pSrc)->m_ptCenter;
};

// vykreslov�n� trackeru
void CPositionTracker::OnDrawObjectTrackInDP(CDC* pDC,const CTrackerBase* pTracker) const
{	OnDrawTrackFrameInDP(pDC,pTracker); };

void CPositionTracker::OnDrawObjectTHitsInDP(CDC* pDC,const CTrackerBase* pTracker) const
{	OnDrawTrackHitsInDP(pDC,pTracker); };

// testov�n� polohy nad trackerem
BOOL CPositionTracker::IsPointAtTrackerPos(CPoint ptMouse,int nDif) const
{	return IsPointAtArea(ptMouse); };

// v�po�et �chopov�ho boud
void CPositionTracker::CalcAutoPositionCenter()
{
	CRect rHull = GetPositionHull();
	// v�po�et podle rectu
	m_ptCenter.x = (rHull.left + rHull.right)/2;
	m_ptCenter.y = (rHull.top  + rHull.bottom)/2;
};

// posun plochy
void CPositionTracker::OffsetPosition(int xAmount,int yAmount)
{
	CPositionGroup::OffsetPosition(xAmount,yAmount);
	m_ptCenter.x += xAmount;
	m_ptCenter.y += yAmount;
};

void CPositionTracker::TransformPosition(FMntMatrix2Val trans, CPointVal origin)
{
  CPositionGroup::TransformPosition(trans,origin);
  m_ptCenter = trans * (m_ptCenter - origin) + origin;
}
// p�evod na DP z LP
void CPositionTracker::LPtoDP(CDC* pDC)
{
	CPositionGroup::LPtoDP(pDC);
	pDC->LPtoDP(&m_ptCenter);
};
// p�evod na DP ze Scrolleru
void CPositionTracker::ScrolltoDP(CFMntScrollerView* pView)
{
	CPositionGroup::ScrolltoDP(pView);
	m_ptCenter = pView->FromScrollToWindowPosition(m_ptCenter);
};
