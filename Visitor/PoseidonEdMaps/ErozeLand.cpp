/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"
#include "math.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/*
void World::WashLandscape( struct washParams &pars, bool selected )
{
    // very long operation - show progress bar
        
    progress->Create(this);
    progress->SetMax(pars.nGens);
    progress->SetPos(0);
    //progress->EnableCancel();
    
    typedef float w_type;
    // simulate water washing out the surface
    // suppose, that material is homogenous
    // and washing out is dependent only on the water flow
    
    // Later - maybe we could consider washing high places
    // less than low - higher are probably rocks
    
     int z,x;
     
    static w_type water0[ZRange][XRange]; // water - original state
    static w_type water[ZRange][XRange]; // water after one simulation round
    
    static w_type sediment0[ZRange][XRange]; // sediments flown - original state
    static w_type sediment[ZRange][XRange]; // sediments after one simulation round
    
    static coordType landscapeN[ZRange][XRange]; // land - after one simulation round
    
    // rain is falling - init water
    for( z=0; z<ZRange; z++ )  for( x=0; x<XRange; x++ )
    {
        
        water[z][x]=pars.initRain; // begin with strong rain everywhere
        sediment[z][x]=0; // clear water
        landscapeN[z][x]=Landscape(z,x);
    }

    // wash determines speed of the process
    // sedim determines erosion level
    
    for( int simRound=0; simRound<pars.nGens; simRound++ )
    {
        // update current land and water state
        for( z=0; z<ZRange; z++ )  for( x=0; x<XRange; x++ )
        {
            //if( landscapeN[z][x]<=0 ) landscapeN[z][x]=0;
            SetLandscape(z,x,landscapeN[z][x]);
            
            water[z][x]+=pars.steadyRain; // slow steady rain
            
            water0[z][x]=water[z][x];
            sediment0[z][x]=sediment[z][x];
        }
        
        // do one round of simulation
        
        WRect sel=IfSelection();
        for( z=sel.y+1; z<sel.y+sel.h-1; z++ ) 
        for( x=sel.x+1; x<sel.x+sel.w-1; x++ )
        if( !selected || obj.Selected(z,x) )
        {
            coordType pointY=Landscape(z,x);
            w_type waterY=water0[z][x];
            w_type sedimentY=sediment0[z][x];
            
            coordType pointUp=Landscape(z+1,x);
            coordType pointDown=Landscape(z-1,x);
            coordType pointLeft=Landscape(z,x-1);
            coordType pointRight=Landscape(z,x+1);
            
            w_type flowUp=pointY-pointUp;
            w_type flowDown=pointY-pointDown;
            w_type flowLeft=pointY-pointLeft;
            w_type flowRight=pointY-pointRight;
            
            coordType minY=min(min(pointUp,pointDown),min(pointLeft,pointRight));
            //coordType maxY=max(max(pointUp,pointDown),max(pointLeft,pointRight));
            
            // simulate only flow-out, in will be simulated when simulating neighbourgh
            w_type flowSum=0;
            if( flowUp>0 ) flowSum+=flowUp;
            if( flowDown>0 ) flowSum+=flowDown;
            if( flowLeft>0 ) flowSum+=flowLeft;
            if( flowRight>0 ) flowSum+=flowRight;
            
            w_type deltaUp=0.0;
            w_type deltaDown=0.0;
            w_type deltaLeft=0.0;
            w_type deltaRight=0.0;
            
            // land washed-out
            // calculate water speed
            w_type flowTotal=fabs(flowUp)+fabs(flowDown)+fabs(flowLeft)+fabs(flowRight);
            
            // cannot wash lower than to lowest neighbourgh level
            w_type maxWash=max(0,pointY-minY);
            // cannot sediment higher than to highest neighbourgh level
            //w_type maxSediment=max(0,maxY-pointY);
            
            // depending on water speed - slow water sediments, fast washes out
            w_type landWashed=waterY*flowTotal*pars.washCoef;
            if( landWashed>maxWash ) landWashed=maxWash;
            
            w_type landSedimented;
            if( flowTotal>0 ) landSedimented=sedimentY*(pars.sedimCoef/flowTotal+pars.sedimBase);
            else landSedimented=sedimentY;
            if( landSedimented>sedimentY ) landSedimented=sedimentY;
            //if( landSedimented>maxSediment ) landSedimented=maxSediment;
            
            // simulate sediments or wash-out
            // flow out land - depending on total water flow
            landscapeN[z][x]-=landWashed-landSedimented*pars.sedimEff;
            
            sediment[z][x]+=landWashed-landSedimented;

            // avoid distibution of sedimented land
            sedimentY+=landWashed-landSedimented;
            // flow to the neighbourgh points
            if( flowUp>0 ) deltaUp=flowUp/flowSum;
            if( flowDown>0 ) deltaDown=flowDown/flowSum;
            if( flowLeft>0 ) deltaLeft=flowLeft/flowSum;
            if( flowRight>0 ) deltaRight=flowRight/flowSum;
            
            if( flowSum>0 )
            {
                // distribute sediments
                sediment[z+1][x]+=deltaUp*sedimentY;
                sediment[z-1][x]+=deltaDown*sedimentY;
                sediment[z][x-1]+=deltaLeft*sedimentY;
                sediment[z][x+1]+=deltaRight*sedimentY;
                
                // distribute water
                water[z+1][x]+=deltaUp*waterY;
                water[z-1][x]+=deltaDown*waterY;
                water[z][x-1]+=deltaLeft*waterY;
                water[z][x+1]+=deltaRight*waterY;
                
                // flow out water and sediments
                // sum(delta...)=1.0
                
                water[z][x]-=waterY;
                sediment[z][x]-=sedimentY;
            }
        }

        // water disappears on the borders
        for( z=0; z<ZRange; z++ ) water[z][0]=water[z][XRange-1]=0;
        for( x=0; x<XRange; x++ ) water[0][x]=water[ZRange-1][x]=0;
        
        
        if( simRound%5==0 )
        {
            //DoBlur();
            RedrawViews();
        }
        progress->SetPos(simRound);
        if( progress->Canceled() ) break;
    }
    progress->Close();
    
    SetDirty();
    
    if( pars.finalBlur ) DoBlur(selected);
    RedrawViews();
}

void World::DoBlur( bool selected )
{
    static coordType landscape0[ZRange][XRange];
    #define blurSize 1
    
    int z,x,zz,xx;
    for( z=0; z<ZRange; z++ )  for( x=0; x<XRange; x++ )
    {
        landscape0[z][x]=Landscape(z,x);
    }
    
    WRect rect=IfSelection();
    for( z=rect.y+blurSize; z<rect.y+rect.h-blurSize; z++ )
    for( x=rect.x+blurSize; x<rect.x+rect.w-blurSize; x++ )
    if( !selected || obj.Selected(z,x) )
    {
        coordType sum=0;
        for( zz=z-blurSize; zz<=z+blurSize; zz++ ) for( xx=x-blurSize; xx<=x+blurSize; xx++ )
        {
            sum+=landscape0[zz][xx];
        }
        SetLandscape(z,x,sum/(blurSize*2+1)/(blurSize*2+1));
    }

    SetDirty();
}

WRect World::IfSelection() const
{
    if( _rectSelection.h>0 && _rectSelection.w>0 ) return _rectSelection;
    return WRect(0,0,XRange,ZRange);
}

void World::LandZero( bool selected )
{
    int z,x;
    WRect rect=IfSelection();
    for( z=rect.y; z<rect.y+rect.h; z++ ) for( x=rect.x; x<rect.x+rect.w; x++ )
    if( !selected || obj.Selected(z,x) )
    {
        dirty=true;
        SetLandscape(z,x,LAND_BASE_LEVEL);
    }
}
*/

/////////////////////////////////////////////////////////////////////////////
//	eroze krajiny

// transformace prametr�
#define coordType				LANDHEIGHT
#define XRange				m_nWidth
#define ZRange				m_nHeight
#define Landscape(z,x)		GetLandscapeHeight(x,z)
#define SetLandscape(z,x,h) SetLandscapeHeight(x,z,h,true,false)
#define IsSelected(z,x)		IsUnitCenterAtArea(x,z,&Area)


void CPoseidonMap::DoBlur(CPositionArea& Area)
{
    static coordType landscape0[MAX_WORLD_SIZE][MAX_WORLD_SIZE];
    #define blurSize 1
    
    int z,x,zz,xx;
    for( z=0; z<ZRange; z++ )  for( x=0; x<XRange; x++ )
    {
        landscape0[z][x]=Landscape(z,x);
    }

	
	CRect	rLogHull;		rLogHull = Area.GetPositionHull();
	UNITPOS LftTop,RghBtm;	GetUnitPosHull(LftTop,RghBtm,rLogHull);
	for( z=LftTop.z; z>=RghBtm.z; z--)
	for( x=LftTop.x; x<=RghBtm.x; x++)
  {        
    UNITPOS upos;
    upos.x = x;
    upos.z = z;
    if (Area.IsPointAtArea(FromUnitToViewPos(upos)))  //Is selected?  
    {
        coordType sum=0;
        // is it on map?
        zz=z-blurSize;
        xx=x-blurSize;

        saturateMax(zz, 0);
        saturateMax(xx, 0);
        
        int ze = z + blurSize;
        int xe = x + blurSize;

        saturateMin(ze, ZRange - 1);
        saturateMin(xe, XRange - 1);

        for( ; zz<=ze; zz++ ) for(; xx<=xe; xx++ )
        {
            sum+=landscape0[zz][xx];
        }
        SetLandscape(z,x,sum/(blurSize*2+1)/(blurSize*2+1));
    }
  }

}; 

void CPoseidonMap::DoLandEroze(WashParams& Params,CPositionArea& Area)
{
  AfxGetApp()->BeginWaitCursor();

  // obal plochy    
  CRect	rLogHull;		rLogHull = Area.GetPositionHull();
  UNITPOS LftTop,RghBtm;	GetUnitPosHull(LftTop,RghBtm,rLogHull);

  typedef float w_type;
  // simulate water washing out the surface
  // suppose, that material is homogenous
  // and washing out is dependent only on the water flow

  // Later - maybe we could consider washing high places
  // less than low - higher are probably rocks

  int z,x;
  int nDimX = RghBtm.x - LftTop.x + 1;
  int nDimZ = LftTop.z - RghBtm.z + 1;

  int nDim = nDimX * nDimZ;

  w_type * water0 = new w_type[nDim];		// water - original state
  w_type * water = new w_type[nDim];		// water after one simulation round

  w_type * sediment0 = new w_type[nDim];	// sediments flown - original state
  w_type * sediment = new w_type[nDim];		// sediments after one simulation round

  coordType * landscapeN = new coordType[nDim];	// land - after one simulation round

  if (water0 == NULL || water == NULL || sediment0 == NULL || sediment == NULL|| landscapeN == NULL)
  {    
    delete [] water0;
    delete [] water;
    delete [] sediment0;
    delete [] sediment;
    delete [] landscapeN;
    return; 
  }

  // rain is falling - init water
  for( z=0; z < nDimZ; z++)
    for( x=0; x < nDimX; x++)
  {

    water[x + z * nDimX]=(float)Params.initRain; // begin with strong rain everywhere
    sediment[x + z * nDimX]=0; // clear water
    landscapeN[x + z * nDimX]=Landscape(RghBtm.z + z, LftTop.x + x);
  }

  // wash determines speed of the process
  // sedim determines erosion level

  for( int simRound=0; simRound<Params.nGens; simRound++ )
  {
    // update current land and water state
    for( z=0; z < nDimZ; z++) for( x=0; x < nDimX; x++)
    {      
      //if( landscapeN[z][x]<=0 ) landscapeN[z][x]=0;
      SetLandscape(RghBtm.z + z, LftTop.x + x,landscapeN[x + nDimX * z]);

      water[x + nDimX * z]+=(float)Params.steadyRain; // slow steady rain

      water0[x + nDimX * z]=water[x + nDimX * z];
      sediment0[x + nDimX * z]=sediment[x + nDimX * z];
    }

    // do one round of simulation
    for( z=0; z < nDimZ; z++) for( x=0; x < nDimX; x++)
      {
        UNITPOS upos;
        upos.x = LftTop.x + x;
        upos.z = RghBtm.z + z;
        if (Area.IsPointAtArea(FromUnitToViewPos(upos)))  //Is selected?        
        {
          coordType pointY=Landscape(upos.z,upos.x);
          w_type waterY=water0[x + nDimX * z];
          w_type sedimentY=sediment0[x + nDimX * z];

          coordType pointUp=Landscape(upos.z+1,upos.x);
          coordType pointDown=Landscape(upos.z-1,upos.x);
          coordType pointLeft=Landscape(upos.z,upos.x-1);
          coordType pointRight=Landscape(upos.z,upos.x+1);

          w_type flowUp=pointY-pointUp;
          w_type flowDown=pointY-pointDown;
          w_type flowLeft=pointY-pointLeft;
          w_type flowRight=pointY-pointRight;

          coordType minY=min(min(pointUp,pointDown),min(pointLeft,pointRight));
          //coordType maxY=max(max(pointUp,pointDown),max(pointLeft,pointRight));

          // simulate only flow-out, in will be simulated when simulating neighbourgh
          w_type flowSum=0;
          if( flowUp>0 ) flowSum+=flowUp;
          if( flowDown>0 ) flowSum+=flowDown;
          if( flowLeft>0 ) flowSum+=flowLeft;
          if( flowRight>0 ) flowSum+=flowRight;

          w_type deltaUp=0.0;
          w_type deltaDown=0.0;
          w_type deltaLeft=0.0;
          w_type deltaRight=0.0;

          // land washed-out
          // calculate water speed
          w_type flowTotal=(float)(fabs(flowUp)+fabs(flowDown)+fabs(flowLeft)+fabs(flowRight));

          // cannot wash lower than to lowest neighbourgh level
          w_type maxWash=max(0,pointY-minY);
          // cannot sediment higher than to highest neighbourgh level
          //w_type maxSediment=max(0,maxY-pointY);

          // depending on water speed - slow water sediments, fast washes out
          w_type landWashed=waterY*flowTotal*(float)Params.washCoef;
          if( landWashed>maxWash ) landWashed=maxWash;

          w_type landSedimented;
          if( flowTotal>0 ) landSedimented=sedimentY*((float)Params.sedimCoef/flowTotal+(float)Params.sedimBase);
          else landSedimented=sedimentY;
          if( landSedimented>sedimentY ) landSedimented=sedimentY;
          //if( landSedimented>maxSediment ) landSedimented=maxSediment;

          // simulate sediments or wash-out
          // flow out land - depending on total water flow
          landscapeN[x + nDimX * z]-=landWashed-landSedimented*(float)Params.sedimEff;

          sediment[x + nDimX * z]+=landWashed-landSedimented;

          // avoid distibution of sedimented land
          sedimentY+=landWashed-landSedimented;
          // flow to the neighbourgh points
          if( flowUp>0 ) deltaUp=flowUp/flowSum;
          if( flowDown>0 ) deltaDown=flowDown/flowSum;
          if( flowLeft>0 ) deltaLeft=flowLeft/flowSum;
          if( flowRight>0 ) deltaRight=flowRight/flowSum;

          if( flowSum>0 )
          {
            if (z != 0)
            {            
              sediment[x + nDimX * (z - 1)]+=deltaDown*sedimentY;
              water[x + nDimX * (z - 1)]+=deltaDown*waterY;
            }

            if (x != 0)
            {
              sediment[x - 1 + nDimX * z]+=deltaLeft*sedimentY;
              water[x - 1 + nDimX * z]+=deltaLeft*waterY;
            }

            if (z != nDimZ - 1)
            {
              sediment[x + nDimX * (z + 1)]+=deltaUp*sedimentY;
              water[x + nDimX * (z + 1)]+=deltaUp*waterY;
            }

            if (x != nDimX - 1)
            {
              sediment[x + 1 + nDimX * z]+=deltaRight*sedimentY;
              water[x + 1 + nDimX * z]+=deltaRight*waterY;
            }

            // flow out water and sediments
            // sum(delta...)=1.0

            water[x + nDimX * z]-=waterY;
            sediment[x + nDimX * z]-=sedimentY;            
          }
        }
      }

      // water disappears on the borders
      for( z=0; z<nDimZ; z++ ) water[z * nDimX]=water[nDimX-1 + z * nDimX]=0;
      for( x=0; x<nDimX; x++ ) water[x]=water[x +  (nDimZ-1) * nDimX]=0;
  }

  delete [] water0;
  delete [] water;
  delete [] sediment0;
  delete [] sediment;
  delete [] landscapeN;

  if( Params.finalBlur ) 
    DoBlur(Area);

  // update objects height
  rLogHull.left -= VIEW_LOG_UNIT_SIZE;
  rLogHull.top -= VIEW_LOG_UNIT_SIZE;
  rLogHull.right += VIEW_LOG_UNIT_SIZE;
  rLogHull.bottom += VIEW_LOG_UNIT_SIZE;

  UpdateObjectsHeight(rLogHull);

  AfxGetApp()->EndWaitCursor();
};


