/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPosKeyPointObject

IMPLEMENT_DYNCREATE(CPosKeyPointObject,CPosObject)

CPosKeyPointObject::CPosKeyPointObject(const CPoseidonMap& map) : base(map)
{
  m_cEntire =
    m_cFrame  = DEFAULT_COLOR;
  m_nStyle  = drStyleDef;
  m_nKeyPtID= UNDEF_REG_ID;
  m_bVisible= TRUE;
  m_ptRealPos.x =
    m_ptRealPos.z = 0.f;
  m_nRealWidth	=
    m_nRealHeight	= 100.f;	

  m_LogPosition = new CPositionEllipse();
  m_DevPosition = new CPositionEllipse();
}

CPosKeyPointObject::CPosKeyPointObject()
{
	m_cEntire =
	m_cFrame  = DEFAULT_COLOR;
	m_nStyle  = drStyleDef;
	m_nKeyPtID= UNDEF_REG_ID;
	m_bVisible= TRUE;
	m_ptRealPos.x =
	m_ptRealPos.z = 0.f;
	m_nRealWidth	=
	m_nRealHeight	= 100.f;

  m_LogPosition = new CPositionEllipse();
  m_DevPosition = new CPositionEllipse();
};

CPosKeyPointObject::~CPosKeyPointObject()
{
};

void CPosKeyPointObject::CopyFrom(const CPosObject* pSrc)
{
	ASSERT_KINDOF(CPosKeyPointObject,pSrc);
  base::CopyFrom(pSrc);
	// kopie dat	
	m_sName		= ((CPosKeyPointObject*)pSrc)->m_sName;
	m_cEntire	= ((CPosKeyPointObject*)pSrc)->m_cEntire;
	m_cFrame	= ((CPosKeyPointObject*)pSrc)->m_cFrame;
	m_nStyle	= ((CPosKeyPointObject*)pSrc)->m_nStyle;
	m_bVisible	= ((CPosKeyPointObject*)pSrc)->m_bVisible;

	m_ptRealPos		= ((CPosKeyPointObject*)pSrc)->m_ptRealPos;
	m_nRealWidth	= ((CPosKeyPointObject*)pSrc)->m_nRealWidth;
	m_nRealHeight	= ((CPosKeyPointObject*)pSrc)->m_nRealHeight;

	m_nKeyPtID	= ((CPosKeyPointObject*)pSrc)->m_nKeyPtID;
  m_Type=((CPosKeyPointObject*)pSrc)->m_Type;
  m_Text=((CPosKeyPointObject*)pSrc)->m_Text;
  m_otherProp=((CPosKeyPointObject*)pSrc)->m_otherProp;
	// sou�adnice

};

// serializace
void CPosKeyPointObject::DoSerialize(CArchive& ar, DWORD nVer)
{
  base::DoSerialize(ar,nVer);
	MntSerializeString(ar, m_sName);
	MntSerializeColor( ar, m_cEntire);
	MntSerializeColor( ar, m_cFrame);
	MntSerializeInt(   ar, m_nStyle);
	MntSerializeBOOL(  ar, m_bVisible);
	MntSerializeFloat( ar, m_ptRealPos.x);
	MntSerializeFloat( ar, m_ptRealPos.z);
	MntSerializeFloat( ar, m_nRealWidth);
	MntSerializeFloat( ar, m_nRealHeight);
	MntSerializeInt(   ar, m_nKeyPtID);
	// logick� sou�adnice
  if (nVer <= 29)
	  m_LogPosition->DoSerialize(ar, nVer);
  if (nVer >= 57)
  {
    MntSerializeString(ar,m_Text);
    MntSerializeString(ar,m_Type);
    MntSerializeString(ar,m_otherProp);
  }

};

// informace o poloze objektu

// informace o objektu
void CPosKeyPointObject::GetObjectName(CString& sText) const
{	sText = m_sName; };


// vykreslov�n� objektu
void CPosKeyPointObject::GetDrawAreaColors(COLORREF& cEntire,COLORREF& cFrame,int& nStyle,const CDrawPar* pParams) const
{
	// standardn� barvy
	cEntire = m_cEntire;
	cFrame  = m_cFrame;
	nStyle	= m_nStyle;

	if (cEntire == DEFAULT_COLOR)
		cEntire = pParams->m_pCfg->m_cDefColors[CPosEdCfg::defcolKeyPt];
	if (cEntire == DEFAULT_COLOR)
		cFrame  = pParams->m_pCfg->m_cDefColors[CPosEdCfg::defcolFKeyP];
	if (nStyle  == drStyleDef)
		nStyle	= pParams->m_pCfg->m_nFillKeyPt;
};

void CPosKeyPointObject::OnDrawObject(CDC* pDC,const CDrawPar* pParams) const
{
  if (pParams == NULL)
    return;

	// barvy pro vykreslov�n�
	COLORREF cAreaEntire,cAreaFrame;
	int		 nStyle,nBrIndex = -1;
	GetDrawAreaColors(cAreaEntire,cAreaFrame,nStyle,pParams);
	switch(nStyle)
	{
	case drStyleHori:
		nBrIndex = HS_HORIZONTAL; break;
	case drStyleVert:
		nBrIndex = HS_VERTICAL; break;
	case drStyleCros:
		nBrIndex = HS_CROSS; break;
	case drStyleLfRg:
		nBrIndex = HS_BDIAGONAL; break;
	case drStyleRgLf:
		nBrIndex = HS_FDIAGONAL; break;
	case drStyleDgCr:
		nBrIndex = HS_DIAGCROSS; break;
	default:
		nStyle = 0;
	};

  if (pParams->m_VwStyle.m_ShowLock && Locked())
  {
    cAreaEntire = RGBToY(cAreaEntire);
    cAreaFrame = RGBToY(cAreaFrame);
  }

  m_DevPosition->OnDrawObject(pDC,cAreaFrame, pParams->m_VwStyle.m_bKeyPtBorder ? -1 : -2 ,
    cAreaEntire, pParams->m_VwStyle.m_bKeyPtEntire ? nBrIndex : -2, pParams);

  if (m_Text.GetLength())
  {  
    CRect insideRect=m_DevPosition->GetPositionHull();
    int sz=toLargeInt((float)insideRect.Size().cx/(float)m_Text.GetLength());
    if (sz>0)
    {    
      CFont fnt;
      fnt.CreateFont(-sz*2,(int)(sz*0.9),0,0,FW_BOLD,0,0,0,0,0,0,0,0,"Courier");
      CFont *oldf=pDC->SelectObject(&fnt);
      pDC->SetTextColor(0);
      pDC->SetBkMode(TRANSPARENT);
      pDC->SetTextAlign(TA_CENTER|TA_BASELINE);
      CPoint pt=m_DevPosition->GetCenter();
      pDC->TextOut(pt.x+2,pt.y+2,m_Text);
      pDC->SetTextColor(0xFFFFFF);
      pDC->TextOut(pt.x,pt.y,m_Text);
      pDC->SelectObject(oldf);
    }
  }

	/*CBrush  brE(nBrIndex,cAreaEntire);
	CBrush  brS(cAreaEntire);
	CPen cPen(PS_SOLID, 2, cAreaFrame); 
  CPen cPenNull(PS_NULL, 0, cAreaFrame); 
  
  CPen * cPenOld; 
  // or�mov�n�
  if (pParams->m_VwStyle.m_bKeyPtBorder)	
    cPenOld = pDC->SelectObject(&cPen);	
  else
    cPenOld = pDC->SelectObject(&cPenNull);

	// v�pl� 
	if ( pParams->m_VwStyle.m_bKeyPtEntire)
	{
    CBrush * cBrushOld;  
		if (nStyle!=0)
			cBrushOld = pDC->SelectObject(&brE);
		else
			cBrushOld = pDC->SelectObject(&brS);

    pDC->SetBkMode(TRANSPARENT);
    pDC->Ellipse(m_DevPosition.m_Position);
    pDC->SelectObject(&cBrushOld);  
	}
  else
    pDC->Arc(m_DevPosition.m_Position, CPoint(0,0), CPoint(0,0));
    
	pDC->SelectObject(&cPenOld);*/
};

BOOL CPosKeyPointObject::IsSameObject(const CPosObject & obj) const
{
  if (!obj.IsKindOf(RUNTIME_CLASS(CPosKeyPointObject)))
    return FALSE;

  return (((const CPosKeyPointObject&) obj).m_sName == m_sName);
}

// informace o objektu
BOOL CPosKeyPointObject::IsEnabledObject(const EnabledObjects& enabled) const
{
	return (enabled.m_bKeyPt && m_bVisible && !_hidden);
};

// editace objektu
BOOL CPosKeyPointObject::DoEditObject()
{	return  TRUE; };

// v�po�et logick�ch sou�adnic objektu
void CPosKeyPointObject::CalcObjectLogPosition()
{
	float nMSize = ((float)VIEW_LOG_UNIT_SIZE)/_map->GetDocument()->m_nRealSizeUnit;
	// p�evod na log.
	CPoint  ptCenter = _map->FromRealToViewPos(m_ptRealPos);
	// rozm�ry objektu podle st�edu
  int nW = (int) (m_nRealWidth*nMSize/2.f);
	int nH = (int) (m_nRealHeight*nMSize/2.f);
	// nastaven� polohy

  CRect rect;
  rect.SetRect(ptCenter.x - nW, ptCenter.y - nH, ptCenter.x + nW, ptCenter.y + nH);
  static_cast<CPositionEllipse*>(m_LogPosition.GetRef())->Create(rect);
};

void CPosKeyPointObject::CalcObjectPosPosition()
{
	CPoint  ptCenter = m_LogPosition->GetCenter();
	// nastaven� polohy
	m_ptRealPos = _map->FromViewToRealPos(ptCenter);
};

// �prava polohy objekt�
void CPosKeyPointObject::CalcOffsetLogPosition(int xAmount,int yAmount)
{
  // posun sou�adnic
  m_LogPosition->OffsetPosition( xAmount, yAmount);
 	
	// nastaven� polohy
	m_ptRealPos = _map->FromViewToRealPos(m_LogPosition->GetCenter());	
};

void CPosKeyPointObject::CalcRotateLogPosition(float grad,CPointVal origin)
{
  REALPOS realOrigin = _map->FromViewToRealPos(origin);
  FMntVector2 vecOrigin(realOrigin.x, realOrigin.z);
  FMntVector2 vecPos(m_ptRealPos.x, m_ptRealPos.z);

  FMntMatrix2 trans;
  trans.Rotation(/*360 -*/ grad);

  vecPos = trans * (vecPos - vecOrigin) + vecOrigin;
  
  m_ptRealPos.x = vecPos[0];
  m_ptRealPos.z = vecPos[1];

  CalcObjectLogPosition();
};

int CPosKeyPointObject::SetFreeID()
{
  return m_nKeyPtID = _map->GetFreeKeyPtID();
}
