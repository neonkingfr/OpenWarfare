#include "stdafx.h"
#include "resource.h"
#include "PoseidonEdMaps.h"
#include <El/ParamFile/paramFile.hpp>

IMPLEMENT_DYNAMIC(CTextureLayer, CObject)

CTextureLayer::CTextureLayer(CPoseidonMap& map)
: m_map(map)
, m_nTexSizeInSqLog(0)
, m_nTexSizeInSq(0)
, m_nTexWidth(0)
, m_nTexHeight(0)
, m_pBaseTextures(NULL)
, m_pLandTextures(NULL)
, m_bActive(false)
, m_layered(false)
{
}

CTextureLayer::~CTextureLayer()
{
  DestroyLayer();
}

/*CTextureLayer::CTextureLayer(const CTextureLayer& src): m_map(src.m_map), m_bActive(false)
{
  if (OnNewLayer(src.m_map.m_nWidth, src.m_map.m_nWidth, src.m_nTexSizeInSqLog, src.m_Name))
  {
    int nTexDim = m_nTexWidth * m_nTexHeight;
    for(int i=0; i<nTexDim; i++)
    {
      m_pBaseTextures[i] = src.m_pBaseTextures[i];
      m_pLandTextures[i] = src.m_pLandTextures[i];
    }
  }
}*/

void CTextureLayer::DestroyLayer()
{
  if (m_pBaseTextures == NULL && m_pLandTextures == NULL)
    return;

  int nTexDim = m_nTexWidth * m_nTexHeight;
  if (m_pBaseTextures)
    for(int i = 0; i < nTexDim; i++)
    {
      Ref<CPosTextureTemplate> tex = m_pBaseTextures[i];
      if (tex.NotNull())
      {
        tex->m_nTimesUsedInBaseGlobal--;
        if (m_bActive)
          tex->m_nTimesUsedInBaseActive--;
      }
    }

  if (m_pLandTextures)
    for(int i = 0; i < nTexDim; i++)
    {
      Ref<CPosTextureTemplate> tex2 = m_pLandTextures[i];
      if (tex2.NotNull())
      {
        tex2->m_nTimesUsedInLandGlobal--;
        if (m_bActive)
          tex2->m_nTimesUsedInLandActive--;
      }
    }

  delete [] m_pBaseTextures;
  m_pBaseTextures = NULL;
  delete [] m_pLandTextures;
  m_pLandTextures = NULL;
  m_locked.Clear();
}

void CTextureLayer::ResizeLayer(int newWidth, int newHeight)
{
  float diffx=(float)newWidth/(float)(m_nTexWidth*m_nTexSizeInSq);
  float diffy=(float)newHeight/(float)(m_nTexHeight*m_nTexSizeInSq);
  while (diffx>=2)
  {
    m_nTexSizeInSqLog++;
    m_nTexSizeInSq<<=1;
    diffx/=2;
  }
  while (diffx<=0.5)
  {
    m_nTexSizeInSqLog--;
    m_nTexSizeInSq>>=1;
    diffx*=2;
  }
}

BOOL CTextureLayer::OnNewLayer(int nWidth, int nHeight, 
                               int nTexSizeInSqLog, const CString& name)
{
  TRY
  {
    m_Name = name;
    m_nTexSizeInSqLog = nTexSizeInSqLog;
    m_nTexSizeInSq = 1 << m_nTexSizeInSqLog;
    m_nTexWidth = nWidth/m_nTexSizeInSq;
    m_nTexHeight = nHeight/m_nTexSizeInSq;   

    // pole pro prim�rn� textury
    int nTexDim = m_nTexWidth * m_nTexHeight;
    m_pBaseTextures = new PTRTEXTURETMPLT[nTexDim];

    // pole pro sekund�rn� textury
    m_pLandTextures = new PTRTEXTURETMPLT[nTexDim];

    // pole pro zamky
    m_locked.Alloc(nTexDim);
  }
  CATCH_ALL(e)
  {
    MntReportMemoryException();
    // likvidace nepoveden� mapy
    DestroyLayer();
    return FALSE;
  }
  END_CATCH_ALL
    // inicializace hodnot
  OnInitLayer();
  return TRUE;
}

void CTextureLayer::OnInitLayer()
{
  if (m_pLandTextures && m_pBaseTextures)
  {

    int nTexDim = m_nTexWidth * m_nTexHeight;
    memset(m_pBaseTextures, 0x0, nTexDim * sizeof(*m_pBaseTextures));
    memset(m_pLandTextures, 0x0, nTexDim * sizeof(*m_pLandTextures));
    m_locked.Neutralize();
  }
}

PTRTEXTURETMPLT	CTextureLayer::GetPrimTextureAtEx(const UNITPOS& Pos) const
{
  PTRTEXTURETMPLT	pBase = GetBaseTextureAt(Pos);
  if (pBase == NULL || pBase->IsAllowedSecondary())
  {
    UNITPOS Pos2 = Pos;
    Pos2.x--;
    PTRTEXTURETMPLT pBase2 = GetBaseTextureAt(Pos2);
    if (pBase2 != NULL && !pBase2->IsAllowedSecondary())
    {
      pBase = pBase2;
    }
    else
    {      
      Pos2.z--;
      pBase2 = GetBaseTextureAt(Pos2);
      if (pBase2 != NULL && !pBase2->IsAllowedSecondary())
      {
        pBase = pBase2;
      }
      else
      {
        Pos2.x++;
        pBase2 = GetBaseTextureAt(Pos2);
        if (pBase2 != NULL && !pBase2->IsAllowedSecondary())
        {
          pBase = pBase2;
        }
      }
    }
  }

  if (pBase != NULL)
    return pBase->GetPrimarTexture();
  
  return NULL;
};

PTRTEXTURETMPLT	CTextureLayer::GetPrimTextureAt(const UNITPOS& Pos) const
{
  PTRTEXTURETMPLT	pBase = GetBaseTextureAt(Pos);
  if (pBase != NULL)
    return pBase->GetPrimarTexture();

  return NULL;
};

PTRTEXTURETMPLT	CTextureLayer::GetBaseTextureAt(const UNITPOS& Pos) const
{
  if (!IsAtLayerWorld(Pos) || !m_pBaseTextures)
    return NULL;	// nen� na map� sv�ta
  // vrac�m p��slu�nou texturu
  ASSERT(m_pBaseTextures!=NULL);
  return m_pBaseTextures[Pos.x+Pos.z*m_nTexWidth];
};

PTRTEXTURETMPLT	CTextureLayer::GetBaseTextureAtTerrainGrid(const UNITPOS& Pos) const
{
  if (!m_map.IsAtWorld(Pos) || !m_pBaseTextures)
    return NULL;	// nen� na map� sv�ta
  // vrac�m p��slu�nou texturu
  ASSERT(m_pBaseTextures!=NULL);
  return m_pBaseTextures[Pos.x/m_nTexSizeInSq+Pos.z/m_nTexSizeInSq*m_nTexWidth];
};

void CTextureLayer::SetBaseTextureAt(const UNITPOS& Pos,PTRTEXTURETMPLT pTxtr)
{
  if ((m_pBaseTextures==NULL)||(!IsAtLayerWorld(Pos)))
    return;	// nen� na map� sv�ta

  if (Locked(Pos))
    return;

  ASSERT(m_pBaseTextures!=NULL);
  PTRTEXTURETMPLT& oldTex = m_pBaseTextures[Pos.x+Pos.z*m_nTexWidth];
  if (oldTex == pTxtr)
    return;	// ��dn� zm�na
  if (oldTex.NotNull())
  {
    oldTex->m_nTimesUsedInBaseGlobal--;
    if (m_bActive)
      oldTex->m_nTimesUsedInBaseActive--;
  }

  if (pTxtr.NotNull())
  {
    pTxtr->m_nTimesUsedInBaseGlobal++;
    if (m_bActive)
      pTxtr->m_nTimesUsedInBaseActive++;
  }

  oldTex = pTxtr;
};

PTRTEXTURETMPLT	CTextureLayer::GetLandTextureAt(const UNITPOS& Pos) const
{
  if (!IsAtLayerWorld(Pos) || !m_pLandTextures)
    return NULL;	// nen� na map� sv�ta
  // vrac�m p��slu�nou texturu
  ASSERT(m_pLandTextures!=NULL);
  return m_pLandTextures[Pos.x+Pos.z*m_nTexWidth];
};

PTRTEXTURETMPLT	CTextureLayer::GetLandTextureAtTerrainGrid(const UNITPOS& Pos) const
{
  if (!m_map.IsAtWorld(Pos) || !m_pLandTextures)
    return NULL;	// nen� na map� sv�ta
  // vrac�m p��slu�nou texturu

  ASSERT(m_pLandTextures!=NULL);
  return m_pLandTextures[Pos.x/m_nTexSizeInSq+Pos.z/m_nTexSizeInSq*m_nTexWidth];
};

static RString AbsoluteFilePath(CPosEdBaseDoc *doc, const char *relative)
{
  CString projDir;
  doc->GetProjectDirectory(projDir, CPosEdBaseDoc::dirText, FALSE);
  return RString(projDir) + relative;
}

static RString ProjectRelativeFilePath(CPosEdBaseDoc *doc, const char *relative)
{
  CString projDir;
  doc->GetProjectDirectory(projDir, CPosEdBaseDoc::dirText, TRUE);
  return RString(projDir) + relative;
}


void CTextureLayer::RegisterBimpas(int index)
{
  m_explicitBimpasRegister[index].useCount++;
}
void CTextureLayer::UnregisterBimpas(int index)
{
  BimpasUsed &slot = m_explicitBimpasRegister[index];
  if (--slot.useCount==0)
  {
    // we need to remove the bimpas from the slot
    slot.name = RString();
    // make sure free slots get freed as soon as possible
    int resize = m_explicitBimpasRegister.Size();
    while (index>=0 && index==resize-1)
    {
      resize = index;
      --index;
      if (index<0 || m_explicitBimpasRegister[index].name.GetLength()!=0)
      {
        // if not empty we cannot free any more
        break;
      }
    }
    m_explicitBimpasRegister.Resize(resize);
  }
}

void CTextureLayer::ClearExplicitBimaps()
{
  Assert(m_layered);
  // SetExplicitBimpasAt
  for (int z=0; z<m_nTexHeight; z++) for (int x=0; x<m_nTexWidth; x++)
  {
    UNITPOS pos = {x,z};
    SetExplicitBimpasAt(pos,RString(),0,false);
  }
  // we may delete any trailing zero slots now
  while (m_explicitBimpasRegister.Size()>1)
  {
    BimpasUsed &used = m_explicitBimpasRegister[m_explicitBimpasRegister.Size()-1];
    if (used.name.GetLength()>0) break;
    m_explicitBimpasRegister.Resize(m_explicitBimpasRegister.Size()-1);
  }
}

void CTextureLayer::SwitchToExplicitBimaps()
{
  if (m_layered)
  {
    Assert(!m_pBaseTextures);
    Assert(!m_pLandTextures);
    return;
  }
  
  // destroy non-explicit information
  DestroyLayer();
  
  m_layered = true;
  
  // allocated explicit storage
  m_explicitBimpas.Dim(m_nTexWidth,m_nTexHeight);
  
  for (int x=0; x<m_nTexWidth; x++) for (int y=0; y<m_nTexWidth; y++)
  {
    m_explicitBimpas(x,y) = 0;
  }
  
  m_explicitBimpasRegister.Resize(1);
  // 1st slot is reserved as as a sea texture
  // this is some kind of historical relic
  // Cf. Landscape::SetTexture in the game engine
  m_explicitBimpasRegister[0].name = "--sea--";
  m_explicitBimpasRegister[0].major = 0;
  m_explicitBimpasRegister[0].useCount = 1;
  m_explicitBimpasRegister.Append();
}


void CTextureLayer::SetExplicitBimpasAt(const UNITPOS& pos, RString name, int major, bool fromBuldozer)
{
  int &slot = m_explicitBimpas(pos.x,pos.z);
  
  BimpasUsed used;
  used.name = name;
  used.major = major;
  int index = name.GetLength()>0 ? m_explicitBimpasRegister.FindKey(used) : 0;
  if (index==slot)
  {
    // no change - do nothing
    return;
  }
  
  // we need to keep the register of bimpases up to date
  // unregister the only one
  if (index>0)
  {
    RegisterBimpas(index);
  }
  else if (name.GetLength()>0)
  {
    used.useCount = 1;
    // first recycle empty names
    int free = m_explicitBimpasRegister.FindKey(BimpasUsed());
    if (free<0)
    {
      index = m_explicitBimpasRegister.Add(used);
    }
    else
    {
      m_explicitBimpasRegister[free] = used;
      index = free;
    }
    if (m_bActive && !fromBuldozer && m_map.GetDocument()->RealViewer())
    {
      RString projName = ProjectRelativeFilePath(m_map.GetDocument(),name);
      m_map.GetDocument()->RealViewer()->RegisterLandscapeTexture(SMoveObjectPosMessData(index),projName);
    }
  }
  if (slot>0)
  {
    UnregisterBimpas(slot);
  }
  
  slot = index;
  

  if (m_bActive && !fromBuldozer && m_map.GetDocument()->RealViewer())
  {
    m_map.GetDocument()->RealViewer()->LandTextureChange(STexturePosMessData(pos.x,pos.z,0,index));
  }
}



void CTextureLayer::SetLandTextureAt(const UNITPOS& Pos,PTRTEXTURETMPLT pTxtr,BOOL bFromBuldozer)
{
  if ((m_pLandTextures==NULL)||(!IsAtLayerWorld(Pos)))
    return;	// nen� na map� sv�ta

  PTRTEXTURETMPLT& oldTex = m_pLandTextures[Pos.x+Pos.z*m_nTexWidth];
  if (oldTex == pTxtr)
    return;	// ��dn� zm�na

  if (oldTex.NotNull())
  {
    oldTex->m_nTimesUsedInLandGlobal--;
    if (m_bActive)
      oldTex->m_nTimesUsedInLandActive--;
  }

  oldTex = pTxtr;
  if (pTxtr.NotNull())
  {
    pTxtr->m_nTimesUsedInLandGlobal++;
    if (m_bActive)
    {
      pTxtr->m_nTimesUsedInLandActive++;

      // nen�-li z buldozera->provedu update
      if (!bFromBuldozer && pTxtr->m_nTimesUsedInLandActive == 1 && pTxtr->m_nTxtrType != CPosTextureTemplate::txtrTypeSecundar)
      {
        // zaregistruj texturu do buldozeru
        CString fileName; 
        pTxtr->GetBuldozerTextureFileName(fileName);

        if (m_map.GetDocument()->RealViewer()) m_map.GetDocument()->RealViewer()->RegisterLandscapeTexture(SMoveObjectPosMessData(pTxtr->m_nTextureID),fileName);
      }
    }
  }

  if (m_bActive && !bFromBuldozer && m_map.GetDocument()->RealViewer())
  {	// po�lu zm�nu textury pro buldozer        
    if (m_map.GetDocument()->RealViewer()) m_map.GetDocument()->RealViewer()->LandTextureChange(STexturePosMessData(Pos.x,Pos.z,0,(pTxtr!=NULL)?(pTxtr->m_nTextureID):TEXTURE_SEA_ID));
  }
};

bool CTextureLayer::Locked(const UNITPOS& pos) const
{
  int p = pos.x+pos.z*m_nTexWidth;
  return m_locked[p];
}

void CTextureLayer::Lock(const UNITPOS& pos, bool lock)
{
  int p = pos.x+pos.z*m_nTexWidth;
  m_locked[p] = lock;  
}

BOOL CTextureLayer::IsAtLayerWorld(const UNITPOS& Pos) const
{
  if ((Pos.x <0)||(Pos.z <0)) return FALSE;
  if ((Pos.x >=m_nTexWidth)||(Pos.z >=m_nTexHeight)) return FALSE;
  return TRUE;
};


BOOL CTextureLayer::IsTextureUnitCenterAtArea(UNITPOS nUnit,const CPositionArea* pArea) const
{
  if (!IsAtLayerWorld(nUnit))
    return FALSE;

  //float   nUnitSz2= GetTextureRealSize()/2.f;
  POINT nLog = FromTextureUnitToViewPos(nUnit);
  nLog.x += VIEW_LOG_UNIT_SIZE/2;	// um�st�m na st�ed
  nLog.y -= VIEW_LOG_UNIT_SIZE/2;    
  return  pArea->IsPointAtArea(nLog);
};

void CTextureLayer::GetTextureUnitPosHull(UNITPOS& LftTop,UNITPOS& RghBtm,const CRect& rArea) const
{
  m_map.GetUnitPosHull(LftTop, RghBtm, rArea);

  LftTop.x /= m_nTexSizeInSq;
  LftTop.z /= m_nTexSizeInSq;
  RghBtm.x /= m_nTexSizeInSq;
  RghBtm.z /= m_nTexSizeInSq;
};

REALPOS	CTextureLayer::FromTextureUnitToRealPos(const UNITPOS& PosFrom) const
{
  //ASSERT(IsAtWorld(PosFrom));
  REALPOS PosTo;
  PosTo.x = (float)(PosFrom.x * GetTextureRealSize());
  PosTo.z = (float)(PosFrom.z * GetTextureRealSize());
  return PosTo;
};

POINT CTextureLayer::FromTextureUnitToViewPos(const UNITPOS& PosFrom) const
{
  POINT PosTo;
  PosTo.x = PosFrom.x * VIEW_LOG_UNIT_SIZE * m_nTexSizeInSq;
  PosTo.y = (m_nTexHeight - PosFrom.z ) * VIEW_LOG_UNIT_SIZE * m_nTexSizeInSq;
  return PosTo;
};

UNITPOS	CTextureLayer::FromRealToTextureUnitPos(const REALPOS& PosFrom) const
{
  //ASSERT(IsAtWorld(PosFrom));
  UNITPOS PosTo;
  double nInvTexureGrid = 1.f/((double)GetTextureRealSize());
  PosTo.x = (int)((double)(PosFrom.x * nInvTexureGrid));
  PosTo.z = (int)((double)(PosFrom.z * nInvTexureGrid));
  return PosTo;
};

UNITPOS CTextureLayer::FromTextureUnitToUnitPos(const UNITPOS& PosFrom) const
{
  UNITPOS pos;
  pos.x = PosFrom.x * m_nTexSizeInSq;
  pos.z = PosFrom.z * m_nTexSizeInSq;

  return pos;
}

UNITPOS CTextureLayer::FromUnitToTextureUnitPos(const UNITPOS& PosFrom) const
{
  UNITPOS pos;
  pos.x = PosFrom.x / m_nTexSizeInSq;
  pos.z = PosFrom.z / m_nTexSizeInSq;

  return pos;
}

void CTextureLayer::DoSerialize(CArchive& ar,int nVer)
{
  if (ar.IsLoading())
  {
    DestroyLayer();
    MntSerializeInt(ar,m_nTexSizeInSqLog);
    CString name;
    MntSerializeString(ar, name);
    if (!OnNewLayer(m_map.GetSize().x, m_map.GetSize().z, m_nTexSizeInSqLog, name))
      return;

    // check if explicit bimpases are used or not
    bool explicitBimpas = false;
    if (nVer>=55)
    {
      MntSerializeBool(ar,explicitBimpas);
    }
    
    OnInitLayer();

    if (!explicitBimpas)
    {
      TextureBank& textures = m_map.GetDocument()->m_Textures;

      UNITPOS pos;
      for(pos.z = 0; pos.z < m_nTexHeight; pos.z++)
        for(pos.x = 0; pos.x < m_nTexWidth; pos.x++)
        {
          int nTxtrID = 0;
          MntSerializeInt(ar, nTxtrID);            
          Ref<CPosTextureTemplate> tex = textures.FindEx(nTxtrID);
          SetBaseTextureAt(pos,tex);    
        };

      for(pos.z = 0; pos.z < m_nTexHeight; pos.z++)
        for(pos.x = 0; pos.x < m_nTexWidth; pos.x++)
        {
          int nTxtrID = 0;
          MntSerializeInt(ar, nTxtrID);            
          Ref<CPosTextureTemplate> tex = textures.FindEx(nTxtrID);
          SetLandTextureAt(pos,tex,TRUE);    
        };  
      if (nVer >= 46)
      {
        m_locked.DoSerialize(ar);      
      }
    }
    else
    {
      SwitchToExplicitBimaps();
      
      // load bimpas register

      int sizeRegister = 0;
      MntSerializeInt(ar,sizeRegister);
      m_explicitBimpasRegister.Resize(sizeRegister);
      
      for (int i=0; i<sizeRegister; i++)
      {
        MntSerializeInt(ar,m_explicitBimpasRegister[i].useCount);

        CString name;
        MntSerializeString(ar,name);
        m_explicitBimpasRegister[i].name = cc_cast(name);

        if (nVer>=56)
        {
          MntSerializeInt(ar,m_explicitBimpasRegister[i].major);
        }
        else
        {
          m_explicitBimpasRegister[i].major = 0;
        }
      }
      
      // load bimpas array
      
      for (int x=0; x<m_nTexHeight; x++) for (int z=0; z<m_nTexHeight; z++)
      {
        MntSerializeInt(ar,m_explicitBimpas(x,z));
      }
    }

  }
  else
  {
    MntSerializeInt(ar,m_nTexSizeInSqLog);
    MntSerializeString(ar,m_Name);
    
    bool explicitBimpas = CheckExplicitBimpas();
    MntSerializeBool(ar,explicitBimpas);
    
    if (!explicitBimpas)
    {
      UNITPOS pos;
      for(pos.z = 0; pos.z < m_nTexHeight; pos.z++)
        for(pos.x = 0; pos.x < m_nTexWidth; pos.x++)
        {        
          Ref<CPosTextureTemplate> tex = GetBaseTextureAt(pos);
          int nTxtrID = tex.NotNull()? tex->m_nTextureID : 0;
          MntSerializeInt(ar, nTxtrID);                      
        };

      for(pos.z = 0; pos.z < m_nTexHeight; pos.z++)
        for(pos.x = 0; pos.x < m_nTexWidth; pos.x++)
        {
          Ref<CPosTextureTemplate> tex = GetLandTextureAt(pos);
          int nTxtrID = tex.NotNull()? tex->m_nTextureID : 0;
          MntSerializeInt(ar, nTxtrID);        
        }; 

      m_locked.DoSerialize(ar);
    }
    else
    {
      int sizeRegister = m_explicitBimpasRegister.Size();
      MntSerializeInt(ar,sizeRegister);
      
      for (int i=0; i<sizeRegister; i++)
      {
        MntSerializeInt(ar,m_explicitBimpasRegister[i].useCount);
        
        CString name = m_explicitBimpasRegister[i].name;
        MntSerializeString(ar,name);
        MntSerializeInt(ar,m_explicitBimpasRegister[i].major);
      }
      
      for (int x=0; x<m_nTexHeight; x++) for (int z=0; z<m_nTexHeight; z++)
      {
        MntSerializeInt(ar,m_explicitBimpas(x,z));
      }
    }
    
    
  }  
}

float CTextureLayer::GetTextureRealSize() const 
{
  return m_map.GetDocument()->m_nRealSizeUnit * m_nTexSizeInSq;
}; 

BOOL CTextureLayer::ChangeTextureLog(int nTexLog)
{
  if (m_nTexSizeInSqLog == nTexLog)
    return TRUE; 
    
  int newTexWidth = m_map.GetSize().x / (1 << nTexLog);
  int newTexHeight = m_map.GetSize().z / (1 << nTexLog);
  
  if (m_pBaseTextures==NULL || m_pLandTextures==NULL)
  {
    // we cannot resize explicit information
    // unregister everything - new import will be needed
    ClearExplicitBimaps();

    m_nTexWidth = newTexWidth;
    m_nTexHeight = newTexHeight;
    
    m_nTexSizeInSqLog = nTexLog;
    m_nTexSizeInSq = 1 << m_nTexSizeInSqLog;
    
    m_explicitBimpas.Dim(m_nTexWidth,m_nTexHeight);

    // set everything to zero
    for (int x=0; x<m_nTexWidth; x++) for (int y=0; y<m_nTexWidth; y++)
    {
      m_explicitBimpas(x,y) = 0;
    }
    
    return TRUE;
  }

  // alocate new memory for textures

  PTRTEXTURETMPLT * pBaseTex = NULL; 
  PTRTEXTURETMPLT * pLandTex = NULL;
  TRY
  {    
    // pole pro prim�rn� textury
    int nTexDim = newTexWidth * newTexHeight;
    pBaseTex = new PTRTEXTURETMPLT[nTexDim];    
    // pole pro sekund�rn� textury
    pLandTex = new PTRTEXTURETMPLT[nTexDim];
  
  }
  CATCH_ALL(e)
  {
    MntReportMemoryException();    

    delete [] pBaseTex;
    delete [] pLandTex;
    return FALSE;
  }
  END_CATCH_ALL;

  // Recalculate base textures
  if (m_nTexSizeInSqLog > nTexLog)
  {  
    int dif = 1 << (m_nTexSizeInSqLog - nTexLog);
    for(int z = 0; z < newTexHeight; z++)
      for(int x = 0; x < newTexWidth; x++)
      {        
        pBaseTex[x + z * newTexWidth] = m_pBaseTextures[x / dif + z / dif * m_nTexWidth ];
        Ref<CPosTextureTemplate> baseTex = pBaseTex[x + z * newTexWidth];

        if (baseTex.NotNull())
        {
          baseTex->m_nTimesUsedInBaseGlobal++;
          if (m_bActive)
            baseTex->m_nTimesUsedInBaseActive++;
        }
      }
  }
  else
  {  
    int dif = 1 << (nTexLog - m_nTexSizeInSqLog);
    for(int z = 0; z < newTexHeight; z++)
      for(int x = 0; x < newTexWidth; x++)
      {
        pBaseTex[x + z * newTexWidth] = m_pBaseTextures[x * dif + z * dif * m_nTexWidth ];
        Ref<CPosTextureTemplate> baseTex = pBaseTex[x + z * newTexWidth];
        if (baseTex.NotNull())
        {
          baseTex->m_nTimesUsedInBaseGlobal++;
          if (m_bActive)
            baseTex->m_nTimesUsedInBaseActive++;
        }
      }
  }

  //release old textures 
  DestroyLayer();

  m_pBaseTextures = pBaseTex;
  m_pLandTextures = pLandTex;

  m_nTexWidth = newTexWidth;
  m_nTexHeight = newTexHeight;

  m_locked.Alloc(m_nTexWidth*m_nTexHeight );

  m_nTexSizeInSqLog = nTexLog;
  m_nTexSizeInSq = 1 << m_nTexSizeInSqLog;

    // recalculate land textures
  UNITPOS pos;
  for(pos.x = 0; pos.x < m_nTexWidth; pos.x++)
    for(pos.z = 0; pos.z < m_nTexHeight; pos.z++)    
      DoUpdateSecndTexturesAt(pos, FALSE);

  return TRUE;
}

void CTextureLayer::Activate()
{
  if (m_pLandTextures && m_pBaseTextures && !m_bActive)
  {
    int nTexDim = m_nTexWidth * m_nTexHeight;
    for(int i=0; i<nTexDim; i++)
    {
      if (m_pBaseTextures[i].NotNull())
        m_pBaseTextures[i]->m_nTimesUsedInBaseActive++;

      if (m_pLandTextures[i].NotNull())
        m_pLandTextures[i]->m_nTimesUsedInLandActive++;
    }
  }

  m_bActive = true;
}

void CTextureLayer::Deactivate()
{
  if (m_pLandTextures && m_pBaseTextures && m_bActive)
  {
    int nTexDim = m_nTexWidth * m_nTexHeight;
    for(int i=0; i<nTexDim; i++)
    {
      if (m_pBaseTextures[i].NotNull())
        m_pBaseTextures[i]->m_nTimesUsedInBaseActive--;

      if (m_pLandTextures[i].NotNull())
        m_pLandTextures[i]->m_nTimesUsedInLandActive--;
    }
  }

  m_bActive = false;
}

// provede update/generov�n� sekund�rn� textury pro pole UNITPOS Pos
void CTextureLayer::DoUpdateSecndTexturesAt(const UNITPOS &Pos, BOOL bRegenerate)
{
  //		pUU		pZZ
  //		pXX		pYY
  PTRTEXTURETMPLT pXX = NULL;
  PTRTEXTURETMPLT pYY = NULL;
  PTRTEXTURETMPLT pUU = NULL;
  PTRTEXTURETMPLT pZZ = NULL;
  PTRTEXTURETMPLT pAA = NULL;
  // prim�rn� textury v dan�ch bodech
  UNITPOS ptAt;
  ptAt = Pos;
  pAA  = GetBaseTextureAt(ptAt);
 
  pXX  = GetPrimTextureAtEx(ptAt);	

  ptAt = Pos; ptAt.x++;
  pYY  = GetPrimTextureAtEx(ptAt);	

  ptAt = Pos; ptAt.z++;
  pUU  = GetPrimTextureAtEx(ptAt);

  ptAt = Pos; ptAt.x++; ptAt.z++;
  pZZ  = GetPrimTextureAtEx(ptAt);

 
  // zn�m v�echny prim�rn� textury
  // jak� bude sekund�rn� ?

  // jsou-li v�echny stejn� -> pou�iji pXX or if on this zone is not allowed to do secondary texture
  if ((pAA && !pAA->m_pLandZone->IsAllowedSecondary()) || (pXX == pYY && pYY == pUU && pUU == pZZ))
  {
    // v�echny prim�rn� stejn� -> pou�iji variantn�
    if (pAA && !pAA->m_pLandZone->IsAllowedSecondary())
      SetLandTextureAt(Pos, pAA, FALSE);    
    else
      if (pAA && pAA->GetPrimarTexture() != pXX)
        SetLandTextureAt(Pos, pXX, FALSE);   
      else
        SetLandTextureAt(Pos, pAA, FALSE);    


    if (bRegenerate && pAA && pAA->m_nTxtCounter == 0)
    {

      pAA->m_nTxtCounter = 1;

      // recreate bimpas file for primary texture
      CString fullPath;
      pAA->GetFullBimPasFileName(fullPath);
      CPoseidonMap::CreateSquareSurfaceBimPasFile(fullPath, pAA);
    }
    return;
  }
  // je to slo�en� textura
  CString sBimPasFileName;
  m_map.CreateTransitionSquareSurfaceBimPasFileName(sBimPasFileName,pXX,pYY,pUU,pZZ);
  if (sBimPasFileName.GetLength()== 0)
  {
    // chyba -> pou�iji p�vodn� jako sekund�rn�
    SetLandTextureAt(Pos, pAA, FALSE);
    return;
  };

  if (!bRegenerate)
  {	// nevy�aduji regenerov�n�

    // nen� zm�n�na 
    PTRTEXTURETMPLT	pOldScnd = GetLandTextureAt(Pos);
    if (pOldScnd.NotNull())
    {	
      if (lstrcmp(pOldScnd->m_sTextureFile,sBimPasFileName)==0)
        return; // textura je nezm�n�na
    }
    // budu pot�ebovat novou se jm�nem sScndName
    PTRTEXTURETMPLT	pNewScnd = m_map.GetDocument()->m_Textures.FindFileEx(sBimPasFileName);
    if (pNewScnd.NotNull())
    {	// ji� existuje a je registrov�na -> vlo��m ji
      SetLandTextureAt(Pos, pNewScnd, FALSE);      
      return;
    }
  }
  else
  {
    PTRTEXTURETMPLT	pNewScnd = m_map.GetDocument()->m_Textures.FindFileEx(sBimPasFileName);
    if (pNewScnd.NotNull())
    {
      // texture exist put her into map and check if file exist
      SetLandTextureAt(Pos, pNewScnd, FALSE);      
      if (pNewScnd->m_nTxtCounter == 0)
      { 
        CString realPath;
        pNewScnd->GetFullBimPasFileName(realPath);          
        m_map.CreateTransitionSquareSurfaceBimPasFile(realPath,pXX,pYY,pUU,pZZ);

        if (!MntExistFile(realPath))          
          // nepovedlo se nic vytvo�it ->pou�iji pAA
          SetLandTextureAt(Pos, pAA, FALSE);
        else
          pNewScnd->m_nTxtCounter = 1;
        return;
      }
      return;
    }
  }

  // sekund�rn� neexistuje -> vytvo��m ji
  PTRTEXTURETMPLT	pGenScnd = m_map.CreateSecundTextureFrom(pXX,pYY,pUU,pZZ,bRegenerate);
  if (pGenScnd.NotNull())
  {	// vytvo�il jsem �sp�n� novou
    m_map.GetDocument()->m_Textures.AddID(pGenScnd);
    SetLandTextureAt(Pos, pGenScnd, FALSE);
    if (bRegenerate)
      pGenScnd->m_nTxtCounter = 1;

    return;
  }

  // nepovedlo se nic vytvo�it ->pou�iji pAA
  SetLandTextureAt(Pos, pAA, FALSE);
  return;
};

static void AddVector(ParamEntry &entry, Vector3Par vector)
{
  entry.AddValue(vector.X());
  entry.AddValue(vector.Y());
  entry.AddValue(vector.Z());
}

static void AddFloats(ParamClass &cls, const char *name, const float *val, int nVal)
{
  ParamEntryPtr entry = cls.AddArray(name);
  for (int i=0; i<nVal; i++)
  {
    entry->AddValue(val[i]);
  }
}


static void AddStage(ParamClass &cfg,
  const char *stageName, const char *texture, int texGen
)
{
  ParamClassPtr no = cfg.AddClass(stageName);
  no->Add("texture",texture);
  no->Add("texGen",texGen);
}

static void AddTexGen(ParamClass &cfg,
  const char *stageName, const char *uvSource, Matrix4Par texgen
)
{
  ParamClassPtr no = cfg.AddClass(stageName);
  no->Add("uvSource",uvSource);
  ParamClassPtr texGen = no->AddClass("uvTransform");
  AddVector(*texGen->AddArray("aside"),texgen.DirectionAside());
  AddVector(*texGen->AddArray("up"),texgen.DirectionUp());
  AddVector(*texGen->AddArray("dir"),texgen.Direction());
  AddVector(*texGen->AddArray("pos"),texgen.Position());
}

/**
@param pos given in main document grid
@param satTexU used part of the sat. texture
@param satTexV used part of the sat. texture
@param satMaskU used part of the mask texture
@param satMaskV used part of the mask texture
@param saveAsBinary if true the rvmat files will be saved in binary form, otherwise in text form
@param detailTexturesList contains the list of textures filename to be saved in the "texture.lst"
  when the rvmat are saved in binary format
*/
void CTextureLayer::DoUpdateSatAndMaskAt(const UNITPOS& pos, int nLayers, 
                     const int* layers, int major,
                     int segX, int segZ,
                     float satOffU, float satOffV, 
                     float satTexU, float satTexV,
                     float maskOffU, float maskOffV, 
                     float maskTexU, float maskTexV,
                     bool saveAsBinary, 
                     StringList& detailTexturesList)
{
  // compose rvmats from satellite and mask
  // each rvmat contains:
  // * reference to sat and mask (stages 3 and 4)
  // * stage id in specular (1,0,0,0) ... (0,0,0,1)
  // * detail map + normal map (stages 1 and 2)
  Assert(nLayers > 0);
  #if ARMA2_LEVEL
  Assert(nLayers <= 6);
  #else
  Assert(nLayers <= 4);
  #endif
  // sat + layer mask tex-gen is calculated in world space here
  // we need to convert from texture segment space to world space
  // we know texture 
  // bimpas should reference all rvmats
  // texgen stages:
  // 0 - primary
  // 1 - normal map
  // 2 - detail texture
  // 3 - satellite map
  // 4 - mask

  // texture stages:
  // 0 - sat. map
  // 1 - layer mask
  // 2+3*i - primary for stage i
  // 3+3*i - normal map for stage i
  // 4+3*i - detail texture for stage i
  
  static const char folderPattern[] = "Layers\\";
  // bimpas/rvmat name: sat. segment, 
  static const char bimpasPattern[] = "Layers\\P_%03d-%03d";

  CPosEdBaseDoc* doc = m_map.GetDocument();
  float unit = doc->m_nRealSizeUnit;

  float satGrid = doc->m_satelliteGridG * unit;

  RString absFolderName = AbsoluteFilePath(m_map.GetDocument(), folderPattern);
  CreateDirectory(absFolderName, NULL);

  detailTexturesList.Filename(absFolderName + "textures.lst");

  int rangeZGrid = m_nTexHeight * m_nTexSizeInSq;
  float rangeZ = m_nTexHeight * m_nTexSizeInSq * unit;
  
  
  int satGridX = segX;
  int satGridZ = segZ;
  
//  int satGridX = pos.x/doc->m_satelliteGridG;
//  int satGridZ = (rangeZGrid-1-pos.z)/doc->m_satelliteGridG;
  int maskGridX = satGridX;
  int maskGridZ = satGridZ;
  
  // 1 segment of satellite map has two dimension - total size and used size
  // we map the used size, the rest is only because of filtering  

  // calculate world space pos. of the segment
  float xBeg = satGridX * satGrid, xEnd = xBeg + satGrid;
  float zBeg = rangeZ - satGridZ * satGrid, zEnd = zBeg - satGrid;

  // tCA, tCB found as a solution of equations
  //   beg*tCA + tCB = tOffC (mapping for c=beg)
  //   end*tCA + tCB = tOffC+tTexC (mapping for c=end)
  //
  //   tCA = tTexC/(end-beg)
  //   tCB = xOffC - xBeg*tCA


  // endX-begX = satGrid
  // endZ-begZ = -satGrid


  float invSatGrid = 1 / satGrid;
  float satUA = satTexU * invSatGrid;
  float satUB = satOffU - xBeg * satUA;
  
  float maskUA = maskTexU * invSatGrid;
  float maskUB = maskOffU - xBeg * maskUA;
  
  float satVA = -satTexV * invSatGrid;
  float satVB = satOffV - zBeg * satVA;
  
  float maskVA = -maskTexV * invSatGrid;
  float maskVB = maskOffV - zBeg * maskVA;


  // convert from world space to U/V  
  Matrix4 satGen(satUA,                       0,     0,  satUB,
             0,                       0, satVA,  satVB,
             0, sqrt(fabs(satUA*satVA)),     0,      0);
  Matrix4 maskGen(maskUA,                      0,      0,   maskUB,
             0,                      0, maskVA,   maskVB,
             0, sqrt(fabs(maskUA*maskVA)),   0,        0);
  
  RString bimpasBase = Format(bimpasPattern,
                satGridX,satGridZ, // sat
                maskGridX,maskGridZ // mask
                );

  RString bimpasName = bimpasBase;
  int layerBitmask = 0;
  int majorOrder = -1;
  int pass = 0;
  for (int i = 0; i < nLayers; i++)
  {
    int index = layers[i];
    if (index >= 0)
    {
      layerBitmask |= 1<<i;
      bimpasName = bimpasName + Format("_L%02d",index);
      if (index == major) majorOrder = pass;
      pass++;
    }
    else
    {
      bimpasName = bimpasName + "_N";
    }
  }
  if (majorOrder<0)
  {
    LogF("Major pass not found for %d,%d - major %d",satGridX,satGridZ,major);
    majorOrder = 0;
  }
  bimpasName = bimpasName + ".rvmat";
  RString absBimpasName = AbsoluteFilePath(m_map.GetDocument(), bimpasName);

  BString<1024> satName, maskName; 
  sprintf(satName, m_satellitePattern, satGridX, satGridZ);
  sprintf(maskName, m_layerMaskPattern, maskGridX, maskGridZ);
  RString absSatName = ProjectRelativeFilePath(m_map.GetDocument(), satName);
  RString absMaskName = ProjectRelativeFilePath(m_map.GetDocument(), maskName);

  // if bimpas already exists, there is no need to recreate it unless source is changed
  QFileTime tgtTime = QIFileFunctions::TimeStamp(absBimpasName);
  QFileTime srcTime = m_layersTimeStamp;
  if (tgtTime < srcTime)
  {
    ParamFile material;
    
    // TODO: check if all values match
    float specular[4]={0, 0, 0, 0};
    
    
    AddFloats(material, "ambient", m_commonLayer.ambient, lenof(m_commonLayer.ambient));
    AddFloats(material, "diffuse", m_commonLayer.diffuse, lenof(m_commonLayer.diffuse));
    AddFloats(material, "forcedDiffuse", m_commonLayer.forcedDiffuse, lenof(m_commonLayer.forcedDiffuse));
    AddFloats(material, "emmisive", m_commonLayer.emissive, lenof(m_commonLayer.emissive));
    AddFloats(material, "specular", specular, lenof(specular));
    
    material.Add("specularPower", 0.0f);
    
    AddStage(material,"Stage0", absSatName, 3);
    AddStage(material,"Stage1", absMaskName, 4);
    AddTexGen(material, "TexGen3", "worldPos", satGen);
    AddTexGen(material, "TexGen4", "worldPos", maskGen);
    
    AddTexGen(material, "TexGen0", "tex", m_commonLayer.txTexgen);
    AddTexGen(material, "TexGen1", "tex", m_commonLayer.noTexgen);
    AddTexGen(material, "TexGen2", "tex", m_commonLayer.dtTexgen);
    
#   if ARMA2_LEVEL
    // ARMA2 style - TerrainX pixel shader, 2 stages per layer
    material.Add("PixelShaderID", "TerrainX");
    material.Add("VertexShaderID", "Terrain");
    // middle detail specified only once
    // game will override it anyway - we provide only a sensible fallback
    AddStage(material,"Stage2", "#(rgb,1,1,1)color(0.5,0.5,0.5,1,cdt)", 0);
    int pass = 0;
    for (int i = 0; i < nLayers; i++)
    {
      BString<128> stageN;
      BString<128> stageD;
      sprintf(stageN, "Stage%d", pass * 2 + 3);
      sprintf(stageD, "Stage%d", pass * 2 + 4);
      
      int index = layers[i];
      if (index >= 0)
      {
        const CMaskedLayer &layer = m_layers[index];
        
        AddStage(material, stageN, layer.no, 1);
        AddStage(material, stageD, layer.dt, 2);

        detailTexturesList.Add(layer.tx.Data());
        detailTexturesList.Add(layer.no.Data());
        detailTexturesList.Add(layer.dt.Data());

      }
      else
      {
        // when skipping empty layers, we need to insert an empty stage
        AddStage(material, stageN, "", 1);
        AddStage(material, stageD, "", 2);
      }
      pass++;
    }
#   else
    // ARMA style - Terrain1..Terrain15 pixel shaders
    BString<64> psName;
    sprintf(psName, "Terrain%d", layerBitmask);
    material.Add("PixelShaderID", cc_cast(psName));
    material.Add("VertexShaderID", "Terrain");
    int pass = 0;
    for (int i = 0; i < nLayers; i++)
    {
      int index = layers[i];
      if (index >= 0)
      {
        const CMaskedLayer &layer = m_layers[index];

        BString<128> stageP;
        BString<128> stageN;
        BString<128> stageD;
        sprintf(stageP, "Stage%d", pass * 3 + 2);
        sprintf(stageN, "Stage%d", pass * 3 + 3);
        sprintf(stageD, "Stage%d", pass * 3 + 4);

        AddStage(material, stageP, layer.tx, 0);
        AddStage(material, stageN, layer.no, 1);
        AddStage(material, stageD, layer.dt, 2);

        detailTexturesList.Add(layer.tx.Data());
        detailTexturesList.Add(layer.no.Data());
        detailTexturesList.Add(layer.dt.Data());

        pass++;
      }
    }
#   endif
    // major pass is encoded in the name

    if(!saveAsBinary)
    // saves as text file
    {
      LSError saveResult = material.Save(absBimpasName);
      if (saveResult != LSOK)
      {
        LogF("Error %d saving '%s'", saveResult, cc_cast(absBimpasName));
//				CString msg;
//				AfxFormatString1(msg,IDS_ERROR_SAVE,absBimpasName);
//				AfxMessageBox(msg);
      }
    }
    else
    // saves as binary file
    {
      bool saveResult = material.SaveBin(absBimpasName);
      if (!saveResult)
      {
        LogF("Error saving '%s'", cc_cast(absBimpasName));
//				CString msg;
//				AfxFormatString1(msg,IDS_ERROR_SAVE,absBimpasName);
//				AfxMessageBox(msg);
      }
    }

  }

  // set bimpas to the given location
  UNITPOS texpos;
  texpos.x = pos.x / m_nTexSizeInSq;
  texpos.z = pos.z / m_nTexSizeInSq;
//	LogF("SetAt %2d,%2d (%2d,%2d), %s",pos.x,pos.z,texpos.x,texpos.z,cc_cast(bimpasBase));
  SetExplicitBimpasAt(texpos, bimpasName, majorOrder, false);
}
