/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditObjectDlg dialog

class CEditObjectDlg : public CDialog
{
	// Construction
public:
	CEditObjectDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
	//{{AFX_DATA(CEditObjectDlg)
	enum { IDD = IDD_OBJECT_EDIT };	
	//}}AFX_DATA

	const CPoseidonMap*       m_pMap;
	const CPosObjectTemplate* m_pTemplate;

	CString	m_sName;
	CString m_sType;

	float m_nPosX;
	float m_nPosZ;
	float m_nScale;
	float m_nRelHght;

	int   m_nType;
	float m_nValue;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditObjectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEditObjectDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEditHeight();
	afx_msg void OnEnChangeEditPosx();
};

CEditObjectDlg::CEditObjectDlg(CWnd* pParent /*=NULL*/)
: CDialog(CEditObjectDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditObjectDlg)
	//}}AFX_DATA_INIT
	m_nType  = 0;
	m_nValue = 0.0;

	m_nPosX = 0.0;
	m_nPosZ = 0.0;
	m_nScale = 0;
	m_nRelHght = 0.0;
}

void CEditObjectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditObjectDlg)
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_EDIT_NAME, m_sName);
	MDDV_MaxChars(pDX, m_sName, MAX_OBJC_NAME_LEN, IDC_EDIT_NAME);

	if (!SAVEDATA) ::SetWindowText(DLGCTRL(IDC_SHOW_TYPE), m_sType);	

	// Position
	// poloha - maximum podle mapy
	FMntVector2 size = m_pMap->GetRealSize(); 

	MDDX_Text(pDX, IDC_EDIT_POSX, m_nPosX);
	MDDV_MinMaxDouble(pDX, m_nPosX, 0., size[0], IDC_EDIT_POSX, 1.);

	MDDX_Text(pDX, IDC_EDIT_POSZ, m_nPosZ);
	MDDV_MinMaxDouble(pDX, m_nPosZ, 0., size[1], IDC_EDIT_POSZ, 1.);

	// m���tko - maximum podle �ablony
	if ((m_pTemplate != NULL)/*&&(m_pTemplate->m_bRandomSize)*/)
	{
		MDDX_Text(pDX, IDC_EDIT_SCALE,  m_nScale);
		MDDV_MinMaxDouble(pDX, m_nScale, 1, 500, IDC_EDIT_SCALE, 1.);
	} 
	else
	{
		if (!SAVEDATA)
		{
			MDDX_Text(pDX, IDC_EDIT_SCALE, m_nScale);
			::EnableWindow(DLGCTRL(IDC_EDIT_SCALE), FALSE);
		}
	}

	// v��ka - maximum podle �ablony
	MDDX_Text(pDX, IDC_EDIT_HEIGHT, m_nRelHght);
	MDDV_MinMaxDouble(pDX, m_nRelHght, MIN_LAND_HEIGHT, MAX_LAND_HEIGHT, IDC_EDIT_HEIGHT, 1.);

	//Rotation
	DDX_Radio(pDX, IDC_RADIO_0, m_nType);
  
	if (SAVEDATA)
	{
		switch(m_nType)
		{
		case 1:
			m_nValue = 90.;
			break;
		case 2:
			m_nValue = 180.;
			break;
		case 3:
			m_nValue = 270.;
			break;
		default:
			{
				MDDX_Text(pDX, IDC_EDIT_VALUE, m_nValue);
				MDDV_MinMaxDouble(pDX, m_nValue, -360., 360., IDC_EDIT_VALUE, 1.);
				if (m_nValue < 0.) m_nValue += 360.;
			}
		}
	} 
	else
	{
		MDDX_Text(pDX, IDC_EDIT_VALUE, m_nValue);
		MDDV_MinMaxDouble(pDX, m_nValue, -360., 360., IDC_EDIT_VALUE, 1.);
	}
}

BEGIN_MESSAGE_MAP(CEditObjectDlg, CDialog)
	//{{AFX_MSG_MAP(CEditObjectDlg)
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(IDC_EDIT_HEIGHT, OnEnChangeEditHeight)
	ON_EN_CHANGE(IDC_EDIT_POSX, OnEnChangeEditPosx)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// editace objektu

BOOL CPosEdObject::DoEditObject()
{
	CEditObjectDlg dlg;
	dlg.m_sName = m_sObjName;  
	dlg.m_sType = GetBuldozerFileName();
	dlg.m_pMap = _map;

	if (m_pTemplate == NULL && m_nTemplateID != UNDEF_REG_ID)
	{
		dlg.m_pTemplate = GetDocument()->GetObjectTemplate(m_nTemplateID);
	}
	else
	{
		dlg.m_pTemplate = m_pTemplate;
	}

	REALPOS rPos	= GetRealPos();
	dlg.m_nPosX		= rPos.x;
	dlg.m_nPosZ		= rPos.z;
	dlg.m_nScale	= (float) Round(m_nSizeScale * 100., 3);
	dlg.m_nRelHght  = m_nRelHeight;

	float nOldScale = dlg.m_nScale;

	if (dlg.DoModal() != IDOK) return FALSE;

	if (lstrcmp(m_sObjName, dlg.m_sName) != 0) m_sObjName = dlg.m_sName;    

	CalcRotationPosition(-dlg.m_nValue);

	if (nOldScale != dlg.m_nScale)
	{
		m_nSizeScale = dlg.m_nScale / 100.0f;
		// reset polohy
		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 3; ++j)
			{
				m_Position(i, j) *= dlg.m_nScale / nOldScale;
			}
		}
	}

	m_Position(0, 3)= (float)dlg.m_nPosX;
	m_Position(2, 3)= (float)dlg.m_nPosZ;
	m_nRelHeight	= (float)dlg.m_nRelHght;

	// update v��ky
	CalcObjectPoseidonHeight();

	CalcObjectLogPosition();

	return TRUE;
}

void CEditObjectDlg::OnEnChangeEditHeight()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}

void CEditObjectDlg::OnEnChangeEditPosx()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}