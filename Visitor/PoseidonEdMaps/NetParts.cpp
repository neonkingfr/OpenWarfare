/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "math.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// tolerance (d�ly jsou pova�ov�ny za stejnou v��ku)
#define NET_PART_HEIGHT_TOLERANCE	0.001

/////////////////////////////////////////////////////////////////////////////
// CNetPartBase - z�kladn� ��st s�t�

IMPLEMENT_DYNAMIC(CNetPartBase, CObject)

CNetPartBase::CNetPartBase()
{
	m_nPosObjectID  = UNDEF_REG_ID;
	m_pCreateObject = NULL;
}

void CNetPartBase::MakeDefault()
{
	// z�kladn� poloha
	InitRealNetPos(m_nBaseRealPos);
	// v��ku lze m�nit automaticky
	//m_bAutoHeight	= TRUE;		
	// relativn� v��ka s�t� v bod� (A) nad povrchem
	//m_nRelaHeight	= 0.;		
	// nen� vytvo�en objekt
	m_nPosObjectID  = UNDEF_REG_ID;
	m_pCreateObject = NULL;
}

void CNetPartBase::Destroy()
{
}

// vytvo�en� kopie objektu
CNetPartBase* CNetPartBase::CreateCopyObject() const
{
	CObject* pObject = NULL;
	TRY
	{
		pObject = GetRuntimeClass()->CreateObject();
		if (pObject)
		{
			ASSERT_KINDOF(CNetPartBase, pObject);
			((CNetPartBase*)pObject)->CopyFrom(this);
		} 
		else
		{
			AfxThrowUserException();
		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pObject) delete pObject;
		pObject = NULL;
	}
	END_CATCH_ALL
	return ((CNetPartBase*)pObject);
}

CNetPartBase* CNetPartBase::CreateObjectType(int nType)
{
	CNetPartBase* pObj = NULL;
	switch(nType)
	{
	case netPartSTRA: pObj = new CNetPartSTRA(); break;
	case netPartBEND: pObj = new CNetPartBEND(); break;
	case netPartSPEC: pObj = new CNetPartSPEC(); break;
	case netPartKEY:  pObj = new CNetPartKEY();  break;
	};
	return pObj;
}

void CNetPartBase::CopyFrom(const CNetPartBase* pSrc)
{
	ASSERT_KINDOF(CNetPartBase, pSrc);
	// zru�en� dat
	Destroy();
	// kopie dat
	m_nBaseRealPos = pSrc->m_nBaseRealPos;
	// v��ku lze m�nit automaticky
	//m_bAutoHeight	= pSrc->m_bAutoHeight;		
	// relativn� v��ka s�t� v bod� (A) nad povrchem
	//m_nRelaHeight	= pSrc->m_nRelaHeight;		

	m_nPosObjectID  = UNDEF_REG_ID;
	m_pCreateObject = NULL;

	// identifikace objekt� Poseidon
	ASSERT((m_nPosObjectID == UNDEF_REG_ID) || (m_pCreateObject != NULL));
	m_nPosObjectID  = pSrc->m_nPosObjectID;
	m_pCreateObject = pSrc->m_pCreateObject;
}

// serializace
void CNetPartBase::DoSerialize(CArchive& ar, DWORD nVer)
{
	// poloha
	MntSerializeNetPos(ar, nVer, m_nBaseRealPos);
	// v��ka
	if (ar.IsLoading() && nVer < 42)
	{
		BOOL bAutoHeight;
		MntSerializeBOOL(ar, bAutoHeight);

		double m_nRelaHeight;
		MntSerializeDouble(ar, m_nRelaHeight);
	}

	// identifikace objekt� Poseidon
	MntSerializeInt(ar, m_nPosObjectID);
	ASSERT((m_nPosObjectID != UNDEF_REG_ID) && (m_pCreateObject == NULL));
}

// informace o poloze objektu
void CNetPartBase::RecalcDevObjectPositionFromLP(CDC* pDC)
{
	const CPositionBase* pLog = GetLogObjectPosition();
	const CPositionBase* pDev = GetDevObjectPosition();
	ASSERT((pLog != NULL) && (pDev != NULL));
	if ((pLog != NULL) && (pDev != NULL))
	{
		((CPositionBase*)pDev)->CopyFrom(pLog);
		((CPositionBase*)pDev)->LPtoDP(pDC);
	}
}

// pr�ce s objekty Poseidon (Update a Vytvo�en�)
BOOL CNetPartBase::DoUpdatePoseidonNetObjects(CObArray* ExistObjs, const CPoseidonMap* pMap)
{
	if (m_pCreateObject != NULL)
	{	
		// bude vytvo�en nov� - igmnoruji
		ASSERT(m_nPosObjectID == UNDEF_REG_ID);
		return TRUE;
	}

	ASSERT(m_nPosObjectID != UNDEF_REG_ID);
	
	for (int i = 0; i < ExistObjs->GetSize(); ++i)
	{
		CPosEdObject* pExist = ((CPosEdObject*)((*ExistObjs)[i]));
		ASSERT(pExist != NULL);
		if ((pExist) && (pExist->GetID() == m_nPosObjectID))
		{	// p��znak pou�it�
			ASSERT(pExist->m_nTempObjCounter == 0);
			pExist->m_nTempObjCounter++;
			// provedu update objektu
			return DoUpdatePoseidonNetObject(pExist, pMap);
		}
	}
	return FALSE;
}

BOOL CNetPartBase::DoUpdatePoseidonNetObject(CPosEdObject* pObject, const CPoseidonMap* pMap)
{
	ASSERT(m_pCreateObject == NULL);
	if (pObject == NULL) return FALSE;	// nen� objekt

	// nastav�m jednotkovou matici
	pObject->m_Position = M4Identity;	

	return TRUE;
}

BOOL CNetPartBase::DoCreatePoseidonNetObject(CObArray* CreateObjs, const CPoseidonMap* pMap)
{
	if (m_nPosObjectID != UNDEF_REG_ID) return TRUE;	// ji� je vytvo�en
	ASSERT(m_pCreateObject != NULL);
	if (m_pCreateObject == NULL) return FALSE;
	// vytvo��m nov� objekt
	CPosEdObject* pObj = NULL;
	{
		// poloha objektu
		REALPOS rPos = m_nBaseRealPos.nPos;
		// vytvo��m nov� objekt
		pObj = new CPosEdObject(*pMap);
	
		pObj->CreateFromTemplateAt(m_pCreateObject, rPos);
		// nen� ur�eno ID objektu !
		ASSERT(pObj->GetID() == UNDEF_REG_ID);
	}
	// existuje objekt -> p�i�ad�m ID, zru��m Create �ablonu a za�ad�m ho
	ASSERT(pObj != NULL);
	pObj->SetFreeID();	
	CreateObjs->Add(pObj);
	m_pCreateObject = NULL;
	m_nPosObjectID  = pObj->GetID();
	// provedu update objektu
	return DoUpdatePoseidonNetObject(pObj, pMap);
}

// pr�ce se sou�adnicemi objektu
BOOL CNetPartBase::IsPointAtArea(const POINT& pt) const
{	
	return GetLogObjectPosition()->IsPointAtArea(pt); 
}

bool CNetPartBase::IsIntersection(const CPosObject& sec) const
{	
	return GetLogObjectPosition()->IsIntersection(*sec.GetLogObjectPosition()); 
}

REALPOS CNetPartBase::CalcRealPosFromRelativePos(REALPOS rFrom) const
{
	REALPOS rTo; 
	rTo.x = rFrom.x + m_nBaseRealPos.nPos.x;
	rTo.z = rFrom.z + m_nBaseRealPos.nPos.z;
	// oto�en� podle �hlu orientace

	FMntMatrix2 mat;
	mat.Rotation((float)-m_nBaseRealPos.nGra);  // rotate in log coordinates
	FMntVector2 ptVec(rFrom.x, rFrom.z);

	ptVec = mat * ptVec; 
	rTo.x = ptVec[0] + m_nBaseRealPos.nPos.x;
	rTo.z = ptVec[1] + m_nBaseRealPos.nPos.z;	
	return rTo;
}

REALPOS CNetPartBase::CalcRealPosFromRelativePos(Vector3 pt) const
{
	REALPOS rPos; 
	rPos.x = pt[0]; 
	rPos.z = pt[2];
	return CalcRealPosFromRelativePos(rPos);
}

POINT CNetPartBase::CalcLogPosFromRelativePoint(Vector3 pt, const CPoseidonMap* pMap) const
{
	REALPOS rPos; 
	rPos.x = pt[0];
	rPos.z = pt[2];
	rPos = CalcRealPosFromRelativePos(rPos);
	POINT ptLog = pMap->FromRealToViewPos(rPos);
	return ptLog;
}

Ref<CPosObjectTemplate> CNetPartBase::GetObjectTemplate(const CPoseidonMap* map) const
{
	CPosEdObject* obj = map->GetPoseidonObject(m_nPosObjectID);
	if (obj == NULL) return NULL;

	return obj->GetTemplate();
}

/////////////////////////////////////////////////////////////////////////////
// CNetPartSIMP - ��st s�t� - rovinka v. zat��ka

IMPLEMENT_DYNAMIC(CNetPartSIMP, CNetPartBase)

CNetPartSIMP::CNetPartSIMP()
{
	m_bCanChangePartSlope = FALSE;
}

CNetPartSIMP::~CNetPartSIMP()
{
}

// vytvo�en� kopie objektu
void CNetPartSIMP::CopyFrom(const CNetPartBase* pSrc)
{
	CNetPartBase::CopyFrom(pSrc);
	// vlastn� data
	ASSERT_KINDOF(CNetPartSIMP, pSrc);
	m_nPartSubtype   = ((CNetPartSIMP*)pSrc)->m_nPartSubtype;
	m_nComponentName = ((CNetPartSIMP*)pSrc)->m_nComponentName;

	// relativn� poloha �sek� A,B
	m_SectionBeginA = ((CNetPartSIMP*)pSrc)->m_SectionBeginA;
	m_SectionBeginB = ((CNetPartSIMP*)pSrc)->m_SectionBeginB;
	m_nRelBankWidth = ((CNetPartSIMP*)pSrc)->m_nRelBankWidth;

	// body s�ov�ho objektu relativn� k A
	m_ptRelBoundingCenter = ((CNetPartSIMP*)pSrc)->m_ptRelBoundingCenter;
	for (int i = 0; i < ptNetCount; ++i)
	{
		m_ptRelNetObjPoints[i] = ((CNetPartSIMP*)pSrc)->m_ptRelNetObjPoints[i];
	}

	m_LogPosition.CopyFrom(&((CNetPartSIMP*)pSrc)->m_LogPosition);

	m_nConnRealPos = ((CNetPartSIMP*)pSrc)->m_nConnRealPos;
	m_nTestState   = ((CNetPartSIMP*)pSrc)->m_nTestState;

	// temp data
	m_bCanChangePartSlope = ((CNetPartSIMP*)pSrc)->m_bCanChangePartSlope;
}

// serializace
void CNetPartSIMP::DoSerialize(CArchive& ar, DWORD nVer)
{
	CNetPartBase::DoSerialize(ar, nVer);
	// vlastn� data
	MntSerializeWord(ar, m_nPartSubtype);
	MntSerializeString(ar, m_nComponentName);

	// relativn� poloha �sek� A,B
	MntSerializeNetPos(ar, nVer, m_SectionBeginA);
	MntSerializeNetPos(ar, nVer, m_SectionBeginB);
	if (nVer >= 24)
	{
		MntSerializeDouble(ar, m_nRelBankWidth);
	}
	else
	{
		m_nRelBankWidth = 10.25;
	}

	// body s�ov�ho objektu relativn� k A
	MntSerializeFloat(ar, m_ptRelBoundingCenter[0]);
	MntSerializeFloat(ar, m_ptRelBoundingCenter[1]);
	MntSerializeFloat(ar, m_ptRelBoundingCenter[2]);
	for (int i = 0; i < ptNetCount; ++i)
	{
		MntSerializeFloat(ar, m_ptRelNetObjPoints[i][0]);
		MntSerializeFloat(ar, m_ptRelNetObjPoints[i][1]);
		MntSerializeFloat(ar, m_ptRelNetObjPoints[i][2]);
	}
}

// pr�ce se sou�adnicemi objektu
void CNetPartSIMP::DoUpdateLogPosition(const CPoseidonMap* pMap)
{
	m_LogPosition.ResetPosition();
	m_LogPosition.AddPoint(CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetLE], pMap));
	m_LogPosition.AddPoint(CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetPE], pMap));
	m_LogPosition.AddPoint(CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetPB], pMap));
  //m_LogPosition.m_Position[3] = CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetPB],pMap);
	m_LogPosition.AddPoint(CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetLB], pMap));
}

// poloha n�spu pro ptNetLB,ptNet...
REALPOS CNetPartSIMP::CalcBankRealPosition(int nPt) const
{
	REALPOS ptBank;
	switch(nPt)
	{
	case ptNetLB:
		ptBank.x = m_SectionBeginB.nPos.x - (float)m_nRelBankWidth;
		ptBank.z = m_SectionBeginB.nPos.z;
		break;
	case ptNetPB:
		ptBank.x = m_SectionBeginB.nPos.x + (float)m_nRelBankWidth;
		ptBank.z = m_SectionBeginB.nPos.z;
		break;
	case ptNetLE:
		ptBank.x = m_SectionBeginA.nPos.x - (float)m_nRelBankWidth;
		ptBank.z = m_SectionBeginA.nPos.z;
		break;
	case ptNetPE:
		ptBank.x = m_SectionBeginA.nPos.x + (float)m_nRelBankWidth;
		ptBank.z = m_SectionBeginA.nPos.z;
		break;
	default:
		ASSERT(FALSE);
	}
	// p�epo�et z relativn�ch na absolutn�
	return CalcRealPosFromRelativePos(ptBank);
}

// pr�ce s v��kou
double CNetPartSIMP::GetCurrentSlopeRad() const // sklon d�lu
{	
	double nSlope = 0.;
	// spo��t�m absolutn� v��ku v obou konc�ch
	if (m_nConnRealPos.nHgt != m_nBaseRealPos.nHgt)
	{
		if (GetNetPartType() == CNetPartBase::netPartBEND)
		{
			double nPom  = m_nBaseRealPos.nHgt - m_nConnRealPos.nHgt;
				   nPom /= CNetBEND::GetNormalizeLength(m_nPartSubtype);
			nSlope = atan(nPom);
		} 
		else
		{
			double nPom  = m_nBaseRealPos.nHgt - m_nConnRealPos.nHgt;
				   nPom /= CNetBaseStra::GetNormalizeLength(m_nPartSubtype);
			nSlope = atan(nPom);
		}
	}
	return nSlope;
}

// vykreslov�n� objektu
void CNetPartSIMP::OnDrawObject(CDC* pDC, const CDrawPar* pParams, bool locked) const
{
	ASSERT(FALSE);
}

void CNetPartSIMP::OnDrawObject(CDC* pDC, const CDrawPar* pParams, CBrush* pBr, BOOL bFill) const
{
	RECT hull = m_DevPosition.GetPositionHull();
#define MIN_DRAW_SIZE 3
	if ((hull.right - hull.left) < MIN_DRAW_SIZE && (hull.bottom - hull.top) < MIN_DRAW_SIZE)
	{
		LOGBRUSH brushPar;
		pBr->GetLogBrush(&brushPar);

		pDC->SetPixelV((hull.right + hull.left) / 2, (hull.top + hull.bottom) / 2, brushPar.lbColor);
		return;
	}

	CRgn rgn;
	m_DevPosition.CreatePolyRgn(rgn);
	if (bFill)
	{
		pDC->FillRgn(&rgn, pBr);
	}
	else
	{
		pDC->FrameRgn(&rgn, pBr, 1, 1);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CNetPartSTRA - ��st s�t� - rovinka

IMPLEMENT_DYNCREATE(CNetPartSTRA, CNetPartSIMP)

CNetPartSTRA::CNetPartSTRA()
{	
	m_bCanChangePartSlope = TRUE;
	MakeDefault(); 
}

// vytvo�en� kopie objektu
void CNetPartSTRA::CopyFrom(const CNetPartBase* pSrc)
{
	CNetPartSIMP::CopyFrom(pSrc);
	// vlastn� data
	ASSERT_KINDOF(CNetPartSTRA, pSrc);
	m_bDisChangeSlope = ((CNetPartSTRA*)pSrc)->m_bDisChangeSlope;
}

// update registrovan�ch objekt� podle definovan�ch
void CNetPartSTRA::UpdateNetsObjectsFromDefObjects(const CMgrNets*, const CNetPartsList*)
{
	ASSERT(m_bCanChangePartSlope);
}

// serializace
void CNetPartSTRA::DoSerialize(CArchive& ar, DWORD nVer)
{
	CNetPartSIMP::DoSerialize(ar, nVer);
	// vlastn� data
	if (nVer <= 27)
	{
		BOOL bCanChangeSlope = TRUE;
		MntSerializeBOOL(ar, bCanChangeSlope);
	}
	MntSerializeBOOL(ar, m_bDisChangeSlope);
}

void CNetPartSTRA::DoUpdateFromTemplate(const CPoseidonMap *map)
{
	Ref<CPosObjectTemplate> templ = GetObjectTemplate(map);
	if (templ)
	{
		templ->UpdateParamsFromSourceFile();

		// body s�ov�ho objektu relativn� k A
		Vector3 ptA = templ->GetNetPointA();
		m_ptRelBoundingCenter = -ptA; 		
		m_ptRelNetObjPoints[ptNetLB] = GetRelativPointToA(templ->GetNetPoint(ptNetLB), ptA);
		m_ptRelNetObjPoints[ptNetPB] = GetRelativPointToA(templ->GetNetPoint(ptNetPB), ptA);
		m_ptRelNetObjPoints[ptNetLB][2] = 
		m_ptRelNetObjPoints[ptNetPB][2] = m_SectionBeginB.nPos.z;
		m_ptRelNetObjPoints[ptNetLE] = GetRelativPointToA(templ->GetNetPoint(ptNetLE), ptA);
		m_ptRelNetObjPoints[ptNetPE] = GetRelativPointToA(templ->GetNetPoint(ptNetPE), ptA);
		m_ptRelNetObjPoints[ptNetLE][2] = 
		m_ptRelNetObjPoints[ptNetPE][2] = m_SectionBeginA.nPos.z;

		// ���ka n�spu
		m_nRelBankWidth	= (templ->GetWidth() / 2.);
	}
}

// vytvo�en� objektu
void CNetPartSTRA::CreateFromTemplateSTRA(const CNetSTRA* pStra,const CNetPartsList* pOwner)
{
	// typ d�lu
	m_nPartSubtype		= pStra->m_nSTRAType;
	// jm�no komponenty
	m_nComponentName	= pStra->m_sCompName;
	// v��ka automaticky, nastaven default

	//m_bAutoHeight		= TRUE;
	//m_nRelaHeight		= 0.f;
	//if (pOwner)
		//m_nRelaHeight	= (float)pOwner->m_nStdHeight;

	// bude vytvo�en podle �ablony objektu
	ASSERT(m_nPosObjectID == UNDEF_REG_ID);
	ASSERT(m_pCreateObject == NULL);
	ASSERT(pStra->GetObjectTemplate() != NULL);
	m_pCreateObject = pStra->GetObjectTemplate();

	// lze m�nit sklon d�lu
 	ASSERT(m_bCanChangePartSlope);		
	m_bDisChangeSlope = (pStra->m_bChngHeight) ? FALSE : TRUE;

	// relativn� poloha �sek� A,B
	InitRealNetPos(m_SectionBeginA);
	// B
	m_SectionBeginB			= m_SectionBeginA;
	m_SectionBeginB.nPos.z -= pStra->GetStraLength();
	m_SectionBeginB.nGra	= 180;

	// body s�ov�ho objektu relativn� k A
	Vector3 ptA = pStra->GetObjectTemplate()->GetNetPointA();
	m_ptRelBoundingCenter = -ptA; 
	
	m_ptRelNetObjPoints[ptNetLB] = GetRelativPointToA(pStra->GetObjectTemplate()->GetNetPoint(ptNetLB),ptA);
	m_ptRelNetObjPoints[ptNetPB] = GetRelativPointToA(pStra->GetObjectTemplate()->GetNetPoint(ptNetPB),ptA);
	m_ptRelNetObjPoints[ptNetLB][2] = 
	m_ptRelNetObjPoints[ptNetPB][2] = m_SectionBeginB.nPos.z;
	m_ptRelNetObjPoints[ptNetLE] = GetRelativPointToA(pStra->GetObjectTemplate()->GetNetPoint(ptNetLE),ptA);
	m_ptRelNetObjPoints[ptNetPE] = GetRelativPointToA(pStra->GetObjectTemplate()->GetNetPoint(ptNetPE),ptA);
	m_ptRelNetObjPoints[ptNetLE][2] = 
	m_ptRelNetObjPoints[ptNetPE][2] = m_SectionBeginA.nPos.z;

	// ���ka n�spu
	m_nRelBankWidth	= (pStra->GetObjectTemplate()->GetWidth() / 2.);
}

REALNETPOS CNetPartSTRA::CalcABasePosition(REALNETPOS PosConn, const CPoseidonMap* pMap)
{
	// nastav�m m_nConnRealPos
	m_nConnRealPos		= PosConn;
	// v�po�et polohy pro A
	REALNETPOS rPosNext = PosConn;
	// pousunu se ve sm�ru �hlu o GetNormalizeLength podle typu
	float		nLenght = CNetBaseStra::GetNormalizeLength(m_nPartSubtype);
	REALPOS	    rNewPos;
	GetRadixPoint(rNewPos, PosConn.nPos, nLenght, GetRadFromGrad(PosConn.nGra));
	rPosNext.nPos = rNewPos;
	// �hel nGra z�stane zachov�n - sm�r se nem�n�
	
	// v��ka v dan�m bod� + relativn� posun
	rPosNext.nHgt = pMap->GetLandscapeHeight(rPosNext.nPos);
	//rPosNext.nHgt+= (float)m_nRelaHeight;

	// nem�-li sklon -> v��ka v B bude stejn�
	if ((!CanChangeSlope()) || (DisChangeSlope())) m_nConnRealPos.nHgt = rPosNext.nHgt;

	m_nBaseRealPos = rPosNext;

	return rPosNext;
}

void CalcVertRotZPosition(CPosEdObject* pObject, double nRad)
{
	// obr�cen� sm�r - nev�m pro�, ale funguje
//	nRad  =  6.28318530718-nRad;
//	nGrad = 360.-nGrad;
	// p��prava dat
	float TI[4][4];
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			TI[i][j] = pObject->m_Position(i, j);
		}
	}

	// p��prava konstant
	float t = (float)tan(nRad);
	// oto�en� orientace
	pObject->m_Position(1, 0) = TI[1][0] + t * TI[2][0];
	pObject->m_Position(1, 1) = TI[1][1] + t * TI[2][1];
	pObject->m_Position(1, 2) = TI[1][2] + t * TI[2][2];
}

BOOL CNetPartSTRA::DoUpdatePoseidonNetObject(CPosEdObject* pObject, const CPoseidonMap* pMap)
{
	if (!CNetPartBase::DoUpdatePoseidonNetObject(pObject, pMap)) return FALSE;

	if ((m_nConnRealPos.nHgt != m_nBaseRealPos.nHgt) &&
		(CanChangeSlope()) && (!DisChangeSlope()))
	{	// d�l se sklonem

		//nSlope = atan((m_nBaseRealPos.nHgt-m_nConnRealPos.nHgt)/CNetBaseStra::GetNormalizeLength(m_nPartSubtype));
		double nSlope = GetCurrentSlopeRad();
		ASSERT(nSlope != 0.);
		// naklon�m o p��slu�n� �hel
		CalcVertRotZPosition(pObject, nSlope);

		// poloha objektu

		// bouding center (relativn� k A)
		REALPOS   rBounding;
		rBounding.x = m_ptRelBoundingCenter[0];
		rBounding.z = m_ptRelBoundingCenter[2];
		rBounding   = CalcRealPosFromRelativePos(rBounding);

		// um�st�m objekt na tuto polohu
		pObject->m_Position(0, 3) = rBounding.x;
		pObject->m_Position(2, 3) = rBounding.z;

		// zarotuji 
		pObject->CalcRotationPosition(-m_nBaseRealPos.nGra);		// is in log

		// v��ka povrchu v bod� A a ve st�edu
		float nHgA = pMap->GetLandscapeHeight(m_nBaseRealPos.nPos);
		float nHgC = pMap->GetLandscapeHeight(rBounding);
		// o kolik se samovoln� zm�n� v��ka mezi st�edem a bodem A ?
		double nDistance = GetDistancePoints(m_nBaseRealPos.nPos, rBounding);
		nDistance = nDistance * tan(nSlope);

		// relativn� v��ka objektu
		pObject->m_nRelHeight = (float)/*m_nRelaHeight + */(nHgA - nHgC) - (float)nDistance;
		// v�po�et v��ky	
		//m_Position(1,3) = CalcObjectPoseidonHeight(pMap);
		if (pObject->m_pTemplate.NotNull())
		{
			pObject->m_Position(1, 3) = pObject->m_nRelHeight + nHgC + pObject->m_pTemplate->GetBoundingCenter()[1];
		}
		else
		{
			Ref<CPosObjectTemplate> templ = pMap->m_pDocument->GetObjectTemplate(pObject->m_nTemplateID);
			if (templ.NotNull()) pObject->m_Position(1, 3) = pObject->m_nRelHeight + nHgC + templ->GetBoundingCenter()[1];
		}
	}	
	else
	{	// rovn� d�l
		// bouding center (relativn� k A)
		REALPOS   rBounding;
		rBounding.x = m_ptRelBoundingCenter[0];
		rBounding.z = m_ptRelBoundingCenter[2];

		rBounding   = CalcRealPosFromRelativePos(rBounding);
		// um�st�m objekt na tuto polohu
		pObject->m_Position(0, 3) = rBounding.x;
		pObject->m_Position(2, 3) = rBounding.z;
		// zarotuji 
		pObject->CalcRotationPosition(-m_nBaseRealPos.nGra);
		// v��ka povrchu v bod� A a ve st�edu
		float nHgA = pMap->GetLandscapeHeight(m_nBaseRealPos.nPos);
		float nHgC = pMap->GetLandscapeHeight(rBounding);
		// relativn� v��ka objektu
		pObject->m_nRelHeight = (float)/*m_nRelaHeight + */(nHgA - nHgC);
		// v�po�et v��ky	
		//m_Position(1,3) = CalcObjectPoseidonHeight(pMap);
		if (pObject->m_pTemplate.NotNull())
		{
			pObject->m_Position(1,3) = pObject->m_nRelHeight + nHgC + pObject->m_pTemplate->GetBoundingCenter()[1];
		}
		else
		{
			Ref<CPosObjectTemplate> templ = pMap->m_pDocument->GetObjectTemplate(pObject->m_nTemplateID);
			if (templ.NotNull()) pObject->m_Position(1, 3) = pObject->m_nRelHeight + nHgC + templ->GetBoundingCenter()[1];
		}
	}
	// v�po�et nov� logick� polohy objektu
	pObject->CalcObjectLogPosition();
	return TRUE;
}

void CNetPartSTRA::AdvanceWalkPoint(NetWalkPoint& wpt, float& dist) const
{
	float rest = (float)GetDistancePoints(wpt._pos, m_nBaseRealPos.nPos);
  //float	lenght = CNetBaseStra::GetNormalizeLength(m_nPartSubtype);

	if (rest < dist)
	{
		dist -= rest;
		wpt._pos = m_nBaseRealPos.nPos;
		wpt._grad = -(float) m_nBaseRealPos.nGra;
		return;
	}
	else
	{    
		GetRadixPoint(wpt._pos,wpt._pos, dist, GetRadFromGrad(m_nBaseRealPos.nGra)); 
		wpt._grad = -(float) m_nBaseRealPos.nGra;
		dist = 0; 
	}
}

/////////////////////////////////////////////////////////////////////////////
// CNetPartSPEC - ��st s�t� - spec. �sek

IMPLEMENT_DYNCREATE(CNetPartSPEC,CNetPartSIMP)

CNetPartSPEC::CNetPartSPEC()
{	
	m_bCanChangePartSlope = TRUE;
	MakeDefault(); 
}

// vytvo�en� kopie objektu
void CNetPartSPEC::CopyFrom(const CNetPartBase* pSrc)
{
	CNetPartSIMP::CopyFrom(pSrc);
	// vlastn� data
	ASSERT_KINDOF(CNetPartSPEC, pSrc);
	m_bDisChangeSlope = ((CNetPartSPEC*)pSrc)->m_bDisChangeSlope;
}

// update registrovan�ch objekt� podle definovan�ch
void CNetPartSPEC::UpdateNetsObjectsFromDefObjects(const CMgrNets*, const CNetPartsList*)
{
	ASSERT(m_bCanChangePartSlope);
}

// serializace
void CNetPartSPEC::DoSerialize(CArchive& ar, DWORD nVer)
{
	CNetPartSIMP::DoSerialize(ar, nVer);
	// vlastn� data
	if (nVer <= 27)
	{
		BOOL bCanChangeSlope = TRUE;
		MntSerializeBOOL(ar, bCanChangeSlope);
	}
	MntSerializeBOOL(ar, m_bDisChangeSlope);
}

void CNetPartSPEC::DoUpdateFromTemplate(const CPoseidonMap* map)
{
	Ref<CPosObjectTemplate> templ = GetObjectTemplate(map);

	if (templ)
	{
		templ->UpdateParamsFromSourceFile();

		// body s�ov�ho objektu relativn� k A
		Vector3 ptA = templ->GetNetPointA();
		m_ptRelBoundingCenter = -ptA; 		
		m_ptRelNetObjPoints[ptNetLB] = GetRelativPointToA(templ->GetNetPoint(ptNetLB), ptA);
		m_ptRelNetObjPoints[ptNetPB] = GetRelativPointToA(templ->GetNetPoint(ptNetPB), ptA);
		m_ptRelNetObjPoints[ptNetLB][2] = 
		m_ptRelNetObjPoints[ptNetPB][2] = m_SectionBeginB.nPos.z;
		m_ptRelNetObjPoints[ptNetLE] = GetRelativPointToA(templ->GetNetPoint(ptNetLE), ptA);
		m_ptRelNetObjPoints[ptNetPE] = GetRelativPointToA(templ->GetNetPoint(ptNetPE), ptA);
		m_ptRelNetObjPoints[ptNetLE][2] = 
		m_ptRelNetObjPoints[ptNetPE][2] = m_SectionBeginA.nPos.z;

		// ���ka n�spu
		m_nRelBankWidth	= (templ->GetWidth() / 2.);
	}
}

// vytvo�en� objektu
void CNetPartSPEC::CreateFromTemplateSPEC(const CNetSPEC* pSpec, const CNetPartsList* pOwner)
{
	// typ d�lu
	m_nPartSubtype		= pSpec->m_nSTRAType;
	// jm�no komponenty
	m_nComponentName	= pSpec->m_sCompName;

	// v��ka automaticky, nastaven default
	//m_bAutoHeight		= TRUE;
	//m_nRelaHeight		= 0.f;
	//if (pOwner)
	//	m_nRelaHeight	= (float)pOwner->m_nStdHeight;

	// bude vytvo�en podle �ablony objektu
	ASSERT(m_nPosObjectID == UNDEF_REG_ID);
	ASSERT(m_pCreateObject == NULL);
	ASSERT(pSpec->GetObjectTemplate() != NULL);
	m_pCreateObject = pSpec->GetObjectTemplate();

	// lze m�nit sklon d�lu
	ASSERT(m_bCanChangePartSlope);
	m_bDisChangeSlope = (pSpec->m_bChngHeight) ? FALSE : TRUE;

	// relativn� poloha �sek� A,B
	InitRealNetPos(m_SectionBeginA);
	// B
	m_SectionBeginB			= m_SectionBeginA;
	m_SectionBeginB.nPos.z -= pSpec->GetStraLength();
	m_SectionBeginB.nGra	= 180;

	// body s�ov�ho objektu relativn� k A
	Vector3 ptA = pSpec->GetObjectTemplate()->GetNetPointA();
	m_ptRelBoundingCenter = -ptA; 	
	m_ptRelNetObjPoints[ptNetLB] = GetRelativPointToA(pSpec->GetObjectTemplate()->GetNetPoint(ptNetLB),ptA);
	m_ptRelNetObjPoints[ptNetPB] = GetRelativPointToA(pSpec->GetObjectTemplate()->GetNetPoint(ptNetPB),ptA);
	m_ptRelNetObjPoints[ptNetLB][2] = 
	m_ptRelNetObjPoints[ptNetPB][2] = m_SectionBeginB.nPos.z;
	m_ptRelNetObjPoints[ptNetLE] = GetRelativPointToA(pSpec->GetObjectTemplate()->GetNetPoint(ptNetLE),ptA);
	m_ptRelNetObjPoints[ptNetPE] = GetRelativPointToA(pSpec->GetObjectTemplate()->GetNetPoint(ptNetPE),ptA);
	m_ptRelNetObjPoints[ptNetLE][2] = 
	m_ptRelNetObjPoints[ptNetPE][2] = m_SectionBeginA.nPos.z;

	// ���ka n�spu
	m_nRelBankWidth	= (pSpec->GetObjectTemplate()->GetWidth() / 2.);
}

REALNETPOS CNetPartSPEC::CalcABasePosition(REALNETPOS PosConn, const CPoseidonMap* pMap)
{
	// nastav�m m_nConnRealPos
	m_nConnRealPos		= PosConn;
	// v�po�et polohy pro A
	REALNETPOS rPosNext = PosConn;
	// pousunu se ve sm�ru �hlu o GetNormalizeLength podle typu
	float		nLenght = CNetBaseStra::GetNormalizeLength(m_nPartSubtype);
	REALPOS	    rNewPos;
	GetRadixPoint(rNewPos, PosConn.nPos, nLenght, GetRadFromGrad(PosConn.nGra));
	rPosNext.nPos = rNewPos;	
	// �hel nGra z�stane zachov�n - sm�r se nem�n�

	// v��ka v dan�m bod� + relativn� posun
	rPosNext.nHgt = pMap->GetLandscapeHeight(rPosNext.nPos);
	//rPosNext.nHgt+= (float)m_nRelaHeight;
	// nem�-li sklon -> v��ka v B bude stejn�
	if ((!CanChangeSlope()) || (DisChangeSlope())) m_nConnRealPos.nHgt = rPosNext.nHgt;

	m_nBaseRealPos = rPosNext;

	return rPosNext;
}

// pr�ce s objekty Poseidon (Update a Vytvo�en�)
BOOL CNetPartSPEC::DoUpdatePoseidonNetObject(CPosEdObject* pObject, const CPoseidonMap* pMap)
{
	if (!CNetPartSIMP::DoUpdatePoseidonNetObject(pObject, pMap)) return FALSE;

	if ((m_nConnRealPos.nHgt != m_nBaseRealPos.nHgt) &&
		(CanChangeSlope())&&(!DisChangeSlope()))
	{	// d�l se sklonem

		//nSlope = atan((m_nBaseRealPos.nHgt-m_nConnRealPos.nHgt)/CNetBaseStra::GetNormalizeLength(m_nPartSubtype));
		double nSlope = GetCurrentSlopeRad();
		ASSERT(nSlope != 0.);
		// naklon�m o p��slu�n� �hel
		CalcVertRotZPosition(pObject, nSlope);

		// poloha objektu

		// bouding center (relativn� k A)
		REALPOS   rBounding;
		rBounding.x = m_ptRelBoundingCenter[0];
		rBounding.z = m_ptRelBoundingCenter[2];
		rBounding   = CalcRealPosFromRelativePos(rBounding);

		// um�st�m objekt na tuto polohu
		pObject->m_Position(0, 3) = rBounding.x;
		pObject->m_Position(2, 3) = rBounding.z;

		// zarotuji 
		pObject->CalcRotationPosition(-m_nBaseRealPos.nGra);		

		// v��ka povrchu v bod� A a ve st�edu
		float nHgA = pMap->GetLandscapeHeight(m_nBaseRealPos.nPos);
		float nHgC = pMap->GetLandscapeHeight(rBounding);
		// o kolik se samovoln� zm�n� v��ka mezi st�edem a bodem A ?
		double nDistance = GetDistancePoints(m_nBaseRealPos.nPos, rBounding);
		nDistance = nDistance * tan(nSlope);

		// relativn� v��ka objektu
		pObject->m_nRelHeight = (float)/*m_nRelaHeight + */(nHgA - nHgC) - (float)nDistance;
		// v�po�et v��ky	
		//m_Position(1,3) = CalcObjectPoseidonHeight(pMap);
		if (pObject->m_pTemplate.NotNull())
		{
			pObject->m_Position(1, 3) = pObject->m_nRelHeight + nHgC + pObject->m_pTemplate->GetBoundingCenter()[1];
		}
		else
		{
			Ref<CPosObjectTemplate> templ = pMap->m_pDocument->GetObjectTemplate(pObject->m_nTemplateID);
			if (templ.NotNull()) pObject->m_Position(1, 3) = pObject->m_nRelHeight + nHgC + templ->GetBoundingCenter()[1];
		}
	}	
	else
	{	// rovn� d�l
		// bouding center (relativn� k A)
		REALPOS   rBounding;
		rBounding.x = m_ptRelBoundingCenter[0];
		rBounding.z = m_ptRelBoundingCenter[2];
		rBounding   = CalcRealPosFromRelativePos(rBounding);
		// um�st�m objekt na tuto polohu
		pObject->m_Position(0, 3) = rBounding.x;
		pObject->m_Position(2, 3) = rBounding.z;
		// zarotuji 
		pObject->CalcRotationPosition(-m_nBaseRealPos.nGra);    
		// v��ka povrchu v bod� A a ve st�edu
		float nHgA = pMap->GetLandscapeHeight(m_nBaseRealPos.nPos);
		float nHgC = pMap->GetLandscapeHeight(rBounding);
		// relativn� v��ka objektu
		pObject->m_nRelHeight = (float)/*m_nRelaHeight + */(nHgA - nHgC);
		// v�po�et v��ky	
		//m_Position(1,3) = CalcObjectPoseidonHeight(pMap);
		if (pObject->m_pTemplate.NotNull())
		{
			pObject->m_Position(1, 3) = pObject->m_nRelHeight + nHgC + pObject->m_pTemplate->GetBoundingCenter()[1];
		}
		else
		{
			Ref<CPosObjectTemplate> templ = pMap->m_pDocument->GetObjectTemplate(pObject->m_nTemplateID);
			if (templ.NotNull()) pObject->m_Position(1, 3) = pObject->m_nRelHeight + nHgC + templ->GetBoundingCenter()[1];
		}
	}
	// v�po�et nov� logick� polohy objektu
	pObject->CalcObjectLogPosition();
	return TRUE;
}

void CNetPartSPEC::AdvanceWalkPoint(NetWalkPoint& wpt, float& dist) const
{
	float rest = (float)GetDistancePoints(wpt._pos, m_nBaseRealPos.nPos);

	if (rest < dist)
	{
		dist -= dist;
		wpt._pos = m_nBaseRealPos.nPos;
		wpt._grad = -(float) m_nBaseRealPos.nGra;
		return;
	}
	else
	{    
		GetRadixPoint(wpt._pos, wpt._pos, dist, GetRadFromGrad(m_nBaseRealPos.nGra)); 
		wpt._grad = -(float) m_nBaseRealPos.nGra;
		dist = 0; 
	}
}

/////////////////////////////////////////////////////////////////////////////
// CNetPartBEND - ��st s�t� - zat��ka

IMPLEMENT_DYNCREATE(CNetPartBEND, CNetPartSIMP)

CNetPartBEND::CNetPartBEND()
{	
	m_bCanChangePartSlope = TRUE;

	MakeDefault(); 
}

// vytvo�en� kopie objektu
void CNetPartBEND::CopyFrom(const CNetPartBase* pSrc)
{
	CNetPartSIMP::CopyFrom(pSrc);
	// vlastn� data
	ASSERT_KINDOF(CNetPartBEND, pSrc);
	m_bLeftBend	= ((CNetPartBEND*)pSrc)->m_bLeftBend;
}

// update registrovan�ch objekt� podle definovan�ch
void CNetPartBEND::UpdateNetsObjectsFromDefObjects(const CMgrNets* pMgr, const CNetPartsList* pList)
{
	//if ((pList != NULL)&&(pList->m_bAutoHeight))
		m_bCanChangePartSlope = TRUE;
	//else
	//	m_bCanChangePartSlope = FALSE;
}

// serializace
void CNetPartBEND::DoSerialize(CArchive& ar, DWORD nVer)
{
	CNetPartSIMP::DoSerialize(ar, nVer);
	// vlastn� data
	MntSerializeBOOL(ar, m_bLeftBend);
}

void CNetPartBEND::DoUpdateFromTemplate(const CPoseidonMap* map)
{
	//CString name = m_nComponentName + ".p3d";
	Ref<CPosObjectTemplate> templ = GetObjectTemplate(map);

	if (templ)
	{
		templ->UpdateParamsFromSourceFile();

		// body s�ov�ho objektu relativn� k A
		Vector3 ptA = templ->GetNetPointA();

		// new code
		// Added to allow for variable angle
		// **********************************
		CNetBEND bend;
		CPosObjectTemplate* tempNetBend = new CPosObjectTemplate(NULL);
		tempNetBend->CopyFrom(templ);
		bend.CreateFromObjectTemplate(tempNetBend);
		float nGrad = bend.GetNormalizeGrad();

		// old code
		// **********************************
		/*
		int nGrad = CNetBEND::GetNormalizeGrad();
		*/
		// **********************************

		m_ptRelBoundingCenter = VZero;		
		m_ptRelBoundingCenter = GetRelativPtRotToA(m_ptRelBoundingCenter, ptA, nGrad);
		m_ptRelNetObjPoints[ptNetLB] = GetRelativPtRotToA(templ->GetNetPoint(ptNetLB), ptA, nGrad);
		m_ptRelNetObjPoints[ptNetPB] = GetRelativPtRotToA(templ->GetNetPoint(ptNetPB), ptA, nGrad);
		m_ptRelNetObjPoints[ptNetLE] = GetRelativPtRotToA(templ->GetNetPoint(ptNetLE), ptA, nGrad);
		m_ptRelNetObjPoints[ptNetPE] = GetRelativPtRotToA(templ->GetNetPoint(ptNetPE), ptA, nGrad);
				
		// B spo��t�m jako st�ed LB,PB
		m_SectionBeginB.nPos.x = (m_ptRelNetObjPoints[ptNetLB][0] + m_ptRelNetObjPoints[ptNetPB][0]) / 2.0f;
		m_SectionBeginB.nPos.z = (m_ptRelNetObjPoints[ptNetLB][2] + m_ptRelNetObjPoints[ptNetPB][2]) / 2.0f;

		// ���ka n�spu
		// old code
		// **********************************
		/*
		CNetBEND bend;
		CPosObjectTemplate* templ2 = new CPosObjectTemplate(NULL);
		templ2->CopyFrom(templ);
		bend.CreateFromObjectTemplate(templ2);
		*/
		double nMore = bend.GetNormalizeRadix(bend.m_nBENDType) * (0.0151922469878);
		m_nRelBankWidth	= ((templ->GetWidth() - nMore) / 2.);
	}
}

// vytvo�en� objektu
void CNetPartBEND::CreateFromTemplateBEND(const CNetBEND* pBend, BOOL bLeft, const CNetPartsList* pOwner)
{
	// typ d�lu
	m_nPartSubtype		= pBend->m_nBENDType;
	// jm�no komponenty
	m_nComponentName	= pBend->m_sCompName;
	// orientace zat��ky
	m_bLeftBend			= bLeft;

	// v��ka automaticky, nastaven default
	//m_bAutoHeight		= TRUE;
	//m_nRelaHeight		= 0.f;
	//if (pOwner)
	//{
	//	m_nRelaHeight	= (float)pOwner->m_nStdHeight;
	//	m_bCanChangePartSlope = pOwner->m_bAutoHeight;
	//}
	m_bCanChangePartSlope = TRUE;

	// bude vytvo�en podle �ablony objektu
	ASSERT(m_nPosObjectID == UNDEF_REG_ID);
	ASSERT(m_pCreateObject == NULL);
	ASSERT(pBend->GetObjectTemplate() != NULL);
	m_pCreateObject = pBend->GetObjectTemplate();

	// relativn� poloha �sek� A,B

	InitRealNetPos(m_SectionBeginA);
	// B
	m_SectionBeginB = m_SectionBeginA;

	// new code
	// Added to allow for variable angle
	// **********************************
	float nGrad = pBend->GetNormalizeGrad();
	m_SectionBeginB.nGra = bLeft ? (180.0f + nGrad) : (180.0f - nGrad);
	// **************************************

	// old code
	// **********************************
	/*
	// B pozd�ji spo��t�m jako st�ed 	 LB,PB
	m_SectionBeginB.nGra = bLeft ? (180 + CNetBEND::GetNormalizeGrad()) : (180 - CNetBEND::GetNormalizeGrad());
	*/
	// **********************************

	// body s�ov�ho objektu relativn� k A
	Vector3 ptA = pBend->GetObjectTemplate()->GetNetPointA();

	// old code
	// **********************************
	/*
	int nGrad = CNetBEND::GetNormalizeGrad();
	*/
	// **********************************

	m_ptRelBoundingCenter = VZero;	
	m_ptRelBoundingCenter		   = GetRelativPtRotToA(m_ptRelBoundingCenter, ptA, nGrad);
	m_ptRelNetObjPoints[ptNetLB]   = GetRelativPtRotToA(pBend->GetObjectTemplate()->GetNetPoint(ptNetLB), ptA, nGrad);
	m_ptRelNetObjPoints[ptNetPB]   = GetRelativPtRotToA(pBend->GetObjectTemplate()->GetNetPoint(ptNetPB), ptA, nGrad);
	m_ptRelNetObjPoints[ptNetLE]   = GetRelativPtRotToA(pBend->GetObjectTemplate()->GetNetPoint(ptNetLE), ptA, nGrad);
	m_ptRelNetObjPoints[ptNetPE]   = GetRelativPtRotToA(pBend->GetObjectTemplate()->GetNetPoint(ptNetPE), ptA, nGrad);
	/*m_ptRelNetObjPoints[ptNetLE][2] = 
	m_ptRelNetObjPoints[ptNetPE][2] = m_SectionBeginA.nPos.z;
	// je-li m_bLeftBend -> zm�na LP, PB na druhou stranu osy X
	if (m_bLeftBend)
	{
		Vector3 vPom = m_ptRelNetObjPoints[ptNetLB];
		m_ptRelNetObjPoints[ptNetLB]   =  m_ptRelNetObjPoints[ptNetPB];
		m_ptRelNetObjPoints[ptNetLB][0] = -m_ptRelNetObjPoints[ptNetPB][0];
		m_ptRelNetObjPoints[ptNetPB]   = vPom;
		m_ptRelNetObjPoints[ptNetPB][0] = -vPom[0];
	}*/
	// B spo��t�m jako st�ed LB,PB
	m_SectionBeginB.nPos.x = (m_ptRelNetObjPoints[ptNetLB][0] + m_ptRelNetObjPoints[ptNetPB][0]) / 2.0f;
	m_SectionBeginB.nPos.z = (m_ptRelNetObjPoints[ptNetLB][2] + m_ptRelNetObjPoints[ptNetPB][2]) / 2.0f;

	// ���ka n�spu
	double nMore = pBend->GetNormalizeRadix(pBend->m_nBENDType) * (0.0151922469878); // * (1-cos(10�))
	m_nRelBankWidth	= ((pBend->GetObjectTemplate()->GetWidth() - nMore) / 2.);
}

REALNETPOS CNetPartBEND::CalcABasePosition(REALNETPOS PosConn, const CPoseidonMap* pMap)
{
	// nastav�m m_nConnRealPos
	m_nConnRealPos		= PosConn;
	// v�po�et polohy pro A
	REALNETPOS rPosNext = PosConn;
	// posun o d�lku CNetBEND::GetNormalizeLength
	// ve sm�ru �hlu +- CNetBEND::GetNormalizeGrad/2
	double nLength = CNetBEND::GetNormalizeLength(m_nPartSubtype);

	// new code
	// Added to allow for variable angle
	// **********************************
	float tempGrad = CNetBEND::GetNormalizeGrad(m_nPartSubtype);
	float nGrad = PosConn.nGra + (m_bLeftBend ? (-tempGrad / 2.0f) : (+tempGrad / 2.0f));
	//*************************************************************

	// old code
	//*************************************************************
	/*
	int nGrad = PosConn.nGra + (m_bLeftBend ? (-CNetBEND::GetNormalizeGrad() / 2) : (+CNetBEND::GetNormalizeGrad() / 2));
	*/
	//*************************************************************

	REALPOS rNewPos;
	GetRadixPoint(rNewPos, PosConn.nPos, nLength, GetRadFromGrad(nGrad));
	// nastaven� polohy
	rPosNext.nPos = rNewPos;
	// �hel nGra +- 10

	// new code
	// Added to allow for variable angle
	// **********************************
	rPosNext.nGra += m_bLeftBend ? (-tempGrad) : (tempGrad);
	// **********************************

	// old code
	// **********************************
	/*
	rPosNext.nGra += m_bLeftBend ? (-CNetBEND::GetNormalizeGrad()) : (CNetBEND::GetNormalizeGrad());
	*/
	// **********************************

	rPosNext.nGra  = GetNormalizedGrad(rPosNext.nGra);

	// v��ka v dan�m bod� + relativn� posun
	rPosNext.nHgt = pMap->GetLandscapeHeight(rPosNext.nPos);
	//rPosNext.nHgt+= (float)m_nRelaHeight;
	// nem� sklon -> v��ka v B bude stejn�
	// nem�-li sklon -> v��ka v B bude stejn�
	if ((!CanChangeSlope()) || (DisChangeSlope()))
	{
		m_nConnRealPos.nHgt = rPosNext.nHgt;
	}
	else
	{
		ASSERT(m_bCanChangePartSlope);
	}

	m_nBaseRealPos = rPosNext;

	if (m_bLeftBend)
	{
		m_nBaseRealPos.nPos = m_nConnRealPos.nPos;
		m_nBaseRealPos.nGra = m_nConnRealPos.nGra + 180/*-CNetBEND::GetNormalizeGrad())*/;
	}

	return rPosNext;
}

REALNETPOS CNetPartBEND::GetEndRealPos() const
{
	if (!m_bLeftBend) return m_nBaseRealPos;
  
	REALNETPOS ret;
	// find corner end for left bend (it is just rotated right bend). 
	double nLength = CNetBEND::GetNormalizeLength(m_nPartSubtype);

	// new code
	// Added to allow for variable angle
	// **********************************
	float tempGrad = CNetBEND::GetNormalizeGrad(m_nPartSubtype);
	float nGrad = m_nConnRealPos.nGra - tempGrad / 2.0f;
	// **********************************

	// old code
	// **********************************
	/*
	int nGrad = m_nConnRealPos.nGra - CNetBEND::GetNormalizeGrad() / 2;
	*/
	// **********************************

	REALPOS rNewPos;
	GetRadixPoint(rNewPos, m_nBaseRealPos.nPos, nLength, GetRadFromGrad(nGrad));                  
	ret.nPos = rNewPos;

	// new code
	// Added to allow for variable angle
	// **********************************
	ret.nGra = m_nConnRealPos.nGra - tempGrad;
	// **********************************

	// old code
	// **********************************
	/*
	ret.nGra = toInt(m_nConnRealPos.nGra - CNetBEND::GetNormalizeGrad());
	*/
	// **********************************
  
	return ret;
}

// poloha n�spu pro ptNetLB,ptNet...
REALPOS CNetPartBEND::CalcBankRealPosition(int nPt) const
{
	if (nPt == ptNetLB)
	{
		REALPOS rBank;
		double  nRad = m_bLeftBend ? (4.886921905584) : (4.537856055185);  // m_bLeftBend ? (270+10�) : (270-10�)
		GetRadixPoint(rBank, m_SectionBeginB.nPos, m_nRelBankWidth, nRad);
		return CalcRealPosFromRelativePos(rBank);
	} 
	else if (nPt == ptNetPB)
	{
		REALPOS rBank;
		double  nRad = m_bLeftBend ? (1.745329251994) : (1.396263401595);  // m_bLeftBend ? (90+10�) : (90-10�)
		GetRadixPoint(rBank, m_SectionBeginB.nPos, m_nRelBankWidth, nRad);
		return CalcRealPosFromRelativePos(rBank);
	}
	// standardn� v�po�et 
	return CNetPartSIMP::CalcBankRealPosition(nPt);
}

// pr�ce s objekty Poseidon (Update a Vytvo�en�)
BOOL CNetPartBEND::DoUpdatePoseidonNetObject(CPosEdObject* pObject, const CPoseidonMap* pMap)
{
	if (!CNetPartBase::DoUpdatePoseidonNetObject(pObject, pMap)) return FALSE;
	if (m_bLeftBend)
	{
		// jako puvodni objekt s prochozenymiA,B
		/*REALNETPOS OldBasePos = m_nBaseRealPos;
		m_nBaseRealPos.nPos   = m_nConnRealPos.nPos;
		m_nBaseRealPos.nGra   = m_nBaseRealPos.nGra + (180-CNetBEND::GetNormalizeGrad());*/

		// bouding center (relativn� k A)
		REALPOS   rBounding;
		rBounding.x = m_ptRelBoundingCenter[0];
		rBounding.z = m_ptRelBoundingCenter[2];
    
		rBounding   = CalcRealPosFromRelativePos(rBounding);
		// um�st�m objekt na tuto polohu
		pObject->m_Position(0,3) = rBounding.x;
		pObject->m_Position(2,3) = rBounding.z;

		// new code
		// Added to allow for variable angle
		// **********************************
		double tempGrad = CNetBEND::GetNormalizeGrad(m_nPartSubtype);
		pObject->CalcRotationPosition(-(m_nBaseRealPos.nGra - tempGrad));
		// **********************************

		// old code
		// **********************************
		/*
		// zarotuji 
		pObject->CalcRotationPosition(-(m_nBaseRealPos.nGra - CNetBEND::GetNormalizeGrad()));
		*/
		// **********************************

		// vrac�m zp�t sou�adnice
		//m_nBaseRealPos = OldBasePos;

		// v��ka povrchu v bod� A a ve st�edu
		float nHgA = pMap->GetLandscapeHeight(m_nBaseRealPos.nPos);
		float nHgC = pMap->GetLandscapeHeight(rBounding);
		// relativn� v��ka objektu
		pObject->m_nRelHeight = (float)/*m_nRelaHeight + */(nHgA - nHgC);
		// v�po�et v��ky	
		//m_Position(1,3) = CalcObjectPoseidonHeight(pMap);
		if (pObject->m_pTemplate.NotNull())
		{
			pObject->m_Position(1, 3) = pObject->m_nRelHeight + nHgC + pObject->m_pTemplate->GetBoundingCenter()[1];
		}
		else
		{
			Ref<CPosObjectTemplate> templ = pMap->m_pDocument->GetObjectTemplate(pObject->m_nTemplateID);
			if (templ.NotNull())
			{
				pObject->m_Position(1, 3) = pObject->m_nRelHeight + nHgC + templ->GetBoundingCenter()[1];
			}
		}
	} 
	else
	{
		// bouding center (relativn� k A)
		REALPOS   rBounding;
		rBounding.x = m_ptRelBoundingCenter[0];
		rBounding.z = m_ptRelBoundingCenter[2];
		rBounding   = CalcRealPosFromRelativePos(rBounding);
		// um�st�m objekt na tuto polohu
		pObject->m_Position(0, 3) = rBounding.x;
		pObject->m_Position(2, 3) = rBounding.z;

		// new code
		// Added to allow for variable angle
		// **********************************
		double tempGrad = CNetBEND::GetNormalizeGrad(m_nPartSubtype);
		pObject->CalcRotationPosition(-(m_nBaseRealPos.nGra - tempGrad)); // it is because m_nBaseRealPos.nGra is orientation of the end point    
		// **********************************

		// old code
		// **********************************
		/*
		// zarotuji 
		pObject->CalcRotationPosition(-(m_nBaseRealPos.nGra - CNetBEND::GetNormalizeGrad())); // it is because m_nBaseRealPos.nGra is orientation of the end point    
		*/
		// **********************************

		// v��ka povrchu v bod� A a ve st�edu
		float nHgA = pMap->GetLandscapeHeight(m_nBaseRealPos.nPos);
		float nHgC = pMap->GetLandscapeHeight(rBounding);
		// relativn� v��ka objektu
		pObject->m_nRelHeight = (float)/*m_nRelaHeight + */(nHgA - nHgC);
		// v�po�et v��ky	
		//m_Position(1,3) = CalcObjectPoseidonHeight(pMap);
		if (pObject->m_pTemplate.NotNull())
		{
			pObject->m_Position(1, 3) = pObject->m_nRelHeight + nHgC + pObject->m_pTemplate->GetBoundingCenter()[1];
		}
		else
		{
			Ref<CPosObjectTemplate> templ = pMap->m_pDocument->GetObjectTemplate(pObject->m_nTemplateID);
			if (templ.NotNull())
			{
				pObject->m_Position(1, 3) = pObject->m_nRelHeight + nHgC + templ->GetBoundingCenter()[1];
			}
		}
	}
	// v�po�et nov� logick� polohy objektu
	pObject->CalcObjectLogPosition();
	return TRUE;
}

void CNetPartBEND::AdvanceWalkPoint(NetWalkPoint& wpt, float& dist) const
{
	// first check its position 

	// new code
	// Added to allow for variable angle
	// **********************************
	float tempGrad = CNetBEND::GetNormalizeGrad(m_nPartSubtype);
	float grad = m_bLeftBend ? tempGrad + (-wpt._grad - m_nConnRealPos.nGra) : (m_nBaseRealPos.nGra + wpt._grad);
	// **********************************

	// old code
	// **********************************
	/*
	float grad = m_bLeftBend ? CNetBEND::GetNormalizeGrad() + (-wpt._grad - m_nConnRealPos.nGra) : (m_nBaseRealPos.nGra + wpt._grad);
	*/
	// **********************************

	grad = GetNormalizedGrad(grad);

	// old code
	// **********************************
	/*
	if (grad > CNetBEND::GetNormalizeGrad())
	*/
	// **********************************

	// new code
	// Added to allow for variable angle
	// **********************************
	if (grad > tempGrad)
	// **********************************
	{
		// numericaly in next part        
		return;
	}
	float radius = (float)CNetBEND::GetNormalizeRadix(m_nPartSubtype);
	float rest = (float) (radius * GetRadFromGrad(grad));
	if (rest < dist)
	{
		dist -= rest;
		if (m_bLeftBend)
		{
			// find corner end
			double nLength = CNetBEND::GetNormalizeLength(m_nPartSubtype);

			// new code
			// Added to allow for variable angle
			// **********************************
			float nGrad = m_nConnRealPos.nGra - CNetBEND::GetNormalizeGrad(m_nPartSubtype) / 2.0f;
			// **********************************

			// old code
			// **********************************
			/*
			int nGrad = m_nConnRealPos.nGra - CNetBEND::GetNormalizeGrad()/2;
			*/
			// **********************************

			REALPOS rNewPos;
			GetRadixPoint(rNewPos, m_nBaseRealPos.nPos, nLength, GetRadFromGrad(nGrad));                  
			wpt._pos = rNewPos;

			// new code
			// Added to allow for variable angle
			// **********************************
			wpt._grad = -(float)(m_nConnRealPos.nGra - CNetBEND::GetNormalizeGrad(m_nPartSubtype));
			// **********************************

			// old code
			// **********************************
			/*
			wpt._grad = -(float)(m_nConnRealPos.nGra - CNetBEND::GetNormalizeGrad());
			*/
			// **********************************
		}
		else
		{
			wpt._pos  = m_nBaseRealPos.nPos;
			wpt._grad = -(float)m_nBaseRealPos.nGra;
		}
		return;
	}
	else
	{
		wpt._grad -= (m_bLeftBend ? -1 : 1) *  (dist / rest * grad);
		wpt._grad = GetNormalizedGrad(wpt._grad);
		float angl0 = (float)GetRadFromGrad((m_bLeftBend ? 180 : 0) + m_nConnRealPos.nGra);
		float angl1 = (float)GetRadFromGrad((m_bLeftBend ? 180 : 0) - wpt._grad);

		wpt._pos.x = m_nConnRealPos.nPos.x - radius * (cos(angl1) - cos(angl0));
		wpt._pos.z = m_nConnRealPos.nPos.z + radius * (sin(angl1) - sin(angl0));

		//GetRadixPoint(wpt._pos.nPos,m_nConnRealPos.nPos, CNetBEND::GetNormalizeLength(m_nPartSubtype) - rest + dist, GetRadFromGrad(wpt._pos.nGra));    
		dist = 0;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CNetPartKEY	- ��st s�t� - kl��ov� bod

IMPLEMENT_DYNCREATE(CNetPartKEY, CNetPartBase)

CNetPartKEY::CNetPartKEY()
{	
	MakeDefault(); 
}

void CNetPartKEY::MakeDefault()
{
	CNetPartBase::MakeDefault();
	// vlastn� data
	m_nKeyPartType		= typeCross;
	m_nKeyPartSubtype	= 0;
	m_nCountSections	= 0;
	// jm�no s�t� nebo k�i�ovatky
	m_nTemplateName.Empty();
	m_nComponentName.Empty();
	//m_nMinPartHg		= 0.;
	//m_nMaxPartHg		= 0.;
	// barva - norm�ln� & vypl� zobrazen�
	m_clrNor			= RGB(0,0,0);			
	m_bFillEntire		= FALSE;
}

// vytvo�en� kopie objektu
void CNetPartKEY::CopyFrom(const CNetPartBase* pSrc)
{
	CNetPartBase::CopyFrom(pSrc);
	// vlastn� data
	ASSERT_KINDOF(CNetPartKEY,pSrc);
	m_nKeyPartType			= ((CNetPartKEY*)pSrc)->m_nKeyPartType;
	m_nKeyPartSubtype		= ((CNetPartKEY*)pSrc)->m_nKeyPartSubtype;
	// jm�no s�t� nebo k�i�ovatky
	m_nTemplateName			= ((CNetPartKEY*)pSrc)->m_nTemplateName;
	m_nComponentName		= ((CNetPartKEY*)pSrc)->m_nComponentName;
	//m_nMinPartHg			= ((CNetPartKEY*)pSrc)->m_nMinPartHg;
	//m_nMaxPartHg			= ((CNetPartKEY*)pSrc)->m_nMaxPartHg;
	// barva - norm�ln� & vypl� zobrazen�
	m_clrNor				= ((CNetPartKEY*)pSrc)->m_clrNor;			
	m_bFillEntire			= ((CNetPartKEY*)pSrc)->m_bFillEntire;
	// existence �sek�
	m_nCountSections		= ((CNetPartKEY*)pSrc)->m_nCountSections;

	for (int i = 0; i < maxSections; ++i)
	{
		m_SectionBegins[i]	= ((CNetPartKEY*)pSrc)->m_SectionBegins[i];
		m_SectionNetType[i]	= ((CNetPartKEY*)pSrc)->m_SectionNetType[i];
	}
	// body s�ov�ho objektu relativn� k A
	m_ptRelBoundingCenter	= ((CNetPartKEY*)pSrc)->m_ptRelBoundingCenter;
	for (int i = 0; i < ptNetCount; ++i)
	{
		m_ptRelNetObjPoints[i] = ((CNetPartKEY*)pSrc)->m_ptRelNetObjPoints[i];
	}

	// sou�adnice
	m_LogPosition.CopyFrom(&((CNetPartKEY*)pSrc)->m_LogPosition);
}

// update registrovan�ch objekt� podle definovan�ch
void CNetPartKEY::UpdateNetsObjectsFromDefObjects(const CMgrNets* pMgr, const CNetPartsList*)
{
	if (m_nKeyPartType == typeCross)
	{	// podle k�i�ovatky
		CPosCrossTemplate* pCross = ((CMgrNets*)pMgr)->GetCrossTemplate(m_nComponentName);
		if (pCross != NULL)
		{
			// v��ka dle default k�i�ovatky
			//m_nMinPartHg = pCross->m_nMinHeight;
			//m_nMaxPartHg = pCross->m_nMaxHeight;
			// barva - norm�ln� & vypl� zobrazen�
			m_clrNor	 = pCross->m_clrNor;
			m_bFillEntire= pCross->m_bFillEntire;
		}
	} 
	else
	{	// podle s�t�
		CPosNetTemplate* pNet = ((CMgrNets*)pMgr)->GetNetTemplate(m_nTemplateName);
		if (pNet != NULL)
		{
			// v��ka dle default s�t�
			//m_nMinPartHg = pNet->m_nMinHeight;
			//m_nMaxPartHg = pNet->m_nMaxHeight;
			// barva - norm�ln� & vypl� zobrazen�
			m_clrNor	 = pNet->m_clrKey;
			m_bFillEntire= pNet->m_bFillEntire;
		}
	}
}

// serializace
void CNetPartKEY::DoSerialize(CArchive& ar, DWORD nVer)
{
	CNetPartBase::DoSerialize(ar, nVer);
	// vlastn� data
	MntSerializeInt(ar, m_nKeyPartType);
	MntSerializeWord(ar, m_nKeyPartSubtype);
	// jm�no s�t� nebo k�i�ovatky
	MntSerializeString(ar,m_nTemplateName);
	MntSerializeString(ar,m_nComponentName);

	if (ar.IsLoading() && nVer < 42)
	{
		double nMinPartHg;
		MntSerializeDouble(ar, nMinPartHg);

		double nMaxPartHg;
		MntSerializeDouble(ar, nMaxPartHg);
	}

	// barva - norm�ln� & vypl� zobrazen�
	MntSerializeColor(ar, m_clrNor);
	MntSerializeBOOL(ar, m_bFillEntire);
	// existence �sek�
	MntSerializeInt(ar, m_nCountSections);
	for (int i = 0; i < m_nCountSections; ++i)
	{
		MntSerializeNetPos(ar,nVer,m_SectionBegins[i]);
		MntSerializeString(ar,	m_SectionNetType[i]);
	}
	// body s�ov�ho objektu relativn� k A
	MntSerializeFloat(ar, m_ptRelBoundingCenter[0]);
	MntSerializeFloat(ar, m_ptRelBoundingCenter[1]);
	MntSerializeFloat(ar, m_ptRelBoundingCenter[2]);
	for (int i = 0; i < ptNetCount; ++i)
	{
		MntSerializeFloat(ar,m_ptRelNetObjPoints[i][0]);
		MntSerializeFloat(ar,m_ptRelNetObjPoints[i][1]);
		MntSerializeFloat(ar,m_ptRelNetObjPoints[i][2]);
	}
}

void CNetPartKEY::DoUpdateFromTemplate(const CPoseidonMap* map)
{
	Ref<CPosObjectTemplate> templ = GetObjectTemplate(map);
	if (templ)
	{
		templ->UpdateParamsFromSourceFile();

		// update section position atd...  TODO  
		// body s�ov�ho objektu relativn� k A
		Vector3 ptA = templ->GetNetPointA();
		m_ptRelBoundingCenter = -ptA; 
	}
}

// vytvo�en� podle �ablony
BOOL CNetPartKEY::CreateFromTemplateCross(const CPosCrossTemplate* pCross)
{
	ASSERT(pCross != NULL);
	ASSERT_KINDOF(CPosCrossTemplate, pCross);
	// typ kl��ov�ho bodu & podtyp dle vzoru
	m_nKeyPartType		= typeCross;
	m_nKeyPartSubtype	= pCross->m_nCrossType;
	// jm�no s�t� nebo k�i�ovatky
	m_nTemplateName.Empty();
	m_nComponentName = pCross->m_sCrossName;
	// v��ka dle default k�i�ovatky
	//m_bAutoHeight	= FALSE;
	//m_nRelaHeight	= (float)pCross->m_nStdHeight;

	//m_nMinPartHg = pCross->m_nMinHeight;
	//m_nMaxPartHg = pCross->m_nMaxHeight;
	// barva - norm�ln� & vypl� zobrazen�
	m_clrNor	 = pCross->m_clrNor;
	m_bFillEntire= pCross->m_bFillEntire;

	// bude vytvo�en podle �ablony objektu
	ASSERT(m_nPosObjectID ==UNDEF_REG_ID);
	ASSERT(m_pCreateObject == NULL);
	ASSERT(pCross->GetObjectTemplate() != NULL);
	m_pCreateObject = pCross->GetObjectTemplate();

	// po�et sekc�
	Vector3 ptA  = pCross->GetObjectTemplate()->GetNetPointA();
	Vector3 ptLB = pCross->GetObjectTemplate()->GetNetPoint(CPosObjectTemplate::ptNetLB);

	m_nCountSections	= 2;	// A,B - v�dy
	// A
	InitRealNetPos(m_SectionBegins[0]);
	m_SectionNetType[0] = pCross->m_sPartsNets[CPosCrossTemplate::netCROSS_A];
	// B
	m_SectionBegins[1] = m_SectionBegins[0];
	m_SectionBegins[1].nPos.z -= CNetBaseStra::GetNormalizeLength(ptA[2]-ptLB[2]);
	m_SectionBegins[1].nGra	  = 180;
	m_SectionNetType[1] = pCross->m_sPartsNets[CPosCrossTemplate::netCROSS_B];

	if ((m_nKeyPartSubtype == CPosCrossTemplate::netCROSS_L) ||
		(m_nKeyPartSubtype == CPosCrossTemplate::netCROSS_K))
	{	// p�id�m C
		Vector3 ptLH = pCross->GetObjectTemplate()->GetNetPoint(CPosObjectTemplate::ptNetLH);

		m_nCountSections++;
		m_SectionBegins[2] = m_SectionBegins[0];
		m_SectionBegins[2].nPos.x -= CNetBaseStra::GetNormalizeLength(ptA[0]-ptLH[0]);
		m_SectionBegins[2].nPos.z -= CNetBaseStra::GetNormalizeLength(ptA[2]-ptLB[2])/2.f;
		m_SectionBegins[2].nGra	  = 270;
		m_SectionNetType[2] = pCross->m_sPartsNets[CPosCrossTemplate::netCROSS_C];
	}
	if ((m_nKeyPartSubtype == CPosCrossTemplate::netCROSS_P)||
		(m_nKeyPartSubtype == CPosCrossTemplate::netCROSS_K))
	{	// p�id�m D
		Vector3 ptPH = pCross->GetObjectTemplate()->GetNetPoint(CPosObjectTemplate::ptNetPH);
		m_SectionBegins[m_nCountSections] = m_SectionBegins[0];
		m_SectionBegins[m_nCountSections].nPos.x += CNetBaseStra::GetNormalizeLength(ptA[0]-ptPH[0]);
		m_SectionBegins[m_nCountSections].nPos.z -= CNetBaseStra::GetNormalizeLength(ptA[2]-ptLB[2])/2.f;
		m_SectionBegins[m_nCountSections].nGra	  = 90;
		m_SectionNetType[m_nCountSections] = pCross->m_sPartsNets[CPosCrossTemplate::netCROSS_D];
		m_nCountSections++;
	}

	// body s�ov�ho objektu relativn� k A
	m_ptRelBoundingCenter = -ptA; 
	m_ptRelNetObjPoints[ptNetLB] = GetRelativPointToA(pCross->GetObjectTemplate()->GetNetPoint(ptNetLB),ptA);
	m_ptRelNetObjPoints[ptNetPB] = GetRelativPointToA(pCross->GetObjectTemplate()->GetNetPoint(ptNetPB),ptA);
	m_ptRelNetObjPoints[ptNetLE] = GetRelativPointToA(pCross->GetObjectTemplate()->GetNetPoint(ptNetLE),ptA);
	m_ptRelNetObjPoints[ptNetPE] = GetRelativPointToA(pCross->GetObjectTemplate()->GetNetPoint(ptNetPE),ptA);
	if ((m_nKeyPartSubtype == CPosCrossTemplate::netCROSS_L) ||
		(m_nKeyPartSubtype == CPosCrossTemplate::netCROSS_K))
	{
		m_ptRelNetObjPoints[ptNetLH] = GetRelativPointToA(pCross->GetObjectTemplate()->GetNetPoint(ptNetLH),ptA);
		m_ptRelNetObjPoints[ptNetLD] = GetRelativPointToA(pCross->GetObjectTemplate()->GetNetPoint(ptNetLD),ptA);
	}
	if ((m_nKeyPartSubtype == CPosCrossTemplate::netCROSS_P) ||
		(m_nKeyPartSubtype == CPosCrossTemplate::netCROSS_K))
	{
		m_ptRelNetObjPoints[ptNetPH] = GetRelativPointToA(pCross->GetObjectTemplate()->GetNetPoint(ptNetPH),ptA);
		m_ptRelNetObjPoints[ptNetPD] = GetRelativPointToA(pCross->GetObjectTemplate()->GetNetPoint(ptNetPD),ptA);
	}

	return TRUE;
}

BOOL CNetPartKEY::CreateFromTemplateStra(const CPosNetTemplate* pNet, const CNetBaseStra* pTempl)
{
	ASSERT(pNet != NULL);
	ASSERT_KINDOF(CPosNetTemplate,pNet);
	// typ kl��ov�ho bodu & podtyp dle vzoru
	m_nKeyPartType		= typeStra;
	m_nKeyPartSubtype	= pTempl->m_nSTRAType;
	// jm�no s�t� nebo k�i�ovatky
	m_nTemplateName		= pNet->m_sNetName;
	m_nComponentName	= pTempl->m_sCompName;
	// v��ka dle default s�t�
	//m_bAutoHeight= FALSE;
	//m_nRelaHeight= (float)pNet->m_nStdHeight;

	//m_nMinPartHg = pNet->m_nMinHeight;
	//m_nMaxPartHg = pNet->m_nMaxHeight;
	// barva - norm�ln� & vypl� zobrazen�
	m_clrNor	 = pNet->m_clrKey;
	m_bFillEntire= pNet->m_bFillEntire;

	// existence �sek� & navazuj�c� body relativn� k A
	ASSERT(pTempl != NULL);
	ASSERT(pTempl->GetObjectTemplate() != NULL);

	if (!pTempl->GetObjectTemplate())
	{
		LogF("CNetPartKEY::CreateFromTemplateStra : template missing");
		return FALSE;
	}

	// bude vytvo�en podle �ablony objektu
	ASSERT(m_nPosObjectID == UNDEF_REG_ID);
	ASSERT(m_pCreateObject == NULL);
	m_pCreateObject = pTempl->GetObjectTemplate();

	m_nCountSections	= 2;	// A,B
	// A
	InitRealNetPos(m_SectionBegins[0]);
	// B
	m_SectionBegins[1] = m_SectionBegins[0];
	m_SectionBegins[1].nPos.z -= pTempl->GetStraLength();
	m_SectionBegins[1].nGra	  = 180;

	// typy s�t� = s�
	m_SectionNetType[0] = pNet->m_sNetName;
	m_SectionNetType[1] = pNet->m_sNetName;

	// body s�ov�ho objektu relativn� k A
	Vector3 ptA = pTempl->GetObjectTemplate()->GetNetPointA();
	m_ptRelBoundingCenter = -ptA; 	
	m_ptRelNetObjPoints[ptNetLB] = GetRelativPointToA(pTempl->GetObjectTemplate()->GetNetPoint(ptNetLB),ptA);
	m_ptRelNetObjPoints[ptNetPB] = GetRelativPointToA(pTempl->GetObjectTemplate()->GetNetPoint(ptNetPB),ptA);
	m_ptRelNetObjPoints[ptNetLE] = GetRelativPointToA(pTempl->GetObjectTemplate()->GetNetPoint(ptNetLE),ptA);
	m_ptRelNetObjPoints[ptNetPE] = GetRelativPointToA(pTempl->GetObjectTemplate()->GetNetPoint(ptNetPE),ptA);

	return TRUE;
}

BOOL CNetPartKEY::CreateFromTemplateSpec(const CPosNetTemplate* pNet, const CNetBaseStra* pTempl)
{
	ASSERT(pNet != NULL);
	ASSERT_KINDOF(CPosNetTemplate, pNet);
	// stejn� jako rovinka
	CreateFromTemplateStra(pNet, pTempl);
	// typ kl��ov�ho bodu & podtyp dle vzoru
	m_nKeyPartType		= typeSpec;
	m_nKeyPartSubtype	= pTempl->m_nSTRAType;
	return TRUE;
}

BOOL CNetPartKEY::CreateFromTemplateTerm(const CPosNetTemplate* pNet, const CNetTERM* pTempl)
{
	ASSERT(pNet != NULL);
	ASSERT_KINDOF(CPosNetTemplate, pNet);
	// typ kl��ov�ho bodu & podtyp dle vzoru
	m_nKeyPartType    = typeTerm;
	m_nKeyPartSubtype = 0;
	// jm�no s�t� nebo k�i�ovatky
	m_nTemplateName  = pNet->m_sNetName;
	m_nComponentName = pTempl->m_sCompName;
	// v��ka dle default s�t�
	//m_bAutoHeight= FALSE;
	//m_nRelaHeight= (float)pNet->m_nStdHeight;

	//m_nMinPartHg = pNet->m_nMinHeight;
	//m_nMaxPartHg = pNet->m_nMaxHeight;
	// barva - norm�ln� & vypl� zobrazen�
	m_clrNor	  = pNet->m_clrKey;
	m_bFillEntire = pNet->m_bFillEntire;

	// existence �sek� & navazuj�c� body relativn� k A
	ASSERT(pTempl != NULL);
	ASSERT(pTempl->GetObjectTemplate() != NULL);

	// bude vytvo�en podle �ablony objektu
	ASSERT(m_nPosObjectID == UNDEF_REG_ID);
	ASSERT(m_pCreateObject == NULL);
	m_pCreateObject = pTempl->GetObjectTemplate();

	m_nCountSections = 1;	// A
	// A
	InitRealNetPos(m_SectionBegins[0]);
	// typy s�t� = s�
	m_SectionNetType[0] = pNet->m_sNetName;

	// body s�ov�ho objektu relativn� k A
	Vector3 ptA = pTempl->GetObjectTemplate()->GetNetPointA();
	m_ptRelBoundingCenter = -ptA; 

	m_ptRelNetObjPoints[ptNetLE] = GetRelativPointToA(pTempl->GetObjectTemplate()->GetNetPoint(ptNetLE), ptA);
	m_ptRelNetObjPoints[ptNetPE] = GetRelativPointToA(pTempl->GetObjectTemplate()->GetNetPoint(ptNetPE), ptA);
	m_ptRelNetObjPoints[ptNetLB] = GetRelativPointToA(pTempl->GetObjectTemplate()->GetNetPoint(ptNetLB), ptA);
	m_ptRelNetObjPoints[ptNetPB] = GetRelativPointToA(pTempl->GetObjectTemplate()->GetNetPoint(ptNetPB), ptA);

	// p�id�m fale�n� ptNetLB, ptNetPB na d�lku objektu
	m_ptRelNetObjPoints[ptNetLB] = m_ptRelNetObjPoints[ptNetLE];
	m_ptRelNetObjPoints[ptNetLB][2] -= (float)pTempl->GetObjectTemplate()->GetDrawHeight();
	m_ptRelNetObjPoints[ptNetPB] = m_ptRelNetObjPoints[ptNetPE];
	m_ptRelNetObjPoints[ptNetPB][2] -= (float)pTempl->GetObjectTemplate()->GetDrawHeight();

	m_SectionBegins[0].nGra = 0;
	m_SectionBegins[0].nPos.x = (m_ptRelNetObjPoints[ptNetLE][0] + m_ptRelNetObjPoints[ptNetPE][0]) / 2;
	m_SectionBegins[0].nPos.z = m_ptRelNetObjPoints[ptNetLE][2];

	return TRUE;
}

// pr�ce se sou�adnicemi objektu

// pro Section vrac� odpov�daj�c� polohu (m_nBaseRealPos seznamu)
REALNETPOS CNetPartKEY::GetRealPosForSection(int nSection, const CPoseidonMap* pMap) const
{
	ASSERT((nSection >= 0) && (nSection < m_nCountSections));
	REALNETPOS rPos = m_nBaseRealPos;
	// p�ipo�tu relativn� polohu
	rPos.nPos.x += m_SectionBegins[nSection].nPos.x;
	rPos.nPos.z += m_SectionBegins[nSection].nPos.z;
	rPos.nPos = CalcRealPosFromRelativePos(m_SectionBegins[nSection].nPos);
	// p�ipo�tu �hel
	rPos.nGra = GetNormalizedGrad(rPos.nGra + m_SectionBegins[nSection].nGra);
	// v��ku nem�n�m - horizont�ln� objekt -> stejn� jako v m_nBaseRealPos

	// v��ku m�nim jen v p��pad�, �e by byly v��kov� odli�n� objekty - zat�m NE
	// rPos.nHgh += m_SectionBegins[nSection].nHgh;
	
	return rPos;
}

void CNetPartKEY::DoUpdateLogPosition(const CPoseidonMap* pMap)
{
	m_LogPosition.ResetPosition();

	switch(m_nKeyPartType)
	{
	case typeStra:
	case typeSpec:
	case typeTerm:
		{
			m_LogPosition.DoAddNewPoint(CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetLE], pMap), FALSE);
			m_LogPosition.DoAddNewPoint(CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetPE], pMap), FALSE);
			m_LogPosition.DoAddNewPoint(CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetPB], pMap), FALSE);
			m_LogPosition.DoAddNewPoint(CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetLB], pMap), TRUE);
		} 
		break;
	case typeCross:
		{
			Vector3 pt; pt[1] = 0.f;
			// LE,PE v�dy
			m_LogPosition.DoAddNewPoint(CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetLE], pMap), FALSE);
			m_LogPosition.DoAddNewPoint(CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetPE], pMap), FALSE);
			// je PH, PD ?
			if ((m_nKeyPartSubtype == CPosCrossTemplate::netCROSS_P) ||
				(m_nKeyPartSubtype == CPosCrossTemplate::netCROSS_K))
			{
				pt[0] = m_ptRelNetObjPoints[ptNetPE][0]; 
				pt[2] = m_ptRelNetObjPoints[ptNetPH][2];
				m_LogPosition.DoAddNewPoint(CalcLogPosFromRelativePoint(pt, pMap), FALSE);
				m_LogPosition.DoAddNewPoint(CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetPH], pMap), FALSE);
				m_LogPosition.DoAddNewPoint(CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetPD], pMap), FALSE);
				pt[0] = m_ptRelNetObjPoints[ptNetPB][0]; 
				pt[2] = m_ptRelNetObjPoints[ptNetPD][2];
				m_LogPosition.DoAddNewPoint(CalcLogPosFromRelativePoint(pt, pMap), FALSE);
			}
			// LB, PB v�dy
			m_LogPosition.DoAddNewPoint(CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetPB],pMap),FALSE);
			m_LogPosition.DoAddNewPoint(CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetLB],pMap),TRUE);
			// je PH, PD ?
			if ((m_nKeyPartSubtype == CPosCrossTemplate::netCROSS_L)||
				(m_nKeyPartSubtype == CPosCrossTemplate::netCROSS_K))
			{
				pt[0] = m_ptRelNetObjPoints[ptNetLB][0]; 
				pt[2] = m_ptRelNetObjPoints[ptNetLD][2];
				m_LogPosition.DoAddNewPoint(CalcLogPosFromRelativePoint(pt,pMap),FALSE);
				m_LogPosition.DoAddNewPoint(CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetLD],pMap),FALSE);
				m_LogPosition.DoAddNewPoint(CalcLogPosFromRelativePoint(m_ptRelNetObjPoints[ptNetLH],pMap),FALSE);
				pt[0] = m_ptRelNetObjPoints[ptNetLE][0]; 
				pt[2] = m_ptRelNetObjPoints[ptNetLH][2];
				m_LogPosition.DoAddNewPoint(CalcLogPosFromRelativePoint(pt,pMap),TRUE);
			}
		}
		break;
	default:
		{
			ASSERT(FALSE);
		}
	}
}

// pr�ce s objekty Poseidon (Update a Vytvo�en�)
BOOL CNetPartKEY::DoUpdatePoseidonNetObject(CPosEdObject* pObject, const CPoseidonMap* pMap)
{
	if (!CNetPartBase::DoUpdatePoseidonNetObject(pObject, pMap)) return FALSE;
	// bouding center (relativn� k A)
	REALPOS rBounding;
	rBounding.x = m_ptRelBoundingCenter[0];
	rBounding.z = m_ptRelBoundingCenter[2];

	rBounding   = CalcRealPosFromRelativePos(rBounding);
	// um�st�m objekt na tuto polohu
	pObject->m_Position(0, 3) = rBounding.x;
	pObject->m_Position(2, 3) = rBounding.z;
	// zarotuji 
	pObject->CalcRotationPosition(-m_nBaseRealPos.nGra);

	// v��ka povrchu v bod� A a ve st�edu
	float nHgA = pMap->GetLandscapeHeight(m_nBaseRealPos.nPos);
	float nHgC = pMap->GetLandscapeHeight(rBounding);
	// relativn� v��ka objektu
	pObject->m_nRelHeight = (float)/*m_nRelaHeight + */(nHgA - nHgC);
	// v�po�et v��ky	
	//m_Position(1,3) = CalcObjectPoseidonHeight(pMap);
	if (pObject->m_pTemplate.NotNull())
	{
		pObject->m_Position(1, 3) = pObject->m_nRelHeight + nHgC + pObject->m_pTemplate->GetBoundingCenter()[1];
	}
	else
	{
		Ref<CPosObjectTemplate> templ = pMap->m_pDocument->GetObjectTemplate(pObject->m_nTemplateID);
		if (templ.NotNull()) pObject->m_Position(1, 3) = pObject->m_nRelHeight + nHgC + templ->GetBoundingCenter()[1];
	}
	// v�po�et nov� logick� polohy objektu
	pObject->CalcObjectLogPosition();
	return TRUE;
}

// vykreslov�n� objektu
void CNetPartKEY::OnDrawObject(CDC* pDC, const CDrawPar* pParams, bool locked) const
{
	COLORREF cColor = m_clrNor;
	if (m_clrNor == DEFAULT_COLOR)
	{
		if (m_nKeyPartType == typeCross)
		{
			cColor = pParams->m_pCfg->m_cDefColors[CPosEdCfg::defcolNetCrs];
		}
		else
		{
			cColor = pParams->m_pCfg->m_cDefColors[CPosEdCfg::defcolNetKey];
		}
	} 

	if (locked) cColor = RGBToY(cColor);

	RECT hull = m_DevPosition.GetPositionHull();
	if ((hull.right - hull.left) < MIN_DRAW_SIZE && (hull.bottom - hull.top) < MIN_DRAW_SIZE)
	{    
		pDC->SetPixelV((hull.right + hull.left) / 2, (hull.top + hull.bottom) / 2, cColor);
		return;
	}

	CBrush br(cColor);
	CRgn   rgn;
	m_DevPosition.CreateMultiRgn(rgn);
	if (m_bFillEntire)
	{
		pDC->FillRgn(&rgn, &br);
	}
	else
	{
		pDC->FrameRgn(&rgn, &br, 1, 1);
	}
}

bool CNetPartKEY::PrepareForPaste(const CPosEdBaseDoc & doc)
{
	if (m_nTemplateName.IsEmpty())
	{
		// crossroad
		CPosCrossTemplate* pTempl = (CPosCrossTemplate*) doc.m_MgrNets.GetCrossTemplate(m_nComponentName);
		if (pTempl)
		{
			m_pCreateObject = pTempl->GetObjectTemplate();
			m_nPosObjectID = -1;
			return true;
		}
		return false;
	}
	else
	{
		CPosNetTemplate* pTempl = (CPosNetTemplate*) doc.m_MgrNets.GetNetTemplate(m_nTemplateName);
		if (!pTempl) return false;

		CNetComponent* pNetComp;

		switch (m_nKeyPartType)
		{
			case typeStra:
				pNetComp = pTempl->GetStraTemplate(m_nComponentName);
				break;
			case typeSpec:
				pNetComp = pTempl->GetSpecTemplate(m_nComponentName);
				break;
			case typeTerm:
				pNetComp = pTempl->GetTermTemplate(m_nComponentName);
				break;
			default:
				return false;
		}

		if (pNetComp)
		{    
			m_pCreateObject = pNetComp->GetObjectTemplate();
			m_nPosObjectID = -1;
			return true;
		}

		return false;
	}
}

bool CNetPartKEY::IsIntersection(const CPosObject& sec) const
{
  return GetLogObjectPosition()->IsIntersection(*sec.GetLogObjectPosition());
}

/////////////////////////////////////////////////////////////////////////////
// CNetPartsList - posloupnost ��st� s�t� (CNetPartSTRA,CNetPartBEND)

IMPLEMENT_DYNAMIC(CNetPartsList,CObject)

CNetPartsList::CNetPartsList()
{
	//m_bAutoHeight = FALSE;
}

CNetPartsList::~CNetPartsList()
{	
	Destroy(); 
}

// nastaven� objektu
void CNetPartsList::Destroy()
{	// ru��m pole objekt�
	DestroyArrayObjects(&m_NetParts);
	m_nTemplateName.Empty();
}

// vytvo�en� kopie objektu
void CNetPartsList::CopyFrom(const CNetPartsList* pSrc)
{
	// likvidace dat
	Destroy();
	// kopie objekt�
	for(int i=0; i<pSrc->m_NetParts.GetSize(); i++)
	{
		CNetPartBase* pPart = (CNetPartBase*)(pSrc->m_NetParts[i]);
		if (pPart)
			m_NetParts.Add(pPart->CreateCopyObject());
	}
	// kopie dat
	m_nTemplateName	= pSrc->m_nTemplateName;
	m_nBaseRealPos	= pSrc->m_nBaseRealPos;
	// barvy s�t�
	m_clrKey		= pSrc->m_clrKey;
	m_clrNor		= pSrc->m_clrNor;
	m_bFillEntire	= pSrc->m_bFillEntire;
	// v��ka d�l�
	//m_nMinHeight	= pSrc->m_nMinHeight;
	//m_nMaxHeight	= pSrc->m_nMaxHeight;
	//m_nStdHeight	= pSrc->m_nStdHeight;
	//m_bAutoHeight	= pSrc->m_bAutoHeight;
	// maxim�ln� sklon & maxim�ln� �hel mezi d�ly
	m_nMaxSlope		= pSrc->m_nMaxSlope;		
	m_nPartSlope	= pSrc->m_nPartSlope;		
};

void CNetPartsList::CreateFromNetTemplate(const CPosNetTemplate* pNet)
{
	ASSERT(pNet);
	// ma�u objekty
	Destroy();
	// jm�no s�t�
	m_nTemplateName = pNet->m_sNetName;
	// barvy s�t�
	m_clrKey		= pNet->m_clrKey;
	m_clrNor		= pNet->m_clrNor;
	m_bFillEntire	= pNet->m_bFillEntire;
	// v��ka dle default s�t�
	//m_nMinHeight	= pNet->m_nMinHeight;
	//m_nMaxHeight	= pNet->m_nMaxHeight;
	//m_nStdHeight	= pNet->m_nStdHeight;
	//m_bAutoHeight	= pNet->m_bAutoHeight;
	// maxim�ln� sklon & maxim�ln� �hel mezi d�ly
	m_nMaxSlope		= pNet->m_nMaxSlope;		
	m_nPartSlope	= pNet->m_nPartSlope;		
};

// update registrovan�ch objekt� podle definovan�ch
void CNetPartsList::UpdateNetsObjectsFromDefObjects(const CMgrNets* pMgr)
{
	CPosNetTemplate* pNet = ((CMgrNets*)pMgr)->GetNetTemplate(m_nTemplateName);
	if (pNet != NULL)
	{
		// barvy s�t�
		m_clrKey		= pNet->m_clrKey;
		m_clrNor		= pNet->m_clrNor;
		m_bFillEntire	= pNet->m_bFillEntire;
		// v��ka dle default s�t�
		//m_nMinHeight	= pNet->m_nMinHeight;
		//m_nMaxHeight	= pNet->m_nMaxHeight;
		//m_nStdHeight	= pNet->m_nStdHeight;
		//m_bAutoHeight	= pNet->m_bAutoHeight;
		// maxim�ln� sklon & maxim�ln� �hel mezi d�ly
		m_nMaxSlope		= pNet->m_nMaxSlope;		
		m_nPartSlope	= pNet->m_nPartSlope;		
	}
	// update objekt�
	for(int i=0; i<m_NetParts.GetSize(); i++)
	{
		CNetPartBase* pPart = (CNetPartBase*)(m_NetParts[i]);
		if (pPart)	pPart->UpdateNetsObjectsFromDefObjects(pMgr,this);
	};
};

// serializace
void CNetPartsList::DoSerialize(CArchive& ar, DWORD nVer)
{
	int i, nCount = 0;

	if (ar.IsStoring())
	{	// ulo�en� objekt�
		nCount = m_NetParts.GetSize(); 
		MntSerializeInt(ar, nCount);
		for (i = 0; i < nCount; ++i)
		{
			CNetPartBase* pPart = (CNetPartBase*)(m_NetParts[i]);
			BOOL bExist = (pPart != NULL);
			MntSerializeBOOL(ar, bExist);
			if (bExist)
			{	ASSERT_KINDOF(CNetPartBase, pPart);
				// serializace typu objektu
				WORD nType = (WORD)(pPart->GetNetPartType());
				MntSerializeWord(ar, nType);
				// serializace objektu
				pPart->DoSerialize(ar, nVer);
			}
		}
	} 
	else
	{	// na�ten� objekt�
		MntSerializeInt(ar, nCount);
		for (i = 0; i < nCount; ++i)
		{
			BOOL bExist = FALSE;
			MntSerializeBOOL(ar, bExist);
			if (bExist)
			{	// serializace typu
				WORD nType; 
				MntSerializeWord(ar, nType);
				// vytvo�en� objektu
				CNetPartBase* pPart = CNetPartBase::CreateObjectType((int)nType);
				if (pPart == NULL) AfxThrowMemoryException();
				// serializace objektu
				pPart->DoSerialize(ar, nVer);
				m_NetParts.Add(pPart);
			} 
			else
			{
				m_NetParts.Add(NULL);
			}
		}
	}
	// serializace dat
	MntSerializeString(ar, m_nTemplateName);

	// poloha
	MntSerializeNetPos(ar, nVer, m_nBaseRealPos);
	// barvy s�t�
	MntSerializeColor(ar, m_clrKey);
	MntSerializeColor(ar, m_clrNor);
	MntSerializeBOOL(ar, m_bFillEntire);

	// v��ka d�l�
	if (ar.IsLoading() && nVer < 42)
	{  
		double nMinHeight;
		MntSerializeDouble(ar, nMinHeight);
		double nMaxHeight;
		MntSerializeDouble(ar, nMaxHeight);
		double nStdHeight;
		MntSerializeDouble(ar, nStdHeight);
		if (nVer >= 27)
		{
			BOOL bAutoHeight;
			MntSerializeBOOL(ar, bAutoHeight);
		}
	}

	// maxim�ln� sklon & maxim�ln� �hel mezi d�ly
	if (nVer >= 23)
	{
		MntSerializeDouble(ar, m_nMaxSlope);
		MntSerializeDouble(ar, m_nPartSlope);
	} 
	else
	{
		m_nMaxSlope = 25.;
		m_nPartSlope= 5.;
	}

	// update objekt�
	if (!ar.IsStoring())
	{
		for (int i = 0; i < m_NetParts.GetSize(); ++i)
		{
			CNetPartBase* pPart = (CNetPartBase*)(m_NetParts[i]);
			if (pPart) pPart->UpdateNetsObjectsFromDefObjects(NULL, this);
		}
	}
}

void CNetPartsList::DoUpdateFromTemplate(const CPoseidonMap* map)
{
	int nCount = m_NetParts.GetSize(); 
	for (int i = 0; i < nCount; ++i)
	{
		CNetPartBase* pPart = (CNetPartBase*)(m_NetParts[i]);
		if (pPart == NULL) continue;
		ASSERT_KINDOF(CNetPartBase, pPart);
		pPart->DoUpdateFromTemplate(map);
	}
}

// informace o poloze objektu
void CNetPartsList::RecalcDevObjectPositionFromLP(CDC* pDC)
{
	for (int i = 0; i < m_NetParts.GetSize(); ++i)
	{
		CNetPartBase* pPart = (CNetPartBase*)(m_NetParts[i]);
		if (pPart)pPart->RecalcDevObjectPositionFromLP(pDC);
	}
}

void CNetPartsList::ResetDevObjectPosition()
{
	for(int i = 0; i < m_NetParts.GetSize(); ++i)
	{
		CNetPartBase* pPart = (CNetPartBase*)(m_NetParts[i]);
		if (pPart) ((CPositionBase*)(pPart->GetDevObjectPosition()))->ResetPosition();
	}
}

// pr�ce se sou�adnicemi objektu
void CNetPartsList::GetObjectHull(CRect& rHull) const
{
	if (m_NetParts.GetSize() <= 0)
	{
		rHull.SetRect(INT_MAX,INT_MAX,INT_MAX,INT_MAX);
		return;
	}
	CNetPartBase* pPart = (CNetPartBase*)(m_NetParts[0]);
	if (pPart)
		rHull = pPart->GetLogObjectPosition()->GetPositionHull();
	CRect rPom;
	for(int i=1; i<m_NetParts.GetSize(); i++)
	{
		CNetPartBase* pPart = (CNetPartBase*)(m_NetParts[i]);
		if (pPart)
		{
			rPom = pPart->GetLogObjectPosition()->GetPositionHull();
			if (rHull.left   > rPom.left)
				rHull.left   = rPom.left;
			if (rHull.right  < rPom.right)
				rHull.right  = rPom.right;
			if (rHull.top    > rPom.top)
				rHull.top    = rPom.top;
			if (rHull.bottom < rPom.bottom)
				rHull.bottom = rPom.bottom;
		};
	};
};

BOOL CNetPartsList::IsPointAtArea(const POINT& pt) const
{
	for(int i=0; i<m_NetParts.GetSize(); i++)
	{
		CNetPartBase* pPart = (CNetPartBase*)(m_NetParts[i]);
		if (pPart)
		{
			if (pPart->IsPointAtArea(pt))
				return TRUE;
		};
	};
	return FALSE;
};

bool CNetPartsList::IsIntersection(const CPosObject& sec) const
{
  for(int i=0; i<m_NetParts.GetSize(); i++)
  {
    CNetPartBase* pPart = (CNetPartBase*)(m_NetParts[i]);
    if (pPart)
    {
      if (pPart->IsIntersection(sec))
        return true;
    };
  };
  return false;

}

// pr�ce s objekty Poseidon (Update a Vytvo�en�)
BOOL CNetPartsList::DoUpdatePoseidonNetObjects(CObArray* ExistObjs,const CPoseidonMap* pMap)
{
	for(int i=0; i<m_NetParts.GetSize(); i++)
	{
		CNetPartBase* pPart = (CNetPartBase*)(m_NetParts[i]);
		if (pPart)
		{
			if (!pPart->DoUpdatePoseidonNetObjects(ExistObjs,pMap))
				return FALSE;
		};
	};
	return TRUE;
};

BOOL CNetPartsList::DoCreatePoseidonNetObjects(CObArray* CreateObjs, const CPoseidonMap* pMap)
{
	for (int i = 0; i < m_NetParts.GetSize(); ++i)
	{
		CNetPartBase* pPart = (CNetPartBase*)(m_NetParts[i]);
		if (pPart)
		{
			if (!pPart->DoCreatePoseidonNetObject(CreateObjs, pMap)) return FALSE;
		}
	}
	return TRUE;
}

// pr�ce se sou�adnicemi objektu
void CNetPartsList::DoUpdateNetPartsBaseRealPos(REALNETPOS BasePos, const CPoseidonMap* pMap)
{
	m_nBaseRealPos = BasePos;
	for (int i = 0; i < m_NetParts.GetSize(); ++i)
	{
		CNetPartSIMP* pPart = (CNetPartSIMP*)(m_NetParts[i]);
		if (pPart)
		{	
			ASSERT_KINDOF(CNetPartSIMP, pPart);
			// spo��t�m BasePos pro A
			BasePos = pPart->CalcABasePosition(BasePos, pMap);
			// nastav�m novou m_nBaseRealPos
			//pPart->m_nBaseRealPos = BasePos;
			// nastav�m v��ku na mapu
			//if (m_bAutoHeight)
			{
				pPart->m_nConnRealPos.nHgt = pMap->GetLandscapeHeight(pPart->m_nConnRealPos.nPos);
				pPart->m_nBaseRealPos.nHgt = pMap->GetLandscapeHeight(pPart->m_nBaseRealPos.nPos);
			}
		}
	}
}

REALNETPOS CNetPartsList::GetNetPartsEndRealPos()
{
	REALNETPOS Pos = m_nBaseRealPos;
	if (m_NetParts.GetSize() > 0)
	{
		for (int i = m_NetParts.GetSize() - 1; i >= 0; --i)
		{
			CNetPartSIMP* pPart = (CNetPartSIMP*)(m_NetParts[i]);
			if (pPart)
			{
				Pos  = pPart->GetEndRealPos();
				return Pos;
			}
		}
	}
	return Pos;
}

double CNetPartsList::GetEndNetSlope() const
{
	double nSlope = 0.;
	if(m_NetParts.GetSize() > 0)
	{
		for(int i=m_NetParts.GetSize()-1;i>=0;i--)
		{
			CNetPartSIMP* pPart = (CNetPartSIMP*)(m_NetParts[i]);
			if (pPart)
			{
				nSlope  = pPart->GetCurrentSlopeRad();
				return nSlope;
			}
		}
	}
	return nSlope;
};

void CNetPartsList::DoUpdateLogPosition(const CPoseidonMap* pMap)
{
	for(int i=0; i<m_NetParts.GetSize(); i++)
	{
		CNetPartBase* pPart = (CNetPartBase*)(m_NetParts[i]);
		if (pPart)	pPart->DoUpdateLogPosition(pMap);
	};
};

// vykreslov�n� objektu
void CNetPartsList::OnDrawObject(CDC* pDC,const CDrawPar* pParams, bool locked) const
{
	COLORREF cColor = m_clrNor;
	if (m_clrNor == DEFAULT_COLOR)
	{
		cColor = pParams->m_pCfg->m_cDefColors[CPosEdCfg::defcolNetNor];
	}
  if (locked)
    cColor = RGBToY(cColor);

	CBrush br(cColor);

	for(int i=0; i<m_NetParts.GetSize(); i++)
	{
		CNetPartSIMP* pPart = (CNetPartSIMP*)(m_NetParts[i]);
		if (pPart)	pPart->OnDrawObject(pDC,pParams,&br,m_bFillEntire);
	};
};


bool CNetPartsList::PrepareForPaste(const CPosEdBaseDoc& doc)
{
  CPosNetTemplate * pTempl = doc.m_MgrNets.GetNetTemplate(m_nTemplateName);
  if (!pTempl)
    return false;

  for(int i=0; i<m_NetParts.GetSize(); i++)
  {
    CNetPartSIMP* pPart = (CNetPartSIMP*)(m_NetParts[i]);
    
    if (pPart)
    {    
      CNetComponent * pNetComp;

      switch(pPart->GetNetPartType())
      {
      case CNetPartBase::netPartSTRA:               
        pNetComp = (CNetComponent *) pTempl->GetStraTemplate(pPart->m_nComponentName);
        break;
      case CNetPartBase::netPartBEND:
        pNetComp = (CNetComponent *) pTempl->GetBendTemplate(pPart->m_nComponentName);
        break;
      case CNetPartBase::netPartSPEC:
        pNetComp = (CNetComponent *) pTempl->GetSpecTemplate(pPart->m_nComponentName);
        break;
      case CNetPartBase::netPartKEY:
        {
          CNetPartKEY * pKey = (CNetPartKEY *) pPart;
          pKey->PrepareForPaste(doc);
          continue;
        }
      }
      if (!pNetComp)
        return false;

      pPart->m_nPosObjectID = UNDEF_REG_ID;
      pPart->m_pCreateObject = pNetComp->GetObjectTemplate();
    }    
  };
  return true;
}

bool CNetPartsList::GetWalkPoint(NetWalkPoint& wpt) const
{
  //if (m_NetParts.GetSize() == 0) // changed on Rasta request, return all walk points also ones without part... 
  //  return false;

  wpt._pos = m_nBaseRealPos.nPos;
  wpt._grad = (float) -m_nBaseRealPos.nGra;
  wpt._partIndx = 0;
  return true;
}

bool CNetPartsList::AdvanceWalkPoint(NetWalkPoint& wpt, float dist) const 
{
  if (dist <= 0)
    return true;

  wpt._partIndx--;
  while (dist > 0 && ++wpt._partIndx < m_NetParts.GetSize())
  {
    CNetPartBase * part = (CNetPartBase *) m_NetParts[wpt._partIndx];
    part->AdvanceWalkPoint(wpt, dist);
  }
  return (dist == 0);
}



///////////////////////////////////////////////////////////////////////////////
// testov�n� d�lu
WORD CNetPartSIMP::DoTestNetPart_All(REALNETPOS nPrvHg,double nPrvSl,const CNetPartsList* pList,const CPoseidonMap* pMap) const
{
	ASSERT(pList != NULL);
	WORD nSate = tstStateOK;

	/*if ((pList != NULL)&&(!pList->m_bAutoHeight))
	{	// nem� p�ilnavost -> mus�m kontrolovat v��ku
		if (!DoTestNetPart_PrvHg(nPrvHg,pList))
			nSate |= tstErrBadPrvHg;
		if (!DoTestNetPart_NetUp(pList,pMap))
			nSate |= tstErrBadNetUp;
		if (!DoTestNetPart_NetDn(pList,pMap))
			nSate |= tstErrBadNetDn;
	};*/
	// sklon
	if (!DoTestNetPart_Slope(pList))
		nSate |= tstErrBadSlope;
	// p�ilnavost
	if (!DoTestNetPart_PrvSl(nPrvSl,pList))
		nSate |= tstErrBadPrvSl;
	return nSate;
};
BOOL CNetPartSIMP::DoTestNetPart_PrvHg(REALNETPOS nPrvHg,const CNetPartsList* pList) const
{
	if (fabs(nPrvHg.nHgt-m_nConnRealPos.nHgt) > NET_PART_HEIGHT_TOLERANCE)
		return FALSE;
	return TRUE;
};
BOOL CNetPartSIMP::DoTestNetPart_Slope(const CNetPartsList* pList) const
{
	double nSlope = fabs(GetCurrentSlopeRad());
	double nMaxSl = ((6.28318530718*pList->m_nMaxSlope)/360.);
	if (nSlope > nMaxSl)
		return FALSE;
	return TRUE;
};
BOOL CNetPartSIMP::DoTestNetPart_PrvSl(double nPrvSl,const CNetPartsList* pList) const
{
	double nSlope = fabs(nPrvSl - GetCurrentSlopeRad());
	double nMaxSl = ((6.28318530718*pList->m_nPartSlope)/360.);
	if (nSlope > nMaxSl)
		return FALSE;
	return TRUE;
};


BOOL CNetPartSIMP::DoTestNetPart_NetUp(const CNetPartsList* pList,const CPoseidonMap* pMap) const
{
	// poloha krajin�ch bod� - n�sp�
	REALPOS ptLB,ptPB,ptLE,ptPE;
	ptLB = CalcBankRealPosition(ptNetLB);
	ptPB = CalcBankRealPosition(ptNetPB);
	ptLE = CalcBankRealPosition(ptNetLE);
	ptPE = CalcBankRealPosition(ptNetPE);
	// v��ka v krajin�ch bodech
//	LANDHEIGHT hLB,hPB,hLE,hPE;

	// vodorovn� -> jednodu��� p��pad
		// v��ka cel�ho povrchu je m_nBaseRealPos.nHgt
	// naklon�n� d�l
		// v��ka v bod� A je m_nBaseRealPos.nHgt
		// v��ka v bod� B je m_nConnRealPos.nHgt

	// testuji, zda krajn� body jsou nad povrchem
/*	if ((m_nConnRealPos.nHgt - (hLB = pMap->GetLandscapeHeight(ptLB))) > pList->m_nMaxHeight) return FALSE;
	if ((m_nConnRealPos.nHgt - (hPB = pMap->GetLandscapeHeight(ptPB))) > pList->m_nMaxHeight) return FALSE;
	if ((m_nBaseRealPos.nHgt - (hLE = pMap->GetLandscapeHeight(ptLE))) > pList->m_nMaxHeight) return FALSE;
	if ((m_nBaseRealPos.nHgt - (hPE = pMap->GetLandscapeHeight(ptPE))) > pList->m_nMaxHeight) return FALSE;
*/
	return TRUE;
};
BOOL CNetPartSIMP::DoTestNetPart_NetDn(const CNetPartsList* pList,const CPoseidonMap* pMap) const
{
	// poloha krajin�ch bod�
	REALPOS ptLB,ptPB,ptLE,ptPE;
	ptLB = CalcRealPosFromRelativePos(m_ptRelNetObjPoints[ptNetLB]);
	ptPB = CalcRealPosFromRelativePos(m_ptRelNetObjPoints[ptNetPB]);
	ptLE = CalcRealPosFromRelativePos(m_ptRelNetObjPoints[ptNetLE]);
	ptPE = CalcRealPosFromRelativePos(m_ptRelNetObjPoints[ptNetPE]);
	// v��ka v krajin�ch bodech
//	LANDHEIGHT hLB,hPB,hLE,hPE;

	// vodorovn� -> jednodu��� p��pad
		// v��ka cel�ho povrchu je m_nBaseRealPos.nHgt
	// naklon�n� d�l
		// v��ka v bod� A je m_nBaseRealPos.nHgt
		// v��ka v bod� B je m_nConnRealPos.nHgt

	// testuji, zda krajn� body jsou nad povrchem
/*	if ((m_nConnRealPos.nHgt - (hLB = pMap->GetLandscapeHeight(ptLB))) < pList->m_nMinHeight) return FALSE;
	if ((m_nConnRealPos.nHgt - (hPB = pMap->GetLandscapeHeight(ptPB))) < pList->m_nMinHeight) return FALSE;
	if ((m_nBaseRealPos.nHgt - (hLE = pMap->GetLandscapeHeight(ptLE))) < pList->m_nMinHeight) return FALSE;
	if ((m_nBaseRealPos.nHgt - (hPE = pMap->GetLandscapeHeight(ptPE))) < pList->m_nMinHeight) return FALSE;*/


/*
	if ((!CanChangeSlope())||(DisChangeSlope())||
		(m_nConnRealPos.nHgt == m_nBaseRealPos.nHgt))
	{	// vodorovn� -> jednodu��� p��pad
		// v��ka cel�ho povrchu je m_nBaseRealPos.nHgt

	}
	// naklon�n� d�l
	// v��ka v bod� A je m_nBaseRealPos.nHgt
	// v��ka v bod� B je m_nConnRealPos.nHgt
*/

	return TRUE;
};
