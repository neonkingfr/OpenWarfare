/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPosGroupObject

IMPLEMENT_DYNCREATE(CPosGroupObject,CPosObject)

CPosGroupObject::CPosGroupObject()
{	
  m_LogPosition = new CPositionGroup();
  m_DevPosition = new CPositionGroup();
};

CPosGroupObject::CPosGroupObject(const CPoseidonMap& map) : base(map)
{	
  m_LogPosition = new CPositionGroup();
  m_DevPosition = new CPositionGroup();
};

CPosGroupObject::CPosGroupObject(const CPosGroupObject& src) : base(*src._map)
{
  m_LogPosition = new CPositionGroup();
  m_DevPosition = new CPositionGroup();

  CopyFrom(&src);
}


CPosGroupObject::~CPosGroupObject()
{	// pr�ce s vybran�mi objekty
	DestroyGroup();
};

void CPosGroupObject::CopyFrom(const CPosObject* pSrc)
{
  // vypr�zdn�n� skupiny
  DestroyGroup();

	ASSERT_KINDOF(CPosGroupObject,pSrc);
  base::CopyFrom(pSrc);

	// kopie dat
	for(int i=0;i<((CPosGroupObject*)pSrc)->m_SelObjects.GetSize(); i++)
	{
		m_SelObjects.Add(((CPosGroupObject*)pSrc)->m_SelObjects[i]);
	};

};

// serializace
void CPosGroupObject::DoSerialize(CArchive& ar, DWORD nVer)
{
	ASSERT(FALSE);
};



// informace o objektu
CString sGroupObjName;
void CPosGroupObject::GetObjectName(CString& sText) const
{	
	if (sGroupObjName.GetLength() == 0)
		sGroupObjName.LoadString(IDS_GROUP_OBJ_NAME);
	sText = sGroupObjName;
};


BOOL CPosGroupObject::IsEnabledObject(const EnabledObjects& enabled) const
{	// skupina je v�dy vid�t
	return TRUE;
};

// vykreslov�n� objektu
void CPosGroupObject::OnDrawObject(CDC* pDC,const CDrawPar* pParams) const
{
	ASSERT(FALSE);	// objekt se nevykresluje
};

// pr�ce s vybran�mi objekty
void CPosGroupObject::DestroyGroup()
{	m_SelObjects.RemoveAll();
  m_LogPosition->ResetPosition();
  m_DevPosition->ResetPosition();
};

int	CPosGroupObject::GetGroupSize() const
{	return m_SelObjects.GetSize(); };

BOOL CPosGroupObject::IsObjectAtGroup(const CObject* pObj,int* pIndx) const
{
	for(int i=0;i<m_SelObjects.GetSize(); i++)
	{
		if (m_SelObjects[i] == pObj)
		{
			if (pIndx != NULL)
				(*pIndx) = i;
			return TRUE;
		}
	}
	return FALSE;
};

void CPosGroupObject::AddObjectGroup(CObject* pObj, bool exclusive)
{
	if (!exclusive || !IsObjectAtGroup(pObj,NULL))
	{
		// vlo��m objekt
		m_SelObjects.Add(pObj);

    //++FIX 30.7.2003 Fehy, do it iteratively 
    // update polohy
    if (pObj)
      GetLogObjectPosition()->DoAddNewPos(((CPosObject*)pObj)->GetLogObjectPosition());
	  //UpdateLogPosition();
    //--FIX
	}
};

void CPosGroupObject::DelObjectGroup(CObject* pObj)
{
	int nIndx;
	if (IsObjectAtGroup(pObj,&nIndx))
	{
		// vyjmu objekt
		m_SelObjects.RemoveAt(nIndx);
		// update polohy
		UpdateLogPosition();
	}
};

void CPosGroupObject::UpdateLogPosition()
{
	GetLogObjectPosition()->ResetPosition();
	// polohy objekt�
	for(int i=0;i<m_SelObjects.GetSize(); i++)
	{
		CPosObject* pObj = (CPosObject*)(m_SelObjects[i]);
		if (pObj)
		{
			GetLogObjectPosition()->DoAddNewPos(pObj->GetLogObjectPosition());
		}
	}
};

const CPosGroupObject& CPosGroupObject::operator=(const CPosGroupObject& src)
{
  m_SelObjects.Copy(src.m_SelObjects);
  UpdateLogPosition();
  return *this;
}




