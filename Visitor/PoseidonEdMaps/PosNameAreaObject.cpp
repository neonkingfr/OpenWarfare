/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPosEdObject

IMPLEMENT_DYNCREATE(CPosNameAreaObject,CPosAreaObject)

CPosNameAreaObject::CPosNameAreaObject()
{
	m_nAreaID		= UNDEF_REG_ID;
	m_bAreaVisible	= TRUE;
};

CPosNameAreaObject::CPosNameAreaObject(const CPoseidonMap& map) : base(map)
{
  m_nAreaID		= UNDEF_REG_ID;
  m_bAreaVisible	= TRUE;
}

void CPosNameAreaObject::CopyFrom(const CPosObject* pSrc)
{
	ASSERT_KINDOF(CPosNameAreaObject,pSrc);
	// kopie dat	
	CPosAreaObject::CopyFrom(pSrc);
	m_nAreaID		= ((CPosNameAreaObject*)pSrc)->m_nAreaID;
	m_bAreaVisible	= ((CPosNameAreaObject*)pSrc)->m_bAreaVisible;
};

// serializace
void CPosNameAreaObject::DoSerialize(CArchive& ar, DWORD nVer)
{
	CPosAreaObject::DoSerialize(ar, nVer);
	// serializace dat lesa
	MntSerializeInt(ar, m_nAreaID);
	MntSerializeInt(ar, m_bAreaVisible);
};

/////////////////////////////////////////////////////////////////////////////
// informace o objektu

BOOL CPosNameAreaObject::IsEnabledObject(const EnabledObjects& enabled) const
{	return (enabled.m_bNameArea && m_bAreaVisible && !_hidden); };

/////////////////////////////////////////////////////////////////////////////
// editace objektu

BOOL CPosNameAreaObject::DoEditObject()
{	return TRUE; };

int CPosNameAreaObject::SetFreeID() 
{
  return m_nAreaID = _map->GetFreeNameAreaID();
};



