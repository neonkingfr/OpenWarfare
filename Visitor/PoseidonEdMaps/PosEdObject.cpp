/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include <math.h>
#include <El/Math/math3d.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CPosEdObject

IMPLEMENT_DYNCREATE(CPosEdObject,CPosObject)
//IMPLEMENT_DYNAMIC(CPosEdObject,CPosObject)

CPosEdObject::CPosEdObject(const CPoseidonMap& map) : base(map) 
{	
	m_nObjectID	= UNDEF_REG_ID;
	m_nTemplateID = UNDEF_REG_ID;
	// master objekty - nejsou
	m_nMasterWood = UNDEF_REG_ID;
	m_nMasterNet  = UNDEF_REG_ID;
	m_nRelHeight  = 0.f;
	m_nSizeScale  = 1.;
};

void CPosEdObject::CopyFrom(const CPosObject* pSrc)
{
  CPosObject::CopyFrom(pSrc);
	ASSERT_KINDOF(CPosEdObject,pSrc);
  _map = ((CPosEdObject*)pSrc)->_map;
	// kopie dat
	m_nObjectID		= ((CPosEdObject*)pSrc)->m_nObjectID;
	m_sObjName		= ((CPosEdObject*)pSrc)->m_sObjName;	
	m_Position = ((CPosEdObject*)pSrc)->m_Position;
	m_nSizeScale	= ((CPosEdObject*)pSrc)->m_nSizeScale;
	m_nRelHeight	= ((CPosEdObject*)pSrc)->m_nRelHeight;

	// master objekty
	m_nMasterWood	= ((CPosEdObject*)pSrc)->m_nMasterWood;
	m_nMasterNet	= ((CPosEdObject*)pSrc)->m_nMasterNet;
	// �ablonu kop�ruji - plati po celou dobu existence
	m_nTemplateID		= ((CPosEdObject*)pSrc)->m_nTemplateID;
	// sou�adnice objektu	
};

// serializace
void CPosEdObject::DoSerialize(CArchive& ar, DWORD nVer)
{
  base::DoSerialize(ar,nVer);
	MntSerializeInt(ar, m_nObjectID);
 
  if (ar.IsLoading() && nVer < 48)
	  MntSerializeString(ar, m_tempString);
	// serializace matice
    
	  for(int i=0; i<3; i++)
		  for(int j=0; j<4; j++)
      {    
			  MntSerializeFloat(ar, m_Position(i,j));      
      }

      if (nVer <= 50)
      { /// read remaining 4 floats
        float temp;
        MntSerializeFloat(ar, temp);
        MntSerializeFloat(ar, temp);
        MntSerializeFloat(ar, temp);
        MntSerializeFloat(ar, temp);
      };

	// pom�r velikosti
	if (nVer >= 18)
	{
		MntSerializeDouble(ar, m_nSizeScale);
		MntSerializeString(ar, m_sObjName);
	}
	if (nVer >= 21)
		MntSerializeFloat(ar, m_nRelHeight);

	// master objekty
	if (nVer >= 12)
		MntSerializeInt(ar, m_nMasterWood);
	if (nVer >= 20)
		MntSerializeInt(ar, m_nMasterNet);

  if (nVer > 36)
  {
    MntSerializeInt(ar, m_nTemplateID);
    if (ar.IsLoading())
      AllocPositions();
  }

  CalcObjectPoseidonHeight();

  

#if 1 // repair wrong coords
  bool wrong = false; 
  for(int i=0; i<3 && !wrong; i++)
    for(int j=0; j<3; j++)
    {            
      if (!_finite(m_Position(i,j)))
      {
        wrong = true;
        break;
      }
    }

    if (wrong)
    {       
      LogF("Warning: Infinite orientation in object %s ID %d. Replaced by identity", GetBuldozerFileName(), m_nObjectID);
      for(int i=0; i<3; i++)
        for(int j=0; j<3; j++)
        {            
          m_Position(i,j) = i == j? 1.0f : 0.0f;       
        }
    }

    if (!_finite(m_Position(0,3)) || !_finite(m_Position(2,3)) )
    {     
      LogF("Warning: Infinite position (%lf %lf %lf)in object %s ID %d. Cannot be solved.", m_Position(0,3), m_Position(1,3), m_Position(2,3), GetBuldozerFileName(), m_nObjectID);
    }
    else
    {    
      if (!_finite(m_Position(1,3)))
      {
        if (!_finite(m_nRelHeight))
        {          
          LogF("Warning: Infinite position (%lf %lf %lf) and also relativeHeight, object %s ID %d. Cannot be solved.", m_Position(0,3), m_Position(1,3), m_Position(2,3), GetBuldozerFileName(), m_nObjectID);
        }
        else
        {          
          LogF("Warning: Infinite position (%lf %lf %lf) object %s ID %d. Solved.", m_Position(0,3), m_Position(1,3), m_Position(2,3), GetBuldozerFileName(), m_nObjectID);
          m_Position(1,3) = 0;
          CalcObjectPoseidonHeight();
        }
      }
    }

    if (!_finite(m_nSizeScale))
    {
      m_nSizeScale = 1;     
      LogF("Warning: Infinite scale, object %s ID %d. Changed to 1", GetBuldozerFileName(), m_nObjectID);
    }
#endif

};

// informace o poloze objektu


// informace o objektu
void CPosEdObject::GetObjectName(CString& sText) const
{	 
	sText = m_sObjName;	
}

CString	CPosEdObject::GetStatusInfo() const 
{
	CString sText; 
	Ref<CPosObjectTemplate> teml = GetTemplate();
	if (teml)
	{
		sText = teml->GetName();
	}
   
	if (!m_sObjName.IsEmpty())
	{
		sText += " [";
		sText += m_sObjName;
		sText += "]";
	}

	return sText;
}

BOOL CPosEdObject::IsEnabledObject(const EnabledObjects& enabled) const
{
	if (Hidden()) return FALSE;

	// master objekt nen� viditeln�
	if (m_nMasterWood != UNDEF_REG_ID) return FALSE;
	if (m_nMasterNet  != UNDEF_REG_ID) return FALSE;

	if (m_pTemplate != NULL)
	{
		switch(m_pTemplate->m_nObjType)
		{
		case CPosObjectTemplate::objTpUndef:    return enabled.m_bUnknowObjcs;
		case CPosObjectTemplate::objTpNatural:  return enabled.m_bNatureObjcs;
		case CPosObjectTemplate::objTpPeople:   return enabled.m_bPeopleObjcs;
		case CPosObjectTemplate::objTpNewRoads: return enabled.m_bNewRoadsObjcs;
		}
	}
	return FALSE;
}

BOOL CPosEdObject::IsSameObject(const CPosObject & obj) const
{
	if (!obj.IsKindOf(RUNTIME_CLASS(CPosEdObject))) return FALSE;

	return (((const CPosEdObject&) obj).m_nObjectID == m_nObjectID);
}

// realativn� cesta k souboru pac (m_sObjFile)
CString& CPosEdObject::GetBuldozerFileName(CString& sFile) const 
{	
	Ref<CPosObjectTemplate> tmp = GetTemplate();
	if (tmp)
	{
		tmp->GetBuldozerFileName(sFile);
	}
	else
	{
		Assert(FALSE);
	}
	return sFile;
}

// m_sObjFile z rel. cesty buldozeru
/*void CPosEdObject::SetFileFromBuldozer(LPCTSTR pPath)	 
{
	CString sName = pPath;
	int nLen = sName.GetLength()-1;
	while((nLen >= 0)&&(sName[nLen]!=_T('\\'))&&(sName[nLen]!=_T('/')))
		nLen--;
	m_sObjFile  = (LPCTSTR)(sName.GetBuffer(0)+(nLen+1));
};*/

// vlo�� ID,jm�no a polohu objektu do zpr�vy



void CPosEdObject::SetObjectDataToMessage(SObjectPosMessData& sMsg) const
{	
	sMsg.nID	= m_nObjectID;	
	for(int i=0;i<3;i++)
		for(int j=0;j<4;j++)
			sMsg.Position[i][j] = m_Position(i,j);
};

void CPosEdObject::SetObjectDataToMessage(SMoveObjectPosMessData& sMsg) const
{
  sMsg.nID	= m_nObjectID;
  for(int i=0;i<3;i++)
    for(int j=0;j<4;j++)
      sMsg.Position[i][j] = m_Position(i,j);
};

// vytvo�en� z buldozeru
void CPosEdObject::CreateFromBuldozerMsg(const SPosMessage& sMsg)
{
  ASSERT(FALSE);
	/*ASSERT(sMsg._iMsgType == OBJECT_CREATE);
	// soubor
  MESSAGE_DATA_CONST_VAR(OBJECT_CREATE, &sMsg, msgData)

	//SetFileFromBuldozer(msgData.szName);
	// �ablona
	CPosObjectTemplate* pTempl = pMap->GetRegisteredObject(m_sObjFile);
	ASSERT(pTempl != NULL);
	m_pTemplate = pTempl;
	m_sObjFile  = m_pTemplate->m_sObjFile;
	// kop�ruji pizici z ud�losti
	for(int i=0;i<4;i++)
		for(int j=0;j<4;j++)
			m_Position(i,j)= msgData.Position[i][j];
	// ID z ud�losti
	m_nObjectID = msgData.nID;
  */
};

void CPosEdObject::AllocPositions()
{
	Ref<CPosObjectTemplate> templ = GetTemplate();
  
	if (!templ) return;

	switch (templ->GetPosType())
	{
	case CPosObjectTemplate::posTypePoly:
		{
			m_LogPosition = new CPositionPoly();
			m_DevPosition = new CPositionPoly();
			break;
		}
	case CPosObjectTemplate::posTypeEllipse:
		{
/*
			m_LogPosition = new CPositionEllipse();
			m_DevPosition = new CPositionEllipse();
*/
			m_LogPosition = new CPositionPoly();
			m_DevPosition = new CPositionPoly();
			break;
		}
	}; 
}

// vytvo�en� ze �ablony na pozici
void CPosEdObject::CreateFromTemplateAt(Ref<CPosObjectTemplate> pTempl,REALPOS rPos)
{
	ASSERT(pTempl != NULL);
	m_pTemplate = pTempl;
  m_nTemplateID = pTempl->m_ID;

  AllocPositions();

	// v�po�et matice dle polohy
  m_Position = M4Zero;
	
	// randomizace objektu
	if ((pTempl->m_bRandomSize)||(pTempl->m_bRandomVert)||(pTempl->m_bRandomRota))
	{
		RandomFromTemplateAt(pTempl,rPos);
	} else
	{
		// jednotkov� matice
    m_Position = M4Identity;
		
		// poloha objektu
		m_Position(0,3) = rPos.x;
		m_Position(2,3) = rPos.z;
		CalcObjectPoseidonHeight();
	};
  //prepare positions  
    // ID neni ur�eno
  m_nObjectID = UNDEF_REG_ID;
};

void CPosEdObject::CreateFromTemplateAt(Ref<CPosObjectTemplate> pTempl,CPosEdObject& muster)
{
  ASSERT(pTempl != NULL);
  m_pTemplate = pTempl;
  m_nTemplateID = pTempl->m_ID;
  //prepare positions
  AllocPositions();

  // v�po�et matice dle polohy
  m_Position = M4Zero;  

  Ref<CPosObjectTemplate> musterTempl = muster.GetTemplate();

  if (m_pTemplate->m_bRandomSize && muster.m_nSizeScale != 1.0 && musterTempl.NotNull())
  {
    if (muster.m_nSizeScale <= 1.0)    
      m_nSizeScale = 1.0 - m_pTemplate->m_nMinRanSize * (1.0 - muster.m_nSizeScale) / musterTempl->m_nMinRanSize;
    else
      m_nSizeScale = 1.0 + m_pTemplate->m_nMaxRanSize * (muster.m_nSizeScale - 1.0) / musterTempl->m_nMaxRanSize;
  }
  else  
    m_nSizeScale = 1.0;  

  m_Position(0,0) = (float)m_nSizeScale;
  m_Position(1,1) = (float)m_nSizeScale;
  m_Position(2,2) = (float)m_nSizeScale;

  if (m_pTemplate->m_bRandomVert && musterTempl.NotNull() && musterTempl->m_bRandomVert)
  {
    double skew  = muster.GetGradSkew();
    if (skew > 180)
      skew -= 360;

    if (skew < 0)
      skew = m_pTemplate->m_nMinRanVert * skew /  musterTempl->m_nMinRanVert;
    else
      skew = m_pTemplate->m_nMaxRanVert * skew /  musterTempl->m_nMaxRanVert;

    CalcVertRotXPosition(skew);
  }
  
  CalcRotationPosition(muster.GetGradRotation());

  REALPOS rPos = muster.GetRealPos();
  m_Position(0,3) = rPos.x;
  m_Position(2,3) = rPos.z;
  CalcObjectPoseidonHeight();

  
  // ID neni ur�eno
  m_nObjectID = UNDEF_REG_ID;
};

// randomizace objektu podle �ablony
void CPosEdObject::RandomFromTemplateAt(Ref<CPosObjectTemplate> pTempl,REALPOS rPos)
{
	// jednotkov� matice
	m_Position = M4Identity;
	
	// n�dodn� velikost
	if (pTempl->m_bRandomSize)
	{
		int nRand = rand();
		// interval
		double nInt = pTempl->m_nMinRanSize + pTempl->m_nMaxRanSize;
		double nKoe = (double)(nRand); nKoe = nKoe / (double)(RAND_MAX);
			   nKoe = nKoe * nInt;
		if (nKoe < pTempl->m_nMinRanSize)
		{
			// zmen�en� objektu
			nKoe /= 100.;
			m_nSizeScale = (1.- nKoe);
		} else
		{
			nKoe -= pTempl->m_nMinRanSize;
			ASSERT(nKoe <= pTempl->m_nMaxRanSize);
			// zv�t�en� objektu
			if (nKoe > 0)
			{
				nKoe /= 100.;
				m_nSizeScale = (1.+ nKoe);
			};
		};
		m_Position(0,0) = (float)m_nSizeScale;
		m_Position(1,1) = (float)m_nSizeScale;
		m_Position(2,2) = (float)m_nSizeScale;
	}
	// n�hodn� sklon
	if (pTempl->m_bRandomVert)
	{
		int nRand = rand();
		// interval
		double nInt = pTempl->m_nMinRanVert + pTempl->m_nMaxRanVert;
		double nKoe = (double)(nRand); nKoe = nKoe / (double)(RAND_MAX);
			   nKoe = nKoe * nInt;
		if (nKoe < pTempl->m_nMinRanVert)
		{
			// n�klon
			CalcVertRotXPosition(360-nKoe);
		} else
		{
			nKoe -= pTempl->m_nMinRanVert;
			ASSERT(nKoe <= pTempl->m_nMaxRanVert);
			if (nKoe > 0)
			{
				// n�klon
				CalcVertRotXPosition(nKoe);
			};
		};
	}
	// n�hodn� rotace
	if (pTempl->m_bRandomRota)
	{
		int nRand = rand();
		// interval
		double nInt = pTempl->m_nMinRanRota + pTempl->m_nMaxRanRota;
		double nKoe = (double)(nRand); nKoe = nKoe / (double)(RAND_MAX);
			   nKoe = nKoe * nInt;
		if (nKoe < pTempl->m_nMinRanRota)
		{
			// rotace vlevo
			CalcRotationPosition(-nKoe);
		} else
		{
			nKoe -= pTempl->m_nMinRanRota;
			ASSERT(nKoe <= pTempl->m_nMaxRanRota);
			if (nKoe > 0)
			{
				// rotace vpravo
				CalcRotationPosition(nKoe);
			};
		};
	}
	// poloha objektu
	m_Position(0,3) = rPos.x;
	m_Position(2,3) = rPos.z;
	CalcObjectPoseidonHeight();
  
};

// v�po�et logick�ch sou�adnic objektu
void CPosEdObject::CalcObjectLogPosition()
{
  float nMSize = ((float)VIEW_LOG_UNIT_SIZE)/GetDocument()->m_nRealSizeUnit;

  // rozm�ry objektu podle st�edu
  float nW;
  float nH;
  
  Vector3 boundingCenter(VZero);

  CPosObjectTemplate::PosType type;
  if (m_pTemplate.IsNull())
  {
    Ref<CPosObjectTemplate> templ = GetDocument()->GetObjectTemplate(m_nTemplateID);
    if (templ.IsNull())
    {
      LogF("Error: Object template %s(ID %d) not found.", m_sObjName,m_nTemplateID );
      return;
    }
    if (!templ->HasAutoCenter())
      boundingCenter = templ->GetDrawBoundingCenter();
    else
      boundingCenter = templ->GetDrawBoundingCenter() - templ->GetBoundingCenter();


    nW = (float) max(templ->GetDrawWidth()*nMSize/2., 0.5);
    nH = (float) max(templ->GetDrawHeight()*nMSize/2., 0.5);
    type = templ->GetPosType();
  }
  else
  {
    if (!m_pTemplate->HasAutoCenter())
      boundingCenter = m_pTemplate->GetDrawBoundingCenter();
    else
      boundingCenter = m_pTemplate->GetDrawBoundingCenter() - m_pTemplate->GetBoundingCenter();
  
    nW = (float) max(m_pTemplate->GetDrawWidth()*nMSize/2., 0.5);
    nH = (float) max(m_pTemplate->GetDrawHeight()*nMSize/2., 0.5);

    type = m_pTemplate->GetPosType();
  }

  // st�ed objektu z matice  
  REALPOS Pos;  
  Vector3 realPos = PositionModelToWorld(boundingCenter);

  Pos.x = realPos[0];
  Pos.z = realPos[2];
  
  // p�evod na log.
  CPoint  ptCenter = _map->FromRealToViewPos(Pos);

  /// find what kind of position visualization is used
  switch (type)
  {
  case CPosObjectTemplate::posTypePoly:  
    {
      static_cast<CPositionPoly*>(m_LogPosition.GetRef())->CreateBox(ptCenter, nW, nH, m_Position.Orientation());   
      break;
    }
  case CPosObjectTemplate::posTypeEllipse:    
    {
      static_cast<CPositionPoly*>(m_LogPosition.GetRef())->CreateEllipse(ptCenter, nW, nH, m_Position.Orientation());
      /*CRect rect;
      rect.SetRect(ptCenter.x - nW, ptCenter.y - nH, ptCenter.x + nW, ptCenter.y + nH);
      static_cast<CPositionEllipse*>(m_LogPosition.GetRef())->Create(rect);   */
      break;
    }
  default:
    Assert(false);
  }
};


Vector3	CPosEdObject::PositionModelToWorld(const Vector3 &v) const
{
	Vector3 result;
	result[0] =
		v[0] * m_Position(0,0) +
		v[1] * m_Position(0,1) +
		v[2] * m_Position(0,2) +
		m_Position(0,3);
	result[1] =
		v[0] * m_Position(1,0) +
		v[1] * m_Position(1,1) +
		v[2] * m_Position(1,2) +
		m_Position(1,3);
	result[2] =
		v[0] * m_Position(2,0) +
		v[1] * m_Position(2,1) +
		v[2] * m_Position(2,2) +
		m_Position(2,3);

	return result;
}

void CPosEdObject::RepairMatrix()
{
	Matrix3 transform;
	int i;
	for (i=0; i<3; i++)
		for (int j=0; j<3; j++)
			transform(i, j) = m_Position(i,j);

	float scale = 1.0f;
	if (!m_pTemplate || !m_pTemplate->m_bIsNetObject)
		scale = transform.Scale();
	transform.SetUpAndAside
	(
		transform.DirectionUp(), transform.DirectionAside()
	);
	transform.SetScale(scale);

	for (i=0; i<3; i++)
		for (int j=0; j<3; j++)
			m_Position(i,j) = transform(i, j);
}

void CPosEdObject::CalcObjectPoseidonHeight()
{
	Vector3 bc(VZero);
	if (m_pTemplate.NotNull())
	{
    if (m_pTemplate->HasAutoCenter())
		  bc = -m_pTemplate->GetBoundingCenter();

		Vector3 pos = PositionModelToWorld(bc);
		REALPOS rPos; rPos.x = pos[0]; rPos.z = pos[2];
		LANDHEIGHT yPos = _map->GetLandscapeHeight(rPos);
		LANDHEIGHT diff = yPos - pos[1];
		m_Position(1,3) += diff + (LANDHEIGHT)m_nRelHeight;
    return ;
	}
	else
	{
    Ref<CPosObjectTemplate> templ = GetDocument()->GetObjectTemplate(m_nTemplateID);
    if (templ.NotNull())
    {
      if (templ->HasAutoCenter())
        bc = -templ->GetBoundingCenter();      

      Vector3 pos = PositionModelToWorld(bc);
      REALPOS rPos; rPos.x = pos[0]; rPos.z = pos[2];
      LANDHEIGHT yPos = _map->GetLandscapeHeight(rPos);
      LANDHEIGHT diff = yPos - pos[1];
      m_Position(1,3) += diff + (LANDHEIGHT)m_nRelHeight;
      return;
    }
    else
    {
      REALPOS		rPos;
      LANDHEIGHT	yPos;
      rPos = GetRealPos();
      yPos = _map->GetLandscapeHeight(rPos);
      m_Position(1,3) = yPos + (LANDHEIGHT)m_nRelHeight;
      return;
    }
  }
};

void CPosEdObject::RepairObjectPoseidonRelHeight() 
{ 
  Ref<CPosObjectTemplate> templ;
  if (m_pTemplate.NotNull())
  {
    templ = m_pTemplate;
  }
  else
  {
    templ = GetDocument()->GetObjectTemplate(m_nTemplateID);
  }

  if (templ.IsNull())
    return;  

  if (_stricmp(templ->GetPlacement(), "slope") || !templ->HasAutoCenter())
    return; // nothing to repair.. 
        
  Vector3 bcGood = -templ->GetBoundingCenter();

  Vector3 posGood = PositionModelToWorld(bcGood);
  REALPOS rPosGood; rPosGood.x = posGood[0]; rPosGood.z = posGood[2];
  float dXGood,dZGood; 
  LANDHEIGHT yPosGood= _map->GetLandscapeHeight(rPosGood, &dXGood, &dZGood);

  Vector3 bcBad = templ->GetBoundingCenter();
  Vector3 posBad = PositionModelToWorld(bcBad);
  REALPOS rPosBad; rPosBad.x = posBad[0]; rPosBad.z = posBad[2];
  float dXBad,dZBad; 
  _map->GetLandscapeHeight(rPosBad, &dXBad, &dZBad);

  float diffHeight = (posGood[0] - m_Position(0,3)) * dXGood + (posGood[2] - m_Position(2,3)) * dZGood;   
  if (fabs(dXGood - dXBad) > 0.001f || fabs(dZGood - dZBad) > 0.001f)
  {
    RptF("Bad-good slope change: obj %s pos (%lf, %lf), change (%lf, %lf), height change %lf", templ->GetName(), m_Position(0,3), m_Position(2,3), dXGood - dXBad, dZGood - dZBad, -diffHeight);
  }

  m_nRelHeight -= diffHeight;

  m_Position(1,3) += yPosGood - posGood[1] + (LANDHEIGHT)m_nRelHeight;
  return;
};

REALPOS CPosEdObject::GetRealZeroPos() const
{  
  Ref<CPosObjectTemplate> templ;
  if (m_pTemplate.NotNull()) 
    templ = m_pTemplate;
  else
    templ = GetDocument()->GetObjectTemplate(m_nTemplateID);

  if (templ.IsNull())
    return REALPOS(0,0);  

  Vector3 bc(VZero);

  if (templ->HasAutoCenter())
    bc = -templ->GetBoundingCenter();

  Vector3 pos = PositionModelToWorld(bc);
  return REALPOS(pos[0], pos[2]);
}

float CPosEdObject::CalcRelHeightFromCurrPos() const
{
	Vector3 bc(VZero);
  if (m_pTemplate.IsNull())
  {
    Ref<CPosObjectTemplate> templ = GetDocument()->GetObjectTemplate(m_nTemplateID);
    if (templ.NotNull())
    {  
      if (templ->HasAutoCenter())
        bc = -templ->GetBoundingCenter();      
    }
    else
      LogF("Template not found %s.", m_sObjName);
  }
  else
  {  
    if (m_pTemplate->HasAutoCenter())
	    bc = -m_pTemplate->GetBoundingCenter();
  }
	Vector3 pos = PositionModelToWorld(bc);
	REALPOS rPos; rPos.x = pos[0]; rPos.z = pos[2];
	LANDHEIGHT yPos = _map->GetLandscapeHeight(rPos);
	return pos[1] - yPos; 
};

// �prava polohy objekt�
void CPosEdObject::CalcOffsetLogPosition(int xAmount,int yAmount)
{
	// �prava matice
	REALPOS rPosX = _map->FromViewToRealPos(CPoint(xAmount,0));
	REALPOS rPosZ = _map->FromViewToRealPos(CPoint(yAmount,0));
	m_Position(0,3) += rPosX.x;
	m_Position(2,3) -= rPosZ.x;
	// v�po�et nov� polohy objektu
	CalcObjectLogPosition();
};

void CPosEdObject::CalcRotationPosition(double nGrad)
{
	// obr�cen� sm�r - nev�m pro�, ale funguje
	//nGrad = 360.-nGrad;

	//while(nGrad < 0.)   nGrad += 360.;
	//while(nGrad >= 360.)nGrad -= 360.;
	if (nGrad == 0.) return;
	// p��prava dat
	Matrix4 rot(MRotationY,(float)GRADTORAD(nGrad)); 

	m_Position = m_Position * rot;	

	// p��prava konstant
	/*float c = (float)cos(GRADTORAD(nGrad));
	float s = -(float)sin(GRADTORAD(nGrad));
	// oto�en� orientace
	m_Position(0,0) = c*TI[0][0] + s*TI[0][2];
	m_Position(0,1) = TI[0][1];
	m_Position(0,2) = -s*TI[0][0] + c*TI[0][2];
	m_Position(1,0) = c*TI[1][0] + s*TI[1][2];
	m_Position(1,1) = TI[1][1];
	m_Position(1,2) = -s*TI[1][0] + c*TI[1][2];
	m_Position(2,0) = c*TI[2][0] + s*TI[2][2];
	m_Position(2,1) = TI[2][1];
	m_Position(2,2) = -s*TI[2][0] + c*TI[2][2];*/
	
	// v�po�et nov� polohy objektu
	CalcObjectLogPosition();
}

void CPosEdObject::CalcRotationPosition(double nGrad, CPointVal originLog)
{
	CalcRotationPosition(nGrad);

	REALPOS realOrigin = _map->FromViewToRealPos(originLog);
	FMntVector2 realOriginVec(realOrigin.x, realOrigin.z);
	FMntVector2 realPos(m_Position(0, 3), m_Position(2, 3));

	FMntMatrix2 trans;
	trans.Rotation((float) nGrad);

	realPos = trans * (realPos - realOriginVec) + realOriginVec;
	m_Position(0, 3) = realPos[0];
	m_Position(2, 3) = realPos[1];

	CalcObjectLogPosition();
}

void CPosEdObject::CalcVertRotXPosition(double nGrad)
{
	// use rotation instead of skew
	
	while(nGrad < 0.)   nGrad += 360.;
	while(nGrad >= 360.)nGrad -= 360.;

	if (nGrad == 0.)	return;


	// obr�cen� sm�r - nev�m pro�, ale funguje
	nGrad = /*360.-*/nGrad;

  Matrix4 rot(MRotationX,(float) GRADTORAD(nGrad)); 
  m_Position = m_Position * rot;	


	/*// p��prava dat
	float TI[4][4];
	for(int i=0;i<4;i++)
		for(int j=0;j<4;j++)
		{
			TI[i][j] = m_Position(i,j);
		};

	// p��prava konstant
	float c = (float)cos(GRADTORAD(nGrad));
	float s = (float)sin(GRADTORAD(nGrad));

	// oto�en� orientace
	m_Position(0,0) = TI[0][0];
	m_Position(0,1) = c*TI[0][1] + s*TI[0][2];
	m_Position(0,2) = -s*TI[0][1] + c*TI[0][2];
	m_Position(1,0) = TI[1][0];
	m_Position(1,1) = c*TI[1][1] + s*TI[1][2];
	m_Position(1,2) = -s*TI[1][1] + c*TI[1][2];
	m_Position(2,0) = TI[2][0];
	m_Position(2,1) = c*TI[2][1] + s*TI[2][2];
	m_Position(2,2) = -s*TI[2][1] + c*TI[2][2];
	
	// dopln�n� matice
	m_Position(3,0) = 0;
	m_Position(3,1) = 0;
	m_Position(3,2) = 0;
	m_Position(3,3) = 1;*/
	
	// v�po�et nov� polohy objektu
	CalcObjectLogPosition();
};

// vykreslov�n� objektu
void CPosEdObject::OnDrawObject(CDC* pDC, const CDrawPar* pParams) const
{
	COLORREF cFrame, cEntir;
	if (m_pTemplate.NotNull())
	{    
		cFrame = m_pTemplate->GetObjectFrameColor(pParams->m_pCfg);
		cEntir = m_pTemplate->GetObjectEntireColor(pParams->m_pCfg);
	}
	else
	{   
		Ref<CPosObjectTemplate> templ = GetDocument()->GetObjectTemplate(m_nTemplateID);
		if (templ.NotNull())
		{  
			cFrame = templ->GetObjectFrameColor(pParams->m_pCfg);
			cEntir = templ->GetObjectEntireColor(pParams->m_pCfg);  
		}
		else
		{
			LogF("Template not found %s (id %d).", m_sObjName, m_nObjectID);
			return; // without template we do not now what to draw...
		}
	}

	if (pParams->m_VwStyle.m_ShowLock && Locked())
	{
		cFrame = RGBToY(cFrame);
		cEntir = RGBToY(cEntir);
	}

	m_DevPosition->OnDrawObject(pDC, cFrame, -1, cEntir, -1, pParams);
}

Ref<CPosObjectTemplate> CPosEdObject::GetTemplate()  const
{
	if (m_pTemplate.NotNull()) return m_pTemplate;

	if (!GetDocument()) return NULL;

	return GetDocument()->GetObjectTemplate(m_nTemplateID);
}

double CPosEdObject::GetGradRotation() const
{
	FMntVector2 to(m_Position(0, 0), m_Position(2, 0));
	FMntVector2 from(1, 0);
	return -from.GradAngleTo(to);
}

double CPosEdObject::GetGradSkew() const
{
	FMntVector2 to(m_Position(1, 1), m_Position(2, 1));
	FMntVector2 from(1, 0);
	return from.GradAngleTo(to);
}

void CPosEdObject::SetRealPos(const REALPOS& pos)
{
  m_Position(0,3) = pos.x;
  m_Position(2,3) = pos.z;

  CalcObjectPoseidonHeight(); 
  CalcObjectLogPosition();
}

int CPosEdObject::SetFreeID()
{
  return m_nObjectID = _map->GetFreeObjectID();
}

void CPosEdObject::MakeUndef()
{
  m_nObjectID = UNDEF_REG_ID;
  m_sObjName.Empty();
}
