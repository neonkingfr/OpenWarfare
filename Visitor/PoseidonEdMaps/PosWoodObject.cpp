/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPosWoodObject

IMPLEMENT_DYNCREATE(CPosWoodObject,CPosAreaObject)

CPosWoodObject::CPosWoodObject()
{
	m_nWoodID	 = UNDEF_REG_ID;
};

CPosWoodObject::CPosWoodObject(const CPoseidonMap& map) : base(map)
{
  m_nWoodID	 = UNDEF_REG_ID;
};

void CPosWoodObject::CopyFrom(const CPosObject* pSrc)
{
	ASSERT_KINDOF(CPosWoodObject,pSrc);
	// kopie plochy
	CPosAreaObject::CopyFrom(pSrc);
	m_nWoodID		= ((CPosWoodObject*)pSrc)->m_nWoodID;
	m_sTemplName	= ((CPosWoodObject*)pSrc)->m_sTemplName;
};

// serializace
void CPosWoodObject::DoSerialize(CArchive& ar, DWORD nVer)
{
	CPosAreaObject::DoSerialize(ar, nVer);
	// serializace dat lesa
	MntSerializeInt(ar, m_nWoodID);
	MntSerializeString(ar, m_sTemplName);
};

// vytvo�en� ze �ablony na pozici
void CPosWoodObject::CreateFromTemplateAt(const CPosWoodTemplate* pTempl,const CPosAreaObject* pArea, int ID)
{
	// kop�ruji polohu plochy
	CPosAreaObject::CopyFrom(pArea);
	// jm�no a barvy podle �ablony lesa
	m_sAreaName	 = pTempl->m_sWoodName;
	m_cAreaEntire= pTempl->m_clrEntire;
	m_cAreaFrame = pTempl->m_clrFrame;
	// jm�no �ablony
	m_sTemplName = pTempl->m_sWoodName;
  m_nWoodID = ID;
};

CPosWoodTemplate* CPosWoodObject::GetWoodTemplate() const
{	return _map->GetDocument()->GetWoodTemplate(m_sTemplName); };

void CPosWoodObject::GetDrawAreaColors(COLORREF& cEntire,COLORREF& cFrame,int& nStyle,const CDrawPar* pParams) const
{
	CPosWoodTemplate* pWoodTempl = GetWoodTemplate();
	if (pWoodTempl)
	{
		cEntire = pWoodTempl->GetObjectEntireColor(pParams->m_pCfg);
		cFrame  = pWoodTempl->GetObjectFrameColor(pParams->m_pCfg);
		nStyle	= pParams->m_pCfg->m_nFillWoods;
	} else
	{
		cEntire = pParams->m_pCfg->m_cDefColors[CPosEdCfg::defcolWoods];
		cFrame  = pParams->m_pCfg->m_cDefColors[CPosEdCfg::defcolFWood];
		nStyle  = pParams->m_pCfg->m_nFillWoods;
	};
};

// informace o objektu
BOOL CPosWoodObject::IsEnabledObject(const EnabledObjects& enabled) const
{	return enabled.m_bWoods && !_hidden; };

/////////////////////////////////////////////////////////////////////////////
// editace objektu

BOOL CPosWoodObject::DoEditObject()
{
	return FALSE;
};

int CPosWoodObject::SetFreeID()  {return m_nWoodID = _map->GetFreeWoodID();};



