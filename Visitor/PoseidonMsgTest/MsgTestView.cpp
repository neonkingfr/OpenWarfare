// MsgTestView.cpp : implementation of the CMsgTestView class
//

#include "stdafx.h"
#include "MsgTestApp.h"

#include "MsgTestDoc.h"
#include "MsgTestView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CMsgListBox

CMsgListBox::CMsgListBox()
{
	m_pOwner = NULL;
};

// z�sk�n� �daj� pro vykreslov�n�
extern void GetMessageText(CString& sText,SPosMessage& sMsg);
BOOL CMsgListBox::GetItemText(CString& sText, LPDRAWITEMSTRUCT lpDIS)
{
	if (m_pOwner == NULL)
		return FALSE;
	
	sText	= _T("");
	int nID = (int)lpDIS->itemData;
	if (m_bServer)
	{
		if ((nID >= 0)&&(nID < m_pOwner->m_ServerList.GetSize()))
		{
			GetMessageText(sText,m_pOwner->m_ServerList[nID]);
		}
	} else
	{
		if ((nID >= 0)&&(nID < m_pOwner->m_ClientList.GetSize()))
		{
			GetMessageText(sText,m_pOwner->m_ClientList[nID]);
		}
	};
	return TRUE;
};

/////////////////////////////////////////////////////////////////////////////
// CMsgTestView

#define UPDATE_TIMER_ID			100	
#define UPDATE_TIMER_COUNT		250		// 4 x / sec 

IMPLEMENT_DYNCREATE(CMsgTestView, CFormView)

BEGIN_MESSAGE_MAP(CMsgTestView, CFormView)
	//{{AFX_MSG_MAP(CMsgTestView)
	ON_BN_CLICKED(IDC_SRV_SENDMSG, OnServer_SendMessage)
	ON_BN_CLICKED(IDC_CLI_SENDMSG, OnClient_SendMessage)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_SRV_GETMSG, OnServer_GetMessage)
	ON_BN_CLICKED(IDC_SRV_GETALL, OnServer_GetMessageAll)
	ON_BN_CLICKED(IDC_CLI_GETMSG, OnClient_GetMessage)
	ON_BN_CLICKED(IDC_CLI_GETALL, OnClient_GetMessageAll)
	ON_UPDATE_COMMAND_UI(ID_SERVER_AUTO, OnUpdateServer_AutoMessages)
	ON_UPDATE_COMMAND_UI(ID_CLIENT_AUTO, OnUpdateClient_AutoMessages)
	ON_COMMAND(ID_SERVER_AUTO, OnServer_AutoMessages)
	ON_COMMAND(ID_CLIENT_AUTO, OnClient_AutoMessages)
	ON_BN_CLICKED(IDC_SRV_GET_AUTO, OnServer_AutoMessagesChange)
	ON_BN_CLICKED(IDC_CLI_GET_AUTO, OnClient_AutoMessagesChange)
	ON_UPDATE_COMMAND_UI(ID_SERVER_GETMSG, OnUpdateServer_GetMessage)
	ON_UPDATE_COMMAND_UI(ID_SERVER_GETALL, OnUpdateServer_GetMessageAll)
	ON_UPDATE_COMMAND_UI(ID_CLIENT_GETMSG, OnUpdateClient_GetMessage)
	ON_COMMAND(ID_SERVER_SENDMSG, OnServer_SendMessage)
	ON_COMMAND(ID_CLIENT_SENDMSG, OnClient_SendMessage)
	ON_COMMAND(ID_SERVER_GETMSG, OnServer_GetMessage)
	ON_COMMAND(ID_SERVER_GETALL, OnServer_GetMessageAll)
	ON_COMMAND(ID_CLIENT_GETMSG, OnClient_GetMessage)
	ON_COMMAND(ID_CLIENT_GETALL, OnClient_GetMessageAll)
	ON_UPDATE_COMMAND_UI(ID_CLIENT_GETALL, OnUpdateClient_GetMessageAll)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMsgTestView construction/destruction

CMsgTestView::CMsgTestView()
	: CFormView(CMsgTestView::IDD)
{
	//{{AFX_DATA_INIT(CMsgTestView)
	m_bClientAuto = FALSE;
	m_bServerAuto = FALSE;
	//}}AFX_DATA_INIT

	// po��tadla hodnot
	m_nCurrServerAll	= 0;
	m_nCurrClientAll	= 0;

	m_bChangeServerList =
	m_bChangeClientList = FALSE;

	// aktu�ln� zobrazovan� parametry
	// SERVER
	m_nSrvMessagesAll	= 0;	// celkem p��jmut�ch ud�lost�
	m_nSrvMessagesFront	= 0;	// ud�lost� ve front�

	// CLIENT
	m_nCliMessagesAll	= 0;	// celkem p��jmut�ch ud�lost�
	m_nCliMessagesFront	= 0;	// ud�lost� ve front�


	m_cListServer.m_pOwner = this;
	m_cListServer.m_bServer= TRUE;
	m_cListClient.m_pOwner = this;
	m_cListClient.m_bServer= FALSE;
}

CMsgTestView::~CMsgTestView()
{
}

void CMsgTestView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMsgTestView)
	DDX_Control(pDX, IDC_LIST_SERVER, m_cListServer);
	DDX_Control(pDX, IDC_LIST_CLIENT, m_cListClient);
	DDX_Check(pDX, IDC_CLI_GET_AUTO, m_bClientAuto);
	DDX_Check(pDX, IDC_SRV_GET_AUTO, m_bServerAuto);
	//}}AFX_DATA_MAP
}

BOOL CMsgTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CMsgTestView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	// napln�n� LB
	for(int i=0; i<MSG_VIEW_COUNT; i++)
	{
		LBAddString(m_cListServer.m_hWnd, _T(""), (DWORD)i);
		LBAddString(m_cListClient.m_hWnd, _T(""), (DWORD)i);
	}
}

int CMsgTestView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	// nastaven� timeru
	SetTimer(UPDATE_TIMER_ID,UPDATE_TIMER_COUNT,NULL);
	return 0;
}

void CMsgTestView::OnDestroy() 
{
	// zru�en� timeru
	KillTimer(UPDATE_TIMER_ID);

	CFormView::OnDestroy();
}

/////////////////////////////////////////////////////////////////////////////
// CMsgTestView diagnostics

#ifdef _DEBUG
void CMsgTestView::AssertValid() const
{
	CFormView::AssertValid();
}

void CMsgTestView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CMsgTestDoc* CMsgTestView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMsgTestDoc)));
	return (CMsgTestDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMsgTestView message handlers

void CMsgTestView::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent == UPDATE_TIMER_ID)
	{
		// zkus�m vyslat ud�losti
		m_MessageFront.TryToSendMessages();
		// zobrazen� hodnot
		DoUpdateView();
	} else
	CFormView::OnTimer(nIDEvent);
}

/////////////////////////////////////////////////////////////////////////////
// Update hodnot pro timer

// zobrazen� hodnot
static BOOL bInUpdate = FALSE;
void CMsgTestView::DoUpdateView()	
{
	if (bInUpdate) return;
	bInUpdate = TRUE;

	LONG	nPom;
	TCHAR	buff[20];
	
	// SERVER
	if (m_nCurrServerAll != m_nSrvMessagesAll)
	{
		m_nSrvMessagesAll = m_nCurrServerAll;
		_ltoa(m_nSrvMessagesAll,buff,10);
		::SetWindowText(::GetDlgItem(m_hWnd,IDC_SRV_COUNT_ALL),buff);
	}

	nPom = m_MessageFront.GetServerMsgCount();
	if (nPom != m_nSrvMessagesFront)
	{
		m_nSrvMessagesFront = nPom;
		_ltoa(m_nSrvMessagesFront,buff,10);
		::SetWindowText(::GetDlgItem(m_hWnd,IDC_SRV_COUNT_FNT),buff);
	}
	// auto-v�b�r
	if ((m_nSrvMessagesFront > 0)&&(m_bServerAuto))
	{
		OnServer_GetMessageAll();
	}
	if (m_bChangeServerList)
	{
		m_bChangeServerList = FALSE;
		m_cListServer.Invalidate();
		if (m_cListServer.GetCurSel() != (MSG_VIEW_COUNT-1))
			m_cListServer.SetCurSel(max(0,m_ServerList.GetSize()-1));
	}

	// CLIENT
	if (m_nCurrClientAll != m_nCliMessagesAll)
	{
		m_nCliMessagesAll = m_nCurrClientAll;
		_ltoa(m_nCliMessagesAll,buff,10);
		::SetWindowText(::GetDlgItem(m_hWnd,IDC_CLI_COUNT_ALL),buff);
	}
	nPom = m_MessageFront.GetClientMsgCount();
	if (nPom != m_nCliMessagesFront)
	{
		m_nCliMessagesFront = nPom;
		_ltoa(m_nCliMessagesFront,buff,10);
		::SetWindowText(::GetDlgItem(m_hWnd,IDC_CLI_COUNT_FNT),buff);
	}
	// auto-v�b�r
	if ((m_nCliMessagesFront > 0)&&(m_bClientAuto))
	{
		OnClient_GetMessageAll();
	}
	if (m_bChangeClientList)
	{
		m_bChangeClientList = FALSE;
		m_cListClient.Invalidate();
		if (m_cListClient.GetCurSel() != (MSG_VIEW_COUNT-1))
			m_cListClient.SetCurSel(max(0,m_ClientList.GetSize()-1));
	}

	bInUpdate = FALSE;
};

/////////////////////////////////////////////////////////////////////////////
// Funkce SERVERU

extern BOOL DoEditMessage(SPosMessage& sMsg);
void CMsgTestView::OnServer_SendMessage() 
{
	SPosMessage	sMsg;
	if (DoEditMessage(sMsg))
		m_MessageFront.PostMessageToClient(sMsg);
}

void CMsgTestView::OnServer_AutoMessagesChange() 
{
	UpdateData(TRUE);
	::EnableWindow(::GetDlgItem(m_hWnd,IDC_SRV_GETMSG),!m_bServerAuto);
	::EnableWindow(::GetDlgItem(m_hWnd,IDC_SRV_GETALL),!m_bServerAuto);
}


void CMsgTestView::OnServer_AutoMessages() 
{
	m_bServerAuto = !m_bServerAuto;
	UpdateData(FALSE);
	OnServer_AutoMessagesChange();
}

void CMsgTestView::OnUpdateServer_AutoMessages(CCmdUI* pCmdUI) 
{
	if (pCmdUI->m_pMenu)
		pCmdUI->SetCheck(m_bServerAuto);
}

void CMsgTestView::OnServer_GetMessage() 
{
	SPosMessage	sMsg;
	if (m_MessageFront.GetServerMessage(sMsg))
	{	// byla p��jmuta ud�lost
		HandleServerMessage(sMsg);
	}
}

void CMsgTestView::OnUpdateServer_GetMessage(CCmdUI* pCmdUI) 
{
	if (pCmdUI->m_pMenu)
		pCmdUI->Enable(!m_bServerAuto);
}

void CMsgTestView::OnServer_GetMessageAll() 
{
	SPosMessage	sMsg;
	while(m_MessageFront.GetServerMessage(sMsg))
	{	// byla p��jmuta ud�lost
		HandleServerMessage(sMsg);
	}
}

void CMsgTestView::OnUpdateServer_GetMessageAll(CCmdUI* pCmdUI) 
{
	if (pCmdUI->m_pMenu)
		pCmdUI->Enable(!m_bServerAuto);
}

/////////////////////////////////////////////////////////////////////////////
// Funkce CLIENTA

void CMsgTestView::OnClient_SendMessage() 
{
	SPosMessage	sMsg;
	if (DoEditMessage(sMsg))
		m_MessageFront.PostMessageToServer(sMsg);
}

void CMsgTestView::OnClient_AutoMessagesChange() 
{
	UpdateData(TRUE);
	::EnableWindow(::GetDlgItem(m_hWnd,IDC_CLI_GETMSG),!m_bClientAuto);
	::EnableWindow(::GetDlgItem(m_hWnd,IDC_CLI_GETALL),!m_bClientAuto);
}

void CMsgTestView::OnClient_AutoMessages() 
{
	m_bClientAuto = !m_bClientAuto;
	UpdateData(FALSE);
	OnClient_AutoMessagesChange();
}

void CMsgTestView::OnUpdateClient_AutoMessages(CCmdUI* pCmdUI) 
{
	if (pCmdUI->m_pMenu)
		pCmdUI->SetCheck(m_bClientAuto);
}

void CMsgTestView::OnClient_GetMessage() 
{
	SPosMessage	sMsg;
	if(m_MessageFront.GetClientMessage(sMsg))
	{	// byla p��jmuta ud�lost
		HandleClientMessage(sMsg);
	}
}

void CMsgTestView::OnUpdateClient_GetMessage(CCmdUI* pCmdUI) 
{
	if (pCmdUI->m_pMenu)
		pCmdUI->Enable(!m_bClientAuto);
}

void CMsgTestView::OnClient_GetMessageAll() 
{
	SPosMessage	sMsg;
	while(m_MessageFront.GetClientMessage(sMsg))
	{	// byla p��jmuta ud�lost
		HandleClientMessage(sMsg);
	}
}

void CMsgTestView::OnUpdateClient_GetMessageAll(CCmdUI* pCmdUI) 
{
	if (pCmdUI->m_pMenu)
		pCmdUI->Enable(!m_bClientAuto);
}

/////////////////////////////////////////////////////////////////////////////
// Handle ud�lost�

void CMsgTestView::HandleServerMessage(SPosMessage& sMsg)
{
	m_ServerList.Add(sMsg);
	while(m_ServerList.GetSize() > MSG_VIEW_COUNT)
	{
		m_ServerList.RemoveAt(0);
	}
	m_bChangeServerList = TRUE;
	m_nCurrServerAll++;
};

void CMsgTestView::HandleClientMessage(SPosMessage& sMsg)
{
	m_ClientList.Add(sMsg);
	while(m_ClientList.GetSize() > MSG_VIEW_COUNT)
	{
		m_ClientList.RemoveAt(0);
	}
	m_bChangeClientList = TRUE;
	m_nCurrClientAll++;
};



