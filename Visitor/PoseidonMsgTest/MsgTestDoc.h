// MsgTestDoc.h : interface of the CMsgTestDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MSGTESTDOC_H__057BADB4_D577_11D1_B1AB_0060083C95F5__INCLUDED_)
#define AFX_MSGTESTDOC_H__057BADB4_D577_11D1_B1AB_0060083C95F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


class CMsgTestDoc : public CDocument
{
protected: // create from serialization only
	CMsgTestDoc();
	DECLARE_DYNCREATE(CMsgTestDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMsgTestDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMsgTestDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMsgTestDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MSGTESTDOC_H__057BADB4_D577_11D1_B1AB_0060083C95F5__INCLUDED_)
