// PoseidonMsgTest.h : main header file for the POSEIDONMSGTEST application
//

#if !defined(AFX_POSEIDONMSGTEST_H__057BADAE_D577_11D1_B1AB_0060083C95F5__INCLUDED_)
#define AFX_POSEIDONMSGTEST_H__057BADAE_D577_11D1_B1AB_0060083C95F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CMsgTestApp:
// See PoseidonMsgTest.cpp for the implementation of this class
//

class CMsgTestApp : public CWinApp
{
public:
	CMsgTestApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMsgTestApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CMsgTestApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_POSEIDONMSGTEST_H__057BADAE_D577_11D1_B1AB_0060083C95F5__INCLUDED_)
