#include "stdafx.h"
#include "MsgTestApp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define LEN_FLT	5

/////////////////////////////////////////////////////////////////////////////
// GetPositionText

void GetPositionText(CString& sText, float pos[4][4])
{
	TCHAR buff[32];
	for (int i=0; i<4; i++)
	{
		sText += "[";
		for (int j=0; j<3; j++)
		{
			_gcvt(pos[i][j], LEN_FLT, buff);
			sText += buff;
			sText += ",";
		}
		_gcvt(pos[i][3], LEN_FLT, buff);
		sText += buff;
		sText += "]";
	}
}

/////////////////////////////////////////////////////////////////////////////
// GetMessageText

void GetMessageText(CString& sText,SPosMessage& sMsg)
{
	TCHAR buff[32];

	// nMsgID
	switch (sMsg.nMsgID)
	{
	case SYSTEM_QUIT:
		sText = "System Quit";
		break;
	case SYSTEM_INIT:
		sText = "System Init";
		break;
	case FILE_EXPORT:
		sText = "File Export to: ";
		sText += sMsg.szFileName;
		break;
	case FILE_IMPORT:
		sText = "File Import from:";
		sText += sMsg.szFileName;
		break;
	case FILE_TRANSFER:
		sText = "File Transfer from:";
		sText += sMsg.szPathFrom;
		sText += " to:";
		sText += sMsg.szPathTo;
		break;
	case CURSOR_POSITION_SET:
		sText = "Cursor position:";
		GetPositionText(sText, sMsg.Position);
		break;
	case SELECTION_CLEAR:
		sText = "Selection Clear";
		break;
	case SELECTION_OBJECT_ADD:
		if (sMsg.bState)
			sText = "Selection Add object:";
		else
			sText = "Selection Delete object:";
		_itoa(sMsg.nID, buff, 10);
		sText += buff;
		break;
	case REGISTER_LANDSCAPE_TEXTURE:
		sText = "Register Texture:";
		_itoa(sMsg.nID, buff, 10);
		sText += buff;
		sText += "=";
		sText += sMsg.szName;
		break;
	case REGISTER_OBJECT_TYPE:
		sText = "Register Object: ";
		sText += sMsg.szName;
		sText += " cen[";
		_gcvt(sMsg.centerX, LEN_FLT, buff);
		sText += buff;
		sText += ",";
		_gcvt(sMsg.centerY, LEN_FLT, buff);
		sText += buff;
		sText += ",";
		_gcvt(sMsg.centerZ, LEN_FLT, buff);
		sText += buff;
		sText += "] min[";
		_gcvt(sMsg.minX, LEN_FLT, buff);
		sText += buff;
		sText += ",";
		_gcvt(sMsg.minY, LEN_FLT, buff);
		sText += buff;
		sText += ",";
		_gcvt(sMsg.minZ, LEN_FLT, buff);
		sText += buff;
		sText += "] max[";
		_gcvt(sMsg.maxX, LEN_FLT, buff);
		sText += buff;
		sText += ",";
		_gcvt(sMsg.maxY, LEN_FLT, buff);
		sText += buff;
		sText += ",";
		_gcvt(sMsg.maxZ, LEN_FLT, buff);
		sText += buff;
		sText += "]";
		break;
	case LAND_HEIGHT_CHANGE:
		sText = "Land Height:[";
		_itoa(sMsg.nX, buff, 10);
		sText += buff;
		sText += ",";
		_itoa(sMsg.nZ, buff, 10);
		sText += buff;
		sText += "]=";
		_gcvt(sMsg.Y, LEN_FLT, buff);
		sText += buff;
		break;
	case LAND_TEXTURE_CHANGE:
		sText = "Land Texture:[";
		_itoa(sMsg.nX, buff, 10);
		sText += buff;
		sText += ",";
		_itoa(sMsg.nZ, buff, 10);
		sText += buff;
		sText += "]=";
		_itoa(sMsg.nTextureID, buff, 10);
		sText += buff;
		break;
	case OBJECT_CREATE:
		sText = "Object Create:";
		_itoa(sMsg.nID, buff, 10);
		sText += buff;
		sText += "=";
		sText += sMsg.szName;
		sText += ",";
		GetPositionText(sText, sMsg.Position);
		break;
	case OBJECT_DESTROY:
		sText = "Object Destroy:";
		_itoa(sMsg.nID, buff, 10);
		sText += buff;
		break;
	case OBJECT_MOVE:
		sText = "Object Move:";
		_itoa(sMsg.nID, buff, 10);
		sText += buff;
		sText += "=";
		GetPositionText(sText, sMsg.Position);
		break;
	case OBJECT_TYPE_CHANGE:
		sText = "Object Change type:";
		_itoa(sMsg.nID, buff, 10);
		sText += buff;
		sText += "=";
		sText += sMsg.szName;
		break;
	}
};
