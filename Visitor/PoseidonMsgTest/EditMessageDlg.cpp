// EditMessageDlg.cpp : implementation file
//

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "stdafx.h"
#include "MsgTestApp.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditMessageDlg dialog

class CEditMessageDlg : public CDialog
{
// Construction
public:
	CEditMessageDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEditMessageDlg)
	enum { IDD = IDD_EDIT_MESSAGE };
	CComboBox	m_CMsgID;
	float	m_centerX;
	float	m_centerY;
	float	m_centerZ;
	CString	m_FileName;
	int		m_nID;
	float	m_maxX;
	float	m_maxY;
	float	m_maxZ;
	float	m_minX;
	float	m_minY;
	float	m_minZ;
	CString	m_Name;
	int		m_nX;
	int		m_nZ;
	CString	m_PathFrom;
	CString	m_PathTo;
	float	m_pos00;
	float	m_pos01;
	float	m_pos02;
	float	m_pos03;
	float	m_pos10;
	float	m_pos11;
	float	m_pos12;
	float	m_pos13;
	float	m_pos20;
	float	m_pos21;
	float	m_pos22;
	float	m_pos23;
	float	m_pos30;
	float	m_pos31;
	float	m_pos32;
	float	m_pos33;
	BOOL	m_bState;
	int		m_nTextureID;
	float	m_Y;
	//}}AFX_DATA
	int m_nMsgID;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditMessageDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void ChangeWindowsStates();

	// Generated message map functions
	//{{AFX_MSG(CEditMessageDlg)
	afx_msg void OnSelchangeMsgid();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CEditMessageDlg::CEditMessageDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEditMessageDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditMessageDlg)
	m_centerX = 0.0f;
	m_centerY = 0.0f;
	m_centerZ = 0.0f;
	m_FileName = _T("");
	m_nID = 0;
	m_maxX = 0.0f;
	m_maxY = 0.0f;
	m_maxZ = 0.0f;
	m_minX = 0.0f;
	m_minY = 0.0f;
	m_minZ = 0.0f;
	m_Name = _T("");
	m_nX = 0;
	m_nZ = 0;
	m_PathFrom = _T("");
	m_PathTo = _T("");
	m_pos00 = 0.0f;
	m_pos01 = 0.0f;
	m_pos02 = 0.0f;
	m_pos03 = 0.0f;
	m_pos10 = 0.0f;
	m_pos11 = 0.0f;
	m_pos12 = 0.0f;
	m_pos13 = 0.0f;
	m_pos20 = 0.0f;
	m_pos21 = 0.0f;
	m_pos22 = 0.0f;
	m_pos23 = 0.0f;
	m_pos30 = 0.0f;
	m_pos31 = 0.0f;
	m_pos32 = 0.0f;
	m_pos33 = 0.0f;
	m_bState = FALSE;
	m_nTextureID = 0;
	m_Y = 0.0f;
	//}}AFX_DATA_INIT
	m_nMsgID = 0;
}


void CEditMessageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditMessageDlg)
	DDX_Control(pDX, IDC_MSGID, m_CMsgID);
	DDX_Text(pDX, IDC_CENTERX, m_centerX);
	DDX_Text(pDX, IDC_CENTERY, m_centerY);
	DDX_Text(pDX, IDC_CENTERZ, m_centerZ);
	DDX_Text(pDX, IDC_FILENAME, m_FileName);
	DDV_MaxChars(pDX, m_FileName, 128);
	DDX_Text(pDX, IDC_ID, m_nID);
	DDX_Text(pDX, IDC_MAXX, m_maxX);
	DDX_Text(pDX, IDC_MAXY, m_maxY);
	DDX_Text(pDX, IDC_MAXZ, m_maxZ);
	DDX_Text(pDX, IDC_MINX, m_minX);
	DDX_Text(pDX, IDC_MINY, m_minY);
	DDX_Text(pDX, IDC_MINZ, m_minZ);
	DDX_Text(pDX, IDC_NAME, m_Name);
	DDV_MaxChars(pDX, m_Name, 32);
	DDX_Text(pDX, IDC_NX, m_nX);
	DDX_Text(pDX, IDC_NZ, m_nZ);
	DDX_Text(pDX, IDC_PATHFROM, m_PathFrom);
	DDV_MaxChars(pDX, m_PathFrom, 64);
	DDX_Text(pDX, IDC_PATHTO, m_PathTo);
	DDV_MaxChars(pDX, m_PathTo, 64);
	DDX_Text(pDX, IDC_POS00, m_pos00);
	DDX_Text(pDX, IDC_POS01, m_pos01);
	DDX_Text(pDX, IDC_POS02, m_pos02);
	DDX_Text(pDX, IDC_POS03, m_pos03);
	DDX_Text(pDX, IDC_POS10, m_pos10);
	DDX_Text(pDX, IDC_POS11, m_pos11);
	DDX_Text(pDX, IDC_POS12, m_pos12);
	DDX_Text(pDX, IDC_POS13, m_pos13);
	DDX_Text(pDX, IDC_POS20, m_pos20);
	DDX_Text(pDX, IDC_POS21, m_pos21);
	DDX_Text(pDX, IDC_POS22, m_pos22);
	DDX_Text(pDX, IDC_POS23, m_pos23);
	DDX_Text(pDX, IDC_POS30, m_pos30);
	DDX_Text(pDX, IDC_POS31, m_pos31);
	DDX_Text(pDX, IDC_POS32, m_pos32);
	DDX_Text(pDX, IDC_POS33, m_pos33);
	DDX_Check(pDX, IDC_STATE, m_bState);
	DDX_Text(pDX, IDC_TEXTUREID, m_nTextureID);
	DDX_Text(pDX, IDC_Y, m_Y);
	//}}AFX_DATA_MAP
	if (pDX->m_bSaveAndValidate)
	{
		m_nMsgID = m_CMsgID.GetItemData(m_CMsgID.GetCurSel());
	}
}

BEGIN_MESSAGE_MAP(CEditMessageDlg, CDialog)
	//{{AFX_MSG_MAP(CEditMessageDlg)
	ON_CBN_SELCHANGE(IDC_MSGID, OnSelchangeMsgid)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CEditMessageDlg::ChangeWindowsStates()
{
	BOOL bEnable;
	bEnable = (m_nMsgID == FILE_EXPORT || m_nMsgID == FILE_IMPORT);
	GetDlgItem(IDC_FILENAME)->EnableWindow(bEnable);
	bEnable = (m_nMsgID == FILE_TRANSFER);
	GetDlgItem(IDC_PATHFROM)->EnableWindow(bEnable);
	GetDlgItem(IDC_PATHTO)->EnableWindow(bEnable);
	bEnable = (m_nMsgID == SELECTION_OBJECT_ADD ||
				m_nMsgID == REGISTER_LANDSCAPE_TEXTURE ||
				m_nMsgID == OBJECT_CREATE ||
				m_nMsgID == OBJECT_DESTROY ||
				m_nMsgID == OBJECT_MOVE ||
				m_nMsgID == OBJECT_TYPE_CHANGE);
	GetDlgItem(IDC_ID)->EnableWindow(bEnable);
	bEnable = (m_nMsgID == REGISTER_LANDSCAPE_TEXTURE ||
				m_nMsgID == REGISTER_OBJECT_TYPE ||
				m_nMsgID == OBJECT_CREATE ||
				m_nMsgID == OBJECT_TYPE_CHANGE);
	GetDlgItem(IDC_NAME)->EnableWindow(bEnable);
	bEnable = (m_nMsgID == SELECTION_OBJECT_ADD);
	GetDlgItem(IDC_STATE)->EnableWindow(bEnable);
	bEnable = (m_nMsgID == CURSOR_POSITION_SET ||
				m_nMsgID == OBJECT_CREATE ||
				m_nMsgID == OBJECT_MOVE);
	GetDlgItem(IDC_POS00)->EnableWindow(bEnable);
	GetDlgItem(IDC_POS01)->EnableWindow(bEnable);
	GetDlgItem(IDC_POS02)->EnableWindow(bEnable);
	GetDlgItem(IDC_POS03)->EnableWindow(bEnable);
	GetDlgItem(IDC_POS10)->EnableWindow(bEnable);
	GetDlgItem(IDC_POS11)->EnableWindow(bEnable);
	GetDlgItem(IDC_POS12)->EnableWindow(bEnable);
	GetDlgItem(IDC_POS13)->EnableWindow(bEnable);
	GetDlgItem(IDC_POS20)->EnableWindow(bEnable);
	GetDlgItem(IDC_POS21)->EnableWindow(bEnable);
	GetDlgItem(IDC_POS22)->EnableWindow(bEnable);
	GetDlgItem(IDC_POS23)->EnableWindow(bEnable);
	GetDlgItem(IDC_POS30)->EnableWindow(bEnable);
	GetDlgItem(IDC_POS31)->EnableWindow(bEnable);
	GetDlgItem(IDC_POS32)->EnableWindow(bEnable);
	GetDlgItem(IDC_POS33)->EnableWindow(bEnable);
	bEnable = (m_nMsgID == REGISTER_OBJECT_TYPE);
	GetDlgItem(IDC_CENTERX)->EnableWindow(bEnable);
	GetDlgItem(IDC_CENTERY)->EnableWindow(bEnable);
	GetDlgItem(IDC_CENTERZ)->EnableWindow(bEnable);
	GetDlgItem(IDC_MINX)->EnableWindow(bEnable);
	GetDlgItem(IDC_MINY)->EnableWindow(bEnable);
	GetDlgItem(IDC_MINZ)->EnableWindow(bEnable);
	GetDlgItem(IDC_MAXX)->EnableWindow(bEnable);
	GetDlgItem(IDC_MAXY)->EnableWindow(bEnable);
	GetDlgItem(IDC_MAXZ)->EnableWindow(bEnable);
	bEnable = (m_nMsgID == LAND_HEIGHT_CHANGE ||
				m_nMsgID == LAND_TEXTURE_CHANGE);
	GetDlgItem(IDC_NX)->EnableWindow(bEnable);
	GetDlgItem(IDC_NZ)->EnableWindow(bEnable);
	bEnable = (m_nMsgID == LAND_HEIGHT_CHANGE);
	GetDlgItem(IDC_Y)->EnableWindow(bEnable);
	bEnable = (m_nMsgID == LAND_TEXTURE_CHANGE);
	GetDlgItem(IDC_TEXTUREID)->EnableWindow(bEnable);
}

/////////////////////////////////////////////////////////////////////////////
// CEditMessageDlg message handlers

void CEditMessageDlg::OnSelchangeMsgid() 
{
	m_nMsgID = m_CMsgID.GetItemData(m_CMsgID.GetCurSel());
	ChangeWindowsStates();
}

BOOL CEditMessageDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	int nIndex;
	nIndex = m_CMsgID.AddString("SYSTEM_QUIT");
	m_CMsgID.SetItemData(nIndex, SYSTEM_QUIT);
	nIndex = m_CMsgID.AddString("SYSTEM_INIT");
	m_CMsgID.SetItemData(nIndex, SYSTEM_INIT);
	nIndex = m_CMsgID.AddString("FILE_EXPORT");
	m_CMsgID.SetItemData(nIndex, FILE_EXPORT);
	nIndex = m_CMsgID.AddString("FILE_IMPORT");
	m_CMsgID.SetItemData(nIndex, FILE_IMPORT);
	nIndex = m_CMsgID.AddString("FILE_TRANSFER");
	m_CMsgID.SetItemData(nIndex, FILE_TRANSFER);
	nIndex = m_CMsgID.AddString("CURSOR_POSITION_SET");
	m_CMsgID.SetItemData(nIndex, CURSOR_POSITION_SET);
	nIndex = m_CMsgID.AddString("SELECTION_CLEAR");
	m_CMsgID.SetItemData(nIndex, SELECTION_CLEAR);
	nIndex = m_CMsgID.AddString("SELECTION_OBJECT_ADD");
	m_CMsgID.SetItemData(nIndex, SELECTION_OBJECT_ADD);
	nIndex = m_CMsgID.AddString("REGISTER_LANDSCAPE_TEXTURE");
	m_CMsgID.SetItemData(nIndex, REGISTER_LANDSCAPE_TEXTURE);
	nIndex = m_CMsgID.AddString("REGISTER_OBJECT_TYPE");
	m_CMsgID.SetItemData(nIndex, REGISTER_OBJECT_TYPE);
	nIndex = m_CMsgID.AddString("LAND_HEIGHT_CHANGE");
	m_CMsgID.SetItemData(nIndex, LAND_HEIGHT_CHANGE);
	nIndex = m_CMsgID.AddString("LAND_TEXTURE_CHANGE");
	m_CMsgID.SetItemData(nIndex, LAND_TEXTURE_CHANGE);
	nIndex = m_CMsgID.AddString("OBJECT_CREATE");
	m_CMsgID.SetItemData(nIndex, OBJECT_CREATE);
	nIndex = m_CMsgID.AddString("OBJECT_DESTROY");
	m_CMsgID.SetItemData(nIndex, OBJECT_DESTROY);
	nIndex = m_CMsgID.AddString("OBJECT_MOVE");
	m_CMsgID.SetItemData(nIndex, OBJECT_MOVE);
	nIndex = m_CMsgID.AddString("OBJECT_TYPE_CHANGE");
	m_CMsgID.SetItemData(nIndex, OBJECT_TYPE_CHANGE);

	m_CMsgID.SetCurSel(0);

	ChangeWindowsStates();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/////////////////////////////////////////////////////////////////////////////
// DoEditMessage(SPosMessage&)

BOOL DoEditMessage(SPosMessage& sMsg)
{
	CEditMessageDlg dlg;

	if (dlg.DoModal()!=IDOK)
		return FALSE;

	// kopie parametrý do message
	sMsg.nMsgID  =  dlg.m_nMsgID;
	switch (sMsg.nMsgID)
	{
	case FILE_EXPORT:
	case FILE_IMPORT:
		strcpy(sMsg.szFileName, dlg.m_FileName);
		break;
	case FILE_TRANSFER:
		strcpy(sMsg.szPathFrom, dlg.m_PathFrom);
		strcpy(sMsg.szPathTo, dlg.m_PathTo);
		break;
	case OBJECT_CREATE:
		strcpy(sMsg.szName, dlg.m_Name);
	case OBJECT_MOVE:
		sMsg.nID = dlg.m_nID;
	case CURSOR_POSITION_SET:
		sMsg.Position[0][0] = dlg.m_pos00;
		sMsg.Position[0][1] = dlg.m_pos01;
		sMsg.Position[0][2] = dlg.m_pos02;
		sMsg.Position[0][3] = dlg.m_pos03;
		sMsg.Position[1][0] = dlg.m_pos10;
		sMsg.Position[1][1] = dlg.m_pos11;
		sMsg.Position[1][2] = dlg.m_pos12;
		sMsg.Position[1][3] = dlg.m_pos13;
		sMsg.Position[2][0] = dlg.m_pos20;
		sMsg.Position[2][1] = dlg.m_pos21;
		sMsg.Position[2][2] = dlg.m_pos22;
		sMsg.Position[2][3] = dlg.m_pos23;
		sMsg.Position[3][0] = dlg.m_pos30;
		sMsg.Position[3][1] = dlg.m_pos31;
		sMsg.Position[3][2] = dlg.m_pos32;
		sMsg.Position[3][3] = dlg.m_pos33;
		break;
	case SELECTION_OBJECT_ADD:
		sMsg.nID = dlg.m_nID;
		sMsg.bState = (dlg.m_bState != 0);
		break;
	case REGISTER_LANDSCAPE_TEXTURE:
		sMsg.nID = dlg.m_nID;
		strcpy(sMsg.szName, dlg.m_Name);
		break;
	case REGISTER_OBJECT_TYPE:
		strcpy(sMsg.szName, dlg.m_Name);
		sMsg.centerX = dlg.m_centerX;
		sMsg.centerY = dlg.m_centerY;
		sMsg.centerZ = dlg.m_centerZ;
		sMsg.minX = dlg.m_minX;
		sMsg.minY = dlg.m_minY;
		sMsg.minZ = dlg.m_minZ;
		sMsg.maxX = dlg.m_maxX;
		sMsg.maxY = dlg.m_maxY;
		sMsg.maxZ = dlg.m_maxZ;
		break;
	case LAND_HEIGHT_CHANGE:
		sMsg.nX = dlg.m_nX;
		sMsg.nZ = dlg.m_nZ;
		sMsg.Y = dlg.m_Y;
		break;
	case LAND_TEXTURE_CHANGE:
		sMsg.nX = dlg.m_nX;
		sMsg.nZ = dlg.m_nZ;
		sMsg.nTextureID = dlg.m_nTextureID;
		break;
	case OBJECT_DESTROY:
		sMsg.nID = dlg.m_nID;
		break;
	case OBJECT_TYPE_CHANGE:
		sMsg.nID = dlg.m_nID;
		strcpy(sMsg.szName, dlg.m_Name);
		break;
	case SYSTEM_QUIT:
	case SYSTEM_INIT:
	case SELECTION_CLEAR:
		// no params
		break;
	}

	return TRUE;
};
