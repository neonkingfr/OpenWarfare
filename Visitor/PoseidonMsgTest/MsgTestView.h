// MsgTestView.h : interface of the CMsgTestView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MSGTESTVIEW_H__057BADB6_D577_11D1_B1AB_0060083C95F5__INCLUDED_)
#define AFX_MSGTESTVIEW_H__057BADB6_D577_11D1_B1AB_0060083C95F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#define MSG_VIEW_COUNT	100	// po�et zobrazovan�ch p��jmut�ch ud�lost�

class CMsgTestView;
class CMsgListBox : public CMntListBox
{
// Construction
public:
				 CMsgListBox();

	// z�sk�n� �daj� pro vykreslov�n�
	virtual BOOL GetItemText(CString& , LPDRAWITEMSTRUCT lpDIS);


// data
	CMsgTestView*	m_pOwner;
	BOOL			m_bServer;
};

class CMsgTestView : public CFormView
{
protected: // create from serialization only
	CMsgTestView();
	DECLARE_DYNCREATE(CMsgTestView)

public:
	//{{AFX_DATA(CMsgTestView)
	enum { IDD = IDD_POSEIDONMSGTEST_FORM };
	CMsgListBox	m_cListClient;
	CMsgListBox	m_cListServer;
	BOOL	m_bClientAuto;
	BOOL	m_bServerAuto;
	//}}AFX_DATA

// Attributes
public:
	CMsgTestDoc* GetDocument();

	// seznamy p��jmut�ch ud�lost�
	CArray<SPosMessage,SPosMessage&>	m_ServerList;
	CArray<SPosMessage,SPosMessage&>	m_ClientList;

	BOOL	m_bChangeServerList;
	BOOL	m_bChangeClientList;

	// po��tadla hodnot
	LONG	m_nCurrServerAll;
	LONG	m_nCurrClientAll;

	// aktu�ln� zobrazovan� parametry

	// SERVER
	LONG	m_nSrvMessagesAll;		// celkem p��jmut�ch ud�lost�
	LONG	m_nSrvMessagesFront;	// ud�lost� ve front�

	// CLIENT
	LONG	m_nCliMessagesAll;		// celkem p��jmut�ch ud�lost�
	LONG	m_nCliMessagesFront;	// ud�lost� ve front�



	// kominukace s jinou alikac�
	CMessageFront	m_MessageFront;


// Operations
public:
			void DoUpdateView();	// zobrazen� hodnot

			void HandleServerMessage(SPosMessage&);
			void HandleClientMessage(SPosMessage&);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMsgTestView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMsgTestView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMsgTestView)
	afx_msg void OnServer_SendMessage();
	afx_msg void OnClient_SendMessage();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnServer_GetMessage();
	afx_msg void OnServer_GetMessageAll();
	afx_msg void OnClient_GetMessage();
	afx_msg void OnClient_GetMessageAll();
	afx_msg void OnUpdateServer_AutoMessages(CCmdUI* pCmdUI);
	afx_msg void OnUpdateClient_AutoMessages(CCmdUI* pCmdUI);
	afx_msg void OnServer_AutoMessages();
	afx_msg void OnClient_AutoMessages();
	afx_msg void OnServer_AutoMessagesChange();
	afx_msg void OnClient_AutoMessagesChange();
	afx_msg void OnUpdateServer_GetMessage(CCmdUI* pCmdUI);
	afx_msg void OnUpdateServer_GetMessageAll(CCmdUI* pCmdUI);
	afx_msg void OnUpdateClient_GetMessage(CCmdUI* pCmdUI);
	afx_msg void OnUpdateClient_GetMessageAll(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in MsgTestView.cpp
inline CMsgTestDoc* CMsgTestView::GetDocument()
   { return (CMsgTestDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MSGTESTVIEW_H__057BADB6_D577_11D1_B1AB_0060083C95F5__INCLUDED_)
