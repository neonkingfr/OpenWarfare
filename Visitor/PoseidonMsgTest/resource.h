//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by MsgTest.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_POSEIDONMSGTEST_FORM        101
#define IDD_EDIT_MESSAGE                102
#define IDR_MAINFRAME                   128
#define IDR_POSEIDTYPE                  129
#define IDC_SRV_COUNT_ALL               1000
#define IDC_SRV_COUNT_FNT               1001
#define IDC_SRV_SENDMSG                 1002
#define IDC_CLI_COUNT_ALL               1003
#define IDC_CLI_COUNT_FNT               1004
#define IDC_CLI_SENDMSG                 1005
#define IDC_SRV_GET_AUTO                1006
#define IDC_CLI_GET_AUTO                1007
#define IDC_SRV_GETMSG                  1008
#define IDC_SRV_GETALL                  1009
#define IDC_EDIT_MSGID                  1009
#define IDC_CLI_GETMSG                  1010
#define IDC_CLI_GETALL                  1011
#define IDC_LIST_SERVER                 1012
#define IDC_LIST_CLIENT                 1013
#define IDC_MSGID                       1013
#define IDC_FILENAME                    1014
#define IDC_PATHFROM                    1015
#define IDC_PATHTO                      1016
#define IDC_ID                          1017
#define IDC_NAME                        1018
#define IDC_STATE                       1019
#define IDC_POS00                       1020
#define IDC_POS01                       1021
#define IDC_POS02                       1022
#define IDC_POS03                       1023
#define IDC_POS10                       1024
#define IDC_POS11                       1025
#define IDC_POS12                       1026
#define IDC_POS13                       1027
#define IDC_POS20                       1028
#define IDC_POS21                       1029
#define IDC_POS22                       1030
#define IDC_POS23                       1031
#define IDC_POS30                       1032
#define IDC_POS31                       1033
#define IDC_POS32                       1034
#define IDC_POS33                       1035
#define IDC_CENTERX                     1036
#define IDC_CENTERY                     1037
#define IDC_CENTERZ                     1038
#define IDC_MINX                        1039
#define IDC_MINY                        1040
#define IDC_MINZ                        1041
#define IDC_MAXX                        1045
#define IDC_MAXY                        1046
#define IDC_MAXZ                        1047
#define IDC_NX                          1048
#define IDC_NZ                          1049
#define IDC_Y                           1050
#define IDC_TEXTUREID                   1051
#define ID_SERVER_SENDMSG               32771
#define ID_CLIENT_SENDMSG               32772
#define ID_SERVER_AUTO                  32773
#define ID_SERVER_GETMSG                32774
#define ID_SERVER_GETALL                32775
#define ID_CLIENT_AUTO                  32776
#define ID_CLIENT_GETMSG                32777
#define ID_CLIENT_GETALL                32778

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
