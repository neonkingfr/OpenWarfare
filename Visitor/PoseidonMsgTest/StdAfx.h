// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__057BADB0_D577_11D1_B1AB_0060083C95F5__INCLUDED_)
#define AFX_STDAFX_H__057BADB0_D577_11D1_B1AB_0060083C95F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxtempl.h>

#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxdb.h>			// MFC ODBC database classes
#include <afxdao.h>			// MFC DAO database classes

/////////////////////////////////////////////////////////////
// Mentar libraries

#include <MntTools.h>

#include "..\PoseidonMessage\MessageDll.h"

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__057BADB0_D577_11D1_B1AB_0060083C95F5__INCLUDED_)


