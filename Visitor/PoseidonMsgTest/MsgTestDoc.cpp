// MsgTestDoc.cpp : implementation of the CMsgTestDoc class
//

#include "stdafx.h"
#include "MsgTestApp.h"

#include "MsgTestDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMsgTestDoc

IMPLEMENT_DYNCREATE(CMsgTestDoc, CDocument)

BEGIN_MESSAGE_MAP(CMsgTestDoc, CDocument)
	//{{AFX_MSG_MAP(CMsgTestDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMsgTestDoc construction/destruction

CMsgTestDoc::CMsgTestDoc()
{
	// TODO: add one-time construction code here

}

CMsgTestDoc::~CMsgTestDoc()
{
}

BOOL CMsgTestDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMsgTestDoc serialization

void CMsgTestDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMsgTestDoc diagnostics

#ifdef _DEBUG
void CMsgTestDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMsgTestDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMsgTestDoc commands
