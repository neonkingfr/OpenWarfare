#pragma once
#include "resource.h"

// CDlgOriginOffset dialog

class CDlgOriginOffset : public CDialog
{
	DECLARE_DYNAMIC(CDlgOriginOffset)

public:
	CDlgOriginOffset(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgOriginOffset();

// Dialog Data
	enum { IDD = IDD_ORIGIN_OFFSET };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	double m_OriginX;
	double m_OriginY;
};
