/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// About dialog

void DoAboutPosEdApp(int nIDR)
{	
	DoMntAboutApplicationSheet(ID_ABOUT_LOGO,nIDR,0x0FFF); 
}

/////////////////////////////////////////////////////////////////////////////
// COptionsAppPage dialog

class COptionsAppPage : public CMntPropertyPage
{
// Construction
public:
	COptionsAppPage();

// Dialog Data
	//{{AFX_DATA(COptionsAppPage)
	enum { IDD = IDD_OPTIONS_APP_PAGE };
	BOOL	m_bMaximizeApp;
	int		m_nBaseWorldWidth;
	int		m_nBaseWorldHeight;
	int		m_nMoveWindowCoef;
	double	m_nInOutScaleStep;
	int		m_nTxtrFormat;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(COptionsAppPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(COptionsAppPage)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

COptionsAppPage::COptionsAppPage() : CMntPropertyPage(COptionsAppPage::IDD)
{
	//{{AFX_DATA_INIT(COptionsAppPage)
	m_bMaximizeApp = FALSE;
	m_nTxtrFormat  = 0;
	//}}AFX_DATA_INIT
}

void COptionsAppPage::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COptionsAppPage)
	DDX_Check(pDX, IDC_MAXIMIZED_APP, m_bMaximizeApp);
	DDX_CBIndex(pDX, IDC_TXTR_FORMAT, m_nTxtrFormat);
	//}}AFX_DATA_MAP
	MDDX_Text(pDX, IDC_EDIT_STDWIDTH, m_nBaseWorldWidth);
	MDDV_MinMaxInt(pDX, m_nBaseWorldWidth, MIN_WORLD_SIZE, MAX_WORLD_SIZE, IDC_EDIT_STDWIDTH);
	MDDX_Text(pDX, IDC_EDIT_STDHEIGHT, m_nBaseWorldHeight);
	MDDV_MinMaxInt(pDX, m_nBaseWorldHeight, MIN_WORLD_SIZE, MAX_WORLD_SIZE, IDC_EDIT_STDHEIGHT);

	MDDX_Text(pDX, IDC_EDIT_MOVE_COEF, m_nMoveWindowCoef);
	MDDV_MinMaxInt(pDX, m_nMoveWindowCoef, 10, 100, IDC_EDIT_MOVE_COEF,10);
	MDDX_Text(pDX, IDC_EDIT_SCALE_COEF, m_nInOutScaleStep);
	MDDV_MinMaxDouble(pDX, m_nInOutScaleStep, 1., 5., IDC_EDIT_SCALE_COEF,0.1);
}


BEGIN_MESSAGE_MAP(COptionsAppPage, CMntPropertyPage)
	//{{AFX_MSG_MAP(COptionsAppPage)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COptionsWndPage dialog

class COptionsWndPage : public CMntPropertyPage
{
// Construction
public:
	COptionsWndPage();

// Dialog Data
	//CViewStyle	m_ViewStyle;
	//{{AFX_DATA(COptionsWndPage)
	enum { IDD = IDD_OPTIONS_WND_PAGE };
	int		m_nBgnScale;
	BOOL	m_bFitWindow;
	BOOL	m_bMaxWindow;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(COptionsWndPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(COptionsWndPage)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

COptionsWndPage::COptionsWndPage() : CMntPropertyPage(COptionsWndPage::IDD)
{
	//{{AFX_DATA_INIT(COptionsWndPage)
	//}}AFX_DATA_INIT
}

void COptionsWndPage::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COptionsWndPage)
	DDX_Text(pDX, IDC_EDIT_BGN_SCALE, m_nBgnScale);
	DDV_MinMaxInt(pDX, m_nBgnScale, 10, 500);
	DDX_Check(pDX, IDC_FIT_SCALE, m_bFitWindow);
	DDX_Check(pDX, IDC_MAXIMIZED_WND, m_bMaxWindow);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COptionsWndPage, CMntPropertyPage)
	//{{AFX_MSG_MAP(COptionsWndPage)	
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



/////////////////////////////////////////////////////////////////////////////
// COptionsBldPage dialog

class COptionsBldPage : public CMntPropertyPage
{
// Construction
public:
	COptionsBldPage();

// Dialog Data
	//{{AFX_DATA(COptionsBldPage)
	enum { IDD = IDD_OPTIONS_BLD_PAGE };
	int		m_nBldCursorUpdate;
	BOOL	m_bBldSynchrAction;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(COptionsBldPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(COptionsBldPage)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

COptionsBldPage::COptionsBldPage() : CMntPropertyPage(COptionsBldPage::IDD)
{
	//{{AFX_DATA_INIT(COptionsBldPage)
	//}}AFX_DATA_INIT
}

void COptionsBldPage::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COptionsBldPage)
	DDX_CBIndex(pDX, IDC_CURSOR_UPDATE, m_nBldCursorUpdate);
	DDX_Check(pDX, IDC_SYNCHRO_CURSOR, m_bBldSynchrAction);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COptionsBldPage, CMntPropertyPage)
	//{{AFX_MSG_MAP(COptionsBldPage)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Volby programu a syst�mu (DlgOptions.cpp)

BOOL DoSetupPosEdOptions()
{
	// p��stup k options
	CPosEdEnvironment*	pEnvir = GetPosEdEnvironment();
	if (pEnvir == NULL) return FALSE;

	CMntPropertySheet	dlg(IDS_OPTIONS_TITLE_DLG,PSH_NOAPPLYNOW);
	// sr�nky options
	COptionsAppPage		appPage;
	COptionsWndPage		wndPage;
	//COptionsBldPage		bldPage;
	dlg.AddPageIntoWizard(&appPage);
	dlg.AddPageIntoWizard(&wndPage);
	//dlg.AddPageIntoWizard(&bldPage);

	// p�enos dat
	appPage.m_bMaximizeApp		= pEnvir->m_optPosEd.m_bMaximizeApp;
	appPage.m_nBaseWorldWidth	= pEnvir->m_optPosEd.m_nBaseWorldWidth;
	appPage.m_nBaseWorldHeight	= pEnvir->m_optPosEd.m_nBaseWorldHeight;
	appPage.m_nTxtrFormat		= pEnvir->m_optPosEd.m_nTextureFormat;
	appPage.m_nMoveWindowCoef	= pEnvir->m_optPosEd.m_nMoveWindowCoef;
	appPage.m_nInOutScaleStep	= pEnvir->m_optPosEd.m_nInOutScaleStep;

	wndPage.m_nBgnScale			= pEnvir->m_optPosEd.m_nProjScale;
	wndPage.m_bFitWindow		= pEnvir->m_optPosEd.m_bFitProjWnd;
	wndPage.m_bMaxWindow		= pEnvir->m_optPosEd.m_bMaxProjWnd;
	//wndPage.m_ViewStyle.CopyFrom(&pEnvir->m_optPosEd.m_ViewStyle);

	//bldPage.m_nBldCursorUpdate  = pEnvir->m_optPosEd.m_nBldCursorUpdate;
	//bldPage.m_bBldSynchrAction  = pEnvir->m_optPosEd.m_bBldSynchrAction;


	if (dlg.DoModal()!=IDOK) return FALSE;
	// p�enos dat
	pEnvir->m_optPosEd.m_bMaximizeApp		= appPage.m_bMaximizeApp;
	pEnvir->m_optPosEd.m_nBaseWorldWidth	= appPage.m_nBaseWorldWidth;
	pEnvir->m_optPosEd.m_nBaseWorldHeight	= appPage.m_nBaseWorldHeight;
	pEnvir->m_optPosEd.m_nTextureFormat		= appPage.m_nTxtrFormat;
	pEnvir->m_optPosEd.m_nMoveWindowCoef	= appPage.m_nMoveWindowCoef;
	pEnvir->m_optPosEd.m_nInOutScaleStep	= appPage.m_nInOutScaleStep;

	pEnvir->m_optPosEd.m_nProjScale			= wndPage.m_nBgnScale;
	pEnvir->m_optPosEd.m_bFitProjWnd		= wndPage.m_bFitWindow;
	pEnvir->m_optPosEd.m_bMaxProjWnd		= wndPage.m_bMaxWindow;
	//pEnvir->m_optPosEd.m_ViewStyle.CopyFrom(&wndPage.m_ViewStyle);

	//pEnvir->m_optPosEd.m_nBldCursorUpdate	= bldPage.m_nBldCursorUpdate;
	//pEnvir->m_optPosEd.m_bBldSynchrAction	= bldPage.m_bBldSynchrAction;

	// ulo�en� atribut�
	pEnvir->m_optPosEd.SaveParams();
	return TRUE;
};




