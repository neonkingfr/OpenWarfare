// SplashLicense.cpp : implementation file
//

#include "stdafx.h"
#include "SplashLicenseDlg.h"

#include <string>

using std::string;

// CSplashLicenseDlg dialog

IMPLEMENT_DYNAMIC(CSplashLicenseDlg, CDialog)

CSplashLicenseDlg* CSplashLicenseDlg::m_SplashLicenseDlg;

CSplashLicenseDlg::CSplashLicenseDlg(CWnd* pParent /*=NULL*/)
: CDialog(CSplashLicenseDlg::IDD, pParent)
{
}

CSplashLicenseDlg::~CSplashLicenseDlg()
{
}

void CSplashLicenseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LICENSE_TEXT, m_License_Text);
	DDX_Control(pDX, IDC_CHECK_AGREE, m_Agreed);
}

BEGIN_MESSAGE_MAP(CSplashLicenseDlg, CDialog)
	ON_BN_CLICKED(IDC_CHECK_AGREE, &CSplashLicenseDlg::OnBnClickedCheckAgree)
	ON_BN_CLICKED(IDOK, &CSplashLicenseDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CSplashLicenseDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


BOOL CSplashLicenseDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();

	CenterWindow();	
	SetWindowPos(&CWnd::wndTopMost, 0, 0, 0, 0,  SWP_NOMOVE | SWP_NOSIZE);

	string license;

#ifdef __ONLY_FOR_PUBLIC__
	// *******************
	// PUBLIC EDITION ONLY
	// *******************
	license += "LICENSE AGREEMENT\n\n";
	license += "THIS SOFTWARE IS PROVIDED TO YOU BY BOHEMIA INTERACTIVE FREE OF CHARGE FOR YOUR PERSONAL USE\n";
	license += "AND TO CREATE NON-COMMERCIAL GAME CONTENT FOR BOHEMIA INTERACTIVE'S PRODUCTS.\n";
	license += "ANY COMMERCIAL USE IS PROHIBITED WITHOUT PERMISSION.\n\n";
	license += "IMPORTANT - READ CAREFULLY:\n\n";
	license += "YOU SHOULD CAREFULLY READ THE FOLLOWING END-USER LICENSE AGREEMENT BEFORE INSTALLING\n";
	license += "THIS SOFTWARE PROGRAM.\n\n"; 
	license += "This computer software program, any printed materials, any on-line or electronic documentation,\n"; 
	license += "and any and all copies and derivative works of such software program and materials (the �Program�)\n";
	license += "are the copyrighted work. All use of the Program is governed by the copyright law and by the terms\n";
	license += "of the End-User License Agreement, which is provided below (�License�). By using the Program you\n";
	license += "agree to be legally bound by the terms of this license agreement. Any use, reproduction or\n";
	license += "redistribution of the Program not in accordance with the terms of the License is expressly\n";
	license += "prohibited. If you do not agree to the terms of this Agreement, do not install or use the Program.\n\n";
	license += "1. Ownership:\n";
	license += "All title, ownership rights and intellectual property rights in and to the Program and any and all\n";
	license += "copies thereof (including but not limited to any titles, computer code, themes, objects, methods\n";
	license += "of operation, any related documentation, and addons incorporated into the Program) are owned by\n";
	license += "Bohemia Interactive a.s. (the Licensor) or its licensors. The Program is protected by the Czech\n";
	license += "copyright laws, international copyright treaties and conventions and any other applicable laws.\n";
	license += "All rights are reserved.\n\n";
	license += "2. Limited Use of License:\n";
	license += "the Licensor hereby grants, and by installing the Program you thereby accept, a restricted,\n";
	license += "non-exclusive license and right to install and use one (1) copy of the Program for your personal\n";
	license += "use. You may not network the Program or otherwise install it or use it on more than one computer\n";
	license += "at a time, except if expressly authorized otherwise in the applicable documentation. The licensor\n";
	license += "also specifically prohibits the use of the Software for other purpose than designing, developing,\n";
	license += "testing, and producing non-commercial game content for computer games developed by the Licensor only.\n";
	license += "The Program is licensed and your license confers no title or ownership in the Program.\n\n";
	license += "3. End User's Obligations:\n";
	license += "A. As a Subject to the Grant of License herein above, you may not, in whole or in part, copy, duplicate,\n";
	license += "   reproduce, translate, reverse-engineer, modify, disassemble, decompile, derive source code, create\n";
	license += "   derivative works based on the Program, remove any proprietary notices or labels from the Program or\n";
	license += "   otherwise modify the Program without the prior written consent of the Licensor.\n";
	license += "B. You are entitled to use the Program for your own use, but you are not entitled to:\n";
	license += "   (i) Sell or transfer reproductions of the Program to other parties in any way, nor to rent, lease\n";
	license += "       or license the Program to others;\n";
	license += "   (ii) Publish and/or distribute the computer Program or any of its parts;\n";
	license += "   (iii) Exploit the Program or any of its parts for any commercial purpose including, but not limited\n";
	license += "         to, use at a cybercaf�, computer gaming center, computer aided training center or any other\n";
	license += "         location-based site where multiple users may access the Program;\n";
	license += "   (iv) Commercially exploit or allow a 3rd party commercially exploit game content you created using\n";
	license += "        the Software, including but not limited to use by military organizations for computer aided\n";
	license += "        training or commercially released game content;\n\n";
	license += "4. No Support:\n";
	license += "You acknowledge and agree that BI is providing you the Software free of charge in order to allow you\n";
	license += "creation of non-commercial content for BI's gaming products only and you agree to not commercially\n";
	license += "exploit any game content you may create using the Software without BI�s prior written permission.\n\n";
	license += "5. License Transfer:\n";
	license += "You may permanently transfer all of your rights under this License to the recipient, provided that the\n";
	license += "recipient agrees to the terms of this License and you remove the Program from your computer.\n\n";
	license += "6. Termination:\n";
	license += "This License is effective until terminated. You may terminate the License at any time by destroying the\n";
	license += "Program and any New Material. The Licensor may, at its discretion, terminate this License in the event\n";
	license += "that you fail to comply with the terms and conditions contained herein. In such event, you must immediately\n";
	license += "destroy the Program and any New Material.\n\n";
	license += "7. Limited Warranty:\n";
	license += "THE LICENSOR EXPRESSLY DISCLAIMS ANY WARRANTY FOR THE PROGRAM. THE PROGRAM IS PROVIDED �AS IS�\n";
	license += "WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, THE\n";
	license += "IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT.\n";
	license += "The entire risk arising out of use or performance of the Program remains with you.\n\n";
	license += "8. Limitation of Liability:\n";
	license += "NEITHER THE LICENSOR, ITS PARENT, SUBSIDIARIES, AFFILIATES OR LICENSORS SHALL BE LIABLE IN ANY\n";
	license += "WAY FOR LOSS OR DAMAGE OF ANY KIND RESULTING FROM THE USE OF THE PROGRAM, INCLUDING BUT NOT\n";
	license += "LIMITED TO LOSS OF GOODWILL, WORK STOPPAGE, COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL\n";
	license += "OTHER COMMERCIAL DAMAGE OR LOSSES.\n\n";
	license += "9. Miscellaneous:\n";
	license += "The License shall be deemed to have been made and executed in the Czech Republic, and any dispute arising\n";
	license += "hereunder shall be resolved in accordance with the Czech law. You hereby acknowledge that you have read and\n";
	license += "understand the foregoing License and agree that the action of installing the Program is an acknowledgment of\n";
	license += "your agreement to be bound by the terms and conditions of the License contained herein. You also acknowledge\n";
	license += "and agree that this License is the complete and exclusive statement of the agreement between\n";
	license += "the Licensor and you.\n\n";
	license += "Copyright � 2009 Bohemia Interactive. All rights reserved.";
#else
	#ifdef __ONLY_FOR_VBS__
	// ****************
	// VBS EDITION ONLY
	// ****************
	license += "LICENSE AGREEMENT\n\n";
	license += "IMPORTANT - READ CAREFULLY:\n";
	license += "\n";
	license += "YOU SHOULD CAREFULLY READ THE FOLLOWING END-USER LICENSE AGREEMENT BEFORE INSTALLING\n";
	license += "THIS SOFTWARE PROGRAM.\n\n"; 
	license += "This computer software program, any printed materials, any on-line or electronic documentation,\n"; 
	license += "and any and all copies and derivative works of such software program and materials (the �Program�)\n";
	license += "are the copyrighted work. All use of the Program is governed by the copyright law and by the terms\n";
	license += "of the End-User License Agreement, which is provided below (�License�). By using the Program you\n";
	license += "agree to be legally bound by the terms of this license agreement. Any use, reproduction or\n";
	license += "redistribution of the Program not in accordance with the terms of the License is expressly\n";
	license += "prohibited. If you do not agree to the terms of this Agreement, do not install or use the Program.\n\n";
	license += "1. Ownership:\n";
	license += "All title, ownership rights and intellectual property rights in and to the Program and any and all\n";
	license += "copies thereof (including but not limited to any titles, computer code, themes, objects, methods\n";
	license += "of operation, any related documentation, and addons incorporated into the Program) are owned by\n";
	license += "Bohemia Interactive a.s. (the Licensor) or its licensors. The Program is protected by the Czech\n";
	license += "copyright laws, international copyright treaties and conventions and any other applicable laws.\n";
	license += "All rights are reserved.\n\n";
	license += "2. Limited Use of License:\n";
	license += "The Licensor hereby grants, and by installing the Program you thereby accept, a restricted,\n";
	license += "non-exclusive license and right to install and use one (1) copy of the Program for your personal\n";
	license += "use. You may not network the Program or otherwise install it or use it on more than one computer\n";
	license += "at a time, except if expressly authorized otherwise in the applicable documentation.\n";
	license += "The Program is licensed and your license confers no title or ownership in the Program.\n";
	license += "\n";
	license += "3. End User's Obligations:\n";
	license += "A. As a Subject to the Grant of License herein above, you may not, in whole or in part, copy,\n";
	license += "   duplicate, reproduce, translate, reverse-engineer,modify, disassemble, decompile, derive source\n";
	license += "   code, create derivative works based on the Program, remove any proprietary notices or labels from\n";
	license += "   the Program or otherwise modify the Program without the prior written consent of the Licensor.\n";
	license += "B. You are entitled to use the Program for your own use, but you are not entitled to:\n";
	license += "   (i)  Sell or transfer reproductions of the Program to other parties in any way, nor to rent,\n";
	license += "        lease or license the Program to others;\n";
	license += "   (ii) Publish and/or distribute the computer Program or any of its parts.\n";
	license += "\n";
	license += "4. License Transfer:\n";
	license += "You may permanently transfer all of your rights under this License to the recipient, provided that\n";
	license += "the recipient agrees to the terms of this License and you remove the Program from your computer.\n";
	license += "\n";
	license += "5. Termination:\n";
	license += "This License is effective until terminated. You may terminate the License at any time by destroying\n";
	license += "the Program and any New Material. The Licensor may, at its discretion, terminate this License in the\n";
	license += "event that you fail to comply with the terms and conditions contained herein. In such event, you must\n";
	license += "immediately destroy the Program and any New Material.\n";
	license += "\n";
	license += "6. Limited Warranty:\n";
	license += "THE LICENSOR EXPRESSLY DISCLAIMS ANY WARRANTY FOR THE PROGRAM.\n";
	license += "THE PROGRAM IS PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND,\n";
	license += "EITHER EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES\n";
	license += "OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT.\n";
	license += "The entire risk arising out of use or performance of the Program remains with you.\n";
	license += "\n";
	license += "7. Limitation of Liability:\n";
	license += "NEITHER THE LICENSOR, ITS PARENT, SUBSIDIARIES, AFFILIATES OR LICENSORS\n";
	license += "SHALL BE LIABLE IN ANY WAY FOR LOSS OR DAMAGE OF ANY KIND RESULTING FROM THE USE\n";
	license += "OF THE PROGRAM, INCLUDING BUT NOT LIMITED TO LOSS OF GOODWILL, WORK STOPPAGE,\n";
	license += "COMPUTER FAILURE OR MALFUNCTION, OR ANY AND ALL OTHER COMMERCIAL DAMAGE OR LOSSES.\n";
	license += "\n";
	license += "8. Miscellaneous:\n";
	license += "The License shall be deemed to have been made and executed in the Czech Republic, and any dispute\n";
	license += "arising hereunder shall be resolved in accordance with the Czech law. You hereby acknowledge that you\n";
	license += "have read and understand the foregoing License and agree that the action of installing the Program is\n";
	license += "an acknowledgment of your agreement to be bound by the terms and conditions of the License contained\n";
	license += "herein. You also acknowledge and agree that this License is the complete and exclusive statement of the\n";
	license += "agreement between the Licensor and you.\n";
	license += "\n";
	license += "Copyright � 2009 Bohemia Interactive. All rights reserved.";
	#else
	#endif
#endif

	m_License_Text.SetWindowTextA(license.c_str());

	return TRUE;
}

// CSplashLicense message handlers

void CSplashLicenseDlg::OnBnClickedCheckAgree()
{
	if (m_Agreed.GetCheck() == TRUE)
	{
		GetDlgItem(IDOK)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDOK)->EnableWindow(FALSE);
	}
}

void CSplashLicenseDlg::OnBnClickedOk()
{
	OnOK();
}

void CSplashLicenseDlg::OnBnClickedCancel()
{
	OnCancel();
}
