/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright © Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditNetPartSTRADlg dialog

class CEditNetPartSTRADlg : public CDialog
{
	// Construction
public:
	CEditNetPartSTRADlg(CWnd* pParent = NULL);   // standard constructor
	
	// Dialog Data
	CNetSTRA	m_Obj;

	//{{AFX_DATA(CEditNetPartSTRADlg)
	enum { IDD = IDD_NETPART_EDIT_STRA };
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditNetPartSTRADlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CEditNetPartSTRADlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedFileBrowse();
};

CEditNetPartSTRADlg::CEditNetPartSTRADlg(CWnd* pParent /*=NULL*/)
: CDialog(CEditNetPartSTRADlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditNetPartSTRADlg)
	//}}AFX_DATA_INIT
}

void CEditNetPartSTRADlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditNetPartSTRADlg)
	//}}AFX_DATA_MAP
	
	if (SAVEDATA)
	{
		int nType;
		DDX_CBIndex(pDX, IDC_COMBO_TYPE, nType);
		m_Obj.m_nSTRAType = (WORD)nType;
	} 
	else
	{
		int nType = (int)m_Obj.m_nSTRAType;
		DDX_CBIndex(pDX, IDC_COMBO_TYPE, nType);
	}

	DDX_Text(pDX, IDC_EDIT_NAME, m_Obj.m_sCompName);
	MDDV_MaxChars(pDX, m_Obj.m_sCompName, MAX_OBJC_NAME_LEN, IDC_EDIT_NAME);
	MDDV_MinChars(pDX, m_Obj.m_sCompName, 1);

	DDX_Check(pDX, IDC_CHECK_CHNGHGHT, m_Obj.m_bChngHeight);
	// zobrazení objektu
	if (!SAVEDATA)
	{
		CString sText;
		if (m_Obj.GetObjectTemplate() != NULL) sText = m_Obj.GetObjectTemplate()->GetFileName();
		::SetWindowText(DLGCTRL(IDC_SHOW_OBJECT), sText);
	}
}

BEGIN_MESSAGE_MAP(CEditNetPartSTRADlg, CDialog)
	//{{AFX_MSG_MAP(CEditNetPartSTRADlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_FILE_BROWSE, OnBnClickedFileBrowse)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditNetPartBENDDlg dialog

class CEditNetPartBENDDlg : public CDialog
{
	// Construction
public:
	CEditNetPartBENDDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
	CNetBEND	m_Obj;

	//{{AFX_DATA(CEditNetPartBENDDlg)
	enum { IDD = IDD_NETPART_EDIT_BEND };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditNetPartBENDDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEditNetPartBENDDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedFileBrowse();
};

CEditNetPartBENDDlg::CEditNetPartBENDDlg(CWnd* pParent /*=NULL*/)
: CDialog(CEditNetPartBENDDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditNetPartBENDDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

void CEditNetPartBENDDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditNetPartBENDDlg)
	//}}AFX_DATA_MAP

	if (SAVEDATA)
	{
		int nType;
		DDX_CBIndex(pDX, IDC_COMBO_TYPE, nType);
		m_Obj.m_nBENDType = (WORD)nType;
	} 
	else
	{
		int nType = (int)m_Obj.m_nBENDType;
		DDX_CBIndex(pDX, IDC_COMBO_TYPE, nType);
	};

	DDX_Text(pDX, IDC_EDIT_NAME, m_Obj.m_sCompName);
	MDDV_MaxChars(pDX, m_Obj.m_sCompName, MAX_OBJC_NAME_LEN, IDC_EDIT_NAME);
	MDDV_MinChars(pDX, m_Obj.m_sCompName, 1);

	// zobrazení objektu
	if (!SAVEDATA)
	{
		CString sText;
		if (m_Obj.GetObjectTemplate() != NULL) sText = m_Obj.GetObjectTemplate()->GetFileName();
		::SetWindowText(DLGCTRL(IDC_SHOW_OBJECT), sText);
	}
}

BEGIN_MESSAGE_MAP(CEditNetPartBENDDlg, CDialog)
	//{{AFX_MSG_MAP(CEditNetPartBENDDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_FILE_BROWSE, OnBnClickedFileBrowse)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditNetPartSPECDlg dialog

class CEditNetPartSPECDlg : public CDialog
{
// Construction
public:
	CEditNetPartSPECDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
	CNetSPEC	m_Obj;

	//{{AFX_DATA(CEditNetPartSPECDlg)
	enum { IDD = IDD_NETPART_EDIT_SPEC };
	//}}AFX_DATA


	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditNetPartSPECDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEditNetPartSPECDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedFileBrowse();
};

CEditNetPartSPECDlg::CEditNetPartSPECDlg(CWnd* pParent /*=NULL*/)
: CDialog(CEditNetPartSPECDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditNetPartSPECDlg)
	//}}AFX_DATA_INIT
}

void CEditNetPartSPECDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditNetPartSPECDlg)
	//}}AFX_DATA_MAP
	if (SAVEDATA)
	{
		int nType;
		DDX_CBIndex(pDX, IDC_COMBO_TYPE, nType);
		m_Obj.m_nSTRAType = (WORD)nType;
	} 
	else
	{
		int nType = (int)m_Obj.m_nSTRAType;
		DDX_CBIndex(pDX, IDC_COMBO_TYPE, nType);
	}

	DDX_Text(pDX, IDC_EDIT_NAME, m_Obj.m_sCompName);
	MDDV_MaxChars(pDX, m_Obj.m_sCompName, MAX_OBJC_NAME_LEN, IDC_EDIT_NAME);
	MDDV_MinChars(pDX, m_Obj.m_sCompName, 1);

	DDX_Check(pDX, IDC_CHECK_CHNGHGHT, m_Obj.m_bChngHeight);
	// zobrazení objektu
	if (!SAVEDATA)
	{
		CString sText;
		if (m_Obj.GetObjectTemplate() != NULL) sText = m_Obj.GetObjectTemplate()->GetFileName();
		::SetWindowText(DLGCTRL(IDC_SHOW_OBJECT), sText);
	}
}

BEGIN_MESSAGE_MAP(CEditNetPartSPECDlg, CDialog)
	//{{AFX_MSG_MAP(CEditNetPartSPECDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_STN_CLICKED(IDC_FILE_BROWSE, OnBnClickedFileBrowse)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditNetPartTERMDlg dialog

class CEditNetPartTERMDlg : public CDialog
{
	// Construction
public:
	CEditNetPartTERMDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
	CNetTERM	m_Obj;

	//{{AFX_DATA(CEditNetPartTERMDlg)
	enum { IDD = IDD_NETPART_EDIT_TERM };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditNetPartTERMDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEditNetPartTERMDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedFileBrowse();
};

CEditNetPartTERMDlg::CEditNetPartTERMDlg(CWnd* pParent /*=NULL*/)
: CDialog(CEditNetPartTERMDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditNetPartTERMDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CEditNetPartTERMDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditNetPartTERMDlg)
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_EDIT_NAME, m_Obj.m_sCompName);
	MDDV_MaxChars(pDX, m_Obj.m_sCompName, MAX_OBJC_NAME_LEN, IDC_EDIT_NAME);
	MDDV_MinChars(pDX, m_Obj.m_sCompName, 1);

	// zobrazení objektu
	if (!SAVEDATA)
	{
		CString sText;
		if (m_Obj.GetObjectTemplate() != NULL) sText = m_Obj.GetObjectTemplate()->GetFileName();
		::SetWindowText(DLGCTRL(IDC_SHOW_OBJECT), sText);
	}
}

BEGIN_MESSAGE_MAP(CEditNetPartTERMDlg, CDialog)
	//{{AFX_MSG_MAP(CEditNetPartTERMDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_FILE_BROWSE, OnBnClickedFileBrowse)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// EDIT NET PARTS

BOOL DoEditNetPartSTRA(CNetSTRA* pObj)
{
	CEditNetPartSTRADlg dlg;
	dlg.m_Obj.CopyFrom(pObj);
	if (dlg.DoModal() != IDOK) return FALSE;
	pObj->CopyFrom(&(dlg.m_Obj));
	return TRUE;
}

BOOL DoEditNetPartBEND(CNetBEND* pObj)
{
	CEditNetPartBENDDlg dlg;
	dlg.m_Obj.CopyFrom(pObj);
	if (dlg.DoModal() != IDOK) return FALSE;
	pObj->CopyFrom(&(dlg.m_Obj));
	return TRUE;
}

BOOL DoEditNetPartSPEC(CNetSPEC* pObj)
{
	CEditNetPartSPECDlg dlg;
	dlg.m_Obj.CopyFrom(pObj);
	if (dlg.DoModal() != IDOK) return FALSE;
	pObj->CopyFrom(&(dlg.m_Obj));
	return TRUE;
}

BOOL DoEditNetPartTERM(CNetTERM* pObj)
{
	CEditNetPartTERMDlg dlg;
	dlg.m_Obj.CopyFrom(pObj);
	if (dlg.DoModal() != IDOK) return FALSE;
	pObj->CopyFrom(&(dlg.m_Obj));
	return TRUE;
}

void CEditNetPartBENDDlg::OnBnClickedFileBrowse()
{
  CString subDir;
  m_Obj.GetDocument()->GetProjectDirectory(subDir, CPosEdBaseDoc::dirObj);
  MakeShortDirStr(subDir, subDir);

  CString initDir;  
  initDir = GetPosEdEnvironment()->m_optSystem.m_sNetPath;  

  // initDir must be under subdir
  if(subDir.CompareNoCase(initDir.Left(subDir.GetLength())) != 0) initDir = subDir;  

  CString file;
  CString filter(_T("Objects (*.p3d)|*.p3d||"));  
  if (!DoLoadFileFromSubDir(file, subDir, initDir, filter)) return;

  SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sNetPath, file); 
  GetPosEdEnvironment()->m_optSystem.SaveParams();

  int subdirL = subDir.GetLength() + 1;
  if (m_Obj.GetObjectTemplate())
  {
    if (!m_Obj.GetObjectTemplate()->AreSameDataInSourceFile(file)) // do not allow to plug file with different data
    {
      AfxMessageBox(IDS_DIFFERENT_DATA);
      return;
    }
    m_Obj.GetObjectTemplate()->SetFileName(file.Right(file.GetLength() - subdirL));
    m_Obj.GetObjectTemplate()->UpdateParamsFromSourceFile();
    UpdateData(FALSE);
  }
}

void CEditNetPartTERMDlg::OnBnClickedFileBrowse()
{
  CString subDir;
  m_Obj.GetDocument()->GetProjectDirectory(subDir, CPosEdBaseDoc::dirObj);
  MakeShortDirStr(subDir,subDir);

  CString initDir;  
  initDir = GetPosEdEnvironment()->m_optSystem.m_sNetPath;  

  // initDir must be under subdir
  if(subDir.CompareNoCase(initDir.Left(subDir.GetLength())) != 0) initDir = subDir;  

  CString file;
  CString filter(_T("Objects (*.p3d)|*.p3d||"));  
  if (!DoLoadFileFromSubDir(file, subDir, initDir, filter)) return;

  SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sNetPath, file); 
  GetPosEdEnvironment()->m_optSystem.SaveParams();

  int subdirL = subDir.GetLength() + 1;
  if (m_Obj.GetObjectTemplate())
  {
    if (!m_Obj.GetObjectTemplate()->AreSameDataInSourceFile(file)) // do not allow to plug file with different data
    {
      AfxMessageBox(IDS_DIFFERENT_DATA);
      return;
    }
    m_Obj.GetObjectTemplate()->SetFileName(file.Right(file.GetLength() - subdirL));
    m_Obj.GetObjectTemplate()->UpdateParamsFromSourceFile();
    UpdateData(FALSE);
  }
}

void CEditNetPartSTRADlg::OnBnClickedFileBrowse()
{
	CString subDir;
	m_Obj.GetDocument()->GetProjectDirectory(subDir, CPosEdBaseDoc::dirObj);
	MakeShortDirStr(subDir, subDir);

	CString initDir;  
	initDir = GetPosEdEnvironment()->m_optSystem.m_sNetPath;  

	// initDir must be under subdir
	if(subDir.CompareNoCase(initDir.Left(subDir.GetLength())) != 0) initDir = subDir;  

	CString file;
	CString filter(_T("Objects (*.p3d)|*.p3d||"));  
	if (!DoLoadFileFromSubDir(file, subDir, initDir, filter)) return;

	SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sNetPath, file); 
	GetPosEdEnvironment()->m_optSystem.SaveParams();

	int subdirL = subDir.GetLength() + 1;
	if (m_Obj.GetObjectTemplate())
	{
		if (!m_Obj.GetObjectTemplate()->AreSameDataInSourceFile(file)) // do not allow to plug file with different data
		{
			AfxMessageBox(IDS_DIFFERENT_DATA);
			return;
		}
		m_Obj.GetObjectTemplate()->SetFileName(file.Right(file.GetLength() - subdirL));
		m_Obj.GetObjectTemplate()->UpdateParamsFromSourceFile();
		UpdateData(FALSE);
	}
}

void CEditNetPartSPECDlg::OnBnClickedFileBrowse()
{
  CString subDir;
  m_Obj.GetDocument()->GetProjectDirectory(subDir, CPosEdBaseDoc::dirObj);
  MakeShortDirStr(subDir,subDir);

  CString initDir;  
  initDir = GetPosEdEnvironment()->m_optSystem.m_sNetPath;  

  // initDir must be under subdir
  if(subDir.CompareNoCase(initDir.Left(subDir.GetLength())) != 0) initDir = subDir;  

  CString file;
  CString filter(_T("Objects (*.p3d)|*.p3d||"));  
  if (!DoLoadFileFromSubDir(file, subDir, initDir, filter)) return;

  SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sNetPath, file); 
  GetPosEdEnvironment()->m_optSystem.SaveParams();

  int subdirL = subDir.GetLength() + 1;
  if (m_Obj.GetObjectTemplate())
  {
    if (!m_Obj.GetObjectTemplate()->AreSameDataInSourceFile(file)) // do not allow to plug file with different data
    {
      AfxMessageBox(IDS_DIFFERENT_DATA);
      return;
    }
    m_Obj.GetObjectTemplate()->SetFileName(file.Right(file.GetLength() - subdirL));
    m_Obj.GetObjectTemplate()->UpdateParamsFromSourceFile();
    UpdateData(FALSE);
  }
}
