/////////////////////////////////////////////////////////////////////////////
// CEditProjectParamsDlg dialog

class CEditProjectParamsDlg : public CDialog
{
// Construction
public:
	CEditProjectParamsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEditProjectParamsDlg)
	enum { IDD = IDD_EDIT_PROJECT_PARAMS };
	//}}AFX_DATA
	CString	m_sTexturesPath;
	CString	m_sObjectsPath;
	CString m_sBaseDir;
	CString m_configName; 

	// ---------------------------------------------------------- //
	// these lines are no more required after the introduction of //
	// the new project params dialog                              //
	// ---------------------------------------------------------- //
/*
	/// primary grid size (mask grid)
	float m_nRealSizeUnit;
	/// satellite grid size (one physical texture per grid)
	int m_satelliteGridG;
*/

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditProjectParamsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEditProjectParamsDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

