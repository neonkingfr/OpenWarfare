#pragma once

#include "DlgProjectParameters.h"

// CDlgSatelliteGridCalc dialog

class CDlgSatelliteGridCalc : public CDialog
{
	DECLARE_DYNAMIC(CDlgSatelliteGridCalc)

public:
	CDlgSatelliteGridCalc(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgSatelliteGridCalc();

// Dialog Data
	enum { IDD = IDD_SATELLITE_GRID_CALC };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	// map size parameters
	float    m_fSquareSize;
	int      m_nWorldSquareWidth;

	// satellite params
	int m_satelliteGridG;
	int m_segmentSizePixel;
	int m_segmentOverlapPixel;
	int m_imageSizePixel;

	float m_proposedSegmentOverlapPixel;

	int m_activeTextureLayerIndex;
public:
	virtual BOOL OnInitDialog();
protected:
	void ComputeSatelliteValues();
	void UpdateValues();
public:
	afx_msg void OnBnClickedApplyBtn();
	afx_msg void OnEnChangeSatImageEdit();
	afx_msg void OnEnChangeSatImageSegSizePx();
	afx_msg void OnEnChangeSatImageSegOverPx();
};
