#include "stdafx.h"
#include "resource.h"
#include <float.h>
#include <io.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//////////////////////////////////////////
// File with several small functions

//////////////////////////////////////////
// CheckFile

bool CheckFileAccess(const CString& fileName, BOOL write, BOOL mustExist, BOOL messageBox)
{
   if (_access(fileName,0) != 0)
   {
     if (!mustExist)
       return true;
     // file does not exist
     if (messageBox)
     {
       CString text;
       text.Format(IDS_FILE_NOT_EXIST,fileName);
       AfxMessageBox(text);
     }
     return false;
   }
     
   if (write)
   {
     if  (_access(fileName,02) != 0)
     {
       if (messageBox)
       {
         CString text;
         text.Format(IDS_FILE_READONLY,fileName);
         AfxMessageBox(text);
       }
       return false;
     }
   }
   else
   {
     if  (_access(fileName,04) != 0)
     {
       if (messageBox)
       {
         CString text;
         text.Format(IDS_FILE_WRITEONLY,fileName);
         AfxMessageBox(text);
       }
       return false;
     }
   }
   return true;
}