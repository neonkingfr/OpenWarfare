/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"
#include <float.h>

// -------------------------------------------------------- //
// this include is needed for the new project params dialog //
// -------------------------------------------------------- //
#include "DlgProjectParameters.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// --------------------------------------------------- //
// the following implementation have been moved in the //
// CNumeEdit.h and CNumEdit.cpp files                  //
// --------------------------------------------------- //
/*

/////////////////////////////////////////////////////////////////////////////
// CNumEdit window

class CNumEdit : public CEdit
{
protected:
  CString m_lastString;
  // Construction
public:
  CNumEdit();
  // Attributes
  enum {VALID = 0x00, OUT_OF_RANGE = 0x01, INVALID_CHAR = 0x02};
  // Operations
protected:
  int IsValid(const CString &str);
  // Implementation
public:
  void GetRange(float &max, float &min);
  void SetRange(float max, float min);
  void Verbose(BOOL v);
  BOOL Verbose();
  BOOL OnChange();  
  BOOL IsValid();
  void SetValue(float val);
  float GetValue();
  virtual ~CNumEdit();

  // Generated message map functions
protected:
  BOOL m_Verbose;
  float m_MinValue, m_MaxValue; 

  DECLARE_MESSAGE_MAP()

};

/////////////////////////////////////////////////////////////////////////////
// CNumEdit

CNumEdit::CNumEdit()
{
  m_Verbose = FALSE;
  m_MinValue = -FLT_MAX;
  m_MaxValue = FLT_MAX;
}

CNumEdit::~CNumEdit()
{
}

BEGIN_MESSAGE_MAP(CNumEdit, CEdit)  
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNumEdit message handlers

float CNumEdit::GetValue()
{
  float f = 0;  
  CString str;
  GetWindowText(str);
  if (IsValid(str) == VALID)
  {
    sscanf(str, "%f", &f);
  }
  return f;
}

void CNumEdit::SetValue(float val)
{
  CString str;
  str.Format("%.1f", val);
  SetWindowText(str);
}

BOOL CNumEdit::IsValid()
{
  CString str;
  GetWindowText(str);
  return (VALID == IsValid(str));
}

int CNumEdit::IsValid(const CString &str)
{
  //ASSERT(m_MinValue >= m_MaxValue);
  int res = VALID;
  float f;
  if (sscanf(str, "%f", &f) != 1) res = INVALID_CHAR;
  if (f > m_MaxValue || f < m_MinValue) res = OUT_OF_RANGE;
  if (m_Verbose && res != VALID)
  {
    CString msg;
    msg.Empty();
    if (res & OUT_OF_RANGE) msg += _T("Given value is out of range.\n");
    if (res & INVALID_CHAR) msg += _T("Characters must be a number.\n");
    AfxMessageBox(msg, MB_OK | MB_ICONSTOP);
    SetFocus();
  }
  return res;
}

BOOL CNumEdit::OnChange() 
{  
  CString newstr;
  GetWindowText(newstr);  
  if (IsValid(newstr) != VALID)
  {
    //SetWindowText(m_lastString);
    //SetSel(0, -1);
    return FALSE;    
  }
  else
  {
    //m_lastString = newstr;
    return TRUE;
  }
}

BOOL CNumEdit::Verbose()
{
  return m_Verbose;
}

void CNumEdit::Verbose(BOOL v)
{
  m_Verbose = v;
}

void CNumEdit::SetRange(float max, float min)
{
  m_MinValue = min;
  m_MaxValue = max;
}

void CNumEdit::GetRange(float & max, float & min)
{
  min = m_MinValue;
  max = m_MaxValue;
}

*/

/////////////////////////////////////////////////////////////////////////////
// CNewProjectDlg dialog

class CNewProjectDlg : public CDialog
{
private:
  bool m_bIgnoreEnChange;
// Construction
public:
	CNewProjectDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNewProjectDlg)
	enum { IDD = IDD_NEW_PRJ_DLG };
	float		m_fWorldWantedHeight;
	float		m_fWorldWantedWidth;
  float   m_fSquareSize;

  int m_nWorldSquareWidth;
  int m_nWorldSquareHeight;
  float m_fWorldHeight;
  float m_fWorldWidth;

  CNumEdit m_cWorldWantedWidth;
  CNumEdit m_cSquareSize;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewProjectDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CNewProjectDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
//  afx_msg void OnBnClickedRecalc();
protected:
  virtual void OnOK();
public:
  virtual BOOL OnInitDialog();
  afx_msg void OnEnChangeEditWrldWidth();
  afx_msg void OnEnChangeEditSqSize();
  afx_msg void OnBnClickedRecalc();
};

static const float MIN_WORLD_REAL_SIZE = (MIN_WORLD_SIZE * MIN_REAL_SIZE);
static const float MAX_WORLD_REAL_SIZE = (MAX_WORLD_SIZE * MAX_REAL_SIZE);

CNewProjectDlg::CNewProjectDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNewProjectDlg::IDD, pParent)
{ 
  m_bIgnoreEnChange = false;
}

void CNewProjectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewProjectDlg)
	//DDX_Text(pDX, IDC_EDIT_WRLD_WIDTH, m_fWorldWantedWidth);
  //DDV_MinMaxFloat(pDX, m_fWorldWantedWidth, MIN_WORLD_REAL_SIZE, MAX_WORLD_REAL_SIZE);

  DDX_Control(pDX, IDC_EDIT_WRLD_WIDTH, m_cWorldWantedWidth);

	DDX_Text(pDX, IDC_EDIT_WRLD_HEIGHT, m_fWorldWantedHeight);
	//DDV_MinMaxFloat(pDX, m_fWorldWantedHeight, MIN_WORLD_REAL_SIZE, MAX_WORLD_REAL_SIZE);
  
  //DDX_Text(pDX, IDC_EDIT_SQ_SIZE, m_fSquareSize);
  //DDV_MinMaxFloat(pDX, m_fSquareSize, MIN_REAL_SIZE, MAX_REAL_SIZE);

  DDX_Control(pDX, IDC_EDIT_SQ_SIZE, m_cSquareSize);

  DDX_Text(pDX, IDC_EDIT_WRLD_WIDTH2, m_fWorldWidth);
  //DDV_MinMaxFloat(pDX, m_fWorldWidth, MIN_WORLD_REAL_SIZE, MAX_WORLD_REAL_SIZE);
  DDX_Text(pDX, IDC_EDIT_WRLD_HEIGHT2, m_fWorldHeight);
  //DDV_MinMaxFloat(pDX, m_fWorldHeight, MIN_WORLD_REAL_SIZE, MAX_WORLD_REAL_SIZE);

  DDX_Text(pDX, IDC_EDIT_WRLD_SQ_WIDTH, m_nWorldSquareWidth);
  //DDV_MinMaxInt(pDX, m_nWorldSquareWidth, MIN_WORLD_SIZE, MAX_WORLD_SIZE);
  DDX_Text(pDX, IDC_EDIT_WRLD_SQ_HEIGHT, m_nWorldSquareHeight);
  //DDV_MinMaxInt(pDX, m_nWorldSquareHeight, MIN_WORLD_SIZE, MAX_WORLD_SIZE);

	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewProjectDlg, CDialog)
	//{{AFX_MSG_MAP(CNewProjectDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP

//  ON_EN_CHANGE(IDC_EDIT_SQ_SIZE, OnBnClickedRecalc)  
  ON_EN_CHANGE(IDC_EDIT_WRLD_WIDTH, OnEnChangeEditWrldWidth)
  ON_EN_CHANGE(IDC_EDIT_SQ_SIZE, OnEnChangeEditSqSize)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Parametry nov�ho projektu	

BOOL OnNewPoseidonProject(CPosEdMainDoc* pDoc)
{
	// p��stup k options
	CPosEdEnvironment*	pEnvir = GetPosEdEnvironment();
	if (pEnvir == NULL) return FALSE;

// ---------------------------------------- //
// this is the new code which uses the new  //
// project params dialog                    //
// ---------------------------------------- //

	CDlgProjectParameters dlg;

	// map size
	dlg.m_fSquareSize            = pDoc->m_nRealSizeUnit; // this value is defined first in CPoseidonDoc::OnNewDocument
	dlg.m_nWorldSquareWidth      = pEnvir->m_optPosEd.m_nBaseWorldWidth;
	dlg.m_nWorldSquareHeight     = pEnvir->m_optPosEd.m_nBaseWorldHeight;
	dlg.m_bAllowMapSizeEdit      = true;
	dlg.m_bAllowTextureLayerEdit = false;
	dlg.m_pDoc                   = pDoc;

	if (dlg.DoModal() != IDOK) return FALSE;

	pDoc->m_nRealSizeUnit  = dlg.m_fSquareSize;
	pDoc->m_satelliteGridG = dlg.m_satelliteGridG;
	if (!pDoc->m_PoseidonMap.OnNewMap(dlg.m_nWorldSquareWidth, dlg.m_nWorldSquareHeight)) return FALSE; 

// ---------------------------------------- //
// this is the old code which used the old  //
// starting dialog                          //
// ---------------------------------------- //
/*
	CNewProjectDlg	dlg;
	dlg.m_fSquareSize = 50;

	dlg.m_nWorldSquareWidth  = pEnvir->m_optPosEd.m_nBaseWorldWidth;
	dlg.m_nWorldSquareHeight = pEnvir->m_optPosEd.m_nBaseWorldHeight;

	dlg.m_fWorldWidth = dlg.m_fWorldWantedWidth = dlg.m_nWorldSquareWidth * dlg.m_fSquareSize;
	dlg.m_fWorldHeight = dlg.m_fWorldWantedHeight = dlg.m_nWorldSquareHeight * dlg.m_fSquareSize;

	if (dlg.DoModal()!=IDOK)
		return FALSE;

	// inicializace dokumentu
	pDoc->m_nRealSizeUnit = dlg.m_fSquareSize;
	// TODO: check dlg
	pDoc->m_satelliteGridG = 7;
	if (!pDoc->m_PoseidonMap.OnNewMap(dlg.m_nWorldSquareWidth,dlg.m_nWorldSquareHeight))
		return FALSE; // nepoda�ilo se vytvo�it mapu
 */

	return TRUE;
};

// -------------------------------------------------------------------------- //
// this is the new function added to allow to modify the project params using //
// the new project params dialog                                              //
// -------------------------------------------------------------------------- //
BOOL OnEditPoseidonProjectParameters(CPosEdMainDoc* pDoc)
{
	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	if (pEnvir == NULL) return FALSE;

	CDlgProjectParameters dlg;

	// map size
	UNITPOS mapSize = pDoc->m_PoseidonMap.GetSize();
	dlg.m_fSquareSize            = pDoc->m_nRealSizeUnit;
	dlg.m_nWorldSquareWidth      = mapSize.x;
	dlg.m_nWorldSquareHeight     = mapSize.z;
	dlg.m_bAllowMapSizeEdit      = false;
	dlg.m_bAllowTextureLayerEdit = true;
	dlg.m_pDoc                   = pDoc;

	// satellite
	dlg.m_satelliteGridG = pDoc->m_satelliteGridG;
	if (dlg.DoModal() != IDOK) return FALSE;

	bool updateBuld = false;
 
	if (pDoc->m_satelliteGridG != dlg.m_satelliteGridG) updateBuld = pDoc->ChangeSatGrid(dlg.m_satelliteGridG);

	bool updateView = updateBuld;

	if (updateView) pDoc->UpdateAllViews(NULL);

	if (updateBuld) pDoc->OnUpdateBuldozer();

	return TRUE;
}

void CNewProjectDlg::OnBnClickedRecalc()
{
  if (m_bIgnoreEnChange)
    return;
  // Recalculate result values according to wanted ones.
  if (!UpdateData(TRUE))
    return;

  if (!m_cWorldWantedWidth.IsValid() || !m_cSquareSize.IsValid())
    return;

  m_fWorldWantedWidth = m_cWorldWantedWidth.GetValue();
  m_fSquareSize = m_cSquareSize.GetValue();

  m_fWorldWantedHeight = m_fWorldWantedWidth;
  m_nWorldSquareWidth = (int) (m_fWorldWantedWidth / m_fSquareSize);
  if (m_nWorldSquareWidth * m_fSquareSize < m_fWorldWantedWidth)
    m_nWorldSquareWidth++;
  //m_nWorldSquareHeight = m_fWorldWantedWidth / m_fSquareSize + 1;

  int log = 0;
  int orig = m_nWorldSquareWidth;
  for(; m_nWorldSquareWidth > 0; log++)
    m_nWorldSquareWidth = m_nWorldSquareWidth >> 1;

  if (orig == 1 << (log - 1))
    m_nWorldSquareWidth = 1 << (log - 1);
  else
    m_nWorldSquareWidth = 1 << log;


  if (m_nWorldSquareWidth < MIN_WORLD_SIZE)
    m_nWorldSquareWidth = MIN_WORLD_SIZE;

  if (m_nWorldSquareWidth > MAX_WORLD_SIZE)
    m_nWorldSquareWidth = MAX_WORLD_SIZE;

  m_nWorldSquareHeight = m_nWorldSquareWidth;
  m_fWorldWidth = m_nWorldSquareWidth * m_fSquareSize;
  m_fWorldHeight = m_nWorldSquareHeight * m_fSquareSize;

  UpdateData(FALSE);
}

void CNewProjectDlg::OnOK()
{
  m_cSquareSize.Verbose(TRUE);
  m_cWorldWantedWidth.Verbose(TRUE);

  if (!m_cWorldWantedWidth.IsValid() || !m_cSquareSize.IsValid())
  {
    m_cSquareSize.Verbose(FALSE);
    m_cWorldWantedWidth.Verbose(FALSE);
    return;
  }
  m_cSquareSize.Verbose(FALSE);
  m_cWorldWantedWidth.Verbose(FALSE);

  OnBnClickedRecalc();
  
  CDialog::OnOK();
}

BOOL CNewProjectDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  m_bIgnoreEnChange = true;

  m_cSquareSize.SetRange(MAX_REAL_SIZE,0);
  m_cSquareSize.SetValue(m_fSquareSize);
  
  m_cWorldWantedWidth.SetRange(MAX_WORLD_REAL_SIZE,0);
  m_cWorldWantedWidth.SetValue(m_fWorldWantedWidth);
  
  m_bIgnoreEnChange = false;  

  return TRUE;  
}

void CNewProjectDlg::OnEnChangeEditWrldWidth()
{
 if (m_cWorldWantedWidth.OnChange())
   OnBnClickedRecalc();
}

void CNewProjectDlg::OnEnChangeEditSqSize()
{
  if (m_cSquareSize.OnChange())
    OnBnClickedRecalc();
}

/////////////////////////////////////////////////////////////////////////////
// CNewProjectDlg dialog

class CXYZProjectDlg : public CDialog
{
private:
  bool m_bIgnoreEnChange;
  // Construction
public:
  CXYZProjectDlg(CWnd* pParent = NULL);   // standard constructor

  // Dialog Data
  //{{AFX_DATA(CNewProjectDlg)
  enum { IDD = IDD_XYZ_PRJ_DLG };
  float		m_fSourceHeight;
  float		m_fSourceWidth;
  float		m_fWorldWantedHeight;
  float		m_fWorldWantedWidth;
  float   m_fSquareSize;

  int m_nWorldSquareWidth;
  int m_nWorldSquareHeight;
  float m_fWorldHeight;
  float m_fWorldWidth;

  int m_iAlignMap;

  CNumEdit m_cWorldWantedWidth;
  CNumEdit m_cSquareSize;
  //}}AFX_DATA

  // Overrides
  // ClassWizard generated virtual function overrides
  //{{AFX_VIRTUAL(CNewProjectDlg)
protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  //}}AFX_VIRTUAL

  // Implementation
protected:

  // Generated message map functions
  //{{AFX_MSG(CNewProjectDlg)
  // NOTE: the ClassWizard will add member functions here
  //}}AFX_MSG
  DECLARE_MESSAGE_MAP()
public:
  //  afx_msg void OnBnClickedRecalc();
protected:
  virtual void OnOK();
public:
  virtual BOOL OnInitDialog();
  afx_msg void OnEnChangeEditWrldWidth();
  afx_msg void OnEnChangeEditSqSize();
  afx_msg void OnBnClickedRecalc();
};

CXYZProjectDlg::CXYZProjectDlg(CWnd* pParent /*=NULL*/)
: CDialog(CXYZProjectDlg::IDD, pParent)
{ 
  m_bIgnoreEnChange = false;
  m_iAlignMap = 0;
}

void CXYZProjectDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CNewProjectDlg)
  //DDX_Text(pDX, IDC_EDIT_WRLD_WIDTH, m_fWorldWantedWidth);
  //DDV_MinMaxFloat(pDX, m_fWorldWantedWidth, MIN_WORLD_REAL_SIZE, MAX_WORLD_REAL_SIZE);

  DDX_Control(pDX, IDC_EDIT_WRLD_WIDTH, m_cWorldWantedWidth);

  DDX_Text(pDX, IDC_EDIT_WRLD_HEIGHT, m_fWorldWantedHeight);
  //DDV_MinMaxFloat(pDX, m_fWorldWantedHeight, MIN_WORLD_REAL_SIZE, MAX_WORLD_REAL_SIZE);

  //DDX_Text(pDX, IDC_EDIT_SQ_SIZE, m_fSquareSize);
  //DDV_MinMaxFloat(pDX, m_fSquareSize, MIN_REAL_SIZE, MAX_REAL_SIZE);

  DDX_Control(pDX, IDC_EDIT_SQ_SIZE, m_cSquareSize);

  DDX_Text(pDX, IDC_EDIT_WRLD_WIDTH2, m_fWorldWidth);
  //DDV_MinMaxFloat(pDX, m_fWorldWidth, MIN_WORLD_REAL_SIZE, MAX_WORLD_REAL_SIZE);
  DDX_Text(pDX, IDC_EDIT_WRLD_HEIGHT2, m_fWorldHeight);
  //DDV_MinMaxFloat(pDX, m_fWorldHeight, MIN_WORLD_REAL_SIZE, MAX_WORLD_REAL_SIZE);

  DDX_Text(pDX, IDC_EDIT_WRLD_WIDTH3, m_fSourceWidth);
  DDX_Text(pDX, IDC_EDIT_WRLD_HEIGHT3, m_fSourceHeight);

  DDX_Text(pDX, IDC_EDIT_WRLD_SQ_WIDTH, m_nWorldSquareWidth);
  //DDV_MinMaxInt(pDX, m_nWorldSquareWidth, MIN_WORLD_SIZE, MAX_WORLD_SIZE);
  DDX_Text(pDX, IDC_EDIT_WRLD_SQ_HEIGHT, m_nWorldSquareHeight);
  //DDV_MinMaxInt(pDX, m_nWorldSquareHeight, MIN_WORLD_SIZE, MAX_WORLD_SIZE);

  DDX_Radio(pDX, IDC_RADIO1, m_iAlignMap);

  //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CXYZProjectDlg, CDialog)
  //{{AFX_MSG_MAP(CNewProjectDlg)
  // NOTE: the ClassWizard will add message map macros here
  //}}AFX_MSG_MAP

  //  ON_EN_CHANGE(IDC_EDIT_SQ_SIZE, OnBnClickedRecalc)  
  ON_EN_CHANGE(IDC_EDIT_WRLD_WIDTH, OnEnChangeEditWrldWidth)
  ON_EN_CHANGE(IDC_EDIT_SQ_SIZE, OnEnChangeEditSqSize)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Parametry nov�ho projektu	

BOOL OnXYZPoseidonProject(/*CPosEdMainDoc* pDoc*/ SMapSize & mapSize)
{
  // p��stup k options
  CPosEdEnvironment*	pEnvir = GetPosEdEnvironment();
  if (pEnvir == NULL) return FALSE;

  CXYZProjectDlg	dlg;
  dlg.m_fSquareSize = mapSize.step;

  dlg.m_fWorldWantedHeight = 
  dlg.m_fWorldWantedWidth = max(mapSize.size[0],mapSize.size[1]) * mapSize.step;  

  dlg.m_fSourceHeight = mapSize.size[1] * mapSize.step;
  dlg.m_fSourceWidth = mapSize.size[0] * mapSize.step; 

  if (dlg.DoModal()!=IDOK)
    return FALSE;
  
  switch (dlg.m_iAlignMap)
  {
  case 1:    
    mapSize.offset[1] -= (dlg.m_nWorldSquareHeight - mapSize.size[1]) * mapSize.step;
  case 0:
  default:
    mapSize.size[0] = dlg.m_nWorldSquareWidth;
    mapSize.size[1] = dlg.m_nWorldSquareHeight;
  }
  
  //if (!pDoc->m_PoseidonMap.OnNewMap(dlg.m_nWorldSquareWidth,dlg.m_nWorldSquareHeight))
  //  return FALSE; // nepoda�ilo se vytvo�it mapu

  return TRUE;
};

void CXYZProjectDlg::OnBnClickedRecalc()
{
  if (m_bIgnoreEnChange)
    return;
  // Recalculate result values according to wanted ones.
  if (!UpdateData(TRUE))
    return;

  if (!m_cWorldWantedWidth.IsValid() || !m_cSquareSize.IsValid())
    return;

  m_fWorldWantedWidth = m_cWorldWantedWidth.GetValue();
  m_fSquareSize = m_cSquareSize.GetValue();

  m_fWorldWantedHeight = m_fWorldWantedWidth;
  m_nWorldSquareWidth = (int) (m_fWorldWantedWidth / m_fSquareSize);
  if (m_nWorldSquareWidth * m_fSquareSize < m_fWorldWantedWidth)
    m_nWorldSquareWidth++;
  //m_nWorldSquareHeight = m_fWorldWantedWidth / m_fSquareSize + 1;

  int log = 0;
  int orig = m_nWorldSquareWidth;
  for(; m_nWorldSquareWidth > 0; log++)
    m_nWorldSquareWidth = m_nWorldSquareWidth >> 1;

  if (orig == 1 << (log - 1))
    m_nWorldSquareWidth = 1 << (log - 1);
  else
    m_nWorldSquareWidth = 1 << log;


  if (m_nWorldSquareWidth < MIN_WORLD_SIZE)
    m_nWorldSquareWidth = MIN_WORLD_SIZE;

  if (m_nWorldSquareWidth > MAX_WORLD_SIZE)
    m_nWorldSquareWidth = MAX_WORLD_SIZE;

  m_nWorldSquareHeight = m_nWorldSquareWidth;
  m_fWorldWidth = m_nWorldSquareWidth * m_fSquareSize;
  m_fWorldHeight = m_nWorldSquareHeight * m_fSquareSize;

  UpdateData(FALSE);
}

void CXYZProjectDlg::OnOK()
{
  m_cSquareSize.Verbose(TRUE);
  m_cWorldWantedWidth.Verbose(TRUE);

  if (!m_cWorldWantedWidth.IsValid() || !m_cSquareSize.IsValid())
  {
    m_cSquareSize.Verbose(FALSE);
    m_cWorldWantedWidth.Verbose(FALSE);
    return;
  }
  m_cSquareSize.Verbose(FALSE);
  m_cWorldWantedWidth.Verbose(FALSE);

  OnBnClickedRecalc();

  CDialog::OnOK();
}

BOOL CXYZProjectDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  m_bIgnoreEnChange = true;

  m_cSquareSize.SetRange(MAX_REAL_SIZE,MIN_REAL_SIZE);
  m_cSquareSize.SetValue(m_fSquareSize);

  m_cWorldWantedWidth.SetRange(MAX_WORLD_REAL_SIZE,MIN_WORLD_REAL_SIZE);
  m_cWorldWantedWidth.SetValue(m_fWorldWantedWidth);

  m_bIgnoreEnChange = false;  

  OnBnClickedRecalc();

  return TRUE;  
}

void CXYZProjectDlg::OnEnChangeEditWrldWidth()
{
  if (m_cWorldWantedWidth.OnChange())
    OnBnClickedRecalc();
}

void CXYZProjectDlg::OnEnChangeEditSqSize()
{
  if (m_cSquareSize.OnChange())
    OnBnClickedRecalc();
}


