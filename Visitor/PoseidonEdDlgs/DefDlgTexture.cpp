/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef Fail
#undef Fail
#endif

#define MAX_VARIANT_SUMA_PROP	95

/////////////////////////////////////////////////////////////////////////////
// �ablona textury ze souboru .pac

BOOL GetTextureFile(CString& sFile, BOOL bOpen,CPosEdBaseDoc* pDoc)
{
	CString sFilter,sFilter2,sExt,sDir,sTitle;
	VERIFY(sTitle.LoadString(IDS_BULDOZER_TXTR_SEL));
	pDoc->GetProjectDirectory(sDir,CPosEdBaseDoc::dirText);
	MakeShortDirStr(sDir,sDir);
	// p��prava CFileDialog
	CFileDialog dlg(bOpen);
	if (bOpen)
		dlg.m_ofn.Flags |= OFN_FILEMUSTEXIST;
	else
		dlg.m_ofn.Flags |= OFN_PATHMUSTEXIST;

	VERIFY(sFilter.LoadString(IDS_BULDOZER_GIF_FILE));
	VERIFY(sFilter2.LoadString(IDS_BULDOZER_TEXTURE_FILE));
	sFilter += (TCHAR)(_T('\0'));
  sFilter += _T("*.tga;*.gif");
	sFilter += (TCHAR)(_T('\0'));
	sFilter += sFilter2;
	sFilter += (TCHAR)(_T('\0'));
	sFilter += _T("*.pac;*.paa");
	sFilter += (TCHAR)(_T('\0'));
	sFilter += (TCHAR)(_T('\0'));
	dlg.m_ofn.nMaxCustFilter = 1;
	dlg.m_ofn.lpstrFilter = sFilter;
	dlg.m_ofn.lpstrDefExt = _T("tga");
	dlg.m_ofn.lpstrTitle  = sTitle;
	dlg.m_ofn.lpstrFile = sFile.GetBuffer(_MAX_PATH);
	dlg.m_ofn.lpstrInitialDir = sDir;
	BOOL bRes = (dlg.DoModal()==IDOK)?TRUE:FALSE;
	sFile.ReleaseBuffer();
	return bRes;
};


// functions allow user to choose texture file and then converts it to good format and move to right dir
bool GetCorrectTextureFile(CString& sFile,CPosEdBaseDoc* pDoc)
{
  if (!GetTextureFile(sFile, TRUE, pDoc))
    return false;	// nebyl vybr�n soubor

  sFile.MakeLower();
  
  CString sDir;
  pDoc->GetProjectDirectory(sDir,CPosEdBaseDoc::dirText);
  if (_tcsncmp(sDir, sFile, sDir.GetLength()) != 0)
  {
    CString sFileName;
    SeparateNameFromFileName(sFileName, sFile);
    CString sNewFile = sDir + sFileName;

    CString sQuestion;
    sQuestion.Format(IDS_COPY_TEXTURE, (LPCTSTR) sDir, (LPCTSTR) sNewFile);

    if (AfxMessageBox(sQuestion,MB_YESNO|MB_ICONQUESTION)!=IDYES)
      return NULL;

    // check if file exist
    if (MntExistFile(sNewFile))
    {
      sQuestion.Format(IDS_OVERWRITE, (LPCTSTR) sNewFile);

      if (AfxMessageBox(sQuestion,MB_YESNO|MB_ICONQUESTION)!=IDYES)
        return NULL;
    }

    CopyFile(sFile, sNewFile, FALSE);   
    sFile = sNewFile;
  }

  return true;
}

CPosTextureTemplate* CreateTextureFromPacFile(CPosEdBaseDoc* pDoc)
{
  CString sFile;
	if (!GetCorrectTextureFile(sFile, pDoc))
    return NULL;

	// vytvo��m texturu
	CPosTextureTemplate* pTxtr = NULL;
	TRY
	{
		pTxtr = new CPosTextureTemplate(pDoc);
		pTxtr->CreateFromPacFile(sFile);
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pTxtr)
			delete pTxtr;
		return NULL;
	}
	END_CATCH_ALL
	return pTxtr;
};


bool GetRvMatFile(CString& sFile,CPosEdBaseDoc* pDoc)
{
  CString sFilter,sFilter2,sExt,sDir,sTitle;
  VERIFY(sTitle.LoadString(IDS_BULDOZER_MAT_SEL));
  pDoc->GetProjectDirectory(sDir,CPosEdBaseDoc::dirText);
  MakeShortDirStr(sDir,sDir);
  // p��prava CFileDialog
  CFileDialog dlg(TRUE);  
  dlg.m_ofn.Flags |= OFN_FILEMUSTEXIST;
  
  VERIFY(sFilter.LoadString(IDS_BULDOZER_MAT_FILE));
  VERIFY(sFilter2.LoadString(IDS_BULDOZER_ALL_FILE));
  sFilter += (TCHAR)(_T('\0'));
  sFilter += _T("*.rvmat");
  sFilter += (TCHAR)(_T('\0'));
  sFilter += sFilter2;
  sFilter += (TCHAR)(_T('\0'));
  sFilter += _T("*.*");
  sFilter += (TCHAR)(_T('\0'));
  sFilter += (TCHAR)(_T('\0'));
  dlg.m_ofn.nMaxCustFilter = 1;
  dlg.m_ofn.lpstrFilter = sFilter;
  dlg.m_ofn.lpstrDefExt = _T("rvmat");
  dlg.m_ofn.lpstrTitle  = sTitle;
  dlg.m_ofn.lpstrFile = sFile.GetBuffer(_MAX_PATH);
  dlg.m_ofn.lpstrInitialDir = sDir;
  BOOL bRes = (dlg.DoModal()==IDOK)?TRUE:FALSE;
  sFile.ReleaseBuffer();
  return (bRes? true : false);
}

// functions allow user to choose rvmat file and then move it into right dir
bool GetCorrectRvMatFile(CString& sFile, CPosEdBaseDoc* pDoc)
{
  if (!GetRvMatFile(sFile, pDoc))
    return false;

  sFile.MakeLower();

  CString sDir;
  pDoc->GetProjectDirectory(sDir,CPosEdBaseDoc::dirText);
  if (_tcsncmp(sDir, sFile, sDir.GetLength()) != 0)
  {
    CString sFileName;
    SeparateNameFromFileName(sFileName, sFile);
    CString sNewFile = sDir + sFileName;

    CString sQuestion;
    sQuestion.Format(IDS_COPY_TEXTURE, (LPCTSTR) sDir, (LPCTSTR) sNewFile);

    if (AfxMessageBox(sQuestion,MB_YESNO|MB_ICONQUESTION)!=IDYES)
      return false;

    // check if file exist
    if (MntExistFile(sNewFile))
    {
      sQuestion.Format(IDS_OVERWRITE, (LPCTSTR) sNewFile);

      if (AfxMessageBox(sQuestion,MB_YESNO|MB_ICONQUESTION)!=IDYES)
        return false;
    }

    CopyFile(sFile, sNewFile, FALSE);   
    sFile = sNewFile;
  }
  return true;
}
/////////////////////////////////////////////////////////////////////////////
// CDefTxtrZoneDlg dialog
class IsNameUnique
{
public:
  virtual bool operator()(const CString& name,const CPosTextureTemplate * ignore = NULL) = 0; 
};

class CDefTxtrZoneDlg : public CDialog
{
// Construction
public:
	CDefTxtrZoneDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	CPosTextureZone		 m_Zone; // editing zone
   
  IsNameUnique * m_IsNameUnique;

	// update dat pouze pro variantn� texturu
	BOOL				m_bUpdateVariOnly;
	Ref<CPosTextureTemplate> m_pLastSelVarTxtr;

	//{{AFX_DATA(CDefTxtrZoneDlg)
	enum { IDD = IDD_DEF_TXTR_ZONE_DLG };
	CEdit	m_cVariName;
	CVariantListBox	m_VariList;
	CFMntColorButton	m_cVariColor;
	CFMntColorButton	m_cPrimColor;
	float	m_nProbability;
	//}}AFX_DATA

  void UpdateControls();
  void UpdateVariList(CPosTextureTemplate* pSel = NULL);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDefTxtrZoneDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDefTxtrZoneDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnAdd();
	afx_msg void OnDone();
	afx_msg void OnPrimar();
  afx_msg void OnPrimarMaterial();
	afx_msg void OnPrimColor();
	afx_msg void OnVariColor();
	afx_msg void OnSelchangeVariList();
	afx_msg void OnChangeEditVariName();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnBnClickedCheckVariMaterial();
  afx_msg void OnBnClickedVariSearchMat();
  afx_msg void OnBnClickedVariSearchTexture();
  afx_msg void OnEnKillfocusEditPrimName();
  afx_msg void OnEnKillfocusEditVariName();
};

CDefTxtrZoneDlg::CDefTxtrZoneDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDefTxtrZoneDlg::IDD, pParent)
{
	m_bUpdateVariOnly	= FALSE;
	m_pLastSelVarTxtr	= NULL;
	//{{AFX_DATA_INIT(CDefTxtrZoneDlg)
	m_nProbability = 0.0f;
	//}}AFX_DATA_INIT
	m_VariList.m_nVariant = CVariantListBox::itmVariantTxtr;
  m_IsNameUnique = NULL;
}

BOOL CDefTxtrZoneDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// typ krajiny
	ASSERT(m_Zone.m_pOwner != NULL);
	::SetWindowText(DLGCTRL(IDC_SHOW_LAND),m_Zone.m_pOwner->m_sLandName);
	// nen� definov�na prim texutra
	if (m_Zone.m_pPrimarTxtr==NULL)
	{
		::EnableWindow(DLGCTRL(IDC_PRIM_COLOR),FALSE);
		::EnableWindow(DLGCTRL(IDC_EDIT_PRIM_NAME),FALSE);
    ::EnableWindow(DLGCTRL(IDC_EDIT_PRIM_MATERIAL),FALSE);
    ::EnableWindow(DLGCTRL(IDC_PRIM_SEARCH_MATERIAL),FALSE);
	}

	// vlo��m seznam textur
	UpdateVariList();
	// update kontrol�	
	UpdateControls();

	return TRUE;
}

void CDefTxtrZoneDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDefTxtrZoneDlg)
	DDX_Control(pDX, IDC_EDIT_VARI_NAME, m_cVariName);
	DDX_Control(pDX, IDC_LIST, m_VariList);
	DDX_Control(pDX, IDC_VARI_COLOR, m_cVariColor);
	DDX_Control(pDX, IDC_PRIM_COLOR, m_cPrimColor);
	//}}AFX_DATA_MAP

  DDX_Check(pDX, IDC_CHECK_DISABLE_SECONDARY, m_Zone.m_bNoSecondary);

	if (!m_bUpdateVariOnly)
	{
		// jm�no p�sma
		MDDX_Text(pDX, IDC_EDIT_NAME, m_Zone.m_sZoneName);
		MDDV_MaxChars(pDX, m_Zone.m_sZoneName, MAX_ZONE_NAME_LEN, IDC_EDIT_NAME);

		if (SAVEDATA)
			MDDV_MinChars(pDX, m_Zone.m_sZoneName, 1);
		// v��ka p�sma
		MDDX_Text(pDX, IDC_EDIT_HEIGHT_MIN, m_Zone.m_nHeightMin);
		MDDV_MinMaxFloat(pDX, m_Zone.m_nHeightMin, MIN_LAND_HEIGHT, MAX_LAND_HEIGHT, IDC_EDIT_HEIGHT_MIN, 10.f);
		MDDX_Text(pDX, IDC_EDIT_HEIGHT_MAX, m_Zone.m_nHeightMax);
		MDDV_MinMaxFloat(pDX, m_Zone.m_nHeightMax, MIN_LAND_HEIGHT, MAX_LAND_HEIGHT, IDC_EDIT_HEIGHT_MAX, 10.f);

		MDDX_Text(pDX, IDC_EDIT_HGGRAD_MIN, m_Zone.m_nHgGradMin);
		MDDV_MinMaxFloat(pDX, m_Zone.m_nHgGradMin, MIN_LAND_HGGRAD, MAX_LAND_HGGRAD, IDC_EDIT_HGGRAD_MIN, 5.f);
		MDDX_Text(pDX, IDC_EDIT_HGGRAD_MAX, m_Zone.m_nHgGradMax);
		MDDV_MinMaxFloat(pDX, m_Zone.m_nHgGradMax, MIN_LAND_HGGRAD, MAX_LAND_HGGRAD, IDC_EDIT_HGGRAD_MAX, 5.f);
		// mus� b�t definov�na prim�rn� texutra
		if (m_Zone.m_pPrimarTxtr == NULL)
		{
			if (SAVEDATA)
			{
				AfxMessageBox(IDS_ERR_NODEF_PRIM_TXTR,MB_OK|MB_ICONEXCLAMATION);
				pDX->Fail();
			} 
      else
      {      
				::SetWindowText(DLGCTRL(IDC_EDIT_PRIM_FILE),"");					
        ::SetWindowText(DLGCTRL(IDC_EDIT_PRIM_MATERIAL),"");	
      }
		} 
    else
		{	
      // transfer dat prim�rn� textury
			MDDX_Text(pDX, IDC_EDIT_PRIM_NAME, m_Zone.m_pPrimarTxtr->m_sTextureName);
			MDDV_MaxChars(pDX, m_Zone.m_pPrimarTxtr->m_sTextureName, MAX_TXTR_NAME_LEN, IDC_EDIT_PRIM_NAME);
			
      if (SAVEDATA)
			{
				MDDV_MinChars(pDX, m_Zone.m_pPrimarTxtr->m_sTextureName, 1);
        if (m_IsNameUnique && !(*m_IsNameUnique)(m_Zone.m_pPrimarTxtr->m_sTextureName,m_Zone.m_pPrimarTxtr))
        {
          AfxMessageBox(IDS_TEXTURE_NAME_UNIQUE);
          pDX->Fail();
        }
				// barva
				m_Zone.m_pPrimarTxtr->m_cTxtrColor = m_cPrimColor.m_cColor;
			}
			else
			{
				::SetWindowText(DLGCTRL(IDC_EDIT_PRIM_FILE),m_Zone.m_pPrimarTxtr->m_sTextureFile);	
        CString mat; m_Zone.m_pPrimarTxtr->GetRvMatFileName(mat);
        ::SetWindowText(DLGCTRL(IDC_EDIT_PRIM_MATERIAL),mat);		
				// barva
				m_cPrimColor.m_cColor = m_Zone.m_pPrimarTxtr->m_cTxtrColor;
			}   
		};
	};
	// update dat varinatn� textury
	if (m_pLastSelVarTxtr != NULL)
	{
		// jm�no
		MDDX_Text(pDX, IDC_EDIT_VARI_NAME, m_pLastSelVarTxtr->m_sTextureName);
		MDDV_MaxChars(pDX, m_pLastSelVarTxtr->m_sTextureName, MAX_TXTR_NAME_LEN, IDC_EDIT_VARI_NAME);
		if (SAVEDATA)
    {    
			MDDV_MinChars(pDX, m_pLastSelVarTxtr->m_sTextureName,1);

      if (m_IsNameUnique && !(*m_IsNameUnique)(m_pLastSelVarTxtr->m_sTextureName,m_pLastSelVarTxtr))
      {
        AfxMessageBox(IDS_TEXTURE_NAME_UNIQUE);
        pDX->Fail();
      }
    }


		// pravd�podobnost
		MDDX_Text(pDX, IDC_EDIT_PROPAB, m_pLastSelVarTxtr->m_nVarPorpab);
		MDDV_MinMaxFloat(pDX, m_pLastSelVarTxtr->m_nVarPorpab, 0.f, 100.f, IDC_EDIT_PROPAB, 1.f);

		// barva
		if (!SAVEDATA)
		{
			m_cVariColor.m_cColor = m_pLastSelVarTxtr->m_cTxtrColor;
			m_cVariColor.Invalidate();
			// soubor
			::SetWindowText(DLGCTRL(IDC_EDIT_VARI_FILE),m_pLastSelVarTxtr->m_sTextureFile);
      CString mat; m_pLastSelVarTxtr->GetRvMatFileName(mat);
      ::SetWindowText(DLGCTRL(IDC_EDIT_VARI_MATERIAL),mat);
    } else
    {
			m_pLastSelVarTxtr->m_cTxtrColor = m_cVariColor.m_cColor;
		}

    int temp = m_pLastSelVarTxtr->m_bUseOwnMaterial ? 1 : 0;
    DDX_Check(pDX, IDC_CHECK_VARI_MATERIAL, temp);
    m_pLastSelVarTxtr->m_bUseOwnMaterial = temp == 0 ? false : true;

    if (m_pLastSelVarTxtr->m_bUseOwnMaterial)
    {
      ::EnableWindow(DLGCTRL(IDC_EDIT_VARI_MATERIAL), TRUE);
      ::EnableWindow(DLGCTRL(IDC_VARI_SEARCH_MAT), TRUE);
    }
    else
    {
      ::EnableWindow(DLGCTRL(IDC_EDIT_VARI_MATERIAL), FALSE);
      ::EnableWindow(DLGCTRL(IDC_VARI_SEARCH_MAT), FALSE);

      CString mat;
      m_pLastSelVarTxtr->GetRvMatFileName(mat);
      ::SetWindowText(DLGCTRL(IDC_EDIT_VARI_MATERIAL), mat);
    }

	} else
	{
		// jm�no 
		CString sEmpty;
		MDDX_Text(pDX, IDC_EDIT_VARI_NAME, sEmpty);		
		// pravd�podobnost
		float nProp = 0.f;
		MDDX_Text(pDX, IDC_EDIT_PROPAB, nProp);
		// soubor
		::SetWindowText(DLGCTRL(IDC_EDIT_VARI_FILE),"");
    ::SetWindowText(DLGCTRL(IDC_EDIT_VARI_MATERIAL),"");
		// barva
		m_cVariColor.m_cColor = RGB(0,0,0);
	};
	// suma pravd�podobnost� mus� byt < 95 %
	if ((!m_bUpdateVariOnly)&&(SAVEDATA))
	{
		float nProp = 0.f;
		for(int i=0;i<m_Zone.m_VarTxtTmpl.Size();i++)
		{
			PTRTEXTURETMPLT pTxtr = (PTRTEXTURETMPLT)(m_Zone.m_VarTxtTmpl[i]);
			if (pTxtr != NULL)
				nProp += pTxtr->m_nVarPorpab;
		}
		if (nProp > (float)MAX_VARIANT_SUMA_PROP)
		{
			CString sText,sNum;
			NumToLocalString(sNum,(LONG)MAX_VARIANT_SUMA_PROP);
			AfxFormatString1(sText,IDS_BAD_SUMA_PROP,sNum);
			AfxMessageBox(sText,MB_OK|MB_ICONEXCLAMATION);
			sText.Empty();
			sNum.Empty();
			pDX->Fail();
		}
	};
}

void CDefTxtrZoneDlg::UpdateControls()
{
	::EnableWindow( DLGCTRL(IDC_DONE), m_pLastSelVarTxtr!=NULL );
	::EnableWindow( DLGCTRL(IDC_VARI_COLOR), m_pLastSelVarTxtr!=NULL );
	::EnableWindow( DLGCTRL(IDC_EDIT_VARI_NAME), m_pLastSelVarTxtr!=NULL );
	::EnableWindow( DLGCTRL(IDC_EDIT_PROPAB), m_pLastSelVarTxtr!=NULL );
  ::EnableWindow( DLGCTRL(IDC_CHECK_VARI_MATERIAL), m_pLastSelVarTxtr!=NULL );
  
	// update dat
	UpdateData(FALSE);
};
void CDefTxtrZoneDlg::UpdateVariList(CPosTextureTemplate* pSel)
{
	m_VariList.ResetContent();
	m_pLastSelVarTxtr = NULL;

	for(int i=0;i<m_Zone.m_VarTxtTmpl.Size();i++)
	{
		CPosTextureTemplate* pTxtr = (CPosTextureTemplate*)(m_Zone.m_VarTxtTmpl[i]);
		if (pTxtr)
      m_VariList.AddStringEx( pTxtr->m_sTextureName, (DWORD)pTxtr);
			//LBAddString(m_VariList.m_hWnd, pTxtr->m_sTextureName, (DWORD)pTxtr);
	}
	if (pSel != NULL)
	{
		LBSetCurSelData(m_VariList.m_hWnd,(DWORD)pSel);
	} else
	if (m_VariList.GetCount()>0)
		m_VariList.SetCurSel(0);

	OnSelchangeVariList();
};

void CDefTxtrZoneDlg::OnSelchangeVariList() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_VariList.m_hWnd,dwData))
		dwData = 0;
	// lze p�ej�t na jin� prvek ?
	if ((m_pLastSelVarTxtr != NULL)&&(dwData != ((DWORD)m_pLastSelVarTxtr.GetRef())))
	{
		m_bUpdateVariOnly = TRUE;
		if (!UpdateData(TRUE))
		{	// nelze uchovat data - p�ejdu na p�vodn�
			LBSetCurSelData(m_VariList.m_hWnd,(DWORD)m_pLastSelVarTxtr.GetRef());
			m_bUpdateVariOnly = FALSE;
			return;
		}
		m_bUpdateVariOnly = FALSE;
	}
	// p�echod na jin� prvek
	if (dwData != 0)
	{
		m_pLastSelVarTxtr = (CPosTextureTemplate*)dwData;
	} else
		m_pLastSelVarTxtr = NULL;
	// update kontrol�
	m_bUpdateVariOnly = TRUE;
	UpdateControls();	
	m_bUpdateVariOnly = FALSE;
}

BEGIN_MESSAGE_MAP(CDefTxtrZoneDlg, CDialog)
	//{{AFX_MSG_MAP(CDefTxtrZoneDlg)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_DONE, OnDone)
	ON_BN_CLICKED(IDC_PRIM_SEARCH_FILE, OnPrimar)
  ON_BN_CLICKED(IDC_PRIM_SEARCH_MATERIAL, OnPrimarMaterial)
	ON_BN_CLICKED(IDC_PRIM_COLOR, OnPrimColor)
	ON_BN_CLICKED(IDC_VARI_COLOR, OnVariColor)
	ON_LBN_SELCHANGE(IDC_LIST, OnSelchangeVariList)
	ON_EN_CHANGE(IDC_EDIT_VARI_NAME, OnChangeEditVariName)
	//}}AFX_MSG_MAP
  ON_BN_CLICKED(IDC_CHECK_VARI_MATERIAL, OnBnClickedCheckVariMaterial)
  ON_BN_CLICKED(IDC_VARI_SEARCH_MAT, OnBnClickedVariSearchMat)
  ON_BN_CLICKED(IDC_VARI_SEARCH_FILE, OnBnClickedVariSearchTexture)
  ON_EN_KILLFOCUS(IDC_EDIT_PRIM_NAME, OnEnKillfocusEditPrimName)
  ON_EN_KILLFOCUS(IDC_EDIT_VARI_NAME, OnEnKillfocusEditVariName)
END_MESSAGE_MAP()

void CDefTxtrZoneDlg::OnPrimar() 
{   
  if (m_Zone.m_pPrimarTxtr != NULL)
  { 
    CString sFile;
    if (!GetCorrectTextureFile(sFile, m_Zone.m_pOwner->m_pOwner))
      return;

    m_Zone.m_pPrimarTxtr->SetFileFromFullPath(sFile);   
  }
  else
  {
	  Ref<CPosTextureTemplate> pTxtr = CreateTextureFromPacFile(m_Zone.m_pOwner->m_pOwner);
	  if (pTxtr.IsNull()) return;

    m_Zone.m_pPrimarTxtr = pTxtr;
  }

  m_Zone.m_pPrimarTxtr->m_nTxtrType = CPosTextureTemplate::txtrTypePrimar;

	// vlo��m novou prim�rn� texturu
	ASSERT(m_Zone.m_pPrimarTxtr->m_nTxtrType == CPosTextureTemplate::txtrTypePrimar);
	// povol�m kontroly
	::EnableWindow(DLGCTRL(IDC_PRIM_COLOR),TRUE);
	::EnableWindow(DLGCTRL(IDC_EDIT_PRIM_NAME),TRUE);
  ::EnableWindow(DLGCTRL(IDC_EDIT_PRIM_MATERIAL),TRUE);
  ::EnableWindow(DLGCTRL(IDC_PRIM_SEARCH_MATERIAL),TRUE);

	// zobraz�m data do kontrol�
	::SetWindowText(DLGCTRL(IDC_EDIT_PRIM_NAME),m_Zone.m_pPrimarTxtr->m_sTextureName);
	::SetWindowText(DLGCTRL(IDC_EDIT_PRIM_FILE),m_Zone.m_pPrimarTxtr->m_sTextureFile);
  CString mat; m_Zone.m_pPrimarTxtr->GetRvMatFileName(mat);
  ::SetWindowText(DLGCTRL(IDC_EDIT_PRIM_MATERIAL),mat);

	m_cPrimColor.m_cColor = m_Zone.m_pPrimarTxtr->m_cTxtrColor;
	m_cPrimColor.Invalidate();
}

void CDefTxtrZoneDlg::OnPrimarMaterial() 
{   
  if (m_Zone.m_pPrimarTxtr == NULL)
    return;

  CString sFile;
  if (!GetCorrectRvMatFile(sFile, m_Zone.m_pOwner->m_pOwner))
    return;

  m_Zone.m_pPrimarTxtr->SetRvMatFileNameFromFullPath(sFile);
  CString mat; m_Zone.m_pPrimarTxtr->GetRvMatFileName(mat);
  ::SetWindowText(DLGCTRL(IDC_EDIT_PRIM_MATERIAL),mat);  
}

void CDefTxtrZoneDlg::OnAdd() 
{
	// mohu p�ej�t na jin� prvek seznamu
	if (m_pLastSelVarTxtr != NULL)
	{
		m_bUpdateVariOnly = TRUE;
		if (!UpdateData(TRUE))
		{	// nelze uchovat data - p�ejdu na p�vodn�
			LBSetCurSelData(m_VariList.m_hWnd,(DWORD)m_pLastSelVarTxtr.GetRef());
			m_bUpdateVariOnly = FALSE;
			return;
		}
		m_bUpdateVariOnly = FALSE;
	};
	// vytvo��m novou �ablonu
	Ref<CPosTextureTemplate> pTxtr = CreateTextureFromPacFile(m_Zone.m_pOwner->m_pOwner);
	if (pTxtr.IsNull()) return;

  // Check if its name is unique
  if (m_IsNameUnique && !(*m_IsNameUnique)(pTxtr->Name()))
  {
    //try to find some other unique name
    CString uniqueName;
    int i = 1;
    do 
    {
      uniqueName.Format(_T("%s%d"),pTxtr->Name(),i);
      i++;
    } while(!(*m_IsNameUnique)(uniqueName));

    pTxtr->SetName(uniqueName);
  }

	// vlo��m do seznamu  
  pTxtr->m_nTxtrType = CPosTextureTemplate::txtrTypeVariant;
  pTxtr->m_pLandZone = &m_Zone;
	m_Zone.m_VarTxtTmpl.Add(pTxtr);
  
	// update seznamu
	UpdateVariList(pTxtr);
}

void CDefTxtrZoneDlg::OnDone() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_VariList.m_hWnd,dwData))
		return;	// nen� vybr�n ��dn� prvek
	// kontroln� dotaz
	CPosTextureTemplate* pDelTxtr = (CPosTextureTemplate*)dwData;
	if (pDelTxtr==NULL) return;

  if (pDelTxtr->m_nTimesUsedInBaseGlobal > 0)
  {
    AfxMessageBox(IDS_TEXTURE_DEL_USED);
    return;
  }
	CString sText;
	AfxFormatString1(sText,IDS_VERIFY_DEL_TEXTURE,pDelTxtr->m_sTextureName);
	if (AfxMessageBox(sText,MB_YESNOCANCEL|MB_ICONQUESTION,MB_DEFBUTTON2)!=IDYES) return;
	// sma�u prvek ze seznamu
	for(int i=0;i<m_Zone.m_VarTxtTmpl.Size();i++)
	{
		CPosTextureTemplate* pTxtr = (CPosTextureTemplate*)(m_Zone.m_VarTxtTmpl[i]);
		if (pTxtr == pDelTxtr)
		{
			m_Zone.m_VarTxtTmpl.Delete(i);
			delete pDelTxtr;
			break;
		}
	};
	pDelTxtr = NULL;
	// najdu novou selekci
	int nIndx = m_VariList.GetCurSel();
	m_VariList.DeleteString(nIndx);
	if (nIndx >= m_VariList.GetCount())	nIndx--;
	if (nIndx >= 0)
		pDelTxtr = (CPosTextureTemplate*)(m_VariList.GetItemData(nIndx));
	// update seznamu
	UpdateVariList(pDelTxtr);
}

void CDefTxtrZoneDlg::OnPrimColor() 
{
	m_cPrimColor.DoEditColor();
}

void CDefTxtrZoneDlg::OnVariColor() 
{
	if (m_pLastSelVarTxtr != NULL)
	{
		if (m_cVariColor.DoEditColor())
		{
			m_pLastSelVarTxtr->m_cTxtrColor = m_cVariColor.m_cColor;
			m_VariList.Invalidate();
		}
	}
}

void CDefTxtrZoneDlg::OnChangeEditVariName() 
{
	if (m_pLastSelVarTxtr != NULL)
	{
		m_cVariName.GetWindowText(m_pLastSelVarTxtr->m_sTextureName);
		m_VariList.Invalidate();
	};
}

void CDefTxtrZoneDlg::OnBnClickedCheckVariMaterial()
{
  if (m_pLastSelVarTxtr == NULL)
    return;

  CButton * but = (CButton *) GetDlgItem(IDC_CHECK_VARI_MATERIAL);
  m_pLastSelVarTxtr->m_bUseOwnMaterial = (but->GetCheck() == BST_CHECKED);

  if (m_pLastSelVarTxtr->m_bUseOwnMaterial)
  {
    ::EnableWindow(DLGCTRL(IDC_EDIT_VARI_MATERIAL), TRUE);
    ::EnableWindow(DLGCTRL(IDC_VARI_SEARCH_MAT), TRUE);
  }
  else
  {
    ::EnableWindow(DLGCTRL(IDC_EDIT_VARI_MATERIAL), FALSE);
    ::EnableWindow(DLGCTRL(IDC_VARI_SEARCH_MAT), FALSE);
  }

  CString mat; m_pLastSelVarTxtr->GetRvMatFileName(mat);
  ::SetWindowText(DLGCTRL(IDC_EDIT_VARI_MATERIAL),mat);  
}

void CDefTxtrZoneDlg::OnBnClickedVariSearchMat()
{
  if (m_pLastSelVarTxtr == NULL || !m_pLastSelVarTxtr->m_bUseOwnMaterial)
    return;

  CString sFile;
  if (!GetCorrectRvMatFile(sFile, m_Zone.m_pOwner->m_pOwner))
    return;

  m_pLastSelVarTxtr->SetRvMatFileNameFromFullPath(sFile);
  CString mat; m_pLastSelVarTxtr->GetRvMatFileName(mat);
  ::SetWindowText(DLGCTRL(IDC_EDIT_VARI_MATERIAL),mat);  
}

void CDefTxtrZoneDlg::OnBnClickedVariSearchTexture()
{
  if (m_pLastSelVarTxtr == NULL )
    return;

  CString sFile;
  if (!GetCorrectTextureFile(sFile, m_Zone.m_pOwner->m_pOwner))
    return;

  m_pLastSelVarTxtr->SetFileFromFullPath(sFile);
  CString mat; m_pLastSelVarTxtr->GetTextureFileName(mat);
  ::SetWindowText(DLGCTRL(IDC_EDIT_VARI_FILE),mat);  
}

void CDefTxtrZoneDlg::OnEnKillfocusEditPrimName()
{
  if (GetFocus() == GetDlgItem(IDCANCEL))
    return;
  
  UpdateData();
}

void CDefTxtrZoneDlg::OnEnKillfocusEditVariName()
{
  if (GetFocus() == GetDlgItem(IDCANCEL))
    return;

  UpdateData();
}
///////////////////////
//  
class IsNameUniqueMgr : public IsNameUnique
{
protected:
  const CMgrTextures& _mgr;
  const CPosTextureZone * _replaceThis;
  const CPosTextureZone * _replaceWith;

public:
  IsNameUniqueMgr(const CMgrTextures& mgr, const CPosTextureZone * replaceThis,
    const CPosTextureZone * replaceWith) : _mgr(mgr), _replaceThis(replaceThis), _replaceWith(replaceWith) {};

  bool operator()(const CString& name,const CPosTextureTemplate * ignore = NULL);
};

bool IsNameUniqueMgr::operator()(const CString& name,const CPosTextureTemplate * ignore)
{
  if (_mgr.ExistTextureName(name, _replaceThis, ignore))
    return false;

  if (_replaceWith && _replaceWith->ExistTextureName(name, ignore))
    return false;

  return true;
}

BOOL DoEditTxtrZone(const CMgrTextures& mgr, CPosTextureZone* pZone)
{
	CDefTxtrZoneDlg dlg;
	dlg.m_Zone.CopyFrom(pZone);
	dlg.m_Zone.m_pOwner = pZone->m_pOwner;
  dlg.m_Zone.CreateLocalTextures(); // make copies of textures, CPosTextureZone::CopyFrom does not copies textures

  IsNameUniqueMgr isName(mgr,pZone, &dlg.m_Zone);
  dlg.m_IsNameUnique = &isName;

	if (dlg.DoModal()!=IDOK)
		return FALSE;

	pZone->CopyFrom(&(dlg.m_Zone));
  pZone->CreateLocalTextures(); // make copies of textures

	return TRUE;
};

/////////////////////////////////////////////////////////////////////////////
// CDefTxtrLandsDlg dialog

class CDefTxtrLandsDlg : public CDialog
{
  // Construction
public:
  CDefTxtrLandsDlg(CWnd* pParent = NULL);   // standard constructor

  // Dialog Data
  CMgrTextures		m_Texutres;
  CPosTextureLand*	m_pLastSelLand;

  //{{AFX_DATA(CDefTxtrLandsDlg)
  enum { IDD = IDD_DEF_TXTR_LAND_DLG };
  CEdit			m_cEditName;
  CFMntColorButton	m_cLandColor;
  CVariantListBox	m_ListZone;
  CVariantListBox	m_ListLand;
  //}}AFX_DATA


  // Overrides
  void UpdateControls();
  void UpdateLandList(CPosTextureLand* pSel = NULL);
  void UpdateZoneList(CPosTextureZone* pSel = NULL);

  // ClassWizard generated virtual function overrides
  //{{AFX_VIRTUAL(CDefTxtrLandsDlg)
protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  //}}AFX_VIRTUAL

  // Implementation
protected:

  // Generated message map functions
  //{{AFX_MSG(CDefTxtrLandsDlg)
  virtual BOOL OnInitDialog();
  afx_msg void OnAdd();
  afx_msg void OnDone();
  afx_msg void OnEdit();
  afx_msg void OnEditColor();
  afx_msg void OnNewLand();
  afx_msg void OnDelLand();
  afx_msg void OnSelchangeListLand();
  afx_msg void OnChangeEditName();
  //}}AFX_MSG
  DECLARE_MESSAGE_MAP()
};


CDefTxtrLandsDlg::CDefTxtrLandsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDefTxtrLandsDlg::IDD, pParent)
{
	m_pLastSelLand = NULL;
	//{{AFX_DATA_INIT(CDefTxtrLandsDlg)
	//}}AFX_DATA_INIT
	m_ListLand.m_nVariant = CVariantListBox::itmVariantLand;
	m_ListZone.m_nVariant = CVariantListBox::itmVariantZone;
}

BOOL CDefTxtrLandsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// vlo��m seznam typ� krajiny
	UpdateLandList();

	// update kontrol�	
	UpdateControls();
	return TRUE;
}

void CDefTxtrLandsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDefTxtrLandsDlg)
	DDX_Control(pDX, IDC_EDIT_NAME, m_cEditName);
	DDX_Control(pDX, IDC_EDIT_COLOR, m_cLandColor);
	DDX_Control(pDX, IDC_LIST_ZONE, m_ListZone);
	DDX_Control(pDX, IDC_LIST_LAND, m_ListLand);
	//}}AFX_DATA_MAP
	if (m_pLastSelLand != NULL)
	{
		// jm�no
		MDDX_Text(pDX, IDC_EDIT_NAME, m_pLastSelLand->m_sLandName);
		MDDV_MaxChars(pDX, m_pLastSelLand->m_sLandName, MAX_LAND_NAME_LEN, IDC_EDIT_NAME);
		if (SAVEDATA)
			MDDV_MinChars(pDX, m_pLastSelLand->m_sLandName,1);
		// relativizace v��ky
		MDDX_Text(pDX, IDC_EDIT_HEIGHT, m_pLastSelLand->m_sLandHeight);
		MDDV_MaxChars(pDX, m_pLastSelLand->m_sLandHeight, STD_STRING_LEN, IDC_EDIT_HEIGHT);
		// mo�e
		DDX_Check(pDX, IDC_IS_SEA, m_pLastSelLand->m_bIsSea);
		// barva
		if (!SAVEDATA)
		{
			m_cLandColor.m_cColor = m_pLastSelLand->m_cLandColor;
			m_cLandColor.Invalidate();
		} else
		{
			m_pLastSelLand->m_cLandColor = m_cLandColor.m_cColor;
		}
	} else
	{
		// jm�no 
		CString sEmpty;
		MDDX_Text(pDX, IDC_EDIT_NAME, sEmpty);
		MDDV_MaxChars(pDX, sEmpty, MAX_LAND_NAME_LEN, IDC_EDIT_NAME);
		// relativizace v��ky
		MDDX_Text(pDX, IDC_EDIT_HEIGHT, sEmpty);
		MDDV_MaxChars(pDX, sEmpty, STD_STRING_LEN, IDC_EDIT_HEIGHT);
		// mo�e
		BOOL bSea = FALSE;
		DDX_Check(pDX, IDC_IS_SEA, bSea);
		// barva
		m_cLandColor.m_cColor = RGB(0,0,0);
	};
}

void CDefTxtrLandsDlg::UpdateControls()
{
	// smazat typ mohu jen, je-li vybr�n
	::EnableWindow( DLGCTRL(IDC_DEL_LAND), m_pLastSelLand!=NULL );
	
	::EnableWindow( DLGCTRL(IDC_EDIT_NAME),    m_pLastSelLand!=NULL );
	::EnableWindow( DLGCTRL(IDC_EDIT_COLOR),   m_pLastSelLand!=NULL );
	::EnableWindow( DLGCTRL(IDC_IS_SEA),		m_pLastSelLand!=NULL );
	::EnableWindow( DLGCTRL(IDC_EDIT_HEIGHT),  m_pLastSelLand!=NULL );
	
	::EnableWindow( DLGCTRL(IDC_ADD), m_pLastSelLand!=NULL );

	// update dat
	UpdateData(FALSE);
	// update zon
	UpdateZoneList();
};

void CDefTxtrLandsDlg::UpdateLandList(CPosTextureLand* pSel)
{
	m_ListLand.ResetContent();
	m_pLastSelLand = NULL;

	for(int i=0;i<m_Texutres.m_TextureLands.GetSize();i++)
	{
		CPosTextureLand* pLand = (CPosTextureLand*)(m_Texutres.m_TextureLands[i]);
		if (pLand)
            m_ListLand.AddStringEx( pLand->m_sLandName, (DWORD)pLand);
			//LBAddString(m_ListLand.m_hWnd, pLand->m_sLandName, (DWORD)pLand);
	}
	if (pSel != NULL)
	{
		LBSetCurSelData(m_ListLand.m_hWnd,(DWORD)pSel);
	} else
	if (m_ListLand.GetCount()>0)
		m_ListLand.SetCurSel(0);
	OnSelchangeListLand();
};

void CDefTxtrLandsDlg::UpdateZoneList(CPosTextureZone* pSel)
{
	m_ListZone.ResetContent();
	if (m_pLastSelLand == NULL)
		return;	// nen� vybr�no p�smo
	// vlo��m p�sma (jsou set��d�na)
	for(int i=0; i<m_pLastSelLand->m_LandZones.GetSize(); i++)
	{
		CPosTextureZone* pZone = (CPosTextureZone*)(m_pLastSelLand->m_LandZones[i]);
		if (pZone)
			m_ListZone.AddStringEx( pZone->m_sZoneName, (DWORD)pZone);
	}
	if (pSel != NULL)
	{
		LBSetCurSelData(m_ListZone.m_hWnd,(DWORD)pSel);
	} else
	if (m_ListZone.GetCount()>0)
		m_ListZone.SetCurSel(0);
	// mo�nost upravy p�sem ?
	int nIndx = m_ListZone.GetCurSel();
	::EnableWindow( DLGCTRL(IDC_DONE), nIndx!=LB_ERR );
	::EnableWindow( DLGCTRL(IDC_EDIT), nIndx!=LB_ERR );
};

BEGIN_MESSAGE_MAP(CDefTxtrLandsDlg, CDialog)
	//{{AFX_MSG_MAP(CDefTxtrLandsDlg)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_DONE, OnDone)
	ON_BN_CLICKED(IDC_EDIT, OnEdit)
	ON_BN_CLICKED(IDC_EDIT_COLOR, OnEditColor)
	ON_BN_CLICKED(IDC_NEW_LAND, OnNewLand)
	ON_BN_CLICKED(IDC_DEL_LAND, OnDelLand)
	ON_LBN_SELCHANGE(IDC_LIST_LAND, OnSelchangeListLand)
	ON_EN_CHANGE(IDC_EDIT_NAME, OnChangeEditName)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CDefTxtrLandsDlg::OnSelchangeListLand() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_ListLand.m_hWnd,dwData))
		dwData = 0;
	// lze p�ej�t na jin� prvek ?
	if ((m_pLastSelLand != NULL)&&(dwData != ((DWORD)m_pLastSelLand)))
	{
		if (!UpdateData(TRUE))
		{	// nelze uchovat data - p�ejdu na p�vodn�
			LBSetCurSelData(m_ListLand.m_hWnd,(DWORD)m_pLastSelLand);
			return;
		}
	}
	// p�echod na jin� prvek
	if (dwData != 0)
	{
		m_pLastSelLand = (CPosTextureLand*)dwData;
	} else
		m_pLastSelLand = NULL;
	// update kontrol�
	UpdateControls();	
}

// �pravy
void CDefTxtrLandsDlg::OnNewLand() 
{
	// mohu vlo�it nov�
	if (m_pLastSelLand != NULL)
	{
		if (!UpdateData(TRUE))
		{	// nelze uchovat data - p�ejdu na p�vodn�
			LBSetCurSelData(m_ListLand.m_hWnd,(DWORD)m_pLastSelLand);
			return;
		}
	};
	// vlo��m nov�
	CString sText;
	if (DoPromptTextDlg(sText, 10, IDS_LAND_TYPE,IDS_NEW_LAND_TYPE_NAME))
	{
		CPosTextureLand* pLand = new CPosTextureLand(m_Texutres.m_pOwner);
		pLand->m_sLandName = sText;
		m_Texutres.m_TextureLands.Add(pLand);
		UpdateLandList(pLand);
	}
}

void CDefTxtrLandsDlg::OnDelLand() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_ListLand.m_hWnd,dwData))
		return;	// nen� vybr�n ��dn� prvek
	// kontroln� dotaz
	CPosTextureLand* pDelLand = (CPosTextureLand*)dwData;
	if (pDelLand==NULL) 
    return;
  if (pDelLand->IsInUseBase())
  {
    AfxMessageBox(IDS_LAND_DEL_USE,MB_OK);
    return;
  }

	CString sText;
	AfxFormatString1(sText,IDS_VERIFY_DEL_LAND_TYPE,pDelLand->m_sLandName);
	if (AfxMessageBox(sText,MB_YESNOCANCEL|MB_ICONQUESTION,MB_DEFBUTTON2)!=IDYES) return;
	// sma�u prvek ze seznamu
	for(int i=0;i<m_Texutres.m_TextureLands.GetSize();i++)
	{
		CPosTextureLand* pLand = (CPosTextureLand*)(m_Texutres.m_TextureLands[i]);
		if (pLand == pDelLand)
		{
			m_Texutres.m_TextureLands.RemoveAt(i);
			delete pDelLand;
			break;
		}
	};
	pDelLand = NULL;
	// najdu novou selekci
	int nIndx = m_ListLand.GetCurSel();
	m_ListLand.DeleteString(nIndx);
	if (nIndx >= m_ListLand.GetCount())	nIndx--;
	if (nIndx >= 0)
		pDelLand = (CPosTextureLand*)(m_ListLand.GetItemData(nIndx));
	// update seznamu
	UpdateLandList(pDelLand);
}

void CDefTxtrLandsDlg::OnAdd() 
{
	if (m_pLastSelLand == NULL)
		return;	// nen� kam vlo�it

	CPosTextureZone* pZone = new CPosTextureZone();
	pZone->m_pOwner = m_pLastSelLand;
	if ((pZone)&&(DoEditTxtrZone(m_Texutres,pZone)))
	{	// vlo��m nov� p�smo
		m_pLastSelLand->AddNewZone(pZone);
		UpdateZoneList(pZone);
		return;
	}
	// zru��m nepou�it� p�smo
	if (pZone)
		delete pZone;
}

void CDefTxtrLandsDlg::OnDone() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_ListZone.m_hWnd,dwData))
		return;
  
  CPosTextureZone* pZone = (CPosTextureZone*)dwData;
	if (pZone)
	{
		ASSERT_KINDOF(CPosTextureZone,pZone);

    if (pZone->IsInUseBase())
    {
      AfxMessageBox(IDS_ZONE_DEL_USED);
      return;
    }
		CString sText;
		AfxFormatString1(sText,IDS_VERIFY_ZONE_DEL,pZone->m_sZoneName);
		if (AfxMessageBox(sText,MB_YESNOCANCEL|MB_ICONQUESTION|MB_DEFBUTTON2)!=IDYES)
			return;
		// sma�u p�smo
		m_pLastSelLand->DeleteZone(pZone);
		// najdu novou selekci
		int nIndx = m_ListZone.GetCurSel();
		m_ListZone.DeleteString(nIndx);
		if (nIndx >= m_ListZone.GetCount())	nIndx--;
		if (nIndx >= 0)
			pZone = (CPosTextureZone*)(m_ListZone.GetItemData(nIndx));
		// update seznamu
		UpdateZoneList(pZone);
	}
}

void CDefTxtrLandsDlg::OnEdit() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_ListZone.m_hWnd,dwData))
		return;
	CPosTextureZone* pZone = (CPosTextureZone*)dwData;
	if (pZone)
	{
		ASSERT_KINDOF(CPosTextureZone,pZone);
		// editace p�sma
		if (DoEditTxtrZone(m_Texutres,pZone))
		{	// zat��d�m p�smo
			m_pLastSelLand->DeleteZone(pZone,FALSE);// jen vyjmu !!!
			m_pLastSelLand->AddNewZone(pZone);
			UpdateZoneList(pZone);
		}
	}
}

void CDefTxtrLandsDlg::OnEditColor() 
{
	if (m_cLandColor.DoEditColor() && (m_pLastSelLand != NULL))
	{	// zm�nila se barva
		m_pLastSelLand->m_cLandColor = m_cLandColor.m_cColor;
		m_ListLand.Invalidate();
	}
}

void CDefTxtrLandsDlg::OnChangeEditName() 
{
	if (m_pLastSelLand != NULL)
	{
		m_cEditName.GetWindowText(m_pLastSelLand->m_sLandName);
		m_ListLand.Invalidate();
	};
}

/////////////////////////////////////////////////////////////////////////////
// definice 

BOOL DoDefineTemplateTexture(CPosEdMainDoc* pDoc)
{
	CDefTxtrLandsDlg	dlg;
	dlg.m_Texutres.CopyFrom(&(pDoc->m_MgrTextures)); // Be Ware textures are shared ... 
                                                   //now primary textures even in pDoc->m_MgrTextures points to zones from dlg.m_Texutres
                                                   // Call Doc->m_MgrTextures.UpdateFlagsAndOwners() later     
	dlg.m_Texutres.m_pOwner = pDoc;
  dlg.m_Texutres.CreateLocalTextures();
  pDoc->m_MgrTextures.UpdateFlagsAndOwners();
  
	if (dlg.DoModal()!=IDOK)
		return FALSE;
	// zm�ny v definici textur
	pDoc->m_MgrTextures.CopyFrom(&dlg.m_Texutres);
  pDoc->m_MgrTextures.SyncLocalTexturesIntoDoc();
  pDoc->m_Textures.CleanUnUsed();
	return TRUE;
};














