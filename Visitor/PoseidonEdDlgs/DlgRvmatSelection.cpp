// DlgRvmatSelection.cpp : implementation file
//

#include "stdafx.h"
#include "DlgRvmatSelection.h"


// CDlgRvmatSelection dialog

IMPLEMENT_DYNAMIC(CDlgRvmatSelection, CDialog)

CDlgRvmatSelection::CDlgRvmatSelection(CWnd* pParent /*=NULL*/)
: CDialog(CDlgRvmatSelection::IDD, pParent)
, m_Choice(0)
{

}

CDlgRvmatSelection::~CDlgRvmatSelection()
{
}

void CDlgRvmatSelection::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_RADIO1, m_Choice);
}

BEGIN_MESSAGE_MAP(CDlgRvmatSelection, CDialog)
END_MESSAGE_MAP()


// CDlgRvmatSelection message handlers
