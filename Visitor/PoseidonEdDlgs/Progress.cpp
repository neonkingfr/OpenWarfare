#include "stdafx.h"
#include "resource.h"
#include "Progress.h"

/*class CProgressDialog : public CDialog
{
  // Construction
public:
  CProgressDialog(CWnd* pParent = NULL);   // standard constructor

  enum { IDD = IDD_DEF_TXTR_ZONE_DLG };

  void Create();
  void SetRange(int range) {};
  void SetPos(int pos) {};
  void DoStep(int step = 1) {};

  void Done();
};*/

CProgressDialog::CProgressDialog(CWnd* pParent) :
CDialog(CProgressDialog::IDD, pParent)
{
}

void CProgressDialog::Create()
{
  CDialog::Create(CProgressDialog::IDD, NULL); //GetParent());
  ShowWindow(SW_SHOW);
}

void CProgressDialog::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);  
  DDX_Control(pDX, IDC_PROGRESS1, m_cProgress);
}

void CProgressDialog::SetRange(int range) 
{
  m_cProgress.SetRange32(0, range);
};

void CProgressDialog::SetPos(int pos) 
{
  m_cProgress.SetPos(pos);
  UpdateWindow();
};

void CProgressDialog::DoStep(int step ) 
{
  m_cProgress.SetPos(m_cProgress.GetPos() + step);
  UpdateWindow();
};