#pragma once

#include "resource.h"

// CAboutNew dialog

class CAboutNewDlg : public CDialog
{
	DECLARE_DYNAMIC(CAboutNewDlg)

public:
	CAboutNewDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAboutNewDlg();

// Dialog Data
	enum { IDD = IDD_ABOUT_NEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

private:
	virtual BOOL OnInitDialog(void);

	DECLARE_MESSAGE_MAP()
};
