/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


// Fail is defined as a macro in debuglog.hh
#ifdef Fail
#undef Fail
#endif
/////////////////////////////////////////////////////////////////////////////
// 

#define MAX_OBJECT_NAME_LEN	50



/////////////////////////////////////////////////////////////////////////////
// CAddObjectsDlg dialog

class CAddObjectsDlg : public CDialog
{
// Construction
public:
	CAddObjectsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	CPosEdMainDoc*	m_pDocument;
	int			    m_Type;	// p��rodn� objekty
	CStringArray	m_Results;	// soubory k vlo�en�
	BOOL			m_bSingle;
	BOOL			m_bIn1Categ;// mzus� b�t jen v 1 kategorii

	//{{AFX_DATA(CAddObjectsDlg)
	enum { IDD = IDD_ADD_OBJECTS_DLG };
	CListBox	m_List;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAddObjectsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAddObjectsDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


CAddObjectsDlg::CAddObjectsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAddObjectsDlg::IDD, pParent)
{
	m_pDocument = NULL;
	m_bSingle = FALSE;
	m_bIn1Categ = TRUE;
	//{{AFX_DATA_INIT(CAddObjectsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

static void FindFiles(CString path, CString prefix, CString mask, CListBox &list)
{
	WIN32_FIND_DATA info;

	HANDLE h = FindFirstFile(path + prefix + mask, &info);
	if (h != INVALID_HANDLE_VALUE)
	{
		do
		{
			if ((info.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
			{
				list.AddString(prefix + CString(info.cFileName));
			}
		}
		while (FindNextFile(h, &info));
		FindClose(h);
	}

	h = FindFirstFile(path + prefix + "*.*", &info);
	if (h != INVALID_HANDLE_VALUE)
	{
		do
		{
			if ((info.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)
			{
				if (info.cFileName[0] != '.')
				{
					FindFiles(path, prefix + CString(info.cFileName) + _T("\\"), mask, list);
				}
			}
		}
		while (FindNextFile(h, &info));
		FindClose(h);
	}
}

BOOL CAddObjectsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// aktualizuji seznam soubor�
	CString sPath;
	m_pDocument->GetProjectDirectory(sPath,CPosEdBaseDoc::dirObj);
	CString mask = _T("*");
	mask += DATA3D_FILE_EXT;
	FindFiles(sPath, "", mask, m_List);
	return TRUE;
}

void CAddObjectsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAddObjectsDlg)
	DDX_Control(pDX, IDC_LIST, m_List);
	//}}AFX_DATA_MAP
	if (SAVEDATA)
	{
		m_Results.RemoveAll();

		int nCount = m_List.GetSelCount();
		if (nCount <= 0)
		{
			AfxMessageBox(IDS_NO_OBJECT_SELECTED, MB_OK | MB_ICONEXCLAMATION);
			pDX->Fail();
		}
		if ((m_bSingle) && (nCount != 1))
		{
			AfxMessageBox(IDS_NO_ONE_OBJECT_SELECTED, MB_OK|MB_ICONEXCLAMATION);
			pDX->Fail();
		}
		// na�ten� prvk�
		LPINT pBuff = new int[nCount];
		m_List.GetSelItems(nCount, pBuff);
		for(int i = 0; i < nCount; ++i)
		{
			CString sFile;
			m_List.GetText(pBuff[i], sFile);
			m_Results.Add(sFile);
		}
		delete [] pBuff;
		// kontrola soubor�
		CString		sText,sObj;
		BOOL		bInOtherCat = FALSE;
		if (m_bIn1Categ)
		{			
			if (m_pDocument != NULL)
			{				
				for(int i = 0; i < m_Results.GetSize(); ++i)
				{
					for(int t = 0; t < m_pDocument->ObjectTemplatesSize(); ++t)
					{
						CPosObjectTemplate* pTempl = (CPosObjectTemplate*)m_pDocument->GetIthObjectTemplate(t);
						if (pTempl && m_Results[i].CompareNoCase(pTempl->GetFileName()) == 0)
						{
							if ((m_Type == CPosObjectTemplate::objTpNatural && pTempl->m_nObjType != CPosObjectTemplate::objTpNatural) ||
								(m_Type == CPosObjectTemplate::objTpPeople && pTempl->m_nObjType != CPosObjectTemplate::objTpPeople) ||
								(m_Type == CPosObjectTemplate::objTpNewRoads && pTempl->m_nObjType != CPosObjectTemplate::objTpNewRoads))
							{              
								sObj = m_Results[i];              
								bInOtherCat = TRUE;
								break;
							}
						}
					}
				}
			}
		}
		if (bInOtherCat)
		{
			AfxFormatString1(sText, IDS_OBJECT_INOTHER_CATEG, sObj);
			AfxMessageBox(sText,MB_OK | MB_ICONEXCLAMATION);
			pDX->Fail();
		}
	}
}

BEGIN_MESSAGE_MAP(CAddObjectsDlg, CDialog)
	//{{AFX_MSG_MAP(CAddObjectsDlg)
	ON_LBN_DBLCLK(IDC_LIST, OnOK)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL GetObjectFileSelection(CString& sObjFile, CPosEdMainDoc* pDoc, BOOL bIn1Categ)
{
	CAddObjectsDlg dlg;
	dlg.m_pDocument = pDoc;
	dlg.m_Type      = CPosObjectTemplate::objTpNatural;
	dlg.m_bSingle   = TRUE;
	dlg.m_bIn1Categ = bIn1Categ;
	if (dlg.DoModal() != IDOK) return FALSE;
	if (dlg.m_Results.GetSize() == 1)
	{
		sObjFile = dlg.m_Results[0];
		return TRUE;
	}
	return FALSE;
};

/////////////////////////////////////////////////////////////////////////////
// CDefObjectsDlg dialog

class CDefObjectsDlg : public CDialog
{
// Construction
public:
	CDefObjectsDlg(CWnd* pParent = NULL);   // standard constructor
	~CDefObjectsDlg();

// Dialog Data
	CPosEdMainDoc* m_pDocument;
	int			   m_Type;	// p��rodn� objekty

	ObjectBank m_Templates;
	
	CPosEdEnvironment* m_pEnvir;

	int m_nLastSel;
	//{{AFX_DATA(CDefObjectsDlg)
	enum { IDD = IDD_DEF_OBJECTS_DLG };
	CVariantListBox	 m_List;
	CFMntColorButton m_FrameColor;
	CFMntColorButton m_ColorColor;

	int     m_ColorType;
	int		m_FrameType;
	BOOL    m_bRanSize;
	double	m_nRanSize;
	//}}AFX_DATA

	void UpdateList(int nSel = -1);
	void AddObjectTemplateFromFile(LPCTSTR pFileName);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDefObjectsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDefObjectsDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnAdd();
	afx_msg void OnDel();
	afx_msg void OnDeleteAllEmpty(); 
	afx_msg void OnSelchangeList();
	afx_msg void OnChangeColorDef();
	afx_msg void OnChangeFrameDef();
	afx_msg void OnEditColorColor();
	afx_msg void OnEditFrameColor();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedAddBrowse();
	afx_msg void OnBnClickedFileBrowse();
};

CDefObjectsDlg::CDefObjectsDlg(CWnd* pParent /*=NULL*/)
: CDialog(CDefObjectsDlg::IDD, pParent)
{
	m_pEnvir	= GetPosEdEnvironment();
	m_nLastSel	= -1;
	//{{AFX_DATA_INIT(CDefObjectsDlg)
	m_bRanSize = FALSE;
	m_nRanSize = 0.0;
	//}}AFX_DATA_INIT
	m_List.m_nVariant = CVariantListBox::itmVariantNObj;
}

CDefObjectsDlg::~CDefObjectsDlg()
{	
}

BOOL CDefObjectsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// titulek dialogu
	CString sText;
/*
	VERIFY(sText.LoadString(m_bNatural?IDS_NATOBJS_DLG_TITLE:IDS_PPLOBJS_DLG_TITLE));
*/	
	switch (m_Type)
	{
	case CPosObjectTemplate::objTpNatural:
		{
			sText = "Nature Objects Definition";
		}
		break;
	case CPosObjectTemplate::objTpPeople:
		{
			sText = "Artificial Objects Definition";
		}
		break;
	case CPosObjectTemplate::objTpNewRoads:
		{
			sText = "New Roads Objects Definition";
		}
		break;
	default:
		{
			sText = "Artificial Objects Definition";
		}
	}

	::SetWindowText(m_hWnd,sText);
	// nastaven� seznamu objekt�
	UpdateList(0);
	// data objektu
	OnSelchangeList();
	return TRUE;  
}

void CDefObjectsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDefObjectsDlg)
	DDX_Control(pDX, IDC_LIST, m_List);
	DDX_Control(pDX, IDC_FRAME_COLOR, m_FrameColor);
	DDX_Control(pDX, IDC_COLOR_COLOR, m_ColorColor);

 
	//}}AFX_DATA_MAP
	if (SAVEDATA)
	{
		if (m_nLastSel != -1)
		{	// vlo�en� zm�n
			CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(m_List.GetItemData(m_nLastSel));
			ASSERT(pTempl != NULL);

			CString name = pTempl->GetName();
			MDDX_Text(pDX, IDC_EDIT_NAME, name);
			MDDV_MinChars(pDX, name, 1);
			MDDV_MaxChars(pDX, name, MAX_OBJECT_NAME_LEN, IDC_EDIT_NAME);
			pTempl->SetName(name);

			// barvy
			DDX_Radio(pDX, IDC_COLOR_STD, m_ColorType);
			DDX_Radio(pDX, IDC_FRAME_STD, m_FrameType);
			if (m_ColorType == 0)
			{
				pTempl->m_clrEntire = DEFAULT_COLOR;
			}
			else
			{
				pTempl->m_clrEntire = m_ColorColor.m_cColor;
			}

			if (m_FrameType == 0)
			{
				pTempl->m_clrFrame = DEFAULT_COLOR;
			}
			else
			{
				pTempl->m_clrFrame = m_FrameColor.m_cColor;
			}
      
			int posType = pTempl->GetPosType();
			DDX_Radio(pDX, IDC_MARKER_RECT, posType);
			pTempl->SetPosType((CPosObjectTemplate::PosType) posType);

			// randomizace objektu
			DDX_Check(pDX, IDC_CHECK_RAN_SIZE, pTempl->m_bRandomSize);
			MDDX_Text(pDX, IDC_RAN_MIN_SIZE, pTempl->m_nMinRanSize);
			MDDV_MinMaxDouble(pDX, pTempl->m_nMinRanSize, MIN_OBJRANDOM_SIZE, min(95,MAX_OBJRANDOM_SIZE), IDC_RAN_MIN_SIZE);
			MDDX_Text(pDX, IDC_RAN_MAX_SIZE, pTempl->m_nMaxRanSize);
			MDDV_MinMaxDouble(pDX, pTempl->m_nMaxRanSize, MIN_OBJRANDOM_SIZE, MAX_OBJRANDOM_SIZE, IDC_RAN_MAX_SIZE);
			DDX_Check(pDX, IDC_CHECK_RAN_VERT, pTempl->m_bRandomVert);
			MDDX_Text(pDX, IDC_RAN_MIN_VERT, pTempl->m_nMinRanVert);
			MDDV_MinMaxDouble(pDX, pTempl->m_nMinRanVert, MIN_OBJRANDOM_VERT, MAX_OBJRANDOM_VERT, IDC_RAN_MIN_VERT);
			MDDX_Text(pDX, IDC_RAN_MAX_VERT, pTempl->m_nMaxRanVert);
			MDDV_MinMaxDouble(pDX, pTempl->m_nMaxRanVert, MIN_OBJRANDOM_VERT, MAX_OBJRANDOM_VERT, IDC_RAN_MAX_VERT);
			DDX_Check(pDX, IDC_CHECK_RAN_ROTA, pTempl->m_bRandomRota);
			MDDX_Text(pDX, IDC_RAN_MIN_ROTA, pTempl->m_nMinRanRota);
			MDDV_MinMaxDouble(pDX, pTempl->m_nMinRanRota, MIN_OBJRANDOM_ROTA, MAX_OBJRANDOM_ROTA, IDC_RAN_MIN_ROTA);
			MDDX_Text(pDX, IDC_RAN_MAX_ROTA, pTempl->m_nMaxRanRota);
			MDDV_MinMaxDouble(pDX, pTempl->m_nMaxRanRota, MIN_OBJRANDOM_ROTA, MAX_OBJRANDOM_ROTA, IDC_RAN_MAX_ROTA);
		}
	} 
	else
	{	// nov� data
		if (m_nLastSel == -1)
		{	// prvek nen� ur�en
			::SetWindowText(DLGCTRL(IDC_FILE_NAME),_T(""));
			::SetWindowText(DLGCTRL(IDC_EDIT_NAME),_T(""));
			::EnableWindow(DLGCTRL(IDC_FRAME_STD),FALSE);
			::EnableWindow(DLGCTRL(IDC_FRAME_USER),FALSE);
			::EnableWindow(DLGCTRL(IDC_FRAME_COLOR),FALSE);
			::EnableWindow(DLGCTRL(IDC_COLOR_STD),FALSE);
			::EnableWindow(DLGCTRL(IDC_COLOR_USER),FALSE);
			::EnableWindow(DLGCTRL(IDC_COLOR_COLOR),FALSE);
			::EnableWindow(DLGCTRL(IDC_MARKER_RECT),FALSE);
			::EnableWindow(DLGCTRL(IDC_MARKER_ELLIPSE),FALSE);
			::EnableWindow(DLGCTRL(IDC_MARKER_2D),FALSE);
		}
		else      
		{	// prvek je ur�en
			CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(m_List.GetItemData(m_nLastSel));
			ASSERT(pTempl != NULL);

			::EnableWindow(DLGCTRL(IDC_FRAME_STD),TRUE);
			::EnableWindow(DLGCTRL(IDC_FRAME_USER),TRUE);
			::EnableWindow(DLGCTRL(IDC_FRAME_COLOR),TRUE);
			::EnableWindow(DLGCTRL(IDC_COLOR_STD),TRUE);
			::EnableWindow(DLGCTRL(IDC_COLOR_USER),TRUE);
			::EnableWindow(DLGCTRL(IDC_COLOR_COLOR),TRUE);
			::EnableWindow(DLGCTRL(IDC_MARKER_RECT),TRUE);
			::EnableWindow(DLGCTRL(IDC_MARKER_ELLIPSE),TRUE);
			::SetWindowText(DLGCTRL(IDC_FILE_NAME),pTempl->GetFileName());

			CString name = pTempl->GetName();
			MDDX_Text(pDX, IDC_EDIT_NAME, name);
			MDDV_MaxChars(pDX,name, MAX_OBJECT_NAME_LEN, IDC_EDIT_NAME);
			pTempl->SetName(name);

			// barvy
			m_ColorType = (pTempl->m_clrEntire == DEFAULT_COLOR) ? 0 : 1;
			m_FrameType = (pTempl->m_clrFrame  == DEFAULT_COLOR) ? 0 : 1;
			DDX_Radio(pDX, IDC_COLOR_STD, m_ColorType);
			DDX_Radio(pDX, IDC_FRAME_STD, m_FrameType);
			// barvy do tla��tek
			m_FrameColor.m_cColor = pTempl->GetObjectFrameColor(&m_pEnvir->m_cfgCurrent);
			m_FrameColor.Invalidate();
			m_ColorColor.m_cColor = pTempl->GetObjectEntireColor(&m_pEnvir->m_cfgCurrent);
			m_ColorColor.Invalidate();
			OnChangeColorDef();
			OnChangeFrameDef();

			int posType = pTempl->GetPosType();
			DDX_Radio(pDX, IDC_MARKER_RECT, posType);
			pTempl->SetPosType((CPosObjectTemplate::PosType) posType);

			// randomizace objektu
			DDX_Check(pDX, IDC_CHECK_RAN_SIZE, pTempl->m_bRandomSize);
			MDDX_Text(pDX, IDC_RAN_MIN_SIZE, pTempl->m_nMinRanSize);
			MDDV_MinMaxDouble(pDX, pTempl->m_nMinRanSize, MIN_OBJRANDOM_SIZE, min(95,MAX_OBJRANDOM_SIZE), IDC_RAN_MIN_SIZE);
			MDDX_Text(pDX, IDC_RAN_MAX_SIZE, pTempl->m_nMaxRanSize);
			MDDV_MinMaxDouble(pDX, pTempl->m_nMaxRanSize, MIN_OBJRANDOM_SIZE, MAX_OBJRANDOM_SIZE, IDC_RAN_MAX_SIZE);
			DDX_Check(pDX, IDC_CHECK_RAN_VERT, pTempl->m_bRandomVert);
			MDDX_Text(pDX, IDC_RAN_MIN_VERT, pTempl->m_nMinRanVert);
			MDDV_MinMaxDouble(pDX, pTempl->m_nMinRanVert, MIN_OBJRANDOM_VERT, MAX_OBJRANDOM_VERT, IDC_RAN_MIN_VERT);
			MDDX_Text(pDX, IDC_RAN_MAX_VERT, pTempl->m_nMaxRanVert);
			MDDV_MinMaxDouble(pDX, pTempl->m_nMaxRanVert, MIN_OBJRANDOM_VERT, MAX_OBJRANDOM_VERT, IDC_RAN_MAX_VERT);
			DDX_Check(pDX, IDC_CHECK_RAN_ROTA, pTempl->m_bRandomRota);
			MDDX_Text(pDX, IDC_RAN_MIN_ROTA, pTempl->m_nMinRanRota);
			MDDV_MinMaxDouble(pDX, pTempl->m_nMinRanRota, MIN_OBJRANDOM_ROTA, MAX_OBJRANDOM_ROTA, IDC_RAN_MIN_ROTA);
			MDDX_Text(pDX, IDC_RAN_MAX_ROTA, pTempl->m_nMaxRanRota);
			MDDV_MinMaxDouble(pDX, pTempl->m_nMaxRanRota, MIN_OBJRANDOM_ROTA, MAX_OBJRANDOM_ROTA, IDC_RAN_MAX_ROTA);
		}
	}
}

BEGIN_MESSAGE_MAP(CDefObjectsDlg, CDialog)
	//{{AFX_MSG_MAP(CDefObjectsDlg)
//	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_DELETEALLEMPTY, OnDeleteAllEmpty)	
	ON_BN_CLICKED(IDC_DEL, OnDel)
	ON_LBN_SELCHANGE(IDC_LIST, OnSelchangeList)
	ON_BN_CLICKED(IDC_COLOR_STD, OnChangeColorDef)
	ON_BN_CLICKED(IDC_FRAME_STD, OnChangeFrameDef)
	ON_BN_CLICKED(IDC_COLOR_USER, OnChangeColorDef)
	ON_BN_CLICKED(IDC_FRAME_USER, OnChangeFrameDef)
	ON_BN_CLICKED(IDC_COLOR_COLOR, OnEditColorColor)
	ON_BN_CLICKED(IDC_FRAME_COLOR, OnEditFrameColor)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_ADD_BROWSE, OnBnClickedAddBrowse)
	ON_BN_CLICKED(IDC_FILE_BROWSE, OnBnClickedFileBrowse)
END_MESSAGE_MAP()

void CDefObjectsDlg::UpdateList(int nSel)
{
	m_List.ResetContent();
	for (int i = 0; i < m_Templates.Size(); ++i)
	{
		CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(m_Templates[i]);
		if (pTempl)
		{
            m_List.AddStringEx( pTempl->GetName(), (DWORD)pTempl);
			//LBAddString(m_List.m_hWnd, pTempl->m_sObjName, (DWORD)pTempl);
		}
	}
	if ((m_List.GetCount() > 0) && (nSel >= 0))
	{
		if (nSel >= m_List.GetCount()) nSel =  m_List.GetCount() - 1;
		m_List.SetCurSel(nSel);
		OnSelchangeList();
	}
}

void CDefObjectsDlg::AddObjectTemplateFromFile(LPCTSTR pFileName)
{
	CString fileName(pFileName);
	
	for (int i = 0; i < m_Templates.Size(); ++i)
	{
		CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(m_Templates[i]);
		if (fileName.CompareNoCase(pTempl->GetFileName()) == 0) return;
	}

	// dosud neexistuje
	Ref<CPosObjectTemplate> pTempl;
	TRY
	{
		pTempl = new CPosObjectTemplate(m_pDocument);
		
		pTempl->CreateFromObjFile(pFileName);
		// typ objektu
/*
		if (m_bNatural)
		{		
			pTempl->m_nObjType = CPosObjectTemplate::objTpNatural;
			pTempl->SetPosType(CPosObjectTemplate::posTypeEllipse);
		}
		else
		{
			pTempl->m_nObjType = CPosObjectTemplate::objTpPeople;
			pTempl->SetPosType(CPosObjectTemplate::posTypePoly);
		}
*/

		switch (m_Type)
		{
		case CPosObjectTemplate::objTpNatural:
			{
				pTempl->m_nObjType = CPosObjectTemplate::objTpNatural;
		        pTempl->SetPosType(CPosObjectTemplate::posTypeEllipse);
			}
			break;
		case CPosObjectTemplate::objTpPeople:
			{
				pTempl->m_nObjType = CPosObjectTemplate::objTpPeople;
		        pTempl->SetPosType(CPosObjectTemplate::posTypePoly);
			}
			break;
		case CPosObjectTemplate::objTpNewRoads:
			{
				pTempl->m_nObjType = CPosObjectTemplate::objTpNewRoads;
		        pTempl->SetPosType(CPosObjectTemplate::posTypePoly);
			}
			break;
		default:
			{
				pTempl->m_nObjType = CPosObjectTemplate::objTpPeople;
		        pTempl->SetPosType(CPosObjectTemplate::posTypePoly);
			}
		}

		// vlo�en� do �ablon
		m_Templates.Add(pTempl);
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
	}
	END_CATCH_ALL
}

void CDefObjectsDlg::OnChangeColorDef() 
{	
	::ShowWindow(DLGCTRL(IDC_COLOR_COLOR),(IsDlgButtonChecked(IDC_COLOR_USER) != 0) ? SW_SHOW : SW_HIDE); 
	m_List.Invalidate();
}

void CDefObjectsDlg::OnChangeFrameDef() 
{	
	::ShowWindow(DLGCTRL(IDC_FRAME_COLOR),(IsDlgButtonChecked(IDC_FRAME_USER) != 0) ? SW_SHOW : SW_HIDE); 
	m_List.Invalidate();
}

void CDefObjectsDlg::OnEditColorColor() 
{	
	m_ColorColor.DoEditColor(); 
	m_List.Invalidate();
}

void CDefObjectsDlg::OnEditFrameColor() 
{	
	m_FrameColor.DoEditColor(); 
	m_List.Invalidate();
}

void CDefObjectsDlg::OnSelchangeList() 
{
	int nIndx = m_List.GetCurSel();
	if (nIndx == LB_ERR)
	{	// nelze nic smazat
		::EnableWindow(DLGCTRL(IDC_DEL),FALSE);
	} 
	else
	{	// povoleno smazat
		::EnableWindow(DLGCTRL(IDC_DEL),TRUE);
	}

	if (m_List.GetCount() > 0)
	{
		::EnableWindow(DLGCTRL(IDC_DELETEALLEMPTY),TRUE);
	}
	else
	{
		::EnableWindow(DLGCTRL(IDC_DELETEALLEMPTY),FALSE);
	}

	// update p�vodn�ho
	if (nIndx == m_nLastSel) return;

	if (!UpdateData(TRUE))
	{
		m_List.SetCurSel(m_nLastSel);
		OnSelchangeList();
		return;
	}
	// nastav�m nov� prvek
	m_nLastSel = nIndx;
	UpdateData(FALSE);
}

void CDefObjectsDlg::OnAdd() 
{
	if (!UpdateData(TRUE)) return;

	CAddObjectsDlg dlg;
	dlg.m_pDocument = m_pDocument;
	dlg.m_Type      = m_Type;

	if (dlg.DoModal() != IDOK) return;

	// je seznam objekt� pro vlo�en�
	for(int i = 0; i < dlg.m_Results.GetSize(); ++i)
	{
		AddObjectTemplateFromFile(dlg.m_Results[i]);
	}

	// update seznamu
	m_nLastSel = -1;
	UpdateList(0);
	if (dlg.m_Results.GetSize() == 1)
	{	// vyberu vlo�en� prvek
		for(int i = 0; i < m_List.GetCount(); ++i)
		{
			CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(m_List.GetItemData(i));
			if ((pTempl) && (dlg.m_Results[0].CompareNoCase(pTempl->GetFileName()) == 0))
			{
				m_List.SetCurSel(i);
				OnSelchangeList();        
				break;
			}
		}
	}
}

void CDefObjectsDlg::OnBnClickedAddBrowse()
{
	CString subDir;
	m_pDocument->GetProjectDirectory(subDir, CPosEdBaseDoc::dirObj);
	MakeShortDirStr(subDir, subDir);

	CString initDir; 

/*
  if (m_bNatural)
    initDir = GetPosEdEnvironment()->m_optSystem.m_sObjNaturPath;
  else
    initDir = GetPosEdEnvironment()->m_optSystem.m_sObjPeoplPath;
*/

	switch (m_Type)
	{
	case CPosObjectTemplate::objTpNatural:
		{
			initDir = GetPosEdEnvironment()->m_optSystem.m_sObjNaturPath;
		}
		break;
	case CPosObjectTemplate::objTpPeople:
		{
		    initDir = GetPosEdEnvironment()->m_optSystem.m_sObjPeoplPath;
		}
		break;
	case CPosObjectTemplate::objTpNewRoads:
		{
		    initDir = GetPosEdEnvironment()->m_optSystem.m_sObjNewRoadsPath;
		}
		break;
	default:
		{
		    initDir = GetPosEdEnvironment()->m_optSystem.m_sObjPeoplPath;
		}
		break;
	}

	// initDir must be under subdir
	if(subDir.CompareNoCase(initDir.Left(subDir.GetLength())) != 0)  
    initDir = subDir;  

	CStringArray files;
	CString filter(_T("Objects (*.p3d)|*.p3d||"));  
	if (!DoLoadFilesFromSubDir(files, subDir, initDir, filter)) return;

/*
  if (m_bNatural)
    SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sObjNaturPath, files[0]);
  else
    SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sObjPeoplPath, files[0]);
*/

	switch (m_Type)
	{
	case CPosObjectTemplate::objTpNatural:
		{
		    SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sObjNaturPath, files[0]);
		}
		break;
	case CPosObjectTemplate::objTpPeople:
		{
		    SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sObjPeoplPath, files[0]);
		}
		break;
	case CPosObjectTemplate::objTpNewRoads:
		{
		    SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sObjNewRoadsPath, files[0]);
		}
		break;
	default:
		{
		    SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sObjPeoplPath, files[0]);
		}
		break;
	}

	GetPosEdEnvironment()->m_optSystem.SaveParams();

	int subdirL = subDir.GetLength() + 1;

	for(int i = 0; i < files.GetSize(); ++i)
	{
	    AddObjectTemplateFromFile(files[i].Right(files[i].GetLength() - subdirL));
	}

	// update seznamu
	m_nLastSel = -1;
	UpdateList(0);
	if (files.GetSize() == 1)
	{	// vyberu vlo�en� prvek
		CString selName = files[0].Right(files[0].GetLength() - subdirL);

		for(int i = 0; i < m_List.GetCount(); ++i)
		{
			CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(m_List.GetItemData(i));
			if ((pTempl) && (selName.CompareNoCase(pTempl->GetFileName()) == 0))
			{
				m_List.SetCurSel(i);
				OnSelchangeList();
				break;
			}
		}
	}
}

void CDefObjectsDlg::OnBnClickedFileBrowse()
{
	CString subDir;
	m_pDocument->GetProjectDirectory(subDir, CPosEdBaseDoc::dirObj);
	MakeShortDirStr(subDir, subDir);

	CString initDir; 

/*
  if (m_bNatural)
    initDir = GetPosEdEnvironment()->m_optSystem.m_sObjNaturPath;
  else
    initDir = GetPosEdEnvironment()->m_optSystem.m_sObjPeoplPath;
*/

	switch (m_Type)
	{
	case CPosObjectTemplate::objTpNatural:
		{
			initDir = GetPosEdEnvironment()->m_optSystem.m_sObjNaturPath;
		}
		break;
	case CPosObjectTemplate::objTpPeople:
		{
		    initDir = GetPosEdEnvironment()->m_optSystem.m_sObjPeoplPath;
		}
		break;
	case CPosObjectTemplate::objTpNewRoads:
		{
		    initDir = GetPosEdEnvironment()->m_optSystem.m_sObjNewRoadsPath;
		}
		break;
	default:
		{
		    initDir = GetPosEdEnvironment()->m_optSystem.m_sObjPeoplPath;
		}
		break;
	}

	// initDir must be under subdir
	if(subDir.CompareNoCase(initDir.Left(subDir.GetLength())) != 0) initDir = subDir;  

	CString file;
	CString filter(_T("Objects (*.p3d)|*.p3d||"));  
	if (!DoLoadFileFromSubDir(file, subDir, initDir, filter)) return;

/*
  if (m_bNatural)
    SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sObjNaturPath, file);
  else
    SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sObjPeoplPath, file);
*/

	switch (m_Type)
	{
	case CPosObjectTemplate::objTpNatural:
		{
		    SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sObjNaturPath, file);
		}
		break;
	case CPosObjectTemplate::objTpPeople:
		{
		    SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sObjPeoplPath, file);
		}
		break;
	case CPosObjectTemplate::objTpNewRoads:
		{
		    SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sObjNewRoadsPath, file);
		}
		break;
	default:
		{
		    SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sObjPeoplPath, file);
		}
		break;
	}

	GetPosEdEnvironment()->m_optSystem.SaveParams();

	int subdirL = subDir.GetLength() + 1;

	int nIndx = m_List.GetCurSel();
	if (nIndx == LB_ERR) return;

	CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(m_List.GetItemData(nIndx));
	pTempl->SetFileName(file.Right(file.GetLength() - subdirL));
	pTempl->UpdateParamsFromSourceFile();
	UpdateData(FALSE);
}

void CDefObjectsDlg::OnDeleteAllEmpty() 
{
	int i = 0;

	while (i < m_List.GetCount())
	{
		CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(m_List.GetItemData(i));

		if (pTempl)
		{
			if (pTempl->m_nTimesUsed <= 0)
			{
				for (int j = 0; j < m_Templates.Size(); ++j)
				{
					CPosObjectTemplate* pComp = (CPosObjectTemplate*)(m_Templates[j]);
					if ((pComp) && (pComp == pTempl))
					{
						m_Templates.Delete(j);					
						m_nLastSel = -1;
					}
				}
			}
		}
		i++;
	}
	UpdateList(-1);
}

void CDefObjectsDlg::OnDel() 
{
	int nIndx = m_List.GetCurSel();
	if (nIndx == LB_ERR)
	{
		MessageBeep(MB_DEFAULT);
		return;
	}

	// smazat ?
	CPosObjectTemplate* pTempl = (CPosObjectTemplate*)(m_List.GetItemData(nIndx));
  
	if (pTempl->m_nTimesUsed > 0)
	{
		// in use
		AfxMessageBox(IDS_OBJECT_DEL_USED);
		return;
	}

	if (pTempl)
	{
		CString sText;
		AfxFormatString1(sText, IDS_VERIFY_DEL_OBJ_TEMPL, pTempl->GetName());
		if (AfxMessageBox(sText, MB_YESNOCANCEL | MB_ICONQUESTION) == IDYES)
		{	// vyjmu ze seznamu a sma�u
			for (int i = 0; i < m_Templates.Size(); ++i)
			{
				CPosObjectTemplate* pComp = (CPosObjectTemplate*)(m_Templates[i]);
				if ((pComp) && (pComp == pTempl))
				{
					m_Templates.Delete(i);					
					m_nLastSel = -1;
					UpdateList(nIndx);
					return;
				}
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// definice objekt� obecn�

/*
// old code

BOOL DoDefineTemplateObjects(CPosEdMainDoc* pDoc, BOOL bNatural)
{
	CDefObjectsDlg	dlg;
	dlg.m_bNatural = bNatural;
	dlg.m_pDocument= pDoc;

	if (!dlg.m_Templates.HardCopyFrom(pDoc->GetObjectTemplates(), bNatural ? (CPosObjectTemplate::objTpNatural) : (CPosObjectTemplate::objTpPeople)))
		return FALSE;
	if (dlg.DoModal() != IDOK)
		return FALSE;
	// zm�na v seznamu objekt� ?!
  
	if (!dlg.m_Templates.SyncTarget(pDoc->GetObjectTemplates(), bNatural ? (CPosObjectTemplate::objTpNatural) : (CPosObjectTemplate::objTpPeople)))
		return FALSE;
	return TRUE;
};
*/

BOOL DoDefineTemplateObjects(CPosEdMainDoc* pDoc, int type)
{
	CDefObjectsDlg	dlg;
	dlg.m_Type = type;
	dlg.m_pDocument= pDoc;

	if (!dlg.m_Templates.HardCopyFrom(pDoc->GetObjectTemplates(), type)) return FALSE;

	if (dlg.DoModal() != IDOK) return FALSE;
	// zm�na v seznamu objekt� ?!
  
	if (!dlg.m_Templates.SyncTarget(pDoc->GetObjectTemplates(), type)) return FALSE;
	return TRUE;
};

/////////////////////////////////////////////////////////////////////////////
// definice p��rodn�ch objekt�

BOOL DoDefineTemplateNatObjs(CPosEdMainDoc* pDoc)
{	
	return DoDefineTemplateObjects(pDoc, CPosObjectTemplate::objTpNatural); 
};






