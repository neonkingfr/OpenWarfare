/////////////////////////////////////////////////////////////////////////////
// CNumEdit window

class CNumEdit : public CEdit
{
protected:
	CString m_lastString;
	// Construction
public:
	CNumEdit();
	// Attributes
	enum {VALID = 0x00, OUT_OF_RANGE = 0x01, INVALID_CHAR = 0x02};
	// Operations
protected:
	int IsValid(const CString &str);
	// Implementation
public:
	void GetRange(float &max, float &min);
	void SetRange(float max, float min);
	void Verbose(BOOL v);
	BOOL Verbose();
	BOOL OnChange();  
	BOOL IsValid();
	void SetValue(float val);
	float GetValue();
	virtual ~CNumEdit();

	// Generated message map functions
protected:
	BOOL m_Verbose;
	float m_MinValue, m_MaxValue; 

	DECLARE_MESSAGE_MAP()
};

