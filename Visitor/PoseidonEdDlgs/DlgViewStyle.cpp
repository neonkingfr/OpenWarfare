/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright © Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CViewStyleDlg dialog

class CViewStyleDlg : public CDialog
{
// Construction
public:
	CViewStyleDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	CViewStyle m_ViewStyle;

	//{{AFX_DATA(CViewStyleDlg)
	enum { IDD = IDD_VIEW_STYLE_DLG };
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CViewStyleDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
	virtual BOOL OnInitDialog(void);

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CViewStyleDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// CViewStyleDlg dialog


CViewStyleDlg::CViewStyleDlg(CWnd* pParent /*=NULL*/)
: CDialog(CViewStyleDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CViewStyleDlg)
	//}}AFX_DATA_INIT
}

BOOL CViewStyleDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();

#ifndef __ONLY_FOR_PUBLIC__
	GetDlgItem(IDC_NEWROADS_OBJCS)->ShowWindow(TRUE);
#else
	GetDlgItem(IDC_NEWROADS_OBJCS)->ShowWindow(FALSE);
#endif

	return TRUE;
}

void CViewStyleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CViewStyleDlg)
	//}}AFX_DATA_MAP
	DDX_Radio(pDX, IDC_VIEW_STYLE0, m_ViewStyle.m_nBaseStyle);

	DDX_Check(pDX, IDC_SHOW_COUNTLN,  m_ViewStyle.m_bShowCountLn);
	DDX_Check(pDX, IDC_SHOW_GRID,	  m_ViewStyle.m_bShowGrid);
	DDX_Check(pDX, IDC_SHOW_SCNDTXTR, m_ViewStyle.m_bShowScndTxtr);
	DDX_Check(pDX, IDC_SHOW_SEA,	  m_ViewStyle.m_bShowSea);
	DDX_Check(pDX, IDC_SHOW_RIVERS,	  m_ViewStyle.m_bWaterLayer);

	DDX_Check(pDX, IDC_NATURE_OBJCS,   m_ViewStyle.m_bNatureObjcs);
	DDX_Check(pDX, IDC_PEOPLE_OBJCS,   m_ViewStyle.m_bPeopleObjcs);

	DDX_Check(pDX, IDC_NEWROADS_OBJCS, m_ViewStyle.m_bNewRoadsObjcs);
	
	DDX_Check(pDX, IDC_SHOW_NETS,  m_ViewStyle.m_bShowNets);
	DDX_Check(pDX, IDC_SHOW_WOODS, m_ViewStyle.m_bShowWoods);

	DDX_Check(pDX, IDC_AREAS_BORDER, m_ViewStyle.m_bAreasBorder);
	DDX_Check(pDX, IDC_AREAS_ENTIRE, m_ViewStyle.m_bAreasEntire);
	DDX_Check(pDX, IDC_KEYPT_BORDER, m_ViewStyle.m_bKeyPtBorder);
	DDX_Check(pDX, IDC_KEYPT_ENTIRE, m_ViewStyle.m_bKeyPtEntire);
	
	DDX_Check(pDX, IDC_SHOW_RULER,     m_ViewStyle.m_bShowRuler);
	DDX_Check(pDX, IDC_SHOW_BG_IMAGES, m_ViewStyle.m_bShowBGImage);
	DDX_Check(pDX, IDC_SHOW_SHADOWING, m_ViewStyle.m_Shadowing);
	DDX_Check(pDX, IDC_SHOW_LOCK,      m_ViewStyle.m_ShowLock);
}

BEGIN_MESSAGE_MAP(CViewStyleDlg, CDialog)
	//{{AFX_MSG_MAP(CViewStyleDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Parametry zobrazení okna		(DlgViewStyle.cpp)

BOOL OnEditViewStyle(CViewStyle* pStyle)
{
	CViewStyleDlg dlg;
	dlg.m_ViewStyle.CopyFrom(pStyle);

	if (dlg.DoModal()==IDOK)
	{
		pStyle->CopyFrom(&dlg.m_ViewStyle);
		return TRUE;
	}
	return FALSE;
};


