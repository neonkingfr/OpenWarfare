/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSysOptsDirPage dialog

/*class CSysOptsDirPage : public CMntPropertyPage
{
// Construction
public:
	CSysOptsDirPage();

	BOOL m_bShowDangerous;

// Dialog Data
	//{{AFX_DATA(CSysOptsDirPage)
	enum { IDD = IDD_SYSOPTS_DIR_PAGE };
	CString	m_sBaseDir;
	CString	m_sTexturesPath;
	CString	m_sObjNaturPath;
	CString	m_sObjPeoplPath;
	CString	m_sObjOtherPath;
	CString	m_sObjWorldPath;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CSysOptsDirPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog( );
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CSysOptsDirPage)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

CSysOptsDirPage::CSysOptsDirPage() : CMntPropertyPage(CSysOptsDirPage::IDD), m_bShowDangerous(TRUE)
{
	//{{AFX_DATA_INIT(CSysOptsDirPage)
	//}}AFX_DATA_INIT
}

void CSysOptsDirPage::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSysOptsDirPage)
	//}}AFX_DATA_MAP
	MDDX_Text(pDX, IDC_EDIT_BASE_DIR, m_sBaseDir);
	MDDV_MaxChars(pDX, m_sBaseDir, _MAX_PATH, IDC_EDIT_BASE_DIR);

	MDDX_Text(pDX, IDC_EDIT_DIR_TEXT, m_sTexturesPath);
	MDDV_MaxChars(pDX, m_sTexturesPath, _MAX_PATH, IDC_EDIT_DIR_TEXT);
	MDDX_Text(pDX, IDC_EDIT_DIR_ONAT, m_sObjNaturPath);
	MDDV_MaxChars(pDX, m_sObjNaturPath, _MAX_PATH, IDC_EDIT_DIR_ONAT);
	MDDX_Text(pDX, IDC_EDIT_DIR_OPEO, m_sObjPeoplPath);
	MDDV_MaxChars(pDX, m_sObjPeoplPath, _MAX_PATH, IDC_EDIT_DIR_OPEO);
	MDDX_Text(pDX, IDC_EDIT_DIR_OOTH, m_sObjOtherPath);
	MDDV_MaxChars(pDX, m_sObjOtherPath, _MAX_PATH, IDC_EDIT_DIR_OOTH);
	MDDX_Text(pDX, IDC_EDIT_DIR_WRLD, m_sObjWorldPath);
	MDDV_MaxChars(pDX, m_sObjWorldPath, _MAX_PATH, IDC_EDIT_DIR_WRLD);
}

BOOL CSysOptsDirPage::OnInitDialog( )
{
	CMntPropertyPage::OnInitDialog();

	if (!m_bShowDangerous)
	{
		GetDlgItem(IDC_EDIT_DIR_ONAT)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_DIR_TEXT)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_DIR_OPEO)->EnableWindow(FALSE);	
		GetDlgItem(IDC_EDIT_DIR_OOTH)->EnableWindow(FALSE);	
	}

	return TRUE;
}


BEGIN_MESSAGE_MAP(CSysOptsDirPage, CMntPropertyPage)
	//{{AFX_MSG_MAP(CSysOptsDirPage)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSysOptsAppPage dialog

class CSysOptsAppPage : public CMntPropertyPage
{
// Construction
public:
	CSysOptsAppPage();

	BOOL m_bShowDangerous;

// Dialog Data
	//{{AFX_DATA(CSysOptsAppPage)
	enum { IDD = IDD_SYSOPTS_APP_PAGE };
	CString	m_sPreviewAppPath;
	CString	m_sBankMapsPath;
	CString	m_sBankTexture;
	int		m_nStdWorldHeight;
	int		m_nStdWorldWidth;
	int		m_nRealSizeUnit;
	BOOL	m_bStdSizeOnly;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CSysOptsAppPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog( );
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CSysOptsAppPage)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

CSysOptsAppPage::CSysOptsAppPage() : CMntPropertyPage(CSysOptsAppPage::IDD), m_bShowDangerous(TRUE)
{
	//{{AFX_DATA_INIT(CSysOptsAppPage)
	m_bStdSizeOnly = FALSE;
	//}}AFX_DATA_INIT
}

void CSysOptsAppPage::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSysOptsAppPage)
	DDX_Check(pDX, IDC_CHECK_STDONLY, m_bStdSizeOnly);
	//}}AFX_DATA_MAP
	MDDX_Text(pDX, IDC_EDIT_APP_PATH, m_sPreviewAppPath);
	MDDV_MaxChars(pDX, m_sPreviewAppPath, _MAX_PATH, IDC_EDIT_APP_PATH);
	MDDX_Text(pDX, IDC_EDIT_STDHEIGHT, m_nStdWorldHeight);
	MDDV_MinMaxInt(pDX, m_nStdWorldHeight, MIN_WORLD_SIZE, MAX_WORLD_SIZE, IDC_EDIT_STDHEIGHT);
	MDDX_Text(pDX, IDC_EDIT_STDWIDTH, m_nStdWorldWidth);
	MDDV_MinMaxInt(pDX, m_nStdWorldWidth, MIN_WORLD_SIZE, MAX_WORLD_SIZE, IDC_EDIT_STDWIDTH);
	MDDX_Text(pDX, IDC_EDIT_REALSZ, m_nRealSizeUnit);
	MDDV_MinMaxInt(pDX, m_nRealSizeUnit, MIN_REAL_SIZE, MAX_REAL_SIZE, IDC_EDIT_REALSZ);

	MDDX_Text(pDX, IDC_EDIT_BANK_PATH, m_sBankMapsPath);
	MDDV_MaxChars(pDX, m_sBankMapsPath, _MAX_PATH, IDC_EDIT_BANK_PATH);
	MDDX_Text(pDX, IDC_EDIT_BANK_TEXT, m_sBankTexture);
	MDDV_MaxChars(pDX, m_sBankTexture,  _MAX_PATH, IDC_EDIT_BANK_TEXT);
}

BOOL CSysOptsAppPage::OnInitDialog( )
{
	CMntPropertyPage::OnInitDialog();

	if (!m_bShowDangerous)
	{
		GetDlgItem(IDC_EDIT_STDHEIGHT)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_STDWIDTH)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_REALSZ)->EnableWindow(FALSE);	
		GetDlgItem(IDC_EDIT_BANK_PATH)->EnableWindow(FALSE);	
		GetDlgItem(IDC_EDIT_BANK_TEXT)->EnableWindow(FALSE);	

	}

	return TRUE;
}


BEGIN_MESSAGE_MAP(CSysOptsAppPage, CMntPropertyPage)
	//{{AFX_MSG_MAP(CSysOptsAppPage)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

*/
////////////////////////////////////////////////////////////////////////////
//

class CSysOptsDlg : public CDialog
{
  // Construction
public:
  CSysOptsDlg(CWnd* pParent = NULL);   // standard constructor

  enum { IDD = IDD_SYSOPTS_APP_PAGE};  

  CString m_sBuldCommand;
  CString m_sBuldWorkingDir;
  CString m_sccServerName;
  CString m_sccProjectName;
  CString m_sccLocalPath;

  
protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 
  // Implementation
protected:
  
  DECLARE_MESSAGE_MAP()
};

BEGIN_MESSAGE_MAP(CSysOptsDlg, CDialog)
END_MESSAGE_MAP()

CSysOptsDlg::CSysOptsDlg(CWnd* pParent) : 
CDialog( CSysOptsDlg::IDD, pParent)
{};

void CSysOptsDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);

  MDDX_Text(pDX, IDC_EDIT_APP_COMMAND, m_sBuldCommand);
  MDDX_Text(pDX, IDC_EDIT_APP_PATH, m_sBuldWorkingDir);
}


/////////////////////////////////////////////////////////////////////////////
// Volby programu a syst�mu (DlgSysOpts.cpp)

BOOL DoSetupSystemOptions()
{
  // p��stup k options
  CPosEdEnvironment*	pEnvir = GetPosEdEnvironment();
  if (pEnvir == NULL) return FALSE;

  CFrameWnd * pFrame = ((CFrameWnd *) AfxGetMainWnd())->GetActiveFrame();
  BOOL bShowDangerous = (NULL == pFrame->GetActiveDocument());

  CSysOptsDlg dlg;

  dlg.m_sBuldWorkingDir = pEnvir->m_optSystem.m_sPreviewAppPath;
  dlg.m_sBuldCommand = pEnvir->m_optSystem.m_sPreviewAppCommand;
  dlg.m_sccServerName = pEnvir->m_optSystem.m_sccServerName;
  dlg.m_sccProjectName = pEnvir->m_optSystem.m_sccProjectName;
  dlg.m_sccLocalPath = pEnvir->m_optSystem.m_sccLocalPath;

  if (dlg.DoModal() != IDOK) return FALSE;
  // p�enos dat
  dlg.m_sBuldWorkingDir.MakeLower();
  pEnvir->m_optSystem.m_sPreviewAppPath = dlg.m_sBuldWorkingDir;
  pEnvir->m_optSystem.m_sPreviewAppCommand = dlg.m_sBuldCommand;
  dlg.m_sccServerName.MakeLower();
  pEnvir->m_optSystem.m_sccServerName = dlg.m_sccServerName;
  pEnvir->m_optSystem.m_sccProjectName = dlg.m_sccProjectName;
  dlg.m_sccLocalPath.MakeLower();
  pEnvir->m_optSystem.m_sccLocalPath = dlg.m_sccLocalPath;

  // ulo�en� atribut�
  pEnvir->m_optSystem.SaveParams();

  return TRUE;
};

/*BOOL DoSetupSystemOptions()
{
	// p��stup k options
	CPosEdEnvironment*	pEnvir = GetPosEdEnvironment();
	if (pEnvir == NULL) return FALSE;

	CFrameWnd * pFrame = ((CFrameWnd *) AfxGetMainWnd())->GetActiveFrame();
	BOOL bShowDangerous = (NULL == pFrame->GetActiveDocument());

	//CMntPropertySheet	dlg(IDS_SYSOPTS_TITLE_DLG,PSH_NOAPPLYNOW);
	// sr�nky options
	CSysOptsDirPage		dirPage;
	CSysOptsAppPage		appPage;
	dlg.AddPageIntoWizard(&dirPage);
	dlg.AddPageIntoWizard(&appPage);
	
	// p�enos dat
	dirPage.m_bShowDangerous = bShowDangerous;
	dirPage.m_sBaseDir			= pEnvir->m_optSystem.m_sPosEdDirectory;
	dirPage.m_sTexturesPath		= pEnvir->m_optSystem.m_sTexturesPath;
	dirPage.m_sObjNaturPath		= pEnvir->m_optSystem.m_sObjNaturPath;
	dirPage.m_sObjPeoplPath		= pEnvir->m_optSystem.m_sObjPeoplPath;
	dirPage.m_sObjOtherPath		= pEnvir->m_optSystem.m_sObjOtherPath;
	dirPage.m_sObjWorldPath		= pEnvir->m_optSystem.m_sObjWorldPath;

	appPage.m_bShowDangerous = bShowDangerous;
	appPage.m_sPreviewAppPath	= pEnvir->m_optSystem.m_sPreviewAppPath;
	appPage.m_sBankMapsPath		= pEnvir->m_optSystem.m_sBankMapsPath;
	appPage.m_sBankTexture		= pEnvir->m_optSystem.m_sBankTexture;
	appPage.m_nStdWorldHeight	= pEnvir->m_optSystem.m_nStdWorldHeight;
	appPage.m_nStdWorldWidth	= pEnvir->m_optSystem.m_nStdWorldWidth;
	appPage.m_bStdSizeOnly		= pEnvir->m_optSystem.m_bStdSizeOnly;
	appPage.m_nRealSizeUnit		= pEnvir->m_optSystem.m_nRealSizeUnit;

	if (dlg.DoModal()!=IDOK) return FALSE;
	// p�enos dat
	pEnvir->m_optSystem.m_sPosEdDirectory	= dirPage.m_sBaseDir;
	pEnvir->m_optSystem.m_sOptionsDirectory	= dirPage.m_sBaseDir;

	pEnvir->m_optSystem.m_sTexturesPath		= dirPage.m_sTexturesPath;
	pEnvir->m_optSystem.m_sObjNaturPath		= dirPage.m_sObjNaturPath;
	pEnvir->m_optSystem.m_sObjPeoplPath		= dirPage.m_sObjPeoplPath;
	pEnvir->m_optSystem.m_sObjOtherPath		= dirPage.m_sObjOtherPath;
	pEnvir->m_optSystem.m_sObjWorldPath		= dirPage.m_sObjWorldPath;

	pEnvir->m_optSystem.m_sPreviewAppPath	= appPage.m_sPreviewAppPath;
	pEnvir->m_optSystem.m_sBankMapsPath		= appPage.m_sBankMapsPath;
	pEnvir->m_optSystem.m_sBankTexture		= appPage.m_sBankTexture;
	pEnvir->m_optSystem.m_nStdWorldHeight	= appPage.m_nStdWorldHeight;
	pEnvir->m_optSystem.m_nStdWorldWidth	= appPage.m_nStdWorldWidth;
	pEnvir->m_optSystem.m_bStdSizeOnly		= appPage.m_bStdSizeOnly;
	pEnvir->m_optSystem.m_nRealSizeUnit		= appPage.m_nRealSizeUnit;

	// ulo�en� atribut�
	pEnvir->m_optSystem.SaveParams();
	// zm�na se projev� a� po nov�m spu�t�n�
	//AfxMessageBox(IDS_CHANGE_SYSOPT_NEWRUN,MB_OK|MB_ICONINFORMATION);
	return TRUE;
};*/




