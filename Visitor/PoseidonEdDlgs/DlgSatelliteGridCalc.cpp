// DlgProjectParameters.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "DlgSatelliteGridCalc.h"
#include <string>

using std::string;

// CDlgProjectParameters dialog

IMPLEMENT_DYNAMIC(CDlgSatelliteGridCalc, CDialog)

CDlgSatelliteGridCalc::CDlgSatelliteGridCalc(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSatelliteGridCalc::IDD, pParent)
{
	m_imageSizePixel      = 0;
	m_segmentSizePixel    = 512;
	m_segmentOverlapPixel = 16;
}

CDlgSatelliteGridCalc::~CDlgSatelliteGridCalc()
{
}

void CDlgSatelliteGridCalc::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CDlgSatelliteGridCalc, CDialog)
	ON_EN_CHANGE(IDC_IMAGESIZE_W, OnEnChangeSatImageEdit)
	ON_BN_CLICKED(IDC_APPLY_BTN, OnBnClickedApplyBtn)
	ON_EN_CHANGE(IDC_SATIMAGESEGSIZEPX, OnEnChangeSatImageSegSizePx)
	ON_EN_CHANGE(IDC_SATIMAGESEGOVERPX, OnEnChangeSatImageSegOverPx)
END_MESSAGE_MAP()

BOOL CDlgSatelliteGridCalc::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString data;
	data.Format("%d", m_segmentSizePixel);
	GetDlgItem(IDC_SATIMAGESEGSIZEPX)->SetWindowTextA((LPCTSTR)data);
	data.Format("%d", m_segmentOverlapPixel);
	GetDlgItem(IDC_SATIMAGESEGOVERPX)->SetWindowTextA((LPCTSTR)data);

	return TRUE;
}

void CDlgSatelliteGridCalc::ComputeSatelliteValues()
{
	// new code
	float tempSegmentOverlapPixel = m_segmentOverlapPixel;

	bool done  = false;
	bool valid = false;

	float realWidth;
	float imageResolution;
	float segmentSizeMeter;
	float segmentOverlapMeter;
	float satelliteGridCalc;

	while(!done)
	{
		realWidth           = m_fSquareSize * m_nWorldSquareWidth;
		imageResolution     = realWidth / m_imageSizePixel;
		segmentSizeMeter    = imageResolution * m_segmentSizePixel;
//		float segmentOverlapMeter = imageResolution * m_segmentOverlapPixel;
		segmentOverlapMeter = imageResolution * tempSegmentOverlapPixel;

//		float satelliteGridCalc = (m_segmentSizePixel - m_segmentOverlapPixel) * imageResolution / m_fSquareSize;
		satelliteGridCalc = (m_segmentSizePixel - tempSegmentOverlapPixel) * imageResolution / m_fSquareSize;

		// searches the best fitting between multiple of 8 and multiple of 12
		int i8 = 8;
		while (i8 <= satelliteGridCalc) i8 += 8;
		i8 -= 8;

		int i12 = 12;
		while (i12 <= satelliteGridCalc) i12 += 12;
		i12 -= 12;

		if (i8 > i12)
		{
			m_satelliteGridG = i8;
		}
		else
		{
			m_satelliteGridG = i12;
		}

		if (imageResolution != 0.0)
		{
			m_proposedSegmentOverlapPixel = m_segmentSizePixel - m_satelliteGridG * m_fSquareSize / imageResolution;
		}
		else
		{
			m_proposedSegmentOverlapPixel = 0.0f;
		}

		int gameSegment = static_cast<int>(m_satelliteGridG / (1 << m_activeTextureLayerIndex));

		// now we check if the value is a multiple of 8 or 12, which should be the correct value
		// to pass to the engine
		if ((gameSegment % 8 == 0) || (gameSegment % 12 == 0))
		{
			valid = true;
			done  = true;
		}
		else
		{
			tempSegmentOverlapPixel *= 2;
			if (tempSegmentOverlapPixel >= m_segmentSizePixel)
			{
				done = true;
			}
		}
	}

	CString data;
	data.Format("%.3f", imageResolution);
	GetDlgItem(IDC_SATIMAGERESEDIT)->SetWindowTextA((LPCTSTR)data);
	data.Format("%.3f", segmentSizeMeter);
	GetDlgItem(IDC_SATIMAGESEGSIZEMT)->SetWindowTextA((LPCTSTR)data);
	data.Format("%.3f", segmentOverlapMeter);
	GetDlgItem(IDC_SATIMAGESEGOVERMT)->SetWindowTextA((LPCTSTR)data);

	if (valid)
	{
		data.Format("%.3f", satelliteGridCalc);
		GetDlgItem(IDC_SATELLITEGRID)->SetWindowTextA((LPCTSTR)data);
		data.Format("%d", m_satelliteGridG);
		GetDlgItem(IDC_SATELLITEGRIDPROPOSED)->SetWindowTextA((LPCTSTR)data);
		data.Format("%.3f", m_proposedSegmentOverlapPixel);
		GetDlgItem(IDC_OVERLAPPROPOSED)->SetWindowTextA((LPCTSTR)data);
	}
	else
	{
		GetDlgItem(IDC_SATELLITEGRID)->SetWindowTextA("");
		GetDlgItem(IDC_SATELLITEGRIDPROPOSED)->SetWindowTextA("");
		GetDlgItem(IDC_OVERLAPPROPOSED)->SetWindowTextA("");
	}
}

void CDlgSatelliteGridCalc::OnBnClickedApplyBtn()
{
	CDlgProjectParameters* parent = reinterpret_cast<CDlgProjectParameters*>(GetParent());
	parent->m_satelliteGridG = m_satelliteGridG;
	parent->UpdateData(FALSE);
	parent->ComputeGameSegment();
}

void CDlgSatelliteGridCalc::UpdateValues()
{
	if(m_imageSizePixel == 0 || m_segmentSizePixel == 0 || m_segmentOverlapPixel == 0)
	{
		GetDlgItem(IDC_IMAGESIZE_H)->SetWindowTextA("x");
		GetDlgItem(IDC_SATIMAGERESEDIT)->SetWindowTextA("");
		GetDlgItem(IDC_SATIMAGESEGSIZEMT)->SetWindowTextA("");
		GetDlgItem(IDC_SATIMAGESEGOVERMT)->SetWindowTextA("");
		GetDlgItem(IDC_SATELLITEGRID)->SetWindowTextA("");
		GetDlgItem(IDC_SATELLITEGRIDPROPOSED)->SetWindowTextA("");
		GetDlgItem(IDC_OVERLAPPROPOSED)->SetWindowTextA("");
		GetDlgItem(IDC_APPLY_BTN)->EnableWindow(FALSE);		
		return;
	}
	else
	{
		CString data;
		GetDlgItem(IDC_IMAGESIZE_W)->GetWindowTextA(data);
		data = "x " + data;
		GetDlgItem(IDC_IMAGESIZE_H)->SetWindowTextA((LPCTSTR)data);
		GetDlgItem(IDC_APPLY_BTN)->EnableWindow(TRUE);		
		ComputeSatelliteValues();
	}
}

void CDlgSatelliteGridCalc::OnEnChangeSatImageEdit()
{
	CString data;
	GetDlgItem(IDC_IMAGESIZE_W)->GetWindowTextA(data);
	m_imageSizePixel = atoi(string(data).c_str());
	UpdateValues();
}

void CDlgSatelliteGridCalc::OnEnChangeSatImageSegSizePx()
{
	CString data;
	GetDlgItem(IDC_SATIMAGESEGSIZEPX)->GetWindowTextA(data);
	m_segmentSizePixel = atoi(string(data).c_str());
	UpdateValues();
}

void CDlgSatelliteGridCalc::OnEnChangeSatImageSegOverPx()
{
	CString data;
	GetDlgItem(IDC_SATIMAGESEGOVERPX)->GetWindowTextA(data);
	m_segmentOverlapPixel = atoi(string(data).c_str());
	UpdateValues();
}
