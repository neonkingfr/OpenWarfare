#include "stdafx.h"
#include "resource.h"
#include <float.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////
// Image export dlg

class CImageExportDlg : public CDialog
{
  // Construction
public:
  CImageExportDlg(CWnd * pParentWnd = NULL) : CDialog(IDD, pParentWnd), _enanleOnlyArea(false), _onlyArea(false) {};

  // Dialog Data
  //{{AFX_DATA(CImageExportDlg)
  enum { IDD = IDD_EXPORT_IMAGE};
  float _minTerrainHeight;
  float _maxTerrainHeight;
  float _minAreaHeight;
  float _maxAreaHeight;
  float _minImageHeight;
  float _maxImageHeight;  
  int  _onlyArea;
  bool _enanleOnlyArea;
  //}}AFX_DATA

  // Overrides
  // ClassWizard generate virtual function overrides
  //{{AFX_VIRTUAL(CImageExportDlg)
protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  //}}AFX_VIRTUAL

  // Implementation
protected: 
  DECLARE_MESSAGE_MAP()

public:
  afx_msg void OnBnClickedExportImageArea();
  virtual BOOL OnInitDialog();
};

BEGIN_MESSAGE_MAP(CImageExportDlg, CDialog ) 
  ON_BN_CLICKED(IDC_EXPORT_IMAGE_AREA, OnBnClickedExportImageArea)
END_MESSAGE_MAP( )



void CImageExportDlg::DoDataExchange(CDataExchange * pDX)
{
  DDX_Text(pDX, IDC_EXPORT_IMAGE_IMAGE_MIN, _minImageHeight);
  DDX_Text(pDX, IDC_EXPORT_IMAGE_IMAGE_MAX, _maxImageHeight);
  

  if (!_onlyArea)
  {  
    DDX_Text(pDX, IDC_EXPORT_IMAGE_TERRAIN_MIN, _minTerrainHeight);
    DDX_Text(pDX, IDC_EXPORT_IMAGE_TERRAIN_MAX, _maxTerrainHeight);
    MDDV_MinMaxFloat(pDX, _minImageHeight, -FLT_MAX, _minTerrainHeight, IDC_EXPORT_IMAGE_IMAGE_MIN);  
    MDDV_MinMaxFloat(pDX, _maxImageHeight, _maxTerrainHeight, FLT_MAX, IDC_EXPORT_IMAGE_IMAGE_MAX);
  }
  else
  {
    DDX_Text(pDX, IDC_EXPORT_IMAGE_TERRAIN_MIN, _minAreaHeight);
    DDX_Text(pDX, IDC_EXPORT_IMAGE_TERRAIN_MAX, _maxAreaHeight);
    MDDV_MinMaxFloat(pDX, _minImageHeight, -FLT_MAX, _minAreaHeight, IDC_EXPORT_IMAGE_IMAGE_MIN);  
    MDDV_MinMaxFloat(pDX, _maxImageHeight, _maxAreaHeight, FLT_MAX, IDC_EXPORT_IMAGE_IMAGE_MAX);
  }  

  DDX_Check(pDX, IDC_EXPORT_IMAGE_AREA, _onlyArea);
}

void CImageExportDlg::OnBnClickedExportImageArea()
{
  UpdateData(); 
  if (_onlyArea)
  {
    _minImageHeight = _minAreaHeight - (_minTerrainHeight - _minImageHeight) / (_maxTerrainHeight - _minTerrainHeight) * (_maxAreaHeight - _minAreaHeight);
    _maxImageHeight = _maxAreaHeight + (_maxImageHeight - _maxTerrainHeight) / (_maxTerrainHeight - _minTerrainHeight) * (_maxAreaHeight - _minAreaHeight);
  }
  else
  {
    _minImageHeight = _minTerrainHeight - (_minAreaHeight - _minImageHeight) * (_maxTerrainHeight - _minTerrainHeight) / (_maxAreaHeight - _minAreaHeight);
    _maxImageHeight = _maxTerrainHeight + (_maxImageHeight - _maxAreaHeight) * (_maxTerrainHeight - _minTerrainHeight) / (_maxAreaHeight - _minAreaHeight);
  }

  UpdateData(FALSE); 
}

BOOL CImageExportDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  GetDlgItem(IDC_EXPORT_IMAGE_AREA)->EnableWindow(_enanleOnlyArea);

  return TRUE;  
}

bool OnImageExport(const float *  minmaxTerrainHeight, float * minmaxImageHeight, CWnd * pParentWnd , 
                   const float * minmaxAreaHeight, bool * onlyArea )
{
  CImageExportDlg dlg(pParentWnd);

  dlg._minTerrainHeight = minmaxTerrainHeight[0];
  dlg._maxTerrainHeight = minmaxTerrainHeight[1];

  if (minmaxAreaHeight != NULL && onlyArea)
  {
    // area can be also saved...
    dlg._minAreaHeight = minmaxAreaHeight[0];
    dlg._maxAreaHeight = minmaxAreaHeight[1];
    dlg._enanleOnlyArea = true;
  }

  dlg._minImageHeight = minmaxImageHeight[0];
  dlg._maxImageHeight = minmaxImageHeight[1];

  if (IDOK != dlg.DoModal())
    return FALSE;

  minmaxImageHeight[0] = dlg._minImageHeight;
  minmaxImageHeight[1] = dlg._maxImageHeight;

  if (minmaxAreaHeight != NULL && onlyArea)
    *onlyArea = dlg._onlyArea != 0;    

  return TRUE;
}





