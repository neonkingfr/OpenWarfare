/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// definice lidsk�ch objekt� = stejn� jako p��rodn�ch

extern BOOL DoDefineTemplateObjects(CPosEdMainDoc*, BOOL bNatural);

BOOL DoDefineTemplatePplObjs(CPosEdMainDoc* pDoc)
{	
	return DoDefineTemplateObjects(pDoc, CPosObjectTemplate::objTpPeople); 
}

BOOL DoDefineTemplateNewRoadsObjs(CPosEdMainDoc* pDoc)
{	
	return DoDefineTemplateObjects(pDoc, CPosObjectTemplate::objTpNewRoads); 
}
