/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCfgViewPage dialog

class CCfgViewPage : public CMntPropertyPage
{
// Construction
public:
	CCfgViewPage();

// Dialog Data
	CPosEdCfg		m_Cfg;

	//{{AFX_DATA(CCfgViewPage)
	enum { IDD = IDD_CFG_VIEW_PAGE };
	CComboBox	m_cGridStyle;
	CComboBox	m_cCursType;
	CComboBox	m_cCursDraw;
	CFMntColorButton	m_nGridColor;
	CFMntColorButton	m_nCursColor;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CCfgViewPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CCfgViewPage)
	afx_msg void OnGridColor();
	afx_msg void OnCursColor();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

CCfgViewPage::CCfgViewPage() : CMntPropertyPage(CCfgViewPage::IDD)
{
	//{{AFX_DATA_INIT(CCfgViewPage)
	//}}AFX_DATA_INIT
}

void CCfgViewPage::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCfgViewPage)
	DDX_Control(pDX, IDC_COMBO_GRID_STYLE, m_cGridStyle);
	DDX_Control(pDX, IDC_CURSOR_TYPE, m_cCursType);
	DDX_Control(pDX, IDC_CURSOR_DRAW, m_cCursDraw);
	DDX_Control(pDX, IDC_GRID_COLOR, m_nGridColor);
	DDX_Control(pDX, IDC_CURSOR_COLOR, m_nCursColor);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, IDC_CHECK_GRID_XOR, m_Cfg.m_bGridXor);
	DDX_Check(pDX, IDC_SHOW_CURSOR,	m_Cfg.m_bCursorVisible);
	MDDX_Text(pDX, IDC_EDIT_MINPIX, m_Cfg.m_nMinTextSize);
	MDDV_MinMaxInt(pDX, m_Cfg.m_nMinTextSize, 1, 20, IDC_EDIT_MINPIX);
	if (SAVEDATA)
	{
		m_Cfg.m_nGridColor	= m_nGridColor.m_cColor;
		m_Cfg.m_nGridStyle	= m_cGridStyle.GetCurSel();
		m_Cfg.m_cCursorColor = m_nCursColor.m_cColor;
		m_Cfg.m_nCursorType = m_cCursType.GetCurSel();
		m_Cfg.m_nCursorDraw = m_cCursDraw.GetCurSel();
	} else
	{
		m_nGridColor.m_cColor = m_Cfg.m_nGridColor;
		m_cGridStyle.SetCurSel(m_Cfg.m_nGridStyle);
		m_nCursColor.m_cColor = m_Cfg.m_cCursorColor;
		m_cCursType.SetCurSel(m_Cfg.m_nCursorType);
		m_cCursDraw.SetCurSel(m_Cfg.m_nCursorDraw);
	};
	MDDX_Text(pDX, IDC_CURSOR_SZ, m_Cfg.m_nCursorDevWd);
	MDDV_MinMaxInt(pDX, m_Cfg.m_nCursorDevWd, 1, 5, IDC_CURSOR_SZ);
	MDDX_Text(pDX, IDC_CURSOR_WD, m_Cfg.m_nCursorDevSz);
	MDDV_MinMaxInt(pDX, m_Cfg.m_nCursorDevSz, MIN_CURSOR_SIZE, MAX_CURSOR_SIZE, IDC_CURSOR_WD);
}


BEGIN_MESSAGE_MAP(CCfgViewPage, CMntPropertyPage)
	//{{AFX_MSG_MAP(CCfgViewPage)
	ON_BN_CLICKED(IDC_GRID_COLOR, OnGridColor)
	ON_BN_CLICKED(IDC_CURSOR_COLOR, OnCursColor)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CCfgViewPage::OnGridColor() 
{
	m_nGridColor.DoEditColor();
}

void CCfgViewPage::OnCursColor() 
{
	m_nCursColor.DoEditColor();
}

/////////////////////////////////////////////////////////////////////////////
// CCfgColorPage dialog

class CCfgColorPage : public CMntPropertyPage
{
// Construction
public:
	CCfgColorPage();

// Dialog Data

	CPosEdCfg		m_Cfg;

	//{{AFX_DATA(CCfgColorPage)
	enum { IDD = IDD_CFG_DEFCOL_PAGE };
	CFMntColorButton	m_ChngDefColor;
	CFMntListBox	m_ListDefColor;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CCfgColorPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CCfgColorPage)
	afx_msg void OnChangeDefColor();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeListDefCol();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

CCfgColorPage::CCfgColorPage() : CMntPropertyPage(CCfgColorPage::IDD)
{
	m_ListDefColor.Init(MNTLST_COLOR);
	//{{AFX_DATA_INIT(CCfgColorPage)
	//}}AFX_DATA_INIT
}

BOOL CCfgColorPage::OnInitDialog() 
{
	CMntPropertyPage::OnInitDialog();
	// nastaven� barev
	for(int i=0; i<CPosEdCfg::defcolCount; i++)
	{
		CString sText;
		m_Cfg.GetDefColorName(sText,i);
    m_ListDefColor.AddStringEx(sText,m_Cfg.m_cDefColors[i]);
		//LBAddString(m_ListDefColor.m_hWnd,sText,m_Cfg.m_cDefColors[i]);
	}
	m_ListDefColor.SetCurSel(0);

	OnSelchangeListDefCol();
	return TRUE;
}

void CCfgColorPage::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCfgColorPage)
	DDX_Control(pDX, IDC_CHNG_DEFCOL, m_ChngDefColor);
	DDX_Control(pDX, IDC_LIST_DEFCOL, m_ListDefColor);
	//}}AFX_DATA_MAP
	DDX_CBIndex(pDX, IDC_COMBO_FILL0, m_Cfg.m_nFillWoods);
	DDX_CBIndex(pDX, IDC_COMBO_FILL1, m_Cfg.m_nFillNArea);
	DDX_CBIndex(pDX, IDC_COMBO_FILL2, m_Cfg.m_nFillKeyPt);
	DDX_CBIndex(pDX, IDC_COMBO_FILL3, m_Cfg.m_nFillSArea);
}


BEGIN_MESSAGE_MAP(CCfgColorPage, CMntPropertyPage)
	//{{AFX_MSG_MAP(CCfgColorPage)
	ON_BN_CLICKED(IDC_CHNG_DEFCOL, OnChangeDefColor)
	ON_LBN_SELCHANGE(IDC_LIST_DEFCOL, OnSelchangeListDefCol)
	ON_LBN_DBLCLK(IDC_LIST_DEFCOL, OnChangeDefColor)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CCfgColorPage::OnSelchangeListDefCol() 
{
	DWORD dwData;
	if (LBGetCurSelData(m_ListDefColor.m_hWnd,dwData))
	{
		m_ChngDefColor.m_cColor = dwData;
		m_ChngDefColor.Invalidate();
	}
}

void CCfgColorPage::OnChangeDefColor() 
{
	int nIndx = m_ListDefColor.GetCurSel();
	if (nIndx != LB_ERR)
	{
		if (m_ChngDefColor.DoEditColor())
		{
			m_ListDefColor.SetItemData(nIndx, m_ChngDefColor.m_cColor);			
			m_ListDefColor.Invalidate();
			m_Cfg.m_cDefColors[nIndx] = m_ChngDefColor.m_cColor;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCfgHeigthPage dialog

class CCfgHeigthPage : public CMntPropertyPage
{
// Construction
public:
	CCfgHeigthPage();

// Dialog Data

	CPosEdCfg		m_Cfg;

	//{{AFX_DATA(CCfgHeigthPage)
	enum { IDD = IDD_CFG_HGHCOL_PAGE };
	CFMntColorButton	m_cCountLns;
	CFMntColorButton	m_cMonoMax;
	CFMntColorButton	m_cMonoMin;
	CFMntColorButton	m_cMonoMinSea;
	CFMntColorButton	m_cMonoMaxSea;
	CFMntColorButton	m_ChngDefColor;
	CFMntListBox	m_ListDefColor;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CCfgHeigthPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CCfgHeigthPage)
	afx_msg void OnChangeDefColor();
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeListDefCol();
	afx_msg void OnMonoMinSea();
	afx_msg void OnMonoMaxSea();
	afx_msg void OnMonoMin();
	afx_msg void OnMonoMax();
	afx_msg void OnCountLines();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

CCfgHeigthPage::CCfgHeigthPage() : CMntPropertyPage(CCfgHeigthPage::IDD)
{
	m_ListDefColor.Init(MNTLST_COLOR);
	//{{AFX_DATA_INIT(CCfgHeigthPage)
	//}}AFX_DATA_INIT
}

BOOL CCfgHeigthPage::OnInitDialog() 
{
	// nastaven� barev
	m_cCountLns.m_cColor = m_Cfg.m_cCountLns;
	m_cMonoMax.m_cColor  = m_Cfg.m_cMaxMonoHg;
	m_cMonoMin.m_cColor  = m_Cfg.m_cMinMonoHg;
	m_cMonoMaxSea.m_cColor = m_Cfg.m_cMaxSeaHg;
	m_cMonoMinSea.m_cColor = m_Cfg.m_cMinSeaHg;

	CMntPropertyPage::OnInitDialog();
	// nastaven� barev
	for(int i=0; i<MAX_SCALE_COLOR_ITEMS; i++)
	{
		CString sText;
		m_Cfg.GetHeightColorName(sText,i);    
    m_ListDefColor.AddStringEx(sText,m_Cfg.m_cColorHg[i].m_cColor);
		//LBAddString(m_ListDefColor.m_hWnd,sText,m_Cfg.m_cColorHg[i].m_cColor);
	}
	m_ListDefColor.SetCurSel(0);
	OnSelchangeListDefCol();
	return TRUE;
}

void CCfgHeigthPage::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCfgHeigthPage)
	DDX_Control(pDX, IDC_COUNT_LINES, m_cCountLns);
	DDX_Control(pDX, IDC_MONO_MAX, m_cMonoMax);
	DDX_Control(pDX, IDC_MONO_MIN, m_cMonoMin);
	DDX_Control(pDX, IDC_MONO_SEA_MAX, m_cMonoMaxSea);
	DDX_Control(pDX, IDC_MONO_SEA_MIN, m_cMonoMinSea);
	DDX_Control(pDX, IDC_CHNG_DEFCOL, m_ChngDefColor);
	DDX_Control(pDX, IDC_LIST_DEFCOL, m_ListDefColor);
	//}}AFX_DATA_MAP
  DDX_Text(pDX,IDC_EDIT_FROM, m_Cfg.m_HgFrom);
  DDX_Text(pDX,IDC_EDIT_STEP, m_Cfg.m_HgStep);
  MDDV_MinMaxFloat(pDX, m_Cfg.m_HgStep, 0, 1e10, IDC_EDIT_STEP);
}


BEGIN_MESSAGE_MAP(CCfgHeigthPage, CMntPropertyPage)
	//{{AFX_MSG_MAP(CCfgHeigthPage)
	ON_BN_CLICKED(IDC_CHNG_DEFCOL, OnChangeDefColor)
	ON_LBN_SELCHANGE(IDC_LIST_DEFCOL, OnSelchangeListDefCol)
	ON_LBN_DBLCLK(IDC_LIST_DEFCOL, OnChangeDefColor)
	ON_BN_CLICKED(IDC_MONO_MIN, OnMonoMin)
	ON_BN_CLICKED(IDC_MONO_MAX, OnMonoMax)
	ON_BN_CLICKED(IDC_MONO_SEA_MIN, OnMonoMinSea)
	ON_BN_CLICKED(IDC_MONO_SEA_MAX, OnMonoMaxSea)
	ON_BN_CLICKED(IDC_COUNT_LINES, OnCountLines)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CCfgHeigthPage::OnSelchangeListDefCol() 
{
	DWORD dwData;
	if (LBGetCurSelData(m_ListDefColor.m_hWnd,dwData))
	{
		m_ChngDefColor.m_cColor = dwData;
		m_ChngDefColor.Invalidate();
	}
}

void CCfgHeigthPage::OnChangeDefColor() 
{
	int nIndx = m_ListDefColor.GetCurSel();
	if (nIndx != LB_ERR)
	{
		if (m_ChngDefColor.DoEditColor())
		{
			m_ListDefColor.SetItemData(nIndx, m_ChngDefColor.m_cColor);						
			m_ListDefColor.Invalidate();
			m_Cfg.m_cColorHg[nIndx].m_cColor = m_ChngDefColor.m_cColor;
		}
	}
}

void CCfgHeigthPage::OnMonoMinSea() 
{
	if (m_cMonoMinSea.DoEditColor())
	{
		m_Cfg.m_cMinSeaHg = m_cMonoMinSea.m_cColor;
	}
}
void CCfgHeigthPage::OnMonoMaxSea() 
{
	if (m_cMonoMaxSea.DoEditColor())
	{
		m_Cfg.m_cMaxSeaHg = m_cMonoMaxSea.m_cColor;
	}
}

void CCfgHeigthPage::OnMonoMin() 
{
	if (m_cMonoMin.DoEditColor())
	{
		m_Cfg.m_cMinMonoHg = m_cMonoMin.m_cColor;
	}
}

void CCfgHeigthPage::OnMonoMax() 
{
	if (m_cMonoMax.DoEditColor())
	{
		m_Cfg.m_cMaxMonoHg = m_cMonoMax.m_cColor;
	}
}

void CCfgHeigthPage::OnCountLines() 
{
	if (m_cCountLns.DoEditColor())
	{
		m_Cfg.m_cCountLns = m_cCountLns.m_cColor;
	}
}

/////////////////////////////////////////////////////////////////////////////
// DoEditConfiguration

BOOL DoEditConfiguration(CPosEdCfg* pCfg)
{
	// p��prava str�nek
	CCfgViewPage		ViewPage;
	ViewPage.m_Cfg.CopyFrom(pCfg);

	CCfgColorPage		ColorPage;
	ColorPage.m_Cfg.CopyFrom(pCfg);

	CCfgHeigthPage		HeightPage;
	HeightPage.m_Cfg.CopyFrom(pCfg);


	// p��prava dialogu
	CMntPropertySheet	dlg(IDS_SETUP_CONFIG,PSH_NOAPPLYNOW);

	dlg.AddPageIntoWizard(&ViewPage);		// zobrazen�
	dlg.AddPageIntoWizard(&HeightPage);	// v��ka
	dlg.AddPageIntoWizard(&ColorPage);	// barvy
	

	// realizace okna
	if (dlg.DoModal()!=IDOK) return FALSE;

	// realizace zm�n

	// ViewPage
	pCfg->m_nMinTextSize	= ViewPage.m_Cfg.m_nMinTextSize;
	pCfg->m_nGridStyle		= ViewPage.m_Cfg.m_nGridStyle;
	pCfg->m_nGridColor		= ViewPage.m_Cfg.m_nGridColor;
	pCfg->m_bGridXor		= ViewPage.m_Cfg.m_bGridXor;

	pCfg->m_bCursorVisible	= ViewPage.m_Cfg.m_bCursorVisible;
	pCfg->m_nCursorType		= ViewPage.m_Cfg.m_nCursorType;
	pCfg->m_nCursorDraw		= ViewPage.m_Cfg.m_nCursorDraw;
	pCfg->m_cCursorColor	= ViewPage.m_Cfg.m_cCursorColor;
	pCfg->m_nCursorDevWd	= ViewPage.m_Cfg.m_nCursorDevWd;
	pCfg->m_nCursorDevSz	= ViewPage.m_Cfg.m_nCursorDevSz;

	// barvy
	for(int i=0; i<CPosEdCfg::defcolCount; i++)
		pCfg->m_cDefColors[i] = ColorPage.m_Cfg.m_cDefColors[i];
	pCfg->m_nFillWoods = ColorPage.m_Cfg.m_nFillWoods;
	pCfg->m_nFillNArea = ColorPage.m_Cfg.m_nFillNArea; 
	pCfg->m_nFillKeyPt = ColorPage.m_Cfg.m_nFillKeyPt;
	pCfg->m_nFillSArea = ColorPage.m_Cfg.m_nFillSArea;

	// v��ka
	pCfg->m_cCountLns		= HeightPage.m_Cfg.m_cCountLns;
	pCfg->m_cMinSeaHg		= HeightPage.m_Cfg.m_cMinSeaHg;
	pCfg->m_cMaxSeaHg		= HeightPage.m_Cfg.m_cMaxSeaHg;
	pCfg->m_cMinMonoHg		= HeightPage.m_Cfg.m_cMinMonoHg;
	pCfg->m_cMaxMonoHg		= HeightPage.m_Cfg.m_cMaxMonoHg;
  pCfg->m_HgFrom		= HeightPage.m_Cfg.m_HgFrom;
  pCfg->m_HgStep		= HeightPage.m_Cfg.m_HgStep;

	for(int i=0; i<MAX_SCALE_COLOR_ITEMS; i++)
	{
		pCfg->m_cColorHg[i] = HeightPage.m_Cfg.m_cColorHg[i];
	};


	return TRUE;
};

BOOL DoEditCurrentConfig()
{
  CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
  BOOL bRes = DoEditConfiguration(&(pEnvir->m_cfgCurrent));
  if  (bRes)	// update glob�ln�ch dat prost�ed�
    pEnvir->m_cfgCurrent.SaveParams();

  return bRes; 
};


