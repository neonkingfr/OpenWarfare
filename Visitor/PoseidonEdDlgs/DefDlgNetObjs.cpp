/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef Fail
#undef Fail
#endif

/////////////////////////////////////////////////////////////////////////////
// EDIT NET PARTS

extern BOOL DoEditNetPartSTRA(CNetSTRA*);
extern BOOL DoEditNetPartBEND(CNetBEND*);
extern BOOL DoEditNetPartSPEC(CNetSPEC*);
extern BOOL DoEditNetPartTERM(CNetTERM*);


/////////////////////////////////////////////////////////////////////////////
// GET NET OBJECT TEMPLATE

extern BOOL GetObjectFileSelection(CString& sObjFile, CPosEdMainDoc* pDoc, BOOL bIn1Categ);

CPosObjectTemplate*	GetNetObjectTemplateFromFile(CPosEdBaseDoc* pDoc)
{
	CString sObjFile;
	if (GetObjectFileSelection(sObjFile, (CPosEdMainDoc*)pDoc, FALSE))
	{
		// dosud neexistuje
		CPosObjectTemplate* pTempl = NULL;
		TRY
		{
			pTempl = new CPosObjectTemplate(pDoc);
			// nastav�m objektu p��znak s�t�
			if (pTempl != NULL)
			{
				pTempl->m_bIsNetObject = TRUE;
				pTempl->CreateFromObjFile(sObjFile);
				// typ objektu
				pTempl->m_nObjType = CPosObjectTemplate::objTpNet;
			}
		}
		CATCH_ALL(e)
		{
			MntReportMemoryException();
			pTempl = NULL;
		}
		END_CATCH_ALL
		return pTempl;
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////
// CDefNetNamePage dialog

class CDefNetNamePage : public CMntPropertyPage
{
	// Construction
public:
	CDefNetNamePage();
	~CDefNetNamePage();

	// Dialog Data
	CPosNetTemplate*   m_pNet;
	CPosEdEnvironment* m_pEnvir;

	//{{AFX_DATA(CDefNetNamePage)
	enum { IDD = IDD_DEFNET_NAME_PAGE };

	CFMntColorButton m_KeyColor;
	CFMntColorButton m_NorColor;
	int              m_KeyType;
	int              m_NorType;
	//}}AFX_DATA


	// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CDefNetNamePage)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CDefNetNamePage)
	afx_msg void OnChangeKeyDef();
	afx_msg void OnChangeNorDef();
	afx_msg void OnEditKeyColor();
	afx_msg void OnEditNorColor();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CDefNetNamePage::CDefNetNamePage() 
: CMntPropertyPage(CDefNetNamePage::IDD)
{
	m_pEnvir = GetPosEdEnvironment();

	//{{AFX_DATA_INIT(CDefNetNamePage)
	//}}AFX_DATA_INIT
}

CDefNetNamePage::~CDefNetNamePage()
{
}

void CDefNetNamePage::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDefNetNamePage)
	DDX_Control(pDX, IDC_KEY_COLOR, m_KeyColor);
	DDX_Control(pDX, IDC_NOR_COLOR, m_NorColor);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, IDC_CHECK_FILL_NET, m_pNet->m_bFillEntire);
	//DDX_Check(pDX, IDC_AUTO_HEIGHT, m_pNet->m_bAutoHeight);

	DDX_Text(pDX, IDC_EDIT_NAME, m_pNet->m_sNetName);
	MDDV_MaxChars(pDX, m_pNet->m_sNetName, MAX_OBJC_NAME_LEN, IDC_EDIT_NAME);

//	MDDX_Text(pDX, IDC_EDIT_HGMIN, m_pNet->m_nMinHeight);
//	MDDV_MinMaxDouble(pDX, m_pNet->m_nMinHeight, 0., 100., IDC_EDIT_HGMIN);
//	MDDX_Text(pDX, IDC_EDIT_HGMAX, m_pNet->m_nMaxHeight);
//	MDDV_MinMaxDouble(pDX, m_pNet->m_nMaxHeight, 0., 100., IDC_EDIT_HGMAX);
//	MDDX_Text(pDX, IDC_EDIT_HGSTD, m_pNet->m_nStdHeight);
//	MDDV_MinMaxDouble(pDX, m_pNet->m_nStdHeight, 0., 100., IDC_EDIT_HGSTD);

	MDDX_Text(pDX, IDC_EDIT_SLOPE, m_pNet->m_nMaxSlope);
	MDDV_MinMaxDouble(pDX, m_pNet->m_nMaxSlope, 0., 90., IDC_EDIT_SLOPE, 0.5);
	MDDX_Text(pDX, IDC_EDIT_PARTSLO, m_pNet->m_nPartSlope);
	MDDV_MinMaxDouble(pDX, m_pNet->m_nPartSlope, 0., 90., IDC_EDIT_PARTSLO, 0.5);

	if (SAVEDATA)
	{
		MDDV_MinChars(pDX, m_pNet->m_sNetName, 1);
		// barvy
		DDX_Radio(pDX, IDC_KEY_STD, m_KeyType);
		DDX_Radio(pDX, IDC_NOR_STD, m_NorType);
		if (m_KeyType == 0)
		{
			m_pNet->m_clrKey = DEFAULT_COLOR;
		}
		else
		{
			m_pNet->m_clrKey = m_KeyColor.m_cColor;
		}
		if (m_NorType == 0)
		{
			m_pNet->m_clrNor = DEFAULT_COLOR;
		}
		else
		{
			m_pNet->m_clrNor = m_NorColor.m_cColor;
		}
	} 
	else
	{
		// barvy
		m_KeyType = (m_pNet->m_clrKey == DEFAULT_COLOR) ? 0 : 1;
		m_NorType = (m_pNet->m_clrNor == DEFAULT_COLOR) ? 0 : 1;
		DDX_Radio(pDX, IDC_KEY_STD, m_KeyType);
		DDX_Radio(pDX, IDC_NOR_STD, m_NorType);
		// barvy do tla��tek
		BOOL bFill;
		m_KeyColor.m_cColor = m_pNet->GetObjectKeyColor(bFill, &m_pEnvir->m_cfgCurrent);
		m_KeyColor.Invalidate();
		m_NorColor.m_cColor = m_pNet->GetObjectNorColor(bFill, &m_pEnvir->m_cfgCurrent);
		m_NorColor.Invalidate();
		OnChangeKeyDef();
		OnChangeNorDef();
	}
}

BEGIN_MESSAGE_MAP(CDefNetNamePage, CMntPropertyPage)
	//{{AFX_MSG_MAP(CDefNetNamePage)
	ON_BN_CLICKED(IDC_KEY_STD,  OnChangeKeyDef)
	ON_BN_CLICKED(IDC_NOR_STD,  OnChangeNorDef)
	ON_BN_CLICKED(IDC_KEY_USER, OnChangeKeyDef)
	ON_BN_CLICKED(IDC_NOR_USER, OnChangeNorDef)
	ON_BN_CLICKED(IDC_KEY_COLOR, OnEditKeyColor)
	ON_BN_CLICKED(IDC_NOR_COLOR, OnEditNorColor)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CDefNetNamePage::OnChangeKeyDef()
{	
	::ShowWindow(DLGCTRL(IDC_KEY_COLOR), (IsDlgButtonChecked(IDC_KEY_USER) != 0) ? SW_SHOW : SW_HIDE); 
}

void CDefNetNamePage::OnChangeNorDef()
{	
	::ShowWindow(DLGCTRL(IDC_NOR_COLOR), (IsDlgButtonChecked(IDC_NOR_USER) != 0) ? SW_SHOW : SW_HIDE); 
}

void CDefNetNamePage::OnEditKeyColor()
{	
	m_KeyColor.DoEditColor(); 
}

void CDefNetNamePage::OnEditNorColor()
{	
	m_NorColor.DoEditColor(); 
}

/////////////////////////////////////////////////////////////////////////////
// CDefNetStraPage dialog

class CDefNetStraPage : public CMntPropertyPage
{
	// Construction
public:
	CDefNetStraPage(AutoArray<int>& del);
	~CDefNetStraPage();

	// Dialog Data
	CPosNetTemplate* m_pNet;
	CPosEdMainDoc*   m_pDocument;
	AutoArray<int>&  m_DeleteObjectTemplates;

	//{{AFX_DATA(CDefNetStraPage)
	enum { IDD = IDD_DEFNET_STRA_PAGE };
	CListBox m_List;
	//}}AFX_DATA

	void UpdateList(DWORD nSelData = 0);

	// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CDefNetStraPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CDefNetStraPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnEdit();
	afx_msg void OnAdd();
	afx_msg void OnDel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedBrowse();
};

CDefNetStraPage::CDefNetStraPage(AutoArray<int>& del) 
: CMntPropertyPage(CDefNetStraPage::IDD)
, m_DeleteObjectTemplates(del)
{
	//{{AFX_DATA_INIT(CDefNetStraPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CDefNetStraPage::~CDefNetStraPage()
{
}

BOOL CDefNetStraPage::OnInitDialog() 
{
	CMntPropertyPage::OnInitDialog();
	// update seznamu
	UpdateList();
	return TRUE;
}

void CDefNetStraPage::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDefNetStraPage)
	DDX_Control(pDX, IDC_LIST, m_List);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDefNetStraPage, CMntPropertyPage)
	//{{AFX_MSG_MAP(CDefNetStraPage)
	ON_BN_CLICKED(IDC_EDIT, OnEdit)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_DEL, OnDel)
	ON_LBN_DBLCLK(IDC_LIST, OnEdit)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BROWSE, OnBnClickedBrowse)
END_MESSAGE_MAP()

void CDefNetStraPage::UpdateList(DWORD nSelData)
{
	m_List.ResetContent();
	for (int i = 0; i < m_pNet->m_NetSTRA.GetSize(); ++i)
	{
		CNetComponent* pObj = (CNetComponent*)(m_pNet->m_NetSTRA[i]);
		if (pObj)
		{
			LBAddString(m_List.m_hWnd, pObj->m_sCompName, (DWORD)pObj);
		}
	}
	// v�b�r objektu
	if (m_List.GetCount() > 0)
	{
		m_List.SetCurSel(0);
		if (nSelData != 0) LBSetCurSelData(m_List.m_hWnd, nSelData);
	}
}

void CDefNetStraPage::OnEdit() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_List.m_hWnd, dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	CNetSTRA* pObj = (CNetSTRA*)dwData;
	ASSERT(pObj != NULL);
	// editace
	if (!DoEditNetPartSTRA(pObj)) return;
	// update seznamu
	UpdateList((DWORD)pObj);
}

void CDefNetStraPage::OnAdd() 
{
	CNetSTRA* pObj = NULL;
	TRY
	{
		CPosObjectTemplate* pTempl = GetNetObjectTemplateFromFile(m_pDocument);
		if (pTempl)
		{	// je vybr�n objekt
			pObj = new CNetSTRA(m_pDocument);
			// nastav�m parametry podle �ablony
			if (!pObj->CreateFromObjectTemplate(pTempl))
			{
				AfxMessageBox(IDS_OBJECT_NOT_NET_OBJ, MB_OK | MB_ICONEXCLAMATION);
				delete pObj;
				return;
			}
			// eidtace
			if (!DoEditNetPartSTRA(pObj))
			{
				delete pObj;
				return;
			}
			// vlo�en� do seznamu
			m_pNet->m_NetSTRA.Add(pObj);
			// update seznamu
			UpdateList((DWORD)pObj);
		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pObj) delete pObj;
	}
	END_CATCH_ALL
}

void CDefNetStraPage::OnBnClickedBrowse()
{
	CString file;

	CString subDir;
	m_pDocument->GetProjectDirectory(subDir, CPosEdBaseDoc::dirObj);
	MakeShortDirStr(subDir, subDir);

	CString initDir = GetPosEdEnvironment()->m_optSystem.m_sNetPath;

	// initDir must be under subDir
	if (subDir.CompareNoCase(initDir.Left(subDir.GetLength())) != 0) initDir = subDir;

	CString filter(_T("Objects (*.p3d)|*.p3d||"));  
	if (!DoLoadFileFromSubDir(file, subDir, initDir, filter)) return;

	// set up new init dir
	SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sNetPath, file);

	int subdirL = subDir.GetLength() + 1;
	file = file.Right(file.GetLength() - subdirL);

	CPosObjectTemplate* pTempl = new CPosObjectTemplate(m_pDocument);
	// nastav�m objektu p��znak s�t�  
	pTempl->m_bIsNetObject = TRUE;
	pTempl->CreateFromObjFile(file);
	// typ objektu
	pTempl->m_nObjType = CPosObjectTemplate::objTpNet;

	CNetSTRA* pObj = new CNetSTRA(m_pDocument);
	// nastav�m parametry podle �ablony
	if (!pObj->CreateFromObjectTemplate(pTempl))
	{
		AfxMessageBox(IDS_OBJECT_NOT_NET_OBJ, MB_OK | MB_ICONEXCLAMATION);
		delete pTempl;
		delete pObj;
		return;
	}
	// editace
	if (!DoEditNetPartSTRA(pObj))
	{
		delete pObj;
		return;
	}
	// vlo�en� do seznamu
	m_pNet->m_NetSTRA.Add(pObj);
	// update seznamu
	UpdateList((DWORD)pObj);
}

void CDefNetStraPage::OnDel() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_List.m_hWnd, dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	CNetComponent* pObj = (CNetComponent*)dwData;
	ASSERT(pObj != NULL);
	if (pObj->IsUsed())
	{
		AfxMessageBox(IDS_OBJECT_DEL_USED);
		return;
	}
	// dotaz na smaz�n�
	CString sText;
	AfxFormatString1(sText, IDS_VERIFY_DEL_OBJ_TEMPL, pObj->m_sCompName);
	if (AfxMessageBox(sText, MB_YESNOCANCEL | MB_ICONQUESTION | MB_DEFBUTTON2) == IDYES)
	{	// odstran�n� objektu
		for (int i = 0; i < m_pNet->m_NetSTRA.GetSize(); ++i)
		{
			CNetComponent* pObj2 = (CNetComponent*)(m_pNet->m_NetSTRA[i]);
			if (pObj2 == pObj)
			{
				m_pNet->m_NetSTRA.RemoveAt(i);
				break;
			}
		}
		// smaz�n� objektu
		// smaz�n� objektu
		if (pObj->GetObjectTemplate()) m_DeleteObjectTemplates.Add(pObj->GetObjectTemplate()->m_ID);
		delete pObj;

		// update seznamu
		UpdateList();
	}
}

/////////////////////////////////////////////////////////////////////////////
// CDefNetBendPage dialog

class CDefNetBendPage : public CMntPropertyPage
{
// Construction
public:
	CDefNetBendPage(AutoArray<int>& del);
	~CDefNetBendPage();

// Dialog Data
	CPosNetTemplate*	m_pNet;
	CPosEdMainDoc*		m_pDocument;

  AutoArray<int>&  m_DeleteObjectTemplates;

	//{{AFX_DATA(CDefNetBendPage)
	enum { IDD = IDD_DEFNET_BEND_PAGE };
	CListBox	m_List;
	//}}AFX_DATA

	void UpdateList(DWORD nSelData = 0);

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CDefNetBendPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CDefNetBendPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnEdit();
	afx_msg void OnAdd();
	afx_msg void OnDel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
  afx_msg void OnBnClickedBrowse();
};

CDefNetBendPage::CDefNetBendPage(AutoArray<int>& del) 
: CMntPropertyPage(CDefNetBendPage::IDD)
, m_DeleteObjectTemplates(del)
{
	//{{AFX_DATA_INIT(CDefNetBendPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CDefNetBendPage::~CDefNetBendPage()
{
}

BOOL CDefNetBendPage::OnInitDialog() 
{
	CMntPropertyPage::OnInitDialog();
	// update seznamu
	UpdateList();
	return TRUE;
}

void CDefNetBendPage::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDefNetBendPage)
	DDX_Control(pDX, IDC_LIST, m_List);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDefNetBendPage, CMntPropertyPage)
	//{{AFX_MSG_MAP(CDefNetBendPage)
	ON_BN_CLICKED(IDC_EDIT, OnEdit)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_DEL, OnDel)
	ON_LBN_DBLCLK(IDC_LIST, OnEdit)
	//}}AFX_MSG_MAP
  ON_BN_CLICKED(IDC_BROWSE, OnBnClickedBrowse)
END_MESSAGE_MAP()

void CDefNetBendPage::UpdateList(DWORD nSelData)
{
	m_List.ResetContent();
	for (int i = 0; i < m_pNet->m_NetBEND.GetSize(); ++i)
	{
		CNetComponent* pObj = (CNetComponent*)(m_pNet->m_NetBEND[i]);
		if (pObj)
		{
			LBAddString(m_List.m_hWnd, pObj->m_sCompName, (DWORD)pObj);
		}
	}
	// v�b�r objektu
	if (m_List.GetCount() > 0)
	{
		m_List.SetCurSel(0);
		if (nSelData != 0) LBSetCurSelData(m_List.m_hWnd,nSelData);
	}
}

void CDefNetBendPage::OnEdit() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_List.m_hWnd,dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	CNetBEND* pObj = (CNetBEND*)dwData;
	ASSERT(pObj != NULL);
	// eidtace
	if (!DoEditNetPartBEND(pObj)) return;
	// update seznamu
	UpdateList((DWORD)pObj);
}

void CDefNetBendPage::OnAdd() 
{
	CNetBEND* pObj = NULL;
	TRY
	{
		CPosObjectTemplate* pTempl = GetNetObjectTemplateFromFile(m_pDocument);
		if (pTempl)
		{	// je vybr�n objekt

			pObj = new CNetBEND(m_pDocument);
			// nastav�m parametry podle �ablony
			if (!pObj->CreateFromObjectTemplate(pTempl))
			{
				AfxMessageBox(IDS_OBJECT_NOT_NET_OBJ,MB_OK|MB_ICONEXCLAMATION);
				delete pObj;
				return;
			}
			// eidtace
			if (!DoEditNetPartBEND(pObj))
			{
				delete pObj;
				return;
			}
			// vlo�en� do seznamu
			m_pNet->m_NetBEND.Add(pObj);
			// update seznamu
			UpdateList((DWORD)pObj);
		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pObj)
			delete pObj;
	}
	END_CATCH_ALL
}

void CDefNetBendPage::OnDel() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_List.m_hWnd, dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	};
	CNetComponent* pObj = (CNetComponent*)dwData;
	ASSERT(pObj != NULL);
	if (pObj->IsUsed())
	{
		AfxMessageBox(IDS_OBJECT_DEL_USED);
		return;
	}

	// dotaz na smaz�n�
	CString sText;
	AfxFormatString1(sText, IDS_VERIFY_DEL_OBJ_TEMPL, pObj->m_sCompName);
	if (AfxMessageBox(sText,MB_YESNOCANCEL | MB_ICONQUESTION | MB_DEFBUTTON2) == IDYES)
	{	
		// odstran�n� objektu
		for (int i = 0; i < m_pNet->m_NetBEND.GetSize(); ++i)
		{
			CNetComponent* pObj2 = (CNetComponent*)(m_pNet->m_NetBEND[i]);
			if (pObj2 == pObj)
			{
				m_pNet->m_NetBEND.RemoveAt(i);
				break;
			}
		}
		// smaz�n� objektu
		if (pObj->GetObjectTemplate()) m_DeleteObjectTemplates.Add(pObj->GetObjectTemplate()->m_ID);
		delete pObj;
		// update seznamu
		UpdateList();
	}
}

void CDefNetBendPage::OnBnClickedBrowse()
{
	CString file;

	CString subDir;
	m_pDocument->GetProjectDirectory(subDir, CPosEdBaseDoc::dirObj);
	MakeShortDirStr(subDir, subDir);

	CString initDir = GetPosEdEnvironment()->m_optSystem.m_sNetPath;

	// initDir must be under subDir
	if (subDir.CompareNoCase(initDir.Left(subDir.GetLength())) != 0) initDir = subDir;

	CString filter(_T("Objects (*.p3d)|*.p3d||"));  
	if (!DoLoadFileFromSubDir( file, subDir, initDir, filter)) return;

	// set up new init dir
	SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sNetPath, file);

	int subdirL = subDir.GetLength() + 1;
	file = file.Right(file.GetLength() - subdirL);

	CPosObjectTemplate* pTempl = new CPosObjectTemplate(m_pDocument);
	// nastav�m objektu p��znak s�t�  
	pTempl->m_bIsNetObject = TRUE;
	pTempl->CreateFromObjFile(file);
    // typ objektu
	pTempl->m_nObjType = CPosObjectTemplate::objTpNet;

	CNetBEND* pObj = new CNetBEND(m_pDocument);
	// nastav�m parametry podle �ablony
	if (!pObj->CreateFromObjectTemplate(pTempl))
	{
		AfxMessageBox(IDS_OBJECT_NOT_NET_OBJ, MB_OK | MB_ICONEXCLAMATION);
		delete pTempl;
		delete pObj;
		return;
	}
	// editace
	if (!DoEditNetPartBEND(pObj))
	{
		delete pObj;
		return;
	}
	// vlo�en� do seznamu
	m_pNet->m_NetBEND.Add(pObj);
	// update seznamu
	UpdateList((DWORD)pObj);
}

/////////////////////////////////////////////////////////////////////////////
// CDefNetSpecPage dialog

class CDefNetSpecPage : public CMntPropertyPage
{
// Construction
public:
	CDefNetSpecPage(AutoArray<int>& del);
	~CDefNetSpecPage();

// Dialog Data
	CPosNetTemplate*	m_pNet;
	CPosEdMainDoc*		m_pDocument;

  AutoArray<int>&  m_DeleteObjectTemplates;
	//{{AFX_DATA(CDefNetSpecPage)
	enum { IDD = IDD_DEFNET_SPEC_PAGE };
	CListBox	m_List;
	//}}AFX_DATA

	void UpdateList(DWORD nSelData = 0);

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CDefNetSpecPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CDefNetSpecPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnEdit();
	afx_msg void OnAdd();
	afx_msg void OnDel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
  afx_msg void OnBnClickedBrowse();
};

CDefNetSpecPage::CDefNetSpecPage(AutoArray<int>& del) : 
CMntPropertyPage(CDefNetSpecPage::IDD), m_DeleteObjectTemplates(del)
{
	//{{AFX_DATA_INIT(CDefNetSpecPage)
	//}}AFX_DATA_INIT
}

CDefNetSpecPage::~CDefNetSpecPage()
{
}

BOOL CDefNetSpecPage::OnInitDialog() 
{
	CMntPropertyPage::OnInitDialog();
	// update seznamu
	UpdateList();
	return TRUE;
}

void CDefNetSpecPage::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDefNetSpecPage)
	DDX_Control(pDX, IDC_LIST, m_List);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDefNetSpecPage, CMntPropertyPage)
	//{{AFX_MSG_MAP(CDefNetSpecPage)
	ON_BN_CLICKED(IDC_EDIT, OnEdit)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_DEL, OnDel)
	ON_LBN_DBLCLK(IDC_LIST, OnEdit)
	//}}AFX_MSG_MAP
  ON_BN_CLICKED(IDC_BROWSE, OnBnClickedBrowse)
END_MESSAGE_MAP()

void CDefNetSpecPage::UpdateList(DWORD nSelData)
{
	m_List.ResetContent();
	for (int i = 0; i < m_pNet->m_NetSPEC.GetSize(); ++i)
	{
		CNetComponent* pObj = (CNetComponent*)(m_pNet->m_NetSPEC[i]);
		if (pObj)
		{
			LBAddString(m_List.m_hWnd, pObj->m_sCompName, (DWORD)pObj);
		}
	}
	// v�b�r objektu
	if (m_List.GetCount()>0)
	{
		m_List.SetCurSel(0);
		if (nSelData != 0) LBSetCurSelData(m_List.m_hWnd, nSelData);
	}
}

void CDefNetSpecPage::OnEdit() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_List.m_hWnd, dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	CNetSPEC* pObj = (CNetSPEC*)dwData;
	ASSERT(pObj != NULL);
	// eidtace
	if (!DoEditNetPartSPEC(pObj)) return;
	// update seznamu
	UpdateList((DWORD)pObj);
}

void CDefNetSpecPage::OnAdd() 
{
	CNetSPEC* pObj = NULL;
	TRY
	{
		CPosObjectTemplate* pTempl = GetNetObjectTemplateFromFile(m_pDocument);
		if (pTempl)
		{	// je vybr�n objekt

			pObj = new CNetSPEC(m_pDocument);
			// nastav�m parametry podle �ablony
			if (!pObj->CreateFromObjectTemplate(pTempl))
			{
				AfxMessageBox(IDS_OBJECT_NOT_NET_OBJ, MB_OK | MB_ICONEXCLAMATION);
				delete pObj;
				return;
			}
			// eidtace
			if (!DoEditNetPartSPEC(pObj))
			{
				delete pObj;
				return;
			}
			// vlo�en� do seznamu
			m_pNet->m_NetSPEC.Add(pObj);
			// update seznamu
			UpdateList((DWORD)pObj);
		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pObj) delete pObj;
	}
	END_CATCH_ALL
}

void CDefNetSpecPage::OnBnClickedBrowse()
{
	CString file;

	CString subDir;
	m_pDocument->GetProjectDirectory(subDir, CPosEdBaseDoc::dirObj);
	MakeShortDirStr(subDir, subDir);

	CString initDir = GetPosEdEnvironment()->m_optSystem.m_sNetPath;

	// initDir must be under subDir
	if (subDir.CompareNoCase(initDir.Left(subDir.GetLength())) != 0) initDir = subDir;

	CString filter(_T("Objects (*.p3d)|*.p3d||"));  
	if (!DoLoadFileFromSubDir( file, subDir, initDir, filter)) return;

	// set up new init dir
	SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sNetPath, file);

	int subdirL = subDir.GetLength() + 1;
	file = file.Right(file.GetLength() - subdirL);

	CPosObjectTemplate* pTempl = new CPosObjectTemplate(m_pDocument);
	// nastav�m objektu p��znak s�t�  
	pTempl->m_bIsNetObject = TRUE;
	pTempl->CreateFromObjFile(file);
	// typ objektu
	pTempl->m_nObjType = CPosObjectTemplate::objTpNet;

	CNetSPEC* pObj = new CNetSPEC(m_pDocument);
	// nastav�m parametry podle �ablony
	if (!pObj->CreateFromObjectTemplate(pTempl))
	{
		AfxMessageBox(IDS_OBJECT_NOT_NET_OBJ, MB_OK | MB_ICONEXCLAMATION);
		delete pTempl;
		delete pObj;
		return;
	}
	// editace
	if (!DoEditNetPartSPEC(pObj))
	{
		delete pObj;
		return;
	}
	// vlo�en� do seznamu
	m_pNet->m_NetSPEC.Add(pObj);
	// update seznamu
	UpdateList((DWORD)pObj);
}

void CDefNetSpecPage::OnDel() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_List.m_hWnd, dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	CNetComponent* pObj = (CNetComponent*)dwData;
	ASSERT(pObj != NULL);
	if (pObj->IsUsed())
	{
		AfxMessageBox(IDS_OBJECT_DEL_USED);
		return;
	}
	// dotaz na smaz�n�
	CString sText;
	AfxFormatString1(sText, IDS_VERIFY_DEL_OBJ_TEMPL, pObj->m_sCompName);
	if (AfxMessageBox(sText, MB_YESNOCANCEL | MB_ICONQUESTION | MB_DEFBUTTON2) == IDYES)
	{	// odstran�n� objektu
		for (int i = 0; i < m_pNet->m_NetSPEC.GetSize(); ++i)
		{
			CNetComponent* pObj2 = (CNetComponent*)(m_pNet->m_NetSPEC[i]);
			if (pObj2 == pObj)
			{
				m_pNet->m_NetSPEC.RemoveAt(i);
				break;
			}
		}
		// smaz�n� objektu
		if (pObj->GetObjectTemplate()) m_DeleteObjectTemplates.Add(pObj->GetObjectTemplate()->m_ID);
		delete pObj;
		// update seznamu
		UpdateList();
	}
}

/////////////////////////////////////////////////////////////////////////////
// CDefNetTermPage dialog

class CDefNetTermPage : public CMntPropertyPage
{
	// Construction
public:
	CDefNetTermPage(AutoArray<int>& del);
	~CDefNetTermPage();

	// Dialog Data
	CPosNetTemplate*	m_pNet;
	CPosEdMainDoc*		m_pDocument;
	AutoArray<int>&  m_DeleteObjectTemplates;

	//{{AFX_DATA(CDefNetTermPage)
	enum { IDD = IDD_DEFNET_TERM_PAGE };
	CListBox	m_List;
	//}}AFX_DATA

	void UpdateList(DWORD nSelData = 0);

	// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CDefNetTermPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CDefNetTermPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnEdit();
	afx_msg void OnAdd();
	afx_msg void OnDel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
  afx_msg void OnBnClickedBrowse();
};

CDefNetTermPage::CDefNetTermPage(AutoArray<int>& del) 
: CMntPropertyPage(CDefNetTermPage::IDD)
, m_DeleteObjectTemplates(del)
{
	//{{AFX_DATA_INIT(CDefNetTermPage)
	//}}AFX_DATA_INIT
}

CDefNetTermPage::~CDefNetTermPage()
{
}

BOOL CDefNetTermPage::OnInitDialog() 
{
	CMntPropertyPage::OnInitDialog();
	// update seznamu
	UpdateList();
	return TRUE;
}

void CDefNetTermPage::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDefNetTermPage)
	DDX_Control(pDX, IDC_LIST, m_List);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDefNetTermPage, CMntPropertyPage)
	//{{AFX_MSG_MAP(CDefNetTermPage)
	ON_BN_CLICKED(IDC_EDIT, OnEdit)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_DEL, OnDel)
	ON_LBN_DBLCLK(IDC_LIST, OnEdit)
	//}}AFX_MSG_MAP
  ON_BN_CLICKED(IDC_BROWSE, OnBnClickedBrowse)
END_MESSAGE_MAP()

void CDefNetTermPage::UpdateList(DWORD nSelData)
{
	m_List.ResetContent();
	for (int i = 0; i < m_pNet->m_NetTERM.GetSize(); ++i)
	{
		CNetComponent* pObj = (CNetComponent*)(m_pNet->m_NetTERM[i]);
		if (pObj)
		{
			LBAddString(m_List.m_hWnd, pObj->m_sCompName, (DWORD)pObj);
		}
	}
	// v�b�r objektu
	if (m_List.GetCount()>0)
	{
		m_List.SetCurSel(0);
		if (nSelData != 0) LBSetCurSelData(m_List.m_hWnd, nSelData);
	}
}

void CDefNetTermPage::OnEdit() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_List.m_hWnd, dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	CNetTERM* pObj = (CNetTERM*)dwData;
	ASSERT(pObj != NULL);
	// eidtace
	if (!DoEditNetPartTERM(pObj)) return;
	// update seznamu
	UpdateList((DWORD)pObj);
}

void CDefNetTermPage::OnAdd() 
{
	CNetTERM* pObj = NULL;
	TRY
	{
		CPosObjectTemplate* pTempl = GetNetObjectTemplateFromFile(m_pDocument);
		if (pTempl)
		{	// je vybr�n objekt

			pObj = new CNetTERM(m_pDocument);
			// nastav�m parametry podle �ablony
			if (!pObj->CreateFromObjectTemplate(pTempl))
			{
				AfxMessageBox(IDS_OBJECT_NOT_NET_OBJ, MB_OK | MB_ICONEXCLAMATION);
				delete pObj;
				return;
			}
			// editace
			if (!DoEditNetPartTERM(pObj))
			{
				delete pObj;
				return;
			}
			// vlo�en� do seznamu
			m_pNet->m_NetTERM.Add(pObj);
			// update seznamu
			UpdateList((DWORD)pObj);
		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pObj) delete pObj;
	}
	END_CATCH_ALL
}

void CDefNetTermPage::OnDel() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_List.m_hWnd, dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	CNetComponent* pObj = (CNetComponent*)dwData;
	ASSERT(pObj != NULL);
	if (pObj->IsUsed())
	{
		AfxMessageBox(IDS_OBJECT_DEL_USED);
		return;
	}
	// dotaz na smaz�n�
	CString sText;
	AfxFormatString1(sText, IDS_VERIFY_DEL_OBJ_TEMPL, pObj->m_sCompName);
	if (AfxMessageBox(sText, MB_YESNOCANCEL | MB_ICONQUESTION | MB_DEFBUTTON2) == IDYES)
	{	// odstran�n� objektu
		for (int i = 0; i < m_pNet->m_NetTERM.GetSize(); ++i)
		{
			CNetComponent* pObj2 = (CNetComponent*)(m_pNet->m_NetTERM[i]);
			if (pObj2 == pObj)
			{
				m_pNet->m_NetTERM.RemoveAt(i);
				break;
			}
		}
		// smaz�n� objektu
		if (pObj->GetObjectTemplate()) m_DeleteObjectTemplates.Add(pObj->GetObjectTemplate()->m_ID);

		delete pObj;
		// update seznamu
		UpdateList();
	}
}

void CDefNetTermPage::OnBnClickedBrowse()
{
	CString file;

	CString subDir;
	m_pDocument->GetProjectDirectory(subDir, CPosEdBaseDoc::dirObj);
	MakeShortDirStr(subDir, subDir);

	CString initDir = GetPosEdEnvironment()->m_optSystem.m_sNetPath;

	// initDir must be under subDir
	if (subDir.CompareNoCase(initDir.Left(subDir.GetLength())) != 0) initDir = subDir;

	CString filter(_T("Objects (*.p3d)|*.p3d||"));  
	if (!DoLoadFileFromSubDir( file, subDir, initDir, filter)) return;

	// set up new init dir
	SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sNetPath, file);

	int subdirL = subDir.GetLength() + 1;
	file = file.Right(file.GetLength() - subdirL);

	CPosObjectTemplate* pTempl = new CPosObjectTemplate(m_pDocument);
	// nastav�m objektu p��znak s�t�  
	pTempl->m_bIsNetObject = TRUE;
	pTempl->CreateFromObjFile(file);
	// typ objektu
	pTempl->m_nObjType = CPosObjectTemplate::objTpNet;

	CNetTERM* pObj = new CNetTERM(m_pDocument);
	// nastav�m parametry podle �ablony
	if (!pObj->CreateFromObjectTemplate(pTempl))
	{
		AfxMessageBox(IDS_OBJECT_NOT_NET_OBJ, MB_OK | MB_ICONEXCLAMATION);
		delete pTempl;
		delete pObj;
		return;
	}
	// editace
	if (!DoEditNetPartTERM(pObj))
	{
		delete pObj;
		return;
	}
	// vlo�en� do seznamu
	m_pNet->m_NetTERM.Add(pObj);
	// update seznamu
	UpdateList((DWORD)pObj);
}

/////////////////////////////////////////////////////////////////////////////
// Editace - CPosNetTemplate

BOOL DoEditNetTemplate(CPosNetTemplate* pTempl, AutoArray<int>& del)
{
	// editovan� �ablona
	CPosNetTemplate EditTempl(pTempl->m_pOwner);
	EditTempl.CopyFrom(pTempl);
	EditTempl.CreateLocalObjectTemplates();

	// str�nky
	CDefNetNamePage NamePage;      NamePage.m_pNet = &EditTempl; 
	CDefNetStraPage StraPage(del); StraPage.m_pNet = &EditTempl;
	CDefNetBendPage BendPage(del); BendPage.m_pNet = &EditTempl;
	CDefNetSpecPage SpecPage(del); SpecPage.m_pNet = &EditTempl;
	CDefNetTermPage TermPage(del); TermPage.m_pNet = &EditTempl;

//	NamePage.m_pDocument =
	StraPage.m_pDocument =
	BendPage.m_pDocument =
	SpecPage.m_pDocument =
	TermPage.m_pDocument = (CPosEdMainDoc*)(pTempl->m_pOwner);

	// dialog
	CString sText; AfxFormatString1(sText, IDS_NET_DEF_NAME_TITLE, pTempl->m_sNetName);
	CMntPropertySheet dlg(sText, PSH_NOAPPLYNOW);
	dlg.AddPageIntoWizard(&NamePage);
	dlg.AddPageIntoWizard(&StraPage);
	dlg.AddPageIntoWizard(&BendPage);
	dlg.AddPageIntoWizard(&SpecPage);	
	dlg.AddPageIntoWizard(&TermPage);
	if (dlg.DoModal() != IDOK) return FALSE;

	// zm�ny dat 
	pTempl->CopyFrom(&EditTempl);
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CEditCrossDataDlg dialog

class CEditCrossDataDlg : public CDialog
{
// Construction
public:
	CEditCrossDataDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	CPosCrossTemplate*	m_pCross;
	CMgrNets*			m_pMgrNets;

	CPosEdEnvironment*	m_pEnvir;

	//{{AFX_DATA(CEditCrossDataDlg)
	enum { IDD = IDD_NETPART_EDIT_CROSS };
	CFMntColorButton	m_NorColor;
	int				m_NorType;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditCrossDataDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEditCrossDataDlg)
	afx_msg void OnChangeNorDef();
	afx_msg void OnEditNorColor();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnBnClickedFileBrowse();
};

CEditCrossDataDlg::CEditCrossDataDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEditCrossDataDlg::IDD, pParent)
{
	m_pEnvir	= GetPosEdEnvironment();
	//{{AFX_DATA_INIT(CEditCrossDataDlg)
	//}}AFX_DATA_INIT
}

static int nCtrlPartName[CPosCrossTemplate::netCROSS_Parts] =
{ IDC_SHOW_PART_A, IDC_SHOW_PART_B, IDC_SHOW_PART_C, IDC_SHOW_PART_D };
static int nCtrlPartNets[CPosCrossTemplate::netCROSS_Parts] =
{ IDC_EDIT_PART_A, IDC_EDIT_PART_B, IDC_EDIT_PART_C, IDC_EDIT_PART_D };

static int nCtrlCroosType[CPosCrossTemplate::netCROSS_Types] =
{ IDC_CROSS_IMG_I, IDC_CROSS_IMG_L, IDC_CROSS_IMG_P, IDC_CROSS_IMG_K };

BOOL CEditCrossDataDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// inicializuji napojen� na s�t�
	for(WORD nPart = 0; nPart < CPosCrossTemplate::netCROSS_Parts; nPart++)
	{
		BOOL bSupport = m_pCross->SupportCrossPart(nPart);
		// zobrazen� oken
		::ShowWindow(DLGCTRL(nCtrlPartName[nPart]),bSupport?SW_SHOW:SW_HIDE);
		::ShowWindow(DLGCTRL(nCtrlPartNets[nPart]),bSupport?SW_SHOW:SW_HIDE);
		// nepou�it� s�
		CString sText;
		sText.LoadString(IDS_UNDEF_NET);
		CBAddString(DLGCTRL(nCtrlPartNets[nPart]), sText, (DWORD)0);
		// nastaven� seznamu s�t�
		for(int nNet = 0; nNet < m_pMgrNets->m_NetTemplates.GetSize(); nNet++)
		{
			CPosNetTemplate* pNet = (CPosNetTemplate*)(m_pMgrNets->m_NetTemplates[nNet]);
			if (pNet != NULL)
			{
				CBAddString(DLGCTRL(nCtrlPartNets[nPart]), pNet->m_sNetName, (DWORD)pNet);
			}
		}
	};
	// update dat
	UpdateData(FALSE);
	return TRUE; 
}

void CEditCrossDataDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditCrossDataDlg)
	DDX_Control(pDX, IDC_NOR_COLOR, m_NorColor);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, IDC_CHECK_FILL_NET, m_pCross->m_bFillEntire);
	//DDX_Check(pDX, IDC_AUTO_HEIGHT, m_pCross->m_bAutoHeight);
	DDX_Text(pDX, IDC_EDIT_NAME, m_pCross->m_sCrossName);
	MDDV_MaxChars(pDX, m_pCross->m_sCrossName, MAX_OBJC_NAME_LEN, IDC_EDIT_NAME);
	MDDV_MinChars(pDX, m_pCross->m_sCrossName, 1);

//	MDDX_Text(pDX, IDC_EDIT_HGMIN, m_pCross->m_nMinHeight);
//	MDDV_MinMaxDouble(pDX, m_pCross->m_nMinHeight, 0., 100., IDC_EDIT_HGMIN);
//	MDDX_Text(pDX, IDC_EDIT_HGMAX, m_pCross->m_nMaxHeight);
//	MDDV_MinMaxDouble(pDX, m_pCross->m_nMaxHeight, 0., 100., IDC_EDIT_HGMAX);
//	MDDX_Text(pDX, IDC_EDIT_HGSTD, m_pCross->m_nStdHeight);
//	MDDV_MinMaxDouble(pDX, m_pCross->m_nStdHeight, 0., 100., IDC_EDIT_HGSTD);

	// zobrazen� objektu
	if (!SAVEDATA)
	{
		// objekt
		CString sText;
		if (m_pCross->GetObjectTemplate() != NULL)
			sText = m_pCross->GetObjectTemplate()->GetFileName();
		::SetWindowText(DLGCTRL(IDC_SHOW_OBJECT),sText);
		// typ
		m_pCross->GetTypeString(sText);
		::SetWindowText(DLGCTRL(IDC_SHOW_TYPE),sText);

		for(WORD i=0; i<CPosCrossTemplate::netCROSS_Types; i++)
		{
			::ShowWindow(DLGCTRL(nCtrlCroosType[i]),(i==m_pCross->m_nCrossType)?SW_SHOW:SW_HIDE);
		}
		// barvy
		m_NorType = (m_pCross->m_clrNor == DEFAULT_COLOR)?0:1;
		DDX_Radio(pDX, IDC_NOR_STD, m_NorType);
		// barvy do tla��tek
		BOOL bFill;
		m_NorColor.m_cColor = m_pCross->GetObjectNorColor(bFill,&m_pEnvir->m_cfgCurrent);
		m_NorColor.Invalidate();
		OnChangeNorDef();

		// �seky - s�t�
		for(WORD nPart = 0; nPart < CPosCrossTemplate::netCROSS_Parts; nPart++)
		{
			BOOL bSupport = m_pCross->SupportCrossPart(nPart);
			if (bSupport)
			{
				CPosNetTemplate* pNet = m_pCross->GetPartNetTemplate(nPart,m_pMgrNets);
				CBSetCurSelData(DLGCTRL(nCtrlPartNets[nPart]),(DWORD)0);
				CBSetCurSelData(DLGCTRL(nCtrlPartNets[nPart]),(DWORD)pNet);
			}
		};
	} else
	{
		// barvy
		DDX_Radio(pDX, IDC_NOR_STD, m_NorType);
		if (m_NorType == 0)
			m_pCross->m_clrNor = DEFAULT_COLOR;
		else
			m_pCross->m_clrNor = m_NorColor.m_cColor;
		
		// �seky - s�t�
		for(WORD nPart = 0; nPart < CPosCrossTemplate::netCROSS_Parts; nPart++)
		{
			BOOL bSupport = m_pCross->SupportCrossPart(nPart);
			if (bSupport)
			{
				DWORD dwData;
				if (!CBGetCurSelData(DLGCTRL(nCtrlPartNets[nPart]),dwData))
				{
					AfxMessageBox(IDS_NET_IS_NOT_DEFINED,MB_OK|MB_ICONEXCLAMATION);
					pDX->Fail();
				}
				CPosNetTemplate* pNet = (CPosNetTemplate*)dwData;
				if (pNet == NULL)
				{
					AfxMessageBox(IDS_NET_IS_NOT_DEFINED,MB_OK|MB_ICONEXCLAMATION);
					pDX->Fail();
				}
				m_pCross->m_sPartsNets[nPart] = pNet->m_sNetName; 
			} else
			{
				m_pCross->m_sPartsNets[nPart].Empty(); 
			};
		};
	};
}

void CEditCrossDataDlg::OnBnClickedFileBrowse()
{
  CString subDir;
  m_pCross->GetDocument()->GetProjectDirectory(subDir, CPosEdBaseDoc::dirObj);
  MakeShortDirStr(subDir,subDir);

  CString initDir;  
  initDir = GetPosEdEnvironment()->m_optSystem.m_sNetPath;  

  // initDir must be under subdir
  if(subDir.CompareNoCase(initDir.Left(subDir.GetLength())) != 0)  
    initDir = subDir;  

  CString file;
  CString filter(_T("Objects (*.p3d)|*.p3d||"));  
  if (!DoLoadFileFromSubDir(file, subDir, initDir, filter))
    return;

  SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sNetPath, file); 
  GetPosEdEnvironment()->m_optSystem.SaveParams();

  int subdirL = subDir.GetLength() + 1;
  if (m_pCross->GetObjectTemplate())
  {
    if (!m_pCross->GetObjectTemplate()->AreSameDataInSourceFile(file)) // do not allow to plug file with different data
    {
      AfxMessageBox(IDS_DIFFERENT_DATA);
      return;
    }
    m_pCross->GetObjectTemplate()->SetFileName(file.Right(file.GetLength() - subdirL));
    m_pCross->GetObjectTemplate()->UpdateParamsFromSourceFile();
    UpdateData(FALSE);
  }
}


BEGIN_MESSAGE_MAP(CEditCrossDataDlg, CDialog)
	//{{AFX_MSG_MAP(CEditCrossDataDlg)
	ON_BN_CLICKED(IDC_NOR_STD,  OnChangeNorDef)
	ON_BN_CLICKED(IDC_NOR_COLOR, OnEditNorColor)
	ON_BN_CLICKED(IDC_NOR_USER, OnChangeNorDef)
	//}}AFX_MSG_MAP
  ON_BN_CLICKED(IDC_FILE_BROWSE, OnBnClickedFileBrowse)
END_MESSAGE_MAP()

void CEditCrossDataDlg::OnChangeNorDef()
{	::ShowWindow(DLGCTRL(IDC_NOR_COLOR),(IsDlgButtonChecked(IDC_NOR_USER)!=0)?SW_SHOW:SW_HIDE); }
void CEditCrossDataDlg::OnEditNorColor()
{	m_NorColor.DoEditColor(); }

/////////////////////////////////////////////////////////////////////////////
// Editace - CPosCrossTemplate

BOOL DoEditCrossTemplate(CPosCrossTemplate* pTempl,CMgrNets* pMgrNets)
{
	// editovan� �ablona
	CPosCrossTemplate		EditTempl(pTempl->m_pOwner);
	EditTempl.CopyFrom(pTempl); 
	// dialog
	CEditCrossDataDlg dlg;
	dlg.m_pCross = &EditTempl;
	dlg.m_pMgrNets = pMgrNets;

	if (dlg.DoModal()!=IDOK) return FALSE;
	// kopie �ablony

	pTempl->CopyFrom(&EditTempl);
	
	return TRUE;
};

/////////////////////////////////////////////////////////////////////////////
// CNetDefBasePage dialog

class CNetDefBasePage : public CMntPropertyPage
{
	// Construction
public:
	CNetDefBasePage(AutoArray<int>& del);
	~CNetDefBasePage();

	// Dialog Data
	BOOL			m_bChanged;
	CMgrNets*		m_pMgrNets;
	CPosEdBaseDoc*  m_pDocument;

	AutoArray<int>& m_DeletedObjectTemplates; 

	//{{AFX_DATA(CNetDefBasePage)
	enum { IDD = IDD_DEF_NET_BASE_PAGE };
	CVariantListBox	m_List;
	//}}AFX_DATA

	//Overrides
	void DoUpdateList(CPosNetTemplate* pSel = NULL);

	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CNetDefBasePage)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CNetDefBasePage)
	afx_msg void OnAdd();
	afx_msg void OnDel();
	afx_msg void OnEdit();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CNetDefBasePage::CNetDefBasePage(AutoArray<int>& del) 
: CMntPropertyPage(CNetDefBasePage::IDD)
, m_DeletedObjectTemplates(del)
{
	m_bChanged = FALSE;
	m_pMgrNets = NULL;

	m_List.m_nVariant = CVariantListBox::itmVariantNets;
	//{{AFX_DATA_INIT(CNetDefBasePage)
	//}}AFX_DATA_INIT
}

CNetDefBasePage::~CNetDefBasePage()
{
}

BOOL CNetDefBasePage::OnInitDialog() 
{
	CMntPropertyPage::OnInitDialog();
	// update seznamu
	DoUpdateList();
	return TRUE;
}

void CNetDefBasePage::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNetDefBasePage)
	DDX_Control(pDX, IDC_LIST, m_List);
	//}}AFX_DATA_MAP
}

void CNetDefBasePage::DoUpdateList(CPosNetTemplate* pSel)
{
	m_List.ResetContent();
	for(int i = 0; i < m_pMgrNets->m_NetTemplates.GetSize(); ++i)
	{
		CPosNetTemplate* pNet = (CPosNetTemplate*)(m_pMgrNets->m_NetTemplates[i]);
		LBAddString(m_List.m_hWnd, pNet->m_sNetName, (DWORD)pNet);
	}
	if (m_List.GetCount() > 0)
	{
		m_List.SetCurSel(0);
		if (pSel != NULL)
		{
			LBSetCurSelData(m_List.m_hWnd, (DWORD)pSel);
		}
	}
}

BEGIN_MESSAGE_MAP(CNetDefBasePage, CMntPropertyPage)
	//{{AFX_MSG_MAP(CNetDefBasePage)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_DEL, OnDel)
	ON_BN_CLICKED(IDC_EDIT, OnEdit)
	ON_LBN_DBLCLK(IDC_LIST, OnEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CNetDefBasePage::OnAdd() 
{
	CPosNetTemplate* pNet = NULL;
	TRY
	{
		pNet = new CPosNetTemplate(m_pDocument);
		pNet->MakeDefaultValues();
		// editace
		if (!DoEditNetTemplate(pNet, m_DeletedObjectTemplates))
		{
			delete pNet;
			return;
		}
		// vlo�en� do seznamu
		m_pMgrNets->m_NetTemplates.Add(pNet);
		// update seznamu
		DoUpdateList(pNet);
		m_bChanged = TRUE;
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pNet) delete pNet;
	}
	END_CATCH_ALL
}

void CNetDefBasePage::OnDel() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_List.m_hWnd, dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	CPosNetTemplate* pNet = (CPosNetTemplate*)dwData;
	ASSERT(pNet != NULL);
	// dotaz na smaz�n�
	if (pNet->IsUsed())
	{
		AfxMessageBox(IDS_OBJECT_DEL_USED);
		return;
	}

	CString sText;
	AfxFormatString1(sText, IDS_VERIFY_DEL_OBJ_TEMPL, pNet->m_sNetName);
	if (AfxMessageBox(sText, MB_YESNOCANCEL | MB_ICONQUESTION | MB_DEFBUTTON2) == IDYES)
	{	// odstran�n� objektu
		for (int i = 0; i < m_pMgrNets->m_NetTemplates.GetSize(); ++i)
		{
			CPosNetTemplate* pNet2 = (CPosNetTemplate*)(m_pMgrNets->m_NetTemplates[i]);
			if (pNet2 == pNet)
			{
				m_pMgrNets->m_NetTemplates.RemoveAt(i);
				break;
			}
		}
		// smaz�n� objektu
		pNet->GetObjectTemplates(m_DeletedObjectTemplates);
		delete pNet;
		// update seznamu
		DoUpdateList();
		m_bChanged = TRUE;
	}
}

void CNetDefBasePage::OnEdit() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_List.m_hWnd, dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	CPosNetTemplate* pNet = (CPosNetTemplate*)dwData;
	ASSERT(pNet != NULL);
	// eidtace
	CString sOldName = pNet->m_sNetName;
	if (!DoEditNetTemplate(pNet, m_DeletedObjectTemplates)) return;
	// zm�na
	m_bChanged = TRUE;
	// update seznamu
	if (lstrcmp(sOldName, pNet->m_sNetName) != 0)
	{	// p�ejmenov�n� k��ovatek
		m_pMgrNets->RenameNetTemplate(sOldName, pNet->m_sNetName);
		DoUpdateList(pNet);
	}
	else
	{
		m_List.Invalidate();
	}
}

/////////////////////////////////////////////////////////////////////////////
// CNetDefCrossPage dialog

class CNetDefCrossPage : public CMntPropertyPage
{
	// Construction
public:
	CNetDefCrossPage(AutoArray<int>& del);
	~CNetDefCrossPage();

	// Dialog Data
	BOOL           m_bChanged;
	CMgrNets*      m_pMgrNets;
	CPosEdBaseDoc* m_pDocument;

	AutoArray<int>& m_DeletedObjectTemplates; 

	//{{AFX_DATA(CNetDefCrossPage)
	enum { IDD = IDD_DEF_NET_CROSS_PAGE };
	CVariantListBox	m_List;
	//}}AFX_DATA

	// Overrides
	void DoUpdateList(CPosCrossTemplate* pSel = NULL);

	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CNetDefCrossPage)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CNetDefCrossPage)
	afx_msg void OnAdd();
	afx_msg void OnDel();
	afx_msg void OnEdit();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedBrowse();
};

CNetDefCrossPage::CNetDefCrossPage(AutoArray<int>& del) 
: CMntPropertyPage(CNetDefCrossPage::IDD)
, m_DeletedObjectTemplates(del)
{
	m_bChanged = FALSE;
	m_pMgrNets = NULL;

	m_List.m_nVariant = CVariantListBox::itmVariantCros;
	//{{AFX_DATA_INIT(CNetDefCrossPage)
	//}}AFX_DATA_INIT
}

CNetDefCrossPage::~CNetDefCrossPage()
{
}

BOOL CNetDefCrossPage::OnInitDialog() 
{
	CMntPropertyPage::OnInitDialog();
	// update seznamu
	DoUpdateList();
	return TRUE;
}

void CNetDefCrossPage::DoDataExchange(CDataExchange* pDX)
{
	CMntPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNetDefCrossPage)
	DDX_Control(pDX, IDC_LIST, m_List);
	//}}AFX_DATA_MAP
}

void CNetDefCrossPage::DoUpdateList(CPosCrossTemplate* pSel)
{
	m_List.ResetContent();
	for (int i = 0; i < m_pMgrNets->m_CrossTemplates.GetSize(); ++i)
	{
		CPosCrossTemplate* pNet = (CPosCrossTemplate*)(m_pMgrNets->m_CrossTemplates[i]);
		LBAddString(m_List.m_hWnd, pNet->m_sCrossName, (DWORD)pNet);
	}
	if (m_List.GetCount() > 0)
	{
		m_List.SetCurSel(0);
		if (pSel != NULL)
		{
			LBSetCurSelData(m_List.m_hWnd, (DWORD)pSel);
		}
	}
}

BEGIN_MESSAGE_MAP(CNetDefCrossPage, CMntPropertyPage)
	//{{AFX_MSG_MAP(CNetDefCrossPage)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_DEL, OnDel)
	ON_BN_CLICKED(IDC_EDIT, OnEdit)
	ON_LBN_DBLCLK(IDC_LIST, OnEdit)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BROWSE, OnBnClickedBrowse)
END_MESSAGE_MAP()

void CNetDefCrossPage::OnAdd() 
{
	CPosCrossTemplate* pNet = NULL;
	TRY
	{
		CPosObjectTemplate* pTempl = GetNetObjectTemplateFromFile(m_pDocument);
		if (pTempl)
		{	// je vybr�n objekt
			pNet = new CPosCrossTemplate(m_pDocument);
			pNet->MakeDefaultValues();
			// nastav�m parametry podle �ablony
			if (!pNet->CreateFromObjectTemplate(pTempl))
			{
				AfxMessageBox(IDS_OBJECT_NOT_NET_OBJ, MB_OK | MB_ICONEXCLAMATION);
				delete pNet;
				return;
			}
			// eidtace
			if (!DoEditCrossTemplate(pNet, m_pMgrNets))
			{
				delete pNet;
				return;
			}
			// vlo�en� do seznamu
			m_pMgrNets->m_CrossTemplates.Add(pNet);
			// update seznamu
			DoUpdateList(pNet);
			m_bChanged = TRUE;
		}
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
		if (pNet) delete pNet;
	}
	END_CATCH_ALL
}

void CNetDefCrossPage::OnBnClickedBrowse()
{
	CString file;

	CString subDir;
	m_pDocument->GetProjectDirectory(subDir, CPosEdBaseDoc::dirObj);
	MakeShortDirStr(subDir, subDir);

	CString initDir = GetPosEdEnvironment()->m_optSystem.m_sNetPath;

	// initDir must be under subDir
	if (subDir.CompareNoCase(initDir.Left(subDir.GetLength())) != 0) initDir = subDir;

	CString filter(_T("Objects (*.p3d)|*.p3d||"));  
	if (!DoLoadFileFromSubDir( file, subDir, initDir, filter)) return;

	// set up new init dir
	SeparateDirFromFileName(GetPosEdEnvironment()->m_optSystem.m_sNetPath, file);

	int subdirL = subDir.GetLength() + 1;
	file = file.Right(file.GetLength() - subdirL);

	CPosObjectTemplate* pTempl = new CPosObjectTemplate(m_pDocument);
	// nastav�m objektu p��znak s�t�  
	pTempl->m_bIsNetObject = TRUE;
	pTempl->CreateFromObjFile(file);
	// typ objektu
	pTempl->m_nObjType = CPosObjectTemplate::objTpNet;
    
	CPosCrossTemplate* pNet = new CPosCrossTemplate(m_pDocument);
	pNet->MakeDefaultValues();
	// nastav�m parametry podle �ablony
	if (!pNet->CreateFromObjectTemplate(pTempl))
	{
		AfxMessageBox(IDS_OBJECT_NOT_NET_OBJ, MB_OK | MB_ICONEXCLAMATION);
		delete pTempl;
		delete pNet;
		return;
	}
   
	// editace
	if (!DoEditCrossTemplate(pNet, m_pMgrNets))
	{
		delete pNet;
		return;
	}
	// vlo�en� do seznamu
	m_pMgrNets->m_CrossTemplates.Add(pNet);
	// update seznamu
	DoUpdateList(pNet);
	m_bChanged = TRUE;
}

void CNetDefCrossPage::OnDel() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_List.m_hWnd, dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	CPosCrossTemplate* pNet = (CPosCrossTemplate*)dwData;
	ASSERT(pNet != NULL);

	if (pNet->GetObjectTemplate() && pNet->GetObjectTemplate()->m_nTimesUsed > 0)
	{
		// in use
		AfxMessageBox(IDS_OBJECT_DEL_USED);
		return;
	}

	// dotaz na smaz�n�
	CString sText;
	AfxFormatString1(sText, IDS_VERIFY_DEL_OBJ_TEMPL, pNet->m_sCrossName);
	if (AfxMessageBox(sText, MB_YESNOCANCEL | MB_ICONQUESTION | MB_DEFBUTTON2) == IDYES)
	{	// odstran�n� objektu
		for (int i = 0; i < m_pMgrNets->m_CrossTemplates.GetSize(); ++i)
		{
			CPosCrossTemplate* pNet2 = (CPosCrossTemplate*)(m_pMgrNets->m_CrossTemplates[i]);
			if (pNet2 == pNet)
			{
				m_pMgrNets->m_CrossTemplates.RemoveAt(i);
				break;
			}
		}
		// smaz�n� objektu
		if (pNet->GetObjectTemplate())
		m_DeletedObjectTemplates.Add(pNet->GetObjectTemplate()->m_ID);

		delete pNet;
		// update seznamu
		DoUpdateList();
		m_bChanged = TRUE;
	}
}

void CNetDefCrossPage::OnEdit() 
{
	DWORD dwData;
	if (!LBGetCurSelData(m_List.m_hWnd, dwData))
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	CPosCrossTemplate* pNet = (CPosCrossTemplate*)dwData;
	ASSERT(pNet != NULL);
	// eidtace
	CString sOldName = pNet->m_sCrossName;
	if (!DoEditCrossTemplate(pNet, m_pMgrNets)) return;
	// zm�na
	m_bChanged = TRUE;
	// update seznamu
	if (lstrcmp(sOldName, pNet->m_sCrossName) != 0)
	{
		DoUpdateList(pNet);
	}
	else
	{
		m_List.Invalidate();
	}
}

/////////////////////////////////////////////////////////////////////////////
// definice 

BOOL DoDefineTemplateNets(CPosEdMainDoc* pDoc)
{
	CMgrNets MgrNets;
	MgrNets.CopyFrom(&(pDoc->m_MgrNets));
	MgrNets.CreateLocalObjectTemplates();

	AutoArray<int> deletedObjectTemplates;
	// str�nky
	CNetDefBasePage  BasePage(deletedObjectTemplates);
	CNetDefCrossPage CrossPage(deletedObjectTemplates);
	BasePage.m_pDocument  = pDoc;
	CrossPage.m_pDocument = pDoc;
	BasePage.m_pMgrNets  = &MgrNets;
	CrossPage.m_pMgrNets = &MgrNets;

	// p��prava dialogu
	CMntPropertySheet dlg(IDS_NET_DEF_TITLE, PSH_NOAPPLYNOW);
	dlg.AddPageIntoWizard(&BasePage);		
	dlg.AddPageIntoWizard(&CrossPage);	
	// realizace okna
	if (dlg.DoModal() != IDOK) return FALSE;
	if ((BasePage.m_bChanged) || (CrossPage.m_bChanged))
	{
		// First delete object templates
		pDoc->DeleteObjectTemplates(deletedObjectTemplates);

		pDoc->m_MgrNets.CopyFrom(&MgrNets);
		pDoc->m_MgrNets.SyncLocalObjectTemplatesIntoDoc();
		return TRUE;
	}
//	AfxMessageBox("NEPOUZIVAT !", MB_OK);
	return FALSE;
}

