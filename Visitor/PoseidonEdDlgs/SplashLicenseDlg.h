#pragma once

#include "resource.h"
#include "afxcmn.h"
#include "afxwin.h"

// CSplashLicenseDlg dialog

class CSplashLicenseDlg : public CDialog
{
	DECLARE_DYNAMIC(CSplashLicenseDlg)

public:
	CSplashLicenseDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSplashLicenseDlg();

// Dialog Data
	enum { IDD = IDD_SPLASH_LICENSE };

public:
	static CSplashLicenseDlg* m_SplashLicenseDlg;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
private:
	CRichEditCtrl m_License_Text;

public:
	virtual BOOL OnInitDialog(void);

public:
	CButton m_Agreed;
	afx_msg void OnBnClickedCheckAgree();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
};
