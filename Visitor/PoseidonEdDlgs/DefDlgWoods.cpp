/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef Fail
#undef Fail
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditWoodTemplateDlg dialog

class CEditWoodTemplateDlg : public CDialog
{
// Construction
public:
	CEditWoodTemplateDlg(AutoArray<int>& deleted, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	CPosWoodTemplate	m_Template;
	CPosEdEnvironment*	m_pEnvir;

	CPosEdMainDoc*		m_pDocument;
  AutoArray<int>& m_DeletedObjectTemplates;

	//{{AFX_DATA(CEditWoodTemplateDlg)
	enum { IDD = IDD_DEF_WOODTMPL_DLG };
	CFMntColorButton	m_FrameColor;
	CFMntColorButton	m_ColorColor;
	int				m_ColorType;
	int				m_FrameType;
	//}}AFX_DATA

			void	UpdateTexts();
CPosObjectTemplate*	GetObjectTemplateFromFile();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditWoodTemplateDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL


// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CEditWoodTemplateDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeColorDef();
	afx_msg void OnChangeFrameDef();
	afx_msg void OnEditColorColor();
	afx_msg void OnEditFrameColor();
	afx_msg void OnSelectObjectRentir();
	afx_msg void OnSelectObjectRframe();
	afx_msg void OnSelectObjectTentir();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CEditWoodTemplateDlg::CEditWoodTemplateDlg(AutoArray<int>& deleted, CWnd* pParent /*=NULL*/)
	: CDialog(CEditWoodTemplateDlg::IDD, pParent), m_DeletedObjectTemplates(deleted)
{
	m_pEnvir	= GetPosEdEnvironment();
	//{{AFX_DATA_INIT(CEditWoodTemplateDlg)
	//}}AFX_DATA_INIT
}

void CEditWoodTemplateDlg::UpdateTexts()
{
	CString sText;
	sText = (m_Template.m_pWoodObjREntir!=NULL)?(m_Template.m_pWoodObjREntir->GetFileName()):_T("");
	::SetWindowText(DLGCTRL(IDC_SHOW_OBJECT_RENTIR),sText);
	sText = (m_Template.m_pWoodObjRFrame!=NULL)?(m_Template.m_pWoodObjRFrame->GetFileName()):_T("");
	::SetWindowText(DLGCTRL(IDC_SHOW_OBJECT_RFRAME),sText);
	sText = (m_Template.m_pWoodObjTEntir!=NULL)?(m_Template.m_pWoodObjTEntir->GetFileName()):_T("");
	::SetWindowText(DLGCTRL(IDC_SHOW_OBJECT_TENTIR),sText);
};

BOOL CEditWoodTemplateDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// update �ablon
	UpdateTexts();
	return TRUE; 
}

void CEditWoodTemplateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditWoodTemplateDlg)
	DDX_Control(pDX, IDC_FRAME_COLOR, m_FrameColor);
	DDX_Control(pDX, IDC_COLOR_COLOR, m_ColorColor);
	//}}AFX_DATA_MAP
	MDDX_Text(pDX, IDC_EDIT_NAME, m_Template.m_sWoodName);
	MDDV_MaxChars(pDX, m_Template.m_sWoodName, STD_STRING_LEN, IDC_EDIT_NAME);
	if (SAVEDATA)
	{
		MDDV_MinChars(pDX, m_Template.m_sWoodName, 1);
		// barvy
		DDX_Radio(pDX, IDC_COLOR_STD, m_ColorType);
		DDX_Radio(pDX, IDC_FRAME_STD, m_FrameType);
		if (m_ColorType == 0)
			m_Template.m_clrEntire = DEFAULT_COLOR;
		else
			m_Template.m_clrEntire = m_ColorColor.m_cColor;
		if (m_FrameType == 0)
			m_Template.m_clrFrame  = DEFAULT_COLOR;
		else
			m_Template.m_clrFrame  = m_FrameColor.m_cColor;

		// musej� b�t ur�eny z�kladn� objekty
		if ((m_Template.m_pWoodObjREntir == NULL)||
			(m_Template.m_pWoodObjRFrame == NULL)||
			(m_Template.m_pWoodObjTEntir == NULL))
		{
			AfxMessageBox(IDS_NO_CORRECT_WOOD,MB_OK|MB_ICONEXCLAMATION);
			pDX->Fail();
		};
	} else
	{
		// barvy
		m_ColorType = (m_Template.m_clrEntire == DEFAULT_COLOR)?0:1;
		m_FrameType = (m_Template.m_clrFrame  == DEFAULT_COLOR)?0:1;
		DDX_Radio(pDX, IDC_COLOR_STD, m_ColorType);
		DDX_Radio(pDX, IDC_FRAME_STD, m_FrameType);
		// barvy do tla��tek
		m_FrameColor.m_cColor = m_Template.GetObjectFrameColor(&m_pEnvir->m_cfgCurrent);
		m_FrameColor.Invalidate();
		m_ColorColor.m_cColor = m_Template.GetObjectEntireColor(&m_pEnvir->m_cfgCurrent);
		m_ColorColor.Invalidate();
		OnChangeColorDef();
		OnChangeFrameDef();
	};
}

BEGIN_MESSAGE_MAP(CEditWoodTemplateDlg, CDialog)
	//{{AFX_MSG_MAP(CEditWoodTemplateDlg)
	ON_BN_CLICKED(IDC_COLOR_STD, OnChangeColorDef)
	ON_BN_CLICKED(IDC_FRAME_STD, OnChangeFrameDef)
	ON_BN_CLICKED(IDC_COLOR_COLOR, OnEditColorColor)
	ON_BN_CLICKED(IDC_FRAME_COLOR, OnEditFrameColor)
	ON_BN_CLICKED(IDC_COLOR_USER, OnChangeColorDef)
	ON_BN_CLICKED(IDC_FRAME_USER, OnChangeFrameDef)
	ON_BN_CLICKED(IDC_SELECT_OBJECT_RENTIR, OnSelectObjectRentir)
	ON_BN_CLICKED(IDC_SELECT_OBJECT_RFRAME, OnSelectObjectRframe)
	ON_BN_CLICKED(IDC_SELECT_OBJECT_TENTIR, OnSelectObjectTentir)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CEditWoodTemplateDlg::OnChangeColorDef() 
{	::ShowWindow(DLGCTRL(IDC_COLOR_COLOR),(IsDlgButtonChecked(IDC_COLOR_USER)!=0)?SW_SHOW:SW_HIDE); }

void CEditWoodTemplateDlg::OnChangeFrameDef() 
{	::ShowWindow(DLGCTRL(IDC_FRAME_COLOR),(IsDlgButtonChecked(IDC_FRAME_USER)!=0)?SW_SHOW:SW_HIDE); }

void CEditWoodTemplateDlg::OnEditColorColor() 
{	m_ColorColor.DoEditColor(); }

void CEditWoodTemplateDlg::OnEditFrameColor() 
{	m_FrameColor.DoEditColor(); }

// v�b�r objekt�
extern BOOL GetObjectFileSelection(CString& sObjFile,CPosEdMainDoc* pDoc,BOOL bIn1Categ);
CPosObjectTemplate*	CEditWoodTemplateDlg::GetObjectTemplateFromFile()
{
	CString sObjFile;
	if (GetObjectFileSelection(sObjFile,m_pDocument,FALSE))
	{
		// dosud neexistuje
		CPosObjectTemplate* pTempl = NULL;
		TRY
		{
			pTempl = new CPosObjectTemplate(m_pDocument);
			pTempl->CreateFromObjFile(sObjFile);
			// typ objektu
			pTempl->m_nObjType = CPosObjectTemplate::objTpWood;
		}
		CATCH_ALL(e)
		{
			MntReportMemoryException();
			pTempl = NULL;
		}
		END_CATCH_ALL
		return pTempl;
	};
	return NULL;
};

void CEditWoodTemplateDlg::OnSelectObjectRentir() 
{
  if (m_Template.m_pWoodObjREntir.NotNull())
  {
    CString sObjFile;
    if (!GetObjectFileSelection(sObjFile,m_pDocument,FALSE))  
      return;

    m_Template.m_pWoodObjREntir->SetFileName(sObjFile);
    UpdateTexts();
  }
  else
  {
    CPosObjectTemplate* pTempl = GetObjectTemplateFromFile();
    if (pTempl)
      m_Template.m_pWoodObjREntir = pTempl;
    UpdateTexts();
  }	
}

void CEditWoodTemplateDlg::OnSelectObjectRframe() 
{
  if (m_Template.m_pWoodObjRFrame.NotNull())
  {
    CString sObjFile;
	  if (!GetObjectFileSelection(sObjFile,m_pDocument,FALSE))  
      return;

    m_Template.m_pWoodObjRFrame->SetFileName(sObjFile);
    UpdateTexts();
  }
  else
  {
    CPosObjectTemplate* pTempl = GetObjectTemplateFromFile();
    if (pTempl)
      m_Template.m_pWoodObjRFrame = pTempl;
    UpdateTexts();
  }
}

void CEditWoodTemplateDlg::OnSelectObjectTentir() 
{
  if (m_Template.m_pWoodObjTEntir.NotNull())
  {
    CString sObjFile;
    if (!GetObjectFileSelection(sObjFile,m_pDocument,FALSE))  
      return;

    m_Template.m_pWoodObjTEntir->SetFileName(sObjFile);
    UpdateTexts();
  }
  else
  {
    CPosObjectTemplate* pTempl = GetObjectTemplateFromFile();
    if (pTempl)
      m_Template.m_pWoodObjTEntir = pTempl;
    UpdateTexts();
  }
}

/////////////////////////////////////////////////////////////////////////////
// CDefWoodsDlg dialog

class CDefWoodsDlg : public CDialog
{
  // Construction
public:
  CDefWoodsDlg(CWnd* pParent = NULL);   // standard constructor

  // Dialog Data
  AutoArray<int> m_DeletedObjectTemplates; 
  CMgrWoods		m_Woods;
  CPosEdMainDoc* 	m_pDocument;

  //{{AFX_DATA(CDefWoodsDlg)
  enum { IDD = IDD_DEF_WOODS_DLG };
  CVariantListBox	m_List;
  //}}AFX_DATA

  void UpdateList(CPosWoodTemplate* pTempl = NULL);

  // Overrides
  // ClassWizard generated virtual function overrides
  //{{AFX_VIRTUAL(CDefWoodsDlg)
protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  //}}AFX_VIRTUAL

  // Implementation
protected:

  // Generated message map functions
  //{{AFX_MSG(CDefWoodsDlg)
  virtual BOOL OnInitDialog();
  afx_msg void OnEdit();
  afx_msg void OnAdd();
  afx_msg void OnDel();
  afx_msg void OnSelchangeList();
  //}}AFX_MSG
  DECLARE_MESSAGE_MAP()
};

CDefWoodsDlg::CDefWoodsDlg(CWnd* pParent)
	: CDialog(CDefWoodsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDefWoodsDlg)
	//}}AFX_DATA_INIT
	m_List.m_nVariant = CVariantListBox::itmVariantWood;
}


BOOL CDefWoodsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// napln�n� sezamu lesem
	UpdateList(0);
	return TRUE;
}

void CDefWoodsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDefWoodsDlg)
	DDX_Control(pDX, IDC_LIST, m_List);
	//}}AFX_DATA_MAP
}

void CDefWoodsDlg::UpdateList(CPosWoodTemplate* pSel)
{
	m_List.ResetContent();
	for(int i=0; i<m_Woods.m_WoodTemplates.GetSize(); i++)
	{
		CPosWoodTemplate* pTempl = (CPosWoodTemplate*)(m_Woods.m_WoodTemplates[i]);
		if (pTempl)
		{
			m_List.AddStringEx( pTempl->m_sWoodName, (DWORD)pTempl);
		}
	}
	if (m_List.GetCount() > 0)
	{
		m_List.SetCurSel(0);
		if (pSel != NULL)
			LBSetCurSelData(m_List.m_hWnd,(DWORD)pSel);
	};	
	OnSelchangeList();
};

BEGIN_MESSAGE_MAP(CDefWoodsDlg, CDialog)
	//{{AFX_MSG_MAP(CDefWoodsDlg)
	ON_BN_CLICKED(IDC_EDIT, OnEdit)
	ON_BN_CLICKED(IDC_ADD, OnAdd)
	ON_BN_CLICKED(IDC_DEL, OnDel)
	ON_LBN_DBLCLK(IDC_LIST, OnEdit)
	ON_LBN_SELCHANGE(IDC_LIST, OnSelchangeList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CDefWoodsDlg::OnSelchangeList() 
{
  ::EnableWindow(DLGCTRL(IDC_EDIT), (m_List.GetCount() > 0)&&(m_List.GetCurSel()!=LB_ERR));
  ::EnableWindow(DLGCTRL(IDC_DEL),  (m_List.GetCount() > 0)&&(m_List.GetCurSel()!=LB_ERR));
}

void CDefWoodsDlg::OnEdit() 
{
	int nIndx = m_List.GetCurSel();
	if (nIndx == LB_ERR)
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	// smazat ?
	CPosWoodTemplate* pTempl = (CPosWoodTemplate*)(m_List.GetItemData(nIndx));
	if (pTempl)
	{
		CEditWoodTemplateDlg dlg(m_DeletedObjectTemplates);
		dlg.m_Template.CopyFrom(pTempl);
		dlg.m_pDocument = m_pDocument;
		if (dlg.DoModal()!=IDOK)
			return;
		// kop�ruji data
		pTempl->CopyFrom(&dlg.m_Template);
		// update seznamu
		UpdateList(pTempl);
	};
}

void CDefWoodsDlg::OnAdd() 
{
	CEditWoodTemplateDlg dlg(m_DeletedObjectTemplates);
	dlg.m_Template.MakeDefaultValues();
  dlg.m_Template.m_pOwner = m_pDocument;
	dlg.m_pDocument = m_pDocument;
	if (dlg.DoModal()!=IDOK)
		return;
	// vlo��m novou �ablonu
	CPosWoodTemplate* pNewTempl = new CPosWoodTemplate(m_pDocument);
	if (pNewTempl)
	{	// kop�ruji data
		pNewTempl->CopyFrom(&dlg.m_Template);
		// vlo��m do �ablon
		m_Woods.m_WoodTemplates.Add(pNewTempl);
		// update seznamu
		UpdateList(pNewTempl);
	}
}

void CDefWoodsDlg::OnDel() 
{
	int nIndx = m_List.GetCurSel();
	if (nIndx == LB_ERR)
	{
		MessageBeep(MB_DEFAULT);
		return;
	}
	// smazat ?
	CPosWoodTemplate* pTempl = (CPosWoodTemplate*)(m_List.GetItemData(nIndx));
	if (pTempl)
	{    
    if ((pTempl->m_pWoodObjREntir.NotNull() && pTempl->m_pWoodObjREntir->m_nTimesUsed > 0)  ||
      (pTempl->m_pWoodObjRFrame.NotNull() && pTempl->m_pWoodObjRFrame->m_nTimesUsed > 0) ||
      (pTempl->m_pWoodObjTEntir.NotNull() && pTempl->m_pWoodObjTEntir->m_nTimesUsed > 0))
    {
      // object is used and cannot be deleted
      AfxMessageBox(IDS_OBJECT_DEL_USED, MB_OK);
      return;
    }

		CString sText;
		AfxFormatString1(sText,IDS_VERIFY_DEL_OBJ_TEMPL,pTempl->m_sWoodName);
		if (AfxMessageBox(sText,MB_YESNOCANCEL|MB_ICONQUESTION)==IDYES)
		{	// vyjmu ze seznamu a sma�u
			for(int i=0; i<m_Woods.m_WoodTemplates.GetSize(); i++)
			{
				CPosWoodTemplate* pComp = (CPosWoodTemplate*)(m_Woods.m_WoodTemplates[i]);
				if ((pComp)&&(pComp == pTempl))
				{
					m_Woods.m_WoodTemplates.RemoveAt(i);
          if (pTempl->m_pWoodObjREntir.NotNull())
            m_DeletedObjectTemplates.Add(pTempl->m_pWoodObjREntir->m_ID);
          if (pTempl->m_pWoodObjRFrame.NotNull())
            m_DeletedObjectTemplates.Add(pTempl->m_pWoodObjRFrame->m_ID);
          if (pTempl->m_pWoodObjTEntir.NotNull())
            m_DeletedObjectTemplates.Add(pTempl->m_pWoodObjTEntir->m_ID);
					delete pTempl;
					UpdateList(NULL);
				}
			}

		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// definice 

BOOL DoDefineTemplateWoods(CPosEdMainDoc* pDoc)
{
	CDefWoodsDlg dlg;
	dlg.m_Woods.CopyFrom(&(pDoc->m_MgrWoods));
  dlg.m_Woods.CreateLocalObjectTemplates();

	dlg.m_pDocument = pDoc;
	if (dlg.DoModal() != IDOK)
		return FALSE;
	// kop�ruji v�sledky
  // delete object templates
  pDoc->DeleteObjectTemplates(dlg.m_DeletedObjectTemplates);  
  // copy results.
	pDoc->m_MgrWoods.CopyFrom(&dlg.m_Woods);
  pDoc->m_MgrWoods.SyncLocalObjectTemplatesIntoDoc();

	return TRUE;
};



