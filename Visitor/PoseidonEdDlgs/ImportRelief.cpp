/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImportReliefDlg dialog

class CImportReliefDlg : public CDialog
{
// Construction
public:
	CImportReliefDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CImportReliefDlg)
	enum { IDD = IDD_IMPORT_RELIEF_DLG };
	CString	m_sFileName;
	float	m_nMaxHeight;
	float	m_nMinHeight;
	float	m_nZeroValue;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImportReliefDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CImportReliefDlg)
	afx_msg void OnSearch();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CImportReliefDlg::CImportReliefDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CImportReliefDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CImportReliefDlg)
	m_sFileName = _T("");
	//}}AFX_DATA_INIT
#ifdef _DEBUG
//	m_sFileName = _T("E:\\Projekty\\Poseidon\\Dokument\\Import\\krajina1.ase");
	m_sFileName = _T("H:\\A\\lef.ase");
#endif
	m_nMaxHeight =  400.f;
	m_nMinHeight = -50.f;
	m_nZeroValue =  6.5f;
}


void CImportReliefDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CImportReliefDlg)
	//}}AFX_DATA_MAP
	MDDX_Text(pDX, IDC_EDIT_FILE, m_sFileName);
	MDDV_MaxChars(pDX, m_sFileName, _MAX_PATH, IDC_EDIT_FILE);
	if (SAVEDATA)
		MDDV_FileExist(pDX, m_sFileName );
	MDDX_Text(pDX, IDC_EDIT_MAX_HEIGHT, m_nMaxHeight);
	MDDV_MinMaxFloat(pDX, m_nMaxHeight, MIN_LAND_HEIGHT, MAX_LAND_HEIGHT, IDC_EDIT_MAX_HEIGHT, 10.f);
	MDDX_Text(pDX, IDC_EDIT_MIN_HEIGHT, m_nMinHeight);
	MDDV_MinMaxFloat(pDX, m_nMinHeight, MIN_LAND_HEIGHT, MAX_LAND_HEIGHT, IDC_EDIT_MIN_HEIGHT, -10.f);
	MDDX_Text(pDX, IDC_EDIT_ZERO_VAL, m_nZeroValue);
	MDDV_MinMaxFloat(pDX, m_nZeroValue, -100.f, 100.f, IDC_EDIT_ZERO_VAL, 0.5f);
}


BEGIN_MESSAGE_MAP(CImportReliefDlg, CDialog)
	//{{AFX_MSG_MAP(CImportReliefDlg)
	ON_BN_CLICKED(IDC_SEARCH, OnSearch)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CImportReliefDlg::OnSearch() 
{
	CString sFilter,sExt,sDir,sTitle,sFile;
	VERIFY(sFilter.LoadString(IDS_IMPORT_RELIEF_FILE));
	VERIFY(sTitle.LoadString(IDS_IMPORT_RELIEF_SEL));
	sDir = GetPosEdEnvironment()->m_optSystem.m_sObjWorldPath;
	MakeShortDirStr(sDir,sDir);
	// p��prava CFileDialog
	CFileDialog dlg(TRUE);
	dlg.m_ofn.Flags |= OFN_FILEMUSTEXIST;
	sFilter += (TCHAR)(_T('\0'));
	sFilter += _T("*.ase");
	sFilter += (TCHAR)(_T('\0'));
	sFilter += (TCHAR)(_T('\0'));
	dlg.m_ofn.nMaxCustFilter = 1;
	dlg.m_ofn.lpstrFilter = sFilter;
	dlg.m_ofn.lpstrDefExt = _T("ase");
	dlg.m_ofn.lpstrTitle  = sTitle;
	dlg.m_ofn.lpstrFile   = sFile.GetBuffer(_MAX_PATH);
	dlg.m_ofn.lpstrInitialDir = sDir;
	BOOL bRes = (dlg.DoModal()==IDOK)?TRUE:FALSE;
	sFile.ReleaseBuffer();


	if (bRes)
	{
    SeparateDirFromFileName(sDir, (LPCTSTR) sFile);
    GetPosEdEnvironment()->m_optSystem.m_sObjWorldPath = sFile;
    GetPosEdEnvironment()->m_optSystem.SaveParams();

		::SetWindowText(DLGCTRL(IDC_EDIT_FILE),sFile);
	}
}

/////////////////////////////////////////////////////////////////////////////
// Na�ten� importovan�ho souboru

#define ANST_FND0	1
#define ANST_FND1	2
#define ANST_FND2	3
#define ANST_FND3	4

BOOL DoAnalyzeImportLine(LPTSTR pLine,float& nF1,float& nF2,float& nF3)
{
	int nLen = lstrlen(pLine);
	int nPos = 0;
	while(nPos < nLen)
	{
		if (pLine[nPos]=='*')
		{	// adept na spr�vn� ��dek
			static char* sCompare = "MESH_VERTEX";
			if (pLine[nPos+1+lstrlen(sCompare)]==' ')
				pLine[nPos+1+lstrlen(sCompare)] = 0;
			if (lstrcmp(pLine+(nPos+1),sCompare)==0)
			{	// je to spr�vn� ��dek
				LPTSTR pLineN = pLine+(2+nPos+lstrlen(sCompare));
				int	   nPosN  = 0;
				// anal�za ��sel
				int nState = ANST_FND0;
				while(pLineN[nPosN]!=0)
				{
					// hled�m za��tek ��sla
					if (isdigit(pLineN[nPosN])||(pLineN[nPosN]=='-'))
					{	// konvertuji na ��slo
						char  *pEnd;
						double nNum = strtod(pLineN,&pEnd);
						if (pLineN == pEnd)
							return FALSE;
						pLineN = pEnd;
						nPosN  = 0;
						switch(nState)
						{
						case ANST_FND0:
							{	// nic neukl�d�m
							}; break;
						case ANST_FND1:
							{
								nF1 = (float)nNum;
							}; break;
						case ANST_FND2:
							{
								nF2 = (float)nNum;
							}; break;
						case ANST_FND3:
							{
								nF3 = (float)nNum;
							}; return TRUE;
						default:
							return FALSE;
						};
						nState++;
					}
					nPosN++;
				}
			}
		}
		nPos++;
	}
	return FALSE;
};

BOOL DoLoadImportData(LPCTSTR pFileName,int& nCount,float* nMin,float* nMax,
					  CFltArray& nX,CFltArray& nZ,CFltArray& nY)
{
	// load object
	CFile file;
	CFileException fe;
	if (!file.Open(pFileName, CFile::modeRead | CFile::shareDenyWrite, &fe))
	{
		AfxMessageBox(IDS_FAILED_TO_READ_IMPORT_FILE,MB_OK|MB_ICONSTOP);
		return FALSE;
	}
	CArchive loadArchive(&file, CArchive::load | CArchive::bNoFlushOnDelete);
	AfxGetApp()->BeginWaitCursor();
	// uchov�m p�vodn� soubor a nastv�m aktu�ln�
	TRY
	{
		// serializace dat
		int		nLineIndx = 0;
		char	sLineBuff[1000];

		while(loadArchive.Read(&(sLineBuff[nLineIndx]),sizeof(char))==sizeof(char))
		{
			if (sLineBuff[nLineIndx] == '\a')
				continue;
			if (sLineBuff[nLineIndx] == '\n')
			{	// konec ��dku
				sLineBuff[nLineIndx] = 0;	

				float nF1,nF2,nF3;
				if (DoAnalyzeImportLine(sLineBuff,nF1,nF2,nF3))
				{
					if (nCount == 0)
					{
						nMin[0] = nMax[0] = nF1;
						nMin[1] = nMax[1] = nF2;
						nMin[2] = nMax[2] = nF3;
					} else
					{
						if (nMin[0] > nF1)  nMin[0] = nF1;
						if (nMax[0] < nF1)  nMax[0] = nF1;
						if (nMin[1] > nF2)  nMin[1] = nF2;
						if (nMax[1] < nF2)  nMax[1] = nF2;
						if (nMin[2] > nF3)  nMin[2] = nF3;
						if (nMax[2] < nF3)  nMax[2] = nF3;
					};
					// vlo��m do seznamu
					nX.Add(nF1);
					nZ.Add(nF2);
					nY.Add(nF3);
					nCount++;
				}
				// nov� ��dek
				nLineIndx = 0;
			} else
			{	// dal�� znak
				nLineIndx++;
			};
		}

		loadArchive.Close();
		file.Close();
	}
	CATCH_ALL(e)
	{
		file.Abort(); 
		AfxGetApp()->EndWaitCursor();
		AfxMessageBox(IDS_FAILED_TO_READ_IMPORT_FILE,MB_OK|MB_ICONSTOP);
		return FALSE;
	}
	END_CATCH_ALL
	AfxGetApp()->EndWaitCursor();
	return TRUE;
};

/////////////////////////////////////////////////////////////////////////////
// Absolutizace v��ky -> p�evod na absolutn� hodnotu

BOOL MakeAbsoluteHeight(float nMin,float nMax,CFltArray& nY,
						float nMaxHeight,float nMinHeight,float nZeroValue)
{
	float nDif = nMax-nMin;
	float nZer = (nZeroValue)/100.f;
	if (nDif == 0.f)
	{
		AfxMessageBox(IDS_NO_HEIGHT_DATA,MB_OK|MB_ICONEXCLAMATION);
		return FALSE;
	}
	// koeficient pro kladne (P) a z�porn� hodnot y(N)
	float nKoefP = nMaxHeight/(1.f-nZer);
	float nKoefN = (-nMinHeight)/(nZer);

	// uprav�m hodnoty
	for(int i=0; i<nY.GetSize(); i++)
	{
		float nVal = nY[i];
		// p�evod na <0,1>
		nVal  = (nVal-nMin)/nDif;
		// posun na 0 hladinu
		nVal -= nZer;
		// spr�vn� v��ka
		if (nVal < 0.f)
		{
			nVal *= nKoefN;
		} else
		{
			nVal *= nKoefP;
		};

		// upraven� honota
		nY[i] = nVal;
	}
	return TRUE;
};

/////////////////////////////////////////////////////////////////////////////
// Absolutizace sou�adnic -> p�evod na re�ln� sou�adnice buldozeru

BOOL MakeAbsoluteRealPos(float nMin,float nMax,CFltArray& nX,
						 float nWidth)
{
	float nDif = nMax-nMin;
	if (nDif == 0.f)
	{
		AfxMessageBox(IDS_NO_IMPPOS_DATA,MB_OK|MB_ICONEXCLAMATION);
		return FALSE;
	}
	// uprav�m hodnoty
	for(int i=0; i<nX.GetSize(); i++)
	{
		float nVal = nX[i];
		// posun na <0,1>
		nVal -= nMin;
		nVal  = nVal/nDif;
		if (nVal < 0.f)
			nVal = 0.f;
		if (nVal > 1.f)
			nVal = 1.f;
		// absolutn� sou�adnice
		nVal *= nWidth;
		// upraven� honota
		nX[i] = nVal;
	}
	return TRUE;
};

/////////////////////////////////////////////////////////////////////////////
// vlo�en� dat do mapy

BOOL DoSetImportData(CPoseidonMap* pMap,LANDHEIGHT nIniHeight,CFltArray&	nX,CFltArray& nZ,CFltArray& nY)
{
	ASSERT(nX.GetSize() == nZ.GetSize());
	ASSERT(nX.GetSize() == nY.GetSize());
	// celou mapu inicializuji na nejhlub�� mo�e
	pMap->SetGlobalHeight(nIniHeight);
	// nastaven� na�ten�ch v��ek
	for(int i=0; i<nX.GetSize(); i++)
	{
		REALPOS rPos;
		UNITPOS uPos;
		rPos.x = nX[i];
		rPos.z = nZ[i];
		// p�evod na jednotkov� sou�adnice
		if (pMap->IsAtWorld(rPos))
		{
			uPos = pMap->FromRealToUnitPos(rPos);
			// vlo�en� v��ky
			pMap->SetLandscapeHeight(uPos,nY[i],false);
		}
	}
	return TRUE;
};

/////////////////////////////////////////////////////////////////////////////
// Import reli�fu

BOOL DoImportRelief(CPosEdMainDoc* pDoc)
{
	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();

	CImportReliefDlg dlg;
	if (dlg.DoModal()!=IDOK) 
		return FALSE;	// zru�eno u�ivatelem

	// importovan� data
	int			nCount = 0;	// po�et importovan�ch ��dk�
	float		nMin[3],nMax[3];
	CFltArray	nX,nZ,nY;
	// na�ten� importovan�ch dat
	if (!DoLoadImportData(dlg.m_sFileName,nCount,nMin,nMax,nX,nZ,nY))
		return FALSE;	// nepoda�ilo se na��st data
	// absolutn� v��ka
	if (!MakeAbsoluteHeight(nMin[2],nMax[2],nY,dlg.m_nMaxHeight,dlg.m_nMinHeight,dlg.m_nZeroValue))
		return FALSE;	// nepoda�ilo se 
	// absolutn� sou�adnice
	float nRealWidth;

	nRealWidth = (float)(pDoc->m_PoseidonMap.GetSize().x+1);
	nRealWidth*= (float)(pDoc->m_nRealSizeUnit);
	if (!MakeAbsoluteRealPos(nMin[0],nMax[0],nX,nRealWidth))
		return FALSE;	// nepoda�ilo se 
	nRealWidth = (float)(pDoc->m_PoseidonMap.GetSize().z+1);
	nRealWidth*= (float)(pDoc->m_nRealSizeUnit);
	if (!MakeAbsoluteRealPos(nMin[1],nMax[1],nZ,nRealWidth))
		return FALSE;	// nepoda�ilo se 

	// vlo�en� dat do mapy
	if (!DoSetImportData(&(pDoc->m_PoseidonMap),dlg.m_nMinHeight,nX,nZ,nY))
		return FALSE;	// nepoda�ilo se 

	// poslani vysky do buldozeru
	if (pDoc->IsRealDocument() && pDoc->RealViewer())
	{
		for(int x=0; x<pDoc->m_PoseidonMap.GetSize().x; x++)
			for(int z=0; z<pDoc->m_PoseidonMap.GetSize().z; z++)
			{
                    UNITPOS Pos = {x,z};
					pDoc->RealViewer()->LandHeightChange(STexturePosMessData(x,z,pDoc->m_PoseidonMap.GetLandscapeHeight(Pos),0));
			}
	}

	return TRUE;
};



