/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"
#include "EditProjectParamsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditProjectParamsDlg dialog


CEditProjectParamsDlg::CEditProjectParamsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEditProjectParamsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditProjectParamsDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CEditProjectParamsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditProjectParamsDlg)
	//}}AFX_DATA_MAP
	MDDX_Text(pDX, IDC_EDIT_DIR_TEXT, m_sTexturesPath);
	MDDV_MaxChars(pDX, m_sTexturesPath, _MAX_PATH, IDC_EDIT_DIR_TEXT);
	MDDX_Text(pDX, IDC_EDIT_DIR_ONAT, m_sObjectsPath);
	MDDV_MaxChars(pDX, m_sObjectsPath, _MAX_PATH, IDC_EDIT_DIR_ONAT);
  MDDX_Text(pDX, IDC_EDIT_DIR_ONAT_PREF, m_sBaseDir);
  MDDX_Text(pDX, IDC_EDIT_DIR_TEXT_PREF, m_sBaseDir);

	// ---------------------------------------------------------- //
	// these lines are no more required after the introduction of //
	// the new project params dialog                              //
	// ---------------------------------------------------------- //
/*
	MDDX_Text(pDX, IDC_EDIT_REAL_UNIT_SIZE, m_nRealSizeUnit);
	MDDX_Text(pDX, IDC_EDIT_SAT_GRID, m_satelliteGridG);
	MDDV_MinMaxFloat(pDX, m_nRealSizeUnit, MIN_REAL_SIZE, MAX_REAL_SIZE, IDC_EDIT_REAL_UNIT_SIZE);
	MDDV_MinMaxInt(pDX, m_satelliteGridG, MIN_SATGRID_SIZE, MAX_SATGRID_SIZE, IDC_EDIT_SAT_GRID);
*/

  MDDX_Text(pDX, IDC_EDIT_CONFIG_NAME, m_configName);
}


BEGIN_MESSAGE_MAP(CEditProjectParamsDlg, CDialog)
	//{{AFX_MSG_MAP(CEditProjectParamsDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// 

BOOL DoEditPoseidonProject(CPosEdMainDoc* pDoc)
{
	CEditProjectParamsDlg dlg;
	MakeShortDirStr(dlg.m_sTexturesPath,pDoc->m_sTexturesPath); 
	MakeShortDirStr(dlg.m_sObjectsPath,pDoc->m_sObjectsPath);
	MakeLongDirStr(dlg.m_sBaseDir, GetPosEdEnvironment()->m_optSystem.m_sPreviewAppPath);

	// ---------------------------------------------------------- //
	// these lines are no more required after the introduction of //
	// the new project params dialog                              //
	// ---------------------------------------------------------- //
/*
	dlg.m_nRealSizeUnit = pDoc->m_nRealSizeUnit; 
	dlg.m_satelliteGridG = pDoc->m_satelliteGridG;
*/

	dlg.m_configName = pDoc->GetConfigName();

	if (dlg.DoModal()!=IDOK) return FALSE;

	bool updateBuld = false;
 
	// ---------------------------------------------------------- //
	// these lines are no more required after the introduction of //
	// the new project params dialog                              //
	// ---------------------------------------------------------- //
/*
	if (pDoc->m_nRealSizeUnit != dlg.m_nRealSizeUnit)  
		updateBuld = pDoc->ChangeRealSizeUnit(dlg.m_nRealSizeUnit);  
	if (pDoc->m_satelliteGridG != dlg.m_satelliteGridG)  
		updateBuld = pDoc->ChangeSatGrid(dlg.m_satelliteGridG);
*/
	bool updateView = updateBuld;

	updateBuld = updateBuld || pDoc->m_sTexturesPath != dlg.m_sTexturesPath || pDoc->m_sObjectsPath	!= dlg.m_sObjectsPath 
						    || dlg.m_configName != pDoc->GetConfigName();

	dlg.m_sTexturesPath.MakeLower();
	dlg.m_sObjectsPath.MakeLower();
	pDoc->m_sTexturesPath = dlg.m_sTexturesPath; 
	pDoc->m_sObjectsPath  = dlg.m_sObjectsPath;
	pDoc->SetConfigName(dlg.m_configName);

	if (updateView) pDoc->UpdateAllViews(NULL);

	if (updateBuld) pDoc->OnUpdateBuldozer();
    
	return TRUE;
};


