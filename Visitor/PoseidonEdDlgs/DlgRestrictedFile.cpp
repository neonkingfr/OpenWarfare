/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"
#include <dlgs.h>

class SubDirFileDlg : public CFileDialog
{
protected: 
	const CString _subDir;
	bool _first;

public:
	explicit SubDirFileDlg(BOOL bOpenFileDialog, const CString& subDir, const CString& filter) 
	: _subDir(subDir)
	, _first(true)
	, CFileDialog(bOpenFileDialog, // TRUE for FileOpen, FALSE for FileSaveAs
				  NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, filter) 
	{
	}

	virtual void OnFolderChange();
protected:
	virtual BOOL OnFileNameOK();
};

void SubDirFileDlg::OnFolderChange()
{
  //CString currFolder = GetFolderPath();

  //HideControl(cmb2);
  //HideControl(stc4);

/*  
  if (_subDir.CompareNoCase(currFolder.Left(_subDir.GetLength())) != 0)
  { 
    CString text(_T("Can't load"));
    SetControlText(stc4, text);
  }
  else
  { 
    CString text(_T(""));
    SetControlText(stc4, text);
  }*/
}

BOOL SubDirFileDlg::OnFileNameOK()
{
	CString currFolder = GetFolderPath();
	if (_subDir.CompareNoCase(currFolder.Left(_subDir.GetLength())) != 0)
	{    
		CString text;
		text.Format(IDS_NOT_IN_SUBDIR, _subDir);
		AfxMessageBox(text);
		return TRUE;
	}
	return FALSE;
}

bool DoLoadFileFromSubDir(CString& file, const CString& subDir, const CString& initDir, const CString& filter)
{
	SubDirFileDlg dlg(TRUE, subDir, filter);

#ifdef OFN_EX_NOPLACESBAR
	dlg.m_ofn.FlagsEx |= OFN_EX_NOPLACESBAR;
#endif
	dlg.m_ofn.lpstrInitialDir = initDir;
	CString title;
	title.Format(IDS_OPEN_SUBDIR, subDir);
	dlg.m_ofn.lpstrTitle = title;

	if (IDOK != dlg.DoModal()) return false;

	file = dlg.GetPathName();
	return true;
}

bool DoLoadFilesFromSubDir(CStringArray& files, const CString& subDir, const CString& initDir,const CString& filter)
{
	SubDirFileDlg dlg(TRUE, subDir, filter);

#ifdef OFN_EX_NOPLACESBAR
	dlg.m_ofn.FlagsEx |= OFN_EX_NOPLACESBAR;
#endif
	dlg.m_ofn.Flags |= OFN_ALLOWMULTISELECT;

	dlg.m_ofn.lpstrInitialDir = initDir;
	CString title;
	title.Format(IDS_OPEN_SUBDIR, subDir);
	dlg.m_ofn.lpstrTitle = title;

	if (IDOK != dlg.DoModal()) return false;

	POSITION pos = dlg.GetStartPosition();
	while(pos)
	{
		CString file = dlg.GetNextPathName(pos);
		files.Add(file);
	}

	return true;
}