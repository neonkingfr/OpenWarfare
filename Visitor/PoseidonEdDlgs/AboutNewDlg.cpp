// AboutNew.cpp : implementation file
//

#include "stdafx.h"
#include "AboutNewDlg.h"


// CAboutNew dialog

IMPLEMENT_DYNAMIC(CAboutNewDlg, CDialog)

CAboutNewDlg::CAboutNewDlg(CWnd* pParent /*=NULL*/)
: CDialog(CAboutNewDlg::IDD, pParent)
{
}

CAboutNewDlg::~CAboutNewDlg()
{
}

void CAboutNewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutNewDlg, CDialog)
END_MESSAGE_MAP()

BOOL CAboutNewDlg::OnInitDialog(void)
{
	CDialog::OnInitDialog();

	CString caption;
	CString application;

#ifndef __ONLY_FOR_PUBLIC__
	caption     = "About Visitor 3";
	application = "Visitor 3";
#else
	caption     = "About Visitor 3 Personal Edition for ArmA II";
	application = "Visitor 3 Personal Edition for ArmA II";
#endif

	SetWindowTextA(caption);
	GetDlgItem(IDC_VISITOR)->SetWindowTextA(application);

	return TRUE;
}

// CAboutNew message handlers
