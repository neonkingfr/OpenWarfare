/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include <El/elementpch.hpp>
#include "resource.h"
#include "Progress.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTxtrAnalyseDlg dialog

class CTxtrAnalyseDlg : public CDialog
{
// Construction
public:
	CTxtrAnalyseDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	CPosEdMainDoc*		m_pDoc;
	PTRTEXTURETMPLT		m_pCurTexture;

	BOOL		m_bGoto;
	DWORD		m_dwGoTo;
	
	//{{AFX_DATA(CTxtrAnalyseDlg)
	enum { IDD = IDD_TEXTURE_ANALYSE_DLG };
	CListBox	m_ListPos;
	CListBox	m_List;
	BOOL		m_bPrimarOnly; 
	//}}AFX_DATA
 
  void UpdateTextureList();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTxtrAnalyseDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTxtrAnalyseDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPrimarOnly();
	afx_msg void OnSelchangeList1();
	afx_msg void OnGotoPos();
	afx_msg void OnShowPos();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public: 
  afx_msg void OnBnClickedBimPasDel();
  afx_msg void OnBnClickedBimPasRecr();
};
/////////////////////////////////////////////////////////////////////////////
// CTxtrAnalyseDlg dialog


CTxtrAnalyseDlg::CTxtrAnalyseDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTxtrAnalyseDlg::IDD, pParent)
{
	m_bGoto		  = FALSE;
	m_pCurTexture = NULL;
	//{{AFX_DATA_INIT(CTxtrAnalyseDlg)
	m_bPrimarOnly = FALSE;
	//}}AFX_DATA_INIT
}


BOOL CTxtrAnalyseDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// update seznamu
  int tabs[2];
  tabs[0] = 100;
  tabs[1] = 110;
  m_List.SetTabStops(2, tabs);	
  UpdateTextureList();
	OnSelchangeList1();
  
	return TRUE;  
}

void CTxtrAnalyseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTxtrAnalyseDlg)
	DDX_Control(pDX, IDC_LIST_POS, m_ListPos);
	DDX_Control(pDX, IDC_LIST1, m_List);
	DDX_Check(pDX, IDC_PRIMAR_ONLY, m_bPrimarOnly); 
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTxtrAnalyseDlg, CDialog)
	//{{AFX_MSG_MAP(CTxtrAnalyseDlg)
	ON_BN_CLICKED(IDC_PRIMAR_ONLY, OnPrimarOnly)
	ON_LBN_SELCHANGE(IDC_LIST1, OnSelchangeList1)
	ON_BN_CLICKED(IDC_GOTO_POS, OnGotoPos)
	ON_BN_CLICKED(IDC_SHOW_POS, OnShowPos)
	ON_LBN_DBLCLK(IDC_LIST_POS, OnGotoPos)
	ON_LBN_DBLCLK(IDC_LIST1, OnShowPos)
	//}}AFX_MSG_MAP  
  ON_BN_CLICKED(IDC_BIMPAS_DEL, OnBnClickedBimPasDel)
  ON_BN_CLICKED(IDC_BIMPAS_RECR, OnBnClickedBimPasRecr)
END_MESSAGE_MAP()

void CTxtrAnalyseDlg::OnPrimarOnly() 
{
	if (UpdateData(TRUE))
	{
		UpdateTextureList();
	}
}

/////////////////////////////////////////////////////////////////////////////
// Texture operations

void CTxtrAnalyseDlg::UpdateTextureList()
{
	AfxGetApp()->BeginWaitCursor();

  // Calculate textures in selection
  TextureBank& textures = m_pDoc->m_Textures;
  int size = textures.Size();
  for(int i = 0; i< size;  i++)  
    textures[i]->m_nTxtCounter = textures[i]->m_nTxtCounter2 = 0;

  CRect rLogHull;
  m_pDoc->m_CurrentArea.GetObjectHull(rLogHull);
  UNITPOS m_LftTop, m_RghBtm;
  m_pDoc->m_PoseidonMap.GetTextureUnitPosHull(m_LftTop,m_RghBtm,rLogHull);

  CPoseidonMap& map = m_pDoc->m_PoseidonMap;
  CPositionArea& position = *m_pDoc->m_CurrentArea.GetLogObjectPosition();

  // read data from the map
  UNITPOS pos;
  for(pos.z = m_LftTop.z; pos.z >=m_RghBtm.z; pos.z--)
    for(pos.x = m_LftTop.x; pos.x <= m_RghBtm.x; pos.x++)
    { 
      if (map.IsTextureUnitCenterAtArea(pos, &position))
      {        
        PTRTEXTURETMPLT text = map.GetLandTextureAt(pos);
        if (text.NotNull())
          text->m_nTxtCounter++;

        text = map.GetBaseTextureAt(pos);
        if (text.NotNull())
          text->m_nTxtCounter2++;
      }
    };

  TextureRefBank sortTxtr; 
  for(int i= 0; i<textures.Size(); i++)
  {
    PTRTEXTURETMPLT pTxtr = (PTRTEXTURETMPLT)(textures[i]);
    if (pTxtr && (pTxtr->m_nTxtCounter > 0 || pTxtr->m_nTxtCounter2 > 0) && (!m_bPrimarOnly || pTxtr->m_nTxtrType != CPosTextureTemplate::txtrTypeSecundar))
    {	// sort textures
      BOOL bAdd = FALSE;
      for(int j = 0; j<sortTxtr.Size(); j++)
      {
        PTRTEXTURETMPLT pTxtr2 = (PTRTEXTURETMPLT)(sortTxtr[j]);
        if ((pTxtr2)&&(pTxtr2->m_nTxtCounter > pTxtr->m_nTxtCounter))
        {
          sortTxtr.Insert(j,pTxtr);
          bAdd = TRUE;
          break;
        }
      };
      if (!bAdd)
        sortTxtr.Add(pTxtr);
    }
  };

	// insert textures
	m_List.ResetContent();
	CString sText,sNum;
	for(int j = 0; j<sortTxtr.Size(); j++)
	{
		PTRTEXTURETMPLT pTxtr2 = (PTRTEXTURETMPLT)(sortTxtr[j]);
		if (pTxtr2)
		{
			sText  = pTxtr2->m_sTextureName;
			sText += _T(" (");      
			sText += pTxtr2->m_sTextureFile;     

      sText += _T(") \t");
      if (pTxtr2->m_nTxtCounter > 0) 
			  NumToLocalString(sNum,(LONG)(pTxtr2->m_nTxtCounter));
      else
      { // pure base texture
        NumToLocalString(sNum,(LONG)(pTxtr2->m_nTxtCounter2));
        sNum += _T(" (base)");
      }

			sText += sNum;
			LBAddString(m_List.m_hWnd, sText, (DWORD)pTxtr2.GetRef());
		}
	};
	LONG nCount = m_List.GetCount();
	if (nCount>0)
		m_List.SetCurSel(0);
	// display texture count
	NumToLocalString(sText,nCount);
	::SetWindowText(DLGCTRL(IDC_TXTR_COUNT),sText);

	AfxGetApp()->EndWaitCursor();
};

void CTxtrAnalyseDlg::OnSelchangeList1() 
{
	DWORD dwData;
	PTRTEXTURETMPLT pOld = m_pCurTexture;
	if (LBGetCurSelData(m_List.m_hWnd,dwData))
	{
		m_pCurTexture = (CPosTextureTemplate *)(dwData);
		if (m_pCurTexture)
		{	// name update

			CString sText  = m_pCurTexture->m_sTextureName;
			sText += _T(" (");
			sText += m_pCurTexture->m_sTextureFile;
			sText += _T(")");
			::SetWindowText(DLGCTRL(IDC_SEL_TXTR_NAME),sText);
			
		}
	} else
		m_pCurTexture = NULL;

	if (pOld != m_pCurTexture)
		m_ListPos.ResetContent();
}

void CTxtrAnalyseDlg::OnShowPos() 
{
	m_ListPos.ResetContent();

  // find all selected textures
  CRect rLogHull;
  m_pDoc->m_CurrentArea.GetObjectHull(rLogHull);
  UNITPOS m_LftTop, m_RghBtm;
  m_pDoc->m_PoseidonMap.GetTextureUnitPosHull(m_LftTop,m_RghBtm,rLogHull);

  CPoseidonMap& map = m_pDoc->m_PoseidonMap;
  CPositionArea& position = *m_pDoc->m_CurrentArea.GetLogObjectPosition();

  // read data from the map
  if (m_pCurTexture->m_nTxtCounter > 0)
  { //land texture
    UNITPOS pos;
    for(pos.z = m_LftTop.z; pos.z >=m_RghBtm.z; pos.z--) for(pos.x = m_LftTop.x; pos.x <= m_RghBtm.x; pos.x++)
    { 
      if (map.IsTextureUnitCenterAtArea(pos, &position))
      {              
        CPosTextureTemplate* pLand = map.GetLandTextureAt(pos);
        if ((pLand != NULL)&&(pLand == m_pCurTexture))
        {
          CString sX,sZ;
          NumToLocalString(sX,(LONG)pos.x);
          NumToLocalString(sZ,(LONG)pos.z);
          sX += _T(" ; ");
          sX += sZ;

          DWORD dwData = MAKELONG(pos.x, pos.z);
          LBAddString(m_ListPos.m_hWnd, sX, dwData);
        };
      };
    }
  }
  else
  {
    // base texture
    UNITPOS pos;
    for(pos.z = m_LftTop.z; pos.z >=m_RghBtm.z; pos.z--) for(pos.x = m_LftTop.x; pos.x <= m_RghBtm.x; pos.x++)
    { 
      if (map.IsTextureUnitCenterAtArea(pos, &position))
      {              
        CPosTextureTemplate* pLand = map.GetBaseTextureAt(pos);
        if ((pLand != NULL)&&(pLand == m_pCurTexture))
        {
          CString sX,sZ;
          NumToLocalString(sX,(LONG)pos.x);
          NumToLocalString(sZ,(LONG)pos.z);
          sX += _T(" ; ");
          sX += sZ;

          DWORD dwData = MAKELONG(pos.x, pos.z);
          LBAddString(m_ListPos.m_hWnd, sX, dwData);
        };
      };
    }
  }

	if (m_ListPos.GetCount()>0)
		m_ListPos.SetCurSel(0);
}

void CTxtrAnalyseDlg::OnGotoPos() 
{
	if (LBGetCurSelData(m_ListPos.m_hWnd,m_dwGoTo))
	{
		m_bGoto = TRUE;
		EndDialog(IDOK);
	} else
	{
		MessageBeep(MB_DEFAULT);
		m_bGoto = FALSE;
	}
}

void CTxtrAnalyseDlg::OnBnClickedBimPasDel()
{  
  AfxGetApp()->BeginWaitCursor();
  CStringArray	FilesToDelete;
  
  // actualize file list
  CString sPath;
  m_pDoc->GetProjectDirectory(sPath,CPosEdBaseDoc::dirText);
  sPath += _T("*");
  sPath += BIMPAS_FILE_EXT;
  //	find files
  WIN32_FIND_DATA	findData;
  HANDLE hFind = FindFirstFile(sPath,&findData);
  if (hFind != INVALID_HANDLE_VALUE)
  {    
    FilesToDelete.Add(findData.cFileName);  
    while(FindNextFile(hFind,&findData))
    {    
      FilesToDelete.Add(findData.cFileName);
    };
    FindClose(hFind);
  }
  // array contains eveything to be deleted
  int MaxNameLen = MAX_FILENAME_LEN_TEXTURE_PRIMAR + lstrlen(BIMPAS_FILE_EXT); // maxlen for primary texture

  if (FilesToDelete.GetSize()>0)
  {    
    m_pDoc->m_Textures.CleanUnUsed();
    for(int i=0; i< FilesToDelete.GetSize(); i++)
    {
      //if it is used do not delete it.
      int l = FilesToDelete[i].GetLength();

      Ref<CPosTextureTemplate> pTxtr;
      if (l <= MaxNameLen)
      {
        // primary texture file search for texture file in texture database.
        CString textureFile;
        CutExtFromFileName(textureFile, FilesToDelete[i]);
        textureFile += TEXTURE_FILE_EXT2;
        pTxtr = m_pDoc->m_Textures.FindFileEx(textureFile);      
      }
      else
        pTxtr = m_pDoc->m_Textures.FindFileEx(FilesToDelete[i]);

      if (pTxtr.IsNull())
      {      
        CString sPath;
        m_pDoc->GetProjectDirectory(sPath,CPosEdBaseDoc::dirText);
        sPath += FilesToDelete[i];
        // delete the file
        MntDeleteFile(sPath);        
      }
    }
  } 
 
  UpdateTextureList();
  AfxGetApp()->EndWaitCursor();
}

void CTxtrAnalyseDlg::OnBnClickedBimPasRecr()
{
  CProgressDialog progress(this);
  progress.Create();
  progress.SetRange(m_pDoc->m_PoseidonMap.GetSize().x);
  progress.SetPos(0);

  CWnd * wnd = GetDlgItem(IDC_BIMPAS_RECR);
  if (wnd)
  {
    CString text;
    wnd->GetWindowText(text);
    progress.SetWindowText(text);
  }
  AfxGetApp()->BeginWaitCursor();
  
  // Zero texture counter
  int size = m_pDoc->m_Textures.Size();
  for(int i = 0; i< size;  i++)  
    m_pDoc->m_Textures[i]->m_nTxtCounter = 0;

  Ref<CTextureLayer> actualLayer = m_pDoc->m_PoseidonMap.GetActiveTextureLayer();
  for(int x = 0; x< m_pDoc->m_PoseidonMap.GetSize().x;  x++)
  {
    progress.SetPos(x);
    for(int y = 0; y< m_pDoc->m_PoseidonMap.GetSize().z; y++)
    {
      UNITPOS unPos = { x,y };
      actualLayer->DoUpdateSecndTexturesAt(unPos,TRUE);      
    };  
  };
  
  m_pDoc->m_Textures.CleanUnUsed();
  UpdateTextureList();
  AfxGetApp()->EndWaitCursor();
}

BOOL DoTextureAnalyse(CPosEdMainDoc* pDoc,REALPOS& GoTo)
{
  CTxtrAnalyseDlg dlg;
  dlg.m_pDoc = pDoc;

  if (dlg.DoModal()==IDOK)
  {
    if (dlg.m_bGoto)
    {
      UNITPOS Pos = { LOWORD(dlg.m_dwGoTo), HIWORD(dlg.m_dwGoTo) };
      GoTo = pDoc->m_PoseidonMap.FromTextureUnitToRealPos(Pos);
      GoTo.x += (pDoc->m_PoseidonMap.GetTextureRealSize())/2;
      GoTo.z += (pDoc->m_PoseidonMap.GetTextureRealSize())/2;
      return TRUE;
    }
  }
  return FALSE;
};
