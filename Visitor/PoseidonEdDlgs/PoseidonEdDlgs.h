/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#if !defined(_POSEDDLGS_MAIN_H_)
#define _POSEDDLGS_MAIN_H_

/////////////////////////////////////////////////////////////////////////////
//  PoseidonEdDlgs.h : header file
//	resource >> 18000

/////////////////////////////////////////////////////////////////////////////
// �ablona objektu

/*AFX_EXT_API*/	BOOL GetObjectFileSelection(CString& sObjFile,CPosEdMainDoc* pDoc,BOOL bIn1Categ);

/////////////////////////////////////////////////////////////////////////////
// Parametry nov�ho projektu	(DlgNewProject.cpp)

/*AFX_EXT_API*/	BOOL OnNewPoseidonProject(CPosEdMainDoc*);

// -------------------------------------------------------------------------- //
// this is the new function added to allow to modify the project params using //
// the new project params dialog  (DlgProjectParameters.cpp)                  //
// -------------------------------------------------------------------------- //
/*AFX_EXT_API*/	BOOL OnEditPoseidonProjectParameters(CPosEdMainDoc*);

/*AFX_EXT_API*/	BOOL DoEditPoseidonProject(CPosEdMainDoc*);
BOOL OnXYZPoseidonProject(/*CPosEdMainDoc* pDoc*/ SMapSize & mapSize);

/////////////////////////////////////////////////////////////////////////////
// Parametry zobrazen� okna		(DlgViewStyle.cpp)

/*AFX_EXT_API*/	BOOL OnEditViewStyle(CViewStyle*);

/////////////////////////////////////////////////////////////////////////////
// spr�va konfigurac�		(DlgEditConfigs.cpp)

/*AFX_EXT_API*/	BOOL DoEditPosEdConfigs();
/*AFX_EXT_API*/	BOOL DoEditCurrentConfig();

/////////////////////////////////////////////////////////////////////////////
// definice objekt�

/*AFX_EXT_API*/	BOOL DoDefineTemplateTexture(CPosEdMainDoc*);
/*AFX_EXT_API*/	BOOL DoDefineTemplateNatObjs(CPosEdMainDoc*);
/*AFX_EXT_API*/	BOOL DoDefineTemplatePplObjs(CPosEdMainDoc*);

/*AFX_EXT_API*/	BOOL DoDefineTemplateNewRoadsObjs(CPosEdMainDoc*);

/*AFX_EXT_API*/	BOOL DoDefineTemplateNets(CPosEdMainDoc*);
/*AFX_EXT_API*/	BOOL DoDefineTemplateWoods(CPosEdMainDoc*);

/////////////////////////////////////////////////////////////////////////////
// Volby programu a syst�mu (DlgOptions.cpp)

/*AFX_EXT_API*/	BOOL DoSetupPosEdOptions();

// About dialog
/*AFX_EXT_API*/	void DoAboutPosEdApp(int nIDR);

// Volby programu a syst�mu (DlgSysOpts.cpp)
/*AFX_EXT_API*/	BOOL DoSetupSystemOptions();

/////////////////////////////////////////////////////////////////////////////
// Import reli�fu		(ImportRelief.cpp)

/*AFX_EXT_API*/	BOOL DoImportRelief(CPosEdMainDoc*);

/////////////////////////////////////////////////////////////////////////////
// Anal�za textur		(TxtrAnalyse.cpp)

/*AFX_EXT_API*/	BOOL DoTextureAnalyse(CPosEdMainDoc*,REALPOS& GoTo);

/////////////////////////////////////////////////////////////////////////////
// Pomocn� prompt dialogy - PromptUnivDlg.cpp

// editace jednoduch�ho textu
/*AFX_EXT_API*/ BOOL DoPromptTextDlg(CString& sText, int nMaxLen,
					UINT nDlgTitle, UINT nDlgText = 0,
					CStringArray* pExclusive = NULL, BOOL bRemSpace = FALSE);

/*AFX_EXT_API*/ BOOL DoPromptTextMapDlg(CString& sText, int nMaxLen,
					UINT nDlgTitle, UINT nDlgText = 0,
					CMapIntToStr* pExclusive = NULL, BOOL bRemSpace = FALSE);

///////////////////////////////////////////////////////////////////////////
// Dialog for image export
          bool OnImageExport(const float *  minmaxTerrainHeight, float * minmaxImageHeight, CWnd * pParentWnd = NULL, 
            const float * minmaxAreaHeight = NULL, bool * onlyArea = NULL);


//////////////////////////////////////////////////////////////////////////
// Check file
bool CheckFileAccess(const CString& fileName, BOOL write = TRUE, BOOL mustExist = TRUE, BOOL messageBox = TRUE);

//////////////////////////////
// Load/Save to subdir
bool DoLoadFileFromSubDir(CString& file, const CString& subDir, const CString& initDir,const CString& filter);
bool DoLoadFilesFromSubDir(CStringArray& files, const CString& subDir, const CString& initDir,const CString& filter);

/////////////////////////////////////////////////////////////////////////////
#endif // !defined(_POSEDDLGS_MAIN_H_)

