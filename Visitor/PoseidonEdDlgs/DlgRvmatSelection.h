#pragma once
#include "resource.h"


// CDlgRvmatSelection dialog

class CDlgRvmatSelection : public CDialog
{
	DECLARE_DYNAMIC(CDlgRvmatSelection)

public:
	CDlgRvmatSelection(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgRvmatSelection();

// Dialog Data
	enum { IDD = IDD_RVMATSELECTION };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	int m_Choice;
};
