/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

#include "stdafx.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef Fail
#undef Fail
#endif

/////////////////////////////////////////////////////////////////////////////
// CPromptTextDlg dialog

class CPromptTextDlg : public CDialog
{
// Construction
public:
	CPromptTextDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	CString			m_sTitle;
	CString			m_sText;
	CString			m_sOldName;

	int				m_nMaxLength;
	BOOL			m_bRemoveSpa;
	CStringArray*	m_pExclusive;
	CMapIntToStr*	m_pExclusMap;

	//{{AFX_DATA(CPromptTextDlg)
	enum { IDD = IDD_PROMPT_SINGLE_TEXT };
	CString	m_sName;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPromptTextDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CPromptTextDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


CPromptTextDlg::CPromptTextDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPromptTextDlg::IDD, pParent)
{
	m_pExclusive = NULL;
	m_pExclusMap = NULL;
	//{{AFX_DATA_INIT(CPromptTextDlg)
	m_sName = _T("");
	//}}AFX_DATA_INIT
}

BOOL CPromptTextDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	::SetWindowText(m_hWnd,m_sTitle);
	::SetWindowText(DLGCTRL(IDC_EDIT_TEXT),m_sText);
	return TRUE;
}

void CPromptTextDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPromptTextDlg)
	//}}AFX_DATA_MAP
	MDDX_Text(pDX, IDC_EDIT_NAME, m_sName);
	MDDV_MaxChars(pDX, m_sName, m_nMaxLength, IDC_EDIT_NAME);

	if (SAVEDATA)
	{
		if (m_bRemoveSpa)
		{	// odstran�n� mezer
			CString sPom;
			LPTSTR  pDest = sPom.GetBuffer(m_sName.GetLength()+1);
			m_sName = CutLeftChar(pDest,CutRightChar(pDest,m_sName));
			sPom.ReleaseBuffer();
		}
		MDDV_MinChars(pDX, m_sName, 1);
		if (m_pExclusive != NULL)
		{	// kontrola exclusivity
			for(int i=m_pExclusive->GetSize()-1;i>=0;i--)
			{
				if ((m_sName == ((*m_pExclusive)[i]))&&
					(m_sName != m_sOldName))
				{	// text se opakuje
					AfxMessageBox(IDS_ERROR_DUPLIC_TEXT,MB_OK|MB_ICONEXCLAMATION);
					pDX->Fail();
				}
			}
		} else
		if (m_pExclusMap != NULL)
		{
			for(POSITION pos = m_pExclusMap->GetStartPosition(); pos != NULL; )
			{
				int nKey;
				CString sText;
				m_pExclusMap->GetNextAssoc(pos,nKey,sText);

				if ((m_sName == sText)&&
					(m_sName != m_sOldName))
				{	// text se opakuje
					AfxMessageBox(IDS_ERROR_DUPLIC_TEXT,MB_OK|MB_ICONEXCLAMATION);
					pDX->Fail();
				}
			};
		}
	}
}


BEGIN_MESSAGE_MAP(CPromptTextDlg, CDialog)
	//{{AFX_MSG_MAP(CPromptTextDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// editace jednoduch�ho textu

BOOL DoPromptTextDlg(CString& sText, int nMaxLen,
					UINT nDlgTitle, UINT nDlgText,
					CStringArray* pExclusive, BOOL bRemSpace)
{
	CPromptTextDlg dlg;
	// titulek a text
	VERIFY(dlg.m_sTitle.LoadString(nDlgTitle));
	VERIFY(dlg.m_sText.LoadString((nDlgText!=0)?nDlgText:nDlgTitle));
	dlg.m_pExclusive = pExclusive;
	// inicializace dat
	dlg.m_nMaxLength = nMaxLen;
	dlg.m_bRemoveSpa = bRemSpace;
	dlg.m_sOldName	 =
	dlg.m_sName		 = sText;

	if (dlg.DoModal()==IDOK)
	{
		if (dlg.m_sName != sText)
		{
			sText = dlg.m_sName;
			return TRUE;
		}
	}
	return FALSE;
};

BOOL DoPromptTextMapDlg(CString& sText, int nMaxLen,
					UINT nDlgTitle, UINT nDlgText,
					CMapIntToStr* pExclusive, BOOL bRemSpace)
{
	CPromptTextDlg dlg;
	// titulek a text
	VERIFY(dlg.m_sTitle.LoadString(nDlgTitle));
	VERIFY(dlg.m_sText.LoadString((nDlgText!=0)?nDlgText:nDlgTitle));
	dlg.m_pExclusMap = pExclusive;
	// inicializace dat
	dlg.m_nMaxLength = nMaxLen;
	dlg.m_bRemoveSpa = bRemSpace;
	dlg.m_sOldName	 =
	dlg.m_sName		 = sText;

	if (dlg.DoModal()==IDOK)
	{
		if (dlg.m_sName != sText)
		{
			sText = dlg.m_sName;
			return TRUE;
		}
	}
	return FALSE;
};
