// DlgProjectParameters.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "DlgProjectParameters.h"
#include "DlgSatelliteGridCalc.h"
#include <string>

// -------------------------------------------------- //
// added to give access to the class CTextureLayerDlg //
// -------------------------------------------------- //
#include "..\poseidonedobjs\Actions_TextureLayers.h"

using std::string;

// CDlgProjectParameters dialog

IMPLEMENT_DYNAMIC(CDlgProjectParameters, CDialog)

CDlgProjectParameters::CDlgProjectParameters(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgProjectParameters::IDD, pParent)
	, m_satelliteGridG(7)
{
	m_bAllowMapSizeEdit      = false;
	m_bAllowTextureLayerEdit = false;
	m_pDoc                   = NULL;
}

CDlgProjectParameters::~CDlgProjectParameters()
{
}

void CDlgProjectParameters::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TERRAINCELLEDIT, m_cSquareSize);
	DDX_Control(pDX, IDC_TERRAINSIZECOMBO, m_TerrainSizeComboCtrl);
	DDX_Control(pDX, IDC_MAPSIZEEDIT, m_MapSizeEditCtrl);
	DDX_Text(pDX, IDC_SATELLITEGRIDUSED, m_satelliteGridG);
	DDX_Control(pDX, IDC_TEXTURELAYERLIST, m_ListCtrl);
	DDX_Text(pDX, IDC_GAMESEGMENTEDIT, m_GameSegment);
}

BEGIN_MESSAGE_MAP(CDlgProjectParameters, CDialog)
	ON_CBN_SELCHANGE(IDC_TERRAINSIZECOMBO, OnCbnSelchangeTerrainsizecombo)
	ON_EN_CHANGE(IDC_TERRAINCELLEDIT, OnEnChangeTerraincelledit)
	ON_BN_CLICKED(IDC_CALC_BTN, OnBnClickedCalcBtn)
	ON_LBN_SELCHANGE(IDC_TEXTURELAYERLIST, OnLbnSelchangeTexturelayerlist)
	ON_BN_CLICKED(IDC_ADD_BTN, OnBnClickedAddBtn)
	ON_BN_CLICKED(IDC_EDIT_BTN, OnBnClickedEditBtn)
	ON_BN_CLICKED(IDC_REMOVE_BTN, OnBnClickedRemoveBtn)
	ON_LBN_DBLCLK(IDC_TEXTURELAYERLIST, OnLbnDblclkTexturelayerlist)
	ON_EN_CHANGE(IDC_SATELLITEGRIDUSED, OnEnChangeSatellitegridused)
END_MESSAGE_MAP()


// CDlgProjectParameters message handlers

BOOL CDlgProjectParameters::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_cSquareSize.SetRange(MAX_REAL_SIZE, 0);
	m_cSquareSize.SetValue(m_fSquareSize);
	
	for(int i = MIN_WORLD_SIZE; i <= MAX_WORLD_SIZE; i *= 2)
	{
		CString str;
		str.Format("%d x %d", i, i);
		m_TerrainSizeComboCtrl.AddString((LPCTSTR)str);
	}

	CString selStr;
	selStr.Format("%d x %d", m_nWorldSquareWidth, m_nWorldSquareHeight);
	m_TerrainSizeComboCtrl.SelectString(-1, (LPCTSTR)selStr);

	ComputeMapSize();	

	if(m_bAllowMapSizeEdit)
	{
		GetDlgItem(IDC_TERRAINSIZECOMBO)->EnableWindow(TRUE);		
		GetDlgItem(IDC_TERRAINCELLEDIT)->EnableWindow(TRUE);		
		GetDlgItem(IDC_MAPSIZEEDIT)->EnableWindow(TRUE);		
	}
	else
	{
		GetDlgItem(IDC_TERRAINSIZECOMBO)->EnableWindow(FALSE);		
		GetDlgItem(IDC_TERRAINCELLEDIT)->EnableWindow(FALSE);		
		GetDlgItem(IDC_MAPSIZEEDIT)->EnableWindow(FALSE);		
	}

	if(m_bAllowTextureLayerEdit && m_pDoc != NULL)
	{
		FillTextureLayerList();
		m_ListCtrl.EnableWindow(TRUE);		
		GetDlgItem(IDC_ADD_BTN)->EnableWindow(TRUE);		
		GetDlgItem(IDCANCEL)->EnableWindow(FALSE);		
	}
	else
	{
		m_ListCtrl.AddString("Base (active)");
		m_ListCtrl.EnableWindow(FALSE);		
		GetDlgItem(IDC_ADD_BTN)->EnableWindow(FALSE);		
		GetDlgItem(IDCANCEL)->EnableWindow(TRUE);		
	}

	ComputeGameSegment();

	return TRUE;
}

void CDlgProjectParameters::OnCbnSelchangeTerrainsizecombo()
{
	CString str;
	m_TerrainSizeComboCtrl.GetWindowTextA(str);

	if(str == "")
	{
		m_MapSizeEditCtrl.SetWindowTextA("");
		return;
	}

	int pos = str.Find('x');
	CString strWidth  = str.Left(pos - 1);
	CString strHeight = str.Right(str.GetLength() - (pos + 2));

	m_nWorldSquareWidth  = atoi(string(strWidth).c_str());
	m_nWorldSquareHeight = atoi(string(strHeight).c_str());

	ComputeMapSize();	
}

void CDlgProjectParameters::OnEnChangeTerraincelledit()
{
	if(!UpdateData(TRUE) || !m_cSquareSize.IsValid())
	{
		m_MapSizeEditCtrl.SetWindowTextA("");
		GetDlgItem(IDC_CALC_BTN)->EnableWindow(FALSE);		
		GetDlgItem(IDOK)->EnableWindow(FALSE);		
		return;
	}
	else
	{
		GetDlgItem(IDC_CALC_BTN)->EnableWindow(TRUE);		
		GetDlgItem(IDOK)->EnableWindow(TRUE);		
	}

	m_fSquareSize = m_cSquareSize.GetValue();

	ComputeMapSize();	
	ComputeGameSegment();
}

void CDlgProjectParameters::ComputeMapSize()
{	
	float realWidth  = m_fSquareSize * m_nWorldSquareWidth;
	float realHeight = m_fSquareSize * m_nWorldSquareHeight;

	CString strMapSize;
	strMapSize.Format("%.1f x %.1f", realWidth, realHeight);
	m_MapSizeEditCtrl.SetWindowTextA((LPCTSTR)strMapSize);
}

void CDlgProjectParameters::ComputeGameSegment()
{	
	// searches for active texture layer
	int activeTextureLayerIndex = -1;
    for (int i = 0; i < m_pDoc->m_PoseidonMap.GetTextureLayersCount(); ++i)
    {
		Ref<CTextureLayer> layer = m_pDoc->m_PoseidonMap.GetTextureLayer(i);
		if ((layer != NULL) && (layer->Name().GetLength() > 0))
		{
			if (layer == m_pDoc->m_PoseidonMap.GetActiveTextureLayer())
			{
				activeTextureLayerIndex = layer->m_nTexSizeInSqLog;
				break;
			}
		}
    }

	// when we enter this dialog with the new command, no texture layer is still active
	// so we use the first in the list (index = 0) as default
	if (activeTextureLayerIndex == -1)
	{
		activeTextureLayerIndex = 0;
	}

	// denominator is equal to 2^activeTextureLayerIndex
	int denominator = (1 << activeTextureLayerIndex);

	m_GameSegment = static_cast<int>(m_satelliteGridG / denominator);
	UpdateData(FALSE);

	// now we check if the value is a multiple of 8 or 12, which should be the correct value
	// to pass to the engine
	if ((m_GameSegment % 8 == 0) || (m_GameSegment % 12 == 0))
	{
		GetDlgItem(IDC_GAMEOKLBL)->SetWindowTextA("(Valid)");		
	}
	else
	{
		GetDlgItem(IDC_GAMEOKLBL)->SetWindowTextA("(Invalid)");		
	}
}

void CDlgProjectParameters::OnBnClickedCalcBtn()
{
	CDlgSatelliteGridCalc dlg;

	// searches for active texture layer
	int activeTextureLayerIndex = -1;
    for (int i = 0; i < m_pDoc->m_PoseidonMap.GetTextureLayersCount(); ++i)
    {
		Ref<CTextureLayer> layer = m_pDoc->m_PoseidonMap.GetTextureLayer(i);
		if ((layer != NULL) && (layer->Name().GetLength() > 0))
		{
			if (layer == m_pDoc->m_PoseidonMap.GetActiveTextureLayer())
			{
				activeTextureLayerIndex = layer->m_nTexSizeInSqLog;
				break;
			}
		}
    }

	// when we enter this dialog with the new command, no texture layer is still active
	// so we use the first in the list (index = 0) as default
	if (activeTextureLayerIndex == -1)
	{
		activeTextureLayerIndex = 0;
	}

	dlg.m_fSquareSize       = m_fSquareSize;
	dlg.m_nWorldSquareWidth = m_nWorldSquareWidth;
	dlg.m_activeTextureLayerIndex = activeTextureLayerIndex;
	if (dlg.DoModal() != IDOK) return;
}

void CDlgProjectParameters::OnLbnSelchangeTexturelayerlist()
{
	int sel = m_ListCtrl.GetCurSel();
	if (sel == LB_ERR)
	{
		GetDlgItem(IDC_EDIT_BTN)->EnableWindow(FALSE);		
		GetDlgItem(IDC_REMOVE_BTN)->EnableWindow(FALSE);		
	}
	else
	{
		GetDlgItem(IDC_EDIT_BTN)->EnableWindow(TRUE);		
		GetDlgItem(IDC_REMOVE_BTN)->EnableWindow(TRUE);		
	}
}

void CDlgProjectParameters::FillTextureLayerList()
{
	m_ListCtrl.ResetContent();
    for (int i = 0; i < m_pDoc->m_PoseidonMap.GetTextureLayersCount(); ++i)
    {
		Ref<CTextureLayer> layer = m_pDoc->m_PoseidonMap.GetTextureLayer(i);
		if ((layer != NULL) && (layer->Name().GetLength() > 0))
		{
			if (layer != m_pDoc->m_PoseidonMap.GetActiveTextureLayer())
			{
				m_ListCtrl.AddString(layer->Name());
			}
			else
			{
				CString layerName;
				layerName = layer->Name() + _T(" (active)");
				m_ListCtrl.AddString(layerName);
			}
		}
    }
}
void CDlgProjectParameters::OnBnClickedAddBtn()
{
	CTextureLayerDlg dlg(m_pDoc);

	if (dlg.DoModal() != IDOK) return;

	TRY
	{  
		CPosTextureLayerAction* pAction;
		pAction = new CPosTextureLayerAction(IDA_TEXTURE_LAYER_CREATE);

		pAction->m_CurrName               = dlg.m_Name;
		pAction->m_CurrTextureSizeInSqLog = dlg.m_nTexSizeInSqLog;

		m_pDoc->ProcessEditAction(pAction);
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException(); 
	}
	END_CATCH_ALL

	FillTextureLayerList();
	ComputeGameSegment();
}

void CDlgProjectParameters::OnBnClickedEditBtn()
{
	int sel = m_ListCtrl.GetCurSel();
	if (sel != LB_ERR)
	{
		Ref<CTextureLayer> layer = m_pDoc->m_PoseidonMap.GetTextureLayer(sel);

		CTextureLayerDlg dlg(m_pDoc);
		dlg.m_Name = dlg.m_NameInit = layer->Name();
		dlg.m_nTexSizeInSqLog = layer->m_nTexSizeInSqLog;

		if (dlg.DoModal() != IDOK) return;

		TRY
		{  
			CPosTextureLayerAction* pAction;
			pAction = new CPosTextureLayerAction(IDA_TEXTURE_LAYER_EDIT);
	    
			pAction->CreateOldData(layer);
			pAction->m_CurrName               = dlg.m_Name;
			pAction->m_CurrTextureSizeInSqLog = dlg.m_nTexSizeInSqLog;

			m_pDoc->ProcessEditAction(pAction);
		}
		CATCH_ALL(e)
		{
			MntReportMemoryException(); 
		}
		END_CATCH_ALL

		FillTextureLayerList();
		ComputeGameSegment();
	}
}

void CDlgProjectParameters::OnBnClickedRemoveBtn()
{
	int sel = m_ListCtrl.GetCurSel();

	if (sel != LB_ERR)
	{
		Ref<CTextureLayer> layer = m_pDoc->m_PoseidonMap.GetTextureLayer(sel);

		TRY
		{  
			CPosTextureLayerAction* pAction;
			pAction = new CPosTextureLayerAction(IDA_TEXTURE_LAYER_REMOVE);

			pAction->CreateOldData(layer);

			m_pDoc->ProcessEditAction(pAction);
		}
		CATCH_ALL(e)
		{
			MntReportMemoryException(); 
		}
		END_CATCH_ALL

		FillTextureLayerList();
		ComputeGameSegment();
	}
}

void CDlgProjectParameters::OnLbnDblclkTexturelayerlist()
{
	int sel = m_ListCtrl.GetCurSel();

	if (sel != LB_ERR)
	{
		Ref<CTextureLayer> layer = m_pDoc->m_PoseidonMap.GetTextureLayer(sel);

		BeginWaitCursor();
		m_pDoc->m_PoseidonMap.SwitchActiveTextureLayer(layer);
		EndWaitCursor();

		FillTextureLayerList();
		ComputeGameSegment();
	}
}

void CDlgProjectParameters::OnEnChangeSatellitegridused()
{
	UpdateData(TRUE);
	ComputeGameSegment();
}
