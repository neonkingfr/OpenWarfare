#include "stdafx.h"
#include "resource.h"
#include "CNumEdit.h"

/////////////////////////////////////////////////////////////////////////////
// CNumEdit

CNumEdit::CNumEdit()
{
	m_Verbose = FALSE;
	m_MinValue = -FLT_MAX;
	m_MaxValue = FLT_MAX;
}

CNumEdit::~CNumEdit()
{
}

BEGIN_MESSAGE_MAP(CNumEdit, CEdit)  
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNumEdit message handlers

float CNumEdit::GetValue()
{
	float f = 0;  
	CString str;
	GetWindowText(str);
	if (IsValid(str) == VALID)
	{
		sscanf(str, "%f", &f);
	}
	return f;
}

void CNumEdit::SetValue(float val)
{
	CString str;
	str.Format("%.1f", val);
	SetWindowText(str);
}

BOOL CNumEdit::IsValid()
{
	CString str;
	GetWindowText(str);
	return (VALID == IsValid(str));
}

int CNumEdit::IsValid(const CString &str)
{
	//ASSERT(m_MinValue >= m_MaxValue);
	int res = VALID;
	float f;
	if (sscanf(str, "%f", &f) != 1) res = INVALID_CHAR;
	if (f > m_MaxValue || f < m_MinValue) res = OUT_OF_RANGE;
	if (m_Verbose && res != VALID)
	{
		CString msg;
		msg.Empty();
		if (res & OUT_OF_RANGE) msg += _T("Given value is out of range.\n");
		if (res & INVALID_CHAR) msg += _T("Characters must be a number.\n");
		AfxMessageBox(msg, MB_OK | MB_ICONSTOP);
		SetFocus();
	}
	return res;
}

BOOL CNumEdit::OnChange() 
{  
	CString newstr;
	GetWindowText(newstr);  
	if (IsValid(newstr) != VALID)
	{
		//SetWindowText(m_lastString);
		//SetSel(0, -1);
		return FALSE;    
	}
	else
	{
		//m_lastString = newstr;
		return TRUE;
	}
}

BOOL CNumEdit::Verbose()
{
	 return m_Verbose;
}

void CNumEdit::Verbose(BOOL v)
{
	m_Verbose = v;
}

void CNumEdit::SetRange(float max, float min)
{
	m_MinValue = min;
	m_MaxValue = max;
}

void CNumEdit::GetRange(float & max, float & min)
{
	min = m_MinValue;
	max = m_MaxValue;
}
