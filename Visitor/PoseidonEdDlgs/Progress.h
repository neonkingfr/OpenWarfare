#pragma once

class CProgressDialog : public CDialog
{
protected:
  CProgressCtrl	m_cProgress;

  // Construction
public:
  CProgressDialog(CWnd* pParent = NULL);   // standard constructor
 
  enum { IDD = IDD_PROGRESS };
protected:
  virtual void DoDataExchange(CDataExchange* pDX);

public:
  void Create();
  void SetRange(int range);
  void SetPos(int pos);
  void DoStep(int step = 1);

  void Done();
};