// DlgOriginOffset.cpp : implementation file
//

#include "stdafx.h"
#include "DlgOriginOffset.h"


// CDlgOriginOffset dialog

IMPLEMENT_DYNAMIC(CDlgOriginOffset, CDialog)

CDlgOriginOffset::CDlgOriginOffset(CWnd* pParent /*=NULL*/)
: CDialog(CDlgOriginOffset::IDD, pParent)
{

}

CDlgOriginOffset::~CDlgOriginOffset()
{
}

void CDlgOriginOffset::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_X_COORD, m_OriginX);
	DDX_Text(pDX, IDC_Y_COORD, m_OriginY);
}


BEGIN_MESSAGE_MAP(CDlgOriginOffset, CDialog)
END_MESSAGE_MAP()


// CDlgOriginOffset message handlers
