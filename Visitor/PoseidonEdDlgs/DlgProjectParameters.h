#pragma once
#include "afxwin.h"

#include "CNumEdit.h"

// CDlgProjectParameters dialog

class CDlgProjectParameters : public CDialog
{
	DECLARE_DYNAMIC(CDlgProjectParameters)

public:
	CDlgProjectParameters(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgProjectParameters();

// Dialog Data
	enum { IDD = IDD_PROJECT_PARAMS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	// map size parameters
	float          m_fSquareSize;
	CNumEdit       m_cSquareSize;
	int            m_nWorldSquareWidth;
	int            m_nWorldSquareHeight;
	// 
	bool           m_bAllowMapSizeEdit;
	bool           m_bAllowTextureLayerEdit;
	// the current doc
	CPosEdMainDoc* m_pDoc;

	/// satellite grid size (one physical texture per grid)
	int  m_satelliteGridG;
	// the satellite segment used by the engine
	int  m_GameSegment;

protected:
	CEdit     m_MapSizeEditCtrl;
	CComboBox m_TerrainSizeComboCtrl;
	CListBox  m_ListCtrl;
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnCbnSelchangeTerrainsizecombo();
	afx_msg void OnEnChangeTerraincelledit();
protected:
	void ComputeMapSize();
	void FillTextureLayerList();
public:
	void ComputeGameSegment();
public:
	afx_msg void OnBnClickedCalcBtn();
	afx_msg void OnLbnSelchangeTexturelayerlist();
	afx_msg void OnBnClickedAddBtn();
	afx_msg void OnBnClickedEditBtn();
	afx_msg void OnBnClickedRemoveBtn();
	afx_msg void OnLbnDblclkTexturelayerlist();
	afx_msg void OnEnChangeSatellitegridused();
};
