#include "stdAfx.h"
#include "resource.h"
#include "FMntTools.h"
#include ".\fmnttools.h"


void UpdateAllDocs(LPARAM lHint, CObject* pHint) {
  //DebugTrapSoft();
  CWinApp *app = AfxGetApp();
  if (app != NULL) {
    POSITION docTemplatePosition = app->GetFirstDocTemplatePosition();
    while (docTemplatePosition != NULL) {
      CDocTemplate *docTemplate = app->GetNextDocTemplate(docTemplatePosition);
      POSITION docPosition = docTemplate->GetFirstDocPosition();
      while (docPosition != NULL) {
        CDocument *document = docTemplate->GetNextDoc(docPosition);
        document->UpdateAllViews(NULL, lHint, pHint);
      }
    }
  }
};

LONG GetDistancePoints(LONG xx, LONG xy, LONG yx, LONG yy) {
  //DebugTrapSoft();
  LONG x = yx-xx;
  LONG y = yy-xy;
  return (LONG) sqrt(x*x + y*y);
};

LONG GetDistancePoints(POINT x, POINT y) {
  //DebugTrapSoft();
  return GetDistancePoints(x.x, x.y, y.x, y.y);
};



//////////////////////////////////////////////////////////////////////////////


void MntShowWindow(HWND hWnd, BOOL bShow) {
  //SendMessage(hWnd, WM_SHOWWINDOW, bShow, 0);
  if (bShow) {
    ShowWindow(hWnd, SW_SHOW);
    //SendMessage(hWnd, WM_SHOWWINDOW, SW_SHOW, 0);
  }
  else {
    ShowWindow(hWnd, SW_HIDE);
    //SendMessage(hWnd, WM_SHOWWINDOW, SW_HIDE, 0);
  }
  //DebugTrap();
};



LPCTSTR MakeLongDirStr(CString& dest,LPCTSTR source) 
{
  if (source!=(dest.GetBuffer(0))) dest = source;
  // change all / into '\\'
  int len;
	if ((len = dest.GetLength())== 0)
    return ((LPCTSTR)dest);

  LPTSTR destData = dest.GetBuffer(0);
  for(int i = 0; i < len; i++)
  {
    if (destData[i] == _T('/'))
      destData[i] = _T('\\');
  }
	
  dest.ReleaseBuffer();
	if (dest[len-1]!=_T('\\'))
	  dest += _T("\\");

	return ((LPCTSTR)dest);
};

LPCTSTR MakeShortDirStr(CString& dest,LPCTSTR source) 
{
  if (source!=(dest.GetBuffer(0))) dest = source;

  int len;
  if ((len = dest.GetLength()) == 0)
    return ((LPCTSTR)dest);

  // change all / into '\\'
  LPTSTR destData = dest.GetBuffer(0);
  for(int i = 0; i < len; i++)
  {
    if (destData[i] == _T('/'))
      destData[i] = _T('\\');
  }

  if (destData[len-1]==_T('\\'))
  {
    destData[len-1] = 0;    
  }
  dest.ReleaseBuffer();

  return ((LPCTSTR)dest);
}

BOOL MntExistFile(LPCTSTR pFileName) {
	FileServerHandle check=GFileServerFunctions->OpenReadHandle(pFileName);
	if( check==INVALID_HANDLE_VALUE ) return false;
	GFileServerFunctions->CloseReadHandle(pFileName,check);
	return true;
}



//////////////////////////////////////////////
// Num to string
LPCTSTR NumToStandardString(CString& sStr, LONG nNum )
{
	sStr.Format(_T("%ld"), nNum);
	return sStr;
}
LPCTSTR NumToStandardString(CString& sStr, double nNum,  int nDecimal)
{
	//CString sFormat;
	//sFormat.Format(_T("%%.%dg"), nDecimal);
	//sStr.Format(sFormat, nNum);
	sStr.Format("%g", nNum);

	return sStr;
}	

LPCTSTR NumToLocalString(CString& sStr, LONG nNum) 
{  
	return NumToStandardString(sStr, nNum);
}

LPCTSTR NumToLocalString(CString& sStr, double nNum, 
								         int nDecimal,
                         BOOL bStrict) 
{  
  return NumToStandardString(sStr, nNum, nDecimal);
}

BOOL MntGetSubstring(CString& dest, LPCTSTR lpSrc, TCHAR cSeparator, int nNum) {
  //DebugTrapSoft();
  dest.Empty();

  // Skip over nNum count of separators
  LPCTSTR currStr = lpSrc;
  if (nNum > 0) {
    for(int i = 0; i < nNum; i++) {
      currStr = strchr(currStr, cSeparator);
      if (currStr == NULL) return FALSE;
      currStr++;
    }
  }

  // Read chars until separator or end of string found
  while ((currStr[0] != cSeparator) && (currStr[0] != '\0')) {
    dest += currStr[0];
    currStr++;
  }
  return TRUE;
};

LPCTSTR SeparateDirFromFileName(CString& dest,LPCTSTR source)
{
  if (source != (LPCTSTR)dest) dest = source;
  int len = dest.GetLength();
  while(len > 0 && 
   (dest[len - 1] != _T('\\')) && 
   (dest[len - 1] != _T('/')))
   len--;

  (dest.GetBuffer(0))[len-1] = 0;
  dest.ReleaseBuffer();

  return ((LPCTSTR)dest);
}

LPCTSTR CutExtFromFileName(CString& dest,LPCTSTR source)
{
  if (source != (LPCTSTR)dest) dest = source;
  int len = dest.GetLength();
  while(len > 0 && 
    (dest[len - 1] != _T('\\')) && 
    (dest[len - 1] != _T('/')) && (source[len - 1] != _T('.')))
    len--;

  (dest.GetBuffer(0))[len-1] = 0;
  dest.ReleaseBuffer();

  return ((LPCTSTR)dest);
}


LPCTSTR SeparateNameFromFileName(CString& dest,LPCTSTR source)
{  
  int len = _tcslen(source);
  while(len > 0 && 
    (source[len - 1] != _T('\\')) && 
    (source[len - 1] != _T('/')))
    len--;

  dest = source + len;
  return ((LPCTSTR)dest);
}

LPCTSTR SeparateExtFromFileName(CString& dest,LPCTSTR source)
{  
  int len = _tcslen(source);
  while(len > 0 && 
    (source[len - 1] != _T('\\')) && 
    (source[len - 1] != _T('/')) && (source[len - 1] != _T('.')))
    len--;

  dest = source + len;
  return ((LPCTSTR)dest);
}

// --------------------------------------------------------------------
// --------------------------------------------------------------------


// the struct below is used to determine the qualities of a particular handle
struct AFX_HANDLEINFO
{
	size_t nOffsetX;    // offset within RECT for X coordinate
	size_t nOffsetY;    // offset within RECT for Y coordinate
	int nCenterX;       // adjust X by Width()/2 * this number
	int nCenterY;       // adjust Y by Height()/2 * this number
	int nHandleX;       // adjust X by handle size * this number
	int nHandleY;       // adjust Y by handle size * this number
	int nInvertX;       // handle converts to this when X inverted
	int nInvertY;       // handle converts to this when Y inverted
};

// this array describes all 8 handles (clock-wise)
static const AFX_HANDLEINFO rgHandleInfo[] =
{
	// corner handles (top-left, top-right, bottom-right, bottom-left
	{ offsetof(RECT, left), offsetof(RECT, top),        0, 0,  0,  0, 1, 3 },
	{ offsetof(RECT, right), offsetof(RECT, top),       0, 0, -1,  0, 0, 2 },
	{ offsetof(RECT, right), offsetof(RECT, bottom),    0, 0, -1, -1, 3, 1 },
	{ offsetof(RECT, left), offsetof(RECT, bottom),     0, 0,  0, -1, 2, 0 },
	// side handles (top, right, bottom, left)
	{ offsetof(RECT, left), offsetof(RECT, top),        1, 0,  1,  0, 4, 6 },
	{ offsetof(RECT, right), offsetof(RECT, top),       0, 1, -1,  1, 7, 5 },
	{ offsetof(RECT, left), offsetof(RECT, bottom),     1, 0,  1, -1, 6, 4 },
	{ offsetof(RECT, left), offsetof(RECT, top),        0, 1,  0,  1, 5, 7 }
};

/////////////////////////////////////////////////////////////////////////////
// CAction

IMPLEMENT_DYNAMIC(CAction,CObject)

CAction::CAction(int nType, LPCTSTR pActName)
{
	m_nActType = nType;
	if (pActName)
		m_sActName = pActName;
};

// proveden� akce v dokumentu
// m_OldValue se pou�ije jako nov� hodnota
BOOL CAction::RealizeAction(CDocument*)
{
	return TRUE;	// v�dy UK
};

// v�m�na hodnot m_CurValue a m_OldValue
BOOL CAction::ChangeValues()
{	// mus� b�t p�eps�no potomkem
	ASSERT(FALSE);
	return FALSE;
};

/////////////////////////////////////////////////////////////////////////////
// CActionManager

IMPLEMENT_DYNAMIC(CActionManager, CObject)

CActionManager::CActionManager()
{
	m_pDocument = NULL;
}

CActionManager::~CActionManager()
{	
	ClearUndoRedoBuffers(); 
}

void CActionManager::ClearUndoRedoBuffers()
{
	DestroyArrayObjects(&m_ActionUndoFront);
	DestroyArrayObjects(&m_ActionRedoFront);
}

BOOL CActionManager::RealizeAction(CAction* pAction) const
{	// nech�m to na vlastn� akci
	return pAction->RealizeAction(m_pDocument);
}

BOOL CActionManager::DoNewAction(CAction* pAction)
{
	if (pAction == NULL) return FALSE;
	// nejprve akci provedu
	if (!RealizeAction(pAction))
	{
		delete pAction;
		return FALSE;
	}
	// zam�n�m hodnoty akce
	VERIFY(pAction->ChangeValues());
	// vlo��m jako posledn�ho kandid�ta na undo
	m_ActionUndoFront.Add(pAction);
	// ma�u Redo
	DestroyArrayObjects(&m_ActionRedoFront);
	// v�e je OK
	return TRUE;
}
		
BOOL CActionManager::DoUndoAction()
{
	int nLast = m_ActionUndoFront.GetSize()-1;
	if (nLast>=0)
	{
		CAction* pAction = (CAction*)(m_ActionUndoFront[nLast]);
		ASSERT_KINDOF(CAction,pAction);
		// provedu akci
    if (pAction->IsKindOf(RUNTIME_CLASS(CPosAction)))
    {
      if (((CPosAction *) pAction)->m_nActType == IDA_GROUPOBJ_MOVE)
        ((CPosAction *) pAction)->m_bFromBuldozer = FALSE;
      else
        ((CPosAction *) pAction)->SetFromBuldozer(FALSE);
    }

		if (!RealizeAction(pAction))
			return FALSE;
		// zam�n�m hodnoty, vy�ad�m z Undo a d�m do Redo
		VERIFY(pAction->ChangeValues());
		m_ActionUndoFront.RemoveAt(nLast);
		m_ActionRedoFront.Add(pAction);
		// v�e je OK
		return TRUE;
	}
	return FALSE;
}

void CActionManager::OnUpdateUndoAction(CCmdUI* pCmdUI) const
{
	int nLast = m_ActionUndoFront.GetSize()-1;
	if (nLast>=0)
	{
		CAction* pAction = (CAction*)(m_ActionUndoFront[nLast]);
		CString sText;
		AfxFormatString1(sText,IDS_MENU_EDIT_UNDO,pAction->m_sActName);
		pCmdUI->SetText(sText);
		pCmdUI->Enable(TRUE);
	} else
	{
		CString sText; sText.LoadString(IDS_MENU_EDIT_NO_UNDO);
		pCmdUI->SetText(sText);
		pCmdUI->Enable(FALSE);
	}
}
	
BOOL CActionManager::DoRedoAction()
{
	int nLast = m_ActionRedoFront.GetSize()-1;
	if (nLast>=0)
	{
		CAction* pAction = (CAction*)(m_ActionRedoFront[nLast]);
		ASSERT_KINDOF(CAction,pAction);
		// provedu akci
		if (!RealizeAction(pAction))
			return FALSE;
		// zam�n�m hodnoty, vy�ad�m z Redo a d�m do Undo
		VERIFY(pAction->ChangeValues());
		m_ActionRedoFront.RemoveAt(nLast);
		m_ActionUndoFront.Add(pAction);
		// v�e je OK
		return TRUE;
	}
	return FALSE;
}

void CActionManager::OnUpdateRedoAction(CCmdUI* pCmdUI) const
{
	int nLast = m_ActionRedoFront.GetSize()-1;
	if (nLast>=0)
	{
		CAction* pAction = (CAction*)(m_ActionRedoFront[nLast]);
		CString sText;
		AfxFormatString1(sText,IDS_MENU_EDIT_REDO,pAction->m_sActName);
		pCmdUI->SetText(sText);
		pCmdUI->Enable(TRUE);
	} else
	{
		CString sText; sText.LoadString(IDS_MENU_EDIT_NO_REDO);
		pCmdUI->SetText(sText);
		pCmdUI->Enable(FALSE);
	}
}

void CActionManager::OnUpdateDelUndoRedoActions(CCmdUI* pCmdUI) const
{
  pCmdUI->Enable(m_ActionRedoFront.GetSize() > 0 || 
    m_ActionUndoFront.GetSize() > 0);
}

/////////////////////////////////////////////////////////////////////////////
// Trackers - global parametets

static HCURSOR	rghCursors[10];
static HBRUSH	hatchBrush;
static HPEN		blackDottedPen;
static HPEN 	xorDottedPen;
static int		nHandleSize;

HBRUSH CFMntScrollerTracker::GetTrackHatchBrush()
{ return hatchBrush; };
HPEN   CFMntScrollerTracker::GetTrackBlackPen()
{ return blackDottedPen; };
HPEN   CFMntScrollerTracker::GetTrackXorPen()
{ return xorDottedPen; };
int	   CFMntScrollerTracker::GetTrackHandleSize()
{ return nHandleSize; };

IMPLEMENT_DYNAMIC(CFMntScrollerTracker,CObject)

CFMntScrollerTracker::CFMntScrollerTracker(CFMntScrollerView* pScroller)
{
	ASSERT(pScroller!=NULL);
	ASSERT(pScroller->IsKindOf( RUNTIME_CLASS( CFMntScrollerView )));
	m_pScroller = pScroller;

  m_bClipClient = FALSE;
	// inicializace glob. prom�nn�ch
	// do one-time initialization if necessary
	static BOOL bInitialized;

	//MntLockGlobals(MNTCRIT_MNTTRACKERS);
	if (!bInitialized)
	{
		if (hatchBrush == NULL)
		{	// create the hatch pattern + bitmap
			WORD hatchPattern[8];
			WORD wPattern = 0x1111;
			for (int i = 0; i < 4; i++)
			{
				hatchPattern[i] = wPattern;
				hatchPattern[i+4] = wPattern;
				wPattern <<= 1;
			}
			HBITMAP hatchBitmap = CreateBitmap(8, 8, 1, 1, &hatchPattern);
			if (hatchBitmap == NULL)
			{
				AfxThrowResourceException();
			}
			// create black hatched brush
			hatchBrush = CreatePatternBrush(hatchBitmap);
			DeleteObject(hatchBitmap);
			if (hatchBrush == NULL)
			{
				AfxThrowResourceException();
			}
		}
		if (blackDottedPen == NULL)
		{
			// create black dotted pen
			blackDottedPen = CreatePen(PS_DOT, 0, RGB(0, 0, 0));
			if (blackDottedPen == NULL)
			{
				AfxThrowResourceException();
			}
		}
		if (xorDottedPen == NULL)
		{
			// create xor dotted pen
			xorDottedPen = CreatePen(PS_DOT, 0, RGB(255, 255, 255));
			if (xorDottedPen == NULL)
			{
				AfxThrowResourceException();
			}
		}
		// initialize the cursor array
		
		rghCursors[0] = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_MSCR_TRCK_NWSE));
		rghCursors[1] = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_MSCR_TRCK_NESW));
		rghCursors[2] = rghCursors[0];
		rghCursors[3] = rghCursors[1];
		rghCursors[4] = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_MSCR_TRCK_NS));
		rghCursors[5] = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_MSCR_TRCK_WE));
		rghCursors[6] = rghCursors[4];
		rghCursors[7] = rghCursors[5];
		rghCursors[8] = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_MSCR_MOVE));
		rghCursors[9] = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_MSCR_TRCK_4WAY));


		// get default handle size from Windows profile setting
		/*static const TCHAR szWindows[] = _T("windows");
		static const TCHAR szInplaceBorderWidth[] =
			_T("oleinplaceborderwidth");
		nHandleSize = GetProfileInt(szWindows, szInplaceBorderWidth, 4);*/

    nHandleSize = 8;
		// inicalizace se ji� nebude opakovat
		bInitialized = TRUE;
	}
	//MntUnlockGlobals(MNTCRIT_MNTTRACKERS);

	m_oldMouseParam = 0L;
	m_bTracking		= FALSE;
}

// Operations
BOOL CFMntScrollerTracker::SetCursor(CWnd* pWnd, CPoint& point) const
{
	// trackers should only be in client area
	//if (nHitTest != HTCLIENT) return FALSE;
	// convert cursor position to client co-ordinates
	//CPoint point; GetCursorPos(&point);
	//pWnd->ScreenToClient(&point);
	// do hittest and normalize hit
	int nHandle = HitTestHandles(point);
	if (nHandle < 0)
		return FALSE;
	// need to normalize the hittest such that we get proper cursors
	nHandle = NormalizeHit(nHandle);
	if (m_bLineTracker)
	{
		if (nHandle == hitAllLine) nHandle = (TrackerHit)8;
		else nHandle = (TrackerHit)9;
	}
	ASSERT(nHandle < sizeof(rghCursors)/sizeof(rghCursors[0]));  

  if (nHandle == hitNothing)
    return FALSE;
  else
	  ::SetCursor(rghCursors[nHandle]);

	return TRUE;
}

BOOL CFMntScrollerTracker::OnMouseMessage(int nHandle,UINT nMsg, WPARAM wParam, LPARAM lParam, CWnd* pWnd, CWnd* pWndClipTo)
{
	// ulo��m nov� sou�adnice
	m_oldMouseParam = lParam;
	m_oldHandle		= nHandle;	
	
	return TRUE;	
}

void CFMntScrollerTracker::OnAutoScrollTimer()
{
	m_bAutoScrTimer = FALSE; // povoluji auto-scroll
	// opakuji sou�adnici my�i
	//if (m_oldMouseParam != 0)
	//	OnMouseMessage(m_oldHandle,WM_MOUSEMOVE, 0, m_oldMouseParam,m_pScroller,m_pClipWnd);
}

// modifications by scroller
void CFMntScrollerTracker::SetTrackParams(WORD nStyle,BOOL bClipClient, AlignMode eAlignGrid, 
                                          const CPoint& alignSquareSizeLog , const CPoint& alignSquareOffsetLog)
{
	m_nStyle 	  = nStyle;
	m_bClipClient = bClipClient;
	m_eAlignGrid  = eAlignGrid;
  m_alignSquareSizeLog = alignSquareSizeLog;
  m_alignSquareOffsetLog = alignSquareOffsetLog;

	// v�e se sm� odehr�vat pouze v client plo�e m_pScroller
	m_rectClip  = m_pScroller->FromScrollToDevicePosition(m_pScroller->GetLogClientRect());
	m_rectClip -= m_pScroller->GetDeviceScrollPosition();
}

BOOL CFMntScrollerTracker::OnAutoScroll(CPoint& mouse,BOOL bTest)
{

	// je-li �as & je povolen
	if ((!m_bAutoScrTimer)&&(m_pScroller->m_bAutoScroll)&&(!m_bClipClient))
	{	// uchov�m p�vodn� view origin
		CPoint oldDevOrg = m_pScroller->GetDeviceScrollPosition();
    CRect clientRect;
    m_pScroller->GetClientRect(clientRect);

		// jestli�e je tento bod mimo m_rectClip - scroll window
		if (mouse.x < clientRect.left)
		{
			if (bTest) return TRUE;
			m_pScroller->OnScrollBar(SB_HORZ, SB_LINEUP, 0);
			m_bAutoScrTimer = TRUE;
		}
		else if (mouse.x > clientRect.right)
		{
			if (bTest) return TRUE;
			m_pScroller->OnScrollBar(SB_HORZ, SB_LINEDOWN, 0);
			m_bAutoScrTimer = TRUE;
		}
		if (mouse.y < clientRect.top)
		{
			if (bTest) return TRUE;
			m_pScroller->OnScrollBar(SB_VERT, SB_LINEUP, 0);
			m_bAutoScrTimer = TRUE;
		}
		else if (mouse.y > clientRect.bottom)
		{
			if (bTest) return TRUE;
			m_pScroller->OnScrollBar(SB_VERT, SB_LINEDOWN, 0);
			m_bAutoScrTimer = TRUE;
		}
		// rozd�l oproti sou�asn�mu view origin
		oldDevOrg -= (CSize)(m_pScroller->GetDeviceScrollPosition());
		// zm�nilo se n�co ???
		if ((oldDevOrg.x!=0)||(oldDevOrg.y!=0))
		{
			// pokud je�t� nen� upraven
			if ((m_pScroller->m_pEditInfo == NULL)||
			    (m_pScroller->m_pEditInfo->m_pSelTracker != this))
			{ // uprav�m zpracov�van� dev. sou�adnice
				ScrollTracker(oldDevOrg.x,oldDevOrg.y);
			} // jinak je ji� upraven p�i OnScrollBar
			// update m_rectClip
			m_rectClip  = m_pScroller->FromScrollToDevicePosition(m_pScroller->GetLogClientRect());
			m_rectClip -= m_pScroller->GetDeviceScrollPosition();
			return TRUE;
		}
	}

	return FALSE;
}

void CFMntScrollerTracker::FromSrollerToTracker(CRect& dest,const CRect& src) const
{
	dest  = m_pScroller->FromScrollToDevicePosition(src);
	dest -= m_pScroller->GetDeviceScrollPosition();
}

































/////////////////////////////////////////////////////////////////////////////
// CFMntScrollerTrackerRect

CFMntScrollerTrackerRect::CFMntScrollerTrackerRect(CFMntScrollerView* pScroller):
	CFMntScrollerTracker(pScroller)
{
	m_bLineTracker = FALSE;
	m_rectLast.SetRectEmpty();
	m_sizeLast.cx = m_sizeLast.cy = 0;
	m_bErase = FALSE;
	m_bFinalErase =  FALSE;
}

CFMntScrollerTrackerRect::~CFMntScrollerTrackerRect()
{
};

// Operations
BOOL CFMntScrollerTrackerRect::TrackRubberBand(CWnd* pWnd, CPoint point, BOOL bAllowInvert)
{
	// zarovn�m po��te�n� bod
	switch (m_eAlignGrid)
  {
  case fstAlignGrid:
    point = m_pScroller->AlignToGrid(point,m_pScroller->GetLogClientRect());
    break;
  case fstAlignSquare:
    point = m_pScroller->AlignToStep(point,m_alignSquareSizeLog, m_alignSquareOffsetLog, m_pScroller->GetLogClientRect());
    break;
  /*case fstAlignVertex:
    point = m_pScroller->AlignToStep(point,CPoint(VIEW_LOG_UNIT_SIZE, VIEW_LOG_UNIT_SIZE),
      CPoint(VIEW_LOG_UNIT_SIZE/2, VIEW_LOG_UNIT_SIZE/2), m_pScroller->GetLogClientRect());
    break;*/
  }
		

	// point je v log. sou�adnic�ch
	m_finalRect.left = m_finalRect.right = point.x;
	m_finalRect.top  = m_finalRect.bottom= point.y;
	// p�iprav�m pracovn� Recty
	m_logRect  = m_finalRect;
	FromSrollerToTracker(m_devRect,m_finalRect);
	// simply call helper function to track from bottom right handle
	m_bAllowInvert = bAllowInvert;
	CPoint pt(m_devRect.right,m_devRect.bottom);
	// provedu vlastn� track
	if (TrackHandle(hitBottomRight, pWnd, pt, NULL))
	{	// m_logRect mus� b�t aktualizov�n pr�b�n�
		m_logRect.NormalizeRect();
		//if (!(m_finalRect.EqualRect(&m_logRect)))
		{
			m_finalRect.CopyRect(m_logRect);
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CFMntScrollerTrackerRect::Track(CWnd* pWnd, CPoint point, BOOL bAllowInvert,
						   		    CWnd* pWndClipTo)
{
	// p�iprav�m pracovn� Recty
	m_logRect  = m_finalRect;
	FromSrollerToTracker(m_devRect,m_finalRect);
	// perform hit testing on the handles
	int nHandle = HitTestHandles(point);
	if (nHandle < 0)
	{
		// didn't hit a handle, so just return FALSE
		return FALSE;
	}
	// otherwise, call helper function to do the tracking
	m_bAllowInvert = bAllowInvert;
	// provedu vlastn� track
	if (TrackHandle(nHandle, pWnd, point, pWndClipTo))
	{	// m_logRect mus� b�t aktualizov�n pr�b�n�
		m_logRect.NormalizeRect();
		if (!(m_finalRect.EqualRect(&m_logRect)))
		{
			m_finalRect.CopyRect(m_logRect);
			return TRUE;
		}
	} else
	{	// vr�t�m p�vodn� rect
		m_logRect  = m_finalRect;
		FromSrollerToTracker(m_devRect,m_finalRect);
	}
	return FALSE;
}

void CFMntScrollerTrackerRect::GetTrueRect(LPRECT lpTrueRect) const
{
	ASSERT(AfxIsValidAddress(lpTrueRect, sizeof(RECT)));
	// v�dy pouze z final rect
	CRect rect;
	if (m_bTracking) 
    FromSrollerToTracker(rect,m_finalRect);
	else 
    rect = m_devRect;

	rect.NormalizeRect();
	int nInflateBy = 0;
	if ((m_nStyle & (resizeOutside|hatchedBorder)) != 0)
		nInflateBy += GetHandleSize() - 1;
	if ((m_nStyle & (solidLine|dottedLine)) != 0)
		++nInflateBy;
	rect.InflateRect(nInflateBy, nInflateBy);
	*lpTrueRect = rect;
}

int  CFMntScrollerTrackerRect::NormalizeHit(int nHandle) const
{
	ASSERT(nHandle <= 8 && nHandle >= -1);
	if (nHandle == hitMiddle || nHandle == hitNothing)
		return nHandle;
	const AFX_HANDLEINFO* pHandleInfo = &rgHandleInfo[nHandle];
	if (m_devRect.Width() < 0)
	{
		nHandle = (TrackerHit)pHandleInfo->nInvertX;
		pHandleInfo = &rgHandleInfo[nHandle];
	}
	if (m_devRect.Height() < 0)
		nHandle = (TrackerHit)pHandleInfo->nInvertY;
	return nHandle;
}

void CFMntScrollerTrackerRect::ScrollTracker(int xAmount,int yAmount)
{
	m_devRect.OffsetRect(xAmount,yAmount);
}

void CFMntScrollerTrackerRect::SetRect(LPCRECT pRect)
{
	m_finalRect.CopyRect(pRect);
	FromSrollerToTracker(m_devRect,m_finalRect);
}

void CFMntScrollerTrackerRect::Draw(CDC* pDC)
{
	// set initial DC state
	int nSaveDC = pDC->SaveDC();
	ASSERT(nSaveDC != 0);
	pDC->SetMapMode(MM_TEXT);
	pDC->SetViewportOrg(0, 0);
	pDC->SetWindowOrg(0, 0);
	pDC->SetBkMode(OPAQUE);
	pDC->SetBkColor(RGB(255,255,255));

	// v�dy pouze z final rect
	CRect rect;
	if (m_bTracking) FromSrollerToTracker(rect,m_finalRect);
	else rect = m_devRect;
	rect.NormalizeRect();

	CPen* pOldPen = NULL;
	CBrush* pOldBrush = NULL;
	CGdiObject* pTemp;
	int nOldROP;

	// draw lines
	if ((m_nStyle & (dottedLine|solidLine)) != 0)
	{
		if (m_nStyle & dottedLine)
			pOldPen = pDC->SelectObject(CPen::FromHandle(blackDottedPen));
		else
			pOldPen = (CPen*)pDC->SelectStockObject(BLACK_PEN);
		pOldBrush = (CBrush*)pDC->SelectStockObject(NULL_BRUSH);
		nOldROP = pDC->SetROP2(R2_COPYPEN);
		rect.InflateRect(+1, +1);   // borders are one pixel outside
		pDC->Rectangle(rect.left, rect.top, rect.right, rect.bottom);
		pDC->SetROP2(nOldROP);
	}

	// if hatchBrush is going to be used, need to unrealize it
	if ((m_nStyle & (hatchInside|hatchedBorder)) != 0)
		UnrealizeObject(hatchBrush);

	// hatch inside
	if ((m_nStyle & hatchInside) != 0)
	{
		pTemp = pDC->SelectStockObject(NULL_PEN);
		if (pOldPen == NULL)
			pOldPen = (CPen*)pTemp;
		pTemp = pDC->SelectObject(CBrush::FromHandle(hatchBrush));
		if (pOldBrush == NULL)
			pOldBrush = (CBrush*)pTemp;
		pDC->SetBkMode(TRANSPARENT);
		nOldROP = pDC->SetROP2(R2_MASKNOTPEN);
		pDC->Rectangle(rect.left+1, rect.top+1, rect.right, rect.bottom);
		pDC->SetROP2(nOldROP);
	}

	// draw hatched border
	if ((m_nStyle & hatchedBorder) != 0)
	{
		pTemp = pDC->SelectObject(CBrush::FromHandle(hatchBrush));
		if (pOldBrush == NULL)
			pOldBrush = (CBrush*)pTemp;
		pDC->SetBkMode(OPAQUE);
		CRect rectTrue;
		GetTrueRect(&rectTrue);
		pDC->PatBlt(rectTrue.left, rectTrue.top, rectTrue.Width(),
			rect.top-rectTrue.top, 0x000F0001 /* Pn */);
		pDC->PatBlt(rectTrue.left, rect.bottom,
			rectTrue.Width(), rectTrue.bottom-rect.bottom, 0x000F0001 /* Pn */);
		pDC->PatBlt(rectTrue.left, rect.top, rect.left-rectTrue.left,
			rect.Height(), 0x000F0001 /* Pn */);
		pDC->PatBlt(rect.right, rect.top, rectTrue.right-rect.right,
			rect.Height(), 0x000F0001 /* Pn */);
	}

	// draw resize handles
	if ((m_nStyle & (resizeInside|resizeOutside)) != 0)
	{
		UINT mask = GetHandleMask();
		for (int i = 0; i < 8; ++i)
		{
			if (mask & (1<<i))
			{
				GetHandleRect((TrackerHit)i, &rect);
				pDC->FillSolidRect(rect, m_pScroller->m_wndColor ^ 0xFFFFFFFF);
			}
		}
	}
	// cleanup pDC state
	if (pOldPen != NULL)
		pDC->SelectObject(pOldPen);
	if (pOldBrush != NULL)
		pDC->SelectObject(pOldBrush);
	VERIFY(pDC->RestoreDC(nSaveDC));
}

int CFMntScrollerTrackerRect::HitTest(CPoint point) const
{
	TrackerHit hitResult = hitNothing;

	CRect rectTrue;
	GetTrueRect(&rectTrue);
	ASSERT(rectTrue.left <= rectTrue.right);
	ASSERT(rectTrue.top <= rectTrue.bottom);
	if (rectTrue.PtInRect(point))
	{
		if ((m_nStyle & (resizeInside|resizeOutside)) != 0)
			hitResult = (TrackerHit)HitTestHandles(point);
		else
			hitResult = hitMiddle;
	}
	return hitResult;
}


void CFMntScrollerTrackerRect::DrawTrackerRect(
	LPCRECT lpRect, CWnd* pWndClipTo, CDC* pDC, CWnd* pWnd)
{
	// first, normalize the rectangle for drawing
	CRect rect = *lpRect;
	rect.NormalizeRect();
	// convert to client coordinates
	if (pWndClipTo != NULL)
	{
		pWnd->ClientToScreen(&rect);
		pWndClipTo->ScreenToClient(&rect);
	}
	CSize size(0, 0);
	if (!m_bFinalErase && rect.Width() > MAX_MOUSE_CLICK_MOVE && rect.Height() > MAX_MOUSE_CLICK_MOVE)
	{
		// otherwise, size depends on the style
		if (m_nStyle & hatchedBorder)
		{
			size.cx = size.cy = max(1, GetHandleSize(rect)-1);
			rect.InflateRect(size);
		}
		else
		{
			size.cx = 1; // CX_BORDER
			size.cy = 1; // CY_BORDER
		}
	}
	// and draw it
	if (m_bFinalErase || !m_bErase )
		pDC->DrawDragRect(rect, size, m_rectLast, m_sizeLast);
	// remember last rectangles
	m_rectLast = rect;
	m_sizeLast = size;
}

// return TRUE if continue
BOOL CFMntScrollerTrackerRect::OnMouseMessage(int nHandle,UINT nMsg, WPARAM wParam, LPARAM lParam, CWnd* pWnd, CWnd* pWndClipTo) 
{
	CFMntScrollerTracker::OnMouseMessage(nHandle,nMsg,wParam, lParam,pWnd,pWndClipTo);
	if ((nMsg==WM_LBUTTONUP)||(nMsg==WM_MOUSEMOVE))
	{
		CPoint  mouse((int)(short)LOWORD(lParam),
					  (int)(short)HIWORD(lParam));
		// handle movement/accept messages
		// try autoscroll first
		{
			if (OnAutoScroll(mouse))
			{	// hide rect first
				if (m_bMoved)
				{
					m_bErase = m_bFinalErase = TRUE;
					DrawTrackerRect(&m_devRect, pWndClipTo, m_pDrawDC, pWnd);
					m_bFinalErase = FALSE;
				}
				// auto-scroll
				OnAutoScroll(mouse);
				if (m_bMoved)
				{
					m_bErase = FALSE;
					DrawTrackerRect(&m_devRect, pWndClipTo, m_pDrawDC, pWnd);
				}
			}
		}
		// normal handle mouse event
		CRect rectOld = m_devRect;
		// handle resize cases (and part of move)
		if (m_px != NULL)
			*m_px = mouse.x - m_xDiff;
		if (m_py != NULL)
			*m_py = mouse.y - m_yDiff;
		// handle move case
		if (nHandle == hitMiddle)
		{
			m_devRect.right = m_devRect.left + m_nWidth;
			m_devRect.bottom = m_devRect.top + m_nHeight;
		}
		// allow caller to adjust the rectangle if necessary
		AdjustRect(nHandle);
		// only redraw and callback if the rect actually changed!
		m_bFinalErase = (nMsg == WM_LBUTTONUP);
		if (!rectOld.EqualRect(&m_devRect) || m_bFinalErase)
		{
			if (m_bMoved)
			{
				m_bErase = TRUE;
				DrawTrackerRect(&rectOld, pWndClipTo, m_pDrawDC, pWnd);
			}
			if (nMsg != WM_LBUTTONUP)
				m_bMoved = TRUE;
		}
		if (m_bFinalErase)
			return FALSE;
		if (!rectOld.EqualRect(&m_devRect))
		{
			m_bErase = FALSE;
			DrawTrackerRect(&m_devRect, pWndClipTo, m_pDrawDC, pWnd);
		}
	}
	// pokra�ovat ve zpracov�n�
	return TRUE;
}

BOOL CFMntScrollerTrackerRect::TrackHandle(int nHandle, CWnd* pWnd, CPoint point,
	CWnd* pWndClipTo)
{
	ASSERT(m_pScroller==pWnd);
	m_pClipWnd = pWndClipTo;
	m_oldMouseParam = 0L;	// no auto-scroll before

	ASSERT(nHandle >= 0);
	ASSERT(nHandle <= 8);   // handle 8 is inside the rect
	// don't handle if capture already set
	if ((::GetCapture() != NULL)&&
		(::GetCapture() != pWnd->m_hWnd)) return FALSE;
	ASSERT(!m_bFinalErase);
	// save original width & height in pixels
	m_nWidth = m_devRect.Width();
	m_nHeight = m_devRect.Height();
	// set capture to the window which received this message
	pWnd->SetCapture();
	ASSERT(pWnd == CWnd::GetCapture());
	pWnd->UpdateWindow();
	if (pWndClipTo != NULL)
		pWndClipTo->UpdateWindow();
	// find out what x/y coords we are supposed to modify
	GetModifyPointers(nHandle, &m_px, &m_py, &m_xDiff, &m_yDiff);
	m_xDiff = point.x - m_xDiff;
	m_yDiff = point.y - m_yDiff;
	// get DC for drawing
	if (pWndClipTo != NULL)
	{	// clip to arbitrary window by using adjusted Window DC
		m_pDrawDC = pWndClipTo->GetDCEx(NULL, DCX_CACHE);
	} else
	{ 	// otherwise, just use normal DC
		m_pDrawDC = pWnd->GetDC();
	}
	ASSERT_VALID(m_pDrawDC);

	m_bMoved 	= FALSE;
	m_bTracking = TRUE;
	BOOL bCancel= FALSE;
	// get messages until capture lost or cancelled/accepted
	for (;;)
	{
		MSG msg;
		VERIFY(::GetMessageA(&msg, NULL, 0, 0));
		if (CWnd::GetCapture() != pWnd)
			break;
		switch (msg.message)
		{
		// handle movement/accept messages
		case WM_LBUTTONUP:
		case WM_MOUSEMOVE:    
			if (!OnMouseMessage(nHandle,msg.message,msg.wParam,msg.lParam,pWnd,pWndClipTo))
				goto ExitLoop;
			break;
		// handle cancel messages
		case WM_KEYDOWN:
			if (msg.wParam != VK_ESCAPE)
				break;
		case WM_RBUTTONDOWN:
			if (m_bMoved)
			{
				m_bErase = m_bFinalErase = TRUE;
				DrawTrackerRect(&m_devRect, pWndClipTo, m_pDrawDC, pWnd);
			}
			bCancel = TRUE;
			goto ExitLoop;
		// just dispatch rest of the messages
		default:
			DispatchMessage(&msg);
			break;
		}
	}
ExitLoop:
	if (pWndClipTo != NULL)
		pWndClipTo->ReleaseDC(m_pDrawDC);
	else
		pWnd->ReleaseDC(m_pDrawDC);
	ReleaseCapture();
	// result
	m_bTracking	  = FALSE;
	m_bFinalErase = FALSE;
	return (!bCancel);
}

void CFMntScrollerTrackerRect::AdjustRect(int nHandle)
{
	// rect pro o��znut�
	CRect clipRect = m_pScroller->GetLogClientRect();
	// zarovn�n� na grid ?
	//BOOL  bGridAlign = ((m_bAlignGrid)&&(m_pScroller->m_bAlignGrid)); TODO
	// p�evedu Rect na logick� sou�adnice
	CRect rect(m_devRect);
	rect += m_pScroller->GetDeviceScrollPosition();
	rect =  m_pScroller->FromDeviceToScrollPosition(rect);
	// sou�adnice, kter� se nem�n� z�stanou z m_logRect
	switch(nHandle)
	{
		case CFMntScrollerTracker::hitTop:
			rect.left  = m_logRect.left;
		case CFMntScrollerTracker::hitTopLeft:
		{
			rect.right  = m_logRect.right;
			rect.bottom = m_logRect.bottom;
			break;
		}
		case CFMntScrollerTracker::hitRight:
			rect.top   = m_logRect.top;
		case CFMntScrollerTracker::hitTopRight:
		{
			rect.left   = m_logRect.left;
			rect.bottom = m_logRect.bottom;
			break;
		}
		case CFMntScrollerTracker::hitLeft:
			rect.bottom = m_logRect.bottom;
		case CFMntScrollerTracker::hitBottomLeft:
		{
			rect.right = m_logRect.right;
			rect.top   = m_logRect.top;
			break;
		}
		case CFMntScrollerTracker::hitBottom:
			rect.right = m_logRect.right;
		case CFMntScrollerTracker::hitBottomRight:
		{
			rect.left = m_logRect.left;
			rect.top  = m_logRect.top;
			switch (m_eAlignGrid)
			{
      case fstAlignGrid:
        {
				  CPoint ptRB = m_pScroller->AlignToGrid(rect.BottomRight(),m_pScroller->GetLogClientRect());
				  rect.right  = ptRB.x;
				  rect.bottom = ptRB.y;
          break;
        }
      case fstAlignSquare:
        {
          CPoint ptRB = m_pScroller->AlignToStep(rect.BottomRight(), m_alignSquareSizeLog, m_alignSquareOffsetLog,m_pScroller->GetLogClientRect());
          rect.right  = ptRB.x;
          rect.bottom = ptRB.y;
          break;
        }
      /*case fstAlignVertex:
        {
          CPoint ptRB = m_pScroller->AlignToStep(rect.BottomRight(),CPoint(VIEW_LOG_UNIT_SIZE, VIEW_LOG_UNIT_SIZE)
            ,CPoint(VIEW_LOG_UNIT_SIZE/2, VIEW_LOG_UNIT_SIZE/2),m_pScroller->GetLogClientRect());
          rect.right  = ptRB.x;
          rect.bottom = ptRB.y;
          break;
        }*/
      }
			break;
		}
		case CFMntScrollerTracker::hitMiddle:
		{
			rect.right  = rect.left + m_logRect.Width();
			rect.bottom = rect.top  + m_logRect.Height();
			break;
		}
	}
	// vol�m zarovn�n� pro objekt
	m_pScroller->AdjustTrackRect(this,nHandle,rect,clipRect,
								 m_eAlignGrid ,m_bAllowInvert);
	// p�i�ad�m nov� m_logRect
	m_logRect = rect;
	// p�evedu rect zp�t a p�i�ad�m
	rect = m_pScroller->FromScrollToDevicePosition(rect);
	rect -= m_pScroller->GetDeviceScrollPosition();
	m_devRect.CopyRect(rect);
}

int  CFMntScrollerTrackerRect::HitTestHandles(CPoint point) const
{
	CRect rect;
	UINT mask = GetHandleMask();
	// see if hit anywhere inside the tracker
	GetTrueRect(&rect);
	if (!rect.PtInRect(point))
		return hitNothing;  // totally missed
	// see if we hit a handle
	for (int i = 0; i < 8; ++i)
	{
		if (mask & (1<<i))
		{
			GetHandleRect((TrackerHit)i, &rect);
			if (rect.PtInRect(point))
				return (TrackerHit)i;
		}
	}
	// last of all, check for non-hit outside of object, between resize handles
	if ((m_nStyle & hatchedBorder) == 0)
	{
		CRect rect = m_devRect;
		rect.NormalizeRect();
		if ((m_nStyle & dottedLine|solidLine) != 0)
			rect.InflateRect(+1, +1);
		if (!rect.PtInRect(point))
			return hitNothing;  // must have been between resize handles
	}
	return hitMiddle;   // no handle hit, but hit object (or object border)
}

UINT CFMntScrollerTrackerRect::GetHandleMask() const
{
	UINT mask = 0x0F;   // always have 4 corner handles
	int size = nHandleSize*3;
	// je-li outside, sta�� pouze, bay se ve�el prost�edn�
	if (m_nStyle & resizeOutside)
		size = nHandleSize;
	if (abs(m_devRect.Width()) - size > 4)
		mask |= 0x50;
	if (abs(m_devRect.Height()) - size > 4)
		mask |= 0xA0;
	return mask;
}

void CFMntScrollerTrackerRect::GetHandleRect(int nHandle, CRect* pHandleRect) const
{
	ASSERT(nHandle < 8);
	// get normalized rectangle of the tracker
	// v�dy pouze z final rect
	CRect rectT;
	if (m_bTracking) FromSrollerToTracker(rectT,m_finalRect);
	else rectT = m_devRect;
	rectT.NormalizeRect();
	if ((m_nStyle & (solidLine|dottedLine)) != 0)
		rectT.InflateRect(+1, +1);
	// since the rectangle itself was normalized, we also have to invert the
	//  resize handles.
	nHandle = NormalizeHit(nHandle);
	// handle case of resize handles outside the tracker
	int size = GetHandleSize();
	if (m_nStyle & resizeOutside)
		rectT.InflateRect(size-1, size-1);
	// calculate position of the resize handle
	int nWidth = rectT.Width();
	int nHeight = rectT.Height();

	CRect rect;
	const AFX_HANDLEINFO* pHandleInfo = &rgHandleInfo[nHandle];
	rect.left = *(int*)((BYTE*)&rectT + pHandleInfo->nOffsetX);
	rect.top = *(int*)((BYTE*)&rectT + pHandleInfo->nOffsetY);
	rect.left += size * pHandleInfo->nHandleX;
	rect.top += size * pHandleInfo->nHandleY;
	//rect.left += pHandleInfo->nCenterX * (nWidth - size) / 2;
	//rect.top += pHandleInfo->nCenterY * (nHeight - size) / 2;
	rect.right = rect.left + size + pHandleInfo->nCenterX * (nWidth - 3 * size);
	rect.bottom = rect.top + size + pHandleInfo->nCenterY * (nHeight - 3 * size);
	*pHandleRect = rect;
}

void CFMntScrollerTrackerRect::GetModifyPointers(int nHandle, int**ppx, int**ppy, int* px, int*py)
{
	ASSERT(nHandle >= 0);
	ASSERT(nHandle <= 8);
	if (nHandle == hitMiddle)
		nHandle = hitTopLeft;   // same as hitting top-left
	*ppx = NULL;
	*ppy = NULL;
	// fill in the part of the rect that this handle modifies
	//  (Note: handles that map to themselves along a given axis when that
	//   axis is inverted don't modify the value on that axis)
	const AFX_HANDLEINFO* pHandleInfo = &rgHandleInfo[nHandle];
	if (pHandleInfo->nInvertX != nHandle)
	{
		*ppx = (int*)((BYTE*)&m_devRect + pHandleInfo->nOffsetX);
		if (px != NULL)
			*px = **ppx;
	}
	else
	{
		// middle handle on X axis
		if (px != NULL)
			*px = m_devRect.left + abs(m_devRect.Width()) / 2;
	}
	if (pHandleInfo->nInvertY != nHandle)
	{
		*ppy = (int*)((BYTE*)&m_devRect + pHandleInfo->nOffsetY);
		if (py != NULL)
			*py = **ppy;
	}
	else
	{
		// middle handle on Y axis
		if (py != NULL)
			*py = m_devRect.top + abs(m_devRect.Height()) / 2;
	}
}

int  CFMntScrollerTrackerRect::GetHandleSize(LPCRECT lpRect) const
{
	if (lpRect == NULL)
		lpRect = &m_devRect;
	int size = nHandleSize;
	if (!(m_nStyle & resizeOutside))
	{
		// make sure size is small enough for the size of the rect
		int sizeMax = min(abs(lpRect->right - lpRect->left),
			abs(lpRect->bottom - lpRect->top));
		if (size * 2 > sizeMax)
			size = sizeMax / 2;
	}
	return size;
}






























































/////////////////////////////////////////////////////////////////////////////
// Parametry pro podporu pr�ce se scrollerem - EDITACE

IMPLEMENT_DYNAMIC(CFMntScrollEditParams, CObject)

CFMntScrollEditParams::CFMntScrollEditParams()
{	m_pSelObject = NULL; };
CFMntScrollEditParams::~CFMntScrollEditParams()
{
};

/////////////////////////////////////////////////////////////////////////////
// Parametry pro podporu pr�ce se scrollerem - EDITACE

IMPLEMENT_DYNCREATE(CFMntScrollEditInfo, CObject)

CFMntScrollEditInfo::CFMntScrollEditInfo()
{	m_pSelObject	= NULL; 
	m_pSelTracker	= NULL;
	m_bAutoScroll	= TRUE;
};
CFMntScrollEditInfo::~CFMntScrollEditInfo()
{
	if (m_pSelTracker)
		delete m_pSelTracker;
};

// TRACKER pro objekt
// podle parametr� objektu nastav� tracker
void CFMntScrollEditInfo::CreateTracker(CFMntScrollerView* pScroller,BOOL bDraw)
{
	// POTOMEK MUSI NASTAVIT TRACKER PODLE OBJEKTU

	// vykresl�m tracker
	if ((bDraw)&&(m_pSelTracker))
	{
		CDC* pDrawDC = pScroller->GetDC();
		m_pSelTracker->Draw(pDrawDC);
		pScroller->ReleaseDC(pDrawDC);
	}
};

// podle parametr� objektu obnov� tracker
void CFMntScrollEditInfo::ScrollTracker(int xAmount,int yAmount)
{
	if (m_pSelTracker) 
		m_pSelTracker->ScrollTracker(xAmount,yAmount);
};

// zru�� nastaven� Tracker
void CFMntScrollEditInfo::DestroyTracker(CFMntScrollerView* pScroller,BOOL bDraw)
{
  
	if (m_pSelTracker)
	{ 	// ukryji tracker
    CRect rRect; m_pSelTracker->GetTrueRect(rRect);
		delete m_pSelTracker;
		m_pSelTracker = NULL;
			 	
		// p�ekresl�m max. z trackeru a objektu
		//CRect rect = pScroller->FromScrollToDevicePosition(m_rPosition);
		//rect  -= pScroller->GetDeviceScrollPosition();
		//rect.UnionRect(rect,rRect);
    // enlargeb to be sure that whole object was redrawen
    rRect.InflateRect(rRect.Width()/10 + 10, rRect.Height()/10 +10);
		
		if (bDraw)
			pScroller->InvalidateRect(rRect,TRUE);
	}
};

// testuji, zda objekt m� sm� prov�d�t danou operaci
BOOL CFMntScrollEditInfo::CanObjectHit(int nHandle) const
{	// def. povolen hit, kter� generuje tracker
	return TRUE;
};

// tracking object
BOOL CFMntScrollEditInfo::TrackObject(CFMntScrollerView* pView,CPoint ptAt)
{		
	return m_pSelTracker->Track(pView,ptAt);
};

// obnova objektu pro Tracking
void CFMntScrollEditInfo::UpdateOnTrack(CFMntScrollerView* pView)
{
	// vol�m obnovu objektu
	BOOL bDraw = pView->EOObjectFromTracker();
	// obnova na nov� parametry objektu
	//pView->UpdateEditChanges(FALSE);
};

// max. rect mezi objektem a trackerem (v log.)
void CFMntScrollEditInfo::GetLogTrueRect(CFMntScrollerView* pScroller,CRect& rRect)
{
	m_pSelTracker->GetTrueRect(rRect);
	rRect += pScroller->GetDeviceScrollPosition();
	rRect =  pScroller->FromDeviceToScrollPosition(rRect);
	//rRect.UnionRect(rRect,m_rPosition);
};



// reakce na zm�ny objektu (p�etvo�en� trackeru)
void CFMntScrollEditInfo::UpdateObjectChanges(CFMntScrollerView* pView,BOOL bDelete, BOOL bDraw)
{
	// zru��m tracker 
	DestroyTracker(pView,bDraw);

	if (!bDelete)
	{
		// dotaz na parametry objektu
		pView->EOUpdateInfo();
		// obnov�m tracker na novou pozici - jesli�e byl objekt vykreslen, pak kresl�m i tracker
		CreateTracker(pView,!bDraw);
	}
};

void CFMntScrollEditInfo::OnChangeDP(CFMntScrollerView* pView)
{
  UpdateObjectChanges(pView, FALSE);
}


/////////////////////////////////////////////////////////////////////////////
// Parametry pro podporu udpate view

IMPLEMENT_DYNAMIC(CFMntScrollUpdate, CObject)

CFMntScrollUpdate::CFMntScrollUpdate(DWORD nType,CView* pView)
{
	m_nType		= nType;
	m_pSender	= pView;
	m_pParent	= (pView != NULL)?pView->GetParent():NULL;
	
	// def. parametry
	m_pView		= NULL;
	m_pObject	= NULL;
	m_rPlace.SetRect(0,0,0,0);
	m_nParam	= 0;
	m_nParam1	= 
	m_nParam2	= 0L;
	// p��znak realizace
	m_bRealize	= FALSE;
};
CFMntScrollUpdate::~CFMntScrollUpdate()
{
};

/////////////////////////////////////////////////////////////////////////////
// CFMntScrollerView

IMPLEMENT_DYNAMIC(CFMntScrollerView, CView)

const SIZE CFMntScrollerView::sizePgDefault = {90,90};
const SIZE CFMntScrollerView::sizeLnDefault = {10,10};

CFMntScrollerView::CFMntScrollerView()
{
	// Init everything to zero
	AFX_ZERO_INIT_OBJECT(CView);
	m_nMapMode     = MM_NONE;
	m_nRealMapMode = MM_NONE;  
	m_nScale 	   = SS_NOSCALE;
	m_nMouseMode   = MM_NORMAL;

	// def. sm� prov�d�t Auto-scroll
	m_bAutoScroll = TRUE;

	// parametry m��ky - no grid by default
	m_bShowGrid = m_bAlignGrid = FALSE;
	m_gridStep.cx = m_gridStep.cy = 10;

	// parametry nejsou nastaveny
	m_bEditSupport = FALSE;
	m_pEditParams  = NULL;
	m_pEditInfo	   = NULL;
	m_pEInfoClass  = RUNTIME_CLASS(CFMntScrollEditInfo);
}

CFMntScrollerView::~CFMntScrollerView()
{
	if (m_pEditInfo) delete m_pEditInfo;
}

BEGIN_MESSAGE_MAP(CFMntScrollerView, CView)
	//{{AFX_MSG_MAP(CFMntScrollerView)
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_SIZE()
	ON_WM_KEYDOWN()
	ON_WM_LBUTTONDOWN()
	ON_WM_RBUTTONDOWN()
  ON_WM_RBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_TIMER()
	ON_WM_SETCURSOR()
	ON_WM_ERASEBKGND()
	ON_WM_CANCELMODE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFMntScrollerView update

void CFMntScrollerView::InitialUpdateMetrics()
{
	// def. barva pozad� a m��ky
	m_wndColor  = GetSysColor(COLOR_WINDOW) & 0x00FFFFFF;
	m_gridColor = m_wndColor ^ 0xFF00FFFF;
	// def. nastaven� zobrazen�
	m_rAreaLog.SetRect(0,0,1000,1000);
	m_nScale	= 100;
	//SetScrollSizes(MM_LOMETRIC);
	SetScrollSizes(MM_TEXT);
	SetScrollScale(SS_ISOTROPICFIT,SS_NOSCALE);
	// nastaven� m��ky
	SetGrid(m_bShowGrid, m_bAlignGrid, m_gridStep);
};

void CFMntScrollerView::CopySplitViewMetrics(CFMntScrollerView* pPane)
{
  m_nScale		= pPane->m_nScale;
	//m_nMinScale		= pPane->m_nMinScale;
	m_nMaxScale		= pPane->m_nMaxScale;

	m_rAreaLog		= pPane->m_rAreaLog;
  m_nMapMode		= pPane->m_nMapMode;
  m_nRealMapMode	= pPane->m_nRealMapMode;
	m_bCenterInView	= pPane->m_bCenterInView;

	m_szTotalDev	= pPane->m_szTotalDev;
	m_szTotalRDev	= pPane->m_szTotalRDev;
	
	m_szPageDev		= pPane->m_szPageDev;
	m_szLineDev		= pPane->m_szLineDev;

	m_bInsideUpdate	= pPane->m_bInsideUpdate;

	//m_nZoomHistory	= pPane->m_nZoomHistory;
	//for(int i=0; i<NUM_ZOOM_HISTORY; i++)
	//	m_sHistory[i] = pPane->m_sHistory[i];

	m_bAutoScroll	= pPane->m_bAutoScroll;
	m_pScrollTracker= NULL;

	m_nMouseMode	= pPane->m_nMouseMode;
	m_bValidObjRect	= pPane->m_bValidObjRect;
	m_rLastPos		= pPane->m_rLastPos;
	
	m_wndColor		= pPane->m_wndColor;
	m_gridColor		= pPane->m_gridColor;

	// nastaven� m��ky
	SetGrid(pPane->m_bShowGrid, pPane->m_bAlignGrid, pPane->m_gridStep);
	m_pEditParams	= pPane->m_pEditParams;

	// fit to Window
	if ((m_nScale == SS_ISOTROPICFIT)||(m_nScale == SS_ANISOTROPICFIT))
	{
		SetScrollScale(m_nScale);
	}
};

void CFMntScrollerView::UpdateAllViews(CFMntScrollUpdate* pUpdate)
{
	ASSERT_KINDOF(CFMntScrollUpdate,pUpdate);
	m_pDocument->UpdateAllViews(NULL,pUpdate->m_nType,pUpdate);
};
void CFMntScrollerView::UpdateAllViews(DWORD nType)
{
	CFMntScrollUpdate cUpdate(nType,this);
	UpdateAllViews(&cUpdate);
};

void CFMntScrollerView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
	//if (GetDlgCtrlID() == AFX_IDW_PANE_FIRST)
	{
		InitialUpdateMetrics();
	}/* else
	{
		CWnd* pParent = GetParent();
		ASSERT(pParent != NULL);
		ASSERT_KINDOF(CSplitterWnd,pParent);
		CFMntScrollerView* pPane = (CFMntScrollerView*)(((CSplitterWnd*)pParent)->GetPane(1,1));
		ASSERT(pPane != NULL);
		ASSERT_KINDOF(CFMntScrollerView,pPane);
		CopySplitViewMetrics(pPane);
	}*/  
}

void CFMntScrollerView::SetMouseMode(WORD nMode)
{
  if (nMode != MM_PULL)
    m_nMouseMode = nMode;
  else
  {
    POINT CursorPos;    
    GetCursorPos(&CursorPos);
    ScreenToClient(&CursorPos);

    CRect rClient;
    GetClientRect(&rClient);

    if (rClient.PtInRect(CursorPos))    
    {
      // center to mouse point
      m_cPullPointLog = CursorPos;
      m_cPullPointLog += GetDeviceScrollPosition();
      m_cPullPointLog = FromDeviceToScrollPosition(m_cPullPointLog);
      m_nMouseMode = MM_PULL;       
    }  
  }
  
  SetCursorFromMouseMode();
};

void CFMntScrollerView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	if (lHint >= uvfFirstUpdate)
	{
		ASSERT(pHint != NULL);
		ASSERT_KINDOF(CFMntScrollUpdate,pHint);
		CFMntScrollUpdate* pUpdate = (CFMntScrollUpdate*)pHint;
		// v r�mci cel�ho dokumentu
		switch(lHint)
		{		
			case uvfEOUpdtChngObj:
				// reakce na zm�nu objektu (m_nParam != 0)->unselect
				if ((m_pEditInfo!=NULL)&&(m_pEditInfo->m_pSelObject == pUpdate->m_pObject))
					m_pEditInfo->UpdateObjectChanges(this,(pUpdate->m_nParam != 0), TRUE);
				return;
			case uvfEOInvalRect:
				// p�ekreslen� rectu ve v�ech view
				InvalidateLogRect(pUpdate->m_rPlace,pUpdate->m_nParam != 0);
				return;
		};
		// pouze v r�mci view �i splitteru
		if (pUpdate->m_pParent != GetParent())
			return;
		switch(lHint)
		{
			case uvfChangeBkColor:
				// zm�na v r�mci okna �i splitteru
				m_wndColor = (COLORREF)(pUpdate->m_nParam1);
				Invalidate();
				break;
			case uvfChangeScale:
				// zm�na v r�mci okna �i splitteru
				SetScrollScale(pUpdate->m_nParam);
				break;
			case uvfDoZoomIn:
				DoZoomIn(pUpdate);
				break;
			/*case uvfDoZoomOut:
				// proveden� ZoomOut
				DoZoomOut();
				break;*/
			/*case uvfSaveCurrScale:
				// ulo�en� akt. nastaven� do historie
				SaveZoomHistory();
				break;*/
			/*case uvfLastCurrScale:
				// posledn� m��. v historii
				LastZoomHistory();
				break;*/
			/*case uvfResetHisScale:
				// nulov�n� historie pro ZoomOut
				ResetZoomHistory();
				break;*/
			/*case uvfSetMouseMode:
				// nastaven� modu my�i
				SetMouseMode((WORD)pUpdate->m_nParam1);
				break;
			case uvfCancelMouseMode:
				CancelMouseMode();
				break;*/
			case uvfChangeGrid:
				// zm�na m��ky
				SetGrid(pUpdate->m_nParam1!=0,pUpdate->m_nParam2!=0,CSize(pUpdate->m_rPlace.left,pUpdate->m_rPlace.top));
				break;

			case uvfEOSelObject:
				// ur�en objekt v m_pEditParams - vyberu ho
				DoSelectObject();
				break;
			case uvfEOUnselObject:
				// zru�en v�b�r objektu v m_pEditParams
				DoUnselectObject();
        //m_pEditParams->m_pSelObject = NULL;
				break;
			default:
				ASSERT(FALSE);
				CView::OnUpdate( pSender, lHint, pHint);
				break;
		};
	} else
		CView::OnUpdate( pSender, lHint, pHint);
}

/////////////////////////////////////////////////////////////////////////////
// CFMntScrollerView drawing

void CFMntScrollerView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo) 
{
	//MASSERT_VALID(pDC);
#ifdef _DEBUG
	if ((m_nMapMode == MM_NONE)||(m_nRealMapMode == MM_NONE))
	{
		//MTRACE("Error: must call SetScrollSizes() or SetScaleToFitSize()"
		//	   " before painting scroll view\n");
		MASSERT(FALSE);
		return;
	}
#endif //_DEBUG
	//MASSERT(m_areaLog.left >= 0 && m_areaLog.top >= 0);
	//MASSERT(m_areaLog.Width() >= 0 && m_areaLog.Height() >= 0);
	//MASSERT(m_totalDev.cx >= 0 && m_totalDev.cx >= 0);
	if (!pDC->IsPrinting())
	{	// na obrazovce
		switch (m_nScale)
		{
			case SS_ISOTROPICFIT:
				pDC->SetMapMode(MM_ISOTROPIC);
				break;
			case SS_ANISOTROPICFIT:
				pDC->SetMapMode(MM_ANISOTROPIC);
				break;
			default:
				MASSERT(m_nMapMode > 0);
				pDC->SetMapMode(m_nMapMode);
				break;
		};
		pDC->SetWindowExt(m_rAreaLog.Width(),m_rAreaLog.Height());  // window is in logical coordinates
		pDC->SetViewportExt(m_szTotalDev);
		#ifdef _DEBUG			
			if (m_szTotalDev.cx == 0 || m_szTotalDev.cy == 0)
				TRACE0("Warning: CScrollerView scaled to nothing\n");
		#endif //_DEBUG				

		CPoint ptVpOrg(0, 0);       // assume no shift for printing
		if (!pDC->IsPrinting())
		{
			MASSERT(pDC->GetWindowOrg() == CPoint(0,0));
			pDC->SetWindowOrg(m_rAreaLog.left,m_rAreaLog.top);
			// by default shift viewport origin in negative direction of scroll
			ptVpOrg = -GetDeviceScrollPosition();
			// centrov�n� plochy v okn� (je-li men��)
			if (m_bCenterInView)
			{
				CRect rect;
				GetClientRect(&rect);
				// if client area is larger than total device size,
				// override scroll positions to place origin such that
				// output is centered in the window
				if (m_szTotalDev.cx < rect.Width())
					ptVpOrg.x = (rect.Width() - m_szTotalDev.cx) / 2;
				if (m_szTotalDev.cy < rect.Height())
					ptVpOrg.y = (rect.Height() - m_szTotalDev.cy) / 2;
			}
		}
		pDC->SetViewportOrg(ptVpOrg);
	} else
	{	// pro tisk
/*
		ASSERT(pInfo != NULL);
		int nPage = (int)(pInfo->m_nCurPage);
		ASSERT(nPage > 0);
		// attributy tisku
		SPrintParams sPrnPar;
		GetPrintParams(sPrnPar);
		// p��prava DC a obsah str�nky
		GetPrintPageContent(pDC, pInfo, sPrnPar,
							nPage, m_rPrintRect, TRUE);
*/
	}
}


void CFMntScrollerView::OnDraw(CDC* pDC)
{
	if (!pDC->IsPrinting())
	{	// draw grid first
		DrawGrid(pDC);
	}/* else
		pDC->IntersectClipRect(m_rPrintRect);
*/
	// draw user objects
	OnDrawObjects(pDC);

	if (!pDC->IsPrinting())
	{
		// draw track objects
		if ((m_pEditInfo)&&(m_pEditInfo->m_pSelTracker))
		{
			m_pEditInfo->m_pSelTracker->Draw(pDC);      
		}	
	} /*else
		pDC->ExcludeClipRect(m_rPrintRect);
*/
}

// user draw & redraw support
void CFMntScrollerView::OnDrawObjects(CDC* pDC)
{	// aplikace u�ivatele mus� p�epsat tuto funkci 
	// pro vykreslen� obsahu view

	// provizorn� kreslen�
	CPen* pOldPen = (CPen*)pDC->SelectStockObject(BLACK_PEN);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(RGB(0,0,0));
	pDC->SetTextAlign(TA_LEFT|TA_TOP);
	for(int x=0; x<m_rAreaLog.right; x+=250)
	{
		for(int y=0; y<m_rAreaLog.bottom; y+=250)
		{
			CString sX,sY;
			NumToLocalString(sX,(double)(x/10));
			NumToLocalString(sY,(double)(y/10));
			sX += _T(",");
			sX += sY;
			pDC->TextOut(x,y,sX,lstrlen(sX));
			// Y ��ra
			pDC->MoveTo(0,y);
			pDC->LineTo(m_rAreaLog.right,y);
		}
			// X ��ra
			pDC->MoveTo(x,0);
			pDC->LineTo(x,m_rAreaLog.bottom);
	}
	pDC->SelectObject(pOldPen);
}	

void CFMntScrollerView::InvalidateLogRect(CRect& aRect,BOOL bErase) const
{
	CRect rect = FromScrollToDevicePosition(aRect);
	rect	  -= GetDeviceScrollPosition();
	// p�id�m jeden device bod
	rect.InflateRect(1,1);
	CRect client;GetClientRect(client);
	if (rect.IntersectRect(rect,client))
		::InvalidateRect(m_hWnd,rect,bErase);
}

/////////////////////////////////////////////////////////////////////////////
// CFMntScrollerView diagnostics

#ifdef _DEBUG
void CFMntScrollerView::AssertValid() const
{
	CView::AssertValid();
}

void CFMntScrollerView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// scroll positions & units conversion

CPoint CFMntScrollerView::FromDeviceToScrollPosition(CPoint dev) const
{
	CWindowDC DC(NULL);
    switch (m_nScale)
    {
		case SS_ISOTROPICFIT:
			DC.SetMapMode(MM_ISOTROPIC);
			break;
		case SS_ANISOTROPICFIT:
			DC.SetMapMode(MM_ANISOTROPIC);
			break;
		default:
			MASSERT(m_nMapMode > 0);
			DC.SetMapMode(m_nMapMode);
			break;
    };
	DC.SetWindowExt(m_rAreaLog.Width(),m_rAreaLog.Height());  // window is in logical coordinates
	DC.SetViewportExt(m_szTotalDev);
	DC.SetWindowOrg(m_rAreaLog.left,m_rAreaLog.top);
	// centrov�n� plochy v okn� (je-li men��)
	CPoint ptVpOrg(0,0);
	if (m_bCenterInView)
	{
		CRect rect;
		GetClientRect(&rect);
		// if client area is larger than total device size,
		// override scroll positions to place origin such that
		// output is centered in the window
		if (m_szTotalDev.cx < rect.Width())
			ptVpOrg.x = (rect.Width() - m_szTotalDev.cx) / 2;
		if (m_szTotalDev.cy < rect.Height())
			ptVpOrg.y = (rect.Height() - m_szTotalDev.cy) / 2;
	}
	DC.SetViewportOrg(ptVpOrg);
	DC.DPtoLP(&dev);
	return dev;
}

CRect CFMntScrollerView::FromDeviceToScrollPosition(CRect r)  const
{
	CRect scr;
	CPoint devPt,scrPt;
	devPt.x = r.left; devPt.y = r.top;
	scrPt = FromDeviceToScrollPosition(devPt);
	scr.left = scrPt.x, scr.top = scrPt.y;
	devPt.x = r.right; devPt.y = r.bottom;
	scrPt = FromDeviceToScrollPosition(devPt);
	scr.right = scrPt.x, scr.bottom = scrPt.y;
	return scr;
}

CPoint CFMntScrollerView::FromScrollToDevicePosition(CPoint scr) const
{
	CPoint dev = scr;
	CWindowDC DC(NULL);
    switch (m_nScale)
    {
		case SS_ISOTROPICFIT:
			DC.SetMapMode(MM_ISOTROPIC);
			break;
		case SS_ANISOTROPICFIT:
			DC.SetMapMode(MM_ANISOTROPIC);
			break;
		default:
			MASSERT(m_nMapMode > 0);
			DC.SetMapMode(m_nMapMode);
			break;
    };
	DC.SetWindowExt(m_rAreaLog.Width(),m_rAreaLog.Height());  // window is in logical coordinates
	DC.SetViewportExt(m_szTotalDev);
	DC.SetWindowOrg(m_rAreaLog.left,m_rAreaLog.top);
	// centrov�n� plochy v okn� (je-li men��)
	CPoint ptVpOrg(0,0);
	if (m_bCenterInView)
	{
		CRect rect;
		GetClientRect(&rect);
		// if client area is larger than total device size,
		// override scroll positions to place origin such that
		// output is centered in the window
		if (m_szTotalDev.cx < rect.Width())
			ptVpOrg.x = (rect.Width() - m_szTotalDev.cx) / 2;
		if (m_szTotalDev.cy < rect.Height())
			ptVpOrg.y = (rect.Height() - m_szTotalDev.cy) / 2;
	}
	DC.SetViewportOrg(ptVpOrg);
	DC.LPtoDP(&dev);
	return dev;
}

CRect  CFMntScrollerView::FromScrollToDevicePosition(CRect r)  const
{
	CRect scr;
	CPoint devPt,scrPt;
	devPt.x = r.left; devPt.y = r.top;
	scrPt = FromScrollToDevicePosition(devPt);
	scr.left = scrPt.x, scr.top = scrPt.y;
	devPt.x = r.right; devPt.y = r.bottom;
	scrPt = FromScrollToDevicePosition(devPt);
	scr.right = scrPt.x, scr.bottom = scrPt.y;
	return scr;
}

CPoint CFMntScrollerView::WindowToScrollerPosition(CPoint mouse) const
{
	mouse  += GetDeviceScrollPosition();
	return FromDeviceToScrollPosition(mouse);
}

CRect CFMntScrollerView::WindowToScrollerPosition(CRect mouseRect) const
{
  CPoint devScroll = GetDeviceScrollPosition();
  mouseRect.left  += devScroll.x;
  mouseRect.top  += devScroll.y;

  mouseRect.right  += devScroll.x;
  mouseRect.bottom  += devScroll.y;

  return FromDeviceToScrollPosition(mouseRect);
}

CPoint CFMntScrollerView::FromScrollToWindowPosition(CPoint ptPos) const
{
	ptPos  = FromScrollToDevicePosition(ptPos);
	ptPos -= GetDeviceScrollPosition();
	return ptPos;
}

CRect CFMntScrollerView::FromScrollToWindowPosition(CRect rPos)  const
{
	rPos  = FromScrollToDevicePosition(rPos);
	rPos -= GetDeviceScrollPosition();
	return rPos;
};

CRect  CFMntScrollerView::GetLogClientRect() const// client rect v log. sou�adnic�ch
{
	CRect r; GetClientRect(r);
 	r += GetDeviceScrollPosition();
	r = FromDeviceToScrollPosition(r);
	r.IntersectRect(r,m_rAreaLog);
	return r;
}

CSize  CFMntScrollerView::GetLogInMM() const	// po�et log. bod� na mm p�i akt. m���tku	
{
	//return CSize(3,3);

	CWindowDC  dc(NULL);

	// 28.7.2003 On tomas computer dc.GetDeviceCaps(HORZSIZE) and dc.GetDeviceCaps(VERTSIZE) are corrupted.
	// Lets fix it.
	//CSize sz(dc.GetDeviceCaps(HORZRES)/dc.GetDeviceCaps(HORZSIZE),
	//		 dc.GetDeviceCaps(VERTRES)/dc.GetDeviceCaps(VERTSIZE) );
	CSize sz(dc.GetDeviceCaps(HORZRES), dc.GetDeviceCaps(VERTRES));
	int horsize = dc.GetDeviceCaps(HORZSIZE);
	int versize = dc.GetDeviceCaps(VERTSIZE);
	sz.cx /= (horsize  > 10) ? horsize : 360;
	sz.cy /= (versize  > 10) ? versize : 270;
	
	//LogF("GetLogInMM: HORZRES %d, HORZSIZE %d, VERTRES %d, VERTSIZE %d \n", dc.GetDeviceCaps(HORZRES), dc.GetDeviceCaps(HORZSIZE),
	//		 dc.GetDeviceCaps(VERTRES), dc.GetDeviceCaps(VERTSIZE));
	//LogF("GetLogInMM: sz(%d,%d)\n", sz.cx, sz.cy);

    switch (m_nScale)
    {
		case SS_ISOTROPICFIT:
			dc.SetMapMode(MM_ISOTROPIC);
			break;
		case SS_ANISOTROPICFIT:
			dc.SetMapMode(MM_ANISOTROPIC);
			break;
		default:
			MASSERT(m_nMapMode > 0);
			dc.SetMapMode(m_nMapMode);
			break;
    };
	dc.SetWindowExt(m_rAreaLog.Width(),m_rAreaLog.Height());  // window is in logical coordinates
	//LogF("GetLogInMM: m_rAreaLog(%d,%d)\n", m_rAreaLog.Width(),m_rAreaLog.Height());
	dc.SetViewportExt(m_szTotalDev);
	//LogF("GetLogInMM: m_szTotalDev(%d,%d)\n", m_szTotalDev.cx,m_szTotalDev.cy);
	dc.SetWindowOrg(0,0);
	dc.SetViewportOrg(0,0);
	dc.DPtoLP(&sz);
	return sz;
}

/////////////////////////////////////////////////////////////////////////////
// in logical units - call one of the following Set routines

void CFMntScrollerView::SetScrollSizes(int nRealMapMode, 
									  SIZE sizePage, SIZE sizeLine)
{
	//MASSERT(m_rAreaLog.left >= 0 && m_rAreaLog.top >= 0);
	MASSERT((m_rAreaLog.Width()) > 0 && (m_rAreaLog.Height()) > 0);
	MASSERT(nRealMapMode > 0);
	MASSERT(nRealMapMode != MM_ISOTROPIC && nRealMapMode != MM_ANISOTROPIC);
	MASSERT(nRealMapMode == MM_LOMETRIC  || nRealMapMode == MM_HIMETRIC ||  nRealMapMode == MM_TEXT);
	
	m_nRealMapMode 		= nRealMapMode;
	m_nMapMode 			= MM_ISOTROPIC;
	m_nScale			= SS_REALSCALE;
		
	ASSERT(m_szTotalDev.cx >= 0 && m_szTotalDev.cy >= 0);
	//BLOCK: convert logical coordinate space to device coordinates 
	//       for RealScale
	{
		CWindowDC dc(NULL);
		ASSERT(m_nRealMapMode > 0);
		dc.SetMapMode(m_nRealMapMode);
		// total size
		m_szTotalRDev = m_rAreaLog.Size();
		dc.LPtoDP((LPPOINT)&m_szTotalRDev);
		if (m_szTotalRDev.cy < 0)
			m_szTotalRDev.cy = -m_szTotalRDev.cy;
		
		m_szPageDev = sizePage;
		m_szLineDev = sizeLine;
	    m_szTotalDev= m_szTotalRDev;
	} // release DC here
	// adjust min & max scale
	{
		//m_nMinScale  = SS_MINSCALE;
		double scale = min((double)(INT_MAX)/*INT_MAX*//(double)m_szTotalRDev.cx,
						   (double)(INT_MAX)/*INT_MAX*//(double)m_szTotalRDev.cy);
		m_nMaxScale  = min((int)(scale*SCALE_UNIT),SS_MAXSCALE);    
	}
	if (m_hWnd != NULL)
	{	// window has been created, invalidate now
		UpdateBars();
		Invalidate(TRUE);
	};
	// reset zoom history if size changed
	//ResetZoomHistory();
};

/////////////////////////////////////////////////////////////////////////////
// scale settings & verification

void CFMntScrollerView::SetScrollScale(int aScale,POINT* pOrigin)
{
	//LogF("SetScrollScale: %d\n",aScale);
	// nov� origin po nastaven� m���tka
	CPoint ptOrigin; 
	ptOrigin = ((pOrigin!=NULL)?(*pOrigin):((POINT)GetScrollPosition()));
	// nastaven� m���tka
	ASSERT(m_nRealMapMode > 0);

	if (aScale == SS_ISOTROPICFIT) 
	{	
		// isotropic fit in window
		m_nScale   = SS_ISOTROPICFIT;
		m_nMapMode = MM_ISOTROPIC;
		if (m_hWnd != NULL) /* && (GetStyle() & (WS_HSCROLL|WS_VSCROLL)))*/
		{
			SetScrollPos(SB_HORZ, 0);
			SetScrollPos(SB_VERT, 0);
			EnableScrollBarCtrl(SB_BOTH, FALSE);
			MASSERT((GetStyle() & (WS_HSCROLL|WS_VSCROLL)) == 0);
		}
		CRect rectT; GetClientRect(rectT);
		m_szTotalDev = rectT.Size();
		// vezmu odpov�daj�c� velikost
		CRect rPom   = FromScrollToDevicePosition(m_rAreaLog);
		m_szTotalDev = rPom.Size();
	} else
	if (aScale == SS_ANISOTROPICFIT) // anisotropic fit in window
	{
		// anisotropic fit in window
		m_nScale   = SS_ANISOTROPICFIT;
		m_nMapMode = MM_ANISOTROPIC;
		if (m_hWnd != NULL) /* && (GetStyle() & (WS_HSCROLL|WS_VSCROLL)))*/
		{
			SetScrollPos(SB_HORZ, 0);
			SetScrollPos(SB_VERT, 0);
			EnableScrollBarCtrl(SB_BOTH, FALSE);
			MASSERT((GetStyle() & (WS_HSCROLL|WS_VSCROLL)) == 0);
		}
		CRect rectT; GetClientRect(rectT);
		m_szTotalDev = rectT.Size();
	} else
	{
		ASSERT((aScale >= SS_MINSCALE)&&(aScale <= SS_MAXSCALE));
		ASSERT((aScale >= SS_MINSCALE)&&(aScale <= SS_MAXSCALE));
    CRect rectT; GetClientRect(rectT);    
    int nMinScale = GetMinScale();
		if (aScale < nMinScale)
			aScale = nMinScale;
		if (aScale > m_nMaxScale)
			aScale = m_nMaxScale;
		// men�� m���tko -> ignoruji v�t�� m���tka v historii
		/*for(WORD p=0;p<m_nZoomHistory;p++)
		{
			if (m_sHistory[p].nScale >= aScale)
			{	// toto ji� nen� zoom out (p�eru��m)
				m_nZoomHistory = p;	break;
			}
		}*/
		// isotropic scale in window
		m_nScale   = aScale;
		m_nMapMode = MM_ISOTROPIC;
		double scale = (double)m_nScale;
		scale /= SCALE_UNIT;
		scale = m_szTotalRDev.cx * scale;
		m_szTotalDev.cx = (int)scale;
		scale = m_nScale;
		scale /= SCALE_UNIT;
		scale = m_szTotalRDev.cy * scale;
		m_szTotalDev.cy = (int)scale;
		
	}; 
	// reclac grid
	CalcCurrentGrid();
	// update bars
	if (m_hWnd != NULL)
	{
		// nastav�m po��tek podobn� jako UpdateBars(), ale nescroluji
		MASSERT(m_nMapMode > 0);     // not allowed for shrink to fit
		// Lock out recursion
		m_bInsideUpdate = TRUE;
		// update the horizontal to reflect reality
		MASSERT(m_szTotalDev.cx >= 0 && m_szTotalDev.cy >= 0);
		CSize sizeClient;
		CSize sizeSb;
		if (!GetTrueClientSize(sizeClient, sizeSb))
		{
			// no room for scroll bars (common for zero sized elements)
			CRect rect;	GetClientRect(&rect);
			if (rect.right > 0 && rect.bottom > 0)
			{
				// if entire client area is not invisible, assume we have
				//  control over our scrollbars
				EnableScrollBarCtrl(SB_BOTH, FALSE);
			}
			m_bInsideUpdate = FALSE;
			return;
		}
		// enough room to add scrollbars
		CSize sizeRange = m_szTotalDev - sizeClient;
		// > 0 => need to scroll
		// p��mo bod na kter� m�m d�lat nastavit po��tek
		CPoint ptMove = FromScrollToDevicePosition(ptOrigin); 
		// point to move to (start at current scroll pos)
		BOOL bNeedH = sizeRange.cx > 0;
		if (bNeedH)
			sizeRange.cy += sizeSb.cy;          // need room for a scroll bar
		else
			ptMove.x = 0;                       // jump back to origin
		BOOL bNeedV = sizeRange.cy > 0;
		if (bNeedV)
			sizeRange.cx += sizeSb.cx;
		else
			ptMove.y = 0;                       // jump back to origin
		if (bNeedV && !bNeedH && sizeRange.cx > 0)
		{
			// need a horizontal scrollbar after all
			bNeedH = TRUE;
			sizeRange.cy += sizeSb.cy;
		}
		// if current scroll position will be past the limit, scroll to limit
		if (sizeRange.cx > 0 && ptMove.x >= sizeRange.cx)
			ptMove.x = sizeRange.cx;
		if (sizeRange.cy > 0 && ptMove.y >= sizeRange.cy)
			ptMove.y = sizeRange.cy;
		// now update the bars as appropriate
		// this structure needed to update the scrollbar page range
		SCROLLINFO info;
		info.fMask = SIF_PAGE|SIF_RANGE;
		info.nMin = 0;
		// now update the bars as appropriate
		// EnableScrollBarCtrl(SB_HORZ, TRUE);
		if (bNeedH)
		{
			EnableScrollBarCtrl(SB_HORZ, bNeedH);
			info.nPage = sizeClient.cx;
			//info.nMax  = m_szTotalDev.cx-1;
     info.nMax  = sizeRange.cx;
			if (!SetScrollInfo(SB_HORZ, &info, TRUE))
				SetScrollRange(SB_HORZ, 0, info.nMax, TRUE);
			// scroll the window as needed - pouze scrollbar
			SetScrollPos(SB_HORZ, ptMove.x);
		} else
		{
			// scroll the window as needed - pouze scrollbar
			SetScrollPos(SB_HORZ, ptMove.x);
			EnableScrollBarCtrl(SB_HORZ, bNeedH);
		}
		// EnableScrollBarCtrl(SB_VERT, TRUE);
		if (bNeedV)
		{
			EnableScrollBarCtrl(SB_VERT, bNeedV);
			info.nPage = sizeClient.cy;
			//info.nMax = m_szTotalDev.cy-1;
      info.nMax = sizeRange.cy;
			if (!SetScrollInfo(SB_VERT, &info, TRUE))
				SetScrollRange(SB_VERT, 0, sizeRange.cy, TRUE);
			// scroll the window as needed - pouze scrollbar
			SetScrollPos(SB_VERT, ptMove.y);
		} else
		{
			// scroll the window as needed - pouze scrollbar
			SetScrollPos(SB_VERT, ptMove.y);
			EnableScrollBarCtrl(SB_VERT, bNeedV);
		}
		// Remove recursion lockout
		m_bInsideUpdate = FALSE;
		// update track objects
		if (m_pEditInfo)
		{
      m_pEditInfo->OnChangeDP(this);
			//m_pEditInfo->DestroyTracker(this,FALSE);
			//m_pEditInfo->CreateTracker(this,FALSE); 
		}	
		// window has been created, invalidate
    OnScrollScaleChanged();
		Invalidate(TRUE);

	};
}

// ulo�en� akt. stavu do historie
/*void CFMntScrollerView::SaveZoomHistory()
{
	if (m_nScale <= SS_NOSCALE)
		return; // spec. m���tka neuchov�v�m

	// je m�sto v bufferu ?
	if (++m_nZoomHistory >= NUM_ZOOM_HISTORY)
	{  // zapomenu pozici 0 a posunu v�e zp�t
		for(int p=0;p<(NUM_ZOOM_HISTORY-1);p++)
			m_sHistory[p] = m_sHistory[p+1];
		m_nZoomHistory--;
	}

	// ulo��m historii na nPos
	ASSERT(m_nZoomHistory < NUM_ZOOM_HISTORY);
	m_sHistory[m_nZoomHistory].nScale = m_nScale;
	CPoint org = GetScrollPosition();
	m_sHistory[m_nZoomHistory].ptOrigin.x = org.x;
	m_sHistory[m_nZoomHistory].ptOrigin.y = org.y;
}
*/
/*
// nastaven� posledn�ho m���tka historie
void CFMntScrollerView::LastZoomHistory()	
{
	ASSERT(m_nZoomHistory>0);
	ASSERT(m_nZoomHistory<NUM_ZOOM_HISTORY);
	// nastav�m posledn� stav historie
	SetScrollScale(m_sHistory[m_nZoomHistory].nScale,
				 &(m_sHistory[m_nZoomHistory].ptOrigin));
	m_nZoomHistory--;
};
*/
void CFMntScrollerView::DoZoomIn(CFMntScrollUpdate* pParams)
{	
	if (pParams->m_pSender != this)
	{
		POINT pt = GetScrollPosition();
		SetScrollScale(pParams->m_nParam,&pt);
	}
}

BOOL CFMntScrollerView::CanZoomIn() const
{	return (m_nScale<m_nMaxScale); }

/*#define NUM_ZOOM_OUT_STEPS 11
static int dataZoomOut[NUM_ZOOM_OUT_STEPS] =
{ 500,400,300,200,100,85,66,50,25,10,5 };
*/
/*void CFMntScrollerView::DoZoomOut()
{
	ASSERT(m_nZoomHistory==0);
	// ZoomIn po zvolen�ch intervalech
	int nScale = m_nMinScale; // posledn� ZoomOut
	for(int i=0;i<NUM_ZOOM_OUT_STEPS;i++)
		if ((dataZoomOut[i]<m_nScale)&&(dataZoomOut[i]>=m_nMinScale))
		{
			nScale = dataZoomOut[i]; break;
		}
	SetScrollScale(nScale);
}*/

BOOL CFMntScrollerView::CanZoomOut() const
{	return (m_nScale>GetMinScale()); }

/////////////////////////////////////////////////////////////////////////////
// grid settings & align

void CFMntScrollerView::SetGrid(BOOL bShow, BOOL bAlign, CSize& sizeGrid)
{
	BOOL bInvalidate = (m_bShowGrid != bShow);
	m_bShowGrid = bShow;
	m_bAlignGrid= bAlign;
	// zm�na rizli�en�
	if (m_bShowGrid)
		bInvalidate |= (m_gridStep != sizeGrid);
	m_gridStep	= sizeGrid;
	//LogF("SetGrid: m_gridStep(%ld, %ld)", m_gridStep.cx, m_gridStep.cy);

	// zm�na m��ky
	CalcCurrentGrid();
	if ((m_pEditInfo)&&(m_pEditInfo->m_pSelTracker))
	{
		m_pEditInfo->m_pSelTracker->m_eAlignGrid =
      m_bAlignGrid ? fstAlignSquare : fstAlignNone;
	}
	// invalidate iff viditeln� zm�na
	if (bInvalidate) Invalidate(TRUE);
};

void CFMntScrollerView::CalcCurrentGrid()
{
	if ((!m_bShowGrid)&&(!m_bAlignGrid)) return;
	CSize  sz = GetLogInMM();
	// minim�ln� krok 4 mm
	sz.cx *= 4; sz.cy *= 4;
	// spo��t�m kolik krok� mus�m vynechat
	m_currGridStep = m_gridStep;
	//LogF("CalcCurrentGrid: m_currGridStep: %ld, %ld", m_currGridStep.cx, m_currGridStep.cy);
	//LogF("CalcCurrentGrid: sz: %ld, %ld", sz.cx, sz.cy);
	while(m_currGridStep.cx < sz.cx) m_currGridStep.cx *= 2;
	while(m_currGridStep.cy < sz.cy) m_currGridStep.cy *= 2;
};

void CFMntScrollerView::DrawGrid(CDC* pDC)
{
	if (!m_bShowGrid) return;
	// paint grid to clip rect or full rect
	CRect rect;
	pDC->GetClipBox(&rect);
	rect.InflateRect(m_currGridStep);
	if (!rect.IntersectRect(rect,m_rAreaLog)) return;
	rect = AlignRectToGrid(rect,rect);
	//LogF("Draw: m_currGridStep: %ld, %ld", m_currGridStep.cx, m_currGridStep.cy);
	for(int x = rect.left; x <= rect.right; x += m_currGridStep.cx)
	{
		for(int y = rect.top; y <= rect.bottom; y += m_currGridStep.cy)
		{
			pDC->SetPixelV(x,y,m_gridColor);
		}
	}
};

CPoint CFMntScrollerView::UpdateInClipRect(LPCRECT lpRect,CPoint& point)
{
	// UpdateInClipRect
	CPoint pt = point;
	if (pt.x < lpRect->left)
		pt.x = lpRect->left;
	if (pt.x > lpRect->right)
		pt.x = lpRect->right;
	if (pt.y < lpRect->top)
		pt.y = lpRect->top;
	if (pt.y > lpRect->bottom)
		pt.y = lpRect->bottom;
	return pt;
};

CPoint CFMntScrollerView::AlignToStep(CPoint& point, const CPoint& step, const CPoint& offset,LPCRECT pClip/* = NULL*/)
{
  CPoint pt((point.x - offset.x)/step.x,(point.y - offset.y)/step.y);
  pt.x = pt.x * step.x ;
  pt.y = pt.y * step.y ;

  // zaokrouhl�m na nejbli���
  if ((point.x-pt.x - offset.x)>(pt.x+step.x - point.x + offset.x))
    pt.x += step.x;
  if ((point.y-pt.y - offset.y)>(pt.y+step.y - point.y + offset.y))
    pt.y += step.y;

  pt.x += offset.x;
  pt.y += offset.y;

  if (pClip!=NULL)
  {
    if (pt.x < pClip->left  ) 
      pt.x += step.x * ((pClip->left-pt.x) / step.x + 1);
    if (pt.x > pClip->right )
      pt.x -= step.x * ((pt.x-pClip->right) / step.x + 1);
    if (pt.y < pClip->top )
      pt.y += step.y * ((pClip->top -pt.y) / step.y + 1);
    if (pt.y > pClip->bottom)
      pt.y -= step.y * ((pt.y-pClip->bottom) / step.y + 1);
  }
  return pt;
};

CPoint CFMntScrollerView::AlignToGrid(CPoint& point,LPCRECT pClip)
{
	CPoint pt(point.x/m_currGridStep.cx,point.y/m_currGridStep.cy);
	pt.x *= m_currGridStep.cx;
	pt.y *= m_currGridStep.cy;
	// zaokrouhl�m na nejbli���
	if ((point.x-pt.x)>(pt.x+m_currGridStep.cx-point.x))
		pt.x += m_currGridStep.cx;
	if ((point.y-pt.y)>(pt.y+m_currGridStep.cy-point.y))
		pt.y += m_currGridStep.cy;
	if (pClip!=NULL)
	{
		if (pt.x < pClip->left  ) pt.x += m_currGridStep.cx * ((pClip->left-pt.x+m_currGridStep.cx-1) / m_currGridStep.cx);
		if (pt.x > pClip->right ) pt.x -= m_currGridStep.cx * ((pt.x-pClip->right+m_currGridStep.cx-1) / m_currGridStep.cx);
		if (pt.y < pClip->top   ) pt.y += m_currGridStep.cy * ((pClip->top -pt.y+m_currGridStep.cy-1) / m_currGridStep.cy);
		if (pt.y > pClip->bottom) pt.y -= m_currGridStep.cy * ((pt.y-pClip->bottom+m_currGridStep.cy-1) / m_currGridStep.cy);
	}
	return pt;
};

CRect  CFMntScrollerView::AlignRectToGrid(CRect& rect,LPCRECT pClip,SIZE* pMinSize)
{
	CRect gridRect;
	CPoint pt(rect.left,rect.top);
	pt = AlignToGrid(pt,pClip);
	gridRect.left = pt.x; gridRect.top = pt.y;
	pt.x = rect.right; pt.y = rect.bottom;
	pt = AlignToGrid(pt,pClip);
	gridRect.right = pt.x; gridRect.bottom = pt.y;
	// testuji minim�ln� velikost
	if (pMinSize)
	{
		ASSERT(gridRect.left <= gridRect.right);
		ASSERT(gridRect.top  <= gridRect.bottom);

		if ((gridRect.right-gridRect.left)<pMinSize->cx)
			gridRect.right += m_currGridStep.cx * ((pMinSize->cx-(gridRect.right-gridRect.left)+m_currGridStep.cx-1) / m_currGridStep.cx);
		
		if ((gridRect.bottom-gridRect.top )<pMinSize->cy)
			gridRect.bottom += m_currGridStep.cy * ((pMinSize->cy -(gridRect.bottom-gridRect.top )+m_currGridStep.cy-1) / m_currGridStep.cy);
		// testuji, zda neopustil ClipRect
		if (pClip)
		{
			while(gridRect.right > pClip->right)
			{
				if ((gridRect.left-m_currGridStep.cx)<pClip->left) break;
				gridRect.OffsetRect(-m_currGridStep.cx,0);

			}
			while(gridRect.bottom > pClip->bottom)
			{
				if ((gridRect.top-m_currGridStep.cy)<pClip->top) break;
				gridRect.OffsetRect(0,-m_currGridStep.cy);
			}
		}
	}
	return gridRect;
};

/////////////////////////////////////////////////////////////////////////////
// scrolling to position

CPoint CFMntScrollerView::GetDeviceScrollPosition() const
{
	CPoint pt(GetScrollPos(SB_HORZ), GetScrollPos(SB_VERT));
	ASSERT(pt.x >= 0 && pt.y >= 0);
	return pt;
}

void  CFMntScrollerView::ScrollToPosition(POINT pt)    	     // set upper left position
{
  CPoint ptOrig = GetDeviceScrollPosition();

	MASSERT(m_nMapMode > 0);     // not allowed for shrink to fit
	// podobn� jako UpdateBars, aby se zabr�nilo dvoj�mu p�ekreslen�
	// Lock out recursion
	m_bInsideUpdate = TRUE;
	// update the horizontal to reflect reality
	// NOTE: turning on/off the scrollbars will cause 'OnSize' callbacks
	MASSERT(m_szTotalDev.cx >= 0 && m_szTotalDev.cy >= 0);
	CSize sizeClient;
	CSize sizeSb;
	if (!GetTrueClientSize(sizeClient, sizeSb))
	{
		// no room for scroll bars (common for zero sized elements)
		CRect rect;
		GetClientRect(&rect);
		if (rect.right > 0 && rect.bottom > 0)
		{	// if entire client area is not invisible, assume we have
			//  control over our scrollbars
			EnableScrollBarCtrl(SB_BOTH, FALSE);
		}
		m_bInsideUpdate = FALSE;
		return;
	}
	// enough room to add scrollbars
	CSize sizeRange = m_szTotalDev - sizeClient;
	// > 0 => need to scroll
	// p��mo bod na kter� m�m d�lat scroll
	CPoint ptMove = FromScrollToDevicePosition(pt); 
  if (ptMove.x < 0)
    ptMove.x = 0;
  if (ptMove.y < 0)
    ptMove.y = 0;
  
	// point to move to (start at current scroll pos)
	BOOL bNeedH = sizeRange.cx > 0;
	if (bNeedH)
  {  
		//sizeRange.cy += sizeSb.cy;          // need room for a scroll bar
    // TODO: Island is drawn in whole client area, also under toolbars. why?
  }
	else
		ptMove.x = 0;                       // jump back to origin
	BOOL bNeedV = sizeRange.cy > 0;
	if (bNeedV)
  {  
		//sizeRange.cx += sizeSb.cx;
  }
	else
		ptMove.y = 0;                       // jump back to origin
	if (bNeedV && !bNeedH && sizeRange.cx > 0)
	{
		// need a horizontal scrollbar after all
		bNeedH = TRUE;
		//sizeRange.cy += sizeSb.cy;
	}
	// if current scroll position will be past the limit, scroll to limit
	if (sizeRange.cx > 0 && ptMove.x >= sizeRange.cx)
		ptMove.x = sizeRange.cx;
	if (sizeRange.cy > 0 && ptMove.y >= sizeRange.cy)
		ptMove.y = sizeRange.cy;
	// first scroll the window as needed
/*
	ScrollToDevicePosition(ptMove); // will set the scroll bar positions too
*/
	// now update the bars as appropriate
/*
	if (bNeedH) EnableScrollBarCtrl(SB_HORZ, bNeedH);
	if (bNeedV) EnableScrollBarCtrl(SB_VERT, bNeedV);
	SetScrollRange(SB_HORZ, 0, bNeedH?sizeRange.cx:0, TRUE);
	SetScrollRange(SB_VERT, 0, bNeedV?sizeRange.cy:0, TRUE);
	if (!bNeedH) EnableScrollBarCtrl(SB_HORZ, bNeedH);
	if (!bNeedV) EnableScrollBarCtrl(SB_VERT, bNeedV);
*/
	// now update the bars as appropriate
	// this structure needed to update the scrollbar page range
	SCROLLINFO info;
	info.fMask = SIF_PAGE|SIF_RANGE|SIF_POS;
	info.nMin = 0;
	// now update the bars as appropriate
	// EnableScrollBarCtrl(SB_HORZ, TRUE);
  BOOL bInvalidateH = FALSE;
	if (bNeedH)
	{
		EnableScrollBarCtrl(SB_HORZ, bNeedH);

    GetScrollInfo(SB_HORZ, &info, info.fMask);
    if (info.nPage != sizeClient.cx)
    {
		  info.nPage = sizeClient.cx;
      bInvalidateH = TRUE;
    }

		if (info.nMax  != m_szTotalDev.cx-1)
    {
      info.nMax = m_szTotalDev.cx-1;
      bInvalidateH = TRUE;
    }

    if (info.nMin  != 0)
    {
      info.nMin = 0;
      bInvalidateH = TRUE;
    }
    if (info.nPos  != ptMove.x)
    {
      info.nPos = ptMove.x;
      bInvalidateH = TRUE;
    }

    if (bInvalidateH)
      VERIFY(SetScrollInfo(SB_HORZ, &info, TRUE));
		
	} else
	{
		// scroll the window as needed - pouze scrollbar
		SetScrollPos(SB_HORZ, ptMove.x);
		EnableScrollBarCtrl(SB_HORZ, bNeedH);
	}
	// EnableScrollBarCtrl(SB_VERT, TRUE);
  BOOL bInvalidateV = FALSE;
	if (bNeedV)
	{
    GetScrollInfo(SB_VERT, &info, info.fMask);
    if (info.nPage != sizeClient.cy)
    {
      info.nPage = sizeClient.cy;
      bInvalidateV = TRUE;
    }

    if (info.nMax  != m_szTotalDev.cy-1)
    {
      info.nMax = m_szTotalDev.cy-1;
      bInvalidateV = TRUE;
    }

    if (info.nMin  != 0)
    {
      info.nMin = 0;
      bInvalidateV = TRUE;
    }
    if (info.nPos  != ptMove.y)
    {
      info.nPos = ptMove.y;
      bInvalidateV = TRUE;
    }

    if (bInvalidateV)
      VERIFY(SetScrollInfo(SB_VERT, &info, TRUE));
	} else
	{
		// scroll the window as needed - pouze scrollbar
		SetScrollPos(SB_VERT, ptMove.y);
		EnableScrollBarCtrl(SB_VERT, bNeedV);
	}

	// Remove recursion lockout
	m_bInsideUpdate = FALSE;

	// update track objects
	
  
  if (m_pEditInfo)
  {
    m_pEditInfo->OnChangeDP(this);
    //m_pEditInfo->DestroyTracker(this,FALSE);
    //m_pEditInfo->CreateTracker(this,FALSE); 
  }	
  ScrollWindow(ptOrig.x - ptMove.x, ptOrig.y - ptMove.y);
  UpdateWindow();

  OnScrollScaleChanged();
	// window has been created, invalidate
  //if(bInvalidateV || bInvalidateH)
	//  Invalidate(TRUE);
}

void   CFMntScrollerView::ScrollToDevicePosition(POINT ptDev) // explicit scrolling no checking
{
	MASSERT(ptDev.x >= 0);
	MASSERT(ptDev.y >= 0);
	int xOrig = SetScrollPos(SB_HORZ, ptDev.x);
	int yOrig = SetScrollPos(SB_VERT, ptDev.y);

	if (m_pEditInfo) 
		m_pEditInfo->ScrollTracker(xOrig - ptDev.x,yOrig - ptDev.y);

	ScrollWindow(xOrig - ptDev.x, yOrig - ptDev.y);
}

void CFMntScrollerView::CenterToRect(LPCRECT pRect)	// centrov�n� rectu
{
	DWORD xSz = pRect->left; xSz += pRect->right;
	DWORD ySz = pRect->top;  ySz += pRect->bottom;
	POINT pt;
	pt.x = (int)(xSz/2);
	pt.y = (int)(ySz/2);
	CRect lClient = GetLogClientRect();
	pt.x -= lClient.Width()/2;
	if (pt.x < m_rAreaLog.left) pt.x = m_rAreaLog.left;
	pt.y -= lClient.Height()/2;
	if (pt.y < m_rAreaLog.top)  pt.y = m_rAreaLog.top;
	ScrollToPosition(pt);
}

/////////////////////////////////////////////////////////////////////////////
// in logical units - call one of the following Set routines

// scrollbars manipulation
void CFMntScrollerView::OnScrollBar(int nBar, UINT nSBCode, UINT nPos) 
{
	CSize sizeClient;
	CSize sizeSb;
	if (!GetTrueClientSize(sizeClient, sizeSb))
	{	// no room for scroll bars (common for zero sized elements)
		CRect rect;	GetClientRect(&rect);
		sizeClient = rect.Size();
	}

	CSize szPage((sizeClient.cx * m_szPageDev.cx)/100,(sizeClient.cy * m_szPageDev.cy)/100);
	CSize szLine((sizeClient.cx * m_szLineDev.cx)/100,(sizeClient.cy * m_szLineDev.cy)/100);

	sizeClient.cx -= szPage.cx;
	if (sizeClient.cx <= 0) sizeClient.cx = szLine.cx;
	sizeClient.cy -= szPage.cy;
	if (sizeClient.cy <= 0) sizeClient.cy = szLine.cy;

	// do scrolling
	MASSERT(nBar == SB_HORZ || nBar == SB_VERT);
	BOOL bHorz = (nBar == SB_HORZ);

	int zOrig, z;   // z = x or y depending on 'nBar'
	int zMin, zMax;
	zOrig = z = GetScrollPos(nBar);

	//GetScrollRange(nBar, &zMin, &zMax);
  SCROLLINFO  info;

  zMin = 0;
  zMax = GetScrollLimit(nBar);
	
	MASSERT(zMin == 0);
	if (zMax <= 0)
	{
		//MTRACE("Warning: no scroll range - ignoring scroll message\n");
		MASSERT(z == 0);     // must be at top
		return;
	}

	switch (nSBCode)
	{
	case SB_TOP:
		z = 0;
		break;

	case SB_BOTTOM:
		z = zMax;
		break;
		
	case SB_LINEUP:
		z -= bHorz ? szLine.cx : szLine.cy;
		break;

	case SB_LINEDOWN:
		z += bHorz ? szLine.cx : szLine.cy;
		break;

	case SB_PAGEUP:
		z -= bHorz ? sizeClient.cx : sizeClient.cy;
		break;

	case SB_PAGEDOWN:
		z += bHorz ? sizeClient.cx : sizeClient.cy;
		break;

	case SB_THUMBTRACK:
    if(!GetScrollInfo(nBar, &info,  SIF_TRACKPOS))
      return;
		z = info.nTrackPos;
		break;

	default:        // ignore other notifications
		return;
	}

	if (z < 0)
		z = 0;
	else if (z > zMax)
		z = zMax;

	if (z != zOrig)
	{
		if (bHorz)
		{
			if (m_pEditInfo) 
				m_pEditInfo->ScrollTracker(-(z-zOrig), 0);
			ScrollWindow(-(z-zOrig), 0);
		}
		else
		{
			if (m_pEditInfo) 
				m_pEditInfo->ScrollTracker(0, -(z-zOrig));
			ScrollWindow(0, -(z-zOrig));
		}
		SetScrollPos(nBar, z);
		UpdateWindow();
	}

  OnScrollScaleChanged();
}
  
// adjust scrollbars etc
void CFMntScrollerView::UpdateBars()
{
	// UpdateBars may cause window to be resized - ignore those resizings
	if (m_bInsideUpdate)
		return;         // Do not allow recursive calls
	// Lock out recursion
	m_bInsideUpdate = TRUE;
	// update the horizontal to reflect reality
	// NOTE: turning on/off the scrollbars will cause 'OnSize' callbacks
	ASSERT(m_szTotalDev.cx >= 0 && m_szTotalDev.cy >= 0);
	CSize sizeClient;
	CSize sizeSb;

	if (!GetTrueClientSize(sizeClient, sizeSb))
	{
		// no room for scroll bars (common for zero sized elements)
		CRect rect;
		GetClientRect(&rect);
		if (rect.right > 0 && rect.bottom > 0)
		{
			// if entire client area is not invisible, assume we have
			//  control over our scrollbars
			EnableScrollBarCtrl(SB_BOTH, FALSE);
		}
		m_bInsideUpdate = FALSE;
		return;
	}
	// enough room to add scrollbars
	CSize sizeRange = m_szTotalDev - sizeClient;

	// > 0 => need to scroll
	CPoint ptMove = GetDeviceScrollPosition();
						// point to move to (start at current scroll pos)
	BOOL bNeedH = sizeRange.cx > 0;
	if (bNeedH)
		sizeRange.cy += sizeSb.cy;          // need room for a scroll bar
	else
		ptMove.x = 0;                       // jump back to origin
	BOOL bNeedV = sizeRange.cy > 0;
	if (bNeedV)
		sizeRange.cx += sizeSb.cx;
	else
		ptMove.y = 0;                       // jump back to origin
	if (bNeedV && !bNeedH && sizeRange.cx > 0)
	{
		// need a horizontal scrollbar after all
		bNeedH = TRUE;
		sizeRange.cy += sizeSb.cy;
	}
	// if current scroll position will be past the limit, scroll to limit
	if (sizeRange.cx > 0 && ptMove.x >= sizeRange.cx)
		ptMove.x = sizeRange.cx;
	if (sizeRange.cy > 0 && ptMove.y >= sizeRange.cy)
		ptMove.y = sizeRange.cy;
	// first scroll the window as needed
	ScrollToDevicePosition(ptMove); // will set the scroll bar positions too
	// now update the bars as appropriate
	// this structure needed to update the scrollbar page range
	SCROLLINFO info;
	info.fMask = SIF_PAGE|SIF_RANGE;
	info.nMin = 0;
	// now update the bars as appropriate
	// EnableScrollBarCtrl(SB_HORZ, TRUE);
	if (bNeedH)
	{
		EnableScrollBarCtrl(SB_HORZ, bNeedH);
		info.nPage = sizeClient.cx;
		//info.nMax  = m_szTotalDev.cx-1;
    info.nMax = sizeRange.cx;
		if (!SetScrollInfo(SB_HORZ, &info, TRUE))
			SetScrollRange(SB_HORZ, 0, sizeRange.cx, TRUE);
	} else
		EnableScrollBarCtrl(SB_HORZ, bNeedH);

	// EnableScrollBarCtrl(SB_VERT, TRUE);
	if (bNeedV)
	{
		EnableScrollBarCtrl(SB_VERT, bNeedV);
		info.nPage = sizeClient.cy;
		//info.nMax = m_szTotalDev.cy-1;
    info.nMax = sizeRange.cy;
		if (!SetScrollInfo(SB_VERT, &info, TRUE))
			SetScrollRange(SB_VERT, 0, sizeRange.cy, TRUE);
	} else
		EnableScrollBarCtrl(SB_VERT, bNeedV);

	// Remove recursion lockout
	m_bInsideUpdate = FALSE;
}

// size with no bars
BOOL CFMntScrollerView::GetTrueClientSize(CSize& size, CSize& sizeSb)
{
	CRect rect;	GetClientRect(&rect);
	MASSERT(rect.top == 0 && rect.left == 0);
	size.cx = rect.right;
	size.cy = rect.bottom;
	sizeSb.cx = sizeSb.cy = 0;
	DWORD dwStyle = GetStyle();
	// first calculate the size of a potential scrollbar
		// (scroll bar controls do not get turned on/off)
	if (GetScrollBarCtrl(SB_VERT) == NULL)
	{
		// vert scrollbars will impact client area of this window
		sizeSb.cx = GetSystemMetrics(SM_CXVSCROLL);
		if (dwStyle & WS_BORDER)
			sizeSb.cx -= 1;
		//if (dwStyle & WS_VSCROLL)
		//	size.cx += sizeSb.cx;       // currently on - adjust now
	}
	if (GetScrollBarCtrl(SB_HORZ) == NULL)
	{
		// horz scrollbars will impact client area of this window
		sizeSb.cy = GetSystemMetrics(SM_CYHSCROLL);
		if (dwStyle & WS_BORDER)
			sizeSb.cy -= 1;
		//if (dwStyle & WS_HSCROLL)
		//	size.cy += sizeSb.cy;       // currently on - adjust now
	}
	// return TRUE if enough room
	return (size.cx > sizeSb.cx && size.cy > sizeSb.cy);
}

/////////////////////////////////////////////////////////////////////////////
// CFMntScrollerView message handlers


void CFMntScrollerView::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	MASSERT(pScrollBar == GetScrollBarCtrl(SB_HORZ));    // may be null
	OnScrollBar(SB_HORZ, nSBCode, nPos );
}

void CFMntScrollerView::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	MASSERT(pScrollBar == GetScrollBarCtrl(SB_VERT));    // may be null
	OnScrollBar(SB_VERT, nSBCode, nPos );
}

void CFMntScrollerView::OnSize(UINT nType, int cx, int cy) 
{
	CView::OnSize(nType, cx, cy);
	if (m_nScale == SS_ISOTROPICFIT)
	{
		SetScrollScale(SS_ISOTROPICFIT);
	} else
	if (m_nScale == SS_ANISOTROPICFIT)
	{
		SetScrollScale(SS_ANISOTROPICFIT);
	} else  
	{
		// UpdateBars() handles locking out recursion
		UpdateBars();
	};
	CalcCurrentGrid();
}

BOOL CFMntScrollerView::OnEraseBkgnd(CDC* pDC) 
{
	CBrush  bkBrush(m_wndColor);
	CBrush *pOldBrush = pDC->SelectObject(&bkBrush);       
	CRect rect;	pDC->GetClipBox(&rect);
	pDC->PatBlt(rect.left,rect.top,rect.Width(),rect.Height(),PATCOPY);
	pDC->SelectObject(pOldBrush);
	return TRUE;
}

void CFMntScrollerView::OnCancelMode() 
{
	CancelMouseMode();
	CView::OnCancelMode();
}

void CFMntScrollerView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if ((nChar == VK_ESCAPE)&&(GetMouseMode() != MM_NORMAL))
		CancelMouseMode();
	CView::OnKeyDown(nChar,nRepCnt,nFlags);
}

void CFMntScrollerView::DoZoomInTracker(CPoint &point)
{
  CRect rZoomTo;
  if (GetUserTrackRectNew(rZoomTo,point,TRUE,FALSE))
  {	// do zoom in
    if ((rZoomTo.Width()!=0)&&(rZoomTo.Height()!=0))
    {   
      CRect r; GetClientRect(r);
      double scx = ((double)r.Width())*((double)m_rAreaLog.Width());
      scx /= (double)rZoomTo.Width();
      scx *= (double)SCALE_UNIT;
      scx /= (double)m_szTotalRDev.cx;

      double scy = ((double)r.Height())*((double)m_rAreaLog.Height());
      scy /= (double)rZoomTo.Height();
      scy *= (double)SCALE_UNIT;
      scy /= (double)m_szTotalRDev.cy;

      int nScale = (scx<scy)?((int)Round(scx,0)):((int)Round(scy,0));
      int nMinScale = GetMinScale();
      if (nScale<nMinScale) nScale = nMinScale;
      if (nScale>m_nMaxScale) nScale = m_nMaxScale;

      // ulo�en� historie
      // UpdateAllViews(uvfSaveCurrScale);
      // nastaven� m���tka v jin�ch okn�ch
      CFMntScrollUpdate cUpdate(uvfDoZoomIn,this);
      cUpdate.m_nParam = nScale;
      UpdateAllViews(&cUpdate);
      // nastaven� m���tka a polohy v tomto okn�
      SetScrollScale(nScale,&(rZoomTo.TopLeft()));
    }
  }
  CancelMouseMode();
}

void CFMntScrollerView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	CPoint ptMouse = WindowToScrollerPosition( point );
 
  switch(GetMouseMode())
  {
  case MM_ZOOM_IN:
    {
      DoZoomInTracker(point);
      break;
    }
  case MM_NORMAL:
    {
      // move object
      {
        if ((m_bEditSupport)&&(m_pEditParams != NULL))
        {	// aktivn� podpora editace
          // testuji zda bude operace pro tracker
          if (m_pEditInfo)
          {
            int nHandle;
            if ((m_pEditInfo->m_pSelTracker)&&
              ((nHandle = m_pEditInfo->m_pSelTracker->HitTest(point))>=0)&&
              (m_pEditInfo->CanObjectHit(nHandle)))
            {	// prov�d�m tracking objektu

              if (m_pEditInfo->m_bAutoScroll) 
                SetAutoScrollTrack(m_pEditInfo->m_pSelTracker);

              if (nFlags & MK_CONTROL || nFlags & MK_SHIFT)
                ((CTrackerBase *)m_pEditInfo->m_pSelTracker)->SetTrackMode(CTrackerBase::trRotate);
              else
                ((CTrackerBase *)m_pEditInfo->m_pSelTracker)->SetTrackMode(CTrackerBase::trOffset);


              BOOL bRes = m_pEditInfo->TrackObject(this,point);
              ((CTrackerBase *)m_pEditInfo->m_pSelTracker)->SetTrackMode(CTrackerBase::trOffset);

              RemoveAutoScrollTrack();
              if (bRes)
              {	// objekt zm�n�n
                m_pEditInfo->UpdateOnTrack(this);
              }
              return;
            }
            // ru��m v�b�r objektu
            //if (nFlags & MK_CONTROL) 
            {        
              CFMntScrollUpdate cUpdate(uvfEOUnselObject,this);
              UpdateAllViews(&cUpdate);
              //m_pEditParams->m_pSelObject = NULL;
              ASSERT(m_pEditInfo == NULL);
            }
          }
          // hled�m objekt pro v�b�r
        
          { 
            if (EOSearchEditObject(ptMouse))
            {	// je nalezen objekt->vyberu ho
              CFMntScrollUpdate cUpdate(uvfEOSelObject,this);
              UpdateAllViews(&cUpdate);
            } else
              if (m_pEditInfo)
              {	// zru�en� v�b�ru objektu
                CFMntScrollUpdate cUpdate(uvfEOUnselObject,this);
                UpdateAllViews(&cUpdate);
                // konec informac� o objektu
                //m_pEditParams->m_pSelObject = NULL;
              }
          }
        }
      } 
      break;
    }
  default:
    CView::OnLButtonDown(nFlags, point);
  }
}

void CFMntScrollerView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// ru��m m�d my�i
  if (GetMouseMode() != MM_PULL)
    SetMouseMode(MM_PULL);

	CView::OnRButtonDown(nFlags, point);
}

void CFMntScrollerView::OnRButtonUp(UINT nFlags, CPoint point)
{
  CancelMouseMode();
  CView::OnRButtonUp(nFlags, point);
}

void CFMntScrollerView::OnMouseMove(UINT nFlags, CPoint point) 
{
  if (nFlags & MK_RBUTTON)
  {
    if (GetMouseMode() != MM_PULL)
      SetMouseMode(MM_PULL);
  }
  else
  {
    if (GetMouseMode() == MM_PULL)
      SetMouseMode(MM_NORMAL);
  }

	if (GetMouseMode() == MM_PULL)
  {
    // set scroll so that m_cPullPointLog is under mouse pointer
    POINT CursorPos;    
    GetCursorPos(&CursorPos);
    ScreenToClient(&CursorPos);

    CRect rClient;
    GetClientRect(&rClient);

    if (rClient.PtInRect(CursorPos))    
    {
      // center to mouse point
      CPoint cCursorPosLog = CursorPos;
      cCursorPosLog += GetDeviceScrollPosition();
      cCursorPosLog = FromDeviceToScrollPosition(cCursorPosLog);
      cCursorPosLog = GetScrollPosition() - cCursorPosLog + m_cPullPointLog;

      ScrollToPosition(cCursorPosLog);
    }    
  }

	CView::OnMouseMove(nFlags, point);
}

void CFMntScrollerView::KeepVisibleLog(POINT pt)
{
  CRect rClient = GetLogClientRect();

  rClient.DeflateRect(rClient.Width() /10, rClient.Height() /  10);

  if (rClient.PtInRect(pt)) //nothing to do.
    return;

  CPoint  cDiff(0,0);
  
  if (pt.x <= rClient.left)
    cDiff.x =  pt.x -  rClient.left;
  else
    if (pt.x >= rClient.right)
      cDiff.x =  pt.x -  rClient.right;

  if (pt.y <= rClient.top)
    cDiff.y =  pt.y -  rClient.top;
  else
    if (pt.y >= rClient.bottom)
      cDiff.y =  pt.y -  rClient.bottom;

  cDiff = GetScrollPosition() + cDiff;

  ScrollToPosition(cDiff);
}

void CFMntScrollerView::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent == MNTL_TIMER_3)
	{	// reset auto-scroll flag
		if (m_pScrollTracker!=NULL) 
			m_pScrollTracker->OnAutoScrollTimer();
	} else
		CView::OnTimer(nIDEvent);
}

void CFMntScrollerView::SetCursorFromMouseMode()
{
  HCURSOR hCursor = NULL;
  switch(GetMouseMode())
  {
  case MM_NORMAL:
    {	
      // testuji editovan� objekt
      if (m_pEditInfo)
      {
        // convert cursor position to client co-ordinates
        CPoint point; GetCursorPos(&point);
        ScreenToClient(&point);
        if (m_pEditInfo->m_pSelTracker)
        {
          int nHandle = m_pEditInfo->m_pSelTracker->HitTest(point);
          if (m_pEditInfo->CanObjectHit(nHandle))
          {
            if (m_pEditInfo->m_pSelTracker->SetCursor(this,point)) 
              return ;
          }
        }
      }
      // jinak kurzor pro scroller				
      hCursor = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_MSCR_ARROW));
      break;
    }
  case MM_ZOOM_IN:
    {
      hCursor = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_MSCR_ZOOM_IN));
      break;
    }
  case MM_PULL:
    hCursor = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_MSCR_HMOVE));
    break;
  case MM_BGIMAGE_AREA_ADD:
    hCursor = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_MSCR_ARROW));
    break;
  }
  if (hCursor!=NULL)
  {
    ::SetCursor(hCursor); 
  }

}

BOOL CFMntScrollerView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	// pouze v tomto okn�
	if ((pWnd==this)&&(nHitTest==HTCLIENT))
	{ // upravuji pouze v Client plo�e
    SetCursorFromMouseMode();
    return TRUE;
	}
	return CView::OnSetCursor(pWnd, nHitTest, message);
}

/////////////////////////////////////////////////////////////////////////////
// podpora pro v�b�r r�me�ku a ��ry


BOOL CFMntScrollerView::GetUserTrackRectNew(CRect& rect,CPoint point,
							  BOOL bClipClient,BOOL bClipLogClient,
                AlignMode eAlignGrid, const CPoint& alignSquareSizeLog, const CPoint& alignSquareOffsetLog, BOOL bAllowInver)
{
	// testuji, zda po��te�n� bod je na plo�e
	CPoint logMouse  = WindowToScrollerPosition(point);
	if (bClipLogClient)
	{
		CRect  logClient = GetLogClientRect();
		if (!logClient.PtInRect(logMouse)) return FALSE;
	}
	// nastaven� a uschova - rect objektu
	BOOL	bOldValidRect  = m_bValidObjRect;
	CRect	rOldObjectRect = m_rLastPos;
	m_bValidObjRect = TRUE;
	m_rLastPos.SetRect(logMouse.x,logMouse.y,logMouse.x,logMouse.y);
	// vytv���m rect od dan�ho bodu
	CFMntScrollerTrackerRect tracker(this);
	tracker.SetTrackParams(0,bClipClient,eAlignGrid /*&& m_bAlignGrid*/,
    alignSquareSizeLog, alignSquareOffsetLog);
  tracker.m_finalRect = tracker.m_logRect = rect;  
	// set timer for auto-scroll timing
	SetAutoScrollTrack(&tracker);
  //BOOL bRes = tracker.Track(this,point);
	BOOL bRes = tracker.TrackRubberBand(this,logMouse,bAllowInver);
	// remove auto-scroll timer
	RemoveAutoScrollTrack();
	// zpracuji v�sledek
	if (bRes)
	{
		rect.CopyRect(tracker.m_finalRect);
	}
	// navr�cen� rectu
	m_bValidObjRect = bOldValidRect;
	m_rLastPos = rOldObjectRect;
	return bRes;
};

BOOL CFMntScrollerView::UserTrackRectOnSetCursor(CRect& devRect, CPoint& point)
{
  // testuji, zda po��te�n� bod je na plo�e
  CFMntScrollerTrackerRect tracker(this);
  tracker.SetTrackParams(0,FALSE, fstAlignNone);
  tracker.m_devRect = devRect; 
  return tracker.SetCursor(this, point);
}

BOOL CFMntScrollerView::GetUserTrackRect(CRect& rect,CPoint point,
                                            BOOL bClipClient,BOOL bClipLogClient,
                                            AlignMode eAlignGrid, BOOL bAllowInver)
{
  // testuji, zda po��te�n� bod je na plo�e
  CPoint logMouse  = WindowToScrollerPosition(point);
  if (bClipLogClient)
  {
    CRect  logClient = GetLogClientRect();
    if (!logClient.PtInRect(logMouse)) return FALSE;
  }
  // nastaven� a uschova - rect objektu
  BOOL	bOldValidRect  = m_bValidObjRect;
  CRect	rOldObjectRect = m_rLastPos;
  m_bValidObjRect = TRUE;
  m_rLastPos.SetRect(logMouse.x,logMouse.y,logMouse.x,logMouse.y);
  // vytv���m rect od dan�ho bodu
  CFMntScrollerTrackerRect tracker(this);
  tracker.SetTrackParams(0,bClipClient,eAlignGrid /*&& m_bAlignGrid*/);
  tracker.m_finalRect = tracker.m_logRect = rect;  
  // set timer for auto-scroll timing
  SetAutoScrollTrack(&tracker);
  BOOL bRes = tracker.Track(this,point);
  //BOOL bRes = tracker.TrackRubberBand(this,logMouse,bAllowInver);
  // remove auto-scroll timer
  RemoveAutoScrollTrack();
  // zpracuji v�sledek
  if (bRes)
  {
    rect.CopyRect(tracker.m_finalRect);
  }
  // navr�cen� rectu
  m_bValidObjRect = bOldValidRect;
  m_rLastPos = rOldObjectRect;
  return bRes;
};

void CFMntScrollerView::AdjustTrackRect( CFMntScrollerTracker*,int nHandle,CRect& atRect,
				 	  LPCRECT pClipRect,AlignMode eGridAlign,
					  BOOL bAllowInvert)
{
/*
	// vol�m EOAdjustTrackRect s p��slu�n�mi parametry
	CEOInfoObject* pInfo = NULL;
	if ((m_pEditInfo)&&(m_pEditInfo->m_pTracker == pTracker))
		pInfo = m_pEditInfo;
	// vol�m zarovn�n� pro objekt
	CRect rClip(pClipRect);
	if ((m_pEditInfo)&&(!m_pEditInfo->m_bClipInClient))
		  rClip = m_areaLog;
	EOAdjustTrackRect(pInfo,nHandle,atRect,rClip,bGridAlign,bAllowInvert);
	// zobrazen� rectu objektu
	if (m_bValidObjRect)
	{
		m_rLastPos = atRect;
	}
*/
};

void CFMntScrollerView::SetAutoScrollTrack(CFMntScrollerTracker* pTracker)
{
	RemoveAutoScrollTrack();
	if ((pTracker==NULL)||(!m_bAutoScroll)) return;
	pTracker->m_bAutoScrTimer = FALSE; // �ek�m na timer
	// set timer for auto-scroll timing
	SetTimer(MNTL_TIMER_3,TIMER_AUTOSCROLL_DELAY,NULL);
	m_pScrollTracker = pTracker;
}

void CFMntScrollerView::RemoveAutoScrollTrack()
{
	if (m_pScrollTracker)
	{
		// remove auto-scroll timer
		m_pScrollTracker = NULL;
		KillTimer(MNTL_TIMER_3);
	}
}

/////////////////////////////////////////////////////////////////////////////
// operace prov�d�n� u�ivatelem



void CFMntScrollerView::OnSetScale(int nScale,BOOL bResetHist)
{
	/*if (nScale==SS_ISOTROPICFIT)
	{
		if (m_nScale == SS_ISOTROPICFIT)
		{
			UpdateAllViews(uvfLastCurrScale); // p�vodn� m���tko
			return;
		}
		ASSERT(!bResetHist);
	} else
	if (nScale==SS_ANISOTROPICFIT)
	{
		if (m_nScale == SS_ANISOTROPICFIT)
		{
			UpdateAllViews(uvfLastCurrScale); // p�vodn� m���tko
			return;
		}
		ASSERT(!bResetHist);
	} else*/
	if (!CanSetScale(nScale))
		return;

	if (nScale == m_nScale)
	{	// stejn� m���tko, nen� co m�nit
		//if (bResetHist)
		//	UpdateAllViews(uvfResetHisScale);
		return;
	}
	// uchov�n� nastaven� pro ZoomOut
	//UpdateAllViews(bResetHist?uvfResetHisScale:uvfSaveCurrScale);  
	// nastaven� m���tka
	CFMntScrollUpdate cUpdate(uvfChangeScale,this);
	cUpdate.m_nParam = nScale;
	UpdateAllViews(&cUpdate);
};

void CFMntScrollerView::OnUpdateSetScale(int nScale,CCmdUI* pCmdUI)
{
	if (nScale==SS_ISOTROPICFIT)
	{
		pCmdUI->Enable(TRUE);
		pCmdUI->SetCheck(m_nScale == SS_ISOTROPICFIT);
	} else
	if (nScale==SS_ANISOTROPICFIT)
	{
		pCmdUI->Enable(TRUE);
		pCmdUI->SetCheck(m_nScale == SS_ANISOTROPICFIT);
	} else
		pCmdUI->Enable(CanSetScale(nScale));
};

// nastaven� m��ky
void CFMntScrollerView::OnSetGrid(BOOL bShow, BOOL bAlign, CSize& sizeGrid)
{	// nastaven� m���tka
	CFMntScrollUpdate cUpdate(uvfChangeGrid,this);
	cUpdate.m_nParam1 = bShow ? 1 : 0;
	cUpdate.m_nParam2 = bAlign ? 1 : 0;
	cUpdate.m_rPlace.left = sizeGrid.cx;
	cUpdate.m_rPlace.top  = sizeGrid.cy;
	UpdateAllViews(&cUpdate);
};
BOOL CFMntScrollerView::IsActiveGrid() const
{	return (m_bShowGrid && m_bAlignGrid); };
void CFMntScrollerView::OnActiveGridChange()
{
	if (IsActiveGrid())
		OnSetGrid(FALSE, FALSE, m_gridStep);
	else
		OnSetGrid(TRUE, TRUE, m_gridStep);
};
void CFMntScrollerView::OnUpdateActiveGridChange(CCmdUI* pCmdUI)
{	pCmdUI->Enable(TRUE);
	pCmdUI->SetCheck(IsActiveGrid()); };

void CFMntScrollerView::OnShowGridChange()
{	OnSetGrid(!m_bShowGrid, m_bAlignGrid, m_gridStep); }
void CFMntScrollerView::OnUpdateShowGridChange(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(m_bShowGrid);
	pCmdUI->Enable(TRUE);
};

void CFMntScrollerView::OnAlignGridChange()
{	OnSetGrid(m_bShowGrid, !m_bAlignGrid, m_gridStep); }
void CFMntScrollerView::OnUpdateAlignGridChange(CCmdUI* pCmdUI)
{	pCmdUI->SetCheck(m_bAlignGrid);
	pCmdUI->Enable(TRUE);
};

// nastaven� barev
void CFMntScrollerView::OnSetBackground(COLORREF cColor)
{
	CFMntScrollUpdate cUpdate(uvfChangeBkColor,this);
	cUpdate.m_nParam1 = (DWORD)cColor;
	UpdateAllViews(&cUpdate);
};

// nastaven� m�du my�i
/*void CFMntScrollerView::OnSetMouseMode(WORD nMode)
{
	CFMntScrollUpdate cUpdate(uvfSetMouseMode,this);
	cUpdate.m_nParam1 = (DWORD)nMode;
	UpdateAllViews(&cUpdate);
};*/


/////////////////////////////////////////////////////////////////////////////
// podpora editace

BOOL CFMntScrollerView::DoSelectObject()
{
	ASSERT(m_bEditSupport);
	ASSERT(m_pEditParams != NULL);	
	// jestli�e jsou stejn� - ignoruji
	if ((m_pEditInfo!=NULL)&&(m_pEditInfo->m_pSelObject == m_pEditParams->m_pSelObject)) return TRUE;
	// zru��m selekci p�edchoz�ho objektu
	if (!DoUnselectObject())
		return FALSE;
	// sm�m vybrat objekt ???
	TRY 
	{
		if (m_pEditParams->m_pSelObject != NULL)
		{
			ASSERT( m_pEditInfo == NULL );
			// vyto��m m_pEditInfo
			m_pEditInfo = (CFMntScrollEditInfo*)((*m_pEInfoClass->m_pfnCreateObject)());
			if (m_pEditInfo == NULL) AfxThrowMemoryException();
			// v�b�r a update parametr� objektu
			if (!EOSelectObject())
			{	// nen� povolen v�b�r objektu
				delete m_pEditInfo; m_pEditInfo = NULL;	
				return FALSE;
			}
			// dotaz na parametry objektu
			EOUpdateInfo();
			// nastaven� trackeru
			m_pEditInfo->CreateTracker(this);
		}
	}
	CATCH_ALL(e)
	{
		// zru��m selekci
		if (m_pEditInfo) { delete m_pEditInfo; m_pEditInfo = NULL; };
		// Out of Memory		
		MntReportMemoryException();
		return FALSE;
	}
	END_CATCH_ALL
	return TRUE;
};

BOOL CFMntScrollerView::DoUnselectObject()
{
  if (m_pEditInfo == NULL) return TRUE;  // nen� co likvidovat

	ASSERT(m_bEditSupport);
	ASSERT(m_pEditParams != NULL);	
	// likvidace v�b�ru objektu	
	if (!EOUnselectObject()) return FALSE; // zak�z�no odsel. objektu

	// zru��m tracker
	m_pEditInfo->DestroyTracker(this);
	
	// provedu odselektov�n�
	delete m_pEditInfo; m_pEditInfo = NULL;
  //m_pEditParams->m_pSelObject = NULL;
	return TRUE;
};

// pr�ce s m_pEditParams
BOOL CFMntScrollerView::EOSearchEditObject(CPoint ptAt) 
{ return FALSE; };

// vytvo�� m_pEditInfo podle m_pEditParams
BOOL CFMntScrollerView::EOSelectObject()	
{ return FALSE; };
// validace zru�en� v�b�ru objektu
BOOL CFMntScrollerView::EOUnselectObject()
{ return TRUE; };

// �daje o objektu (vrac� p��znak zm�ny �daj�)
BOOL CFMntScrollerView::EOUpdateInfo()
{ return FALSE; };

// objekt byl zm�n�n trackerem->p�izp�sob� se
BOOL CFMntScrollerView::EOObjectFromTracker()	
{ return TRUE; };

// p�epis rectu v log. sou�adnic�ch
void CFMntScrollerView::EOInvalidateLogRect(CRect& rRect,BOOL bErase) 
{ 
	CFMntScrollUpdate cUpdate(uvfEOInvalRect,this);
	cUpdate.m_rPlace = rRect;
	cUpdate.m_nParam = bErase ? 1 : 0;
	UpdateAllViews(&cUpdate);
};

// implementace p��m� editace
BOOL CFMntScrollerView::EOEditObject()
{
	return FALSE;
}

BOOL CFMntScrollerView::EOCanEditObject()
{	// mus� b�t vybr�n objekt
	return ((m_pEditInfo != NULL) && (m_pEditInfo->m_pSelObject != NULL));
}

// provede editaci objektu
void CFMntScrollerView::DoEditObject(POINT* pPopUp)	
{	// def. provede p��mou editaci
	if (!EOCanEditObject()) return;	
	// provedu editaci
	if (EOEditObject()) UpdateEditChanges();
}

void CFMntScrollerView::UpdateEditChanges(BOOL bDelObj)
{
	if ((m_pEditInfo != NULL) && (m_pEditInfo->m_pSelObject != NULL))
	{
		CFMntScrollUpdate cUpdate(uvfEOUpdtChngObj, this);
		cUpdate.m_pObject= m_pEditInfo->m_pSelObject;
		cUpdate.m_nParam = bDelObj ? 1 : 0;
		UpdateAllViews(&cUpdate);
	}
}

///////////////////////////////////////////////////
/// Rounds
///////////////////////////////////////////////////

int RoundStep(int style)
{
	switch (style)
	{
		case SR_1:
			return 1;
		case SR_10:
			return 10;
		case SR_100:
			return 100;
		case SR_1000:
			return 1000;
		case SR_5:
			return 5;
		default:
			return 0;
	}
}

double RoundStepF(int style)
{
	switch (style)
	{
		case SR_1:
			return 1;
		case SR_10:
			return 10;
		case SR_100:
			return 100;
		case SR_1000:
			return 1000;
		case SR_5:
			return 5;
		case SR_05:
			return 0.5;
		case SR_025:
			return 0.25;
		case SR_01:
			return 0.1;
		case SR_005:
			return 0.05;
		case SR_0025:
			return 0.025;
		case SR_001:
			return 0.01;
		case SR_0001:
			return 0.0001;
		default:
			return 0;
	}
}

int SpecRound(int x, int style)
{
	int nRound = RoundStep(style);
	if (nRound == 0) return x;

	return ((int) ((x + nRound / 2.0f) / nRound)) * nRound;
}

long int SpecRound(long int x, int style)
{
	int nRound = RoundStep(style);
	if (nRound == 0) return x;

	return ((long int) ((x + nRound / 2.0) / nRound)) * nRound;
}
	
double SpecRound(double x, int style)
{
	double nRound = RoundStepF(style);
	if (nRound == 0) return x;

	return  ((long int) ((x + nRound / 2.0) / nRound)) * nRound;
}

long double SpecRound(long double x,int style)
{
	double nRound = RoundStepF(style);
	if (nRound == 0)
		return x;

	return ((long int) ((x + nRound/2.0)/nRound)) * nRound;
}

double Round(double x,int ex) {
  //Assert(ex == 0); // Cos' I dunno what to hell it means
  double integer;
  modf(x + (( x >= 0) ? 0.5 : -0.49), &integer);
  return integer;
};

long double Roundl(long double x,int ex) {
  //Assert(ex == 0); // Cos' I dunno what to hell it means
  long double integer;
  modfl(x + (( x >= 0) ? 0.5 : -0.49), &integer);
  return integer;
};

//////////////////////////////////////////////////


BOOL MntDeleteFile(LPCTSTR pFileName)
{
	TRY
	{
    CFile::Remove( pFileName );
		return TRUE;
	}
	CATCH( CFileException, e )
	{
		ASSERT(FALSE);
		return FALSE;
	}
	END_CATCH
}

void DestroyListObjects(CObList* pToReset)
{
	if (pToReset == NULL) return;

	POSITION pos = pToReset->GetHeadPosition();
	while (pos != NULL)
	{
		CObject* pObject = pToReset->GetNext(pos);
		delete pObject;
	}

	pToReset->RemoveAll();
}

void DestroyArrayObjects(CObArray* pToReset) 
{
	for (int i = 0; i < pToReset->GetSize(); ++i)
	{
		delete (*pToReset)[i];
	}

	pToReset->RemoveAll();
}

//////////////////////////////////////////////////
// BITMAPS
 
SIZE GetBitmapSize(HBITMAP hBmp)
{
	BITMAP Info;
	SIZE size;
	size.cx = size.cy = 0;
	
	if (0 <= GetObjectA(hBmp, sizeof(Info), &Info))
	{
		size.cx = Info.bmWidth;
		size.cy = Info.bmHeight;
	}
	return size;
}


void  PaintSpecBitmapRect(HDC hDC,HBITMAP hBmp,
								 RECT  To,
                 POINT From,SIZE Size,DWORD Style)
{
	// centralize
	int x = To.top;
	int y = To.left;
	if ((To.right - To.left) > Size.cx)
	{
		x += ((To.right - To.left) - Size.cx)/2;
	}

	if ((To.bottom - To.top) > Size.cy)
	{
		y += ((To.bottom - To.top) - Size.cy)/2;
	}
	PaintSpecBitmapRect(hDC, hBmp, x, y, From, Size, Style);
}

void  PaintSpecBitmapRect(HDC hDC,HBITMAP hBmp,
								 int x,int y,
                 POINT From,SIZE Size,DWORD Style)
{
	HDC hDCSrc = CreateCompatibleDC(hDC);

	HBITMAP hOld = (HBITMAP) SelectObject(hDCSrc, hBmp);

	BitBlt(hDC, x, y, Size.cx, Size.cy, hDCSrc, From.x, From.y, Style);
 
	SelectObject(hDCSrc, hOld);
	DeleteDC(hDCSrc);
	
	return;
}

//////////////////////////////////////////////
// Chars
LPCTSTR	CutLeftChar( LPTSTR pDest, LPCTSTR pSrc, TCHAR ch )
{
	LPCTSTR pSrcIt = pSrc;
	int i = 0;
	while (pSrcIt[i] != _T('\0') && pSrcIt[i] == ch)
		i++;

	_tcsncpy(pDest, pSrc + i, _tcslen(pSrc) - i);
	pDest[_tcslen(pSrc) - i] = _T('\0');
	return pDest;
}

LPCTSTR	CutRightChar( LPTSTR pDest, LPCTSTR pSrc, TCHAR ch )
{
	LPCTSTR pSrcIt = pSrc;
	int i = _tcslen(pSrc);
	while (pSrcIt[i] != _T('\0') && pSrcIt[i] == ch)
		i--;

	_tcsncpy(pDest, pSrc, _tcslen(pSrc) - i);
	pDest[_tcslen(pSrc) - i] = _T('\0');
	return pDest;
}



///////////////////////////////////////////////////////
// Errors
void MntReportMemoryException(int strID) 
{ 
	if (strID < 0)
		AfxMessageBox( IDS_NO_MEMORY, MB_OK|MB_ICONEXCLAMATION );
	else
		AfxMessageBox( strID, MB_OK|MB_ICONEXCLAMATION );

	AfxThrowUserException();
};

void MntInvalidFileFormat(LPCTSTR pFileName) 
{
	CString str; 
	str.Format(IDS_WRONG_FILEFORMAT, pFileName == NULL ? _T("") : pFileName);
	AfxMessageBox( str, MB_OK|MB_ICONEXCLAMATION );
	str.Empty();

	AfxThrowUserException();
};							
BEGIN_MESSAGE_MAP(CFMntMenuListBox, CListBox)
  ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()

///////////////////////////////////////////////////
// CFMntFromToFile
///////////////////////////////////////////////////


  
void CFMntFromToFile::StartFromToSession(ULONGLONG from, ULONGLONG to, const CString& origin)
{
  _from = from;
  _to = to;
  _realto = _from;
  _originFile = origin;

  if (_from == 0)
    return;

  // copy data from source file into mirrorfile
  base::Seek(0,CFile::begin);

  CFile oldFile;
  if (!oldFile.Open(_originFile, CFile::modeRead))
  {
    RptF( "CFMntFromToFile::StartFromToSession: Open of origin file(%s) failed", _originFile);
    return;
  }

  // alloc buffer
  BYTE * pBuffer = new BYTE[(UINT) _from];
  if (!pBuffer)
    return;

  oldFile.Read(pBuffer, (UINT) _from);
  oldFile.Close();

  base::Write(pBuffer, (UINT) _from);

  delete [] pBuffer;
}
  

void CFMntFromToFile::EndFromToSession(ULONGLONG &to)
{
  to = _realto;

  // copy data from source file into mirrorfile
  base::Seek(_realto,CFile::begin);

  CFile oldFile;  
  if (!oldFile.Open(_originFile, CFile::modeRead))
  {
    _from = 0;
    _to = _UI64_MAX;
    RptF( "CFMntFromToFile::EndFromToSession: Open of origin file(%s) failed", _originFile);
    return;
  }

  // alloc buffer
  ULONGLONG endPos = oldFile.GetLength();
  if (endPos <= _to)
  {
    _from = 0;
    _to = _UI64_MAX;
    oldFile.Close();
    return;
  }

  UINT dataSize = (UINT) (endPos - _to);
  if (dataSize != 0)
  {
    BYTE * pBuffer = new BYTE[(UINT) (endPos - _to)];
    if (!pBuffer)
    {
      _from = 0;
      _to = _UI64_MAX;
      oldFile.Close();
      return;
    }

    oldFile.Seek(_to, CFile::begin);
    oldFile.Read(pBuffer, dataSize);

    base::Write(pBuffer, dataSize);
    delete [] pBuffer; 
  }

  oldFile.Close();

  _from = 0;
  _to = _UI64_MAX;

}

ULONGLONG CFMntFromToFile::Seek(LONGLONG lOff, UINT nFrom)
{
  if (nFrom == CFile::begin)
    lOff += _from;

  ULONGLONG pos = base::Seek(lOff, nFrom);
  if(pos > _realto)
    _realto = pos;

  ASSERT(pos >= _from);

  return pos - _from;
}
  

void CFMntFromToFile::Write(const void* lpBuf, UINT nCount)
{
  base::Write(lpBuf, nCount);
  ULONGLONG pos = base::Seek(0, CFile::current);

  if (pos > _realto)
   _realto = pos;
};

///////////////////////////////////////////////////////
// COLORs
//////////////////////////////////////////////////////

COLORREF RGBToY(const COLORREF& rgb)
{
  BYTE Y = (BYTE) (0.299f*GetRValue(rgb)+0.587f*GetGValue(rgb)+0.114f*GetBValue(rgb));
  Y = 127 + Y/2;
  return RGB(Y,Y,Y);
}
