#ifndef _FMntTools_h_
#define _FMntTools_h_

#include <el/ParamFile/paramFile.hpp>
#include <es/Containers/array.hpp>
#include <afxpriv.h>

//#define MTRACE              ::MntNoTrace
#define MASSERT(f)          Assert(f)
#define MVERIFY(f)          Verify(f)

///////////////////////////////////////////////////////
// CMnt*File
//////////////////////////////////////////////////////
// soubory realizujfcf heslov�nf a kompresi
class CMntCompressFile : public CFile
{
public:
					CMntCompressFile();
	virtual			~CMntCompressFile();

	enum cmpType
	{	compressNone = 0 };

			BOOL	SetCompress(BYTE nType);

	virtual UINT	Read(void* lpBuf,UINT nCount);
	virtual void	Write(const void* lpBuf,UINT nCount);

// data
public:
	BYTE	m_nType;
};

#define MAX_PSW_LENGTH	30
class CMntPasswordFile : public CMntCompressFile
{
public:
					CMntPasswordFile();
	virtual			~CMntPasswordFile();

	enum pswType
	{	pswNone = 0, pswXor };

			BOOL	SetPassword(BYTE nType,LPCSTR pPassword);

	virtual UINT	Read(void* lpBuf,UINT nCount);
	virtual void	Write(const void* lpBuf,UINT nCount);

// data
public:
	BYTE	m_nType;
	char	m_sPassword[MAX_PSW_LENGTH+1];
	// pr�b8h komprese
	int		m_nPswLen;
	int		m_nPswPos;
};

// z�kladn� t��da pro implementaci serializace
#define MNT_FILE_ID_LEN	5
class CMntFileObject : public CObject
{
DECLARE_DYNAMIC(CMntFileObject);
public:

  CMntFileObject();
  CMntFileObject(LPCTSTR pFileName,WORD nFlags = 0,LONG nOffset = 0);
  virtual			~CMntFileObject();

  void	SetFile(LPCTSTR pFileName,LONG nOffset = 0);
  void	SetParams(WORD nFlags, LPCTSTR pFileID = NULL);

	// serializace objektu (implementuj� potomci)
  virtual	void	DoSerialize(CArchive& ar, DWORD nVer, void* pParams) {};
  virtual	void	OnSerialize(BOOL bSave, DWORD nVer, void* pParams) {};	

	// serializace souboru
  virtual	BOOL	OnLoadObject(void* pParams,DWORD nVersion = 0);
  virtual	BOOL	OnSaveObject(void* pParams,DWORD nVersion = 0);

			void	OnReportSerException(CException* pEx,
        BOOL bSaving, UINT nIDSDefault) const {};
	static	void	OnReportSerExceptionFile(CException* pEx,
    BOOL bSaving, UINT nIDSDefault, LPCTSTR pFilename) {};

	// vrac� z�kladn�ch informace ze souboru (bez jeho na�ten�)
	// 0 = OK, 1 = ERR, 2 = PSW, 3 = OLD
  virtual	WORD	ReadFileObjectName(DWORD nVersion = 0);

	// nastaven� modifikace
  virtual	void	SetModified(BOOL bModif = TRUE);


	// p��znaky pou��v�n� objektu
	enum fileFlags
	{
		fileID		= 0x0001,	// ukl�dat a kontrolovat m_cFileID
		fileVer		= 0x0002,	// ukl�dat a kontrolovat verzi
		fileIDEnd	= 0x0004,	// ukl�dat a kontrolovat m_cFileID na konci
		filePassword= 0x0010,	// ukl�dat a na��tat s heslem m_sFilePassword
		fileQueryPsw= 0x0020,	// p�i na��t�n� dotaz na heslo ?
		fileVerifPsw= 0x0040,	// p�i na��t�n� kontroovat heslo na m_cFileID
		fileCompress= 0x0080,	// prov�d�na komprese souboru
		fileObjName = 0x0100,	// ukl�dat a poskytovat m_sFileObjectName
		fileObjDesr = 0x0200,	// ukl�dat a poskytovat m_sFileObjectDescription
	};
// data souboru
public:
	WORD	m_nFlags;		// p��znaky pou��v�n� - kombinace fileFlags
protected:
	CString m_sFileName;	// jm�no souboru 
public:
	LONG	m_nFilePos;		// pozice v souboru
	BOOL	m_bModified;	// p��znak modifikace

	// podm�n�n� data, dle m_nFlags - mus� nastavit u�ivatel
	char	m_cFileID[MNT_FILE_ID_LEN];	// identifikace souboru
	DWORD	m_nFileVersion;				// verze souboru
	CString m_sFileObjectName;			// jm�no objektu
	CString m_sFileObjectDescription;	// koment�� objektu
	CString m_sFilePassword;			// heslo souboru
};


// Podpora options pro programy	(MntOptions.cpp) - DUMMY IMPLEMENTATION
// CMntOprions are replaced by CFMntOptions. This implementations is only for CMgrTemplates class
// do not use it elsewhere.
class CMntOptions : public CMntFileObject
{
DECLARE_DYNAMIC(CMntOptions)
public:		
	// styl options
	enum optStyle
	{
		optNone	   = 0, optFile   = 1,
		optPrgKey  = 2, optUsrKey = 3
	};
	// constructor
  CMntOptions();
  virtual ~CMntOptions();

  virtual void SetStyle(WORD nStyle,LPCTSTR pRoot,LPCTSTR pDef = NULL);
protected:
	WORD	m_nStyle;	// optStyle
	CString	m_sRoot;	// Root key or filename
	CString	m_sDefault;	// def., nen�-li v root

	// parametry z�pisu a �ten� options do registru
	BOOL	m_bSave;	// aktu�ln� styl UpdateParams
	HKEY	m_hKey;		// aktu�ln� kl�� pro z�pis
};

//////////////////////////////////////////////
// CDockState
//////////////////////////////////////////////

class CFMntDockState : public CDockState
{
  DECLARE_DYNAMIC(CFMntDockState)
protected:
  RStringB m_cClassName;
  ParamClassPtr m_pParentClass;

public:
  CFMntDockState() : CDockState() {};
  ~CFMntDockState() {ReleaseInOutParamFile();};

  virtual BOOL	LoadParams(BOOL bReportErr = TRUE);
  virtual BOOL	SaveParams(BOOL bReportErr = TRUE); 
  virtual void Flush() const {};

  void SetInOutParamFile(const char * pszClassName, ParamClassPtr pParentClass)  
  {m_cClassName = pszClassName; m_pParentClass = pParentClass;}

  void ReleaseInOutParamFile() 
  {  m_pParentClass = NULL;
  }
};

//////////////////////////////////////////////
// Scrolls & comp.
//////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CFMntScrollerTracker

#define SS_MINSCALE			1
#define SS_MAXSCALE     100000
#define SS_REALSCALE    	1000
#define SCALE_UNIT 1000000

#define SS_NOSCALE			0 
#define SS_ISOTROPICFIT     (-1)
#define SS_ANISOTROPICFIT   (-2)

// MouseModes for scroller
#define MM_NORMAL			0
#define MM_ZOOM_IN			1
#define MM_PULL      2

// Zoom history
#define NUM_ZOOM_HISTORY	10
typedef struct
{
	int		nScale;
	POINT	ptOrigin;

}	SZoomHistory;

enum AlignMode {
  fstAlignNone = 0,
  fstAlignGrid = 1,
  fstAlignSquare = 2,
  fstAlignVertex = 3,
};

class CFMntScrollerView;
class CFMntScrollerTracker : public CObject
{
DECLARE_DYNAMIC(CFMntScrollerTracker)
public:
// Style Flags
	enum StyleFlags
	{
		solidLine = 1, dottedLine = 2, hatchedBorder = 4,
		resizeInside = 8, resizeOutside = 16, hatchInside = 32,
		resizeStart = 64, resizeStop = 128
	};
	enum TrackerHit
	{
		hitNothing = -1,
		hitTopLeft = 0, hitTopRight = 1, hitBottomRight = 2, hitBottomLeft = 3,
		hitTop = 4, hitRight = 5, hitBottom = 6, hitLeft = 7, hitMiddle = 8,
	};
	enum LineTrackerHit
	{
		hitPtStart = 0, hitPtStop = 1, hitPtMiddle = 2, hitAllLine = 3,
		hitPtNew   = 4
	};

					// Constructors
  CFMntScrollerTracker(CFMntScrollerView*);
	virtual			~CFMntScrollerTracker() { };

// Operations
	virtual void	Draw(CDC* pDC) = 0;
	virtual int		HitTest(CPoint point) const { return hitNothing; };
	virtual BOOL	Track(CWnd* pWnd, CPoint point, BOOL bAllowInvert = FALSE,
			   			  CWnd* pWndClipTo = NULL) = 0;
	virtual void	GetTrueRect(LPRECT lpTrueRect) const = 0;
	virtual int		NormalizeHit(int nHandle) const { return nHandle; };

  BOOL	SetCursor(CWnd* pWnd, CPoint& point) const;
  void	OnAutoScrollTimer();

	// implementation helpers
	virtual int		HitTestHandles(CPoint point) const = 0;
	// return TRUE if continue
  virtual BOOL	OnMouseMessage(int nHandle,UINT nMsg, WPARAM wParam,
    LPARAM lParam, CWnd* pWnd, CWnd* pWndClipTo); 

// modifications by scroller
  void	SetTrackParams(WORD nStyle,BOOL bClipClient, AlignMode eAlignGrid, 
    const CPoint& alignSquareSize = CPoint(), const CPoint& alignSquareOffset = CPoint());
  BOOL	OnAutoScroll(CPoint& mouse,BOOL bTest = FALSE);
  virtual void	ScrollTracker(int xAmount,int yAmount) = 0;

  void	FromSrollerToTracker(CRect& dest,const CRect& src) const;

  virtual void OnChangeDP() {}; // device unit was changed...

// global parameters
	static	HBRUSH GetTrackHatchBrush();
	static	HPEN GetTrackBlackPen();
	static	HPEN GetTrackXorPen();
	static	int	GetTrackHandleSize();

// data
public:
	CFMntScrollerView*		m_pScroller;
	CWnd*					m_pClipWnd;

	WORD 	m_nStyle;     	// current style
	BOOL 	m_bClipClient;	// r�me�ek nesm� opustit client

	AlignMode 	m_eAlignGrid;	// zarovn�v�n� na aktu�ln� grid
  CPoint      m_alignSquareSizeLog;
  CPoint      m_alignSquareOffsetLog;
  //BOOL  m_bAlignGrid;
	CRect	m_rectClip;		// pr�nik Client a plochy por Scroller (v dev.)	
	
	BOOL 	m_bAllowInvert; // sm� b�t p�evr�cen rect
	BOOL	m_bLineTracker;	// tracker pro ��ru
	BOOL	m_bAutoScrTimer;// TRUE = je �as na autoscroll
	LPARAM	m_oldMouseParam;// posledn� sou�adnice my�i pro autoscroll
	int		m_oldHandle;	// poslend� handle     - " -
	BOOL	m_bTracking;	// je prov�d�n tracking

protected:
	CDC*	m_pDrawDC;
};

/////////////////////////////////////////////////////////////////////////////
// CFMntScrollerTrackerRect

class CFMntScrollerTrackerRect : public CFMntScrollerTracker
{
public:

  // Constructors
  CFMntScrollerTrackerRect(CFMntScrollerView*);
  ~CFMntScrollerTrackerRect();

  // Operations
  virtual void Draw(CDC* pDC) ;
  virtual int  HitTest(CPoint point) const;
  virtual BOOL Track(CWnd* pWnd, CPoint point, BOOL bAllowInvert = FALSE,
    CWnd* pWndClipTo = NULL);
  BOOL TrackRubberBand(CWnd* pWnd, CPoint point, BOOL bAllowInvert);
  virtual void GetTrueRect(LPRECT lpTrueRect) const;
  virtual int  NormalizeHit(int nHandle) const;
  virtual void ScrollTracker(int xAmount,int yAmount);

  void SetRect(LPCRECT);

protected:
  BOOL TrackHandle(int nHandle, CWnd* pWnd, CPoint point, CWnd* pWndClipTo);
  void DrawTrackerRect(LPCRECT lpRect, CWnd* pWndClipTo, CDC* pDC, CWnd* pWnd);
  virtual void AdjustRect(int nHandle);
  // implementation helpers
  int  HitTestHandles(CPoint point) const;
  UINT GetHandleMask() const;
  void GetHandleRect(int nHandle, CRect* pHandleRect) const;
  void GetModifyPointers(int nHandle, int**ppx, int**ppy, int* px, int*py);
  int  GetHandleSize(LPCRECT lpRect = NULL) const;
  // return TRUE if continue
  BOOL OnMouseMessage(int nHandle,UINT nMsg, WPARAM wParam, LPARAM lParam, CWnd* pWnd, CWnd* pWndClipTo); 

  // data
public:
  CRect	m_finalRect;	// po�adovan� Rect v log. sou�adnic�ch

  CRect	m_devRect;		// pracovn� rect v dev. sou�adnic�ch
  CRect	m_logRect;		// pracovn� rect v log. sou�adnic�ch

protected:
  BOOL m_bMoved;			// moved rect
  int  m_nWidth;
  int  m_nHeight;
  int* m_px, *m_py;
  int  m_xDiff, m_yDiff;

  CRect m_rectLast;
  CSize m_sizeLast;
  BOOL  m_bErase;          // TRUE if DrawTrackerRect is called for erasing
  BOOL  m_bFinalErase;     // TRUE if DragTrackerRect called for final erase
};


/////////////////////////////////////////////////////////////////////////////
// Parametry pro podporu pr�ce se scrollerem

class CFMntScrollEditParams : public CObject
{
DECLARE_DYNAMIC(CFMntScrollEditParams)
public:
					CFMntScrollEditParams();
	virtual			~CFMntScrollEditParams();

// Attributes
public:
	CObject*		m_pSelObject;
};

/////////////////////////////////////////////////////////////////////////////
// Parametry pro lok�ln� podporu editace

class CFMntScrollerView;
class CFMntScrollEditInfo : public CObject
{
  DECLARE_DYNCREATE(CFMntScrollEditInfo)
public:
  CFMntScrollEditInfo();
  virtual			~CFMntScrollEditInfo();

  // TRACKER pro objekt
  // podle parametr� objektu nastav� tracker
  virtual void	CreateTracker(CFMntScrollerView*,BOOL bDraw = TRUE); 
  // podle parametr� objektu obnov� tracker
  virtual void    ScrollTracker(int xAmount,int yAmount);
  // zru�� nastaven� Tracker
  virtual void	DestroyTracker(CFMntScrollerView*,BOOL bDraw = TRUE);

  // testuji, zda objekt m� sm� prov�d�t danou operaci
  virtual BOOL	CanObjectHit(int nHandle) const; 
  // tracking object
  virtual BOOL	TrackObject(CFMntScrollerView*,CPoint);
  // obnova objektu pro Tracking
  virtual void	UpdateOnTrack(CFMntScrollerView*);
  // max. rect mezi objektem a trackerem (v log.)
  void	GetLogTrueRect(CFMntScrollerView*,CRect&);
 
  // reakce na zm�ny objektu (p�etvo�en� trackeru)
  virtual void	UpdateObjectChanges(CFMntScrollerView*, BOOL bDelete, BOOL bDraw = TRUE);

  // reakce 
  virtual void OnChangeDP(CFMntScrollerView*); 


  // Attributes
public:
  CObject*				m_pSelObject; 
  BOOL					m_bAutoScroll;	// povolen� sroll. obrazu p�i trackingu 

  CFMntScrollerTracker*	m_pSelTracker;
};

#define TIMER_AUTOSCROLL_DELAY	20
  
// Special mapping modes just for CScrollerView implementation
#define MM_NONE             0

// Parametry pro podporu udpate view
class CFMntScrollUpdate : public CObject
{
DECLARE_DYNAMIC(CFMntScrollUpdate)

public:
					CFMntScrollUpdate(DWORD nType,CView* pView);
	virtual			~CFMntScrollUpdate();

// Attributes
public:
	DWORD		m_nType;
	CView*		m_pSender;
	CWnd*		m_pParent;

	// n�sleduj�c� parametry jsou vypln�ny pouze, pokud to vy�aduje ud�lost
	CView*		m_pView;	// view - (jinak NULL)
	CObject*	m_pObject;	// schema (jinak NULL)
	CRect		m_rPlace;	// plocha

	// univerz�ln� prametry
	int			m_nParam;
	DWORD		m_nParam1;
	DWORD		m_nParam2;

	// p��znak  realizace
	BOOL		m_bRealize;	// def. FALSE
};



class CFMntScrollerView : public CView
{
	DECLARE_DYNAMIC(CFMntScrollerView)

public:
	CFMntScrollerView();     
	virtual ~CFMntScrollerView();


	// Attributes
public:
	static const SIZE sizePgDefault;
	static const SIZE sizeLnDefault;

	int m_nScale;			// Scale for axis in percent
  //int		m_nMinScale;		// Min scale
	int m_nMaxScale;		// Max scale for current device

	CRect m_rAreaLog;			// logick� plocha zobrazen� v oon�
	int   m_nMapMode;			// current MapMode
	int   m_nRealMapMode;		// metrika zobrazen� p�i m���tku 100%
	BOOL  m_bCenterInView;	// centrov�n� plochy v okn�, je-li men�� 

	CSize m_szTotalDev;		// current total size in device units
	CSize m_szTotalRDev;		// total size in device units for real scale 

	CSize m_szPageDev;		// velikost p�ekryvu str�nek p�i scroll v %
	CSize m_szLineDev;		// velikost ��dek pro scroll v %

	BOOL m_bInsideUpdate;	// internal state for OnSize callback

  //WORD	m_nZoomHistory;		// po�et zoom ulo�en�ch v historii
  //SZoomHistory m_sHistory[NUM_ZOOM_HISTORY];	// historie Zoom

	// zpracov�n� objektu a my�i
	BOOL m_bAutoScroll;		// sm� prov�d�t scroller Auto-scroll
	CFMntScrollerTracker* m_pScrollTracker; // actual tracker for auto-scroll

	// poloha objektu a my�i
	WORD   m_nMouseMode;		// m�d my�i
	CPoint m_cPullPointLog; // if mode is pull, the point in log. units, that must be allways under mouse pointer.
	BOOL   m_bValidObjRect;	// m� smysl rect m_rLastPos
	CRect  m_rLastPos;			// pozice objektu p�i p�esunu my��

	// barvy pozad� a m��ky
	COLORREF m_wndColor;		// barva pozad�
	COLORREF m_gridColor;	// barva pro m��ku (def. COLORWINDOW xor 0x00FFFF00)

	// nastaven� m��ky
	BOOL  m_bShowGrid;	// bude zobrazena m��ka
	BOOL  m_bAlignGrid;	// zarovn�v�n� objekt� na m��ku
	CSize m_gridStep;		// krok m��ky v log. bodech
	CSize m_currGridStep; // aktu�ln� krok m��ky pro dan� m���tko

	// parametry pro podporu editace
	BOOL m_bEditSupport;	// podporov�ny funkce editace

	CFMntScrollEditParams* m_pEditParams; // glob�ln� parametry editace
	CFMntScrollEditInfo*   m_pEditInfo;	// lok�ln� parametry editace
	CRuntimeClass*         m_pEInfoClass;	// �ablona pro m_pEditInfo

	// konstanty pro update stav�
	enum UpdateOperation 
	{
		uvfFirstUpdate	 = 0x1FFF0000,
		uvfChangeBkColor = 0x1FFF0001,	// zm�ny barvy pozad�

		uvfChangeScale	 = 0x1FFF0003,	// nastaven� m���tka zobrazen�
		uvfDoZoomIn		 = 0x1FFF0004,	// proveden� ZoomIn
    //  uvfDoZoomOut	 = 0x1FFF0005,	// proveden� ZoomOut

    //  uvfSaveCurrScale = 0x1FFF0007,  // ulo�en� akt. nastaven� do historie
    //  uvfLastCurrScale = 0x1FFF0008,	// posledn� m��. v historii
    //  uvfResetHisScale = 0x1FFF0009,	// ru�en� historie m���tek

    //  uvfSetMouseMode  = 0x1FFF000A,	// nastaven� modu my�i
    //  uvfCancelMouseMode=0x1FFF000B,	// zru�en� modu my�i

		uvfChangeGrid	 = 0x1FFF000C,	// zm�na m��ky

		uvfEOSelObject	 = 0x2FFF0001,	// ur�en objekt v m_pEditParams
		uvfEOUnselObject = 0x2FFF0002,	// zru�en v�b�r objektu v m_pEditParams

    //  uvfEOPrepChngObj = 0x2FFF0003,	// p��prava na mo�nou zm�nu objektu
		uvfEOUpdtChngObj = 0x2FFF0004,	// reakce na zm�nu objektu (m_nParam != 0)->unselect

		uvfEOInvalRect	 = 0x2FFF0005,	// p�ekreslen� rectu ve v�ech view

    /*
		uvfChangeLogArea = 0xFF000001,	// zm�na m_pPrivParams->m_areaLog
		uvfEndFitScale	 = 0xFF000003,	// ukon�en� modu FitZoom

		uvfChangeBkColor = 0xFF000006,	// zm�ny barvy pozad�
    */
	};

	// Operations
	// scroll positions & units conversion
	CPoint FromDeviceToScrollPosition(CPoint) const;
	CRect  FromDeviceToScrollPosition(CRect)  const;
	CPoint FromScrollToDevicePosition(CPoint) const;
	CRect  FromScrollToDevicePosition(CRect)  const;

	CPoint WindowToScrollerPosition(CPoint) const;
	CRect  WindowToScrollerPosition(CRect) const;

	CPoint FromScrollToWindowPosition(CPoint) const;
	CRect  FromScrollToWindowPosition(CRect)  const;

	// upper corner of scrolling
	CPoint GetScrollPosition() const 
	{
		if ((m_nScale == SS_ISOTROPICFIT) || (m_nScale == SS_ANISOTROPICFIT)) return CPoint(0, 0);    // must be 0,0
		return FromDeviceToScrollPosition(GetDeviceScrollPosition());
	}

	CSize GetTotalSize() const;	// logical size
	CRect GetLogClientRect() const;	// client rect v log. sou�adnic�ch

	CSize GetLogInMM() const;	// po�et log. bod� na mm p�i akt. m���tku

	// for device units
	CPoint GetDeviceScrollPosition() const;

	// nastaven� velikosti plochy a metriky
	void SetScrollSizes(int nRMapMode, SIZE sizePage = sizePgDefault,
						SIZE sizeLine = sizeLnDefault);

	// scale settings & verification
	virtual int GetMinScale() const
	{
		CRect rClient; 
		GetClientRect(rClient); 
		return min(rClient.Width() * SCALE_UNIT / m_szTotalRDev.cx, rClient.Height() * SCALE_UNIT/m_szTotalRDev.cy) + 1;
	}

	virtual int GetMaxScale() const 
	{
		return m_nMaxScale;
	}

	void SetScrollScale(int aScale, POINT* pOrigin = NULL);

	BOOL CanSetScale(int aScale) const 
	{
		return ((aScale == SS_ISOTROPICFIT) || (aScale == SS_ANISOTROPICFIT) || ((aScale >= GetMinScale()) && (aScale <= m_nMaxScale)));
	}

  //void SaveZoomHistory();	// ulo�en� akt. stavu do historie
  //void LastZoomHistory();	// nastaven� posledn�ho m���tka historie
  //void ResetZoomHistory() {m_nZoomHistory = 0;};	// nulov�n� historie pro ZoomOut

	void DoZoomInTracker(CPoint& point);
	void DoZoomIn(CFMntScrollUpdate*);
	BOOL CanZoomIn() const;

	//void	DoZoomOut();
	BOOL CanZoomOut() const;

	// grid settings & align
	virtual void SetGrid(BOOL bShow, BOOL bAlign, CSize& sizeGrid);
	virtual void CalcCurrentGrid();
	virtual void DrawGrid(CDC* pDC);

	CPoint UpdateInClipRect(LPCRECT lpRect,CPoint& point);  
	CPoint AlignToGrid(CPoint& point, LPCRECT pClip = NULL);
	CPoint AlignToStep(CPoint& point, const CPoint& step, const CPoint& offset = CPoint(0, 0), LPCRECT pClip = NULL);
	CRect  AlignRectToGrid(CRect&, LPCRECT pClip = NULL, SIZE* pMinSize = NULL);

	// scroller modes & actions
	virtual WORD GetMouseMode() const 
	{
		return m_nMouseMode;
	}

	virtual void SetMouseMode(WORD nMode);

	virtual void CancelMouseMode() 
	{
		m_nMouseMode = MM_NORMAL;
	}

	// scrolling to position
	void ScrollToPosition(POINT pt);    		// set upper left position
	void ScrollToDevicePosition(POINT ptDev);// explicit scrolling no checking
	void CenterToRect(LPCRECT pRect);		// centrov�n� rectu

	void KeepVisibleLog(POINT pt);

	// scrollbars manipulation
	void OnScrollBar(int nBar, UINT nSBCode, UINT nPos);

	// adjust scrollbars etc
	void UpdateBars();									
	// size with no bars
	BOOL GetTrueClientSize(CSize& size, CSize& sizeSb);

	// podpora pro v�b�r r�me�ku a ��ry
	virtual BOOL GetUserTrackRectNew(CRect& rect, CPoint point, 
									 BOOL bClipClient = TRUE,
									 BOOL bClipLogClient = TRUE, 
									 AlignMode eAlignGrid  = fstAlignNone, 
									 const CPoint& alignSquareSizeLog = CPoint(), 
									 const CPoint& alignSquareOffsetLog = CPoint(), 
									 BOOL bAllowInver = TRUE);

	virtual BOOL GetUserTrackRect(CRect& rect, CPoint point, 
								  BOOL bClipClient = TRUE,  
								  BOOL bClipLogClient = TRUE, 
								  AlignMode eAlignGrid  = fstAlignNone, 
								  BOOL bAllowInver = TRUE);

	 // zarovn�n� Rectu p�i Trackingu
	virtual void AdjustTrackRect(CFMntScrollerTracker*, int nHandle, CRect& atRect, 
								 LPCRECT pClipRect, AlignMode eGridAlign, 
								 BOOL bAllowInvert);
	void SetAutoScrollTrack(CFMntScrollerTracker*);
	void RemoveAutoScrollTrack();

	BOOL UserTrackRectOnSetCursor(CRect& rect, CPoint& point);

	// podpora editace
	virtual BOOL DoSelectObject();
	virtual BOOL DoUnselectObject();

	virtual BOOL EOSearchEditObject(CPoint ptAt); // pr�ce s m_pEditParams
	virtual BOOL EOSelectObject();	// v�b�r objektu podle m_pEditParams
	virtual BOOL EOUnselectObject();// validace zru�en� v�b�ru objektu
	virtual BOOL EOUpdateInfo();	// �daje o objektu (vrac� p��znak zm�ny �daj�)

	virtual BOOL EOObjectFromTracker();	// objekt byl zm�n�n trackerem->p�izp�sob� se

	void EOInvalidateLogRect(CRect& rRect, BOOL bErase); // p�epis rectu v log. sou�adnic�ch

	virtual BOOL EOEditObject();	// implementace p��m� editace (TRUE->zm�na)
	virtual BOOL EOCanEditObject();

	virtual void DoEditObject(POINT* pPopUp);	// provede editaci objektu

	void UpdateEditChanges(BOOL bDelObj = FALSE);
	void SetCursorFromMouseMode(); // Set cursor according to current MouseMode.

	// Overrides
	// update view s parametry
	void UpdateAllViews(CFMntScrollUpdate*);
	void UpdateAllViews(DWORD nType);
	virtual void InitialUpdateMetrics();
	virtual void CopySplitViewMetrics(CFMntScrollerView*);
	// user draw & redraw support
	virtual void OnDrawObjects(CDC* pDC);
	void InvalidateLogRect(CRect& aRect, BOOL bErase = TRUE) const;
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFMntScrollerView)
public:
	virtual void OnInitialUpdate();
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

	// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	// Generated message map functions
public:
	// operace prov�d�n� u�ivatelem !!!

	// nastaven� m���tka
	virtual void OnScrollScaleChanged() 
	{
	}

	afx_msg void OnSetScale(int nScale, BOOL bResetHist = FALSE);
	afx_msg void OnUpdateSetScale(int nScale, CCmdUI* pCmdUI);
	afx_msg void OnUserSetScale() 
	{
	}

	// nastaven� m��ky
	void OnSetGrid(BOOL bShow, BOOL bAlign, CSize& sizeGrid);
	BOOL IsActiveGrid() const;

	afx_msg void OnActiveGridChange();
	afx_msg void OnUpdateActiveGridChange(CCmdUI* pCmdUI);
	afx_msg void OnShowGridChange();
	afx_msg void OnUpdateShowGridChange(CCmdUI* pCmdUI);
	afx_msg void OnAlignGridChange();
	afx_msg void OnUpdateAlignGridChange(CCmdUI* pCmdUI);
	afx_msg void OnUserSetGrid();
	afx_msg void OnUpdateUserSetGrid(CCmdUI* pCmdUI);

	// nastaven� barev
	void OnSetBackground(COLORREF);

	// nastaven� m�du my�i
  //void OnSetMouseMode(WORD nMode);
  //void OnCancelMouseMode() { UpdateAllViews(uvfCancelMouseMode);};

protected:
	//{{AFX_MSG(CFMntScrollerView)
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnCancelMode();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//////////////////////////////////////////////////////////////////////
/// Support for Options (PoseidonEdOpts) CFMntOptions
//////////////////////////////////////////////////////////////////////

typedef	union
{
	int		nInt;
	LONG	nLong;
	WORD	nWord;
	DWORD	nDWord;
	float	nFloat;
	double	nDouble;
}	FTAnyNumber;

typedef struct
{
	WORD		nItemType;		// typ prom�nn� - optType
	void*		nItemValue;		// prom�nn� obsahuj�c� hodnotu
	FTAnyNumber	nItemDefault;	// def. hodnota
	RStringB	pItemKeyName;	// jm�no kl��e
}	FMNT_OPTMAP_ENTRY;

typedef struct
{
    void * pArray;
    int nSize;
    WORD nArrayItemType;
    unsigned int nStep; // valid only if nArrayItemType is optClass
} FMT_OPTMAP_ENTRY_ARRAY;


#define DECLARE_OPTIONS_MAP(nSize) \
private: \
				  FMNT_OPTMAP_ENTRY _optionsEntries[nSize+1]; \
protected: \
	virtual const FMNT_OPTMAP_ENTRY* GetOptionsMap() const; \
	virtual void					InitOptionsMap(); \

#define BEGIN_OPTIONS_MAP( theClass ) \
	const FMNT_OPTMAP_ENTRY* theClass::GetOptionsMap() const \
					{ return (theClass::_optionsEntries); }; \
	void  theClass::InitOptionsMap() { \
					int nEntry = 0; \


#define END_OPTIONS_MAP() \
		_optionsEntries[nEntry].nItemType = 0; \
	}; \



class CFMntOptions : public CObject
{
    DECLARE_DYNAMIC(CFMntOptions)
public:		
   
    enum optType
    {
        optEnd	 = 0, 
        optBool  = 1, optInt   = 2, optLong  = 3,
        optChar  = 4, optWord  = 5, optDWord = 6,
        optFloat = 7, optDouble= 8,
        optColor = 9, optString= 10, optClass  = 11,
        optArray = 12
    };
    
    // constructor
    CFMntOptions() 
	: m_bInitialized(FALSE)
	, m_cClassName("")
	, m_pParentClass(NULL)  
	{
	}

    virtual	~CFMntOptions() 
	{
	}
    
    // Init();
    void Init();        
    
    // zpracov�n� options
    virtual BOOL	LoadParams(BOOL bReportErr = TRUE);
    virtual BOOL	SaveParams(BOOL bReportErr = TRUE);
    virtual void	CopyFrom(CFMntOptions*);

    void SetInOutParamFile(const char * pszClassName, ParamClassPtr pParentClass)  
    {
		m_cClassName = pszClassName; 
		m_pParentClass = pParentClass;
	}

    void ReleaseInOutParamFile() 
    {        
        m_pParentClass = NULL;
    }

    const CFMntOptions& operator=(const CFMntOptions& opt) 
    {
		m_cClassName = opt.m_cClassName; 
		m_pParentClass = opt.m_pParentClass;
		return *this;
	}    
    
protected:    
    virtual void Flush() const 
	{
	}

    // mapov�n� options    
    virtual const FMNT_OPTMAP_ENTRY* GetOptionsMap() const 
	{
		return NULL;
	}

    virtual void InitOptionsMap() 
	{
	}
    
    // nastaven� def. hodnot
    virtual void SetDefaultValues();
      
protected:
    BOOL m_bInitialized;

    RStringB m_cClassName;
    ParamClassPtr m_pParentClass;
};

// definice jednotliv�ch typ� options
#define MNTOPT_BOOL( value, defval, key ) \
	_optionsEntries[nEntry].nItemType		= CFMntOptions::optBool; \
	_optionsEntries[nEntry].nItemValue		= (BOOL*)(&value); \
	_optionsEntries[nEntry].nItemDefault.nInt = (int)defval; \
	_optionsEntries[nEntry++].pItemKeyName	= key; \

#define MNTOPT_INT( value, defval, key ) \
	_optionsEntries[nEntry].nItemType		= CFMntOptions::optInt; \
	_optionsEntries[nEntry].nItemValue		= (int*)(&value); \
	_optionsEntries[nEntry].nItemDefault.nInt = defval; \
	_optionsEntries[nEntry++].pItemKeyName	= key; \

#define MNTOPT_LONG( value, defval, key ) \
	_optionsEntries[nEntry].nItemType		= CFMntOptions::optLong; \
	_optionsEntries[nEntry].nItemValue		= (LONG*)(&value); \
	_optionsEntries[nEntry].nItemDefault.nLong = defval; \
	_optionsEntries[nEntry++].pItemKeyName	= key; \

#define MNTOPT_CHAR( value, defval, key ) \
	_optionsEntries[nEntry].nItemType		= CFMntOptions::optChar; \
	_optionsEntries[nEntry].nItemValue		= (TCHAR*)(&value); \
	_optionsEntries[nEntry].nItemDefault.nInt = (int)_T(defval); \
	_optionsEntries[nEntry++].pItemKeyName	= key; \

#define MNTOPT_WORD( value, defval, key ) \
	_optionsEntries[nEntry].nItemType		= CFMntOptions::optWord; \
	_optionsEntries[nEntry].nItemValue		= (WORD*)(&value); \
	_optionsEntries[nEntry].nItemDefault.nWord = defval; \
	_optionsEntries[nEntry++].pItemKeyName	= key; \

#define MNTOPT_DWORD( value, defval, key ) \
	_optionsEntries[nEntry].nItemType		= CFMntOptions::optDWord; \
	_optionsEntries[nEntry].nItemValue		= (DWORD*)(&value); \
	_optionsEntries[nEntry].nItemDefault.nDWord = defval; \
	_optionsEntries[nEntry++].pItemKeyName	= key; \

#define MNTOPT_FLOAT( value, defval, key ) \
	_optionsEntries[nEntry].nItemType		= CFMntOptions::optFloat; \
	_optionsEntries[nEntry].nItemValue		= (float*)(&value); \
	_optionsEntries[nEntry].nItemDefault.nFloat = defval; \
	_optionsEntries[nEntry++].pItemKeyName	= key; \

#define MNTOPT_DOUBLE( value, defval, key ) \
	_optionsEntries[nEntry].nItemType		= CFMntOptions::optDouble; \
	_optionsEntries[nEntry].nItemValue		= (double*)(&value); \
	_optionsEntries[nEntry].nItemDefault.nDouble = defval; \
	_optionsEntries[nEntry++].pItemKeyName	= key; \

#define MNTOPT_COLOR( value, defval, key ) \
	_optionsEntries[nEntry].nItemType		= CFMntOptions::optColor; \
	_optionsEntries[nEntry].nItemValue		= (COLORREF*)(&value); \
	_optionsEntries[nEntry].nItemDefault.nDWord = (DWORD)defval; \
	_optionsEntries[nEntry++].pItemKeyName	= key; \

#define MNTOPT_STRING( value, defval, key ) \
	_optionsEntries[nEntry].nItemType		= CFMntOptions::optString; \
	_optionsEntries[nEntry].nItemValue		= (CString*)(&value); \
	_optionsEntries[nEntry].nItemDefault.nDWord = (DWORD)_T(defval); \
	_optionsEntries[nEntry++].pItemKeyName	= key; \

// class and array does not have default values
#define MNTOPT_CLASS( value,  key ) \
	_optionsEntries[nEntry].nItemType		= CFMntOptions::optClass; \
	_optionsEntries[nEntry].nItemValue		= (void*)(&value); \
	/*_optionsEntries[nEntry].nItemDefault.nDWord = (DWORD)_T(defval); */\
	_optionsEntries[nEntry++].pItemKeyName	= key; \

#define MNTOPT_ARRAY( value,  key) \
	_optionsEntries[nEntry].nItemType		= CFMntOptions::optArray; \
	_optionsEntries[nEntry].nItemValue		= (void*)(&value); \
	/*_optionsEntries[nEntry].nItemDefault.nDWord = (DWORD)_T(defval); */\
	_optionsEntries[nEntry++].pItemKeyName	= key; \


///////////////////////////////////////////
/// ComboBox & ListBox
///////////////////////////////////////////

// DataType for MntListBox and MntComboBox
#define MNTLST_USER			0	// defined and used by user, some of the virtual function must be defined
#define MNTLST_BITMAP		1	// bitmap, function LoadImageBitmap must be called.  Data consist from LOWORD itemID and 
                          // HIWORD position of the item image in bitmap. Bitmap must contains images in row. 
													// The number of images is given by nPtrns in LoadImageBitmap, function.

#define MNTLST_COLOR		3	// data must be COLORREF,zobrazen� barev
#define MNTLST_UNKNOWN  -1 
//#define MNTLST_IMAGE		2	

class CFMntComboBox : public CComboBox
{
// Construction
public:
	CFMntComboBox(): m_iDataType(MNTLST_UNKNOWN) {};

// Attributes
protected:
   int m_iDataType;  

		// Bitmap data
		CBitmap m_cBitmap;
		int m_nPtrnts;

// Operations
public: 
	void Init(int iDataType) {m_iDataType = iDataType;}; // Set up type of data connect with items. Must be on of the MNTLST_ values  
  void AddStringEx(const CString& str, DWORD Value); 

    virtual int  GetItemImageWidth(LPDRAWITEMSTRUCT lpDIS); 
    virtual void OnDrawItemImage(CRect&, LPDRAWITEMSTRUCT lpDIS);
    virtual void OnDrawItemText(CRect&, LPDRAWITEMSTRUCT lpDIS);

    virtual BOOL GetItemText(CString& , LPDRAWITEMSTRUCT lpDIS);
    virtual BOOL GetItemColor(COLORREF&, LPDRAWITEMSTRUCT lpDIS);

    void LoadImageBitmap(LPCTSTR lpBmp, int nPtrns); // see MNTLST_BITMAP
    
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFMntComboBox)
	public:	
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);	
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct); 

	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CFMntComboBox)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

class CFMntListBox : public CListBox
{
// Construction
public:
	
	CFMntListBox(): m_iDataType(MNTLST_UNKNOWN) {};

// Attributes
protected:    
    int m_iDataType;  

		// Bitmap data
		CBitmap m_cBitmap;
		int m_nPtrnts;

// Operations
public: 
	void Init(int iDataType) {m_iDataType = iDataType;}; 
    
    void AddStringEx(const CString& str, DWORD Value); 
    virtual int  GetItemImageWidth(LPDRAWITEMSTRUCT lpDIS);
    virtual void OnDrawItemImage(CRect&, LPDRAWITEMSTRUCT lpDIS);
    virtual void OnDrawItemText(CRect&, LPDRAWITEMSTRUCT lpDIS);

    virtual BOOL GetItemText(CString& , LPDRAWITEMSTRUCT lpDIS);
    virtual BOOL GetItemColor(COLORREF&, LPDRAWITEMSTRUCT lpDIS);

    void LoadImageBitmap(LPCTSTR lpBmp, int nPtrns);
    
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFMntListBox)
	public:	
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);	
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct); 

	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CFMntListBox)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

class CFMntMenuListBox : public CListBox
{
protected:
  int m_MainMenuID;
  int m_ItemMenuID;
  CWnd * m_owner;

public:
  CFMntMenuListBox(): m_MainMenuID(-1), m_ItemMenuID(-1), m_owner(NULL) {};
  void Init(int mainMenuID, int itemMenuID, CWnd * owner) 
  { m_MainMenuID = mainMenuID; m_ItemMenuID = itemMenuID; m_owner = owner;};

  DECLARE_MESSAGE_MAP()
  afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
};

void LBAddString(HWND hWnd, LPCTSTR string, DWORD Value = 0L);
void LBDeleteString(HWND hWnd, LPCTSTR string);
void LBAddStringID(HWND hWnd, int strID, DWORD Value = 0L);
void CBAddString(HWND hWnd, LPCTSTR string, DWORD Value = 0L);
void CBAddStringID(HWND hWnd, int strID, DWORD Value = 0L);

// select item in Listbox & Combobox
void LBSetCurSel(HWND hWnd,int Index);
void CBSetCurSel(HWND hWnd,int Index);

// set data to selected items in Listbox & Combobox
void LBSetCurSelData(HWND hWnd,DWORD ItmData);
void CBSetCurSelData(HWND hWnd,DWORD ItmData);

// get selected item from Listbox & Combobox
int  LBGetCurSel(HWND hWnd);
int  CBGetCurSel(HWND hWnd);

// get daya from selected items in Listbox & Combobox
BOOL LBGetCurSelData(HWND hWnd,DWORD& ItmData);
BOOL CBGetCurSelData(HWND hWnd,DWORD& ItmData);

//inline void LBClearAll(HWND hWnd) {DebugTrap();};
//inline void CBClearAll(HWND hWnd) {DebugTrap();};


///////////////////////////////////////////////
// CMntEditableListCtrl
///////////////////////////////////////////////

class CInPlaceEdit : public CEdit
{
  // Construction
public:
  CInPlaceEdit(int iItem, int iSubItem, CString sInitText);

  // Attributes
public:

  // Operations
public:

  // Overrides
  // ClassWizard generated virtual function overrides
  //{{AFX_VIRTUAL(CInPlaceEdit)
public:
  virtual BOOL PreTranslateMessage(MSG* pMsg);
  //}}AFX_VIRTUAL

  // Implementation
public:
  virtual ~CInPlaceEdit();

  // Generated message map functions
protected:
  // validate string
  virtual bool Validate(CString& str) {return true;};


  //{{AFX_MSG(CInPlaceEdit)
  afx_msg void OnKillFocus(CWnd* pNewWnd);
  afx_msg void OnNcDestroy();
  afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
  afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
  //}}AFX_MSG

  DECLARE_MESSAGE_MAP()
private:
  int m_iItem;
  int m_iSubItem;
  CString m_sInitText;
  BOOL    m_bESC;	 	// To indicate whether ESC key was pressed
};

/*class CInPlaceEditFloat : public CInPlaceEdit
{
protected:
  virtual bool Validate(CString& str);

public:
  CInPlaceEditFloat(int iItem, int iSubItem, CString sInitText);
};*/

class CMntEditableListCtrl : public CListCtrl
{
protected:
  virtual bool CanEditSubItem(int iItem, int iSubItem) {return true;};
  virtual CEdit * ConstructInPlaceEdit(int iItem, int iSubItem, CString sInitText) 
  {return new CInPlaceEdit(iItem, iSubItem, sInitText);};

public:
  int HitTestEx(CPoint &point, int & iSubItem);
  CEdit* EditSubLabel( int nItem, int iSubItem );
  void OnEndLabelEdit(NMHDR* pNMHDR, LRESULT* pResult);
  
  DECLARE_MESSAGE_MAP()
  afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
  afx_msg void OnLvnEndlabeledit(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
  afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
};


///////////////////////////////////////////////
// CMntPropertyPage&CMntPropertySheet
///////////////////////////////////////////////

class CMntPropertyPage;

class CMntPropertySheet : public CPropertySheet
{
public:
  CMntPropertySheet(UINT nIDCaption, DWORD dwAdvFlags = 0, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
  CMntPropertySheet(LPCTSTR pszCaption, DWORD dwAdvFlags = 0, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

	void AddPageIntoWizard(CMntPropertyPage * pPage);

	void OnWizardBack();
	void OnWizardNext();
};

class CMntPropertyPage : public CPropertyPage
{
protected:
	CMntPropertySheet * m_pSheet;
public:
  CMntPropertyPage(UINT nIDTemplate, UINT nIDCaption = 0  ) :
			CPropertyPage(nIDTemplate, nIDCaption), m_pSheet(NULL) {};
	CMntPropertyPage(LPCTSTR lpszTemplateName, UINT nIDCaption = 0) : 
			CPropertyPage(lpszTemplateName, nIDCaption), m_pSheet(NULL) {};

	void AssignSheet(CMntPropertySheet * pSheet) {m_pSheet = pSheet;};

	virtual LRESULT OnWizardBack();
	virtual LRESULT OnWizardNext();
};

//////////////////////////////////////////////////
// CFMntColorButton window
//////////////////////////////////////////////////

class CFMntColorButton : public CButton
{
// Construction
public:
  virtual BOOL DoEditColor();
 
// Attributes
public:
	COLORREF 		 m_cColor;

public:  
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFMntColorButton)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);	
	//}}AFX_VIRTUAL
	// Generated message map functions
protected:
	//{{AFX_MSG(CFMntColorButton)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


//////////////////////////////////////////////////
// CAction probably connected with undo/redo
//////////////////////////////////////////////////
class CAction : public CObject
{
DECLARE_DYNAMIC(CAction)
public:
  CAction(int nType, LPCTSTR pActName = NULL);

			// proveden� akce v dokumentu
			// m_OldValue se pou�ije jako nov� hodnota
  virtual BOOL RealizeAction(CDocument*);
			// v�m�na hodnot m_CurValue a m_OldValue
  virtual BOOL ChangeValues();

// data
public:
	int		m_nActType;
	CString	m_sActName;
	//		m_CurValue;
	//		m_OldValue;
};

// CActionManager
class CActionManager : public CObject
{
DECLARE_DYNAMIC(CActionManager)
public:
  CActionManager();
  ~CActionManager();

  void	ClearUndoRedoBuffers();

  virtual BOOL	RealizeAction(CAction*) const;

  virtual BOOL	DoNewAction(CAction*);
			
  virtual BOOL	DoUndoAction();
  void	OnUpdateUndoAction(CCmdUI*) const;
		
  virtual BOOL	DoRedoAction();
  void	OnUpdateRedoAction(CCmdUI*) const;

  void OnUpdateDelUndoRedoActions(CCmdUI* pCmdUI) const;
			
// data
public:
	CDocument*	m_pDocument;
	CObArray	m_ActionUndoFront;
	CObArray	m_ActionRedoFront;
};

///////////////////////////////////////////
// Rounds
///////////////////////////////////////////
//	special round formats
#define SR_NONE		0			//  no round
#define SR_1		1			//	1. = integers
#define SR_10		2           //  10
#define SR_100		3           //  100
#define SR_1000		4           //  1000
#define SR_5		5           //  5
#define SR_05		6           //  0.5
#define SR_025		7           //  0.25
#define SR_01		8           //  0.1
#define SR_005		9           //  0.05
#define SR_0025		10          //  0.025
#define SR_001		11          //  0.01
#define SR_0001		12          //  0.001
	
double SpecRound(double x,int style);
long double SpecRound(long double x,int style);
	
int SpecRound(int x,int style);
long int SpecRound(long int x,int style);

// Round double number
double Round(double x,int ex);
long double Roundl(long double x,int ex);

///////////////////////////////////////////
// DDV & DDX
///////////////////////////////////////////
void MDDX_Text(CDataExchange* pDX, int nIDC, int& value, BOOL bLocal = TRUE);
void MDDX_Text(CDataExchange* pDX, int nIDC, long& value, BOOL bLocal = TRUE);
//void MDDX_Text(CDataExchange* pDX, int nIDC, WORD& value, BOOL bLocal = TRUE);
void MDDX_Text(CDataExchange* pDX, int nIDC, DWORD& value, BOOL bLocal = TRUE);
void MDDX_Text(CDataExchange* pDX, int nIDC, float& value, BOOL bLocal = TRUE);
void MDDX_Text(CDataExchange* pDX, int nIDC, double& value, BOOL bLocal = TRUE);
void MDDX_Text(CDataExchange* pDX, int nIDC, CString& value);

void MDDV_MinMaxInt(CDataExchange* pDX, int value, int minVal, int maxVal, int nIDC = 0, int step = 1, int round = SR_NONE);
void MDDV_MinMaxLong(CDataExchange* pDX, long value, long minVal, long maxVal, int nIDC = 0, long step = 1, int round = SR_NONE);
void MDDV_MinMaxWord(CDataExchange* pDX, WORD value, WORD minVal, WORD maxVal, int nIDC = 0, WORD step = 1, int round = SR_NONE);
void MDDV_MinMaxDWord(CDataExchange* pDX, DWORD value, DWORD minVal, DWORD maxVal, int nIDC = 0, DWORD step = 1, int round = SR_NONE);
void MDDV_MinMaxFloat(CDataExchange* pDX, float const& value, float minVal, float maxVal, int nIDC = 0, float step = 0.5, int round = SR_NONE);
void MDDV_MinMaxDouble(CDataExchange* pDX, double const& value, double minVal, double maxVal, int nIDC = 0, double step = 0.5, int round = SR_NONE);
void MDDV_MaxChars(CDataExchange* pDX, LPCTSTR value, int nChars, int nIDC = 0, BOOL setEdit = TRUE);
void MDDV_MinChars(CDataExchange* pDX, LPCTSTR value, int nChars, int nIDS = -1 );

void MDDV_LBValidSelItem(CDataExchange* pDX, int nIDC, int strID = -1) ;
void MDDV_CBValidSelItem(CDataExchange* pDX, int nIDC, int strID = -1);
void MDDV_FileExist(CDataExchange* pDX, LPCTSTR pFile );

////////////////////////////////////////////////
// String functions
////////////////////////////////////////////////

// Num to string convertion
#define MAX_NUMBER_DIGITS		50

LPCTSTR NumToLocalString(CString& sStr, LONG nNum);
LPCTSTR NumToLocalString(CString& sStr, double nNum, 
								         int nDecimal = MAX_NUMBER_DIGITS,
												 BOOL bStrict = FALSE);

LPCTSTR NumToStandardString(CString& sStr, LONG nNum );
LPCTSTR NumToStandardString(CString& sStr, double nNum,  int nDecimal = MAX_NUMBER_DIGITS);

LPCTSTR MakeLongDirStr(CString& dest,LPCTSTR source);
LPCTSTR MakeShortDirStr(CString& dest,LPCTSTR source);

LPCTSTR	CutLeftChar( LPTSTR pDest, LPCTSTR pSrc, TCHAR ch = _T(' '));
LPCTSTR	CutRightChar( LPTSTR pDest, LPCTSTR pSrc, TCHAR ch = _T(' '));

BOOL	MntGetSubstring(CString& dest, LPCTSTR lpSrc, TCHAR cSeparator, int nNum);

LPCTSTR SeparateDirFromFileName(CString& dest,LPCTSTR source);
LPCTSTR SeparateNameFromFileName(CString& dest,LPCTSTR source);
LPCTSTR SeparateExtFromFileName(CString& dest,LPCTSTR source);
LPCTSTR CutExtFromFileName(CString& dest,LPCTSTR source);

////////////////////////////////////////////////
// Windows functions
////////////////////////////////////////////////

// provede vol�n� UpdateAllViews pro v�echny dokumenty aplikace
void UpdateAllDocs(LPARAM lHint = 0L, CObject* pHint = NULL);


// zobrazen� a validace okna (pouze v p��pad� zm�ny)
void MntShowWindow(HWND hWnd, BOOL bShow);

BOOL GetEditInt(HWND hWnd, int& value);
BOOL GetEditFloat(HWND hWnd, float& value);

#define SAVEDATA		(pDX->m_bSaveAndValidate)
#define DLGCTRL(idc)	(::GetDlgItem(m_hWnd,idc))

// about window
inline void DoMntAboutApplicationSheet(UINT nIDPageRes,UINT nIDPgIcon = -1,
                                WORD nStyle = 0xFFFF) {};
inline void DoMntAboutApplicationSheet(CPropertyPage* pPage,UINT nIDPageRes = -1,UINT nIDPgIcon = -1,
                                WORD nStyle = 0xFFFF) {};

// startovn� okno aplikace
inline void EnableStartWnd(BOOL bEnable) {};
inline BOOL ShowStartWnd(CWnd* pParentWnd, UINT nBmpID, CRect* prUser = NULL) {return FALSE;};
inline BOOL WaitStartWnd(int nShowCmd,const CDockState* pDock = NULL) {return FALSE;};
inline void HideStartWnd() {};


////////////////////////////////////////////////
// Unsorted functions
////////////////////////////////////////////////
// maz�n� objekt� z pole a seznamu
void DestroyListObjects(CObList* pToReset);
void DestroyArrayObjects(CObArray* pToReset);

BOOL MntExistFile(LPCTSTR pFileName);

#define MB_DEFAULT	0xFFFFFFFF


#define MNTL_TIMER_1  9999  // animace bmp
#define MNTL_TIMER_2  9998  // semafor
#define MNTL_TIMER_3  9997	// autoscrol

// style message	
#define MCM_GETSTYLE		0x0500
#define MCM_SETSTYLE		0x0501

#define MCM_GETPARAM		0x0550
#define MCM_SETPARAM		0x0551

// semaphore styles
#define MSMS_COLOR_MASK		0x0FFF
#define MSMS_COLOR_RED		0x0000
#define MSMS_COLOR_YELLOW	0x0001
#define MSMS_COLOR_GREEN	0x0002
#define MSMS_COLOR_LGBLUE	0x0003
#define MSMS_COLOR_BLUE		0x0004
#define MSMS_COLOR_CYAN		0x0005
#define MSMS_COLOR_DISABLED	0x00FF

#define MSMS_COLOR_LIVE		0x1000
#define MSMS_DISABLE_RED	0x2000

TypeIsSimple(POINT);
typedef AutoArray<POINT> CPointArray;

BOOL MntDeleteFile(LPCTSTR pFileName);

// paint bitmaps
SIZE GetBitmapSize(HBITMAP hBmp);

void PaintSpecBitmapRect(HDC hDC,HBITMAP hBmp,
								 int x,int y,
                 POINT From,SIZE Size,DWORD Style=SRCCOPY);

void PaintSpecBitmapRect(HDC hDC,HBITMAP hBmp,
								 RECT  To,
                 POINT From,SIZE Size,DWORD Style=SRCCOPY);

void MntReportMemoryException(int strID = -1);
void MntInvalidFileFormat(LPCTSTR pFileName = NULL); 

// diastance points x and y
LONG GetDistancePoints(LONG xx,LONG xy,LONG yx,LONG yy);
LONG GetDistancePoints(POINT x,POINT y);

///////////////////////////////////////////////////////////////////////////
// FMntVector2

class FMntVector2
{
protected:
  float _v[2];
public:
  FMntVector2() {_v[0] = 0; _v[1] = 0;};
  FMntVector2(float x, float y) {_v[0] = x; _v[1] = y;};
  FMntVector2(const FMntVector2& vect) {_v[0] = vect._v[0]; _v[1] = vect._v[1];};

  float& operator[](int i) {return _v[i];};
  float operator[](int i) const {return _v[i];};

  float operator*(const FMntVector2& vect) const {return _v[0] * vect._v[0] + _v[1] * vect._v[1];};  
  const FMntVector2& operator*=(float coef) {_v[0] *= coef; _v[1] *= coef; return *this;};  
  FMntVector2 operator-() {return FMntVector2(-_v[0], -_v[1]);}; 

  FMntVector2 operator-(const FMntVector2& vect) {return FMntVector2(_v[0] - vect._v[0], _v[1] - vect._v[1]);}; 
  FMntVector2 operator+(const FMntVector2& vect) {return FMntVector2(_v[0] + vect._v[0], _v[1] + vect._v[1]);}; 

  const FMntVector2& operator+=(const FMntVector2& vect) {_v[0] += vect._v[0]; _v[1] += vect._v[1]; return *this;};
  const FMntVector2& operator-=(const FMntVector2& vect) {_v[0] -= vect._v[0]; _v[1] -= vect._v[1]; return *this;};


  float SquareSize() const {return _v[0]*_v[0] + _v[1]*_v[1];};
  void Normalize() { 
    float fSSize = SquareSize(); 
    if (fSSize<= 0)
      return;
    fSSize = sqrtf(fSSize);
    _v[0] /= fSSize; _v[1] /= fSSize;};

  float Size() const {return sqrtf(SquareSize());};


#define PI 3.14159265359f    
#define GRADTORAD(x) ((x)/180.0f * PI)
#define RADTOGRAD(x) ((x)*180.0f / PI)

  void FromAngle(int grad) 
  {
    _v[1] = sin(GRADTORAD(grad));
    _v[0] = cos(GRADTORAD(grad));
  };

  // returns values from -180 to 180
  float GradAngleTo(const FMntVector2& pt) const
  {
    float size = Size();
    float sizePt = pt.Size();

    float cosA = operator*(pt);
    float sinA = (_v[0] * pt[1] - _v[1] * pt[0]) / (float) size / (float) sizePt;

    float alfa = asinf(sinA);
    alfa = RADTOGRAD(alfa);

    if (cosA < 0)
      alfa = 180 - alfa;

    if (alfa > 180)
      alfa -= 360;

    return alfa;
  }
};

typedef const CPoint& CPointVal;

class FMntVectorLong2 : public CPoint
{
public:
  FMntVectorLong2() : CPoint(0,0) {};
  FMntVectorLong2(LONG x, LONG y) : CPoint(x,y) {}; 
  FMntVectorLong2(const POINT& v) : CPoint(v) {}; 
  FMntVectorLong2(CPoint from, CPoint to) : CPoint(to - from) {};
  
  LONG operator*(const POINT& pt) const {return x * pt.x + y * pt.y;};
  float operator*(const FMntVector2& pt) const {return x * pt[0] + y * pt[1];};
  const FMntVectorLong2& operator*=(const LONG val) {x *= val; y *= val; return *this;};
  LONG operator[](const int i) const {ASSERT(i < 2); return (i == 0) ? x : y;};  
  LONG& operator[](const int i) {ASSERT(i < 2); return (i == 0) ? x : y;};
  const FMntVectorLong2& operator=(CPointVal v) {*((CPoint*) this) = v; return *this;};

  LONG Size() const {return (LONG) sqrt(x * x + y * y);};
  float GradAngleTo(const POINT& pt) const
  {
    int size = Size();
    int sizePt = FMntVectorLong2(pt).Size();

    float cosA = operator*(pt) / (float) size / (float) sizePt;
    float sinA = (x * pt.y - y * pt.x) / (float) size / (float) sizePt;

    float alfa = asinf(sinA);    
    alfa = RADTOGRAD(alfa);

    if (cosA < 0)
      alfa = 180 - alfa;

    return alfa;
  }
};

typedef const FMntVectorLong2& FMntVectorLong2Val;


//////////////////////////////////////////////////////////////////////////
// FMntMatrix2

class FMntMatrix2
{
protected:
  float _m[4];

public:
  FMntMatrix2() {};
  
  float& operator()(int i, int j) {return _m[i *2 + j];};
  const float& operator()(int i, int j) const {return _m[i *2 + j];};

  FMntVector2 operator*(const FMntVector2& in) const
    {return FMntVector2(_m[0] * in[0] + _m[1] * in[1], _m[2] * in[0] + _m[3] * in[1]);};

  CPoint operator*(CPointVal in) const
    {return CPoint((LONG) (_m[0] * in.x + _m[1] * in.y), (LONG) (_m[2] * in.x + _m[3] * in.y));};   

  void Rotation(float grad) {grad = GRADTORAD(grad); _m[0] = _m[3] = cos(grad); _m[1] = -sin(grad); _m[2] = -_m[1];};

  FMntMatrix2 NegativeY() const {FMntMatrix2 ret; ret._m[0] = _m[0]; ret._m[1] = -_m[1]; ret._m[2] = -_m[2]; ret._m[3] =_m[3];return ret;};
  
};


typedef const FMntMatrix2& FMntMatrix2Val;


//////////////////////////////////////////////////////////////////////////
// FMntRect

/*typedef struct tagFltRECT
{
  float    left;
  float    top;
  float    right;
  float    bottom;
} FLTRECT, *PFLTRECT, NEAR *NPFLTRECT, FAR *LPFLTRECT;

typedef const LPFLTRECT LPCFLTRECT;
*/
class FltRect 
{
public:
  union
  { 
    float _m[4];
    struct 
    {
      float    left;
      float    top;
      float    right;
      float    bottom;
    };
  };
  // Constructors
public:
  // uninitialized rectangle
  FltRect() : left(0),  top(0), right(0), bottom(0) {};
  // from left, top, right, and bottom
  FltRect(float l, float t, float r, float b)  : left(l),  top(t), right(r), bottom(b) {}; 
  // copy constructor
  FltRect(const FltRect& srcRect) { operator=(srcRect);}; 
  // from a point and size
  //FltRect(FMntVector2 point, FMntVector2 size) { 
  //  _m[0] = point[0]; _m[1] = point[1]; _m[2] = _m[0] + size[0]; m[3] = _m[1] + size[1];}
  // from two points
  FltRect(FMntVector2 topLeft, FMntVector2 bottomRight) 
  { SetRect(topLeft, bottomRight);};
 
  // Attributes (in addition to RECT members)

  // retrieves the width
  float Width() const {return _m[2] - _m[0];};  
  // returns the height
  float Height() const {return _m[3] - _m[1];};  
  // returns the size
  FMntVector2 Size() const {return FMntVector2(_m[2]-_m[0],_m[3]-_m[1]);};  
  // the top-left point
  FMntVector2 TopLeft() const {return FMntVector2(_m[0],_m[1]);};  
  // the bottom-right point
  FMntVector2 BottomRight() const {return FMntVector2(_m[2],_m[3]);}; 
  // the geometric center point of the rectangle
  FMntVector2 CenterPoint() const {return FMntVector2((_m[2]+_m[0])/2,(_m[3]+_m[1])/2);};  
   
  // returns TRUE if rectangle has no area
  BOOL IsRectEmpty() const {return ((Width() == 0) && (Height() == 0));};
  // returns TRUE if rectangle is at (0,0) and has no area
  BOOL IsRectNull() const {return (_m[0] == 0 && _m[1] == 0 && _m[2] == 0 && _m[3] == 0);};
  // returns TRUE if point is within rectangle
  BOOL PtInRect(FMntVector2 point) const 
  {return (_m[0] <= point[0] && point[0] <= _m[1] && _m[2] <= point[1] && point[1] <= _m[3]);};

  // Operations
  // set rectangle from left, top, right, and bottom
  void SetRect(float x1, float y1, float x2, float y2) {_m[0] = x1; _m[1] = y1; _m[2] = x2; _m[3] = y2;};
  void SetRect(FMntVector2 topLeft, FMntVector2 bottomRight) 
  {_m[0] = topLeft[0]; _m[1] = topLeft[1]; _m[2] = bottomRight[0]; _m[3] = bottomRight[1];}
  // empty the rectangle
  void SetRectEmpty() {_m[0] = _m[1] = _m[2] = _m[3] = 0;};  

  // Inflate rectangle's width and height by
  // x units to the left and right ends of the rectangle
  // and y units to the top and bottom.
  //void InflateRect(int x, int y) // throw();
  // Inflate rectangle's width and height by
  // size.cx units to the left and right ends of the rectangle
  // and size.cy units to the top and bottom.
  //void InflateRect(SIZE size) // throw();
  // Inflate rectangle's width and height by moving individual sides.
  // Left side is moved to the left, right side is moved to the right,
  // top is moved up and bottom is moved down.
  //void InflateRect(LPCRECT lpRect) // throw();
  //void InflateRect(int l, int t, int r, int b) // throw();

  // deflate the rectangle's width and height without
  // moving its top or left
  //void DeflateRect(int x, int y) // throw();
  //void DeflateRect(SIZE size) // throw();
  //void DeflateRect(LPCRECT lpRect) // throw();
  //void DeflateRect(int l, int t, int r, int b) // throw();

  // translate the rectangle by moving its top and left
  //void OffsetRect(int x, int y) // throw();
  //void OffsetRect(SIZE size) // throw();
  //void OffsetRect(POINT point) // throw();
  //void NormalizeRect() // throw();

  // absolute position of rectangle
  //void MoveToY(int y) // throw();
  //void MoveToX(int x) // throw();
  //void MoveToXY(int x, int y) // throw();
  //void MoveToXY(POINT point) // throw();

  // set this rectangle to intersection of two others
  //BOOL IntersectRect(LPCRECT lpRect1, LPCRECT lpRect2) // throw();

  // set this rectangle to bounding union of two others
  //BOOL UnionRect(LPCRECT lpRect1, LPCRECT lpRect2) // throw();

  // set this rectangle to minimum of two others
  //BOOL SubtractRect(LPCRECT lpRectSrc1, LPCRECT lpRectSrc2) // throw();

  // Additional Operations
  void operator=(const FltRect& srcRect) 
  { SetRect(srcRect._m[0], srcRect._m[1],
    srcRect._m[2],srcRect._m[3]);};  
  BOOL operator==(const FltRect& rect) const
  {return (_m[0] == rect._m[0] && _m[1] == rect._m[1] && 
    _m[2] == rect._m[2] && _m[3] == rect._m[3]);};
  BOOL operator!=(const FltRect& rect) const {return !operator==(rect);};

  float& operator[](int i) {return _m[i];};
  float operator[](int i) const {return _m[i];};
  
  /*void operator+=(POINT point) // throw();
  void operator+=(SIZE size) // throw();
  void operator+=(LPCRECT lpRect) // throw();
  void operator-=(POINT point) // throw();
  void operator-=(SIZE size) // throw();
  void operator-=(LPCRECT lpRect) // throw();
  void operator&=(const RECT& rect) // throw();
  void operator|=(const RECT& rect) // throw();

  // Operators returning CRect values
  CRect operator+(POINT point) const // throw();
  CRect operator-(POINT point) const // throw();
  CRect operator+(LPCRECT lpRect) const // throw();
  CRect operator+(SIZE size) const // throw();
  CRect operator-(SIZE size) const // throw();
  CRect operator-(LPCRECT lpRect) const // throw();
  CRect operator&(const RECT& rect2) const // throw();
  CRect operator|(const RECT& rect2) const // throw();
  //CRect MulDiv(int nMultiplier, int nDivisor) const throw();
  */
};


///////////////////////////////////////////
// Serialization
///////////////////////////////////////////

// podpora serializace
void MntSerializeString(CArchive& ar, CString&, LPCTSTR pFileName = NULL);
void MntSerializeRect(CArchive& ar, CRect&, LPCTSTR pFileName = NULL);
void MntSerializePoint(CArchive& ar, CPoint&, LPCTSTR pFileName = NULL);
void MntSerializeChar(CArchive& ar, TCHAR&, LPCTSTR pFileName = NULL);
void MntSerializeBOOL(CArchive& ar, BOOL&, LPCTSTR pFileName = NULL);
void MntSerializeBool(CArchive& ar, bool&, LPCTSTR pFileName = NULL);
void MntSerializeByte(CArchive& ar, BYTE&, LPCTSTR pFileName = NULL);
void MntSerializeInt(CArchive& ar, int&, LPCTSTR pFileName = NULL);
void MntSerializeLong(CArchive& ar, LONG&, LPCTSTR pFileName = NULL);
void MntSerializeWord(CArchive& ar, WORD&, LPCTSTR pFileName = NULL);
void MntSerializeDWord(CArchive& ar, DWORD&, LPCTSTR pFileName = NULL);
void MntSerializeFloat(CArchive& ar, float&, LPCTSTR pFileName = NULL);
void MntSerializeDouble(CArchive& ar, double&, LPCTSTR pFileName = NULL);
void MntSerializeColor(CArchive& ar, COLORREF& col, LPCTSTR pFileName = NULL);
void MntSerializeFont(CArchive& ar, LOGFONT& font, LPCTSTR pFileName = NULL);
void MntSerializeFltRect(CArchive& ar, FltRect&, LPCTSTR pFileName = NULL);

/////////////////////////////////////////
// Rectangle intersections
/////////////////////////////////////////

inline BOOL IsIntesectLine(LONG a1, LONG a2, LONG b1, LONG b2)
{
  return !((a2 <= b1) || (b2 <= a1));
}

inline BOOL IsIntersectRect(const RECT& rect1, const RECT& rect2)
{
  return IsIntesectLine(rect1.left,rect1.right,rect2.left,rect2.right) && 
    IsIntesectLine(rect1.top,rect1.bottom,rect2.top,rect2.bottom);
}

inline BOOL IsLineInLine(LONG a1, LONG a2, LONG b1, LONG b2)
{
  return ((b1 <= a1) && (a2 <= b2));
}

inline BOOL IsRectInRect(const RECT& rect1, const RECT& rect2)
{
  return IsLineInLine(rect1.left,rect1.right,rect2.left,rect2.right) && 
    IsLineInLine(rect1.top,rect1.bottom,rect2.top,rect2.bottom);
}

inline BOOL IsRectAInRectB(const RECT& rA,const RECT& rB,BOOL bFull)
{
  if (bFull)
    return IsRectInRect(rA,rB);
  else
    return IsIntersectRect(rA,rB);	
};

///////////////////////////////////////////////////////
// CFMnt*File
//////////////////////////////////////////////////////

class CFMntFromToFile : public CMirrorFile
{
  typedef CMirrorFile base;
protected:
  ULONGLONG _from;
  ULONGLONG _to;
  ULONGLONG _realto;

  CString _originFile;

public: 
  CFMntFromToFile() : CMirrorFile(), _from(0), _to(_UI64_MAX), _realto(0) {};

  void StartFromToSession(ULONGLONG from, ULONGLONG to, const CString& origin);
  void EndFromToSession(ULONGLONG &to);

  virtual ULONGLONG Seek(LONGLONG lOff, UINT nFrom);   
  virtual void Write(const void* lpBuf, UINT nCount);
};


///////////////////////////////////////////////////////
// COLORs
//////////////////////////////////////////////////////

COLORREF RGBToY(const COLORREF& rgb);

///////////////////////////////////////////////////////
// BitArray
//////////////////////////////////////////////////////

class BitArray;

class BitArrayMember
{
protected:
  const int _byte;   
  const BYTE _bit; 
  BitArray& _array;
  
public:
  inline BitArrayMember(BitArray& array, int pos);
  inline operator bool() const;
  inline bool operator=(bool val);
};

class BitArray
{
  friend BitArrayMember;
protected:
  BYTE * _values; 
  int _size;
  int _sizeInBytes;
public:
  BitArray() : _values(NULL), _size(0), _sizeInBytes(0) {};
  ~BitArray() { Clear();} ;

  void Clear() {delete [] _values; _values = NULL; _size = _sizeInBytes = 0;};
  void Alloc(int size) {_size = size; _sizeInBytes = (_size >> 3) + 1; _values = new BYTE[_sizeInBytes]; Neutralize();};
  void Realloc(int size)
  {
    if (_size >= size) return;
    int sizeInBytes =  (size >> 3) + 1; 
    if (_sizeInBytes == sizeInBytes) 
    {
      _size = size;
      return;
    };

    BYTE * newData = new BYTE[sizeInBytes];
    
    if (_sizeInBytes > 0)
    {
      memcpy(newData, _values, _sizeInBytes);
      memset(newData + _sizeInBytes, 0x0, sizeInBytes - _sizeInBytes);
    }
    else
      memset(newData, 0x0, sizeInBytes);    

    delete [] _values;
    _values = newData;
    _size = size;
    _sizeInBytes = sizeInBytes;
  };

  const BitArrayMember operator[](int i) const {return BitArrayMember(*const_cast<BitArray*>(this), i);};
  BitArrayMember operator[](int i) {return BitArrayMember(*this, i);};

  void DoSerialize(CArchive& ar)
  {
    for(int i = 0; i < _sizeInBytes; i++)
    {      
      MntSerializeByte(ar, _values[i]);                  
    };  
  }

  void Neutralize() {memset(_values, 0x0, _sizeInBytes); };
};

BitArrayMember::BitArrayMember(BitArray& array, int pos) : _array(array) , _byte(pos >> 3), _bit(1 << (pos & 7)) {};
BitArrayMember::operator bool() const {return ((_array._values[_byte] & _bit) == _bit);};

bool BitArrayMember::operator=(bool val) 
{
  if (val)
    _array._values[_byte] |= _bit;    
  else
    _array._values[_byte] &= ~_bit;  

  return *this;
};
const int STORAGE_GROW = 1024;
class IDStorage : public BitArray
{
protected: 
  int _freeIndx; 

  void FindNextFreeID()
  {
    for(_freeIndx++; _freeIndx < _size; _freeIndx++)
      if (!operator[](_freeIndx))
        return;
  }

public:
  IDStorage() : _freeIndx(0), BitArray() {};
  void Init() {Neutralize();};
 
  int ReserveID()
  {
    int ret = _freeIndx;
    FindNextFreeID();
    
    if (ret >= _size)
      Realloc(ret + STORAGE_GROW); 

    Assert(!operator[](ret));
    Assert(_freeIndx >= _size || !operator[](_freeIndx));
    operator[](ret) = true;
    return ret;
  }

  void ReserveID(int ID)
  {
    if (ID < _size)
    {
      Assert(!operator[](ID));
    }
    else
    {
      Realloc(ID + STORAGE_GROW);      
    }
    Assert(!operator[](ID));



    operator[](ID) = true;
    if (ID == _freeIndx)
      FindNextFreeID();

    Assert(_freeIndx >= _size || !operator[](_freeIndx));
  }

  void ReleaseID(int ID)
  {
    Assert(ID < _size);
    operator[](ID) = false;
    if (ID < _freeIndx)
      _freeIndx = ID;

    Assert(_freeIndx >= _size || !operator[](_freeIndx));
  }
};



#endif
