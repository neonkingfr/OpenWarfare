#include "stdAfx.h"
#include "resource.h"
#include "FMntTools.h"

/////////////////////////////////////////////////
// CFMntOprions - new impl.
/////////////////////////////////////////////////

IMPLEMENT_DYNAMIC(CFMntOptions, CObject)

void CFMntOptions::SetDefaultValues()
{
    Init();
    // default values according to OptionsMap
    const FMNT_OPTMAP_ENTRY * pMap = GetOptionsMap();
    for(int i = 0; pMap[i].nItemType !=  CFMntOptions::optEnd; i++ )
    {
        const FMNT_OPTMAP_ENTRY& tEntry = pMap[i];
        switch(tEntry.nItemType)
        {
        case  CFMntOptions::optBool: 
            *(BOOL *)tEntry.nItemValue = tEntry.nItemDefault.nInt;
            break;
        case  CFMntOptions::optInt: 
            *(int *)tEntry.nItemValue = tEntry.nItemDefault.nInt;
            break;
        case  CFMntOptions::optLong: 
            *(long *)tEntry.nItemValue = tEntry.nItemDefault.nLong;
            break;
        case  CFMntOptions::optChar: 
            *(TCHAR *)tEntry.nItemValue = tEntry.nItemDefault.nInt;
            break;
        case  CFMntOptions::optWord: 
            *(WORD *)tEntry.nItemValue = tEntry.nItemDefault.nWord;
            break;
        case  CFMntOptions::optDWord: 
            *(DWORD *)tEntry.nItemValue = tEntry.nItemDefault.nDWord;
            break;
        case  CFMntOptions::optFloat: 
            *(float *)tEntry.nItemValue = tEntry.nItemDefault.nFloat;
            break;
        case  CFMntOptions::optDouble: 
            *(double *)tEntry.nItemValue = tEntry.nItemDefault.nDouble;
            break;
        case  CFMntOptions::optColor: 
            *(COLORREF*)tEntry.nItemValue = tEntry.nItemDefault.nDWord;
            break;
        case  CFMntOptions::optString: 
            *(CString*)tEntry.nItemValue = (LPCTSTR) (tEntry.nItemDefault.nDWord);
            break;
        case  CFMntOptions::optClass: 
            ((CFMntOptions *)tEntry.nItemValue)->SetDefaultValues();
            break;
        default:
            //todo logging
            ASSERT(false);
        }
    }
}

BOOL CFMntOptions::LoadParams(BOOL bReportErr /*= TRUE*/)
{
  Init();    

  if (m_pParentClass.IsNull())
  {
    SetDefaultValues();
    return FALSE;
  }

  ParamEntryPtr pEntry = m_pParentClass->FindEntry(m_cClassName);
  if (pEntry.IsNull() || !pEntry->IsClass())
  {
    SetDefaultValues();
    return FALSE;
  }

  ParamClassPtr pClass = pEntry->GetClassInterface();

  const FMNT_OPTMAP_ENTRY * pMap = GetOptionsMap();
  for(int i = 0; pMap[i].nItemType !=  CFMntOptions::optEnd; i++ )
  {
    const FMNT_OPTMAP_ENTRY& tEntry = pMap[i];

    pEntry = pClass->FindEntry(tEntry.pItemKeyName);
    if (pEntry.IsNull())
    {
      switch(tEntry.nItemType)
      {
      case  CFMntOptions::optBool: 
        *(BOOL *)tEntry.nItemValue = tEntry.nItemDefault.nInt;
        break;
      case  CFMntOptions::optInt: 
        *(int *)tEntry.nItemValue = tEntry.nItemDefault.nInt;
        break;
      case  CFMntOptions::optLong: 
        *(long *)tEntry.nItemValue = tEntry.nItemDefault.nLong;
        break;
      case  CFMntOptions::optChar: 
        *(TCHAR *)tEntry.nItemValue = tEntry.nItemDefault.nInt;
        break;
      case  CFMntOptions::optWord: 
        *(WORD *)tEntry.nItemValue = tEntry.nItemDefault.nWord;
        break;
      case  CFMntOptions::optDWord: 
        *(DWORD *)tEntry.nItemValue = tEntry.nItemDefault.nDWord;
        break;
      case  CFMntOptions::optFloat: 
        *(float *)tEntry.nItemValue = tEntry.nItemDefault.nFloat;
        break;
      case  CFMntOptions::optDouble: 
        *(double *)tEntry.nItemValue = tEntry.nItemDefault.nDouble;
        break;
      case  CFMntOptions::optColor: 
        *(COLORREF*)tEntry.nItemValue = tEntry.nItemDefault.nDWord;
        break;
      case  CFMntOptions::optString: 
        *(CString*)tEntry.nItemValue = (LPCTSTR) (tEntry.nItemDefault.nDWord);
        break;
      case  CFMntOptions::optClass: 
        ((CFMntOptions *)tEntry.nItemValue)->SetInOutParamFile(tEntry.pItemKeyName, pClass);
        ((CFMntOptions *)tEntry.nItemValue)->LoadParams(); 
        ((CFMntOptions *)tEntry.nItemValue)->ReleaseInOutParamFile();
        break;
      default:
        //todo logging
        ASSERT(false);
      }
      continue;
    }

    switch(tEntry.nItemType)
    {
    case  CFMntOptions::optBool:                          
      *(BOOL *)tEntry.nItemValue = (bool)(*pEntry);
      break;
    case  CFMntOptions::optInt:
      *(int *)tEntry.nItemValue = (int)(*pEntry);
      break;
    case  CFMntOptions::optLong:            
      *(long *)tEntry.nItemValue = (int)(*pEntry);
      break;
    case  CFMntOptions::optChar:              
      *(TCHAR *)tEntry.nItemValue = ((RStringB)(*pEntry))[0];
      break;
    case  CFMntOptions::optWord:            
      *(WORD *)tEntry.nItemValue = (int)(*pEntry);
      break;
    case  CFMntOptions::optDWord:             
      *(DWORD *)tEntry.nItemValue = (int)(*pEntry);
      break;
    case  CFMntOptions::optFloat:             
      *(float *)tEntry.nItemValue = (float)(*pEntry);
      break;
    case  CFMntOptions::optDouble:              
      *(double *)tEntry.nItemValue = (float)(*pEntry);
      break;
    case  CFMntOptions::optColor:              
      *(COLORREF*)tEntry.nItemValue = (int)(*pEntry);
      break;
    case  CFMntOptions::optString:             
      *(CString*)tEntry.nItemValue = (LPCSTR)((RStringB)(*pEntry));
      break;
    case  CFMntOptions::optClass:            
      ((CFMntOptions *)tEntry.nItemValue)->SetInOutParamFile(tEntry.pItemKeyName, pClass);
      ((CFMntOptions *)tEntry.nItemValue)->LoadParams(); 
      ((CFMntOptions *)tEntry.nItemValue)->ReleaseInOutParamFile();

      break;
    case CFMntOptions::optArray:
      {
        FMT_OPTMAP_ENTRY_ARRAY * pArrayOptmapEntry = (FMT_OPTMAP_ENTRY_ARRAY *) tEntry.nItemValue;

        if (pArrayOptmapEntry->nArrayItemType == CFMntOptions::optClass)
        {
          if (!pEntry->IsClass())
          {
            ASSERT(FALSE);
            break;
          }

          const int nSize = pArrayOptmapEntry->nSize;
          char pszTemp[256];

          BYTE * pArray = (BYTE *) pArrayOptmapEntry->pArray;

          for(int j = 0; j < nSize; j++)
          {
            sprintf(pszTemp, "item_%d", j);                         
            ((CFMntOptions *)pArray)->SetInOutParamFile(pszTemp, pEntry->GetClassInterface());
            ((CFMntOptions *)pArray)->LoadParams();  
            ((CFMntOptions *)pArray)->ReleaseInOutParamFile();
            pArray += pArrayOptmapEntry->nStep;
          }

          break;

        }

        if (!pEntry->IsArray())
        {
          ASSERT(FALSE);
          break;
        }



        int nSize = pEntry->GetSize();
        if (nSize > pArrayOptmapEntry->nSize)
        {
          // TODO: LOG
          nSize = pArrayOptmapEntry->nSize;
        }

        switch(pArrayOptmapEntry->nArrayItemType)
        {
#define LOADTOARRAY(typearray, typeentry)  {\
  typearray * pArray = (typearray *) pArrayOptmapEntry->pArray;\
  for(int j = 0; j < nSize; j++)\
          {\
          pArray[j] = (typeentry) (*pEntry)[j];\
          }\
          break;\
}

    case  CFMntOptions::optBool:
      LOADTOARRAY(BOOL, bool);                     
    case  CFMntOptions::optInt:
      LOADTOARRAY(int, int);                     
    case  CFMntOptions::optLong:
      LOADTOARRAY(long, int);                    
    case  CFMntOptions::optWord:
      LOADTOARRAY(WORD, int);                    
    case  CFMntOptions::optColor: 
      {
        COLORREF * pArray = (COLORREF *) pArrayOptmapEntry->pArray;
        for(int j = 0; j < nSize; j++)
        {
          pArray[j] = (int) (*pEntry)[j];
        }
        break;
      }                                                        
    case  CFMntOptions::optChar:
      {
        char * pArray = (char *) pArrayOptmapEntry->pArray;
        for(int j = 0; j < nSize; j++)
        {
          pArray[j] = ((RStringB) (*pEntry)[j])[0];
        }
        break;
      }                              
    case  CFMntOptions::optFloat:
      LOADTOARRAY(float, float);  
    case  CFMntOptions::optDouble: 
      LOADTOARRAY(double, float);
    case  CFMntOptions::optString:
      {
        CString * pArray = (CString *) pArrayOptmapEntry->pArray;
        for(int j = 0; j < nSize; j++)
        {
          pArray[j] = (LPCSTR)((RStringB) (*pEntry)[j]);
        }
        break;
      }                      
      break;
    case  CFMntOptions::optClass:
    case  CFMntOptions::optArray:
      //TODO LOG 
      break;                                        
    default:
      //TODO LOG
      ASSERT(false);
        }
        break;
      }
    default:
      //TODO: logging
      ASSERT(false);
    }
  }

  return true;
}
void CFMntOptions::Init()
{
    if (!m_bInitialized)
    {
        InitOptionsMap();
        m_bInitialized = TRUE;
    }
}

BOOL CFMntOptions::SaveParams(BOOL bReportErr /*= TRUE*/)
{
    Init();
    
    if (m_pParentClass.IsNull()) return FALSE;

    m_pParentClass->Delete(m_cClassName);    

    ParamClassPtr pClass = m_pParentClass->AddClass(m_cClassName);

    const FMNT_OPTMAP_ENTRY* pMap = GetOptionsMap();
    for(int i = 0; pMap[i].nItemType !=  CFMntOptions::optEnd; i++ )
    {
        const FMNT_OPTMAP_ENTRY& tEntry = pMap[i];
        
        switch(tEntry.nItemType)
        {
        case  CFMntOptions::optBool:                       
            pClass->Add(tEntry.pItemKeyName, (int)*(BOOL *)tEntry.nItemValue);           
            break;
        case  CFMntOptions::optInt:
            pClass->Add(tEntry.pItemKeyName, *(int *)tEntry.nItemValue); 
            break;
        case  CFMntOptions::optLong:            
            pClass->Add(tEntry.pItemKeyName, (int)*(long *)tEntry.nItemValue); 
            break;
        case  CFMntOptions::optChar: 
            pClass->Add(tEntry.pItemKeyName, RStringB(RString((char*)tEntry.nItemValue,1)));            
            break;
        case  CFMntOptions::optWord:            
            pClass->Add(tEntry.pItemKeyName, (int)*(WORD *)tEntry.nItemValue); 
            break;
        case  CFMntOptions::optDWord:             
            pClass->Add(tEntry.pItemKeyName, (int)*(DWORD *)tEntry.nItemValue); 
            break;
        case  CFMntOptions::optFloat:             
            pClass->Add(tEntry.pItemKeyName, *(float *)tEntry.nItemValue); 
            break;
        case  CFMntOptions::optDouble:              
            pClass->Add(tEntry.pItemKeyName, (float)*(double *)tEntry.nItemValue);
            break;
        case  CFMntOptions::optColor:              
            pClass->Add(tEntry.pItemKeyName, (int)*(COLORREF*)tEntry.nItemValue); 
            break;
        case  CFMntOptions::optString:             
            pClass->Add(tEntry.pItemKeyName, RStringB((LPCSTR)*(CString*)tEntry.nItemValue));
            break;
        case  CFMntOptions::optClass:             
            ((CFMntOptions *)tEntry.nItemValue)->SetInOutParamFile(tEntry.pItemKeyName, pClass);
            ((CFMntOptions *)tEntry.nItemValue)->SaveParams();
            ((CFMntOptions *)tEntry.nItemValue)->ReleaseInOutParamFile();
            

            //TODO: solve it later
            //*(LOGFONT*)tEntry.nItemValue = *(LOGFONT*) (tEntry.nItemDefault.nDWord);
            break;
        case CFMntOptions::optArray:
            {
                FMT_OPTMAP_ENTRY_ARRAY * pArrayOptmapEntry = (FMT_OPTMAP_ENTRY_ARRAY *) tEntry.nItemValue;

                if (pArrayOptmapEntry->nArrayItemType == CFMntOptions::optClass)
                {
                    ParamClassPtr pSubClass = pClass->AddClass(tEntry.pItemKeyName);
                    if (pSubClass.IsNull())
                    {
                        ASSERT(FALSE);
                        break;
                    }

                    const int nSize = pArrayOptmapEntry->nSize;
                    char pszTemp[256];

                    BYTE * pArray = (BYTE *) pArrayOptmapEntry->pArray;

                    for(int j = 0; j < nSize; j++)
                    {
                        sprintf(pszTemp, "item_%d", j);                          
                        ((CFMntOptions *)pArray)->SetInOutParamFile(pszTemp, pSubClass);
                        ((CFMntOptions *)pArray)->SaveParams();
                         ((CFMntOptions *)pArray)->ReleaseInOutParamFile();
                        
                        pArray += pArrayOptmapEntry->nStep;
                    }

                    break;

                }

                ParamEntryPtr pParamArray = pClass->AddArray(tEntry.pItemKeyName);
                if (pParamArray.IsNull())
                {
                    ASSERT(FALSE);
                    break;
                }
                                                
                int nSize = pArrayOptmapEntry->nSize;
                pParamArray->ReserveArrayElements(nSize);

                if (nSize > pArrayOptmapEntry->nSize)
                {
                    // TODO: LOG
                    nSize = pArrayOptmapEntry->nSize;
                }
                
                switch(pArrayOptmapEntry->nArrayItemType)
                {
#define SAVETOARRAY(typearray, typeentry)  {\
                    typearray * pArray = (typearray *) pArrayOptmapEntry->pArray;\
                    for(int j = 0; j < nSize; j++)\
                    {\
                        pParamArray->AddValue((typeentry) pArray[j]);\
                    }\
                    break;\
                }
#pragma warning( push )
#pragma warning(disable : 4800)                   
                case  CFMntOptions::optBool:
                    SAVETOARRAY(BOOL, bool);                     
#pragma warning( pop )
                case  CFMntOptions::optInt:
                    SAVETOARRAY(int, int);                     
                case  CFMntOptions::optLong:
                    SAVETOARRAY(long, int);                    
                case  CFMntOptions::optWord:
                    SAVETOARRAY(WORD, int);                    
                case  CFMntOptions::optColor: 
                    SAVETOARRAY(COLORREF, int);                                      
                case  CFMntOptions::optChar:
                    {
                        char * pArray = (char *) pArrayOptmapEntry->pArray;
                        for(int j = 0; j < nSize; j++)
                        {
                            pParamArray->AddValue( RStringB(RString(pArray + j,1)));
                        }
                        break;
                    }                              
                case  CFMntOptions::optFloat:
                    SAVETOARRAY(float, float);  
                case  CFMntOptions::optDouble: 
                    SAVETOARRAY(double, float);
                case  CFMntOptions::optString:
                    {
                        CString * pArray = (CString *) pArrayOptmapEntry->pArray;
                        for(int j = 0; j < nSize; j++)
                        {
                            pParamArray->AddValue( RStringB((LPCSTR)pArray[j]));
                        }
                        break;
                    }                      
                    break;
                case  CFMntOptions::optClass:
                case  CFMntOptions::optArray:
                    //TODO LOG 
                    break;                                        
                default:
                    //TODO LOG
                    ASSERT(false);
                }
                break;
            }       
        default:
            //todo logging
            ASSERT(false);
        }
    }
     
    Flush();

    return true;
}
    
void CFMntOptions::CopyFrom(CFMntOptions* pFrom)
{  
    if (pFrom == NULL)
    {
        //TODO LOG
        return;
    }

    Init();
    pFrom->Init();

    const FMNT_OPTMAP_ENTRY * pMapTo = GetOptionsMap();
    const FMNT_OPTMAP_ENTRY * pMapFrom = pFrom->GetOptionsMap();

    for(int i = 0; pMapTo[i].nItemType !=  CFMntOptions::optEnd && pMapFrom[i].nItemType !=  CFMntOptions::optEnd; i++ )
    {
        const FMNT_OPTMAP_ENTRY& tEntryTo = pMapTo[i];
        const FMNT_OPTMAP_ENTRY& tEntryFrom = pMapFrom[i];

        if (tEntryTo.nItemType != tEntryFrom.nItemType)
        {
            // TODO LOG
            continue;
        }
                
        switch(tEntryTo.nItemType)
        {
#define COPYENTRY(type) *(type *)tEntryTo.nItemValue = *(type *)tEntryFrom.nItemValue; break;
        case  CFMntOptions::optBool:  
            COPYENTRY(BOOL)                               
        case  CFMntOptions::optInt:
            COPYENTRY(int) 
        case  CFMntOptions::optLong:            
            COPYENTRY(long) 
        case  CFMntOptions::optChar: 
            COPYENTRY(char)  
        case  CFMntOptions::optWord:            
            COPYENTRY(WORD) 
        case  CFMntOptions::optDWord:             
            COPYENTRY(DWORD) 
        case  CFMntOptions::optFloat:             
            COPYENTRY(float) 
        case  CFMntOptions::optDouble:              
            COPYENTRY(double) 
        case  CFMntOptions::optColor:              
            COPYENTRY(COLORREF) 
        case  CFMntOptions::optString:             
            COPYENTRY(CString)
        case  CFMntOptions::optClass:             
            ((CFMntOptions *)tEntryTo.nItemValue)->CopyFrom((CFMntOptions *)tEntryFrom.nItemValue);            
            break;
        case CFMntOptions::optArray:
            {
                FMT_OPTMAP_ENTRY_ARRAY * pArrayOptmapEntryTo = (FMT_OPTMAP_ENTRY_ARRAY *) tEntryTo.nItemValue;
                FMT_OPTMAP_ENTRY_ARRAY * pArrayOptmapEntryFrom = (FMT_OPTMAP_ENTRY_ARRAY *) tEntryFrom.nItemValue;

                if (pArrayOptmapEntryTo->nArrayItemType != pArrayOptmapEntryFrom->nArrayItemType ||
                    pArrayOptmapEntryTo->nSize != pArrayOptmapEntryFrom->nSize)
                {
                    //TODO LOG
                    break;
                }

                if (pArrayOptmapEntryTo->nArrayItemType == CFMntOptions::optClass)
                {
                    

                    const int nSize = pArrayOptmapEntryTo->nSize;                    

                    BYTE * pArrayTo = (BYTE *) pArrayOptmapEntryTo->pArray;
                    BYTE * pArrayFrom = (BYTE *) pArrayOptmapEntryFrom->pArray;

                    for(int j = 0; j < nSize; j++)
                    {
                        ((CFMntOptions *)pArrayTo)->CopyFrom((CFMntOptions *)pArrayFrom);  
                        pArrayTo += pArrayOptmapEntryTo->nStep;
                        pArrayFrom += pArrayOptmapEntryFrom->nStep;
                    }

                    break;

                }
                                                                
                int nSize = pArrayOptmapEntryTo->nSize;                              
                
                switch(pArrayOptmapEntryTo->nArrayItemType)
                {
#define COPYARRAY(type) memcpy(pArrayOptmapEntryTo->pArray, pArrayOptmapEntryFrom->pArray, nSize * sizeof(type)); break;
                    
                case  CFMntOptions::optBool:                    
                    COPYARRAY(BOOL)                     
                case  CFMntOptions::optInt:
                    COPYARRAY(int)                     
                case  CFMntOptions::optLong:
                    COPYARRAY(long)                   
                case  CFMntOptions::optWord:
                    COPYARRAY(WORD)                    
                case  CFMntOptions::optColor: 
                    COPYARRAY(COLORREF);                                    
                case  CFMntOptions::optChar:
                    COPYARRAY(char)                                         
                case  CFMntOptions::optFloat:
                    COPYARRAY(float)
                case  CFMntOptions::optDouble: 
                    COPYARRAY(double)
                case  CFMntOptions::optString:
                    {
                        CString * pArrayTo = (CString *) pArrayOptmapEntryTo->pArray;
                        CString * pArrayFrom = (CString *) pArrayOptmapEntryFrom->pArray;
                        for(int j = 0; j < nSize; j++)
                        {
                            pArrayTo[j] = pArrayFrom[j];                            
                        }
                        break;
                    }                      
                    break;
                case  CFMntOptions::optClass:
                case  CFMntOptions::optArray:
                    //TODO LOG 
                    break;                                        
                default:
                    //TODO LOG
                    ASSERT(false);
                }
								break;
            }       
        default:
            //todo logging
            ASSERT(false);
        }
    }         
}

/////////////////////////////////////////////////////////////////////
// CMntCompressFile = soubor pro kompresi dat

CMntCompressFile::CMntCompressFile()
{
	m_nType = compressNone;
}

CMntCompressFile::~CMntCompressFile()
{};

BOOL CMntCompressFile::SetCompress(BYTE nType)
{	// zat�m neimplementov�na komprese dat
	if (nType != compressNone) return FALSE;
	ASSERT(m_nType == compressNone);
	return TRUE;
};

UINT CMntCompressFile::Read(void* lpBuf,UINT nCount)
{	// zat�m nepou�ito
	ASSERT(m_nType == compressNone);
	return CFile::Read(lpBuf,nCount);
};

void CMntCompressFile::Write(const void* lpBuf,UINT nCount)
{	// zat�m nepou�ito
	ASSERT(m_nType == compressNone);
	CFile::Write(lpBuf,nCount);
};

/////////////////////////////////////////////////////////////////////
// CMntPasswordFile = soubor pro heslov�n� dat

CMntPasswordFile::CMntPasswordFile()
{
	m_nType = pswNone;
};

CMntPasswordFile::~CMntPasswordFile()
{};

BOOL CMntPasswordFile::SetPassword(BYTE nType,LPCSTR pPassword)
{
	if ((nType >= pswNone)&&(nType <= pswXor))
	{
		m_nType = nType;
		if (m_nType == pswXor)
		{
			m_nPswLen = lstrlen(pPassword);
			m_nPswPos = 0;
			ASSERT((m_nPswLen > 0)&&(m_nPswLen <= MAX_PSW_LENGTH));
			lstrcpy(m_sPassword, pPassword);
			return TRUE;
		}
	}
	return FALSE;
};

UINT CMntPasswordFile::Read(void* lpBuf,UINT nCount)
{
	if (nCount  == 0)
		return  0;
	if (m_nType == pswXor)
	{
		char* pBuff = NULL;
		UINT  nRes  = 0;
		TRY
		{
			pBuff = new char[nCount];
			// serializace
			nRes = CMntCompressFile::Read(pBuff,nCount);;
			// odheslov�n� dat v pBuff
			for(UINT i=0;i<nCount;i++)
			{
				// rotace bit�
				int  nPos = (m_sPassword[m_nPswPos] & 0x03) + 1; // 1-4 bity
				WORD nWrd = MAKEWORD(0,pBuff[i]);
				nWrd >>= nPos;
				pBuff[i]  = (BYTE)LOBYTE(nWrd) | (BYTE)HIBYTE(nWrd);
				// xor s heslem
				pBuff[i]  = (pBuff[i] ^ m_sPassword[m_nPswPos]);
				// posun na dal�� byte hesla
				if (++m_nPswPos == m_nPswLen)
					m_nPswPos = 0;
			}
			// kopie dat
			memcpy(lpBuf, pBuff, nCount);
			delete [] pBuff;
		}
		CATCH_ALL(e)
		{
			if (pBuff) delete [] pBuff;
			THROW_LAST();
		}
		END_CATCH_ALL
		return nRes;
	}
	// standardn� serializace
	return CMntCompressFile::Read(lpBuf,nCount);
};

void CMntPasswordFile::Write(const void* lpBuf,UINT nCount)
{
	if (nCount  == 0)
		return;
	if (m_nType == pswXor)
	{
		char* pBuff = NULL;
		TRY
		{
			// kopie dat
			pBuff = new char[nCount];

			memcpy(pBuff, lpBuf, nCount);
			// heslov�n� dat v pBuff
			for(UINT i=0;i<nCount;i++)
			{
				// xor s heslem
				pBuff[i]  = (pBuff[i] ^ m_sPassword[m_nPswPos]);
				// rotace bit�
				WORD nWrd = MAKEWORD(pBuff[i],0);
				int  nPos = (m_sPassword[m_nPswPos] & 0x03) + 1; // 1-4 bity
				nWrd <<= nPos;
				pBuff[i]  = (BYTE)LOBYTE(nWrd) | (BYTE)HIBYTE(nWrd);
				// posun na dal�� byte hesla
				if (++m_nPswPos == m_nPswLen)
					m_nPswPos = 0;
			}
			// serializace
			CMntCompressFile::Write(pBuff,nCount);
			delete [] pBuff;
		}
		CATCH_ALL(e)
		{
			if (pBuff) delete [] pBuff;
			THROW_LAST();
		}
		END_CATCH_ALL
		return;
	}
	// standardn� serializace
	CMntCompressFile::Write(lpBuf,nCount);
};

/////////////////////////////////////////////////////////////////////
// CMntFileObject
IMPLEMENT_DYNAMIC(CMntFileObject,CObject);

CMntFileObject::CMntFileObject()
{
	m_nFlags	= 0;
	m_nFilePos	= 0L;
	m_bModified = FALSE;
};

CMntFileObject::CMntFileObject(LPCTSTR pFileName,WORD nFlags,LONG nOffset)
{
	m_sFileName	= pFileName;
	m_nFlags	= nFlags;
	m_nFilePos	= nOffset;
	m_bModified	= FALSE;
};

CMntFileObject::~CMntFileObject()
{
};

void CMntFileObject::SetFile(LPCTSTR pFileName,LONG nOffset)
{
	m_sFileName	= pFileName;
	m_nFilePos	= nOffset;
};

void CMntFileObject::SetParams(WORD nFlags, LPCTSTR pFileID)
{
	m_nFlags	= nFlags;
	if (m_nFlags & fileID)
	{
		ASSERT(pFileID != NULL);
		ASSERT(lstrlen(pFileID)==MNT_FILE_ID_LEN);
		// kop�ruji identifikaci
		for(int i=0; i<MNT_FILE_ID_LEN; i++)
		{ m_cFileID[i] = pFileID[i]; };
	};
};

BOOL CMntFileObject::OnLoadObject(void* pParams,DWORD nVersion)
{
	MASSERT(m_nFilePos >= 0);	
	MASSERT(m_sFileName.GetLength() > 0);

	if (!MntExistFile(m_sFileName))
	{
		//AfxPSMessageBox(IDS_FILE_NOT_FOUND,m_sFileName,MB_OK|MB_ICONEXCLAMATION);
		return FALSE;
	};
	// load object
	CMntPasswordFile file;
	CFileException   fe;
	if (!file.Open(m_sFileName, CFile::modeRead | CFile::shareDenyWrite, &fe))
	{
		//OnReportSerException(&fe, FALSE, IDS_FAILED_TO_OPEN_FILE);
		return FALSE;
	}
	// archiv
	CArchive loadArchive(&file, CArchive::load | CArchive::bNoFlushOnDelete);
	AfxGetApp()->BeginWaitCursor();

	TRY
	{
		if (m_nFilePos!=0) file.Seek(m_nFilePos, CFile::begin);
		// objekt nen� modifikov�n - b�hem serializace to m��e objekt zm�nit
		m_bModified = FALSE;

		// HLAVI�KA A PARAMETRY
		m_nFileVersion = 0;
		if (m_nFlags & fileID)
		{	// identifikace souboru
			char	cFileID[MNT_FILE_ID_LEN];
			if (loadArchive.Read((LPSTR)cFileID,MNT_FILE_ID_LEN)!=MNT_FILE_ID_LEN)
				MntInvalidFileFormat(m_sFileName);
			// kontrola hlavi�ky
			for(int c=0; c<MNT_FILE_ID_LEN; c++)
				if (cFileID[c] != m_cFileID[c])
					MntInvalidFileFormat(m_sFileName);
		}
		if (m_nFlags & fileVer)
		{	// na�ten� verze
			if (loadArchive.Read(&m_nFileVersion,sizeof(DWORD))!=sizeof(DWORD))
				MntInvalidFileFormat(m_sFileName);
			// kontrola minim�ln� verze
			if ((nVersion != 0)&&(nVersion > m_nFileVersion))
			{	// star� form�t souboru
				//AfxPSMessageBox(IDS_FILE_VER_OLD,m_sFileName,MB_OK|MB_ICONEXCLAMATION);
				AfxThrowUserException();
			}
		}
		if (m_nFlags & fileCompress)
		{	
			// posledn� typ comprese
			BYTE nType = CMntCompressFile::compressNone;
			if (loadArchive.Read(&nType,sizeof(BYTE)) != sizeof(BYTE))
				MntInvalidFileFormat(m_sFileName);
			loadArchive.Flush();
			if (!file.SetCompress(nType))
				MntInvalidFileFormat(m_sFileName); // nezn�m� komprese
		}
		if (m_nFlags & filePassword)
		{	// zah�jen� heslov�n�
			// posledn� typ hesla
			BYTE nType = CMntPasswordFile::pswNone;
			if (loadArchive.Read(&nType,sizeof(BYTE)) != sizeof(BYTE))
				MntInvalidFileFormat(m_sFileName);
			if ((nType != CMntPasswordFile::pswNone)&&(m_nFlags & fileQueryPsw))
			{	// dotaz na heslo
/*
				if (!DoInsertPasswordPrompt(m_sFilePassword))
					AfxThrowUserException();
*/
        //DebugTrap();
				// heslo zad�no
				if (m_sFilePassword.GetLength()==0)
				{	// chybn� heslo
					//DoShowPasswordError();
					AfxThrowUserException();
				}
			}
			loadArchive.Flush();
			if (!file.SetPassword(nType,m_sFilePassword))
				MntInvalidFileFormat(m_sFileName);
			// kontrola hesla pomoc� identifikace souboru
			if (m_nFlags & fileVerifPsw)
			{
				char	cFileID[MNT_FILE_ID_LEN];
				if (loadArchive.Read((LPSTR)cFileID,MNT_FILE_ID_LEN)!=MNT_FILE_ID_LEN)
					MntInvalidFileFormat(m_sFileName);
				// kontrola hlavi�ky
				for(int c=0; c<MNT_FILE_ID_LEN; c++)
					if (cFileID[c] != m_cFileID[c])
				{	// chybn� heslo
          //DebugTrap();
					//DoShowPasswordError();
					AfxThrowUserException();
				}
			}
		}
		if (m_nFlags & fileObjName)
			// jm�no objektu
			MntSerializeString(loadArchive,m_sFileObjectName,m_sFileName);
		if (m_nFlags & fileObjDesr)
			// popis objektu
			MntSerializeString(loadArchive,m_sFileObjectDescription,m_sFileName);

		// provedeno na�ten� objektu		
		DoSerialize(loadArchive, m_nFileVersion, pParams);
		loadArchive.Close();
		file.Close();
		// update po serializaci
		OnSerialize(FALSE, m_nFileVersion, pParams);	
	}
	CATCH_ALL(e)
	{
		file.Abort(); // will not throw an exception
		AfxGetApp()->EndWaitCursor();
    //DebugTrap();
		//OnReportSerException(e, FALSE, IDS_FAILED_TO_OPEN_FILE);
		return FALSE;
	}
	END_CATCH_ALL
	AfxGetApp()->EndWaitCursor();
	return TRUE;
};

BOOL CMntFileObject::OnSaveObject(void* pParams,DWORD nVersion)
{
	MASSERT(m_nFilePos >= 0);	// p�i�azen soubor
	MASSERT(m_sFileName.GetLength() > 0);
	// save object
	CMntPasswordFile file;
	CFileException   fe;
	if (m_nFilePos == 0)
	{ // create file
		if (!file.Open(m_sFileName, CFile::modeCreate |
		  CFile::modeReadWrite | CFile::shareExclusive, &fe))
		{
      //DebugTrap();
			//OnReportSerException(&fe,TRUE, IDS_INVALID_FILENAME);
			return FALSE;
		}
	} else
	{ // append file
		if (!file.Open(m_sFileName, CFile::modeReadWrite | CFile::shareExclusive, &fe))
		{
      //DebugTrap();
			//OnReportSerException(&fe,TRUE, IDS_INVALID_FILENAME);
			return FALSE;
		}
		// nastaven� na danou pozici
		LONG nSeekEnd = (LONG)(file.SeekToEnd());
		if (nSeekEnd!=m_nFilePos)
		{
			file.Abort(); 
			OnReportSerException(&fe, TRUE, AFX_IDP_FILE_EOF);
			return FALSE;
		}
	}
	CArchive saveArchive(&file, CArchive::store | CArchive::bNoFlushOnDelete);
	AfxGetApp()->BeginWaitCursor();
	TRY
	{
		if (nVersion != 0)
			m_nFileVersion = nVersion;
		// p��prava na serializaci
		OnSerialize(TRUE, m_nFileVersion, pParams);	
	
		// HLAVI�KA A PARAMETRY
		if (m_nFlags & fileID)
		{	// identifikace souboru
			saveArchive.Write((LPCSTR)m_cFileID,MNT_FILE_ID_LEN);
		}
		if (m_nFlags & fileVer)
		{	// ulo�en� verze
			MASSERT(m_nFileVersion != 0);
			saveArchive.Write(&m_nFileVersion,sizeof(DWORD));
		}
		if (m_nFlags & fileCompress)
		{	// zah�jen� comprese
			ASSERT(FALSE);		// not implemented
			// posledn� typ comprese
			BYTE nType = CMntCompressFile::compressNone;
			saveArchive.Write(&nType,sizeof(BYTE));
			saveArchive.Flush();
			file.SetCompress(nType);
		}
		if (m_nFlags & filePassword)
		{	// zah�jen� heslov�n�
			// posledn� typ hesla
			BYTE nType = CMntPasswordFile::pswXor;
			if (m_sFilePassword.GetLength()==0)
				nType = CMntPasswordFile::pswNone; // nen� heslo
			saveArchive.Write(&nType,sizeof(BYTE));
			saveArchive.Flush();
			file.SetPassword(nType,m_sFilePassword);
			if (m_nFlags & fileVerifPsw)
			{	// kontrola hesla pomoc� identifikace souboru
				saveArchive.Write((LPCSTR)m_cFileID,MNT_FILE_ID_LEN);
			}
		}
		if (m_nFlags & fileObjName)
			// jm�no objektu
			MntSerializeString(saveArchive,m_sFileObjectName,m_sFileName);
		if (m_nFlags & fileObjDesr)
			// popis objektu
			MntSerializeString(saveArchive,m_sFileObjectDescription,m_sFileName);
		
		// provedeno ulo�en� objektu		
		DoSerialize(saveArchive, m_nFileVersion, pParams);
		saveArchive.Close();
		file.Close();
	}
	CATCH_ALL(e)
	{
		file.Abort(); // will not throw an exception
		AfxGetApp()->EndWaitCursor();
    //DebugTrap();
		//OnReportSerException(e,TRUE, IDS_FAILED_TO_SAVE_FILE);
		return FALSE;
	}
	END_CATCH_ALL

	AfxGetApp()->EndWaitCursor();
	// objekt nen� modifikov�n
	m_bModified = FALSE;
	return TRUE;        // success
};

// vrac� informace ze souboru (bez jeho na�ten�)
// 0 = OK, 1 = ERR, 2 = PSW, 3 = OLD
WORD CMntFileObject::ReadFileObjectName(DWORD nVersion)
{
	MASSERT(m_nFilePos >= 0);	
	MASSERT(m_sFileName.GetLength() > 0);

	if (!MntExistFile(m_sFileName))
		return 1;
	// load object
	CMntPasswordFile file;
	CFileException   fe;
	if (!file.Open(m_sFileName, CFile::modeRead | CFile::shareDenyWrite, &fe))
		return 1;
	// archiv
	CArchive loadArchive(&file, CArchive::load | CArchive::bNoFlushOnDelete);
	TRY
	{
		if (m_nFilePos!=0) file.Seek(m_nFilePos, CFile::begin);
		// HLAVI�KA A PARAMETRY
		m_nFileVersion = 0;
		if (m_nFlags & fileID)
		{	// identifikace souboru
			char	cFileID[MNT_FILE_ID_LEN];
			if (loadArchive.Read((LPSTR)cFileID,MNT_FILE_ID_LEN)!=MNT_FILE_ID_LEN)
				AfxThrowUserException();
			// kontrola hlavi�ky
			for(int c=0; c<MNT_FILE_ID_LEN; c++)
				if (cFileID[c] != m_cFileID[c])
					AfxThrowUserException();
		}
		if (m_nFlags & fileVer)
		{	// na�ten� verze
			if (loadArchive.Read(&m_nFileVersion,sizeof(DWORD))!=sizeof(DWORD))
				AfxThrowUserException();
			// kontrola minim�ln� verze
			if ((nVersion != 0)&&(nVersion > m_nFileVersion))
			{
				file.Abort(); // will not throw an exception
				return 3;
			}
		}
		if (m_nFlags & filePassword)
		{	// zah�jen� heslov�n�
		}
		if (m_nFlags & fileObjName)
			// jm�no objektu
			MntSerializeString(loadArchive,m_sFileObjectName,m_sFileName);
		if (m_nFlags & fileObjDesr)
			// popis objektu
			MntSerializeString(loadArchive,m_sFileObjectDescription,m_sFileName);

		loadArchive.Close();
		file.Close();
	}
	CATCH_ALL(e)
	{
		file.Abort(); // will not throw an exception
		return 1; // error
	}
	END_CATCH_ALL
	return 0; // OK
};

// nastaven� modifikace
void CMntFileObject::SetModified(BOOL bModif)
{
  m_bModified = bModif;
};

//////////////////////////////////////////////////////////////////////////////
// CMntOptions
IMPLEMENT_DYNAMIC(CMntOptions,  CObject);

#define MNTOPT_FILE_VER		0x0200	// posledn� verze souboru

// constructor
CMntOptions::CMntOptions()
{	m_nStyle = optNone; 
	// parametry v p��pad� ukl�d�n� do souboru
	m_nFlags = fileID | fileVer | filePassword | fileVerifPsw;
	// identifikace souboru
	static TCHAR BASED_CODE szMntOptID[] = _T("MTOPT");
	for(int i = 0; i<MNT_FILE_ID_LEN; i++)
		m_cFileID[i] = szMntOptID[i];
	// verze
	m_nFileVersion   = MNTOPT_FILE_VER;
	// heslo
	//GetAppString(m_sFilePassword,APPSTR_PASSWORD);
  m_sFilePassword = "vorisek";
}


CMntOptions::~CMntOptions()
{
};

void CMntOptions::SetStyle(WORD nStyle,LPCTSTR pRoot,LPCTSTR pDef)
{
	m_nStyle = nStyle; 
	m_sRoot = pRoot;
	m_sDefault = (pDef != NULL)?pDef:pRoot;
	
};

///////////////////////////////////////////////////////////
// CFMntDockState
///////////////////////////////////////////////////////////
const char _afxVisible[] = "Visible";
const char _afxBarSection[] = "%s-Bar%d";
const char _afxSummarySection[] = "%s-Summary";
const char _afxXPos[] = "XPos";
const char _afxYPos[] = "YPos";
const char _afxMRUWidth[] = "MRUWidth";
const char _afxDocking[] = "Docking";
const char _afxMRUDockID[] = "MRUDockID";
const char _afxMRUDockLeftPos[] = "MRUDockLeftPos";
const char _afxMRUDockRightPos[] = "MRUDockRightPos";
const char _afxMRUDockTopPos[] = "MRUDockTopPos";
const char _afxMRUDockBottomPos[] = "MRUDockBottomPos";
const char _afxMRUFloatStyle[] = "MRUFloatStyle";
const char _afxMRUFloatXPos[] = "MRUFloatXPos";
const char _afxMRUFloatYPos[] = "MRUFloatYPos";

const char _afxBarID[] = "BarID";
const char _afxHorz[] = "Horz";
const char _afxFloating[] = "Floating";
const char _afxBars[] = "Bars";
const char _afxScreenCX[] = "ScreenCX";
const char _afxScreenCY[] = "ScreenCY";
const char _afxBar[] = "Bar%d";

#include <afxpriv.h>

class CFMntControlBarInfo : public CControlBarInfo
{
public:
  CFMntControlBarInfo() {};
  CFMntControlBarInfo(const CControlBarInfo& barInfo);

  BOOL LoadState(const char * pszClassName, ParamClassPtr pParentClass, int nIndex, CDockState* pDockState);
  BOOL SaveState(const char * pszClassName, ParamClassPtr pParentClass, int nIndex);
};

CFMntControlBarInfo::CFMntControlBarInfo(const CControlBarInfo& barInfo)
{
  m_nBarID = barInfo.m_nBarID;      // ID of this bar
  m_bVisible = barInfo.m_bVisible;    // visibility of this bar
  m_bFloating = barInfo.m_bFloating;   // whether floating or not
  m_bHorz = barInfo.m_bHorz;       // orientation of floating dockbar
  m_bDockBar = barInfo.m_bDockBar;    // TRUE if a dockbar
  m_pointPos = barInfo.m_pointPos;  // topleft point of window

  m_nMRUWidth = barInfo.m_nMRUWidth;   // MRUWidth for Dynamic Toolbars
  m_bDocking = barInfo.m_bDocking;    // TRUE if this bar has a DockContext
  m_uMRUDockID = barInfo.m_uMRUDockID;  // most recent docked dockbar
  m_rectMRUDockPos = barInfo.m_rectMRUDockPos; // most recent docked position
  m_dwMRUFloatStyle = barInfo.m_dwMRUFloatStyle; // most recent floating orientation
  m_ptMRUFloatPos = barInfo.m_ptMRUFloatPos; // most recent floating position

  m_arrBarID.Copy(barInfo.m_arrBarID);   // bar IDs for bars contained within this one
  m_pBar = barInfo.m_pBar;    // bar which this refers to (transient)
}

#define LOAD_INT(name, value, defvalue) pEntry = pClass->FindEntry(name);\
  if (pEntry.IsNull()) \
  value = defvalue; \
  else \
  value = (int) (*pEntry);

BOOL CFMntControlBarInfo::LoadState(const char * pszClassName, 
                                    ParamClassPtr pParentClass, 
                                    int nIndex, 
                                    CDockState* pDockState)
{
  ASSERT(pDockState != NULL);

  if (pParentClass.IsNull())
    return FALSE;

  ParamEntryPtr pEntry = pParentClass->FindEntry(pszClassName);
  if (pEntry.IsNull() || !pEntry->IsClass())
    return FALSE;

  ParamClassPtr pClass = pEntry->GetClassInterface();

  LOAD_INT(_afxBarID, m_nBarID, 0);
  LOAD_INT(_afxVisible, m_bVisible, TRUE);
  LOAD_INT(_afxHorz, m_bHorz, TRUE);
  LOAD_INT(_afxFloating, m_bFloating, FALSE);
  LOAD_INT(_afxXPos, m_pointPos.x, -1);
  LOAD_INT(_afxYPos, m_pointPos.y, -1);
  
  pDockState->ScalePoint(m_pointPos);
  if (m_bFloating)
  {
    // multi-monitor support only available under Win98/ME/Win2000 or greater
    OSVERSIONINFO vi;
    ZeroMemory(&vi, sizeof(OSVERSIONINFO));
    vi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
    ::GetVersionEx(&vi);
    if ((vi.dwMajorVersion > 4) || (vi.dwMajorVersion == 4 && vi.dwMinorVersion != 0))
    {
      if (m_pointPos.x - GetSystemMetrics(SM_CXFRAME) < GetSystemMetrics(SM_XVIRTUALSCREEN))
        m_pointPos.x = GetSystemMetrics(SM_CXFRAME) + GetSystemMetrics(SM_XVIRTUALSCREEN);
      if (m_pointPos.x + GetSystemMetrics(SM_CXFRAME) > GetSystemMetrics(SM_CXVIRTUALSCREEN))
        m_pointPos.x = - GetSystemMetrics(SM_CXFRAME) + GetSystemMetrics(SM_CXVIRTUALSCREEN);
      if (m_pointPos.y - GetSystemMetrics(SM_CYFRAME) - GetSystemMetrics(SM_CYSMCAPTION) < GetSystemMetrics(SM_YVIRTUALSCREEN))
        m_pointPos.y = GetSystemMetrics(SM_CYFRAME) + GetSystemMetrics(SM_CYSMCAPTION) + GetSystemMetrics(SM_YVIRTUALSCREEN);
      if (m_pointPos.y + GetSystemMetrics(SM_CYFRAME) + GetSystemMetrics(SM_CYSMCAPTION) > GetSystemMetrics(SM_CYVIRTUALSCREEN))
        m_pointPos.y = - GetSystemMetrics(SM_CYFRAME) - GetSystemMetrics(SM_CYSMCAPTION) + GetSystemMetrics(SM_CYVIRTUALSCREEN);

      HMODULE hUser = ::GetModuleHandle(_T("user32.dll"));
      ASSERT(hUser);
      if (hUser != NULL)
      {
        // in case of multiple monitors check if point is in one of monitors
        HMONITOR (WINAPI* pfnMonitorFromPoint)(POINT, DWORD) = NULL;
        (FARPROC&)pfnMonitorFromPoint = ::GetProcAddress(hUser, "MonitorFromPoint");
        if (pfnMonitorFromPoint && !(pfnMonitorFromPoint)(m_pointPos, MONITOR_DEFAULTTONULL))
        {
          m_pointPos.x = GetSystemMetrics(SM_CXFRAME);
          m_pointPos.y = GetSystemMetrics(SM_CYSMCAPTION) + GetSystemMetrics(SM_CYFRAME);
        }
      }
    }
    else
    {
      if (m_pointPos.x - GetSystemMetrics(SM_CXFRAME) < 0)
        m_pointPos.x = GetSystemMetrics(SM_CXFRAME) + 0;
      if (m_pointPos.y - GetSystemMetrics(SM_CYFRAME) - GetSystemMetrics(SM_CYSMCAPTION) < 0)
        m_pointPos.y = GetSystemMetrics(SM_CYFRAME) + GetSystemMetrics(SM_CYSMCAPTION) + 0;
    }
  }


  LOAD_INT(_afxMRUWidth, m_nMRUWidth, 32767);
  LOAD_INT(_afxDocking, m_bDocking, 0);
    
  if (m_bDocking)
  {
    LOAD_INT(_afxMRUDockID, m_uMRUDockID, 0);
    LOAD_INT(_afxMRUDockLeftPos, m_rectMRUDockPos.left, 0);
    LOAD_INT(_afxMRUDockTopPos, m_rectMRUDockPos.top, 0);
    LOAD_INT(_afxMRUDockRightPos, m_rectMRUDockPos.right, 0);
    LOAD_INT(_afxMRUDockBottomPos, m_rectMRUDockPos.bottom, 0);
    pDockState->ScaleRectPos(m_rectMRUDockPos);

    LOAD_INT(_afxMRUFloatStyle, m_dwMRUFloatStyle, 0);
    LOAD_INT(_afxMRUFloatXPos, m_ptMRUFloatPos.x, 0);
    LOAD_INT(_afxMRUFloatYPos, m_ptMRUFloatPos.y, 0);   
    pDockState->ScalePoint(m_ptMRUFloatPos);
  }

  int nBars;
  LOAD_INT(_afxBars, nBars, 0);    
  for (int i=0; i < nBars; i++)
  {
    char buf[16];
    sprintf(buf, _afxBar, i);

    int val;
    LOAD_INT(buf, val, 0);    
    m_arrBarID.Add((void*)(UINT_PTR)val);
  }

  return m_nBarID != 0;
}

BOOL CFMntControlBarInfo::SaveState(const char * pszClassName, 
                                    ParamClassPtr pParentClass, 
                                    int nIndex)
{
  if (m_bDockBar && m_bVisible && !m_bFloating && m_pointPos.x == -1 &&
    m_pointPos.y == -1 && m_arrBarID.GetSize() <= 1)
  {
    return FALSE;
  }

  if (pParentClass.IsNull())
    return FALSE;

  pParentClass->Delete(pszClassName);    

  ParamClassPtr pClass = pParentClass->AddClass(pszClassName);

  pClass->Add(_afxBarID , (int) m_nBarID);
  pClass->Add(_afxVisible, (int) m_bVisible);

  pClass->Add( _afxHorz, m_bHorz);
  pClass->Add( _afxFloating, m_bFloating);

  pClass->Add( _afxXPos, m_pointPos.x);
  pClass->Add( _afxYPos, m_pointPos.y);

  pClass->Add( _afxMRUWidth, (int) m_nMRUWidth);
  pClass->Add( _afxDocking, m_bDocking);
  pClass->Add( _afxMRUDockID, (int) m_uMRUDockID);
  pClass->Add( _afxMRUDockLeftPos, m_rectMRUDockPos.left);
  pClass->Add( _afxMRUDockTopPos, m_rectMRUDockPos.top);
  pClass->Add( _afxMRUDockRightPos, m_rectMRUDockPos.right);
  pClass->Add( _afxMRUDockBottomPos, m_rectMRUDockPos.bottom);
  pClass->Add( _afxMRUFloatStyle, (int) m_dwMRUFloatStyle);
  pClass->Add( _afxMRUFloatXPos, m_ptMRUFloatPos.x);
  pClass->Add( _afxMRUFloatYPos, m_ptMRUFloatPos.y);
 

  pClass->Add( _afxBars, (int)m_arrBarID.GetSize());
  if (m_arrBarID.GetSize() > 1) //if ==1 then still empty
  {
    
    for (int i = 0; i < m_arrBarID.GetSize(); i++)
    {
      char buf[16];
      sprintf(buf, _afxBar, i);
      pClass->Add( buf, (int)(INT_PTR)m_arrBarID[i]);
    }
  }
  return TRUE;
}

IMPLEMENT_DYNAMIC(CFMntDockState,CDockState)

BOOL CFMntDockState::LoadParams(BOOL bReportErr)
{
  Clear();

  if (m_pParentClass.IsNull())
    return FALSE;
 
  ParamEntryPtr pEntry = m_pParentClass->FindEntry(m_cClassName);
  if (pEntry.IsNull() || !pEntry->IsClass())
    return FALSE;
  
  ParamClassPtr pClass = pEntry->GetClassInterface();

  int nBars;
  LOAD_INT(_afxBars, nBars, 0);

  CSize size;
  LOAD_INT(_afxScreenCX, size.cx, 0);
  LOAD_INT(_afxScreenCY, size.cy, 0);  
  SetScreenSize(size);

  for (int i = 0; i < nBars; i++)
  {
    CFMntControlBarInfo* pInfo = new CFMntControlBarInfo;
    m_arrBarInfo.Add(pInfo);

    char buf[128];
    sprintf(buf,"ControlBarInfo%d", i);

    pInfo->LoadState( buf, pClass, i, this);
  }

  return TRUE;
}

BOOL CFMntDockState::SaveParams(BOOL bReportErr)
{
  if (m_pParentClass.IsNull())
    return FALSE;

  m_pParentClass->Delete(m_cClassName);    

  ParamClassPtr pClass = m_pParentClass->AddClass(m_cClassName);

  CSize size = GetScreenSize();
  pClass->Add( _afxScreenCX, size.cx);
  pClass->Add( _afxScreenCY, size.cy);

  int nIndex = 0;
  for (int i = 0;i < m_arrBarInfo.GetSize(); i++)
  {
    CControlBarInfo* pInfo = (CControlBarInfo*)m_arrBarInfo[i];

    CFMntControlBarInfo info(*pInfo);    

    char buf[128];
    sprintf(buf,"ControlBarInfo%d", nIndex);

    if (info.SaveState( buf, pClass, nIndex))
      nIndex++;
  }  
  pClass->Add( _afxBars, nIndex);
  Flush();
  return TRUE;
}


