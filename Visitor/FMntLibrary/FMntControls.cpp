#include "stdAfx.h"
#include "resource.h"
#include "FMntTools.h"
#include ".\fmnttools.h"

/** Visual controls.
	@author Fehy
	*/

//////////////////////////////
/// ComboBox
//////////////////////////////////

BEGIN_MESSAGE_MAP(CFMntComboBox, CComboBox)
END_MESSAGE_MAP();

BOOL CFMntComboBox::GetItemText(CString& text, LPDRAWITEMSTRUCT lpDIS) 
{
    int iItem = lpDIS->itemID;			
		GetLBText(iItem, text);
			
    return TRUE;
};

BOOL CFMntComboBox::GetItemColor(COLORREF& color, LPDRAWITEMSTRUCT lpDIS) 
{
	if (m_iDataType == MNTLST_COLOR)
	{
    color = lpDIS->itemData;
		return TRUE;
	}

	return FALSE;
};

void CFMntComboBox::OnDrawItemText(CRect& rect, LPDRAWITEMSTRUCT lpDIS)
{
    CString text;
    if (GetItemText(text, lpDIS))
    {
        CDC dc;
        dc.Attach(lpDIS->hDC);

        CRect textRect = rect;
        textRect.left += 2;
        ::DrawTextA(dc.m_hDC,text,-1,(LPRECT)textRect,DT_LEFT|DT_NOPREFIX|DT_SINGLELINE|DT_VCENTER|DT_TABSTOP);
        
        //rect.left += dc.GetTextExtent(text);
        dc.Detach();
    }
};

void CFMntComboBox::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	if (((int) lpDIS->itemID ) < 0)
		return;
		
	CRect rect(lpDIS->rcItem);
	
	int nWidth = GetItemImageWidth(lpDIS);
	if (nWidth > 0)
	{
		CRect imageRect;
		imageRect.left = rect.left + 2;
		imageRect.top =  rect.top + 1;
		imageRect.right = imageRect.left + nWidth;
		imageRect.bottom = rect.bottom - 1;
			
		rect.left += nWidth + 4;
			
		OnDrawItemImage(imageRect, lpDIS);
	}
			    
	COLORREF color;
	if (GetItemColor(color, lpDIS))
	{
		CDC dc;
		dc.Attach(lpDIS->hDC);

		CRect colorRect;
		colorRect.left = rect.left + 2;
		colorRect.top =  rect.top + 2;
		colorRect.right = colorRect.left + 30;
		colorRect.bottom = rect.bottom - 4;
			
		rect.left += 35;
			
		CBrush brush;            
		brush.CreateSolidBrush(GetSysColor(COLOR_3DFACE));
			
		dc.FillRect( (LPCRECT) colorRect, &brush);
			
		CBrush brush2;            
		brush2.CreateSolidBrush(GetSysColor(COLOR_3DSHADOW));
		colorRect.left += 1;
		colorRect.top += 1;
		colorRect.right -= 1;
		colorRect.bottom -= 1;
		dc.FillRect( (LPCRECT) colorRect, &brush2);
			
		CBrush brush3;
		brush3.CreateSolidBrush(color);
			
		colorRect.left += 1;
		colorRect.top += 1;
		dc.FillRect( (LPCRECT) colorRect, &brush3);

		dc.Detach();
			
	}
    
	OnDrawItemText(rect, lpDIS);
}

void CFMntComboBox::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	if (m_iDataType == MNTLST_BITMAP)
	{			
		BITMAP bmpInfo;
		if (0 == m_cBitmap.GetBitmap(&bmpInfo))
			return;		

		lpMeasureItemStruct->itemHeight = bmpInfo.bmHeight + 2;
	}
}


void CFMntComboBox::AddStringEx(const CString& str, DWORD Value)
{
    int iItems =  AddString(str);
    if (iItems != LB_ERR)
    {                
        SetItemData(iItems,  Value);           
    }
}

void CFMntComboBox::LoadImageBitmap(LPCTSTR lpBmp, int nPtrns)
{
	m_cBitmap.LoadBitmap(lpBmp);
	m_nPtrnts = nPtrns;
}

int CFMntComboBox::GetItemImageWidth(LPDRAWITEMSTRUCT lpDIS)
{
	if (m_iDataType != MNTLST_BITMAP)
		return 0;

	BITMAP bmpInfo;
  if (0 == m_cBitmap.GetBitmap(&bmpInfo))
		return 0;

	return bmpInfo.bmWidth / m_nPtrnts;
}

void CFMntComboBox::OnDrawItemImage(CRect& rect, LPDRAWITEMSTRUCT lpDIS)
{

	CDC dc;
	dc.Attach(lpDIS->hDC);
	
	// Get the size of the bitmap
	BITMAP bmpInfo;
	m_cBitmap.GetBitmap(&bmpInfo);
	
	// Create an in-memory DC compatible with the
	// display DC we're using to paint
	CDC dcMemory;
	dcMemory.CreateCompatibleDC(&dc);
	
	// Select the bitmap into the in-memory DC
	CBitmap* pOldBitmap = dcMemory.SelectObject(&m_cBitmap);
		
	dc.BitBlt(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, 
				&dcMemory,HIWORD(lpDIS->itemData) * bmpInfo.bmWidth / m_nPtrnts , 0, SRCCOPY);
	
	dcMemory.SelectObject(pOldBitmap);
	dcMemory.DeleteDC();

	dc.Detach();
}

//////////////////////////////
/// ListBox
//////////////////////////////////

BEGIN_MESSAGE_MAP(CFMntListBox, CListBox)
END_MESSAGE_MAP();

BOOL CFMntListBox::GetItemText(CString& text, LPDRAWITEMSTRUCT lpDIS) 
{
    int iItem = lpDIS->itemID;			
		GetText(iItem, text);
			
    return TRUE;
};

BOOL CFMntListBox::GetItemColor(COLORREF& color, LPDRAWITEMSTRUCT lpDIS) 
{
	if (m_iDataType == MNTLST_COLOR)
	{
    color = lpDIS->itemData;
		return TRUE;
	}

	return FALSE;
};

void CFMntListBox::OnDrawItemText(CRect& rect, LPDRAWITEMSTRUCT lpDIS)
{
    CString text;
    if (GetItemText(text, lpDIS))
    {
        CDC dc;
        dc.Attach(lpDIS->hDC);
				//dc.SetBkColor(::GetSysColor(COLOR_HIGHLIGHT));

        CRect textRect = rect;
        textRect.left += 2;
        ::DrawTextA(dc.m_hDC,text,-1,(LPRECT)textRect,DT_LEFT|DT_NOPREFIX|DT_SINGLELINE|DT_VCENTER|DT_TABSTOP);
        
        //rect.left += dc.GetTextExtent(text);
        dc.Detach();
    }
};

void CFMntListBox::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	if (((int) lpDIS->itemID ) < 0)
		return;
		
	CRect rect(lpDIS->rcItem);
	
	int nWidth = GetItemImageWidth(lpDIS);
	if (nWidth > 0)
	{
		CRect imageRect;
		imageRect.left = rect.left + 2;
		imageRect.top =  rect.top + 1;
		imageRect.right = imageRect.left + nWidth;
		imageRect.bottom = rect.bottom - 1;
			
		rect.left += nWidth + 4;
			
		OnDrawItemImage(imageRect, lpDIS);
	}
			    
	COLORREF color;
	if (GetItemColor(color, lpDIS))
	{
		CDC dc;
		dc.Attach(lpDIS->hDC);

		CRect colorRect;
		colorRect.left = rect.left + 2;
		colorRect.top =  rect.top + 2;
		colorRect.right = colorRect.left + 30;
		colorRect.bottom = rect.bottom - 4;
			
		rect.left += 35;
			
		CBrush brush;            
		brush.CreateSolidBrush(GetSysColor(COLOR_3DFACE));
			
		dc.FillRect( (LPCRECT) colorRect, &brush);
			
		CBrush brush2;            
		brush2.CreateSolidBrush(GetSysColor(COLOR_3DSHADOW));
		colorRect.left += 1;
		colorRect.top += 1;
		colorRect.right -= 1;
		colorRect.bottom -= 1;
		dc.FillRect( (LPCRECT) colorRect, &brush2);
			
		CBrush brush3;
		brush3.CreateSolidBrush(color);
			
		colorRect.left += 1;
		colorRect.top += 1;
		dc.FillRect( (LPCRECT) colorRect, &brush3);

		dc.Detach();
			
	}
    

	if (lpDIS->itemState & ODS_SELECTED)
	{
		CDC dc;
		dc.Attach(lpDIS->hDC);
		COLORREF oldbg = dc.GetBkColor();
		COLORREF text = dc.GetTextColor();
		dc.SetTextColor(::GetSysColor(COLOR_HIGHLIGHTTEXT));
    dc.SetBkColor(::GetSysColor(COLOR_HIGHLIGHT));		
		OnDrawItemText(rect, lpDIS);
		dc.SetBkColor(oldbg);
		dc.SetTextColor(text);
		dc.Detach();
	}
	else
		OnDrawItemText(rect, lpDIS);


}

void CFMntListBox::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	if (m_iDataType == MNTLST_BITMAP)
	{			
		BITMAP bmpInfo;
		if (0 == m_cBitmap.GetBitmap(&bmpInfo))
			return;		

		lpMeasureItemStruct->itemHeight = bmpInfo.bmHeight + 2;
	}
}


void CFMntListBox::AddStringEx(const CString& str, DWORD Value)
{
    int iItems =  AddString(str);
    if (iItems != LB_ERR)
    {                
        SetItemData(iItems,  Value);           
    }
}

void CFMntListBox::LoadImageBitmap(LPCTSTR lpBmp, int nPtrns)
{
	m_cBitmap.LoadBitmap(lpBmp);
	m_nPtrnts = nPtrns;
}

int CFMntListBox::GetItemImageWidth(LPDRAWITEMSTRUCT lpDIS)
{
	if (m_iDataType != MNTLST_BITMAP)
		return 0;

	BITMAP bmpInfo;
  if (0 == m_cBitmap.GetBitmap(&bmpInfo))
		return 0;

	return bmpInfo.bmWidth / m_nPtrnts;
}

void CFMntListBox::OnDrawItemImage(CRect& rect, LPDRAWITEMSTRUCT lpDIS)
{

	CDC dc;
	dc.Attach(lpDIS->hDC);
	
	// Get the size of the bitmap
	BITMAP bmpInfo;
	m_cBitmap.GetBitmap(&bmpInfo);
	
	// Create an in-memory DC compatible with the
	// display DC we're using to paint
	CDC dcMemory;
	dcMemory.CreateCompatibleDC(&dc);
	
	// Select the bitmap into the in-memory DC
	CBitmap* pOldBitmap = dcMemory.SelectObject(&m_cBitmap);
		
	dc.BitBlt(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, 
				&dcMemory,HIWORD(lpDIS->itemData) * bmpInfo.bmWidth / m_nPtrnts , 0, SRCCOPY);
	
	dcMemory.SelectObject(pOldBitmap);
	dcMemory.DeleteDC();

	dc.Detach();
}

void CFMntMenuListBox::OnRButtonDown(UINT nFlags, CPoint point)
{
  int useMenu = m_MainMenuID;

  BOOL bOutSide;
  UINT item = ItemFromPoint(point, bOutSide);

  if (!bOutSide)
  {  
    CRect itemRect;
    GetItemRect(item, itemRect);
    if (LB_ERR != GetItemRect(item, itemRect) && itemRect.PtInRect(point))
    {
      SetCurSel(item);
      useMenu = m_ItemMenuID;
    }
  }
  
  CMenu menu;
  if(menu.LoadMenu(useMenu))
  {  
    ClientToScreen(&point);
    CMenu * menuPopUp = menu.GetSubMenu(0);
    menuPopUp->TrackPopupMenu(TPM_LEFTALIGN, point.x, point.y, m_owner);
  }
}

void LBAddString(HWND hWnd, LPCTSTR string, DWORD Value)
{
    int iItems =  SendMessage(hWnd, LB_ADDSTRING, 0, (DWORD) string);
    if (iItems != LB_ERR)
    {                
        SendMessage( hWnd, LB_SETITEMDATA, iItems, (DWORD) Value);           
    }
}

void LBAddStringID(HWND hWnd, int strID, DWORD Value)
{
	CString string;
	if (string.LoadString(strID))
	{
		int iItems =  SendMessage(hWnd, LB_ADDSTRING, 0, (DWORD)(LPCTSTR) string);
    if (iItems != LB_ERR)
    {                
        SendMessage( hWnd, LB_SETITEMDATA, iItems, (DWORD) Value);           
    }

	}
}

void LBDeleteString(HWND hWnd, LPCTSTR string)
{
	 int iItem =  SendMessage(hWnd, LB_FINDSTRINGEXACT, -1, (DWORD) string);
   if (iItem != LB_ERR)
   {                
       SendMessage( hWnd, LB_DELETESTRING, iItem, 0);           
   }
}

void CBAddString(HWND hWnd, LPCTSTR string, DWORD Value )
{
    int iItems =  SendMessage(hWnd, CB_ADDSTRING, 0, (DWORD) string);
    if (iItems != CB_ERR)
    {                
        SendMessage( hWnd, CB_SETITEMDATA, iItems, (DWORD) Value);           
    }
}

void CBAddStringID(HWND hWnd, int strID, DWORD Value )
{
	CString string;
	if (string.LoadString(strID))
	{
		int iItems =  SendMessage(hWnd, CB_ADDSTRING, 0, (DWORD)(LPCTSTR) string);
    if (iItems != LB_ERR)
    {                
        SendMessage( hWnd, CB_SETITEMDATA, iItems, (DWORD) Value);           
    }

	}
}

void LBSetCurSel(HWND hWnd,int Index)
{
	SendMessage(hWnd, LB_SETCURSEL, Index,0);
}

void CBSetCurSel(HWND hWnd,int Index)
{
	SendMessage(hWnd, CB_SETCURSEL, Index,0);
}

void LBSetCurSelData(HWND hWnd,DWORD ItmData)
{
	/*int iIndex = SendMessage( hWnd, LB_GETCURSEL, 0,0);
	if (iIndex != LB_ERR)
  {                
        SendMessage( hWnd,LB_SETITEMDATA, iIndex, ItmData);           
  }*/

  int nItems = SendMessage( hWnd, LB_GETCOUNT, 0,0);
  if (nItems == LB_ERR)
    return;

  for(int i = 0; i < nItems; i++)
  {
    DWORD ItmData2 = SendMessage( hWnd, LB_GETITEMDATA, i, 0);
    if (ItmData2 == ItmData)
    {
      SendMessage(hWnd, LB_SETCURSEL, i,0);
      return;
    }
  }
}

void CBSetCurSelData(HWND hWnd,DWORD ItmData)
{
	/*int iIndex = SendMessage( hWnd, CB_GETCURSEL, 0,0);
	if (iIndex != CB_ERR)
  {                
		SendMessage( hWnd, CB_SETITEMDATA, iIndex, ItmData);           
  }*/

  int nItems = SendMessage( hWnd, CB_GETCOUNT, 0,0);
  if (nItems == LB_ERR)
    return;

  for(int i = 0; i < nItems; i++)
  {
    DWORD ItmData2 = SendMessage( hWnd, CB_GETITEMDATA, i, 0);
    if (ItmData2 == ItmData)
    {
      SendMessage(hWnd, CB_SETCURSEL, i,0);
      return;
    }
  }
}

int LBGetCurSel(HWND hWnd)
{
	return SendMessage( hWnd, LB_GETCURSEL, 0,0);
};

int CBGetCurSel(HWND hWnd)
{
	return SendMessage( hWnd, CB_GETCURSEL, 0,0);
}

BOOL LBGetCurSelData(HWND hWnd,DWORD& ItmData) 
{
	int iIndex = SendMessage( hWnd, LB_GETCURSEL, 0,0);
	if (iIndex != LB_ERR)
  {                
		ItmData = SendMessage( hWnd, LB_GETITEMDATA, iIndex, 0);   
		return ItmData != LB_ERR;
  }

	return FALSE;
}

BOOL CBGetCurSelData(HWND hWnd,DWORD& ItmData) 
{
	int iIndex = SendMessage( hWnd, CB_GETCURSEL, 0,0);
	if (iIndex != CB_ERR)
  {                
		ItmData = SendMessage( hWnd, CB_GETITEMDATA, iIndex, 0);   
		return ItmData != CB_ERR;
  }

	return FALSE;
}

//////////////////////////////////
// Color Button
//////////////////////////////////

BEGIN_MESSAGE_MAP(CFMntColorButton, CButton)
END_MESSAGE_MAP()

BOOL CFMntColorButton::DoEditColor() 
{
	CColorDialog dlg;
	if (IDOK == dlg.DoModal())
	{
		m_cColor = dlg.GetColor(); 
		InvalidateRect(NULL);
		return TRUE;
	}
	return FALSE;
};

void CFMntColorButton::DrawItem(LPDRAWITEMSTRUCT lpDIS) 
{	  
	CRect rect(lpDIS->rcItem);	
	 
  CDC dc;
  dc.Attach(lpDIS->hDC);

  if (lpDIS->itemState & ODS_DEFAULT)
  {

    CBrush brush;            
    brush.CreateSolidBrush(GetSysColor(COLOR_3DDKSHADOW));
    dc.FrameRect(rect,&brush);
  }

	rect.DeflateRect(1,1);

	dc.DrawFrameControl(rect,DFC_BUTTON, DFCS_BUTTONPUSH | 
		(lpDIS->itemState & ODS_SELECTED ? DFCS_PUSHED : 0x0));

	rect.DeflateRect(3,3);

	if (lpDIS->itemState & ODS_SELECTED)
	{
		rect.top += 1;
		rect.left += 1;
	}

	if (lpDIS->itemState & ODS_FOCUS || lpDIS->itemState & ODS_SELECTED)
  {
    dc.DrawFocusRect(rect);
  }

	rect.DeflateRect(2,2);
	
	// Color rectangle
	CRect colorRect;
  colorRect.left = rect.left;
  colorRect.top =  rect.top ;
  colorRect.right = colorRect.left + 30;
  colorRect.bottom = rect.bottom;

	dc.DrawFrameControl(colorRect,DFC_BUTTON, DFCS_BUTTONPUSH | DFCS_PUSHED);

	colorRect.DeflateRect(1,1);

  CBrush brush;
  brush.CreateSolidBrush(m_cColor);  
  dc.FillRect( (LPCRECT) colorRect, &brush); 
    
	rect.left += 37;  	
	rect.bottom += 1;
	rect.top -= 1;

  CString text;
  GetWindowText(text);

  ::DrawTextA(dc.m_hDC,text,-1,(LPRECT)rect,DT_LEFT|DT_SINGLELINE|DT_VCENTER|DT_TABSTOP);
    
  dc.Detach();
};

/////////////////////////////////
// Property Sheet
//////////////////////////////////

CMntPropertySheet::CMntPropertySheet(UINT nIDCaption, DWORD dwAdvFlags /*= 0*/, CWnd* pParentWnd /*= NULL*/, UINT iSelectPage /*= 0*/) 
{
  CPropertySheet::CPropertySheet(nIDCaption, pParentWnd, iSelectPage );
  m_psh.dwFlags |= dwAdvFlags;
};

CMntPropertySheet::CMntPropertySheet(LPCTSTR pszCaption, DWORD dwAdvFlags /*= 0*/, CWnd* pParentWnd /*= NULL*/, UINT iSelectPage /*= 0*/) 
{
  CPropertySheet::CPropertySheet(pszCaption, pParentWnd, iSelectPage );
  m_psh.dwFlags |= dwAdvFlags;
};

void CMntPropertySheet::AddPageIntoWizard(CMntPropertyPage * pPage)
{
	AddPage(pPage);
	pPage->AssignSheet(this);
}

void CMntPropertySheet::OnWizardBack()
{ 
	if ( GetActiveIndex() == GetPageCount() - 12)
	{
		SetWizardButtons(PSWIZB_BACK | PSWIZB_NEXT);
	}
};

void CMntPropertySheet::OnWizardNext()
{
	if ( GetActiveIndex() == GetPageCount() - 2)
	{
		SetWizardButtons(PSWIZB_BACK | PSWIZB_FINISH);
	}
};

/////////////////////////////////
// Property Page
//////////////////////////////////

LRESULT CMntPropertyPage::OnWizardBack()
{
   m_pSheet->OnWizardBack();
	 return CPropertyPage::OnWizardBack();
};

LRESULT CMntPropertyPage::OnWizardNext()
{
   m_pSheet->OnWizardNext();
	 return CPropertyPage::OnWizardNext();
};

//////////////////////////////////
// Get int from editbox.
//////////////////////////////////

BOOL GetEditInt(HWND hWnd, int& value)
{
	CString text;
	(CWnd::FromHandle(hWnd))->GetWindowText(text);
	return 0 == _stscanf(text, _T("%d"), &value) ? false : true;
}

// new code
//**************************
BOOL GetEditFloat(HWND hWnd, float& value)
{
	CString text;
	(CWnd::FromHandle(hWnd))->GetWindowText(text);
	return 0 == _stscanf(text, _T("%f"), &value) ? false : true;
}

///////////////////////////////////////////////
// CMntEditableListCtrl
///////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CInPlaceEdit

CInPlaceEdit::CInPlaceEdit(int iItem, int iSubItem, CString sInitText)
:m_sInitText( sInitText )
{
  m_iItem = iItem;
  m_iSubItem = iSubItem;
  m_bESC = FALSE;
}

CInPlaceEdit::~CInPlaceEdit()
{
}


BEGIN_MESSAGE_MAP(CInPlaceEdit, CEdit)
  //{{AFX_MSG_MAP(CInPlaceEdit)
  ON_WM_KILLFOCUS()
  ON_WM_NCDESTROY()
  ON_WM_CHAR()
  ON_WM_CREATE()
  //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInPlaceEdit message handlers

BOOL CInPlaceEdit::PreTranslateMessage(MSG* pMsg)
{
  if( pMsg->message == WM_KEYDOWN )
  {
    if(pMsg->wParam == VK_RETURN
      || pMsg->wParam == VK_DELETE
      || pMsg->wParam == VK_ESCAPE
      || GetKeyState( VK_CONTROL)
      )
    {
      ::TranslateMessage(pMsg);
      ::DispatchMessage(pMsg);
      return TRUE;		    	// DO NOT process further
    }
  }

  return CEdit::PreTranslateMessage(pMsg);
}


void CInPlaceEdit::OnKillFocus(CWnd* pNewWnd)
{
  CString str;
  GetWindowText(str);  

  CEdit::OnKillFocus(pNewWnd);

  // Send Notification to parent of ListView ctrl
  LV_DISPINFO dispinfo;
  dispinfo.hdr.hwndFrom = GetParent()->m_hWnd;
  dispinfo.hdr.idFrom = GetDlgCtrlID();
  dispinfo.hdr.code = LVN_ENDLABELEDIT;

  dispinfo.item.mask = LVIF_TEXT;
  dispinfo.item.iItem = m_iItem;
  dispinfo.item.iSubItem = m_iSubItem;
  dispinfo.item.pszText = m_bESC ? NULL : LPTSTR((LPCTSTR)str);
  dispinfo.item.cchTextMax = str.GetLength();

  GetParent()->GetParent()->SendMessage( WM_NOTIFY, GetParent()->GetDlgCtrlID(), 
    (LPARAM)&dispinfo );

  DestroyWindow();
}

void CInPlaceEdit::OnNcDestroy()
{
  CEdit::OnNcDestroy();

  delete this;
}


void CInPlaceEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
  if( nChar == VK_ESCAPE || nChar == VK_RETURN)
  {
    if( nChar == VK_ESCAPE )
      m_bESC = TRUE;
    GetParent()->SetFocus();
    return;
  }


  CEdit::OnChar(nChar, nRepCnt, nFlags);

  // Resize edit control if needed

  // Get text extent
  CString str;

  GetWindowText( str );
  CWindowDC dc(this);
  CFont *pFont = GetParent()->GetFont();
  CFont *pFontDC = dc.SelectObject( pFont );
  CSize size = dc.GetTextExtent( str );
  dc.SelectObject( pFontDC );
  size.cx += 5;			   	// add some extra buffer

  // Get client rect
  CRect rect, parentrect;
  GetClientRect( &rect );
  GetParent()->GetClientRect( &parentrect );

  // Transform rect to parent coordinates
  ClientToScreen( &rect );
  GetParent()->ScreenToClient( &rect );

  // Check whether control needs to be resized
  // and whether there is space to grow
  if( size.cx > rect.Width() )
  {
    if( size.cx + rect.left < parentrect.right )
      rect.right = rect.left + size.cx;
    else
      rect.right = parentrect.right;
    MoveWindow( &rect );
  }
}

int CInPlaceEdit::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
  if (CEdit::OnCreate(lpCreateStruct) == -1)
    return -1;

  // Set the proper font
  CFont* font = GetParent()->GetFont();
  SetFont(font);

  SetWindowText( m_sInitText );
  SetFocus();
  SetSel( 0, -1 );
  return 0;
}

/*///////////////////////////////////////////
// CInPlaceEditFloat
CInPlaceEditFloat::CInPlaceEditFloat(int iItem, int iSubItem, CString sInitText) :
CInPlaceEdit(iItem, iSubItem, sInitText)
{  
}

bool CInPlaceEditFloat::Validate(CString& str)
{
  float number;
  int n = _stscanf(str, "%f", &number);
  if (n < str.GetLength())
  {
    AfxMessageBox(IDS_NOT_NUMBER);
    return false;
  }
  return true;
}*/

///////////////////////////////////////////
// CMntEditableListCtrl

// HitTestEx	- Determine the row index and column index for a point
// Returns	- the row index or -1 if point is not over a row
// point	- point to be tested.
// col		- to hold the column index
int CMntEditableListCtrl::HitTestEx(CPoint &point, int & iSubItem)
{
  // Make sure that the ListView is in LVS_REPORT
  ASSERT((GetWindowLong(m_hWnd, GWL_STYLE) & LVS_TYPEMASK) == LVS_REPORT );

  LVHITTESTINFO hitTestInfo;
  hitTestInfo.pt = point;

  int row = CListCtrl::SubItemHitTest(&hitTestInfo);    
  iSubItem = hitTestInfo.iSubItem;

  return row;
}

// EditSubLabel		- Start edit of a sub item label
// Returns		- Temporary pointer to the new edit control
// nItem		- The row index of the item to edit
// nCol			- The column of the sub item.
CEdit* CMntEditableListCtrl::EditSubLabel( int nItem, int iSubItem )
{
  // The returned pointer should not be saved

  // Make sure that the item is visible
  if( !EnsureVisible( nItem, TRUE ) ) return NULL;
 
  CRect rect;
  GetSubItemRect( nItem, iSubItem, LVIR_BOUNDS, rect);

  // Now scroll if we need to expose the column
  CRect rcClient;
  GetClientRect( &rcClient );
  if( rect.left < 0 || rect.left > rcClient.right )
  {
    CSize size;
    size.cx = rect.left;
    size.cy = 0;
    Scroll( size );    
  }

  GetSubItemRect( nItem, iSubItem, LVIR_BOUNDS, rect);

  // Get Column alignment
  LV_COLUMN lvcol;
  lvcol.mask = LVCF_FMT;
  GetColumn( iSubItem, &lvcol );
  DWORD dwStyle ;
  if((lvcol.fmt&LVCFMT_JUSTIFYMASK) == LVCFMT_LEFT)
    dwStyle = ES_LEFT;
  else if((lvcol.fmt&LVCFMT_JUSTIFYMASK) == LVCFMT_RIGHT)
    dwStyle = ES_RIGHT;
  else dwStyle = ES_CENTER;  

  dwStyle |= WS_BORDER|WS_CHILD|WS_VISIBLE|ES_AUTOHSCROLL;
  CEdit *pEdit = ConstructInPlaceEdit(nItem, iSubItem, GetItemText( nItem, iSubItem ));
  pEdit->Create( dwStyle, rect, this, IDC_IPEDIT );
  return pEdit;
}

void CMntEditableListCtrl::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
  if( GetFocus() != this ) SetFocus();
  CListCtrl::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CMntEditableListCtrl::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
  if( GetFocus() != this ) SetFocus();
  CListCtrl::OnVScroll(nSBCode, nPos, pScrollBar);
}

BEGIN_MESSAGE_MAP(CMntEditableListCtrl, CListCtrl)
  ON_WM_LBUTTONDBLCLK()
  ON_NOTIFY_REFLECT(LVN_ENDLABELEDIT, OnLvnEndlabeledit)
  ON_WM_HSCROLL()
  ON_WM_VSCROLL()
END_MESSAGE_MAP()

void CMntEditableListCtrl::OnLButtonDblClk(UINT nFlags, CPoint point)
{
  int iSubItem;
  int iItem = HitTestEx(point, iSubItem);

  if (iItem < 0 || !CanEditSubItem(iItem, iSubItem))
    return;

  EditSubLabel( iItem, iSubItem );
}

void CMntEditableListCtrl::OnLvnEndlabeledit(NMHDR *pNMHDR, LRESULT *pResult)
{
  LV_DISPINFO *plvDispInfo = (LV_DISPINFO *)pNMHDR;
  LV_ITEM	*plvItem = &plvDispInfo->item;

  if (plvItem->pszText != NULL)
  {
    SetItemText(plvItem->iItem, plvItem->iSubItem, plvItem->pszText);
  }
  *pResult = FALSE;  
}

