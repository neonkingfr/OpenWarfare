#include "stdAfx.h"
#include "resource.h"
#include "FMntTools.h"


#define MAX_SER_STR_LENGTH	0x1000	// v�t�� �et�zce nepodporuji
void MntSerializeString(CArchive& ar, CString& str, LPCTSTR pFileName)
{
	if (ar.IsStoring())
	{ // save
		DWORD nLen = (DWORD)(str.GetLength()+1);
		ASSERT( nLen <= MAX_SER_STR_LENGTH);
		ar.Write(&nLen,sizeof(DWORD));
		ar.Write(str.GetBuffer(0),(UINT)nLen);
	} else
	{ // load
		DWORD nLen;
		if (ar.Read(&nLen,sizeof(DWORD))!=sizeof(DWORD))
			MntInvalidFileFormat(pFileName);
		if (nLen > MAX_SER_STR_LENGTH)
			MntInvalidFileFormat(pFileName);
		LPSTR pBuff = str.GetBuffer((int)nLen);
		if (pBuff==NULL) AfxThrowMemoryException();
		if (ar.Read(pBuff,(UINT)nLen)!=((UINT)nLen))
			MntInvalidFileFormat(pFileName);
		str.ReleaseBuffer();
	}
};

void MntSerializeRect(CArchive& ar, CRect& rect, LPCTSTR pFileName)
{
	if (ar.IsStoring())
	{ // save
		DWORD pom;
		pom = (DWORD)(rect.left);
		ar.Write(&pom,sizeof(DWORD));
		pom = (DWORD)(rect.top);
		ar.Write(&pom,sizeof(DWORD));
		pom = (DWORD)(rect.right);
		ar.Write(&pom,sizeof(DWORD));
		pom = (DWORD)(rect.bottom);
		ar.Write(&pom,sizeof(DWORD));
	} else
	{ // load
		DWORD pom;
		if (ar.Read(&pom,sizeof(DWORD))!=sizeof(DWORD))
			MntInvalidFileFormat(pFileName);
		rect.left = (int)(pom);
		if (ar.Read(&pom,sizeof(DWORD))!=sizeof(DWORD))
			MntInvalidFileFormat(pFileName);
		rect.top = (int)(pom);
		if (ar.Read(&pom,sizeof(DWORD))!=sizeof(DWORD))
			MntInvalidFileFormat(pFileName);
		rect.right = (int)(pom);
		if (ar.Read(&pom,sizeof(DWORD))!=sizeof(DWORD))
			MntInvalidFileFormat(pFileName);
		rect.bottom = (int)(pom);
	}
};

void MntSerializePoint(CArchive& ar, CPoint& pt, LPCTSTR pFileName)
{
	if (ar.IsStoring())
	{ // save
		DWORD pom;
		pom = (DWORD)(pt.x);
		ar.Write(&pom,sizeof(DWORD));
		pom = (DWORD)(pt.y);
		ar.Write(&pom,sizeof(DWORD));
	} else
	{ // load
		DWORD pom;
		if (ar.Read(&pom,sizeof(DWORD))!=sizeof(DWORD))
			MntInvalidFileFormat(pFileName);
		pt.x = (int)(pom);
		if (ar.Read(&pom,sizeof(DWORD))!=sizeof(DWORD))
			MntInvalidFileFormat(pFileName);
		pt.y = (int)(pom);
	}
};

void MntSerializeChar(CArchive& ar, TCHAR& cChar, LPCTSTR pFileName)
{
	if (ar.IsStoring())
	{ // save
		WORD pom = (WORD)(cChar);
		ar.Write(&pom,sizeof(WORD));
	} else
	{ // load
		WORD pom;
		if (ar.Read(&pom,sizeof(WORD))!=sizeof(WORD))
			MntInvalidFileFormat(pFileName);
		cChar = (TCHAR)pom;
	}
};

void MntSerializeBOOL(CArchive& ar, BOOL& bVal, LPCTSTR pFileName)
{
	if (ar.IsStoring())
	{ // save
		WORD pom = (WORD)(bVal);
		ar.Write(&pom,sizeof(WORD));
	} else
	{ // load
		WORD pom;
		if (ar.Read(&pom,sizeof(WORD))!=sizeof(WORD))
			MntInvalidFileFormat(pFileName);
		bVal = (BOOL)pom;
	}
};

void MntSerializeBool(CArchive& ar, bool& bVal, LPCTSTR pFileName)
{
  if (ar.IsStoring())
  { // save
    WORD pom = bVal ? 1 : 0;
    ar.Write(&pom,sizeof(WORD));
  } else
  { // load
    WORD pom;
    if (ar.Read(&pom,sizeof(WORD))!=sizeof(WORD))
      MntInvalidFileFormat(pFileName);

    bVal = pom > 0;
  }
};

void MntSerializeByte(CArchive& ar, BYTE& value, LPCTSTR pFileName)
{
	if (ar.IsStoring())
	{ // save
		WORD pom = (WORD)(value);
		ar.Write(&pom,sizeof(WORD));
	} else
	{ // load
		WORD pom;
		if (ar.Read(&pom,sizeof(WORD))!=sizeof(WORD))
			MntInvalidFileFormat(pFileName);
		value = (BYTE)pom;
	}
}

void MntSerializeInt(CArchive& ar, int& value, LPCTSTR pFileName)
{
	if (ar.IsStoring())
	{ // save
		DWORD pom = (DWORD)(value);
		ar.Write(&pom,sizeof(DWORD));
	} else
	{ // load
		DWORD pom;
		if (ar.Read(&pom,sizeof(DWORD))!=sizeof(DWORD))
			MntInvalidFileFormat(pFileName);
		value = (int)pom;
	}
};

void MntSerializeLong(CArchive& ar, LONG& value, LPCTSTR pFileName)
{
	if (ar.IsStoring())
	{ // save
		DWORD pom = (DWORD)(value);
		ar.Write(&pom,sizeof(DWORD));
	} else
	{ // load
		DWORD pom;
		if (ar.Read(&pom,sizeof(DWORD))!=sizeof(DWORD))
			MntInvalidFileFormat(pFileName);
		value = (LONG)pom;
	}
};

void MntSerializeWord(CArchive& ar, WORD& value, LPCTSTR pFileName)
{
	if (ar.IsStoring())
	{ // save
		ar.Write(&value,sizeof(WORD));
	} else
	{ // load
		if (ar.Read(&value,sizeof(WORD))!=sizeof(WORD))
			MntInvalidFileFormat(pFileName);
	}
};

void MntSerializeDWord(CArchive& ar, DWORD& value, LPCTSTR pFileName)
{
	if (ar.IsStoring())
	{ // save
		ar.Write(&value,sizeof(DWORD));
	} else
	{ // load
		if (ar.Read(&value,sizeof(DWORD))!=sizeof(DWORD))
			MntInvalidFileFormat(pFileName);
	}
};

void MntSerializeFloat(CArchive& ar, float& value, LPCTSTR pFileName)
{
	if (ar.IsStoring())
	{ // save
		ar.Write(&value,sizeof(float));
	} else
	{ // load
		if (ar.Read(&value,sizeof(float))!=sizeof(float))
			MntInvalidFileFormat(pFileName);
	}
};

void MntSerializeDouble(CArchive& ar, double& value, LPCTSTR pFileName)
{
	if (ar.IsStoring())
	{ // save
		ar.Write(&value,sizeof(double));
	} else
	{ // load
		if (ar.Read(&value,sizeof(double))!=sizeof(double))
			MntInvalidFileFormat(pFileName);
	}
};

void MntSerializeColor(CArchive& ar, COLORREF& col, LPCTSTR pFileName)
{
	if (ar.IsStoring())
	{
		DWORD dwColor = (DWORD)col;
		MntSerializeDWord(ar,dwColor);
	} else
	{
		DWORD dwColor;
		MntSerializeDWord(ar,dwColor);
		col = (COLORREF)dwColor;
	};
};

void MntSerializeFont(CArchive& ar, LOGFONT& font, LPCTSTR pFileName)
{
	MntSerializeLong(ar, font.lfHeight); 
	MntSerializeLong(ar, font.lfWidth); 
	MntSerializeLong(ar, font.lfEscapement); 
	MntSerializeLong(ar, font.lfOrientation); 
	MntSerializeLong(ar, font.lfWeight); 
	MntSerializeByte(ar, font.lfItalic); 
	MntSerializeByte(ar, font.lfUnderline); 
	MntSerializeByte(ar, font.lfStrikeOut); 
	MntSerializeByte(ar, font.lfCharSet); 
	MntSerializeByte(ar, font.lfOutPrecision); 
	MntSerializeByte(ar, font.lfClipPrecision); 
	MntSerializeByte(ar, font.lfQuality); 
	MntSerializeByte(ar, font.lfPitchAndFamily); 
	if (ar.IsStoring())
	{ // save
		CString sName;
		sName = font.lfFaceName;
		MntSerializeString(ar, sName);
	} else
	{ // load
		CString sName;
		MntSerializeString(ar, sName);
		lstrcpyn(font.lfFaceName,sName,LF_FACESIZE);
	}
};

void MntSerializeFltRect(CArchive& ar, FltRect& rect, LPCTSTR pFileName)
{
  MntSerializeFloat(ar,rect.left);
  MntSerializeFloat(ar,rect.top);
  MntSerializeFloat(ar,rect.right);
  MntSerializeFloat(ar,rect.bottom);
}