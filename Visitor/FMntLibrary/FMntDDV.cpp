#include "stdAfx.h"
#include "resource.h"
#include "FMntTools.h"

void MDDX_Text(CDataExchange* pDX, int nIDC, int& value, BOOL bLocal) {DDX_Text(pDX, nIDC, value);};
void MDDX_Text(CDataExchange* pDX, int nIDC, long& value, BOOL bLocal) {DDX_Text(pDX, nIDC, value);};
//void MDDX_Text(CDataExchange* pDX, int nIDC, WORD& value, BOOL bLocal) {DebugTrap();};
void MDDX_Text(CDataExchange* pDX, int nIDC, DWORD& value, BOOL bLocal) {DDX_Text(pDX, nIDC, value);};
void MDDX_Text(CDataExchange* pDX, int nIDC, float& value, BOOL bLocal) {DDX_Text(pDX, nIDC, value);};
void MDDX_Text(CDataExchange* pDX, int nIDC, double& value, BOOL bLocal) {DDX_Text(pDX, nIDC, value);};
void MDDX_Text(CDataExchange* pDX, int nIDC, CString& value) {DDX_Text(pDX, nIDC, value);};
//void MDDX_Text(CDataExchange* pDX, int nIDC, LPSTR value, int nMaxLen) {DebugTrap();};

class CSpinButtonCtrlRef : public RefCount, public CSpinButtonCtrl
{};

typedef struct 
{
	int nIDC;
	HWND hDialog;
} TIDCIDENT;

class SpinItem
{
public:
	TIDCIDENT m_tIDCIdent;
	Ref<CSpinButtonCtrlRef> m_pSpin;

	SpinItem() : m_pSpin(NULL) {};
	~SpinItem() { m_pSpin.Free();};

	ClassIsBinary(SpinItem);
};

struct TSPINMAPFindArrayKeyTraits
{
	typedef const TIDCIDENT& KeyType;
	static bool IsEqual(KeyType a, KeyType b)
	{
		return a.nIDC == b.nIDC  && a.hDialog == b.hDialog;
	}
	static KeyType GetKey(const SpinItem &a) {return a.m_tIDCIdent;}
};

FindArrayKey<SpinItem,TSPINMAPFindArrayKeyTraits> g_Spins;
AutoArray<int> g_DeleteSpins;


template <class Type>
class CFmtSpinButtonCtrl : public CSpinButtonCtrlRef
{
protected:
	Type m_Min, m_Max, m_Step;
	int m_Round;
	typedef CSpinButtonCtrl base;

	void UpdateString(CString& strValue, int iSteps);

	Type GetValueFromString(const CString& strValue);
	void SetValueToString(CString& strValue, Type Value);
	Type RoundValue(Type Value);

	virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam );

public:

		void SetRangeAndStep(Type min, Type max, Type step, int round) 
		{  
			m_Min = min; m_Max = max; m_Step = step;
			m_Round = round;
			SetRange(-100,100);
			SetPos(0);
		};

	
		virtual BOOL OnChildNotify( UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult );	
};

template<class Type>
void CFmtSpinButtonCtrl<Type>::UpdateString(CString& strValue, int iSteps)
{
	Type Value = GetValueFromString(strValue);;
	
	Value += iSteps * m_Step;

	Value = RoundValue(Value);

	if (Value > m_Max)
		Value = m_Max;

	if (Value < m_Min)
		Value = m_Min;

	SetValueToString(strValue, Value);
	
}

template<class Type>
BOOL CFmtSpinButtonCtrl<Type>::OnChildNotify( UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult )
{	
		if (message == WM_NOTIFY && ((NMHDR *)lParam)->code == UDN_DELTAPOS)
		{
			NMUPDOWN * pData = (NMUPDOWN *) lParam;
			CWnd * pBuddy = GetBuddy();

			CString cBuddyText;
			pBuddy->GetWindowText(cBuddyText);
			UpdateString(cBuddyText,pData->iDelta);
			pBuddy->SetWindowText(cBuddyText);			

			SetPos(0);
			return 1;
		}

		return 0;
}

#define MY_CONTROLS_OFFSET 55000
template<class Type>
LRESULT CFmtSpinButtonCtrl<Type>::WindowProc( UINT message, WPARAM wParam, LPARAM lParam )
{
	// Set up CSpin objects to delete
	if (message == WM_DESTROY)
	{
		CWnd * pParent = GetParent();
		ASSERT(pParent != NULL);

		TIDCIDENT tCtrlID;
		tCtrlID.nIDC = GetDlgCtrlID() - MY_CONTROLS_OFFSET;
		tCtrlID.hDialog = pParent->m_hWnd;

		int iIndex = g_Spins.FindKey(tCtrlID);
		if (iIndex >= 0)
		{
			g_DeleteSpins.Add(iIndex);
		}
	}

	return base::WindowProc( message, wParam, lParam );
}

template<class Type>
Type CFmtSpinButtonCtrl<Type>::GetValueFromString(const CString& strValue)
{
	return (Type) 0;
}

template<class Type>
void CFmtSpinButtonCtrl<Type>::SetValueToString(CString& strValue, Type Value)
{
	return;
}

template<class Type>
Type CFmtSpinButtonCtrl<Type>::RoundValue(Type Value)
{
	return 0;
}

template<>
int CFmtSpinButtonCtrl<int>::GetValueFromString(const CString& strValue)
{
  int iValue;
	_stscanf(strValue,_T("%d"), &iValue);
	return iValue;
}

template<>
void CFmtSpinButtonCtrl<int>::SetValueToString(CString& strValue, int iValue)
{
	strValue.Format(_T("%d"), iValue);
	return;
}

template<>
int CFmtSpinButtonCtrl<int>::RoundValue(int Value)
{
	return SpecRound(Value, m_Round);
}

template<>
long CFmtSpinButtonCtrl<long>::GetValueFromString(const CString& strValue)
{
  long iValue;
	_stscanf(strValue,_T("%ld"), &iValue);
	return iValue;
}

template<>
void CFmtSpinButtonCtrl<long>::SetValueToString(CString& strValue, long iValue)
{
	strValue.Format(_T("%ld"), iValue);
	return;
}

template<>
long CFmtSpinButtonCtrl<long>::RoundValue(long Value)
{
	return SpecRound(Value, m_Round);
}

template<>
DWORD CFmtSpinButtonCtrl<DWORD>::GetValueFromString(const CString& strValue)
{
  DWORD iValue;
	_stscanf(strValue,_T("%lu"), &iValue);
	return iValue;
}

template<>
void CFmtSpinButtonCtrl<DWORD>::SetValueToString(CString& strValue, DWORD iValue)
{
	strValue.Format(_T("%lu"), iValue);
	return;
}

template<>
DWORD CFmtSpinButtonCtrl<DWORD>::RoundValue(DWORD Value)
{
	return SpecRound((long) Value, m_Round);
}

template<>
float CFmtSpinButtonCtrl<float>::GetValueFromString(const CString& strValue)
{
  float Value;
	_stscanf(strValue,_T("%f"), &Value);
	return Value;
}

template<>
void CFmtSpinButtonCtrl<float>::SetValueToString(CString& strValue, float Value)
{
	strValue.Format(_T("%-.2f"), Value);
	return;
}

template<>
float CFmtSpinButtonCtrl<float>::RoundValue(float Value)
{
	return (float) SpecRound(Value, m_Round);
}

template<>
double CFmtSpinButtonCtrl<double>::GetValueFromString(const CString& strValue)
{
  double Value;
	_stscanf(strValue,_T("%lf"), &Value);
	return Value;
}

template<>
void CFmtSpinButtonCtrl<double>::SetValueToString(CString& strValue, double Value)
{
	strValue.Format(_T("%-.3lf"), Value);
	return;
}

template<>
double CFmtSpinButtonCtrl<double>::RoundValue(double Value)
{
	return SpecRound(Value, m_Round);
}

typedef CFmtSpinButtonCtrl<int> CFmtSpinButtonCtrlInt;
typedef CFmtSpinButtonCtrl<long> CFmtSpinButtonCtrlLong;
typedef CFmtSpinButtonCtrl<DWORD> CFmtSpinButtonCtrlDWORD;
typedef CFmtSpinButtonCtrl<float> CFmtSpinButtonCtrlFloat;
typedef CFmtSpinButtonCtrl<double> CFmtSpinButtonCtrlDouble;

void DeleteOldSpins()
{
	if (g_DeleteSpins.Size() == 0)
		return;

	for(int i = 0; i < g_DeleteSpins.Size(); i++)
	{
		//delete item in g_Spins;
		int iIndex = g_DeleteSpins[i];		
		g_Spins.DeleteAt(iIndex);

		// index has moved recalculate them
		for(int j = i + 1; j < g_DeleteSpins.Size(); j++) 
		{
			if (g_DeleteSpins[j] > iIndex)
			{
				g_DeleteSpins[j]--;
			}
		}		
	}

	g_DeleteSpins.Resize(0);
}

/* MDDV_* functions create also SpinButtons. The SpinButtons are attached to editboxes, when dialog is destroyed 
SpinButtons recieve WM_DESTROY function and they write it self to delete list and later they are deleted. */ 

void MDDV_MinMaxInt(CDataExchange* pDX, int value, int minVal, int maxVal, int nIDC , int step , int round )
{

	DeleteOldSpins();

	TIDCIDENT tTempKey;
	tTempKey.nIDC = nIDC;
	tTempKey.hDialog = pDX->m_pDlgWnd->m_hWnd;

	int iIndex = g_Spins.FindKey(tTempKey);
	if (iIndex < 0 )
	{
		// Create spin
		SpinItem& tSpin = g_Spins.Append();
		tSpin.m_tIDCIdent = tTempKey;

		CFmtSpinButtonCtrlInt* pSpin = new CFmtSpinButtonCtrlInt;
		tSpin.m_pSpin = pSpin;

		CRect rect(0, 0, 20, 20);		
		pSpin->Create(WS_VISIBLE | UDS_ALIGNRIGHT, rect, pDX->m_pDlgWnd, MY_CONTROLS_OFFSET + nIDC);
		pSpin->SetRangeAndStep(minVal, maxVal, step, round);
		pSpin->SetBuddy(pDX->m_pDlgWnd->GetDlgItem(nIDC));				
	}

	DDV_MinMaxInt(pDX, value, minVal, maxVal);
}
	
void MDDV_MinMaxLong(CDataExchange* pDX, long value, long minVal, long maxVal, int nIDC, long step, int round) 
{
	DeleteOldSpins();

	TIDCIDENT tTempKey;
	tTempKey.nIDC = nIDC;
	tTempKey.hDialog = pDX->m_pDlgWnd->m_hWnd;

	int iIndex = g_Spins.FindKey(tTempKey);
	if (iIndex < 0 )
	{
		// Create spin
		SpinItem& tSpin = g_Spins.Append();
		tSpin.m_tIDCIdent = tTempKey;

		CFmtSpinButtonCtrlLong * pSpin = new CFmtSpinButtonCtrlLong;
		tSpin.m_pSpin = pSpin;

		CRect rect(0,0,20,20);		
		pSpin->Create(WS_VISIBLE | UDS_ALIGNRIGHT ,rect, pDX->m_pDlgWnd, MY_CONTROLS_OFFSET + nIDC);
		pSpin->SetRangeAndStep(minVal,maxVal,step, round);
		pSpin->SetBuddy(pDX->m_pDlgWnd->GetDlgItem(nIDC));				
	}

  DDV_MinMaxLong(pDX, value, minVal, maxVal);  
};

inline void MDDV_MinMaxWord(CDataExchange* pDX, WORD value, WORD minVal, WORD maxVal, int nIDC, WORD step, int round )
{
	DeleteOldSpins();

	TIDCIDENT tTempKey;
	tTempKey.nIDC = nIDC;
	tTempKey.hDialog = pDX->m_pDlgWnd->m_hWnd;

	int iIndex = g_Spins.FindKey(tTempKey);
	if (iIndex < 0 )
	{
		// Create spin
		SpinItem& tSpin = g_Spins.Append();
		tSpin.m_tIDCIdent = tTempKey;

		CFmtSpinButtonCtrlInt * pSpin = new CFmtSpinButtonCtrlInt;
		tSpin.m_pSpin = pSpin;

		CRect rect(0,0,20,20);		
		pSpin->Create(WS_VISIBLE | UDS_ALIGNRIGHT ,rect, pDX->m_pDlgWnd, MY_CONTROLS_OFFSET + nIDC);
		pSpin->SetRangeAndStep(minVal,maxVal,step, round);
		pSpin->SetBuddy(pDX->m_pDlgWnd->GetDlgItem(nIDC));				
	}

  DDV_MinMaxUInt(pDX, value, minVal, maxVal);
}


void MDDV_MinMaxDWord(CDataExchange* pDX, DWORD value, DWORD minVal, DWORD maxVal, int nIDC, DWORD step, int round) 
{
	DeleteOldSpins();

	TIDCIDENT tTempKey;
	tTempKey.nIDC = nIDC;
	tTempKey.hDialog = pDX->m_pDlgWnd->m_hWnd;

	int iIndex = g_Spins.FindKey(tTempKey);
	if (iIndex < 0 )
	{
		// Create spin
		SpinItem& tSpin = g_Spins.Append();
		tSpin.m_tIDCIdent = tTempKey;

		CFmtSpinButtonCtrlDWORD * pSpin = new CFmtSpinButtonCtrlDWORD;
		tSpin.m_pSpin = pSpin;

		CRect rect(0,0,20,20);		
		pSpin->Create(WS_VISIBLE | UDS_ALIGNRIGHT ,rect, pDX->m_pDlgWnd, MY_CONTROLS_OFFSET + nIDC);
		pSpin->SetRangeAndStep(minVal,maxVal,step, round);
		pSpin->SetBuddy(pDX->m_pDlgWnd->GetDlgItem(nIDC));				
	}

  DDV_MinMaxDWord(pDX, value, minVal, maxVal);  
};

void MDDV_MinMaxFloat(CDataExchange* pDX, float const& value, float minVal, float maxVal, int nIDC, float step, int round) 
{
	DeleteOldSpins();

	TIDCIDENT tTempKey;
	tTempKey.nIDC = nIDC;
	tTempKey.hDialog = pDX->m_pDlgWnd->m_hWnd;

	int iIndex = g_Spins.FindKey(tTempKey);
	if (iIndex < 0 )
	{
		// Create spin
		SpinItem& tSpin = g_Spins.Append();
		tSpin.m_tIDCIdent = tTempKey;

		CFmtSpinButtonCtrlFloat* pSpin = new CFmtSpinButtonCtrlFloat;
		tSpin.m_pSpin = pSpin;

		CRect rect(0, 0, 20, 20);		
		pSpin->Create(WS_VISIBLE | UDS_ALIGNRIGHT, rect, pDX->m_pDlgWnd, MY_CONTROLS_OFFSET + nIDC);
		pSpin->SetRangeAndStep(minVal, maxVal, step, round);
		pSpin->SetBuddy(pDX->m_pDlgWnd->GetDlgItem(nIDC));				
	}

	DDV_MinMaxFloat(pDX, value, minVal, maxVal); 
}

void MDDV_MinMaxDouble(CDataExchange* pDX, double const& value, double minVal, double maxVal, int nIDC, double step, int round) 
{
	DeleteOldSpins();

	TIDCIDENT tTempKey;
	tTempKey.nIDC = nIDC;
	tTempKey.hDialog = pDX->m_pDlgWnd->m_hWnd;

	int iIndex = g_Spins.FindKey(tTempKey);
	if (iIndex < 0 )
	{
		// Create spin
		SpinItem& tSpin = g_Spins.Append();
		tSpin.m_tIDCIdent = tTempKey;

		CFmtSpinButtonCtrlDouble* pSpin = new CFmtSpinButtonCtrlDouble;
		tSpin.m_pSpin = pSpin;

		CRect rect(0, 0, 20, 20);		
		pSpin->Create(WS_VISIBLE | UDS_ALIGNRIGHT, rect, pDX->m_pDlgWnd, MY_CONTROLS_OFFSET + nIDC);
		pSpin->SetRangeAndStep(minVal, maxVal, step, round);
		pSpin->SetBuddy(pDX->m_pDlgWnd->GetDlgItem(nIDC));				
	}

	DDV_MinMaxDouble(pDX, value, minVal, maxVal); 
}

void MDDV_MaxChars(CDataExchange* pDX, LPCTSTR value, int nChars, int nIDC, BOOL setEdit) 
{
  DDV_MaxChars(pDX, value, nChars);
};

void MDDV_MinChars(CDataExchange* pDX, LPCTSTR value, int nChars, int nIDS) 
{  
	if (_tcsclen(value) < (unsigned int) nChars)
	{
		if (!pDX->m_bSaveAndValidate)
		{
			TRACE0("Warning: initial dialog data is out of range.\n");
			return;     // don't stop now
		}		
		CString prompt;
		if (nIDS > 0)
		{
			prompt.Format(nIDS, nChars);
		}
		else
		{
			prompt.Format(_T("String must be minimaly %d chars long."), nChars);
		}		
		AfxMessageBox(prompt, MB_ICONEXCLAMATION, 0);
		prompt.Empty(); // exception prep

#ifdef Fail
#undef Fail
#endif

		pDX->Fail();
	}  
};

void MDDV_LBValidSelItem(CDataExchange* pDX, int nIDC, int strID/* = -1*/)
{
	if (!pDX->m_bSaveAndValidate)
		return;

	if (pDX->m_pDlgWnd != NULL)
	{
		CWnd * pListBox = pDX->m_pDlgWnd->GetDlgItem(nIDC);
		if (pListBox != NULL)
		{
			int iIndex = pListBox->SendMessage(LB_GETCURSEL, 0,0);
			if (iIndex != LB_ERR)
				return;
			
			CString prompt;
			if (strID > 0)
			{
				prompt.LoadString(strID);
			}
			else
			{
				prompt= _T("Item in list box must be selected.");
			}		
			AfxMessageBox(prompt, MB_ICONEXCLAMATION, 0);
			prompt.Empty(); // exception prep
		}
	}

#ifdef Fail
#undef Fail
#endif

	pDX->Fail(); 
};

void MDDV_CBValidSelItem(CDataExchange* pDX, int nIDC, int strID/* = -1*/)
{
	if (!pDX->m_bSaveAndValidate)
		return;

	if (pDX->m_pDlgWnd != NULL)
	{
		CWnd * pComboBox = pDX->m_pDlgWnd->GetDlgItem(nIDC);
		if (pComboBox != NULL)
		{
			int iIndex = pComboBox->SendMessage(CB_GETCURSEL, 0,0);
			if (iIndex != CB_ERR)
				return;
			
			CString prompt;
			if (strID > 0)
			{
				prompt.LoadString(strID);
			}
			else
			{
				prompt= _T("Item in combo box must be selected.");
			}		
			AfxMessageBox(prompt, MB_ICONEXCLAMATION, 0);
			prompt.Empty(); // exception prep
		}
	}

#ifdef Fail
#undef Fail
#endif

	pDX->Fail(); 

};

void MDDV_FileExist(CDataExchange* pDX, LPCTSTR pFile )
{
	if (!pDX->m_bSaveAndValidate)
		return;
	
	CFileFind cFind;

	if (!cFind.FindFile(pFile))
	{
		CString prompt;
		
		prompt.Format(_T("File %s does not exist."), pFile);		
		AfxMessageBox(prompt, MB_ICONEXCLAMATION, 0);
		prompt.Empty(); // exception prep

#ifdef Fail
#undef Fail
#endif
		pDX->Fail();
	}
}

