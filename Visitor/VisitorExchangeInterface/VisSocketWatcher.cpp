#include <stdio.h>
#include <process.h>
#define WIN32_LEAN_AND_MEAN
#include <Es/Common/win.h>
#include <el/TCPIpBasics/Socket.h>
#include "VisSocketWatcher.h"

struct SocketInfo
{
  const Socket &sock;
  ISocketWatcherEvent *event;
  HANDLE blocker;

  SocketInfo(const Socket &sock, ISocketWatcherEvent *event):sock(sock),event(event),blocker(CreateEvent(0,0,0,0)) {}
  ~SocketInfo() {CloseHandle(blocker);}
};

static void WatchDataThread(SocketInfo *nfo)
{
  ISocketWatcherEvent *event=nfo->event;
  Socket sock=nfo->sock;
  SetEvent(nfo->blocker);

  bool exit=false;
  do
  {
    int res=sock.Wait(INFINITE,sock.WaitRead|sock.WaitExcept);   
    if (res==sock.WaitError || res & sock.WaitExcept || !sock) exit=true;
    else
    {
      exit=false;
      if (res==sock.WaitRead) 
        if (event->SocketEvent(sock)==false) exit=true;
    }
  }
  while (!exit);  
}

typedef void (*THREADPROC)(void *);

void SocketWatcher::Watch(const Socket &sock, ISocketWatcherEvent *event)
{
  SocketInfo nfo(sock,event);
  _beginthread((THREADPROC)WatchDataThread,0,&nfo);
  WaitForSingleObject(nfo.blocker,INFINITE);
}
