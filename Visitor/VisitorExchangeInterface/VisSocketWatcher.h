#pragma once

///Defines callback interface for various socket events
class ISocketWatcherEvent
{
public:
  virtual bool SocketEvent(const Socket &sock)=0;
};


///Implements watching thread for socket. 
class SocketWatcher
{
public:
  /// Starts watching thread
  /**
  When an event on socket is detected, it notifies an ISocketWatchEvent interface
  */
  static void Watch(const Socket &sock, ISocketWatcherEvent *event);
};