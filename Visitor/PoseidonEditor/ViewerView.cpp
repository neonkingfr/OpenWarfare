#include <stdafx.h>
#include "PoseidonEditor.h"
#include "PosMainFrm.h"
#include "PoseidonDoc.h"
#include ".\viewerview.h"

ViewerView::ViewerView(IVisViewerExchange &viewer):_viewer(viewer)
{
}

ViewerView::~ViewerView(void)
{
}

#define INVOKEREALDOC if (GetRealDoc()) GetRealDoc()

CPosEdMainDoc *ViewerView::GetRealDoc()
{
  CPosEdMainDoc *doc=((CPosEdMainDoc*)(GetPosEdEnvironment()->m_pRealDoc));
  return doc;
}

void ViewerView::ComVersion(int version)
{
  if (version != MSG_VERSION)
  {
    CString text;
    text.Format(IDS_MSG_VERSION_ERROR, MSG_VERSION, version);
    AfxMessageBox(text,MB_OK|MB_ICONEXCLAMATION);
  }        
}

void ViewerView::FileImportBegin(const STexturePosMessData& data)
{
  _viewer.Import();
}

void ViewerView::LandTextureChange(const STexturePosMessData &data)
{  
  INVOKEREALDOC->SetLandTextureAt(data.nX,data.nZ,data.nTextureID,TRUE);
}

void ViewerView::LandHeightChange(const STexturePosMessData &data)
{
  INVOKEREALDOC->SetLandHeight(data.nX, data.nZ,  data.Y);
}

void ViewerView::BlockLandHeightChange(const Array<STexturePosMessData> &data)
{
  INVOKEREALDOC->SetBlockLandHeight(data);
}

void ViewerView::ObjectMove(const SMoveObjectPosMessData &data)
{
  INVOKEREALDOC->MoveObject(data);
}

void ViewerView::SelectionObjectAdd(const SObjectPosMessData &data)
{
  INVOKEREALDOC->SelectionObjectAdd(data);
}

void ViewerView::BlockSelectionObject(const Array<SMoveObjectPosMessData> &data)
{
  INVOKEREALDOC->BlockSelectionObject(data);
}

void ViewerView::CursorPositionSet(const SMoveObjectPosMessData &data)
{
  INVOKEREALDOC->OnCursorPositionSet(data);
}

void ViewerView::SelectionLandAdd(const SLandSelPosMessData &data)
{
  INVOKEREALDOC->SelectionLandAdd(data);
}

void ViewerView::SelectionLandClear()
{
  INVOKEREALDOC->SelectionLandClear();
}

void ViewerView::BlockSelectionLand(const Array<SLandSelPosMessData> &data)
{
  INVOKEREALDOC->BlockSelectionLand(data);
}
void ViewerView::SelectionObjectClear()
{
  INVOKEREALDOC->SelectionObjectClear();
}

void ViewerView::BlockMove(const Array<SMoveObjectPosMessData> &data)
{
  INVOKEREALDOC->BlockMoveObject(data);
}