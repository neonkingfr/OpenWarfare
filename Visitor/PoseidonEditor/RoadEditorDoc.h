#pragma once
#include "simplevectoreditordoc.h"

class RoadEditorDoc : public SimpleVectorEditorDoc
{
  mutable SimpleVectorEditorModel _generatedNet;
public:

  void RegenerateNet() const;
  
  SimpleVectorEditorModel &GetRoadNet() const
  {
    if (_generatedNet.GetPointCount()==0) RegenerateNet();
    return _generatedNet;
  }

};
