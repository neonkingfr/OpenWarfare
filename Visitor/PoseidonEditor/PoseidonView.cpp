/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

// PoseidonView.cpp : implementation of the CPoseidonView class
//

#include "stdafx.h"
#include "PoseidonEditor.h"

#include "PoseidonDoc.h"
#include "PoseidonView.h"
#include "PosMainFrm.h"
#include ".\poseidonview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPoseidonView

IMPLEMENT_DYNCREATE(CPoseidonView, CPosEdMainView)

BEGIN_MESSAGE_MAP(CPoseidonView, CPosEdMainView)
	//{{AFX_MSG_MAP(CPoseidonView)
	ON_COMMAND(ID_VIEW_STYLE, OnViewStyle)	
	ON_WM_RBUTTONDOWN()
	//}}AFX_MSG_MAP
	
	// STATUS bar
	ON_UPDATE_COMMAND_UI(ID_STATUS_ITEM_OBJECT, OnUpdateStatusInfoObject)
	// PANEL
	ON_COMMAND(IDC_FILLAREA_TXTR,OnFillAreaByTxtr)
	ON_UPDATE_COMMAND_UI(IDC_FILLAREA_TXTR,OnUpdateFillAreaByTxtr)
	ON_COMMAND(IDC_FILLAREA_ZONE,OnFillAreaByZone)
	ON_UPDATE_COMMAND_UI(IDC_FILLAREA_ZONE,OnUpdateFillAreaByZone)
	ON_COMMAND(IDC_FILLAREA_LAND,OnFillAreaByLand)
	ON_UPDATE_COMMAND_UI(IDC_FILLAREA_LAND,OnUpdateFillAreaByLand)
  ON_COMMAND(IDC_TOOLS_ANALYSE_TXTRS, OnToolsAnalyseTxtrs)
  ON_UPDATE_COMMAND_UI(IDC_TOOLS_ANALYSE_TXTRS, OnUpdateToolsAnalyseTxtrs)

	ON_COMMAND(IDC_CREATE_WOOD,OnCreateWood)
	ON_UPDATE_COMMAND_UI(IDC_CREATE_WOOD,OnUpdateCreateWood)
	ON_COMMAND(IDC_GOTO_NAMEAREA,OnGoToNameArea)
	ON_UPDATE_COMMAND_UI(IDC_GOTO_NAMEAREA,OnUpdateGoToNameArea)
	ON_COMMAND(IDC_EDIT_NAMEAREA,OnEditNameArea)
	ON_UPDATE_COMMAND_UI(IDC_EDIT_NAMEAREA,OnUpdateEditNameArea)
	ON_COMMAND(IDC_SELECT_NAMEAREA,OnSelectNameArea)
	ON_UPDATE_COMMAND_UI(IDC_SELECT_NAMEAREA,OnUpdateSelectNameArea)
	ON_COMMAND(IDC_SELOBJ_NAMEAREA,OnSelObjNameArea)
	ON_UPDATE_COMMAND_UI(IDC_SELOBJ_NAMEAREA,OnUpdateSelObjNameArea)
	ON_COMMAND(IDC_GOTO_KEYPOINT,OnGoToKeyPoint)
	ON_UPDATE_COMMAND_UI(IDC_GOTO_KEYPOINT,OnUpdateGoToKeyPoint)
	ON_COMMAND(IDC_EDIT_KEYPOINT,OnEditKeyPoint)
	ON_UPDATE_COMMAND_UI(IDC_EDIT_KEYPOINT,OnUpdateEditKeyPoint)
	ON_COMMAND(IDC_SELECT_KEYPOINT,OnSelectKeyPoint)
	ON_UPDATE_COMMAND_UI(IDC_SELECT_KEYPOINT,OnUpdateSelectKeyPoint)
	ON_COMMAND(IDC_SELOBJ_KEYPOINT,OnSelObjKeyPoint)
	ON_UPDATE_COMMAND_UI(IDC_SELOBJ_KEYPOINT,OnUpdateSelObjKeyPoint)
	ON_COMMAND(IDC_FILLAREA_CHNG,OnFillAreaByChange)
	ON_UPDATE_COMMAND_UI(IDC_FILLAREA_CHNG,OnUpdateFillAreaByChange)
	ON_COMMAND(IDC_GOTO_OBJNAME,OnGoToNameObject)
	ON_UPDATE_COMMAND_UI(IDC_GOTO_OBJNAME,OnUpdateGoToNameObject)
	// editace a zru�en� objektu
	ON_COMMAND(ID_EDIT_OBJECT_EDIT,OnEditObject)
	ON_UPDATE_COMMAND_UI(ID_EDIT_OBJECT_EDIT,OnUpdateEditObject)  

	ON_COMMAND(ID_EDIT_OBJECT_DEL,OnDeleteObject)
	ON_UPDATE_COMMAND_UI(ID_EDIT_OBJECT_DEL,OnUpdateDeleteObject)

  ON_COMMAND(ID_EDIT_CROP,OnEditCrop)

  // Lock & unlock ability
  ON_COMMAND(ID_LOCK,OnLock)
  ON_UPDATE_COMMAND_UI(ID_LOCK,OnUpdateLock)

  ON_COMMAND(ID_UNLOCK, OnUnlock)
  ON_UPDATE_COMMAND_UI(ID_UNLOCK, OnUpdateUnlock)

  // nahodny rozmystnovac objectu
  ON_COMMAND(IDC_RANDOM_PLACER,OnRandomPlacer)
  ON_UPDATE_COMMAND_UI(IDC_RANDOM_PLACER,OnUpdateRandomPlacer)
  // snap objects
  ON_COMMAND(ID_SNAP_OBJS,OnSnapObjects)
  ON_UPDATE_COMMAND_UI(ID_SNAP_OBJS,OnUpdateSnapObjects)
	
	// generov�n� s�t�
	ON_COMMAND(ID_TOOLS_GENER_NET,OnGenerateNet)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_GENER_NET,OnUpdateGenerateNet)
	// pr�ce s kurzorem
	ON_COMMAND(ID_TOOLS_GOTO_CURSOR,OnGotoCursorObject)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_GOTO_CURSOR,OnUpdateGotoCursorObject)
  ON_COMMAND(ID_PICK_TEXTURE,OnPickTexture)
  ON_COMMAND(ID_PICK_OBJECT,OnPickObject)
	// pr�ce s v��kou	
  ON_COMMAND(IDC_BTN_CHANGE_HEIGHT,OnHeightLandscape)
  ON_UPDATE_COMMAND_UI(IDC_BTN_CHANGE_HEIGHT,OnUpdateHeightLandscape)
  ON_COMMAND(IDC_BTN_EROSION,OnErozeLandscape)
  ON_UPDATE_COMMAND_UI(IDC_BTN_EROSION,OnUpdateErozeLandscape)	
	// nastaven� styl� zobrazen�
	ON_COMMAND(ID_VIEW_SHOW_STYLE0,OnBaseStyle0)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SHOW_STYLE0,OnUpdateBaseStyle0)
	ON_COMMAND(ID_VIEW_SHOW_STYLE1,OnBaseStyle1)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SHOW_STYLE1,OnUpdateBaseStyle1)
	ON_COMMAND(ID_VIEW_SHOW_STYLE2,OnBaseStyle2)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SHOW_STYLE2,OnUpdateBaseStyle2)
	ON_COMMAND(ID_VIEW_SHOW_STYLE3,OnBaseStyle3)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SHOW_STYLE3,OnUpdateBaseStyle3)
	ON_COMMAND(ID_VIEW_SHOW_STYLE4,OnBaseStyle4)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SHOW_STYLE4,OnUpdateBaseStyle4)
	ON_COMMAND(ID_VIEW_SHOW_STYLE5,OnBaseStyle5)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SHOW_STYLE5,OnUpdateBaseStyle5)
  ON_COMMAND(ID_VIEW_SHOW_STYLE6,OnBaseStyle6)
  ON_UPDATE_COMMAND_UI(ID_VIEW_SHOW_STYLE6,OnUpdateBaseStyle6)

	ON_COMMAND(ID_VIEW_STYLE_AREA_BOR,OnAreasBorder)
	ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_AREA_BOR,OnUpdateAreasBorder)
	ON_COMMAND(ID_VIEW_STYLE_AREA_ENT,OnAreasEntire)
	ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_AREA_ENT,OnUpdateAreasEntire)
	ON_COMMAND(ID_VIEW_STYLE_NATOBJ,OnNaturObjcs)
	ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_NATOBJ,OnUpdateNaturObjcs)
	ON_COMMAND(ID_VIEW_STYLE_PPLOBJ,OnPeopleObjcs)
	ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_PPLOBJ,OnUpdatePeopleObjcs)

	ON_COMMAND(ID_VIEW_STYLE_NEWROADSOBJ,OnNewRoadsObjcs)
	ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_NEWROADSOBJ,OnUpdateNewRoadsObjcs)

	ON_COMMAND(ID_VIEW_STYLE_UKWOBJ,OnUnknowObjcs)
	ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_UKWOBJ,OnUpdateUnknowObjcs)
	ON_COMMAND(ID_VIEW_STYLE_WOODS,OnShowWoods)
	ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_WOODS,OnUpdateShowWoods)
	ON_COMMAND(ID_VIEW_STYLE_NETS,OnShowNets)
	ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_NETS,OnUpdateShowNets)
	ON_COMMAND(ID_VIEW_STYLE_LNLN,OnEnableCntLn)
	ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_LNLN,OnUpdateEnableCntLn)
	ON_COMMAND(ID_ENABLE_GRID, OnEnableGrid)
	ON_UPDATE_COMMAND_UI(ID_ENABLE_GRID, OnUpdateEnableGrid)
	ON_COMMAND(ID_VIEW_STYLE_SCND, OnEnableScndTxtr)
	ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_SCND, OnUpdateEnableScndTxtr)
	ON_COMMAND(ID_VIEW_STYLE_SEA, OnEnableShowSea)
	ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_SEA, OnUpdateEnableShowSea)
  ON_COMMAND(ID_VIEW_STYLE_RIVERS, OnEnableShowRivers)
  ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_RIVERS, OnUpdateEnableShowRivers)
  ON_COMMAND(ID_VIEW_STYLE_BG_IMAGES, OnEnableShowBGImages)
  ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_BG_IMAGES, OnUpdateEnableShowBGImages)
	ON_COMMAND(ID_VIEW_STYLE_KEYPT_BOR, OnKeyPtBorder)
	ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_KEYPT_BOR, OnUpdateKeyPtBorder)
	ON_COMMAND(ID_VIEW_STYLE_KEYPT, OnKeyPtEntire)
	ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_KEYPT, OnUpdateKeyPtEntire)
  ON_COMMAND(ID_VIEW_STYLE_SHADOWING, OnEnableShowShadowing)
  ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_SHADOWING, OnUpdateEnableShowShadowing)
  ON_COMMAND(ID_VIEW_STYLE_LOCK, OnEnableShowLock)
  ON_UPDATE_COMMAND_UI(ID_VIEW_STYLE_LOCK, OnUpdateEnableShowLock)
	// nastaven� m���tka zobrazen�
	ON_COMMAND(ID_VIEW_ZOOM_IN, OnZoomIn)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ZOOM_IN, OnUpdateZoomIn)
	ON_COMMAND(ID_VIEW_ZOOM_OUT, OnZoomOut)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ZOOM_OUT, OnUpdateZoomOut)
	ON_COMMAND(ID_VIEW_ZOOM_FIT, OnViewZoomFit)
	ON_UPDATE_COMMAND_UI(ID_VIEW_ZOOM_FIT, OnUpdateViewZoomFit)
	ON_COMMAND(ID_VIEW_SCALE_10, OnViewScale10)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SCALE_10, OnUpdateViewScale10)
	ON_COMMAND(ID_VIEW_SCALE_25, OnViewScale25)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SCALE_25, OnUpdateViewScale25)
	ON_COMMAND(ID_VIEW_SCALE_50, OnViewScale50)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SCALE_50, OnUpdateViewScale50)
	ON_COMMAND(ID_VIEW_SCALE_75, OnViewScale75)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SCALE_75, OnUpdateViewScale75)
	ON_COMMAND(ID_VIEW_SCALE_100, OnViewScale100)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SCALE_100, OnUpdateViewScale100)
	ON_COMMAND(ID_VIEW_SCALE_150, OnViewScale150)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SCALE_150, OnUpdateViewScale150)
	ON_COMMAND(ID_VIEW_SCALE_200, OnViewScale200)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SCALE_200, OnUpdateViewScale200)
	ON_COMMAND(ID_VIEW_SCALE_250, OnViewScale250)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SCALE_250, OnUpdateViewScale250)
	ON_COMMAND(ID_VIEW_SCALE_500, OnViewScale500)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SCALE_500, OnUpdateViewScale500)
	ON_COMMAND(ID_VIEW_SCALE_USER, OnViewScaleUser)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SCALE_USER, OnUpdateViewScaleUser)
	// speci�ln� operace
	ON_COMMAND(ID_SPEC_ZOOM_IN, OnViewScaleSpecIn)
	ON_UPDATE_COMMAND_UI(ID_SPEC_ZOOM_IN, OnUpdateViewScaleSpec)
	ON_COMMAND(ID_SPEC_ZOOM_OUT, OnViewScaleSpecOut)
	ON_UPDATE_COMMAND_UI(ID_SPEC_ZOOM_OUT, OnUpdateViewScaleSpec)
	ON_COMMAND(ID_SPEC_MOVE_LEFT, OnViewAreaMoveLeft)
	ON_UPDATE_COMMAND_UI(ID_SPEC_MOVE_LEFT, OnUpdateViewAreaMove)
	ON_COMMAND(ID_SPEC_MOVE_RIGHT, OnViewAreaMoveRight)
	ON_UPDATE_COMMAND_UI(ID_SPEC_MOVE_RIGHT, OnUpdateViewAreaMove)
	ON_COMMAND(ID_SPEC_MOVE_TOP, OnViewAreaMoveTop)
	ON_UPDATE_COMMAND_UI(ID_SPEC_MOVE_TOP, OnUpdateViewAreaMove)
	ON_COMMAND(ID_SPEC_MOVE_BOTTOM, OnViewAreaMoveBottom)
	ON_UPDATE_COMMAND_UI(ID_SPEC_MOVE_BOTTOM, OnUpdateViewAreaMove)
	
  // background images
  ON_COMMAND(IDC_ADD_BG_IMAGE, OnCreateBgImage)
  ON_COMMAND(IDC_BG_IMAGE_DEL, OnRemoveBgImage)
  ON_UPDATE_COMMAND_UI(IDC_BG_IMAGE_DEL, OnUpdateOperBgImage)
  ON_COMMAND(IDC_BG_IMAGE_PROP, OnEditBgImage)
  ON_UPDATE_COMMAND_UI(IDC_BG_IMAGE_PROP, OnUpdateOperBgImage)
  // Texture layers 
  ON_COMMAND(ID_TEXTURE_LAYER_CREATE, OnAddTextureLayer)
  ON_UPDATE_COMMAND_UI(ID_TEXTURE_LAYER_CREATE, OnUpdateAddTextureLayer)
  ON_COMMAND(ID_TEXTURE_LAYER_REMOVE, OnRemoveTextureLayer)
  ON_UPDATE_COMMAND_UI(ID_TEXTURE_LAYER_REMOVE, OnUpdateRemoveTextureLayer)
  ON_COMMAND(ID_TEXTURE_LAYER_EDIT, OnEditTextureLayer)
  ON_UPDATE_COMMAND_UI(ID_TEXTURE_LAYER_EDIT, OnUpdateEditTextureLayer)

  //Clipboard
  ON_COMMAND(ID_EDIT_COPY, OnClipboardCopy)
  ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateClipboardCopy)
  ON_COMMAND(ID_EDIT_PASTE, OnClipboardPaste)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPoseidonView construction/destruction

CPoseidonView::CPoseidonView()
{
	// TODO: add construction code here
}

/////////////////////////////////////////////////////////////////////////////
// CPoseidonView message handlers

void CPoseidonView::OnViewStyle() 
{
	if (OnEditViewStyle(&GetPosEdEnvironment()->m_optPosEd.m_ViewStyle))
	{
		OnChangeViewStyle();
	}
}

/////////////////////////////////////////////////////////////////////////////
// TOOLS


void CPoseidonView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// standardn� zpracov�n�	
	CPosEdMainView::OnRButtonDown(nFlags, point);
}

void CPoseidonView::OnInitialUpdate()
{
	CPosEdMainView::OnInitialUpdate();

	m_pObjsPanel = &((CMainFrame *) AfxGetMainWnd())->m_wndObjPanel;
}
