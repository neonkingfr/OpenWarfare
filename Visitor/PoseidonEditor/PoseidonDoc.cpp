/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

// PoseidonDoc.cpp : implementation of the CPoseidonDoc class
//

#include "stdafx.h"
#include "PoseidonEditor.h"
#include "PoseidonDoc.h"
#include "PosMainFrm.h"
#include "..\PoseidonEdDlgs\progress.h"
#include ".\poseidondoc.h"
#include "png.h"
#include <float.h>
#include <Img/Interfaces/iImgFormat.hpp>
#include <Img/PNG/pngFormat.hpp>
#include <Es/Algorithms/qsort.hpp>
#include <Es/Files/filenames.hpp>
#include <Es/Strings/bstring.hpp>
#include <El/Common/perfProf.hpp>
#include <io.h>
#include "..\PoseidonEdDlgs\DlgRvmatSelection.h"
#include "..\PoseidonEdDlgs\DlgOriginOffset.h"
#include "StringList.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPoseidonDoc

IMPLEMENT_DYNCREATE(CPoseidonDoc, CPosEdMainDoc)

BEGIN_MESSAGE_MAP(CPoseidonDoc, CPosEdMainDoc)
  //{{AFX_MSG_MAP(CPoseidonDoc)
  ON_COMMAND(ID_PROJECT_SAVE, OnProjectSave)
  ON_UPDATE_COMMAND_UI(ID_PROJECT_SAVE, OnUpdateProjectSave)
  ON_COMMAND(ID_PROJECT_SAVE_AS, OnProjectSaveAs)
  ON_COMMAND(ID_TOOLS_TEMPL_TEXTURE, OnToolsTemplTexture)
  ON_COMMAND(ID_TOOLS_TEMPL_NATOBJ, OnToolsTemplNatobj)
  ON_COMMAND(ID_TOOLS_TEMPL_PPLOBJ, OnToolsTemplPplobj)

  ON_COMMAND(ID_TOOLS_TEMPL_NEWROADSOBJ, OnToolsTemplNewRoadsobj)

  ON_COMMAND(ID_TOOLS_TEMPL_NETS, OnToolsTemplNets)
  ON_COMMAND(ID_TOOLS_TEMPL_WOODS, OnToolsTemplWoods)
  ON_COMMAND(ID_PROJECT_IMPORT_RELIEF, OnProjectImportRelief)
  ON_COMMAND(ID_PROJECT_IMPORTTERRAINFROMXYZ1295, OnProjectImportXYZ)
  
  // ------------------------------------------------------------------------- //
  // this is the new command added to allow to modify the project params using //
  // the new project params dialog                                             //
  // ------------------------------------------------------------------------- //
  ON_COMMAND(ID_TOOLS_PROJECT_PARAMETERS, OnToolsProjectParameters)

  ON_COMMAND(ID_TOOLS_PROJECT_PARAMS, OnToolsProjectParams)
  //}}AFX_MSG_MAP
  
  // UNDO/REDO
  ON_COMMAND(ID_EDIT_ACTION_UNDO, OnEditUndoAction)
  ON_UPDATE_COMMAND_UI(ID_EDIT_ACTION_UNDO, OnUpdateEditUndoAction)
  ON_COMMAND(ID_EDIT_ACTION_REDO, OnEditRedoAction)
  ON_UPDATE_COMMAND_UI(ID_EDIT_ACTION_REDO, OnUpdateEditRedoAction)
  ON_COMMAND(ID_EDIT_UNDO_REDO_DEL, OnDelUndoRedoActions)
  ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO_REDO_DEL, OnUpdateDelUndoRedoActions)

  // STATUS bar
  ON_UPDATE_COMMAND_UI(ID_STATUS_ITEM_CURPOS, OnUpdateStatusInfoCurpos)
  ON_UPDATE_COMMAND_UI(ID_STATUS_ITEM_HEIGHT, OnUpdateStatusInfoHeight)
  ON_UPDATE_COMMAND_UI(ID_STATUS_ITEM_PRIMTXTR, OnUpdateStatusInfoPrimtxtr)
  ON_UPDATE_COMMAND_UI(ID_STATUS_ITEM_SCNDTXTR, OnUpdateStatusInfoScndtxtr)
  // re�ln� dokument	
  ON_COMMAND(ID_PROJECT_UPDATE_BULDOZER, OnUpdateBuldozer)
  ON_UPDATE_COMMAND_UI(ID_PROJECT_UPDATE_BULDOZER, OnUpdateUpdateBuldozer)
  ON_COMMAND(ID_PROJECT_REAL_IMPORT, OnImportRealDoc)
  ON_UPDATE_COMMAND_UI(ID_PROJECT_REAL_IMPORT, OnUpdateImportRealDoc)
  ON_COMMAND(ID_PROJECT_REAL_EXPORT, OnExportRealDoc)
  ON_UPDATE_COMMAND_UI(ID_PROJECT_REAL_EXPORT, OnUpdateExportRealDoc)
  // spr�va textur a objekt�

  ON_COMMAND(ID_TOOLS_MANAGE_OBJS, OnObjectsManagement)
  ON_UPDATE_COMMAND_UI(ID_TOOLS_MANAGE_OBJS, OnUpdateObjectsManagement)
  ON_COMMAND(ID_TOOLS_MANAGE_OBJSCHANGE,OnObjectsManageChange)
  ON_UPDATE_COMMAND_UI(ID_TOOLS_MANAGE_OBJSCHANGE, OnUpdateObjectsManagement)

  // PNG
  ON_COMMAND(ID_PROJECT_LOAD_PNG, OnProjectImportPNG)
  ON_COMMAND(ID_PROJECT_SAVE_PNG, OnProjectExportPNG)
  ON_UPDATE_COMMAND_UI(ID_PROJECT_LOAD_PNG, OnUpdateProjectImportPng)
  ON_UPDATE_COMMAND_UI(ID_PROJECT_SAVE_PNG, OnUpdateProjectExportPng)
  ON_COMMAND(ID_PROJECT_LOAD_WATER_PNG, OnProjectImportWaterPNG)
  ON_UPDATE_COMMAND_UI(ID_PROJECT_LOAD_WATER_PNG, OnUpdateProjectImportWaterPng)
  ON_COMMAND(ID_PROJECT_IMPORTFROMLANDBUILDER, OnProjectImportFromLandBuilder)

  ON_COMMAND(ID_TERRAIN_EXPORT_ASC, OnTerrainExportASC)
  ON_UPDATE_COMMAND_UI(ID_TERRAIN_EXPORT_ASC, OnUpdateTerrainExportASC)
  ON_COMMAND(ID_TERRAIN_EXPORT_XYZ, OnTerrainExportXYZ)
  ON_UPDATE_COMMAND_UI(ID_TERRAIN_EXPORT_XYZ, OnUpdateTerrainExportXYZ)

  ON_COMMAND(ID_PROJECT_IMPORTNEWROADSFROMLANDBUILDER, OnProjectNewRoadsImportFromLandBuilder)

  // SCC
  ON_COMMAND(ID_SCC_INTEG_CHECK_OUT, OnSccIntegCheckOut)
  ON_UPDATE_COMMAND_UI(ID_SCC_INTEG_CHECK_OUT, OnUpdateSccIntegCheckOut)
  ON_COMMAND(ID_SCC_INTEG_UNDO_CHECK_OUT, OnSccIntegUndoCheckOut)
  ON_UPDATE_COMMAND_UI(ID_SCC_INTEG_UNDO_CHECK_OUT, OnUpdateSccIntegUndoCheckOut)
  ON_COMMAND(ID_SCC_INTEG_GET_LATEST, OnSccIntegGetLatest)
  ON_UPDATE_COMMAND_UI(ID_SCC_INTEG_GET_LATEST, OnUpdateSccIntegGetLatest)
  ON_COMMAND(ID_SCC_INTEG_GET_LATEST_ALL, OnSccIntegGetLatestAll)
  ON_UPDATE_COMMAND_UI(ID_SCC_INTEG_GET_LATEST_ALL, OnUpdateSccIntegGetLatestAll)
  ON_COMMAND(ID_SCC_INTEG_CHECK_IN, OnSccIntegCheckIn)
  ON_UPDATE_COMMAND_UI(ID_SCC_INTEG_CHECK_IN, OnUpdateSccIntegCheckIn)
  ON_COMMAND(ID_SCC_INTEG_ADD, OnSccIntegAdd)
  ON_UPDATE_COMMAND_UI(ID_SCC_INTEG_ADD, OnUpdateSccIntegAdd)
  ON_UPDATE_COMMAND_UI(ID_TOOLS_TEMPL_TEXTURE, OnUpdateToolsTemplTexture)
  ON_COMMAND(ID_TOOLS_IMPORTSATELLITE, OnToolsImportSatellite)
  ON_COMMAND(ID_PROJECT_IMPORT_TEMPLATES, OnImportTemplates)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CPoseidonDoc construction/destruction

CPoseidonDoc::CPoseidonDoc()
{
  _askForCheckOut = false;
}

CPoseidonDoc::~CPoseidonDoc()
{
}

BOOL CPoseidonDoc::OnNewDocument()
{
  if (!CPosEdMainDoc::OnNewDocument()) return FALSE;

  m_nRealSizeUnit = 25; 
  // vytvo�en� nov�ho dokumentu
 
  if (!OnNewPoseidonProject(this)) return FALSE;
  
  // update polohy curzoru
  m_CursorObject.SetCursorLogPosition(m_CursorObject.m_ptCurrentPos,FALSE);

  m_pObjsPanel = &((CMainFrame *) AfxGetMainWnd())->m_wndObjPanel;


  _askForCheckOut = true;
  UpdateSSStatus();
  return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// zpracov�n� zpr�v z preview

/*void CPoseidonDoc::SendRealMessage(const SPosMessage& sMsg) const
{
  if (IsRealDocument())
  {
    CMainFrame* pMainFrame = (CMainFrame*)(AfxGetMainWnd());
    if (pMainFrame)
    {
      // reset fronty
      if (sMsg._iMsgType == SYSTEM_INIT)
      {
/*		
        pMainFrame->m_MessageFront.ResetClientMessageFront();
        pMainFrame->m_MessageFront.ResetServerMessageFront();
*//*
      }
      // poslu udalost
      ASSERT_KINDOF(CMainFrame,pMainFrame);
      pMainFrame->SendRealMessage(sMsg);
    }
  };
};*/

/////////////////////////////////////////////////////////////////////////////
// CPoseidonDoc commands

void CPoseidonDoc::OnProjectSave() 
{	
  TCHAR  dir[512];
  GetCurrentDirectory(512, dir);
  OnFileSave(); 
  SetCurrentDirectory(dir);
}

void CPoseidonDoc::OnUpdateProjectSave(CCmdUI* pCmdUI) 
{	
  pCmdUI->Enable(TRUE); 
}

void CPoseidonDoc::OnProjectSaveAs() 
{	
  TCHAR  dir[512];
  GetCurrentDirectory(512, dir);
  CString lastPathName = GetPathName();
  OnFileSaveAs(); 
  SetCurrentDirectory(dir);

  CString newPathName = GetPathName();

  CString oldDir;
  CString newDir;
  SeparateDirFromFileName(oldDir, lastPathName);
  SeparateDirFromFileName(newDir, newPathName);

  if (oldDir != newDir)
  {
    MakeLongDirStr(oldDir, oldDir);
    MakeLongDirStr(newDir, newDir);
    //copy also source bitmaps
    int n = m_PoseidonMap.GetBgImagesCount();
    for(int i = 0; i < n; i++)
    {
      CPosBackgroundImageObject * pImage = m_PoseidonMap.GetBgImage(i);
      if (pImage && !pImage->m_sFileName.IsEmpty())
      {
        // move image into new location
        CString oldFileName = oldDir + pImage->m_sFileName;
        CString newFileName = newDir + pImage->m_sFileName;

        if (MntExistFile(newFileName))
        {
          CString sQuestion;
          sQuestion.Format(IDS_OVERWRITE, (LPCTSTR) newFileName);

          int ret = AfxMessageBox(sQuestion,MB_YESNO|MB_ICONQUESTION);           

          if (ret == IDOK)
            CopyFile(oldFileName, newFileName, FALSE);
        }
        else
          CopyFile(oldFileName, newFileName, FALSE);    
      }      
    }
  }
};

// import reli�fu
void CPoseidonDoc::OnProjectImportRelief() 
{	
  DoImportRelief(this);	
}

/*void CPoseidonDoc::OnProjectMakeReal() 
{
  CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
  ASSERT_KINDOF(CMainFrame,pMainFrame);

  if (!IsRealDocument() || pMainFrame->IsConnected())
    OnMakeDocReal();


    if (IsRealDocument())
    {	// pokus o spou�t��n� buldozeru			
      if (!pMainFrame->IsConnected())
      {
        // spu�teni buldozeru
        CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
        if (pEnvir->m_optSystem.m_sPreviewAppPath.GetLength()!=0)
        {					
          WinExec(pEnvir->m_optSystem.m_sPreviewAppPath,SW_SHOW);
        }
      }
    };
}*/

void CPoseidonDoc::OnUpdateToolsTemplTexture(CCmdUI *pCmdUI)
{
  // TODO: Add your command update UI handler code here
  CTextureLayer* layer = GetActiveTextureLayer();
  bool enabled = layer && !layer->CheckExplicitBimpas();
  pCmdUI->Enable(enabled);
}

// menu TOOLS - definice objekt�
void CPoseidonDoc::OnToolsTemplTexture() 
{
  CTextureLayer *layer = GetActiveTextureLayer();
  if (!layer || layer->CheckExplicitBimpas()) return;
  if (DoDefineTemplateTexture(this))
  {
    SetModifiedFlag();
    // update dat v panelu
    CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
    ASSERT_KINDOF(CMainFrame,pMainFrame);
    pMainFrame->m_wndObjPanel.UpdateDocData(this,TRUE);
   
    m_PoseidonMap.TransferMapToBuldozer();
    UpdateAllViews(NULL,0,NULL);
  }
}

void CPoseidonDoc::OnToolsTemplNatobj() 
{	
  if (DoDefineTemplateNatObjs(this))
  {
    SetModifiedFlag();
    // update dat v panelu
    CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
    ASSERT_KINDOF(CMainFrame,pMainFrame);
    pMainFrame->m_wndObjPanel.UpdateDocData(this,TRUE);
    m_PoseidonMap.RecalculateObjectsSize();
    m_PoseidonMap.TransferMapToBuldozer();
    UpdateAllViews(NULL, 0, NULL);
  }
}

void CPoseidonDoc::OnToolsTemplPplobj() 
{	
  if (DoDefineTemplatePplObjs(this))
  {
    SetModifiedFlag();
    // update dat v panelu
    CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
    ASSERT_KINDOF(CMainFrame,pMainFrame);
    pMainFrame->m_wndObjPanel.UpdateDocData(this,TRUE);
    m_PoseidonMap.RecalculateObjectsSize();
    m_PoseidonMap.TransferMapToBuldozer();
    // update registrovan�ch objekt�
    //m_PoseidonMap.UpdateRegObjectsFromDefObjects(&m_MgrObjects);
    UpdateAllViews(NULL, 0, NULL);
  }
}

void CPoseidonDoc::OnToolsTemplNewRoadsobj() 
{	
  if (DoDefineTemplateNewRoadsObjs(this))
  {
    SetModifiedFlag();
    // update dat v panelu
    CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
    ASSERT_KINDOF(CMainFrame, pMainFrame);
    pMainFrame->m_wndObjPanel.UpdateDocData(this, TRUE);
    m_PoseidonMap.RecalculateObjectsSize();
    m_PoseidonMap.TransferMapToBuldozer();
    // update registrovan�ch objekt�
    //m_PoseidonMap.UpdateRegObjectsFromDefObjects(&m_MgrObjects);
    UpdateAllViews(NULL, 0, NULL);
  }
}

void CPoseidonDoc::OnToolsTemplNets() 
{	
	if (DoDefineTemplateNets(this))
	{
		SetModifiedFlag();
		// update dat v panelu
		CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
		ASSERT_KINDOF(CMainFrame, pMainFrame);
		pMainFrame->m_wndObjPanel.UpdateDocData(this, TRUE);
		// update existuj�c�ch s�t�
		m_PoseidonMap.UpdateNetsObjectsFromDefObjects(&m_MgrNets);
		m_PoseidonMap.TransferMapToBuldozer();
		UpdateAllViews(NULL, 0, NULL);
	}
}

void CPoseidonDoc::OnToolsTemplWoods() 
{	
	if (DoDefineTemplateWoods(this))
	{
		SetModifiedFlag();
		// update dat v panelu
		CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
		ASSERT_KINDOF(CMainFrame,pMainFrame);
		pMainFrame->m_wndObjPanel.UpdateDocData(this, TRUE);
		m_PoseidonMap.TransferMapToBuldozer();
		UpdateAllViews(NULL, 0, NULL);
	}
}

void CPoseidonDoc::OnToolsProjectParams() 
{
	if (DoEditPoseidonProject(this))
	{ // zmenilo se nastaveni adresaru
	}
}

void CPoseidonDoc::OnBuldConnect(IVisViewerExchange *realViewer)
{ 
    base::OnBuldConnect(realViewer);
}


BOOL CPoseidonDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
  _askForCheckOut = false; 
  m_pObjsPanel = &((CMainFrame *) AfxGetMainWnd())->m_wndObjPanel;

  CString ext;
  SeparateExtFromFileName(ext, lpszPathName);
  ext.MakeLower();

  if (ext == _T("pew"))
  {
    BOOL ret = CPosEdMainDoc::OnOpenDocument(lpszPathName);
    _askForCheckOut = true;
    UpdateSSStatus(lpszPathName);
    return ret;
  }    

  if (ext == _T("pbl"))
  {
    BOOL ret = OnOpenpblDocument(lpszPathName);
    _askForCheckOut = true;
    ::PostMessage(AfxGetMainWnd()->m_hWnd, WM_RENAME_DOC, 0, 0);
    UpdateSSStatus();
    return ret;
  }

  /// Read _T("ZYZ") file
  XYZFile file;
  file.SetSourceFile(lpszPathName);

  SDimension dim;
  if (!file.LoadDimension(dim))
    return FALSE;

  SMapSize mapSize;
  mapSize.step = (dim.StepX + dim.StepY) * 0.5f;
  m_nRealSizeUnit = mapSize.step;
  mapSize.size[0] = dim.SizeX;
  mapSize.size[1] = dim.SizeY;
  mapSize.offset[0] = dim.MinX;
  mapSize.offset[1] = dim.MinY;

  if (!OnXYZPoseidonProject(mapSize))
    return FALSE;

  file.Load(m_PoseidonMap, mapSize);

  ::PostMessage(AfxGetMainWnd()->m_hWnd, WM_RENAME_DOC, 0, 0);
  // update polohy curzoru
  //m_CursorObject.SetCursorLogPosition(m_CursorObject.m_ptCurrentPos,FALSE);
  _askForCheckOut = true;
  UpdateSSStatus();
  return TRUE;
}

BOOL CPoseidonDoc::ReloadDocument(BOOL bLoadLODShapes )
{
  DeleteContents();
  g_bLoadLODShapes = bLoadLODShapes;
  if (!OnOpenDocument(GetPathName()))
  {  
    g_bLoadLODShapes = TRUE;
    return FALSE;
  }
  
  UpdateAllViews(NULL);
  if (IsRealDocument()) m_PoseidonMap.TransferMapToBuldozer();
  return TRUE;
}

// error and warning handling for png
void user_error_fn(png_structp png_ptr, png_const_charp error_msg)
{
  AfxMessageBox(error_msg);
  LogF(error_msg);
}

void user_warning_fn(png_structp png_ptr, png_const_charp warning_msg)
{
  LogF(warning_msg);
}

#include <fstream>
#include <iomanip>
using std::ofstream;
using std::endl;
using std::ios;
using std::setprecision;

void CPoseidonDoc::OnTerrainExportASC()
{
  CDlgOriginOffset orDlg;
  orDlg.m_OriginX = 0.0;
  orDlg.m_OriginY = 0.0;

  if (IDOK != orDlg.DoModal()) return;

  CFileDialog dlg(FALSE, "asc", NULL, OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST , "Terrain in ARCINFOASCII (*.asc)|*.asc||");

  if (IDOK != dlg.DoModal()) return;

  BeginWaitCursor();

  CString fileName = dlg.GetPathName();

  if (!CheckFileAccess(fileName, TRUE, FALSE)) return;

  ofstream oFile;
  oFile.open(fileName, ios::out);

  // error in opening the file
  if (!oFile.is_open()) 
  {
    EndWaitCursor();
    return;
  }

  oFile << "ncols         " << m_PoseidonMap.GetSize().x << endl;
  oFile << "nrows         " << m_PoseidonMap.GetSize().z << endl;
  oFile << setiosflags(ios::fixed) << setprecision(1);
  oFile << "xllcorner     " << orDlg.m_OriginX << endl;
  oFile << "yllcorner     " << orDlg.m_OriginY << endl;
  oFile << "cellsize      " << m_nRealSizeUnit << endl;
  oFile << "NODATA_value  -9999" << endl;

  UNITPOS lftTop;
  UNITPOS rgtBot;

  lftTop.x = 0;
  lftTop.z = m_PoseidonMap.GetSize().z;

  rgtBot.x = m_PoseidonMap.GetSize().x;
  rgtBot.z = 0;

  oFile << setiosflags(ios::fixed) << setprecision(2);

  UNITPOS pos;
  for (pos.z = lftTop.z - 1; pos.z >= rgtBot.z; pos.z--) 
  {
    for (pos.x = lftTop.x; pos.x < rgtBot.x; pos.x++)
    {
      float val = m_PoseidonMap.GetLandscapeHeight(pos);
      oFile << val << " ";
    }
    oFile << endl;
  }
  oFile.close();

  EndWaitCursor();
}

void CPoseidonDoc::OnUpdateTerrainExportASC(CCmdUI* pCmdUI)
{
  pCmdUI->Enable(TRUE);
}

void CPoseidonDoc::OnTerrainExportXYZ()
{
  CDlgOriginOffset orDlg;
  orDlg.m_OriginX = 0.0;
  orDlg.m_OriginY = 0.0;

  if (IDOK != orDlg.DoModal()) return;

  CFileDialog dlg(FALSE, "xyz", NULL, OFN_OVERWRITEPROMPT | OFN_PATHMUSTEXIST , "Terrain in XYZ (*.xyz)|*.xyz||");

  if (IDOK != dlg.DoModal()) return;

  BeginWaitCursor();

  CString fileName = dlg.GetPathName();

  if (!CheckFileAccess(fileName, TRUE, FALSE)) return;

  ofstream oFile;
  oFile.open(fileName, ios::out);

  // error in opening the file
  if (!oFile.is_open()) 
  {
    EndWaitCursor();
    return;
  }

  UNITPOS lftTop;
  UNITPOS rgtBot;

  lftTop.x = 0;
  lftTop.z = m_PoseidonMap.GetSize().z;

  rgtBot.x = m_PoseidonMap.GetSize().x;
  rgtBot.z = 0;

  oFile << setiosflags(ios::fixed) << setprecision(2);

  UNITPOS pos;
  for (pos.z = rgtBot.z; pos.z < lftTop.z; pos.z++) 
  {
    for (pos.x = lftTop.x; pos.x < rgtBot.x; pos.x++)
    {
      oFile << setw(16) << setfill(' ') << orDlg.m_OriginX + pos.x * m_nRealSizeUnit;
      oFile << setw(16) << setfill(' ') << orDlg.m_OriginY + pos.z * m_nRealSizeUnit;
      float val = m_PoseidonMap.GetLandscapeHeight(pos);
      oFile << setw(16) << setfill(' ') << val << endl;
    }
  }
  oFile.close();

  EndWaitCursor();
}

void CPoseidonDoc::OnUpdateTerrainExportXYZ(CCmdUI* pCmdUI)
{
  pCmdUI->Enable(TRUE);
}

void CPoseidonDoc::OnProjectExportPNG()
{
  CFileDialog dlg(FALSE, "pbl", NULL, OFN_OVERWRITEPROMPT|OFN_PATHMUSTEXIST , "Terrain in image cfg (*.pbl)|*.pbl||");

  if (IDOK != dlg.DoModal()) return;

  CString fileName = dlg.GetPathName();

  if (!CheckFileAccess(fileName, TRUE, FALSE)) return;

  //float minHeight = FLT_MAX;
  //float maxHeight = FLT_MIN;

  // if area is selected save just area...
  UNITPOS lftTop;
  UNITPOS rgtBot;
 
  float minmaxImageHeight[2];  

  if (m_CurrentArea.IsValidArea())
  {
    CRect saveRect;
    m_CurrentArea.GetObjectHull(saveRect);
    m_PoseidonMap.GetUnitPosHull(lftTop, rgtBot, saveRect);   

    float minmaxAreaHeight[2]; 
    minmaxAreaHeight[0] = FLT_MAX;
    minmaxAreaHeight[1] = FLT_MIN;

    UNITPOS pos;
    for(pos.z = rgtBot.z; pos.z < lftTop.z; pos.z++) for(pos.x = lftTop.x; pos.x < rgtBot.x; pos.x++)
    {
      float val = m_PoseidonMap.GetLandscapeHeight(pos);
      minmaxAreaHeight[0] = min(val, minmaxAreaHeight[0]);
      minmaxAreaHeight[1] = max(val, minmaxAreaHeight[1]);
    }

    if (minmaxAreaHeight[0] == minmaxAreaHeight[1])
    {
      minmaxAreaHeight[1] += 1; // just to avoid division by zero...
    }

    float minmaxHeight[2]; 
    minmaxHeight[0] = FLT_MAX;
    minmaxHeight[1] = FLT_MIN;

    for(pos.z = 0; pos.z < m_PoseidonMap.GetSize().z; pos.z++) for(pos.x = 0; pos.x < m_PoseidonMap.GetSize().x; pos.x++)
    {
      float val = m_PoseidonMap.GetLandscapeHeight(pos);
      minmaxHeight[0] = min(val, minmaxHeight[0]);
      minmaxHeight[1] = max(val, minmaxHeight[1]);
    }

    if (minmaxHeight[0] == minmaxHeight[1])
    {
      minmaxHeight[1] += 1; // just to avoid division by zero...
    }

    float step = (minmaxHeight[1] - minmaxHeight[0]);
    minmaxImageHeight[0] = minmaxHeight[0] - step * 0.1f;
    minmaxImageHeight[1] = minmaxHeight[1] + step * 0.1f;

    bool onlyArea;
    if (!OnImageExport(minmaxHeight, minmaxImageHeight, NULL, minmaxAreaHeight, &onlyArea))
    {
      return;
    }

    if (!onlyArea)
    {
      lftTop.x = 0;
      lftTop.z = m_PoseidonMap.GetSize().z;

      rgtBot.x = m_PoseidonMap.GetSize().x;
      rgtBot.z = 0;
    }
  }
  else
  {
    lftTop.x = 0;
    lftTop.z = m_PoseidonMap.GetSize().z;

    rgtBot.x = m_PoseidonMap.GetSize().x;
    rgtBot.z = 0;

    float minmaxHeight[2]; 
    minmaxHeight[0] = FLT_MAX;
    minmaxHeight[1] = FLT_MIN;

    UNITPOS pos;
    for(pos.z = rgtBot.z; pos.z < lftTop.z; pos.z++) for(pos.x = lftTop.x; pos.x < rgtBot.x; pos.x++)
    {
      float val = m_PoseidonMap.GetLandscapeHeight(pos);
      minmaxHeight[0] = min(val, minmaxHeight[0]);
      minmaxHeight[1] = max(val, minmaxHeight[1]);
    }

    if (minmaxHeight[0] == minmaxHeight[1])
    {
      minmaxHeight[1] += 1; // just to avoid division by zero...
    }

    float step = (minmaxHeight[1] - minmaxHeight[0]);
    minmaxImageHeight[0] = minmaxHeight[0] - step * 0.1f;
    minmaxImageHeight[1] = minmaxHeight[1] + step * 0.1f;

    if (!OnImageExport(minmaxHeight, minmaxImageHeight)) return;
  }

  int width = rgtBot.x - lftTop.x;
  int height = lftTop.z - rgtBot.z;

  ParamFile cfgFile;
  if (LSOK != cfgFile.Parse(fileName))
  {
    AfxMessageBox(IDS_OPER_FAILED);
    return;
  }

  ParamClassPtr cfgClass = cfgFile.AddClass("cfg");
  CString pngFileName; 
  CutExtFromFileName(pngFileName, fileName);
  pngFileName += ".png";

  if (!CheckFileAccess(pngFileName, TRUE, FALSE)) return;

  FILE *fp = fopen(pngFileName, "wb");
  if (!fp) 
  {
    AfxMessageBox(IDS_OPER_FAILED);
    return;
  };

  SeparateNameFromFileName(pngFileName, pngFileName);
  cfgClass->Add("PNGfilename", RStringB((LPCTSTR) pngFileName));
  cfgClass->Add("squareSize", m_nRealSizeUnit);
  cfgClass->Add("originX", lftTop.x);
  cfgClass->Add("originY", rgtBot.z);

  png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL,NULL,NULL); 

  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr)
  {
    png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
    AfxMessageBox(IDS_OPER_FAILED);
    fclose(fp);
    return;
  }

  if (setjmp(png_jmpbuf(png_ptr)))
  {
    png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
    AfxMessageBox(IDS_OPER_FAILED);
    fclose(fp);
    return;
  }

  png_init_io(png_ptr, fp);

  png_set_IHDR(png_ptr, info_ptr, width, height, 16, PNG_COLOR_TYPE_GRAY, 
         PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, 
         PNG_FILTER_TYPE_DEFAULT);

  png_uint_16pp row_pointers = new png_uint_16p[height];
  png_uint_16p data = new png_uint_16[height* width];  

  for (int i = 0; i < height; i++)
  {
    row_pointers[i] = data + i * width;  
  }

  cfgClass->Add("minHeight", minmaxImageHeight[0]);
  cfgClass->Add("maxHeight", minmaxImageHeight[1]);

  cfgFile.Save();

  float step = (minmaxImageHeight[1] - minmaxImageHeight[0]) / 0xffff;  
  int heightm1 = lftTop.z - 1;
  UNITPOS pos;
  for (pos.z = rgtBot.z; pos.z < lftTop.z; pos.z++) for(pos.x = lftTop.x; pos.x < rgtBot.x; pos.x++)
  {
    float val = m_PoseidonMap.GetLandscapeHeight(pos);    
    data[(heightm1 - pos.z) * width + (pos.x - lftTop.x)] = (png_uint_16) ((val - minmaxImageHeight[0])/step);
  }

  png_set_rows(png_ptr, info_ptr,(png_bytepp) row_pointers);
  png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_SWAP_ENDIAN, NULL);
  png_destroy_write_struct(&png_ptr, &info_ptr);

  delete [] row_pointers;
  delete [] data;

  fclose(fp);
}

void CPoseidonDoc::OnProjectImportPNG()
{
  CFileDialog dlg(TRUE, NULL, NULL, OFN_FILEMUSTEXIST, "Terrain in image cfg (*.pbl)|*.pbl||");

  if (IDOK != dlg.DoModal())
    return;

 CString cfgFileName = dlg.GetPathName();
 if (!CheckFileAccess(cfgFileName, FALSE)) 
   return;


 
 ParamFile cfgFile;
 cfgFile.Parse(cfgFileName);
 ParamClassPtr cfgClass = cfgFile.GetClass("cfg");
 if (cfgClass.IsNull())
 {
   LogF("Entry %s, not found in paramfile %s.","cfg",cfgFileName );
   AfxMessageBox(IDS_OPER_FAILED);
   return;
 }

 ParamEntryPtr entry = cfgClass->FindEntry("PNGfilename");
 if (entry.IsNull())
 {
   LogF("Entry %s, not found in paramfile %s.","PNGfilename",cfgFileName );
   AfxMessageBox(IDS_OPER_FAILED);
   return;
 }

 CString fileName = (LPCSTR)((RStringB)(*entry));
 CString dir;
 SeparateDirFromFileName(dir, cfgFileName);
 MakeLongDirStr(dir, dir);

 if (!CheckFileAccess(dir + fileName, FALSE)) 
   return;

 FILE *fp = fopen(dir + fileName, "rb");
 if (!fp) 
 {
   AfxMessageBox(IDS_OPER_FAILED);
   return;
 };
 
 png_byte header[8];
 fread(header, sizeof(png_byte), 8, fp); 
 int ret = png_sig_cmp((png_bytep) header, 0, 8);
 if (ret != 0) 
 {
   AfxMessageBox(IDS_NOT_PNG);
   fclose(fp);
   return; 
 }

 png_structp png_ptr = png_create_read_struct
   (PNG_LIBPNG_VER_STRING, NULL,NULL,NULL);

 if (!png_ptr)
 {
   LogF("png_create_read_struct failed");
   AfxMessageBox(IDS_OPER_FAILED);
   fclose(fp);
   return;
 }

 png_infop info_ptr = png_create_info_struct(png_ptr);
 if (!info_ptr)
 {
   png_destroy_read_struct(&png_ptr,
     (png_infopp)NULL, (png_infopp)NULL);
   LogF("png_create_info_struct failed");
   AfxMessageBox(IDS_OPER_FAILED);
   fclose(fp);
   return;
 }

 png_infop end_info = png_create_info_struct(png_ptr);
 if (!end_info)
 {
   png_destroy_read_struct(&png_ptr, &info_ptr,
     (png_infopp)NULL);
   LogF("png_create_info_struct failed");
   AfxMessageBox(IDS_OPER_FAILED);
   fclose(fp);
   return;
 }

 if (setjmp(png_jmpbuf(png_ptr)))
 {
   png_destroy_read_struct(&png_ptr, &info_ptr,
     (png_infopp)NULL);
   AfxMessageBox(IDS_OPER_FAILED);
   fclose(fp);
   return;
 }
 
 png_init_io(png_ptr, fp);
 png_set_sig_bytes(png_ptr, 8);

 png_read_info(png_ptr, info_ptr);

 png_uint_32 width;
 png_uint_32 height;
 int bit_depth;
 int color_type;
 png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth,
   &color_type, NULL, NULL, NULL);

 if (color_type != PNG_COLOR_TYPE_GRAY && bit_depth != 16)
 {
   AfxMessageBox(IDS_BAD_PNG);
   png_destroy_read_struct(&png_ptr, &info_ptr,
     &end_info);

   LogF("png_create_info_struct failed");
   fclose(fp);
   return;
 }
 
 if (bit_depth == 16)
   png_set_swap(png_ptr);

 png_read_update_info(png_ptr, info_ptr);

 png_uint_16pp row_pointers = new png_uint_16p[height];
 png_uint_16p data = new png_uint_16[height * width];

 //memset(data, 0x0, sizeof(*data) * height * width); 

 for(unsigned int i = 0; i < height; i++)
   row_pointers[i] = data + i * width;

 png_read_image(png_ptr, (png_bytepp)row_pointers);
 png_read_end(png_ptr, end_info);
 png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
 fclose(fp);

 entry = cfgClass->FindEntry("maxHeight");
 if (entry.IsNull())
 {
   LogF("Entry %s, not found in paramfile %s.","maxHeight",cfgFileName );
   AfxMessageBox(IDS_OPER_FAILED);
   return;
 }
 float maxHeight = *entry;

 entry = cfgClass->FindEntry("minHeight");
 if (entry.IsNull())
 {
   LogF("Entry %s, not found in paramfile %s.","minHeight",cfgFileName );
   AfxMessageBox(IDS_OPER_FAILED);
   return;
 }
 float minHeight = *entry;

 float step = (maxHeight - minHeight) / 0xffff;

 /// If there is an origin read it
 int originX = 0;
 entry = cfgClass->FindEntry("originX");
 if (!entry.IsNull()) 
  originX = (int) *entry;

 int originY = 0;
 entry = cfgClass->FindEntry("originY");
 if (!entry.IsNull()) 
   originY = (int) *entry;
 
 UNITPOS pos;
 int heightm1 = height - 1;
 for(pos.z = 0; pos.z < (int)height; pos.z++) for(pos.x = 0; pos.x < (int)width; pos.x++)
   m_PoseidonMap.SetLandscapeHeight(pos.x + originX, pos.z + originY, (data[(heightm1 - pos.z) * width + pos.x ]) * step + minHeight, FALSE);

 m_PoseidonMap.UpdateObjectsHeight();

 UpdateAllViews(NULL);
}

BOOL CPoseidonDoc::OnOpenpblDocument(const CString& cfgFileName)
{
  ParamFile cfgFile;
  if (LSOK != cfgFile.Parse(cfgFileName))
  {
    CString text;
    text.Format(IDS_FILE_OPEN_FAILED, cfgFileName);
    AfxMessageBox(text);
    return FALSE;
  }

  ParamClassPtr cfgClass = cfgFile.GetClass("cfg");
  if (cfgClass.IsNull())
  {
    LogF("Entry %s, not found in paramfile %s.","cfg",cfgFileName );
    AfxMessageBox(IDS_OPER_FAILED);
    return FALSE;
  }
  ParamEntryPtr entry = cfgClass->FindEntry("PNGfilename");
  if (entry.IsNull())
  {
    LogF("Entry %s, not found in paramfile %s.","PNGfilename",cfgFileName );
    AfxMessageBox(IDS_OPER_FAILED);
    return FALSE;
  }

  CString fileName = (LPCSTR)((RStringB)(*entry));
  CString dir;
  SeparateDirFromFileName(dir, cfgFileName);
  MakeLongDirStr(dir, dir);

  FILE *fp = fopen(dir + fileName, "rb");
  if (!fp) 
  {
    CString text;
    text.Format(IDS_FILE_OPEN_FAILED, dir + fileName);
    AfxMessageBox(text);
    return FALSE;
  }

  png_byte header[8];
  fread(header, sizeof(png_byte), 8, fp); 
  int ret = png_sig_cmp((png_bytep) header, 0, 8);
  if (ret != 0) 
  {
    AfxMessageBox(IDS_NOT_PNG);
    fclose(fp);
    return FALSE; 
  }

  png_structp png_ptr = png_create_read_struct
    (PNG_LIBPNG_VER_STRING, NULL,user_error_fn,user_warning_fn);

  if (!png_ptr)
  {
    AfxMessageBox(IDS_OPER_FAILED);
    LogF("png_create_read_struct failed");
    fclose(fp);
    return FALSE;
  }

  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr)
  {
    png_destroy_read_struct(&png_ptr,
      (png_infopp)NULL, (png_infopp)NULL);
    AfxMessageBox(IDS_OPER_FAILED);
    LogF("png_create_info_struct failed");
    fclose(fp);
    return FALSE;
  }

  png_infop end_info = png_create_info_struct(png_ptr);
  if (!end_info)
  {
    png_destroy_read_struct(&png_ptr, &info_ptr,
      (png_infopp)NULL);
    AfxMessageBox(IDS_OPER_FAILED);
    LogF("png_create_info_struct failed");
    fclose(fp);
    return FALSE;
  }

  if (setjmp(png_jmpbuf(png_ptr)))
  {
    png_destroy_read_struct(&png_ptr, &info_ptr,
      (png_infopp)NULL);
    AfxMessageBox(IDS_OPER_FAILED);
    fclose(fp);
    return FALSE;
  }

  png_init_io(png_ptr, fp);
  png_set_sig_bytes(png_ptr, 8);

  png_read_info(png_ptr, info_ptr);

  png_uint_32 width;
  png_uint_32 height;
  int bit_depth;
  int color_type;
  png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth,
    &color_type, NULL, NULL, NULL);

  if (color_type != PNG_COLOR_TYPE_GRAY && bit_depth != 16)
  {
    AfxMessageBox(IDS_BAD_PNG);
    png_destroy_read_struct(&png_ptr, &info_ptr,
      &end_info);

    LogF("png_create_info_struct failed");
    fclose(fp);
    return FALSE;
  }

  if (bit_depth == 16)
    png_set_swap(png_ptr);

  png_read_update_info(png_ptr, info_ptr);

  png_uint_16pp row_pointers = new png_uint_16p[height];
  png_uint_16p data = new png_uint_16[height * width];

  for(unsigned int i = 0; i < height; i++)
    row_pointers[i] = data + i * width;

  png_read_image(png_ptr, (png_bytepp)row_pointers);
  png_read_end(png_ptr, end_info);
  png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
  fclose(fp);

  entry = cfgClass->FindEntry("maxHeight");
  if (entry.IsNull())
  {
    LogF("Entry %s, not found in paramfile %s.","maxHeight",cfgFileName );
    AfxMessageBox(IDS_OPER_FAILED);
    return FALSE;
  }
  float maxHeight = *entry;

  entry = cfgClass->FindEntry("minHeight");
  if (entry.IsNull())
  {
    LogF("Entry %s, not found in paramfile %s.","minHeight",cfgFileName );
    AfxMessageBox(IDS_OPER_FAILED);
    return FALSE;
  }
  float minHeight = *entry;

  float step = (maxHeight - minHeight) / 0xffff;

  // Create new document
  entry = cfgClass->FindEntry("squareSize");
  if (entry.IsNull())
  {
    LogF("Entry %s, not found in paramfile %s.","squareSize",cfgFileName );
    AfxMessageBox(IDS_OPER_FAILED);
    return FALSE;
  }
  m_nRealSizeUnit = *entry;

  // size must be power of 2
  int log = 0;
  int orig = max(width, height);
  int size = orig;
  for(; size > 0; log++)
    size = size >> 1;

  if (orig == 1 << (log - 1))
    size = 1 << (log - 1);
  else
    size = 1 << log;

  m_PoseidonMap.OnNewMap(size,size);

  UNITPOS pos;
  int heightm1 = height - 1;
  for(pos.z = 0; pos.z < (int) height; pos.z++) for(pos.x = 0; pos.x < (int) width; pos.x++)
    m_PoseidonMap.SetLandscapeHeight(pos,  (data[(heightm1 - pos.z) * width + pos.x]) * step + minHeight, FALSE);

  //UpdateAllViews(NULL);

  return TRUE;
}

void CPoseidonDoc::OnProjectImportWaterPNG()
{
  CFileDialog dlg(TRUE, NULL, NULL, OFN_FILEMUSTEXIST, "Water in image cfg (*.pbl)|*.pbl||");

  if (IDOK != dlg.DoModal())
    return;

  CString cfgFileName = dlg.GetPathName();
  if (!CheckFileAccess(cfgFileName, FALSE)) 
    return;

  ParamFile cfgFile;
  cfgFile.Parse(cfgFileName);
  ParamClassPtr cfgClass = cfgFile.GetClass("cfg");
  if (cfgClass.IsNull())
  {
    LogF("Entry %s, not found in paramfile %s.","cfg",cfgFileName );
    AfxMessageBox(IDS_OPER_FAILED);
    return;
  }

  ParamEntryPtr entry = cfgClass->FindEntry("PNGfilename");
  if (entry.IsNull())
  {
    LogF("Entry %s, not found in paramfile %s.","PNGfilename",cfgFileName );
    AfxMessageBox(IDS_OPER_FAILED);
    return;
  }
  CString fileName = (LPCSTR)((RStringB)(*entry));

  entry = cfgClass->FindEntry("squareSize");
  if (entry.IsNull())
  {
    LogF("Entry %s, not found in paramfile %s.","squareSize",cfgFileName );
    AfxMessageBox(IDS_OPER_FAILED);
    return;
  }
  float realSize = *entry;

  CString dir;
  SeparateDirFromFileName(dir, cfgFileName);
  MakeLongDirStr(dir, dir);

  if (!CheckFileAccess(dir + fileName, FALSE)) 
    return;

  FILE *fp = fopen(dir + fileName, "rb");
  if (!fp) 
  {
    AfxMessageBox(IDS_OPER_FAILED);
    return;
  };

  png_byte header[8];
  fread(header, sizeof(png_byte), 8, fp); 
  int ret = png_sig_cmp((png_bytep) header, 0, 8);
  if (ret != 0) 
  {
    AfxMessageBox(IDS_NOT_PNG);
    fclose(fp);
    return; 
  }

  png_structp png_ptr = png_create_read_struct
    (PNG_LIBPNG_VER_STRING, NULL,user_error_fn,user_warning_fn);

  if (!png_ptr)
  {
    LogF("png_create_read_struct failed");
    AfxMessageBox(IDS_OPER_FAILED);
    fclose(fp);
    return;
  }

  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr)
  {
    png_destroy_read_struct(&png_ptr,
      (png_infopp)NULL, (png_infopp)NULL);
    LogF("png_create_info_struct failed");
    AfxMessageBox(IDS_OPER_FAILED);
    fclose(fp);
    return;
  }

  png_infop end_info = png_create_info_struct(png_ptr);
  if (!end_info)
  {
    png_destroy_read_struct(&png_ptr, &info_ptr,
      (png_infopp)NULL);
    LogF("png_create_info_struct failed");
    AfxMessageBox(IDS_OPER_FAILED);
    fclose(fp);
    return;
  }

  if (setjmp(png_jmpbuf(png_ptr)))
  {
    png_destroy_read_struct(&png_ptr, &info_ptr,
      (png_infopp)NULL);
    AfxMessageBox(IDS_OPER_FAILED);
    fclose(fp);
    return;
  }


  png_init_io(png_ptr, fp);
  png_set_sig_bytes(png_ptr, 8);

  png_read_info(png_ptr, info_ptr);

  png_uint_32 width;
  png_uint_32 height;
  int bit_depth;
  int color_type;
  png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth,
    &color_type, NULL, NULL, NULL);

  if (color_type != PNG_COLOR_TYPE_GRAY && bit_depth != 16)
  {
    AfxMessageBox(IDS_BAD_PNG);
    png_destroy_read_struct(&png_ptr, &info_ptr,
      &end_info);

    LogF("png_create_info_struct failed");
    fclose(fp);
    return;
  }

  if (bit_depth == 16)
    png_set_swap(png_ptr);

  png_read_update_info(png_ptr, info_ptr);

  png_uint_16pp row_pointers = new png_uint_16p[height];
  png_uint_16p data = new png_uint_16[height * width];

  //memset(data, 0x0, sizeof(*data) * height * width); 

  for(unsigned int i = 0; i < height; i++)
    row_pointers[i] = data + i * width;

  png_read_image(png_ptr, (png_bytepp)row_pointers);
  png_read_end(png_ptr, end_info);
  png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
  fclose(fp);

  entry = cfgClass->FindEntry("maxHeight");
  if (entry.IsNull())
  {
    LogF("Entry %s, not found in paramfile %s.","maxHeight",cfgFileName );
    AfxMessageBox(IDS_OPER_FAILED);
    return;
  }
  float maxHeight = *entry;

  entry = cfgClass->FindEntry("minHeight");
  if (entry.IsNull())
  {
    LogF("Entry %s, not found in paramfile %s.","minHeight",cfgFileName );
    AfxMessageBox(IDS_OPER_FAILED);
    return;
  }
  float minHeight = *entry;

  float step = (maxHeight - minHeight) / 0xffff;

  /// If there is an origin read it
  int originX = 0;
  entry = cfgClass->FindEntry("originX");
  if (!entry.IsNull()) 
    originX = (int) *entry;

  int originY = 0;
  entry = cfgClass->FindEntry("originY");
  if (!entry.IsNull()) 
    originY = (int) *entry;

  WaterLayer& waterLayer = m_PoseidonMap.GetWaterLayer();
  waterLayer.Create(originX + width, originY + height, realSize);

  int heightm1 = height - 1;
  for(int z = 0; z < (int)height; z++) for(int x = 0; x < (int)width; x++)
  {
    UNITPOS pos = {x + originX, z + originY};
    waterLayer.SetHeight(pos, (data[(heightm1 - z) * width + x ]) * step + minHeight);
  }

    //m_PoseidonMap.SetLandscapeHeight(pos.x + originX, pos.z + originY, (0xffff - data[(heightm1 - pos.z) * width + pos.x ]) * step + minHeight, FALSE);
  UpdateAllViews(NULL);
}

void CPoseidonDoc::OnUpdateProjectImportPng(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(TRUE);
}

void CPoseidonDoc::OnUpdateProjectExportPng(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(TRUE);
}

void CPoseidonDoc::OnUpdateProjectImportWaterPng(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(TRUE);
}

void CPoseidonDoc::OnSccIntegCheckOut()
{
  UpdateSSStatus();
  if (_status == SCC_STATUS_INVALID || !(_status & SCC_STATUS_CONTROLLED) || (_status & SCC_STATUS_OUTBYUSER))
    return;

  CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
  MsSccFunctions& sccFunctions = pMainFrame->GetSccFunctions();

  CString path = GetPathName();  

  SCCRTN ret = sccFunctions.CheckOut(GetPathName());

  if (IS_SCC_SUCCESS(ret))  
    return; 

  if (IS_SCC_WARNING(ret))
  {
    LogF("%s %d : MsSccFunctions warning %d", __FILE__, __LINE__,ret);
    return;
  }

  CString text;
  text.Format(IDS_OPER_FAILED2, ret);
  AfxMessageBox(text);
  return;
}

void CPoseidonDoc::OnUpdateSccIntegCheckOut(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_status != SCC_STATUS_INVALID && (_status & SCC_STATUS_CONTROLLED) && !(_status & SCC_STATUS_OUTBYUSER)); 
}

void CPoseidonDoc::OnSccIntegUndoCheckOut()
{  
  UpdateSSStatus();
  if (!(_status != SCC_STATUS_INVALID && (_status & SCC_STATUS_CONTROLLED) && (_status & SCC_STATUS_CHECKEDOUT)))
    return;

  if (IsModified() && AfxMessageBox(IDS_SCC_INTEG_LOST_CHANGES, MB_YESNO) == IDNO)
    return;
  
  CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
  MsSccFunctions& sccFunctions = pMainFrame->GetSccFunctions(); 

  SCCRTN ret = sccFunctions.UndoCheckOut(GetPathName());

  if (IS_SCC_SUCCESS(ret))  
  {
    ReloadDocument();
    return; 
  }

  if (IS_SCC_WARNING(ret))
  {
    LogF("%s %d : MsSccFunctions warning %d", __FILE__, __LINE__,ret);
    ReloadDocument();
    return;
  }

  CString text;
  text.Format(IDS_OPER_FAILED2, ret);
  AfxMessageBox(text);
  return;
}

void CPoseidonDoc::OnUpdateSccIntegUndoCheckOut(CCmdUI *pCmdUI)
{
  CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();  
  SCCSTAT _status = pMainFrame->GetSccFunctions().Status(GetPathName()); 

  pCmdUI->Enable( ((_status != SCC_STATUS_INVALID) && (_status & SCC_STATUS_CONTROLLED) && (_status & SCC_STATUS_CHECKEDOUT)));
}

void CPoseidonDoc::SetModifiedFlag(BOOL bModified)
{
  // during opening of the document the modification flag is set to true.
  // to avoid asking for check out i use flag _askForCheckOut
  if (_askForCheckOut && bModified && !IsModified())
  {  
    CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();  
    SCCSTAT _status = pMainFrame->GetSccFunctions().Status(GetPathName());

    if ((_status != SCC_STATUS_INVALID) && (_status & SCC_STATUS_CONTROLLED) && ((_status & SCC_STATUS_CHECKEDOUT) == 0))
    {
      if (AfxMessageBox(IDS_SCC_INTEG_DOC_CHANGE,MB_YESNO) == IDYES )      
        OnSccIntegCheckOut();            
    }
  }

  base::SetModifiedFlag(bModified);
}


void CPoseidonDoc::OnSccIntegGetLatest()
{
  UpdateSSStatus();
  if (_status != SCC_STATUS_INVALID && (_status & SCC_STATUS_CONTROLLED))
    return;

  if (IsModified() && AfxMessageBox(IDS_SCC_INTEG_LOST_CHANGES, MB_YESNO) == IDNO)
    return;

  CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
  MsSccFunctions& sccFunctions = pMainFrame->GetSccFunctions();

  SCCRTN ret = sccFunctions.GetLatestVersion(GetPathName());

  if (IS_SCC_SUCCESS(ret))
  {
    ReloadDocument();
    return;  
  }
  
  if (IS_SCC_WARNING(ret))
  {
    LogF("%s %d : MsSccFunctions warning %d", __FILE__, __LINE__,ret);
    ReloadDocument();
    return;  
  }    

  CString text;
  text.Format(IDS_OPER_FAILED2, ret);
  AfxMessageBox(text);
  return;
}

void CPoseidonDoc::OnUpdateSccIntegGetLatest(CCmdUI *pCmdUI)
{  
  pCmdUI->Enable(_status != SCC_STATUS_INVALID && (_status & SCC_STATUS_CONTROLLED));
}

// simple dialog for Check In Note
class CCommentDlg : public CDialog
{
  // Construction
public:
  CCommentDlg();

  enum { IDD = IDD_SCC_INTEG_COMMENT};
  CString m_Comment;


  // Overrides
protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support  

  // Implementation
protected:
  DECLARE_MESSAGE_MAP()
};

CCommentDlg::CCommentDlg() 
: CDialog(CCommentDlg::IDD)
{
}

void CCommentDlg::DoDataExchange(CDataExchange* pDX)
{   
  MDDX_Text(pDX, IDC_SCC_INTEG_COMMENT, m_Comment);
}

BEGIN_MESSAGE_MAP(CCommentDlg, CDialog)
END_MESSAGE_MAP()

void CPoseidonDoc::OnSccIntegCheckIn()
{
  UpdateSSStatus();

  if (_status == SCC_STATUS_INVALID || !(_status & SCC_STATUS_CONTROLLED) || !(_status & SCC_STATUS_CHECKEDOUT))
    return;

  CCommentDlg dlg; 
  if (dlg.DoModal() != IDOK)
    return;

  CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
  OnProjectSave();
  SCCRTN ret = pMainFrame->GetSccFunctions().CheckIn(GetPathName(),dlg.m_Comment);
  if (IS_SCC_SUCCESS(ret))  
    return;

  if (IS_SCC_WARNING(ret))
  {
    LogF("%s %d : MsSccFunctions warning %d", __FILE__, __LINE__,ret);   
    return;  
  }    

  CString text;
  text.Format(IDS_OPER_FAILED2, ret);
  AfxMessageBox(text);
  return;
}

void CPoseidonDoc::OnUpdateSccIntegCheckIn(CCmdUI *pCmdUI)
{ 
  pCmdUI->Enable( (_status != SCC_STATUS_INVALID) && (_status & SCC_STATUS_CONTROLLED) && (_status & SCC_STATUS_CHECKEDOUT));
}

void CPoseidonDoc::OnSccIntegAdd()
{
  UpdateSSStatus();

  if (_status == SCC_STATUS_INVALID  || (_status & SCC_STATUS_CONTROLLED))
    return;

  CCommentDlg dlg; 
  if (dlg.DoModal() != IDOK)
    return;

  CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd(); 
  OnProjectSave(); 

  SCCRTN ret = pMainFrame->GetSccFunctions().Add(GetPathName(), dlg.m_Comment);  
  if (IS_SCC_SUCCESS(ret))  
    return;

  if (IS_SCC_WARNING(ret))
  {
    LogF("%s %d : MsSccFunctions warning %d", __FILE__, __LINE__,ret);   
    return;  
  }    

  CString text;
  text.Format(IDS_OPER_FAILED2, ret);
  AfxMessageBox(text);
  return;
}

void CPoseidonDoc::OnUpdateSccIntegAdd(CCmdUI *pCmdUI)
{
  pCmdUI->Enable( _status != SCC_STATUS_INVALID  && !(_status & SCC_STATUS_CONTROLLED));
}

void CPoseidonDoc::OnSccIntegGetLatestAll()
{
  UpdateSSStatus();

  if (_status == SCC_STATUS_INVALID  || !(_status & SCC_STATUS_CONTROLLED))
    return; 

  if (IsModified() && AfxMessageBox(IDS_SCC_INTEG_LOST_CHANGES, MB_YESNO) == IDNO)
    return;

  CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
  MsSccFunctions& sccFunctions = pMainFrame->GetSccFunctions();

  SCCRTN ret = sccFunctions.GetLatestVersion(GetPathName());

  if (IS_SCC_WARNING(ret))
  {
    LogF("%s %d : MsSccFunctions warning %d", __FILE__, __LINE__,ret);      
  }

  if (IS_SCC_ERROR(ret))
  {      
    CString text;
    text.Format(IDS_OPER_FAILED2, ret);
    AfxMessageBox(text);
    ReloadDocument(TRUE);
    return;
  }

  //  get latest version templates
  for(int i = 0; i < m_ObjectTemplates.Size(); i++)
  {
    Ref<CPosObjectTemplate> templ = m_ObjectTemplates[i];
    if (templ.IsNull())
      continue;

    CString path;
    templ->GetFullFileName(path);

    if (sccFunctions.UnderSSControl(path))
    {

      SCCRTN ret = sccFunctions.GetLatestVersion(path);

      if (IS_SCC_WARNING(ret))
      {
        LogF("%s %d : MsSccFunctions warning %d", __FILE__, __LINE__,ret);      
      }

      if (IS_SCC_ERROR(ret))
      {      
        CString text;
        text.Format(IDS_OPER_FAILED2, ret);
        AfxMessageBox(text);
        ReloadDocument(TRUE);
        return;
      }
    }      
  }

  //  get latest version templates
  for(int i = 0; i < m_Textures.Size(); i++)
  {
    Ref<CPosTextureTemplate> templ = m_Textures[i];
    if (templ.IsNull())
      continue;

    CString path;
    templ->GetFullTextureFileName(path);

    if (sccFunctions.UnderSSControl(path))
    {
      SCCRTN ret = sccFunctions.GetLatestVersion(path);

      if (IS_SCC_WARNING(ret))
      {
        LogF("%s %d : MsSccFunctions warning %d", __FILE__, __LINE__,ret);      
      }

      if (IS_SCC_ERROR(ret))
      {      
        CString text;
        text.Format(IDS_OPER_FAILED2, ret);
        AfxMessageBox(text);
        ReloadDocument(TRUE);
        return;
      }
    }

    if (templ->m_nTxtrType != CPosTextureTemplate::txtrTypeSecundar)
    {    
      templ->GetFullBimPasFileName(path);
      if (sccFunctions.UnderSSControl(path))
      {
        SCCRTN ret = sccFunctions.GetLatestVersion(path);

        if (IS_SCC_WARNING(ret))
        {
          LogF("%s %d : MsSccFunctions warning %d", __FILE__, __LINE__,ret);      
        }

        if (IS_SCC_ERROR(ret))
        {      
          CString text;
          text.Format(IDS_OPER_FAILED2, ret);
          AfxMessageBox(text);
          ReloadDocument(TRUE);
          return;
        }
      }

      templ->GetFullRvMatFileName(path);
      if (sccFunctions.UnderSSControl(path))
      {
        SCCRTN ret = sccFunctions.GetLatestVersion(path);

        if (IS_SCC_WARNING(ret))
        {
          LogF("%s %d : MsSccFunctions warning %d", __FILE__, __LINE__,ret);      
        }

        if (IS_SCC_ERROR(ret))
        {      
          CString text;
          text.Format(IDS_OPER_FAILED2, ret);
          AfxMessageBox(text);
          ReloadDocument(TRUE);
          return;
        }
      }
    }
  }
  
  ReloadDocument(TRUE);
}

void CPoseidonDoc::OnUpdateSccIntegGetLatestAll(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_status != SCC_STATUS_INVALID && (_status & SCC_STATUS_CONTROLLED));
}

void CPoseidonDoc::UpdateSSStatus(LPCTSTR lpszPathName)
{
  CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
  MsSccFunctions& sccFunctions = pMainFrame->GetSccFunctions();
  if (lpszPathName)
    _status = sccFunctions.Status(lpszPathName);
  else
    _status = sccFunctions.Status(GetPathName());
}

void CPoseidonDoc::OnRenameDoc()
{

  CString newFileName; 
  CutExtFromFileName( newFileName, GetPathName());
  newFileName += _T(".pew");
  SetPathName(newFileName);
  UpdateSSStatus();
  SetModifiedFlag();

  //SeparateNameFromFileName(newFileName, newFileName);
  //SetTitle(newFileName);

}

//////////////////////////////////////////////////////////////////////////
// Satellite imagery handling

/// load vector (3 floats) from a config file

static bool GetValue(Vector3 &val, ParamEntryPar entry)
{
  if (entry.GetSize() != 3)
  {
    val = VZero;
    return false;
  }
  val = Vector3(entry[0].GetFloat(), entry[1].GetFloat(), entry[2].GetFloat());
  return true;
}

/// load 4x3 matrix from a config file

static void LoadMatrix4(Matrix4& matrix, ParamEntryPar entry)
{
  Vector3 val;
  GetValue(val, entry >> "aside");
  matrix.SetDirectionAside(val);

  GetValue(val, entry >> "up");
  matrix.SetDirectionUp(val);

  GetValue(val, entry >> "dir");
  matrix.SetDirection(val);

  GetValue(val, entry >> "pos");
  matrix.SetPosition(val);
}

/// load 4 floats (color) from a config file
static void LoadColor(float* color, ParamEntryPar entry)
{
  for (int i = 0; i < 4; ++i)
  {
    color[i] = entry[i];
  }
}

/// load 4 floats (color) from array file
static void LoadColor(int* color, const IParamArrayValue& value)
{
  for (int i = 0; i < 3; ++i) color[i] = value[i];
}

/// convert relative in-project path to absolute
static RString AbsoluteFilePath(CPosEdBaseDoc* doc, const char* relative, CPosEdBaseDoc::ProjDir base = CPosEdBaseDoc::dirRoot)
{
  CString projDir;
  doc->GetProjectDirectory(projDir, base);
  return RString(projDir) + relative;
}

static CString RelativeFilePath(CPosEdBaseDoc* doc, CString absolute, CPosEdBaseDoc::ProjDir root = CPosEdBaseDoc::dirRoot)
{
  // we want it relative to the project dir?
  CString projDir;
  doc->GetProjectDirectory(projDir, root);
  
  return absolute.Mid(projDir.GetLength());
}

void CCommonLayer::Load(CPosEdBaseDoc *doc, ParamEntryPar entry, QFileTime &newest)
{
  RString material = entry>>"material";
  ParamFile rvMat;
  // convert part to absolute one
  RString absMaterial = AbsoluteFilePath(doc,material);
  rvMat.ParseBinOrTxt(absMaterial);
  
  QFileTime timeStamp = QIFileFunctions::TimeStamp(absMaterial);
  if (timeStamp>newest) newest = timeStamp;
  
  ParamEntryVal stage1 = rvMat>>"Stage1"; // no
  ParamEntryVal stage2 = rvMat>>"Stage2"; // dt
  LoadMatrix4(noTexgen,stage1>>"uvTransform");
  
  LoadMatrix4(dtTexgen,stage2>>"uvTransform");
  
  ConstParamEntryPtr stage0 = rvMat.FindEntry("Stage0");
  if (stage0)
  {
    LoadMatrix4(txTexgen,stage2>>"uvTransform");
  }
  else
  {
    txTexgen = M4Identity;
  }
  LoadColor(ambient,rvMat>>"ambient");
  LoadColor(diffuse,rvMat>>"diffuse");
  LoadColor(forcedDiffuse,rvMat>>"forcedDiffuse");
  // note: typo in rvmat name, emmisive instead of emissive
  LoadColor(emissive,rvMat>>"emmisive");
}

void CMaskedLayer::Load(CPosEdBaseDoc *doc, ParamEntryPar entry, QFileTime &newest)
{
  name = entry.GetName();

  tx = entry>>"texture";
  
  RString material = entry>>"material";
  ParamFile rvMat;
  // convert part to absolute one
  RString absMaterial = AbsoluteFilePath(doc,material);
  rvMat.ParseBinOrTxt(absMaterial);
  
  QFileTime timeStamp = QIFileFunctions::TimeStamp(absMaterial);
  if (timeStamp>newest) newest = timeStamp;
  
  ParamEntryVal stage1 = rvMat>>"Stage1"; // no
  ParamEntryVal stage2 = rvMat>>"Stage2"; // dt
  no = stage1>>"texture";
  
  dt = stage2>>"texture";
}


/// information about one mask pixel - transformed from legend+mask representation
struct LayerBlendItem
{
  /// layer 0 index
  short l0;
  /// layer 1 index
  short l1;
  /// amount of layer1
  float a1;
  
  LayerBlendItem(){}
  explicit LayerBlendItem(int layer)
  {
    l0 = l1 = layer;
    a1 = 0;
  }
  
  LayerBlendItem Lerp(float factor, const LayerBlendItem &with) const;
};

TypeIsSimple(LayerBlendItem)

LayerBlendItem LayerBlendItem::Lerp(float factor, const LayerBlendItem &with) const
{
  // check if both blends are actually pure
  if (a1==0 && with.a1==0)
  {
    LayerBlendItem ret;
    ret.l0 = l0;
    ret.l1 = with.l0;
    ret.a1 = factor;
    return ret;
  }
  // TODO: compute all four coefs, select two most important and normalize them
  if (factor>0.5)
  {
    return with;
  }
  else
  {
    return *this;
  }
}


/// representation of legend pure colors
struct LegendPureItem
{
  /// item color
  IPictureFormat::Pixel color;
  /// representation as a blend
  LayerBlendItem blend;
};

TypeIsSimple(LegendPureItem)

/// layer legend representation
/** Cf. Wiki Landscape Surface Editing */
class LayerLegend
{
  /// first line of the legend file
  SRefArray<IPictureFormat::Pixel> _line;
  /// conversion from _line to LayerBlendItem
  SRefArray<LayerBlendItem> _lineBlendItem;
  /// _line and _lineBlendItem size
  int _lineWidth;
  /// pure colors from the legend
  AutoArray<LegendPureItem> _pure;

  /// find nearest pure legend color for the pixel
  int FindPure(IPictureFormat::Pixel pixel, int mask=0xffffff) const;
  /// initialize direct array
  void InitDirect();
  
  /// direct conversion from color to LayerBlendItem
  AutoArray<LayerBlendItem> _direct;
  
  /**
  32 is for 5 bits, 64 for 6 bits
  32 is 1/4 MB, 64 is 2 MB
  */
  enum
  {
    DirectDimLog=5, 
    DirectDim=1<<DirectDimLog,
    DirectDimMask=DirectDim-1
  }; 
  
  public:
  LayerLegend(
    const IPictureFormat::Pixel *legend, int width, ParamEntryVal cfg,
    const Array<CMaskedLayer> &layers
  );
  
  /// convert a single pixel to blending information
  const LayerBlendItem &ConvertToBlend(IPictureFormat::Pixel pixel, int prec=0) const;

  /// optimized conversion to blending information
  const LayerBlendItem &ConvertToBlendOpt(IPictureFormat::Pixel pixel) const
  {
    int r = (pixel>>(16+8-DirectDimLog))&DirectDimMask;
    int g = (pixel>>(8+8-DirectDimLog))&DirectDimMask;
    int b = (pixel>>(0+8-DirectDimLog))&DirectDimMask;
    int index = (r<<(DirectDimLog*2))|(g<<DirectDimLog)|b;
    const LayerBlendItem &ret = _direct[index];
    #if 0 // _DEBUG
      const LayerBlendItem &check = ConvertToBlend(pixel,0xf8f8f8);
      // check if the values are close enough
      static float checkThold = 0.01f;
      if (
        ret.l0!=check.l0 || ret.l1!=check.l1 ||
        fabs(ret.a1-check.a1)>checkThold
      )
      {
        __asm nop;
      }
    #endif
    return ret;
  }
  
};

static int Distance2(IPictureFormat::Pixel p0, IPictureFormat::Pixel p1)
{
  int r = ((p0>>16)&0xff)-((p1>>16)&0xff);
  int g = ((p0>>8)&0xff)-((p1>>8)&0xff);
  int b = ((p0>>0)&0xff)-((p1>>0)&0xff);
  return (r*r+g*g+b*b);
}

LayerLegend::LayerLegend(
  const IPictureFormat::Pixel *legend, int width, ParamEntryVal cfg,
  const Array<CMaskedLayer> &layers
)
{
  //PROFILE_SCOPE_EX(legCv,*);
  // make sure each color is represented only once
  if (width>0)
  {
    AutoArray<IPictureFormat::Pixel> line;
    line.Realloc(width);
    line.Add(legend[0]);
    IPictureFormat::Pixel last = legend[0];
    for (int i=1; i<width; i++)
    {
      IPictureFormat::Pixel curr = legend[i];
      if (last==curr) continue;
      line.Add(curr);
      last = curr;
    }
    _line.Realloc(line.Size());
    _lineWidth = line.Size();
    for (int i=0; i<line.Size(); i++)
    {
      _line[i] = line[i];
    }
  }
  else
  {
    Fail("Empty legend");
    _lineWidth = 0;
  }
  
  ParamEntryVal colors = cfg>>"Colors";
  for (ParamClass::Iterator<> it = &colors; it; ++it)
  {
    ParamEntryVal entry = *it;
    if (!entry.IsArray()) continue;
    
    // check if surface type is known
    RStringB surface = entry.GetName();
    int index = -1;
    for (int i=0; i<layers.Size(); i++)
    {
      if (!_strcmpi(layers[i].name,surface))
      {
        index = i;
        break;
      }
    }
    if (index<0)
    {
      LogF("Surface %s not defined",cc_cast(surface));
      continue;
    }
    
    // entry is array of arrays
    // each sub-array represents one color
    for (int a=0; a<entry.GetSize(); a++)
    {
      int color[3];
      LoadColor(color,entry[a]);
      IPictureFormat::Pixel pixel = (color[0]<<16)|(color[1]<<8)|(color[2]<<0);
      LegendPureItem &item = _pure.Append();
      item.color = pixel;
      item.blend = LayerBlendItem(index);
    }
    
  }

  // create a conversion from blending to pure colors
  // find pure colors
  AutoArray<int> pureIndex;
  AutoArray<int> purePos;
  for (int b=0; b<_lineWidth; b++)
  {
    IPictureFormat::Pixel pixel = _line[b];
    int pure = FindPure(pixel);
    if (pure<0) continue;
    pureIndex.Add(pure);
    purePos.Add(b);
  }
  
  _lineBlendItem.Realloc(_lineWidth);
  if (purePos.Size()<=0)
  {
    // fall-back solution - for each color select the nearest pure color
    LogF("No pure colors found");
    if (_pure.Size()<=0)
    {
      Fail("No pure colors defined");
      return;
    }
    for (int b=0; b<_lineWidth; b++)
    {
      IPictureFormat::Pixel pixel = _line[b];

      int nearest = 0;
      int nearestDist2 = INT_MAX;
      for (int i=0; i<_pure.Size(); i++)
      {
        int dist2 = Distance2(_pure[i].color,pixel);
        if (nearestDist2>dist2)
        {
          nearestDist2 = dist2;
          nearest = i;
        }
      }

      LayerBlendItem &blend = _lineBlendItem[b];
      blend = _pure[nearest].blend;
    }
    InitDirect();
    return;
  }
  // make sure some pure color represents first and last pixel
  for (int b=0; b<purePos[0]; b++)
  {
    _lineBlendItem[b] = _pure[pureIndex[0]].blend;
  }
  for (int b=purePos[purePos.Size()-1]; b<_lineWidth; b++)
  {
    _lineBlendItem[b] = _pure[pureIndex[purePos.Size()-1]].blend;
  }
  // fill gradients between pure colors
  for (int i=0; i<purePos.Size()-1; i++)
  {
    int posBeg = purePos[i];
    int posEnd = purePos[i+1];
    int indexBeg = pureIndex[i];
    int indexEnd = pureIndex[i+1];
    const LayerBlendItem &blendBeg = _pure[indexBeg].blend;
    const LayerBlendItem &blendEnd = _pure[indexEnd].blend;
    _lineBlendItem[posBeg] = blendBeg;
    _lineBlendItem[posEnd] = blendEnd;
    
    for (int b=posBeg+1; b<posEnd; b++)
    {
      float endBegFactor = float(b-posBeg)*(1/float(posEnd-posBeg));
      LayerBlendItem &item = _lineBlendItem[b];
      item = blendBeg.Lerp(endBegFactor,blendEnd);
    }
  }
  InitDirect();
}

int LayerLegend::FindPure(IPictureFormat::Pixel pixel, int mask) const
{
  // ignore alpha or user given bits
  pixel &= mask;
  for (int i=0; i<_pure.Size(); i++)
  {
    if ((_pure[i].color&mask)==pixel)
    {
      return i;
    }
  }
  return -1;
}

void LayerLegend::InitDirect()
{
  _direct.Realloc(DirectDim*DirectDim*DirectDim);
  _direct.Resize(DirectDim*DirectDim*DirectDim);
  for (int r=0; r<DirectDim; r++)
  for (int g=0; g<DirectDim; g++)
  for (int b=0; b<DirectDim; b++)
  {
    int index = (r<<(DirectDimLog*2))|(g<<DirectDimLog)|b;
    int rx = toInt(r*(255.0f/(DirectDim-1)));
    int gx = toInt(g*(255.0f/(DirectDim-1)));
    int bx = toInt(b*(255.0f/(DirectDim-1)));
    IPictureFormat::Pixel pixel = (rx<<16)|(gx<<8)|bx;
    _direct[index] = ConvertToBlend(pixel,0xf8f8f8);
    
  }
}

/**
@param prec mask for comparison with pure colors
  used when we know source has lower precision and we effectively want to compare in less bits
*/
const LayerBlendItem &LayerLegend::ConvertToBlend(IPictureFormat::Pixel pixel, int prec) const
{
  int pure = FindPure(pixel,prec);
  if (pure>=0)
  {
    return _pure[pure].blend;
  }
  int nearest = 0;
  int nearestDist2 = INT_MAX;
  // blended color - find position in the mask
  for (int i=0; i<_lineWidth; i++)
  {
    int dist2 = Distance2(_line[i],pixel);
    if (nearestDist2>dist2)
    {
      nearestDist2 = dist2;
      nearest = i;
      if (dist2<=0) break;
    }
  }
  
  // step 2. - convert legend color into a 2-color blend
  return _lineBlendItem[nearest];
}

// calculate layer usage histogram
struct LayerUsage
{
  float usage;
  int layer;
  
  static int CompareUsage(const LayerUsage *l1, const LayerUsage *l2)
  {
    int ret = sign(l2->usage-l1->usage);
    if (ret) return ret;
    // we want the results to be stable and deterministic
    return l1->layer - l2->layer;
  }
  static int CompareLayer(const LayerUsage *l1, const LayerUsage *l2)
  {
    return l1->layer - l2->layer;
  }
};
TypeIsSimple(LayerUsage);


/// container to simplify picture memory management
struct PictureData
{
  SRef<PNGFormatLoader> _loader;
  
  int _w,_h;
  AutoArray< SRefArray<IPictureFormat::Pixel> > _rows;
  /// line number which is about to be read
  int _currRead;
  /// line number which is about to be released
  int _currFree;

  PictureData()
  {
    _w = _h = 0;
    _currFree = 0;
    _currRead = 0;
  }
  
  /// load whole PNG at once, not using the loader
  int LoadPNG(const char *name)
  {
    int ret = GFormatPNG->LoadRows(name,_w,_rows);
    _h = _rows.Size();
    _currFree = 0;
    _currRead = _h;
    return ret;
  }
  
  int StartLoadPNG(const char *name)
  {
    _loader = new PNGFormatLoader(name);
    if (_loader->LoadHeader()<0) return -1;
    if (setjmp(_loader->GetJmpbuf()))
    {
      // any error goes here
      return -1;
    }
    _loader->LoadDimensions(_w,_h);
    _rows.Realloc(_h);
    _rows.Resize(_h);
    return 0;
  }
  
  int ReadRows(int toRead, int toFree)
  {
    if (setjmp(_loader->GetJmpbuf()))
    {
      //throw CException
      return -1;
    }
    // ignore requests out of bounds
    if (toRead>_h) toRead = _h;
    if (toFree<0) toFree = 0;
    if (toFree>_currRead)
    {
      Fail("PictureData::ReadRows: Request to free data which are not loaded yet");
      toFree=_currRead;
    }
    // read from _currRead to toRead
    if (toFree>_currFree)
    {
      for (int line=_currFree; line<toFree; line++)
      {
        _rows[line].Free();
      }
      _currFree = toFree;
    }
    if (toRead>_currRead)
    {
      for (int line=_currRead; line<toRead; line++)
      {
        _rows[line] = new IPictureFormat::Pixel[_w];
        _loader->ReadRow((png_byte *)_rows[line].Data());
      }
      _currRead = toRead;
    }
    return 0;
  }
  
  int ReadEnd()
  {
    if (setjmp(_loader->GetJmpbuf()))
    {
      //throw CException
      return -1;
    }
    _loader->ReadEnd();
    return 0;
  }
  
  void Free()
  {
    _w = _h = 0;
    _rows.Clear();
    _loader.Free();
  }
  
  IPictureFormat::Pixel GetClamped(int x, int y) const
  {
    saturate(x,0,_w-1);
    saturate(y,0,_h-1);
    return _rows[y][x];
  }
  const IPictureFormat::Pixel *GetRowData(int y) const {return _rows[y];}
  /*
  IPictureFormat::Pixel GetWrapped(int x, int y) const
  {
    x %= _w;
    y %= _h;
    return _data[_w*y+x];
  }
  */
};


static bool ComparePNG(const Array2D<IPictureFormat::Pixel> &data, const char *path)
{
  bool changed = true;
  {// check if file was changed and needs to be saved
    PictureData compare;
    // if there is any error, assume it has changed
    if (
      compare.StartLoadPNG(path)>=0 &&
      compare._w==data.GetXRange() && compare._h==data.GetYRange()
    )
    {
      changed = false;
      for (int i=0; i<compare._h; i++)
      {
        if (compare.ReadRows(i+1,i)<0)
        {
          changed = true;
          break;
        }
        // compare row i with sat row i
        const IPictureFormat::Pixel *old = compare.GetRowData(i);
        const IPictureFormat::Pixel *cur = data.Data1D()+i*data.GetXRange();
        if (memcmp(old,cur,sizeof(*old)*compare._w))
        {
          changed = true;
          break;
        }
      }
      if (compare.ReadEnd()<0)
      {
        changed = true;
      }
    }
  }
  return changed;
}
static inline bool CheckInteger(float x)
{
  return fabs(x-toInt(x))<1e-6f;
}

/// description of a grid properties in the global space
struct SegmentGrid
{
  //@{ active grid area
  int _wSegment;
  int _hSegment;
  //@}
  //@{ texture dimensions (includes borders)
  int _wSegTex;
  int _hSegTex;
  //@}
  //@( placement of the active grid in the texture
  int _xOffset;
  int _zOffset;
  //@}
  
  bool Init(
    int w, int h, float gridInWSize, float gridInHSize
  )
  {
    float wSegmentExact = w*gridInWSize;
    float hSegmentExact = h*gridInHSize;
    
    if (!CheckInteger(wSegmentExact) || !CheckInteger(hSegmentExact))
    {
      return false;
    }
    
    // it might be possible to handle not integer values, but mapping would be more intricate
    _wSegment = toInt(wSegmentExact);
    _hSegment = toInt(hSegmentExact);
    // round up to the nearest power of 2
    _wSegTex = roundTo2PowerNCeil(_wSegment);
    _hSegTex = roundTo2PowerNCeil(_hSegment);
    // calculate offsets needed for borders
    _xOffset = (_wSegTex-_wSegment)/2;
    _zOffset = (_hSegTex-_hSegment)/2;
    return true;
  }
};


/// helper for conversion from ARGB into lerp form
static inline float LayerDiv(float layer, float covered)
{
  if (covered<=0)
  {
    Assert(layer<=0);
    return 0;
  }
  return layer/covered;
}


#if 0 //_DEBUG
#define DEBUG_MASK 1
#define DEBUG_MASK_X 24
#define DEBUG_MASK_Z 21

#pragma optimize ("",off)
#endif

#if ARMA2_LEVEL
const int NLayers = 6;
#else
const int NLayers = 4;
#endif

/// direct representation - each layer represented by one 8b value
struct ARGBMask
{
  unsigned char argb[NLayers];

  unsigned char operator[] (int i) const {return argb[i];}
  unsigned char &operator[] (int i) {return argb[i];}
};

TypeIsSimple(ARGBMask)
/**
@param x position of current square in the argbMask
@param z position of current square in the argbMask
@return index of most important layer in the usedLayers array
*/
static int SelectUsedLayers(
  AutoArray<int> &usedLayers, const AutoArray<int> &layerList,
  int xBeg, int zBeg, int xSize, int zSize,
  int xBegAct, int zBegAct, int xSizeAct, int zSizeAct,
  const Array2D<ARGBMask> &argbMask
)
{
  // record which layers are used at all
  usedLayers.Resize(layerList.Size());
  for (int i=0; i<usedLayers.Size(); i++) usedLayers[i] = -1;

  // layer usage histogram
  AutoArray< int, MemAllocStack<int,16> > usage;
  usage.Resize(layerList.Size());
  for (int i=0; i<usage.Size(); i++) usage[i]=0;

  for (int zz=zBeg; zz<zBeg+zSize; zz++)
  for (int xx=xBeg; xx<xBeg+xSize; xx++)
  {
    ARGBMask argb = argbMask(xx,zz);
    // argbMask is using non-lerp representation
    for (int i=0; i<NLayers; i++)
    {
      int c = argb[i];
      if (c>0 && layerList.Size()>i) usedLayers[i] = layerList[i], usage[i] += c;
    }
  }

  // find most important layer
  int major = 0;
  int majorUsage = 0;
  for (int i=0; i<usage.Size(); i++)
  {
    if (majorUsage<usage[i])
    {
      majorUsage = usage[i];
      major = i;
    }
  }
  return layerList[major];
}


void CPoseidonDoc::OnToolsImportSatellite()
{
  Ref<CTextureLayer> actualLayer = m_PoseidonMap.GetActiveTextureLayer();
  
  // check if import is possible
  if (m_satelliteGridG%actualLayer->m_nTexSizeInSq)
  {
    CString msg;
    CString satGrid;
    CString texGrid;
    satGrid.Format("%d", m_satelliteGridG);
    texGrid.Format("%d", actualLayer->m_nTexSizeInSq);
    AfxFormatString2(msg, IDS_ERROR_SAT_GRID, satGrid, texGrid);
    AfxMessageBox(msg);
    return;
  }

  CMainFrame* thisWindow = (CMainFrame*)AfxGetMainWnd();
  
  // first as for a layer config - it acts as a kind of a "project" file
  static const char cfgFilter[] = "Configuration files (*.cfg)|*.cfg|All Files (*.*)|*.*||";
  CFileDialog askFileCfg(true, ".cfg", "", 
               OFN_HIDEREADONLY | OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST,
               cfgFilter,thisWindow);
  
  CString title;
  // ask user for a layer config file
  if (title.LoadString(IDS_SEL_LAYER_CONFIG))
  {
    askFileCfg.GetOFN().lpstrTitle = title;
  }

  if (askFileCfg.DoModal() != IDOK) return;
  
  CString layerConfigPath = askFileCfg.GetPathName();
  
  // parse layer config file
  ParamFile layerCfg;
  layerCfg.ParseBinOrTxt(layerConfigPath);
  
  if (layerCfg.GetEntryCount()<=0)
  {
    LogF("Error loading '%s'", cc_cast(layerConfigPath));
    CString msg;
    AfxFormatString1(msg, IDS_ERROR_LOAD, layerConfigPath);
    AfxMessageBox(msg);
    return;
  }

  CString satellitePath;
  CString layerMaskPath;
  
  if (layerCfg.FindEntry("satelliteMap"))
  {
    RString name = layerCfg>>"satelliteMap";
    
    BString<1024> fullPath;
    strcpy(fullPath,layerConfigPath);
    strcpy(GetFilenameExt(fullPath),name);
    
    satellitePath = fullPath;
  }

  if (layerCfg.FindEntry("layerMask"))
  {
    RString name = layerCfg>>"layerMask";
    
    BString<1024> fullPath;
    strcpy(fullPath,layerConfigPath);
    strcpy(GetFilenameExt(fullPath),name);
    
    layerMaskPath = fullPath;
  }
  
  // before converting, ask whether the user wants to save rvmat files in text or in binary format.
  // rvmatDlg.m_Choice = 0 -> text
  // rvmatDlg.m_Choice = 1 -> binary

  bool rvmatSaveFileFormat;
  
  if (layerCfg.FindEntry("rvmatBinary"))
  {
    rvmatSaveFileFormat = layerCfg>>"rvmatBinary";
  }
  else
  {
    CDlgRvmatSelection rvmatDlg;

    // default choice: binary
    rvmatDlg.m_Choice = 1;
    if (rvmatDlg.DoModal() != IDOK)
    {
      return;
    }

    rvmatSaveFileFormat = (rvmatDlg.m_Choice == 1);
  }

  // import satellite + layer mask, create bimpas from them
  // ask for satellite image path

  // ask for layer mask path
  
  // Step 1 - one big texture for whole landscape, paa name given

  // if there is sat or mask written in the layers.cfg, do not ask for it
  static const char texFilter[] = "Satellite maps (*.png)|*.png|All Files (*.*)|*.*||";
  static const char maskFilter[] = "Layer mask files (*.png)|*.png|All Files (*.*)|*.*||";
  CFileDialog askFileTex(true, ".png", "", 
               OFN_HIDEREADONLY | OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST,
               texFilter, thisWindow);
  CFileDialog askFileMask(true, ".png", "", 
              OFN_HIDEREADONLY | OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST, 
              maskFilter,thisWindow);

  // ask user for a satellite map
  if (satellitePath.IsEmpty())
  {
    if (title.LoadString(IDS_SEL_SATELLITE))
    {
      askFileTex.GetOFN().lpstrTitle = title;
    }
    
    if (askFileTex.DoModal() != IDOK) return;

    satellitePath = askFileTex.GetPathName();
  
  }

  if (layerMaskPath.IsEmpty())
  {
    // ask user for a layer mask map
    if (title.LoadString(IDS_SEL_LAYER_MASK))
    {
      askFileMask.GetOFN().lpstrTitle = title;
    }
    
    if (askFileMask.DoModal() != IDOK) return;

    layerMaskPath = askFileMask.GetPathName();
  }

  // start BUSY indication + progress
  CProgressDialog progress(thisWindow);
  progress.Create();
  //UNITPOS size = m_PoseidonMap.GetSize();
  progress.SetPos(0);

  
  CString opText;
  opText.LoadString(IDS_PROGRESS_SAT);
  progress.SetWindowText(opText);
  
  class WaitScope : private NoCopy
  {
    CWinApp* _app;
    public:
    WaitScope(CWinApp* app)
    : _app(app)
    {
      _app->BeginWaitCursor();
    }
    ~WaitScope()
    {
      _app->EndWaitCursor();
    }
  };
  
  WaitScope wait(AfxGetApp());
    
  
  actualLayer->m_layers.Clear();
  actualLayer->m_layersTimeStamp = QIFileFunctions::TimeStamp(layerConfigPath);
  
  bool once = true;
  // load layers
  ParamEntryVal layers = layerCfg >> "Layers";
  for (ParamClass::Iterator<> it = &layers; it; ++it)
  {
    ParamEntryVal entry = *it;
    if (!entry.IsClass()) continue;
    
    CMaskedLayer& newLayer = actualLayer->m_layers.Append();
    newLayer.Load(this, entry, actualLayer->m_layersTimeStamp);
    if (once)
    {
      actualLayer->m_commonLayer.Load(this, entry, actualLayer->m_layersTimeStamp);
      once = false;
    }
  }
  
  // done for individual parts:
  // convert colored mask+legend into a RGBA mask
  ParamEntryVal legendCfg = layerCfg >> "Legend";
  RString legendFile = legendCfg >> "picture";
  RString absLegendFile = AbsoluteFilePath(this, legendFile);
  // load legend file
  PictureData legendPic;
  if (legendPic.LoadPNG(absLegendFile) < 0)
  {
    LogF("Error loading '%s'", cc_cast(absLegendFile));
    CString msg;
    AfxFormatString1(msg, IDS_ERROR_LOAD, absLegendFile);
    AfxMessageBox(msg);
    return;
  }
  // load mask file
  // create a pattern for satellite split files
  // add a _%03d_%03d suffix to the name
  CString relSat = RelativeFilePath(this, satellitePath, CPosEdBaseDoc::dirText);
  BString<1024> satPattern;
  strcpy(satPattern, relSat);
  
  BString<1024> layeredSatPattern;
  LString satFilename = GetFilenameExt(satPattern);
  
  const char* satType = strrchr(satFilename, '_');
  if (!satType) satType = "_lco.png";
  strcpy(layeredSatPattern, satPattern);  
  LString layeredSatName = GetFilenameExt(layeredSatPattern);
  sprintf(layeredSatName, "Layers\\S_%%03d_%%03d%s", satType);
  RString absSatPattern = AbsoluteFilePath(this, layeredSatName, CPosEdBaseDoc::dirText);

  CString relMask = RelativeFilePath(this, layerMaskPath, CPosEdBaseDoc::dirText);
  BString<1024> maskPattern;
  strcpy(maskPattern, relMask);
  BString<1024> layeredMaskPattern;
  LString maskFilename = GetFilenameExt(maskPattern);
  #if ARMA2_LEVEL
    // only one file format supported for the mask in ArmA2
    // needed to support 6 layers
    const char *maskType="_lca.png";
  #else
    // for ArmA we keep the type as given on the command line
    const char* maskType = strrchr(maskFilename, '_');
    if (!maskType) maskType = "_lco.png";
  #endif
  strcpy(layeredMaskPattern, maskPattern);  
  LString layeredMaskName = GetFilenameExt(layeredMaskPattern);
  sprintf(layeredMaskName, "Layers\\M_%%03d_%%03d%s", maskType);
  
  RString absMaskPattern = AbsoluteFilePath(this, layeredMaskName, CPosEdBaseDoc::dirText);
  
  int layerStep   = actualLayer->m_nTexSizeInSq;
  int layerWidth  = actualLayer->m_nTexWidth * layerStep;
  int layerHeight = actualLayer->m_nTexHeight * layerStep;
  Assert(layerWidth == m_PoseidonMap.GetSize().x);
  Assert(layerHeight == m_PoseidonMap.GetSize().z);
  
  // split satellite image based on the pattern
  //  float realSizeX = m_nRealSizeUnit*layerWidth;
  //  float realSizeZ = m_nRealSizeUnit*layerHeight;
  //  
  //  float satGrid = m_satelliteGridG*m_nRealSizeUnit;
  
  // satGrid/realSizeX
  // what part of the total size does one grid represent?
  float gridInWSize = float(m_satelliteGridG) /  float(layerWidth);
  float gridInHSize = float(m_satelliteGridG) /  float(layerHeight);

#if _ENABLE_PERFLOG
    GPerfCounters.Enable();
    GPerfProfilers.Enable();
#endif

  // sat. segment needs to be some accurate multiply of the landscape grid size
  int wSegmentGrid = m_satelliteGridG;
  int hSegmentGrid = m_satelliteGridG;
  
  int wCount = (layerWidth + wSegmentGrid - 1) / wSegmentGrid;
  int hCount = (layerHeight + hSegmentGrid - 1) / hSegmentGrid;

  progress.SetRange(wCount * hCount * 2);
  
  // convert absolute to relative
  CString relSatPattern  = RelativeFilePath(this, cc_cast(absSatPattern), CPosEdBaseDoc::dirText);
  CString relMaskPattern = RelativeFilePath(this, cc_cast(absMaskPattern), CPosEdBaseDoc::dirText);
  
  actualLayer->m_satellitePattern = cc_cast(relSatPattern);
  actualLayer->m_layerMaskPattern = cc_cast(relMaskPattern);
  
  // create Layers folder
  RString absLayers = AbsoluteFilePath(this, "Layers", CPosEdBaseDoc::dirText);

  CreateDirectory(absLayers,NULL);

#if !DEBUG_MASK
  
  // delete everything from "Layers" folder before generating
  CStringArray	filesToDelete;
  WIN32_FIND_DATA	findData;
  
  RString absLayersBase = absLayers + "\\";
  HANDLE hFind = FindFirstFile(absLayersBase + "*.*", &findData);
  if (hFind != INVALID_HANDLE_VALUE)
  {
    do
    {
      if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) continue;
      char letter1 = toupper(findData.cFileName[0]);
      char letter2 = toupper(findData.cFileName[1]);
      // we know files starting with S_, M_, P_ (Satellite, Mask, Pass)
      //if (letter1!='P' && letter1!='M' && letter1!='S' || letter2!='_')
      // currently we need to always recreate materials, as we do not know if source is the same
      if (letter1 != 'M' && letter1 != 'S' || letter2 != '_')
      {
        RString filename = absLayersBase + findData.cFileName;
        DeleteFile(filename);
      }
    } 
    while(FindNextFile(hFind, &findData));
    FindClose(hFind);
  }
#endif
  
  SegmentGrid sat;
  
  struct Timing
  {
    #if _ENABLE_PERFLOG
    DWORD _start;
    DWORD _last;
    #endif
    
    Timing()
    {
  #if _ENABLE_PERFLOG
      _last = _start = GetTickCount();
      LogF("*** Timing started");
  #endif
    }
    
    void operator << (const char* name)
    {
  #if _ENABLE_PERFLOG
      DWORD now = GetTickCount();
      LogF("%s: time %d ms (%.1f s)", name, now - _last, (now - _start) * 0.001f);
      _last = now;
  #endif
    }
  } timing;
  
  // avoid allocating satellite and mask at once - data are huge
#if !DEBUG_MASK && !_DEBUG
  {
    //PROFILE_SCOPE_EX(satGn,*);

      // load the satellite image        
    PictureData satPic;
    {
      //PROFILE_SCOPE_EX(satLd,*);
      if (satPic.StartLoadPNG(satellitePath) < 0)
      {
        LogF("Error loading '%s'", cc_cast(satellitePath));
        CString msg;
        AfxFormatString1(msg, IDS_ERROR_LOAD, satellitePath);
        AfxMessageBox(msg);
        return;
      }
      
      timing << "*** Sat loading started";
    }
        
    // size in satellite texture texels
    if (!sat.Init(satPic._w, satPic._h, gridInWSize, gridInHSize))
    {
      float wSegmentExact = satPic._w * gridInWSize;
      float hSegmentExact = satPic._h * gridInHSize;

      CString msg;
      CString texels;
      texels.Format("%gx%g", wSegmentExact, hSegmentExact);
      AfxFormatString1(msg, IDS_ERROR_SEG_SAT, texels);
      AfxMessageBox(msg);
      
      Fail("Bad texel mapping for satellite image");
      return;
    }
    
    SetModifiedFlag();
    actualLayer->SwitchToExplicitBimaps();
    m_MgrTextures.Destroy();
    m_Textures.Clear();

    if (m_pObjsPanel) m_pObjsPanel->UpdateDocData(this, TRUE);

    // we want to clear any bimpas information
    actualLayer->ClearExplicitBimaps();
    // the register should be empty now

    Array2D<IPictureFormat::Pixel> segTex;
    segTex.Dim(sat._wSegTex, sat._hSegTex);

    for (int zs = 0; zs < hCount; ++zs) 
    {
      for (int xs = 0; xs < wCount; ++xs)
      {
        progress.DoStep(1);
        // split satellite image for given segment	      
        {
          //PROFILE_SCOPE_EX(satSp,*);
          // we need to have borders on all sides
          int begXSat = xs * sat._wSegment;
          int begZSat = zs * sat._hSegment;
          // split only the part we are interested in
          // make sure we have enough read
          // free what is no longer needed
          if (satPic.ReadRows(begZSat - sat._zOffset + sat._hSegTex,
                    begZSat - sat._zOffset) < 0)
          {
            LogF("Error loading '%s'", cc_cast(satellitePath));
            CString msg;
            AfxFormatString1(msg, IDS_ERROR_LOAD, satellitePath);
            AfxMessageBox(msg);
            return;
          }
          for (int zz = 0; zz < sat._hSegTex; ++zz)
          {
            for (int xx = 0; xx < sat._wSegTex; ++xx)
            {
              //IPictureFormat::Pixel texel = satPic.GetClamped(begXSat+xx-xOffsetSat,begZSat+zz-zOffsetSat);
              IPictureFormat::Pixel texel = satPic.GetClamped(begXSat + xx - sat._xOffset,
                                      begZSat + zz - sat._zOffset);
              segTex(xx, zz) = texel;
            }
          }
        }
        
        {
          //PROFILE_SCOPE_EX(satSv,*);
          BString<1024> satSplit;
          sprintf(satSplit, absSatPattern, xs, zs);

          // simple: load and compare
          
          bool changed = ComparePNG(segTex, satSplit);
          
          // TODO: consider storing CRC instead
          
          if (changed)
          {
            //PROFILE_SCOPE_EX(satSS,*);
            if (GFormatPNG->Save(satSplit, sat._wSegTex, sat._hSegTex,
                       segTex.Data1D()) < 0)
            {
              LogF("Error saving '%s'", cc_cast(satSplit));
              CString msg;
              AfxFormatString1(msg, IDS_ERROR_SAVE, satSplit);
              AfxMessageBox(msg);
              return;
            }
          }
        }
        timing << "  * Sat split";
      }
    }
    
    if (satPic.ReadEnd() < 0)
    {
      LogF("Error loading '%s'", cc_cast(satellitePath));
      CString msg;
      AfxFormatString1(msg, IDS_ERROR_LOAD, satellitePath);
      AfxMessageBox(msg);
    }
    timing << "*** Sat split";
  }
#endif
  
  {
    //PROFILE_SCOPE_EX(mskGn,*);

    // we are interested in 1st line of the legend
    LayerLegend legend(legendPic._rows[0], legendPic._w, legendCfg,
               actualLayer->m_layers);
    legendPic.Free();

    // load the layer mask
    PictureData mask;
    {
      //PROFILE_SCOPE_EX(mskLd,*);
      if (mask.StartLoadPNG(layerMaskPath) < 0)
      {
        LogF("Error loading '%s'", cc_cast(layerMaskPath));
        CString msg;
        AfxFormatString1(msg, IDS_ERROR_LOAD, layerMaskPath);
        AfxMessageBox(msg);
        return;
      }
    }
    timing << "*** Mask loading started";
    
    SegmentGrid msk;
    
    int usedPass = 0;
    int totalPass = 0;
    
    if (!msk.Init(mask._w, mask._h, gridInWSize, gridInHSize))
    {
      float wSegmentExact = mask._w * gridInWSize;
      float hSegmentExact = mask._h * gridInHSize;

      CString msg;
      CString texels;
      texels.Format("%gx%g", wSegmentExact, hSegmentExact);
      AfxFormatString1(msg, IDS_ERROR_SEG_MASK, texels);
      AfxMessageBox(msg);
      return;
    }

    // no need to allocate this over and over  
    Array2D<LayerBlendItem> blendInfo;
    Array2D<ARGBMask> argbMask;
    Array2D<unsigned long> lerpMask;
    blendInfo.Dim(msk._wSegTex, msk._hSegTex);
    argbMask.Dim(msk._wSegTex, msk._hSegTex);
    lerpMask.Dim(msk._wSegTex, msk._hSegTex);

    StringList detailTexturesList;

    for (int zs = 0; zs < hCount; zs++) 
      for (int xs = 0; xs < wCount; xs++)
    { 
      // split mask for given segment, extract layer list
      progress.DoStep(1);
      
      AutoArray<int> layerList;
      int begXMask = xs * msk._wSegment;
      int begZMask = zs * msk._hSegment;

      if (mask.ReadRows(begZMask - msk._zOffset + msk._hSegTex, 
                begZMask - msk._zOffset) < 0)
      {
        LogF("Error loading '%s'", cc_cast(layerMaskPath));
        CString msg;
        AfxFormatString1(msg, IDS_ERROR_LOAD, layerMaskPath);
        AfxMessageBox(msg);
        return;
      }
      
      // process only the one we need
#if DEBUG_MASK
      if (zs != DEBUG_MASK_Z || xs != DEBUG_MASK_X) continue;
#endif
      {
        //PROFILE_SCOPE_EX(mskCB,*);
        // convert mask + legend into blendInfo representation
        for(int z = 0; z < msk._hSegTex; z++)
        {
          for(int x = 0; x < msk._wSegTex; x++)
          {
            int xSrc = begXMask + x - msk._xOffset;
            int zSrc = begZMask + z - msk._zOffset;
            LayerBlendItem& item = blendInfo(x, z);
            item = legend.ConvertToBlendOpt(mask.GetClamped(xSrc, zSrc));
          }
        }
      }

      
      AutoArray<int> layerSubst;
      { 
        // find most common layers
        //PROFILE_SCOPE_EX(mskFL,*);
        AutoArray<LayerUsage> layerUsage;
        int totalLayers = actualLayer->m_layers.Size();
        layerUsage.Resize(totalLayers);
        for (int i = 0; i<layerUsage.Size(); i++) layerUsage[i].usage = 0, layerUsage[i].layer = i;

        // scan the whole segment and compute border lengths
        for (int z = 0; z < msk._hSegTex; z++) for (int x = 0; x < msk._wSegTex; x++)
        {
          // for the active part increase the weights
          // consider border area as well, but with much lower priority
          float xWeight = x > msk._xOffset && x <= msk._xOffset + msk._wSegment ? 1 : 0.01f;
          float zWeight = z > msk._xOffset && z <= msk._xOffset + msk._wSegment ? 1 : 0.01f;
          const LayerBlendItem& item = blendInfo.Get(x, z);
          layerUsage[item.l0].usage += (1 - item.a1) * xWeight * zWeight;
          layerUsage[item.l1].usage += item.a1 * xWeight * zWeight;
        }
        

        // TODO: when selecting used layers, consider also "distance between layers"
        // similar approach like median cut palette construction could be used
        // select the most important, but keep order
        QSort(layerUsage, LayerUsage::CompareUsage);
        
        int layerSubset = layerUsage.Size();
        if (layerSubset > NLayers) layerSubset = NLayers;
        // ignore layers with no usage
        for (int i = 0; i < layerSubset; i++)
        {
          if (layerUsage[i].usage <= 0)
          {
            layerSubset = i;
            break;
          }
        }
        // selected subset ordered by layer index
        
        // substitution table: conversion from all layers to select subset
        layerList.Resize(layerSubset);
        layerSubst.Resize(layerUsage.Size());
        
#       if ARMA2_LEVEL
        if (layerSubset>5)
        {
          // ABC XYZ style
          // sort the layers not based on their frequency, but based on border length instead
          /*
          Create "border length" matrix - for each layer measure the distance of its border with all other layers
          We approximate this by computing neighboring per texel
          This is an overestimate, as the same neighbor will be counted againt several texels
          */
          Array2D<float> border;
          border.Dim(totalLayers,totalLayers);
          for (int s=0; s<totalLayers; s++) for (int d=0; d<totalLayers; d++) border(s,d) = 0;
          
          // scan the whole segment and compute border lengths
          for (int z = 0; z < msk._hSegTex; z++) for (int x = 0; x < msk._wSegTex; x++)
          {
            // for the active part increase the weights
            // consider border area as well, but with much lower priority
            float xWeight = x > msk._xOffset && x <= msk._xOffset + msk._wSegment ? 1 : 0.01f;
            float zWeight = z > msk._xOffset && z <= msk._xOffset + msk._wSegment ? 1 : 0.01f;
            
            const LayerBlendItem &item0 = blendInfo.Get(x, z);
            // if there is any blending in the item0, it should be counted against artifacts as well
            if (item0.a1>0)
            {
              // the closer the blending is to 0.5, the more important the artifact is
              float w = 1-fabs(item0.a1-0.5f)*2 * (xWeight*zWeight);
              // we assume the same importance for both layers
              border(item0.l0,item0.l1) += w, border(item0.l1,item0.l0) += w;
            }
            
            for (int zz=z-1; zz<=z+1; zz++) if (zz>=0 && zz<msk._hSegTex)
            for (int xx=x-1; xx<=x+1; xx++) if (xx>=0 && xx<msk._wSegTex)
            if (zz!=x || xx!=x)
            {
              const LayerBlendItem &item1 = blendInfo.Get(xx, zz);
              
              // compute with weights based on the blending ratio
              float i00i10 = (1-item0.a1)*(1-item1.a1)*(xWeight*zWeight);
              float i01i10 = (  item0.a1)*(1-item1.a1)*(xWeight*zWeight);
              float i00i11 = (1-item0.a1)*(  item1.a1)*(xWeight*zWeight);
              float i01i11 = (  item0.a1)*(  item1.a1)*(xWeight*zWeight);
              
              border(item0.l0,item1.l0) += i00i10, border(item1.l0,item0.l0) += i00i10;
              border(item0.l1,item1.l0) += i01i10, border(item1.l0,item0.l1) += i01i10;
              border(item0.l0,item1.l1) += i00i11, border(item1.l1,item0.l0) += i00i11;
              border(item0.l1,item1.l1) += i01i11, border(item1.l1,item0.l1) += i01i11;
              
            }
            // check all neighbors
            
          }

          // Naive solution was:
          // try all possible selections of ABC and XYZ
          // note: trying all ABC combinations and XYZ variations would be enough (120 possibilities)
          // brute force would be to try all ABCXYZ variations (720 possibilities)
          // we are trying only some of them (A<B<C), which should effectively try combinations only
          
          // Actually as we are only interested in error, all we need to do is to permute the elements
          // which contribute to the error. Ordering of the other elements may be arbitrary.
          
          // with 6 layers the only source of the error is X-Z transition
          // we start by selecting the best possible X-Z pair
          float minError = FLT_MAX;
          int bestX = -1, bestZ = -1;
          // we walk back to front because when error is equal, we want to use less popular layer
          for (int z = layerSubset; --z>=0;)
          for (int x = z; --x>=0;)
          {
            // we know the border matrix is symmetrical
            // compute the error estimation
            float error = 0;
            // error is only between layers X and Z
            // which original layer is the one we are marking as x/z here?
            int xLayer = layerUsage[x].layer;
            int zLayer = layerUsage[z].layer;
            error += border(xLayer,zLayer);
            
            if (minError>error)
            {
              minError = error;
              bestX = xLayer;
              bestZ = zLayer;
              // if we already have the best possible solution, do not search for any other
              if (minError<=0) break;
            }
          }
          Assert(bestX>=0 && bestZ>=0);
          
          // layerUsage is sorted by popularity
          // select the one least popular as the Y
          // while this is not really necessary, it leads to masks which are somewhat easier to understand
          int bestY = -1;
          for (int i=layerSubset; --i>=0; )
          {
            int layer = layerUsage[i].layer;
            if (layer==bestX || layer==bestZ) continue;
            bestY = layer;
            break;
          }
          // ABC - sort by layer indices to keep maximum layer stability
          // again, there is no real reason for this, other than easier mask texture understanding
          QSort(layerUsage.Data(), layerSubset, LayerUsage::CompareLayer);
          int done = 0;
          for (int i=0; i<layerSubset; i++)
          {
            int layer = layerUsage[i].layer;
            if (layer==bestX || layer==bestY || layer==bestZ) continue;
            layerList[done] = layer;
            layerSubst[layer] = done;
            done++;
          }
          // we know there were 6 layers, and we have already processed XYZ, therefore 3 must have been left
          Assert(done==3);
          
          // now fill the rest of the layers
          layerList[3] = bestX, layerSubst[bestX] = 3;
          layerList[4] = bestY, layerSubst[bestY] = 4;
          layerList[5] = bestZ, layerSubst[bestZ] = 5;
        }
        else
#       endif
        {
          // ArmA ABCD style, or ABC XYZ with at most 5 layers - sort the layers to keep ordering as stable as possible
          // with 4 or 5 layers no artifacts are possible
          QSort(layerUsage.Data(), layerSubset, LayerUsage::CompareLayer);
          // build the substitution table
          for (int i = 0; i < layerSubset; i++)
          {
            int layer = layerUsage[i].layer;
            layerList[i] = layer;
            layerSubst[layer] = i;
          }
        }
        
        for (int i = layerSubset; i < layerUsage.Size(); i++)
        {
          // TODO: control replacing by providing some replace information in the config
          int layer = layerUsage[i].layer;
          layerSubst[layer] = -1;
        }
      }

      { 
        // convert layer blend representation into a ARGB weighting mask
        //PROFILE_SCOPE_EX(mskCM,*);
        for (int z = 0; z < msk._hSegTex;  z++)
        {
          for (int x = 0; x < msk._wSegTex; x++)
          {
            const LayerBlendItem& item = blendInfo.Get(x, z);
            // RGBA ordering
            float l[NLayers];
            for (int i=0; i<NLayers; i++) l[i] = 0;
            int index0 = layerSubst[item.l0];
            int index1 = layerSubst[item.l1];
            if (index0 >= 0) l[index0] += 1 - item.a1;
            if (index1 >= 0) l[index1] += item.a1;
            
            float size = 0;
            for (int i=0; i<NLayers; i++) size += l[i];
            // if size is zero, provide a blend of all layers
            if (size == 0)
            {
              for (int i=0; i<NLayers; i++) l[i] = 1.0f/NLayers;
            }
            else
            {
              float invSize = 1.0f / size;
              for (int i=0; i<NLayers; i++) l[i] *= invSize;
            }
            
            { 
              // convert to ARGB representation
              ARGBMask argb;
              int sum = 0;
              for (int i=0; i<NLayers; i++)
              {
                int c = toInt(l[i] * 255);
                saturate(c, 0, 255);
                argb[i] = c;
                sum += c;
              }
              // note: sum of all layers should always be 1, but due to rounding errors a slight inaccuracy might happen
              // we do not care much, as long as it is not excessive
              // assume 1b error per layer is fine
              Assert(sum>=255-NLayers && sum<=255+NLayers);
              argbMask(x, z) = argb;
            }

            { 
              // convert into lerp representation
              // first layer is always 1 - goes into the A
              #if ARMA2_LEVEL
                // ArmA 2 style - ABC XYZ
                // lerp3 is a common weight of XYZ together

                float lerp3 = l[3];
                for (int i=4; i<NLayers; i++)
                {
                  lerp3 = lerp3*(1-l[i])+l[i];
                }

                float lerp2 = LayerDiv(l[2], (1 - lerp3));
                float lerp1 = LayerDiv(l[1], ((1 - lerp2) * (1 - lerp3)));
                float lerp0 = LayerDiv(l[0], ((1 - lerp1) * (1 - lerp2) * (1 - lerp3)));
                (void)lerp0;
                //we do not care about lerp0 factor at all
                
                // weight for layer Y grows between a = 0 .. 0.5 
                // weight for layer Z grows between a = 0.5 .. 1
                // check if X (layer 3) or Z (layer 5) is more important
                // based on that select between 0..1 or 1..2 blend
                float fXYZ;
                // note: a typical case the mask is not blending layers at all
                // as a result, at most one of the 3..5 layers is 1, other are 0
                if (l[3]>=l[5])
                {
                  // blending between X..Y
                  float yxRatio = l[3]>0 ? l[4]/(l[3]+l[4]) : 1;
                  fXYZ = yxRatio*0.5f;
                }
                else
                {
                  // blending between 1..2
                  float zyRatio = l[4]>0 ? l[5]/(l[4]+l[5]) : 1;
                  fXYZ = zyRatio*0.5f+0.5f;
                }
                
                // when lerp3 is zero, we want to always keep alpha = 1
                if (lerp3==0) fXYZ = 0;

                #if _DEBUG
                  // verification lerp really gives the expected results for all layers
                  // simulate the pixel shader work here
                  float aXYZ = lerp3;
                  float aC = lerp2 * (1 - lerp3);
                  float aB = lerp1 * (1 - lerp2) * (1 - lerp3);
                  float aA = lerp0 * (1 - lerp1) * (1 - lerp2) * (1 - lerp3);
            
                  // weight for layer 1 grows between a = 0 .. 0.5 
                  // weight for layer 2 grows between a = 0.5 .. 1
                  float weightsXYZ[3] = {1, floatMinMax(fXYZ*2,0,1), floatMinMax((fXYZ-0.5f)*2,0,1) };
                  
                  float aZ = aXYZ*weightsXYZ[2];
                  float aY = aXYZ*weightsXYZ[1] * (1-aZ);
                  float aX = aXYZ*weightsXYZ[0] * (1-aY) * (1-aZ);
                  Assert(fabs(aA - l[0]) < 1e-4f);
                  Assert(fabs(aB - l[1]) < 1e-4f);
                  Assert(fabs(aC - l[2]) < 1e-4f);
                  Assert(fabs(aX - l[3]) < 1e-4f);
                  Assert(fabs(aY - l[4]) < 1e-4f);
                  Assert(fabs(aZ - l[5]) < 1e-4f);
                #endif

                // encode the XYZ factor into alpha
                int a = toInt((1-fXYZ)*255);
              #else
                // ArmA style - ABCD
                float lerp3 = l[3];
                float lerp2 = LayerDiv(l[2], (1 - lerp3));
                float lerp1 = LayerDiv(l[1], ((1 - lerp2) * (1 - lerp3)));
                float lerp0 = LayerDiv(l[0], ((1 - lerp1) * (1 - lerp2) * (1 - lerp3)));
                (void)lerp0;

                #if _DEBUG
                  // lerp verification

                  float a3 = lerp3;
                  float a2 = lerp2 * (1 - lerp3);
                  float a1 = lerp1 * (1 - lerp2) * (1 - lerp3);
                  float a0 = lerp0 * (1 - lerp1) * (1 - lerp2) * (1 - lerp3);
                  Assert(fabs(a0 - l[0]) < 1e-4f);
                  Assert(fabs(a1 - l[1]) < 1e-4f);
                  Assert(fabs(a2 - l[2]) < 1e-4f);
                  Assert(fabs(a3 - l[3]) < 1e-4f);
                #endif

                int a = 255; // a does not matter - any other may be 1 if needed to cover it
              #endif
              int r = toInt(lerp1 * 255);
              int g = toInt(lerp2 * 255);
              int b = toInt(lerp3 * 255);
              saturate(r, 0, 255);
              saturate(g, 0, 255);
              saturate(b, 0, 255);
              saturate(a,0,255);
              
              lerpMask(x, z) = (a << 24) | (r << 16) | (g << 8) | b;
            }
          }
        }
        #if ARMA2_LEVEL
          if (layerList.Size()>4)
          {
            // postprocess lerp representation to prevent linear interpolation artifacts in the A channel
            // we need to make sure neighboring texels to those with B component (XYZ channel presents) share the A value
            
            // avoid setting on pixel twice
            Array2D<unsigned char> alreadySetPrev;
            Array2D<unsigned char> alreadySet;
            alreadySet.Dim(msk._wSegTex,msk._hSegTex);
            for (int z = 0; z < msk._hSegTex;  z++) for (int x = 0; x < msk._wSegTex; x++) alreadySet(x,z) = false;
            // note: we might use more elaborate scheme, e.g. weight by distance, but a simple one seems enough for our purpose
            for (int i=0; i<8; i++)
            {
              alreadySetPrev = alreadySet;
              bool somethingDone = false;
              
              for (int z = 0; z < msk._hSegTex;  z++) for (int x = 0; x < msk._wSegTex; x++)
              {
                // if there is B set in this pixel, spread alpha value to all neighbouring pixels
                if (lerpMask(x, z)&0xff || alreadySetPrev(x,z))
                {
                  unsigned int a = lerpMask(x,z)&0xff000000;
                  for (int zz=z-1; zz<=z+1; zz++) if (zz>=0 && zz<msk._hSegTex)
                  for (int xx=x-1; xx<=x+1; xx++) if (xx>=0 && xx<msk._wSegTex)
                  {
                    if (alreadySetPrev(xx,zz)) continue;
                    unsigned int oldMask = lerpMask(xx,zz);
                    if (((oldMask&0xff))==0)
                    {
                      unsigned int newMask = (oldMask&0xffffff)|a;
                      lerpMask(xx,zz) = newMask;
                      if (oldMask!=newMask)
                      {
                        somethingDone = true;
                        alreadySet(xx,zz) = true;
                      }
                    }
                    
                  }
                }
              }
              
              if (!somethingDone) break;
            }
          
          }
        #endif
      }

      { 
        // save argbMask as a texture
        //PROFILE_SCOPE_EX(mskSv,*);
        // adjust mask name by changing the path
        const char* maskFilename = GetFilenameExt(layerMaskPath);
        RString layerMaskTgtPath = (RString(layerMaskPath, maskFilename-layerMaskPath) + "Layers\\" + maskFilename);
        BString<1024> maskSplit;
        sprintf(maskSplit, absMaskPattern, xs, zs);

        bool changed = ComparePNG(lerpMask, maskSplit);
        if (changed)
        {
          //PROFILE_SCOPE_EX(mskSS,*);
          if (GFormatPNG->Save(maskSplit, lerpMask.GetXRange(), lerpMask.GetYRange(), 
                     lerpMask.Data1D()) < 0)
          {
            LogF("Error saving '%s'", cc_cast(maskSplit));
            CString msg;
            AfxFormatString1(msg, IDS_ERROR_SAVE, maskSplit);
            AfxMessageBox(msg);
            return;
          }
        }
      }

      { 
        // update texture mapping information
        //PROFILE_SCOPE_EX(mskUA,*);
        int begXGrid = xs * wSegmentGrid;
        int begZGrid = layerHeight - hSegmentGrid - zs * hSegmentGrid;
        
        // mapping from landscape space to segment space
        int xOffset = msk._xOffset, zOffset = msk._zOffset;
        int xStepSeg = msk._wSegment / wSegmentGrid, zStepSeg = msk._hSegment / hSegmentGrid;
        int xStepSize = xStepSeg * layerStep, zStepSize = zStepSeg * layerStep;
        
        AutoArray<int> usedLayers;

        for (int z = 0; z < hSegmentGrid; z += layerStep)
        {
          for (int x = 0; x < wSegmentGrid; x += layerStep)
          {
            UNITPOS unPos = { begXGrid + x, begZGrid + z };
            if (unPos.x < 0 || unPos.x >= layerWidth) continue;
            if (unPos.z < 0 || unPos.z >= layerHeight) continue;
            #if DEBUG_MASK
            if (unPos.x == 584 && unPos.z == 510)
            {
              __asm nop;
            }
            #endif
            // TODO: once lerp style blending is used, use smaller borders
            // I am not quite sure about the meaning of this comment now the lerp style is used
            // it probably means something like:
            // border is needed to avoid interpolation artifacts
            // with ARGB representation missed pixel could mean a hole in the ground
            // with lerp it means only slight visual inaccuracy
            int major = SelectUsedLayers(usedLayers, layerList,
                           // size including borders
                           x * xStepSeg,
                           (hSegmentGrid - layerStep - z) * zStepSeg,
                           xStepSize + xOffset * 2,
                           zStepSize + zOffset * 2,
                           // size not including borders
                           x * xStepSeg + xOffset,
                           (hSegmentGrid - layerStep - z) * zStepSeg + zOffset,
                           xStepSize, 
                           zStepSize, 
                           argbMask);

            for (int i = 0; i < usedLayers.Size(); i++)
            {
              if (usedLayers[i] >= 0) usedPass++;
            }
            totalPass += layerList.Size();
            actualLayer->DoUpdateSatAndMaskAt(unPos, 
              usedLayers.Size(),usedLayers.Data(),
              major,
              xs,zs, 
              float(sat._xOffset) / sat._hSegTex, float(sat._zOffset) / sat._wSegTex,
              float(sat._hSegment) / sat._hSegTex, float(sat._wSegment) / sat._wSegTex, 
              float(msk._xOffset) / msk._hSegTex, float(msk._zOffset) / msk._wSegTex,
              float(msk._hSegment) / msk._hSegTex, float(msk._wSegment) / msk._wSegTex,
              rvmatSaveFileFormat, 
              detailTexturesList
            );
          }
        } // for each grid square
      }
      timing << "  * Mask split";
    } // for each segment
    
    // if rvmat are saved as binary, save the file "textures.lst"
    if (rvmatSaveFileFormat)
    {
      detailTexturesList.Save();
    }

    if (mask.ReadEnd() < 0)
    {
      LogF("Error loading '%s'", cc_cast(layerMaskPath));
      CString msg;
      AfxFormatString1(msg, IDS_ERROR_LOAD, layerMaskPath);
      AfxMessageBox(msg);
    }
    timing << "*** Mask split";
    LogF("Passes: total %d, optimized to %d",totalPass,usedPass);
  } // convert mask

#if _ENABLE_PERFLOG
  //GPerfCounters.Diagnose();
  GPerfProfilers.Diagnose();
#endif
  UpdateAllViews(NULL, 0, NULL);
}

void CPoseidonDoc::OnProjectImportFromLandBuilder()
{
	static const char filter[] = "Land Builder file (*.lbt)|*.lbt|All Files (*.*)|*.*||";
	CFileDialog askFile(true, ".lbt", "", OFN_HIDEREADONLY | OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST, filter);
	if (askFile.DoModal() == IDOK)
	{
		std::ifstream file(askFile.GetPathName(), std::ios::in);
		if (!file)
		{
			AfxMessageBox("Open file error");
		}
		else
		{
			ImportLandBuilderFile(file, askFile.GetFileTitle(), false);
			CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
			ASSERT_KINDOF(CMainFrame, pMainFrame);
			pMainFrame->m_wndObjPanel.UpdateDocData(this, TRUE);
		}
	}
}

void CPoseidonDoc::OnProjectNewRoadsImportFromLandBuilder()
{
	static const char filter[] = "Land Builder file (*.lbt)|*.lbt|All Files (*.*)|*.*||";
	CFileDialog askFile(true, ".lbt", "", OFN_HIDEREADONLY | OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST, filter);
	if (askFile.DoModal() == IDOK)
	{
		std::ifstream file(askFile.GetPathName(), std::ios::in);
		if (!file)
		{
			AfxMessageBox("Open file error");
		}
		else
		{
			ImportLandBuilderFile(file, askFile.GetFileTitle(), true);
			CMainFrame* pMainFrame = (CMainFrame*)AfxGetMainWnd();
			ASSERT_KINDOF(CMainFrame, pMainFrame);
			pMainFrame->m_wndObjPanel.UpdateDocData(this, TRUE);
		}
	}
}

void CPoseidonDoc::RegisterNewItem(const CString& name, const CString& fname, int type)
{
	CString subDir;
    GetProjectDirectory(subDir, CPosEdBaseDoc::dirObj);

    CString myfname;
    if (subDir.CompareNoCase(fname.Mid(0, subDir.GetLength())) == 0)
	{
        myfname = fname.Mid(subDir.GetLength());
	}
    else
	{
        myfname = fname;
	}

    Ref<CPosObjectTemplate> pTempl;
	TRY
	{
        pTempl = new CPosObjectTemplate(this);
		pTempl->CreateFromObjFile(myfname, name);

/*
    // typ objektu
    if (m_bNatural)
        {		
      pTempl->m_nObjType = CPosObjectTemplate::objTpNatural;
            pTempl->SetPosType(CPosObjectTemplate::posTypeEllipse);
        }
    else
        {   
      pTempl->m_nObjType = CPosObjectTemplate::objTpPeople;
            pTempl->SetPosType(CPosObjectTemplate::posTypePoly);
        }
*/

		switch (type)
		{
		case CPosObjectTemplate::objTpNatural:
			{
				pTempl->m_nObjType = CPosObjectTemplate::objTpNatural;
				pTempl->SetPosType(CPosObjectTemplate::posTypeEllipse);
			}
			break;
		case CPosObjectTemplate::objTpPeople:
			{
				pTempl->m_nObjType = CPosObjectTemplate::objTpPeople;
				pTempl->SetPosType(CPosObjectTemplate::posTypePoly);
			}
			break;
		case CPosObjectTemplate::objTpNewRoads:
			{
				pTempl->m_nObjType = CPosObjectTemplate::objTpNewRoads;
				pTempl->SetPosType(CPosObjectTemplate::posTypePoly);
			}
			break;
		default:
			{
				pTempl->m_nObjType = CPosObjectTemplate::objTpPeople;
				pTempl->SetPosType(CPosObjectTemplate::posTypePoly);
			}
		}

		// vlo�en� do �ablon
		AddObjectTemplateSetID(pTempl);
	}
	CATCH_ALL(e)
	{
		MntReportMemoryException();
	}
	END_CATCH_ALL    
}

 extern void DoClearNetPartTestStates(CNetPartsList* pList, int nFrom = 0);

void CPoseidonDoc::ImportLandBuilderFile(std::istream& infile, const char* selname, bool newRoads)
{
	char strbuff[2000];
	int strbufflen = sizeof(strbuff);  
	AutoArray<CString> missingTemplates;
	CString prefix, suffix;
	CString filter(_T("Objects (*.p3d)|*.p3d||"));  
	bool firstAsk = true;
	bool enableReg = true;

	CProgressDialog progress(AfxGetMainWnd());
	progress.Create();
	//UNITPOS size = m_PoseidonMap.GetSize();
	progress.SetPos(0);
	infile.seekg(0, std::ios::end);
	progress.SetRange(infile.tellg());
	infile.seekg(0);
  
	CWaitCursor wt;

	infile.getline(strbuff, strbufflen, '\n');

	if (_stricmp(strbuff, "heightmap") == 0)
	{
		int left = 0, top = 0, right = 0, bottom = 0;
		float stepx = 0, stepy = 0;
		infile >> left >> top >> right >> bottom >> stepx >> stepy;
		UNITPOS sz = m_PoseidonMap.GetSize();
		for (int y = top; y <= bottom; ++y)
		{
			for (int x = left; x <= right; ++x)
			{
				float val = 0;
				infile >> val;
				if (x >= 0 && x < sz.x && y >= 0 && y < sz.z)
				{
					m_PoseidonMap.SetLandscapeHeight(x, y, val, false, false);
				}
			}
		}
		ws(infile);
		infile.getline(strbuff, strbufflen, '\n');
		if (_stricmp(strbuff, "end heightmap"))
		{
			AfxMessageBox("Excepting 'end heightmap' - format corrupted");
			return;
		}
		infile.getline(strbuff, strbufflen, '\n');
	}

	while (_stricmp(strbuff, "road template") == 0)
	{
		CPosNetTemplate* pNet = NULL;
		infile.getline(strbuff, strbufflen, '\n');
		TRY
		{
			pNet = new CPosNetTemplate(this);
			pNet->MakeDefaultValues();
			pNet->m_sNetName = CString(strbuff);
		}
		CATCH_ALL(e)
		{
			MntReportMemoryException();
			if (pNet) delete pNet;
		}
		END_CATCH_ALL

		string modelTypeS;
		string modelFileNameS;
		do
		{
			infile >> modelTypeS >> modelFileNameS;
			if (!infile) 
			{
				AfxMessageBox("Input stream error");
				break;
			}
			if (modelTypeS == "end")
			{
				if (modelFileNameS == "road") 
				{
					infile.getline(strbuff, strbufflen, '\n');
					break;
				}
			}

			if (pNet)
			{
				CString modelType = modelTypeS.c_str();
				CString modelFileName = modelFileNameS.c_str();
				modelFileName.Replace('#', ' ');

				CString guessFname = prefix + modelFileName + ".p3d";

				if (_access(guessFname, 0) == -1)
				{
					bool done = false;
					while (!done)
					{
						AfxMessageBox("Please select the models.");
						CFileDialog fdlg(TRUE, 0, prefix + modelFileName + ".p3d", OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, filter);
						if (fdlg.DoModal() == IDOK) 
						{
							guessFname = fdlg.GetPathName();
							int substr = guessFname.Find(modelFileName);
							if (substr != -1)
							{
								prefix = guessFname.Mid(0, substr);
							}
							else
							{
								AfxMessageBox("Warning: you have selected a different model from the one specified in the lbt file.");
							}
							done = true;
						}
					}
				}

				CString subDir;
				GetProjectDirectory(subDir, CPosEdBaseDoc::dirObj);

				CString myfname;
				if (subDir.CompareNoCase(guessFname.Mid(0, subDir.GetLength())) == 0)
				{
					myfname = guessFname.Mid(subDir.GetLength());
				}
				else
				{
					myfname = guessFname;
				}

				Ref<CPosObjectTemplate> pTempl = new CPosObjectTemplate(this);
				ASSERT(pTempl != NULL);
				// nastav�m objektu p��znak s�t�  
				pTempl->m_bIsNetObject = TRUE;
				pTempl->CreateFromObjFile(myfname);
				pTempl->m_nObjType = CPosObjectTemplate::objTpNet;
				AddObjectTemplateSetID(pTempl);

				if (modelType == "straight")
				{
					CNetSTRA* pObj = new CNetSTRA(this);
					// nastav�m parametry podle �ablony
					if (!pObj->CreateFromObjectTemplate(pTempl))
					{
						AfxMessageBox(IDS_OBJECT_NOT_NET_OBJ, MB_OK | MB_ICONEXCLAMATION);
						//delete pTempl;
						delete pObj;
						pObj = NULL;
					}
					if (pObj) pNet->m_NetSTRA.Add(pObj);
				}
				else if (modelType == "corner")
				{
					CNetBEND* pObj = new CNetBEND(this);
					// nastav�m parametry podle �ablony
					if (!pObj->CreateFromObjectTemplate(pTempl))
					{
						AfxMessageBox(IDS_OBJECT_NOT_NET_OBJ, MB_OK | MB_ICONEXCLAMATION);
						//delete pTempl;
						delete pObj;
						pObj = NULL;
					}
					if (pObj) pNet->m_NetBEND.Add(pObj);
				}
				else if (modelType == "special")
				{
					CNetSPEC* pObj = new CNetSPEC(this);
					// nastav�m parametry podle �ablony
					if (!pObj->CreateFromObjectTemplate(pTempl))
					{
						AfxMessageBox(IDS_OBJECT_NOT_NET_OBJ, MB_OK | MB_ICONEXCLAMATION);
						//delete pTempl;
						delete pObj;
						pObj = NULL;
					}
					if (pObj) pNet->m_NetSPEC.Add(pObj);
				}
				else if (modelType == "terminator")
				{
					CNetTERM* pObj = new CNetTERM(this);
					// nastav�m parametry podle �ablony
					if (!pObj->CreateFromObjectTemplate(pTempl))
					{
						AfxMessageBox(IDS_OBJECT_NOT_NET_OBJ, MB_OK | MB_ICONEXCLAMATION);
						//delete pTempl;
						delete pObj;
						pObj = NULL;
					}
					if (pObj) pNet->m_NetTERM.Add(pObj);
				}
			}
		}
		while (true);

		if (pNet) m_MgrNets.m_NetTemplates.Add(pNet);
		infile.getline(strbuff, strbufflen, '\n');
	}

	while (_stricmp(strbuff, "road definition") == 0)
	{
		if (m_MgrNets.m_NetTemplates.GetSize() > 0)
		{
			string roadTemplateS;
			float posX;
			float posZ;
			float angle;
			CPosNetObjectKey* pNetKey = NULL;
			CNetPartsList* pNetPartList = NULL;
			bool terminated = false;

			infile >> roadTemplateS >> posX >> posZ >> angle;
			if (!infile) 
			{
				AfxMessageBox("Input stream error");
			}

			CString roadTemplate = roadTemplateS.c_str();
			CPosNetTemplate* pTempl = m_MgrNets.GetNetTemplate(roadTemplate);
			if (pTempl)
			{
				CNetPartKEY* pKeyObj = new CNetPartKEY();
				ASSERT(pKeyObj != NULL);
				InitRealNetPos(pKeyObj->m_nBaseRealPos);
				pKeyObj->m_nBaseRealPos.nPos.x = posX;
				pKeyObj->m_nBaseRealPos.nPos.z = posZ;
				pKeyObj->m_nBaseRealPos.nGra   = angle;

				string roadPartTypeS;
				string roadPartNameS;
				string partDirectionS;
				int counter = 0;
				do
				{
					infile >> roadPartTypeS >> roadPartNameS >> partDirectionS;
					if (!infile) 
					{
						AfxMessageBox("Input stream error");
						break;
					}
					if (roadPartTypeS == "end")
					{
						infile.getline(strbuff, strbufflen, '\n');
						break;
					}

					if (pKeyObj)
					{
						CString roadPartName = roadPartNameS.c_str();
						roadPartName.Replace('#', ' ');

						if (counter == 0)
						{
							if (roadPartTypeS == "terminator")
							{
								pKeyObj->m_nKeyPartType = CNetPartKEY::typeTerm;
								pKeyObj->CreateFromTemplateTerm(pTempl, pTempl->GetTermTemplate(roadPartName));
							}
							else if (roadPartTypeS == "straight")
							{
								pKeyObj->m_nKeyPartType = CNetPartKEY::typeStra;
								pKeyObj->CreateFromTemplateStra(pTempl, pTempl->GetStraTemplate(roadPartName));
							}
							else if (roadPartTypeS == "special")
							{
								pKeyObj->m_nKeyPartType = CNetPartKEY::typeSpec;
								pKeyObj->CreateFromTemplateSpec(pTempl, pTempl->GetSpecTemplate(roadPartName));
							}
							else if (roadPartTypeS == "corner")
							{
								//pKeyObj->m_nKeyPartType = CNetPartKEY::typeBend;
								// not implemeted yet
								delete pKeyObj;
								pKeyObj = NULL;
							}
							else if (roadPartTypeS == "cross")
							{
								//pKeyObj->m_nKeyPartType = CNetPartKEY::typeCross;
								// not implemeted yet
								delete pKeyObj;
								pKeyObj = NULL;
							}

							if (pKeyObj)
							{
								CPosActionGroup* pGrpAction = NULL;
								TRY
								{
									pNetKey = new CPosNetObjectKey(m_PoseidonMap);
									ASSERT(pNetKey != NULL);
									pNetKey->CreateFromNetKeyPart(pKeyObj, &(m_MgrNets));
									//pNetKey->SetFreeID();
									pNetKey->DoUpdateNetPartsBaseRealPos();
									pNetKey->DoUpdateNetPartsLogPosition();
									pGrpAction = new CPosObjectGroupAction(IDA_NET_KEY_CREATE_GROUP);
									pGrpAction->m_bFromBuldozer = FALSE;

									CPosObjectAction* pNetAct = (CPosObjectAction*)(GenerNewObjectAction(IDA_NET_KEY_CREATE, 
																										 NULL,
																										 pNetKey,
																										 FALSE,
																										 pGrpAction));
									if (!DoUpdateNetPoseidonObjects(pNetKey, this, pGrpAction))
									{
										if (pGrpAction)
										{
											delete pGrpAction;
											pGrpAction = NULL;
										}
										else if (pNetKey)
										{
											delete pNetKey;
											pNetKey = NULL;
										}
									}
									else
									{
										ProcessEditAction(pGrpAction);
									}
								}
								CATCH_ALL(e)
								{
									MntReportMemoryException();
									if (pGrpAction)
									{
										delete pGrpAction;
										pGrpAction = NULL;
									} 
									else if (pNetKey)
									{
										delete pNetKey;
										pNetKey = NULL;
									}
								}
								END_CATCH_ALL
							}
						}
						else
						{
							if (pNetKey)
							{
								pNetPartList = (CNetPartsList*)(pNetKey->m_PartLists[0]);

								if (roadPartTypeS == "straight")
								{
									CNetSTRA* pSTRA = pTempl->GetStraTemplate(roadPartName);
									ASSERT(pSTRA != NULL);
									ASSERT_KINDOF(CNetSTRA, pSTRA);
									CNetPartSTRA* pNewPart = new CNetPartSTRA();
									ASSERT(pNewPart != NULL);
									pNewPart->CreateFromTemplateSTRA(pSTRA, pNetPartList);
									pNetPartList->m_NetParts.Add(pNewPart);
								}
								else if (roadPartTypeS == "corner")
								{
									CNetBEND* pBEND = pTempl->GetBendTemplate(roadPartName);
									ASSERT(pBEND != NULL);
									ASSERT_KINDOF(CNetBEND, pBEND);
									CNetPartBEND* pNewPart = new CNetPartBEND();
									ASSERT(pNewPart != NULL);
									if (partDirectionS == "right")
									{
										pNewPart->CreateFromTemplateBEND(pBEND, FALSE, pNetPartList);
									}
									else
									{
										pNewPart->CreateFromTemplateBEND(pBEND, TRUE, pNetPartList);
									}
									pNetPartList->m_NetParts.Add(pNewPart);
								}
								else if (roadPartTypeS == "special")
								{
									CNetSPEC* pSPEC = pTempl->GetSpecTemplate(roadPartName);
									ASSERT(pSPEC != NULL);
									ASSERT_KINDOF(CNetSPEC, pSPEC);
									CNetPartSPEC* pNewPart = new CNetPartSPEC();
									ASSERT(pNewPart != NULL);
									pNewPart->CreateFromTemplateSPEC(pSPEC, pNetPartList);
									pNetPartList->m_NetParts.Add(pNewPart);
								}
								else if (roadPartTypeS == "terminator")
								{
									if (!terminated)
									{
										REALNETPOS nGenerNextKey = pNetPartList->GetNetPartsEndRealPos();
										nGenerNextKey.nGra += 180;

										CNetPartKEY* pTermKeyObj = new CNetPartKEY();
										ASSERT(pTermKeyObj != NULL);
										InitRealNetPos(pTermKeyObj->m_nBaseRealPos);
										pTermKeyObj->m_nBaseRealPos = nGenerNextKey;
										pTermKeyObj->m_nKeyPartType = CNetPartKEY::typeTerm;
										pTermKeyObj->CreateFromTemplateTerm(pTempl, pTempl->GetTermTemplate(roadPartName));

										CPosNetObjectKey* pTermNetKey = NULL;
										CPosActionGroup* pGrpAction = NULL;
										TRY
										{
											pTermNetKey = new CPosNetObjectKey(m_PoseidonMap);
											ASSERT(pTermNetKey != NULL);
											//pTermNetKey->SetFreeID();
											pTermNetKey->CreateFromNetKeyPart(pTermKeyObj, &(m_MgrNets));
											pTermNetKey->DoUpdateNetPartsBaseRealPos();
											pTermNetKey->DoUpdateNetPartsLogPosition();
											pGrpAction = new CPosObjectGroupAction(IDA_NET_KEY_CREATE_GROUP);
											pGrpAction->m_bFromBuldozer = FALSE;

											CPosObjectAction* pNetAct = (CPosObjectAction*)(GenerNewObjectAction(IDA_NET_KEY_CREATE, 
																												 NULL,
																												 pTermNetKey,
																												 FALSE,
																												 pGrpAction));
											if (!DoUpdateNetPoseidonObjects(pTermNetKey, this, pGrpAction))
											{
												if (pGrpAction)
												{
													delete pGrpAction;
													pGrpAction = NULL;
												}
												else if (pTermNetKey)
												{
													delete pTermNetKey;
													pTermNetKey = NULL;
												}
											}
											else
											{
												ProcessEditAction(pGrpAction);
											}
										}
										CATCH_ALL(e)
										{
											MntReportMemoryException();
											if (pGrpAction)
											{
												delete pGrpAction;
												pGrpAction = NULL;
											} 
											else if (pTermNetKey)
											{
												delete pTermNetKey;
												pTermNetKey = NULL;
											}
										}
										END_CATCH_ALL

										terminated = true;
									}
								}
								DoClearNetPartTestStates(pNetPartList, pNetPartList->m_NetParts.GetSize() - 1);
								pNetPartList->DoUpdateNetPartsBaseRealPos(pNetPartList->m_nBaseRealPos, &m_PoseidonMap);
							}
						}
					}
					counter++;
				}
				while (true);

				if (pNetKey)
				{
					if (pNetPartList->m_NetParts.GetSize() > 0) 
					{
						CPosNetObjectKey CopyObj(m_PoseidonMap);
						CopyObj.CopyFrom((CPosNetObjectKey*)pNetKey);

						CNetPartsList* pList = (CNetPartsList*)(CopyObj.m_PartLists[0]);
						if (pList)
						{
							pList->CopyFrom(pNetPartList);
							Action_EditPoseidonNetKey(&CopyObj, this, NULL, FALSE);
						}
					}
				}
			}
			else
			{
				AfxMessageBox("Warning: found non defined road.");
				while (_stricmp(strbuff, "end road definition") != 0)
				{
					infile.getline(strbuff, strbufflen, '\n');
				}
			}
		}
		infile.getline(strbuff, strbufflen, '\n');
	}

	if (_stricmp(strbuff, "objects") == 0)
	{
		VisSelection sel;

		do 
		{          
			float a11, a12, a13, a21, a22, a23, a31, a32, a33, a41, a42, a43;
			int tag;
			infile >> a11 >> a12 >> a13 >> a21 >> a22 >> a23 >> a31 >> a32 >> a33 >> a41 >> a42 >> a43 >> tag;
			if (!infile) infile.clear();
			ws(infile);
			infile.getline(strbuff, strbufflen, '\n');
			if (!infile) 
			{
				AfxMessageBox("Input stream error");
				break;
			}
			if (_strnicmp(strbuff, "end objects", 11) == 0) break;
			CString modelName(strbuff);
			Ref<CPosObjectTemplate> tempobj = GetObjectTemplateByName(modelName);
			bool ismissing = false;
			if (tempobj.IsNull())
			{ 
				if (firstAsk) 
				{
					int res = AfxMessageBox("Some objects in LBT file are not registered. " 
											"Visitor will register them now. "
											"Note that you will be promted serveral times to locate object on the disk."
											"If you don't want register missing objects, press Cancel", MB_OKCANCEL | MB_ICONINFORMATION);
					if (res == IDCANCEL) enableReg = false;
					firstAsk = false;
				}
				int i;
				for (i = 0; i < missingTemplates.Size(); ++i) 
				{ 
					if (missingTemplates[i] == strbuff) break; 
				}
				if (i != missingTemplates.Size()) 
				{
					ismissing = true;
				}
				else if (enableReg) 
				{
					CString guessFname = prefix + modelName + suffix;
					while (_access(guessFname, 0) != 0) 
					{
						CFileDialog fdlg(TRUE, 0, prefix + modelName + suffix, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, filter);
						if (fdlg.DoModal() == IDCANCEL) 
						{
							guessFname = "";
							break;
						}

						guessFname = fdlg.GetPathName();
						int substr = guessFname.Find(modelName);
						if (substr != -1)
						{
							prefix = guessFname.Mid(0, substr);
							suffix = guessFname.Mid(substr + modelName.GetLength());
						}
					}
                
					if (guessFname.GetLength()) 
					{
						if (newRoads)
						{
							RegisterNewItem(modelName, guessFname, CPosObjectTemplate::objTpNewRoads);
						}
						else
						{
							RegisterNewItem(modelName, guessFname);
						}
					}
					tempobj = GetObjectTemplateByName(modelName);
				}
			}
			if (tempobj.IsNull())
			{
				if (!ismissing) missingTemplates.Add(CString(strbuff));
			}
			else
			{
				CPosEdObject* object = new CPosEdObject(m_PoseidonMap);
				object->m_pTemplate = tempobj;
				object->m_Position = Matrix4P(a11, a21, a31, a41, a12, a22, a32, a42, a13, a23, a33, a43);
				object->m_nSizeScale = 10;
				object->m_nRelHeight = a42;
				object->m_nTemplateID = tempobj->m_ID;
				object->SetFreeID();
				object->AllocPositions();
				m_PoseidonMap.AddNewObject(object, FALSE);
				sel.Include(NamSelItem::objInstance, object->GetID());
			}
			progress.SetPos(infile.tellg());
		} 
		while (true);

		char buff[50];
		DefineNamedSelection(RString(selname) + "_" + _itoa(GetTickCount() / 1000, buff, 16), &sel);

		m_PoseidonMap.UpdateObjectsHeight(true, false);    
		if (missingTemplates.Size())
		{
			CString mss;
			for (int i = 0; i < missingTemplates.Size(); ++i) 
			{
				mss = mss + missingTemplates[i] + "\r\n";
			}
			mss = "Following templates are not defined and was not imported:\r\n\r\n" + mss;
			AfxMessageBox(mss);
		}
	}
	OnDelUndoRedoActions();
}

void CPoseidonDoc::OnProjectImportXYZ()
{
  CFileDialog fdlg(TRUE,".xyz",0,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,"*.XYZ|*.XYZ|");
  if (fdlg.DoModal()==IDOK)
  {
    float landWidth=m_nRealSizeUnit*m_PoseidonMap.GetSize().x;
    XYZFile file;
    file.SetSourceFile(fdlg.GetFileName());;

    SDimension dim;
    if (!file.LoadDimension(dim)) return;

    SMapSize mapSize;
    mapSize.step = (dim.StepX + dim.StepY) * 0.5f;
    m_nRealSizeUnit = mapSize.step;
    mapSize.size[0] = dim.SizeX;
    mapSize.size[1] = dim.SizeY;
    mapSize.offset[0] = dim.MinX;
    mapSize.offset[1] = dim.MinY;

    file.Load(m_PoseidonMap, mapSize,true);
    m_nRealSizeUnit=landWidth/m_PoseidonMap.GetSize().x;
    m_PoseidonMap.UpdateObjectsHeight(true,false);    
    UpdateAllViews(NULL,0,NULL);    
  }
}

// -------------------------------------------------------------------------- //
// this is the new function added to allow to modify the project params using //
// the new project params dialog                                              //
// -------------------------------------------------------------------------- //
void CPoseidonDoc::OnToolsProjectParameters()
{
	if (OnEditPoseidonProjectParameters(this))
	{ 
	}
}


void CPoseidonDoc::OnImportTemplates() 
{
  CFileDialog fdlg(TRUE,0,0,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,"Another project|*.pew|");      
  if (fdlg.DoModal()==IDOK)
  {
    int cnt = 0;
    CWaitCursor wt;
    wt.Restore();
    CString cannotImport;
    CPoseidonDoc subDoc;
    if(!subDoc.OnOpenDocument(fdlg.GetPathName())) {
        AfxMessageBox("LoadProject general error");
        return;
    }
   
    int sz = subDoc.ObjectTemplatesSize();
    for (int i = 0; i < sz; i++) {
        Ref<CPosObjectTemplate> itm = subDoc.GetIthObjectTemplate(i);
        if (itm.NotNull())
        {
            const CString &name = itm->GetName();
            if (this->GetObjectTemplateByName(name).IsNull())
            {
                CPosObjectTemplate *copy = new CPosObjectTemplate(*itm);
                itm->m_pOwner = this;
                this->AddObjectTemplateSetID(itm);
                cnt++;
            }
            else
            {
                if (cannotImport.GetLength()<500) cannotImport=cannotImport+name+"\r\n";
            }
        }
    }
    if (cannotImport.GetLength())  {        
        CString s;
        AfxFormatString1(s,IDS_CANNOT_IMPORT_TEMPLATES,cannotImport);
        AfxMessageBox(s,MB_ICONINFORMATION);
    }
    OnUpdateBuldozer();
    {
        char buff[50];
        CString s;
        AfxFormatString1(s,IDS_IMPORTED_TEMPLATES,_itoa(cnt,buff,10));
        AfxMessageBox(s,MB_ICONINFORMATION);
    }
  }

}