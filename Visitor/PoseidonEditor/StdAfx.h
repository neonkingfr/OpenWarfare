/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(_POSEDITOR_STDAFX_H_)
#define _POSEDITOR_STDAFX_H_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#define WINVER 0x0500

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxole.h>
#include <afxadv.h>
#include <afxtempl.h>

#include <afxcmn.h>			// MFC support for Windows 95 Common Controls
//#include <afxdb.h>			// MFC ODBC database classes
//#include <afxdao.h>			// MFC DAO database classes
#include <windows.h>

#include <assert.h>
#include <fstream>


//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

/////////////////////////////////////////////////////////////

#pragma warning(disable : 4786) // Disabled warning about truncation of the name for source browser

#include <Es/essencePch.hpp>
#include "dep_app.hpp"

// model  dependecies should be moved elsewhere


#include "..\PoseidonEdCore\dep_core.hpp"
#include "..\PoseidonEdMaps\dep_maps.hpp"
#include "..\PoseidonEdObjs\dep_objs.hpp"
#include "..\PoseidonEdDlgs\dep_dlgs.hpp"


#endif // !defined(_POSEDITOR_STDAFX_H_)

