/////////////////////////////////////////////////////////////////////////////
// Rulers. Written By Stefan Ungureanu (stefanu@usa.net)
//

#include "stdafx.h"
#include "Ruler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRulerSplitterWnd

CRulerSplitterWnd::CRulerSplitterWnd()
{
	m_cxSplitter     = 0;
	m_cySplitter     = 0;
	m_cxBorderShare  = 0;
	m_cyBorderShare  = 0;
	m_cxSplitterGap  = 1;
	m_cySplitterGap  = 1;
  m_bRulersVisible = FALSE;
}

CRulerSplitterWnd::~CRulerSplitterWnd()
{
}


BEGIN_MESSAGE_MAP(CRulerSplitterWnd, CSplitterWnd)
	//{{AFX_MSG_MAP(CRulerSplitterWnd)
	ON_WM_LBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CRulerSplitterWnd::CreateRulers(CFrameWnd* pParent, CCreateContext* pContext)
{
	if (!CreateStatic(pParent, 2, 2))				
		return FALSE;														
																			
	if (!CreateView(0, 0, RUNTIME_CLASS(CRulerCornerView), CSize(0,0), pContext))				
		return FALSE;														
	if (!CreateView(0, 1, RUNTIME_CLASS(CRulerView), CSize(0,0), pContext))					
		return FALSE;														
	if (!CreateView(1, 0, RUNTIME_CLASS(CRulerView), CSize(0,0), pContext))					
		return FALSE;														
	if (!CreateView(1, 1, pContext->m_pNewViewClass, CSize(0,0), pContext))					
		return FALSE;														
																			
	SetColumnInfo(0, 0, 0);				
	SetRowInfo(0, 0, 0);				
																			
	((CRulerView*)GetPane(0, 1))->SetRulerType(RT_HORIZONTAL);										
	((CRulerView*)GetPane(1, 0))->SetRulerType(RT_VERTICAL);
    
    SetActivePane(1, 1);	
    
    return TRUE;
}

void CRulerSplitterWnd::ShowRulers(BOOL bShow)
{
  int nSize       = (bShow)?RULER_SIZE:0;
  int nSizeBorder = (bShow)?3:1;

  SetRowInfo(0, nSize, 0);
  SetColumnInfo(0, nSize, 0);
  m_cxSplitterGap  = nSizeBorder;
  m_cySplitterGap  = nSizeBorder;
  m_bRulersVisible = bShow;
  RecalcLayout();
}

void CRulerSplitterWnd::UpdateRulersInfo(stRULER_INFO stRulerInfo, RulerOrient orient )
{
  if (orient == RT_VERTICAL)
    ((CRulerView*)GetPane(1, 0))->UpdateRulersInfo(stRulerInfo);
  else
    ((CRulerView*)GetPane(0, 1))->UpdateRulersInfo(stRulerInfo);
  

  /*if (((int)(stRulerInfo.DocSize.cx*stRulerInfo.fZoomFactor) < stRulerInfo.DocSize.cx) ||
    ((int)(stRulerInfo.DocSize.cy*stRulerInfo.fZoomFactor) < stRulerInfo.DocSize.cy))
      ShowRulers(FALSE, FALSE);
    else*/ 
  //if (m_bRulersVisible)
  //  ShowRulers(TRUE);
}

void CRulerSplitterWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
    //Lock Splitter
    //CSplitterWnd::OnLButtonDown(nFlags, point);
}

void CRulerSplitterWnd::OnMouseMove(UINT nFlags, CPoint point) 
{
    //Lock Splitter
	//CSplitterWnd::OnMouseMove(nFlags, point);
}
/////////////////////////////////////////////////////////////////////////////
// CRulerSplitterWnd message handlers

/////////////////////////////////////////////////////////////////////////////
// CRulerView

IMPLEMENT_DYNCREATE(CRulerView, CView)

CRulerView::CRulerView()
{
	m_rulerType   = RT_HORIZONTAL;
	m_scrollPos   = 0;
	m_lastPos     = -1;
  m_fZoomFactor = 1;
}

CRulerView::~CRulerView()
{
}


BEGIN_MESSAGE_MAP(CRulerView, CView)
	//{{AFX_MSG_MAP(CRulerView)
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRulerView drawing
void CRulerView::UpdateRulersInfo(stRULER_INFO stRulerInfo)
{
  m_DocSize     = stRulerInfo.DocSize;
  m_fZoomFactor = stRulerInfo.fZoomFactor;
  m_scrollPos   = stRulerInfo.ScrollPos;
  m_bInverted   = stRulerInfo.bInverted;
  m_Pos = stRulerInfo.Pos;


  if (stRulerInfo.uMessage == RW_POSITION) {
    DrawCursorPos();
  }
  else if (stRulerInfo.uMessage == RW_SCROLL)
  {
   
    CDC* pDC = GetDC();
    OnDraw(pDC);
    ReleaseDC(pDC);    
  }
  else
    Invalidate();
} 

void CRulerView::OnDraw(CDC* pDC)
{ 
	m_lastPos = -1;
    
	// set the map mode right
	int oldMapMode=pDC->SetMapMode(MM_TEXT);

	CFont vFont;
	CFont hFont;
	vFont.CreateFont(10, 0, 900, 900, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_TT_ALWAYS, PROOF_QUALITY, VARIABLE_PITCH|FF_ROMAN, "Times New Roman");
	hFont.CreateFont(12, 0, 000, 000, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_TT_ALWAYS, PROOF_QUALITY, VARIABLE_PITCH|FF_ROMAN, "Times New Roman");
	
    CFont* pOldFont  = pDC->SelectObject((m_rulerType==RT_HORIZONTAL)?&hFont:&vFont);
	int oldTextAlign = pDC->SetTextAlign((m_rulerType==RT_HORIZONTAL)?(TA_RIGHT|TA_TOP):(TA_LEFT|TA_TOP));
	int oldBkMode    = pDC->SetBkMode(TRANSPARENT);

	CRect rulerRect;
	GetClientRect(&rulerRect);
	
	pDC->FillSolidRect(rulerRect, RGB(255, 255, 255));

  float nFactor = 100;
  DrawTicker(pDC, rulerRect, nFactor);
  nFactor /= 2;
  if (nFactor > 0)
  {
    DrawTicker(pDC, rulerRect, nFactor, 10, FALSE);
    nFactor /= 2;
    if (nFactor > 0)
      DrawTicker(pDC, rulerRect, nFactor, 12, FALSE);
  }

	// restore DC settings
	pDC->SetMapMode(oldMapMode);
  pDC->SelectObject(pOldFont);
	pDC->SetTextAlign(oldTextAlign);
	pDC->SetBkMode(oldBkMode);

  // Draw cursor Pos.
  DrawCursorPos();
}


void CRulerView::DrawTicker(CDC* pDC, CRect rulerRect, float& nFactor, int nBegin, BOOL bShowPos)
{
  int nSize  = (m_rulerType == RT_HORIZONTAL)?rulerRect.Width():rulerRect.Height();
  float realSize = nSize / m_fZoomFactor;
  
  float realStart =  m_scrollPos - (m_bInverted ? realSize : 0);

#define MAX_TEXT_DIST_PX 300
  if (bShowPos && nFactor * m_fZoomFactor > MAX_TEXT_DIST_PX)
  {
    nFactor /= (int)((nFactor * m_fZoomFactor)/MAX_TEXT_DIST_PX) + 1;
  }
 
#define MIN_TEXT_DIST_PX 100
  if (bShowPos && nFactor * m_fZoomFactor < MIN_TEXT_DIST_PX)
  {
    nFactor *= (int) (MIN_TEXT_DIST_PX /m_fZoomFactor/nFactor) + 1;    
  }
   
  int end = (int) ((realStart + realSize) / nFactor);
  int start = (int) (realStart / nFactor) + 1;

  for (int i = start; i <= end; i++)
  {
    char buffer[100];
    sprintf(buffer, "%.1f", i*nFactor);

    if (m_rulerType==RT_HORIZONTAL)
    {
      if (m_bInverted)
      {
        int pos = nSize - (int)((nFactor*i-realStart) * m_fZoomFactor);
        pDC->PatBlt( pos, rulerRect.top+nBegin, 1, rulerRect.Height(), BLACKNESS);
        if (bShowPos)
          pDC->TextOut( pos, rulerRect.top, CString(buffer));
      }
      else
      {
        int pos = (int)((nFactor*i-realStart) * m_fZoomFactor);
        pDC->PatBlt( pos, rulerRect.top+nBegin, 1, rulerRect.Height(), BLACKNESS);
        if (bShowPos)
          pDC->TextOut( pos, rulerRect.top, CString(buffer));
      }
    }
    else //(m_rulerType==RT_VERTICAL)
    {
      if (m_bInverted)
      {
        int pos = nSize - (int)((nFactor*i-realStart) * m_fZoomFactor);
        pDC->PatBlt(rulerRect.left+nBegin, pos, rulerRect.Width(), 1, BLACKNESS);
        if (bShowPos)
          pDC->TextOut(rulerRect.left, pos, CString(buffer));
      }
      else
      {
        int pos = (int)((nFactor*i-realStart) * m_fZoomFactor);
        pDC->PatBlt(rulerRect.left+nBegin, pos, rulerRect.Width(), 1, BLACKNESS);
        if (bShowPos)
          pDC->TextOut(rulerRect.left, pos, CString(buffer));
      }
    }
  }
}

void CRulerView::DrawCursorPos()
{
	CDC* pDC = GetDC();
	// set the map mode right
	int oldMapMode = pDC->SetMapMode(MM_TEXT);

	CRect clientRect;
	GetClientRect(&clientRect);
	if (m_rulerType==RT_HORIZONTAL)
	{
		// erase the previous position
    if (m_lastPos >= 0)
      pDC->PatBlt(m_lastPos, clientRect.top, 1, clientRect.Height(), DSTINVERT);
		// draw the new position

    if (m_bInverted)    
      m_lastPos = (int)((m_scrollPos - m_Pos) * m_fZoomFactor);
    else
		  m_lastPos = (int)((m_Pos - m_scrollPos) * m_fZoomFactor);
    pDC->PatBlt(m_lastPos, clientRect.top, 1, clientRect.Height(), DSTINVERT);
	}
	else // (m_rulerType==RT_VERTICAL)
	{
		// erase the previous position
    if (m_lastPos >= 0)
      pDC->PatBlt(clientRect.left, m_lastPos, clientRect.Width(), 1, DSTINVERT);
		// draw the new position
    if (m_bInverted)    
      m_lastPos = (int)((m_scrollPos - m_Pos) * m_fZoomFactor);
    else
      m_lastPos = (int)((m_Pos - m_scrollPos) * m_fZoomFactor);
    pDC->PatBlt(clientRect.left, m_lastPos, clientRect.Width(), 1, DSTINVERT);
	}

	pDC->SetMapMode(oldMapMode);
	ReleaseDC(pDC);
}

/////////////////////////////////////////////////////////////////////////////
// CRulerView diagnostics

#ifdef _DEBUG
void CRulerView::AssertValid() const
{
	CView::AssertValid();
}

void CRulerView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CRulerView message handlers

BOOL CRulerView::PreCreateWindow(CREATESTRUCT& cs) 
{
	// create a brush using the same color as the background
	if (cs.lpszClass == NULL)
	{
		HBRUSH hBr=CreateSolidBrush(GetSysColor(COLOR_MENU));
		cs.lpszClass = AfxRegisterWndClass(CS_DBLCLKS|CS_BYTEALIGNWINDOW, NULL, hBr);
	}

	return CView::PreCreateWindow(cs);
}

void CRulerView::OnSetFocus(CWnd* pOldWnd) 
{
	CView::OnSetFocus(pOldWnd);
	
    ((CSplitterWnd*)GetParent())->SetActivePane(1, 1);	
}

void CRulerView::OnInitialUpdate() 
{
	CView::OnInitialUpdate();
}

/////////////////////////////////////////////////////////////////////////////
// CRulerCornerView

IMPLEMENT_DYNCREATE(CRulerCornerView, CView)

CRulerCornerView::CRulerCornerView()
{
}

CRulerCornerView::~CRulerCornerView()
{
}


BEGIN_MESSAGE_MAP(CRulerCornerView, CView)
	//{{AFX_MSG_MAP(CRulerCornerView)
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRulerCornerView drawing

void CRulerCornerView::OnDraw(CDC* pDC)
{
}

/////////////////////////////////////////////////////////////////////////////
// CRulerCornerView diagnostics

#ifdef _DEBUG
void CRulerCornerView::AssertValid() const
{
	CView::AssertValid();
}

void CRulerCornerView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CRulerCornerView message handlers

BOOL CRulerCornerView::PreCreateWindow(CREATESTRUCT& cs) 
{
	// create a brush using the same color as the background
	if (cs.lpszClass == NULL)
	{
		HBRUSH hBr=CreateSolidBrush(GetSysColor(COLOR_MENU));
		cs.lpszClass = AfxRegisterWndClass(CS_DBLCLKS|CS_BYTEALIGNWINDOW, NULL, hBr);
	}

	return CView::PreCreateWindow(cs);
}

void CRulerCornerView::OnSetFocus(CWnd* pOldWnd) 
{
	CView::OnSetFocus(pOldWnd);
	
    ((CSplitterWnd*)GetParent())->SetActivePane(1, 1);	
}

