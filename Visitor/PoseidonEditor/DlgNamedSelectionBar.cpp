// DlgNamedSelectionBar.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "DlgNamedSelectionBar.h"
#include ".\dlgnamedselectionbar.h"

// DlgNamedSelectionBar dialog

DlgNamedSelectionBar::DlgNamedSelectionBar(CWnd* pParent /*=NULL*/)
{
	_nameCounter = 1;
}

DlgNamedSelectionBar::~DlgNamedSelectionBar()
{
}

BEGIN_MESSAGE_MAP(DlgNamedSelectionBar, CDialogBar)
  ON_WM_CONTEXTMENU()
  ON_COMMAND(ID_NAMEDSEL_ADD, OnNamedselAdd)
  ON_COMMAND(ID_NAMEDSEL_DELETE, OnNamedselDelete)
  ON_COMMAND(ID_NAMEDSEL_DELETEWITHOBJECTS, OnNamedselDeleteWithObj)
  ON_COMMAND(ID_NAMEDSEL_RENAME, OnNamedselRename)
//  ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LIST, OnLvnEndlabeleditList)
//  ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST, OnLvnItemchangedList)
  ON_COMMAND(ID_NAMEDSEL_REDEFINE, OnNamedselRedefine)
  ON_COMMAND(ID_NAMEDSEL_INVERTSELECTION, OnNamedselInvertselection)
  ON_COMMAND(ID_NAMEDSEL_SELECTALL, OnNamedselSelectall)
  ON_COMMAND(ID_NAMEDSEL_HIDESELECTED, OnNamedselHideselected)
  ON_COMMAND(ID_NAMEDSEL_SHOWSELECTED, OnNamedselShowselected)
  ON_COMMAND(ID_NAMEDSEL_SHOWALL, OnNamedselShowall)
  ON_NOTIFY(NM_CLICK, IDC_LIST, OnNMClickList)
  ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LIST, OnLvnEndlabeleditList)
  ON_NOTIFY(LVN_ITEMACTIVATE, IDC_LIST, OnLvnItemActivateList)
  ON_COMMAND(ID_NAMEDSEL_SHOWONLYSELECTED, OnNamedselShowonlyselected)
  ON_COMMAND(ID_NAMEDSEL_LOCKSELECTED, OnNamedselLockselected)
  ON_COMMAND(ID_NAMEDSEL_UNLOCKALL, OnNamedselUnlockall)
  ON_COMMAND(ID_NAMEDSEL_UNLOCKSELECTED, OnNamedselUnlockselected)
END_MESSAGE_MAP()

CSize DlgNamedSelectionBar::CalcDynamicDim(int nLength, DWORD dwMode)
{
	// Return default if it is being docked or floated
	if ((dwMode & LM_VERTDOCK) || (dwMode & LM_HORZDOCK))
	{
		if (dwMode & LM_STRETCH) // if not docked stretch to fit
		{
			return CSize((dwMode & LM_HORZ) ? 32767 : m_sizeDocked.cx, (dwMode & LM_HORZ) ? m_sizeDocked.cy : 32767);
		}
		else
		{
			return m_sizeDocked;
		}
	}
	if (dwMode & LM_MRUWIDTH) return m_sizeFloating;
	// In all other cases, accept the dynamic length
	if (dwMode & LM_LENGTHY)
	{
		return CSize(m_sizeFloating.cx, /*(m_bChangeDockedSize) ?*/
					 m_sizeFloating.cy = m_sizeDocked.cy = nLength /*:
                                                  m_sizeFloating.cy = nLength*/);
	}
	else
	{
		return CSize(/*(m_bChangeDockedSize) ?*/
					 m_sizeFloating.cx = m_sizeDocked.cx = nLength /*:
                                                m_sizeFloating.cx = nLength*/, m_sizeFloating.cy);
	}
}

#define MIN_SIZE_X 150
#define MIN_SIZE_Y 200 

CSize DlgNamedSelectionBar::CalcDynamicLayout(int nLength, DWORD dwMode)
{
	CSize size = CalcDynamicDim(nLength, dwMode);
	if (size.cx < MIN_SIZE_X) size.cx = MIN_SIZE_X;

	if (size.cy < MIN_SIZE_Y) size.cy = MIN_SIZE_Y;

	RecalcChildWindowPos(size);
	return size;
}

void DlgNamedSelectionBar::RecalcChildWindowPos(const CSize& size)
{  
	wList.MoveWindow(CRect(5, 5, size.cx - 10, size.cy - 10), TRUE);
}

BOOL DlgNamedSelectionBar::Create(CWnd* pParentWnd, UINT nIDTemplate, UINT nStyle, UINT nID)
{
	BOOL ret =  CDialogBar::Create(pParentWnd, nIDTemplate, nStyle, nID);
	m_sizeFloating = m_sizeDocked = GetPosEdEnvironment()->m_appDockState.m_namSelPanelSize;
	wList.SubclassDlgItem(IDC_LIST, this);
	wList.InsertColumn(0,"");
	ilist.Create(IDB_SELECTIONICON, 16, 0, RGB(255, 0, 255));
	wList.SetImageList(&ilist, LVSIL_SMALL);
	return ret;
}

namespace Functors
{
	class NamedSelFillListbox
	{
		mutable CListCtrl& list;
	public:
		NamedSelFillListbox(CListCtrl& list)
		: list(list) 
		{
		}

		int operator () (const VisNamedSelection* sel) const
		{
			list.InsertItem(0, sel->GetName(), 0);
			return 0;
		}
	};
}

void DlgNamedSelectionBar::ReloadBar()
{
	wList.DeleteAllItems();
	wList.EnableWindow(m_curDoc != 0);
	if (m_curDoc == 0) return;
 
	m_curDoc->ForEachNamedSel(Functors::NamedSelFillListbox(wList));
}

void DlgNamedSelectionBar::OnContextMenu(CWnd* pWnd, CPoint point)
{
	CPoint locPoint = point;
	_contextItem = wList.GetNextItem(-1, LVNI_SELECTED);
	wList.ScreenToClient(&locPoint);
	CMenu mnu;
	mnu.LoadMenu(IDR_NAMEDSELPOPUP);
	CMenu* submenu = mnu.GetSubMenu(0);
	if (m_curDoc == 0)
	{
		for (int i = 0, cnt = submenu->GetMenuItemCount(); i < cnt; ++i)
		{
			submenu->EnableMenuItem(i, MF_BYPOSITION | MF_GRAYED);
		}
	}
	else
	{
		if (_contextItem == -1)
		{
			submenu->EnableMenuItem(ID_NAMEDSEL_DELETE, MF_GRAYED);
			submenu->EnableMenuItem(ID_NAMEDSEL_RENAME, MF_GRAYED);
			submenu->EnableMenuItem(ID_NAMEDSEL_REDEFINE, MF_GRAYED);
		}
	}
	submenu->TrackPopupMenu(TPM_RIGHTBUTTON, point.x, point.y, this);
}

void DlgNamedSelectionBar::SelectionAdded(const char* name)
{
	LVFINDINFO ffo;
	ffo.flags = LVFI_STRING;
	ffo.psz = name;
	int i = wList.FindItem(&ffo);
	if (i == -1) wList.InsertItem(0, name, 0);
}

void DlgNamedSelectionBar::SelectionRemoved(const char* name)
{
	LVFINDINFO ffo;
	ffo.flags = LVFI_STRING;
	ffo.psz = name;
	int i = wList.FindItem(&ffo);
	if (i != -1) wList.DeleteItem(i);
}

CString DlgNamedSelectionBar::OfferUniqueSelectionName()
{
	CString x;
	int i;
	LVFINDINFO ffo;
	ffo.flags = LVFI_STRING;
	_nameCounter--;

	do 
	{
		_nameCounter++;
		x.Format(IDS_SELECTION_BASIC_NAME, _nameCounter);
		ffo.psz = x;
		i = wList.FindItem(&ffo);
	} 
	while(i != -1);

	return x;
}

CString DlgNamedSelectionBar::GetSelectedSelectionName() const
{
	int i = wList.GetNextItem(-1, LVNI_FOCUSED);
	if (i == -1) 
	{
		return CString();
	}
	else
	{
		return wList.GetItemText(i, 0);
	}
}

void DlgNamedSelectionBar::EditSelectionName(const char* name)
{
	LVFINDINFO ffo;
	ffo.flags = LVFI_STRING;
	ffo.psz = name;
	int i = wList.FindItem(&ffo);
	if (i != -1) wList.EditLabel(i);
}

void DlgNamedSelectionBar::OnNamedselAdd()
{
	CString guessName = OfferUniqueSelectionName();
	if (m_curDoc) m_curDoc->DefineNamedSelectionAction(RStringI(guessName));
	EditSelectionName(guessName);
}

void DlgNamedSelectionBar::OnNamedselDelete()
{
	UseSelection(_contextItem);
	CString name = GetSelectedSelectionName();
	if (name.GetLength())
	{
		if (m_curDoc) m_curDoc->DeleteSelectionAction(RStringI(name));
	}
}

void DlgNamedSelectionBar::OnNamedselRename()
{
	UseSelection(_contextItem);
	CString name = GetSelectedSelectionName();
	if (name.GetLength())
	{
		EditSelectionName(name);
	}
}

BOOL DlgNamedSelectionBar::PreTranslateMessage(MSG* pMsg)
{
	if (wList.GetEditControl() != 0)
	{
		if (IsDialogMessage(pMsg)) return TRUE;
	}  

	return CDialogBar::PreTranslateMessage(pMsg);
}

void DlgNamedSelectionBar::OnNamedselRedefine()
{
	CString sel = GetSelectedSelectionName();
	if (sel.GetLength())
	{
		if (m_curDoc) m_curDoc->DefineNamedSelectionAction(RStringI(sel));
	}
}

void DlgNamedSelectionBar::OnNamedselInvertselection()
{
	if (m_curDoc) 
	{
		UseSelection(_contextItem);
		CWaitCursor wait;
		m_curDoc->InvertSelection();
		m_curDoc->UpdateAllViewsSelection();
	}
}

void DlgNamedSelectionBar::OnNamedselSelectall()
{
	if (m_curDoc) 
	{  
		POSITION pos = m_curDoc->GetFirstViewPosition();
		while (pos)
		{    
			CView* v = m_curDoc->GetNextView(pos);
			CPosEdMainView* vv = dynamic_cast<CPosEdMainView*>(v);
			if (vv)
			{
				vv->OnEditSelectAll();
				break;
			}
		}
	}
}

void DlgNamedSelectionBar::OnNamedselHideselected()
{
	if (m_curDoc) 
	{
		UseSelection(_contextItem);
		CWaitCursor wait;
		m_curDoc->HideObjects();m_curDoc->UpdateAllViewsSelection();
	}
}

void DlgNamedSelectionBar::OnNamedselShowselected()
{
	if (m_curDoc)   
	{
		UseSelection(_contextItem);
		CWaitCursor wait;
		m_curDoc->HideObjects(true, false);
		m_curDoc->UpdateAllViewsSelection();
	}
}

void DlgNamedSelectionBar::OnNamedselShowall()
{
	CWaitCursor wait;
	if (m_curDoc) {m_curDoc->HideObjects(false, false);
	m_curDoc->UpdateAllViews(0);}
}

void DlgNamedSelectionBar::UseSelection(int selId)
{
	if (selId < 0) return;
	CString name = wList.GetItemText(selId, 0);
	CWaitCursor wait;
	if (m_curDoc) m_curDoc->UseNamedSelection(RStringI(name), GetKeyState(VK_CONTROL) < 0, GetKeyState(VK_SHIFT) < 0);
}

void DlgNamedSelectionBar::OnNMClickList(NMHDR* pNMHDR, LRESULT* pResult)
{
	POSITION pos = wList.GetFirstSelectedItemPosition();
	while (pos)
	{
		UseSelection(wList.GetNextSelectedItem(pos));
	}
	*pResult = 0;
}

void DlgNamedSelectionBar::OnLvnEndlabeleditList(NMHDR* pNMHDR, LRESULT* pResult)
{
	NMLVDISPINFO* pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
	*pResult = 0;
	if (pDispInfo->item.pszText == 0 || pDispInfo->item.pszText[0] == 0) return;
	CString curName = wList.GetItemText(pDispInfo->item.iItem, 0);
	CString newName = pDispInfo->item.pszText;
	if (m_curDoc) m_curDoc->RenameSelectionAction(RStringI(curName), RStringI(newName));
}

void DlgNamedSelectionBar::OnLvnItemActivateList(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	UseSelection(pNMIA->iItem);
	*pResult = 0;
}

void DlgNamedSelectionBar::OnNamedselShowonlyselected()
{
	if (m_curDoc)   
	{
		UseSelection(_contextItem);
		CWaitCursor wait;
		m_curDoc->HideObjects(true, false);
		m_curDoc->InvertSelection();
		m_curDoc->HideObjects(true, true);
		m_curDoc->m_GroupObjects.DestroyGroup();
		m_curDoc->UpdateAllViewsSelection();
		m_curDoc->UpdateAllViews(0);
	}
}

void DlgNamedSelectionBar::OnNamedselLockselected()
{
	if (m_curDoc) 
	{  
		UseSelection(_contextItem);
		POSITION pos = m_curDoc->GetFirstViewPosition();
		while (pos)
		{    
			CView* v = m_curDoc->GetNextView(pos);
			CPosEdMainView* vv = dynamic_cast<CPosEdMainView*>(v);
			if (vv)
			{
				vv->OnLock();
				break;
			}
		}
	}
}

void DlgNamedSelectionBar::OnNamedselUnlockall()
{
	if (m_curDoc)
	{
		_contextItem = -1;
		OnNamedselSelectall();
		OnNamedselUnlockselected();
		m_curDoc->m_GroupObjects.DestroyGroup();
		m_curDoc->UpdateAllViewsSelection();
		m_curDoc->UpdateAllViews(0);
	}
}

void DlgNamedSelectionBar::OnNamedselUnlockselected()
{
	if (m_curDoc)  
	{  
		POSITION pos = m_curDoc->GetFirstViewPosition();
		while (pos)
		{    
			CView* v = m_curDoc->GetNextView(pos);
			CPosEdMainView* vv = dynamic_cast<CPosEdMainView*>(v);
			if (vv)
			{
				vv->OnUnlock();
				break;
			}
		}
	}
}

void DlgNamedSelectionBar::OnNamedselDeleteWithObj()
{
	if (m_curDoc)  
	{  
		UseSelection(_contextItem);
		POSITION pos = m_curDoc->GetFirstViewPosition();
		while (pos)
		{    
			CView* v = m_curDoc->GetNextView(pos);
			CPosEdMainView* vv = dynamic_cast<CPosEdMainView*>(v);
			if (vv)
			{
				vv->OnDeleteObject();
				break;
			}
		}
	}

	OnNamedselDelete();  
}