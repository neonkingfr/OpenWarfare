#pragma once

#include <string>
#include <vector>

using std::string;
using std::vector;

class StringList
{
	string m_Filename;
	vector<string> m_List;

public:
	StringList();

	void Filename(const char* filename);

	bool Add(const string& stringItem, bool onlyIfNotExist = true);

	bool Save() const;
};