/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

// PoseidonEditor.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "PoseidonEditor.h"

#include <strstream>
using namespace std;
#include <el/Pathname/Pathname.h>

#include "PosMainFrm.h"
#include "PosChildFrm.h"
#include "PoseidonDoc.h"
#include "PoseidonView.h"

#include "..\PoseidonEdDlgs\AboutNewDlg.h"
#include "..\PoseidonEdDlgs\SplashLicenseDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPoseidonApp

BEGIN_MESSAGE_MAP(CPoseidonApp, CWinApp)
	//{{AFX_MSG_MAP(CPoseidonApp)
	ON_COMMAND(ID_HELP_ABOUT, OnHelpAbout)
	ON_COMMAND(ID_TOOLS_OPTIONS, OnToolsOptions)
	ON_COMMAND(ID_TOOLS_OPTSYSTEM, OnToolsOptsystem)
	ON_COMMAND(ID_PROJECT_NEW, OnProjectNew)
	ON_COMMAND(ID_PROJECT_OPEN, OnProjectOpen)
  ON_COMMAND(ID_PROJECT_OPEN_EX, OnProjectOpenEx)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPoseidonApp construction

CPoseidonApp::CPoseidonApp() 
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CPoseidonApp object

CPoseidonApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CPoseidonApp initialization

BOOL CPoseidonApp::InitInstance()
{
//TODO: call AfxInitRichEdit2() to initialize richedit2 library.
  // OLE 2.0 initialization
  if (!AfxOleInit())
  {
    AfxMessageBox(_T("Ole initialization failed."));
    return FALSE;
  }

  theApp.SetRegistryKey(_T("Bohemia Interactive Studio"));
    

	// Standard initialization
//	Enable3dControls();			

	// Mentar initialization
/*FLY
	if (!InitMntToolsLibrary())
		return FALSE;
*/

#ifndef _DEBUG
	// generov�n� informac� o u�ivateli
	// DoSaveMntTools("MntTools.dat", "JRC Interactive",
	//			   "JRC Interactive", "---");
	// na�ten� informac� o u�ivateli
/*FLY
	if (!DoLoadMntTools())
		return FALSE;
*/
#endif

	// Parse command line for standard shell commands, DDE, file open

	ParseCommandLine(m_cCommandLineInfo);

	// Enable start window
	//FLYEnableStartWnd(cmdInfo.m_bShowSplash);

	if (!DoInitPosEdSystem((LPCTSTR) m_cCommandLineInfo.GetConfFile()))
	{	// nelze inicializovat prost�ed� programu
		AfxMessageBox(IDS_ERROR_INIT_APP,MB_OK|MB_ICONSTOP);
		return FALSE;
	}
  
	
	// Prost�ed� aplikace
	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	if (pEnvir == NULL) return FALSE;

	// create file MRU since nMaxMRU not zero
	m_pRecentFileList = new CRecentFileList(0, _T(""), _T(""), 4);
	if (m_pRecentFileList)
	{
		for(int i = 0; i<MAX_RECENT_FILES; i++)
		{
			m_pRecentFileList->m_arrNames[i] =
				pEnvir->m_optPosEd.m_sRecentFile[i];
		}
	}

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.
	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(

#ifdef __ONLY_FOR_PUBLIC__
		// *******************
		// PUBLIC EDITION ONLY
		// *******************
		IDR_POSEIDTYPE_PUBLIC,
#else
	#ifdef __ONLY_FOR_VBS__
		// ****************
		// VBS EDITION ONLY
		// ****************
		IDR_POSEIDTYPE_REDUCED,
	#else
		// ********************
		// new internal version
		// ********************
		IDR_POSEIDTYPE_REDUCED,
		// ********************
		// old internal version
		// ********************
//		IDR_POSEIDTYPE,
	#endif
#endif

		RUNTIME_CLASS(CPoseidonDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CPoseidonView));
	AddDocTemplate(pDocTemplate);

	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;

#ifdef __ONLY_FOR_PUBLIC__
		// *******************
		// PUBLIC EDITION ONLY
		// *******************
		if (!pMainFrame->LoadFrame(IDR_MAINFRAME_PUBLIC)) return FALSE;
#else
	#ifdef __ONLY_FOR_VBS__
		// ****************
		// VBS EDITION ONLY
		// ****************
		if (!pMainFrame->LoadFrame(IDR_MAINFRAME_REDUCED)) return FALSE;
	#else
		// ********************
		// new internal version
		// ********************
		if (!pMainFrame->LoadFrame(IDR_MAINFRAME_REDUCED)) return FALSE;
		// ********************
		// old internal version
		// ********************
//		if (!pMainFrame->LoadFrame(IDR_MAINFRAME)) return FALSE;
	#endif
#endif

	// Dispatch commands specified on the command line
	if (m_cCommandLineInfo.m_nShellCommand != CCommandLineInfo::FileNew)
		if (!ProcessShellCommand(m_cCommandLineInfo))
			return FALSE;

	m_pMainWnd = pMainFrame;

	// p�vodn� velikost okna
	m_pMainWnd->SetWindowPos(NULL,
			pEnvir->m_optPosEd.m_nLastXPos,
			pEnvir->m_optPosEd.m_nLastYPos,
			pEnvir->m_optPosEd.m_nLastCXPos,
			pEnvir->m_optPosEd.m_nLastCYPos, SWP_NOZORDER);
	if ((pEnvir->m_optPosEd.m_bMaximizeApp)||
		(pEnvir->m_optPosEd.m_bLastMaximize))
    	m_nCmdShow = SW_SHOWMAXIMIZED;

	// poloha BlockBars
	pMainFrame->SetDockState(pEnvir->m_appDockState);

	// ***************************************************
	// new internal version / public edition / vbs edition
	// ***************************************************
	// hide Scc toolbar
	pMainFrame->ShowControlBar(&pMainFrame->m_wndSccIntegBar, FALSE, FALSE);

	// The main window has been initialized, so show and update it.
	if (!WaitStartWnd(m_nCmdShow))
	{
		pMainFrame->ShowWindow(m_nCmdShow);
		pMainFrame->UpdateWindow();
	}

#ifdef __SHOW_EULA__
	// ***************************
	// PUBLIC and VBS EDITION ONLY
	// ***************************
	// splash window for license agreement
	int agreed = theApp.GetProfileIntA(_T("License"), "Agreed", 0);

	if (agreed == 0)
	{
		AfxInitRichEdit();
		CSplashLicenseDlg licenseDlg;
		if (licenseDlg.DoModal() != IDOK) 
		{
			return FALSE;
		}

		bool res = theApp.WriteProfileInt(_T("License"), "Agreed", 1);
		if (!res)
		{
			AfxMessageBox("Failed to write to registry", MB_OK | MB_ICONSTOP);
			return FALSE;
		}
	}
	INT_PTR
#endif

	// inicializuji gener�tor n�hodn�ch ��sel
	srand((unsigned)time(NULL));

	return TRUE;
}

int CPoseidonApp::ExitInstance() 
{
	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	MASSERT(pEnvir != NULL);
	// p�epis recent files
	if (m_pRecentFileList)
	{
		for(int i = 0; i<MAX_RECENT_FILES; i++)
		{
			pEnvir->m_optPosEd.m_sRecentFile[i] =
				m_pRecentFileList->m_arrNames[i];
		}
	}
	// ulo�en� parametre� aplikace
	pEnvir->m_optPosEd.SaveParams();
	if (m_pRecentFileList)
	{
		delete m_pRecentFileList;
		m_pRecentFileList = NULL;
	}
	// Cleanup DAO if necessary
	if (m_lpfnDaoTerm != NULL)
		(*m_lpfnDaoTerm)();
    // TODO: VS7 has different MFC
	//return m_msgCur.wParam; 
    DoDonePosEdSystem();
    return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CPoseidonApp commands

BOOL CPoseidonApp::OnIdle(LONG lCount) 
{
	CMainFrame* pMainFrame = (CMainFrame*)m_pMainWnd;
	CFrameWnd*	pFrameWnd  = pMainFrame->GetActiveFrame( );
	CDocument*  pActivDoc  = NULL;
	if ((pFrameWnd)&&(pFrameWnd!=pMainFrame))
		pActivDoc = pFrameWnd->GetActiveDocument();
	pMainFrame->m_wndObjPanel.UpdateDocData((CPosEdMainDoc*)pActivDoc,FALSE);
  pMainFrame->_namedSelectionBar.CheckDocChanged((CPosEdMainDoc*)pActivDoc);

	return CWinApp::OnIdle(lCount);
}

/////////////////////////////////////////////////////////////////////////////
// menu Help
void CPoseidonApp::OnHelpAbout() 
{	
	CAboutNewDlg aboutDlg;
	aboutDlg.DoModal();

//  Old about dialog
//	DoAboutPosEdApp(IDR_MAINFRAME); 
}

/////////////////////////////////////////////////////////////////////////////
// menu Tools
void CPoseidonApp::OnToolsOptions() 
{
	if (DoSetupPosEdOptions())
	{	// zm�na parametr� aplikace
		CUpdatePar cUpdate(CUpdatePar::uvfChngOpts);
		UpdateAllDocs(CUpdatePar::uvfChngOpts,&cUpdate);
	}
}

void CPoseidonApp::OnToolsOptsystem() 
{	// syst�mov� options
	DoSetupSystemOptions();
}

/////////////////////////////////////////////////////////////////////////////
// menu project

void CPoseidonApp::OnProjectNew() 
{	
	TCHAR dir[512];
	GetCurrentDirectory(512, dir);
	OnFileNew(); 
	SetCurrentDirectory(dir);
}

void CPoseidonApp::OnProjectOpen() 
{	
	TCHAR dir[512];
	GetCurrentDirectory(512, dir);
	OnFileOpen(); 
	SetCurrentDirectory(dir); 
}

void CPoseidonApp::OnProjectOpenEx()
{	
	g_bLoadLODShapes = FALSE;
	TCHAR dir[512];
	GetCurrentDirectory(512, dir);
	OnFileOpen();
	SetCurrentDirectory(dir);
	g_bLoadLODShapes = TRUE;
}

void CPoseidonApp::OnFileOpen()
{
  CString sFilter,sDir,sFile;
  
  sDir = GetPosEdEnvironment()->m_optPosEd.m_sRecentFile[0];
  SeparateDirFromFileName(sDir,sDir);
  MakeShortDirStr(sDir,sDir);

  // p��prava CFileDialog
  CFileDialog dlg(TRUE);  
  dlg.m_ofn.Flags |= OFN_FILEMUSTEXIST;  
 
  sFilter += _T("*.pew");
  sFilter += (TCHAR)(_T('\0'));
  sFilter += _T("*.pew");
  sFilter += (TCHAR)(_T('\0'));
  //sFilter += (TCHAR)(_T('\0'));

  sFilter += _T("*.XYZ");
  sFilter += (TCHAR)(_T('\0'));
  sFilter += _T("*.XYZ");
  sFilter += (TCHAR)(_T('\0'));
  //sFilter += (TCHAR)(_T('\0'));

  sFilter += _T("*.pbl");
  sFilter += (TCHAR)(_T('\0'));
  sFilter += _T("*.pbl");
  sFilter += (TCHAR)(_T('\0'));
  sFilter += (TCHAR)(_T('\0'));

  dlg.m_ofn.nMaxCustFilter = 1;
  dlg.m_ofn.lpstrFilter = sFilter;
  dlg.m_ofn.lpstrDefExt = _T("pew");
  //dlg.m_ofn.lpstrTitle  = sTitle;
  dlg.m_ofn.lpstrFile = sFile.GetBuffer(_MAX_PATH);
  dlg.m_ofn.lpstrInitialDir = sDir;
  BOOL bRes = (dlg.DoModal()==IDOK)?TRUE:FALSE;
  sFile.ReleaseBuffer();
  if (bRes)
  {
    OpenDocumentFile(sFile);
    if (m_pRecentFileList)
    {
      for(int i = 0; i<MAX_RECENT_FILES; i++)
      {
        GetPosEdEnvironment()->m_optPosEd.m_sRecentFile[i] =
          m_pRecentFileList->m_arrNames[i];
      }

      GetPosEdEnvironment()->m_optPosEd.SaveParams();
    }
  }
}

//////////////////////////////////////////////////////////////////////////////
// Parse command line
void CPosEdCommandLineInfo::ParseParam( LPCTSTR lpszParam, BOOL bFlag, BOOL bLast )
{
    if (bFlag)
    {
        if (_tcscmp(lpszParam, _T("cfg")) == 0)
        {
            m_eLastFlag = ED_CONF_FILE;
            return;
        }

        CCommandLineInfo::ParseParam( lpszParam, bFlag, bLast );
        return;
    }

    // value
    switch(m_eLastFlag)
    {
    case ED_CONF_FILE:
        m_pszConfFile = lpszParam;
        break;
    default:;
        CCommandLineInfo::ParseParam( lpszParam, bFlag, bLast );
    }   
}


//Folowing lines solves some link errors. Don't remove them!
#if 0
#ifndef _DEBUG
#undef __argv
#undef __argc
extern "C"
{
 int __argc;          /* count of cmd line args */
 char ** __argv;      /* pointer to table of cmd line args */
 wchar_t ** __wargv;  /* pointer to table of wide cmd line args */
 int _iob; 
 int _mbctype;
}
#endif
#endif