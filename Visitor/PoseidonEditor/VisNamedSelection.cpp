#include <es/strings/rstring.hpp>
#include ".\visnamedselection.h"

bool VisSelection::IsSelected(NamSelItem::ObjectType type, unsigned int id) const
{
	BTreeIterator<NamSelItem,BTreeStaticCompareDefUnset> iter(_selection);
	iter.BeginFrom(NamSelItem(id, type, false));
	NamSelItem* it = iter.Next();
	if (it == 0 || it->_type != type) return false;
	if (it->_endOfRange) return true;
	return it->_id == id;
}

void VisSelection::Include(NamSelItem::ObjectType type, unsigned int id)
{
	BTreeIterator<NamSelItem,BTreeStaticCompareDefUnset> iter(_selection);
	iter.BeginFrom(NamSelItem(id, type, false));  
	NamSelItem* it = iter.Next();
	//previous item with id-1 (can be null if doesn't exist
	NamSelItem* itb = _selection.Find(NamSelItem(id - 1, type));
	bool isnext = false;
	if (it != 0 && it->_type == type)
	{
		if (it->_endOfRange) return; //already selected, return now
		if (it->_id-1 == id) //next object is selected
		{
			isnext = true;
			NamSelItem* it2 = iter.Next();
			if (it2 && it2->_type == type && it2->_endOfRange) //next object is range
			{
				// remove old begin of range
				_selection.Remove(*it);
			}
			else//next object is separate object
			{
				//mark this as end of range;
				it->_endOfRange = true;
			}
		}
	}
	//if there is previous item and it is end of range
	if (itb && itb->_endOfRange)
	{
		//remove end of range mark
		_selection.Remove(*itb);
		if (!isnext) _selection.Add(NamSelItem(id, type, true));
	}  
	else
	{    
		_selection.Add(NamSelItem(id, type, itb && !itb->_endOfRange));    
	}
}

NamSelItem::ObjectType VisSelection::NearNextObjectType(NamSelItem::ObjectType type) const
{
	BTreeIterator<NamSelItem, BTreeStaticCompareDefUnset> iter(_selection);
	iter.BeginFrom(NamSelItem(0, type, false));  
	NamSelItem* it = iter.Next();
	if (it == 0) 
	{
		return NamSelItem::objNoMoreTypes;
	}
	else
	{
		return it->_type;
	}
}

void VisSelection::Include(NamSelItem::ObjectType type, unsigned int first, unsigned int last)
{
	//solve border conditions
	//if first is bigger then last, swap arguments
	if (first > last) return Include(type, last, first);
	//to add range, include first item and last item
	Include(type, first);
	//exit in case, where range consists on one item
	if (first == last) return;
	//insert last item as end of range
	Include(type, last);
	//now search all marks between first and last item
	BTreeIterator<NamSelItem, BTreeStaticCompareDefUnset> iter(_selection);
	//start at one item above the first
	NamSelItem beg(first + 1, type);
	iter.BeginFrom(beg);
	NamSelItem* it = iter.Next();  
	//cycle until last item is found
	while (it && it->_type == type && it->_id < last)
	{
		//remove any mark inside of range
		_selection.Remove(*it);
		//after removing, we must restart searching
		iter.BeginFrom(beg);
		//get next mark
		it = iter.Next();
	}
	//finally, we found end of range. If it is not last item,
	//it was included into wide range. Otherwise, mark last item as end of range  
	if (it->_id == last && it->_type == type) it->_endOfRange = true;
}

void VisSelection::Exclude(NamSelItem::ObjectType type, unsigned int id)
{
	BTreeIterator<NamSelItem, BTreeStaticCompareDefUnset> iter(_selection);
	iter.BeginFrom(NamSelItem(id, type));
	NamSelItem* it = iter.Next();
	//not found, item is already excluded;
	if (it == 0 || it->_type != type) return;
	//found item, that is end of range  
	if (it->_endOfRange)
	{
		if (it->_id == id) //excluded item is marked as end of range
		{
			//look, if item id-1 marks begin of range
			NamSelItem* prev = _selection.Find(NamSelItem(id - 1, type));
			//when it does
			if (prev)
			{
				//remove end of range mark, and make previous item single
				_selection.Remove(*it);
			}
			else
			{
				//range must be shorted. Following operation is valid, because btree order is not corrupted
				it->_id--;
			}
		}
		else //excluded item is somewhere inside of range
		{
			//look, if item id+1 is mark of end of range
			if (it->_id == id + 1)
			{
				//when it does
				//make item single
				it->_endOfRange = false;
			}
			else
			{
				//create new begin of range
				_selection.Add(NamSelItem(id + 1, type, false));
			}
			//look, if item id-1 marks begin of range
			NamSelItem* prev = _selection.Find(NamSelItem(id - 1, type));
			if (!prev)
			{//no, so range continues, then new end of range
				_selection.Add(NamSelItem(id - 1, type, true));
			}
		}
	}
	//we found begin of range
	else if (it->_id == id)
	{
		//what is next item?
		NamSelItem* x = iter.Next();
		//next item was found and it is end of range
		if (x && x->_type == type && x->_endOfRange)
		{
			//next item is item id+1
			if (x->_id == id + 1)
			{
				//make item single
				x->_endOfRange = false;
				//remove excluded item.
				_selection.Remove(*it);
			}
			else
			{
				//short range;
				it->_id++;
			}
		}
		else //this item is single
		{
			//remove item.
			_selection.Remove(*it);
		}
	}
	//else item is not included
}

void VisSelection::Exclude(NamSelItem::ObjectType type, unsigned int first, unsigned int last)
{
	//solve border conditions
	//if first is bigger then last, swap arguments
	if (first > last) return Exclude(type, last, first);
	//to remove range, exclude first item and last item
	Exclude(type, first);
	//exit in case, where range consists on one item
	if (first == last) return;
	//remove last item as end of range
	Exclude(type, last);
	//now search all marks between first and last item
	BTreeIterator<NamSelItem, BTreeStaticCompareDefUnset> iter(_selection);
	NamSelItem beg(first, type);
	iter.BeginFrom(beg);
	NamSelItem* it = iter.Next();  
	//cycle until last item is found
	while (it && it->_type == type && it->_id <= last)
	{
		//remove any mark inside of range
		_selection.Remove(*it);
		//after removing, we must restart searching
		iter.BeginFrom(beg);
		//get next mark
		it = iter.Next();
	}
}

void VisSelection::SetSelected(NamSelItem::ObjectType type, unsigned int id, bool select)
{
	if (select) 
	{
		Include(type, id);
	}
	else 
	{
		Exclude(type, id);
	}
}

void VisSelection::SetSelected(NamSelItem::ObjectType type, unsigned int first, unsigned int last, bool select)
{
	if (select) 
	{
		Include(type, first, last);
	}
	else 
	{
		Exclude(type, first, last);
	}
}
void VisSelection::Clear()
{
	_selection.Clear();
}

void VisSelection::Save(std::ostream& out) const
{
	unsigned long cnt = _selection.Size();
	out.write((char*)&cnt, sizeof(cnt));
	BTreeIterator<NamSelItem, BTreeStaticCompareDefUnset> iter(_selection);
	NamSelItem* it = iter.Next(); 
	while (it)
	{
		out.write((char*)it, sizeof(*it));
		it = iter.Next();
	}
}

bool VisSelection::Load(std::istream& in)
{
	Clear();
	unsigned long cnt = 0;
	in.read((char*)&cnt, sizeof(cnt));
	if (in.gcount() != sizeof(cnt)) return false;
	for (unsigned int i = 0; i < cnt; ++i)
	{
		NamSelItem it;
		in.read((char*)&it,sizeof(it));
		if (in.gcount() != sizeof(it)) return false;
		_selection.Add(it);
	}
	return true;
}

void VisNamedSelection::Save(std::ostream& out) const
{
	unsigned long nameLen = _name.GetLength();
	if (nameLen > 0xFFFF) nameLen = 0xFFFF;
	unsigned short len = (unsigned short)nameLen;
	out.write((char*)&nameLen, 2);
	out.write(_name.Data(), nameLen);
	VisSelection::Save(out);
}

bool VisNamedSelection::Load(std::istream& in)
{
	unsigned short nameLen = 0;
	in.read((char*)&nameLen, 2);
	if (in.gcount() != 2) return false;
	char* buff = _name.CreateBuffer(nameLen);
	in.read(buff, nameLen);
	buff[nameLen] = 0;
	if (in.gcount() != nameLen) return false;
	return VisSelection::Load(in);
}

namespace Functors
{
	class RelocateList
	{
		const Array<int>& imap;
		mutable AutoArray<unsigned int, MemAllocStack<int, 256> > firstMap;
		mutable AutoArray<unsigned int, MemAllocStack<int, 256> > lastMap;
  public:
		RelocateList(const Array<int>& imap)
		: imap(imap) 
		{
		}

		int operator () (NamSelItem::ObjectType type, unsigned int first, unsigned int last) const
		{
			unsigned int fm = (unsigned)imap.Size() > first ? imap[first] : first;
			unsigned int lm = (unsigned)imap.Size() > last ? imap[last] : last;
			unsigned int lm1 = (unsigned)imap.Size() > (last + 1) ? imap[last + 1] : lm + 1;

			if (first != last && fm == lm)
			{
				if (lm1 == lm) return 0;
			}
			if (lm1 == lm)
			{
				lm--;
			}
			if (lm < fm) return 0;
			firstMap.Add(fm);
			lastMap.Add(lm);
			return 0;
		}

		const Array<unsigned int>& FirstMap() const 
		{
			return firstMap;
		}

		const Array<unsigned int>& LastMap() const 
		{
			return lastMap;
		}
	};
}

void VisSelection::RemapIndexes(const Array<int>& imap, NamSelItem::ObjectType type)
{
	Functors::RelocateList rl(imap);
	ForEachSelected(rl, &type);
	const Array<unsigned int>& firstMap = rl.FirstMap();
	const Array<unsigned int>& lastMap = rl.LastMap();
	Exclude(type, 0, INT_MAX);
	for (int i = 0; i < firstMap.Size(); ++i)
	{
		Include(type, firstMap[i], lastMap[i]);
	}
}

namespace Functors
{
	class TestSelectedRanges
	{
		const VisSelection& test;
	public:
		TestSelectedRanges(const VisSelection& test)
		: test(test) 
		{
		}

		int operator() (NamSelItem::ObjectType obj, int from, int to) const
		{
			return test.IsSelected(obj, from, to) ? 0 : 1;
		}
	};

	class IncludeSelectedRanges
	{
		VisSelection& ss;

	public:
		IncludeSelectedRanges(VisSelection& ss)
		: ss(ss) 
		{
		}

		int operator()(NamSelItem::ObjectType obj, int from, int to) const
		{
			ss.Include(obj, from, to);
			return 0;
		}
	};

	class ExcludeSelectedRanges
	{
		VisSelection& ss;

	public:
		ExcludeSelectedRanges(VisSelection& ss)
		: ss(ss) 
		{
		}

		int operator () (NamSelItem::ObjectType obj, int from, int to) const
		{
			ss.Exclude(obj, from, to);
			return 0;
		}
	};

	class CropSelectedRanges
	{
		const VisSelection& processed;
		VisSelection& ss;
		mutable int _excludeLast;
		mutable NamSelItem::ObjectType _excludeLastObjType;
  public:
		CropSelectedRanges(const VisSelection& processed, VisSelection& ss)
		: processed(processed)
		, ss(ss)
		, _excludeLast(0)
		, _excludeLastObjType(processed.NearNextObjectType((NamSelItem::ObjectType)0)) {}

		int operator () (NamSelItem::ObjectType obj, int from, int to) const
		{
			while (_excludeLastObjType != obj)
			{
				ss.Exclude(_excludeLastObjType, _excludeLast, INT_MAX);
				_excludeLastObjType = processed.NearNextObjectType(_excludeLastObjType);
				if (_excludeLastObjType == NamSelItem::objNoMoreTypes) return 1;
				_excludeLast = 0;
			}
			if (from!=_excludeLast) ss.Exclude(obj, _excludeLast, from - 1);
			_excludeLast = to + 1;
			return 0;
		}

		~CropSelectedRanges()
		{
			while (_excludeLastObjType != NamSelItem::objNoMoreTypes)
			{
				ss.Exclude(_excludeLastObjType, _excludeLast, INT_MAX);
				_excludeLastObjType = processed.NearNextObjectType(_excludeLastObjType);
				_excludeLast = 0;
			}
		}
	};
}

bool VisSelection::IsSelected(const VisSelection& sel) const
{
	int res = sel.ForEachSelected(Functors::TestSelectedRanges(*this));
	return res == 0;
}

bool VisSelection::IsSelected(NamSelItem::ObjectType type, unsigned int first, unsigned int last) const
{
	if (first == last) return IsSelected(type, first);

	BTreeIterator<NamSelItem, BTreeStaticCompareDefUnset> iter1(_selection);
	iter1.BeginFrom(NamSelItem(first, type, false));
	NamSelItem* it1 = iter1.Next();
	BTreeIterator<NamSelItem, BTreeStaticCompareDefUnset> iter2(_selection);
	iter2.BeginFrom(NamSelItem(last, type, false));
	NamSelItem* it2 = iter2.Next();
	if (it1 == 0 || it1->_type != type || it2 == 0 || it2->_type != type) return false;

	if (it2->_endOfRange)
	{
		if (it1->_id == first && !it1->_endOfRange)
		{
			it1 = iter1.Next();
			if (it1 == 0 || it1->_type != type) return false;
			if (it1->_id == it2->_id && it1->_endOfRange) return true;
		}
	}
	return false;
}

void VisSelection::Include(const VisSelection& sel)
{
	sel.ForEachSelected(Functors::IncludeSelectedRanges(*this));
}

void VisSelection::Exclude(const VisSelection& sel)
{
	sel.ForEachSelected(Functors::IncludeSelectedRanges(*this));
}

bool VisSelection::IsEmpty() const
{
	return _selection.Size() == 0;
}

void VisSelection::Crop(const VisSelection& sel)
{
	sel.ForEachSelected(Functors::CropSelectedRanges(sel, *this));
}