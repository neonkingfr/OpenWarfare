#include <es/strings/rstring.hpp>
#include ".\visselectionlist.h"

VisNamedSelection* VisSelectionList::CreateSelection(const VisNamedSelection& source)
{
	VisNamedSelection* curSel = GetSelection(source.GetName());
	if (curSel) 
	{
		*curSel = source;
	}
	else
	{
		_selList.Add(curSel = new VisNamedSelection(source));
		_selIndex.Add(curSel);
	}
	return curSel;
}

void VisSelectionList::AddSelectionInstance(VisNamedSelection* instance)
{
	VisNamedSelection* curSel = GetSelection(instance->GetName());
	if (curSel) 
	{
		DeleteSelection(curSel->GetName());
	}
	_selList.Add(curSel = instance);
	_selIndex.Add(curSel);
}

VisNamedSelection* VisSelectionList::CreateSelection(const RStringI& name)
{
	VisNamedSelection tmp(name);
	return CreateSelection(tmp);
}

VisNamedSelection* VisSelectionList::GetSelection(const RStringI& name)
{
	VisNamedSelection** x = _selIndex.Find(&VisNamedSelection(name));
	return x ? *x : 0;
}

const VisNamedSelection* VisSelectionList::GetSelection(const RStringI& name) const
{
	VisNamedSelection** x = _selIndex.Find(&VisNamedSelection(name));
	return x ? *x : 0;
}

void VisSelectionList::DeleteSelection(const RStringI& name)
{
	VisNamedSelection* sel = _selIndex.Remove(&VisNamedSelection(name));
	for (int i = 0; i < _selList.Size(); ++i) 
	{
		if (_selList[i].GetRef() == sel)
		{
			_selList.Delete(i);
			break;      
		}
	}
}

void VisSelectionList::Save(std::ostream& out) const
{
	unsigned long cnt = Size();
	out.write((char*)&cnt, sizeof(cnt));
	BTreeIterator<VisNamedSelection*,BTreeRefCompare> iter(_selIndex);
	VisNamedSelection** item = iter.Next();
	while (item)
	{
		(*item)->Save(out);
		item = iter.Next();
	}
}

bool VisSelectionList::Load(std::istream& in)
{
	unsigned long cnt = 0;
	in.read((char*)&cnt, sizeof(cnt));
	if (in.gcount() != sizeof(cnt)) return false;
	for (unsigned int i = 0; i < cnt; ++i)
	{
		VisNamedSelection x;
		if (x.Load(in) == false) return false;
		CreateSelection(x);
	}
	return true;
}

TypeIsMovable(SRef<VisNamedSelection>);
