/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

// PosMainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(_POSMAINFRM_H_)
#define _POSMAINFRM_H_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <winsock.h>
#include "ruler.h"
#include <El/Scc/Scc.hpp>
#include "DlgScriptOutput.hpp"
#include <el/TCPIpBasics/IPA.h>
#include <El/TCPIpBasics/Socket.h>

#include <strstream>
using namespace std;
#include <el/Pathname/Pathname.h>

#include "DlgManageScripts.h"
#include "DlgNamedSelectionBar.h"

#include "../VisitorExchangeInterface/VisViewerPipe.h"
#include "ViewerView.h"


class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

// Attributes
protected:  
  MsSccFunctions _sccFunctions; //< Source Safe integration
  CScriptOutputDlg _scriptOutputDlg;

  ScriptMenuItemList<> _scriptMenuCmds;

  ViewerView _viewerview;
  Ref<VisViewerPipe> _visexchange;
  CWnd m_connectWnd;

public:
	// CMessageFront	m_MessageFront;
//	void SendRealMessage(const SPosMessage&);
//	void HandleRealMessages();
	bool IsConnected(); 
  
  void Disconnect(bool quit);
  IVisViewerExchange *GetViewerInterface() {return _visexchange;}

//	bool ReceiveMessage(PSPosMessage	&sMsg);
	void HandleError(bool send);
	
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();

	// control bar embedded members
public:
	CStatusBar			m_wndStatusBar;
	CToolBar			  m_wndToolBar;
	CToolBar			  m_wndViewBar;
  CToolBar			  m_wndSccIntegBar;
	CPosObjectsPanel	m_wndObjPanel;
  DlgNamedSelectionBar _namedSelectionBar;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg void OnViewConfig();
	afx_msg void OnViewViewbar();
	afx_msg void OnUpdateViewViewbar(CCmdUI* pCmdUI);
    afx_msg LRESULT OnIncomePipeConnection(WPARAM wParam,LPARAM lParam);
	afx_msg void OnSelchangeObjectType();
	afx_msg void OnViewEditpanel();
	afx_msg void OnUpdateViewEditpanel(CCmdUI* pCmdUI);
	afx_msg void OnViewBldCursor();
	afx_msg void OnUpdateViewBldCursor(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStatusInfoObject(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStatusInfoCurpos(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStatusInfoPrimtxtr(CCmdUI* pCmdUI);
	afx_msg void OnUpdateStatusInfoScndtxtr(CCmdUI* pCmdUI);
  afx_msg void OnUpdateStatusInfoBuldozer(CCmdUI* pCmdUI);
	afx_msg void OnToolsPanelType0();
	afx_msg void OnToolsPanelType1();
	afx_msg void OnToolsPanelType2();
	afx_msg void OnToolsPanelType3();
	afx_msg void OnToolsPanelType4();
	afx_msg void OnToolsPanelType5();
	afx_msg void OnToolsPanelType6();
	afx_msg void OnToolsPanelType7();
	afx_msg void OnToolsPanelType8();
	afx_msg void OnToolsPanelType9();
	afx_msg void OnToolsPanelType10();

  afx_msg void OnLaunchBuldozer();
  afx_msg void OnRenameDoc();
  afx_msg void OnTimer(UINT_PTR nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnUpdateProjectMakeReal(CCmdUI *pCmdUI);
  afx_msg void OnUpdateLaunchBuldozer(CCmdUI *pCmdUI);
  afx_msg void OnProjectMakeReal();  
protected:
  virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);  
public:
  afx_msg void OnViewRuler();
  afx_msg void OnUpdateViewRuler(CCmdUI *pCmdUI);

  //SCC Integration
protected:
  void InitSCC();
  afx_msg void OnSccIntegBinding();
  afx_msg void OnSccIntegBindingUpdate(CCmdUI *pCmdUI);
  afx_msg void OnSccIntegRefresh();
  afx_msg void OnSccIntegRefreshUpdate(CCmdUI *pCmdUI);
public:
  MsSccFunctions& GetSccFunctions() {return _sccFunctions;};

//  afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
  afx_msg void OnViewSsIntegToolbar();
  afx_msg void OnUpdateViewSsIntegToolbar(CCmdUI *pCmdUI);
  void OnScript(CString& scriptPath);
  afx_msg void OnScriptPanel();
  afx_msg void OnUpdateScriptPanel(CCmdUI *pCmdUI);
  afx_msg void OnUpdateScriptRun(CCmdUI *pCmdUI);
  afx_msg void OnUpdateScriptBrowse(CCmdUI *pCmdUI);
  afx_msg void OnScriptsManagescripts();
  void SaveScriptMenu();
  afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
  afx_msg void OnRunScriptFromMenu(UINT cmd);
  afx_msg void OnScriptsAddrunscript();

  afx_msg LRESULT OnScriptUnlockRequest(WPARAM wParam, LPARAM lParam);

  void MakeRealDoc(CPosEdMainDoc *doc);



};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(_POSMAINFRM_H_)
