/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

// PoseidonDoc.h : interface of the CPoseidonDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(_POSEIDONDOC_H_)
#define _POSEIDONDOC_H_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <El/Scc/Scc.hpp>
#include <iostream>

class CPoseidonDoc : public CPosEdMainDoc
{
	DECLARE_DYNCREATE(CPoseidonDoc)

	typedef CPosEdMainDoc base;

protected: 

	bool _askForCheckOut; //< Used in function SetModifiedFlag
	SCCSTAT _status; // Keep SS status and do not ask allways...

	CPoseidonDoc();
	virtual ~CPoseidonDoc();

	// Overrides

			// zpracov�n� zpr�v z preview
///	virtual void SendRealMessage(const SPosMessage&) const;

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPoseidonDoc)
	public:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:
	//{{AFX_MSG(CPoseidonDoc)
	afx_msg void OnProjectSave();
	afx_msg void OnUpdateProjectSave(CCmdUI* pCmdUI);
	afx_msg void OnProjectSaveAs();
	afx_msg void OnToolsTemplTexture();
	afx_msg void OnToolsTemplNatobj();
	afx_msg void OnToolsTemplPplobj();

	afx_msg void OnToolsTemplNewRoadsobj();

	afx_msg void OnToolsTemplNets();
	afx_msg void OnToolsTemplWoods();
	afx_msg void OnProjectImportRelief();
	afx_msg void OnToolsProjectParams();
  
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	virtual void OnBuldConnect(IVisViewerExchange* realViewer);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	BOOL OnOpenpblDocument(const CString& cfgFileName);
	void OnRenameDoc();
	BOOL ReloadDocument(BOOL bLoadLODShapes = FALSE);
	// PBL + PNG suport
	afx_msg void OnProjectExportPNG();  
	afx_msg void OnProjectImportPNG();
	afx_msg void OnProjectImportWaterPNG();
	afx_msg void OnUpdateProjectImportPng(CCmdUI* pCmdUI);
	afx_msg void OnUpdateProjectExportPng(CCmdUI* pCmdUI);
	afx_msg void OnUpdateProjectImportWaterPng(CCmdUI* pCmdUI);

	afx_msg void OnTerrainExportASC();
	afx_msg void OnUpdateTerrainExportASC(CCmdUI* pCmdUI);
	afx_msg void OnTerrainExportXYZ();
	afx_msg void OnUpdateTerrainExportXYZ(CCmdUI* pCmdUI);

	// SCC
	afx_msg void OnSccIntegCheckOut();
	afx_msg void OnUpdateSccIntegCheckOut(CCmdUI* pCmdUI);
	afx_msg void OnSccIntegUndoCheckOut();
	afx_msg void OnUpdateSccIntegUndoCheckOut(CCmdUI* pCmdUI);
	virtual void SetModifiedFlag( BOOL bModified = TRUE);
	afx_msg void OnSccIntegGetLatest();
	afx_msg void OnUpdateSccIntegGetLatest(CCmdUI* pCmdUI);
	afx_msg void OnSccIntegGetLatestAll();
	afx_msg void OnUpdateSccIntegGetLatestAll(CCmdUI* pCmdUI);
	afx_msg void OnSccIntegCheckIn();
	afx_msg void OnUpdateSccIntegCheckIn(CCmdUI* pCmdUI);
	afx_msg void OnSccIntegAdd();
	afx_msg void OnUpdateSccIntegAdd(CCmdUI* pCmdUI);

	void UpdateSSStatus(LPCTSTR lpszPathName = NULL); // Update Source Safe status... 
	afx_msg void OnUpdateToolsTemplTexture(CCmdUI* pCmdUI);
	afx_msg void OnToolsImportSatellite();
	afx_msg void OnProjectImportFromLandBuilder();
	void ImportLandBuilderFile(std::istream& infile, const char* selname, bool newRoads);
	void RegisterNewItem(const CString& name, const CString& fname, int type = CPosObjectTemplate::objTpPeople);
	afx_msg void OnProjectImportXYZ();
	afx_msg void OnImportTemplates();

	afx_msg void OnProjectNewRoadsImportFromLandBuilder();

	// -------------------------------------------------------------------------- //
	// this is the new function added to allow to modify the project params using //
	// the new project params dialog                                              //
	// -------------------------------------------------------------------------- //
	afx_msg void OnToolsProjectParameters();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(_POSEIDONDOC_H_)
