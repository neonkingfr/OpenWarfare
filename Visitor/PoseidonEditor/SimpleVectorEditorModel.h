#pragma once

#include <map>
#include <el/BTree/Btree.h>
#include <iostream>
#include <es/strings/rstring.hpp>
#include "VisSelectionList.h"

class SimpleVectorEditorModel
{
public:

  static const char *SSpecialSelName;
  static const char *SSpecialSelected;
  static const char *SSpecialHidden;
  static const char *SSpecialLocked;
  enum PointType
  {
    ptImportant,
    ptHelper,
    ptDeleted    
  };

  static const int PropBaseStep=(1<<17);
  enum PropertyName
  {
    prFloatBase=0,
    prIntegerBase=PropBaseStep,
    prShortCharBase=PropBaseStep*2,
    prTextBase=PropBaseStep*3,
    prDataBase=PropBaseStep*4,
  };

  class Point: public Vector3P
  {
    PointType type:4;
    int prop:28;
  public:
    Point() {}
    Point(const Vector3 &other, PointType pt):Vector3(other),type(pt),prop(0) {}
    ClassIsMovable(Point);

    void SetDeleted() {type=ptDeleted;}
    bool IsDeleted() const {return type==ptDeleted;}
    PointType GetPointType() const {return type;}

    void SetPropertyId(int id) {prop=id;}
    int GetPropertyId() const {return prop;}
  };

  class Line
  {
    int _begin;
    int _end;
  public:
    Line() {}
    Line(int beg, int end):_begin(beg),_end(end) {}
    int Begin() const {return _begin;}
    int End() const {return _end;}
    
    static int Size() {return 2;}
    int operator[](int x) const {if (x) return End();else return Begin();}

    void SetDeleted() {_end=-1;}
    bool IsDeleted() const {return _end==-1;}
    

    ClassIsMovable(Line);
  };

  class PropertySelection: public VisNamedSelection
  {
    RString _value;
  public:
    PropertySelection(const char *name, const char *value=0):VisNamedSelection(name),_value(value) {}
    PropertySelection(const RStringI &name, const RString value=RString()):VisNamedSelection(name),_value(value) {}
    PropertySelection(const char *name, const char *value,const VisSelection &sel):VisNamedSelection(name,sel),_value(value) {}
    PropertySelection(const RStringI &name, const RString value,const VisSelection &sel):VisNamedSelection(name,sel),_value(value) {}

    template<class Str>
    void SetValue(const Str &value) {_value=value;}

    const RString &GetValue() const {return _value;}

    void Save(std::ostream &out) const;
    bool Load(std::istream &in);

    virtual int Compare(const VisNamedSelection &other) const;
  };



  class PropertyList
  {
    AutoArray<SRef<PropertySelection> > _selList;
    BTree<PropertySelection *,BTreeRefCompare> _selIndex;
  public:
    void AddSelectionInstance(PropertySelection *instance);
    const PropertySelection *GetSelection(const PropertySelection  &name) const;
    PropertySelection *GetSelection(const PropertySelection  &name);
    void DeleteSelection(const PropertySelection &name);

    template<class Functor> 
    int ForEach(Functor &funct, const PropertySelection *filter) const
    {
      BTreeIterator<PropertySelection *,BTreeRefCompare> iter(_selIndex);
      if (filter) iter.BeginFrom(const_cast<PropertySelection *>(filter));
      PropertySelection **sel=iter.Next();
      while (sel && (filter==0 || filter->GetName()==(*sel)->GetName()))
      {
        int res=funct(*sel);
        if (res) return res;
        sel=iter.Next();
      }
      return 0;
    }
    int Size() const {return _selList.Size();}
    void Save(std::ostream &out) const;
    bool Load(std::istream &in);
  };

  friend class LineGraph;
  class LineGraph: public std::multimap<int,int>
  {
  public:
    typedef std::pair<int,int> Pair;
    typedef std::multimap<int,int>::const_iterator Iterator;

    LineGraph(const SimpleVectorEditorModel &source);

    int CountJoints(int srcpt) const;
    int GetJoint(int srcpt, int index) const;

    friend class LineGraph;
    class JointList: public AutoArray<int,MemAllocStack<int,32> >
    {
    public:
      JointList(const LineGraph &graph, int point);
    };

    JointList GetJoints(int point) {return JointList(*this,point);}

  };





protected:
  AutoArray<Point> _points;
  AutoArray<Line> _lines;

  PropertyList _properties;


public:

  template<class Functor>
  bool ForEachPoint(const Functor &functor) const
  {
    for (int i=0;i<_points.Size();i++) if (!_points[i].IsDeleted()) 
      if (functor(i,_points[i])) return true;
    return false;
  }

  template<class Functor>
    bool ForEachLine(const Functor &functor) const
  {
    for (int i=0;i<_lines.Size();i++) if (!_lines[i].IsDeleted()) 
      if (functor(i,_lines[i])) return true;
    return false;
  }

  const Array<Point> &Points() const {return _points;}
  const Array<Line> &Lines() const {return _lines;}

  void SetPointPos(int index, const Point &pt) {_points[index]=pt;}
  void SetLine(int index, const Line &pt) {_lines[index]=pt;}

  LineGraph BuildGraph() const {return LineGraph(*this);}

  int AddPoint(const Point &pt) {return AddPoints(Array<const Point>(&pt,1));}
  int AddPoints(const Array<const Point> &pt);

  int AddLine(int begin, int end) {return AddLine(Line(begin,end));}
  int AddLine(const Line &line) {return AddLines(Array<const Line>(&line,1));}
  int AddLines(const Array<const Line> &lines);
  
  void Delete(const VisSelection &sel);
  void OptimizeAfterDelete();
  void InsertPoint(const Point &pt, int at);
  void InsertLine(const Line &pt, int at);

  template<class Functor>
  class ListProperties
  {
    const VisSelection &sel;
    Functor &functor;
  public:
    ListProperties(Functor &functor, const VisSelection &sel):sel(sel),functor(functor) {}
    int operator()(const PropertySelection *csel) const
    {
      if (csel->IsSelected(sel))
        return functor(csel);
      return false;
    }

  };

  void SetProperty(const char *name, const char *value, const VisSelection &selection);

  template<class Functor>
  bool ForEachProperty(Functor &functor, const PropertySelection *filter)
  {
    return _properties.ForEach(ListProperties<Functor>(functor,*filter),filter)!=0;
  }

  template<class Functor>
  bool ForEachProperty(Functor &functor)
  {
    _properties.ForEach(functor)
  }

  void UnsetProperty(const char *name, const VisSelection &selection);
  const char *GetLineProperty(int index, const char *name);
  const char *GetPointProperty(int index, const char *name);
  const PropertySelection *FindSelection(const char *name, const char *value) const;
  PropertySelection *FindSelection(const char *name, const char *value);

  void AddSelectionInstance(PropertySelection *instance) {_properties.AddSelectionInstance(instance);}  

  void Save(std::ostream &stm) const;
  void Load(std::istream &stm);

  unsigned int GetPointCount() const
  {
    return _points.Size();
  }
  unsigned int GetLineCount() const
  {
    return _lines.Size();
  }

};
