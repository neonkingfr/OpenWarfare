// DlgStopScript.cpp : implementation file
//

#include "stdafx.h"
#include <El\Evaluator\ScriptDebuggerBase.h>
#include "resource.h"
#include "DlgStopScript.h"
#include ".\dlgstopscript.h"

#define MSG_EXITDLG (WM_APP+1)

// DlgStopScript dialog

IMPLEMENT_DYNAMIC(DlgStopScript, CDialog)
DlgStopScript::DlgStopScript(ScriptDebuggerBase *dbg,const char *scriptName, CWnd* pParent /*=NULL*/)
	: CDialog(DlgStopScript::IDD, pParent),_dbg(dbg)
    , vScriptName(scriptName)
{
  _bgrDlg=AfxBeginThread(RunThread,this,THREAD_PRIORITY_NORMAL,0,CREATE_SUSPENDED,0);
  _bgrDlg->m_bAutoDelete=FALSE;
  _bgrDlg->ResumeThread();
  _callback=true;
}

DlgStopScript::~DlgStopScript()
{
  while (m_hWnd==0) Sleep(1);
  PostMessage(MSG_EXITDLG,0,0);
  WaitForSingleObject(_bgrDlg->m_hThread,INFINITE);
  delete _bgrDlg;
}

UINT DlgStopScript::RunThread(LPVOID ptr)
    {
      DlgStopScript *self=reinterpret_cast<DlgStopScript *>(ptr);
      self->Create(self->IDD,CWnd::GetDesktopWindow());      
      while (AfxPumpMessage());
      self->DestroyWindow();
      return 0;
    }

    
void DlgStopScript::ReplyFromApp(HWND hwnd,UINT uMsg, ULONG_PTR dwData, LRESULT lResult)
{
  DlgStopScript *self=reinterpret_cast<DlgStopScript *>(dwData);
  self->_callback=true;  
}

void DlgStopScript::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_FAKEPROGRESS, _progress);
  DDX_Text(pDX, IDC_NAME, vScriptName);
}


BEGIN_MESSAGE_MAP(DlgStopScript, CDialog)
  ON_WM_TIMER()
  ON_MESSAGE(MSG_EXITDLG,OnExitDlg)
END_MESSAGE_MAP()


// DlgStopScript message handlers

void DlgStopScript::OnCancel()
{
  _dbg->SetTraceMode(ScriptDebuggerBase::traceTerminate); 
}

BOOL DlgStopScript::OnInitDialog()
{
  CDialog::OnInitDialog();

  SetTimer(1,80,0);  
  // TODO:  Add extra initialization here

  CRect rc;
  AfxGetMainWnd()->GetWindowRect(&rc);
  CRect rc2;
  GetWindowRect(&rc2);  
  rc.top=rc.top+(rc.bottom-rc.top)/4;
  rc.bottom=rc.top+rc2.Size().cy;
  rc.left=(rc.right+rc.left-rc2.Size().cx)/2;
  rc.right=rc.left+rc2.Size().cx;
  MoveWindow(&rc);

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgStopScript::OnTimer(UINT nIDEvent)
{
  if (_callback)
  {
    SendMessageCallback(*AfxGetMainWnd(),WM_APP+9989,0,0,ReplyFromApp,(ULONG_PTR)this);
    if (IsWindowVisible()) 
    {
      ShowWindow(SW_HIDE);
      KillTimer(nIDEvent);
      SetTimer(nIDEvent,1000,0);
    }
    _callback=false;
  }
  else
  {
    if (!IsWindowVisible()) 
    {
      ShowWindow(SW_SHOW);
      SetWindowPos(&wndTopMost,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
      SetWindowPos(&wndNoTopMost,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
      KillTimer(nIDEvent);
      SetTimer(nIDEvent,80,0);
    }
    _progress.SetStop(_dbg->GetTraceMode()==_dbg->traceTerminate);
    _progress.Invalidate(FALSE);
  }
  CDialog::OnTimer(nIDEvent);
}

LRESULT DlgStopScript::OnExitDlg(WPARAM wParam,LPARAM lParam)
{
  EndDialog(0);
  PostQuitMessage(0);
  return 0;
}