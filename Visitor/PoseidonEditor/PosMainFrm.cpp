/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

// PosMainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "PoseidonEditor.h"
#include "PosMainFrm.h"
#include "PoseidonDoc.h"

#include "..\Script\script.hpp"
#include ".\posmainfrm.h"

#include <el/tcpipbasics/WSA.h>
#include <el/Evaluator/Addons/Dialogs/dialogs.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define WM_INCOME_MESSAGE (WM_APP+3801)
#define WM_INCOME_CONNECTION (WM_APP+3802)
#define WM_INCOME_PIPECONNECTION (WM_APP+3803)

#define ID_SCRIPT_MENU_BEGIN 30000
#define ID_SCRIPT_MENU_END 32767 

#define IDW_VIEW_BAR			(AFX_IDW_TOOLBAR+2)
#define IDW_EDIT_PANEL			(AFX_IDW_TOOLBAR+3)
#define IDW_SCC_INTEG_BAR			(AFX_IDW_TOOLBAR+4)

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

static CWSA staticWsa(0x101);

#define ON_WM_APP(msg, fcn) \
{ (msg), 0, 0, 0, AfxSig_vv, \
  (AFX_PMSG) (static_cast< void (AFX_MSG_CALL CMainFrame::*)(void) > (&fcn)) },

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
  ON_WM_APP(WM_RENAME_DOC, OnRenameDoc)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_COMMAND(ID_VIEW_CONFIG, OnViewConfig)
	ON_COMMAND(ID_VIEW_VIEWBAR, OnViewViewbar)
	ON_UPDATE_COMMAND_UI(ID_VIEW_VIEWBAR, OnUpdateViewViewbar)
	ON_CBN_SELCHANGE(IDC_COMBO_TYPE, OnSelchangeObjectType)
	ON_COMMAND(ID_VIEW_EDITPANEL, OnViewEditpanel)
	ON_UPDATE_COMMAND_UI(ID_VIEW_EDITPANEL, OnUpdateViewEditpanel)
	ON_COMMAND(ID_VIEW_BLD_CURSOR, OnViewBldCursor)
  ON_COMMAND_EX(ID_VIEW_NAMEDSELECTIONSPANEL,OnBarCheck)
  ON_UPDATE_COMMAND_UI(ID_VIEW_NAMEDSELECTIONSPANEL,OnUpdateControlBarMenu)
	ON_UPDATE_COMMAND_UI(ID_VIEW_BLD_CURSOR, OnUpdateViewBldCursor)
	ON_UPDATE_COMMAND_UI(ID_STATUS_ITEM_OBJECT, OnUpdateStatusInfoObject)
	ON_UPDATE_COMMAND_UI(ID_STATUS_ITEM_CURPOS, OnUpdateStatusInfoCurpos)
	ON_UPDATE_COMMAND_UI(ID_STATUS_ITEM_HEIGHT, OnUpdateStatusInfoCurpos)
	ON_UPDATE_COMMAND_UI(ID_STATUS_ITEM_PRIMTXTR, OnUpdateStatusInfoPrimtxtr)
	ON_UPDATE_COMMAND_UI(ID_STATUS_ITEM_SCNDTXTR, OnUpdateStatusInfoScndtxtr)
  ON_UPDATE_COMMAND_UI(ID_STATUS_ITEM_BULDOZER_CONN, OnUpdateStatusInfoBuldozer)

	ON_COMMAND(ID_TOOLS_PANEL_TYPE_0, OnToolsPanelType0)
	ON_COMMAND(ID_TOOLS_PANEL_TYPE_1, OnToolsPanelType1)
	ON_COMMAND(ID_TOOLS_PANEL_TYPE_2, OnToolsPanelType2)
	ON_COMMAND(ID_TOOLS_PANEL_TYPE_3, OnToolsPanelType3)
	ON_COMMAND(ID_TOOLS_PANEL_TYPE_4, OnToolsPanelType4)
	ON_COMMAND(ID_TOOLS_PANEL_TYPE_5, OnToolsPanelType5)
	ON_COMMAND(ID_TOOLS_PANEL_TYPE_6, OnToolsPanelType6)
	ON_COMMAND(ID_TOOLS_PANEL_TYPE_7, OnToolsPanelType7)
	ON_COMMAND(ID_TOOLS_PANEL_TYPE_8, OnToolsPanelType8)
	ON_COMMAND(ID_TOOLS_PANEL_TYPE_9, OnToolsPanelType9) 
	ON_COMMAND(ID_TOOLS_PANEL_TYPE_10, OnToolsPanelType10) 

	//}}AFX_MSG_MAP
  ON_UPDATE_COMMAND_UI(ID_PROJECT_MAKE_REAL, OnUpdateProjectMakeReal)
 // ON_COMMAND(ID_LAUNCH_BULDOZER, OnLaunchBuldozer)
 // ON_UPDATE_COMMAND_UI(ID_LAUNCH_BULDOZER, OnUpdateLaunchBuldozer)
  ON_COMMAND(ID_PROJECT_MAKE_REAL, OnProjectMakeReal)
  ON_COMMAND(ID_VIEW_RULER, OnViewRuler)
  ON_UPDATE_COMMAND_UI(ID_VIEW_RULER, OnUpdateViewRuler)
//  ON_WM_LBUTTONDOWN()
  //SCC Integration
  ON_COMMAND(ID_SCC_INTEG_BINDING, OnSccIntegBinding)
  ON_UPDATE_COMMAND_UI(ID_SCC_INTEG_BINDING, OnSccIntegBindingUpdate)
  ON_COMMAND(ID_SCC_INTEG_REFRESH, OnSccIntegRefresh)
  ON_UPDATE_COMMAND_UI(ID_SCC_INTEG_REFRESH, OnSccIntegRefreshUpdate)
  ON_COMMAND(ID_VIEW_SS_INTEG_TOOLBAR, OnViewSsIntegToolbar)
  ON_UPDATE_COMMAND_UI(ID_VIEW_SS_INTEG_TOOLBAR, OnUpdateViewSsIntegToolbar)  
  ON_COMMAND(ID_SCRIPT_PANEL, OnScriptPanel)
  ON_UPDATE_COMMAND_UI(ID_SCRIPT_PANEL, OnUpdateScriptPanel)
  ON_UPDATE_COMMAND_UI(IDC_SCRIPT_RUN, OnUpdateScriptRun)
  ON_UPDATE_COMMAND_UI(IDC_SCRIPT_BROWSE, OnUpdateScriptBrowse)
  ON_COMMAND(ID_SCRIPTS_MANAGESCRIPTS, OnScriptsManagescripts)
  ON_COMMAND_RANGE(ID_SCRIPT_MENU_BEGIN,ID_SCRIPT_MENU_END,OnRunScriptFromMenu)
  ON_MESSAGE(WM_INCOME_PIPECONNECTION,OnIncomePipeConnection)
  ON_WM_INITMENUPOPUP()
  ON_WM_TIMER()
  ON_COMMAND(ID_SCRIPTS_ADDRUNSCRIPT, OnScriptsAddrunscript)
  ON_MESSAGE(WM_DIALOGUNLOCKNOTIFY,OnScriptUnlockRequest)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_STATUS_ITEM_OBJECT,
	ID_STATUS_ITEM_CURPOS,
	ID_STATUS_ITEM_HEIGHT,
	ID_STATUS_ITEM_PRIMTXTR,
	ID_STATUS_ITEM_SCNDTXTR,
  ID_STATUS_ITEM_BULDOZER_CONN,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame() : _viewerview(*(_visexchange.GetRef()))
{
  _scriptOutputDlg.SetMainFrame(this);
  _viewerview.AddRef();
  
  InitSCC();
}

CMainFrame::~CMainFrame()
{
}


LRESULT CMainFrame::OnIncomePipeConnection(WPARAM wParam,LPARAM lParam)
{
	if (wParam == 0 && lParam == 0) //request for parameters
	{
		return 256 * 1024;
	}

	VisViewerPipe* ex = new VisViewerPipe(_viewerview);
	bool res = ex->Create(wParam, lParam);
	if (res) 
	{
		_visexchange = ex;
		ReplyMessage(TRUE); //Reply to viewer - connection accepted
		PSPosMessage msg;
		int waitMsg = COM_VERSION; //wait for message protocol number
		VisViewerExchange::Status wt = ex->WaitMessage(Array<int>(waitMsg), msg, 10000);
		if (wt != ex->statOk) 
		{
			_visexchange = 0;
			return FALSE;
		}
		ex->DispatchMessage(*msg);
		CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
		if (pEnvir->m_pRealDoc != NULL)
		{
			((CPosEdMainDoc *)pEnvir->m_pRealDoc)->OnBuldConnect(_visexchange);
		}
		SetTimer(500, 500, 0);
		return TRUE;
	} 
	else 
	{
		delete ex; 
		return FALSE;
	}  
}

void CMainFrame::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == 500 && _visexchange)
	{
		IVisViewerExchange::Status status = _visexchange->CheckInterfaceStatus();
		if (status != IVisViewerExchange::statOk && status != IVisViewerExchange::statNoMessage)
		{
			CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
			if (pEnvir->m_pRealDoc != NULL)
			{
				((CPosEdMainDoc *)pEnvir->m_pRealDoc)->OnBuldDisconnect();
			}
			pEnvir->m_pRealDoc = 0;
			_visexchange = 0;      
			KillTimer(500);
		}
	}
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.Create(this) || !m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndViewBar.Create(this,WS_CHILD|WS_VISIBLE|CBRS_TOP ,IDW_VIEW_BAR) ||

#ifdef __ONLY_FOR_PUBLIC__
		// *******************
		// PUBLIC EDITION ONLY
		// *******************
		!m_wndViewBar.LoadToolBar(IDR_VIEW_STYLE_PUBLIC))
#else
	#ifdef __ONLY_FOR_VBS__
		// ****************
		// VBS EDITION ONLY
		// ****************
		!m_wndViewBar.LoadToolBar(IDR_VIEW_STYLE_REDUCED))
	#else
			// ********************
			// new internal version
			// ********************
			!m_wndViewBar.LoadToolBar(IDR_VIEW_STYLE_REDUCED))
			// ********************
			// old internal version
			// ********************
	//		!m_wndViewBar.LoadToolBar(IDR_VIEW_STYLE))
	#endif
#endif

	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndSccIntegBar.Create(this, WS_CHILD | WS_VISIBLE | CBRS_TOP, IDW_SCC_INTEG_BAR) ||
		!m_wndSccIntegBar.LoadToolBar(IDR_SCC_INTEG))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!_scriptOutputDlg.Create(this, CScriptOutputDlg::IDD, CBRS_ALIGN_ANY, CScriptOutputDlg::IDD))
	{
		TRACE0("Failed to create script window\n");
		return -1;      // fail to create
	}
  
	if (!_namedSelectionBar.Create(this, DlgNamedSelectionBar::IDD, CBRS_ALIGN_ANY, ID_VIEW_NAMEDSELECTIONSPANEL))
	{
		TRACE0("Failed to create script window\n");
		return -1;      // fail to create
	}

	if (!m_wndObjPanel.CreatePanel(this, IDW_EDIT_PANEL))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Remove this if you don't want tool tips or a resizeable toolbar
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndViewBar.SetBarStyle(m_wndViewBar.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndSccIntegBar.SetBarStyle(m_wndViewBar.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndObjPanel.SetBarStyle(m_wndObjPanel.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	_scriptOutputDlg.SetBarStyle(m_wndObjPanel.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	_namedSelectionBar.SetBarStyle(m_wndObjPanel.GetBarStyle() | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndViewBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndSccIntegBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndObjPanel.EnableDocking(CBRS_ALIGN_LEFT|CBRS_ALIGN_RIGHT);
	_scriptOutputDlg.EnableDocking(CBRS_ALIGN_ANY);
	_namedSelectionBar.EnableDocking(CBRS_ALIGN_ANY);

	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);
	DockControlBar(&m_wndViewBar);
	DockControlBar(&m_wndSccIntegBar);
	DockControlBar(&m_wndObjPanel);
	DockControlBar(&_scriptOutputDlg);
	DockControlBar(&_namedSelectionBar);

  // Script output later will ne also DockControlBar
  //_scriptOutputDlg.Create(CScriptOutputDlg::IDD, this);
  //_scriptOutputDlg.ShowWindow(SW_SHOW);

	// show start bitmap
#ifndef _DEBUG
//	CRect rUser(281, 154, 490, 190);
//	ShowStartWnd(this, IDB_START, &rUser);
#endif


	LPBYTE menudata;
	UINT menusize;

	theApp.GetProfileBinary("ScriptMenu","MenuData", &menudata, &menusize);
	_scriptMenuCmds.Load(reinterpret_cast<const char *>(menudata), menusize);
	delete menudata;

	return 0;
}

void CMainFrame::OnClose() 
{
	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	MASSERT(pEnvir != NULL);
	// poloha BlockBars
	GetDockState(pEnvir->m_appDockState);
	pEnvir->m_appDockState.m_objectPanelSize = m_wndObjPanel.m_sizeDocked;
	pEnvir->m_appDockState.m_scriptPanelSize = _scriptOutputDlg.GetSizeDocked();
	pEnvir->m_appDockState.m_namSelPanelSize= _namedSelectionBar.GetSizeDocked();
	pEnvir->m_appDockState.SaveParams();

	if (IsConnected())
	{
		Disconnect(true);
	}
  
	CMDIFrameWnd::OnClose();
}

void CMainFrame::OnDestroy() 
{
	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	MASSERT(pEnvir != NULL);
	// posledn� poloha okna 
	pEnvir->m_optPosEd.m_bLastMaximize = (GetStyle()&WS_MAXIMIZE) != 0;
	RECT r;
	if (pEnvir->m_optPosEd.m_bLastMaximize)
	{
		WINDOWPLACEMENT wpl;
		wpl.length = sizeof(WINDOWPLACEMENT);
		wpl.flags  = 0;
		GetWindowPlacement(&wpl);
		r = wpl.rcNormalPosition;
	} 
	else
	{
		GetWindowRect(&r);
	}
	pEnvir->m_optPosEd.m_nLastXPos	= r.left;
	pEnvir->m_optPosEd.m_nLastYPos	= r.top;
	pEnvir->m_optPosEd.m_nLastCXPos = r.right - r.left;
	pEnvir->m_optPosEd.m_nLastCYPos = r.bottom- r.top;

	// standardn� Destroy
	CMDIFrameWnd::OnDestroy();
}


/////////////////////////////////////////////////////////////////////////////
// komunikace s jinou aplikac�

void CMainFrame::HandleError(bool send)
{
	int error = WSAGetLastError();

	Disconnect(false);

	CString text;
	if (send)   
	{
		text.Format(_T("Sockets: Connection lost (cannot finish send) error %d"), error);  
		AfxMessageBox(text, MB_OK);
	}
	else 
	{
		text.Format(_T("Sockets: Connection lost (cannot finish recv) error %d"), error);  
		AfxMessageBox(text, MB_OK); 
	}
}

void CMainFrame::Disconnect(bool quit)
{
	// Change document status
	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	if (pEnvir->m_pRealDoc != NULL)
	{
		((CPosEdMainDoc *)pEnvir->m_pRealDoc)->OnBuldDisconnect();
	}

	pEnvir->m_pRealDoc = NULL;
	if (quit) _visexchange->SystemQuit();
	_visexchange = 0;
}

bool CMainFrame::IsConnected()
{
	return _visexchange != 0;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnViewViewbar() 
{	
	CMDIFrameWnd::OnBarCheck(IDW_VIEW_BAR); 
}

void CMainFrame::OnUpdateViewViewbar(CCmdUI* pCmdUI) 
{	
	pCmdUI->Enable(TRUE);
	pCmdUI->m_nID = IDW_VIEW_BAR;
	OnUpdateControlBarMenu(pCmdUI); 
}

void CMainFrame::OnViewEditpanel() 
{	CMDIFrameWnd::OnBarCheck(IDW_EDIT_PANEL); }
void CMainFrame::OnUpdateViewEditpanel(CCmdUI* pCmdUI) 
{	pCmdUI->Enable(TRUE);
	pCmdUI->m_nID = IDW_EDIT_PANEL;
	OnUpdateControlBarMenu(pCmdUI); }

/////////////////////////////////////////////////////////////////////////////
// STATUS bar

void CMainFrame::OnUpdateStatusInfoBuldozer(CCmdUI* pCmdUI)
{
  pCmdUI->Enable(TRUE);
  if (IsConnected())
    pCmdUI->SetText(_T("Connected"));
  else
    pCmdUI->SetText(_T("Disconnected"));
}
void CMainFrame::OnUpdateStatusInfoObject(CCmdUI* pCmdUI) 
{	pCmdUI->Enable(TRUE);
	pCmdUI->SetText(_T(" "));
}
void CMainFrame::OnUpdateStatusInfoCurpos(CCmdUI* pCmdUI) 
{	OnUpdateStatusInfoObject(pCmdUI); }
void CMainFrame::OnUpdateStatusInfoPrimtxtr(CCmdUI* pCmdUI) 
{	OnUpdateStatusInfoObject(pCmdUI); }
void CMainFrame::OnUpdateStatusInfoScndtxtr(CCmdUI* pCmdUI) 
{	OnUpdateStatusInfoObject(pCmdUI); }

/////////////////////////////////////////////////////////////////////////////
// typy objekt� v panelu

void CMainFrame::OnToolsPanelType0() 
{	
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypePlOb); 
}

void CMainFrame::OnToolsPanelType1() 
{	
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeNtOb); 
}

void CMainFrame::OnToolsPanelType2() 
{	
#ifdef __ONLY_FOR_PUBLIC__
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeNets); 
#else
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeNROb); 
#endif
}

void CMainFrame::OnToolsPanelType3() 
{	
#ifdef __ONLY_FOR_PUBLIC__
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeKPla); 
#else
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeNets); 
#endif
}

void CMainFrame::OnToolsPanelType4() 
{	
#ifdef __ONLY_FOR_PUBLIC__
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeLand); 
#else
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeKPla); 
#endif
}

void CMainFrame::OnToolsPanelType5() 
{	
#ifdef __ONLY_FOR_PUBLIC__
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeBgImage); 
#else
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeLand); 
#endif
}

void CMainFrame::OnToolsPanelType6() 
{	
#ifdef __ONLY_FOR_PUBLIC__
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeName); 
#else
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeBgImage); 
#endif
}

void CMainFrame::OnToolsPanelType7() 
{	
#ifdef __ONLY_FOR_PUBLIC__
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeNObj); 
#else
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeName); 
#endif
}

void CMainFrame::OnToolsPanelType8() 
{	
#ifdef __ONLY_FOR_PUBLIC__
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeTxtr); 
#else
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeNObj); 
#endif
}

void CMainFrame::OnToolsPanelType9() 
{	
#ifdef __ONLY_FOR_PUBLIC__
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeWood); 
#else
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeTxtr); 
#endif
}

void CMainFrame::OnToolsPanelType10() 
{	
#ifdef __ONLY_FOR_PUBLIC__
#else
	m_wndObjPanel.SetTypeObjects(CPosObjectsPanel::grTypeWood); 
#endif
}


/////////////////////////////////////////////////////////////////////////////
// menu VIEW

void CMainFrame::OnSelchangeObjectType() 
{
	m_wndObjPanel.OnChangeTypeObjects();
}

void CMainFrame::OnViewConfig() 
{
	if (DoEditCurrentConfig())
	{	// informuji dokumenty o zm�n� konfigurace
		CUpdatePar cUpdate(CUpdatePar::uvfChngConfig);
		UpdateAllDocs(CUpdatePar::uvfChngConfig,&cUpdate);
		// p�ekreslen� panelu
		m_wndObjPanel.Invalidate();
	}
}

void CMainFrame::OnViewBldCursor() 
{
	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	if (pEnvir)
	{	// zm�na viditelnosti kurzoru
		pEnvir->m_cfgCurrent.m_bCursorVisible = !pEnvir->m_cfgCurrent.m_bCursorVisible;
		// update oken
		CUpdatePar cUpdate(CUpdatePar::uvfVisiCursor);
		UpdateAllDocs(CUpdatePar::uvfVisiCursor,&cUpdate);
	}
}

void CMainFrame::OnUpdateViewBldCursor(CCmdUI* pCmdUI) 
{
	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	if (pEnvir)
	{
		pCmdUI->Enable(TRUE);
		pCmdUI->SetCheck(pEnvir->m_cfgCurrent.m_bCursorVisible);
	}
}

void CMainFrame::OnUpdateProjectMakeReal(CCmdUI *pCmdUI)
{
  CFrameWnd * pcFrame = GetActiveFrame();
  CPosEdMainDoc * doc = (CPosEdMainDoc *) pcFrame->GetActiveDocument();
  
  if (doc == NULL)
  {
    pCmdUI->Enable(TRUE);      
    pCmdUI->SetCheck(FALSE);
    return;
  } 

  if (doc->IsRealDocument())
  {
    pCmdUI->Enable(TRUE);      
    pCmdUI->SetCheck(TRUE);
  } 
  else
  {
    //CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
    pCmdUI->Enable(/*pEnvir->m_pRealDoc == NULL*/TRUE);
    pCmdUI->SetCheck(FALSE);
  };  
}

void CMainFrame::MakeRealDoc(CPosEdMainDoc* doc)
{
	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	if (pEnvir->m_pRealDoc)
	{
		((CPosEdMainDoc*)(pEnvir->m_pRealDoc))->OnBuldDisconnect();
	}
	pEnvir->m_pRealDoc = doc;
	if (pEnvir->m_pRealDoc)
	{
		((CPosEdMainDoc*)(pEnvir->m_pRealDoc))->OnBuldConnect(_visexchange);
	}
}

void CMainFrame::OnProjectMakeReal()
{
	CFrameWnd* pcFrame = GetActiveFrame();
	CPosEdMainDoc* doc = (CPosEdMainDoc*) pcFrame->GetActiveDocument();

	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	if (doc == pEnvir->m_pRealDoc)
	{
		MakeRealDoc(0);
	}
	else
	{
		MakeRealDoc(doc);
	}

	if (!IsConnected()) OnLaunchBuldozer();
}

void CMainFrame::OnLaunchBuldozer()
{
  if (IsConnected())
    return;

  CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
  if (pEnvir->m_optSystem.m_sPreviewAppCommand.GetLength()!=0)
  {
    TCHAR dir[512];
    GetCurrentDirectory(512, dir);

    MakeLongDirStr(pEnvir->m_optSystem.m_sPreviewAppPath,pEnvir->m_optSystem.m_sPreviewAppPath);
    if (pEnvir->m_optSystem.m_sPreviewAppPath.GetLength()!=0)
    {      
      if (!SetCurrentDirectory(pEnvir->m_optSystem.m_sPreviewAppPath))
      {
        AfxMessageBox(IDS_WORK_DIR_ERROR, MB_OK);
        Disconnect(false);
        return;
      }     
    }    

    RString cmdline=pEnvir->m_optSystem.m_sPreviewAppCommand;
    const char *connectStr=strstr(cmdline,"-connect=");
    if (connectStr!=0)
    {
      connectStr+=9;
      if (_strnicmp(connectStr,"pipe\\",5)==0)
      {
        connectStr+=5;
        char name[50];
        sscanf(connectStr,"%49s",name);        
        if (m_connectWnd.GetSafeHwnd()) m_connectWnd.DestroyWindow();
        m_connectWnd.Create("STATIC",name,WS_CHILD,CRect(0,0,0,0),this,WM_INCOME_PIPECONNECTION,0);
      }
      else
      {
        AfxMessageBox(IDS_BULDOZER_ERROR_NOSOCKETS);
        return;
      }
    }
    else
    {    
      RString connect;
      sprintf(connect.CreateBuffer(100)," -connect=pipe\\%X,%X",m_hWnd,WM_INCOME_PIPECONNECTION);
      cmdline=cmdline+connect;
    }
    // if command line contains options only, do not execute, but keep the pipe open
    if ( cmdline[0]!='-' && 32 > WinExec(cmdline,SW_SHOW))
    {
      AfxMessageBox(IDS_BULDOZER_ERROR, MB_OK);
      Disconnect(false);      
    }
    SetCurrentDirectory(dir);
  }
}

void CMainFrame::OnUpdateLaunchBuldozer(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(!IsConnected());
}

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
  // TODO: Add your specialized code here and/or call the base class

  return CMDIFrameWnd::OnCreateClient(lpcs, pContext);
}

void CMainFrame::OnViewRuler()
{
  BOOL& showRuler = GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowRuler;
  showRuler = !showRuler;
  GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.SaveParams();

  CChildFrame *pFrame = (CChildFrame *) GetActiveFrame();
  if (pFrame)
    pFrame->ShowRulers(showRuler);
}

void CMainFrame::OnUpdateViewRuler(CCmdUI *pCmdUI)
{
  CFrameWnd *pFrame = GetActiveFrame();  
  BOOL bEnable = pFrame != NULL && pFrame->GetActiveDocument() != NULL;
  pCmdUI->Enable(bEnable);
  pCmdUI->SetCheck(bEnable && GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowRuler);
}

//void CMainFrame::OnLButtonDown(UINT nFlags, CPoint point)
//{
//  // TODO: Add your message handler code here and/or call default
//
//  CMDIFrameWnd::OnLButtonDown(nFlags, point);
//}

void CMainFrame::OnRenameDoc()
{
  CFrameWnd * pFrame = GetActiveFrame();
  if (pFrame)
  {
    CPoseidonDoc * pDoc = (CPoseidonDoc *) pFrame->GetActiveDocument();
    pDoc->OnRenameDoc();
  }
}

////////////////////////////////////////////////////
// SCC integration

/*
class CSccIntegBindingDlg : public CDialog
{
  // Construction
public:
  CSccIntegBindingDlg(CWnd* pParent = NULL);   // standard constructor

  enum { IDD = IDD_SCC_INTEG_BINDING};  

  CString m_sccServerName;
  CString m_sccProjectName;
  CString m_sccLocalPath;

  MsSccFunctions * m_sccFunctions;

protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

  // Implementation
protected:

  DECLARE_MESSAGE_MAP()
  virtual void OnOK();
};

BEGIN_MESSAGE_MAP(CSccIntegBindingDlg, CDialog)
END_MESSAGE_MAP()

CSccIntegBindingDlg::CSccIntegBindingDlg(CWnd* pParent) : 
CDialog( CSccIntegBindingDlg::IDD, pParent)
{};

void CSccIntegBindingDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
 
  MDDX_Text(pDX, IDC_EDIT_SCC_INTEG_SERVER, m_sccServerName);
  MDDX_Text(pDX, IDC_EDIT_SCC_INTEG_PROJECT, m_sccProjectName);
  MDDX_Text(pDX, IDC_EDIT_SCC_INTEG_LOCAL_DIR, m_sccLocalPath);
}

void CSccIntegBindingDlg::OnOK()
{
  UpdateData();
  //Try binding, if it is OK.    

  SCCRTN ret = m_sccFunctions->Init(m_sccServerName, m_sccProjectName, m_sccLocalPath);
  //m_sccFunctions->Init();

  if (IS_SCC_SUCCESS(ret) || IS_SCC_WARNING(ret))
    return CDialog::OnOK();

  CString text;
  text.Format(IDS_SCC_INTEG_BINDING_ERR, ret);
  AfxMessageBox(text);  
}*/


void CMainFrame::InitSCC()
{
  CSystemOptions& sysOpt = GetPosEdEnvironment()->m_optSystem;
  /*SCCRTN ret =*/ _sccFunctions.Init(sysOpt.m_sccServerName, sysOpt.m_sccProjectName, sysOpt.m_sccLocalPath, m_hWnd);
  //SCCRTN ret = _sccFunctions.Init("\\\\new_server\\vssdatap\\dataP", "$/", "p:", m_hWnd);
  //ret = _sccFunctions.CheckOut("P:\\ofp2\\Maps\\Europe\\Bohemia\\Source\\bohemia.pew");
}

void CMainFrame::OnSccIntegBinding()
{  
  SCCRTN ret = _sccFunctions.ChooseProject();

  if (IS_SCC_SUCCESS(ret) || IS_SCC_WARNING(ret)) 
  {
    if (IS_SCC_WARNING(ret))
    {
      LogF("%s %d : MsSccFunctions warning %d", __FILE__, __LINE__,ret);
    }

    CSystemOptions& sysOpt = GetPosEdEnvironment()->m_optSystem;
    sysOpt.m_sccServerName = _sccFunctions.GetServerName();
    sysOpt.m_sccProjectName = _sccFunctions.GetProjName();
    sysOpt.m_sccLocalPath = _sccFunctions.GetLocalPath();

    sysOpt.SaveParams();  

    OnSccIntegRefresh();
    
    return; 
  }
 
  CString text;
  text.Format(IDS_OPER_FAILED2, ret);
  AfxMessageBox(text);
  return;

 
}

void CMainFrame::OnSccIntegBindingUpdate(CCmdUI *pCmdUI)
{
  pCmdUI->Enable();
}

void CMainFrame::OnSccIntegRefresh()
{
  CFrameWnd * pFrame = GetActiveFrame();
  if (!pFrame)
    return;

  CPoseidonDoc * pDoc = (CPoseidonDoc *) pFrame->GetActiveDocument();
  if (pDoc)
    pDoc->UpdateSSStatus();
}

void CMainFrame::OnSccIntegRefreshUpdate(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(_sccFunctions.Opened());
}

void CMainFrame::OnViewSsIntegToolbar()
{
  CMDIFrameWnd::OnBarCheck(IDW_SCC_INTEG_BAR);  
}

void CMainFrame::OnUpdateViewSsIntegToolbar(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(TRUE);
  pCmdUI->m_nID = IDW_SCC_INTEG_BAR;
  OnUpdateControlBarMenu(pCmdUI);
}

void CMainFrame::OnScript(CString& scriptPath)
{
  CFrameWnd * pFrame = GetActiveFrame();
  if (!pFrame)
    return;

  CPoseidonDoc * pDoc = (CPoseidonDoc *) pFrame->GetActiveDocument(); 

  if (_visexchange) _visexchange->StartBuffer(1024*1024);

  ProcessScript script;
  script(scriptPath,*pDoc, &_scriptOutputDlg);  

  if (_visexchange) _visexchange->EndBuffer();

  pDoc->UpdateAllViews(NULL);
  m_wndObjPanel.UpdateDocData(pDoc,TRUE);
}

void CMainFrame::OnScriptPanel()
{
  	CMDIFrameWnd::OnBarCheck(CScriptOutputDlg::IDD);
}

void CMainFrame::OnUpdateScriptPanel(CCmdUI *pCmdUI)
{
 pCmdUI->Enable(TRUE);
 pCmdUI->m_nID = CScriptOutputDlg::IDD;
 OnUpdateControlBarMenu(pCmdUI);
}

void CMainFrame::OnUpdateScriptRun(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(TRUE);
}

void CMainFrame::OnUpdateScriptBrowse(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(TRUE);
}
void CMainFrame::OnScriptsManagescripts()
{
  DlgManageScripts dlg(_scriptMenuCmds);
  dlg.SetScc(&_sccFunctions);
  if (dlg.DoModal()==IDOK)
  {
    SaveScriptMenu();
  }

}

void CMainFrame::SaveScriptMenu()
{
  int sz;
  char *p=_scriptMenuCmds.Save(sz);
  theApp.WriteProfileBinary("ScriptMenu","MenuData",reinterpret_cast<LPBYTE>(p),sz);
  delete [] p;
}
void CMainFrame::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu)
{
  if (pPopupMenu->GetMenuState(ID_SCRIPTS_MANAGESCRIPTS,MF_BYCOMMAND)!=0xFFFFFFFF)
  {
      while (pPopupMenu->GetMenuItemID(2)!=ID_SCRIPTS_MANAGESCRIPTS) 
      {
        pPopupMenu->DeleteMenu(0,MF_BYPOSITION);
      }
      _scriptMenuCmds.InsertToMenu(*pPopupMenu,0,ID_SCRIPT_MENU_BEGIN);
  }
  else
    CMDIFrameWnd::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);
}


void CMainFrame::OnRunScriptFromMenu(UINT cmd)
{ 
  int uid=cmd-ID_SCRIPT_MENU_BEGIN ;
  int idx=_scriptMenuCmds.FindId(uid);
  if (idx!=-1)
  {
    const ScriptMenuItem &itm=_scriptMenuCmds[idx];
    CString path=itm.pathname;
    OnScript(path);
  }
}
void CMainFrame::OnScriptsAddrunscript()
{
  Pathname scr;
  if (_scriptMenuCmds.Size())
  {
    scr=_scriptMenuCmds[0].pathname;
    scr.SetFilename("");
  }
  if (DlgManageScripts::SelectNewScript(scr,0))
  {     
     ScriptMenuItem itm;
     itm.flags=0;
     itm.name=scr.GetTitle();
     itm.pathname=scr;
     itm.uid=DlgManageScripts(_scriptMenuCmds).GuessIDExt();          
     _scriptMenuCmds.Insert(0)=itm;
     CString path=scr;
     OnScript(path);
  }
}

LRESULT CMainFrame::OnScriptUnlockRequest(WPARAM wParam, LPARAM lParam)
{
  //HWND hDlg=(HWND)wParam; //dialog, that requests unlock parent
  //GameState *gs=(GameState *)lParam; //pointer to GameState

  // TODO: Make some action, before this request is allowed.
  return TRUE; //Allow this operation
}
