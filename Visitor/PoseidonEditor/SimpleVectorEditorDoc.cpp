#include "StdAfx.h"
#include <afxmt.h>
#include <el/Math/math3d.hpp>
#include "VisNamedSelection.h"
#include ".\simplevectoreditordoc.h"

bool SimpleVectorEditorDoc::Add(const SimpleVectorEditorModel::Point &pt, int &index)
{
  index=_model.AddPoint(pt);
  VisSelection selection;
  selection.Include(NamSelItem::objPoint,index);
  _undo.Delete(selection);
  for(Each x(this);x;x->Add(pt,index));
  return true;
}

bool SimpleVectorEditorDoc::Add(const SimpleVectorEditorModel::Line &pt, int &index)
{
  index=_model.AddLine(pt);
  VisSelection selection;
  selection.Include(NamSelItem::objLine,index);
  _undo.Delete(selection);
  for(Each x(this);x;x->Add(pt,index));
  return true;
}

bool SimpleVectorEditorDoc::Add(const Array<const SimpleVectorEditorModel::Point> &pt, int &index)
{
  index=_model.AddPoints(pt);
  VisSelection selection;
  selection.Include(NamSelItem::objPoint,index,_model.GetPointCount()-1);
  _undo.Delete(selection);
  for(Each x(this);x;x->Add(pt,index));    
  return true;
}

bool SimpleVectorEditorDoc::Add(const Array<const SimpleVectorEditorModel::Line> &pt, int &index)
{
  index=_model.AddLines(pt);
  VisSelection selection;
  selection.Include(NamSelItem::objLine,index,_model.GetLineCount()-1);
  _undo.Delete(selection);
  for(Each x(this);x;x->Add(pt,index));
  return true;
}

bool SimpleVectorEditorDoc::Set(int index, const SimpleVectorEditorModel::Point &pt)
{
  _undo.Set(index,_model.Points()[index]);
  _model.SetPointPos(index,pt);
  for(Each x(this);x;x->Set(index,pt));
  return true;
}

bool SimpleVectorEditorDoc::Set(int index, const SimpleVectorEditorModel::Line &pt)
{
  _undo.Set(index,_model.Points()[index]);
  _model.SetLine(index,pt);
  for(Each x(this);x;x->Set(index,pt));
  return true;
}

bool SimpleVectorEditorDoc::Delete(const VisSelection &sel)
{
  VisSelection willDel;
  willDel.Include(sel);
  for (unsigned int i=0;i<_model.GetLineCount();i++) 
  {
    const SimpleVectorEditorModel::Line &ln=_model.Lines()[i];
    if (sel.IsSelected(NamSelItem::objPoint,ln.Begin())||sel.IsSelected(NamSelItem::objPoint,ln.End()))
      willDel.Include(NamSelItem::objLine,i);
  }

  AutoArray<SimpleVectorEditorModel::Point> delPoints;
  AutoArray<SimpleVectorEditorModel::Line> delLines;
  AutoArray<int> ptIndex;
  AutoArray<int> lnIndex;

  for (int i=0;i<_model.Points().Size();i++) if (willDel.IsSelected(NamSelItem::objPoint,i))
  {
    ptIndex.Add(i);
    delPoints.Add(_model.Points()[i]);
  } 
  for (int i=0;i<_model.Lines().Size();i++) if (willDel.IsSelected(NamSelItem::objLine,i))
  {
    lnIndex.Add(i);
    delLines.Add(_model.Lines()[i]);
  }
  _undo.OpenGroup();
  if (delPoints.Size()) _undo.Add(ptIndex,delPoints);
  if (delLines.Size()) _undo.Add(lnIndex,delLines);
  _undo.CloseGroup();
  for(Each x(this);x;x->Delete(sel));
  _model.Delete(sel);
  return true;
}

namespace Functors
{
  class UndoPropertyFunctor
  {
    mutable VisSelection curSel;
    mutable SimpleVectorEditorUndo &undo;
    const char *propName;
    bool willNewSelCreated;
  public:
    UndoPropertyFunctor(const VisSelection &curSel, SimpleVectorEditorUndo &undo, const char *propName, bool willNewSelCreated):
        propName(propName),undo(undo),curSel(curSel),willNewSelCreated(willNewSelCreated) {undo.OpenGroup();}
    int operator()(const SimpleVectorEditorModel::PropertySelection *sel) const
    {
      VisSelection wrk=*sel;
      wrk.Crop(curSel);
      if (!wrk.IsEmpty())
      {
        undo.SetProperty(propName,sel->GetValue(),wrk);
        curSel.Exclude(wrk);
      }
      return 0;
    }
    ~UndoPropertyFunctor()
    {
      if (!curSel.IsEmpty() && willNewSelCreated)
        undo.UnsetProperty(propName,curSel);
      undo.CloseGroup();
    }
  };
}

bool SimpleVectorEditorDoc::SetProperty(const char *name, const char *value, const VisSelection &sel)
{
  _model.ForEachProperty(Functors::UndoPropertyFunctor(sel,_undo,name,true),&SimpleVectorEditorModel::PropertySelection(name));
  _model.SetProperty(name,value,sel);
  for(Each x(this);x;x->SetProperty(name,value,sel));
  return true;
}

bool SimpleVectorEditorDoc::UnsetProperty(const char *name, const VisSelection &selection)
{
  _model.ForEachProperty(Functors::UndoPropertyFunctor(selection,_undo,name,false),&SimpleVectorEditorModel::PropertySelection(name));
  _model.UnsetProperty(name,selection);
  for(Each x(this);x;x->UnsetProperty(name,selection));
  return true;
}

bool SimpleVectorEditorDoc::Load(const SimpleVectorEditorModel &what)
{
  _undo.Load(_model);
  _model=what;
  for(Each x(this);x;x->Load(what));
  return true;
}

bool SimpleVectorEditorDoc::Add(const Array<int> &indexes, const Array<const SimpleVectorEditorModel::Point> &pt)
{
  VisSelection sel;
  for (int i=0;i<pt.Size();i++) sel.Include(NamSelItem::objPoint,indexes[i]);
  _undo.Delete(sel);
  for (int i=0;i<pt.Size();i++) _model.InsertPoint(pt[i],indexes[i]);
  for(Each x(this);x;x->Add(indexes,pt));
  return true;
}

bool SimpleVectorEditorDoc::Add(const Array<int> &indexes, const Array<const SimpleVectorEditorModel::Line> &pt)
{
  VisSelection sel;
  for (int i=0;i<pt.Size();i++) sel.Include(NamSelItem::objLine,indexes[i]);
  _undo.Delete(sel);
  for (int i=0;i<pt.Size();i++) _model.InsertLine(pt[i],indexes[i]);
  for(Each x(this);x;x->Add(indexes,pt));
  return true;
}

bool SimpleVectorEditorDoc::SpecialSelOp(const char *name,SelOp selOp,const VisSelection *rsel)
{
  SimpleVectorEditorModel::PropertySelection *sel=_model.FindSelection(_model.SSpecialSelName,name);
  if (sel==0)
  {
    if (selOp==selopClear || selOp==selopExclude) return true;
    _undo.SpecialSelOp(name,selopClear,0);
    _model.AddSelectionInstance(new SimpleVectorEditorModel::PropertySelection(_model.SSpecialSelName,name,*rsel));
    return true;    
  }

  _undo.SpecialSelOp(name,selopUse,sel);
  switch(selOp)
  {
  case selopClear: sel->Clear();break;
  case selopInclude: sel->Include(*rsel);break;
  case selopExclude: sel->Exclude(*rsel);break;
  case selopUse: sel->Clear();sel->Include(*rsel);break;
  }

  return true;

}


bool SimpleVectorEditorUndo::Add(const SimpleVectorEditorModel::Point &pt, int &index)
{
  PushAction(new StdAction2<
      bool (ISimpleVectorEditorActions::*)(const SimpleVectorEditorModel::Point &pt, int &index),
      &IDoc::Add,
      SimpleVectorEditorModel::Point,
      int>(pt,index));
  return true;
}

bool SimpleVectorEditorUndo::Add(const SimpleVectorEditorModel::Line &pt, int &index)
{
  PushAction(new StdAction2<
    bool (ISimpleVectorEditorActions::*)(const SimpleVectorEditorModel::Line &pt, int &index),
    &IDoc::Add,
    SimpleVectorEditorModel::Line,
    int>(pt,index));
  return true;
}

bool SimpleVectorEditorUndo::Add(const Array<const SimpleVectorEditorModel::Point> &pt, int &index)
{
  PushAction(new StdAction2<
    bool (ISimpleVectorEditorActions::*)(const Array<const SimpleVectorEditorModel::Point> &pt, int &index),
    &IDoc::Add,
    Array<SimpleVectorEditorModel::Point>,
    int>(Array<SimpleVectorEditorModel::Point>(pt),index));
  return true;
}

bool SimpleVectorEditorUndo::Add(const Array<const SimpleVectorEditorModel::Line> &pt, int &index)
{
  PushAction(new StdAction2<
    bool (ISimpleVectorEditorActions::*)(const Array<const SimpleVectorEditorModel::Line> &pt, int &index),
    &IDoc::Add,
    Array<SimpleVectorEditorModel::Line>,
    int>(Array<SimpleVectorEditorModel::Line>(pt),index));
  return true;
}

bool SimpleVectorEditorUndo::Set(int index, const SimpleVectorEditorModel::Point &pt)
{
  PushAction(new StdAction2<
    bool (ISimpleVectorEditorActions::*)(int index, const SimpleVectorEditorModel::Point &pt),
    &IDoc::Set,
    int,SimpleVectorEditorModel::Point>(index,pt));
  return true;
}

bool SimpleVectorEditorUndo::Set(int index, const SimpleVectorEditorModel::Line &pt)
{
  PushAction(new StdAction2<
    bool (ISimpleVectorEditorActions::*)(int index, const SimpleVectorEditorModel::Line &pt),
    &IDoc::Set,
    int,SimpleVectorEditorModel::Line>(index,pt));
  return true;
}

bool SimpleVectorEditorUndo::Delete(const VisSelection &sel)
{
  PushAction(new StdAction1<
    bool (ISimpleVectorEditorActions::*)(const VisSelection &sel),
    &IDoc::Delete,
    VisSelection>(sel));
  return true;
}

bool SimpleVectorEditorUndo::SetProperty(const char *name, const char *value, const VisSelection &sel)
{
  PushAction(new StdAction3<
    bool (ISimpleVectorEditorActions::*)(const char *name, const char *value, const VisSelection &sel),
    &IDoc::SetProperty,
    RStringI,RString,VisSelection>(name,value,sel));
  return true;
}

bool SimpleVectorEditorUndo::UnsetProperty(const char *name, const VisSelection &selection)
{
  PushAction(new StdAction2<
    bool (ISimpleVectorEditorActions::*)(const char *name,const VisSelection &sel),
    &IDoc::UnsetProperty,
    RStringI,VisSelection>(name,selection));
  return true;
}

bool SimpleVectorEditorUndo::Load(const SimpleVectorEditorModel &what)
{
  PushAction(new StdAction1<
    bool (ISimpleVectorEditorActions::*)(const SimpleVectorEditorModel &sel),
    &IDoc::Load,
    SimpleVectorEditorModel>(what));
  return true;
}

bool SimpleVectorEditorUndo::Add(const Array<int> &indexes, const Array<const SimpleVectorEditorModel::Point> &pt)
{
  PushAction(new StdAction2<
    bool (ISimpleVectorEditorActions::*)(const Array<int> &indexes, const Array<const SimpleVectorEditorModel::Point> &pt),
    &IDoc::Add,
    Array<int>,Array<SimpleVectorEditorModel::Point> >
    (indexes,Array<SimpleVectorEditorModel::Point>(pt)));
  return true;
}

bool SimpleVectorEditorUndo::Add(const Array<int> &indexes, const Array<const SimpleVectorEditorModel::Line> &pt)
{
  PushAction(new StdAction2<
    bool (ISimpleVectorEditorActions::*)(const Array<int> &indexes, const Array<const SimpleVectorEditorModel::Line> &pt),
    &IDoc::Add,
    Array<int>,Array<SimpleVectorEditorModel::Line> >
    (indexes,Array<SimpleVectorEditorModel::Line>(pt)));
  return true;
}
bool SimpleVectorEditorUndo::SpecialSelOp(const char *name,SelOp selOp,const VisSelection *sel)
{
  PushAction(new StdAction3<
    bool (ISimpleVectorEditorActions::*)(const char *name, SelOp selOp, const VisSelection *sel),
    &IDoc::SpecialSelOp,
    RString,SelOp,SRef<VisSelection> >(name,selOp,sel?new VisSelection(*sel):0));
  return true;
}

static VisSelection emptySelection; 

const VisSelection &ISimpleVectorEditor::GetSelected() const
{
  const VisSelection *sel=GetModel().FindSelection(SimpleVectorEditorModel::SSpecialSelName,SimpleVectorEditorModel::SSpecialSelected);
  if (sel==0) sel=&emptySelection;
  return *sel;
}
const VisSelection &ISimpleVectorEditor::GetHidden() const
{
  const VisSelection *sel=GetModel().FindSelection(SimpleVectorEditorModel::SSpecialSelName,SimpleVectorEditorModel::SSpecialHidden);
  if (sel==0) sel=&emptySelection;
  return *sel;
}
const VisSelection &ISimpleVectorEditor::GetLocked() const
{
  const VisSelection *sel=GetModel().FindSelection(SimpleVectorEditorModel::SSpecialSelName,SimpleVectorEditorModel::SSpecialLocked);
  if (sel==0) sel=&emptySelection;
  return *sel;
}

namespace Functors
{
  class TransformPoints
  {
    Matrix4 _mx;
    mutable SimpleVectorEditorModel &_model;
  public:
    TransformPoints(const Matrix4 &mx,SimpleVectorEditorModel &model):_mx(mx),_model(model) {}
    int operator()(NamSelItem::ObjectType type,unsigned int first, unsigned int last) const
    {
      if (type==NamSelItem::objPoint)
      {
        for (unsigned int i=first;i<=last;i++)
        {
          Vector3 nw=_mx.FastTransform(_model.Points()[i]);
          _model.SetPointPos(i,SimpleVectorEditorModel::Point(nw,_model.Points()[i].GetPointType()));
        }
      }
      return 0;
    }
  };
}

bool SimpleVectorEditorDoc::Transform(const Matrix4 &mx, const VisSelection *what)
{
  VisSelection tmp;
  if (!what) 
  {
    tmp.Include(GetSelected());
    tmp.Exclude(GetLocked());
    what=&tmp;
  }

  if (!_undo.BeginBatch()) return false;
  NamSelItem::ObjectType type(NamSelItem::objPoint);
  what->ForEachSelected(Functors::TransformPoints(mx,_model),&type);
  _undo.EndBatch();
  return true;

}
