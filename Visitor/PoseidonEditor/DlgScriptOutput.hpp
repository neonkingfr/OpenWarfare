
#pragma once
#include "resource.h"
class CMainFrame;

////////////////////////////////////////////////////////////////
//

class CScriptOutputDlg : public CDialogBar
{
protected:
  CString _text;
  CString _scriptPath;
  CMainFrame * _mainFrame;

  // Sizes for resizing dialogbar
  CSize m_sizeDocked;
  CSize m_sizeFloating;

  // Construction
public:
  CScriptOutputDlg() : _mainFrame(NULL), CDialogBar() {};   // standard constructor

  //{{AFX_DATA(CScriptOutputDlg)
  enum { IDD = IDD_SCRIPT_OUTPUT };
  //}}AFX_DATA

  void SetMainFrame(CMainFrame * mainFrame) {_mainFrame = mainFrame;};

  void AddText(const CString& val);

  virtual CSize CalcDynamicLayout(int nLength, DWORD dwMode);
  virtual CSize CalcDynamicDim(int nLength, DWORD dwMode);
  void RecalcChildWindowPos(const CSize& size);

  const CSize& GetSizeDocked() const {return m_sizeDocked;};

  // Overrides
  // ClassWizard generated virtual function overrides
  //{{AFX_VIRTUAL(CScriptOutputDlg)
protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  //}}AFX_VIRTUAL

  // Implementation
protected:
  // Generated message map functions
  //{{AFX_MSG(CScriptOutputDlg)
  // NOTE: the ClassWizard will add member functions here
  //}}AFX_MSG
  DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnBnClickedScriptBrowse();
  afx_msg void OnBnClickedScriptRun();
  virtual BOOL Create(CWnd* pParentWnd, UINT nIDTemplate, UINT nStyle, UINT nID);
};