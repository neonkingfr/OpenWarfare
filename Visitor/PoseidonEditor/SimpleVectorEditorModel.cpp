#include <el/Math/math3d.hpp>
#include ".\simplevectoreditormodel.h"
#include <set>

using namespace std;

const char *SimpleVectorEditorModel::SSpecialSelName="_spec";
const char *SimpleVectorEditorModel::SSpecialSelected="Selected";
const char *SimpleVectorEditorModel::SSpecialHidden="Hidden";
const char *SimpleVectorEditorModel::SSpecialLocked="Locked";

int SimpleVectorEditorModel::AddPoints(const Array<const Point> &pt)
{
  int c=_points.Size();
  _points.Append(pt);
  return c;
}

int SimpleVectorEditorModel::AddLines(const Array<const Line> &lines)
{
  Array<const Point> k=_points;
  int c=_lines.Size();
  _lines.Append(lines);
  return c;
}


namespace Functors
{
  class OptimizeSelections
  {
    const Array<int> &_poffs;
    const Array<int> &_loffs;
  public:
    OptimizeSelections(const Array<int> &poffs,const Array<int> &loffs):_loffs(loffs),_poffs(poffs) {}

    bool operator()(const VisNamedSelection *sel) const
    {
      const_cast<VisNamedSelection *>(sel)->RemapIndexes(_poffs,NamSelItem::objPoint);
      const_cast<VisNamedSelection *>(sel)->RemapIndexes(_loffs,NamSelItem::objLine);
      return false;
    }
  };
}

void SimpleVectorEditorModel::OptimizeAfterDelete()
{
  SRef<int,SRefArrayTraits<int> > offsets=new int[_points.Size()];
  SRef<int,SRefArrayTraits<int> > loffsets=new int[_lines.Size()];
  int i,k,cnt;
  for (i=k=0,cnt=_points.Size();i<cnt;i++)
  {
    if (_points[i].IsDeleted())
    {
      if (i!=k)
        _points[k]=_points[i];
      k++;
    }
    offsets[i]=k;
  } 
  if (i!=k) _points.Delete(k,i-k);
  for (i=k=0,cnt=_lines.Size();i<cnt;i++)
  {   
    if (_lines[i].IsDeleted())
    {
      const SimpleVectorEditorModel::Line &l=_lines[i];
      _lines[k]=SimpleVectorEditorModel::Line(offsets[l.Begin()],offsets[l.End()]);
    }
    loffsets[i]=k;
  }
  if (i!=k) _lines.Delete(k,i-k);

  _properties.ForEach(Functors::OptimizeSelections(Array<int>(offsets,_points.Size()),Array<int>(loffsets,_lines.Size())),0);
}

namespace Functors
{
  class FillMap
  {
    mutable SimpleVectorEditorModel::LineGraph &graph;
  public:
    FillMap(SimpleVectorEditorModel::LineGraph &graph):graph(graph) {}
    bool operator()(int index,const SimpleVectorEditorModel::Line &other) const
    {
      graph.insert(SimpleVectorEditorModel::LineGraph::Pair(other.Begin(),other.End()));
      graph.insert(SimpleVectorEditorModel::LineGraph::Pair(other.End(),other.Begin()));
      return false;
    }
  };
}

SimpleVectorEditorModel::LineGraph::LineGraph(const SimpleVectorEditorModel &source)
{
  source.ForEachLine(Functors::FillMap(*this));
}
SimpleVectorEditorModel::LineGraph::JointList::JointList(const LineGraph &graph, int point)
{
  Iterator it=graph.find(point);
  Iterator e=graph.end();
  while (it!=e && it->first==point) Add(it->second);
}

int SimpleVectorEditorModel::LineGraph::CountJoints(int srcpt) const
{
  int cnt=0;
  Iterator it=find(srcpt);
  Iterator e=end();
  while (it!=e && it->first==srcpt) cnt++;
  return cnt;
}

int SimpleVectorEditorModel::LineGraph::GetJoint(int srcpt, int index) const
{
  Iterator it=find(srcpt);
  Iterator e=end();
  while (index && it!=e && it->first==srcpt) index--;
  if (it==e) return -1;
  return it->second;  
}


void SimpleVectorEditorModel::Save(std::ostream &stm) const
{
  unsigned int sz;
  sz=3;
  stm.write((char *)sz,sizeof(sz));
  sz=_points.Size();
  stm.write((char *)sz,sizeof(sz));
  sz=_lines.Size();
  stm.write((char *)sz,sizeof(sz));
  sz=_properties.Size();
  stm.write((char *)sz,sizeof(sz));
  stm.write((char *)_points.Data(),_points.Size()*sizeof(Point));
  stm.write((char *)_lines.Data(),_lines.Size()*sizeof(Line));
  _properties.Save(stm);
}

void SimpleVectorEditorModel::Load(std::istream &stm)
{
  unsigned int sz=0;
  stm.read((char *)sz,sizeof(sz));
  if (sz!=3) return stm.setstate(ios::failbit,false);
  stm.read((char *)sz,sizeof(sz));
  _points.Resize(sz);
  stm.read((char *)sz,sizeof(sz));
  _lines.Resize(sz);
  stm.read((char *)sz,sizeof(sz));
  stm.read((char *)_points.Data(),_points.Size()*sizeof(Point));
  stm.read((char *)_lines.Data(),_lines.Size()*sizeof(Line));
  _properties.Load(stm);
}

void SimpleVectorEditorModel::PropertySelection::Save(std::ostream &out) const
{
  VisNamedSelection::Save(out);
  unsigned long nameLen=_value.GetLength();
  if (nameLen>0xFFFF) nameLen=0xFFFF;
  unsigned short len=(unsigned short)nameLen;
  out.write((char *)&nameLen,2);
  out.write(_value.Data(),nameLen);
}
bool SimpleVectorEditorModel::PropertySelection::Load(std::istream &in)
{
  if (VisNamedSelection::Load(in)==false) return false;
  unsigned short nameLen=0;
  in.read((char *)&nameLen,2);
  if (in.gcount()!=2) return false;
  char *buff=_value.CreateBuffer(nameLen);
  in.read(buff,nameLen);
  buff[nameLen]=0;
  if (in.gcount()!=nameLen) return false;
  return true;
}

int SimpleVectorEditorModel::PropertySelection::Compare(const VisNamedSelection &other) const
{
  int res=VisNamedSelection::Compare(other);
  if (res!=0) return res;
  const PropertySelection &pother=static_cast<const PropertySelection &>(other);
  return strcmp(_value,pother._value);
}

const SimpleVectorEditorModel::PropertySelection *SimpleVectorEditorModel::FindSelection(const char *name, const char *value) const 
{
  return _properties.GetSelection(PropertySelection(name,value));
}

SimpleVectorEditorModel::PropertySelection *SimpleVectorEditorModel::FindSelection(const char *name, const char *value) 
{
  return _properties.GetSelection(PropertySelection(name,value));
}

void SimpleVectorEditorModel::SetProperty(const char *name, const char *value, const VisSelection &selection)
{
  UnsetProperty(name,selection);
  PropertySelection *sel=FindSelection(name, value);
  if (sel==0) _properties.AddSelectionInstance(sel=new PropertySelection(name,value));
  sel->Include(selection);
}

namespace Functors
{
  class UnsetProperty
  {
    mutable AutoArray<SimpleVectorEditorModel::PropertySelection *>_toDel;
    const VisSelection &_selection;
    SimpleVectorEditorModel::PropertyList *_list;
  public:
    UnsetProperty(SimpleVectorEditorModel::PropertyList *list,
                  const VisSelection &selection
                  ):_selection(selection),_list(list) {}

    int operator()(const SimpleVectorEditorModel::PropertySelection *sel) const
    {
      SimpleVectorEditorModel::PropertySelection *c=const_cast<SimpleVectorEditorModel::PropertySelection *>(sel);
      c->Exclude(_selection);
      if (c->IsEmpty()) _toDel.Add(c);
      return 0;
    }

    ~UnsetProperty()
    {
      for (int i=0;i<_toDel.Size();i++)
        _list->DeleteSelection(*_toDel[i]);
    }
  };

  class GetProperty
  {
    int index;
    NamSelItem::ObjectType type;
    const char *result;
  public:
    GetProperty(int index,NamSelItem::ObjectType type):index(index),type(type),result(0) {}
    int operator()(const SimpleVectorEditorModel::PropertySelection *sel)
    {
      if (sel->IsSelected(type,index))
      {
        result=sel->GetValue();
        return 1;
      }
      else
        return 0;
    }
    const char *GetResult() const
    {
      return result;
    }
  };
}

void SimpleVectorEditorModel::UnsetProperty(const char *name, const VisSelection &selection)
{
  Functors::UnsetProperty fn(&_properties,selection);
  _properties.ForEach(fn,&PropertySelection(name));
}

const char *SimpleVectorEditorModel::GetLineProperty(int index, const char *name)
{
  Functors::GetProperty fn(index,NamSelItem::objLine);
  if (_properties.ForEach(fn,&PropertySelection(name))==0) return 0;
  else return fn.GetResult();
}
const char *SimpleVectorEditorModel::GetPointProperty(int index, const char *name)
{
  Functors::GetProperty fn(index,NamSelItem::objPoint);
  if (_properties.ForEach(fn,&PropertySelection(name))==0) return 0;
  else return fn.GetResult();

}


void SimpleVectorEditorModel::PropertyList::AddSelectionInstance(PropertySelection *instance)
{
  PropertySelection *curSel=GetSelection(instance->GetName());
  if (curSel) {DeleteSelection(*curSel);}
  _selList.Add(curSel=instance);
  _selIndex.Add(curSel);
}



const SimpleVectorEditorModel::PropertySelection *SimpleVectorEditorModel::PropertyList::GetSelection(const PropertySelection &name) const
{
  PropertySelection **x=_selIndex.Find(const_cast<PropertySelection *>(&name));
  return x?*x:0;
}

SimpleVectorEditorModel::PropertySelection *SimpleVectorEditorModel::PropertyList::GetSelection(const PropertySelection &name)
{
  PropertySelection **x=_selIndex.Find(const_cast<PropertySelection *>(&name));
  return x?*x:0;
}

void SimpleVectorEditorModel::PropertyList::DeleteSelection(const PropertySelection &name)
{
  VisNamedSelection *sel=_selIndex.Remove(const_cast<PropertySelection *>(&name));
  for (int i=0;i<_selList.Size();i++) 
    if (_selList[i].GetRef()==sel)
    {
      _selList.Delete(i);
      break;      
    }
}

void SimpleVectorEditorModel::PropertyList::Save(std::ostream &out) const
{
  int cnt=Size();
  out.write((char *)&cnt,sizeof(cnt));
  for (int i=0;i<cnt;i++)
    _selList[i]->Save(out);
}

bool SimpleVectorEditorModel::PropertyList::Load(std::istream &in)
{
  unsigned long cnt=0;
  in.read((char *)&cnt,sizeof(cnt));
  if (in.gcount()!=sizeof(cnt)) return false;
  for (unsigned int i=0;i<cnt;i++)
  {
    PropertySelection *x=new PropertySelection ("");
    if (x->Load(in)==false) 
    {
      delete x;
      return false;
    }
    AddSelectionInstance(x);
  }
  return true;
}

void SimpleVectorEditorModel::Delete(const VisSelection &sel)
{
  for (int i=0;i<_points.Size();i++)
    if (sel.IsSelected(NamSelItem::objPoint,i)) _points[i].SetDeleted();
  for (int i=0;i<_lines.Size();i++)
  {
    if (sel.IsSelected(NamSelItem::objLine,i)) _lines[i].SetDeleted();
    else if (_points[_lines[i].Begin()].IsDeleted() || _points[_lines[i].End()].IsDeleted())
    {
      _lines[i].SetDeleted();
    }
  }
}


void SimpleVectorEditorModel::InsertPoint(const Point &pt, int at)
{
  _points.Insert(at,pt);
  for (int j=0;j<_lines.Size();j++)
  {
    if (_lines[j].Begin()>=at)
      if (_lines[j].End()>=at)
        _lines[j]=Line(_lines[j].Begin()+1,_lines[j].End()+1);
      else
        _lines[j]=Line(_lines[j].Begin()+1,_lines[j].End());
    else if (_lines[j].End()>=at)
      _lines[j]=Line(_lines[j].Begin(),_lines[j].End()+1);
  }  
}
void SimpleVectorEditorModel::InsertLine(const Line &pt, int at)
{
  _lines.Insert(at,pt);

}


TypeIsSimpleZeroed(SimpleVectorEditorModel::PropertySelection *)