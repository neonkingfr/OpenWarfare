#pragma once

#include "VisNamedSelection.h"

class VisSelectionList
{
protected:
	AutoArray<SRef<VisNamedSelection> >        _selList;
	BTree<VisNamedSelection*, BTreeRefCompare> _selIndex;

public:
	VisNamedSelection* CreateSelection(const RStringI& name);
	VisNamedSelection* CreateSelection(const VisNamedSelection& source);
	VisNamedSelection* GetSelection(const RStringI& name);
	void AddSelectionInstance(VisNamedSelection* instance);
	const VisNamedSelection* GetSelection(const RStringI& name) const;
	const VisNamedSelection* GetSelection(const VisNamedSelection& name) const;
	void DeleteSelection(const RStringI& name);
	void DeleteSelection(const VisNamedSelection& name);
  
	template<class Functor> 
	int ForEach(const Functor& funct) const
	{
		BTreeIterator<VisNamedSelection*, BTreeRefCompare> iter(_selIndex);
		VisNamedSelection** sel=iter.Next();
		while (sel)
		{
			int res = funct(*sel);
			if (res) return res;
			sel = iter.Next();
		}
		return 0;
	}

	int Size() const 
	{
		return _selList.Size();
	}

	void Save(std::ostream& out) const;
	bool Load(std::istream& in);
};