#pragma once
#include <afxcmn.h>
#include <afxwin.h>


class SccFunctions;
struct ScriptMenuItem
{
  enum Mode {Normal=0, Popup=1, EndItem=2};

  CString name;
  CString pathname;
  int flags;
  int uid;

  template<class Archive>
  void Serialize(Archive &arch)
  {
    arch(name);
    arch(pathname);
    arch(flags);
    arch(uid);
  }
};

TypeIsMovable(ScriptMenuItem);

template<class Allocator=MemAllocD>
class ScriptMenuItemList: public AutoArray<ScriptMenuItem,Allocator>
{
public:

  int FindId(int id)
  {
    for (int i=0;i<Size();i++) if ((*this)[i].uid==id) return i;
    return -1;
  }

  int InsertToMenu(CMenu &mnu, int atpos, int idoffset, int from=-1)
  {
    from++;
    while (from<Size())
    {
      int i=from;      
      if ((*this)[i].flags & ScriptMenuItem::Popup)
      {
        CMenu newmenu;
        newmenu.CreateMenu();
        mnu.InsertMenu(atpos++,MF_BYPOSITION|MF_POPUP|MF_STRING,(UINT_PTR)newmenu.GetSafeHmenu(),(*this)[i].name);
        from=InsertToMenu(newmenu,0,idoffset, from);
        newmenu.Detach();
      }
      else
      {
        if (strcmp((*this)[i].name,"----")==0)        
          mnu.InsertMenu(atpos++,MF_BYPOSITION|MF_SEPARATOR,0,(LPCTSTR)0);        
        else
          mnu.InsertMenu(atpos++,MF_BYPOSITION|MF_STRING,idoffset+(*this)[i].uid,(*this)[i].name);
      }
      if ((*this)[i].flags & ScriptMenuItem::EndItem) break;
      from++;
    }
    return from;
  }

  template<class Archive>
  void Serialize(Archive &arch)
  {
    int n=Size();
    arch(n);
    if (-arch) 
    {
      Clear();
      if (n==0) return;
      Access(n-1);
    }
    for (int i=0;i<n;i++) (*this)[i].Serialize(arch);
  }

  class SaveClass
  {
    ostream &out;
  public:
    SaveClass(ostream &out):out(out) {}
    void operator()(int &p) {out.write((char *)&p,sizeof(p));}
    void operator()(CString &p) {out.write((const char *)p,p.GetLength()+1);}
    bool operator-() {return false;}
  };

  class LoadClass
  {
    istream &in;
  public:
    LoadClass(istream &in):in(in) {}
    void operator()(int &p) {in.read((char *)&p,sizeof(p));}
    void operator()(CString &p)
      {
        p="";
        char buff[256];
        do
        {
          in.get(buff,256,'\0');
          p=p+buff;
        }
        while (!in.fail());
        in.clear();
        in.get();
      }
    bool operator-() {return true;}
  };

  char *Save(int &sz)
  {
    ostrstream out;
    SaveClass save(out);
    Serialize(save);
    sz=out.pcount();
    return out.str();
  }

  void Load(const char *data, int size)
  {
    istrstream in(data,size);
    LoadClass load(in);
    Serialize(load);
  }
};


TypeIsMovable(CString)
// DlgManageScripts dialog

class DlgManageScripts : public CDialog
{
	DECLARE_DYNAMIC(DlgManageScripts)
    ScriptMenuItemList<> &_menu;
    AutoArray<CString> _files;
    AutoArray<int> _ids;
    Pathname _lastPath;
    CImageList _ilist;
    HTREEITEM _dragItem;
    CImageList *_dragItemIcon;
    SccFunctions *_scc;
public:
	DlgManageScripts(ScriptMenuItemList<> &menu,CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgManageScripts();

// Dialog Data
	enum { IDD = IDD_MANAGESCRIPTS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

    void MenuToTree(void);

    long AddFile(const CString &fname, int uid);
    void SubMenuToTree(HTREEITEM submenu, int &offset);
    void TreeToSubMenu(HTREEITEM submenu);
    CTreeCtrl wTree;
    afx_msg void OnBnClickedAdd();
    int GuessID();
public:

    void SetScc(SccFunctions *scc) {_scc=scc;}
    static bool SelectNewScript(Pathname &scr,AutoArray<Pathname> *multiple);
    afx_msg void OnBnClickedNewgroup();
    virtual BOOL OnInitDialog();
protected:
  virtual void OnOK();
public:
  afx_msg void OnBnClickedDelete();
  afx_msg void OnTvnSelchangedTree(NMHDR *pNMHDR, LRESULT *pResult);
  CEdit wPathname;
  afx_msg void OnTvnEndlabeleditTree(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnEnKillfocusEdit1();
  afx_msg void OnBnClickedAbout();
  afx_msg void OnBnClickedEdit();
  afx_msg void OnBnClickedBrowse();
  afx_msg void OnTvnBegindragTree(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnMouseMove(UINT nFlags, CPoint point);
  afx_msg void OnLButtonUp(UINT nFlags, CPoint point);

  void MoveItemToItemNoTest(HTREEITEM what, HTREEITEM where,bool last=false);
  void MoveItemToItem(HTREEITEM what, HTREEITEM where);
  afx_msg void OnBnClickedSeparator();
  int GuessIDExt();
};
#pragma once


