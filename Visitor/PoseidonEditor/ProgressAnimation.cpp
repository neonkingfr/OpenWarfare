// ProgressAnimation.cpp : implementation file
//

#include "stdafx.h"
#include "ProgressAnimation.h"
#include ".\progressanimation.h"


// CProgressAnimation

IMPLEMENT_DYNAMIC(CProgressAnimation, CStatic)
CProgressAnimation::CProgressAnimation()
{
  _bornTime=GetTickCount();
}

CProgressAnimation::~CProgressAnimation()
{
}


BEGIN_MESSAGE_MAP(CProgressAnimation, CStatic)
  ON_WM_PAINT()
END_MESSAGE_MAP()



DWORD CProgressAnimation::GetCurTime()
{
  return GetTickCount()-_bornTime;
}

void CProgressAnimation::DrawAnim(CDC &dc, CRect &rc)
{
  DWORD totalTime=1500;
  DWORD ctime=GetCurTime() % totalTime;
  CSize sz=rc.Size();
  COLORREF color=_stop?RGB(255,0,0):GetSysColor(COLOR_HIGHLIGHT);

  int totalLen=sz.cx+sz.cy;
  int pos=totalLen*ctime/totalTime;
  int end=pos+sz.cx/2;
  int endx=end;
  int w=sz.cy/2;
  int w2=sz.cy-w;
  if (endx>sz.cx)
  {
    int endy=endx-sz.cx;    
    if (endy>sz.cy)
    {
      int endz=endy-sz.cy;
      dc.FillSolidRect(rc.right-endz,rc.top+w,endz,w2,color);
      dc.FillSolidRect(rc.left,rc.top,endz,w,color);
      endy=sz.cy;
    }
    int beg=pos-sz.cx;
    if (beg<0) beg=0;
    if (beg<sz.cy)
    {
      dc.FillSolidRect(rc.right-w,rc.top+beg,w2,endy-beg,color);
      dc.FillSolidRect(rc.left,rc.top+sz.cy-endy,w,endy-beg,color);
    }
    endx=sz.cx;
  }
  int beg=pos;
  if (beg<sz.cx)
  {
    dc.FillSolidRect(rc.left+beg,rc.top,endx-beg,w,color);
    dc.FillSolidRect(rc.right-endx,rc.top+w,endx-beg,w2,color);
  }
  CBrush brsh(color);
  CBrush *old=dc.SelectObject(&brsh);
  dc.SelectStockObject(NULL_PEN);
  dc.Ellipse(sz.cx/2-3,w-3,sz.cx/2+4,w+4);
  dc.SelectObject(old);
}


// CProgressAnimation message handlers


void CProgressAnimation::OnPaint()
{
  CPaintDC dc(this); // device context for painting
  CRect rc;
  GetClientRect(&rc);
  if (_hlpBmp.GetSafeHandle()==0)
  {
    _hlpBmp.CreateCompatibleBitmap(&dc,rc.Size().cx,rc.Size().cy);
  }
  CDC drawDc;
  drawDc.CreateCompatibleDC(&dc);
  CBitmap *oldBmp=drawDc.SelectObject(&_hlpBmp);
  CRect drawRect(CPoint(0,0),rc.Size());
  drawDc.FillSolidRect(&drawRect,GetSysColor(COLOR_BTNFACE));
  DrawAnim(drawDc,drawRect);  
  dc.BitBlt(0,0,drawRect.right,drawRect.bottom,&drawDc,0,0,SRCCOPY);
  drawDc.SelectObject(oldBmp);
}
