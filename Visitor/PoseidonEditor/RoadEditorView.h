#pragma once
#include "iadditionaleditor.h"
#include "SimpleVectorEditorDoc.h"

class RoadEditorView : public ExtraEditorClientDefault, public MVC::View<VectorModel>
{
  int _batchCount;  
  IExtraEditorServer *_server;
  enum SelMode
  {
    smPoints,
    smLines
  };
  SelMode _selMode;

private:
  friend class Transformer;
  class Transformer: public DynamicExtraEditorClient<>
  {
    RoadEditorView &_outer;
    CPoint _startPt;
    Matrix4 _lastMx;
    bool _moving;
  public:
    Transformer(RoadEditorView &outer, CPoint startPt):
        _outer(outer),_startPt(startPt),_moving(false) {}
    virtual bool OnMouseEvent(MouseEvent msEv, const CPoint &pt, UINT flags, bool &lockMouse);
  };

  friend class RectangleSelector;
  class RectangleSelector: public DynamicExtraEditorClient<>
  {
    RoadEditorView &_outer;
    CPoint _startPt;
    CPoint _otherPoint;
    bool _drawing;
  public:
    RectangleSelector(RoadEditorView &outer,const CPoint &pt):_outer(outer),_startPt(pt),_drawing(false) {}
    virtual bool OnMouseEvent(MouseEvent msEv, const CPoint &pt, UINT flags, bool &lockMouse);
  };

  friend class RectangleSelector;
  class PathBuilder: public DynamicExtraEditorClient<>
  {
    RoadEditorView &_outer;
    int _curPoint;
    int _lastPos;
    AutoArray<int> _points;
    bool _drawing;
  public:
    PathBuilder(RoadEditorView &outer,int index):_outer(outer),_curPoint(index),_drawing(false) {}
    virtual bool OnMouseEvent(MouseEvent msEv, const CPoint &pt, UINT flags, bool &lockMouse);
    template<class Allocator>
    AutoArray<CPoint,Allocator> GeneratePolyline(int lastPos);
  };

public:
  RoadEditorView(IExtraEditorServer *s):_batchCount(0),_server(s),_selMode(smPoints) {}
  void Redraw();

  bool BeginBatch(void **startBatch)
  {
    _batchCount++;
    return true;
  }
  
  bool EndBatch()
  {
    _batchCount++;
    Redraw();
    return true;
  }

  virtual bool Add(const SimpleVectorEditorModel::Point &pt, int &index) {Redraw();return true;}  
  virtual bool Add(const SimpleVectorEditorModel::Line &pt, int &index) {Redraw(); return true;}  
  virtual bool Add(const Array<const SimpleVectorEditorModel::Point> &pt, int &index) {Redraw(); return true;}  
  virtual bool Add(const Array<const SimpleVectorEditorModel::Line> &pt, int &index) {Redraw(); return true;}  
  virtual bool SpecialSelOp(const char *name,SelOp selOp,const VisSelection *sel) {Redraw();return true;}


  virtual bool Set(int index, const SimpleVectorEditorModel::Point &pt) {Redraw(); return true;}  
  virtual bool Set(int index, const SimpleVectorEditorModel::Line &pt) {Redraw(); return true;}  

  virtual bool Delete(const VisSelection &sel) {Redraw(); return true;}  
  virtual bool SetProperty(const char *name, const char *value, const VisSelection &sel) { return false;}  
  virtual bool UnsetProperty(const char *name, const VisSelection &selection) { return false;} 
  virtual bool Load(const SimpleVectorEditorModel &what) {Redraw(); return true;}  

  virtual bool Add(const Array<int> &indexes, const Array<const SimpleVectorEditorModel::Point> &pt) {Redraw(); return true;}  
  virtual bool Add(const Array<int> &indexes, const Array<const SimpleVectorEditorModel::Line> &pt) {Redraw(); return true;}  


  virtual bool OnMouseEvent(MouseEvent msEv, const CPoint &pt, UINT flags, bool &lockMouse);
  virtual bool OnDraw(CDC &dc);
  virtual HCURSOR OnSetCursor(const CPoint &pt);
};
