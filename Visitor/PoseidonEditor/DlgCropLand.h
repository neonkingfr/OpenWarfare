#pragma once
#include "afxwin.h"


// DlgCropLand dialog

class DlgCropLand : public CDialog
{
	DECLARE_DYNAMIC(DlgCropLand)

public:
	DlgCropLand(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgCropLand();

// Dialog Data
	enum { IDD = IDD_CROPLAND };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CEdit wTop;
  CEdit wLeft;
  CEdit wHeight;
  CEdit wRight;
  CEdit wWidth;
  CEdit wBottom;
  CButton wMeters;
  CButton wSquares;
  CButton wOk;
  CButton wCancel;
  int vUnits;
  unsigned long vLeft;
  unsigned long vTop;
  unsigned long vRight;
  unsigned long vBottom;
  unsigned long vWidth;
  unsigned long vHeight;
  afx_msg void OnEnChangeLeft();
  afx_msg void OnEnChangeHeight();
  afx_msg void OnEnChangeTop();
  afx_msg void OnEnChangeWidth();
  afx_msg void OnEnChangeRight();
  afx_msg void OnEnChangeBottom();
  void CalculateDiff(CEdit &a, CEdit &b, CEdit &c);
  void CalculateSumm(CEdit &a, CEdit &b, CEdit &c);
protected:
  virtual void OnOK();
};
