#pragma once


// DlgStopScript dialog
#include "ProgressAnimation.h"

class DlgStopScript : public CDialog
{
	DECLARE_DYNAMIC(DlgStopScript)
    
    CWinThread *_bgrDlg;
    ScriptDebuggerBase *_dbg;
    bool _callback;
    CProgressAnimation _progress;    
    const char *_lastExp;

public:
	DlgStopScript(ScriptDebuggerBase *dbg,const char *scriptName,CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgStopScript();

// Dialog Data
	enum { IDD = IDD_STOPSCRIPT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    
    static UINT RunThread(LPVOID ptr);
    static void WINAPI ReplyFromApp(HWND hwnd,UINT uMsg, ULONG_PTR dwData, LRESULT lResult);
    



	DECLARE_MESSAGE_MAP()
    virtual void OnCancel();
public:
  virtual BOOL OnInitDialog();
  afx_msg void OnTimer(UINT nIDEvent);
  afx_msg LRESULT OnExitDlg(WPARAM wParam,LPARAM lParam);
  CString vScriptName;
};
