/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

// PoseidonEditor.h : main header file for the POSEIDONEDITOR application
//

#if !defined(_POSEIDONEDITOR_H_)
#define _POSEIDONEDITOR_H_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CPosEdCommandLineInfo:
class CPosEdCommandLineInfo : public CCommandLineInfo
{
protected:
    // Data
    enum {
        ED_UNKNOWN = 0,
        ED_CONF_FILE
    } m_eLastFlag;

    CString m_pszConfFile; 

public:
    CPosEdCommandLineInfo() : m_eLastFlag(ED_UNKNOWN) 
    {
      TCHAR dir[512];
      GetCurrentDirectory(512, dir);
      m_pszConfFile = dir;
      m_pszConfFile += _T("\\visitor.cfg");
    };
    virtual void ParseParam( LPCTSTR lpszParam, BOOL bFlag, BOOL bLast );

    const CString& GetConfFile() const {return m_pszConfFile;};
};

/////////////////////////////////////////////////////////////////////////////
// CPoseidonApp:

class CPoseidonApp : public CWinApp
{
public:
	CPoseidonApp();
    const CPosEdCommandLineInfo& GetCommandLineInfo() const {return m_cCommandLineInfo;};

protected:
    CPosEdCommandLineInfo m_cCommandLineInfo;    
    
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPoseidonApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual BOOL OnIdle(LONG lCount);
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CPoseidonApp)
	afx_msg void OnHelpAbout();
	afx_msg void OnToolsOptions();
	afx_msg void OnToolsOptsystem();
	afx_msg void OnProjectNew();
	afx_msg void OnProjectOpen();
  afx_msg void OnProjectOpenEx();
	//}}AFX_MSG

  void OnFileOpen();

	DECLARE_MESSAGE_MAP()

};

extern CPoseidonApp theApp;
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(_POSEIDONEDITOR_H_)
