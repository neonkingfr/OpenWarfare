/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

// PosChildFrm.cpp : implementation of the CChildFrame class
//

#include "stdafx.h"
#include "PoseidonEditor.h"

#include "PosChildFrm.h"
#include "PoseidonDoc.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CChildFrame)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP   
  ON_WM_SETFOCUS()
  ON_WM_ACTIVATE()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChildFrame construction/destruction

CChildFrame::CChildFrame()
{
	CPosEdEnvironment* pEnvir = GetPosEdEnvironment();
	m_bMaxFrame = pEnvir->m_optPosEd.m_bMaxProjWnd;
}

/////////////////////////////////////////////////////////////////////////////
// CChildFrame message handlers

void CChildFrame::ActivateFrame(int nCmdShow) 
{
	if (m_bMaxFrame)
	{
		if (nCmdShow==-1) nCmdShow = SW_SHOWMAXIMIZED;
		m_bMaxFrame = FALSE;	// p��t� ji� nemaximalizuji
	}
	CMDIChildWnd::ActivateFrame(nCmdShow);
}

BOOL CChildFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
  if (!m_Rulers.CreateRulers(this, pContext)) {
    TRACE("Error creation of rulers\n");
    return CMDIChildWnd::OnCreateClient(lpcs, pContext);
  }

  return TRUE;
}

void CChildFrame::ShowRulers(BOOL bShow)
{
  m_Rulers.ShowRulers(bShow);
}

void CChildFrame::UpdateRulersInfo(stRULER_INFO stRulerInfo, RulerOrient orient)
{
  m_Rulers.UpdateRulersInfo(stRulerInfo, orient);
}

void CChildFrame::OnSetFocus(CWnd* pOldWnd)
{
  m_Rulers.ShowRulers(GetPosEdEnvironment()->m_optPosEd.m_ViewStyle.m_bShowRuler);
  CMDIChildWnd::OnSetFocus(pOldWnd);
}

void CChildFrame::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
  CMDIChildWnd::OnActivate(nState, pWndOther, bMinimized);
  // Update SS status
  CPoseidonDoc * doc = static_cast<CPoseidonDoc *>(GetActiveDocument());
  if (doc)
    doc->UpdateSSStatus();
}
