#include <fstream>

#include "stdafx.h"
#include "StringList.h"

using std::ofstream;
using std::ios;
using std::endl;

StringList::StringList()
{
}

void StringList::Filename(const char* filename)
{
	m_Filename = filename;
}

bool StringList::Add(const string& stringItem, bool onlyIfNotExist)
{
	if (onlyIfNotExist)
	{
		for (size_t i = 0, cnt = m_List.size(); i < cnt; ++i)
		{
			if (m_List[i] == stringItem) return false;
		}
	}

	m_List.push_back(stringItem);
	return true;
}

bool StringList::Save() const
{
	if (m_Filename.length() == 0) return false;

	ofstream file;
	file.open(m_Filename.c_str(), ios::out);

	if (file.fail()) return false;

	for (size_t i = 0, cnt = m_List.size(); i < cnt; ++i)
	{
		file << m_List[i] << endl;
		if (file.fail()) return false;
	}

	file.close();
	return true;
}