#pragma once

#include <el/BTree/Btree.h>
#include <iostream>

struct NamSelItem
{
	enum ObjectType
	{    
		objInstance,
		objWood,
		objNet,
		objArea,
		objKeyPoint,
		objPoint,
		objLine,
		objNoMoreTypes = INT_MAX
	};

	///object reference ID; 
	unsigned int _id; 
	///Object type (name of array, from which id comes)
	ObjectType _type:8;
	///TRUE, if this item means end of range of objects
	bool _endOfRange;

	NamSelItem(unsigned int id, ObjectType type, bool endOfRange = false)
	: _id(id)
	,_type(type)
	,_endOfRange(endOfRange) 
	{
	}

	NamSelItem()
	: _id(-1) 
	{
	}

	int Compare(const NamSelItem& other) const
	{
		if (other._type != _type)
		{
			return (_type > other._type) - (_type < other._type);
		}
		else
		{
			return (_id > other._id) - (_id < other._id);
		}
	}
};

class VisSelection
{
	BTree<NamSelItem, BTreeStaticCompareDefUnset> _selection;

public:
	void Include(NamSelItem::ObjectType type, unsigned int id);
	void Include(NamSelItem::ObjectType type, unsigned int first, unsigned int last);
	void Include(const VisSelection& sel);
	void Exclude(NamSelItem::ObjectType type, unsigned int id);
	void Exclude(NamSelItem::ObjectType type, unsigned int first, unsigned int last);
	void Exclude(const VisSelection& sel);
	void Crop(const VisSelection& sel);

	void SetSelected(NamSelItem::ObjectType type, unsigned int id, bool select);
	void SetSelected(NamSelItem::ObjectType type, unsigned int first, unsigned int last, bool select);

	bool IsSelected(NamSelItem::ObjectType type, unsigned int id) const;
	bool IsSelected(const VisSelection& sel) const;
	bool IsSelected(NamSelItem::ObjectType type, unsigned int first, unsigned int last) const;

	void Clear();
	bool IsEmpty() const;

	VisSelection() 
	{
	}

	VisSelection(const VisSelection& other)
	: _selection(other._selection) 
	{
	}

	VisSelection(NamSelItem::ObjectType type, unsigned int id)
	{
		Include(type, id);
	}

	VisSelection(NamSelItem::ObjectType type, unsigned int first, unsigned int last)
	{
		Include(type, first, last);
	}

	template<class Functor>
	int ForEachSelected(const Functor& func, const NamSelItem::ObjectType* objtype = 0) const
	{
		BTreeIterator<NamSelItem, BTreeStaticCompareDefUnset> iter(_selection);
		NamSelItem* firstItem;
		NamSelItem* nextItem;
		if (objtype) iter.BeginFrom(NamSelItem(0, *objtype));
		firstItem = iter.Next();
		while (firstItem && (objtype == 0 || firstItem->_type == *objtype))
		{
			NamSelItem::ObjectType curType=firstItem->_type;
			unsigned int firstId = firstItem->_id;
			unsigned int lastId = firstId;

			nextItem = iter.Next();
			while (nextItem != 0 && nextItem->_endOfRange && nextItem->_type == curType)
			{
				lastId = nextItem->_id;
				nextItem = iter.Next();
			}
			int res = func(curType, firstId, lastId);
			if (res) return res;
			firstItem = nextItem;
		}
		return 0;
	}

	void Save(std::ostream& out) const;
	bool Load(std::istream& in);

	void RemapIndexes(const Array<int>& imap, NamSelItem::ObjectType type);
	NamSelItem::ObjectType NearNextObjectType(NamSelItem::ObjectType type) const;
};

class VisNamedSelection : public VisSelection
{
	RStringI _name;

public:
	VisNamedSelection(const char* name = 0)
	: _name(name) 
	{
	}

	VisNamedSelection(const RStringI& name)
	: _name(name) 
	{
	}

	VisNamedSelection(const char* name, const VisSelection& sel)
	: _name(name)
	, VisSelection(sel) 
	{
	}

	VisNamedSelection(const RStringI& name, const VisSelection& sel)
	: _name(name)
	, VisSelection(sel) 
	{
	}
  
	 template<class Str>
	void SetName(const Str& name) 
	{
		_name = name;
	}

	const RStringI& GetName() const 
	{
		return _name;
	}

	virtual int Compare(const VisNamedSelection& other) const
	{
		return _name.Cmp(other._name);
	}

	void Save(std::ostream& out) const;
	bool Load(std::istream& in);
};