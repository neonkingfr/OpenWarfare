/*********************************************************************/
/*																	 */
/*	Poseidon Editor  											     */
/*									Copyright � Mentar, 1998         */
/*																	 */
/*********************************************************************/

// PoseidonView.h : interface of the CPoseidonView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(_POSEIDONVIEW_H_)
#define _POSEIDONVIEW_H_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CPoseidonView : public CPosEdMainView
{
DECLARE_DYNCREATE(CPoseidonView)
protected: 
	CPoseidonView();
	
// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPoseidonView)
	//}}AFX_VIRTUAL

// Implementation
public:

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CPoseidonView)
	afx_msg void OnViewStyle();

	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(_POSEIDONVIEW_H_)
