// DlgCropLand.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "DlgCropLand.h"
#include ".\dlgcropland.h"


// DlgCropLand dialog

IMPLEMENT_DYNAMIC(DlgCropLand, CDialog)
DlgCropLand::DlgCropLand(CWnd* pParent /*=NULL*/)
	: CDialog(DlgCropLand::IDD, pParent)
  , vUnits(0)
  , vLeft(0)
  , vTop(0)
  , vBottom(0)
  , vRight(0)
  , vWidth(0)
  , vHeight(0)
{
}

DlgCropLand::~DlgCropLand()
{
}

void DlgCropLand::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_TOP, wTop);
  DDX_Control(pDX, IDC_LEFT, wLeft);
  DDX_Control(pDX, IDC_HEIGHT, wHeight);
  DDX_Control(pDX, IDC_RIGHT, wRight);
  DDX_Control(pDX, IDC_WIDTH, wWidth);
  DDX_Control(pDX, IDC_BOTTOM, wBottom);
  DDX_Control(pDX, IDC_METERS, wMeters);
  DDX_Control(pDX, IDC_SQUARES, wSquares);
  DDX_Control(pDX, IDOK, wOk);
  DDX_Control(pDX, IDCANCEL, wCancel);
  DDX_Radio(pDX,IDC_METERS,vUnits);
  DDX_Text(pDX, IDC_TOP, vTop);
  DDX_Text(pDX, IDC_LEFT, vLeft);
  DDX_Text(pDX, IDC_HEIGHT, vHeight);
  DDX_Text(pDX, IDC_RIGHT, vRight);
  DDX_Text(pDX, IDC_WIDTH, vWidth);
  DDX_Text(pDX, IDC_BOTTOM, vBottom);
}


BEGIN_MESSAGE_MAP(DlgCropLand, CDialog)
  ON_EN_CHANGE(IDC_LEFT, OnEnChangeLeft)
  ON_EN_CHANGE(IDC_HEIGHT, OnEnChangeHeight)
  ON_EN_CHANGE(IDC_TOP, OnEnChangeTop)
  ON_EN_CHANGE(IDC_WIDTH, OnEnChangeWidth)
  ON_EN_CHANGE(IDC_RIGHT, OnEnChangeRight)
  ON_EN_CHANGE(IDC_BOTTOM, OnEnChangeBottom)
END_MESSAGE_MAP()


// DlgCropLand message handlers

void DlgCropLand::OnEnChangeLeft()
{
  if (GetFocus()==&wLeft) CalculateDiff(wRight,wLeft,wWidth);
}

void DlgCropLand::OnEnChangeHeight()
{
  if (GetFocus()==&wHeight) CalculateSumm(wBottom,wHeight,wTop);
}

void DlgCropLand::OnEnChangeTop()
{
  if (GetFocus()==&wTop) CalculateDiff(wTop,wBottom,wHeight);
}

void DlgCropLand::OnEnChangeWidth()
{
  if (GetFocus()==&wWidth) CalculateSumm(wLeft,wWidth,wRight);
}

void DlgCropLand::OnEnChangeRight()
{
  if (GetFocus()==&wRight) CalculateDiff(wRight,wLeft,wWidth);
}

void DlgCropLand::OnEnChangeBottom()
{
  if (GetFocus()==&wBottom) CalculateDiff(wTop,wBottom,wHeight);
}

void DlgCropLand::CalculateDiff(CEdit &a, CEdit &b, CEdit &c)
{
  char tmp[50];
  unsigned int na=GetDlgItemInt(a.GetDlgCtrlID());
  unsigned int nb=GetDlgItemInt(b.GetDlgCtrlID());
  if (na<nb)
  {
    wOk.EnableWindow(FALSE);
    c.SetWindowText("");
  }
  else
  {
    if (c.GetWindowTextLength()==0) wOk.EnableWindow(TRUE);
    c.SetWindowText(_itoa(na-nb+1,tmp,10));
  }
}
void DlgCropLand::CalculateSumm(CEdit &a, CEdit &b, CEdit &c)
{
  char tmp[50];
  unsigned int na=GetDlgItemInt(a.GetDlgCtrlID());
  unsigned int nb=GetDlgItemInt(b.GetDlgCtrlID());
  c.SetWindowText(_itoa(na+nb-1,tmp,10));
  wOk.EnableWindow(TRUE);
}

void DlgCropLand::OnOK()
{
  if (UpdateData()==FALSE) return;
  if (vLeft>vRight)
  {
    MessageBeep(MB_ICONASTERISK);
    wWidth.SetFocus();
    return;
  }
  if (vBottom>vTop)
  {
    MessageBeep(MB_ICONASTERISK);
    wHeight.SetFocus();
    return;
  }

  CDialog::OnOK();
}
