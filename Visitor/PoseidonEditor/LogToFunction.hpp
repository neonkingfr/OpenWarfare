#pragma once

#include <Es/essencepch.hpp>
#include <Es/Framework/appFrame.hpp>

typedef void (*LogFunction)(void * context, const char *format, va_list argptr);

class ToFunctionAppFrameFunctions : public AppFrameFunctions
{
protected:
  LogFunction _fnc;
  void * _context;
public:
  ToFunctionAppFrameFunctions(LogFunction fnc, void * context): _fnc(fnc), _context(context) {};
  virtual ~ToFunctionAppFrameFunctions() {};

  virtual void LogF(const char *format, va_list argptr) {_fnc(_context, format, argptr);}; 

};


