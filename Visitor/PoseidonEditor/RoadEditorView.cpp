#include "StdAfx.h"
#include ".\roadeditorview.h"



void RoadEditorView::Redraw()
{
  _server->GetExtraEditorWindow()->Invalidate(TRUE);
}

TypeIsSimpleZeroed(CPoint);

template<class NetDrawFunctor>
void DrawNetwork(IExtraEditorServer *editorServer, const SimpleVectorEditorModel &model, const NetDrawFunctor &fn)
{
  const SimpleVectorEditorModel::PropertySelection *sel=model.FindSelection(model.SSpecialSelName,model.SSpecialSelected);
  const SimpleVectorEditorModel::PropertySelection *hid=model.FindSelection(model.SSpecialSelName,model.SSpecialHidden);
  const SimpleVectorEditorModel::PropertySelection *lock=model.FindSelection(model.SSpecialSelName,model.SSpecialLocked);

  AutoArray<CPoint,MemAllocStack<CPoint,256> > points;
  for (unsigned int i=0;i<model.GetPointCount();i++)
  {    
    editorServer->TransformWorldToScreen(Array<const Vector3>(&model.Points()[i],1),Array<CPoint>(&points.Append(),1));
  }

  for(int xsel=0;xsel<2;xsel++)
  {  
    for (unsigned int i=0;i<model.GetLineCount();i++) 
    {
      bool bhid=hid && hid->IsSelected(NamSelItem::objLine,i);
      bool bsel=sel && sel->IsSelected(NamSelItem::objLine,i);
      bool block=lock && lock->IsSelected(NamSelItem::objLine,i);
      if (bsel==(xsel==1) && !bhid)
      {
        const SimpleVectorEditorModel::Line &ln=model.Lines()[i];
        if (fn(i,points[ln.Begin()],points[ln.End()],bsel,block)==false) break;
      }
    }  


    for (unsigned int i=0;i<model.GetPointCount();i++) 
    {
      bool bhid=hid && hid->IsSelected(NamSelItem::objPoint,i);
      bool bsel=sel && sel->IsSelected(NamSelItem::objPoint,i);
      bool block=lock && lock->IsSelected(NamSelItem::objPoint,i);
      if (bsel==(xsel==1) && (!bhid || bsel)) 
      {
        const SimpleVectorEditorModel::Point &pt=model.Points()[i];
        if (fn(i,points[i],bsel,block,pt.GetPointType()==SimpleVectorEditorModel::ptImportant)==false) break;
      }
    }
  }
}

namespace Functors
{
  class DrawStdNetwork
  {
    mutable CPen _basicColor;
    mutable CPen _selectedColor;
    mutable CPen _lockColor;
    mutable CPen _importantPoint;
    mutable CBrush _basicColorBrush;
    mutable CBrush _selectedColorBrush;
    mutable CBrush _lockColorBrush;
    mutable CBrush _importantPointBrush;
    CDC &_dc;
    CPen *_old;
    CBrush *_oldBrush;
  public:
    DrawStdNetwork(CDC &dc,COLORREF basicColor=0xFFFF80,
                    COLORREF selectedColor=0xFF8080,
                    COLORREF lockColor=0x8080FF,
                    COLORREF importantPoint=0xFF80FF):_basicColor(PS_SOLID,1,basicColor),
                                            _selectedColor(PS_SOLID,1,selectedColor),
                                            _lockColor(PS_SOLID,1,lockColor),
                                            _importantPoint(PS_SOLID,1,importantPoint),
                                            _basicColorBrush(basicColor),
                                            _selectedColorBrush(selectedColor),
                                            _lockColorBrush(lockColor),
                                            _importantPointBrush(importantPoint),
                                            _dc(dc)

    {
      _old=_dc.GetCurrentPen();
      _oldBrush=_dc.GetCurrentBrush();      
    }
  
    ~DrawStdNetwork()
    {
      _dc.SelectObject(_old);
      _dc.SelectObject(_oldBrush);
    }

    bool operator()(unsigned int index,const CPoint &from, const CPoint &to, bool sel, bool locked) const
    {
      if (sel) _dc.SelectObject(&_selectedColor);
      else if(locked) _dc.SelectObject(&_lockColor);
      else _dc.SelectObject(&_basicColor);
      _dc.MoveTo(from);
      _dc.LineTo(to);
      return true;
    }

    bool operator()(unsigned int index,const CPoint &pt, bool sel, bool locked, bool important) const
    {
      CPoint f=pt;
      _dc.LPtoDP(&f);
      if (sel) _dc.SelectObject(&_selectedColorBrush);
      else if(locked) _dc.SelectObject(&_lockColorBrush);
      else if (important) _dc.SelectObject(&_importantPointBrush);
      else _dc.SelectObject(&_basicColorBrush);
      _dc.SelectStockObject(NULL_PEN);   
       CSize sz(4,4);
      _dc.DPtoLP(&sz);
      _dc.Ellipse(pt.x-sz.cx,pt.y-sz.cy,pt.x+sz.cx,pt.y+sz.cy);
      return true;
    }

  };

  class CursorOnDrawing
  {
    const CPoint &_pt;
  public:
    enum HitTest {htNone, htPoint, htLine};
  private:
    mutable unsigned int _index;
    mutable HitTest _ht;
    mutable float _bestLen;
    bool _selonly;
  public:
    CursorOnDrawing(const IExtraEditorServer *server,const CPoint &pt, bool selonly,float distance=4):_pt(pt),_bestLen(distance),_selonly(selonly),_ht(htNone) 
    {
      CRect rc(server->GetGlobalMsPosition(CPoint(0,0)),server->GetGlobalMsPosition(CPoint(1,1)));
      CSize sz=rc.Size();
      _bestLen*=sz.cx;      
    }

    HitTest GetHitTest() const {return _ht;}
    int GetHitIndex() const {return _index;}    

    bool operator()(unsigned int index,const CPoint &from, const CPoint &to, bool sel, bool locked) const
    {
      if (locked) return true;
      if (!sel && _selonly) return true;
      CPoint vec=(from-to);
      CPoint kvec(vec.y,-vec.x);
      int d1=-from.x*kvec.x+from.y*kvec.y;
      int d2=-_pt.x*kvec.x+_pt.y*kvec.y;
      int ld=d2-d1;
      float d=sqrt(kvec.x*kvec.x+kvec.y*kvec.y);
      float shift=ld/d;
      CPoint nwfrom(toInt(from.x+kvec.x*shift),toInt(from.y+kvec.y*shift));
      CPoint nwptdir=nwfrom-_pt;
      if (nwptdir.x*vec.x+nwptdir.y*vec.y<0) return true;
      float nd=sqrt(nwptdir.x*nwptdir.x+nwptdir.y*nwptdir.y);
      if (nd>d) return true;
      float fld=fabs(shift);
      if (fld<_bestLen) 
      {
        _bestLen=fld;
        _ht=htLine;
        _index=index;
      }
      return true;
    }
    bool operator()(unsigned int index,const CPoint &pt, bool sel, bool locked, bool important) const
    {
      if (locked) return true;
      if (!sel && _selonly) return true;
      CPoint diff=_pt-pt;
      float d=sqrt((float)diff.x*(float)diff.x+(float)diff.y*diff.y);
      if (d<_bestLen)
      {
        _bestLen=d;
        _ht=htPoint;
        _index=index;
      }
      return true;
    }
  };
}

bool RoadEditorView::OnDraw(CDC &dc)
{
  ISimpleVectorEditor *doc=GetModel();
  const SimpleVectorEditorModel &model=doc->GetModel();  
  DrawNetwork(_server,GetModel()->GetModel(), Functors::DrawStdNetwork(dc));
  return true;
}


HCURSOR RoadEditorView::OnSetCursor(const CPoint &pt)
{
  CPoint cpt=_server->GetGlobalMsPosition(pt);
  Functors::CursorOnDrawing ht(_server,cpt,true);
  DrawNetwork(_server,GetModel()->GetModel(),ht);
  if (ht.GetHitTest()!=Functors::CursorOnDrawing::htNone)
    return LoadCursor(0,IDC_UPARROW);
  else
    return LoadCursor(0,IDC_ARROW);
}

bool RoadEditorView::OnMouseEvent(MouseEvent msEv, const CPoint &pt, UINT flags, bool &lockMouse)
{
  const SimpleVectorEditorModel &model=GetModel()->GetModel();
  const SimpleVectorEditorModel::PropertySelection *sel=model.FindSelection(SimpleVectorEditorModel::SSpecialSelName,SimpleVectorEditorModel::SSpecialSelected);
  CPoint point=_server->GetGlobalMsPosition(pt);
  Functors::CursorOnDrawing ht(_server,point,false);
  if (msEv==meLClick)
  {
    DrawNetwork(_server,model,ht);
    Functors::CursorOnDrawing::HitTest httest=ht.GetHitTest();
    int htindex=ht.GetHitIndex();
    if (httest==Functors::CursorOnDrawing::htLine)
    {
      bool selected=sel && sel->IsSelected(NamSelItem::objLine,htindex);
      if ((flags & MK_CONTROL)==0)         
        GetModel()->SpecialSelOp(
            SimpleVectorEditorModel::SSpecialSelected,selected?selopExclude:selopInclude,&VisSelection(
                NamSelItem::objLine,htindex
                                ));
      else      
      {
        _server->ActivateExEditor(new Transformer(*this,point));      
        lockMouse=true;
      }
      
    }
    else if (httest==Functors::CursorOnDrawing::htPoint)
    {
      bool selected=sel && sel->IsSelected(NamSelItem::objPoint,htindex);
        if ((flags & MK_CONTROL)==0)         
          GetModel()->SpecialSelOp(
            SimpleVectorEditorModel::SSpecialSelected,selected?selopExclude:selopInclude,&VisSelection(
            NamSelItem::objPoint,htindex
          ));
        else      
        {
          _server->ActivateExEditor(new Transformer(*this,point));      
          lockMouse=true;
        }
    }
    else
    {
      _server->ActivateExEditor(new RectangleSelector(*this,point));
      lockMouse=true;
    }
  }
  else if (msEv==meLDblClick)
  {
    DrawNetwork(_server,model,ht);
    Functors::CursorOnDrawing::HitTest httest=ht.GetHitTest();
    int htindex=ht.GetHitIndex();
    if (httest==Functors::CursorOnDrawing::htPoint)
    {
      if (flags & MK_CONTROL)
      {
        const SimpleVectorEditorModel::Point &pt=model.Points()[htindex];
        if (pt.GetPointType()==SimpleVectorEditorModel::ptImportant) 
          GetModel()->Set(htindex,SimpleVectorEditorModel::Point(pt,SimpleVectorEditorModel::ptHelper));
        else
          GetModel()->Set(htindex,SimpleVectorEditorModel::Point(pt,SimpleVectorEditorModel::ptImportant));
      }
      else
      {      
        _server->ActivateExEditor(new PathBuilder(*this,htindex));
        lockMouse=true;
      }
    }
    else
    {
      Vector3 vx;
      _server->TransformScreenToWorld(Array<const CPoint>(&point,1),Array<Vector3>(&vx,1));
      int index;
      GetModel()->Add(SimpleVectorEditorModel::Point(vx,SimpleVectorEditorModel::ptHelper),index);
    }
  }
  else if (msEv==meRDblClick)
  {
    DrawNetwork(_server,model,ht);
    Functors::CursorOnDrawing::HitTest httest=ht.GetHitTest();
    int htindex=ht.GetHitIndex();
    if (httest==Functors::CursorOnDrawing::htPoint)
      GetModel()->Delete(VisSelection(NamSelItem::objPoint,htindex));
    else if (httest==Functors::CursorOnDrawing::htLine)
      GetModel()->Delete(VisSelection(NamSelItem::objLine,htindex));
  }
  else
  {
    return false;
  }
  return true;
}

namespace Functors
{
  class DrawMovePreview
  {
    CDC &_dc;
    Matrix4 _transform;

  public:
      DrawMovePreview(CDC &dc,const Matrix4 &transform):_dc(dc),_transform(transform) {}
      bool operator()(unsigned int index,const CPoint &from, const CPoint &to, bool sel, bool locked) const
      {
        if (sel && !locked)
        {
          Vector3 vx1=_transform.FastTransform(Vector3((Coord)from.x,0,(Coord)from.y));
          Vector3 vx2=_transform.FastTransform(Vector3((Coord)to.x,0,(Coord)to.y));
          CPoint cfrom(toInt(vx1[0]),toInt(vx1[2]));
          CPoint cto(toInt(vx2[0]),toInt(vx2[2]));
          _dc.MoveTo(cfrom);
          _dc.LineTo(cto);
        }
        return true;
      }

      bool operator()(unsigned int index,const CPoint &pt, bool sel, bool locked, bool important) const
      {
        return false;
      }
  };
}


bool RoadEditorView::Transformer::OnMouseEvent(MouseEvent msEv, const CPoint &pt, UINT flags, bool &lockMouse)
{
  CPoint point=_outer._server->GetGlobalMsPosition(pt);
  CPoint dist=point-_startPt;
  if (msEv==meLRelease || (flags & MK_LBUTTON)==0)
  {
    lockMouse=false;
    if (_moving)
    {
      Vector3 res;
      _outer._server->TransformScreenToWorld(Array<const CPoint>(&dist,1),Array<Vector3>(&res,1));
      Matrix4 mx(MTranslation,res);
       MVC::Model<VectorModel> *doc=_outer.GetModel();
      const SimpleVectorEditorModel &mdl=doc->GetModel();
      doc->Transform(Matrix4(MTranslation,res));
    }
    _outer._server->ActivateExEditor(&_outer);
  }
  else
  {
    CClientDC dc(_outer._server->GetExtraEditorWindow());
    _outer._server->PrepareDC(&dc);
    dc.SetROP2(R2_NOT);
    dc.SelectStockObject(WHITE_PEN);
    if (_moving)
    {
      DrawNetwork(_outer._server,_outer.GetModel()->GetModel(),Functors::DrawMovePreview(dc,_lastMx));
    }
    if (_moving || abs(dist.x)+abs(dist.y)>3)
    {    
      _lastMx=Matrix4(MTranslation,Vector3((Coord)dist.x,0,(Coord)dist.y));
      DrawNetwork(_outer._server,_outer.GetModel()->GetModel(),Functors::DrawMovePreview(dc,_lastMx));
      _moving=true;
    }
  }
  return true;
}

bool RoadEditorView::RectangleSelector::OnMouseEvent(MouseEvent msEv, const CPoint &pt, UINT flags, bool &lockMouse)
{
  CPoint point=_outer._server->GetGlobalMsPosition(pt);
  CPoint dist=point-_startPt;
  if (msEv==meLRelease || (flags & MK_LBUTTON)==0)
  {
    lockMouse=false;
    if (_drawing)
    {

    }
    _outer._server->ActivateExEditor(&_outer);
  }
  else
  {
    CClientDC dc(_outer._server->GetExtraEditorWindow());
    _outer._server->PrepareDC(&dc);    
    dc.SetROP2(R2_NOT);
    dc.SelectStockObject(WHITE_PEN);
    dc.SelectStockObject(HOLLOW_BRUSH);
    if (_drawing)
    {
      CRect rc(_startPt,_otherPoint);
      dc.Rectangle(&rc);
    }
    if (_drawing || abs(dist.x)+abs(dist.y)>3)
    {
      _otherPoint=point;
      CRect rc(_startPt,_otherPoint);
      dc.Rectangle(&rc);
      _drawing=true;
    }
  }
  return true;
}

template<class Allocator>
AutoArray<CPoint,Allocator> RoadEditorView::PathBuilder::GeneratePolyline(int lastPos)
{
  const SimpleVectorEditorModel &mdl=_outer.GetModel()->GetModel();
  AutoArray<CPoint,Allocator> hlp;
  for (int i=0;i<_points.Size();i++)
  {
    CPoint pt;
    const Vector3 &pos=mdl.Points()[_points[i]];
    _outer._server->TransformWorldToScreen(Array<const Vector3>(&pos,1),Array<CPoint>(&pt,1));
    hlp.Append(pt);
  }
  if (lastPos>=0 && (unsigned)lastPos<mdl.GetPointCount())
  {
    CPoint pt;
    const Vector3 &pos=mdl.Points()[lastPos];
    _outer._server->TransformWorldToScreen(Array<const Vector3>(&pos,1),Array<CPoint>(&pt,1));
    hlp.Append(pt);
  }
  return hlp;
}

namespace Functors
{
  class NearestPoint
  {
    const CPoint &_src;
    mutable int &_result;
    mutable float _distance;    
  public:
    NearestPoint(const CPoint &pt, int &result):_src(pt),_result(result),_distance(FLT_MAX) {_result=-1;}
    bool operator()(unsigned int index,const CPoint &from, const CPoint &to, bool sel, bool locked) const
    {
      return false;
    }
    bool operator()(unsigned int index,const CPoint &pt, bool sel, bool locked, bool important) const
    {
      CPoint diff=pt-_src;
      float d=(float)diff.x*diff.x+(float)diff.y*diff.y;
      if (d<_distance)
      {
        _distance=d;
        _result=index;
      }
      return true; 
    }
  };
}

bool RoadEditorView::PathBuilder::OnMouseEvent(MouseEvent msEv, const CPoint &pt, UINT flags, bool &lockMouse)
{
  CPoint point=_outer._server->GetGlobalMsPosition(pt);
  CClientDC dc(_outer._server->GetExtraEditorWindow());
  _outer._server->PrepareDC(&dc);
  dc.SetROP2(R2_NOT);
  dc.SelectStockObject(WHITE_PEN);
  if (msEv==meMove)
  {
    if (flags & MK_RBUTTON) return false;

    if (_drawing)
    {
      AutoArray<CPoint, MemAllocStack<CPoint,64> > poly=GeneratePolyline<MemAllocStack<CPoint,64> >(_lastPos);
      dc.Polyline(poly.Data(),poly.Size());
    }
    else
    { 
      _points.Add(_curPoint);
      _drawing=true;
    }
    DrawNetwork(_outer._server,_outer.GetModel()->GetModel(),Functors::NearestPoint(point,_lastPos));        

    AutoArray<CPoint, MemAllocStack<CPoint,64> > poly=GeneratePolyline<MemAllocStack<CPoint,64> >(_lastPos);
    dc.Polyline(poly.Data(),poly.Size());
  }
  else if (msEv==meLClick)
  {
    if (flags & MK_RBUTTON)
    {
      AutoArray<CPoint, MemAllocStack<CPoint,64> > poly1=GeneratePolyline<MemAllocStack<CPoint,64> >(_lastPos);
      if (_points.Size()>1) _points.Delete(_points.Size()-1);
      AutoArray<CPoint, MemAllocStack<CPoint,64> > poly2=GeneratePolyline<MemAllocStack<CPoint,64> >(_lastPos);
      dc.Polyline(poly1.Data(),poly1.Size());
      dc.Polyline(poly2.Data(),poly2.Size());
    }
    else
      DrawNetwork(_outer._server,_outer.GetModel()->GetModel(),Functors::NearestPoint(point,_lastPos));
      _points.Add(_lastPos);
  }
  else if (msEv==meLDblClick)
  {
    //TODO: use result;
    _outer._server->ActivateExEditor(&_outer);
  }
  else if (msEv==meRClick) return false;
  else if (msEv==meRDblClick)
  {
    AutoArray<CPoint, MemAllocStack<CPoint,64> > poly1=GeneratePolyline<MemAllocStack<CPoint,64> >(_lastPos);
    dc.Polyline(poly1.Data(),poly1.Size());
    _outer._server->ActivateExEditor(&_outer);
  }
  return true;
}
