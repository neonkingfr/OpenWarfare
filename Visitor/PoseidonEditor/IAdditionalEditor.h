#pragma once

class IExtraEditorClient;
class IExtraEditorServer
{
public:
  virtual CWnd *GetExtraEditorWindow() const=0;
  virtual void TransformWorldToScreen(const Array<const Vector3> &inpoints, Array<CPoint> &outpoints) const=0;
  virtual void TransformScreenToWorld(const Array<const CPoint> &inpoints, Array<Vector3> &outpoints) const=0;
  virtual IExtraEditorClient *ActivateExEditor(IExtraEditorClient *editor)=0;
  virtual CPoint GetGlobalMsPosition(const CPoint &pt) const=0;
  virtual void PrepareDC(CDC* pDC) const=0;
};

class IExtraEditorClient
{
public:

  enum MouseEvent {meMove, meLClick, meRClick, meMClick, meLRelease, meMRelease, meRRelease, meWheel, meLDblClick,meRDblClick, meMDblClick};  

  virtual bool OnMouseEvent(MouseEvent msEv, const CPoint &pt, UINT flags, bool &lockMouse)=0;

  enum KeyEvent {keDown,keUp,keRepeat,keChar,keUnknown};

  virtual bool OnKeyboardEvent(KeyEvent ev, int code, int repeats, unsigned int flags)=0;

  virtual bool OnDraw(CDC &dc)=0;

  virtual HCURSOR OnSetCursor(const CPoint &pt)=0;

  virtual bool OnCopy()=0;
  virtual bool OnPaste()=0;
  virtual bool OnUndo()=0;
  virtual bool OnDelete()=0;
  virtual bool OnRedo()=0;
  virtual bool OnSelectAll()=0;
  virtual bool OnActivateEditor(bool activate)=0;  

  enum CanResult {canTrue=1,canFalse=0,canNotHandled=-1};

  virtual CanResult CanUndo() const=0;
  virtual CanResult CanRedo() const=0;
  virtual CanResult CanPaste() const=0;
  virtual CanResult CanDelete() const=0;
  virtual CanResult CanCopy() const=0;
  virtual CanResult CanSelectAll() const=0;

  virtual bool CanActivate() const=0;
  
    

  virtual bool OnWndMsg(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pResult) {return false;}

  virtual ~IExtraEditorClient(void) {}
};


class ExtraEditorClientDefault: public IExtraEditorClient
{
public:
  bool OnMouseEvent(MouseEvent msEv, const CPoint &pt, UINT flags, bool &lockMouse) {return true;}
  virtual bool OnKeyboardEvent(KeyEvent ev, int code, int repeats, unsigned int flags) {return true;}
  bool OnDraw(CDC &dc) {return true;}
  HCURSOR OnSetCursor(const CPoint &pt) {return 0;}

  virtual bool OnCopy() {return true;}
  virtual bool OnPaste()  {return true;}
  virtual bool OnUndo()  {return true;}
  virtual bool OnDelete()  {return true;}
  virtual bool OnRedo()  {return true;}
  virtual bool OnSelectAll()  {return true;}
  virtual bool OnActivateEditor(bool activate)   {return true;}  

  virtual IExtraEditorClient::CanResult CanUndo() const{return canFalse;}
  virtual IExtraEditorClient::CanResult CanRedo()  const {return canFalse;}
  virtual IExtraEditorClient::CanResult CanPaste()  const {return canFalse;}
  virtual IExtraEditorClient::CanResult CanDelete()   const {return canFalse;}
  virtual IExtraEditorClient::CanResult CanCopy()   const {return canFalse;}
  virtual IExtraEditorClient::CanResult CanSelectAll()   const {return canFalse;}

  virtual bool CanActivate()   const {return true;}
};

template<class Base=ExtraEditorClientDefault>
class DynamicExtraEditorClient: public Base
{
public:
  virtual bool OnActivateEditor(bool activate) 
  {
    if (activate==false) 
        delete this; 
    return true;
  }
};
