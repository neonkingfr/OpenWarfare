#pragma once
#include "..\visitorexchangeinterface\ivisviewerexchange.h"

class CPosEdMainDoc;

///Implements interface between Buldozer and Visitor
/**
This class catches all events from Buldozer and broadcast it to different places in visitor.
Each incoming message is converted into member function with parameters. Implementation 
gets the parameters and dispatches the message.
*/
class ViewerView : public IVisViewerExchangeDocView
{
  IVisViewerExchange &_viewer;

public:
  ViewerView(IVisViewerExchange &viewer);
  ~ViewerView(void);


  void UnimplementedMessage()
  {
    int message_is_not_implemented=0;
    assert(message_is_not_implemented);
  }
//Buldozer events
  virtual void SelectionObjectClear();
  virtual void CursorPositionSet(const SMoveObjectPosMessData &data);
  virtual void ObjectCreate(const SMoveObjectPosMessData &data, const char *name) {UnimplementedMessage();return;}
  virtual void ObjectDestroy(const SMoveObjectPosMessData &data) {UnimplementedMessage();} 
  virtual void SelectionObjectAdd(const SObjectPosMessData &data);
  virtual void SystemInit(const SLandscapePosMessData &data, const char *configName) {UnimplementedMessage();} 
  virtual void FileImportBegin(const STexturePosMessData& data);  
  virtual void FileImport(const char *data) {UnimplementedMessage();} 
  virtual void FileExport(const char *data) {UnimplementedMessage();} 
  virtual void LandHeightChange(const STexturePosMessData& data);
  virtual void LandTextureChange(const STexturePosMessData& data);
  virtual void RegisterObjectType(const char *name) {UnimplementedMessage();} 
  virtual void RegisterLandscapeTexture(const SMoveObjectPosMessData &data, const char *name) {UnimplementedMessage();} 
  virtual void SelectionLandAdd(const SLandSelPosMessData& data);
  virtual void SelectionLandClear();
  virtual void BlockMove(const Array<SMoveObjectPosMessData> &data);
  virtual void BlockSelectionObject(const Array<SMoveObjectPosMessData> &data);
  virtual void BlockSelectionLand(const Array<SLandSelPosMessData> &data);
  virtual void BlockLandHeightChange(const Array<STexturePosMessData> &data);
  virtual void BlockLandHeightChangeInit(const Array<float> &heights){UnimplementedMessage();}
  virtual void BlockLandTextureChangeInit(const Array<int> &ids) {UnimplementedMessage();} 
  virtual void BlockWaterHeightChangeInit(const Array<float> &heights) {UnimplementedMessage();} 
  virtual void ComVersion(int version);
  virtual void ObjectMove(const SMoveObjectPosMessData& data);
  virtual void SystemQuit() {UnimplementedMessage();}
  virtual void FileImportEnd() {UnimplementedMessage();}
  virtual void Import()  {UnimplementedMessage();}

  ///AddRef is not used - empty
  virtual int AddRef() const {return 1;}
  ///Release is not used - empty
  virtual int Release() const {return 0;}



  static CPosEdMainDoc *GetRealDoc();
};
