#pragma once

#include "SimpleVectorEditorModel.h"
#include "ModelViewControler.h"



class ISimpleVectorEditorActions
{
public:
  enum SelOp
  {
    selopClear,
    selopExclude,
    selopInclude,
    selopAll,
    selopUse
  };
  virtual bool Add(const SimpleVectorEditorModel::Point &pt, int &index)=0;
  virtual bool Add(const SimpleVectorEditorModel::Line &pt, int &index)=0;
  virtual bool Add(const Array<const SimpleVectorEditorModel::Point> &pt, int &index)=0;
  virtual bool Add(const Array<const SimpleVectorEditorModel::Line> &pt, int &index)=0;

  virtual bool Set(int index, const SimpleVectorEditorModel::Point &pt)=0;
  virtual bool Set(int index, const SimpleVectorEditorModel::Line &pt)=0;

  virtual bool Delete(const VisSelection &sel)=0;
  virtual bool SetProperty(const char *name, const char *value, const VisSelection &sel)=0;
  virtual bool UnsetProperty(const char *name, const VisSelection &selection)=0;
  virtual bool SpecialSelOp(const char *name,SelOp selOp,const VisSelection *sel)=0;
  virtual bool Load(const SimpleVectorEditorModel &what)=0;

  virtual bool Add(const Array<int> &indexes, const Array<const SimpleVectorEditorModel::Point> &pt)=0;
  virtual bool Add(const Array<int> &indexes, const Array<const SimpleVectorEditorModel::Line> &pt)=0;
  
  virtual bool Transform(const Matrix4 &mx, const VisSelection *what=0) {return false;}
};

class ISimpleVectorEditor:public ISimpleVectorEditorActions
{
public:
  typedef ISimpleVectorEditorActions Actions;

  virtual const SimpleVectorEditorModel &GetModel() const=0;
  const VisSelection &GetSelected() const;
  const VisSelection &GetHidden() const;
  const VisSelection &GetLocked() const;


};

typedef MVC::DefineMVC<ISimpleVectorEditor,MVC::SingleThread > VectorModel;

class SimpleVectorEditorUndo:public MVC::UndoRedo<VectorModel>
{
public:
  typedef MVC::Model<VectorModel> IDoc;
  SimpleVectorEditorUndo(MVC::Model<VectorModel> &owner):MVC::UndoRedo<VectorModel>(owner) {}
  virtual bool Add(const SimpleVectorEditorModel::Point &pt, int &index);
  virtual bool Add(const SimpleVectorEditorModel::Line &pt, int &index);
  virtual bool Add(const Array<const SimpleVectorEditorModel::Point> &pt, int &index);
  virtual bool Add(const Array<const SimpleVectorEditorModel::Line> &pt, int &index);

  virtual bool Set(int index, const SimpleVectorEditorModel::Point &pt);
  virtual bool Set(int index, const SimpleVectorEditorModel::Line &pt);

  virtual bool Delete(const VisSelection &sel);
  virtual bool SetProperty(const char *name, const char *value, const VisSelection &sel);
  virtual bool UnsetProperty(const char *name, const VisSelection &selection);
  virtual bool Load(const SimpleVectorEditorModel &what);

  virtual bool Add(const Array<int> &indexes, const Array<const SimpleVectorEditorModel::Point> &pt);
  virtual bool Add(const Array<int> &indexes, const Array<const SimpleVectorEditorModel::Line> &pt);

  virtual bool SpecialSelOp(const char *name,SelOp selOp,const VisSelection *sel);

};
#pragma warning(push)
#pragma warning(disable: 4355)

class SimpleVectorEditorDoc:public MVC::Model<VectorModel>
{
  SimpleVectorEditorUndo _undo;
  SimpleVectorEditorModel _model;
public:
  SimpleVectorEditorDoc(void):_undo(*this){}  

  virtual bool Add(const SimpleVectorEditorModel::Point &pt, int &index);
  virtual bool Add(const SimpleVectorEditorModel::Line &pt, int &index);
  virtual bool Add(const Array<const SimpleVectorEditorModel::Point> &pt, int &index);
  virtual bool Add(const Array<const SimpleVectorEditorModel::Line> &pt, int &index);
  virtual bool Set(int index, const SimpleVectorEditorModel::Point &pt);
  virtual bool Set(int index, const SimpleVectorEditorModel::Line &pt);
  virtual bool Delete(const VisSelection &sel);
  virtual bool SetProperty(const char *name, const char *value, const VisSelection &sel);
  virtual bool UnsetProperty(const char *name, const VisSelection &selection);
  virtual bool Load(const SimpleVectorEditorModel &what);
  virtual bool Add(const Array<int> &indexes, const Array<const SimpleVectorEditorModel::Point> &pt);
  virtual bool Add(const Array<int> &indexes, const Array<const SimpleVectorEditorModel::Line> &pt);

  virtual const SimpleVectorEditorModel &GetModel() const {return _model;}
  virtual bool SpecialSelOp(const char *name,SelOp selOp,const VisSelection *sel);

  virtual bool Transform(const Matrix4 &mx, const VisSelection *what=0);

};
#pragma warning (pop)
