#include "stdafx.h"
#include <Es/essencepch.hpp>
#include <Es/Framework/appFrame.hpp>

#include <stdio.h>
#include <string.h>
#include <Es/Common/win.h>
#include <Es/Strings/bstring.hpp>

#if _MSC_VER && !defined INIT_SEG_COMPILER
	#pragma warning(disable:4074)
	#pragma init_seg(compiler)
	#define INIT_SEG_COMPILER
#endif

#define _ENABLE_REPORT 1

class ToFileAppFrameFunctions : public AppFrameFunctions
{
public:
	ToFileAppFrameFunctions() {};
	virtual ~ToFileAppFrameFunctions() {};

#if _ENABLE_REPORT
	virtual void LogF(const char *format, va_list argptr);
#endif	
};

static ToFileAppFrameFunctions GAppFrameFunctions INIT_PRIORITY_URGENT;
AppFrameFunctions *CurrentAppFrameFunctions = &GAppFrameFunctions;

#if _ENABLE_REPORT
void ToFileAppFrameFunctions::LogF(const char *format, va_list argptr)
{
	char buf[512];
	vsprintf(buf,format,argptr);
	strcat(buf,"\n");
#ifdef _WIN32
	OutputDebugString(buf);
#endif
	
	TCHAR dir[512];
	GetModuleFileName(NULL, dir, 512);
	
	CString file;
	file.Format(_T("%s%s"),dir, _T(".log")); 
	FILE * fOutput = _tfopen(file,"a");
	fprintf(fOutput,buf);
	fclose(fOutput);
}

#endif

