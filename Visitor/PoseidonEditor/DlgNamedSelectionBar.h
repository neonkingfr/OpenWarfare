#pragma once

// DlgNamedSelectionBar dialog

class DlgNamedSelectionBar : public CDialogBar
{
	CSize m_sizeDocked;
	CSize m_sizeFloating;
	CPosEdMainDoc* m_curDoc;
	int _nameCounter;
	CImageList ilist;
	int _contextItem;

public:
	DlgNamedSelectionBar(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgNamedSelectionBar();

	// Dialog Data
	enum { IDD = IDD_NAMEDSELECTIONSDLG };


	virtual CSize CalcDynamicLayout(int nLength, DWORD dwMode);
	virtual CSize CalcDynamicDim(int nLength, DWORD dwMode);
	void RecalcChildWindowPos(const CSize& size);
	BOOL Create(CWnd* pParentWnd, UINT nIDTemplate, UINT nStyle, UINT nID);

	const CSize& GetSizeDocked() const 
	{
		return m_sizeDocked;
	}

	void CheckDocChanged(CPosEdMainDoc* newdoc)
	{
		if (newdoc != m_curDoc) 
		{
			m_curDoc = newdoc;
			ReloadBar();
		}
	}

	CListCtrl wList;

	void ReloadBar();

	void SelectionAdded(const char* name);
	void SelectionRemoved(const char* name);
	CString OfferUniqueSelectionName();
	CString GetSelectedSelectionName() const;
	void EditSelectionName(const char* name);

	void UseSelection(int selId);

	DECLARE_MESSAGE_MAP()

	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
	afx_msg void OnNamedselAdd();
	afx_msg void OnNamedselDelete();
	afx_msg void OnNamedselRename();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnNamedselRedefine();
	afx_msg void OnNamedselInvertselection();
	afx_msg void OnNamedselSelectall();
	afx_msg void OnNamedselHideselected();
	afx_msg void OnNamedselShowselected();
	afx_msg void OnNamedselShowall();
	afx_msg void OnNMClickList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLvnEndlabeleditList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLvnItemActivateList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnNamedselShowonlyselected();
	afx_msg void OnNamedselLockselected();
	afx_msg void OnNamedselUnlockall();
	afx_msg void OnNamedselUnlockselected();
	afx_msg void OnNamedselDeleteWithObj();
};