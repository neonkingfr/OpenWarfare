#pragma once


// CProgressAnimation

class CProgressAnimation : public CStatic
{
	DECLARE_DYNAMIC(CProgressAnimation)
    DWORD _bornTime;    
    CBitmap _hlpBmp;    
    bool _stop;


public:
	CProgressAnimation();
	virtual ~CProgressAnimation();

protected:
    DWORD GetCurTime();
    void DrawAnim(CDC &dc, CRect &rc);

	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnPaint();
  void SetStop(bool st) {_stop=st;}
};


