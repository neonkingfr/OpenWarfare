// DlgManageScripts.cpp : implementation file
//

#include "stdafx.h"
#include "PoseidonEditor.h"
#include <fstream>
#include <strstream>
#include <El/Pathname/Pathname.h>
using namespace std;
#include "resource.h"
#include "DlgManageScripts.h"
#include ".\dlgmanagescripts.h"
#include <Intshcut.h>

#include <El/Interfaces/iScc.hpp>

// DlgManageScripts dialog

IMPLEMENT_DYNAMIC(DlgManageScripts, CDialog)
DlgManageScripts::DlgManageScripts(ScriptMenuItemList<> &menu,CWnd* pParent /*=NULL*/)
	: CDialog(DlgManageScripts::IDD, pParent),_menu(menu)
{
  _ilist.Create(IDB_MANAGESCRICONS,16,0,RGB(255,0,255));
  _dragItem=0;
  _scc=0;
}

DlgManageScripts::~DlgManageScripts()
{
}

void DlgManageScripts::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_TREE, wTree);
  DDX_Control(pDX, IDC_EDIT1, wPathname);
}


BEGIN_MESSAGE_MAP(DlgManageScripts, CDialog)
  ON_BN_CLICKED(IDC_ADD, OnBnClickedAdd)
  ON_BN_CLICKED(IDC_NEWGROUP, OnBnClickedNewgroup)
  ON_BN_CLICKED(IDC_DELETE, OnBnClickedDelete)
  ON_NOTIFY(TVN_SELCHANGED, IDC_TREE, OnTvnSelchangedTree)
  ON_NOTIFY(TVN_ENDLABELEDIT, IDC_TREE, OnTvnEndlabeleditTree)
  ON_EN_KILLFOCUS(IDC_EDIT1, OnEnKillfocusEdit1)
  ON_BN_CLICKED(IDC_ABOUT, OnBnClickedAbout)
  ON_BN_CLICKED(IDC_EDIT, OnBnClickedEdit)
  ON_BN_CLICKED(IDC_BROWSE, OnBnClickedBrowse)
  ON_NOTIFY(TVN_BEGINDRAG, IDC_TREE, OnTvnBegindragTree)
  ON_WM_MOUSEMOVE()
  ON_WM_LBUTTONUP()
  ON_BN_CLICKED(IDC_SEPARATOR, OnBnClickedSeparator)
END_MESSAGE_MAP()


// DlgManageScripts message handlers

void DlgManageScripts::MenuToTree(void)
{
  if (wTree.GetRootItem()!=0)
    wTree.DeleteAllItems();
  HTREEITEM rt=wTree.InsertItem(CString((LPCTSTR)IDS_VISITORSCRIPTS),0,0);
  wTree.SetItemData(rt,-1);

  int offset=0;
  SubMenuToTree(rt,offset);
  wTree.Expand(rt,TVE_EXPAND);
}

long DlgManageScripts::AddFile(const CString &fname, int uid)
{
  for (int i=0;i<_files.Size();i++) 
    if (_files[i]==fname) 
    {return i;
    }
   if (uid==-1) return -1;
  _files.Append()=fname;
  _ids.Append()=uid;
  return _files.Size()-1;
}

void DlgManageScripts::SubMenuToTree(HTREEITEM submenu, int &offset)
{
  while (offset<_menu.Size())
  {
    HTREEITEM itm=wTree.InsertItem(_menu[offset].name,1,1,submenu,TVI_LAST);
    int idx=offset;
    if (_menu[offset].flags & ScriptMenuItem::Popup)    
    {
      wTree.SetItemData(itm,-1);
      wTree.SetItemImage(itm,2,2);
      offset++;
      SubMenuToTree(itm,offset);
    }
    else
    {  
      wTree.SetItemData(itm,AddFile(_menu[offset].pathname,_menu[offset].uid));    
      offset++;
    }
    if (_menu[idx].flags & ScriptMenuItem::EndItem) break;
  }
}


void DlgManageScripts::TreeToSubMenu(HTREEITEM submenu)
{
  while (submenu)
  {
    HTREEITEM next=wTree.GetNextItem(submenu,TVGN_NEXT);
    ScriptMenuItem item;
    item.name=wTree.GetItemText(submenu);
    int data=wTree.GetItemData(submenu);
    if (data==-1)
    {
      HTREEITEM subItem=wTree.GetChildItem(submenu);
      if (subItem)
      {
        item.uid=-1;
        item.flags=item.Popup|(next?0:item.EndItem);        
        _menu.Append()=item;
        TreeToSubMenu(subItem);
      }
    }
    else
    {
      item.pathname=_files[data];
      item.uid=_ids[data];
      item.flags=item.Normal|(next?0:item.EndItem);
      _menu.Append()=item;
    }
    submenu=next;
  }
}


void DlgManageScripts::OnBnClickedAdd()
{
  HTREEITEM itm=wTree.GetSelectedItem();
  int data=itm==0?-1:wTree.GetItemData(itm);
  if (data!=-1)
  {
    _lastPath=_files[data];
  }
  _lastPath.SetFilename("");
  AutoArray<Pathname> mfiles;
  if (SelectNewScript(_lastPath,&mfiles))
  {
    if (itm==0) itm=TVI_ROOT;
    else if (data!=-1) itm=wTree.GetParentItem(itm);
    for (int i=0;i<mfiles.Size();i++)
    {
      HTREEITEM res=wTree.InsertItem(mfiles[i].GetTitle(),1,1,itm,TVI_LAST);
      wTree.SetItemData(res,AddFile(CString(mfiles[i]),GuessID()));
      wTree.EnsureVisible(res);
      wTree.EditLabel(res);
    }
  }
}


int DlgManageScripts::GuessID()
  {
    AutoArray<unsigned long,MemAllocStack<unsigned long,32> > idarr;
    for(int i=0;i<_ids.Size();i++)
    {
      int id=_ids[i];
      if (id!=-1)
      {
        idarr.Access(id/32);
        idarr[id/32]|=(1<<(id&31));
      }
    }
    for (int i=0;i<idarr.Size();i++) if (idarr[i]!=0xFFFFFFFF)
    {
      unsigned long j=idarr[i];
      int id=0;
      while ((j & 1)!=0) {id++;j>>=1;}
      return id+(i*32);
    }
    return _ids.Size();
  }

bool DlgManageScripts::SelectNewScript(Pathname &scr,AutoArray<Pathname> *multiple)
{
  CString filter;filter.LoadString(IDS_VISITORSCRIPT);
  CString folder;
  int len=strlen(scr.GetDirectoryWithDrive())+1;
  scr.GetDirectoryWithDriveWLBS(folder.GetBuffer(len),len);
  folder.ReleaseBuffer();
  CString fname=scr.GetFilename();

  CFileDialog fdlg(TRUE,"vis",fname,OFN_HIDEREADONLY|OFN_FILEMUSTEXIST|(multiple?OFN_ALLOWMULTISELECT:0),filter);
  fdlg.m_pOFN->lpstrInitialDir=folder;
  SRef<char>buffer=new char[65536];
  fdlg.m_pOFN->lpstrFile=buffer;
  fdlg.m_pOFN->nMaxFile=65535;
  strcpy(buffer,fname);
  if (fdlg.DoModal()==IDOK)
  {
    if (multiple)
    {
      POSITION pos=fdlg.GetStartPosition();
      while (pos) multiple->Append()=Pathname(fdlg.GetNextPathName(pos));
      scr=multiple->operator [](0);
    }
    else
      scr=fdlg.GetPathName();
    return true;
  }
  return false;
}
void DlgManageScripts::OnBnClickedNewgroup()
{
  CString name((LPCTSTR)IDS_NEWSCRIPTGROUP);
  HTREEITEM itm=wTree.GetSelectedItem();
  int data=itm==0?-1:wTree.GetItemData(itm);
  HTREEITEM res=wTree.InsertItem(name,2,2,data==-1?itm:wTree.GetParentItem(itm));
  wTree.SetItemData(res,-1);
  wTree.EnsureVisible(res);
  wTree.EditLabel(res);
}

BOOL DlgManageScripts::OnInitDialog()
{
  CDialog::OnInitDialog();

  wTree.SetImageList(&_ilist,TVSIL_NORMAL);
  MenuToTree();

  wPathname.EnableWindow(FALSE);

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

TypeIsSimpleZeroed(const char *);
void DlgManageScripts::OnOK()
{
  if (wTree.GetEditControl())
  {
    wPathname.SetFocus();
    return;
  }
  if (GetFocus()==&wTree)
  {
    wTree.EditLabel(wTree.GetSelectedItem());
    return;
  }
  if (GetFocus()==&wPathname)
  {
    wTree.SetFocus();
    return;
  }
  _menu.Clear();
  HTREEITEM tr=wTree.GetRootItem();
  tr=wTree.GetChildItem(tr);
  TreeToSubMenu(tr);
  CWaitCursor wcurs;

  if (_scc)
  {
    CString list;
    AutoArray<const char *, MemAllocStack<const char *, 32> > checkedOut;
    for (int i=0;i<_menu.Size();i++)
      if (_menu[i].pathname.GetLength() && _scc->UnderSSControl(_menu[i].pathname) && _scc->CheckedOut(_menu[i].pathname))
      {
        checkedOut.Append(_menu[i].pathname);
        list=list+_menu[i].pathname+"\r\n";
      }
    CString msg;
    AfxFormatString1(msg,IDS_SOMESCRIPTSARECHECKEDOUT,list);

    if (checkedOut.Size() && AfxMessageBox(msg,MB_YESNO|MB_ICONQUESTION)==IDYES)
    {
      wcurs.Restore();
      _scc->CheckIn(checkedOut.Data(),checkedOut.Size());
    }
  }

  CDialog::OnOK();
}

void DlgManageScripts::OnBnClickedDelete()
{
  HTREEITEM itm=wTree.GetSelectedItem();
  if (itm) wTree.DeleteItem(itm);
  
}

void DlgManageScripts::OnTvnSelchangedTree(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
  HTREEITEM itm=pNMTreeView->itemNew.hItem;
  int data=pNMTreeView->itemNew.lParam;
  if (data>=0)
  {
    wPathname.SetWindowText(_files[data]);
    wPathname.SetSel(_files[data].GetLength(),_files[data].GetLength());
    _lastPath=_files[data];    
  }
  wPathname.EnableWindow(data>=0);
  GetDlgItem(IDC_BROWSE)->EnableWindow(data>=0);
  GetDlgItem(IDC_EDIT)->EnableWindow(data>=0);
  GetDlgItem(IDC_ABOUT)->EnableWindow(data>=0);
  GetDlgItem(IDC_DELETE)->EnableWindow(itm!=wTree.GetRootItem());
  *pResult = 0;
}

void DlgManageScripts::OnTvnEndlabeleditTree(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMTVDISPINFO pTVDispInfo = reinterpret_cast<LPNMTVDISPINFO>(pNMHDR);
  if (pTVDispInfo->item.pszText && pTVDispInfo->item.pszText[0]==0) 
  {
    *pResult =0;
  }
  else
  {
    *pResult = 1;
  }
}

#include <io.h>
#include <afxwin.h>

void DlgManageScripts::OnEnKillfocusEdit1()
{
  HTREEITEM itm=wTree.GetSelectedItem();
  int data=itm==0?-1:wTree.GetItemData(itm);
  if (data==-1) return;
  CString text;wPathname.GetWindowText(text);
  if (_access(text,0)!=0)
    if (AfxMessageBox(IDS_SCRIPTNOTFOUNDWARNING,MB_OKCANCEL|MB_ICONQUESTION)==IDCANCEL)
      return;
  wTree.SetItemData(itm,AddFile(text,_ids[wTree.GetItemData(itm)]));

}

void DlgManageScripts::OnBnClickedAbout()
{
  HTREEITEM itm=wTree.GetSelectedItem();
  int data=itm==0?-1:wTree.GetItemData(itm);
  if (data==-1) return;
  ifstream infile(_files[data],ios::in|ios::binary);
  if (!infile) return;
  ws(infile);
  int p=infile.get();
  int q=infile.get();
  if (p!='/' || q!='*') 
  {
    AfxMessageBox(IDS_NOSCRIPTDESCRIPTION);
  }
  else
  {
    CString text;
    char buff[256];
    do
    {
      infile.get(buff,256,'*');
      text+=buff;
      infile.clear();
      p=infile.get();
      if (p==EOF) break;
      while (infile.peek()=='*') 
        {
          text+=(char)p;    
          p=infile.get();
        }
      if (infile.peek()=='/') break;
      text+=(char)p;    
    }    
    while (true);
    MessageBox(text,_files[data],MB_ICONINFORMATION);
  }
}// C:\Msdev\BIS\Visitor\PoseidonEditor\DlgManageScripts.cpp : implementation file
//


class DlgManageScriptOpen : public CDialog
{
	DECLARE_DYNAMIC(DlgManageScriptOpen)

public:
	DlgManageScriptOpen(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgManageScriptOpen();

// Dialog Data
	enum { IDD = IDD_MANAGESCRIPTSEDIT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CString vEditedFile;
  CString vEditor;
  BOOL vCheckOut;
  BOOL vBrowse;
  BOOL vEnableCheckOut;
  afx_msg void OnBnClickedChange();
  virtual BOOL OnInitDialog();
  CButton wCheckOut;
};


// DlgManageScriptOpen dialog

IMPLEMENT_DYNAMIC(DlgManageScriptOpen, CDialog)
DlgManageScriptOpen::DlgManageScriptOpen(CWnd* pParent /*=NULL*/)
	: CDialog(DlgManageScriptOpen::IDD, pParent)
    , vEditedFile(_T(""))
    , vEditor(_T(""))
    , vCheckOut(FALSE)
    , vBrowse(FALSE)
    , vEnableCheckOut(FALSE)
{
}

DlgManageScriptOpen::~DlgManageScriptOpen()
{
}

void DlgManageScriptOpen::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_EDITEDFILE, vEditedFile);
  DDX_Text(pDX, IDC_EDITOR, vEditor);
  DDX_Check(pDX, IDC_CHECKOUT, vCheckOut);
  DDX_Check(pDX, IDC_BROWSE, vBrowse);
  DDX_Control(pDX, IDC_CHECKOUT, wCheckOut);
}


BEGIN_MESSAGE_MAP(DlgManageScriptOpen, CDialog)
  ON_BN_CLICKED(IDC_CHANGE, OnBnClickedChange)
END_MESSAGE_MAP()

TypeIsMovable(Pathname);

// DlgManageScriptOpen message handlers

typedef HRESULT (WINAPI *type_MIMEAssociationDialog)(HWND hwndParent,  DWORD dwInFlags,    LPCSTR pcszFile,
    LPCSTR pcszMIMEContentType,    LPSTR pszAppBuf,    UINT ucAppBufLen);

void DlgManageScriptOpen::OnBnClickedChange()
{
  CString buff;
  HMODULE mod=LoadLibrary("url.dll");
  if (mod)
  {
    type_MIMEAssociationDialog mimeassoc=reinterpret_cast<type_MIMEAssociationDialog>(GetProcAddress(mod,"MIMEAssociationDialogA"));
    if (mimeassoc)
    {
      HRESULT res=mimeassoc(*this,0,vEditedFile,CString("application/")+(Pathname::GetExtensionFromPath(vEditedFile)+1),buff.GetBuffer(MAX_PATH),MAX_PATH);
      buff.ReleaseBuffer();
      if (res==S_OK || res==S_FALSE)
      {
        vEditor=buff;
        SetDlgItemText(IDC_EDITOR,vEditor);
      }
    }
    FreeLibrary(mod);
  }
}

void DlgManageScripts::OnBnClickedEdit()
{
  DlgManageScriptOpen dlg;
  dlg.vEditor=theApp.GetProfileString("ScriptMenu","Editor","");
  dlg.vCheckOut=theApp.GetProfileInt("ScriptMenu","CheckOut",1)!=0;
  dlg.vBrowse=theApp.GetProfileInt("ScriptMenu","Browse",0)!=0;
  wPathname.GetWindowText(dlg.vEditedFile);
  dlg.vEnableCheckOut=_scc && _scc->UnderSSControl(dlg.vEditedFile);
  if (dlg.DoModal()==IDOK)
  {
    Pathname edited=dlg.vEditedFile;
    if (_scc && dlg.vCheckOut) _scc->CheckOut(edited);
    ShellExecute(*this,NULL,dlg.vEditor,edited,edited.GetDirectoryWithDriveWLBS(),SW_NORMAL);
    if (dlg.vBrowse)
    {
      ShellExecute(*this,NULL,"explorer.exe", CString("/select,\"")+edited+"\"",0,SW_NORMAL);
    }
  theApp.WriteProfileString("ScriptMenu","Editor",dlg.vEditor);
  theApp.WriteProfileInt("ScriptMenu","CheckOut",dlg.vCheckOut?1:0);
  theApp.WriteProfileInt("ScriptMenu","Browse",dlg.vBrowse?1:0);
  }
}

void DlgManageScripts::OnBnClickedBrowse()
{
  if (SelectNewScript(_lastPath,0))
  {
    wPathname.SetWindowText(_lastPath);
    wPathname.SetSel((int)strlen(_lastPath),(int)strlen(_lastPath));
    wPathname.SetFocus();
  }
}

static void ScreenToWindow(CWnd *wnd, CPoint &pt)
{
  CRect rc;
  wnd->GetWindowRect(&rc);
  pt.x-=rc.left;
  pt.y-=rc.top;
}

void DlgManageScripts::OnTvnBegindragTree(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
  CPoint pos(GetMessagePos());
  ScreenToWindow(this,pos);

  _dragItem=pNMTreeView->itemNew.hItem;
  _dragItemIcon=wTree.CreateDragImage(_dragItem);
  _dragItemIcon->BeginDrag(0,CPoint(24,8));
  SetCapture();
  *pResult = 0;
}

void DlgManageScripts::OnMouseMove(UINT nFlags, CPoint point)
{
  // TODO: Add your message handler code here and/or call default
  if (_dragItem)
  {
    _dragItemIcon->DragLeave(this);
    CPoint ls=point;
    ClientToScreen(&ls);
    ls.y-=8;
    wTree.ScreenToClient(&ls);
    UINT flags=0;
    HTREEITEM hit=wTree.HitTest(ls,&flags);
    wTree.SetInsertMark(hit, TRUE);
    wTree.UpdateWindow();
    CPoint pos(GetMessagePos());
    ScreenToWindow(this,pos);
    _dragItemIcon->DragEnter(this,pos);
  }

  CDialog::OnMouseMove(nFlags, point);
}

void DlgManageScripts::MoveItemToItemNoTest(HTREEITEM what, HTREEITEM where,bool last)
{
  HTREEITEM pass=where;
  if (where==0) return;
  if (wTree.GetItemData(where)!=-1 || (wTree.GetItemState(where,TVIS_EXPANDED)& TVIS_EXPANDED)==0 && wTree.ItemHasChildren(where)) where=wTree.GetParentItem(where);  
  else pass=last?TVI_LAST:TVI_FIRST;
  int image;
  wTree.GetItemImage(what,image,image);
  CString text=wTree.GetItemText(what);
  DWORD_PTR data=wTree.GetItemData(what);
  HTREEITEM newitm=wTree.InsertItem(text,image,image,where,pass);
  wTree.SetItemData(newitm,data);
  HTREEITEM child=wTree.GetChildItem(what);
  while (child)
  { 
    MoveItemToItemNoTest(child,newitm,true);
    child=wTree.GetChildItem(what);
  }
  wTree.DeleteItem(what);
  wTree.SelectItem(newitm);
}

void DlgManageScripts::MoveItemToItem(HTREEITEM what, HTREEITEM where)
{
  HTREEITEM test=where;
  while (test!=what && test)
    test=wTree.GetParentItem(test);
  if (test==what) return;
  MoveItemToItemNoTest(what,where);
}

void DlgManageScripts::OnLButtonUp(UINT nFlags, CPoint point)
{
  if (_dragItem)
  {
    _dragItemIcon->DragLeave(this);
    CPoint ls=point;
    ls.y-=8;
    ClientToScreen(&ls);
    wTree.ScreenToClient(&ls);
    UINT flags=0;
    HTREEITEM hit=wTree.HitTest(ls,&flags);
    if (hit && (GetKeyState(VK_CONTROL) & 0x80)!=0)
      hit=wTree.GetParentItem(hit);
    MoveItemToItem(_dragItem,hit);
    ReleaseCapture();
    ShowCursor(true);
    wTree.SetInsertMark(NULL);
    _dragItem=NULL;
    _dragItemIcon->EndDrag();
    delete _dragItemIcon;
  }

  CDialog::OnLButtonUp(nFlags, point);
}

void DlgManageScripts::OnBnClickedSeparator()
{
  HTREEITEM itm=wTree.GetSelectedItem();
  int data=itm==0?-1:wTree.GetItemData(itm);
  if (itm==0) itm=TVI_ROOT;
  else if (data!=-1) itm=wTree.GetParentItem(itm);
  HTREEITEM res=wTree.InsertItem("----",1,1,itm,TVI_LAST);
  wTree.SetItemData(res,AddFile("",GuessID()));
  wTree.EnsureVisible(res);
}

BOOL DlgManageScriptOpen::OnInitDialog()
{
  CDialog::OnInitDialog();
  
  wCheckOut.EnableWindow(vEnableCheckOut);  
 
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

int DlgManageScripts::GuessIDExt()
{  
  for (int i=0;i<_menu.Size();i++) if (_menu[i].uid>=0) _ids.Append()=_menu[i].uid;
  return GuessID();
}