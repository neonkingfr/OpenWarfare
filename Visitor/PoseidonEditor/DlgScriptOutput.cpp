#include "stdafx.h"
#include "resource.h"

#include "DlgScriptOutput.hpp"
#include ".\dlgscriptoutput.hpp"
#include "PosMainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BEGIN_MESSAGE_MAP(CScriptOutputDlg, CDialogBar)
  //{{AFX_MSG_MAP(CScriptOutputDlg)
  // NOTE: the ClassWizard will add message map macros here
  //}}AFX_MSG_MAP
  ON_BN_CLICKED(IDC_SCRIPT_BROWSE, OnBnClickedScriptBrowse)
  ON_BN_CLICKED(IDC_SCRIPT_RUN, OnBnClickedScriptRun)
END_MESSAGE_MAP()

void CScriptOutputDlg::DoDataExchange(CDataExchange* pDX)
{
  DDX_Text(pDX, IDC_SCRIPT_OUTPUT_EDIT, _text);
  DDX_Text(pDX, IDC_SCRIPT_EDIT, _scriptPath);

  if (!pDX->m_bSaveAndValidate)
  {  
    CEdit * edit = (CEdit *) GetDlgItem(IDC_SCRIPT_OUTPUT_EDIT);
    edit->LineScroll(INT_MAX);
  }
}

void CScriptOutputDlg::OnBnClickedScriptBrowse()
{
  // Open file   
  CString sDir = GetPosEdEnvironment()->m_optSystem.m_sScriptPath;
  MakeShortDirStr(sDir,sDir);
  

  // p��prava CFileDialog
  CFileDialog dlg(true);

  dlg.m_ofn.Flags |= OFN_FILEMUSTEXIST;

  CString sFilter = _T("Script Files(*.vis)");
  sFilter += (TCHAR)(_T('\0'));
  sFilter += _T("*.");
  sFilter += _T("vis");
  sFilter += (TCHAR)(_T('\0'));
  sFilter += _T("All files (*.*)");
  sFilter += (TCHAR)(_T('\0'));
  sFilter += _T("*.*");
  sFilter += (TCHAR)(_T('\0'));
  sFilter += (TCHAR)(_T('\0'));


  dlg.m_ofn.nMaxCustFilter = 1;
  dlg.m_ofn.lpstrFilter = sFilter;
  dlg.m_ofn.lpstrDefExt = _T("vis");
  //dlg.m_ofn.lpstrTitle  = sTitle;  
  //dlg.m_ofn.lpstrFile = sFile.GetBuffer(_MAX_PATH);
  dlg.m_ofn.lpstrInitialDir = sDir;


  BOOL bRes = (dlg.DoModal()==IDOK)?TRUE:FALSE;
  //sFile.ReleaseBuffer();
  //sFile.MakeLower();
  if (bRes)
  {    
    _scriptPath = dlg.GetPathName();
    SeparateDirFromFileName(sDir, (LPCTSTR) _scriptPath);
    GetPosEdEnvironment()->m_optSystem.m_sScriptPath = sDir;
    GetPosEdEnvironment()->m_optSystem.SaveParams();

    _scriptPath = dlg.GetPathName();
    UpdateData(FALSE);
  }
}

void CScriptOutputDlg::OnBnClickedScriptRun()
{
  _mainFrame->OnScript(_scriptPath);
}

void CScriptOutputDlg::AddText(const CString& val) 
{
  CString goodVal(val); 
  goodVal.Replace("\n","\r\n"); 
  _text += goodVal; 
  if (_text.GetLength() > 1 << 12)
  {
    // cut a part of text 
    int resLength = _text.GetLength() / 2;
    CString newText = _text.Right(resLength);
    _text = newText;
  }
  UpdateData(FALSE);  
};

CSize CScriptOutputDlg::CalcDynamicDim(int nLength, DWORD dwMode)
{
  // Return default if it is being docked or floated
  if ((dwMode & LM_VERTDOCK) || (dwMode & LM_HORZDOCK))
  {
    if (dwMode & LM_STRETCH) // if not docked stretch to fit
      return CSize((dwMode & LM_HORZ) ? 32767 : m_sizeDocked.cx,
      (dwMode & LM_HORZ) ? m_sizeDocked.cy : 32767);
    else
      return m_sizeDocked;
  }
  if (dwMode & LM_MRUWIDTH)
    return m_sizeFloating;
  // In all other cases, accept the dynamic length
  if (dwMode & LM_LENGTHY)
    return CSize(m_sizeFloating.cx, /*(m_bChangeDockedSize) ?*/
    m_sizeFloating.cy = m_sizeDocked.cy = nLength /*:
                                                  m_sizeFloating.cy = nLength*/);
  else
  return CSize(/*(m_bChangeDockedSize) ?*/
  m_sizeFloating.cx = m_sizeDocked.cx = nLength /*:
                                                m_sizeFloating.cx = nLength*/, m_sizeFloating.cy);
}

#define MIN_SIZE_X 200
#define MIN_SIZE_Y 300 

CSize CScriptOutputDlg::CalcDynamicLayout(int nLength, DWORD dwMode)
{
  CSize size = CalcDynamicDim(nLength, dwMode);
  if (size.cx < MIN_SIZE_X)
    size.cx = MIN_SIZE_X;

  if (size.cy < MIN_SIZE_Y)
    size.cy = MIN_SIZE_Y;

  RecalcChildWindowPos(size);
  return size;
}

#define SPACE_X 7
#define SPACE_Y 5
#define START_AT 11

void CScriptOutputDlg::RecalcChildWindowPos(const CSize& size)
{
  // staic
  CWnd * wnd = GetDlgItem(IDC_STATIC);
  CRect rect;
  wnd->GetWindowRect(rect);
  ScreenToClient(rect);  

  int staticHeight = rect.Height();

  // Run button 
  wnd = GetDlgItem(IDC_SCRIPT_RUN);
  wnd->GetWindowRect(rect);
  ScreenToClient(rect);  

  int runHeight = rect.Height();
  int runWidth = rect.Width();

  rect.bottom = size.cy - SPACE_Y;
  rect.right = size.cx - SPACE_X;
  rect.top = rect.bottom - runHeight;
  rect.left = rect.right - runWidth;

  wnd->MoveWindow(rect,TRUE);

  // browse button 
  wnd = GetDlgItem(IDC_SCRIPT_BROWSE);
  wnd->GetWindowRect(rect);
  ScreenToClient(rect);  

 
  int browseWidth = rect.Width();

  rect.bottom = size.cy - SPACE_Y;
  rect.right = size.cx - SPACE_X * 2 - runWidth;
  rect.top = rect.bottom - runHeight;
  rect.left = rect.right - browseWidth;

  wnd->MoveWindow(rect,TRUE);

  // Edit box
  wnd = GetDlgItem(IDC_SCRIPT_EDIT);
  
  rect.bottom = size.cy - SPACE_Y;
  rect.right = size.cx - SPACE_X * 2 - runWidth - browseWidth;
  rect.top = rect.bottom - runHeight;
  rect.left = SPACE_X;

  wnd->MoveWindow(rect,TRUE);

  // big output window
  wnd = GetDlgItem(IDC_SCRIPT_OUTPUT_EDIT);

  rect.top = SPACE_Y * 3 + staticHeight;
  rect.left = SPACE_X;
  rect.right = size.cx - SPACE_X;
  rect.bottom = size.cy - (SPACE_Y * 2 + runHeight);  

  wnd->MoveWindow(rect,TRUE);
}
BOOL CScriptOutputDlg::Create(CWnd* pParentWnd, UINT nIDTemplate, UINT nStyle, UINT nID)
{
  BOOL ret =  CDialogBar::Create(pParentWnd, nIDTemplate, nStyle, nID);
  m_sizeFloating = m_sizeDocked = GetPosEdEnvironment()->m_appDockState.m_scriptPanelSize;
  return ret;
}
