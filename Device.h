#pragma once

#include <d3dx9.h>
#include <windows.h>

namespace Framework
{
    class Device
    {
    private:
        UINT _adapter;
        IDirect3D9 *_p3d;
        IDirect3DDevice9 *_device;
        D3DDISPLAYMODE *_pDisMode;
        D3DPRESENT_PARAMETERS *_pParams;

        bool _deviceLost;

    public:
        Device(bool windowed = true);
        Device(UINT16 width, UINT16 height, bool windowed = true, D3DFORMAT backBufferFormat = D3DFMT_X8R8G8B8);
        ~Device(void);

    public:
        bool CreateDevice(HWND hWnd);
        bool TryReset(void);

    public:
        bool GetDeviceLost(void) const { return _deviceLost; }
        void SetDeviceLost(bool value = true) { _deviceLost = value; }
        LPDIRECT3DDEVICE9 GetDevice(void) const { return _device; }

    private:
        Device(const Device &device);
        Device operator =(const Device &device);

    private:
        D3DDEVTYPE SelectDeviceType(void);
        DWORD SelectBehaviorFlags(D3DDEVTYPE devType);
        D3DFORMAT SelectDepthFormat(D3DDEVTYPE devType, D3DFORMAT adapterFmt, D3DFORMAT renderFmt);
    };
}