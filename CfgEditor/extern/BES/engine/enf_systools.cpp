//-----------------------------------------------------------------------------
// File: enf_systools.cpp
//
// Desc: Implementation of application stack and assert
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#include "precomp.h"
#include "enf_config.h"
#include "enf_main.h"
#include "common/enf_types.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#ifndef ENF_XBOX
	#include <windows.h>
	#include "Dbghelp.h"
#endif

#ifdef NDEBUG
#undef NDEBUG
#endif  /* NDEBUG */

struct AppStackEntry
{
	const char*	Function;
	const char*	File;
	uint			Line;
	void*			Pad;
};

const uint MAX_APP_STACK = 256;

AppStackEntry	AppStack[MAX_APP_STACK];
uint				AppStackPos = 0;

PUBLIC_API void _Assert(const char *, const char *, unsigned);

#ifdef _DEBUG
_StackTag::_StackTag(const char* fname, const char* file, int line)
{
	enf_assert2(AppStackPos < MAX_APP_STACK, "Application stack overflow");
	AppStack[AppStackPos].Function = fname;
	AppStack[AppStackPos].File = file;
	AppStack[AppStackPos].Line = line;
	AppStack[AppStackPos].Pad = NULL;
	AppStackPos++;
}

_StackTag::~_StackTag()
{
	enf_assert2(AppStackPos > 0, "Application stack underflow");
	AppStackPos--;
}

#else
_StackTag::_StackTag(const char* fname)
{
	enf_assert2(AppStackPos < MAX_APP_STACK, "Application stack overflow");
	AppStack[AppStackPos].Function = fname;
	AppStack[AppStackPos].File = NULL;
	AppStack[AppStackPos].Line = NULL;
	AppStack[AppStackPos].Pad = NULL;
	AppStackPos++;
}

_StackTag::~_StackTag()
{
	enf_assert2(AppStackPos > 0, "Application stack underflow");
	AppStackPos--;
}

#endif

#ifdef ENF_DBGHELP

#define MAX_LINES 64

//=============================================================================
bool PrintStack(uint ebp, uint eip, char *text, unsigned buffLen)
{
	unsigned textOffs;
	int ret = 0;
	STACKFRAME stackFrame;
	DWORD displ;
	IMAGEHLP_SYMBOL *symbol;
	IMAGEHLP_LINE line;
	memset(&line, 0, sizeof(IMAGEHLP_LINE));
	line.SizeOfStruct = sizeof(IMAGEHLP_LINE);
	memset(&stackFrame, 0, sizeof(STACKFRAME));
	symbol = (IMAGEHLP_SYMBOL *)malloc(sizeof(IMAGEHLP_SYMBOL)+1024);
	memset(symbol, 0, sizeof(IMAGEHLP_SYMBOL)+1024);
	symbol->SizeOfStruct = sizeof(IMAGEHLP_SYMBOL);
	symbol->MaxNameLength = 1024;
	stackFrame.AddrPC.Mode = AddrModeFlat;
	stackFrame.AddrFrame.Mode = AddrModeFlat;
	stackFrame.AddrFrame.Offset = ebp;
	stackFrame.AddrPC.Offset = eip;

	char buff[MAX_PATH+1];
	GetModuleFileNameA(NULL, buff, MAX_PATH);
	char *konec = strrchr(buff, '\\');
	if (konec == NULL)
	{
		buff[0] = '.';
		buff[1] = 0;
	}
	else
		konec[0] = 0;

	unsigned lines = 0;
	char msg[2048] = "\0";

	sprintf(msg, "SymInit search path: %s\n", buff);
	//	instance->AssertLogFunc(assertType, msg, instance->console);
	textOffs = strlen(msg);
	if (textOffs < buffLen)
		strcpy(text, msg);

	ret = SymInitialize(GetCurrentProcess(), buff, TRUE);
	if (!ret)
	{
		sprintf(msg, "SymInitialize:%i addr: 0x%08x\n", GetLastError(), eip);
		//		instance->AssertLogFunc(assertType, msg, instance->console);
		textOffs += strlen(msg);
		if (textOffs < buffLen)
			strcat(text, msg);
		free(symbol);
		return false;
	}

	for (;;)
	{
		ret = StackWalk(IMAGE_FILE_MACHINE_I386, GetCurrentProcess(), GetCurrentThread(), &stackFrame, NULL, NULL, NULL, NULL, NULL);
		if (!ret)
		{
			if (lines >= MAX_LINES && textOffs < buffLen)
			{
				sprintf(msg, "... not shown %i lines\n", lines - MAX_LINES);
				textOffs += strlen(msg);
				if (textOffs < buffLen)
					strcat(text, msg);
			}
			//sprintf(msg, "StackWalk:%i\n", GetLastError());
			//			instance->AssertLogFunc(assertType, msg, instance->console);
			textOffs += strlen(msg);
			if (textOffs < buffLen)
				strcat(text, msg);
			free(symbol);
			break;
		}

		if (lines > 1000)
		{
			//instance->AssertLogFunc(assertType, "huh, stackDepth vic jak 1000? to asi cyklime\n", instance->console);
			free(symbol);
			break;
		}

		ret = SymGetSymFromAddr(GetCurrentProcess(), stackFrame.AddrPC.Offset, &displ, symbol);
		if (!ret)
		{
			sprintf(msg, "SymGetSymFromAddr:%i, addr:0x%08x\n", GetLastError(), stackFrame.AddrPC.Offset);
			//			instance->AssertLogFunc(assertType, msg, instance->console);
			textOffs += strlen(msg);
			if (textOffs < buffLen && lines < MAX_LINES)
				strcat(text, msg);
			++lines;
		}
		else
		{
			if (SymGetLineFromAddr(GetCurrentProcess(), stackFrame.AddrPC.Offset, &displ, &line))
			{
				sprintf(msg, "[%s]: %s(%i) addr:0x%08x\n", symbol->Name, line.FileName, line.LineNumber, stackFrame.AddrPC.Offset);
				//				instance->AssertLogFunc(assertType, msg, instance->console);
				textOffs += strlen(msg);
				if (textOffs < buffLen && lines < MAX_LINES)
					strcat(text, msg);
				++lines;
			}
			else
			{
				sprintf(msg, "[%s]: ??? addr:0x%08x\n", symbol->Name, stackFrame.AddrPC.Offset);
				//				instance->AssertLogFunc(assertType, msg, instance->console);
				textOffs += strlen(msg);
				if (textOffs < buffLen && lines < MAX_LINES)
					strcat(text, msg);
				++lines;
			}
		}
	}
	return (lines != 0);
}
#endif

// assertion format string for use with output to stderr
static char _assertstring[] = "Assertion failed: %s, file %s, line %d\n";

//assertion string components for message box

#define BOXINTRO    "Assertion failed!"
#define PROGINTRO   "Program: "
#define FILEINTRO   "File: "
#define LINEINTRO   "Line: "
#define EXPRINTRO   "Reason: "
#define HELPINTRO   "\n(Press Retry to debug the application - JIT must be enabled)"

static char * dotdotdot = "...";
static char * newline = "\n";
static char * dblnewline = "\n\n";

#define DOTDOTDOTSZ 3
#define NEWLINESZ   1
#define DBLNEWLINESZ   2

#define MAXLINELEN  60 /* max length for line in message box */
#define ASSERTBUFSZ (MAXLINELEN * 32) /* 9 lines in message box */

#if defined (_M_IX86)
#define _DbgBreak() __asm { int 3 }
#elif defined (_M_ALPHA)
void _BPT();
#pragma intrinsic(_BPT)
#define _DbgBreak() _BPT()
#elif defined (_M_IA64)
void __break(int);
#pragma intrinsic (__break)
#define _DbgBreak() __break(0x80016)
#else  /* defined (_M_IA64) */
#define _DbgBreak() DebugBreak()
#endif  /* defined (_M_IA64) */

PUBLIC_API ErrorResponse _ShowError(const char* title, const char *error, const char *filename, unsigned lineno)
{
	int nCode;
	char *pch;
	char assertbuf[ASSERTBUFSZ];
	char progname[MAX_PATH + 1];

	::ShowCursor(true);

	// Line 1: box intro line
	enf::strcpy( assertbuf, BOXINTRO );
	enf::strcat( assertbuf, dblnewline );

	// Line 2: program line
	enf::strcat( assertbuf, PROGINTRO );

	progname[MAX_PATH] = '\0';
	if ( !GetModuleFileName( NULL, progname, MAX_PATH ))
		strcpy( progname, "<program name unknown>");

	pch = (char *)progname;

	// sizeof(PROGINTRO) includes the NULL terminator
	if ( sizeof(PROGINTRO) + strlen(progname) + NEWLINESZ > MAXLINELEN )
	{
		pch += (sizeof(PROGINTRO) + strlen(progname) + NEWLINESZ) - MAXLINELEN;
		strncpy( pch, dotdotdot, DOTDOTDOTSZ );
	}

	enf::strcat( assertbuf, pch );
	enf::strcat( assertbuf, newline );

	// Line 3: file line
	enf::strcat( assertbuf, FILEINTRO );

	// plenty of room on the line, just append the filename */
	enf::strcat( assertbuf, filename );

	enf::strcat( assertbuf, newline );

	// Line 4: line line
	enf::strcat( assertbuf, LINEINTRO );
	_itoa( lineno, assertbuf + strlen(assertbuf), 10 );
	enf::strcat( assertbuf, dblnewline );

	// Line 5: message line
	enf::strcat( assertbuf, EXPRINTRO );

	// sizeof(HELPINTRO) includes the NULL terminator

	if (enf::strlen(assertbuf) +
		enf::strlen(error) +
		2*DBLNEWLINESZ +
		sizeof(HELPINTRO) > ASSERTBUFSZ )
	{
		strncat( assertbuf, error,
			ASSERTBUFSZ -
			(strlen(assertbuf) +
			DOTDOTDOTSZ +
			2*DBLNEWLINESZ +
			sizeof(HELPINTRO)) );
		enf::strcat(assertbuf, dotdotdot );
	}
	else
		enf::strcat(assertbuf, error );

	enf::strcat(assertbuf, dblnewline );

	int currTextLen = enf::strlen(assertbuf);
#ifdef ENF_DBGHELP
	DWORD _ebp, _eip;
	__asm {
		push eax
			mov _ebp, ebp
			call dummy
dummy:
		pop eax
			mov _eip, eax
			pop eax
	}
	if(!PrintStack(_ebp, _eip, assertbuf+currTextLen, ASSERTBUFSZ-currTextLen))
#endif
	{
		if((currTextLen + 20) < ASSERTBUFSZ)
			enf::strcat(assertbuf, "Custom stack trace:\n");

		for(int n = AppStackPos - 1; n >= 0; n--)
		{
			char buff[256];

			sprintf(buff, "[%s] : %s (%d)\n",
				AppStack[n].Function,
				AppStack[n].File,
				AppStack[n].Line);

			currTextLen = enf::strlen(assertbuf);

			if((currTextLen + enf::strlen(buff) + 1) >= ASSERTBUFSZ)
				break;

			enf::strcat(assertbuf, buff);
		}
	}

	currTextLen = enf::strlen(assertbuf);

	if((currTextLen + enf::strlen(HELPINTRO) + 1) < ASSERTBUFSZ)
	{
		// Line 8: help line
		enf::strcat(assertbuf, HELPINTRO);
	}

	assertbuf[1024] = 0;
	nCode = ::MessageBox(NULL, assertbuf,
		title,
		MB_ABORTRETRYIGNORE|MB_ICONHAND|MB_SETFOREGROUND|MB_TASKMODAL);

	if(nCode == IDABORT)
		return ER_ABORT;
	if(nCode == IDRETRY)
		return ER_RETRY;
	if(nCode == IDIGNORE)
		return ER_IGNORE;

	return ER_ABORT;
}

PUBLIC_API void _Assert(const char *expr, const char *filename, unsigned lineno)
{
	ErrorResponse er = _ShowError("Enforce engine", expr, filename, lineno);

	// Abort: abort the program
	if (er == ER_ABORT)
	{
		// raise abort signal
		raise(SIGABRT);

		// We usually won't get here, but it's possible that
		//	SIGABRT was ignored.  So exit the program anyway.
		_exit(3);
	}

	// Retry: call the debugger
	if (er == ER_RETRY)
	{
		_DbgBreak();
		// return to user code
		return;
	}

	// Ignore: continue execution
	if (er == ER_IGNORE)
		return;

	abort();
}

//-----------------------------------------------------------------------
//TODO: move it to something like enf_system.cpp
//-----------------------------------------------------------------------
void SetFPUPrecision(FPUPrecision prec)
{
	ushort pp = 0;

	switch(prec)
	{
	case FPU_PRECISION_MINIMUM:
		pp = 0x000;
		break;

	case FPU_PRECISION_MEDIUM:
		pp = 0x200;
		break;
	case FPU_PRECISION_MAXIMUM:
		pp = 0x300;
		break;
	}

	ushort sr;

	__asm {
		fstcw sr;
		mov	ax, word ptr[sr]
		and	ax, 0fcffh
		or		ax, word ptr[pp]
		mov	word ptr[sr], ax
		fldcw	sr;
	}
}

void _OutputDebugString(const char* str)
{
	OutputDebugString(str);
}

void* _LoadLibrary(const char* name)
{
	return LoadLibrary(name);
}

void* _GetProcAddress(void* module, const char* procname)
{
	return GetProcAddress((HMODULE)module, procname);
}

short _GetAsyncKeyState(int vkey)
{
	return GetAsyncKeyState(vkey);
}
