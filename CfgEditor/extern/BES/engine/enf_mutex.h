//-----------------------------------------------------------------------------
// File: enf_mutex.h
//
// Desc: Mutual exclusive access object
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#ifndef ENF_MUTEX_H
#define ENF_MUTEX_H

#include "enf_config.h"

//!Placeholder for win32's CRITICAL_SECTION
struct SCriticalSectionStandIn
{
	void* a;
	long b;
	long c;
	void* d;
	void* e;
	unsigned long f;
};

struct _RTL_CRITICAL_SECTION;
extern "C"
{
	__declspec(dllimport) void __stdcall InitializeCriticalSection(_RTL_CRITICAL_SECTION*);
	__declspec(dllimport) void __stdcall DeleteCriticalSection(_RTL_CRITICAL_SECTION*);
	__declspec(dllimport) void __stdcall EnterCriticalSection(_RTL_CRITICAL_SECTION*);
	__declspec(dllimport) void __stdcall LeaveCriticalSection(_RTL_CRITICAL_SECTION*);
}

/*!
Object for preventing to access some place of code
from two threads at the same time.
*/
class CMutex
{
	mutable SCriticalSectionStandIn m_CS;

	// prevent copying and assignment
	CMutex(const CMutex&);
	const CMutex& operator=(const CMutex&);

public:

	//!Constructs mutex object
	ENF_INLINE CMutex()
	{
		InitializeCriticalSection((_RTL_CRITICAL_SECTION*)&m_CS);
	}

	//!Deletes mutex object
	ENF_INLINE ~CMutex()
	{
		DeleteCriticalSection((_RTL_CRITICAL_SECTION*)&m_CS);
	}

	/*!
	Locks the mutex object. Any other thread,
	attempting to lock this mutex will be stopped until
	Unlock is called
	*/
	ENF_INLINE void Lock() const
	{
		EnterCriticalSection((_RTL_CRITICAL_SECTION*)&m_CS);
	}

	/*!
	Unlocks the mutex object.
	*/
	ENF_INLINE void Unlock() const
	{
		LeaveCriticalSection((_RTL_CRITICAL_SECTION*)&m_CS);
	}
};

/*!
Convenient way for locking CMutex, where CMutex::Lock()
is called from constructor and CMutex::Unlock() is called
from destructor, so you don't need to care about unlocking,
when leaving critical piece of code
*/
class CMutexLock
{
    const CMutex& m_ToLock;

    // prevent copying and assignment
    CMutexLock(const CMutexLock&);
    const CMutexLock& operator=(const CMutexLock&);

public:

    ENF_INLINE CMutexLock(const CMutex& toLock) :
     m_ToLock(toLock)
    {
        m_ToLock.Lock();
    }
    
	 ENF_INLINE ~CMutexLock()
    {
        m_ToLock.Unlock();
    }
};

#endif //ENF_MUTEX_H
