
struct AllocLocation
{
	const char* File;
	int			Line;
	size_t		Size;
	int			Count;
	const void*	Data;
};

//-----------------------------------------------------------------------------
/*
Reimplemented from ESet for maximum efficiency
*/
class AllocationSet
{
public:
	AllocationSet():
		size(0),
			count(0),
			data1(NULL)
	{
		Grow(1024);
	}


	~AllocationSet()
	{
		if (data1) MemoryManager::Free(data1);
	}

	ENF_INLINE const AllocLocation& operator[] (unsigned int i) const
	{
		enf_assert(i < count);
		return data1[i];
	}

	ENF_INLINE const AllocLocation* GetArray() const
	{
		return data1;
	}

	ENF_INLINE unsigned int GetCardinality() const
	{	//number of elements
		return count;
	}

	ENF_INLINE void Clear()
	{
		count = 0;
	}

	ENF_INLINE void Insert(const char* file, unsigned int line, size_t allocsize, const void* data)
	{
		int p = 0;
		int r = count - 1;
		int i;
		for(;r >= p;)
		{
			i = (p + r) / 2;

			int cmp = (enf::strcmp(data1[i].File, file) * 1000000 + (data1[i].Line - line));

			if(cmp > 0)
			{
				r = i - 1;
			}
			else
			{
				if(cmp == 0)
				{
					data1[i].Count++;
					data1[i].Size += allocsize;
					data1[i].Data = data;
					return;
				}
				p = i + 1;
			}
		}

		//move the elements [l count-1] to [l+1 count]
		if (size <= count) Grow(1 + size * 2);
		if (count - p > 0)
			memmove(&data1[p + 1], &data1[p], (count - p) * sizeof(AllocLocation));

		data1[p].Count = 1;
		data1[p].Size = allocsize;
		data1[p].File = file;
		data1[p].Line = line;
		data1[p].Data = data;
		count++;
		return;
	}

	void Dump();

private:
	AllocLocation *data1;
	unsigned int size;
	unsigned int count;

	void Grow(unsigned int nsize)
	{
		data1 = (AllocLocation*)MemoryManager::ReAlloc(data1, nsize * sizeof(AllocLocation));
		enf_assert(data1);
		this->size = nsize;
	}
};
