//-----------------------------------------------------------------------------
// File: enf_staticbuffer.h
//
// Desc: Static heap allocator, static string allocator.
//			For cases when there is never deallocation
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#ifndef ENF_STATICBUFFER_H
#define ENF_STATICBUFFER_H

#include "enf_config.h"
#include "common/enf_memory.h"

/*!
Class for static allocations. These are allocations which will
never be freed. It also holds unique strings in effective tree
for fast searching.
*/
class PUBLIC_API StaticBuffer
{
friend class ScriptStreamer;

public:
	ENF_DECLARE_MMOBJECT(StaticBuffer);

struct StrNode
{
	const char* m_strString;
	int			m_iWeight;
	StrNode*		m_psChildren[2];
};

private:
	StrNode*		StringRoot;
	char*			BeginData;
	const char*	EndData;
	char*			Buffer;
	unsigned int	Size;

public:
	ENF_INLINE unsigned int GetSize() const
	{
		return Size;
	}

	ENF_INLINE bool IsInitialized() const
	{
		return (Buffer != NULL && Size != 0);
	}

	/*!
	Tells whether the pointer points into allocated data
	\param ptr Pointer to be tested
	\return
	True when pointer points to memory allocated by this StaticBuffer
	*/
	ENF_INLINE bool IsAllocated(const void* ptr) const
	{
		return (((char *)ptr) >= Buffer && ((char *)ptr) < BeginData);
	}

	/*!
	Tells how much memory we hve allocated
	\return
	Number of allocated bytes
	*/
	ENF_INLINE int Allocated() const
	{
		return (int)(BeginData - Buffer);
	}

	const StrNode* GetRootNode() const
	{
		return StringRoot;
	}

	const void* GetData() const
	{
		return Buffer;
	}

	/*!
	Adds string to unique collection. It quickly recognize when
	string is already present.
	\param string
	Null terminated string to be inserted
	\return
	Either new allocated unique string, or just string pointer
	when string is already present.
	*/
	const char* AddString(const char* string);
	/*!
	Adds string to unique collection. It quickly recognize when
	string is already present.
	\param string
	Pointer to string to be inserted. It must not be null terminated
	\param len
	Length of data
	\return
	Either new allocated unique string, or just string pointer
	when string is already present.
	*/
	const char* AddString(const char* string, int len);

	/*!
	Finds unique string in collection
	\param string
	Null terminated string to be found
	\return
	Pointer to string if it's found. Null otherwise
	*/
	const char* FindString(const char* string);

	/*!
	Allocates raw memory and makes copy od supplied data
	\param len
	Length of buffer to be allocated
	\param data
	Pointer to data to be copied into new allocated buffer
	\return
	Pointer to new memory buffer
	*/
	void* AllocData(int len, const void *data);

	/*!
	Allocates raw memory
	\param len
	Length of buffer to be allocated
	\return
	Pointer to new memory buffer
	*/
	void* AllocData(int len);

	/*!
	Frees allocated data. Can be used just after AllocData and it deallocated
	memory from back of static memory buffer. Must be used carefully!
	\param len
	Number of bytes to be freed
	*/
	void FreeData(int len);

	bool Init(int size);
	void Free();

	~StaticBuffer();
	StaticBuffer();
};

//-----------------------------------------------------------------------------
/*!
StaticMem
*/
//-----------------------------------------------------------------------------
template <size_t size> class StaticMem
{
private:
	char		Buffer[size];
	size_t	Pos;

public:
	ENF_INLINE size_t GetAllocated() const
	{
		return Pos;
	}

	ENF_INLINE void Clear()
	{
		Pos = 0;
	}

	ENF_INLINE void* Alloc(size_t alloc, bool cleared = false)
	{
		enf_assert((Pos + alloc) < size);

		void* ptr = (void*)(Buffer + Pos);
		if(cleared)
		{
			ZeroMem(ptr, alloc);
		}
		Pos += alloc;
		return ptr;
	}

	ENF_INLINE const char* Alloc(const char* str)
	{
		size_t alloc = enf::strlen(str) + 1;
		char* ptr = (char*)Alloc(alloc);
		enf::strcpy(ptr, str);
		return ptr;
	}

	StaticMem()
	{
		Clear();
	}
};

#endif //ENF_STATICBUFFER_H