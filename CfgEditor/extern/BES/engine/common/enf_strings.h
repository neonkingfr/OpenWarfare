//-----------------------------------------------------------------------------
// File: enf_strings.h
//
// Desc: String types
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#ifndef ENF_STRINGS_H
#define ENF_STRINGS_H

#include <stdarg.h>
#include <ctype.h>
#include "enf_config.h"

PUBLIC_API void __vsprintf(char*, const char*, va_list argptr);

//because of collision with Max SDK in tools

namespace enf
{
typedef unsigned short UNICHAR;

PUBLIC_API int estrlen(const UNICHAR *str);
PUBLIC_API void estrcpy(UNICHAR *dest, const UNICHAR *src);
PUBLIC_API void estrcat(UNICHAR *dest, const UNICHAR *src);
PUBLIC_API void estrcnv(UNICHAR *dest, const char *src);

ENF_INLINEFUNC bool endchar(UNICHAR chr)
{
	return (chr == 0);
}

ENF_INLINEFUNC char u2c(UNICHAR chr)
{
	return chr;
}

ENF_INLINEFUNC UNICHAR c2u (char chr)
{
	return (UNICHAR)chr;
}

PUBLIC_API int strcmpi (const char * dst, const char * src);
PUBLIC_API int strcmp (const char * dst, const char * src);
PUBLIC_API size_t strlen (const char * src);
PUBLIC_API char* strcat (char * dst, const char * src);
PUBLIC_API char* strcpy(char * dst, const char * src);
PUBLIC_API char* strchr (const char * string, int ch);
PUBLIC_API char* strrchr (const char * string, int ch);

/*!
Simple wrapper over const char* strings
to avoid strcmp etc. usage.
*/
class PUBLIC_API CStr
{
private:
	const char* m_pCString;

public:
	CStr()
	{
		m_pCString = NULL;
	}

	CStr(const char* str)
	{
		m_pCString = str;
	}

	enf::CStr operator=(const enf::CStr& str)
	{
		m_pCString = str.m_pCString;
		return *this;
	}
	bool IsNull() const
	{
		return (m_pCString == NULL);
	}
	
	bool IsEmpty() const
	{
		return (m_pCString == NULL || m_pCString[0] == 0);
	}

	bool operator==(const char* str) const;
	bool operator!=(const char* str) const;
	bool operator==(const CStr& cstr) const;
	bool operator!=(const CStr& cstr) const;
	bool operator<(const CStr& cstr) const;
	bool operator>(const CStr& cstr) const;
	char operator[](int index) const
	{
		enf_assert(m_pCString != NULL);
		return m_pCString[index];
	}

	//operator const char*() const { return m_pCString; }

	CStr operator=(const char* str)
	{
		m_pCString = str;
		return *this;
	}

	double ToDouble() const;
	float ToFloat() const;
	int ToInt() const;

	int cmp(CStr str) const
	{
		if(m_pCString == str.m_pCString) return 0;
		if(m_pCString == NULL) return -1;
		if(str.m_pCString == NULL) return 1;
		return enf::strcmp(m_pCString, str.m_pCString);
	}
	
	int cmpi(CStr str) const
	{
		if(m_pCString == str.m_pCString) return 0;
		if(m_pCString == NULL) return -1;
		if(str.m_pCString == NULL) return 1;
		return enf::strcmpi(m_pCString, str.m_pCString);
	}

	int ncmp(CStr str, size_t len) const;
	const char* strchr(int chr) const;
	const char* strrchr(int chr) const;
	const char* strstr(CStr string) const;
	int scanf(const char* format, ...) const;

	unsigned int Length() const;

	const char* cstr() const
	{
		return m_pCString;
	}

	enf::CStr operator+=(int offset)
	{
		while(m_pCString && *m_pCString && offset--)
			m_pCString++;

		return *this;
	}

	enf::CStr operator++(int)
	{
		if(m_pCString && *m_pCString)
			m_pCString++;

		return *this;
	}
};
};

template<int len = 256> class SString
{
private:
	char		m_Buffer[len];

	const char* _set(const char* str, size_t offset, size_t l)
	{
		enf_assert((offset + l) < len);
		memcpy(m_Buffer + offset, str, l + 1);

		return m_Buffer;
	}

public:
	SString()
	{
		m_Buffer[0] = '\0';
	}

	SString(enf::CStr str)
	{
		*this = str;
	}

	ENF_INLINE const char* cstr() const
	{
		return m_Buffer;
	}

	ENF_INLINE size_t Length() const
	{
		return enf::strlen(m_Buffer);
	}

	ENF_INLINE void Clear()
	{
		m_Buffer[0] = '\0';
	}

	ENF_INLINE void SetSize(size_t l)
	{
		enf_assert(l < len);

		m_Buffer[l] = 0;
	}

	ENF_INLINE const char* AppendChar(unsigned int pos, char chr)
	{
		while(pos < len)
		{
			if(m_Buffer[pos] == 0)
			{
				m_Buffer[pos] = chr;
				m_Buffer[pos + 1] = 0;
				return m_Buffer;
			}
			pos++;
		}
		return m_Buffer;
	}

	ENF_INLINE int cmp(enf::CStr str) const
	{
		return enf::strcmp(m_Buffer, str.cstr());
	}

	ENF_INLINE int cmpi(enf::CStr str) const
	{
		return enf::strcmpi(m_Buffer, str.cstr());
	}

	ENF_INLINE int cmpi(const SString& str) const
	{
		return enf::strcmpi(m_Buffer, str.m_Buffer);
	}

	ENF_INLINE const char* operator=(enf::CStr str)
	{
		return _set(str.cstr(), 0, str.Length());
	}

	ENF_INLINE const char* operator=(const SString& str)
	{
		return _set(str.m_Buffer, 0, str.Length());
	}

	ENF_INLINE const char* operator+=(enf::CStr str)
	{
		return _set(str.cstr(), Length(), str.Length());
	}

	ENF_INLINE const char* operator+=(const SString& str)
	{
		return _set(str.m_Buffer, Length(), str.Length());
	}

	ENF_INLINE SString<len> operator+(enf::CStr str) const
	{
		SString<len> result = *this;

		result += str;

		return result;
	}

	ENF_INLINE SString<len> operator+(const SString& str) const
	{
		SString<len> result = *this;

		result += str;

		return result;
	}

	ENF_INLINE bool operator==(const SString& str) const
	{
		return (enf::strcmp(m_Buffer, str.m_Buffer) == 0);
	}

	ENF_INLINE bool operator==(enf::CStr str) const
	{
		return (enf::strcmp(m_Buffer, str.cstr()) == 0);
	}

	ENF_INLINE bool operator!=(const SString& str) const
	{
		return (enf::strcmp(m_Buffer, str.m_Buffer) != 0);
	}

	ENF_INLINE bool operator!=(enf::CStr str) const
	{
		return (enf::strcmp(m_Buffer, str.cstr()) != 0);
	}

	ENF_INLINE char operator[](size_t pos) const
	{
		enf_assert(pos <= Length());

		return m_Buffer[pos];
	}

	ENF_INLINE long ToLong()
	{
		return atol(m_Buffer);
	}

	ENF_INLINE long ToInt()
	{
		return atoi(m_Buffer);
	}
	
	ENF_INLINE double ToDouble()
	{
		return atof(m_Buffer);
	}

	const char* upper()
	{
		for(unsigned int n = 0; m_Buffer[n] != 0 && n < len; n++)
		{
			m_Buffer[n] = toupper(m_Buffer[n]);
		}
		return m_Buffer;
	}

	const char* lower()
	{
		for(unsigned int n = 0; m_Buffer[n] != 0 && n < len; n++)
		{
			m_Buffer[n] = tolower(m_Buffer[n]);
		}
		return m_Buffer;
	}

	ENF_INLINE size_t Find(const char* chr)
	{
		const char* p = strstr(m_Buffer, chr);

		if(p != NULL)
			return (p - m_Buffer);
		else
			return (size_t)-1;
	}

	ENF_INLINE size_t Find(char chr)
	{
		const char* p = enf::strchr(m_Buffer, (int)chr);

		if(p != NULL)
			return (p - m_Buffer);
		else
			return (size_t)-1;
	}

	ENF_INLINE size_t FindR(char chr)
	{
		const char* p = enf::strrchr(m_Buffer, (int)chr);

		if(p != NULL)
			return (p - m_Buffer);
		else
			return (size_t)-1;
	}

	ENF_INLINE const char* operator=(int num)
	{
		return format("%d", num);
	}

	ENF_INLINE const char* operator=(float num)
	{
		return format("%f", num);
	}

	const char* format(char *fmt, ...)
	{
		va_list argptr;
		va_start (argptr, fmt);

		__vsprintf(m_Buffer, fmt, argptr);
		
	//		enf_assert(enf::strlen(m_Buffer) < len);
	//		va_end (argptr);

		//enf_assert(m_iLength < len);
		va_end(argptr);
		return m_Buffer;
	}

	size_t TrimInPlace();

	const char* ReplaceExt(const char* ext)
	{
		char* ptr = enf::strrchr(m_Buffer, '.');

		if(ptr)
			enf::strcpy(ptr + 1, ext);

		return m_Buffer;
	}

	const char* StripExt()
	{
		int n = 0;
		int ext = -1;
		while(m_Buffer[n])
		{
			if(m_Buffer[n] == '.')
				ext = n;
			n++;
		}

		if(ext != -1)
			m_Buffer[ext] = 0;

		return m_Buffer;
	}

	const char* Copy(const char* str, unsigned int length)
	{
		char* dest  = m_Buffer;
		while(length-- && *str)
		{
			*dest++ = *str++;
		}

		*dest = '\0';
		return m_Buffer;
	}
};

class StaticBuffer;

class PUBLIC_API DString
{
  public :
	// The size type used
  	typedef size_t size_type;

	// Error value for find primitive
	static const size_type npos; // = -1;


	// DString empty constructor
	DString () : rep_(&nullrep_)
	{
	}

	// DString copy constructor
	DString ( const DString & copy)
	{
		init(copy.length());
		memcpy(start(), copy.data(), length());
	}

	//TODO: copy of static string!
	// DString constructor, based on a string
	DString ( const char * copy)
	{
		if(copy == NULL)
		{
			rep_ = &nullrep_;
			return;
		}
		init( static_cast<size_type>( enf::strlen(copy) ));
		memcpy(start(), copy, length());
	}

	//TODO: copy of static string!
	// DString constructor, based on a string
	DString ( const char * str, size_type len)
	{
		init(len);
		memcpy(start(), str, len);
	}

	// DString destructor
	~DString ()
	{
		quit();
	}

	// = operator
	DString& setConstant(const char * copy);

	//TODO: copy of static string!
	// = operator
	DString& operator = (const char * copy)
	{
		if(copy == NULL)
		{
			clear();
			return *this;
		}
		return assign( copy, (size_type)enf::strlen(copy));
	}

	//TODO: copy of static string!
	// = operator
	DString& operator = (const DString & copy)
	{
		return assign(copy.start(), copy.length());
	}


	// += operator. Maps to append
	DString& operator += (const char * suffix)
	{
		return append(suffix, static_cast<size_type>( enf::strlen(suffix) ));
	}

	// += operator. Maps to append
	DString& operator += (char single)
	{
		return append(&single, 1);
	}

	// += operator. Maps to append
	DString& operator += (const DString & suffix)
	{
		return append(suffix.data(), suffix.length());
	}

	// + operator. Maps to append
	DString operator+(const DString & suffix)
	{
		DString res = *this;
		res.append(suffix.data(), suffix.length());
		return res;
	}


	// Convert a DString into a null-terminated char *
	const char * c_str () const { return rep_->str; }

	// Convert a DString into a char * (need not be null terminated).
	const char * data () const { return rep_->str; }

	ENF_INLINE int cmp(const DString& str) const
	{
		return enf::strcmp(c_str(), str.c_str());
	}

	ENF_INLINE int cmpi(const DString& str) const
	{
		return enf::strcmpi(c_str(), str.c_str());
	}

	// Return the length of a DString
	size_type length () const { return rep_->size; }

	// Alias for length()
	size_type size () const { return rep_->size; }

	// Checks if a DString is empty
	bool empty () const { return rep_->size == 0; }

	// Return capacity of string
	size_type capacity () const { return rep_->capacity; }


	// single char extraction
	const char& at (size_type index) const
	{
		enf_assert( index < length() );
		return rep_->str[ index ];
	}

	// [] operator
	char& operator [] (size_type index) const
	{
		enf_assert( index < length() );
		return rep_->str[ index ];
	}

	// find a char in a string. Return DString::npos if not found
	size_type find (char lookup) const
	{
		return find(lookup, 0);
	}

	// find a char in a string from an offset. Return DString::npos if not found
	size_type find (char tofind, size_type offset) const
	{
		if (offset >= length()) return npos;

		for (const char* p = c_str() + offset; *p != '\0'; ++p)
		{
		   if (*p == tofind) return static_cast< size_type >( p - c_str() );
		}
		return npos;
	}

	void clear ()
	{
		quit();
		init(0,0);
	}

	/*	Function to reserve a big amount of data when we know we'll need it. Be aware that this
		function DOES NOT clear the content of the DString if any exists.
	*/
	void reserve (size_type cap);

	DString& assign (const char* str, size_type len);

	DString& append (const char* str, size_type len);

	void swap (DString& other)
	{
		Rep* r = rep_;
		rep_ = other.rep_;
		other.rep_ = r;
	}

  private:

	void init(size_type sz) { init(sz, sz); }
	void set_size(size_type sz) { rep_->str[ rep_->size = sz ] = '\0'; }
	char* start() const { return rep_->str; }
	char* finish() const { return rep_->str + rep_->size; }

	struct Rep
	{
		size_type size, capacity;
		char str[1];
	};

	void init(size_type sz, size_type cap);
	void quit();

	Rep * rep_;
	static Rep nullrep_;
	static StaticBuffer* staticbuff_;
};

inline bool operator == (const DString & a, const DString & b)
{
	return    ( a.length() == b.length() )				// optimization on some platforms
	       && ( enf::strcmp(a.c_str(), b.c_str()) == 0 );	// actual compare
}
inline bool operator < (const DString & a, const DString & b)
{
	return enf::strcmp(a.c_str(), b.c_str()) < 0;
}

inline bool operator != (const DString & a, const DString & b) { return !(a == b); }
inline bool operator >  (const DString & a, const DString & b) { return b < a; }
inline bool operator <= (const DString & a, const DString & b) { return !(b < a); }
inline bool operator >= (const DString & a, const DString & b) { return !(a < b); }

inline bool operator == (const DString & a, const char* b) { return enf::strcmp(a.c_str(), b) == 0; }
inline bool operator == (const char* a, const DString & b) { return b == a; }
inline bool operator != (const DString & a, const char* b) { return !(a == b); }
inline bool operator != (const char* a, const DString & b) { return !(b == a); }

DString operator + (const DString & a, const DString & b);
DString operator + (const DString & a, const char* b);
DString operator + (const char* a, const DString & b);

struct DStringHashFuncCS
{
	unsigned int operator()(const DString& str) const
	{
		unsigned int hashval = 0;

		for(const char* s = str.c_str(); *s; s++)
			hashval = hashval * 37 + *s;

		return hashval;
	}

	int compare(const DString& str1, const DString& str2) const
	{
		return str1.cmp(str2);
	}
};

struct DStringHashFunc
{
	unsigned int operator()(const DString& str) const
	{
		unsigned int hashval = 0;

		for(const char* s = str.c_str(); *s; s++)
			hashval = hashval * 37 + *s;

		return hashval;
	}

	int compare(const DString& str1, const DString& str2) const
	{
		return str1.cmpi(str2);
	}
};

struct CStrHashFuncCS
{
	unsigned int operator()(const enf::CStr& str) const
	{
		unsigned int hashval = 0;
		for(const char* s = str.cstr(); *s; s++)
			hashval = hashval * 37 + *s;

		return hashval;
	}

	int compare(const enf::CStr& str1, const enf::CStr& str2) const
	{
		return str1.cmp(str2);
	}
};

struct CStrHashFunc
{
	unsigned int operator()(const enf::CStr& str) const
	{
		unsigned int hashval = 0;
		for(const char* s = str.cstr(); *s; s++)
			hashval = hashval * 37 + ::tolower(*s);

		return hashval;
	}

	int compare(const enf::CStr& str1, const enf::CStr& str2) const
	{
		return str1.cmpi(str2);
	}
};

#endif //ENF_STRINGS
