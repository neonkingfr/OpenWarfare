//-----------------------------------------------------------------------------
// File: enf_memorybuffer.cpp
//
// Desc: Growable memory buffer
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#include "precomp.h"
#include "common/enf_memorybuffer.h"

//-----------------------------------------------------------------------
MemoryBuffer::MemoryBuffer(uint initialLength)
{
	m_pBuffer = (unsigned char*)MemoryManager::Alloc(initialLength, __FILE__, __LINE__);
	m_iPos = 0;
	m_iLength = initialLength;
	m_iSize = 0;
}

//-----------------------------------------------------------------------
MemoryBuffer::~MemoryBuffer()
{
	MemoryManager::Free(m_pBuffer);
}

//-----------------------------------------------------------------------
void MemoryBuffer::Seek(uint pos)
{
	if (pos > m_iSize)
	{
		m_iPos = m_iSize;
	}
	else
	{
		m_iPos = pos;
	}
}

//-----------------------------------------------------------------------
void MemoryBuffer::Write(const unsigned char* buffer, const uint length)
{
	Grow(m_iPos + length);
	memcpy(m_pBuffer + m_iPos, buffer, length);
	m_iPos += length;
	if (m_iPos > m_iSize)
	{
		m_iSize = m_iPos;
	}
}

//-----------------------------------------------------------------------
int MemoryBuffer::Read(unsigned char* buffer, const uint length)
{
	uint toRead = length;
	if (m_iPos + length > m_iSize)
	{
		toRead = m_iSize - m_iPos;
	}
	memcpy(buffer, m_pBuffer + m_iPos, toRead);
	m_iPos += toRead;
	return toRead;
}

//-----------------------------------------------------------------------
void MemoryBuffer::Grow(uint newMinLength)
{
	if (m_iLength < newMinLength)
	{
		uint newSize = enf_max(newMinLength, m_iLength * 2);
		m_pBuffer = (unsigned char*)MemoryManager::ReAlloc(m_pBuffer, newSize);
		m_iLength = newSize;
	}
}

//-----------------------------------------------------------------------
void MemoryBuffer::Reallocate(uint newLength=1024)
{
	m_pBuffer = (unsigned char*)MemoryManager::ReAlloc(m_pBuffer, newLength);
	m_iPos = 0;
	m_iSize = 0;
	m_iLength = newLength;
}
