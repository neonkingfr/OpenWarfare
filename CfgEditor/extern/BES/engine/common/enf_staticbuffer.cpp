//-----------------------------------------------------------------------------
// File: enf_staticbuffer.cpp
//
// Desc: Static heap allocator, static string allocator.
//			For cases when there is never deallocation
//
// Copyright (c) 2000-2006 Black Element Software. All rights reserved
//-----------------------------------------------------------------------------

#include "precomp.h"
#include "common/enf_types.h"
#include "common/enf_staticbuffer.h"

const uint ALIGN = 4;

//-----------------------------------------------------------------------
ENF_INLINE int StringWeight(const char *str)
{
	uint weight = 0;

	while(*str)
		weight = (weight * 37) + (unsigned)*str++;

	return (int)weight;
}

//-----------------------------------------------------------------------
ENF_INLINE int StringWeight(const char *str, int len)
{
	uint weight = 0;

	while(len--)
		weight = (weight * 37) + (unsigned)*str++;

	return weight;
}

//-----------------------------------------------------------------------
bool StaticBuffer::Init(int size)
{
	BeginData = NULL;
	EndData = NULL;
	Size = 0;
	StringRoot = NULL;
	Buffer = (char*)MemoryManager::Alloc(size, __FILE__, __LINE__);

	if(Buffer == NULL)
		return false;

	FillMem(Buffer, size, 0);
	BeginData = Buffer;
	Size = size;
	EndData = BeginData + size;
	return true;
}

//-----------------------------------------------------------------------
void StaticBuffer::Free()
{
	MemoryManager::Free(Buffer);
	Size = 0;
}

//-----------------------------------------------------------------------
void *StaticBuffer::AllocData(int len)
{
int p;

	if(len)
	{
		p = (int)BeginData;
		p = (p + (ALIGN - 1)) & ~(ALIGN - 1);

		BeginData = (char *)(p + len);

		enf_assert(BeginData < EndData);
	}
	else
		p = NULL;

	return (void *)p;
}

//-----------------------------------------------------------------------
void *StaticBuffer::AllocData(int len, const void *data)
{
	void *p;

	if((p = AllocData(len)) != NULL)
		CopyMem(p, data, len);

	return (void *)p;
}

//-----------------------------------------------------------------------
void StaticBuffer::FreeData(int len)
{
	enf_assert((BeginData - len) >= Buffer);

	BeginData -=len;
	ZeroMem(BeginData, len);
}

#define SIGN2INDEX(a) ((unsigned)(a) >> 31)

//-----------------------------------------------------------------------
const char *StaticBuffer::AddString(const char *string)
{
	if(string == NULL)
		return NULL;

	//already from string table
	if(string >= Buffer && string < BeginData)
		return string;

StrNode **node = &StringRoot;
StrNode *nnode;

int w = StringWeight(string);

	while((nnode = *node) != NULL)
	{
		int diff = w - nnode->m_iWeight;

		if(diff == 0 && enf::strcmp(string, nnode->m_strString) == 0)
			goto done;

		node = &nnode->m_psChildren[SIGN2INDEX(diff)];
	}

	int len = enf::strlen(string);

	nnode = (StrNode *)AllocData(sizeof(StrNode) + len + 1);
	*node = nnode;

	CopyMem((char *)(nnode + 1), (void*)string, len);
	nnode->m_strString = (char *)(nnode + 1);
	nnode->m_iWeight = w;
done:;
	return nnode->m_strString;
}

//-----------------------------------------------------------------------
const char *StaticBuffer::AddString(const char *string, int len)
{
	//already from string table
	if(string >= Buffer && string < BeginData)
		return string;

StrNode **node = &StringRoot;
StrNode *nnode;

int w = StringWeight(string, len);

	while((nnode = *node) != NULL)
	{
		int diff = w - nnode->m_iWeight;

		if(w == nnode->m_iWeight && strncmp(string, nnode->m_strString, len) == 0)
			goto done;

		node = &nnode->m_psChildren[SIGN2INDEX(diff)];
	}

	
	nnode = (StrNode *)AllocData(sizeof(StrNode) + len + 1);
	*node = nnode;

	CopyMem((char *)(nnode + 1), (void*)string, len);
	nnode->m_strString = (char *)(nnode + 1);
	nnode->m_iWeight = w;

done:;
	return nnode->m_strString;
}

//-----------------------------------------------------------------------
const char *StaticBuffer::FindString(const char *string)
{
	//already from string table
	if(string >= Buffer && string < BeginData)
		return string;

StrNode *node = StringRoot;
int w = StringWeight(string);

	while(node)
	{
		int diff = w - node->m_iWeight;
		if(w == node->m_iWeight && enf::strcmp(string, node->m_strString) == 0)
		{
			return node->m_strString;
		}
		node = node->m_psChildren[SIGN2INDEX(diff)];
	}

	return NULL;
}

//-----------------------------------------------------------------------
StaticBuffer::StaticBuffer()
{
	BeginData = NULL;
	EndData = NULL;
	Buffer = NULL;
	Size = 0;
	StringRoot = NULL;
}

//-----------------------------------------------------------------------
StaticBuffer::~StaticBuffer()
{
	Free();
}
