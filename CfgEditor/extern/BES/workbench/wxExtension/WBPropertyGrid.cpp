#include "wbPrecomp.h"

#ifdef WORKBENCH
	#include "Layout/Workbench.h"
	#include "wxExtension/RichFileDialog.h"
#else
	#include "wx/wx.h"	//je to dementne ale property grid to proste potrebuje pred includovanim sameho seba
#endif

#include "WBPropertyGrid.h"
#include "Common/NativeConvert.h"
#include "Common/WxConvert.h"
#include "Common/WinApiUtil.h"

//definuje novy typ eventu nastavitelny makrom EVT_WORKBENCH_PROPGRID
DEFINE_EVENT_TYPE(wxEVT_WORKBENCH_PROPGRID)

//=============================================================================================
WX_PG_IMPLEMENT_STRING_PROPERTY(WBFileProperty, wxPG_NO_ESCAPE)
#ifdef WORKBENCH
WX_PG_IMPLEMENT_STRING_PROPERTY(WBCustomDialogProperty, wxPG_NO_ESCAPE)

//---------------------------------------------------------------------------------------------
bool WBFilePropertyClass::OnButtonClick( wxPropertyGrid* propgrid, wxString& value )
{
	wxPGId PropId = propgrid->GetSelectedProperty();
	wxPGProperty* prop = propgrid->GetPropertyPtr(PropId);

	PGPropertyClientData* data = (PGPropertyClientData*)prop->GetClientData();
	assert(data);	//musi ich mat
	assert(data->GetType() == CPT_FileProperty);

	RichFileDialog* dialog = (RichFileDialog*)data->GetCustomData();
	assert(dialog);
	dialog->SetInputValue(value);

	if(dialog->ShowAsModal() == wxID_OK)
	{
		value = dialog->GetResultValue();	//zapiseme ze sme hodnotu zmenili
		return true;
	}

	return false;
}

//---------------------------------------------------------------------------------------------
bool WBCustomDialogPropertyClass::OnButtonClick( wxPropertyGrid* propgrid, wxString& value )
{
	wxPGId PropId = propgrid->GetSelectedProperty();
	wxPGProperty* prop = propgrid->GetPropertyPtr(PropId);

	PGPropertyClientData* data = (PGPropertyClientData*)prop->GetClientData();
	assert(data);	//musi ich mat

	assert(data->GetType() == CPT_CustomDialogProperty);

	UniversalBaseDialog* dialog = (UniversalBaseDialog*)data->GetCustomData();
	assert(dialog);
	dialog->SetInputValue(value);

	if(dialog->ShowAsModal() == wxID_OK)
	{
		value = dialog->GetResultValue();	//zapiseme ze sme hodnotu zmenili
		return true;
	}

	return false;
}
#else
//---------------------------------------------------------------------------------------------
bool WBFilePropertyClass::OnButtonClick( wxPropertyGrid* propgrid, wxString& value )
{
	WBPropertyGrid* wbpropgrid = dynamic_cast<WBPropertyGrid*>(propgrid);
	wxPGId PropId = propgrid->GetSelectedProperty();
	wxPGProperty* prop = propgrid->GetPropertyPtr(PropId);

	PGPropertyClientData* data = (PGPropertyClientData*)prop->GetClientData();
	assert(data);	//musi ich mat
	assert(data->GetType() == CPT_FileProperty);
	const char* extension = (const char*)data->GetCustomData();

	if(!extension)
		extension = "*";
	
	wxString MaskString(extension);
	MaskString += wxString(" files (*.") + wxString(extension) + wxString(")|*.") + wxString(extension);
	wxString DialogTitle = propgrid->GetPropertyLabel(prop);

	if(!value.IsEmpty() && value.GetChar(0) == '\\')	//mohlo by byt na zaciatku lomitko
		value = value.AfterFirst('\\');

	if(!taFileExists(value.c_str()))	//moze to tam byt v absolutnej alebo realtivnej trase
		value = wbpropgrid->GetDataRootDir() + wxString("\\") + value;

	wxFileDialog LoadDialog(propgrid, DialogTitle, _T(""), _T(""), MaskString, wxOPEN | wxFILE_MUST_EXIST);
	LoadDialog.SetPath(value);
	LoadDialog.Centre();

	if(LoadDialog.ShowModal() == wxID_OK)
	{
		wxString path = LoadDialog.GetPath();
		path.Replace("\\", "/");
		path.LowerCase();

		wxString RootDir = ((WBPropertyGrid*)propgrid)->GetDataRootDir();
		RootDir.Replace("\\", "/");
		RootDir.LowerCase();

		if(!RootDir.IsEmpty() && path.Find(RootDir.c_str() == 0))
			path.erase(0, RootDir.length() + 1);
		else
			path = "\\" + path;	//specilaita BISu. na zaciatku absilutnej trasy lomitko

		path.Replace("/", "\\");
		value = path;
		return true;
	}

/*	RichFileDialog* dialog = (RichFileDialog*)data->GetCustomData();
	assert(dialog);
	dialog->SetInputValue(value);

	if(dialog->ShowAsModal() == wxID_OK)
	{
		value = dialog->GetResultValue();	//zapiseme ze sme hodnotu zmenili
		return true;
	}*/

	return false;
}

#endif

//---------------------------------------------------------------------------------------------
wxString WBPropGridEventData::GetValueAsString()
{
	switch(GetValueType())
	{
		case PVT_BOOL:
		{
			PGPropertyValueBool* val = (PGPropertyValueBool*)GetValue();
			return ItoA(taBtoI(val->GetValue()));
		}
		case PVT_INT:
		{
			PGPropertyValueInt* val = (PGPropertyValueInt*)GetValue();
			return ItoA(val->GetValue());
		}
		case PVT_FLOAT:
		{
			PGPropertyValueFloat* val = (PGPropertyValueFloat*)GetValue();
#ifdef WORKBENCH
			return FtoA(val->GetValue(), g_Workbench->GetUnitsPrecision());
#else
			return FtoA(val->GetValue());
#endif

		}
		case PVT_STRING:
		{
			PGPropertyValueString* val = (PGPropertyValueString*)GetValue();
			return val->GetValue();
		}
		case PVT_VECTOR:
		{
			PGPropertyValueVector* val = (PGPropertyValueVector*)GetValue();
#ifdef WORKBENCH
			return VecToStr(val->GetValue(), g_Workbench->GetUnitsPrecision());
#else
			return VecToStr(val->GetValue());
#endif
		}
		case PVT_COLOR:
		{
			PGPropertyValueColor* val = (PGPropertyValueColor*)GetValue();
			const wxColour& col = val->GetValue();
			assert(col.IsOk());
			float r = (float)col.Red() / 255.0f;
			float g = (float)col.Green() / 255.0f;
			float b = (float)col.Blue() / 255.0f;
			return VecToStr(Vector3(r, g, b));
		}
	}

	assert(false);
	return "";
}

//---------------------------------------------------------------------------------------------
BEGIN_EVENT_TABLE(WBPropertyGrid, wxPropertyGrid)
	EVT_PG_CHANGED( -1, WBPropertyGrid::OnPropertyGridChange )
//	EVT_KILL_FOCUS(WBPropertyGrid::OnLostFocus)
//	EVT_RIGHT_DOWN(WBPropertyGrid::OnRMBDown)
//	EVT_RIGHT_UP(WBPropertyGrid::OnRMBUp)
END_EVENT_TABLE()

//----------------------------------------------------------------------------------------------
WBPropertyGrid::WBPropertyGrid(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size,  long style, const wxChar* name)
	: wxPropertyGrid(parent, id, pos, size, style, name)
{
}

//----------------------------------------------------------------------------------------------
WBPropertyGrid::~WBPropertyGrid()
{
	for(uint n = 0; n < ClientDatas.GetCardinality(); n++)
	{
		PGPropertyClientData* data = ClientDatas[n];
		delete data;
	}
	ClientDatas.Clear();
}
/*
//----------------------------------------------------------------------------------------------
void WBPropertyGrid::OnLostFocus(wxFocusEvent& event)
{
	wxWindow* EdCtrl = GetEditorControl();	//aktivny editor

	if(EdCtrl)
	{
		wxPGId PropId = GetSelectedProperty();

		if(PropId.IsOk())
		{
			wxPGProperty* prop = GetPropertyPtr(PropId);
			//potrebujeme aby sa editbox po odklepnuti hodnoty skryl. neviem o lepsej moznosti takze takto
			if(dynamic_cast<wxTextCtrl*>(EdCtrl))
				prop->RecreateEditor();
		}
	}

	event.Skip();
}
*/
//----------------------------------------------------------------------------------------------
void WBPropertyGrid::OnRMBDown(wxMouseEvent& event)
{
	event.Skip();
}

//----------------------------------------------------------------------------------------------
void WBPropertyGrid::OnRMBUp(wxMouseEvent& event)
{
	event.Skip();
}

//----------------------------------------------------------------------------------------------
void WBPropertyGrid::OnPropertyGridChange( wxPropertyGridEvent& event )
{
	//if editor control is NULL, this event is not valid because this object was cleared! wxPropertyGrid bug!!!!
	if(!GetEditorControl())
		return;

	wxPGProperty* property = event.GetProperty();
	wxPGProperty* ParentProperty = event.GetMainParent();

	if(ParentProperty)
	{
		PGPropertyClientData* data = (PGPropertyClientData*)ParentProperty->GetClientData();

		wxCommandEvent CustomEvent(wxEVT_WORKBENCH_PROPGRID);
		CustomEvent.SetId(this->GetId());
		CustomEvent.SetEventObject(this);
		CustomEvent.SetString(ParentProperty->GetName());

		if(data && (data->GetType() == CPT_VectorProperty || data->GetType() == CPT_ComboBoxProperty))	//vector ma 3 childy z ktorych poskladame hodnotu
		{
			if(data->GetType() == CPT_VectorProperty)
			{
				wxPGId PropertyIdX = GetFirstChild(ParentProperty->GetId());
				wxPGId PropertyIdY = GetNextSibling(PropertyIdX);
				wxPGId PropertyIdZ = GetNextSibling(PropertyIdY);
				wxPGProperty* PropertyX = GetPropertyPtr(PropertyIdX);
				wxPGProperty* PropertyY = GetPropertyPtr(PropertyIdY);
				wxPGProperty* PropertyZ = GetPropertyPtr(PropertyIdZ);
				assert(PropertyX);
				assert(PropertyY);
				assert(PropertyZ);

				PGPropertyValueVector val(Vector3((float)GetPropertyValueAsDouble(PropertyX), (float)GetPropertyValueAsDouble(PropertyY), (float)GetPropertyValueAsDouble(PropertyZ)));
				WBPropGridEventData EventData(ParentProperty, &val);

				CustomEvent.SetClientObject(&EventData);
				GetEventHandler()->ProcessEvent(CustomEvent);
			}
			else	//CPT_ComboBoxProperty
			{
				if((int)data->GetCustomData() == PVT_STRING)
				{
					PGPropertyValueString val(GetPropertyValueAsString(ParentProperty));
					WBPropGridEventData EventData(ParentProperty, &val);
					CustomEvent.SetClientObject(&EventData);
					GetEventHandler()->ProcessEvent(CustomEvent);
				}
				else	//PVT_INT
				{
					wxPGChoices& choices = GetPropertyChoices(ParentProperty->GetId());
					assert(choices.GetCount() > 0);

					const wxArrayString& labels = choices.GetLabels();
					const wxArrayInt& IntValues = choices.GetValues();
					assert(IntValues.GetCount() > 0);

					wxString StrVal = GetPropertyValueAsString(ParentProperty);
					int ind = labels.Index(StrVal);
					
					if(ind >= 0)
					{
						PGPropertyValueInt val((int)IntValues[ind]);
						WBPropGridEventData EventData(ParentProperty, &val);
						CustomEvent.SetClientObject(&EventData);
						GetEventHandler()->ProcessEvent(CustomEvent);
					}
					else
					{
						PGPropertyValueInt val(taAtoI(StrVal.c_str()));
						WBPropGridEventData EventData(ParentProperty, &val);
						CustomEvent.SetClientObject(&EventData);
						GetEventHandler()->ProcessEvent(CustomEvent);
					}
				}
			}
		}
		else	//hodnotu inych property ziskame priamo z eventu
		{
			wxPGProperty* NormalProp = event.GetProperty();

			const wxString& name = event.GetPropertyName();
			wxVariant value = event.GetPropertyValue();
			wxString type = value.GetType();
			wxString PropType = NormalProp->GetType();

			const wxPGPropertyClassInfo* info = NormalProp->GetClassInfo();

			if(type == "long")
			{
				//ked je to combo a hodnoty su typu string tak musime nasledovne
				if(data && data->GetType() == CPT_ComboBoxProperty && (int)data->GetCustomData() == PVT_STRING)
				{
					int index = value.GetInteger();
					wxPGChoiceInfo ChoiceInfo;
					int num = NormalProp->GetChoiceInfo(&ChoiceInfo);

					PGPropertyValueString val(ChoiceInfo.m_arrWxString[index]);
					WBPropGridEventData EventData(NormalProp, &val);
					CustomEvent.SetClientObject(&EventData);
					GetEventHandler()->ProcessEvent(CustomEvent);
				}
				else	//moze to byt aj combo ale hodnota je int
				{
					PGPropertyValueInt val((int)value.GetInteger());
					WBPropGridEventData EventData(NormalProp, &val);
					CustomEvent.SetClientObject(&EventData);
					GetEventHandler()->ProcessEvent(CustomEvent);
				}
			}
			else if(type == "double")
			{
				PGPropertyValueFloat val((float)value.GetDouble());
				WBPropGridEventData EventData(NormalProp, &val);
				CustomEvent.SetClientObject(&EventData);
				GetEventHandler()->ProcessEvent(CustomEvent);
			}
			else if(type == "string")
			{
				PGPropertyValueString val(value.GetString());
				WBPropGridEventData EventData(NormalProp, &val);
				CustomEvent.SetClientObject(&EventData);
				GetEventHandler()->ProcessEvent(CustomEvent);
			}
			else if(type == "bool")
			{
				PGPropertyValueBool val(value.GetBool());
				WBPropGridEventData EventData(NormalProp, &val);
				CustomEvent.SetClientObject(&EventData);
				GetEventHandler()->ProcessEvent(CustomEvent);
			}
			if(type == "wxColour")
			{
				wxColour col = *(wxColour*)event.GetPropertyValueAsWxObjectPtr();
				PGPropertyValueColor val(col);
				WBPropGridEventData EventData(NormalProp, &val);
				CustomEvent.SetClientObject(&EventData);
				GetEventHandler()->ProcessEvent(CustomEvent);
			}
		}
	}

	wxWindow* EdCtrl = GetEditorControl();	//aktivny editor

	if(EdCtrl)
	{
		//potrebujeme aby sa editbox po odklepnuti hodnoty skryl. neviem o lepsej moznosti takze takto
		if(dynamic_cast<wxTextCtrl*>(EdCtrl))
			property->RecreateEditor();
	}
}

//---------------------------------------------------------------------------------------------------
wxPGProperty* WBPropertyGrid::InsertEditBoxProperty(wxPGId ParentID, const char* name, const char* label, int value, const wxValidator& validator, int position)
{
	wxPGProperty* prop = wxIntProperty(label, name, value);
	prop->SetValidator(validator);

	SetClientDataToProperty(prop, new PGPropertyClientData(CPT_EditBoxProperty, (void*)PVT_INT));
	wxPGId PropID = InsertOrAppendProperty(ParentID, prop, position);
	SetPropertyEditor(PropID,wxPG_EDITOR(SpinCtrl));
	return prop;
}

//---------------------------------------------------------------------------------------------------
wxPGProperty* WBPropertyGrid::InsertEditBoxProperty(wxPGId ParentID, const char* name, const char* label, float value, const wxValidator& validator, int position)
{
	wxPGProperty* prop = wxFloatProperty(label, name, value);
	prop->SetValidator(validator);

	SetClientDataToProperty(prop, new PGPropertyClientData(CPT_EditBoxProperty, (void*)PVT_FLOAT));
	InsertOrAppendProperty(ParentID, prop, position);
	return prop;
}

//---------------------------------------------------------------------------------------------------
wxPGProperty* WBPropertyGrid::InsertEditBoxProperty(wxPGId ParentID, const char* name, const char* label, const char* value, const wxValidator& validator, int position)
{
	wxPGProperty* prop = wxStringProperty(label, name, value);
	prop->SetValidator(validator);

	SetClientDataToProperty(prop, new PGPropertyClientData(CPT_EditBoxProperty, (void*)PVT_STRING));
	InsertOrAppendProperty(ParentID, prop, position);
	return prop;
}

//---------------------------------------------------------------------------------------------------
wxPGProperty* WBPropertyGrid::InsertComboBoxProperty(wxPGId ParentID, const char* name, const char* label, int value, const wxValidator& validator, wxPGChoices& choices, bool ReadOnly, int position)
{
	int NumLables = choices.GetLabels().GetCount();
	int NumValues = choices.GetValues().GetCount();
	assert(NumLables > 0);
	assert(NumLables == NumValues);

	if(NumLables <= 0)	return false;
	if(NumLables != NumValues)	return false;

	wxPGProperty* prop;

	if(ReadOnly)
		prop = wxEnumProperty(label, name, choices);
	else
		prop = wxEditEnumProperty(label, name, choices);

//	SetPropertyValue(prop, choices.GetValues("cross + USE key");

//	prop->SetChoiceSelection
	prop->SetValidator(validator);

	SetClientDataToProperty(prop, new PGPropertyClientData(CPT_ComboBoxProperty, (void*)PVT_INT));	//pametame si ze combo je typu INT

	wxPGId PropID = InsertOrAppendProperty(ParentID, prop, position);

	int ValChoiceIndex = -1;

	for(uint n = 0; n < choices.GetCount(); n++)
	{
		if(choices.GetValue(n) == value)
		{
			ValChoiceIndex = n;
			break;
		}
	}

	if(ValChoiceIndex >= 0 && ValChoiceIndex < NumValues)
		SetPropertyValue (PropID, choices.GetLabels()[ValChoiceIndex]);
	else
		SetPropertyValue (PropID, ItoA(value));

	return prop;

//	choices.GetValues()
}

//---------------------------------------------------------------------------------------------------
wxPGProperty* WBPropertyGrid::InsertComboBoxProperty(wxPGId ParentID, const char* name, const char* label, const char* value, const wxValidator& validator, wxPGChoices& choices, bool ReadOnly, int position)
{
	int NumLables = choices.IsOk() ? choices.GetLabels().GetCount() : 0;
	assert(NumLables > 0);
	if(NumLables <= 0)	return false;

	wxPGProperty* prop;

	if(ReadOnly)
		prop = wxEnumProperty(label, name, choices);
	else
		prop = wxEditEnumProperty(label, name, choices);

	prop->SetValidator(validator);
	SetClientDataToProperty(prop, new PGPropertyClientData(CPT_ComboBoxProperty, (void*)PVT_STRING));	//pametame si ze combo je typu STRING

	wxPGId PropID = InsertOrAppendProperty(ParentID, prop, position);
	SetPropertyValue (PropID, value);
	return prop;
}

//---------------------------------------------------------------------------------------------------
wxPGProperty* WBPropertyGrid::InsertCheckBoxProperty(wxPGId ParentID, const char* name, const char* label, bool value, int position)
{
	wxPGProperty* prop = wxBoolProperty(label, name, value);
	SetClientDataToProperty(prop, new PGPropertyClientData(CPT_CheckBoxProperty, (void*)PVT_BOOL));
   wxPGId PropID = InsertOrAppendProperty(ParentID, prop, position);
	SetPropertyAttribute(PropID,wxPG_BOOL_USE_CHECKBOX,(long)1,wxPG_RECURSE);
	return prop;
}

//---------------------------------------------------------------------------------------------------
wxPGProperty* WBPropertyGrid::InsertVectorProperty(wxPGId ParentID, const char* name, const char* label, Vector3& value, int position)
{
	wxPGProperty* prop = wxParentProperty(name, wxPG_LABEL);
	SetClientDataToProperty(prop, new PGPropertyClientData(CPT_VectorProperty, NULL));	//wxParentProperty nema zoadny typ tak si ho ulozime do client dat aby sme potom pri odchyteni eventu vedeli ze sa jedna o vektor ktory ma 3 child property
   wxPGId PropID = InsertOrAppendProperty(ParentID, prop, position);
   AppendIn( PropID, wxFloatProperty(wxT("x"),wxPG_LABEL,value.x) );
   AppendIn( PropID, wxFloatProperty(wxT("y"),wxPG_LABEL,value.y) );
	AppendIn( PropID, wxFloatProperty(wxT("z"),wxPG_LABEL,value.z) );
	return prop;
}

#ifdef WORKBENCH
//---------------------------------------------------------------------------------------------------
wxPGProperty* WBPropertyGrid::AppendFileProperty(const char* name, const char* label, const char* value, const wxValidator& validator, RichFileDialog* dialog)
{
	wxPGProperty* prop = WBFileProperty(label, name, value);
	prop->SetValidator(validator);
	SetClientDataToProperty(prop, new PGPropertyClientData(CPT_FileProperty, dialog));
	wxPGId PropID = Append( prop );
	return prop;
}

//---------------------------------------------------------------------------------------------------
wxPGProperty* WBPropertyGrid::AppendCustomDialogProperty(const char* name, const char* label, const char* value, const wxValidator& validator, UniversalBaseDialog* dialog)
{
	wxPGProperty* prop = WBCustomDialogProperty(label, name, value);
	prop->SetValidator(validator);
	SetClientDataToProperty(prop, new PGPropertyClientData(CPT_CustomDialogProperty, dialog));
	wxPGId PropID = Append( prop );
	return prop;
}
#else
//---------------------------------------------------------------------------------------------------
wxPGProperty* WBPropertyGrid::InsertFileProperty(wxPGId ParentID, const char* name, const char* label, const char* value, const char* extention, const wxValidator& validator, int position)
{
	wxPGProperty* prop = WBFileProperty(label, name, value);
	prop->SetValidator(validator);
	SetClientDataToProperty(prop, new PGPropertyClientData(CPT_FileProperty, (void*)extention));
	wxPGId PropID = InsertOrAppendProperty(ParentID, prop, position);
	return prop;
}
#endif
//---------------------------------------------------------------------------------------------------
wxPGProperty* WBPropertyGrid::InsertColorProperty(wxPGId ParentID, const char* name, const char* label, wxColour& value, int position)
{
	wxPGProperty* prop = wxColourProperty(label, name, value );
	wxPGId PropID = InsertOrAppendProperty(ParentID, prop, position);
	return prop;
}

//---------------------------------------------------------------------------------------------------
wxPGProperty* WBPropertyGrid::InsertFlagsProperty(wxPGId ParentID, const char* name, const char* label, int value, wxPGChoices& BitNamesAndOrders, int position)
{
	wxPGProperty* prop = wxFlagsProperty(label,
                              name,
                              BitNamesAndOrders.GetLabels(),
                              BitNamesAndOrders.GetValues(),
                              wxDEFAULT_FRAME_STYLE);
	SetClientDataToProperty(prop, new PGPropertyClientData(CPT_FlagsProperty, (void*)PVT_INT));
   wxPGId PropID = InsertOrAppendProperty(ParentID, prop, position);
	SetPropertyAttribute(PropID, wxPG_BOOL_USE_CHECKBOX, (long)1, wxPG_RECURSE);
	SetPropertyValue(PropID, value);
	return prop;
}

//---------------------------------------------------------------------------------------------------
wxPGProperty* WBPropertyGrid::InsertSliderProperty(wxPGId ParentID, const char* name, const char* label, int value, int MinValue, int MaxValue, int position)
{
	wxPGProperty* prop = wxIntSliderProperty(label, name, value);
	prop->SetValidator(IntSliderValidator(MinValue, MaxValue));
	SetClientDataToProperty(prop, new PGPropertyClientData(CPT_SliderProperty, (void*)PVT_INT));
	wxPGId PropID = InsertOrAppendProperty(ParentID, prop, position);
	return prop;
}

//---------------------------------------------------------------------------------------------------
wxPGProperty* WBPropertyGrid::InsertSliderProperty(wxPGId ParentID, const char* name, const char* label, float value, float MinValue, float MaxValue, float step, int position)
{
	wxPGProperty* prop = wxFloatSliderProperty(label, name, value);
	prop->SetValidator(FloatSliderValidator(MinValue, MaxValue, step));
	SetClientDataToProperty(prop, new PGPropertyClientData(CPT_SliderProperty, (void*)PVT_FLOAT));
	wxPGId PropID = InsertOrAppendProperty(ParentID, prop, position);
	return prop;
}

//---------------------------------------------------------------------------------------------------
void WBPropertyGrid::SetClientDataToProperty(wxPGProperty* prop, PGPropertyClientData* data)
{
	assert(prop);
	assert(data);
	prop->SetClientData(data);
	ClientDatas.Insert(data);	//na konci zmazeme
}

//---------------------------------------------------------------------------------------------------
void WBPropertyGrid::SetUserDataToProperty(wxPGProperty* prop, void* data)
{
	PGPropertyClientData* InternalData = (PGPropertyClientData*)prop->GetClientData();
	enf_assert(InternalData);
	InternalData->SetUserData(data);
}

//---------------------------------------------------------------------------------------------------
void* WBPropertyGrid::GetUserDataFromProperty(wxPGProperty* prop)
{
	PGPropertyClientData* InternalData = (PGPropertyClientData*)prop->GetClientData();

	if(!InternalData)
	{
		wxString name = GetPropertyName(prop->GetId());
		wxString label = GetPropertyLabel(prop->GetId());
		label = label;
	}

	enf_assert(InternalData);

//	if(!InternalData)
//		return NULL;

	return InternalData->GetUserData();
}

//---------------------------------------------------------------------------------------------------
wxPGProperty* WBPropertyGrid::SetPropertyValueEx(const char* name, const Vector3& value)
{
	wxPGProperty* prop = this->GetPropertyByName(name);

	if(!prop)
		return false;

	PGPropertyClientData* data = (PGPropertyClientData*)prop->GetClientData();

	if(!data || data->GetType() != CPT_VectorProperty)
		return NULL;

	wxPGId PropertyIdX = GetFirstChild(prop->GetId());
	wxPGId PropertyIdY = GetNextSibling(PropertyIdX);
	wxPGId PropertyIdZ = GetNextSibling(PropertyIdY);
	wxPGProperty* PropertyX = GetPropertyPtr(PropertyIdX);
	wxPGProperty* PropertyY = GetPropertyPtr(PropertyIdY);
	wxPGProperty* PropertyZ = GetPropertyPtr(PropertyIdZ);
	assert(PropertyX);
	assert(PropertyY);
	assert(PropertyZ);

	this->SetPropertyValueDouble(PropertyX, value.x);
	this->SetPropertyValueDouble(PropertyY, value.y);
	this->SetPropertyValueDouble(PropertyZ, value.z);
	return prop;
}

//---------------------------------------------------------------------------------------------------
wxPGProperty* WBPropertyGrid::SetPropertyValueEx(const char* name, float value)
{
	wxPGProperty* prop = this->GetPropertyByName(name);

	if(!prop)
		return NULL;

	if(prop->GetType() != "double")
		return NULL;

	this->SetPropertyValueDouble(prop, value);
	return prop;
}

//---------------------------------------------------------------------------------------------------
void WBPropertyGrid::RedrawProperty(wxPGProperty* prop)
{
	assert(prop);

	if(prop)
		this->RefreshProperty(prop);
}

//---------------------------------------------------------------------------------------------------
void WBPropertyGrid::RedrawProperty(wxPGId& id)
{
	assert(id.IsOk());

	if(id.IsOk())
		RefreshProperty(id);

}

//---------------------------------------------------------------------------------------------------
wxPGId WBPropertyGrid::FindProperty(const char* name)
{
	for(wxPGId id = GetFirstProperty(); id.IsOk(); id = GetNextProperty(id))
	{
		const wxString& pname = GetPropertyName(id);

		if(pname == name)
			return id;
	}
	return wxPGId();
}

//---------------------------------------------------------------------------------------------------
wxPGId WBPropertyGrid::FindCategory(const char* name)
{
	for(wxPGId id = GetFirstCategory(); id.IsOk(); id = GetNextCategory(id))
	{
		const wxString& pname = GetPropertyName(id);

		if(pname == name)
			return id;
	}
	return wxPGId();
}

//---------------------------------------------------------------------------------------------------
wxPGId WBPropertyGrid::FindCategory(void* ClientData)
{
	for(wxPGId id = GetFirstCategory(); id.IsOk(); id = GetNextCategory(id))
	{
		void* data = GetPropertyClientData(id);

		if(data == ClientData)
			return id;
	}
	return wxPGId();
}

//---------------------------------------------------------------------------------------------
void WBPropertyGrid::SetDataRootDir(const char* dir)
{
	DataRootDir = dir;
}

//---------------------------------------------------------------------------------------------
wxPGId WBPropertyGrid::InsertOrAppendProperty(wxPGId ParentID, wxPGProperty* prop, int position)
{
	wxPGId result;

	if(ParentID.IsOk())
	{
		if(position >= 0)
			result = Insert(ParentID, position, prop);
		else
			result = AppendIn( ParentID, prop );
	}
	else
	{
		if(position >= 0)
			result = Insert(wxPGId(), position, prop);	//snad zafunguje. neni tam verzia bez parentu
		else
			result = Append( prop );
	}

	enf_assert(result.IsOk());
	return result;
}

//---------------------------------------------------------------------------------------------
bool WBPropertyGrid::EditorIsFocused()
{
//	return (m_curFocused != NULL) && GetPrimaryEditor() && (GetPrimaryEditor() == m_curFocused);//m_editorFocused;
	return m_editorFocused;
}

//---------------------------------------------------------------------------------------------
void WBPropertyGrid::UnfocusEditor()
{
	m_editorFocused = 0;
	wxWindow* control = GetPrimaryEditor();

	if(control && control->IsShown())
	{
		control->Hide();
		control->Show();
	}
}

//---------------------------------------------------------------------------------------------
void WBPropertyGrid::FocusEditor()
{
	m_editorFocused = 1;
}
