
#include "wbPrecomp.h"
#include "ControlsGlobal.h"
#include "Common/NativeUtil.h"
#include "Common/WxConvert.h"

//----------------------------------------------------------------------------------------
wxString CorrectizeStringFromInput(wxString str, int InputType)
{
	wxString NewVal;
	wxChar Current;
	int n;

	if(InputType == VarType_Int)
	{
		if(str.IsNumber() == false)
		{
			for(n = 0; n < (int)str.Length(); n++)
			{
				Current = str.GetChar(n);

				if(wxString(Current).IsNumber())
				{
					if(Current != '-' || n == 0)	// samotny znak - je u nich number!
						NewVal.Append(Current);
				}
			}
			return NewVal;
		}
		else
			return str;
	}

	if(InputType == VarType_Float)
	{
		int NumPoints = 0;

		if(IsFloatNumber(str.c_str()) == false)
		{
			for(n = 0; n < (int)str.Length(); n++)
			{
				Current = str.GetChar(n);

				if(wxString(Current).IsNumber() || Current == '.')
				{
					if(Current != '-' || n == 0)	// samotny znak - je u nich number!
					{
						if(Current != '.' || n != 0)
						{
							if(Current == '.')
								NumPoints++;

							if(NumPoints <= 1 || Current != '.')
								NewVal.Append(Current);
						}
					}
				}
			}
			return NewVal;
		}
		else
			return str;
	}
	return str;
}

//--------------------------------------------------------------------------------------------------------
void ClampIntValue(wxString& val, wxString& MinVal, wxString& MaxVal)
{
	int CurVal = AtoI(val);
	int Min = AtoI(MinVal);
	int Max = AtoI(MaxVal);

	if(CurVal < Min)
		val = ItoA(Min);
	else if(CurVal > Max)
		val = ItoA(Max);
}

//--------------------------------------------------------------------------------------------------------
void ClampFloatValue(wxString& val, wxString& MinVal, wxString& MaxVal)
{
	float CurVal = (float)atof(val);
	float Min = (float)atof(MinVal);
	float Max = (float)atof(MaxVal);

	if(CurVal < Min)
		val = FtoA(Min);
	else if(CurVal > Max)
		val = FtoA(Max);
}