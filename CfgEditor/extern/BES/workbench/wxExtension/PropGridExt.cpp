#include "wx/wx.h"	//koli propgridu musi byt pred nim nieco wx
#include "PropGridExt.h"
#include "WxExtension/SmartSlider.h"
#include "WxExtension/Validators.h"
#include "common/WxNativeConvert.h"

// -----------------------------------------------------------------------
IntSliderValidator::IntSliderValidator(int MinValue, int MaxValue)
	:IntValidator(MinValue, MaxValue)
{
}

// -----------------------------------------------------------------------
wxObject* IntSliderValidator::Clone() const
{
	return new IntSliderValidator(MinValue, MaxValue);
}

// -----------------------------------------------------------------------
FloatSliderValidator::FloatSliderValidator(float MinValue, float MaxValue, float step)
	:FloatValidator(MinValue, MaxValue)
{
	assert(step > 0);
	assert(step <= (MaxValue - MinValue));
	this->step = step;
}

// -----------------------------------------------------------------------
wxObject* FloatSliderValidator::Clone() const
{
	return new FloatSliderValidator(MinValue, MaxValue, step);
}

// -----------------------------------------------------------------------
// wxIntSliderProperty
// -----------------------------------------------------------------------
wxPG_BEGIN_PROPERTY_CLASS_BODY2(wxIntSliderPropertyClass,wxPGProperty,long,long,long,class)
    WX_PG_DECLARE_BASIC_TYPE_METHODS()
    WX_PG_DECLARE_CHOICE_METHODS()
    WX_PG_DECLARE_ATTRIBUTE_METHODS()
wxPG_END_PROPERTY_CLASS_BODY()

// We cannot use standard WX_PG_IMPLEMENT_PROPERTY_CLASS macro, since
// there is a custom GetEditorClass.

WX_PG_IMPLEMENT_CONSTFUNC(wxIntSliderProperty,long)
WX_PG_IMPLEMENT_CLASSINFO(wxIntSliderProperty,wxBasePropertyClass)
wxPG_GETCLASSNAME_IMPLEMENTATION(wxIntSliderProperty)
wxPG_VALUETYPE_MSGVAL wxIntSliderPropertyClass::GetValueType() const { return wxPG_VALUETYPE(long); }

const wxChar* wxPG_ClassName_wxIntSliderProperty = wxIntSliderProperty_ClassName;

// -----------------------------------------------------------------------
const wxPGEditor* wxIntSliderPropertyClass::DoGetEditorClass() const
{
	return wxPG_EDITOR(IntSlider);
}

// -----------------------------------------------------------------------
wxIntSliderPropertyClass::wxIntSliderPropertyClass( const wxString& label, const wxString& name, long value ) :
    wxPGProperty(label,name)
{
	wxPGRegisterEditorClass(IntSlider);

    DoSetValue((long)value);
    m_flags |= wxPG_PROP_USE_DCC;
}

// -----------------------------------------------------------------------
wxIntSliderPropertyClass::~wxIntSliderPropertyClass() { }

// -----------------------------------------------------------------------
void wxIntSliderPropertyClass::DoSetValue( wxPGVariant value )
{
    long v = wxPGVariantToLong(value);
	 m_value = v;
//    SetValueToUnspecified();	keby hodnota nesedela tak by sa malo volat asi toto
}

// -----------------------------------------------------------------------
wxPGVariant wxIntSliderPropertyClass::DoGetValue() const
{
    return wxPGVariant(m_value);
}

// -----------------------------------------------------------------------
wxString wxIntSliderPropertyClass::GetValueAsString( int argFlags ) const
{
	return ItoA(m_value);
}

// -----------------------------------------------------------------------
int wxIntSliderPropertyClass::GetChoiceInfo( wxPGChoiceInfo* choiceinfo )
{
    return m_value;
}

// -----------------------------------------------------------------------
bool wxIntSliderPropertyClass::SetValueFromString( const wxString& text, int /*argFlags*/ )
{
    if ( text.length() == 0 )
    {
        SetValueToUnspecified();
        return true;
    }

     DoSetValue( (long) AtoI(text) );
     return true;
}

// -----------------------------------------------------------------------
bool wxIntSliderPropertyClass::SetValueFromInt( long value, int )
{
	m_value =  value;
   return true;
}

// -----------------------------------------------------------------------
void wxIntSliderPropertyClass::SetAttribute( int id, wxVariant& value )
{
    int ival = value.GetLong();
}





// -----------------------------------------------------------------------
// wxPGIntSliderEditor
// -----------------------------------------------------------------------

WX_PG_IMPLEMENT_EDITOR_CLASS(IntSlider,wxPGIntSliderEditor,wxPGEditor)

// -----------------------------------------------------------------------
wxWindow* wxPGIntSliderEditor::CreateControls( wxPropertyGrid* propgrid, wxPGProperty* property, const wxPoint& pos, const wxSize& sz, wxWindow** psecondary ) const
{
	wxPGVariant val = property->DoGetValue();
	wxValidator* validator = property->GetValidator();
	assert(validator);
	IntSliderValidator* IntValid = dynamic_cast<IntSliderValidator*>(validator);
	assert(IntValid);

	//minimalnu a maximalnu hodnotu pouzijeme z validatoru pretoze inak neviem ako to sem pretlacit
	int MinValue = IntValid->GetMinValue();
	int MaxValue = IntValid->GetMaxValue();

//	wxPoint SliderPos(pos.x, pos.y - sz.y);
//	wxSize SliderSize(sz.x, sz.y * 2);
	wxPoint SliderPos(pos);
	wxSize SliderSize(sz);

	SmartSlider* slider = new SmartSlider(propgrid, wxPG_SUBID2, val.GetLong(), MinValue, MaxValue, SliderPos, SliderSize, 0, SSS_CURRENT_VALUE_LEFT);
	slider->SetBackgroundColour(propgrid->GetPropertyBackgroundColour(property->GetId()));
	slider->SetFont(propgrid->GetFont());
//	wxSlider* slider = new wxSlider(propgrid, wxPG_SUBID2, val.GetLong(), MinValue, MaxValue, pos, sz, wxSL_HORIZONTAL | wxSL_AUTOTICKS, wxDefaultValidator);

    slider->Connect( wxPG_SUBID2, wxEVT_SMART_SLIDER_MOVE_END,
            (wxObjectEventFunction) (wxEventFunction) (wxCommandEventFunction)
            &wxPropertyGrid::OnCustomEditorEvent, NULL, propgrid );

	wxMouseState MouseState = ::wxGetMouseState();
	if(MouseState.LeftDown())
	{
		wxPoint ClickPos = slider->ScreenToClient(::wxGetMousePosition());
		wxRect SliderRect(wxPoint(0, 0), SliderSize);

		if(SliderRect.Contains(ClickPos))
			slider->SimulateLMBdown(ClickPos);
	}

	return slider;
}

// -----------------------------------------------------------------------
void wxPGIntSliderEditor::DrawValue( wxDC& dc, wxPGProperty* property, const wxRect& rect ) const
{
//	wxString StrVal = property->GetValueAsString();
//	dc.DrawText(StrVal, rect.x, rect.y);

	wxPGVariant val = property->DoGetValue();
	wxValidator* validator = property->GetValidator();
	assert(validator);
	IntSliderValidator* IntValid = dynamic_cast<IntSliderValidator*>(validator);
	assert(IntValid);

	//minimalnu a maximalnu hodnotu pouzijeme z validatoru pretoze inak neviem ako to sem pretlacit
	int MinValue = IntValid->GetMinValue();
	int MaxValue = IntValid->GetMaxValue();
	wxRect SliderRect = rect;
	SliderRect.y -= 2;
	SliderRect.SetHeight(SliderRect.GetHeight() + 4);
	SliderRect.x += 2;
	SliderRect.SetWidth(SliderRect.GetWidth() - 2);

	SmartSlider::DrawInt(dc, SliderRect, val.GetLong(), MinValue, MaxValue, true, false);
//	if ( !(property->GetFlags() & wxPG_PROP_UNSPECIFIED) )	//ked je ako keby bez hodnoty zrejme
}

// -----------------------------------------------------------------------
void wxPGIntSliderEditor::UpdateControl( wxPGProperty* property, wxWindow* ctrl ) const
{
    wxASSERT( ctrl );
//    ((wxSimpleCheckBox*)ctrl)->m_state = property->GetChoiceInfo((wxPGChoiceInfo*)NULL);
 //   ctrl->Refresh();
}

// -----------------------------------------------------------------------
bool wxPGIntSliderEditor::OnEvent( wxPropertyGrid* propGrid, wxPGProperty* property,
    wxWindow* ctrl, wxEvent& event ) const
{
    if ( event.GetEventType() == wxEVT_SMART_SLIDER_MOVE_END )
    {
        if ( CopyValueFromControl( property, ctrl ) )
		  {
           return true;
		  }

	     propGrid->EditorsValueWasNotModified();

//		  propGrid->PropertyWasModified(property);	//toto tu nema byt ale potreba zavolat ked sa property zmenila

//        CLEAR_PROPERTY_UNSPECIFIED_FLAG(property);
        return true;
    }
    return false;
}

// -----------------------------------------------------------------------
bool wxPGIntSliderEditor::CopyValueFromControl( wxPGProperty* property, wxWindow* ctrl ) const
{
	SmartSlider* slider = (SmartSlider*)ctrl;
	property->SetValueFromInt(slider->GetIntValue());
   return true;
}

// -----------------------------------------------------------------------
void wxPGIntSliderEditor::SetControlIntValue( wxWindow* ctrl, int value ) const
{
	int neco = 0;
 //   if ( value != 0 ) value = 1;
//    ((wxSimpleCheckBox*)ctrl)->m_state = value;
//    ctrl->Refresh();
}

// -----------------------------------------------------------------------
void wxPGIntSliderEditor::SetValueToUnspecified( wxWindow* ctrl ) const	//do controlu je potreba vrazit "ziadnu" hodnotu
{
	int neco = 0;
//    ((wxSimpleCheckBox*)ctrl)->m_state = 0;
//    ctrl->Refresh();
}

// -----------------------------------------------------------------------
wxPGIntSliderEditor::~wxPGIntSliderEditor() { }



















// -----------------------------------------------------------------------
// wxFloatSliderProperty
// -----------------------------------------------------------------------
wxPG_BEGIN_PROPERTY_CLASS_BODY2(wxFloatSliderPropertyClass,wxPGProperty,double,double,double,class)
    WX_PG_DECLARE_BASIC_TYPE_METHODS()
    WX_PG_DECLARE_CHOICE_METHODS()
    WX_PG_DECLARE_ATTRIBUTE_METHODS()
wxPG_END_PROPERTY_CLASS_BODY()

// We cannot use standard WX_PG_IMPLEMENT_PROPERTY_CLASS macro, since
// there is a custom GetEditorClass.

WX_PG_IMPLEMENT_CONSTFUNC(wxFloatSliderProperty,double)
WX_PG_IMPLEMENT_CLASSINFO(wxFloatSliderProperty,wxBasePropertyClass)
wxPG_GETCLASSNAME_IMPLEMENTATION(wxFloatSliderProperty)
wxPG_VALUETYPE_MSGVAL wxFloatSliderPropertyClass::GetValueType() const { return wxPG_VALUETYPE(double); }

const wxChar* wxPG_ClassName_wxFloatSliderProperty = wxFloatSliderProperty_ClassName;

// -----------------------------------------------------------------------
const wxPGEditor* wxFloatSliderPropertyClass::DoGetEditorClass() const
{
	return wxPG_EDITOR(FloatSlider);
}

// -----------------------------------------------------------------------
wxFloatSliderPropertyClass::wxFloatSliderPropertyClass( const wxString& label, const wxString& name, double value ) :
    wxPGProperty(label,name)
{
	wxPGRegisterEditorClass(FloatSlider);

    DoSetValue((double)value);
    m_flags |= wxPG_PROP_USE_DCC;
}

// -----------------------------------------------------------------------
wxFloatSliderPropertyClass::~wxFloatSliderPropertyClass() { }

// -----------------------------------------------------------------------
void wxFloatSliderPropertyClass::DoSetValue( wxPGVariant value )
{
    double v = wxPGVariantToDouble(value);
	 m_value = v;
//    SetValueToUnspecified();	keby hodnota nesedela tak by sa malo volat asi toto
}

// -----------------------------------------------------------------------
wxPGVariant wxFloatSliderPropertyClass::DoGetValue() const
{
    return wxPGVariant(m_value);
}

// -----------------------------------------------------------------------
wxString wxFloatSliderPropertyClass::GetValueAsString( int argFlags ) const
{
	return FtoA(m_value);
}

// -----------------------------------------------------------------------
int wxFloatSliderPropertyClass::GetChoiceInfo( wxPGChoiceInfo* choiceinfo )
{
    return m_value;
}

// -----------------------------------------------------------------------
bool wxFloatSliderPropertyClass::SetValueFromString( const wxString& text, int /*argFlags*/ )
{
    if ( text.length() == 0 )
    {
        SetValueToUnspecified();
        return true;
    }

     DoSetValue( AtoF(text) );
     return true;
}

// -----------------------------------------------------------------------
bool wxFloatSliderPropertyClass::SetValueFromInt( long value, int )
{
	m_value =  value;
   return true;
}

// -----------------------------------------------------------------------
void wxFloatSliderPropertyClass::SetAttribute( int id, wxVariant& value )
{
    int ival = value.GetLong();
}





// -----------------------------------------------------------------------
// wxPGFloatSliderEditor
// -----------------------------------------------------------------------

WX_PG_IMPLEMENT_EDITOR_CLASS(FloatSlider,wxPGFloatSliderEditor,wxPGEditor)

// -----------------------------------------------------------------------
wxWindow* wxPGFloatSliderEditor::CreateControls( wxPropertyGrid* propgrid, wxPGProperty* property, const wxPoint& pos, const wxSize& sz, wxWindow** psecondary ) const
{
	wxPGVariant val = property->DoGetValue();
	wxValidator* validator = property->GetValidator();
	assert(validator);
	FloatSliderValidator* FloatValid = dynamic_cast<FloatSliderValidator*>(validator);
	assert(FloatValid);

	//minimalnu a maximalnu hodnotu pouzijeme z validatoru pretoze inak neviem ako to sem pretlacit
	float MinValue = FloatValid->GetMinValue();
	float MaxValue = FloatValid->GetMaxValue();
	float step = FloatValid->GetStep();

//	wxPoint SliderPos(pos.x, pos.y - sz.y);
//	wxSize SliderSize(sz.x, sz.y * 2);
	wxPoint SliderPos(pos);
	wxSize SliderSize(sz);

	SmartSlider* slider = new SmartSlider(propgrid, wxPG_SUBID2, (float)val.GetDouble(), MinValue, MaxValue, step, SliderPos, SliderSize, 0, SSS_CURRENT_VALUE_LEFT);
	slider->SetBackgroundColour(propgrid->GetPropertyBackgroundColour(property->GetId()));
	slider->SetFont(propgrid->GetFont());
//	wxSlider* slider = new wxSlider(propgrid, wxPG_SUBID2, val.GetLong(), MinValue, MaxValue, pos, sz, wxSL_HORIZONTAL | wxSL_AUTOTICKS, wxDefaultValidator);

   slider->Connect( wxPG_SUBID2, wxEVT_SMART_SLIDER_MOVE_END,
            (wxObjectEventFunction) (wxEventFunction) (wxCommandEventFunction)
            &wxPropertyGrid::OnCustomEditorEvent, NULL, propgrid );


	wxMouseState MouseState = ::wxGetMouseState();
	if(MouseState.LeftDown())
	{
		wxPoint ClickPos = slider->ScreenToClient(::wxGetMousePosition());
		wxRect SliderRect(wxPoint(0, 0), SliderSize);

		if(SliderRect.Contains(ClickPos))
			slider->SimulateLMBdown(ClickPos);
	}

	return slider;
}

// -----------------------------------------------------------------------
void wxPGFloatSliderEditor::DrawValue( wxDC& dc, wxPGProperty* property, const wxRect& rect ) const
{
//	wxString StrVal = property->GetValueAsString();
//	dc.DrawText(StrVal, rect.x, rect.y);

	wxPGVariant val = property->DoGetValue();
	wxValidator* validator = property->GetValidator();
	assert(validator);
	FloatSliderValidator* FloatValid = dynamic_cast<FloatSliderValidator*>(validator);
	assert(FloatValid);

	//minimalnu a maximalnu hodnotu pouzijeme z validatoru pretoze inak neviem ako to sem pretlacit
	float MinValue = FloatValid->GetMinValue();
	float MaxValue = FloatValid->GetMaxValue();
	wxRect SliderRect = rect;
	SliderRect.y -= 2;
	SliderRect.SetHeight(SliderRect.GetHeight() + 4);
	SliderRect.x += 2;
	SliderRect.SetWidth(SliderRect.GetWidth() - 2);

	SmartSlider::DrawFloat(dc, SliderRect, val.GetDouble(), MinValue, MaxValue, true, 2, false);
//	if ( !(property->GetFlags() & wxPG_PROP_UNSPECIFIED) )	//ked je ako keby bez hodnoty zrejme
}

// -----------------------------------------------------------------------
void wxPGFloatSliderEditor::UpdateControl( wxPGProperty* property, wxWindow* ctrl ) const
{
    wxASSERT( ctrl );
//    ((wxSimpleCheckBox*)ctrl)->m_state = property->GetChoiceInfo((wxPGChoiceInfo*)NULL);
 //   ctrl->Refresh();
}

// -----------------------------------------------------------------------
bool wxPGFloatSliderEditor::OnEvent( wxPropertyGrid* propGrid, wxPGProperty* property,
    wxWindow* ctrl, wxEvent& event ) const
{
    if ( event.GetEventType() == wxEVT_SMART_SLIDER_MOVE_END )
    {
        if ( CopyValueFromControl( property, ctrl ) )
		  {
            return true;
		  }

	     propGrid->EditorsValueWasNotModified();
//        CLEAR_PROPERTY_UNSPECIFIED_FLAG(property);
        return true;
    }
    return false;
}

// -----------------------------------------------------------------------
bool wxPGFloatSliderEditor::CopyValueFromControl( wxPGProperty* property, wxWindow* ctrl ) const
{
	SmartSlider* slider = (SmartSlider*)ctrl;
	float fff = (float)slider->GetFloatValue();
	property->DoSetValue(wxPGVariant((float)slider->GetFloatValue()));
   return true;
}

// -----------------------------------------------------------------------
void wxPGFloatSliderEditor::SetControlIntValue( wxWindow* ctrl, int value ) const
{
	int neco = 0;
 //   if ( value != 0 ) value = 1;
//    ((wxSimpleCheckBox*)ctrl)->m_state = value;
//    ctrl->Refresh();
}

// -----------------------------------------------------------------------
void wxPGFloatSliderEditor::SetValueToUnspecified( wxWindow* ctrl ) const	//do controlu je potreba vrazit "ziadnu" hodnotu
{
	int neco = 0;
//    ((wxSimpleCheckBox*)ctrl)->m_state = 0;
//    ctrl->Refresh();
}

// -----------------------------------------------------------------------
wxPGFloatSliderEditor::~wxPGFloatSliderEditor() { }
