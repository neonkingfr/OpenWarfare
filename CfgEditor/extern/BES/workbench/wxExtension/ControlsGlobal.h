
#ifndef CONTROLS_GLOBAL_H
#define CONTROLS_GLOBAL_H

#include "wx/string.h"

enum	//datove typy pre handling inputu
{
	VarType_Int = 0,
	VarType_Float,
	VarType_Bool,
	VarType_String,
	VarType_Vector,
};

//--------------------------------------------------------------------------------------
wxString		CorrectizeStringFromInput(wxString str, int InputType);
void			ClampIntValue(wxString& val, wxString& MinVal, wxString& MaxVal);
void			ClampFloatValue(wxString& val, wxString& MinVal, wxString& MaxVal);

#endif