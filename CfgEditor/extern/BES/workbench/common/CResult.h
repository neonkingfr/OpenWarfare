
#ifndef CRESULT_H
#define CRESULT_H

//===============================================================================
class CResult
{
public:
					CResult();
					CResult(bool result, const char* comment = "");
					~CResult();
	void			Set(bool result, const char* comment = "");
	inline bool IsOk() const {return result;}
	const	char* GetComment() const;
	bool			IsCommented() const;
	void			SetResult(bool result);
	void			SetComment(const char* comment);
	void			operator= (const CResult& res);
	void			operator= (bool res);
	bool			operator== (const CResult& res);
	bool			operator== (bool res);
	bool			operator!= (const CResult& res);
	bool			operator!= (bool res);
private:
	bool			result;
	char			comment[512];
};

#endif