
#include "wbPrecomp.h"
#include "XMLConfig.h"
#include "tinyxml/tinyxml.h"
#include "Common/NativeConvert.h"
#include "Common/WxConvert.h"

#ifdef WORKBENCH
	#include "enf_vfilesystem.h"
#endif

//--------------------------------------------------------------------------------------------------
XMLConfigNode::XMLConfigNode(XMLConfig* config, const char* path, TiXmlElement* element)
{
	assert(strlen(path) > 0);
	this->config = config;
	this->element = element;
	this->path = new char[strlen(path) + 1];
	strcpy(this->path, path);
}

//--------------------------------------------------------------------------------------------------
XMLConfigNode::~XMLConfigNode()
{
	SAFE_DELETE_ARRAY(this->path);
}

//--------------------------------------------------------------------------------------------------
const char*	XMLConfigNode::GetPath()
{
	return path;
}

//--------------------------------------------------------------------------------------------------
const char*	XMLConfigNode::GetName()
{
	return element->Value();
}

//--------------------------------------------------------------------------------------------------
XMLConfigNode*	XMLConfigNode::GetChildNode(const char* name, bool CreateIfNotExist)
{
	assert(name);
	assert(strlen(name) > 0);
	char NodePath[256];
	strcpy(NodePath, this->path);
	strcat(NodePath, "/");
	strcat(NodePath, name);
	XMLConfigNode* node = NULL;//config->FindRequestedNode(NodePath);

//	if(node)
//		return node;

	TiXmlElement* elm = config->FindChildElement(element, name);

	if(!elm && CreateIfNotExist == false)
		return NULL;

	if(!elm)
		elm = config->InsertChildElement(element, name);

	node = new XMLConfigNode(config, NodePath, elm);
	config->RequestedNodes.Insert(node);
	return node;
}

//--------------------------------------------------------------------------------------------------
XMLConfigNode*	XMLConfigNode::GetChildNodeWithAttr(const char* name, const char* AttrName, const char* AttrValue, bool CreateIfNotExist)
{
	assert(name);
	assert(strlen(name) > 0);
	assert(AttrValue);
	char NodePath[256];
	strcpy(NodePath, this->path);
	strcat(NodePath, "/");
	strcat(NodePath, name);
	XMLConfigNode* node = NULL;/* = config->FindRequestedNode(NodePath);

	if(node)
		return node;*/

	TiXmlElement* elm = config->FindChildElementWithAttr(element, name, AttrName, AttrValue); 

	if(!elm && CreateIfNotExist == false)
		return NULL;

	if(!elm)
	{
		elm = config->InsertChildElement(element, name);
		elm->SetAttribute(AttrName, AttrValue);
	}

	node = new XMLConfigNode(config, NodePath, elm);
	config->RequestedNodes.Insert(node);
	return node;
}

//--------------------------------------------------------------------------------------------------
XMLConfigNode*	XMLConfigNode::GetFirstChildNode()
{
	TiXmlNode* ChildNode = element->FirstChild();

	if(!ChildNode)
		return NULL;

	TiXmlElement* elm = ChildNode->ToElement();

	if(!elm)
		return NULL;

	char NodePath[256];
	strcpy(NodePath, this->path);
	strcat(NodePath, "/");
	strcat(NodePath, elm->Value());

	XMLConfigNode* node = NULL;//config->FindRequestedNode(NodePath);

//	if(node)
//		return node;

	node = new XMLConfigNode(config, NodePath, elm);
	config->RequestedNodes.Insert(node);
	return node;
}

//--------------------------------------------------------------------------------------------------
XMLConfigNode*	XMLConfigNode::GetNextSiblingNode()
{
	TiXmlElement* elm = element->NextSiblingElement();

	if(!elm)
		return NULL;

	char NodePath[256];
	strcpy(NodePath, this->path);

	int n;
	for(n = strlen(NodePath) - 1; n >= 0; n--)
	{
		if(NodePath[n] == '/')
			break;
	}

	if(n != 0)
	{
		NodePath[n] = '/';
		NodePath[n + 1] = 0;
	}
	else
		NodePath[0] = 0;

	strcat(NodePath, elm->Value());
	XMLConfigNode* node = new XMLConfigNode(config, NodePath, elm);
	config->RequestedNodes.Insert(node);
	return node;
}

//--------------------------------------------------------------------------------------------------
XMLConfigNode*	XMLConfigNode::AppendChildNode(const char* name)
{
	assert(name);
	assert(strlen(name) > 0);
	char NodePath[256];
	strcpy(NodePath, this->path);
	strcat(NodePath, "/");
	strcat(NodePath, name);
	XMLConfigNode* node = NULL;//config->FindRequestedNode(NodePath);	//nesmie to tu byt pretoze tam moze byt element s rovnakym nazmom

//	if(node)
//		return node;

	TiXmlElement* elm = config->InsertChildElement(element, name);
	assert(elm);

	node = new XMLConfigNode(config, NodePath, elm);
	config->RequestedNodes.Insert(node);
	return node;
}

//--------------------------------------------------------------------------------------------------
int XMLConfigNode::RemoveChildNodes()
{
	XMLConfigNode*	node = GetFirstChildNode();
	int count = 0;

	while(node)
	{
		element->RemoveChild(node->element);
		config->RequestedNodes.Remove(node);
		delete node;
		count++;
		node = GetFirstChildNode();
	}
	return count;
}

//--------------------------------------------------------------------------------------------------
const char* XMLConfigNode::GetStringValue(const char* property)
{
	TiXmlElement* elm = config->FindChildElement(element, property);

	if(!elm)
		return NULL;

	return elm->Attribute("value");
}

//--------------------------------------------------------------------------------------------------
int XMLConfigNode::GetIntValue(const char* property)
{
	const char* res = GetStringValue(property);

	if(res)
		return taAtoI(res);

	return 0;
}

//--------------------------------------------------------------------------------------------------
float XMLConfigNode::GetFloatValue(const char* property)
{
	const char* res = GetStringValue(property);

	if(res)
		return taAtoF(res);

	return 0.0f;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::GetValue(const char* property, char* value)
{
	TiXmlElement* elm = config->FindChildElement(element, property);

	if(!elm)
		return false;

	strcpy(value, elm->Attribute("value"));
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::GetValue(const char* property, int& val)
{
	const char* res = GetStringValue(property);

	if(!res)
		return false;

	val = taAtoI(res);
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::GetValue(const char* property, float& val)
{
	const char* res = GetStringValue(property);

	if(!res)
		return false;
	
	val = taAtoF(res);
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::GetValue(const char* property, bool& val)
{
	const char* res = GetStringValue(property);

	if(!res)
		return false;

	val = (taAtoI(res) != 0);
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::GetValue(const char* property, wxString& val)
{
	const char* res = GetStringValue(property);

	if(!res)
		return false;

	val = res;
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::GetValue(const char* property, Vector3& val)
{
	const char* res = GetStringValue(property);

	if(!res)
		return false;

	val = StrToVec(wxString(res));
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::GetAttributeValue(const char* attribute, char* value)
{
	strcpy(value, element->Attribute("value"));
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::GetAttributeValue(const char* attribute, int& val)
{
	const char* res = element->Attribute(attribute);

	if(!res)
		return false;

	val = taAtoI(res);
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::GetAttributeValue(const char* attribute, float& val)
{
	const char* res = element->Attribute(attribute);

	if(!res)
		return false;
	
	val = taAtoF(res);
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::GetAttributeValue(const char* attribute, bool& val)
{
	const char* res = element->Attribute(attribute);

	if(!res)
		return false;

	val = (taAtoI(res) != 0);
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::GetAttributeValue(const char* attribute, wxString& val)
{
	const char* res = element->Attribute(attribute);

	if(!res)
		return false;

	val = res;
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::GetAttributeValue(const char* attribute, Vector3& val)
{
	const char* res = element->Attribute(attribute);

	if(!res)
		return false;

	val = StrToVec(wxString(res));
	return true;
}



//--------------------------------------------------------------------------------------------------
void XMLConfigNode::SetAttributeValue(const char* attribute, char* value)
{
	element->SetAttribute(attribute, value);
}

//--------------------------------------------------------------------------------------------------
void XMLConfigNode::SetAttributeValue(const char* attribute, int& val)
{
	element->SetAttribute(attribute, val);
}

//--------------------------------------------------------------------------------------------------
void XMLConfigNode::SetAttributeValue(const char* attribute, float& val)
{
	element->SetAttribute(attribute, FtoA(val).c_str());
}

//--------------------------------------------------------------------------------------------------
void XMLConfigNode::SetAttributeValue(const char* attribute, bool& val)
{
	element->SetAttribute(attribute, taBtoI(val));
}

//--------------------------------------------------------------------------------------------------
void XMLConfigNode::SetAttributeValue(const char* attribute, wxString& val)
{
	element->SetAttribute(attribute, val.c_str());
}

//--------------------------------------------------------------------------------------------------
void XMLConfigNode::SetAttributeValue(const char* attribute, Vector3& val)
{
	element->SetAttribute(attribute, VecToStr(val).c_str());
}



//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::SetValue(const char* property, const char* value)
{
	TiXmlElement* elm = config->FindChildElement(element, property);

	if(!elm)
		elm = config->InsertChildElement(element, property);

	if(!elm)
		return false;

	elm->SetAttribute("value", value);
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::SetValue(const char* property, int val)
{
	TiXmlElement* elm = config->FindChildElement(element, property);

	if(!elm)
		elm = config->InsertChildElement(element, property);

	if(!elm)
		return false;

	elm->SetAttribute("value", val);
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::SetValue(const char* property, float val)
{
	TiXmlElement* elm = config->FindChildElement(element, property);

	if(!elm)
		elm = config->InsertChildElement(element, property);

	if(!elm)
		return false;

	char temp[128];
	taFtoA(temp, val);
	elm->SetAttribute("value", temp);
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::SetValue(const char* property, bool val)
{
	return SetValue(property, taBtoI(val));
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::SetValue(const char* property, wxString& val)
{
	TiXmlElement* elm = config->FindChildElement(element, property);

	if(!elm)
		elm = config->InsertChildElement(element, property);

	if(!elm)
		return false;

	elm->SetAttribute("value", val.c_str());
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfigNode::SetValue(const char* property, Vector3& val)
{
	TiXmlElement* elm = config->FindChildElement(element, property);

	if(!elm)
		elm = config->InsertChildElement(element, property);

	if(!elm)
		return false;

	elm->SetAttribute("value", VecToStr(val).c_str());
	return true;
}

//--------------------------------------------------------------------------------------------------
XMLConfig::XMLConfig(const char* file, const char* RootElementName)
{
	if(RootElementName == NULL)
		RootElementName = "Config";

	strcpy(FileName, file);
#ifdef WORKBENCH
	FSystem = new FileSystem(); 
#endif
	doc = new TiXmlDocument(file);

	TiXmlElement RootElm(RootElementName);
	TiXmlElement* root = doc->InsertEndChild(RootElm)->ToElement();
	root->SetAttribute("version", "1.0");
}

//--------------------------------------------------------------------------------------------------
XMLConfig::~XMLConfig()
{
	for(uint n = 0; n < RequestedNodes.GetCardinality(); n++)
		delete RequestedNodes[n];

	SAFE_DELETE(doc);
#ifdef WORKBENCH
	FSystem->Release();
	FSystem = NULL;
#endif
}

//--------------------------------------------------------------------------------------------------
bool XMLConfig::Load()
{
	assert(doc);
#ifdef WORKBENCH
	VFile* fl = FSystem->GlobalOpen(FileName, FILEMODE_READ); 

	if(!fl)
		return false;

	if(!doc->LoadFile(fl))
	{
		fl->Close();
		return false;
	}

	fl->Close();	//zaroven sa maze
#else
	if(!doc->LoadFile(FileName))
		return false;
#endif

	TiXmlElement* root = doc->RootElement();
	assert(root);
//	PathsHash.Clear();
//	ParseElementPaths(root, NULL);
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfig::Save()
{
#ifdef WORKBENCH
	VFile* fl = FSystem->GlobalOpen(FileName, FILEMODE_WRITE);

	if(!fl)
		return false;

	bool ret = doc->SaveFile(fl);
	fl->Close();	//fl sa maze
	return ret;
#else
	bool ret = doc->SaveFile(FileName);
	assert(ret);
	return ret;
#endif
}

//--------------------------------------------------------------------------------------------------
XMLConfigNode*	XMLConfig::FindRequestedNode(const char* path)
{
	for(uint n = 0; n < RequestedNodes.GetCardinality(); n++)
	{
		if(strcmp(RequestedNodes[n]->GetPath(), path) == 0)
			return RequestedNodes[n];
	}
	return NULL;
}

//--------------------------------------------------------------------------------------------------
XMLConfigNode*	XMLConfig::GetNode(const char* path, bool CreateIfNotExist)
{
	XMLConfigNode* node = NULL;//FindRequestedNode(path);

//	if(node)
//		return node;

	TiXmlElement* elm = FindElement(path);

	if(!elm && CreateIfNotExist == false)
		return NULL;

	if(!elm)
		elm = InsertEmptyPath(path);

	assert(elm);
	node = new XMLConfigNode(this, path, elm);
	RequestedNodes.Insert(node);
	return node;
}

//--------------------------------------------------------------------------------------------------
XMLConfigNode*	XMLConfig::AddNode(const char* path)
{
	XMLConfigNode* node = FindRequestedNode(path);

	if(node)
		return node;

	TiXmlElement* elm = InsertEmptyPath(path);
	assert(elm);

	node = new XMLConfigNode(this, path, elm);
	RequestedNodes.Insert(node);
	return node;
}

//--------------------------------------------------------------------------------------------------
const char* XMLConfig::GetStringValue(const char* path)
{
//	TiXmlElement* PathElm = PathsHash.Find(path);	//FIXME nechapem preco nefunguje. niektore stringy nechce insertnut. zrejme mu vadia podobne stringy
	TiXmlElement* PathElm = FindElement(path);

	if(!PathElm)
		return NULL;

	return PathElm->Attribute("value");
}

//--------------------------------------------------------------------------------------------------
int XMLConfig::GetIntValue(const char* path)
{
	const char* res = GetStringValue(path);

	if(res)
		return taAtoI(res);

	return 0;
}

//--------------------------------------------------------------------------------------------------
float XMLConfig::GetFloatValue(const char* path)
{
	const char* res = GetStringValue(path);

	if(res)
		return taAtoF(res);

	return 0.0f;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfig::SetValue(const char* path, const char* value)
{
//	TiXmlElement* PathElm = PathsHash.Find(path);
	TiXmlElement* PathElm = FindElement(path);

	if(!PathElm)
		PathElm = InsertEmptyPath(path);

	assert(PathElm);
	PathElm->SetAttribute("value", value);
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfig::SetValue(const char* path, int val)
{
//	TiXmlElement* PathElm = PathsHash.Find(path);
	TiXmlElement* PathElm = FindElement(path);

	if(!PathElm)
		PathElm = InsertEmptyPath(path);

	assert(PathElm);
	PathElm->SetAttribute("value", val);
	return true;
}

//--------------------------------------------------------------------------------------------------
bool XMLConfig::SetValue(const char* path, float val)
{
//	TiXmlElement* PathElm = PathsHash.Find(path);
	TiXmlElement* PathElm = FindElement(path);

	if(!PathElm)
		PathElm = InsertEmptyPath(path);

	assert(PathElm);
	char temp[128];
	taFtoA(temp, val);
	PathElm->SetAttribute("value", temp);
	return true;
}

//--------------------------------------------------------------------------------------------------
TiXmlElement* XMLConfig::FindChildElement(TiXmlElement* parent, const char* name)
{
	for(TiXmlNode* node = parent->FirstChild(); node; node = node->NextSibling())
	{
		TiXmlElement* elm = node->ToElement();

		if(elm && strcmp(elm->Value(), name) == 0)
		{
			return elm;
			break;
		}
	}
	return NULL;
}

//--------------------------------------------------------------------------------------------------
TiXmlElement* XMLConfig::FindChildElementWithAttr(TiXmlElement* parent, const char* name, const char* AttrName, const char* AttrValue )
{
	assert(AttrName != NULL);
	assert(AttrValue != NULL);

	for(TiXmlNode* node = parent->FirstChild(); node; node = node->NextSibling())
	{
		TiXmlElement* elm = node->ToElement();

		if(elm && strcmp(elm->Value(), name) == 0)
		{
			const char* attr = elm->Attribute(AttrName);

			if(attr && strcmp(attr, AttrValue) == 0)
				return elm;
		}
	}
	return NULL;
}

//--------------------------------------------------------------------------------------------------
TiXmlElement* XMLConfig::InsertChildElement(TiXmlElement* parent, const char* name)
{
	assert(parent);
	TiXmlElement elm(name);
	return parent->InsertEndChild(elm)->ToElement();
}

//--------------------------------------------------------------------------------------------------
TiXmlElement* XMLConfig::FindElement(const char* path)
{
	assert(path);
	int length = strlen(path);
	char CurDir[256];
	int CurDirNum = 0;
	TiXmlElement* element = doc->RootElement();

	for(int n = 0; n < length + 1; n++)
	{
		char ch = path[n];

		if(ch == '/' || ch == 0)
		{
			CurDir[CurDirNum] = 0;
			TiXmlElement* CurDirElement = FindChildElement(element, CurDir);

			if(!CurDirElement)
				return NULL;

			element = CurDirElement;
			CurDirNum = 0;
		}
		else
			CurDir[CurDirNum++] = ch;
	}
	return element;
}

//--------------------------------------------------------------------------------------------------
TiXmlElement* XMLConfig::InsertEmptyPath(const char* path)
{
	assert(path);
	int length = strlen(path);
	char CurDir[256];
	int CurDirNum = 0;
	TiXmlElement* element = doc->RootElement();

	for(int n = 0; n < length + 1; n++)
	{
		char ch = path[n];

		if(ch == '/' || ch == 0)
		{
			CurDir[CurDirNum] = 0;
			TiXmlElement* CurDirElement = FindChildElement(element, CurDir);

			if(!CurDirElement)
			{
				TiXmlElement NewElm(CurDir);
				CurDirElement = element->InsertEndChild(NewElm)->ToElement();
			}

			element = CurDirElement;
			CurDirNum = 0;
		}
		else
			CurDir[CurDirNum++] = ch;
	}
	return element;
}
/*
//--------------------------------------------------------------------------------------------------
void XMLConfig::ParseElementPaths(TiXmlElement* element, const char* RootPath)
{
	char CurPath[256];

	for(TiXmlNode* node = element->FirstChild(); node; node = node->NextSibling())
	{
		const char* Node = node->Value();

		TiXmlElement* CurElement = node->ToElement();

		if(CurElement)
		{
			const char* dir = CurElement->Value();

			if(dir)
			{
				if(RootPath)
				{
					strcpy(CurPath, RootPath);
					strcat(CurPath, "/");
					strcat(CurPath, dir);
				}
				else
					strcpy(CurPath, dir);

				bool exist = PathsHash.Contains(CurPath);

				bool inserted = PathsHash.Insert(CurPath, CurElement);
				assert(inserted);
				ParseElementPaths(CurElement, CurPath);
			}
		}
	}
}
*/