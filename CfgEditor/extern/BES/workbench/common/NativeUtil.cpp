
#include "wbPrecomp.h"
#include "NativeUtil.h"

#include <math.h>
#include <stdio.h>
#include <string>
//#include <string.h>
#include <ctype.h>

const char EndLineChr[3] = {13, 10, '\0'}; 

//----------------------------------------------------------------------------------
bool GetFileExtension(const char* filename, char* result)
{
	int lng = strlen(filename);
	
	if(lng <= 0)
		return false;
		
	int n;
	for(n = lng - 1; n >= 0 ; n--)
	{
		if(filename[n] == '.' && (n < lng - 1))
		{
			n++;
			break;
		}
	}
	
	if(n <= 0)
		return false;
	
	char* tmp = (char*)filename + n;
	strcpy(result, tmp);
	return true;
}

//------------------------------------------------------------------------------
void RemoveQuotes(char* str)
{
	int length = strlen(str);

	if(str[0] == '"')
	{
		for(int n = 1; n < length; n++)
			str[n - 1] = str[n];

		length--;
	}

	if(length <= 0)
		return;

	int LastIndex = length - 1;

	if(str[LastIndex] == '"')
		str[LastIndex] = 0;
}

//------------------------------------------------------------------------------
int ReplaceCharacters(char* str, const char* ToFind, const char* ToReplace)
{
	std::string tmp(str);
	int pos = 0;
	int counter = 0;
	int FindLng = strlen(ToFind);

	if(strcmp(ToFind, EndLineChr) == 0)	//vynimka na hladanie znakov konca lajny.
		FindLng = 1;

	int ReplaceLng = strlen(ToReplace);
	pos = tmp.find_first_of(ToFind, pos);

	while(pos != -1)
	{
		tmp = tmp.erase(pos, FindLng);
		tmp = tmp.insert(pos, ToReplace);
		pos += ReplaceLng;
		pos = tmp.find_first_of(ToFind, pos);
		counter++;
	}
	strcpy(str, tmp.c_str());
	return counter;
}

// ----------------------------------------------------------------------------
float	Fmodulo(float n1, float n2)
{
	float rate = n1 / n2;
	int itate = (int)rate;
	float dif = rate - (float)itate;
	return dif;
}

// ----------------------------------------------------------------------------
float	RoundUp(float f)
{
	int i = (int)f;

	float res = f - (float)i;

	if(f >= 0)
	{
		if(fabs(res) >= 0.525f)
			return (float)i + 1.0f;
	}
	else
	{
		if(fabs(res) >= 0.525f)
			return (float)i - 1.0f;
	}

	return (float)i;
}

// ----------------------------------------------------------------------------
float	RoundUp2(float f)
{
	int i = (int)f;

	float res = f - (float)i;

	if(f >= 0)
	{
		if(fabs(res) >= 0.5f)
			return (float)i + 1.0f;
	}
	else
	{
		if(fabs(res) >= 0.5f)
			return (float)i - 1.0f;
	}

	return (float)i;
}

// ----------------------------------------------------------------------------
double RoundUp2Double(double f)
{
	int i = (int)f;

	double res = f - (double)i;

	if(f >= 0)
	{
		if(fabs(res) >= 0.5f)
			return (double)i + 1.0f;
	}
	else
	{
		if(fabs(res) >= 0.5f)
			return (double)i - 1.0f;
	}

	return (double)i;
}

// ----------------------------------------------------------------------------
void HumanizeFloat(float* fl)
{
	if(*fl < 0.00001f && *fl > -0.00001f)
		*fl = 0.0f;
}

// ----------------------------------------------------------------------------
bool taBeforeLastChar(const char* source, char* destination, const char* ToFind)
{
	const char* Temp = source;
	const char* Result = NULL;

	while(Temp && strlen(Temp) > 0)
	{
		Temp++;
		Result = Temp;
		Temp = strstr(Temp, ToFind);
	}

	if(Result)
	{
		int lng = (Result - source) - 1;
		strncpy(destination, source, lng);
		destination[lng] = 0;
		return true;
	}
	else
	{
		destination = NULL;
		return false;
	}
}

// ----------------------------------------------------------------------------
bool taAfterLastChar(const char* source, char* destination, const char* ToFind)
{
	const char* Temp = source;
	const char* Result = source;

	while(Temp && strlen(Temp) > 0)
	{
		Temp = strstr(Temp, ToFind);

		if(Temp)
			Result = ++Temp;
	}

	strcpy(destination, Result);
	return (Result != source);
}

// ----------------------------------------------------------------------------
void taToLower(char* array)	//existuje CharLower();
{
	int size = strlen(array);

	for(int n = 0; n < size; n++)
		array[n] = tolower(array[n]);
}

//-----------------------------------------------------------------------------------------
void ReverseCharArray(char* s)
{
	int i,l=strlen(s);
	char c;
	for(i=0;i<(l>>1);i++)
	{
		c=s[i];
		s[i]=s[l-i-1];
		s[l-i-1]=c;
	}
}
/*
//-----------------------------------------------------------------------------------------
void CleanFloatString(char* str)
{
	if(strstr(str, ".") == NULL)
		return;

	
	int lng = strlen(str);
	char* result = (char*)_alloca(lng + 1);	//alokuje pamet na zasobniku

	int LastIndex = lng - 1;
	int pos = 0;
	bool LookForNulls = true;

	if(LastIndex <= 0)
		return;

	for(int n = LastIndex; n >= 0; n--)
	{
		if(str[n] == '.')
		{
			if(!LookForNulls)
				result[pos++] = str[n];

			LookForNulls = false;
		}
		else
		{
			if(LookForNulls)
			{
				if(str[n] != '0')
				{
					result[pos++] = str[n];
					LookForNulls = false;
				}
			}
			else
				result[pos++] = str[n];
		}
	}
	result[pos] = 0;
	ReverseCharArray(result);
	strcpy(str, result);
}

//-----------------------------------------------------------------------------------------
void CleanVectorString(char* str)
{
	if(strstr(str, ".") == NULL)
		return;

	int lng = strlen(str);
	char* result = (char*)_alloca(lng + 1);
	char* StartPos = str;
	char* EndPos = strstr(StartPos, " ");
	int TokenLng = EndPos - StartPos;
	bool first = true;

	while(TokenLng > 0)
	{
		{
			char* token = (char*)_alloca(TokenLng + 1);
			strncpy(token, StartPos, TokenLng);
			token[TokenLng] = 0;
			CleanFloatString(token);

			if(first)
			{
				first = false;
				strcpy(result, token);
			}
			else
			{
				strcat(result, " ");
				strcat(result, token);
			}
		}

		StartPos = EndPos + 1;
		EndPos = strstr(StartPos, " ");

		if(EndPos == NULL)
		{
//			StartPos--;
			EndPos = str + lng;
		}

		TokenLng = EndPos - StartPos;
	}
	strcpy(str, result);
}
*/
//-----------------------------------------------------------------------------------------
bool IsNumber(char ch)
{
	if(ch < 48 || ch > 57)
		return false;

	return true;
}

//-----------------------------------------------------------------------------------------
bool IsIntNumber(const char* val)
{
	for(unsigned int n = 0; n < strlen(val); n++)
	{
		if(!IsNumber(val[n]))
		{
			if(n != 0 || val[n] != '-')
				return false;
		}
	}
	return true;
}

//-----------------------------------------------------------------------------------------
bool IsFloatNumber(const char* val)
{
	bool valid;
	char Current;
	float NumPoints = 0;

	for(unsigned int n = 0; n < strlen(val); n++)
	{
		valid = false;
		Current = val[n];

		if(n == 0 && Current == '-')
			valid = true;

		if(n != 0 && Current == '.')
			valid = true;

		if(valid == false && (Current != '-' && IsNumber(Current)))
			valid = true;

		if(valid == false)
			return false;

		if(Current == '.')
			NumPoints++;

		if(NumPoints > 1)
			return false;
	}
	return true;
}

char* stristr(const char* String, const char* Pattern)
{
      char *pptr, *sptr, *start;
      unsigned int  slen, plen;

      for (start = (char *)String,
           pptr  = (char *)Pattern,
           slen  = strlen(String),
           plen  = strlen(Pattern);

           /* while string length not shorter than pattern length */

           slen >= plen;

           start++, slen--)
      {
            /* find start of pattern in string */
            while (toupper(*start) != toupper(*Pattern))
            {
                  start++;
                  slen--;

                  /* if pattern longer than string */

                  if (slen < plen)
                        return(NULL);
            }

            sptr = start;
            pptr = (char *)Pattern;

            while (toupper(*sptr) == toupper(*pptr))
            {
                  sptr++;
                  pptr++;

                  /* if end of pattern then pattern was found */

                  if ('\0' == *pptr)
                        return (start);
            }
      }
      return(NULL);
}
/*
//----------------------------------------------------------------------------------
#pragma warning(disable : 4035) // no return value

char* __fastcall stristr(const char* pszMain, const char* pszSub)
{
    pszMain;    // compiler thinks these are unreferenced because
    pszSub;     // they are in ecx and edx registers

    char* pszTmp1;
    char* pszTmp2;
    char  lowerch, upperch;

// We keep the first character of pszSub in lowerch and upperch (lower and
// upper case). First we loop trying to find a match for this character. Once
// we have found a match, we start with the second character of both pszMain
// and pszSub and walk through both strings doing a CharLower on both
// characters before comparing. If we make it all the way through pszSub with
// matches, then we bail with a pointer to the string's location in pszMain.

    _asm {
        mov esi, ecx    // pszMain
        mov edi, edx    // pszSub

        // Check for NULL pointers

        test ecx, ecx
        je short NoMatch // NULL pointer for pszMain
        test edx, edx
        je short NoMatch // NULL pointer for pszSub

        sub eax, eax
        mov al, [edi]
        push eax
        call DWORD PTR CharLower
        mov lowerch, al
        push eax
        call DWORD PTR CharUpper
        mov upperch, al

        push edi    // increment the second string pointer
        call DWORD PTR CharNext
        mov  edi, eax

        mov pszTmp2, edi
        mov edi, DWORD PTR CharNext // faster to call through a register

Loop1:
        mov al, [esi]
        test al, al
        je short NoMatch        // end of main string, so no match
        cmp al, lowerch
        je short CheckString    // lowercase match?
        cmp al, upperch
        je short CheckString    // upppercase match?
        push esi
        call edi                // Call CharNext to update main string pointer
        mov esi, eax
        jmp short Loop1

CheckString:
        mov pszTmp1, esi    // save current pszMain pointer in case its a match
        push esi
        call edi            // first character of both strings match,
        mov  esi, eax       // so move to next pszMain character
        mov edi, pszTmp2
        mov al, [edi]
        jmp short Branch1

Loop3:
        push esi
        call DWORD PTR CharNext    // CharNext to change pszMain pointer
        mov  esi, eax
        push edi
        call DWORD PTR CharNext    // CharNext to change pszSub pointer
        mov  edi, eax

        mov al, [edi]
Branch1:
        test al, al
        je short Match       // zero in sub string, means we've got a match
        cmp al, [esi]
        je short Loop3

        // Doesn't match, but might be simply a case mismatch. Lower-case both
        // characters and compare again

        sub ecx, ecx
        mov cl, al  // character from pszSub
        push ecx
        call DWORD PTR CharLower
        mov cl, al
        sub eax, eax
        mov al,  [esi]   // character from pszMain
        push ecx    // preserve register
        push eax
        call DWORD PTR CharLower
        pop ecx
        cmp al, cl
        je short Loop3  // we still have a match, keep checking

        // No match, put everything back, update pszMain to the next character
        // and try again from the top

        mov esi, pszTmp1
        mov  edi, DWORD PTR CharNext
        push esi
        call edi
        mov  esi, eax
        jmp short Loop1

Match:
        mov eax, pszTmp1
        jmp short Done  // Don't just return -- always let the C portion of the code handle the return

NoMatch:
        sub eax, eax
Done:
     }

    // Note lack of return in the C portion of the code. Return value is always in
    // eax register which we have set by the time we get here
}
*/

//--------------------------------------------------------------------------------------------------
unsigned int RateOfUint(unsigned int value, double rate)
{
	__int64 rate64 = (__int64)(rate * 1000.0);
	__int64 range = (__int64)value * 1000;
	__int64 result = range * rate64;
	result = result / (1000 * 1000);
	unsigned int rangepos = result;
	return rangepos;
}