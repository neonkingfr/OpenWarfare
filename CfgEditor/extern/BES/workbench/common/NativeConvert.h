
#ifndef NATIVE_CONVERT_H
#define NATIVE_CONVERT_H

#include "SharedApiDef.h"

SHARED_API void			taFtoA(char* target, float val, unsigned char NumFloatUnits = 6);
SHARED_API float			taAtoF(const char* str);
SHARED_API void			taItoA(char* target, int num);
SHARED_API int				taAtoI(const char* str);
SHARED_API inline	bool	taItoB(int num){return (num > 0);};
SHARED_API inline int	taBtoI(bool b){return (b == false) ? 0 : 1;};
SHARED_API int				taCHtoI(char c);
SHARED_API char			taItoCH( int x );

#endif