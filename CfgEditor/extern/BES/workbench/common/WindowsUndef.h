#if defined(CreateDialog)
    #undef CreateDialog
#endif

#ifdef CreateFont
    #undef CreateFont
#endif

#if defined(CreateWindow)
    #undef CreateWindow
#endif

#ifdef LoadMenu
    #undef LoadMenu
#endif

#ifdef FindText
    #undef FindText
#endif

#ifdef GetCharWidth
   #undef GetCharWidth
#endif

#ifdef FindWindow
   #undef FindWindow
#endif

#ifdef PlaySound
   #undef PlaySound
#endif

#ifdef GetClassName
   #undef GetClassName
#endif

#ifdef GetClassInfo
   #undef GetClassInfo
#endif

#ifdef LoadAccelerators
   #undef LoadAccelerators
#endif

#ifdef DrawText
   #undef DrawText
#endif

#ifdef StartDoc
   #undef StartDoc
#endif

#ifdef GetObject
   #undef GetObject
#endif

#ifdef GetMessage
   #undef GetMessage
#endif

#ifdef LoadIcon
    #undef LoadIcon
#endif

#ifdef LoadBitmap
    #undef LoadBitmap
#endif

#ifdef LoadLibrary
    #undef LoadLibrary
#endif

#ifdef FindResource
    #undef FindResource
#endif

#ifdef IsMaximized
    #undef IsMaximized
#endif

#ifdef GetFirstChild
    #undef GetFirstChild
#endif

#ifdef GetFirstSibling
    #undef GetFirstSibling
#endif

#ifdef GetLastSibling
    #undef GetLastSibling
#endif

#ifdef GetPrevSibling
    #undef GetPrevSibling
#endif

#ifdef GetNextSibling
    #undef GetNextSibling
#endif

// For WINE
#if defined(GetWindowStyle)
  #undef GetWindowStyle
#endif

#ifdef GetFirstChild
   #undef GetFirstChild
#endif

#ifdef GetNextSibling
   #undef GetNextSibling
#endif

#ifdef Yield
    #undef Yield
#endif
