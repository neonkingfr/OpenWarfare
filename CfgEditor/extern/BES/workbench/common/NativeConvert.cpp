
#include "wbPrecomp.h"
#include "NativeConvert.h"
#include <stdlib.h>
#include <stdio.h>

//-----------------------------------------------------------------------
void taFtoA(char* target, float val, unsigned char NumFloatUnits)
{
	sprintf(target, "%.*f", NumFloatUnits, val);
}

//-----------------------------------------------------------------------
float	taAtoF(const char* str)
{
	return (float)atof(str);
}

//-----------------------------------------------------------------------
void taItoA(char* target, int num)
{
	_itoa(num, target, 10);
}

//-----------------------------------------------------------------------
int taAtoI(const char* str)
{
	return atoi(str);
}

//-----------------------------------------------------------------------------------------
int taCHtoI(char c)
{
	if((c>='0')&&(c<='9'))
		return(c-'0');
	else
		return 0;
}

//-----------------------------------------------------------------------------------------
char taItoCH( int x )
{
	if( (x >= 0 ) && ( x <=9 ) )
		return( '0' + x );
	else
		return '0';
}
