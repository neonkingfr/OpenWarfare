
#include "wbPrecomp.h"
#include "WxNativeConvert.h"

//------------------------------------------------------------------------------------
wxString ItoA(int Number)
{
	char array[64];
	_itoa(Number, array, 10);
	return array;
}

//------------------------------------------------------------------------------------
int AtoI(const wxString& str)
{
	long res;
	str.ToLong(&res, 10);
	return (int)res;
}

//------------------------------------------------------------------------------------
wxString FtoA(float val, short int NumFloatUnits)
{
	char temp[64];
	sprintf(temp,"%.*f", NumFloatUnits, val);	//oseka to na dlzku siestich znakov takze napriklad 0.01 je 6 znakov

	int lng = strlen(temp);
	if(lng > 0)
	{
		lng--;

		if(temp[lng] == '.')
			temp[lng] = 0;
	}
	return temp;
}

//------------------------------------------------------------------------------------
float AtoF(const wxString& str)
{
	return (float)atof(str.c_str());
}

//-------------------------------------------------------------------------------------
int wxColourToARGB(wxColour& color)
{
	assert(color.IsOk());
	int a = ((int)color.Alpha() << 24);
	int r = ((int)color.Red() << 16);
	int g = ((int)color.Green() << 8);
	int b = color.Blue();
	return (a | r | g | b);
}