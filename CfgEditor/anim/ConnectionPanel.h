
#ifndef CONNECTION_PANEL_H
#define CONNECTION_PANEL_H

#include "wx/panel.h"

class wxCheckBox;
class wxTextCtrl;
class wxStaticText;
class wxButton;
class ASClass;

enum
{
	CHECKBOX_CONNECT,
	CHECKBOX_INTERPOLATE,
	CHECKBOX_IGNORE_SET_VALUES,
	APPLY_TO_INHERITED_CLASSES,
	EDITBOX_WEIGHT_CONNECT,
	EDITBOX_WEIGHT_INTERP,
	BUTTON_OK,
};

//==========================================================================
//GUI to create/delete/modify connection betwen two anim states
//==========================================================================
class ConnectionPanel : public wxPanel
{
public:
	ConnectionPanel(wxWindow* parent);
	void Set(const char* SrcLabel, const char* DestLabel, bool connect, bool interpolate, const char* ConnectWeight, const char* InterpWeight);
	void SetFromSelectedConnection();
	void UpdateAccesibility();
	void PressOkButton();
	inline wxString& GetConnectionSourceName(){return SourceName;}
	inline wxString& GetConnectionDestName(){return DestName;}
private:
	wxString SourceName;
	wxString DestName;
	wxStaticText* SourceLabel;
	wxStaticText* DestLabel;
	wxCheckBox* ConnectCheck;
	wxCheckBox* InterpCheck;
	wxCheckBox* IgnoreSetValuesCheck;
	wxCheckBox* ApplyToInheritedClassesCheck;
	wxTextCtrl*	ConnectWeightEditBox;
	wxTextCtrl*	InterpWeightEditBox;
	wxButton*	OkButton;
	void OnCheckBox(wxCommandEvent& event);
	void OnButton(wxCommandEvent& event);
	void OnText(wxCommandEvent& event);
	void OnTextEnter(wxCommandEvent& event);
	void OnOkButton();
	bool ConnectionSelected(ASClass** from = NULL, ASClass** to = NULL);
	bool SelectedConnectionExist();
	DECLARE_EVENT_TABLE()
};

#endif