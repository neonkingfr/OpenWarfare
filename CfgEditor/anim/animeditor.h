
#ifndef ANIM_EDITOR_H
#define ANIM_EDITOR_H

#include "base/GenericEditor.h"
#include "anim/ParamConfigAnm.h"
#include "anim/MainFrameAnm.h"
#include "anim/EditActionsAnm.h"

class EACreateSGroup;
class EADeleteSGroup;
class EACreateSGroupRefs;
class EADeleteSGroupRefs;
class EAModifySGroupRefs;
class DWE_DragAndDrop;
class DiagramNode;
class StateFindDialog;
class ParamFileTreeAnm;
class ConnectionPanel;

//===============================================================================
// connection reference betwen two state classes (ASClass)
//===============================================================================
class ConnectionRef
{
public:
	ASClass*		SrcState;
	ASClass*		TargState;
	bool			connect;
	bool			interpolate;
	float			ConnectWeight;
	float			InterpolateWeight;

	ConnectionRef()
	{
		SrcState = NULL;
		TargState = NULL;
		connect = false;
		interpolate = false;
		ConnectWeight = 0.0f;
		InterpolateWeight = 0.0f;
	}

	ConnectionRef(ASClass* SrcState, ASClass* TargState)
	{
		this->SrcState = SrcState;
		this->TargState = TargState;
		connect = false;
		interpolate = false;
		ConnectWeight = 0.0f;
		InterpolateWeight = 0.0f;
	}

	void SetConnect(float ConnectWeight)
	{
		connect = true;
		this->ConnectWeight = ConnectWeight;
	}

	void SetInterpolate(float InterpolateWeight)
	{
		interpolate = true;
		this->InterpolateWeight = InterpolateWeight;
	}

	bool operator> (const ConnectionRef& rf)
	{
		if(SrcState == rf.SrcState)
			return (TargState > rf.TargState);
		
		return (SrcState > rf.SrcState);
	}

	bool operator< (const ConnectionRef& rf)
	{
		if(SrcState == rf.SrcState)
			return (TargState < rf.TargState);
		
		return (SrcState < rf.SrcState);
	}

	bool operator== (const ConnectionRef& rf)
	{
		return ((SrcState == rf.SrcState) && (TargState == rf.TargState));
	}
};

//===============================================================================
// reference to state with weight value. in config file can visible in two dimensional arrays like ConnectTo[]={StateName, WeightValue}
//===============================================================================
class StateRef
{
public:
	ASClass*		state;
	float			weight;

	StateRef()
	{
		state = NULL;
		weight = 0.0f;
	}

	StateRef(ASClass* state, float weight)
	{
		this->state = state;
		this->weight = weight;
	}

	bool operator> (const StateRef& rf)
	{
		return (state > rf.state);
	}

	bool operator< (const StateRef& rf)
	{
		return (state < rf.state);
	}

	bool operator== (const StateRef& rf)
	{
		return (state == rf.state);
	}
};

//==================================================================
// encapsulate copied data. StateName and reference to "animstate group" for single state
//==================================================================
class CopyStorage
{
public:
	CopyStorage(const char* StateName, const ASGroupRef& StateGraphRef)
	{
		this->StateName = StateName;
		this->StateGraphRef = StateGraphRef;
	}
	inline const DString& GetStateName(){return StateName;}
	inline const ASGroupRef& GetStateGraphRef(){return StateGraphRef;}
	ENF_DECLARE_MMOBJECT(CopyStorage);
private:
	DString StateName;
	ASGroupRef StateGraphRef;
};

enum
{
	CONNECT_DIRECTION_FROM = 0,
	CONNECT_DIRECTION_TO,
	CONNECT_DIRECTION_BOTH,
};

enum// ConnectTypeFlags
{
	CT_CONNECT = 1,
	CT_INTERPOLATE = 2,
	CT_BOTH = 3,
};

//===============================================================================
//animation config editor main class, inherited from generic version of editor
//all different implementations of GenericEditor funktionality are solved using virtual functions and own (inherited) instancies of any key class created inside AnimEditor
//===============================================================================
class AnimEditor : public GenericEditor
{
	friend class ConfigEdAnmApp;
	friend class MainFrameAnm;
	friend class AnimPFTreeTarget;
	friend class StateGroupsPanel;
	friend class StateFindDialog;
	friend class ParamFileTreeAnm;
	friend class ConnectionPanel;
public:
									AnimEditor();
	virtual						~AnimEditor();
	virtual bool				New();		//"new" user action event
	bool							Load();		//"load" user action event. with load in AnimEditor means load of working XML format of param config
	virtual bool				Save();		//"save" user action event. with save in AnimEditor means save of working XML format of param config
	bool							SaveAs();	//"SaveAs" user action event
	bool							Import();	//"Import" user action event. with import in AnimEditor means load native param file format (*.cpp) files
	bool							Export();	//"Export" user action event. same like import but for export
	CResult						Import(const char* FileName);	//import process
	virtual void				Copy();		//copy user action event
	virtual void				Paste();		//paste user action event
	virtual XMLConfig*		CreateEditorConfig();			//create editor application config file
	virtual ParamConfigAnm* CreateParamConfig();				//create instance of editable config object
	virtual MainFrameAnm*	CreateMainFrame();				//create application main window
	virtual ParamConfigAnm*	GetParamConfig();					//only get funtion of object created by CreateParamConfig() (only one singleton exist)
	virtual MainFrameAnm*	GetMainFrame();					//get main window pointer created by CreateMainFrame()
	DiagramWindow*				GetDiagramWindow();				//get diagram or "graph" connections edit window
	inline AnimConfigDef*	GetAnimDef(){return AnimDef;}	//get definition 
	virtual bool				GetGuiChoices(const char* property, const char* value, wxPGChoices& choices);	//if for any of property from "Class content" window is defined control with choices but choices are not available, this function is called. this is opportunity to fill choices object with any custom choices
	virtual const char*		GetAppName();						//app name
	virtual void				AddHistoryPoint(HistoryPoint* point);	//it register of undo/redo state internally called "HistoryPoint"
	virtual HistoryPointAnm* CreateHistoryPoint(const char* name);	//create undo/redo state used for edit ParamConfigAnm object
	virtual bool				RestoreHistoryPoint(HistoryPoint* point, bool undo = false);	//called only internally after undo/redo user actions for restore edited ParamConfigAnm object and editor gui to specific state. with undo parameter means restore form undo or redo action
	virtual bool				RealizeActions(const EArray<EditAction*>& actions);	//realize strip of realy needed editable actions to realize any of logical action
	virtual bool				RealizeAction(const EditAction* action);					//realize single editable action. all changes means only over ParamConfigAnm object
	bool							DoRealizeAction(const EACreateSGroup* action);			//realize "create new animstate group" action
	bool							DoRealizeAction(const EADeleteSGroup* action);			//realize "delete existing animstate group" action
	bool							DoRealizeAction(const EACreateSGroupRefs* action);		//realize "add referencies to some anim state classes in ParamConfigAnm to specific anim state group of single character class ACharacterClass" visual result of this are anim state classes inserted to graph
	bool							DoRealizeAction(const EADeleteSGroupRefs* action);		//same as previous but it removes all anim state refenecies from specific group of single character (remove states from graph)
	bool							DoRealizeAction(const EAModifySGroupRefs* action);		//modify state referencies to single graph (at this time only state positions on graf)

	void							GetStatesFromConnectionArray(PCClass* StatesClass, PCArray* arr, EArray<ASClass*>& states);	//only helper function to find state names from config's array
	void							RemoveStatesFromCurrentGraph(const EArray<ASClass*>& states);	//remove some anim state classes from graph
	uint							GetStatesSelectedInGraph(EArray<ASClass*>& states);				//get anim state classes just selected in graph
	void							UpdateConnection(ConnectionRef& connection, bool ApplyOnInheritedClasses);							//update connection betwen two anim states. means change of connect or interpolate referencies, and "weight" values
	PCVar*						FindStateRefValueFromArray(PCArray* arr, const char* StateName);
	PCVar*						FindStateRefValueFromArray(ASClass* state, const char* ArrayName, const char* StateName, bool recursiveToBase);

	bool							UpdateCurrentGroupGui();													//means update current graph (in graph stay specific group of single character) update accept all edit actions realized before e.g added, removed connections
	void							UpdateStatesIconsInTree();													//update icons of all state classes visualized in "Class tree" window
	void							UpdateCharacterManagerGuiContent();										//update gui content that are deppend on characters count
	void							UpdateGroupsManagerGuiContent();											//update gui content that are deppend on groups count of current character
	void							UpdateCopyPasteAccesibility();											//enable or disable copy/paste menu/toolbars
	void							UpdateSaveAccesibility();													//same for save

	//PCI = palete color index
	uint							PCI_Connect;		//palete color index to DiagramWindow for "connect" connection
	uint							PCI_Interpolate;	//palete color index to DiagramWindow for "interpolate" connection
	uint							PCI_Both;			//palete color index to DiagramWindow for "connect" and "interpolate" in one connection
	uint							PCI_Selected;		//palete color index to DiagramWindow for selected connection

  wxString CfgMovesBasicClassName;  //"CfgMovesBasic"
  wxString CfgMovesBasicClassID;		//"file/CfgMovesBasic"		//CfgMovesBasic class ID/location. config classes inherited from CfgMovesBasic class are defined as "characters"


									ENF_DECLARE_MMOBJECT(GenericEditor);
protected:
	static const int DND_AREA_DRAG_SOURCE = 3;				//drag area definition of state node in graph window
	static const int DND_AREA_DROP_TARGET = 4;				//drop area definition of state node in graph window

	virtual void				Clear();								//clear editor to state after init
	void							PrepareStatesToEdit();			//reorganize anim state classes to editable state. e.g replaces all connectFrom definfed connections to connectTo and more
	void							GetAllConnectionsFromStates(const EArray<ASClass*>& states, int ConnectTypeFlags, ACharacterClass* character, bool fromInheritedValues, EArray<ConnectionRef>& result);	//get conections informations betwen classes from "states" array. all must have common character as owner
	void							GetStateConnectionsFromArray(ASClass* state, PCArray* arr, ACharacterClass* character, bool connect, int direction, EArray<ConnectionRef>& result);	//get all connections from/to single anim state
	void							FindStateRefsFromArray(PCArray* arr, ACharacterClass* character, EArray<StateRef>& result);	//find all states from names stored in two dimmensional array like connectTo or interpolateTo
	void							FindStateRefsFromArray(PCArray* arr, ASClass* state, EArray<PCVar*>& result);
	void							DeleteObsoleteConnectionParamsFromStates(const EArray<ASClass*>& states);		//remove all obsolete parameters from state classes, that defines any connection. at this time e.g ConnectToArrayName array
//	void							InsertMissingConnectionParamsToStates(const EArray<ASClass*>& states);	//create empty ConnectToArrayName and InterpolateToArrayName arrays to states
	void							AddConnectionsToStates(const EArray<ConnectionRef>& connections);			//write logical connections to config in native format (members in ConnectToArrayName or InterpolateToArrayName arrays)
	ConnectionRef				GetConnectionInfo(ASClass* source, ASClass* target);							//get connection info betwen two anim states
	void							GetAllTargetStatesFrom(const EArray<ASClass*>& states, EArray<ASClass*>& result, int ConnectionTypeFlags);	//get all states to which specific state is connected or interpolated (depends on ConnectionTypeFlags). can say "get all output connections"
	void							GetAllSourceStatesFrom(const EArray<ASClass*>& states, EArray<ASClass*>& result, int ConnectionTypeFlags);	//same for input connections for single state
	uint							SelectStatesInGraph(const EArray<ASClass*>& states);							//select some states in graph (currently selected will be deselected)
	bool							Select(const EArray<ASClass*>& states, bool FromTree);						//select some classes in window "Class tree"
	void							InsertStatesToGraph(bool sources, int ConnectTypeFlags);
	void							InsertSourceStatesToGraph(const EArray<ASClass*>& from, const char* GraphNode, int ConnectTypeFlags);			//insert all "source" states to graph from left (input) side of single state. means all states that connect to single state
	void							InsertTargetStatesToGraph(const EArray<ASClass*>& from, const char* GraphNode, int ConnectTypeFlags);			//same for "target" or "output" states do which single state is connected
	void							RemoveStatesFromGraph(bool sources, int ConnectTypeFlags, bool ConnectOrInterpolate);
	void							RemoveSourceStatesFromGraph(const EArray<ASClass*>& from, int ConnectTypeFlags, bool ConnectOrInterpolate);	//reverse functionality of function InsertSourceStatesToGraph
	void							RemoveTargetStatesFromGraph(const EArray<ASClass*>& from, int ConnectTypeFlags, bool ConnectOrInterpolate);	//reverse functionality of function InsertTargetStatesToGraph
	void							GetSelectedStates(EArray<ASClass*>& states);	//get currently selected states. means in "Class tree" window.
	void							GetAllActionClasses(EArray<PCClass*>& result);
	void							GetAllBaseClassesFrom(PCClass* cl, EArray<PCClass*>& result);
	void							CreateStatesGroup(const char* GroupName);		//create new states group (on states group can look as single graph view) in current character
	void							DeleteStatesGroup(const char* GroupName);		//delete anim state group of current character
	virtual void				CreateClassRenameFeedbackActions(PCClass* cl, const char* NewName, EArray<EditAction*>& RedoActions, EArray<EditAction*>& UndoActions);
	void							CreateConnectionArrays(int ConnectTypeFlags);
	void							CreateConnectionArrays(ASClass* cl, int ConnectTypeFlags, EArray<EditAction*>& RedoActions, EArray<EditAction*>& UndoActions);
	void							ClearConnectionArrays(ASClass* cl, int ConnectTypeFlags, EArray<EditAction*>& RedoActions, EArray<EditAction*>& UndoActions);
	void							ClearConnectionArrays(int ConnectTypeFlags);
	void							RemoveConnectionArrays(int ConnectTypeFlags);
	void							RemoveConnectionArrays(ASClass* cl, int ConnectTypeFlags, EArray<EditAction*>& RedoActions, EArray<EditAction*>& UndoActions);
	void							CopyConnectionsFromBaseClass(int ConnectTypeFlags, bool OnlyMissingConnections);
	void							CopyConnectionsFromBaseClass(ASClass* cl, int ConnectTypeFlags, bool OnlyMissingConnections, EArray<EditAction*>& RedoActions, EArray<EditAction*>& UndoActions);
	void							CopyConnectionsArrayValues(ASClass* from, ASClass* to, const char* ArrayName, bool OnlyMissingConnections, EArray<EditAction*>& RedoActions, EArray<EditAction*>& UndoActions);
	void							DeleteConnectionsContainedInBaseClass(int ConnectTypeFlags, bool recursive);
	void							DeleteConnectionsContainedInBaseClass(ASClass* cl, int ConnectTypeFlags, bool recursive, EArray<EditAction*>& RedoActions, EArray<EditAction*>& UndoActions);
	void							DeleteSameConnectionsArrayValues(ASClass* state, const char* ArrayName, bool recursive, EArray<EditAction*>& RedoActions, EArray<EditAction*>& UndoActions);
	void							GetInheritedStates(ASClass* BaseState, EArray<ASClass*>& InheritedStates);
	void							SelectInheritedStates();
	void							SelectBaseStates();
	virtual void				RemoveDuplicities();	//remove all duplicity members if are inherited (clean config file)
	bool							DoSave(const char* FileName);						//save config process (to working XML version) 
	CResult						Load(const char* FileName);						//load config from working XML version
	CResult						Save(const char* FileName);						//called from DoSave()
	void							DestroyCopyData();									//destroy copied data if any
	wxString						GetImportDialogFilter();
	wxString						GetExportDialogFilter();
	AnimConfigDef*				AnimDef;													//look on AnimConfigDef class description
	EArray<CopyStorage*>		CopyData;												//copy data
	DString						CopyCharacter;											//copy data
	FRect							CopyNodesRect;											//copy data
	FPoint						CopyViewPoint;											//copy data
	int							CopyZoomLevel;											//copy data
   virtual CResult			OnAppInit();											//after application initialized event
	virtual int					OnAppExit();											//before application exit event
	virtual void				OnConfigLoaded();										//after any config is loaded event
	virtual void				OnSelectionChanged(bool FromTree);				//on selection is changed or cleared
	virtual void				OnClassesCreatedFromGui();							//one or more classes was created from GUI because of user activity
	virtual void				OnClassesDeletedFromGui();							//one or more classes was deleted from GUI because of user activity
	virtual void				OnClassRenamedFromGui();							//class was renamed
	virtual void				OnAppConfigChanged();								//application config was changed from options dialog
	void							OnCharacterChanged();								//current character was changed because of user activity
	void							OnASGroupChanged();									//current anim state group (of current character) was changed because of user activity
	void							OnShowSelectionProperies();						//after properties (class members) of selected classes were shown
	void							OnStateGraphNodesMoved(const EArray<DiagramNode*>* nodes);	//nodes in graph moved event
	void							OnGraphZoomChanged(uint zoom);										//zoom in graph windows changed event
	void							OnGraphViewPointChanged(const FPoint& pt);						//ViewPoint (camera position) on graph was changed
	bool							OnGraphWindowDropStates(wxCoord x, wxCoord y, const wxArrayString& StateIDs);	//some state names dropped into graph (using drag & drop)
	void							OnGraphStateMenu(ASClass* state, const wxPoint& pt);			//on graph need show popup menu of state at point pt
	void							OnGraphDragAndDrop(DWE_DragAndDrop* DEvent);						//on graph window internal drag & drop event (used to select connection)
	void							OnGraphSelectionChanged();												//selection in graph was changed event
	void							OnGraphKey(int keycode, bool down);									//any key pressed during graph window is focused

};

extern AnimEditor* g_aeditor;		//global pointer because AnimEditor is a singleton

#endif