
#ifndef IMAGE_LIST_ANM_H
#define IMAGE_LIST_ANM_H

#include "base/ImageList.h"

extern int BI_STATE_CLASS;
extern int BI_STATE_CLASS_ON_GRAPH;

//================================================================================
//extension of ImageList class from GenericEditorBase project
//================================================================================
class ImageListAnm : public ImageList
{
public:
	ImageListAnm(int width, int height, const bool mask = true, int initialCount = 1);
};

#endif