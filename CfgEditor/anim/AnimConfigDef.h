
#ifndef ANIM_CONFIG_DEF_H
#define ANIM_CONFIG_DEF_H

#include "common/CResult.h"

#define MAX_PREFIX_GROUPS 8

class TiXmlNode;
class TiXmlElement;
class TiXmlDocument;

//===============================================================================
//base node class of AnimConfigDef. it encapsulate every XML element from animdef.xml
//===============================================================================
class ACDefNode
{
	friend class ParamConfigDef;
public:
	ACDefNode();
	ACDefNode(TiXmlNode* node);
	virtual TiXmlNode* GetNode() const;
	virtual ~ACDefNode(){};
private:
protected:
	TiXmlNode* node;
	TiXmlElement* GetElement() const;
};

//===============================================================================
// encapsulate "prefix" XML element from animdef.xml
//===============================================================================
class ASPrefixDef : public ACDefNode
{
public:
	ASPrefixDef();
	ASPrefixDef(TiXmlElement* element);
	const char* GetName() const;
	const char* GetLabel() const;
	const char* GetDescription() const;
//	ENF_DECLARE_MMOBJECT(ASPrefixDef);
private:
};

//===============================================================================
// encapsulate "prefixgroup" XML element from animdef.xml
//===============================================================================
class ASPrefixGroupDef : public ACDefNode
{
public:
											ASPrefixGroupDef(TiXmlElement* element);
	uint									GetPrefixesCount();
	const ASPrefixDef&				GetPrefixDef(uint n);
	const char*							GetName();
	ENF_DECLARE_MMOBJECT(ASPrefixGroupDef);
private:
	EArray<ASPrefixDef>				prefixes;
};

//===============================================================================
// prefix mask for fast searching and sorting prefixed names e.g name "AmovPercMstp" is created from prefixes Amov, Perc and Mstp
//===============================================================================
class ASPrefixMask
{
public:
			ASPrefixMask();
			ASPrefixMask(const ASPrefixMask& other);
	void	Clear();
	void	Set(int group, int PrefixIndex);
	bool	Contains(const ASPrefixMask& mask) const;
	bool	AnySet() const;
	void	operator= (const ASPrefixMask& mask);
	bool	operator> (const ASPrefixMask& mask) const;
	bool	operator< (const ASPrefixMask& mask) const;
	bool	operator== (const ASPrefixMask& mask) const;
//	ENF_DECLARE_MMOBJECT(ASPrefixMask);
private:
	int	mask[MAX_PREFIX_GROUPS];	//defaultne -1 vsade ked grupa neni pouzita
};

//===============================================================================
// animdef.xml file wraper. it contains specific animation system informations. at this time only prefix definitions for searching "AnimState" classes
//===============================================================================
class AnimConfigDef
{
public:
							AnimConfigDef();
							~AnimConfigDef();
	virtual CResult	Load(const char* FileName);
	uint					GetPrefixGroupsCount();
	ASPrefixGroupDef* GetPrefixGroup(uint n);
	bool					FindPrefix(const char* PrefixName, uint* group, uint* prefix) const;
	const char*			FindFirstPrefixInString(const char* str, uint* group, uint* prefix, uint* PrefixLength) const;
	ENF_DECLARE_MMOBJECT(ParamConfigDef);
private:
	TiXmlDocument* doc;
	EArray<ASPrefixGroupDef*>	PrefixGroups;
};

extern AnimConfigDef* g_animdef;

#endif