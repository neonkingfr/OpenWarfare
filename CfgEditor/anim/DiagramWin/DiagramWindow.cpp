
#include "precomp.h"
#include "DiagramWindow.h"
#include "DiagramNode.h"
#include "Common/wxNativeConvert.h"
#include "Common/WinApiUtil.h"
#include "Common/NativeUtil.h"
#include "wx/dcbuffer.h"
#include <Mmsystem.h>

DEFINE_EVENT_TYPE(wxEVT_DIAGRAMWIN)

//-----------------------------------------------------------------------------------------
DiagramWindowPanel::DiagramWindowPanel(wxWindow* parent, wxApp* application)
	:wxPanel(parent, -1)
{
	diagram = new DiagramWindow(this, application);
/*
	VScrollBar = new wxScrollBar(this, -1, wxDefaultPosition, wxDefaultSize, wxSB_VERTICAL);
	HScrollBar = new wxScrollBar(this, -1, wxDefaultPosition, wxDefaultSize, wxSB_HORIZONTAL);

	wxBoxSizer* sizer1 = new wxBoxSizer(wxVERTICAL);
	sizer1->Add(diagram, 1, wxGROW | wxALIGN_LEFT | wxLEFT, 0);
	sizer1->Add(HScrollBar, 0, wxGROW | wxALIGN_LEFT | wxLEFT, 0);

	wxBoxSizer* sizer2 = new wxBoxSizer(wxVERTICAL);
	sizer2->Add(VScrollBar, 1, wxGROW | wxALIGN_LEFT | wxLEFT, 0);
	sizer2->AddSpacer(16);

	wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
	sizer->Add(sizer1, 1, wxGROW | wxALIGN_LEFT | wxLEFT, 0);
	sizer->Add(sizer2, 0, wxGROW | wxALIGN_LEFT | wxLEFT, 0);
*/
	wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
	sizer->Add(diagram, 1, wxGROW | wxALIGN_LEFT | wxLEFT, 0);
	SetSizer(sizer);
}

//-----------------------------------------------------------------------------------------
void DiagramWindowPanel::SetVirtSize(const FSize& size)
{
	VirtualSize = size;
/*	wxSize ClSize = GetClientSize();
	

	VScrollBar->SetScrollbar(0, 16, 50, 15, true);
	HScrollBar->SetScrollbar(0, 16, 50, 15, true);
*/
//	VScrollBar->SetScrollbar(VirtualSize.x / 2, VirtualSize.x / ClSize.x + ClSize.x, VirtualSize.x, ClSize.x, true);
//	HScrollBar->SetScrollbar(VirtualSize.y / 2, VirtualSize.y / ClSize.y + ClSize.y, VirtualSize.y, ClSize.y, true);
}

//-----------------------------------------------------------------------------------------
FSize DiagramWindowPanel::GetVirtSize()
{
	return VirtualSize;
}


BEGIN_EVENT_TABLE(DiagramWindow, wxPanel)
	EVT_PAINT(DiagramWindow::OnPaint)
	EVT_KEY_DOWN(DiagramWindow::OnKeyDown)
	EVT_KEY_UP(DiagramWindow::OnKeyUp)
	EVT_ERASE_BACKGROUND(DiagramWindow::OnErase)
	EVT_SIZE(DiagramWindow::OnSize)
	EVT_SCROLLWIN(DiagramWindow::OnScroll) 
	EVT_LEFT_DOWN(DiagramWindow::OnLMBdown)
	EVT_LEFT_UP(DiagramWindow::OnLMBup)
	EVT_LEFT_DCLICK(DiagramWindow::OnLMBDoubleClick) 
	EVT_RIGHT_DOWN(DiagramWindow::OnRMBdown)
	EVT_RIGHT_UP(DiagramWindow::OnRMBup)
	EVT_MOTION(DiagramWindow::OnMouseMove)
	EVT_MOUSEWHEEL(DiagramWindow::OnMouseWheel)
	EVT_MOUSE_CAPTURE_LOST(DiagramWindow::OnMouseCaptureLost) 
	EVT_IDLE(DiagramWindow::OnIdle)
END_EVENT_TABLE()

//-----------------------------------------------------------------------------------------
DiagramWindow::DiagramWindow(DiagramWindowPanel* parent, wxApp* application)
	:wxPanel(parent, -1, wxDefaultPosition, wxDefaultSize, wxFULL_REPAINT_ON_RESIZE/* | wxCLIP_CHILDREN*/)
{
	owner = parent;
	this->application = application;
	nodes.Clear();
	SetBackgroundStyle(wxBG_STYLE_CUSTOM);	//pri pouziti wxBufferedPaintDC
	LastConnectionID = 0;
	DragNode = NULL;
	ZoomFont = NULL;
	PaintBitmap = NULL;
	UnderCursorArea = NULL;
	DragAndDropSource = NULL;
	DragAndDropTarget = NULL;
	SelectedConnection = NULL;
	ZoomLevel = 5;
	scrolling = false;
	status = DWS_NONE;
	OnlyLinesDraw = false;

	for(int n = 2; n <= 18; n+=1)
	{
		float zm = (float)n * 0.05f;
		ZoomLevels.Insert(zm);
		ZoomFonts.Insert(new wxFont(n/*wxSize(iwidth, iheight)*/, wxFONTFAMILY_DEFAULT, wxNORMAL, wxNORMAL, false, _T("Arial")));
	}

	SetFont(*GetLargestZoomFont());
	BackgroundBrush = new wxBrush(wxColour(128, 128, 128), wxSOLID);
	NodeBackgroundBrush = new wxBrush(wxColour(255, 255, 255), wxSOLID);
	NodeSelectedBackgroundBrush = new wxBrush(wxColour(255, 255, 191), wxSOLID);
	NodeFocusedBackgroundBrush = new wxBrush(wxColour(255, 255, 140), wxSOLID);
	SquareSelectingBrush = new wxBrush(wxColour(127, 255, 127), wxTRANSPARENT);
	SquareSelectingPen = new wxPen(wxColour(255, 255, 191), 1, wxSHORT_DASH );

	//windows cursor clip area values to restore it later
	RECT CursorRect;
	GetClipCursor(&CursorRect); 
	ScreenCursorClipArea.x = CursorRect.left;
	ScreenCursorClipArea.y = CursorRect.top;
	ScreenCursorClipArea.width = CursorRect.right - CursorRect.left;
	ScreenCursorClipArea.height = CursorRect.bottom - CursorRect.top;

	zoom = 1.0f;
	OnZoomChanged();
	UpdateScrollPos();
	RecreatePaintBitmap();
}

//-----------------------------------------------------------------------------------------
DiagramWindow::~DiagramWindow()
{
	DestroyConnections(connections);	//delete connections and referencies on them
	DestroyNodes();

	for(uint n = 0; n < ZoomFonts.GetCardinality(); n++)
	{
		SAFE_DELETE(ZoomFonts[n]);
	}
	ZoomFonts.Clear();
	ZoomFont = NULL;

	SAFE_DELETE(PaintBitmap);

	SAFE_DELETE(BackgroundBrush);
	SAFE_DELETE(NodeBackgroundBrush);
	SAFE_DELETE(NodeSelectedBackgroundBrush);
	SAFE_DELETE(NodeFocusedBackgroundBrush);
	SAFE_DELETE(SquareSelectingBrush);
	SAFE_DELETE(SquareSelectingPen);
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::SetCanvasSize(int width, int height)
{
	SetVirtualSize(width, height);
}

//-----------------------------------------------------------------------------------------
bool DiagramWindow::AddNode(DiagramNode* node)
{
	enf_assert(node);
	node->SetCanvas(this);
//	const char* sss = node->GetName().c_str();
	bool inserted = nodes.Insert(node->GetName().c_str(), node);
	enf_assert(inserted);
	NodesArray.Insert(node);
	return inserted;
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::DestroyConnections(ESet<DPConnetionRef>& connections)
{
	for(uint n = 0; n < connections.GetCardinality(); n++)
	{
		DPConnetionRef& ref = connections[n];
		DNConnection* connection = ref.GetConnection();
		enf_assert(connection);
		delete connection;
	}
	connections.Clear();
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::UnselectConnection()
{
	if(SelectedConnection)
	{
		DPConnetionRef ref(SelectedConnection);
		bool removed = connections.Remove(ref);
		enf_assert(removed);

		delete SelectedConnection;
		SelectedConnection = NULL;
	}
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::SelectConnection(DiagramNode* SrcNode, uint SrcSlot, DiagramNode* DestNode, uint DestSlot, uint PaleteColorIndex)
{
	UnselectConnection();
	SelectedConnection = Connect(SrcNode, SrcSlot, DestNode, DestSlot, PaleteColorIndex);
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::Clear()
{
	SelectedConnection = NULL;
	ClearSelection();
	DestroyConnections(connections);	//delete connections and referencies on them
	DestroyNodes();
	LastConnectionID = 0;
	DragNode = NULL;
	UnderCursorArea = NULL;
	DragAndDropSource = NULL;
	DragAndDropTarget = NULL;
	status = DWS_NONE;
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::DestroyNodes()
{
	bool CallEvent = (NodesArray.GetCardinality() > 0);

	for(uint n = 0; n < NodesArray.GetCardinality(); n++)
		DestroyNode(nodes.GetElement(n), false);

	nodes.Clear();
	NodesArray.Clear();

//	if(CallEvent)	//do not call event if all nodes are deleted
//		OnSelectionChanged();
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::DestroyNode(DiagramNode* node, bool RemoveFromList)
{
	if(node->IsSelected())
		RemoveFromSelection(node);

	enf_assert(node);
	DestroyConnections(node->GetConnections());

	if(RemoveFromList)
	{
		nodes.Remove(node->GetName().c_str());
		NodesArray.Remove(node);
	}

	delete node;
}

//-----------------------------------------------------------------------------------------
DNConnection* DiagramWindow::Connect(DiagramNode* SrcNode, uint SrcSlot, DiagramNode* DestNode, uint DestSlot, uint ConCategory)
{
	DNConnection* connection = ENF_NEW DNConnection(DPCSocket(SrcNode, SrcSlot), DPCSocket(DestNode, DestSlot), ConCategory);
	connection->SetID(++LastConnectionID);
	connections.Insert(DPConnetionRef(connection));
	return connection;
}

//-----------------------------------------------------------------------------------------
DiagramNode* DiagramWindow::FindNode(const char* name)
{
	DiagramNode* node = nodes.Find(name);
	return node;
}

//-----------------------------------------------------------------------------------------
DiagramNode* DiagramWindow::GetNodeFromScreen(const wxPoint& pos, FPoint& PickOffsetResult)
{
	FPoint vpos = ScreenToVirtual(pos);

	for(int n = (int)NodesArray.GetCardinality() - 1; n >= 0; n--)
	{
		DiagramNode* node = NodesArray.GetElement(n);

		if(node->IsVisible() == false)
			continue;

		if(node->GetRect().Contains(vpos))
		{
			PickOffsetResult = node->GetPos() - vpos;
			return node;
		}
	}
	return NULL;
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnSize(wxSizeEvent& event)
{
	UpdateScrollPos();
	RecreatePaintBitmap();
	event.Skip();
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnErase(wxEraseEvent& event)	//empty implementation + SetBackgroundStyle(wxBG_STYLE_CUSTOM) is combination to disable auto background drawing
{
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnScroll(wxScrollWinEvent& event)
{
	RepaintFromClient();
	event.Skip();
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnLMBdown(wxMouseEvent& event)
{
	SetFocus();
	DiagramNode* node = GetNodeFromScreen(event.GetPosition(), DragNodeOffset);
	DNDArea* area = node ? node->CheckDNDArea(FPoint(-DragNodeOffset.x, -DragNodeOffset.y)) : NULL;
	bool SelectionChanged = false;
	bool NeedRepaint = false;

	switch(status)
	{
		case DWS_NONE:
		{
			if(node && node->IsSelected())
			{
				if(SetFocusedSelection(node))	//if click to not focused node, some events are needed
				{
					SelectionChanged = true;
					NeedRepaint = true;
				}
			}

			if(area && (area->GetFlags() & DNDF_DRAG))
			{
				DragAndDropSource = area;	//internal drag & drop now begin
				DragAndDropSource->Show(true);
				DragAndDropTarget = NULL;
				DragAndDropSourcePoint = ScreenToVirtual(event.GetPosition());
				ClipCursorInside(true);
				status = DWS_DRAG_AND_DROP;
			}
			else	//selecting
			{
				if(event.ControlDown())	//add to selection
				{
					if(node)
					{
						if(node->IsSelected())
							RemoveFromSelection(node);
						else
							AddToSelection(node);

						SelectionChanged = true;
						NeedRepaint = true;
					}
					else
					{
//						ClearSelection();
//						SelectionChanged = true;
						SquareSelPoint1 = SquareSelPoint2 = ScreenToVirtual(event.GetPosition());
						status = DWS_SQUARE_SELECTING;

						ClipCursorInside(true);
						SetCursor(wxCURSOR_CROSS);
						NeedRepaint = true;
					}
				}
				else	//moving nodes
				{
					if(node)
					{
						if(!node->IsSelected())
						{
							ClearSelection();
							AddToSelection(node);
							SelectionChanged = true;
						}

						DragNode = node;
						MovingSelPickPoint = event.GetPosition();
						status = DWS_MOVING_SELECTION;
						ClipCursorInside(true);
						SetCursor(wxCURSOR_HAND);
						NeedRepaint = true;
					}
					else
					{
//						ClearSelection();
//						SelectionChanged = true;
						SquareSelPoint1 = SquareSelPoint2 = ScreenToVirtual(event.GetPosition());
						status = DWS_SQUARE_SELECTING;

						ClipCursorInside(true);
						SetCursor(wxCURSOR_CROSS);
						NeedRepaint = true;
					}
				}
			}
			break;
		}
		case DWS_DRAGGING_CANVAS:
		{
			break;
		}
		case DWS_SQUARE_SELECTING:
		{
			break;
		}
		case DWS_MOVING_SELECTION:
		{
			break;
		}
		case DWS_DRAG_AND_DROP:
		{
			break;
		}
	}

	if(SelectionChanged)
		OnSelectionChanged();

	if(NeedRepaint)
		RepaintFromClient();
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnLMBup(wxMouseEvent& event)
{
	switch(status)
	{
		case DWS_NONE:
		{
			break;
		}
		case DWS_DRAGGING_CANVAS:
		{
			DragNode = NULL;
			SetCursor(*wxSTANDARD_CURSOR);
			ClipCursorInside(false);
			status = DWS_NONE;
			break;
		}
		case DWS_SQUARE_SELECTING:
		{
			wxPoint LeftUpPt, RightDownPt;
			GetBBoxFromPoints(VirtualToScreen(SquareSelPoint1), VirtualToScreen(SquareSelPoint2), LeftUpPt, RightDownPt);

			FPoint LeftUp = ScreenToVirtual(LeftUpPt);
			FPoint RightDown = ScreenToVirtual(RightDownPt);
			FPoint size = RightDown - LeftUp;
			FRect SelRect(LeftUp.x, LeftUp.y, size.x, size.y);
			EArray<DiagramNode*> ToSelect;
			EArray<DiagramNode*> ToUnselect;
			bool CtrlDown = event.ControlDown();

			if(CtrlDown)
			{
				for(uint n = 0; n < NodesArray.GetCardinality(); n++)
				{
					DiagramNode* node = NodesArray.GetElement(n);

					if(!SelRect.Overlap(node->GetRect()))
						continue;

					if(node->IsSelected())
						ToUnselect.Insert(node);
					else
						ToSelect.Insert(node);
				}
			}
			else	//(!CtrlDown)
			{
				for(uint n = 0; n < NodesArray.GetCardinality(); n++)
				{
					DiagramNode* node = NodesArray.GetElement(n);
					bool OverlapRect = SelRect.Overlap(node->GetRect());
					bool IsSelected = node->IsSelected();

					if(IsSelected && !OverlapRect)
						ToUnselect.Insert(node);
					else if(!IsSelected && OverlapRect)
						ToSelect.Insert(node);
				}

				if(ToSelect.GetCardinality() == 0)
					ToUnselect.Clear();
			}

			if(ToSelect.GetCardinality() > 0 || ToUnselect.GetCardinality() > 0)
			{
				if(ToUnselect.GetCardinality() > 0)
					RemoveFromSelection(ToUnselect);

				if(ToSelect.GetCardinality() > 0)
					AddToSelection(ToSelect);

				OnSelectionChanged();
			}

			status = DWS_NONE;
			SetCursor(*wxSTANDARD_CURSOR);
			ClipCursorInside(false);
			RepaintFromClient();
			break;
		}
		case DWS_MOVING_SELECTION:
		{
			DragNode = NULL;
			SetCursor(*wxSTANDARD_CURSOR);
			ClipCursorInside(false);
			status = DWS_NONE;

			if(MovingSelPickPoint != event.GetPosition())
			{
				//sending event "selected items were moved"
				wxCommandEvent CustomEvent(wxEVT_DIAGRAMWIN);
				CustomEvent.SetId(this->GetId());
				CustomEvent.SetEventObject(this);
		//		CustomEvent.SetString("");
				DWE_NodesMoved EventData(&selection);
				CustomEvent.SetClientObject(&EventData);
				GetEventHandler()->ProcessEvent(CustomEvent);
			}
			break;
		}
		case DWS_DRAG_AND_DROP:
		{
			if(UnderCursorArea && (UnderCursorArea->GetFlags() & DNDF_DROP))
				DragAndDropTarget = UnderCursorArea;

			DNDArea* source = DragAndDropSource;
			DNDArea* target = DragAndDropTarget;

			if(DragAndDropSource)
			{
				DragAndDropSource->Show(false);
				DragAndDropSource = NULL;
			}

			if(DragAndDropTarget)
			{
				DragAndDropTarget->Show(false);
				UnderCursorArea->SetPaleteColorIndex(DragAndDropTargetPI);
				DragAndDropTarget = NULL;
			}

			ClipCursorInside(false);

			if(source && target)
			{
				wxCommandEvent CustomEvent(wxEVT_DIAGRAMWIN);
				CustomEvent.SetId(this->GetId());
				CustomEvent.SetEventObject(this);
		//		CustomEvent.SetString("");
				DWE_DragAndDrop EventData(source, target);
				CustomEvent.SetClientObject(&EventData);
				GetEventHandler()->ProcessEvent(CustomEvent);
			}

			status = DWS_NONE;
			RepaintFromClient();
			break;
		}
	}
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnLMBDoubleClick(wxMouseEvent& event)
{
	switch(status)
	{
		case DWS_NONE:
		{
			ClearSelection();
			OnSelectionChanged();
			RepaintFromClient();
		}
	}
	event.Skip();
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnRMBdown(wxMouseEvent& event)
{
	SetFocus();
	LastMousePos = event.GetPosition();
	LastRMBDownPos = LastMousePos;

	switch(status)
	{
		case DWS_NONE:
		{
			CaptureMouse();
			SetCursor(wxCURSOR_SIZING);
	//		taShowCursor(false);
			CanvasDragStart = LastMousePos;
			status = DWS_DRAGGING_CANVAS;
			break;
		}
		case DWS_DRAGGING_CANVAS:
		{
			break;
		}
		case DWS_SQUARE_SELECTING:
		{
			break;
		}
		case DWS_MOVING_SELECTION:
		{
			break;
		}
		case DWS_DRAG_AND_DROP:
		{
			break;
		}
	}
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnRMBup(wxMouseEvent& event)
{
	switch(status)
	{
		case DWS_NONE:
		{
			break;
		}
		case DWS_DRAGGING_CANVAS:
		{
			ReleaseMouse();
			SetCursor(*wxSTANDARD_CURSOR);
	//		taShowCursor(true);
	//		WarpPointer(CanvasDragStart.x, CanvasDragStart.y);
			status = DWS_NONE;

			DiagramNode* node = GetNodeFromScreen(event.GetPosition(), DragNodeOffset);

			if(node && (abs(LastRMBDownPos.x - LastMousePos.x) < 10) && (abs(LastRMBDownPos.y - LastMousePos.y) < 10))
			{
				if(!node->IsSelected())
				{
					ClearSelection();
					AddToSelection(node);
					OnSelectionChanged();
					RepaintFromClient();
				}
				else
				{
					SetFocusedSelection(node);
					OnSelectionChanged();
					RepaintFromClient();
				}

				wxCommandEvent CustomEvent(wxEVT_DIAGRAMWIN);
				CustomEvent.SetId(this->GetId());
				CustomEvent.SetEventObject(this);
		//		CustomEvent.SetString("");
				DWE_NodeMenu EventData(node, event.GetPosition());
				CustomEvent.SetClientObject(&EventData);
				GetEventHandler()->ProcessEvent(CustomEvent);
			}
			break;
		}
		case DWS_SQUARE_SELECTING:
		{
			break;
		}
		case DWS_MOVING_SELECTION:
		{
			break;
		}
		case DWS_DRAG_AND_DROP:
		{
			break;
		}
	}
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnMouseMove(wxMouseEvent& event)
{
	MousePos = event.GetPosition();
	wxPoint MotionOff = MousePos - LastMousePos;
	bool NeedRepaint = false;

	switch(status)
	{
		case DWS_NONE:
		{
			FPoint CursorOffset;
			DiagramNode* node = GetNodeFromScreen(MousePos, CursorOffset);
			DNDArea* area = node ? node->CheckDNDArea(FPoint(-CursorOffset.x, -CursorOffset.y)) : NULL;

			if(area && (area->GetFlags() & DNDF_DRAG))	//if cursor stay on drag area
			{
				if(UnderCursorArea)	//uz nieco pod kurzorom bolo
				{
					if(UnderCursorArea != area)
					{
						enf_assert(UnderCursorArea);
						UnderCursorArea->Show(false);
						UnderCursorArea = area;
						UnderCursorArea->Show(true);
						NeedRepaint = true;
					}
				}
				else	//este pod kurzorom nebolo nic
				{
					UnderCursorArea = area;
					enf_assert(UnderCursorArea);
					UnderCursorArea->Show(true);
					NeedRepaint = true;
				}
			}
			else	//ignore drag areas
			{
				if(UnderCursorArea)	//but maybe repaint needed
				{
					if(UnderCursorArea != DragAndDropSource)
					{
						UnderCursorArea->Show(false);
						NeedRepaint = true;
					}

					UnderCursorArea = NULL;
				}
			}
			break;
		}
		case DWS_DRAGGING_CANVAS:
		{
			FSize motion = ScreenToVirtual(wxSize(MotionOff.x, MotionOff.y));
	//		motion *= 100;
			ViewPoint -= FPoint(motion.x, motion.y);
			//posleme event ze sa zmenil zoom
			OnViewPointChanged();
			UpdateScrollPos();
			NeedRepaint = true;
			break;
		}
		case DWS_SQUARE_SELECTING:
		{
			SquareSelPoint2 = ScreenToVirtual(event.GetPosition());
			NeedRepaint = true;
			break;
		}
		case DWS_MOVING_SELECTION:
		{
			UpdateSelectionMoving();
			NeedRepaint = true;
			break;
		}
		case DWS_DRAG_AND_DROP:
		{
			FPoint CursorOffset;
			DiagramNode* node = GetNodeFromScreen(MousePos, CursorOffset);
			DNDArea* area = node ? node->CheckDNDArea(FPoint(-CursorOffset.x, -CursorOffset.y)) : NULL;

			if(DragAndDropSource)	//finding target
			{
				if(area && (area->GetFlags() & DNDF_DROP))	//check for drop area
				{
					if(UnderCursorArea)	//any area under cursor before
					{
						if(UnderCursorArea != area)
						{
							UnderCursorArea->Show(false);
							UnderCursorArea->SetPaleteColorIndex(DragAndDropTargetPI);
							UnderCursorArea = area;
							UnderCursorArea->Show(true);
							DragAndDropTargetPI = UnderCursorArea->GetPaleteColorIndex();
							UnderCursorArea->SetPaleteColorIndex(DragAndDropSource->GetPaleteColorIndex());
						}
					}
					else	//no area under cursor before
					{
						UnderCursorArea = area;
						UnderCursorArea->Show(true);
						DragAndDropTargetPI = UnderCursorArea->GetPaleteColorIndex();
						UnderCursorArea->SetPaleteColorIndex(DragAndDropSource->GetPaleteColorIndex());
					}
				}
				else	//no interessant area found
				{
					if(UnderCursorArea)	//but any area under cursor before
					{
						if(UnderCursorArea != DragAndDropSource && UnderCursorArea != DragAndDropTarget)
						{
							UnderCursorArea->SetPaleteColorIndex(DragAndDropTargetPI);
							UnderCursorArea->Show(false);
						}

						UnderCursorArea = NULL;
					}
				}
			}
			NeedRepaint = true;
			break;
		}
	}

	if(!scrolling)	//check for scrolling start if needed
	{
		UpdateScrolling();
		NeedRepaint = true;
	}

	if(NeedRepaint && !scrolling)
		RepaintFromClient();

	LastMousePos = MousePos;
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnMouseWheel(wxMouseEvent& event)
{
	int WheelRotation = event.GetWheelRotation();
	bool forward = WheelRotation < 0;

	switch(status)
	{
		case DWS_NONE:
		{
			if(!forward && ZoomLevel < (ZoomLevels.GetCardinality() - 1))
				ZoomLevel++;
			else if(forward && ZoomLevel > 0)
				ZoomLevel--;

			zoom = ZoomLevels[ZoomLevel];
			ZoomFont = ZoomFonts[ZoomLevel];
			OnZoomChanged();
			UpdateScrollPos();
			RepaintFromClient();
			break;
		}
		case DWS_DRAGGING_CANVAS:
		{
			break;
		}
		case DWS_SQUARE_SELECTING:
		{
			break;
		}
		case DWS_MOVING_SELECTION:
		{
			break;
		}
		case DWS_DRAG_AND_DROP:
		{
			break;
		}
	}
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnMouseCaptureLost(wxMouseCaptureLostEvent& event)
{
	//with empty event without calling event.Skip() here means "yes, i accept that mouse is lost the capture"
	//very impotant for using methods wxWindow::CaptureMouse() and wxWindow::ReleaseMouse();
	//assert from wx core is throwed without this
//	event.Skip();
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnKeyDown(wxKeyEvent& event)
{
	SendKeyEvent(event.GetKeyCode(), true);
	event.Skip();
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnKeyUp(wxKeyEvent& event)
{
	SendKeyEvent(event.GetKeyCode(), false);
	event.Skip();
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::SendKeyEvent(int keycode, bool down)
{
	wxCommandEvent CustomEvent(wxEVT_DIAGRAMWIN);
	CustomEvent.SetId(this->GetId());
	CustomEvent.SetEventObject(this);
	DWE_Key EventData(keycode, down);
	CustomEvent.SetClientObject(&EventData);
	GetEventHandler()->ProcessEvent(CustomEvent);
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnZoomChanged()
{
	//posleme event ze sa zmenil zoom
	wxCommandEvent CustomEvent(wxEVT_DIAGRAMWIN);
	CustomEvent.SetId(this->GetId());
	CustomEvent.SetEventObject(this);
	DWE_ZoomChanged EventData(ZoomLevel);
	CustomEvent.SetClientObject(&EventData);
	GetEventHandler()->ProcessEvent(CustomEvent);
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::UpdateScrollPos()
{
	FSize HalfScreenVirt = ScreenToVirtual(GetClientSize());
	HalfScreenVirt *= 0.5f;
	ScrollPos = ViewPoint - HalfScreenVirt;
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::RecreatePaintBitmap()
{
	wxSize size = GetClientSize();

	if(PaintBitmap)
		delete PaintBitmap;

	PaintBitmap = new wxBitmap(size.x, size.y);
}

//-----------------------------------------------------------------------------------------
wxPoint DiagramWindow::VirtualToScreen(const FPoint& point)
{
	FPoint pt = point - ScrollPos;
	pt *= zoom;
	return wxPoint(RoundUp2(pt.x), RoundUp2(pt.y));
//	return wxPoint((int)pt.x, (int)pt.y);
}

//-----------------------------------------------------------------------------------------
wxSize DiagramWindow::VirtualToScreen(const FSize& size)
{
	return wxSize(RoundUp2(size.x * zoom), RoundUp2(size.y * zoom));
}

//-----------------------------------------------------------------------------------------
float DiagramWindow::VirtualToScreen(float f)
{
	return RoundUp2(f * zoom);
}

//-----------------------------------------------------------------------------------------
FPoint DiagramWindow::ScreenToVirtual(const wxPoint& point)
{
	FPoint pt(point.x, point.y);
	pt /= zoom;
	pt += ScrollPos;
	return pt;
}

//-----------------------------------------------------------------------------------------
FSize DiagramWindow::ScreenToVirtual(const wxSize& size)
{
	FSize sz(size.x, size.y);
	sz.x /= zoom;
	sz.y /= zoom;
	return sz;
}

//-----------------------------------------------------------------------------------------
float DiagramWindow::ScreenToVirtual(int size)
{
	return ((float)size / zoom);
}

//-----------------------------------------------------------------------------------------
wxFont* DiagramWindow::GetZoomFont()
{
	return ZoomFont;
}

//-----------------------------------------------------------------------------------------
wxFont* DiagramWindow::GetLargestZoomFont()
{
	return ZoomFonts[ZoomFonts.GetCardinality() - 1];
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::SetZoomLevel(uint level)
{
	ZoomLevel = level;
	zoom = ZoomLevels[ZoomLevel];
	ZoomFont = ZoomFonts[ZoomLevel];
	OnZoomChanged();
	UpdateScrollPos();
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::SetViewPoint(FPoint pt)
{
	ViewPoint = pt;
	UpdateScrollPos();
}

//-----------------------------------------------------------------------------------------
const FPoint& DiagramWindow::GetViewPoint()
{
	return ViewPoint;
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::RepaintContent(wxDC& dc)
{
	wxSize size = GetClientSize();
	wxRect ClientRect(0, 0, size.x, size.y);
//	wxSize vsize = GetVirtualSize();
	enf_assert(dc.IsOk());

//	dc.SetBackground(*wxGREY_BRUSH);
	dc.SetBackground(*GetBackgroundBrush());
	dc.Clear();

	wxPoint spline[6];

	for(uint n = 0; n < connections.GetCardinality(); n++)
	{
		DNConnection* conection = connections[n].GetConnection();
		DPCSocket& source = conection->GetSource();
		DPCSocket& target = conection->GetTarget();

		DCSlot* SrcSlot = source.node->GetSlot(source.slot);
		DCSlot* DestSlot = target.node->GetSlot(target.slot);

		FPoint SourcePt = SrcSlot->GetConnectionPoint();
		FPoint DestPt = DestSlot->GetConnectionPoint();

		wxPoint SrcPoint = VirtualToScreen(SourcePt);
		wxPoint DestPoint = VirtualToScreen(DestPt);

		const PCConnectionCategory& category = ConCategories[conection->GetCategory()];
		dc.SetPen(wxPen(PaleteColors[category.GetPaleteColorIndex()]));

//		if(ClientRect.Contains(SrcPoint) || ClientRect.Contains(DestPoint))
//		{
		if(OnlyLinesDraw)
			dc.DrawLine(SrcPoint.x, SrcPoint.y, DestPoint.x, DestPoint.y);
		else
		{
			FPoint ToTarget = DestPt - SourcePt;
			float dist = ToTarget.Normalize();

			float DistOff = (dist * 0.2f);

			if(DistOff > 50.0f)
				DistOff = 50.0f;

			const FPoint right(1, 0);
			float dot = right.Dot(ToTarget);

			if(dot < 0.0f && VirtualToScreen(dist) < 1000.0f)	//if angle is lower than 90 or distance over screen is higher than 100 pixels, only lines will be drawn (curves uses to much cpu time)
			{
				float LerpFactor = dot;
				uint SplinePtsNum = (dot < 0.3f) ? 6 : 4;

				if(LerpFactor < 0.0f)
					LerpFactor = 0.0f;

				LerpFactor *= 2.0f;
				LerpFactor = 1.0f - LerpFactor;

				FPoint SourcePt2;

				SourcePt2 = right.Lerp(ToTarget, LerpFactor * 0.5f);

				if(fabs(SourcePt2.x) < 0.001 && fabs(SourcePt2.y) < 0.001)	//lerp returns incorrect results with parallel vectors
				{
					SourcePt2.x = 0.0f;
					SourcePt2.y = -1.0f;
				}
				else
					SourcePt2.Normalize();

				SourcePt2 *= DistOff;

				FPoint DestPt2 = SourcePt2;	//optimalization. on other side is allways reverse vector
				DestPt2.x = -DestPt2.x;

				float fdot = fabs(dot);

				if(fabs(dot) < 0.9997)
					DestPt2.y = -DestPt2.y;

				SourcePt2 += SourcePt;
				DestPt2 += DestPt;

				wxPoint SrcPoint2 = VirtualToScreen(SourcePt2);
				wxPoint DestPoint2 = VirtualToScreen(DestPt2);

				if(SplinePtsNum == 6)
				{
					FPoint SourcePt3 = right;
					SourcePt3 *= DistOff * 0.5;

					FPoint DestPt3 = SourcePt3;
					DestPt3.x = -DestPt3.x;

					if(fabs(dot) < 0.997)
						DestPt3.y = -DestPt3.y;

					SourcePt3 += SourcePt;
					DestPt3 += DestPt;

					wxPoint SrcPoint3 = VirtualToScreen(SourcePt3);//SrcPoint + wxPoint(20 , 0);
					wxPoint DestPoint3 = VirtualToScreen(DestPt3);//DestPoint - wxPoint(20 , 0);

					spline[0] = SrcPoint;
					spline[1] = SrcPoint3;
					spline[2] = SrcPoint2;
					spline[3] = DestPoint2;
					spline[4] = DestPoint3;
					spline[5] = DestPoint;
					dc.DrawSpline(6, spline);
				}
				else	//SplinePtsNum == 4
				{
					spline[0] = SrcPoint;
					spline[1] = SrcPoint2;
					spline[2] = DestPoint2;
					spline[3] = DestPoint;
					dc.DrawSpline(4, spline);
				}

	//			for(int s = 0; s < SplinePtsNum; s++)
	//				dc.DrawCircle(spline[s], 3);
			}
			else	//drawing only lines
			{
				dc.DrawLine(SrcPoint.x, SrcPoint.y, DestPoint.x, DestPoint.y);
			}
		}
	}

	dc.SetPen(*wxBLACK_PEN);

	for(uint n = 0; n < NodesArray.GetCardinality(); n++)
	{
		DiagramNode* node = NodesArray.GetElement(n);
		node->SetVisible(true);
		node->RepaintContent(dc);
	}

	if(status == DWS_SQUARE_SELECTING)
	{
		wxPoint LeftUp, RightDown;
		GetBBoxFromPoints(VirtualToScreen(SquareSelPoint1), VirtualToScreen(SquareSelPoint2), LeftUp, RightDown);

		int width = RightDown.x - LeftUp.x;
		int height = RightDown.y - LeftUp.y;

		if(width > 0 && height > 0)
		{
			dc.SetPen(*GetSquareSelectingPen());
			dc.SetBrush(*GetSquareSelectingBrush());
			dc.DrawRectangle(LeftUp.x, LeftUp.y, width, height);
		}
	}
	else if(status == DWS_DRAG_AND_DROP)
	{
		if(DragAndDropSource && UnderCursorArea && UnderCursorArea != DragAndDropSource)
			dc.SetPen(wxPen(GetPaleteColor(DragAndDropSource->GetPaleteColorIndex()), 1, wxSOLID));

		wxPoint DragSourcePt = VirtualToScreen(DragAndDropSourcePoint);
		dc.DrawLine(DragSourcePt.x, DragSourcePt.y, MousePos.x, MousePos.y);
	}
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnPaint(wxPaintEvent &WXUNUSED(event)/*wxPaintEvent& event*/)
{
	/*	wxBufferedPaintDC dc(this, wxBUFFER_VIRTUAL_AREA);
	PrepareDC(dc);
	RepaintContent(dc);
	*/
	wxSize ClSize = GetClientSize();//GetVirtualSize();
	wxRect windowRect(wxPoint(0, 0), ClSize);    

	wxSize ClSizeScaled = ClSize;
//	ClSizeScaled.x = (int)((float)windowRect.width * 4.0f);
//	ClSizeScaled.y = (int)((float)windowRect.height * 4.0f);

//	CalcUnscrolledPosition(windowRect.x, windowRect.y, &windowRect.x, &windowRect.y);

	wxMemoryDC memdc ;
//	wxBitmap bmp(ClSizeScaled.x, ClSizeScaled.y);
//	memdc.SelectObject(bmp) ;
	memdc.SelectObject(*PaintBitmap);

	RepaintContent(memdc);

	wxPaintDC dc(this);
	PrepareDC(dc);
//	dc.Clear();
/*
	dc.SetLogicalOrigin( 0, 0 );
	dc.SetAxisOrientation( true, false );
	dc.SetUserScale( 0.25, 0.25 );
	dc.SetMapMode( wxMM_TEXT );
*/
	dc.Blit(0, 0, PaintBitmap->GetWidth(), PaintBitmap->GetHeight(), &memdc, 0, 0);
	memdc.SelectObject(wxNullBitmap) ;
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnIdle(wxIdleEvent& event)
{
	if(scrolling)
	{
		UpdateScrolling();

		if(scrolling)
		{
			//some mouse position updates here because mouse maybe stay in idle, but not relative to virtual canvas if scrolling
			if(status == DWS_SQUARE_SELECTING)
				SquareSelPoint2 = ScreenToVirtual(MousePos);
			else if(status == DWS_MOVING_SELECTION)
				UpdateSelectionMoving();

			event.RequestMore();
		}
		RepaintFromClient();
	}
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::RepaintFromClient()
{
	wxPoint ScrollPos;    
	wxSize ClSize = GetClientSize();//GetVirtualSize();

	wxMemoryDC memdc ;
	memdc.SelectObject(*PaintBitmap);
	RepaintContent(memdc);

	wxClientDC dc(this);
	PrepareDC(dc);

	dc.Blit(0, 0, PaintBitmap->GetWidth(), PaintBitmap->GetHeight(), &memdc, 0, 0);
	memdc.SelectObject(wxNullBitmap) ;
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::ClearSelection()
{
	for(uint n = 0; n < selection.GetCardinality(); n++)
	{
		DiagramNode* node = selection[n];
		node->OnSelect(false);
	}
	selection.Clear();
}

//-----------------------------------------------------------------------------------------
uint DiagramWindow::GetSelectionCount()
{
	return selection.GetCardinality();
}

//-----------------------------------------------------------------------------------------
DiagramNode* DiagramWindow::GetSelectionMember(uint n)
{
	return selection[n];
}

//-----------------------------------------------------------------------------------------
DiagramNode* DiagramWindow::GetFocusedSelection()
{
	uint count = selection.GetCardinality();
	return (count > 0) ? selection[count - 1] : NULL;
}

//-----------------------------------------------------------------------------------------
bool DiagramWindow::SetFocusedSelection(DiagramNode* node)
{
	enf_assert(node);

	if(!node->IsSelected())
		return false;

	uint index = selection.GetIndexOf(node);
	enf_assert(index != INDEX_NOT_FOUND);

	DiagramNode* focused = GetFocusedSelection();

	if(focused == node)
		return false;

	//switch pointers
	uint FocusedIndex = selection.GetCardinality() - 1;
	selection[index] = focused;
	selection[FocusedIndex] = node;
	return true;
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::AddToSelection(DiagramNode* node)
{
	enf_assert(node);
	selection.Insert(node);
	node->OnSelect(true);
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::AddToSelection(EArray<DiagramNode*>& nodes)
{
	for(uint n = 0; n < nodes.GetCardinality(); n++)
	{
		DiagramNode* node = nodes[n];
		selection.Insert(node);
		node->OnSelect(true);
	}
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::Select(EArray<DiagramNode*>& nodes, bool GenerateEvent)
{
	ClearSelection();
	AddToSelection(nodes);

	if(GenerateEvent)
		OnSelectionChanged();
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::RemoveFromSelection(DiagramNode* node)
{
	enf_assert(node);
	uint index = selection.GetIndexOf(node);
	enf_assert(index != INDEX_NOT_FOUND);
	selection.RemoveElementOrdered(index);
	node->OnSelect(false);
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::RemoveFromSelection(EArray<DiagramNode*>& nodes)
{
	for(uint n = 0; n < nodes.GetCardinality(); n++)
	{
		DiagramNode* node = nodes[n];

		if(selection.Remove(node))
			node->OnSelect(false);
	}
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::GetBBoxFromPoints(const wxPoint& p1, const wxPoint& p2, wxPoint& LeftUp, wxPoint& RightDown)
{
	if(p1.x < p2.x)
	{
		LeftUp.x = p1.x;
		RightDown.x = p2.x;
	}
	else
	{
		LeftUp.x = p2.x;
		RightDown.x = p1.x;
	}

	if(p1.y < p2.y)
	{
		LeftUp.y = p1.y;
		RightDown.y = p2.y;
	}
	else
	{
		LeftUp.y = p2.y;
		RightDown.y = p1.y;
	}
}

//-----------------------------------------------------------------------------------------
uint DiagramWindow::AddConnectionCategory(const PCConnectionCategory& category)
{
	return ConCategories.Insert(category);
}

//-----------------------------------------------------------------------------------------
const PCConnectionCategory& DiagramWindow::GetConnectionCategory(uint category)
{
	return ConCategories[category];
}

//-----------------------------------------------------------------------------------------
uint DiagramWindow::AddPaleteColor(const wxColour& color)
{
	return PaleteColors.Insert(color);
}

//-----------------------------------------------------------------------------------------
const wxColour& DiagramWindow::GetPaleteColor(uint index)
{
	return PaleteColors[index];
}

//-----------------------------------------------------------------------------------------
void DiagramWindow::OnSelectionChanged()
{
	SortNodes();
	wxCommandEvent CustomEvent(wxEVT_DIAGRAMWIN);
	CustomEvent.SetId(this->GetId());
	CustomEvent.SetEventObject(this);
	DWE_SelectionChanged EventData;
	CustomEvent.SetClientObject(&EventData);
	GetEventHandler()->ProcessEvent(CustomEvent);
}

//---------------------------------------------------------------------------------------------
void DiagramWindow::OnViewPointChanged()
{
	wxCommandEvent CustomEvent(wxEVT_DIAGRAMWIN);
	CustomEvent.SetId(this->GetId());
	CustomEvent.SetEventObject(this);
	DWE_ViewPointChanged EventData(ViewPoint);
	CustomEvent.SetClientObject(&EventData);
	GetEventHandler()->ProcessEvent(CustomEvent);
}

//---------------------------------------------------------------------------------------------
void DiagramWindow::ClipCursorInside(bool OnOff)
{
	if(OnOff)
	{
		int left = 0;
		int top = 0;
		int right, bottom;
		ClientToScreen(&left, &top);
		GetClientSize(&right, &bottom);
		ClientToScreen(&right, &bottom);

		RECT rect;
		rect.left = left;
		rect.top = top;
		rect.right = right;
		rect.bottom = bottom - 1;	//-1 because unknown problem of uncalled OnMouseMove event if mouse move to bottom of window is very fast
		ClipCursor(&rect);
		return;
	}

	wxRect& OrgClipRect = ScreenCursorClipArea;

	RECT rect;
	rect.left = OrgClipRect.x;
	rect.top = OrgClipRect.y;
	rect.right = rect.left + OrgClipRect.width;
	rect.bottom = rect.top + OrgClipRect.height;
	ClipCursor(&rect);
}

//---------------------------------------------------------------------------------------------
void DiagramWindow::OnMainFrameActivate(bool active)
{
	DragNode = NULL;
	UnderCursorArea = NULL;
	DragAndDropSource = NULL;
	DragAndDropTarget = NULL;
	status = DWS_NONE;
}

//---------------------------------------------------------------------------------------------
void DiagramWindow::UpdateScrolling()
{
	if(status == DWS_MOVING_SELECTION || status == DWS_DRAG_AND_DROP || status == DWS_SQUARE_SELECTING)
	{
		wxSize ClSize = GetClientSize();
		int mb = 3;	//movimg border
		FPoint off;

		if(MousePos.x <= mb)
			off.x -= 1.0f;
		else if(MousePos.x >= (ClSize.x - mb))
			off.x += 1.0f;

		if(MousePos.y <= mb)
			off.y -= 1.0f;
		else if(MousePos.y >= (ClSize.y - mb))
			off.y += 1.0f;

		if(off.Length() > 0.0f)
		{
			int CurrentTime = timeGetTime();

			if(!scrolling)
				LastTime = CurrentTime;

			int TimeElapsed = CurrentTime - LastTime;
			float TimeDelta = (float)TimeElapsed * 0.1f;
			LastTime = CurrentTime;

			off.Normalize();
			off *= ScreenToVirtual(10) * TimeDelta;
			SetViewPoint(GetViewPoint() + off);

			OnViewPointChanged();	//this call event

			if(!scrolling)
				application->SendIdleEvents(this, wxIdleEvent());

			scrolling = true;
		}
		else
			scrolling = false;
	}
}

//---------------------------------------------------------------------------------------------
void DiagramWindow::UpdateSelectionMoving()
{
	FPoint VirtMousePos = ScreenToVirtual(MousePos);
	FPoint MovementOff = VirtMousePos - DragNode->GetPos();

	for(uint n = 0; n < selection.GetCardinality(); n++)
	{
		DiagramNode* node = selection[n];
		node->SetPos(node->GetPos() + DragNodeOffset + MovementOff);
	}
}

//---------------------------------------------------------------------------------------------
float DiagramWindow::CalculateNodeVirtualWidth(const char* text)
{
	wxFont* DiagWinFont = GetLargestZoomFont();

	wxCoord w;
	this->GetTextExtent(text, &w, NULL, NULL, NULL, DiagWinFont);

	float fwidth = (float)w * 1.1f;
	fwidth += 50.0f;
	return fwidth;
}

//---------------------------------------------------------------------------------------------
void DiagramWindow::DrawOnlyLines(bool lines)
{
	OnlyLinesDraw = lines;
}

//---------------------------------------------------------------------------------------------
FRect DiagramWindow::GetBoundBoxOfNodes(EArray<DiagramNode*>& nodes)
{
	if(nodes.GetCardinality() == 0)
		return FRect();

	DiagramNode* FirstNode = nodes[0];
	FRect& FirstRc = FirstNode->GetRect();
	float left = FirstRc.x;
	float top = FirstRc.y;
	float right = FirstRc.GetRight();
	float bottom = FirstRc.GetBottom();

	for(uint n = 1; n < nodes.GetCardinality(); n++)
	{
		DiagramNode* node = nodes[n];
		FRect& rc = node->GetRect();
		float r = rc.GetRight();
		float b = rc.GetBottom();

		if(rc.x < left) left = rc.x;
		if(rc.y < top) top = rc.y;
		if(r > right) right = r;
		if(b > bottom) bottom = b;
	}

	return FRect(left, top, right - left, bottom - top);
}

//---------------------------------------------------------------------------------------------
FRect DiagramWindow::GetVisibleRect()
{
	return FRect(ScreenToVirtual(wxPoint(0, 0)), ScreenToVirtual(GetClientSize()));
}

//-----------------------------------------------------------------------
int __cdecl CompareNodes(const void* a, const void* b)
{
	DiagramNode* n1 = *(DiagramNode**)a;
	DiagramNode* n2 = *(DiagramNode**)b;
	bool Node1Selected = n1->IsSelected();
	bool Node2Selected = n2->IsSelected();

	if(Node1Selected == Node2Selected)
	{
		if(Node1Selected)
		{
			return n1->IsFocused() ? 1 : -1;
		}
		else
			return 0;
	}
	else
		return (Node1Selected ? 1 : -1);
}

//---------------------------------------------------------------------------------------------
void DiagramWindow::SortNodes()
{
	qsort(NodesArray.GetArray(), NodesArray.GetCardinality(), sizeof(DiagramNode*), CompareNodes);
}
