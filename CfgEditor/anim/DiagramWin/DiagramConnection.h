
#ifndef DIAGRAM_CONNECTION_H
#define DIAGRAM_CONNECTION_H

class DiagramNode;

//======================================================================
// connection socket
//======================================================================
struct DPCSocket
{
	DPCSocket()
	{
		node = NULL;
		slot = INDEX_NOT_FOUND;
	}

	DPCSocket(DiagramNode*	node, uint slot)
	{
		assert(node);
		this->node = node;
		this->slot = slot;
	}

	inline DiagramNode* GetNode(){return node;}
	DiagramNode*	node;
	uint				slot;
};

//======================================================================
//connection betwen two nodes on canvas
//======================================================================
class DNConnection
{
	friend class DiagramWindow;
public:
	DNConnection(DPCSocket& source, DPCSocket& target, uint category);
	~DNConnection();
	inline DPCSocket& GetSource(){return source;}
	inline DPCSocket& GetTarget(){return target;}
	int	GetType();
	inline uint	GetID(){return id;}
	inline uint	GetCategory(){return category;}
	ENF_DECLARE_MMOBJECT(DNConnection);
private:
	void	SetID(uint id)
	{
		this->id = id;
	}
	uint			category;
	uint			id;
	DPCSocket	source;
	DPCSocket	target;
};

//======================================================================
class DPConnetionRef
{
public:
	DPConnetionRef()
	{
		connection = NULL;
	}

	DPConnetionRef(DNConnection* connection)
	{
//		assert(connection);
		this->connection = connection;
	}
	virtual uint GetID() const
	{
		return connection->GetID();
	}

	inline DNConnection* GetConnection(){return connection;}

	bool operator> (const DPConnetionRef& ref)
	{
		return GetID() > ref.GetID();
	}

	bool operator< (const DPConnetionRef& ref)
	{
		return GetID() < ref.GetID();
	}

	bool operator== (const DPConnetionRef& ref)
	{
		return GetID() == ref.GetID();
	}
private:
	DNConnection* connection;
};

//======================================================================
class DPConnetionRefDummy : public DPConnetionRef
{
public:
	DPConnetionRefDummy(uint id)
		:DPConnetionRef(NULL)
	{
		this->id = id;
	}

	virtual uint GetID()
	{
		return id;
	}
private:
	uint id;
};

//======================================================================
class PCConnectionCategory
{
public:
	PCConnectionCategory(){};
	PCConnectionCategory(uint PaleteIndex);
	uint GetPaleteColorIndex() const;
	void SetPaleteColorIndex(uint PaleteIndex);
private:
	uint PaleteIndex;
};

#endif