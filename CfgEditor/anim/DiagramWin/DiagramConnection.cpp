
#include "precomp.h"
#include "DiagramConnection.h"
#include "DiagramNode.h"

//-----------------------------------------------------------------------------------------
DNConnection::DNConnection(DPCSocket& source, DPCSocket& target, uint category)
{
	this->source = source;
	this->target = target;
	this->category = category;
	source.node->AddConnection(this);
	target.node->AddConnection(this);
}

//-----------------------------------------------------------------------------------------
DNConnection::~DNConnection()
{
	source.node->RemoveConnection(this);
	target.node->RemoveConnection(this);
}

//-----------------------------------------------------------------------------------------
int DNConnection::GetType()
{
	assert(source.node);
	DCSlot* slot = source.node->GetSlot(source.slot);
	assert(slot);
	return slot->GetType();
}

//=========================================================================================
PCConnectionCategory::PCConnectionCategory(uint PaleteIndex)
{
	SetPaleteColorIndex(PaleteIndex);
}

//-----------------------------------------------------------------------------------------
void PCConnectionCategory::SetPaleteColorIndex(uint PaleteIndex)
{
	this->PaleteIndex = PaleteIndex;
}

//-----------------------------------------------------------------------------------------
uint PCConnectionCategory::GetPaleteColorIndex() const
{
	return PaleteIndex;
}
