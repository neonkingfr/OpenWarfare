
#include "precomp.h"
#include "DiagramNode.h"
#include "DiagramWindow.h"
#include "wx/dcbuffer.h"

//---------------------------------------------------------------------------------
bool RectAOverlapB(const wxRect& a, const wxRect& b)
{
	if(a.x + a.width < b.x) return false;
	if(a.x > b.x + b.width) return false;
	if(a.y + a.height < b.y) return false;
	if(a.y > b.x + b.height) return false;
	return true;
}

//---------------------------------------------------------------------------------
void DCSlot::SetOwner(DiagramNode* owner)
{
	assert(owner);
	this->owner = owner;
}

//---------------------------------------------------------------------------------
FPoint DCSlot::GetConnectionPoint()
{
	FPoint pt = owner->GetPos() + rect.GetPos();
	FSize sz = rect.GetSize();
	sz.y *= 0.5f;
	pt.y += sz.y;

	if(alignment == DCSA_RIGHT)
	{
		pt.x += sz.x;
		pt.x -= 4.0f;
	}

	return pt;
//	return owner->GetPos() + rect.GetPos() + (rect.GetSize() * 0.5f);
}

//---------------------------------------------------------------------------------
DiagramNode::DiagramNode(const wxString& name)
{
	this->name = name;
	canvas = NULL;
	visible = true;
	ClientData = NULL;
	selected = false;
}

//---------------------------------------------------------------------------------
DiagramNode::~DiagramNode()
{
	for(uint n = 0; n < slots.GetCardinality(); n++)
	{
		DCSlot* slot = slots[n];
		delete slot;
	}
	slots.Clear();

	for(uint n = 0; n < DNDAreas.GetCardinality(); n++)
	{
		DNDArea* area = DNDAreas[n];
		delete area;
	}
	DNDAreas.Clear();
}

//---------------------------------------------------------------------------------
void DiagramNode::SetCanvas(DiagramWindow* canvas)
{
	assert(canvas);
	this->canvas = canvas;
}

//---------------------------------------------------------------------------------
void DiagramNode::SetPos(const FPoint& pos)
{
	rect.SetPos(pos);
}

//---------------------------------------------------------------------------------
void DiagramNode::SetSize(const FSize& size)
{
	rect.SetSize(size);
}

//---------------------------------------------------------------------------------
FPoint DiagramNode::GetPos()
{
	return rect.GetPos();
}

//---------------------------------------------------------------------------------
FSize DiagramNode::GetSize()
{
	return rect.GetSize();
}

//---------------------------------------------------------------------------------
void DiagramNode::RepaintContent(wxDC& dc)
{
	wxPoint RightDown = canvas->VirtualToScreen(GetPos() + GetSize());
	wxPoint pos = canvas->VirtualToScreen(GetPos());
	wxPoint off = RightDown - pos;
	wxSize size(off.x, off.y);//canvas->VirtualToScreen(GetSize());

	wxRect BBox(pos, size);

	if(RectAOverlapB(BBox, wxRect(wxPoint(0, 0), canvas->GetClientSize())))
	{
		dc.SetBrush(*canvas->GetNodeBackgroundBrush());
		dc.SetPen(*wxBLACK_PEN);

		for(uint n = 0; n < slots.GetCardinality(); n++)
		{
			DCSlot* slot = slots[n];

			FPoint SlotPt = slot->GetRect().GetPos();
			FSize SlotSize = slot->GetRect().GetSize();

			wxPoint pt = canvas->VirtualToScreen(GetPos() + SlotPt);
			wxSize sz = canvas->VirtualToScreen(SlotSize);
			pt.x -= 1;

			wxPoint pts[3];
			pts[0] = wxPoint(0, 0);
			pts[1] = wxPoint(sz.x, sz.y / 2);
			pts[2] = wxPoint(0, sz.y);

			dc.DrawPolygon(3, pts, pt.x, pt.y, wxODDEVEN_RULE);

	//		FPoint center(SlotPt.x + (SlotSize.x * 0.5f), SlotPt.y + (SlotSize.y * 0.5f));
	//		dc.DrawCircle(canvas->VirtualToScreen(GetPos() + center), sz.x);
	//		dc.DrawRectangle(pt.x, pt.y, sz.x, sz.y);
		}

		if(IsSelected())
		{
			if(IsFocused())
			{
				dc.SetBrush(*canvas->GetNodeFocusedBackgroundBrush());
				dc.SetPen(*wxCYAN_PEN);
			}
			else
			{
				dc.SetBrush(*canvas->GetNodeSelectedBackgroundBrush());
				dc.SetPen(*wxBLACK_PEN);
			}
		}

//		dc.DrawRectangle(pos.x, pos.y, size.x, size.y);
		dc.DrawRoundedRectangle(pos.x, pos.y, size.x, size.y, (int)(canvas->GetZoom() * 10.0f));
//		int LineOff = canvas->VirtualToScreen(16);
//		dc.DrawLine(pos.x, pos.y + LineOff, pos.x + size.x, pos.y + LineOff);

		if(canvas->GetZoomLevel() > 0/*font*/)
		{
			wxFont* font = canvas->GetZoomFont();
			wxSize fz = font->GetPixelSize();
			int FontHeight = abs(fz.y);
			int HeightOffset = (size.y - FontHeight) / 2;
			dc.SetFont(*font);
//			dc.DrawText(GetName(), pos.x + 5, pos.y + (abs(fz.y) / 2));
			dc.DrawText(GetName(), pos.x + 5, pos.y + HeightOffset);
		}
	}

	for(uint n = 0; n < DNDAreas.GetCardinality(); n++)
	{
		DNDArea* area = DNDAreas[n];

		if(area->IsVisible())
			area->RepaintContent(canvas, dc);
	}
}

//---------------------------------------------------------------------------------
uint DiagramNode::AddConnectionSlot(DCSlot* slot)
{
	slot->SetOwner(this);
	return slots.Insert(slot);
}

//---------------------------------------------------------------------------------
DCSlot* DiagramNode::GetSlot(uint slot)
{
	return slots[slot];
}

//---------------------------------------------------------------------------------
bool DiagramNode::AddConnection(DNConnection* connection)
{
	connections.Insert(connection);
	return true;
}

//---------------------------------------------------------------------------------
void DiagramNode::RemoveConnection(DNConnection* connection)
{
	connections.Remove(DPConnetionRef(connection));
}

//---------------------------------------------------------------------------------
FSize DiagramNode::OrganizeSlots()
{
//right side slots
	FSize size = GetSize();
	float TopOffset = (size.y - SlotHeight) * 0.5f;

	float x = -SlotWidth, y = TopOffset;

	for(uint n = 0; n < slots.GetCardinality(); n++)
	{
		DCSlot* slot = slots[n];

		if(slot->GetDirection() != DCST_INPUT)
			continue;

		FRect& rect = slot->rect;

		rect.x = x;
		rect.y = y;
		rect.width = SlotWidth;
		rect.height = SlotHeight;
		y += SlotHeight;
	}

	x = size.x;
	y = TopOffset;

	for(uint n = 0; n < slots.GetCardinality(); n++)
	{
		DCSlot* slot = slots[n];

		if(slot->GetDirection() != DCST_OUTPUT)
			continue;

		FRect& rect = slot->rect;

		rect.x = x;
		rect.y = y;
		rect.width = SlotWidth;
		rect.height = SlotHeight;
		y += SlotHeight;
	}

	FSize oversize;
	FPoint RightDown;

	for(uint n = 0; n < slots.GetCardinality(); n++)
	{
		DCSlot* slot = slots[n];
		FRect& rect = slot->rect;
		RightDown = rect.GetPos() + rect.GetSize();

		if(RightDown.x > oversize.x)
			oversize.x = RightDown.x;

		if(RightDown.y > oversize.y)
			oversize.y = RightDown.y;
	}

	return oversize;
}

//---------------------------------------------------------------------------------
bool DiagramNode::IsFocused()
{
	return (canvas->GetFocusedSelection() == this);
}

//---------------------------------------------------------------------------------
void DiagramNode::SetClientData(void* data)
{
	ClientData = data;
}

//---------------------------------------------------------------------------------
void* DiagramNode::GetClientData()
{
	return ClientData;
}

//---------------------------------------------------------------------------------
void DiagramNode::OnSelect(bool select)
{
	selected = select;
}

//---------------------------------------------------------------------------------
uint DiagramNode::AddDNDArea(DNDArea* area)
{
	enf_assert(area);
	return DNDAreas.Insert(area);
}

//---------------------------------------------------------------------------------
DNDArea* DiagramNode::CheckDNDArea(const FPoint& pt)
{
	for(uint n = 0; n < DNDAreas.GetCardinality(); n++)
	{
		DNDArea* area = DNDAreas[n];

		if(area->GetRect().Contains(pt))
			return area;
	}	
	return NULL;
}

//=================================================================================
DNDArea::DNDArea(DiagramNode* owner, int id, const FPoint& pos, const FSize& size, int flags, uint PaleteColorIndex)
{
	this->owner = owner;
	this->id = id;
	this->flags = flags;
	this->PaleteColorIndex = PaleteColorIndex;
	rect.SetPos(pos);
	rect.SetSize(size);
	visible = false;
}

//---------------------------------------------------------------------------------
void DNDArea::RepaintContent(DiagramWindow* canvas, wxDC& dc)
{
	FPoint LeftUp = owner->GetPos() + rect.GetPos();
	FPoint RightDown = LeftUp + rect.GetSize();
	wxPoint lu = canvas->VirtualToScreen(LeftUp);
	wxPoint rd = canvas->VirtualToScreen(RightDown);
	wxPoint sz = rd - lu;

//	dc.SetPen(wxPen(canvas->GetPaleteColor(PaleteColorIndex)));
	wxBrush OldBrush = dc.GetBrush();
	dc.SetBrush(wxBrush(canvas->GetPaleteColor(PaleteColorIndex), wxSOLID));
	dc.DrawRoundedRectangle(lu.x, lu.y, sz.x, sz.y, (int)(canvas->GetZoom() * 10.0f));
	dc.SetBrush(OldBrush);
//	dc.DrawRectangle(lu.x, lu.y, sz.x, sz.y);
}

//---------------------------------------------------------------------------------
void DNDArea::SetPaleteColorIndex(uint index)
{
	PaleteColorIndex = index;
}