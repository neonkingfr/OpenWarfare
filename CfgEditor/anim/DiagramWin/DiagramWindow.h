#ifndef DIAGRAM_WINDOW_H
#define DIAGRAM_WINDOW_H

#include "DiagramConnection.h"
#include "Floating2D.h"
#include "wx/scrolwin.h"
#include "common/enf_hashmap.h"

class wxWindow;
class wxBitmap;
class DiagramNode;
class DNDArea;
class DNConnection;
class DiagramWindowPanel;

//--------- EVT_DIAGRAMWIN event declaration START ------------------
BEGIN_DECLARE_EVENT_TYPES()
    DECLARE_EVENT_TYPE(wxEVT_DIAGRAMWIN, 7980)
END_DECLARE_EVENT_TYPES()

#define EVT_DIAGRAMWIN(winid, func) wx__DECLARE_EVT1(wxEVT_DIAGRAMWIN, winid, wxCommandEventHandler(func))

//--------- EVT_DIAGRAMWIN event declaration END ------------------

enum DiagWinEvtType
{
	DWET_NodesMoved = 1,
	DWET_ZoomChanged,
	DWET_ViewPointChanged,
	DWET_NodeMenu,
	DWET_DragAndDrop,
	DWET_SelectionChanged,
	DWET_Key,
};

//=====================================================================================
//event data base class
//=====================================================================================
class DiagWinEvent : public wxClientData
{
public:
	inline DiagWinEvtType GetType(){return type;}
	DiagWinEvent(DiagWinEvtType type) : wxClientData()
	{
		this->type = type;
	}
private:
	DiagWinEvtType type;
};

//=====================================================================================
class DWE_NodesMoved : public DiagWinEvent
{
public:
	DWE_NodesMoved(const EArray<DiagramNode*>* nodes) : DiagWinEvent(DWET_NodesMoved)
	{
		this->nodes = nodes;
	}
	inline const EArray<DiagramNode*>* GetNodes() {return nodes;}
private:
	const EArray<DiagramNode*>* nodes;
};

//=====================================================================================
class DWE_ZoomChanged : public DiagWinEvent
{
public:
	DWE_ZoomChanged(uint zoom) : DiagWinEvent(DWET_ZoomChanged)
	{
		this->zoom = zoom;
	}
	inline uint GetZoom() {return zoom;}
private:
	uint zoom;
};

//=====================================================================================
class DWE_ViewPointChanged : public DiagWinEvent
{
public:
	DWE_ViewPointChanged(const FPoint& point) : DiagWinEvent(DWET_ViewPointChanged)
	{
		this->point = point;
	}
	const FPoint& GetPoint() {return point;}
private:
	FPoint point;
};

//=====================================================================================
class DWE_NodeMenu : public DiagWinEvent
{
public:
	DWE_NodeMenu(DiagramNode* node, wxPoint point) : DiagWinEvent(DWET_NodeMenu)
	{
		this->node = node;
		this->point = point;
	}
	inline DiagramNode* GetNode() {return node;}
	inline const wxPoint& GetPoint(){return point;}
private:
	DiagramNode* node;
	wxPoint point;
};

//=====================================================================================
class DWE_DragAndDrop : public DiagWinEvent
{
public:
	DWE_DragAndDrop(DNDArea* source, DNDArea* target) : DiagWinEvent(DWET_DragAndDrop)
	{
		this->source = source;
		this->target = target;
	}
	inline DNDArea* GetSource() {return source;}
	inline DNDArea* GetTarget() {return target;}
private:
	DNDArea* source;
	DNDArea* target;
};

//=====================================================================================
class DWE_SelectionChanged : public DiagWinEvent
{
public:
	DWE_SelectionChanged() : DiagWinEvent(DWET_SelectionChanged)
	{
	}
private:
};

//=====================================================================================
class DWE_Key : public DiagWinEvent
{
public:
	DWE_Key(int keycode, bool down) : DiagWinEvent(DWET_Key)
	{
		this->keycode = keycode;
		this->down = down;
	}
	inline int GetKeyCode(){return keycode;}
	bool IsDown(){return down;}
private:
	int keycode;
	bool down;
};

enum
{
	DWS_NONE = 0,
	DWS_DRAGGING_CANVAS,
	DWS_SQUARE_SELECTING,
	DWS_MOVING_SELECTION,
	DWS_DRAG_AND_DROP,
};

//======================================================================
class DiagramWindow : public wxPanel
{
public:
	DiagramWindow(DiagramWindowPanel* parent, wxApp* application);
	~DiagramWindow();
	void SetCanvasSize(int width, int height);
	void Clear();
	bool AddNode(DiagramNode* node);
	void DestroyNode(DiagramNode* node, bool RemoveFromList = true);
	DNConnection* Connect(DiagramNode* SrcNode, uint SrcSlot, DiagramNode* DestNode, uint DestSlot, uint ConCategory);
	DiagramNode* FindNode(const char* name);
	void RepaintContent(wxDC& dc);
	void RepaintFromClient();
	wxFont* GetZoomFont();
	wxFont* GetLargestZoomFont();
	DiagramNode* GetNodeFromScreen(const wxPoint& pos, FPoint& PickOffsetResult);
	inline float GetZoom(){return zoom;}
	inline uint GetZoomLevel(){return ZoomLevel;}
	void SetZoomLevel(uint level);
	void SetViewPoint(FPoint pt);
	const FPoint& GetViewPoint();
	wxPoint VirtualToScreen(const FPoint& point);
	wxSize VirtualToScreen(const FSize& size);
	float VirtualToScreen(float f);
	FPoint ScreenToVirtual(const wxPoint& point);
	FSize ScreenToVirtual(const wxSize& size);
	float ScreenToVirtual(int size);
	float CalculateNodeVirtualWidth(const char* text);
	void ClearSelection();
	uint GetSelectionCount();
	DiagramNode* GetSelectionMember(uint n);
	DiagramNode* GetFocusedSelection();
	bool SetFocusedSelection(DiagramNode* node);
	void AddToSelection(DiagramNode* node);
	void AddToSelection(EArray<DiagramNode*>& nodes);
	void Select(EArray<DiagramNode*>& nodes, bool GenerateEvent = true);
	void RemoveFromSelection(DiagramNode* node);
	void RemoveFromSelection(EArray<DiagramNode*>& nodes);
	FRect GetBoundBoxOfNodes(EArray<DiagramNode*>& nodes);
	FRect GetVisibleRect();
	inline const wxBrush* GetBackgroundBrush(){return BackgroundBrush;}
	inline const wxBrush* GetNodeBackgroundBrush(){return NodeBackgroundBrush;}
	inline const wxBrush* GetNodeSelectedBackgroundBrush(){return NodeSelectedBackgroundBrush;}
	inline const wxBrush* GetNodeFocusedBackgroundBrush(){return NodeFocusedBackgroundBrush;}
	inline const wxBrush* GetSquareSelectingBrush(){return SquareSelectingBrush;}
	inline const wxPen* GetSquareSelectingPen(){return SquareSelectingPen;}
	uint AddConnectionCategory(const PCConnectionCategory& category);
	const PCConnectionCategory& GetConnectionCategory(uint category);
	uint AddPaleteColor(const wxColour& color);
	const wxColour& GetPaleteColor(uint index);
	void SelectConnection(DiagramNode* SrcNode, uint SrcSlot, DiagramNode* DestNode, uint DestSlot, uint PaleteColorIndex);
	void UnselectConnection();
	void OnMainFrameActivate(bool active);	//hlavne okno bolo aktivovano/deactivovano
	inline ESet<DPConnetionRef>& GetConnections(){return connections;}
	void DrawOnlyLines(bool lines);
	inline bool ConnectionSelected(){return SelectedConnection != NULL;}
private:
	EHashMap<const char*, DiagramNode*, StringHashFunc> nodes;
	EArray<DiagramNode*> NodesArray;
	ESet<DPConnetionRef> connections;
	EArray<PCConnectionCategory> ConCategories;
	uint LastConnectionID;
	wxPoint LastMousePos;
	wxPoint LastRMBDownPos;
	wxPoint CanvasDragStart;
	EArray<wxFont*> ZoomFonts;
	EArray<float> ZoomLevels;
	wxBrush* BackgroundBrush;
	wxBrush* NodeBackgroundBrush;
	wxBrush* NodeSelectedBackgroundBrush;
	wxBrush* NodeFocusedBackgroundBrush;
	wxBrush* SquareSelectingBrush;
	wxPen* SquareSelectingPen;
	wxBitmap* PaintBitmap;
	DiagramNode* DragNode;
	int status;
	DNDArea* DragAndDropSource;
	DNDArea* DragAndDropTarget;
	uint DragAndDropTargetPI;
	wxPoint MousePos;
	wxPoint MovingSelPickPoint;
	FPoint DragAndDropSourcePoint;
	FPoint SquareSelPoint1;
	FPoint SquareSelPoint2;
	DNDArea* UnderCursorArea;
	FPoint DragNodeOffset;
	EArray<DiagramNode*> selection;
	wxFont* ZoomFont;
	EArray<wxColour> PaleteColors;
	float zoom;
	uint ZoomLevel;
	FPoint ViewPoint;
	FPoint ScrollPos;
	DiagramWindowPanel* owner;
	DNConnection* SelectedConnection;
	wxRect ScreenCursorClipArea;
	bool scrolling;
	int LastTime;
	wxApp* application;
	bool OnlyLinesDraw;
//	DiagramNode* DraggingNode;
	void SortNodes();
	void UpdateScrolling();
	void UpdateSelectionMoving();
	void ClipCursorInside(bool OnOff);
	void DestroyConnections(ESet<DPConnetionRef>& connections);
	void DestroyNodes();
	void UpdateScrollPos();
	void RecreatePaintBitmap();
	void GetBBoxFromPoints(const wxPoint& p1, const wxPoint& p2, wxPoint& LeftUp, wxPoint& RightDown);
	void OnZoomChanged();
	void OnSelectionChanged();
	void OnViewPointChanged();
	void SendKeyEvent(int keycode, bool down);
//	wxMemoryDC*	dc;
//	wxBitmap* bitmap;
	void OnSize(wxSizeEvent& event);
	void OnPaint(wxPaintEvent& event);
	void OnErase(wxEraseEvent& event);
	void OnScroll(wxScrollWinEvent& event);
	void OnLMBdown(wxMouseEvent& event);
	void OnLMBup(wxMouseEvent& event);
	void OnLMBDoubleClick(wxMouseEvent& event);
	void OnRMBdown(wxMouseEvent& event);
	void OnRMBup(wxMouseEvent& event);
	void OnMouseMove(wxMouseEvent& event);
	void OnMouseWheel(wxMouseEvent& event);
	void OnIdle(wxIdleEvent& event);
	void OnKeyDown(wxKeyEvent& event);
	void OnKeyUp(wxKeyEvent& event);
	void OnMouseCaptureLost(wxMouseCaptureLostEvent& event);
protected:
//	virtual void OnNodeMoved(DiagramNode* node);
	DECLARE_EVENT_TABLE();
};

#endif


//======================================================================
class DiagramWindowPanel : public wxPanel
{
public:
	DiagramWindowPanel(wxWindow* parent, wxApp* application);
	inline DiagramWindow* GetDiagramWin(){return diagram;}
	void SetVirtSize(const FSize& size);
	FSize GetVirtSize();
private:
	DiagramWindow*		diagram;
	FSize					VirtualSize;
	wxScrollBar*		HScrollBar;
	wxScrollBar*		VScrollBar;
};