
#ifndef STATE_FIND_DIALOG_H
#define STATE_FIND_DIALOG_H

#include "wx/panel.h"
#include "wx/vlbox.h"

class wxButton;
class wxComboBox;
class wxListBox;
class ASPrefixMask;
class wxStaticText;
class StateFindDialog;

enum
{
	BUTTON_FIND,
	COMBOBOX_FIRST_PREFIX_GROUP,
	COMBOBOX_FIRST_PREFIX_GROUP2 = 100,
	EDITBOX_STRING,
	CHECKBOX_DECL_ONLY_FIND,
	ID_CHANGE_OPERATION,
	MENU_FORWARD_SELECTION
};

class SFListBoxItem
{
public:
	wxString name;
	int IconIndex;
};

//===================================================================
//virtual owner draw listbox with state find result
//===================================================================
class SFListBox : public wxVListBox
{
	friend class StateFindDialog;
public:
	SFListBox(StateFindDialog* parent);
	void Append(SFListBoxItem& item);
	void ClearAll();
private:
	virtual wxCoord OnMeasureItem(size_t n) const;
	virtual void OnDrawItem(wxDC& dc, const wxRect& rect, size_t n) const;
	virtual void OnDrawSeparator(wxDC& dc, wxRect& rect, size_t n) const;
	const wxString& GetString(int n);
	EArray<SFListBoxItem> StateItems;
	bool dragging;
	bool ReselectOnLMBUp;
	StateFindDialog* dialog;
	void OnKeyDown(wxKeyEvent& event);
	void OnLMBDown(wxMouseEvent& event);
	void OnLMBUp(wxMouseEvent& event);
	void OnLMBDoubleClick(wxMouseEvent& event);
	void OnRMBDown(wxMouseEvent& event);
	void OnRMBUp(wxMouseEvent& event);
	void OnMouseMove(wxMouseEvent& event);
	DECLARE_EVENT_TABLE()
};

//===================================================================
//dialog to find states from "prefix mask"
//===================================================================
class StateFindDialog : public wxDialog
{
public:
	StateFindDialog(wxWindow* parent);
	~StateFindDialog();
	void ShowNormal(const bool show);
	void ChangeOperation();
	void UpdateResult();
	void OnCheckBoxClick(wxCommandEvent& event);
	void ForwardSelectionToEditor();
private:
	wxStaticText*			ResultLabel;
	SFListBox*				ResultListBox;
//	wxButton*				FindButton;
	wxButton*				  ExitButton;
	wxButton*	        OperationButton;
	wxTextCtrl*       PropertiesName;
	wxTextCtrl*       PropertiesValue;
	wxCheckBox*       DeclOnlyCheck;
	int               cmpOperation;

	EArray<wxComboBox*>	PrefixCombos;
	EArray<wxComboBox*>	PrefixCombos2;
	wxTextCtrl*				StringBox;
	ASPrefixMask*			FindMasks[2];
	void UpdatePrefixMask();
	void OnComboBoxChanged(wxCommandEvent& event);
	void OnButtonClick(wxCommandEvent& event);
	void OnKeyDown(wxKeyEvent& event);
	void OnTextChanged(wxCommandEvent& event);
	void OnMenu(wxCommandEvent& event);
	DECLARE_EVENT_TABLE()
};

#endif