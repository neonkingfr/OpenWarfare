
#include "precomp.h"
#include "ParamConfigAnm.h"
#include "common/wxConvert.h"
#include "common/NativeConvert.h"
#include "common/NativeUtil.h"
#include "tinyxml/tinyxml.h"
#include "shlwapi.h"
#include "animeditor.h"

CResult LoadingResult;

//-------------------------------------------------------------------------------
ParamConfigAnm* GetAnimConfigPtr()
{
	return (ParamConfigAnm*)GetConfigPtr();
}

//===============================================================================
AnimStateID::AnimStateID()
{
}

//-------------------------------------------------------------------------------
AnimStateID::AnimStateID(const AnimStateID& id)
{
	Copy(id);
}

//-------------------------------------------------------------------------------
AnimStateID::AnimStateID(const char* name)
{
	assert(name);
	assert(strlen(name) > 0);
	this->name = name;	//every id must store name
	CreateMasks();
}

//-------------------------------------------------------------------------------
AnimStateID::~AnimStateID()
{
	DestroyMasks();
}

//this create a virtual "prefix masks" for searching/sorting. every prefix sequence of valid prefixes like AmovPercMstp creates single prefix mask.
//e.g from state name "AmovPercMstpSlowWrflDnon_AmovPercMevaSrasWrflDl" will be created two prefix masks because AmovPercMstpSlowWrflDnon and AmovPercMevaSrasWrflDl are two prefix sequencies separeates with char "_"
//-------------------------------------------------------------------------------
void AnimStateID::CreateMasks()
{
	DestroyMasks();
	const char* name = this->name.c_str();
	uint group, prefix, PrefixLength, LastPrefixLength;
	const char* LastPrefix = name;
	const char* str = g_animdef->FindFirstPrefixInString(name, &group, &prefix, &PrefixLength);
	ASPrefixMask* mask = NULL;
	LastPrefixLength = 0;

	while(str)
	{
		int off = str - LastPrefix;
		const char* temp = str; temp--;
		bool MaskCreated = false;

		if(str == name || (off > LastPrefixLength && *(temp) == '_'))	//novu masku vytvarame ked prefix je hned na zaciatku nazvu alebo ked najdeny prefix nenasleduje hned za predchadzajucim a pritom zacina podtrzitkom
		{
			mask = new ASPrefixMask();
			masks.Insert(mask);
			MaskCreated = true;
		}

		if(!MaskCreated && off > LastPrefixLength)	//ak sme nasli prefix s odstupom od predchadzajucej serie a nebola vytvorena dalsia maska tak do masky uz nic zapisovat nebudeme
			mask = NULL;

		if(!mask)
			break;

		mask->Set(group, prefix);

		LastPrefixLength = PrefixLength;
		const char* NextPrefixPos = str + PrefixLength;
		const char* NextPrefix = NextPrefixPos ? g_animdef->FindFirstPrefixInString(NextPrefixPos, &group, &prefix, &PrefixLength) : NULL;

		LastPrefix = str;
		str = NextPrefix;
	}

//	enf_assert(masks.GetCardinality() <= 2);
}

//-------------------------------------------------------------------------------
void AnimStateID::DestroyMasks()
{
	for(uint n = 0; n < masks.GetCardinality(); n++)
	{
		ASPrefixMask* mask = masks[n];
		delete mask;
	}

	masks.Clear();
}

//-------------------------------------------------------------------------------
const ASPrefixMask* AnimStateID::GetMask(uint mask) const
{
	return masks[mask];
}

//-------------------------------------------------------------------------------
bool AnimStateID::IsPrefixed() const
{
	return (masks.GetCardinality() > 0);
}

//-------------------------------------------------------------------------------
uint AnimStateID::GetMaskCount() const
{
	return masks.GetCardinality();
}

//-------------------------------------------------------------------------------
bool AnimStateID::operator> (const AnimStateID& id) const
{
//	return (strcmpi(name.c_str(), id.name.c_str()) > 0);

	if(IsPrefixed() == id.IsPrefixed())
	{
		if(IsPrefixed())
		{
			if(GetMaskCount() == id.GetMaskCount())
			{
				return (strcmpi(name.c_str(), id.name.c_str()) > 0);
/*
				for(uint n = 0; n < masks.GetCardinality(); n++)
				{
					if(masks[n] > id.masks[n])
						return true;
				}
				return false;*/
			}
			else
				return (GetMaskCount() > id.GetMaskCount());
		}
		else
			return (strcmpi(name.c_str(), id.name.c_str()) > 0);
	}
	else
		return IsPrefixed();
}

//-------------------------------------------------------------------------------
bool AnimStateID::operator< (const AnimStateID& id) const
{
//	return (strcmpi(name.c_str(), id.name.c_str()) < 0);

	if(IsPrefixed() == id.IsPrefixed())
	{
		if(IsPrefixed())
		{
			if(GetMaskCount() == id.GetMaskCount())
			{
				return (strcmpi(name.c_str(), id.name.c_str()) < 0);
/*				for(uint n = 0; n < masks.GetCardinality(); n++)
				{
					if(masks[n] < id.masks[n])
						return true;
				}
				return false;*/
			}
			else
				return (GetMaskCount() < id.GetMaskCount());
		}
		else
			return (strcmpi(name.c_str(), id.name.c_str()) < 0);
	}
	else
		return !IsPrefixed();
}

//-------------------------------------------------------------------------------
bool AnimStateID::operator== (const AnimStateID& id) const
{
	return (strcmpi(name.c_str(), id.name.c_str()) == 0);

/*	if(IsPrefixed() == id.IsPrefixed())
	{
		if(IsPrefixed())
		{
			if(GetMaskCount() == id.GetMaskCount())
			{
				for(uint n = 0; n < masks.GetCardinality(); n++)
				{
					if((masks[n] == id.masks[n]) == false)
						return false;
				}
				return true;
			}
			else
				return false;
		}
		else
			return (strcmpi(name.c_str(), id.name.c_str()) == 0);
	}
	else
		return false;*/
}

//-------------------------------------------------------------------------------
void AnimStateID::operator= (const AnimStateID& id)
{
	Copy(id);
}

//-------------------------------------------------------------------------------
void AnimStateID::operator= (const char* name)
{
	assert(name);
	assert(strlen(name) > 0);
	this->name = name;
	CreateMasks();
}

//-------------------------------------------------------------------------------
void AnimStateID::Copy(const AnimStateID& id)
{
	name = id.name;
	DestroyMasks();

	for(uint n = 0; n < id.masks.GetCardinality(); n++)
	{
		ASPrefixMask* mask = id.masks[n];
		ASPrefixMask* NewMask = new ASPrefixMask(*mask);
		masks.Insert(NewMask);
	}
}

//---------------------------------------------------------------------------------
ParamConfigAnm::ParamConfigAnm()
	:ParamConfig()
{
	CurCharacter = -1;
}

//---------------------------------------------------------------------------------
ParamConfigAnm::~ParamConfigAnm()
{
	Clear();
}

//-------------------------------------------------------------------------------
void ParamConfigAnm::Clear()
{
	CurCharacter = -1;
	ParamConfig::Clear();
}
/*
//-------------------------------------------------------------------------------
const char* ParamConfigAnm::GetWorkFileExtension()
{
	return ".xml";
}*/

//-------------------------------------------------------------------------------
DString ParamConfigAnm::GetSaveFilename()
{
	char buf[256];
	if(taBeforeLastChar(FileName.c_str(), buf, "."))
	{
		strcat(buf, ".xml");
		return buf;
	}
	return FileName;
}

//-------------------------------------------------------------------------------
CResult ParamConfigAnm::Load(const char* FileName)
{
	Clear();
	
	wxString ConfigFile = FileName;
	ConfigFile = ConfigFile.BeforeLast('.');
	ConfigFile += "_def.xml";

	bool CreatedDeafultConfigDef = false;
	CResult res = ConfigDef->Load(ConfigFile.c_str(), &CreatedDeafultConfigDef);

	if(res == false)
		return res;

	TiXmlDocument* doc = new TiXmlDocument();

	res = doc->LoadFile(FileName);

	if(res == false)
	{
		SAFE_DELETE(doc);
		DString msg("Cannot load file ");
		msg += FileName;
		res.SetComment(msg.c_str());
		return res;
	}

	TiXmlElement* root = doc->RootElement();

	if(!root)
	{
		wxString msg = "Cannot find xml root element in ";
		msg += FileName;
		msg += " (bad format)";
		res.Set(false, msg.c_str());
		SAFE_DELETE(doc);

		if(CreatedDeafultConfigDef)
			wxRemoveFile(ConfigFile);

		return res;
	}

	TiXmlNode* FileClassNode = root->FirstChild();
//	enf_assert(FileClassNode);
	TiXmlElement* FileClassElm = FileClassNode ? FileClassNode->ToElement() : NULL;
	bool FormatIsValid = (FileClassElm != NULL);

	if(FormatIsValid && strcmpi(FileClassElm->Value(), "class") != 0)
		FormatIsValid = false;

	if(FormatIsValid)
	{
		const char* NameAttr = FileClassElm->Attribute("name");
		if(!NameAttr || strcmpi(NameAttr, "file") != 0)
      FormatIsValid = false;
    else
    {

    }
	}

	if(!FormatIsValid)
	{
		wxString msg = "Cannot load file ";
		msg += FileName;
		msg += ". Unknown format";
		res.Set(false, msg.c_str());
		SAFE_DELETE(doc);

		if(CreatedDeafultConfigDef)
			wxRemoveFile(ConfigFile);

		return res;
	}

	TiXmlElement* EditorElm = GetChildElement(FileClassElm, "editor");
	enf_assert(EditorElm);

	if(EditorElm)
	{
		TiXmlElement* CurCharacterElm = GetChildElement(EditorElm, "CurCharacter");
		enf_assert(CurCharacterElm);
		const char* CurCharacterName = CurCharacterElm->Attribute("name");

  	enf_assert(CurCharacterName);
		int CharacterIndex = -1;
		ACharacterClass* character = FindCharacter(CurCharacterName, &CharacterIndex);

		if(character)
			CurCharacter = CharacterIndex;
		else
			CurCharacter = -1;

		TiXmlElement* BaseClassElm = GetChildElement(EditorElm, "BaseClass");
		if (BaseClassElm) 
    {
		  const char* BaseClassName = BaseClassElm->Attribute("name");
      g_aeditor->CfgMovesBasicClassName = BaseClassName;
      g_aeditor->CfgMovesBasicClassID = wxString("file/") + BaseClassName;
      }
	}

	LoadingResult.Set(true);

	if(res.IsCommented())
		LoadingResult.SetComment(res.GetComment());

	PCClass* ThisInstance = LoadClass(this, FileClassElm->ToElement());

	if(LoadingResult.IsOk())
		SetFileName(FileName);

	SAFE_DELETE(doc);
	return LoadingResult;
}

//-------------------------------------------------------------------------------
void ParamConfigAnm::Load(TiXmlElement* elm)
{

}

//-------------------------------------------------------------------------------
CResult ParamConfigAnm::Save(const char* FileName)
{
	TiXmlDocument* doc = new TiXmlDocument();
	TiXmlNode* nd = doc->InsertEndChild(TiXmlElement("animconfig"));
	TiXmlElement* elm = nd->ToElement();
	elm->SetAttribute("version", "1.0");
	SaveClass(this, elm);

	//now store some edit data
	TiXmlElement* ConfigElement = elm->FirstChild()->ToElement();
	enf_assert(ConfigElement);
	TiXmlElement* EditorElm = ConfigElement->InsertEndChild(TiXmlElement("editor"))->ToElement();
	TiXmlElement* CurCharacterElm = EditorElm->InsertEndChild(TiXmlElement("CurCharacter"))->ToElement();

	ACharacterClass* CurCharact = GetCurrentCharacterPtr();
	const char* CurCharactName = CurCharact ? CurCharact->GetName() : "";
	CurCharacterElm->SetAttribute("name", CurCharactName);

  TiXmlElement* BaseClassElm = EditorElm->InsertEndChild(TiXmlElement("BaseClass"))->ToElement();
	BaseClassElm->SetAttribute("name", g_aeditor->CfgMovesBasicClassName);

	CResult result = doc->SaveFile(FileName);

	if(result.IsOk())
		SetFileName(FileName);
	else
	{
		DString comment("Cannot save config to file ");
		comment += FileName;
		comment += "\nFile is read only or in use by another aplication probably.";
		result.SetComment(comment.c_str());
	}

	SAFE_DELETE(doc);
	return result;
}

//-------------------------------------------------------------------------------
TiXmlElement* ParamConfigAnm::SaveClass(PCClass* Cl, TiXmlElement* parent)
{
	TiXmlElement* ClElm = parent->InsertEndChild(TiXmlElement("class"))->ToElement();
	ClElm->SetAttribute("name", Cl->GetName());

	PCClass* base = Cl->GetBase();

	if(base)
		ClElm->SetAttribute("base", base->GetName());

	if(!Cl->IsDefined())
		ClElm->SetAttribute("DeclOnly", "1");

	for(uint n = 0; n < Cl->GetCommandsCount(); n++)
	{
		PCCommand* cmd = Cl->GetCommand(n);
		PCCommandA* command = dynamic_cast<PCCommandA*>(cmd);
		command->Save(ClElm);
	}

	for(uint n = 0; n < Cl->GetNodesCount(); n++)
	{
		PCNode* node = Cl->GetNode(n);

		if(node->IsExtern())
			continue;

		if(node->ToVariable())
		{
			PCVarA* var = dynamic_cast<PCVarA*>(node);
			enf_assert(var);
			var->Save(ClElm);
		}
		else if(node->ToArray())
		{
			PCArrayA* arr = dynamic_cast<PCArrayA*>(node);
			enf_assert(arr);
			arr->Save(ClElm);
		}
		else
		{
			PCClass* Class = node->ToClass();
			enf_assert(Class);

			PCClassA* cl = dynamic_cast<PCClassA*>(node);
			enf_assert(cl);

			TiXmlElement* ChildClassElm = SaveClass(cl, ClElm);
			cl->SaveEditData(ChildClassElm);
		}
	}
	return ClElm;
}

//-------------------------------------------------------------------------------
PCClass* ParamConfigAnm::LoadClass(PCClass* owner, TiXmlElement* elm)
{
	enf_assert(elm);
	for(TiXmlNode* node = elm->FirstChild(); node != NULL; node = node->NextSibling())
	{
		TiXmlElement* ClassElm = node->ToElement();

		if(!ClassElm)
			continue;

		if(strcmp(ClassElm->Value(), "class") == 0)	//child class
		{
			const char* name = ClassElm->Attribute("name");
			const char* base = ClassElm->Attribute("base");
			const char* DeclOnly = ClassElm->Attribute("DeclOnly");

			if(!name || strlen(name) == 0)
			{
				DString msg("Class ");
				msg += owner->GetName();
				msg += " contains unnamed class";
				LoadingResult.Set(false, msg.c_str());
				return NULL;
			}

			bool IsDefined = (!DeclOnly || (strcmp(DeclOnly, "0") == 0));
			PCClass* BaseClass = NULL;
			DString BaseClassId;

			if(base && strlen(base) > 0)
			{
				BaseClass = owner->FindClass(base, true, true);
				enf_assert(BaseClass);

				if(!BaseClass)
				{
					DString msg("Cannot find base class for class ");
					msg += base;
					LoadingResult.Set(false, msg.c_str());
					return NULL;
				}

				BaseClassId = BaseClass->GetId();
			}

			PCClass* TopBase = BaseClass ? BaseClass->GetTopBase() : NULL;	//definicie iba do top base classov
			const PCDefClass* definition = NULL;

/*			if(TopBase)	//if have any base, definition will get from top base
			{
				definition = TopBase->GetDefinition();
			}
			else	//este nema definiciu tak ju vytvorime
			{*/
				DString DefPath;

				if(owner)
				{
					DefPath = owner->GetId();
					DefPath += '/';
				}

				DefPath += name;
				definition = GetDefinition()->GetClassDef(DefPath.c_str());	//get existing class definition. if not exist, will created default (empty) one
//			}

			enf_assert(definition);
			DString OwnID = owner->GetId();

			PCClass* ChildClass = CreateClass(name, owner, OwnID.c_str(), BaseClass, BaseClassId.c_str(), IsDefined, definition, true, GetClassInfoFlags(owner, BaseClass));
			owner->AppendNode(ChildClass);

			LoadClass(ChildClass, ClassElm);
		}
		else if(strcmp(ClassElm->Value(), "array") == 0)
		{
			PCArray* arr = LoadArray(owner, ClassElm);
			owner->AppendNode(arr);
		}
		else if(strcmp(ClassElm->Value(), "variable") == 0)
		{
			PCVar* var = LoadVariable(owner, ClassElm);
			owner->AppendNode(var);
		}
		else if(strcmp(ClassElm->Value(), "command") == 0)
		{
			PCCommand* cmd = LoadCommand(owner, ClassElm);
			owner->AppendCommand(cmd);
		}
		else if(strcmp(ClassElm->Value(), "editor") == 0)
		{
			owner->LoadEditData(ClassElm);
		}
	}
	return this;
}

//-------------------------------------------------------------------------------
PCArray* ParamConfigAnm::LoadArray(PCNode* owner, TiXmlElement* elm)
{
	PCClass* ClOwner = owner->ToClass();
	PCArray* ArrOwner = owner->ToArray();
	const char* name = elm->Attribute("name");
	PCDefParam* ParamDef = NULL;
	DString UniName;

	if(ClOwner)	//array in class must have name
	{
		if(!name || strlen(name) == 0)
		{
			DString msg("Class ");
			msg += ClOwner->GetName();
			msg += " contains unnamed array";
			LoadingResult.Set(false, msg.c_str());
			return NULL;
		}

		const PCDefClass* ClassDefinition = ClOwner->GetDefinition();
		enf_assert(ClassDefinition);

		ParamDef = ClassDefinition ? (PCDefParam*)ClassDefinition->GetParamDef(name) : NULL;
		enf_assert(ParamDef);
	}
	else
	{
		UniName = GenerateUniqueName(UID_ARRAY);
		name = UniName.c_str();
	}

	enf_assert(name);
	PCArray* arr = CreateArray(name, owner, ParamDef);

	for(TiXmlNode* node = elm->FirstChild(); node != NULL; node = node->NextSibling())
	{
		TiXmlElement* ArrElm = node->ToElement();

		if(!ArrElm)
			continue;

		PCNode* NewNode = NULL;

		if(strcmp(ArrElm->Value(), "variable") == 0)
		{
			NewNode = LoadVariable(arr, ArrElm);
		}
		else if(strcmp(ArrElm->Value(), "array") == 0)
		{
			NewNode = LoadArray(NewNode->ToArray(), ArrElm);
		}

		if(NewNode)
			arr->Add(NewNode);
	}

	return arr;
}

//----------------------------------------------------------------------------------
PCVar* ParamConfigAnm::LoadVariable(PCNode* owner, TiXmlElement* elm)
{
	PCClass* ClOwner = owner->ToClass();
	PCArray* ArrOwner = owner->ToArray();
	const char* name = elm->Attribute("name");
	const char* value = elm->Attribute("value");
	PCDefParam* ParamDef = NULL;
	DString UniName;

	if(ClOwner)	//variable in class must have name
	{
		if(!name || strlen(name) == 0)
		{
			DString msg("Class ");
			msg += ClOwner->GetName();
			msg += " contains unnamed variable";
			LoadingResult.Set(false, msg.c_str());
			return NULL;
		}

		const PCDefClass* ClassDefinition = ClOwner->GetDefinition();
		enf_assert(ClassDefinition);

		ParamDef = ClassDefinition ? (PCDefParam*)ClassDefinition->GetParamDef(name) : NULL;
		enf_assert(ParamDef);
	}
	else	//variable inside array is unnamed but for editing must have any unique
	{
		UniName = GenerateUniqueName(UID_VARIABLE);
		name = UniName.c_str(); 
	}

	enf_assert(name);
	PCVar* var = CreateVariable(name, owner, ParamDef);

	if(value)
		var->SetValue(value);

	return var;
}

//-------------------------------------------------------------------------------
PCCommand* ParamConfigAnm::LoadCommand(PCClass* owner, TiXmlElement* elm)
{
	const char* name = elm->Attribute("name");
	const char* value = elm->Attribute("value");

	if(!name || strlen(name) == 0)
	{
		DString msg("Class ");
		msg += owner->GetName();
		msg += " contains unnamed command";
		LoadingResult.Set(false, msg.c_str());
		return NULL;
	}

	PCCommand* cmd = CreateCommand(name, owner);

	enf_assert(value);

	if(value)
		cmd->SetValue(value);

	return cmd;
}

//-------------------------------------------------------------------------------
CResult ParamConfigAnm::Import(const char* BISParamFile)
{
	EArray<PCFilename> fnames;
	fnames.Insert(PCFilename(BISParamFile));

	CResult loaded = ParamConfig::Load(fnames);

	if(loaded.IsOk())
	{
		if(characters.GetCardinality() > 0)
			CurCharacter = 0;
		else
			CurCharacter = -1;
	}

	return loaded;
}

//-------------------------------------------------------------------------------
CResult ParamConfigAnm::Export(const char* BISParamFile)
{
	return ParamConfig::Save(BISParamFile);
}

//-------------------------------------------------------------------------------
PCClass* ParamConfigAnm::GetState(const AnimStateID& id)
{
	return NULL;
}

//-------------------------------------------------------------------------------
void ParamConfigAnm::RegisterCharacterClass(ACharacterClass* character)
{
	CurCharacter = (int)characters.Insert(character);
}

//-------------------------------------------------------------------------------
void ParamConfigAnm::UnregisterCharacterClass(ACharacterClass* character)
{
	uint index = characters.GetIndexOf(character);
	enf_assert(index != INDEX_NOT_FOUND);
	characters.RemoveElementOrdered(index);

	CurCharacter = index;

	if(CurCharacter >= (int)characters.GetCardinality())
		CurCharacter--;
}

//-------------------------------------------------------------------------
ACharacterClass* ParamConfigAnm::GetCurrentCharacterPtr()
{
	if(CurCharacter == -1)
		return NULL;

	return characters[CurCharacter];
}

//-------------------------------------------------------------------------
int ParamConfigAnm::GetCurrentCharacter()
{
	return CurCharacter;
}

//-------------------------------------------------------------------------------
void ParamConfigAnm::SetCurrentCharacter(int character)
{
	enf_assert(character >= -1);
	enf_assert(character < (int)characters.GetCardinality());
	CurCharacter = character;
}

//-------------------------------------------------------------------------------
bool ParamConfigAnm::SetCurrentCharacterAndGroup(const char* CharacterName, const char* GroupName)
{
	bool result = true;

	if(CharacterName)
	{
		int SelCharacterIndex;
		ACharacterClass* SelCharacter = FindCharacter(CharacterName, &SelCharacterIndex);
		enf_assert(SelCharacter);

		if(SelCharacter)
		{
			SetCurrentCharacter(SelCharacterIndex);

			if(GroupName)
			{
				int SelGroupIndex;
				ASGroup* SelGroup = SelCharacter->FindGroup(GroupName, &SelGroupIndex);
				enf_assert(SelGroup);

				if(SelGroup)
					SelCharacter->SetCurrentGroup(SelGroupIndex);
				else
					result = false;
			}
		}
		else
			result = false;
	}
	return result;
}

//-------------------------------------------------------------------------------
ACharacterClass* ParamConfigAnm::FindCharacter(const char* name, int* position)
{
	for(uint n = 0; n < characters.GetCardinality(); n++)
	{
		ACharacterClass* ch = characters[n];

		if(strcmpi(ch->GetName(), name) == 0)
		{
			if(position)
				*position = n;

			return ch;
		}
	}

	if(position)
		*position = -1;

	return NULL;
}

//-------------------------------------------------------------------------------
uint ParamConfigAnm::GetCharactersCount()
{
	return characters.GetCardinality();
}

//-------------------------------------------------------------------------------
ACharacterClass* ParamConfigAnm::GetCharacter(uint index)
{
	return characters[index];
}

//-------------------------------------------------------------------------------
int ParamConfigAnm::GetClassInfoFlags(PCClass* owner, PCClass* base)
{
	int ResultFlags = 0;

	if(base)	//character class detection. base class must be inherited from class CfgMovesBasicClassName
	{
		bool BaseIsCharacterBase = (strcmpi(base->GetName(), g_aeditor->CfgMovesBasicClassName) == 0);

		if(BaseIsCharacterBase || base->IsInheritedFrom(g_aeditor->CfgMovesBasicClassName))
			ResultFlags |= CI_CHARACTER;
	}

	if(owner)	//state class detection. owner must be "States" and owner of "States" must be character class
	{
		if(strcmpi(owner->GetName(), "States") == 0)
		{
			PCClass* StatesClassOwner = owner->GetOwner();

			if(StatesClassOwner && dynamic_cast<ACharacterClass*>(StatesClassOwner) != NULL)
				ResultFlags |= CI_STATE;
		}
	}
	return ResultFlags;
}

//-------------------------------------------------------------------------------
PCClassA* ParamConfigAnm::CreateClass(const char* name, PCNode* owner, const char* OwnerID, PCClass* base, const char* BaseID, bool defined, const PCDefClass* definition, bool FromLoading, int ClassInfoFlags)
{
	enf_assert(OwnerID);	//owner can be null but OwnerID must be allways

	if(ClassInfoFlags & CI_CHARACTER)
	{
		return ENF_NEW ACharacterClass(name, owner, base, defined, definition);
	}
	else if(ClassInfoFlags & CI_STATE)
	{
		ASClass* state = ENF_NEW ASClass(name, owner, base, defined, definition);
/*
		if(!FromLoading)	//if class is created out of loading process, connection arrays must be added
		{
			enf_assert(definition);
			PCDefParam* ConnectToDef = (PCDefParam*)definition->GetParamDef(ConnectToArrayName);
			PCDefParam* InterpolateToDef = (PCDefParam*)definition->GetParamDef(InterpolateToArrayName);
			ConnectToDef->SetReadOnly(true);
			InterpolateToDef->SetReadOnly(true);
			state->AppendNode(ENF_NEW PCArray(ConnectToArrayName, state, ConnectToDef));
			state->AppendNode(ENF_NEW PCArray(InterpolateToArrayName, state, InterpolateToDef));
		}
*/
		return state;
	}

	return ENF_NEW PCClassA(name, owner, base, defined, definition);
}

//-------------------------------------------------------------------------------
PCArray* ParamConfigAnm::CreateArray(const char* name, PCNode* owner, const PCDefParam* definition)
{
	return ENF_NEW PCArrayA(name, owner, definition);
}

//-------------------------------------------------------------------------------
PCVar* ParamConfigAnm::CreateVariable(const char* name, PCNode* owner, const PCDefParam* definition)
{
	return ENF_NEW PCVarA(name, owner, definition);
}

//-------------------------------------------------------------------------------
PCCommand* ParamConfigAnm::CreateCommand(const char* name, PCNode* owner)
{
	return ENF_NEW PCCommandA(name, owner);
}


//==================================================================================
PCVarA::PCVarA(const char* name, PCNode* owner, const PCDefParam* definition)
	:PCVar(name, owner, definition)
{
}

//----------------------------------------------------------------------------------
void PCVarA::Save(TiXmlElement* parent)
{
	TiXmlElement* elm = parent->InsertEndChild(TiXmlElement("variable"))->ToElement();

	if(!IsInsideArray())
		elm->SetAttribute("name", GetName());

	elm->SetAttribute("value", GetValue());
}

//==================================================================================
PCArrayA::PCArrayA(const char* name, PCNode* owner, const PCDefParam* definition)
	:PCArray(name, owner, definition)
{
}

//----------------------------------------------------------------------------------
void PCArrayA::Save(TiXmlElement* parent)
{
	TiXmlElement* ArrElm = parent->InsertEndChild(TiXmlElement("array"))->ToElement();

	if(!IsInsideArray())
		ArrElm->SetAttribute("name", GetName());

	for(uint a = 0; a < GetCardinality(); a++)
	{
		PCNode* node = GetItem(a);

		if(node->ToArray())
		{
			PCArrayA* arr = dynamic_cast<PCArrayA*>(node);
			enf_assert(arr);
			arr->Save(ArrElm);
		}
		else
		{
			PCVarA* var = dynamic_cast<PCVarA*>(node);
			enf_assert(var);
			var->Save(ArrElm);
		}
	}
}

//----------------------------------------------------------------------------------
bool PCArrayA::IsConnectionArray() const
{
	ASClass* StateOwner = dynamic_cast<ASClass*>(GetOwner());

	if(StateOwner)
	{
		if( strcmpi(GetName(), ConnectToArrayName) == 0 )
			return true;

		if( strcmpi(GetName(), InterpolateToArrayName) == 0 )
			return true;
	}

	return false;
}

//----------------------------------------------------------------------------------
bool PCArrayA::IsReadOnly() const
{
	if(IsConnectionArray())
		return true;

	return PCArray::IsReadOnly();
}

//-------------------------------------------------------------------------------
bool PCArrayA::IsEqual(PCArray* other)
{
	if(!other)
		return false;

	PCArrayA* otherArray = dynamic_cast<PCArrayA*>(other);

	if( otherArray == NULL )
		return false;	//not possible but...

	if(!IsConnectionArray())
		return PCArray::IsEqual(other);

	uint count = GetCardinality();
	uint otherCount = other->GetCardinality();

	if(count != otherCount)
		return false;

	//test same content, but order can be another
	enf_assert(!(count & 1));

	for(uint n = 0; n < count; n += 2)
	{
		uint second = n + 1;

		if(second >= count)
			break;

		PCNode* item0 = GetItem(n);
		PCNode* item1 = GetItem(second);

		PCVar* var0 = item0->ToVariable();
		PCVar* var1 = item1->ToVariable();

		if(!var0)
			continue;

		if(!var1)
			continue;

		const char* StateNameFromValue = var0->GetValue();
		const char* WeightFromValue = var1->GetValue();

		PCVar* OtherWeightVar = g_aeditor->FindStateRefValueFromArray(other, StateNameFromValue);

		if(!OtherWeightVar)
			return false;

		if( strcmpi(WeightFromValue, OtherWeightVar->GetValue()) != 0 )
			return false;
	}

	return true;
}

//==================================================================================
PCCommandA::PCCommandA(const char* name, PCNode* owner)
	:PCCommand(name, owner)
{
}

//----------------------------------------------------------------------------------
void PCCommandA::Save(TiXmlElement* parent)
{
	TiXmlElement* elm = parent->InsertEndChild(TiXmlElement("command"))->ToElement();
	elm->SetAttribute("name", GetName());
	elm->SetAttribute("value", GetValue());
}

//==================================================================================
PCClassA::PCClassA(const char* name, PCNode* owner, PCClass* base, bool defined, const PCDefClass* definition)
	:PCClass(name, owner, base, defined, definition)
{
}

//-------------------------------------------------------------------------------
PCClass* PCClassA::CreateSameClass(const char* name, PCNode* owner, PCClass* base, bool defined, const PCDefClass* definition) const
{
	return ENF_NEW PCClassA(name, owner, base, defined, definition);
}

//----------------------------------------------------------------------------------
void PCClassA::LoadEditData(TiXmlElement* elm)
{
}

//----------------------------------------------------------------------------------
void PCClassA::SaveEditData(TiXmlElement* elm)
{
}

//==================================================================================
ASClass::ASClass(const char* name, PCNode* owner, PCClass* base, bool defined, const PCDefClass* definition)
	:PCClassA(name, owner, base, defined, definition)
{
	Clear();
	AnimID = name;	//operator = will create prefix mask from name

	if(this->GetConfig())	//if have config (is inserted in file structure) is a valid ASClass and not only undo/redo storage clone
	{
		ACharacterClass* character = GetCharacter();

		if(character)
			character->RegisterStateClass(this);
	}
}

//-------------------------------------------------------------------------------
ASClass::~ASClass()
{
	ACharacterClass* character = GetCharacter();

	if(character)
		character->UnregisterStateClass(this);
}

//-------------------------------------------------------------------------------
void ASClass::AfterRename(const char* OldName)
{
	ACharacterClass* character = GetCharacter();

	if(character)
	{
		DString NewName = GetName();
		SetName(OldName);
		character->UnregisterStateClass(this);	
		SetName(NewName.c_str());
		character->RegisterStateClass(this);
	}
}

//-------------------------------------------------------------------------------
ACharacterClass* ASClass::GetCharacter()
{
	PCNode* own = owner;
	ACharacterClass* character = NULL;

	while(own)
	{
		character = dynamic_cast<ACharacterClass*>(own);

		if(character)
			break;

		own = own->GetOwner();
	}
	return character;
}

//-------------------------------------------------------------------------------
ASClass*	ASClass::GetBaseState() const
{
	PCClass* BaseClass = GetBase();
	
	if(!BaseClass)
		return NULL;

	return dynamic_cast<ASClass*>(BaseClass);
}

//-------------------------------------------------------------------------------
PCClass* ASClass::CreateSameClass(const char* name, PCNode* owner, PCClass* base, bool defined, const PCDefClass* definition) const
{
	return ENF_NEW ASClass(name, owner, base, defined, definition);
}

//-------------------------------------------------------------------------------
PCClass* ASClass::Clone(PCNode* owner) const
{
	PCClass* cl = PCClass::Clone(owner);
	ASClass* state = dynamic_cast<ASClass*>(cl);
	enf_assert(state);

	for(uint n = 0; n < refs.GetCardinality(); n++)
	{
		const ASGroupRef& ref = refs[n];
		state->AddRef(ref);
	}
	return state;
}

//-------------------------------------------------------------------------------
uint ASClass::AddRef(const ASGroupRef& ref)
{
	return refs.Insert(ref);
}

//-------------------------------------------------------------------------------
bool ASClass::RemoveRef(const ASGroupRef& ref)
{
	return refs.Remove(ref);
}

//-------------------------------------------------------------------------------
void ASClass::ClearRefs()
{
	refs.Clear();
}

//-------------------------------------------------------------------------------
const ASGroupRef& ASClass::GetReference(uint member) const
{
	return refs[member];
}

//-------------------------------------------------------------------------------
ASGroupRef* ASClass::FindGroupRef(const char* name)
{
	enf_assert(name);
	ASGroupRef ref(name);
	uint index = refs.GetIndexOf(ref);

	if(index == INDEX_NOT_FOUND)
		return NULL;

	return &(refs[index]);
}

//-------------------------------------------------------------------------------
uint ASClass::GetRefsCount() const
{
	return refs.GetCardinality();
}

//----------------------------------------------------------------------------------
void ASClass::LoadEditData(TiXmlElement* elm)
{
	enf_assert(elm);
	for(TiXmlNode* node = elm->FirstChild(); node != NULL; node = node->NextSibling())
	{
		TiXmlElement* GroupRefsElm = node->ToElement();

		if(!GroupRefsElm)
			continue;

		if(strcmp(GroupRefsElm->Value(), "GroupRefs") == 0)
		{
			for(TiXmlNode* nd = GroupRefsElm->FirstChild(); nd != NULL; nd = nd->NextSibling())
			{
				TiXmlElement* RefElm = nd->ToElement();

				if(!RefElm)
					continue;

				if(strcmp(RefElm->Value(), "ref") == 0)
				{
					const char* GroupName = RefElm->Attribute("group");
					const char* GraphPosX = RefElm->Attribute("GraphPosX");
					const char* GraphPosY = RefElm->Attribute("GraphPosY");
					enf_assert(GroupName);
					enf_assert(GraphPosX);
					enf_assert(GraphPosY);
					ASGroupRef ref(GroupName);
					ref.SetGraphPos(taAtoI(GraphPosX), taAtoI(GraphPosY));
					AddRef(ref);
				}
			}
		}
	}
}

//----------------------------------------------------------------------------------
void ASClass::SaveEditData(TiXmlElement* elm)
{
	TiXmlElement* EditorElm = elm->InsertEndChild(TiXmlElement("editor"))->ToElement();
	TiXmlElement* RefsElm = EditorElm->InsertEndChild(TiXmlElement("GroupRefs"))->ToElement();

	for(uint n = 0; n < refs.GetCardinality(); n++)
	{
		const ASGroupRef& ref = refs[n];
		TiXmlElement* RefElm = RefsElm->InsertEndChild(TiXmlElement("ref"))->ToElement();
		RefElm->SetAttribute("group", ref.GetGroupName().c_str());
		char fval[128];
		taFtoA(fval, ref.GetGraphPos().x);
		RefElm->SetAttribute("GraphPosX", fval);
		taFtoA(fval, ref.GetGraphPos().y);
		RefElm->SetAttribute("GraphPosY", fval);
	}
}

//-----------------------------------------------------------------------------------
PCClassA* ASClass::GetActionsClass()
{
	PCNode* ActionsNode = GetValueNode(ActionsParamName);
	PCVar* ActionsVar = ActionsNode ? ActionsNode->ToVariable() : NULL;

	if(!ActionsVar)
		return NULL;

	ParamConfigAnm* PConfig = g_aeditor->GetParamConfig();
	PCClass* ActionsOwner = PConfig->FindClassNode(g_aeditor->CfgMovesBasicClassID);
 
	if(!ActionsOwner)
		return NULL;

	PCClass* Actions = ActionsOwner->GetClassMember(ActionsParamName);

	if(!Actions)
		return NULL;

	PCNode* ActionsCl = Actions->GetMember(ActionsVar->GetValue());

	if(!ActionsCl)
		return NULL;

	PCClass* ActionsClass = ActionsCl->ToClass();
	PCClassA* ActionsClassA = dynamic_cast<PCClassA*>(ActionsClass);
	return ActionsClassA;
}

//==================================================================================
ACharacterClass::ACharacterClass(const char* name, PCNode* owner, PCClass* base, bool defined, const PCDefClass* definition)
	:PCClassA(name, owner, base, defined, definition)
{
	registered = false;
	CurGroup = -1;

	ParamConfigAnm* config = this->GetParamConfig();

	if(config)		//if have config (is inserted to file structure) is a ACharacterClass class and not only undo/redo storage clone
	{
		config->RegisterCharacterClass(this);
		registered = true;
	}
}

//----------------------------------------------------------------------------------
ParamConfigAnm* ACharacterClass::GetParamConfig()
{
	return dynamic_cast<ParamConfigAnm*>(this->GetConfig());
}

//----------------------------------------------------------------------------------
ACharacterClass::~ACharacterClass()
{
	if(registered)
	{
		ParamConfigAnm* config = this->GetParamConfig();
		enf_assert(config);
		config->UnregisterCharacterClass(this);
	}

	ClearGroups();
}

//-------------------------------------------------------------------------------
PCClass* ACharacterClass::CreateSameClass(const char* name, PCNode* owner, PCClass* base, bool defined, const PCDefClass* definition) const
{
	return ENF_NEW ACharacterClass(name, owner, base, defined, definition);
}

//----------------------------------------------------------------------------------
PCClass*	ACharacterClass::Clone(PCNode* owner) const
{
	PCClass* cl = PCClass::Clone(owner);
	ACharacterClass* chcl = dynamic_cast<ACharacterClass*>(cl);
	enf_assert(chcl);

	for(uint n = 0; n < StateGroups.GetCardinality(); n++)
	{
		ASGroup* group = StateGroups[n];
		ASGroup* GroupClone = group->Clone();
		enf_assert(GroupClone);
		chcl->AddGroup(GroupClone);
	}

	chcl->CurGroup = CurGroup;
	return chcl;
}

//----------------------------------------------------------------------------------
ASGroup*	ACharacterClass::FindGroup(const char* name, int* index)
{
	for(uint n = 0; n < StateGroups.GetCardinality(); n++)
	{
		ASGroup* group = StateGroups[n];

		if(strcmpi(group->GetName().c_str(), name) == 0)
		{
			if(index)
				(*index) = (int)n;

			return group;
		}
	}
	return NULL;
}

//----------------------------------------------------------------------------------
void ACharacterClass::LoadEditData(TiXmlElement* elm)
{
	enf_assert(elm);
	for(TiXmlNode* node = elm->FirstChild(); node != NULL; node = node->NextSibling())
	{
		TiXmlElement* DataElm = node->ToElement();

		if(!DataElm)
			continue;

		if(strcmp(DataElm->Value(), "groups") == 0)	//groups
		{
			for(TiXmlNode* nd = DataElm->FirstChild(); nd != NULL; nd = nd->NextSibling())
			{
				TiXmlElement* GroupElm = nd->ToElement();

				if(!GroupElm)
					continue;

				if(strcmp(GroupElm->Value(), "group") == 0)
				{
					const char* GroupName = GroupElm->Attribute("name");
					const char* GraphZoom = GroupElm->Attribute("GraphZoom");
					const char* GraphViewPointX = GroupElm->Attribute("GraphViewPointX");
					const char* GraphViewPointY = GroupElm->Attribute("GraphViewPointY");
					ASGroup* group = ENF_NEW ASGroup(GroupName);

					if(GraphZoom)
						group->SetGraphZoomLevel(taAtoI(GraphZoom));

					if(GraphViewPointX && GraphViewPointY)
						group->SetGraphViewPoint(FPoint(taAtoF(GraphViewPointX), taAtoF(GraphViewPointY)));

					StateGroups.Insert(group);
				}
			}
		}
		else if(strcmp(DataElm->Value(), "CurGroup") == 0)
		{
			const char* name = DataElm->Attribute("name");
			int index = -1;
			ASGroup* group = FindGroup(name, &index);
			CurGroup = index;
		}
	}
}

//----------------------------------------------------------------------------------
void ACharacterClass::SaveEditData(TiXmlElement* elm)
{
	TiXmlElement* EditorElm = elm->InsertEndChild(TiXmlElement("editor"))->ToElement();
	TiXmlElement* GroupsElm = EditorElm->InsertEndChild(TiXmlElement("groups"))->ToElement();

	for(uint n = 0; n < StateGroups.GetCardinality(); n++)
	{
		ASGroup* group = StateGroups[n];
		group->Save(GroupsElm);
	}

	TiXmlElement* CurGroupElm = EditorElm->InsertEndChild(TiXmlElement("CurGroup"))->ToElement();

	ASGroup* CurGroup = GetCurrentGroupPtr();
	const char* CurGroupName = CurGroup ? CurGroup->GetName().c_str() : "";
	CurGroupElm->SetAttribute("name", CurGroupName);
}

//-------------------------------------------------------------------------------
uint ACharacterClass::AddGroup(ASGroup* group)
{
	enf_assert(group);
	uint result = StateGroups.Insert(group);
	CurGroup = (int)result;
	return result;
}

//-------------------------------------------------------------------------------
bool ACharacterClass::RemoveGroup(ASGroup* group)
{
	enf_assert(group);
	uint index = StateGroups.GetIndexOf(group);
	enf_assert(index != INDEX_NOT_FOUND);

	if(index == INDEX_NOT_FOUND)
		return false;

	//remove referencies to this group from all states. not needed because referencies just must be deleted
	ASGroupRef GroupRef(group->GetName().c_str());

	for(uint n = 0; n < StatesArray.GetCardinality(); n++)
	{
		ASClass* state = StatesArray[n];
		state->RemoveRef(GroupRef);
	}

	bool result = StateGroups.RemoveElementOrdered(index); 

	CurGroup = index;

	if(CurGroup >= (int)StateGroups.GetCardinality())
		CurGroup--;

	return result;
}

//---------------------------------------------------------------------------------------------
ASGroup* ACharacterClass::GetGroup(uint group)
{
	return StateGroups[group];
}

//---------------------------------------------------------------------------------------------
uint ACharacterClass::GetGroupsCount()
{
	return StateGroups.GetCardinality();
}

//---------------------------------------------------------------------------------
void ACharacterClass::ClearGroups()
{
	for(uint n = 0; n < StateGroups.GetCardinality(); n++)
	{
		ASGroup* group = StateGroups[n];
		delete group;
	}
	StateGroups.Clear();
}

//---------------------------------------------------------------------------------------------
int ACharacterClass::GetCurrentGroup()
{
	return CurGroup;
}

//---------------------------------------------------------------------------------------------
ASGroup* ACharacterClass::GetCurrentGroupPtr()
{
	if(CurGroup == -1)
		return NULL;

	return StateGroups[CurGroup];
}

//---------------------------------------------------------------------------------------------
bool ACharacterClass::SetCurrentGroup(int group)
{
	bool ValidGroup = (group >= -1) && (group < (int)StateGroups.GetCardinality());
	enf_assert(ValidGroup);
	CurGroup = group;
	return ValidGroup;
}

//---------------------------------------------------------------------------------------------
void ACharacterClass::RegisterStateClass(ASClass* state)
{
	enf_assert(state);
	bool ok = StatesHash.Insert(state->GetName(), state);
	StatesArray.Insert(state);
//	StateIDs.Insert(AnimStateID(state->GetName()));
	enf_assert(ok);
}

//---------------------------------------------------------------------------------------------
void ACharacterClass::UnregisterStateClass(ASClass* state)
{
	enf_assert(state);
	bool ok = StatesHash.Remove(state->GetName());
	enf_assert(ok);

	uint index = StatesArray.GetIndexOf(state);
	assert(index != INDEX_NOT_FOUND);
	StatesArray.RemoveElement(index);
//	StateIDs.RemoveElement(index);
}

//---------------------------------------------------------------------------------------------
ASClass* ACharacterClass::FindStateClass(const char* name)
{
	enf_assert(name);
	return StatesHash.Find(name);
}

//---------------------------------------------------------------------------------------------
void ACharacterClass::FindStates(const char* str, const ASPrefixMask* mask1, const ASPrefixMask* mask2, EArray<ASClass*>& result)
{
	enf_assert(mask1);
	bool Mask1AnySet = mask1->AnySet();
	bool Mask2AnySet = mask2 ? mask2->AnySet() : false;
	bool CompareString = strlen(str) > 0;

	for(uint n = 0; n < StatesArray.GetCardinality(); n++)
	{
		ASClass* state = StatesArray[n];
		const AnimStateID& id = state->GetAnimId();
		uint NumMasks = id.GetMaskCount();

		if(!Mask1AnySet && !Mask2AnySet)	//if no mask filter is set
		{
			if(!CompareString || StrStrIA(state->GetName(), str) != NULL)
				result.Insert(state);
			continue;
		}

		if(!id.IsPrefixed())
			continue;

		const ASPrefixMask* IDMask1 = id.GetMask(0);
		bool FirstPass = (!Mask1AnySet || IDMask1->Contains(*mask1));

		if(mask2)	//if mask 2 is set, must pass then
		{
			const ASPrefixMask* IDMask2 = (NumMasks > 1) ? id.GetMask(1) : NULL;
			bool SecondPass = (IDMask2 && IDMask2->Contains(*mask2));

			if(FirstPass && SecondPass)
			{
				if(!CompareString || StrStrIA(state->GetName(), str) != NULL)
					result.Insert(state);
			}
		}
		else	//only mask 1
		{
			if(FirstPass)
			{
				if(!CompareString || StrStrIA(state->GetName(), str) != NULL)
					result.Insert(state);
			}
		}
	}
}

//===============================================================================
ASGroup::ASGroup(const char* name)
{
	enf_assert(name);
	this->name = name;
	GraphZoom = 5;
}

//-------------------------------------------------------------------------------
ASGroup::~ASGroup()
{
}

//-------------------------------------------------------------------------------
void ASGroup::Save(TiXmlElement* parent)
{
	TiXmlElement* GroupElm = parent->InsertEndChild(TiXmlElement("group"))->ToElement();
	GroupElm->SetAttribute("name", GetName().c_str());

	char val[128];
	taItoA(val, GraphZoom);
	GroupElm->SetAttribute("GraphZoom", val);
	taItoA(val, GraphViewPoint.x);
	GroupElm->SetAttribute("GraphViewPointX", val);
	taItoA(val, GraphViewPoint.y);
	GroupElm->SetAttribute("GraphViewPointY", val);
}

//-------------------------------------------------------------------------------
void ASGroup::SetGraphZoomLevel(int zoom)
{
	GraphZoom = zoom;
}

//-------------------------------------------------------------------------------
void ASGroup::SetGraphViewPoint(const FPoint& pt)
{
	GraphViewPoint = pt;
}

//-------------------------------------------------------------------------------
ASGroup* ASGroup::Clone() const
{
	ASGroup* group = ENF_NEW ASGroup(name.c_str());
	group->GraphZoom = GraphZoom;
	group->GraphViewPoint = GraphViewPoint;
	return group;
}

//===============================================================================
ASGroupRef::ASGroupRef()
{
}

ASGroupRef::ASGroupRef(const char* GroupName)
{
	enf_assert(GroupName);
	this->GroupName = GroupName;
}

//-------------------------------------------------------------------------------
void ASGroupRef::SetGraphPos(const FPoint& pt)
{
	GraphPos = pt;
}

//-------------------------------------------------------------------------------
void ASGroupRef::SetGraphPos(float x, float y)
{
	GraphPos.x = x;
	GraphPos.y = y;
}

//-------------------------------------------------------------------------------
void ASGroupRef::SetGroupName(const char* GroupName)
{
	enf_assert(GroupName);
	this->GroupName = GroupName;
}

//-------------------------------------------------------------------------------
const DString& ASGroupRef::GetGroupName() const
{
	return GroupName;
}

//-------------------------------------------------------------------------------
const FPoint& ASGroupRef::GetGraphPos() const
{
	return GraphPos;
}

//-------------------------------------------------------------------------------
bool ASGroupRef::operator> (const ASGroupRef& ref)
{
	return (strcmpi(GroupName.c_str(), ref.GroupName.c_str()) > 0);
}

//-------------------------------------------------------------------------------
bool ASGroupRef::operator< (const ASGroupRef& ref)
{
	return (strcmpi(GroupName.c_str(), ref.GroupName.c_str()) < 0);
}

//-------------------------------------------------------------------------------
bool ASGroupRef::operator== (const ASGroupRef& ref)
{
	return (strcmpi(GroupName.c_str(), ref.GroupName.c_str()) == 0);
}