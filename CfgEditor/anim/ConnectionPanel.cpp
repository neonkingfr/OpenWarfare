
#include "precomp.h"
#include "ConnectionPanel.h"
#include "anim/AnimEditor.h"
#include "anim/ParamConfigAnm.h"
#include "anim/DiagramWin/DiagramWindow.h"
#include "common/WxConvert.h"
#include "common/NativeUtil.h"
#include "wx/wx.h"

BEGIN_EVENT_TABLE(ConnectionPanel, wxPanel)
	EVT_CHECKBOX(-1, ConnectionPanel::OnCheckBox)
	EVT_BUTTON(-1, ConnectionPanel::OnButton)
	EVT_TEXT(-1, ConnectionPanel::OnText)
	EVT_TEXT_ENTER(-1, ConnectionPanel::OnTextEnter)
END_EVENT_TABLE()

//=======================================================================
ConnectionPanel::ConnectionPanel(wxWindow* parent)
	:wxPanel(parent, -1, wxDefaultPosition, wxSize(-1, 30))
{
	SourceLabel = new wxStaticText(this, -1, "from:", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	DestLabel = new wxStaticText(this, -1, "to:", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
	ConnectCheck = new wxCheckBox(this, CHECKBOX_CONNECT, "connect", wxDefaultPosition, wxDefaultSize);
	InterpCheck = new wxCheckBox(this, CHECKBOX_INTERPOLATE, "interpolate", wxDefaultPosition, wxDefaultSize);
	ConnectWeightEditBox = new wxTextCtrl(this, EDITBOX_WEIGHT_CONNECT, wxEmptyString, wxDefaultPosition, wxSize(80, 20), wxTE_PROCESS_ENTER);
	InterpWeightEditBox = new wxTextCtrl(this, EDITBOX_WEIGHT_INTERP, wxEmptyString, wxDefaultPosition, wxSize(80, 20), wxTE_PROCESS_ENTER);
	OkButton = new wxButton(this, BUTTON_OK, "Accept", wxPoint(189, 67), wxSize(63, 63));
	ApplyToInheritedClassesCheck = new wxCheckBox(this, APPLY_TO_INHERITED_CLASSES, "Apply on inherited classes", wxDefaultPosition, wxDefaultSize);
//	wxStaticText* ConnectWeightEditBoxLabel = new wxStaticText(this, -1, "weight", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);
//	wxStaticText* InterpWeightEditBoxLabel = new wxStaticText(this, -1, "weight", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);

	wxBoxSizer* ConnectSizer = new wxBoxSizer(wxHORIZONTAL);
	ConnectSizer->Add(ConnectCheck, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 0);
	ConnectSizer->AddSpacer(20);
	ConnectSizer->Add(ConnectWeightEditBox, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 0);

	wxBoxSizer* InterpSizer = new wxBoxSizer(wxHORIZONTAL);
	InterpSizer->Add(InterpCheck, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 0);
	InterpSizer->AddSpacer(6);
	InterpSizer->Add(InterpWeightEditBox, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 0);

	wxStaticBox* ValuesBox = new wxStaticBox(this, -1, "", wxDefaultPosition, wxDefaultSize);
	wxBoxSizer* ValuesSizer = new wxStaticBoxSizer(ValuesBox, wxVERTICAL);
	ValuesSizer->AddSpacer(16);
	ValuesSizer->Add(ConnectSizer, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 0);
	ValuesSizer->Add(InterpSizer, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 0);

	wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
	sizer->AddSpacer(10);
	sizer->Add(SourceLabel, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 10);
	sizer->AddSpacer(10);
	sizer->Add(DestLabel, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 10);
	sizer->AddSpacer(15);
	sizer->Add(ValuesSizer, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 10);
	sizer->AddSpacer(10);
	sizer->Add(ApplyToInheritedClassesCheck, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT, 10);
	sizer->AddSpacer(10);
	SetSizer(sizer);

	IgnoreSetValuesCheck = new wxCheckBox(this, CHECKBOX_IGNORE_SET_VALUES, "lock these settings", wxPoint(15, 61), wxDefaultSize);
	UpdateAccesibility();
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::Set(const char* SrcLabel, const char* DestLabel, bool connect, bool interpolate, const char* ConnectWeight, const char* InterpWeight)
{
	SourceName = SrcLabel;
	DestName = DestLabel;
	this->SourceLabel->SetLabel(wxString("from: ") + SrcLabel);
	this->DestLabel->SetLabel(wxString("to:     ") + DestLabel);

	if(!IgnoreSetValuesCheck->IsChecked())
	{
		ConnectCheck->SetValue(connect);
		InterpCheck->SetValue(interpolate);
		ConnectWeightEditBox->SetValue(ConnectWeight);
		InterpWeightEditBox->SetValue(InterpWeight);
	}

	UpdateAccesibility();
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::SetFromSelectedConnection()
{
	ASClass* from = NULL;
	ASClass* to = NULL;
	ConnectionSelected(&from, &to);

	if(from == NULL || to == NULL)
		return;

	ConnectionRef connection = g_aeditor->GetConnectionInfo(from, to);
	Set(from->GetName(), to->GetName(), connection.connect, connection.interpolate, FtoA(connection.ConnectWeight).c_str(), FtoA(connection.InterpolateWeight).c_str());
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::UpdateAccesibility()
{
	bool ConnectionIsSelected = ConnectionSelected();

	ConnectCheck->Enable(ConnectionIsSelected);
	ConnectWeightEditBox->Enable(ConnectionIsSelected && ConnectCheck->IsChecked());
	InterpCheck->Enable(ConnectionIsSelected);
	InterpWeightEditBox->Enable(ConnectionIsSelected && InterpCheck->IsChecked());
	OkButton->Enable(ConnectionIsSelected);
	IgnoreSetValuesCheck->Enable(ConnectionIsSelected);
	ApplyToInheritedClassesCheck->Enable(ConnectionIsSelected);
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::OnCheckBox(wxCommandEvent& event)
{
	switch(event.GetId())
	{
	case CHECKBOX_CONNECT:
		UpdateAccesibility();
		break;
	case CHECKBOX_INTERPOLATE:
		UpdateAccesibility();
		break;
	case APPLY_TO_INHERITED_CLASSES:
		UpdateAccesibility();
		break;
	case CHECKBOX_IGNORE_SET_VALUES:
		SetFromSelectedConnection();
		break;
	}
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::OnText(wxCommandEvent& event)
{
	switch(event.GetId())
	{
	case EDITBOX_WEIGHT_CONNECT:
		OkButton->Enable(true);
		break;
	case EDITBOX_WEIGHT_INTERP:
		OkButton->Enable(true);
		break;
	}
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::OnTextEnter(wxCommandEvent& event)
{
	switch(event.GetId())
	{
	case EDITBOX_WEIGHT_CONNECT:
		if(InterpWeightEditBox->IsEnabled())
		{
			InterpWeightEditBox->SetFocus();
			InterpWeightEditBox->SetSelection(0, InterpWeightEditBox->GetLineLength(0));
		}
		else
			OkButton->SetFocus();
		break;
	case EDITBOX_WEIGHT_INTERP:
		OkButton->SetFocus();
		break;
	}
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::OnButton(wxCommandEvent& event)
{
	switch(event.GetId())
	{
		case BUTTON_OK:
		{
			OnOkButton();
			break;
		}
	}
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::OnOkButton()
{
	wxString ConnectWeightStr = ConnectWeightEditBox->GetValue();
	wxString InterpWeightStr = InterpWeightEditBox->GetValue();

	float ConnectWeight = AtoF(ConnectWeightStr);
	float InterpWeight = AtoF(InterpWeightStr);

	if(!IsFloatNumber(ConnectWeightStr.c_str()) || !IsFloatNumber(InterpWeightStr.c_str()) || ConnectWeight < 0.0f || /*ConnectWeight > 1.0f ||*/ InterpWeight < 0.0f /*|| InterpWeight > 1.0f*/) // Weight can be bigger that 1
	{
		wxMessageDialog dialog(g_editor->GetMainFrame(), "weight values in editboxes must be floating value in range 0 - 1", _T("Update connection error"), wxICON_INFORMATION | wxOK);
		dialog.ShowModal();
		return;
	}

	ParamConfigAnm* PConfig = g_aeditor->GetParamConfig();
	ACharacterClass* character = PConfig->GetCurrentCharacterPtr();

	ASClass* SrcState = character->FindStateClass(SourceName.c_str());
	ASClass* DestState = character->FindStateClass(DestName.c_str());
	enf_assert(SrcState);
	enf_assert(DestState);

	if(SrcState && DestState)
	{
		ConnectionRef connection(SrcState, DestState);

		if(ConnectCheck->IsChecked())
			connection.SetConnect(ConnectWeight);

		if(InterpCheck->IsChecked())
			connection.SetInterpolate(InterpWeight);

		g_aeditor->UpdateConnection(connection, ApplyToInheritedClassesCheck->IsChecked());
		OkButton->Enable(false);
	}
}

//----------------------------------------------------------------------------------------------
void ConnectionPanel::PressOkButton()
{
	if(OkButton->IsEnabled())
		OnOkButton();
}

//----------------------------------------------------------------------------------------------
bool ConnectionPanel::ConnectionSelected(ASClass** from, ASClass** to)
{
	DiagramWindow* DiagWin = g_aeditor->GetDiagramWindow();

	if(!DiagWin)
		return false;

	if(!DiagWin->ConnectionSelected())
		return false; 

	ParamConfigAnm* PConfig = g_aeditor->GetParamConfig();

	if(!PConfig)
		return false;

	ACharacterClass* character = PConfig->GetCurrentCharacterPtr();

	if(!character)
		return false;

	ASClass* SrcState = character->FindStateClass(SourceName.c_str());
	ASClass* DestState = character->FindStateClass(DestName.c_str());

	if(!SrcState)
		return false;

	if(!DestState)
		return false;

	if(from)
		(*from) = SrcState;

	if(to)
		(*to) = DestState;

	return true;
}

//----------------------------------------------------------------------------------------------
bool ConnectionPanel::SelectedConnectionExist()
{
	DiagramWindow* DiagWin = g_aeditor->GetDiagramWindow();

	if(!DiagWin)
		return false;

	if(!DiagWin->ConnectionSelected())
		return false; 

	ParamConfigAnm* PConfig = g_aeditor->GetParamConfig();

	if(!PConfig)
		return false;

	ACharacterClass* character = PConfig->GetCurrentCharacterPtr();

	if(!character)
		return false;

	ASClass* SrcState = character->FindStateClass(SourceName.c_str());
	ASClass* DestState = character->FindStateClass(DestName.c_str());

	if(!SrcState)
		return false;

	if(!DestState)
		return false;

	PCArray* connectTo = SrcState->GetArrayMember(ConnectToArrayName);
	PCArray* interpolateTo = SrcState->GetArrayMember(InterpolateToArrayName);

	if( connectTo && g_aeditor->FindStateRefValueFromArray(connectTo, DestName.c_str()) )
		return true;

	if( interpolateTo && g_aeditor->FindStateRefValueFromArray(interpolateTo, DestName.c_str()) )
		return true;

	return false;
}

