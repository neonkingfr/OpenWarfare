#include "precomp.h"
#include "ParamFileTreeAnm.h"
#include "ParamConfigAnm.h"
#include "AnimEditor.h"
#include "base/PropertiesPanel.h"

/*
#include "base/MainFrame.h"
#include "base/GenericEditor.h"
#include "base/ImageList.h"
#include "base/EditActions.h"
#include "base/CreateClassDialog.h"
#include "base/BaseClassDialog.h"
#include "base/ListMessageDialog.h"
#include "Common/NativeUtil.h"
#include "Common/TextFile.h"
#include "wxExtension/WBPropertyGrid.h"
#include "wx/clipbrd.h"*/

BEGIN_EVENT_TABLE(ParamFileTreeAnm, ParamFileTree)
	EVT_MENU(-1, ParamFileTreeAnm::OnSelectFromMenu)
END_EVENT_TABLE()

//======================================================================
ParamFileTreeAnm::ParamFileTreeAnm(wxWindow* parent, wxWindowID id)
	:ParamFileTree(parent, id)
{
}

//----------------------------------------------------------------------
void ParamFileTreeAnm::OnSelectFromMenu(wxCommandEvent& event)
{
	switch(event.GetId())
	{
		case MENU_GOTO_ACTIONS_CLASS:
		{
			PCClass* MainSel = g_aeditor->GetMainSelection();
			enf_assert(MainSel);

			ASClass* state = dynamic_cast<ASClass*>(MainSel);
			enf_assert(state);
			
			if(state)
			{
				PCClass* ActionsClass = state->GetActionsClass();

				if(ActionsClass && !ActionsClass->IsHidden())
				{
					g_editor->Select(ActionsClass, false);
					PropertiesPanel* PropPanel = g_MainFrame->GetPropPanel();
					PropPanel->GetPropGrid()->Refresh();
				}
			}
			break;
		}
	default:
		event.Skip();
	}
}

//----------------------------------------------------------------------
wxMenu* ParamFileTreeAnm::CreateSelClassMenu()
{
	//create menu from base project and append AEG specific items
	wxMenu* menu = ParamFileTree::CreateSelClassMenu();
	enf_assert(menu);

	PCClass* MainSel = g_aeditor->GetMainSelection();
	enf_assert(MainSel);

	ASClass* state = dynamic_cast<ASClass*>(MainSel);
	
	if(state)
	{
		bool AnyBaseClass = false;
		bool AnyConnectArray = false;
		bool AnyInterpolateArray = false;
		bool AnyItemInConnectArray = false;
		bool AnyItemInInterpolateArray = false;

		EArray<ASClass*> SelStates;
		g_aeditor->GetSelectedStates(SelStates);

		for(uint n = 0; n < SelStates.GetCardinality(); n++)
		{
			ASClass* SelMember = SelStates[n];

			if(!AnyBaseClass)
			{
				if(SelMember->GetBase() != NULL)
					AnyBaseClass = true;
			}

			if(!AnyConnectArray || !AnyItemInConnectArray)
			{
				PCArray* ConnectTo = SelMember->GetArrayMember(ConnectToArrayName);
				if(ConnectTo)
				{
					if(!AnyConnectArray)
						AnyConnectArray = true;

					if(!AnyItemInConnectArray && ConnectTo->GetCardinality() > 0)
						AnyItemInConnectArray = true;
				}
			}

			if(!AnyInterpolateArray || !AnyItemInInterpolateArray)
			{
				PCArray* InterpolateTo = SelMember->GetArrayMember(InterpolateToArrayName);
				if(InterpolateTo)
				{
					if(!AnyInterpolateArray)
						AnyInterpolateArray = true;

					if(!AnyItemInInterpolateArray && InterpolateTo->GetCardinality() > 0)
						AnyItemInInterpolateArray = true;
				}
			}
		}

		if(!SelStates.IsEmpty())
		{
			wxMenu* ConnectToMenu = new wxMenu();
			ConnectToMenu->Append(MENU_COPY_MISSING_CONNECT_TARGETS_FROM_BASE_CLASS, _T("Copy missing targets from base class"), _T(""));
			ConnectToMenu->Append(MENU_COPY_ALL_CONNECT_TARGETS_FROM_BASE_CLASS, _T("Copy all targets from base class"), _T(""));
			ConnectToMenu->Append(MENU_DELETE_CONNECT_TARGETS_CONTAINED_IN_BASE_CLASS, _T("Delete same targets contained in base class"), _T(""));
			ConnectToMenu->Append(MENU_DELETE_CONNECT_TARGETS_CONTAINED_IN_BASE_CLASS_RECURSIVE, _T("Delete same targets contained in any of base classes"), _T(""));

			ConnectToMenu->Append(MENU_CREATE_CONNECT_ARRAY, _T("Create array"), _T(""));
			ConnectToMenu->Append(MENU_CLEAR_CONNECT_ARRAY, _T("Clear array"), _T(""));
			ConnectToMenu->Append(MENU_REMOVE_CONNECT_ARRAY, _T("Delete array"), _T(""));

			if(!AnyBaseClass)
			{
				ConnectToMenu->Enable(MENU_COPY_MISSING_CONNECT_TARGETS_FROM_BASE_CLASS, false);
				ConnectToMenu->Enable(MENU_COPY_ALL_CONNECT_TARGETS_FROM_BASE_CLASS, false);
				ConnectToMenu->Enable(MENU_DELETE_CONNECT_TARGETS_CONTAINED_IN_BASE_CLASS, false);
				ConnectToMenu->Enable(MENU_DELETE_CONNECT_TARGETS_CONTAINED_IN_BASE_CLASS_RECURSIVE, false);
			}

			if(!AnyItemInConnectArray)
				ConnectToMenu->Enable(MENU_CLEAR_CONNECT_ARRAY, false);

			if(AnyConnectArray)
				ConnectToMenu->Enable(MENU_CREATE_CONNECT_ARRAY, false);
			else
				ConnectToMenu->Enable(MENU_REMOVE_CONNECT_ARRAY, false);

			wxMenu* InterpolateToMenu = new wxMenu();
			InterpolateToMenu->Append(MENU_COPY_MISSING_INTERPOLATE_TARGETS_FROM_BASE_CLASS, _T("Copy missing targets from base class"), _T(""));
			InterpolateToMenu->Append(MENU_COPY_ALL_INTERPOLATE_TARGETS_FROM_BASE_CLASS, _T("Copy all targets from base class"), _T(""));
			InterpolateToMenu->Append(MENU_DELETE_INTERPOLATE_TARGETS_CONTAINED_IN_BASE_CLASS, _T("Delete targets contained in base class"), _T(""));
			InterpolateToMenu->Append(MENU_DELETE_INTERPOLATE_TARGETS_CONTAINED_IN_BASE_CLASS_RECURSIVE, _T("Delete same targets contained in any of base classes"), _T(""));

			InterpolateToMenu->Append(MENU_CREATE_INTERPOLATE_ARRAY, _T("Create array"), _T(""));
			InterpolateToMenu->Append(MENU_CLEAR_INTERPOLATE_ARRAY, _T("Clear array"), _T(""));
			InterpolateToMenu->Append(MENU_REMOVE_INTERPOLATE_ARRAY, _T("Delete array"), _T(""));

			if(!AnyBaseClass)
			{
				InterpolateToMenu->Enable(MENU_COPY_MISSING_INTERPOLATE_TARGETS_FROM_BASE_CLASS, false);
				InterpolateToMenu->Enable(MENU_COPY_ALL_INTERPOLATE_TARGETS_FROM_BASE_CLASS, false);
				InterpolateToMenu->Enable(MENU_DELETE_INTERPOLATE_TARGETS_CONTAINED_IN_BASE_CLASS, false);
				InterpolateToMenu->Enable(MENU_DELETE_INTERPOLATE_TARGETS_CONTAINED_IN_BASE_CLASS_RECURSIVE, false);
			}

			if(!AnyItemInInterpolateArray)
				InterpolateToMenu->Enable(MENU_CLEAR_INTERPOLATE_ARRAY, false);

			if(AnyInterpolateArray)
				InterpolateToMenu->Enable(MENU_CREATE_INTERPOLATE_ARRAY, false);
			else
				InterpolateToMenu->Enable(MENU_REMOVE_INTERPOLATE_ARRAY, false);

			menu->AppendSeparator();
			menu->AppendSubMenu(ConnectToMenu, "ConnectTo");
			menu->AppendSubMenu(InterpolateToMenu, "InterpolateTo");

			menu->Append(MENU_SELECT_INHERITED_STATES, _T("Select inherited states"), _T(""));
			menu->Append(MENU_SELECT_BASE_STATES, _T("Select base states"), _T(""));

			if(SelStates.GetCardinality() > 1)
			{
				menu->Enable(MENU_SELECT_INHERITED_STATES, false);
				menu->Enable(MENU_SELECT_BASE_STATES, false);
			}
		}


		PCClass* ActionsClass = state->GetActionsClass();

		if(ActionsClass && !ActionsClass->IsHidden())
		{
			if(MainSel->GetBase() && !MainSel->GetBase()->IsHidden())
				menu->Append(MENU_GOTO_ACTIONS_CLASS, _T("Select actions class"), _T("Select actions class"));
		}
	}
 
	return menu;
}