#ifndef EDIT_ACTIONS_ANM_H
#define EDIT_ACTIONS_ANM_H

#include "base/EditActions.h"
#include "anim/ParamConfigAnm.h"

enum
{
	EA_CREATE_SGROUP = EA_LAST_UNUSED,	//create new state group for character
	EA_DELETE_SGROUP,							//delete state group of character
	EA_CREATE_SGROUP_REFS,					//add group reference to some anim state classes (visualy insert some state classes to graph)
	EA_DELETE_SGROUP_REFS,					//reverse of EA_CREATE_SGROUP_REFS
	EA_MODIFY_SGROUP_REFS,					//modify group reference data. currently only graph position
};

//==========================================================================
//create new state group for character
//===============================================================================
class EACreateSGroup : public EditAction
{
public:
					EACreateSGroup(const char* name, const char* CharacterName);
					EACreateSGroup(ASGroup* storage, const char* CharacterName);
					~EACreateSGroup();
	inline const DString& GetName() const {return name;}
	inline const DString& GetCharacterName() const {return CharacterName;}
	inline ASGroup*	GetStorage() const{return storage;}
	ENF_DECLARE_MMOBJECT(EACreateSGroup);
private:
	ASGroup*	storage;
	DString	name;
	DString	CharacterName;
};

//==========================================================================
//delete state group of character
//===============================================================================
class EADeleteSGroup : public EditAction
{
public:
					EADeleteSGroup(const char* name, const char* CharacterName);
	inline const DString& GetName() const {return name;}
	inline const DString& GetCharacterName() const {return CharacterName;}
	ENF_DECLARE_MMOBJECT(EADeleteSGroup);
private:
	DString	name;
	DString	CharacterName;
};

//==========================================================================
//add group reference to some anim state classes (visualy insert some state classes to graph)
//===============================================================================
class EACreateSGroupRefs : public EditAction
{
public:
					EACreateSGroupRefs(const char* CharacterName);
	inline const DString& GetCharacterName() const {return CharacterName;}
	void			AddStateRefData(const char* StateName, const ASGroupRef& reference);
	uint			GetStateRefDataCount() const;
	const	DString& GetStateName(uint n) const;
	const	ASGroupRef& GetStateRefData(uint n) const;
	ENF_DECLARE_MMOBJECT(EACreateSGroupRefs);
private:
	DString	CharacterName;
	EArray<ASGroupRef> StateRefDatas;
	EArray<DString> StateNames;
};

//==========================================================================
//reverse of EA_CREATE_SGROUP_REFS
//===============================================================================
class EADeleteSGroupRefs : public EditAction
{
public:
					EADeleteSGroupRefs(const char* GroupName, const char* CharacterName);
	inline const DString& GetGroupName() const {return GroupName;}
	inline const DString& GetCharacterName() const {return CharacterName;}
	void			AddStateName(const char* name);
	const			EArray<DString>& GetStateNames() const {return StateNames;}
	ENF_DECLARE_MMOBJECT(EADeleteSGroupRefs);
private:
	DString	GroupName;
	DString	CharacterName;
	EArray<DString> StateNames;
};

//==========================================================================
//modify group reference data. currently only graph position
//===============================================================================
class EAModifySGroupRefs : public EditAction
{
public:
					EAModifySGroupRefs(const char* CharacterName);
	inline const DString& GetCharacterName() const {return CharacterName;}
	void			AddStateRefData(const char* StateName, const ASGroupRef& reference);
	uint			GetStateRefDataCount() const;
	const	DString& GetStateName(uint n) const;
	const	ASGroupRef& GetStateRefData(uint n) const;
	ENF_DECLARE_MMOBJECT(EAModifySGroupRefs);
private:
	DString	CharacterName;
	EArray<ASGroupRef> StateRefDatas;
	EArray<DString> StateNames;
};

//==========================================================================
//extension of HistoryPoint class from GenericEditoBase project. all only GUI states of some windows
//==========================================================================
class HistoryPointAnm : public HistoryPoint
{
public:
									HistoryPointAnm(const char* name);
	const DString&				GetUndoCharacterSel(){return UndoCharacterSel;}
	const DString&				GetRedoCharacterSel(){return RedoCharacterSel;}
	const DString&				GetUndoGroupSel(){return UndoGroupSel;}
	const DString&				GetRedoGroupSel(){return RedoGroupSel;}
	void							SetUndoCharacterSel(const char* character);
	void							SetRedoCharacterSel(const char* character);
	void							SetUndoGroupSel(const char* group);
	void							SetRedoGroupSel(const char* group);
	void							SetGraphViewPos(const FPoint& ViewPos);
	void							SetGraphZoomLevel(int ZoomLevel);
	const FPoint&				GetGraphViewPos(){return GraphViewPos;}
	const int					GetGraphZoomLevel(){return GraphZoomLevel;}
									ENF_DECLARE_MMOBJECT(HistoryPointAnm);
private:
	DString						UndoCharacterSel;
	DString						RedoCharacterSel;
	DString						UndoGroupSel;
	DString						RedoGroupSel;
	FPoint						GraphViewPos;
	int							GraphZoomLevel;
};

#endif