
#ifndef MAIN_FRAME_ANM_H
#define MAIN_FRAME_ANM_H

#include "base/MainFrame.h"

class DiagramWindow;
class StateGroupsPanel;
class ASClass;
class ConnectionPanel;
class StateFindDialog;
class AnimEditor;
class ActionsPanel;

enum AnimGuiIDs
{
	AnimGuiIDs_FIRST_ENUM = GenericGuiIDs_LAST_ENUM + 1,
	MENU_IMPORT,
	MENU_EXPORT,
	MENU_FIND_STATES,
	MENU_CHARACTER_CHOICE,
	MENU_CONNECT_TO,
	MENU_INTERPOLATE_TO,
	MENU_REMOVE_STATE_FROM_CURRENT_GRAPH,
	MENU_COPY_STATE_NAME_CURRENT_GRAPH,
	MENU_INSERT_CONNECT_TARGETS_TO_GRAPH,
	MENU_INSERT_INTERPOLATE_TARGETS_TO_GRAPH,
	MENU_INSERT_ALL_TARGETS_TO_GRAPH,
	MENU_INSERT_CONNECT_SOURCES_TO_GRAPH,
	MENU_INSERT_INTERPOLATE_SOURCES_TO_GRAPH,
	MENU_INSERT_ALL_SOURCES_TO_GRAPH,
	MENU_REMOVE_CONNECT_TARGETS_FROM_GRAPH,
	MENU_REMOVE_INTERPOLATE_TARGETS_FROM_GRAPH,
	MENU_REMOVE_ALL_TARGETS_FROM_GRAPH,
	MENU_REMOVE_CONNECT_SOURCES_FROM_GRAPH,
	MENU_REMOVE_INTERPOLATE_SOURCES_FROM_GRAPH,
	MENU_REMOVE_ALL_SOURCES_FROM_GRAPH,
	MENU_FIND_STATES_DIALOG,
	MENU_DRAW_ONLY_LINES,
};

//================================================================================
//anim editor main window. is a extension of MainFrame from GenericEditorBase project
//all subwindows, menus, toolbars, and event handling from class MainFrame can be overwritten here using virtual methods
//================================================================================
class MainFrameAnm : public MainFrame
{
	friend class AnimEditor;
public:
	MainFrameAnm();
	virtual ~MainFrameAnm();
	inline StateGroupsPanel* GetGroupsPanel(){return GroupsPanel;}
	inline ConnectionPanel* GetConnectionPanel(){return ConnPanel;}
	inline wxMenu* GetGraphMenu(){return GraphMenu;}
	inline StateFindDialog* GetStateFindDialog(){return SFindDialog;}
	inline ActionsPanel* GetActionsPanel(){return ActPanel;}
private:
	wxMenu*					GraphMenu;
	StateGroupsPanel*		GroupsPanel;
	ConnectionPanel*		ConnPanel;
	ActionsPanel*			ActPanel;
	wxRect					ScreenCursorClipArea;
	StateFindDialog*		SFindDialog;
	virtual wxMenu*		CreateFileMenu();
	virtual wxMenu*		CreateEditMenu();
	virtual wxToolBar*	 CreateMainToolBar();
	virtual void			CreateAUIPanes();
	virtual wxMenuBar*	CreateMenuBar();
	virtual void			CreateImageList();
	virtual wxString		GetDefaultPanesPerspective();
	virtual OptionsDialogPanel* CreateOptionsPanel(OptionsDialog* parent);
	void						ShowFindDialog();
	void						OnAppInit();
	void						OnSelectFromMenu(wxCommandEvent& event);
	void						OnDiagramEvent(wxCommandEvent& event);
	void						OnTreeBeginDrag(wxTreeEvent& event);
	void						OnActivate(wxActivateEvent& event);
	DECLARE_EVENT_TABLE();
};

//============================================================================
//file drop target for dropping files into graph window using clasic windows drag & drop system
//============================================================================
class AnimPFTreeTarget : public wxFileDropTarget
{
public:
	AnimPFTreeTarget();
	virtual bool OnDropFiles(wxCoord x, wxCoord y, const wxArrayString& filenames);
private:
};

extern ASClass* g_MenuState;

#endif