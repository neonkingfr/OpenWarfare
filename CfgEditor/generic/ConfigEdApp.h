
#ifndef CONFIG_ED_APP_H
#define CONFIG_ED_APP_H

#include "wx/app.h"

class GenericEditor;
class MainFrame;

//application class. instance created using macros DECLARE_APP a IMPLEMENT_APP
//-------------------------------------------------------------------------------------------------------
class ConfigEdApp : public wxApp
{
public:
	ConfigEdApp();
	inline GenericEditor* GetEditor(){return editor;}
private:
   virtual bool	OnInit();
	virtual int		OnExit();
	int				MainLoop();
	GenericEditor*	editor;
};

DECLARE_APP(ConfigEdApp)

#endif