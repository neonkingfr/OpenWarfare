#pragma once

#include "base/GenericEditor.h"
#include "VBSParamConfig.h"
#include "FileSystemOperations.h"
#include "Addon.h"

//===============================================================================
class VBSConfigEditor : public GenericEditor
{
public:
	/// Struct of tree node data
	struct STreeNodeData : public wxTreeItemData
	{
		STreeNodeData(CAddon *addon):
	m_addon(addon) {};

	CAddon *GetAddon() const { return m_addon; }
	private:
		CAddon *m_addon;
	};

	virtual CResult OnAppInit();
	virtual int OnAppExit() { return GenericEditor::OnAppExit(); }	
	virtual bool Load();	
	bool LoadOnInit(const char* filename);
	virtual bool New();
	

	// set/get working directory methods
	/////////////////////////////////////////
	bool SetTempDir(bool onStart=false);	
	std::string &GetTempDir() { return m_tempDirectory; }
	/////////////////////////////////////////

	/// Set data directory methods
	bool SetDataDir();	

	/// Set external bin directory method
	bool SetBinDir();

	// set/get exec filename
	void SetExecFilename(const std::string &filename) { m_filename=filename; }
	const std::string &GetExecFilename() { return m_filename; }

	void UpdateBinDir();
	void UpdateCfgDir();

	/// Check if external app exist in bin directory
	bool CheckExtAppExist(bool verbose=true);
	bool CheckExtAppExist(const std::string &where, bool verbose=true);

protected:
	virtual CResult	Load(const char* FileName);
	virtual CResult Save(const char* FileName);
	virtual XMLConfig*	CreateEditorConfig();
	virtual ParamConfig* CreateParamConfig();
		
	virtual void OnAppConfigChanged();

  // overwrite to remove DataPath prefix
  virtual bool DoRealizeAction(const EAModifyVariable* action);

  /// Check if app can be restarted
	bool CanRestartApp();	
	/// Check if working directory can be erased
	bool CanRemoveTemp();	
	/// Check if working directory has been stored in app config
	void CheckWorkDir();
	/// Check if data directory has been stored in app config
	void CheckDataDir();
	/// Check path to external app (bankrev,filebank)
	void CheckExtBinDir();
	/// Check path to base config files
	void CheckCfgsDir();

	bool TryToPreloadDependencies(const std::string &rootName);

	virtual MainFrame*	CreateMainFrame();

	// select config file methods
	/////////////////////////////////////////
	// listbox style
	std::string ShowConfigDlg(const VBSParamConfig::TVecConfigNames &configList);
	// treeview style
	std::string ShowConfigDlg(const std::string &filename);
	/////////////////////////////////////////

	/// working directory variable
	std::string	m_tempDirectory;

	/// data directory variable
	std::string	m_dataDirectory;

	/// external bin directory variable(path to bankrev and filebank)
	std::string	m_extBinDirectory;

	/// directory holding default configuration files with base parent classes
	std::string m_baseConfigsDirectory;

	/// full filename of pbo
	std::string m_PBOpath;

	/// exec filename
	std::string m_filename;

private:
	// methods for build wxTreeCtrl 
	// menu with all configs in pbo
	/////////////////////////////////////////
	void AddFilesToTree(wxTreeCtrl *parentTree, wxTreeCtrl *tree, CAddon *addon,
		wxTreeItemId parent, const CFileSystemOperations::SFileInfoNode *fileNode);

	void AddPboFilesToTree(wxTreeCtrl *parentTree,wxTreeCtrl *tree, CAddon *addon, wxTreeItemId parent);
	void AddAddonToTree(CAddon *addon, wxTreeCtrl *tree);

	CResult ImportConfigTree(const std::string &filename);
	CResult ImportConfigTreeRaw(const std::string &filename);
	/////////////////////////////////////////

	bool m_loadOnInit;	
};