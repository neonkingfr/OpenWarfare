#include <es\common\win.h>
#include <algorithm>
#include <queue>
#include "AddonManager.h"
#include "Addon.h"
#include "FileSystemOperations.h"

const std::string PBO_EXT	  = ".pbo";
const std::string BIN_EXT	  = ".bin";
const std::string CPP_EXT	  = ".cpp";
const std::string CONFIG_FILE = "config.bin";
const std::string CONFIG_FILE_NO_BIN = "config.cpp";
/*
//=============================================================================
bool IsConfig(const std::string filename)
{
	// try to find config.bin/config.cpp

	if (filename.find(CONFIG_FILE) != std::string::npos)
		return true;

	if (filename.find(CONFIG_FILE_NO_BIN) != std::string::npos)
		return true;

	return false;
}

//=============================================================================
//=============================================================================
//=============================================================================
void StoreConfigFiles( const FileInfoO& fi, const FileBankType* files, void* context )
{
	std::string filename = std::string(fi.name);
	CAddonManager::TVecAddonsNames* list = 
		static_cast<CAddonManager::TVecAddonsNames*>(context);
	
	if (IsConfig(filename))
		list->push_back(filename);
}

//=============================================================================
void GetAllConfigFiles(const std::string &from, CAddonManager::TVecAddonsNames &out)
{
	// make space in file bank
	ParamFile PFile;
	int index = GFileBanks.Add();
	QFBank& bank = GFileBanks[index];

	// erase extension
	std::string fullFilename = from;
	fullFilename.erase(fullFilename.size()-PBO_EXT.size());

	// open bank for load configs
	bank.open( fullFilename.c_str() );	

	// fill addon internal files
	bank.ForEach( StoreConfigFiles, &out );
}
//=============================================================================
//=============================================================================
//=============================================================================
*/
//=============================================================================
void FillAddonFiles( const FileInfoO& fi, const FileBankType* files, void* context )
{
	// if filename = config.cpp or config.bin
	// parse it's dependencies and link them
	// to addon

	// type to CAddon
	CAddon* addon = static_cast<CAddon*>(context);
	std::string filename = std::string(fi.name);
		
	// and push addon file into addon internal files structure
	if (CFileSystemOperations::IsExt(filename,CPP_EXT) || CFileSystemOperations::IsExt(filename,BIN_EXT))
	addon->
		PushAddonInternalFile(CFileSystemOperations::SFileInfo(filename,
				unsigned int(fi.length)));	
}

//=============================================================================
bool CAddonManager::IsIn(const CAddon* addon)
{
	// check if addon is already in manager
	TMapAddons::const_iterator it = 
		m_AddonsMap.find(CAddon::ToLower(addon->GetName()));
	
	return it!=m_AddonsMap.end();
}
//=============================================================================
void CAddonManager::StoreAddon(const ParamClass* ClassInterf, const std::string &path, 
							   const CFileSystemOperations::SFileInfo &fileinfo, 
							   CAddon* addon, int index, QFBank& bank)
{
	// TODO:
	// config.cpp or config.bin wasn't found
	if (ClassInterf->IsError())
		return;

	// if addon is new
	if (!addon)
	{
		addon = new CAddon(ClassInterf);
		addon->PushAddonFile(fileinfo);
	}

	addon->SetValid();
	addon->SetPath(path);
	addon->SetBankIndex(index);
	addon->SetAddonManager(this);

	// check if addon already exist
	if (IsIn(addon))
	{
		delete addon;
		return;
	}

	// fill addon internal files
	bank.ForEach( FillAddonFiles, addon );

	/// build directory tree
	addon->MakeDirectoriesMap();

	// and make key in map
	m_ValidAddonsMap.insert(std::make_pair<std::string,CAddon*>
		(CAddon::ToLower(addon->GetName()),addon));

	m_AddonsMap.insert(std::make_pair<std::string,CAddon*>
		(CAddon::ToLower(addon->GetName()),addon));
}

//=============================================================================
const ParamClass* CAddonManager::GetAddonRootClass(const std::string &text, 
												   QFBank &bank, ParamFile &PFile)
{
	// and store file
	bank.open( text.c_str() );
	bool some = PFile.ParseBin(bank,CONFIG_FILE.c_str());	

	// if fail.. try find config.cpp
	if (!some)
		PFile.Parse(bank,CONFIG_FILE_NO_BIN.c_str());

	// finally parse addon file
	ParamClassPtr patchesClass = 
		PFile.GetClass(CAddon::ToLower("CfgPatches").c_str());

	return patchesClass.GetPointer();
}

//=============================================================================
bool CAddonManager::CheckAddonDuplicity(const std::string &addonName, CAddon* addon)
{
	std::string name = CAddon::ToLower(addonName);
	// check duplicity
		// if addon is in valid addons map
		// continue with next file
		if (m_ValidAddonsMap.find(name) != m_ValidAddonsMap.end())
			return false;

		// if addon is invalid
		// set it's handler and fill it
		// with correct data.
		// search addon in invalid addons
		CAddonManager::TMapAddons::iterator itAddon = 
			m_InvalidAddonsMap.find(name);

		// on match, assign addon handle
		if (itAddon != m_InvalidAddonsMap.end())
		{
			addon = itAddon->second;

			//and remove from invalid addons map
			// cause addon will become valid
			m_InvalidAddonsMap.erase(itAddon);
		}

	return true;
}

//=============================================================================
void CAddonManager::GetAllPBOs(TSetFiles &into)
{
	// add pbo file from each valid addon into set
	for (CAddonManager::TMapAddons::const_iterator it=m_ValidAddonsMap.begin();
		it!=m_ValidAddonsMap.end();++it)
	{
		// make full pbo file name and store it
		std::string path = it->second->GetPath();
		std::string filename = it->second->GetFileName();
			into.insert(path + "\\" + filename);
	}
}

//=============================================================================
void CAddonManager::AddAddonDependencies(CAddon* addon, const std::string &configFile)
{
	// check validity
	Assert(addon);
	if (!addon)
		return;

	ParamFile PFile;
	QFBank& bank = GFileBanks[addon->GetBankIndex()];
	bool some = PFile.ParseBin(bank,configFile.c_str());	

	// if fail.. try find config.cpp
	if (!some)
		PFile.Parse(bank,configFile.c_str());

	// finally parse addon file
	ParamClassPtr patchesClass = 
		PFile.GetClass(CAddon::ToLower("CfgPatches").c_str());

	const ParamClass* ClassInterf = patchesClass.GetPointer();

	// error during parse config.cpp/config.bin
	if (ClassInterf->IsError())
		return;

	// TODO: cela tahle fce se musi prekopat
	// protoze dela to same jako LoadAddons

	// make temporary addon
	CAddon* temp = new CAddon(ClassInterf);
	CAddonManager* mgr = addon->GetAddonManager();

	temp->PushAddonFile(*addon->GetFiles().begin());
	temp->SetPath(addon->GetPath());
	temp->SetBankIndex(addon->GetBankIndex());
	temp->SetAddonManager(mgr);

	// test if addon's manager exists
	Assert(mgr);
	if (!mgr)
		return;

	// create addon's structure key
	std::pair<std::string,CAddon*> key = std::make_pair<std::string,CAddon*>
		(CAddon::ToLower(temp->GetName()),temp);

	// and store it into valid and all addons
	mgr->InsertValidAddon(key);
}

//=============================================================================
void CAddonManager::SwitchLoadMode(ELoadMode mode, const std::string &addonsPath)
{	
	switch(mode)
	{
	case lmNormal:
		m_Paths.clear();

	case lmAdd:
		m_Paths.insert(addonsPath);
		break;

	case lmErase:
		{
			// go through entire set and remove current path
			// if exist
			TSetFiles::iterator it = m_Paths.find(addonsPath);
			if (it != m_Paths.end())
				m_Paths.erase(it);
			break;
		}

	case lmLoad:
		// no code here
		break;
	}
}

//=============================================================================
int CAddonManager::GetTotalFiles()
{
	// return sum of pbo files in all paths
	int buffer = 0;
	for (TSetFiles::const_iterator pathIter=m_Paths.begin();
		pathIter!=m_Paths.end();++pathIter)
	{
		// get files from directory
		CFileSystemOperations::TvecFiles files;
		CFileSystemOperations::GetFiles(*pathIter,files,PBO_EXT);
		buffer += files.size();
	}

	// and return result
	return buffer;
}

//=============================================================================
int CAddonManager::LoadAddons(const TSetFiles &paths, TProgressCallback progressFunc)
{
	// overwrite current paths
	m_Paths = paths;

	// and simply load addons
	return LoadAddons("",progressFunc,lmLoad);
}

//=============================================================================
int CAddonManager::LoadAddons(const std::string &addonsPath, 
							  TProgressCallback progressFunc,ELoadMode mode)
{
	// erase old workspace
	CleanUp();

	// select mode and prepare to work
	SwitchLoadMode(mode,addonsPath);

	// prepare progress
	int numOfModules = GetTotalFiles();
	int currentModule = 0;

	for (TSetFiles::const_iterator pathIter=m_Paths.begin();
		pathIter!=m_Paths.end();++pathIter)
	{
		// set current work path
		std::string path = *pathIter;

		// get files from directory
		CFileSystemOperations::TvecFiles files;
		CFileSystemOperations::GetFiles(path,files,PBO_EXT);
		
		// load addon for each file in list
		for (CFileSystemOperations::TvecFiles::iterator it=files.begin();
			it != files.end();++it)
		{
			//temporary addon
			CAddon* addon = NULL;

			// inc modules counter
			++currentModule;

			// update progress
			if (progressFunc)
				if (!progressFunc(float(currentModule)/numOfModules))
					break;

			// Chybka se vloudila tohle je uplne blbe
			//if (isAdding)
			//	if (!CheckAddonDuplicity((*it).filename,addon))
			//		continue;
			
			// make space in file bank
			ParamFile PFile;
			int index = GFileBanks.Add();
			QFBank& bank = GFileBanks[index];

			// save addon in internal structures if
			// not exist
			StoreAddon(GetAddonRootClass
				(GetFullNameFromFile(path,(*it).filename,PBO_EXT.size()), 
					bank, PFile),path,*it,addon,index,bank);
		}
	
	}
	// set addons internal files if needed
	SetAddonsInternalFiles();

	// create dependecy map
	BuildAddonsDependecies();
/*
	TSetFiles test;
	GetAllPBOs(test);

	CFileSystemOperations::UnpackAddon(*test.begin());
*/
	// return addons count
	return m_AddonsMap.size();
}

//=============================================================================
CAddon* CAddonManager::LoadAddon(const std::string &filename)
{
		std::string path = filename;		

			//temporary addon
			CAddon* addon = new CAddon();

			// make space in file bank
			ParamFile PFile;
			int index = GFileBanks.Add();
			QFBank& bank = GFileBanks[index];						

			// and store file
			bank.open( GetFullNameFromFile("",filename,PBO_EXT.size()).c_str() );

			// fill addon internal files
			bank.ForEach( FillAddonFiles, addon );

			/// build directory tree
			addon->MakeDirectoriesMap();

			/// set internal files
			SetAddonsInternalFiles();

			/// and close bank
			bank.close();

	return addon;
}

//=============================================================================
void CAddonManager::AddAddons(const std::string &path, TProgressCallback progressFunc)
{
	LoadAddons(path,progressFunc,lmAdd);
}

//=============================================================================
void CAddonManager::RemoveAddons(const std::string &path, TProgressCallback progressFunc)
{
	LoadAddons(path,progressFunc,lmErase);
	return;

	int currentModule = 0;
	int numOfModules  = m_AddonsMap.size();
	CAddon::TVecAddons buffer;
	for (CAddonManager::TMapAddons::const_iterator it=m_AddonsMap.begin();
		it!=m_AddonsMap.end();++it)
	{
		// inc modules counter
		++currentModule;

		// update progress
		if (progressFunc)
			if (!progressFunc(float(currentModule)/numOfModules))
				break;

		// addon's path is same as removing path
		// insert addon into remove list
		if (it->second->GetPath() == path)
			buffer.push_back(it->second);			
	}
	
	// delete each addon in erase list
	// and remove it from addon manager maps
	for (CAddon::TVecAddons::iterator it=buffer.begin();
		it!=buffer.end();++it)
	{
		EraseAddonFromMgr(*it);
		delete *it;
	}

	int t = 0;
}

//=============================================================================
void CAddonManager::EraseAddonFromMgr(CAddon* what)
{
	// erase [what] addon from
	// internal maps

	// extract addon name for searching in maps
	std::string name = CAddon::ToLower(what->GetName());

	// All addons map
	CAddonManager::TMapAddons::iterator it = m_AddonsMap.find(name);
	if (it!=m_AddonsMap.end())
		m_AddonsMap.erase(it);

	// ValidAddonsMap
	it = m_ValidAddonsMap.find(name);
	if (it!=m_ValidAddonsMap.end())
		m_ValidAddonsMap.erase(it);

	// InvalidAddonsMap
	it = m_InvalidAddonsMap.find(name);
	if (it!=m_InvalidAddonsMap.end())
		m_InvalidAddonsMap.erase(it);	
}

//=============================================================================
void CAddonManager::SetAddonsInternalFiles()
{
	for (CAddonManager::TMapAddons::const_iterator it=m_ValidAddonsMap.begin();
		it!=m_ValidAddonsMap.end();++it)
	{
		CAddon* addon = it->second;
		const CFileSystemOperations::SFileInfoNode* node = addon->GetRootNode();
		bool emptyNode = node->children.empty();
		
		if (emptyNode)
			addon->InheritInternalFiles
				(GetAddonWithSamePBO(addon));
	}
}

//=============================================================================
const CAddon* CAddonManager::GetAddonWithSamePBO(const CAddon* whatAddon) const
{
	Assert(whatAddon);
	if (!whatAddon)
		return NULL;

	std::string filename = whatAddon->GetFileName();

	for (CAddonManager::TMapAddons::const_iterator it=m_ValidAddonsMap.begin();
		it!=m_ValidAddonsMap.end();++it)
	{
		CAddon* addon = it->second;
		const CFileSystemOperations::SFileInfoNode* node = addon->GetRootNode();
		bool emptyNode = node->children.empty();

		if (whatAddon != addon && addon->GetFileName() == filename && !emptyNode)
			return addon;
	}

	return NULL;
}

//=============================================================================
void CAddonManager::CleanUp()
{
	// remove object from memory
	for (TMapAddons::const_iterator it=m_AddonsMap.begin(); 
		it != m_AddonsMap.end(); ++it)
			delete it->second;

	// and clean up maps
	m_AddonsMap.clear();
	m_ValidAddonsMap.clear();
	m_InvalidAddonsMap.clear();
}

//=============================================================================
std::string CAddonManager::GetFullNameFromFile
	(const std::string &path,const std::string &filename,std::string::size_type extSize)
{
	// make and return full filename with path
	std::string fullFilename = filename;

	if (path.size())
		fullFilename = path + "\\" + fullFilename;

	// erase extension
	fullFilename.erase(fullFilename.size()-extSize);

	return fullFilename;
}

//=============================================================================
void CAddonManager::BuildAddonsDependecies()
{
	for (TMapAddons::const_iterator it=m_ValidAddonsMap.begin(); 
		it != m_ValidAddonsMap.end(); ++it)
			it->second->CreateDependecies();
}

//=============================================================================
CAddon* CAddonManager::CreateEmptyAddon(const std::string &addonName)
{
	/// create new addon
	CAddon* addon = new CAddon();
	addon->SetAddonName(addonName);
	addon->SetInvalid();
	addon->SetAddonManager(this);

	// store it into addons map and invalid addons map
	m_AddonsMap.insert(std::make_pair<std::string,CAddon*>
		(CAddon::ToLower(addonName),addon));
	
	m_InvalidAddonsMap.insert(std::make_pair<std::string,CAddon*>
		(CAddon::ToLower(addonName),addon));

	// and return it
	return addon;
}
