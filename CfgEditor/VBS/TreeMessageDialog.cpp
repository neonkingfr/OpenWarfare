#include "precomp.h"
#include "TreeMessageDialog.h"
#include "FileSystemOperations.h"

#define wxTestMYBUTTON 1234
#define wxTestMYTREE 1231

////Event Table Start
BEGIN_EVENT_TABLE(TreeMessageDialog,wxDialog)
	EVT_BUTTON(wxTestMYBUTTON,TreeMessageDialog::OnOkButtonClick)
	EVT_TREE_SEL_CHANGING(wxTestMYTREE,TreeMessageDialog::OnTreeSelChanged)
	EVT_TREE_ITEM_ACTIVATED(wxTestMYTREE,TreeMessageDialog::OnTreeSubmit)
END_EVENT_TABLE()

//=============================================================================
TreeMessageDialog::TreeMessageDialog(wxWindow* parent, const wxString& title)
:wxDialog(parent, -1, title, wxDefaultPosition, wxSize(480, 640), wxCAPTION|wxTHICK_FRAME|wxSYSTEM_MENU|wxCLOSE_BOX | wxTAB_TRAVERSAL|wxNO_BORDER)
{
	m_messageArea = new wxStaticText(this, wxID_ANY, "", wxDefaultPosition, wxSize(-1, -1), wxST_NO_AUTORESIZE);
	
	m_tree = new wxTreeCtrl(this, wxTestMYTREE, wxDefaultPosition, wxDefaultSize, wxTR_HAS_BUTTONS | wxTR_FULL_ROW_HIGHLIGHT | wxTR_HIDE_ROOT | wxTR_MULTIPLE | wxTR_EXTENDED);

	m_okButton = new wxButton(this, wxTestMYBUTTON, "Ok", wxDefaultPosition, wxSize(80, 25));
	m_cancelButton = new wxButton(this, wxID_CANCEL, "Cancel", wxDefaultPosition, wxSize(80, 25));

	wxBoxSizer* TopSizer = new wxBoxSizer(wxVERTICAL);
	TopSizer->Add(m_messageArea, 0, wxGROW | wxALIGN_LEFT | wxALL, 0);
	TopSizer->AddSpacer(10);
	TopSizer->Add(m_tree, 1, wxGROW | wxALIGN_LEFT | wxALL, 0);

	wxBoxSizer* ButtonsSizer = new wxBoxSizer(wxHORIZONTAL);
	ButtonsSizer->Add(m_okButton, 0, wxALIGN_LEFT | wxALL, 0);
	ButtonsSizer->AddStretchSpacer();
	ButtonsSizer->Add(m_cancelButton, 0, wxALIGN_LEFT | wxALL, 0);

	wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
	sizer->Add(TopSizer, 1, wxGROW | wxALIGN_LEFT | wxALL, 10);
	sizer->Add(ButtonsSizer, 0, wxGROW | wxALIGN_LEFT | wxALL, 10);
	SetSizer(sizer);

	m_cancelButton->SetFocus();
}

//=============================================================================
void TreeMessageDialog::SetMessage(const wxString& message)
{
	m_messageArea->SetLabel(message);
}

//=============================================================================
void TreeMessageDialog::OnOkButtonClick(wxCommandEvent& event)
{
	const std::string BIN_EXT	  = ".bin";
	const std::string CPP_EXT	  = ".cpp";

	if (m_itemId==m_tree->GetRootItem())
	{
		wxDialog::EndModal(wxID_CANCEL);
		return;
	}


	// go backward to root and store item text
	// to compile full config's path
		std::string buf = m_selected;

		wxTreeItemId child;
		while (m_itemId.IsOk())
		{
			m_itemId = m_tree->GetItemParent(m_itemId);	
			if (m_itemId == m_tree->GetRootItem())
				break;

			buf = m_tree->GetItemText(m_itemId) + "\\" + buf;
			int t = 0;
		}

		// check if file has been selected(directory don't have extension)
		m_selected = buf;
		if (CFileSystemOperations::IsExt(m_selected,BIN_EXT) || 
			CFileSystemOperations::IsExt(m_selected,CPP_EXT))
		{
				wxDialog::EndModal(wxID_OK);
				return;
		}

		// reset state
		m_selected = "";
		m_itemId.Unset();
		//wxDialog::EndModal(wxID_CANCEL);
}

//=============================================================================
void TreeMessageDialog::OnTreeSelChanged(wxTreeEvent& event)
{
	wxTreeItemId item = event.GetItem();
	if (item.IsOk())
	{
		m_selected = m_tree->GetItemText(item);
		m_itemId = item;
	}	
}

//=============================================================================
void TreeMessageDialog::OnTreeSubmit(wxTreeEvent& event)
{
	OnOkButtonClick(event);
}