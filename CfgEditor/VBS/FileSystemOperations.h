#pragma once
#include <vector>
#include <string>
#include <map>

#include <Windows.h>
//=============================================================================

/// CFileSystemOperations class
class CFileSystemOperations
{
public:
	CFileSystemOperations(void){};
	~CFileSystemOperations(void){};

	/// SFileInfo struct that stores info about addon file
	struct SFileInfo
	{
		SFileInfo(const std::string &filename, unsigned int filesize):
			filename(filename),filesize(filesize){};

		/// Filename
		std::string filename;

		/// Length of file
		unsigned int filesize;
	};

	/// Forward declaration
	struct SFileInfoNode;
	typedef std::vector<SFileInfoNode*> TvecFilesNodes;

	/// SFileInfoNode struct that stores info about addon file structure
	struct SFileInfoNode
	{
		SFileInfoNode(const std::string &name,bool isDirectory=true):
			nodeName(name),isDirectory(isDirectory){};
		
		/// Copy constructor
		SFileInfoNode(const SFileInfoNode* second)
		{
			_ASSERT(second);
			if (!second)
				return;

			// copy children
			for (TvecFilesNodes::const_iterator it=second->children.begin();
				it!=second->children.end();++it)
					children.push_back(new SFileInfoNode(*it));

			nodeName = second->nodeName;
			isDirectory = second->isDirectory;
			fullpath	= second->fullpath;
		}

		/// Destroy node and children
		~SFileInfoNode()
		{
			for (TvecFilesNodes::iterator it=children.begin();
				it!=children.end();++it)
					delete *it;
		}

		/// Return directory flag
		bool IsDirectory() const { return isDirectory; }

		/** If <i>what</i> node is child, return true
			
			\param what is tested node
		*/
		bool IsIn(const SFileInfoNode &what) const 
		{
			for (TvecFilesNodes::const_iterator it=children.begin();
				it!=children.end();++it)
					if ((*it)->nodeName == what.nodeName)
						return true;

			return false;
		}

		/** Check file node existenci by node name.

			 If node is child(by string), return true
			 UNSAFE! .. must be in same subDir

			 \param what name of file node
		*/
		bool IsIn(const std::string &what) const
		{
			for (TvecFilesNodes::const_iterator it=children.begin();
				it!=children.end();++it)
				if ((*it)->nodeName == what)
					return true;

			return false;
		}

		/** Return child node.

			If node doesn't exist in tree, return NULL

			\param what name of file node
		*/
		SFileInfoNode* GetChild(const std::string &what)
		{
			for (TvecFilesNodes::const_iterator it=children.begin();
				it!=children.end();++it)
				if ((*it)->nodeName == what)
					return *it;

			return NULL;
		}

		/** Set full path of file node
			
			internal files map(CAddon) using fullDirectory paths
			as its keys. So if you need directory content,
			you can simply get it by fullpath value

			\param parent if exist, link node to existing
				path. Otherwise, set path to root\\nodename
			
		*/
		void SetFullPath(const SFileInfoNode* parent)
		{
			if (parent && parent->fullpath.size())
				fullpath = parent->fullpath + '\\'+ nodeName;
			else
				fullpath = nodeName;
		}

		/// String contains name of node(e.g. Sound)
		std::string nodeName;

		/** Full path (e.g. Data/Sound) 
			for internal files map(CAddon)
		*/
		std::string fullpath;

		/// Variables for directory flag
		bool isDirectory;

		/// children vector
		TvecFilesNodes children;
	};

	typedef std::vector<std::string> TvecDirectories;
	typedef std::vector<SFileInfo> TvecFiles;
	typedef std::map<std::string,CFileSystemOperations::TvecFiles> TmapDirectory;
	
	/** Get files from directory
		\param path Directory path
		
		\param fileList Output container that will be store 
				SFileInfo structures

		\param bCountHidden If false, function rejected all 
				hidden files
	*/
	static int GetFiles(const std::string &path, TvecFiles& fileList, 
		bool bCountHidden = false);
	
	/** Get files with <i>file_ext</i> extension from directory
		\param path Directory path

		\param fileList Output container that will be store 
				SFileInfo structures

		\param file_ext Filter extension

		\param bCountHidden If false, function rejected all 
				hidden files		
	*/
	static int GetFiles(const std::string &path, TvecFiles& fileList, 
		const std::string &file_ext, bool bCountHidden = false);
	
	/** Extract files with <i>file_ext</i> extension
		
		\param fileList Work container
		\param file_ext Filter extension
	*/
	static int GetFiles(TvecFiles &fileList, const std::string &file_ext);

	/// delete all files from list on given path
	static bool DeleteFiles(const std::string &path, TvecFiles& fileList);

	/// Return true if <i>path</i> is empty or doesn't exist
	static bool IsDirectoryEmpty(const std::string &path);

	/** Check extension.
		Return true if extension passes to filename extension

		\param filename Tested filename
		\param file_ext Correct extension
	*/
	static bool IsExt(const std::string& filename, const std::string &file_ext);

	/** Return file path from full filename.
		e.g. Data/Sound/some.ogg -> Data/Sound

		\param filename Full filename with path
	*/
	static std::string GetPath(const std::string &filename);

	/** Return file name from full file path.
		e.g. Data/Sound/some.ogg -> some.ogg

		\param path Full path
	*/
	static std::string GetFileName(const std::string &path);

	/** Return way to Root directory
		e.g. Data/Sound/new -> {'Data','Sound','new'}

		\param path Full path
	*/
	static TvecDirectories GetDirectoriesToRoot(const std::string &path);

	/** Unpack pbo via BankRev.exe
		\param
	*/
	static bool UnpackPBO(const std::string &path,
		const std::string &fullAddonName,const std::string &whereDir);

	/** Pack pbo via FileBank.exe
		\param
	*/
	static bool PackPBO(const std::string &path,
		const std::string &addonDirectory, const std::string &params, 
		const std::string &targetPBO);

	/** Delete non-empty directory.
		With all subfolders and files.

		\param sPath erasing path
	*/
	static bool DeleteDirectory(const TCHAR* sPath);

	static bool CheckFileExist(const std::string &what, const std::string &where);
};
