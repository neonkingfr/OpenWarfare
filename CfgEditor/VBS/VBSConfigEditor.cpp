#include "precomp.h"
#include "base/GenericEditor.h"
#include "base/EditActions.h"
#include "base/MainFrame.h"

#include "base/ListMessageDialog.h"
#include "TreeMessageDialog.h"

#include "base/ParamFileTree.h"
#include "base/PropertiesPanel.h"
#include "FileSystemOperations.h"
#include "Common/XMLConfig.h"
#include "Common/WinApiUtil.h"

#include "VBSConfigEditor.h"
#include "VBSMainFrame.h"
#include "VBSParamConfig.h"

#include "GarbageCollector.h"

#include "Addon.h"
#include "AddonManager.h"

#include <fstream>

// wx includes
#ifndef WX_PRECOMP
	#include <wx/wx.h>
#include "wx/xml/xml.h"
#else
	#include <wx/wxprec.h>
#endif

// inicialize GC(yes i know, it's weird, but don't judge me)
CGarbageCollector* CGarbageCollector::m_instance = NULL;

static const std::string FILEBANK_EXEC = "filebank.exe";
static const std::string BANKREV_EXEC = "bankrev.exe";

//=============================================================================
CResult VBSConfigEditor::OnAppInit()
{
	config = CreateEditorConfig();	//virtual
	m_loadOnInit = true;

	if(config->Load() == false)
	{
		wxMessageDialog dialog(NULL, "Config file load failed. Using default", "Warning", wxICON_INFORMATION | wxOK);
		dialog.ShowModal();
	}

	// create param config
	PConfig = CreateParamConfig();	//virtual

	// Init working directory
	CheckWorkDir();

	// Init data directory
	CheckDataDir();

	// Init external bin directory
	CheckExtBinDir();

	//Init base config files directory
	CheckCfgsDir();

	static_cast<VBSParamConfig*>(PConfig)->SetTempDir(GetTempDir());
	MFrame = CreateMainFrame();		//virtual
	UManager = ENF_NEW UndoManager();
	
  //set the current directory, otherwise paramfile cannot resolve relative paths
  if(!m_dataDirectory.empty())
    SetCurrentDirectory(m_dataDirectory.c_str());

	//g_editor->OnAppConfigChanged();
	OnAppConfigChanged();
	
	return New();	//new (unamed) config
}

//=============================================================================
ParamConfig* VBSConfigEditor::CreateParamConfig()
{
	return ENF_NEW VBSParamConfig();
}

//=============================================================================
void VBSConfigEditor::CheckWorkDir()
{
	/*
	//apply zmien
	XMLConfig* config = g_editor->GetConfig();
	XMLConfigNode* node = config->GetNode("defaults", true);

	wxString StringValue;
	if(node->GetValue("TempDir", StringValue))
	{*/
		if (PConfig)
		{
			char buf[256];
			taGetAplicationDir(buf);
			m_tempDirectory = std::string(buf)+"/tmp";
			//wxMessageBox(m_tempDirectory);
			static_cast<VBSParamConfig*>(PConfig)->SetTempDir(m_tempDirectory);
			static_cast<VBSParamConfig*>(PConfig)->CheckPersist();
		}
/*
}
	else
	{
		bool isSet = false;
		while (!isSet)
			isSet = SetTempDir(true);		
	}
*/
}

//=============================================================================
void VBSConfigEditor::CheckDataDir()
{
	//apply zmien
	XMLConfig* config = g_editor->GetConfig();
	XMLConfigNode* node = config->GetNode("defaults", true);

	wxString StringValue;
	if(node->GetValue("DataDir", StringValue))
	{
		if (PConfig)
		{
			m_dataDirectory = StringValue.c_str();
			static_cast<VBSParamConfig*>(PConfig)->SetDataDir(m_dataDirectory);
		}
	}
	else
		wxMessageBox("Pbo's data directory wasn't set.\n Using default from pbo.",
			"WARNING",wxICON_EXCLAMATION);

}

//=============================================================================
void VBSConfigEditor::CheckExtBinDir()
{
	//apply zmien
	XMLConfig* config = g_editor->GetConfig();
	XMLConfigNode* node = config->GetNode("defaults", true);

	wxString StringValue;
	if(node->GetValue("ExtBinDir", StringValue))
	{
		if (PConfig)
		{
			m_extBinDirectory = StringValue.c_str();
			static_cast<VBSParamConfig*>(PConfig)->SetBinDir(m_extBinDirectory);
		}
	}
	else
		wxMessageBox("external bin(filebank,bankrev) path wasn't set.\n Using default.",
		"WARNING",wxICON_EXCLAMATION);

}

//=============================================================================
void VBSConfigEditor::CheckCfgsDir()
{
	//apply zmien
	XMLConfig* config = g_editor->GetConfig();
	XMLConfigNode* node = config->GetNode("defaults", true);

	wxString StringValue;
	if(node->GetValue("BaseCfgsDir", StringValue))
	{
		if (PConfig)
		{
			m_baseConfigsDirectory = StringValue.c_str();
			static_cast<VBSParamConfig*>(PConfig)->SetCfgsDir(m_baseConfigsDirectory);
		}
	}
	else
		wxMessageBox("directory with base configuration files wasn't set.",
		"WARNING",wxICON_EXCLAMATION);

}

//=============================================================================
CResult VBSConfigEditor::ImportConfigTreeRaw(const std::string &filename)
{
	using namespace std;
	// first is root 
	string rootFilename;
	string configFile;
	string buf;	

	// open file
	ifstream is(filename.c_str(),ios::in);

	// read first line(root item)
	getline(is,buf);
	wxString line = buf;
	rootFilename = line.Before(';');
	configFile   = line.After(';');
	
	// read line be line dependencies 
	// from file and store it into list
	CAddon::TVecAddonsNames configList;
	while (is.good())
	{
		getline(is,buf);
		if (buf.size())
			configList.push_back(buf);

		// TODO: Preload
	}

	// close stream
	is.close();

	// and load root addon
	return static_cast<VBSParamConfig*>(PConfig)->LoadPBOFile(rootFilename,configFile);
}

//=============================================================================
CResult VBSConfigEditor::ImportConfigTree(const std::string &filename)
{
	wxXmlDocument doc;
	wxXmlNode *node = NULL;
	std::string rootFilename;
	std::string configFile;
	std::string buf;	
	CAddon::TVecAddonsNames configList;

	// Load xml document. If load failed, xml file is damaged
	if (!doc.Load(filename))
		return false;

	// Get root node
	node = doc.GetRoot();

	// Test to name of root node
	if (node->GetName() != "Export")
		return false;

	// Iterate xml nodes and read configuration
	std::string addonsNames;
	wxXmlNode *child = node->GetChildren();
	while (child)
	{
		// load root addon
		if (child->GetName() == "Root")		
		{
			wxString line = child->GetPropVal("value","");
			rootFilename = line.Before(';');
			configFile   = line.After(';');
		}
		
		// and store dependency tree
		if (child->GetName() == "Tree")
		{
			buf = child->GetPropVal("value","");
			if (buf.size())
				configList.push_back(buf);
		}

		child = child->GetNext();
	}

	if (!rootFilename.size())
		return false;

	// and load root addon
	return static_cast<VBSParamConfig*>(PConfig)->LoadPBOFile(rootFilename,configFile);
}

//=============================================================================
bool VBSConfigEditor::TryToPreloadDependencies(const std::string &rootName)
{
	wxXmlDocument doc;
	wxXmlNode *node = NULL;
	std::string rootFilename;
	std::string configFile;
	std::string buf;	
	CAddon::TVecAddonsNames configList;

	wxString normRootName = rootName;
	normRootName.Replace("/","\\");

	// Load xml document. If load failed, xml file is damaged
	if (!doc.Load("C:\\workdir\\CE\\bin\\export_cache.xml"))
		return false;

	// Get root node
	node = doc.GetRoot();

	// Test to name of root node
	if (node->GetName() != "Export")
		return false;

	// Iterate xml nodes and read configuration
	std::string addonsNames;
	wxXmlNode *child = node->GetChildren();
	bool parsing = false;
	while (child)
	{
		// load root addon
		if (child->GetName() == "Root")		
		{
			if (!parsing)
			{
				wxString line = child->GetPropVal("value","");
				if (std::string(normRootName.c_str())==line)
					parsing=true;
			}
			else
				break;	
		}

		// and store dependency tree
		if (child->GetName() == "Tree" && parsing)
		{
			buf = child->GetPropVal("value","");
			if (buf.size() && !CAddon::IsIn(configList,buf))
				configList.push_back(buf);
		}

		child = child->GetNext();
	}

	if (parsing)
	{

		return true;
	}

	return false;
}

//=============================================================================
CResult VBSConfigEditor::Load(const char* FileName)
{
	wxString ext = FileName;
	ext = ext.AfterLast('.');
	if (ext == wxString("pbo"))
	{
		// konfigy v pbocku
		VBSParamConfig::TVecConfigNames configList;
		static_cast<VBSParamConfig*>(PConfig)->GetAllConfigFiles(FileName,configList);

		std::string configFile;
		// pokud je v souboru pouze 
		// jeden config, muzu ho rovnou nacist
		switch(configList.size())
		{
		case 0:
			//assert(configList.size());
			return CResult(false,"Missing at least one config file to load!");
			break;
		case 1:
			configFile = *configList.begin();
			
			//break;
		default:
			
			// list
			//std::string configStr = ShowConfigDlg(configList);

			// tree
			std::string configStr = ShowConfigDlg(FileName);
			
			if (!configStr.size())
				return CResult(false,"No config file has been chosen.");

			configFile = configStr;
			break;
		}

		
		//TryToPreloadDependencies(std::string(FileName)+';'+configFile);
		return static_cast<VBSParamConfig*>(PConfig)->LoadPBOFile(FileName,configFile);
	}

	if (ext == wxString("xml"))
		return ImportConfigTree(FileName);
  
  // use base directory
  char buff[MAX_PATH];
  sprintf(buff, "%s/%s", m_baseConfigsDirectory.c_str(), "config.cpp");

	EArray<PCFilename> fnames;
	fnames.Insert(PCFilename(buff));			//load this as first
	fnames.Insert(PCFilename(FileName));	//load as update of first. this is the edit context because is last

  return PConfig->Load(fnames);
  
}

//=============================================================================
CResult VBSConfigEditor::Save(const char* FileName)
{
	return static_cast<VBSParamConfig*>(PConfig)->Save(FileName);
}

//=============================================================================
bool VBSConfigEditor::CanRestartApp()
{
	if (!AnyChanges())
		return true;

	int dlgRes = wxMessageBox("all unsaved changes will be forgotten and application will cleans up current workdir. Continue?",
		"Close project",wxYES_NO, MFrame);

	return (dlgRes==wxYES);
}


//=============================================================================
bool VBSConfigEditor::CanRemoveTemp()
{
	if (!CFileSystemOperations::IsDirectoryEmpty(m_tempDirectory))
	{
		int dlgRes = wxMessageBox("Current temp directory isn't empty. Delete it anyway?",
			"Confirm delete",wxYES_NO, MFrame);

		if (dlgRes==wxID_NO)
			return false;
	}

	return true;
}

//=============================================================================
bool VBSConfigEditor::SetTempDir(bool onStart)
{
	std::string msg = "Select temporary directory for unpacking. \n";
	msg += "PLEASE NOTE: Content of this directory will be errased on application end";
	wxDirDialog tempDirDialog(MFrame,(_T(msg.c_str())));

	tempDirDialog.SetPath(m_tempDirectory.c_str());

	// zeptam se jestli muzu smazat projekt
	if (!CanRestartApp() && !onStart)
		return false;

	if (tempDirDialog.ShowModal() == wxID_OK)
	{
		// restartuji projekt
		if (!onStart)
			GenericEditor::New();

		// smazu stavajici workdir
		static_cast<VBSParamConfig*>(PConfig)->CleanUpTemp(false);

		// ulozim novy
		m_tempDirectory = tempDirDialog.GetPath();		
		assert(PConfig);
		if (PConfig)
		{
			static_cast<VBSParamConfig*>(PConfig)->SetTempDir(m_tempDirectory);
			static_cast<VBSParamConfig*>(PConfig)->CheckPersist();
			// a procistim ho
			static_cast<VBSParamConfig*>(PConfig)->CleanUpTemp(true);
		}

		// store dir into config.xml
		XMLConfig* config = g_editor->GetConfig();
		XMLConfigNode* node = config->GetNode("defaults", true);

		assert(node);
		if (node)
			node->SetValue("TempDir",m_tempDirectory.c_str());


		return true;
	}

	return false;
}

//=============================================================================
bool VBSConfigEditor::SetDataDir()
{
	std::string msg = "Select data directory for pbo's [prefix] property. \n";
	wxDirDialog dataDirDialog(MFrame,(_T(msg.c_str())));

	dataDirDialog.SetPath(m_dataDirectory.c_str());	

	if (dataDirDialog.ShowModal() == wxID_OK)
	{
		// ulozim novy
		m_dataDirectory = dataDirDialog.GetPath();		
		assert(PConfig);
		if (PConfig)
			static_cast<VBSParamConfig*>(PConfig)->SetDataDir(m_dataDirectory);

    //also set the current directory since otherwise paramfile cannot resolve a relative path
    SetCurrentDirectory(m_dataDirectory.c_str());

		// store dir into config.xml
		XMLConfig* config = g_editor->GetConfig();
		XMLConfigNode* node = config->GetNode("defaults", true);

		assert(node);
		if (node)
			node->SetValue("DataDir",m_dataDirectory.c_str());


		return true;
	}

	return false;
}

//=============================================================================
void VBSConfigEditor::OnAppConfigChanged()
{
	GenericEditor::OnAppConfigChanged();
	UpdateBinDir();
	UpdateCfgDir();
}

//=============================================================================
void VBSConfigEditor::UpdateBinDir()
{
	XMLConfig* config = GetConfig();
	XMLConfigNode* node = config->GetNode("defaults", true);

	wxString StringValue;
	if(node->GetValue("ExtBinDir", StringValue))
	m_extBinDirectory = StringValue;
	assert(PConfig);
	if (PConfig)
		static_cast<VBSParamConfig*>(PConfig)->SetBinDir(m_extBinDirectory);
}

//=============================================================================
void VBSConfigEditor::UpdateCfgDir()
{
	XMLConfig* config = GetConfig();
	XMLConfigNode* node = config->GetNode("defaults", true);

	wxString StringValue;
	if(!node->GetValue("BaseCfgsDir", StringValue))
		return;

	assert(PConfig);
	if (PConfig)
		static_cast<VBSParamConfig*>(PConfig)->SetCfgsDir(StringValue.c_str());
}

//=============================================================================
bool VBSConfigEditor::SetBinDir()
{
	std::string msg = "Select external bin directory(path to bankrev and filebank).";
	wxDirDialog extBinDirDialog(MFrame,(_T(msg.c_str())));

	extBinDirDialog.SetPath(m_extBinDirectory.c_str());	

	if (extBinDirDialog.ShowModal() == wxID_OK)
	{
		// store old_extBin for check
		std::string buf = m_extBinDirectory;
		
		// ulozim novy
		m_extBinDirectory = extBinDirDialog.GetPath();		
		
		// check existence
		if (!CheckExtAppExist())
		{
			// restore old path
			m_extBinDirectory = buf;
			return false;
		}

		assert(PConfig);
		if (PConfig)
			static_cast<VBSParamConfig*>(PConfig)->SetBinDir(m_extBinDirectory);

		// store dir into config.xml
		XMLConfig* config = g_editor->GetConfig();
		XMLConfigNode* node = config->GetNode("defaults", true);

		assert(node);
		if (node)
			node->SetValue("ExtBinDir",m_extBinDirectory.c_str());

		return true;
	}

	return false;
}

//=============================================================================
bool VBSConfigEditor::CheckExtAppExist(bool verbose)
{
	return CheckExtAppExist(m_extBinDirectory,verbose);
}

//=============================================================================
bool VBSConfigEditor::CheckExtAppExist(const std::string &where, bool verbose)
{
	bool tmp = true;

	// check FILE_BANK exists
	if (!CFileSystemOperations::CheckFileExist(FILEBANK_EXEC,where))
	{
		if (verbose)
			wxMessageBox(FILEBANK_EXEC+" hasn't found in: "+where,
			"ERROR",wxICON_ERROR);

		tmp &= false;
	}

	// check BANK_REV exists
	if (!CFileSystemOperations::CheckFileExist(BANKREV_EXEC,where))
	{
		if (verbose)
			wxMessageBox(BANKREV_EXEC+" hasn't found in: "+where,
			"ERROR",wxICON_ERROR);

		tmp &= false;
	}
	
	return tmp;
}

//=============================================================================
bool VBSConfigEditor::Load()
{
	if(AnyChanges() && !PConfig->IsEmpty())
	{
		wxMessageDialog dialog(MFrame, _T("Save changes before load another config?"), _T("Unsaved data detected"), wxICON_QUESTION | wxYES_NO);

		if(dialog.ShowModal() == wxID_YES)
			GenericEditor::Save();
	}

	wxFileDialog LoadDialog(MFrame, _T("Load Config"), _T(""), _T(""), 
		_T("Config files (*.cpp)|*.cpp|Addon files (*.pbo)|*.pbo|Binary config files (*.bin)|*.bin|CME exported files (*.xml)|*.xml" ), 
		wxOPEN | wxFILE_MUST_EXIST);
	LoadDialog.SetDirectory(m_PBOpath.c_str()); 
	LoadDialog.Centre();
	CResult result = false;


	if(LoadDialog.ShowModal() == wxID_OK)
	{
		Clear();
		wxString LoadPath = LoadDialog.GetPath();
		LoadPath.Replace("\\", "/");
		wxYield();
		result = Load(LoadPath.c_str());
		SetChanged(false);
	}

	if(result.IsOk())
		OnConfigLoaded();

	if(result.IsCommented())
	{
		wxMessageDialog dialog(MFrame, result.GetComment(), _T("Loading config"), wxICON_INFORMATION | wxOK);
		dialog.ShowModal();
	}

	return result.IsOk();
}

//=============================================================================
bool VBSConfigEditor::LoadOnInit(const char* filename)
{
	CResult result = false;

		Clear();
		wxString LoadPath = filename;
		LoadPath.Replace("\\", "/");
		wxYield();
		result = Load(LoadPath.c_str());
		SetChanged(false);

	if(result.IsOk())
		OnConfigLoaded();

	if(result.IsCommented())
	{
		wxMessageDialog dialog(MFrame, result.GetComment(), _T("Loading config"), wxICON_INFORMATION | wxOK);
		dialog.ShowModal();
	}

	return result.IsOk();
}

//=============================================================================
void VBSConfigEditor::AddAddonToTree(CAddon *addon, wxTreeCtrl *tree)
{
	wxTreeItemId item;
	wxTreeItemId parent = tree->GetRootItem();
	// Create node data structure
	STreeNodeData *data = new STreeNodeData(addon);
	CGarbageCollector::GetSingleton()->PushData(data);
	AddPboFilesToTree(tree,tree, data->GetAddon(), tree->GetRootItem());
}

//=============================================================================
void VBSConfigEditor::AddPboFilesToTree(wxTreeCtrl *parentTree,wxTreeCtrl *tree, 
										CAddon *addon, wxTreeItemId parent)
{
	parent = parentTree->AppendItem(parent, "ROOT");
	AddFilesToTree(parentTree,tree, addon, parent, addon->GetRootNode());
}

//=============================================================================
void VBSConfigEditor::AddFilesToTree(wxTreeCtrl *parentTree, wxTreeCtrl *tree, 
									 CAddon *addon, wxTreeItemId parent, 
									 const CFileSystemOperations::SFileInfoNode *fileNode)
{
	if (!fileNode)
		return;

	const CFileSystemOperations::TvecFilesNodes &fileNodes = fileNode->children;
	// Add folder nodes to parent node
	for each (const CFileSystemOperations::SFileInfoNode *node in fileNodes)
	{
		wxTreeItemId item;
		item = parentTree->AppendItem(parent, node->nodeName);
		// Recurse call for sub nodes
		AddFilesToTree(parentTree,tree, addon, item, node);
	}

	const CFileSystemOperations::TvecFiles &files = addon->GetFilesFromNode(fileNode);
	// Add file nodes to parent node
	for each (const CFileSystemOperations::SFileInfo &info in files)
	{			
		parentTree->AppendItem(parent, info.filename);
	}
}

//=============================================================================
std::string VBSConfigEditor::ShowConfigDlg(const std::string &filename)
{
	std::string selected;

	CAddonManager* addonMgr = new CAddonManager();
	CAddon* addon = addonMgr->LoadAddon(filename);

	TreeMessageDialog treeDlg(NULL,"Select config file");
	wxTreeCtrl* tree = treeDlg.GetTreeCtrl();

	AddAddonToTree(addon,tree);
	tree->Expand(tree->GetRootItem());

	if (treeDlg.ShowModal()==wxID_OK)
		selected = treeDlg.GetSelectedPath();

	delete addonMgr;
	delete addon;
	CGarbageCollector::GetSingleton()->CleanUpTrash();
	delete CGarbageCollector::GetSingleton();

	return selected;
}

//=============================================================================
std::string VBSConfigEditor::ShowConfigDlg(const VBSParamConfig::TVecConfigNames &configList)
{
	std::string selected;

	ListMessageDialog listDlg(NULL,"Select config file");
	wxListBox* list = listDlg.GetListBox();
	// fill listbox
	int counter = 0;
	for (VBSParamConfig::TVecConfigNames::const_iterator it=configList.begin();
		it!=configList.end();++it)
		list->Insert(*it,counter++);

	if (listDlg.ShowModal()==wxID_OK)
	{
		// check list ptr
		assert(list);
		if (!list)
			return selected;

		// and return selected config
		int id=list->GetSelection();
		selected = list->GetString(id);
	}

	return selected;
}

//=============================================================================
MainFrame* VBSConfigEditor::CreateMainFrame()
{
	MainFrame* frame = new VBSMainFrame();
	frame->Create(NULL, wxID_ANY, "Config Editor", wxDefaultPosition, 
		wxSize(1024, 768), wxDEFAULT_FRAME_STYLE, "GenericConfigEditor");

	return frame;
}

//=============================================================================
XMLConfig* VBSConfigEditor::CreateEditorConfig()
{
	//vytvorime config
	char AppDir[256];
	taGetAplicationDir(AppDir);

	wxString ConfigFile = AppDir;
	ConfigFile += "/VBSConfigEditor.xml";

	return ENF_NEW XMLConfig(ConfigFile.c_str());
}

//=============================================================================
bool VBSConfigEditor::New()
{
	if(AnyChanges() && !PConfig->IsEmpty())
	{
		wxMessageDialog dialog(MFrame, _T("Save changes of current config?"), _T("Save"), wxICON_QUESTION | wxYES_NO);

		if(dialog.ShowModal() == wxID_YES)
		{
			if(!GenericEditor::Save())
				return false;
		}
	}
	
	Clear();
	SetChanged(true);
	MFrame->UpdateTitle();
	OnConfigLoaded();
	return true;
}
/*
//=============================================================================
bool VBSConfigEditor::New(bool reloadConfig)
{
	if(AnyChanges() && !PConfig->IsEmpty())
	{
		wxMessageDialog dialog(MFrame, _T("Save changes of current config?"), _T("Save"), wxICON_QUESTION | wxYES_NO);

		if(dialog.ShowModal() == wxID_YES)
		{
			if(!GenericEditor::Save())
				return false;
		}

		// a nastavim zpatky flagu
		// ze nejsou zadne zmeny
		SetChanged(false);
	}

	Clear();
	if (!reloadConfig)
		return true;

	assert(PConfig);
	if (PConfig)
		static_cast<VBSParamConfig*>(PConfig)->CleanUpTemp(true);
	OnConfigLoaded();
	return true;
}
*/

//overwrite to check string

bool VBSConfigEditor::DoRealizeAction(const EAModifyVariable* action)
{
  const DString& id = action->GetId();
  const DString& value = action->GetValue();
  PCNode* node = PConfig->FindNode(id.c_str());

  if(!node)
    return false;

  PCVar* variable = node->ToVariable();
  assert(variable);

  if(!variable)
    return false;
  
  wxString str = value.c_str();
  wxString path = m_dataDirectory.c_str();
  path.Replace("/", "\\");

  int index = str.Find(path);
  if(index >= 0)
    str.erase(0, index + m_dataDirectory.size() -1);

  variable->SetValue(str.c_str());

	MFrame->GetPropGrid()->Update();
  return true;
}
