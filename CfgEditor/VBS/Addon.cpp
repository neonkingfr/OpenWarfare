#include <es\common\win.h>
#include "Addon.h"
#include "AddonManager.h"

//=============================================================================
void CAddon::LoadDependecies(ConstParamEntryPtr entr)
{
	if (!entr)
		return;

	ParamEntryVal val = *entr;
	// go through entire array and push every requires into list
	for(int i = 0; i < val.GetSize(); i++)
		m_dependenciesNames.push_back(val[i].GetValueRaw().Data());
}

//=============================================================================
void CAddon::AddDependenciesVec(const TVecAddonsNames &dependencies)
{
	// go through entire dependencies list and add all new addon
	// names into m_DependenciesNames
	for (TVecAddonsNames::const_iterator it=dependencies.begin();
		it!=dependencies.end();++it)
			if (!IsIn(m_dependenciesNames,*it))
				m_dependenciesNames.push_back(*it);	
}

//=============================================================================
void CAddon::InheritInternalFiles(const CAddon* from)
{
	Assert(from);
	if (!from)
		return;

	// copy files structures
	m_addonInternalFiles = from->m_addonInternalFiles;
	m_directoryMap		 = from->m_directoryMap;
	
	// clear old root
	delete m_root;

	// and copy new
	m_root				 = new CFileSystemOperations::SFileInfoNode(from->m_root);
}

//=============================================================================
void CAddon::MakeDirectoriesMap()
{
	for (CFileSystemOperations::TvecFiles::iterator it=m_addonInternalFiles.begin();
		it!=m_addonInternalFiles.end();++it)
	{
		// extract filename and file path from 
		// full filename
		std::string filename = CFileSystemOperations::GetFileName(it->filename);
		std::string path = CFileSystemOperations::GetPath(it->filename);

		// paste file into directory structure
		// if doesn't exist, will be create
		m_directoryMap[path].push_back
			(CFileSystemOperations::SFileInfo(filename,it->filesize));
	}

	// build directory nodes
	BuildDirectoriesNodes();	
}

//=============================================================================
void CAddon::BuildDirectoriesNodes()
{
	for (CFileSystemOperations::TmapDirectory::iterator it=m_directoryMap.begin();
		it!=m_directoryMap.end();++it)
	{
		// get directories list to root
		CFileSystemOperations::TvecDirectories dirList = 
			CFileSystemOperations::GetDirectoriesToRoot(it->first);
		
		// FORCE make directory nodes
		// and link files
		MakeDirectoryNode(dirList);
	}
}

//=============================================================================
void CAddon::MakeDirectoryNode(const CFileSystemOperations::TvecDirectories &dirs)
{
	// go through directories list backward
	// sound,data,()
	// ()/data/sound
	CFileSystemOperations::SFileInfoNode* lastNode = m_root;
	for (CFileSystemOperations::TvecDirectories::const_reverse_iterator it=dirs.rbegin();
		it!=dirs.rend();++it)
	{
		// if current dir hasn't in home node
		// add it
		if (!lastNode->IsIn(*it))
		{
			// create new child
			CFileSystemOperations::SFileInfoNode* node = 
				new CFileSystemOperations::SFileInfoNode(*it,true);

			// set full path for
			// internal files map
			node->SetFullPath(lastNode);

			// store child into children list
			lastNode->children.push_back(node);

			// and switch last node
			lastNode = node;	
		}
		else
			lastNode = lastNode->GetChild(*it);		
	}
}

const CFileSystemOperations::TvecFiles &CAddon::GetFilesFromNode
		(const CFileSystemOperations::SFileInfoNode* node)
{
	Assert(node);

	// if node isn't null
	if (node)
	{
		// try to find key in internal files map
		CFileSystemOperations::TmapDirectory::iterator resFind = 
			m_directoryMap.find(node->fullpath);

		// if success return files list
		if (resFind!=m_directoryMap.end())
			return resFind->second;
	}

	// otherwise return empty list
	return m_directoryMap["NULL"];
}

//=============================================================================
const CAddon::TVecAddons &CAddon::GetInvalidDependecies()
{
	// if addon didn't checked dependencies
	// check its now and return all invalid 
	if (!m_dependecyCheck)
		CreateDependecies();

	return m_invalidDependencies;
}

//=============================================================================
const CAddon::TVecAddons &CAddon::GetValidDependecies()
{
	// if addon didn't checked dependencies
	// check its now and return all valid 
	if (!m_dependecyCheck)
		CreateDependecies();

	return m_validDependencies;
}


//=============================================================================
const CAddon::TVecAddons &CAddon::GetDependecies()
{
	// if addon didn't checked dependencies
	// check its now and return all(valid,invalid)
	if (!m_dependecyCheck)
		CreateDependecies();

	return m_dependencies;
}


//=============================================================================
void CAddon::GetAllDependecies(CAddon::TVecAddons &buffer)
{
	// make them
	MakeAllDependecies(buffer);

	// remove self
	CAddon::TVecAddons::iterator it = 
		find(buffer.begin(),buffer.end(),this);

	// if exist
	if (it != buffer.end())
		buffer.erase(it);
}

//=============================================================================
void CAddon::GetAllPBODependecies(CAddon::TVecAddons &buffer)
{
	// take normal dependencies
	GetAllDependecies(buffer);

	// get helpers
	CAddonManager* mgr = GetAddonManager();
	Assert(mgr);
	const CAddonManager::TMapAddons &addons = mgr->GetAddons();
	const std::string filename = GetFileName();
	
	// find all addons with same filename and get
	// their dependencies too
	for (CAddonManager::TMapAddons::const_iterator it=addons.begin();
		it!=addons.end();++it)
	{
		CAddon* addon = it->second;
			
			// if addon filename is same
			// link it's dependencies and it's name
			// to 'parent' addon
		if (addon != this && addon->GetFileName() == filename && !CAddon::IsIn(buffer,addon))
			{
				// stack overflow
				//addon->GetAllPBODependecies(buffer);
				
				addon->GetAllDependecies(buffer);
				buffer.push_back(addon);
			}
	}

	// remove self
	CAddon::TVecAddons::iterator it = 
		find(buffer.begin(),buffer.end(),this);

	// if exist
	if (it != buffer.end())
		buffer.erase(it);
}

//=============================================================================
void CAddon::MakeAllDependecies(CAddon::TVecAddons &dependencyList)
{	
	// make all dependencies

	// if addon is not valid then
	// it is not necessary do any computes
	if (!IsValid())
		return;

	// push_back self
	if (!IsIn(dependencyList,this))
		dependencyList.push_back(this);

	// go through entire valid dependency list
	for (CAddon::TVecAddons::iterator it=m_validDependencies.begin();
		it!=m_validDependencies.end(); ++it)
			if (!IsIn(dependencyList,*it))
			{
				// if dependency is has not already in list
				dependencyList.push_back(*it);

				// push all dependencies from dependency
				// and store all into list
				(*it)->MakeAllDependecies(dependencyList);
			}

	// go through entire invalid dependency list
	for (CAddon::TVecAddons::iterator it=m_invalidDependencies.begin();
		it!=m_invalidDependencies.end(); ++it)
			if (!IsIn(dependencyList,*it))
				dependencyList.push_back(*it);

}

//=============================================================================
const CAddon::TVecAddons &CAddon::CreateDependecies()
{
	// create and return dependencies

	// if dependencies has been checked so
	// we can return dependecies list
	if (m_dependecyCheck)
		return m_dependencies;

	// check null pointer in manager
	Assert(m_addonManager);
	if (!m_addonManager)
		return m_dependencies;

	// set dependecy check flag to true
	m_dependecyCheck = true;

	// go through dependencies names array
	// check every name in manager and store into list
	for (CAddon::TVecAddonsNames::iterator it=m_dependenciesNames.begin();
		it!=m_dependenciesNames.end();++it)
	{
		CAddon* addon = m_addonManager->GetAddon(*it);
		if (addon)
		{
			// addon exist
			m_dependencies.push_back(addon);
			m_validDependencies.push_back(addon);
		}
		else
		{
			// addon doesn't exist 

			// create empty addon by mgr and
			// store it into invalid dependencies list
			addon = m_addonManager->CreateEmptyAddon(*it);
			m_dependencies.push_back(addon);
			m_invalidDependencies.push_back(addon);
		}
	}

	// and return dependecies Objects
	return m_dependencies;
}

//=============================================================================
void CAddon::LoadDependecies(const ParamClass* source)
{
	// fill dependencies names
	for(int n = 0; n < source->GetEntryCount(); ++n)
	{
		// take next entry in object
		ParamEntryVal entr = source->GetEntry(n);

		// retype to class
		const ParamClass* ClassInterf = entr.GetClassInterface();

		// if is class, load dependencies
		if(entr.IsClass() && !ClassInterf->IsDelete())
				LoadDependecies(GetDependeciesArray(ClassInterf));	
	}
}

//=============================================================================
const std::string &CAddon::LoadAddonName(const ParamClass* source, bool dependeciesToo)
{
	// returns addon's name and loads dependencies

	for(int n = 0; n < source->GetEntryCount(); ++n)
	{
		//  take next entry in object
		ParamEntryVal entr = source->GetEntry(n);
		const ParamClass* ClassInterf = entr.GetClassInterface();

		// if is class, load dependencies
		if(entr.IsClass() && !ClassInterf->IsDelete())
		{			
			// get and set entry name
			m_addonName = entr.GetName().Data();

			// dependencies flag enable
			// load addon dependencies too
			if (dependeciesToo)
				LoadDependecies(GetDependeciesArray(ClassInterf));

			// and return addon name
			return m_addonName;
		}
	}

	// bad input data
	// maybe :)
	m_addonName = "NOTHING";
	return m_addonName;
}

//=============================================================================
ConstParamEntryPtr CAddon::GetDependeciesArray(const ParamClass* source) const
{
	// search source entry by entry
	for(int n = 0; n < source->GetEntryCount(); ++n)
	{
		ParamEntryVal entr = source->GetEntry(n);

		// and return it if exist
		if (entr.IsArray() 
			&& std::string(CAddon::ToLower(entr.GetName().Data())) 
				== CAddon::ToLower("requiredAddons"))
					return &entr;
	}
	
	return NULL;
}

//=============================================================================
bool CAddon::LoadAddon(const ParamClass* cl)
{
	// if addon class is invalid
	// we can't load adon and return false
	if (cl->IsError())
		return false;
	else
		LoadAddonName(cl,true);

	return true;
}

//=============================================================================
void CAddon::CleanUp()
{
	delete m_root;
}
