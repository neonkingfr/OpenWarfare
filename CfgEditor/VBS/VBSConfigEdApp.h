
#ifndef VBS_CONFIG_ED_APP_H
#define VBS_CONFIG_ED_APP_H

#include "wx/app.h"

class GenericEditor;
class MainFrame;
class VBSConfigEditor;

//=============================================================================
class VBSConfigEdApp : public wxApp
{
public:
	VBSConfigEdApp();
	inline VBSConfigEditor* GetEditor(){return editor;}
private:
    virtual bool	OnInit();
	virtual int		OnExit();
	int				MainLoop();
	VBSConfigEditor*	editor;
};

DECLARE_APP(VBSConfigEdApp)

#endif