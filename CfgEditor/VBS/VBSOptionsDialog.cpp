
#include "precomp.h"
#include "common/XmlConfig.h"
#include "wxExtension/FileTextBox.h"
#include "base/OptionsDialog.h"
#include "base/GenericEditor.h"

#include "VBSOptionsDialog.h"
#include "VBSMainFrame.h"
#include "VBSConfigEditor.h"


//===============================================================================
VBSOptionsDialogPanel::VBSOptionsDialogPanel(OptionsDialog* parent)
:OptionsDialogPanel(parent)
{
	extDirFileBox = new FileTextBox(this, FILEBOX_ROOT_DIR, wxDefaultPosition, wxSize(-1, 21), wxTE_PROCESS_ENTER, NULL, FTB_DIALOGTYPE_DIR);
	wxStaticText* extDirFileBoxLabel = new wxStaticText(this, -1, "External bin directory", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);

	baseCfgDirFileBox = new FileTextBox(this, FILEBOX_ROOT_DIR, wxDefaultPosition, wxSize(-1, 21), wxTE_PROCESS_ENTER, NULL, FTB_DIALOGTYPE_DIR);
	wxStaticText* baseCfgDirFileBoxLabel = new wxStaticText(this, -1, "Base configuration files directory(absolute path)", wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT);

	extDirFileBox->button->SetLabel("...");
	baseCfgDirFileBox->button->SetLabel("...");

	wxBoxSizer* MainSizer = new wxBoxSizer(wxVERTICAL);
	MainSizer->AddSpacer(10);
	MainSizer->Add(extDirFileBoxLabel, 0, wxALIGN_LEFT | wxLEFT, 10);
	MainSizer->Add(extDirFileBox, 0, wxGROW | wxALIGN_RIGHT | wxLEFT | wxRIGHT, 10);
	MainSizer->AddSpacer(10);
	MainSizer->Add(baseCfgDirFileBoxLabel, 0, wxALIGN_LEFT | wxLEFT, 10);
	MainSizer->Add(baseCfgDirFileBox, 0, wxGROW | wxALIGN_RIGHT | wxLEFT | wxRIGHT, 10);

	SetSizer(MainSizer);
}

//===============================================================================
void VBSOptionsDialogPanel::ValuesFromConfig()
{
	XMLConfig* config = g_editor->GetConfig();
	XMLConfigNode* node = config->GetNode("defaults", true);

	wxString StringValue;
	if(node->GetValue("ExtBinDir", StringValue))
		extDirFileBox->SetValue(StringValue);

	if(node->GetValue("BaseCfgsDir", StringValue))
	{
		StringValue.Replace("\\","/");
		baseCfgDirFileBox->SetValue(StringValue);
	}
}

//===============================================================================
void VBSOptionsDialogPanel::ValuesToConfig()
{
	XMLConfig* config = g_editor->GetConfig();
	XMLConfigNode* node = config->GetNode("defaults", true);
	wxString StringValue = extDirFileBox->GetValue();
	if (!static_cast<VBSConfigEditor*>(g_editor)->
		CheckExtAppExist(std::string(StringValue.c_str())))
			return;

	node->SetValue("ExtBinDir", StringValue);

	StringValue = baseCfgDirFileBox->GetValue();
	StringValue.Replace("\\","/");
	node->SetValue("BaseCfgsDir", StringValue);
}
