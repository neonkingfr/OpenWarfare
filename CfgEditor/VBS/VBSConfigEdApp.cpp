//#include "resource.h"

#include "precomp.h"
#include "VBSConfigEdApp.h"
#include "base/GenericEditor.h"
#include "base/MainFrame.h"
#include "anim/AnimEditor.h"
#include "anim/MainFrameAnm.h"

#include "VBSConfigEditor.h"

#include "wx/evtloop.h"
#include "wx/app.h"
#include "wx/msw/private.h"

#ifdef _DEBUG
//#define CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

IMPLEMENT_APP(VBSConfigEdApp)

//=============================================================================
class ConfigEdAppEventLoop : public wxEventLoop
{
	bool PreProcessMessage(WXMSG* msg)
	{
		if(wxGetApp().GetEditor())
		{
			if(wxGetApp().GetEditor()->PreProcessMessage(msg) == true)
				return true;	//without next pocessing
		}

		return wxEventLoop::PreProcessMessage(msg);
	}
};

//=============================================================================
int VBSConfigEdApp::MainLoop()
{
	wxEventLoop* myloop = new ConfigEdAppEventLoop;
	wxEventLoop* prevloop = m_mainLoop;
	m_mainLoop = myloop;

   int res = m_mainLoop->Run();
	m_mainLoop = prevloop;
	delete myloop;
	return res;
}

//=============================================================================
VBSConfigEdApp::VBSConfigEdApp()
{
	editor = NULL;
}

//=============================================================================
bool VBSConfigEdApp::OnInit()
{
#ifdef _DEBUG
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );	//zariadi ze na uplnom konci aplikacie sa samo zavola _CrtDumpMemoryLeaks();
#endif
//	_CrtSetBreakAlloc(10554);
	
	editor = new VBSConfigEditor();

	editor->SetExecFilename(wxApp::argv[0]);
	CResult InitResult = editor->OnAppInit();	//inicializacia editoru

	if(!InitResult.IsOk())
	{
		if(InitResult.IsCommented())
		{
			wxMessageDialog dialog(NULL, InitResult.GetComment(), "Init error", wxICON_ERROR | wxOK);
			dialog.ShowModal();
		}
		return false;
	}
	
	MainFrame* frame = editor->GetMainFrame();
	assert(frame);
	if (frame)
		frame->SetMinSize(wxSize(640,480));
	frame->Show(true);
	SetTopWindow(frame);

	// check command line args
	int count = wxApp::argc;
	if (argc == 2)
	{
		// argv[1] -- filename of pbo
		const char* filename = wxApp::argv[1];
		
		// control
		assert(editor);
		if (!editor)
			return false;

		// try to load file name
		editor->LoadOnInit(filename);
	}


	return true;
}

//=============================================================================
int VBSConfigEdApp::OnExit()	//nezvratny koniec aplikacie
{
	int res = wxApp::OnExit();	//zmaze sa veskere GUI, okna si ukladaju nastavenia a pod.

	editor->OnAppExit();			//editor musi zmazat vsetky alokacie uz teraz
	delete editor;
	editor = NULL;

#ifdef ENF_LEAK_DETECTOR
          MemoryManager::Dump("\n\nMemoryManager: Memory leaks report\n");
#endif
	return res;
}
