#pragma once
#include "base/OptionsDialog.h"

class wxButton;
class FileTextBox;
class OptionsDialog;


class VBSOptionsDialogPanel : public OptionsDialogPanel
{
public:
	VBSOptionsDialogPanel(OptionsDialog* parent);

protected:
	virtual void ValuesFromConfig();	//values from GUI need store to config
	virtual void ValuesToConfig();		//values from config need insert to gui

private:
	FileTextBox* extDirFileBox;
	FileTextBox* baseCfgDirFileBox;
};
