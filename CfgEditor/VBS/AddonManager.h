#pragma once
#include <algorithm>
#include <iterator>
#include <vector>
#include <set>
#include <map>
#include <string>
#include "Addon.h"
#include "FileSystemOperations.h"

//=============================================================================

/// CAddonManager class
class CAddonManager
{
public:
	typedef std::map<std::string, CAddon*> TMapAddons;
	typedef std::vector<std::string> TVecAddonsNames;
	typedef std::set<std::string> TSetFiles;

	/// Type of call back function for update build progress
	typedef bool (*TProgressCallback)(float);

	enum ELoadMode
	{
		lmNormal = 0,
		lmAdd,
		lmErase,
		lmLoad
	};

	CAddonManager(void){}
	~CAddonManager(void){ CleanUp(); }

	/** Load addons from specified path
		Return num of loaded addons 

		\param path path to addons directory
		\param progressFunc callback to wxProgressbar
		\param mode Load mode
	*/
	int LoadAddons(const std::string &path, TProgressCallback progressFunc = NULL, 
		ELoadMode mode=lmNormal);

	/// Load addon from file.
	CAddon* LoadAddon(const std::string &filename);

	/** Load addons from directories set
		Return num of loaded addons 

		\param paths set of addons directories
		\param progressFunc callback to wxProgressbar
		\param mode Load mode
	*/
	int LoadAddons(const TSetFiles &paths, TProgressCallback progressFunc = NULL);

	/** Add addons from directory into existing addons structures.

		\param path path to addons directory
		\param progressFunc callback to wxProgressbar		
	*/
	void AddAddons(const std::string &path, TProgressCallback progressFunc = NULL); 

	/** Remove addons from directory from existing addons structures.

		\param path path to addons directory
		\param progressFunc callback to wxProgressbar		
	*/
	void RemoveAddons(const std::string &path, TProgressCallback progressFunc = NULL); 

	/** Find addon by name
		and return it

		\param addonName name of addon which function 
			is looking for
	*/
	CAddon* GetAddon(const std::string &addonName)
	{
		// return addon's name if exist
		// or null if doesn't
		TMapAddons::iterator it = m_AddonsMap.find(CAddon::ToLower(addonName));
		if (it != m_AddonsMap.end())
			return it->second;
		else
			return NULL;
	}
	
	/** Insert existing addon into mgr.
		Addon will be store in m_AddonsMap and
		m_ValidAddonsMap in CAddonManager internal 
		structures

		\param what Contains information about inserting addon
	*/
	void InsertValidAddon(const std::pair<std::string,CAddon*> &what)
	{
		m_AddonsMap.insert(what);
		m_ValidAddonsMap.insert(what);
	}

	/// Returns true, if addons manager has no addons in map
	bool IsEmpty() const { return m_AddonsMap.empty(); }

	/// Return addons map (valid/invalid)
	const TMapAddons &GetAddons() const { return m_AddonsMap; }

	/// Return valid addons map
	const TMapAddons &GetValidAddons() const { return m_ValidAddonsMap; }

	/// Return invalid addons map
	const TMapAddons &GetInvalidAddons() const { return m_InvalidAddonsMap; }

	/// create empty addon and store it
	CAddon* CreateEmptyAddon(const std::string &addonName);

	/** Add additionally dependencies into addon.

		Without duplicity.

		\param addon parent addon
		\param configFile Contains dependencies array
	*/
	static void CAddonManager::AddAddonDependencies
		(CAddon* addon, const std::string &configFile);

	/** Set <i>no root</i> addons internal files structure.
		Without any computation.
	*/
	void SetAddonsInternalFiles();

	/** Check if addon is in manager

		\param addon checking addon
	*/
	bool IsIn(const CAddon* addon);

	void GetAllPBOs(TSetFiles &into);
private:
	/// Addons map
	TMapAddons m_AddonsMap;

	/// Invalid addons map
	TMapAddons m_InvalidAddonsMap;

	/// Valid addons map
	TMapAddons m_ValidAddonsMap;

	/// Addons path
	TSetFiles m_Paths;

	/** Make full name from filename and path 
	
		and erase file extension.
			
		\param path The path of file directory
		\param filename name of file(extension included)
		\param extSize contains extension length
	*/ 
	static std::string GetFullNameFromFile
		(const std::string &path,const std::string &filename,std::string::size_type extSize);

	/// Build addons dependecies(down direction)
	void BuildAddonsDependecies();

	/// Remove addons from maps and clean up memory
	void CleanUp();

	/** Return valid addon with same filename.
		And with filled internal files structure

		\param addon <i>'Parent'</i> addon
	*/
	const CAddon* GetAddonWithSamePBO(const CAddon* addon) const;

	/** Erase addon from mgr and memory.

		\param what Erasing addon
	*/
	void EraseAddonFromMgr(CAddon* what);

	/** Save addon into manager.
		And set additional addon information
		like internal addon's files and build
		addon's dependencies tree.

		\param ClassInterf Root ParamFile entry for additional information
		\param path Current addon's directory path
		\param fileinfo Information about addon's file(filename,size)
		\param addon Handle to CAddon object
		\param index To QFBank
		\param bank QFBank reference
	*/
	void StoreAddon(const ParamClass* ClassInterf, const std::string &path, 
					const CFileSystemOperations::SFileInfo &fileinfo, CAddon* addon, 
					int index, QFBank& bank);

	/// PROBABLY OBSOLETE
	bool CheckAddonDuplicity(const std::string &addonName, CAddon* addon);

	/** Parse addon PFile and return root ParamFile class.
		Try to open bank and parse config.cpp or config.bin.

		\param fullname
		\param bank
		\param PFile
	*/
	const ParamClass* GetAddonRootClass(const std::string &fullname, 
		QFBank &bank, ParamFile &PFile);

	/** Set load mode and prepare workspace.
		e. g. clear m_Paths, insert new path,...

		\param mode Current load mode
		\param addonsPath Current working path
	*/
	void CAddonManager::SwitchLoadMode(ELoadMode mode, 
		const std::string &addonsPath);

	/// Return count of files from all addons paths
	int CAddonManager::GetTotalFiles();
 };
