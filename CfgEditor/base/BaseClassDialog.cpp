#include "precomp.h"
#include "BaseClassDialog.h"
#include "ParamConfig.h"

//-------------------------------------------------------------
BaseClassDialog::BaseClassDialog(wxWindow* parent, PCClass* SelectedClass)
	:wxDialog(parent, -1, "Set base class", wxDefaultPosition, wxSize(320, 160), wxCAPTION|wxTHICK_FRAME|wxSYSTEM_MENU|wxCLOSE_BOX | wxTAB_TRAVERSAL|wxNO_BORDER)
{
	wxArrayString choices;
	EArray<PCClass*> PossibleBases;

	SelectedClass->GetPossibleBaseClasses(PossibleBases);
	
	for(uint n = 0; n < PossibleBases.GetCardinality(); n++)
	{
		PCClass* cl = PossibleBases[n];
		choices.Add(cl->GetName());
	}

	wxStaticText* BaseLabel = new wxStaticText(this, wxID_ANY, "base class", wxDefaultPosition, wxSize(-1, -1), wxST_NO_AUTORESIZE);
	BaseNameEditBox = new wxComboBox(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(220, 20), choices, 0);
	wxButton* OkButton = new wxButton(this, wxID_OK, "Ok", wxDefaultPosition, wxSize(80, 25));
	wxButton* CancelButton = new wxButton(this, wxID_CANCEL, "Cancel", wxDefaultPosition, wxSize(80, 25));

	wxBoxSizer* TopSizer = new wxBoxSizer(wxVERTICAL);
	TopSizer->AddSpacer(20);
	TopSizer->Add(BaseLabel, 0, wxALIGN_LEFT | wxBOTTOM, 0);
	TopSizer->AddSpacer(2);
	TopSizer->Add(BaseNameEditBox, 0, wxGROW | wxALIGN_LEFT | wxBOTTOM, 0);

	wxBoxSizer* ButtonsSizer = new wxBoxSizer(wxHORIZONTAL);
	ButtonsSizer->Add(OkButton, 0, wxALIGN_LEFT | wxALL, 0);
	ButtonsSizer->AddStretchSpacer();
	ButtonsSizer->Add(CancelButton, 0, wxALIGN_LEFT | wxALL, 0);

	wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
	sizer->Add(TopSizer, 1, wxGROW | wxALIGN_LEFT | wxALL, 10);
	sizer->Add(ButtonsSizer, 0, wxGROW | wxALIGN_LEFT | wxALL, 10);
	SetSizer(sizer);
}

//-------------------------------------------------------------
void BaseClassDialog::SetBaseName(const wxString& name)
{
	BaseNameEditBox->SetValue(name);
}

//-------------------------------------------------------------
wxString BaseClassDialog::GetBaseName()
{
	return BaseNameEditBox->GetValue();
}
