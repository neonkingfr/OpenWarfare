
#include "precomp.h"
#include "IDSet.h"

//===============================================================================
PCPath::PCPath()
{
	pos = 0;
}

//-------------------------------------------------------------------------------
PCPath::PCPath(const char* path)
{
	assert(path);
	this->path = path;
	pos = this->path.length();
}

//-------------------------------------------------------------------------------
const char* PCPath::AddLevel(const char* name)
{
#ifdef _DEBUG
	assert(name);
	int len = strlen(name);
	assert(len > 0);	
	const char* ptr = name;

	for(int n = 0; n < len; n++, ptr++)
	{
		if(*ptr == '/')
		{
			assert(false);
			return path.c_str();
		}
	}
#endif

	if(path.length() > 0)
		path += '/';

	path += name;
	pos = path.length();
	return path.c_str();
}

//-------------------------------------------------------------------------------
DString PCPath::GetFirstLevel() const
{
	pos = 0;
	const char* start = path.c_str() + pos;
	const char* end = start;

	while(*end != 0 && *end != '/')
		end++;

	int lng = end - start;
	pos += lng;

	if(*end == '/')
		pos++;

	return DString(start, lng);
}

//-------------------------------------------------------------------------------
DString PCPath::GetNextLevel() const
{
	const char* start = path.c_str() + pos;
	const char* end = start;

	while(*end != 0 && *end != '/')
		end++;

	int lng = end - start;
	pos += lng;

	if(*end == '/')
		pos++;

	return DString(start, lng);
}

//-------------------------------------------------------------------------------
const char* PCPath::GetLastLevel() const
{
	int len = path.length();

	if(len == 0)
		return "";

	const char* start = path.c_str();
	const char* end = start + (len - 1);

	while(end > start && *end != '/')
		end--;

	if(*end == '/')
		end++;

	return end;
}

//-------------------------------------------------------------------------------
const char* PCPath::GetPath() const
{
	return path.c_str();
}

//-------------------------------------------------------------------------------
bool PCPath::operator> (const PCPath& p)
{
	return (strcmp(GetPath(), p.GetPath()) > 0);
}

//-------------------------------------------------------------------------------
bool PCPath::operator< (const PCPath& p)
{
	return (strcmp(GetPath(), p.GetPath()) < 0);
}

//-------------------------------------------------------------------------------
bool PCPath::operator== (const PCPath& p)
{
	return (strcmp(GetPath(), p.GetPath()) == 0);
}


//===============================================================================
IDSet::IDSet()
{
}

//---------------------------------------------------------------------------
IDSet::IDSet(const IDSet& set)
{
	Copy(set);
}

//---------------------------------------------------------------------------
uint IDSet::GetCount() const
{
	return IDs.GetCardinality();
}

//---------------------------------------------------------------------------
uint IDSet::Add(const PCPath& id)
{
	return IDs.Insert(id);
}

//---------------------------------------------------------------------------
bool IDSet::Remove(const PCPath& id)
{
	return IDs.Remove(id);
}

//---------------------------------------------------------------------------
bool IDSet::Contains(const PCPath& id) const
{
	return (IDs.GetIndexOf(id) != INDEX_NOT_FOUND);
}

//---------------------------------------------------------------------------
void IDSet::Clear()
{
	IDs.Clear();
}

//---------------------------------------------------------------------------
void IDSet::operator=(const IDSet& set)
{
	Copy(set);
}

//---------------------------------------------------------------------------
const PCPath& IDSet::operator[](uint index) const
{
	return IDs[index];
}

//---------------------------------------------------------------------------
void IDSet::Copy(const IDSet& set)
{
	Clear();

	for(uint n = 0; n < set.GetCount(); n++)
	{
		IDs.Insert(set[n]);
	}
}
