
#ifndef PARAM_CONFIG_DEF_H
#define PARAM_CONFIG_DEF_H

#include "common/CResult.h"

class PCDefParam;
class PCDefClass;
class ParamConfigDef;
class TiXmlDocument;
class	TiXmlElement;
class TiXmlNode;
//class FileSystem;

//===============================================================================
// ParamConfigVariableType
//===============================================================================
enum PCVarType
{
	PCVT_UNKNOWN = 0,
	PCVT_BOOL,
	PCVT_INT,
	PCVT_FLOAT,
	PCVT_STRING,
};

PCVarType String2VarType(const char* str);

//===============================================================================
// any param config definition node. base class for all other
//===============================================================================
class PCDefNode
{
	friend class ParamConfigDef;
public:
	PCDefNode();
	PCDefNode(TiXmlNode* node);
	virtual TiXmlNode* GetNode() const;
	PCDefNode* GetFirstChild() const;
	PCDefNode* GetNextSiblingNode() const;
	PCDefNode* GetParentNode() const;
	virtual ~PCDefNode(){};
	virtual inline PCDefParam* ToParam() {return NULL;}
	virtual inline PCDefClass* ToClass() {return NULL;}
	virtual inline const PCDefParam* ToParamConst() const {return NULL;}
	virtual inline const PCDefClass* ToClassConst() const {return NULL;}
	inline bool IsGenerated() const {return generated;}
	void			SetGenerated(bool generated);
private:
	bool generated;
protected:
	TiXmlNode* node;
	TiXmlElement* GetElement() const;
};

//===============================================================================
// single param node definition. with param means variable or array with class owner
// this is definition for classes PCVar and PCArray
//===============================================================================
class PCDefParam : public PCDefNode
{
public:
	PCDefParam(TiXmlElement* element);
	virtual inline PCDefParam* ToParam() {return this;}
	virtual inline const PCDefParam* ToParamConst() const {return this;}
/*	const char* GetReadOnly() const;
	const char* GetHidden() const;
	const char* GetDefaultValue() const;
	const char* GetMinValue() const;
	const char* GetMaxValue() const;
	const char* GetGuiType() const;*/
	const char* GetAttr(const char* name) const;
//	const char* GetDescription() const;
	const TiXmlElement* GetValuesElement()const;
	void SetVarType(PCVarType type);
	void SetEditControl(const char* control);
	void SetReadOnly(bool readonly);
	void SetHidden(bool hidden);
	ENF_DECLARE_MMOBJECT(PCDefParam);
private:
};

//===============================================================================
// class node definition. this is definition for class PCClass
//===============================================================================
class PCDefClass : public PCDefNode
{
public:
												PCDefClass(TiXmlElement* element);
	const PCDefParam*						GetParamDef(const char* name, bool create = true) const;
	virtual inline PCDefClass*			ToClass() {return this;}
	virtual inline const PCDefClass* ToClassConst() const {return this;}
	virtual void							Clear();
	bool										IsHidden() const;
	bool										IsReadOnly() const;
	void										SetHidden(bool hidden);
	void										SetReadOnly(bool readonly);
	ENF_DECLARE_MMOBJECT(PCDefClass);
protected:
	bool DoesNameMatch(const wxString &text, const wxString &regEx) const;
	void LoadWildParams();
	EArray<const TiXmlElement*> wildParams;
};

//===============================================================================
// definition of class ParamConfig
//===============================================================================
class ParamConfigDef : public PCDefClass
{
public:
								ParamConfigDef();
								~ParamConfigDef();
	virtual CResult		Load(const char* FileName, bool* CreatedDeafultFile);
	virtual CResult		Save(const char* FileName);
	const PCDefClass*		GetClassDef(const char* ClassPath) const;
	const PCDefParam*		GetGlobalParamDef(const char* name) const;
	virtual TiXmlNode*	GetNode() const;
	virtual void			Clear();
	void						Destroy();
	ENF_DECLARE_MMOBJECT(ParamConfigDef);
protected:
	void								LoadChilds(TiXmlElement* parent);
	TiXmlElement*					GetChildClassDefElement(TiXmlNode* parent, const char* ClassName) const;
	TiXmlDocument*					doc;
	TiXmlElement*					GlobalParamsElm;
	void								DeleteGlobalyDefinedParams();
	void								InsertDefaultRootToDocument();
	EArray<TiXmlElement*>			wildGlobals;
};

TiXmlElement*					GetChildElement(TiXmlNode* parent, const char* name);
extern ParamConfigDef* g_PConfigDef;

#endif