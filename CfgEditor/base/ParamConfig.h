
#ifndef PARAM_CONFIG_H
#define PARAM_CONFIG_H

#include "ParamConfigDef.h"
#include "PCFilename.h"
#include "base/IDSet.h"
#include "EL/ParamFile/ParamFile.hpp"

class PCVar;
class PCArray;
class TiXmlDocument;
class PCClass;
class ParamClass;	//BIS
class ParamConfig;
class PCCommand;
class TxtFile;

enum UniIdPrefix
{
	UID_VARIABLE,
	UID_ARRAY,
};

extern ParamConfig* GetConfigPtr();

//===============================================================================
// ParamConfigNode base class for PCVar, PCArray and PCClass
//===============================================================================
class PCNode
{
public:
										PCNode(const char* name, PCNode* owner, const PCDefNode* definition);
	virtual							~PCNode();
	virtual inline PCClass*		ToClass()  {return NULL;}
	virtual inline PCVar*		ToVariable()  {return NULL;}
	virtual inline PCArray*		ToArray()  {return NULL;}
	virtual inline ParamConfig* ToConfig()  {return NULL;}
	virtual inline PCCommand*	ToCommand()  {return NULL;}
	const char*						GetName() const;
	virtual inline PCNode*		GetOwner() const {return owner;}
	virtual const PCDefNode*	GetDefinition() const;
	virtual PCNode*				Clone(PCNode* owner) const {return NULL;}
	virtual uint					GetPosInOwner(bool SkipOwnerClasses = false) const;
	PCClass*							GetFirstClassOwner() const;
	DString							GetLocation();
	DString							GetId() const;
	PCNode*							GetTopOwner();
	ParamConfig*					GetConfig();
	virtual void					Clear(){};
	bool								IsInsideArray() const;
	virtual bool					IsHidden() const =0;
	virtual bool					IsReadOnly() const =0;
	virtual bool					IsDefinedAsReadOnly() const =0;
	void								SetDefinition(const PCDefNode* def);
	void								SetExtern(bool IsExtern);
	void								SetExternOnLoad(bool IsExtern);
	void								SetName(const char* name);
	inline bool						IsExtern() const {return IsExternNode;}
	inline bool						WasExternOnLoad() const {return WasExternNodeOnLoad;}
protected:
	const PCDefNode*				definition;
	PCNode*							owner;	//owner moze byt class alebo pole
	bool								IsExternNode;
	bool								WasExternNodeOnLoad;
private:
	DString name;
};

//===============================================================================
//ParamConfigVariable node of ParamConfig class
// variable can be contained in PCClass or PCArray
//===============================================================================
class PCVar : public PCNode
{
public:
										PCVar(const char* name, PCNode* owner, const PCDefParam* definition = NULL);
	virtual							~PCVar();
	bool								IsEqual(PCVar* other);
	virtual inline PCVar*		ToVariable() {return this;}
	const char*						GetValue() const;
	void								SetValue(const char* value);
	virtual const PCDefParam*	GetDefinition() const;
	virtual bool					IsHidden() const;
	virtual bool					IsReadOnly() const;
	virtual bool					IsDefinedAsReadOnly() const;
	virtual PCVar*					Clone(PCNode* owner) const;
	virtual void					Clear(){};
										ENF_DECLARE_MMOBJECT(PCVar);
private:
	DString		value;
};

//===============================================================================
//ParamConfigArray node from ParamConfig class
//array can be contained in PCClass or another PCArray
//PCArray members can be PCVar or another PCArray
//===============================================================================
class PCArray : public PCNode
{
public:
										PCArray(const char* name, PCNode* owner, const PCDefParam* definition = NULL);
	virtual							~PCArray();
	virtual bool					IsEqual(PCArray* other);
	virtual inline PCArray*		ToArray()  {return this;}
	bool								Add(PCNode* value);
	bool								Insert(PCNode* value, uint pos);
	bool								Remove(PCNode* value);
	uint								GetCardinality() const;
	PCNode*							GetItem(uint n) const;
	PCNode*							GetMember(const char* name) const;
	PCVar*							GetVariableMemberFromValue(const char* value, uint* index);
	uint								GetIndexOf(const PCNode* member);
	virtual const PCDefParam*	GetDefinition() const;	//nemusi mat definition ked sa jedna o pole ako prvok ineho pola
	virtual bool					IsHidden() const;
	virtual bool					IsReadOnly() const;
	virtual bool					IsDefinedAsReadOnly() const;
	virtual PCArray*				Clone(PCNode* owner) const;
	virtual void					Clear();
	bool								CanSaveInline() const;
										ENF_DECLARE_MMOBJECT(PCArray);
protected:
	EArray<PCNode*>	values;
};

//===============================================================================
//ParamConfigCommand node from ParamConfig. in class can found special commands. at this time only "delete" command
//owner can be only PCClass and PCCommand is not real member of class. by iterating over PCClass members is invisible
//===============================================================================
class PCCommand : public PCNode
{
public:
	PCCommand(const char* name, PCNode* owner);
	virtual							~PCCommand();
	virtual inline PCCommand*	ToCommand()  {return this;}
	const char*						GetValue() const;
	void								SetValue(const char* value);
	virtual bool					IsHidden() const {return true;}
	virtual bool					IsReadOnly() const {return true;}
	virtual bool					IsDefinedAsReadOnly() const;
	virtual PCCommand*			Clone(PCNode* owner) const;
	virtual void					Clear(){};
										ENF_DECLARE_MMOBJECT(PCCommand);
private:
	DString		value;
};

//===============================================================================
//ParamConfigClass node from ParamConfig
//PCClass members can be PCVar, PCArray or another PCClass
//===============================================================================
class PCClass : public PCNode
{
	friend class ParamConfig;
public:
										PCClass(const char* name, PCNode* owner, PCClass* base, bool defined, const PCDefClass* definition = NULL);
	virtual							~PCClass();
	virtual inline PCClass*		ToClass() {return this;}
	virtual PCClass*				GetOwner() const;
	inline PCClass*				GetBase() const {return base;}
	PCClass*							GetTopBase() const;
	int								GetNumBases() const;
	int								GetAbsorptionLevel() const;
	bool								IsChildOf(PCClass* cl, bool recursive) const;
	bool								IsDeleted() const;
	inline bool						IsUnused() const {return unused;};
	inline bool						IsDefined() const {return defined;}
	virtual bool					IsHidden() const;
	virtual bool					IsReadOnly() const;
	virtual bool					IsDefinedAsReadOnly() const;
	virtual const PCDefClass*	GetDefinition() const;
	bool								AppendNode(PCNode* node);
	bool								Insert(PCNode* node, uint pos);
	bool								RemoveNode(PCNode* node);
	uint								GetNodesCount() const;
	PCNode*							GetNode(uint n) const;
	uint								GetIndexOf(const PCNode* member);
	PCNode*							GetMember(const char* name) const;
	PCVar*							GetVarMember(const char* name) const;
	PCArray*							GetArrayMember(const char* name) const;
	PCClass*							GetClassMember(const char* name) const;
	void								GetClassMembers(ESet<PCClass*>& target, bool recursive);
	PCNode*							GetValueNode(const char* name, PCClass* LastBase = NULL) const;	//find variable or array in members and if not found, find continues recursively to base class
	uint								GetPosInOwner(bool SkipOwnerClasses = false) const;
	bool								AppendCommand(PCCommand* command);
	bool								DeleteCommand(const char* name, const char* value);
	uint								GetCommandsCount() const;
	PCCommand*						GetCommand(uint n) const;
	PCCommand*						FindCommand(const char* name, const char* value);
	uint								GetCommandIndex(PCCommand* command);
	void								SortCommands(bool recursive);
	PCClass*							FindClass(const char* name, bool InBase, bool InOwner) const;	//find class function as clone of orginal BIS ParamFile
	void								GetPossibleBaseClasses(EArray<PCClass*>& result) const;
	virtual PCClass*				Clone(PCNode* owner) const;	//create a complete clone of all content. recursive
	virtual PCClass*				CreateSameClass(const char* name, PCNode* owner, PCClass* base, bool defined, const PCDefClass* definition = NULL) const;				//create new emtpty class instance. not recursive
	virtual void					Clear();
	virtual bool					IsEmpty() const;
	virtual void					AfterRename(const char* OldName){};
	inline const DString&		GetBaseID() const {return BaseID;}
	void								SetBaseID(const char* BaseID);
	void								SetBase(PCClass* base);
	uint								GetAllInheritClasses(PCClass* base, EArray<PCClass*>& target, bool ExternClasses);
	void								GetDirectlyInheritedClasses(PCClass* base, EArray<PCClass*>& target) const;
	int								IsInheritedFrom(PCClass* cl);
	int								IsInheritedFrom(const char* ClassName);
	const TiXmlElement*			GetPropertyValuesDef(const char* name);
	const char*						GetPropertyDefAttr(const char* name, const char* attribute);
	virtual							void LoadEditData(TiXmlElement* elm){};	//abstract
	void								SetUnused(bool unused);
	bool								SetDeleted(bool deleted);
	void								SetDefined(bool defined);
	uint								RemoveSameMembersIfAreInherited();
	int								MarkFlags;	//internal only for custom marking.
										ENF_DECLARE_MMOBJECT(PCClass);
protected:
//	void								StroreBasePtrToString();
	PCClass*					base;
	DString					BaseID;		//becuse of some problems must be stored BaseID
	EArray<PCNode*>		nodes;		//all members exclude commands
	EArray<PCCommand*>	commands;	//commands (delete)
	bool						defined;		//if false, class is not real class with body but only forward declaration
	bool						unused;		//true = is extern and not inherited by any not extern
};

//===============================================================================
// orginal BIS ParamFile class clone. for loading from file uses orginal ParamFile from BIS
//===============================================================================
class ParamConfig : public PCClass
{
public:
											ParamConfig();
	virtual								~ParamConfig();
	CResult								Load(EArray<PCFilename>& FileNames);	//load using BIS ParamFile. optional dependencies stores referencies to all external configs required to preload
	CResult								Save(const char* FileName);	//save to BIS ParamFile format
	virtual void						Clear();
	virtual bool						IsEmpty() const;
	virtual bool						IsReadOnly() const;
	virtual inline ParamConfig*	ToConfig()  {return this;}
	virtual ParamConfigDef*			GetDefinition() const;
	const DString&						GetFileName();
	virtual DString					GetSaveFilename();
	void									SetFileName(const char* FileName);
//	virtual const char*				GetWorkFileExtension();
	PCNode*								FindNode(const char* id);
	PCClass*								FindClassNode(const char* id);
	DString								GenerateUniqueName(UniIdPrefix prefix);
	void									UpdateClassBasePtr(PCClass* cl, bool recursive);
	void									UpdateClassBaseID(PCClass* cl, bool recursive);
	virtual int							GetClassInfoFlags(PCClass* owner, PCClass* base){return 0;}
	virtual PCClass*					CreateClass(const char* name, PCNode* owner, const char* OwnerID, PCClass* base, const char* BaseID, bool defined, const PCDefClass* definition, bool FromLoading, int ClassInfoFlags);
	virtual PCArray*					CreateArray(const char* name, PCNode* owner, const PCDefParam* definition = NULL);
	virtual PCVar*						CreateVariable(const char* name, PCNode* owner, const PCDefParam* definition = NULL);
	virtual PCCommand*				CreateCommand(const char* name, PCNode* owner);
											ENF_DECLARE_MMOBJECT(ParamConfig);
protected:
	PCClass*								LoadClass(const ParamClass* source, bool IsRoot, PCClass* owner, PCClass* base, const PCDefClass* definition, bool SourceIsUpdated);
	PCArray*								LoadArray(const IParamArrayValue& entr, const char* name, PCNode* owner, PCDefParam* definition);
	PCVar*								LoadVariable(const char* name, const char* value, PCVarType type, PCNode* owner, PCDefParam* definition);
	void									SaveClass(const PCClass* cl, TxtFile* file, int NumTabs);
	void									SaveArray(const PCArray* arr, TxtFile* file, int NumTabs, int InOwnerArray);
	void									SaveVariable(const PCVar* var, DString& line);
	PCVarType							GetEntryType(const ParamEntryVal& entr);
	PCVarType							GetArrayValueType(const IParamArrayValue& val);
	ParamConfigDef*					ConfigDef;
	uint									UniqueNameCounter;
	DString								FileName;
private:
	CResult								LoadSimpleConfig(ParamFile* PFile, const char* filename);
};

#endif