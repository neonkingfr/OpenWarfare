
#include "precomp.h"
//#include "ParamConfig.h"
#include "ParamConfigDef.h"
#include "base/IDSet.h"
#include "common/WinApiUtil.h"
#include "tinyxml/tinyxml.h"

ParamConfigDef* g_PConfigDef = NULL;

//--------------------------------------------------------------------------------
PCVarType String2VarType(const char* str)
{
	if(!str)
		return PCVT_UNKNOWN;

	if(strcmpi(str, "bool") == 0)
		return PCVT_BOOL;
	else if(strcmpi(str, "int") == 0)
		return PCVT_INT;
	else if(strcmpi(str, "float") == 0)
		return PCVT_FLOAT;
	else if(strcmpi(str, "string") == 0)
		return PCVT_STRING;

	return PCVT_UNKNOWN;
}

//===============================================================================
PCDefNode::PCDefNode(TiXmlNode* node)
{
//	enf_assert(node);
	this->node = node;
	generated = false;
}

//-------------------------------------------------------------------------------
PCDefNode::PCDefNode()
{
	node = NULL;
	generated = false;
}

//-------------------------------------------------------------------------------
void PCDefNode::SetGenerated(bool generated)
{
	this->generated = generated;
}

//-----------------------------------------------------------------------------------------
TiXmlNode* PCDefNode::GetNode() const
{
	assert(node);
	return node;
}

//-------------------------------------------------------------------------------
TiXmlElement* PCDefNode::GetElement() const
{
	enf_assert(node);
	return node->ToElement();
}

//-------------------------------------------------------------------------------
PCDefNode* PCDefNode::GetFirstChild() const
{
	assert(node);
	const TiXmlNode* n = node->FirstChild();

	if(!n)
		return NULL;

	PCDefNode* FirstChild = (PCDefNode*)n->GetUserData();
	assert(FirstChild);
	return FirstChild;
}

//-------------------------------------------------------------------------------
PCDefNode* PCDefNode::GetNextSiblingNode() const
{
	assert(node);
	TiXmlNode* n = node->NextSibling();

	if(!n)
		return NULL;

	PCDefNode* NextSibling = (PCDefNode*)n->GetUserData();
	assert(NextSibling);
	return NextSibling;
}

//-------------------------------------------------------------------------------
PCDefNode* PCDefNode::GetParentNode() const
{
	assert(node);
	const TiXmlNode* parent = node->Parent();

	if(!parent)
		return NULL;

	PCDefNode* ParentNode = (PCDefNode*)parent->GetUserData();
	assert(ParentNode);
	return ParentNode;
}

//===============================================================================
PCDefParam::PCDefParam(TiXmlElement* element)
	:PCDefNode(element)
{
}
/*
//-------------------------------------------------------------------------------
const char* PCDefParam::GetReadOnly() const
{
	assert(GetElement());
	return GetElement()->Attribute("readonly");
}

//-------------------------------------------------------------------------------
const char* PCDefParam::GetHidden() const
{
	assert(GetElement());
	return GetElement()->Attribute("hidden");
}
*/
//-------------------------------------------------------------------------------
void PCDefParam::SetVarType(PCVarType VarType)
{
	assert(GetElement());
	const char* attr = NULL;

	switch(VarType)
	{
	case PCVT_UNKNOWN:
		attr = NULL;
		break;
	case PCVT_BOOL:
		attr = "bool";
		break;
	case PCVT_INT:
		attr = "int";
		break;
	case PCVT_FLOAT:
		attr = "float";
		break;
	case PCVT_STRING:
		attr = "string";
		break;
	default:
		assert(false);
	}

	if(attr)
		GetElement()->SetAttribute("type", attr);
}

//-------------------------------------------------------------------------------
void PCDefParam::SetReadOnly(bool readonly)
{
	assert(GetElement());
	if(readonly)
		GetElement()->SetAttribute("readonly", "true");
	else
		GetElement()->SetAttribute("readonly", "false");
}

//-------------------------------------------------------------------------------
void PCDefParam::SetHidden(bool hidden)
{
	assert(GetElement());
	if(hidden)
		GetElement()->SetAttribute("hidden", "true");
	else
		GetElement()->SetAttribute("hidden", "false");
}

/*
//-------------------------------------------------------------------------------
const char* PCDefParam::GetDefaultValue() const
{
	assert(GetElement());
	const char* val = GetElement()->Attribute("default");
	return val ? val : "";
}

//-------------------------------------------------------------------------------
const char* PCDefParam::GetMinValue() const
{
	assert(GetElement());
	return GetElement()->Attribute("minvalue");
}

//-------------------------------------------------------------------------------
const char* PCDefParam::GetMaxValue() const
{
	assert(GetElement());
	return GetElement()->Attribute("maxvalue");
}

//-------------------------------------------------------------------------------
const char* PCDefParam::GetGuiType() const
{
	assert(GetElement());
	return GetElement()->Attribute("gui");
}
*/
//-------------------------------------------------------------------------------
const char* PCDefParam::GetAttr(const char* name) const
{
	enf_assert(name);
	assert(GetElement());
	return GetElement()->Attribute(name);
}
/*
//-------------------------------------------------------------------------------
const char* PCDefParam::GetDescription() const
{
	assert(GetElement());
	return GetElement()->Attribute("desc");
}
*/
//-------------------------------------------------------------------------------
const TiXmlElement* PCDefParam::GetValuesElement() const
{
	TiXmlElement* elm = GetElement();
	assert(elm);
	const char* valdasds = elm->Value();
	const char* name = elm->Attribute("name");
	elm = GetChildElement(elm, "values");
	return elm;
}

//-------------------------------------------------------------------------------
void PCDefParam::SetEditControl(const char* control)
{
	assert(control);
	assert(GetElement());
	GetElement()->SetAttribute("gui", control);
}

//-------------------------------------------------------------------------------
int CompareElements(const void* a, const void* b)
{
	const TiXmlElement** elemA = (const TiXmlElement**)(a);
	const TiXmlElement** elemB = (const TiXmlElement**)(b);
	const char* strA = (*elemA)->Attribute("name");
	const char* strB = (*elemB)->Attribute("name");

	int lenA = strlen(strA);// length of the string
	int lenB = strlen(strB);
	int cCountA = 0;// number of characters in the string, except star characters (*)
	int cCountB = 0;
	int sCountA = 0;// number of star characters in the string
	int sCountB = 0;

	// count all star and other characters.
	for(int i=0;i<lenA;++i)
		if(strA[i] == '*')
			++sCountA;
		else
			++cCountA;

	// same for the other string
	for(int i=0;i<lenB;++i)
		if(strB[i] == '*')
			++sCountB;
		else
			++cCountB;

	// if number of ordinary characters are the same, result depends on difference in star count
	if(cCountA == cCountB)
		return sCountA - sCountB;

	return cCountB - cCountA;
}

//===============================================================================
PCDefClass::PCDefClass(TiXmlElement* element)
	:PCDefNode(element)
{
	LoadWildParams();
}

//-------------------------------------------------------------------------------
void PCDefClass::LoadWildParams()
{
	if(!node)
		return;

	// pick all params, which contains star character
	for(const TiXmlNode* node = GetNode()->FirstChild(); node; node = node->NextSibling())
	{
		const TiXmlElement* elm = node->ToElement();

		if(!elm)
			continue;

		if(strcmpi(elm->Value(), "param") == 0)
		{
			const char* attr = elm->Attribute("name"); 
			if(strchr(attr, '*') != NULL)
				wildParams.Push(elm);
		}
	}

	// Now we have to sort them because we want to have parameter "*_small_number" before "*_number" and all these before "*" etc.
	qsort(wildParams.GetArray(), wildParams.GetCardinality(), sizeof(TiXmlElement*), CompareElements);
}

//===============================================================================
const PCDefParam* PCDefClass::GetParamDef(const char* name, bool create) const
{
//	if(strcmpi(name, InterpolateToArrayName) == 0)
//		int gg = 0;

	for(const TiXmlNode* node = GetNode()->FirstChild(); node; node = node->NextSibling())
	{
		const TiXmlElement* elm = node->ToElement();

		if(!elm)
			continue;

		if(strcmpi(elm->Value(), "param") == 0)
		{
/*			const char* ssss = elm->Value();
			const char* vvvv = elm->Attribute("name");

			if(strcmpi(vvvv, InterpolateToArrayName) == 0)
			{
				const char* desc = elm->Attribute("desc");
				const char* type = elm->Attribute("type");
				const char* gui = elm->Attribute("gui");
				const char* readonly = elm->Attribute("readonly");
				int gg = 0;
			}
*/
			const char* attr = elm->Attribute("name"); 

			// skip all parameters with star character, they will be tested later.
			if(strchr(attr, '*'))
				continue;

			if(strcmpi(name, attr) == 0)
			{
				PCDefNode* node = (PCDefNode*)elm->GetUserData();
				assert(node);

				const PCDefParam* pd = node->ToParam();
				assert(pd);
				return pd;
			}
		}
	}
	
	// now test params with star character
	wxString wxName = name;
	for(int i=0;i<wildParams.GetCardinality();++i)
	{
		const TiXmlElement* elm = wildParams[i];
		if(!elm)
			continue;

		wxString wxAttr = elm->Attribute("name");

		if(DoesNameMatch(wxName, wxAttr))
		{
			PCDefNode* node = (PCDefNode*)elm->GetUserData();
			assert(node);

			const PCDefParam* pd = node->ToParam();
			assert(pd);
			return pd;
		}		
	}

	// look for definition in parent classes
	if(GetNode()->Parent())
	{
		PCDefClass* parentClass = (PCDefClass*)GetNode()->Parent()->GetUserData();
		if(parentClass)
		{
			const PCDefParam * defParam = parentClass->GetParamDef(name, false);
			if(defParam)
				return defParam;
		}
	}

	if(!create)
		return NULL;

	//neexistuje tak vytvorime novy
	TiXmlNode* ClassElm = GetNode();
	TiXmlElement* ParamElm = ClassElm->InsertEndChild(TiXmlElement("param"))->ToElement();
	assert(ParamElm);
	ParamElm->SetAttribute("name", name);

	PCDefParam* prm = ENF_NEW PCDefParam(ParamElm);
	ParamElm->SetUserData(prm);
	prm->SetGenerated(true);

	return prm;
}
//-------------------------------------------------------------------------------
bool PCDefClass::DoesNameMatch(const wxString &text, const wxString &regEx) const
{
	int regI = 0;
	int lastStar = -1;
	int lastStarI = -1;
	for(int i = 0;i<text.length();++i)
	{
		if(regI >= regEx.length())
		{
			if(lastStar == -1)
				return false;
			else
			{
				regI = lastStar+1;
				i = ++lastStarI;
			}
		}

		if(regEx[regI] == '*')
		{
			lastStar = regI;
			lastStarI = i;
			++regI;
		}

		if(text[i] != regEx[regI])
		{
			if(lastStar == -1)
				return false;
			else
			{
				regI = lastStar + 1;
				i = ++lastStarI;
			}
			if(text[i] == regEx[regI])
				--i;
		}
		else
			++regI;
	}

	if(regI == regEx.length()-1 && regEx[regI] == '*')
		return true;
	return regI == regEx.length();
}

//-------------------------------------------------------------------------------
void PCDefClass::Clear()
{
	if(!GetNode())
		return;

	for(TiXmlNode* node = GetNode()->FirstChild(); node; node = node->NextSibling())
	{
		TiXmlElement* elm = node->ToElement();

		if(!elm)
			continue;

		PCDefNode* DefNode = (PCDefNode*)elm->GetUserData();

		if(DefNode && DefNode->ToClass())
			DefNode->ToClass()->Clear();

		SAFE_DELETE(DefNode);	//can be NULL if class is a file class
	}
	wildParams.Clear();
}

//-------------------------------------------------------------------------------
bool PCDefClass::IsHidden() const
{
	if(node && GetElement())
	{
		const char* hidden = GetElement()->Attribute("hidden");
	
		if(hidden && strcmpi(hidden, "true") == 0)
			return true;
	}

	return false;
}

//-------------------------------------------------------------------------------
bool PCDefClass::IsReadOnly() const
{
	if(node && GetElement())
	{
		const char* hidden = GetElement()->Attribute("readonly");
	
		if(hidden && strcmpi(hidden, "true") == 0)
			return true;
	}

	return false;
}

//-------------------------------------------------------------------------------
void PCDefClass::SetHidden(bool hidden)
{
	assert(GetElement());
	if(hidden)
		GetElement()->SetAttribute("hidden", "true");
	else
		GetElement()->SetAttribute("hidden", "false");
}

//-------------------------------------------------------------------------------
void PCDefClass::SetReadOnly(bool readonly)
{
	assert(GetElement());
	if(readonly)
		GetElement()->SetAttribute("readonly", "true");
	else
		GetElement()->SetAttribute("readonly", "false");
}

//===============================================================================
// definicia classu ParamConfig
//===============================================================================
ParamConfigDef::ParamConfigDef()
	:PCDefClass(NULL)
{
	enf_assert(!g_PConfigDef);

	if(!g_PConfigDef)
		g_PConfigDef = this;

	doc = new TiXmlDocument();
	InsertDefaultRootToDocument();
	GlobalParamsElm = NULL;
}

//-------------------------------------------------------------------------------
ParamConfigDef::~ParamConfigDef()
{
	SAFE_DELETE(doc);
	g_PConfigDef = NULL;
}

//-------------------------------------------------------------------------------
void ParamConfigDef::Destroy()
{
	Clear();
	delete this;
}

//-------------------------------------------------------------------------------
void ParamConfigDef::Clear()
{
	PCDefClass::Clear();
	DeleteGlobalyDefinedParams();
	doc->Clear();
	InsertDefaultRootToDocument();
}

//-----------------------------------------------------------------------------------------
void ParamConfigDef::DeleteGlobalyDefinedParams()
{
	if(GlobalParamsElm)
	{
		for(TiXmlNode* n = GlobalParamsElm->FirstChild(); n != NULL; n = n->NextSibling())
		{
			TiXmlElement* e = n->ToElement();

			if(!e)
				continue;

			PCDefNode* DefNode = (PCDefNode*)e->GetUserData();
			SAFE_DELETE(DefNode);
		}
		GlobalParamsElm = NULL;
	}
	wildGlobals.Clear();
}

//-----------------------------------------------------------------------------------------
void ParamConfigDef::InsertDefaultRootToDocument()
{
	enf_assert(doc);
	TiXmlNode* nd = doc->InsertEndChild(TiXmlElement("configdef"));
	TiXmlElement* elm = nd->ToElement();
	elm->SetAttribute("version", "1.0");

	TiXmlElement* root = elm->InsertEndChild(TiXmlElement("classdef"))->ToElement();
	root->SetAttribute("class", "file");
}

//-----------------------------------------------------------------------------------------
TiXmlNode* ParamConfigDef::GetNode() const
{
	assert(doc);
	TiXmlElement* root = doc->RootElement();
//	assert(root);
	return root;
}

//-----------------------------------------------------------------------------------------
TiXmlElement* GetChildElement(TiXmlNode* parent, const char* name)
{
	for(TiXmlNode* node = parent->FirstChild(); node; node = node->NextSibling())
	{
		TiXmlElement* elm = node->ToElement();

		if(elm && strcmpi(elm->Value(), name) == 0)
			return elm;
	}
	return NULL;
}

//-----------------------------------------------------------------------------------------
TiXmlElement* ParamConfigDef::GetChildClassDefElement(TiXmlNode* parent, const char* ClassName) const
{
	for(TiXmlNode* node = parent->FirstChild(); node; node = node->NextSibling())
	{
		TiXmlElement* elm = node->ToElement();

		if(elm && strcmpi(elm->Value(), "classdef") == 0)
		{
			const char* ClName = elm->Attribute("class");

			if(ClName && strcmpi(ClName, ClassName) == 0)
				return elm;
		}
	}
	return NULL;
}

//-------------------------------------------------------------------------------
void ParamConfigDef::LoadChilds(TiXmlElement* parent)
{
	assert(parent);

	const char* parentName = parent->Value();

	for(TiXmlNode* node = parent->FirstChild(); node != NULL; node = node->NextSibling())
	{
		TiXmlElement* elm = node->ToElement();

		if(!elm)
			continue;

		if(strcmpi(elm->Value(), "classdef") == 0)
		{
			PCDefClass* cl = ENF_NEW PCDefClass(elm);
			elm->SetUserData(cl);
			LoadChilds(elm);
		}
		else if(strcmpi(elm->Value(), "param") == 0)
		{
			PCDefParam* prm = ENF_NEW PCDefParam(elm);
			elm->SetUserData(prm);
		}
		else if(strcmpi(elm->Value(), "globalparams") == 0)
		{
			enf_assert(!GlobalParamsElm);

			if(!GlobalParamsElm)
			{
				GlobalParamsElm = elm;

				for(TiXmlNode* n = elm->FirstChild(); n != NULL; n = n->NextSibling())
				{
					TiXmlElement* e = n->ToElement();

					if(!e)
						continue;

					if(strcmpi(e->Value(), "param") == 0)
					{
						const char* name = e->Attribute("name");
						if(name && strchr(name, '*') != NULL)
							wildGlobals.Push(e);

						PCDefParam* prm = ENF_NEW PCDefParam(e);
						e->SetUserData(prm);
					}
				}
			}
		}
	}

	qsort(wildGlobals.GetArray(), wildGlobals.GetCardinality(), sizeof(TiXmlElement*), CompareElements);
}

//-------------------------------------------------------------------------------
CResult ParamConfigDef::Load(const char* FileName, bool* CreatedDeafultFile)
{
	assert(doc);
	CResult result(true);
	bool loaded = doc->LoadFile(FileName);

	if(!loaded)
	{
		DString comment("Cannot load config definition file ");
		comment += FileName;

		if(taFileExists(FileName))
			comment += " because file format is invalid";
		else
		{
			comment += " because file not exist. Now creating default)";
			*CreatedDeafultFile = doc->SaveFile(FileName);
		}

		comment += '\n';
		comment += '\n';
		comment += "Some features will unavailable!";
		result.SetComment(comment.c_str());
//		return CResult(false, comment.c_str());
	}

	TiXmlElement* root = doc->RootElement();
	assert(root);

	if(!root)
		return CResult(false);

	if(loaded)
		LoadChilds(root);	//rekurzivne nahraje classdefy a paramdefy

	return result;
}

//-------------------------------------------------------------------------------
CResult ParamConfigDef::Save(const char* FileName)
{
	assert(doc);
	bool saved = doc->SaveFile(FileName);

	if(!saved)
	{
		DString comment("Cannot save config definition file ");
		comment += "(";
		comment += FileName;
		comment += ")";
		return CResult(false, comment.c_str());
	}

	return CResult(true);
}

//-------------------------------------------------------------------------------
const PCDefClass*	ParamConfigDef::GetClassDef(const char* ClassPath) const
{
	TiXmlElement* root = doc->RootElement();
	assert(root);

	if(!root)
		return NULL;

	PCPath path(ClassPath);
	TiXmlElement* FindElm = root;

	for(DString level = path.GetFirstLevel(); !level.empty(); level = path.GetNextLevel())
	{
		TiXmlElement* LevelElm = GetChildClassDefElement(FindElm, level.c_str());

		if(!LevelElm)
		{
			LevelElm = FindElm->InsertEndChild(TiXmlElement("classdef"))->ToElement();

			LevelElm->SetAttribute("class", level.c_str());
			PCDefClass* cl = ENF_NEW PCDefClass(LevelElm);
			LevelElm->SetUserData(cl);

			cl->SetGenerated(true);	//generovane sa neukladaju na disk
		}

		FindElm = LevelElm;
	}

	assert(FindElm != root);

	PCDefNode* node = (PCDefNode*)FindElm->GetUserData();
	assert(node);

	PCDefClass* cl = node->ToClass();
	assert(cl);
	return cl;
}

//-------------------------------------------------------------------------------
const PCDefParam* ParamConfigDef::GetGlobalParamDef(const char* name) const
{
	if(!GlobalParamsElm)
		return NULL;

	for(TiXmlNode* n = GlobalParamsElm->FirstChild(); n != NULL; n = n->NextSibling())
	{
		TiXmlElement* e = n->ToElement();

		if(!e)
			continue;

		if(strcmpi(e->Value(), "param") == 0)
		{
			const char* attr = e->Attribute("name");
			
			if(strchr(attr, '*') != NULL)
				continue;

			if(attr && strcmpi(attr, name) == 0)
			{
				PCDefNode* node = (PCDefNode*)e->GetUserData();
				assert(node);

				const PCDefParam* pd = node->ToParam();
				enf_assert(pd);
				return pd;
			}
		}
	}

	wxString wxName = name;
	for(int i=0;i<wildGlobals.GetCardinality();++i)
	{
		const TiXmlElement* elm = wildGlobals[i];
		if(!elm)
			continue;

		wxString wxAttr = elm->Attribute("name");

		if(DoesNameMatch(wxName, wxAttr))
		{
			PCDefNode* node = (PCDefNode*)elm->GetUserData();
			assert(node);

			const PCDefParam* pd = node->ToParam();
			assert(pd);
			return pd;
		}		
	}

	return NULL;
}