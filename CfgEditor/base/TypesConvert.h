#ifndef TYPES_CONVERT_H
#define TYPES_CONVERT_H

//some BIS compactible functions to correct type detection from string
int ScanHex(const char *val, bool &ok);
float ScanDb(const char *ptr, bool &ok);
float ScanFloatPlain(const char *ptr, bool &ok);
int ScanIntPlain(const char *ptr, bool &ok);
int ScanInt(const char *ptr, bool &ok);
float ScanFloat(const char *ptr, bool &ok);
bool ScanBool(const char *ptr, bool &ok);
bool ScanString(const char* val);

#endif