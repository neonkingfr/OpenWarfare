
#ifndef LIST_MESSAGE_DIALOG_H
#define LIST_MESSAGE_DIALOG_H

#include "wx/dialog.h"

//===========================================================================
// custom message dialog with listbox. used for very large messages
//===========================================================================
class ListMessageDialog : public wxDialog
{
public:
	ListMessageDialog(wxWindow* parent, const wxString& title);
	inline wxListBox* GetListBox(){return listbox;}
	void SetMessage(const wxString& message);
private:
	wxStaticText* MessageArea;
	wxListBox* listbox;
	wxButton*	OkButton;
	wxButton*	CancelButton;
};

#endif