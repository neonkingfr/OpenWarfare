#include "precomp.h"
#include "ListMessageDialog.h"

//-------------------------------------------------------------
ListMessageDialog::ListMessageDialog(wxWindow* parent, const wxString& title)
	:wxDialog(parent, -1, title, wxDefaultPosition, wxSize(480, 640), wxCAPTION|wxTHICK_FRAME|wxSYSTEM_MENU|wxCLOSE_BOX | wxTAB_TRAVERSAL|wxNO_BORDER)
{
	MessageArea = new wxStaticText(this, wxID_ANY, "", wxDefaultPosition, wxSize(-1, -1), wxST_NO_AUTORESIZE);
	listbox = new wxListBox(this, -1, wxDefaultPosition, wxDefaultSize, 0, NULL, wxLB_SORT);

	wxButton* OkButton = new wxButton(this, wxID_OK, "Ok", wxDefaultPosition, wxSize(80, 25));
	wxButton* CancelButton = new wxButton(this, wxID_CANCEL, "Cancel", wxDefaultPosition, wxSize(80, 25));

	wxBoxSizer* TopSizer = new wxBoxSizer(wxVERTICAL);
	TopSizer->Add(MessageArea, 0, wxGROW | wxALIGN_LEFT | wxALL, 0);
	TopSizer->AddSpacer(10);
	TopSizer->Add(listbox, 1, wxGROW | wxALIGN_LEFT | wxALL, 0);

	wxBoxSizer* ButtonsSizer = new wxBoxSizer(wxHORIZONTAL);
	ButtonsSizer->Add(OkButton, 0, wxALIGN_LEFT | wxALL, 0);
	ButtonsSizer->AddStretchSpacer();
	ButtonsSizer->Add(CancelButton, 0, wxALIGN_LEFT | wxALL, 0);

	wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
	sizer->Add(TopSizer, 1, wxGROW | wxALIGN_LEFT | wxALL, 10);
	sizer->Add(ButtonsSizer, 0, wxGROW | wxALIGN_LEFT | wxALL, 10);
	SetSizer(sizer);

	CancelButton->SetFocus();
}

//-------------------------------------------------------------
void ListMessageDialog::SetMessage(const wxString& message)
{
	MessageArea->SetLabel(message);
}