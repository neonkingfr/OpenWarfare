#include "precomp.h"
#include "PCFilename.h"

//------------------------------------------------------------------------------------
PCFilename::PCFilename()
{
	protect = true;
	CanUpdateBase = false;
}

//------------------------------------------------------------------------------------
PCFilename::PCFilename(const char* FileName)
{
	this->FileName = FileName;
	protect = true;
	CanUpdateBase = false;
}

//------------------------------------------------------------------------------------
PCFilename::PCFilename(const char* FileName, bool protect, bool CanUpdateBase)
{
	this->FileName = FileName;
	this->protect = protect;
	this->CanUpdateBase = CanUpdateBase;
}

