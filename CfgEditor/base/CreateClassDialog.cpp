#include "precomp.h"
#include "CreateClassDialog.h"
#include "ParamConfig.h"

BEGIN_EVENT_TABLE(CreateClassDialog, wxDialog)
EVT_CHECKBOX(-1, CreateClassDialog::OnCheckBoxSwitch)
END_EVENT_TABLE()

//-------------------------------------------------------------
CreateClassDialog::CreateClassDialog(wxWindow* parent, PCClass* SelectedClass, bool CreateInside)
	:wxDialog(parent, -1, "Create class", wxDefaultPosition, wxSize(320, 160), wxCAPTION|wxTHICK_FRAME|wxSYSTEM_MENU|wxCLOSE_BOX | wxTAB_TRAVERSAL|wxNO_BORDER)
{
	wxStaticText* ClassLabel = new wxStaticText(this, wxID_ANY, "Name", wxDefaultPosition, wxSize(-1, -1), wxST_NO_AUTORESIZE);
	wxStaticText* BaseLabel = new wxStaticText(this, wxID_ANY, "Base class", wxDefaultPosition, wxSize(-1, -1), wxST_NO_AUTORESIZE);

	NameEditBox = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(220, 20), 0);
//	BaseNameEditBox = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(140, 20), 0);

	bool SearchBases = true;
	if(CreateInside)
	{
		SearchBases = false;
		for(int n = (int)SelectedClass->GetNodesCount() - 1; n >= 0; n--)
		{
			PCNode* node = SelectedClass->GetNode(n);

			if(node->ToClass())
			{
				SelectedClass = node->ToClass();
				SearchBases = true;
				break;
			}
		}
	}

	wxArrayString choices;
	EArray<PCClass*> PossibleBases;

	if(SearchBases)
	{
		SelectedClass->GetPossibleBaseClasses(PossibleBases);
		PossibleBases.Insert(SelectedClass);
		
		for(uint n = 0; n < PossibleBases.GetCardinality(); n++)
		{
			PCClass* cl = PossibleBases[n];
			choices.Add(cl->GetName());
		}
	}

	BaseNameEditBox = new wxComboBox(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(220, 20), choices, 0);

	DeclOnlyCheck = new wxCheckBox(this, CHECKBOX_DECL_ONLY, "declaration only", wxDefaultPosition, wxDefaultSize, 0);

	wxButton* OkButton = new wxButton(this, wxID_OK, "Ok", wxDefaultPosition, wxSize(80, 25));
	wxButton* CancelButton = new wxButton(this, wxID_CANCEL, "Cancel", wxDefaultPosition, wxSize(80, 25));

	wxBoxSizer* LeftSizer = new wxBoxSizer(wxVERTICAL);
	LeftSizer->Add(ClassLabel, 0, wxALIGN_LEFT | wxBOTTOM, 15);
	LeftSizer->Add(BaseLabel, 0, wxALIGN_LEFT | wxBOTTOM, 15);

	wxBoxSizer* RightSizer = new wxBoxSizer(wxVERTICAL);
	RightSizer->Add(NameEditBox, 0, wxALIGN_LEFT | wxBOTTOM, 7);
	RightSizer->Add(BaseNameEditBox, 0, wxALIGN_LEFT | wxBOTTOM, 12);
	RightSizer->Add(DeclOnlyCheck, 0, wxALIGN_LEFT | wxBOTTOM, 12);

	wxBoxSizer* HorzSizer = new wxBoxSizer(wxHORIZONTAL);
	HorzSizer->Add(LeftSizer, 0, wxALIGN_LEFT | wxTOP, 4);
	HorzSizer->AddSpacer(20);
	HorzSizer->Add(RightSizer, 0, wxGROW | wxALIGN_LEFT | wxALL, 0);

	wxBoxSizer* ButtonsSizer = new wxBoxSizer(wxHORIZONTAL);
	ButtonsSizer->Add(OkButton, 0, wxALIGN_LEFT | wxALL, 0);
	ButtonsSizer->AddStretchSpacer();
	ButtonsSizer->Add(CancelButton, 0, wxALIGN_LEFT | wxALL, 0);

	wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
	sizer->Add(HorzSizer, 1, wxGROW | wxALIGN_LEFT | wxALL, 10);
	sizer->Add(ButtonsSizer, 0, wxGROW | wxALIGN_LEFT | wxALL, 10);
	SetSizer(sizer);
}

//-------------------------------------------------------------
void CreateClassDialog::SetName(const wxString& name)
{
	NameEditBox->SetValue(name);
}

//-------------------------------------------------------------
void CreateClassDialog::SetBaseName(const wxString& name)
{
	BaseNameEditBox->SetValue(name);
}

//-------------------------------------------------------------
void CreateClassDialog::SetDeclarationOnly(bool declaration)
{
	DeclOnlyCheck->SetValue(declaration);
	OnCheckBoxChanged();
}

//-------------------------------------------------------------
wxString CreateClassDialog::GetName()
{
	return NameEditBox->GetValue();
}

//-------------------------------------------------------------
wxString CreateClassDialog::GetBaseName()
{
	return BaseNameEditBox->GetValue();
}

//-------------------------------------------------------------
bool CreateClassDialog::GetDeclarationOnly()
{
	return DeclOnlyCheck->IsChecked();
}

//-------------------------------------------------------------
void CreateClassDialog::OnCheckBoxSwitch(wxCommandEvent& event)
{
	switch(event.GetId())
	{
	case CHECKBOX_DECL_ONLY:
		OnCheckBoxChanged();
		break;
	}
}

//-------------------------------------------------------------
void CreateClassDialog::OnCheckBoxChanged()
{
	BaseNameEditBox->SetValue("");
	BaseNameEditBox->SetEditable(!DeclOnlyCheck->IsChecked());

	if(DeclOnlyCheck->IsChecked())
	{
		BaseNameEditBox->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_INACTIVEBORDER));
	}
	else
	{
		BaseNameEditBox->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
	}
}