#ifndef PROPERTIES_PANEL_H
#define PROPERTIES_PANEL_H

#include "wxExtension/WBPropertyGrid.h"
#include "wx/panel.h"

class WBPropertyGrid;
class PCArray;
class wxPGProperty;
class PropertiesPanel;
class PCClass;
class PCVar;
class PCNode;
class AttrDescriptions;

enum
{
	PROPGRID = 1,
	CHECKBOX_OVERLOAD_MODE,
	MENU_INSERT_VARIABLE,
	MENU_INSERT_ARRAY,
	MENU_DELETE_PARAM,
	MENU_OVERLOAD_PARAM,
	CHECKBOX_TREE_MODE,
};

//property grid control from "Class content" window
//==================================================================================
class PPPropertyGrid : public WBPropertyGrid
{
public:
	PPPropertyGrid(wxWindow* parent, PropertiesPanel* PropPanel, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize,  long style = wxPG_DEFAULT_STYLE, const wxChar* name = wxPropertyGridNameStr);
private:
	PropertiesPanel* PropPanel;
	void OnRMBUp(wxMouseEvent& event);
	void OnLMBUp(wxMouseEvent& event);
	void OnKeyDown(wxKeyEvent& event);
	DECLARE_EVENT_TABLE()
};

// Small panel for attribute description
//========================================================================
class DescriptionPanel : public wxPanel
{
public:
	DescriptionPanel(wxWindow *parent, PropertiesPanel* PropPanel, const char* AttrFilelName);
	~DescriptionPanel();
	void SetDescription(const wxString &name, const wxString &className = wxString(""));
	void ClearDescription();
private:
	wxStaticText*		Description;
	PropertiesPanel*	PropPanel;
	wxBoxSizer*			Sizer;
	AttrDescriptions*	AttrDesc;
};


//parent container window for PPPropertyGrid with toolbar
//========================================================================
class PropertiesPanel : public wxPanel
{
	friend class PPPropertyGrid;
public:
	PropertiesPanel(wxWindow* parent);
	void ShowSelectionProperties();
	inline WBPropertyGrid* GetPropGrid(){return PropGrid;}
	bool InOverloadMode();
	void SetOverloadMode(bool OverloadMode);
	void EnableChildProperties(wxPGId& prop);
	void Clear();
private:
//	wxToolBar*			toolbar;
	WBPropertyGrid*	PropGrid;
	DescriptionPanel*	DescPanel;
	wxCheckBox*			OverloadModeCheck;
	wxColour				PropSetColor;
	wxColour				PropSetColorDuplicate;
	wxCheckBox*			HideTreeCheck;
	wxBitmap				VariableBitmap;
	wxBitmap				ExternVariableBitmap;
	wxBitmap				ArrayBitmap;
	wxBitmap				ExternArrayBitmap;
	void					OnCheckBoxClick(wxCommandEvent& event);
	void					OnToolClick(wxCommandEvent& event);
	void					OnPropertyChanged(wxCommandEvent& event);
	void					OnPropGridRMBUp(wxMouseEvent& event);
	void					OnPropGridLMBUp(wxMouseEvent& event);
	void					OnPropGridKeyDown(wxKeyEvent& event);
	wxPGProperty*		ShowArrayProperties(PCArray* arr, wxPGProperty* ParentProp, const char* label, const char* name, bool ClassOwner, bool ReadOnly);
	void					CreateActionFromMenu(int MenuID);
	void					CanCreateActions(bool& InsertProperty, bool& DeleteProperty, bool& OverloadProperty);
	wxPGProperty*		CreatePropertyForVariable(PCNode* VarOwner, PCVar* variable, const char* VarName, const char* id, const char* VarValue, wxPGId ParentProp, int position);
	wxPGId				FindPropSelectionAfterDelete(const wxPGId& PropID);
	void					GetClassesToView(EArray<PCClass*>& classes);
	void					GetParamOwnersFromSelection(EArray<PCNode*>& ParamOwners, const char* param);
	void					UpdateItemDescription();
  bool PropertiesPanel::TreeHidden();
  void PropertiesPanel::HideTree(bool HideTree);
	DECLARE_EVENT_TABLE();
};

#endif