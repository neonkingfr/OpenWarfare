
#ifndef ID_SET_H
#define ID_SET_H

#include "common/enf_eset.h"

//===============================================================================
class PCPath
{
public:
					PCPath();
					PCPath(const char* path);
	const char*	AddLevel(const char* name);
	DString		GetFirstLevel() const;
	DString		GetNextLevel() const;
	const char* GetLastLevel() const;
	const char* GetPath() const;
	bool			operator>(const PCPath& p);
	bool			operator<(const PCPath& p);
	bool			operator==(const PCPath& p);
//					ENF_DECLARE_MMOBJECT(PCPath);
private:
	DString path;
	mutable int pos;
};

//==========================================================================
class IDSet
{
public:
			IDSet();
			IDSet(const IDSet& set);
	uint	GetCount() const;
	uint	Add(const PCPath& id);
	bool	Remove(const PCPath& id);
	bool	Contains(const PCPath& id) const;
	void	Clear();
	void	operator=(const IDSet& set);
	const PCPath& operator[](uint index) const;
	ENF_DECLARE_MMOBJECT(IDSet);
private:
	void	Copy(const IDSet& set);
	ESet<PCPath> IDs;
};

#endif