
#ifndef OPTIONS_DIALOG_H
#define OPTIONS_DIALOG_H

#include "wx/panel.h"

class wxButton;
class wxCheckBox;
class FileTextBox;
class OptionsDialog;

enum
{
	FILEBOX_ROOT_DIR,
	FILEBOX_WORKING_DIR
};

//===================================================================
// for custom versions of editor can create custom panel in free space of OptionsDialog. 
// this is possible when u create own panel with any content inherited from this a then if MainFrame::CreateOptionsPanel() function is called, u can create it
//===================================================================
class OptionsDialogPanel : public wxPanel
{
	friend class OptionsDialog;	//because of call methods ValuesFromConfig() and ValuesToConfig()
public:
	OptionsDialogPanel(OptionsDialog* parent);
protected:
	virtual void ValuesFromConfig(){};	//values from GUI need store to config
	virtual void ValuesToConfig(){};		//values from config need insert to gui
};

//===================================================================
// application options panel. custom versions of editor may opportunity create own gui inside using OptionsDialogPanel
//===================================================================
class OptionsDialog : public wxDialog
{
public:
	OptionsDialog(wxWindow* parent);
	~OptionsDialog();
private:
	OptionsDialogPanel* ExternPanel;
	FileTextBox* WorkingDirFileBox;
	FileTextBox* RootDirFileBox;
	wxCheckBox* HideUnusedExternClasses;
	void OnButtonPress(wxCommandEvent& event);
	void ValuesFromConfig();
	void ValuesToConfig();
	DECLARE_EVENT_TABLE()
};

#endif