
#ifndef MAIN_FRAME_H
#define MAIN_FRAME_H

class wxMenu;
class DiagramWindow;
class WBPropertyGrid;
class wxAuiNotebook;
class ParamFileTree;
class PropertiesPanel;
class OptionsDialog;
class OptionsDialogPanel;

enum GenericGuiIDs
{
	MENU_NEW = 2,
	MENU_LOAD,
	MENU_SEQUENCE_LOAD,
	MENU_SAVE,
	MENU_SAVE_AS,
	MENU_GENERATE_DEF_FILE,
	MENU_REMOVE_DUPLICITIES,
	MENU_COPY,
	MENU_PASTE,
	MENU_UNDO,
	MENU_REDO,
	MENU_FIRST_PANE,
	MENU_LAST_PANE = 50,
	TREE_PARAM,
	MENU_OPTIONS_DIALOG,
	MENU_BASE_CLASSES_IN_TREE,
	MENU_EXPAND_ALL_CLASSES,
	MENU_COLLAPSE_ALL_CLASSES,
	GenericGuiIDs_LAST_ENUM
};

//======================================================================
//editor main window. for custom versions of editor is good idea inerit it because all different implementations 
//include creating windows and event handling can realize using virtual methods
//======================================================================
class MainFrame : public wxFrame
{
	friend class OptionsDialog;
public:
	MainFrame();
	~MainFrame();
	bool							Create(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_FRAME_STYLE, const wxString& name = "frame");	//init window content .all subwindows, toolbars...
	WBPropertyGrid*			GetPropGrid();									//properties window. show full class content except classes
	inline wxAuiManager*		GetAUIManager(){return AuiManager;}		//wx advanced UI manager for docking windows and more
	inline ParamFileTree*	GetParamTree(){return ParamTree;}		//treeview s clasovou strukturou
	wxMenu*						GetFileMenu(){return FileMenu;}			//file menu pointer
	wxMenu*						GetEditMenu(){return EditMenu;}			//edit menu pointer
	wxMenu*						GetOptionsMenu(){return OptionsMenu;}	//options menu pointer
	wxToolBar*					GetMainToolBar(){return MainToolBar;}	//main toolbar pointer
	inline bool					IsDestroying(){return destroing;}		//true == this window is destroying. usefull to ignore some events before app exist
	inline PropertiesPanel* GetPropPanel(){return PropPanel;}		//panel with ParamFileTree ("Class content" window)
	virtual void				UpdateTitle();									//refresh title name
	void							ShowOptionsDialog();							//show options dialog
	virtual wxString			GetDefaultPanesPerspective();				//default AUI panes perspective setting for wx advanced UI mamager
	void							LoadAUIPanesPerspective();					//from config load AUI panes perspective setting for wx advanced UI mamager
	void							SaveAUIPanesPerspective();					//save AUI panes perspective setting for wx advanced UI mamager
protected:
	virtual void				CreateAUIPanes();								//for custom versions of editor opportunity to create absolutly different subwindows if needed
	virtual wxMenuBar*		CreateMenuBar();								//same for menu bar
	virtual wxToolBar*		CreateMainToolBar();							//same for main toolbar
	virtual wxMenu*			CreateFileMenu();								//same for file menu
	virtual wxMenu*			CreateEditMenu();								//same for edit menu
	wxMenu*						CreateWindowsMenu();							//same for windows menu
	virtual void				CreateImageList();							//image list with image storages shared for all gui components in editor
	virtual OptionsDialogPanel* CreateOptionsPanel(OptionsDialog* parent){return NULL;}	//for custom versions of editor like AnimEditor oppotrunity to create own gui in OptionsDialog
	wxAuiManager*		AuiManager;
	PropertiesPanel*	PropPanel;
	wxMenu*				FileMenu;
	wxMenu*				EditMenu;
	wxMenu*				WindowMenu;
	wxMenu*				OptionsMenu;
	wxToolBar*			MainToolBar;
	ParamFileTree*		ParamTree;
	bool					destroing;
private:
	int GetIndexOfPane(wxString& PaneCaption);
	void OnClose(wxCloseEvent& event);
	void OnPaneClose(wxAuiManagerEvent& event);
	void OnSelectFromMenu(wxCommandEvent& event);
	void OnIdle(wxIdleEvent& event);
	DECLARE_EVENT_TABLE();
};

extern MainFrame* g_MainFrame;

#endif