
#include "precomp.h"
#include "MainFrame.h"
#include "GenericEditor.h"
#include "PropertiesPanel.h"
#include "ParamFileTree.h"
#include "OptionsDialog.h"
#include "common/WxNativeConvert.h"
#include "common/XmlConfig.h"
#include "ImageList.h"
#include "wxExtension/WBPropertyGrid.h"
#include "bitmaps/ParamClassSuper.xpm"
#include "wxExtension/TreeListCtrl.h"

MainFrame* g_MainFrame = NULL;

BEGIN_EVENT_TABLE(MainFrame, wxFrame)
	EVT_MENU(-1, MainFrame::OnSelectFromMenu)
	EVT_CLOSE(MainFrame::OnClose)
	EVT_AUI_PANE_CLOSE(MainFrame::OnPaneClose)
END_EVENT_TABLE()


//======================================================================
MainFrame::MainFrame()
	:wxFrame()
{
	destroing = false;
	AuiManager = NULL;
	ParamTree = NULL;
	FileMenu = NULL;
	EditMenu = NULL;
	OptionsMenu = NULL;
	MainToolBar = NULL;
}

//----------------------------------------------------------------------
bool MainFrame::Create(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
{
	bool result = wxFrame::Create(parent, id, title, pos, size, style, name);

	assert(!g_MainFrame);
	g_MainFrame = this;

	SetIcon(wxIcon(ParamClassSuper_xpm));

	CreateImageList();
	AuiManager = new wxAuiManager(this, /*wxAUI_MGR_DEFAULT*/wxAUI_MGR_ALLOW_FLOATING | wxAUI_MGR_TRANSPARENT_DRAG | wxAUI_MGR_VENETIAN_BLINDS_HINT |/*wxAUI_MGR_RECTANGLE_HINT |*/ wxAUI_MGR_HINT_FADE | wxAUI_MGR_NO_VENETIAN_BLINDS_FADE | wxAUI_MGR_ALLOW_ACTIVE_PANE);

	CreateAUIPanes();
	LoadAUIPanesPerspective();
	AuiManager->Update();
	SetMenuBar(CreateMenuBar());
	return result;
}

//-------------------------------------------------------------------------------------
wxString MainFrame::GetDefaultPanesPerspective()
{
	return "layout2|name=Toolbar;caption=Toolbar;state=2108156;dir=1;layer=1;row=0;pos=0;prop=100000;bestw=-1;besth=23;minw=-1;minh=-1;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=ConfigFileWindow;caption=Class tree;state=4196096;dir=5;layer=0;row=0;pos=0;prop=100000;bestw=200;besth=200;minw=200;minh=200;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=-1;floath=-1|name=ClassParmsWindow;caption=Class content;state=6309884;dir=2;layer=0;row=0;pos=0;prop=100000;bestw=800;besth=200;minw=200;minh=200;maxw=-1;maxh=-1;floatx=-1;floaty=-1;floatw=400;floath=600|dock_size(1,1,0)=25|dock_size(5,0,0)=202|dock_size(2,0,0)=496|";
}

//----------------------------------------------------------------------
void MainFrame::LoadAUIPanesPerspective()
{
	XMLConfig* cfg = g_editor->GetConfig();
	XMLConfigNode* node = cfg->GetNode("defaults", true);

	wxString PanesPerspective;
	if(node->GetValue("PanesPerspective", PanesPerspective) == false)
		PanesPerspective = GetDefaultPanesPerspective();

	if(!PanesPerspective.IsEmpty())
		AuiManager->LoadPerspective(PanesPerspective, false);
}

//----------------------------------------------------------------------
void MainFrame::SaveAUIPanesPerspective()
{
	XMLConfig* cfg = g_editor->GetConfig();
	XMLConfigNode* node = cfg->GetNode("defaults", true);

	wxString PanesPerspective = AuiManager->SavePerspective();
	node->SetValue("PanesPerspective", PanesPerspective);
}

//----------------------------------------------------------------------
MainFrame::~MainFrame()
{
	XMLConfig* config = g_editor->GetConfig();
	XMLConfigNode* node = config->GetNode("defaults", true);
	node = node->GetChildNode("MainWindow", true);
	node->SetValue("maximize", IsMaximized());

	if(!IsMaximized())	//store window rect only if is not maximized!
	{
		wxPoint MainWindowPos = GetPosition();
		wxSize MainWindowSize = GetSize();
		node->SetValue("left", MainWindowPos.x);
		node->SetValue("top", MainWindowPos.y);
		node->SetValue("width", MainWindowSize.x);
		node->SetValue("height", MainWindowSize.y);
	}

	destroing = true;
	DestroyChildren();

	if(AuiManager)
	{
		AuiManager->UnInit();	//need call this before delete
		delete AuiManager;
		AuiManager = NULL;
	}

	SAFE_DELETE(g_ImageList);
	g_MainFrame = NULL;
}

//-------------------------------------------------------------------------
void MainFrame::CreateImageList()
{
	new ImageList(16, 16, true);	//common image list. g_ImageList pointer on
}

//----------------------------------------------------------------------------
void MainFrame::OnClose(wxCloseEvent& event)
{
	SaveAUIPanesPerspective();

	if ( event.CanVeto())
	{
		if(g_editor->ReadyToClose() == false)
		{
			event.Veto();
			return;
		}
	}

	event.Skip();
}

//----------------------------------------------------------------------
void MainFrame::CreateAUIPanes()
{
	MainToolBar = CreateMainToolBar();

	PropPanel = new PropertiesPanel(this);

	ParamTree = new ParamFileTree(this, TREE_PARAM);
	ParamTree->SetImageList(g_ImageList);

	AuiManager->AddPane(MainToolBar, wxAuiPaneInfo().Layer(1).ToolbarPane().BestSize(-1, 23).LeftDockable(false).RightDockable(false).Top().Name("Toolbar").Caption("Toolbar"));
	AuiManager->AddPane(ParamTree, wxAuiPaneInfo().CenterPane().MinSize(200, 200).Name("ConfigFileWindow").Caption("Class tree").CaptionVisible(true).MaximizeButton(true));
	AuiManager->AddPane(PropPanel, wxAuiPaneInfo().Right().BestSize(800, 200).FloatingSize(400, 600).MinSize(200, 200).Name("ClassParmsWindow").Caption("Class content").MaximizeButton(true));
}

//---------------------------------------------------------------------------------------------
wxToolBar* MainFrame::CreateMainToolBar()
{
	wxToolBar* bar = new wxToolBar(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTB_FLAT | wxTB_NODIVIDER);
	bar->SetToolBitmapSize(wxSize(16, 16));
	bar->AddTool(MENU_NEW, wxT("New"), g_ImageList->GetBitmap(BI_NEW), wxT("New config"));
	bar->AddTool(MENU_LOAD, wxT("Open"), g_ImageList->GetBitmap(BI_OPEN), wxT("Open config"));
	bar->AddTool(MENU_SEQUENCE_LOAD, wxT("Open as update"), g_ImageList->GetBitmap(BI_OPEN_AS_UPDATE), wxT("Open config as update of another config(s)"));
	bar->AddTool(MENU_SAVE, wxT("Save"), g_ImageList->GetBitmap(BI_SAVE), wxT("Save config"));
	bar->AddTool(MENU_UNDO, wxT("Undo"), g_ImageList->GetBitmap(BI_UNDO), wxT("Undo last operation"));
	bar->AddTool(MENU_REDO, wxT("Redo"), g_ImageList->GetBitmap(BI_REDO), wxT("Redo last operation"));
	bar->AddCheckTool(MENU_BASE_CLASSES_IN_TREE, wxT("Show base classes int tree"), g_ImageList->GetBitmap(BI_SHOW_BASE_CLASS), wxNullBitmap, wxT("Show base classes in tree"));
	bar->AddTool(MENU_EXPAND_ALL_CLASSES, wxT("Expand class tree"), g_ImageList->GetBitmap(BI_EXPAND), wxT("Expand class tree"));
	bar->AddTool(MENU_COLLAPSE_ALL_CLASSES, wxT("Collapse class tree"), g_ImageList->GetBitmap(BI_COLLAPSE), wxT("Collapse class tree"));
//	bar->AddTool(MENU_COPY, wxT("Copy"), g_ImageList->GetBitmap(BI_COPY), wxT("Copy"));
//	bar->AddTool(MENU_PASTE, wxT("Paste"), g_ImageList->GetBitmap(BI_PASTE), wxT("Paste"));
	bar->EnableTool(MENU_SAVE, false);
	bar->EnableTool(MENU_UNDO, false);
	bar->EnableTool(MENU_REDO, false);
//	bar->AddSeparator();
	bar->Realize();
	return bar;
}

//----------------------------------------------------------------------
wxMenuBar* MainFrame::CreateMenuBar()
{
	wxMenuBar* MenuBar = new wxMenuBar;										

	FileMenu = CreateFileMenu();
	MenuBar->Append(FileMenu, _T("&File"));

	EditMenu = CreateEditMenu();
	MenuBar->Append(EditMenu, _T("&Edit"));

	WindowMenu = CreateWindowsMenu();
	MenuBar->Append(WindowMenu, _T("&Window"));

	OptionsMenu = new wxMenu;
	OptionsMenu->Append(MENU_OPTIONS_DIALOG, _T("Settings"), _T("app settings"));

	MenuBar->Append(OptionsMenu, _T("&Options"));

	return MenuBar;
}

//----------------------------------------------------------------------
wxMenu* MainFrame::CreateFileMenu()
{
   wxMenu* menu = new wxMenu;
	menu->Append(MENU_NEW, _T("New\tCtrl-N"), _T("create new param config"));
	menu->Append(MENU_LOAD, _T("Open\tCtrl-O"), _T("load param file config"));
	menu->Append(MENU_SEQUENCE_LOAD, _T("Open as update\tCtrl-U"), _T("load param file config as update of another config(s)"));
	menu->Append(MENU_SAVE, _T("Save\tCtrl-S"), _T("save param file config"));
	menu->Append(MENU_SAVE_AS, _T("Save As"), _T("save param file config"));
	menu->Append(MENU_GENERATE_DEF_FILE, _T("Generate definition"), _T("generate config definition file"));
	menu->Append(MENU_REMOVE_DUPLICITIES, _T("Remove unnecessary variables"), _T("Remove unnecessary variables"));
	menu->Enable(MENU_SAVE, false);
	menu->Enable(MENU_SAVE_AS, false);
	menu->Enable(MENU_GENERATE_DEF_FILE, false);
	menu->Enable(MENU_REMOVE_DUPLICITIES, false);
	return menu;
}

//----------------------------------------------------------------------
wxMenu* MainFrame::CreateEditMenu()
{
   wxMenu* menu = new wxMenu;
	menu->Append(MENU_COPY, _T("Copy\tCtrl-C"), _T("copy"));
	menu->Append(MENU_PASTE, _T("Paste\tCtrl-V"), _T("paste"));
	menu->Append(MENU_UNDO, _T("Undo\tCtrl-Z"), _T("undo"));
	menu->Append(MENU_REDO, _T("Redo\tCtrl-Y"), _T("redo"));
	menu->Enable(MENU_COPY, false);
	menu->Enable(MENU_PASTE, false);
	menu->Enable(MENU_UNDO, false);
	menu->Enable(MENU_REDO, false);
	return menu;
}

//----------------------------------------------------------------------
wxMenu* MainFrame::CreateWindowsMenu()
{
	wxMenu* menu = new wxMenu;
	wxAuiPaneInfoArray& panes = AuiManager->GetAllPanes();

	for(uint n = 0; n < panes.GetCount(); n++)
	{
		wxAuiPaneInfo& PaneInfo = panes[n];
		int ID = MENU_FIRST_PANE + n;
		menu->AppendCheckItem(ID, PaneInfo.caption, PaneInfo.caption);
		menu->Check(ID, PaneInfo.IsShown());
	}
	return menu;
}

//----------------------------------------------------------------------
void MainFrame::OnSelectFromMenu(wxCommandEvent& event)
{
	int ID = event.GetId();

	if(ID >= MENU_FIRST_PANE && ID <= MENU_LAST_PANE)
	{
		int PaneIndex = ID - MENU_FIRST_PANE;
		wxAuiPaneInfoArray& panes = AuiManager->GetAllPanes();
		assert(PaneIndex < (int)panes.GetCount());
		panes[PaneIndex].Show(WindowMenu->IsChecked(ID));
		AuiManager->Update();
		event.Skip();
		return;
	}

	switch(ID)
	{
		case MENU_NEW:
			g_editor->New();
			break;
		case MENU_LOAD:
			g_editor->Load();
			break;
		case MENU_SEQUENCE_LOAD:
			g_editor->SequenceLoad();
			break;
		case MENU_SAVE:
			g_editor->Save();
			break;
		case MENU_SAVE_AS:
			g_editor->SaveAs();
			break;
		case MENU_GENERATE_DEF_FILE:
			g_editor->SaveConfigDef();
			break;
		case MENU_REMOVE_DUPLICITIES:
			g_editor->RemoveDuplicities();
			break;
		case MENU_UNDO:
			g_editor->Undo();
			break;
		case MENU_REDO:
			g_editor->Redo();
			break;
		case MENU_COPY:
			g_editor->Copy();
			break;
		case MENU_PASTE:
			g_editor->Paste();
			break;
		case MENU_OPTIONS_DIALOG:
			ShowOptionsDialog();
			break;
		case MENU_BASE_CLASSES_IN_TREE:
			ParamTree->ShowBaseClasses(event.GetInt() == 0);
//			ParamTree->Refresh();
			break;
		case MENU_EXPAND_ALL_CLASSES:
		{
			wxTreeItemId root = ParamTree->GetRootItem();

			if(root.IsOk());
				ParamTree->ExpandAll(root);

			break;
		}
		case MENU_COLLAPSE_ALL_CLASSES:
		{
			wxTreeItemId root = ParamTree->GetRootItem();

			if(root.IsOk());
				ParamTree->CollapseAll(root);

			break;
		}
	}
	event.Skip();
}

//---------------------------------------------------------------------------------------------
void MainFrame::OnPaneClose(wxAuiManagerEvent& event)
{
	int PaneIndex = GetIndexOfPane(event.pane->caption);
	assert(PaneIndex >= 0);
	int MenuID = MENU_FIRST_PANE + PaneIndex;
	wxMenuItem* item = WindowMenu->FindItem(MenuID);

	if(item)
		WindowMenu->Check(MenuID, false);

/*    if (event.pane->name == wxT("test10"))
    {
        int res = wxMessageBox(wxT("Are you sure you want to close/hide this pane?"),
                               wxT("wxAUI"),
                               wxYES_NO,
                               this);
        if (res != wxYES)
            event.Veto();
    }*/
}

//----------------------------------------------------------------------
void MainFrame::OnIdle(wxIdleEvent& event)
{
	event.RequestMore();
}

//---------------------------------------------------------------------------------------------
void MainFrame::UpdateTitle()
{
	wxString title(g_editor->GetAppName());
	title += " - ";
	ParamConfig* cfg = g_editor->GetParamConfig();
	assert(cfg);
	DString file = cfg->GetSaveFilename();

	if(!file.empty())
	{
		wxString file2 = file.c_str();

		if(file2.Find('/'))
			file2 = file2.AfterLast('/');

		title += file2;
	}
	else
		title += "unnamed";

	if(g_editor->AnyChanges())
		title += '*';

	SetTitle(title);
}

//---------------------------------------------------------------------------------------------
int MainFrame::GetIndexOfPane(wxString& PaneCaption)
{
	wxAuiPaneInfoArray& panes = AuiManager->GetAllPanes();

	for(uint n = 0; n < panes.GetCount(); n++)
	{
		if(panes[n].caption == PaneCaption)
			return n;
	}
	return -1;
}

//--------------------------------------------------------------------------------------
WBPropertyGrid* MainFrame::GetPropGrid()
{
	return PropPanel->GetPropGrid();
}

//-------------------------------------------------------------------------------------
void MainFrame::ShowOptionsDialog()
{
	OptionsDialog dialog(this);

	if(dialog.ShowModal() == wxID_OK)
	{
		g_editor->OnAppConfigChanged();
	}
}

