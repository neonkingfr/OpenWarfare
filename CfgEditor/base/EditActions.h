
#ifndef EDIT_ACTIONS_H
#define EDIT_ACTIONS_H

//#include "base/IDSet.h"
#include "Common/UndoManager.h"

class PCClass;
class PCArray;

enum//EditActionType
{
	EA_UNKNOWN = 0,
	EA_CREATE_CLASS,				//create class action
	EA_CREATE_VARIABLE,			//create param of type variable
	EA_CREATE_ARRAY,				//create param of type array
	EA_MODIFY_VARIABLE,			//modify variable value
	EA_DELETE_NODE,				//delete class, array or variable node
	EA_RENAME_NODE,				//rename class, array or variable node. for class only implemented
	EA_SET_BASE_CLASS,			//set base class action
	EA_SET_NODE_EXTERN,			//make node extern or not extern. with extern node means class, variable or array from extern/preloaded config
	EA_SET_CLASS_DELETED,		//insert/remove "delete" command to disfunction class from external/base config
	EA_SET_CLASS_DEFINED,		//set class as defined or no. normal class or forward declaration only
	EA_LAST_UNUSED,				//unused action type. is a start define for extra actions for versions of editor
};

//===============================================================================
//edit action base class
//===============================================================================
class EditAction
{
public:
						EditAction(const char* id, int type);
virtual				~EditAction(){};
	inline int	GetType() const {return type;}
	inline const DString& GetId() const {return id;}
private:
	DString	id;
	int type;
};

//===============================================================================
//create class edit action
//===============================================================================
class EACreateClass : public EditAction
{
public:
	EACreateClass(const char* id, const char* BaseID, const char* OwnerID, PCClass* storage, uint PosInOwner, int ClassInfoFlags);
	~EACreateClass();
	ENF_DECLARE_MMOBJECT(EACreateClass);
	inline const DString& GetOwnerID() const {return OwnerID;}
	inline const DString& GetBaseId() const {return BaseID;}
	inline PCClass* GetStorage() const {return storage;}
	uint GetPosInOwner() const {return PosInOwner;}
	inline int GetClassInfoFlags(){return ClassInfoFlags;}
	DString	OwnerID;
	DString	BaseID;
	private:
	PCClass* storage;
	uint PosInOwner;
	int ClassInfoFlags;
};

//===============================================================================
//create class edit action
//===============================================================================
class EACreateArray : public EditAction
{
public:
	EACreateArray(const char* id, PCArray* storage, uint PosInOwner);
	~EACreateArray();
	const char* GetName() const;
	inline PCArray* GetStorage() const {return storage;}
	uint GetPosInOwner() const {return PosInOwner;}
	ENF_DECLARE_MMOBJECT(EACreateArray);
	private:
	PCArray* storage;
	uint PosInOwner;
};

//===============================================================================
//create variable edit action
//===============================================================================
class EACreateVariable : public EditAction
{
public:
	EACreateVariable(const char* id, const char* name, const char* value, uint PosInOwner);
	const char* GetName() const;
	const char* GetValue() const;
	uint GetPosInOwner() const {return PosInOwner;}
	ENF_DECLARE_MMOBJECT(EACreateVariable);
	private:
	DString	name;
	DString	value;
	uint PosInOwner;
};

//===============================================================================
//modify variable value edit action
//===============================================================================
class EAModifyVariable : public EditAction
{
public:
	EAModifyVariable(const char* id, const char* value);
	inline const DString& GetValue() const {return value;}
	ENF_DECLARE_MMOBJECT(EAModifyVariable);
	private:
	DString	value;
};

//===============================================================================
//create config file structure node edit action. node can be class, array or variable
//===============================================================================
class EADeleteNode : public EditAction
{
public:
	EADeleteNode(const char* id);
	ENF_DECLARE_MMOBJECT(EADeleteNode);
	private:
};

//===============================================================================
//rename config file structure node edit action. node can be class, array or variable
//===============================================================================
class EARenameNode : public EditAction
{
public:
	EARenameNode(const char* id, const char* NewName);
	inline const DString& GetNewName() const {return NewName;}
	ENF_DECLARE_MMOBJECT(EADeleteNode);
	private:
	DString NewName;
};

//===============================================================================
//set base class edit action.
//===============================================================================
class EASetBaseClass : public EditAction
{
public:
	EASetBaseClass(const char* id, const char* BaseID);
	inline const DString& GetBaseID() const {return BaseID;}
	ENF_DECLARE_MMOBJECT(EASetBaseClass);
	private:
	DString BaseID;
};

//===============================================================================
//set class extern or not. if class is extern, will be not saved (is from extern - not editable config)
//===============================================================================
class EASetNodeExtern : public EditAction
{
public:
	EASetNodeExtern(const char* id, bool MakeExtern);
	inline bool GetExtern() const {return MakeExtern;}
	ENF_DECLARE_MMOBJECT(EASetNodeExtern);
	private:
	bool MakeExtern;
};

//===============================================================================
//set extern class deleted or not. this causes add/remove "delete ClassName" command to begin of not extern owner body
//===============================================================================
class EASetClassDeleted : public EditAction
{
public:
	EASetClassDeleted(const char* id, bool MakeDeleted);
	inline bool GetDeleted() const {return MakeDeleted;}
	ENF_DECLARE_MMOBJECT(EASetClassDeleted);
	private:
	bool MakeDeleted;
};

//===============================================================================
//set class as defined or no. normal class or forward declaration only
//===============================================================================
class EASetClassDefined : public EditAction
{
public:
	EASetClassDefined(const char* id, bool MakeDefined);
	inline bool GetDefined() const {return MakeDefined;}
	ENF_DECLARE_MMOBJECT(EASetClassDefined);
	private:
	bool MakeDefined;
};

//==========================================================================
// undo/redo history state. contains strip of edit actions needed to realize one user look logical action
// and second strip of actions to restore changes bask to state before
// and states of some gui elements for undo/redo separatelly
//==========================================================================
class HistoryPoint : public UndoState
{
public:
									HistoryPoint(const char* name);
	virtual						~HistoryPoint();
	bool							AppendRedoAction(EditAction* action);
	bool							AppendUndoAction(EditAction* action);
	virtual bool				Undo();
	virtual bool				Redo();
	EArray<EditAction*>&		GetRedoActions();
	EArray<EditAction*>&		GetUndoActions();
	const EArray<DString>&	GetUndoClassSel();
	const EArray<DString>&	GetRedoClassSel();
	const DString&				GetUndoPropSel();
	const DString&				GetRedoPropSel();
	bool							GetRedoProperyGridMode();
	bool							GetUndoProperyGridMode();
	void							SetUndoClassSel(const EArray<DString>& sel);
	void							SetRedoClassSel(const EArray<DString>& sel);
	void							SetUndoPropSel(const char* sel);
	void							SetRedoPropSel(const char* sel);
	void							SetRedoProperyGridMode(bool OverloadMode);
	void							SetUndoProperyGridMode(bool OverloadMode);
									ENF_DECLARE_MMOBJECT(HistoryPoint);
private:
	EArray<EditAction*>		RedoActions;	//redo action strip
	EArray<EditAction*>		UndoActions;	//undo action strip
	EArray<DString>			UndoClassSel;	//classes selected in "Class tree" window before realizing redo actions strip
	EArray<DString>			RedoClassSel;	//classes selected in "Class tree" window after realizing redo actions strip
	DString						UndoPropSel;	//property selected in "Class content" window before realizing redo actions strip
	DString						RedoPropSel;	//property selected in "Class content" window after realizing redo actions strip
	bool							RedoProperyGridMode;
	bool							UndoProperyGridMode;
};

#endif