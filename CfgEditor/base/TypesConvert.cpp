
#include "precomp.h"
#include "TypesConvert.h"

inline int toLargeInt( float f )
{
	int retVal;
	_asm
	{
		fld f;
		fistp retVal;
	}
	return retVal;
}

int ScanHex(const char *val, bool &ok)
{
  ok = false;
  if (!strnicmp(val, "0x", 2))
  {
    char c;
    const char *ptr = (const char *)val + 2;
    ok = isxdigit(*ptr) != 0;
    if (!ok) return 0;
    int iValue = 0;
    while (c=*(ptr++), isxdigit(c))
    {
      iValue *= 16;
      if (isdigit(c))   // 0..9
        iValue += c - '0';
      else if (c<='F')  // A..F
        iValue += 10 + c - 'A';
      else              // a..f
        iValue += 10 + c - 'a';
    }
    ok = (c==0);
    return iValue;
  }
  else
  {
    return 0;
  }
}

float ScanDb(const char *ptr, bool &ok)
{
  ok=false;
  if (ptr[0]!='d' || ptr[1]!='b') return 0;
  ok=true;
  char *end;
  float db = strtod(ptr+2,&end);
  return pow(10.0f,db*(1.0f/20));
}

float ScanFloatPlain(const char *ptr, bool &ok)
{
  char *end;
  float db = strtod(ptr,&end);
  ok = (*end==0);
  return db;
}

int ScanIntPlain(const char *ptr, bool &ok)
{
  char *end;
  long db = strtol(ptr,&end,10);
  ok = (*end==0);
  return (int)db;
}

int ScanInt(const char *ptr, bool &ok)
{
  ok = false;
  if (!*ptr) return 0;
  int val = ScanIntPlain(ptr,ok);
  if (ok) return val;
  val = ScanHex(ptr,ok);
  if (ok) return val;
  return 0;
}

float ScanFloat(const char *ptr, bool &ok)
{
  ok = false;
  if (!*ptr) return 0;
  float val = ScanFloatPlain(ptr,ok);
  if (ok) return val;
  val = ScanDb(ptr,ok);
  if (ok) return val;
  return 0;
}

bool ScanBool(const char *ptr, bool &ok)
{
	if(strcmpi(ptr, "true") == 0)
	{
		ok = true;
		return true;
	}
	else if(strcmpi(ptr, "false") == 0)
	{
		ok = true;
		return false;
	}

	ok = false;
	return false;
}

//-------------------------------------------------------------------------------
bool ScanString(const char* val)
{
	if(strlen(val) == 0)
		return true;

	bool ok;
	ScanInt(val, ok);

	if(!ok)
		ScanFloat(val, ok);

	if(!ok)
		ScanBool(val, ok);

	if(ok)
		return false;

	return true;
}

float ToFloatValue(const char* value, bool &ok)
{
  // check for simple cases
  float valF = ScanFloat(value,ok);
  if (ok) return valF;
  int valI = ScanInt(value,ok);
  if (ok) return valI;
  return 0.0f;
}
int ToIntValue(const char* value, bool &ok)
{
  // check for simple cases
  int valI = ScanInt(value,ok);
  if (ok) return valI;
  float valF = ScanFloat(value,ok);
  // if there is no file, we cannot evaluate expressions
  if (ok)
    return toLargeInt(valF);

  return 0;
}
