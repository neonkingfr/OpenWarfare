
#include "precomp.h"
#include "base/ParamConfig.h"
#include "base/TypesConvert.h"
#include "Common/WxConvert.h"
#include "Common/NativeConvert.h"
#include "Common/NativeUtil.h"
#include "Common/WinApiUtil.h"
#include "Common/TextFile.h"

ParamConfig* g_ConfigPtr = NULL;

//-------------------------------------------------------------------------------
ParamConfig* GetConfigPtr()
{
	enf_assert(g_ConfigPtr);
	return g_ConfigPtr;
}

//-------------------------------------------------------------------------------
void AddTabs(DString& str, int num)
{
	if(num == 0)
		return;

	char* tabs = new char[num + 1];
	memset(tabs, '	', num);

	tabs[num] = 0;
	str += tabs;
	delete[] tabs;
}

//-------------------------------------------------------------------------------
char TemporalBuffer[4096];

//-------------------------------------------------------------------------------
const char* FormatStringValue(const char* value)
{
	if(ScanString(value) == true)
		return value;

	const char quot[2] = {'"', 0};
	strcpy(TemporalBuffer, quot);
	strcat(TemporalBuffer, value);
	strcat(TemporalBuffer, quot);
	return TemporalBuffer;
}

//-------------------------------------------------------------------------------
bool IsQuotedString(const char* str)
{
	int lng = strlen(str);

	if(lng < 2)
		return false;

	if(str[0] != '"' || str[lng - 1] != '"')
		return false;

	return true;
}

//-------------------------------------------------------------------------------
CResult LSError2CResult(LSError error)
{
	CResult res;

	switch(error)
	{
	case LSOK:
		res.SetResult(true);
		break;
	case LSFileNotFound:
		res.Set(true, "no such file");
		break;
	case LSBadFile:
		res.Set(false, "error in loaded file (CRC error...)");
		break;
	case LSStructure:
		res.Set(false, "fire structure error");
		break;
	case LSUnsupportedFormat:
		res.Set(false, "attempt to load other file format");
		break;
	case LSVersionTooNew:
		res.Set(false, "attempt to load unknown version");
		break;
	case LSVersionTooOld:
		res.Set(false, "attempt to load version that is no longer supported");
		break;
	case LSDiskFull:
		res.Set(false, "cannot save - disk full");
		break;
	case LSAccessDenied:
		res.Set(false, "file is read only");
		break;
	case LSDiskError:
		res.Set(false, "some disk error");
		break;
	case LSNoEntry:
		res.Set(false, "entry in ParamArchive not found");
		break;
	case LSNoAddOn:
		res.Set(false, "ADDED in patch 1.01 - AddOns check");
		break;
	case LSUnknownError:
		res.Set(false, "unknown error");
		break;
	default:
		assert(false);
		res.Set(false, "unknown error");
	}
	return res;
}

//===============================================================================
// ParamConfigNode base class for PCVar, PCArray a PCClass
//===============================================================================
PCNode::PCNode(const char* name, PCNode* owner, const PCDefNode* definition)
{
	enf_assert(name);
	this->name = name;
	this->owner = owner;
	this->definition = definition;
	IsExternNode = false;
	WasExternNodeOnLoad = false;
}

//-------------------------------------------------------------------------------
PCNode::~PCNode()
{
}

//-------------------------------------------------------------------------------
void PCNode::SetExtern(bool IsExtern)
{
	this->IsExternNode = IsExtern;
}

//-------------------------------------------------------------------------------
void PCNode::SetExternOnLoad(bool IsExtern)
{
	this->WasExternNodeOnLoad = IsExtern;
}

//-------------------------------------------------------------------------------
const char* PCNode::GetName() const
{
	return name.c_str();
}

//-------------------------------------------------------------------------------
const PCDefNode* PCNode::GetDefinition()const 
{
	return definition;
}

//-------------------------------------------------------------------------------
void PCNode::SetDefinition(const PCDefNode* def)
{
	enf_assert(def);
	definition = def;
}

//-------------------------------------------------------------------------------
PCNode* PCNode::GetTopOwner()
{
	PCNode* own = owner;
	PCNode* result = owner;

	while(own)
	{
		result = own;
		own = own->GetOwner();
	}

	return result;
}

//-------------------------------------------------------------------------------
ParamConfig* PCNode::GetConfig()
{
	PCNode* TopOwner = GetTopOwner();

	if(TopOwner)
		return TopOwner->ToConfig();

	return NULL;
}

//-------------------------------------------------------------------------------
DString PCNode::GetLocation()
{
	DString res;
	PCNode* own = owner;
	EArray<PCNode*>nodes;

	while(own)
	{
		nodes.Insert(own);
		own = own->GetOwner();
	}

	for(int n = (int)nodes.GetCardinality() - 1; n >= 0; n-- )
	{
		own = nodes[n];
		res += own->GetName();

		if(n > 0)
			res += '/';
	}

	return res;
}

//-------------------------------------------------------------------------------
DString PCNode::GetId() const
{
	DString res;
	PCNode* own = owner;
	EArray<PCNode*>nodes;

	while(own)
	{
		nodes.Insert(own);
		own = own->GetOwner();
	}

	for(int n = (int)nodes.GetCardinality() - 1; n >= 0; n-- )
	{
		own = nodes[n];
		res += own->GetName();

		if(n > 0)
			res += '/';
	}

	if(!res.empty())
		res += '/';

	res += GetName();
	return res;
}

//-------------------------------------------------------------------------------
uint PCNode::GetPosInOwner(bool SkipOwnerClasses) const
{
	if(!owner)
		return INDEX_NOT_FOUND;

	PCClass* cl = owner->ToClass();

	if(cl)
	{
		if(SkipOwnerClasses)
		{
			uint counter = 0;
			for(uint n = 0; n < cl->GetNodesCount(); n++)
			{
				PCNode* node = cl->GetNode(n);
				PCVar* var = node->ToVariable();
				PCArray* arr = node->ToArray();

				if((var || arr) && !node->IsHidden())
				{
					if(node == this)
						return counter;

					counter++;
				}
			}
			return counter;
		}
		else
			return cl->GetIndexOf(this);
	}
	else
	{
		PCArray* arr = owner->ToArray();
		enf_assert(arr);
		return arr->GetIndexOf(this);
	}
}

//-------------------------------------------------------------------------------
void PCNode::SetName(const char* name)
{
	enf_assert(name);
	this->name = name;
}

//-------------------------------------------------------------------------------
bool PCNode::IsInsideArray() const
{
	return(GetOwner() && GetOwner()->ToArray());
}

//-------------------------------------------------------------------------------
PCClass* PCNode::GetFirstClassOwner() const
{
	PCNode* own = GetOwner();

	while(own)
	{
		PCClass* cl = own->ToClass();

		if(cl)
			return cl;

		own = own->GetOwner();
	}
	return NULL;
}

//===============================================================================
PCVar::PCVar(const char* name, PCNode* owner, const PCDefParam* definition)
	:PCNode(name, owner, definition)
{
}

//-------------------------------------------------------------------------------
PCVar::~PCVar()
{
}

//-------------------------------------------------------------------------------
const PCDefParam* PCVar::GetDefinition() const
{
	return (PCDefParam*)definition;
}

//-------------------------------------------------------------------------------
void PCVar::SetValue(const char* value)
{
	this->value = value;
}

//-------------------------------------------------------------------------------
const char*	PCVar::GetValue() const
{
	return value.c_str();
}

//-------------------------------------------------------------------------------
PCVar* PCVar::Clone(PCNode* owner) const
{
	PCVar* var = GetConfigPtr()->CreateVariable(GetName(), owner, GetDefinition());
	var->SetValue(GetValue());
	var->SetExtern(IsExtern());
	var->SetExternOnLoad(WasExternOnLoad());
	return var;
}

//-------------------------------------------------------------------------------
bool PCVar::IsHidden() const
{
	PCNode* own = GetOwner();
	enf_assert(own);

	if(own->IsHidden())
		return true;

	PCClass* ClOwn = own->ToClass();

	if(ClOwn)
	{
		const char* hidden = ClOwn->GetPropertyDefAttr(GetName(), "hidden");
		bool Hidden = hidden && (strcmpi(hidden, "true") == 0);

		if(Hidden)
			return true;
	}
	return false;
}

//-------------------------------------------------------------------------------
bool PCVar::IsReadOnly() const
{
	if(IsExtern())
		return true;

	PCNode* own = GetOwner();
	enf_assert(own);

	if(own->IsReadOnly())
		return true;

	return IsDefinedAsReadOnly();
}

//-------------------------------------------------------------------------------
bool PCVar::IsDefinedAsReadOnly() const
{
	PCNode* own = GetOwner();
	enf_assert(own);
	PCClass* ClOwn = own->ToClass();

	if(ClOwn)
	{
		const char* readonly = ClOwn->GetPropertyDefAttr(GetName(), "readonly");
		bool ReadOnly = readonly && (strcmpi(readonly, "true") == 0);

		if(readonly)
			return true;
	}

	return false;
}

//-------------------------------------------------------------------------------
bool PCVar::IsEqual(PCVar* other)
{
	if(!other)
		return false;

	return ( strcmpi(GetValue(), other->GetValue()) == 0);
}

//===============================================================================
PCArray::PCArray(const char* name, PCNode* owner, const PCDefParam* definition)
	:PCNode(name, owner, definition)
{
}

//-------------------------------------------------------------------------------
PCArray::~PCArray()
{
	Clear();
}

//-------------------------------------------------------------------------------
const PCDefParam* PCArray::GetDefinition() const
{
	return (PCDefParam*)definition;
}

//-------------------------------------------------------------------------------
bool PCArray::Add(PCNode* value)
{
	enf_assert(value);
	return (values.Insert(value) != INDEX_NOT_FOUND);
}

//-------------------------------------------------------------------------------
bool PCArray::Insert(PCNode* value, uint pos)
{
	enf_assert(value);
	enf_assert(pos != INDEX_NOT_FOUND);
	uint res = values.InsertAt(value, pos);
	return (res != INDEX_NOT_FOUND);
}

//-------------------------------------------------------------------------------
bool PCArray::Remove(PCNode* value)
{
	enf_assert(value);
	uint index = values.GetIndexOf(value);

	if(index == INDEX_NOT_FOUND)
		return false;

	values.RemoveElementOrdered(index);
	return true;
}

//-------------------------------------------------------------------------------
uint PCArray::GetCardinality() const
{
	return values.GetCardinality();
}

//-------------------------------------------------------------------------------
PCNode* PCArray::GetItem(uint n) const
{
	return values[n];
}

//-------------------------------------------------------------------------------
PCNode* PCArray::GetMember(const char* name) const
{
	for(uint n = 0; n < values.GetCardinality(); n++)
	{
		PCNode* node = values[n];

		if(strcmpi(node->GetName(), name) == 0)
			return node;
	}
	return NULL;
}

//-------------------------------------------------------------------------------
PCVar* PCArray::GetVariableMemberFromValue(const char* value, uint* index)
{
	enf_assert(value);

	for(uint n = 0; n < values.GetCardinality(); n++)
	{
		PCNode* node = values[n];
		PCVar* var = node->ToVariable();

		if(!var)
			continue;

		if(strcmpi(var->GetValue(), value) == 0)
		{
			*index = n;
			return var;
		}
	}
	return NULL;
}

//-------------------------------------------------------------------------------
uint PCArray::GetIndexOf(const PCNode* member)
{
	enf_assert(member);
	return values.GetIndexOf((PCNode*)member);
}

//-------------------------------------------------------------------------------
PCArray* PCArray::Clone(PCNode* owner) const
{
	PCArray* var = GetConfigPtr()->CreateArray(GetName(), owner, GetDefinition());
	var->SetExtern(IsExtern());
	var->SetExternOnLoad(WasExternOnLoad());

	for(uint n = 0; n < GetCardinality(); n++)
	{
		PCNode* item = GetItem(n);
		var->Add(item->Clone(var));
	}
	return var;
}

//-------------------------------------------------------------------------------
void PCArray::Clear()
{
	for(uint n = 0; n < values.GetCardinality(); n++)
	{
		PCNode* node = values[n];
		delete node;
	}
	values.Clear();
}

//-------------------------------------------------------------------------------
bool PCArray::CanSaveInline() const
{
	if(GetCardinality() == 0)
		return true;

	for(uint n = 0; n < values.GetCardinality(); n++)
	{
		PCNode* node = values[n];
		
		if(node->ToArray())
			return false;
		else	//if any string value in array, do not inline save is possible (BIS ParamFile compactibility)
		{
			if(ScanString(node->ToVariable()->GetValue()))
				return false;
		}
	}

	return true;
}

//-------------------------------------------------------------------------------
bool PCArray::IsEqual(PCArray* other)
{
	if(!other)
		return false;

	if(values.GetCardinality() != other->values.GetCardinality())
		return false;

	for(uint n = 0; n < values.GetCardinality(); n++)
	{
		PCNode* node = values[n];
		PCNode* otherNode = other->values[n];
		bool nodeIsArray = (node->ToArray() != NULL);
		bool otherNodeIsArray = (otherNode->ToArray() != NULL);

		if(nodeIsArray != otherNodeIsArray)
			return false;

		if(nodeIsArray)
		{
			if(!node->ToArray()->IsEqual(otherNode->ToArray()))
				return false;
		}
		else	//node is variable
		{
			if(!node->ToVariable()->IsEqual(otherNode->ToVariable()))
				return false;
		}
	}

	return true;
}

//-------------------------------------------------------------------------------
bool PCArray::IsHidden() const
{
	PCNode* own = GetOwner();
	enf_assert(own);

	if(own->IsHidden())
		return true;

	PCClass* ClOwn = own->ToClass();

	if(ClOwn)
	{
		const char* hidden = ClOwn->GetPropertyDefAttr(GetName(), "hidden");
		bool Hidden = hidden && (strcmpi(hidden, "true") == 0);

		if(Hidden)
			return true;
	}
	return false;
}

//-------------------------------------------------------------------------------
bool PCArray::IsReadOnly() const
{
	if(IsExtern())
		return true;

	PCNode* own = GetOwner();
	enf_assert(own);

	if(own->IsReadOnly())
		return true;

	return IsDefinedAsReadOnly();
}

//-------------------------------------------------------------------------------
bool PCArray::IsDefinedAsReadOnly() const
{
	PCNode* own = GetOwner();
	enf_assert(own);
	PCClass* ClOwn = own->ToClass();

	if(ClOwn)
	{
		const char* readonly = ClOwn->GetPropertyDefAttr(GetName(), "readonly");
		bool ReadOnly = readonly && (strcmpi(readonly, "true") == 0);

		if(readonly)
			return true;
	}
	return false;
}

//===============================================================================
PCCommand::PCCommand(const char* name, PCNode* owner)
	:PCNode(name, owner, NULL)
{
}

//-------------------------------------------------------------------------------
PCCommand::~PCCommand()
{
}

//-------------------------------------------------------------------------------
const char* PCCommand::GetValue() const
{
	return value.c_str();
}

//-------------------------------------------------------------------------------
void PCCommand::SetValue(const char* value)
{
	this->value = value;
}

//-------------------------------------------------------------------------------
PCCommand* PCCommand::Clone(PCNode* owner) const
{
	PCCommand* cmd = GetConfigPtr()->CreateCommand(GetName(), owner);
	cmd->SetValue(GetValue());
	cmd->SetExtern(IsExtern());
	cmd->SetExternOnLoad(WasExternOnLoad());
	return cmd;
}

//-------------------------------------------------------------------------------
bool PCCommand::IsDefinedAsReadOnly() const
{
	return false;
}


//===============================================================================
PCClass::PCClass(const char* name, PCNode* owner, PCClass* base, bool defined, const PCDefClass* definition)
	:PCNode(name, owner, definition)
{
	SetBase(base);
	this->defined = defined;
	unused = false;
	MarkFlags = 0;
}

//-------------------------------------------------------------------------------
PCClass::~PCClass()
{
	Clear();
}

//-------------------------------------------------------------------------------
const PCDefClass* PCClass::GetDefinition() const
{
	return (const PCDefClass*)definition;
}

//-------------------------------------------------------------------------------
bool PCClass::AppendCommand(PCCommand* command)
{
	enf_assert(command);
	return (commands.Insert(command) != INDEX_NOT_FOUND);
}

//-----------------------------------------------------------------------
int __cdecl CompareCommands(const void* a, const void* b)
{
	PCCommand* cmd1 = *(PCCommand**)a;
	PCCommand* cmd2 = *(PCCommand**)b;
	PCNode* OwnerNode = cmd1->GetOwner();
	PCClass* owner = OwnerNode->ToClass();

	int cmp = strcmpi(cmd1->GetName(), cmd2->GetName());

	if(cmp != 0)
		return cmp;

	PCClass* cl1 = owner->FindClass(cmd1->GetValue(), true, true);
	PCClass* cl2 = owner->FindClass(cmd2->GetValue(), true, true);

	if(!cl1 && !cl2)
		return owner->GetCommandIndex(cmd1) - owner->GetCommandIndex(cmd2);

	if(cl1 && !cl2)
		return -1;

	if(!cl1 && cl2)
		return 1;

	int b1 = 0;
	PCClass* base1 = cl1->GetBase();
	while(base1)
	{
		b1++;
		base1 = base1->GetBase();
	}

	int b2 = 0;
	PCClass* base2 = cl2->GetBase();
	while(base2)
	{
		b2++;
		base2 = base2->GetBase();
	}

	int res = b2 - b1;

	if(res == 0)
		res = owner->GetCommandIndex(cmd1) - owner->GetCommandIndex(cmd2);

	return res;
}

//-------------------------------------------------------------------------------
void PCClass::SortCommands(bool recursive)	//sort commands for correct order for save
{
	if(commands.GetCardinality() > 0)
	{
		qsort(commands.GetArray(), commands.GetCardinality(), sizeof(PCCommand*), CompareCommands);
	}

	if(!recursive)
		return;

	for(uint n = 0; n < GetNodesCount(); n++)
	{
		PCNode* node = GetNode(n);
		PCClass* cl = node->ToClass();

		if(!cl)
			continue;

		cl->SortCommands(recursive);
	}
}

//-------------------------------------------------------------------------------
bool PCClass::DeleteCommand(const char* name, const char* value)
{
	enf_assert(name);
	enf_assert(value);
	for(uint n = 0; n < commands.GetCardinality(); n++)
	{
		PCCommand* cmd = commands[n];

		if(strcmpi(cmd->GetName(), name) == 0 && strcmpi(cmd->GetValue(), value) == 0)
		{
			commands.RemoveElement(n);
			delete cmd;
			return true;
		}
	}
	return false;
}

//-------------------------------------------------------------------------------
bool PCClass::AppendNode(PCNode* node)
{
	if(!node)
		int gg = 0;
	enf_assert(node);
	return (nodes.Insert(node) != INDEX_NOT_FOUND);
}

//-------------------------------------------------------------------------------
bool PCClass::Insert(PCNode* node, uint pos)
{
	enf_assert(node);
	enf_assert(pos != INDEX_NOT_FOUND);
	uint res = nodes.InsertAt(node, pos);
	return (res != INDEX_NOT_FOUND);
}

//-------------------------------------------------------------------------------
bool PCClass::RemoveNode(PCNode* node)
{
	enf_assert(node);
	uint index = nodes.GetIndexOf(node);

	if(index == INDEX_NOT_FOUND)
		return false;

	nodes.RemoveElementOrdered(index);
	return true;
}

//-------------------------------------------------------------------------------
uint PCClass::GetNodesCount() const
{
	return nodes.GetCardinality();
}

//-------------------------------------------------------------------------------
PCNode* PCClass::GetNode(uint n) const
{
	return nodes[n];
}

//-------------------------------------------------------------------------------
uint PCClass::GetCommandsCount() const
{
	return commands.GetCardinality();
}

//-------------------------------------------------------------------------------
PCCommand* PCClass::GetCommand(uint n) const
{
	return commands[n];
}

//-------------------------------------------------------------------------------
PCCommand* PCClass::FindCommand(const char* name, const char* value)
{
	enf_assert(name);
	enf_assert(value);

	for(uint n = 0; n < commands.GetCardinality(); n++)
	{
		PCCommand* cmd = commands[n];

		if(strcmpi(cmd->GetName(), name) == 0 && strcmpi(cmd->GetValue(), value) == 0)
			return cmd;
	}
	return NULL;
}

//-------------------------------------------------------------------------------
uint PCClass::GetCommandIndex(PCCommand* command)
{
	enf_assert(command);
	return commands.GetIndexOf(command);
}

//-------------------------------------------------------------------------------
PCClass* PCClass::GetOwner() const
{
	if(owner)
	{
		enf_assert(owner->ToClass());
		return owner->ToClass();
	}
	return NULL;
}

//-------------------------------------------------------------------------------
void PCClass::SetBase(PCClass* base)
{
	this->base = base;

	if(base)
		BaseID = base->GetId();
}

//-------------------------------------------------------------------------------
void PCClass::SetBaseID(const char* BaseID)
{
	enf_assert(BaseID);
	this->BaseID = BaseID;
}

//-------------------------------------------------------------------------------
PCNode* PCClass::GetMember(const char* name) const
{
	for(uint n = 0; n < nodes.GetCardinality(); n++)
	{
		PCNode* node = nodes[n];

		if(strcmpi(node->GetName(), name) == 0)
			return node;
	}
	return NULL;
}

//-------------------------------------------------------------------------------
uint PCClass::GetIndexOf(const PCNode* member)
{
	enf_assert(member);
	return nodes.GetIndexOf((PCNode*)member);
}

//-------------------------------------------------------------------------------
PCVar* PCClass::GetVarMember(const char* name) const
{
	for(uint n = 0; n < nodes.GetCardinality(); n++)
	{
		PCVar* node = nodes[n]->ToVariable();

		if(!node)
			continue;

		if(strcmpi(node->GetName(), name) == 0)
			return node;
	}
	return NULL;
}

//-------------------------------------------------------------------------------
PCArray* PCClass::GetArrayMember(const char* name) const
{
	for(uint n = 0; n < nodes.GetCardinality(); n++)
	{
		PCArray* node = nodes[n]->ToArray();

		if(!node)
			continue;

		if(strcmpi(node->GetName(), name) == 0)
			return node;
	}
	return NULL;
}

//-------------------------------------------------------------------------------
PCClass* PCClass::GetClassMember(const char* name) const
{
	enf_assert(name);

	for(uint n = 0; n < nodes.GetCardinality(); n++)
	{
		PCClass* node = nodes[n]->ToClass();

		if(!node)
			continue;

		if(strcmpi(node->GetName(), name) == 0)
			return node;
	}
	return NULL;
}

//-------------------------------------------------------------------------------
void PCClass::GetClassMembers(ESet<PCClass*>& target, bool recursive)
{
	for(uint n = 0; n < nodes.GetCardinality(); n++)
	{
		PCClass* node = nodes[n]->ToClass();

		if(!node)
			continue;

		PCClass* cl = node->ToClass();

		if(!cl)
			continue;

		target.Insert(cl);
		
		if(recursive)
			cl->GetClassMembers(target, recursive);
	}
}

//-------------------------------------------------------------------------------
uint PCClass::RemoveSameMembersIfAreInherited()
{
//	if(strcmpi(GetName(), "ClassC") == 0)
//		int gg = 0;

	PCClass* baseCl = GetBase();

	if(!baseCl)
		return 0;

	EArray<PCNode*> toDelete;

	for(uint n = 0; n < nodes.GetCardinality(); n++)
	{
		PCNode* node = nodes[n];

		if(node->ToClass())
			continue;	//ignore classes

		PCNode* inheritedNode = baseCl->GetValueNode(node->GetName());

		if(!inheritedNode)
			continue;	//nothing inherited, nothing to delete

		if(inheritedNode == node)
			continue;

		bool isEqual = false;
		if(node->ToVariable() && inheritedNode->ToVariable())
			isEqual = node->ToVariable()->IsEqual(inheritedNode->ToVariable());
		else if(node->ToArray() && inheritedNode->ToArray())
			isEqual = node->ToArray()->IsEqual(inheritedNode->ToArray());

		if(isEqual)
			toDelete.Insert(node);
	}

	for(uint n = 0; n < toDelete.GetCardinality(); n++)
	{
		PCNode* node = toDelete[n];
		RemoveNode(node);
		delete node;
	}
	return toDelete.GetCardinality();
}

//-------------------------------------------------------------------------------
PCNode* PCClass::GetValueNode(const char* name, PCClass* LastBase) const
{
	PCNode* node = NULL;
	const PCClass* cl = this;

	while(node == NULL && cl != NULL)
	{
		node = cl->GetMember(name);

		if(node)
			return node;

		if(LastBase && (cl == LastBase))
			return NULL;

		cl = cl->GetBase();
	}
	return NULL;
}

//-------------------------------------------------------------------------------
uint PCClass::GetPosInOwner(bool SkipOwnerClasses) const
{
	if(!owner)
		return INDEX_NOT_FOUND;

	PCClass* cl = owner->ToClass();

	if(SkipOwnerClasses)
	{
		uint counter = 0;
		for(uint n = 0; n < cl->GetNodesCount(); n++)
		{
			PCNode* node = cl->GetNode(n);
			PCClass* ClassNode = node->ToClass();

			if(ClassNode && !ClassNode->IsHidden())
			{
				if(ClassNode == this)
					return counter;

				counter++;
			}
		}
		return counter;
	}
	else
		return cl->GetIndexOf(this);
}

//-------------------------------------------------------------------------------
bool PCClass::IsChildOf(PCClass* cl, bool recursive) const
{
	enf_assert(cl);
	PCClass* own = GetOwner();
	
	while(own)
	{
		if(own == cl)
			return true;

		if(recursive)
			own = own->GetOwner();
		else
			own = NULL;
	}

	return false;
}

//-------------------------------------------------------------------------------
PCClass* PCClass::FindClass(const char* name, bool InBase, bool InOwner) const
{
	enf_assert(name);
	//finding begin in members, if NULL result finding continues to base class and if NULL result again, finging continues to parent class. all recursive up to top owner (to PramConfig class). this is compactible with orginal ParamFile from BIS
	PCClass* InherClass = GetClassMember(name);

	if(InherClass)
		return InherClass;

	if(InBase && GetBase())
	{
		InherClass = GetBase()->FindClass(name, true, false);

		if(InherClass)
			return InherClass;
	}

	if(InOwner && GetOwner())
	{
		InherClass = GetOwner()->FindClass(name, true, true);

		if(InherClass)
			return InherClass;
	}

	return NULL;
}

//-------------------------------------------------------------------------------
void PCClass::GetPossibleBaseClasses(EArray<PCClass*>& result) const
{
	PCClass* own = GetOwner();

	if(!own)
		return;

	for(uint n = 0; n < own->GetNodesCount(); n++)
	{
		PCNode* node = own->GetNode(n);
		PCClass* cl = node->ToClass();

		if(!cl)
			continue;

		if(cl == this)
			break;

		if(cl->ToConfig())
			continue;

		result.Insert(cl);
	}

	PCClass* base = own->GetBase();

	while(base)
	{
		for(uint n = 0; n < base->GetNodesCount(); n++)
		{
			PCNode* node = base->GetNode(n);
			PCClass* cl = node->ToClass();

			if(!cl)
				continue;

			result.Insert(cl);
		}

		base = base->GetBase();
	}
}

//-------------------------------------------------------------------------------
PCClass* PCClass::GetTopBase() const
{
	PCClass* res = NULL;
	PCClass* b = base;

	while(b)
	{
		res = b;
		b = b->GetBase();
	}
	return res;
}

//-------------------------------------------------------------------------------
int PCClass::GetNumBases() const
{
	PCClass* b = base;
	int result = 0;

	while(b)
	{
		result++;
		b = b->GetBase();
	}
	return result;
}

//-------------------------------------------------------------------------------
int PCClass::GetAbsorptionLevel() const
{
	PCClass* own = GetOwner();
	int result = 0;

	while(own)
	{
		result++;
		own = own->GetOwner();
	}
	return result;
}

//-------------------------------------------------------------------------------
PCClass* PCClass::CreateSameClass(const char* name, PCNode* owner, PCClass* base, bool defined, const PCDefClass* definition) const
{
	return ENF_NEW PCClass(name, owner, base, defined, definition);
}

//-------------------------------------------------------------------------------
PCClass* PCClass::Clone(PCNode* owner) const
{
	const DString& MyBaseID = GetBaseID();
	const char* MyBaseIDCStr = MyBaseID.empty() ? NULL : MyBaseID.c_str();

	PCClass* cl = CreateSameClass(GetName(), owner, NULL, IsDefined(), GetDefinition());
	cl->SetExtern(IsExtern());
	cl->SetExternOnLoad(WasExternOnLoad());
	cl->SetUnused(IsUnused());
	
	cl->SetBaseID(MyBaseID.c_str());

	for(uint n = 0; n < nodes.GetCardinality(); n++)
	{
		PCNode* node = nodes[n];
		cl->AppendNode(node->Clone(cl));
	}

	for(uint n = 0; n < commands.GetCardinality(); n++)
	{
		PCCommand* cmd = commands[n];
		cl->AppendCommand(cmd->Clone(cl));
	}

	return cl;
}

//-------------------------------------------------------------------------------
void PCClass::Clear()
{
	for(uint n = 0; n < nodes.GetCardinality(); n++)
	{
		PCNode* node = nodes[n];
		node->Clear();
		delete node;
	}
	nodes.Clear();

	for(uint n = 0; n < commands.GetCardinality(); n++)
	{
		PCCommand* cmd = commands[n];
		cmd->Clear();
		delete cmd;
	}
	commands.Clear();
}

//-------------------------------------------------------------------------------
bool PCClass::IsEmpty() const
{
	return (GetNodesCount() == 0);
}

//-------------------------------------------------------------------------------
void	PCClass::SetDefined(bool defined)
{
	this->defined = defined;
}

//-------------------------------------------------------------------------------
void	PCClass::SetUnused(bool unused)
{
	if(unused)
	{
		enf_assert(IsExternNode);
	}
	this->unused = unused;
}

//-------------------------------------------------------------------------------
bool PCClass::SetDeleted(bool deleted)
{
	enf_assert(IsExtern());	//only extern class can be marked as deleted

	if(!IsExtern())
		return false;

	PCClass* own = GetOwner();
	enf_assert(own);

	if(deleted)
	{
		enf_assert(own->FindCommand("delete", GetName()) == NULL);
		PCCommand* cmd = GetConfig()->CreateCommand("delete", own);
		cmd->SetValue(GetName());
		return own->AppendCommand(cmd);
	}

	bool res = own->DeleteCommand("delete", GetName());
	enf_assert(res);
	return res;
}

//-------------------------------------------------------------------------------
uint PCClass::GetAllInheritClasses(PCClass* base, EArray<PCClass*>& target, bool ExternClasses)
{
	enf_assert(base);
	bool InsertThis = ExternClasses ? true : !IsExtern();

	if(InsertThis && IsInheritedFrom(base) && target.GetIndexOf(this) == INDEX_NOT_FOUND)
		target.Insert(this);
	
	for(uint n = 0; n < nodes.GetCardinality(); n++)
	{
		PCNode* node = nodes[n];
		PCClass* cl = node->ToClass();

		if(!cl)
			continue;

		cl->GetAllInheritClasses(base, target, ExternClasses);
	}

	return target.GetCardinality();
}

//-------------------------------------------------------------------------------
void PCClass::GetDirectlyInheritedClasses(PCClass* base, EArray<PCClass*>& target) const
{
	if(GetBase() == base)
		target.Insert((PCClass*)this);

	for(uint n = 0; n < nodes.GetCardinality(); n++)
	{
		PCNode* node = nodes[n];
		PCClass* cl = node->ToClass();

		if(!cl)
			continue;

		cl->GetDirectlyInheritedClasses(base, target);
	}
}

//-------------------------------------------------------------------------------
int PCClass::IsInheritedFrom(PCClass* cl)
{
	enf_assert(cl);
	PCClass* b = GetBase();
	int level = 0;

	while(b)
	{
		level++;

		if(b == cl)
			return level;

		b = b->GetBase();
	}

	return 0;
}

//-------------------------------------------------------------------------------
int PCClass::IsInheritedFrom(const char* ClassName)
{
	enf_assert(ClassName);
	PCClass* b = GetBase();
	int level = 0;

	while(b)
	{
		level++;

		if(strcmpi(b->GetName(), ClassName) == 0)
			return level;

		b = b->GetBase();
	}

	return 0;
}

//-------------------------------------------------------------------------------
const char*	PCClass::GetPropertyDefAttr(const char* name, const char* attribute)
{
	PCNode* PropNode = GetMember(name);
	const char* attr = NULL;

	if(PropNode)
	{
		const PCDefNode* sdef = PropNode->GetDefinition();
		const PCDefParam* def = sdef->ToParamConst();
		enf_assert(def);
		attr = def->GetAttr(attribute);
	}

	if(attr)
		return attr;

	if(base)
		return base->GetPropertyDefAttr(name, attribute);

	const PCDefParam* GlobalDef = g_PConfigDef->GetGlobalParamDef(name);

	if(GlobalDef)
		return GlobalDef->GetAttr(attribute);

	return NULL;
}

//-------------------------------------------------------------------------------
const TiXmlElement* PCClass::GetPropertyValuesDef(const char* name)
{
	PCNode* PropNode = GetMember(name);
	const TiXmlElement* elm = NULL;

	if(PropNode)
	{
		const PCDefNode* sdef = PropNode->GetDefinition();
		const PCDefParam* def = sdef->ToParamConst();
		enf_assert(def);
		elm = def->GetValuesElement();
	}

	if(elm)
		return elm;

	if(base)
		return base->GetPropertyValuesDef(name);

	const PCDefParam* GlobalDef = g_PConfigDef->GetGlobalParamDef(name);

	if(GlobalDef)
		return GlobalDef->GetValuesElement();

	return NULL;
}

//-------------------------------------------------------------------------------
bool PCClass::IsDeleted() const
{
	PCClass* own = GetOwner();

	if(own)
	{
		if(own->FindCommand("delete", GetName()))
			return true;
	}
	return false;
}

//-------------------------------------------------------------------------------
bool PCClass::IsHidden() const
{
	if(IsUnused())
		return true;

	const PCDefClass* def = GetDefinition();
	enf_assert(def);
	return def->IsHidden();
}

//-------------------------------------------------------------------------------
bool PCClass::IsReadOnly() const
{
	if(IsExtern())
		return true;

	return IsDefinedAsReadOnly();
}

//-------------------------------------------------------------------------------
bool PCClass::IsDefinedAsReadOnly() const
{
	const PCDefClass* def = GetDefinition();
	enf_assert(def);
	return def->IsReadOnly();
}

//===============================================================================
ParamConfig::ParamConfig()
	:PCClass("file", NULL, NULL, NULL)
{
	g_ConfigPtr = this;
	ConfigDef = new ParamConfigDef();
	UniqueNameCounter = 0;
	defined = true;	//file class is defined always
}

//-------------------------------------------------------------------------------
ParamConfig::~ParamConfig()
{
	Clear();
	SAFE_DESTROY(ConfigDef);
	g_ConfigPtr = NULL;
}

//-------------------------------------------------------------------------------
bool ParamConfig::IsReadOnly() const
{
	return false;
}

//-------------------------------------------------------------------------------
ParamConfigDef* ParamConfig::GetDefinition() const
{
	return ConfigDef;
}

//-------------------------------------------------------------------------------
const DString& ParamConfig::GetFileName()
{
	return FileName;
}

//-------------------------------------------------------------------------------
DString ParamConfig::GetSaveFilename()
{
/*	char buf[256];
	if(taBeforeLastChar(FileName.c_str(), buf, "."))
	{
		strcat(buf, GetWorkFileExtension());
		return buf;
	}*/
	return FileName;
}
/*
//-------------------------------------------------------------------------------
const char* ParamConfig::GetWorkFileExtension()
{
	return ".cpp";
}*/

//-------------------------------------------------------------------------------
void ParamConfig::Clear()
{
	PCClass::Clear();
	ConfigDef->Clear();
	FileName.clear();
	UniqueNameCounter = 0;
}

//-------------------------------------------------------------------------------
bool ParamConfig::IsEmpty() const
{
	return PCClass::IsEmpty();
}

//-------------------------------------------------------------------------------
PCVarType ParamConfig::GetEntryType(const ParamEntryVal& entr)
{
	bool IsFloat = entr.IsFloatValue();
	bool IsInt = entr.IsIntValue();

	if(entr.IsFloatValue())
		return PCVT_FLOAT;
	else if(entr.IsIntValue())
		return PCVT_INT;

	assert(entr.IsTextValue());
	return PCVT_STRING;
}

//-------------------------------------------------------------------------------
PCVarType ParamConfig::GetArrayValueType(const IParamArrayValue& val)
{
	if(val.IsFloatValue())
		return PCVT_FLOAT;
	else if(val.IsIntValue())
		return PCVT_INT;

	assert(val.IsTextValue());
	return PCVT_STRING;
}

//-------------------------------------------------------------------------------
PCClass* ParamConfig::LoadClass(const ParamClass* source, bool IsRoot, PCClass* owner, PCClass* base, const PCDefClass* definition, bool SourceIsUpdated)
{
	PCClass* cl;
	RStringB ClassName = source->GetName();
	bool IsDefined;

  bool aa=source->IsForwardDeclInLastUpdateSource();
	if(SourceIsUpdated)
		IsDefined = (source->IsDefined() && !aa);
	else
		IsDefined = source->IsDefined();

	DString OwnerID;
	DString BaseID;
	const char* BaseIDCStr = NULL;
	
	if(base)
	{
		BaseID = base->GetId();
		BaseIDCStr = BaseID.c_str();
	}

	if(IsRoot)
		cl = this;
	else
	{
		OwnerID = owner->GetId();
		cl = CreateClass(ClassName.Data(), owner, OwnerID.c_str(), base, BaseIDCStr, IsDefined, definition, true, GetClassInfoFlags(owner, base));
	}

	for(uint n = 0; n < source->GetEntryCount(); n++)
	{
		ParamEntryVal entr = source->GetEntry(n);
		ParamEntry* EntrPtr = entr.GetModPointer();
		bool IsExternNode = (SourceIsUpdated && (!EntrPtr->IsUpdatedFromLastUpdateSource()));
		bool IsDeletedNode = EntrPtr->IsDeletedFromLastUpdateSource();
		RStringB EName = entr.GetName();
		const char* EntryName = EName.Data();
		PCClass* EntryOwner = cl;//IsRoot ? NULL : cl;

//		if(strcmpi("rscObject", EntryName) == 0)
//			int gg = 0;

		if(entr.IsClass())
		{
			const ParamClass* ClassInterf = entr.GetClassInterface();

			bool IsDelete = (ClassInterf->IsDelete());				//entr is not real class but only command to undefine previous defined class
//			bool IsError = ClassInterf->IsError();					//probably unused type. not available in config files
//			bool IsExpession = ClassInterf->IsExpression();		//probably unused type. not available in config files

			if(IsDelete || IsDeletedNode)	//node was normally deleted but in editor stay alive marked as deleted
			{
				PCCommand* cmd = CreateCommand("delete", EntryOwner);
				cmd->SetValue(EntryName);
				cl->AppendCommand(cmd);	//storing separately of members
			}

			if(!IsDelete)	//is real class and not only special "delete" command
			{
				const ParamClass* EntryBase = ClassInterf->FindBase().GetClass();
				const char* BaseName = ClassInterf->GetBaseName();

				PCClass* base = (BaseName && strlen(BaseName) > 0) ? cl->FindClass(BaseName, true, true) : NULL;
				PCClass* TopBase = base;

				if(TopBase)
				{
					PCClass* top = TopBase->GetTopBase();

					if(top)
						TopBase = top;
				}

				const PCDefClass* definition = NULL;

/*				if(TopBase)	//if have ancestor
				{
					definition = TopBase->GetDefinition();
				}
				else	//este nema definiciu tak ju vytvorime
				{*/
					DString DefPath;

					if(EntryOwner)
					{
						DefPath = EntryOwner->GetId();
						DefPath += '/';
					}

					DefPath += EntryName;
					definition = ConfigDef->GetClassDef(DefPath.c_str());	//do get for definition for this class. if not exist, wil be created empty default one
//				}

				enf_assert(definition);

				PCClass* NewClass = LoadClass(ClassInterf, false, EntryOwner, base, definition, SourceIsUpdated);
				NewClass->SetExtern(IsExternNode);
				NewClass->SetExternOnLoad(IsExternNode);

				cl->AppendNode(NewClass);
			}
		}
		else	//array or variable
		{
			//get defined or generate empty definition of this param
			PCDefParam* ParamDef = definition ? (PCDefParam*)definition->GetParamDef(EntryName) : NULL;
			assert(ParamDef);

			if(entr.IsArray())
			{
				assert(ParamDef);
				PCArray* arr = CreateArray(EntryName, EntryOwner, ParamDef);

				for(int i = 0; i < entr.GetSize(); i++)
				{
					const IParamArrayValue& val = entr[i];

/*					if(val.IsExpression())
					{
						const char* exp = val.GetValueRaw().Data();
						exp = exp;
					}*/

					if(val.IsArrayValue())	//moze byt pole v poli
					{
						DString UniID = GenerateUniqueName(UID_ARRAY);
						PCArray* subarr = LoadArray(val, UniID.c_str(), arr, NULL);
						arr->Add(subarr);
					}
					else
					{
						DString UniID = GenerateUniqueName(UID_VARIABLE);
						RStringB value = val.GetValueRaw();
						PCVar* var = LoadVariable(UniID.c_str(), value.Data(), GetArrayValueType(val), arr, NULL);
						arr->Add(var);
					}
				}

				arr->SetExtern(IsExternNode);
				arr->SetExternOnLoad(IsExternNode);
				cl->AppendNode(arr);
			}
			else	//variable
			{
				RStringB value = entr.GetValueRaw();

/*				if(ParamDef->IsGenerated())
				{
//					ParamDef->SetVarType(PCVT_STRING);
					ParamDef->SetDefaultValue(value.Data());
//					ParamDef->SetEditControl("editbox");
				}*/

				PCVar* var = LoadVariable(EntryName, value.Data(), GetEntryType(entr), EntryOwner, ParamDef);

				var->SetExtern(IsExternNode);
				var->SetExternOnLoad(IsExternNode);
				cl->AppendNode(var);
			}
		}
	}
	return cl;
}

//-------------------------------------------------------------------------------
PCArray*	ParamConfig::LoadArray(const IParamArrayValue& entr, const char* name, PCNode* owner, PCDefParam* definition)
{
	PCArray* arr = CreateArray(name, owner, definition);

	for(int i = 0; i < entr.GetItemCount(); i++)
	{
		const IParamArrayValue& val = entr[i];

		if(val.IsExpression())
		{
			const char* exp = val.GetValueRaw().Data();
			exp = exp;
		}

		if(val.IsArrayValue())
		{
			DString UniID = GenerateUniqueName(UID_ARRAY);
			PCArray* subarr = LoadArray(val, UniID.c_str(), arr, NULL);
			arr->Add(subarr);
		}
		else
		{
			DString UniID = GenerateUniqueName(UID_VARIABLE);
			RStringB value = val.GetValueRaw();
			PCVar* var = LoadVariable(UniID.c_str(), value.Data(), GetArrayValueType(val), arr, NULL);
			arr->Add(var);
		}
	}

	return arr;
}

//-------------------------------------------------------------------------------
PCVar* ParamConfig::LoadVariable(const char* name, const char* value, PCVarType type, PCNode* owner, PCDefParam* definition)
{
	PCVar* var = CreateVariable(name, owner, definition);

	if(type == PCVT_FLOAT)
		var->SetValue(value);
	else if(type == PCVT_STRING)
		var->SetValue(FormatStringValue(value));
	else
		var->SetValue(value);

	return var;
}

//-------------------------------------------------------------------------------
CResult ParamConfig::LoadSimpleConfig(ParamFile* PFile, const char* filename)
{
	enf_assert(PFile);
	enf_assert(filename);

	if(!taFileExists(filename))
	{
		DString msg("Cannot load config file ");
		msg += filename;
		msg += " file not exist";
		return CResult(false, msg.c_str());
	}

	wxString FileExtension = filename;
	FileExtension = FileExtension.AfterLast('.');
	CResult result;

	if(FileExtension.CmpNoCase("cpp") == 0)	//load source config files
	{
		result = LSError2CResult(PFile->Parse(filename));

		if(!result.IsOk())
			return result;
	}
	else if(FileExtension.CmpNoCase("bin") == 0)	//load from bin
	{
		result = PFile->ParseBin(filename);

		if(!result.IsOk())
		{
			wxString message("Cannot load bin config file ");
			message += filename;
			return CResult(false, message.c_str());
		}
	}
	else
	{
		wxString message("Unknown file type with extension ");
		message += FileExtension;
		return CResult(false, message.c_str());
	}
	return true;
}

//-------------------------------------------------------------------------------
CResult ParamConfig::Load(EArray<PCFilename>& FileNames)
{
	Clear();
	int LastFileIndex = (int)FileNames.GetCardinality() - 1;
	enf_assert(LastFileIndex >= 0);

	if(LastFileIndex < 0)
		return CResult(false, "No config to load");

	const PCFilename& FileName = FileNames[LastFileIndex];	//last config is editable config
	const char* filename = FileName.GetFilename().c_str();
	wxString ConfigFile = filename;
	wxString FileExtension = ConfigFile.AfterLast('.');
	ConfigFile = ConfigFile.BeforeLast('.');
	ConfigFile += "_def.xml";

	bool CreatedDeafultConfigDef = false;
	CResult res = ConfigDef->Load(ConfigFile.c_str(), &CreatedDeafultConfigDef);

//	if(res == false)
//		return res;

	ParamFile* PFile = new ParamFile();

	for(uint n = 0; n < FileNames.GetCardinality(); n++)
	{
		const PCFilename& fname = FileNames[n];

		if(n == 0)
		{
			res = LoadSimpleConfig(PFile, fname.GetFilename().c_str());

			if(!res.IsOk())
				break;
		}
		else
		{
			ParamFile* PFileTemp = new ParamFile();
			
			res = LoadSimpleConfig(PFileTemp, fname.GetFilename().c_str());

			if(!res.IsOk())
			{
				SAFE_DELETE(PFileTemp);
				break;
			}

			bool IsLastUpdate = (n == LastFileIndex);
			bool updated = PFile->Update(*PFileTemp, fname.ProtectByUpdate(), fname.CanUpdateBaseByUpdate(), IsLastUpdate);

/*			if(!updated)
			{
//				updated is always false if not all entries of config is overloaded. this is normal situation
				SAFE_DELETE(PFileTemp);
				wxString message("Cannot update config with file ");
				message += fname.GetFilename().c_str();
				res.Set(false, message.c_str());
				break;
			}*/

			SAFE_DELETE(PFileTemp);
		}
	}

	if(res == false)
	{
		SAFE_DELETE(PFile);

		if(CreatedDeafultConfigDef)
			wxRemoveFile(ConfigFile);

		return res;
	}

	bool LoadedWithExternConfigs = (FileNames.GetCardinality() > 1);
	const ParamClass* cl = PFile->GetRoot();
	PCClass* loaded = LoadClass(cl, true, NULL, NULL, ConfigDef, LoadedWithExternConfigs);
	assert(loaded == this);

	if(loaded)
		SetFileName(filename);

	SAFE_DELETE(PFile);
	return res;
}

//-------------------------------------------------------------------------------
CResult ParamConfig::Save(const char* FileName)
{
	this->SortCommands(true);	//sort all commands before save for correct order

	wxString FileExtension(FileName);
	FileExtension = FileExtension.AfterLast('.');

	if(FileExtension.CmpNoCase("cpp") == 0)
	{
		TextParser parser;
		TxtFile* file = new TxtFile(&parser);
		SaveClass(this, file, 0);

		CResult result = file->Save(FileName);

		if(result.IsOk())
			SetFileName(FileName);
		else
		{
			DString comment("Cannot save config to file ");
			comment += FileName;
			comment += "\nFile is read only or in use by another aplication probably.";
			result.SetComment(comment.c_str());
		}

		SAFE_DELETE(file);
		return result;
	}
	else if(FileExtension.CmpNoCase("bin") == 0)
	{
		TextParser parser;
		TxtFile* file = new TxtFile(&parser);
		SaveClass(this, file, 0);

		char AppDir[256];
		taGetAplicationDir(AppDir);
		wxString TempFile(AppDir);
		TempFile += "/TempConfigFile.cpp";
		bool TempFileSaved = file->Save(TempFile.c_str());
		SAFE_DELETE(file);

		if(!TempFileSaved)
		{
			wxString msg("cannot save temp file ");
			msg += TempFile;
			return CResult(false, msg.c_str());
		}

		ParamFile* PFile = new ParamFile();
		LSError res = PFile->Parse(TempFile.c_str());

		if(res != LSOK)
		{
			SAFE_DELETE(PFile);
			wxRemoveFile(TempFile);
			return LSError2CResult(res);	//return reason of Parse() fail
		}

		if(!PFile->SaveBin(FileName))
		{
			SAFE_DELETE(PFile);
			wxRemoveFile(TempFile);
			wxString msg("cannot save binarized file ");
			msg += FileName;
			return CResult(false, msg);
		}

		SAFE_DELETE(PFile);
		wxRemoveFile(TempFile);
		SetFileName(FileName);
		return true;
	}

	DString comment("Cannot save config to file ");
	comment += FileName;
	return CResult(false, comment.c_str());
}

//-------------------------------------------------------------------------------
void ParamConfig::SaveClass(const PCClass* cl, TxtFile* file, int NumTabs)
{
	DString line;

	DString tabs;
	DString ScopeTabs;
	AddTabs(tabs, NumTabs);
	AddTabs(ScopeTabs, NumTabs + 1);

	for(uint n = 0; n < cl->GetCommandsCount(); n++)
	{
		PCCommand* cmd = cl->GetCommand(n);
		line = tabs;
		line += cmd->GetName();
		line += ' ';
		line += cmd->GetValue();
		line += ';';
		file->AddLine(line.c_str());
	}

	for(uint n = 0; n < cl->GetNodesCount(); n++)
	{
		PCNode* node = cl->GetNode(n);

		if(node->IsExtern())
			continue;

		if(node->ToVariable())
		{
			line = tabs;
			SaveVariable(node->ToVariable(), line);
			line += ';';
			file->AddLine(line.c_str());
		}
		else if(node->ToArray())
		{
			PCArray* Array = node->ToArray();
			SaveArray(Array, file, NumTabs, 0);
		}
		else if(node->ToClass())	//is class
		{
			PCClass* Class = node->ToClass();
			assert(Class);
			PCClass* base = Class->GetBase();

			line = tabs;
			line += "class ";
			line += Class->GetName();

			if(Class->IsDefined())	//normal class
			{
				if(base)
				{
					line += ": ";
					line += base->GetName();
				}

				file->AddLine(line.c_str()); line.clear();
				
				line = tabs;
				line += '{';
				file->AddLine(line.c_str()); line.clear();

				SaveClass(Class, file, NumTabs + 1);

				line = tabs;
				line += "};";
			}
			else	//is a forward declaration only
			{
				line += ";";
			}

			file->AddLine(line.c_str());
		}
	}
}

//-------------------------------------------------------------------------------
void ParamConfig::SaveArray(const PCArray* arr, TxtFile* file, int NumTabs, int InOwnerArray)
{
	DString line;
	DString tabs;
	DString ArrTabs;
	AddTabs(tabs, NumTabs);
	AddTabs(ArrTabs, NumTabs + 1);
	bool InsideAnotherArray = (InOwnerArray > 0);
	bool IsLastInsideAnotherArray = (InOwnerArray == 2);
	bool InlineSave = arr->CanSaveInline();

	if(InlineSave)
	{
		line = tabs;

		if(InsideAnotherArray == false)
		{
			line += arr->GetName();
			line += "[]={";
		}
		else
			line += '{';

		int LastIndex = (int)arr->GetCardinality() - 1;

		for(uint a = 0; a < arr->GetCardinality(); a++)
		{
			PCNode* node = arr->GetItem(a);

			if(node->ToArray())
			{
				PCArray* Array = node->ToArray();
				SaveArray(Array, file, 0, (a == LastIndex) ? 2 : 1);
			}
			else
			{
				PCVar* var = node->ToVariable();
				assert(var);

				SaveVariable(var, line);

				if(a < LastIndex)
					line += ',';
			}
		}
	}
	else
	{
		if(InsideAnotherArray == false)
		{
			line = tabs;
			line += arr->GetName();
			line += "[]=";
			file->AddLine(line.c_str());
		}
		else
		{
			file->AddLine(tabs.c_str());
		}
		
		line = tabs;
		line += '{';
		file->AddLine(line.c_str());

		int LastIndex = (int)arr->GetCardinality() - 1;
		int ScopeTabs = NumTabs + 1;

		for(uint a = 0; a < arr->GetCardinality(); a++)
		{
			PCNode* node = arr->GetItem(a);

			if(node->ToArray())
			{
				PCArray* Array = node->ToArray();
				SaveArray(Array, file, ScopeTabs, (a == LastIndex) ? 2 : 1);
			}
			else
			{
				PCVar* var = node->ToVariable();
				assert(var);

				line = ArrTabs;
				SaveVariable(var, line);

				if(a < LastIndex)
					line += ',';

				file->AddLine(line.c_str());
			}
		}

		line = tabs;
	}

	if(InsideAnotherArray)
	{
		if(IsLastInsideAnotherArray)
			line += '}';
		else
			line += "},";
	}
	else
		line += "};";

	file->AddLine(line.c_str());
}

//-------------------------------------------------------------------------------
void ParamConfig::SaveVariable(const PCVar* var, DString& line)
{
	const PCDefParam* def = var->GetDefinition();	//if var owner is another array then definition is NULL
	const char* val = var->GetValue();
	int ValLng = strlen(val);

	if(var->IsInsideArray() == false)	//if var owner is another array then will be saved only value
	{
		line += var->GetName();
		line += '=';
	}

	if(ValLng == 0)
	{
		line += '"';
		line += '"';
	}
	else
	{
		PCVarType type = String2VarType(GetPropertyDefAttr(var->GetName(), "type"));
//		PCVarType type = def ? def->GetVarType() : PCVT_UNKNOWN;

		if(type == PCVT_UNKNOWN)	//if type is not defined, need scan for it
		{
			bool ok;
			int i = ScanInt(val, ok);

			if(!ok)
			{
				float f = ScanFloat(val, ok);

				if(ok)
					type = PCVT_FLOAT;
				else if(ScanString(val))
					type = PCVT_STRING;
			}
		}

		if(type == PCVT_FLOAT)
		{
			bool ok;
			float f = ScanFloat(val, ok);
			enf_assert(ok);
			sprintf(TemporalBuffer, "%f", f);
			line += TemporalBuffer;
		}
		else if(type == PCVT_STRING)
		{
			bool IsQuoted = IsQuotedString(val);	//string value can be quoted because of user input quotes

			if(!IsQuoted)
				line += '"';

			int ValLng = strlen(val);
			int LastIndex = ValLng - 1;
			int BufferPos = 0;

			for(int n = 0; n < ValLng; n++)	//quotes in string need save 2x because of meaning of this is string in string
			{
				char ch = val[n];
				TemporalBuffer[BufferPos++] = ch;

				if(ch == '"')
				{
					if(!IsQuoted || (n > 0 && n < LastIndex))
						TemporalBuffer[BufferPos++] = ch;
				}
			}

			TemporalBuffer[BufferPos] = 0;
			line += TemporalBuffer;

			if(!IsQuoted)
				line += '"';
		}
		else
			line += val;
	}
}

//-------------------------------------------------------------------------------
void ParamConfig::SetFileName(const char* FileName)
{
	enf_assert(FileName);
	this->FileName = FileName;
}

//-------------------------------------------------------------------------------
PCNode* ParamConfig::FindNode(const char* id)
{
	PCPath path(id);
	PCNode* current = NULL;
	DString level = path.GetFirstLevel();

	enf_assert(!level.empty());

	if(strcmpi(level.c_str(), GetName()) == 0)
		current = this;
	else
		return NULL;	//id path must begin with name of param config. (this name is always "file")

	for(DString level = path.GetNextLevel(); !level.empty(); level = path.GetNextLevel())
	{
		PCNode* member = NULL;
		PCClass* cl = current->ToClass();

		if(cl)
		{
			member = cl->GetMember(level.c_str());
		}
		else
		{
			PCArray* arr = current->ToArray();
			assert(arr);
			member = arr->GetMember(level.c_str());
		}

		if(!member)
			return NULL;

		current = member;
	}

	assert(current);
	return current;
}

//-------------------------------------------------------------------------------
PCClass* ParamConfig::FindClassNode(const char* id)
{
	PCNode* node = FindNode(id);
	return (node != NULL) ? node->ToClass() : NULL;
}

//-------------------------------------------------------------------------------
DString ParamConfig::GenerateUniqueName(UniIdPrefix prefix)
{
	const char* prf;

	if(prefix == UID_ARRAY)
		prf = "array";
	else	//UID_VARIABLE
		prf = "ArrayVariable";

	char UniNumber[64];
	taItoA(UniNumber, ++UniqueNameCounter);

	DString ID(prf);
	ID += '_';
	ID += UniNumber;
	return ID;
}

//-------------------------------------------------------------------------------
void ParamConfig::UpdateClassBasePtr(PCClass* cl, bool recursive)
{
	if(!cl->GetBaseID().empty())
	{
		PCNode* BaseNode = FindNode(cl->GetBaseID().c_str());
		enf_assert(BaseNode);
		PCClass* ClassBase = BaseNode->ToClass();
		enf_assert(ClassBase);
		cl->SetBase(ClassBase);
	}

	if(!recursive)
		return;

	for(uint n = 0; n < cl->GetNodesCount(); n++)
	{
		PCNode* node = cl->GetNode(n);
		PCClass* c = node->ToClass();

		if(!c)
			continue;

		UpdateClassBasePtr(c, recursive);
	}
}

//-------------------------------------------------------------------------------
void ParamConfig::UpdateClassBaseID(PCClass* cl, bool recursive)
{
	PCClass* base = cl->GetBase();

	if(base)
	{
		enf_assert(!cl->GetBaseID().empty());
		DString BaseID = base->GetId();
		cl->SetBaseID(BaseID.c_str());
	}

	if(!recursive)
		return;

	for(uint n = 0; n < cl->GetNodesCount(); n++)
	{
		PCNode* node = cl->GetNode(n);
		PCClass* c = node->ToClass();

		if(!c)
			continue;

		UpdateClassBaseID(c, recursive);
	}
}

//-------------------------------------------------------------------------------
PCClass* ParamConfig::CreateClass(const char* name, PCNode* owner, const char* OwnerID, PCClass* base, const char* BaseID, bool defined, const PCDefClass* definition, bool FromLoading, int ClassInfoFlags)
{
	return ENF_NEW PCClass(name, owner, base, defined, definition);
}

//-------------------------------------------------------------------------------
PCArray* ParamConfig::CreateArray(const char* name, PCNode* owner, const PCDefParam* definition)
{
	return ENF_NEW PCArray(name, owner, definition);
}

//-------------------------------------------------------------------------------
PCVar* ParamConfig::CreateVariable(const char* name, PCNode* owner, const PCDefParam* definition)
{
	return ENF_NEW PCVar(name, owner, definition);
}

//-------------------------------------------------------------------------------
PCCommand* ParamConfig::CreateCommand(const char* name, PCNode* owner)
{
	return ENF_NEW PCCommand(name, owner);
}