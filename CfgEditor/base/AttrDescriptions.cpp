#include "precomp.h"
#include "AttrDescriptions.h"
#include "base/MainFrame.h"
#include "tinyxml/tinyxml.h"

AttrDescriptions::AttrDescriptions(const char* file)
{
	LoadXmlFile(file);
}

AttrDescriptions::~AttrDescriptions()
{
}

wxString AttrDescriptions::GetDescriptionText(const wxString &name, const wxString &className)
{	
	TItems *items = &descItems;

	if(className != "")
	{
		TClassItems::iterator classIt = classDescItems.find(className);
		if(classIt != classDescItems.end())
			items = &(classIt->second.descItems);
	}

	if(!items)
		return wxString("Items are NULL");

	TItems::iterator it = items->find(name);
	if(it == items->end())
		return wxString("No description found.");

	DescItem &item = it->second;
	wxString description;

	for(int i=0;i<item.params.GetCardinality();++i)
	{
		description += item.params[i].first + ": " + item.params[i].second + " \n"; 
	}

	return description;
}


void AttrDescriptions::LoadXmlFile(const char* file)
{
	TiXmlDocument doc(file);
	if(!doc.LoadFile())
	{
		wxString msg = "Can't load file: ";
		msg += file;
		wxMessageDialog dialog(g_MainFrame, msg ,"XML AttrDescriptions", wxICON_ERROR | wxOK);
		dialog.CentreOnParent();
		dialog.ShowModal();
		return;
	}

	if(doc.Type() != TiXmlNode::DOCUMENT)
		return;

	TiXmlElement* descriptions = doc.FirstChildElement();

	if(!descriptions || strcmpi(descriptions->Value(), "descriptions") != 0)
		return;

	for(TiXmlElement* elem = descriptions->FirstChildElement(); elem != NULL; elem = elem->NextSiblingElement())
	{
		if(elem->Type() != TiXmlNode::ELEMENT)
			continue;

		if(strcmpi(elem->Value(), "common") == 0)
			LoadCommonDescriptions(elem);
		else
		if(strcmpi(elem->Value(), "class") == 0)
			LoadClassDescriptions(elem);
	}

}

void AttrDescriptions::LoadCommonDescriptions(const TiXmlElement *commonElement)
{
	if(!commonElement || strcmpi(commonElement->Value(), "common") != 0)
		return;

	LoadDescriptions(commonElement, descItems);
}

void AttrDescriptions::LoadClassDescriptions(const TiXmlElement *classElement)
{
	if(!classElement || strcmpi(classElement->Value(), "class") != 0)
		return;

	ClassDescriptions classDesc;
	classDesc.className = classElement->Attribute("name"); 

	LoadDescriptions(classElement, classDesc.descItems);

	classDescItems[classDesc.className] = classDesc;
}

void AttrDescriptions::LoadDescriptions(const TiXmlElement* parentElement, TItems &items)
{
	if(!parentElement)
		return;

	for(const TiXmlElement* descr = parentElement->FirstChildElement(); descr != NULL; descr = descr->NextSiblingElement())
	{
		const TiXmlAttribute* descriptionName = descr->FirstAttribute();
		if(descriptionName == NULL)
			continue;

		DescItem newItem;
		newItem.name = descriptionName->Value();

		for(const TiXmlElement* parameters = descr->FirstChildElement(); parameters != NULL; parameters = parameters->NextSiblingElement())
		{

			wxString name = parameters->Value();
			wxString value = parameters->Attribute("value");

			newItem.params.Push(std::make_pair(name, value));
		}
		items[newItem.name] = newItem;
	}
}