#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "RefCount.hpp"

#ifndef STREAM_TOKENIZER
#define STREAM_TOKENIZER

using namespace std;
// We kick everything off from above the ASCII table, this should do.

enum TokenTypes
{
  TT_WORD = 256,     // Word as example _myVar1 or _mayVar, that is not has the deliminator of TT_UNKNOWN
  TT_STRING_LITERAL, // Defined as "myString"
  TT_INT_CONST,
  TT_FLOAT_CONST,
  TT_EOL,
  TT_EOF,
  TT_SOF
};

struct TokenizerRollBack
{
  const char *_pos;
  TokenizerRollBack(const char *pos):_pos(pos){}
};

class StreamTokenizer : public RefCount
{
  std::vector<TokenizerRollBack> _history;

  // This includes the null terminator
  std::string _rawData;
  const char *_stream;
protected:
  // We hold the current token value.
  std::string _stringVal;
private:
  void Init();
  TokenTypes RemoveControlCharAndSpace();
  void InitNextToken();

protected:
  // we take our data.
  StreamTokenizer(ifstream &inputFile);
  StreamTokenizer(const char *data);

  // Will cause Object to restart
  void Parse(ifstream &inputFile);

  // Will cause Object to restart
  void Parse(const char *data);

  // Roll back to the previous token.
  // this is easy, we pop the current one
  TokenTypes PreviousStreamToken();

  // Retrieve the next token based on the type.
  TokenTypes NextStreamToken();

public:
  const char *GetStartPos(){ return _rawData.data(); }
  const char *GetCurrentPos(){ return _stream; }

  int GetLineNumber()
  {
    int number = 0;
    const char *search = _rawData.data();

    while (search != _stream && search < (_rawData.data() + _rawData.length())) 
    {
      if (*search == '\n') number++;
      search++;
    } 

    return number;
  }
};

#endif