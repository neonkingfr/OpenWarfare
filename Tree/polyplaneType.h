#pragma once

class GameData;

namespace TreeEngine {
      // Polyplane Type
      static const int PT_NONE =1;
      static const int PT_BLOCK =2;
      static const int PT_BLOCKBEND =3;
      static const int PT_BLOCKNORM =4;
      static const int PT_BLOCKSIMPLE =5;
      static const int PT_BLOCKSIMPLE2 =6;
      static const int PT_BLOCKBEND2 =7;
      static const int PT_BLOCKNORM2 =8;
      static const int PT_UNIPLANE_X =9;
      static const int PT_UNIPLANE_Y =10;
      static const int PT_UNIPLANE_Z =11;
      static const int PT_NOTDEFINED =12;
      static const int PT_BUNCH =13;
      static const int PT_USER =14;    ///<User polyplane, see SPolyplaneType

    class CPolyplane;
    class CPlantType;
    class IPolyplaneFactory: public RefCount
    {
    public:
        virtual~IPolyplaneFactory() {}
        virtual CPolyplane *CreatePolyplane()=0;
    };


    struct SPolyplaneWithData;

    class SPolyplaneType {
        int polyplaneType;
        Ref<IPolyplaneFactory> userPolyplane;
        bool forced;
    public:  
        SPolyplaneType():polyplaneType(PT_NOTDEFINED),forced(false) {}
        explicit SPolyplaneType(int polyplaneType, bool forced = false):polyplaneType(polyplaneType),forced(forced) {}
        SPolyplaneType(const Ref<IPolyplaneFactory> &userPolyplane)
            :polyplaneType(PT_USER), userPolyplane(userPolyplane) {}
        explicit SPolyplaneType(GameValuePar polyType);

        Ref<SPolyplaneWithData> create(CPlantType &plant, int textureSize) const;
        int getType() const {return polyplaneType;}
        Ref<IPolyplaneFactory> getUserPolyFactory() const {return userPolyplane;}
        bool isForced() const {return forced;}

    };
}