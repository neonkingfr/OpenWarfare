#include "Polyplane.h"

namespace TreeEngine
{

  struct SFabricPlane {
    ///Vector of normal for the plane
    Vector3 _dir;
    ///distance of plane from center of polyplane (in direction)
    float _shift;
    ///true, whether use texture to map other side of plane
    bool _doubleSided;

    SFabricPlane(Vector3 dir, float shift, bool doubleSided)
        :_dir(dir),_shift(shift),_doubleSided(doubleSided) {}
    SFabricPlane() {}
    
    ClassIsMovable(SFabricPlane);
  };

  struct SFabricPlaneConfig {

      ///List of planes
      AutoArray<SFabricPlane> _planes;
      ///weight of geometry centere. 
      /** Origin will be rotated to have direction oriented to this centre (=1)
      or remains in original direction (=0). 
      */
       
      float _dirToFocus;
      float _centerToFocus;

      SFabricPlaneConfig():_dirToFocus(0),_centerToFocus(0) {}
      SFabricPlaneConfig(AutoArray<SFabricPlane> planes,float dirToFocus, float centerToFocus)
          :_planes(planes),_dirToFocus(dirToFocus),_centerToFocus(centerToFocus) {}

  };

  struct SFabricPlaneInfo {
      Vector3 up;
      Vector3 aside;
      Vector3 center;
      Vector3 correct;
      float width;
      float height;
      float depth;
      bool dblsided;
      ClassIsSimpleZeroed(SFabricPlaneInfo);
  };

  class CFabricPolyplane: public CPolyplane
  {
  protected:
    
    AutoArray<PlaneResources> _textures;

    SFabricPlaneConfig _config;
    AutoArray<SFabricPlaneInfo> _finalPlanes;
    Matrix4 _originAdjust;
    float _focusDist;
  public:
      CFabricPolyplane(SFabricPlaneConfig config):_config(config) {}

    void Render(CPrimitiveStream &PSBranch, CPrimitiveStream &PSPolyplane,
      CPrimitiveStream &PSStageInit, Matrix4Par Origin,   const CPointList &pointList,
      float SizeCoef, const DrawParameters &dp);
    void Draw(CPrimitiveStream &PSTriplane, Matrix4Par Origin, float NewSizeCoef);
    void UpdatePointList(Matrix4Par Origin, float NewSizeCoef, CPointList &pointList);
    virtual void Save(RString fileName, RString suffix);
    virtual int GetNumberOfTextures() {return _config._planes.Size();}
    virtual ComRef<IDirect3DTexture8> GetTexture(int index);
    virtual ComRef<IDirect3DTexture8> GetNormalTexture(int index);
    

    struct AdjustInfo {
        Matrix4 adjMx;
        Vector3 geometryFocus;
        
        AdjustInfo(const Matrix4 &adjMx,
                   const Vector3 &geometryFocus)
                   :adjMx(adjMx),geometryFocus(geometryFocus) {}
    };

    virtual AdjustInfo  adjustOrigin(const Matrix4 &src, const CPointList &points);
    virtual Vector3 adjustFocus(const Vector3 &center, const Vector3 &focus);
  
  };

  
}