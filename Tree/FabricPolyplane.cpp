#include "RVTreeCommon.h"
#include "FabricPolyplane.h"

namespace TreeEngine
{

    void CFabricPolyplane::Render(CPrimitiveStream &PSBranch, CPrimitiveStream &PSPolyplane,
        CPrimitiveStream &PSStageInit, Matrix4Par Origin,   const CPointList &pointList,
        float SizeCoef, const DrawParameters &dp) 
    {
        _finalPlanes.Reserve(_config._planes.Size());

        AdjustInfo nfo =  adjustOrigin(Origin,pointList);;
        Matrix4 adjOrig = nfo.adjMx;
        _originAdjust =   Origin.InverseGeneral() * adjOrig;

        SBoundingBox bbox;
        pointList.ComputeBoundingBox(adjOrig, bbox);
        ModifyNormals(PSBranch, PSPolyplane, pointList, bbox.GetVolume(), dp);
        
        Vector3 fcPt = adjustFocus(adjOrig * bbox.GetFocus(),nfo.geometryFocus);
        _focusDist = (fcPt - Origin.Position()).Size();


        for (int i=0;i<_config._planes.Size();i++)
        {

            const Vector3 &dir = _config._planes[i]._dir;            
            SFabricPlaneInfo finPlane;            
            if (dir.Z() > 0.7 || dir.Z()<-0.7) {
                Vector3 up = dir.CrossProduct(Vector3(1,0,0)).Normalized();
                Vector3 aside = up.CrossProduct(dir).Normalized();
                finPlane.up = up;
                finPlane.aside = aside;
            } else {
                Vector3 aside = dir.CrossProduct(Vector3(0,0,1)).Normalized();
                Vector3 up = dir.CrossProduct(aside).Normalized();
                finPlane.up = up;
                finPlane.aside = aside;
            }


            Matrix4 camera;
            camera.SetPosition(Vector3(0,0,0));
            camera.SetUpAndAside(finPlane.up,finPlane.aside);
            SBoundingBox bbox;
            camera = adjOrig * camera;            
            Matrix4 invCamera;
            pointList.ComputeBoundingBox(camera, bbox);
            Vector3 dim = bbox.GetDimension();
            finPlane.width = dim.X();
            finPlane.height = dim.Y();

            float shift = _config._planes[i]._shift;
            if (shift>1) {
                finPlane.depth = shift * dim.Z();
            } else if (shift >= 0.5f) {
                finPlane.depth = _focusDist + (dim.Z() - _focusDist) * (shift-0.5f)* 2.0f;
            } else if (shift > -0.5) {
                finPlane.depth = _focusDist * shift * 2.0f;
            } else  if (shift > -1) {
                finPlane.depth = -(_focusDist + (dim.Z() - _focusDist) * (shift+0.5f)* 2.0f);
            } else {
                finPlane.depth = shift * dim.Z();
            }


            finPlane.dblsided = _config._planes[i]._doubleSided;
            finPlane.center = bbox.GetFocus();
            _finalPlanes.Add(finPlane);
            
        }

        PSBranch.Prepare();
        PSPolyplane.Prepare();

        _textures.Resize(_finalPlanes.Size());
        for (int i=0;i<_finalPlanes.Size();i++)
        {
            const SFabricPlaneInfo &plane = _finalPlanes[i];


            Matrix4 camera;
            camera.SetPosition(Vector3(0,0,0));
            camera.SetUpAndAside(plane.up,plane.aside);
            camera = camera * Matrix4(MTranslation,plane.center);

            CPointList hull;
            PlaneResources txt;
            RenderPlane(PSBranch, PSPolyplane,
                        txt._p, txt._pNorm, pointList, adjOrig * camera,
                        plane.width, plane.height, dp, &txt._hull);

            RegisterTextureToStage(PSStageInit,i,txt._p,txt._pNorm);
            _textures[i] = txt;
        }
        
        _SizeCoef = SizeCoef;

    }
    void CFabricPolyplane::Draw(CPrimitiveStream &PSTriplane, Matrix4Par Origin, float newSizeCoef) {
        
        Matrix4 adjOrig =  Origin * _originAdjust;
        int stage=_baseStage;
        float drawSizeCoef = CalcScale(newSizeCoef);

        for (int i=0;i<_finalPlanes.Size();i++)
        {
            const SFabricPlaneInfo &plane = _finalPlanes[i];

            float width = plane.width * drawSizeCoef;
            float height = plane.height * drawSizeCoef;
            float depth = plane.depth * drawSizeCoef;      



            Matrix4 camera;
            Vector3 center = plane.center * drawSizeCoef;
            camera.SetPosition(Vector3(0,0,0));
            camera.SetUpAndAside(plane.up,plane.aside);
            camera = camera * Matrix4(MTranslation,Vector3(center.X(),center.Y(),depth));

            Matrix4 finMx = adjOrig * camera ;

            bool doubleSided = plane.dblsided;
            DrawPlane(stage + i, PSTriplane, 
                _textures[i]._p, _textures[i]._pNorm, finMx, width, height, 0, &_textures[i]._hull,doubleSided);
        }
    }

    void CFabricPolyplane::UpdatePointList(Matrix4Par Origin, 
        float newSizeCoef, CPointList &pointList) {

        Matrix4 adjOrig = Origin * _originAdjust;
        float drawSizeCoef = CalcScale(newSizeCoef);
        //Matrix4 polyTrn = adjOrig * Matrix4(MTranslation,_center * drawSizeCoef);
        for (int i=0;i<_finalPlanes.Size();i++)
        {

            const SFabricPlaneInfo &plane = _finalPlanes[i];

            float width = plane.width * drawSizeCoef;
            float height = plane.height * drawSizeCoef;
            float depth = plane.depth * drawSizeCoef;      


            Matrix4 camera;
            Vector3 center = plane.center * drawSizeCoef;
            camera.SetPosition(Vector3(0,0,0));
            camera.SetUpAndAside(plane.up,plane.aside);
            camera = camera * Matrix4(MTranslation,Vector3(center.X(),center.Y(),
                depth+_focusDist));

            Matrix4 finMx = adjOrig * camera ;
            pointList.AddPoint(finMx * Vector3(width*0.5f,height*0.5,0));
            pointList.AddPoint(finMx * Vector3(width*0.5f,height*-0.5,0));
            pointList.AddPoint(finMx * Vector3(width*-0.5f,height*-0.5,0));
            pointList.AddPoint(finMx * Vector3(width*-0.5f,height*0.5,0));
        }
    }


  void CFabricPolyplane::Save(RString fileName, RString suffix) {
      for (int i=0;i<_textures.Size();i++) {
          char buff[10];
          sprintf_s(buff,"_%c",i+'A');
          RString base = fileName + buff + suffix;
          SaveTexture(base + ".tga", _textures[i]._p, false);
          SaveTexture(base + "_NO.tga", _textures[i]._pNorm, true);
      }
  }

  ComRef<IDirect3DTexture8> CFabricPolyplane::GetTexture(int index) {
      return _textures[index]._p;
  }
  ComRef<IDirect3DTexture8> CFabricPolyplane::GetNormalTexture(int index) {
      return _textures[index]._pNorm;
  }


  CFabricPolyplane::AdjustInfo  CFabricPolyplane::adjustOrigin(const Matrix4 &src, const CPointList &points) {

      Vector3 dir = src.Direction();

      Vector3 focus = src.Position();
      if (points.ComputeFocus(focus)) {

          Vector3 newDir = (focus - src.Position()).Normalized();
          if (newDir.IsFinite()) {
              
              Vector3 resDir = (dir + ( newDir - dir) * _config._dirToFocus).Normalized();
              if (resDir.IsFinite()) {
                Matrix4 res;
                res.SetDirectionAndUp(resDir,src.DirectionUp());
                res.SetDirectionAndAside(resDir,Vector3(res.DirectionAside()));
                res.SetPosition(src.Position());
                return AdjustInfo(res,focus);
              }
          }
      }
      return AdjustInfo(src,focus);
  }

  Vector3 CFabricPolyplane::adjustFocus(const Vector3 &center, const Vector3 &focus) {
        return center + (focus - center) * _config._centerToFocus;
  }
}

