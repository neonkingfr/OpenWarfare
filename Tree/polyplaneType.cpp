#include "RVTreeCommon.h"
#include "polyplaneType.h"
#include "polyplane.h"
#include "FabricPolyplane.h"
#include "mytree.h"

namespace TreeEngine {
    Ref<SPolyplaneWithData> SPolyplaneType::create(CPlantType &plant,int textureSize) const {

        if (polyplaneType != PT_USER) {
            return plant.CreatePolyplaneByType(polyplaneType,textureSize);
        } else if (userPolyplane.NotNull()) {
            Ref<SPolyplaneWithData> polyplaneWithData = new SPolyplaneWithData();
            CPolyplane *p=userPolyplane->CreatePolyplane();
            if (!p)  return 0;
            polyplaneWithData->_pPolyplane=p;

            // Initialize the polyplane
            if (FAILED(polyplaneWithData->_pPolyplane->Init(plant._texFactory,textureSize)))
            {
                LogF("Failed to initialize a polyplane.");
                return 0;
            }
            return polyplaneWithData;
        } 
        return 0;
    }

    IPolyplaneFactory *parseFabricPolyplane(const GameArrayType &arr) {

        //["@fabricPolyplane@",centerWeight,[x,y,z,shift,dblsided]]

        if (arr.Size()<4) {
            LogF("Fabric polyplane definition error: Definition need at least 4 fields [\"className\",dirToFocus,centerToFocus,[planeDef],...]");
            return 0;
        }

        if (arr[1].GetType() != GameScalar) {
            LogF("Fabric polyplane definition error: Second field must be scalar 'dirToFocus'");
            return 0;
        }
        if (arr[2].GetType() != GameScalar) {
            LogF("Fabric polyplane definition error: Third field must be scalar 'centerToFocus'");
            return 0;
        }
        SFabricPlaneConfig cfg;
        cfg._dirToFocus = arr[1];
        cfg._centerToFocus = arr[2];

        for (int i = 3; i < arr.Size(); i++) {
            if (arr[i].GetType() == GameArray) {
                const GameArrayType &vc = arr[i];
                if (vc.Size() > 2) {
                    float x = vc[0];
                    float y = vc[1];
                    float z = vc[2];
                    float shift = 0;
                    if (vc.Size() > 3) shift = vc[3];
                    bool dblsided = true;
                    if (vc.Size() > 4) {
                        if (toInt(vc[4]) == 1) dblsided = false;
                        else if (toInt(vc[4]) == 2) dblsided = true;
                        else LogF("Fabric polyplane: dblsided field is not valid in field %d", i);
                    }
                    cfg._planes.Add(
                        SFabricPlane(Vector3(x,y,z),shift,dblsided));
                }
            } else {
                LogF("Fabric polyplane definition error: %dth field must be Array'",i);
                return 0;
            }
        }

    

        class FabricFactory: public IPolyplaneFactory {
        public:
            SFabricPlaneConfig cfg;
            FabricFactory(SFabricPlaneConfig cfg):cfg(cfg) {}

            virtual CPolyplane *CreatePolyplane() {
                return new CFabricPolyplane(cfg);
            }
        };
        return new FabricFactory(cfg);

    }

    SPolyplaneType::SPolyplaneType(GameValuePar polyType):forced(false) {

        if (polyType.GetType() == GameScalar) {
            int id = toInt(polyType);
            if (id < 0) {
                forced = true; id = -id;
            }
            polyplaneType = id;
        } else if (polyType.GetType() == GameArray) {
            polyplaneType = PT_NONE;

            const GameArrayType &arr = polyType;
            
            if (arr.Size() <1) {
                LogF("Unknown polyplane type (empty definition)");
                return;
            }

            RStringI cmd = (RString)arr[0];
            if (cmd == RStringI("@fabricPolyplane@"))
                userPolyplane = parseFabricPolyplane(arr);
            else 
                LogF("Unknown polyplane class: %s",cmd.Data());

            if (userPolyplane.NotNull())
                polyplaneType = PT_USER;
        }
    }

}