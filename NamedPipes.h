#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include <string>

struct Client
{
public:
  enum Error{
    ALL_OK              //No problems, yay :-)
    , IO_ERROR            //Problem with name pipes IO check function GetLastIOError for details
    , NO_NEW_DATA         //Receiving failed because there are no data
    , WRONG_HEADER        //Received data doesn't fulfill requirements of our protocol, either it is wrong message, or we are lost in the stream
    , WRONG_OUT_HEADER    //Header that we are trying to send doesn't fit header buffer - probably problem in code, this is not really runtime error
    , WRONG_TYPE          //Received data had different type than expected
    , WRONG_LENGTH        //Received data had different length then expected
    , MESSAGE_TOO_LONG    //Received message was too long (see bufferSize for the current limit)
    , NOT_ENOUGH_MEMORY   //Memory allocation failed
  };

  virtual bool Connect(const std::string & connectionString)=0;
  virtual void Disconnect()=0;
  virtual bool SendBool(bool value)=0;
  virtual bool SendInt(int value)=0;
  virtual bool SendString(const std::string & value)=0;

  virtual bool ReceiveBool()=0;
  virtual int ReceiveInt()=0;
  virtual float ReceiveFloat()=0;
  virtual double ReceiveDouble()=0;
  virtual std::string ReceiveString()=0;

  virtual Error GetLastError()=0;
};

template <class T> static int copyVarToCharArray(T value, char* arr, int index)
{
  char * data = (char*) & value;
  for (int i = 0; i < sizeof(T); i++)
    arr[index+i] = data[i];
  return index+sizeof(T);
}
template <class T> static int copyVarFromCharArray(T & value, char* arr, int index)
{
  char * data = (char*) & value;
  for (int i = 0; i < sizeof(T); i++)
    data[i] = arr[index+i];
  return index+sizeof(T);
}

class NamedPipeClient:public Client
{
private:
  HANDLE hPipe;
  static const int bufferSize = 2048; //All messages have to be smaller than 2KB
  char _buff[bufferSize];

  static const int headerTextSize = 128;
  static const int headerSize = headerTextSize+3*sizeof(int);
  char _header[headerSize];
  
  int _messageNumber;
  enum DataType{type_void, type_bool, type_int, type_float, type_double, type_string };
  Error _lastError;

  bool Send(const char* data, int dataType, int byteLength)
  {
    int index = headerTextSize;
    index = copyVarToCharArray(_messageNumber++, _header, index);
    index = copyVarToCharArray(dataType, _header, index);
    index = copyVarToCharArray(byteLength, _header, index);
    if (index != headerSize) {_lastError=WRONG_OUT_HEADER; return false;}

    DWORD written;
    BOOL fSuccess = WriteFile(
      hPipe,         // pipe handle
      _header,       // message
      headerSize,    // message length
      &written,      // bytes written
      NULL);         // not overlapped
    if ( ! fSuccess || written != headerSize) 
    {
      DWORD lError = GetLastError();
      _lastError=IO_ERROR; return false;
    }

    fSuccess = WriteFile(
      hPipe,         // pipe handle
      data,          // message
      byteLength,    // message length
      &written,      // bytes written
      NULL);         // not overlapped

    if ( ! fSuccess || written != byteLength) 
    {
      DWORD lError = GetLastError();
      _lastError=IO_ERROR; return false;
    }
    return true;
  }
  bool Receive(const char * & data, int & dataType, int &byteLength)
  {
    byteLength = 0;
    BOOL fSuccess = FALSE;
    DWORD actRead=0;
    fSuccess = ReadFile(
      hPipe,    // pipe handle
      _buff,    // buffer to receive reply
      headerSize,// size of buffer
      &actRead,  // number of bytes read
      NULL);    // not overlapped
    if (!fSuccess)
    {
      if (::GetLastError()==ERROR_NO_DATA) {_lastError=NO_NEW_DATA; return false;}
      if (actRead!=headerSize){_lastError=IO_ERROR; return false;}
    }
    if (!CheckHeader(_buff)) {_lastError=WRONG_HEADER; return false;}
    int index = headerTextSize, messNumber;
    index = copyVarFromCharArray(messNumber, _buff, index);
    index = copyVarFromCharArray(dataType, _buff, index);
    index = copyVarFromCharArray(byteLength, _buff, index);

    if (byteLength > bufferSize) {_lastError=MESSAGE_TOO_LONG; return false;} //Maybe pop whole message out of the buffer
    fSuccess = ReadFile(
      hPipe,    // pipe handle
      _buff,    // buffer to receive reply
      byteLength,// size of buffer
      &actRead,  // number of bytes read
      NULL);    // not overlapped
    if (!fSuccess || actRead != byteLength) {_lastError=IO_ERROR; return false;}

    //if ( ! fSuccess && ::GetLastError() == ERROR_MORE_DATA ) //Don't care
    data = _buff;
    return true;
  }
  bool ReceiveCheckAndCopy(char* dest, DataType expectedDataType,  int expectedLength = -1)
  {
    int dataType, length = expectedLength; const char* data;
    if (!Receive(data, dataType, length)) return false;
    if (expectedLength > 0 && expectedLength!=length){_lastError=WRONG_LENGTH; return false;}
    if ((int)expectedDataType != dataType){_lastError=WRONG_TYPE; return false;}
    memcpy(dest, data, length);
    return true;
  }
  bool CheckHeader(const char* data){for (int i=0; i < headerTextSize; i++) if (data[i]!=_header[i]) return false; return true;}

public:
  NamedPipeClient():_messageNumber(0), _lastError(ALL_OK)
  {
    for (int i=0; i < headerTextSize; i++) _header[i] = 1; //Fill header text wit something
    strcpy(_header, "Named pipe protocol version description.");
  }
  virtual bool Connect(const std::string & connectionString)
  {
    char pipeName[150];
    strcpy (pipeName, "\\\\.\\pipe\\");
    strcat (pipeName, connectionString.c_str());

    DWORD error = 0;
    while (1)
    {
      hPipe = CreateFile(
        pipeName,       // pipe name
        GENERIC_READ |  // read and write access
        GENERIC_WRITE,
        0,              // no sharing
        NULL,           // default security attributes
        OPEN_EXISTING,  // opens existing pipe
        0,              // default attributes
        NULL);          // no template file

      if (hPipe != INVALID_HANDLE_VALUE) // Break if the pipe handle is valid.
        break;

      error = ::GetLastError();
      if (error != ERROR_PIPE_BUSY)  // Exit if an error other than ERROR_PIPE_BUSY occurs.
      {
        _lastError=IO_ERROR;
        return false;
      }

      if ( ! WaitNamedPipe(pipeName, 2000)) // All pipe instances are busy, so wait for 2 seconds.
      {
        _lastError=IO_ERROR;
        return false;
      }
    }

    // The pipe connected; change to message-read mode.
    DWORD dwMode = PIPE_TYPE_BYTE | PIPE_NOWAIT;
    BOOL fSuccess = SetNamedPipeHandleState(
      hPipe,    // pipe handle
      &dwMode,  // new pipe mode
      NULL,     // don't set maximum bytes
      NULL);    // don't set maximum time
    if ( ! fSuccess){ _lastError=IO_ERROR; return false;}
    return true;
  }
  virtual void Disconnect(){CloseHandle(hPipe);}
  virtual bool SendBool(bool value){return Send((char*)&value, type_bool, sizeof(bool));}
  virtual bool SendInt(int value){return Send((char*)&value, type_int, sizeof(int));}
  virtual bool SendFloat(float value){return Send((char*)&value, type_float, sizeof(float));}
  virtual bool SendDouble(double value){return Send((char*)&value, type_double, sizeof(double));}
  virtual bool SendString(const std::string & value){return Send(value.c_str(), type_string, (int)(value.size()+1)*sizeof(char));}

  virtual bool ReceiveBool(){bool res=false; ReceiveCheckAndCopy((char*)&res, type_bool); return res;}
  virtual int ReceiveInt(){int res=0; ReceiveCheckAndCopy((char*)&res, type_int); return res;}
  virtual float ReceiveFloat(){float res=0; ReceiveCheckAndCopy((char*)&res, type_float); return res;}
  virtual double ReceiveDouble(){double res=0; ReceiveCheckAndCopy((char*)&res, type_double); return res;}
  virtual std::string ReceiveString()
  {
    int dataType, length; const char* data;
    if (!Receive(data, dataType, length)) return NULL;
    if (dataType!=type_string) return NULL;
    if (length < 1) return NULL;
    return std::string(data);
  }

  virtual Error GetLastError(){return _lastError;}
  DWORD GetLastIOError(){return ::GetLastError();}
};

