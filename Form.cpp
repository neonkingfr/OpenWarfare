#include "Form.h"

#include <Winuser.h>

namespace Framework
{
    Form::Form(HINSTANCE hInst, const SIZE &size, const char* className) : _pWndParams(NULL), _pEvents(NULL)
    {
        // class structures inicialization
        _pWndParams = new tagXWindow();
        _pEvents = new Events();

        _pWndParams->isCreated = true;
        _pWndParams->hInst = hInst;
        _pWndParams->size = size;
        _pWndParams->className = new char[strlen(className) + 1];
        strcpy_s(_pWndParams->className, strlen(className) + 1, className);

        WNDCLASSEX wcex;

        // clear memory
        ZeroMemory(&wcex, sizeof(WNDCLASSEX));

        wcex.cbSize         = sizeof(WNDCLASSEX); 
        wcex.style          = CS_HREDRAW | CS_VREDRAW;
        wcex.hCursor        = LoadCursor(NULL, IDC_ARROW);
        wcex.hbrBackground  = CreateSolidBrush(RGB(0, 0, 0));
        wcex.hIcon          = NULL;
        wcex.hIconSm        = NULL;
        wcex.lpszMenuName   = NULL;
        wcex.hInstance      = hInst;
        wcex.lpfnWndProc    = Form::WndProc;
        wcex.lpszClassName  = _pWndParams->className;

        RegisterClassEx(&wcex);

        Create(NULL);
    }

    Form::~Form(void)
    {
        UnregisterClassA(_pWndParams->className, _pWndParams->hInst);

        if (_pWndParams)
            delete _pWndParams;

        if (_pEvents)
            delete _pEvents;
    }

    LRESULT CALLBACK Form::WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
    {
        Form* xw = (Form*) GetWindowLongPtr(hWnd, GWLP_USERDATA);

        switch (uMsg)
        {
        case WM_CREATE:
            // Saving pointer to class
            SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)((LPCREATESTRUCT)lParam)->lpCreateParams);
            return 0;

        case WM_KEYDOWN:
            xw->OnKeyDown(wParam, lParam);
            return 0;

        case WM_KEYUP:
            xw->OnKeyUp(wParam, lParam);
            return 0;

        case WM_MOUSEMOVE:
            xw->OnMouseMove(lParam);
            return 0;

        case WM_LBUTTONDOWN:
        case WM_RBUTTONDOWN:
            xw->OnMouseDown(uMsg, lParam);
            return 0;

        case WM_LBUTTONUP:
        case WM_RBUTTONUP:
            xw->OnMouseUp(uMsg, lParam);
            return 0;

        case WM_MOUSEWHEEL:
            xw->OnMouseWheel(wParam);
            return 0;

        case WM_SIZE:
            xw->OnWindowSizeChange();
            return 0;

        case WM_DESTROY:
            xw->OnWindowClose();
            PostQuitMessage(0);
            return 0;
        }

        return DefWindowProc(hWnd, uMsg, wParam, lParam);
    }


    /* Create window */
    void Form::Create(HWND hWndParent)
    {
        RECT rc = { 0, 0, _pWndParams->size.cx, _pWndParams->size.cy };

        AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, false);

        _pWndParams->hWnd = CreateWindowEx(
            0, //WS_EX_TOPMOST,
            _pWndParams->className,
            _pWndParams->className,
            WS_OVERLAPPEDWINDOW,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            rc.right - rc.left,
            rc.bottom - rc.top,
            (HWND) NULL,                // no parent or owner window, NULL = desctop parent
            (HMENU) NULL,
            _pWndParams->hInst,    // aplication instance handle
            this);                      // window creation data - pointer to class
    }

    int Form::DoEvents(void)
    {
        MSG msg;

        while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

        return msg.wParam;
    }

    BOOL Form::Show(int nCmdShow)
    {
        return ShowWindow(_pWndParams->hWnd, nCmdShow);
    }

    void Form::OnWindowSizeChange(void)
    {
        GetClientRect(_pWndParams->hWnd, &_pWndParams->rect);
    }

    void Form::OnWindowClose(void)
    {
        _pWndParams->isCreated = false;
    }

    void Form::OnKeyDown(WPARAM wParam, LPARAM lParam)
    {
        _pEvents->Keys[wParam] = true;
    }

    void Form::OnKeyUp(WPARAM wParam, LPARAM lParam)
    {
        _pEvents->Keys[wParam] = false;
    }

    void Form::OnMouseUp(UINT &uMsg, LPARAM lParam)
    {
        if (uMsg == WM_RBUTTONUP)
            _pEvents->RightMouse = false;

        if (uMsg == WM_LBUTTONUP)
            _pEvents->LeftMouse = false;
    }

    void Form::OnMouseMove(LPARAM lParam)
    {
        _pEvents->MouseMove = true;

        _pEvents->X = (short)(lParam & 0x0000FFFF);
        _pEvents->Y = (short)(lParam >> 16) & 0x0000FFFF;
    }

    void Form::OnMouseDown(UINT &uMsg, LPARAM lParam)
    {
        if (uMsg == WM_RBUTTONDOWN)
            _pEvents->RightMouse = true;

        if (uMsg == WM_LBUTTONDOWN)
            _pEvents->LeftMouse = true;

        _pEvents->X = (short)(lParam & 0x0000FFFF);
        _pEvents->Y = (short)(lParam >> 16) & 0x0000FFFF;
    }

    void Form::OnMouseWheel(WPARAM wParam)
    {
        _pEvents->MouseWheel = (char) ((wParam >> 16) & 0x000000FF);
    }
}