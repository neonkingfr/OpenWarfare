/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Translate;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author ondra
 */
public class Translate extends HttpServlet {

    Connection getConnection()
            throws ClassNotFoundException, SQLException
    {
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection ("jdbc:mysql://dev.bistudio.com/Multilang","multilang","aq{onM5P[_");
    }
    
    void AddTranslation(String original, String srcLang, String translation, String tgtLang)
    {
        try {
            String addQuery =
                "INSERT INTO `Multilang`.`translate` (`original`, `originalLang`, `translationLang`, `translation`) " +
                "VALUES (?, ?, ?, ?)";
            Connection con = getConnection();
            PreparedStatement ps = con.prepareStatement(addQuery);
            ps.setString(1,original);
            ps.setString(2,srcLang);
            ps.setString(3,tgtLang);
            ps.setString(4,translation);
            ps.executeUpdate();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Translate.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Translate.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private static String DecodeHTML(String s)
    {
        try {
            String test = URLDecoder.decode(s, "utf-8");
            StringBuilder w = new StringBuilder(s.length());
            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);
                if (c == '&') {
                    // parse HTML encoding, expected ascii code, but handle also lt, gt, apos, quot, and amp ( handle nbsp as well?)
                    // scan until entity name end
                    int term = s.indexOf(';', i + 1);
                    if (term > i) {
                        String entName = s.substring(i + 1, term);
                        if (entName.charAt(0) == '#') {
                            int code = Integer.parseInt(entName.substring(1));
                            w.append((char)code);
                        } else {
                            w.append("&" + entName + ";");
                        }
                        i = term;
                    }

                } else {
                    w.append(c);
                }
            }
            return w.toString();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Translate.class.getName()).log(Level.SEVERE, null, ex);
            return s;
        }
    }

    class TransResult
    {
        String translation;
        String fromLang;
    };
    
    ArrayList<TransResult> GoogleTrans(String srcLang, String tgtLang, ArrayList<String> original)
    {
        if (original.size()>0)
        {
            try {
                String url = "https://www.googleapis.com/language/translate/v2";
                String apiKey = "AIzaSyA7VE45i_XwokoHVLTgbLWRCHRFb2Pv2HI";

                HttpURLConnection urlCon = (HttpURLConnection) (new URL(url).openConnection());
                urlCon.setDoOutput(true); // POST
                urlCon.setRequestProperty("X-HTTP-Method-Override", "GET");
                urlCon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                String body = String.format("key=%s", URLEncoder.encode(apiKey, "utf-8"))
                        + String.format("&target=%s", URLEncoder.encode(tgtLang, "utf-8"));
                if (srcLang != null) {
                    body += String.format("&source=%s", URLEncoder.encode(srcLang, "utf-8"));
                }
                for (String orig : original) {
                    body += String.format("&q=%s", URLEncoder.encode(orig, "utf-8"));
                }

                urlCon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
                urlCon.setRequestProperty("Accept-Charset", "utf-8");

                OutputStream outStream = urlCon.getOutputStream();
                outStream.write(body.getBytes());

                int status = urlCon.getResponseCode();
                if (status < 400) {
                    InputStream transResponse = urlCon.getInputStream();
                    InputStreamReader is = new InputStreamReader(transResponse, "utf-8");
                    BufferedReader br = new BufferedReader(is);
                    String read;
                    StringBuilder sb = new StringBuilder();
                    while ((read = br.readLine()) != null) {
                        sb.append(read);
                    }

                    JSONObject respJSON = new JSONObject(sb.toString());

                    JSONObject respData = respJSON.getJSONObject("data");
                    JSONArray respTrans = respData.getJSONArray("translations");
                    ArrayList<TransResult> results = new ArrayList<TransResult>();
                    for (int t = 0; t < original.size(); t++) {
                        JSONObject to = respTrans.getJSONObject(t);

                        String translatedText = DecodeHTML(to.getString("translatedText"));

                        TransResult result = new TransResult();
                        result.translation = original.get(t);
                        result.fromLang = srcLang;

                        if (srcLang == null) {
                            result.fromLang = to.getString("detectedSourceLanguage");
                            srcLang = result.fromLang;
                        }
                        if (translatedText != null && !translatedText.isEmpty()) {
                            result.translation = translatedText;
                            if (srcLang.equals(tgtLang)) {
                                translatedText = "";
                            }
                            if (srcLang != null && !srcLang.isEmpty()) {
                                // create a new translation entry
                                AddTranslation(original.get(t), srcLang, translatedText, tgtLang);
                            }
                        }
                        results.add(result);
                    }
                    return results;
                } else {
                    /*
                     * InputStream transResponse = urlCon.getErrorStream();
                     * InputStreamReader is=new
                     * InputStreamReader(transResponse,"utf-8"); BufferedReader
                     * br=new BufferedReader(is); String read; StringBuilder sb
                     * = new StringBuilder(); while((read = br.readLine()) !=
                     * null) sb.append(read); JSONObject ret = new
                     * JSONObject(sb.toString()); out.append(ret.toString(0));
                     */
                }
            } catch (JSONException ex) {
                Logger.getLogger(Translate.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Translate.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(Translate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        ArrayList<TransResult> results = new ArrayList<TransResult>();
        for (String orig: original)
        {
            TransResult result = new TransResult();
            result.translation = orig;
            result.fromLang = srcLang;
            results.add(result);
        }
        
        return results;
    }
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String reqEnc = request.getCharacterEncoding();
            if (reqEnc==null)
            {
                request.setCharacterEncoding("UTF-8");
            }
            
            String srcLang = request.getParameter("orig");
            String tgtLang = request.getParameter("target");
            ArrayList<String> original = new ArrayList<String>();
            String textPar = request.getParameter("text");
            if (textPar!=null) original.add(textPar);
            if (srcLang!=null) srcLang = srcLang.toLowerCase();
            if (tgtLang!=null) tgtLang = tgtLang.toLowerCase();
            
            if (tgtLang==null || original.size()==0)
            {
                ServletInputStream inputStream = request.getInputStream();

                InputStreamReader is=new InputStreamReader(inputStream,"utf-8");
                BufferedReader br=new BufferedReader(is);
                    
                String inputRead;
                StringBuilder sb = new StringBuilder();
                while((inputRead = br.readLine()) != null) sb.append(inputRead);

                try
                {
                    JSONObject inputJSON = new JSONObject(sb.toString());
                    JSONArray qArray = inputJSON.optJSONArray("q");
                    if (qArray==null)
                    {
                        original.add(inputJSON.getString("q"));
                    }
                    else
                    {
                        for (int q=0; q<qArray.length(); q++)
                        {
                            original.add(qArray.getString(q));
                        }
                    }
                    tgtLang = inputJSON.getString("target");
                    srcLang = inputJSON.getString("source");
                } catch (JSONException ex)
                {
                }
                
                
                if (tgtLang==null || original==null)
                {
                    response.setStatus(400);
                    return;
                }
            }
            
            //out.append("// " + original + "\n");
            ArrayList<String> translation = new ArrayList<String>(original);
            ArrayList<String> fromLang = new ArrayList<String>(original);
            // search in the SQL database
            Connection con = getConnection();
            String statement = "SELECT * FROM translate WHERE ";
            PreparedStatement ps;
            PreparedStatement psO = con.prepareStatement(statement+"original=?");
            if (srcLang==null)
            {
                ps = con.prepareStatement(statement+"original=? AND translationLang=?");
                ps.setString(2,tgtLang);
            }
            else
            {
                ps = con.prepareStatement(statement+"original=? AND originalLang=? AND translationLang=?");
                ps.setString(1,srcLang);
                ps.setString(3,tgtLang);
            }

            ArrayList<Integer> toExternal = new ArrayList<Integer>();
            for (int o = 0; o < original.size(); o++) {
                String orig = original.get(o);
                if (srcLang == null) {
                    // check if we already know the phrase is in the target language
                    psO.setString(1, orig);
                    ResultSet rs = psO.executeQuery();
                    if (rs.next() && rs.getString("originalLang").equals(tgtLang)) { // text already known to be in the target language
                        translation.set(o, orig);
                        fromLang.set(o,tgtLang);
                        continue;
                    }
                }
                ps.setString(1, orig);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) // only one result interesting for us (should be unique anyway)
                {
                    translation.set(o, rs.getString("translation"));
                    fromLang.set(o,rs.getString("originalLang"));
                } else {
                    toExternal.add(o);
                }
            }

            ArrayList<String> origToExternal = new ArrayList<String>();
            for (int o : toExternal) {
                origToExternal.add(original.get(o));
            }
            ArrayList<TransResult> trans = GoogleTrans(srcLang, tgtLang, origToExternal);
            if (trans.size()==toExternal.size())
            {
                for (int oi=0; oi<toExternal.size(); oi++) {
                    int o = toExternal.get(oi);
                    // request a new translation
                    translation.set(o, trans.get(oi).translation);
                    fromLang.set(o, trans.get(oi).fromLang);
                }

                JSONObject ret = new JSONObject();
                JSONArray retTrans = new JSONArray();
                for (int i=0; i<original.size(); i++) {
                    JSONObject transI = new JSONObject();
                    transI.put("src",fromLang.get(i));
                    transI.put("translation",translation.get(i));
                    retTrans.put(transI);
                }
                ret.put("translations",retTrans);
                out.append(ret.toString(0));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Translate.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Translate.class.getName()).log(Level.SEVERE, null, ex);
        }catch (JSONException ex) {
            Logger.getLogger(Translate.class.getName()).log(Level.SEVERE, null, ex);
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
