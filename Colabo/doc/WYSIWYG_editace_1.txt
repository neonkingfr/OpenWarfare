﻿Chceme umět i WYSIWYG editaci.

V zásadě bych rád, aby to v editačním okně fungovalo zhruba jako editace ve vbulletinu, kde si mohu libovolně přepínat mezi WYSIWYG a normální editací, navíc mám možnost i při normální editaci snadno vkládat a upravovat speciálni formátování přes ikonky (podobně jako v mediawiki) a kdykoli vygenerovat i plné preview výsledného příspěvku.

-----
***ondra** (22.1.2010 10:06:23)*: Srov. [Formátování (Markdown)](trunk/doc/formatovani.txt?server=dev.bistudio.com&database=colabo&id=3)

