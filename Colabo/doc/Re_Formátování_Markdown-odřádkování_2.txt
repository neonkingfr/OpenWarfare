﻿_____
**rola** (9.3.2010 13:48:46)

Stadardní Markdown jedno odřádkování ignoruje, ale **dvě mezery na konci řádku + odřádkování** už zachovává. Nynější změněná konvence dost komplikuje parsing, protože samotné odřádkování ovládá state Markown parseru (zvláště to vadí u vnořených seznamů a citací, které se teď musí „pročistit“ pomocí několika hacků).

Nemá smysl to ještě přehodnotit? Jenom návrh.


_____
**ondra** (9.3.2010 13:56:27)

Ten protiargument je příliš zásadní: vcelku normálně a pěkně zformátovaný plain text vypadá po prohnání standardním markdownem často děsivě a je k nepoznání. Viz již uvedené [Coding Horror: Treating User Myopia](http://www.codinghorror.com/blog/archives/001306.html)

_____
**rola** (9.3.2010 14:24:23)

Ok, udělám vše pro to, aby to fungovalo pořádně.

**Návrh:** chtěl bych udělat formátování méně laxní a stanovit přesná pravidla pro začátky řádků (teď to každá implementace Markdownu dělá trošku jinak, což je frustrující). V naší implementaci teď nefungují pořádně citace nebo kód po odrážce:

####Kód:

    > Já jsem citace.
    > * Mám v sobě odrážku.

    * Já jsem odrážka

        Mám pod sebou Source Code oddělený řádkem.

####Výsledek:

> Já jsem citace.
> * Mám v sobě odrážku.

* Já jsem odrážka

    Mám pod sebou Source Code oddělený řádkem.
