﻿<div class="noteheader"><b>rola</b> (23.2.2010 18:36:55)</div>
> Lepší přístup:
> 
> - uživatel něco změní v HTML textu (stiskne klávesu nebo Bold/Italic)
> - změna se zachytí
> - udělá se v Markdownovém textu (převod pozice)
> - pustí se převod Markdown&rarr;HTML
> - kurzor v novém HTML se umístí na nově převedené místo (opticky stejné jako předtím)

Nedělal jsi už něco v tomhle smyslu?
Stává se mi teď, že během editace, když například k tagu PRE přidím nějaký style, že se mi v preview nezobrazuje. Jakmile ho ale hodím do clipboardu a provedu CUT a znovu PASTE, začne všechno fungovat???


-----
***rola** (24.2.2010 15:10:47)*
To je tím, že se tam vkládá kurzor (takže v tom má prsty Jirka ;-)). Stačí si zaeditovat někde jinde a bude to fungovat. Pracuju na opravě.