namespace Colabo
{
  partial class DialogFind
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.buttonFindNext = new System.Windows.Forms.Button();
      this.buttonCancel = new System.Windows.Forms.Button();
      this.checkBoxMatchCase = new System.Windows.Forms.CheckBox();
      this.groupBoxDirection = new System.Windows.Forms.GroupBox();
      this.radioButtonUp = new System.Windows.Forms.RadioButton();
      this.radioButtonDown = new System.Windows.Forms.RadioButton();
      this.checkBoxWholeWords = new System.Windows.Forms.CheckBox();
      this.textBoxFind = new System.Windows.Forms.TextBox();
      this.groupBoxDirection.SuspendLayout();
      this.SuspendLayout();
      // 
      // buttonFindNext
      // 
      this.buttonFindNext.Location = new System.Drawing.Point(225, 15);
      this.buttonFindNext.Name = "buttonFindNext";
      this.buttonFindNext.Size = new System.Drawing.Size(75, 23);
      this.buttonFindNext.TabIndex = 1;
      this.buttonFindNext.Text = "Find Next";
      this.buttonFindNext.UseVisualStyleBackColor = true;
      this.buttonFindNext.Click += new System.EventHandler(this.buttonFindNext_Click);
      // 
      // buttonCancel
      // 
      this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.buttonCancel.Location = new System.Drawing.Point(225, 44);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.Size = new System.Drawing.Size(75, 23);
      this.buttonCancel.TabIndex = 2;
      this.buttonCancel.Text = "Cancel";
      this.buttonCancel.UseVisualStyleBackColor = true;
      this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
      // 
      // checkBoxMatchCase
      // 
      this.checkBoxMatchCase.AutoSize = true;
      this.checkBoxMatchCase.Location = new System.Drawing.Point(12, 70);
      this.checkBoxMatchCase.Name = "checkBoxMatchCase";
      this.checkBoxMatchCase.Size = new System.Drawing.Size(83, 17);
      this.checkBoxMatchCase.TabIndex = 3;
      this.checkBoxMatchCase.Text = "Match &Case";
      this.checkBoxMatchCase.UseVisualStyleBackColor = true;
      // 
      // groupBoxDirection
      // 
      this.groupBoxDirection.Controls.Add(this.radioButtonDown);
      this.groupBoxDirection.Controls.Add(this.radioButtonUp);
      this.groupBoxDirection.Location = new System.Drawing.Point(117, 47);
      this.groupBoxDirection.Name = "groupBoxDirection";
      this.groupBoxDirection.Size = new System.Drawing.Size(88, 74);
      this.groupBoxDirection.TabIndex = 5;
      this.groupBoxDirection.TabStop = false;
      this.groupBoxDirection.Text = "Direction";
      // 
      // radioButtonUp
      // 
      this.radioButtonUp.AutoSize = true;
      this.radioButtonUp.Location = new System.Drawing.Point(15, 23);
      this.radioButtonUp.Name = "radioButtonUp";
      this.radioButtonUp.Size = new System.Drawing.Size(39, 17);
      this.radioButtonUp.TabIndex = 0;
      this.radioButtonUp.Text = "&Up";
      this.radioButtonUp.UseVisualStyleBackColor = true;
      // 
      // radioButtonDown
      // 
      this.radioButtonDown.AutoSize = true;
      this.radioButtonDown.Checked = true;
      this.radioButtonDown.Location = new System.Drawing.Point(15, 46);
      this.radioButtonDown.Name = "radioButtonDown";
      this.radioButtonDown.Size = new System.Drawing.Size(53, 17);
      this.radioButtonDown.TabIndex = 1;
      this.radioButtonDown.TabStop = true;
      this.radioButtonDown.Text = "&Down";
      this.radioButtonDown.UseVisualStyleBackColor = true;
      // 
      // checkBoxWholeWords
      // 
      this.checkBoxWholeWords.AutoSize = true;
      this.checkBoxWholeWords.Location = new System.Drawing.Point(12, 93);
      this.checkBoxWholeWords.Name = "checkBoxWholeWords";
      this.checkBoxWholeWords.Size = new System.Drawing.Size(91, 17);
      this.checkBoxWholeWords.TabIndex = 4;
      this.checkBoxWholeWords.Text = "Whole &Words";
      this.checkBoxWholeWords.UseVisualStyleBackColor = true;
      // 
      // textBoxFind
      // 
      this.textBoxFind.Location = new System.Drawing.Point(12, 15);
      this.textBoxFind.Name = "textBoxFind";
      this.textBoxFind.Size = new System.Drawing.Size(193, 20);
      this.textBoxFind.TabIndex = 0;
      // 
      // DialogFind
      // 
      this.AcceptButton = this.buttonFindNext;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.buttonCancel;
      this.ClientSize = new System.Drawing.Size(315, 132);
      this.Controls.Add(this.textBoxFind);
      this.Controls.Add(this.checkBoxWholeWords);
      this.Controls.Add(this.groupBoxDirection);
      this.Controls.Add(this.checkBoxMatchCase);
      this.Controls.Add(this.buttonCancel);
      this.Controls.Add(this.buttonFindNext);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "DialogFind";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Find";
      this.Activated += new System.EventHandler(this.DialogFind_Activated);
      this.groupBoxDirection.ResumeLayout(false);
      this.groupBoxDirection.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button buttonFindNext;
    private System.Windows.Forms.Button buttonCancel;
    private System.Windows.Forms.CheckBox checkBoxMatchCase;
    private System.Windows.Forms.GroupBox groupBoxDirection;
    private System.Windows.Forms.RadioButton radioButtonDown;
    private System.Windows.Forms.RadioButton radioButtonUp;
    private System.Windows.Forms.CheckBox checkBoxWholeWords;
    private System.Windows.Forms.TextBox textBoxFind;
  }
}