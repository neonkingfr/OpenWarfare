using System;
using System.Collections.Generic;
using System.Windows.Forms;

using System.ComponentModel;
using System.Xml;
using System.IO;
using System.Net;
using System.Threading;

namespace Colabo
{
  public static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread] // needed for System.Windows.Forms.WebBrowser
    static void Main(string[] args)
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);

      _queue = new Queue<ITask>[Priorities];
      for (int i = 0; i < Priorities; i++) _queue[i] = new Queue<ITask>();

      _context = new ColaboApplicationContext();

      // new log file, write down the header
      string dir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Colabo";
      if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
      StreamWriter writer = new StreamWriter(dir + "\\log.txt", false, System.Text.Encoding.UTF8);
      writer.WriteLine("Colabo started, revision {0}", Revision.RevisionNo);
      writer.Close();

      string configFile;
      if (args != null && args.Length > 0)
        configFile = args[0];
      else
        configFile = _defaultConfigFile;
      _config = new Configuration(configFile);
      // We need to have _config valid now
      _config.Load(_context, true);

      if (string.IsNullOrEmpty(_updatePath))
      {
        // create at least one form
        if (_context.forms.Count == 0) _context.NewForm(null);

        // add low priority tasks in some interval
        _timer = new System.Threading.Timer(OnTimer, null, _timerDueTime, 0);

        Application.Run(_context);
      }
    }

    // auto-update the application if needed
    public static bool Update(string path, bool save)
    {
      if (string.IsNullOrEmpty(path)) return false;

      Uri url = new Uri(path);

      // read the revision number of the current distribution
      string dummy ;
      string content;
      using (WebResponse response = FormColabo.AccessWebResponse(new Uri(url, "revision.txt"), null, null, null, out dummy, false))
      {
        if (response == null) return false; // authentication failed
        using (Stream stream = response.GetResponseStream())
        {
          using (StreamReader reader = new StreamReader(stream, System.Text.Encoding.UTF8))
          {
            content = reader.ReadToEnd();
          }

        }
      }

      int revision;
      if (int.TryParse(content, out revision) && revision > Revision.RevisionNo)
      {
        // update is available
        DialogResult result = MessageBox.Show(
          string.Format("New version (revision {0}) is available. Do you want to download it?", revision), "Colabo",
          MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        if (result == DialogResult.Yes)
        {
          // download the update
          string dir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
          if (string.IsNullOrEmpty(dir)) return false;
          dir += "\\Colabo\\Distribution\\";

          SharpSvn.SvnClient client = new SharpSvn.SvnClient();
          client.Authentication.UserNamePasswordHandlers += new EventHandler<SharpSvn.Security.SvnUserNamePasswordEventArgs>(
            delegate(object sender, SharpSvn.Security.SvnUserNamePasswordEventArgs args)
            {
              SVNDatabase.Authenticate(args, dummy);
            });
          client.Authentication.SslServerTrustHandlers += new EventHandler<SharpSvn.Security.SvnSslServerTrustEventArgs>(
            SVNDatabase.CheckServerCertificate
            );

          if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
          Uri oldUrl = client.GetUriFromWorkingCopy(dir);
          if (oldUrl == null)
          {
            try
            {
              EmptyDirectory(dir); // remove the old repository if present
              client.CheckOut(url, dir);
            }
            catch (System.Exception e)
            {
              MessageBox.Show("Update failed: \n" + e, "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
              return false;
            }
          }
          else if (oldUrl != url)
          {
            // repository changed, replace the old working copy
            try
            {
              EmptyDirectory(dir);
              client.CheckOut(url, dir);
            }
            catch (System.Exception e)
            {
              MessageBox.Show("Update failed: \n" + e, "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
              return false;
            }
          }
          else
          {
            // make sure no false lock is present
            client.CleanUp(dir);
            // in case of conflict, always force SVN version
            SharpSvn.SvnUpdateArgs args = new SharpSvn.SvnUpdateArgs();
            args.Conflict += delegate(object sender, SharpSvn.SvnConflictEventArgs e)
            {
              e.Choice = SharpSvn.SvnAccept.Theirs;
            };
            try
            {
              client.Update(dir, args);
            }
            catch (System.Exception e)
            {
              MessageBox.Show("Update failed: \n" + e, "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
              return false;
            }
          }

          // launch the update process to finish the update
          string parameters = string.Format(
            "\" {0} \" \" {1} \" \" {2} \" {3}", dir, Application.ExecutablePath, Directory.GetCurrentDirectory(),
            System.Diagnostics.Process.GetCurrentProcess().Id);
          System.Diagnostics.Process.Start(dir + "ColaboUpdate.exe", parameters);

          // close all windows, wait until background threads are done
          _updatePath = dir;
          Exit(save);
          return true;
        }
      }
      return false;
    }
    private static string _updatePath;
    public static void EmptyDirectory(string target_dir)
    {
      string[] files = Directory.GetFiles(target_dir);
      string[] dirs = Directory.GetDirectories(target_dir);

      foreach (string file in files)
      {
        File.SetAttributes(file, FileAttributes.Normal);
        File.Delete(file);
      }

      foreach (string dir in dirs)
      {
        EmptyDirectory(dir);
        Directory.Delete(dir, false);
      }
    }

    static Mutex _logMutex = new Mutex(false, "Colabo_Log_File_{04c7a0ba-328c-4609-b328-32265cd13273}");

    // diagnostics
    public static void Log(string text)
    {
      try
      {
        _logMutex.WaitOne();
      }
      catch (AbandonedMutexException)
      {
        // http://stackoverflow.com/questions/15456986/how-to-gracefully-get-out-of-abandonedmutexexception
        // http://msdn.microsoft.com/en-us/library/system.threading.mutex.aspx
        // mutex may be abandonned in another process was terminated while writing to log
        // nothing to do in such case, just continue, the mutex is already acquired
      }
      try
      {
        string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Colabo\\log.txt";
        StreamWriter writer = new StreamWriter(path, true, System.Text.Encoding.UTF8);
        writer.WriteLine(text);
        writer.Close();
        Console.WriteLine(text);
      }
      finally
      {
        _logMutex.ReleaseMutex();
      }
    }

    public static FormColabo NewForm()
    {
      if (_context == null) return null;
      
      // inherit settings from the first form
      XmlElement clsView = _config.FirstView();
      return _context.NewForm(clsView);
    }
    public static void Exit(bool save)
    {
      if (_context != null) _context.Clear(save);
    }

    // add low priority tasks in some interval
    private static System.Threading.Timer _timer;
    private const int _timerDueTime = 60 * 1000; // 1 minute
    private const int _timerPeriod = 15 * 60 * 1000; // 15 minutes
    private delegate void OnTimerDelegate();
    private static void OnTimer(object state)
    {
      int lpCount = 0;
      for (int i = 0; i < _queue.Length; i++) lpCount += _queue[i].Count;
      if (lpCount==0)
      {
        if (forms.Count > 0)
        {
          FormColabo form = forms[0];
          form.BeginInvoke(new OnTimerDelegate(delegate() { form.CreateLPTasks(); }));
        }
      }
      // plan the next tick
      _timer.Change(_timerPeriod, 0);
    }

    private const int Priorities = 3;
    /// Queue of tasks for the background worker
    private static Queue<ITask>[] _queue;

    private static bool IsTaskPending()
    {
      for (int i = 0; i < Priorities; i++)
        if (_queue[i].Count > 0) return true;
      return false;
    }

    public static void AddTask(ITask task, int priority)
    {
      if (priority < 0) priority = 0;
      if (priority > Priorities - 1) priority = Priorities - 1;
      if (IsTaskPending())
      {
        // some tasks are waiting
        _queue[priority].Enqueue(task);
        return;
      }
      FormColabo form = null;
      if (forms.Count > 0) form = forms[0];
      if (form == null || !form.RunTask(task))
      {
        // cannot launch the task now
        _queue[priority].Enqueue(task);
      }
    }

    public static void AddTask(ITask task)
    {
      AddTask(task, 0);
    }

    public static void UpdateProgress(int progress, string message)
    {
      string[] taskTypes = new string[Priorities];
      int[] tasks = new int[Priorities];
      for (int i = 0; i < tasks.Length; i++)
      {
        tasks[i] = _queue[i].Count;
        taskTypes[i] = "";
        foreach (ITask task in _queue[i])
        {
          string type = task.ProgressType();
          if (!taskTypes[i].Contains(type))
          {
            if (taskTypes[i].Length > 0) taskTypes[i] += ", ";
            else taskTypes[i] += "x ";
            taskTypes[i] += type;
          }
        }
      }
      foreach (FormColabo form in forms) form.UpdateProgress(progress, message, tasks, taskTypes);
    }
    public static void OnTaskCompleted(FormColabo oldForm)
    {
      if (_context.OnTaskCompleted(oldForm)) return;

      // report the finished task to UI
      UpdateProgress(0, "Done");

      // find the first form
      FormColabo form = null;
      if (forms.Count > 0) form = forms[0];

      if (form != null)
      {
        for (int i = 0; i < Priorities; i++)
        {
          // check if other task is waiting
          if (_queue[i].Count > 0)
          {
            ITask task = _queue[i].Dequeue();
            if (!form.RunTask(task)) _queue[i].Enqueue(task);
            break;
          }
        }
      }
    }

    /// configuration   
    private static Configuration _config;
    public static Configuration config
    {
      get { return _config; }
    }
    // default config file
    const string _defaultConfigFile = "colabo.cfg.xml";

    // keep trace of all forms, close application when all forms are closed
    public class ColaboApplicationContext : ApplicationContext
    {
      // opened windows
      private List<FormColabo> _forms;
      // windows waiting for background worker to finish
      private List<FormColabo> _formsClosing;

      public List<FormColabo> forms
      {
        get { return _forms; }
      }

      public ColaboApplicationContext()
      {
        _forms = new List<FormColabo>();
        _formsClosing = new List<FormColabo>();
      }

      public FormColabo NewForm(XmlElement clsView)
      {
        FormColabo form = new FormColabo();
        if (clsView != null) form.LoadDialog(clsView);
        _forms.Add(form);
        form.Closing += new CancelEventHandler(OnFormClosing);
        form.Closed += new EventHandler(OnFormClosed);
        form.Show();
        return form;
      }
      public void Clear(bool save)
      {
        // save the configuration
        if (save) Program.config.Save(_forms);
        while (_forms.Count > 0)
        {
          FormColabo form = _forms[0];
          _forms.Remove(form);
          // wait until the current task is completed
          if (form.IsTaskRunning())
          {
            _formsClosing.Add(form);
            form.Enabled = false;
          }
        }

        // When the count gets to 0, exit the application by calling ExitThread().
        if (_forms.Count == 0 && _formsClosing.Count == 0)
        {
          ExitThread();
        }
      }

      private void OnFormClosing(object sender, CancelEventArgs e)
      {
        // check if the last form is closing
        if (_forms.Count == 1)
        {
          // save form properties to recover it later
          Program._config.Save(_forms);
        }
      }

      private void OnFormClosed(object sender, EventArgs e)
      {
        // remove form from the list
        FormColabo form = (FormColabo)sender;
        _forms.Remove(form);
        form._notifyIdle.Stop(); // stop idle notification before stopping presence threads
        // save the cache
        foreach (SQLDatabase database in form.databases)
        {
          database.SaveCache();
          database.OnClose();
        }
        // wait until the current task is completed
        if (form.IsTaskRunning())
        {
          _formsClosing.Add(form);
          form.Enabled = false;
        }
        // When the count gets to 0, exit the application by calling ExitThread().
        if (_forms.Count == 0 && _formsClosing.Count == 0)
        {
          ExitThread();
        }
      }

      public bool OnTaskCompleted(FormColabo oldForm)
      {
        // if (_formsClosing.Contains(oldForm))
        {
          _formsClosing.Remove(oldForm); // we was waiting for task to complete

          // When the count gets to 0, exit the application by calling ExitThread().
          if (_forms.Count == 0 && _formsClosing.Count == 0)
          {
            ExitThread();
            return true;
          }
        }
        return false;
      }
    }

    private static ColaboApplicationContext _context;

    public static List<FormColabo> forms
    {
      get { return _context.forms; }
    }
  }
}