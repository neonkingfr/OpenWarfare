using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Colabo
{
  public partial class DialogAbout : Form
  {
    public DialogAbout()
    {
      InitializeComponent();
    }

    private void buttonOk_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}