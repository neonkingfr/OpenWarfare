﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

[StructLayout(LayoutKind.Sequential)]
public struct FLASHWINFO
{
  public UInt32 cbSize;
  public IntPtr hwnd;
  public Int32 dwFlags;
  public UInt32 uCount;
  public Int32 dwTimeout;
}

[Flags]
public enum FlashMode
{
  FLASHW_STOP = 0,
  FLASHW_CAPTION = 1,
  FLASHW_TRAY = 2,
  FLASHW_ALL = (FLASHW_CAPTION | FLASHW_TRAY),
  FLASHW_TIMER = 4,
  FLASHW_TIMERNOFG = 12
}

namespace Colabo
{
  public class FlashWindow
  {
    [DllImport("user32.dll")]
    static extern Int32 FlashWindowEx(ref  FLASHWINFO pwfi);

    public static void Flash(Form window)
    {

      FLASHWINFO fw = new FLASHWINFO();
      fw.cbSize = Convert.ToUInt32(Marshal.SizeOf(typeof(FLASHWINFO)));
      fw.hwnd = window.Handle;
      fw.dwFlags = (Int32)FlashMode.FLASHW_ALL;
      fw.uCount = 5;
      FlashWindowEx(ref  fw);
    }
  }

}
