using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Colabo
{
  public partial class DialogDatabaseDN : Form
  {
    public string DBDN
    {
      get { return this.value.Text; }
      set { this.value.Text = value; }
    }

    public DialogDatabaseDN()
    {
      InitializeComponent();
    }
  }
}