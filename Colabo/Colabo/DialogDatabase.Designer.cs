namespace Colabo
{
  partial class DialogDatabase
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.valueName = new System.Windows.Forms.TextBox();
      this.valueServer = new System.Windows.Forms.TextBox();
      this.valueDatabase = new System.Windows.Forms.TextBox();
      this.valueUser = new System.Windows.Forms.TextBox();
      this.valueSVN = new System.Windows.Forms.TextBox();
      this.buttonCancel = new System.Windows.Forms.Button();
      this.buttonOK = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.valuePassword = new System.Windows.Forms.TextBox();
      this.buttonUserInfo = new System.Windows.Forms.Button();
      this.valueFullName = new System.Windows.Forms.TextBox();
      this.label7 = new System.Windows.Forms.Label();
      this.valuePicture = new System.Windows.Forms.TextBox();
      this.label8 = new System.Windows.Forms.Label();
      this.colorDialog = new System.Windows.Forms.ColorDialog();
      this.valueColor = new System.Windows.Forms.Label();
      this.buttonColor = new System.Windows.Forms.Button();
      this.label9 = new System.Windows.Forms.Label();
      this.valueAliases = new System.Windows.Forms.TextBox();
      this.label10 = new System.Windows.Forms.Label();
      this.ldapLoad = new System.Windows.Forms.Button();
      this.ldapDN = new System.Windows.Forms.TextBox();
      this.valueServerType = new System.Windows.Forms.ComboBox();
      this.label11 = new System.Windows.Forms.Label();
      this.label12 = new System.Windows.Forms.Label();
      this.valueTranslate = new System.Windows.Forms.TextBox();
      this.SuspendLayout();
      // 
      // valueName
      // 
      this.valueName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueName.Location = new System.Drawing.Point(89, 35);
      this.valueName.Name = "valueName";
      this.valueName.Size = new System.Drawing.Size(295, 20);
      this.valueName.TabIndex = 0;
      // 
      // valueServer
      // 
      this.valueServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueServer.Location = new System.Drawing.Point(89, 87);
      this.valueServer.Name = "valueServer";
      this.valueServer.Size = new System.Drawing.Size(295, 20);
      this.valueServer.TabIndex = 1;
      // 
      // valueDatabase
      // 
      this.valueDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueDatabase.Location = new System.Drawing.Point(89, 113);
      this.valueDatabase.Name = "valueDatabase";
      this.valueDatabase.Size = new System.Drawing.Size(295, 20);
      this.valueDatabase.TabIndex = 2;
      // 
      // valueUser
      // 
      this.valueUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueUser.Location = new System.Drawing.Point(89, 139);
      this.valueUser.Name = "valueUser";
      this.valueUser.Size = new System.Drawing.Size(295, 20);
      this.valueUser.TabIndex = 3;
      // 
      // valueSVN
      // 
      this.valueSVN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueSVN.Location = new System.Drawing.Point(89, 191);
      this.valueSVN.Name = "valueSVN";
      this.valueSVN.Size = new System.Drawing.Size(295, 20);
      this.valueSVN.TabIndex = 5;
      // 
      // buttonCancel
      // 
      this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.buttonCancel.Location = new System.Drawing.Point(305, 387);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.Size = new System.Drawing.Size(75, 23);
      this.buttonCancel.TabIndex = 7;
      this.buttonCancel.Text = "Cancel";
      this.buttonCancel.UseVisualStyleBackColor = true;
      // 
      // buttonOK
      // 
      this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.buttonOK.Location = new System.Drawing.Point(224, 387);
      this.buttonOK.Name = "buttonOK";
      this.buttonOK.Size = new System.Drawing.Size(75, 23);
      this.buttonOK.TabIndex = 6;
      this.buttonOK.Text = "OK";
      this.buttonOK.UseVisualStyleBackColor = true;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(17, 38);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(38, 13);
      this.label1.TabIndex = 8;
      this.label1.Text = "Name:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(17, 90);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(41, 13);
      this.label2.TabIndex = 9;
      this.label2.Text = "Server:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(17, 116);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(56, 13);
      this.label3.TabIndex = 10;
      this.label3.Text = "Database:";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(17, 142);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(32, 13);
      this.label4.TabIndex = 11;
      this.label4.Text = "User:";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(17, 168);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(56, 13);
      this.label5.TabIndex = 12;
      this.label5.Text = "Password:";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(17, 194);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(57, 13);
      this.label6.TabIndex = 13;
      this.label6.Text = "SVN Path:";
      // 
      // valuePassword
      // 
      this.valuePassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valuePassword.Location = new System.Drawing.Point(89, 165);
      this.valuePassword.Name = "valuePassword";
      this.valuePassword.Size = new System.Drawing.Size(295, 20);
      this.valuePassword.TabIndex = 4;
      this.valuePassword.UseSystemPasswordChar = true;
      // 
      // buttonUserInfo
      // 
      this.buttonUserInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonUserInfo.Location = new System.Drawing.Point(20, 247);
      this.buttonUserInfo.Name = "buttonUserInfo";
      this.buttonUserInfo.Size = new System.Drawing.Size(364, 23);
      this.buttonUserInfo.TabIndex = 14;
      this.buttonUserInfo.Text = "Edit user info";
      this.buttonUserInfo.UseVisualStyleBackColor = true;
      this.buttonUserInfo.Click += new System.EventHandler(this.buttonUserInfo_Click);
      // 
      // valueFullName
      // 
      this.valueFullName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueFullName.Location = new System.Drawing.Point(89, 277);
      this.valueFullName.Name = "valueFullName";
      this.valueFullName.Size = new System.Drawing.Size(294, 20);
      this.valueFullName.TabIndex = 15;
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(17, 280);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(55, 13);
      this.label7.TabIndex = 16;
      this.label7.Text = "Full name:";
      // 
      // valuePicture
      // 
      this.valuePicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valuePicture.Location = new System.Drawing.Point(89, 304);
      this.valuePicture.Name = "valuePicture";
      this.valuePicture.Size = new System.Drawing.Size(293, 20);
      this.valuePicture.TabIndex = 17;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(17, 307);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(68, 13);
      this.label8.TabIndex = 18;
      this.label8.Text = "Picture URL:";
      // 
      // valueColor
      // 
      this.valueColor.BackColor = System.Drawing.Color.Black;
      this.valueColor.Location = new System.Drawing.Point(89, 331);
      this.valueColor.Name = "valueColor";
      this.valueColor.Size = new System.Drawing.Size(35, 23);
      this.valueColor.TabIndex = 19;
      // 
      // buttonColor
      // 
      this.buttonColor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonColor.Location = new System.Drawing.Point(130, 331);
      this.buttonColor.Name = "buttonColor";
      this.buttonColor.Size = new System.Drawing.Size(251, 23);
      this.buttonColor.TabIndex = 20;
      this.buttonColor.Text = "Change color";
      this.buttonColor.UseVisualStyleBackColor = true;
      this.buttonColor.Click += new System.EventHandler(this.buttonColor_Click);
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(17, 336);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(34, 13);
      this.label9.TabIndex = 21;
      this.label9.Text = "Color:";
      // 
      // valueAliases
      // 
      this.valueAliases.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueAliases.Location = new System.Drawing.Point(89, 361);
      this.valueAliases.Name = "valueAliases";
      this.valueAliases.Size = new System.Drawing.Size(291, 20);
      this.valueAliases.TabIndex = 22;
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(17, 364);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(43, 13);
      this.label10.TabIndex = 23;
      this.label10.Text = "Aliases:";
      // 
      // ldapLoad
      // 
      this.ldapLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.ldapLoad.Location = new System.Drawing.Point(284, 6);
      this.ldapLoad.Name = "ldapLoad";
      this.ldapLoad.Size = new System.Drawing.Size(100, 23);
      this.ldapLoad.TabIndex = 24;
      this.ldapLoad.Text = "Load from LDAP";
      this.ldapLoad.UseVisualStyleBackColor = true;
      this.ldapLoad.Click += new System.EventHandler(this.ldapLoad_Click);
      // 
      // ldapDN
      // 
      this.ldapDN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.ldapDN.Enabled = false;
      this.ldapDN.Location = new System.Drawing.Point(20, 9);
      this.ldapDN.Name = "ldapDN";
      this.ldapDN.Size = new System.Drawing.Size(258, 20);
      this.ldapDN.TabIndex = 25;
      // 
      // valueServerType
      // 
      this.valueServerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.valueServerType.FormattingEnabled = true;
      this.valueServerType.Location = new System.Drawing.Point(89, 62);
      this.valueServerType.Name = "valueServerType";
      this.valueServerType.Size = new System.Drawing.Size(295, 21);
      this.valueServerType.TabIndex = 26;
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(17, 65);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(64, 13);
      this.label11.TabIndex = 27;
      this.label11.Text = "Server type:";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(17, 220);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(62, 13);
      this.label12.TabIndex = 29;
      this.label12.Text = "Translation:";
      // 
      // valueTranslate
      // 
      this.valueTranslate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueTranslate.Location = new System.Drawing.Point(89, 217);
      this.valueTranslate.Name = "valueTranslate";
      this.valueTranslate.Size = new System.Drawing.Size(295, 20);
      this.valueTranslate.TabIndex = 28;
      // 
      // DialogDatabase
      // 
      this.AcceptButton = this.buttonOK;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.buttonCancel;
      this.ClientSize = new System.Drawing.Size(392, 418);
      this.Controls.Add(this.label12);
      this.Controls.Add(this.valueTranslate);
      this.Controls.Add(this.label11);
      this.Controls.Add(this.valueServerType);
      this.Controls.Add(this.ldapDN);
      this.Controls.Add(this.ldapLoad);
      this.Controls.Add(this.label10);
      this.Controls.Add(this.valueAliases);
      this.Controls.Add(this.label9);
      this.Controls.Add(this.buttonColor);
      this.Controls.Add(this.valueColor);
      this.Controls.Add(this.label8);
      this.Controls.Add(this.valuePicture);
      this.Controls.Add(this.label7);
      this.Controls.Add(this.valueFullName);
      this.Controls.Add(this.buttonUserInfo);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.buttonOK);
      this.Controls.Add(this.buttonCancel);
      this.Controls.Add(this.valueSVN);
      this.Controls.Add(this.valuePassword);
      this.Controls.Add(this.valueUser);
      this.Controls.Add(this.valueDatabase);
      this.Controls.Add(this.valueServer);
      this.Controls.Add(this.valueName);
      this.MaximumSize = new System.Drawing.Size(600, 456);
      this.MinimumSize = new System.Drawing.Size(300, 456);
      this.Name = "DialogDatabase";
      this.Text = "Database";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DialogDatabase_FormClosing);
      this.Load += new System.EventHandler(this.DialogDatabase_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox valueName;
    private System.Windows.Forms.TextBox valueServer;
    private System.Windows.Forms.TextBox valueDatabase;
    private System.Windows.Forms.TextBox valueUser;
    private System.Windows.Forms.TextBox valueSVN;
    private System.Windows.Forms.Button buttonCancel;
    private System.Windows.Forms.Button buttonOK;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.TextBox valuePassword;
    private System.Windows.Forms.Button buttonUserInfo;
    private System.Windows.Forms.TextBox valueFullName;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.TextBox valuePicture;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.ColorDialog colorDialog;
    private System.Windows.Forms.Label valueColor;
    private System.Windows.Forms.Button buttonColor;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.TextBox valueAliases;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Button ldapLoad;
    private System.Windows.Forms.TextBox ldapDN;
    private System.Windows.Forms.ComboBox valueServerType;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.TextBox valueTranslate;
  }
}