namespace Colabo
{
  partial class DialogDatabaseManagement
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tabControl = new System.Windows.Forms.TabControl();
      this.tabPageCommon = new System.Windows.Forms.TabPage();
      this.label5 = new System.Windows.Forms.Label();
      this.valueServerType = new System.Windows.Forms.ComboBox();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.valueSVNPath = new System.Windows.Forms.TextBox();
      this.valueDatabase = new System.Windows.Forms.TextBox();
      this.valueServer = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.valueName = new System.Windows.Forms.TextBox();
      this.tabPageUsers = new System.Windows.Forms.TabPage();
      this.buttonAddUser = new System.Windows.Forms.Button();
      this.buttonEditUser = new System.Windows.Forms.Button();
      this.buttonDeleteUser = new System.Windows.Forms.Button();
      this.users = new System.Windows.Forms.ListBox();
      this.tabPageGroups = new System.Windows.Forms.TabPage();
      this.groupUsers = new System.Windows.Forms.CheckedListBox();
      this.groups = new System.Windows.Forms.ListBox();
      this.buttonAddGroup = new System.Windows.Forms.Button();
      this.buttonEditGroup = new System.Windows.Forms.Button();
      this.buttonDeleteGroup = new System.Windows.Forms.Button();
      this.buttonCancel = new System.Windows.Forms.Button();
      this.buttonOK = new System.Windows.Forms.Button();
      this.label6 = new System.Windows.Forms.Label();
      this.valueTranslate = new System.Windows.Forms.TextBox();
      this.tabControl.SuspendLayout();
      this.tabPageCommon.SuspendLayout();
      this.tabPageUsers.SuspendLayout();
      this.tabPageGroups.SuspendLayout();
      this.SuspendLayout();
      // 
      // tabControl
      // 
      this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.tabControl.Controls.Add(this.tabPageCommon);
      this.tabControl.Controls.Add(this.tabPageUsers);
      this.tabControl.Controls.Add(this.tabPageGroups);
      this.tabControl.Location = new System.Drawing.Point(12, 12);
      this.tabControl.Name = "tabControl";
      this.tabControl.SelectedIndex = 0;
      this.tabControl.Size = new System.Drawing.Size(370, 296);
      this.tabControl.TabIndex = 0;
      // 
      // tabPageCommon
      // 
      this.tabPageCommon.Controls.Add(this.label6);
      this.tabPageCommon.Controls.Add(this.valueTranslate);
      this.tabPageCommon.Controls.Add(this.label5);
      this.tabPageCommon.Controls.Add(this.valueServerType);
      this.tabPageCommon.Controls.Add(this.label4);
      this.tabPageCommon.Controls.Add(this.label3);
      this.tabPageCommon.Controls.Add(this.label2);
      this.tabPageCommon.Controls.Add(this.valueSVNPath);
      this.tabPageCommon.Controls.Add(this.valueDatabase);
      this.tabPageCommon.Controls.Add(this.valueServer);
      this.tabPageCommon.Controls.Add(this.label1);
      this.tabPageCommon.Controls.Add(this.valueName);
      this.tabPageCommon.Location = new System.Drawing.Point(4, 22);
      this.tabPageCommon.Name = "tabPageCommon";
      this.tabPageCommon.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageCommon.Size = new System.Drawing.Size(362, 270);
      this.tabPageCommon.TabIndex = 0;
      this.tabPageCommon.Text = "Common";
      this.tabPageCommon.UseVisualStyleBackColor = true;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(6, 37);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(64, 13);
      this.label5.TabIndex = 9;
      this.label5.Text = "Server type:";
      // 
      // valueServerType
      // 
      this.valueServerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.valueServerType.FormattingEnabled = true;
      this.valueServerType.Location = new System.Drawing.Point(85, 34);
      this.valueServerType.Name = "valueServerType";
      this.valueServerType.Size = new System.Drawing.Size(271, 21);
      this.valueServerType.TabIndex = 8;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(6, 114);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(56, 13);
      this.label4.TabIndex = 7;
      this.label4.Text = "SVN path:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(6, 88);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(56, 13);
      this.label3.TabIndex = 6;
      this.label3.Text = "Database:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(6, 62);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(41, 13);
      this.label2.TabIndex = 5;
      this.label2.Text = "Server:";
      // 
      // valueSVNPath
      // 
      this.valueSVNPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueSVNPath.Location = new System.Drawing.Point(85, 111);
      this.valueSVNPath.Name = "valueSVNPath";
      this.valueSVNPath.Size = new System.Drawing.Size(271, 20);
      this.valueSVNPath.TabIndex = 4;
      // 
      // valueDatabase
      // 
      this.valueDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueDatabase.Location = new System.Drawing.Point(85, 85);
      this.valueDatabase.Name = "valueDatabase";
      this.valueDatabase.Size = new System.Drawing.Size(271, 20);
      this.valueDatabase.TabIndex = 3;
      // 
      // valueServer
      // 
      this.valueServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueServer.Location = new System.Drawing.Point(85, 59);
      this.valueServer.Name = "valueServer";
      this.valueServer.Size = new System.Drawing.Size(271, 20);
      this.valueServer.TabIndex = 2;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(6, 10);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(73, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Display name:";
      // 
      // valueName
      // 
      this.valueName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueName.Enabled = false;
      this.valueName.Location = new System.Drawing.Point(85, 7);
      this.valueName.Name = "valueName";
      this.valueName.Size = new System.Drawing.Size(271, 20);
      this.valueName.TabIndex = 0;
      // 
      // tabPageUsers
      // 
      this.tabPageUsers.Controls.Add(this.buttonAddUser);
      this.tabPageUsers.Controls.Add(this.buttonEditUser);
      this.tabPageUsers.Controls.Add(this.buttonDeleteUser);
      this.tabPageUsers.Controls.Add(this.users);
      this.tabPageUsers.Location = new System.Drawing.Point(4, 22);
      this.tabPageUsers.Name = "tabPageUsers";
      this.tabPageUsers.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageUsers.Size = new System.Drawing.Size(362, 270);
      this.tabPageUsers.TabIndex = 1;
      this.tabPageUsers.Text = "Users";
      this.tabPageUsers.UseVisualStyleBackColor = true;
      // 
      // buttonAddUser
      // 
      this.buttonAddUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonAddUser.Location = new System.Drawing.Point(118, 241);
      this.buttonAddUser.Name = "buttonAddUser";
      this.buttonAddUser.Size = new System.Drawing.Size(75, 23);
      this.buttonAddUser.TabIndex = 3;
      this.buttonAddUser.Text = "Add";
      this.buttonAddUser.UseVisualStyleBackColor = true;
      this.buttonAddUser.Click += new System.EventHandler(this.buttonAddUser_Click);
      // 
      // buttonEditUser
      // 
      this.buttonEditUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonEditUser.Location = new System.Drawing.Point(199, 241);
      this.buttonEditUser.Name = "buttonEditUser";
      this.buttonEditUser.Size = new System.Drawing.Size(75, 23);
      this.buttonEditUser.TabIndex = 2;
      this.buttonEditUser.Text = "Edit";
      this.buttonEditUser.UseVisualStyleBackColor = true;
      this.buttonEditUser.Click += new System.EventHandler(this.buttonEditUser_Click);
      // 
      // buttonDeleteUser
      // 
      this.buttonDeleteUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonDeleteUser.Location = new System.Drawing.Point(280, 241);
      this.buttonDeleteUser.Name = "buttonDeleteUser";
      this.buttonDeleteUser.Size = new System.Drawing.Size(75, 23);
      this.buttonDeleteUser.TabIndex = 1;
      this.buttonDeleteUser.Text = "Delete";
      this.buttonDeleteUser.UseVisualStyleBackColor = true;
      this.buttonDeleteUser.Click += new System.EventHandler(this.buttonDeleteUser_Click);
      // 
      // users
      // 
      this.users.FormattingEnabled = true;
      this.users.Location = new System.Drawing.Point(7, 7);
      this.users.Name = "users";
      this.users.Size = new System.Drawing.Size(349, 225);
      this.users.TabIndex = 0;
      this.users.SelectedIndexChanged += new System.EventHandler(this.users_SelectedIndexChanged);
      // 
      // tabPageGroups
      // 
      this.tabPageGroups.Controls.Add(this.groupUsers);
      this.tabPageGroups.Controls.Add(this.groups);
      this.tabPageGroups.Controls.Add(this.buttonAddGroup);
      this.tabPageGroups.Controls.Add(this.buttonEditGroup);
      this.tabPageGroups.Controls.Add(this.buttonDeleteGroup);
      this.tabPageGroups.Location = new System.Drawing.Point(4, 22);
      this.tabPageGroups.Name = "tabPageGroups";
      this.tabPageGroups.Padding = new System.Windows.Forms.Padding(3);
      this.tabPageGroups.Size = new System.Drawing.Size(362, 270);
      this.tabPageGroups.TabIndex = 2;
      this.tabPageGroups.Text = "Groups";
      this.tabPageGroups.UseVisualStyleBackColor = true;
      // 
      // groupUsers
      // 
      this.groupUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.groupUsers.FormattingEnabled = true;
      this.groupUsers.Location = new System.Drawing.Point(165, 10);
      this.groupUsers.Name = "groupUsers";
      this.groupUsers.Size = new System.Drawing.Size(191, 214);
      this.groupUsers.TabIndex = 8;
      this.groupUsers.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.groupUsers_ItemCheck);
      // 
      // groups
      // 
      this.groups.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
      this.groups.FormattingEnabled = true;
      this.groups.Location = new System.Drawing.Point(3, 10);
      this.groups.Name = "groups";
      this.groups.Size = new System.Drawing.Size(156, 225);
      this.groups.TabIndex = 7;
      this.groups.SelectedIndexChanged += new System.EventHandler(this.groups_SelectedIndexChanged);
      // 
      // buttonAddGroup
      // 
      this.buttonAddGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.buttonAddGroup.Location = new System.Drawing.Point(6, 241);
      this.buttonAddGroup.Name = "buttonAddGroup";
      this.buttonAddGroup.Size = new System.Drawing.Size(75, 23);
      this.buttonAddGroup.TabIndex = 6;
      this.buttonAddGroup.Text = "Add";
      this.buttonAddGroup.UseVisualStyleBackColor = true;
      this.buttonAddGroup.Click += new System.EventHandler(this.buttonGroupAdd_Click);
      // 
      // buttonEditGroup
      // 
      this.buttonEditGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.buttonEditGroup.Location = new System.Drawing.Point(87, 241);
      this.buttonEditGroup.Name = "buttonEditGroup";
      this.buttonEditGroup.Size = new System.Drawing.Size(75, 23);
      this.buttonEditGroup.TabIndex = 5;
      this.buttonEditGroup.Text = "Edit";
      this.buttonEditGroup.UseVisualStyleBackColor = true;
      this.buttonEditGroup.Click += new System.EventHandler(this.buttonGroupEdit_Click);
      // 
      // buttonDeleteGroup
      // 
      this.buttonDeleteGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.buttonDeleteGroup.Location = new System.Drawing.Point(168, 241);
      this.buttonDeleteGroup.Name = "buttonDeleteGroup";
      this.buttonDeleteGroup.Size = new System.Drawing.Size(75, 23);
      this.buttonDeleteGroup.TabIndex = 4;
      this.buttonDeleteGroup.Text = "Delete";
      this.buttonDeleteGroup.UseVisualStyleBackColor = true;
      this.buttonDeleteGroup.Click += new System.EventHandler(this.buttonGroupDelete_Click);
      // 
      // buttonCancel
      // 
      this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.buttonCancel.Location = new System.Drawing.Point(315, 314);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.Size = new System.Drawing.Size(75, 23);
      this.buttonCancel.TabIndex = 1;
      this.buttonCancel.Text = "Cancel";
      this.buttonCancel.UseVisualStyleBackColor = true;
      // 
      // buttonOK
      // 
      this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.buttonOK.Location = new System.Drawing.Point(234, 313);
      this.buttonOK.Name = "buttonOK";
      this.buttonOK.Size = new System.Drawing.Size(75, 23);
      this.buttonOK.TabIndex = 2;
      this.buttonOK.Text = "Save";
      this.buttonOK.UseVisualStyleBackColor = true;
      this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(6, 140);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(62, 13);
      this.label6.TabIndex = 11;
      this.label6.Text = "Translation:";
      // 
      // valueTranslate
      // 
      this.valueTranslate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueTranslate.Location = new System.Drawing.Point(85, 137);
      this.valueTranslate.Name = "valueTranslate";
      this.valueTranslate.Size = new System.Drawing.Size(271, 20);
      this.valueTranslate.TabIndex = 10;
      // 
      // DialogDatabaseManagement
      // 
      this.AcceptButton = this.buttonOK;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.buttonCancel;
      this.ClientSize = new System.Drawing.Size(394, 342);
      this.Controls.Add(this.buttonOK);
      this.Controls.Add(this.buttonCancel);
      this.Controls.Add(this.tabControl);
      this.MinimumSize = new System.Drawing.Size(300, 270);
      this.Name = "DialogDatabaseManagement";
      this.Text = "Database Management";
      this.Load += new System.EventHandler(this.DialogDatabaseManagement_Load);
      this.tabControl.ResumeLayout(false);
      this.tabPageCommon.ResumeLayout(false);
      this.tabPageCommon.PerformLayout();
      this.tabPageUsers.ResumeLayout(false);
      this.tabPageGroups.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TabControl tabControl;
    private System.Windows.Forms.TabPage tabPageCommon;
    private System.Windows.Forms.TabPage tabPageUsers;
    private System.Windows.Forms.TabPage tabPageGroups;
    private System.Windows.Forms.Button buttonCancel;
    private System.Windows.Forms.Button buttonOK;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox valueName;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox valueSVNPath;
    private System.Windows.Forms.TextBox valueDatabase;
    private System.Windows.Forms.ListBox users;
    private System.Windows.Forms.Button buttonEditUser;
    private System.Windows.Forms.Button buttonDeleteUser;
    private System.Windows.Forms.Button buttonAddUser;
    private System.Windows.Forms.TextBox valueServer;
    private System.Windows.Forms.Button buttonAddGroup;
    private System.Windows.Forms.Button buttonEditGroup;
    private System.Windows.Forms.Button buttonDeleteGroup;
    private System.Windows.Forms.CheckedListBox groupUsers;
    private System.Windows.Forms.ListBox groups;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.ComboBox valueServerType;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.TextBox valueTranslate;
  }
}