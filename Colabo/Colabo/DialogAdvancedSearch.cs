using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Colabo
{
  public partial class DialogAdvancedSearch : Colabo.DialogSearchResults
  {
    public DialogAdvancedSearch(FormColabo owner) : base(owner)
    {
      InitializeComponent();
      _originalTitle = this.Text;
    }

    // create a query string from the dialog controls
    public override string GetQuery()
    {
      string query = "";
      if (!string.IsNullOrEmpty(valueContent.Text))
        QueryAppendAnd(ref query, string.Format("content:({0})", valueContent.Text));
      if (!string.IsNullOrEmpty(valueAuthor.Text))
        QueryAppendAnd(ref query, string.Format("author:({0})", valueAuthor.Text));
      if (valueTimeFrom.Checked || valueTimeTo.Checked)
      {
        DateTime from = valueTimeFrom.Checked ? valueTimeFrom.Value : DateTime.MinValue;
        DateTime to = valueTimeTo.Checked ? valueTimeTo.Value : DateTime.MaxValue;
        QueryAppendAnd(ref query, string.Format("time:[{0:yyyyMMddHHmm} TO {1:yyyyMMddHHmm}]", from, to));
      }
      if (!string.IsNullOrEmpty(valueTitle.Text))
        QueryAppendAnd(ref query, string.Format("title:({0})", valueTitle.Text));
      return query;
    }
    private void QueryAppendAnd(ref string query, string append)
    {
      if (query.Length > 0) query += " AND ";
      query += append;
    }

    // create a user readable query string (used in the dialog title)
    public override string GetQueryReadable()
    {
      string queryReadable = "";
      if (!string.IsNullOrEmpty(valueContent.Text))
        queryReadable += string.Format(" {0}", valueContent.Text);
      if (!string.IsNullOrEmpty(valueAuthor.Text))
        queryReadable += string.Format(" by {0}", valueAuthor.Text);
      if (valueTimeFrom.Checked)
        queryReadable += string.Format(" from {0}", valueTimeFrom.Value);
      if (valueTimeTo.Checked)
        queryReadable += string.Format(" to {0}", valueTimeTo.Value);
      if (!string.IsNullOrEmpty(valueTitle.Text))
        queryReadable += string.Format(" Title: {0}", valueTitle.Text);
      return queryReadable;
    }

    // helper to disable some controls when search is in progress
    protected override void EnableSearch(bool enable)
    {
      valueContent.Enabled = enable;
      valueAuthor.Enabled = enable;
      valueTimeFrom.Enabled = enable;
      valueTimeTo.Enabled = enable;
      valueTitle.Enabled = enable;
    }

    private void buttonSearch_Click(object sender, EventArgs e)
    {
      StartSearch();
    }
    private void buttonCancel_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}

