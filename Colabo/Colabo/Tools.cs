using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.ComponentModel;

namespace Colabo
{
  class Tools
  {
#if false
    static public void MakeURLsRelative(FormColabo owner)
    {
      foreach (SQLDatabase db in owner.databases)
      {
        string root = db.svnRoot;
        if (string.IsNullOrEmpty(root)) continue;

        // change URLs in headers (SQL database)
        /*
        foreach (ArticleItem article in db.cache.Values)
        {
          string url = article.RawURL;
          string newUrl = db.ToRelative(url);
          if (newUrl != url)
          {
            article.RawURL = newUrl;
            if (article._ownTags == null) article._ownTags = new List<string>();
            
            db.SendArticle(article);
            System.Console.WriteLine("URL '{0}' => '{1}'", url, newUrl);
          }
        }
        */
 
        // change links inside articles
        ITask task = new TaskMakeLinksRelative(db);
        owner.AddTask(task);
      }
    }

    class TaskMakeLinksRelative : ITask
    {
      private SQLDatabase _database;

      public override string ProgressType() { return "Send"; }

      public TaskMakeLinksRelative(SQLDatabase database)
      {
        _database = database;
      }

      public override void Process(BackgroundWorker worker)
      {
        string message = "Changing links ...";
        worker.ReportProgress(0, message);

        ArticlesStore svn = _database.GetArticlesStore();

        int i = 0;
        int count = _database.cache.Count;
        foreach (ArticleItem article in _database.cache.Values)
        {
          worker.ReportProgress(100 * i / count);
          i++;

          svn.UpdateWorkingCopy(_database, article, true, int.MaxValue); // update the article, then append the note

          string path = article._workingCopy;
          if (path.Length > 0 && File.Exists(path))
          {
            // read the file
            StreamReader reader = new StreamReader(path, Encoding.UTF8);
            string content = reader.ReadToEnd();
            reader.Close();

            // fix all links
            string newContent = "";
            int done = 0;
            int index = content.IndexOf(_database.svnRoot);
            while (index >= done)
            {
              newContent += content.Substring(done, index - done);
              done = index + _database.svnRoot.Length;
              index = content.IndexOf(_database.svnRoot, done);
            }
            newContent += content.Substring(done);

            // save the file
            StreamWriter writer = new StreamWriter(path, false, System.Text.Encoding.UTF8);
            writer.Write(newContent);
            writer.Close();

            // commit
            // svn.Commit(path, _item._title);
            // svn.UpdateRevisionAndDate(path, true, ref _item);
          }
        }

        worker.ReportProgress(100);
      }

      public override void Finish()
      {
        // update revision number in database
        // _database.UpdateRevisionAndDate(_item);
      }
    }
#endif
    /*
      // Delete "{COMA}\r\n" and "MARKDOWN\r\n" from the beginning of every article.
      static public void SwitchToComa(ArticlesCache cache, FormColabo owner)
      {
          // recursively visit all articles
          foreach (ArticleItem item in cache.Values) {
              
              // change the article asynchronously if it's in the SVN (HACK)
              if (item._url.StartsWith("https://dev.bistudio.com/svn")) {
                  ITask task = new TaskSwitchArticleToComa(item, owner);
                  owner.AddTask(task);
              }
          }
      }

      class TaskSwitchArticleToComa : ITask
      {
          private ArticleItem _item;
          private FormColabo _owner;

          public TaskSwitchArticleToComa(ArticleItem item, FormColabo owner)
          {
              _item = item;
              _owner = owner;
          }

          public override void Process(BackgroundWorker worker)
          {
              string message = "Converting to Coma ...";
              worker.ReportProgress(0, message);

              _owner.svnDatabase.UpdateWorkingCopy(_item, true, int.MaxValue);  // update the article

              worker.ReportProgress(50);

              string path = _item._workingCopy;
              
              // append the note to the file and commit to SVN
              if (path.Length > 0 && File.Exists(path))
              {
                  // read the input file
                  StreamReader reader = new StreamReader(path, System.Text.Encoding.UTF8);
                  string articleText = reader.ReadToEnd();
                  reader.Close();

                  // delete {COMA} or MARKDOWN
                  if ( articleText.StartsWith("{COMA}\r\n") || articleText.StartsWith("MARKDOWN\r\n") )
                  {
                      StreamWriter writer = new StreamWriter(path, false, System.Text.Encoding.UTF8);
                      writer.Write(articleText.Substring(8));
                      writer.Close();
//                      _owner.svnDatabase.Commit(path, _item._title);  // not yet, mind you :)
                  }
              }

              worker.ReportProgress(100);
          }

          public override void Finish()
          {
              // do nothing
          }
      }
*/

      
/*
    // procedure optimizing and renaming article tags (new prefixes meaning)
    static public void OptimizeTags(ArticlesCache cache, SQLDatabase database, FormColabo owner)
    {
      // recursively visit all articles
      List<string> inheritedTags = new List<string>();
      foreach (ArticleItem item in cache.Values)
      {
        if (item._parent < 0) UpdateTags(cache, item, inheritedTags);
      }

      // store tags to database
      database.StoreTagsToDB(cache);

      // store tags to SVN
      database.GetArticleStores().StoreTagsToSVN(database, cache);
    }
    static public void UpdateTags(ArticlesCache cache, ArticleItem item, List<string> tags)
    {
      List<string> newTags = new List<string>();
      List<string> inheritedTags = new List<string>(tags);

      // stop inheriting tags
      List<string> toRemove = new List<string>();
      foreach (string tag in inheritedTags)
      {
        if (item._ownTags != null)
        {
          if (item._ownTags.Contains(tag)) continue;
          if (item._ownTags.Contains("+" + tag)) continue;
        }
        if (item._inheritedTags != null)
        {
          if (item._inheritedTags.Contains(tag)) continue;
        }

        item._ownTags.Add("-" + tag);
        toRemove.Add(tag);
      }
      foreach (string tag in toRemove)
      {
        inheritedTags.Remove(tag);
      }

      // optimize list of tags of the current article
      if (item._ownTags != null) foreach (string tag in item._ownTags)
      {
        if (tag[0] == '+')
        {
          string t = tag.Substring(1).Trim();
          if (!newTags.Contains(t)) newTags.Add(t);
          if (!inheritedTags.Contains(t)) inheritedTags.Add(t);
        }
        else if (tag[0] == '-')
        {
          if (!newTags.Contains(tag)) newTags.Add(tag);
          string t = tag.Substring(1).Trim();
          inheritedTags.Remove(t);
        }
        else if (!inheritedTags.Contains(tag))
        {
          bool someChildInherits;
          if (item._children == null || item._children.Count == 0) someChildInherits = true;
          else
          {
            someChildInherits = false;
            foreach (int i in item._children)
            {
              ArticleItem child;
              if (cache.TryGetValue(i, out child))
              {
                if (child._ownTags != null && child._ownTags.Contains(tag))
                {
                  someChildInherits = true;
                  break;
                }
              }
            }
          }

          if (someChildInherits)
          {
            if (!newTags.Contains(tag)) newTags.Add(tag);
            if (!inheritedTags.Contains(tag)) inheritedTags.Add(tag);
          }
          else // replace tag by #tag
          {
            string t = "#" + tag;
            if (!newTags.Contains(t)) newTags.Add(t);
          }
        }
      }

      // set the new tags
      item._ownTags = newTags;
      item._inheritedTags = tags; // tags inherited from the
      item.UpdateEffectiveTags();

      // recursively call for children
      if (item._children != null) foreach (int i in item._children)
      {
        ArticleItem child;
        if (cache.TryGetValue(i, out child)) UpdateTags(cache, child, inheritedTags);
      }
    }
*/ 
  }
}