using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Xml;

namespace Colabo
{
  public partial class ImportWiki : Form
  {
    public Colabo.FormColabo _colabo;
    
    public StringBuilder _workingCopyRoot;

    public ImportWiki(Colabo.FormColabo colabo)
    {
      _colabo = colabo;
      // find the working copy root for the svnrepository
      _workingCopyRoot = new StringBuilder();

      InitializeComponent();
    }

    private void ImportWiki_Load(object sender, EventArgs e)
    {
      // create list of databases
      for (int i=0; i<_colabo.databases.Count; i++)
      {
        databasesList.Items.Add(_colabo.databases[i].name);
      }
      databasesList.SelectedIndex = 0;
    }

    class TaskWiki2Colabo : Colabo.ITask
    {
      ImportWiki parent;
      string svnRepository;
      SQLDatabase database;
      string failed = null;
      bool importCategory;

      private StringBuilder wikiOutput = null;

      public override string ProgressType() { return "Import"; }

      public TaskWiki2Colabo(ImportWiki parent, SQLDatabase database)
      {
        this.parent = parent;
        this.database = database;
        this.svnRepository = database.svnRepository;
        this.importCategory = parent.importTypeCombo.SelectedIndex == 0;
      }

      public override void Process(BackgroundWorker worker)
      {
        string message = "Downloading the Wiki articles...";
        worker.ReportProgress(10, message);

        // Initialize the process and its StartInfo properties.
        // The sort command is a console application that
        // reads and sorts text input.

        Process wiki2ColaboProcess;
        
        wiki2ColaboProcess = new Process();
        wiki2ColaboProcess.StartInfo.FileName = "wiki2Colabo.exe";
        if (importCategory) //category
        { //download all articles from given category
          wiki2ColaboProcess.StartInfo.Arguments = string.Format("\"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\"",
                 parent.category.Text, parent.username.Text, parent.password.Text,
                 parent._workingCopyRoot.Append(@"wiki").ToString(), svnRepository.TrimEnd(' ') + @"wiki/");
        }
        else
        { //download only single article (full path or article title should work)
          wiki2ColaboProcess.StartInfo.Arguments = string.Format("-1 \"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\"",
                 parent.username.Text, parent.password.Text, parent._workingCopyRoot.Append(@"wiki").ToString(), 
                 svnRepository.TrimEnd(' ') + @"wiki/", parent.category.Text);
        }

        // Start the process.
        try {
          if (!wiki2ColaboProcess.Start())
          {
            // FAILED to start wiki2colabo.exe
            failed = "Error: cannot run wiki2colabo.exe\r\nExecutable is possibly not located at the colabo directory.\r\n";
            return;
          }
        }
        catch //(Win32Exception e)
        {
          // FAILED to start wiki2colabo.exe
          failed = "Error: cannot run wiki2colabo.exe\r\nExecutable is possibly not located at the colabo directory.\r\n";
          return;
        }
        // Wait for the sort process to write the sorted text lines.
        wiki2ColaboProcess.WaitForExit();

        wiki2ColaboProcess.Close();

        worker.ReportProgress(100, message);
      }

      class WikiArticle : Colabo.ArticleItem
      {
        public string _path;
        public WikiArticle() {}
      }
      private bool ParseWikiXML(out List<WikiArticle> articles, out List<ImportedArticle> images)
      {
        string xmlFileName;
        if (importCategory) //category
          xmlFileName = parent.category.Text + ".xml";
        else
          xmlFileName = "wikiImportInfo.xml";
        articles = new List<WikiArticle>();
        images = new List<ImportedArticle>();
        if (!File.Exists(xmlFileName)) return false;

        // read the configuration
        XmlDocument file = new XmlDocument();
        file.Load(xmlFileName);

        XmlElement clsWiki = (XmlElement)file.SelectSingleNode("Wiki");
        // articles
        XmlElement cls = (XmlElement)clsWiki.SelectSingleNode("WikiArticles");
        if (cls != null)
        {
          XmlNodeList list = cls.SelectNodes("Article");
          for (int i = 0; i < list.Count; i++)
          {
            WikiArticle article = new WikiArticle();
            XmlElement articleCls = (XmlElement)list[i];
            //title
            XmlElement el = (XmlElement)articleCls.SelectSingleNode("Title");
            article._title = el.InnerText;
            //tags
            el = (XmlElement)articleCls.SelectSingleNode("Categories");
            string[] tags = el.InnerText.Split(',');
            for (int j=0; j<tags.Length; j++)
            {
              article.AddTag(tags[j]);
            }
            //author
            el = (XmlElement)articleCls.SelectSingleNode("Author");
            article._author = el.InnerText;
            //date
            el = (XmlElement)articleCls.SelectSingleNode("Date");
            article._date = DateTime.Parse(el.InnerText);
            //url
            el = (XmlElement)articleCls.SelectSingleNode("Url");
            string fullURL = el.InnerText;
            article.RawURL = database.ToRelative(fullURL);
            //path
            el = (XmlElement)articleCls.SelectSingleNode("Path");
            article._path = el.InnerText;
            //add parsed article at last
            articles.Add(article);
          }
        }
        // images
        cls = (XmlElement)clsWiki.SelectSingleNode("WikiImages");
        if (cls != null)
        {
          XmlNodeList list = cls.SelectNodes("Image");
          for (int i = 0; i < list.Count; i++)
          {
            //attribute path
            XmlElement el = (XmlElement)list[i];
            images.Add( new ImportedArticle(el.GetAttribute("path"), null) );
          }
        }
        return true;
      }
      
      public override void Finish()
      {
        if (!string.IsNullOrEmpty(failed))
        {
          parent.wikiOutput.Text = failed;
          return;
        }
        //parse the resulting XML file and add listed articles to Colabo and commit folders (including images)
        List<WikiArticle> articles;
        List<ImportedArticle> images;
        StringBuilder importlist = new StringBuilder();  //import info shown inside wikiOutput edit box of ImportWiki dialog
        if (!ParseWikiXML(out articles, out images))
        {
          importlist.AppendLine("Error: cannot parse the import file");
          importlist.AppendLine(string.Format("File: '{0}'", parent.category.Text + ".xml"));
          parent.wikiOutput.Text = importlist.ToString();
          return; //failed
        }

        // prepare file list to final commit
        List<ImportedArticle> wikiFileList = new List<ImportedArticle>();
        for (int i = 0; i < articles.Count; i++)
        {
          // commit the document to the SVN with correct properties
          string value = "";
          WikiArticle article = articles[i];
          if (article._ownTags != null)
          {
            for (int k = 0; k < article._ownTags.Count; k++)
            {
              string tag = article._ownTags[k].Trim();
              // do not store private tags
              if (Colabo.ArticleItem.FromPrivateTag(tag) != null) continue;
              if (tag.Length > 0)
              {
                if (value.Length > 0) value = value + "\n";
                value = value + tag;
              }
            }
          }
          wikiFileList.Add(new ImportedArticle(articles[i]._path, value));
        }
        wikiFileList.AddRange(images);

        // Send articles to database
        for (int i = 0; i < articles.Count; i++)
        {
          // send it to database
          WikiArticle article = articles[i];
          parent._colabo.SendArticle(database, article);
        }
        // COMMIT asynchronously
        Colabo.ITask task = new Colabo.TaskCommitWorkingPathRoot(parent._workingCopyRoot.ToString(), "Importing Articles from Wiki", wikiFileList, database.GetArticlesStore());
        parent._colabo.AddTask(task);
        // DONE
        importlist.AppendLine("[[[ Imported Articles ]]]");
        importlist.AppendLine("----------------------------------------------");
        for (int i = 0; i < articles.Count; i++) importlist.AppendLine(articles[i]._title);
        importlist.AppendLine("----------------------------------------------");
        importlist.AppendLine("[[[ Imported Images ]]]");
        importlist.AppendLine("----------------------------------------------");
        for (int i = 0; i < images.Count; i++)
        {
          int ix = images[i]._path.LastIndexOf("wiki\\images\\");
          importlist.AppendLine(images[i]._path.Substring(ix));
        }
        parent.wikiOutput.Text = importlist.ToString();
      }
    }

    private void startImport_Click(object sender, EventArgs e)
    {
      SQLDatabase database = _colabo.databases[databasesList.SelectedIndex];
      _workingCopyRoot = new StringBuilder();
      Colabo.ITask taskWC = new Colabo.TaskGetWorkingPathRoot(database.svnRepository, _workingCopyRoot, database.GetArticlesStore());
      _colabo.AddTask(taskWC);

      Colabo.ITask taskImport = new TaskWiki2Colabo(this, database);
      _colabo.AddTask(taskImport);      // Create the process.
    }
  }
}