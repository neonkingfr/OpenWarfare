using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;

namespace Colabo
{
  public partial class DialogOptions : Form
  {
    FormColabo _owner;
    Configuration _config;

    public struct LocationEdit
    {
      public string Key { get; set; }
      private string _value;
      public string Value {get {return _value;} set {_value = value;}}
    };

    public DialogOptions(FormColabo owner, Configuration config)
    {
      _owner = owner;
      _config = config;
      InitializeComponent();

      markAsRead.Checked = _config.MarkAsRead;
      markAsReadTime.Value = _config.MarkAsReadTime;

      synchronize.Checked = _config.SynchronizeInterval > 0;
      synchronizeInterval.Value = _config.SynchronizeInterval > 0 ? _config.SynchronizeInterval : 1;
      awayAfterInterval.Value = _config.AwayAfterInterval;
      //location.Text = _config.StationLocation;
    }

    private void buttonOk_Click(object sender, EventArgs e)
    {
      _config.MarkAsRead = markAsRead.Checked;
      _config.MarkAsReadTime = (int)markAsReadTime.Value;

      if (synchronize.Checked) _config.SynchronizeInterval = (int)synchronizeInterval.Value;
      else _config.SynchronizeInterval = 0;
      _config.AwayAfterInterval = (int)awayAfterInterval.Value;

      Close();
    }

    private void buttonCancel_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void label3_Click(object sender, EventArgs e)
    {

    }

    private void synchronizeInterval_ValueChanged(object sender, EventArgs e)
    {

    }

    private void DialogOptions_Load(object sender, EventArgs e)
    {

    }

    private void location_TextChanged(object sender, EventArgs e)
    {

    }

    private void locationNames_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {

    }
  }
}