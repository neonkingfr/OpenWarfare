﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

// own implementation of autocomplete

namespace Colabo
{
  public interface ICollectCandidates
  {
    void Process(string prefix, ref SortedList<string, int> result);
  }

  public class Autocomplete
  {
    private TextBox _owner;
    private ICollectCandidates _collect;

    private ToolStripDropDown _list = null;
    private int _startWord;
    private int _endWord;

    public Autocomplete(TextBox owner, ICollectCandidates collect)
    {
      _owner = owner;
      _collect = collect;
    }

    public bool OnKeyDown(KeyEventArgs e)
    {
      switch (e.KeyCode)
      {
        case Keys.Tab:
        case Keys.Enter:
          if (_list != null)
          {
            int index = FindSelectedIndex();
            if (index >= 0 && index < _list.Items.Count)
            {
              string replace = _list.Items[index].Text;
              _owner.Text = _owner.Text.Substring(0, _startWord) + replace + _owner.Text.Substring(_endWord);
              _owner.SelectionStart = _startWord + replace.Length;
              _owner.SelectionLength = 0;
            }
            e.Handled = true;
            e.SuppressKeyPress = true;
            return true;
          }
          break;
        case Keys.Down:
          if (_list != null)
          {
            int index = FindSelectedIndex() + 1;
            if (index >= _list.Items.Count) index = _list.Items.Count - 1;
            if (index < 0) index = 0;
            _list.Items[index].Select();
            e.Handled = true;
            e.SuppressKeyPress = true;
            return true;
          }
          break;
        case Keys.Up:
          if (_list != null)
          {
            int index = FindSelectedIndex() - 1;
            if (index >= _list.Items.Count) index = _list.Items.Count - 1;
            if (index < 0) index = 0;
            _list.Items[index].Select();
            e.Handled = true;
            e.SuppressKeyPress = true;
            return true;
          }
          break;
      }
      // not handled
      return false;
    }

    public bool OnKeyUp(KeyEventArgs e)
    {
      if (_list != null)
      {
        switch (e.KeyCode)
        {
          case Keys.Tab:
          case Keys.Enter:
            // handle key, and close the list
            e.Handled = true;
            e.SuppressKeyPress = true;
            break;
          case Keys.Down:
          case Keys.Up:
            // handle key, do not close the list
            e.Handled = true;
            e.SuppressKeyPress = true;
            return true;
          case Keys.ShiftKey:
          case Keys.ControlKey:
          case Keys.Menu:
          case Keys.LMenu:
          case Keys.RMenu:
            // do not close the list
            return true;
        }
        // for other keys, destroy the old list
        _list.Dispose();
        _list = null;
      }

      if (_owner.SelectionLength == 0 && IsPrintable(e.KeyCode)) OfferAutocomplete();
      return true;
    }

    public void OnLostFocus(EventArgs e)
    {
      if (_list != null)
      {
        _list.Dispose();
        _list = null;
      }
    }

    // implementation
    private void OfferAutocomplete()
    {
      // find the prefix
      char[] separators = { ',', ' ', '\t', '\r', '\n', '\f' };

      int endWord = _owner.SelectionStart;
      string text = _owner.Text.Substring(0, endWord);
      int startWord = text.LastIndexOfAny(separators) + 1;
      if (startWord < 0) startWord = 0;
      // skip special characters, check if prefix is not empty
      if (startWord >= endWord) return;
      if (text[startWord] == TagSpecialChars.PrefixLocal)
      {
        startWord++;
        if (startWord >= endWord) return;
      }
      if (text[startWord] == TagSpecialChars.PrefixRemove)
      {
        startWord++;
        if (startWord >= endWord) return;
      }
      string prefix = text.Substring(startWord, endWord - startWord).ToLower();

      // collect candidates 
      SortedList<string, int> candidates = new SortedList<string, int>();
      _collect.Process(prefix, ref candidates);
      if (candidates.Count == 0) return;

      // find the best candidate
      int bestIndex = 0;
      int maxValue = candidates.Values[0];
      for (int i = 1; i < candidates.Count; i++)
      {
        int value = candidates.Values[i];
        if (value > maxValue)
        {
          bestIndex = i;
          maxValue = value;
        }
      }

      // offer the hint
      string hint = candidates.Keys[bestIndex];
      // paste rest of the hint to the selection
      string toAdd = hint.Substring(prefix.Length);
      _owner.Text = text + toAdd + _owner.Text.Substring(endWord);
      _owner.SelectionStart = endWord;
      _owner.SelectionLength = toAdd.Length;

      // store the current word
      _startWord = startWord;
      _endWord = endWord + toAdd.Length;

      // offer the list
      _list = new ToolStripDropDown();
      for (int i = 0; i < candidates.Count; i++)
      {
        ToolStripMenuItem item = new ToolStripMenuItem(candidates.Keys[i], null, new EventHandler(OnAutocomplete));
        if (i == bestIndex) item.Select();
        _list.Items.Add(item);
      }
      Point position = new Point(_owner.Cursor.HotSpot.X, _owner.Height); // at the bottom edge of textBox
      _list.AutoClose = false;
      _list.Show(_owner.PointToScreen(position));
    }
    private void OnAutocomplete(object sender, EventArgs e)
    {
      string replace = ((ToolStripMenuItem)sender).Text;
      _owner.Text = _owner.Text.Substring(0, _startWord) + replace + _owner.Text.Substring(_endWord);
      _owner.SelectionStart = _startWord + replace.Length;
      _owner.SelectionLength = 0;
      // hide the list
      _list.Dispose();
      _list = null;
    }
    private bool IsPrintable(Keys key)
    {
      if (key == Keys.Space) return true;
      if (key >= Keys.D0 && key <= Keys.D9) return true;
      if (key >= Keys.A && key <= Keys.Z) return true;
      if (key >= Keys.NumPad0 && key <= Keys.Divide) return true; // NumPad keyboard
      if (key >= Keys.Oem1 && key <= Keys.Oem102) return true; // special characters on the main keyboard
      return false;
    }
    private int FindSelectedIndex()
    {
      for (int i = 0; i < _list.Items.Count; i++)
      {
        if (_list.Items[i].Selected) return i;
      }
      return -1;
    }
  }
}
