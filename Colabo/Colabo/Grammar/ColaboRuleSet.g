grammar ColaboRuleSet;

options
{
	language = CSharp2;
}

tokens
{
	OR = '|';
	AND = '&';
	NOT = '!';
	LPAR = '(';
	RPAR = ')';
	GT = '>';
	LT = '<';
	GE = '>=';
	LE = '<=';
	EQ = '==';
	NE = '!=';
	IMPL = '=>';
	END = ';';
}

@header
{
using System.Collections.Generic;
using System.Windows.Forms;
}

@members
{
  override public void DisplayRecognitionError(string[] tokenNames, RecognitionException e)
  {
    string header = GetErrorHeader(e);
    string message = GetErrorMessage(e, tokenNames);
    MessageBox.Show(header + "\n\n" + message, "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
  }

  public List<IToken> _result;
}

// Parser rules
ruleSet
@init {_result = new List<IToken>();}
	:       EOF | rule (END rule)*;
rule
	:	expression op = IMPL action {_result.Add($op);};
expression
	:	term (op = OR term {_result.Add($op);})*;
term
	:	factor (op = AND factor {_result.Add($op);})*;
factor
	:	(mask_or_id | comparison | (LPAR expression RPAR)) |
		(op = NOT (mask_or_id | comparison | (LPAR expression RPAR))) {_result.Add($op);};
comparison
	:	mask_or_id op = (GT | LT | GE | LE | EQ | NE) id {_result.Add($op);};
action
	:	val = IDENTIFIER {_result.Add($val);};
mask_or_id
	:	val = (MASK | IDENTIFIER) {_result.Add($val);};
id	
	:	val = IDENTIFIER {_result.Add($val);};
	
// Lexer rules	
WHITESPACE
	:	(' ' | '\t' | '\r' | '\n' | '\u000C')+ {$channel = HIDDEN;};

IDENTIFIER
	:	CHAR+ | ('"' (ESC_SEQ | ~('\\' | '"' | '*' | '?'))* '"');

MASK	
	:	(CHAR | '*' | '?')+ | ('"' (ESC_SEQ | ~('\\' | '"'))* '"');

// characters legal in identifiers
fragment CHAR:  ~(' ' | '\t' | '\r' | '\n' | '\u000C' | '\'' | '\\' | '\"' | ';' | '<' | '>' | '=' | '&' | '|' | '!' | '(' | ')' | '+' | '*' | '?' | ',')
    ;

// escape sequence is legal inside strings
fragment ESC_SEQ
    	:   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\'|'0')
    	|   UNICODE_ESC
    	|   HEXADECIMAL_ESC;

fragment HEXADECIMAL_ESC
    	:   '\\' 'x' HEX_DIGIT HEX_DIGIT;

fragment UNICODE_ESC
    	:   '\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT;

fragment HEX_DIGIT
	: ('0'..'9' | 'a'..'f' | 'A'..'F');
	