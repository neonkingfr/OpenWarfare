// $ANTLR 3.2 Sep 23, 2009 12:02:23 O:\\colabo\\Grammar\\ColaboRuleSet.g 2010-01-25 16:29:02

// The variable 'variable' is assigned but its value is never used.
#pragma warning disable 168, 219
// Unreachable code detected.
#pragma warning disable 162


using System.Collections.Generic;
using System.Windows.Forms;


using System;
using Antlr.Runtime;
using IList 		= System.Collections.IList;
using ArrayList 	= System.Collections.ArrayList;
using Stack 		= Antlr.Runtime.Collections.StackList;


public partial class ColaboRuleSetParser : Parser
{
    public static readonly string[] tokenNames = new string[] 
	{
        "<invalid>", 
		"<EOR>", 
		"<DOWN>", 
		"<UP>", 
		"OR", 
		"AND", 
		"NOT", 
		"LPAR", 
		"RPAR", 
		"GT", 
		"LT", 
		"GE", 
		"LE", 
		"EQ", 
		"NE", 
		"IMPL", 
		"END", 
		"IDENTIFIER", 
		"MASK", 
		"WHITESPACE", 
		"CHAR", 
		"ESC_SEQ", 
		"UNICODE_ESC", 
		"HEXADECIMAL_ESC", 
		"HEX_DIGIT"
    };

    public const int GE = 11;
    public const int LT = 10;
    public const int UNICODE_ESC = 22;
    public const int HEXADECIMAL_ESC = 23;
    public const int CHAR = 20;
    public const int HEX_DIGIT = 24;
    public const int WHITESPACE = 19;
    public const int NOT = 6;
    public const int AND = 5;
    public const int EOF = -1;
    public const int ESC_SEQ = 21;
    public const int MASK = 18;
    public const int LPAR = 7;
    public const int IDENTIFIER = 17;
    public const int OR = 4;
    public const int GT = 9;
    public const int RPAR = 8;
    public const int EQ = 13;
    public const int IMPL = 15;
    public const int END = 16;
    public const int LE = 12;
    public const int NE = 14;

    // delegates
    // delegators



        public ColaboRuleSetParser(ITokenStream input)
    		: this(input, new RecognizerSharedState()) {
        }

        public ColaboRuleSetParser(ITokenStream input, RecognizerSharedState state)
    		: base(input, state) {
            InitializeCyclicDFAs();

             
        }
        

    override public string[] TokenNames {
		get { return ColaboRuleSetParser.tokenNames; }
    }

    override public string GrammarFileName {
		get { return "O:\\colabo\\Grammar\\ColaboRuleSet.g"; }
    }


      override public void DisplayRecognitionError(string[] tokenNames, RecognitionException e)
      {
        string header = GetErrorHeader(e);
        string message = GetErrorMessage(e, tokenNames);
        MessageBox.Show(header + "\n\n" + message, "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }

      public List<IToken> _result;



    // $ANTLR start "ruleSet"
    // O:\\colabo\\Grammar\\ColaboRuleSet.g:44:1: ruleSet : ( EOF | rule ( END rule )* );
    public void ruleSet() // throws RecognitionException [1]
    {   
        _result = new List<IToken>();
        try 
    	{
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:46:2: ( EOF | rule ( END rule )* )
            int alt2 = 2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0 == EOF) )
            {
                alt2 = 1;
            }
            else if ( ((LA2_0 >= NOT && LA2_0 <= LPAR) || (LA2_0 >= IDENTIFIER && LA2_0 <= MASK)) )
            {
                alt2 = 2;
            }
            else 
            {
                NoViableAltException nvae_d2s0 =
                    new NoViableAltException("", 2, 0, input);

                throw nvae_d2s0;
            }
            switch (alt2) 
            {
                case 1 :
                    // O:\\colabo\\Grammar\\ColaboRuleSet.g:46:10: EOF
                    {
                    	Match(input,EOF,FOLLOW_EOF_in_ruleSet157); 

                    }
                    break;
                case 2 :
                    // O:\\colabo\\Grammar\\ColaboRuleSet.g:46:16: rule ( END rule )*
                    {
                    	PushFollow(FOLLOW_rule_in_ruleSet161);
                    	rule();
                    	state.followingStackPointer--;

                    	// O:\\colabo\\Grammar\\ColaboRuleSet.g:46:21: ( END rule )*
                    	do 
                    	{
                    	    int alt1 = 2;
                    	    int LA1_0 = input.LA(1);

                    	    if ( (LA1_0 == END) )
                    	    {
                    	        alt1 = 1;
                    	    }


                    	    switch (alt1) 
                    		{
                    			case 1 :
                    			    // O:\\colabo\\Grammar\\ColaboRuleSet.g:46:22: END rule
                    			    {
                    			    	Match(input,END,FOLLOW_END_in_ruleSet164); 
                    			    	PushFollow(FOLLOW_rule_in_ruleSet166);
                    			    	rule();
                    			    	state.followingStackPointer--;


                    			    }
                    			    break;

                    			default:
                    			    goto loop1;
                    	    }
                    	} while (true);

                    	loop1:
                    		;	// Stops C# compiler whining that label 'loop1' has no statements


                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return ;
    }
    // $ANTLR end "ruleSet"


    // $ANTLR start "rule"
    // O:\\colabo\\Grammar\\ColaboRuleSet.g:47:1: rule : expression op= IMPL action ;
    public void rule() // throws RecognitionException [1]
    {   
        IToken op = null;

        try 
    	{
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:48:2: ( expression op= IMPL action )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:48:4: expression op= IMPL action
            {
            	PushFollow(FOLLOW_expression_in_rule176);
            	expression();
            	state.followingStackPointer--;

            	op=(IToken)Match(input,IMPL,FOLLOW_IMPL_in_rule182); 
            	PushFollow(FOLLOW_action_in_rule184);
            	action();
            	state.followingStackPointer--;

            	_result.Add(op);

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return ;
    }
    // $ANTLR end "rule"


    // $ANTLR start "expression"
    // O:\\colabo\\Grammar\\ColaboRuleSet.g:49:1: expression : term (op= OR term )* ;
    public void expression() // throws RecognitionException [1]
    {   
        IToken op = null;

        try 
    	{
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:50:2: ( term (op= OR term )* )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:50:4: term (op= OR term )*
            {
            	PushFollow(FOLLOW_term_in_expression194);
            	term();
            	state.followingStackPointer--;

            	// O:\\colabo\\Grammar\\ColaboRuleSet.g:50:9: (op= OR term )*
            	do 
            	{
            	    int alt3 = 2;
            	    int LA3_0 = input.LA(1);

            	    if ( (LA3_0 == OR) )
            	    {
            	        alt3 = 1;
            	    }


            	    switch (alt3) 
            		{
            			case 1 :
            			    // O:\\colabo\\Grammar\\ColaboRuleSet.g:50:10: op= OR term
            			    {
            			    	op=(IToken)Match(input,OR,FOLLOW_OR_in_expression201); 
            			    	PushFollow(FOLLOW_term_in_expression203);
            			    	term();
            			    	state.followingStackPointer--;

            			    	_result.Add(op);

            			    }
            			    break;

            			default:
            			    goto loop3;
            	    }
            	} while (true);

            	loop3:
            		;	// Stops C# compiler whining that label 'loop3' has no statements


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return ;
    }
    // $ANTLR end "expression"


    // $ANTLR start "term"
    // O:\\colabo\\Grammar\\ColaboRuleSet.g:51:1: term : factor (op= AND factor )* ;
    public void term() // throws RecognitionException [1]
    {   
        IToken op = null;

        try 
    	{
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:52:2: ( factor (op= AND factor )* )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:52:4: factor (op= AND factor )*
            {
            	PushFollow(FOLLOW_factor_in_term215);
            	factor();
            	state.followingStackPointer--;

            	// O:\\colabo\\Grammar\\ColaboRuleSet.g:52:11: (op= AND factor )*
            	do 
            	{
            	    int alt4 = 2;
            	    int LA4_0 = input.LA(1);

            	    if ( (LA4_0 == AND) )
            	    {
            	        alt4 = 1;
            	    }


            	    switch (alt4) 
            		{
            			case 1 :
            			    // O:\\colabo\\Grammar\\ColaboRuleSet.g:52:12: op= AND factor
            			    {
            			    	op=(IToken)Match(input,AND,FOLLOW_AND_in_term222); 
            			    	PushFollow(FOLLOW_factor_in_term224);
            			    	factor();
            			    	state.followingStackPointer--;

            			    	_result.Add(op);

            			    }
            			    break;

            			default:
            			    goto loop4;
            	    }
            	} while (true);

            	loop4:
            		;	// Stops C# compiler whining that label 'loop4' has no statements


            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return ;
    }
    // $ANTLR end "term"


    // $ANTLR start "factor"
    // O:\\colabo\\Grammar\\ColaboRuleSet.g:53:1: factor : ( ( mask_or_id | comparison | ( LPAR expression RPAR ) ) | (op= NOT ( mask_or_id | comparison | ( LPAR expression RPAR ) ) ) );
    public void factor() // throws RecognitionException [1]
    {   
        IToken op = null;

        try 
    	{
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:54:2: ( ( mask_or_id | comparison | ( LPAR expression RPAR ) ) | (op= NOT ( mask_or_id | comparison | ( LPAR expression RPAR ) ) ) )
            int alt7 = 2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0 == LPAR || (LA7_0 >= IDENTIFIER && LA7_0 <= MASK)) )
            {
                alt7 = 1;
            }
            else if ( (LA7_0 == NOT) )
            {
                alt7 = 2;
            }
            else 
            {
                NoViableAltException nvae_d7s0 =
                    new NoViableAltException("", 7, 0, input);

                throw nvae_d7s0;
            }
            switch (alt7) 
            {
                case 1 :
                    // O:\\colabo\\Grammar\\ColaboRuleSet.g:54:4: ( mask_or_id | comparison | ( LPAR expression RPAR ) )
                    {
                    	// O:\\colabo\\Grammar\\ColaboRuleSet.g:54:4: ( mask_or_id | comparison | ( LPAR expression RPAR ) )
                    	int alt5 = 3;
                    	int LA5_0 = input.LA(1);

                    	if ( ((LA5_0 >= IDENTIFIER && LA5_0 <= MASK)) )
                    	{
                    	    int LA5_1 = input.LA(2);

                    	    if ( ((LA5_1 >= OR && LA5_1 <= AND) || LA5_1 == RPAR || LA5_1 == IMPL) )
                    	    {
                    	        alt5 = 1;
                    	    }
                    	    else if ( ((LA5_1 >= GT && LA5_1 <= NE)) )
                    	    {
                    	        alt5 = 2;
                    	    }
                    	    else 
                    	    {
                    	        NoViableAltException nvae_d5s1 =
                    	            new NoViableAltException("", 5, 1, input);

                    	        throw nvae_d5s1;
                    	    }
                    	}
                    	else if ( (LA5_0 == LPAR) )
                    	{
                    	    alt5 = 3;
                    	}
                    	else 
                    	{
                    	    NoViableAltException nvae_d5s0 =
                    	        new NoViableAltException("", 5, 0, input);

                    	    throw nvae_d5s0;
                    	}
                    	switch (alt5) 
                    	{
                    	    case 1 :
                    	        // O:\\colabo\\Grammar\\ColaboRuleSet.g:54:5: mask_or_id
                    	        {
                    	        	PushFollow(FOLLOW_mask_or_id_in_factor237);
                    	        	mask_or_id();
                    	        	state.followingStackPointer--;


                    	        }
                    	        break;
                    	    case 2 :
                    	        // O:\\colabo\\Grammar\\ColaboRuleSet.g:54:18: comparison
                    	        {
                    	        	PushFollow(FOLLOW_comparison_in_factor241);
                    	        	comparison();
                    	        	state.followingStackPointer--;


                    	        }
                    	        break;
                    	    case 3 :
                    	        // O:\\colabo\\Grammar\\ColaboRuleSet.g:54:31: ( LPAR expression RPAR )
                    	        {
                    	        	// O:\\colabo\\Grammar\\ColaboRuleSet.g:54:31: ( LPAR expression RPAR )
                    	        	// O:\\colabo\\Grammar\\ColaboRuleSet.g:54:32: LPAR expression RPAR
                    	        	{
                    	        		Match(input,LPAR,FOLLOW_LPAR_in_factor246); 
                    	        		PushFollow(FOLLOW_expression_in_factor248);
                    	        		expression();
                    	        		state.followingStackPointer--;

                    	        		Match(input,RPAR,FOLLOW_RPAR_in_factor250); 

                    	        	}


                    	        }
                    	        break;

                    	}


                    }
                    break;
                case 2 :
                    // O:\\colabo\\Grammar\\ColaboRuleSet.g:55:3: (op= NOT ( mask_or_id | comparison | ( LPAR expression RPAR ) ) )
                    {
                    	// O:\\colabo\\Grammar\\ColaboRuleSet.g:55:3: (op= NOT ( mask_or_id | comparison | ( LPAR expression RPAR ) ) )
                    	// O:\\colabo\\Grammar\\ColaboRuleSet.g:55:4: op= NOT ( mask_or_id | comparison | ( LPAR expression RPAR ) )
                    	{
                    		op=(IToken)Match(input,NOT,FOLLOW_NOT_in_factor263); 
                    		// O:\\colabo\\Grammar\\ColaboRuleSet.g:55:13: ( mask_or_id | comparison | ( LPAR expression RPAR ) )
                    		int alt6 = 3;
                    		int LA6_0 = input.LA(1);

                    		if ( ((LA6_0 >= IDENTIFIER && LA6_0 <= MASK)) )
                    		{
                    		    int LA6_1 = input.LA(2);

                    		    if ( ((LA6_1 >= GT && LA6_1 <= NE)) )
                    		    {
                    		        alt6 = 2;
                    		    }
                    		    else if ( ((LA6_1 >= OR && LA6_1 <= AND) || LA6_1 == RPAR || LA6_1 == IMPL) )
                    		    {
                    		        alt6 = 1;
                    		    }
                    		    else 
                    		    {
                    		        NoViableAltException nvae_d6s1 =
                    		            new NoViableAltException("", 6, 1, input);

                    		        throw nvae_d6s1;
                    		    }
                    		}
                    		else if ( (LA6_0 == LPAR) )
                    		{
                    		    alt6 = 3;
                    		}
                    		else 
                    		{
                    		    NoViableAltException nvae_d6s0 =
                    		        new NoViableAltException("", 6, 0, input);

                    		    throw nvae_d6s0;
                    		}
                    		switch (alt6) 
                    		{
                    		    case 1 :
                    		        // O:\\colabo\\Grammar\\ColaboRuleSet.g:55:14: mask_or_id
                    		        {
                    		        	PushFollow(FOLLOW_mask_or_id_in_factor266);
                    		        	mask_or_id();
                    		        	state.followingStackPointer--;


                    		        }
                    		        break;
                    		    case 2 :
                    		        // O:\\colabo\\Grammar\\ColaboRuleSet.g:55:27: comparison
                    		        {
                    		        	PushFollow(FOLLOW_comparison_in_factor270);
                    		        	comparison();
                    		        	state.followingStackPointer--;


                    		        }
                    		        break;
                    		    case 3 :
                    		        // O:\\colabo\\Grammar\\ColaboRuleSet.g:55:40: ( LPAR expression RPAR )
                    		        {
                    		        	// O:\\colabo\\Grammar\\ColaboRuleSet.g:55:40: ( LPAR expression RPAR )
                    		        	// O:\\colabo\\Grammar\\ColaboRuleSet.g:55:41: LPAR expression RPAR
                    		        	{
                    		        		Match(input,LPAR,FOLLOW_LPAR_in_factor275); 
                    		        		PushFollow(FOLLOW_expression_in_factor277);
                    		        		expression();
                    		        		state.followingStackPointer--;

                    		        		Match(input,RPAR,FOLLOW_RPAR_in_factor279); 

                    		        	}


                    		        }
                    		        break;

                    		}


                    	}

                    	_result.Add(op);

                    }
                    break;

            }
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return ;
    }
    // $ANTLR end "factor"


    // $ANTLR start "comparison"
    // O:\\colabo\\Grammar\\ColaboRuleSet.g:56:1: comparison : mask_or_id op= ( GT | LT | GE | LE | EQ | NE ) id ;
    public void comparison() // throws RecognitionException [1]
    {   
        IToken op = null;

        try 
    	{
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:57:2: ( mask_or_id op= ( GT | LT | GE | LE | EQ | NE ) id )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:57:4: mask_or_id op= ( GT | LT | GE | LE | EQ | NE ) id
            {
            	PushFollow(FOLLOW_mask_or_id_in_comparison292);
            	mask_or_id();
            	state.followingStackPointer--;

            	op = (IToken)input.LT(1);
            	if ( (input.LA(1) >= GT && input.LA(1) <= NE) ) 
            	{
            	    input.Consume();
            	    state.errorRecovery = false;
            	}
            	else 
            	{
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    throw mse;
            	}

            	PushFollow(FOLLOW_id_in_comparison322);
            	id();
            	state.followingStackPointer--;

            	_result.Add(op);

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return ;
    }
    // $ANTLR end "comparison"


    // $ANTLR start "action"
    // O:\\colabo\\Grammar\\ColaboRuleSet.g:58:1: action : val= IDENTIFIER ;
    public void action() // throws RecognitionException [1]
    {   
        IToken val = null;

        try 
    	{
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:59:2: (val= IDENTIFIER )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:59:4: val= IDENTIFIER
            {
            	val=(IToken)Match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_action336); 
            	_result.Add(val);

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return ;
    }
    // $ANTLR end "action"


    // $ANTLR start "mask_or_id"
    // O:\\colabo\\Grammar\\ColaboRuleSet.g:60:1: mask_or_id : val= ( MASK | IDENTIFIER ) ;
    public void mask_or_id() // throws RecognitionException [1]
    {   
        IToken val = null;

        try 
    	{
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:61:2: (val= ( MASK | IDENTIFIER ) )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:61:4: val= ( MASK | IDENTIFIER )
            {
            	val = (IToken)input.LT(1);
            	if ( (input.LA(1) >= IDENTIFIER && input.LA(1) <= MASK) ) 
            	{
            	    input.Consume();
            	    state.errorRecovery = false;
            	}
            	else 
            	{
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    throw mse;
            	}

            	_result.Add(val);

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return ;
    }
    // $ANTLR end "mask_or_id"


    // $ANTLR start "id"
    // O:\\colabo\\Grammar\\ColaboRuleSet.g:62:1: id : val= IDENTIFIER ;
    public void id() // throws RecognitionException [1]
    {   
        IToken val = null;

        try 
    	{
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:63:2: (val= IDENTIFIER )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:63:4: val= IDENTIFIER
            {
            	val=(IToken)Match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_id371); 
            	_result.Add(val);

            }

        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
        }
        finally 
    	{
        }
        return ;
    }
    // $ANTLR end "id"

    // Delegated rules


	private void InitializeCyclicDFAs()
	{
	}

 

    public static readonly BitSet FOLLOW_EOF_in_ruleSet157 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_rule_in_ruleSet161 = new BitSet(new ulong[]{0x0000000000010002UL});
    public static readonly BitSet FOLLOW_END_in_ruleSet164 = new BitSet(new ulong[]{0x00000000000600C0UL});
    public static readonly BitSet FOLLOW_rule_in_ruleSet166 = new BitSet(new ulong[]{0x0000000000010002UL});
    public static readonly BitSet FOLLOW_expression_in_rule176 = new BitSet(new ulong[]{0x0000000000008000UL});
    public static readonly BitSet FOLLOW_IMPL_in_rule182 = new BitSet(new ulong[]{0x0000000000020000UL});
    public static readonly BitSet FOLLOW_action_in_rule184 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_term_in_expression194 = new BitSet(new ulong[]{0x0000000000000012UL});
    public static readonly BitSet FOLLOW_OR_in_expression201 = new BitSet(new ulong[]{0x00000000000600C0UL});
    public static readonly BitSet FOLLOW_term_in_expression203 = new BitSet(new ulong[]{0x0000000000000012UL});
    public static readonly BitSet FOLLOW_factor_in_term215 = new BitSet(new ulong[]{0x0000000000000022UL});
    public static readonly BitSet FOLLOW_AND_in_term222 = new BitSet(new ulong[]{0x00000000000600C0UL});
    public static readonly BitSet FOLLOW_factor_in_term224 = new BitSet(new ulong[]{0x0000000000000022UL});
    public static readonly BitSet FOLLOW_mask_or_id_in_factor237 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_comparison_in_factor241 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_LPAR_in_factor246 = new BitSet(new ulong[]{0x00000000000600C0UL});
    public static readonly BitSet FOLLOW_expression_in_factor248 = new BitSet(new ulong[]{0x0000000000000100UL});
    public static readonly BitSet FOLLOW_RPAR_in_factor250 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_NOT_in_factor263 = new BitSet(new ulong[]{0x0000000000060080UL});
    public static readonly BitSet FOLLOW_mask_or_id_in_factor266 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_comparison_in_factor270 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_LPAR_in_factor275 = new BitSet(new ulong[]{0x00000000000600C0UL});
    public static readonly BitSet FOLLOW_expression_in_factor277 = new BitSet(new ulong[]{0x0000000000000100UL});
    public static readonly BitSet FOLLOW_RPAR_in_factor279 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_mask_or_id_in_comparison292 = new BitSet(new ulong[]{0x0000000000007E00UL});
    public static readonly BitSet FOLLOW_set_in_comparison298 = new BitSet(new ulong[]{0x0000000000020000UL});
    public static readonly BitSet FOLLOW_id_in_comparison322 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IDENTIFIER_in_action336 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_set_in_mask_or_id350 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IDENTIFIER_in_id371 = new BitSet(new ulong[]{0x0000000000000002UL});

}
