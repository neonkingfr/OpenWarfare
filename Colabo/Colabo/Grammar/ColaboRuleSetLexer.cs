// $ANTLR 3.2 Sep 23, 2009 12:02:23 O:\\colabo\\Grammar\\ColaboRuleSet.g 2010-01-25 16:29:03

// The variable 'variable' is assigned but its value is never used.
#pragma warning disable 168, 219
// Unreachable code detected.
#pragma warning disable 162


using System;
using Antlr.Runtime;
using IList 		= System.Collections.IList;
using ArrayList 	= System.Collections.ArrayList;
using Stack 		= Antlr.Runtime.Collections.StackList;


public partial class ColaboRuleSetLexer : Lexer {
    public const int GE = 11;
    public const int LT = 10;
    public const int UNICODE_ESC = 22;
    public const int HEXADECIMAL_ESC = 23;
    public const int CHAR = 20;
    public const int HEX_DIGIT = 24;
    public const int WHITESPACE = 19;
    public const int NOT = 6;
    public const int AND = 5;
    public const int EOF = -1;
    public const int ESC_SEQ = 21;
    public const int MASK = 18;
    public const int LPAR = 7;
    public const int IDENTIFIER = 17;
    public const int OR = 4;
    public const int GT = 9;
    public const int RPAR = 8;
    public const int IMPL = 15;
    public const int EQ = 13;
    public const int END = 16;
    public const int LE = 12;
    public const int NE = 14;

    // delegates
    // delegators

    public ColaboRuleSetLexer() 
    {
		InitializeCyclicDFAs();
    }
    public ColaboRuleSetLexer(ICharStream input)
		: this(input, null) {
    }
    public ColaboRuleSetLexer(ICharStream input, RecognizerSharedState state)
		: base(input, state) {
		InitializeCyclicDFAs(); 

    }
    
    override public string GrammarFileName
    {
    	get { return "O:\\colabo\\Grammar\\ColaboRuleSet.g";} 
    }

    // $ANTLR start "OR"
    public void mOR() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = OR;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:7:4: ( '|' )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:7:6: '|'
            {
            	Match('|'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "OR"

    // $ANTLR start "AND"
    public void mAND() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = AND;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:8:5: ( '&' )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:8:7: '&'
            {
            	Match('&'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "AND"

    // $ANTLR start "NOT"
    public void mNOT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = NOT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:9:5: ( '!' )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:9:7: '!'
            {
            	Match('!'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "NOT"

    // $ANTLR start "LPAR"
    public void mLPAR() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = LPAR;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:10:6: ( '(' )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:10:8: '('
            {
            	Match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "LPAR"

    // $ANTLR start "RPAR"
    public void mRPAR() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = RPAR;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:11:6: ( ')' )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:11:8: ')'
            {
            	Match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "RPAR"

    // $ANTLR start "GT"
    public void mGT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = GT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:12:4: ( '>' )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:12:6: '>'
            {
            	Match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "GT"

    // $ANTLR start "LT"
    public void mLT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = LT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:13:4: ( '<' )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:13:6: '<'
            {
            	Match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "LT"

    // $ANTLR start "GE"
    public void mGE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = GE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:14:4: ( '>=' )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:14:6: '>='
            {
            	Match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "GE"

    // $ANTLR start "LE"
    public void mLE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = LE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:15:4: ( '<=' )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:15:6: '<='
            {
            	Match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "LE"

    // $ANTLR start "EQ"
    public void mEQ() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = EQ;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:16:4: ( '==' )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:16:6: '=='
            {
            	Match("=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "EQ"

    // $ANTLR start "NE"
    public void mNE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = NE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:17:4: ( '!=' )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:17:6: '!='
            {
            	Match("!="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "NE"

    // $ANTLR start "IMPL"
    public void mIMPL() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = IMPL;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:18:6: ( '=>' )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:18:8: '=>'
            {
            	Match("=>"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "IMPL"

    // $ANTLR start "END"
    public void mEND() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = END;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:19:5: ( ';' )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:19:7: ';'
            {
            	Match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "END"

    // $ANTLR start "WHITESPACE"
    public void mWHITESPACE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = WHITESPACE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:67:2: ( ( ' ' | '\\t' | '\\r' | '\\n' | '\\u000C' )+ )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:67:4: ( ' ' | '\\t' | '\\r' | '\\n' | '\\u000C' )+
            {
            	// O:\\colabo\\Grammar\\ColaboRuleSet.g:67:4: ( ' ' | '\\t' | '\\r' | '\\n' | '\\u000C' )+
            	int cnt1 = 0;
            	do 
            	{
            	    int alt1 = 2;
            	    int LA1_0 = input.LA(1);

            	    if ( ((LA1_0 >= '\t' && LA1_0 <= '\n') || (LA1_0 >= '\f' && LA1_0 <= '\r') || LA1_0 == ' ') )
            	    {
            	        alt1 = 1;
            	    }


            	    switch (alt1) 
            		{
            			case 1 :
            			    // O:\\colabo\\Grammar\\ColaboRuleSet.g:
            			    {
            			    	if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n') || (input.LA(1) >= '\f' && input.LA(1) <= '\r') || input.LA(1) == ' ' ) 
            			    	{
            			    	    input.Consume();

            			    	}
            			    	else 
            			    	{
            			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
            			    	    Recover(mse);
            			    	    throw mse;}


            			    }
            			    break;

            			default:
            			    if ( cnt1 >= 1 ) goto loop1;
            		            EarlyExitException eee1 =
            		                new EarlyExitException(1, input);
            		            throw eee1;
            	    }
            	    cnt1++;
            	} while (true);

            	loop1:
            		;	// Stops C# compiler whining that label 'loop1' has no statements

            	_channel = HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "WHITESPACE"

    // $ANTLR start "IDENTIFIER"
    public void mIDENTIFIER() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = IDENTIFIER;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:70:2: ( ( CHAR )+ | ( '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' | '*' | '?' ) )* '\"' ) )
            int alt4 = 2;
            int LA4_0 = input.LA(1);

            if ( ((LA4_0 >= '\u0000' && LA4_0 <= '\b') || LA4_0 == '\u000B' || (LA4_0 >= '\u000E' && LA4_0 <= '\u001F') || (LA4_0 >= '#' && LA4_0 <= '%') || (LA4_0 >= '-' && LA4_0 <= ':') || (LA4_0 >= '@' && LA4_0 <= '[') || (LA4_0 >= ']' && LA4_0 <= '{') || (LA4_0 >= '}' && LA4_0 <= '\uFFFF')) )
            {
                alt4 = 1;
            }
            else if ( (LA4_0 == '\"') )
            {
                alt4 = 2;
            }
            else 
            {
                NoViableAltException nvae_d4s0 =
                    new NoViableAltException("", 4, 0, input);

                throw nvae_d4s0;
            }
            switch (alt4) 
            {
                case 1 :
                    // O:\\colabo\\Grammar\\ColaboRuleSet.g:70:4: ( CHAR )+
                    {
                    	// O:\\colabo\\Grammar\\ColaboRuleSet.g:70:4: ( CHAR )+
                    	int cnt2 = 0;
                    	do 
                    	{
                    	    int alt2 = 2;
                    	    int LA2_0 = input.LA(1);

                    	    if ( ((LA2_0 >= '\u0000' && LA2_0 <= '\b') || LA2_0 == '\u000B' || (LA2_0 >= '\u000E' && LA2_0 <= '\u001F') || (LA2_0 >= '#' && LA2_0 <= '%') || (LA2_0 >= '-' && LA2_0 <= ':') || (LA2_0 >= '@' && LA2_0 <= '[') || (LA2_0 >= ']' && LA2_0 <= '{') || (LA2_0 >= '}' && LA2_0 <= '\uFFFF')) )
                    	    {
                    	        alt2 = 1;
                    	    }


                    	    switch (alt2) 
                    		{
                    			case 1 :
                    			    // O:\\colabo\\Grammar\\ColaboRuleSet.g:70:4: CHAR
                    			    {
                    			    	mCHAR(); 

                    			    }
                    			    break;

                    			default:
                    			    if ( cnt2 >= 1 ) goto loop2;
                    		            EarlyExitException eee2 =
                    		                new EarlyExitException(2, input);
                    		            throw eee2;
                    	    }
                    	    cnt2++;
                    	} while (true);

                    	loop2:
                    		;	// Stops C# compiler whining that label 'loop2' has no statements


                    }
                    break;
                case 2 :
                    // O:\\colabo\\Grammar\\ColaboRuleSet.g:70:12: ( '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' | '*' | '?' ) )* '\"' )
                    {
                    	// O:\\colabo\\Grammar\\ColaboRuleSet.g:70:12: ( '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' | '*' | '?' ) )* '\"' )
                    	// O:\\colabo\\Grammar\\ColaboRuleSet.g:70:13: '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' | '*' | '?' ) )* '\"'
                    	{
                    		Match('\"'); 
                    		// O:\\colabo\\Grammar\\ColaboRuleSet.g:70:17: ( ESC_SEQ | ~ ( '\\\\' | '\"' | '*' | '?' ) )*
                    		do 
                    		{
                    		    int alt3 = 3;
                    		    int LA3_0 = input.LA(1);

                    		    if ( (LA3_0 == '\\') )
                    		    {
                    		        alt3 = 1;
                    		    }
                    		    else if ( ((LA3_0 >= '\u0000' && LA3_0 <= '!') || (LA3_0 >= '#' && LA3_0 <= ')') || (LA3_0 >= '+' && LA3_0 <= '>') || (LA3_0 >= '@' && LA3_0 <= '[') || (LA3_0 >= ']' && LA3_0 <= '\uFFFF')) )
                    		    {
                    		        alt3 = 2;
                    		    }


                    		    switch (alt3) 
                    			{
                    				case 1 :
                    				    // O:\\colabo\\Grammar\\ColaboRuleSet.g:70:18: ESC_SEQ
                    				    {
                    				    	mESC_SEQ(); 

                    				    }
                    				    break;
                    				case 2 :
                    				    // O:\\colabo\\Grammar\\ColaboRuleSet.g:70:28: ~ ( '\\\\' | '\"' | '*' | '?' )
                    				    {
                    				    	if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!') || (input.LA(1) >= '#' && input.LA(1) <= ')') || (input.LA(1) >= '+' && input.LA(1) <= '>') || (input.LA(1) >= '@' && input.LA(1) <= '[') || (input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) 
                    				    	{
                    				    	    input.Consume();

                    				    	}
                    				    	else 
                    				    	{
                    				    	    MismatchedSetException mse = new MismatchedSetException(null,input);
                    				    	    Recover(mse);
                    				    	    throw mse;}


                    				    }
                    				    break;

                    				default:
                    				    goto loop3;
                    		    }
                    		} while (true);

                    		loop3:
                    			;	// Stops C# compiler whining that label 'loop3' has no statements

                    		Match('\"'); 

                    	}


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "IDENTIFIER"

    // $ANTLR start "MASK"
    public void mMASK() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = MASK;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:73:2: ( ( CHAR | '*' | '?' )+ | ( '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' ) )* '\"' ) )
            int alt7 = 2;
            int LA7_0 = input.LA(1);

            if ( ((LA7_0 >= '\u0000' && LA7_0 <= '\b') || LA7_0 == '\u000B' || (LA7_0 >= '\u000E' && LA7_0 <= '\u001F') || (LA7_0 >= '#' && LA7_0 <= '%') || LA7_0 == '*' || (LA7_0 >= '-' && LA7_0 <= ':') || (LA7_0 >= '?' && LA7_0 <= '[') || (LA7_0 >= ']' && LA7_0 <= '{') || (LA7_0 >= '}' && LA7_0 <= '\uFFFF')) )
            {
                alt7 = 1;
            }
            else if ( (LA7_0 == '\"') )
            {
                alt7 = 2;
            }
            else 
            {
                NoViableAltException nvae_d7s0 =
                    new NoViableAltException("", 7, 0, input);

                throw nvae_d7s0;
            }
            switch (alt7) 
            {
                case 1 :
                    // O:\\colabo\\Grammar\\ColaboRuleSet.g:73:4: ( CHAR | '*' | '?' )+
                    {
                    	// O:\\colabo\\Grammar\\ColaboRuleSet.g:73:4: ( CHAR | '*' | '?' )+
                    	int cnt5 = 0;
                    	do 
                    	{
                    	    int alt5 = 2;
                    	    int LA5_0 = input.LA(1);

                    	    if ( ((LA5_0 >= '\u0000' && LA5_0 <= '\b') || LA5_0 == '\u000B' || (LA5_0 >= '\u000E' && LA5_0 <= '\u001F') || (LA5_0 >= '#' && LA5_0 <= '%') || LA5_0 == '*' || (LA5_0 >= '-' && LA5_0 <= ':') || (LA5_0 >= '?' && LA5_0 <= '[') || (LA5_0 >= ']' && LA5_0 <= '{') || (LA5_0 >= '}' && LA5_0 <= '\uFFFF')) )
                    	    {
                    	        alt5 = 1;
                    	    }


                    	    switch (alt5) 
                    		{
                    			case 1 :
                    			    // O:\\colabo\\Grammar\\ColaboRuleSet.g:
                    			    {
                    			    	if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\b') || input.LA(1) == '\u000B' || (input.LA(1) >= '\u000E' && input.LA(1) <= '\u001F') || (input.LA(1) >= '#' && input.LA(1) <= '%') || input.LA(1) == '*' || (input.LA(1) >= '-' && input.LA(1) <= ':') || (input.LA(1) >= '?' && input.LA(1) <= '[') || (input.LA(1) >= ']' && input.LA(1) <= '{') || (input.LA(1) >= '}' && input.LA(1) <= '\uFFFF') ) 
                    			    	{
                    			    	    input.Consume();

                    			    	}
                    			    	else 
                    			    	{
                    			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
                    			    	    Recover(mse);
                    			    	    throw mse;}


                    			    }
                    			    break;

                    			default:
                    			    if ( cnt5 >= 1 ) goto loop5;
                    		            EarlyExitException eee5 =
                    		                new EarlyExitException(5, input);
                    		            throw eee5;
                    	    }
                    	    cnt5++;
                    	} while (true);

                    	loop5:
                    		;	// Stops C# compiler whining that label 'loop5' has no statements


                    }
                    break;
                case 2 :
                    // O:\\colabo\\Grammar\\ColaboRuleSet.g:73:26: ( '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' ) )* '\"' )
                    {
                    	// O:\\colabo\\Grammar\\ColaboRuleSet.g:73:26: ( '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' ) )* '\"' )
                    	// O:\\colabo\\Grammar\\ColaboRuleSet.g:73:27: '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' ) )* '\"'
                    	{
                    		Match('\"'); 
                    		// O:\\colabo\\Grammar\\ColaboRuleSet.g:73:31: ( ESC_SEQ | ~ ( '\\\\' | '\"' ) )*
                    		do 
                    		{
                    		    int alt6 = 3;
                    		    int LA6_0 = input.LA(1);

                    		    if ( (LA6_0 == '\\') )
                    		    {
                    		        alt6 = 1;
                    		    }
                    		    else if ( ((LA6_0 >= '\u0000' && LA6_0 <= '!') || (LA6_0 >= '#' && LA6_0 <= '[') || (LA6_0 >= ']' && LA6_0 <= '\uFFFF')) )
                    		    {
                    		        alt6 = 2;
                    		    }


                    		    switch (alt6) 
                    			{
                    				case 1 :
                    				    // O:\\colabo\\Grammar\\ColaboRuleSet.g:73:32: ESC_SEQ
                    				    {
                    				    	mESC_SEQ(); 

                    				    }
                    				    break;
                    				case 2 :
                    				    // O:\\colabo\\Grammar\\ColaboRuleSet.g:73:42: ~ ( '\\\\' | '\"' )
                    				    {
                    				    	if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!') || (input.LA(1) >= '#' && input.LA(1) <= '[') || (input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) 
                    				    	{
                    				    	    input.Consume();

                    				    	}
                    				    	else 
                    				    	{
                    				    	    MismatchedSetException mse = new MismatchedSetException(null,input);
                    				    	    Recover(mse);
                    				    	    throw mse;}


                    				    }
                    				    break;

                    				default:
                    				    goto loop6;
                    		    }
                    		} while (true);

                    		loop6:
                    			;	// Stops C# compiler whining that label 'loop6' has no statements

                    		Match('\"'); 

                    	}


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "MASK"

    // $ANTLR start "CHAR"
    public void mCHAR() // throws RecognitionException [2]
    {
    		try
    		{
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:76:14: (~ ( ' ' | '\\t' | '\\r' | '\\n' | '\\u000C' | '\\'' | '\\\\' | '\\\"' | ';' | '<' | '>' | '=' | '&' | '|' | '!' | '(' | ')' | '+' | '*' | '?' | ',' ) )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:76:17: ~ ( ' ' | '\\t' | '\\r' | '\\n' | '\\u000C' | '\\'' | '\\\\' | '\\\"' | ';' | '<' | '>' | '=' | '&' | '|' | '!' | '(' | ')' | '+' | '*' | '?' | ',' )
            {
            	if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\b') || input.LA(1) == '\u000B' || (input.LA(1) >= '\u000E' && input.LA(1) <= '\u001F') || (input.LA(1) >= '#' && input.LA(1) <= '%') || (input.LA(1) >= '-' && input.LA(1) <= ':') || (input.LA(1) >= '@' && input.LA(1) <= '[') || (input.LA(1) >= ']' && input.LA(1) <= '{') || (input.LA(1) >= '}' && input.LA(1) <= '\uFFFF') ) 
            	{
            	    input.Consume();

            	}
            	else 
            	{
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    Recover(mse);
            	    throw mse;}


            }

        }
        finally 
    	{
        }
    }
    // $ANTLR end "CHAR"

    // $ANTLR start "ESC_SEQ"
    public void mESC_SEQ() // throws RecognitionException [2]
    {
    		try
    		{
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:81:6: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' | '0' ) | UNICODE_ESC | HEXADECIMAL_ESC )
            int alt8 = 3;
            int LA8_0 = input.LA(1);

            if ( (LA8_0 == '\\') )
            {
                switch ( input.LA(2) ) 
                {
                case '\"':
                case '\'':
                case '0':
                case '\\':
                case 'b':
                case 'f':
                case 'n':
                case 'r':
                case 't':
                	{
                    alt8 = 1;
                    }
                    break;
                case 'u':
                	{
                    alt8 = 2;
                    }
                    break;
                case 'x':
                	{
                    alt8 = 3;
                    }
                    break;
                	default:
                	    NoViableAltException nvae_d8s1 =
                	        new NoViableAltException("", 8, 1, input);

                	    throw nvae_d8s1;
                }

            }
            else 
            {
                NoViableAltException nvae_d8s0 =
                    new NoViableAltException("", 8, 0, input);

                throw nvae_d8s0;
            }
            switch (alt8) 
            {
                case 1 :
                    // O:\\colabo\\Grammar\\ColaboRuleSet.g:81:10: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' | '0' )
                    {
                    	Match('\\'); 
                    	if ( input.LA(1) == '\"' || input.LA(1) == '\'' || input.LA(1) == '0' || input.LA(1) == '\\' || input.LA(1) == 'b' || input.LA(1) == 'f' || input.LA(1) == 'n' || input.LA(1) == 'r' || input.LA(1) == 't' ) 
                    	{
                    	    input.Consume();

                    	}
                    	else 
                    	{
                    	    MismatchedSetException mse = new MismatchedSetException(null,input);
                    	    Recover(mse);
                    	    throw mse;}


                    }
                    break;
                case 2 :
                    // O:\\colabo\\Grammar\\ColaboRuleSet.g:82:10: UNICODE_ESC
                    {
                    	mUNICODE_ESC(); 

                    }
                    break;
                case 3 :
                    // O:\\colabo\\Grammar\\ColaboRuleSet.g:83:10: HEXADECIMAL_ESC
                    {
                    	mHEXADECIMAL_ESC(); 

                    }
                    break;

            }
        }
        finally 
    	{
        }
    }
    // $ANTLR end "ESC_SEQ"

    // $ANTLR start "HEXADECIMAL_ESC"
    public void mHEXADECIMAL_ESC() // throws RecognitionException [2]
    {
    		try
    		{
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:86:6: ( '\\\\' 'x' HEX_DIGIT HEX_DIGIT )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:86:10: '\\\\' 'x' HEX_DIGIT HEX_DIGIT
            {
            	Match('\\'); 
            	Match('x'); 
            	mHEX_DIGIT(); 
            	mHEX_DIGIT(); 

            }

        }
        finally 
    	{
        }
    }
    // $ANTLR end "HEXADECIMAL_ESC"

    // $ANTLR start "UNICODE_ESC"
    public void mUNICODE_ESC() // throws RecognitionException [2]
    {
    		try
    		{
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:89:6: ( '\\\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:89:10: '\\\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT
            {
            	Match('\\'); 
            	Match('u'); 
            	mHEX_DIGIT(); 
            	mHEX_DIGIT(); 
            	mHEX_DIGIT(); 
            	mHEX_DIGIT(); 

            }

        }
        finally 
    	{
        }
    }
    // $ANTLR end "UNICODE_ESC"

    // $ANTLR start "HEX_DIGIT"
    public void mHEX_DIGIT() // throws RecognitionException [2]
    {
    		try
    		{
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:92:2: ( ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' ) )
            // O:\\colabo\\Grammar\\ColaboRuleSet.g:92:4: ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' )
            {
            	if ( (input.LA(1) >= '0' && input.LA(1) <= '9') || (input.LA(1) >= 'A' && input.LA(1) <= 'F') || (input.LA(1) >= 'a' && input.LA(1) <= 'f') ) 
            	{
            	    input.Consume();

            	}
            	else 
            	{
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    Recover(mse);
            	    throw mse;}


            }

        }
        finally 
    	{
        }
    }
    // $ANTLR end "HEX_DIGIT"

    override public void mTokens() // throws RecognitionException 
    {
        // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:8: ( OR | AND | NOT | LPAR | RPAR | GT | LT | GE | LE | EQ | NE | IMPL | END | WHITESPACE | IDENTIFIER | MASK )
        int alt9 = 16;
        alt9 = dfa9.Predict(input);
        switch (alt9) 
        {
            case 1 :
                // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:10: OR
                {
                	mOR(); 

                }
                break;
            case 2 :
                // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:13: AND
                {
                	mAND(); 

                }
                break;
            case 3 :
                // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:17: NOT
                {
                	mNOT(); 

                }
                break;
            case 4 :
                // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:21: LPAR
                {
                	mLPAR(); 

                }
                break;
            case 5 :
                // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:26: RPAR
                {
                	mRPAR(); 

                }
                break;
            case 6 :
                // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:31: GT
                {
                	mGT(); 

                }
                break;
            case 7 :
                // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:34: LT
                {
                	mLT(); 

                }
                break;
            case 8 :
                // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:37: GE
                {
                	mGE(); 

                }
                break;
            case 9 :
                // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:40: LE
                {
                	mLE(); 

                }
                break;
            case 10 :
                // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:43: EQ
                {
                	mEQ(); 

                }
                break;
            case 11 :
                // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:46: NE
                {
                	mNE(); 

                }
                break;
            case 12 :
                // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:49: IMPL
                {
                	mIMPL(); 

                }
                break;
            case 13 :
                // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:54: END
                {
                	mEND(); 

                }
                break;
            case 14 :
                // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:58: WHITESPACE
                {
                	mWHITESPACE(); 

                }
                break;
            case 15 :
                // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:69: IDENTIFIER
                {
                	mIDENTIFIER(); 

                }
                break;
            case 16 :
                // O:\\colabo\\Grammar\\ColaboRuleSet.g:1:80: MASK
                {
                	mMASK(); 

                }
                break;

        }

    }


    protected DFA9 dfa9;
	private void InitializeCyclicDFAs()
	{
	    this.dfa9 = new DFA9(this);
	    this.dfa9.specialStateTransitionHandler = new DFA.SpecialStateTransitionHandler(DFA9_SpecialStateTransition);
	}

    const string DFA9_eotS =
        "\x03\uffff\x01\x0f\x02\uffff\x01\x11\x01\x13\x03\uffff\x01\x16"+
        "\x17\uffff";
    const string DFA9_eofS =
        "\x23\uffff";
    const string DFA9_minS =
        "\x01\x00\x02\uffff\x01\x3d\x02\uffff\x03\x3d\x02\uffff\x02\x00"+
        "\x0a\uffff\x01\x22\x01\x00\x01\uffff\x01\x00\x05\x30\x01\x00\x01"+
        "\x30\x01\x00";
    const string DFA9_maxS =
        "\x01\uffff\x02\uffff\x01\x3d\x02\uffff\x02\x3d\x01\x3e\x02\uffff"+
        "\x02\uffff\x0a\uffff\x01\x78\x01\uffff\x01\uffff\x01\uffff\x05\x66"+
        "\x01\uffff\x01\x66\x01\uffff";
    const string DFA9_acceptS =
        "\x01\uffff\x01\x01\x01\x02\x01\uffff\x01\x04\x01\x05\x03\uffff"+
        "\x01\x0d\x01\x0e\x02\uffff\x01\x10\x01\x0b\x01\x03\x01\x08\x01\x06"+
        "\x01\x09\x01\x07\x01\x0a\x01\x0c\x01\x0f\x02\uffff\x01\x0f\x09\uffff";
    const string DFA9_specialS =
        "\x01\x04\x0a\uffff\x01\x00\x01\x06\x0b\uffff\x01\x05\x01\uffff"+
        "\x01\x02\x05\uffff\x01\x01\x01\uffff\x01\x03}>";
    static readonly string[] DFA9_transitionS = {
            "\x09\x0b\x02\x0a\x01\x0b\x02\x0a\x12\x0b\x01\x0a\x01\x03\x01"+
            "\x0c\x03\x0b\x01\x02\x01\uffff\x01\x04\x01\x05\x01\x0d\x02\uffff"+
            "\x0e\x0b\x01\x09\x01\x07\x01\x08\x01\x06\x01\x0d\x1c\x0b\x01"+
            "\uffff\x1f\x0b\x01\x01\uff83\x0b",
            "",
            "",
            "\x01\x0e",
            "",
            "",
            "\x01\x10",
            "\x01\x12",
            "\x01\x14\x01\x15",
            "",
            "",
            "\x09\x0b\x02\uffff\x01\x0b\x02\uffff\x12\x0b\x03\uffff\x03"+
            "\x0b\x04\uffff\x01\x0d\x02\uffff\x0e\x0b\x04\uffff\x01\x0d\x1c"+
            "\x0b\x01\uffff\x1f\x0b\x01\uffff\uff83\x0b",
            "\x22\x18\x01\x19\x07\x18\x01\x0d\x14\x18\x01\x0d\x1c\x18\x01"+
            "\x17\uffa3\x18",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\x01\x1a\x04\uffff\x01\x1a\x08\uffff\x01\x1a\x2b\uffff\x01"+
            "\x1a\x05\uffff\x01\x1a\x03\uffff\x01\x1a\x07\uffff\x01\x1a\x03"+
            "\uffff\x01\x1a\x01\uffff\x01\x1a\x01\x1b\x02\uffff\x01\x1c",
            "\x22\x18\x01\x19\x07\x18\x01\x0d\x14\x18\x01\x0d\x1c\x18\x01"+
            "\x17\uffa3\x18",
            "",
            "\x22\x18\x01\x19\x07\x18\x01\x0d\x14\x18\x01\x0d\x1c\x18\x01"+
            "\x17\uffa3\x18",
            "\x0a\x1d\x07\uffff\x06\x1d\x1a\uffff\x06\x1d",
            "\x0a\x1e\x07\uffff\x06\x1e\x1a\uffff\x06\x1e",
            "\x0a\x1f\x07\uffff\x06\x1f\x1a\uffff\x06\x1f",
            "\x0a\x20\x07\uffff\x06\x20\x1a\uffff\x06\x20",
            "\x0a\x21\x07\uffff\x06\x21\x1a\uffff\x06\x21",
            "\x22\x18\x01\x19\x07\x18\x01\x0d\x14\x18\x01\x0d\x1c\x18\x01"+
            "\x17\uffa3\x18",
            "\x0a\x22\x07\uffff\x06\x22\x1a\uffff\x06\x22",
            "\x22\x18\x01\x19\x07\x18\x01\x0d\x14\x18\x01\x0d\x1c\x18\x01"+
            "\x17\uffa3\x18"
    };

    static readonly short[] DFA9_eot = DFA.UnpackEncodedString(DFA9_eotS);
    static readonly short[] DFA9_eof = DFA.UnpackEncodedString(DFA9_eofS);
    static readonly char[] DFA9_min = DFA.UnpackEncodedStringToUnsignedChars(DFA9_minS);
    static readonly char[] DFA9_max = DFA.UnpackEncodedStringToUnsignedChars(DFA9_maxS);
    static readonly short[] DFA9_accept = DFA.UnpackEncodedString(DFA9_acceptS);
    static readonly short[] DFA9_special = DFA.UnpackEncodedString(DFA9_specialS);
    static readonly short[][] DFA9_transition = DFA.UnpackEncodedStringArray(DFA9_transitionS);

    protected class DFA9 : DFA
    {
        public DFA9(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 9;
            this.eot = DFA9_eot;
            this.eof = DFA9_eof;
            this.min = DFA9_min;
            this.max = DFA9_max;
            this.accept = DFA9_accept;
            this.special = DFA9_special;
            this.transition = DFA9_transition;

        }

        override public string Description
        {
            get { return "1:1: Tokens : ( OR | AND | NOT | LPAR | RPAR | GT | LT | GE | LE | EQ | NE | IMPL | END | WHITESPACE | IDENTIFIER | MASK );"; }
        }

    }


    protected internal int DFA9_SpecialStateTransition(DFA dfa, int s, IIntStream _input) //throws NoViableAltException
    {
            IIntStream input = _input;
    	int _s = s;
        switch ( s )
        {
               	case 0 : 
                   	int LA9_11 = input.LA(1);

                   	s = -1;
                   	if ( ((LA9_11 >= '\u0000' && LA9_11 <= '\b') || LA9_11 == '\u000B' || (LA9_11 >= '\u000E' && LA9_11 <= '\u001F') || (LA9_11 >= '#' && LA9_11 <= '%') || (LA9_11 >= '-' && LA9_11 <= ':') || (LA9_11 >= '@' && LA9_11 <= '[') || (LA9_11 >= ']' && LA9_11 <= '{') || (LA9_11 >= '}' && LA9_11 <= '\uFFFF')) ) { s = 11; }

                   	else if ( (LA9_11 == '*' || LA9_11 == '?') ) { s = 13; }

                   	else s = 22;

                   	if ( s >= 0 ) return s;
                   	break;
               	case 1 : 
                   	int LA9_32 = input.LA(1);

                   	s = -1;
                   	if ( (LA9_32 == '\"') ) { s = 25; }

                   	else if ( (LA9_32 == '\\') ) { s = 23; }

                   	else if ( ((LA9_32 >= '\u0000' && LA9_32 <= '!') || (LA9_32 >= '#' && LA9_32 <= ')') || (LA9_32 >= '+' && LA9_32 <= '>') || (LA9_32 >= '@' && LA9_32 <= '[') || (LA9_32 >= ']' && LA9_32 <= '\uFFFF')) ) { s = 24; }

                   	else if ( (LA9_32 == '*' || LA9_32 == '?') ) { s = 13; }

                   	if ( s >= 0 ) return s;
                   	break;
               	case 2 : 
                   	int LA9_26 = input.LA(1);

                   	s = -1;
                   	if ( (LA9_26 == '\"') ) { s = 25; }

                   	else if ( (LA9_26 == '\\') ) { s = 23; }

                   	else if ( ((LA9_26 >= '\u0000' && LA9_26 <= '!') || (LA9_26 >= '#' && LA9_26 <= ')') || (LA9_26 >= '+' && LA9_26 <= '>') || (LA9_26 >= '@' && LA9_26 <= '[') || (LA9_26 >= ']' && LA9_26 <= '\uFFFF')) ) { s = 24; }

                   	else if ( (LA9_26 == '*' || LA9_26 == '?') ) { s = 13; }

                   	if ( s >= 0 ) return s;
                   	break;
               	case 3 : 
                   	int LA9_34 = input.LA(1);

                   	s = -1;
                   	if ( (LA9_34 == '\"') ) { s = 25; }

                   	else if ( (LA9_34 == '\\') ) { s = 23; }

                   	else if ( ((LA9_34 >= '\u0000' && LA9_34 <= '!') || (LA9_34 >= '#' && LA9_34 <= ')') || (LA9_34 >= '+' && LA9_34 <= '>') || (LA9_34 >= '@' && LA9_34 <= '[') || (LA9_34 >= ']' && LA9_34 <= '\uFFFF')) ) { s = 24; }

                   	else if ( (LA9_34 == '*' || LA9_34 == '?') ) { s = 13; }

                   	if ( s >= 0 ) return s;
                   	break;
               	case 4 : 
                   	int LA9_0 = input.LA(1);

                   	s = -1;
                   	if ( (LA9_0 == '|') ) { s = 1; }

                   	else if ( (LA9_0 == '&') ) { s = 2; }

                   	else if ( (LA9_0 == '!') ) { s = 3; }

                   	else if ( (LA9_0 == '(') ) { s = 4; }

                   	else if ( (LA9_0 == ')') ) { s = 5; }

                   	else if ( (LA9_0 == '>') ) { s = 6; }

                   	else if ( (LA9_0 == '<') ) { s = 7; }

                   	else if ( (LA9_0 == '=') ) { s = 8; }

                   	else if ( (LA9_0 == ';') ) { s = 9; }

                   	else if ( ((LA9_0 >= '\t' && LA9_0 <= '\n') || (LA9_0 >= '\f' && LA9_0 <= '\r') || LA9_0 == ' ') ) { s = 10; }

                   	else if ( ((LA9_0 >= '\u0000' && LA9_0 <= '\b') || LA9_0 == '\u000B' || (LA9_0 >= '\u000E' && LA9_0 <= '\u001F') || (LA9_0 >= '#' && LA9_0 <= '%') || (LA9_0 >= '-' && LA9_0 <= ':') || (LA9_0 >= '@' && LA9_0 <= '[') || (LA9_0 >= ']' && LA9_0 <= '{') || (LA9_0 >= '}' && LA9_0 <= '\uFFFF')) ) { s = 11; }

                   	else if ( (LA9_0 == '\"') ) { s = 12; }

                   	else if ( (LA9_0 == '*' || LA9_0 == '?') ) { s = 13; }

                   	if ( s >= 0 ) return s;
                   	break;
               	case 5 : 
                   	int LA9_24 = input.LA(1);

                   	s = -1;
                   	if ( (LA9_24 == '\"') ) { s = 25; }

                   	else if ( (LA9_24 == '\\') ) { s = 23; }

                   	else if ( ((LA9_24 >= '\u0000' && LA9_24 <= '!') || (LA9_24 >= '#' && LA9_24 <= ')') || (LA9_24 >= '+' && LA9_24 <= '>') || (LA9_24 >= '@' && LA9_24 <= '[') || (LA9_24 >= ']' && LA9_24 <= '\uFFFF')) ) { s = 24; }

                   	else if ( (LA9_24 == '*' || LA9_24 == '?') ) { s = 13; }

                   	if ( s >= 0 ) return s;
                   	break;
               	case 6 : 
                   	int LA9_12 = input.LA(1);

                   	s = -1;
                   	if ( (LA9_12 == '\\') ) { s = 23; }

                   	else if ( ((LA9_12 >= '\u0000' && LA9_12 <= '!') || (LA9_12 >= '#' && LA9_12 <= ')') || (LA9_12 >= '+' && LA9_12 <= '>') || (LA9_12 >= '@' && LA9_12 <= '[') || (LA9_12 >= ']' && LA9_12 <= '\uFFFF')) ) { s = 24; }

                   	else if ( (LA9_12 == '\"') ) { s = 25; }

                   	else if ( (LA9_12 == '*' || LA9_12 == '?') ) { s = 13; }

                   	if ( s >= 0 ) return s;
                   	break;
        }
        NoViableAltException nvae9 =
            new NoViableAltException(dfa.Description, 9, _s, input);
        dfa.Error(nvae9);
        throw nvae9;
    }
 
    
}
