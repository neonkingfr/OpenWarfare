using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Colabo
{
  public partial class DialogRenameTag : Form
  {
    private string _oldTag;

    public DialogRenameTag(string oldTag)
    {
      _oldTag = oldTag;
      InitializeComponent();
      valueTag.Text = oldTag;
    }

    public string tag
    {
      get { return valueTag.Text.ToLower(); }
    }
    public string oldTag
    {
      get { return _oldTag; }
    }

    private void DialogRenameTag_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (DialogResult == DialogResult.OK)
      {
        string newTag = tag;
        if (string.IsNullOrEmpty(newTag))
        {
          // valid - remove tag
          return;
        }

        // check if tag name is valid
        string message = null;
        if (newTag[0] == TagSpecialChars.PrefixLocal || newTag[0] == TagSpecialChars.PrefixRemove)
        {
          message = string.Format("Invalid tag name - cannot start with '{0}'", newTag[0]);
        }
        else if (newTag[0] == TagSpecialChars.TagPrivate && _oldTag[0] != TagSpecialChars.TagPrivate)
        {
          message = "Cannot change tag to private";
        }
        else if (newTag[0] != TagSpecialChars.TagPrivate && _oldTag[0] == TagSpecialChars.TagPrivate)
        {
          message = "Cannot change tag from private";
        }
        // invalid tag
        if (message != null)
        {
          MessageBox.Show(message, "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
          e.Cancel = true;
        }
      }
    }

    private void valueTag_TextChanged(object sender, EventArgs e)
    {
      if (string.IsNullOrEmpty(valueTag.Text))
        this.Text = string.Format("Remove tag '{0}'", oldTag);
      else
        this.Text = string.Format("Rename tag '{0}'", oldTag);
    }
  }
}