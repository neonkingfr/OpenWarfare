﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using System.ComponentModel;

namespace Colabo
{
  namespace UserActivityMonitor
  {

    internal struct LASTINPUTINFO
    {
      public uint cbSize;

      public uint dwTime;
    }

    public class UserActivityMonitor
    {
      [DllImport("User32.dll")]
      public static extern bool LockWorkStation();

      [DllImport("User32.dll")]
      private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

      [DllImport("Kernel32.dll")]
      private static extern uint GetLastError();

      public static uint GetIdleTime()
      {
        LASTINPUTINFO lastInPut = new LASTINPUTINFO();
        lastInPut.cbSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(lastInPut);
        GetLastInputInfo(ref lastInPut);

        return ((uint)Environment.TickCount - lastInPut.dwTime);
      }

      public static long GetTickCount()
      {
        return Environment.TickCount;
      }

      public static long GetLastInputTime()
      {
        LASTINPUTINFO lastInput = new LASTINPUTINFO();
        lastInput.cbSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(lastInput);
        if (!GetLastInputInfo(ref lastInput))
        {
          throw new Exception(GetLastError().ToString());
        }

        return lastInput.dwTime;
      }


    }


    public class NotifyIdle
    {
      Thread _thread;
      ManualResetEvent _threadTerminate = new ManualResetEvent(false);
      String _stateReported = null;
      String _mode = "Online";
      AutoResetEvent _modeChanged = new AutoResetEvent(false);

      public class IdleState : EventArgs
      {
        private String _state;
        public String State
        {
          set { _state = value; }
          get { return _state; }
        }
        public IdleState(String state) { _state = state; }
      };

      public delegate void Notification(IdleState idle);
      event Notification _notify;

      public void DoWork()
      {
        DateTime lastTimeAlive = DateTime.MinValue;
        while (true)
        {
          WaitHandle[] handles = { _threadTerminate, _modeChanged };
          int signaled = WaitHandle.WaitAny(handles,10000);
          if (signaled == 0) break;

          bool idle = UserActivityMonitor.GetIdleTime() > Program.config.AwayAfterInterval * 60 * 1000;
          String state = idle ? "Away" : "Online";
          if (_mode != null && !_mode.Equals("Online")) state = _mode;

          DateTime now = DateTime.Now;
          if (_stateReported == null || !_stateReported.Equals(state) || now-lastTimeAlive>TimeSpan.FromMinutes(2))
          {
            var handler = _notify;
            if (handler != null)
            {
              handler(new IdleState(state));
              _stateReported = state;
            }
            lastTimeAlive = now;
          }
        }
      }

      public String DetectionMode
      {
        set
        {
          _mode = value;
          _modeChanged.Set();
        }
      }
      public void Subscribe(Notification notify)
      {
        _notify += notify;
        _modeChanged.Set(); // make sure handler is executed immediately
      }
      public void Unsubscribe(Notification notify) { _notify -= notify; }

      public void Start()
      {
        if (_thread == null)
        {
          _thread = new Thread(new ThreadStart(DoWork));
          _thread.Start();
        }
      }
      public void Stop()
      {
        if (_thread != null)
        {
          _threadTerminate.Set();
          _thread.Join();
          _thread = null;
        }
      }

      public NotifyIdle()
      {
        Start();
      }
      ~NotifyIdle()
      {
        Stop();
      }
    };
  }
}
