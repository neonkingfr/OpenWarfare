using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using Jayrock.Json;
using Jayrock.Json.Conversion;

namespace Colabo
{
  public partial class DialogArticle : Form, IMessageFilter, IReloadPage
  {
    public enum Mode { New, Edit, Note, Chat };

    private ArticleItem _item;
    /// path to the temporary draft file (need to be different than _item._draft to enable rollback)
    private string _draft;
    /// for reply, the content of the original article
    private string _initialContent;

    ///  for chat, content of the previous conversation
    private ChatConversation _historyContent;

    private ChatConversation.ChatData.UserList _chatUserState;

    private FormColabo _owner;
    private bool _urlChanged;
    private SQLDatabase _database;
    private bool _contentChanged;
    private Mode _mode;

    private bool _initialized; // avoid event handlers reaction to controls initialization
    private bool _svnUpdateFailed; // update of working copy failed for the current article

    private bool _previewUpdateNeeded;
    private bool _scriptDebugNeeded;

    private struct DebuggingState
    {
      public Object stateIE;
      public Object stateOther;

      public DebuggingState(Object ie, Object other)
      {
        stateIE = ie; stateOther = other;
      }
    }
    private DebuggingState _oldState;

    public FormColabo owner { get { return _owner; } }

    private Size _clientSize;

    // minor edit detection support
    private int _contentLength;
    private int _contentChanges;

    /// store original version - just in case users gets very close to it again
    private String _textOriginal;
    /// store last version before "individual continous change"
    private String _textBeforeChange;
    private int _changesBeforeChange;

    /// store last version before previous keypress
    private String _textBeforeLastEdit;

    private bool _showWithoutActivation = false;

    public SQLDatabase database
    {
      get
      {
        if (_database != null) return _database;
        return (SQLDatabase)valueDatabase.SelectedItem;
      }
    }

    private int contentChanges
    {
      get { return _contentChanges; }
      set { _contentChanges = value; if (debugChanges != null) debugChanges.Text = string.Format("{0} changes", value); }
    }

    public bool IsEdittingArticle(SQLDatabase database, int id)
    {
      return database == _database && id == _item._id;
    }

    protected override bool ShowWithoutActivation
    {
      get { return _showWithoutActivation; }
    }

    public void ShowInBackground()
    {
      _showWithoutActivation = true;
      this.WindowState = FormWindowState.Minimized; 
      Show();
    }

    public DialogArticle(ArticleItem item, FormColabo owner, string initialContent, SQLDatabase database, Mode mode)
    {
      _item = item;
      if (_item._chatId >= 0 || _item._chatUsers != null)
      {
        // loaded a chat, switch to a chat mode
        mode = Mode.Chat;
      }
      _mode = mode;
      _initialContent = initialContent;
      _owner = owner;
      _database = database;
      _urlChanged = false;
      _contentChanged = false;
      _initialized = false;
      _svnUpdateFailed = false;

      _previewUpdateNeeded = false;
      _scriptDebugNeeded = false;

      _contentLength = 0;
      _contentChanges = 0;
      _changesBeforeChange = 0;
      _textBeforeChange = "";
      _textBeforeLastEdit = "";
      _textOriginal = "";

      // drag and drop support and attachments
      _dragAndDropValidData = false;

      InitializeComponent();

      chatUsers.StateImageList = owner.PresenceImages;

      bool chat = AdjustChatButtons();

      _attWidth = panelAttachments.Width;
      _chatUsersHeight = groupChat.Height;
      _headersHeight = groupHeaders.Height;

      panelAttachments.Width = 0;
      groupChat.Height = 0;

      panelArticleDetails.Width += _attWidth;
      preview.Height += _chatUsersHeight;
      preview.Top -= _chatUsersHeight;
      splitContainer1.SplitterDistance -= _chatUsersHeight / 2;


      panelAttachments.Visible = false;
      groupChat.Visible = false;

      if (chat)
      {
        ListChatUsers();
        ShowChatUsersPanel();
        ShowHeaders(false);
      }
      else if (_mode == Mode.Note)
      {
        ShowHeaders(false);
      }
      if (valueTags != null) valueTags.SetOwner(this);
      if (attachmentsList != null) attachmentsList.SetOwner(this);
      if (preview != null) preview.ObjectForScripting = new Scripting(owner, this);

      // hook to handle keyboard events
      Application.AddMessageFilter(this);
      if (chat)
      {
        owner.OnDialogChatCreated(this);
      }
    }

    private bool AdjustChatButtons()
    {
      bool chat = _mode == Mode.Chat;
      buttonEdit.Visible = chat;
      buttonSendChat.Visible = chat;
      buttonStartChat.Visible = !chat && _mode != Mode.Edit;
      ButtonSend.Visible = !chat;
      buttonDiff.Visible = !chat;
      buttonDebug.Visible = !chat;
      return chat;
    }

    private void ListChatUsers()
    {
      var usersCopy = new Dictionary<string, UserInfo>(_database.users);
      var toRemove = new List<string>();
      foreach (ChatUserItem user in chatUsers.Items)
      {
        if (!usersCopy.Remove(user.Name))
        {
          toRemove.Add(user.Name);
        }
      }
      chatUsers.BeginUpdate();
      foreach (var user in toRemove)
      {
        chatUsers.Items.RemoveByKey(user);
      }
      foreach (var user in _item._chatUsers)
      {
        if (chatUsers.FindItemWithText(user)!=null) continue;
        UserInfo info;
        if (usersCopy.TryGetValue(user, out info))
        {
          chatUsers.Items.Add(new ChatUserItem(user, info));
        }
      }
      chatUsers.EndUpdate();
    }

    public void UpdateChatUsers(Dictionary<string, UserInfo> users)
    {
      chatUsers.BeginUpdate();

      foreach (ChatUserItem item in chatUsers.Items)
      {
        UserInfo info;
        if (users.TryGetValue(item.Text, out info))
        {
          item.SetDetails(info);
        }
      }
      
      chatUsers.EndUpdate();
    }
    // start asynchronous loading of the article content
    private void LoadContent(bool askToReload)
    {
      // show that the article is loading
      valueURL.Enabled = false;
      valueURL.ForeColor = Color.Blue;
      valueTags.Enabled = false;
      valueContent.Enabled = false;
      ButtonSend.Enabled = false;
      buttonSave.Enabled = false;
      buttonDiff.Enabled = false;
      // until loaded, the article content is unavailable
      _svnUpdateFailed = true;

      // start the task
      ITask task = new TaskLoadArticle(this, database, valueURL.Text, _item, askToReload);
      _owner.AddTask(task);
    }
    // called when loading article is finished
    public void OnContentLoaded(bool underSVN, string content, string tags)
    {
      if (content == null)
      {
        string msg = "Colabo failed to read the article.";
        MessageBox.Show(msg, "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      // forget the old content in draft
      if (!string.IsNullOrEmpty(_draft))
      {
        File.Delete(_draft);
      }

      // extract attachments from articles
      ExtractAttachmentsRefs(database, _attachments, ref content);

      DisplayAttachments();

      if (underSVN)
      {
        valueURL.Enabled = true;
        valueURL.ForeColor = Color.FromKnownColor(KnownColor.WindowText);
        valueTags.Enabled = true;
        valueContent.Enabled = true;
        buttonSave.Enabled = true;
        buttonDiff.Enabled = true;
        if (tags != null)
        {
          // add the private tags
          if (_item._ownTags != null)
          {
            foreach (string tag in _item._ownTags)
            {
              if (ArticleItem.FromPrivateTag(tag) != null)
              {
                // private tags
                if (tags.Length > 0) tags += ", ";
                tags += tag;
              }
            }
          }
          // store to the control
          valueTags.Text = tags;
        }
        if (content != null)
        {
          valueContent.Text = content;
          ContentChanged = false;
        }
        // content loaded from the SVN
        _svnUpdateFailed = false;
      }
      else
      {
        valueURL.Enabled = true;
        valueURL.ForeColor = Color.Red;
        valueTags.Enabled = true;
        valueContent.Enabled = false;
        buttonSave.Enabled = false;
        buttonDiff.Enabled = false;
      }

      contentChanges = 0;
      _contentLength = valueContent.Text.Length;
      _changesBeforeChange = 0;

      if (_mode == Mode.Note)
      {
        if (_initialContent != null) QuoteInitialContent();
        else valueContent.Text = "";
        ContentChanged = false;
        minorEdit.Checked = false;
        minorEdit.Enabled = false;
        buttonSave.Enabled = false;
        buttonDiff.Enabled = false;
      }
      else
      {
        if (_item._id >= 0) minorEdit.Checked = true;
      }

      _textOriginal = valueContent.Text;
      _textBeforeChange = valueContent.Text;
      _textBeforeLastEdit = valueContent.Text;

      ButtonSend.Enabled = true;
      if (_mode==Mode.Note || _mode==Mode.Chat)
      {
        valueContent.Focus();
      }
    }

    private void DialogArticle_Load(object sender, EventArgs e)
    {
      _clientSize = ClientSize;

      // fill the controls
      valueDatabase.Items.Clear();
      valueDatabase.Items.AddRange(_owner.databases.ToArray());
      if (_database == null)
      {
        valueDatabase.Enabled = true;
        // select the most frequented database in the current view
        Dictionary<SQLDatabase, int> databases = new Dictionary<SQLDatabase, int>();
        foreach (IndexItem index in _owner.index)
        {
          SQLDatabase db = index._articleId._database;
          if (databases.ContainsKey(db))
            databases[db]++;
          else
            databases.Add(db, 1);
        }
        if (databases.Count > 0)
        {
          // find the best database
          int maxUsed = 0;
          SQLDatabase db = null;
          foreach (KeyValuePair<SQLDatabase, int> pair in databases)
          {
            if (pair.Value > maxUsed)
            {
              db = pair.Key;
              maxUsed = pair.Value;
            }
          }
          valueDatabase.SelectedItem = db;
        }
        else valueDatabase.SelectedIndex = 0;
      }
      else
      {
        valueDatabase.Enabled = false;
        valueDatabase.SelectedItem = _database;
      }

      valueTitle.Text = _item._title;
      valueTags.Text = ArticleItem.WriteTags(_item._ownTags);
      previewTags.Text = ArticleItem.WriteTags(_item._inheritedTags);

      if (_mode == Mode.Note || _mode == Mode.Chat)
      {
        valueContent.Focus();
      }

      // Here, list of tags can be changed for the SVN document
      InitContent();
      // content is corresponding to the URL
      _urlChanged = false;

      if (_initialContent != null)
      {
        if (_item._id < 0 && _item._parent >= 0) // New reply - quote the original article
        {
          // extract attachment from _initialContent first
          ExtractAttachmentsRefs(database, _attachments, ref _initialContent);
          DisplayAttachments();
          QuoteInitialContent();
        }
        else // Script output
        {
          valueContent.Text = _initialContent;
          valueContent.Select(0, 0);
        }
      }

      UpdatePreview();

      _contentChanged = false;
      contentChanges = 0;
      _contentLength = valueContent.Text.Length;
      _changesBeforeChange = 0;
      _textBeforeChange = valueContent.Text;
      _textBeforeLastEdit = valueContent.Text;
      _textOriginal = valueContent.Text;
      if (_item._id >= 0 && _mode!=Mode.Note && _mode!=Mode.Chat)
      {
        minorEdit.Checked = true;
      }
      else
      {
        // new article or append note is never minor edit
        minorEdit.Checked = false;
        minorEdit.Enabled = false;
      }

      SetTitle();
      _initialized = true;
    }

    private void QuoteInitialContent()
    {
      // quote the original article
      valueContent.Text = "";

      int i1 = 0;
      int i2 = _initialContent.IndexOf('\n', i1);
      while (i2 >= 0)
      {
        int lineEndIx = i2;
        if (i2 > 0 && _initialContent[i2 - 1] == '\r') lineEndIx--; //skip \r too
        valueContent.Text += "> " + _initialContent.Substring(i1, lineEndIx - i1) + Environment.NewLine;
        i1 = i2 + 1;
        i2 = _initialContent.IndexOf('\n', i1);
      }
      if (i1 < _initialContent.Length)
        valueContent.Text += "> " + _initialContent.Substring(i1);
      valueContent.Select();
    }

    private void DisplayAttachments()
    {
      if (_attachments.Count > 0)
      {
        for (int i = 0; i < _attachments.Count; i++)
        {
          attachmentsList.Items.Add(_attachments[i]);
        }
        ShowAttachmentPanel();
      }
    }
    private void DialogArticle_FormClosing(object sender, FormClosingEventArgs e)
    {
      // Check if some attachment changed
      List<Attachment> opened = new List<Attachment>();
      foreach (Attachment attachment in _attachments)
      {
        if (attachment.uploaded && attachment.changed && !string.IsNullOrEmpty(attachment.sourceFile))
        {
          // this one was changed, warn the user
          opened.Add(attachment);
        }
      }
      if (opened.Count > 0)
      {
        string msg = "Some attachments was probably changed: \r\n";
        foreach (Attachment attachment in opened)
        {
          msg += Path.GetFileName(attachment.sourceFile) + "\r\n";
        }
        msg += "\r\nChanges in these attachments will be lost. Are you sure you want to continue?";
        DialogResult result = MessageBox.Show(msg, "Colabo", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
        if (result != DialogResult.Yes)
        {
          e.Cancel = true;
          return;
        }
        // closing dialog - unlock locked attachments
        foreach (Attachment attachment in opened)
        {
          ITask taskAttachment = new TaskUnlockAttachment(attachment.sourceFile, database.GetArticlesStore());
          _owner.AddTask(taskAttachment);
        }
      }

      if (_mode == Mode.Chat && string.IsNullOrEmpty(valueContent.Text))
      { // chat with no message currently being written - nothing to confirm, send it
        if (_historyContent!=null)
        {
          if (!ArticleSend())
          {
            e.Cancel = true;
            return;
          }
        }
        else
        {
          RemoveArticleDraft();
        }
        if (_mode == Mode.Chat)
        {
          _owner.OnDialogChatDestroyed(this);
        }
        return;
      }
      if (ContentChanged || !string.IsNullOrEmpty(_draft))
      {
        string message = "Do you want to save changes of this message? (if you'll answer No, the content will be lost)";
        DialogResult result = MessageBox.Show(message, "Colabo", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
        switch (result)
        {
          case DialogResult.Yes:
            if (_mode == Mode.Note)
            {
              // TODO: draft saving for notes
              e.Cancel = true;
              return;
            }
            // backup the article
            SaveDraft();
            // remove the old backup
            if (!string.IsNullOrEmpty(_item._draftLocal)) File.Delete(_item._draftLocal);
            // mark the new backup
            _item._draftLocal = _draft;
            _owner.Synchronize(database, ArticleId.None);
            break;
          case DialogResult.No:
            // remove the current draft
            RemoveArticleDraft();
            break;
          case DialogResult.Cancel:
            e.Cancel = true;
            return;
        }
      }

      if (_selectUser != null)
      {
        _selectUser.Close();
      }
      if (_mode == Mode.Chat)
      {
        _owner.OnDialogChatDestroyed(this);
      }
      // unhook to handle keyboard events
      Application.RemoveMessageFilter(this);
    }

    private void RemoveArticleDraft()
    {
      if (!string.IsNullOrEmpty(_draft))
      {
        File.Delete(_draft);
        _owner.Synchronize(database, ArticleId.None); // draft could be already present in cache
      }
      if (_item._chatId >= 0)
      {
        _database.LeaveChat(_item._chatId);
      }
    }
    private void InitContent()
    {
      if (!string.IsNullOrEmpty(_item._draftLocal))
      {
        if (File.Exists(_item._draftLocal))
        {
          DocXML file = DocXML.Load(_item._draftLocal, "Article");
          if (file != null)
          {
            valueURL.Text = database.ToAbsolute(file.GetAttribute("url"));
            valueURL.ForeColor = Color.FromKnownColor(KnownColor.WindowText);
            valueContent.Text = file.GetValue();
            StructuredNode node = file.GetChild("chatHistory");
            if (node != null)
            {
              _historyContent = new ChatConversation(node);
            }
            return; // draft loaded
          }
        }

        // invalid draft
        _item._draftLocal = null;
      }
      if (_item._chatUsers!=null)
      {
        Trace.Assert(_mode == Mode.Chat);
        if (_item._chatId >= 0)
        {
          string itemURL = _item.AbsoluteURL(database);
          if (!string.IsNullOrEmpty(itemURL))
          {
            valueURL.Text = itemURL;
            _historyContent = _database.LoadChatConversation(_item._chatId,true);

          }
          return;
        }
      }
      else
      {
        Trace.Assert(_mode != Mode.Chat);
      }
      {
        string itemURL = _item.AbsoluteURL(database);
        if (!string.IsNullOrEmpty(itemURL))
        {
          valueURL.Text = itemURL;
          LoadContent(false);
        }
        else
        {
          char[] delimiters = new char[] { '\\', '/' };

          string path = "";
          if (_item._parent >= 0)
          {
            // inherit path from parent
            ArticleItem article;
            if (database.cache.TryGetValue(_item._parent, out article))
            {
              string parentURL = article.AbsoluteURL(database);
              int i = parentURL.LastIndexOfAny(delimiters);
              if (i >= 0) path = parentURL.Substring(0, i + 1);
            }
          }
          // fall-back
          if (path.Length == 0) path = database.svnRepository;

          // create a new document
          if (path.Length > 0 || database.IsEmptyPathEnabled())
          {
            // path will be decided during sending
            valueURL.Text = path + SVNDatabase._defaultFilename + ".txt";

            // content is valid only when SVN URI is given
            valueContent.Enabled = true;
            buttonSave.Enabled = true;
            valueURL.ForeColor = Color.FromKnownColor(KnownColor.WindowText);
          }
          else
          {
            valueURL.Text = "";
            valueContent.Enabled = false;
            buttonSave.Enabled = false;
            valueURL.ForeColor = Color.Red;
          }
          buttonDiff.Enabled = false;
        }
      }
    }



    // save the content regularly
    private void timer_Tick(object sender, EventArgs e)
    {
      // TODO: draft saving for append notes
      if (_contentChanged && _mode!=Mode.Note) SaveDraft();
    }

    struct OpenedAttachment
    {
      public string _filename;
      public string _application;

      public OpenedAttachment(string filename, string application)
      {
        _filename = filename; _application = application;
      }
    }

    // Tool strip 
    private void ButtonSend_Click(object sender, EventArgs e)
    {
      if (ArticleSend())
      {
        // close the dialog
        Close();
      }
    }

    private bool ArticleSend()
    {
      // Check if url was changed
      if (_urlChanged && !valueURL.Text.Contains(SVNDatabase._defaultFilename))
      {
        _urlChanged = false;
        LoadContent(true);
        return true;
      }


      // Update the article
      _item._title = valueTitle.Text;
      _item.RawURL = database.ToRelative(valueURL.Text);
      if (!ArticleItem.ReadTags(valueTags.Text, out _item._ownTags)) return true;
      _item.UpdateEffectiveTags();

      FormColabo.Log("Sending article: " + _item._title);

      // Backup the article (to do not lost it during sending)
      if (_mode!=Mode.Note)
      {
        SaveDraft();
      }
      // remove the old backup
      if (!string.IsNullOrEmpty(_item._draftLocal)) File.Delete(_item._draftLocal);
      if (_item._chatId >= 0)
      {
        if (valueContent.Text.Length == 0 && _historyContent==null)
        {
          _contentChanged = false; // no confirmation needed
          return true;
        }
        // no longer consider this a chat  - convert to a normal article
        Trace.Assert(_mode == Mode.Chat);

        if (_mode == Mode.Chat)
        {
          _owner.OnDialogChatDestroyed(this);
          _mode = Mode.New;
        }
        bool send = _database.LeaveChat(_item._chatId);
        _item._chatId = -1;
        _item._chatUsers = null; // TODO: make sure read status is maintained properly
        if (!send)
        {
          RemoveArticleDraft();
          _contentChanged = false; // no confirmation needed
          return true;
        }
      }


      // mark the new backup
      _item._draftLocal = _draft;
      _draft = null;

      // check access rights of the new article
      ArticleItem.VTCache cache = new ArticleItem.VTCache(database, _item);
      AccessRights access = _item.GetAccessRights(database, cache);
      if ((access & AccessRights.Read) == 0)
      {
        string msg = "You don't have permissions to send this article. Please, check if tags are set correctly.";
        MessageBox.Show(msg, "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return false;
      }
      else if ((access & AccessRights.Edit) == 0)
      {
        string msg = "You will not be able to edit this article later (check the tags). Do you want to send it anyway?";
        DialogResult result = MessageBox.Show(msg, "Colabo", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
        if (result != DialogResult.Yes) return false;
      }

      // check if some attachment is opened
      List<OpenedAttachment> opened = new List<OpenedAttachment>();
      foreach (Attachment attachment in _attachments)
      {
        if (attachment.editor != null && !attachment.editor.HasExited)
        {
          string filename = Path.GetFileName(attachment.sourceFile);
          string application = attachment.editor.ProcessName;
          opened.Add(new OpenedAttachment(filename, application));
        }
      }
      if (opened.Count > 0)
      {
        string msg = "Some attachments are probably opened for editing: \r\n";
        foreach (OpenedAttachment o in opened)
          msg += string.Format("{0} in {1}\r\n", o._filename, o._application);
        msg += "\r\nUnsaved changes may be lost. Do you want to send article anyway?";
        DialogResult result = MessageBox.Show(msg, "Colabo", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
        if (result != DialogResult.Yes) return false;
      }

      bool majorEdit = true;
      if (_item._id >= 0 && _mode==Mode.Edit) majorEdit = !minorEdit.Checked;

      FormColabo.Log(string.Format(" - {0} attachments, {1}", _attachments.Count, _mode==Mode.Note ? "append note" : majorEdit ? "major edit" : "minor edit"));

      // we know we want to send article now
      if (_item.AbsoluteURL(database).Contains(SVNDatabase._defaultFilename)) FindPathByTags(); // try to find nice path from tags

      ArticlesStore svn = database.GetArticlesStore();

      // Save the attachments asynchronously first
      for (int i=0; i<_attachments.Count; i++)
      {
        // do not upload already uploaded and unchanged older attachments (while editing article with attachments)
        Attachment attachment = _attachments[i];
        if (!attachment.uploaded)
        {
          // prepare attachment url (can be changed to make it unique)
          string url = _item.AbsoluteURL(database);
          url = url.Substring(0, url.LastIndexOf('/') + 1) + SVNDatabase._defaultFilename + Path.GetExtension(attachment.sourceFile);
          // Save the Attachment asynchronously
          ITask taskAttachment = new TaskSaveNewAttachment(url, i, majorEdit, this, svn);
          _owner.AddTask(taskAttachment);
        }
        else if (attachment.changed && !string.IsNullOrEmpty(attachment.sourceFile))
        {
          ITask taskAttachment = new TaskSaveAttachment(attachment.sourceFile, majorEdit, svn);
          _owner.AddTask(taskAttachment);
          attachment.changed = false; // do not check again in DialogArticle_FormClosing
        }
      }

      if (_mode!=Mode.Note || _contentChanged) // do not send append note without a _contentChanged
      {
        // Send the article asynchronously
        string content = UseHistoryContent(true) + valueContent.Text;
        ITask task = new TaskSendArticle(_item, content, _attachments, majorEdit, _owner, svn, database, _svnUpdateFailed, _mode==Mode.Note);
        _owner.AddTask(task);
      }

      _contentChanged = false; // no confirmation needed any more
      return true;
    }
    private void FindPathByTags()
    {
      string itemURL = _item.AbsoluteURL(database);
      // search for the repository info in the database
      RepositoryDirectories repository = database.FindRepository(itemURL);
      if (repository == null) return; // no info about the current directories
      
      // find where in the hierarchy our URL is
      string path = repository._root;
      string relative = itemURL.Substring(path.Length);
      List<DirectoryInfo> list = repository._directories;
      int index;
      while ((index = relative.IndexOf('/')) >= 0)
      {
        string subdir = relative.Substring(0, index);
        relative = relative.Substring(index + 1);
        path += subdir + "/";

        DirectoryInfo info = list.Find(delegate(DirectoryInfo i)
        {
          return i._name.Equals(subdir, StringComparison.OrdinalIgnoreCase);
        });
        if (info == null) return; // directory out of discovered hierarchy
        list = info._directories;
      }
      // relative now points to file name ... _item._url = path + relative

      // try to find sub-folders by tags - expand path by these folders
      if (_item._effectiveTags != null)
      {
        char[] invalidChars = Path.GetInvalidFileNameChars();
        bool found = true;
        while (found) // repeat until some sub-folder found
        {
          found = false;
          foreach (string tag in _item._effectiveTags)
          {
            string tagLowerCase = tag.ToLower();
            foreach (char c in invalidChars) tagLowerCase = tagLowerCase.Replace(c.ToString(), "");
            // check sub-folder corresponding to a tag,
            DirectoryInfo info = list.Find(delegate(DirectoryInfo i)
            {
              return i._name.ToLower() == tagLowerCase;
            });
            if (info == null) continue; // try the next tag

            found = true;
            list = info._directories;
            path += info._name + "/";
            break;
          }
        }
      }

      _item.RawURL = database.ToRelative(path + relative);
    }
    private void buttonSave_Click(object sender, EventArgs e)
    {
      SaveDraft();
    }
    private void buttonDiff_Click(object sender, EventArgs e)
    {
      if (_item._id < 0) return;

      // create a temporary file
      string path1 = Path.GetTempFileName();
      if (!string.IsNullOrEmpty(path1))
      {
        // export the HEAD revision from SVN
        if (SVNDatabase.Export(valueURL.Text, path1, this))
        {
          string path2 = Path.GetTempFileName();
          if (!string.IsNullOrEmpty(path2))
          {
            // save the current content
            StreamWriter writer = new StreamWriter(path2, false, System.Text.Encoding.UTF8);
            writer.Write(valueContent.Text);
            writer.Flush();
            writer.Close();

            // compare the files
            Process proc = FormColabo.SVNCommand("diff",
              string.Format("/path:\"{0}\" /path2:\"{1}\"", path2, path1));

            // wait for TortoiseProc and all child processes to exit
            proc.WaitForExit();

            // enumerate children
            List<Process> processes = new List<Process>();
            foreach (Process child in Process.GetProcesses())
            {
              PerformanceCounter counter =
                new PerformanceCounter("Process", "Creating Process ID", child.ProcessName);
              if (counter != null)
              {
                try
                {
                  int id = (int)counter.NextValue();
                  if (id == proc.Id) processes.Add(child);
                }
                catch (System.Exception)
                {
                }
              }
            }
            // wait
            foreach (Process child in processes)
            {
              child.WaitForExit();
            }
          }

          // read the file (can be modified)
          valueContent.Text = ReadFile(path2);

          // remove the temporary file
          File.Delete(path2);
        }

        // remove the temporary file
        File.Delete(path1);
      }
    }

    private void buttonDebug_Click(object sender, EventArgs e)
    {
      _oldState = SetDebuggingState(new DebuggingState("no", "no"));

      _scriptDebugNeeded = true;
      UpdatePreview();
    }
    private DebuggingState SetDebuggingState(DebuggingState value)
    {
      RegistryKey regKey = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Internet Explorer\\Main", true);
      if (regKey == null) return new DebuggingState(null, null);

      const string nameIE = "DisableScriptDebuggerIE";
      const string nameOther = "Disable Script Debugger";

      // store the original values
      Object oldIE = regKey.GetValue(nameIE);
      Object oldOther = regKey.GetValue(nameOther);
      // enable debugging
      if (value.stateIE != null) regKey.SetValue(nameIE, value.stateIE);
      if (value.stateOther != null) regKey.SetValue(nameOther, value.stateOther);

      // refresh the browser windows
      UIntPtr result;
      SendMessageTimeout(HWND_BROADCAST, (uint)WM_SETTINGCHANGE,
                         UIntPtr.Zero, UIntPtr.Zero,
                          SendMessageTimeoutFlags.SMTO_NORMAL, 1000, out result);

      return new DebuggingState(oldIE, oldOther);
    }

    // URL text box
    private void valueURL_TextChanged(object sender, EventArgs e)
    {
      if (_initialized)
      {
        _urlChanged = true;
        valueURL.ForeColor = Color.Blue;
        // if URL changes, decide who is the author again
        _item._author = "";
      }
    }
    private void valueURL_Leave(object sender, EventArgs e)
    {
      if (_urlChanged && !valueURL.Text.Contains(SVNDatabase._defaultFilename))
      {
        _urlChanged = false;
        LoadContent(true);
      }
    }

    // Content text box
    private void valueContent_TextChanged(object sender, EventArgs e)
    {
      if (_initialized)
      {
        ContentChanged = true;
        if (contentChanges >= 0 && _mode==Mode.Edit)
        {
          // compare with the "before last change"
          // detect "individual change" - individual change is when we discard matching start and end
          // and count how many characters are changed in between
          int changedChars = _changesBeforeChange + CountChanges(_textBeforeChange, valueContent.Text);

          // once there is a jump and it would be better to count as a two changes instead, do it
          int changesBeforeLastEdit = CountChanges(_textBeforeChange, _textBeforeLastEdit);
          int asTwoChanges = _changesBeforeChange + changesBeforeLastEdit + CountChanges(_textBeforeLastEdit, valueContent.Text);

          if (asTwoChanges<changedChars)
          {
            _changesBeforeChange = _changesBeforeChange + changesBeforeLastEdit;
            _textBeforeChange = _textBeforeLastEdit;

            changedChars = asTwoChanges;
          }

          int sinceOriginal = CountChanges(_textOriginal, valueContent.Text);
          if (sinceOriginal < changedChars)
          {
            changedChars = sinceOriginal;
            _textBeforeChange = _textOriginal;
            _changesBeforeChange = 0;

          }
          contentChanges = changedChars;

          //  contentChanges += Math.Max(1, Math.Abs(valueContent.Text.Length - _contentLength));
          minorEdit.Checked = (contentChanges < 50);

          _textBeforeLastEdit = valueContent.Text;
        }
        if (_item._chatId > 0)
        {
          if (valueContent.Text.Length > 0 && _contentLength == 0)
          { // started writing a response
            database.ReportTyping(_item._chatId, true);
          }
          else if (valueContent.Text.Length == 0 && _contentLength > 0)
          { // response erased - no longer consider typing
            database.ReportTyping(_item._chatId, false);
          }
        }
        _contentLength = valueContent.Text.Length;
        UpdatePreview();
      }
    }

    private static int CountChanges(String oldText, String newText)
    {
      int sameStart = 0;
      int sameEnd = 0;
      int shorter = Math.Min(oldText.Length, newText.Length);
      int longer = Math.Max(oldText.Length, newText.Length);
      for (int i = 0; i < shorter; i++)
      {
        if (newText[i] != oldText[i]) break;
        sameStart = i + 1;
      }

      for (int i = 0; i < shorter; i++)
      {
        if (newText[newText.Length - 1 - i] != oldText[oldText.Length - 1 - i])
          break;
        sameEnd = i + 1;
      }

      int changedChars = 0;
      if (sameStart < longer)
      {
        changedChars = Math.Max(1, Math.Max(longer - sameStart - sameEnd, longer - shorter));
      }
      return changedChars;
    }
    private bool ContentChanged
    {
      get { return _contentChanged; }
      set
      {
        if (_contentChanged != value)
        {
          _contentChanged = value; SetTitle();
        }
      }
    }
    private void SetTitle()
    {
      if (_mode == Mode.Chat) this.Text = string.Format("{0} - chat", valueTitle.Text);
      else if (_mode == Mode.Note) this.Text = string.Format("{0} - appending a note", valueTitle.Text);
      else if (_mode==Mode.Edit) this.Text = string.Format("{0} - editing", valueTitle.Text);
      else this.Text = string.Format("{0} - writing", valueTitle.Text);

#if DEBUG
      this.Text += " DEBUG";
#endif
      if (_contentChanged) this.Text += "*";
    }

    public const int FEATURE_DISABLE_NAVIGATION_SOUNDS = 21;
    private const int SET_FEATURE_ON_THREAD = 0x00000001;
    private const int SET_FEATURE_ON_PROCESS = 0x00000002;

    [DllImport("urlmon.dll")]
    [PreserveSig]
    [return: MarshalAs(UnmanagedType.Error)]
    internal static extern int CoInternetSetFeatureEnabled(int FeatureEntry, [MarshalAs(UnmanagedType.U4)] int dwFlags, bool fEnable);

    [DllImport("urlmon.dll")]
    [PreserveSig]
    [return: MarshalAs(UnmanagedType.Error)]
    internal static extern int CoInternetIsFeatureEnabled(int featureEntry, [MarshalAs(UnmanagedType.U4)] int dwFlags);

    public static bool SetInternetFeature(int feature, bool set)
    {
      int result = CoInternetIsFeatureEnabled(feature, SET_FEATURE_ON_PROCESS);
      CoInternetSetFeatureEnabled(feature, SET_FEATURE_ON_PROCESS, set);
      return result == HRESULT.S_OK;
    }

    static IntPtr HWND_BROADCAST = new IntPtr(0xffff);
    static IntPtr WM_SETTINGCHANGE = new IntPtr(0x001A);
    enum SendMessageTimeoutFlags : uint
    {
        SMTO_NORMAL = 0x0000,
        SMTO_BLOCK = 0x0001,
        SMTO_ABORTIFHUNG = 0x0002,
        SMTO_NOTIMEOUTIFNOTHUNG = 0x0008
    }
    [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
    private static extern IntPtr SendMessageTimeout(IntPtr hWnd,
                                            uint Msg,
                                            UIntPtr wParam,
                                            UIntPtr lParam,
                                            SendMessageTimeoutFlags fuFlags,
                                            uint uTimeout,
                                            out UIntPtr lpdwResult);


    
    // preview web browser
    private void UpdatePreview()
    {
      if (preview.ReadyState != WebBrowserReadyState.Complete && preview.ReadyState != WebBrowserReadyState.Uninitialized)
      {
        // not ready to be updated, add a request only
        _previewUpdateNeeded = true;
        return;
      }
      _previewUpdateNeeded = false;

      // insert the selection position to the text to find it in preview
      string text = UseHistoryContent();
      text += valueContent.Text.Substring(0, valueContent.SelectionStart) +
        "\x00" +  // Coma.ComaParser.Markers.SELECTION_OPEN
        valueContent.Text.Substring(valueContent.SelectionStart, valueContent.SelectionLength) +
        "\x01" +  // Coma.ComaParser.Markers.SELECTION_CLOSE
        valueContent.Text.Substring(valueContent.SelectionStart + valueContent.SelectionLength);

      text = text + AttachmentsRefs(database, _attachments, true);

      bool oldValue = SetInternetFeature(FEATURE_DISABLE_NAVIGATION_SOUNDS, true);
      // convert markdown formatted text to HTML
      try
      {
        string newText = Colabo.Configuration.GetCiteHTMLHeader() +
          (_scriptDebugNeeded ? "<script type=\"text/javascript\">debugger;</script>\n" : "") +
          Coma.ComaParser.Transform(database.svnRoot, database.urlTranslate, text, new List<string>()) +
          Colabo.Configuration.GetCiteHTMLFooter();
        if (newText != preview.DocumentText)
        {
          preview.DocumentText = newText;
        }
      }
      catch (System.Exception e)
      {
        Console.Write(e.ToString());
      }
      SetInternetFeature(FEATURE_DISABLE_NAVIGATION_SOUNDS, oldValue); 
    }

    private string UseHistoryContent(bool final=false)
    {
      string text = "";
      if (_historyContent != null)
      {
        text = _historyContent.GetText(_database, final ? null : _chatUserState);
        if (!string.IsNullOrEmpty(valueContent.Text)) text += FormatChatHeader(_database.user, null);
      }
      return text;
    }

    private void preview_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
      if (preview.Document != null && preview.Document.Body != null)
      {
        // find what element need to be scrolled
        HtmlElementCollection list = preview.Document.GetElementsByTagName("HTML");
        HtmlElement root = null;
        if (list.Count > 0) root = list[0];
        else root = preview.Document.Body;

        if (root != null)
        {
          // find the cursor
          foreach (HtmlElement element in root.GetElementsByTagName("A"))
          {
            if (element.Name == "cursor")
            {
              // find the center of the selection
              int top = element.OffsetRectangle.Top + element.OffsetRectangle.Height / 2;
              int left = element.OffsetRectangle.Left + element.OffsetRectangle.Right / 2;
              
              // center it in the preview
              root.ScrollTop = top - root.ClientRectangle.Height / 2;
              root.ScrollLeft = left - root.ClientRectangle.Width / 2;

              break;
            }
          }
        }
      }

      // handle preview request
      if (_previewUpdateNeeded)
      {
        UpdatePreview();
      }
      else if (_scriptDebugNeeded)
      {
        _scriptDebugNeeded = false;
        SetDebuggingState(_oldState);
      }
    }

    // Title text box
    private void valueTitle_TextChanged(object sender, EventArgs e)
    {
      SetTitle();
    }

    // Tags text box
    private void valueTags_TextChanged(object sender, EventArgs e)
    {
      if (_initialized)
      {
        if (contentChanges >= 0) // not changed manually yet
        {
          minorEdit.Checked = false;
          contentChanges = -1; // do not switch to Major change again
        }
      }
    }

    public class ListBoxWithDelete : ListBox
    {
      private DialogArticle _owner;
      public ListBoxWithDelete() {}
      public void SetOwner(DialogArticle owner) { _owner = owner; }
      protected override void OnKeyDown(KeyEventArgs e)
      {
        switch (e.KeyCode)
        {
          case Keys.Delete:
            if (SelectedIndex>=0)
            {
              // remove from the _attachments list
              Attachment att = _owner._attachments[SelectedIndex];
              
              // check if attachment was changed
              if (att.uploaded && att.changed && !string.IsNullOrEmpty(att.sourceFile))
              {
                string msg = string.Format("Attachment {0} was probably changed. These changes will be lost. Are you sure you want to continue?",
                  Path.GetFileName(att.sourceFile));
                DialogResult result = MessageBox.Show(msg, "Colabo", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result != DialogResult.Yes) break;
                // unlock locked attachment
                ITask taskAttachment = new TaskUnlockAttachment(att.sourceFile, _owner.database.GetArticlesStore());
                _owner._owner.AddTask(taskAttachment);
              }

              _owner._attachments.RemoveAt(SelectedIndex);
              // remove the link from text
              // find link to Attachment{att.linkIndex} and remove its text [suppose it is in the simple attachment form]
              string content = _owner.ValueContent;
              RemoveAttachment(ref content, att.linkIndex);
              _owner.ValueContent = content;
              // remove from the listbox control
              Items.RemoveAt(SelectedIndex);
            }
            break;
        }
        base.OnKeyDown(e);
      }
    }
    public class TextBoxWithAutocomplete : TextBox
    {
      private DialogArticle _owner = null;
      private Autocomplete _autocomplete = null;

      // used by the designer
      public TextBoxWithAutocomplete()
      {
      }

      public void SetOwner(DialogArticle owner)
      {
        _owner = owner;
        _autocomplete = new Autocomplete(this, new TagsInDatabase(owner));
      }

      private class TagsInDatabase : ICollectCandidates
      {
        private DialogArticle _owner;

        public TagsInDatabase(DialogArticle owner)
        {
          _owner = owner;
        }
        public void Process(string prefix, ref SortedList<string, int> result)
        {
          if (_owner != null && _owner.database != null)
            _owner.database.FindTags(prefix, ref result);
        }
      }

      protected override void OnKeyDown (KeyEventArgs e)
      {
        if (_autocomplete != null && _autocomplete.OnKeyDown(e)) return;
        base.OnKeyDown(e);
      }
      protected override void OnKeyUp(KeyEventArgs e)
      {
        if (_autocomplete != null && _autocomplete.OnKeyUp(e)) return;
        base.OnKeyUp(e);
      }
      protected override void OnLostFocus(EventArgs e)
      {
        if (_autocomplete != null) _autocomplete.OnLostFocus(e);
      }
    }

    // Database combo box
    private void valueDatabase_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (valueURL.Text.Contains(SVNDatabase._defaultFilename))
      {
        // change the path to the default SVN attached to the selected database
        string path = database.svnRepository;
        if (path.Length > 0 || database.IsEmptyPathEnabled())
        {
          // path will be decided during sending
          valueURL.Text = path + SVNDatabase._defaultFilename + ".txt";
        }
      }
      valueContent.SetDatabase(database);
    }

    // return the content of the file, handle correctly CR/LF
    public string ReadFile(string path)
    {
      StreamReader reader = new StreamReader(path, Encoding.UTF8);
      StringWriter writer = new StringWriter();
      string line;
      while ((line = reader.ReadLine()) != null) writer.WriteLine(line);
      reader.Close();
      return writer.ToString();
    }

    // IMessageFilter implementation
    // this will help us to handle keyboard event before offered to focused control
    public bool PreFilterMessage(ref Message m)
    {
      if (Form.ActiveForm != this) return false;

      if (m.Msg == 0x100) // WM_KEYDOWN
      {
        Keys keys = ((Keys)(int)m.WParam & Keys.KeyCode) | Control.ModifierKeys;
        return HandleKeyDown(keys);
      }
      else return false;
    }
    private bool HandleKeyDown(Keys keyData)
    {
      if ((keyData & Keys.Modifiers) == Keys.Control && (keyData & Keys.KeyCode) == Keys.Enter)
      { // Ctrl+Enter for send
        if (_mode == Mode.Chat)
        {
          SaveChat();
        }
        else
        {
          ButtonSend.PerformClick();
        }
        return true;
      }
      if ((keyData & Keys.Modifiers) == Keys.Control && (keyData & Keys.KeyCode) == Keys.S)
      { // Ctrl+S for save
        SaveDraft();
        return true;
      }
      return false; // not handled
    }

    private void DialogArticle_ClientSizeChanged(object sender, EventArgs e)
    {
      int diff = ClientSize.Height - _clientSize.Height;
      float coef = valueContent.Height / (preview.Height + valueContent.Height);
      if (coef > 0)
      {
        if (coef > 1) coef = 1;
        splitContainer1.SplitterDistance += (int)(coef * diff);
      }
      _clientSize = ClientSize;
    }
    private void SaveDraft()
    {
      if (_svnUpdateFailed) return; // do not save article with invalid content
      // assign a file name 
      if (string.IsNullOrEmpty(_draft))
      {
        string dir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
        if (!string.IsNullOrEmpty(dir))
        {
          int i = 0;
          do
          {
            i++;
            _draft = string.Format("{0}\\Colabo\\Draft\\draft_{1:D}.xml", dir, i);
          } while (File.Exists(_draft));
        }
      }

      // save the article with values modified by this dialog
      StructuredDoc file = new DocXML();
      StructuredDoc cls = file.CreateChild("Article");
      SaveTo(cls);

      if (!string.IsNullOrEmpty(_draft))
      {
        file.Save(_draft);
      }
    }

    private string FormatChatHeader(string user, DateTime? time)
    {
      return ChatConversation.FormatChatHeader(_database, user, time);
    }

    private void SaveChat()
    {
      if (_svnUpdateFailed) return; // do not save article with invalid content

      // save the article with values modified by this dialog
      var file = new DocJSON();

      if (_item._chatId < 0) // for a new chat we need to transmit the user list
      {
        SaveCommonTo(file.CreateChild("Article"));
        JsonObject userList = new JsonObject();
        JsonArray ar = new JsonArray();
        foreach (var u in _item._chatUsers)
        {
          ar.Add(u);
        }
        file.doc.Put("users", ar);
      }
      else
      {
        string replyText = valueContent.Text;
        file.SetAttribute("Reply", replyText);
      }
      string jsonChat = file.Export();
      // send document contents to the server
      int newId = _database.SendChatConversation(_item._chatId,jsonChat);
      if (newId >= 0)
      {
        // sent, remove the content
        valueContent.Text = "";
        _item._chatId = newId;
        LoadChat(true);
        UpdatePreview();
      }
    }

    private void LoadChat(bool active)
    {
      if (_item._chatId <= 0) return;
      _historyContent = _database.LoadChatConversation(_item._chatId,active);
    }
    private void SaveDraftHeaderTo(StructuredDoc cls)
    {
      cls.SetAttribute("server", database.server);
      cls.SetAttribute("database", database.database);
      if (_historyContent != null)
      {
        var history = cls.CreateChild("chatHistory");
        _historyContent.Save(history);
      }
    }

    private void SaveChatUserListTo(StructuredDoc cls)
    {
      if (_item._chatUsers == null) return;
      var userList = new StringBuilder();
      bool first = true;
      foreach (var user in _item._chatUsers)
      {
        if (!first) userList.Append(",");
        userList.Append(user);
        first = false;
      }

      cls.SetAttribute("chatUserList", userList.ToString());
    }

    private void SaveCommonTo(StructuredDoc cls)
    {
      cls.SetAttribute("id", _item._id.ToString());
      cls.SetAttribute("chatId", _item._chatId.ToString());
      cls.SetAttribute("parent", _item._parent.ToString());
      cls.SetAttribute("title", valueTitle.Text);
      cls.SetAttribute("url", database.ToRelative(valueURL.Text));
      string author = _item._author;
      if (string.IsNullOrEmpty(author)) author = database.user;
      cls.SetAttribute("author", author);
      cls.SetAttribute("date", DateTime.Now.ToString());
      cls.SetAttribute("tags", valueTags.Text);
      cls.SetAttribute("toRead", _item._toRead.ToString());
      cls.SetAttribute("changed", _item._changed.ToString());
      cls.SetAttribute("expanded", _item._expanded.ToString());
      cls.SetValue(valueContent.Text);
    }

    private void SaveTo(StructuredDoc cls)
    {
      SaveDraftHeaderTo(cls);
      SaveChatUserListTo(cls);
      SaveCommonTo(cls);
    }

    protected bool GetFilename(out string filename, DragEventArgs e)
    {
      bool ret = false;
      filename = String.Empty;

      if ((e.AllowedEffect & DragDropEffects.Copy) == DragDropEffects.Copy)
      {
        Array data = ((IDataObject)e.Data).GetData("FileName") as Array;
        if (data != null)
        {
          if ((data.Length == 1) && (data.GetValue(0) is String))
          {
            filename = ((string[])data)[0];
            if (System.IO.File.Exists(filename))
            {
              ret = true;
            }
          }
        }
      }
      return ret;
    }

    // filename got from drag&drop event is in ugly short 8.3 form
    // use pinvoke to convert it "back"
    [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    public static extern int GetLongPathName(
        [MarshalAs(UnmanagedType.LPTStr)]
        string path,
        [MarshalAs(UnmanagedType.LPTStr)]
        StringBuilder longPath,
        int longPathLength
        );

    string _dragAndDropLastFilename;
    bool _dragAndDropValidData;

    public List<Attachment> _attachments = new List<Attachment>();
    int _attWidth = 0; //width of attachment list control panel
    int _chatUsersHeight = 0; //height of chat user list control panel
    int _headersHeight = 0;
    private DialogSelectUser _selectUser;

    private void valueContent_DragEnter(object sender, DragEventArgs e)
    {
      // get the dropped file filename
      string filename;
      _dragAndDropValidData = GetFilename(out filename, e);
      StringBuilder longPath = new StringBuilder(255);
      GetLongPathName(filename, longPath, longPath.Capacity);
      filename = longPath.ToString();
      if (!string.IsNullOrEmpty(filename))
      {
        Debug.WriteLine(string.Format("Drag and drop enter - filename: {0}",filename));
        if (_dragAndDropLastFilename != filename)
        {
          _dragAndDropLastFilename = filename;
          string ext = Path.GetExtension(filename).ToLower();
        }
        e.Effect = DragDropEffects.Copy;
      }
      else
      {
        e.Effect = DragDropEffects.None;
      }
    }

    public string ValueContent
    {
      get { return valueContent.Text; }
      set { valueContent.Text = value; }
    }

    // Get the refs to attachments definitions (one attachment per line)
    public static StringBuilder AttachmentsRefs(SQLDatabase database, List<Attachment> attachments, bool localLinks)
    {
      StringBuilder attachmentsRefs = new StringBuilder();
      if (attachments != null)
      {
        for (int i = 0; i < attachments.Count; i++)
        {
          Attachment att = attachments[i];
          if (localLinks && !att.uploaded)
          {
            string fileNameNoSpaces = att.sourceFile.Replace(" ", "%20");
            attachmentsRefs.Append(string.Format("\r\n[Attachment{0}]: file:///{1}", att.linkIndex, fileNameNoSpaces));
          }
          else
          {
            string url = database.ToRelative(att.targetURL);
            attachmentsRefs.Append(string.Format("\r\n[Attachment{0}]: {1}", att.linkIndex, url));
          }
        }
      }
      return attachmentsRefs;
    }

    // attachments 
    /// <param name="attachments">List of attachments to be filled from Attachments Refs found in text</param>
    /// <param name="text">Text of article the attachments list should be listed from. These Refs are extracted.</param>
    public static void ExtractAttachmentsRefs(SQLDatabase database, List<Attachment> attachments, ref string text)
    {
      StringBuilder textOut = new StringBuilder();
      using (StringReader reader = new StringReader(text))
      {
        string line;
        while ((line = reader.ReadLine()) != null)
        {
          // is it Attachment Reference Style Link? format: $\[Attachment[:digit:]+\]: .*^ 
          string attachmentRefPattern = @"^\[Attachment([0-9]+)\]\: (\S+)$"; //something like "[Attachment1]: https://bistudio.com"
          Match match = Regex.Match(line, attachmentRefPattern);
          if (!string.IsNullOrEmpty(match.Groups[0].Value))
          {
            int attachmentNo = int.Parse(match.Groups[1].Value);
            string attachmentUrl = match.Groups[2].Value;
            string fullURL = database.ToAbsolute(attachmentUrl);
            //Debug.WriteLine(string.Format("[Attachment{0}]: {1}",attachmentNo, fullURL));
            if (attachments == null) attachments = new List<Attachment>();
            attachments.Add(new Attachment(fullURL, attachmentNo, true));  //already uploaded attachment, do not upload it again (with different number at the end)
          }
          else textOut.AppendLine(line); //line is not attachment ref-style link, leave it untouched in text
        }
      }
      text = textOut.ToString();
    }

    // removes link to [xxx][Attachment{attNo}], i.e. attachment with attNo
    private static void RemoveAttachment(ref string text, int attNo)
    {
      StringBuilder textOut = new StringBuilder();
      using (StringReader reader = new StringReader(text))
      {
        string line;
        while ((line = reader.ReadLine()) != null)
        {
          // is it Attachment Reference Style Link? format: [..url..][Attachment{attNo}]
          string attachmentPattern = @"([!]?\[[^\]]*\]\[Attachment{0}\])";
          attachmentPattern = string.Format(attachmentPattern, attNo);
          Match match = Regex.Match(line, attachmentPattern);
          line = Regex.Replace(line, attachmentPattern, ""); //" *LINK REPLACED* "
          textOut.AppendLine(line); //line is not attachment ref-style link, leave it untouched in text
        }
      }
      text = textOut.ToString();
    }

    private void valueContent_DragDrop(object sender, DragEventArgs e)
    {
      if (_dragAndDropValidData)
      {
        Point ccPos = valueContent.Location;
        Point dropPos = new System.Drawing.Point(e.X,e.Y);
        Point dropPosClient = valueContent.PointToClient(dropPos);
        int charpos = valueContent.GetCharIndexFromPosition(dropPosClient);
        // find first whitespace to insert link into
        for (; charpos<valueContent.Text.Length && !Char.IsWhiteSpace(valueContent.Text[charpos]); charpos++) ;
        // change link to images to images link format (leading '!' char)
        string ext = Path.GetExtension(_dragAndDropLastFilename).ToLower();
        int newAttNumber = 1;
        for (int i = 0; i < _attachments.Count; i++)
        {
          if (_attachments[i].linkIndex >= newAttNumber)
            newAttNumber = _attachments[i].linkIndex + 1;
        }

        _attachments.Add(new Attachment(_dragAndDropLastFilename, newAttNumber));
        string fileNameFixUnderscores = Path.GetFileName(_dragAndDropLastFilename).Replace("_",@"\_");
        string attachmentLink = string.Format("[{0}][Attachment{1}] ", fileNameFixUnderscores, newAttNumber);
        if ((ext == ".jpg") || (ext == ".png") || (ext == ".bmp") || (ext == ".gif"))
          attachmentLink = " !" + attachmentLink;
        else
          attachmentLink = " " + attachmentLink;
        // insert ref link as markdown to edited text
        valueContent.Text = valueContent.Text.Insert(charpos, attachmentLink);
        attachmentsList.Items.Add(_attachments[_attachments.Count - 1]);


        ShowAttachmentPanel();
      }
    }

    private void ShowAttachmentPanel()
    {
      if (!panelAttachments.Visible)
      {
        panelArticleDetails.Width = panelArticleDetails.Width - _attWidth;
        panelAttachments.Width = panelAttachments.Width + _attWidth;
        panelAttachments.Visible = true;

        Invalidate();
      }
    }
    private void ShowChatUsersPanel()
    {
      if (!groupChat.Visible)
      {
        preview.Height -= _chatUsersHeight;
        preview.Top += _chatUsersHeight;
        groupChat.Height += _chatUsersHeight;
        groupChat.Visible = true;
        splitContainer1.SplitterDistance += _chatUsersHeight / 2;
        Invalidate();
      }
    }

    private void attachmentsList_DoubleClick(object sender, EventArgs e)
    {
      // edit the attachment
      Attachment attachment = (Attachment)attachmentsList.SelectedItem;
      if (attachment != null)
      {
        if (attachment.uploaded)
        {
          // SVN operations need to be executed using Task
          // start the task
          ITask task = new TaskEditAttachment(database.GetArticlesStore(), attachment);
          _owner.AddTask(task);
        }
        else
        {
          try
          {
            attachment.editor = Process.Start(attachment.sourceFile);
            if (attachment.editor != null) attachment.changed = true;
          }
          catch (Win32Exception)
          {
            MessageBox.Show(string.Format("Cannot open file {0}, please check if application is associated to this file type", attachment.sourceFile), "Colabo");
          }
          catch (Exception exception)
          {
            Console.Write(exception.ToString());
          }
        }
      }
    }

    // implementation of IReloadPage
    public void ReloadPage()
    {
      UpdatePreview();
    }

    private void preview_Navigating(object sender, WebBrowserNavigatingEventArgs e)
    {
      if (e.Url.Scheme.Equals("about", StringComparison.OrdinalIgnoreCase) && 
        e.Url.LocalPath.Equals("blank", StringComparison.OrdinalIgnoreCase)) return;

      _owner.GoToArticle(e.Url);
      e.Cancel = true;
    }

    private void buttonEdit_Click(object sender, EventArgs e)
    {

    }

    private void buttonSendChat_Click(object sender, EventArgs e)
    {
      SaveChat();
    }

    private void buttonCloseChat_Click(object sender, EventArgs e)
    {
      ButtonSend_Click(sender, e);
    }

    private void buttonStartChat_Click(object sender, EventArgs e)
    {
      if (_mode == Mode.Chat) return;
      _mode = Mode.Chat;

      var chatUsers = new List<string>();
      chatUsers.Add(database.user);
      _item._chatUsers = chatUsers;
      
      ListChatUsers();
      ShowChatUsersPanel();
      ShowHeaders(false);
      AdjustChatButtons();
      _owner.OnDialogChatCreated(this);
      buttonStartChat.Visible = false;
      SetTitle();
    }

    internal bool IsChat(int id)
    {
      return _item._chatId == id;
    }

    internal void RefreshChat(ChatConversation.ChatData data, bool active)
    {
      if (_item._chatId >= 0)
      {
        bool update = false;
        if (data.revToRead > 0)
        {
          LoadChat(active);
          update = true;
        }
        _chatUserState = new ChatConversation.ChatData.UserList(data.users);
        // TODO: detect changes in user state
        update = true;
        if (update)
        {
          UpdatePreview();
        }
      }

    }

    private void buttonHeaders_Click(object sender, EventArgs e)
    {
      ShowHeaders(!groupHeaders.Visible);
    }

    private void ShowHeaders(bool show)
    {
      if (groupHeaders.Visible == show) return;
      groupHeaders.Visible = show;
      buttonHeaders.Image = show ? Properties.Resources.collapse : Properties.Resources.expander_16xLG;

      int move = show ? -_headersHeight : +_headersHeight;
      preview.Height += move;
      groupChat.Top -= move;
      preview.Top -= move;
      splitContainer1.SplitterDistance -= move / 2;
    }

    private void DialogArticle_Activated(object sender, EventArgs e)
    {
      if (_item._chatId >= 0)
      {
        _owner.RemoveChatNotifications(database, _item._chatId);
      }

    }

    private void buttonAddUser_Click(object sender, EventArgs e)
    {
      if (_selectUser != null) return;
      _selectUser = new DialogSelectUser(this);
      _selectUser.Show(this);
      buttonAddUser.Enabled = false;
    }


    internal void SelectUserClosed()
    {
      _selectUser = null;
      buttonAddUser.Enabled = true;
    }

    internal void AddChatUsers(List<string> users)
    {
      foreach (var user in users)
      {
        if (_item._chatUsers.Contains(user)) continue;
        _item._chatUsers.Add(user);
      }
      ListChatUsers();
    }

    private void removeChatMenuItem_Click(object sender, EventArgs e)
    {
      ChatUserItem item = (ChatUserItem)chatUsers.FocusedItem;
      if (item == null) return;
      string name = item.Name;
      _item._chatUsers.Remove(name);
      chatUsers.Items.Remove(item);
      // if we are removing ourselves, close the window for us?
      if (name == database.name)
      {
        // TODO: remove user from the chat
      }
    }

    private void chatUsers_DragDrop(object sender, DragEventArgs e)
    {
      if (e.Data.GetDataPresent("System.Windows.Forms.ListView+SelectedListViewItemCollection", false))
      {
        var dropped = (ListView.SelectedListViewItemCollection)e.Data.GetData("System.Windows.Forms.ListView+SelectedListViewItemCollection");
        var toAdd = new List<string>();
        foreach (ChatUserItem item in dropped)
        {
          if (item==null) continue;
          string name = item.Name;
          if (!database.users.ContainsKey(name)) continue;
          toAdd.Add(name);
        }
        AddChatUsers(toAdd);
      }
    }

    private void chatUsers_DragEnter(object sender, DragEventArgs e)
    {
      e.Effect = DragDropEffects.Copy;
    }

    private void chatUsers_ItemDrag(object sender, ItemDragEventArgs e)
    {
      chatUsers.DoDragDrop(chatUsers.SelectedItems, DragDropEffects.Move);
    }
  }

  public class Attachment
  {
    public string sourceFile;   //local file
    public string targetURL;    //corresponding url on svn server
    public bool uploaded = false;       //true for already existing attachments (parsed from existing article while its editing)
    public bool changed = false;  // true when attachment was opened for editing
    public Process editor = null; // process of application attachment was opened with
    public int linkIndex;
    public Attachment() { }
    public Attachment(string sourceFile, int attNo) 
    {
      this.sourceFile = sourceFile;
      this.linkIndex = attNo;
    }
    public Attachment(string targetURL, int attNo, bool uploaded) 
    { 
      this.targetURL = targetURL; 
      this.linkIndex = attNo;  
      this.uploaded = true; /*even when third argument is false!*/
    }
    public override string ToString() 
    {
      if (!uploaded)
        return Path.GetFileName(sourceFile);
      else
      {
        int ix = targetURL.LastIndexOf('/');
        if (ix >= 0)
        {
          return targetURL.Substring(ix + 1);
        }
        return targetURL;
      }
    }
  }

  class TaskLoadArticle : ITask
  {
    private DialogArticle _owner;
    private SQLDatabase _database;
    private string _url;
    private ArticleItem _item;
    private string _content;
    private string _tags;
    private bool _askToReload;
    private bool _underSVN;

    public override string ProgressType() { return "Read"; }

    public TaskLoadArticle(DialogArticle owner, SQLDatabase database, string url, ArticleItem item, bool askToReload)
    {
      _owner = owner;
      _database = database;
      _url = url;
      _item = item;
      _content = null;
      _tags = null;
      _askToReload = askToReload;
      _underSVN = false;
    }

    public override void Process(BackgroundWorker worker)
    {
      string message = "Loading article ...";
      worker.ReportProgress(0, message);

      ArticlesStore svn = _database.GetArticlesStore();

      string itemURL = _item.AbsoluteURL(_database);
      string path;
      if (itemURL == _url)
      {
        int changedRevision = _database.LastChangeRevision(_item._id);
        svn.UpdateWorkingCopy(_database, _item, true, changedRevision);  // update the article to show the current revision
        path = _item._workingCopy;
      }
      else
      {
        // url has changed, the working copy is not longer valid
        _item._workingCopy = null;
        path = svn.GetWorkingCopy(_url, true);
      }
      worker.ReportProgress(50, message);

      if (path.Length > 0)
      {
        // content is valid only when SVN URI is given
        _underSVN = true;
        string pathFilesystem = System.Uri.UnescapeDataString(path);
        if (File.Exists(pathFilesystem))
        {
          if (_askToReload)
          {
            string msg = string.Format("'{0}' is a valid SVN file. Do you want to reload the content and the tags? (if you answer No, the article will be rewritten by the current content)", _url);
            DialogResult result = MessageBox.Show(msg, "Colabo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (!result.Equals(DialogResult.Yes))
            {
              worker.ReportProgress(100, message);
              return;
            }
          }

          // read the file content
          _content = _owner.ReadFile(pathFilesystem);
          // read the properties
          svn.LoadProperties(path, out _tags);
        }
      }
      worker.ReportProgress(100, message);
    }

    public override void Finish()
    {
      // update the owner if still open
      if (!_owner.Disposing && _owner.IsHandleCreated) _owner.OnContentLoaded(_underSVN, _content, _tags);
    }
  }

  class TaskSendArticle : ITask
  {
    private ArticleItem _item;
    private string _content;
    private bool _appendNote;
    private List<Attachment> _attachments;
    private bool _majorRevision;
    private FormColabo _owner;
    private ArticlesStore _svn;
    private SQLDatabase _database;
    private bool _svnUpdateFailed;
    // keep result of Commit, if failed, keep the article as a draft 
    private bool _ok;

    public override string ProgressType() { return "Send"; }

    public TaskSendArticle(ArticleItem item, string content, List<Attachment> attachments, bool majorRevision, FormColabo owner, ArticlesStore svn, SQLDatabase database, bool svnUpdateFailed, bool appendNote)
    {
      _item = item;
      _appendNote = appendNote;
      _content = content;
      _attachments = attachments;
      _majorRevision = majorRevision;
      _owner = owner;
      _svn = svn;
      _database = database;
      _svnUpdateFailed = svnUpdateFailed;
      _ok = false;
    }

    public override void Process(BackgroundWorker worker)
    {
      string message = "Sending article ...";
      worker.ReportProgress(0, message);


      string itemURL = _item.AbsoluteURL(_database);
      bool newArticle = itemURL.Contains(SVNDatabase._defaultFilename);

      FormColabo.Log(string.Format(" - sending {0} article (working copy {1})", newArticle ? "new" : "old",
        string.IsNullOrEmpty(_item._workingCopy) ? "unknown" : _item._workingCopy));

      // if article was not correctly read from SVN, do not update it
      string workingPath = "";
      if (!_svnUpdateFailed)
      {
        _svn.UpdateWorkingCopy(_database, _item, false, int.MaxValue); // do not update the article, keep the copy we worked with
        workingPath = _item._workingCopy;
      }

      worker.ReportProgress(20);

      // Write the content and tags to the working copy and commit the article
      if (workingPath.Length > 0)
      {
        UpdateText process = (content) => _content + DialogArticle.AttachmentsRefs(_database, _attachments, false);
        if (_appendNote)
        {
          // append a note header.
          // in the content add a space before every line, TODO: not robust, might not work due to line end escaping
          _content = string.Format("\n[{0} {1}]:\n {2}\n", _database.user, DateTime.Now, _content.Replace("\n", "\n "));

          process = (content) =>
          {
            string note = _content;
            List<Attachment> attachments = new List<Attachment>();
            DialogArticle.ExtractAttachmentsRefs(_database, attachments, ref content);
            //attachments.AddRange(_attachments);
            content = content + note + DialogArticle.AttachmentsRefs(_database, _attachments, false);
            return content; 
          };
        }

        string path = _svn.CommitArticle(_database, ref _item, ref workingPath, process, _majorRevision, worker);
        _ok = !string.IsNullOrEmpty(path);
        if (!_ok) FormColabo.Log(" - commit failed"); 
      }
      else if (newArticle || !string.IsNullOrEmpty(_content))
      {
        // some content not stored to SVN detected
        string text = string.Format("Sending of article '{0}' failed. Article was stored to drafts. Please, try to send it later.", _item._title);
        MessageBox.Show(text, "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        _ok = false;
      }
      else
      {
        // for articles outside SVN, store the last header update
        if (string.IsNullOrEmpty(_item._author)) // try to keep the original author
          _item._author = _database.user;
        _item._date = DateTime.Now;
        _item._revision = 0;
        _item._majorRevision = 0;
        _ok = true;
      }

      FormColabo.Log(" - sent to SVN");

      worker.ReportProgress(100);
    }

    public override void Finish()
    {
      if (_ok)
      {
        // send it to database
        _item._id = _database.SendArticle(_item);

        FormColabo.Log(string.Format(" - sent to database, id = {0}", _item._id));

        // all finished correctly, draft is no more needed
        if (!string.IsNullOrEmpty(_item._draftLocal) && _item._id >= 0)
        {
          File.Delete(_item._draftLocal);
          _item._draftLocal = null;
        }
        if (_item._chatId >= 0)
        {
          _database.DeleteChat(_item._chatId);
        }
      }

      // synchronize only the affected database
      if (!_owner.Disposing && _owner.IsHandleCreated) _owner.Synchronize(_database, ArticleId.None); // Synchronize(id)
    }
  }

  class TaskAppendNote : ITask
  {
    private FormColabo _owner;
    private ArticleItemWithDb _item;
    private string _text;
    private DateTime _date;

    public override string ProgressType() { return "Send"; }

    public TaskAppendNote(FormColabo owner, ArticleItemWithDb item, string text, DateTime date)
    {
      _owner = owner;
      _item = item;
      _text = text;
      _date = date;
    }

    public override void Process(BackgroundWorker worker)
    {
      string message = "Sending note ...";
      worker.ReportProgress(0, message);

      ArticlesStore svn = _item._database.GetArticlesStore();

      svn.UpdateWorkingCopy(_item._database, _item._item, true, int.MaxValue); // update the article, then append the note

      worker.ReportProgress(50);

      string path = _item._item._workingCopy;
      // append the note to the file and commit to SVN
      if (path.Length > 0 && File.Exists(path))
      {
        StreamWriter writer = new StreamWriter(path, true, System.Text.Encoding.UTF8);
        //        writer.Write(string.Format("\n\n_____\n**{0}** ({1})\n\n{2}", _item._database.user, _date, _text));
        writer.Write(string.Format("\n[{0} {1}]:\n {2}\n", _item._database.user, _date, _text.Replace("\n", "\n ")));  // add a space before every line, TODO: might not work due to line end escaping
        writer.Close();

        svn.Commit(path, _item._item._title);
        svn.UpdateRevisionAndDate(path, true, ref _item._item);
      }

      worker.ReportProgress(100);
    }

    public override void Finish()
    {
      // update revision number in database
      if (!_owner.Disposing && _owner.IsHandleCreated) _owner.UpdateRevisionAndDate(_item);
    }
  }

  public class ImportedArticle
  {
    public string _path;
    public string _tags;
    public ImportedArticle() {}
    public ImportedArticle(string path, string tags) {_path=path; _tags=tags; }
  }

  class TaskCommitWorkingPathRoot : ITask
  {
    private string _path;
    private string _title;
    private ArticlesStore _svn;
    private List<ImportedArticle> _articles;

    public override string ProgressType() { return "Working Copy"; }

    public TaskCommitWorkingPathRoot(string path, string title, List<ImportedArticle> articles, ArticlesStore svn)
    {
      _path = path;
      _title = title;
      _svn = svn;
      _articles = articles;
    }

    public override void Process(BackgroundWorker worker)
    {
      string message = "Committing the WP folder...";
      worker.ReportProgress(35, message);
      _svn.AddFiles(_articles);
      worker.ReportProgress(75, message);
      _svn.Commit(_path, _title);
      worker.ReportProgress(100, message);
    }

    public override void Finish()
    {
      //nothing to be done in the main thread afterwards
    }
  }

  class TaskGetWorkingPathRoot : ITask
  {
    StringBuilder _workingCopyRoot;
    private ArticlesStore _svn;
    private string _path;

    public override string ProgressType() { return "Working Copy"; }

    public TaskGetWorkingPathRoot(string path, StringBuilder workingCopyRoot, ArticlesStore svn)
    {
      _path = path;
      _workingCopyRoot = workingCopyRoot;
      _svn = svn;
    }

    public override void Process(BackgroundWorker worker)
    {
      string message = "Getting working path root...";
      worker.ReportProgress(30, message);
      _workingCopyRoot.Append(_svn.GetWorkingCopy(_path, false)); // do not update the article, we only need the path
      worker.ReportProgress(100, message);
    }

    public override void Finish()
    {
      //nothing to be done in the main thread afterwards
    }
  }

  class TaskSaveNewAttachment: ITask
  {
    private string _url;
    private string _attachmentFile;
    private int _attachmentIx;
    private bool _majorRevision;
    private DialogArticle _owner;
    private ArticlesStore _svn;

    public override string ProgressType() { return "Send"; }

    public TaskSaveNewAttachment(string url, int attachmentIx, bool majorRevision, DialogArticle owner, ArticlesStore svn)
    {
      _url = url;
      _attachmentFile = owner._attachments[attachmentIx].sourceFile;
      _attachmentIx = attachmentIx;
      _majorRevision = majorRevision;
      _owner = owner;
      _svn = svn;
    }

    public override void Process(BackgroundWorker worker)
    {
      string message = "Sending attachment ...";
      worker.ReportProgress(0, message);

      string workingPath = _svn.GetWorkingCopy(_url, false); // do not update the attachment, keep the copy we worked with

      worker.ReportProgress(20);

      // Write the content and tags to the working copy and commit the article
      if (workingPath.Length > 0)
      {
        string Path = _svn.CommitNewAttachment(ref _url, ref workingPath, _attachmentFile, _majorRevision, worker);
      } 
      worker.ReportProgress(100);
    }

    public override void Finish()
    {
      // replace the former link to attachment from local place to SVN repository
      _owner._attachments[_attachmentIx].targetURL = _url; // FIX: condition removed (this one is not a function call on disposed object)
    }
  };

  class TaskSaveAttachment : ITask
  {
    private string _workingPath;
    private bool _majorRevision;
    private ArticlesStore _svn;

    public override string ProgressType() { return "Send"; }

    public TaskSaveAttachment(string workingPath, bool majorRevision, ArticlesStore svn)
    {
      _workingPath = workingPath;
      _majorRevision = majorRevision;
      _svn = svn;
    }
    public override void Process(BackgroundWorker worker)
    {
      string message = "Sending attachment ...";
      worker.ReportProgress(0, message);
      _svn.Unlock(_workingPath);
      _svn.Commit(_workingPath, "Attachment update", _majorRevision);
      worker.ReportProgress(100);
    }
    public override void Finish()
    {
    }
  };

  class TaskUnlockAttachment : ITask
  {
    private string _workingPath;
    private ArticlesStore _svn;

    public override string ProgressType() { return "Send"; }

    public TaskUnlockAttachment(string workingPath, ArticlesStore svn)
    {
      _workingPath = workingPath;
      _svn = svn;
    }
    public override void Process(BackgroundWorker worker)
    {
      string message = "Sending attachment ...";
      worker.ReportProgress(0, message);
      _svn.Unlock(_workingPath);
      worker.ReportProgress(100);
    }
    public override void Finish()
    {
    }
  };

  class TaskEditAttachment : ITask
  {
    private ArticlesStore _svn;
    private Attachment _attachment;
    private String _workingPath;

    public override string ProgressType() { return "Read"; }

    public TaskEditAttachment(ArticlesStore svn, Attachment attachment)
    {
      _svn = svn;
      _attachment = attachment;
      _workingPath = null;
    }

    public override void Process(BackgroundWorker worker)
    {
      string message = "Loading attachment ...";
      worker.ReportProgress(0, message);

      _workingPath = _svn.GetWorkingCopy(_attachment.targetURL, true);

      worker.ReportProgress(50);

      // Write the content and tags to the working copy and commit the article
      if (!string.IsNullOrEmpty(_workingPath))
        _svn.Lock(_workingPath);

      worker.ReportProgress(100);
    }

    public override void Finish()
    {
      if (!string.IsNullOrEmpty(_workingPath))
      {
        try
        {
          _attachment.editor = System.Diagnostics.Process.Start(_workingPath);
          if (_attachment.editor != null)
          {
            _attachment.changed = true;            _attachment.sourceFile = _workingPath;
          }
        }
        catch (Win32Exception)
        {
          MessageBox.Show(string.Format("Cannot open file {0}, please check if application is associated to this file type", _workingPath), "Colabo");
        }
        catch (Exception exception)
        {
          Console.Write(exception.ToString());
        }
      }
    }
  }
}
