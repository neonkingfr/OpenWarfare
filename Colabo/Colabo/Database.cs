using System;
using System.Threading;
using System.Windows.Forms;
using System.Collections.Generic;

using System.IO;
using System.Xml;
using System.Text;
using System.Diagnostics;
using System.DirectoryServices.Protocols;

using Token = Antlr.Runtime.IToken;

// Communication with ColaboWebServices
using System.Net;
using Jayrock.Json;
using Jayrock.Json.Conversion;
using Colabo.UserActivityMonitor;

namespace Colabo
{
  public delegate string CallPresence();
  /// additional info about the user
  public class UserInfo
  {
    // name used in UI
    public string _fullName;
    // URL to the user picture
    public string _picture;
    // user color
    public int _color;

    public string _presence;
    public string _location;
    public string _ipAddr;
    public string _build;
    
    public DateTime _presenceTime;

    //public CallPresence _callPresence;
    //public IAsyncResult _presenceResult;

  }


  public delegate string UpdateText(string content);

  /// Abstract storage for content of articles
  public interface ArticlesStore
  {
    // read the current content of the article
    string GetArticleContent(string path, ref BrowserRemotingProxy browserProxy);

    // avoid user interaction
    bool SetSilent(bool silent);
    // set item._working copy, update the working copy if asked
    void UpdateWorkingCopy(SQLDatabase database, ArticleItem item, bool update, int changedRevision);
    // return the working copy path, update if asked
    string GetWorkingCopy(string path, bool update);
    // save the article content and commit to store
    string CommitArticle(SQLDatabase database, ref ArticleItem item, ref string path, UpdateText content, bool majorRevision, System.ComponentModel.BackgroundWorker worker);
    string CommitNewAttachment(ref string url, ref string path, string attachmentFile, bool majorRevision, System.ComponentModel.BackgroundWorker worker);
    // remove the article
    void DeleteArticle(string path, string title);
    // change the date, revision and major revision of the article
    void UpdateRevisionAndDate(string path, bool majorRevision, ref ArticleItem item);
    // read the specific revision of the file
    string GetArticleRevision(string url, int revision);
    // read the properties of the specific revision of the file
    List<string> GetPropertiesRevision(string url, int revision);
    // try to read the properties from the repository
    bool LoadProperties(string path, out string tags);
    // find the revision of the last change of the working copy
    int GetLocalRevision(string path);

    // simple commit of the file
    void Commit(string path, string title);
    void Commit(string path, string title, bool majorRevision);
    // collect available revisions of article
    void GetLog(ref SVNLog logRef, Uri url, System.ComponentModel.BackgroundWorker worker);
    // lock the file (attachment editing)
    void Lock(string path);
    // unlock the file (attachment editing)
    void Unlock(string path);

    // helpers for full-text indexing
    Uri GetRepositoryRoot(Uri url);
    string GetRepositoryGuid(Uri url);
    string GetRepositoryGuidUnsafe(Uri url);

    // helper for fast search of basic repository info
    bool GetRepositoryInfo(Uri url, out string root, out string uuid, bool silent);

    // helper to add imported articles to the store
    void AddFiles(List<ImportedArticle> articles);

    // helpers for mass change of tags
    void StoreTagsToSVN(SQLDatabase database, ArticlesCache cache);
    bool UpdateTags(SQLDatabase database, ref ArticleItem item, bool majorRevision);

    // diagnostics and benchmark
    string SVNBenchmark();
  }

  /// Abstract database engine
  abstract public class DBEngine
  {
    /// Connection info
    protected DBInfo _info;

    protected static ArticlesStore _defaultStore = null;
    public static ArticlesStore GetDefaultArticlesStore()
    {
      return _defaultStore;
    }

    public DBEngine(DBInfo info)
    {
      _info = info;
      if (_defaultStore == null)
      {
        // initialize shared default implementation of articles storage
        _defaultStore = new SVNDatabase(Program.config);
      }
    }

    public DBInfo info { get { return _info; } }
    // overridden in some DBEngine implementations
    virtual public ArticlesStore GetArticlesStore()
    {
      return _defaultStore;
    }
    // empty URL path is enabled for new articles
    virtual public bool IsEmptyPathEnabled()
    {
      return false;
    }

    virtual public void OnClose() { }
    abstract public DBUserInfo LoadUserInfo();
    abstract public void SaveUserInfo(DBUserInfo userInfo);
    // save the user to the database (used to update database from LDAP)
    abstract public void SaveUser(UserInfo userInfo);
    public class UserList: Dictionary<string, UserInfo> {};
    public delegate void NotifyUserChange(UserList users);
    public delegate void NotifyChat(ChatConversation.ChatData data);
    virtual public void ChangePresenceState(string state) { }
    virtual public void SetLocationName(string ipAddr, string location) {}

    abstract public void LoadUsers(ref Dictionary<string, UserInfo> users, ref Dictionary<string, string> aliases, NotifyUserChange notifyUserChange, NotifyChat notifyChat);
    abstract public void SaveUsers(List<ManageUserInfo> users, List<ManageGroupInfo> groups, List<ManageGroupUserInfo> groupUsers);
    abstract public void LoadUserSettings(ref List<string> groups, ref List<Token> accessRights, ref int homepage);
    abstract public int LoadHomepage();
    abstract public void LoadRuleSets(ref List<RuleSet> ruleSets);
    abstract public void SaveRuleSets(ListBox.ObjectCollection ruleSets);
    abstract public void LoadBookmarks(ref List<Bookmark> bookmarks, SQLDatabase database);
    abstract public int SaveBookmark(Bookmark bookmark, SQLDatabase database);
    abstract public void UpdateBookmark(Bookmark bookmark, SQLDatabase database);
    abstract public void DeleteBookmark(int id, int parent);
    abstract public void LoadScripts(ref List<Script> scripts);
    abstract public void SaveScripts(List<Script> scripts);
    abstract public void SaveHomepage(int homepage);
    abstract public void LoadArticles(ref ArticlesCache cache, System.ComponentModel.BackgroundWorker worker);
    abstract public int SaveArticle(ArticleItem item);
    abstract public void DeleteArticle(ArticleItem item);
    abstract public int LoadArticleRead(int id);
    abstract public void SaveArticleRead(int id, int revision);
    abstract public void DeleteArticleRead(int id);
    abstract public void SaveArticleCollapsed(int id);
    abstract public void DeleteArticleCollapsed(int id);
    abstract public int LoadRevision(int id);
    abstract public void SaveRevisionAndDate(ArticleItem item);
    abstract public void SaveParent(int id, int parent);
    abstract public void UpdateTag(int id, string addTag, string removeTag);
    // abstract public void SaveTags(ArticlesCache cache);
    abstract public string GetArticleRevision(int id, int revision);
    abstract public bool IsFullTextIndexLocal();
    abstract public string CheckFullTextIndex();
    abstract public List<int> FullTextRevisionsOfArticle(ArticleItem item);
    abstract public bool FullTextNeedToIndexArticle(ArticleItem item, RevisionInfo revision);
    abstract public void FullTextIndexArticle(ArticleItem item, RevisionInfo revision, string content);
    abstract public List<SearchResult> FullTextSearch(SQLDatabase database, string query);
    abstract public double Benchmark();

    virtual public int StartChat(string user, List<string> users) { return -1; }
    virtual public bool LeaveChat(int p) { return false; }
    virtual public void DeleteChat(int p) {  }
    virtual public int SendChatConversation(int id, string jsonChat) { return id; }
    virtual public void ReportTyping(int chat, bool typing) { }

    virtual public ChatConversation LoadChatConversation(int id, bool active) { return null; }

    virtual public List<int> LoadChats() {return null;}

    virtual public bool LoadChatHeader(int chat, ArticleItem item) {return false;}

  };

  /// encapsulation of operations over single SQL database
  public class SQLDatabase
  {
    /// Database engine
    private DBEngine _engine;


    /// cache of articles
    private ArticlesCache _cache;

    // list of users
    private Dictionary<string, UserInfo> _users;
    // user aliases
    private Dictionary<string, string> _aliases;

    // groups the user belongs to
    private List<string> _groups;

    // compiled rule set for access rights
    private List<Token> _accessRights;

    // id of the starting article
    private int _homepage;

    // user added bookmarks
    private List<Bookmark> _bookmarks;

    // global scripts
    private List<Script> _scripts;

    public NotifyIdle.Notification _idleNotify;

    // found SVN repositories and their directory structure
    List<RepositoryDirectories> _directories;

    // definition of tag
    public class TagInfo
    {
      public int _count;
      // TODO: tag properties

      public TagInfo()
      {
        _count = 1;
      }
    }

    // list of all tags used in the database
    private SortedList<string, TagInfo> _tags;

    public string name
    {
      get {return _engine.info._name;}
    }
    public string user
    {
      get { return _engine.info._user; }
    }
    public List<string> groups
    {
      get { return _groups; }
    }
    public List<Token> accessRights
    {
      get { return _accessRights; }
    }
    public int homepage
    {
      get { return _homepage; }
      set { _homepage = value; _engine.SaveHomepage(_homepage); }
    }
    public List<Bookmark> bookmarks
    {
      get { return _bookmarks; }
    }
    public List<Script> scripts
    {
      get { return _scripts; }
    }
    public string server
    {
      get { return _engine.info._server; }
    }
    public string database
    {
      get { return _engine.info._database; }
    }
    public string svnRepository
    {
      get { return _engine.info._svnRepository; }
    }
    public string svnRoot
    {
      get { return _engine.info._svnRoot; }
    }
    public string urlTranslate
    {
      //get { return "http://localhost:8084/MultiLang/Translate"; }
      get { return _engine.info._urlTranslate; }
    }
    public ArticlesCache cache
    {
      get { return _cache; }
    }
    public Dictionary<string, UserInfo> users
    {
      get { lock (_users) { return _users; } }
    }
    public Dictionary<string, string> aliases
    {
      get { return _aliases; }
    }
    public override string ToString()
    {
      return _engine.info._name;
    }

    // URL transfromations
    public string ToAbsolute(string url)
    {
      if (string.IsNullOrEmpty(_engine.info._svnRoot)) return url;
      try
      {
        Uri test = new Uri(url, UriKind.RelativeOrAbsolute);
        if (test.IsAbsoluteUri) return url;
        return _engine.info._svnRoot + url;
      }
      catch (Exception)
      {
        return url;
      }
    }
    public string ToRelative(string url)
    {
      if (string.IsNullOrEmpty(_engine.info._svnRoot)) return url;

      if (url.StartsWith(_engine.info._svnRoot))
        return url.Substring(_engine.info._svnRoot.Length);

      return url;
    }
 
    // find the tags with given prefix
    public void FindTags(string prefix, ref SortedList<string, int> result)
    {
      // binary searching in the sorted list
      int first = 0, last = _tags.Keys.Count - 1;
      while (first <= last)
      {
        int mid = (first + last) / 2;  // compute mid point.
        if (_tags.Keys[mid].StartsWith(prefix))
        {
          // some item found, now search the whole range (inside first .. last)

          // check the range first ... mid - 1
          int minIndex = first;
          for (int i = mid - 1; i >= first; i--)
          {
            if (!_tags.Keys[i].StartsWith(prefix))
            {
              minIndex = i + 1;
              break;
            }
          }
          // check the range mid + 1 ... last
          int maxIndex = last;
          for (int i = mid + 1; i <= last; i++)
          {
            if (!_tags.Keys[i].StartsWith(prefix))
            {
              maxIndex = i - 1;
              break;
            }
          }

          // copy entries to the result
          for (int i = minIndex; i <= maxIndex; i++)
          {
            int index = result.IndexOfKey(_tags.Keys[i]);
            if (index >= 0)
            {
              int total = result.Values[index] + _tags.Values[i]._count;
              result[_tags.Keys[i]] = total;
            }
            else result.Add(_tags.Keys[i], _tags.Values[i]._count);
          }
          return;
        }
        if (prefix.CompareTo(_tags.Keys[mid]) > 0)
          first = mid + 1;  // repeat search in top half.
        else
          last = mid - 1; // repeat search in bottom half.
      }
      // none found
    }

    // Return true, if given tag exists
    public bool TagExists(string name)
    {
      return _tags.Keys.Contains(name);
    }

    // access to user properties
    public string AliasFindUser(string alias)
    {
      string user;
      if (_aliases.TryGetValue(alias, out user)) return user;
      return alias; // this is not an alias
    }
    public UserInfo GetUser(string id)
    {
      UserInfo info;
      if (_users.TryGetValue(id, out info)) return info;
      return null;
    }
    public UserInfo AliasGetUser(string alias)
    {
      return GetUser(AliasFindUser(alias));
    }


    public string UserDisplayName(ref UserInfo info, string alias)
    {
      string author;
      if (info != null && !string.IsNullOrEmpty(info._fullName))
      {
        author = string.Format("{0} ({1})", alias, info._fullName);
      }
      else author = alias;
      return author;
    }
    public string UserDisplayName(string alias)
    {
      UserInfo info = GetUser(AliasFindUser(alias));
      return UserDisplayName(ref info, alias);
    }

    //public delegate void NotifyUserChange(DBEngine.UserList users);
    public delegate void NotifyUserChange(SQLDatabase db, DBEngine.UserList users);
    public delegate void NotifyChat(SQLDatabase db, ChatConversation.ChatData data);

    public DBEngine.NotifyUserChange NotifyUserChangeWrap(NotifyUserChange notify)
    {
      return delegate(DBEngine.UserList users) { notify(this, users); };
    }
    public DBEngine.NotifyChat NotifyChatWrap(NotifyChat notify)
    {
      return delegate(ChatConversation.ChatData data) { notify(this, data); };
    }

    // Initialize the connection to the database
    public SQLDatabase(DBInfo info, Configuration.LDAPProfile ldapProfile, NotifyUserChange notifyUserChange, NotifyChat notifyChat)
    {
      _engine = CreateEngine(info);

      _cache = new ArticlesCache();
      _users = new Dictionary<string, UserInfo>();
      _aliases = new Dictionary<string, string>();
      _groups = new List<string>();
      _accessRights = new List<Token>();
      _tags = new SortedList<string, TagInfo>();
      _homepage = 0;
      _bookmarks = new List<Bookmark>();
      _scripts = new List<Script>();
      _directories = new List<RepositoryDirectories>();

      // use LDAP if user profile was found, the uid matches and the database was accessed through LDAP
      bool useLDAP = ldapProfile != null && !string.IsNullOrEmpty(info._ldapDN) && (ldapProfile._uid == info._user);

      // the list of users
      _engine.LoadUsers(ref _users, ref _aliases, NotifyUserChangeWrap(notifyUserChange), NotifyChatWrap(notifyChat));
      if (useLDAP) LDAPLoadUser(ldapProfile);

      // the properties of the current user
      if (useLDAP) LDAPLoadUserSettings();
      else _engine.LoadUserSettings(ref _groups, ref _accessRights, ref _homepage);

      _engine.LoadBookmarks(ref _bookmarks, this);
      _engine.LoadScripts(ref _scripts);
    }

    private static DBEngine CreateEngine(DBInfo info)
    {
      switch (info._serverType)
      {
        case DBServerType.STWebServices:
          return new WebServicesEngine(info);
        case DBServerType.STMySQL:
        case DBServerType.STSQLServer:
        default:
          return new SQLEngine(info);
        case DBServerType.STNewsServer:
          return new NNTPEngine(info);
      }
    }
    public void OnClose()
    {
      _engine.OnClose();
    }

    public ArticlesStore GetArticlesStore()
    {
      return _engine.GetArticlesStore();
    }
    public bool IsEmptyPathEnabled()
    {
      return _engine.IsEmptyPathEnabled();
    }

    // Read the user info from given database
    public static DBUserInfo LoadUserInfo(DBInfo info)
    {
      // Create a temporary engine
      DBEngine engine = CreateEngine(info);
      return engine.LoadUserInfo();
    }

    public void SaveUserInfo(DBUserInfo userInfo)
    {
      _engine.SaveUserInfo(userInfo);
    }

    // patch the database user profile by the values from LDAP
    private void LDAPLoadUser(Configuration.LDAPProfile ldapProfile)
    {
      UserInfo user = GetUser(ldapProfile._uid);
      if (user != null)
      {
        bool changed = false;
        if (ldapProfile._name != null && ldapProfile._name != user._fullName)
        {
          user._fullName = ldapProfile._name;
          changed = true;
        }
        if (ldapProfile._picture != null && ldapProfile._picture != user._picture)
        {
          user._picture = ldapProfile._picture;
          changed = true;
        }
        if (ldapProfile._colorSet && ldapProfile._color != user._color)
        {
          user._color = ldapProfile._color;
          changed = true;
        }
        if (changed) _engine.SaveUser(user);
      }
      else
      {
        FormColabo.Log(string.Format("User {0} ({1}) not found", ldapProfile._uid, ldapProfile._name));
      }
    }
    // load the user settings from the LDAP, except the user defined home page (this can change often and so not stored to the LDAP)
    private void LDAPLoadUserSettings()
    {
      FormColabo.Log(string.Format("User settings for {0}:", name));
      // groups, defaults
      foreach (DBInfo.LDAPGroupInfo group in _engine.info._groups)
      {
        if (!string.IsNullOrEmpty(group._name)) _groups.Add(group._name);
        FormColabo.Log(string.Format(" - member of {0}", group._name));
        if (!string.IsNullOrEmpty(group._accessRights))
        {
          List<Token> set = FormColabo.CompileRuleSet(group._accessRights);
          if (set != null) _accessRights.AddRange(set);
        }
        if (group._homePage > 0) _homepage = group._homePage;
      }

      // user
      if (!string.IsNullOrEmpty(_engine.info._accessRights))
      {
        List<Token> set = FormColabo.CompileRuleSet(_engine.info._accessRights);
        if (set != null) _accessRights.AddRange(set);
      }

      int homepage = _engine.LoadHomepage();
      if (homepage > 0) _homepage = homepage;
    }

    // Read the rule sets from the database
    public void LoadRuleSets(ref List<RuleSet> ruleSets)
    {
      ruleSets.Clear();
      _engine.LoadRuleSets(ref ruleSets);
    }
    // Store the new rule sets to the database
    public void SaveRuleSets(ListBox.ObjectCollection ruleSets)
    {
      _engine.SaveRuleSets(ruleSets);
    }

    public void SaveScripts(List<Script> scripts)
    {
      _engine.SaveScripts(scripts);
    }

    // Read the content of the database to the cache
    public void ReadDBContent(ref ArticlesCache cache, System.ComponentModel.BackgroundWorker worker)
    {
      _engine.LoadArticles(ref cache, worker);

      LoadDrafts(ref cache);
      LoadChats(ref cache);
      cache.UpdateChilds();
    }
    private void LoadDrafts(ref ArticlesCache cache)
    {
      // read article drafts
      string dir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
      if (!string.IsNullOrEmpty(dir))
      {
        dir += "\\Colabo\\Draft";
        if (Directory.Exists(dir)) foreach (string path in Directory.GetFiles(dir))
          {
            // read the article draft from the file
            DocXML cls = DocXML.Load(path,"Article");
            if (cls==null) continue;

            // check the database
            string srv = cls.GetAttribute("server");
            string db = cls.GetAttribute("database");
            if (!srv.Equals(server, StringComparison.OrdinalIgnoreCase) || !db.Equals(database, StringComparison.OrdinalIgnoreCase)) continue;

            LoadDraftItem(cache, path, cls);
          }
      }
    }

    private static void LoadDraftItem(ArticlesCache cache, string path, DocXML cls)
    {
      // find / create the article
      ArticleItem item = null;
      int id = -1;
      int.TryParse(cls.GetAttribute("id"), out id);
      if (id >= 0)
        cache.TryGetValue(id, out item);
      else
        id = cache.UniqueTempId();
      if (item == null)
      {
        item = new ArticleItem();
        item._id = id;
        cache.Add(id, item);
      }
      else if (!string.IsNullOrEmpty(item._draftLocal))
      {
        // two drafts exist for this article, select the later one
        if (File.GetLastWriteTime(item._draftLocal) > File.GetLastWriteTime(path)) return;
      }
      item._draftLocal = path;

      LoadArticle(cls, item);
    }

    private static void LoadArticle(StructuredDoc cls, ArticleItem item)
    {
      // read the article
      int.TryParse(cls.GetAttribute("parent"), out item._parent);
      item._title = cls.GetAttribute("title");
      item.RawURL = cls.GetAttribute("url");
      item._title = cls.GetAttribute("title");
      item._author = cls.GetAttribute("author");

      int.TryParse(cls.GetAttribute("chatId"), out item._chatId);

      string userList = cls.GetAttribute("chatUserList");
      if (!string.IsNullOrEmpty(userList))
      {
        string[] users = userList.Split(',');
        item._chatUsers = new List<string>(users);
      }


      DateTime.TryParse(cls.GetAttribute("date"), out item._date);
      string tags = cls.GetAttribute("tags");
      ArticleItem.ReadTags(tags, out item._ownTags);
      string toRead = cls.GetAttribute("toRead");
      if (string.IsNullOrEmpty(toRead))
      {
        bool read = false;
        bool.TryParse(cls.GetAttribute("read"), out read);
        item._toRead = !read;
      }
      else bool.TryParse(toRead, out item._toRead);
      string changed = cls.GetAttribute("changed");
      if (string.IsNullOrEmpty(changed))
      {
        bool read = false;
        bool.TryParse(cls.GetAttribute("read"), out read);
        item._changed = !read;
      }
      else bool.TryParse(changed, out item._changed);
      bool.TryParse(cls.GetAttribute("expanded"), out item._expanded);
    }

    private void LoadChats(ref ArticlesCache cache)
    {
      // enumerate all chats on the server (all databases)
      //_engine.LoadChatConversation();
      List<int> chats = _engine.LoadChats();
      if (chats != null)
      {
        foreach (int chat in chats)
        {
          var item = new ArticleItem();

          if (_engine.LoadChatHeader(chat, item))
          {
            item._id = cache.UniqueTempId(); // unique negative id
            item._chatId = chat;
            cache.Add(item._id, item);
          }
        }
      }

    }
 
    // save the content of _cache to the file
    public bool SaveCache()
    {
      string filename = string.Format("{0}\\Colabo\\{1}+{2}.cache",
        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
        Uri.EscapeDataString(server), database);

      try
      {
        using (FileStream stream = new FileStream(filename, FileMode.Create))
        {
          int version = 1;
          BinaryWriter writer = new BinaryWriter(stream);
          writer.Write(version);
          _cache.Save(writer, version);
        }
      }
      catch (System.Exception e)
      {
        Console.WriteLine(e.ToString());
        return false;
      }
      return true;
    }

    // load the content of _cache from the file
    public bool LoadCache()
    {
      string filename = string.Format("{0}\\Colabo\\{1}+{2}.cache",
        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
        Uri.EscapeDataString(server), database);

      try
      {
        using (FileStream stream = new FileStream(filename, FileMode.Open))
        {
          BinaryReader reader = new BinaryReader(stream);
          int version = reader.ReadInt32();
          _cache.Load(reader, version);
        }
      }
      catch (System.Exception e)
      {
        Console.WriteLine(e.ToString());
        return false;
      }

      LoadDrafts(ref _cache);
      LoadChats(ref _cache);
      _cache.UpdateChilds();

      return true;
    }

    public void ReplaceCache(ArticlesCache cache, List<RepositoryDirectories> directories)
    {
      FormColabo.Log(string.Format("Database {0} - {1} articles loaded", name, cache.Count));

      _directories = directories;

      // keep cache info not stored in database
      foreach (ArticleItem item in _cache.Values)
      {
        ArticleItem newItem;
        if (cache.TryGetValue(item._id, out newItem))
        {
          // if URL did not change, working copy did not change as well
          if (newItem.RawURL == item.RawURL) newItem._workingCopy = item._workingCopy;
        }
      }

      _cache = cache;
      // build the list of tags
      _tags.Clear();
      foreach (ArticleItem item in cache.Values)
      {
        if (item._effectiveTags != null)
        {
          foreach (string tag in item._effectiveTags)
          {
            TagInfo info = null;
            if (_tags.TryGetValue(tag, out info)) info._count++;
            else _tags.Add(tag, new TagInfo());
          }
        }
      }
    }

    public RepositoryDirectories FindRepository(string url)
    {
      return _directories.Find(delegate(RepositoryDirectories item)
      {
        return url.StartsWith(item._root);
      });
    }
    public void CollectDirectories(ArticlesCache cache, ref List<RepositoryDirectories> directories)
    {
      // discover SVN repositories and their directory structure
      foreach (ArticleItem item in cache.Values)
      {
        ArticleItem.VTCache vtc = new ArticleItem.VTCache(this, item);
        if ((item.GetAccessRights(this, vtc) & AccessRights.Read) != 0)
        {
          // register the path to the directory structure
          RegisterURL(item.AbsoluteURL(this), ref directories);
        }
      }
    }
    private void RegisterURL(string url, ref List<RepositoryDirectories> directories)
    {
      // find repository url belong to
      RepositoryDirectories dirs = directories.Find(delegate(RepositoryDirectories item)
      {
        return url.StartsWith(item._root);
      });
      if (dirs == null)
      {
        Uri uri = null;
        try
        {
          uri = new Uri(url);
        }
        catch (Exception)
        {
        }
        if (uri == null) return; // wrong url
        if (uri.Scheme == "news") return; // not a SVN

        string root, uuid;
        ArticlesStore svn = GetArticlesStore();
        if (!svn.GetRepositoryInfo(uri, out root, out uuid, true))
        {
          // Console.WriteLine("Repository not found for " + url);
          return; // SVN repository not found
        }

        // Console.WriteLine("Repository found: " + root);
        dirs = new RepositoryDirectories(root, uuid);
        directories.Add(dirs);
      }

      // copy the directory structure
      string subdir = url.Substring(dirs._root.Length);
      List<DirectoryInfo> list = dirs._directories;
      int index;
      while ((index = subdir.IndexOf('/')) >= 0)
      {
        string dir = subdir.Substring(0, index);
        subdir = subdir.Substring(index + 1);
        DirectoryInfo info = list.Find(delegate(DirectoryInfo i)
        {
          return i._name.Equals(dir, StringComparison.OrdinalIgnoreCase);
        });
        if (info == null)
        {
          info = new DirectoryInfo(dir);
          list.Add(info);
        }
        list = info._directories;
      }
    }

    // Check what article revision was read already
    public int GetReadRevision(int id)
    {
      System.Diagnostics.Trace.Assert(id >= 0);
      return _engine.LoadArticleRead(id);
    }
    // Store the read flag to the database
    public void SaveArticleRead(int id, int revision)
    {
      System.Diagnostics.Trace.Assert(id >= 0);
      _engine.SaveArticleRead(id, revision);
    }
    // Remove the read flag from the database
    public void SaveArticleUnread(int id)
    {
      System.Diagnostics.Trace.Assert(id >= 0);
      _engine.DeleteArticleRead(id);
    }

    // Remove the expanded flag from the database
    public void SaveArticleCollapsed(int id)
    {
      System.Diagnostics.Trace.Assert(id >= 0);
      _engine.SaveArticleCollapsed(id);
    }
    // Store the expanded flag to the database
    public void SaveArticleExpanded(int id)
    {
      System.Diagnostics.Trace.Assert(id >= 0);
      _engine.DeleteArticleCollapsed(id);
    }

    // Add article header to the database, return the id of the article
    public int SendArticle(ArticleItem item)
    {
      return _engine.SaveArticle(item);
    }

    // update the revision of the article in the database
    public void UpdateRevisionAndDate(ArticleItem item)
    {
      System.Diagnostics.Trace.Assert(item._id >= 0);
      _engine.SaveRevisionAndDate(item);
    }

    // change the parent of the article
    public void ChangeParent(ArticleItem item, int parent)
    {
      System.Diagnostics.Trace.Assert(item._id >= 0);
      _engine.SaveParent(item._id, parent);
    }

    // Delete the article from the database
    public void DeleteArticle(ArticleItem item)
    {
      System.Diagnostics.Trace.Assert(item._id >= 0);
      _engine.DeleteArticle(item);
    }

    public void ReplaceTag(int id, string addTag, string removeTag)
    {
      System.Diagnostics.Trace.Assert(id >= 0);
      _engine.UpdateTag(id, addTag, removeTag);
    }
/*
    public void StoreTagsToDB(ArticlesCache cache)
    {
      _engine.SaveTags(cache);
    }
*/
    
    // update the article header in the draft
    public void SaveArticleHeader(ArticleItem item)
    {
      if (string.IsNullOrEmpty(item._draftLocal)) return;
      if (!File.Exists(item._draftLocal)) return;

      // update header only, do not touch content
      XmlDocument file = new XmlDocument();
      file.Load(item._draftLocal);
      XmlElement cls = (XmlElement)file.SelectSingleNode("Article");
      if (cls != null)
      {
        cls.SetAttribute("server", server);
        cls.SetAttribute("database", database);
        cls.SetAttribute("id", item._id.ToString());
        cls.SetAttribute("parent", item._parent.ToString());
        cls.SetAttribute("title", item._title);
        cls.SetAttribute("url", item.RawURL);
        cls.SetAttribute("author", item._author);
        cls.SetAttribute("date", item._date.ToString());
        cls.SetAttribute("tags", ArticleItem.WriteTags(item._ownTags));
        cls.SetAttribute("toRead", item._toRead.ToString());
        cls.SetAttribute("changed", item._changed.ToString());
        cls.SetAttribute("expanded", item._expanded.ToString());
      }
      else
      {
        Trace.Fail("Article draft should already be saved");
      }

      file.Save(item._draftLocal);
    }

    public int LastChangeRevision(int id)
    {
      return _engine.LoadRevision(id);
    }

    public void AddBookmark(Bookmark bookmark)
    {
      // store to the database
      bookmark._id = _engine.SaveBookmark(bookmark, this);

      if (bookmark._id >= 0)
      {
        // update the local list
        _bookmarks.Add(bookmark);
      }
    }
    public void UpdateBookmark(Bookmark bookmark)
    {
      _engine.UpdateBookmark(bookmark, this);
    }
    public void DeleteBookmark(int id)
    {
      // remove from the local list
      int index = _bookmarks.FindIndex(delegate(Bookmark bookmark)
      {
        return bookmark._id == id;
      });
      int parent = -1;
      if (index >= 0)
      {
        parent = _bookmarks[index]._parent;
        _bookmarks.RemoveAt(index);
      }
      // update the children to not point to this bookmark
      foreach (Bookmark b in _bookmarks)
      {
        if (b._parent == id) b._parent = parent;
      }

      // remove from the database
      _engine.DeleteBookmark(id, parent);
    }
    public bool IsAncestor(Bookmark a, Bookmark b)
    {
      if (a._id == b._id) return true;
      
      int parent = b._parent;
      while (parent >= 0)
      {
        if (a._id == parent) return true;
        Bookmark c = _bookmarks.Find(delegate(Bookmark bookmark)
        {
          return bookmark._id == parent;
        });
        if (c == null) return false; // invalid parent
        parent = c._parent;
      }
      return false;
    }

    public string GetArticleLink(ArticleId id)
    {
      if (id._database != this) return "";
      if (id._id < 0) return "";

      ArticleItem item;
      if (!_cache.TryGetValue(id._id, out item)) return "";
      return item.GetLink(this, true);
    }

    public string GetArticleRevision(int id, int revision)
    {
      return _engine.GetArticleRevision(id, revision);
    }

    public bool IsFullTextIndexLocal()
    {
      return _engine.IsFullTextIndexLocal();
    }
    public string CheckFullTextIndex()
    {
      return _engine.CheckFullTextIndex();
    }
    public List<int> FullTextRevisionsOfArticle(ArticleItem item)
    {
      return _engine.FullTextRevisionsOfArticle(item);
    }
    public bool FullTextNeedToIndexArticle(ArticleItem item, RevisionInfo revision)
    {
      return _engine.FullTextNeedToIndexArticle(item, revision);
    }
    public void FullTextIndexArticle(ArticleItem item, RevisionInfo revision, string content)
    {
      _engine.FullTextIndexArticle(item, revision, content);
    }
    public List<SearchResult> FullTextSearch(string query)
    {
      return _engine.FullTextSearch(this, query);
    }

    public string Benchmark()
    {
      Stopwatch watch = Stopwatch.StartNew();

      // find the revision of some article
      double queryDuration = _engine.Benchmark();
      if (queryDuration < 0) return ""; // failed

      TimeSpan diff = watch.Elapsed;
      return string.Format("  SQL query to {0}: {1} s, internal {2} s \n", name, diff.TotalSeconds, queryDuration);
    }

    // mirror the info about users and groups from LDAP to SQL
    public static void SaveLDAPUsers(string host, int port, string dbDN, string user, string password)
    {
      LdapConnection connection = Configuration.OpenLDAPConnection(host, port, user, password);
      if (connection == null) return;

      DBInfo dbInfo = new DBInfo();
      dbInfo._user = ""; // will be read from LDAP
      dbInfo._password = password;

      List<ManageUserInfo> users = new List<ManageUserInfo>();
      List<ManageGroupInfo> groups = new List<ManageGroupInfo>();
      List<ManageGroupUserInfo> groupUsers = new List<ManageGroupUserInfo>();

      try
      {
        // read user info (to find the user name)
        SearchRequest request = new SearchRequest(user, "(objectClass=*)", SearchScope.Base, new String[] {"uid"});
        SearchResponse response = (SearchResponse)connection.SendRequest(request);

        if (response.Entries.Count > 0)
        {
          SearchResultEntry entry = response.Entries[0];
          // read the login name from the user profile
          DirectoryAttribute attributes = entry.Attributes["uid"];
          if (attributes != null && attributes.Count == 1) dbInfo._user = attributes[0].ToString();
        }

        // read common database settings
        request = new SearchRequest(dbDN, "(objectClass=*)", SearchScope.Base,
          new String[] { "colaboDBServerType", "colaboDBServer", "colaboDBDatabase" });
        response = (SearchResponse)connection.SendRequest(request);

        if (response.Entries.Count > 0)
        {
          SearchResultEntry entry = response.Entries[0];

          DirectoryAttribute attributes = entry.Attributes["colaboDBServerType"];
          if (attributes != null && attributes.Count == 1)
          {
            string value = attributes[0].ToString();
            if (Enum.IsDefined(typeof(DBServerType), value))
              dbInfo._serverType = (DBServerType)Enum.Parse(typeof(DBServerType), value, true);
          }
          attributes = entry.Attributes["colaboDBServer"];
          if (attributes != null && attributes.Count == 1) dbInfo._server = attributes[0].ToString();
          attributes = entry.Attributes["colaboDBDatabase"];
          if (attributes != null && attributes.Count == 1) dbInfo._database = attributes[0].ToString();
        }

        // Collect info about users and groups to internal structures

        // Users
        request = new SearchRequest("ou=users," + dbDN, "(objectClass=colaboUser)", SearchScope.Subtree,
          new String[] { "cn", "colaboAccessRights", "colaboUserDN" });
        response = (SearchResponse)connection.SendRequest(request);

        foreach (SearchResultEntry entry in response.Entries)
        {
          DirectoryAttribute attributes = entry.Attributes["cn"];
          if (attributes == null || attributes.Count != 1) continue;
          ManageUserInfo info = new ManageUserInfo();
          info._id = attributes[0].ToString();

          attributes = entry.Attributes["colaboAccessRights"];
          if (attributes != null && attributes.Count == 1) info._access = attributes[0].ToString();

          attributes = entry.Attributes["colaboUserDN"];
          if (attributes != null && attributes.Count == 1)
          {
            // copy password from the main user entry
            string userDN = attributes[0].ToString();
            SearchRequest userRequest = new SearchRequest(userDN, "(objectClass=*)", SearchScope.Base, new String[] { "userPassword" });
            SearchResponse userResponse = (SearchResponse)connection.SendRequest(userRequest);

            if (userResponse.Entries.Count > 0)
            {
              SearchResultEntry userEntry = userResponse.Entries[0];
              // read the login name from the user profile
              DirectoryAttribute userAttributes = userEntry.Attributes["userPassword"];
              if (userAttributes != null && userAttributes.Count == 1) info._password = userAttributes[0].ToString();
            }
          }

          users.Add(info);
        }

        // Groups
        request = new SearchRequest("ou=groups," + dbDN, "(objectClass=colaboGroup)", SearchScope.Subtree,
          new String[] { "cn", "description", "colaboAccessRights", "colaboGroupLevel", "colaboHomePage", "memberUid", "member"});
        response = (SearchResponse)connection.SendRequest(request);

        foreach (SearchResultEntry entry in response.Entries)
        {
          DirectoryAttribute attributes = entry.Attributes["cn"];
          if (attributes == null || attributes.Count != 1) continue;
          ManageGroupInfo info = new ManageGroupInfo();
          info._id = attributes[0].ToString();

          attributes = entry.Attributes["description"];
          if (attributes != null && attributes.Count == 1) info._name = attributes[0].ToString();
          attributes = entry.Attributes["colaboAccessRights"];
          if (attributes != null && attributes.Count == 1) info._access = attributes[0].ToString();
          attributes = entry.Attributes["colaboGroupLevel"];
          if (attributes != null && attributes.Count == 1) int.TryParse(attributes[0].ToString(), out info._level);
          attributes = entry.Attributes["colaboHomePage"];
          if (attributes != null && attributes.Count == 1) int.TryParse(attributes[0].ToString(), out info._home);

          groups.Add(info);

          attributes = entry.Attributes["memberUid"];
          if (attributes != null)
          {
            for (int i = 0; i < attributes.Count; i++)
            {
              string userId = attributes[i].ToString();
              if (!string.IsNullOrEmpty(userId)) groupUsers.Add(new ManageGroupUserInfo(info._id, userId));
            }
          }

          attributes = entry.Attributes["member"];
          if (attributes != null)
          {
            for (int i = 0; i < attributes.Count; i++)
            {
              string member = attributes[i].ToString();
              CollectMembers(connection, member, info._id, ref users, ref groupUsers);
            }
          }
        }
      }
      catch (System.Exception e)
      {
        // LDAP operation failed, keep settings from the config file
        Console.WriteLine(e.ToString());
      }

      // create a temporary engine
      DBEngine engine = CreateEngine(dbInfo);
      engine.SaveUsers(users, groups, groupUsers);
    }
    private static void CollectMembers(LdapConnection connection, string dn, string groupId,
      ref List<ManageUserInfo> users, ref List<ManageGroupUserInfo> groupUsers)
    {
      // access the user / group entry with the given dn
      SearchResponse response = null;
      try
      {
        SearchRequest request = new SearchRequest(dn, "(objectClass=*)", SearchScope.Base, new String[] { "uid", "userPassword", "member" });
        response = (SearchResponse)connection.SendRequest(request);
      }
      catch (DirectoryException)
      {
        // the request cannot be executed (missing rights probably)
        return;
      }
      if (response.Entries.Count > 0)
      {
        SearchResultEntry entry = response.Entries[0];
        // check if this is user entry with given uid
        DirectoryAttribute attributes = entry.Attributes["uid"];
        if (attributes != null && attributes.Count == 1)
        {
          string userId = attributes[0].ToString();
          // add to the list of users if not present already
          if (users.Find(delegate(ManageUserInfo info)
          {
            return info._id.Equals(userId);
          }) == null)
          {
            ManageUserInfo info = new ManageUserInfo();
            info._id = userId;
            attributes = entry.Attributes["userPassword"];
            if (attributes != null && attributes.Count == 1) info._password = attributes[0].ToString();
            users.Add(info);
          }
          // add to the list of group members if not present already
          if (groupUsers.Find(delegate(ManageGroupUserInfo info)
          {
            return info._group.Equals(groupId) && info._user.Equals(userId);
          }) == null)
          {
            groupUsers.Add(new ManageGroupUserInfo(groupId, userId));
          }
        }
        // check members of group entry recursively
        attributes = entry.Attributes["member"];
        if (attributes != null)
        {
          for (int i = 0; i < attributes.Count; i++)
          {
            string member = attributes[i].ToString();
            CollectMembers(connection, member, groupId, ref users, ref groupUsers);
          }
        }
      }
    }

    public void UpdateUserPresence(Dictionary<string, UserInfo> userList)
    {
      lock (_users)
      {
        foreach (var user in userList)
        {
          UserInfo uInfo;
          if (_users.TryGetValue(user.Key, out uInfo))
          {
            uInfo._presence = user.Value._presence;
            uInfo._presenceTime = user.Value._presenceTime;
            uInfo._location = user.Value._location;
            uInfo._ipAddr = user.Value._ipAddr;
            uInfo._build = user.Value._build;
            _users[user.Key] = uInfo;
          }
        }
      }

    }

    internal void ChangePresenceState(string state)
    {
      _engine.ChangePresenceState(state);
    }

    internal void SetLocationName(string ipAddr, string location) { _engine.SetLocationName(ipAddr, location); }
    internal int StartChat(List<string> users) { return _engine.StartChat(user, users); }
    internal bool LeaveChat(int p) { return _engine.LeaveChat(p); }
    internal void DeleteChat(int p) { _engine.DeleteChat(p); }
    internal int SendChatConversation(int id, string jsonChat) { return _engine.SendChatConversation(id, jsonChat); }
    internal ChatConversation LoadChatConversation(int id, bool active) { return _engine.LoadChatConversation(id, active); }
    internal bool LoadChatHeader(int chat, ArticleItem item) { return _engine.LoadChatHeader(chat, item); }
    internal void ReportTyping(int chat, bool typing){_engine.ReportTyping(chat,typing);}
  }

  public class ManageUserInfo
  {
    public string _id;
    public string _access;
    public string _password;

    public ManageUserInfo()
    {
      _id = ""; _access = ""; _password = "";
    }
  };
  public class ManageGroupInfo
  {
    public string _id;
    public string _name;
    public string _access;
    public int _level;
    public int _home;

    public ManageGroupInfo()
    {
      _id = ""; _name = ""; _access = "";
      _level = 0; _home = 0;
    }
  };
  public class ManageGroupUserInfo
  {
    public string _group;
    public string _user;

    public ManageGroupUserInfo(string group, string user)
    {
      _group = group; _user = user;
    }
  };

  // Discovered directory hierarchy, used when access is limited to some part (for example root) of SVN repository
  public class RepositoryDirectories
  {
    public string _root;
    public string _uuid;
    public List<DirectoryInfo> _directories;

    public RepositoryDirectories(string root, string uuid)
    {
      _root = root;
      _uuid = uuid;
      _directories = new List<DirectoryInfo>();
    }
  }
  // element of directory hierarchy used in RepositoryDirectories
  public class DirectoryInfo
  {
    public string _name;
    public List<DirectoryInfo> _directories;

    public DirectoryInfo(string name)
    {
      _name = name;
      _directories = new List<DirectoryInfo>();
    }
  }
}
