namespace Colabo
{
  partial class DialogOptions
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.markAsRead = new System.Windows.Forms.CheckBox();
      this.markAsReadTime = new System.Windows.Forms.NumericUpDown();
      this.buttonOk = new System.Windows.Forms.Button();
      this.buttonCancel = new System.Windows.Forms.Button();
      this.synchronize = new System.Windows.Forms.CheckBox();
      this.synchronizeInterval = new System.Windows.Forms.NumericUpDown();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.awayAfterInterval = new System.Windows.Forms.NumericUpDown();
      this.label3 = new System.Windows.Forms.Label();
      this.Presence = new System.Windows.Forms.GroupBox();
      this.Articles = new System.Windows.Forms.GroupBox();
      ((System.ComponentModel.ISupportInitialize)(this.markAsReadTime)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.synchronizeInterval)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.awayAfterInterval)).BeginInit();
      this.Presence.SuspendLayout();
      this.Articles.SuspendLayout();
      this.SuspendLayout();
      // 
      // markAsRead
      // 
      this.markAsRead.AutoSize = true;
      this.markAsRead.Location = new System.Drawing.Point(6, 19);
      this.markAsRead.Name = "markAsRead";
      this.markAsRead.Size = new System.Drawing.Size(179, 17);
      this.markAsRead.TabIndex = 0;
      this.markAsRead.Text = "Mark as read after specified time";
      this.markAsRead.UseVisualStyleBackColor = true;
      // 
      // markAsReadTime
      // 
      this.markAsReadTime.Location = new System.Drawing.Point(203, 19);
      this.markAsReadTime.Name = "markAsReadTime";
      this.markAsReadTime.Size = new System.Drawing.Size(50, 20);
      this.markAsReadTime.TabIndex = 2;
      // 
      // buttonOk
      // 
      this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.buttonOk.Location = new System.Drawing.Point(241, 166);
      this.buttonOk.Name = "buttonOk";
      this.buttonOk.Size = new System.Drawing.Size(75, 23);
      this.buttonOk.TabIndex = 3;
      this.buttonOk.Text = "Ok";
      this.buttonOk.UseVisualStyleBackColor = true;
      this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
      // 
      // buttonCancel
      // 
      this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.buttonCancel.Location = new System.Drawing.Point(160, 166);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.Size = new System.Drawing.Size(75, 23);
      this.buttonCancel.TabIndex = 4;
      this.buttonCancel.Text = "Cancel";
      this.buttonCancel.UseVisualStyleBackColor = true;
      this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
      // 
      // synchronize
      // 
      this.synchronize.AutoSize = true;
      this.synchronize.Location = new System.Drawing.Point(6, 51);
      this.synchronize.Name = "synchronize";
      this.synchronize.Size = new System.Drawing.Size(114, 17);
      this.synchronize.TabIndex = 5;
      this.synchronize.Text = "Synchronize each ";
      this.synchronize.UseVisualStyleBackColor = true;
      // 
      // synchronizeInterval
      // 
      this.synchronizeInterval.Location = new System.Drawing.Point(127, 51);
      this.synchronizeInterval.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
      this.synchronizeInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.synchronizeInterval.Name = "synchronizeInterval";
      this.synchronizeInterval.Size = new System.Drawing.Size(58, 20);
      this.synchronizeInterval.TabIndex = 6;
      this.synchronizeInterval.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.synchronizeInterval.ValueChanged += new System.EventHandler(this.synchronizeInterval_ValueChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(192, 51);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(43, 13);
      this.label1.TabIndex = 7;
      this.label1.Text = "minutes";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(234, 16);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(43, 13);
      this.label2.TabIndex = 9;
      this.label2.Text = "minutes";
      // 
      // awayAfterInterval
      // 
      this.awayAfterInterval.Location = new System.Drawing.Point(167, 16);
      this.awayAfterInterval.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
      this.awayAfterInterval.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.awayAfterInterval.Name = "awayAfterInterval";
      this.awayAfterInterval.Size = new System.Drawing.Size(58, 20);
      this.awayAfterInterval.TabIndex = 8;
      this.awayAfterInterval.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(6, 16);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(155, 13);
      this.label3.TabIndex = 10;
      this.label3.Text = "Show as away after inactive for";
      this.label3.Click += new System.EventHandler(this.label3_Click);
      // 
      // Presence
      // 
      this.Presence.Controls.Add(this.label3);
      this.Presence.Controls.Add(this.awayAfterInterval);
      this.Presence.Controls.Add(this.label2);
      this.Presence.Location = new System.Drawing.Point(12, 106);
      this.Presence.Name = "Presence";
      this.Presence.Size = new System.Drawing.Size(304, 54);
      this.Presence.TabIndex = 14;
      this.Presence.TabStop = false;
      this.Presence.Text = "Presence";
      // 
      // Articles
      // 
      this.Articles.Controls.Add(this.synchronize);
      this.Articles.Controls.Add(this.synchronizeInterval);
      this.Articles.Controls.Add(this.label1);
      this.Articles.Controls.Add(this.markAsRead);
      this.Articles.Controls.Add(this.markAsReadTime);
      this.Articles.Location = new System.Drawing.Point(12, 15);
      this.Articles.Name = "Articles";
      this.Articles.Size = new System.Drawing.Size(304, 75);
      this.Articles.TabIndex = 15;
      this.Articles.TabStop = false;
      this.Articles.Text = "Articles";
      // 
      // DialogOptions
      // 
      this.AcceptButton = this.buttonOk;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.buttonCancel;
      this.ClientSize = new System.Drawing.Size(326, 209);
      this.Controls.Add(this.Articles);
      this.Controls.Add(this.Presence);
      this.Controls.Add(this.buttonCancel);
      this.Controls.Add(this.buttonOk);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "DialogOptions";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Options";
      this.Load += new System.EventHandler(this.DialogOptions_Load);
      ((System.ComponentModel.ISupportInitialize)(this.markAsReadTime)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.synchronizeInterval)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.awayAfterInterval)).EndInit();
      this.Presence.ResumeLayout(false);
      this.Presence.PerformLayout();
      this.Articles.ResumeLayout(false);
      this.Articles.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.CheckBox markAsRead;
    private System.Windows.Forms.NumericUpDown markAsReadTime;
    private System.Windows.Forms.Button buttonOk;
    private System.Windows.Forms.Button buttonCancel;
    private System.Windows.Forms.CheckBox synchronize;
    private System.Windows.Forms.NumericUpDown synchronizeInterval;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.NumericUpDown awayAfterInterval;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.GroupBox Presence;
    private System.Windows.Forms.GroupBox Articles;
  }
}