﻿namespace Colabo
{
  partial class DialogSelectUser
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.chatUsers = new System.Windows.Forms.ListView();
      this.userColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.stateColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.lastSeenColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.locationColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.buttonOK = new System.Windows.Forms.Button();
      this.buttonCancel = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // chatUsers
      // 
      this.chatUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.chatUsers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.userColumn,
            this.stateColumn,
            this.lastSeenColumn,
            this.locationColumn});
      this.chatUsers.FullRowSelect = true;
      this.chatUsers.Location = new System.Drawing.Point(2, 1);
      this.chatUsers.Name = "chatUsers";
      this.chatUsers.Size = new System.Drawing.Size(382, 139);
      this.chatUsers.TabIndex = 2;
      this.chatUsers.UseCompatibleStateImageBehavior = false;
      this.chatUsers.View = System.Windows.Forms.View.Details;
      // 
      // userColumn
      // 
      this.userColumn.Text = "User";
      // 
      // stateColumn
      // 
      this.stateColumn.Text = "Activity";
      // 
      // lastSeenColumn
      // 
      this.lastSeenColumn.Text = "Last seen";
      this.lastSeenColumn.Width = 100;
      // 
      // locationColumn
      // 
      this.locationColumn.Text = "Location";
      this.locationColumn.Width = 140;
      // 
      // buttonOK
      // 
      this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonOK.Location = new System.Drawing.Point(218, 146);
      this.buttonOK.Name = "buttonOK";
      this.buttonOK.Size = new System.Drawing.Size(75, 23);
      this.buttonOK.TabIndex = 3;
      this.buttonOK.Text = "OK";
      this.buttonOK.UseVisualStyleBackColor = true;
      this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
      // 
      // buttonCancel
      // 
      this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.buttonCancel.Location = new System.Drawing.Point(311, 146);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.Size = new System.Drawing.Size(75, 23);
      this.buttonCancel.TabIndex = 4;
      this.buttonCancel.Text = "Cancel";
      this.buttonCancel.UseVisualStyleBackColor = true;
      this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
      // 
      // DialogSelectUser
      // 
      this.AcceptButton = this.buttonOK;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.buttonCancel;
      this.ClientSize = new System.Drawing.Size(396, 180);
      this.Controls.Add(this.buttonCancel);
      this.Controls.Add(this.buttonOK);
      this.Controls.Add(this.chatUsers);
      this.Name = "DialogSelectUser";
      this.Text = "Select User";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DialogSelectUser_FormClosed);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ListView chatUsers;
    private System.Windows.Forms.ColumnHeader userColumn;
    private System.Windows.Forms.ColumnHeader stateColumn;
    private System.Windows.Forms.ColumnHeader lastSeenColumn;
    private System.Windows.Forms.ColumnHeader locationColumn;
    private System.Windows.Forms.Button buttonOK;
    private System.Windows.Forms.Button buttonCancel;
  }
}