namespace Colabo
{
  partial class DialogSearchResults
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogSearchResults));
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.splitContainer2 = new System.Windows.Forms.SplitContainer();
      this.results = new System.Windows.Forms.DataGridView();
      this.Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Revision = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Author = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.preview = new System.Windows.Forms.WebBrowser();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.splitContainer2.Panel1.SuspendLayout();
      this.splitContainer2.Panel2.SuspendLayout();
      this.splitContainer2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.results)).BeginInit();
      this.SuspendLayout();
      // 
      // splitContainer1
      // 
      this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
      this.splitContainer1.Size = new System.Drawing.Size(857, 683);
      this.splitContainer1.SplitterDistance = 161;
      this.splitContainer1.TabIndex = 0;
      // 
      // splitContainer2
      // 
      this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer2.Location = new System.Drawing.Point(0, 0);
      this.splitContainer2.Name = "splitContainer2";
      this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer2.Panel1
      // 
      this.splitContainer2.Panel1.Controls.Add(this.results);
      // 
      // splitContainer2.Panel2
      // 
      this.splitContainer2.Panel2.Controls.Add(this.preview);
      this.splitContainer2.Size = new System.Drawing.Size(857, 518);
      this.splitContainer2.SplitterDistance = 280;
      this.splitContainer2.TabIndex = 1;
      // 
      // results
      // 
      this.results.AllowUserToAddRows = false;
      this.results.AllowUserToDeleteRows = false;
      this.results.AllowUserToOrderColumns = true;
      this.results.AllowUserToResizeRows = false;
      this.results.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.results.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Title,
            this.Revision,
            this.Author,
            this.Date,
            this.Score});
      this.results.Dock = System.Windows.Forms.DockStyle.Fill;
      this.results.Location = new System.Drawing.Point(0, 0);
      this.results.MultiSelect = false;
      this.results.Name = "results";
      this.results.ReadOnly = true;
      this.results.RowHeadersVisible = false;
      this.results.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.results.Size = new System.Drawing.Size(853, 276);
      this.results.TabIndex = 0;
      this.results.VirtualMode = true;
      this.results.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.results_CellDoubleClick);
      this.results.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.results_ColumnHeaderMouseClick);
      this.results.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.results_CellValueNeeded);
      this.results.CurrentCellChanged += new System.EventHandler(this.results_CurrentCellChanged);
      // 
      // Title
      // 
      this.Title.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      this.Title.FillWeight = 40F;
      this.Title.HeaderText = "Subject";
      this.Title.Name = "Title";
      this.Title.ReadOnly = true;
      this.Title.Width = 300;
      // 
      // Revision
      // 
      this.Revision.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      this.Revision.FillWeight = 10F;
      this.Revision.HeaderText = "Revision";
      this.Revision.Name = "Revision";
      this.Revision.ReadOnly = true;
      this.Revision.Width = 75;
      // 
      // Author
      // 
      this.Author.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      this.Author.FillWeight = 20F;
      this.Author.HeaderText = "Posted by";
      this.Author.Name = "Author";
      this.Author.ReadOnly = true;
      this.Author.Width = 150;
      // 
      // Date
      // 
      this.Date.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      this.Date.FillWeight = 20F;
      this.Date.HeaderText = "Date";
      this.Date.Name = "Date";
      this.Date.ReadOnly = true;
      this.Date.Width = 150;
      // 
      // Score
      // 
      this.Score.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.Score.FillWeight = 10F;
      this.Score.HeaderText = "Score";
      this.Score.Name = "Score";
      this.Score.ReadOnly = true;
      // 
      // preview
      // 
      this.preview.AllowWebBrowserDrop = false;
      this.preview.Dock = System.Windows.Forms.DockStyle.Fill;
      this.preview.Location = new System.Drawing.Point(0, 0);
      this.preview.MinimumSize = new System.Drawing.Size(20, 20);
      this.preview.Name = "preview";
      this.preview.Size = new System.Drawing.Size(853, 230);
      this.preview.TabIndex = 0;
      // 
      // DialogSearchResults
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(857, 683);
      this.Controls.Add(this.splitContainer1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "DialogSearchResults";
      this.Text = "Search Results";
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.ResumeLayout(false);
      this.splitContainer2.Panel1.ResumeLayout(false);
      this.splitContainer2.Panel2.ResumeLayout(false);
      this.splitContainer2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.results)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataGridView results;
    private System.Windows.Forms.WebBrowser preview;
    private System.Windows.Forms.DataGridViewTextBoxColumn Title;
    private System.Windows.Forms.DataGridViewTextBoxColumn Revision;
    private System.Windows.Forms.DataGridViewTextBoxColumn Author;
    private System.Windows.Forms.DataGridViewTextBoxColumn Date;
    private System.Windows.Forms.DataGridViewTextBoxColumn Score;
    private System.Windows.Forms.SplitContainer splitContainer2;
    protected System.Windows.Forms.SplitContainer splitContainer1;
  }
}