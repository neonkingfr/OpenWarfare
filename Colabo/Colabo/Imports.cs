using System;
using System.Text;
using System.Runtime.InteropServices;

namespace Colabo
{
    public struct CREDUI_INFO
    {
        public int cbSize;
        public IntPtr hwndParent;
        public string pszMessageText;
        public string pszCaptionText;
        public IntPtr hbmBanner;
    };

    [Flags]
    enum CREDUI_FLAGS
    {
        INCORRECT_PASSWORD = 0x1,
        DO_NOT_PERSIST = 0x2,
        REQUEST_ADMINISTRATOR = 0x4,
        EXCLUDE_CERTIFICATES = 0x8,
        REQUIRE_CERTIFICATE = 0x10,
        SHOW_SAVE_CHECK_BOX = 0x40,
        ALWAYS_SHOW_UI = 0x80,
        REQUIRE_SMARTCARD = 0x100,
        PASSWORD_ONLY_OK = 0x200,
        VALIDATE_USERNAME = 0x400,
        COMPLETE_USERNAME = 0x800,
        PERSIST = 0x1000,
        SERVER_CREDENTIAL = 0x4000,
        EXPECT_CONFIRMATION = 0x20000,
        GENERIC_CREDENTIALS = 0x40000,
        USERNAME_TARGET_CREDENTIALS = 0x80000,
        KEEP_USERNAME = 0x100000,
    };

    public enum CredUIReturnCodes
    {
        NO_ERROR = 0,
        ERROR_CANCELLED = 1223,
        ERROR_NO_SUCH_LOGON_SESSION = 1312,
        ERROR_NOT_FOUND = 1168,
        ERROR_INVALID_ACCOUNT_NAME = 1315,
        ERROR_INSUFFICIENT_BUFFER = 122,
        ERROR_INVALID_PARAMETER = 87,
        ERROR_INVALID_FLAGS = 1004,
    };

    class Import
    {
        [DllImport("credui.dll", EntryPoint = "CredUIConfirmCredentialsW", CharSet = CharSet.Unicode)]
        public static extern CredUIReturnCodes CredUIConfirmCredentials(string targetName, [MarshalAs(UnmanagedType.Bool)] bool confirm);

        [DllImport("credui.dll", EntryPoint = "CredUIPromptForCredentials", CharSet = CharSet.Ansi)]
        public static extern CredUIReturnCodes CredUIPromptForCredentials(ref CREDUI_INFO creditUR, string targetName, IntPtr reserved1,
            int iError, StringBuilder userName, int maxUserName, StringBuilder password, int maxPassword, [MarshalAs(UnmanagedType.Bool)] ref bool pfSave, CREDUI_FLAGS flags);

        const int MAX_USER_NAME = 100;
        const int MAX_PASSWORD = 100;
        const int MAX_DOMAIN = 100;

        public static CredUIReturnCodes PromptForCredentials(Uri url, string realm, ref string username, ref string password, ref bool savePassword)
        {
            // CREDUI_INFO
            string host = url.Host;
            string comment = "";
            if (!string.IsNullOrEmpty(realm)) comment = " for " + realm;
            CREDUI_INFO info = new CREDUI_INFO();
            info.cbSize = Marshal.SizeOf(info);
            info.pszCaptionText = host;
            info.pszMessageText = "The server " + host + " requires a username and password" + comment + ".";

            // CREDUI_FLAGS
            CREDUI_FLAGS flags = CREDUI_FLAGS.GENERIC_CREDENTIALS | CREDUI_FLAGS.EXCLUDE_CERTIFICATES|
                CREDUI_FLAGS.SHOW_SAVE_CHECK_BOX |
                CREDUI_FLAGS.ALWAYS_SHOW_UI |
                CREDUI_FLAGS.EXPECT_CONFIRMATION;

            StringBuilder user = new StringBuilder(username, MAX_USER_NAME);
            StringBuilder pwd = new StringBuilder(password, MAX_PASSWORD);
            CredUIReturnCodes result = CredUIPromptForCredentials(ref info, host, IntPtr.Zero, 0, user, MAX_USER_NAME, pwd, MAX_PASSWORD, ref savePassword, flags);
            username = user.ToString();
            password = pwd.ToString();

            return result;
        }
    }
}
