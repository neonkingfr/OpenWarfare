using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Diagnostics;

namespace Colabo
{
  public partial class DialogSearchResults : Form, IReloadPage
  {
    private FormColabo _owner;
    private List<SearchResult> _results;
    protected string _originalTitle;

    // sorting support
    private int _sortedColumn;
    private SortOrder _sortOrder;

    // WinForm designer needs the default constructor
    public DialogSearchResults()
    {
      InitializeComponent();
      _sortedColumn = 4; // score
      _sortOrder = SortOrder.Descending;
      // display the sort order
      if (results != null)
      {
        results.Columns[_sortedColumn].HeaderCell.SortGlyphDirection = _sortOrder;
      }
    }
    public DialogSearchResults(FormColabo owner) : this()
    {
      _owner = owner;
      if (preview != null) preview.ObjectForScripting = new Scripting(owner, this);
    }

    private void results_CurrentCellChanged(object sender, EventArgs e)
    {
      UpdatePreview();
    }
    private void UpdatePreview()
    {
      preview.Hide();
      if (preview.Document != null) preview.Document.Write("");

      DataGridViewRow row = results.CurrentRow;
      if (row == null) return;
      if (row.Index < 0 || row.Index >= _results.Count) return;

      SearchResult result = _results[row.Index];
      string content = result._content;
      if (content == null)
      {
//Stopwatch watch = Stopwatch.StartNew();
         // ask for the content
         content = result._database.GetArticleRevision(result._id, result._revision);
//TimeSpan diff = watch.Elapsed;
//Console.WriteLine("Revision received in {0} s.", diff.TotalSeconds);
      }

      try
      {
        preview.DocumentText = Colabo.Configuration.GetCiteHTMLHeader() + Coma.ComaParser.Transform(result._database.svnRoot, result._database.urlTranslate, content, new List<string>()) + Colabo.Configuration.GetCiteHTMLFooter();
        preview.Show();
      }
      catch (System.Exception e)
      {
        Console.Write(e.ToString());
      }
    }

    private void results_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
    {
      // find the related article info
      if (e.RowIndex < 0 || e.RowIndex >= _results.Count)
      {
        e.Value = "";
        return;
      }

      SearchResult result = _results[e.RowIndex];
      switch (e.ColumnIndex)
      {
        case 0:
          // title
          e.Value = result._title;
          break;
        case 1:
          // revision
          if (result._headRevision)
            e.Value = string.Format("HEAD: {0}", result._revision);
          else
            e.Value = result._revision;
          break;
        case 2:
          // author
          e.Value = result._database.UserDisplayName(result._author);
          break;
        case 3:
          // date
          e.Value = result._time;
          break;
        case 4:
          // score
          e.Value = string.Format("{0:##0}%", 100 * result._score);
          break;
        default:
          e.Value = "";
          break;
      }

    }

    private void results_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
    {
      // find the related article info
      if (e.RowIndex < 0 || e.RowIndex >= _results.Count) return;
      SearchResult result = _results[e.RowIndex];

      // switch to the article in the main form
      if (result != null)
      {
        _owner.GoToArticle(result._database, result._id);
        _owner.Focus();
      }
    }

    // create a query string from the dialog controls
    public virtual string GetQuery()
    {
      return "";
    }
    // create a user readable query string (used in the dialog title)
    public virtual string GetQueryReadable()
    {
      return "";
    }

    // helper to disable some controls when search is in progress
    protected virtual void EnableSearch(bool enable)
    {
    }

    // start the search task
    protected void StartSearch()
    {
      string query = GetQuery();
      if (!string.IsNullOrEmpty(query))
      {
        EnableSearch(false);

        // empty the list of results
        results.RowCount = 0;
        _results = null;

        ITask task = new TaskSearch(this, _owner.databases, query);
        _owner.AddTask(task);

        if (!string.IsNullOrEmpty(_originalTitle)) this.Text = _originalTitle + " - " + GetQueryReadable();
      }
    }
    // called when the search task finished
    public void FinishSearch(List<SearchResult> searchResults)
    {
      _results = searchResults;
      results.RowCount = searchResults.Count;
      Sort(false);
      EnableSearch(true);
    }

    private void results_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      // change the sorting criteria
      if (e.ColumnIndex == _sortedColumn)
      {
        if (_sortOrder == SortOrder.Ascending) _sortOrder = SortOrder.Descending;
        else _sortOrder = SortOrder.Ascending;
      }
      else
      {
        // remove the glyph from the previously sorted column
        results.Columns[_sortedColumn].HeaderCell.SortGlyphDirection = SortOrder.None;

        _sortedColumn = e.ColumnIndex;
        _sortOrder = SortOrder.Ascending;
      }
      results.Columns[_sortedColumn].HeaderCell.SortGlyphDirection = _sortOrder;

      Sort(true);
    }

    // sort the results based on stored sort criteria
    private void Sort(bool keepSelected)
    {
      if (_sortOrder == SortOrder.None) return;
      bool reversed = _sortOrder == SortOrder.Descending;
      CompareSearchResult compare = null;
      switch (_sortedColumn)
      {
        case 0:
          compare = delegate(SearchResult r1, SearchResult r2) { return r1._title.CompareTo(r2._title); };
          break;
        case 1:
          compare = delegate(SearchResult r1, SearchResult r2) { return r1._revision.CompareTo(r2._revision); };
          break;
        case 2:
          compare = delegate(SearchResult r1, SearchResult r2) { return r1._author.CompareTo(r2._author); };
          break;
        case 3:
          compare = delegate(SearchResult r1, SearchResult r2) { return r1._time.CompareTo(r2._time); };
          break;
        case 4:
          compare = delegate(SearchResult r1, SearchResult r2) { return r1._score.CompareTo(r2._score); };
          break;
      }
      if (compare != null)
      {
        // keep the selected article
        int selectedRow = -1;
        if (results.CurrentCell != null) selectedRow = results.CurrentCell.RowIndex;
        int selectedID = -1;
        if (keepSelected)
        {
          if (selectedRow >= 0 && selectedRow < _results.Count) selectedID = _results[selectedRow]._id;
        }

        _results.Sort(new SearchResultComparer(compare, reversed));
        results.Invalidate();

        if (results.Rows.Count>0)
        {
          // select the original article
          int newSelectedRow = 0;
          if (selectedID >= 0) newSelectedRow = _results.FindIndex(delegate(SearchResult result) { return result._id == selectedID; });
          if (newSelectedRow >= 0 && newSelectedRow != selectedRow) results.CurrentCell = results.Rows[newSelectedRow].Cells[0];
          else UpdatePreview();
        }
      }
    }
    private delegate int CompareSearchResult(SearchResult r1, SearchResult r2);
    private class SearchResultComparer : IComparer<SearchResult>
    {
      private bool _reversed;
      private CompareSearchResult _compare;
      public SearchResultComparer(CompareSearchResult compare, bool reversed) { _compare = compare; _reversed = reversed; }
      public int Compare(SearchResult r1, SearchResult r2)
      {
        int diff = _compare(r1, r2);
        return _reversed ? -diff : diff;
      }
    }

    // implementation of IReloadPage
    public void ReloadPage()
    {
      UpdatePreview();
    }
  }
}