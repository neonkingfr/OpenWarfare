﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Colabo
{
  public partial class DialogPrompt : Form
  {
    private DialogPrompt(string title, string initValue)
    {
      InitializeComponent();
      this.Text = title;
      value.Text = initValue;
    }
    private string GetValue()
    {
      return value.Text;
    }
    
    public static string Prompt(string title, string value)
    {
      DialogPrompt dialog = new DialogPrompt(title, value);
      dialog.ShowDialog();
      return dialog.GetValue();
    }
  }
}
