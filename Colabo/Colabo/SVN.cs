using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;
using System.IO;
using System.Net;
using System.Xml;
using System.Diagnostics;
using System.Threading;

using SharpSvn;

namespace Colabo
{
  public class SVNDatabase : ArticlesStore
  {
    public const string _defaultFilename = "{DEFAULT FILENAME}";

    private SvnClient _client;
    // conflicts during the last Update operation
    private List<string> _conflicts;

    // used as a storage for authentications
    private Configuration _config;

    // silent mode (no UI)
    private bool _silent;

    public SVNDatabase(Configuration config)
    {
      _client = new SvnClient();
      _client.Conflict += new EventHandler<SvnConflictEventArgs>(SVNOnConflict);

      _config = config;
      _silent = false;

      // use our UI for authorization
      _client.Authentication.SslServerTrustHandlers += new EventHandler<SharpSvn.Security.SvnSslServerTrustEventArgs>(SVNCheckServerCertificate);
      _client.Authentication.UserNamePasswordHandlers += new EventHandler<SharpSvn.Security.SvnUserNamePasswordEventArgs>(SVNAuthenticationHandler);

      _conflicts = new List<string>();
    }

    // event handler fired when conflict found during Update action
    private void SVNOnConflict(object sender, SvnConflictEventArgs args)
    {
      // store the conflict data to handle later
      _conflicts.Add(args.Path);
    }
    private void SVNCheckServerCertificate(object sender, SharpSvn.Security.SvnSslServerTrustEventArgs args)
    {
      // silent mode - avoid UI
      if (_silent)
      {
        args.Cancel = args.Break = true;
        return;
      }
      CheckServerCertificate(sender, args);
    }
    public static void CheckServerCertificate(object sender, SharpSvn.Security.SvnSslServerTrustEventArgs args)
    {
      string message = "Error validating server certificate for " + args.CommonName + ":\n";
      if ((args.Failures & SharpSvn.Security.SvnCertificateTrustFailures.CertificateNotValidYet) != 0)
      {
        message += "Certificate valid from " + args.ValidFrom + ".\n";
      }
      if ((args.Failures & SharpSvn.Security.SvnCertificateTrustFailures.CertificateExpired) != 0)
      {
        message += "Certificate expired on " + args.ValidUntil + ".\n";
      }
      if ((args.Failures & SharpSvn.Security.SvnCertificateTrustFailures.CommonNameMismatch) != 0)
      {
        message += "Common name mismatch.\n";
      }
      if ((args.Failures & SharpSvn.Security.SvnCertificateTrustFailures.UnknownCertificateAuthority) != 0)
      {
        message += "Unknown certificate issuer.\n";
      }
      if ((args.Failures & SharpSvn.Security.SvnCertificateTrustFailures.UnknownSslProviderFailure) != 0)
      {
        message += "Unknown SSL provider failure.\n";
      }
      message += " Fingerprint: " + args.Fingerprint + "\n";
      message += " Distinguished name: " + args.Issuer + "\n";
      message += "Do you want to proceed?";

      SslServerCertificateTrustDialog dialog = new SslServerCertificateTrustDialog();
      dialog.SetMessage(message);
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.Yes)
      {
        // accept permanently
        args.AcceptedFailures = args.Failures;
        args.Save = true;
      }
      else if (result == DialogResult.No)
      {
        // accept once
        args.AcceptedFailures = args.Failures;
        args.Save = false;
      }
      else
      {
        // reject
        args.Cancel = args.Break = true;
      }
    }
    private void SVNAuthenticationHandler(object sender, SharpSvn.Security.SvnUserNamePasswordEventArgs args)
    {
      bool retry = string.IsNullOrEmpty(args.InitialUserName);
      string realm = string.Copy(args.Realm);
      if (!string.IsNullOrEmpty(realm))
      {
        // remove the server name
        if (realm[0] == '<')
        {
          int index = realm.IndexOf('>');
          if (index > 0) realm = realm.Substring(index + 2);
        }
      }

      bool found = false;
      if (_config != null)
      {

        string user, password;
        if (_config.FindCredentials(args.RealmUri, realm, "Basic", out user, out password))
        {
          args.UserName = user;
          args.Password = password;
          found = true;
          if (!retry) return;
        }
      }

      // silent mode - avoid UI
      if (_silent)
      {
        if (!found)
        {
          args.Cancel = args.Break = true;
        }
        return;
      }

      if (!found)
      {
        args.UserName = args.InitialUserName;
        if (string.IsNullOrEmpty(args.UserName)) args.UserName = Environment.UserName;
        args.Password = "";
      }
      if (Authenticate(args, realm))
      {
        if (args.Save)
        {
          // store to the shared cache
          // TODO: MT safety when invoked from the background worker
          if (_config != null)
            _config.UpdateAuthentications(args.RealmUri, realm, "Basic", new NetworkCredential(args.UserName, args.Password), true); // replace, add and store updated config file
        }
      }
    }
    public static bool Authenticate(SharpSvn.Security.SvnUserNamePasswordEventArgs args, string realm)
    {
      // ask user for username / password
      // invoke the dialog
      string user = args.UserName;
      string pwd = args.Password;
      bool savePwd = true;
      CredUIReturnCodes code = Import.PromptForCredentials(args.RealmUri, realm, ref user, ref pwd, ref savePwd);
      if (code != CredUIReturnCodes.NO_ERROR)
      {
        args.Cancel = args.Break = true;
        return false;
      }

      args.UserName = user;
      args.Password = pwd;
      args.Save = savePwd;
      return true;
    }

    private bool InsideTask()
    {
      // check if invoked from the background thread
      return Thread.CurrentThread.IsBackground;
    }

    public string GetArticleContent(string path, ref BrowserRemotingProxy browserProxy)
    {
      string extension = Path.GetExtension(path);
      if (extension != ".txt" && extension != ".js") return null; // only markdown formatted files need to be read directly

      browserProxy.NavigateUrl = path;
      // read the content by ourselves
      // System.Diagnostics.Debug.Print("Entering AccessWebResponse for {0}", url);
      // Passing 'Colabo.BrowserRemotingProxy.realm' as ref or out or taking its address may cause a runtime exception because it is a field of a marshal-by-reference class
#pragma warning disable 0197
      return FormColabo.GetArticleContent(path, out browserProxy.realm);
#pragma warning restore
    }

    // set / reset silent mode, return the original value
    public bool SetSilent(bool silent)
    {
      bool old = _silent;
      _silent = silent;
      return old;
    }

    // find in which repository the url is
    public Uri GetRepositoryRoot(Uri url)
    {
      Trace.Assert(InsideTask(), "SVN operation called from the UI thread");
      try
      {
        return _client.GetRepositoryRoot(url);
      }
      catch (System.Exception exception)
      {
        Console.Write(exception.ToString());
        return null;
      }
    }

    // find in which repository the url is
    public string GetRepositoryGuid(Uri url)
    {
      Trace.Assert(InsideTask(), "SVN operation called from the UI thread");

      Guid guid;
      if (!_client.TryGetRepositoryId(url, out guid)) return null;
      return guid.ToString();
    }

    // find in which repository the url is
    public string GetRepositoryGuidUnsafe(Uri url)
    {
      Guid guid;
      if (!_client.TryGetRepositoryId(url, out guid)) return null;
      return guid.ToString();
    }

    // Fast info check using WebDAV protocol
    public bool GetRepositoryInfo(Uri url, out string root, out string uuid, bool silent)
    {
      root = null;
      uuid = null;
      try
      {
        string dummy;
        string[] headers = { "Depth: 0" };
        string data = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
          "<D:propfind xmlns:D=\"DAV:\" xmlns:lp3=\"http://subversion.tigris.org/xmlns/dav/\">" +
          "  <D:prop>" +
          "    <lp3:baseline-relative-path/>" +
          "    <lp3:repository-uuid/>" +
          "  </D:prop>" +
          "</D:propfind>";
        using (WebResponse response = FormColabo.AccessWebResponse(url, "PROPFIND", headers, data, out dummy, silent))
        {

          if (response == null) return false;

          using (Stream stream = response.GetResponseStream())
          {
            using (XmlReader reader = XmlReader.Create(stream))
            {

              while (reader.Read())
              {
                if (!reader.IsStartElement()) continue;
                if (reader.Name.EndsWith("baseline-relative-path", StringComparison.OrdinalIgnoreCase))
                {
                  if (reader.IsEmptyElement) root = url.ToString();
                  else
                  {
                    string full = url.ToString();
                    int len = full.Length;
                    if (full[len - 1] == '/')
                    {
                      len--;
                      full = full.Substring(0, len);
                    }
                    string relative = reader.ReadString();
                    root = full.Substring(0, len - relative.Length);
                  }
                }
                else if (reader.Name.EndsWith("repository-uuid", StringComparison.OrdinalIgnoreCase))
                {
                  if (reader.IsEmptyElement) uuid = "";
                  else uuid = reader.ReadString();
                }
              }
            }
          }
        }
        return true;
      }
      catch (Exception exception)
      {
        FormColabo.Log(string.Format(" - DAV info about {0}, exception {1}", url, exception.ToString()));
        return false;
      }
    }

    // update the file / directory, handle all problems / exceptions
    private bool Update(string path, bool dir)
    {
      // first ensure if parent directory exists
      string parent = Path.GetDirectoryName(path);
      if (!Directory.Exists(parent))
      {
        if (!Update(parent, true)) return false;
      }

      // update no children
      SvnUpdateArgs updateArgs = new SvnUpdateArgs();
      if (dir) updateArgs.Depth = SvnDepth.Empty;

      try
      {
        if (_client.Update(path, updateArgs)) return true;
        if (!_silent) MessageBox.Show(string.Format("Update of {0} failed.", path), "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return false;
      }
      catch (SvnWorkingCopyLockException exception)
      {
        Console.Write(exception.ToString());

        // working copy locked, try if CleanUp will help
        _client.CleanUp(Path.GetDirectoryName(path));
      }
      catch (System.Exception exception)
      {
        if (!_silent) MessageBox.Show(string.Format("Update of {0} failed. ({0})", exception.ToString()), "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return false;
      }

      // try to process operation again when lock is repaired
      try
      {
        if (_client.Update(path, updateArgs)) return true;
        if (!_silent) MessageBox.Show(string.Format("Update of {0} failed.", path), "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return false;
      }
      catch (System.Exception exception)
      {
        if (!_silent) MessageBox.Show(string.Format("Update of {0} failed. ({0})", exception.ToString()), "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return false;
      }
    }

    // Update the working copy of the article
    public void UpdateWorkingCopy(SQLDatabase database, ArticleItem item, bool update, int changedRevision)
    {
      string itemURL = item.AbsoluteURL(database);
      if (!string.IsNullOrEmpty(item._workingCopy) && item._id >= 0)
      {
        // some working copy found in previous call of GetWorkingCopy
        if (File.Exists(item._workingCopy))
        {
          // check if working copy is valid
          Uri url = null;
          try
          {
            url = _client.GetUriFromWorkingCopy(item._workingCopy);
          }
          catch (System.Exception exception)
          {
            FormColabo.Log(" - working copy update failed " + exception.ToString());
          }
          if (url != null && url.ToString() == itemURL)
          {
            // update the working copy
            if (update && changedRevision > GetLocalRevision(item._workingCopy)) Update(item._workingCopy, false);
            return;
          }
        }
      }
      // find the working copy
      item._workingCopy = GetWorkingCopy(itemURL, update);
    }

    // Create working copy of the given SVN file, return its path (or "" if not correct URL is given)
    public string GetWorkingCopy(string path, bool update)
    {
      Trace.Assert(InsideTask(), "SVN operation called from the UI thread");

      if (string.IsNullOrEmpty(path)) return "";

      // Find the info about the file in repository
      Uri root = null;
      Guid guid = Guid.Empty;
      long remoteRevision = 0;

      if (path.Contains(_defaultFilename))
      {
        // new article - Info requires the item exist in repository
        try
        {
          root = _client.GetRepositoryRoot(new Uri(path));
          if (!_client.TryGetRepositoryId(new Uri(path), out guid))
          {
            FormColabo.Log(" - repository id not found");
            return "";
          }
        }
        catch (System.Exception exception)
        {
          FormColabo.Log(" - repository info failed " + exception.ToString());
          return "";
        }
      }
      else
      {
        try
        {
          _client.Info(path, delegate(object sender, SvnInfoEventArgs info)
          {
            root = info.RepositoryRoot;
            guid = info.RepositoryId;
            remoteRevision = info.LastChangeRevision;
          });
        }
        catch (System.Exception exception)
        {
          if (!_silent) MessageBox.Show(string.Format("Repository check failed. Invalid URL '{0}'. ({1})", path, exception.ToString()), "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
          return ""; // invalid URL
        }
      }

      if (root == null) return "";  // not a SVN source

      // relative path of the file
      string relative = path.Substring(root.ToString().Length);
      if (root + relative != path)
      {
        if (!_silent) MessageBox.Show(string.Format("Operation failed. Wrong relative path. ({0} != {1})", root + relative, path), "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return "";
      }

      // create the working copy for the repository
      string workingRoot = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
      if (workingRoot.Length == 0) return "";
      workingRoot = workingRoot + "\\Colabo\\" + guid.ToString();

      // root can be unaccessible, try to find the first accessible subdirectory
      if (!FindAccessibleRoot(ref root, ref workingRoot, ref relative))
      {
        if (!_silent) MessageBox.Show(string.Format("Operation failed. Access not granted to '{0}'.", path), "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return "";
      }

      string workingCopyRoot = FindWorkingRoot(root.ToString());
      if (!String.IsNullOrEmpty(workingCopyRoot) && workingRoot != workingCopyRoot)
      {
        // the working copy is wrong - its root is not where expected
        // delete the whole working copy
        try
        {
          Program.EmptyDirectory(workingCopyRoot);
        }
        catch (System.Exception e)
        {
          if (!_silent) MessageBox.Show(string.Format("Operation failed. Working copy '{0}' is corrupted. ({1})", workingCopyRoot, e.ToString()), "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
          return "";
        }
      }

      // if working copy does not exist at all, create it now (need to exist when article is sending)
      if (!Directory.Exists(workingRoot)) Directory.CreateDirectory(workingRoot);

      Uri urlCheck = null;
      try
      {
        urlCheck = _client.GetUriFromWorkingCopy(workingRoot);
      }
      catch (System.Exception e)
      {
        if (!_silent) MessageBox.Show(string.Format("Operation failed. Cannot get Uri from working copy '{0}'. ({1})", workingRoot, e.ToString()), "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return "";
      }
      if (urlCheck != root)
      {
        // the root directory is not a working copy, we need to check out
        // ensure the directory is empty (delete remains from possible old working copy)
        try
        {
          Program.EmptyDirectory(workingRoot);
        }
        catch (System.Exception e)
        {
          if (!_silent) MessageBox.Show(string.Format("Operation failed. Working copy '{0}' is corrupted. ({1})", workingRoot, e.ToString()), "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
          return "";
        }

        SvnCheckOutArgs args = new SvnCheckOutArgs();
        args.Depth = SvnDepth.Empty;
        try
        {
          _client.CheckOut(root, workingRoot, args);
        }
        catch (System.Exception e)
        {
          if (!_silent) MessageBox.Show(string.Format("Operation failed. Cannot check-out '{0}'. ({1})", root, e.ToString()), "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
          return "";
        }
      }

      string workingPath = workingRoot + "\\" + relative;
      // Character '#' is legal in path, but forbidden in URL
      // TODO: check if more such characters needs to be handled
      workingPath = workingPath.Replace("%23", "#");
      if (update)
      {
        long localRevision = -1;
        if (File.Exists(workingPath))
        {
          try
          {
            _client.Info(workingPath, delegate(object sender, SvnInfoEventArgs info)
            {
              localRevision = info.LastChangeRevision;
            });
          }
          catch (System.Exception e)
          {
            if (!_silent) MessageBox.Show(string.Format("Local info on '{0}' failed. ({1})", workingPath, e.ToString()), "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
          }
        }

        if (remoteRevision > localRevision) Update(workingPath, false);
      }
      return workingPath;
    }
    // find the revision of the last change of the working copy
    public int GetLocalRevision(string path)
    {
      if (!File.Exists(path)) return 0; // no working copy, update is necessary

      try
      {
        int revision = 0;
        _client.Info(path, delegate(object sender, SvnInfoEventArgs info)
        {
          revision = (int)info.LastChangeRevision;
        });
        return revision;
      }
      catch (System.Exception)
      {
        return 0; // invalid path
      }
    }
    // Find the first accessible subdirectory
    private bool FindAccessibleRoot(ref Uri rootUri, ref string workingRoot, ref string relative)
    {
      string root = rootUri.ToString();
      char[] delimiters = new char[] { '\\', '/' };
      while (true)
      {
        bool accessFailed = false;
        try
        {
          // cheep operation to check the access
          _client.Info(SvnTarget.FromString(root), delegate(object sender, SvnInfoEventArgs info) {});
          // no exception - access granted
          rootUri = new Uri(root);
          return true;
        }
        catch (SvnRepositoryIOException e)
        {
          accessFailed = (e.SvnErrorCode == SvnErrorCode.SVN_ERR_RA_DAV_REQUEST_FAILED) || (e.SvnErrorCode == SvnErrorCode.SVN_ERR_RA_DAV_FORBIDDEN);
        }
        catch (Exception e)
        {
          FormColabo.Log(string.Format(" - SVN info about {0}, exception {1}", root, e.ToString()));
          return false;
        }
        if (!accessFailed) return false; // some other error

        // access failed, try to check the subdirectory
        int index = relative.IndexOfAny(delimiters);
        if (index < 0)
        {
          // no subdirectory, even the leaf directory is unaccessible
          return false;
        }
        root += relative.Substring(0, index + 1);
        if (workingRoot != null) workingRoot += "\\" + relative.Substring(0, index);
        relative = relative.Substring(index + 1);
      }
    }
    // Find the root of working copy for given article
    private string FindWorkingRoot(string url)
    {
      // repository root
      Uri root;
      try
      {
        root = _client.GetRepositoryRoot(new Uri(url));
      }
      catch (System.Exception)
      {
        return "";
      }

      if (root == null)
      {
        return "";
      }

      // relative path of the file
      string relative = url.Substring(root.ToString().Length);
      if (root + relative != url.ToString())
      {
        return "";
      }

      // corresponding local root
      Guid guid;
      if (!_client.TryGetRepositoryId(new Uri(url), out guid)) return null;
      string workingRoot = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Colabo\\" + guid.ToString();

      // find the working copy
      char[] delimiters = new char[] { '\\', '/' };
      while (true)
      {
        if (_client.GetUriFromWorkingCopy(workingRoot) != null) return workingRoot;

        // not a working copy, try to check the subdirectory
        int index = relative.IndexOfAny(delimiters);
        if (index < 0)
        {
          // not found
          return "";
        }
        workingRoot += "\\" + relative.Substring(0, index);
        relative = relative.Substring(index + 1);
      }
    }

    // Load the tags of the working copy
    public bool LoadProperties(string path, out string tags)
    {
      Trace.Assert(InsideTask(), "SVN operation called from the UI thread");

      string value;
      if (_client.GetProperty(path, "colabo_tag", out value))
      {
        tags = ""; // comma separeted list

        if (value != null)
        {
          // \n is used as a tags separator
          string tag;
          int i1 = 0;
          int i2 = value.IndexOf('\n', i1);
          while (i2 >= 0)
          {
            tag = value.Substring(i1, i2 - i1).Trim();
            if (tag.Length > 0)
            {
              if (tags.Length > 0) tags += ", ";
              tags += tag;
            }
            i1 = i2 + 1;
            i2 = value.IndexOf('\n', i1);
          }
          tag = value.Substring(i1).Trim();
          if (tag.Length > 0)
          {
            if (tags.Length > 0) tags += ", ";
            tags += tag;
          }
        }
        return true;
      }

      tags = null;
      return false;
    }

    public static bool Export(string url, string path, Form parent)
    {
      // create own instance to avoid MT problems
      using (SvnClient client = new SvnClient())
      {
        SharpSvn.UI.SvnUI.Bind(client, parent);
        try
        {
          SvnExportArgs args = new SvnExportArgs();
          args.Overwrite = true;
          return client.Export(SvnTarget.FromString(url), path, args);
        }
        catch (System.Exception e)
        {
          Console.WriteLine(e.ToString());
          return false;
        }
      }
    }

    // Write the content and tags to the working copy and commit the article
    public string CommitArticle(SQLDatabase database, ref ArticleItem item, ref string path, UpdateText content, bool majorRevision, System.ComponentModel.BackgroundWorker worker)
    {
      Trace.Assert(InsideTask(), "SVN operation called from the UI thread");

      // working copy root
      string workingRoot = FindWorkingRoot(item.AbsoluteURL(database));
      if (string.IsNullOrEmpty(workingRoot))
      {
        MessageBox.Show("Article not sent - working copy not found.", "Colabo",
          MessageBoxButtons.OK, MessageBoxIcon.Error);
        return null;
      }

      // write the content to the file, add to the SVN if necessary
      if (path.Contains(_defaultFilename))
      {
        path = AddUniqueFile(path, item._title, workingRoot, item._ownTags);
        item._workingCopy = path;
        if (string.IsNullOrEmpty(path)) return null;
        string fullURL = _client.GetUriFromWorkingCopy(path).ToString();
        item.RawURL = database.ToRelative(fullURL);
      }
      else if (!File.Exists(path))
      {
        Directory.CreateDirectory(Path.GetDirectoryName(path));
        File.CreateText(path).Close(); // create an empty file

        // add the whole path
        SvnAddArgs addArgs = new SvnAddArgs();
        addArgs.AddParents = true;
        _client.Add(path, addArgs);
      }

      if (worker != null) worker.ReportProgress(40);

      string text = System.IO.File.ReadAllText(path, System.Text.Encoding.UTF8);

      text = content(text);

      System.IO.File.WriteAllText(path, text, System.Text.Encoding.UTF8);

      if (worker != null) worker.ReportProgress(50);

      // commit the document to the SVN with correct properties
      string value = "";
      if (item._ownTags != null)
      {
        for (int i = 0; i < item._ownTags.Count; i++)
        {
          string tag = item._ownTags[i].Trim();
          // do not store private tags
          if (ArticleItem.FromPrivateTag(tag) != null) continue;
          if (tag.Length > 0)
          {
            if (value.Length > 0) value = value + "\n";
            value = value + tag;
          }
        }
      }
      _client.SetProperty(path, "colabo_tag", value);

      if (worker != null) worker.ReportProgress(60);

      // commit all changes to the SVN (call commit on the whole repository)
      TryCommit(workingRoot, item._title, majorRevision);

      if (worker != null) worker.ReportProgress(80);

      SvnInfoEventArgs info;
      if (_client.GetInfo(path, out info))
      {
        item._revision = (int)info.LastChangeRevision;
        if (majorRevision)
        {
          item._majorRevision = item._revision;
          item._date = info.LastChangeTime.ToLocalTime();
          FormColabo.Log(string.Format("Date set to {0} (last change was {1})", item._date, info.LastChangeTime));
        }
      }
      else
      {
        MessageBox.Show("The info about the last revision is not accessible, please check the article header.", "Colabo",
          MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      // find author from the log (the original author of the article)
      if (item._author == null || item._author.Length == 0)
      {
        // reverse order of revisions, limit to 1 revision
        SvnLogArgs logArgs = new SvnLogArgs(new SvnRevisionRange(1, SvnRevision.Head));
        logArgs.Limit = 1;
        string firstAuthor = null;
        if (!_client.Log(path, logArgs, delegate(object sender, SvnLogEventArgs args) { firstAuthor = args.Author; }))
        {
          MessageBox.Show("The info about the first revision is not accessible, please check the article header.", "Colabo",
            MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        if (firstAuthor != null) item._author = firstAuthor;
      }

      return path;
    }

    public void AddFiles(List<ImportedArticle> articles)
    {
      // add the whole path
      SvnAddArgs addArgs = new SvnAddArgs();
      addArgs.AddParents = true;
      foreach (ImportedArticle article in articles)
      {
        _client.Add(article._path, addArgs);
        if ( !String.IsNullOrEmpty(article._tags) ) _client.SetProperty(article._path, "colabo_tag", article._tags);
      }
    }
    // add the file with a unique name matching the given mask to SVN
    private string AddUniqueFile(string mask, string title, string workingRoot, List<string> tags)
    {
      Trace.Assert(InsideTask(), "SVN operation called from the UI thread");

      string originalTitle = title;
      // change title to avoid invalid characters
      char[] invalidChars = Path.GetInvalidFileNameChars();
      foreach (char c in invalidChars) title = title.Replace(c, '_');
      title = title.Replace(' ', '_');
      // parentheses are not invalid, but they do not work well with URL parsing in default Markdown
      title = title.Replace('(', '_');
      title = title.Replace(')', '_');
      // two underscores do not look very nice
      title = title.Replace("__", "_");
      // some signs do not look nice with underscores around them
      title = title.Replace("_-_", "-");
      title = title.Replace("_+_", "+");

      // add the directory first (recursively)
      string dir = Path.GetDirectoryName(mask);

      // update all parents of the file in the working copy (including dir)
      if (!Update(dir, true)) return "";
      if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);

      SvnAddArgs addArgs = new SvnAddArgs();
      addArgs.AddParents = true;
      string addPath = "";
      try
      {
        // create the subfolder in SVN if needed
        if (dir.Length > workingRoot.Length)
          _client.Add(dir, addArgs);
      }
      catch (SvnEntryException e)
      {
        if (e.SvnErrorCode == SvnErrorCode.SVN_ERR_ENTRY_EXISTS)
        {
          // ok, folder already exists in SVN
        }
        else
        {
          MessageBox.Show(string.Format("Article not sent - cannot add '{0}' to SVN. ({1})", dir, e.ToString()), "Colabo",
            MessageBoxButtons.OK, MessageBoxIcon.Error);
          return "";
        }
      }
      catch (System.Exception exception)
      {
        MessageBox.Show(string.Format("Article not sent - cannot add '{0}' to SVN. ({1})", dir, exception.ToString()), "Colabo",
          MessageBoxButtons.OK, MessageBoxIcon.Error);
        return "";
      }

      // if update did not create the directory, create it now
      Directory.CreateDirectory(dir);

      {
        try
        {
          SvnCommitArgs commitArgs = new SvnCommitArgs();
          commitArgs.LogMessage = string.Format("Colabo: New folder for {0}", originalTitle);
          _client.Commit(workingRoot, commitArgs);
        }
        catch (System.Exception exception)
        {
          MessageBox.Show(string.Format("Article not sent - commit of '{0}' failed. ({1})", workingRoot, exception.ToString()), "Colabo",
            MessageBoxButtons.OK, MessageBoxIcon.Error);
          return "";
        }
      }


      int i = 0;
      while (true)
      {
        i++;
        string path = "";
        if (i == 1)
        {
          // avoid _1 suffix
          if (addPath.Length > 0)
            path = mask.Replace(_defaultFilename, string.Format("{1}\\{0}", title, addPath));
          else
            path = mask.Replace(_defaultFilename, title);
        }
        else
        {
          if (addPath.Length > 0)
            path = mask.Replace(_defaultFilename, string.Format("{2}\\{0}_{1:D}", title, i, addPath));
          else
            path = mask.Replace(_defaultFilename, string.Format("{0}_{1:D}", title, i));
        }

        path = path.Replace('/', '\\'); // sometimes it seems some slashes slip through
        Update(path, false);
        if (!File.Exists(path))
        {
          // create an empty file
          try
          {
            StreamWriter writer = File.CreateText(path);
            writer.Close();
          }
          catch (System.Exception exception)
          {
            Console.Write(exception.ToString());
          }

          // try to add it to SVN
          try
          {
            _client.Add(path);
          }
          catch (SvnEntryException e)
          {
            File.Delete(path);
            if (e.SvnErrorCode == SvnErrorCode.SVN_ERR_ENTRY_EXISTS)
            {
              // already exists, try other path
              continue;
            }
            else
            {
              MessageBox.Show(string.Format("Article not sent - cannot add file '{0}' to SVN. ({1})", path, e.ToString()), "Colabo",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
              return "";
            }
          }
          catch (System.Exception exception)
          {
            Console.Write(exception.ToString());
            // something went wrong
            File.Delete(path);
            MessageBox.Show(string.Format("Article not sent - cannot add file '{0}' to SVN. ({1})", path, exception.ToString()), "Colabo",
              MessageBoxButtons.OK, MessageBoxIcon.Error);
            return "";
          }

          // try to commit
          try
          {
            SvnCommitArgs commitArgs = new SvnCommitArgs();
            commitArgs.LogMessage = string.Format("Colabo: {0}", originalTitle);
            _client.Commit(path, commitArgs);
            // succeeded
            return path;
          }
          catch (SvnRepositoryIOException e)
          {
            _client.Revert(path); // revert Add operation
            File.Delete(path);
            if (e.SvnErrorCode == SvnErrorCode.SVN_ERR_RA_DAV_ALREADY_EXISTS)
            {
              // already exists, try other path
            }
            else
            {
              MessageBox.Show(string.Format("Article not sent - commit of '{0}' failed. ({1})", path, e.ToString()), "Colabo",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
              return "";
            }
          }
          catch (System.Exception e)
          {
            // something went wrong
            _client.Revert(path); // revert Add operation
            File.Delete(path);
            MessageBox.Show(string.Format("Article not sent - commit of '{0}' failed. ({1})", path, e.ToString()), "Colabo",
              MessageBoxButtons.OK, MessageBoxIcon.Error);
            return "";
          }
        }
      }
    }

    // SVN log of a whole repository
    public void GetLog(ref SVNLog logRef, Uri url, System.ComponentModel.BackgroundWorker worker)
    {
      // avoid C# error CS1628
      SVNLog log = logRef;

      // given revision range
      int lastRevision = log._lastRevision;
      SvnLogArgs logArgs = new SvnLogArgs(new SvnRevisionRange(SvnRevision.Head, lastRevision + 1));
      _client.Log(url, logArgs,
        delegate(object sender, SvnLogEventArgs args)
        {
          RevisionInfo revision = new RevisionInfo();
          revision._revision = (int)args.Revision;
          revision._author = args.Author;
          revision._time = args.Time;

          if (args.ChangedPaths != null)
          {
            foreach (SvnChangeItem item in args.ChangedPaths)
            {
              log.Add(item.RepositoryPath.ToString(), revision);
            }
          }

          if (worker != null)
          {
            // report progress
            if (log._lastRevision > lastRevision)
            {
              float progress = 100.0f * (log._lastRevision - args.Revision) / (log._lastRevision - lastRevision);
              worker.ReportProgress((int)progress);
            }
          }
        });
    }

    // SVN log - helper for scripting
    public static void GetLog(Form parent, Uri url, DateTime from, SvnLogEntry handler)
    {
      // create own instance to avoid MT problems
      SvnClient client = new SvnClient();
      SharpSvn.UI.SvnUI.Bind(client, parent);

      string root = client.GetRepositoryRoot(url).ToString();
      try
      {
        client.Log(url, delegate(object sender, SvnLogEventArgs args)
          {
              if (args.Time < from)
              {
                // time limit reached
                args.Cancel = true;
                return;
              }
              handler(args, root);
          });
      }
      catch (System.Exception exception)
      {
        Console.Write(exception.ToString());
      }
    }
    public delegate void SvnLogEntry(SvnLogEventArgs args, string root);

    public void Lock(string path)
    {
      Trace.Assert(InsideTask(), "SVN operation called from the UI thread");
      _client.Lock(path, "Editing in Colabo");
    }

    public void Unlock(string path)
    {
      Trace.Assert(InsideTask(), "SVN operation called from the UI thread");
      _client.Unlock(path);
    }

    // SVN log of a single file
    public List<RevisionInfo> GetRevisions(string url, SvnRevision from, SvnRevision to)
    {
      List<RevisionInfo> result = new List<RevisionInfo>();

      // given revision range
      SvnLogArgs logArgs = new SvnLogArgs(new SvnRevisionRange(to, from));
      _client.Log(new Uri(url), logArgs, 
        delegate(object sender, SvnLogEventArgs args)
        {
          RevisionInfo info = new RevisionInfo();
          info._revision = (int)args.Revision;
          info._author = args.Author;
          info._time = args.Time;

          result.Add(info);
        });
      return result;
    }
    public List<RevisionInfo> GetRevisions(string url, SvnRevision from)
    {
      return GetRevisions(url, from, SvnRevision.Head);
    }
    public List<RevisionInfo> GetRevisions(string url)
    {
      return GetRevisions(url, 1, SvnRevision.Head);
    }

    // Remove the article from the SVN
    public void DeleteArticle(string path, string title)
    {
      Trace.Assert(InsideTask(), "SVN operation called from the UI thread");

      // delete
      if (SafeSvn(delegate() { return _client.Delete(path); }, null) != null) return;

      // commit
      TryCommit(path, title,true);
    }

    // return the content of the specifier revision of given file
    public string GetArticleRevision(string url, int revision)
    {
      Trace.Assert(InsideTask(), "SVN operation called from the UI thread");

      SvnWriteArgs args = new SvnWriteArgs();
      args.Revision = revision;
      Stream stream = new MemoryStream();
      bool ok = false;
      try
      {
        ok = _client.Write(SvnTarget.FromString(url), stream, args);
      }
      catch (System.Exception exception)
      {
        Console.Write(exception.ToString());
        return null;
      }

      if (ok)
      {
        stream.Seek(0, SeekOrigin.Begin);
        StreamReader reader = new StreamReader(stream, Encoding.UTF8);
        string content = reader.ReadToEnd();
        reader.Close();
        return content;
      }
      return null;
    }

    // return the content of the specifier revision of given file
    public List<string> GetPropertiesRevision(string url, int revision)
    {
      Trace.Assert(InsideTask(), "SVN operation called from the UI thread");

      SvnGetPropertyArgs args = new SvnGetPropertyArgs();
      args.Revision = revision;

      SvnTargetPropertyCollection properties;
      bool ok = false;
      try
      {
        ok = _client.GetProperty(url, "colabo_tag", args, out properties);
      }
      catch (System.Exception exception)
      {
        Console.Write(exception.ToString());
        return null;
      }
      if (ok && properties.Count == 1)
      {
        SvnPropertyValue val = properties[0];
        string value = val.StringValue;
        if (value != null)
        {
          // \n is used as a tags separator
          List<string> array = new List<string>();
          string tag;
          int i1 = 0;
          int i2 = value.IndexOf('\n', i1);
          while (i2 >= 0)
          {
            tag = value.Substring(i1, i2 - i1).Trim();
            if (tag.Length > 0) array.Add(tag);
            i1 = i2 + 1;
            i2 = value.IndexOf('\n', i1);
          }
          tag = value.Substring(i1).Trim();
          if (tag.Length > 0) array.Add(tag);

          return array;
        }
      }

      return null;
    }
    // Helper to handle exceptions in SVN operations
    delegate bool SvnOperation();
    private string SafeSvn(SvnOperation op, string workingCopyRoot)
    {
      try
      {
        bool result = op();
        return result ? null : "FAILED";
      }
      catch (SvnWorkingCopyLockException exception)
      {
        Console.Write(exception.ToString());

        // clean up the working copy
        if (workingCopyRoot == null) return exception.ToString();
        _client.CleanUp(workingCopyRoot);
      }
      catch (System.Exception exception)
      {
        return exception.ToString();
      }

      // try to process operation again when lock is repaired
      try
      {
        bool result = op();
        return result ? null : "FAILED";
      }
      catch (System.Exception exception)
      {
        return exception.ToString();
      }
    }

    // store SVN property for a single article
    public bool UpdateTags(SQLDatabase database, ref ArticleItem item, bool majorRevision)
    {
      UpdateWorkingCopy(database, item, true, int.MaxValue); // update the article then change the tags
      if (item._workingCopy.Length > 0) // valid SVN file
      {
        // commit the document to the SVN with correct properties
        string value = "";
        if (item._ownTags != null)
        {
          for (int i = 0; i < item._ownTags.Count; i++)
          {
            string tag = item._ownTags[i].Trim();
            // do not store private tags
            if (ArticleItem.FromPrivateTag(tag) != null) continue;
            if (tag.Length > 0)
            {
              if (value.Length > 0) value = value + "\n";
              value = value + tag;
            }
          }
        }
        _client.SetProperty(item._workingCopy, "colabo_tag", value);

        // commit
        TryCommit(item._workingCopy, item._title, false);

        // update the article properties
        UpdateRevisionAndDate(item._workingCopy, majorRevision, ref item);

        return true;
      }
      return false;
    }

    // rebuild SVN property for all articles
    public void StoreTagsToSVN(SQLDatabase database, ArticlesCache cache)
    {
      Trace.Assert(InsideTask(), "SVN operation called from the UI thread");

      foreach (ArticleItem item in cache.Values)
      {
        UpdateWorkingCopy(database, item, true, int.MaxValue); // update the article then change the tags
        if (item._workingCopy.Length > 0) // valid SVN file
        {
          // commit the document to the SVN with correct properties
          string value = "";
          if (item._ownTags != null)
          {
            for (int i = 0; i < item._ownTags.Count; i++)
            {
              string tag = item._ownTags[i].Trim();
              // do not store private tags
              if (ArticleItem.FromPrivateTag(tag) != null) continue;
              if (tag.Length > 0)
              {
                if (value.Length > 0) value = value + "\n";
                value = value + tag;
              }
            }
          }
          _client.SetProperty(item._workingCopy, "colabo_tag", value);
          // do not commit yet - avoid a lot of small commits
        }
      }
    }

    // commit operation, handle conflicts and other exceptions
    private bool TryCommit(string path, string title, bool majorRevision)
    {
      SvnCommitArgs commitArgs = new SvnCommitArgs();
      commitArgs.LogMessage = string.Format("Colabo{1}: {0}", title, majorRevision ? "" : " (minor)");

      while (true)
      {
        try
        {
          _client.Commit(path, commitArgs);
          return true;
        }
        catch (SvnFileSystemOutOfDateException exception)
        {
          FormColabo.Log("Commit exception: " + exception.ToString());

          if (!ResolveConflict(path))
          {
            ResolveManually(path);
            return false;
          }
          // try again
        }
        catch (System.Exception exception)
        {
          FormColabo.Log("Commit exception: " + exception.ToString());

          ResolveManually(path);
          return false;	
        }
      }
    }
    private void OnFileCreated(object sender, FileSystemEventArgs args)
    {
      Console.WriteLine("{0}: File created: {1}", DateTime.Now, args.Name);
    }
    private bool ResolveConflict(string path)
    {
      FileSystemWatcher watcher = new FileSystemWatcher(Path.GetDirectoryName(path));
      watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
           | NotifyFilters.FileName | NotifyFilters.DirectoryName;
      watcher.Created += new FileSystemEventHandler(OnFileCreated);
      watcher.EnableRaisingEvents = true;

      // check if some conflicts appeared
      _conflicts.Clear();
      bool updated = false;
      try
      {
        _client.Update(path);
        updated = true;
      }
      catch (SvnWorkingCopyLockException exception)
      {
        Console.Write(exception.ToString());

        // clean up the working copy
        _client.CleanUp(path);
      }
      catch (System.Exception)
      {
        return false;
      }

      if (!updated)
      {
        // cleaned up, try the Update once more
        try
        {
          _client.Update(path);
        }
        catch (System.Exception)
        {
          return false;
        }
      }

      Console.WriteLine("{0}: Update finished", DateTime.Now);

      // solve all conflicts
      foreach (string conflict in _conflicts)
      {
        // Use TortoiseSVN to resolve the conflict
        System.Diagnostics.Process proc = new System.Diagnostics.Process();
        proc.StartInfo.FileName = "TortoiseProc.exe";
        proc.StartInfo.Arguments = string.Format("/command:conflicteditor /path:\"{0}\"", conflict);
        proc.StartInfo.UseShellExecute = false;
        proc.StartInfo.RedirectStandardOutput = false;
        if (proc.Start())
        {
          proc.WaitForExit();
          _client.Resolved(conflict);
        }
      }

      return true; // all conflicts resolved
    }
    private void ResolveManually(string path)
    {
      DialogResult result = MessageBox.Show(
          "SVN commit failed, please try to resolve manually.\r\nThe folder will be opened.", "Colabo",
          MessageBoxButtons.OK, MessageBoxIcon.Error);
      // execute the windows explorer
      System.Diagnostics.Process proc = new System.Diagnostics.Process();
      proc.StartInfo.FileName = Path.GetDirectoryName(path);
      proc.StartInfo.Verb = "Explore";
      proc.Start();
    }

    // simple commit of the file, return the revision
    public void Commit(string path, string title)
    {
      Trace.Assert(InsideTask(), "SVN operation called from the UI thread");

      TryCommit(path, title, true);
    }
    public void Commit(string path, string title, bool majorRevision)
    {
      Trace.Assert(InsideTask(), "SVN operation called from the UI thread");

      TryCommit(path, title, majorRevision);
    }
    
    public void UpdateRevisionAndDate(string path, bool majorRevision, ref ArticleItem item)
    {
      SvnInfoEventArgs info;
      if (_client.GetInfo(path, out info))
      {
        item._revision = (int)info.LastChangeRevision;
        if (majorRevision)
        {
          item._majorRevision = item._revision;
          item._date = info.LastChangeTime.ToLocalTime();
        }
      }
    }

    // Write the content and tags to the working copy and commit the article
    public string CommitNewAttachment(ref string url, ref string path, string attachmentFile, bool majorRevision, System.ComponentModel.BackgroundWorker worker)
    {
      Trace.Assert(InsideTask(), "SVN operation called from the UI thread");

      // working copy root
      string workingRoot = FindWorkingRoot(url);
      if (string.IsNullOrEmpty(workingRoot))
      {
        MessageBox.Show("Article not sent - working copy not found.", "Colabo",
          MessageBoxButtons.OK, MessageBoxIcon.Error);
        return null;
      }

      // write the content to the file, add to the SVN if necessary
      if (path.Contains(_defaultFilename))
      {
        path = AddUniqueFile(path, Path.GetFileNameWithoutExtension(attachmentFile), workingRoot, new List<string>()/*empty tags*/);
        if (string.IsNullOrEmpty(path)) return null;
        url = _client.GetUriFromWorkingCopy(path).ToString();
      }
      else return path; // cannot happen, but better to be careful

      if (worker != null) worker.ReportProgress(30);

      // copy the content of original attachment file to the destination file
      File.Copy(attachmentFile, path, true /*overwrite*/);

      if (worker != null) worker.ReportProgress(60);

      // commit all changes to the SVN (call commit on the whole repository)
      TryCommit(workingRoot, "adding attachment", majorRevision);

      if (worker != null) worker.ReportProgress(90);

      return path;
    }

    public string SVNBenchmark()
    {
      string result = "Timing:\n";

      // find the working directory
      Stopwatch watch = Stopwatch.StartNew();
      string workingRoot = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
      if (string.IsNullOrEmpty(workingRoot)) return result;
      workingRoot += "\\Colabo";
      TimeSpan diff = watch.Elapsed;
      result += string.Format("  Working directory found in - {0} s\n", diff.TotalSeconds);    

      // check for SVN working copies
      List<string> paths = new List<string>();
      string[] dirs = Directory.GetDirectories(workingRoot, "????????-????-????-????-????????????");
      foreach (string dir in dirs)
      {
        result += BenchmarkDir(dir, ref paths);
      }

      // update of multiple items
      watch = Stopwatch.StartNew();
      SvnUpdateArgs updateArgs = new SvnUpdateArgs();
      updateArgs.Depth = SvnDepth.Empty;
      _client.Update(paths, updateArgs);
      diff = watch.Elapsed;
      result += string.Format("  Update of {0} items - {1} s\n", paths.Count, diff.TotalSeconds);

      return result;
    }
    private string BenchmarkDir(string path, ref List<string> paths)
    {
      string result = string.Format("  Directory {0}\n", Path.GetFileName(path));

      Stopwatch watch = Stopwatch.StartNew();
      Uri uri = _client.GetUriFromWorkingCopy(path);
      TimeSpan diff = watch.Elapsed;
      result += string.Format("    Uri found in - {0} s\n", diff.TotalSeconds);

      if (uri != null)
      {
        watch = Stopwatch.StartNew();
        _client.Info(path, delegate(object sender, SvnInfoEventArgs info) { });
        diff = watch.Elapsed;
        result += string.Format("    Info - {0} s\n", diff.TotalSeconds);

        watch = Stopwatch.StartNew();
        SvnInfoArgs args = new SvnInfoArgs();
        args.Revision = SvnRevision.Head;
        _client.Info(path, args, delegate(object sender, SvnInfoEventArgs info) { });
        diff = watch.Elapsed;
        result += string.Format("    Remote Info - {0} s\n", diff.TotalSeconds);

        watch = Stopwatch.StartNew();
        string root, uuid;
        GetRepositoryInfo(uri, out root, out uuid, true);
        diff = watch.Elapsed;
        result += string.Format("    DAV Info - {0} s\n", diff.TotalSeconds);

        watch = Stopwatch.StartNew();
        SvnLogArgs logArgs = new SvnLogArgs(new SvnRevisionRange(SvnRevision.Head, SvnRevision.Head));
        _client.Log(path, logArgs, delegate(object sender, SvnLogEventArgs info) { });
        diff = watch.Elapsed;
        result += string.Format("    Log - {0} s\n", diff.TotalSeconds);

        watch = Stopwatch.StartNew();
        SvnUpdateArgs updateArgs = new SvnUpdateArgs();
        updateArgs.Depth = SvnDepth.Empty;
        _client.Update(path, updateArgs);
        diff = watch.Elapsed;
        result += string.Format("    Update - {0} s\n", diff.TotalSeconds);

        paths.Add(path);
      }

      string[] dirs = Directory.GetDirectories(path);
      foreach (string dir in dirs)
      {
        if (Path.GetFileName(dir).Equals(".svn", StringComparison.OrdinalIgnoreCase)) continue;
        result += BenchmarkDir(dir, ref paths);
        break;
      }

      string[] files = Directory.GetFiles(path);
      int n = files.Length;
      if (n > 2) n = 2;
      for (int i=0; i<n; i++)
      {
        result += BenchmarkFile(files[i], ref paths);
        paths.Add(files[i]);
      }

      return result;
    }
    private string BenchmarkFile(string path, ref List<string> paths)
    {
      string result = string.Format("  File {0}\n", Path.GetFileName(path));

      Stopwatch watch = Stopwatch.StartNew();
      Uri uri = _client.GetUriFromWorkingCopy(path);
      TimeSpan diff = watch.Elapsed;
      result += string.Format("    Uri found in - {0} s\n", diff.TotalSeconds);

      if (uri != null)
      {
        long localRevision;

        watch = Stopwatch.StartNew();
        _client.Info(path, delegate(object sender, SvnInfoEventArgs info) 
        {
          localRevision = info.LastChangeRevision;
        });
        diff = watch.Elapsed;
        result += string.Format("    Info - {0} s\n", diff.TotalSeconds);

        Uri root;
        Guid guid;
        long remoteRevision;

        watch = Stopwatch.StartNew();
        _client.Info(uri, delegate(object sender, SvnInfoEventArgs info)
        {
          root = info.RepositoryRoot;
          guid = info.RepositoryId;
          remoteRevision = info.LastChangeRevision;
        });
        diff = watch.Elapsed;
        result += string.Format("    Remote Info - total {0} s\n", diff.TotalSeconds);

        watch = Stopwatch.StartNew();
        string rootPath, uuid;
        GetRepositoryInfo(uri, out rootPath, out uuid, false);
        diff = watch.Elapsed;
        result += string.Format("    DAV Info - {0} s\n", diff.TotalSeconds);

        watch = Stopwatch.StartNew();
        _client.Info(uri, delegate(object sender, SvnInfoEventArgs info)
        {
          root = info.RepositoryRoot;
          guid = info.RepositoryId;
        });
        diff = watch.Elapsed;
        result += string.Format("    Remote Info - root + guid {0} s\n", diff.TotalSeconds);

        watch = Stopwatch.StartNew();
        _client.Info(uri, delegate(object sender, SvnInfoEventArgs info)
        {
          remoteRevision = info.LastChangeRevision;
        });
        diff = watch.Elapsed;
        result += string.Format("    Remote Info - revision {0} s\n", diff.TotalSeconds);

        watch = Stopwatch.StartNew();
        root = _client.GetRepositoryRoot(uri);
        _client.TryGetRepositoryId(uri, out guid);
        diff = watch.Elapsed;
        result += string.Format("    Repository root + ID - {0} s\n", diff.TotalSeconds);

        watch = Stopwatch.StartNew();
        SvnLogArgs logArgs = new SvnLogArgs(new SvnRevisionRange(SvnRevision.Head, SvnRevision.One));
        _client.Log(path, logArgs, delegate(object sender, SvnLogEventArgs info)
        {
          info.Cancel = true;
        });
        diff = watch.Elapsed;
        result += string.Format("    Log - {0} s\n", diff.TotalSeconds);

        watch = Stopwatch.StartNew();
        _client.Update(path);
        diff = watch.Elapsed;
        result += string.Format("    Update - {0} s\n", diff.TotalSeconds);

        paths.Add(path);
      }

      return result;
    }
  }
}
