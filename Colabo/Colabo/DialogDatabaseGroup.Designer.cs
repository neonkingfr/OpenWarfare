namespace Colabo
{
  partial class DialogDatabaseGroup
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.buttonOK = new System.Windows.Forms.Button();
      this.buttonCancel = new System.Windows.Forms.Button();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.valueAccessRights = new System.Windows.Forms.TextBox();
      this.valueName = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.valueDescription = new System.Windows.Forms.TextBox();
      this.valueLevel = new System.Windows.Forms.NumericUpDown();
      this.valueHomePage = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.valueLevel)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.valueHomePage)).BeginInit();
      this.SuspendLayout();
      // 
      // buttonOK
      // 
      this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.buttonOK.Location = new System.Drawing.Point(180, 216);
      this.buttonOK.Name = "buttonOK";
      this.buttonOK.Size = new System.Drawing.Size(75, 23);
      this.buttonOK.TabIndex = 11;
      this.buttonOK.Text = "OK";
      this.buttonOK.UseVisualStyleBackColor = true;
      this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
      // 
      // buttonCancel
      // 
      this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.buttonCancel.Location = new System.Drawing.Point(261, 216);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.Size = new System.Drawing.Size(75, 23);
      this.buttonCancel.TabIndex = 10;
      this.buttonCancel.Text = "Cancel";
      this.buttonCancel.UseVisualStyleBackColor = true;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 118);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(78, 13);
      this.label2.TabIndex = 9;
      this.label2.Text = "Access Rights:";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 12);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(38, 13);
      this.label1.TabIndex = 8;
      this.label1.Text = "Name:";
      // 
      // valueAccessRights
      // 
      this.valueAccessRights.AcceptsReturn = true;
      this.valueAccessRights.AcceptsTab = true;
      this.valueAccessRights.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueAccessRights.Location = new System.Drawing.Point(96, 115);
      this.valueAccessRights.Multiline = true;
      this.valueAccessRights.Name = "valueAccessRights";
      this.valueAccessRights.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.valueAccessRights.Size = new System.Drawing.Size(240, 95);
      this.valueAccessRights.TabIndex = 7;
      // 
      // valueName
      // 
      this.valueName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueName.Location = new System.Drawing.Point(96, 9);
      this.valueName.Name = "valueName";
      this.valueName.Size = new System.Drawing.Size(240, 20);
      this.valueName.TabIndex = 6;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(12, 38);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(63, 13);
      this.label3.TabIndex = 13;
      this.label3.Text = "Description:";
      // 
      // valueDescription
      // 
      this.valueDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueDescription.Location = new System.Drawing.Point(96, 35);
      this.valueDescription.Name = "valueDescription";
      this.valueDescription.Size = new System.Drawing.Size(240, 20);
      this.valueDescription.TabIndex = 12;
      // 
      // valueLevel
      // 
      this.valueLevel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueLevel.Location = new System.Drawing.Point(96, 62);
      this.valueLevel.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
      this.valueLevel.Name = "valueLevel";
      this.valueLevel.Size = new System.Drawing.Size(239, 20);
      this.valueLevel.TabIndex = 14;
      // 
      // valueHomePage
      // 
      this.valueHomePage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueHomePage.Location = new System.Drawing.Point(96, 89);
      this.valueHomePage.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
      this.valueHomePage.Name = "valueHomePage";
      this.valueHomePage.Size = new System.Drawing.Size(238, 20);
      this.valueHomePage.TabIndex = 15;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(12, 64);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(64, 13);
      this.label4.TabIndex = 16;
      this.label4.Text = "Group level:";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(12, 91);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(65, 13);
      this.label5.TabIndex = 17;
      this.label5.Text = "Home page:";
      // 
      // DialogDatabaseGroup
      // 
      this.AcceptButton = this.buttonOK;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.buttonCancel;
      this.ClientSize = new System.Drawing.Size(348, 251);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.valueHomePage);
      this.Controls.Add(this.valueLevel);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.valueDescription);
      this.Controls.Add(this.buttonOK);
      this.Controls.Add(this.buttonCancel);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.valueAccessRights);
      this.Controls.Add(this.valueName);
      this.MinimumSize = new System.Drawing.Size(250, 210);
      this.Name = "DialogDatabaseGroup";
      this.Text = "Database Group";
      this.Load += new System.EventHandler(this.DialogDatabaseGroup_Load);
      ((System.ComponentModel.ISupportInitialize)(this.valueLevel)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.valueHomePage)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button buttonOK;
    private System.Windows.Forms.Button buttonCancel;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox valueAccessRights;
    private System.Windows.Forms.TextBox valueName;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox valueDescription;
    private System.Windows.Forms.NumericUpDown valueLevel;
    private System.Windows.Forms.NumericUpDown valueHomePage;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
  }
}