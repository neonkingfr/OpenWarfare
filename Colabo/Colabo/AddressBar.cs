using System;
using System.Drawing;
using System.Windows.Forms;


namespace Colabo
{
  public class AddressBar : ToolStripTextBox
  {
    private bool _gotFocus;

    private Autocomplete _autocomplete = null;

    public AddressBar()
    {
      KeyDown +=new KeyEventHandler(OnKeyDown);
      KeyUp += new KeyEventHandler(OnKeyUp);
      GotFocus += new EventHandler(OnGotFocus);
      LostFocus += new EventHandler(OnLostFocus);
      Click += new EventHandler(OnClick);
      BackColor = SystemColors.Window;
    }

    public void SetAutocomplete(Autocomplete autocomplete)
    {
      _autocomplete = autocomplete;
    }

    public override Size GetPreferredSize(Size constrainingSize)
    {
      // Use the default size if the text box is on the overflow menu
      // or is on a vertical ToolStrip.
      if (IsOnOverflow || Owner.Orientation == Orientation.Vertical)
      {
        return DefaultSize;
      }

      // Declare a variable to store the total available width as 
      // it is calculated, starting with the display width of the 
      // owning ToolStrip.
      Int32 width = Owner.DisplayRectangle.Width;

      // Subtract the width of the overflow button if it is displayed. 
      if (Owner.OverflowButton.Visible)
      {
        width = width - Owner.OverflowButton.Width -
                Owner.OverflowButton.Margin.Horizontal;
      }

      // Declare a variable to maintain a count of ToolStripSpringTextBox 
      // items currently displayed in the owning ToolStrip. 
      Int32 springBoxCount = 0;

      foreach (ToolStripItem item in Owner.Items)
      {
        // Ignore items on the overflow menu.
        if (item.IsOnOverflow) continue;

        if (item is AddressBar)
        {
          // For ToolStripSpringTextBox items, increment the count and 
          // subtract the margin width from the total available width.
          springBoxCount++;
          width -= item.Margin.Horizontal;
        }
        else
        {
          // For all other items, subtract the full width from the total
          // available width.
          width = width - item.Width - item.Margin.Horizontal;
        }
      }

      // If there are multiple ToolStripSpringTextBox items in the owning
      // ToolStrip, divide the total available width between them. 
      if (springBoxCount > 1) width /= springBoxCount;

      // If the available width is less than the default width, use the
      // default width, forcing one or more items onto the overflow menu.
      if (width < DefaultSize.Width) width = DefaultSize.Width;

      // Retrieve the preferred size from the base class, but change the
      // width to the calculated width. 
      Size size = base.GetPreferredSize(constrainingSize);
      size.Width = width;
      return size;
    }

    public enum ProcessShortcutResult
    {
      NotProcessed,   // Not processed (unknown) shortcut
      Processed,      // Shortcut was processed, don't process again
      PassToHandler   // Know shortcut, not processed, pass them to main key handler
    }

    // Try and process shortcuts
    public ProcessShortcutResult ProcessShortcut(Keys keyData)
    {
      // Test for keys without modifiers
      switch (keyData)
      {
        case Keys.Delete:
        case Keys.Back:
        return ProcessShortcutResult.PassToHandler;
      }
      // Test for keys with Control modifier
      if ((keyData & Keys.Modifiers) == Keys.Control)
        switch (keyData & Keys.KeyCode)
        {
          case Keys.C:
          case Keys.Insert:
            Copy();
            return ProcessShortcutResult.Processed;
          case Keys.X:
          case Keys.Delete:
            // Cut don't delete selection!
            //Cut();
            // Hack to functional Cut
            if (SelectedText.Length > 0)
            {
              Copy();
              int selStart = SelectionStart;
              Text = Text.Remove(SelectionStart, SelectionLength);
              SelectionStart = selStart;
            }
            return ProcessShortcutResult.Processed;
          case Keys.V:
            Paste();
            return ProcessShortcutResult.Processed;
        }
      // Test for keys with Shift modifier
      if ((keyData & Keys.Modifiers) == Keys.Shift)
        switch (keyData & Keys.KeyCode)
        {
          case Keys.Insert:
            Paste();
            return ProcessShortcutResult.Processed;
          case Keys.Delete:
            if (SelectedText.Length > 0)
            {
              Copy();
              int selStart = SelectionStart;
              Text = Text.Remove(SelectionStart, SelectionLength);
              SelectionStart = selStart;
            }
            return ProcessShortcutResult.Processed;
        }
      // Pass all basic keys
      string s = keyData.ToString();
      if (s.Length == 1)
      {
        // Keys with no modifier
        if ((keyData & Keys.Modifiers) == Keys.None)
          if (Char.IsNumber(s, 0) || Char.IsLetter(s, 0))
            return ProcessShortcutResult.PassToHandler;
        // Keys with shift modifier
        if ((keyData & Keys.Modifiers) == Keys.Shift)
          if (Char.IsNumber(s, 0) || Char.IsLetter(s, 0))
            return ProcessShortcutResult.PassToHandler;
      }
      // Key not processed
      return ProcessShortcutResult.NotProcessed;
    }


    private void OnKeyDown(object sender, KeyEventArgs e)
    {
      if (_autocomplete != null) _autocomplete.OnKeyDown(e);
    }
    private void OnKeyUp(object sender, KeyEventArgs e)
    {
      if (_autocomplete != null) _autocomplete.OnKeyUp(e);
    }

    void OnGotFocus(object sender, EventArgs e)
    {
      _gotFocus = true;
      if (SelectedText.Length == 0)
        SelectAll();
    }
    void OnLostFocus(object sender, EventArgs e)
    {
      _gotFocus = false;
      if (_autocomplete != null) _autocomplete.OnLostFocus(e);
    }
    void OnClick(object sender, EventArgs e)
    {
      // Select all at first click, prevent for repeating selection on next click
      if (_gotFocus)
      {
        if (SelectedText.Length == 0)
          SelectAll();
        _gotFocus = false;
      }
    }

  }

}
