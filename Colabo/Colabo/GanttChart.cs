using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Colabo
{
  public partial class DialogGanttChart : Form
  {
    public DialogGanttChart()
    {
      InitializeComponent();

      ganttChart.FromDate = DateTime.Today;
      ganttChart.ToDate = DateTime.Today + TimeSpan.FromDays(10);

      ganttChart.AddChartBar("Task1", null, DateTime.Now, DateTime.Now + TimeSpan.FromDays(3), Color.LightGreen, Color.Green, 0);
      ganttChart.AddChartBar("Task2", null, DateTime.Now, DateTime.Now + TimeSpan.FromDays(5), Color.LightBlue, Color.Blue, 1);
      ganttChart.AddChartBar("Task3", null, DateTime.Now, DateTime.Now + TimeSpan.FromDays(8), Color.LightPink, Color.Pink, 2);
    }

    private Point _movingPos = Point.Empty;
    private void ganttChart_MouseDown(object sender, MouseEventArgs e)
    {
      if ((e.Button & MouseButtons.Left) != 0) _movingPos = e.Location;
    }
    private void ganttChart_MouseMove(object sender, MouseEventArgs e)
    {
      if (_movingPos != Point.Empty)
      {
        float dist = (float)(e.Location.X - _movingPos.X) / (float)ganttChart.Width;
        TimeSpan diff = ganttChart.ToDate - ganttChart.FromDate;
        double result = dist * diff.TotalDays;
        ganttChart.FromDate += TimeSpan.FromDays(result);
        ganttChart.ToDate += TimeSpan.FromDays(result);
        ganttChart.Invalidate();

        _movingPos = e.Location;
      }
    }
    private void ganttChart_MouseUp(object sender, MouseEventArgs e)
    {
      if ((e.Button & MouseButtons.Left) != 0) _movingPos = Point.Empty;
    }
  }
}