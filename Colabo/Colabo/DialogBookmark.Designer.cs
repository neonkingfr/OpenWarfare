namespace Colabo
{
  partial class DialogBookmark
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogBookmark));
      this.valueName = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.valueImage = new System.Windows.Forms.ComboBox();
      this.label2 = new System.Windows.Forms.Label();
      this.valueTags = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.valueArticle = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.buttonCancel = new System.Windows.Forms.Button();
      this.buttonOK = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // valueName
      // 
      this.valueName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueName.Location = new System.Drawing.Point(57, 13);
      this.valueName.Name = "valueName";
      this.valueName.Size = new System.Drawing.Size(225, 20);
      this.valueName.TabIndex = 0;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 16);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(30, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Title:";
      // 
      // valueImage
      // 
      this.valueImage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueImage.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.valueImage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.valueImage.FormattingEnabled = true;
      this.valueImage.Location = new System.Drawing.Point(57, 40);
      this.valueImage.Name = "valueImage";
      this.valueImage.Size = new System.Drawing.Size(224, 21);
      this.valueImage.TabIndex = 2;
      this.valueImage.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.valueImage_DrawItem);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 43);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(39, 13);
      this.label2.TabIndex = 3;
      this.label2.Text = "Image:";
      // 
      // valueTags
      // 
      this.valueTags.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueTags.Location = new System.Drawing.Point(57, 68);
      this.valueTags.Name = "valueTags";
      this.valueTags.Size = new System.Drawing.Size(225, 20);
      this.valueTags.TabIndex = 4;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(12, 71);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(34, 13);
      this.label3.TabIndex = 5;
      this.label3.Text = "Tags:";
      // 
      // valueArticle
      // 
      this.valueArticle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueArticle.Location = new System.Drawing.Point(57, 95);
      this.valueArticle.Name = "valueArticle";
      this.valueArticle.Size = new System.Drawing.Size(225, 20);
      this.valueArticle.TabIndex = 6;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(12, 98);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(39, 13);
      this.label4.TabIndex = 7;
      this.label4.Text = "Article:";
      // 
      // buttonCancel
      // 
      this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.buttonCancel.Location = new System.Drawing.Point(207, 125);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.Size = new System.Drawing.Size(75, 23);
      this.buttonCancel.TabIndex = 8;
      this.buttonCancel.Text = "Cancel";
      this.buttonCancel.UseVisualStyleBackColor = true;
      // 
      // buttonOK
      // 
      this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.buttonOK.Location = new System.Drawing.Point(126, 124);
      this.buttonOK.Name = "buttonOK";
      this.buttonOK.Size = new System.Drawing.Size(75, 23);
      this.buttonOK.TabIndex = 9;
      this.buttonOK.Text = "OK";
      this.buttonOK.UseVisualStyleBackColor = true;
      // 
      // DialogBookmark
      // 
      this.AcceptButton = this.buttonOK;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.buttonCancel;
      this.ClientSize = new System.Drawing.Size(294, 160);
      this.Controls.Add(this.buttonOK);
      this.Controls.Add(this.buttonCancel);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.valueArticle);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.valueTags);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.valueImage);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.valueName);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximumSize = new System.Drawing.Size(640, 194);
      this.MinimumSize = new System.Drawing.Size(194, 194);
      this.Name = "DialogBookmark";
      this.Text = "Bookmark";
      this.Load += new System.EventHandler(this.DialogBookmark_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox valueName;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ComboBox valueImage;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox valueTags;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox valueArticle;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Button buttonCancel;
    private System.Windows.Forms.Button buttonOK;
  }
}