﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;
using System.IO;
using Jayrock.Json;
using Jayrock.Json.Conversion;
using System.Threading;
using System.Linq;

namespace Coma
{
    /// <summary>
    /// COMA (ColaboMarkdown) is a text-to-HTML conversion tool (created exclusively for Colabo).
    /// Construct it with a COMA-formatted string, then read the resultant HTML and parsing info.
    /// </summary>
    public partial class ComaParser
    {
        // Source (COMA).
        private string _input;
        public string input { get { return _input; } }
        // Result (HTML).
        private string _output;
        public string output { get { return _output; } }
        // A table for converting input positions to output positions. -1 if a *guaranteed visible conversion* doesn't exist.
        // TODO: this can only be an interval-to-interval table
        // TODO: implement this!
        private int[] _i2o;
        public int[] inputToOutput { get { return _i2o; } }

        public delegate void CompleteTranslate(String translation);
        public delegate void TranslateCall(String urlTranslate, CompleteTranslate complete, String langCode, String source, String srcLang);

        /**
        Batch translation, cache translation results
        */
        private class TranslateCache
        {
          private class ItemName: IEquatable<ItemName>
          {
            public string urlTranslate;
            public String tgtLang;
            public String source;
            public String srcLang;

            public bool Equals(ItemName other)
            {
              if (other == null)
                return false;

              if (this.source != other.source) return false;
              if (this.tgtLang != other.tgtLang) return false;
              if (this.srcLang != other.srcLang) return false;
              if (this.urlTranslate != other.urlTranslate) return false;
              return true;
            }

            public override bool Equals(Object other)
            {
              if (other == null) return base.Equals(other); 
              if (other is ItemName) return this.Equals((ItemName)other);
              return false;
            }

            public override int GetHashCode()
            {
              return source.GetHashCode();
            }
          };
          private class Item
          {
            public LinkedListNode<ItemName> inList;
            public String translation;
            public bool done;
            public List<CompleteTranslate> complete;

            public Item(CompleteTranslate complete)
            {
              done = false;
              this.complete = new List<CompleteTranslate>();
              this.complete.Add(complete);
            }
            public void Done(String translation)
            {
              this.translation = translation;
              done = true;
            }
          };
          Dictionary<ItemName,Item> cache_;
          LinkedList<ItemName> lru_;
          List<ItemName> pending_;
          int pendingSize_;

          public TranslateCache()
          {
            cache_ = new Dictionary<ItemName,Item>();
            lru_ = new LinkedList<ItemName>();
            pending_ = new List<ItemName>();
            pendingSize_ = 0;
          }
          public void Translate(string urlTranslate, CompleteTranslate complete, String tgtLang, String source, String srcLang)
          {
            ItemName name = new ItemName();
            name.urlTranslate = urlTranslate;
            name.tgtLang = tgtLang;
            name.source = source;
            name.srcLang = srcLang;
            Item newItem = null;

            lock (cache_)
            {
              Item value;
              if (cache_.TryGetValue(name, out value))
              {
                // note: if the item is there, it might still be pending - in such case add callback only
                if (!value.done)
                {
                  value.complete.Add(complete);
                }
                else
                {
                  complete(value.translation);
                }
                // if item is already here, push it to last
                if (value.inList!=null)
                {
                  lru_.Remove(value.inList);
                  lru_.AddLast(value.inList);
                }
                else
                {
                  // should not happen - should be in list
                  value.inList = lru_.AddLast(name);
                  cache_.Add(name, newItem);
                }
              }
              else
              {
                // wrap complete so that it inserts into the cache as well
                newItem = new Item(complete);
                newItem.inList = lru_.AddLast(name);
                cache_.Add(name, newItem);
              }
              if (lru_.Count > 500)
              {
                LinkedListNode<ItemName> first = lru_.First;
                cache_.Remove(first.Value);
                lru_.Remove(first);
              }
            }

            if (newItem != null)
            {
              if (pending_.Count>0 && (pending_[0].srcLang!=name.srcLang || pending_[0].tgtLang!=name.tgtLang || pending_[0].urlTranslate!=name.urlTranslate))
              {
                FlushAll(); // aggregate only the same language translation
              }
              bool flush = false;
              lock (cache_)
              {
                pendingSize_ += source.Length;
                pending_.Add(name);
                const int autoFlush = 1000;
                flush = pendingSize_ > autoFlush;
              }
              if (flush) FlushAll();
            }
          }

          struct ItemWithName
          {
            public ItemName name;
            public Item value;
          }
          internal void FlushAll()
          {
            List<ItemWithName> pending = new List<ItemWithName>();
            lock (cache_)
            {
              foreach (ItemName name in pending_)
              {
                Item value;
                if (cache_.TryGetValue(name, out value))
                {
                  ItemWithName item = new ItemWithName();
                  item.name = name;
                  item.value = value;
                  pending.Add(item);
                }
              }
              pendingSize_ = 0;
              pending_.Clear();
            }
            if (pending.Count == 0) return;
            String srcLang = pending[0].name.srcLang;
            String tgtLang = pending[0].name.tgtLang;
            String urlTranslate = pending[0].name.urlTranslate;
            List<ToTranslate> toTranslate = new List<ToTranslate>();
            foreach (ItemWithName itemR in pending)
            {
              ItemWithName item = itemR;
              CompleteTranslate completeToCache = delegate(String translation)
              {
                item.value.translation = translation;
                foreach (CompleteTranslate complete in item.value.complete)
                {
                  complete(translation);
                }
                item.value.complete.Clear();
                item.value.done = true;
              };
              ToTranslate toItem = new ToTranslate();
              toItem.complete = completeToCache;
              toItem.source = item.name.source;
              toTranslate.Add(toItem);
            }
            TranslationAPI.Translate(urlTranslate, toTranslate, tgtLang, srcLang);
          }
        }


        private static TranslateCache _translate;

        public List<string> _languages;
        // Base for the relative links
        string _urlBase;

        // delegate types to allow functions in functions
        private delegate void InlineFunction();
        private delegate bool BoolInlineFunction();
        private delegate void InlineFunctionString(string s);

        public static string Transform(string urlBase, string urlTranslate, string text, List<string> languages)
        {
          if (string.IsNullOrEmpty(text)) return "";

          Coma.ComaParser comaParser = new Coma.ComaParser(urlBase, urlTranslate, text, languages);
          return comaParser.output;
        }

      
        // Transform COMA-formatted text into HTML.
        public ComaParser(string urlBase, string urlTranslate, string input, List<string> languages)
        {
            _urlBase = urlBase;
            _input = input;
            _languages = languages;
            if (_translate==null) _translate = new TranslateCache();
            _i2o = new int[_input.Length];
            articleParts = new List<ArticlePart>();
            nesting = new List<string>();  // List of row prefixes.
            links = new List<Link>();
            targets = new Dictionary<string, Target>(StringComparer.InvariantCultureIgnoreCase);
            markersWanted = new int[(int)(Markers.LAST)+1];
            maybeMarkers = new int[(int)(Markers.LAST)+1];

            step1 = Preprocess();         // Do simple text transformations to simplify the parser.
            position = 0;

            // Main part: convert text into `articleParts` with inline-level constructs already converted to HTML.
            while (NextLine() == true) { }

            _output = Postprocess(urlTranslate);      // Gather targets from `articleParts`. Convert `articleParts` into block-level HTML code.
        }

        /// <summary>
        /// Preprocess the input. Keep track of input -> output positions.
        ///  * Turn newlines (`\r\n`, `\r` and `\n`) into `\n`. Throw away spaces before them.
        ///  * Convert tabs (`\t`) into single spaces.
        ///  * Throw away the remaining characters between 0 and 1Fh. These are for internal use.
        /// </summary>
        /// TODO: correct tabs
        /// TODO: Invalid characters should be kept and processed in the parser (but not let through)
        ///       - Why? ... for the cursor and ins/del
        private string Preprocess()
        {
            StringBuilder res = new StringBuilder(input.Length);

            for (int i = 0, o = 0, spaces = 0; i < _input.Length; i++) {
                _i2o[i] = -1;
                switch (_input[i]) {
                    case ' ':
                    case '\t': spaces++; break;
                    case '\r': if (i < _input.Length - 1 && _input[i + 1] == '\n') i++; goto case '\n';
                    case '\n': spaces = 0; _i2o[i] = o++; res.Append('\n'); break;
                    default:
                        if (_input[i] <= (char)(Markers.LAST) || _input[i] >= '\x20') {  // let markers through
                            while (spaces != 0) { spaces--; _i2o[i - spaces - 1] = o++; res.Append(' '); }
                            _i2o[i] = o++; res.Append(_input[i]);
                        }
                        break;
                }
            }
            res.Append('\n');  // append a newline so that no markers are at the very end
            return res.ToString();
        }

        // Markers.
        enum Markers { SELECTION_OPEN, SELECTION_CLOSE, INS_OPEN, INS_CLOSE, DEL_OPEN, DEL_CLOSE, LAST = DEL_CLOSE };
        private static readonly string[] markersString = { "<ins class=\"selection\"><a name=\"cursor\"></a>", "</ins>", "<ins>", "</ins>", "<del>", "</del>" };
        int[] markersWanted, maybeMarkers;

        // Insert any markers from the last Munch(). Should be called *before* pasting the Munch result.
        private void DoOpeningMarkers() {
            for (int i=0; i<=(int)(Markers.LAST); i+=2) {
                while (markersWanted[i] > 0) {
                    markersWanted[i]--;
                    par.Append(markersString[i]);
                }
            }
        }
        private void DoClosingMarkers() {
            for (int i=1; i<=(int)(Markers.LAST); i+=2) {
                while (markersWanted[i] > 0) {
                    markersWanted[i]--;
                    par.Append(markersString[i]);
                }
            }
        }

        // TODO: refactor parser state into a private class ParserState
        // TODO: all `step1[position]` functions should be encapsulated into a new Input class (partially done)
        // TODO: refactor the big switch into functions
        // TODO: all static data should be static
        // TODO: use `word` less often

        enum PartType {
            ITEM_MINUS, ITEM_PLUS, ITEM_STAR,
            ITEM_NUMBER, ITEM_LETTER, ITEM_ROMAN,
            CITATION,
            ITEM_AUTO,
            NOTE, LABEL,
            PARAGRAPH, PARAGRAPH_ENDSWITHPRE, VERBATIM_HTML, VERTICAL_SPACE, HORIZONTAL_RULE,
            UNDERLINED_HEADER, HEADER_HASH, HEADER_EQUALS,
            NESTING_LEVEL_START,
            INVALID,
        };
        private static readonly string[] PartHTMLStart = {
            "<ul style=\"list-style-type:square\"", "<ul style=\"list-style-type:circle\"", "<ul style=\"list-style-type:disc\"",
            "<ol style=\"list-style-type:decimal\" start=\"", "<ol style=\"list-style-type:lower-alpha\" start=\"", "<ol style=\"list-style-type:upper-roman\" start=\"",
            "<div class=\"cite\">",
        };
        private static readonly string[] PartHTMLEnd = {
            "</li></ul>", "</li></ul>", "</li></ul>",
            "</li></ol>", "</li></ol>", "</li></ol>", 
            "</div>",
        };
        private class ArticlePart
        {
            public PartType type;
            public string s;          // text in immediate representation 2: links are hashes, the rest is already converted to HTML
            public int number;        // multi-purpose number: par indentation, vertical space width, header level, list_item number...
            public int nestingLevel;  // depth in block structure
            // public int[] i2o;  // TODO: relative input position to output position

            public ArticlePart(PartType type, string s, int number, int nestingLevel) { this.s = s; this.type = type; this.number = number; this.nestingLevel = nestingLevel; }
        }

        // Code.

        // All code languages that `google-code-prettify` supports.
        // Possible extensions: Lisp, CSS, Haskell, Lua, OCAML, SML, F#, Visual Basic, SQL, Protocol Buffers, WikiText
        enum CodeLanguage { AUTO, AUTO_2, NONE, NONE_2, NO_TYPEWRITER, NO_TYPEWRITER_2, BSH, C, CC, CPP, CS, CSH, CYC, CV, HTM, HTML, JAVA, JS, M, MXML, PERL, PL, PM, PY, RB, SH, XHTML, XML, XSL, UNSUPPORTED_LANGUAGE }
        private static readonly string[] codeLanguageString = { "auto",         "a",            "none", "n", "text",  "t",    "bsh", "c", "cc", "cpp", "cs", "csh", "cyc", "cv", "htm", "html", "java", "js", "m", "mxml", "perl", "pl", "pm", "py", "rb", "sh", "xhtml", "xml", "xsl" };
        private static readonly string[] codeLanguageHTML =   { " prettyprint", " prettyprint", "",     "",  " nott", " nott" };  // the rest is ` class="prettyprint lang-{codeLanguageString}"`

        /// <summary>Test whether `candidate` is a language name.</summary>
        CodeLanguage ParseCodeLanguages(string candidate)
        {
            if (candidate.Length > 0) { // early exit for ``
                for (int i = 0; i < codeLanguageString.Length; i++) {
                    if (codeLanguageString[i].Equals(candidate, StringComparison.InvariantCultureIgnoreCase))
                        return (CodeLanguage)i;
                }
            }
            return CodeLanguage.UNSUPPORTED_LANGUAGE;
        }

        /// <summary>Return a HTML string starting CODE or PRE with `language`</summary>
        private string HTMLStartCodeWithLanguage(ConstructType c, int language)
        {
            if (language < codeLanguageHTML.Length)
                return HTMLStart[(int)c] + codeLanguageHTML[language] + "\">";
            else
                return HTMLStart[(int)c] + " prettyprint lang-" + codeLanguageString[language] + "\">";
        }


        // Automatic link starts.
        private static readonly string autoLinkPossibleStarts = "hfnmw";
        private static readonly string[] autoLinkStart = { "http://", "https://", "ftp://", "news:", "mailto:", "file://", "www." };

        // Smileys are in ComaSmileys.cs

        // Sequences
        // There are also context-sensitive sequences (processed separately).
        private static readonly string sequencesPossibleStarts = "+<>=:.- ,^";
        private static readonly string[] sequence = {
            "+-","&plusmn;", "<=","&le;", ">=","&ge;", "=>","&rArr;", "<>","&ne;", 
            "...","&hellip;", "<--","&larr;", "<->","&harr;",
            "-->","&rarr;", " -->"," &rarr;", "---","&mdash;", "--","&ndash;",
            " ---","&nbsp;&mdash;", " --","&nbsp;&ndash;", " - ","&nbsp;&ndash; ",
            " & ","&nbsp;&amp;&nbsp;", " : ","&nbsp;:&nbsp;", ",-",",&ndash;",
            ":C:","&copy;", ":R:","&reg;", ":TM:","&trade;",
            ":+:","&dagger;", ":++:","&Dagger;",
            ":o:","&deg;", ":':","&prime;", ":\":","&Prime;",
            ":.:","&middot;", ":%:","&permil;", ":<:","&lsaquo;", ":>:","&rsaquo;", ":<<:","&laquo;", ":>>:","&raquo;",
            ":_:","&thinsp;",
            "^2","&sup2;","^3","&sup3;",
        };

        // Emphasis types.
        // TODO: Czech „quotes“ are implicit. But “these” should be used in English text.
        private const int EMPHASES = 6;
        enum EmphasisType { BOLD, ITALICS, TYPEWRITER, SMALLCAPS, DOUBLE_QUOTES, SINGLE_QUOTES };
        enum ConstructType { DELETE_START, DELETE_END, // <- these can only appear in step2, used to mask out invisible notes
            CODE = EMPHASES, PRE,  // skip the newline (0xA)!
            LINK = 0xB, DQUOTE_PLACEHOLDER, SQUOTE_PLACEHOLDER };
        private static readonly string emphasisChars = "*/|%\"'";
        // these are put in the output when we don't know yet whether they mean "emphasis"; may be overwritten later
        private static readonly string[] emphasisPlaceholders = { "*", "/", "|", "%", ((char)ConstructType.DQUOTE_PLACEHOLDER).ToString(), ((char)ConstructType.SQUOTE_PLACEHOLDER).ToString() };

        private static readonly string[] HTMLStart    = { "<b>", "<i>", "<tt>", "<span class=\"sc\">", "&bdquo;", "&sbquo;", "<code class=\"", "</p><pre class=\"" };
        private static readonly string[] HTMLContinue = { "<b>", "<i>", "<tt>", "<span class=\"sc\">", "", "",               "<code class=\"", "</p><pre class=\"" };
        private static readonly string[] HTMLEnd      = { "</b>", "</i>", "</tt>", "</span>",         "&ldquo;", "&lsquo;", "</code>", "</pre><p>" };
        private static readonly string[] HTMLPause    = { "</b>", "</i>", "</tt>", "</span>",         "", "",              "</code>", "</pre><p>" };

        // Underline levels (for headers or horizontal rules).
        private static readonly string underlineChars = "_%*=-";  // _ is always a horizontal rule

        // Non-parsed item starts
        private static readonly string itemPossibleStarts = "-+*#";
        
        // TODO: Some of these should really be their own functions/classes.
        enum ParserState { LINE_START, NORMAL, SINGLECHAR_ESCAPE, MULTICHAR_ESCAPE, CODE_BACKTICK, CODE_PAREN,
                           HTML_TAG, HTML_LONG, HTML_ENTITY, HTML_ENTITY_HEX, HTML_ENTITY_NUM, AUTOMATIC_LINK,
                           ITEM_NUMBER, ITEM_LETTER, ITEM_ROMAN, TARGET, };

        // At first, targets (notes, labels, references) are kept in `articleParts`.
        // In post-processing they are collected into `targets`.
        enum TargetType { NOTE, REFERENCE, LABEL };
        class Target
        {
            public TargetType type;
            
            // for references
            public string url;
            public string tooltip;

            // for notes
            public int startPosition, endPosition;  // which substring of the pre-output it is
            public string res;                      // the copy of the substring
            public bool pasted;                     // was it already pasted?
            public bool hidden;                     // should it be hidden? (i.e. do ?[links] point to it?)
            
            public Target(TargetType type) { this.type = type; }
            public Target(TargetType type, int startPosition) { this.type = type; this.startPosition = startPosition; this.endPosition = -1; }
            public Target(TargetType type, string[] url_tooltip) { this.type = type; this.url = url_tooltip[0]; this.tooltip = (url_tooltip.Length > 1 ? url_tooltip[1] : url_tooltip[0]); }
        }
        Dictionary<string, Target> targets;    // All notes, labels and references. Keys are not case-sensitive.

        // Links are kept in `links`. They are referenced by `(char)ConstructType.Link` placeholders in the processed text.
        enum LinkType { NORMAL, HERE, LEFT, RIGHT, LOCAL, TOGGLE, HERE_UNLESS_PASTED };  // nothing[], ![], <-[], ->[], ^[], ?[], (implicit before notes)
        private static readonly string[] linkStart = { "[", "![", "<-[", "->[", "^[", "?[" };
        private static readonly string[] linkTypeClass = { "", "here", "left", "right", "", "", "here" };
        class Link
        {
            public LinkType type;
            public string text;      // link text or alt-text: HTML is already escaped
            public string target;    // reference name or URL
            public string original;  // what will be displayed if the reference doesn't exist
            public Link(LinkType type, string text, string target, string original) { this.type = type; this.text = text; this.target = target; this.original = original; }
        }
        List<Link> links;

        // PARSER STATE

        string step1;                      // Preprocessed string and the current position inside it.
        int position;

        List<ArticlePart> articleParts;    // All article parts, along with their metadata.

        List<string> nesting;              // What a line must start with to keep the current nesting level.

        StringBuilder word;                // Current word. Also used for other word-like objects, like multi-char escapes, HTML tags, code...
        StringBuilder par;                 // Current paragraph.

        bool[] doubleEmphasisOn;           // Is double-char emphasis currently on?
        int[] emphasisStartCandidate;      // Position in `par` of the last single-char emphasis start. -1 = none
        int[] maybeEmphasisStart;          // Last occurrence of an emphasis char that might have started emphasis. -1 = none
        int[] maybeEmphasisEnd;            // Last occurrence of an emphasis char that might have ended emphasis. -1 = none

        string lastMatch;                  // Last match of MunchIf (or MunchIfAny). Very useful!

        /// <summary>Called at line start, returns the maximum possible nesting level of this line (number of matched and skipped prefixes).</summary>
        private int NestingLevelHighBound(int to) { 
            for (int i = 0; i < to; i++) {
                if (MunchIf(nesting[i])) continue;
                if (nesting[i].Trim().Length == 0 && (EOF() || Peek()=='\n')) continue;
                return i;
            }
            return to;
        }
        
        // Process a line that starts at `step1[i]`.
        // Returns false if this is the final line.
        // TODO: local relative i2o
        private bool NextLine()
        {
            bool done = false;

            word = new StringBuilder(64);
            par = new StringBuilder(1024);

            // Emphasis never leaves the current line.
            doubleEmphasisOn = new bool[EMPHASES];  // all are false=off on start of line
            emphasisStartCandidate = new int[EMPHASES]; 
            maybeEmphasisStart = new int[EMPHASES];
            maybeEmphasisEnd = new int[EMPHASES];
            for (int i = 0; i < EMPHASES; i++) emphasisStartCandidate[i] = maybeEmphasisStart[i] = maybeEmphasisEnd[i] = -1;

            PartType type = PartType.PARAGRAPH;
            ParserState state = ParserState.LINE_START;
            CodeLanguage codeLanguage = CodeLanguage.AUTO;  // unused, initialized just to shut up the compiler
            lastMatch = "";

            int parenLevel = 0;  // parentheses level in URLs
            int number = 0;      // multi-use number (used in ArticlePart)
            int distanceFromLastBlock = 0;

            int oldPosition = 0;
            int[] oldMarkersWanted = new int[(int)(Markers.LAST)+1];
            InlineFunction SaveState = delegate() { oldPosition = position; for (int i=0; i<markersWanted.Length; i++) oldMarkersWanted[i] = markersWanted[i]; };
            InlineFunction LoadState = delegate() { position = oldPosition; for (int i=0; i<markersWanted.Length; i++) markersWanted[i] = oldMarkersWanted[i]; };

            // Convert the COMA input into an immediate representation:
            // - 00..31: special chars (emphasis, links, code, quotes, ...)
            // TODO: - links and the like are hashed (hex character = 10..1F) and postponed until everything is parsed
            // other stuff is already HTML-encoded

            // TODO: `done` should be a parser state?

            // Resolve the nesting level of this line.
            {
            int nLevel = NestingLevelHighBound(nesting.Count);
            if (nLevel != nesting.Count) nesting.RemoveRange(nLevel, nesting.Count - nLevel);
            }
            
            // TODO: big speedup - `if`s in cases should be switches on the first char
            while (!done) switch (state) {
                case ParserState.LINE_START:  // we're at block start - we can count on word.Length==0
                    // EOF
                    if (EOF()) { done = true; break; }
                    // Any spaces?
                    if (MunchIf(' ')) { distanceFromLastBlock++; break; }
                    
                    // Underlines
                    int u = underlineChars.IndexOf(Peek());
                    if (u != -1) {
                        SaveState();
                        int chars = 0;
                        char underlineChar = underlineChars[u];
                        while (MunchIf(underlineChar)) chars++;  // munch all equal chars
                        if (chars >= 2 && (EOF() || MunchIf('\n'))) {
                            // header? ("___" is always a horizontal rule - doesn't eat a newline either)
                            if (underlineChar != '_' && (articleParts.Count > 0 && articleParts[articleParts.Count-1].nestingLevel == nesting.Count && articleParts[articleParts.Count-1].type == PartType.PARAGRAPH)) {
                                articleParts[articleParts.Count-1].type = PartType.UNDERLINED_HEADER;
                                articleParts[articleParts.Count-1].number = u;  // number = header level (1..4)
                                return !EOF();
                            }
                            // it's a horizontal rule -- eat up one empty line (if it's not ___)
                            else {
                                if (underlineChar != '_' && (articleParts.Count > 0 && articleParts[articleParts.Count-1].nestingLevel == nesting.Count && articleParts[articleParts.Count-1].type == PartType.VERTICAL_SPACE)) {
                                    articleParts[articleParts.Count-1].number--;
                                }
                                articleParts.Add(new ArticlePart(PartType.HORIZONTAL_RULE, "", u, nesting.Count));
                                return !EOF();
                            }
                        }
                        else LoadState();
                    }
                    // Auto-links
                    if (autoLinkPossibleStarts.Contains(Peek().ToString())) {
                        for (int i = 0; i < autoLinkStart.Length; i++) if (MunchIf(autoLinkStart[i])) {
                            word.Append(autoLinkStart[i]); state = ParserState.AUTOMATIC_LINK; parenLevel = 0; goto parsed;
                        }
                    }
                    // Smileys
                    if (TryConvertSmileys() == true) goto backToNormal;
                    // Sequences
                    if (sequencesPossibleStarts.Contains(Peek().ToString())) {
                        for (int i = 0; i < sequence.Length; i+=2) if (MunchIf(sequence[i])) {
                            AddToPar(FinishWordHTML(), sequence[i+1]);
                            goto backToNormal;
                        }
                    }
                    // Explicit ![links]. [Simple links] are handled in ParserState.TARGET because they start like `[labels]:`.
                    for (int i = 1; i < linkStart.Length; i++) if (MunchIf(linkStart[i])) {
                        done = ProcessExplicitLink((LinkType)i);
                        goto backToNormal;
                    }
                    // Citations
                    if (MunchIf('>')) {
                        articleParts.Add(new ArticlePart(PartType.CITATION, "", 0, nesting.Count));
                        nesting.Add(Spaces(distanceFromLastBlock) + ">");
                        distanceFromLastBlock = 0;
                        break;
                    }
                    // Lists * - +
                    // Auto-numbered lists #. #) +. +)
                    if (itemPossibleStarts.Contains(Peek().ToString())) {
                        SaveState();
                        PartType partType = PartType.INVALID;
                        if (MunchIf("*")) partType = PartType.ITEM_STAR;
                        else if (MunchIf("+.", "+)")) partType = PartType.ITEM_AUTO;
                        else if (MunchIf("#.", "#)")) partType = PartType.ITEM_AUTO;
                        else if (MunchIf("+")) partType = PartType.ITEM_PLUS;
                        else if (MunchIf("-")) partType = PartType.ITEM_MINUS;

                        if (partType != PartType.INVALID) {
                            if (" \n".Contains(Peek().ToString())) {   
                                articleParts.Add(new ArticlePart(partType, "", 0, nesting.Count));
                                nesting.Add(Spaces(distanceFromLastBlock + 1));
                                distanceFromLastBlock = lastMatch.Length - 1;
                                state = ParserState.LINE_START;
                                break;
                            }
                            else LoadState();
                        }
                    }
                    
                    // Headers
                    if (Peek()=='#') if (MunchIf("######", "#####", "####", "###", "##", "#")) {
                        type = PartType.HEADER_HASH; number = lastMatch.Length; goto backToNormal;
                    }
                    if (Peek()=='=') if (MunchIf("======", "=====", "====", "===", "==", "=")) {
                        type = PartType.HEADER_EQUALS; number = lastMatch.Length; goto backToNormal;
                    }

                    // Lists 1. 1) a. a) I. I)
                    if (MunchIfAny("0-9"))     { word.Append(lastMatch); state = ParserState.ITEM_NUMBER; break; }
                    if (MunchIfAny("a-z"))     { word.Append(lastMatch); state = ParserState.ITEM_LETTER; break; }
                    if (MunchIfAny("IVXLCDM")) { word.Append(lastMatch); state = ParserState.ITEM_ROMAN; break; }
                    
                    // [Notes, references or labels]: or [links]
                    if (MunchIf('[')) { state = ParserState.TARGET; break; }

                    // This is not a line-starting Coma construct: try other ones
                    state = ParserState.NORMAL;
                    goto case ParserState.NORMAL;

                case ParserState.ITEM_LETTER:
                case ParserState.ITEM_NUMBER:
                case ParserState.ITEM_ROMAN:
                    if (EOF() || MunchIf('\n')) { AddToPar(FinishWord()); done = true; break; }       // EOF or EOL: cancel
                    if (MunchIf(". ", ") ", ".\n", ")\n")) {                                          // ". " or ") ": finish number
                        string w = FinishWord();
                        int num = 0;
                        PartType partType;
                        switch (state) {
                            case ParserState.ITEM_NUMBER:
                                if (!int.TryParse(w, out num)) { AddToParNoMarkers(w, lastMatch); goto backToNormal; } partType = PartType.ITEM_NUMBER; break;
                            case ParserState.ITEM_LETTER:
                                num = w[0] - 'a' + 1; partType = PartType.ITEM_LETTER; break;
                            case ParserState.ITEM_ROMAN: default:
                                if (!TryParseRoman(w, out num)) { AddToParNoMarkers(w, lastMatch); goto backToNormal; } partType = PartType.ITEM_ROMAN; break;
                        }
                        articleParts.Add(new ArticlePart(partType, "", num, nesting.Count));
                        nesting.Add(Spaces(distanceFromLastBlock + 1));
                        distanceFromLastBlock = w.Length + 1;
                        state = ParserState.LINE_START;
                        if (lastMatch[1] == '\n') done = true;
                        break;
                    }
                    if ((state==ParserState.ITEM_NUMBER && MunchIfAny("0-9")) ||
                        (state==ParserState.ITEM_ROMAN && MunchIfAny("IVXLCDM"))) {
                        word.Append(lastMatch); break;
                    }

                    state = ParserState.NORMAL;
                    goto case ParserState.NORMAL;  // cancel the word

                case ParserState.TARGET:
                    // TODO: use the restarting thing here
                    if (EOF() || MunchIf('\n')) { AddToPar("[", FinishWordHTML()); done = true; break; }  // EOF or EOL: cancel
                    if (MunchIf(']')) {
                        if (MunchIf(':')) {            // ]: start of target (note, reference or label)
                            string refName = FinishWord();

                            if (EOF() || MunchIf('\n')) {       // it's a note: finish
                                articleParts.Add(new ArticlePart(PartType.NOTE, refName, 0, nesting.Count));
                                nesting.Add(Spaces(distanceFromLastBlock + 1));
                                distanceFromLastBlock = refName.Length + 2;
                                return !EOF();
                            }
                            else {                          // has further text on its line - it's a reference or a label.
                                // check whether the first word is an URI: yes - it's a reference: finish
                                string[] url_tooltip = MunchRestOfLine().Trim().Split(new char[]{' '}, 2);
                                if (CanBeURI(url_tooltip[0])) {
                                    if (targets.ContainsKey(url_tooltip[0])) targets.Remove(url_tooltip[0]);
                                    if (targets.ContainsKey(refName)) targets.Remove(refName);  // HACK: multiple definitions of the same thing occur when changes are displayed
                                    targets.Add(refName, new Target(TargetType.REFERENCE, url_tooltip));
                                    while (!(EOF() || MunchIf('\n'))) Munch();  // skip the rest of the line
                                    return !EOF();
                                }
                                // no - it's a label
                                articleParts.Add(new ArticlePart(PartType.LABEL, refName, 0, nesting.Count));
                                state = ParserState.LINE_START;
                            }
                            break;
                        }
                        else { done = ProcessLink2(LinkType.NORMAL); goto backToNormal; }  // ] it's a link: parse the rest
                    }
                    else word.Append(Munch());
                    break;

                case ParserState.NORMAL:
                    SaveState();
                    // EOF or EOL
                    if (EOF() || MunchIf('\n')) { AddToPar(FinishWordHTML()); ResolveEmphasisAtNonWordChar(); done = true; break; }  
                    // Auto-links, smileys
                    if (word.Length == 0) {
                        if (autoLinkPossibleStarts.Contains(Peek().ToString())) {
                            for (int i = 0; i < autoLinkStart.Length; i++) if (MunchIf(autoLinkStart[i])) {
                                word.Append(autoLinkStart[i]); ResolveEmphasisAtConstruct(); state = ParserState.AUTOMATIC_LINK; parenLevel = 0; goto parsed;
                            }
                        }
                        if (TryConvertSmileys()) break;
                    }
                    // Sequences
                    if (sequencesPossibleStarts.Contains(Peek().ToString())) {
                        for (int i = 0; i < sequence.Length; i += 2) if (MunchIf(sequence[i])) {
                            AddToPar(FinishWordHTML(), sequence[i+1]); ResolveEmphasisAtConstruct();
                            goto backToNormal;
                        }
                    }
                    // Emphasis (all types)
                    int e = emphasisChars.IndexOf(Peek());
                    if (e != -1) {
                        Munch();
                        // double-char: works everywhere in normal text, doesn't break words
                        // Is it sensible to allow ""text"" and ''text''? IMHO yes
                        if (MunchIf(emphasisChars[e])) {
                            if (word.Length == 0) AddToParNoMarkers(((char)e).ToString());  // don't change word status
                            else word.Append((char)e);

                            doubleEmphasisOn[e] = !doubleEmphasisOn[e];  // toggle emphasis on/off
                            maybeEmphasisStart[e] = emphasisStartCandidate[e] = -1;  // discard eventual single-char emphasis start
                            goto backToNormal;
                        }
                        // single-char: works only on word boundaries (whatever that means ;))
                        else {
                            // maybe end, if we have a start candidate (prefer the earliest possibility)
                            // doesn't work after an explicit space
                            if (emphasisStartCandidate[e]!=-1 && maybeEmphasisEnd[e]==-1 && !(word.Length==0 && par.Length>0 && par[par.Length-1]==' ')) {
                                AddToPar(FinishWordHTML());
                                AddToParNoMarkers(emphasisPlaceholders[e]);
                                maybeEmphasisEnd[e] = par.Length - 1;
                                goto backToNormal;
                            }

                            // maybe start, if we're not in a word and the corresponding double-char emphasis isn't on
                            if (word.Length==0 && !doubleEmphasisOn[e]) {
                                ResolveEmphasisEndAtWordBoundary();  // resolve words that have started without a non-word char
                                AddToParNoMarkers(emphasisPlaceholders[e]);
                                maybeEmphasisStart[e] = par.Length - 1;
                                goto backToNormal;
                            }

                            // if it's a single-char " which doesn't end any opened " and word.Length>0, it's an acronym: GNU"GNU's Not Unix".
                            // Acronyms may not begin with a space.
                            // TODO: (long acronyms)"tooltip" (*)"tooltip"
                            if (e == emphasisChars.IndexOf('"') && Peek()!=' ') {
                                SaveState();
                                StringBuilder tooltip = new StringBuilder(32);
                                while (true) {
                                    if (EOF() || Peek()=='\n') { LoadState(); break; }
                                    if (MunchIf('\"')) { AddToPar("<span title=\"", ToHTML(tooltip.ToString()), "\">", FinishWordHTML(), "</span>"); ResolveEmphasisAtConstruct(); goto backToNormal; }
                                    tooltip.Append(Munch());
                                }
                            }

                            // otherwise it's just a normal non-word character: PUT IT IN
                            // is has to be a single char - if it needs to be escaped in HTML, replace it later
                            AddToPar(FinishWordHTML(), emphasisPlaceholders[e]);
                            ResolveEmphasisAtNonWordChar();
                            goto backToNormal;
                        }
                    }
                    // TODO: how do word boundaries around escapes and HTML entities work? now they always do

                    // Single-char escape
                    if (MunchIf('\\')) {
                        AddToPar(FinishWordHTML()); ResolveEmphasisAtConstruct();
                        if (EOF()) LoadState();                                        // EOF: cancel
                        else { AddToPar(ToHTMLEscaped(Munch())); goto backToNormal; }  // Other char (including EOL) - PUT IT IN 
                    }
                    // Inline `code`
                    if (MunchIf('`')) {
                        string languageCandidate = FinishWord();
                        codeLanguage = ParseCodeLanguages(languageCandidate);
                        if (codeLanguage == CodeLanguage.UNSUPPORTED_LANGUAGE) { AddToPar(ToHTML(languageCandidate)); }
                        ResolveEmphasisAtConstruct();
                        state = ParserState.CODE_BACKTICK; break;
                    }
                    // Long (\n code \n)\n
                    if (MunchIf("(\n")) {
                        string languageCandidate = FinishWord();
                        codeLanguage = ParseCodeLanguages(languageCandidate);
                        if (codeLanguage == CodeLanguage.UNSUPPORTED_LANGUAGE) { AddToPar(ToHTML(languageCandidate)); codeLanguage = CodeLanguage.AUTO; }
                        ResolveEmphasisAtConstruct();
                        string wrapper = ((char)ConstructType.PRE).ToString();
                        AddToParNoMarkers(wrapper, ((char)codeLanguage).ToString());
                        ProcessParenPRE();
                        AddToParNoMarkers(wrapper);
                        type = PartType.PARAGRAPH_ENDSWITHPRE;
                        done = true;
                        break;
                    }   
                    // Inline ( code )
                    if (MunchIf("( ")) {
                        string languageCandidate = FinishWord();
                        codeLanguage = ParseCodeLanguages(languageCandidate);
                        if (codeLanguage == CodeLanguage.UNSUPPORTED_LANGUAGE) { AddToPar(ToHTML(languageCandidate)); }
                        ResolveEmphasisAtConstruct();
                        state = ParserState.CODE_PAREN; break;
                    }
                    // Single-word links
                    if (word.Length > 0 && MunchIfAny("([")) {
                        string linkTextCandidate = FinishWord();
                        if (!IsWordLink2(lastMatch[0], linkTextCandidate)) {
                            AddToPar(ToHTML(linkTextCandidate));
                            LoadState();
                        }
                        else { ResolveEmphasisAtConstruct(); goto backToNormal; }
                    }
                    // Explicit links
                    for (int i = 0; i < linkStart.Length; i++) if (MunchIf(linkStart[i])) {
                        AddToPar(FinishWordHTML()); ResolveEmphasisAtConstruct();
                        done = ProcessExplicitLink((LinkType)i);
                        goto backToNormal;
                    }
                    // Long HTML
                    if (MunchIf("<\n")) { type = PartType.VERBATIM_HTML; ProcessVerbatimHTML(); done = true; break; }
                    // Inline HTML tag
                    if (PeekAfterMunchIf("a-zA-Z",  "<", "</")) {
                        AddToPar(FinishWordHTML()); ResolveEmphasisAtConstruct();
                        string start = lastMatch;
                        StringBuilder tagText = new StringBuilder(16);
                        while (true) {
                            if (EOF() || Peek()=='\n') { LoadState(); break; }                               // EOF or EOL: undo
                            else if (MunchIf('>')) { AddToParNoMarkers(start, tagText.ToString(), lastMatch); goto backToNormal; }  // > ends tag
                            else tagText.Append(Munch());
                        }
                    }
                    // Inline HTML entity
                    if (Peek() == '&') {
                        AddToPar(FinishWordHTML()); ResolveEmphasisAtConstruct();
                        if (MunchIf("&#x", "&#X")) { word.Append(lastMatch); state = ParserState.HTML_ENTITY_HEX; break; }
                        if (MunchIf("&#"))         { word.Append(lastMatch); state = ParserState.HTML_ENTITY_NUM; break; }
                        if (MunchIf("&"))          { word.Append(lastMatch); state = ParserState.HTML_ENTITY; break; }
                    }
                    
                    // TODO: move some transformations after translation

                    // Ligatures after numbers: 123x, 14., 12-34, 123 456 789, 3 x 4
                    if (word.Length > 0 && word[word.Length-1] >= '0' && word[word.Length-1] <= '9') {
                        if (MunchIf("x")) { AddToPar(FinishWordHTML(),"&times;"); ResolveEmphasisAtNonWordChar(); goto backToNormal; }
                        if (PeekAfterMunchIf("0-9", " x ")) { AddToPar(FinishWordHTML(), "&nbsp;&times;&nbsp;"); ResolveEmphasisAtNonWordChar(); goto backToNormal; }
                        if (word.Length <= 4 && MunchIf(" ")) { AddToPar(FinishWordHTML(), "&nbsp;"); ResolveEmphasisAtNonWordChar(); goto backToNormal; }
                        if (MunchIf(". - ")) { AddToPar(FinishWordHTML(), ".&nbsp;&ndash; "); ResolveEmphasisAtNonWordChar(); goto backToNormal; }
                        if (MunchIf(". ")) { AddToPar(FinishWordHTML(), ".&nbsp;"); ResolveEmphasisAtNonWordChar(); goto backToNormal; }
                        if (PeekAfterMunchIf("0-9", "-")) { AddToPar(FinishWordHTML(), "&ndash;"); ResolveEmphasisAtNonWordChar(); goto backToNormal; }
                    }
                    // Minus: -3
                    if (word.Length==0 && PeekAfterMunchIf("0-9", "-")) { AddToPar("&minus;"); ResolveEmphasisAtNonWordChar(); goto backToNormal; }
                    // Initials and Czech prepositions: z_toho v_tom s_tim s._r._o.
                    if (word.Length==1) {
                        if (MunchIf(" ")) { AddToPar(FinishWordHTML(), "&nbsp;"); ResolveEmphasisAtNonWordChar(); goto backToNormal; }
                        if (MunchIf(". ")) { AddToPar(FinishWordHTML(), ".&nbsp;"); ResolveEmphasisAtNonWordChar(); goto backToNormal; }
                    }

                    // Other char - PUT IT IN (always an option :D)
                    if (Peek()=='_' || Char.IsLetterOrDigit(Peek())) { word.Append(Munch()); ResolveEmphasisAtWordChar(); }
                    else { AddToPar(FinishWordHTML(),ToHTML(Munch())); ResolveEmphasisAtNonWordChar(); }

                    goto backToNormal;

                case ParserState.CODE_BACKTICK:
                    if (EOF()) {                                                                  // EOF: write it as code, append "..."
                        if (codeLanguage == CodeLanguage.UNSUPPORTED_LANGUAGE) codeLanguage = CodeLanguage.NONE;
                        string wrapper = ((char)ConstructType.CODE).ToString();
                        AddToPar(wrapper, ((char)codeLanguage).ToString(), FinishWord(), "<span class=\"nocode error\">...</span>", wrapper);
                        done = true;
                    }   
                    else if (MunchIf('\n')) {                                                // EOL inside <code> turns it into a inline <pre>
                        done = ProcessPRE((char)codeLanguage);
                        goto backToNormal;
                    }  
                    else if (MunchIf("``")) word.Append('`');                                // `` produces a single `
                    else if (MunchIf('`')) {                                                 // ` closes it
                        if (codeLanguage == CodeLanguage.UNSUPPORTED_LANGUAGE) codeLanguage = CodeLanguage.NONE;
                        string wrapper = ((char)ConstructType.CODE).ToString();
                        AddToPar(wrapper, ((char)codeLanguage).ToString(), FinishWord(), wrapper);
                        goto backToNormal;
                    }
                    else word.Append(ToHTML(Munch()));                                                    // Other char - PUT IT IN 
                    break;

                case ParserState.CODE_PAREN:
                    if (EOF() || MunchIf("\n", " )")) {                    // EOF, EOL or " )" closes it
                        if (codeLanguage == CodeLanguage.UNSUPPORTED_LANGUAGE) codeLanguage = CodeLanguage.NONE;
                        string wrapper = ((char)ConstructType.CODE).ToString();
                        AddToPar(wrapper, ((char)codeLanguage).ToString(), FinishWord(), wrapper);
                        if (lastMatch == "" || lastMatch == "\n") done = true;
                        goto backToNormal;
                    }   
                    else word.Append(ToHTML(Munch()));                                                    // Other char - PUT IT IN 
                    break;

                case ParserState.HTML_ENTITY:
                case ParserState.HTML_ENTITY_HEX:
                case ParserState.HTML_ENTITY_NUM:
                    if (EOF()) { AddToPar(FinishWordHTML()); done = true; }                             // EOF: cancel
                    else if (MunchIf(';')) { AddToPar(FinishWord(), ";"); goto backToNormal; }     // ; ends entities
                    else if ((state==ParserState.HTML_ENTITY     && MunchIfAny("a-zA-Z0-9")) ||
                             (state==ParserState.HTML_ENTITY_HEX && MunchIfAny("a-fA-F0-9")) ||
                             (state==ParserState.HTML_ENTITY_NUM && MunchIfAny("0-9"))
                    )
                        word.Append(lastMatch);
                    else { AddToPar(FinishWordHTML()); goto backToNormal; }                             // otherwise cancel and process the next char as normal: it was just a &
                    break;

                case ParserState.AUTOMATIC_LINK:
                    if (EOF() || MunchIfAny("\n )")) {  // end on EOF, newline, ' ' or a ')' that doesn't match a '('.
                        if (lastMatch == ")" && parenLevel > 0) { parenLevel--; word.Append(')'); }
                        else {
                            char[] endChars = { ':', '.', ',', '!', '?' };  // if these chars are at the end, they are not part of the URL
                            string w = FinishWord();
                            string uri = w.TrimEnd(endChars);         
                            string rest = w.Substring(uri.Length);

                            AddToPar(MakeExternalLink(uri, uri, uri));
                            AddToPar(rest);

                            if (lastMatch=="" || lastMatch=="\n") done = true;
                            else AddToPar(lastMatch);
                            goto backToNormal;
                        }
                    }
                    else if (MunchIf('(')) { parenLevel++; word.Append('('); }
                    else word.Append(Munch());
                    break;

                default:
                backToNormal: state = ParserState.NORMAL;
                parsed: break;
            }

            // Convert the immediate representation into HTML.
            // We get here only on ArticleParts that (should) contain text.
            
            string s = par.ToString();
            par = new StringBuilder(s.Length);
            Stack<char> emphasisState = new Stack<char>();  // TODO: Stack is weak in .NET 2.0 - make it a List.
            bool insideCode = false;

            // headers: strip trailing "###..." or "===...", delete whitespace
            if (type == PartType.HEADER_HASH || type == PartType.HEADER_EQUALS) {
                int len = 0;
                while (s.Length-1-len >= 0 && s[s.Length-1-len] == (type == PartType.HEADER_HASH ? '#' : '=')) len++;
                s = s.Substring(0, s.Length-len).Trim();
            }

            // empty lines = vertical spaces
            if (s.Length == 0) {
                // merge with previous vertical space?
                if (articleParts.Count > 0 && articleParts[articleParts.Count-1].nestingLevel == nesting.Count && articleParts[articleParts.Count-1].type == PartType.VERTICAL_SPACE) {
                    articleParts[articleParts.Count-1].number++;
                    return !EOF();
                }
                else { type = PartType.VERTICAL_SPACE; number = 1; }  // number = skipped half-lines
            }

            // resolve emphasis nesting
            for (int i=0; i<s.Length; i++) {
                char c = s[i];
                switch (c) {
                    case (char)ConstructType.CODE: case (char)ConstructType.PRE:
                        if (!insideCode) {
                            foreach (char e in emphasisState) AddToParNoMarkers(HTMLPause[e]);  // turn off emphasis for a while
                            insideCode = true;
                            AddToParNoMarkers(HTMLStartCodeWithLanguage((ConstructType)c, s[++i]));
                        }
                        else {
                            insideCode = false; AddToParNoMarkers(HTMLEnd[c]);
                            Array reversedEmphasisState = emphasisState.ToArray(); Array.Reverse(reversedEmphasisState);  // TODO: This is awful - better make it a List.
                            foreach (char e in reversedEmphasisState) AddToParNoMarkers(HTMLContinue[e]);  // turn in on again
                        }
                        break;
                    case (char)ConstructType.LINK:               AddToParNoMarkers(c.ToString(), s[++i].ToString()); break;   // links are resolved in Postprocess()
                    case (char)ConstructType.DQUOTE_PLACEHOLDER: AddToParNoMarkers("&quot;"); break;
                    case (char)ConstructType.SQUOTE_PLACEHOLDER: AddToParNoMarkers("&rsquo;"); break;  // typographic apostrophe unless in code or escaped
                    default:
                        if (c < EMPHASES) { // emphasis
                            // emphasis `c` is ON: end it (take care of correct nesting)
                            if (emphasisState.Contains(c)) {
                                char e;
                                Stack<char> tmp = new Stack<char>();

                                // close every emphasis up to `c`
                                while ((e = emphasisState.Pop()) != c) { AddToParNoMarkers(HTMLPause[e]); tmp.Push(e); }
                                // end emphasis `c`
                                AddToParNoMarkers(HTMLEnd[e]);
                                // restart every closed emphasis
                                while (tmp.Count > 0) { e = tmp.Pop(); AddToParNoMarkers(HTMLContinue[e]); emphasisState.Push(e); }
                            }
                            // emphasis `c` is OFF: Start it
                            else {
                                AddToParNoMarkers(HTMLStart[c]); emphasisState.Push(c);
                            }
                        }
                        else AddToParNoMarkers(c.ToString());  // different char: PUT IT IN
                        break;
                }
            }

            // empty the emphasis stack at the end of a paragraph
            while (emphasisState.Count > 0) { char e = emphasisState.Pop(); AddToParNoMarkers(HTMLEnd[e]); }

            // whew!
            articleParts.Add(new ArticlePart(type, par.ToString(), number, nesting.Count));

            return !EOF();
        }



        private void ZeroMaybeMarkers() { for (int i=0; i<=(int)(Markers.LAST); i++) maybeMarkers[i] = 0; }
        private void TransferMaybeMarkers() { for (int i=0; i<=(int)(Markers.LAST); i++) markersWanted[i] += maybeMarkers[i]; }
        
        // Returns the first position that doesn't contain a marker.
        // Keep skipped markers in `markersWanted` for later.
        private int SkipMarkersSilent(int p) { while (step1[p] <= (int)(Markers.LAST)) { p++; } return p; }
        private int SkipMarkersSilentEOF(int p) { while (p < step1.Length && step1[p] <= (int)(Markers.LAST)) { p++; } return p; }
        private int SkipMarkers(int p) { while (step1[p] <= (int)(Markers.LAST)) { markersWanted[step1[p]]++; p++; } return p; }
        private int SkipMaybeMarkersEOF(int p) { while (p < step1.Length && step1[p] <= (int)(Markers.LAST)) { maybeMarkers[step1[p]]++; p++; } return p; }
        private int SkipMaybeMarkers(int p) { while (step1[p] <= (int)(Markers.LAST)) { maybeMarkers[step1[p]]++; p++; } return p; }

        // Return true if the cursor passed the end of `step1` (EOF).
        private bool EOF() { if (position >= step1.Length) { lastMatch = ""; return true; } else return false; }

        // Return the character on the cursor (skip markers before it). Don't check for EOF!
        private char Peek() { return step1[SkipMarkersSilent(position)]; }
        
        // Eat one character from the cursor (skip markers before it). Don't check for EOF!
        private char Munch() { position = SkipMarkers(position); return step1[position++]; }
        
        // Return the rest of the line.
        private string MunchRestOfLine() {
            StringBuilder s = new StringBuilder(64);
            while (true) {
                if (EOF() || Peek()=='\n') return s.ToString();
                else s.Append(Munch());
            }
        }

        // Try to match a string/character. On success, set `lastMatch` to the match, move the cursor and return true.
        private bool MunchIf(string s) {
            ZeroMaybeMarkers();
            int p = position;
            for (int i = 0; i < s.Length; i++) {
                p = SkipMaybeMarkersEOF(p); if (p == step1.Length) return false;
                if (step1[p] == s[i]) { p++; continue; }
                return false;
            }
            lastMatch = s; position = p; TransferMaybeMarkers(); return true;
        }
        private bool MunchIf(string s0, params string[] ss) {
            if (MunchIf(s0)) return true;
            foreach (string s in ss) if (MunchIf(s)) return true;
            return false;
        }
        private bool MunchIf(char c) { return MunchIf(c.ToString()); }

        // Try to match a bunch of strings. If a string matches and the next character fulfills the light regexp `peek`, success!
        private bool PeekAfterMunchIf(string peek, params string[] ss) {
            foreach (string s in ss) {
                ZeroMaybeMarkers();
                int p = position;
                for (int i = 0; i < s.Length; i++) {
                    p = SkipMaybeMarkersEOF(p); if (p == step1.Length) goto nextString;
                    if (step1[p] == s[i]) { p++; continue; }
                    return false;
                }
                int q = p;
                for (int i = 0; i < peek.Length; i++) {
                    q = SkipMarkersSilentEOF(q); if (q == step1.Length) goto nextString;
                    if (i+2 < peek.Length && peek[i+1]=='-') { if (step1[q]>=peek[i] && step1[q]<=peek[i+2]) { lastMatch = s; position = p; TransferMaybeMarkers(); return true; } i+=2; }
                    else                                     { if (step1[q]==peek[i])                        { lastMatch = s; position = p; TransferMaybeMarkers(); return true; } }
                }
                nextString:;
            }
            return false;
        }

        /// <summary>Try to match a character with multiple variants and/or character ranges (like "a-zA-Z0-9#/").
        /// On success, set `lastMatch` to the match, move the cursor and return true.
        /// Don't check for EOF!</summary>
        private bool MunchIfAny(string s) {
            ZeroMaybeMarkers();
            int p = position;
            for (int i = 0; i < s.Length; i++) {
                p = SkipMaybeMarkers(p); if (p == step1.Length) return false;
                if (i+2 < s.Length && s[i+1]=='-') { if (step1[p]>=s[i] && step1[p]<=s[i+2]) { lastMatch = step1[p++].ToString(); position = p; TransferMaybeMarkers(); return true; } i+=2; }
                else                               { if (step1[p]==s[i])                     { lastMatch = step1[p++].ToString(); position = p; TransferMaybeMarkers(); return true; } }
            }
            return false;
        }

        // Resolve emphasis when a specific event occurs:
        void ClearMaybeStarts() { for (int i = 0; i < EMPHASES; i++) maybeEmphasisStart[i] = -1; }
        void ClearMaybeEnds() { for (int i = 0; i < EMPHASES; i++) maybeEmphasisEnd[i] = -1; }
        void PromoteMaybeStarts() { for (int i = 0; i < EMPHASES; i++) if (maybeEmphasisStart[i] != -1) { emphasisStartCandidate[i] = maybeEmphasisStart[i]; maybeEmphasisStart[i] = -1; } }
        void PromoteMaybeEnds() { for (int i = 0; i < EMPHASES; i++) if (maybeEmphasisEnd[i] != -1) { par[emphasisStartCandidate[i]] = par[maybeEmphasisEnd[i]] = (char)i; emphasisStartCandidate[i] = maybeEmphasisEnd[i] = -1; } }

        // We got a word character (alphanumeric or _).
        void ResolveEmphasisAtWordChar() { PromoteMaybeStarts(); ClearMaybeEnds(); }
        // We got a non-word character, EOL or EOF.
        void ResolveEmphasisAtNonWordChar() { ClearMaybeStarts(); PromoteMaybeEnds(); }
        // We ended a word and started another one right away.
        void ResolveEmphasisEndAtWordBoundary() { PromoteMaybeEnds(); }
        // We started a construct (possibly inside a word).
        void ResolveEmphasisAtConstruct() { PromoteMaybeStarts(); PromoteMaybeEnds(); }

        // Return a string of `count` spaces.
        private string Spaces(int count) { return new string(' ', count); }

        // Encode a char/string as literal HTML characters. Also works for HTML tag attributes.
        private string ToHTML(char c)
        {
            if ("<>&\"".IndexOf(c)>=0 || c >= 0x80) return "&#" + ((int)c).ToString() + ";";
            else return c.ToString();
        }

        private string ToHTML(string s)
        {
            StringBuilder w = new StringBuilder(s.Length * 3);
            foreach (char c in s) w.Append(ToHTML(c));
            return w.ToString();
        }

        // Encode a string as a valid URI.
        private string ToURI(char c)  // This is only a helper, it can't be a char-to-char conversion ;).
        {
            if (c == '&') return "&amp;";
            if ((c>='0' && c<='9') || (c>='a' && c<='z') || (c>='A' && c<='Z') || "._~-:/?#[]@!*'();=+$,\\".IndexOf(c)>=0) return c.ToString();
            if (c >= 0x800) return string.Format("%{0:X2}%{1:X2}%{2:X2}", 0xE0+(c>>12), 0x80+(c>>6 & 0x3F), 0x80+(c&0x3F));  // UTF-8 encoding
            if (c >= 0x80) return string.Format("%{0:X2}%{1:X2}", 0xC0+(c>>6), 0x80+(c&0x3F));
            return string.Format("%{0:X2}", c);
        }
        private string ToURI(string s)
        {
            if (!string.IsNullOrEmpty(_urlBase))
            {
              // resolve possibly relative URL
              try
              {
                Uri test = new Uri(s, UriKind.RelativeOrAbsolute);
                if (!test.IsAbsoluteUri) s = _urlBase + s;
              }
              catch (Exception)
              {
                // keep the original value
              }
            }

            StringBuilder w = new StringBuilder(s.Length * 2);

            for (int i=0; i<s.Length; i++) {
                // copy %(hex)(hex) without change (no double-encoding)
                if (s[i]=='%' && i+2<s.Length && "0123456789abcdefABCDEF".IndexOf(s[i+1])>=0 && "0123456789abcdefABCDEF".IndexOf(s[i+2])>=0) {
                    w.Append(s[i]).Append(s[i+1]).Append(s[i+2]);
                    i += 2;
                }
                else w.Append(ToURI(s[i]));
            }
            string uri = w.ToString();
            // if it doesn't contain a protocol, add `http`.
            if (!uri.Contains("://")) uri = "http://" + uri;
            return uri;
        }

        // Return true if a string may be an URI (pretty lenient - called at places where an URI is expected).
        private bool CanBeURI(string s)
        {
            // URIs may not be too short and may not contain spaces.
            // TODO: apparently, mortals have a different opinion
            if (s.Length < 4 || s.Contains(" ")) return false;
            // When it starts with "http://", "news:" or other common link starts, it's an URI.
            if (autoLinkPossibleStarts.Contains(s[0].ToString())) for (int i = 0; i < autoLinkStart.Length; i++) if (s.StartsWith(autoLinkStart[i])) return true;
            // Just check for dots. URIs contain dots, right?
            // TODO: dots shouldn't have a space after them
            return s.Substring(1,s.Length-2).Contains(".");
        }

        // Encode a char/string as a HTML numbered entity (special version for \escaping).
        private string ToHTMLEscaped(char c)
        {
            if (c==' ') return "&nbsp;";
            else if (c=='\n') return " ";
            else return "&#" + ((int)c).ToString() + ";";
        }
        private string ToHTMLEscaped(string s)
        {
            StringBuilder w = new StringBuilder(s.Length * 5);
            foreach (char c in s) w.Append(ToHTMLEscaped(c));
            return w.ToString();
        }

        // Encode a string as an unique case-insensitive HTML label.
        // Letters [A-Z] are let through, other characters are changed into _UUU (underscore + decimal codepoint).
        private string ToLabelName(string s)
        {
            StringBuilder res = new StringBuilder(s.Length * 2);
            foreach (char c in s.ToUpperInvariant()) {
                if (c >= 'A' && c <= 'Z') res.Append(c);
                else res.Append("_").Append((int)c);
            }
            return res.ToString();
        }

        // Move `word` into a string and make `word` empty. Return the new string.
        private string FinishWord() { string s = word.ToString(); word.Length = 0; return s; }
        private string FinishWordHTML() { return ToHTML(FinishWord()); }

        /// <summary>Append some strings into `par`.</summary>
        private void AddToPar(params string[] ss) {
            DoOpeningMarkers();
            foreach (string s in ss) par.Append(s);
            DoClosingMarkers();
        }
        private void AddToParNoMarkers(params string[] ss) {
            foreach (string s in ss) par.Append(s);
        }
        
        private static readonly string[] romanStrings = { "CM", "CD", "XM", "XD", "XC", "XL", "IM", "ID", "IC", "IL", "IX", "IV", "M", "D", "C", "L", "X", "V", "I" };
        private static readonly int[]    romanValues =  { 900,  400,  990,  490,  90,   40,   999,  499,  99,   49,   9,    4,    1000,500, 100, 50,  10,  5,   1   };
        /// <summary>Convert a Roman number into an int (with no error checks!). Follows the conventions of `int.TryParse`.</summary>
        private bool TryParseRoman(string s, out int num)
        {
            num = 0;
            while (s.Length > 0) {
                for (int i = 0; i < romanStrings.Length; i++) if (s.StartsWith(romanStrings[i])) {
                    s = s.Substring(romanStrings[i].Length);
                    num += romanValues[i]; if (num > 3000) { num = 0; return false; }
                    break;
                }
            }
            return true;
        }

        
        // Process an explicit link. If it doesn't end in ']', cancel it.
        private bool ProcessExplicitLink(LinkType type) {
            while (true) {
                if (EOF() || MunchIf('\n')) { AddToPar(ToHTML(linkStart[(int)type]), FinishWordHTML()); return true; }  // EOF or EOL: cancel
                if (MunchIf(']')) break; // ] - it's a link
                word.Append(Munch());
            }
            return ProcessLink2(type);
        }

        /*
        // Return `false` if the following text is not a link.
        private bool IsExplicitLink(char openingChar) {  
            while (true) {
                if (EOF() || Peek()=='\n') { return false; }  // EOF or EOL: not a link candidate
                if      (openingChar == '[') { if (MunchIf(']')) break; }
                else if (openingChar == '(') { if (MunchIf(')')) break; }
                word.Append(Munch());
            }
            return ProcessLinkPart2(type);
        }
        */

        /// <summary>Called when a [link] ends correctly. Parses an optional [second][part] of the link and adds the link to the link pool.</summary>
        /// The optional part can look like [b] or (b).
        /// TODO: allow a[b], a(b), (a)[b], (a)(b) and versions with tooltips a(b)"c"
        private bool ProcessLink2(LinkType type)
        {
            string text = FinishWord();

            if (!EOF() && MunchIfAny("([")) {  // The optional part is there.
                char start = lastMatch[0];
                while (true) {
                    if (EOF() || MunchIf('\n')) { AddToPar(ToHTML(linkStart[(int)type]), ToHTML(text), "]", start.ToString(), FinishWordHTML()); return true; }  // EOF or EOL: cancel second part
                    if (start=='[' && MunchIf(']')) break;
                    if (start=='(' && MunchIf(')')) break;
                    word.Append(Munch());
                }
                char end = lastMatch[0];
                string target = FinishWord();

                AddToPar(((char)ConstructType.LINK).ToString(), ((char)links.Count).ToString()); // it's a [long][link]
                links.Add(new Link(type, text, target, ToHTML(linkStart[(int)type] + text + "]" + start.ToString() + target + end.ToString())));
            }
            else {
                AddToPar(((char)ConstructType.LINK).ToString(), ((char)links.Count).ToString()); // it's a [link]
                links.Add(new Link(type, text, text, ToHTML(linkStart[(int)type] + text + "]")));
            }
            return false;
        }

        // Called after finding a word is followed by a '(' or '['. Returns whether it was a link.
        private bool IsWordLink2(char openingChar, string linkText)
        {
            char closingChar = (openingChar == '(') ? ')' : ']';
            StringBuilder target = new StringBuilder(16);
            while (true) {
                if (EOF() || Peek()=='\n') { return false; }  // EOF or EOL: write the word, cancel
                if (MunchIf(closingChar)) break;
                target.Append(Munch());
            }

            AddToPar(((char)ConstructType.LINK).ToString(), ((char)links.Count).ToString()); // it's a [long][link]
            links.Add(new Link(LinkType.NORMAL, linkText, target.ToString(), ToHTML(linkText + openingChar + target.ToString() + closingChar)));
            return true;
        }
        
        /// <summary>Process chars until the end of `...\n PRE`. Returns true if the paragraph ends.</summary>
        private bool ProcessPRE(char codeLanguage)
        {
            List<string> lines = new List<string>();
            List<int> lineNestingLevels = new List<int>();
            int minNestingLevel = nesting.Count;

            // Finish the current line and find nesting of the next line.
            InlineFunction DoLineEnd = delegate() {
                word.Append('\n'); lines.Add(FinishWord());
                int level = NestingLevelHighBound(minNestingLevel);
                if (minNestingLevel > level) minNestingLevel = level;   
                lineNestingLevels.Add(level);
            };

            InlineFunctionString DoPREEnd = delegate(string appended) {
                if (codeLanguage == (char)CodeLanguage.UNSUPPORTED_LANGUAGE) codeLanguage = (char)CodeLanguage.AUTO;
                lines.Add(FinishWord());                               // complete last line
                string wrapper = ((char)ConstructType.PRE).ToString();
                AddToParNoMarkers(wrapper, codeLanguage.ToString(), lines[0]);  // first line is exempt from nesting checks
                for (int i = 0; i < lineNestingLevels.Count; i++) {
                    for (int j = minNestingLevel; j < lineNestingLevels[i]; j++) AddToParNoMarkers(nesting[j]);  // discard up to the minimum seen nesting level
                    AddToPar(lines[i + 1]);
                }
                AddToParNoMarkers(appended);
                AddToParNoMarkers(wrapper);
            };

            // we got here on the first '\n' in CODE: add the first line
            DoLineEnd();

            while (true) {
                if (EOF()) { DoPREEnd("\n<span class=\"nocode error\">...</span>"); return true; }  // EOF closes PRE, but signalizes that it's unclosed
                else if (MunchIf('\n')) DoLineEnd();                    // EOL: finish current line, start next line
                else if (MunchIf("``")) word.Append('`');               // `` produces a single `
                else if (MunchIf('`')) { DoPREEnd(""); return false; }  // ` closes PRE
                else word.Append(ToHTML(Munch()));                           // Other char - PUT IT IN 
            }
        }

        /// <summary>Process chars until the end of (\n PRE \n)\n.</summary>
        private void ProcessParenPRE()
        {
            List<string> lines = new List<string>();
            List<int> lineNestingLevels = new List<int>();
            int minNestingLevel = nesting.Count;

            InlineFunction Finish = delegate() {
                for (int i = 0; i < lines.Count; i++) {
                    for (int j = minNestingLevel; j < lineNestingLevels[i]; j++) AddToParNoMarkers(nesting[j]);  
                    AddToPar(lines[i]);
                }
            };

            BoolInlineFunction DoLineStart = delegate() {
                int level = NestingLevelHighBound(minNestingLevel);
                if (minNestingLevel > level) minNestingLevel = level;
                lineNestingLevels.Add(level);
                while (MunchIf(" ")) word.Append(' ');
                return MunchIf(")\n");
            };

            if (DoLineStart()) { Finish(); return; }
            while (true) {
                if (EOF()) { lines.Add(FinishWord()); Finish(); return; }   // EOF: complete last line and close the whole thing
                else if (MunchIf('\n')) { word.Append('\n'); lines.Add(FinishWord()); if (DoLineStart()) { Finish(); return; } }
                else word.Append(ToHTML(Munch()));
            }
        }


        /// <summary>Process chars until the end of long HTML.</summary>
        private void ProcessVerbatimHTML()
        {
            List<string> lines = new List<string>();
            List<int> lineNestingLevels = new List<int>();
            int minNestingLevel = nesting.Count;

            InlineFunction Finish = delegate() {
                for (int i = 0; i < lines.Count; i++) {
                    for (int j = minNestingLevel; j < lineNestingLevels[i]; j++) AddToParNoMarkers(nesting[j]);
                    AddToParNoMarkers(lines[i]);  // no markers in long HTML, period!
                }
            };

            BoolInlineFunction DoLineStart = delegate() {
                int level = NestingLevelHighBound(minNestingLevel);
                if (minNestingLevel > level) minNestingLevel = level;
                lineNestingLevels.Add(level);
                while (MunchIf(" ")) word.Append(' ');
                return MunchIf(">\n");
            };

            if (DoLineStart()) { Finish(); return; }
            while (true) {
                if (EOF()) { lines.Add(FinishWord()); Finish(); return; }   // EOF: complete last line and close the whole thing
                else if (MunchIf('\n')) { word.Append('\n'); lines.Add(FinishWord()); if (DoLineStart()) { Finish(); return; } }
                else word.Append(Munch());
            }
        }

        //////////////////////////////////////////////////////////////////////////

        string MakeExternalLink(string uri, string tooltip, string text) { 
            // Is it a Colabo link? Don't display the little arrow.
            if (uri.Contains("?server=") && uri.Contains("&database=") && uri.Contains("&id="))
                return string.Format("<a class=\"external\" href=\"{0}\" title=\"{1}\">{2}</a>", ToURI(uri), ToHTML(tooltip), ToHTML(text)); 
            return string.Format("<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAALAQMAAACefJUiAAAABlBMVEVAAIAAZszmE0ciAAAAAXRSTlMAQObYZgAAACVJREFUeF5j4H/AwKLA8EmBoUWBoWMBw8QEhi4gu4GhAYz+NwAAk9wKEKkXsnsAAAAASUVORK5CYII=\"><a class=\"external\" href=\"{0}\" title=\"{1}\">{2}</a>", ToURI(uri), ToHTML(tooltip), ToHTML(text)); 
        }
        string MakeLocalLink(string label, string tooltip, string text)  { return string.Format("<a class=\"local\" href=\"#{0}\" title=\"{1}\">{2}</a>", ToLabelName(label), ToHTML(tooltip), ToHTML(text)); }
        string MakeToggleLink(string label, string tooltip, string text) { return string.Format("<a class=\"toggle\" href=\"#_\" title=\"{1}\" onclick=\"toggle('_{0}')\">{2}</a>", ToLabelName(label), ToHTML(tooltip), ToHTML(text)); }
        string MakeLabel(string name)                                    { return string.Format("<a name=\"{0}\"></a>", ToLabelName(name)); }

        public struct ToTranslate
        {
          public CompleteTranslate complete;
          public String source;
        };

        private class TranslationAPI
        {

          public static string EncodeJsString(string s)
          {
            StringBuilder sb = new StringBuilder();
            sb.Append("\"");
            foreach (char c in s)
            {
              switch (c)
              {
                case '\"': sb.Append("\\\""); break;
                case '\\': sb.Append("\\\\"); break;
                case '\b': sb.Append("\\b"); break;
                case '\f': sb.Append("\\f"); break;
                case '\n': sb.Append("\\n"); break;
                case '\r': sb.Append("\\r"); break;
                case '\t': sb.Append("\\t"); break;
                default:
                  int i = (int)c;
                  if (i < 32 || i > 127)
                  {
                    sb.AppendFormat("\\u{0:X04}", i);
                  }
                  else
                  {
                    sb.Append(c);
                  }
                  break;
              }
            }
            sb.Append("\"");

            return sb.ToString();
          }

          public delegate void CallAsync();

          public static void Translate(string urlTranslate, List<ToTranslate> toTranslate, String tgtLang, String srcLang)
          {
            List<ToTranslate> nonEmpty = new List<ToTranslate>();
            foreach (ToTranslate toTrans in toTranslate)
            {
              bool someAlpha = false;
              foreach (char c in toTrans.source) { if (char.IsLetter(c)) { someAlpha = true; break; } }
              if (!someAlpha)
              {
                toTrans.complete(toTrans.source);
              }
              else
              {
                nonEmpty.Add(toTrans);
              }
            }
            JsonObject reqJSON = new JsonObject();
            JsonArray qs = new JsonArray();
            if (srcLang != null) reqJSON.Put("source", srcLang);
            reqJSON.Put("target", tgtLang);
            foreach (ToTranslate toTrans in nonEmpty)
            {
              qs.Push(toTrans.source);
            }
            reqJSON.Put("q", qs);

            try
            {
              // if there is nothing to translate, return the original string
              var request = WebRequest.Create(urlTranslate);
              request.Method = "POST";
              request.ContentType = "application/json;charset=UTF-8";

              // {"source":"en","target":"cs","text":"This needs to be translated"}


              byte[] bytes = Encoding.UTF8.GetBytes(JsonConvert.ExportToString(reqJSON));
              if (bytes.Length > 5120) throw new ArgumentOutOfRangeException("text");

              request.ContentLength = bytes.Length;
              using (var output = request.GetRequestStream())
              {
                output.Write(bytes, 0, bytes.Length);
              }

              IAsyncResult ar = null;
              CallAsync asyncGetResponse = null;
              asyncGetResponse = delegate()
              {
                bool completed = false;
                try
                {
                  using (WebResponse response = request.GetResponse())
                  {
                    string content;
                    using (Stream respStream = response.GetResponseStream())
                    {
                      StreamReader reader = new StreamReader(respStream, Encoding.UTF8);
                      content = reader.ReadToEnd();
                      reader.Close();
                    }

                    object obj = JsonConvert.Import(content);
                    if (obj is JsonObject)
                    {
                      JsonObject root = (JsonObject)obj;
                      if (root.Contains("translations"))
                      {
                        JsonArray translations = (JsonArray)root["translations"];
                        if (translations != null)
                        {
                          for (int t = 0; t < nonEmpty.Count; t++)
                          {
                            JsonObject trans = (JsonObject)translations[t];
                            nonEmpty[t].complete(trans["translation"].ToString());
                          }
                          completed = true;
                        }
                      }
                    }
                  }

                }
                catch (Exception)
                {
                }
                if (!completed)
                {
                  foreach (var it in nonEmpty) it.complete(it.source);
                }
                asyncGetResponse.EndInvoke(ar);
              };

              // not using BeginGetResponse, may block on DNS, and it is hard to handle timeouts properly (see http://msdn.microsoft.com/en-us/library/system.net.httpwebrequest.begingetresponse.aspx)
              ar = asyncGetResponse.BeginInvoke(null, null);

            }
            catch (Exception)
            {
              foreach (var it in nonEmpty) it.complete(it.source);
            }
          }
        }

        private interface IStringTransform
        {
          String Transform(String s);
          String Reverse(String s);
        };

        private abstract class StringReplaceWithSeq: IStringTransform
        {
          protected String source; // used for debugging only
          protected List<String> replaced;

          protected String specSeq_;

          public StringReplaceWithSeq(String specSeq)
          {
            specSeq_ = specSeq;
            replaced = new List<String>();
          }

          public abstract String Transform(String s);
          public String Reverse(String s)
          {
            StringBuilder outS = new StringBuilder();
            int replacedIndex = 0;
            for (int i = 0; i < s.Length; i++)
            {
              if (s[i] == specSeq_[0] && s.Substring(i).StartsWith(specSeq_))
              {
                if (replacedIndex < replaced.Count) outS.Append(replaced[replacedIndex++]);
                i += specSeq_.Length - 1;
              }
              else
              {
                outS.Append(s[i]);
              }
            }
            return outS.ToString();
          }
        };

        private class RemoveTagsAndMarkers : StringReplaceWithSeq
        {
          public RemoveTagsAndMarkers(): base("*")
          {
          }
          public override String Transform(String s)
          {
            source = s;
            StringBuilder outS = new StringBuilder();
            // adapted pro final part of Postprocess
            for (int i = 0; i < s.Length; i++)
            {
              if (s[i] < ' ')
              {
                if (s[i] == (char)ConstructType.LINK)
                {
                  // link is followed by a character keeping link index
                  String extract = s[i].ToString() + s[++i].ToString();
                  outS.Append('*');
                  replaced.Add(extract);
                }
                else
                {
                  // replace any other special character - this includes ConstructType.DELETE_START or ConstructType.DELETE_END
                  String extract = s[i].ToString();
                  outS.Append('*');
                  replaced.Add(extract);
                }
              }
              else if (s[i] == specSeq_[0] && s.Substring(i).StartsWith(specSeq_))
              {
                outS.Append(specSeq_);
                replaced.Add(specSeq_);
              }
              else if (s[i] == '<')
              {
                // remove HTML tag
                int term = s.IndexOf('>', i + 1);
                if (term > i)
                {
                  String tag = s.Substring(i, term+1-i);
                  replaced.Add(tag);
                  outS.Append(specSeq_);
                }
                i = term;
              }
              else
              {
                outS.Append(s[i]);
              }
            }
            return outS.ToString();
          }
        };

        private struct HTMLEncode
        {
          public char c;
          public string name;

          public HTMLEncode(string name, char c)
          {
            this.c = c;
            this.name = name;
          }
        };

        static readonly HTMLEncode[] HTMLEncoding = // codes based on http://www.webune.com/forums/times-html-code.html
        {
          new HTMLEncode("lt",'<'),
          new HTMLEncode("gt"   ,'>'),
          new HTMLEncode("apos" ,'\''),
          new HTMLEncode("quot" ,'"'),
          new HTMLEncode("amp"  ,'&'),
          new HTMLEncode("nbsp" ,'\u00A0')
          /*
          new HTMLEncode("sup2" ,'\u00B2'),
          new HTMLEncode("bdquo",'\u201E'),
          new HTMLEncode("sbquo",'\u201A'),
          new HTMLEncode("ldquo",'\u201C'),
          new HTMLEncode("lsquo",'\u2018'),
          new HTMLEncode("times",'\u00D7'),
          new HTMLEncode("ndash",'\u2013'),
          new HTMLEncode("rsquo",'\u2019')
           */
          /* All HTML entities used in COMA:
          &plusmn; &le; &ge; &rArr; &ne; &hellip; &larr; &harr; &rarr; &rarr; &mdash; &ndash; &amp; &copy; &reg; &trade; &dagger; &Dagger;
          &deg; &prime; &Prime; &middot; &permil; &lsaquo; &rsaquo; &laquo; &raquo; &thinsp; &sup2; &sup3; &times; &minus; &quot; &rsquo;
          */
        };

        private class HTMLTransform: IStringTransform
        {
          public String Reverse(String s)
          {
            StringBuilder w = new StringBuilder(s.Length * 2);
            for (int i = 0; i < s.Length; i++)
            {
              char c = s[i];
              bool found = false;
              foreach (HTMLEncode enc in HTMLEncoding)
              {
                if (c == enc.c) { w.AppendFormat("&{0};", enc.name); found = true; break; }
              }
              if (!found)
              {
                if (c >= 0x80)
                {
                  w.AppendFormat("&#{0};", Convert.ToInt16(c));
                }
                else
                {
                  w.Append(c);
                }

              }
            }
            return w.ToString();
          }

          public String Transform(String s)
          {
            StringBuilder w = new StringBuilder(s.Length * 2);
            for (int i = 0; i < s.Length; i++)
            {
              char c = s[i];
              if (c == '&')
              {
                // parse HTML encoding, expected ascii code, but handle also some named entities
                int term = s.IndexOf(';', i + 1); // scan until entity name end
                if (term > i)
                {
                  String entName = s.Substring(i + 1, term - (i + 1));
                  if (entName[0] == '#')
                  {
                    uint code = Convert.ToUInt32(entName.Substring(1));
                    w.Append(Convert.ToChar(code));
                  }
                  else
                  {
                    bool found = false;
                    foreach (HTMLEncode enc in HTMLEncoding)
                    {
                      if (entName == enc.name) { w.Append(enc.c); found = true; break; }
                    }
                    if (!found)
                    {
                      w.Append("&" + entName + ";");
                    }
                  }
                  i = term;
                }

              }
              else
              {
                w.Append(c);
              }
            }
            return w.ToString();
          }
        }

        private class RemoveHTMLEntities : StringReplaceWithSeq
        {
          public RemoveHTMLEntities()
            : base("*")
          {
          }

          private static bool IsAlphaNum(string str)
          {
            if (string.IsNullOrEmpty(str))
              return false;

            return (str.ToCharArray().All(c => Char.IsLetter(c) || Char.IsNumber(c)));
          }
          private static bool IsNum(string str)
          {
            if (string.IsNullOrEmpty(str))
              return false;

            return (str.ToCharArray().All(c => Char.IsNumber(c)));
          }

          public override String Transform(String s)
          {
            StringBuilder w = new StringBuilder(s.Length * 2);
            for (int i = 0; i < s.Length; i++)
            {
              char c = s[i];
              bool matched = false;
              if (s[i] == specSeq_[0] && s.Substring(i).StartsWith(specSeq_))
              {
                w.Append(specSeq_);
                replaced.Add(specSeq_);
                matched = true;
              }
              else if (c == '&')
              {
                // parse HTML encoding, expected ascii code, but handle also some named entities
                int term = s.IndexOf(';', i + 1); // scan until entity name end
                if (term > i) // TODO: if the name is too long, it is not an entity
                {
                  String entName = s.Substring(i + 1, term - (i + 1));
                  if (entName[0]=='#' && IsNum(entName.Substring(1)) || IsAlphaNum(entName))
                  {
                    replaced.Add("&"+entName+";");
                    w.Append("*");
                    i = term;
                    matched = true;
                  }
                }
              }
              if (!matched)
              {
                w.Append(c);
              }
            }
            return w.ToString();

          }
        }

        private class ReplaceSeq : StringReplaceWithSeq
        {
          String srcSeq_;

          public ReplaceSeq(String tgt, String src)
            : base(tgt)
          {
            srcSeq_ = src;
          }
          public override String Transform(String s)
          {
            source = s;
            StringBuilder outS = new StringBuilder();
            replaced = new List<String>();
            // adapted pro final part of Postprocess
            for (int i = 0; i < s.Length; i++)
            {
              if (s[i] == specSeq_[0] && s.Substring(i).StartsWith(specSeq_))
              {
                outS.Append(specSeq_);
                replaced.Add(specSeq_);
                i += specSeq_.Length - 1;
              }
              else if (s[i] == srcSeq_[0] && s.Substring(i).StartsWith(srcSeq_))
              {
                outS.Append(specSeq_);
                replaced.Add(srcSeq_);
                i += srcSeq_.Length - 1;
              }
              else
              {
                outS.Append(s[i]);
              }
            }
            return outS.ToString();
          }
        };

        private class TranslateArticlePart
        {
          ArticlePart p;
          List<IStringTransform> transforms;
          EventWaitHandle eventHandle;

          public TranslateArticlePart(ArticlePart p, string urlTranslate, String langCode, TranslateCall translate)
          {
            this.p = p;
            this.eventHandle = new ManualResetEvent(false);
            switch (p.type)
            {
              case PartType.NOTE:
                Complete(null);
                return;
            }
            transforms = new List<IStringTransform>();
            transforms.Add(new RemoveTagsAndMarkers());
            transforms.Add(new HTMLTransform());
            transforms.Add(new RemoveHTMLEntities());
            transforms.Add(new ReplaceSeq("<>", "*"));

            String processing = p.s;
            foreach (var s in transforms)
            {
              String processed = s.Transform(processing);

              String verify = s.Reverse(processed);
              //System.Diagnostics.Debug.Assert(processing == verify); // different HTML encoding is possible
              processing = processed;
            }
            // translate the text
            translate(urlTranslate, Complete, langCode, processing, null);
          }

          public void WaitOne() { eventHandle.WaitOne(); }

          public void Complete(String translated)
          {
            if (translated != null && translated.Length > 0)
            {
              String processing = translated;
              for (int i = transforms.Count; --i >= 0; )
              {
                String processed = transforms[i].Reverse(processing);
                processing = processed;
              }
              p.s = processing;
              transforms = null;
            }
            p = null;
            eventHandle.Set();
          }

        };

        private void Translate(string urlTranslate)
        {
          // TODO: pass list of supported languages into Translate
          if (_languages.Count == 0) return;
          String langCode = _languages[0];
          // translate all articleParts
          TranslateArticlePart[] pending = new TranslateArticlePart[articleParts.Count];
          for (int i = 0; i < articleParts.Count; i++)
          {
            pending[i] = new TranslateArticlePart(articleParts[i], urlTranslate, langCode,_translate.Translate);
          }
          _translate.FlushAll();
          foreach (var p in pending) p.WaitOne(); // EventWaitHandle.WaitAll(completed) did not work because of [STAThread]
        }

        /// <summary>
        /// Take all articleParts and join them into a string, putting appropriate tags around them.
        /// </summary>
        private string Postprocess(string urlTranslate)
        {
          if (!String.IsNullOrEmpty(urlTranslate)) Translate(urlTranslate);

          StringBuilder res = new StringBuilder();
            List<ArticlePart> lastPart = new List<ArticlePart>();

            // Function: Close the deepest HTML structure.
            InlineFunction ClosePart = delegate() { 
                ArticlePart a = lastPart[lastPart.Count-1];
                if (a.type == PartType.NOTE && targets[a.s].endPosition == -1) {  // the second test is against smartasses who redefine a note inside of itself ;)
                    targets[a.s].endPosition = res.Length;
                    res.Append(((char)ConstructType.DELETE_END).ToString());
                }
                if ((int)a.type < PartHTMLEnd.Length) res.Append(PartHTMLEnd[(int)a.type]);
            };

            // Function: Close a structure with same-depth vertical space (e.g. if it isn't a list).
            InlineFunction ClosePartVerticalSpace = delegate() {
                PartType t = lastPart[lastPart.Count-1].type;
                switch (t) {
                    case PartType.ITEM_MINUS:    case PartType.ITEM_PLUS:     case PartType.ITEM_STAR:
                    case PartType.ITEM_NUMBER:   case PartType.ITEM_LETTER:   case PartType.ITEM_ROMAN:
                        return;
                }
                ClosePart();
            };

            int articlePartIndex = 0;

            BoolInlineFunction ContainsOnlyVerticalSpace = delegate() {
                int nl = articleParts[articlePartIndex].nestingLevel;
                for (int i = articlePartIndex+1; i < articleParts.Count; i++) {
                    if (articleParts[i].nestingLevel <= nl) break;
                    if (articleParts[i].type == PartType.VERTICAL_SPACE || articleParts[i].type == PartType.NOTE) continue;
                    return false;
                }
                return true;
            };
            InlineFunction DeleteVerticalSpaceAtNoteEnd = delegate() {
                int i;
                int nl = articleParts[articlePartIndex].nestingLevel;
                for (i = articlePartIndex+1; i < articleParts.Count; i++) {
                    if (articleParts[i].nestingLevel <= nl) break;
                }
                if (articleParts[i-1].type == PartType.VERTICAL_SPACE) articleParts[i-1].number = 0;
            };

            foreach (ArticlePart p in articleParts) {
                // close everything below the current nesting level
                while (lastPart.Count-1 > p.nestingLevel) { ClosePart(); lastPart.RemoveAt(lastPart.Count-1); }

                // We always want the previous ArticlePart to be defined (because of lists).
                // => if we've just entered this nesting level, add dummy parts for all higher levels (now there's only 1 at most, but this might change)
                while (lastPart.Count-1 < p.nestingLevel) lastPart.Add(new ArticlePart(PartType.NESTING_LEVEL_START, "", 0, lastPart.Count));

                res.Append('\n').Append(Spaces(p.nestingLevel));
                
                switch (p.type) {
                    case PartType.VERBATIM_HTML:   
                        ClosePart(); res.Append(p.s); break;
                    case PartType.PARAGRAPH: case PartType.PARAGRAPH_ENDSWITHPRE:
                        ClosePart(); res.AppendFormat("<p>{0}</p>", p.s); break;
                    case PartType.HEADER_HASH: case PartType.HEADER_EQUALS:
                        ClosePart(); res.AppendFormat("<h{0}>{1}</h{0}>", p.number, p.s); break;
                    case PartType.UNDERLINED_HEADER:     
                        ClosePart(); res.AppendFormat("<h{0} class=\"underlined\">{1}</h{0}>", p.number, p.s); break;
                    case PartType.HORIZONTAL_RULE:
                        ClosePart(); res.AppendFormat("<hr class=\"h{0}\">", p.number); break;
                    case PartType.CITATION:
                        ClosePart(); res.Append(PartHTMLStart[(int)p.type]); break;
                    case PartType.VERTICAL_SPACE:
                        ClosePartVerticalSpace();   // Vertical space won't close lists in the current level.
                        if (p.number > 0) {
                            res.Append("<div style=\"font-size:0.5em");  // TODO: This could work nicely in BLOCK_CODE, too.
                            if (p.number > 1) { res.AppendFormat(";margin-bottom:{0}em", p.number-1); }
                            res.Append("\">&nbsp;</div>");
                        }
                        break;
                    case PartType.ITEM_STAR:
                    case PartType.ITEM_PLUS:
                    case PartType.ITEM_MINUS: // continue an opened list? needs to have the same type
                        if (lastPart[p.nestingLevel].type != p.type) { ClosePart(); res.Append(PartHTMLStart[(int)p.type]).Append("\"><li>"); }
                        else res.Append("</li><li>");
                        break;
                    case PartType.ITEM_NUMBER:
                    case PartType.ITEM_LETTER:
                    case PartType.ITEM_ROMAN: // continue an opened list? needs to have the same type and increase by 1
                        if (lastPart[p.nestingLevel].type != p.type || lastPart[p.nestingLevel].number != p.number-1) {
                            ClosePart();
                            res.Append(PartHTMLStart[(int)p.type]).Append(p.number).Append("\"><li>");
                        }
                        else res.Append("</li><li>");
                        break;
                    case PartType.ITEM_AUTO:
                        switch (lastPart[p.nestingLevel].type) {
                            case PartType.ITEM_NUMBER: 
                            case PartType.ITEM_LETTER:
                            case PartType.ITEM_ROMAN:
                                p.type = lastPart[p.nestingLevel].type; p.number = lastPart[p.nestingLevel].number + 1; break;
                            default:
                                p.type = PartType.ITEM_NUMBER; p.number = 1; break;
                        }
                        goto case PartType.ITEM_NUMBER;
                    case PartType.NOTE:
                        if (ContainsOnlyVerticalSpace()) goto case PartType.LABEL;  // notes without a body are only labels
                        ClosePart();
                        
                        res.Append(((char)ConstructType.LINK).ToString());  // add a dummy link before the note
                        res.Append(((char)links.Count).ToString());
                        links.Add(new Link(LinkType.HERE_UNLESS_PASTED, "", p.s, ""));

                        res.Append(((char)ConstructType.DELETE_START).ToString());
                        if (targets.ContainsKey(p.s)) targets.Remove(p.s);
                        targets.Add(p.s, new Target(TargetType.NOTE, res.Length));
                        DeleteVerticalSpaceAtNoteEnd();
                        break;
                    case PartType.LABEL:
                        ClosePart();
                        res.Append(MakeLabel(p.s));
                        if (targets.ContainsKey(p.s)) targets.Remove(p.s);
                        targets.Add(p.s, new Target(TargetType.LABEL)); 
                        break;
                }
                lastPart[p.nestingLevel] = p;  // this was the last part of the current level
                articlePartIndex++;
            }

            // Close every part that's left.
            while (lastPart.Count != 0) { ClosePart(); lastPart.RemoveAt(lastPart.Count-1); }


            int maxPasses = 6;
            bool unexpandedLinkSeen = true;
            string imm = "";

            // Convert to string `imm` and copy its substrings into targets. Recursively!
            for (int pass = 1; pass <= maxPasses; pass++)
            {
                if (pass != 2) imm = res.ToString();  // first pass doesn't change anything, only collects links
                res = new StringBuilder(imm.Length);

                int deleteDepth = 0;
                if (!unexpandedLinkSeen) pass = maxPasses;  // last pass! But always at least 2.
                unexpandedLinkSeen = false;
                
                if (pass == 1) foreach (KeyValuePair<string, Target> st in targets) { st.Value.res = imm.Substring(st.Value.startPosition, st.Value.endPosition - st.Value.startPosition); }

                // Finally, resolve links.
                // TODO: detect note-link cycles, display only the first seen note from every cycle
                for (int i=0; i<imm.Length; i++) {
                    if (pass == maxPasses && imm[i] == (char)ConstructType.DELETE_START) deleteDepth++;
                    else if (pass == maxPasses && imm[i] == (char)ConstructType.DELETE_END) deleteDepth--;
                    else if (imm[i] == (char)ConstructType.LINK) {
                        unexpandedLinkSeen = true;
                        Link link = links[imm[++i]];
                        Target target;

                        if (deleteDepth == 0) {
                            if (pass == maxPasses) { res.Append("<b class=\"error\">!</b>"); continue; }
                            
                            // Is it a link to a named target?
                            if (!targets.TryGetValue(link.target, out target)) {
                                // No: is it an URL+tooltip? Add it as a reference!
                                string[] url_tooltip = link.target.Trim().Split(new char[]{' '}, 2);
                                if (CanBeURI(url_tooltip[0])) {
                                    if (targets.ContainsKey(link.target)) targets.Remove(link.target);  // Make a generic reference name with both the URL and the tooltip (if present)
                                    targets.Add(link.target, new Target(TargetType.REFERENCE, url_tooltip));
                                    target = targets[link.target];
                                }
                                // Otherwise just show the original text.
                                else { res.Append(link.original); continue; }
                            }

                            switch (target.type) {
                                case TargetType.LABEL:  // labels can only be pointed to (like [this] or ^[this]) - no pasting
                                    switch (link.type) {
                                        case LinkType.NORMAL:
                                        case LinkType.LOCAL: res.Append(MakeLocalLink(link.target, link.target, link.text)); break;
                                        default:             res.Append(link.original); break;
                                    }
                                    break;
                                case TargetType.NOTE:
                                    switch (link.type) {
                                        case LinkType.NORMAL:
                                        case LinkType.LOCAL:  res.Append(MakeLocalLink(link.target, link.target, link.text)); break;  // TODO: `target.res` as a tooltip?
                                        case LinkType.TOGGLE: target.hidden = true;  res.Append(MakeToggleLink(link.target, link.target, link.text)); break;
                                        case LinkType.LEFT: case LinkType.RIGHT: case LinkType.HERE:
                                            target.pasted = true; res.Append(MakeLabel(link.target));
                                            res.AppendFormat("<div class=\"comanote {0} {4}\" id=\"_{3}\"><div class=\"comanoteheader\">{1}</div>\n<div class=\"comanotebody\">{2}</div></div>",
                                              linkTypeClass[(int)link.type], ToHTML(link.text), target.res, ToLabelName(link.target), target.hidden?"hidden":""); break;
                                        case LinkType.HERE_UNLESS_PASTED:
                                            if (pass != 1) {
                                                if (!target.pasted) {
                                                    target.pasted = true; res.Append(MakeLabel(link.target));
                                                    res.AppendFormat("<div class=\"comanote {0} {4}\" id=\"_{3}\"><div class=\"comanoteheader\">{1}</div>\n<div class=\"comanotebody\">{2}</div></div>",
                                                      linkTypeClass[(int)link.type], ToHTML(link.target), target.res, ToLabelName(link.target), target.hidden?"hidden":""); break;
                                                }
                                            }
                                            else res.Append((char)ConstructType.LINK).Append(imm[i]);  // survives to next pass
                                            break;
                                    }
                                    break;
                                case TargetType.REFERENCE:
                                    switch (link.type) {
                                        case LinkType.NORMAL: res.Append(MakeExternalLink(target.url, target.tooltip, link.text)); break;
                                        case LinkType.LOCAL: res.Append(MakeLocalLink(link.target, target.tooltip, link.text)); break;
                                        case LinkType.TOGGLE: target.hidden = true; res.Append(MakeToggleLink(link.target, target.tooltip, link.text)); break;
                                        // TODO: resolve type and show an <img> or <iframe> here (or load the external file?)
                                        case LinkType.LEFT: case LinkType.RIGHT: case LinkType.HERE:
                                            target.pasted = true; res.Append(MakeLabel(link.target));
                                            res.AppendFormat("<img class=\"{0} {5}\" src=\"{1}\" title=\"{2}\" alt=\"{3}\" id=\"_{4}\">",
                                              linkTypeClass[(int)link.type], ToURI(target.url), ToHTML(target.tooltip), ToHTML(link.text), ToLabelName(link.target), target.hidden?"hidden":""); break;
                                    }
                                    break;
                            }
                        }
                    }
                    else {
                        if (deleteDepth == 0) res.Append(imm[i]);
                    }
                }
            }
            
            return res.ToString();
        }
    }
}

