﻿Markdown  -  A text-to-HTML conversion tool for web writers
-----------------------------------------------------------

- http://daringfireball.net/projects/markdown/
- Markdown is free software, available under the terms of a BSD-style open source license. (http://daringfireball.net/projects/markdown/license) 
