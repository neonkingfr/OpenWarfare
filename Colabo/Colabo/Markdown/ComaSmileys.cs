using System;

namespace Coma
{
    public partial class ComaParser 
    {
        private struct Smiley
        {
            public int shift;
            public string[] text;  // the last one is always the image
            public Smiley(int shift, params string[] text)
            {
                this.shift = shift;
                this.text = text;
            }
        }

        /// <summary>If input matches a smiley, write it to `par` and return true.</summary>
        private bool TryConvertSmileys()
        {
            if (":;X>#oO^98B|\\".Contains(Peek().ToString())) {  // premature exit (optimization - there are lots of smileys)
                foreach (Smiley sm in smileys)  for (int i = 0; i < sm.text.Length - 1; i++) {
                    if (sm.text[i].Length > 2 || (par.Length==0 || par[par.Length-1]==' ')) {  // 2-char smileys must have an explicit space before them (or be at the line start)
                        if (MunchIf(sm.text[i])) {
                            ResolveEmphasisAtConstruct();
                            AddToPar("<img style=\"margin-bottom:", (sm.shift - 2).ToString(), "px\" alt=\"", ToHTML(sm.text[i]), "\" title=\"", ToHTML(sm.text[i]),
                                    "\" src=\"data:image/gif;base64,", sm.text[sm.text.Length - 1], "\">");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        // Smileys: generated with an external program.
        private static readonly Smiley[] smileys = 
        {
new Smiley( 0, ":chuckle:",":giggle:",":)))",":-)))", "R0lGODlhDwAPAPIAAGAAgAAAAP/gQP///7ukLLu7uwAAAAAAACH5BAEAAAAALAAAAAAPAA8AAANDCKrRvRA0Iip5MNDKxV1aJ36SaAYSdQXc5DUVK7ByLZsifN5zOMeFAe0jWwWDASFjMxsEB1DUsgWtShm+ZDSCdWAACQA7"),
new Smiley( 0, ":smile:",":-)",":)",                "R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI1hB2Zx5Aj4lgswIjFPDb77WCBKDqXMEaplmTrisKeK89qh3rp9iovcpqBTLYhAscKNDi+RgEAOw=="),
new Smiley( 0, ":wink:",";-)",";)",                 "R0lGODlhDwAPAPIAAGAAgAAAAP/gQLukLP///7u7uwAAAAAAACH5BAEAAAAALAAAAAAPAA8AAAM8CKrRvRC0Iep4MNDKxV1aJ34SF4hVIG3CmYaXm87ei97NjU6e41MfV6BAKBaEjI3PRVrdmoyQBROR+CIJADs="),
new Smiley( 0, ":laugh:",":-D",":D",                "R0lGODlhDwAPAPIAAGAAgP/gQAAAAP///7ukLLu7uwAAAAAAACH5BAEAAAAALAAAAAAPAA8AAANCCKrSvRA0Eip5UNDKw11a0HDjJ3WagErbWLmX2s2k6ty3aBdD7w8FW4r36wU/u2JQxKAIiioTS+fwCCITTiwCukUSADs="),
new Smiley( 0, ":rofl:",                            "R0lGODlhDwAPAPIAAGAAgP/gQAAAAP///7ukLLu7uwAAAAAAACH5BAEAAAAALAAAAAAPAA8AAANCCKrSvRA0Eip5UFDXwl1aIAwkKXgCIxYlWZypJrCtK8N0/Yow5/eVoFA4qXSMJ5SEeCEyNkme8jm0pDIh6zUi4UQSADs="),
new Smiley( 0, ":lol:",":lmao:","X-D","XD",         "R0lGODlhDwAPAPIAAGAAgAAAAP///4cZRf/gQLukLP+goAAAACH5BAEAAAAALAAAAAAPAA8AAAM/CKrRvRA4Ql0MIoeC9eqZAHqSZopeE57h5Lwv0chrHch3MOw8n2+6Xg8okQl3OQbnOCAqgz7OpWGoWiKMVyQBADs="),
new Smiley( 0, ":yum:",":-9",                       "R0lGODlhDwAPAPIAAGAAgAAAAP/gQLukLP+goESq/wAAAAAAACH5BAEAAAAALAAAAAAPAA8AAANBCKrRvRC0Iep4MNDKxV1aJ36S13CTEEhUULmuasZifdZqUBQ5HhAEXY6yeQFdnxjtp2RsHDcSCydlhCyYiAQaSQAAOw=="),
new Smiley( 0, ":eww:",">:-P",">:-p",">:P",">:p",   "R0lGODlhDwAPAPIAAGAAgAAAAP/gQLukLP+goIcZRQAAAAAAACH5BAEAAAAALAAAAAAPAA8AAANACKrRvRC0Iep4MNDKxV1aJ36SaAYS5Wir13QBF8tzVdfCa57htU6uUixAKBCGOcaGSDhaUEqbjgQKPaERySqSAAA7"),
new Smiley( 0, ":serious:",":-[",":[",              "R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI2hB2Zx5Aj4lgswIjFPDb77QiKNYrOJWIBNq3qq7renMz2Y2ebLNYqcpG5QCcbEdGRUBqOUaMAADs="),
new Smiley( 0, ":happy:",":-]",":]",                "R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI3hB2Zx5Aj4lgswIjFPDb77Xhi4AxT4D2ngLJYiybiLLtj9GjKDm3tjENcZqASkVTpSCgNx65RAAA7"),
new Smiley( 0, ":awesome:",":glee:",";-D",";D",     "R0lGODlhDwAPAPIAAGAAgP/gQAAAAIcZRbukLP////+goAAAACH5BAEAAAAALAAAAAAPAA8AAANFCKrSvRA0Eip5UNDKw11OF35S0VRCYQqBIKUmqsaXFrYOi3e8yOZA3aQ1KBoHrM/vaPwxKIKiYZp0PXEjK0jDqUW2jkgCADs="),
new Smiley( 0, ":blush:","#-)",                     "R0lGODlhDwAPAPIAAGAAgAAAAP/gQLukLP8AAAAAAAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAAM+CKrRvRC0Iep4MNDKxV1aJ34SF5imtAln1XpN977sSQh3d9/NjnM+wkRE/MxQLsbKwSGpiLDI0IKJSByBSAIAOw=="),
new Smiley( 0, ":blushhappy:","#-]","=^_^=",        "R0lGODlhDwAPAPIAAGAAgP/gQAAAALukLP8AAAAAAAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAAM/CKrSvRC0Eep4UNDKw11aJ36SaAoSdQnc5DUVG7ByLZtiQwT7XvW8CXDI+7CAP96MscGRUk5UJmTBRCQO6SIBADs="),
new Smiley( 0, ":kiss:",":-*",":*",                 "R0lGODlhDwAPAPIAAGAAgP/gQAAAALukLP8AAP+goAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAAM9CKrSvRC0Eep4UNDKw11aJ36SaAoS5XDrJZhwUxWFSFdyRwR7N3GEYJDzeQGHLMYGRkoxUZmQBROROKCLBAA7"),
new Smiley( 0, ":inlove:",                          "R0lGODlhDwAPAPIAAGAAgP/gQAAAAP8AALukLAAAAAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAANCCKrSvRA0Eip5UNDKw11aJ35SMJjceQoSMbyvCQ+XUMFpHDSpeFY8kXAXGnI+NiAnuWNsiExSayhlFD2YiMTBgiQAADs="),
new Smiley( 0, ":horny:",                           "R0lGODlhDwAPAPIAAGAAgAAAAP+goP////8AALt0dAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAANCCKrRvRC0Imp5MNDKxV1O0AkhM5xiFZxDILEoCbdrG9d4zYo6Gjac34QgIBqLxY/oyCQxmkzXExmNTDgXaURSgiQAADs="),
new Smiley( 0, ":listen:",                          "R0lGODlhEAAPAPIAAGAAgP/gQLukLP///wAAALu7uwAAAAAAACH5BAEAAAAALAAAAAAQAA8AAANFCKrUvZAREap4cdZtCXQEF4BLMwxhZaIAeKLj6r4wQRPF+ob2iaOgBuXi8GlEnJ4nhVQVWMdmI8egIC8tj8QaIJYypEgCADs="),
new Smiley( 0, ":whisper:",                         "R0lGODlhDwAPAPIAAGAAgAAAAP/gQLukLP///7u7uwAAAAAAACH5BAEAAAAALAAAAAAPAA8AAAM8CKrRvRC0Iep4MNDKxV1aJ36SaAbSZnJXIDht08mVW4v0Otv56DQEG05CKAaCPmKxgGShGEWjBQP6OSIJADs="),
new Smiley( 0, ":proud:",                           "R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI1hB2Zx5Aj4lgswIjFPBbDqwWIoGCl2GVqGKxZS8Kmkibu+tweqkeiA1ptEMFdIyehNBy0RgEAOw=="),
new Smiley( 0, ":smug:",                            "R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI1hB2Zx5Aj4lgsQAHvPFZFoWzOR5KBc5XkFJRK+yWqBIP1HFv4t93zidqJgqohQkcDNhyvRgEAOw=="),
new Smiley( 0, ":ashamed:",                         "R0lGODlhDwAPAPIAAGAAgP+goAAAALt0dP8AAAAAAAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAAM/CKrSvRC0Eep4UNDKw11aJ34SJ5imtAVn1XpN974sLcoCEeh6xe+Tn3D3Of18OxZjdSOpbrDIhHMRREAOKyQBADs="),
new Smiley( 0, ":sweat:",":-S",":S",                "R0lGODlhDwAPAPIAAGAAgAAAAP/gQLukLESq/wAAAAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAANACKrRvRC0QYQYD4Zhq73BsoHfiDFC01lqOLJCFcTg7M3wbcN8rH5AIGESLJ5wRGKKwWGNZidmERRR1iIiRwiSAAA7"),
new Smiley( 0, ":wtf:","o_O","O_o",                 "R0lGODlhDwAPAPIAAGAAgP/gQAAAAP///7ukLAAAAAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAANACKrSvRA0Eip5UNDKw12O0AUhM5xiJZyDILEoCbdrG9d4zYo62uyWkOPSGHE+KSPHJdkoPwxnBwrSHDERSQmSAAA7"),
new Smiley( 0, ":sweatdrop:","^_^'",                "R0lGODlhDwAQAPIAAGAAgAAAAP/gQLukLESq/4cZRQAAAAAAACH5BAEAAAAALAAAAAAPABAAAANDCLqs1CpI6ZocIg/5VsBZKBBkBIICRnZBimor8In0uLU3fQkSn7W/YK9W2xFDG1loMuSZfoUoMPnkDamemYYCkTEbCQA7"),
new Smiley( 0, ":cheerful:","^_^",                  "R0lGODlhDwAPAPIAAGAAgAAAAP/gQLukLIcZRQAAAAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAAM8CKrRvRC0Iep4MNDKxV1aJ36SaAbSZnJX4DXiJMDuzNUuvMbhbqG1mSOISlUChGSNZBQui6DeCxoZUgEJADs="),
new Smiley( 0, ":grin:",">:-D",">:D",               "R0lGODlhDwAPAPIAAGAAgP/gQAAAALukLP///7u7uwAAAAAAACH5BAEAAAAALAAAAAAPAA8AAANDCKrSvRC0Eep4UNDKw13axF0hUzUcGgjStgrvqJ5pN3d47Oz7exUEgiA4FH5gw6RQuGJQikvYx/lyeFgZjQwbkewiCQA7"),
new Smiley( 0, ":nod:",                             "R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI3hB2Zx5Aj4lgswIjFPDb77Xhi4FwiNgVYsqoRK7ieC5/ZE+Opusk2WRpJgMGcC8TpDIkNBeVQAAA7"),
new Smiley( 0, ":wasntme:","9_9",                   "R0lGODlhDwAPAPIAAGAAgP/gQAAAAP///7ukLAAAAAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAANCCKrSvRA0Eip5UNDKw11a4IiNJzDiMDRqeYZCG6+CWaetulYlvuq3Hqk24oU6SJMkiTxJNp3bhwGdKDNHWwTkcC4SADs="),
new Smiley( 0, ":puppyeyes:",                       "R0lGODlhDwAPAPIAAGAAgAAAAP/gQP///7u7u7ukLAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAANFCKrRvRC0Imp5MNDKxV1Ow4UBQwwDEQjBmZbt0LznrAaojOPo7Dsy3a+GUxFfDZeKpXyQRiHGpsP5SKmWUkZTxUQkUUgCADs="),
new Smiley( 0, ":sorrow:",":-(((",";-(((",":(((",";(((", "R0lGODlhDwAPAPIAAGAAgP/gQAAAAESq/4cZRbukLP////+goCH5BAEAAAAALAAAAAAPAA8AAANJCKrSvRC0Emp5UNDKw11aJ35S1XBnIEiUMIiD4KWvGNTn8Dr5LusCg1Do8hWAhGSy+CkKlEsgAwl1gmJQgmuVeR6+Wm5EwoskAAA7"),
new Smiley( 0, ":frown:",":sad:",":-(",":(",        "R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI3hB2Zx5Aj4lgswIjFPDYG/GkB4mVCOHZfeLYT25YgzNKwmT24udVKjYDcPpvgrsjpSCgNx69RAAA7"),
new Smiley( 0, ":cry:",":crying:",";-(",";(",       "R0lGODlhDwAPAPIAAGAAgAAAAP/gQLukLESq/wAAAAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAAM/CKrRvRC0Iep4MNDKxV1aJ36SFXChEEhUI7rXic6oXNm2ahN4N1UEgshUEgiHKsZGZyOxjh0nI+XBRCSOFSQBADs="),
new Smiley( 0, ":meh:",":neutral:",":unfazed:",":-|",":|", "R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI1hB2Zx5Aj4lgswIjFPDb77WCBKDqXMEaplmTrisKeK89qFystG9ql6eMhThkQB8cKNI6KRgEAOw=="),
new Smiley( 0, ":unsure:",":whatever:",":-/",":/",@":-\",@":\", "R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI3hB2Zx5Aj4lgswIjFPDb77WCBKDqXMEaplmTrisKeK89qdz/YVqOlaZMEOKcMiINjDRsIxfJQAAA7"),
new Smiley( 0, ":confused:",":-?",":?",             "R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI3hB2Zx5Aj4lgswIjFPDYLpQVIBHpiR2IBNq3qq7rpTMqeid7sWSuy6LhkJiGOcAjk5FoNpaJRAAA7"),
new Smiley( 0, ":surprised:",":-O",":O",            "R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI5hB2Zx5Aj4lgswIjFPHYG/GyO8GkmSToXGpVakrkuOctxDd5rlm0z30LseCJVThJoPDCeBkeRZBQAADs="),
new Smiley( 0, ":kill:",">:-(((",">:(((",           "R0lGODlhDwAPAPIAAGAAgAAAAP/gQLukLP////8AALu7uwAAACH5BAEAAAAALAAAAAAPAA8AAANCCKrRvRC0Iep4MFw99l1aQBDhGDKCWJDEGqQSVc20sNFOg79VYIwEAy81nHR0PU1t+RkuewzZ8xOdBiKT2SYCykUSADs="),
new Smiley( 0, ":angry:",":mad:",">:-(",">:(",      "R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI5hB2Zx5Aj4lgswIjFPFZ0GWgBgoWR6TmXFybYhHpl5mZ2Gd84pPTmlqOlVDoQZ5XZVD6iUQPRaxQAADs="),
new Smiley( 0, ":screaming:",":-@",":@",            "R0lGODlhDwAPAPIAAGAAgAAAAP8AALsAAIcZRf///wAAAAAAACH5BAEAAAAALAAAAAAPAA8AAANECKrRvRC0Iep4MNDKxV2aEHSiFzDWWIWitFled6liU9M2qdek07OigpD2UQUISKSRQTkmCaMP05nUnDIBoRYTkTiuiwQAOw=="),
new Smiley( 0, ":annoyed:",":irritated:",">:-|",">:|", "R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI1hB2Zx5Aj4lgswIjFPDb77QiKNYrOJWIBNq3qq7renMz2Y2ebO5YBgpqBTrYhoiOhNByjRgEAOw=="),
new Smiley( 0, ":mute:",":-X",":X",                 "R0lGODlhDwAPAPIAAGAAgAAAAP/gQLukLP///7u7uwAAAAAAACH5BAEAAAAALAAAAAAPAA8AAANDCKrRvRC0Iep4MNDKxV1aFXCjFzBkKkpb516l0JDxLBPFGOClfec8WqvBK31qPAJBx6DscrLRpxntTEEhCyYicZwgCQA7"),
new Smiley( 0, ":cool:","8-)","B-)",                "R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI2hB2Zx5Aj4lgswIjFPDb77Xhi4FwiNinqqkYKlAaurCXYM5tnHZ40WcooJMCgCMTpEIuNVaMAADs="),
new Smiley( 0, ":yawn:","X-O",                      "R0lGODlhDwAPAPIAAGAAgP/gQAAAAIcZRbukLP////+goAAAACH5BAEAAAAALAAAAAAPAA8AAANGCKrSvRA0Eip5UNDKw11a0HDjJ4kbd4lS6owWLBY0LZB3JQw8n4u/XW8QDImGxNjpOMwJGJSdYZo0tYAOzzNj1G4jklckAQA7"),
new Smiley(-2, ":omg:","O_O",                       "R0lGODlhDwARAPIAAGAAgAAAAP/gQP///4cZRbukLAAAAAAAACH5BAEAAAAALAAAAAAPABEAAANLCKrRvRC0Imp5MIwRqtDcojXcx43MWY5ocG1dBbqNHHv1d3s8vjs5G8W0gVkCEhlhuYwhJZQAsym4iC5TAi3SyGK43oioCBOLgJEEADs="),
new Smiley(-1, ":dead:","X_X",                      "R0lGODlhDwAQAPIAAGAAgLukLP+goAAAAP/gQIcZRQAAAAAAACH5BAEAAAAALAAAAAAPABAAAANECKrTvRC0QGp4cNDKyV1aJ36SaA7SZnLXQLgvB7uNBctb/ap2Nc0y3+e2ejEojqQHdXwJCgIXCdR4RjERiWDLzFK7iwQAOw=="),
new Smiley(-1, ":thinking:",">:-/",@">:-\",">:/",@">:\","R0lGODlhDwAQAPIAAGAAgP/gQP///7ukLLu7uwAAAAAAAAAAACH5BAEAAAAALAAAAAAPABAAAANHCKrVvRC0Eep4sNDKw11aJ35S4JyNKVGNILSvl5ruWxfVbKYOp4uiie/E+eBMhBuPcUn2LAVQoRaTRaYE5yV6FWQdkQwmnAAAOw=="),
new Smiley( 0, ":sleep:","X-)","|-)",               "R0lGODlhEgAPAPIAAGAAgAAAAP/gQLukLP8AAAAAAAAAAAAAACH5BAEAAAAALAAAAAASAA8AAANJCKrRvYBIstoQeEwa6xUbxnFA8GGgKARbiargBJlvmAXVq784s/891woTGBqDtFWRuBwEhT8ipHTaOadU1OmKLSUFzmfX8egmAAA7"),
new Smiley( 0, ":punch:",                           "R0lGODlhFQAPAPIAAGAAgAAAAP/gQLukLP///7u7uwAAAAAAACH5BAEAAAAALAAAAAAVAA8AAANOCLoM8a/J9Ya4I05qRbhYsDlW9oFCtp0PCopSYLVuGL/1e19nDv293CtAKBaBspqJ8DA2CYUkSkRkQitAlMoRKDAVUteWAgOHTaMGsJEAADs="),
new Smiley(-1, ":bored:",":-!",":!",                "R0lGODlhDwAQAPIAAGAAgAAAALukLP///7u7u//gQAAAAAAAACH5BAEAAAAALAAAAAAPABAAAANCCKrRvRA0Uap4MNDKy11aJ36SaAYS5YTc2gXeGgzN21Zzbb40+660UkszKBKCLBiuUUSlRIFjEJQ8EpwR4DSSxQISADs="),
new Smiley( 0, ":heart:",                           "R0lGODlhDwAPAPIAAGAAgP8AAAAAALsAAP///wAAAAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAAM5CCqswMKtEUIUdcQVCKnYBz6DWJ2nFoIn1q5WizIUawfag+4jyacbHy8nce2CxaNkaUQuOc7nQpoAADs="),
new Smiley( 0, ":alien:",                           "R0lGODlhDwAPAPEAAGAAgAAAAP///7u7uyH5BAEAAAAALAAAAAAPAA8AAAI1hB2Zx5Aj4lgswIjFPDb77Xhi4FwiNgXnqK5ZIjwf/ChmapGqgvFk6QIBRUJER0JpOBS/QwEAOw=="),
new Smiley( 0, ":cat:",":-3",                       "R0lGODlhDwARAPIAAGAAgLukLP/gQP+goP///wAAALu7uwAAACH5BAEAAAAALAAAAAAPABEAAANSWArcrWqM5eIscgSoQrZBKIyhd2ljqnoFwBXpyxQGQRgwbeNurdy6H65gI/yItiORYzRCnIUAKRqddlTYqWsEE3S/M+kmV908pFlzpWp1rCHuBAA7"),
new Smiley( 0, ":sun:",                             "R0lGODlhDwAPAPIAAGAAgP/gQP+gAAAAALukLP3MEAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAAM/CKoC3Su6ICiVgFarQ9xBKF4MOIbCExLDILYEOrmBS7uqeVbquU8h2kv02LSOoQIE6CMpOpyUR8KzLDEPhyQBADs="),
new Smiley(-1, ":devil:",">:-)",">:)",              "R0lGODlhEQASAPIAAGAAgLsAAP8AAAAAAP///7u7uwAAAAAAACH5BAEAAAAALAAAAAARABIAAANQCDDazg+6NuSqtzIdaxBgoFnaB4Yc5Z0oRgUmK8DS8K3tDA1n1fOgHcvn6ymAwd7weJMlRZTgqEi6FQjYAg/6uI10GVVMVLNkMLvFZH1pJAAAOw=="),
new Smiley( 0, ":angel:","O:-)","O:)",              "R0lGODlhDwARAPIAAGAAgAAAALu7u/+gAP/////gQAAAAAAAACH5BAEAAAAALAAAAAAPABEAAANJCDrcrXCUSemIc4S9ZbmAFwhEKQQZVlUgMDrwqQRlbZeBS973Sdc7QpCwwRWNvyPvthkuZT9iLZrTTZWymdNUhTSBG4jYxekqEgA7"),
new Smiley( 0, ":robot:",                           "R0lGODlhDwARAPEAAGAAgESq/6LU/wAAACH5BAEAAAAALAAAAAAPABEAAAI+hD95yyPDInBQLmqT3gqJD35BZTghOJbJZz7CqLmyO8Jy69Rre+rDeEL9NkGRpvTgDC/FF2kSiEqlTyXHUAAAOw=="),
new Smiley(-1, ":handshake:",":shake:",             "R0lGODlhIAAQAPIAAGAAgAAAALukLP/////gQLu7uwAAAAAAACH5BAEAAAAALAAAAAAgABAAAANtCKrRvTC6F1kQJAtaG9Yc82Xk1o0kYYppu7JtGTBxFtjQTejxfNU7EIznagCDOACP2DIeSTPlE8oERqs1T1YGQxalWUfOYe3GNpMf8KUuDgKD+BvFHY/Q8UKjENeCoh0TDHJvfUqCFYljaYoQCQA7"),
new Smiley( 0, ":rock:",@"\,,/",                    "R0lGODlhEwASAPIAAGAAgAAAALukLP/gQP///7u7u8D/gACAACH5BAEAAAAALAAAAAATABIAAANgCLrR+/AFE6Jlp97YqHaKFgZCkJUNUQVqKAzDFMBly64v3My6mq6woDBGKBR9uWFwVzA2lskSUWUTkgZS3S6kbaCeMUlSSRNxu+Aw5BowEoqzkoXkFaDkF28QtWHszBAJADs="),
new Smiley( 0, ":thumbsup:",":ok:",":rules:",       "R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI2hB95uzESmAwCSkbFiNc4zU1JFjaP9mxYlVQsdm7upgDjeM5e/rmVd+vlEKfMYIhIpIKYm6QAADs="),
new Smiley( 0, ":thumbsdown:",":ko:",":sucks:",     "R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI1hBGZx6YjhloOiiCiFW4Gu1XZtY3dVEZihGUS1Sbj1SAgJNWIkgX68WH9HC7fMDYUGZOKWgEAOw=="),
new Smiley(-1, ":note:",                            "R0lGODlhDwARAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPABEAAAI9hI8Qm7kjQjNPWOkCjCG6vVge0oXCUDoiGmJHx1LjOy4p6WmiK1f72+qsMLqLwoKibIwKhmw4eSajFGe0AAA7"),
new Smiley( 0, ":star:",                            "R0lGODlhDwAPAPIAAGAAgP/gQP+gAP/AIP///wAAAAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAAM5CLog/G/AKYKbbASJVQ3WI4yjBg4E6XzgSbRXY27vFnsaet6LoIO8xe9EqbGCr8EqJ9pEQsKgTJEAADs="),
new Smiley( 0, ":flower:",                          "R0lGODlhDwAPAPIAAGAAgACAAP8AAP+goMD/gAAAAAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAAM6CLogzlC5MV6UNQsVAhMauAWEh4UVWS4iSAmq2bkioJZm42wcsV6MjrADtN1+Ec8xt8gNmZwiBHpJAAA7"),
new Smiley( 0, ":beer:",":pivo:",                   "R0lGODlhDwAPAPIAAGAAgAAAAP/gQP+gAP///7u7uwAAAAAAACH5BAEAAAAALAAAAAAPAA8AAANICKpV4CsyQhqUr5ZKMAgUVwVYMITUQEqBMLynGqxKK9wC4b50O7g510xVA/50JNPKZxQkiZ/mb9iT7mjRn9aJLeKcntpshkkAADs="),
new Smiley( 0, ":coffee:",":kafe:",                 "R0lGODlhDwAQAPIAAGAAgAAAALu7u7sAAP////8AAIBAAAAAACH5BAEAAAAALAAAAAAPABAAAANCCAqmvdDJSN+jKuiNQSAgKARUIBhoOkZmqpKQuW0jnA2hiK+ZUPy/QY1nyhEEQ1gxh9QQBUZkc7EMNW09K680wyoSADs="),
new Smiley( 0, ":cake:",":dort:",                   "R0lGODlhDwAPAPMAAGAAgAAAAPT02cR8NeWjWudENdXAsf9wXqYmFK6IeQAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAAAALAAAAAAPAA8AAARKEMhJq714IpRlOEeBBFcQCEWKCORkCrCRGqx3wnjO3sYQ+z5DwjQYEAgCI8HYGxCLQKixaCqEUtYr9qXr7qRgqelILpdN6LTaFAEAOw=="),
new Smiley( 0, ":pizza:",                           "R0lGODlhDwAPAPIAAGAAgAAAAP8AAOWjWv/gQMR8NbsAAAAAACH5BAEAAAAALAAAAAAPAA8AAANNCKrRvRC0Mmp5MNBBhLDBog2e0HnDxXAEYbTvF46GENtu2pCmUbelwQ5ochh3t1pIUWBQfrBQUzR4WYtMJ+kEXIo2ro8uMqnovGRjJAEAOw=="),
new Smiley( 0, ":tongueout:",":tongue:",":-P",":-p",":P",":p", "R0lGODlhDwAPAPIAAGAAgAAAAP/gQLukLP+goIcZRQAAAAAAACH5BAEAAAAALAAAAAAPAA8AAAM/CKrRvRC0Iep4MNDKxV1aJ34SF5imtAln1XpN974sLcqt7bph7cSwkotQIMwYmwBh2SKpeM1AZMK5SCOMXyQBADs="),
        };

    }
}
