using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.CodeDom.Compiler;
using System.Reflection;

namespace Colabo
{
  public partial class DialogScript : Form
  {
    private FormColabo _colabo;
    private ListBox.ObjectCollection _items;
    private int _index;
    private ScriptInfo _script;

    public string name
    {
      get { return valueName.Text; }
    }
    public string usage
    {
      get { return valueUsage.Text; }
    }
    public string image
    {
      get { return valueImage.Text; }
    }
    public string language
    {
      get { return valueLanguage.Text; }
    }
    public string script
    {
      get { return valueScript.Text; }
    }

    private ScriptInfo GetScript(int index)
    {
      return (ScriptInfo)_items[index];
    }

    public DialogScript(FormColabo colabo, ListBox.ObjectCollection items, int index, bool readOnly)
    {
      _colabo = colabo;
      _items = items;
      _index = index;
      _script = _index < 0 ? null : GetScript(index);

      InitializeComponent();

      if (readOnly)
      {
        buttonOK.Visible = false;
        buttonCancel.Text = "Close";
        this.AcceptButton = buttonCancel;

        valueName.ReadOnly = true;
        valueUsage.ReadOnly = true;
        valueImage.Enabled = false;
        valueLanguage.Enabled = false;
        valueScript.ReadOnly = true;
      }
    }

    private void DialogScript_Load(object sender, EventArgs e)
    {
      valueImage.Items.Clear();
      valueImage.Items.Add("");
      foreach (string icon in _colabo.icons.Keys)
        valueImage.Items.Add(icon);

      valueLanguage.Items.Clear();
      valueLanguage.Items.Add("JScript");
      valueLanguage.Items.Add("VBScript");

      if (_script != null)
      {
        valueName.Text = _script._script._name;
        valueUsage.Text = _script._script._usage;
        valueScript.Text = _script._script._script;

        int index = valueImage.Items.IndexOf(_script._script._image);
        if (index < 0) index = 0;
        valueImage.SelectedIndex = index;

        index = valueLanguage.Items.IndexOf(_script._script._language);
        if (index < 0) index = 0;
        valueLanguage.SelectedIndex = index;
      }
      else
      {
        valueImage.SelectedIndex = 0;
        valueLanguage.SelectedIndex = 0;
      }
    }

    private void DialogScript_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (DialogResult == DialogResult.OK)
      {
        string name = valueName.Text;
        if (name.Length == 0)
        {
          MessageBox.Show("The name should not be empty.", "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
          e.Cancel = true;
          return;
        }
        // check for items duplicity
        SQLDatabase database;
        string user;
        if (_script != null)
        {
          database = _script._database;
          user = _script._script._user;
        }
        else
        {
          // TODO: valid database and user
          database = _colabo.databases[0];
          user = _colabo.databases[0].user;
        }
        for (int i = 0; i < _items.Count; i++)
        {
          if (i == _index) continue; // myself
          ScriptInfo script = GetScript(i);
          if (
            script._database == database &&
            script._script._user.Equals(user) &&
            script._script._name.Equals(name))
          {
            MessageBox.Show("The name is already used.", "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            e.Cancel = true;
            return;
          }
        }

        // syntax check of the script
        Assembly assembly = CompileScript(this.language, this.script, "Script", "Main");
        if (assembly == null)
        {
          e.Cancel = true;
          return;
        }
      }
    }

    public static Assembly CompileScript(string language, string script, string className, string methodName)
    {
      CodeDomProvider provider = CodeDomProvider.CreateProvider(language);
      CompilerParameters par = new CompilerParameters();
      par.GenerateInMemory = true;
      // par.ReferencedAssemblies.Add("System.dll");
      // par.ReferencedAssemblies.Add("System.Windows.Forms.dll");

      CompilerResults results = provider.CompileAssemblyFromSource(par, script);
      if (results.Errors != null && results.Errors.HasErrors)
      {
        string errors = "";
        foreach (CompilerError error in results.Errors) errors += error.ToString() + "\r\n";
        MessageBox.Show(errors, "Script Error(s)");
        return null;
      }

      Assembly assembly = results.CompiledAssembly;
      Type type = assembly.GetType(className);
      if (type == null)
      {
        MessageBox.Show("Missing class " + className, "Script Error(s)");
        return null;
      }
      MethodInfo method = type.GetMethod(methodName, new Type[] { typeof(object) });
      if (method == null)
      {
        MessageBox.Show("Missing method " + className + "." + methodName, "Script Error(s)");
        return null;
      }
      return assembly;
    }

    public static Boolean ExecuteScript(Assembly assembly, string className, string methodName, Scripting scripting)
    {
      try
      {
        Type type = assembly.GetType(className);
        object instance = Activator.CreateInstance(type);
        type.InvokeMember(methodName, BindingFlags.InvokeMethod, null, instance, new object[] { scripting });
        return true;
      }
      catch (System.Exception exception)
      {
        MessageBox.Show(exception.ToString(), "Script Runtime Error");
        return false;
      }
    }
  }
}