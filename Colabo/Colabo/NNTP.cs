using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Globalization;

using Token = Antlr.Runtime.IToken;

using TcpClient = System.Net.Sockets.TcpClient;
using NetworkStream = System.Net.Sockets.NetworkStream;

namespace Colabo
{
  public class NNTPEngine : DBEngine, ArticlesStore
  {
    public NNTPEngine(DBInfo info)
      : base(info)
    {
    }

    // implementation of DBEngine
    public override ArticlesStore GetArticlesStore()
    {
      return this;
    }
    public override bool IsEmptyPathEnabled()
    {
      return true;
    }
    public override DBUserInfo LoadUserInfo()
    {
      return null;
    }
    public override void SaveUserInfo(DBUserInfo userInfo)
    {

    }
    public override void SaveUser(UserInfo userInfo)
    {

    }
    public override void LoadUsers(ref Dictionary<string, UserInfo> users, ref Dictionary<string, string> aliases, NotifyUserChange notifyUserChange, NotifyChat notifyChat)
    {

    }
    public override void SaveUsers(List<ManageUserInfo> users, List<ManageGroupInfo> groups, List<ManageGroupUserInfo> groupUsers)
    {

    }
    public override void LoadUserSettings(ref List<string> groups, ref List<Token> accessRights, ref int homepage)
    {
      accessRights = FormColabo.CompileRuleSet("* => ALLOW:READ; * => ALLOW:TO_READ; * => ALLOW:EDIT");
    }
    public override int LoadHomepage()
    {
      return -1;
    }
    public override void LoadRuleSets(ref List<RuleSet> ruleSets)
    {

    }
    public override void SaveRuleSets(ListBox.ObjectCollection ruleSets)
    {

    }
    public override void LoadBookmarks(ref List<Bookmark> bookmarks, SQLDatabase database)
    {

    }
    public override int SaveBookmark(Bookmark bookmark, SQLDatabase database)
    {
      return -1;
    }
    public override void UpdateBookmark(Bookmark bookmark, SQLDatabase database)
    {

    }
    public override void DeleteBookmark(int id, int parent)
    {

    }
    public override void LoadScripts(ref List<Script> scripts)
    {

    }
    public override void SaveScripts(List<Script> scripts)
    {

    }
    public override void SaveHomepage(int homepage)
    {

    }
    public override void LoadArticles(ref ArticlesCache cache, System.ComponentModel.BackgroundWorker worker)
    {
      using (NNTPConnection conn = NNTPConnection.Connect(_info))
      {
        if (conn == null) return;

        // get indices of articles
        string response = conn.Response("GROUP " + _info._database, null);
        if (!response.StartsWith("211")) return;
        string[] values = response.Split(new char[] { ' ' });
        if (values.Length < 5) return;
        int count = int.Parse(values[1]);
        int start = int.Parse(values[2]);
        int end = int.Parse(values[3]);

        Dictionary<string, int> map = new Dictionary<string, int>();
        CultureInfo culture = new CultureInfo("en-US"); // for time parsing
        string dateFormat = "ddd, d MMM yyyy HH:mm:ss zzz";

        int maxArticles = 4000;

        int progressStart = 20;
        int progressEnd = 30;
        for (int from = start; from <= end; from += maxArticles)
        {
          if (worker != null) worker.ReportProgress(progressStart + (progressEnd - progressStart) * from / count);

          int to = from + maxArticles - 1;
          string range = string.Format(" {0}-{1}", from, to);
          response = conn.Response("XHDR Message-ID " + range, ".");
          ParseHeader(response, ref cache, delegate(ref ArticlesCache articles, int id, int beg, int len)
          {
            if (len > 2)
            {
              string url = response.Substring(beg + 1, len - 2); // remove < >
              map.Add(url, id);
              // news:el3sv0$mdp$1@new_server.localdomain
              ArticleItem item = new ArticleItem();
              item._id = id;
              item.RawURL = "news:" + url;
              item._changed = false;
              item._toRead = false;
              articles.Add(id, item);
            }
          });
        }

        progressStart = 30;
        progressEnd = 80;
        for (int from = start; from <= end; from += maxArticles)
        {
          if (worker != null) worker.ReportProgress(progressStart + (progressEnd - progressStart) * from / count);

          int to = from + maxArticles - 1;
          string range = string.Format(" {0}-{1}", from, to);
          response = conn.Response("XHDR References " + range, ".");
          ParseHeader(response, ref cache, delegate(ref ArticlesCache articles, int id, int beg, int len)
          {
            ArticleItem item;
            if (articles.TryGetValue(id, out item))
            {
              while (len > 0)
              {
                // skip to begin of <Message-ID>
                int index = response.IndexOf('<', beg, len);
                if (index < 0) return;
                len -= (index - beg + 1);
                beg = index + 1;

                // find the end
                index = response.IndexOf('>', beg, len);
                if (index < 0) return;
                string name = response.Substring(beg, index - beg);
                int parent;
                if (map.TryGetValue(name, out parent))
                {
                  // parent found
                  item._parent = parent;
                  return;
                }

                // next id
                len -= (index - beg + 1);
                beg = index + 1;
              }
            }
          });
          response = conn.Response("XHDR From " + range, ".");
          ParseHeader(response, ref cache, delegate(ref ArticlesCache articles, int id, int beg, int len)
          {
            ArticleItem item;
            if (articles.TryGetValue(id, out item))
            {
              item._author = response.Substring(beg, len).Trim();
            }
          });
          response = conn.Response("XHDR Subject " + range, ".");
          ParseHeader(response, ref cache, delegate(ref ArticlesCache articles, int id, int beg, int len)
          {
            ArticleItem item;
            if (articles.TryGetValue(id, out item))
            {
              item._title = response.Substring(beg, len).Trim();
            }
          });
          response = conn.Response("XHDR Date " + range, ".");
          ParseHeader(response, ref cache, delegate(ref ArticlesCache articles, int id, int beg, int len) 
          {
            ArticleItem item;
            if (articles.TryGetValue(id, out item))
            {
              string date = response.Substring(beg, len).Trim();
              try
              {
                // it looks like 
                // Mon, 9 Nov 2009 15:09:21 +0000 (UTC)
                // translate the zone offset to "zzz" format
                int zone = date.LastIndexOfAny(new char[] { '+', '-' });
                if (zone >= 0)
                {
                  int n = 0;
                  while (zone + n + 1 < date.Length && char.IsDigit(date[zone + n + 1])) n++;
                  if (n >= 4)
                  {
                    date = date.Substring(0, zone + 3) + ":" + date.Substring(zone + 3, 2);
                  }
                  item._date = DateTime.ParseExact(date, dateFormat, culture);
                  return;
                }
                // fall-back
                item._date = DateTime.Parse(date, culture);
              }
              catch (System.Exception exception)
              {
                Console.Write(exception.ToString());
              }
            }
          });
        }
        if (worker != null) worker.ReportProgress(progressEnd);
      }

      // resolve tags
      Dictionary<int, BestTags> bestTags = new Dictionary<int, BestTags>();
      // find the newest tags from all threads
      foreach (ArticleItem item in cache.Values)
      {
        int index = item._title.IndexOf("#$");
        if (index < 0) continue; // no tags

        ArticleItem root = item; // in NG, tags are applied to the root
        while (root._parent >= 0)
        {
          ArticleItem parent;
          if (cache.TryGetValue(root._parent, out parent)) root = parent;
        }

        // check if old tags need to be rewritten
        BestTags tags;
        if (!bestTags.TryGetValue(root._id, out tags))
        {
          // no tags
          tags = new BestTags();
          tags._newest = item._date;
          tags._tags = item._title.Substring(index + 2);
          bestTags.Add(root._id, tags);
        }
        else if (item._date > tags._newest)
        {
          // newer tags
          tags._newest = item._date;
          tags._tags = item._title.Substring(index + 2);
        }

        // strip the title
        item._title = item._title.Substring(0, index);
      }
      // apply tags to thread roots
      foreach (KeyValuePair<int, BestTags> pair in bestTags)
      {
        ArticleItem item;
        if (cache.TryGetValue(pair.Key, out item))
        {
          string[] tags = pair.Value._tags.Split(',');
          foreach (string t in tags)
          {
            if (string.IsNullOrEmpty(t)) continue;

            string tag = t.ToLower();

            string prefix = "assignedto:";
            if (tag.StartsWith(prefix))
            {
              item.AddTag("@" + tag.Substring(prefix.Length));
              continue;
            }
            
            prefix = "assingedto:"; // mistype
            if (tag.StartsWith(prefix))
            {
              item.AddTag("@" + tag.Substring(prefix.Length));
              continue;
            }

            prefix = "close:";
            if (tag.StartsWith(prefix))
            {
              item.AddTag("/done");
              item.AddTag("/closed");
              item.AddTag("/" + tag);
              continue;
            }

            item.AddTag("/" + tag);
          }
        }
      }
      // add tags derived from the group name
      {
        string[] tags = _info._database.Split('.');
        foreach (ArticleItem item in cache.Values)
        {
          if (item._parent < 0)
          {
            foreach (string tag in tags) item.AddTag(tag);
          }
        }
      }
    }
    private delegate void ProcessLine(ref ArticlesCache articles, int id, int beg, int len);
    private void ParseHeader(string response, ref ArticlesCache cache, ProcessLine func)
    {
      // skip the first line
      int beg = response.IndexOf('\n', 0);
      if (beg < 0) return;
      beg++;

      while (beg < response.Length)
      {
        // parse single line
        int id = 0;
        while (char.IsDigit(response[beg]))
        {
          id = 10 * id + response[beg] - '0';
          beg++;
        }

        while (response[beg] == ' ') beg++;

        int end = response.IndexOf('\n', beg);
        if (end < 0) return;

        if (id > 0) func(ref cache, id, beg, end - beg - 1); // remove \r as well
        beg = end + 1;
      }
    }
    private class BestTags
    {
      public DateTime _newest;
      public string _tags;
    };
    public override int SaveArticle(ArticleItem item)
    {
      return -1;
    }
    public override void DeleteArticle(ArticleItem item)
    {

    }
    public override int LoadArticleRead(int id)
    {
      return -1;
    }
    public override void SaveArticleRead(int id, int revision)
    {

    }
    public override void DeleteArticleRead(int id)
    {

    }
    public override void SaveArticleCollapsed(int id)
    {

    }
    public override void DeleteArticleCollapsed(int id)
    {

    }
    public override int LoadRevision(int id)
    {
      return int.MaxValue;
    }
    public override void SaveRevisionAndDate(ArticleItem item)
    {

    }
    public override void SaveParent(int id, int parent)
    {

    }
    public override void UpdateTag(int id, string addTag, string removeTag)
    {

    }
    public override string GetArticleRevision(int id, int revision)
    {
      return "";
    }
    public override bool IsFullTextIndexLocal()
    {
      return false;
    }
    public override string CheckFullTextIndex()
    {
      return "";
    }
    public override List<int> FullTextRevisionsOfArticle(ArticleItem item)
    {
      return null;
    }
    public override bool FullTextNeedToIndexArticle(ArticleItem item, RevisionInfo revision)
    {
      return false;
    }
    public override void FullTextIndexArticle(ArticleItem item, RevisionInfo revision, string content)
    {
      
    }
    public override List<SearchResult> FullTextSearch(SQLDatabase database, string query)
    {
      List<SearchResult> results = new List<SearchResult>();
      return results;
    }
    public override double Benchmark()
    {
      return -1;
    }

    // implementation of ArticlesStore
    public string GetArticleContent(string path, ref BrowserRemotingProxy browserProxy)
    {
      using (NNTPConnection conn = NNTPConnection.Connect(_info))
      {
        if (conn == null) return string.Format("Cannot connect to {0}.", _info._name);

        // remove "news:" prefix to obtain message-id
        string prefix = "news:";
        if (!path.StartsWith(prefix)) return string.Format("Invalid news URL {0}.", path);

        // set the current newsgroup
        string response = conn.Response("GROUP " + _info._database, null);
        if (!response.StartsWith("211")) return string.Format("Newsgroup {0} not found.", _info._database);

        // get indices of articles
        response = conn.Response(string.Format("BODY <{0}>", path.Substring(prefix.Length)), null);
        if (!response.StartsWith("222")) return string.Format("Cannot receive article {0} (error '{1}').", path, response);

        response = conn.Response(null, ".");
        // extract the article content
        int lineStart = 0;
        Section section = Section.Text;
        string result = "";
        while (true) 
        {
          int index = response.IndexOf('\n', lineStart);
          if (index < 0) break;
          string line = response.Substring(lineStart, index + 1 - lineStart);
          if (line == ".\r\n")
          {
            // terminator, ignore
          }
          else if (line == "..\r\n")
            result += ".\r\n";
          else
          {
            switch (section)
            {
              case Section.Text:
                prefix = "begin ";
                int prefixLen = prefix.Length;
                if (line.StartsWith(prefix) &&
                  line.Length >= prefixLen + 4 &&
                  char.IsLetterOrDigit(line[prefixLen]) && char.IsLetterOrDigit(line[prefixLen + 1]) &&
                  char.IsLetterOrDigit(line[prefixLen + 2]) && line[prefixLen + 3] == ' ')
                {
                  section = Section.Attachment;
                  break;
                }
                result += line.Replace(@"\", @"\\");
                break;
              case Section.Attachment:
                if (line == " \r\n" || line == "`\r\n")
                {
                  section = Section.AttachmentEnd;
                  break;
                }
                // add to the encoded content
                break;
              case Section.AttachmentEnd:
                if (line == "end\r\n")
                {

                }
                else
                {
                  // TODO: warning - invalid attachment end
                }
                section = Section.Text;
                break;
            }
          }

          lineStart = index + 1;
        };

        return result;
      }
    }
    enum Section
    {
      Text,
      Attachment,
      AttachmentEnd
    }
    public bool SetSilent(bool silent)
    {
      return false;
    }
    public void UpdateWorkingCopy(SQLDatabase database, ArticleItem item, bool update, int changedRevision)
    {

    }
    public string GetWorkingCopy(string path, bool update)
    {
      return null;
    }
    public string CommitArticle(SQLDatabase database, ref ArticleItem item, ref string path, UpdateText content, bool majorRevision, System.ComponentModel.BackgroundWorker worker)
    {
      return null;
    }
    public string CommitNewAttachment(ref string url, ref string path, string attachmentFile, bool majorRevision, System.ComponentModel.BackgroundWorker worker)
    {
      return null;
    }
    public void DeleteArticle(string path, string title)
    {

    }
    public void UpdateRevisionAndDate(string path, bool majorRevision, ref ArticleItem item)
    {

    }
    public string GetArticleRevision(string url, int revision)
    {
      return null;
    }
    public List<string> GetPropertiesRevision(string url, int revision)
    {
      return null;
    }
    public bool LoadProperties(string path, out string tags)
    {
      tags = null;
      return false;
    }
    public int GetLocalRevision(string path)
    {
      return 0;
    }
    public void Commit(string path, string title)
    {

    }
    public void Commit(string path, string title, bool majorRevision)
    {

    }
    public void GetLog(ref SVNLog logRef, Uri url, System.ComponentModel.BackgroundWorker worker)
    {

    }
    public void Lock(string path)
    {

    }
    public void Unlock(string path)
    {

    }
    public Uri GetRepositoryRoot(Uri url)
    {
      return null;
    }
    public string GetRepositoryGuid(Uri url)
    {
      return null;
    }
    public string GetRepositoryGuidUnsafe(Uri url)
    {
      return null;
    }
    public bool GetRepositoryInfo(Uri url, out string root, out string uuid, bool silent)
    {
      root = null;
      uuid = null;
      return false;
    }
    public void AddFiles(List<ImportedArticle> articles)
    {

    }
    public void StoreTagsToSVN(SQLDatabase database, ArticlesCache cache)
    {
    }
    public bool UpdateTags(SQLDatabase database, ref ArticleItem item, bool majorRevision)
    {
      return false;
    }
    public string SVNBenchmark()
    {
      return "";
    }

    // helper class to handle opened NNTP connection
    private class NNTPConnection : IDisposable
    {
      private TcpClient _client;
      private NetworkStream _stream;
      private byte[] _buffer;

      private NNTPConnection(TcpClient client, NetworkStream stream)
      {
        _client = client;
        _stream = stream;
        _buffer = new byte[8192]; // same size as the internal socket buffer
      }
      ~NNTPConnection()
      {
        Dispose();
      }
      // IDisposable implementation
      public void Dispose()
      {
        if (_stream != null)
        {
          string response = Response("QUIT", null);
          if (!response.StartsWith("205"))
          {
            Console.Write("NNTP disconnect failed!");
          }
          _stream = null;
        }
      }


      public static NNTPConnection Connect(DBInfo info)
      {
        string url = "";
        int port = 119; // default NNTP port

        int index = info._server.LastIndexOf(':');
        if (index >= 0)
        {
          url = info._server.Substring(0, index);
          int.TryParse(info._server.Substring(index + 1), out port);
        }
        else url = info._server;

        TcpClient client = new TcpClient();
        client.Connect(url, port);
        NetworkStream stream = client.GetStream();
        if (stream == null) return null;
        NNTPConnection conn = new NNTPConnection(client, stream);

        // response to Connect
        string response = conn.Response(null, null);
        if (!response.StartsWith("200")) return null;

        // authentication
        response = conn.Response("AUTHINFO USER " + info._user, null);
        if (!response.StartsWith("381")) return null;
        response = conn.Response("AUTHINFO PASS " + info._password, null);
        if (!response.StartsWith("281")) return null;

        return conn;
      }

      public string Response(string cmd, string end)
      {
        string response = "";
        if (_stream == null) return response;
        
        Encoding enc = Encoding.GetEncoding(1250);

        // write the command
        if (!string.IsNullOrEmpty(cmd))
        {
          byte[] bytes = enc.GetBytes(cmd + "\r\n");
          _stream.Write(bytes, 0, bytes.Length);
        }

        // read the response
        do
        {
          int size = _stream.Read(_buffer, 0, _buffer.Length);
          if (size == 0) return response; // nothing to read
          response += enc.GetString(_buffer, 0, size);
        } while (_stream.DataAvailable || !IsEnd(response, end));

        return response;
      }
      private bool IsEnd(string response, string end)
      {
        if (!response.EndsWith("\r\n")) return false;
        if (string.IsNullOrEmpty(end)) return true;

        // the last line contains <end>
        if (response == (end + "\r\n")) return true;
        return response.EndsWith("\r\n" + end + "\r\n");
      }
    }
  }
}
