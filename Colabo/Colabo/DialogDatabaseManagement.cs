using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.DirectoryServices.Protocols;

namespace Colabo
{
  public partial class DialogDatabaseManagement : Form
  {
    private string _host;
    private int _port;
    private string _dbDN;
    private string _user;
    private string _password;

    public class UserInfo
    {
      public string _name;
      public string _dn;
      public string _accessRights;

      public override string ToString() { return string.Format("{0} ({1})", _name, _dn); }
    };
    public class GroupInfo
    {
      public string _name;
      public string _description;
      public string _accessRights;
      public int _level;
      public int _homepage;
      public List<string> _users;

      public GroupInfo() { _users = new List<string>(); }
      public override string ToString() { return _name; }
    };

    public DialogDatabaseManagement(string host, int port, string dbDN, string user, string password)
    {
      _host = host;
      _port = port;
      _dbDN = dbDN;
      _user = user;
      _password = password;

      InitializeComponent();
    }

    private void SelectServerType(DBServerType serverType)
    {
      foreach (DBServerTypeInfo item in valueServerType.Items)
      {
        if (item._value == serverType)
        {
          valueServerType.SelectedItem = item;
          return;
        }
      }
    }

    private void DialogDatabaseManagement_Load(object sender, EventArgs e)
    {
      valueServerType.Items.AddRange(DBServerTypeInfo.Values);

      // load the LDAP database entry, fill all controls 
      LdapConnection connection = OpenConnection();
      if (connection != null)
      {
        try
        {
          // common settings
          SearchRequest request = new SearchRequest(_dbDN, "(objectClass=*)", SearchScope.Base,
            new String[] {"cn", "colaboDBServerType", "colaboDBServer", "colaboDBDatabase", "colaboSVNPath", "colaboTranslate"});
          SearchResponse response = (SearchResponse)connection.SendRequest(request);
          if (response.Entries.Count == 1)
          {
            SearchResultEntry entry = response.Entries[0];

            DirectoryAttribute attributes = entry.Attributes["cn"];
            if (attributes != null && attributes.Count == 1) valueName.Text = attributes[0].ToString();

            // find the current server type
            DBServerType serverType = DBServerType.STMySQL;
            attributes = entry.Attributes["colaboDBServerType"];
            if (attributes != null && attributes.Count == 1)
            {
              string value = attributes[0].ToString();
              if (Enum.IsDefined(typeof(DBServerType), value))
                serverType = (DBServerType)Enum.Parse(typeof(DBServerType), value, true);
            }
            SelectServerType(serverType);

            attributes = entry.Attributes["colaboDBServer"];
            if (attributes != null && attributes.Count == 1) valueServer.Text = attributes[0].ToString();
            attributes = entry.Attributes["colaboDBDatabase"];
            if (attributes != null && attributes.Count == 1) valueDatabase.Text = attributes[0].ToString();
            attributes = entry.Attributes["colaboSVNPath"];
            if (attributes != null && attributes.Count == 1) valueSVNPath.Text = attributes[0].ToString();
            attributes = entry.Attributes["colaboTranslate"];
            if (attributes != null && attributes.Count == 1) valueTranslate.Text = attributes[0].ToString();
          }

          // users
          request = new SearchRequest("ou=users," + _dbDN, "(objectClass=colaboUser)", SearchScope.Subtree,
            new String[] {"cn", "colaboUserDN", "colaboAccessRights"});
          response = (SearchResponse)connection.SendRequest(request);
          foreach (SearchResultEntry entry in response.Entries)
          {
            UserInfo user = new UserInfo();
            users.Items.Add(user);
            groupUsers.Items.Add(user);

            DirectoryAttribute attributes = entry.Attributes["cn"];
            if (attributes != null && attributes.Count == 1) user._name = attributes[0].ToString();
            attributes = entry.Attributes["colaboUserDN"];
            if (attributes != null && attributes.Count == 1) user._dn = attributes[0].ToString();
            attributes = entry.Attributes["colaboAccessRights"];
            if (attributes != null && attributes.Count == 1) user._accessRights = attributes[0].ToString();
          }

          // groups
          request = new SearchRequest("ou=groups," + _dbDN, "(objectClass=colaboGroup)", SearchScope.Subtree,
            new String[] { "cn", "description", "colaboAccessRights", "colaboGroupLevel", "colaboHomePage", "memberUid" });
          response = (SearchResponse)connection.SendRequest(request);
          foreach (SearchResultEntry entry in response.Entries)
          {
            GroupInfo group = new GroupInfo();
            groups.Items.Add(group);

            DirectoryAttribute attributes = entry.Attributes["cn"];
            if (attributes != null && attributes.Count == 1) group._name = attributes[0].ToString();
            attributes = entry.Attributes["description"];
            if (attributes != null && attributes.Count == 1) group._description = attributes[0].ToString();
            attributes = entry.Attributes["colaboAccessRights"];
            if (attributes != null && attributes.Count == 1) group._accessRights = attributes[0].ToString();
            attributes = entry.Attributes["colaboGroupLevel"];
            if (attributes != null && attributes.Count == 1) int.TryParse(attributes[0].ToString(), out group._level);
            attributes = entry.Attributes["colaboHomePage"];
            if (attributes != null && attributes.Count == 1) int.TryParse(attributes[0].ToString(), out group._homepage);
            attributes = entry.Attributes["memberUid"];
            if (attributes != null)
            {
              for (int i=0; i<attributes.Count; i++)
              {
                group._users.Add(attributes[i].ToString());
              }
            }
          }
        }
        catch (System.Exception exception)
        {
          Console.WriteLine(exception.ToString());
        }
      }

      buttonEditUser.Enabled = false;
      buttonDeleteUser.Enabled = false;
      buttonEditGroup.Enabled = false;
      buttonDeleteGroup.Enabled = false;
    }

    private void users_SelectedIndexChanged(object sender, EventArgs e)
    {
      UserInfo user = (UserInfo)users.SelectedItem;
      buttonEditUser.Enabled = user != null;
      buttonDeleteUser.Enabled = user != null;
    }

    private void groups_SelectedIndexChanged(object sender, EventArgs e)
    {
      // SetItemChecked / SetItemCheckState raises the ItemCheck event, use the Indeterminate state to avoid reaction

      // switch all users
      for (int i = 0; i < groupUsers.Items.Count; i++) groupUsers.SetItemCheckState(i, CheckState.Indeterminate);

      // check users in group
      GroupInfo group = (GroupInfo)groups.SelectedItem;
      if (group != null)
      {
        for (int i=0; i<group._users.Count; i++)
        {
          int index = GetUserIndex(group._users[i]);
          if (index >= 0) groupUsers.SetItemChecked(index, true);
        }
      }

      // handle the remaining Indeterminate states
      for (int i = 0; i < groupUsers.Items.Count; i++)
      {
        if (groupUsers.GetItemCheckState(i) == CheckState.Indeterminate) groupUsers.SetItemChecked(i, false);
      }

      buttonEditGroup.Enabled = group != null;
      buttonDeleteGroup.Enabled = group != null;
    }
    private int GetUserIndex(string name)
    {
      for (int i=0; i<groupUsers.Items.Count; i++)
      {
        UserInfo user = (UserInfo)groupUsers.Items[i];
        if (user._name.Equals(name, StringComparison.OrdinalIgnoreCase)) return i;
      }
      return -1;
    }

    private void buttonAddUser_Click(object sender, EventArgs e)
    {
      DialogDatabaseUser dialog = new DialogDatabaseUser(users.Items, -1, _host, _port, _user, _password);
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK)
      {
        // add the user
        UserInfo user = dialog.result;
        if (user != null)
        {
          users.Items.Add(user);
          groupUsers.Items.Add(user);
        }
      }
    }

    private void buttonEditUser_Click(object sender, EventArgs e)
    {
      int index = users.SelectedIndex;
      if (index >= 0)
      {
        DialogDatabaseUser dialog = new DialogDatabaseUser(users.Items, index, _host, _port, _user, _password);
        DialogResult result = dialog.ShowDialog();
        if (result == DialogResult.OK)
        {
          // update the user
          UserInfo user = dialog.result;
          if (user != null)
          {
            string oldName = ((UserInfo)users.Items[index])._name;
            if (user._name != oldName)
            {
              // rename the user in all groups
              foreach (GroupInfo group in groups.Items)
              {
                int i = group._users.IndexOf(oldName);
                if (i >= 0) group._users[i] = user._name;
              }
            }

            users.Items[index] = user;
            groupUsers.Items[index] = user;
          }
        }
      }
    }

    private void buttonDeleteUser_Click(object sender, EventArgs e)
    {
      int index = users.SelectedIndex;
      if (index >= 0)
      {
        UserInfo user = (UserInfo)users.Items[index];
        string message = string.Format("Are you sure? The user {0} will be removed.", user);
        DialogResult result = MessageBox.Show(message, "Colabo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        if (result == DialogResult.Yes)
        {
          // remove the user from all groups
          foreach (GroupInfo group in groups.Items)
            group._users.Remove(user._name);

          users.Items.RemoveAt(index);
          groupUsers.Items.RemoveAt(index);
        }
      }
    }

    private void buttonGroupAdd_Click(object sender, EventArgs e)
    {
      DialogDatabaseGroup dialog = new DialogDatabaseGroup(groups.Items, -1);
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK)
      {
        // add the group
        GroupInfo group = dialog.result;
        if (group != null) groups.Items.Add(group);
      }
    }

    private void buttonGroupEdit_Click(object sender, EventArgs e)
    {
      int index = groups.SelectedIndex;
      if (index >= 0)
      {
        DialogDatabaseGroup dialog = new DialogDatabaseGroup(groups.Items, index);
        DialogResult result = dialog.ShowDialog();
        if (result == DialogResult.OK)
        {
          // update the group
          GroupInfo group = dialog.result;
          if (group != null)
          {
            // keep the list of users
            GroupInfo oldGroup = (GroupInfo)groups.Items[index];
            group._users = oldGroup._users;

            groups.Items[index] = group;
          }
        }
      }
    }

    private void buttonGroupDelete_Click(object sender, EventArgs e)
    {
      int index = groups.SelectedIndex;
      if (index >= 0)
      {
        GroupInfo group = (GroupInfo)groups.Items[index];
        string message = string.Format("Are you sure? The group {0} will be removed.", group);
        DialogResult result = MessageBox.Show(message, "Colabo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        if (result == DialogResult.Yes)
        {
          groups.Items.RemoveAt(index);
        }
      }
    }

    private void groupUsers_ItemCheck(object sender, ItemCheckEventArgs e)
    {
      if (e.CurrentValue == CheckState.Indeterminate || e.NewValue == CheckState.Indeterminate) return; // changed by the code

      GroupInfo group = (GroupInfo)groups.SelectedItem;
      if (group != null)
      {
        UserInfo user = (UserInfo)groupUsers.Items[e.Index];
        if (user != null)
        {
          string uid = user._name;
          if (e.NewValue == CheckState.Checked)
          {
            if (!group._users.Contains(uid)) group._users.Add(uid);
          }
          else
          {
            group._users.Remove(uid);
          }
        }
      }
    }

    private void buttonOK_Click(object sender, EventArgs e)
    {
      // save the content to LDAP
      LdapConnection connection = OpenConnection();
      if (connection == null) return;

      // common settings
      {
        try
        {
          ModifyRequest request = new ModifyRequest();
          request.DistinguishedName = _dbDN;

          LDAPSetProperty(request, "colaboDBServerType", ((DBServerTypeInfo)valueServerType.SelectedItem)._value.ToString());
          LDAPSetProperty(request, "colaboDBServer", valueServer.Text);
          LDAPSetProperty(request, "colaboDBDatabase", valueDatabase.Text);
          LDAPSetProperty(request, "colaboSVNPath", valueSVNPath.Text);
          LDAPSetProperty(request, "colaboTranslate", valueTranslate.Text);

          connection.SendRequest(request);
        }
        catch (System.Exception exception)
        {
          MessageBox.Show(String.Format("LDAP update failed: {0}", exception.Message), "LDAP", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
      }

      // users
      {
        // collect the list from the dialog
        List<UserInfo> usersToProcess = new List<UserInfo>();
        foreach (UserInfo info in users.Items) usersToProcess.Add(info);

        // handle current users
        SearchRequest readRequest = new SearchRequest("ou=users," + _dbDN, "(objectClass=colaboUser)", SearchScope.Subtree,
          new String[] {"colaboUserDN", "colaboAccessRights"});
        SearchResponse readResponse = (SearchResponse)connection.SendRequest(readRequest);
        foreach (SearchResultEntry entry in readResponse.Entries)
        {
          // find the user in the list of users to process
          int index = -1;
          DirectoryAttribute attributes = entry.Attributes["colaboUserDN"];
          if (attributes != null && attributes.Count == 1)
          {
            string dn = attributes[0].ToString();
            index = usersToProcess.FindIndex(delegate(UserInfo info) { return info._dn.Equals(dn, StringComparison.OrdinalIgnoreCase); });
          }

          if (index >= 0)
          {
            UserInfo info = usersToProcess[index];
            // update the entry
            ModifyRequest request = new ModifyRequest();
            request.DistinguishedName = entry.DistinguishedName;
            LDAPSetProperty(request, "colaboAccessRights", info._accessRights);
            connection.SendRequest(request);
            // processed
            usersToProcess.RemoveAt(index);
          }
          else
          {
            // entry was removed
            DeleteRequest request = new DeleteRequest(entry.DistinguishedName);
            connection.SendRequest(request);
          }
        }
        // add new users
        foreach (UserInfo info in usersToProcess)
        {
          AddRequest request = new AddRequest();
          request.DistinguishedName = string.Format("cn={0},ou=users,", info._name) + _dbDN;
          request.Attributes.Add(new DirectoryAttribute("objectClass", "colaboUser"));
          request.Attributes.Add(new DirectoryAttribute("colaboUserDN", info._dn));
          request.Attributes.Add(new DirectoryAttribute("cn", info._name));
          if (!string.IsNullOrEmpty(info._accessRights)) request.Attributes.Add(new DirectoryAttribute("colaboAccessRights", info._accessRights));
          connection.SendRequest(request);
        }
      }

      // groups
      {
        // collect the list from the dialog
        List<GroupInfo> groupsToProcess = new List<GroupInfo>();
        foreach (GroupInfo info in groups.Items) groupsToProcess.Add(info);

        // handle current groups
        SearchRequest readRequest = new SearchRequest("ou=groups," + _dbDN, "(objectClass=colaboGroup)", SearchScope.Subtree,
          new String[] {"cn", "description", "colaboAccessRights", "colaboGroupLevel", "colaboHomePage", "memberUid"});
        SearchResponse readResponse = (SearchResponse)connection.SendRequest(readRequest);
        foreach (SearchResultEntry entry in readResponse.Entries)
        {
          // find the group in the list of groups to process
          int index = -1;
          DirectoryAttribute attributes = entry.Attributes["cn"];
          if (attributes != null && attributes.Count == 1)
          {
            string name = attributes[0].ToString();
            index = groupsToProcess.FindIndex(delegate(GroupInfo info) { return info._name.Equals(name, StringComparison.OrdinalIgnoreCase); });
          }

          if (index >= 0)
          {
            GroupInfo info = groupsToProcess[index];
            // update the entry
            ModifyRequest request = new ModifyRequest();
            request.DistinguishedName = entry.DistinguishedName;
            LDAPSetProperty(request, "description", info._description);
            LDAPSetProperty(request, "colaboAccessRights", info._accessRights);
            LDAPSetProperty(request, "colaboGroupLevel", info._level.ToString());
            LDAPSetProperty(request, "colaboHomePage", info._homepage.ToString());

            DirectoryAttributeModification modification = new DirectoryAttributeModification();
            modification.Operation = DirectoryAttributeOperation.Replace;
            modification.Name = "memberUid";
            foreach (string user in info._users) modification.Add(user);
            request.Modifications.Add(modification);
 
            connection.SendRequest(request);
            // processed
            groupsToProcess.RemoveAt(index);
          }
          else
          {
            // entry was removed
            DeleteRequest request = new DeleteRequest(entry.DistinguishedName);
            connection.SendRequest(request);
          }
        }
        // add new groups
        foreach (GroupInfo info in groupsToProcess)
        {
          AddRequest request = new AddRequest();
          request.DistinguishedName = string.Format("cn={0},ou=groups,", info._name) + _dbDN;
          request.Attributes.Add(new DirectoryAttribute("objectClass", "colaboGroup"));
          request.Attributes.Add(new DirectoryAttribute("cn", info._name));
          if (!string.IsNullOrEmpty(info._description)) request.Attributes.Add(new DirectoryAttribute("description", info._description));
          if (!string.IsNullOrEmpty(info._accessRights)) request.Attributes.Add(new DirectoryAttribute("colaboAccessRights", info._accessRights));
          request.Attributes.Add(new DirectoryAttribute("colaboGroupLevel", info._level.ToString()));
          request.Attributes.Add(new DirectoryAttribute("colaboHomePage", info._homepage.ToString()));
          DirectoryAttribute attribute = new DirectoryAttribute();
          attribute.Name = "memberUid";
          foreach (string user in info._users) attribute.Add(user);
          connection.SendRequest(request);
        }
      }

      // mirror the accounts info to the SQL database
      SQLDatabase.SaveLDAPUsers(_host, _port, _dbDN, _user, _password);
    }
    private LdapConnection OpenConnection()
    {
      return Configuration.OpenLDAPConnection(_host, _port, _user, _password);
    }
    private void LDAPSetProperty(ModifyRequest request, string name, string value)
    {
      DirectoryAttributeModification modification = new DirectoryAttributeModification();
      modification.Operation = DirectoryAttributeOperation.Replace;
      modification.Name = name;
      if (!string.IsNullOrEmpty(value)) modification.Add(value);
      request.Modifications.Add(modification);
    }
  }
}