namespace Colabo
{
  partial class DialogGanttChart
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogGanttChart));
      this.ganttChart = new GanttChart.GanttChart();
      this.SuspendLayout();
      // 
      // ganttChart
      // 
      this.ganttChart.AllowManualEditBar = false;
      this.ganttChart.BackColor = System.Drawing.SystemColors.Window;
      this.ganttChart.DateFont = new System.Drawing.Font("Verdana", 8F);
      this.ganttChart.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ganttChart.FromDate = new System.DateTime(2010, 5, 5, 14, 17, 55, 303);
      this.ganttChart.Location = new System.Drawing.Point(0, 0);
      this.ganttChart.Name = "ganttChart";
      this.ganttChart.RowFont = new System.Drawing.Font("Verdana", 8F);
      this.ganttChart.Size = new System.Drawing.Size(292, 266);
      this.ganttChart.TabIndex = 0;
      this.ganttChart.TimeFont = new System.Drawing.Font("Verdana", 8F);
      this.ganttChart.ToDate = new System.DateTime(2010, 5, 5, 14, 17, 55, 303);
      this.ganttChart.ToolTipText = ((System.Collections.Generic.List<string>)(resources.GetObject("ganttChart.ToolTipText")));
      this.ganttChart.ToolTipTextTitle = "";
      this.ganttChart.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ganttChart_MouseMove);
      this.ganttChart.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ganttChart_MouseDown);
      this.ganttChart.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ganttChart_MouseUp);
      // 
      // DialogGanttChart
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(292, 266);
      this.Controls.Add(this.ganttChart);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "DialogGanttChart";
      this.Text = "Gantt Chart";
      this.ResumeLayout(false);

    }

    #endregion

    private GanttChart.GanttChart ganttChart;
  }
}