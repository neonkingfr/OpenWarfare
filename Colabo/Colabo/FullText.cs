using System;
using System.Windows.Forms;
using System.Collections.Generic;

using Lucene.Net.Store;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Index;
using Lucene.Net.Documents;
using Lucene.Net.Search;
using Lucene.Net.QueryParsers;

namespace Colabo
{
  public class FullText
  {
    private Directory _directory;
    private Analyzer _analyzer;

    private const string _fieldName = "name";
    private const string _fieldAuthor = "author";
    private const string _fieldTime = "time";
    private const string _fieldTitle = "title";
    private const string _fieldContent = "content";

    public FullText(string server, string database)
    {
      string dir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Colabo\\" +
        string.Format("index+{0}+{1}", Uri.EscapeDataString(server), database);

      bool create = !System.IO.Directory.Exists(dir);
      _directory = FSDirectory.GetDirectory(dir, create);
      _analyzer = new StandardAnalyzer();
    }

    public string CheckIndex()
    {
      if (!IndexReader.IndexExists(_directory)) return "No index"; // no index exist yet

      try
      {
        IndexReader reader = IndexReader.Open(_directory);
        int total = reader.NumDocs();
        int invalid = 0;
        for (int i = 0; i < total; i++)
        {
          Document doc = reader.Document(i);
          string name = doc.Get(_fieldName);
          if (name == null) invalid++;
        }
        if (invalid > 0) return string.Format("{0} documents, {1} invalid", total, invalid);
        else return string.Format("{0} documents", total);
      }
      catch (System.Exception e)
      {
        return string.Format("Index corrupted: {0}", e);      	
      }
    }

    public List<int> RevisionsOfArticle(ArticleItem item)
    {
      List<int> revisions = new List<int>();
      if (!IndexReader.IndexExists(_directory)) return revisions; // no index exist yet

      IndexReader reader = IndexReader.Open(_directory);
      string prefix = string.Format("{0:D}:", item._id);

      TermEnum terms = reader.Terms(new Term(_fieldName, prefix));
      while (terms.Term() != null && terms.Term().Text().StartsWith(prefix))
      {
        int revision = int.Parse(terms.Term().Text().Substring(prefix.Length));
        revisions.Add(revision);
        terms.Next();
      }
      terms.Close();
      return revisions;
    }
    public bool NeedToIndexArticle(ArticleItem item, RevisionInfo revision)
    {
      if (!IndexReader.IndexExists(_directory)) return true; // no index exist yet

      // unique document identifier
      string name = string.Format("{0:D}:{1:D}", item._id, revision._revision);
      // check if document is already present in the index
      IndexReader reader = IndexReader.Open(_directory);
      TermDocs docs = reader.TermDocs(new Term(_fieldName, name));
      bool found = docs.Next();
      reader.Close();
      return !found;
    }
    public void IndexArticle(ArticleItem item, RevisionInfo revision, string content)
    {
      bool create = !IndexReader.IndexExists(_directory); // create a new index if not exists
      IndexWriter writer = new IndexWriter(_directory, _analyzer, create);
      
      Document doc = new Document();

      // unique document identifier
      string name = string.Format("{0:D}:{1:D}", item._id, revision._revision);
      doc.Add(new Field(_fieldName, name, Field.Store.YES, Field.Index.UN_TOKENIZED));
      doc.Add(new Field(_fieldAuthor, revision._author.ToLowerInvariant(), Field.Store.YES, Field.Index.UN_TOKENIZED));
      doc.Add(new Field(_fieldTime, revision._time.ToString("yyyyMMddHHmm"), Field.Store.YES, Field.Index.UN_TOKENIZED));
      doc.Add(new Field(_fieldTitle, item._title, Field.Store.YES, Field.Index.TOKENIZED));
      doc.Add(new Field(_fieldContent, content == null ? "" : content, Field.Store.YES, Field.Index.TOKENIZED));

      writer.AddDocument(doc);
      writer.Close();
    }

    public List<SearchResult> Search(SQLDatabase database, string query)
    {
      if (!IndexReader.IndexExists(_directory)) return null; // no index exists

      IndexSearcher searcher = new IndexSearcher(_directory);
      QueryParser parser = new QueryParser("content", _analyzer);
      Query q;
      try
      {
        q = parser.Parse(query);
      }
      catch (Lucene.Net.QueryParsers.ParseException exception)
      {
        Console.Write(exception.ToString());
        return null;
      }

      Hits hits = searcher.Search(q);

      List<SearchResult> results = new List<SearchResult>();
      for (int i = 0; i < hits.Length(); i++)
      {
        Document doc = hits.Doc(i);
        string name = doc.Get(_fieldName);

        SearchResult result = new SearchResult();
        result._database = database;
        result._id = -1;
        result._revision = 0;
        int index = name.IndexOf(':');
        if (index >= 0)
        {
          int.TryParse(name.Substring(0, index), out result._id);
          int.TryParse(name.Substring(index + 1), out result._revision);
        }
        result._author = doc.Get(_fieldAuthor);
        DateTime.TryParseExact(doc.Get(_fieldTime), "yyyyMMddHHmm", null, System.Globalization.DateTimeStyles.None, out result._time);
        result._title = doc.Get(_fieldTitle);
        result._content = doc.Get(_fieldContent);
        result._score = hits.Score(i);

        // need to find article in the database to check access
        ArticleItem item = null;
        if (database.cache.TryGetValue(result._id, out item) && item != null)
        {
          ArticleItem.VTCache cache = new ArticleItem.VTCache(database, item);
          if ((item.GetAccessRights(database, cache) & AccessRights.Read) != 0)
          {
            result._headRevision = result._revision >= item._revision;
            // Console.WriteLine("Explanation for {0}: {1}", result._title, searcher.Explain(q, hits.Id(i)).ToString());
            if (!string.IsNullOrEmpty(result._content)) results.Add(result);
          }
        }
      }

      searcher.Close();

      // optimize result - remove older revisions of articles with lower score
      // create Dictionary of article revisions
      Dictionary<int, List<SearchResult>> dictionary = new Dictionary<int, List<SearchResult>>();
      foreach (SearchResult result in results)
      {
        List<SearchResult> list;
        if (!dictionary.TryGetValue(result._id, out list))
        {
          list = new List<SearchResult>();
          dictionary.Add(result._id, list);
        }
        list.Add(result);
      }
      results.Clear();
      // optimize each list of revisions
      foreach (List<SearchResult> list in dictionary.Values)
      {
        list.Sort(new SortByRevision());
        // remove older revisions
        for (int i=0; i<list.Count; i++)
        {
          float score = list[i]._score;
          for (int j=i+1; j<list.Count;)
          {
            if (list[j]._score <= score) list.RemoveAt(j);
            else j++;
          }
        }
        // add remaining revisions to results
        results.AddRange(list);
      }
      // sort results by the score again
      results.Sort(new SortByScore());
      return results;
    }
    private class SortByRevision : IComparer<SearchResult>
    {
      public int Compare(SearchResult a, SearchResult b)
      {
        return b._revision - a._revision; // newer revisions first
      }
    }
    private class SortByScore : IComparer<SearchResult>
    {
      public int Compare(SearchResult a, SearchResult b)
      {
        int diff = Math.Sign(b._score - a._score); // higher score first
        if (diff != 0) return diff;
        diff = a._id - b._id; // lower id first
        if (diff != 0) return diff;
        return b._revision - a._revision; // newer revisions first
      }
    }
  }
}