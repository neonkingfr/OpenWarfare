namespace Colabo
{
  partial class DialogAdvancedSearch
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.buttonCancel = new System.Windows.Forms.Button();
      this.buttonSearch = new System.Windows.Forms.Button();
      this.label5 = new System.Windows.Forms.Label();
      this.valueContent = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.valueTitle = new System.Windows.Forms.TextBox();
      this.valueTimeTo = new System.Windows.Forms.DateTimePicker();
      this.label3 = new System.Windows.Forms.Label();
      this.valueTimeFrom = new System.Windows.Forms.DateTimePicker();
      this.label2 = new System.Windows.Forms.Label();
      this.valueAuthor = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.SuspendLayout();
      // 
      // splitContainer1
      // 
      this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.buttonCancel);
      this.splitContainer1.Panel1.Controls.Add(this.buttonSearch);
      this.splitContainer1.Panel1.Controls.Add(this.label5);
      this.splitContainer1.Panel1.Controls.Add(this.valueContent);
      this.splitContainer1.Panel1.Controls.Add(this.label4);
      this.splitContainer1.Panel1.Controls.Add(this.valueTitle);
      this.splitContainer1.Panel1.Controls.Add(this.valueTimeTo);
      this.splitContainer1.Panel1.Controls.Add(this.label3);
      this.splitContainer1.Panel1.Controls.Add(this.valueTimeFrom);
      this.splitContainer1.Panel1.Controls.Add(this.label2);
      this.splitContainer1.Panel1.Controls.Add(this.valueAuthor);
      this.splitContainer1.Panel1.Controls.Add(this.label1);
      this.splitContainer1.SplitterDistance = 143;
      // 
      // buttonCancel
      // 
      this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.buttonCancel.Location = new System.Drawing.Point(770, 119);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.Size = new System.Drawing.Size(75, 23);
      this.buttonCancel.TabIndex = 23;
      this.buttonCancel.Text = "Cancel";
      this.buttonCancel.UseVisualStyleBackColor = true;
      this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
      // 
      // buttonSearch
      // 
      this.buttonSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonSearch.Location = new System.Drawing.Point(689, 119);
      this.buttonSearch.Name = "buttonSearch";
      this.buttonSearch.Size = new System.Drawing.Size(75, 23);
      this.buttonSearch.TabIndex = 22;
      this.buttonSearch.Text = "Search";
      this.buttonSearch.UseVisualStyleBackColor = true;
      this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(12, 100);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(47, 13);
      this.label5.TabIndex = 21;
      this.label5.Text = "Content:";
      // 
      // valueContent
      // 
      this.valueContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueContent.Location = new System.Drawing.Point(92, 93);
      this.valueContent.Name = "valueContent";
      this.valueContent.Size = new System.Drawing.Size(753, 20);
      this.valueContent.TabIndex = 20;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(12, 73);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(30, 13);
      this.label4.TabIndex = 19;
      this.label4.Text = "Title:";
      // 
      // valueTitle
      // 
      this.valueTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueTitle.Location = new System.Drawing.Point(92, 66);
      this.valueTitle.Name = "valueTitle";
      this.valueTitle.Size = new System.Drawing.Size(753, 20);
      this.valueTitle.TabIndex = 18;
      // 
      // valueTimeTo
      // 
      this.valueTimeTo.Checked = false;
      this.valueTimeTo.CustomFormat = "d.M.yyyy H:mm";
      this.valueTimeTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.valueTimeTo.Location = new System.Drawing.Point(305, 39);
      this.valueTimeTo.Name = "valueTimeTo";
      this.valueTimeTo.ShowCheckBox = true;
      this.valueTimeTo.Size = new System.Drawing.Size(150, 20);
      this.valueTimeTo.TabIndex = 17;
      this.valueTimeTo.Value = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(280, 43);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(19, 13);
      this.label3.TabIndex = 16;
      this.label3.Text = "to:";
      // 
      // valueTimeFrom
      // 
      this.valueTimeFrom.Checked = false;
      this.valueTimeFrom.CustomFormat = "d.M.yyyy H:mm";
      this.valueTimeFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
      this.valueTimeFrom.Location = new System.Drawing.Point(92, 39);
      this.valueTimeFrom.Name = "valueTimeFrom";
      this.valueTimeFrom.ShowCheckBox = true;
      this.valueTimeFrom.Size = new System.Drawing.Size(182, 20);
      this.valueTimeFrom.TabIndex = 15;
      this.valueTimeFrom.Value = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 43);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(56, 13);
      this.label2.TabIndex = 14;
      this.label2.Text = "Date from:";
      // 
      // valueAuthor
      // 
      this.valueAuthor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueAuthor.Location = new System.Drawing.Point(92, 12);
      this.valueAuthor.Name = "valueAuthor";
      this.valueAuthor.Size = new System.Drawing.Size(753, 20);
      this.valueAuthor.TabIndex = 13;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 15);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(41, 13);
      this.label1.TabIndex = 12;
      this.label1.Text = "Author:";
      // 
      // DialogAdvancedSearch
      // 
      this.AcceptButton = this.buttonSearch;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.CancelButton = this.buttonCancel;
      this.ClientSize = new System.Drawing.Size(857, 683);
      this.Name = "DialogAdvancedSearch";
      this.Text = "Advanced Search";
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel1.PerformLayout();
      this.splitContainer1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button buttonCancel;
    private System.Windows.Forms.Button buttonSearch;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.TextBox valueContent;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox valueTitle;
    private System.Windows.Forms.DateTimePicker valueTimeTo;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.DateTimePicker valueTimeFrom;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox valueAuthor;
    private System.Windows.Forms.Label label1;
  }
}
