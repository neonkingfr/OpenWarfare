using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Colabo
{
  public partial class DialogDatabaseGroup : Form
  {
    // group properties
    private ListBox.ObjectCollection _items;
    private int _index;

    private DialogDatabaseManagement.GroupInfo _result;
    public DialogDatabaseManagement.GroupInfo result { get { return _result; } }

    public DialogDatabaseGroup(ListBox.ObjectCollection items, int index)
    {
      _items = items;
      _index = index;

      InitializeComponent();
    }

    private void buttonOK_Click(object sender, EventArgs e)
    {
      // check if the data are valid
      string name = valueName.Text;
      // check for dn duplicity
      for (int i = 0; i < _items.Count; i++)
      {
        if (i != _index && ((DialogDatabaseManagement.GroupInfo)_items[i])._name.Equals(name, StringComparison.OrdinalIgnoreCase))
        {
          MessageBox.Show("This group is already in the list.", "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Information);
          DialogResult = DialogResult.None;
          return;
        }
      }

      _result = new DialogDatabaseManagement.GroupInfo();
      _result._name = name;
      _result._description = valueDescription.Text;
      _result._accessRights = valueAccessRights.Text;
      _result._level = (int)valueLevel.Value;
      _result._homepage = (int)valueHomePage.Value;
    }

    private void DialogDatabaseGroup_Load(object sender, EventArgs e)
    {
      if (_index >= 0)
      {
        DialogDatabaseManagement.GroupInfo group = (DialogDatabaseManagement.GroupInfo)_items[_index];
        valueName.Text = group._name;
        valueDescription.Text = group._description;
        valueAccessRights.Text = group._accessRights;
        valueLevel.Value = group._level;
        valueHomePage.Value = group._homepage;
      }
    }
  }
}