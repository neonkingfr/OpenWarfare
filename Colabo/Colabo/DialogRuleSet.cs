using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Colabo
{
  public partial class DialogRuleSet : Form
  {
    private ListBox.ObjectCollection _items;
    private int _index;
    private RuleSet _ruleSet;
    private DBInfo _database;

    private RuleSet GetRuleSet(int index)
    {
      return (RuleSet)_items[index];
    }
    public string name
    {
      get { return nameTextBox.Text; }
    }
    public string ruleSet
    {
      get { return ruleSetTextBox.Text; }
    }
    public string parentUser
    {
      get
      {
        {
          if (parentComboBox.SelectedItem == null) return "";
          int index = ((ParentValue)parentComboBox.SelectedItem)._index;
          if (index < 0) return "";
          return GetRuleSet(index)._user;
        }
      }
    }
    public string parentName
    {
      get
      {
        {
          if (parentComboBox.SelectedItem == null) return "";
          int index = ((ParentValue)parentComboBox.SelectedItem)._index;
          if (index < 0) return "";
          return GetRuleSet(index)._name;
        }
      }
    }

    private class ParentValue
    {
      public int _index;
      public string _text;

      public ParentValue(int index, string text) { _index = index; _text = text; }
      public override string ToString() { return _text; }
    }

    public DialogRuleSet(ListBox.ObjectCollection items, int index, bool readOnly, DBInfo database)
    {
      _items = items;
      _index = index;
      _ruleSet = _index < 0 ? null : GetRuleSet(index);
      _database = database;
      InitializeComponent();

      if (readOnly)
      {
        buttonOK.Visible = false;
        buttonCancel.Text = "Close";
        this.AcceptButton = buttonCancel;

        nameTextBox.ReadOnly = true;
        parentComboBox.Enabled = false;
        ruleSetTextBox.ReadOnly = true;
      }
    }

    private void DialogRuleSet_Load(object sender, EventArgs e)
    {
      if (_ruleSet != null)
      {
        nameTextBox.Text = _ruleSet._name;
        ruleSetTextBox.Text = _ruleSet._expression;
      }

      parentComboBox.Items.Clear();
      parentComboBox.Items.Add(new ParentValue(-1, ""));
      int selected = 0;
      // add possible parents
      for (int i=0; i<_items.Count; i++)
      {
        RuleSet item = GetRuleSet(i);
        if (i != _index && (_ruleSet == null || !Inherits(item, _ruleSet)))
        {
          parentComboBox.Items.Add(new ParentValue(i, string.Format("{0}:{1}", item._user, item._name)));
          if (_ruleSet != null && item._user.Equals(_ruleSet._parentUser, StringComparison.OrdinalIgnoreCase) &&
              item._name.Equals(_ruleSet._parentName, StringComparison.OrdinalIgnoreCase)) selected = parentComboBox.Items.Count - 1;
        }
      }
      // select the current parent
      parentComboBox.SelectedIndex = selected;
    }
    private bool Inherits(RuleSet ruleSet, RuleSet from)
    {
      while (ruleSet != null)
      {
        if (ruleSet._parentUser.Equals(from._user, StringComparison.OrdinalIgnoreCase) &&
          ruleSet._parentName.Equals(from._name, StringComparison.OrdinalIgnoreCase)) return true;

        ruleSet = FindRuleSet(ruleSet._parentUser, ruleSet._parentName);
      }
      return false;
    }
    private RuleSet FindRuleSet(string user, string name)
    {
      foreach (RuleSet item in _items)
      {
        if (item._user.Equals(user, StringComparison.OrdinalIgnoreCase) &&
            item._name.Equals(name, StringComparison.OrdinalIgnoreCase)) return item;
      }
      return null;
    }

    private void DialogRuleSet_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (DialogResult == DialogResult.OK)
      {
        string name = nameTextBox.Text;
        if (name.Length == 0)
        {
          MessageBox.Show("The name should not be empty.", "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
          e.Cancel = true;
          return;
        }
        // check for items duplicity
        for (int i = 0; i < _items.Count; i++)
        {
          if (i == _index) continue; // myself
          if (GetRuleSet(i)._name.Equals(name) && GetRuleSet(i)._user.Equals(_database._user))
          {
            MessageBox.Show("The name is already used.", "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            e.Cancel = true;
            return;
          }
        }
        // check the expression
        if (FormColabo.CompileRuleSet(ruleSet) == null)
        {
          e.Cancel = true;
          return;
        }
      }
    }
  }
}