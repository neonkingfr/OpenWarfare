using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Net;
using System.Security.Permissions;
using InternetExplorer = System.Object;

namespace Colabo
{
  [ComVisible(true)]
  public class BrowserRemotingProxy : MarshalByRefObject, IAuthenticate, IServiceProvider
  {
    // access to Colabo configuration file
    private Colabo.Configuration config;
    // URL stored just before WebBrowser.Navigate function was called with
    string navigateUrl;
    public string realm;
    // set when webBrowser.Navigate returned 401 Unauthorized code
    class FFCHelper
    {
      public bool forcePromptForCredentials = false;
      public bool forceForCredentialsCanceled = false;
    }
    private FFCHelper _ffcHelper = new FFCHelper();

    public BrowserRemotingProxy(WebBrowser browserControl, Colabo.Configuration config)
    {
      this.browserControl = browserControl;
      this.IE = browserControl.ActiveXInstance as InternetExplorer;
      this.config = config;
      IServiceProvider sp = this.IE as IServiceProvider;
      IProfferService theProfferService = null;
      IntPtr objectProffer = IntPtr.Zero;
      uint cookie = 0;

      sp.QueryService(ref SID_SProfferService, ref IID_IProfferService, out objectProffer);
      theProfferService = Marshal.GetObjectForIUnknown(objectProffer) as IProfferService;
      theProfferService.ProfferService(ref IID_IAuthenticate, this, out cookie);
    }

    public string NavigateUrl {
      get {return navigateUrl;}
      set {navigateUrl=value;}
    }

    public bool ForcePromptForCredentials {
      get { return _ffcHelper.forcePromptForCredentials; }
      set { _ffcHelper.forcePromptForCredentials = value; }
    }
    public bool PromptForCredentialsCanceled {
      get { return _ffcHelper.forceForCredentialsCanceled; }
      set { _ffcHelper.forceForCredentialsCanceled = value; }
    }

    public Guid IID_IProfferService = new Guid("cb728b20-f786-11ce-92ad-00aa00a74cd0");
    public Guid SID_SProfferService = new Guid("cb728b20-f786-11ce-92ad-00aa00a74cd0");
    public Guid IID_IAuthenticate = new Guid("79eac9d0-baf9-11ce-8c82-00aa004ba90b");

    #region IServiceProvider Members

    void IServiceProvider.QueryService(ref Guid guidService, ref Guid riid, out IntPtr ppvObject)
    {
      //int hr = HRESULT.E_NOINTERFACE;

      if (guidService == IID_IAuthenticate && riid == IID_IAuthenticate)
      {
        ppvObject = Marshal.GetComInterfaceForObject(this, typeof(IAuthenticate)); // this as IAuthenticate; //
        //if (ppvObject != null) {
        //    hr = HRESULT.S_OK;
        //}
      }
      else
      {
        ppvObject = IntPtr.Zero;
      }

      //return hr;
    }

    #endregion

    #region IAuthenticate Members

    void IAuthenticate.Authenticate(out IntPtr phwnd, out string pszUsername, out string pszPassword)
    {
      phwnd = IntPtr.Zero;

      bool savePwd = true;
      // try to find cached matching credential inside config stored authentications
      Uri navUri = new Uri(navigateUrl);
      Uri serverUrl = new Uri(navUri.Scheme + "://" + navUri.Host + "/"); //take only prefixUrl
      if (ForcePromptForCredentials || !config.FindCredentials(serverUrl, realm, "Basic", out pszUsername, out pszPassword))
      {
        ForcePromptForCredentials = false;
        // Prompt For Credentials, as matching cached credential for given URL was not found
        pszUsername = Environment.UserName;
        pszPassword = "";
        CredUIReturnCodes code = Import.PromptForCredentials(serverUrl, realm, ref pszUsername, ref pszPassword, ref savePwd);
        if (code != CredUIReturnCodes.NO_ERROR)
        {
          PromptForCredentialsCanceled = true;
          return;
        }

        if (savePwd)
        {
          // use the shared cache
          config.UpdateAuthentications(serverUrl, realm, "Basic", new NetworkCredential(pszUsername, pszPassword), true);
        }
      }
    }

    #endregion

    private WebBrowser browserControl;
    private InternetExplorer IE;
  }

  // ColaboWebBrowser extends WebBrowser to add especially OnNavigateError possibilities
  public class ColaboWebBrowser : WebBrowser
  {
    AxHost.ConnectionPointCookie cookie;
    ColaboWebBrowserEventHelper helper;

    //store HTTP error code
    int _httpErrorCode = 200; //OK

    [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
    protected override void CreateSink()
    {
      base.CreateSink();

      // Create an instance of the client that will handle the event
      // and associate it with the underlying ActiveX control.
      helper = new ColaboWebBrowserEventHelper(this);
      cookie = new AxHost.ConnectionPointCookie( this.ActiveXInstance, helper, typeof(DWebBrowserEvents2) );
    }

    [PermissionSetAttribute(SecurityAction.LinkDemand, Name = "FullTrust")]
    protected override void DetachSink()
    {
      // Disconnect the client that handles the event
      // from the underlying ActiveX control.
      if (cookie != null)
      {
        cookie.Disconnect();
        cookie = null;
      }
      base.DetachSink();
    }

    public event WebBrowserNavigateErrorEventHandler NavigateError;

    // Raises the NavigateError event.
    protected virtual void OnNavigateError(WebBrowserNavigateErrorEventArgs e)
    {
      if (this.NavigateError != null)
      {
        this.NavigateError(this, e);
      }
    }

    // Handles the NavigateError event from the underlying ActiveX 
    // control by raising the NavigateError event defined in this class.
    private class ColaboWebBrowserEventHelper : StandardOleMarshalObject, DWebBrowserEvents2
    {
      private ColaboWebBrowser parent;

      public ColaboWebBrowserEventHelper(ColaboWebBrowser parent)
      {
        this.parent = parent;
      }

      public void NavigateError(object pDisp, ref object url, ref object frame, ref object statusCode, ref bool cancel)
      {
        // Raise the NavigateError event.
        this.parent.OnNavigateError( new WebBrowserNavigateErrorEventArgs( (String)url, (String)frame, (Int32)statusCode, cancel) );
      }
    }

    public int httpErrorCode {
      get {return _httpErrorCode;}
      set {_httpErrorCode = value;}
    }
  }

  // Represents the method that will handle the ColaboWebBrowser.NavigateError event.
  public delegate void WebBrowserNavigateErrorEventHandler(object sender, WebBrowserNavigateErrorEventArgs e);

  // Provides data for the ColaboWebBrowser.NavigateError event.
  public class WebBrowserNavigateErrorEventArgs : EventArgs
  {
    private String urlValue;
    private String frameValue;
    private Int32 statusCodeValue;
    private Boolean cancelValue;

    public WebBrowserNavigateErrorEventArgs(
        String url, String frame, Int32 statusCode, Boolean cancel)
    {
      urlValue = url;
      frameValue = frame;
      statusCodeValue = statusCode;
      cancelValue = cancel;
    }

    public String Url
    {
      get { return urlValue; }
      set { urlValue = value; }
    }

    public String Frame
    {
      get { return frameValue; }
      set { frameValue = value; }
    }

    public Int32 StatusCode
    {
      get { return statusCodeValue; }
      set { statusCodeValue = value; }
    }

    public Boolean Cancel
    {
      get { return cancelValue; }
      set { cancelValue = value; }
    }
  }

}

#region COM Interfaces import Declarations
public struct HRESULT
{
  public const int S_OK = 0;
  public const int S_FALSE = 1;
  public const int E_NOTIMPL = unchecked((int)0x80004001);
  public const int E_INVALIDARG = unchecked((int)0x80070057);
  public const int E_NOINTERFACE = unchecked((int)0x80004002);
  public const int E_FAIL = unchecked((int)0x80004005);
  public const int E_UNEXPECTED = unchecked((int)0x8000FFFF);
}

#region IServiceProvider Interface

[ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown),
 Guid("6D5140C1-7436-11CE-8034-00AA006009FA")]
public interface IServiceProvider
{
  [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType =
MethodCodeType.Runtime)]
  void QueryService([In] ref Guid guidService, [In] ref Guid riid,
[Out] out IntPtr ppvObject);
}

#endregion
#region IProfferService Interface
[ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown),
 Guid("CB728B20-F786-11CE-92AD-00AA00A74CD0")]
public interface IProfferService
{
  [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType =
MethodCodeType.Runtime)]
  void ProfferService([In] ref Guid rguidService, [In,
MarshalAs(UnmanagedType.Interface)] IServiceProvider psp, out uint pdwCookie);
  [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType =
MethodCodeType.Runtime)]
  void RevokeService([In] uint dwCookie);
}

#endregion
#region IAuthenticate Interface
//MIDL_INTERFACE("79eac9d0-baf9-11ce-8c82-00aa004ba90b")
//IAuthenticate : public IUnknown
//{
//public:
//    virtual HRESULT STDMETHODCALLTYPE Authenticate(
//        /* [out] */ HWND *phwnd,
//        /* [out] */ LPWSTR *pszUsername,
//        /* [out] */ LPWSTR *pszPassword) = 0;
//}
[ComImport, InterfaceType(ComInterfaceType.InterfaceIsIUnknown),
ComConversionLoss,
 Guid("79EAC9D0-BAF9-11CE-8C82-00AA004BA90B")]
public interface IAuthenticate
{
  [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType =
MethodCodeType.Runtime)]
  void Authenticate([Out, ComAliasName("UrlMonInterop.wireHWND")] out
IntPtr phwnd, [Out, MarshalAs(UnmanagedType.LPWStr)] out string pszUsername,
[Out, MarshalAs(UnmanagedType.LPWStr)] out string pszPassword);
}

#endregion

#region NavigateError import
// Imports the NavigateError method from the OLE DWebBrowserEvents2 
// interface. 
[ComImport, Guid("34A715A0-6587-11D0-924A-0020AFC7AC4D"),
InterfaceType(ComInterfaceType.InterfaceIsIDispatch),
TypeLibType(TypeLibTypeFlags.FHidden)]
public interface DWebBrowserEvents2
{
  [DispId(271)]
  void NavigateError(
      [In, MarshalAs(UnmanagedType.IDispatch)] object pDisp,
      [In] ref object URL, [In] ref object frame,
      [In] ref object statusCode, [In, Out] ref bool cancel);
}
#endregion

#endregion 
