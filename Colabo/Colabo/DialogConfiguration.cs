using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.DirectoryServices.Protocols;

namespace Colabo
{
  public partial class DialogConfiguration : Form
  {
    FormColabo _owner;
    Configuration _config;
    Configuration.LDAPProfile _ldapProfile;

    public DialogConfiguration(FormColabo owner, Configuration config)
    {
      _owner = owner;
      _config = config;
      _ldapProfile = null;
      InitializeComponent();
    }

    private void DialogConfiguration_Load(object sender, EventArgs e)
    {
      // deep copy of databases configuration
      DBInfoExt[] databases = new DBInfoExt[_config.databases.Count];
      for (int i = 0; i < _config.databases.Count; i++)
      {
        databases[i] = new DBInfoExt(_config.databases[i]);
      }
      databasesList.Items.AddRange(databases);
      editDatabaseButton.Enabled = false;
      deleteDatabaseButton.Enabled = false;
      manageDatabaseButton.Enabled = false;

      // create the deep copy of the rule sets
      RuleSet[] ruleSets = new RuleSet[_owner._ruleSets.Count];
      for (int i = 0; i < _owner._ruleSets.Count; i++ )
      {
        ruleSets[i] = new RuleSet(_owner._ruleSets[i]);
      }
      ruleSetsList.Items.AddRange(ruleSets);
      EditRuleSetButton.Enabled = false;
      DeleteRuleSetButton.Enabled = false;

      // create the deep copy of the scripts
      int n = 0;
      foreach (SQLDatabase database in _owner.databases) n += database.scripts.Count;
      ScriptInfo[] scripts = new ScriptInfo[n];
      {
        int i = 0;
        foreach (SQLDatabase database in _owner.databases)
        {
          foreach (Script script in database.scripts) scripts[i++] = new ScriptInfo(database, (Script)script.Clone());
        }
      }
      scriptsList.Items.AddRange(scripts);
      EditScriptButton.Enabled = false;
      DeleteScriptButton.Enabled = false;

      // create list of credentials (stored passwords)
      Hashtable hashtab = _config.colaboCache;
      IEnumerator listCredentials = hashtab.GetEnumerator();
      while (listCredentials.MoveNext())
      {
        DictionaryEntry auth = (DictionaryEntry)listCredentials.Current;
        Colabo.CredentialKey colKey = (Colabo.CredentialKey)auth.Key;
        passwordsComboBox.Items.Add(new CredentialInfo(colKey.UriPrefix, colKey.Realm, colKey.AuthenticationType));
      }
      DeletePasswordButton.Enabled = false;

      useLDAP.Checked = _config.useLDAP;
      ldapHost.Text = _config.ldapHost;
      ldapPort.Value = _config.ldapPort;
      ldapUser.Text = _config.ldapUser;
      ldapPassword.Text = _config.ldapPassword;
      EnableLDAP();
    }

    public ListBox.ObjectCollection ruleSets
    {
      get { return ruleSetsList.Items; }
    }
    public ListBox.ObjectCollection scripts
    {
      get { return scriptsList.Items; }
    }
    public ListBox.ObjectCollection databases
    {
      get { return databasesList.Items; }
    }

    private LdapConnection OpenConnection()
    {
      return Configuration.OpenLDAPConnection(ldapHost.Text, Convert.ToInt32(ldapPort.Value), ldapUser.Text, ldapPassword.Text);
    }

    private void SaveButton_Click(object sender, EventArgs e)
    {
      if (useLDAP.Checked)
      {
        // Save user settings to LDAP database
        LdapConnection connection = OpenConnection();
        if (connection != null)
        {
          try
          {
            ModifyRequest request = new ModifyRequest();
            request.DistinguishedName = ldapUser.Text;
            if (_ldapProfile != null)
            {
              // store _ldapProfile
              LDAPSetProperty(request, "displayName", valueFullName.Text);
              LDAPSetProperty(request, "colaboUserPicture", valuePicture.Text);
              if (valueColor.Visible)
              {
                int color = Color.FromArgb(0, valueColor.BackColor).ToArgb(); // remove alpha
                LDAPSetProperty(request, "colaboUserColor", color.ToString());
              }
            }
#if DEBUG
#else
            // list of databases
            DirectoryAttributeModification modification = new DirectoryAttributeModification();
            modification.Operation = DirectoryAttributeOperation.Replace;
            modification.Name = "colaboDBDN";
            foreach (DBInfoExt database in databases)
            {
              string dn = database._dbInfo._ldapDN;
              if (!string.IsNullOrEmpty(dn)) modification.Add(dn);
            }
            request.Modifications.Add(modification);
#endif

            connection.SendRequest(request);
          }
          catch (System.Exception exception)
          {
            MessageBox.Show(String.Format("LDAP update failed: {0}", exception.Message), "LDAP", MessageBoxButtons.OK, MessageBoxIcon.Error);
            FormColabo.Log("LDAP exception:" + exception.ToString());
          }
        }
      }

      _config.SaveLDAP(useLDAP.Checked, ldapHost.Text, (int)ldapPort.Value, ldapUser.Text, ldapPassword.Text);
      _config.Save(this, Program.forms);
      Close();
    }
    private void LDAPSetProperty(ModifyRequest request, string name, string value)
    {
      DirectoryAttributeModification modification = new DirectoryAttributeModification();
      modification.Operation = DirectoryAttributeOperation.Replace;
      modification.Name = name;
      if (!string.IsNullOrEmpty(value)) modification.Add(value);
      request.Modifications.Add(modification);
    }

    private void ruleSetsList_SelectedIndexChanged(object sender, EventArgs e)
    {
      RuleSet ruleSet = (RuleSet)ruleSetsList.SelectedItem;
      if (ruleSet == null)
      {
        EditRuleSetButton.Enabled = false;
        EditRuleSetButton.Text = "Edit";
        DeleteRuleSetButton.Enabled = false;
      }
      else if (ruleSet._user.Equals("default", StringComparison.OrdinalIgnoreCase))
      {
        EditRuleSetButton.Enabled = true;
        EditRuleSetButton.Text = "View";
        DeleteRuleSetButton.Enabled = false;
      }
      else
      {
        EditRuleSetButton.Enabled = true;
        EditRuleSetButton.Text = "Edit";
        DeleteRuleSetButton.Enabled = true;
      }
    }

    private void AddRuleSetButton_Click(object sender, EventArgs e)
    {
      DialogRuleSet dialog = new DialogRuleSet(ruleSetsList.Items, -1, false, _config.databases[0]);
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK)
      {
        RuleSet ruleSet = new RuleSet();
        if (_config.databases.Count > 0)
        {
          ruleSet._user = _config.databases[0]._user;
        }
        ruleSet._name = dialog.name;
        ruleSet._expression = dialog.ruleSet;
        ruleSet._parentUser = dialog.parentUser;
        ruleSet._parentName = dialog.parentName;
        ruleSetsList.Items.Add(ruleSet);
      }
    }
    private void EditRuleSetButton_Click(object sender, EventArgs e)
    {
      RuleSet ruleSet = (RuleSet)ruleSetsList.SelectedItem;
      if (ruleSet != null)
      {
        bool readOnly = ruleSet._user.Equals("default", StringComparison.OrdinalIgnoreCase);
        DialogRuleSet dialog = new DialogRuleSet(ruleSetsList.Items, ruleSetsList.SelectedIndex, readOnly, _config.databases[0]);
        DialogResult result = dialog.ShowDialog();
        if (result == DialogResult.OK && !readOnly)
        {
          if (_config.databases.Count > 0)
          {
            ruleSet._user = _config.databases[0]._user;
          }
          ruleSet._name = dialog.name;
          ruleSet._expression = dialog.ruleSet;
          ruleSet._parentUser = dialog.parentUser;
          ruleSet._parentName = dialog.parentName;
          ruleSetsList.Items[ruleSetsList.SelectedIndex] = ruleSet;
          ruleSetsList.Invalidate();
        }
      }
    }
    private void DeleteRuleSetButton_Click(object sender, EventArgs e)
    {
      RuleSet ruleSet = (RuleSet)ruleSetsList.SelectedItem;
      if (ruleSet != null && !ruleSet._user.Equals("default", StringComparison.OrdinalIgnoreCase))
      {
        ruleSetsList.Items.Remove(ruleSet);
      }
    }

    private void scriptsList_SelectedIndexChanged(object sender, EventArgs e)
    {
      ScriptInfo script = (ScriptInfo)scriptsList.SelectedItem;
      if (script == null)
      {
        EditScriptButton.Enabled = false;
        EditScriptButton.Text = "Edit";
        DeleteScriptButton.Enabled = false;
      }
      else if (script._script._user.Equals("default", StringComparison.OrdinalIgnoreCase))
      {
        EditScriptButton.Enabled = true;
        EditScriptButton.Text = "View";
        DeleteScriptButton.Enabled = false;
      }
      else
      {
        EditScriptButton.Enabled = true;
        EditScriptButton.Text = "Edit";
        DeleteScriptButton.Enabled = true;
      }
    }

    private void AddScriptButton_Click(object sender, EventArgs e)
    {
      DialogScript dialog = new DialogScript(_owner, scriptsList.Items, -1, false);
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK)
      {
        Script script = new Script();
        script._name = dialog.name;
        script._usage = dialog.usage;
        script._image = dialog.image;
        script._language = dialog.language;
        script._script = dialog.script;

        // TODO: valid database and user
        SQLDatabase database = _owner.databases[0];
        script._user = _owner.databases[0].user;
        
        ScriptInfo info = new ScriptInfo(database, script);
        scriptsList.Items.Add(info);
      }
    }
    private void EditScriptButton_Click(object sender, EventArgs e)
    {
      ScriptInfo script = (ScriptInfo)scriptsList.SelectedItem;
      if (script != null)
      {
        bool readOnly = script._script._user.Equals("default", StringComparison.OrdinalIgnoreCase);

        DialogScript dialog = new DialogScript(_owner, scriptsList.Items, scriptsList.SelectedIndex, readOnly);
        DialogResult result = dialog.ShowDialog();
        if (result == DialogResult.OK)
        {
          script._script._name = dialog.name;
          script._script._usage = dialog.usage;
          script._script._image = dialog.image;
          script._script._language = dialog.language;
          script._script._script = dialog.script;
          scriptsList.Items[scriptsList.SelectedIndex] = script;
          scriptsList.Invalidate();
        }
      }
    }
    private void DeleteScriptButton_Click(object sender, EventArgs e)
    {
      ScriptInfo script = (ScriptInfo)scriptsList.SelectedItem;
      if (script != null && !script._script._user.Equals("default", StringComparison.OrdinalIgnoreCase))
      {
        scriptsList.Items.Remove(script);
      }
    }

    private void databasesList_SelectedIndexChanged(object sender, EventArgs e)
    {
      DBInfoExt info = (DBInfoExt)databasesList.SelectedItem;
      editDatabaseButton.Enabled = info != null;
      deleteDatabaseButton.Enabled = info != null;
      manageDatabaseButton.Enabled = info != null && IsLDAPWriteAccess(info._dbInfo._ldapDN);
    }
    private void addDatabaseButton_Click(object sender, EventArgs e)
    {
      string host = null, user = null, password = null;
      int port = 0;
      if (useLDAP.Checked)
      {
        host = ldapHost.Text;
        port = Convert.ToInt32(ldapPort.Value);
        user = ldapUser.Text;
        password = ldapPassword.Text;
      }

      DialogDatabase dialog = new DialogDatabase(databases, -1, host, port, user, password);
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK)
      {
        databasesList.Items.Add(dialog.GetDBInfo());
      }
    }
    private void editDatabaseButton_Click(object sender, EventArgs e)
    {
      DBInfoExt database = (DBInfoExt)databasesList.SelectedItem;
      if (database != null)
      {
        string host = null, user = null, password = null;
        int port = 0;
        if (useLDAP.Checked)
        {
          host = ldapHost.Text;
          port = Convert.ToInt32(ldapPort.Value);
          user = ldapUser.Text;
          password = ldapPassword.Text;
        }

        DialogDatabase dialog = new DialogDatabase(databases, databasesList.SelectedIndex, host, port, user, password);
        DialogResult result = dialog.ShowDialog();
        if (result == DialogResult.OK)
        {
          databasesList.Items[databasesList.SelectedIndex] = dialog.GetDBInfo();
        }
      }
    }
    private void deleteDatabaseButton_Click(object sender, EventArgs e)
    {
      DBInfoExt database = (DBInfoExt)databasesList.SelectedItem;
      if (database != null) databasesList.Items.Remove(database);
    }
    private void manageDatabaseButton_Click(object sender, EventArgs e)
    {
      DBInfoExt database = (DBInfoExt)databasesList.SelectedItem;
      if (useLDAP.Checked && database != null && !string.IsNullOrEmpty(database._dbInfo._ldapDN))
      {
        // Open Database Management dialog, store the results
        DialogDatabaseManagement dialog = new DialogDatabaseManagement(ldapHost.Text, Convert.ToInt32(ldapPort.Value), database._dbInfo._ldapDN, ldapUser.Text, ldapPassword.Text);
        DialogResult result = dialog.ShowDialog();
        if (result == DialogResult.OK)
        {
          // nothing to do now, result was stored by the dialog already
        }
      }
    }

    private void passwordsComboBox_SelectedIndexChanged(object sender, EventArgs e)
    {
      CredentialInfo key = (CredentialInfo)passwordsComboBox.SelectedItem;
      if (key != null)
      {
        DeletePasswordButton.Enabled = true;
      }
      else
      {
        DeleteRuleSetButton.Enabled = false;
      }
    }

    private void DeletePasswordButton_Click(object sender, EventArgs e)
    {
      CredentialInfo key = (CredentialInfo)passwordsComboBox.SelectedItem;
      if (key != null)
      {
        passwordsComboBox.Items.Remove(key);
        _config.RemoveAuthentication(key.UriPrefix, key.Realm, key.AuthenticationType, true);
      }
    }

    // check if write is enabled to the given LDAP entry
    private bool IsLDAPWriteAccess(string dn)
    {
      if (string.IsNullOrEmpty(dn)) return false;
      if (!useLDAP.Checked) return false;

      LdapConnection connection = OpenConnection();
      if (connection == null) return false;

      try
      {
        string cn = null;

        SearchRequest readRequest = new SearchRequest(dn, "(objectClass=*)", SearchScope.Base, new String[] {"cn"});
        SearchResponse readResponse = (SearchResponse)connection.SendRequest(readRequest);
        if (readResponse.Entries.Count == 1)
        {
          SearchResultEntry entry = readResponse.Entries[0];
          DirectoryAttribute attributes = entry.Attributes["cn"];
          if (attributes != null && attributes.Count == 1) cn = attributes[0].ToString();
        }
        if (cn == null) return false;

        ModifyRequest request = new ModifyRequest();
        request.DistinguishedName = dn;
        LDAPSetProperty(request, "cn", cn);
        connection.SendRequest(request);
        return true;
      }
      catch //(System.Exception e)
      {
        return false;
      }
    }

    private void EnableLDAP()
    {
      bool enable = useLDAP.Checked;

      ldapHost.Enabled = enable;
      ldapPort.Enabled = enable;
      ldapUser.Enabled = enable;
      ldapPassword.Enabled = enable;
      ldapConnect.Enabled = enable;

      bool profileEnabled = enable && _ldapProfile != null;
      valueFullName.Enabled = profileEnabled;
      valuePicture.Enabled = profileEnabled;
      valueColor.Visible = profileEnabled && _ldapProfile._colorSet;
      buttonColor.Enabled = profileEnabled;
    }

    private void useLDAP_CheckedChanged(object sender, EventArgs e)
    {
      EnableLDAP();
    }

    private void ldapConnect_Click(object sender, EventArgs e)
    {
      LdapConnection connection = OpenConnection();
      if (connection != null)
      {
        try
        {
          SearchRequest request = new SearchRequest(ldapUser.Text, "(objectClass=*)", SearchScope.Base,
            new String[] {"uid", "displayName", "colaboUserPicture", "colaboUserColor"});
          SearchResponse response = (SearchResponse)connection.SendRequest(request);

          if (response.Entries.Count == 0)
          {
            // user not found in LDAP
            MessageBox.Show(String.Format("Entry {0} found.", ldapUser.Text), "LDAP", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
          }
          SearchResultEntry entry = response.Entries[0];

          // read the info and enable to configure it
          Configuration.LDAPProfile profile = new Configuration.LDAPProfile();

          DirectoryAttribute attributes = entry.Attributes["uid"];
          if (attributes != null && attributes.Count == 1) profile._uid = attributes[0].ToString();
          attributes = entry.Attributes["displayName"];
          if (attributes != null && attributes.Count == 1) profile._name = attributes[0].ToString();
          attributes = entry.Attributes["colaboUserPicture"];
          if (attributes != null && attributes.Count == 1) profile._picture = attributes[0].ToString();
          attributes = entry.Attributes["colaboUserColor"];
          if (attributes != null && attributes.Count == 1)
          {
            profile._colorSet = int.TryParse(attributes[0].ToString(), out profile._color);
          }

          _ldapProfile = profile;

          valueFullName.Text = profile._name;
          valuePicture.Text = profile._picture;
          valueColor.BackColor = Color.FromArgb(255, Color.FromArgb(profile._color)); // set alpha = 255
          EnableLDAP();
        }
        catch (System.Exception exception)
        {
          MessageBox.Show(String.Format("LDAP authentication failed: {0}", exception.Message), "LDAP", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
      }
    }

    private void buttonColor_Click(object sender, EventArgs e)
    {
      colorDialog.Color = valueColor.BackColor;
      DialogResult result = colorDialog.ShowDialog();
      if (result == DialogResult.OK)
      {
        valueColor.BackColor = colorDialog.Color;
        valueColor.Visible = true;
      }
    }

    private class CredentialInfo
    {
      public Uri UriPrefix;
      public string Realm;
      public string AuthenticationType;

      public CredentialInfo(Uri uriPrefix, string realm, string authenticationType)
      {
        UriPrefix = uriPrefix;
        Realm = realm;
        AuthenticationType = authenticationType;
      }

      public override string ToString()
      {
        if (string.IsNullOrEmpty(Realm)) return UriPrefix.ToString();
        return UriPrefix.ToString() + ":" + Realm;
      }
    }
  }

  // database user (extended) properties
  public class DBUserInfo
  {
    // name used in UI
    public string _fullName;
    // URL to the user picture
    public string _picture;
    // user color
    public int _color;
    // comma separated list of aliases
    public string _aliases;

    public DBUserInfo()
    {
      _color = 0;
    }
  }
  
  // DBInfo extended by the user properties
  public class DBInfoExt
  {
    public DBInfo _dbInfo;
    public DBUserInfo _userInfo;

    public DBInfoExt()
    {
      _dbInfo = new DBInfo();
      _userInfo = null;
    }
    public DBInfoExt(DBInfo src)
    {
      _dbInfo = new DBInfo(src);
      _userInfo = null;
    }

    public override string ToString()
    {
      return _dbInfo == null ? "" : _dbInfo.ToString();
    }
  }
}
