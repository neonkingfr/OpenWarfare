﻿namespace Colabo
{
  partial class SslServerCertificateTrustDialog
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.text = new System.Windows.Forms.Label();
      this.acceptPermanently = new System.Windows.Forms.Button();
      this.acceptOnce = new System.Windows.Forms.Button();
      this.reject = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // text
      // 
      this.text.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.text.Location = new System.Drawing.Point(12, 9);
      this.text.Name = "text";
      this.text.Size = new System.Drawing.Size(528, 159);
      this.text.TabIndex = 0;
      this.text.Text = "label1";
      // 
      // acceptPermanently
      // 
      this.acceptPermanently.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.acceptPermanently.DialogResult = System.Windows.Forms.DialogResult.Yes;
      this.acceptPermanently.Location = new System.Drawing.Point(237, 171);
      this.acceptPermanently.Name = "acceptPermanently";
      this.acceptPermanently.Size = new System.Drawing.Size(121, 23);
      this.acceptPermanently.TabIndex = 1;
      this.acceptPermanently.Text = "Accept permanently";
      this.acceptPermanently.UseVisualStyleBackColor = true;
      // 
      // acceptOnce
      // 
      this.acceptOnce.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.acceptOnce.DialogResult = System.Windows.Forms.DialogResult.No;
      this.acceptOnce.Location = new System.Drawing.Point(364, 171);
      this.acceptOnce.Name = "acceptOnce";
      this.acceptOnce.Size = new System.Drawing.Size(94, 23);
      this.acceptOnce.TabIndex = 2;
      this.acceptOnce.Text = "Accept once";
      this.acceptOnce.UseVisualStyleBackColor = true;
      // 
      // reject
      // 
      this.reject.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.reject.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.reject.Location = new System.Drawing.Point(464, 171);
      this.reject.Name = "reject";
      this.reject.Size = new System.Drawing.Size(76, 23);
      this.reject.TabIndex = 3;
      this.reject.Text = "Reject";
      this.reject.UseVisualStyleBackColor = true;
      // 
      // SslServerCertificateTrustDialog
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.reject;
      this.ClientSize = new System.Drawing.Size(552, 206);
      this.ControlBox = false;
      this.Controls.Add(this.reject);
      this.Controls.Add(this.acceptOnce);
      this.Controls.Add(this.acceptPermanently);
      this.Controls.Add(this.text);
      this.MaximizeBox = false;
      this.MaximumSize = new System.Drawing.Size(560, 240);
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(560, 240);
      this.Name = "SslServerCertificateTrustDialog";
      this.Text = "Colabo";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label text;
    private System.Windows.Forms.Button acceptPermanently;
    private System.Windows.Forms.Button acceptOnce;
    private System.Windows.Forms.Button reject;
  }
}