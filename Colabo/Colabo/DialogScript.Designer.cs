namespace Colabo
{
  partial class DialogScript
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.buttonCancel = new System.Windows.Forms.Button();
      this.buttonOK = new System.Windows.Forms.Button();
      this.valueName = new System.Windows.Forms.TextBox();
      this.valueUsage = new System.Windows.Forms.TextBox();
      this.valueImage = new System.Windows.Forms.ComboBox();
      this.valueLanguage = new System.Windows.Forms.ComboBox();
      this.valueScript = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // buttonCancel
      // 
      this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.buttonCancel.Location = new System.Drawing.Point(358, 451);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.Size = new System.Drawing.Size(75, 23);
      this.buttonCancel.TabIndex = 5;
      this.buttonCancel.Text = "Cancel";
      this.buttonCancel.UseVisualStyleBackColor = true;
      // 
      // buttonOK
      // 
      this.buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.buttonOK.Location = new System.Drawing.Point(277, 451);
      this.buttonOK.Name = "buttonOK";
      this.buttonOK.Size = new System.Drawing.Size(75, 23);
      this.buttonOK.TabIndex = 4;
      this.buttonOK.Text = "OK";
      this.buttonOK.UseVisualStyleBackColor = true;
      // 
      // valueName
      // 
      this.valueName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueName.Location = new System.Drawing.Point(105, 12);
      this.valueName.Name = "valueName";
      this.valueName.Size = new System.Drawing.Size(328, 20);
      this.valueName.TabIndex = 6;
      // 
      // valueUsage
      // 
      this.valueUsage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueUsage.Location = new System.Drawing.Point(105, 38);
      this.valueUsage.Name = "valueUsage";
      this.valueUsage.Size = new System.Drawing.Size(328, 20);
      this.valueUsage.TabIndex = 7;
      // 
      // valueImage
      // 
      this.valueImage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueImage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.valueImage.FormattingEnabled = true;
      this.valueImage.Location = new System.Drawing.Point(105, 65);
      this.valueImage.Name = "valueImage";
      this.valueImage.Size = new System.Drawing.Size(328, 21);
      this.valueImage.TabIndex = 8;
      // 
      // valueLanguage
      // 
      this.valueLanguage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.valueLanguage.FormattingEnabled = true;
      this.valueLanguage.Location = new System.Drawing.Point(105, 92);
      this.valueLanguage.Name = "valueLanguage";
      this.valueLanguage.Size = new System.Drawing.Size(328, 21);
      this.valueLanguage.TabIndex = 9;
      // 
      // valueScript
      // 
      this.valueScript.AcceptsReturn = true;
      this.valueScript.AcceptsTab = true;
      this.valueScript.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueScript.Location = new System.Drawing.Point(12, 119);
      this.valueScript.Multiline = true;
      this.valueScript.Name = "valueScript";
      this.valueScript.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.valueScript.Size = new System.Drawing.Size(421, 326);
      this.valueScript.TabIndex = 10;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 15);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(38, 13);
      this.label1.TabIndex = 11;
      this.label1.Text = "Name:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 41);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(41, 13);
      this.label2.TabIndex = 12;
      this.label2.Text = "Usage:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(12, 68);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(39, 13);
      this.label3.TabIndex = 13;
      this.label3.Text = "Image:";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(12, 95);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(58, 13);
      this.label4.TabIndex = 14;
      this.label4.Text = "Language:";
      // 
      // DialogScript
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(445, 486);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.valueScript);
      this.Controls.Add(this.valueLanguage);
      this.Controls.Add(this.valueImage);
      this.Controls.Add(this.valueUsage);
      this.Controls.Add(this.valueName);
      this.Controls.Add(this.buttonCancel);
      this.Controls.Add(this.buttonOK);
      this.MinimumSize = new System.Drawing.Size(270, 300);
      this.Name = "DialogScript";
      this.Text = "Script";
      this.Load += new System.EventHandler(this.DialogScript_Load);
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DialogScript_FormClosing);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button buttonCancel;
    private System.Windows.Forms.Button buttonOK;
    private System.Windows.Forms.TextBox valueName;
    private System.Windows.Forms.TextBox valueUsage;
    private System.Windows.Forms.ComboBox valueImage;
    private System.Windows.Forms.ComboBox valueLanguage;
    private System.Windows.Forms.TextBox valueScript;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
  }
}