﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Colabo
{
  public partial class DialogSelectUser : Form
  {
    DialogArticle owner_;
    public DialogSelectUser(DialogArticle owner)
    {
      owner_ = owner;
      InitializeComponent();
      chatUsers.StateImageList = owner.owner.PresenceImages;
      ListUsers(owner.database);

    }

    public void ListUsers(SQLDatabase db)
    {

      Action<KeyValuePair<String, UserInfo>> addUser = delegate(KeyValuePair<String, UserInfo> user)
      {
        var item = new ChatUserItem(user.Key, user.Value);
        item.Name = user.Key;
        chatUsers.Items.Add(item);
      };
      var usersCopy = new Dictionary<string, UserInfo>(db.users);
      foreach (var user in usersCopy)
      {
        if (user.Value._presence != null && user.Value._presence.Length > 0) addUser(user);
      }
      foreach (var user in usersCopy)
      {
        if (user.Value._presence == null || user.Value._presence.Length <= 0) addUser(user);
      }
    
    
    
    }

    private void buttonOK_Click(object sender, EventArgs e)
    {
      List<string> users = new List<string>();
      foreach (ChatUserItem item in chatUsers.SelectedItems)
      {
        users.Add(item.Name);
      }
      owner_.AddChatUsers(users);
      Close();
    }

    private void buttonCancel_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void DialogSelectUser_FormClosed(object sender, FormClosedEventArgs e)
    {
      owner_.SelectUserClosed();
    }
  }
}
