using System;
using System.Threading;
using System.Windows.Forms;
using System.Collections.Generic;

using System.Security.Cryptography;
using System.Text;

using Token = Antlr.Runtime.IToken;

using Connection = System.Data.Common.DbConnection;
using Command = System.Data.Common.DbCommand;
using Reader = System.Data.Common.DbDataReader;
using Parameter = System.Data.Common.DbParameter;
using DbType = System.Data.DbType;

namespace Colabo
{
  /// Implementation of DBEngine using the direct connection to SQL server
  public class SQLEngine : DBEngine
  {
    /// SQL connection string
    private Connection _connection;
    /// Special SQL command to retrieve the ID of changed article
    private string _commandIDSuffix;

    /// current database connection mode
    private enum DbMode
    {
      Disconnected,
      Offline,
      Online
    };
    private DbMode _mode;

    /// full-text index and query support
    private FullText _fullText;

    /*
        public Connection connection {get {return _connection;}}
        public string commandIDSuffix {get {return _commandIDSuffix;}}
    */

    public SQLEngine(DBInfo info)
      : base(info)
    {
      _mode = DbMode.Online;
      _fullText = new FullText(info._server, info._database);

      if (info._serverType == DBServerType.STMySQL)
      {
        _connection = new MySql.Data.MySqlClient.MySqlConnection();
        _commandIDSuffix = "SELECT LAST_INSERT_ID()";
        _connection.ConnectionString = string.Format(
          "Data Source={0};Initial Catalog={1};Persist Security Info=False;User ID={2};Password={3};charset=utf8",
          info._server, info._database, info._user, info._password);
      }
      else // DBServerType.STSQLServer
      {
        _connection = new System.Data.SqlClient.SqlConnection();
        _commandIDSuffix = "SELECT SCOPE_IDENTITY()";
        _connection.ConnectionString = string.Format(
          "Data Source={0};Initial Catalog={1};Persist Security Info=False;User ID={2};Password={3}",
          info._server, info._database, info._user, info._password);
      }
    }

    public override DBUserInfo LoadUserInfo()
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return null;

        DBUserInfo userInfo = new DBUserInfo();
        userInfo._aliases = "";

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@User", _info._user);

        cmd.CommandText = "SELECT FullName, Picture, Color FROM Users WHERE (ID = @User)";
        using (Reader reader = cmd.ExecuteReader())
        {
          if (reader != null)
            while (reader.Read())
            {
              userInfo._fullName = ReadString(reader[0]);
              userInfo._picture = ReadString(reader[1]);
              userInfo._color = ReadInt(reader[2], 0);
              break; // only one item should be present
            }
        }

        cmd.CommandText = "SELECT Alias FROM UserAliases WHERE (ID = @User)";
        using (Reader reader = cmd.ExecuteReader())
        {
          if (reader != null)
            while (reader.Read())
            {
              string alias = ReadString(reader[0]);
              if (!string.IsNullOrEmpty(alias))
              {
                if (userInfo._aliases.Length > 0) userInfo._aliases += ", ";
                userInfo._aliases += alias;
              }
            }
        }

        return userInfo;
      }
    }

    public override void SaveUserInfo(DBUserInfo userInfo)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        // insert a new record if not exist yet
        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@User", _info._user);

        cmd.CommandText = "INSERT IGNORE INTO Users (ID) VALUES (@User)";
        cmd.ExecuteNonQuery();

        // remove the current aliases
        cmd.CommandText = "DELETE FROM UserAliases WHERE (ID = @User)";
        cmd.ExecuteNonQuery();

        // update the basic values
        AddParameter(ref cmd, "@FullName", userInfo._fullName);
        AddParameter(ref cmd, "@Picture", userInfo._picture);
        AddParameter(ref cmd, "@Color", userInfo._color);
        cmd.CommandText = "UPDATE Users SET FullName = @FullName, Picture = @Picture, Color = @Color WHERE (ID = @User)";
        cmd.ExecuteNonQuery();

        // insert the new aliases
        cmd.Parameters.Clear();
        AddParameter(ref cmd, "@User", _info._user);

        string aliases = userInfo._aliases;
        string args = "";
        int pos = 0;
        int index = 1;
        do
        {
          int end = aliases.IndexOf(',', pos);
          string alias = (end >= pos ? aliases.Substring(pos, end - pos) : aliases.Substring(pos)).Trim();
          if (alias.Length > 0)
          {
            if (args.Length > 0) args += ",";
            string paramName = string.Format("@Alias{0:D}", index++);
            args += string.Format("({0},@User)", paramName);
            AddParameter(ref cmd, paramName, alias);
          }
          pos = end + 1;
        } while (pos > 0);
        if (args.Length > 0)
        {
          cmd.CommandText = "INSERT IGNORE INTO UserAliases (Alias, ID) VALUES " + args;
          cmd.ExecuteNonQuery();
        }
      }
    }

    public override void SaveUser(UserInfo userInfo)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@FullName", userInfo._fullName);
        AddParameter(ref cmd, "@Picture", userInfo._picture);
        AddParameter(ref cmd, "@Color", userInfo._color);
        AddParameter(ref cmd, "@User", _info._user);

        cmd.CommandText = "UPDATE Users FullName = @FullName, Picture = @Picture, Color = @Color WHERE (ID = @User)";

        try
        {
          cmd.ExecuteNonQuery();
        }
        catch (Exception e)
        {
          Console.WriteLine(e.ToString());
        }
      }
    }

    public override void LoadUsers(ref Dictionary<string, UserInfo> users, ref Dictionary<string, string> aliases, NotifyUserChange notifyUserChange, NotifyChat notifyChat)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();

        // read users
        cmd.CommandText = "SELECT ID, FullName, Picture, Color FROM Users";
        using (Reader reader = cmd.ExecuteReader())
        {
          if (reader != null)
            while (reader.Read())
            {
              UserInfo userInfo = new UserInfo();
              string id = ReadString(reader[0]);
              userInfo._fullName = ReadString(reader[1]);
              userInfo._picture = ReadString(reader[2]);
              userInfo._color = ReadInt(reader[3], 0);
              users.Add(id, userInfo);
            }
        }

        // read aliases
        cmd.CommandText = "SELECT Alias, ID FROM UserAliases";
        using (Reader reader = cmd.ExecuteReader())
        {
          if (reader != null)
            while (reader.Read())
            {
              string alias = ReadString(reader[0]);
              string id = ReadString(reader[1]);
              aliases.Add(alias, id);
            }
        }
      }
    }

    public override void SaveUsers(List<ManageUserInfo> users, List<ManageGroupInfo> groups, List<ManageGroupUserInfo> groupUsers)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();

        // users
        string deleteList = "", insertList = "";
        int index = 1;
        foreach (ManageUserInfo user in users)
        {
          string paramUser = string.Format("@User{0:D}", index);
          if (index > 1)
          {
            deleteList += " AND ";
            insertList += ", ";
          }
          deleteList += string.Format("ID != {0}", paramUser);
          insertList += string.Format("({0})", paramUser);
          AddParameter(ref cmd, paramUser, user._id);
          index++;
        }
        cmd.CommandText = string.Format("DELETE FROM Users WHERE ({0})", deleteList);
        cmd.ExecuteNonQuery();
        cmd.CommandText = string.Format("INSERT IGNORE INTO Users (ID) VALUES {0}", insertList);
        cmd.ExecuteNonQuery();
        foreach (ManageUserInfo user in users)
        {
          cmd.Parameters.Clear();
          AddParameter(ref cmd, "@Access", user._access);
          AddParameter(ref cmd, "@User", user._id);
          AddParameter(ref cmd, "@Password", user._password);
          cmd.CommandText = "UPDATE Users SET AccessRights = @Access, Password = @Password WHERE (ID = @User)";
          cmd.ExecuteNonQuery();
        }

        // groups
        cmd.Parameters.Clear();
        cmd.CommandText = "DELETE FROM Groups";
        cmd.ExecuteNonQuery();
        insertList = "";
        index = 1;
        foreach (ManageGroupInfo group in groups)
        {
          string paramId = string.Format("@ID{0:D}", index);
          string paramName = string.Format("@Name{0:D}", index);
          string paramAccess = string.Format("@Access{0:D}", index);
          string paramLevel = string.Format("@Level{0:D}", index);
          string paramHome = string.Format("@Home{0:D}", index);
          if (index > 1) insertList += ", ";
          insertList += string.Format("({0}, {1}, {2}, {3}, {4})", paramId, paramName, paramAccess, paramLevel, paramHome);
          AddParameter(ref cmd, paramId, group._id);
          AddParameter(ref cmd, paramName, group._name);
          AddParameter(ref cmd, paramAccess, group._access);
          AddParameter(ref cmd, paramLevel, group._level);
          AddParameter(ref cmd, paramHome, group._home);
          index++;
        }
        cmd.CommandText = string.Format("INSERT IGNORE INTO Groups (ID, FullName, AccessRights, Level, Homepage) VALUES {0}", insertList);
        cmd.ExecuteNonQuery();

        // group users
        cmd.Parameters.Clear();
        cmd.CommandText = "DELETE FROM GroupUsers";
        cmd.ExecuteNonQuery();
        insertList = "";
        index = 1;
        foreach (ManageGroupUserInfo info in groupUsers)
        {
          if (index > 1) insertList += ", ";

          string paramGroup = string.Format("@Group{0:D}", index);
          string paramUser = string.Format("@User{0:D}", index);

          insertList += string.Format("({0}, {1})", paramGroup, paramUser);
          AddParameter(ref cmd, paramGroup, info._group);
          AddParameter(ref cmd, paramUser, info._user);
          index++;
        }
        cmd.CommandText = string.Format("INSERT IGNORE INTO GroupUsers (GroupID, UserID) VALUES {0}", insertList);
        cmd.ExecuteNonQuery();
      }
    }

    public override void LoadUserSettings(ref List<string> groups, ref List<Token> accessRights, ref int homepage)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@User", _info._user);

        // list of groups, user settings defined for these groups
        cmd.CommandText = "SELECT ID, AccessRights, HomePage FROM Groups INNER JOIN GroupUsers ON (ID = GroupID) WHERE (UserId = @User) ORDER BY Level";
        using (Reader reader = cmd.ExecuteReader())
        {
          if (reader != null)
            while (reader.Read())
            {
              string group = ReadString(reader[0]);
              groups.Add(group);
              string rights = ReadString(reader[1]).Trim();
              if (!string.IsNullOrEmpty(rights))
              {
                List<Token> set = FormColabo.CompileRuleSet(rights);
                if (set != null) accessRights.AddRange(set);
              }
              int home = ReadInt(reader[2], 0);
              if (home > 0) homepage = home;
            }
        }

        // user settings defined for user
        cmd.CommandText = "SELECT AccessRights, Homepage FROM Users WHERE (ID = @User)";
        using (Reader reader = cmd.ExecuteReader())
        {
          if (reader != null)
            while (reader.Read())
            {
              string rights = ReadString(reader[0]).Trim();
              if (!string.IsNullOrEmpty(rights))
              {
                List<Token> set = FormColabo.CompileRuleSet(rights);
                if (set != null) accessRights.AddRange(set);
              }
              int home = ReadInt(reader[1], 0);
              if (home > 0) homepage = home;
            }
        }
      }
    }

    public override int LoadHomepage()
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return -1;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@User", _info._user);

        cmd.CommandText = "SELECT Homepage FROM Users WHERE (ID = @User)";
        object result = cmd.ExecuteScalar();
        if (result != null && result != DBNull.Value) return Convert.ToInt32(result);
        else return -1;
      }
    }

    public override void SaveHomepage(int homepage)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@User", _info._user);

        // insert a new record if not exist yet
        cmd.CommandText = "INSERT IGNORE INTO Users (ID) VALUES (@User)";
        cmd.ExecuteNonQuery();

        // update the basic values
        AddParameter(ref cmd, "@Homepage", homepage);
        cmd.CommandText = "UPDATE Users SET Homepage = @Homepage WHERE (ID = @User)";
        cmd.ExecuteNonQuery();
      }
    }

    public override void LoadRuleSets(ref List<RuleSet> ruleSets)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@User", _info._user);

        cmd.CommandText = "SELECT Name, RuleSetName, Expression, ParentName, ParentRuleSetName FROM RuleSets WHERE (Name = 'Default' OR Name = @User)";
        using (Reader reader = cmd.ExecuteReader())
        {
          if (reader != null)
            while (reader.Read())
            {
              RuleSet ruleSet = new RuleSet();
              ruleSet._user = ReadString(reader[0]);
              ruleSet._name = ReadString(reader[1]);
              ruleSet._expression = ReadString(reader[2]);
              ruleSet._parentUser = ReadString(reader[3]);
              ruleSet._parentName = ReadString(reader[4]);
              ruleSets.Add(ruleSet);
            }
        }
      }
    }

    public override void SaveRuleSets(ListBox.ObjectCollection ruleSets)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@User", _info._user);

        // remove the old rule sets
        cmd.CommandText = "DELETE FROM RuleSets WHERE (Name = @User)";
        cmd.ExecuteNonQuery();

        // insert the new rule sets
        string values = "";
        int index = 1;
        foreach (RuleSet ruleSet in ruleSets)
        {
          if (!ruleSet._user.Equals(_info._user)) continue;
          if (values.Length > 0) values += ",";

          string paramName = string.Format("@Name{0:D}", index);
          string paramExpr = string.Format("@Expr{0:D}", index);
          string paramParentUser = string.Format("@ParentUser{0:D}", index);
          string paramParentName = string.Format("@ParentName{0:D}", index);
          index++;

          values += string.Format("(@User,{0},{1},{2},{3})",
            paramName, paramExpr, paramParentUser, paramParentName);
          AddParameter(ref cmd, paramName, ruleSet._name);
          AddParameter(ref cmd, paramExpr, ruleSet._expression);
          AddParameter(ref cmd, paramParentUser, ruleSet._parentUser);
          AddParameter(ref cmd, paramParentName, ruleSet._parentName);
        }
        if (values.Length > 0)
        {
          cmd.CommandText = "INSERT IGNORE INTO RuleSets (Name, RuleSetName, Expression, ParentName, ParentRuleSetName) VALUES " + values;
          cmd.ExecuteNonQuery();
        }
      }
    }

    public override void LoadBookmarks(ref List<Bookmark> bookmarks, SQLDatabase database)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@User", _info._user);

        cmd.CommandText = "SELECT ID, Title, Tags, Article, Image, Parent FROM Bookmarks WHERE (Name = @User)";

        using (Reader reader = cmd.ExecuteReader())
        {
          if (reader != null)
            while (reader.Read())
            {
              Bookmark bookmark = new Bookmark(database);
              bookmark._id = ReadInt(reader[0], -1);
              bookmark._name = ReadString(reader[1]);
              ArticleItem.ReadTags(ReadString(reader[2]), out bookmark._tags);
              bookmark._article = new ArticleId(database, ReadInt(reader[3], -1));
              bookmark._image = ReadInt(reader[4], 0);
              bookmark._parent = ReadInt(reader[5], -1);
              bookmarks.Add(bookmark);
            }
        }
      }
    }

    public override int SaveBookmark(Bookmark bookmark, SQLDatabase database)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return -1;

        string tags = ArticleItem.WriteTags(bookmark._tags);
        string article = (bookmark._article._database == database && bookmark._article._id >= 0) ? bookmark._article._id.ToString() : null;
        string parent = bookmark._parent >= 0 ? bookmark._parent.ToString() : null;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@User", _info._user);
        AddParameter(ref cmd, "@Name", bookmark._name);
        AddParameter(ref cmd, "@Tags", tags);
        AddParameter(ref cmd, "@Article", article);
        AddParameter(ref cmd, "@Image", bookmark._image);
        AddParameter(ref cmd, "@Parent", parent);

        cmd.CommandText = "INSERT IGNORE INTO Bookmarks (Name, Title, Tags, Article, Image, Parent) VALUES (@User, @Name, @Tags, @Article, @Image, @Parent); ";
        cmd.CommandText += _commandIDSuffix;
        Object objId = cmd.ExecuteScalar();
        return (int)(long)objId;
      }
    }

    public override void UpdateBookmark(Bookmark bookmark, SQLDatabase database)
    {
      // store to the database
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        string tags = ArticleItem.WriteTags(bookmark._tags);
        string article = (bookmark._article._database == database && bookmark._article._id >= 0) ? bookmark._article._id.ToString() : null;
        string parent = bookmark._parent >= 0 ? bookmark._parent.ToString() : null;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@Name", bookmark._name);
        AddParameter(ref cmd, "@Tags", tags);
        AddParameter(ref cmd, "@Article", article);
        AddParameter(ref cmd, "@Image", bookmark._image);
        AddParameter(ref cmd, "@Parent", parent);
        AddParameter(ref cmd, "@ID", bookmark._id);

        cmd.CommandText = "UPDATE Bookmarks SET Title = @Name, Tags = @Tags, Article = @Article, Image = @Image, Parent = @Parent WHERE (ID = @ID)";
        cmd.ExecuteNonQuery();
      }
    }

    public override void DeleteBookmark(int id, int parent)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@ID", id);
        cmd.CommandText = "DELETE FROM Bookmarks WHERE (ID = @ID)";
        cmd.ExecuteNonQuery();

        // update the children to not point to this bookmark
        string strParent = parent >= 0 ? parent.ToString() : null;
        AddParameter(ref cmd, "@Parent", strParent);
        cmd.CommandText = "UPDATE Bookmarks SET Parent = @Parent WHERE (Parent = @ID)";
        cmd.ExecuteNonQuery();
      }
    }

    public override void LoadScripts(ref List<Script> scripts)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@User", _info._user);

        cmd.CommandText = "SELECT UserName, `Usage`, Name, Image, Language, Script FROM Scripts WHERE (UserName = 'Default' OR UserName = @User)";
        using (Reader reader = cmd.ExecuteReader())
        {
          if (reader != null)
            while (reader.Read())
            {
              Script script = new Script();
              script._user = ReadString(reader[0]);
              script._usage = ReadString(reader[1]);
              script._name = ReadString(reader[2]);
              script._image = ReadString(reader[3]);
              script._language = ReadString(reader[4]);
              script._script = ReadString(reader[5]);
              scripts.Add(script);
            }
        }
      }
    }

    public override void SaveScripts(List<Script> scripts)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@User", _info._user);

        // remove the old rule sets
        cmd.CommandText = "DELETE FROM Scripts WHERE (UserName = @User)";
        cmd.ExecuteNonQuery();

        // insert the new rule sets
        string values = "";
        int index = 1;
        foreach (Script script in scripts)
        {
          if (!script._user.Equals(_info._user)) continue;
          if (values.Length > 0) values += ",";

          string paramUsage = string.Format("@Usage{0:D}", index);
          string paramName = string.Format("@Name{0:D}", index);
          string paramImage = string.Format("@Image{0:D}", index);
          string paramLanguage = string.Format("@Language{0:D}", index);
          string paramScript = string.Format("@Script{0:D}", index);
          index++;

          values += string.Format("(@User,{0},{1},{2},{3},{4})",
            paramUsage, paramName, paramImage, paramLanguage, paramScript);
          AddParameter(ref cmd, paramUsage, script._usage);
          AddParameter(ref cmd, paramName, script._name);
          AddParameter(ref cmd, paramImage, script._image);
          AddParameter(ref cmd, paramLanguage, script._language);
          AddParameter(ref cmd, paramScript, script._script);
        }
        if (values.Length > 0)
        {
          cmd.CommandText = "INSERT IGNORE INTO Scripts (UserName, `Usage`, Name, Image, Language, Script) VALUES " + values;
          cmd.ExecuteNonQuery();
        }
      }
    }

    public override void LoadArticles(ref ArticlesCache cache, System.ComponentModel.BackgroundWorker worker)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        // First, read all articles and store them in the cache
        Command cmd = _connection.CreateCommand();
        cmd.CommandText = "SELECT ID, Title, URL, Parent, Author, Date, Revision, MajorRevision FROM Articles";

        using (Reader reader = cmd.ExecuteReader())
        {
          if (reader != null)
            while (reader.Read())
            {
              ArticleItem item = new ArticleItem();
              item._id = ReadInt(reader[0], -1);
              item._title = ReadString(reader[1]);
              item.RawURL = ReadString(reader[2]);
              item._parent = ReadInt(reader[3], -1); ;
              item._author = ReadString(reader[4]);
              item._date = ReadDate(reader[5], DateTime.MinValue);
              item._revision = ReadInt(reader[6], -1);
              item._majorRevision = ReadInt(reader[7], -1);
              cache.Add(item._id, item);
            }
        }

        if (worker != null) worker.ReportProgress(40);

        // Add tags
        cmd.CommandText = "SELECT ID, Tag FROM Tags";
        using (Reader reader = cmd.ExecuteReader())
        {
          if (reader != null)
            while (reader.Read())
            {
              int id = ReadInt(reader[0], -1);
              string tag = ReadString(reader[1]).Trim();
              if (tag.Length > 0)
              {
                ArticleItem item;
                if (cache.TryGetValue(id, out item)) item.AddTag(tag.ToLower());
              }
            }
        }

        AddParameter(ref cmd, "@User", _info._user);

        // Add private tags
        cmd.CommandText = "SELECT ID, Tag FROM PrivateTags WHERE (Name = @User)";
        using (Reader reader = cmd.ExecuteReader())
        {
          if (reader != null)
            while (reader.Read())
            {
              int id = ReadInt(reader[0], -1);
              string tag = ReadString(reader[1]).Trim();
              if (tag.Length > 0)
              {
                ArticleItem item;
                if (cache.TryGetValue(id, out item)) item.AddTag(ArticleItem.ToPrivateTag(tag.ToLower()));
              }
            }
        }

        if (worker != null) worker.ReportProgress(60);

        // Mark articles read
        cmd.CommandText = "SELECT ID, Revision FROM MarkedRead WHERE (Name = @User)";
        using (Reader reader = cmd.ExecuteReader())
        {
          if (reader != null)
            while (reader.Read())
            {
              int id = ReadInt(reader[0], -1);
              int revision = ReadInt(reader[1], -1);
              ArticleItem item;
              if (cache.TryGetValue(id, out item))
              {
                item._toRead = revision < item._majorRevision;
                item._changed = revision < item._revision;
              }
            }
        }

        if (worker != null) worker.ReportProgress(80);

        // Collapse subtrees
        cmd.CommandText = "SELECT ID FROM Collapsed WHERE (Name = @User)";
        using (Reader reader = cmd.ExecuteReader())
        {
          if (reader != null)
            while (reader.Read())
            {
              int id = ReadInt(reader[0], -1);
              ArticleItem item;
              if (cache.TryGetValue(id, out item)) item._expanded = false;
            }
        }
      }
    }

    public override int SaveArticle(ArticleItem item)
    {
      int id = item._id;

      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return id;

        string parent = item._parent >= 0 ? item._parent.ToString() : null;

        // send to database, synchronize controls
        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@Title", item._title);
        AddParameter(ref cmd, "@URL", item.RawURL);
        AddParameter(ref cmd, "@Author", item._author);
        AddParameter(ref cmd, "@Date", item._date);
        AddParameter(ref cmd, "@Revision", item._revision);
        AddParameter(ref cmd, "@MajorRevision", item._majorRevision);
        AddParameter(ref cmd, "@Parent", parent);

        // different command for new article (INSERT) and edit article (UPDATE)            
        if (id < 0)
        {
          // insert a row to the Articles table, find the id of the new item
          cmd.CommandText = "INSERT INTO Articles (Title, URL, Author, Date, Revision, MajorRevision, Parent) VALUES (@Title, @URL, @Author, @Date, @Revision, @MajorRevision, @Parent); ";
          cmd.CommandText += _commandIDSuffix;

          Object objId = cmd.ExecuteScalar();
          id = (int)(long)objId;
        }
        else
        {
          AddParameter(ref cmd, "@ID", id);

          // update the row of the Articles table
          cmd.CommandText = "UPDATE Articles SET Title = @Title, URL = @URL, Author = @Author, Date = @Date, Revision = @Revision, MajorRevision = @MajorRevision, Parent = @Parent WHERE (ID = @ID)";
          cmd.ExecuteNonQuery();

          // new set of parameters
          cmd.Parameters.Clear();
          AddParameter(ref cmd, "@ID", id);

          // remove the read flag for all users
          if (item._revision <= 0)
          {
            // for SVN sources (revision > 0), keep the latest read revision
            cmd.CommandText = "DELETE FROM MarkedRead WHERE (ID = @ID)";
            cmd.ExecuteNonQuery();
          }

          // remove all old tags from Tags table
          cmd.CommandText = "DELETE FROM Tags WHERE (ID = @ID)";
          cmd.ExecuteNonQuery();
          // remove all my old private tags from PrivateTags table
          AddParameter(ref cmd, "@User", _info._user);
          cmd.CommandText = "DELETE FROM PrivateTags WHERE (ID = @ID AND Name = @User)";
          cmd.ExecuteNonQuery();
        }

        StoreTags(item, id, ref cmd);

        // mark article as read for the sender if it was marker read before
        if (!item._changed)
        {
          // new set of parameters
          cmd.Parameters.Clear();
          AddParameter(ref cmd, "@User", _info._user);
          AddParameter(ref cmd, "@ID", id);
          AddParameter(ref cmd, "@Revision", item._revision);

          cmd.CommandText = "REPLACE INTO MarkedRead (Name, ID, Revision) VALUES (@User, @ID, @Revision)";
          cmd.ExecuteNonQuery();
        }
      }

      return id;
    }
    // insert tags of the article to the database
    private void StoreTags(ArticleItem item, int id, ref Command cmd)
    {
      // new set of parameters
      cmd.Parameters.Clear();
      AddParameter(ref cmd, "@User", _info._user);

      // insert new tags into Tags / PrivateTags tables
      string tags = "";
      string privateTags = "";
      if (item._ownTags != null)
      {
        for (int i = 0; i < item._ownTags.Count; i++)
        {
          string tag = item._ownTags[i].Trim();
          string privateTag = ArticleItem.FromPrivateTag(tag);
          if (privateTag == null)
          {
            // global tag
            if (tag.Length > 0)
            {
              if (tags.Length > 0) tags += ",";

              string paramId = string.Format("@ID{0:D}", i);
              string paramTag = string.Format("@Tag{0:D}", i);
              tags += string.Format("({0},{1})", paramId, paramTag);
              AddParameter(ref cmd, paramId, id);
              AddParameter(ref cmd, paramTag, tag);
            }
          }
          else
          {
            // private tag
            if (privateTag.Length > 0)
            {
              if (privateTags.Length > 0) privateTags += ",";

              string paramId = string.Format("@ID{0:D}", i);
              string paramTag = string.Format("@Tag{0:D}", i);
              privateTags += string.Format("(@User,{0},{1})", paramId, paramTag);
              AddParameter(ref cmd, paramId, id);
              AddParameter(ref cmd, paramTag, privateTag);
            }
          }
        }
      }
      if (tags.Length > 0)
      {
        cmd.CommandText = "INSERT IGNORE INTO Tags (ID, Tag) VALUES " + tags;
        cmd.ExecuteNonQuery();
      }
      if (privateTags.Length > 0)
      {
        cmd.CommandText = "INSERT IGNORE INTO PrivateTags (Name, ID, Tag) VALUES " + privateTags;
        cmd.ExecuteNonQuery();
      }
    }

    public override void DeleteArticle(ArticleItem item)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@ID", item._id);

        // Delete the article itself
        cmd.CommandText = "DELETE FROM Articles WHERE (ID = @ID)";
        cmd.ExecuteNonQuery();

        // Delete records from helper tables
        cmd.CommandText = "DELETE FROM Tags WHERE (ID = @ID)";
        cmd.ExecuteNonQuery();
        cmd.CommandText = "DELETE FROM PrivateTags WHERE (ID = @ID)";
        cmd.ExecuteNonQuery();
        cmd.CommandText = "DELETE FROM MarkedRead WHERE (ID = @ID)";
        cmd.ExecuteNonQuery();
        cmd.CommandText = "DELETE FROM Collapsed WHERE (ID = @ID)";
        cmd.ExecuteNonQuery();

        // Redirect children to the new parent
        string strParent = item._parent >= 0 ? item._parent.ToString() : null;
        AddParameter(ref cmd, "@Parent", strParent);
        cmd.CommandText = "UPDATE Articles SET Parent = @Parent WHERE (Parent = @ID)";
        cmd.ExecuteNonQuery();
      }
    }

    public override int LoadArticleRead(int id)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return 0;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@User", _info._user);
        AddParameter(ref cmd, "@ID", id);

        cmd.CommandText = "SELECT Revision FROM MarkedRead WHERE (Name = @User AND ID = @ID)";
        object result = cmd.ExecuteScalar();
        if (result == null || result == DBNull.Value) return -1;
        return Convert.ToInt32(result);
      }
    }

    public override void SaveArticleRead(int id, int revision)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@User", _info._user);
        AddParameter(ref cmd, "@ID", id);
        AddParameter(ref cmd, "@Revision", revision);

        cmd.CommandText = "REPLACE INTO MarkedRead (Name, ID, Revision) VALUES (@User, @ID, @Revision)";
        cmd.ExecuteNonQuery();
      }
    }

    public override void DeleteArticleRead(int id)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@User", _info._user);
        AddParameter(ref cmd, "@ID", id);

        cmd.CommandText = "DELETE FROM MarkedRead WHERE (Name = @User AND ID = @ID)";
        cmd.ExecuteNonQuery();
      }
    }

    public override void SaveArticleCollapsed(int id)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@User", _info._user);
        AddParameter(ref cmd, "@ID", id);

        cmd.CommandText = "INSERT IGNORE INTO Collapsed (Name, ID) VALUES (@User, @ID)";
        cmd.ExecuteNonQuery();
      }
    }

    public override void DeleteArticleCollapsed(int id)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@User", _info._user);
        AddParameter(ref cmd, "@ID", id);

        cmd.CommandText = "DELETE FROM Collapsed WHERE (Name = @User AND ID = @ID)";
        cmd.ExecuteNonQuery();
      }
    }

    public override int LoadRevision(int id)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return int.MaxValue;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@ID", id);
        cmd.CommandText = "SELECT Revision FROM Articles WHERE ID = @ID";
        object result = cmd.ExecuteScalar();
        if (result != null && result != DBNull.Value) return Convert.ToInt32(result);
        else return int.MaxValue;
      }
    }

    public override void SaveRevisionAndDate(ArticleItem item)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();
        AddParameter(ref cmd, "@Revision", item._revision);
        AddParameter(ref cmd, "@MajorRevision", item._majorRevision);
        AddParameter(ref cmd, "@Date", item._date);
        AddParameter(ref cmd, "@ID", item._id);

        cmd.CommandText = "UPDATE Articles SET Revision = @Revision, MajorRevision = @MajorRevision, Date = @Date WHERE (ID = @ID)";
        cmd.ExecuteNonQuery();

        // mark article as read for the sender if it was marker read before
        if (!item._changed)
        {
          cmd.Parameters.Clear();
          AddParameter(ref cmd, "@User", _info._user);
          AddParameter(ref cmd, "@ID", item._id);
          AddParameter(ref cmd, "@Revision", item._revision);

          cmd.CommandText = "REPLACE INTO MarkedRead (Name, ID, Revision) VALUES (@User, @ID, @Revision)";
          cmd.ExecuteNonQuery();
        }
      }
    }

    public override void SaveParent(int id, int parent)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();

        string strParent = parent >= 0 ? parent.ToString() : null;
        AddParameter(ref cmd, "@Parent", strParent);
        AddParameter(ref cmd, "@ID", id);

        // update the row of the Articles table
        cmd.CommandText = "UPDATE Articles SET Parent = @Parent WHERE (ID = @ID)";
        cmd.ExecuteNonQuery();
      }
    }

    public override void UpdateTag(int id, string addTag, string removeTag)
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();

        if (removeTag != null)
        {
          // remove the tag
          string privateTag = ArticleItem.FromPrivateTag(removeTag);
          if (privateTag == null)
          {
            AddParameter(ref cmd, "@ID", id);
            AddParameter(ref cmd, "@Tag", removeTag);
            cmd.CommandText = "DELETE FROM Tags WHERE (ID = @ID AND Tag = @Tag)";
            cmd.ExecuteNonQuery();
          }
          else
          {
            AddParameter(ref cmd, "@User", _info._user);
            AddParameter(ref cmd, "@ID", id);
            AddParameter(ref cmd, "@Tag", privateTag);
            cmd.CommandText = "DELETE FROM PrivateTags WHERE (Name = @User AND ID = @ID AND Tag = @Tag)";
            cmd.ExecuteNonQuery();
          }
        }
        if (addTag != null)
        {
          // add the tag
          string privateTag = ArticleItem.FromPrivateTag(addTag);
          if (privateTag == null)
          {
            cmd.Parameters.Clear();
            AddParameter(ref cmd, "@ID", id);
            AddParameter(ref cmd, "@Tag", addTag);
            cmd.CommandText = "INSERT IGNORE INTO Tags (ID, Tag) VALUES (@ID,@Tag)";
            cmd.ExecuteNonQuery();
          }
          else
          {
            cmd.Parameters.Clear();
            AddParameter(ref cmd, "@User", _info._user);
            AddParameter(ref cmd, "@ID", id);
            AddParameter(ref cmd, "@Tag", privateTag);
            cmd.CommandText = "INSERT IGNORE INTO PrivateTags (Name, ID, Tag) VALUES (@User,@ID,@Tag)";
            cmd.ExecuteNonQuery();
          }
        }
      }
    }

/*
    public override void SaveTags(ArticlesCache cache)
    {
      // do not process private tags - used only for global tags optimization
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return;

        Command cmd = _connection.CreateCommand();

        // delete all tags
        cmd.CommandText = "DELETE FROM Tags";
        cmd.ExecuteNonQuery();

        // insert new tags
        foreach (ArticleItem item in cache.Values)
        {
          // insert new tags into Tags table
          string tags = "";
          cmd.Parameters.Clear();
          if (item._ownTags != null)
          {
            for (int i = 0; i < item._ownTags.Count; i++)
            {
              string tag = item._ownTags[i].Trim();
              if (tag.Length > 0)
              {
                if (tags.Length > 0) tags += ",";
                string paramId = string.Format("@ID{0}", i);
                string paramTag = string.Format("@Tag{0}", i);
                tags += string.Format("({0},{1})", paramId, paramTag);
                AddParameter(ref cmd, paramId, item._id);
                AddParameter(ref cmd, paramTag, tag);
              }
            }
          }
          if (tags.Length > 0)
          {
            cmd.CommandText = "INSERT IGNORE INTO Tags (ID, Tag) VALUES " + tags;
            cmd.ExecuteNonQuery();
          }
        }
      }
    }
*/
    public override string GetArticleRevision(int id, int revision)
    {
      // not supported for this engine
      return "";
    }

    public override bool IsFullTextIndexLocal()
    {
      return true;
    }
    public override string CheckFullTextIndex()
    {
      return _fullText.CheckIndex();
    }
    public override List<int> FullTextRevisionsOfArticle(ArticleItem item)
    {
      return _fullText.RevisionsOfArticle(item);
    }
    public override bool FullTextNeedToIndexArticle(ArticleItem item, RevisionInfo revision)
    {
      return _fullText.NeedToIndexArticle(item, revision);
    }
    public override void FullTextIndexArticle(ArticleItem item, RevisionInfo revision, string content)
    {
      _fullText.IndexArticle(item, revision, content);
    }
    public override List<SearchResult> FullTextSearch(SQLDatabase database, string query)
    {
      return _fullText.Search(database, query);
    }

    public override double Benchmark()
    {
      using (OpenConnection())
      {
        if (_mode == DbMode.Disconnected) return -1;

        Command cmd = _connection.CreateCommand();

        // Simple SQL query
        DateTime now = DateTime.Now;
        cmd.CommandText = "SELECT Revision FROM Articles ORDER BY RAND() LIMIT 1";
        object result = cmd.ExecuteScalar();
        var duration = DateTime.Now - now;
        return duration.TotalMilliseconds;
      }
    }

    // helpers to create parameters of command
    private static void AddParameter(ref Command cmd, string name, string value)
    {
      Parameter param = cmd.CreateParameter();
      param.ParameterName = name;
      param.DbType = DbType.String;
      param.Value = value;
      cmd.Parameters.Add(param);
    }
    private static void AddParameter(ref Command cmd, string name, int value)
    {
      Parameter param = cmd.CreateParameter();
      param.ParameterName = name;
      param.DbType = DbType.Int32;
      param.Value = value;
      cmd.Parameters.Add(param);
    }
    private static void AddParameter(ref Command cmd, string name, DateTime value)
    {
      Parameter param = cmd.CreateParameter();
      param.ParameterName = name;
      param.DbType = DbType.DateTime;
      param.Value = value;
      cmd.Parameters.Add(param);
    }
    // helpers to read results from reader
    private static int ReadInt(Object obj, int defValue)
    {
      if (obj == DBNull.Value) return defValue;
      return Convert.ToInt32(obj);
    }
    private static string ReadString(Object obj)
    {
      if (obj == DBNull.Value) return "";
      return Convert.ToString(obj);
    }
    private static DateTime ReadDate(Object obj, DateTime defValue)
    {
      if (obj == DBNull.Value) return defValue;
      return Convert.ToDateTime(obj);
    }

    /// helper to keep connection opened during set of operations, avoid using connection by more threads simultaneously
    private class OpenedConnection : IDisposable
    {
      private Connection _connection;

      public OpenedConnection(Connection connection)
      {
        _connection = connection;

        if (_connection != null)
        {
          Monitor.Enter(_connection);
          try
          {
            _connection.Open();
          }
          catch (System.Exception exception)
          {
            Console.Write(exception.ToString());


            Monitor.Exit(_connection);
            _connection = null;
          }
        }
      }
      ~OpenedConnection()
      {
        Dispose();
      }
      // IDisposable implementation
      public void Dispose()
      {
        if (_connection != null)
        {
          _connection.Close();
          Monitor.Exit(_connection);
          _connection = null;
        }
      }
      // Check if connection was opened successfully
      public bool IsValid()
      {
        return _connection != null;
      }
    }
    private OpenedConnection OpenConnection()
    {
      if (_mode != DbMode.Online) return null;

      DialogResult result;
      do
      {
        // try to connect for some time
        DateTime until = DateTime.Now.AddSeconds(5);
        do
        {
          OpenedConnection opened = new OpenedConnection(_connection);
          if (opened.IsValid()) return opened;
        } while (DateTime.Now < until);

        // cannot connect to database
        result = MessageBox.Show(
          string.Format("Cannot connect to database '{0}'.\n\nTODO: work offline", _info.ToString()), "Colabo",
          MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning
          );
      } while (result == DialogResult.Retry);

      _mode = DbMode.Disconnected;
      return null;
    }
    /*
        static private OpenedConnection OpenConnection(ref Connection connection)
        {
          // try to connect for some time
          DateTime until = DateTime.Now.AddSeconds(5);
          do
          {
            OpenedConnection opened = new OpenedConnection(connection);
            if (opened.IsValid()) return opened;
          } while (DateTime.Now < until);

          connection = null;
          return null;
        }
    */
    // check if connection to database is established, if not, ask whether to switch to off-line mode
    /*
    private bool GoOffline()
    {
      if (_connection != null && _connection.Ping()) return false;
      DialogResult result = MessageBox.Show(
        "Connection to database failed. Do you want to switch to the off-line mode? (otherwise the application will be terminated)",
        "Colabo", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
      if (result != DialogResult.Yes) Application.Exit();
      return true;
    }
    */
  }
}