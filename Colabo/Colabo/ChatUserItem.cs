﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Colabo
{
  internal class ChatUserItem : ListViewItem
  {
    private string _ipAddr;

    public string IPAddress { get { return _ipAddr; } }

    private static int GetStateImageIndex(string state)
    {
      // ENUM('Ready','Type','Close')
      switch (state)
      {
        case "Offline": return 1;
        case "Online": return 2;
        case "Away": return 3;
        case "DND": return 4;
        case "Busy": return 5;
        case "Invisible": return 6;
        case "N/A": return 1;
      }
      return 0;
    }

    public ChatUserItem(string name, UserInfo userInfo)
      : base(name)
    {
      Name = name;
      string[] row = CreatePresenceRow(userInfo);
      SubItems.AddRange(row);
      StateImageIndex = GetStateImageIndex(userInfo._presence);
      _ipAddr = userInfo._ipAddr;
    }

    internal void SetDetails(UserInfo userInfo)
    {
      string[] row = CreatePresenceRow(userInfo);
      _ipAddr = userInfo._ipAddr;
      for (int i = 1; i < SubItems.Count; i++)
      {
        SubItems[i].Text = row[i - 1];
      }
      StateImageIndex = GetStateImageIndex(userInfo._presence);
    }

    private static string[] CreatePresenceRow(UserInfo userInfo)
    {
      String lastSeen = "";
      String state = userInfo._presence;
      if (state == "Invisible") state = "Offline";
      if ((state == "Offline" || state == "Away" || state == "N/A") && userInfo._presenceTime != DateTime.MinValue)
      {
        TimeSpan ago = DateTime.Now - userInfo._presenceTime;
        lastSeen = FormatTimeSpan(ago, userInfo._presenceTime);
      }
#if DEBUG
      string[] row = { state, lastSeen, userInfo._location, userInfo._build };
#else
      string[] row = { state, lastSeen, userInfo._location };
#endif
      return row;
    }

    public static string FormatTimeSpan(TimeSpan ago, DateTime time)
    {
      DateTime timeAgo = time - ago;
      double daysAgo = (time.Date-timeAgo.Date).TotalDays;
      if (ago.Ticks > 2 * TimeSpan.TicksPerDay && daysAgo>=1.0)
      {
        return time.ToShortDateString();
      }
      else if (ago.Ticks > 1 * TimeSpan.TicksPerDay && daysAgo>0)
      {
        return String.Format("yesterday, at {0}", time.ToShortTimeString());
      }
      else if (ago.Ticks >= 2 * TimeSpan.TicksPerHour)
      {
        return String.Format("{0:f0} hours ago", ago.TotalHours);
      }
      else if (ago.Ticks >= 1 * TimeSpan.TicksPerHour)
      {
        return String.Format("1 hour ago", ago.TotalHours);
      }
      else if (ago.Ticks >= 5 * TimeSpan.TicksPerMinute)
      {
        return String.Format("{0:f0} minutes ago", (int)ago.TotalMinutes / 5 * 5);
      }
      else
      {
        return "now";
      }
    }

  
  }

}
