namespace Colabo
{
    partial class DialogArticle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogArticle));
      this.toolStrip1 = new System.Windows.Forms.ToolStrip();
      this.ButtonSend = new System.Windows.Forms.ToolStripButton();
      this.buttonSave = new System.Windows.Forms.ToolStripButton();
      this.buttonDiff = new System.Windows.Forms.ToolStripButton();
      this.buttonDebug = new System.Windows.Forms.ToolStripButton();
      this.buttonEdit = new System.Windows.Forms.ToolStripButton();
      this.buttonSendChat = new System.Windows.Forms.ToolStripButton();
      this.buttonStartChat = new System.Windows.Forms.ToolStripButton();
      this.labelTitle = new System.Windows.Forms.Label();
      this.valueTitle = new System.Windows.Forms.TextBox();
      this.valueURL = new System.Windows.Forms.TextBox();
      this.labelURL = new System.Windows.Forms.Label();
      this.valueTags = new Colabo.DialogArticle.TextBoxWithAutocomplete();
      this.chatUsersMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.removeChatMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.labelTags = new System.Windows.Forms.Label();
      this.valueContent = new Colabo.ContentTextBox();
      this.previewTags = new System.Windows.Forms.Label();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.buttonHeaders = new System.Windows.Forms.PictureBox();
      this.groupChat = new System.Windows.Forms.GroupBox();
      this.chatUsers = new System.Windows.Forms.ListView();
      this.chatUser = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.chatUserState = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.buttonAddUser = new System.Windows.Forms.Button();
      this.groupHeaders = new System.Windows.Forms.GroupBox();
      this.panelArticleDetails = new System.Windows.Forms.Panel();
      this.debugChanges = new System.Windows.Forms.Label();
      this.minorEdit = new System.Windows.Forms.CheckBox();
      this.label1 = new System.Windows.Forms.Label();
      this.valueDatabase = new System.Windows.Forms.ComboBox();
      this.panelAttachments = new System.Windows.Forms.Panel();
      this.attachmentsList = new Colabo.DialogArticle.ListBoxWithDelete();
      this.label2 = new System.Windows.Forms.Label();
      this.preview = new System.Windows.Forms.WebBrowser();
      this.timer = new System.Windows.Forms.Timer(this.components);
      this.toolStrip1.SuspendLayout();
      this.chatUsersMenu.SuspendLayout();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.buttonHeaders)).BeginInit();
      this.groupChat.SuspendLayout();
      this.groupHeaders.SuspendLayout();
      this.panelArticleDetails.SuspendLayout();
      this.panelAttachments.SuspendLayout();
      this.SuspendLayout();
      // 
      // toolStrip1
      // 
      this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ButtonSend,
            this.buttonSave,
            this.buttonDiff,
            this.buttonDebug,
            this.buttonEdit,
            this.buttonSendChat,
            this.buttonStartChat});
      this.toolStrip1.Location = new System.Drawing.Point(0, 0);
      this.toolStrip1.Name = "toolStrip1";
      this.toolStrip1.Size = new System.Drawing.Size(675, 25);
      this.toolStrip1.TabIndex = 0;
      this.toolStrip1.Text = "toolStrip1";
      // 
      // ButtonSend
      // 
      this.ButtonSend.Image = global::Colabo.Properties.Resources.Send;
      this.ButtonSend.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.ButtonSend.Name = "ButtonSend";
      this.ButtonSend.Size = new System.Drawing.Size(53, 22);
      this.ButtonSend.Text = "Send";
      this.ButtonSend.Click += new System.EventHandler(this.ButtonSend_Click);
      // 
      // buttonSave
      // 
      this.buttonSave.Image = global::Colabo.Properties.Resources.Save;
      this.buttonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.buttonSave.Name = "buttonSave";
      this.buttonSave.Size = new System.Drawing.Size(51, 22);
      this.buttonSave.Text = "Save";
      this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
      // 
      // buttonDiff
      // 
      this.buttonDiff.Image = global::Colabo.Properties.Resources.CompareVersions;
      this.buttonDiff.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.buttonDiff.Name = "buttonDiff";
      this.buttonDiff.Size = new System.Drawing.Size(46, 22);
      this.buttonDiff.Text = "Diff";
      this.buttonDiff.ToolTipText = "Difference";
      this.buttonDiff.Click += new System.EventHandler(this.buttonDiff_Click);
      // 
      // buttonDebug
      // 
      this.buttonDebug.Image = global::Colabo.Properties.Resources.Pause;
      this.buttonDebug.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.buttonDebug.Name = "buttonDebug";
      this.buttonDebug.Size = new System.Drawing.Size(95, 22);
      this.buttonDebug.Text = "Debug Script";
      this.buttonDebug.Click += new System.EventHandler(this.buttonDebug_Click);
      // 
      // buttonEdit
      // 
      this.buttonEdit.Image = global::Colabo.Properties.Resources.EditTableHS;
      this.buttonEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.buttonEdit.Name = "buttonEdit";
      this.buttonEdit.Size = new System.Drawing.Size(47, 22);
      this.buttonEdit.Text = "Edit";
      this.buttonEdit.ToolTipText = "Edit conversation";
      this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
      // 
      // buttonSendChat
      // 
      this.buttonSendChat.Image = global::Colabo.Properties.Resources.SendInstantMessage_32x32;
      this.buttonSendChat.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.buttonSendChat.Name = "buttonSendChat";
      this.buttonSendChat.Size = new System.Drawing.Size(52, 22);
      this.buttonSendChat.Text = "Chat";
      this.buttonSendChat.Click += new System.EventHandler(this.buttonSendChat_Click);
      // 
      // buttonStartChat
      // 
      this.buttonStartChat.Image = global::Colabo.Properties.Resources.SendInstantMessage_32x32;
      this.buttonStartChat.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.buttonStartChat.Name = "buttonStartChat";
      this.buttonStartChat.Size = new System.Drawing.Size(77, 22);
      this.buttonStartChat.Text = "Start chat";
      this.buttonStartChat.Click += new System.EventHandler(this.buttonStartChat_Click);
      // 
      // labelTitle
      // 
      this.labelTitle.AutoSize = true;
      this.labelTitle.Location = new System.Drawing.Point(8, 32);
      this.labelTitle.Name = "labelTitle";
      this.labelTitle.Size = new System.Drawing.Size(30, 13);
      this.labelTitle.TabIndex = 1;
      this.labelTitle.Text = "Title:";
      // 
      // valueTitle
      // 
      this.valueTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueTitle.Location = new System.Drawing.Point(44, 32);
      this.valueTitle.Name = "valueTitle";
      this.valueTitle.Size = new System.Drawing.Size(381, 20);
      this.valueTitle.TabIndex = 2;
      this.valueTitle.TextChanged += new System.EventHandler(this.valueTitle_TextChanged);
      // 
      // valueURL
      // 
      this.valueURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueURL.Location = new System.Drawing.Point(44, 58);
      this.valueURL.Name = "valueURL";
      this.valueURL.Size = new System.Drawing.Size(381, 20);
      this.valueURL.TabIndex = 4;
      this.valueURL.TextChanged += new System.EventHandler(this.valueURL_TextChanged);
      this.valueURL.Leave += new System.EventHandler(this.valueURL_Leave);
      // 
      // labelURL
      // 
      this.labelURL.AutoSize = true;
      this.labelURL.Location = new System.Drawing.Point(8, 58);
      this.labelURL.Name = "labelURL";
      this.labelURL.Size = new System.Drawing.Size(32, 13);
      this.labelURL.TabIndex = 3;
      this.labelURL.Text = "URL:";
      // 
      // valueTags
      // 
      this.valueTags.AcceptsReturn = true;
      this.valueTags.AcceptsTab = true;
      this.valueTags.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueTags.Location = new System.Drawing.Point(44, 99);
      this.valueTags.Multiline = true;
      this.valueTags.Name = "valueTags";
      this.valueTags.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.valueTags.Size = new System.Drawing.Size(381, 35);
      this.valueTags.TabIndex = 6;
      this.valueTags.TextChanged += new System.EventHandler(this.valueTags_TextChanged);
      // 
      // chatUsersMenu
      // 
      this.chatUsersMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeChatMenuItem});
      this.chatUsersMenu.Name = "chatUsersMenu";
      this.chatUsersMenu.Size = new System.Drawing.Size(118, 26);
      // 
      // removeChatMenuItem
      // 
      this.removeChatMenuItem.Name = "removeChatMenuItem";
      this.removeChatMenuItem.Size = new System.Drawing.Size(117, 22);
      this.removeChatMenuItem.Text = "Remove";
      this.removeChatMenuItem.Click += new System.EventHandler(this.removeChatMenuItem_Click);
      // 
      // labelTags
      // 
      this.labelTags.AutoSize = true;
      this.labelTags.Location = new System.Drawing.Point(8, 83);
      this.labelTags.Name = "labelTags";
      this.labelTags.Size = new System.Drawing.Size(34, 13);
      this.labelTags.TabIndex = 5;
      this.labelTags.Text = "Tags:";
      // 
      // valueContent
      // 
      this.valueContent.AcceptsReturn = true;
      this.valueContent.AcceptsTab = true;
      this.valueContent.AllowDrop = true;
      this.valueContent.Dock = System.Windows.Forms.DockStyle.Fill;
      this.valueContent.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.valueContent.Location = new System.Drawing.Point(0, 0);
      this.valueContent.Multiline = true;
      this.valueContent.Name = "valueContent";
      this.valueContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.valueContent.Size = new System.Drawing.Size(671, 219);
      this.valueContent.TabIndex = 7;
      this.valueContent.TextChanged += new System.EventHandler(this.valueContent_TextChanged);
      this.valueContent.DragDrop += new System.Windows.Forms.DragEventHandler(this.valueContent_DragDrop);
      this.valueContent.DragEnter += new System.Windows.Forms.DragEventHandler(this.valueContent_DragEnter);
      // 
      // previewTags
      // 
      this.previewTags.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.previewTags.Location = new System.Drawing.Point(41, 77);
      this.previewTags.Name = "previewTags";
      this.previewTags.Size = new System.Drawing.Size(326, 19);
      this.previewTags.TabIndex = 8;
      this.previewTags.Text = "label1";
      this.previewTags.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // splitContainer1
      // 
      this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.buttonHeaders);
      this.splitContainer1.Panel1.Controls.Add(this.groupChat);
      this.splitContainer1.Panel1.Controls.Add(this.groupHeaders);
      this.splitContainer1.Panel1.Controls.Add(this.preview);
      this.splitContainer1.Panel1MinSize = 170;
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.valueContent);
      this.splitContainer1.Size = new System.Drawing.Size(675, 658);
      this.splitContainer1.SplitterDistance = 431;
      this.splitContainer1.TabIndex = 10;
      // 
      // buttonHeaders
      // 
      this.buttonHeaders.Image = global::Colabo.Properties.Resources.collapse1;
      this.buttonHeaders.InitialImage = global::Colabo.Properties.Resources.expander_16xLG;
      this.buttonHeaders.Location = new System.Drawing.Point(5, 26);
      this.buttonHeaders.Name = "buttonHeaders";
      this.buttonHeaders.Size = new System.Drawing.Size(16, 16);
      this.buttonHeaders.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.buttonHeaders.TabIndex = 16;
      this.buttonHeaders.TabStop = false;
      this.buttonHeaders.Click += new System.EventHandler(this.buttonHeaders_Click);
      // 
      // groupChat
      // 
      this.groupChat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.groupChat.Controls.Add(this.chatUsers);
      this.groupChat.Controls.Add(this.buttonAddUser);
      this.groupChat.Location = new System.Drawing.Point(27, 188);
      this.groupChat.Name = "groupChat";
      this.groupChat.Size = new System.Drawing.Size(641, 56);
      this.groupChat.TabIndex = 5;
      this.groupChat.TabStop = false;
      this.groupChat.Text = "Chat";
      // 
      // chatUsers
      // 
      this.chatUsers.AllowDrop = true;
      this.chatUsers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.chatUsers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chatUser,
            this.chatUserState});
      this.chatUsers.ContextMenuStrip = this.chatUsersMenu;
      this.chatUsers.Location = new System.Drawing.Point(6, 19);
      this.chatUsers.Name = "chatUsers";
      this.chatUsers.Size = new System.Drawing.Size(487, 31);
      this.chatUsers.TabIndex = 3;
      this.chatUsers.UseCompatibleStateImageBehavior = false;
      this.chatUsers.View = System.Windows.Forms.View.SmallIcon;
      this.chatUsers.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.chatUsers_ItemDrag);
      this.chatUsers.DragDrop += new System.Windows.Forms.DragEventHandler(this.chatUsers_DragDrop);
      this.chatUsers.DragEnter += new System.Windows.Forms.DragEventHandler(this.chatUsers_DragEnter);
      // 
      // chatUser
      // 
      this.chatUser.Text = "User";
      // 
      // chatUserState
      // 
      this.chatUserState.Text = "State";
      // 
      // buttonAddUser
      // 
      this.buttonAddUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonAddUser.Image = global::Colabo.Properties.Resources.UserProfilenode_8706;
      this.buttonAddUser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.buttonAddUser.Location = new System.Drawing.Point(514, 19);
      this.buttonAddUser.Name = "buttonAddUser";
      this.buttonAddUser.Size = new System.Drawing.Size(104, 24);
      this.buttonAddUser.TabIndex = 4;
      this.buttonAddUser.Text = "Add User...";
      this.buttonAddUser.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
      this.buttonAddUser.UseVisualStyleBackColor = true;
      this.buttonAddUser.Click += new System.EventHandler(this.buttonAddUser_Click);
      // 
      // groupHeaders
      // 
      this.groupHeaders.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.groupHeaders.Controls.Add(this.panelArticleDetails);
      this.groupHeaders.Controls.Add(this.panelAttachments);
      this.groupHeaders.Location = new System.Drawing.Point(27, 26);
      this.groupHeaders.Name = "groupHeaders";
      this.groupHeaders.Size = new System.Drawing.Size(644, 159);
      this.groupHeaders.TabIndex = 17;
      this.groupHeaders.TabStop = false;
      this.groupHeaders.Text = "Headers";
      // 
      // panelArticleDetails
      // 
      this.panelArticleDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.panelArticleDetails.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
      this.panelArticleDetails.Controls.Add(this.debugChanges);
      this.panelArticleDetails.Controls.Add(this.minorEdit);
      this.panelArticleDetails.Controls.Add(this.label1);
      this.panelArticleDetails.Controls.Add(this.valueDatabase);
      this.panelArticleDetails.Controls.Add(this.previewTags);
      this.panelArticleDetails.Controls.Add(this.labelTitle);
      this.panelArticleDetails.Controls.Add(this.valueTags);
      this.panelArticleDetails.Controls.Add(this.valueTitle);
      this.panelArticleDetails.Controls.Add(this.labelTags);
      this.panelArticleDetails.Controls.Add(this.labelURL);
      this.panelArticleDetails.Controls.Add(this.valueURL);
      this.panelArticleDetails.Location = new System.Drawing.Point(6, 15);
      this.panelArticleDetails.Name = "panelArticleDetails";
      this.panelArticleDetails.Size = new System.Drawing.Size(439, 141);
      this.panelArticleDetails.TabIndex = 13;
      // 
      // debugChanges
      // 
      this.debugChanges.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.debugChanges.Location = new System.Drawing.Point(335, 17);
      this.debugChanges.Name = "debugChanges";
      this.debugChanges.Size = new System.Drawing.Size(90, 12);
      this.debugChanges.TabIndex = 12;
      this.debugChanges.Text = "nnnn changes";
      // 
      // minorEdit
      // 
      this.minorEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.minorEdit.AutoSize = true;
      this.minorEdit.Location = new System.Drawing.Point(312, 3);
      this.minorEdit.Name = "minorEdit";
      this.minorEdit.Size = new System.Drawing.Size(113, 17);
      this.minorEdit.TabIndex = 11;
      this.minorEdit.Text = "This is a minor edit";
      this.minorEdit.UseVisualStyleBackColor = true;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(8, 4);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(56, 13);
      this.label1.TabIndex = 10;
      this.label1.Text = "Database:";
      // 
      // valueDatabase
      // 
      this.valueDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.valueDatabase.FormattingEnabled = true;
      this.valueDatabase.Location = new System.Drawing.Point(77, 1);
      this.valueDatabase.Name = "valueDatabase";
      this.valueDatabase.Size = new System.Drawing.Size(226, 21);
      this.valueDatabase.TabIndex = 9;
      this.valueDatabase.SelectedIndexChanged += new System.EventHandler(this.valueDatabase_SelectedIndexChanged);
      // 
      // panelAttachments
      // 
      this.panelAttachments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.panelAttachments.Controls.Add(this.attachmentsList);
      this.panelAttachments.Controls.Add(this.label2);
      this.panelAttachments.Location = new System.Drawing.Point(446, 14);
      this.panelAttachments.Name = "panelAttachments";
      this.panelAttachments.Size = new System.Drawing.Size(198, 141);
      this.panelAttachments.TabIndex = 14;
      // 
      // attachmentsList
      // 
      this.attachmentsList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.attachmentsList.FormattingEnabled = true;
      this.attachmentsList.HorizontalScrollbar = true;
      this.attachmentsList.Location = new System.Drawing.Point(5, 17);
      this.attachmentsList.Name = "attachmentsList";
      this.attachmentsList.Size = new System.Drawing.Size(190, 121);
      this.attachmentsList.TabIndex = 1;
      this.attachmentsList.DoubleClick += new System.EventHandler(this.attachmentsList_DoubleClick);
      // 
      // label2
      // 
      this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.label2.Location = new System.Drawing.Point(5, 4);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(66, 13);
      this.label2.TabIndex = 0;
      this.label2.Text = "Attachments";
      // 
      // preview
      // 
      this.preview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.preview.Location = new System.Drawing.Point(0, 250);
      this.preview.MinimumSize = new System.Drawing.Size(20, 20);
      this.preview.Name = "preview";
      this.preview.ScriptErrorsSuppressed = true;
      this.preview.Size = new System.Drawing.Size(673, 179);
      this.preview.TabIndex = 9;
      this.preview.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.preview_DocumentCompleted);
      this.preview.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.preview_Navigating);
      // 
      // timer
      // 
      this.timer.Enabled = true;
      this.timer.Interval = 60000;
      this.timer.Tick += new System.EventHandler(this.timer_Tick);
      // 
      // DialogArticle
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(675, 658);
      this.Controls.Add(this.toolStrip1);
      this.Controls.Add(this.splitContainer1);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MinimumSize = new System.Drawing.Size(300, 300);
      this.Name = "DialogArticle";
      this.Text = "Send new message";
      this.Activated += new System.EventHandler(this.DialogArticle_Activated);
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DialogArticle_FormClosing);
      this.Load += new System.EventHandler(this.DialogArticle_Load);
      this.ClientSizeChanged += new System.EventHandler(this.DialogArticle_ClientSizeChanged);
      this.toolStrip1.ResumeLayout(false);
      this.toolStrip1.PerformLayout();
      this.chatUsersMenu.ResumeLayout(false);
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel1.PerformLayout();
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.Panel2.PerformLayout();
      this.splitContainer1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.buttonHeaders)).EndInit();
      this.groupChat.ResumeLayout(false);
      this.groupHeaders.ResumeLayout(false);
      this.panelArticleDetails.ResumeLayout(false);
      this.panelArticleDetails.PerformLayout();
      this.panelAttachments.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton ButtonSend;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.TextBox valueTitle;
        private System.Windows.Forms.TextBox valueURL;
        private System.Windows.Forms.Label labelURL;
        private TextBoxWithAutocomplete valueTags;
        private System.Windows.Forms.Label labelTags;
      private Colabo.ContentTextBox valueContent;
      private System.Windows.Forms.Label previewTags;
      private System.Windows.Forms.SplitContainer splitContainer1;
      private System.Windows.Forms.ComboBox valueDatabase;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Timer timer;
      private System.Windows.Forms.ToolStripButton buttonSave;
      private System.Windows.Forms.CheckBox minorEdit;
      private System.Windows.Forms.Label debugChanges;
      private System.Windows.Forms.ToolStripButton buttonDebug;
      private System.Windows.Forms.ToolStripButton buttonDiff;
      private System.Windows.Forms.Panel panelArticleDetails;
      private System.Windows.Forms.Panel panelAttachments;
      private ListBoxWithDelete attachmentsList;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.WebBrowser preview;
      private System.Windows.Forms.ToolStripButton buttonEdit;
      private System.Windows.Forms.ToolStripButton buttonSendChat;
      private System.Windows.Forms.ListView chatUsers;
      private System.Windows.Forms.ColumnHeader chatUser;
      private System.Windows.Forms.ColumnHeader chatUserState;
      private System.Windows.Forms.GroupBox groupHeaders;
      private System.Windows.Forms.Button buttonAddUser;
      private System.Windows.Forms.GroupBox groupChat;
      private System.Windows.Forms.PictureBox buttonHeaders;
      private System.Windows.Forms.ToolStripButton buttonStartChat;
      private System.Windows.Forms.ContextMenuStrip chatUsersMenu;
      private System.Windows.Forms.ToolStripMenuItem removeChatMenuItem;
    }
}