namespace Colabo
{
  partial class DialogConfiguration
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.SaveButton = new System.Windows.Forms.Button();
      this.AddRuleSetButton = new System.Windows.Forms.Button();
      this.EditRuleSetButton = new System.Windows.Forms.Button();
      this.DeleteRuleSetButton = new System.Windows.Forms.Button();
      this.DeletePasswordButton = new System.Windows.Forms.Button();
      this.passwordsComboBox = new System.Windows.Forms.ComboBox();
      this.pages = new System.Windows.Forms.TabControl();
      this.tabPage4 = new System.Windows.Forms.TabPage();
      this.label9 = new System.Windows.Forms.Label();
      this.buttonColor = new System.Windows.Forms.Button();
      this.valueColor = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.valuePicture = new System.Windows.Forms.TextBox();
      this.label7 = new System.Windows.Forms.Label();
      this.valueFullName = new System.Windows.Forms.TextBox();
      this.ldapPort = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.ldapHost = new System.Windows.Forms.TextBox();
      this.ldapConnect = new System.Windows.Forms.Button();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.ldapPassword = new System.Windows.Forms.TextBox();
      this.ldapUser = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.useLDAP = new System.Windows.Forms.CheckBox();
      this.tabPage1 = new System.Windows.Forms.TabPage();
      this.manageDatabaseButton = new System.Windows.Forms.Button();
      this.databasesList = new System.Windows.Forms.ListBox();
      this.addDatabaseButton = new System.Windows.Forms.Button();
      this.editDatabaseButton = new System.Windows.Forms.Button();
      this.deleteDatabaseButton = new System.Windows.Forms.Button();
      this.tabPage2 = new System.Windows.Forms.TabPage();
      this.ruleSetsList = new System.Windows.Forms.ListBox();
      this.tabPage5 = new System.Windows.Forms.TabPage();
      this.scriptsList = new System.Windows.Forms.ListBox();
      this.DeleteScriptButton = new System.Windows.Forms.Button();
      this.AddScriptButton = new System.Windows.Forms.Button();
      this.EditScriptButton = new System.Windows.Forms.Button();
      this.tabPage3 = new System.Windows.Forms.TabPage();
      this.colorDialog = new System.Windows.Forms.ColorDialog();
      this.pages.SuspendLayout();
      this.tabPage4.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.ldapPort)).BeginInit();
      this.tabPage1.SuspendLayout();
      this.tabPage2.SuspendLayout();
      this.tabPage5.SuspendLayout();
      this.tabPage3.SuspendLayout();
      this.SuspendLayout();
      // 
      // SaveButton
      // 
      this.SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.SaveButton.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.SaveButton.Location = new System.Drawing.Point(321, 298);
      this.SaveButton.Name = "SaveButton";
      this.SaveButton.Size = new System.Drawing.Size(64, 24);
      this.SaveButton.TabIndex = 5;
      this.SaveButton.Text = "Save";
      this.SaveButton.UseVisualStyleBackColor = true;
      this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
      // 
      // AddRuleSetButton
      // 
      this.AddRuleSetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.AddRuleSetButton.Location = new System.Drawing.Point(151, 225);
      this.AddRuleSetButton.Name = "AddRuleSetButton";
      this.AddRuleSetButton.Size = new System.Drawing.Size(64, 23);
      this.AddRuleSetButton.TabIndex = 8;
      this.AddRuleSetButton.Text = "Add";
      this.AddRuleSetButton.UseVisualStyleBackColor = true;
      this.AddRuleSetButton.Click += new System.EventHandler(this.AddRuleSetButton_Click);
      // 
      // EditRuleSetButton
      // 
      this.EditRuleSetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.EditRuleSetButton.Location = new System.Drawing.Point(221, 225);
      this.EditRuleSetButton.Name = "EditRuleSetButton";
      this.EditRuleSetButton.Size = new System.Drawing.Size(64, 23);
      this.EditRuleSetButton.TabIndex = 9;
      this.EditRuleSetButton.Text = "Edit";
      this.EditRuleSetButton.UseVisualStyleBackColor = true;
      this.EditRuleSetButton.Click += new System.EventHandler(this.EditRuleSetButton_Click);
      // 
      // DeleteRuleSetButton
      // 
      this.DeleteRuleSetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.DeleteRuleSetButton.Location = new System.Drawing.Point(291, 225);
      this.DeleteRuleSetButton.Name = "DeleteRuleSetButton";
      this.DeleteRuleSetButton.Size = new System.Drawing.Size(64, 23);
      this.DeleteRuleSetButton.TabIndex = 10;
      this.DeleteRuleSetButton.Text = "Delete";
      this.DeleteRuleSetButton.UseVisualStyleBackColor = true;
      this.DeleteRuleSetButton.Click += new System.EventHandler(this.DeleteRuleSetButton_Click);
      // 
      // DeletePasswordButton
      // 
      this.DeletePasswordButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.DeletePasswordButton.Location = new System.Drawing.Point(291, 225);
      this.DeletePasswordButton.Name = "DeletePasswordButton";
      this.DeletePasswordButton.Size = new System.Drawing.Size(64, 23);
      this.DeletePasswordButton.TabIndex = 11;
      this.DeletePasswordButton.Text = "Delete";
      this.DeletePasswordButton.UseVisualStyleBackColor = true;
      this.DeletePasswordButton.Click += new System.EventHandler(this.DeletePasswordButton_Click);
      // 
      // passwordsComboBox
      // 
      this.passwordsComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.passwordsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.passwordsComboBox.FormattingEnabled = true;
      this.passwordsComboBox.Location = new System.Drawing.Point(0, 6);
      this.passwordsComboBox.Name = "passwordsComboBox";
      this.passwordsComboBox.Size = new System.Drawing.Size(355, 21);
      this.passwordsComboBox.TabIndex = 0;
      this.passwordsComboBox.SelectedIndexChanged += new System.EventHandler(this.passwordsComboBox_SelectedIndexChanged);
      // 
      // pages
      // 
      this.pages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.pages.Controls.Add(this.tabPage4);
      this.pages.Controls.Add(this.tabPage1);
      this.pages.Controls.Add(this.tabPage2);
      this.pages.Controls.Add(this.tabPage5);
      this.pages.Controls.Add(this.tabPage3);
      this.pages.Location = new System.Drawing.Point(16, 12);
      this.pages.Name = "pages";
      this.pages.SelectedIndex = 0;
      this.pages.Size = new System.Drawing.Size(369, 280);
      this.pages.TabIndex = 13;
      // 
      // tabPage4
      // 
      this.tabPage4.Controls.Add(this.label9);
      this.tabPage4.Controls.Add(this.buttonColor);
      this.tabPage4.Controls.Add(this.valueColor);
      this.tabPage4.Controls.Add(this.label8);
      this.tabPage4.Controls.Add(this.valuePicture);
      this.tabPage4.Controls.Add(this.label7);
      this.tabPage4.Controls.Add(this.valueFullName);
      this.tabPage4.Controls.Add(this.ldapPort);
      this.tabPage4.Controls.Add(this.label4);
      this.tabPage4.Controls.Add(this.ldapHost);
      this.tabPage4.Controls.Add(this.ldapConnect);
      this.tabPage4.Controls.Add(this.label3);
      this.tabPage4.Controls.Add(this.label2);
      this.tabPage4.Controls.Add(this.ldapPassword);
      this.tabPage4.Controls.Add(this.ldapUser);
      this.tabPage4.Controls.Add(this.label1);
      this.tabPage4.Controls.Add(this.useLDAP);
      this.tabPage4.Location = new System.Drawing.Point(4, 22);
      this.tabPage4.Name = "tabPage4";
      this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage4.Size = new System.Drawing.Size(361, 254);
      this.tabPage4.TabIndex = 3;
      this.tabPage4.Text = "LDAP";
      this.tabPage4.UseVisualStyleBackColor = true;
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(8, 220);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(34, 13);
      this.label9.TabIndex = 28;
      this.label9.Text = "Color:";
      // 
      // buttonColor
      // 
      this.buttonColor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonColor.Location = new System.Drawing.Point(121, 215);
      this.buttonColor.Name = "buttonColor";
      this.buttonColor.Size = new System.Drawing.Size(231, 23);
      this.buttonColor.TabIndex = 27;
      this.buttonColor.Text = "Change color";
      this.buttonColor.UseVisualStyleBackColor = true;
      this.buttonColor.Click += new System.EventHandler(this.buttonColor_Click);
      // 
      // valueColor
      // 
      this.valueColor.BackColor = System.Drawing.Color.Black;
      this.valueColor.Location = new System.Drawing.Point(80, 215);
      this.valueColor.Name = "valueColor";
      this.valueColor.Size = new System.Drawing.Size(35, 23);
      this.valueColor.TabIndex = 26;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(8, 191);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(68, 13);
      this.label8.TabIndex = 25;
      this.label8.Text = "Picture URL:";
      // 
      // valuePicture
      // 
      this.valuePicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valuePicture.Location = new System.Drawing.Point(80, 188);
      this.valuePicture.Name = "valuePicture";
      this.valuePicture.Size = new System.Drawing.Size(272, 20);
      this.valuePicture.TabIndex = 24;
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(8, 164);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(55, 13);
      this.label7.TabIndex = 23;
      this.label7.Text = "Full name:";
      // 
      // valueFullName
      // 
      this.valueFullName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.valueFullName.Location = new System.Drawing.Point(80, 161);
      this.valueFullName.Name = "valueFullName";
      this.valueFullName.Size = new System.Drawing.Size(272, 20);
      this.valueFullName.TabIndex = 22;
      // 
      // ldapPort
      // 
      this.ldapPort.Location = new System.Drawing.Point(99, 53);
      this.ldapPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
      this.ldapPort.Name = "ldapPort";
      this.ldapPort.Size = new System.Drawing.Size(253, 20);
      this.ldapPort.TabIndex = 10;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(3, 29);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(32, 13);
      this.label4.TabIndex = 9;
      this.label4.Text = "Host:";
      // 
      // ldapHost
      // 
      this.ldapHost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.ldapHost.Location = new System.Drawing.Point(99, 26);
      this.ldapHost.Name = "ldapHost";
      this.ldapHost.Size = new System.Drawing.Size(253, 20);
      this.ldapHost.TabIndex = 8;
      // 
      // ldapConnect
      // 
      this.ldapConnect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.ldapConnect.Location = new System.Drawing.Point(7, 132);
      this.ldapConnect.Name = "ldapConnect";
      this.ldapConnect.Size = new System.Drawing.Size(345, 23);
      this.ldapConnect.TabIndex = 7;
      this.ldapConnect.Text = "Edit user info";
      this.ldapConnect.UseVisualStyleBackColor = true;
      this.ldapConnect.Click += new System.EventHandler(this.ldapConnect_Click);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(3, 109);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(56, 13);
      this.label3.TabIndex = 6;
      this.label3.Text = "Password:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(3, 82);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(51, 13);
      this.label2.TabIndex = 5;
      this.label2.Text = "User DN:";
      // 
      // ldapPassword
      // 
      this.ldapPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.ldapPassword.Location = new System.Drawing.Point(99, 106);
      this.ldapPassword.Name = "ldapPassword";
      this.ldapPassword.Size = new System.Drawing.Size(253, 20);
      this.ldapPassword.TabIndex = 4;
      this.ldapPassword.UseSystemPasswordChar = true;
      // 
      // ldapUser
      // 
      this.ldapUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.ldapUser.Location = new System.Drawing.Point(99, 79);
      this.ldapUser.Name = "ldapUser";
      this.ldapUser.Size = new System.Drawing.Size(253, 20);
      this.ldapUser.TabIndex = 3;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 55);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(29, 13);
      this.label1.TabIndex = 2;
      this.label1.Text = "Port:";
      // 
      // useLDAP
      // 
      this.useLDAP.AutoSize = true;
      this.useLDAP.Location = new System.Drawing.Point(7, 7);
      this.useLDAP.Name = "useLDAP";
      this.useLDAP.Size = new System.Drawing.Size(147, 17);
      this.useLDAP.TabIndex = 0;
      this.useLDAP.Text = "Use LDAP Authentication";
      this.useLDAP.UseVisualStyleBackColor = true;
      this.useLDAP.CheckedChanged += new System.EventHandler(this.useLDAP_CheckedChanged);
      // 
      // tabPage1
      // 
      this.tabPage1.Controls.Add(this.manageDatabaseButton);
      this.tabPage1.Controls.Add(this.databasesList);
      this.tabPage1.Controls.Add(this.addDatabaseButton);
      this.tabPage1.Controls.Add(this.editDatabaseButton);
      this.tabPage1.Controls.Add(this.deleteDatabaseButton);
      this.tabPage1.Location = new System.Drawing.Point(4, 22);
      this.tabPage1.Name = "tabPage1";
      this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage1.Size = new System.Drawing.Size(361, 254);
      this.tabPage1.TabIndex = 0;
      this.tabPage1.Text = "Databases";
      this.tabPage1.UseVisualStyleBackColor = true;
      // 
      // manageDatabaseButton
      // 
      this.manageDatabaseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.manageDatabaseButton.Location = new System.Drawing.Point(37, 225);
      this.manageDatabaseButton.Name = "manageDatabaseButton";
      this.manageDatabaseButton.Size = new System.Drawing.Size(75, 23);
      this.manageDatabaseButton.TabIndex = 4;
      this.manageDatabaseButton.Text = "Manage";
      this.manageDatabaseButton.UseVisualStyleBackColor = true;
      this.manageDatabaseButton.Click += new System.EventHandler(this.manageDatabaseButton_Click);
      // 
      // databasesList
      // 
      this.databasesList.FormattingEnabled = true;
      this.databasesList.Location = new System.Drawing.Point(7, 7);
      this.databasesList.Name = "databasesList";
      this.databasesList.Size = new System.Drawing.Size(348, 212);
      this.databasesList.TabIndex = 3;
      this.databasesList.SelectedIndexChanged += new System.EventHandler(this.databasesList_SelectedIndexChanged);
      // 
      // addDatabaseButton
      // 
      this.addDatabaseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.addDatabaseButton.Location = new System.Drawing.Point(118, 225);
      this.addDatabaseButton.Name = "addDatabaseButton";
      this.addDatabaseButton.Size = new System.Drawing.Size(75, 23);
      this.addDatabaseButton.TabIndex = 0;
      this.addDatabaseButton.Text = "Add";
      this.addDatabaseButton.UseVisualStyleBackColor = true;
      this.addDatabaseButton.Click += new System.EventHandler(this.addDatabaseButton_Click);
      // 
      // editDatabaseButton
      // 
      this.editDatabaseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.editDatabaseButton.Location = new System.Drawing.Point(199, 225);
      this.editDatabaseButton.Name = "editDatabaseButton";
      this.editDatabaseButton.Size = new System.Drawing.Size(75, 23);
      this.editDatabaseButton.TabIndex = 1;
      this.editDatabaseButton.Text = "Edit";
      this.editDatabaseButton.UseVisualStyleBackColor = true;
      this.editDatabaseButton.Click += new System.EventHandler(this.editDatabaseButton_Click);
      // 
      // deleteDatabaseButton
      // 
      this.deleteDatabaseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.deleteDatabaseButton.Location = new System.Drawing.Point(280, 225);
      this.deleteDatabaseButton.Name = "deleteDatabaseButton";
      this.deleteDatabaseButton.Size = new System.Drawing.Size(75, 23);
      this.deleteDatabaseButton.TabIndex = 2;
      this.deleteDatabaseButton.Text = "Delete";
      this.deleteDatabaseButton.UseVisualStyleBackColor = true;
      this.deleteDatabaseButton.Click += new System.EventHandler(this.deleteDatabaseButton_Click);
      // 
      // tabPage2
      // 
      this.tabPage2.Controls.Add(this.ruleSetsList);
      this.tabPage2.Controls.Add(this.DeleteRuleSetButton);
      this.tabPage2.Controls.Add(this.AddRuleSetButton);
      this.tabPage2.Controls.Add(this.EditRuleSetButton);
      this.tabPage2.Location = new System.Drawing.Point(4, 22);
      this.tabPage2.Name = "tabPage2";
      this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage2.Size = new System.Drawing.Size(361, 254);
      this.tabPage2.TabIndex = 1;
      this.tabPage2.Text = "Rule Sets";
      this.tabPage2.UseVisualStyleBackColor = true;
      // 
      // ruleSetsList
      // 
      this.ruleSetsList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.ruleSetsList.FormattingEnabled = true;
      this.ruleSetsList.Location = new System.Drawing.Point(4, 7);
      this.ruleSetsList.Name = "ruleSetsList";
      this.ruleSetsList.Size = new System.Drawing.Size(351, 212);
      this.ruleSetsList.TabIndex = 11;
      this.ruleSetsList.SelectedIndexChanged += new System.EventHandler(this.ruleSetsList_SelectedIndexChanged);
      // 
      // tabPage5
      // 
      this.tabPage5.Controls.Add(this.scriptsList);
      this.tabPage5.Controls.Add(this.DeleteScriptButton);
      this.tabPage5.Controls.Add(this.AddScriptButton);
      this.tabPage5.Controls.Add(this.EditScriptButton);
      this.tabPage5.Location = new System.Drawing.Point(4, 22);
      this.tabPage5.Name = "tabPage5";
      this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage5.Size = new System.Drawing.Size(361, 254);
      this.tabPage5.TabIndex = 4;
      this.tabPage5.Text = "Scripts";
      this.tabPage5.UseVisualStyleBackColor = true;
      // 
      // scriptsList
      // 
      this.scriptsList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.scriptsList.FormattingEnabled = true;
      this.scriptsList.Location = new System.Drawing.Point(5, 7);
      this.scriptsList.Name = "scriptsList";
      this.scriptsList.Size = new System.Drawing.Size(351, 212);
      this.scriptsList.TabIndex = 15;
      this.scriptsList.SelectedIndexChanged += new System.EventHandler(this.scriptsList_SelectedIndexChanged);
      // 
      // DeleteScriptButton
      // 
      this.DeleteScriptButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.DeleteScriptButton.Location = new System.Drawing.Point(292, 225);
      this.DeleteScriptButton.Name = "DeleteScriptButton";
      this.DeleteScriptButton.Size = new System.Drawing.Size(64, 23);
      this.DeleteScriptButton.TabIndex = 14;
      this.DeleteScriptButton.Text = "Delete";
      this.DeleteScriptButton.UseVisualStyleBackColor = true;
      this.DeleteScriptButton.Click += new System.EventHandler(this.DeleteScriptButton_Click);
      // 
      // AddScriptButton
      // 
      this.AddScriptButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.AddScriptButton.Location = new System.Drawing.Point(152, 225);
      this.AddScriptButton.Name = "AddScriptButton";
      this.AddScriptButton.Size = new System.Drawing.Size(64, 23);
      this.AddScriptButton.TabIndex = 12;
      this.AddScriptButton.Text = "Add";
      this.AddScriptButton.UseVisualStyleBackColor = true;
      this.AddScriptButton.Click += new System.EventHandler(this.AddScriptButton_Click);
      // 
      // EditScriptButton
      // 
      this.EditScriptButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.EditScriptButton.Location = new System.Drawing.Point(222, 225);
      this.EditScriptButton.Name = "EditScriptButton";
      this.EditScriptButton.Size = new System.Drawing.Size(64, 23);
      this.EditScriptButton.TabIndex = 13;
      this.EditScriptButton.Text = "Edit";
      this.EditScriptButton.UseVisualStyleBackColor = true;
      this.EditScriptButton.Click += new System.EventHandler(this.EditScriptButton_Click);
      // 
      // tabPage3
      // 
      this.tabPage3.Controls.Add(this.passwordsComboBox);
      this.tabPage3.Controls.Add(this.DeletePasswordButton);
      this.tabPage3.Location = new System.Drawing.Point(4, 22);
      this.tabPage3.Name = "tabPage3";
      this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage3.Size = new System.Drawing.Size(361, 254);
      this.tabPage3.TabIndex = 2;
      this.tabPage3.Text = "Stored Passwords";
      this.tabPage3.UseVisualStyleBackColor = true;
      // 
      // DialogConfiguration
      // 
      this.AcceptButton = this.SaveButton;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(397, 327);
      this.Controls.Add(this.pages);
      this.Controls.Add(this.SaveButton);
      this.MinimumSize = new System.Drawing.Size(263, 168);
      this.Name = "DialogConfiguration";
      this.Text = "Configuration";
      this.Load += new System.EventHandler(this.DialogConfiguration_Load);
      this.pages.ResumeLayout(false);
      this.tabPage4.ResumeLayout(false);
      this.tabPage4.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.ldapPort)).EndInit();
      this.tabPage1.ResumeLayout(false);
      this.tabPage2.ResumeLayout(false);
      this.tabPage5.ResumeLayout(false);
      this.tabPage3.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button SaveButton;
    private System.Windows.Forms.Button AddRuleSetButton;
    private System.Windows.Forms.Button EditRuleSetButton;
    private System.Windows.Forms.Button DeleteRuleSetButton;
    private System.Windows.Forms.Button DeletePasswordButton;
    private System.Windows.Forms.ComboBox passwordsComboBox;
    private System.Windows.Forms.TabControl pages;
    private System.Windows.Forms.TabPage tabPage1;
    private System.Windows.Forms.TabPage tabPage2;
    private System.Windows.Forms.TabPage tabPage3;
    private System.Windows.Forms.ListBox ruleSetsList;
    private System.Windows.Forms.Button addDatabaseButton;
    private System.Windows.Forms.Button editDatabaseButton;
    private System.Windows.Forms.Button deleteDatabaseButton;
    private System.Windows.Forms.ListBox databasesList;
    private System.Windows.Forms.TabPage tabPage4;
    private System.Windows.Forms.CheckBox useLDAP;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox ldapPassword;
    private System.Windows.Forms.TextBox ldapUser;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button ldapConnect;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox ldapHost;
    private System.Windows.Forms.NumericUpDown ldapPort;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Button buttonColor;
    private System.Windows.Forms.Label valueColor;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.TextBox valuePicture;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.TextBox valueFullName;
    private System.Windows.Forms.ColorDialog colorDialog;
    private System.Windows.Forms.Button manageDatabaseButton;
    private System.Windows.Forms.TabPage tabPage5;
    private System.Windows.Forms.ListBox scriptsList;
    private System.Windows.Forms.Button DeleteScriptButton;
    private System.Windows.Forms.Button AddScriptButton;
    private System.Windows.Forms.Button EditScriptButton;
  }
}