using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Xml;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.DirectoryServices.Protocols;
using System.Security.Cryptography.X509Certificates;

namespace Colabo
{
  public enum DBServerType
  {
    STWebServices,
    STMySQL,
    STSQLServer,
    STNewsServer
  }
  public class DBServerTypeInfo
  {
    public DBServerType _value;
    public string _name;

    public DBServerTypeInfo(DBServerType value, string name)
    {
      _value = value;
      _name = name;
    }
    public override string ToString()
    {
      return _name;
    }

    public static DBServerTypeInfo[] Values =
    {
      new DBServerTypeInfo(DBServerType.STWebServices, "Colabo Web Services"),
      new DBServerTypeInfo(DBServerType.STMySQL, "MySQL"),
      new DBServerTypeInfo(DBServerType.STSQLServer, "Microsoft SQL Server"),
      new DBServerTypeInfo(DBServerType.STNewsServer, "News Server")
    };
  }

  /// Database connection info
  public class DBInfo
  {
    // SQL database settings
    public string _name;       // user friendly name of the database
    public string _user;       //"bebul"
    public string _password;   //"divky"
    public DBServerType _serverType;
    public string _server;     //"dev.bistudio.com"
    public string _database;   //"colabo"  

    // LDAP entry path
    public string _ldapDN;

    // SVN default repository
    public string _svnRepository; //"https://dev.bistudio.com/svn/pgm/Colabo/data/"

    // Base directory for relative URIs
    public string _svnRoot; //"https://dev.bistudio.com/svn/pgm/Colabo/"
    public bool SetSVNRoot(string value)
    {
      if (!string.IsNullOrEmpty(value))
      {
        if (!value.EndsWith("/")) value += "/";
      }
      _svnRoot = value;
      return true;
    }

    // URL of the translation service
    public string _urlTranslate; // "https://dev.bistudio.com/MultiLang/Translate"

    // LDAP user settings
    public string _accessRights;
    public class LDAPGroupInfo
    {
      public string _name;
      public string _accessRights;
      public int _level;
      public int _homePage;

      public LDAPGroupInfo()
      {
        _name = null;
        _accessRights = "";
        _level = 0;
        _homePage = 0;
      }
    }
    public List<LDAPGroupInfo> _groups;

    public DBInfo()
    {
      _serverType = DBServerType.STMySQL;
      _accessRights = "";
      _groups = new List<LDAPGroupInfo>();
    }
    public DBInfo(DBInfo src)
    {
      _name = src._name;
      _user = src._user;
      _password = src._password;
      _serverType = src._serverType;
      _server = src._server;
      _database = src._database;
      _ldapDN = src._ldapDN;
      _svnRepository = src._svnRepository;
      _svnRoot = src._svnRoot;
      _urlTranslate = src._urlTranslate;

      _accessRights = src._accessRights;
      _groups = src._groups;
    }

    public override string ToString()
    {
      return string.Format("{0} ({1}@{2}:{3})", _name, _user, _server, _database);
    }
  }

  public class Configuration
  {
    /// file name of the config
    private string _configFile;

    /// URL of the current distribution
    private string _updatePath;
    /// check for updates at startup
    private bool _updateAtStartup;

    /// Flag if automaticaly mark message as read
    private bool _markAsRead;
    /// Time after witch mark message as read
    private int _markAsReadTime;

    /// Interval in minutes how often Synchronize is executed regullary
    private int _synchronizeInterval;

    /// presence away detection
    private int _awayAfterInterval;

    private String _stationGUID;

    /// parameters of attached databases
    private List<DBInfo> _databases;

    public string updatePath
    {
      get { return _updatePath; }
    }
    public List<DBInfo> databases
    {
      get { return _databases; }
    }

    public bool MarkAsRead
    {
      get { return _markAsRead; }
      set { _markAsRead = value; }
    }
    public int MarkAsReadTime
    {
      get { return _markAsReadTime; }
      set { _markAsReadTime = value; }
    }
    public int SynchronizeInterval
    {
      get { return _synchronizeInterval; }
      set { _synchronizeInterval = value; }
    }
    public int AwayAfterInterval
    {
      get { return _awayAfterInterval> 0 ? _awayAfterInterval : 5; }
      set { _awayAfterInterval = value; }
    }
    public string StationGUID
    {
      get { return _stationGUID; }
      set { _stationGUID = value; }
    }

    /// cache of authentications
    ColaboCredentialCache _authentications;

    private ColaboCredentialCache authentications
    {
      get { return _authentications; }
      //set { _authentications = value; }
    }

    /// LDAP authentication
    private bool _useLDAP;
    private string _ldapHost;
    private int _ldapPort;
    private string _ldapUser;
    private string _ldapPassword;
    public bool useLDAP { get { return _useLDAP;} }
    public string ldapHost { get { return _ldapHost; } }
    public int ldapPort { get { return _ldapPort; } }
    public string ldapUser { get { return _ldapUser; } }
    public string ldapPassword { get { return _ldapPassword; } }

    /// LDAP user properties
    public class LDAPProfile
    {
      public string _uid;
      public string _name;
      public string _picture;
      public bool _colorSet;
      public int _color;

      public LDAPProfile()
      {
        _uid = null;
        _name = null;
        _picture = null;
        _colorSet = false;
        _color = 0;
      }
    }
    private LDAPProfile _ldapProfile;
    public LDAPProfile ldapProfile { get { return _useLDAP ? _ldapProfile : null; } }

    public void UpdateAuthentications(Uri url, string realm, string authType, NetworkCredential cred, bool saveConfig)
    {
      authentications.Update(url, realm, authType, cred);
      if (saveConfig)
      {
        Save(Program.forms);
      }
    }

    public void RefreshAuthentications(Uri url, string authType, NetworkCredential cred)
    {
      authentications.Refresh(url, authType, cred);
    }

    public void RemoveAuthentication(Uri url, string realm, string authType, bool saveConfig)
    {
      authentications.Remove(url, realm, authType);
      if (saveConfig)
      {
        Save(Program.forms);
      }
    }

    public bool FindCredentials(Uri urlPrefix, string realm, string authType, out string user, out string password)
    {
      return _authentications.FindCredentials(urlPrefix, realm, authType, out user, out password);
    }

    public CredentialCache credentialCache
    {
      get { return _authentications.credentialCache;  }
    }

    public Hashtable colaboCache
    {
      get { return _authentications.colaboCache; }
    }

    public Configuration(string configFile)
    {
      _configFile = configFile;

      _updatePath = "https://projects.bistudio.com/Colabo/dist/";
      _updateAtStartup = !System.Diagnostics.Debugger.IsAttached;

      _markAsRead = false;
      _markAsReadTime = 1;

      _synchronizeInterval = 0;

      _databases = new List<DBInfo>();
      _authentications = new ColaboCredentialCache();

      _useLDAP = false;
      _ldapHost = "";
      _ldapPort = 389;
      _ldapUser = "";
      _ldapPassword = "";

      _ldapProfile = new LDAPProfile();
    }

    /// returns the configuration class for the first view
    public XmlElement FirstView()
    {
      // read the configuration
      XmlDocument file = new XmlDocument();
      file.Load(_configFile);

      XmlElement clsView = (XmlElement)file.SelectSingleNode("Colabo/View"); // new format - multiple views
      if (clsView == null) clsView = (XmlElement)file.SelectSingleNode("Colabo"); // old format - single view

      return clsView;
    }

    public void Load(Program.ColaboApplicationContext context, bool startup)
    {
      if (!File.Exists(_configFile)) return;

      // read the configuration
      XmlDocument file = new XmlDocument();
      try
      {
        file.Load(_configFile);
      }
      catch (System.Exception e)
      {
        Console.Write(e.ToString());
        return;
      }

      bool needsSaving = false;

      // authentication
      XmlElement cls = (XmlElement)file.SelectSingleNode("Colabo/Credentials");
      if (cls != null)
      {
        XmlNodeList list = cls.SelectNodes("Auth");
        for (int i = 0; i < list.Count; i++)
        {
          XmlElement authCls = (XmlElement)list[i];
          string url = authCls.GetAttribute("url");
          string realm = authCls.GetAttribute("realm");
          if (realm == null) realm = "";
          string user = authCls.GetAttribute("user");
          string pass = Base64ToString(authCls.GetAttribute("password"));
          //System.Diagnostics.Debug.Print(" {0} {1} {2}", url, user, pass);
          Uri serverUrl = new Uri(url);
          _authentications.Update(serverUrl, realm, "Basic", new NetworkCredential(user, pass)); // replace the stored info if present
        }
      }

      // keyboard language
      cls = (XmlElement)file.SelectSingleNode("Colabo/InputLanguage");
      if (cls != null)
      {
        string name = cls.GetAttribute("name");
        foreach (InputLanguage lang in InputLanguage.InstalledInputLanguages)
        {
          if (name.Equals(lang.LayoutName, StringComparison.OrdinalIgnoreCase))
          {
            InputLanguage.CurrentInputLanguage = lang;
            break;
          }
        }
      }

      // auto-update support
      cls = (XmlElement)file.SelectSingleNode("Colabo/Autoupdate");
      if (cls != null)
      {
        _updatePath = cls.GetAttribute("path");
        bool.TryParse(cls.GetAttribute("checkAtStartup"), out _updateAtStartup);
      }
      if (!System.Diagnostics.Debugger.IsAttached && startup && _updateAtStartup && !string.IsNullOrEmpty(_updatePath))
      {
        if (Program.Update(_updatePath, false))  // do not save config, not fully loaded
          return; // program update in progress, restart will follow
      }

      // mark as readed
      cls = (XmlElement)file.SelectSingleNode("Colabo/MarkAsReaded");
      if (cls != null)
      {
        bool.TryParse(cls.GetAttribute("enabled"), out _markAsRead);
        int.TryParse(cls.GetAttribute("time"), out _markAsReadTime);
      }

      // synchronization
      cls = (XmlElement)file.SelectSingleNode("Colabo/Synchronize");
      if (cls != null)
      {
        int.TryParse(cls.GetAttribute("interval"), out _synchronizeInterval);
      }

      cls = (XmlElement)file.SelectSingleNode("Colabo/AwayAfter");
      if (cls != null)
      {
        int.TryParse(cls.GetAttribute("interval"), out _awayAfterInterval);
      }

      cls = (XmlElement)file.SelectSingleNode("Colabo/Station");
      if (cls != null)
      {
        _stationGUID = cls.GetAttribute("guid");
      }

      if (_stationGUID == null || _stationGUID.Length == 0)
      {
        Guid guid = System.Guid.NewGuid();
        _stationGUID = guid.ToString();

        cls = UseElement(file, "Colabo", "Station");
        cls.SetAttribute("guid", _stationGUID);
        needsSaving = true;
      }

      // LDAP authentication
      cls = (XmlElement)file.SelectSingleNode("Colabo/LDAP");
      if (cls != null)
      {
        bool.TryParse(cls.GetAttribute("useLDAP"), out _useLDAP);
        _ldapHost = cls.GetAttribute("host");
        int.TryParse(cls.GetAttribute("port"), out _ldapPort);
        _ldapUser = cls.GetAttribute("user");
        _ldapPassword = cls.GetAttribute("password");
        if (cls.HasAttribute("pe"))
        {
          if (int.Parse(cls.GetAttribute("pe")) != 0)
          {
            _ldapPassword = Base64ToString(_ldapPassword);
          }
        }
      }

      // database connections
      _databases.Clear();
      XmlNodeList dbs = file.SelectNodes("Colabo/Database");
      if (dbs != null && dbs.Count > 0)
      {
        // new format - multiple databases
        foreach (XmlElement db in dbs)
        {
          DBInfo info = LoadDatabase(db);
          if (info != null)
          {
            info._name = db.GetAttribute("name");
            _databases.Add(info);
          }
        }
      }
      else
      {
        // old format - single view
        XmlElement db = (XmlElement)file.SelectSingleNode("Colabo");
        DBInfo info = LoadDatabase(db);
        if (info != null)
        {
          info._name = info._database;
          _databases.Add(info);
        }
      }

      // update the list of databases from LDAP
      LDAPLoadProfile();

      // Forms serialization
      context.forms.Clear();
      XmlNodeList views = file.SelectNodes("Colabo/View");
      if (views != null && views.Count > 0)
      {
        // new format - multiple views
        foreach (XmlElement clsView in views)
          context.NewForm(clsView);
      }
      else
      {
        // old format - single view
        XmlElement clsView = (XmlElement)file.SelectSingleNode("Colabo");
        context.NewForm(clsView);
      }

      if (needsSaving)
      {
        file.Save(_configFile);
      }
    }

    private static XmlElement UseElement(XmlDocument file, String node, String element)
    {
      XmlElement existingNode = (XmlElement)file.SelectSingleNode(node + "/" + element);
      if (existingNode != null) return existingNode;

      XmlElement cls = file.CreateElement(element);

      XmlNode config = file.SelectSingleNode(node);
      config.AppendChild(cls);
      return cls;
    }

    private DBInfo LoadDatabase(XmlElement clsDb)
    {
      DBInfo info = new DBInfo();
      XmlElement cls = (XmlElement)clsDb.SelectSingleNode("Connection");
      if (cls == null) return null;

      info._ldapDN = clsDb.GetAttribute("ldapDN");

      info._serverType = DBServerType.STMySQL;
      if (cls.HasAttribute("serverType"))
      {
        String value = cls.GetAttribute("serverType");
        if (Enum.IsDefined(typeof(DBServerType), value))
          info._serverType = (DBServerType)Enum.Parse(typeof(DBServerType), value, true);
      }

      info._server = cls.GetAttribute("server");
      info._database = cls.GetAttribute("database");
      info._user = cls.GetAttribute("user");
      info._password = cls.GetAttribute("password");
      if (cls.HasAttribute("pe"))
      {
        if (int.Parse(cls.GetAttribute("pe")) != 0)
        {
          info._password = Base64ToString(info._password);
        }
      }

      cls = (XmlElement)clsDb.SelectSingleNode("Svn");
      if (cls == null) return null;

      // storage we will save new articles to
      info._svnRepository = cls.GetAttribute("defaultRepository");

      string svnRoot = cls.GetAttribute("baseUri");
      info.SetSVNRoot(svnRoot);

      string urlTranslate = cls.GetAttribute("urlTranslate");
      info._urlTranslate = urlTranslate;

      return info;
    }

    private bool LDAPLoadProfile()
    {
      // return true if something changed
      if (!_useLDAP) return false;

      // return true when some value changed
      bool changed = false;

      LdapConnection connection = OpenLDAPConnection(_ldapHost, _ldapPort, _ldapUser, _ldapPassword);
      if (connection == null)
      {
        FormColabo.Log("Cannot open LDAP connection!");
        return false;
      }
      try
      {
        // access the user profile
        SearchRequest request = new SearchRequest(_ldapUser, "(objectClass=*)", SearchScope.Base,
          new String[]{"uid", "displayName", "colaboUserPicture", "colaboUserColor", "colaboDBDN"});
        SearchResponse response = (SearchResponse)connection.SendRequest(request);

        if (response.Entries.Count == 0)
        {
          FormColabo.Log(string.Format("LDAP user {0} not found!", _ldapUser));
          return false; // user not found in LDAP
        }
        SearchResultEntry entry = response.Entries[0];

        // read common properties from the user profile
        DirectoryAttribute attributes = entry.Attributes["uid"];
        if (attributes != null && attributes.Count == 1) _ldapProfile._uid = attributes[0].ToString();
        attributes = entry.Attributes["displayName"];
        if (attributes != null && attributes.Count == 1) _ldapProfile._name = attributes[0].ToString();
        attributes = entry.Attributes["colaboUserPicture"];
        if (attributes != null && attributes.Count == 1) _ldapProfile._picture = attributes[0].ToString();
        attributes = entry.Attributes["colaboUserColor"];
        if (attributes != null && attributes.Count == 1) int.TryParse(attributes[0].ToString(), out _ldapProfile._color);

        // user id is required
        if (string.IsNullOrEmpty(_ldapProfile._uid))
        {
          FormColabo.Log(string.Format("LDAP user {0} - uid not found!", _ldapUser));
          return false;
        }

        // list of signed databases
        attributes = entry.Attributes["colaboDBDN"];
        if (attributes != null) for (int i = 0; i < attributes.Count; i++)
        {
          string dn = attributes[i].ToString();

          FormColabo.Log(string.Format("LDAP info - database {0}", dn));
          
          SearchRequest dbRequest = new SearchRequest(dn, "(objectClass=*)", SearchScope.Base,
            new String[] { "cn", "colaboDBServerType", "colaboDBServer", "colaboDBDatabase", "colaboSVNPath", "colaboSVNRoot", "colaboTranslate" });
          SearchResponse dbResponse = (SearchResponse)connection.SendRequest(dbRequest);

          if (dbResponse.Entries.Count == 0)
          {
            FormColabo.Log(" - database DN not found");
            return false; // database not found in LDAP
          }
          SearchResultEntry dbEntry = dbResponse.Entries[0];

          // read the database properties
          DirectoryAttribute dbAttributes = dbEntry.Attributes["cn"];
          if (dbAttributes == null || dbAttributes.Count != 1)
          {
            FormColabo.Log(" - database name not found");
            continue; // cn is required
          }
          string name = dbAttributes[0].ToString();

          DBServerType serverType = DBServerType.STMySQL;
          dbAttributes = dbEntry.Attributes["colaboDBServerType"];
          if (dbAttributes != null && dbAttributes.Count == 1)
          {
            string value = dbAttributes[0].ToString();
            if (Enum.IsDefined(typeof(DBServerType), value))
              serverType = (DBServerType)Enum.Parse(typeof(DBServerType), value, true);
          }

          dbAttributes = dbEntry.Attributes["colaboDBServer"];
          if (dbAttributes == null || dbAttributes.Count != 1) continue; // colaboDBServer is required
          string server = dbAttributes[0].ToString();

          string database = "";
          if (serverType != DBServerType.STWebServices)
          {
            dbAttributes = dbEntry.Attributes["colaboDBDatabase"];
            if (dbAttributes == null || dbAttributes.Count != 1) continue; // colaboDBDatabase is required for this server type
            database = dbAttributes[0].ToString();
          }

          string svn = "";
          if (serverType != DBServerType.STNewsServer)
          {
            dbAttributes = dbEntry.Attributes["colaboSVNPath"];
            if (dbAttributes == null || dbAttributes.Count != 1) continue; // colaboSVNPath is required
            svn = dbAttributes[0].ToString();
          }

          string svnRoot = "";
          dbAttributes = dbEntry.Attributes["colaboSVNRoot"];
          if (dbAttributes != null && dbAttributes.Count == 1) // colaboSVNRoot is optional
            svnRoot = dbAttributes[0].ToString();

          string urlTranslate = "";
          dbAttributes = dbEntry.Attributes["colaboTranslate"];
          if (dbAttributes != null && dbAttributes.Count == 1) // colaboTranslation is optional
            urlTranslate = dbAttributes[0].ToString();

          // update the database info stored in the config
          DBInfo info = FindDatabase(serverType, server, database);
          if (info == null)
          {
            info = new DBInfo();
            _databases.Add(info);
            info._serverType = serverType;
            info._server = server;
            info._database = database;
            changed = true;
          }
          if (info._ldapDN != dn)
          {
            info._ldapDN = dn;
            changed = true;
          }
          if (info._name != name)
          {
            info._name = name;
            changed = true;
          }
          if (info._user != _ldapProfile._uid)
          {
            info._user = _ldapProfile._uid;
            changed = true;
          }
          if (info._password != _ldapPassword)
          {
            info._password = _ldapPassword;
            changed = true;
          }
          if (info._svnRepository != svn)
          {
            info._svnRepository = svn;
            changed = true;
          }
          if (info.SetSVNRoot(svnRoot)) changed = true;
          if (info._urlTranslate != urlTranslate)
          {
            info._urlTranslate = urlTranslate;
            changed = true;
          }
 
          // read the user settings
          SearchRequest userRequest = new SearchRequest("ou=users," + dn, string.Format("(&(objectClass=colaboUser)(colaboUserDN={0}))", _ldapUser), SearchScope.Subtree,
            new String[] {"colaboAccessRights"});
          SearchResponse userResponse = (SearchResponse)connection.SendRequest(userRequest);
          if (userResponse.Entries.Count > 0)
          {
            SearchResultEntry userEntry = userResponse.Entries[0];
            DirectoryAttribute userAttributes = userEntry.Attributes["colaboAccessRights"];
            if (userAttributes != null && userAttributes.Count == 1) info._accessRights = userAttributes[0].ToString();
          }

          // check what groups the user belong to
          SearchRequest groupRequest = new SearchRequest("ou=groups," + dn, "(objectClass=colaboGroup)", SearchScope.Subtree,
            new String[] {"cn", "colaboAccessRights", "colaboGroupLevel", "colaboHomePage", "memberUid", "member"});
          SearchResponse groupResponse = (SearchResponse)connection.SendRequest(groupRequest);

          FormColabo.Log(string.Format(" - {0} groups found", groupResponse.Entries.Count));

          foreach (SearchResultEntry groupEntry in groupResponse.Entries)
          {
            // check if _ldapProfile._uid is a member
            bool isMember = false;
            
            DirectoryAttribute groupAttributes = groupEntry.Attributes["memberUid"];
            if (groupAttributes != null)
            {
              for (int j = 0; j < groupAttributes.Count; j++)
              {
                string member = groupAttributes[j].ToString();
                if (member.Equals(_ldapProfile._uid))
                {
                  isMember = true;
                  break;
                }
              }
            }

            if (!isMember)
            {
              groupAttributes = groupEntry.Attributes["member"];
              if (groupAttributes != null)
              {
                for (int j = 0; j < groupAttributes.Count; j++)
                {
                  string member = groupAttributes[j].ToString();
                  if (IsMember(connection, member, _ldapProfile._uid))
                  {
                    isMember = true;
                    break;
                  }
                }
              }
            }

            if (!isMember) continue; // not a member of this group

            // read the group attributes
            DBInfo.LDAPGroupInfo group = new DBInfo.LDAPGroupInfo();
            info._groups.Add(group);

            groupAttributes = groupEntry.Attributes["cn"];
            if (groupAttributes != null && groupAttributes.Count == 1) group._name = groupAttributes[0].ToString();

            groupAttributes = groupEntry.Attributes["colaboAccessRights"];
            if (groupAttributes != null && groupAttributes.Count == 1) group._accessRights = groupAttributes[0].ToString();

            groupAttributes = groupEntry.Attributes["colaboGroupLevel"];
            if (groupAttributes != null && groupAttributes.Count == 1) int.TryParse(groupAttributes[0].ToString(), out group._level);

            groupAttributes = groupEntry.Attributes["colaboHomePage"];
            if (groupAttributes != null && groupAttributes.Count == 1) int.TryParse(groupAttributes[0].ToString(), out group._homePage);
          }
          info._groups.Sort(delegate(DBInfo.LDAPGroupInfo group1, DBInfo.LDAPGroupInfo group2)
          {
            int diff = group1._level - group2._level;
            if (diff != 0) return diff;
            return group1._name.CompareTo(group2._name);
          });
        }
      }
      catch (System.Exception e)
      {
        // LDAP operation failed, keep settings from the config file
        Console.WriteLine(e.ToString());
        FormColabo.Log(string.Format("LDAP operation failed: {0}", e.ToString()));
        return false;
      }

      return changed;
    }
    private bool IsMember(LdapConnection connection, string dn, string uid)
    {
      // access the user / group entry with the given dn
      SearchResponse response = null;
      try
      {
        SearchRequest request = new SearchRequest(dn, "(objectClass=*)", SearchScope.Base, new String[] { "uid", "member" });
        response = (SearchResponse)connection.SendRequest(request);
      }
      catch (DirectoryException)
      {
      	// the request cannot be executed (missing rights probably)
        return false;
      }
      if (response.Entries.Count > 0)
      {
        SearchResultEntry entry = response.Entries[0];
        // check if this is user entry with given uid
        DirectoryAttribute attributes = entry.Attributes["uid"];
        if (attributes != null && attributes.Count == 1 && attributes[0].ToString().Equals(uid)) return true;
        // check members of group entry recursively
        attributes = entry.Attributes["member"];
        if (attributes != null)
        {
          for (int i = 0; i < attributes.Count; i++)
          {
            string member = attributes[i].ToString();
            if (IsMember(connection, member, uid)) return true;
          }
        }
      }
      return false;
    }

    public static LdapConnection OpenLDAPConnection(string host, int port, string user, string password)
    {
      LdapConnection connection = new LdapConnection(
        new LdapDirectoryIdentifier(string.Format("{0}:{1}", host, port)));
      connection.SessionOptions.ProtocolVersion = 3;
      if (port == 636) // TODO: decide by the new parameter
      {
        connection.SessionOptions.SecureSocketLayer = true;
        connection.SessionOptions.VerifyServerCertificate = new VerifyServerCertificateCallback(CheckCertificate);
      }
      connection.AuthType = AuthType.Basic;
      connection.Credential = new NetworkCredential(user, password);
      try
      {
        // bind the LDAP database
        connection.Bind();
        return connection;
      }
      catch (System.Exception exception)
      {
        MessageBox.Show("Cannot connect to LDAP: " + exception.Message, "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        Console.WriteLine(exception.ToString());
        return null;
      }
    }
    private static bool CheckCertificate(LdapConnection connection, X509Certificate certificate)
    {
      // TODO: for now, all server certificates are trusted
      return true;
    }

    private DBInfo FindDatabase(DBServerType serverType, string server, String database)
    {
      foreach (DBInfo db in _databases)
      {
        if (db._serverType == serverType &&
          db._server.Equals(server, StringComparison.OrdinalIgnoreCase) &&
          db._database.Equals(database, StringComparison.OrdinalIgnoreCase)) return db;
      }
      return null;
    }

    string StringToBase64(string str, Base64FormattingOptions opt)
    {
      System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
      return Convert.ToBase64String(encoding.GetBytes(str), opt);
    }

    string Base64ToString(string str)
    {
      System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
      return encoding.GetString(Convert.FromBase64String(str));
    }

    [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    internal static extern bool EncryptFile(string path);

    public void SaveLDAP(bool useLDAP, string ldapHost, int ldapPort, string ldapUser, string ldapPassword)
    {
      _useLDAP = useLDAP;
      _ldapHost = ldapHost;
      _ldapPort = ldapPort;
      _ldapUser = ldapUser;
      _ldapPassword = ldapPassword;
    }

    public void Save(DialogConfiguration dlg, List<FormColabo> forms)
    {
      _databases.Clear();
      foreach (DBInfoExt database in dlg.databases)
      {
        _databases.Add(database._dbInfo);
      }
      Save(forms);
    }

    public void Save(List<FormColabo> forms)
    {
      // config creation
      XmlDocument file = new XmlDocument();
      XmlElement config = file.CreateElement("Colabo");
      file.AppendChild(config);

      // auto-update support
      XmlElement cls = file.CreateElement("Autoupdate");
      config.AppendChild(cls);
      cls.SetAttribute("path", _updatePath);
      cls.SetAttribute("checkAtStartup", _updateAtStartup.ToString());

      // Mark as readed
      cls = file.CreateElement("MarkAsReaded");
      config.AppendChild(cls);
      cls.SetAttribute("enabled", _markAsRead.ToString());
      cls.SetAttribute("time", _markAsReadTime.ToString());

      // synchronization
      cls = file.CreateElement("Synchronize");
      config.AppendChild(cls);
      cls.SetAttribute("interval", _synchronizeInterval.ToString());

      cls = file.CreateElement("AwayAfter");
      config.AppendChild(cls);
      cls.SetAttribute("interval", _awayAfterInterval.ToString());

      cls = file.CreateElement("Station");
      config.AppendChild(cls);
      cls.SetAttribute("guid", _stationGUID);

      // LDAP authentication
      cls = file.CreateElement("LDAP");
      config.AppendChild(cls);
      cls.SetAttribute("useLDAP", _useLDAP.ToString());
      cls.SetAttribute("host", _ldapHost);
      cls.SetAttribute("port", _ldapPort.ToString());
      cls.SetAttribute("user", _ldapUser);
      cls.SetAttribute("pe", "1");
      cls.SetAttribute("password", StringToBase64(_ldapPassword, Base64FormattingOptions.None));

      // TODO: save list of databases to LDAP server if changed in Configure dialog
      // databases
      foreach (DBInfo info in databases)
      {
        XmlElement clsDb = file.CreateElement("Database");
        config.AppendChild(clsDb);
        SaveDatabase(file, clsDb, info);
      }

      // Keyboard language
      cls = file.CreateElement("InputLanguage");
      config.AppendChild(cls);
      cls.SetAttribute("name", InputLanguage.CurrentInputLanguage.LayoutName);
      // credentials (authentications)
      cls = file.CreateElement("Credentials");
      config.AppendChild(cls);
      Hashtable hashtab = _authentications._colaboCache;
      IEnumerator listCredentials = hashtab.GetEnumerator();
      int ix = 0;
      while ( listCredentials.MoveNext() )
      {
        XmlElement authCls = file.CreateElement("Auth");
        cls.AppendChild(authCls);
        DictionaryEntry auth = (DictionaryEntry)listCredentials.Current;
        Colabo.CredentialKey colKey = (Colabo.CredentialKey)auth.Key;
        NetworkCredential colCred = (NetworkCredential)auth.Value;
        authCls.SetAttribute("ix", string.Format("{0}", ix));
        authCls.SetAttribute("url", colKey.UriPrefix.ToString());
        authCls.SetAttribute("realm", colKey.Realm);
        authCls.SetAttribute("user", colCred.UserName);
        authCls.SetAttribute("password", StringToBase64(colCred.Password, Base64FormattingOptions.None));
        ix++;
      }
      if (!File.Exists(_configFile))
      {
        try
        {
          FileStream stream = File.Create(_configFile);
          stream.Close();
        }
        catch { return; }
      }
      bool encRet = EncryptFile(_configFile);
      if ((File.GetAttributes(_configFile) & FileAttributes.Encrypted) != FileAttributes.Encrypted)
      {
        int error = Marshal.GetLastWin32Error();
        if (error == 3) // PATH_NOT_FOUND
        {
          DialogResult result = MessageBox.Show(
            string.Format("Cannot encrypt the {0} file.\r\nPlease run colabo.exe from non-substituted drive.\r\n\r\nSave it without encryption?", _configFile),
            "Colabo Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
          if (!result.Equals(DialogResult.Yes))
            return; //without save
        }
      }

      // Forms properties
      foreach (FormColabo form in forms)
      {
        cls = file.CreateElement("View");
        config.AppendChild(cls);
        form.SaveDialog(file, cls);
      }

      file.Save(_configFile);
    }

    private void SaveDatabase(XmlDocument file, XmlElement clsDb, DBInfo info)
    {
      clsDb.SetAttribute("name", info._name);
      clsDb.SetAttribute("ldapDN", info._ldapDN);

      // SQL database connection
      XmlElement cls = file.CreateElement("Connection");
      clsDb.AppendChild(cls);
      cls.SetAttribute("serverType", info._serverType.ToString());
      cls.SetAttribute("server", info._server);
      cls.SetAttribute("database", info._database);
      cls.SetAttribute("user", info._user);
      cls.SetAttribute("pe", "1");
      cls.SetAttribute("password", StringToBase64(info._password, Base64FormattingOptions.None));
      
      // SVN default repository
      cls = file.CreateElement("Svn");
      clsDb.AppendChild(cls);
      cls.SetAttribute("defaultRepository", info._svnRepository);

      if (!string.IsNullOrEmpty(info._svnRoot)) cls.SetAttribute("baseUri", info._svnRoot);
      if (!string.IsNullOrEmpty(info._urlTranslate)) cls.SetAttribute("urlTranslate", info._urlTranslate);
    }

#region Public Static Helper Methods
    public static string GetCiteHTMLHeader()
    {
      return @"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01//EN"" ""http://www.w3.org/TR/html4/strict.dtd"">
<html><head><meta http-equiv=""Content-Type"" content=""text/html;charset=utf-8"">
<title>Colabo article</title>
<script type=""text/javascript""><!--
window.PR_SHOULD_USE_CONTINUATION=true,window.PR_TAB_WIDTH=8,window.PR_normalizedHtml=window.PR=window.prettyPrintOne=window.prettyPrint=void 0,window._0=function(){var a=navigator&&navigator.userAgent&&navigator.userAgent.match(/\bMSIE ([678])\./);return a=a?+a[1]:false,window._0=function(){return a},a},(function(){var a=true,b=null,c='break continue do else for if return while auto case char const default double enum extern float goto int long register short signed sizeof static struct switch typedef union unsigned void volatile catch class delete false import new operator private protected public this throw true try typeof ',d=c+'alignof align_union asm axiom bool concept concept_map const_cast constexpr decltype dynamic_cast explicit export friend inline late_check mutable namespace nullptr reinterpret_cast static_assert static_cast template typeid typename using virtual wchar_t where ',e=c+'abstract boolean byte extends final finally implements import instanceof null native package strictfp super synchronized throws transient ',f=e+'as base by checked decimal delegate descending event fixed foreach from group implicit in interface internal into is lock object out override orderby params partial readonly ref sbyte sealed stackalloc string select uint ulong unchecked unsafe ushort var ',g=c+'debugger eval export function get null set undefined var with Infinity NaN ',h='caller delete die do dump elsif eval exit foreach for goto if import last local my next no our print package redo require sub undef unless until use wantarray while BEGIN END ',i='break continue do else for if return while and as assert class def del elif except exec finally from global import in is lambda nonlocal not or pass print raise try with yield False True None ',j='break continue do else for if return while alias and begin case class def defined elsif end ensure false in module next nil not or redo rescue retry self super then true undef unless until when yield BEGIN END ',k='break continue do else for if return while case done elif esac eval fi function in local set then until ',l=d+f+g+h+i+j+k,m=(function(){var a=['!','!=','!==','#','%','%=','&','&&','&&=','&=','(','*','*=','+=',',','-=','->','/','/=',':','::',';','<','<<','<<=','<=','=','==','===','>','>=','>>','>>=','>>>','>>>=','?','@','[','^','^=','^^','^^=','{','|','|=','||','||=','~','break','case','continue','delete','do','else','finally','instanceof','return','throw','try','typeof'],b='(?:^^|[+-]',c;for(c=0;c<a.length;++c)b+='|'+a[c].replace(/([^=<>:&a-z])/g,'\\$1');return b+=')\\s*',b})(),n=/&/g,o=/</g,p=/>/g,q=/\""/g,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F;function G(a){return a.replace(n,'&amp;').replace(o,'&lt;').replace(p,'&gt;').replace(q,'&quot;')}function H(a){return a.replace(n,'&amp;').replace(o,'&lt;').replace(p,'&gt;')}C=/&lt;/g,B=/&gt;/g,w=/&apos;/g,E=/&quot;/g,v=/&amp;/g,D=/&nbsp;/g;function I(a){var b=a.indexOf('&'),c,d,e,f;if(b<0)return a;for(--b;(b=a.indexOf('&#',b+1))>=0;)d=a.indexOf(';',b),d>=0&&(e=a.substring(b+3,d),f=10,e&&e.charAt(0)==='x'&&(e=e.substring(1),f=16),c=parseInt(e,f),isNaN(c)||(a=a.substring(0,b)+String.fromCharCode(c)+a.substring(d+1)));return a.replace(C,'<').replace(B,'>').replace(w,'\'').replace(E,'\""').replace(D,' ').replace(v,'&')}function J(a){return'XMP'===a.tagName}u=/[\r\n]/g;function K(c,d){var e;return'PRE'===c.tagName?a:u.test(d)?(e='',c.currentStyle?(e=c.currentStyle.whiteSpace):window.getComputedStyle&&(e=window.getComputedStyle(c,b).whiteSpace),!e||e==='pre'):a}function L(a,b){var c,d,e,f;switch(a.nodeType){case 1:f=a.tagName.toLowerCase(),b.push('<',f);for(e=0;e<a.attributes.length;++e){c=a.attributes[e];if(!c.specified)continue;b.push(' '),L(c,b)}b.push('>');for(d=a.firstChild;d;d=d.nextSibling)L(d,b);(a.firstChild||!/^(?:br|link|img)$/.test(f))&&b.push('</',f,'>');break;case 2:b.push(a.name.toLowerCase(),'=\""',G(a.value),'\""');break;case 3:case 4:b.push(H(a.nodeValue))}}function M(b){var c=0,d=false,e=false,f,g,h,i;for(f=0,g=b.length;f<g;++f){h=b[f];if(h.ignoreCase)e=a;else if(/[a-z]/i.test(h.source.replace(/\\u[0-9a-f]{4}|\\x[0-9a-f]{2}|\\[^ux]/gi,''))){d=a,e=false;break}}function j(a){if(a.charAt(0)!=='\\')return a.charCodeAt(0);switch(a.charAt(1)){case'b':return 8;case't':return 9;case'n':return 10;case'v':return 11;case'f':return 12;case'r':return 13;case'u':case'x':return parseInt(a.substring(2),16)||a.charCodeAt(1);case'0':case'1':case'2':case'3':case'4':case'5':case'6':case'7':return parseInt(a.substring(1),8);default:return a.charCodeAt(1)}}function k(a){var b;return a<32?(a<16?'\\x0':'\\x')+a.toString(16):(b=String.fromCharCode(a),(b==='\\'||b==='-'||b==='['||b===']')&&(b='\\'+b),b)}function l(a){var b=a.substring(1,a.length-1).match(new RegExp('\\\\u[0-9A-Fa-f]{4}|\\\\x[0-9A-Fa-f]{2}|\\\\[0-3][0-7]{0,2}|\\\\[0-7]{1,2}|\\\\[\\s\\S]|-|[^-\\\\]','g')),c=[],d=[],e=b[0]==='^',f,g,h,i,m,n,o,p,q;for(h=e?1:0,m=b.length;h<m;++h){o=b[h];switch(o){case'\\B':case'\\b':case'\\D':case'\\d':case'\\S':case'\\s':case'\\W':case'\\w':c.push(o);continue}q=j(o),h+2<m&&'-'===b[h+1]?(g=j(b[h+2]),h+=2):(g=q),d.push([q,g]),g<65||q>122||(g<65||q>90||d.push([Math.max(65,q)|32,Math.min(g,90)|32]),g<97||q>122||d.push([Math.max(97,q)&-33,Math.min(g,122)&-33]))}d.sort(function(a,b){return a[0]-b[0]||b[1]-a[1]}),f=[],i=[NaN,NaN];for(h=0;h<d.length;++h)p=d[h],p[0]<=i[1]+1?(i[1]=Math.max(i[1],p[1])):f.push(i=p);n=['['],e&&n.push('^'),n.push.apply(n,c);for(h=0;h<f.length;++h)p=f[h],n.push(k(p[0])),p[1]>p[0]&&(p[1]+1>p[0]&&n.push('-'),n.push(k(p[1])));return n.push(']'),n.join('')}function m(a){var b=a.source.match(new RegExp('(?:\\[(?:[^\\x5C\\x5D]|\\\\[\\s\\S])*\\]|\\\\u[A-Fa-f0-9]{4}|\\\\x[A-Fa-f0-9]{2}|\\\\[0-9]+|\\\\[^ux0-9]|\\(\\?[:!=]|[\\(\\)\\^]|[^\\x5B\\x5C\\(\\)\\^]+)','g')),e=b.length,f=[],g,h,i,j,k;for(j=0,i=0;j<e;++j)k=b[j],k==='('?++i:'\\'===k.charAt(0)&&(h=+k.substring(1),h&&h<=i&&(f[h]=-1));for(j=1;j<f.length;++j)-1===f[j]&&(f[j]=++c);for(j=0,i=0;j<e;++j)k=b[j],k==='('?(++i,f[i]===void 0&&(b[j]='(?:')):'\\'===k.charAt(0)&&(h=+k.substring(1),h&&h<=i&&(b[j]='\\'+f[i]));for(j=0,i=0;j<e;++j)'^'===b[j]&&'^'!==b[j+1]&&(b[j]='');if(a.ignoreCase&&d)for(j=0;j<e;++j)k=b[j],g=k.charAt(0),k.length>=2&&g==='['?(b[j]=l(k)):g!=='\\'&&(b[j]=k.replace(/[a-zA-Z]/g,function(a){var b=a.charCodeAt(0);return'['+String.fromCharCode(b&-33,b|32)+']'}));return b.join('')}i=[];for(f=0,g=b.length;f<g;++f){h=b[f];if(h.global||h.multiline)throw new Error(''+h);i.push('(?:'+m(h)+')')}return new RegExp(i.join('|'),e?'gi':'g')}r=b;function N(a){var c,d,e,f;b===r&&(f=document.createElement('PRE'),f.appendChild(document.createTextNode('<!DOCTYPE foo PUBLIC \""foo bar\"">\n<foo />')),r=!/</.test(f.innerHTML));if(r)return d=a.innerHTML,J(a)?(d=H(d)):K(a,d)||(d=d.replace(/(<br\s*\/?>)[\r\n]+/g,'$1').replace(/(?:[\r\n]+[ \t]*)+/g,' ')),d;e=[];for(c=a.firstChild;c;c=c.nextSibling)L(c,e);return e.join('')}function O(a){var c=0;return function(d){var e=b,f=0,g,h,i,j;for(h=0,i=d.length;h<i;++h){g=d.charAt(h);switch(g){case'	':e||(e=[]),e.push(d.substring(f,h)),j=a-c%a,c+=j;for(;j>=0;j-='                '.length)e.push('                '.substring(0,j));f=h+1;break;case'\n':c=0;break;default:++c}}return e?(e.push(d.substring(f)),e.join('')):d}}z=new RegExp('[^<]+|<!--[\\s\\S]*?-->|<!\\[CDATA\\[[\\s\\S]*?\\]\\]>|</?[a-zA-Z](?:[^>\""\']|\'[^\']*\'|\""[^\""]*\"")*>|<','g'),A=/^<\!--/,y=/^<!\[CDATA\[/,x=/^<br\b/i,F=/^<(\/?)([a-zA-Z][a-zA-Z0-9]*)/;function P(a){var b=a.match(z),c=[],d=0,e=[],f,g,h,i,j,k,l,m;if(b)for(g=0,k=b.length;g<k;++g){j=b[g];if(j.length>1&&j.charAt(0)==='<'){if(A.test(j))continue;if(y.test(j))c.push(j.substring(9,j.length-3)),d+=j.length-12;else if(x.test(j))c.push('\n'),++d;else if(j.indexOf('nocode')>=0&&Q(j)){l=(j.match(F))[2],f=1;for(h=g+1;h<k;++h){m=b[h].match(F);if(m&&m[2]===l)if(m[1]==='/'){if(--f===0)break}else++f}h<k?(e.push(d,b.slice(g,h+1).join('')),g=h):e.push(d,j)}else e.push(d,j)}else i=I(j),c.push(i),d+=i.length}return{source:c.join(''),tags:e}}function Q(a){return!!a.replace(/\s(\w+)\s*=\s*(?:\""([^\""]*)\""|'([^\']*)'|(\S+))/g,' $1=\""$2$3$4\""').match(/[cC][lL][aA][sS][sS]=\""[^\""]*\bnocode\b/)}function R(a,b,c,d){var e;if(!b)return;e={source:b,basePos:a},c(e),d.push.apply(d,e.decorations)}function S(a,c){var d={},e,f,g,h;return(function(){var e=a.concat(c),f=[],g={},i,j,k,l,m,n,o;for(j=0,l=e.length;j<l;++j){m=e[j],o=m[3];if(o)for(i=o.length;--i>=0;)d[o.charAt(i)]=m;n=m[1],k=''+n,g.hasOwnProperty(k)||(f.push(n),g[k]=b)}f.push(/[\0-\uffff]/),h=M(f)})(),f=c.length,g=/\S/,e=function(a){var b=a.source,g=a.basePos,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y;i=[g,'pln'],s=0,y=b.match(h)||[],u={};for(v=0,q=y.length;v<q;++v){w=y[v],t=u[w],p=void 0;if(typeof t==='string')n=false;else{r=d[w.charAt(0)];if(r)p=w.match(r[1]),t=r[0];else{for(m=0;m<f;++m){r=c[m],p=w.match(r[1]);if(p){t=r[0];break}}p||(t='pln')}n=t.length>=5&&'lang-'===t.substring(0,5),n&&!(p&&typeof p[1]==='string')&&(n=false,t='src'),n||(u[w]=t)}x=s,s+=w.length,n?(j=p[1],l=w.indexOf(j),k=l+j.length,p[2]&&(k=w.length-p[2].length,l=k-j.length),o=t.substring(5),R(g+x,w.substring(0,l),e,i),R(g+x+l,j,W(o,j),i),R(g+x+k,w.substring(k),e,i)):i.push(g+x,t)}a.decorations=i},e}function T(a){var c=[],d=[],e,f;return a.tripleQuotedStrings?c.push(['str',/^(?:\'\'\'(?:[^\'\\]|\\[\s\S]|\'{1,2}(?=[^\']))*(?:\'\'\'|$)|\""\""\""(?:[^\""\\]|\\[\s\S]|\""{1,2}(?=[^\""]))*(?:\""\""\""|$)|\'(?:[^\\\']|\\[\s\S])*(?:\'|$)|\""(?:[^\\\""]|\\[\s\S])*(?:\""|$))/,b,'\'\""']):a.multiLineStrings?c.push(['str',/^(?:\'(?:[^\\\']|\\[\s\S])*(?:\'|$)|\""(?:[^\\\""]|\\[\s\S])*(?:\""|$)|\`(?:[^\\\`]|\\[\s\S])*(?:\`|$))/,b,'\'\""`']):c.push(['str',/^(?:\'(?:[^\\\'\r\n]|\\.)*(?:\'|$)|\""(?:[^\\\""\r\n]|\\.)*(?:\""|$))/,b,'\""\'']),a.verbatimStrings&&d.push(['str',/^@\""(?:[^\""]|\""\"")*(?:\""|$)/,b]),a.hashComments&&(a.cStyleComments?(c.push(['com',/^#(?:(?:define|elif|else|endif|error|ifdef|include|ifndef|line|pragma|undef|warning)\b|[^\r\n]*)/,b,'#']),d.push(['str',/^<(?:(?:(?:\.\.\/)*|\/?)(?:[\w-]+(?:\/[\w-]+)+)?[\w-]+\.h|[a-z]\w*)>/,b])):c.push(['com',/^#[^\r\n]*/,b,'#'])),a.cStyleComments&&(d.push(['com',/^\/\/[^\r\n]*/,b]),d.push(['com',/^\/\*[\s\S]*?(?:\*\/|$)/,b])),a.regexLiterals&&(e='/(?=[^/*])(?:[^/\\x5B\\x5C]|\\x5C[\\s\\S]|\\x5B(?:[^\\x5C\\x5D]|\\x5C[\\s\\S])*(?:\\x5D|$))+/',d.push(['lang-regex',new RegExp('^'+m+'('+e+')')])),f=a.keywords.replace(/^\s+|\s+$/g,''),f.length&&d.push(['kwd',new RegExp('^(?:'+f.replace(/\s+/g,'|')+')\\b'),b]),c.push(['pln',/^\s+/,b,' \r\n	\xa0']),d.push(['lit',/^@[a-z_$][a-z_$@0-9]*/i,b],['typ',/^@?[A-Z]+[a-z][A-Za-z_$@0-9]*/,b],['pln',/^[a-z_$][a-z_$@0-9]*/i,b],['lit',new RegExp('^(?:0x[a-f0-9]+|(?:\\d(?:_\\d+)*\\d*(?:\\.\\d*)?|\\.\\d\\+)(?:e[+\\-]?\\d+)?)[a-z]*','i'),b,'0123456789'],['pun',/^.[^\s\w\.$@\'\""\`\/\#]*/,b]),S(c,d)}s=T({keywords:l,hashComments:a,cStyleComments:a,multiLineStrings:a,regexLiterals:a});function U(c){var d=c.source,e=c.extractedTags,f=c.decorations,g=[],h=0,i=b,j=b,k=0,l=0,m=O(window.PR_TAB_WIDTH),n=/([\r\n ]) /g,o=/(^| ) /gm,p=/\r\n?|\n/g,q=/[ \r\n]$/,r=a,s;function t(a){var c,e;a>h&&(i&&i!==j&&(g.push('</span>'),i=b),!i&&j&&(i=j,g.push('<span class=\""',i,'\"">')),c=H(m(d.substring(h,a))).replace(r?o:n,'$1&nbsp;'),r=q.test(c),e=window._0()?'&nbsp;<br />':'<br />',g.push(c.replace(p,e)),h=a)}while(a){k<e.length?l<f.length?(s=e[k]<=f[l]):(s=a):(s=false);if(s)t(e[k]),i&&(g.push('</span>'),i=b),g.push(e[k+1]),k+=2;else if(l<f.length)t(f[l]),j=f[l+1],l+=2;else break}t(d.length),i&&g.push('</span>'),c.prettyPrintedHtml=g.join('')}t={};function V(a,b){var c,d;for(d=b.length;--d>=0;)c=b[d],t.hasOwnProperty(c)?'console'in window&&console.warn('cannot override language handler %s',c):(t[c]=a)}function W(a,b){return a&&t.hasOwnProperty(a)||(a=/^\s*</.test(b)?'default-markup':'default-code'),t[a]}V(s,['default-code']),V(S([],[['pln',/^[^<?]+/],['dec',/^<!\w[^>]*(?:>|$)/],['com',/^<\!--[\s\S]*?(?:-\->|$)/],['lang-',/^<\?([\s\S]+?)(?:\?>|$)/],['lang-',/^<%([\s\S]+?)(?:%>|$)/],['pun',/^(?:<[%?]|[%?]>)/],['lang-',/^<xmp\b[^>]*>([\s\S]+?)<\/xmp\b[^>]*>/i],['lang-js',/^<script\b[^>]*>([\s\S]*?)(<\/script\b[^>]*>)/i],['lang-css',/^<style\b[^>]*>([\s\S]*?)(<\/style\b[^>]*>)/i],['lang-in.tag',/^(<\/?[a-z][^<>]*>)/i]]),['default-markup','htm','html','mxml','xhtml','xml','xsl']),V(S([['pln',/^[\s]+/,b,' 	\r\n'],['atv',/^(?:\""[^\""]*\""?|\'[^\']*\'?)/,b,'\""\'']],[['tag',/^^<\/?[a-z](?:[\w.:-]*\w)?|\/?>$/i],['atn',/^(?!style[\s=]|on)[a-z](?:[\w:-]*\w)?/i],['lang-uq.val',/^=\s*([^>\'\""\s]*(?:[^>\'\""\s\/]|\/(?=\s)))/],['pun',/^[=<>\/]+/],['lang-js',/^on\w+\s*=\s*\""([^\""]+)\""/i],['lang-js',/^on\w+\s*=\s*\'([^\']+)\'/i],['lang-js',/^on\w+\s*=\s*([^\""\'>\s]+)/i],['lang-css',/^style\s*=\s*\""([^\""]+)\""/i],['lang-css',/^style\s*=\s*\'([^\']+)\'/i],['lang-css',/^style\s*=\s*([^\""\'>\s]+)/i]]),['in.tag']),V(S([],[['atv',/^[\s\S]+/]]),['uq.val']),V(T({keywords:d,hashComments:a,cStyleComments:a}),['c','cc','cpp','cxx','cyc','m']),V(T({keywords:'null true false'}),['json']),V(T({keywords:f,hashComments:a,cStyleComments:a,verbatimStrings:a}),['cs']),V(T({keywords:e,cStyleComments:a}),['java']),V(T({keywords:k,hashComments:a,multiLineStrings:a}),['bsh','csh','sh']),V(T({keywords:i,hashComments:a,multiLineStrings:a,tripleQuotedStrings:a}),['cv','py']),V(T({keywords:h,hashComments:a,multiLineStrings:a,regexLiterals:a}),['perl','pl','pm']),V(T({keywords:j,hashComments:a,multiLineStrings:a,regexLiterals:a}),['rb']),V(T({keywords:g,cStyleComments:a,regexLiterals:a}),['js']),V(S([],[['str',/^[\s\S]+/]]),['regex']);function X(a){var b=a.sourceCodeHtml,c=a.langExtension,d,e;a.prettyPrintedHtml=b;try{e=P(b),d=e.source,a.source=d,a.basePos=0,a.extractedTags=e.tags,W(c,d)(a),U(a)}catch(f){'console'in window&&(console.log(f),console.trace())}}function Y(a,b){var c={sourceCodeHtml:a,langExtension:b};return X(c),c.prettyPrintedHtml}function Z(c){var d=window._0(),e=d===6?'\r\n':'\r',f=[document.getElementsByTagName('pre'),document.getElementsByTagName('code'),document.getElementsByTagName('xmp')],g=[],h,i,j,k,l,m;for(i=0;i<f.length;++i)for(j=0,l=f[i].length;j<l;++j)g.push(f[i][j]);f=b,h=Date,h.now||(h={now:function(){return(new Date).getTime()}}),k=0;function n(){var b=window.PR_SHOULD_USE_CONTINUATION?h.now()+250:Infinity,d,e,f,i,j;for(;k<g.length&&h.now()<b;++k){e=g[k];if(e.className&&e.className.indexOf('prettyprint')>=0){f=e.className.match(/\blang-(\w+)\b/),f&&(f=f[1]),i=false;for(j=e.parentNode;j;j=j.parentNode)if((j.tagName==='pre'||j.tagName==='code'||j.tagName==='xmp')&&j.className&&j.className.indexOf('prettyprint')>=0){i=a;break}i||(d=N(e),d=d.replace(/(?:\r\n?|\n)$/,''),m={sourceCodeHtml:d,langExtension:f,sourceNode:e},X(m),o())}}k<g.length?setTimeout(n,250):c&&c()}function o(){var a=m.prettyPrintedHtml,b,c,f,g,h,i,j,k;if(!a)return;f=m.sourceNode;if(!J(f))f.innerHTML=a;else{k=document.createElement('PRE');for(g=0;g<f.attributes.length;++g)b=f.attributes[g],b.specified&&(c=b.name.toLowerCase(),c==='class'?(k.className=b.value):k.setAttribute(b.name,b.value));k.innerHTML=a,f.parentNode.replaceChild(k,f),f=k}if(d&&f.tagName==='PRE'){j=f.getElementsByTagName('br');for(h=j.length;--h>=0;)i=j[h],i.parentNode.replaceChild(document.createTextNode(e),i)}}n()}window.PR_normalizedHtml=L,window.prettyPrintOne=Y,window.prettyPrint=Z,window.PR={combinePrefixPatterns:M,createSimpleLexer:S,registerLangHandler:V,sourceDecorator:T,PR_ATTRIB_NAME:'atn',PR_ATTRIB_VALUE:'atv',PR_COMMENT:'com',PR_DECLARATION:'dec',PR_KEYWORD:'kwd',PR_LITERAL:'lit',PR_NOCODE:'nocode',PR_PLAIN:'pln',PR_PUNCTUATION:'pun',PR_SOURCE:'src',PR_STRING:'str',PR_TAG:'tag',PR_TYPE:'typ'}})()
//-->
</script>
<script type=""text/javascript""><!--
function toggle(id) {
  var e = document.getElementById(id);
  if (e) {
    if (e.tagName=='DIV') e.style.display = (e.style.display == 'inline-block' ? 'none' : 'inline-block');
    else                  e.style.display = (e.style.display == 'inline' ? 'none' : 'inline');
    return false;
  }
  return true;
}
//-->
</script>
<style type=""text/css"">
*{padding:0;margin:0}
body{background:#f5f4f0;color:#111;font:100%/1.33 Calibri,Tahoma,Geneva,""Bitstream Vera Sans"",sans-serif;padding:0.5em 1em 1em}
pre,tt,code,kbd,samp{font-size:93%;font-family:Consolas,""Lucida Console"",Monaco,""Bitstream Vera Sans Mono"",monospace}
#header{background:#ebe8e5;padding:0.25em 1em 0.375em;min-width:0;margin:-0.25em -0.75em 0.5em;border:1px solid;border-color:#fff #ccf #ccf #fff}
#header dt{float:left;margin:0 0.25em 0 0}
#header dd{font-weight:bold;margin:0 0 0 2em}
#header .from,#header .tags,#header .posted, #header .language {font-size:87.5%}

ins{background:#dfd;color:#050;text-decoration:none}
ins.selection{background:#acf;color:#000;text-decoration:none}
del{background:#fee;color:#911;text-decoration:line-through}
.error{color:red}

span.sc{font-variant:small-caps}
code{background:#e0e0e6;color:#000;padding:0 0.25em}
pre{background:#f0f0f4;color:#000;border:1px dashed #99e;padding:0.5em 1em;margin:0.25em 0;font-size:81.25%}
code.nott,pre.nott{font-family:inherit;font-family:expression(this.parentNode.currentStyle.fontFamily)}
.str{color:#080}.kwd{color:#00b;font-weight:bold}.com{color:#678;font-style:italic}.typ{color:#606}.lit{color:#910}.pun{color:#660}.pln{color:#000}.tag{color:#909}.atn{color:#c60}.atv{color:#00c}.dec{color:#606}

ul,ol{padding-left:2.5em}
li{color:#669} li>*{color:#111}

.cite{background:#e9eef8;border-left:#44f 2px solid;padding:0 0.2em 0 0.35em;margin:0.1em 0 0 0.35em;display:inline-block}
.cite .cite{background:#dee2f3}
.cite .cite .cite{background:#d4daf0}
.cite .cite .cite .cite{background:#cad2ec}

a{color:#06C;text-decoration:underline}
a.local{color:#083}
a.toggle{color:#083;border:solid 1px #083;text-decoration:none;padding:0 0.25em}
:link:hover{text-decoration:none}
a.toggle:hover{color:#fff;background:#083}

/*
a[href$="".pdf""]{padding-left:18px;background:url(data:image/png;base64,R0lGODlhDwAPAPEAAGAAgAAAAP/gQLukLCH5BAEAAAAALAAAAAAPAA8AAAI1hB2Zx5Aj4lgswIjFPDb77WCBKDqXMEaplmTrisKeK89qh3rp9iovcpqBTLYhAscKNDi+RgEAOw==) left center no-repeat}
*/

hr.h0,hr.h1,hr.h2,hr.h3,hr.h4{color:#000;background:#000;height:1px;border:none}
hr.h0{}
hr.h1{height:2px}
hr.h2{color:#666;background:#666}
hr.h3{color:#bbb;background:#bbb}
hr.h4{color:#bbb;background:#bbb;border-style:dotted}

h1,h2,h3,h4,h5,h6{color:#213449;line-height:1.1em}
h1 b,h2 b,h3 b,h4 b,h5 b,h6 b,h1 strong,h2 strong,h3 strong,h4 strong,h5 strong,h6 strong{font-weight:normal}
h1{font-size:2em}
h2{font-size:1.625em}
h3{font-size:1.375em}
h4{font-size:1.125em}
h5{font-size:0.9375em}
h6{font-size:0.8125em}
h1.underlined{border-bottom:2px solid #000;margin-bottom:0.25em}
h2.underlined{border-bottom:1px solid #666;margin-bottom:0.3em}
h3.underlined{border-bottom:1px solid #bbb;margin-bottom:0}
h4.underlined{border-bottom:1px dotted #bbb;margin-bottom:0}

div.comanote{background:#fcf4c5;border:1px solid #e5d78c;font-size:87.5%;padding:0;margin:0.33em 0}
div.comanoteheader{padding:0 0.125em;font-style:italic;background:#e5d78c}
div.comanotebody{margin:0;padding:0.25em}

.left,div.left{float:left;max-width:50%;margin:0 0.5em 0 0}
.right,div.right{float:right;max-width:50%;margin:0 0 0 0.5em}
.here{}
div.here{clear:both;display:inline-block}

.hidden,div.hidden{display:none}
span[title]{border-bottom:1px dotted #000}
</style>
<body onload=""prettyPrint()"">
";
    }
      public static string GetCiteHTMLFooter()
      {
          return @"</body></html>";
      }
#endregion
  }

  public class ColaboCredentialCache
  {
    CredentialCache _credentialCache = new CredentialCache();
    public Hashtable _colaboCache = new Hashtable();

    public CredentialCache credentialCache
    {
      get { return _credentialCache; }
    }
    public Hashtable colaboCache
    {
      get { return _colaboCache; }
    }

    public ColaboCredentialCache()
    {
    }

    public void Update(Uri uriPrefix, string realm, string authType, NetworkCredential cred)
    {
      // update the CredentialCache
      _credentialCache.Remove(uriPrefix, authType);
      _credentialCache.Add(uriPrefix, authType, cred);

      // update the internal hashtable
      CredentialKey key = new CredentialKey(uriPrefix, realm, authType);
      _colaboCache.Remove(key);
      _colaboCache.Add(key, cred);
    }

    public void Refresh(Uri uriPrefix, string authType, NetworkCredential cred)
    {
      // update CredentialCache when search in hashtable succeeded
      _credentialCache.Remove(uriPrefix, authType);
      _credentialCache.Add(uriPrefix, authType, cred);
    }

    public void Remove(Uri uriPrefix, string realm, string authType)
    {
      // update the CredentialCache
      _credentialCache.Remove(uriPrefix, authType);

      // update the internal hashtable
      CredentialKey key = new CredentialKey(uriPrefix, realm, authType);
      _colaboCache.Remove(key);
    }

    public bool FindCredentials(Uri urlPrefix, string realm, string authType, out string user, out string password)
    {
      CredentialKey key = new CredentialKey(urlPrefix, realm, authType);
      if (_colaboCache.ContainsKey(key))
      {
        NetworkCredential item = (NetworkCredential)_colaboCache[key];
        user = item.UserName;
        password = item.Password;
        return true;
      }
      else
      {
        user = password = "";
        return false;
      }
    }

#region Debugging helpers
    public void DebugMe(Uri url)
    {
      System.Diagnostics.Debug.Print("Colabo credentials Debug - before accessing {0}", url.ToString());
      IEnumerator listCredentials = _colaboCache.GetEnumerator();
      while (listCredentials.MoveNext())
      {
        DictionaryEntry auth = (DictionaryEntry)listCredentials.Current;
        Colabo.CredentialKey colKey = (Colabo.CredentialKey)auth.Key;
        NetworkCredential colCred = (NetworkCredential)auth.Value;
        System.Diagnostics.Debug.Print("  {0}:{1} - {2} - {3}", colKey.UriPrefix.ToString(), colKey.Realm, colCred.UserName, colCred.Password.Length+100);
      }
      IEnumerator credentialList = _credentialCache.GetEnumerator();
      int count = 0;
      while (credentialList.MoveNext()) count++;
      System.Diagnostics.Debug.Print("  * number of items in credentialCache: {0}", count);
    }
#endregion
  }

  internal class CredentialKey
  {
    public Uri UriPrefix;
    public string Realm;
    internal int UriPrefixLength = -1;
    internal string AuthenticationType;

    internal CredentialKey(Uri uriPrefix, string realm, string authenticationType)
    {
      UriPrefix = uriPrefix;
      Realm = realm;
      UriPrefixLength = UriPrefix.ToString().Length;
      AuthenticationType = authenticationType;
    }

    internal bool Match(Uri uri, string realm, string authenticationType)
    {
      if (uri == null || authenticationType == null)
      {
        return false;
      }
      //
      // If the protocols dont match this credential
      // is not applicable for the given Uri
      //
      if (string.Compare(authenticationType, AuthenticationType, StringComparison.OrdinalIgnoreCase) != 0)
      {
        return false;
      }
      if (realm != Realm)
      {
        return false;
      }

      return IsPrefix(uri, UriPrefix);
    }

    //
    // IsPrefix (Uri)
    //
    // Determines whether <prefixUri> is a prefix of this URI. A prefix
    // match is defined as:
    //
    // scheme match
    // + host match
    // + port match, if any
    // + <prefix> path is a prefix of <URI> path, if any
    //
    // Returns:
    // True if <prefixUri> is a prefix of this URI
    //
    internal bool IsPrefix(Uri uri, Uri prefixUri)
    {
       if (prefixUri.Scheme != uri.Scheme || prefixUri.Host != uri.Host || prefixUri.Port != uri.Port)
           return false;
      
       int prefixLen = prefixUri.AbsolutePath.LastIndexOf('/');
       if (prefixLen > uri.AbsolutePath.LastIndexOf('/'))
           return false;
      
       return String.Compare(uri.AbsolutePath, 0, prefixUri.AbsolutePath, 0, prefixLen, StringComparison.OrdinalIgnoreCase) == 0;
    }

    private int m_HashCode = 0;
    private bool m_ComputedHashCode = false;
    public override int GetHashCode()
    {
      if (!m_ComputedHashCode) {
        //
        // compute HashCode on demand
        //

        m_HashCode = AuthenticationType.ToUpperInvariant().GetHashCode() + UriPrefixLength + UriPrefix.GetHashCode() + Realm.GetHashCode();
        m_ComputedHashCode = true;
      }

      return m_HashCode;
    }

    public override bool Equals(object comparand)
    {
      CredentialKey comparedCredentialKey = comparand as CredentialKey;
      
      if (comparand == null) {
        //
        // this covers also the compared==null case
        //
        return false;
      }

      bool equals = (
        string.Compare(AuthenticationType, comparedCredentialKey.AuthenticationType, StringComparison.OrdinalIgnoreCase) == 0) &&
        UriPrefix.Equals(comparedCredentialKey.UriPrefix) &&
        Realm.Equals(comparedCredentialKey.Realm);

      return equals;
    }

    public override string ToString()
    {
      return "[" + UriPrefixLength.ToString(NumberFormatInfo.InvariantInfo) + "]:" + UriPrefix.ToString() + ":" + Realm + ":" + AuthenticationType.ToString();
    }
  }
}
