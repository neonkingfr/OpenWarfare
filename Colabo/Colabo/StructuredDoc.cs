﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using Jayrock.Json;
using Jayrock.Json.Conversion;

namespace Colabo
{

  public interface StructuredNode
  {
    StructuredDoc CreateChild(string name);
    StructuredNode GetChild(string name);

    void SetAttribute(string name, string value);
    string GetAttribute(string name);

    void SetValue(string p);
    string GetValue();

    List<StructuredNode> GetChildArray(string name);

    StructuredNode AppendChildArray(string name);
  }
  public interface StructuredDoc : StructuredNode
  {
    void Save(string draft);
    string Export();
  }

  public class DocXML : StructuredDoc
  {
    XmlDocument doc_;
    XmlElement node_;

    public DocXML() { doc_ = new XmlDocument(); node_ = null; }
    private DocXML(XmlDocument doc, XmlElement el) { doc_ = doc; node_ = el; }
    public StructuredDoc CreateChild(string name)
    {
      XmlElement cls = node_!=null ? (XmlElement)node_.SelectSingleNode(name) : null;
      if (cls==null)
      {
        cls = doc_.CreateElement(name);
        if (node_ != null) node_.AppendChild(cls);
        else doc_.AppendChild(cls);
      }

      return new DocXML(doc_, cls);
    }
    public StructuredNode AppendChildArray(string name)
    {
      XmlElement cls = doc_.CreateElement(name);
      if (node_ != null) node_.AppendChild(cls);
      else doc_.AppendChild(cls);
      return new DocXML(doc_, cls);
    }

    public StructuredNode GetChild(string name)
    {
      XmlElement node = (XmlElement)node_.SelectSingleNode(name);
      if (node!=null) return new DocXML(doc_,node);
      return null;
    }
    public List<StructuredNode> GetChildArray(string name)
    {
      XmlNodeList nodes = node_.SelectNodes(name);
      if (nodes == null) return null;
      var ret = new List<StructuredNode>();
      foreach (XmlNode value in nodes)
      {
        XmlElement el = (XmlElement)value;
        if (el!=null) ret.Add(new DocXML(doc_, el));
      }
      return ret;
    }

    public void SetAttribute(string name, string value) { node_.SetAttribute(name, value); }
    public string GetAttribute(string name) { return node_.GetAttribute(name); }
    public void SetValue(string text)
    {
      var node = doc_.CreateTextNode(text);
      node_.AppendChild(node);
    }
    public string GetValue()
    {
      // alternative solution using XPath: return node.SelectSingleNode("text()").InnerText;
      // see http://stackoverflow.com/questions/4714962/xmlnode-innertext
      foreach (XmlNode child in node_.ChildNodes)
      {
        if (child.NodeType == XmlNodeType.Text || child.NodeType == XmlNodeType.CDATA)
        {
          return child.Value;
        }
      }
      return "";

    }
    public void Save(string filename)
    {
      Directory.CreateDirectory(Path.GetDirectoryName(filename));
      doc_.Save(filename);
    }
    public string Export() { return null; }

    public static DocXML Load(string path, string rootNode)
    {
      DocXML doc = new DocXML();
      try
      {
        doc.doc_.Load(path);
      }
      catch (System.Exception)
      {
        return null;
      }
      if (rootNode != null)
      {
        doc.node_ = (XmlElement)doc.doc_.SelectSingleNode(rootNode);
        if (doc.node_ == null) return null;
      }
      return doc;
    }
  }

  internal class DocJSON : StructuredDoc
  {
    JsonObject doc_;
    JsonObject node_;

    public DocJSON() { doc_ = new JsonObject(); node_ = doc_; }
    public DocJSON(JsonObject doc) { doc_ = doc; node_ = doc; }
    private DocJSON(JsonObject doc, JsonObject el) { doc_ = doc; node_ = el; }
    public StructuredDoc CreateChild(string name)
    {
      JsonObject child = new JsonObject();
      node_.Put(name, child);

      return new DocJSON(doc_, child);
    }
    public StructuredNode GetChild(string name)
    {
      JsonObject node = (JsonObject)node_[name];
      if (node==null) return null;
      return new DocJSON(doc_,node);
    }
    public List<StructuredNode> GetChildArray(string name)
    {
      JsonArray node = (JsonArray)node_[name];
      if (node == null) return null;
      var ret = new List<StructuredNode>();
      foreach (JsonObject value in node)
      {
        ret.Add(new DocJSON(doc_,value));
      }
      return ret;
    }
    public StructuredNode AppendChildArray(string name)
    {
      JsonArray node = (JsonArray)node_[name];
      if (node == null)
      {
        node = new JsonArray();
        node_.Put(name, node);
      }
      var obj = new JsonObject();
      node.Put(obj);
      return new DocJSON(doc_, obj);
    }
    public void SetAttribute(string name, string value) { node_.Put(name, value); }
    public string GetAttribute(string name) { return node_[name].ToString(); }
    public void SetValue(string text) { node_.Put("_", text); }
    public string GetValue()
    {
      try
      {

        var value = node_["_"];
        return value.ToString();
      }
      catch
      { // no "value" node found, return empty string
        return "";
      }
    }
    public string Export()
    {
      return JsonConvert.ExportToString(doc_);
    }
    public void Save(string filename)
    {
      Directory.CreateDirectory(Path.GetDirectoryName(filename));
      System.IO.File.WriteAllText(filename, Export());
    }

    public JsonObject doc { get { return doc_; } }
  }

}