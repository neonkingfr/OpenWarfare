using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.DirectoryServices.Protocols;

namespace Colabo
{
  public partial class DialogDatabaseUser : Form
  {
    // user properties
    private ListBox.ObjectCollection _items;
    private int _index;
    
    // LDAP server access
    private string _host;
    private int _port;
    private string _user;
    private string _password;

    private DialogDatabaseManagement.UserInfo _result;
    public DialogDatabaseManagement.UserInfo result { get { return _result; } }

    public DialogDatabaseUser(ListBox.ObjectCollection items, int index, string host, int port, string user, string password)
    {
      _items = items;
      _index = index;
      _host = host;
      _port = port;
      _user = user;
      _password = password;

      InitializeComponent();
    }

    private void buttonOK_Click(object sender, EventArgs e)
    {
      // check if the data are valid
      string dn = valueUserDN.Text;
      // check for dn duplicity
      for (int i = 0; i < _items.Count; i++)
      {
        if (i != _index && ((DialogDatabaseManagement.UserInfo)_items[i])._dn.Equals(dn, StringComparison.OrdinalIgnoreCase))
        {
          MessageBox.Show("This user is already in the list.", "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Information);
          DialogResult = DialogResult.None;
          return;
        }
      }
      // find the LDAP entry
      string uid = null;

      LdapConnection connection = Configuration.OpenLDAPConnection(_host, _port, _user, _password);
      if (connection == null)
      {
        DialogResult = DialogResult.None;
        return;
      }

      try
      {
        // access the database properties
        SearchRequest request = new SearchRequest(dn, "(objectClass=*)", SearchScope.Base, new String[] {"uid"});
        SearchResponse response = (SearchResponse)connection.SendRequest(request);

        if (response.Entries.Count == 1)
        {
          SearchResultEntry entry = response.Entries[0];
          DirectoryAttribute attributes = entry.Attributes["uid"];
          if (attributes != null && attributes.Count == 1) uid = attributes[0].ToString();
        }
      }
      catch {}

      if (string.IsNullOrEmpty(uid))
      {
        MessageBox.Show("This user was not found on LDAP server.", "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        DialogResult = DialogResult.None;
        return;
      }

      _result = new DialogDatabaseManagement.UserInfo();
      _result._dn = dn;
      _result._name = uid;
      _result._accessRights = valueAccessRights.Text;
    }

    private void DialogDatabaseUser_Load(object sender, EventArgs e)
    {
      if (_index >= 0)
      {
        DialogDatabaseManagement.UserInfo user = (DialogDatabaseManagement.UserInfo)_items[_index];
        valueUserDN.Text = user._dn;
        valueAccessRights.Text = user._accessRights;
      }
    }
  }
}