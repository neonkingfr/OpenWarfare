using System;
using System.Threading;
using System.Windows.Forms;
using System.Collections.Generic;

using System.IO;
using System.Text;

using Token = Antlr.Runtime.IToken;

using System.Net;
using Jayrock.Json;
using Jayrock.Json.Conversion;
using Colabo.UserActivityMonitor;
using System.Diagnostics;

namespace Colabo
{
  /// Implementation of DBEngine using the Web Services
  public class WebServicesEngine : DBEngine
  {
    public WebServicesEngine(DBInfo info)
      : base(info)
    {
    }

    public override void OnClose()
    {
      if (_presenceThread != null)
      {
        ChangePresenceState("Offline");

        _presenceTerminate.Set();
        _presenceThread.Join();
      }
    }
    private Thread _presenceThread;
    private ManualResetEvent _presenceTerminate = new ManualResetEvent(false);

    private string _stationGUID;

   public void PresenceDoWork(string username, NotifyUserChange notifyUserChange, NotifyChat notifyChat)
   { // TODO: remove notifyLocationChange
     // repeat long-poll request until requested to terminate
     DateTime lastRequestStarted = DateTime.Now;
     while (true)
     {
       string content = "";
       DateTime now = DateTime.Now;
       TimeSpan lastRequestDuration = now - lastRequestStarted;
       lastRequestStarted = now;

       TimeSpan minRequestDelay = new TimeSpan(0, 0, 1);
       TimeSpan toWait = minRequestDelay - lastRequestDuration;
       if (toWait.CompareTo(new TimeSpan(0)) < 0) toWait = new TimeSpan(0);

       if (_presenceTerminate.WaitOne(toWait)) break;

       try
       {
         // TODO: pass reference state so that WebServices can return a delta
         WebRequest request = CreateWebRequest("DB/IMEvents/" + username + "?guid=" + _stationGUID, "GET", null);

         using (WebResponse resp = request.GetResponse())
         {
           using (Stream stream = resp.GetResponseStream())
           {
             using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
             {
               content = reader.ReadToEnd();
             }
           }
         }
       }
       catch (WebException)
       {

       }

       if (_presenceTerminate.WaitOne(0)) break;

       if (!String.IsNullOrEmpty(content))
       {
         try
         {
           JsonObject newState = (JsonObject)JsonConvert.Import(content);
           
           object obj = newState["Users"];
           if (obj is JsonArray)
           {
             bool hasPresence;
             UserList users = LoadUserList((JsonArray)obj, out hasPresence);
             notifyUserChange(users);
           }

           object objChats = newState["chatToRead"];
           if (obj is JsonArray)
           {
             // any new chat to read: update or open a window
             JsonArray chats = (JsonArray)objChats;
             for (int i=0; i<chats.Count; i++)
             {
               JsonObject chat = chats.GetObject(i);
               var data = new ChatConversation.ChatData();
               data.id = ReadInt(chat, "id", 0);
               data.revToRead = ReadInt(chat,"revision",0);

               JsonArray users = (JsonArray)chat["users"];
               if (users!=null)
               {
                 data.users = new ChatConversation.ChatData.UserList();

                 foreach (JsonObject user in users)
                 {
                   var u = new ChatConversation.ChatData.UserState();
                   var name = ReadString(user, "user");
                   u.state = ReadString(user, "state");
                   u.stateRev = ReadInt(user, "stateRev", 0);
                   u.stateTime = ReadDate(user, "stateTime", DateTime.MinValue);
                   data.users.Add(name,u);
                 }
               }


               notifyChat(data);
             }
           }
         }
         catch (JsonException ex)
         {
           Program.Log("IMEvents failed: "+ ex.ToString());
         }
       }
     }
   }

   public void SetLocation(string guid)
   {
     _stationGUID = guid;
     ResponseIgnored("DB/ReportIMLocation/" + _info._user + "/" + _stationGUID + @"/@?build=" + Revision.RevisionNo.ToString(), "PUT", null);
   }

   override public void SetLocationName(string ipAddr, string location)
   {
     if (_presenceThread == null) return;
     ResponseIgnored("DB/IMLocationName/" + ipAddr + "/" + location, "PUT", null);
   }

   private static void SendRequestIgnoreResult(WebRequest request)
   {
     ThreadPool.QueueUserWorkItem(
         (data) =>
         {
           try
           {
             using (WebResponse response = request.GetResponse())
             {
             }
           }
           catch (WebException)
           {
           }
         });
   }

   override public void ChangePresenceState(string state)
   {
     if (_presenceThread == null) return;
     ResponseIgnored("DB/ReportIMState/" + _info._user + "/" + state + "/" + _stationGUID, "PUT", null);
   }

   override public int StartChat(string user, List<string> users)
   {
     if (_presenceThread == null) return -1;
     // StartChat currently unused
     try
     {
       JsonObject userList = new JsonObject();
       JsonArray ar = new JsonArray();
       foreach (var u in users)
       {
         ar.Add(u);
       }
       userList.Put("users",ar);
       string response = Response("DB/StartChat/" + user, "POST", JsonConvert.ExportToString(userList));
       if (response != null)
       {
         object obj = JsonConvert.Import(response);
         if (obj is JsonObject)
         {
           JsonObject root = (JsonObject)obj;
           int id = ReadInt(root, "ID", -1);
           return id;
         }
       }
       
     }
     catch (WebException)
     {
     }

     return -1;
   }

   override public void DeleteChat(int id)
   {
     if (_presenceThread == null) return;
     ResponseIgnored(string.Format("DB/DeleteChat/{0}", id), "PUT", null);
   }

   override public bool LeaveChat(int id)
   {
     if (_presenceThread == null) return false;
     try
     {
       string response = Response(string.Format("DB/LeaveChat/{0}", id), "PUT", null);
       if (response != null)
       {
         object obj = JsonConvert.Import(response);
         if (obj is JsonObject)
         {
           JsonObject root = (JsonObject)obj;
           if (root.Contains("activeUsers"))
           {
             // when all users left, we need to send the response
             return ReadInt(root, "activeUsers", 0)<=0;
           }
         }
       }
     }
     catch (WebException)
     {
     }
     return false;
   }

   override public int SendChatConversation(int id, string jsonChat)
   {
     if (_presenceThread == null) return -1;
     try
     {
       string response = Response(string.Format("DB/SendChat/{0}", id), "POST", jsonChat);

       if (response != null)
       {
         object obj = JsonConvert.Import(response);
         if (obj is JsonObject)
         {
           JsonObject root = (JsonObject)obj;
           if (root.Contains("id"))
           {
             return ReadInt(root, "id", -1);
           }
         }
         return id;
       }

     }
     catch (WebException)
     {
     }
     // TODO: handle errors. How?
     return -1;
   }

   override public void ReportTyping(int chat, bool typing)
   {
     ResponseIgnored(string.Format("DB/ReportChatTyping/{0}?typing={1}", chat, typing), "PUT", null);
   }
  override public ChatConversation LoadChatConversation(int id, bool active)
   {
     if (_presenceThread == null) return null;
     try
     {
       string response = Response(string.Format("DB/LoadChat/{0}?active={1}", id, active), "GET", null);

       if (response != null)
       {
         object obj = JsonConvert.Import(response);
         if (obj is JsonObject)
         {
           JsonObject root = (JsonObject)obj;
           if (root!=null)
           {
             var doc = new DocJSON(root);
             return new ChatConversation(doc);
           }
         }
       }

     }
     catch (WebException)
     {
     }
     // TODO: handle errors. How?
     return null;
   }

   override public List<int> LoadChats()
   {
     if (_presenceThread == null) return null;
     try
     {
       string response = Response("DB/ChatList", "GET", null);

       if (response != null)
       {
         object obj = JsonConvert.Import(response);
         if (obj is JsonObject)
         {
           JsonObject root = (JsonObject)obj;
           JsonArray chats = (JsonArray)root["Chats"];
           var chatIds = new List<int>();
           for (int i=0; i<chats.Count; i++)
           {
             chatIds.Add(chats.GetInt32(i));
           }
           return chatIds;
         }
       }

     }
     catch (JsonException)
     {
     }
     catch (WebException)
     {
     }
     // TODO: handle errors. How?
     return null;
  }

   override public bool LoadChatHeader(int chat, ArticleItem item)
   {
     if (_presenceThread == null) return false;
     try
     {
       string response = Response(string.Format("DB/ChatHeader/{0}", chat), "GET", null);

       if (response != null)
       {
         object obj = JsonConvert.Import(response);
         if (obj is JsonObject)
         {
           JsonObject root = (JsonObject)obj;
           JsonObject article = (JsonObject )root["Article"];
           item._parent = ReadInt(article, "parent", -1);
           item._title = ReadString(article, "title");
           item.RawURL = ReadString(article, "url");
           item._author = ReadString(article, "author");
           string tags = ReadString(article, "tags"); ;
           ArticleItem.ReadTags(tags, out item._ownTags);
           item._date = ReadDate(article, "date", DateTime.MinValue);

           JsonArray users = (JsonArray)root["users"];
           item._chatUsers = new List<string>();
           for (int i=0; i<users.Count; i++)
           {
             item._chatUsers.Add(users.GetString(i));
           }
           return true;
         }
       }

     }
     catch (JsonException)
     {
     }
     catch (WebException)
     {
     }
     return false;
   }

   public override DBUserInfo LoadUserInfo()
   {
     string response = Response(string.Format("DB/UserInfo/{0}/", _info._user), "GET", null);

     if (response != null)
     {
       object obj = JsonConvert.Import(response);
       if (obj is JsonObject)
       {
         JsonObject root = (JsonObject)obj;
         DBUserInfo userInfo = new DBUserInfo();
         userInfo._fullName = ReadString(root, "FullName");
         userInfo._picture = ReadString(root, "Picture");
         userInfo._color = ReadInt(root, "Color", 0);
         userInfo._aliases = ReadString(root, "Aliases");
         return userInfo;
       }
     }

     return null;
   }

    public override void SaveUserInfo(DBUserInfo userInfo)
    {
      // PUT: DB/UserInfo/{user}/
      JsonObject request = new JsonObject();
      request.Put("FullName", userInfo._fullName);
      request.Put("Picture", userInfo._picture);
      request.Put("Color", userInfo._color);
      request.Put("Aliases", userInfo._aliases);

      Response(string.Format("DB/UserInfo/{0}/", _info._user), "PUT", JsonConvert.ExportToString(request));
    }

    public override void SaveUser(UserInfo userInfo)
    {
      // PUT: DB/User/{user}/
      JsonObject request = new JsonObject();
      request.Put("FullName", userInfo._fullName);
      request.Put("Picture", userInfo._picture);
      request.Put("Color", userInfo._color);

      Response(string.Format("DB/User/{0}/", _info._user), "PUT", JsonConvert.ExportToString(request));
    }

    private static UserList LoadUserList(JsonArray users, out bool hasPresence)
    {
      hasPresence = false;

      UserList retUsers = new UserList();

      foreach (object objItem in users)
      {
        if (!(objItem is JsonObject)) continue;

        // Find the user id
        JsonObject item = (JsonObject)objItem;
        string id = ReadString(item, "ID");
        if (string.IsNullOrEmpty(id)) continue; // required

        // load the rest of properties
        UserInfo userInfo = new UserInfo();
        userInfo._fullName = ReadString(item, "FullName");
        userInfo._picture = ReadString(item, "Picture");
        userInfo._color = ReadInt(item, "Color", 0);
        if (item.Contains("Presence")) hasPresence = true;
        userInfo._presence = ReadString(item, "Presence");
        userInfo._location = ReadString(item, "Location");
        userInfo._ipAddr = ReadString(item, "IPAddress");
        userInfo._build = ReadString(item, "Build");
        userInfo._presenceTime = ReadDate(item, "PresenceTime", DateTime.MinValue);
        retUsers.Add(id, userInfo);
      }

      return retUsers;

    }

    public override void LoadUsers(ref Dictionary<string, UserInfo> users, ref Dictionary<string, string> aliases, NotifyUserChange notifyUserChange, NotifyChat notifyChat)
    {
      // GET: DB/Users/
      string response = Response("DB/Users/", "GET", null);
      bool hasPresence = false;
      if (response != null)
      {
        object obj = JsonConvert.Import(response);
        if (obj is JsonObject)
        {
          JsonObject root = (JsonObject)obj;

          obj = root["Users"];
          if (obj is JsonArray)
          {
            users = LoadUserList((JsonArray)obj, out hasPresence);
          }

          obj = root["Aliases"];
          if (obj is JsonArray)
          {
            JsonArray array = (JsonArray)obj;
            foreach (object objItem in array)
            {
              if (!(objItem is JsonObject)) continue;

              // Find the user id
              JsonObject item = (JsonObject)objItem;
              string alias = ReadString(item, "Alias");
              string id = ReadString(item, "ID");
              if (!string.IsNullOrEmpty(alias) && !string.IsNullOrEmpty(id)) aliases.Add(alias, id);
            }
          }
        }

        if (users.Count == 0)
        {
          Program.Log("No users loaded for WebServices '" + _info._server + "', response "+ response);
        }
      }
      else
      {
        Program.Log("No Users response for WebServices '" + _info._server + "'");
      }
      Debug.Assert(users.Count > 0);
      Debug.Assert(_presenceThread == null);
      if (hasPresence)
      {
        SetLocation(Program.config.StationGUID);
        // make space for a long poll connection
        var point = ServicePointManager.FindServicePoint(new Uri(_info._server));
        point.ConnectionLimit++;
        _presenceThread = new Thread(new ThreadStart(delegate() { PresenceDoWork(_info._user, notifyUserChange, notifyChat); }));
        _presenceThread.IsBackground = true;
        _presenceThread.Start();
      }

    }

    public override void SaveUsers(List<ManageUserInfo> users, List<ManageGroupInfo> groups, List<ManageGroupUserInfo> groupUsers)
    {
      // PUT: DB/Users/
      JsonObject request = new JsonObject();

      // users
      JsonArray arrayUsers = new JsonArray();
      request.Put("Users", arrayUsers);
      foreach (ManageUserInfo user in users)
      {
        JsonObject obj = new JsonObject();
        arrayUsers.Put(obj);

        obj.Put("ID", user._id);
        obj.Put("Password", user._password);
        obj.Put("Access", user._access);
      }

      // groups
      JsonArray arrayGroups = new JsonArray();
      request.Put("Groups", arrayGroups);
      foreach (ManageGroupInfo group in groups)
      {
        JsonObject obj = new JsonObject();
        arrayGroups.Put(obj);

        obj.Put("ID", group._id);
        obj.Put("Name", group._name);
        obj.Put("Access", group._access);
        obj.Put("Level", group._level);
        obj.Put("Home", group._home);
      }

      // group users
      JsonArray arrayGroupUsers = new JsonArray();
      request.Put("GroupUsers", arrayGroupUsers);
      foreach (ManageGroupUserInfo info in groupUsers)
      {
        JsonObject obj = new JsonObject();
        arrayGroupUsers.Put(obj);

        obj.Put("Group", info._group);
        obj.Put("User", info._user);
      }

      Response("DB/Users/", "PUT", JsonConvert.ExportToString(request));
    }

    public override void LoadUserSettings(ref List<string> groups, ref List<Token> accessRights, ref int homepage)
    {
      // GET: DB/UserSettings/{user}/
      string response = Response(string.Format("DB/UserSettings/{0}/", _info._user), "GET", null);

      if (response != null)
      {
        object obj = JsonConvert.Import(response);
        if (obj is JsonObject)
        {
          JsonObject root = (JsonObject)obj;

          obj = root["Groups"];
          if (obj is JsonArray)
          {
            JsonArray array = (JsonArray)obj;
            foreach (object group in array)
            {
              string grp = group.ToString();
              if (!string.IsNullOrEmpty(grp)) groups.Add(grp);
            }
          }

          obj = root["Access"];
          if (obj is JsonArray)
          {
            JsonArray array = (JsonArray)obj;
            foreach (object rights in array)
            {
              if (rights == null) continue;
              string r = rights.ToString();
              if (!string.IsNullOrEmpty(r))
              {
                List<Token> set = FormColabo.CompileRuleSet(r);
                if (set != null) accessRights.AddRange(set);
              }
            }
          }

          int home = ReadInt(root, "Home", 0);
          if (home > 0) homepage = home;
        }
      }
    }

    public override int LoadHomepage()
    {
      // GET: DB/Homepage/{user}/
      string response = Response(string.Format("DB/Homepage/{0}/", _info._user), "GET", null);

      if (response != null)
      {
        object obj = JsonConvert.Import(response);
        if (obj is JsonObject)
        {
          JsonObject root = (JsonObject)obj;
          return ReadInt(root, "Homepage", -1);
        }
      }

      return -1;
    }

    public override void SaveHomepage(int homepage)
    {
      // PUT: DB/Homepage/{user}/
      JsonObject request = new JsonObject();
      request.Put("Homepage", homepage);

      Response(string.Format("DB/Homepage/{0}/", _info._user), "PUT", JsonConvert.ExportToString(request));
    }

    public override void LoadRuleSets(ref List<RuleSet> ruleSets)
    {
      // GET: DB/RuleSets/{user}/
      string response = Response(string.Format("DB/RuleSets/{0}/", _info._user), "GET", null);

      if (response != null)
      {
        object obj = JsonConvert.Import(response);
        if (obj is JsonArray)
        {
          JsonArray array = (JsonArray)obj;
          foreach (object objItem in array)
          {
            if (!(objItem is JsonObject)) continue;
            JsonObject item = (JsonObject)objItem;

            RuleSet ruleSet = new RuleSet();
            ruleSet._user = ReadString(item, "User");
            ruleSet._name = ReadString(item, "Name");
            ruleSet._expression = ReadString(item, "Expression");
            ruleSet._parentUser = ReadString(item, "ParentUser");
            ruleSet._parentName = ReadString(item, "ParentName");
            ruleSets.Add(ruleSet);
          }
        }
      }
    }

    public override void SaveRuleSets(ListBox.ObjectCollection ruleSets)
    {
      // PUT: DB/RuleSets/{user}/
      JsonArray request = new JsonArray();
      foreach (RuleSet ruleSet in ruleSets)
      {
        if (!ruleSet._user.Equals(_info._user)) continue;

        JsonObject obj = new JsonObject();
        request.Put(obj);

        obj.Put("Name", ruleSet._name);
        obj.Put("Expression", ruleSet._expression);
        obj.Put("ParentUser", ruleSet._parentUser);
        obj.Put("ParentName", ruleSet._parentName);
      }

      Response(string.Format("DB/RuleSets/{0}/", _info._user), "PUT", JsonConvert.ExportToString(request));
    }

    public override void LoadBookmarks(ref List<Bookmark> bookmarks, SQLDatabase database)
    {
      // GET: DB/Bookmarks/{user}/
      string response = Response(string.Format("DB/Bookmarks/{0}/", _info._user), "GET", null);

      if (response != null)
      {
        object obj = JsonConvert.Import(response);
        if (obj is JsonArray)
        {
          JsonArray array = (JsonArray)obj;
          foreach (object objItem in array)
          {
            if (!(objItem is JsonObject)) continue;
            JsonObject item = (JsonObject)objItem;

            Bookmark bookmark = new Bookmark(database);
            bookmark._id = ReadInt(item, "ID", -1);
            bookmark._name = ReadString(item, "Name");
            ArticleItem.ReadTags(ReadString(item, "Tags"), out bookmark._tags);
            bookmark._article = new ArticleId(database, ReadInt(item, "Article", -1));
            bookmark._image = ReadInt(item, "Image", 0);
            bookmark._parent = ReadInt(item, "Parent", -1);
            bookmarks.Add(bookmark);
          }
        }
      }
    }

    public override int SaveBookmark(Bookmark bookmark, SQLDatabase database)
    {
      // POST: DB/Bookmarks/{user}/
      JsonObject request = new JsonObject();
      request.Put("Name", bookmark._name);
      string tags = ArticleItem.WriteTags(bookmark._tags);
      request.Put("Tags", tags);
      int article = bookmark._article._database == database ? bookmark._article._id : -1;
      request.Put("Article", article);
      request.Put("Image", bookmark._image);
      request.Put("Parent", bookmark._parent);

      string response = Response(string.Format("DB/Bookmarks/{0}/", _info._user), "POST", JsonConvert.ExportToString(request));

      if (response != null)
      {
        object obj = JsonConvert.Import(response);
        if (obj is JsonObject)
        {
          JsonObject root = (JsonObject)obj;
          return ReadInt(root, "ID", -1);
        }
      }

      return -1;
    }

    public override void UpdateBookmark(Bookmark bookmark, SQLDatabase database)
    {
      // PUT: DB/Bookmark/{id}/
      JsonObject request = new JsonObject();
      request.Put("Name", bookmark._name);
      string tags = ArticleItem.WriteTags(bookmark._tags);
      request.Put("Tags", tags);
      int article = bookmark._article._database == database ? bookmark._article._id : -1;
      request.Put("Article", article);
      request.Put("Image", bookmark._image);
      request.Put("Parent", bookmark._parent);

      Response(string.Format("DB/Bookmark/{0:D}/", bookmark._id), "PUT", JsonConvert.ExportToString(request));
    }

    public override void DeleteBookmark(int id, int parent)
    {
      // DELETE: DB/Bookmark/{id}/
      JsonObject request = new JsonObject();
      request.Put("Parent", parent);

      Response(string.Format("DB/Bookmark/{0:D}/", id), "DELETE", JsonConvert.ExportToString(request));
    }

    public override void LoadScripts(ref List<Script> scripts)
    {
      // GET: DB/Scripts/{user}/
      string response = Response(string.Format("DB/Scripts/{0}/", _info._user), "GET", null);

      if (response != null)
      {
        object obj = JsonConvert.Import(response);
        if (obj is JsonArray)
        {
          JsonArray array = (JsonArray)obj;
          foreach (object objItem in array)
          {
            if (!(objItem is JsonObject)) continue;
            JsonObject item = (JsonObject)objItem;

            Script script = new Script();
            script._user = ReadString(item, "User");
            script._usage = ReadString(item, "Usage");
            script._name = ReadString(item, "Name");
            script._image = ReadString(item, "Image");
            script._language = ReadString(item, "Language");
            script._script = ReadString(item, "Script");
            scripts.Add(script);
          }
        }
      }
    }
    public override void SaveScripts(List<Script> scripts)
    {
      // PUT: DB/Scripts/{user}/
      JsonArray request = new JsonArray();
      foreach (Script script in scripts)
      {
        if (!script._user.Equals(_info._user)) continue;

        JsonObject obj = new JsonObject();
        request.Put(obj);

        obj.Put("User", script._user);
        obj.Put("Usage", script._usage);
        obj.Put("Name", script._name);
        obj.Put("Image", script._image);
        obj.Put("Language", script._language);
        obj.Put("Script", script._script);
      }

      Response(string.Format("DB/Scripts/{0}/", _info._user), "PUT", JsonConvert.ExportToString(request));
    }

    public override void LoadArticles(ref ArticlesCache cache, System.ComponentModel.BackgroundWorker worker)
    {
      // GET: DB/Articles/{user}/
      string response = Response(string.Format("DB/Articles/{0}/", _info._user), "GET", null);

      if (response != null)
      {
        object obj = JsonConvert.Import(response);
        if (obj is JsonObject)
        {
          JsonObject root = (JsonObject)obj;

          obj = root["Articles"];
          if (obj is JsonArray)
          {
            JsonArray array = (JsonArray)obj;
            foreach (object objItem in array)
            {
              if (!(objItem is JsonObject)) continue;
              JsonObject item = (JsonObject)objItem;

              ArticleItem article = new ArticleItem();
              article._id = ReadInt(item, "ID", -1);
              article._title = ReadString(item, "Title");
              article.RawURL = ReadString(item, "URL");
              article._parent = ReadInt(item, "Parent", -1);
              article._author = ReadString(item, "Author");
              article._date = ReadDate(item, "Date", DateTime.MinValue);
              article._revision = ReadInt(item, "Revision", -1);
              article._majorRevision = ReadInt(item, "MajorRevision", -1);
              cache.Add(article._id, article);
            }
          }

          obj = root["Tags"];
          if (obj is JsonArray)
          {
            JsonArray array = (JsonArray)obj;
            foreach (object objItem in array)
            {
              if (!(objItem is JsonObject)) continue;
              JsonObject item = (JsonObject)objItem;

              int id = ReadInt(item, "ID", -1);
              string tag = ReadString(item, "Tag").Trim();
              if (tag.Length > 0)
              {
                ArticleItem article;
                if (cache.TryGetValue(id, out article)) article.AddTag(tag.ToLower());
              }
            }
          }

          obj = root["PrivateTags"];
          if (obj is JsonArray)
          {
            JsonArray array = (JsonArray)obj;
            foreach (object objItem in array)
            {
              if (!(objItem is JsonObject)) continue;
              JsonObject item = (JsonObject)objItem;

              int id = ReadInt(item, "ID", -1);
              string tag = ReadString(item, "Tag").Trim();
              if (tag.Length > 0)
              {
                ArticleItem article;
                if (cache.TryGetValue(id, out article)) article.AddTag(ArticleItem.ToPrivateTag(tag.ToLower()));
              }
            }
          }

          obj = root["Read"];
          if (obj is JsonArray)
          {
            JsonArray array = (JsonArray)obj;
            foreach (object objItem in array)
            {
              if (!(objItem is JsonObject)) continue;
              JsonObject item = (JsonObject)objItem;

              int id = ReadInt(item, "ID", -1);
              int revision = ReadInt(item, "Revision", -1);
              ArticleItem article;
              if (cache.TryGetValue(id, out article))
              {
                article._toRead = revision < article._majorRevision;
                article._changed = revision < article._revision;
              }
            }
          }

          obj = root["Collapsed"];
          if (obj is JsonArray)
          {
            JsonArray array = (JsonArray)obj;
            foreach (object objItem in array)
            {
              int id = ReadInt(objItem, -1);
              ArticleItem item;
              if (cache.TryGetValue(id, out item)) item._expanded = false;
            }
          }
        }
      }
    }

    public override int SaveArticle(ArticleItem item)
    {
      // POST: DB/Articles/{user}/
      JsonObject request = new JsonObject();
      request.Put("ID", item._id);
      request.Put("Title", item._title);
      request.Put("URL", item.RawURL);
      request.Put("Author", item._author);
      request.Put("Date", item._date);
      request.Put("Revision", item._revision);
      request.Put("MajorRevision", item._majorRevision);
      request.Put("Parent", item._parent);
      request.Put("Changed", item._changed);

      JsonArray tags = new JsonArray();
      JsonArray privateTags = new JsonArray();
      for (int i = 0; i < item._ownTags.Count; i++)
      {
        string tag = item._ownTags[i].Trim();
        string privateTag = ArticleItem.FromPrivateTag(tag);
        if (privateTag == null)
        {
          if (tag.Length > 0) tags.Put(tag);
        }
        else
        {
          if (privateTag.Length > 0) privateTags.Put(privateTag);
        }
      }
      request.Put("Tags", tags);
      request.Put("PrivateTags", privateTags);

      string response = Response(string.Format("DB/Articles/{0}/", _info._user), "POST", JsonConvert.ExportToString(request));

      if (response != null)
      {
        object obj = JsonConvert.Import(response);
        if (obj is JsonObject)
        {
          JsonObject root = (JsonObject)obj;
          return ReadInt(root, "ID", -1);
        }
      }

      return item._id;
    }

    public override void DeleteArticle(ArticleItem item)
    {
      // DELETE: DB/Article/{id}/
      JsonObject request = new JsonObject();
      request.Put("Parent", item._parent);

      Response(string.Format("DB/Article/{0:D}/", item._id), "DELETE", JsonConvert.ExportToString(request));
    }

    public override int LoadArticleRead(int id)
    {
      // GET: DB/ArticleRead/{user}/{id}/
      string response = Response(string.Format("DB/ArticleRead/{0}/{1:D}/", _info._user, id), "GET", null);

      if (response != null)
      {
        object obj = JsonConvert.Import(response);
        if (obj is JsonObject)
        {
          JsonObject root = (JsonObject)obj;
          return ReadInt(root, "Revision", -1);
        }
      }

      return -1;
    }

    public override void SaveArticleRead(int id, int revision)
    {
      // PUT: DB/ArticleRead/{user}/{id}/
      JsonObject request = new JsonObject();
      request.Put("Revision", revision);

      Response(string.Format("DB/ArticleRead/{0}/{1:D}/", _info._user, id), "PUT", JsonConvert.ExportToString(request));
    }

    public override void DeleteArticleRead(int id)
    {
      // DELETE: DB/ArticleRead/{user}/{id}/
      Response(string.Format("DB/ArticleRead/{0}/{1:D}/", _info._user, id), "DELETE", null);
    }

    public override void SaveArticleCollapsed(int id)
    {
      // PUT: DB/ArticleCollapsed/{user}/{id}/
      Response(string.Format("DB/ArticleCollapsed/{0}/{1:D}/", _info._user, id), "PUT", null);
    }

    public override void DeleteArticleCollapsed(int id)
    {
      // DELETE: DB/ArticleCollapsed/{user}/{id}/
      Response(string.Format("DB/ArticleCollapsed/{0}/{1:D}/", _info._user, id), "DELETE", null);
    }

    public override int LoadRevision(int id)
    {
      // GET: DB/Revision/{id}/
      string response = Response(string.Format("DB/Revision/{0:D}/", id), "GET", null);

      if (response != null)
      {
        object obj = JsonConvert.Import(response);
        if (obj is JsonObject)
        {
          JsonObject root = (JsonObject)obj;
          return ReadInt(root, "Revision", int.MaxValue);
        }
      }

      return int.MaxValue;
    }

    public override void SaveRevisionAndDate(ArticleItem item)
    {
      // PUT: DB/Revision/{user}/{id}/
      JsonObject request = new JsonObject();
      request.Put("Revision", item._revision);
      request.Put("MajorRevision", item._majorRevision);
      request.Put("Date", item._date);
      request.Put("Changed", item._changed);

      Response(string.Format("DB/Revision/{0}/{1:D}/", _info._user, item._id), "PUT", JsonConvert.ExportToString(request));
    }

    public override void SaveParent(int id, int parent)
    {
      // PUT: DB/ArticleParent/{id}/
      JsonObject request = new JsonObject();
      request.Put("Parent", parent);

      Response(string.Format("DB/ArticleParent/{0:D}/", id), "PUT", JsonConvert.ExportToString(request));
    }

    public override void UpdateTag(int id, string addTag, string removeTag)
    {
      // PUT: DB/Tag/{user}/{id}/

      string removePrivateTag = null;
      string addPrivateTag = null;
      // check if tags are private
      if (removeTag != null)
      {
        removePrivateTag = ArticleItem.FromPrivateTag(removeTag);
        if (!string.IsNullOrEmpty(removePrivateTag)) removeTag = null;
      }
      if (addTag != null)
      {
        // add the tag
        addPrivateTag = ArticleItem.FromPrivateTag(addTag);
        if (!string.IsNullOrEmpty(addPrivateTag)) addTag = null;
      }

      JsonObject request = new JsonObject();
      if (!string.IsNullOrEmpty(removeTag)) request.Put("RemoveTag", removeTag);
      if (!string.IsNullOrEmpty(removePrivateTag)) request.Put("RemovePrivateTag", removePrivateTag);
      if (!string.IsNullOrEmpty(addTag)) request.Put("AddTag", addTag);
      if (!string.IsNullOrEmpty(addPrivateTag)) request.Put("AddPrivateTag", addPrivateTag);

      Response(string.Format("DB/Tag/{0}/{1:D}/", _info._user, id), "PUT", JsonConvert.ExportToString(request));
    }

    /*
    public override void SaveTags(ArticlesCache cache)
    {
      // PUT: DB/Tags/

    }
    */

    public override string GetArticleRevision(int id, int revision)
    {
      // GET: DB/ArticleContent/{id}/{revision}
      string response = Response(string.Format("DB/ArticleContent/{0:D}/{1:D}/", id, revision), "GET", null);

      if (response != null)
      {
        object obj = JsonConvert.Import(response);
        if (obj is JsonObject)
        {
          JsonObject root = (JsonObject)obj;
          return ReadString(root, "Content");
        }
      }

      return "";
    }

    public override bool IsFullTextIndexLocal()
    {
      return false;
    }
    public override string CheckFullTextIndex()
    {
      // GET: DB/FullText/CheckIndex/
      string response = Response("DB/FullText/CheckIndex/", "GET", null);

      if (response != null)
      {
        object obj = JsonConvert.Import(response);
        if (obj is JsonObject)
        {
          JsonObject root = (JsonObject)obj;
          return ReadString(root, "Result");
        }
      }

      return "";
    }
    public override List<int> FullTextRevisionsOfArticle(ArticleItem item)
    {
      System.Diagnostics.Trace.Fail("Indexing not supported for Web Services");
      return null;
    }
    public override bool FullTextNeedToIndexArticle(ArticleItem item, RevisionInfo revision)
    {
      System.Diagnostics.Trace.Fail("Indexing not supported for Web Services");
      return false;
    }
    public override void FullTextIndexArticle(ArticleItem item, RevisionInfo revision, string content)
    {
      System.Diagnostics.Trace.Fail("Indexing not supported for Web Services");
    }
    public override List<SearchResult> FullTextSearch(SQLDatabase database, string query)
    {
      // POST: DB/FullText/Search/
      JsonObject request = new JsonObject();
      if (!string.IsNullOrEmpty(query)) request.Put("Query", query);

      string response = Response("DB/FullText/Search/", "POST", JsonConvert.ExportToString(request));

      List<SearchResult> results = new List<SearchResult>();
      if (response != null)
      {
        object obj = JsonConvert.Import(response);
        if (obj is JsonArray)
        {
          JsonArray array = (JsonArray)obj;
          foreach (object objItem in array)
          {
            if (!(objItem is JsonObject)) continue;
            JsonObject item = (JsonObject)objItem;
            int id = ReadInt(item, "Id", -1);

            // need to find article in the database to check access
            ArticleItem article = null;
            if (database.cache.TryGetValue(id, out article) && article != null)
            {
              ArticleItem.VTCache cache = new ArticleItem.VTCache(database, article);
              if ((article.GetAccessRights(database, cache) & AccessRights.Read) != 0)
              {
                SearchResult result = new SearchResult();
                result._database = database;
                result._id = id;
                result._revision = ReadInt(item, "Revision", 0);
                result._author = ReadString(item, "Author");
                string time = ReadString(item, "Time");
                DateTime.TryParseExact(time, "yyyyMMddHHmmss", null, System.Globalization.DateTimeStyles.None, out result._time);
                result._title = ReadString(item, "Title");
                result._content = null;
                result._score = ReadFloat(item, "Score", 0);
                result._headRevision = result._revision >= article._revision;
                results.Add(result);
              }
            }
          }
        }
      }

      return results;
    }

    public override double Benchmark()
    {
      // GET: DB/Benchmark/
      string response = Response("DB/Benchmark/", "GET", null);

      if (response != null)
      {
        object obj = JsonConvert.Import(response);
        if (obj is JsonObject)
        {
          JsonObject root = (JsonObject)obj;
          return ReadInt(root, "QueryDuration", -1)*0.001;
        }
      }
      return -1;
    }

    // Communication test
    private void Test()
    {
      // create the input data
      JsonObject request = new JsonObject();
      request.Put("Name", "Value");
      string data = JsonConvert.ExportToString(request);

      string result = Response(_info._server + "DB/Users/", "POST", data);
      if (result != null)
      {
        JsonObject response = (JsonObject)JsonConvert.Import(result);
        Console.WriteLine(response.ToString());
      }
      else
      {
        Console.WriteLine("Error - no response");
      }
    }

    /// Helper to request Web Services and return the response
    private string Response(string url, string method, string data)
    {
      try
      {
        WebRequest request = CreateWebRequest(url, method, data);
        if (request == null) return null;

        try
        {
          return ReadResponse(request);
        }
        catch (WebException ex)
        {
          // the response may still be readable
          try
          {
            string response = ReadResponse(request);
            Program.Log("Web response to '" + url + "' failed with " + ex.ToString());
            Program.Log("Response:" + response);
          }
          catch
          { // ignore any exceptions reading the response, log the original exception
            Program.Log("Web response to '" + url + "' failed with " + ex.ToString());
            Program.Log("Response not read");
          }
        }

        // no exception
      }
      catch (Exception exception)
      {
        Program.Log("Web response to '" + url + "' failed with " + exception.ToString());
      }
      return null;
    }

    private static string ReadResponse(WebRequest request)
    {
      string content;
      using (WebResponse response = request.GetResponse())
      {
        Stream stream = response.GetResponseStream();
        StreamReader reader = new StreamReader(stream, Encoding.UTF8);
        content = reader.ReadToEnd();
        reader.Close();
        stream.Close();
      }

      return content;
    }

    private WebRequest CreateWebRequest(string url, string method, string data)
    {
      ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
      WebRequest request = WebRequest.Create(_info._server + url);

      // add authorization header
      string auth = _info._user + ":" + _info._password;
      string authHeader = "Authorization: Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes(auth));
      request.Headers.Add(authHeader);

      HttpWebRequest webRequest = (HttpWebRequest)request;
      if (webRequest == null) return null;
      // set additional properties
      webRequest.KeepAlive = false;
      webRequest.ProtocolVersion = HttpVersion.Version10;
      webRequest.Method = method;
      webRequest.ContentType = "application/json";

      // add the request content
      if (!string.IsNullOrEmpty(data))
      {
        byte[] bytes = new UTF8Encoding().GetBytes(data);

        webRequest.ContentLength = bytes.Length;

        Stream stream = webRequest.GetRequestStream();
        stream.Write(bytes, 0, bytes.Length);
        stream.Close();
      }
      request.PreAuthenticate = true;

      return request;
    }

    private bool ResponseIgnored(string url, string method, string data)
    {
      try
      {
        WebRequest request = CreateWebRequest(url, method, data);
        SendRequestIgnoreResult(request);
        return true;
      }
      catch (WebException)
      {
        return false;
      }
    }

    // helpers to read results from reader
    private static int ReadInt(JsonObject obj, string name, int defValue)
    {
      if (obj.Contains(name))
      {
        object item = obj[name];
        if (item is JsonNumber) return ((JsonNumber)item).ToInt32();
      }
      return defValue;
    }
    private static float ReadFloat(JsonObject obj, string name, float defValue)
    {
      if (obj.Contains(name))
      {
        object item = obj[name];
        if (item is JsonNumber) return ((JsonNumber)item).ToSingle();
      }
      return defValue;
    }
    private static string ReadString(JsonObject obj, string name)
    {
      if (obj.Contains(name)) return obj[name].ToString();
      return "";
    }
    private static DateTime ReadDate(JsonObject obj, string name, DateTime defValue)
    {
      if (obj.Contains(name))
      {
        object item = obj[name];
        DateTime result;
        if (DateTime.TryParse(item.ToString(), out result)) return result; 
      }
      return defValue;
    }
    private static int ReadInt(object item, int defValue)
    {
      if (item is JsonNumber) return ((JsonNumber)item).ToInt32();
      return defValue;
    }
    private static DateTime ReadDate(object item, DateTime defValue)
    {
      DateTime result;
      if (DateTime.TryParse(item.ToString(), out result)) return result;
      return defValue;
    }
  };
};