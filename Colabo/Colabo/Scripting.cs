using System;
using System.Collections.Generic;

using System.IO;

using System.Security.Permissions;

using Token = Antlr.Runtime.IToken;

namespace Colabo
{
  public interface IReloadPage
  {
    void ReloadPage();
  };

  // root mapped to window.external
  [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
  [System.Runtime.InteropServices.ComVisibleAttribute(true)]
  public class Scripting
  {
    private FormColabo _owner;
    private Dictionary<string, object> _variables;
    private IReloadPage _reloadPage;

    public Scripting(FormColabo owner, IReloadPage reloadPage) 
    {
      _owner = owner;
      _reloadPage = reloadPage;
      _variables = new Dictionary<string, object>();
    }

    public int NDatabases()
    {
      return _owner.databases.Count;
    }
    public ScriptingDatabase GetDatabase(int i)
    {
      if (i < 0 || i >= _owner.databases.Count) return null;
      return new ScriptingDatabase(_owner.databases[i]);
    }
    public ScriptingLog Log(string path, int days)
    {
      ScriptingLog log = new ScriptingLog();

      // convert parameters
      Uri url;
      try
      {
        url = new Uri(path);
      }
      catch //(System.Exception e)
      {
      	return log;
      }
      DateTime from = DateTime.Now.AddDays(-days);

      // SVN log
      SVNDatabase.GetLog(_owner, url, from,
        delegate(SharpSvn.SvnLogEventArgs args, string root)
        {
          log.Add(args, root);
        });
      return log;
    }
    public void ExportToFile(string filename, string content)
    {
      string dir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Colabo";
      if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
      StreamWriter writer = new StreamWriter(dir + "\\" + filename, false, System.Text.Encoding.UTF8);
      writer.Write(content);
      writer.Close();
    }
    public void ShowAsArticle(string content)
    {
      DialogArticle dialog = new DialogArticle(new ArticleItem(), _owner, content, null, DialogArticle.Mode.New);
      dialog.Show();
    }
    public string Prompt(string text, string value)
    {
      return DialogPrompt.Prompt(text, value);
    }

    /*
    public string ComaToHTML(string content)
    {
      MarkdownSharp.Markdown parser = new MarkdownSharp.Markdown();
      try
      {
        return parser.Transform(null, content);
      }
      catch // System.Exception e)
      {
        return null;
      }
    }
  */

    public void SetGlobalVariable(string name, object value)
    {
      _variables[name] = value;
    }
    public object GetGlobalVariable(string name)
    {
      object value = null;
      if (_variables.TryGetValue(name, out value)) return value;
      return null;
    }

    public void Reload()
    {
      _reloadPage.ReloadPage();
    }
  }

  // type Database
  [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
  [System.Runtime.InteropServices.ComVisibleAttribute(true)]
  public class ScriptingDatabase
  {
    private SQLDatabase _database;

    public ScriptingDatabase(SQLDatabase database) { _database = database; }

    public string Name() { return _database.name; }
    public string User() { return _database.user; }
    public string SVNRoot() { return _database.svnRepository; }
    public string UserFullName()
    {
      string user = _database.user;
      UserInfo info = _database.GetUser(user);
      if (info != null && !String.IsNullOrEmpty(info._fullName)) return info._fullName;
      return user; 
    }
    public ArrayInt Select(string filter)
    {
      List<int> results = new List<int>();

      // compile the filter
      ColaboRuleSetLexer lexer = new ColaboRuleSetLexer(new Antlr.Runtime.ANTLRStringStream(string.Format("({0})", filter)));
      ColaboRuleSetParser parser = new ColaboRuleSetParser(new Antlr.Runtime.CommonTokenStream(lexer));
      List<Token> filterCompiled = new List<Token>();
      parser._result = filterCompiled;
      try
      {
        parser.expression();
      }
      catch (System.Exception)
      {
        return new ArrayInt(results, -1);
      }

      if (parser.NumberOfSyntaxErrors == 0)
      {
        // filter the articles
        foreach (ArticleItem item in _database.cache.Values)
        {
          ArticleItem.VTCache cache = new ArticleItem.VTCache(_database, item);
          if ((item.GetAccessRights(_database, cache) & AccessRights.Read) == 0) continue; // ignore unaccessible articles
          if (item.MatchExpression(filterCompiled, _database, cache)) results.Add(item._id);
        }
      }

      return new ArrayInt(results, -1);
    }
    public ScriptingLog FindMonthUpdates(string user, int year, int month)
    {
      ScriptingLog log = new ScriptingLog();

      DateTime from = new DateTime(year, month, 1);
      DateTime to = from.AddMonths(1);

      // SVN log
      string query = "";
      if (!string.IsNullOrEmpty(user)) QueryAppendAnd(ref query, string.Format("author:({0})", user));
      QueryAppendAnd(ref query, string.Format("time:[{0:yyyyMMddHHmm} TO {1:yyyyMMddHHmm}]", from, to));
      List<SearchResult> results = _database.FullTextSearch(query);

      // sort
      results.Sort(new ResultDateComparer());
      // avoid duplicities
      foreach (SearchResult result in results)
      {
        bool found = false;
        for (int i = 0; i < log.Count(); i++)
        {
          ScriptingLogEntry entry = log.Get(i);
          if (entry.LogMessage().Equals(result._title))
          {
            found = true;
            break;
          }
        }
        if (!found) log.Add(result);
      }

      return log;
    }
    private class ResultDateComparer : IComparer<SearchResult>
    {
      public int Compare(SearchResult r1, SearchResult r2)
      {
        return r1._time.CompareTo(r2._time);
      }
    }
    private void QueryAppendAnd(ref string query, string append)
    {
      if (query.Length > 0) query += " AND ";
      query += append;
    }
    public ScriptingArticle GetArticle(int id)
    {
      ArticleItem item;
      if (!_database.cache.TryGetValue(id, out item)) return null;
      ArticleItem.VTCache cache = new ArticleItem.VTCache(_database, item);
      if ((item.GetAccessRights(_database, cache) & AccessRights.Read) == 0) return null; // ignore unaccessible articles
      return new ScriptingArticle(_database, item);
    }
    public ScriptingUsers Users()
    {
      List<ScriptingUser> users = new List<ScriptingUser>();

      foreach (KeyValuePair<string, UserInfo> user in _database.users)
      {
        users.Add(new ScriptingUser(user.Key, user.Value));
      }

      return new ScriptingUsers(users, _database.aliases);
    }
  }

  // type User
  [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
  [System.Runtime.InteropServices.ComVisibleAttribute(true)]
  public class ScriptingUser
  {
    private string _id;
    private UserInfo _info;

    public ScriptingUser(string id, UserInfo info)
    {
      _id = id;
      _info = info;
    }
    public string ID() { return _id; }
    public string FullName() { return _info._fullName; }
    public string Picture() { return _info._picture; }
    public int Color() { return _info._color; }
  }

  // list of users
  [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
  [System.Runtime.InteropServices.ComVisibleAttribute(true)]
  public class ScriptingUsers
  {
    private List<ScriptingUser> _users;
    private Dictionary<string, string> _aliases;

    public ScriptingUsers(List<ScriptingUser> users, Dictionary<string, string> aliases)
    {
      _users = users;
      _aliases = aliases;
    }
    public int Count()
    {
      return _users.Count;
    }
    public ScriptingUser Get(int index)
    {
      if (index < 0 || index >= _users.Count) return null;
      return _users[index];
    }
    public int FindIndex(string alias)
    {
      // try to check if 
      string user;
      if (!_aliases.TryGetValue(alias, out user)) user = alias;
      return _users.FindIndex(delegate(ScriptingUser u)
      {
        return u.ID() == user;
      });
    }
  }

  // type Article
  [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
  [System.Runtime.InteropServices.ComVisibleAttribute(true)]
  public class ScriptingArticle
  {
    private SQLDatabase _database;
    private ArticleItem _article;

    public ScriptingArticle(SQLDatabase database, ArticleItem article) { _database = database; _article = article; }

    public string Title()
    {
      return _article._title;
    }
    public string URL()
    {
      return _article.AbsoluteURL(_database);
    }
    public string Author()
    {
      return _article._author;
    }
    public string Date()
    {
      return _article._date.ToString("d"); 
    }
    public string Time()
    {
      return _article._date.ToString("t");
    }
    public ArrayString Tags()
    {
      return new ArrayString(_article._effectiveTags, "");
    }
    public int Parent()
    {
      return _article._parent;
    }
    public ArrayInt Children()
    {
      if (_article._children == null) return new ArrayInt(new List<int>(), -1);
      return new ArrayInt(_article._children, -1);
    }
    public string Content()
    {
      string itemURL = _article.AbsoluteURL(_database);
      if (string.IsNullOrEmpty(itemURL)) return null;
      string dummy;
      return FormColabo.GetArticleContent(itemURL, out dummy);
    }
  }

  // SVN Log
  [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
  [System.Runtime.InteropServices.ComVisibleAttribute(true)]
  public class ScriptingLog
  {
    private List<ScriptingLogEntry> _log;

    public ScriptingLog() { _log = new List<ScriptingLogEntry>(); }

    public int Count()
    {
      return _log.Count;
    }
    public ScriptingLogEntry Get(int index)
    {
      if (index < 0 || index >= _log.Count) return null;
      return _log[index];
    }
    public void Add(SharpSvn.SvnLogEventArgs args, string root)
    {
      _log.Add(new ScriptingLogEntry(args, root));
    }
    public void Add(SearchResult result)
    {
      _log.Add(new ScriptingLogEntry(result));
    }
  }

  // SVN Log Entry
  [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
  [System.Runtime.InteropServices.ComVisibleAttribute(true)]
  public class ScriptingLogEntry
  {
    private string _author;
    private ArrayString _changedPaths;
    private string _logMessage;
    private long _revision;
    private DateTime _time;

    public string Author() {return _author;}
    public ArrayString ChangedPaths() {return _changedPaths;}
    public string LogMessage() {return _logMessage;}
    public string LogMessageHTML() { return _logMessage.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;"); }
    public int Revision() { return (int)_revision; }
    public string Date() { return _time.ToString("d"); }
    public string Time() { return _time.ToString("t"); }

    public ScriptingLogEntry(SharpSvn.SvnLogEventArgs args, string root)
    {
      _author = args.Author;
      List<string> paths = new List<string>();
      foreach (SharpSvn.SvnChangeItem item in args.ChangedPaths)
      {
        paths.Add(root + item.RepositoryPath.ToString());
      }
      _changedPaths = new ArrayString(paths, "<no message>");
      _logMessage = args.LogMessage;
      _revision = args.Revision;
      _time = args.Time.ToLocalTime();
    }
    public ScriptingLogEntry(SearchResult result)
    {
      _author = result._author;
      List<string> paths = new List<string>();
      ArticleItem item;
      if (result._database.cache.TryGetValue(result._id, out item)) paths.Add(item.AbsoluteURL(result._database));
      _changedPaths = new ArrayString(paths, "<no message>");
      _logMessage = result._title;
      _revision = result._revision;
      _time = result._time;
    }
  }

  // encapsulation of arrays
  [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
  [System.Runtime.InteropServices.ComVisibleAttribute(true)]
  public class ArrayInt
  {
    private List<int> _data;
    private int _default;

    public ArrayInt(List<int> data, int def) { _data = data; _default = def; }

    public int Count()
    {
      if (_data == null) return 0;
      return _data.Count;
    }
    public int Get(int index)
    {
      if (_data == null || index < 0 || index >= _data.Count) return _default;
      return _data[index];
    }
  }
  [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
  [System.Runtime.InteropServices.ComVisibleAttribute(true)]
  public class ArrayString
  {
    private List<string> _data;
    private string _default;

    public ArrayString(List<string> data, string def) { _data = data; _default = def; }

    public int Count()
    {
      if (_data == null) return 0;
      return _data.Count;
    }
    public string Get(int index)
    {
      if (_data == null || index < 0 || index >= _data.Count) return _default;
      return _data[index];
    }
  }
};