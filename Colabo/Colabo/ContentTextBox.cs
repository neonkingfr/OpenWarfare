using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;
using Net.Sgoliver.NRtfTree.Core;

namespace Colabo
{
  // Text box for the COMA content for better handling of Paste
  class ContentTextBox : TextBox
  {
    private SQLDatabase _database = null;
    public void SetDatabase(SQLDatabase database) { _database = database; }

    public class DetectSourceParser : SarParser
    {
      //...
      public class GroupInfo
      {
        public bool inFontTbl;
        public bool expectFont;

        public GroupInfo()
        {
          expectFont = false;
          inFontTbl = false;
        }
        public GroupInfo(GroupInfo g)
        {
          expectFont = g.expectFont;
          inFontTbl = g.inFontTbl;
        }
      };

      Stack<GroupInfo> stack;

      bool proportionalHit;
      bool fontHit;

      public bool IsSource()
      {
        // source code is anything using a monospaced font, e.g. Consolas (Visual Studio 2010 and 2012)
        return fontHit && !proportionalHit;
      }
      public override void StartRtfDocument()
      {
        proportionalHit = false;
        fontHit = false;

        stack = new Stack<GroupInfo>();
      }

      public override void EndRtfDocument()
      {
      }

      public override void StartRtfGroup()
      {
        stack.Push(stack.Count>0 ? new GroupInfo(stack.Peek()) : new GroupInfo());
      }

      public override void EndRtfGroup()
      {
        stack.Pop();
      }

      public override void RtfControl(string key,
                              bool hasParam, int param)
      {
        //..
      }

      public override void RtfKeyword(string key,
                              bool hasParam, int param)
      {
        switch (key)
        {
          case "fonttbl":
            {
              GroupInfo cur = stack.Peek();
              if (cur != null) cur.inFontTbl = true;
            }
            break;
          case "f":
            {
              GroupInfo cur = stack.Peek();
              if (cur != null) cur.expectFont = true;
            }
            break;
        }
      }

      public override void RtfText(string text)
      {
        GroupInfo cur = stack.Peek();
        if (cur!=null && cur.inFontTbl && cur.expectFont)
        {
          // strip trailing ;
          fontHit = true;
          text = text.TrimEnd(";".ToCharArray());
          // compare againts a list of common monospaced fonts
          string[] monospaced = { "Consolas", "Courier New", "Courier", "Andale Mono", "Inconsolata", "Lucida Console", "Lucida Typewriter" };
          bool isMonospaced = false;
          foreach (string f in monospaced)
          {
            if (text.Equals(f, StringComparison.OrdinalIgnoreCase))
            {
              isMonospaced = true;
              break;
            }
          }
          if (!isMonospaced) proportionalHit = true;
          cur.expectFont = false;
        }
      }
    }

    protected override void WndProc(ref Message msg)
    {
      if (msg.Msg == 0x302) // WM_PASTE
      {
        // if text is shortcut to Colabo article, insert in better looking form
        IDataObject obj = Clipboard.GetDataObject();
        if (obj.GetDataPresent("ColaboShortcut"))
        {
          Object data = obj.GetData("ColaboShortcut");
          string link = data.ToString();
          int index1 = link.LastIndexOf('?');
          if (index1 >= 0)
          {
            int index2 = link.LastIndexOf('?', index1 - 1, index1);
            if (index2 >= 0)
            {
              string title = link.Substring(0, index2);
              string fullURL = link.Substring(index2 + 1, index1 - (index2 + 1));
              string query = link.Substring(index1);
              // make the url relative to the current database
              string url = _database == null ? fullURL : _database.ToRelative(fullURL);
              string escapedUrl = url.Replace("(", "%28").Replace(")", "%29");
              this.SelectedText = string.Format("[{0}]({1}{2})", title, escapedUrl, query);
            }
          }
          return;
        }
        if (obj.GetDataPresent(DataFormats.UnicodeText))
        {
          // Skype alwauys copies as a unicode text
          Object data = obj.GetData(DataFormats.UnicodeText);
          string text = data.ToString();
          // check if it looks like Skype conversation
          if (text.Length > 10)
          {
            bool isSkype = true;
            bool someHeader = obj.GetDataPresent("SkypeMessageFragment"); // detect fragments of a single message
            // if it is from Skype, parse it line by line, convert message headers
            // and append space to each line
            List<string> convertedLines = new List<string>();
            string[] lines = text.Split('\n');
            string before1 = null;
            string before2 = null;
            string before3 = null;
            int before1converted = -1;
            int before2converted = -1;
            int before3converted = -1;
            string lastName = null;
            foreach (string line0 in lines)
            {
              char[] charsToTrim = {'\r', ' ', '\t'};
              String line = line0.TrimEnd(charsToTrim);
              int convertingTo = convertedLines.Count;
              if (line.Length >= 3 && line.Substring(0, 3) == "<<<" && before1 == "" && before2.EndsWith(":"))
              { // quote, in form <<<
                if (before3 == "")
                {
                  // empty line, [time] User Name:, empty line
                  convertedLines.RemoveRange(before3converted, convertedLines.Count - before3converted);
                }
                else
                {
                  // line before should be an empty
                  // line before should end with a time/name header in form [time] User Name
                  int headerLine = before1converted - 1;
                  string headerLineText = convertedLines[headerLine];
                  int headerPos = headerLineText.LastIndexOf('[');
                  if (headerPos <= 1) convertedLines.RemoveRange(headerLine, convertedLines.Count - headerLine);
                  else
                  {
                    convertedLines[headerLine] = headerLineText.Substring(0, headerPos).TrimEnd(charsToTrim);
                    convertedLines.RemoveRange(before1converted, convertedLines.Count - before1converted);
                  }
                }
                convertedLines.Add(" >" + line.Substring(3));
              }
              else if (line.Length == 0 || line[0]!='[')
                convertedLines.Add(" " +line);
              else
              { // time/message header, in form [15:48:31] User Name:
                int closeTime = line.IndexOf(']');
                if (closeTime < 0 || closeTime > 50) { isSkype = false; break; } // too long or not closed
                if (line.Length < closeTime + 3 || line[closeTime + 1] != ' ') { isSkype = false; break; }// Name too short, or format not matching
                string time = line.Substring(1, closeTime-1);
                int edit = time.IndexOf('|');
                if (edit > 0)
                {
                  time = time.Substring(0,edit);
                }
                int endName = line.IndexOf(':', closeTime);
                if (endName < 0 || endName > closeTime + 40) { isSkype = false; break; } // too long or not closed
                string name = line.Substring(closeTime + 2, endName - (closeTime + 2));
                if (name != lastName)
                {
                  convertedLines.Add("[" + time + " " + name + "]:");
                  lastName = name;
                }
                convertedLines.Add(line.Substring(endName + 1));
                someHeader = true;
              }
              before3converted = before2converted; before2converted = before1converted; before1converted = convertingTo;
              before3 = before2; before2 = before1; before1 = line;
            }
            if (isSkype && someHeader)
            {
              if (convertedLines[0].Length > 0 && convertedLines[0][0] != '[')
              { // make sure even first line is a "note"
                convertedLines.Insert(0,string.Format("[Skype {0} {1}]:", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString()));
              }
              string pasteText = "";
              foreach (string line in convertedLines)
              {
                pasteText += line;
                pasteText += "\r\n";
              }
              this.SelectedText = pasteText;
              return;
            }
          }
          if (obj.GetDataPresent(DataFormats.Rtf,false))
          { // is RTF, might be a source code

            //Load the RTF document
            bool isSource = false;
            try
            {
              DetectSourceParser parser = new DetectSourceParser();

              //Create the reader and associate the parser
              RtfReader reader = new RtfReader(parser);

              Object rtfData = obj.GetData(DataFormats.Rtf);

              reader.LoadRtfText(rtfData.ToString());

              //Start parsing
              reader.Parse();

              isSource = parser.IsSource();
            }
            catch (NullReferenceException ex)
            {
              ex.ToString();
            } catch
            {
            }
            if (isSource)
            {
              string pasteText;
              if (text.Contains("\n"))
              {
                pasteText = "(\r\n" + text;
                if (pasteText.Substring(pasteText.Length - 2) != "\r\n") pasteText += "\r\n";
                pasteText += ")\r\n";
              }
              else if (text.Contains("`"))
              {
                pasteText = "( " + text + " )";
              }
              else
              {
                pasteText = "`" + text + "`";

              }
              this.SelectedText = pasteText;
              return;
            }
          }
        }
      }
      base.WndProc(ref msg);
    }
  }
}
