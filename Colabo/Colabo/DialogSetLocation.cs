﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace Colabo
{
  public partial class DialogSetLocation : Form
  {
    FormColabo _owner;

    public DialogSetLocation(FormColabo owner, string locIpAddr, string locLocation)
    {
      _owner = owner;
      InitializeComponent();
      ipAddr.Text = locIpAddr;
      location.Text = locLocation;
    }

    private void buttonCancel_Click(object sender, EventArgs e)
    {

    }

    private void buttonOk_Click(object sender, EventArgs e)
    {
      _owner.SetLocationName(ipAddr.Text, location.Text);
    }
  }
}
