﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Colabo
{
  public class ChatConversation
  {
    public struct ChatData
    {
      public int id;
      public int revToRead;

      public struct UserState
      {
        public string state;
        public DateTime stateTime;
        public int stateRev;
      }

      public class UserList: Dictionary<string,UserState>
      {
        public UserList() { }
        public UserList(UserList src) : base(src) { }
      }
      public class SortedUserList: List<KeyValuePair<string,ChatConversation.ChatData.UserState>> {}

      public UserList users;
    };

    public class Part
    {
      public int part_;
      public string user_;
      public DateTime time_;
      public string text_;
    }

    List<Part> parts_;

    public ChatConversation() {}
    public ChatConversation(StructuredNode node)
    {
      List<StructuredNode> parts = node.GetChildArray("parts");
      if (parts != null)
      {
        parts_ = new List<Part>();
        foreach (var part in parts)
        {
          try
          {
            var p = new Part();
            p.part_ = Int32.Parse(part.GetAttribute("part"));
            p.text_ = part.GetValue();
            p.time_ = DateTime.Parse(part.GetAttribute("time"));
            p.user_ = part.GetAttribute("user");
            parts_.Add(p);
          }
          catch { }
        }
      }
    }
    internal void Save(StructuredNode history)
    {
      if (parts_ != null)
      {
        foreach (var p in parts_)
        {
          try
          {
            StructuredNode parts = history.AppendChildArray("parts");
            parts.SetValue(p.text_);
            parts.SetAttribute("part", p.part_.ToString());
            parts.SetAttribute("time", p.time_.ToString());
            parts.SetAttribute("user", p.user_);
          }
          catch { }

        }
      }
    }

    public string GetText(SQLDatabase db, ChatData.UserList users = null)
    {
      string text = "";
      if (parts_!=null)
      {
        var userState = new ChatData.SortedUserList();

        // insert chat user state
        if (users != null)
        {
          foreach (var item in users)
          {
            if (item.Key == db.user) continue;
            if (item.Value.state == "Ready") continue;
            var info = item.Value;
            userState.Add(item);
          }
          // sort by revision
          userState.Sort((x, y) => {
            int d = x.Value.stateRev - y.Value.stateRev;
            if (d!=0) return d;
            d = DateTime.Compare(x.Value.stateTime, y.Value.stateTime);
            if (d!=0) return d;
            return String.Compare(x.Key, y.Key);
          });
        }
        
        int lastRev = 0;
        foreach (var part in parts_)
        {
          // display status relevant for this revision
          text += ListUserStatus(db, userState, lastRev, part.part_);
          lastRev = part.part_;

          text += FormatChatHeader(db, part.user_, part.time_);
          text += part.text_;
          text = EndWithEOL(text);
        }
        text += ListUserStatus(db, userState, lastRev, Int32.MaxValue);
      
      }
      return text;
    }

    private static string ListUserStatus(SQLDatabase db, ChatData.SortedUserList users, int begRev, int endRev)
    {
      string text = "";
      if (users != null)
      {
        foreach (var user in users)
        {
          if (user.Value.stateRev >= begRev && user.Value.stateRev < endRev)
          {
            text += FormatChatStatus(db, user.Key, user.Value.state, user.Value.stateTime);
          }
        }
      }
      return text;
    }

    internal string EndWithEOL(string x)
    {
      if (!x.EndsWith("\r\n")) x += "\r\n";
      return x;
    }

    static public string FormatChatStatus(SQLDatabase db, string user, string info, DateTime? time)
    {
      string header = "\r\n###### " + db.UserDisplayName(user);
      switch (info)
      {
        case "Close": header += " has closed the window"; break;
        case "Invited": header += " was invited"; break;
        case "Type": header += " is typing"; break;
      }
      //header += info;
      //if (time.HasValue) header += " " + time.Value.ToShortTimeString();
      header += "\r\n";
      return header;
    }

    static public string FormatChatHeader(SQLDatabase db, string user, DateTime? time)
    {
      string header = "\r\n##### " + db.UserDisplayName(user);
      if (time.HasValue) header += " " + time.Value.ToShortTimeString();
      header += "\r\n";
      return header;
    }


    internal Part LastIncoming()
    {
      if (parts_.Count <= 0) return null;
      return parts_[parts_.Count-1];
    }

    internal Part FindPart(int part)
    {
      foreach (var p in parts_)
      {
        if (p.part_ == part) return p;
      }
      return null;
    }
  }
}
