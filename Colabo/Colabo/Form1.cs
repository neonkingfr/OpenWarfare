// Show Design Tools menu item containg tools such as Import from NG
#define SHOW_DESIGN_TOOLS

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Runtime.InteropServices;

using System.Xml;
using Jayrock.Json;

using System.IO;
using System.Net;

using System.Reflection;

using System.Globalization;

using MSHTML;

using Token = Antlr.Runtime.IToken;
using System.Diagnostics;
using Microsoft.Win32;
using Colabo.UserActivityMonitor;

namespace Colabo
{
  public partial class FormColabo : Form, IMessageFilter, IReloadPage
  {
    /// index of the cache - all visible articles (source for DataGridView)
    private List<IndexItem> _index;
    public List<IndexItem> index
    {
      get { return _index; }
    }
    /// index of the cache - all articles which matches current filter (tags), some are not shown because collapsed
    private List<IndexItem> _indexEnabled;

    /// navigation history (for back and forward buttons)
    private List<NavHistoryItem> _navigateHistory; // url and tags of visited articles
    /// navigation history current index
    private int _navigateHistoryIndex;
    /// indication the navigateBack/Forward button was clicked
    bool _navigatingInHistory;

    // Rule sets
    public List<RuleSet> _ruleSets;
    private List<Token> _currentRuleSetCompiled;

    // Scripts
    public List<UIScriptInfo> _uiScripts;

    /// row index (in the _index) context menu was opened for
    private int _contextMenuRow;

    /// navigation tag context menu was opened for
    private string _contextMenuTag;

    /// last loaded URL
    private string _currentURL;
    /// last loaded content (corresponding to _currentURL)
    private string _currentContent;

    /// selected tags
    private List<TagCondition> _tags;

    /// prefered languages
    private List<String> _languages;

    /// limit tags
    private bool _tagsLimit = true; 

    /// filter for tags shown in tag cloud
    private char _tagsFilter = '\0';

    // the sorting method of article rows
    string _sortOrderMethod;
    // direction true for reverse
    bool _sortOrderDir;

    // the current rule set
    private string _ruleSetUser;
    private string _ruleSetName;

    // drag & drop - drag support
    Rectangle _dragBoxFromMouseDown;
    int _mouseDownRowIndex;

    // article current in the last session
    string _initServer;
    string _initDatabase;
    int _initArticle;

    // during the update, do not reload the current article repeatedly
    private bool _suppressChangeRow = false;

    /// set of opened SQL databases
    private List<SQLDatabase> _databases;
    public List<SQLDatabase> databases
    {
      get { return _databases; }
    }

    public ImageList PresenceImages {get {return presenceImages;}}

    // columns indices
    public const int _columnIcon = 0;
    public const int _columnTitle = 1;
    public const int _columnAuthor = 2;
    public const int _columnDate = 3;

    // virtual tags names
    public const string _tagToRead = ":to-read";
    public const string _tagChanged = ":changed";
    public const string _tagToMe = ":@me";
    public const string _tagByMe = ":by-me";
    public const string _tagBy = ":by-{0}";
    public const string _tagDb = ":db-{0}";
    public const string _tagDraft = ":draft";
    public const string _tagChat = ":chat";
    public const string _tagHomepage = ":homepage";
    public const string _tagToday = ":today";
    public const string _tagLastWeek = ":last-week";
    public const string _tagLastMonth = ":last-month";
    public const string _ancestorPrefix = ":ancestor-has-";
    public const string _descendantPrefix = ":descendant-has-";
    public const string _threadPrefix = ":thread-has-";

    // href prefixes
    const string _prefixTag = "about:Tag(";
    const string _prefixLang = "about:Lang(";
    const string _prefixLevel = "about:Level(";
    const string _prefixAllTags = "about:AllTags(";
    const string _prefixMostUsed = "about:MostUsed(";
    const string _prefixLetter = "about:Letter(";
    const string _prefixRoot = "about:Root(";

    // Used especially for its IAuthenticate implementation which is callBacked by WebBrowser.Navigate
    private BrowserRemotingProxy browserProxy = null;

    private Dictionary<string, Icon> _icons;
    public Dictionary<string, Icon> icons
    {
      get { return _icons; }
    }

    // Find info class definition
    class FindInfo
    {
      public string text;      // Text to find
      public bool wholeWords;  // Match whole words
      public bool matchCase;   // Match case
      public bool up;          // Search reverse
    };
    // Find dialog class
    private DialogFind _findDialog;
    // Last find info
    private FindInfo _findInfo;
    // Last found text range from WebBrowser component
    private IHTMLTxtRange _lastFindRange;

    // Address bar component
    private AddressBar _addressBar;
    private bool _addressBarChanging; // Set to true, if changing addressBar text

    // Timer for synchronization
    private Timer _synchronizeTimer;

    // Timer for marking article as read
    private Timer _markArticleReadTimer;

    public NotifyIdle _notifyIdle = new NotifyIdle();

    // list of all open chat windows
    private List<DialogArticle> _chats = new List<DialogArticle>();

    struct WaitingChatData
    {
      public SQLDatabase db;
      public ChatConversation.ChatData data;
    }
    private List<WaitingChatData> _waitingChatData = new List<WaitingChatData>();

    // Form initialization
    public FormColabo()
    {
      DialogArticle.SetInternetFeature(DialogArticle.FEATURE_DISABLE_NAVIGATION_SOUNDS, true);

      // create and initialize data structures
      _index = new List<IndexItem>();
      _indexEnabled = new List<IndexItem>();
      _tags = new List<TagCondition>();
      _languages = new List<String>();

      _navigateHistory = new List<NavHistoryItem>();
      _navigateHistoryIndex = 0;
      _navigatingInHistory = false;

      _databases = new List<SQLDatabase>();

      _ruleSets = new List<RuleSet>();
      _currentRuleSetCompiled = null;
      _ruleSetUser = _ruleSetName = "";

      _uiScripts = new List<UIScriptInfo>();

      _contextMenuRow = -1;

      _currentURL = "";
      _currentContent = null;

      _sizePos = new DialogSizePos(this);

      _dragBoxFromMouseDown = Rectangle.Empty;
      _mouseDownRowIndex = -1;

      _initServer = null;
      _initDatabase = null;
      _initArticle = -1;

      InitializeComponent();

      this.imState.SelectedIndex = 0;
      imUsers.MouseClick += imUsers_MouseClick;
      notifyIcon.Icon = Properties.Resources.user_online;
      notifyIcon.Visible = true;


#if DEBUG
      imUsers.Columns.Add("Build", 55, HorizontalAlignment.Left);
#endif

      // Create address bar
      _addressBar = new AddressBar();
      _addressBar.KeyDown += new KeyEventHandler(_addressBar_KeyDown);
      _addressBar.KeyUp += new KeyEventHandler(_addressBar_KeyUp);
      _addressBar.TextChanged += new EventHandler(_addressBar_TextChanged);
      toolStrip1.Items.Add(_addressBar);

      _addressBar.SetAutocomplete(new Autocomplete(_addressBar.TextBox, new AllTags(this)));
      _addressBar.Multiline = true;
      _addressBar.AcceptsReturn = true;
      _addressBar.AcceptsTab = true;

      _markArticleReadTimer = new Timer();
      _markArticleReadTimer.Tick += new EventHandler(markArticleReadTimer_Tick);

      _synchronizeTimer = new Timer();
      _synchronizeTimer.Tick += new EventHandler(synchronizeTimer_Tick);

      if (webBrowser1 != null) webBrowser1.ObjectForScripting = new Scripting(this, this);

#if SHOW_DESIGN_TOOLS
      this.designToolsToolStripMenuItem.Visible = true;
      this.importFromNGToolStripMenuItem.Visible = true;
#endif

      // hook to handle keyboard events
      Application.AddMessageFilter(this);

      _icons = new Dictionary<string, Icon>();

      // load icons from the resource
      System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
      if (assembly != null)
      {
        const string prefix = "Colabo.Resources.Icons.";
        const string suffix = ".ico";
        string[] names = assembly.GetManifestResourceNames();
        foreach (string name in names)
        {
          if (name.StartsWith(prefix) && name.EndsWith(suffix))
          {
            string shortName = name.Substring(prefix.Length, name.Length - prefix.Length - suffix.Length);
            Stream stream = assembly.GetManifestResourceStream(name);
            Icon icon = new Icon(stream);
            if (!_icons.ContainsKey(shortName.ToLower())) _icons.Add(shortName.ToLower(), icon);
          }
        }
      }
      // load icons from the directory
      if (Directory.Exists("icons"))
      {
        string[] files = Directory.GetFiles("icons", "*.ico");
        foreach (string name in files)
        {
          const string prefix = "icons\\";
          const string suffix = ".ico";
          string shortName = name.Substring(prefix.Length, name.Length - prefix.Length - suffix.Length);
          Stream stream = new FileStream(name, FileMode.Open);
          Icon icon = new Icon(stream);
          if (!_icons.ContainsKey(shortName.ToLower())) _icons.Add(shortName.ToLower(), icon);
        }
      }

      // calendar initialization
      dayView.StartDate = DateTime.Now - TimeSpan.FromDays(1);
      dayView.StartHour = dayView.WorkingHourStart;

      // Connect events for mouse over
      if (dataGridView1 != null)
        dataGridView1.MouseEnter += new EventHandler(dataGridView1_MouseEnter);
      if (bookmarks != null)
        bookmarks.MouseEnter += new EventHandler(bookmarks_MouseEnter);
    }

    private class AllTags : ICollectCandidates
    {
      private FormColabo _owner = null;

      public AllTags(FormColabo owner)
      {
        _owner = owner;
      }
      private void CheckAndAdd(string prefix, string tag, ref SortedList<string, int> result)
      {
        if (tag.StartsWith(prefix))
        {
          if (!result.ContainsKey(tag)) result.Add(tag, 1);
        }
      }
      public void Process(string prefix, ref SortedList<string, int> result)
      {
        if (string.IsNullOrEmpty(prefix)) return;

        if (prefix[0] == ':')
        {
          // virtual tags
          CheckAndAdd(prefix, _tagToRead, ref result);
          CheckAndAdd(prefix, _tagChanged, ref result);
          CheckAndAdd(prefix, _tagToMe, ref result);
          CheckAndAdd(prefix, _tagByMe, ref result);
          CheckAndAdd(prefix, _tagDraft, ref result);
          CheckAndAdd(prefix, _tagChat, ref result);
          CheckAndAdd(prefix, _tagHomepage, ref result);
          CheckAndAdd(prefix, _tagToday, ref result);
          CheckAndAdd(prefix, _tagLastWeek, ref result);
          CheckAndAdd(prefix, _tagLastMonth, ref result);
          // :by-...
          foreach (SQLDatabase db in _owner.databases)
          {
            foreach (string user in db.users.Keys)
            {
              string tag = string.Format(_tagBy, user.ToLower());
              CheckAndAdd(prefix, tag, ref result);
            }
          }
          // :db-...
          foreach (SQLDatabase db in _owner.databases)
          {
            string tag = string.Format(_tagDb, db.name.ToLower());
            CheckAndAdd(prefix, tag, ref result);
          }
        }
        else
        {
          foreach (SQLDatabase db in _owner.databases)
          {
            db.FindTags(prefix, ref result);
          }
        }
      }
    }

    // Mouse events for focussing panels
    private void dataGridView1_MouseEnter(object sender, EventArgs e)
    {
      if (dataGridView1 != null && Form.ActiveForm == this)
        dataGridView1.Focus();
    }
    private void BrowserDocument_MouseOver(object sender, HtmlElementEventArgs e)
    {
      if (webBrowser1 != null && Form.ActiveForm == this)
        webBrowser1.Document.Focus();
    }
    private void TagsDocument_MouseOver(object sender, HtmlElementEventArgs e)
    {
      if (tagsNavigator != null && Form.ActiveForm == this)
        tagsNavigator.Document.Focus();
    }
    private void bookmarks_MouseEnter(object sender, EventArgs e)
    {
      if (bookmarks != null && Form.ActiveForm == this)
        bookmarks.Focus();
    }

    private void markArticleReadTimer_Tick(object sender, EventArgs e)
    {
      MarkAsRead();
    }

    private void synchronizeTimer_Tick(object sender, EventArgs e)
    {
      Synchronize(null, ArticleId.None); // all databases
    }

    private static ArticleItem FindChatArticle(SQLDatabase db, int chatId)
    {
      ArticleItem item = new ArticleItem();
      item._changed = false;
      item._toRead = false;
      // get chat header - TODO: try cache first
      if (db.LoadChatHeader(chatId, item))
      {
        // refresh chat list to contain recent information about all chats
        // TODO: refresh cache as well
        item._chatId = chatId;
        item._id = db.cache.UniqueTempId();
        return item;
      }
      return null;
    }

    private void FocusChat(SQLDatabase db, ChatConversation.ChatData data)
    {
      ArticleItem item = FindChatArticle(db, data.id);
      if (item==null) return;

      foreach (var chat in _chats) // find a corresponding child DialogArticle
      {
        if (chat.IsChat(data.id))
        {
          chat.RefreshChat(data,true);
          chat.Activate();
          chat.Focus();
          return;
        }
      }

      var dialog = NewChatWindow(db, item, true);
      dialog.Activate();
      dialog.Focus();
    }

    private DialogArticle NewChatWindow(SQLDatabase db, ArticleItem item, bool focus)
    {
      DialogArticle dialog = new DialogArticle(item, this, null, db, DialogArticle.Mode.Chat);
      if (!focus)
      {
        dialog.ShowInBackground();
      }
      else
      {
        dialog.Show();
      }
      return dialog;
    }


    private void OnChat(SQLDatabase db, ChatConversation.ChatData data)
    {
      DialogArticle found = null;
      bool focused = false;
      
      foreach (var chat in _chats) // find a corresponding child DialogArticle
      {
        if (chat.IsChat(data.id))
        {
          chat.RefreshChat(data, chat == ActiveForm);
          if (chat == ActiveForm) focused = true;
          else if (data.revToRead>0)
          {
            FlashWindow.Flash(chat);
          }
          found = chat;
        }
      }
      if (data.revToRead>0 && (found==null || !focused))
      {
        ArticleItem item = FindChatArticle(db, data.id);
        if (item!=null)
        {
          if (found==null)
          {
            found = NewChatWindow(db,item, false);
          }
          ChatConversation chat = db.LoadChatConversation(item._chatId, false);

          var incoming = chat.FindPart(data.revToRead);
          if (incoming!=null)
          {
            // check if the item is already waiting
            if (!_waitingChatData.Exists((w)=>(w.db==db && w.data.id==data.id && w.data.revToRead==data.revToRead)))
            {
              notifyIcon.BalloonTipTitle = incoming.user_;
              notifyIcon.BalloonTipText = incoming.text_;
              notifyIcon.ShowBalloonTip(5000);

              var waiting = new WaitingChatData();
              waiting.db = db;
              waiting.data = data;
              _waitingChatData.Add(waiting);
              FlashWindow.Flash(found);
              SystemSounds.PlaySoundEvent("MailBeep");
            }
          }
        }
      }
    }

    private SQLDatabase NewDatabase(DBInfo info)
    {
      SQLDatabase database = new SQLDatabase(info, Program.config.ldapProfile,
        delegate(SQLDatabase db, DBEngine.UserList users) { UpdateIMPresence(db, users);},
        delegate(SQLDatabase db, ChatConversation.ChatData data) { this.Invoke((MethodInvoker)delegate { OnChat(db, data); }); }
      );
      _databases.Add(database);
      database._idleNotify = delegate(NotifyIdle.IdleState state) { database.ChangePresenceState(state.State); };
      _notifyIdle.Subscribe(database._idleNotify);
      return database;
    }
    private void Form1_Load(object sender, EventArgs e)
    {
      UpdateTitle();

      UpdateSortComparer(); // update using _sortOrderMethod

      foreach (DBInfo info in Program.config.databases)
      {
        SQLDatabase database = NewDatabase(info);
        if (database.LoadCache())
        {
          // TODO: show loaded articles
        }
      }

#if false
      Tools.MakeURLsRelative(this);
#endif

      // enable querying during Paint
      dataGridView1.Tag = this;

      webBrowser1.Navigate("about:blank");
      browserProxy = new BrowserRemotingProxy(webBrowser1, Program.config);

      // load the rule sets from database, show them in the UI
      if (_databases.Count > 0) _databases[0].LoadRuleSets(ref _ruleSets);
      ShowRuleSets(_ruleSetUser, _ruleSetName);

      UpdateBookmarks();

      // find the initial page
      ArticleId id = ArticleId.None;
      if (_initServer != null && _initDatabase != null)
      {
        id._database = _databases.Find(delegate(SQLDatabase item)
        {
          return item.server.Equals(_initServer, StringComparison.OrdinalIgnoreCase) &&
            item.database.Equals(_initDatabase, StringComparison.OrdinalIgnoreCase);
        });
        id._id = _initArticle;
      }
      if (id._database == null || id._id < 0)
      {
        id = FindHomePageId(null);
      }
      if (id != ArticleId.None) SelectArticle(id, -1);

      // initial synchronization (all databases)
      Synchronize(null, id);

      // load global scripts from databases
      UpdateScripts();
    }
    private void UpdateTitle()
    {
      // version number to the title
      string tags = "";
      for (int i=0; i<_tags.Count; i++)
      {
        if (i > 0) tags += "/";
        if (!_tags[i]._present) tags += "NOT ";
        tags += _tags[i]._tag;
      }
      string userInfo = "";
#if DEBUG
      userInfo = " DEBUG";
#endif
      this.Text = string.Format("{1} - Colabo ({0}){2}", Revision.RevisionNo, tags, userInfo);
    }

    /// DialogSizePos catches the OnResize and OnMove Events and keeps the actual size and pos fresh
    class DialogSizePos
    {
      private Control _parent;
      public Int32 x, y;
      public Int32 width, height;

      public DialogSizePos(Control parent) {
        _parent = parent;
        // set some default
        x = 0; y = 0; width = 1024; height = 640; 
        // attach to Resize and Move
        _parent.Resize += new System.EventHandler(OnResize);
        _parent.Move += new System.EventHandler(OnMove);
      }
      #region Event Handlers
      private void OnResize(object sender, System.EventArgs e)
      {
        // when the control is resized we will save the new size 
        if (((Form)_parent).WindowState == FormWindowState.Normal)
        {
          width = _parent.Width;
          height = _parent.Height;
        }
      }

      private void OnMove(object sender, System.EventArgs e)
      {
        // when the control is moved we will save the new position 
        if (((Form)_parent).WindowState == FormWindowState.Normal)
        {
          x = _parent.Left;
          y = _parent.Top;
        }
      }
      #endregion
    }

    private DialogSizePos _sizePos;

    public void SaveDialog(XmlDocument file, XmlElement config)
    {
      XmlElement cls = file.CreateElement("DialogWindow");
      config.AppendChild(cls);
      // Splitters
      XmlElement splitter1 = file.CreateElement("Splitter1");
      cls.AppendChild(splitter1);
      splitter1.SetAttribute("distance", string.Format("{0}", splitContainer1.SplitterDistance));
      XmlElement splitter2 = file.CreateElement("Splitter2");
      cls.AppendChild(splitter2);
      splitter2.SetAttribute("distance", string.Format("{0}", splitContainer2.SplitterDistance));
      XmlElement splitter3 = file.CreateElement("Splitter3");
      cls.AppendChild(splitter3);
      splitter3.SetAttribute("distance", string.Format("{0}", splitContainer3.SplitterDistance));
      XmlElement splitter4 = file.CreateElement("Splitter4");
      cls.AppendChild(splitter4);
      splitter4.SetAttribute("distance", string.Format("{0}", splitContainer4.SplitterDistance));
      // Window position and size
      XmlElement window = file.CreateElement("Window");
      cls.AppendChild(window);
      window.SetAttribute("x", string.Format("{0}", this._sizePos.x));
      window.SetAttribute("y", string.Format("{0}", this._sizePos.y));
      window.SetAttribute("width", string.Format("{0}", this._sizePos.width));
      window.SetAttribute("height", string.Format("{0}", this._sizePos.height));
      string state = WindowState == FormWindowState.Maximized ? "Maximized" : "Normal";
      window.SetAttribute("state", state);
      if (WindowState == FormWindowState.Maximized )
      {
        // save also maximized values, as splitters positions are relative to this and will be recomputed after RestoreDown
        XmlElement windowMax = file.CreateElement("WindowMax");
        cls.AppendChild(windowMax);
        windowMax.SetAttribute("x", string.Format("{0}", this.Location.X));
        windowMax.SetAttribute("y", string.Format("{0}", this.Location.Y));
        windowMax.SetAttribute("width", string.Format("{0}", this.Width));
        windowMax.SetAttribute("height", string.Format("{0}", this.Height));
      }
      // Columns width
      XmlElement columns = file.CreateElement("Columns");
      cls.AppendChild(columns);
      for (int i=0; i<dataGridView1.Columns.Count; i++)
      {
        XmlElement col = file.CreateElement("Column");
        columns.AppendChild(col);
        col.SetAttribute("width", string.Format("{0}",dataGridView1.Columns[i].Width));
        col.SetAttribute("displayIndex", string.Format("{0}", dataGridView1.Columns[i].DisplayIndex));
      }

      // Save selected Tag list
      cls = file.CreateElement("Tags");
      config.AppendChild(cls);
      for (int i = 0; i < _tags.Count; i++)
      {
        XmlElement tag = file.CreateElement("Tag");
        cls.AppendChild(tag);
        tag.SetAttribute("tag", _tags[i]._tag);
        tag.SetAttribute("present", string.Format("{0}", _tags[i]._present ? 1 : 0));
      }

      // current rule set
      cls = file.CreateElement("RuleSets");
      config.AppendChild(cls);
      cls.SetAttribute("user", _ruleSetUser);
      cls.SetAttribute("name", _ruleSetName);

      // sorting order of rows
      XmlElement sortOrder = file.CreateElement("SortingOrder");
      config.AppendChild(sortOrder);
      sortOrder.SetAttribute("method", _sortOrderMethod);
      sortOrder.SetAttribute("dir", _sortOrderDir ? "true" : "false");

      // current article
      if (dataGridView1 != null && dataGridView1.CurrentCell != null)
      {
        int rowIndex = dataGridView1.CurrentCell.RowIndex;
        if (rowIndex >= 0)
        {
          ArticleId id = _index[rowIndex]._articleId;
          if (id != ArticleId.None)
          {
            XmlElement currentArticle = file.CreateElement("CurrentArticle");
            config.AppendChild(currentArticle);
            currentArticle.SetAttribute("server", id._database.server);
            currentArticle.SetAttribute("database", id._database.database);
            currentArticle.SetAttribute("article", id._id.ToString());
          }
        }
      }
    }

    public void LoadDialog(XmlElement clsView)
    {
      XmlElement cls = (XmlElement)clsView.SelectSingleNode("DialogWindow");
      if (cls != null)
      {
        // Window position and size
        XmlElement window = (XmlElement)cls.SelectSingleNode("Window");
        XmlElement windowMax = null;
        if (window != null)
        {
          StartPosition = FormStartPosition.Manual;
          string state = window.GetAttribute("state");
          if (state == "Maximized")
          {
            WindowState = FormWindowState.Maximized;
            windowMax = (XmlElement)cls.SelectSingleNode("WindowMax");
          }
          else
          {
            WindowState = FormWindowState.Normal;
          }

          this._sizePos.x = int.Parse(window.GetAttribute("x"));
          this._sizePos.y = int.Parse(window.GetAttribute("y"));
          this._sizePos.width = int.Parse(window.GetAttribute("width"));
          this._sizePos.height = int.Parse(window.GetAttribute("height"));

          if (windowMax == null)
          {
            Location = new System.Drawing.Point(this._sizePos.x, this._sizePos.y);
            Size = new Size(this._sizePos.width, this._sizePos.height);
          }
          else
          { // temporary set the pos and size to stored Maximized values, only to load the splitter positions correctly
            // Note: splitters position was saved with their values relative to maximized size
            Int32 x = int.Parse(windowMax.GetAttribute("x"));
            Int32 y = int.Parse(windowMax.GetAttribute("y"));
            Int32 width = int.Parse(windowMax.GetAttribute("width"));
            Int32 height = int.Parse(windowMax.GetAttribute("height"));
            Location = new System.Drawing.Point(x, y);
            Size = new Size(width, height);
          }
        }
        // Splitters
        XmlElement splitter1 = (XmlElement)cls.SelectSingleNode("Splitter1");
        if (splitter1 != null)
        {
          int distance = int.Parse(splitter1.GetAttribute("distance"));
          try
          {
            splitContainer1.SplitterDistance = distance;
          }
          catch (System.Exception exception)
          {
            Console.Write(exception.ToString());
          }
        }
        XmlElement splitter2 = (XmlElement)cls.SelectSingleNode("Splitter2");
        if (splitter2 != null)
        {
          int distance = int.Parse(splitter2.GetAttribute("distance"));
          try
          {
            splitContainer2.SplitterDistance = distance;
          }
          catch (System.Exception exception)
          {
            Console.Write(exception.ToString());
          }
        }
        XmlElement splitter3 = (XmlElement)cls.SelectSingleNode("Splitter3");
        if (splitter3 != null)
        {
          int distance = int.Parse(splitter3.GetAttribute("distance"));
          try
          {
            splitContainer3.SplitterDistance = distance;
          }
          catch (System.Exception exception)
          {
            Console.Write(exception.ToString());
          }
        }
        XmlElement splitter4 = (XmlElement)cls.SelectSingleNode("Splitter4");
        if (splitter4 != null)
        {
          int distance = int.Parse(splitter4.GetAttribute("distance"));
          try
          {
            splitContainer4.SplitterDistance = distance;
          }
          catch (System.Exception exception)
          {
            Console.Write(exception.ToString());
          }
        }
        // Columns
        XmlElement columns = (XmlElement)cls.SelectSingleNode("Columns");
        XmlNodeList list = columns.SelectNodes("Column");
        if (list != null)
        {
          // patch - new column added
          int offset = dataGridView1.Columns.Count - list.Count;
          if (offset < 0) offset = 0;

          // set AutoSizeColumnMode to None for all columns
          for (int i = 0; i < dataGridView1.Columns.Count; i++)
            dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
          // set width and display index of each column
          for (int i = 0; i < list.Count; i++)
          {
            XmlElement column = (XmlElement)list[i];
            if (i + offset < dataGridView1.Columns.Count)
            {
              string width = column.GetAttribute("width");
              string displayIndex = column.GetAttribute("displayIndex");
              int index;
              if (int.TryParse(displayIndex, out index))
              {
                dataGridView1.Columns[i + offset].DisplayIndex = index + offset;
              }
              dataGridView1.Columns[i + offset].Width = int.Parse(width);
            }
          }
          // set last column autoSizeMode to Fill
          int lastColumn = dataGridView1.Columns.Count - 1;
          for (int i = 0; i < dataGridView1.Columns.Count; i++)
          {
            if (dataGridView1.Columns[i].DisplayIndex == lastColumn)
              dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
          }
        }
        if (windowMax != null)
        { // set the correct size and pos values for Restore Down (transfer from Maximized to Normal window state)
          Location = new System.Drawing.Point(this._sizePos.x, this._sizePos.y);
          Size = new Size(this._sizePos.width, this._sizePos.height);
        }
      }

      // Load selected Tag list
      cls = (XmlElement)clsView.SelectSingleNode("Tags");
      if (cls != null)
      {
        XmlNodeList list = cls.SelectNodes("Tag");
        if (list != null)
        {
          _tags.Capacity = list.Count;
          for (int i = 0; i < list.Count; i++)
          {
            XmlElement tag = (XmlElement)list[i];
            _tags.Add(new TagCondition(tag.GetAttribute("tag"), int.Parse(tag.GetAttribute("present")) != 0));
          }
        }
      }

      // Current rule set
      cls = (XmlElement)clsView.SelectSingleNode("RuleSets");
      if (cls != null)
      {
        // the current rule set
        _ruleSetUser = cls.GetAttribute("user");
        _ruleSetName = cls.GetAttribute("name");
      }

      // Sorting order of rows
      cls = (XmlElement)clsView.SelectSingleNode("SortingOrder");
      if (cls != null)
      {
        _sortOrderMethod = cls.GetAttribute("method");
        _sortOrderDir = bool.Parse(cls.GetAttribute("dir"));
      }

      // Current article
      cls = (XmlElement)clsView.SelectSingleNode("CurrentArticle");
      if (cls != null)
      {
        _initServer = cls.GetAttribute("server");
        _initDatabase = cls.GetAttribute("database");
        _initArticle = int.Parse(cls.GetAttribute("article"));
      }
    }

    #region Article Sorting
    // comparer used to sort dataGrid items (null for unsorted)
    private CompareIndices _selectedComparer = null;
    public abstract class CompareIndices 
    {
      FormColabo _colabo;
      bool _reverse;

      public CompareIndices(FormColabo colabo, bool reverse) { _colabo = colabo; _reverse = reverse; }

      public FormColabo colabo 
      {
        get { return _colabo; }
      }
      public bool reverse
      {
        get { return _reverse; }
        set { _reverse=value; }
      }
      public abstract IComparer<IndexItem> GetComparer();
    }

    /// comparer to provide "plain" itemGrid sorting (not taking account any tree hierarchy)
    public class CompareTitles : CompareIndices, IComparer<IndexItem>
    {
      public CompareTitles(FormColabo colabo, bool reverse) : base(colabo, reverse) {}

      public int Compare(IndexItem a, IndexItem b)
      {
        ArticleItem itemA; if (!a._articleId._database.cache.TryGetValue(a._articleId._id, out itemA)) return 0;
        ArticleItem itemB; if (!b._articleId._database.cache.TryGetValue(b._articleId._id, out itemB)) return 0;
        return reverse ? (-itemA._title.CompareTo(itemB._title)) : (itemA._title.CompareTo(itemB._title));
      }
      public override IComparer<IndexItem> GetComparer() { return this; }
    }

    public class CompareAuthors : CompareIndices, IComparer<IndexItem>
    {
      public CompareAuthors(FormColabo colabo, bool reverse) : base(colabo, reverse) {}

      public int Compare(IndexItem a, IndexItem b)
      {
        ArticleItem itemA; if (!a._articleId._database.cache.TryGetValue(a._articleId._id, out itemA)) return 0;
        ArticleItem itemB; if (!b._articleId._database.cache.TryGetValue(b._articleId._id, out itemB)) return 0;
        return reverse ? (-itemA._author.CompareTo(itemB._author)) : (itemA._author.CompareTo(itemB._author));
      }
      public override IComparer<IndexItem> GetComparer() { return this; }
    }

    public class CompareDates : CompareIndices, IComparer<IndexItem>
    {
      public CompareDates(FormColabo colabo, bool reverse) : base(colabo, reverse) {}

      public int Compare(IndexItem a, IndexItem b)
      {
        if (a._level>0) //not root items should be always sorted the same way (new down) 
          return (a._date.CompareTo(b._date));
        else
          return reverse ? (-a._date.CompareTo(b._date)) : (a._date.CompareTo(b._date));
      }
      public override IComparer<IndexItem> GetComparer() { return this; }
    }

    public class CompareCustom : CompareIndices, IComparer<IndexItem>
    {
      public CompareCustom(FormColabo colabo, bool reverse) : base(colabo, reverse) { }

      public int Compare(IndexItem a, IndexItem b)
      {
        if (a._level > 0) // not root items should be always sorted the same way (new down) 
          return (a._date.CompareTo(b._date));
        else
        {
          // sort threads primary by sort order, secondary by date
          int diff = a._sortOrder.CompareTo(b._sortOrder);
          if (diff != 0) return reverse ? -diff : diff;
          diff = a._date.CompareTo(b._date);
          return reverse ? -diff : diff;
        }
      }
      public override IComparer<IndexItem> GetComparer() { return this; }
    }

    public class IndexComparer : IComparer<IndexItem>
    {
      private IComparer<IndexItem> compareFunct;
      public IndexComparer(IComparer<IndexItem> compareFunct) { this.compareFunct = compareFunct; }

      public int Compare(IndexItem a, IndexItem b)
      {
        while (a._parent != b._parent)
        {
          if (a._level > b._level)
          {
            if (a._parent == b) return 1; //'a' must be after 'b', parents always first, children should obey
            a = a._parent;
          }
          else if (b._level > a._level)
          {
            if (b._parent == a) return -1;
            b = b._parent;
          }
          else //must be equal
          {
            a = a._parent;
            b = b._parent;
          }
        }
        int retVal = compareFunct.Compare(a, b);
        if (retVal != 0) return retVal;

        // compare by unique id
        // Note: using unique sorting is necessary! Without it, the Sort does not work (intransitive ordering)
        // Example:
        //    * 1
        //        2 Node A
        //    * 1
        //        3 Node B
        //        4 Node C
        // Comparing by numbers we have: NodeB<NodeC, but NodeB==NodeA and NodeC==NodeA implies NodeB==NodeC which is contradiction!

        retVal = a._articleId._database.name.CompareTo(b._articleId._database.name);
        if (retVal != 0) return retVal;

        return a._articleId._id - b._articleId._id;
      }
    }
    private void UpdateSortComparer()
    {
      // Hide SortGlyphDirection for all columns
      foreach (DataGridViewColumn column in dataGridView1.Columns)
      {
        column.HeaderCell.SortGlyphDirection = SortOrder.None;
      }
      if (string.IsNullOrEmpty(_sortOrderMethod)) return;

      // create the _selectedComparer based on the type name
      Assembly assembly = Assembly.GetExecutingAssembly();
      _selectedComparer = (CompareIndices)assembly.CreateInstance(_sortOrderMethod, false, BindingFlags.ExactBinding, null,
        new object[] { this, _sortOrderDir }, null, null);
      if (_selectedComparer == null) return; // no sort

      DataGridViewColumn sortColumn = null;
      if (_selectedComparer is CompareTitles) sortColumn = dataGridView1.Columns[_columnTitle];
      else if (_selectedComparer is CompareAuthors) sortColumn = dataGridView1.Columns[_columnAuthor];
      else if (_selectedComparer is CompareDates) sortColumn = dataGridView1.Columns[_columnDate];
      else if (_selectedComparer is CompareCustom) sortColumn = dataGridView1.Columns[_columnIcon];

      if (sortColumn != null) sortColumn.HeaderCell.SortGlyphDirection = _selectedComparer.reverse ? SortOrder.Descending : SortOrder.Ascending;
    }
    #endregion //article sorting

    /// CurrentRaw and all articles from the same TREE are highlighted by special background color
    private void UpdateBackgroundColor()
    {
      for (int i = 0; i < dataGridView1.Rows.Count; i++)
        dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.FromKnownColor(KnownColor.White);
      if (dataGridView1.CurrentRow!=null)
      {
        int ix = dataGridView1.CurrentRow.Index;
        if (ix < _index.Count)
        {
          Color bcol = Color.FromArgb(0xff, 0xff, 0xd0);
          for (int i = ix; _index[i]._level >= 0; i--)
          {
            dataGridView1.Rows[i].DefaultCellStyle.BackColor = bcol;
            if (_index[i]._level == 0) break;
          }
          for (int i = ix + 1; i < _index.Count && _index[i]._level > 0; i++)
            dataGridView1.Rows[i].DefaultCellStyle.BackColor = bcol;
        }
      }
    }

    // IMessageFilter implementation
    // this will help us to handle keyboard event before offered to focused control
    private bool TryToolStripItemShortcut(ToolStripItem item, Keys keyData)
    {
      if (item is ToolStripMenuItem)
      {
        if (item.Tag != null && item.Tag.Equals("list"))
        {
          // items with "list" tag are handled only when list of articles is focused
          if (!dataGridView1.Focused && !dayView.Focused) return false; // not handled
        }

        Keys keys = ((ToolStripMenuItem)item).ShortcutKeys;
        if (keyData == keys)
        {
          // redirect the shortcut
          if (dataGridView1.CurrentCell != null)
          {
            int row = dataGridView1.CurrentCell.RowIndex;
            if (row >= 0)
            {
              UpdateContextMenu(row);
              item.PerformClick();
              return true; // handled
            }
          }
        }
      }
      if (item is ToolStripDropDownItem)
      { // recursively check all dropdown items shortucts
        foreach (ToolStripItem dropDownItem in (item as ToolStripDropDownItem).DropDownItems)
        {
          if (TryToolStripItemShortcut(dropDownItem, keyData)) return true;
        }
      }
      return false; // not handled
    }

    public bool PreFilterMessage(ref Message m)
    {
      if (Form.ActiveForm != this) return false;

      if (m.Msg == 0x100) // WM_KEYDOWN
      {          
        Keys keys = ((Keys)(int)m.WParam & Keys.KeyCode) | Control.ModifierKeys;
        return HandleKeyDown(keys);
      }
      else return false;
    }

    private bool HandleKeyDown(Keys keyData)
    {
      // handle hot keys
      // do not handle shortcuts when textarea or input box is active within HTML code
      HtmlElement activeElement = webBrowser1.Document.ActiveElement;
      if (activeElement != null)
      {
        string htmlTag = activeElement.TagName;
        if (htmlTag.Equals("input", StringComparison.OrdinalIgnoreCase) || htmlTag.Equals("textarea", StringComparison.OrdinalIgnoreCase))
        {
          return false; //not handled
        }
      }

      // If address bar focused, process shortcuts
      if (_addressBar.Focused)
      {
        AddressBar.ProcessShortcutResult res = _addressBar.ProcessShortcut(keyData);
        if (res == AddressBar.ProcessShortcutResult.Processed)
          return true;
        else if (res == AddressBar.ProcessShortcutResult.PassToHandler)
          return false;
      }

      // first, check the context menu for shortcuts
      foreach (ToolStripItem item in contextMenuArticle.Items)
      {
        if (TryToolStripItemShortcut(item, keyData)) return true;
      }
      // check the main menu for shortcuts
      foreach (ToolStripItem item in menuStrip1.Items)
      {
        if (TryToolStripItemShortcut(item, keyData)) return true;
      }

      // other shortcuts
      if ((keyData & Keys.Modifiers) == Keys.None && (keyData & Keys.KeyCode) == Keys.M)
      {
        // mark article as read / unread based on context
        bool someUnread = false;
        if (tabControl1.SelectedTab == tabTasks)
        {
          foreach (DataGridViewRow row in dataGridView1.SelectedRows)
          {
            ArticleItemWithDb item = GetArticle(row.Index);
            if (!item.IsNone())
            {
              if (item._item._changed) someUnread = true;
            }
          }
        }
        else if (tabControl1.SelectedTab == tabCalendar)
        {
          // in calendar view, mark only the current article
          if (dataGridView1.CurrentCell != null)
          {
            int selectedRow = dataGridView1.CurrentCell.RowIndex;
            if (selectedRow >= 0 && selectedRow < _index.Count)
            {
              ArticleItemWithDb article = GetArticle(selectedRow);
              if (!article.IsNone() && article._item._changed) someUnread = true;
            }
          }
        }
        if (someUnread) MarkAsRead();
        else MarkAsUnread();
        return true; // handled
      }
      else if ((keyData & Keys.Modifiers) == Keys.None && (keyData & Keys.KeyCode) == Keys.Back)
      { // navigate back
        navigateBackButton_Click(this, new EventArgs());
        return true;
      }
      else if ((keyData & Keys.Modifiers) == Keys.Shift && (keyData & Keys.KeyCode) == Keys.Back)
      { // navigate forward
        navigateForwardButton_Click(this, new EventArgs());
        return true;
      }
      // Actions with more than one shortcut keys combination must be handled separately to menu shortcuts
      // we can also check the shortcut key already tested in menu shortcuts only to see immediately here what is assigned
      if ((keyData & Keys.Modifiers) == Keys.Control && (keyData & Keys.KeyCode) == Keys.F ||
        (keyData & Keys.Modifiers) == Keys.None && (keyData & Keys.KeyCode) == Keys.F3)
      { // Ctrl+F or F3 for Search
        findButton_Click(this, new EventArgs());
        return true;
      }
      if ((keyData & Keys.Modifiers) == Keys.Control && (keyData & Keys.KeyCode) == Keys.Insert)
      {
        if (dataGridView1.Focused || dayView.Focused)
        {
          _contextMenuRow = -1;
          if (dataGridView1.CurrentCell != null) _contextMenuRow = dataGridView1.CurrentCell.RowIndex;
          copyColaboShortcutToolStripMenuItem_Click(null, null);
          return true; // handled
        }
      }
      if ( ((keyData & Keys.Modifiers) == Keys.Control && (keyData & Keys.KeyCode) == Keys.U)
        || ((keyData & Keys.Modifiers) == Keys.None && (keyData & Keys.KeyCode) == Keys.N) )
      {
        NavigateNextUnread();
        return true; //handled
      }

      return false; // not handled
    }

    // Article preview
    private void webBrowser1_Navigated(object sender, WebBrowserNavigatedEventArgs e)
    {
      if (webBrowser1.httpErrorCode == 401 && !browserProxy.PromptForCredentialsCanceled) // 401 Unauthorized
      {
        webBrowser1.httpErrorCode = 200; //reset http response code
        browserProxy.NavigateUrl = e.Url.AbsoluteUri;
        browserProxy.realm = "";
        browserProxy.ForcePromptForCredentials = true;
        webBrowser1.Navigate(browserProxy.NavigateUrl);
      }
      else 
      {
        browserProxy.PromptForCredentialsCanceled = false;
        browserProxy.ForcePromptForCredentials = false;
      }
    }
    private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
      if (webBrowser1.Document != null && !string.IsNullOrEmpty(_currentURL))
      {
        // update tooltip of the links
        Uri baseURL = new Uri(_currentURL);
        foreach (HtmlElement element in webBrowser1.Document.Links)
        {
          string title = element.GetAttribute("title");
          if (title.Length == 0)
          {
            string href = element.GetAttribute("href");
            if (href.StartsWith(_prefixTag))
            {
            }
            else
            {
              Uri url = new Uri(href);
              if (url.Scheme.Equals("about", StringComparison.OrdinalIgnoreCase))
              {
                if (url.LocalPath.Equals("blank", StringComparison.OrdinalIgnoreCase)) continue;
                if (!Uri.TryCreate(baseURL, url.LocalPath, out url)) continue;
              }
              element.SetAttribute("title", url.ToString());
            }
          }
        }
        // Connect events
        webBrowser1.Document.MouseOver -= new HtmlElementEventHandler(BrowserDocument_MouseOver);
        webBrowser1.Document.MouseOver += new HtmlElementEventHandler(BrowserDocument_MouseOver);
      }

      // refresh the list of articles when filter changes
      string cmd = e.Url.ToString();
      if (cmd.StartsWith(_prefixTag))
      {
        UpdateArticles(ArticleId.None);
        return;
      }
    }

    [DllImport("shell32.dll", EntryPoint = "FindExecutable")]
    public static extern long FindExecutableA(string lpFile, string lpDirectory, StringBuilder lpResult);

    public static string FindExecutable(string filename)
    {
      StringBuilder objResultBuffer = new StringBuilder(1024);
      long lngResult = 0;
      lngResult = FindExecutableA(filename, string.Empty, objResultBuffer);
      if (lngResult >= 32)
      {
        return objResultBuffer.ToString();
      }
      return null;
    }

    public static string GetRegisteredApplication(string StrExt)
    {
      Microsoft.Win32.RegistryKey oHKCR; //HKEY CLASSES ROOT
      string StrProgID;
      string StrExe;
      Microsoft.Win32.RegistryKey oProgID;
      Microsoft.Win32.RegistryKey oOpenCmd;
      int TempPos;

      try
      {
        // Add starting dot to extension
        // StrExt = "." + FileExtension;
        // Get Programmatic Identifier for this extension
        try
        {
          oHKCR = Microsoft.Win32.Registry.ClassesRoot;
          oProgID = oHKCR.OpenSubKey(StrExt);
          StrProgID = oProgID.GetValue(null).ToString();
          oProgID.Close();
        }
        catch //(System.Exception ex)
        {
          // No ProgID, return null
          return null;
        }
        // Get associated application
        try
        {
          oOpenCmd = oHKCR.OpenSubKey(StrProgID + @"\shell\open\command");
          StrExe = oOpenCmd.GetValue(null).ToString();
          oOpenCmd.Close();
        }
        catch //(System.Exception ex)
        {
          // Missing default application
          return null;
        }
        TempPos = StrExe.IndexOf(" %1");
        if (TempPos > 0)
        {
          StrExe = StrExe.Substring(0, TempPos);
        }
        // else // No %1 placeholder found, return strexe
        return StrExe;
      }
      catch //(System.Exception ex)
      {
        return null;
      }
    }

    private void webBrowser1_Navigating(object sender, WebBrowserNavigatingEventArgs e)
    {
      if (e.Url.Equals(_currentURL)) return; // need not navigate elsewhere

      // check for special links
      string cmd = e.Url.ToString();
      if (cmd.StartsWith(_prefixLang))
      {
        string lang = cmd.Substring(_prefixLang.Length, cmd.Length - _prefixLang.Length - 1);
        e.Cancel = true;
        _languages.Clear();
        if (lang.Length > 0) _languages.Add(lang);
        LoadCurrentArticle();
        return;
      }
      if (cmd.StartsWith(_prefixTag))
      {
        string tag = cmd.Substring(_prefixTag.Length, cmd.Length - _prefixTag.Length - 1);
        // do not offer already selected tags
        if (_tags.Contains(new TagCondition(tag, true)))
          e.Cancel = true;
        else
        {
          _tags.Add(new TagCondition(tag, true));
          ResetTagCloud();
          UpdateTitle();
          // update articles later (to allow control changes)
        }
        return;
      }

      // try to resolve relative paths
      Uri newURL = null;
      if (e.Url.Scheme.Equals("about", StringComparison.OrdinalIgnoreCase))
      {
        if (e.Url.LocalPath.Equals("blank", StringComparison.OrdinalIgnoreCase)) return;
        Uri baseURL = new Uri(_currentURL);
        Uri.TryCreate(baseURL, e.Url.LocalPath, out newURL);
      }
      if (newURL == null) newURL = e.Url;

      if (GoToArticle(newURL))
      {
        // navigation done
        browserProxy.NavigateUrl = newURL.ToString();
        browserProxy.realm = "";
        e.Cancel = true;
      }
      else if (!newURL.Host.Equals("none", StringComparison.OrdinalIgnoreCase))
      {
        // open in external application
        // test, whether there is application associated with this extension 
        
        // list of extensions we do not want to open from temporary file (in lowercase)
        string[] openDirectly = { ".html", ".htm", ".aspx" };

        string ext = Path.GetExtension(newURL.AbsolutePath);
        if (string.IsNullOrEmpty(ext) || Array.IndexOf(openDirectly, ext.ToLower()) >= 0 ||
          string.IsNullOrEmpty(GetRegisteredApplication(ext)))
        {
          // empty or known or unregistered extension
          System.Diagnostics.Process.Start(newURL.ToString());
          e.Cancel = true;
          return;
        }

        // can be opened by an external application, download to a temp file
        string tempDir = System.IO.Path.GetTempPath();
        tempDir += @"Colabo\";
        if (!Directory.Exists(tempDir)) Directory.CreateDirectory(tempDir);
        try
        {
          string filename = newURL.ToString();
          int ix = filename.LastIndexOf('/');
          if (ix>=0) 
          {
            filename = tempDir + filename.Substring(ix+1);
            if (File.Exists(filename))
            {
              int i = 0;
              string filenameCore = filename;
              int ixExt = filenameCore.LastIndexOf('.');
              string filenameExt = "";
              if (ixExt >= 0)
              {
                filenameExt = filenameCore.Substring(ixExt);
                filenameCore = filenameCore.Substring(0, ixExt);
              }
              do
              {
                i++;
                filename = string.Format("{0}_{1:D}{2}", filenameCore, i, filenameExt);
              } while (File.Exists(filename));
            }
            // download the file to the temporary file and open it
            System.Net.WebClient Client = new WebClient();
            Client.Credentials = Program.config.credentialCache;
            // TODO: better reaction to authentication failure
            Client.DownloadFile(newURL, filename);
            Client.Dispose();
            File.SetAttributes(filename, FileAttributes.ReadOnly);
            System.Diagnostics.Process.Start(filename);
          }
          else System.Diagnostics.Process.Start(newURL.ToString());
        }
        catch //(System.Exception ex)
        { // something went wrong, download it into external browser from original network position
          System.Diagnostics.Process.Start(newURL.ToString());
        }
        finally
        {
          e.Cancel = true;
        }
      }
    }
    
    // Find ArticleId of possible Colabo article matching given url
    public ArticleId FindArticleID(Uri url)
    {
      string server = url.Host;
      // check if the address references to some colabo article
      foreach (SQLDatabase db in _databases)
      {
        foreach (ArticleItem item in db.cache.Values)
        {
          string itemURL = item.AbsoluteURL(db);
          if (!string.IsNullOrEmpty(itemURL) && url.Equals(new Uri(itemURL)))
          {
            return new ArticleId(db, item._id);
          }
        }
      }
      return ArticleId.None;
    }

    public bool FindArticle(Uri url, out List<string> tags, out SQLDatabase database, out int id)
    {
      tags = null;
      database = null;
      id = -1;

      Uri urlShort = url;
      // parse url query to find hints
      string server = null, dbName = null;
      if (!string.IsNullOrEmpty(url.Query))
      {
        // parse the query
        string[] args = url.Query.Split(new char[] { '?', '&', ';' });
        foreach (string arg in args)
        {
          int index = arg.IndexOf('=');
          if (index < 0) continue;
          string name = arg.Substring(0, index);
          string value = arg.Substring(index + 1);
          if (name.Equals("server", StringComparison.OrdinalIgnoreCase)) server = value;
          else if (name.Equals("database", StringComparison.OrdinalIgnoreCase)) dbName = value;
          else if (name.Equals("id", StringComparison.OrdinalIgnoreCase)) int.TryParse(value, out id);
          else if (name.Equals("tags", StringComparison.OrdinalIgnoreCase))
          {
            string[] tagNames = value.Split(new char[] { ',' });
            tags = new List<string>();
            foreach (string tag in tagNames) tags.Add(tag.Trim().ToLower());
          }
        }

        // remove query from the url
        UriBuilder builder = new UriBuilder(url.Scheme, url.Host, url.Port, url.AbsolutePath);
        urlShort = builder.Uri;
      }

      // default values
      if (server == null)
      {
        server = urlShort.Host;
      }
      if (dbName == null)
      {
        // get the first segment of path
        char[] delimiters = new char[] { '\\', '/' };
        string path = urlShort.AbsolutePath;
        int start = path.IndexOfAny(delimiters, 0);
        int end;
        if (start == 0)
        {
          start = 1;
          end = path.IndexOfAny(delimiters, start);
        }
        else
        {
          end = start;
          start = 0;
        }
        if (end > start) dbName = path.Substring(start, end - start);
      }

      // check if article is in some opened database
      if (server != null && dbName != null)
      {
        database = _databases.Find(delegate(SQLDatabase item)
        {
          return item.server.Equals(server, StringComparison.OrdinalIgnoreCase) &&
            item.database.Equals(dbName, StringComparison.OrdinalIgnoreCase);
        });
      }
      // try to use the hints
      if (database != null && id >= 0)
      {
        // check if the article id is correct
        ArticleItem item;
        if (database.cache.TryGetValue(id, out item) && urlShort.Equals(new Uri(item.AbsoluteURL(database)))) return true;
      }

      // check if the address references to some colabo article
      foreach (SQLDatabase db in _databases)
      {
        foreach (ArticleItem item in db.cache.Values)
        {
          string itemURL = item.AbsoluteURL(db);
          if (!string.IsNullOrEmpty(itemURL) && urlShort.Equals(new Uri(itemURL)))
          {
            database = db;
            id = item._id;
            return true; // found
          }
        }
      }
      return false; // not found
    }

    public bool CheckArticleExists(Uri url)
    {
      List<string> tags;
      SQLDatabase database;
      int id;

      bool found = FindArticle(url, out tags, out database, out id);
      return found;
    }

    public bool GoToArticle(Uri url)
    {
      List<string> tags;
      SQLDatabase database;
      int id;

      bool found = FindArticle(url, out tags, out database, out id);

      // change tags if given in query
      if (tags != null)
      {
        _tags = new List<TagCondition>();
        foreach (string tag in tags)
        {
          if (string.IsNullOrEmpty(tag)) continue;
          if (tag[0] == '!')
            _tags.Add(new TagCondition(tag.Substring(1), false));
          else
            _tags.Add(new TagCondition(tag, true));
        }

        _languages.Clear();
        ResetTagCloud();
        UpdateTitle();
        UpdateArticles(ArticleId.None);
        UpdateNavigateHistory();
      }

      // jump to the article
      if (found)
      {
        GoToArticle(database, id);
        return true;
      }

      return false;
    }
    public void GoToArticle(SQLDatabase database, int id)
    {
      // if the given article is Enabled using current (tag) filter, expand the tree to the root and UpdateArticles
      ArticleItem item;
      if (!database.cache.TryGetValue(id, out item)) return;

      ArticleItem.VTCache cache = new ArticleItem.VTCache(database, item);
      if ((item.GetAccessRights(database, cache) & AccessRights.Read) == 0) return; // cannot navigate to unaccessible article

      // check if the article is already visible
      int row = _index.FindIndex(delegate(IndexItem indexItem)
        {
          return indexItem._articleId._database == database && indexItem._articleId._id == id;
        });
      if (row >= 0)
      {
        dataGridView1.CurrentCell = dataGridView1.Rows[row].Cells[0];
        return;
      }

      // check if the article is in collapsed subtree
      if (item.IsEnabled(_tags, _currentRuleSetCompiled, database, cache)) // do not add the article twice
      {
        // expand all items from this item up to tree root
        ArticleItem itemx = item;
        while (itemx._parent >= 0)
        {
          if (!database.cache.TryGetValue(itemx._parent, out itemx)) break;
          itemx._expanded = true;
        }
        UpdateArticles(new ArticleId(database, id));
        return;
      }
      // article is not Enabled, add temporarily article to the view
      IndexItem index = new IndexItem(database, id, 0, null, false);
      _index.Add(index);
      dataGridView1.RowCount = _index.Count;
      row = _index.Count - 1;
      dataGridView1.CurrentCell = dataGridView1.Rows[row].Cells[0];
    }
    // List of articles
    private void dataGridView1_CurrentCellChanged(object sender, EventArgs e)
    {
      if (!_suppressChangeRow)
      {
        UpdateBackgroundColor();
        _languages.Clear();
        LoadCurrentArticle();
      }
    }
    // Prepare context menu for selected article
    private void UpdateContextMenu(int r)
    {
      _contextMenuRow = r;

      // check if the current row is selected
      bool selected = false;
      foreach (DataGridViewRow row in dataGridView1.SelectedRows)
      {
        if (row.Index == _contextMenuRow)
        {
          selected = true;
          break;
        }
      }
      if (!selected) dataGridView1.CurrentCell = dataGridView1.Rows[_contextMenuRow].Cells[0];

      // show / hide items in the context menu
      bool someUnread = false;
      bool someRead = false;
      bool disableReply = false;
      bool containsVirtual = false;
      bool canEdit = false;
      foreach (DataGridViewRow row in dataGridView1.SelectedRows)
      {
        ArticleItemWithDb item = GetArticle(row.Index);
        if (!item.IsNone())
        {
          if (item._item._changed) someUnread = true;
          if (!item._item._toRead) someRead = true;
          if (!string.IsNullOrEmpty(item._item._draftLocal)) disableReply = true;
          if (item._item._chatId >= 0) disableReply = true;
          if (item._item._id < 0) containsVirtual = true;
          ArticleItem.VTCache cache = new ArticleItem.VTCache(item._database, item._item);
          canEdit |= (item._item.GetAccessRights(item._database, cache) & AccessRights.Edit) != 0;
        }
      }

      int index = contextMenuArticle.Items.IndexOfKey("markAsReadToolStripMenuItem");
      if (index >= 0) contextMenuArticle.Items[index].Visible = someUnread;
      index = contextMenuArticle.Items.IndexOfKey("markAsUnreadToolStripMenuItem");
      if (index >= 0) contextMenuArticle.Items[index].Visible = someRead;
      index = contextMenuArticle.Items.IndexOfKey("replyToolStripMenuItem");
      if (index >= 0) contextMenuArticle.Items[index].Visible = dataGridView1.SelectedRows.Count == 1 && !disableReply;
      index = contextMenuArticle.Items.IndexOfKey("appendNoteToolStripMenuItem");
      if (index >= 0) contextMenuArticle.Items[index].Visible = canEdit && dataGridView1.SelectedRows.Count == 1 && !disableReply;
      index = contextMenuArticle.Items.IndexOfKey("editToolStripMenuItem");
      if (index >= 0) contextMenuArticle.Items[index].Visible = canEdit && dataGridView1.SelectedRows.Count == 1;
      index = contextMenuArticle.Items.IndexOfKey("deleteToolStripMenuItem");
      if (index >= 0) contextMenuArticle.Items[index].Visible = canEdit && dataGridView1.SelectedRows.Count == 1;
      index = contextMenuArticle.Items.IndexOfKey("setAsHomePageToolStripMenuItem");
      if (index >= 0) contextMenuArticle.Items[index].Visible = dataGridView1.SelectedRows.Count == 1 && !containsVirtual;
      index = contextMenuArticle.Items.IndexOfKey("bookmarkThisArticleToolStripMenuItem");
      if (index >= 0) contextMenuArticle.Items[index].Visible = dataGridView1.SelectedRows.Count == 1 && !containsVirtual;
    }

    private void dataGridView1_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
    {
      if (e.RowIndex >= 0)
      {
        if (e.ColumnIndex == _columnIcon)
        {
          // change selection if needed
          if (dataGridView1.SelectedRows.Count != 1 || dataGridView1.SelectedRows[0].Index != e.RowIndex)
            dataGridView1.CurrentCell = dataGridView1.Rows[e.RowIndex].Cells[_columnIcon];
          // create a menu for selected article
          ArticleItemWithDb item = GetArticle(e.RowIndex);
          if (!item.IsNone() && item._item._id >= 0) e.ContextMenuStrip = CreateTagsMenu(item);
        }
        else
        {
          UpdateContextMenu(e.RowIndex);
          // attach the context menu
          e.ContextMenuStrip = contextMenuArticle;
        }
      }
    }
    // create the context menu for icons column (toggle flags)
    private ContextMenuStrip CreateTagsMenu(ArticleItemWithDb item)
    {
      ContextMenuStrip contextMenuTags = new ContextMenuStrip(components);

      // testing item - TODO: foreach tag from database | configuration
      string[] tags = { ".important", "/done", "/closed", "/bug:/fixed" };
      foreach (string tag in tags)
      {
        string tagToSet = tag;
        string tagToRequire = null;
        string[] split = tag.Split(":".ToCharArray());
        if (split.Length == 2)
        {
          tagToRequire = split[0];
          tagToSet = split[1];
        }
        if (tagToRequire==null || item._item.HasTag(tagToRequire))
        {
          ToolStripMenuItem menuItem = new ToolStripMenuItem();
          contextMenuTags.Items.Add(menuItem);
          menuItem.Name = "toggleTag";
          menuItem.Click += new EventHandler(toggleTag_Click);
          if (item._item.HasTag(tagToSet))
          {
            menuItem.Tag = new ToggleTagParams(item, tagToSet, true);
            menuItem.Text = string.Format("No Longer {0}", HumanReadable(tagToSet));
          }
          else
          {
            menuItem.Tag = new ToggleTagParams(item, tagToSet, false);
            menuItem.Text = string.Format("Mark As {0}", HumanReadable(tagToSet));
          }
        }
      }

      return contextMenuTags;
    }
    class ToggleTagParams
    {
      public ArticleItemWithDb _item;
      public string _tag;
      public bool _remove;

      public ToggleTagParams(ArticleItemWithDb item, string tag, bool remove)
      {
        _item = item;
        _tag = tag;
        _remove = remove;
      }
    }
    private void toggleTag_Click(object sender, EventArgs e)
    {
      ToolStripMenuItem menuItem = (ToolStripMenuItem)sender;
      ToggleTagParams pars = (ToggleTagParams)menuItem.Tag;
      if (string.IsNullOrEmpty(pars._tag)) return;

      SQLDatabase database = pars._item._database;
      ArticleItem item = pars._item._item;
      if (item._id < 0) return;

      // add / remove the tag
      string tag = pars._tag;
      if (string.IsNullOrEmpty(tag)) return;
      string removeTag = null;
      string addTag = null;
      
      if (pars._remove)
      {
        // remove the tag
        if (item._ownTags != null)
        {
          if (item._ownTags.Contains(tag)) removeTag = tag;
          else if (item._ownTags.Contains("#" + tag)) removeTag = "#" + tag;
        }
        if (item._inheritedTags != null && item._inheritedTags.Contains(tag)) addTag = "-" + tag;
      }
      else
      {
        // add the tag
        if (item._ownTags != null)
        {
          if (item._ownTags.Contains("-" + tag)) removeTag = "-" + tag;
          else if (item._ownTags.Contains("#-" + tag)) removeTag = "#-" + tag;
        }
        if (item._inheritedTags == null || !item._inheritedTags.Contains(tag)) addTag = tag;
      }
      database.ReplaceTag(item._id, addTag, removeTag);
      if (tag[0] != TagSpecialChars.TagPrivate)
      {
        // save tags to SVN
        if (removeTag != null) item._ownTags.Remove(removeTag);
        if (addTag != null)
        {
          if (item._ownTags == null) item._ownTags = new List<string>();
          item._ownTags.Add(addTag);
        }
        ITask task = new TaskUpdateTags(database, item);
        AddTask(task);
      }
      // synchronize is implemented through task, so will be executed after TaskUpdateTags finished
      Synchronize(database, ArticleId.None); // only affected database
    }
    // Content of the cell (virtual mode support)
    private void dataGridView1_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
    {
      // find the related article info
      if (e.RowIndex < 0 || e.RowIndex >= _index.Count)
      {
        EmptyResult(e);
        return;
      }
      ArticleId id = _index[e.RowIndex]._articleId;
      ArticleItem item;
      if (!id._database.cache.TryGetValue(id._id, out item))
      {
        EmptyResult(e);
        return;
      }

      switch (e.ColumnIndex)
      {
        case _columnIcon:
          // icon

          // collect the actions of the article and collapsed subtree
          List<string> actions, subtreeActions;
          item.CollectActions(out actions, out subtreeActions, id._database, _currentRuleSetCompiled);

          List<object> icons = new List<object>();
          const string prefixIcon = "icon:";

          for (int i = actions.Count - 1; i >= 0; i--)
          {
            string action = actions[i];
            if (action.StartsWith(prefixIcon))
            {
              string iconName = action.Substring(prefixIcon.Length);
              if (_icons.ContainsKey(iconName)) icons.Add(new IconInfo(_icons[iconName], false));
            }
          }
          for (int i = subtreeActions.Count - 1; i >= 0; i--)
          {
            string action = subtreeActions[i];
            if (action.StartsWith(prefixIcon) && !actions.Contains(action))
            {
              string iconName = action.Substring(prefixIcon.Length);
              if (_icons.ContainsKey(iconName)) icons.Add(new IconInfo(_icons[iconName], true));
            }
          }

          e.Value = icons;
          break;
        case _columnTitle:
          // title - pass id, the custom painting is used
          e.Value = e.RowIndex;
          break;
        case _columnAuthor:
          // author
          e.Value = id._database.UserDisplayName(item._author);
          break;
        case _columnDate:
          // date
          e.Value = item._date;
          break;
        default:
          e.Value = "";
          break;
      }
    }
    class IconInfo
    {
      public object _icon;
      public bool _small;

      public IconInfo(object icon, bool small) { _icon = icon; _small = small; }
    }

    private void EmptyResult(DataGridViewCellValueEventArgs e)
    {
      if (e.ColumnIndex == _columnIcon) e.Value = new List<object>();
      else e.Value = "";
    }
    private void dataGridView1_CellToolTipTextNeeded(object sender, DataGridViewCellToolTipTextNeededEventArgs e)
    {
      // modify tooltip of the title column
      if (e.ColumnIndex != _columnTitle) return;

      // find the related article info
      if (e.RowIndex < 0 || e.RowIndex >= _index.Count)
      {
        e.ToolTipText = "";
        return;
      }
      ArticleId id = _index[e.RowIndex]._articleId;
      ArticleItem item;
      if (!id._database.cache.TryGetValue(id._id, out item))
      {
        e.ToolTipText = "";
        return;
      }

      // show URL
      e.ToolTipText = item.AbsoluteURL(id._database);
    }

    internal static Color ColorFromActions(List<string> actions, Color defaultColor)
    {
      const string prefixColor = "color:";

      for (int i = 0; i < actions.Count; i++)
      {
        string action = actions[i];
        if (action.StartsWith(prefixColor))
        {
          // color given by #000, #000000 or name
          string colorName = action.Substring(prefixColor.Length);
          if (colorName.Length > 0)
          {
            if (colorName[0] == '#')
            {
              if (colorName.Length == 4)
              {
                return Color.FromArgb(
                  ToHex(colorName[1]) * 17,
                  ToHex(colorName[2]) * 17,
                  ToHex(colorName[3]) * 17);
              }
              else if (colorName.Length == 7)
              {
                return Color.FromArgb(
                  ToHex(colorName[1]) * 16 + ToHex(colorName[2]),
                  ToHex(colorName[3]) * 16 + ToHex(colorName[4]),
                  ToHex(colorName[5]) * 16 + ToHex(colorName[6]));
              }
            }
            else
            {
              return Color.FromName(colorName);
            }
          }
        }
      }

      return defaultColor;
    }
    static private int ToHex(char c)
    {
      if (c >= '0' && c <= '9') return c - '0';
      if (c >= 'A' && c <= 'F') return c - 'A' + 10;
      if (c >= 'a' && c <= 'f') return c - 'a' + 10;
      return 0;
    }

    // Custom Title cell for DataGridView control (custom painting)
    private class DataGridViewTitleCell : DataGridViewTextBoxCell
    {
      protected VisualStyleRenderer _openGlyph = null;
      protected VisualStyleRenderer _closedGlyph = null;

      const int glyphWidth = 20;
      const int indentWidth = 20;

      public DataGridViewTitleCell()
      {
        if (VisualStyleRenderer.IsSupported)
        {
          if (VisualStyleRenderer.IsElementDefined(VisualStyleElement.TreeView.Glyph.Opened))
            _openGlyph = new VisualStyleRenderer(VisualStyleElement.TreeView.Glyph.Opened);
          if (VisualStyleRenderer.IsElementDefined(VisualStyleElement.TreeView.Glyph.Closed))
            _closedGlyph = new VisualStyleRenderer(VisualStyleElement.TreeView.Glyph.Closed);
        }
      }

      protected override void Paint(
          Graphics graphics, Rectangle clipBounds, Rectangle cellBounds,
          int rowIndex,
          DataGridViewElementStates cellState,
          object value, object formattedValue, string errorText,
          DataGridViewCellStyle cellStyle,
          DataGridViewAdvancedBorderStyle advancedBorderStyle,
          DataGridViewPaintParts paintParts)
      {
        // try to retrieve the article
        FormColabo owner = (FormColabo)this.DataGridView.Tag;
        if (owner == null) return;
        if (!(value is int)) return;
        int row = (int)value;
        ArticleItemWithDb item = owner.GetArticle(row);
        if (item.IsNone()) return;
        int level = owner.GetLevel(row);

        // offset child articles, make some place for + / - glyphs
        int offset = glyphWidth + indentWidth * level;
        cellStyle.Padding = new Padding(cellStyle.Padding.Left + offset, cellStyle.Padding.Top,
            cellStyle.Padding.Right, cellStyle.Padding.Bottom);

        // collect the actions of the article and collapsed subtree
        List<string> actions, subtreeActions;
        item._item.CollectActions(out actions, out subtreeActions, item._database, owner._currentRuleSetCompiled);

        // change font by the rule set
        FontStyle style = FontStyle.Regular;
        if (actions.Contains("bold") || subtreeActions.Contains("bold")) style |= FontStyle.Bold;
        if (actions.Contains("italic") || subtreeActions.Contains("italic")) style |= FontStyle.Italic;
        if (actions.Contains("strikeout") || subtreeActions.Contains("strikeout")) style |= FontStyle.Strikeout;
        if (actions.Contains("underline") || subtreeActions.Contains("underline")) style |= FontStyle.Underline;
        cellStyle.Font = new Font(cellStyle.Font, style);

        // change article color by the rule set
        cellStyle.ForeColor = ColorFromActions(actions, cellStyle.ForeColor);

        base.Paint(graphics, clipBounds, cellBounds,
        rowIndex, cellState, item._item._title, item._item._title, errorText,
        cellStyle, advancedBorderStyle, paintParts);

        // Paint node glyph
        if (owner.IsExpandable(row))
        {
          Rectangle glyphRect = new Rectangle(cellBounds.Left + offset - glyphWidth + 5,
              (cellBounds.Top + cellBounds.Bottom) / 2 - 5, 10, 10);
          if (item._item._expanded)
          {
            if (_openGlyph != null)
              _openGlyph.DrawBackground(graphics, glyphRect);
            else
            {
              // native drawing
              using (Brush brush = new SolidBrush(this.DataGridView.BackColor))
              {
                graphics.FillRectangle(brush, glyphRect);
              }
              using (Pen pen = new Pen(Color.Black))
              {
                graphics.DrawRectangle(pen, glyphRect);
                int y = (glyphRect.Top + glyphRect.Bottom) / 2;
                graphics.DrawLine(pen, new Point(glyphRect.Left + 2, y), new Point(glyphRect.Right - 2, y));
              }
            }
          }
          else
          {
            if (_closedGlyph != null)
              _closedGlyph.DrawBackground(graphics, glyphRect);
            else
            {
              using (Brush brush = new SolidBrush(this.DataGridView.BackColor))
              {
                graphics.FillRectangle(brush, glyphRect);
              }
              using (Pen pen = new Pen(Color.Black))
              {
                graphics.DrawRectangle(pen, glyphRect);
                int y = (glyphRect.Top + glyphRect.Bottom) / 2;
                graphics.DrawLine(pen, new Point(glyphRect.Left + 2, y), new Point(glyphRect.Right - 2, y));
                int x = (glyphRect.Left + glyphRect.Right) / 2;
                graphics.DrawLine(pen, new Point(x, glyphRect.Top + 2), new Point(x, glyphRect.Bottom - 2));
              }
            }
          }
        }
      }
      protected override void OnMouseDown(DataGridViewCellMouseEventArgs e)
      {
        // try to retrieve the article
        FormColabo owner = (FormColabo)this.DataGridView.Tag;
        if (owner != null)
        {
          ArticleItemWithDb item = owner.GetArticle(e.RowIndex);
          if (!item.IsNone() && owner.IsExpandable(e.RowIndex))
          {
            // item contains open / close glyph
            int level = owner.GetLevel(e.RowIndex);
            int offset = indentWidth * level;
            if (e.Location.X >= offset && e.Location.X < offset + glyphWidth)
            {
              // disable selection change during expanding / collapsing
              ((DataGridViewColabo)DataGridView).MouseMoveEnabled = false;
              using (new WaitCursor())
              {
                if (item._item._expanded)
                {
                  item._item._expanded = false;
                  owner.SaveArticleCollapsed(item);
                }
                else
                {
                  item._item._expanded = true;
                  owner.SaveArticleExpanded(item);
                }
                // the _expanded property must be changed also inside the index
                for (int i = 0; i < owner._indexEnabled.Count; i++)
                {
                  if (owner._indexEnabled[i]._articleId._database == item._database && owner._indexEnabled[i]._articleId._id == item._item._id)
                  {
                    owner._indexEnabled[i]._expanded = item._item._expanded;
                    break; //end the for cycle
                  }
                }
                owner.UpdateVisible(new ArticleId(item._database, item._item._id)); //ArticleId.None);
              }
              return;
            }
          }
        }

        // default reaction
        base.OnMouseDown(e);
      }
      protected override void OnMouseUp(DataGridViewCellMouseEventArgs e)
      {
        ((DataGridViewColabo)DataGridView).MouseMoveEnabled = true;

        // default reaction
        base.OnMouseDown(e);
      }
    }
    private class DataGridViewTitleColumn : DataGridViewTextBoxColumn
    {
      public DataGridViewTitleColumn()
      {
        // use our cells
        CellTemplate = new DataGridViewTitleCell();
      }
    }
    private class DataGridViewImagesCell : DataGridViewImageCell
    {
      protected override object GetFormattedValue(
	      object value,
	      int rowIndex,
	      ref DataGridViewCellStyle cellStyle,
	      TypeConverter valueTypeConverter,
	      TypeConverter formattedValueTypeConverter,
	      DataGridViewDataErrorContexts context)
      {
        return value;
      }

      protected override void Paint(
          Graphics graphics, Rectangle clipBounds, Rectangle cellBounds,
          int rowIndex,
          DataGridViewElementStates cellState,
          object value, object formattedValue, string errorText,
          DataGridViewCellStyle cellStyle,
          DataGridViewAdvancedBorderStyle advancedBorderStyle,
          DataGridViewPaintParts paintParts)
      {
        List<object> list = null;
        if (value != null) list = (List<object>)value;
        
        // decide what type of drawing we will use
        int n = list == null ? 0 : list.Count; // number of items
        
        // clear the content
        base.Paint(graphics, clipBounds,
          cellBounds, rowIndex, cellState,
          value, null, errorText,
          cellStyle, advancedBorderStyle,
          paintParts);
        if (n == 0) return;

        const int border = 2;
        int w = cellBounds.Width;
        int h = cellBounds.Height;
        int space = w - h - 2 * border; // square space for each icon

        Rectangle rect = cellBounds;
        rect.Width = h;
        int smallSize = (int)(0.75f * h);
        int smallOffset = (h - smallSize) / 2;
        if (n == 1 || space <= 0)
        {
          rect.X += border + space / 2;
          IconInfo info = (IconInfo)list[0];
          {
            Rectangle iconRect;
            if (info._small)
              iconRect = new Rectangle(rect.X + smallOffset, rect.Y + smallOffset, smallSize, smallSize);
            else
              iconRect = rect;
            if (info._icon is Icon)
              graphics.DrawIcon((Icon)info._icon, iconRect);
            else if (info._icon is Image)
              graphics.DrawImage((Image)info._icon, iconRect);
          }
        }
        else 
        {
          rect.X += border;

          int indent = space / (n - 1);
          for (int i=0; i<n; i++)
          {
            IconInfo info = (IconInfo)list[i];
            {
              Rectangle iconRect;
              if (info._small)
                iconRect = new Rectangle(rect.X + smallOffset, rect.Y + smallOffset, smallSize, smallSize);
              else
                iconRect = rect;
              if (info._icon is Icon)
                graphics.DrawIcon((Icon)info._icon, iconRect);
              else if (info._icon is Image)
                graphics.DrawImage((Image)info._icon, iconRect);
            }
            rect.X += indent;
          }
        }
      }
    }
    private class DataGridViewImagesColumn : DataGridViewColumn
    {
      public DataGridViewImagesColumn()
      {
        // use our cells
        CellTemplate = new DataGridViewImagesCell();
      }
    }
    // Fix: selection change during subtree collapsing
    private class DataGridViewColabo : DataGridView
    {
      private bool _mouseMoveEnabled;

      public DataGridViewColabo()
      {
        _mouseMoveEnabled = true;
      }

      public bool MouseMoveEnabled
      {
        get
        {
          return _mouseMoveEnabled;
        }
        set
        {
          _mouseMoveEnabled = value;
        }
      }

      protected override void OnMouseMove(MouseEventArgs e)
      {
        if (_mouseMoveEnabled) base.OnMouseMove(e);
      }
    }

    // Background Worker
    private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
    {
      ITask task = (ITask)e.Argument;
      if (task != null)
      {
        BackgroundWorker worker = (BackgroundWorker)sender;
        try
        {
          task.Process(worker);
        }
        catch (System.Exception exception)
        {
          Console.Write(exception.ToString());
        }
        e.Result = task;
      }
    }
    private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
    {
      Program.UpdateProgress(e.ProgressPercentage, (string)e.UserState);
    }
    private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      // finish the task in the main thread if needed
      ITask oldTask = (ITask)e.Result;
      if (oldTask != null) oldTask.Finish();
      Program.OnTaskCompleted(this);
    }

    // Tool strip
    private void newArticleButton_Click(object sender, EventArgs e)
    {
      if (_databases.Count == 0) return;

      ArticleItem item = new ArticleItem();
      // we want the new article marked as read for the sender
      item._changed = false;
      item._toRead = false;
      item._ownTags = CurrentTags();

      DialogArticle dialog = new DialogArticle(item, this, null, null, DialogArticle.Mode.New);
      dialog.Show();
    }

    private List<string> CurrentTags()
    {
      // construct tags by the currently selected ones
      var tags = new List<string>();
      foreach (TagCondition tag in _tags)
      {
        // do not add virtual tags
        if (tag._present && tag._tag[0] != TagSpecialChars.TagVirtual) tags.Add(tag._tag);
      }
      return tags;
    }
    private void synchronizeButton_Click(object sender, EventArgs e)
    {
      Synchronize(null, ArticleId.None); // all databases
    }
    private void findButton_Click(object sender, EventArgs e)
    {
      DialogSearch dialog = new DialogSearch(this);
      dialog.Show();
    }
    private void findAdvancedButton_Click(object sender, EventArgs e)
    {
      DialogAdvancedSearch dialog = new DialogAdvancedSearch(this);
      dialog.Show();
    }
    private void goToButton_Click(object sender, EventArgs e)
    {
      GoToLocation();
    }
    private void GoToLocation()
    {
      DialogGoTo dialog = new DialogGoTo();
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK)
      {
        Uri url = null;
        try
        {
          url = new Uri(dialog.Value);
        }
        catch (System.Exception)
        {
        }
        if (url != null) GoToArticle(url);
      }
    }
    private void NextUnreadButton_Click(object sender, EventArgs e)
    {
      NavigateNextUnread();
    }
    private void ganttChartButton_Click(object sender, EventArgs e)
    {
      DialogGanttChart dialog = new DialogGanttChart();
      dialog.Show();
    }
    private void configurationButton_click(object sender, EventArgs e)
    {
      Config();
    }
    private void Config()
    {
      DialogConfiguration dialog = new DialogConfiguration(this, Program.config);
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK)
      {
        // save rule sets
        if (_databases.Count > 0)
        {
          // store the new rule sets to the database
          _databases[0].SaveRuleSets(dialog.ruleSets);
        }

        // save scripts
        foreach (SQLDatabase database in _databases)
        {
          List<Script> scripts = new List<Script>();
          foreach (ScriptInfo script in dialog.scripts)
          {
            if (script._database == database) scripts.Add(script._script);
          }
          database.SaveScripts(scripts);
        }

        // re-create the list of databases
        _databases.Clear();
        foreach (DBInfoExt info in dialog.databases)
        {
          SQLDatabase database = NewDatabase(info._dbInfo);
          if (info._userInfo != null) database.SaveUserInfo(info._userInfo);
        }

        // update rule sets
        if (_databases.Count > 0)
        {
          // load the rule sets from database, show them in the UI
          _databases[0].LoadRuleSets(ref _ruleSets);
        }
        else
        {
          _ruleSets.Clear();
        }
        ShowRuleSets(null, null);

        UpdateBookmarks();
        UpdateScripts();

        Synchronize(null, ArticleId.None); // all databases
      }
    }

    private void Options()
    {
      DialogOptions dialog = new DialogOptions(this, Program.config);
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK)
      {
        UpdateSynchronizeTimer();
      }
    }

    private void newWindowToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Program.NewForm();
    }
    private void exitToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Program.Exit(true); // save the config
    }
    private void ruleSetComboBox_SelectedIndexChanged(object sender, EventArgs e)
    {
      RuleSet ruleSet = (RuleSet)ruleSetComboBox.SelectedItem;
      if (ruleSet != null)
      {
        // store the current rule set to config
        _ruleSetUser = ruleSet._user;
        _ruleSetName = ruleSet._name;
      }

      _currentRuleSetCompiled = new List<Token>();
      while (ruleSet != null)
      {
        List<Token> list = CompileRuleSet(ruleSet._expression);
        if (list != null) _currentRuleSetCompiled.AddRange(list);

        // find the parent rule set
        ruleSet = _ruleSets.Find(delegate(RuleSet item)
        {
          return item._user.Equals(ruleSet._parentUser, StringComparison.OrdinalIgnoreCase) &&
            item._name.Equals(ruleSet._parentName, StringComparison.OrdinalIgnoreCase);
        });
      }

      // update what articles are visible (avoid during initialization)
      int count = 0;
      foreach (SQLDatabase database in _databases) count += database.cache.Count;
      if (count > 0) UpdateArticles(ArticleId.None);
    }

    // menu File
    private void synchronizeToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Synchronize(null, ArticleId.None); // all databases
    }
    private void savePageAsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (webBrowser1 != null)
        webBrowser1.ShowSaveAsDialog();
    }
    private void pageSetupToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (webBrowser1 != null)
        webBrowser1.ShowPageSetupDialog();
    }
    private void printPreviewToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (webBrowser1 != null)
        webBrowser1.ShowPrintPreviewDialog();
    }
    private void printToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (webBrowser1 != null)
        webBrowser1.ShowPrintDialog();
    }
    private void sendLinkToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        DataGridViewRow curRow = dataGridView1.CurrentRow;
        if (curRow == null)
          return;
        ArticleItemWithDb article = GetArticle(curRow.Index);
        if (article.IsNone())
          return;

        // Get article info
        string link = article._item.GetLink(article._database, false);
        string articleName = article._item._title;

        // start email client
        string subject = articleName;
        string body = link;
//        MailAPI.MapiMailMessage message = new MailAPI.MapiMailMessage(null, null);
//        MailAPI.MapiMailMessage message = new MailAPI.MapiMailMessage(subject, body);
        //message.Recipients.Add("Test@Test.com");
        //message.Files.Add(@"C:\del.txt");
//        message.ShowDialog();

        string message = string.Format("mailto:{0}?subject={1}&body={2}", "@", subject, body);
        Process.Start(message);
      }
      catch //(System.Exception ex)
      {
      }
    }

    // menu Edit
    private void cutToolStripMenuItem_Click(object sender, EventArgs e)
    {

    }
    private void copyToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        if (webBrowser1 != null && webBrowser1.Focused)
        {
          IHTMLDocument2 htmlDocument = webBrowser1.Document.DomDocument as IHTMLDocument2;
          IHTMLSelectionObject currentSelection = htmlDocument.selection;
          if (currentSelection != null)
          {
            IHTMLTxtRange range = currentSelection.createRange() as IHTMLTxtRange;
            if (range != null)
            {
              Clipboard.SetText(range.text);
            }
          }
        }
      }
      catch
      {
      }
    }
    private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
    {

    }
    private void deleteToolStripMenuItem1_Click(object sender, EventArgs e)
    {

    }
    private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        if (webBrowser1 != null && webBrowser1.Focused)
        {
          IHTMLDocument2 htmlDocument = webBrowser1.Document.DomDocument as IHTMLDocument2;
          IHTMLBodyElement bodyElement = htmlDocument.body as IHTMLBodyElement;
          if (bodyElement != null)
          {
            IHTMLTxtRange range = bodyElement.createTextRange();
            range.select();
          }
        }
        else if (dataGridView1 != null && dataGridView1.Focused)
        {
          dataGridView1.SelectAll();
        }
      }
      catch
      {
      }
    }
    private void findToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        if (webBrowser1 != null)
        {
          // Set focus to WebBrowser
          if (!webBrowser1.Focused)
            webBrowser1.Focus();

          // Show Find dialog
          if (_findDialog == null)
            _findDialog = new DialogFind();
          DialogResult res = _findDialog.ShowDialog(this);

          if (res == DialogResult.OK)
          {
            // Create find info
            _findInfo = new FindInfo();
            _findInfo.text = _findDialog.FindText;
            _findInfo.matchCase = _findDialog.MatchCase;
            _findInfo.wholeWords = _findDialog.WholeWords;
            _findInfo.up = _findDialog.DirectionUp;
            //_lastFindRange = null;

            IHTMLDocument2 htmlDocument = webBrowser1.Document.DomDocument as IHTMLDocument2;
            IHTMLBodyElement bodyElement = htmlDocument.body as IHTMLBodyElement;

            if (bodyElement != null)
            {
              _lastFindRange = bodyElement.createTextRange();
              FindNext();
            }

          }
        }
      }
      catch
      {
      }
    }
    private void findNextToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (_lastFindRange != null)
      {
        _lastFindRange.move("character", (_findInfo.up ? -1: 1));
        FindNext();
      }
    }
    private void FindNext()
    {
      if (_lastFindRange != null)
      {
        // Construct search flags
        int flags = 0;
        if (_findInfo.up)
          flags += 1;
        if (_findInfo.wholeWords)
          flags += 2;
        if (_findInfo.matchCase)
          flags += 4;

        if (_lastFindRange.findText(_findInfo.text, 99999999, flags))
        {
          _lastFindRange.select();
        }
      }
    }

    // Menu view
    private void goToAddressBarToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (_addressBar != null)
        _addressBar.Focus();
    }

    // menu Go
    private void navigateBackToolStripMenuItem_Click(object sender, EventArgs e)
    {
      NavigateBack();
    }
    private void navigateForwardToolStripMenuItem_Click(object sender, EventArgs e)
    {
      NavigateForward();
    }
    private void homeToolStripMenuItem_Click(object sender, EventArgs e)
    {
      GoHome();
    }
    private void goToLocationToolStripMenuItem_Click(object sender, EventArgs e)
    {
      GoToLocation();
    }
    private void nextUnreadToolStripMenuItem_Click(object sender, EventArgs e)
    {
      NavigateNextUnread();
    }

    // menu Tools
    private void importFromNGToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (_databases.Count == 0) return;

      NNTPReader.Form1 dialogNNTPReader = new NNTPReader.Form1(this, _databases[0]);
      DialogResult result = dialogNNTPReader.ShowDialog();
    }
    private void importFromWikiToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (_databases.Count == 0) return;

      ImportWiki dialogImportWiki = new ImportWiki(this);
      DialogResult result = dialogImportWiki.ShowDialog();
    }
    private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Options();
    }
    private void configToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Config();
    }


    // menu Help
    private void checkForUpdateToolStripMenuItem_Click(object sender, EventArgs e)
    {
      Program.Update(Program.config.updatePath, true); // save before restart
    }

    // menu Debug
    private void test1ToolStripMenuItem_Click(object sender, EventArgs e)
    {
    }
    private void test2ToolStripMenuItem_Click(object sender, EventArgs e)
    {
    }


    // Context menu
    private void editToolStripMenuItem_Click(object sender, EventArgs e)
    {
      ArticleItemWithDb src = GetSelectedArticle();
      if (!src.IsNone())
      {
        if (UseExistingEditWindow(src)) return;

        // edit the article
        ArticleItem item = new ArticleItem(src._item); // create a copy
        DialogArticle dialog = new DialogArticle(item, this, null, src._database, DialogArticle.Mode.Edit);
        dialog.Show();
      }
    }
    private void replyToolStripMenuItem_Click(object sender, EventArgs e)
    {
      ArticleItemWithDb? itemDb = GetCurrentArticleWithDb();

      if (!itemDb.HasValue) return;
      replyUsingSelectedText(itemDb.Value);
    }

    private ArticleItemWithDb? GetCurrentArticleWithDb()
    {
      ArticleId id;
      ArticleItem item;
      if (GetCurrentArticle(out id, out item))
      {
        return new ArticleItemWithDb(id._database, item);
      }
      return null;
    }


    private string getRecentArticleContent(ArticleItemWithDb item)
    {
      string content = null;
      // check if original message is the last loaded
      if (item._item.AbsoluteURL(item._database).Equals(_currentURL)) content = _currentContent;
      // if not, try to load it now
      if (content == null) content = LoadArticle(item._database, item._item);
      return content;
    }

    private void replyWithQuote(ArticleItemWithDb item, string content)
    {
      // reply to the article
      ArticleItem reply = new ArticleItem();
      // we want the new article marked as read for the sender
      reply._changed = false;
      reply._toRead = false;
      reply._parent = item._item._id;
      const string replyPrefix = "Re: ";
      if (item._item._title.StartsWith(replyPrefix))
        reply._title = item._item._title;
      else
        reply._title = replyPrefix + item._item._title;
      reply._inheritedTags = item._item.InheritsTags();

      DialogArticle dialog = new DialogArticle(reply, this, content, item._database, DialogArticle.Mode.New);
      dialog.Show();
    }

    private void appendNoteToolStripMenuItem_Click(object sender, EventArgs e)
    {
      ArticleItemWithDb src = GetSelectedArticle();
      if (!src.IsNone())
      {
        if (UseExistingEditWindow(src)) return;

        // edit the article
        ArticleItem item = new ArticleItem(src._item); // create a copy
        DialogArticle dialog = new DialogArticle(item, this, null, src._database, DialogArticle.Mode.Note);
        dialog.Show();
      }
    }

    private static bool UseExistingEditWindow(ArticleItemWithDb src)
    {
      // check access rights
      ArticleItem.VTCache cache = new ArticleItem.VTCache(src._database, src._item);
      if ((src._item.GetAccessRights(src._database, cache) & AccessRights.Edit) == 0) return true;

      // check if this article is already editted
      foreach (Form form in Application.OpenForms)
      {
        if (form is DialogArticle)
        {
          if (((DialogArticle)form).IsEdittingArticle(src._database, src._item._id))
          {
            form.BringToFront();
            return true;
          }
        }
      }
      return false;
    }
    private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
    {
      ArticleItemWithDb item = GetSelectedArticle();
      if (!item.IsNone())
      {
        string message = string.Format("Do you want to remove article '{0}' from the database?", item._item._title);
        DialogResult result = MessageBox.Show(message, "Colabo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        if (result.Equals(DialogResult.Yes))
        {
          ITask task = new TaskDelete(this, item);
          AddTask(task);
        }
      }
    }
    private void markAsReadToolStripMenuItem_Click(object sender, EventArgs e)
    {
      MarkAsRead();
    }
    private void markAsUnreadToolStripMenuItem_Click(object sender, EventArgs e)
    {
      MarkAsUnread();
    }
    private void SVNLogToolStripMenuItem_Click(object sender, EventArgs e)
    {
      ArticleItemWithDb item = GetSelectedArticle();
      SVNCommand("log", string.Format("/path:\"{0}\"", item._item.AbsoluteURL(item._database)));
    }
    private void blameToolStripMenuItem_Click(object sender, EventArgs e)
    {
      ArticleItemWithDb item = GetSelectedArticle();
      SVNCommand("blame", string.Format("/path:\"{0}\"", item._item.AbsoluteURL(item._database)));
    }
    static public System.Diagnostics.Process SVNCommand(string command, string args)
    {
      RegistryKey regKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\TortoiseSVN", false);
      String procExeName = "TortoiseProc.exe";
      if (regKey != null)
      {
        try
        {
          procExeName = regKey.GetValue("ProcPath").ToString();
        }
        catch
        {
          // unexpected registry value, continue with default name
        }
      }

      System.Diagnostics.Process proc = new System.Diagnostics.Process();
      proc.StartInfo.FileName = procExeName;
      proc.StartInfo.Arguments = string.Format("/command:{0} {1}", command, args);
      proc.StartInfo.UseShellExecute = false;
      proc.StartInfo.RedirectStandardOutput = false;
      proc.Start();
      return proc;
    }
    private void copyColaboShortcutToolStripMenuItem_Click(object sender, EventArgs e)
    {
      ArticleItemWithDb item = GetSelectedArticle();
      if (!item.IsNone())
      {
        copyArticleShortcut(item);
      }
    }

    private static void copyArticleShortcut(ArticleItemWithDb item)
    {
      DataObject obj = new DataObject();
      string fullURL = item._item.AbsoluteURL(item._database);
      obj.SetData(DataFormats.Text, fullURL);
      string link = string.Format(
            "{0}?{1}?server={2}&database={3}&id={4:D}",
            item._item._title, fullURL, Uri.EscapeDataString(item._database.server),
            Uri.EscapeDataString(item._database.database), item._item._id);
      obj.SetData("ColaboShortcut", link);
      Clipboard.SetDataObject(obj);
    }
    private void setAsHomePageToolStripMenuItem_Click(object sender, EventArgs e)
    {
      ArticleItemWithDb item = GetSelectedArticle();
      if (item._database != null && item._item != null && item._item._id > 0)
      {
        item._database.homepage = item._item._id;
      }
    }
    private void bookmarkThisArticleToolStripMenuItem_Click(object sender, EventArgs e)
    {
      ArticleItemWithDb item = GetSelectedArticle();
      if (item._database != null && item._item != null && item._item._id > 0)
      {
        Bookmark bookmark = new Bookmark(item._database);
        bookmark._name = item._item._title;
        bookmark._image = 3;
        bookmark._article = new ArticleId(item._database, item._item._id);

        bookmark._database.AddBookmark(bookmark);
        UpdateBookmarks();
      }
    }

    /// Calendar view handling
    private class ColaboAppointment : Calendar.Appointment
    {
      private ArticleId _id;
      public ArticleId id { get { return _id;} }

      private FontStyle _fontStyle;
      public FontStyle fontStyle
      {
        get {return _fontStyle;}
        set {_fontStyle = value;}
      }

      public ColaboAppointment(ArticleId id) { _id = id; }

      public override bool Equals(System.Object obj)
      {
        // if parameter cannot be cast to ColaboAppointment return false
        ColaboAppointment a = obj as ColaboAppointment;
        if ((Object)a == null) return false;

        return a.id == _id;
      }
      public override int GetHashCode()
      {
        return _id.GetHashCode();
      }

      // custom drawing - text styles and colors
      public override void DrawTitle(Graphics g, Font baseFont, Rectangle rect, StringFormat format)
      {
        Font font = new Font(baseFont, fontStyle);
        g.DrawString(Title, font, SystemBrushes.WindowText, rect, format);
      }

    }
    private void monthCalendar_DateChanged(object sender, DateRangeEventArgs e)
    {
      try
      {
        dayView.StartDate = monthCalendar.SelectionStart - TimeSpan.FromDays(1);
        dayView.StartHour = dayView.WorkingHourStart;
      }
      catch (System.Exception exception)
      {
        Console.WriteLine(exception.ToString());      	
      }
    }
    private void dayView_ResolveAppointments(object sender, Calendar.ResolveAppointmentsEventArgs args)
    {
      // synchronize the selected article in both views
      ArticleId selected = ArticleId.None;
      if (dataGridView1.CurrentCell != null)
      {
        int selectedRow = dataGridView1.CurrentCell.RowIndex;
        if (selectedRow >= 0 && selectedRow < _index.Count)
          selected = _index[selectedRow]._articleId;
      }
      dayView.SelectedAppointment = null;
      monthCalendar.RemoveAllBoldedDates();

      foreach (SQLDatabase database in databases)
      {
        foreach (ArticleItem article in database.cache.Values)
        {
          DateTime timeFrom = article.timeFrom;
          DateTime timeTo = article.timeTo;
          // ensure the event will last at least an hour
          if (timeTo < timeFrom + TimeSpan.FromHours(1)) timeTo = timeFrom + TimeSpan.FromHours(1);

          if (database == selected._database && article._id == selected._id)
          {
            // mark the day in the month calendar
            monthCalendar.AddBoldedDate(timeFrom);
          }

          // check if article is in the shown range
          if (timeTo < args.StartDate || timeFrom >= args.EndDate) continue;
          // check access rights
          ArticleItem.VTCache cache = new ArticleItem.VTCache(database, article);
          if ((article.GetAccessRights(database, cache) & AccessRights.Read) == 0) continue;
          if (!article.IsEnabled(_tags, _currentRuleSetCompiled, database, cache)) continue;

          List<string> actions = article.GetActions(_currentRuleSetCompiled, database, cache);

          // who is the author
          UserInfo author = null;
          if (!string.IsNullOrEmpty(article._author)) author = database.AliasGetUser(article._author);

          // colors
          Color color = Color.LightGray;
          if (author != null) color = Color.FromArgb(author._color);

          Color borderColor = ColorFromActions(actions, Color.Black);

          // text style
          FontStyle style = FontStyle.Regular;
          if (actions.Contains("bold")) style |= FontStyle.Bold;
          if (actions.Contains("italic")) style |= FontStyle.Italic;
          if (actions.Contains("strikeout")) style |= FontStyle.Strikeout;
          if (actions.Contains("underline")) style |= FontStyle.Underline;

          // create appointment
          ColaboAppointment appointment = new ColaboAppointment(new ArticleId(database, article._id));
          appointment.StartDate = timeFrom;
          appointment.EndDate = timeTo;
          appointment.Title = article._title;
          appointment.Color = color;
          appointment.BorderColor = borderColor;
          appointment.fontStyle = style;
          appointment.Locked = false;
          args.Appointments.Add(appointment);

          if (database == selected._database && article._id == selected._id)
          {
            // select the appointment in the calendar
            dayView.SelectedAppointment = appointment;
          }
        }
      }

      monthCalendar.UpdateBoldedDates();
    }
    private void dayView_SelectionChanged(object sender, EventArgs e)
    {
      ColaboAppointment appointment = dayView.SelectedAppointment as ColaboAppointment;
      if (appointment != null)
      {
        GoToArticle(appointment.id._database, appointment.id._id);
        dayView.Invalidate();
      }
    }
    private void tabControl1_Selected(object sender, TabControlEventArgs e)
    {
      if (e.TabPage == tabCalendar)
      {
        // select the day selected article is in
        if (dataGridView1.CurrentCell != null)
        {
          ArticleItemWithDb article = GetArticle(dataGridView1.CurrentCell.RowIndex);
          if (!article.IsNone())
          {
            // TODO: enable set the time of article using some properties
            DateTime timeFrom = article._item.timeFrom;
            monthCalendar.SetDate(timeFrom);

            if (timeFrom.Hour < dayView.StartHour) dayView.StartHour = timeFrom.Hour;
            else
            {
              // what max. hour we need to show
              DateTime timeTo = article._item.timeTo;
              // ensure the event will last at least an hour
              if (timeTo < timeFrom + TimeSpan.FromHours(1)) timeTo = timeFrom + TimeSpan.FromHours(1);

              double hourTo = (timeTo - timeFrom.Date).TotalHours;
              if (hourTo > 24) hourTo = 24;

              // how much hours is visible in the control
              double hours = 0.5 * (dayView.Height - dayView.HeaderHeight) / dayView.HalfHourHeight;
              
              if (hourTo - hours > dayView.StartHour) dayView.StartHour = (int)Math.Ceiling(hourTo - hours);
            }
          }
        }
      }
    }
    private void dayView_MouseDown(object sender, MouseEventArgs e)
    {
      // set the context menu
      dayView.ContextMenuStrip = null;

      if (dataGridView1.CurrentCell != null)
      {
        int row = dataGridView1.CurrentCell.RowIndex;
        if (row >= 0)
        {
          // check if the mouse cursor is over the correct appointment
          ColaboAppointment appointment = dayView.GetAppointmentAt(e.X, e.Y) as ColaboAppointment;
          if (appointment != null && appointment.id == _index[row]._articleId)
          {
            UpdateContextMenu(row);
            dayView.ContextMenuStrip = contextMenuArticle;
          }
        }
      }
    }
    private void dayView_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      if (e.Button == MouseButtons.Left)
      {
        if (dataGridView1.CurrentCell != null)
        {
          int row = dataGridView1.CurrentCell.RowIndex;
          if (row >= 0)
          {
            // check if the mouse cursor is over the correct appointment
            ColaboAppointment appointment = dayView.GetAppointmentAt(e.X, e.Y) as ColaboAppointment;
            if (appointment != null && appointment.id == _index[row]._articleId)
            {
              _contextMenuRow = row;
              editToolStripMenuItem_Click(this, new EventArgs());
            }
          }
        }
      }
    }

    // Tags navigator
    private void tagsNavigator_Navigating(object sender, WebBrowserNavigatingEventArgs e)
    {
      Navigate(e.Url.ToString());
    }

    private void ResetTagCloud(bool tagsLimit = true)
    {
      _tagsLimit = tagsLimit;
      _tagsFilter = '\0';
    }

    // Tags navigator
    private void Navigate(string cmd)
    {
      _suppressChangeRow = true;
      try
      {
        tagsNavigator.Stop();

        if (cmd.StartsWith(_prefixTag))
        {
          string tag = cmd.Substring(_prefixTag.Length, cmd.Length - _prefixTag.Length - 1);
          _tags.Add(new TagCondition(tag, true));
          _languages.Clear();
          ResetTagCloud();
          UpdateTitle();
          UpdateArticles(ArticleId.None);
          UpdateNavigateHistory();
        }
        else if (cmd.StartsWith(_prefixLevel))
        {
          string level = cmd.Substring(_prefixLevel.Length, cmd.Length - _prefixLevel.Length - 1);
          int n;
          if (int.TryParse(level, out n)) _tags.RemoveRange(n, _tags.Count - n);
          _languages.Clear();
          ResetTagCloud();
          UpdateTitle();
          UpdateArticles(ArticleId.None);
          UpdateNavigateHistory();
        }
        else if (cmd.StartsWith(_prefixLetter))
        {
          string letter = cmd.Substring(_prefixLetter.Length, cmd.Length - _prefixLetter.Length - 1);
          if (letter.Length > 0)
          {
            _tagsFilter = letter[0];
            UpdateTags();
          }
        }
        else if (cmd.StartsWith(_prefixAllTags))
        {
          _languages.Clear();
          ResetTagCloud(false);
          UpdateTags();
        }
        else if (cmd.StartsWith(_prefixMostUsed))
        {
          _languages.Clear();
          ResetTagCloud();
          UpdateTags();
        }
        else if (cmd.StartsWith(_prefixRoot))
        {
          string tag = cmd.Substring(_prefixRoot.Length, cmd.Length - _prefixRoot.Length - 1);
          _tags.Clear();
          _tags.Add(new TagCondition(tag, true));
          _languages.Clear();
          ResetTagCloud();
          UpdateTitle();
          UpdateArticles(ArticleId.None);
          UpdateNavigateHistory();
        }
      }
      finally
      {
        _suppressChangeRow = false;
      }
    }
    // Handle right click on links 
    private void contextMenuTags_Opening(object sender, CancelEventArgs e)
    {
      if (tagsNavigator.Document != null)
      {
        HtmlElement active = tagsNavigator.Document.ActiveElement;
        if (active != null && active.TagName.Equals("A", StringComparison.OrdinalIgnoreCase))
        {
          string link = active.GetAttribute("href");
          if (link.StartsWith(_prefixTag))
          {
            _contextMenuTag = link.Substring(_prefixTag.Length, link.Length - _prefixTag.Length - 1);
            int index = contextMenuTags.Items.IndexOfKey("NOTTag");
            if (index >= 0) contextMenuTags.Items[index].Text = string.Format("Select articles not tagged with '{0}'", _contextMenuTag);
            index = contextMenuTags.Items.IndexOfKey("MakeTagRead");
            if (index >= 0) contextMenuTags.Items[index].Text = string.Format("Mark all articles tagged with '{0}' as read", _contextMenuTag);
            index = contextMenuTags.Items.IndexOfKey("renameTag");
            if (index >= 0)
            {
              ToolStripItem item = contextMenuTags.Items[index];
              item.Text = string.Format("Rename tag '{0}'...", _contextMenuTag);
              item.Visible = !_contextMenuTag.StartsWith(":"); // cannot rename virtual tags
            }
            return;
          }
          else if (link.StartsWith(_prefixLevel))
          {
            string level = link.Substring(_prefixLevel.Length, link.Length - _prefixLevel.Length - 1);
            int n;
            if (int.TryParse(level, out n))
            {
              _tags.RemoveAt(n - 1);
              _languages.Clear();
              UpdateTitle();
              UpdateArticles(ArticleId.None);
              UpdateNavigateHistory();
            }
          }
        }
      }
      e.Cancel = true;
    }
    // Add negation to the filter
    private void NOTTag_Click(object sender, EventArgs e)
    {
      if (_contextMenuTag != null)
      {
        _tags.Add(new TagCondition(_contextMenuTag, false));
        ResetTagCloud();
        UpdateTitle();
        UpdateArticles(ArticleId.None);
        UpdateNavigateHistory();
      }
    }

    // Mark articles tagged with selected tag as read
    private void makeTagReadToolStripMenuItem_Click(object sender, EventArgs e)
    {
      bool changed = false; // some local change occurs
      if (_contextMenuTag != null)
      {
        List<TagCondition> selTags = new List<TagCondition>(_tags);
        selTags.Add(new TagCondition(_contextMenuTag, true));
        foreach (SQLDatabase database in _databases)
        {
          List<ArticleItem> items = new List<ArticleItem>();
          foreach (ArticleItem item in database.cache.Values)
          {
            ArticleItem.VTCache cache = new ArticleItem.VTCache(database, item);

            if ((item.GetAccessRights(database, cache) & AccessRights.Read) == 0) continue;
            if ( item.IsEnabled(selTags, database, cache) )
            {
              if (item._changed)
              {
                changed = true; //we need UpdateTags at last
                item._changed = false;
                item._toRead = false;
                items.Add(item);
              }
            }
          }
          if (items.Count > 0)
          {
            ITask task = new TaskMarkAsRead(database, items);
            AddTask(task);
          }
        }
      }
      if (changed)
      {
        UpdateArticles(ArticleId.None);
      }
    }

    // Mass rename of tag
    private void renameTag_Click(object sender, EventArgs e)
    {
      if (string.IsNullOrEmpty(_contextMenuTag) || _contextMenuTag.StartsWith(":")) return;

      DialogRenameTag dialog = new DialogRenameTag(_contextMenuTag);
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK)
      {
        string oldTag = dialog.oldTag;
        string newTag = dialog.tag;
        if (newTag != oldTag)
        {
          string message;
          if (string.IsNullOrEmpty(newTag))
            message = string.Format("Are you sure you want to remove tag '{0}' everywhere?", oldTag);
          else
            message = string.Format("Are you sure you want to rename tag '{0}' to '{1}' everywhere?", oldTag, newTag);
          result = MessageBox.Show(message, "Colabo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
          if (result == DialogResult.Yes)
          {
            RenameTag(oldTag, newTag);
          }
        }
      }
    }
    private void RenameTag(string oldTag, string newTag)
    {
      foreach (SQLDatabase database in _databases)
      {
        foreach (ArticleItem item in database.cache.Values)
        {
          if (item._id < 0) continue; // ? rename tags in drafts
          if (item._ownTags == null) continue; // nothing to rename

          // replace tags in cache and database
          bool changed = false;
          foreach (string prefix in new string[] { "", "#", "-", "#-" })
          {
            if (item._ownTags.Contains(prefix + oldTag))
            {
              item._ownTags.Remove(prefix + oldTag);
              if (string.IsNullOrEmpty(newTag))
              {
                database.ReplaceTag(item._id, null, prefix + oldTag);
              }
              else
              {
                item._ownTags.Add(prefix + newTag);
                database.ReplaceTag(item._id, prefix + newTag, prefix + oldTag);
              }
              changed = true;
            }
          }

          if (changed && oldTag[0] != TagSpecialChars.TagPrivate)
          {
            // save tags to SVN
            ITask task = new TaskUpdateTags(database, item);
            AddTask(task);
          }
        }
      }
      Synchronize(null, ArticleId.None);
    }

    private void WebBrowser_NavigateError(object sender, WebBrowserNavigateErrorEventArgs e)
    {
      webBrowser1.httpErrorCode = e.StatusCode;
    }

    // Helper functions
    // Return the article corresponding to given row in the article list
    public ArticleItemWithDb GetArticle(int row)
    {
      if (row < 0 || row >= _index.Count) return ArticleItemWithDb.None;
      ArticleId id = _index[row]._articleId;
      ArticleItem item;
      if (id._database.cache.TryGetValue(id._id, out item)) return new ArticleItemWithDb(id._database, item);
      return ArticleItemWithDb.None;
    }
    public ArticleItem GetArticleByID(SQLDatabase database, int id)
    {
      ArticleItem item;
      if (database.cache.TryGetValue(id, out item)) return item;
      return null;
    }
    // Return the indentation level of article corresponding to given row in the article list
    public int GetLevel(int row)
    {
      if (row < 0 || row >= _index.Count) return 0;
      return _index[row]._level;
    }
    // Return if article corresponding to given row in the article list is expandable
    public bool IsExpandable(int row)
    {
      if (row < 0 || row >= _index.Count) return false;
      return _index[row]._hasChildren;
    }

    // Condition which should hold true for the searched article
    public delegate bool FindArticlePredicate(ArticleItem item);

    private int FindIndexEnabled(ArticleId id)
    {
      for (int i=0; i<_indexEnabled.Count; i++)
      {
        if (_indexEnabled[i]._articleId == id) return i;
      }
      return -1;
    }

    private ArticleId FindNextUnread()
    {
      DataGridViewRow row = dataGridView1.CurrentRow;
      int ix = FindIndexEnabled(_index[row == null ? 0 : row.Index]._articleId);
      if (ix >= _indexEnabled.Count) ix = _indexEnabled.Count - 1;
      if (ix < 0) return ArticleId.None; //nothing enabled?

      // find next enabled article down the Enabled articles array
      int i = (ix + 1) % _indexEnabled.Count;
      for ( ; i != ix; i = (i + 1) % _indexEnabled.Count)
      {
        ArticleId idx = _indexEnabled[i]._articleId;
        ArticleItem itemx;
        if (!idx._database.cache.TryGetValue(idx._id, out itemx)) continue;
        if (itemx._toRead) break; //next unread article found
      }
      // expand all items from this item up to tree root
      ArticleId id = _indexEnabled[i]._articleId;
      ArticleItem item;
      if (id._database.cache.TryGetValue(id._id, out item))
      {
        ArticleItem itemx = item;
        while (itemx._parent >= 0)
        {
          if (!id._database.cache.TryGetValue(itemx._parent, out itemx)) return ArticleId.None;
          itemx._expanded = true;
        }
        return new ArticleId(id._database, item._id);
      }
      return ArticleId.None;
    }

    private void NavigateNextUnread()
    {
      ArticleId id = FindNextUnread();
      if (id != ArticleId.None)
      {
        UpdateArticles(id);
      }
    }

    private void MarkAsRead()
    {
      Dictionary<SQLDatabase, List<ArticleItem>> articles = new Dictionary<SQLDatabase, List<ArticleItem>>();
      bool changed = false; // some local change occurs

      if (tabControl1.SelectedTab == tabTasks)
      {
        foreach (DataGridViewRow row in dataGridView1.SelectedRows)
        {
          ArticleItemWithDb item = GetArticle(row.Index);
          if (!item.IsNone())
          {
            if (item._item._changed)
            {
              changed = true;
              item._item._changed = false;
              item._item._toRead = false;
              if (item._database != null)
              {
                List<ArticleItem> items;
                if (!articles.TryGetValue(item._database, out items))
                {
                  items = new List<ArticleItem>();
                  articles.Add(item._database, items);
                }
                items.Add(item._item);
              }
              dataGridView1.InvalidateRow(row.Index);
              if (dataGridView1.CurrentCell != null && dataGridView1.CurrentCell.RowIndex == row.Index)
              {
                // re-read the article to hide difference highlighting
                LoadCurrentArticle();
              }
            }
          }
        }
      }
      else if (tabControl1.SelectedTab == tabCalendar)
      {
        // in calendar view, mark only the current article
        if (dataGridView1.CurrentCell != null)
        {
          int selectedRow = dataGridView1.CurrentCell.RowIndex;
          if (selectedRow >= 0 && selectedRow < _index.Count)
          {
            ArticleItemWithDb article = GetArticle(selectedRow);
            if (!article.IsNone() && article._item._changed)
            {
              changed = true;
              article._item._changed = false;
              article._item._toRead = false;
              // add to the structure of items to update
              List<ArticleItem> items = new List<ArticleItem>();
              items.Add(article._item);
              articles.Add(article._database, items);
              // update the view
              dayView.Refresh();
              // re-read the article to hide difference highlighting
              LoadCurrentArticle();
            }
          }
        }
      }

      // create a task for saving articles
      foreach (KeyValuePair<SQLDatabase, List<ArticleItem>> pair in articles)
      {
        ITask task = new TaskMarkAsRead(pair.Key, pair.Value);
        AddTask(task);
      }

      if (changed) UpdateTags();
    }
    private void MarkAsUnread()
    {
      Dictionary<SQLDatabase, List<ArticleItem>> articles = new Dictionary<SQLDatabase, List<ArticleItem>>();
      bool changed = false; // some local change occurs

      if (tabControl1.SelectedTab == tabTasks)
      {
        foreach (DataGridViewRow row in dataGridView1.SelectedRows)
        {
          ArticleItemWithDb item = GetArticle(row.Index);
          if (!item.IsNone())
          {
            if (!item._item._toRead)
            {
              changed = true;
              item._item._changed = true;
              item._item._toRead = true;
              if (item._database != null)
              {
                List<ArticleItem> items;
                if (!articles.TryGetValue(item._database, out items))
                {
                  items = new List<ArticleItem>();
                  articles.Add(item._database, items);
                }
                items.Add(item._item);
              }
              dataGridView1.InvalidateRow(row.Index);
            }
          }
        }
      }
      else if (tabControl1.SelectedTab == tabCalendar)
      {
        // in calendar view, mark only the current article
        if (dataGridView1.CurrentCell != null)
        {
          int selectedRow = dataGridView1.CurrentCell.RowIndex;
          if (selectedRow >= 0 && selectedRow < _index.Count)
          {
            ArticleItemWithDb article = GetArticle(selectedRow);
            if (!article.IsNone() && !article._item._toRead)
            {
              changed = true;
              article._item._changed = true;
              article._item._toRead = true;
              // add to the structure of items to update
              List<ArticleItem> items = new List<ArticleItem>();
              items.Add(article._item);
              articles.Add(article._database, items);
              // update the view
              dayView.Refresh();
            }
          }
        }
      }

      // create a task for saving articles
      foreach (KeyValuePair<SQLDatabase, List<ArticleItem>> pair in articles)
      {
        ITask task = new TaskMarkAsUnread(pair.Key, pair.Value);
        AddTask(task);
      }

      if (changed) UpdateTags();
    }

    // Rule Sets helpers
    private void ShowRuleSets(string user, string name)
    {
      if (user == null || name == null)
      {
        // keep the selected item
        RuleSet selected = (RuleSet)ruleSetComboBox.SelectedItem;
        if (selected != null)
        {
          user = selected._user;
          name = selected._name;
        }
      }

      ruleSetComboBox.Items.Clear();
      ruleSetComboBox.Items.AddRange(_ruleSets.ToArray());

      // select the original selected
      foreach (RuleSet item in ruleSetComboBox.Items)
      {
        if (item._name.Equals(name) && item._user.Equals(user))
        {
          ruleSetComboBox.SelectedItem = item;
          return;
        }
      }
      // select Default:General
      foreach (RuleSet item in ruleSetComboBox.Items)
      {
        if (item._user.Equals("default", StringComparison.OrdinalIgnoreCase) && item._name.Equals("general", StringComparison.OrdinalIgnoreCase))
        {
          ruleSetComboBox.SelectedItem = item;
          return;
        }
      }
      // select the first rule set
      if (ruleSetComboBox.Items.Count > 0) ruleSetComboBox.SelectedIndex = 0;
    }
    public static List<Token> CompileRuleSet(string ruleSet)
    {
      ColaboRuleSetLexer lexer = new ColaboRuleSetLexer(new Antlr.Runtime.ANTLRStringStream(ruleSet));
      ColaboRuleSetParser parser = new ColaboRuleSetParser(new Antlr.Runtime.CommonTokenStream(lexer));

      try
      {
        parser.ruleSet();
      }
      catch (Antlr.Runtime.RecognitionException e)
      {
        // some error occurred during parsing
        string header = parser.GetErrorHeader(e);
        string message = parser.GetErrorMessage(e, parser.TokenNames);
        MessageBox.Show(header + "\n\n" + message, "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return null;
      }
      catch (System.Exception)
      {
        MessageBox.Show("Parsing of rule set failed.", "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return null;
      }

      if (parser.NumberOfSyntaxErrors > 0) return null;
      return new List<Token>(parser._result);
    }

    // Full synchronization
    public void Synchronize(SQLDatabase database, ArticleId selectedID)
    {
      _synchronizeTimer.Stop();

      ITask task = new TaskSynchronize(selectedID, this, database);
      AddTask(task);
    }

    // authentication to access the given URL
    public static WebResponse AccessWebResponse(Uri url, string method, string[] headers, string data, out string realm, bool silent)
    {
      realm = "";

      //_config.authentications.DebugMe(url);
      ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
      WebRequest request;
      try
      {
        request = WebRequest.Create(url);
      }
      catch (Exception exception)
      {
        Log("AccessWebResponse WebRequest.Create Exception: " + exception.ToString());
        return null;
      }
      request.PreAuthenticate = true;
      HttpWebRequest webRequest = (HttpWebRequest)request;
      if (webRequest != null)
      {
        webRequest.KeepAlive = false;
        webRequest.ProtocolVersion = HttpVersion.Version10;
      }

      Uri serverUrl = new Uri(url.Scheme + "://" + url.Host + "/");
      request.Credentials = Program.config.credentialCache;

      if (!string.IsNullOrEmpty(method)) request.Method = method;
      if (headers != null)
      {
        foreach (string header in headers) request.Headers.Add(header);
      }
      if (data != null)
      {
        request.ContentType = "text/xml; charset=\"utf-8\"";

        Stream stream = request.GetRequestStream();
        StreamWriter writer = new StreamWriter(stream);
        writer.Write(data);
        writer.Close();
        stream.Close();
      }

      bool first = true;
      while (true)
      {
        try
        {
          return request.GetResponse();
        }
        catch (WebException exception)
        {
          // for authentication problem show the dialog, for others return null
          if (exception.Status != WebExceptionStatus.ProtocolError) return ReportWebException(url, request, exception);
          HttpWebResponse response = exception.Response as HttpWebResponse;
          if (response == null) return ReportWebException(url, request, exception);
          HttpStatusCode err = response.StatusCode;
          string header = response.GetResponseHeader("WWW-Authenticate");
          response.Close();
          if (err != HttpStatusCode.Unauthorized) return ReportWebException(url, request, exception);

          // extract the realm from the response headers
          if (!string.IsNullOrEmpty(header))
          {
            string prefix = "Basic realm=";
            if (header.StartsWith(prefix))
            {
              realm = header.Substring(prefix.Length);
              int len = realm.Length;
              if (len >= 2 && realm[0] == '"' && realm[len - 1] == '"') realm = realm.Substring(1, len - 2);
            }
          }

          NetworkCredential credentials = null;
          if (first)
          {
            first = false; // next time ask the user

            // on the first failure, try to find credentials in the cache
            string user, pwd;
            if (Program.config.FindCredentials(serverUrl, realm, "Basic", out user, out pwd))
            {
              credentials = new NetworkCredential(user, pwd);
              Program.config.RefreshAuthentications(serverUrl, "Basic", credentials); // update the last used credentials for this server
            }
          }

          if (credentials == null)
          {
            if (silent) return null; // do not ask user

            // invoke the dialog
            string user = Environment.UserName;
            string pwd = "";
            bool savePwd = true;
            CredUIReturnCodes code = Import.PromptForCredentials(url, realm, ref user, ref pwd, ref savePwd);
            if (code != CredUIReturnCodes.NO_ERROR) return ReportWebException(url, request, exception);

            credentials = new NetworkCredential(user, pwd);
            if (savePwd) Program.config.UpdateAuthentications(serverUrl, realm, "Basic", credentials, true); // replace, add and store updated config file
          }

          request = WebRequest.Create(url);
          request.PreAuthenticate = true;
          webRequest = (HttpWebRequest)request;
          if (webRequest != null) webRequest.KeepAlive = false;
          request.Credentials = credentials;
        }
      }
    }

    /**
    return WebResponse so that it can be used as return ReportWebException(url,ex)
    */
    private static WebResponse ReportWebException(Uri url, WebRequest req, WebException exception)
    {
      Log("AccessWebResponse " + req.Method + " url '" + url + "' WebRequest.GetResponse Exception: " + exception.ToString());
      return null;
    }

    /// Read the content of the article from web
    public static string GetArticleContent(string url, out string realm)
    {
      using (WebResponse response = AccessWebResponse(new Uri(url), null, null, null, out realm, false))
      {
        if (response == null)
        {
          Log(string.Format("GetArticleContent: authentication failed for {0}", url));
          return null; // authentication failed
        }

        using (Stream stream = response.GetResponseStream())
        {
          using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
          {
            return reader.ReadToEnd();
          }
        }
      }

      return "";
    }

    /// Try to read the article directly, using http request / response
    private string LoadArticle(SQLDatabase database, ArticleItem item)
    {
      if (!string.IsNullOrEmpty(item._draftLocal))
      {
        if (File.Exists(item._draftLocal))
        {
          DocXML file = DocXML.Load(item._draftLocal,"Article");
          if (file!=null)
          {
            string content = file.GetValue();
            StructuredNode node = file.GetChild("chatHistory");
            if (node != null)
            {
              ChatConversation chat = new ChatConversation(node);
              if (node != null)
              {
                content = chat.GetText(database) + ChatConversation.FormatChatHeader(database, database.user, null) + content;
              }
            }
            return content;
          }
        }
        else if (item._id < 0) return ""; // draft no more exists
      }

      if (item._chatId >= 0 || item._chatUsers!=null)
      {
        ChatConversation chat = database.LoadChatConversation(item._chatId,false);
        if (chat != null)
        {
          return chat.GetText(database);
        }
        return "";
      }

      return database.GetArticlesStore().GetArticleContent(item.AbsoluteURL(database), ref browserProxy);
    }

    // refresh the current article in the preview after differences was found
    public void RefreshArticle(ArticleItem item, SQLDatabase database, string content, List<string> oldTags)
    {
      // check if current article didn't change and was not marked as read
      if (item._changed && item.AbsoluteURL(database).Equals(_currentURL)) FormatArticle(item, database, content, "", oldTags);
    }

    private string TagLink(string tag)
    {
      int pos = 0;
      // skip the prefix
      if (tag.Length > pos && tag[pos] == TagSpecialChars.PrefixLocal) pos++; // local scope
      if (tag.Length > pos && tag[pos] == TagSpecialChars.PrefixRemove) pos++; // tag removing

      if (tag.Length > pos)
      {
        // only pure tag is in the link
        return string.Format("{0}<A href=\"Tag({1})\" title=\"Select articles tagged with '{1}'\">{1}</A>",
          tag.Substring(0, pos), tag.Substring(pos));
      }
      return tag;
    }

    private string LanguageLink(string lang, string code)
    {
      // only pure tag is in the link
      return string.Format("<A href=\"Lang({1})\" title=\"Display article in {0} language\">{0}</A>",lang,code);
    }

    // Format the article header and content
    private void FormatArticle(ArticleItem item, SQLDatabase database, string content, string message, List<string> oldTags)
    {
      // create the header of the article
      string tags = "";
      if (item._ownTags != null)
        foreach (string tag in item._ownTags)
        {
          if (tags.Length > 0) tags = tags + ", ";
          if (oldTags != null && !oldTags.Contains(tag) && ArticleItem.FromPrivateTag(tag) == null) // private tags have no history
            tags = tags + "<ins>" + TagLink(tag) + "</ins>"; // new tag
          else
            tags = tags + TagLink(tag);
        }
      if (oldTags != null)
        foreach (string tag in oldTags)
        {
          if (item._ownTags == null || !item._ownTags.Contains(tag))
          {
            if (tags.Length > 0) tags = tags + ", ";
            tags = tags + "<del>" + TagLink(tag) + "</del>"; // old tag
          }
        }
      string inheritedTags = "";
      if (item._effectiveTags != null)
        foreach (string tag in item._effectiveTags)
        {
          if (item._ownTags != null && (item._ownTags.Contains(tag) || item._ownTags.Contains("#" + tag))) continue; // avoid duplicity
          if (inheritedTags.Length > 0) inheritedTags = inheritedTags + ", ";
          inheritedTags = inheritedTags + TagLink(tag);
        }
      if (inheritedTags.Length > 0)
      {
        if (tags.Length > 0) tags = tags + ", ";
        tags = tags + "<I>" + inheritedTags + "</I>";
      }
      
      UserInfo info = database.AliasGetUser(item._author);
      string author = database.UserDisplayName(ref info, item._author);

      string header = Configuration.GetCiteHTMLHeader();
      // TODO: list of languages based on user settings
      string languages = LanguageLink("Original","") + ", " + LanguageLink("English","en") + ", " + LanguageLink("Czech","cs");
      header += string.Format(
          "<dl id=\"header\"><dt class=\"subject\">Subject:<dd class=\"subject\">{0}"+
          "<dt class=\"from\">From:<dd class=\"from\">{1}"+
          "<dt class=\"language\">Language:<dd class=\"language\">{5}" +
          "<dt class=\"posted\">Posted:<dd class=\"posted\">{2}" +
          "<dt class=\"tags\">Tags:<dd class=\"tags\">{3}{4}</dl>",
          item._title, author, item._date, tags, message, languages);

      if (info != null && !string.IsNullOrEmpty(info._picture))
        header += string.Format("<img src=\"{0}\" style=\"position:absolute; top:20px; right:20px\" />", info._picture);

      // convert markdown formatted text to HTML
      SetWebBrowserDocument(header + Coma.ComaParser.Transform(database.svnRoot, database.urlTranslate, content, _languages) + Configuration.GetCiteHTMLFooter());
    }

    // Set new document text to active WebBrowser
    private void SetWebBrowserDocument(string documentText)
    {
      try
      {
        // Clear last find range
        _lastFindRange = null;
        // Set new document text
        webBrowser1.DocumentText = documentText;
      }
      catch (System.Exception e)
      {
        Console.Write(e.ToString());
      }
    }

    // Load the current article to the browser
    private void LoadCurrentArticle()
    {
      _markArticleReadTimer.Stop();
      if (webBrowser1.Document != null && !webBrowser1.IsBusy)
      {
        SetWebBrowserDocument(@"<html><head><meta http-equiv=""Content-Type"" content=""text/html;charset=utf-8""><title>Loading article�</title><style type=""text/css"">body{font:bold 100% Calibri,Tahoma,Geneva,""Bitstream Vera Sans"",sans-serif}</style><body><p>Loading article�");
/*
        webBrowser1.Document.OpenNew(true);
        webBrowser1.Document.Write("<B>Loading article ...</B>");
*/ 
      }
      if (!TryLoadCurrentArticle())
      {
        // load failed
        string content = "=== Cannot load article";

        // try to find the cause and offer related help
        string filename = null;
        if (databases.Count == 0 && File.Exists("helpNoDb.txt")) filename = "helpNoDb.txt";
        else if (_index.Count == 0 && File.Exists("helpNoArticle.txt")) filename = "helpNoArticle.txt";
        else if (File.Exists("helpWrongArticle.txt")) filename = "helpWrongArticle.txt";

        // read the help file
        if (filename != null)
        {
          try
          {
            using (StreamReader reader = new StreamReader(filename)) content = reader.ReadToEnd();
          }
          catch (System.Exception e)
          {
            Console.Write(e.ToString());
          }
        }

        SetWebBrowserDocument(Configuration.GetCiteHTMLHeader() + Coma.ComaParser.Transform(null, null, content, _languages) + Configuration.GetCiteHTMLFooter());

        // webBrowser1.DocumentText = @"<html><head><meta http-equiv=""Content-Type"" content=""text/html;charset=utf-8""><title>Cannot load article</title><style type=""text/css"">body{font:bold 100% Calibri,Tahoma,Geneva,""Bitstream Vera Sans"",sans-serif;color:#f00}</style><body><p>Cannot load article";

      }
      else
      { // update navigate history
        UpdateNavigateHistory();

        UpdateAddressBar();

        UpdateMarkAsReadTimer();
      }
    }

    // Update and restart article tmer
    private void UpdateMarkAsReadTimer()
    {
      _markArticleReadTimer.Stop();
      if (Program.config.MarkAsRead)
        if (dataGridView1.CurrentCell != null)
        {
          int selectedRow = dataGridView1.CurrentCell.RowIndex;
          if (selectedRow >= 0 && selectedRow < _index.Count)
          {
            ArticleItemWithDb article = GetArticle(selectedRow);
            if (!article.IsNone())
            {
              if (article._item._changed)
              {
                _markArticleReadTimer.Interval = Program.config.MarkAsReadTime*1000;
                _markArticleReadTimer.Start();
              }
            }
          }
        }
    }

    public void UpdateSynchronizeTimer()
    {
      _synchronizeTimer.Stop();
      if (Program.config.SynchronizeInterval > 0)
      {
        _synchronizeTimer.Interval = 60 * 1000 * Program.config.SynchronizeInterval;
        _synchronizeTimer.Start();
      }
    }

    // List of tags needs deep Equals implementation
    private bool TagsEquals(List<TagCondition> tags1, List<TagCondition> tags2)
    {
      if (tags1.Count != tags2.Count) return false;
      for (int i = 0; i < tags1.Count; i++ )
      {
        if (!tags1[i].Equals(tags2[i])) return false;
      }
      return true;
    }

    private void UpdateNavigateHistory()
    {
      if (!_navigatingInHistory)
      {
        // do not update navigation history when _currentURL or _tags has not changed compared to the last stored history state
        if (_navigateHistoryIndex < 0 || _navigateHistoryIndex >= _navigateHistory.Count ||
            _currentURL != _navigateHistory[_navigateHistoryIndex].url ||
            !TagsEquals(_tags, _navigateHistory[_navigateHistoryIndex].tags)
           )
        {
          if (_navigateHistoryIndex + 1 < _navigateHistory.Count)
          {
            _navigateHistoryIndex++;
            _navigateHistory.RemoveRange(_navigateHistoryIndex, _navigateHistory.Count - _navigateHistoryIndex);
          }
          else _navigateHistoryIndex = _navigateHistory.Count; //end of list
#if DEBUG_NAV_HISTORY
          System.Diagnostics.Debug.WriteLine(String.Format("History.add at index {0}: {1}", _navigateHistoryIndex, _currentURL));
          for (int i = 0; i < _tags.Count; i++)
            System.Diagnostics.Debug.Write(String.Format(" {0}, ", _tags[i]._tag));
          System.Diagnostics.Debug.WriteLine("");
#endif
          _navigateHistory.Add(new NavHistoryItem(_currentURL, _tags));
        }
#if DEBUG_NAV_HISTORY
        else
        {
          System.Diagnostics.Debug.WriteLine(String.Format("History.NOT ADD at index {0} is already {1}", _navigateHistoryIndex, _currentURL));
          for (int i = 0; i < _tags.Count; i++)
            System.Diagnostics.Debug.Write(String.Format(" {0}, ", _tags[i]._tag));
          System.Diagnostics.Debug.WriteLine("");
          for (int i = 0; i < _navigateHistory[_navigateHistoryIndex].tags.Count; i++)
            System.Diagnostics.Debug.Write(String.Format(" {0}, ", _navigateHistory[_navigateHistoryIndex].tags[i]._tag));
          System.Diagnostics.Debug.WriteLine("");
        }
#endif
      }
#if DEBUG_NAV_HISTORY
      else
      {
        System.Diagnostics.Debug.WriteLine(string.Format("_navigatingInHistory is TRUE! History not updated! CurIndex {0}",_navigateHistoryIndex));
        for (int i = 0; i < _tags.Count; i++)
          System.Diagnostics.Debug.Write(String.Format(" {0}, ", _tags[i]._tag));
        System.Diagnostics.Debug.WriteLine("");
        for (int i = 0; i < _navigateHistory[_navigateHistoryIndex].tags.Count; i++)
          System.Diagnostics.Debug.Write(String.Format(" {0}, ", _navigateHistory[_navigateHistoryIndex].tags[i]._tag));
        System.Diagnostics.Debug.WriteLine("");
      }
#endif
    }

    // recursive loading of subarticles
    private int LoadSubarticles(ref string content, ArticleItem item, SQLDatabase database, int level, int count)
    {
      string indent = new string(' ', level);
      if (item._children != null) foreach (int id in item._children)
      {
        ArticleItem child;
        if (database.cache.TryGetValue(id, out child))
        {
          // header
          string title = string.Format("{0} ({1} by {2})", child._title, child._date, child._author);
          content += string.Format("{0}![{1}][{2:D}]\n{0}[{2:D}]:\n", indent, title, id);
          // body
          const int maxCount = 5;
          string body = count < maxCount ? LoadArticle(database, child) : "...";
          count++;
          if (body != null)
          {
            string[] lines = body.Split(new char[] { '\n' });
            foreach (string line in lines) content += string.Format("{0} {1}\n", indent, line);
          }
          else
          {
            // add link to the content
            string link = child.GetLink(database, true);
            content += string.Format("{0} [{1}]({2})\n", indent, child._title, link);
          }
          // recurse
          if (level < 3) count = LoadSubarticles(ref content, child, database, level + 1, count);
        }
      }
      return count;
    }

    private ArticleItemWithDb GetSelectedArticle()
    {
      if (_contextMenuRow >= 0) return GetArticle(_contextMenuRow);
      return ArticleItemWithDb.None;
    }

    private bool CheckIMVisible(SQLDatabase db)
    {
      if (imFilter.Items.Count != 1) return false;
      string imFilterName = imFilter.Items[0].ToString();
      return db.name.Equals(imFilterName,StringComparison.OrdinalIgnoreCase);
    }
    private void ShowIMPresence(SQLDatabase db)
    {
      if (CheckIMVisible(db)) return;
      imFilter.Items.Clear();
      imFilter.Items.Add(db.name);
      imUsers.BeginUpdate();
      // do not clear, update only
      imUsers.Items.Clear();
      // first list users who were seen at all

      Action<KeyValuePair<String, UserInfo>> addUser = delegate(KeyValuePair<String, UserInfo> user)
      {
        var item = new ChatUserItem(user.Key, user.Value);
        item.Name = user.Key;
        imUsers.Items.Add(item);
      };
      foreach (var user in db.users)
      {
        if (user.Value._presence != null && user.Value._presence.Length > 0) addUser(user);
      }
      foreach (var user in db.users)
      {
        if (user.Value._presence == null || user.Value._presence.Length <= 0) addUser(user);
      }
      imUsers.EndUpdate();
    }

    private void UpdateIMPresence(SQLDatabase db, Dictionary<string, UserInfo> userList)
    {
      db.UpdateUserPresence(userList);
      // if this database is currently shown, update it
      if (CheckIMVisible(db))
      {
        var usersCopy = new Dictionary<string, UserInfo>(db.users);
        this.BeginInvoke((MethodInvoker)delegate
        { // runs on UI thread
          imUsers.BeginUpdate();

          foreach (var user in usersCopy)
          {
            ListViewItem[] match = imUsers.Items.Find(user.Key, false);
            foreach (ChatUserItem item in match)
            {
              item.SetDetails(user.Value);
            }
          }

          foreach (var chat in _chats)
          {
            chat.UpdateChatUsers(usersCopy);
          }
          imUsers.EndUpdate();
        });
      }
    }

    void ChangeIMState(String state)
    {
      _notifyIdle.DetectionMode = state;
    }
  


        
    private bool TryLoadCurrentArticle()
    {
      // what article was selected
      ArticleId id;
      ArticleItem item;
      if (!GetCurrentArticle(out id, out item)) return false;

      string itemURL = item.AbsoluteURL(id._database);

      // check the referenced url            
      if (itemURL.Length == 0)
      {
        Log("TryLoadCurrentArticle: empty URL");
        return false;
      }

      ShowIMPresence(id._database);

      _currentURL = itemURL;
      _currentContent = LoadArticle(id._database, item);

      if (_currentContent != null)
      {
        string extendedContent = _currentContent;
        if (!item._expanded) LoadSubarticles(ref extendedContent, item, id._database, 0, 0);

        string message = "";
        if (item._changed)
        {
          // check for changes
          int readRevision = item._id >= 0 ? id._database.GetReadRevision(item._id) : 0; // for new drafts, nothing could be read
          if (readRevision > 0)
          {
            message = "<BR><B><SPAN style=\"color:red\">Searching for changes in text ...</SPAN></B>";
            ITask diffTask = new TaskArticleDiff(item, id._database, extendedContent, readRevision, this);
            AddTask(diffTask);
          }
        }
        FormatArticle(item, id._database, extendedContent, message, null);

        // start updating the working copy on background
        if (item._id >= 0)
        {
          int changedRevision = id._database.LastChangeRevision(item._id);
          ITask updateTask = new TaskUpdate(id._database, id._database.GetArticlesStore(), item, changedRevision);
          AddTask(updateTask);
        }

        return true;
      }
      // fall-back
      Log(string.Format("TryLoadCurrentArticle: fall-back navigation to article {0}", _currentURL));
      browserProxy.NavigateUrl = itemURL;
      webBrowser1.Navigate(itemURL);
      return true;
    }

    private bool GetCurrentArticle(out ArticleId id, out ArticleItem item)
    {
      id = new ArticleId();
      item = null;
      DataGridViewRow row = dataGridView1.CurrentRow;
      if (row == null)
      {
        Log("TryLoadCurrentArticle: no current row");
        return false;
      }
      if (row.Index < 0 || row.Index >= _index.Count)
      {
        Log("TryLoadCurrentArticle: row index out of bounds");
        return false;
      }
      id = _index[row.Index]._articleId;


      if (!id._database.cache.TryGetValue(id._id, out item))
      {
        Log("TryLoadCurrentArticle: article not in cache");
        return false;
      }
      return true;
    }

    // called if current article content changed
    public void ReloadArticleIfCurrent(string url)
    {
      if (url.Equals(_currentURL)) LoadCurrentArticle();
    }

    // create the itemIndex list of visible articles from _indexEnabled which MUST be updated before
    private void UpdateIndex()
    {
      _index.Clear();
      for (int i = 0; i < _indexEnabled.Count; i++)
      {
        IndexItem index = _indexEnabled[i];
        index._level = (index._parent != null) ? index._parent._level + 1 : 0;
        if (index._parent == null || (index._parent._visible && index._parent._expanded))
        {
          index._visible = true;
          _index.Add(index);
        }
        else index._visible = false;
      }
    }

    // helper function for recursive traversal of articles
    private bool AddArticleEnabled(SQLDatabase database, ArticleItem item, int level, IndexItem parent, out DateTime latest, out int sortOrder)
    {
      const string prefixSortOrder = "sortorder:";

      bool someEnabled = false;
      IndexItem index = null;

      //set out latest date to item date
      latest = DateTime.MinValue;
      sortOrder = 0;

      ArticleItem.VTCache cache = new ArticleItem.VTCache(database, item);

      // check if article meet the filter criteria
      if ((item.GetAccessRights(database, cache) & AccessRights.Read) != 0)
      {
        // optimization - call GetActions once for both IsEnabled and sort order
        List<string> actions = item.GetActions(_currentRuleSetCompiled, database, cache);

        // enabled 
        if (!actions.Contains("hide") && item.IsEnabled(_tags, database, cache))
        {
          someEnabled = true;
          index = new IndexItem(database, item._id, level++, parent, item._expanded);
          parent = index;
          _indexEnabled.Add(index);
        }

        // update date and sort order even from articles not enabled (do we want it really?)
        latest = item._date;
        // find the first SORTORDER action
        for (int i=0; i<actions.Count; i++)
        {
          string action = actions[i];
          if (action.StartsWith(prefixSortOrder))
          {
            int number;
            if (int.TryParse(action.Substring(prefixSortOrder.Length), out number))
            {
              sortOrder = number;
              break;
            }
          }
        }
      }

      // recursively add child articles
      if (item._children != null)
      {
        foreach (int i in item._children)
        {
          ArticleItem child;
          if (database.cache.TryGetValue(i, out child))
          {
            DateTime childLatest;
            int childSortOrder;
            if (AddArticleEnabled(database, child, level, parent, out childLatest, out childSortOrder))
            {
              if (latest < childLatest) latest = childLatest; //update latest item date to child's more recent one 
              if (sortOrder < childSortOrder) sortOrder = childSortOrder;
              someEnabled = true;
              if (index != null) index._hasChildren = true;
            }
          }
        }
      }

      // update the index date to make sorting by date column more sexy
      if (index != null)
      {
        index._date = latest;
        index._sortOrder = sortOrder;
      }

      return someEnabled;
    }

    // Fill the UI from the cache, using the current filters
    public void UpdateArticles(ArticleId selectedID)
    {
      using (new WaitCursor())
      {
        UpdateTags();
        UpdateVisible(selectedID);
        FormColabo.Log(string.Format("Total: {0} enabled articles, {1} visible", _indexEnabled.Count, _index.Count));
        // update the articles in calendar
        dayView.Refresh();
      }
    }

    public void UpdateIndexEnabled()
    {
      // create index with IsEnabled()==true
      _indexEnabled.Clear();
      foreach (SQLDatabase database in _databases)
      {
        foreach (ArticleItem item in database.cache.Values)
        {
          // children are inserted by the parent
          DateTime latest;
          int sortOrder;
          if (item._parent < 0) AddArticleEnabled(database, item, 0, null, out latest, out sortOrder);
        }
      }
      if (_selectedComparer != null)
      {
        IndexComparer compareIndices = new IndexComparer(_selectedComparer.GetComparer());
        try
        {
          _indexEnabled.Sort(compareIndices);
        }
        catch (System.Exception ex)
        {
          System.Diagnostics.Debug.WriteLine(string.Format("_indexEnabled.Sort is sad to inform you there was an exception due to invalid data.\r\n{0}", ex.ToString()));
        }
      }
    }

    // Select only visible articles, apply sorting and update list of articles
    private void UpdateVisible(ArticleId selectedID)
    {
      _suppressChangeRow = true;
      try
      {
        // what row was selected before the change
        int selectedRow = -1;
        if (dataGridView1.CurrentCell != null) selectedRow = dataGridView1.CurrentCell.RowIndex;

        // Store information to Preserve the Scrolling when collapsing the article trees
        int selectedIDRowIndex = _index.FindIndex(delegate(IndexItem item) { return item._articleId == selectedID; });
        int oldFirstDisplayedScrollingRowIndex = dataGridView1.FirstDisplayedScrollingRowIndex;
        bool preserveScroll = selectedID != ArticleId.None;

        // when selectedID not given, try to keep selected article
        if (selectedID == ArticleId.None)
        {
          if (selectedRow >= 0 && selectedRow < _index.Count) selectedID = _index[selectedRow]._articleId;
        }

        // Create the index of visible articles
        UpdateIndex();
        // Sorting
        if (_selectedComparer != null)
        {
          IndexComparer compareIndices = new IndexComparer(_selectedComparer.GetComparer());
          try
          {
            _index.Sort(compareIndices);
          }
          catch (System.Exception ex)
          {
            System.Diagnostics.Debug.WriteLine(ex.ToString());
          }
        }

        dataGridView1.RowCount = _index.Count;
        UpdateBackgroundColor();
        if (preserveScroll)
        {
          int curSelRow = _index.FindIndex(delegate(IndexItem item) { return item._articleId == selectedID; });
          int scrollTo = curSelRow + (oldFirstDisplayedScrollingRowIndex - selectedIDRowIndex);
          if (scrollTo < 0) scrollTo = 0;
          else if (scrollTo >= _index.Count) scrollTo = _index.Count - 1;
          if (_index.Count > 0)
            dataGridView1.FirstDisplayedScrollingRowIndex = scrollTo;
        }
        else dataGridView1.Invalidate();
        SelectArticle(selectedID, selectedRow);
      }
      finally
      {
        _suppressChangeRow = false;
      }
    }
    private void SelectArticle(ArticleId selectedID, int selectedRow)
    {
      // select the correct article
      int row = -1;
      // try to keep selected item
      if (selectedID != ArticleId.None) row = _index.FindIndex(delegate(IndexItem item) { return item._articleId == selectedID; });

      // if not possible, select the homepage (removed, to select the latest article is better)
      // if (row < 0) row = FindHomePage(selectedID._database);

      // set it
      if (row >= 0) dataGridView1.CurrentCell = dataGridView1.Rows[row].Cells[0];

      // reload the web browser
      if (dataGridView1.CurrentCell == null || dataGridView1.CurrentCell.RowIndex == selectedRow || _suppressChangeRow)
      {
        // selected row did not change, but the content did
        // force the article reload
        LoadCurrentArticle();
      }
    }

    // Update link in address bar from selected article
    private void UpdateAddressBar()
    {
      if (dataGridView1.CurrentCell != null)
      {
        int selectedRow = dataGridView1.CurrentCell.RowIndex;
        if (selectedRow >= 0 && selectedRow < _index.Count)
        {
          ArticleItemWithDb article = GetArticle(selectedRow);
          if (!article.IsNone())
          {
            _addressBarChanging = true;
            _addressBar.Text = article._item.GetLink(article._database, false);
            _addressBarChanging = false;
            _addressBar.BackColor = SystemColors.Window;
            return;
          }
        }
      }
      _addressBarChanging = true;
      _addressBar.Text = "";
      _addressBarChanging = false;
    }

    private bool IsVirtualTag(string tag)
    {
      if (tag.Equals(_tagToRead)) return true;
      if (tag.Equals(_tagChanged)) return true;
      if (tag.Equals(_tagToMe)) return true;
      if (tag.Equals(_tagByMe)) return true;
      if (tag.Equals(_tagDraft)) return true;
      if (tag.Equals(_tagChat)) return true;
      if (tag.Equals(_tagHomepage)) return true;
      if (tag.Equals(_tagToday)) return true;
      if (tag.Equals(_tagLastWeek)) return true;
      if (tag.Equals(_tagLastMonth)) return true;
      string prefix = string.Format(_tagBy, "");
      if (tag.StartsWith(prefix))
      {
        string name = tag.Substring(prefix.Length);
        foreach (SQLDatabase db in databases)
        {
          foreach (string user in db.users.Keys)
          {
            if (user.Equals(name, StringComparison.OrdinalIgnoreCase)) return true;
          }
        }
      }
      prefix = string.Format(_tagDb, "");
      if (tag.StartsWith(prefix))
      {
        string name = tag.Substring(prefix.Length);
        foreach (SQLDatabase db in databases)
        {
          if (db.name.Equals(name, StringComparison.OrdinalIgnoreCase)) return true;
        }
      }
      return false;
    }

    private bool IsValidTag(string tag)
    {
      if (IsVirtualTag(tag)) return true;
      foreach (SQLDatabase db in _databases)
      {
        if (db.TagExists(tag)) return true;
      }
      return false;
    }

    private List<string> ExtractTags(string text)
    {
      List<string> tags = new List<string>();

      char[] separators = { ',', ' ', '\t', '\r', '\n', '\f' };
      int done = 0;
      int index = text.IndexOfAny(separators);
      while (index >= 0)
      {
        string tag = text.Substring(done, index - done).ToLower();
        if (tag.Length > 0)
        {
          if (IsValidTag(tag)) tags.Add(tag);
          else return null;
        }
        done = index + 1;
        index = text.IndexOfAny(separators, done);
      }
      {
        string tag = text.Substring(done).ToLower();
        if (tag.Length > 0)
        {
          if (IsValidTag(tag)) tags.Add(tag);
          else return null;
        }
      }

      if (tags.Count == 0) return null;
      return tags;
    }

    // Check if url in address bar is valid, set corresponding color and show article
    private bool GoToAddressBarUrl(bool onlyCheck)
    {
      try
      {
        string location = _addressBar.Text;
        if (location.Length == 0)
        {
          _addressBar.BackColor = SystemColors.Window;
        }
        else
        {
          bool urlOk = false;

          List<string> tags = ExtractTags(location);
          if (tags != null)
          {
            urlOk = true;
      
            // Go to tags
            if (!onlyCheck)
            {
              // Clear current filter
              _tags.Clear();
              
              // Set new filter
              tagsNavigator.Stop();
              foreach (string tag in tags)
                _tags.Add(new TagCondition(tag, true));

              ResetTagCloud();
              UpdateTitle();
              UpdateArticles(ArticleId.None);
              UpdateNavigateHistory();
            }
          }

          // Go to url
          if (!urlOk)
          {
            try
            {
              Uri url = new Uri(location);
              if (onlyCheck)
                urlOk = CheckArticleExists(url);
              else
                urlOk = GoToArticle(url);
            }
            catch
            {
            }
            if (!urlOk && !onlyCheck)
            {
              // search for given text
              DialogSearch dialog = new DialogSearch(this, location);
              dialog.Show();
            }
          }

          if (urlOk)
          {
            _addressBar.BackColor = SystemColors.Window;
            return true;
          }
          else
            _addressBar.BackColor = Color.LightGray;
        }
      }
      catch (System.Exception e)
      {
        Console.Write(e.ToString());
      }
      return false;
    }

    private void _addressBar_KeyDown(object sender, KeyEventArgs e)
    {
      if (!e.Handled && e.KeyCode == Keys.Enter)
      {
        GoToAddressBarUrl(false);
      }
      // avoid default processing of these keys
      if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
      {
        e.Handled = true;
        e.SuppressKeyPress = true;
      }
    }
    private void _addressBar_KeyUp(object sender, KeyEventArgs e)
    {
      // avoid default processing of these keys
      if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
      {
        e.Handled = true;
        e.SuppressKeyPress = true;
      }
    }
    private void _addressBar_TextChanged(object sender, EventArgs e)
    {
      if (_addressBarChanging)
        return;
      GoToAddressBarUrl(true);
    }


    // find the home page, prefer the given database
    private int FindHomePage(SQLDatabase preferredDatabase)
    {
      // first check the preferred database
      if (preferredDatabase != null)
      {
        int row = FindHomePageInternal(preferredDatabase);
        if (row >= 0) return row;
      }
      // then all others
      foreach (SQLDatabase database in _databases)
      {
        if (database != null && database != preferredDatabase)
        {
          int row = FindHomePageInternal(database);
          if (row >= 0) return row;
        }
      }
      return -1;
    }
    private int FindHomePageInternal(SQLDatabase database)
    {
      if (database.homepage <= 0) return -1;
      return _index.FindIndex(delegate(IndexItem item) 
      { 
        return item._articleId._database == database && item._articleId._id == database.homepage; 
      });
    }
    // find the id of home page, prefer the given database
    private ArticleId FindHomePageId(SQLDatabase preferredDatabase)
    {
      // first check the preferred database
      if (preferredDatabase != null)
      {
        int id = preferredDatabase.homepage;
        if (id > 0) return new ArticleId(preferredDatabase, id);
      }
      // then all others
      foreach (SQLDatabase database in _databases)
      {
        if (database != null && database != preferredDatabase)
        {
          int id = database.homepage;
          if (id > 0) return new ArticleId(database, id);
        }
      }
      return ArticleId.None;
    }

    // Add a new item into `tag_dictionary`. If it already exists, update its statistics.
    private void UpdateStats(Dictionary<string, TagsStatsItem> tagDictionary, string tag, ArticleItem item)
    { 
      if (!_tags.Contains(new TagCondition(tag, true)))  // hide tags in current selection
      {
        TagsStatsItem itemStats = new TagsStatsItem(item);
        if (!tagDictionary.ContainsKey(tag)) tagDictionary.Add(tag, itemStats);
        else tagDictionary[tag].CombineWith(itemStats);
      }
    }


    // Update the navigation through tags
    private void UpdateTags()
    {
      using (new WaitCursor())
      {
        // update the index of enabled articles which is used for summarization of tags not filtered out
        UpdateIndexEnabled();

        // Summarize the tags from articles not filtered out
        Dictionary<string, TagsStatsItem> tagsStats = new Dictionary<string, TagsStatsItem>();

        DateTime newestShown = DateTime.MinValue;
        // tagsStats is summarized from the _indexEnabled list of articles
        // no need to check all articles in each databases cache.Values
        for (int i = 0; i < _indexEnabled.Count; i++)
        {
          IndexItem index = _indexEnabled[i];
          SQLDatabase database = index._articleId._database;
          ArticleItem item;
          if (index._articleId._database == database && database.cache.TryGetValue(index._articleId._id, out item))
          {
            string dbTag = string.Format(_tagDb, database.name.ToLower());
            if (item._effectiveTags != null)
            {
              foreach (string tag in item._effectiveTags)
              {
                // avoid attribute tags
                if (tag.Length >= 1 && tag[0] == TagSpecialChars.TagAttribute) continue;
                if (tag.Length >= 2 && tag[0] == TagSpecialChars.TagPrivate && tag[1] == TagSpecialChars.TagAttribute) continue;
                UpdateStats(tagsStats, tag, item);
              }
            }
            if (item._date > newestShown) newestShown = item._date;
            // virtual tags
            UpdateStats(tagsStats, dbTag, item);  // current database
            if (item._toRead) UpdateStats(tagsStats, _tagToRead, item);
            if (item._changed) UpdateStats(tagsStats, _tagChanged, item);
            if (item._author.Equals(database.user, StringComparison.OrdinalIgnoreCase)) UpdateStats(tagsStats, _tagByMe, item);
            if (item.IsAssignedToMe(database)) UpdateStats(tagsStats, _tagToMe, item);
            if (!string.IsNullOrEmpty(item._draftLocal)) UpdateStats(tagsStats, _tagDraft, item);
            if (item._chatId>=0 || item._chatUsers!=null) UpdateStats(tagsStats, _tagChat, item);
            if (item.IsToday()) UpdateStats(tagsStats, _tagToday, item);
            if (item.IsLastWeek()) UpdateStats(tagsStats, _tagLastWeek, item);
            if (item.IsLastMonth()) UpdateStats(tagsStats, _tagLastMonth, item);
          }
        }

        // index of first letters of all tags
        SortedList<char, char> letters = new SortedList<char, char>();
        foreach (KeyValuePair<string, TagsStatsItem> pair in tagsStats)
        {
          char c = pair.Key[0];
          if (!letters.ContainsKey(c)) letters.Add(c, c);
        }

        // Create index of all tags, compute geometric average and maximum of counts
        double avgCount = 0,
               maxCount = double.MinValue;

        // filter general tags - use only the important enough
        const int maxGeneralTags = 50;
        List<KeyValuePair<string, TagsStatsItem>> pairs = new List<KeyValuePair<string, TagsStatsItem>>();

        if (_tagsFilter != '\0')
        {
          // filter tags
          foreach (KeyValuePair<string, TagsStatsItem> pair in tagsStats)
          {
            char c = pair.Key[0];
            if (c == _tagsFilter) pairs.Add(pair);
          }
          /*
          if (_tagsFilter != '.' && _tagsFilter != '/' && _tagsFilter != '@' && _tagsFilter != ':')
          {
            pairs.Sort(new SortByCountAndAge());
            if (pairs.Count > maxGeneralTags) pairs.RemoveRange(maxGeneralTags, pairs.Count - maxGeneralTags);
          }
          */
        }
        else if (_tagsLimit)
        {
          // show limited count of tags
          foreach (KeyValuePair<string, TagsStatsItem> pair in tagsStats)
          {
            char c = pair.Key[0];
            if (c != '.' && c != '/' && c != '@' && c != ':') pairs.Add(pair);
          }
          pairs.Sort(new SortByCountAndAge());
          if (pairs.Count > maxGeneralTags) pairs.RemoveRange(maxGeneralTags, pairs.Count - maxGeneralTags);

          // do not filter special tags
          foreach (KeyValuePair<string, TagsStatsItem> pair in tagsStats)
          {
            char c = pair.Key[0];
            if (c == '.' || c == '/' || c == '@' || c == ':') pairs.Add(pair);
          }
        }
        else
        {
          // show all tags
          foreach (KeyValuePair<string, TagsStatsItem> pair in tagsStats)
          {
            pairs.Add(pair);
          }
        }

        if (pairs.Count > 0)
        {
          double sumLogCount = 0;

          foreach (KeyValuePair<string, TagsStatsItem> pair in pairs)
          {
            sumLogCount += Math.Log(pair.Value._total);
            maxCount = Math.Max(maxCount, pair.Value._total);
          }
          avgCount = Math.Exp(sumLogCount / pairs.Count);

          pairs.Sort(new SortByName());
        }

        // Create the tag navigator
        string content = @"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01//EN"" ""http://www.w3.org/TR/html4/strict.dtd"">
<html><head><meta http-equiv=""Content-Type"" content=""text/html;charset=utf-8"">
<title>Tag navigator</title>
<style type=""text/css"">
body{font:100% Calibri,Tahoma,Geneva,""Bitstream Vera Sans"",sans-serif}
hr{clear:both}
a{text-decoration:none;white-space:nowrap}
a span{display:none;position:absolute;top:0;right:0;font-size:14px}
a:link,a:visited,a.c0{color:#000}
a.c1{color:#00f}
a.c2{color:#3800e9}
a.c3{color:#6400de}
a.c4{color:#8000d2}
a.c5{color:#9500c5}
a.c6{color:#a700b6}
a.c7{color:#b600a7}
a.c8{color:#c50095}
a.c9{color:#d20080}
a.c10{color:#de0064}
a.c11{color:#e90038}
a.c12{color:#f00}
a.c20{color:#647964}
a.c21{color:#8aa58a}
a.c22{color:#a6c6a6}
a.c23{color:#bde1bd}
a:hover{background:#08c;color:#fff}
a:hover span{display:block;background:#ffc;color:#000}
</style>
<body>
";

        // selected tags
        for (int i = -1; i < _tags.Count; i++)
        {
          string name;
          if (i < 0) name = "all";
          else if (_tags[i]._present) name = _tags[i]._tag;
          else name = "NOT " + _tags[i]._tag;
          if (i == _tags.Count - 1)
            content = content + name;
          else
            content = content + string.Format("<a href=\"Level({0:D})\">{1}</a> - ", i + 1, name);
        }
        content = content + "<hr>";

        // letters
        if (_tagsFilter == '\0' && !_tagsLimit)
          content += "<b>All tags</b> ";
        else
          content += "<a href=\"AllTags()\">All tags</a> ";
        if (_tagsFilter == '\0' && _tagsLimit)
          content += "<b>Most used</b> ";
        else
          content += "<a href=\"MostUsed()\">Most used</a> ";
        foreach (char letter in letters.Keys)
        {
          if (_tagsFilter == letter)
            content += string.Format("<b>{0}</b> ", char.ToUpper(letter));
          else
            content += string.Format("<a href=\"Letter({0})\">{1}</a> ", letter, char.ToUpper(letter));
        }
        content = content + "<hr>";

        // tag cloud
        const int avgSize = 88;                              // avgCount has this size (in percent)
        double doubleSizeCountRatio = Math.Max(2, maxCount); // the biggest tag is twice as big as the tiniest tag, depends on log(article count)
        const int minSize = 63;                              // but enforce minimum size for the small tags

        for (int i = 0; i < pairs.Count; i++)
        {
          // font size calculation: number of articles
          int size = (int)(avgSize * Math.Pow(2, Math.Log(pairs[i].Value._total / avgCount, doubleSizeCountRatio)));
          size = Math.Max(minSize, size);

          int tag_color = 0;  // color = black

          string label = string.Format("{0:D} article{1}", pairs[i].Value._total, pairs[i].Value._total == 1 ? "" : "s");

          // if there are any unread articles, color = blue..red: number of unread articles
          if (pairs[i].Value._unread > 0)
          {
            label += string.Format(", {0:D} to read", pairs[i].Value._unread);
            tag_color = Math.Min(12, (int)(Math.Log(pairs[i].Value._unread + 4) * 4) - 5);
          }
          // if all articles were read, color = black..greenish: newest article age (levels: 4 days, 2 weeks, 2 months, 1 year)
          else
          {
            label += string.Format(", newest {0:yyyy'-'MM'-'dd}", pairs[i].Value._newest);
            if (pairs[i].Value._newest < newestShown.AddYears(-1)) tag_color = 23;
            else if (pairs[i].Value._newest < newestShown.AddMonths(-2)) tag_color = 22;
            else if (pairs[i].Value._newest < newestShown.AddDays(-14)) tag_color = 21;
            else if (pairs[i].Value._newest < newestShown.AddDays(-4)) tag_color = 20;
          }

          content += string.Format(
              "<a href=\"Tag({0})\" class=\"c{3:D}\" title=\"{1}\" style=\"font-size:{2}%\">{0}<span>{1}</span></a> ",
              pairs[i].Key, label, size, tag_color);
        }
        /*
              content = content + "<HR>";
              // navigation to root virtual tags
              content = content + string.Format("<a href=\"Root({0})\">{0}</a><br>", _tagToRead);
              content = content + string.Format("<a href=\"Root({0})\">{0}</a><br>", _tagToMe);
              content = content + string.Format("<a href=\"Root({0})\">{0}</a><br>", _tagByMe);
              content = content + string.Format("<a href=\"Root({0})\">{0}</a><br>", _tagDraft);
              content = content + string.Format("<a href=\"Root({0})\">{0}</a><br>", _tagChanged);
              foreach (SQLDatabase database in _databases)
              {
                string dbTag = string.Format(_tagDb, database.name.ToLower());
                content = content + string.Format("<a href=\"Root({0})\">{0}</a><br>", dbTag);
              }
        */
        tagsNavigator.DocumentText = content;

        if (tagsNavigator.Document != null)
        {
          tagsNavigator.Document.MouseOver -= new HtmlElementEventHandler(TagsDocument_MouseOver);
          tagsNavigator.Document.MouseOver += new HtmlElementEventHandler(TagsDocument_MouseOver);
        }
      }
    }

    // Statistics for a single tag.
    private class TagsStatsItem
    {
      public int _total;
      public int _unread;
      public DateTime _newest;

      // Stats for nothing.
      public TagsStatsItem() { _total = 0; _unread = 0; _newest = new DateTime(DateTime.MinValue.Ticks); }

      // Stats for a single article.
      public TagsStatsItem(ArticleItem item) { _total = 1; _unread = item._toRead ? 1 : 0; _newest = item._date; }

      // Combine two statistics.
      public void CombineWith(TagsStatsItem rhs)
      {
        _total += rhs._total;
        _unread += rhs._unread;
        if (_newest < rhs._newest) _newest = rhs._newest;
      }
    };

    private class SortByCountAndAge : IComparer<KeyValuePair<string, TagsStatsItem>>
    {
      private DateTime _now = DateTime.Now;

      public int Compare(KeyValuePair<string, TagsStatsItem> i1, KeyValuePair<string, TagsStatsItem> i2)
      {
        double age1 = (i1.Value._newest - _now).TotalDays;
        if (age1 < 1) age1 = 1;
        double val1 = i1.Value._total / age1;

        double age2 = (i2.Value._newest - _now).TotalDays;
        if (age2 < 1) age2 = 1;
        double val2 = i2.Value._total / age2;
        
        return Math.Sign(val2 - val1);
      }
    }
    private class SortByName : IComparer<KeyValuePair<string, TagsStatsItem>>
    {
      public int Compare(KeyValuePair<string, TagsStatsItem> i1, KeyValuePair<string, TagsStatsItem> i2)
      {
        return i1.Key.CompareTo(i2.Key);
      }
    }

    private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      switch (e.ColumnIndex)
      {
        case _columnTitle:
          // sort by title
          if (_selectedComparer is CompareTitles) _selectedComparer.reverse = !_selectedComparer.reverse;
          else _selectedComparer = new CompareTitles(this, false);
          break;
        case _columnAuthor:
          // sort by author
          if (_selectedComparer is CompareAuthors) _selectedComparer.reverse = !_selectedComparer.reverse;
          else _selectedComparer = new CompareAuthors(this, false);
          break;
        case _columnDate:
          // sort by date
          if (_selectedComparer is CompareDates) _selectedComparer.reverse = !_selectedComparer.reverse;
          else _selectedComparer = new CompareDates(this, false);
          break;
        case _columnIcon:
          // custom sort
          if (_selectedComparer is CompareCustom) _selectedComparer.reverse = !_selectedComparer.reverse;
          else _selectedComparer = new CompareCustom(this, false);
          break;
      }
      if (_selectedComparer != null)
      {
        // get the currentRow to select it after sorting again
        DataGridViewRow curRow = dataGridView1.CurrentRow;
        ArticleItemWithDb article = curRow != null ? GetArticle(curRow.Index) : ArticleItemWithDb.None;
        
        _index.Sort(new IndexComparer(_selectedComparer.GetComparer()));

        // find the row containing previously active article and make it active again
        foreach (DataGridViewRow row in dataGridView1.Rows)
        {
          ArticleItemWithDb item = GetArticle(row.Index);
          if (item._database == article._database && item._item == article._item)
          {
            dataGridView1.CurrentCell = row.Cells[0];
            break;
          }
        }

        // Update SortGlyphDirection
        DataGridViewColumn sortColumn = dataGridView1.Columns[e.ColumnIndex];
        if (sortColumn!=null)
          sortColumn.HeaderCell.SortGlyphDirection = _selectedComparer.reverse ? SortOrder.Descending : SortOrder.Ascending;
        // Hide SortGlyphDirection for all other columns
        foreach (DataGridViewColumn column in dataGridView1.Columns)
        {
          if (column != sortColumn) column.HeaderCell.SortGlyphDirection = SortOrder.None;
        }
        // store sorted method to config
        _sortOrderMethod = _selectedComparer.ToString();
        _sortOrderDir = _selectedComparer.reverse;
      }
      dataGridView1.Invalidate();
    }

    private void FormColabo_FormClosing(object sender, FormClosingEventArgs e)
    {
      Application.RemoveMessageFilter(this);
    }

    // background worker helpers
    public bool RunTask(ITask task)
    {
      if (backgroundWorker.IsBusy) return false; // cannot run now

      backgroundWorker.RunWorkerAsync(task);
      return true;
    }
    public bool IsTaskRunning()
    {
      return backgroundWorker.IsBusy;
    }
    public void AddTask(ITask task, int priority)
    {
      Program.AddTask(task, priority);
    }
    public void AddTask(ITask task)
    {
      Program.AddTask(task);
    }
    // update the UI to show background worker state
    public void UpdateProgress(int progress, string message, int[] tasks, string[] taskTypes)
    {
      if (message != null)
      {
        // when no message is given, keep the previous
        taskState.Text = message;
        taskState.Invalidate();
      }
      taskProgress.Value = progress;
      int total = 0;
      string progressMessage = "";
      for (int i = 0; i < tasks.Length; i++)
      {
        total += tasks[i];
        if (i > 0) progressMessage += ", ";
        progressMessage += string.Format("{0:D} {1}", tasks[i], taskTypes[i]);
      }
      queueState.Text = total > 0 ? "To process: " + progressMessage : "(No more tasks)";
      queueState.Invalidate();
    }
    public void CreateLPTasks()
    {
      foreach (SQLDatabase database in _databases)
      {
        List<ArticleItem> articles = new List<ArticleItem>(database.cache.Values);

        ITask task;
        if (database.IsFullTextIndexLocal())
        {
          // start the indexing process
          task = new TaskFindRepositories(this, database, articles);
          AddTask(task,1);
        }

        task = new TaskCheckUpdates(this, database, articles);
        AddTask(task,1);
      }
    }

    private void dataGridView1_ColumnDisplayIndexChanged(object sender, DataGridViewColumnEventArgs e)
    {
      // Only last column should have AutoSizeMode set to Fill, other must have None
      // It is necessary in order to be able to set columns width from configuration file
      DataGridViewColumnCollection columns = dataGridView1.Columns;
      int lastColumn = columns.Count-1;
      for (int i=0; i<columns.Count; i++)
      {
        if (columns[i].DisplayIndex == lastColumn)
          columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        else
          columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
      }
    }

    public void SendArticle(SQLDatabase database, Colabo.ArticleItem item)
    {
      item._id = database.SendArticle(item);
      // synchronize only the affected database
      Synchronize(database, ArticleId.None); // Synchronize(id)
    }
    public void UpdateRevisionAndDate(ArticleItemWithDb item)
    {
      if (item._database != null)
      {
        item._database.UpdateRevisionAndDate(item._item);
        // synchronize the cache (only the affected database) and the UI
        Synchronize(item._database, ArticleId.None);
      }
    }
    public void DeleteArticle(ArticleItemWithDb item)
    {
      if (item._database != null)
      {
        if (!string.IsNullOrEmpty(item._item._draftLocal))
        {
          File.Delete(item._item._draftLocal);
        }
        if (item._item._chatId >= 0)
        {
          item._database.DeleteChat(item._item._chatId);
        }
        if (item._item._id >= 0) item._database.DeleteArticle(item._item);
        // synchronize the cache (only the affected database) and the UI
        Synchronize(item._database, ArticleId.None);
      }
    }
    public void ChangeParent(ArticleItemWithDb item, int parent)
    {
      if (item._database != null)
      {
        if (!string.IsNullOrEmpty(item._item._draftLocal))
        {
          item._item._parent = parent;
          item._database.SaveArticleHeader(item._item);
        }
        if (item._item._chatId>=0)
        {
          item._item._parent = parent;
          throw new Exception("Chat: ChangeParent not implemented");
        }
        if (item._item._id >= 0) item._database.ChangeParent(item._item, parent);
        // synchronize the cache (only the affected database) and the UI
        Synchronize(item._database, ArticleId.None);
      }
    }
    public void SaveArticleExpanded(ArticleItemWithDb item)
    {
      if (item._database != null)
      {
        if (!string.IsNullOrEmpty(item._item._draftLocal))
        {
          item._item._expanded = true;
          item._database.SaveArticleHeader(item._item);
        }
        if (item._item._id >= 0) item._database.SaveArticleExpanded(item._item._id);
      }
    }
    public void SaveArticleCollapsed(ArticleItemWithDb item)
    {
      if (item._database != null)
      {
        if (!string.IsNullOrEmpty(item._item._draftLocal))
        {
          item._item._expanded = false;
          item._database.SaveArticleHeader(item._item);
        }
        if (item._item._id >= 0) item._database.SaveArticleCollapsed(item._item._id);
      }
    }

    private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
    {
      _contextMenuRow = e.RowIndex;
      editToolStripMenuItem_Click(this, new EventArgs());
    }

    // drag & drop support
    private void dataGridView1_DragDrop(object sender, DragEventArgs e)
    {
      Point clientPoint = dataGridView1.PointToClient(new Point(e.X, e.Y));
      DataGridView.HitTestInfo info = dataGridView1.HitTest(clientPoint.X, clientPoint.Y);

      // dragged article
      if (!e.Data.GetDataPresent(typeof(ArticleItemWithDb))) return;
      ArticleItemWithDb data = (ArticleItemWithDb)e.Data.GetData(typeof(ArticleItemWithDb));
      if (data.IsNone()) return;

      // mouse over article
      ArticleItemWithDb item = ArticleItemWithDb.None;
      if (info.RowIndex >= 0)
      {
        item = GetArticle(info.RowIndex);
        // cannot move to different database
        if (item._database != data._database) return;
        // cannot move under temporary article
        if (item._item._id < 0) return; 
        // avoid cycles
        if (IsDescendant(item._item, data._item, data._database)) return;
      }

      // valid or no parent
      int newParent = (item.IsNone()) ? -1 : item._item._id;
      if (newParent != data._item._parent)
      {
        // confirmation (can be easily activated by accident)
        string message;
        if (newParent < 0) message = string.Format("Do you want to move article '{0}' to root?", data._item._title);
        else message = string.Format("Do you want to make article '{0}' child of '{1}'?", data._item._title, item._item._title);

        DialogResult result = MessageBox.Show(message, "Colabo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        if (result == DialogResult.Yes) ChangeParent(data, newParent);
      }
    }
    private void dataGridView1_DragOver(object sender, DragEventArgs e)
    {
      // no action by default
      e.Effect = DragDropEffects.None;

      Point clientPoint = dataGridView1.PointToClient(new Point(e.X, e.Y));
      DataGridView.HitTestInfo info = dataGridView1.HitTest(clientPoint.X, clientPoint.Y);
      
      // dragged article
      if (!e.Data.GetDataPresent(typeof(ArticleItemWithDb))) return;
      ArticleItemWithDb data = (ArticleItemWithDb)e.Data.GetData(typeof(ArticleItemWithDb));
      if (data.IsNone()) return;

      // scroll on the edges
      if (clientPoint.Y < dataGridView1.DisplayRectangle.Top + 5)
      {
        dataGridView1.FirstDisplayedScrollingRowIndex--;
      }
      else if (clientPoint.Y > dataGridView1.DisplayRectangle.Bottom - 5)
      {
        dataGridView1.FirstDisplayedScrollingRowIndex++;
      }

      // mouse over article
      if (info.RowIndex >= 0)
      {
        ArticleItemWithDb item = GetArticle(info.RowIndex);
        // cannot move to different database
        if (item._database != data._database) return;
        // cannot move under temporary article
        if (item._item._id < 0) return;
        // avoid cycles
        if (IsDescendant(item._item, data._item, data._database)) return;
      }

      // valid or no parent
      e.Effect = DragDropEffects.Move;
    }
    private bool IsDescendant(ArticleItem item, ArticleItem parent, SQLDatabase database)
    {
      ArticleItem child = item;
      while (child != null)
      {
        if (child == parent) return true;
        // level up with child
        if (child._parent < 0) child = null;
        else child = GetArticleByID(database, child._parent);
      }

      return false;
    }

    private void dataGridView1_MouseDown(object sender, MouseEventArgs e)
    {
      DataGridView.HitTestInfo info = dataGridView1.HitTest(e.X, e.Y);
      _mouseDownRowIndex = info.RowIndex;
      if (info.RowIndex >= 0)
      {
        Size dragSize = SystemInformation.DragSize;
        _dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2), e.Y - (dragSize.Height / 2)), dragSize);
      }
      else
      {
        _dragBoxFromMouseDown = Rectangle.Empty;
      }
    }
    private void dataGridView1_MouseUp(object sender, MouseEventArgs e)
    {
      _dragBoxFromMouseDown = Rectangle.Empty;
    }
    private void dataGridView1_MouseMove(object sender, MouseEventArgs e)
    {
      if ((e.Button & MouseButtons.Left) == MouseButtons.Left && // left mouse button hold
        _dragBoxFromMouseDown != Rectangle.Empty && !_dragBoxFromMouseDown.Contains(e.X, e.Y)) // outside dragBox
      {
        ArticleItemWithDb item = GetArticle(_mouseDownRowIndex);
        if (!item.IsNone()) dataGridView1.DoDragDrop(item, DragDropEffects.Move);
        _dragBoxFromMouseDown = Rectangle.Empty;
        _mouseDownRowIndex = -1;
      }
    }

    private void checkFulltextIndicesToolStripMenuItem_Click(object sender, EventArgs e)
    {
      string message = "Indices: \n\n";
      foreach (SQLDatabase database in _databases)
      {
        message += string.Format("  {0} ... {1} \n", database.name, database.CheckFullTextIndex());
      }
      MessageBox.Show(message, "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }
    private void SVNBenchmarkToolStripMenuItem_Click(object sender, EventArgs e)
    {
      // SVN commands needs to be invoked from the task
      ITask task = new TaskSVNBenchmark(databases);
      AddTask(task);
    }

    private void homeButton_Click(object sender, EventArgs e)
    {
      GoHome();
    }
    private void GoHome()
    {
      SQLDatabase database = null;
      if (dataGridView1.CurrentCell != null)
      {
        int selectedRow = dataGridView1.CurrentCell.RowIndex;
        if (selectedRow >= 0 && selectedRow < _index.Count) database = _index[selectedRow]._articleId._database;
      }

      ArticleId id = FindHomePageId(database);
      if (id != ArticleId.None) GoToArticle(id._database, id._id);
    }
    private void navigateBackButton_Click(object sender, EventArgs e)
    {
      NavigateBack();
    }
    private void NavigateBack()
    {
      try
      {
        if (_navigateHistoryIndex > 0)
        {
          _navigateHistoryIndex--;
          _navigatingInHistory = true;
          ArticleId id = FindArticleID(new Uri(_navigateHistory[_navigateHistoryIndex].url));
          _tags = new List<TagCondition>(_navigateHistory[_navigateHistoryIndex].tags);
          UpdateArticles(id);
        }
      }
      finally
      {
        _navigatingInHistory = false;
      }
    }

    private void navigateForwardButton_Click(object sender, EventArgs e)
    {
      NavigateForward();
    }
    private void NavigateForward()
    {
      try
      {
        if (_navigateHistoryIndex >= 0 && _navigateHistoryIndex < _navigateHistory.Count - 1)
        {
          _navigateHistoryIndex++;
          _navigatingInHistory = true;
          ArticleId id = FindArticleID(new Uri(_navigateHistory[_navigateHistoryIndex].url));
          _tags = new List<TagCondition>(_navigateHistory[_navigateHistoryIndex].tags);
          UpdateArticles(id);
        }
      }
      finally
      {
        _navigatingInHistory = false;
      }
    }

    private string HumanReadable(string tag)
    {
      // skip any leading dots and symbols like that
      while (tag[0] == TagSpecialChars.TagPrivate || tag[0] == '/') tag = tag.Substring(1);
      // make first letter uppercase
      tag = tag.Substring(0, 1).ToUpper() + tag.Substring(1);
      return tag;
    }

    // Bookmarks context menu
    Bookmark _contextMenuBookmark;
    private void bookmarks_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
    {
      if (e.Node == null) return;
      Bookmark bookmark = (Bookmark)e.Node.Tag;
      if (bookmark == null) return;

      if (e.Button == MouseButtons.Left)
      {
        if (e.Node.Bounds.Contains(e.Location)) OpenBookmark(bookmark);
      }
      else if (e.Button == MouseButtons.Right)
      {
        // store the current bookmark
        _contextMenuBookmark = bookmark;
        
        // show menu items based on context

        // evaluate conditions
        bool validBookmark = _contextMenuBookmark._article != ArticleId.None || (_contextMenuBookmark._tags != null && _contextMenuBookmark._tags.Count > 0);
        bool bookmarkInDatabase = _contextMenuBookmark._database != null;
        bool userBookmark = bookmarkInDatabase && _contextMenuBookmark._id >= 0;

        // set visibility of items
        openBookmarkToolStripMenuItem.Visible = validBookmark;
        openBookmarkInNewWindowToolStripMenuItem.Visible = validBookmark;
        toolStripSeparator9.Visible = validBookmark && userBookmark;
        editBookmarkToolStripMenuItem.Visible = userBookmark;
        deleteBookmarkToolStripMenuItem.Visible = userBookmark;
        toolStripSeparator10.Visible = (validBookmark || userBookmark) && bookmarkInDatabase;
        addBookmarkToolStripMenuItem.Visible = bookmarkInDatabase;

        // open the context menu
        contextMenuBookmarks.Show(bookmarks, e.Location);
      }

      bookmarks.SelectedNode = null;
    }

    private void openBookmarkToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (_contextMenuBookmark != null) OpenBookmark(_contextMenuBookmark);
    }
    private void openBookmarkInNewWindowToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (_contextMenuBookmark != null)
      {
        FormColabo form = Program.NewForm();

        SQLDatabase oldDb = _contextMenuBookmark._article._database;
        if (oldDb != null)
        {
          // translate ArticleId - new form is using new databases
          _contextMenuBookmark._article._database = form.databases.Find(delegate(SQLDatabase item)
          {
            return item.server.Equals(oldDb.server, StringComparison.OrdinalIgnoreCase) &&
              item.database.Equals(oldDb.database, StringComparison.OrdinalIgnoreCase);
          });
        }

        // open bookmark when all initial synchronization will be done (queue it as a task)
        ITask task = new TaskOpenBookmark(form, _contextMenuBookmark);
        form.AddTask(task);
      }
    }
    private void editBookmarkToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (_contextMenuBookmark != null && _contextMenuBookmark._database != null && _contextMenuBookmark._id >= 0)
      {
        DialogBookmark dialog = new DialogBookmark(this, _contextMenuBookmark);
        DialogResult result = dialog.ShowDialog();
        if (result == DialogResult.OK)
        {
          Bookmark bookmark = dialog.bookmark;
          if (bookmark._database != null)
          {
            bookmark._database.UpdateBookmark(bookmark);
            UpdateBookmarks();
          }
        }
      }
    }
    private void addBookmarkToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (_contextMenuBookmark != null && _contextMenuBookmark._database != null)
      {
        Bookmark bookmark = new Bookmark(_contextMenuBookmark._database);
        bookmark._parent = _contextMenuBookmark._id;
        bookmark._image = 2;

        if (_tags != null && _tags.Count > 0)
        {
          bookmark._tags = new List<string>();
          bookmark._name = "";
          foreach (TagCondition tag in _tags)
          {
            if (bookmark._name.Length > 0) bookmark._name += " && ";
            string t = (tag._present ? "" : "!") + tag._tag;
            bookmark._tags.Add(t);
            bookmark._name += t;
          }
        }

        if (dataGridView1.CurrentCell != null)
        {
          int selectedRow = dataGridView1.CurrentCell.RowIndex;
          if (selectedRow >= 0 && selectedRow < _index.Count)
          {
            ArticleItemWithDb article = GetArticle(selectedRow);
            if (!article.IsNone())
            {
              bookmark._image = 3;
              bookmark._name = article._item._title;
              bookmark._article = new ArticleId(article._database, article._item._id);
            }
          }
        }

        DialogBookmark dialog = new DialogBookmark(this, bookmark);
        DialogResult result = dialog.ShowDialog();
        if (result == DialogResult.OK)
        {
          bookmark = dialog.bookmark;
          if (bookmark._database != null)
          {
            bookmark._database.AddBookmark(bookmark);
            UpdateBookmarks();
          }
        }
      }
    }
    private void newFolderToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (_contextMenuBookmark != null && _contextMenuBookmark._database != null)
      {
        Bookmark bookmark = new Bookmark(_contextMenuBookmark._database);
        bookmark._parent = _contextMenuBookmark._id;
        bookmark._image = 1;
        bookmark._name = "New Folder";

        DialogBookmark dialog = new DialogBookmark(this, bookmark);
        DialogResult result = dialog.ShowDialog();
        if (result == DialogResult.OK)
        {
          bookmark = dialog.bookmark;
          if (bookmark._database != null)
          {
            bookmark._database.AddBookmark(bookmark);
            UpdateBookmarks();
          }
        }
      }
    }
    private void deleteBookmarkToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (_contextMenuBookmark != null && _contextMenuBookmark._database != null && _contextMenuBookmark._id >= 0)
      {
        string message = string.Format("Are you sure to delete bookmark '{0}'?", _contextMenuBookmark._name);
        DialogResult result = MessageBox.Show(message, "Colabo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        if (result == DialogResult.Yes)
        {
          _contextMenuBookmark._database.DeleteBookmark(_contextMenuBookmark._id);
          UpdateBookmarks();
        }
      }
    }

    private void bookmarks_ItemDrag(object sender, ItemDragEventArgs e)
    {
      if (e.Item is TreeNode)
      {
        TreeNode node = (TreeNode)e.Item;
        Bookmark bookmark = (Bookmark)node.Tag;
        // drag & drop possible only for user added bookmarks
        if (bookmark._database != null && bookmark._id >= 0)
        {
          bookmarks.DoDragDrop(bookmark, DragDropEffects.Move);
        }
      }
    }
    private void bookmarks_DragOver(object sender, DragEventArgs e)
    {
      // no action by default
      e.Effect = DragDropEffects.None;

      // what bookmark we are pointing at
      Point clientPoint = bookmarks.PointToClient(new Point(e.X, e.Y));
      TreeNode node = bookmarks.GetNodeAt(clientPoint);
      if (node == null) return;
      Bookmark parent = (Bookmark)node.Tag;

      // dragging bookmark
      if (e.Data.GetDataPresent(typeof(Bookmark)))
      {
        Bookmark data = (Bookmark)e.Data.GetData(typeof(Bookmark));
        if (data._database == parent._database && !data._database.IsAncestor(data, parent))
        {
          // possible to drag article here
          e.Effect = DragDropEffects.Move;
          return;
        }
      }

      // dragging article
      if (e.Data.GetDataPresent(typeof(ArticleItemWithDb)))
      {
        ArticleItemWithDb data = (ArticleItemWithDb)e.Data.GetData(typeof(ArticleItemWithDb));
        if (!data.IsNone())
        {
          if (data._database == parent._database)
          {
            // possible to drag article here
            e.Effect = DragDropEffects.Move;
            return;
          }
        }
      }
    }
    private void bookmarks_DragDrop(object sender, DragEventArgs e)
    {
      // what bookmark we are pointing at
      Point clientPoint = bookmarks.PointToClient(new Point(e.X, e.Y));
      TreeNode node = bookmarks.GetNodeAt(clientPoint);
      if (node == null) return;
      Bookmark parent = (Bookmark)node.Tag;

      // dragging bookmark
      if (e.Data.GetDataPresent(typeof(Bookmark)))
      {
        Bookmark data = (Bookmark)e.Data.GetData(typeof(Bookmark));
        if (data._database == parent._database && !data._database.IsAncestor(data, parent))
        {
          // possible to drag article here
          data._parent = parent._id;
          data._database.UpdateBookmark(data);
          UpdateBookmarks();
          return;
        }
      }

      // dragging article
      if (e.Data.GetDataPresent(typeof(ArticleItemWithDb)))
      {
        ArticleItemWithDb item = (ArticleItemWithDb)e.Data.GetData(typeof(ArticleItemWithDb));
        if (!item.IsNone())
        {
          if (item._database == parent._database)
          {
            // possible to drag article here
            if (item._database != null && item._item != null && item._item._id > 0)
            {
              Bookmark bookmark = new Bookmark(item._database);
              bookmark._name = item._item._title;
              bookmark._image = 3;
              bookmark._article = new ArticleId(item._database, item._item._id);
              bookmark._parent = parent._id;

              bookmark._database.AddBookmark(bookmark);
              UpdateBookmarks();
            }
            return;
          }
        }
      }
    }

    public void OpenBookmark(Bookmark bookmark)
    {
      // tags filtering
      if (bookmark._tags != null)
      {
        _tags.Clear();
        foreach (string tag in bookmark._tags)
        {
          if (string.IsNullOrEmpty(tag)) continue;
          if (tag[0] == '!')
            _tags.Add(new TagCondition(tag.Substring(1), false));
          else
            _tags.Add(new TagCondition(tag, true));
        }
        ResetTagCloud();
        UpdateTitle();
        UpdateArticles(bookmark._article);
      }
      UpdateNavigateHistory();
      if (bookmark._article != ArticleId.None) GoToArticle(bookmark._article._database, bookmark._article._id);
    }

    // create the list of bookmarks
    private void UpdateBookmarks()
    {
      bookmarks.BeginUpdate();

      bookmarks.Nodes.Clear();

      foreach (Bookmark bookmark in Bookmark.DefaultBookmarks)
      {
        TreeNode node = new TreeNode();
        node.Tag = bookmark;
        node.Text = bookmark._name;
        node.ImageIndex = bookmark._image;
        node.SelectedImageIndex = bookmark._image;
        bookmarks.Nodes.Add(node);
      }

      foreach (SQLDatabase database in _databases)
      {
        TreeNode node = new TreeNode();
        string text = string.Format("{0}:{1}", database.server, database.database);
        string id = string.Format(_tagDb, database.name.ToLower());
        {
          Bookmark bookmark = new Bookmark(database);
          bookmark._name = text;
          bookmark._image = 0;
          bookmark._tags = new List<string>(); bookmark._tags.Add(id);
          node.Tag = bookmark;
        }
        node.Text = database.name;
        node.ImageIndex = 0;
        node.SelectedImageIndex = 0;
        bookmarks.Nodes.Add(node);

        UpdateChildBookmarks(node, database.bookmarks, -1);
      }

      bookmarks.EndUpdate();
    }
    private void UpdateChildBookmarks(TreeNode parentNode, List<Bookmark> bookmarks, int parentID)
    {
      foreach (Bookmark bookmark in bookmarks)
      {
        if (bookmark._parent == parentID)
        {
          TreeNode node = new TreeNode();
          node.Tag = bookmark;
          node.Text = bookmark._name;
          node.ImageIndex = bookmark._image;
          node.SelectedImageIndex = bookmark._image;
          parentNode.Nodes.Add(node);
          UpdateChildBookmarks(node, bookmarks, bookmark._id);         
        }
      }
      parentNode.Expand();
    }

    private void UpdateScripts()
    {
      // remove original scripts from UI
      foreach (UIScriptInfo info in _uiScripts)
      {
        if (info._item != null) RemoveItemRecursive(info._item);
      }
      _uiScripts.Clear();

      // register new scripts
      foreach (SQLDatabase database in _databases)
      {
        foreach (Script script in database.scripts)
        {
          string [] path = script._usage.Split('/');
          if (path.Length > 0)
          {
            if (path[0].Equals("menu", StringComparison.OrdinalIgnoreCase))
            {
              // create the item in the main menu tree
              ToolStripItemCollection menu = menuStrip1.Items;
              for (int i = 1; i < path.Length; i++)
              {
                menu = FindOrCreateItem(menu, path[i]);
              }
              ToolStripItem item = menu.Add(script._name);
              if (!string.IsNullOrEmpty(script._image))
              {
                Icon icon = null;
                if (_icons.TryGetValue(script._image, out icon)) item.Image = icon.ToBitmap();
              }
              item.Tag = script;
              item.Click += new EventHandler(OnPerformScript);

              // register the item
              UIScriptInfo info = new UIScriptInfo(database, script);
              info._item = item;
              _uiScripts.Add(info);
            }
          }
        }
      }
    }
    private static ToolStripItemCollection FindOrCreateItem(ToolStripItemCollection menu, String name)
    {
      for (int i = 0; i < menu.Count; i++)
      {
        ToolStripItem item = menu[i];
        if (item is ToolStripMenuItem && item.Text.Equals(name, StringComparison.OrdinalIgnoreCase))
          return ((ToolStripMenuItem)item).DropDown.Items;
      }

      {
        ToolStripItem item = menu.Add(name);
        return ((ToolStripMenuItem)item).DropDown.Items;
      }
    }
    private static void RemoveItemRecursive(ToolStripItem item)
    {
      ToolStrip owner = item.Owner;
      if (owner != null)
      {
        owner.Items.Remove(item);
        if (owner.Items.Count == 0)
        {
          // we can remove the parent item as well
          ToolStripItem parent = item.OwnerItem;
          if (parent != null) RemoveItemRecursive(parent);
        }
      }
    }
    private void OnPerformScript(object sender, EventArgs e)
    {
      ToolStripItem item = (ToolStripItem)sender;
      Script info = (Script)item.Tag;

      Assembly assembly = DialogScript.CompileScript(info._language, info._script, "Script", "Main");
      if (assembly != null) DialogScript.ExecuteScript(assembly, "Script", "Main", webBrowser1.ObjectForScripting as Scripting);
    }

    // diagnostics
    public static void Log(string text)
    {
      Program.Log(text);
    }

    private void aboutColaboToolStripMenuItem_Click(object sender, EventArgs e)
    {
      DialogAbout dialogAbout = new DialogAbout();
      dialogAbout.ShowDialog();
    }

    // implementation of IReloadPage
    public void ReloadPage()
    {
      LoadCurrentArticle();
    }

    private void imState_SelectedIndexChanged(object sender, EventArgs e)
    {
      object selIt = imState.SelectedItem;
      String text = selIt.ToString();
      ChangeIMState(text);
    }

    private void imUsers_MouseClick(object sender, MouseEventArgs e)
    {
      if (e.Button == MouseButtons.Right)
      {
        if (imUsers.FocusedItem.Bounds.Contains(e.Location) == true)
        {
          contextMenuIM.Show(Cursor.Position);
        }
      } 
    }

    internal void SetLocationName(string ipAddr, string location)
    {
      foreach (var db in databases)
      {
        if (CheckIMVisible(db))
        {
          db.SetLocationName(ipAddr, location);
        }
      }
    }

    private void setLocationNameMenuItem_Click(object sender, EventArgs e)
    {
      ChatUserItem item = (ChatUserItem)imUsers.FocusedItem;
      if (item == null) return;
      if (item.IPAddress.Length == 0) return;
      DialogSetLocation dialog = new DialogSetLocation(this,item.IPAddress,item.SubItems[3].Text);
      DialogResult result = dialog.ShowDialog();

    }


    private void startChat(bool isPrivate)
    {
      var users = new List<string>();

      foreach (ListViewItem item in imUsers.SelectedItems)
      {
        users.Add(item.Text);
      }
      foreach (var db in databases)
      {
        if (CheckIMVisible(db))
        {
          ArticleItem item = new ArticleItem();
          // we want the new article marked as read for the sender
          item._changed = false;
          item._toRead = false;
          // construct tags by the currently selected ones
          item._ownTags = CurrentTags();
          item._ownTags.Add("/chat");
          if (isPrivate) item._ownTags.Add("/private");
          item._title = string.Format("Chat: {0}", db.user);
          item._chatUsers = new List<string>();

          var addUser = new Action<string>(delegate(string user) { item._ownTags.Add("@" + user); item._chatUsers.Add(user); });
          foreach (var user in users)
          {
            addUser(user);
          }
          if (!users.Contains(db.user))
          {
            addUser(db.user);
          }
          item._chatId = -1;
          DialogArticle dialog = new DialogArticle(item, this, null, db, DialogArticle.Mode.Chat);
          dialog.Show();
          break;
        }
      }

    }

    private void startChatMenuItem_Click(object sender, EventArgs e)
    {
      startChat(true);
    }
    private void startPublicChatMenuItem_Click(object sender, EventArgs e)
    {
      startChat(false);
    }

    internal void OnDialogChatCreated(DialogArticle chat)
    {
      _chats.Add(chat);
    }

    internal void OnDialogChatDestroyed(DialogArticle chat)
    {
      _chats.Remove(chat);
    }

    private string getPreviewShortcut()
    {
      HtmlElement activeElement = webBrowser1.Document.ActiveElement;
      if (activeElement != null)
      {
        if (activeElement.TagName.Equals("a", StringComparison.OrdinalIgnoreCase))
        {
          string link = activeElement.GetAttribute("href");
          if (link != null) return link;
        }
      }
      return "";
    }

    private string getPreviewSelection()
    {
      IHTMLDocument2 htmlDocument = webBrowser1.Document.DomDocument as IHTMLDocument2;

      IHTMLSelectionObject currentSelection = htmlDocument.selection;
      if (currentSelection != null)
      {
        IHTMLTxtRange range = currentSelection.createRange() as IHTMLTxtRange;
        if (range != null && !String.IsNullOrEmpty(range.text))
        {
          return range.text;
        }
      }
      return "";
    }


    private void contextMenuPreview_Opening(object sender, CancelEventArgs e)
    {
      copyShortcutPreviewMenu.Visible = !String.IsNullOrEmpty(getPreviewShortcut());
      copyPreviewMenu.Visible = !String.IsNullOrEmpty(getPreviewSelection());
    }

    private void copyPreviewMenu_Click(object sender, EventArgs e)
    {
      Clipboard.SetText(getPreviewSelection());
    }

    private void copyShortcutPreviewMenu_Click(object sender, EventArgs e)
    {
      string url = getPreviewShortcut();
      if (String.IsNullOrEmpty(url)) return;


      List<string> tags;
      SQLDatabase database;
      int id;

      bool found = FindArticle(new Uri(url), out tags, out database, out id);
      if (found)
      {
        ArticleItem item;
        if (database.cache.TryGetValue(id, out item))
        {
          copyArticleShortcut(new ArticleItemWithDb(database,item));
          return;
        }
      }
      Clipboard.SetText(url);
    }

    private void replyPreviewMenu_Click(object sender, EventArgs e)
    {
      ArticleItemWithDb item = GetSelectedArticle();
      replyUsingSelectedText(item);
    
    }

    private void replyUsingSelectedText(ArticleItemWithDb itemDb)
    {
      string quote = null;
      ArticleItemWithDb? current = GetCurrentArticleWithDb();
      if (itemDb==current)
      {
        quote = getPreviewSelection();
      }

      if (!String.IsNullOrEmpty(quote))
      {
        if (!itemDb.IsNone() && string.IsNullOrEmpty(itemDb._item._draftLocal) && itemDb._item._chatId < 0)
        {
          replyWithQuote(itemDb, quote);
        }
      }
      else
      {
        quote = getRecentArticleContent(itemDb);
        replyWithQuote(itemDb, quote);
      }

    }

    private void appendNotePreviewMenuMenuItem_Click(object sender, EventArgs e)
    {
      ArticleId id;
      ArticleItem item;
      if (!GetCurrentArticle(out id, out item)) return;

      ArticleItemWithDb itemDb = new ArticleItemWithDb(id._database, item);

      if (UseExistingEditWindow(itemDb)) return;

      string quote = getPreviewSelection();

      // edit the article
      ArticleItem itemNew = new ArticleItem(itemDb._item); // create a copy
      DialogArticle dialog = new DialogArticle(itemNew, this, quote, id._database, DialogArticle.Mode.Note);
      dialog.Show();
    }


    private void notifyIcon_BalloonTipClicked(object sender, EventArgs e)
    {
      // reopen the last chat
      if (_waitingChatData.Count <= 0) return;
      var waiting = _waitingChatData[0];
      _waitingChatData.RemoveAt(0);
      if (waiting.db == null) return;
      RemoveChatNotifications(waiting.db,waiting.data.id);

      ArticleItem item = FindChatArticle(waiting.db, waiting.data.id);
      if (item != null)
      {
        ChatConversation chat = waiting.db.LoadChatConversation(item._chatId, false);
        FocusChat(waiting.db, waiting.data);
      }
    }

    public void RemoveChatNotifications(SQLDatabase db, int chatId)
    {
      // remove all items from the same conversation
      _waitingChatData.RemoveAll((w) => w.db == db && w.data.id == chatId);
    }

    private void notifyIcon_Click(object sender, EventArgs e)
    {
      // reopen the balloon
      if (_waitingChatData.Count <= 0)
      {
        return;
      }
      var waiting = _waitingChatData[0];

      // TODO: use cached version if available
      ChatConversation chat = waiting.db.LoadChatConversation(waiting.data.id, false);
      var part = chat.FindPart(waiting.data.revToRead);
      if (part == null)
      {
        if (!String.IsNullOrEmpty(notifyIcon.BalloonTipTitle))
        {
          notifyIcon.ShowBalloonTip(5000);
        }
        return;
      }

      notifyIcon.BalloonTipTitle = part.user_;
      notifyIcon.BalloonTipText = part.text_;
      notifyIcon.ShowBalloonTip(5000);
    }

    private void notifyIcon_DoubleClick(object sender, EventArgs e)
    {
      if (_waitingChatData.Count <= 0)
      {
        Activate();
      }
      else
      {
        notifyIcon_Click(sender, e);
      }
    }

    private void notifyIcon_MouseMove(object sender, MouseEventArgs e)
    {
    }

    private void imUsers_DragDrop(object sender, DragEventArgs e)
    {

    }

    private void imUsers_DragEnter(object sender, DragEventArgs e)
    {
      e.Effect = DragDropEffects.Copy;
    }

    private void imUsers_ItemDrag(object sender, ItemDragEventArgs e)
    {
      imUsers.DoDragDrop(imUsers.SelectedItems, DragDropEffects.Copy);
    }


  };

  [Flags]
  public enum AccessRights
  {
    None = 0,
    Read = 0x01,
    ToRead = 0x02,
    Edit = 0x04
  }

  /// cache item - all info about article stored in the database
  public class ArticleItem
  {
    // database fields
    public int _id;
    public int _parent;
    public string _title;
    private string _url;
    public string _author;
    public DateTime _date;
    public int _revision;
    public int _majorRevision;
    // tags attached to this article
    public List<string> _ownTags;
    // tags inherited from parents
    public List<string> _inheritedTags;
    // effective tags of this article
    public List<string> _effectiveTags;
    // list of children articles
    public List<int> _children;
    // article marked to read
    public bool _toRead;
    // article marked as changed
    public bool _changed;
    // article expanded in the tree view (children are visible)
    public bool _expanded;

    // file name of the local copy
    public string _draftLocal = null;

    public int _chatId = -1;

    public List<string> _chatUsers;

    // path to the working copy
    public string _workingCopy;

    // TODO: enable set the time of article using some properties
    // time the event starts
    public DateTime timeFrom { get {return _date;} }
    // time the event ends
    public DateTime timeTo { get {return _date + TimeSpan.FromHours(0.5);} }

    // Full URL of the article
    public string AbsoluteURL(SQLDatabase database)
    {
      if (string.IsNullOrEmpty(_url)) return "";
      return database.ToAbsolute(_url);
    }

    // Access to the raw URL (may be relative)
    public string RawURL
    {
      get { return _url; }
      set { _url = value; }
    }

    public ArticleItem()
    {
      _id = -1;
      _parent = -1;
      _title = "";
      _url = "";
      _author = "";
      _date = DateTime.MinValue;
      _revision = 0;
      _majorRevision = 0;
      _toRead = true;
      _changed = true;
      _expanded = true;
      _workingCopy = null;
    }
    public ArticleItem(ArticleItem src)
    {
      _id = src._id;
      _parent = src._parent;
      _title = src._title;
      _url = src._url;
      _author = src._author;
      _date = src._date;
      _revision = src._revision;
      _majorRevision = src._majorRevision;
      if (src._ownTags != null) _ownTags = new List<string>(src._ownTags);
      if (src._inheritedTags != null) _inheritedTags = new List<string>(src._inheritedTags);
      // do not copy effective tags, can be recalculated when needed
      if (src._children != null) _children = new List<int>(src._children);
      _toRead = src._toRead;
      _changed = src._changed;
      _expanded = src._expanded;
      _draftLocal = src._draftLocal;
      _chatId = src._chatId;
      if (src._chatUsers!=null) _chatUsers = new List<string>(src._chatUsers);
      _workingCopy = src._workingCopy;
    }

    // create a link to the article
    public string GetLink(SQLDatabase database, bool includeQuery)
    {
      string fullUrl = AbsoluteURL(database);
      string escapedUrl = fullUrl.Replace("(", "%28").Replace(")", "%29");
      if (!includeQuery) return escapedUrl;

      return string.Format(
        "{0}?server={1}&database={2}&id={3:D}",
        escapedUrl, Uri.EscapeDataString(database.server), Uri.EscapeDataString(database.database), _id);
    }

    // serialization
    public void Save(BinaryWriter writer, int version)
    {
      writer.Write(_id);
      writer.Write(_parent);
      writer.Write(_title);
      writer.Write(_url);
      writer.Write(_author);
      writer.Write(_date.ToBinary());
      writer.Write(_revision);
      writer.Write(_majorRevision);
      writer.Write(_toRead);
      writer.Write(_changed);
      writer.Write(_expanded);
      bool hasWorkingCopy = _workingCopy != null;
      writer.Write(hasWorkingCopy);
      if (hasWorkingCopy) writer.Write(_workingCopy);
  
      int n = 0;
      if (_ownTags != null) n = _ownTags.Count;
      writer.Write(n);
      for (int i = 0; i < n; i++) writer.Write(_ownTags[i]);
    }
    public void Load(BinaryReader reader, int version)
    {
      _id = reader.ReadInt32();
      _parent = reader.ReadInt32();
      _title = reader.ReadString();
      _url = reader.ReadString();
      _author = reader.ReadString();
      _date = DateTime.FromBinary(reader.ReadInt64());
      _revision = reader.ReadInt32();
      _majorRevision = reader.ReadInt32();
      _toRead = reader.ReadBoolean();
      _changed = reader.ReadBoolean();
      _expanded = reader.ReadBoolean();
      bool hasWorkingCopy = reader.ReadBoolean();
      if (hasWorkingCopy) _workingCopy = reader.ReadString();

      int n = reader.ReadInt32();
      if (n > 0)
      {
        _ownTags = new List<string>(n);
        for (int i=0; i<n; i++)
        {
          string tag = reader.ReadString();
          _ownTags.Add(tag);
        }
      }
    }

    public void AddChild(int id)
    {
      if (_children == null) _children = new List<int>();
      _children.Add(id);
    }
    public void AddTag(string t)
    {
      string tag = t.Trim();
      if (tag.Length > 0)
      {
        if (_ownTags == null) _ownTags = new List<string>();
        _ownTags.Add(tag.ToLower());
      }
    }

    // calculate what tags will be inherited by the children
    public List<string> InheritsTags()
    {
      // get tags inherited by the parent
      List<string> tags;
      if (_inheritedTags == null) tags = new List<string>();
      else tags = new List<string>(_inheritedTags);

      // apply own tags
      if (_ownTags != null)
        for (int i=0; i<_ownTags.Count; i++)
        {
          string tag = _ownTags[i];
          if (string.IsNullOrEmpty(tag)) continue;
          // local tags are not inherited
          if (tag[0] == TagSpecialChars.PrefixLocal) continue;
          // remove tag from the list
          if (tag[0] == TagSpecialChars.PrefixRemove)
          {
            if (tag.Length <= 1) continue; // avoid empty tag
            tag = tag.Substring(1); // remove the '-' modifier
            tags.Remove(tag);
            continue;
          }
          // attribute tags are local
          if (tag[0] == TagSpecialChars.TagAttribute) continue;
          if (tag.Length >= 2 && tag[0] == TagSpecialChars.TagPrivate && tag[1] == TagSpecialChars.TagAttribute) continue;

          if (!tags.Contains(tag)) tags.Add(tag);
        }

      if (tags.Count == 0) return null;
      return tags;
    }

    // calculate what effective tags the article has
    public void UpdateEffectiveTags()
    {
      // get tags inherited by the parent
      List<string> tags;
      if (_inheritedTags == null) tags = new List<string>();
      else tags = new List<string>(_inheritedTags);

      // apply own tags
      if (_ownTags != null)
        for (int i=0; i<_ownTags.Count; i++)
        {
          string tag = _ownTags[i];
          if (string.IsNullOrEmpty(tag)) continue;
          // local tags are handled like globals
          if (tag[0] == TagSpecialChars.PrefixLocal)
          {
            if (tag.Length <= 1) continue; // avoid empty tag
            tag = tag.Substring(1);
          }

          if (tag[0] == TagSpecialChars.PrefixRemove)
          {
            if (tag.Length <= 1) continue; // avoid empty tag
            tag = tag.Substring(1); // remove the '-' modifier
            tags.Remove(tag);
          }
          else
          {
            if (!tags.Contains(tag)) tags.Add(tag);
          }
        }

      if (tags.Count == 0) _effectiveTags = null;
      else _effectiveTags = tags;
    }

    // read the list of tags from the comma (or whitespace) separated text
    public static bool ReadTags(string text, out List<string> tags)
    {
      tags = new List<string>();

      // Tokenize tags into whitespace/commas, normal tags, quoted tags and invalid entries, and put them into the tag list.
      // Matches munch subsequent whitespace.
      // Notes:
      //  - necessary escaping: "" in @"literal strings", \\ in regexes, \\ and \- in [regex character classes]
      //  - IgnorePatternWhitespace doesn't inherit into [regex character classes]
      //  - see ColaboRuleSet.g for grammar details
      Regex regex = new Regex(@"(?<whitespace>   [ \t\r\n\f,]+)
                            |   (?<normal_tag>   [+\-:.]?[^ \t\r\n\f,""'\\;<>=&|()+\*?]+)([ \t\r\n\f,]+|$)
                            | ""(?<escaped_tag>  [+\-:.]?( \\[btnfr""'\\0] | \\x[0-9a-fA-F]{2} | \\u[0-9a-fA-F]{4} | [^\\""*?] )+)""([ \t\r\n\f,]+|$)
                            |   (?<invalid>      [^ \t\r\n\f,]+)", RegexOptions.IgnorePatternWhitespace);  // catch the significant part
      MatchCollection matches = regex.Matches(text);
      foreach (Match match in matches)
      {
        if (match.Groups["normal_tag"].Success)
          tags.Add(match.Groups["normal_tag"].Value.ToLower());
        else if (match.Groups["escaped_tag"].Success)
          tags.Add(Regex.Unescape(match.Groups["escaped_tag"].Value).ToLower());
        else if (match.Groups["invalid"].Success)
        {
          string message = string.Format("Syntax error in tag specification: '{0}'", match.Groups["invalid"].Value);
          MessageBox.Show(message, "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
          return false;
        }
      }
      return true;
    }
    // write the list of tags to the comma (or whitespace) separated text
    public static string WriteTags(List<string> list)
    {
      string tags = "";
      if (list != null)
        foreach (string tag in list)
        {
          if (tags.Length > 0) tags += ", ";
          tags += tag;
        }
      return tags;
    }

    // Collect actions of all articles in the subtree
    private void CollectSubtreeActions(ref List<string> actions, SQLDatabase database, List<Token> ruleSet)
    {
      if (_children == null) return;
      for (int i = 0; i < _children.Count; i++)
      {
        ArticleItem child;
        if (database.cache.TryGetValue(_children[i], out child))
        {
          VTCache cache = new VTCache(database, child);

          List<string> childActions = child.GetActions(ruleSet, database, cache);
          if (!childActions.Contains("hide"))
          {
            // merge
            foreach (string action in childActions)
            {
              if (!actions.Contains(action)) actions.Add(action);
            }
          }
          // recurse
          child.CollectSubtreeActions(ref actions, database, ruleSet);
        }
      }
    }
    // Collect actions of the item and the subtree if collapsed (ignore actions of hidden articles in subtree)
    public void CollectActions(out List<string> actions, out List<string> subtreeActions, SQLDatabase database, List<Token> ruleSet)
    {
      VTCache cache = new VTCache(database, this);
      actions = GetActions(ruleSet, database, cache);
      subtreeActions = new List<string>();
      if (!_expanded) CollectSubtreeActions(ref subtreeActions, database, ruleSet); // collect actions from children
    }

    // evaluate the access rights for the article
    public AccessRights GetAccessRights(SQLDatabase database, VTCache cache)
    {
      AccessRights rights = AccessRights.None;

      const string allowPrefix = "allow:";
      const string denyPrefix = "deny:";
      const string readRight = "read";
      const string toReadRight = "to_read";
      const string editRight = "edit";

      List<string> actions = GetActions(database.accessRights, database, cache);
      foreach (string action in actions)
      {
        if (action.StartsWith(allowPrefix))
        {
          // some right allowed
          string right = action.Substring(allowPrefix.Length);
          if (right == readRight) rights |= AccessRights.Read;
          else if (right == toReadRight) rights |= AccessRights.ToRead;
          else if (right == editRight) rights |= AccessRights.Edit;
        }
        else if (action.StartsWith(denyPrefix))
        {
          // some right denied
          string right = action.Substring(denyPrefix.Length);
          if (right == readRight) rights &= ~AccessRights.Read;
          else if (right == toReadRight) rights &= ~AccessRights.ToRead;
          else if (right == editRight) rights &= ~AccessRights.Edit;
        }
      }

      return rights;
    }

    // find the root of the item tree
    private ArticleItem GetRoot(SQLDatabase database)
    {
      ArticleItem item = this;
      while (item._parent > 0)
      {
        ArticleItem parent;
        if (!database.cache.TryGetValue(item._parent, out parent)) break;
        item = parent;
      }
      return item;
    }
    delegate bool CheckArticleDelegate(ArticleItem item);
    // evaluate the function for each ancestor, including itself
    private bool ForEachAncestor(CheckArticleDelegate func, SQLDatabase database)
    {
      // check itself
      if (func(this)) return true;

      // cycle through parents
      ArticleItem item = this;
      while (item._parent > 0)
      {
        ArticleItem parent;
        if (!database.cache.TryGetValue(item._parent, out parent)) break;
        if (func(parent)) return true;
        item = parent;
      }
      return false;
    }
    // evaluate the function for each descendant, including itself
    private bool ForEachDescendant(CheckArticleDelegate func, SQLDatabase database)
    {
      // check itself
      if (func(this)) return true;

      if (_children != null)
      {
        foreach (int index in _children)
        {
          ArticleItem child;
          if (database.cache.TryGetValue(index, out child))
          {
            // recurse
            if (child.ForEachDescendant(func, database)) return true;
          }
        }
      }
      return false;
    }

    public bool IsToday()
    {
      DateTime today = DateTime.Today;
      return _date.Year == today.Year && _date.DayOfYear == today.DayOfYear;
    }
    public bool IsLastWeek()
    {
      TimeSpan age = DateTime.Now.Subtract(_date);
      TimeSpan maxAge = TimeSpan.FromDays(7);
      return age.CompareTo(maxAge) < 0;
    }
    public bool IsLastMonth()
    {
      TimeSpan age = DateTime.Now.Subtract(_date);
      TimeSpan maxAge = TimeSpan.FromDays(31);
      return age.CompareTo(maxAge) < 0;
    }

    // check if article should be shown
    public bool IsEnabled(List<TagCondition> tags, List<Token> ruleSet, SQLDatabase database, VTCache cache)
    {
      List<string> actions = GetActions(ruleSet, database, cache);
      if (actions.Contains("hide")) return false; // article hidden by rule set
      return IsEnabled(tags, database, cache);
    }
    // check if article is not filtered by tag conditions
    public bool IsEnabled(List<TagCondition> tags, SQLDatabase database, VTCache cache)
    {
      foreach (TagCondition tag in tags)
      {
        if (tag._present)
        {
          if (!ContainsTag(tag._tag, database, cache)) return false;
        }
        else
        {
          if (ContainsTag(tag._tag, database, cache)) return false;
        }
      }
      return true;
    }
    // check if article contains given tag
    private bool ContainsTag(string tagName, SQLDatabase database, VTCache cache)
    {
      if (CheckEachTag(database, cache, delegate(string tag){ return tag == tagName; }))
        return true;
      // these advanced virtual tags are not matching masks to avoid recursion and slowdown
      if (tagName.StartsWith(FormColabo._ancestorPrefix))
      {
        string tag = tagName.Substring(FormColabo._ancestorPrefix.Length);
        return ForEachAncestor(delegate(ArticleItem item) { return item.ContainsTag(tag, database, cache); }, database);
      }
      if (tagName.StartsWith(FormColabo._descendantPrefix))
      {
        string tag = tagName.Substring(FormColabo._descendantPrefix.Length);
        return ForEachDescendant(delegate(ArticleItem item) { return item.ContainsTag(tag, database, cache); }, database);
      }
      if (tagName.StartsWith(FormColabo._threadPrefix))
      {
        string tag = tagName.Substring(FormColabo._threadPrefix.Length);
        return GetRoot(database).ForEachDescendant(delegate(ArticleItem item) { return item.ContainsTag(tag, database, cache); }, database);
      }
      return false;
    }
    // simple check if the article has a real tag
    public bool HasTag(string tag)
    {
      if (_effectiveTags != null && _effectiveTags.Contains(tag)) return true;
      return false;
    }
    // check if some tag is matching the mask
    private bool MatchTag(string mask, SQLDatabase database, VTCache cache)
    {
      // convert wildcard to regular expression
      Regex regex = new Regex("^" + Regex.Escape(mask).Replace("\\*", ".*").Replace("\\?", ".") + "$", RegexOptions.IgnoreCase);

      return CheckEachTag(database, cache, delegate(string tag)
      {
        return regex.IsMatch(tag);
      });
    }
    // check if some tag satisfying the condition is matching the mask
    private bool MatchTag(string mask, CompareStackItems cmp, string with, SQLDatabase database, VTCache cache)
    {
      // convert wildcard to regular expression
      Regex regex = new Regex("^" + Regex.Escape(mask).Replace("\\*", ".*").Replace("\\?", ".") + "$", RegexOptions.IgnoreCase);

      return CheckEachTag(database, cache, delegate(string tag)
      {
        return cmp(tag, with) && regex.IsMatch(tag);
      });
    }
    // call the delegate for each virtual and real tag
    private bool CheckEachTag(SQLDatabase database, VTCache cache, CheckTagDelegate func)
    {
      // virtual tags
      if (_toRead && func(FormColabo._tagToRead)) return true;
      if (_changed && func(FormColabo._tagChanged)) return true;
      if (cache._isToMe && func(FormColabo._tagToMe)) return true;
      if (cache._isByMe && func(FormColabo._tagByMe)) return true;
      if (func(cache._tagAuthor)) return true;
      if (func(cache._tagDatabase)) return true;
      if (cache._isDraft && func(FormColabo._tagDraft)) return true;
      if (cache._isChat && func(FormColabo._tagChat)) return true;
      if (cache._isHomepage && func(FormColabo._tagHomepage)) return true;
      if (cache._isToday && func(FormColabo._tagToday)) return true;
      if (cache._isLastWeek && func(FormColabo._tagLastWeek)) return true;
      if (cache._isLastMonth && func(FormColabo._tagLastMonth)) return true;

      // real tags
      if (_effectiveTags != null) foreach (string tag in _effectiveTags)
      {
        if (func(tag)) return true;
        // private tags matches global tag rules as well
        if (tag[0] == TagSpecialChars.TagPrivate && func(tag.Substring(1))) return true;
      }

      return false;
    }
    delegate bool CheckTagDelegate(string tag);

    // helper to speed up repeated virtual tag evaluation
    public class VTCache
    {
      public bool _isToMe;
      public bool _isByMe;
      public bool _isDraft;
      public bool _isChat;
      public bool _isHomepage;
      public bool _isToday;
      public bool _isLastWeek;
      public bool _isLastMonth;
      public string _tagAuthor;
      public string _tagDatabase;

      public VTCache(SQLDatabase database, ArticleItem item)
      {
        _isToMe = item.IsAssignedToMe(database);
        _isByMe = item._author.Equals(database.user, StringComparison.OrdinalIgnoreCase);
        _tagAuthor = string.Format(FormColabo._tagBy, item._author.ToLower());
        _tagDatabase = string.Format(FormColabo._tagDb, database.name.ToLower());
        _isDraft = !string.IsNullOrEmpty(item._draftLocal);
        _isChat = item._chatId>=0 || item._chatUsers!=null;
        _isHomepage = database.homepage > 0 && item._id == database.homepage;
        _isToday = item.IsToday();
        _isLastWeek = item.IsLastWeek();
        _isLastMonth = item.IsLastMonth();
      }
    }

    public bool IsAssignedToMe(SQLDatabase database)
    {
      if (_effectiveTags == null) return false;

      // assigned to me
      if (_effectiveTags.Contains("@" + database.user.ToLower())) return true;
      // assigned to a group I belong to
      foreach (string group in database.groups)
      {
        if (_effectiveTags.Contains("@" + group.ToLower())) return true;
      }
      return false;
    }

    // evaluate rule set on the current article
    public List<string> GetActions(List<Token> ruleSet, SQLDatabase database, VTCache cache)
    {
      List<string> actions = new List<string>();
      if (ruleSet == null) return actions;
      
      Stack<IStackItem> stack = new Stack<IStackItem>();
      foreach (Token token in ruleSet)
      {
        IStackItem arg1, arg2;
        switch (token.Type)
        {
          case ColaboRuleSetParser.MASK:
            stack.Push(new StackItemMask(token.Text));
            break;
          case ColaboRuleSetParser.IDENTIFIER:
            stack.Push(new StackItemTag(token.Text));
            break;
          case ColaboRuleSetParser.OR:
            stack.Push(new StackItemBool(stack.Pop().Bool(this, database, cache) || stack.Pop().Bool(this, database, cache)));
            break;
          case ColaboRuleSetParser.AND:
            stack.Push(new StackItemBool(stack.Pop().Bool(this, database, cache) && stack.Pop().Bool(this, database, cache)));
            break;
          case ColaboRuleSetParser.NOT:
            stack.Push(new StackItemBool(!stack.Pop().Bool(this, database, cache)));
            break;
          case ColaboRuleSetParser.LT:
            arg2 = stack.Pop(); arg1 = stack.Pop();
            stack.Push(new StackItemBool(arg1.Compare(this, delegate(string a, string b) { return string.Compare(a, b) < 0; }, arg2, database, cache)));
            break;
          case ColaboRuleSetParser.GT:
            arg2 = stack.Pop(); arg1 = stack.Pop();
            stack.Push(new StackItemBool(arg1.Compare(this, delegate(string a, string b) { return string.Compare(a, b) > 0; }, arg2, database, cache)));
            break;
          case ColaboRuleSetParser.LE:
            arg2 = stack.Pop(); arg1 = stack.Pop();
            stack.Push(new StackItemBool(arg1.Compare(this, delegate(string a, string b) { return string.Compare(a, b) <= 0; }, arg2, database, cache)));
            break;
          case ColaboRuleSetParser.GE:
            arg2 = stack.Pop(); arg1 = stack.Pop();
            stack.Push(new StackItemBool(arg1.Compare(this, delegate(string a, string b) { return string.Compare(a, b) >= 0; }, arg2, database, cache)));
            break;
          case ColaboRuleSetParser.EQ:
            arg2 = stack.Pop(); arg1 = stack.Pop();
            stack.Push(new StackItemBool(arg1.Compare(this, delegate(string a, string b) { return string.Compare(a, b) == 0; }, arg2, database, cache)));
            break;
          case ColaboRuleSetParser.NE:
            arg2 = stack.Pop(); arg1 = stack.Pop();
            stack.Push(new StackItemBool(arg1.Compare(this, delegate(string a, string b) { return string.Compare(a, b) != 0; }, arg2, database, cache)));
            break;
          case ColaboRuleSetParser.IMPL:
            arg2 = stack.Pop(); arg1 = stack.Pop();
            if (arg1.Bool(this, database, cache)) actions.Add(arg2.Text());
            break;
        }
      }

      return actions;
    }
    // evaluate condition on the current article
    public bool MatchExpression(List<Token> expression, SQLDatabase database, VTCache cache)
    {
      if (expression == null) return false;

      Stack<IStackItem> stack = new Stack<IStackItem>();
      foreach (Token token in expression)
      {
        IStackItem arg1, arg2;
        switch (token.Type)
        {
          case ColaboRuleSetParser.MASK:
            stack.Push(new StackItemMask(token.Text));
            break;
          case ColaboRuleSetParser.IDENTIFIER:
            stack.Push(new StackItemTag(token.Text));
            break;
          case ColaboRuleSetParser.OR:
            stack.Push(new StackItemBool(stack.Pop().Bool(this, database, cache) || stack.Pop().Bool(this, database, cache)));
            break;
          case ColaboRuleSetParser.AND:
            stack.Push(new StackItemBool(stack.Pop().Bool(this, database, cache) && stack.Pop().Bool(this, database, cache)));
            break;
          case ColaboRuleSetParser.NOT:
            stack.Push(new StackItemBool(!stack.Pop().Bool(this, database, cache)));
            break;
          case ColaboRuleSetParser.LT:
            arg2 = stack.Pop(); arg1 = stack.Pop();
            stack.Push(new StackItemBool(arg1.Compare(this, delegate(string a, string b) { return string.Compare(a, b) < 0; }, arg2, database, cache)));
            break;
          case ColaboRuleSetParser.GT:
            arg2 = stack.Pop(); arg1 = stack.Pop();
            stack.Push(new StackItemBool(arg1.Compare(this, delegate(string a, string b) { return string.Compare(a, b) > 0; }, arg2, database, cache)));
            break;
          case ColaboRuleSetParser.LE:
            arg2 = stack.Pop(); arg1 = stack.Pop();
            stack.Push(new StackItemBool(arg1.Compare(this, delegate(string a, string b) { return string.Compare(a, b) <= 0; }, arg2, database, cache)));
            break;
          case ColaboRuleSetParser.GE:
            arg2 = stack.Pop(); arg1 = stack.Pop();
            stack.Push(new StackItemBool(arg1.Compare(this, delegate(string a, string b) { return string.Compare(a, b) >= 0; }, arg2, database, cache)));
            break;
          case ColaboRuleSetParser.EQ:
            arg2 = stack.Pop(); arg1 = stack.Pop();
            stack.Push(new StackItemBool(arg1.Compare(this, delegate(string a, string b) { return string.Compare(a, b) == 0; }, arg2, database, cache)));
            break;
          case ColaboRuleSetParser.NE:
            arg2 = stack.Pop(); arg1 = stack.Pop();
            stack.Push(new StackItemBool(arg1.Compare(this, delegate(string a, string b) { return string.Compare(a, b) != 0; }, arg2, database, cache)));
            break;
        }
      }

      IStackItem result = stack.Pop();
      return result.Bool(this, database, cache);
    }
    // rule set evaluation helper classes
    private abstract class IStackItem
    {
      public abstract bool Bool(ArticleItem item, SQLDatabase database, VTCache cache);
      public abstract string Text();
      public abstract bool Compare(ArticleItem item, CompareStackItems cmp, IStackItem value, SQLDatabase database, VTCache cache);
    }
    private class StackItemBool : IStackItem
    {
      private bool _value;
      public StackItemBool(bool value) { _value = value; }
      public override bool Bool(ArticleItem item, SQLDatabase database, VTCache cache) { return _value; }
      public override string Text() { throw new ArgumentException(); }
      public override bool Compare(ArticleItem item, CompareStackItems cmp, IStackItem value, SQLDatabase database, VTCache cache) { throw new ArgumentException(); }
    }
    private class StackItemTag : IStackItem
    {
      private string _value;
      public StackItemTag(string value)
      {
        // if tag enclosed in "", remove them and unescape it
        if (value.Length >= 2 && value.IndexOf('"') == 0 && value.LastIndexOf('"') == value.Length - 1)
          _value = Regex.Unescape(value.Substring(1, value.Length - 2)).ToLower();
        else
          _value = value.ToLower(); 
      }
      public override bool Bool(ArticleItem item, SQLDatabase database, VTCache cache) { return item.ContainsTag(_value, database, cache); }
      public override string Text() { return _value; }
      public override bool Compare(ArticleItem item, CompareStackItems cmp, IStackItem value, SQLDatabase database, VTCache cache) { return cmp(Text(), value.Text()); }
    }
    private class StackItemMask : IStackItem
    {
      private string _value;
      public StackItemMask(string value)
      {
        // if mask enclosed in "", remove them and unescape it
        if (value.Length >= 2 && value.IndexOf('"') == 0 && value.LastIndexOf('"') == value.Length - 1)
          _value = Regex.Unescape(value.Substring(1, value.Length - 2)).ToLower();
        else
          _value = value.ToLower();
      }
      public override bool Bool(ArticleItem item, SQLDatabase database, VTCache cache) { return item.MatchTag(_value, database, cache); }
      public override string Text() { throw new ArgumentException(); }
      public override bool Compare(ArticleItem item, CompareStackItems cmp, IStackItem value, SQLDatabase database, VTCache cache)
      { return item.MatchTag(_value, cmp, value.Text(), database, cache); }
    }
    delegate bool CompareStackItems(string a, string b);

    // helper function to decorate the private tag with a special character
    static public string ToPrivateTag(string tag)
    {
      int pos = 0;
      // skip the prefix
      if (tag.Length > pos && tag[pos] == TagSpecialChars.PrefixLocal) pos++; // local scope
      if (tag.Length > pos && tag[pos] == TagSpecialChars.PrefixRemove) pos++; // tag removing
      // insert special character as the first character of the tag
      return tag.Substring(0, pos) + "." + tag.Substring(pos);
    }
    // helper function to recognize if tag is a private tag and remove the decoration
    static public string FromPrivateTag(string tag)
    {
      int pos = 0;
      // skip the prefix
      if (tag.Length > pos && tag[pos] == TagSpecialChars.PrefixLocal) pos++; // local scope
      if (tag.Length > pos && tag[pos] == TagSpecialChars.PrefixRemove) pos++; // tag removing
      // check the special character
      if (tag.Length > pos && tag[pos] == TagSpecialChars.TagPrivate)
        return tag.Substring(0, pos) + tag.Substring(pos + 1); // remove the special character
      // special character not present
      return null;
    }
  };

  public struct ArticleItemWithDb
  {
    public SQLDatabase _database;
    public ArticleItem _item;

    public ArticleItemWithDb(SQLDatabase database, ArticleItem item)
    {
      _database = database;
      _item = item;
    }
    public static ArticleItemWithDb None = new ArticleItemWithDb(null, null);
    public bool IsNone()
    {
      return _database == null && _item == null;
    }

    public override int GetHashCode()
    {
      return _database.GetHashCode() ^ _item.GetHashCode();
    }

    public override bool Equals(object o)
    {
      ArticleItemWithDb? itemDb = o as ArticleItemWithDb?;
      if (itemDb == null) return false;
      return this == itemDb.Value;
    }
    public static bool operator == (ArticleItemWithDb a, ArticleItemWithDb b)
    {
      return a._database.Equals(b._database) && a._item._id==b._item._id;
    }
    public static bool operator != (ArticleItemWithDb a, ArticleItemWithDb b)
    {
      return !(a == b);
    }
  };


  public class ArticlesCache : Dictionary<int, ArticleItem>
  {
    int nextId_ = -2;

    // serialization
    public void Save(BinaryWriter writer, int version)
    {
      // calculate the number of items
      int n = 0;
      foreach (ArticleItem item in Values)
      {
        // do not serialize drafts
        if (item._id >= 0) n++;
      }

      writer.Write(n);
      foreach (ArticleItem item in Values)
      {
        // do not serialize drafts
        if (item._id >= 0) item.Save(writer, version);
      }
    }
    public void Load(BinaryReader reader, int version)
    {
      this.Clear();

      int n = reader.ReadInt32();
      for (int i = 0; i < n; i++)
      {
        ArticleItem item = new ArticleItem();
        item.Load(reader, version);
        this.Add(item._id, item);
      }
    }
    
    public void UpdateChilds()
    {
      // Build lists of children
      foreach (ArticleItem item in this.Values)
      {
        ArticleItem parent;
        if (this.TryGetValue(item._parent, out parent)) parent.AddChild(item._id);
      }

      // Decide inherited tags
      foreach (ArticleItem item in this.Values)
      {
        if (item._parent < 0) AddInheritedTags(item, null);
      }
    }

    // Recursively decide inherited tags
    private void AddInheritedTags(ArticleItem item, List<string> tags)
    {
      // update list of inherited tags
      item._inheritedTags = tags;
      List<string> inheritedTags = item.InheritsTags();
      // apply inherited tags to the current article
      item.UpdateEffectiveTags();

      // recurse
      if (item._children != null) foreach (int i in item._children)
        {
          ArticleItem child;
          if (this.TryGetValue(i, out child)) AddInheritedTags(child, inheritedTags);
        }
    }

    internal int UniqueTempId()
    {
      return nextId_--;
    }
  }

  /// Unique identification of article in the client application
  public struct ArticleId
  {
    // database owner
    public SQLDatabase _database;
    // ID of the article
    public int _id;

    public ArticleId(SQLDatabase database, int id)
    {
      _database = database;
      _id = id;
    }
    
    public static bool operator ==(ArticleId id1, ArticleId id2)
    {
      return id1._database == id2._database && id1._id == id2._id;
    }
    public static bool operator !=(ArticleId id1, ArticleId id2)
    {
      return id1._database != id2._database || id1._id != id2._id;
    }
    public override bool Equals(Object obj)
    {
      if (obj == null || obj.GetType() != GetType()) return false;
      ArticleId to = (ArticleId)obj;
      return to._database == _database && to._id == _id;
    }
    public override int GetHashCode()
    {
      return _database.GetHashCode() ^ _id.GetHashCode();
    }

    public static ArticleId None = new ArticleId(null, -1);
  }

  /// index item - info about the article really visible in the articles list
  public class IndexItem
  {
    public ArticleId _articleId;
    // how far is the article from the root
    public int _level;
    // some children are visible
    public bool _hasChildren;
    // parent id (null for no parent)
    public IndexItem _parent;
    // my date or the latest date of all my children recursively
    public DateTime _date;
    // my sort order or the highest of all my children recursively
    public int _sortOrder;
    // only expanded items has its children shown
    public bool _expanded;
    // visible item, the source for DataGridView (added into _index)
    public bool _visible;

    public IndexItem(SQLDatabase database, int id, int level, IndexItem parent, bool expanded)
    {
      _articleId._database = database;
      _articleId._id = id;
      _level = level;
      _hasChildren = false;
      _parent = parent;
      _date = new DateTime(); //will be set later
      _sortOrder = 0; // will be set later
      _expanded = expanded;
      _visible = false;
    }
  }

  // Navigate history - visited article url alogn with the selected tags during browsing
  public class NavHistoryItem
  {
    public string url;
    public List<TagCondition> tags;

    public NavHistoryItem(string url, List<TagCondition> tags) 
    {
      this.url = url; 
      this.tags = new List<TagCondition>(tags);
    }
  }

  public struct TagCondition
  {
    public string _tag;
    public bool _present;

    public TagCondition(string tag, bool present) { _tag = tag; _present = present; }
  }

  public class RuleSet
  {
    public string _user;
    public string _name;
    public string _expression;
    // rule sets inheritance
    public string _parentUser;
    public string _parentName;

    public RuleSet()
    {
    }
    public RuleSet(RuleSet src)
    {
      _user = src._user;
      _name = src._name;
      _expression = src._expression;
      _parentUser = src._parentUser;
      _parentName = src._parentName;
    }
    public override string ToString()
    {
      return _user + ":" + _name;
    }
  }

  /// Bookmark properties
  public class Bookmark
  {
    // owner database
    public SQLDatabase _database;
    // unique id of the bookmark in the database
    public int _id;
    // user friendly name
    public string _name;
    // image index
    public int _image;
    // list of tags to select
    public List<string> _tags;
    // article to select
    public ArticleId _article;
    // parent bookmark in hierarchy
    public int _parent;

    // used for database items
    public Bookmark(SQLDatabase database)
    {
      _database = database;
      _id = -1;
      _image = 0;
      _article = ArticleId.None;
      _parent = -1;
    }
    // used for default items
    public Bookmark(string name, int image, string[] tags)
    {
      _database = null;
      _id = -1;
      _name = name;
      _image = image;
      if (tags != null)
        _tags = new List<string>(tags);
      else
        _tags = null;
      _article = ArticleId.None;
      _parent = -1;
    }

    // list of default bookmarks
    public static Bookmark[] DefaultBookmarks =
    {
      // name, image index, tags
      new Bookmark("Unread", 2, new string[] {FormColabo._tagToRead}),
      new Bookmark("My Tasks", 2, new string[] {FormColabo._tagToMe}),
      new Bookmark("Sent", 2, new string[] {FormColabo._tagByMe}),
      new Bookmark("Drafts", 2, new string[] {FormColabo._tagDraft}),
      new Bookmark("Chat", 2, new string[] {FormColabo._tagChat})
    };
  }

  public class Script : ICloneable
  {
    public string _user;
    public string _usage;
    public string _name;
    public string _image;
    public string _language;
    public string _script;

    public Script()
    {
    }
    public Object Clone()
    {
      Script clone = new Script();
      clone._user = (string)_user.Clone();
      clone._usage = (string)_usage.Clone();
      clone._name = (string)_name.Clone();
      clone._image = (string)_image.Clone();
      clone._language = (string)_language.Clone();
      clone._script = (string)_script.Clone();
      return clone;
    }
  }

  /// Interface for background task
  public abstract class ITask
  {
    /// part of task which can be processed asynchronously
    public abstract void Process(BackgroundWorker worker);
    /// part of task which need to be processed in the main thread
    public abstract void Finish();
    /// what type should be reported in the progress indication
    public abstract string ProgressType();
  }

  public class TaskSynchronize : ITask
  {
    private ArticleId _selectedID;
    private FormColabo _owner;
    private SQLDatabase _database;
    private Dictionary<SQLDatabase, DatabaseData> _data;

    private class DatabaseData
    {
      public ArticlesCache _cache;
      public List<RepositoryDirectories> _directories;

      public DatabaseData()
      {
        _cache = new ArticlesCache();
        _directories = new List<RepositoryDirectories>();
      }
    }

    public override string ProgressType() { return "Read"; }

    public TaskSynchronize(ArticleId selectedID, FormColabo owner, SQLDatabase database)
    {
      _selectedID = selectedID;
      _owner = owner;
      _database = database;
      _data = new Dictionary<SQLDatabase, DatabaseData>();
    }
    public override void Process(BackgroundWorker worker)
    {
      string message = "Synchronizing ...";
      worker.ReportProgress(0, message);

      if (_database == null)
      {
        // synchronize all databases
        foreach (SQLDatabase database in _owner.databases)
        {
          DatabaseData data = new DatabaseData();
          database.ReadDBContent(ref data._cache, worker);
          database.CollectDirectories(data._cache, ref data._directories);
          _data.Add(database, data);
        }
      }
      else
      {
        // synchronize only selected database
        DatabaseData data = new DatabaseData();
        _database.ReadDBContent(ref data._cache, worker);
        _database.CollectDirectories(data._cache, ref data._directories);
        _data.Add(_database, data);
      }

      worker.ReportProgress(100, message);
    }
    public override void Finish()
    {
      foreach (KeyValuePair<SQLDatabase, DatabaseData> item in _data)
      {
        item.Key.ReplaceCache(item.Value._cache, item.Value._directories);
        item.Key.SaveCache();

        // used only once to delete {COMA} and MARKDOWN headers
        // Tools.SwitchToComa(item.Value, _owner);

        // used only once to rebuild tags:
        // Tools.OptimizeTags(item.Value, item.Key, _owner);
      }

      if (!_owner.Disposing && _owner.IsHandleCreated)
      {
        _owner.UpdateArticles(_selectedID);
      }

      // setup regular synchronization
      _owner.UpdateSynchronizeTimer();
    }
  }


  public class TaskCheckUpdates : ITask
  {
    private FormColabo _owner;
    private SQLDatabase _database;
    private List<ArticleItem> _articles;
    private List<ArticleItem> _toFindWorkingCopy;
    private List<ArticleItem> _toUpdate;

    public override string ProgressType() { return "Update"; }

    public TaskCheckUpdates(FormColabo owner, SQLDatabase database, List<ArticleItem> articles)
    {
      _owner = owner;
      _database = database;
      _articles = articles;
      _toFindWorkingCopy = new List<ArticleItem>();
      _toUpdate = new List<ArticleItem>();
    }
    public override void Process(BackgroundWorker worker)
    {
      string message = "Checking updates ...";

      ArticlesStore svn = _database.GetArticlesStore();

      bool old = svn.SetSilent(true);
      int n = _articles.Count;
      for (int i=0; i<n; i++)
      {
        worker.ReportProgress(100 * i / n, message);

        ArticleItem item = _articles[i];
        if (item._id < 0) continue; // draft only
        ArticleItem.VTCache cache = new ArticleItem.VTCache(_database, item);
        if ((item.GetAccessRights(_database, cache) & AccessRights.Read) == 0) continue; // do not update unaccessible articles
        string itemURL = item.AbsoluteURL(_database);
        if (itemURL.StartsWith("news:")) continue; // not a SVN source 

        if (item._workingCopy == null) _toFindWorkingCopy.Add(item); // working copy directory not known yet
        else
        {
          // check if we know about the newer revision
          int localRevision = svn.GetLocalRevision(item._workingCopy);
          if (localRevision < item._revision) _toUpdate.Add(item);
        }
      }
      svn.SetSilent(old);

      worker.ReportProgress(100, message);
    }
    public override void Finish()
    {
      ArticlesStore svn = _database.GetArticlesStore();

      foreach (ArticleItem item in _toFindWorkingCopy)
      {
        ITask task = new TaskFindWorkingCopy(_owner, _database, svn, item);
        _owner.AddTask(task,2);
      }
      foreach (ArticleItem item in _toUpdate)
      {
        ITask task = new TaskUpdate(_database, svn, item, int.MaxValue);
        _owner.AddTask(task,1);
      }
    }
  }
  public class TaskFindWorkingCopy : ITask
  {
    private FormColabo _owner;
    private SQLDatabase _database;
    private ArticlesStore _svn;
    private ArticleItem _item;
    private int _revision;

    public override string ProgressType() { return "Working Copy"; }

    public TaskFindWorkingCopy(FormColabo owner, SQLDatabase database, ArticlesStore svn, ArticleItem item)
    {
      _owner = owner;
      _database = database;
      _svn = svn;
      _item = item;
      _revision = 0;
    }
    public override void Process(BackgroundWorker worker)
    {
      string message = "Searching working copy ...";
      worker.ReportProgress(0, message);

      bool old = _svn.SetSilent(true);
      _svn.UpdateWorkingCopy(_database, _item, false, int.MaxValue); // only find the working copy
      if (!string.IsNullOrEmpty(_item._workingCopy)) _revision = _svn.GetLocalRevision(_item._workingCopy);
      _svn.SetSilent(old);

      worker.ReportProgress(100, message);
    }
    public override void Finish()
    {
      if (_revision < _item._revision)
      {
        // update of article is needed
        ITask task = new TaskUpdate(_database, _svn, _item, int.MaxValue);
        _owner.AddTask(task,1);
      }
    }
  }
  public class TaskUpdate : ITask
  {
    private SQLDatabase _database;
    private ArticlesStore _svn;
    private ArticleItem _item;
    private int _changedRevision;

    public override string ProgressType() { return "Update"; }

    public TaskUpdate(SQLDatabase database, ArticlesStore svn, ArticleItem item, int changedRevision)
    {
      _database = database;
      _svn = svn;
      _item = item;
      _changedRevision = changedRevision;
    }
    public override void Process(BackgroundWorker worker)
    {
      string message = "Updating ...";
      worker.ReportProgress(0, message);

      bool old = _svn.SetSilent(true);
      _svn.UpdateWorkingCopy(_database, _item, true, _changedRevision); // update the article
      _svn.SetSilent(old);

      worker.ReportProgress(100, message);
    }
    public override void Finish()
    {
    }
  }

  public class RepositoryInfo
  {
    // root of the repository
    public Uri _root;
    // the directory where log operation need to be executed (can be a subdirectory of _root)
    public string _logRoot;
    public List<ArticleItem> _articles;

    public RepositoryInfo(Uri root)
    {
      _root = root;
      _logRoot = null;
      _articles = new List<ArticleItem>();
    }
    public void Add(SQLDatabase database, ArticleItem item)
    {
      _articles.Add(item);
      string itemURL = item.AbsoluteURL(database);
      // update _logRoot
      char[] delimiters = new char[] { '\\', '/' };
      if (_logRoot == null)
      {
        // set directory name to the directory
        int index = itemURL.LastIndexOfAny(delimiters);
        if (index >= 0) _logRoot = itemURL.Substring(0, index);
      }
      else
      {
        // find the equal prefix of item._url and _logRoot
        string path = itemURL;
        do 
        {
          int index = path.LastIndexOfAny(delimiters);
          if (index < 0) return; // invalid path
          path = path.Substring(0, index);
        } while (!_logRoot.StartsWith(path));
        _logRoot = path;
      }
    }
  }
  public class TaskFindRepositories : ITask
  {
    private FormColabo _owner;
    private SQLDatabase _database;
    private List<ArticleItem> _articles;
    private List<RepositoryInfo> _repositories;

    public override string ProgressType() { return "Update"; }

    public TaskFindRepositories(FormColabo owner, SQLDatabase database, List<ArticleItem> articles)
    {
      _owner = owner;
      _database = database;
      _articles = articles;
      _repositories = new List<RepositoryInfo>();
    }
    public override void Process(BackgroundWorker worker)
    {
      string message = "Collecting repositories ...";

      ArticlesStore svn = _database.GetArticlesStore();
      for (int i = 0; i < _articles.Count; i++)
      {
        worker.ReportProgress((int)(100.0 * i / _articles.Count), message);
        string itemURL = _articles[i].AbsoluteURL(_database);
        if (string.IsNullOrEmpty(itemURL)) continue;
        Uri url = new Uri(itemURL);
        // find in which repository this article is
        bool found = false;
        for (int j=0; j<_repositories.Count; j++)
        {
          if (_repositories[j]._root.IsBaseOf(url))
          {
            // we found the repository
            found = true;
            _repositories[j].Add(_database, _articles[i]);
            break;
          }
        }
        if (!found)
        {
          // decide the repository using SVN operation
          bool old = svn.SetSilent(true);
          Uri root = svn.GetRepositoryRoot(url);
          svn.SetSilent(old);
          if (root != null)
          {
            RepositoryInfo repository = new RepositoryInfo(root);
            repository.Add(_database, _articles[i]);
            _repositories.Add(repository);
          }
        }
      }
      worker.ReportProgress(100, message);
    }
    public override void Finish()
    {
      foreach (RepositoryInfo repository in _repositories)
      {
        ITask task = new TaskCollectRevisions(_owner, _database, repository);
        _owner.AddTask(task,2);
      }
    }
  }

  public class RevisionInfo
  {
    public int _revision;
    public string _author;
    public DateTime _time;
    public void Save(BinaryWriter writer, int version)
    {
      writer.Write(_revision);
      writer.Write(_author);
      writer.Write(_time.ToBinary());
    }
    public void Load(BinaryReader reader, int version)
    {
      _revision = reader.ReadInt32();
      _author = reader.ReadString();
      _time = DateTime.FromBinary(reader.ReadInt64());
    }
  }
  public class SVNLog
  {
    public int _lastRevision;
    public Dictionary<string, List<RevisionInfo>> _revisions;

    public SVNLog()
    {
      _lastRevision = 0;
      _revisions = new Dictionary<string, List<RevisionInfo>>();
    }
    public void Add(string url, RevisionInfo revision)
    {
      if (revision._revision > _lastRevision) _lastRevision = revision._revision;

      List<RevisionInfo> list;
      if (!_revisions.TryGetValue(url, out list))
      {
        list = new List<RevisionInfo>();
        _revisions.Add(url, list);
      }
      list.Add(revision);
    }
    public void Save(BinaryWriter writer, int version)
    {
      writer.Write(_lastRevision);
      writer.Write(_revisions.Count);
      foreach (KeyValuePair<string, List<RevisionInfo>> item in _revisions)
      {
        writer.Write(item.Key);
        int m = item.Value.Count;
        writer.Write(m);
        for (int j = 0; j < m; j++) item.Value[j].Save(writer, version);
      }
    }
    public void Load(BinaryReader reader, int version)
    {
      _lastRevision = reader.ReadInt32();
      int n = reader.ReadInt32();
      _revisions = new Dictionary<string, List<RevisionInfo>>(n);
      for (int i = 0; i < n; i++)
      {
        string url = reader.ReadString();
        List<RevisionInfo> list = new List<RevisionInfo>();
        int m = reader.ReadInt32();
        for (int j=0; j<m; j++)
        {
          RevisionInfo revision = new RevisionInfo();
          revision.Load(reader, version);
          list.Add(revision);
        }
        _revisions.Add(url, list);
      }
    }
  }
  public class ArticleRevisions
  {
    public ArticleItem _item;
    public List<RevisionInfo> _revisions;

    public ArticleRevisions(ArticleItem item, List<RevisionInfo> revisions)
    {
      _item = item;
      _revisions = revisions;
    }
  }
  public class TaskCollectRevisions : ITask
  {
    private FormColabo _owner;
    private SQLDatabase _database;
    private RepositoryInfo _repository;
    private List<ITask> _tasks;

    private const int CacheFileMagic = 'L' | ('O' << 8) | ('G' << 16);
    private const int CacheFileVersion = 1;

    public override string ProgressType() { return "Working Copy"; }

    public TaskCollectRevisions(FormColabo owner, SQLDatabase database, RepositoryInfo repository)
    {
      _owner = owner;
      _database = database;
      _repository = repository;
      _tasks = new List<ITask>();
    }
    public override void Process(BackgroundWorker worker)
    {
      string message = "Collecting revisions (load cache) ...";
      worker.ReportProgress(0, message);

      ArticlesStore svn = _database.GetArticlesStore();

      // Create the SVN log of the whole repository, use the cache
      string filename = null;
      string guid = svn.GetRepositoryGuid(_repository._root);
      if (guid != null) filename = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Colabo\\" + guid + ".cache";

      SVNLog log = new SVNLog();

      // read from the cache
      if (filename != null && File.Exists(filename))
      {
        Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
        BinaryReader reader = new BinaryReader(stream);
        int magic = reader.ReadInt32();
        int version = reader.ReadInt32();
        if (magic == CacheFileMagic && version <= CacheFileVersion)
        {
          try
          {
            log.Load(reader, version);
          }
          catch (System.Exception exception)
          {
            Console.Write(exception.ToString());
            // empty the log, create it from scratch
            log = new SVNLog();
          }
        }
        stream.Close();
      }

      message = "Collecting revisions (update cache) ...";
      worker.ReportProgress(0, message);

      // update the cache
      bool old = svn.SetSilent(true);
      svn.GetLog(ref log, new Uri(_repository._logRoot), worker);
      svn.SetSilent(old);

      message = "Collecting revisions (save cache) ...";
      worker.ReportProgress(0, message);

      // write to the cache
      if (filename != null)
      {
        Stream stream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None);
        BinaryWriter writer = new BinaryWriter(stream);
        writer.Write(CacheFileMagic);
        writer.Write(CacheFileVersion);
        log.Save(writer, CacheFileVersion);
        stream.Close();
      }

      message = "Collecting revisions (what to index) ...";
      worker.ReportProgress(0, message);

      // create tasks which will decide what article revisions need to be indexed
      string root = _repository._root.ToString();
      int rootLen = root.Length;
      // each task will check all revisions of up to maxToCheck articles
      List<ArticleRevisions> toCheck = new List<ArticleRevisions>();
      const int maxToCheck = 20; 
      for (int i=0; i<_repository._articles.Count; i++)
      {
        worker.ReportProgress((int)(100.0 * i / _repository._articles.Count));

        ArticleItem item = _repository._articles[i];
        string itemURL = item.AbsoluteURL(_database);

        if (!itemURL.StartsWith(root, true, null))
        {
          Console.Write("Wrong URL: " + itemURL);
          continue;
        }
        string relativePath = itemURL.Substring(rootLen); // skip the root

        // try to find article revisions in the log
        List<RevisionInfo> list;
        if (log._revisions.TryGetValue(relativePath, out list))
        {
          // create task from maxToCheck articles
          toCheck.Add(new ArticleRevisions(item, list));
          if (toCheck.Count >= maxToCheck)
          {
            _tasks.Add(new TaskRevisionsToIndex(_owner, _database, toCheck));
            toCheck = new List<ArticleRevisions>();
          }
        }
      }
      // create task from remaining articles
      if (toCheck.Count > 0)
        _tasks.Add(new TaskRevisionsToIndex(_owner, _database, toCheck));

      worker.ReportProgress(100, message);
    }
    public override void Finish()
    {
      foreach (ITask task in _tasks)
        _owner.AddTask(task,2);
    }
  }

  public class TaskRevisionsToIndex : ITask
  {
    private FormColabo _owner;
    private SQLDatabase _database;
    private List<ArticleRevisions> _toCheck;
    private List<ITask> _tasks;

    public override string ProgressType() { return "Index"; }

    public TaskRevisionsToIndex(FormColabo owner, SQLDatabase database, List<ArticleRevisions> toCheck)
    {
      _owner = owner;
      _database = database;
      _toCheck = toCheck;
      _tasks = new List<ITask>();
    }
    public override void Process(BackgroundWorker worker)
    {
      string message = "Revisions to index ...";
      worker.ReportProgress(0, message);

      ArticlesStore svn = _database.GetArticlesStore();

      // check all articles
      for (int j=0; j<_toCheck.Count; j++)
      {
        worker.ReportProgress((int)(100.0 * j / _toCheck.Count));
        ArticleRevisions list = _toCheck[j];
        // find revisions of article in the index
        List<int> inIndex = _database.FullTextRevisionsOfArticle(list._item);
        // check what revisions are not in the index
        for (int i = 0; i < list._revisions.Count; i++)
        {
          RevisionInfo revision = list._revisions[i];
          if (!inIndex.Contains(revision._revision))
            _tasks.Add(new TaskIndex(_owner, _database, list._item, revision));
        }
      }

      worker.ReportProgress(100, message);
    }
    public override void Finish()
    {
      foreach (ITask task in _tasks)
        _owner.AddTask(task,1);
    }
  }

  public class TaskIndex : ITask
  {
    private FormColabo _owner;
    private SQLDatabase _database;
    private ArticleItem _item;
    private RevisionInfo _revision;

    public override string ProgressType() { return "Index"; }

    public TaskIndex(FormColabo owner, SQLDatabase database, ArticleItem item, RevisionInfo revision)
    {
      _owner = owner;
      _database = database;
      _item = item;
      _revision = revision;
    }
    public override void Process(BackgroundWorker worker)
    {
      string message = "Indexing ...";
      worker.ReportProgress(0, message);

      ArticlesStore svn = _database.GetArticlesStore();
      if (_database.FullTextNeedToIndexArticle(_item, _revision))
      {
        worker.ReportProgress(20, message);
        // retrieve the content of article at given revision
        string content = svn.GetArticleRevision(_item.AbsoluteURL(_database), _revision._revision);

        // add even empty articles to avoid indexing them again
        worker.ReportProgress(80, message);
        // add the article revision to the index
        _database.FullTextIndexArticle(_item, _revision, content);
      }
      worker.ReportProgress(100, message);
    }
    public override void Finish()
    {
    }
  }

  public class SearchResult
  {
    public SQLDatabase _database;
    public int _id;
    public int _revision;
    public bool _headRevision;
    public string _author;
    public DateTime _time;
    public string _title;
    public string _content;
    public float _score;
  }

  public class TaskSearch : ITask
  {
    private DialogSearchResults _owner;
    private List<SQLDatabase> _databases;
    private string _query;
    List<SearchResult> _results;

    public override string ProgressType() { return "Search"; }

    public TaskSearch(DialogSearchResults owner, List<SQLDatabase> databases, string query)
    {
      _owner = owner;
      _databases = databases;
      _query = query;
      _results = new List<SearchResult>();
    }
    public override void Process(BackgroundWorker worker)
    {
      string message = "Searching ...";
      worker.ReportProgress(0, message);
      foreach (SQLDatabase database in _databases)
      {
        _results.AddRange(database.FullTextSearch(_query));
      }
      worker.ReportProgress(100, message);
    }
    public override void Finish()
    {
      // update the owner if still open
      if (!_owner.Disposing && _owner.IsHandleCreated) _owner.FinishSearch(_results);
    }
  }

  public class TaskDelete : ITask
  {
    private FormColabo _owner;
    private ArticleItemWithDb _item;

    public override string ProgressType() { return "Send"; }

    public TaskDelete(FormColabo owner, ArticleItemWithDb item)
    {
      _owner = owner;
      _item = item;
    }
    public override void Process(BackgroundWorker worker)
    {
      string message = "Deleting ...";
      worker.ReportProgress(0, message);

      if (_item._item._id >= 0)
      {
        ArticlesStore svn = _item._database.GetArticlesStore();

        // do not search (only) drafts in the svn
        svn.UpdateWorkingCopy(_item._database, _item._item, true, int.MaxValue); // update the article

        worker.ReportProgress(50, message);

        string path = _item._item._workingCopy;
        if (path.Length > 0)
        {
          message = string.Format("Do you also want to remove source '{0}' from the SVN?", _item._item.AbsoluteURL(_item._database));
          DialogResult result = MessageBox.Show(message, "Colabo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
          if (result.Equals(DialogResult.Yes))
          {
            svn.DeleteArticle(path, _item._item._title);
          }
        }
      }

      worker.ReportProgress(100, message);
    }
    public override void Finish()
    {
      if (!_owner.Disposing && _owner.IsHandleCreated) _owner.DeleteArticle(_item);
    }
  }

  public class TaskUpdateTags : ITask
  {
    private SQLDatabase _database;
    private ArticleItem _item;
    private bool _succeeded;

    public override string ProgressType() { return "Update"; }

    public TaskUpdateTags(SQLDatabase database, ArticleItem item)
    {
      _database = database;
      _item = item;
      _succeeded = false;
    }
    public override void Process(BackgroundWorker worker)
    {
      string message = "Updating tags ...";
      worker.ReportProgress(0, message);
      ArticlesStore svn = _database.GetArticlesStore();
      _succeeded = svn.UpdateTags(_database, ref _item, false);
      worker.ReportProgress(100, message);
    }
    public override void Finish()
    {
      if (_succeeded) _database.UpdateRevisionAndDate(_item);
    }
  }

  public class TaskArticleDiff : ITask
  {
    private ArticleItem _item;
    private SQLDatabase _database;
    private string _currentContent;
    private int _readRevision;
    private FormColabo _owner;

    // content with marked differences
    private string _result;
    // tags in read revision
    private List<string> _readTags;

    public override string ProgressType() { return "Read"; }

    public TaskArticleDiff(ArticleItem item, SQLDatabase database, string currentContent, int readRevision, FormColabo owner)
    {
      _item = item;
      _database = database;
      _currentContent = currentContent;
      _readRevision = readRevision;
      _owner = owner;

      _result = null;
      _readTags = null;
    }

    // info about the single word in the text
    struct WordInfo
    {
      public int _offset;
      public int _length;
      public bool _valid;
      public WordInfo(int offset, int length, bool valid)
      {
        _offset = offset; _length = length; _valid = valid;
      }
    }

    // find all words in the text
    private List<WordInfo> FindWords(string text)
    {
      List<WordInfo> words = new List<WordInfo>();
      int i = 0; // position in the text
      while (i < text.Length)
      {
        // i points to the end of the last word
        
        // skip white spaces
        while (Char.IsWhiteSpace(text[i]))
        {
          i++;
          if (i >= text.Length) return words;
        }

        // i points to the beginning of the new word
        int word = i;
        bool valid = false;

        // read the word
        while (!Char.IsWhiteSpace(text[i]))
        {
          if (Char.IsLetterOrDigit(text, i)) valid = true;
          i++;
          if (i >= text.Length) break;
        }

        // add the word to the list
        if (i > word) words.Add(new WordInfo(word, i - word, valid));
      }

      return words;
    }

    // calculate hash codes of words
    int[] CalcCodes(string text, List<WordInfo> words)
    {
      int[] codes = new int[words.Count];
      for (int i=0; i<words.Count; i++)
      {
        if (words[i]._valid) codes[i] = text.Substring(words[i]._offset, words[i]._length).GetHashCode();
        else codes[i] = 0;
      }
      return codes;
    }

    public override void Process(BackgroundWorker worker)
    {
      string message = "Reading article ...";
      
      // read the read revision
      worker.ReportProgress(0, message);
      ArticlesStore svn = _database.GetArticlesStore();
      string itemURL = _item.AbsoluteURL(_database);
      string readContent = svn.GetArticleRevision(itemURL, _readRevision);
      if (readContent == null) return;
      _readTags = svn.GetPropertiesRevision(itemURL, _readRevision);
      
      // create the list of words and theirs hash codes for DiffInt
      List<WordInfo> wordsOld = FindWords(readContent);
      List<WordInfo> wordsNew = FindWords(_currentContent);
      int[] codesOld = CalcCodes(readContent, wordsOld);
      int[] codesNew = CalcCodes(_currentContent, wordsNew);
      
      // compare the revisions
      worker.ReportProgress(50);
      my.utils.Diff.Item[] diffs = my.utils.Diff.DiffInt(codesOld, codesNew);

      // create the output
      StringBuilder output = new StringBuilder(_currentContent.Length);
      int pos = 0; // read characters from _currentContent
      for (int n = 0; n < diffs.Length; n++)
      {
        my.utils.Diff.Item it = diffs[n];

        // trim invalid words
        while (it.deletedA > 0 && !wordsOld[it.StartA]._valid)
        {
          it.StartA++; it.deletedA--;
        }
        while (it.deletedA > 0 && !wordsOld[it.StartA + it.deletedA - 1]._valid)
        {
          it.deletedA--;
        }
        while (it.insertedB > 0 && !wordsNew[it.StartB]._valid)
        {
          it.StartB++; it.insertedB--;
        }
        while (it.insertedB > 0 && !wordsNew[it.StartB + it.insertedB - 1]._valid)
        {
          it.insertedB--;
        }

        // write unchanged characters
        {
          int startChar;
          if (it.StartB < wordsNew.Count) startChar = wordsNew[it.StartB]._offset;
          else startChar = _currentContent.Length; // it.StartB points behind all text
          if (startChar > pos)
          {
            output.Append(_currentContent.Substring(pos, startChar - pos));
            pos = startChar;
          }
        }

        // write deleted characters
        if (it.deletedA > 0)
        {
          int startChar = wordsOld[it.StartA]._offset;
          int endChar = wordsOld[it.StartA + it.deletedA - 1]._offset + wordsOld[it.StartA + it.deletedA - 1]._length;
          if (endChar > startChar)
          {
            output.Append('\x04');
            output.Append(readContent.Substring(startChar, endChar - startChar));
            output.Append("\x05 ");  // TODO (after fixing hacks in Coma): remove space
          }
        }

        // write inserted characters
        if (it.insertedB > 0)
        {
          int endChar = wordsNew[it.StartB + it.insertedB - 1]._offset + wordsNew[it.StartB + it.insertedB - 1]._length;
          if (endChar > pos)
          {
            output.Append('\x02');
            output.Append(_currentContent.Substring(pos, endChar - pos));
            output.Append("\x03 ");  // TODO (after fixing hacks in Coma): remove space
            pos = endChar;
          }
        }
      }

      // write rest of unchanged chars
      if (pos < _currentContent.Length)
      {
        output.Append(_currentContent.Substring(pos));
      }
      _result = output.ToString();

      worker.ReportProgress(100);
    }
    public override void Finish()
    {
      if (!_owner.Disposing && _owner.IsHandleCreated) 
      {
        if (_result != null) _owner.RefreshArticle(_item, _database, _result, _readTags);
        else _owner.RefreshArticle(_item, _database, _currentContent, _item._ownTags); // remove the progress message
      }
    }
  }

  public class TaskMarkAsRead : ITask
  {
    private SQLDatabase _database;
    private List<ArticleItem> _items;

    public override string ProgressType() { return "Send"; }

    public TaskMarkAsRead(SQLDatabase database, List<ArticleItem> items)
    {
      _database = database;
      _items = items;
    }
    public override void Process(BackgroundWorker worker)
    {
      string message = "Marking as read ...";
      worker.ReportProgress(0, message);
      for (int i=0; i<_items.Count; i++)
      {
        worker.ReportProgress(100 * i / _items.Count);

        ArticleItem item = _items[i];
        if (!string.IsNullOrEmpty(item._draftLocal)) _database.SaveArticleHeader(item);
        if (item._id >= 0) _database.SaveArticleRead(item._id, item._revision);
      }
      // TODO: optimize - _database.SaveArticlesRead(_items, worker);
      worker.ReportProgress(100, message);
    }
    public override void Finish()
    {
    }
  }
  public class TaskMarkAsUnread : ITask
  {
    private SQLDatabase _database;
    private List<ArticleItem> _items;

    public override string ProgressType() { return "Send"; }

    public TaskMarkAsUnread(SQLDatabase database, List<ArticleItem> items)
    {
      _database = database;
      _items = items;
    }
    public override void Process(BackgroundWorker worker)
    {
      string message = "Marking as unread ...";
      worker.ReportProgress(0, message);
      for (int i = 0; i < _items.Count; i++)
      {
        worker.ReportProgress(100 * i / _items.Count);

        ArticleItem item = _items[i];
        if (!string.IsNullOrEmpty(item._draftLocal)) _database.SaveArticleHeader(item);
        if (item._id >= 0) _database.SaveArticleUnread(item._id);
      }
      // TODO: optimize - _database.SaveArticlesRead(_items, worker);
      worker.ReportProgress(100, message);
    }
    public override void Finish()
    {
    }
  }

  // helper task used to delay execution after other tasks
  public class TaskOpenBookmark : ITask
  {
    private FormColabo _owner;
    private Bookmark _bookmark;

    public override string ProgressType() { return "Bookmark"; }

    public TaskOpenBookmark(FormColabo owner, Bookmark bookmark)
    {
      _owner = owner;
      _bookmark = bookmark;
    }
    public override void Process(BackgroundWorker worker)
    {
    }
    public override void Finish()
    {
      if (!_owner.Disposing && _owner.IsHandleCreated) _owner.OpenBookmark(_bookmark);
    }
  }

  public class TaskSVNBenchmark : ITask
  {
    private string _result;
    private List<SQLDatabase> _databases;

    public override string ProgressType() { return "Measure"; }

    public TaskSVNBenchmark(List<SQLDatabase> databases)
    {
      _databases = databases;
    }
    public override void Process(BackgroundWorker worker)
    {
      string message = "SVN Benchmark ...";
      worker.ReportProgress(0, message);
      ArticlesStore svn = DBEngine.GetDefaultArticlesStore();
      _result = svn.SVNBenchmark();
      worker.ReportProgress(100, message);
    }
    public override void Finish()
    {
      if (!string.IsNullOrEmpty(_result))
      {
        foreach (SQLDatabase database in _databases)
        {
          _result += database.Benchmark();
        }

        MessageBox.Show(_result, "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
    }
  }

  // global (independent) script info
  public class ScriptInfo
  {
    // database the script was load from
    public SQLDatabase _database;
    // script content
    public Script _script;

    public ScriptInfo(SQLDatabase database, Script script)
    {
      _database = database;
      _script = script;
    }
    public override string ToString()
    {
      return _script._user + "@" + _database.name + ":" + _script._name;
    }
  };
  // script plugged into UI
  public class UIScriptInfo : ScriptInfo
  {
    // UI the script is plugged in
    public ToolStripItem _item;

    public UIScriptInfo(SQLDatabase database, Script script)
      : base(database, script)
    {
    }
  }

  public class TagSpecialChars
  {
    // special tags begins with these chars
    public static char TagPrivate = '.';
    public static char TagVirtual = ':';
    public static char TagAttribute = '%';

    // these characters are not part of tags, are used to control tag assigning
    public static char PrefixLocal = '#';
    public static char PrefixRemove = '-';

    // conventions: '/', '@'
  }

  /// helper to show hourglass cursor during the long synchronous operations
  public class WaitCursor : IDisposable
  {
    private Cursor _oldCursor;

    public WaitCursor()
    {
      _oldCursor = Cursor.Current;
      Cursor.Current = Cursors.WaitCursor;
    }
    ~WaitCursor()
    {
      Dispose();
    }
    // IDisposable implementation
    public void Dispose()
    {
      Cursor.Current = _oldCursor;
    }
  };
}
