namespace Colabo
{
  partial class DialogGoTo
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogGoTo));
      this.value = new System.Windows.Forms.TextBox();
      this.buttonCancel = new System.Windows.Forms.Button();
      this.buttonGoTo = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // value
      // 
      this.value.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.value.Location = new System.Drawing.Point(13, 13);
      this.value.Name = "value";
      this.value.Size = new System.Drawing.Size(492, 20);
      this.value.TabIndex = 0;
      // 
      // buttonCancel
      // 
      this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.buttonCancel.Location = new System.Drawing.Point(430, 39);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.Size = new System.Drawing.Size(75, 23);
      this.buttonCancel.TabIndex = 1;
      this.buttonCancel.Text = "Cancel";
      this.buttonCancel.UseVisualStyleBackColor = true;
      // 
      // buttonGoTo
      // 
      this.buttonGoTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.buttonGoTo.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.buttonGoTo.Location = new System.Drawing.Point(349, 39);
      this.buttonGoTo.Name = "buttonGoTo";
      this.buttonGoTo.Size = new System.Drawing.Size(75, 23);
      this.buttonGoTo.TabIndex = 2;
      this.buttonGoTo.Text = "Go To";
      this.buttonGoTo.UseVisualStyleBackColor = true;
      // 
      // DialogGoTo
      // 
      this.AcceptButton = this.buttonGoTo;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.buttonCancel;
      this.ClientSize = new System.Drawing.Size(517, 68);
      this.Controls.Add(this.buttonGoTo);
      this.Controls.Add(this.buttonCancel);
      this.Controls.Add(this.value);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximumSize = new System.Drawing.Size(1000, 95);
      this.MinimumSize = new System.Drawing.Size(200, 95);
      this.Name = "DialogGoTo";
      this.Text = "Go To";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox value;
    private System.Windows.Forms.Button buttonCancel;
    private System.Windows.Forms.Button buttonGoTo;
  }
}