using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Colabo
{
  public partial class DialogFind : Form
  {
    public string FindText
    {
      get { return textBoxFind.Text; }
      set { textBoxFind.Text = value; }
    }
    public bool WholeWords
    {
      get { return checkBoxWholeWords.Checked; }
      set { checkBoxWholeWords.Checked = value; }
    }
    public bool MatchCase
    {
      get { return checkBoxMatchCase.Checked; }
      set { checkBoxMatchCase.Checked = value; }
    }
    public bool DirectionUp
    {
      get { return radioButtonUp.Checked; }
      set { if (value) radioButtonUp.Checked = true; else radioButtonDown.Checked = true; }
    }

    public DialogFind()
    {
      InitializeComponent();
    }

    private void buttonFindNext_Click(object sender, EventArgs e)
    {
      DialogResult = DialogResult.OK;
      Hide();
    }

    private void buttonCancel_Click(object sender, EventArgs e)
    {
      DialogResult = DialogResult.Cancel;
      Hide();
    }

    private void DialogFind_Activated(object sender, EventArgs e)
    {
      textBoxFind.Focus();
    }
  }
}