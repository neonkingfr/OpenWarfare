using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Colabo
{
  public partial class DialogGoTo : Form
  {
    public DialogGoTo()
    {
      InitializeComponent();
    }
    public string Value
    {
      get
      {
        return value.Text;
      }
    }
  }
}