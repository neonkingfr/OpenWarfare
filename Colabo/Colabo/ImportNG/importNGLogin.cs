using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace NNTPReader
{
  public partial class Login : Form
  {
    public Login()
    {
      InitializeComponent();
    }

    private void buttonOK_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    public String user
    {
      get { return textBox1.Text; }
      set { textBox1.Text = value; }
    }
    public String passwd
    {
      get { return textBox2.Text; }
      set { textBox2.Text = value; }
    }

    private void textBox1_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
        textBox2.Focus();
    }

    private void textBox2_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
        buttonOK.Focus();
    }
  }
}
