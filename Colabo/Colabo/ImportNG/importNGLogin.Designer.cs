namespace NNTPReader
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
          this.label1 = new System.Windows.Forms.Label();
          this.label2 = new System.Windows.Forms.Label();
          this.textBox1 = new System.Windows.Forms.TextBox();
          this.textBox2 = new System.Windows.Forms.TextBox();
          this.buttonOK = new System.Windows.Forms.Button();
          this.SuspendLayout();
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.Location = new System.Drawing.Point(12, 19);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(36, 13);
          this.label1.TabIndex = 0;
          this.label1.Text = "Login:";
          // 
          // label2
          // 
          this.label2.AutoSize = true;
          this.label2.Location = new System.Drawing.Point(12, 43);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(56, 13);
          this.label2.TabIndex = 1;
          this.label2.Text = "Password:";
          // 
          // textBox1
          // 
          this.textBox1.Location = new System.Drawing.Point(73, 16);
          this.textBox1.Name = "textBox1";
          this.textBox1.Size = new System.Drawing.Size(184, 20);
          this.textBox1.TabIndex = 2;
          this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
          // 
          // textBox2
          // 
          this.textBox2.Location = new System.Drawing.Point(73, 40);
          this.textBox2.Name = "textBox2";
          this.textBox2.PasswordChar = '*';
          this.textBox2.Size = new System.Drawing.Size(184, 20);
          this.textBox2.TabIndex = 3;
          this.textBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox2_KeyDown);
          // 
          // buttonOK
          // 
          this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
          this.buttonOK.Location = new System.Drawing.Point(198, 66);
          this.buttonOK.Name = "buttonOK";
          this.buttonOK.Size = new System.Drawing.Size(59, 25);
          this.buttonOK.TabIndex = 4;
          this.buttonOK.Text = "OK";
          this.buttonOK.UseVisualStyleBackColor = true;
          this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
          // 
          // Login
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(269, 99);
          this.Controls.Add(this.buttonOK);
          this.Controls.Add(this.textBox2);
          this.Controls.Add(this.textBox1);
          this.Controls.Add(this.label2);
          this.Controls.Add(this.label1);
          this.Name = "Login";
          this.Text = "Login";
          this.ResumeLayout(false);
          this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button buttonOK;
    }
}