using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;

namespace NNTPReader
{
  public partial class Form1 : Form
  {
    // Used for receiving info
    byte[] downBuffer = new byte[2048];
    // Used for sending commands
    byte[] byteSendInfo = new byte[2048];
    // Used for connecting a socket to the NNTP server
    TcpClient tcpClient;
    // Used for sending and receiving information
    NetworkStream strRemote;
    // Stores various responses
    string Response;
    // Number of bytes in the buffer
    int bytesSize;
    // Stores the ID of the first message in a newsgroup
    int firstID;
    // Stores the ID of the last message in a newsgroup
    int lastID;
    // Stores chunks of the articles from the buffer
    string NewChunk;
    // group for which Get News was clicked (such as "bistudio.engine.bugs". Is to be converted to colabo tags)
    public string selectedGroup;

    // path to folder where to save articles imported from NG
    public StringBuilder _workingCopyRoot;

    // Thread for converting to SQL
    // Thread saveThread;
#if SAVE_NEWS_TO_SQL_USING_THREAD
    SaveNews2SqlThread saveNews2SqlThread;
#endif

    // Colabo form to make NG Article COMMIT to Colabo SVN and database possible
    public static Colabo.FormColabo _colabo;

    private Colabo.SQLDatabase _database;

    public Form1(Colabo.FormColabo colabo, Colabo.SQLDatabase database)
    {
      _colabo = colabo;
      _database = database;
      // find the working copy root for the svnrepository
      _workingCopyRoot = new StringBuilder();
      /*
            // TaskGetWorkingPathRoot will do
            string workingRoot = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            if (workingRoot.Length != 0)
            {
              // ID of the repository will be used for the working copy directory 
              string guid = _database.GetArticlesStore().GetRepositoryGuid(_database.svnRepository);
              _workingCopyRoot = workingRoot + "\\Colabo\\" + guid + "\\";
            }
      */
      Colabo.ITask task = new Colabo.TaskGetWorkingPathRoot(_database.svnRepository, _workingCopyRoot, _database.GetArticlesStore());
      _colabo.AddTask(task);

      selectedGroup = "";

      InitializeComponent();

      // Test
      //string mytext = @"hello Buddy ,news:hjufed$fmu$1@new-server.localdomain, bla bla blanews:hjufed$fmu$1@new-server.localdomain haha news:hjufed$fmu$1@new-server.localdomain, ?news:hjufed$fmu$1@new-server.localdomain? atd. news:hjufed$fmu$1@new-server.localdomain the end";
      //string line = FixNewsURL(mytext);
      //System.Diagnostics.Debug.WriteLine(line);
    }

    private void btnGo_Click(object sender, EventArgs e)
    {
      // Get the Autentification login and password
      NNTPReader.Login loginForm = new NNTPReader.Login();
      DialogResult result = loginForm.ShowDialog(this);
      if (result != DialogResult.OK)
      {
        return;
      }

      // Open the socket to the server
      tcpClient = new System.Net.Sockets.TcpClient(txtNNTPServer.Text, 119);
      strRemote = tcpClient.GetStream();
      // Read the bytes
      bytesSize = strRemote.Read(downBuffer, 0, 2048);
      // Retrieve the response
      Response = System.Text.Encoding.GetEncoding(1250).GetString(downBuffer, 0, bytesSize);
      // Just as in HTTP, if code 200 is not returned, something's not right
      if (Response.Substring(0, 3) != "200")
      {
        MessageBox.Show("The server returned an unexpected response.", "Connection failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      // Show the response
      txtLog.Text = Response + "\n";

      byteSendInfo = StringToByteArr("AUTHINFO USER "+ loginForm.user + "\r\n");
      strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
      bytesSize = strRemote.Read(downBuffer, 0, 2048);
      Response = System.Text.Encoding.GetEncoding(1250).GetString(downBuffer, 0, bytesSize); //381 PASS required
      if (Response.Substring(0, 3) != "381")
      {
          MessageBox.Show("The server returned an unexpected response.\r\n" + Response, "Connection failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
          return;
      }

      byteSendInfo = StringToByteArr("AUTHINFO PASS "+ loginForm.passwd +"\r\n");
      //String ahoj = String.Format("AUTHINFO PASS {0}\r\n", loginForm.passwd);
      strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
      bytesSize = strRemote.Read(downBuffer, 0, 2048);
      Response = System.Text.Encoding.GetEncoding(1250).GetString(downBuffer, 0, bytesSize); //281 Ok
      if (Response.Substring(0, 3) != "281")
      {
          MessageBox.Show("The server returned an unexpected response.\r\n" + Response, "Connection failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
          return;
      }

      // Make the request to list all newsgroups
      byteSendInfo = StringToByteArr("LIST\r\n");
      strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
      Response = "";

      // Loop to retrieve a list of newsgroups
      while ((bytesSize = strRemote.Read(downBuffer, 0, downBuffer.Length)) > 0)
      {
        // Get the chunk of string
        NewChunk = Encoding.GetEncoding(1250).GetString(downBuffer, 0, bytesSize);
        Response += NewChunk;
        // If the string ends in a "\r\n.\r\n" then the list is over
        if (NewChunk.Substring(NewChunk.Length - 5, 5) == "\r\n.\r\n")
        {
          // Remove the "\r\n.\r\n" from the end of the string
          Response = Response.Substring(0, Response.Length - 3);
          break;
        }
      }
      // Split lines into an array
      string[] ListLines = Response.Split('\n');
      // Loop line by line
      foreach (String ListLine in ListLines)
      {
        // If the response starts with 215, it's the line that indicates the status
        if (ListLine.Length > 3 && ListLine.Substring(0, 3) == "215")
        {
          // Add the status response line to the log window
          txtLog.Text += ListLine + "\r\n";
        }
        else
        {
          // Add the newsgroup to the combobox
          string[] Newsgroup = ListLine.Split(' ');
          cmbNewsgroups.Items.Add(Newsgroup[0]);
        }
      }
    }

    public static byte[] StringToByteArr(string str)
    {
      System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
      return encoding.GetBytes(str);
    }

    private void btnGetNews_Click(object sender, EventArgs e)
    {
      // If a newsgroup is selected in the ComboBox
      if (cmbNewsgroups.SelectedIndex != -1)
      {
        // Request a certain newsgroup
        byteSendInfo = StringToByteArr("GROUP " + cmbNewsgroups.SelectedItem.ToString() + "\r\n");
        selectedGroup = cmbNewsgroups.SelectedItem.ToString();
        strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
        Response = "";
        bytesSize = strRemote.Read(downBuffer, 0, 2048);
        Response = System.Text.Encoding.GetEncoding(1250).GetString(downBuffer, 0, bytesSize);
        // Split the information about the newsgroup by blank spaces
        string[] Group = System.Text.Encoding.GetEncoding(1250).GetString(downBuffer, 0, bytesSize).Split(' ');
        // Show information about the newsgroup in the txtLog TextBox
        Response += Group[1] + " messages in the group (messages " + Group[2] + " through " + Group[3] + ")\r\n";
        txtLog.Text += Response;
        Response = "";
        // The ID of the first article in this newsgroup
        firstID = Convert.ToInt32(Group[2]);
        // The ID of the last article in this newsgroup
        lastID = Convert.ToInt32(Group[3]);
        // Messages are chosen, enable button to savenews
        saveNews.Enabled = true;
      }
      else
      {
        MessageBox.Show("Please connect to a server and select a newsgroup from the dropdown list first.", "Newsgroup retrieval", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void btnNext_Click(object sender, EventArgs e)
    {
      NextArticle();
    }

    public class HeaderInfo
    {
      //messageID, references, from, newsgroups, subject, dateSQL
      public string messageID;
      public List<string> references;
      public string from;
      public string newsgroups;
      public string subject;
      public string dateSQL;
      public string title;
      public string icon;
      public string xwiki;
      public List<string> tags;
      public HeaderInfo(string messageID, List<string> references, string from, string newsgroups, string subject, string dateSQL, string icon, string xwiki)
      {
        this.messageID = messageID;
        this.references = references;
        this.from = from;
        this.newsgroups = newsgroups;
        this.subject = subject;
        this.dateSQL = dateSQL;
        this.icon = icon;
        this.xwiki = xwiki;
        tags = null;
        title = null;
      }
      public void ExtractTagsAndTitle()
      {
        int ix = subject.IndexOf("#$");
        if (ix>=0)
        {
          title = subject.Substring(0, ix);
          string tagstr = subject.Substring(ix + 2);
          string[] properties = tagstr.Split(',');
          tags = new List<string>();
          for (int i = 0; i < properties.Length; i++)
          {
            string[] property = properties[i].Split(':');
            if (property.Length == 2)
            {
              switch (property[0].ToLower())
              {
                case "assingedto": //mistype
                case "assignedto":
                  tags.Add("@" + property[1]);
                  break;
                case "close": // done, closed, close:*
                  {
                    tags.Add("done");
                    tags.Add("closed");
                    tags.Add(properties[i]);
                  }
                  break;
                default:
                  if (properties[i].Length>0)
                    tags.Add(properties[i]);
                  break;
              }
            }
          }
        }
        else title = subject;
      }
    }
    HeaderInfo _headerInfo;

    public static HeaderInfo ParseNNTPHeader(string head)
    {
      string[] rows = head.Split('\n');
      string messageID = "";
      List<string> references = new List<string>();
      string from = "";
      string newsgroups = "";
      string subject = "";
      string icon = null;
      string xwiki = null;
      DateTime date;
      string dateSQL = "2000-01-01 00:00:00";
      for (int i = 0; i < rows.Length; i++)
      {
        // Read an XML file into the DataSet
        int colonIx = rows[i].IndexOf(':');
        if (colonIx >= 0)
        {
          string header = rows[i].Substring(0, colonIx);
          switch (header)
          {
            case "Message-ID":
              {
                string pattern = @"\<([^\>]*)\>";
                Match match = Regex.Match(rows[i], pattern);
                messageID = match.Groups[1].Value;
              }
              break;
            case "References":
              {
                string pattern = @"\<([^\>]*)\>(.*)"; //take only first reference, ignore others
                string reflist = rows[i].Trim();
                do 
                {
                  Match match = Regex.Match(reflist, pattern);
                  references.Add(match.Groups[1].Value);
                  reflist = match.Groups[2].Value;
                } while (reflist.Length > 0);
              }
              break;
            case "Icon":
              {
                icon = rows[i].Substring(colonIx + 1);
                icon = icon.Trim();
              }
              break;
            case "From":
              {
                from = rows[i].Substring(colonIx + 1);
                from = from.Trim();
                from = from.Replace("'", "''"); //double single quotes
              }
              break;
            case "Newsgroups":
              {
                newsgroups = rows[i].Substring(colonIx + 1);
                newsgroups = newsgroups.Trim();
              }
              break;
            case "Subject":
              {
                subject = rows[i].Substring(colonIx + 1);
                subject = subject.Trim();
                subject = subject.Replace("'", "''"); //double single quotes
              }
              break;
            case "Date":
              {
                string dateStr = rows[i].Substring(colonIx + 1);
                dateStr = dateStr.Trim();
                // it looks like 
                // Mon, 9 Nov 2009 15:09:21 +0000 (UTC)
                // we need to delete the +0000 offset and (UTC) and to add it again
                // but it can contain also "Date: 7 Sep 2001 12:31:54 GMT\r"
                string pattern = @"(.*) ([+-].*)";
                Match match = Regex.Match(dateStr, pattern);
                string dateOnly = match.Groups[1].Value;
                if (dateOnly == "") //different format, possibly without +-0100
                {
                  pattern = @"(.* ..\:..\:..) (.*)";
                  match = Regex.Match(dateStr, pattern);
                  dateStr = match.Groups[1].Value;
                  date = DateTime.Parse(dateStr);
                }
                else
                {
                  string addStr = match.Groups[2].Value;
                  pattern = @"([+-]....).*";
                  match = Regex.Match(addStr, pattern);
                  int addVal = Int32.Parse(match.Groups[1].Value) / 100;
                  date = DateTime.Parse(dateOnly);
                  date = date.AddHours(-addVal); // +0700 means 7 was added, so we substract it
                }
                dateSQL = String.Format("{0:D4}-{1:D2}-{2:D2} {3:D2}:{4:D2}:{5:D2}", date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second);
              }
              break;
            case "X-Wiki":
              {
                xwiki = rows[i].Substring(colonIx + 1);
                xwiki = xwiki.Trim();
              }
              break;
          }
        }
      }
      return new HeaderInfo(messageID, references, from, newsgroups, subject, dateSQL, icon, xwiki);
    }

    private void NextArticle()
    {
      if (tcpClient != null && tcpClient.Connected == true && firstID >= 0)
      {
        // Get the header
        txtHead.Text = "";
        // Initialize the buffer to 2048 bytes
        downBuffer = new byte[2048];
        // Request the headers of the article
        byteSendInfo = StringToByteArr("HEAD " + firstID + "\r\n");
        // Send the request to the NNTP server
        strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
        while ((bytesSize = strRemote.Read(downBuffer, 0, downBuffer.Length)) > 0)
        {
          NewChunk = System.Text.Encoding.GetEncoding(1250).GetString(downBuffer, 0, bytesSize);
          txtHead.Text += NewChunk;
          // No such article in the group
          if (NewChunk.Substring(0, 3) == "423")
          {
            // Ready for the next article, unless there is nothing else there...
            if (firstID < lastID)
            {
              firstID++;
            }
            // Next article please
            NextArticle();
            // End this method because it's retrieving a nonexistent article
            return;
          }
          else if (NewChunk.Substring(NewChunk.Length - 5, 5) == "\r\n.\r\n")
          {
            // If the last thing in the buffer is "\r\n.\r\n" the message's finished
            break;
          }
        }

        _headerInfo = ParseNNTPHeader(NewChunk);

        // Get the body
        txtBody.Text = "";
        // Initialize the buffer to 2048 bytes
        downBuffer = new byte[2048];
        // Request the headers of the article
        byteSendInfo = StringToByteArr("BODY " + firstID + "\r\n");
        // Send the request to the NNTP server
        strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
        while ((bytesSize = strRemote.Read(downBuffer, 0, downBuffer.Length)) > 0)
        {
          NewChunk = System.Text.Encoding.GetEncoding(1250).GetString(downBuffer, 0, bytesSize);
          txtBody.Text += NewChunk;
          // If the last thing in the buffer is "\r\n.\r\n" the message's finished
          if (NewChunk.Substring(NewChunk.Length - 5, 5) == "\r\n.\r\n")
          {
            break;
          }
        }

        // Ready for the next article, unless there is nothing else there...
        if (firstID < lastID)
        {
          firstID++;
        }
      }
      else
      {
        MessageBox.Show("Please select a newsgroup from the dropdown list and click on 'Get News' first.", "Newsgroup retrieval", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

#region Save News to SQL Thread
#if SAVE_NEWS_TO_SQL_USING_THREAD
    private class SaveNews2SqlThread
    {
      Form1 parent;
      int firstID, lastID;
      byte[] byteSendInfo = new byte[2048];
      int bytesSize;
      public SaveNews2SqlThread(Form1 parent) { this.parent = parent; firstID = parent.firstID; lastID = parent.lastID;  }
      public void ThreadRun()
      {
        if (parent.tcpClient != null && parent.tcpClient.Connected == true && firstID >= 0)
        {
          //Note: one cannot use direct parent.saveNews.Enabled = false;
          //      for runtime error: Cross-thread operation not valid: Control accessed from a thread other than the thread it was created on.
          // The delegate and Invoke should be used
          parent.Invoke(new MethodInvoker(
            delegate
            {
              parent.saveNews.Enabled = false;
              parent.btnNext.Enabled = false;
              parent.btnGetNews.Enabled = false;
              parent.btnGo.Enabled = false;
            }));
          FileStream myFile = File.Create("newsGroupExport.sql");
          Encoding utf8 = Encoding.UTF8;
          // Initialize the buffer to 2048 bytes
          byte[] buf = new byte[16384];
          while (firstID <= lastID)
          {
            parent.Invoke(new MethodInvoker(delegate { parent.labelSaveProgress.Text = firstID + "/" + lastID; }));
            /// Get the header
            // Request the headers of the article
            byteSendInfo = StringToByteArr("HEAD " + firstID + "\r\n");
            // Send the request to the NNTP server
            parent.strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
            string newsArticle = "";
            string newChunk = "";
            while ((bytesSize = parent.strRemote.Read(buf, 0, buf.Length)) > 0)
            {
              newChunk = System.Text.Encoding.GetEncoding(1250).GetString(buf, 0, bytesSize);
              newsArticle += newChunk;
              // No such article in the group
              if (newChunk.Substring(0, 3) == "423")
              {
                // Ready for the next article, unless there is nothing else there...
                break;
              }
              else if (newChunk.Substring(newChunk.Length - 5, 5) == "\r\n.\r\n")
              {
                string sql = NNTPHeader2SQL(newsArticle);
                byte[] bytes = utf8.GetBytes(sql);
                myFile.Write(bytes, 0, bytes.Length);
                break;
              }
            }
            firstID++; //next article
          }
          myFile.Close();
          parent.Invoke(new MethodInvoker(
            delegate {
              parent.labelSaveProgress.Text = "DONE";
              parent.saveNews.Enabled = true;
              parent.btnNext.Enabled = true;
              parent.btnGetNews.Enabled = true;
              parent.btnGo.Enabled = true;
            }));
        }
      }

      private string NNTPHeader2SQL(string head)
      {
        HeaderInfo hi = ParseNNTPHeader(head);
        return String.Format("INSERT INTO News\r\nVALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}');\r\n",
                        hi.messageID, hi.references, hi.from, hi.newsgroups, hi.subject, hi.dateSQL);
      }
    } //class SaveNews2SqlThread
#endif
#endregion

    List<Colabo.ImportedArticle> _articles;
    class TaskSaveNewsThread : Colabo.ITask
    {
      Form1 parent;
      private Colabo.SQLDatabase _database;
      int firstID, lastID;
      byte[] byteSendInfo = new byte[2048];
      int bytesSize;
      private string selectedGroup;

      public override string ProgressType() { return "Import"; }

      public TaskSaveNewsThread(Form1 parent, Colabo.SQLDatabase database) 
      {
        this.parent = parent; _database = database;
        firstID = parent.firstID; lastID = parent.lastID;
        selectedGroup = parent.selectedGroup;
      }

      public override void Process(BackgroundWorker worker)
      {
        string message = "Saving the NG articles...";
        worker.ReportProgress(10, message);
        if (parent.tcpClient != null && parent.tcpClient.Connected == true && firstID >= 0)
        {
          //Note: one cannot use direct parent.saveNews.Enabled = false;
          //      for runtime error: Cross-thread operation not valid: Control accessed from a thread other than the thread it was created on.
          // The delegate and Invoke should be used
          parent.Invoke(new MethodInvoker(
            delegate
            {
              parent.saveNews.Enabled = false;
              parent.btnNext.Enabled = false;
              parent.btnGetNews.Enabled = false;
              parent.btnGo.Enabled = false;
            }));
          // Initialize the buffer to 2048 bytes
          byte[] buf = new byte[16384];

          List<ArticleItemNG> articlesNG = new List<ArticleItemNG>();
          while (firstID <= lastID)
          {
            parent.Invoke(new MethodInvoker(delegate { parent.labelSaveProgress.Text = firstID + "/" + lastID; }));
            /// Get the header
            // Request the headers of the article
            byteSendInfo = StringToByteArr("HEAD " + firstID + "\r\n");
            // Send the request to the NNTP server
            parent.strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
            string newsArticle = "";
            bool articleReady = false;
            string newChunk = "";
            while ((bytesSize = parent.strRemote.Read(buf, 0, buf.Length)) > 0)
            {
              newChunk = System.Text.Encoding.GetEncoding(1250).GetString(buf, 0, bytesSize);
              newsArticle += newChunk;
              // No such article in the group
              if (newChunk.Substring(0, 3) == "423")
              {
                // Ready for the next article, unless there is nothing else there...
                break;
              }
              else if (newChunk.Substring(newChunk.Length - 5, 5) == "\r\n.\r\n")
              {
                articleReady = true;
                break;
              }
            }
            string articleBody = "";
            if (articleReady)
            {
              // Get the article body
              // Initialize the buffer to 2048 bytes
              byte[] bodyBuffer = new byte[2048];
              // Request the headers of the article
              byteSendInfo = StringToByteArr("BODY " + firstID + "\r\n");
              // Send the request to the NNTP server
              parent.strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
              while ((bytesSize = parent.strRemote.Read(buf, 0, buf.Length)) > 0)
              {
                newChunk = System.Text.Encoding.GetEncoding(1250).GetString(buf, 0, bytesSize);
                // If the last thing in the buffer is "\r\n.\r\n" the message's finished
                if (newChunk.Substring(newChunk.Length - 5, 5) == "\r\n.\r\n")
                {
                  articleBody += newChunk.Substring(0, newChunk.Length - 5);
                  break;
                }
                else articleBody += newChunk;
              }
            }
            if (articleReady)
            {
              // Do the action
              HeaderInfo hi = ParseNNTPHeader(newsArticle);
              parent.SaveToFiles(hi, articleBody);
              // store article inside article list
              ArticleItemNG article = new ArticleItemNG();
              article._author = hi.from;
              hi.ExtractTagsAndTitle();
              article._tagsNG = hi.tags;
              article._title = hi.title.Trim();
              string fullURL = _database.svnRepository + selectedGroup + "/" + hi.messageID + ".txt";
              article.RawURL = _database.ToRelative(fullURL);
              article._dateNG = hi.dateSQL;

              //CultureInfo nfo = new System.Globalization.CultureInfo("en-US");
              article._date = System.Xml.XmlConvert.ToDateTime(hi.dateSQL, "yyyy-MM-dd HH:mm:ss");
              article._tagsDate = article._date;
              article._parentNG = hi.references;
              article._idNG = hi.messageID;
              article._workingPathNG = parent._workingCopyRoot + selectedGroup + "\\" + hi.messageID + ".txt";
              articlesNG.Add(article);
            }
            firstID++; //next article
          }
          // Add prepared articlesNG into Colabo by creating appropriate tasks
          for (int i = 0; i < articlesNG.Count; i++)
          {
            ArticleItemNG article = articlesNG[i];
            // set the parent id (note, parent article was surely added before)
            if (article._parentNG.Count > 0)
            {
              bool found = false;
              for (int pix=article._parentNG.Count-1; pix>=0; pix--)
              {
                string parentAdept = article._parentNG[pix];
                for (int j = 0; j < i; j++)
                {
                  if (articlesNG[j]._idNG == parentAdept)
                  {
                    found = true;
                    article._parentIX = j;
                    article._parent = articlesNG[j]._id; //taken from parent
                    break;
                  }
                }
                if (found) break;
              }
              // It happens sometimes the NG article parent does not exists (checked inside NewsReader)
              // System.Diagnostics.Trace.Assert(found, "Parent for NG article not found!");
            }
          }
          // Find the Effective tags for every root article
          for (int i = 0; i < articlesNG.Count; i++)
          {
            ArticleItemNG article = articlesNG[i];
            if (article._tagsNG != null && article._tagsNG.Count > 0)
            { // has tags
              // find root parent
              int ix = i;
              while (articlesNG[ix]._parentIX >= 0)
              {
                ix = articlesNG[ix]._parentIX;
              }
              if (article._tagsDate.CompareTo(articlesNG[ix]._tagsDate) > 0)
              { // update
                articlesNG[ix]._tagsDate = article._tagsDate;
                articlesNG[ix]._ownTags = article._tagsNG;
              }
            }
          }
          // create NewsGroup identification tags
          List<String> groupTags = new List<String>();
          if (parent.selectedGroup.Length > 0)
          {
            string[] groups = parent.selectedGroup.Split('.');
            // groupTags.Add(parent.selectedGroup); //we do not want long tags: bistudio.tools.colaboration.discuss
            for (int i = 0; i < groups.Length; i++) groupTags.Add(groups[i]);
          }
          // Send articles to database
          for (int i = 0; i < articlesNG.Count; i++)
          {
            // send it to database
            ArticleItemNG article = articlesNG[i];
            if (article._parentIX >= 0)
            { // set parent id (it is known now, as parent had been added already)
              article._parent = articlesNG[article._parentIX]._id;
            }
            else
            { // add NewsGroup identification tags
              if (article._ownTags == null) article._ownTags = new List<string>();
              article._ownTags.AddRange(groupTags);
            }
            _colabo.SendArticle(_database, article);
            // commit the document to the SVN with correct properties
            string value = "";
            if (article._ownTags != null)
            {
              for (int k = 0; k < article._ownTags.Count; k++)
              {
                string tag = article._ownTags[k].Trim();
                // do not store private tags
                if (Colabo.ArticleItem.FromPrivateTag(tag) != null) continue;
                if (tag.Length > 0)
                {
                  if (value.Length > 0) value = value + "\n";
                  value = value + tag;
                }
              }
            }
            parent._articles.Add(new Colabo.ImportedArticle(article._workingPathNG, value));
          }
        }
        worker.ReportProgress(100, message);
      }

      public override void Finish()
      {
        // COMMIT assynchronously
        string workingCopyFolderPath = (parent._workingCopyRoot + selectedGroup);
        Colabo.ITask task = new Colabo.TaskCommitWorkingPathRoot(workingCopyFolderPath, "Importing Articles from NG", parent._articles, _database.GetArticlesStore());
        _colabo.AddTask(task);
        // DONE
        parent.labelSaveProgress.Text = "DONE";
        parent.saveNews.Enabled = true;
        parent.btnNext.Enabled = true;
        parent.btnGetNews.Enabled = true;
        parent.btnGo.Enabled = true;
      }
    }

    private void saveNews_Click(object sender, EventArgs e)
    {
//       saveNews2SqlThread = new SaveNews2SqlThread(this);
//       saveThread = new Thread(new ThreadStart(saveNews2SqlThread.ThreadRun));
      _articles = new List<Colabo.ImportedArticle>();
      Colabo.ITask task = new TaskSaveNewsThread(this, _database);
      _colabo.AddTask(task);
    }

    private string GetMimeType(string fileName)
    {
      string mimeType = "application/unknown";
      string ext = System.IO.Path.GetExtension(fileName).ToLower();
      Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
      if (regKey != null && regKey.GetValue("Content Type") != null)
        mimeType = regKey.GetValue("Content Type").ToString();
      return mimeType;
    }

    private HeaderInfo GetArticleHeaderInfo(string newsHandle)
    {
      /// Get the header
      // Request the headers of the article
      byte[] requestBytes = StringToByteArr("HEAD " + newsHandle + "\r\n");
      // Send the request to the NNTP server
      strRemote.Write(requestBytes, 0, requestBytes.Length);
      string newsArticle = "";
      string newChunk = "";
      int bsize = 0;
      byte[] buf = new byte[4096];
      bool firstLine = true;
      while ((bsize = strRemote.Read(buf, 0, buf.Length)) > 0)
      {
        newChunk = System.Text.Encoding.GetEncoding(1250).GetString(buf, 0, bsize);
        newsArticle += newChunk;
        // No such article in the group
        System.Diagnostics.Debug.WriteLine("{0}", newChunk);
        if (firstLine && newChunk.Substring(0, 3).CompareTo("221")!=0)
        {
          return null;
        }
        else if (newChunk.Substring(newChunk.Length - 5, 5) == "\r\n.\r\n")
        {
          break;
        }
        firstLine = false;
      }
      return ParseNNTPHeader(newsArticle);
    }

    private string GetArticleTitle(string newsHandle)
    {
      HeaderInfo info = GetArticleHeaderInfo(newsHandle);
      if (info != null) return info.subject;
      return null;
    }

    private string FixNewsURL(string line)
    {
      int beginText = 0;
      int index = line.IndexOf("news:");
      string output = "";
      int lnlen = line.Length;
      if (index >= 0) do
      {
        int urlstart = index;
        int urlend = index;
        if ( index==0 || !Char.IsLetterOrDigit(line[index-1]) )
        { //it is a link to other news article
          index += 5;
          while (index < lnlen && line[index] > 32 && line[index] != '>' && line[index] != ')')
          {
            int nxt = index + 1;
            if (!Char.IsLetterOrDigit(line[index]) && line[index] != '/' && line[index] != '\\' && line[index] < 128
                && (nxt >= lnlen || line[nxt] < 33))
            {
              break;
            }
            else if (nxt >= lnlen) 
            { 
              index = lnlen; 
              break; 
            }
            index = nxt;
          }
          urlend = index;
        }
        // convert current text and url into output string
        output += line.Substring(beginText, urlstart - beginText);
        beginText = urlend;
        if (urlstart != urlend)
        {
          string url = line.Substring(urlstart, urlend - urlstart);
          // get the link target article title
          string newsHandle = string.Format("<{0}>", url.Substring(5));
          string title = GetArticleTitle(newsHandle);
          if (title != null) // change news: link to markdown link style
            output += string.Format("[{0}]({1})", title, url); //use title when available
          else
            output += string.Format("[{0}]({1})", url, url); //use url of news article instead
        }
        else urlend++;
        if (urlend < lnlen) index = line.IndexOf("news:", urlend); //find next index
        else index = -1;
      } while (index>=0 && index<lnlen);
      if (beginText<lnlen)
        output += line.Substring(beginText);
      return output;
    }

    public void SaveToFiles(HeaderInfo hi, string body)
    {
      LNStates state = LNStates.LNText;
      string[] lines = body.Split('\n');
      string uuEncodedFile = "";  // the attachment's uuEncoded content
      string fileName = "";       // filename of possible attachment found
      string markdownBody = "";   // main text of the article
      string attachments = "";    // markdown links to saved attachments 
      int attachmentNo = 0;
      for (int i = 0; i < lines.Length; i++)
      {
        string line = lines[i].TrimEnd(new char[] { '\r' });
        switch (state)
        {
          case LNStates.LNText:
            string beginPattern = @"begin [0-9a-fA-F]{3} ([^ ].*)"; //something like "begin 666 myPic.jpg"
            Match match = Regex.Match(line, beginPattern);
            string filen = match.Groups[1].Value;
            if (filen.CompareTo("") == 0)
            {
              // store the line
              if (line.CompareTo("..") == 0) line = "."; //fix double dots to represent single dots (NNTP)
              if (i > 0) //skip first line, containing answer: 222 1906 <hj95ed$alk$1@new-server.localdomain> body
              {
                line = FixNewsURL(line);
                markdownBody += line + "\r\n";
              }
              continue; //no attachment match
            }
            fileName = filen; //remember the filename
            uuEncodedFile = "";
            // attachment found
            state = LNStates.LNFile;
            System.Diagnostics.Debug.WriteLine(fileName);
            break;
          case LNStates.LNFile:
            if (line.CompareTo(" ") == 0 || line.CompareTo("`") == 0)
            {
              state = LNStates.LNEmpty;
              continue;
            }
            // prepare data for uudecode
            uuEncodedFile += line + "\r\n";
            break;
          case LNStates.LNEmpty:
            if (line.CompareTo("end") != 0)
            {
              System.Diagnostics.Debug.WriteLine("Bad attachment {0}, does not ends with single space line and end-line!", fileName);
              state = LNStates.LNText;
            }
            else
            {
              // uudecode the file stored in uuEncodedFile
              // create outputStream file
              string attachmentFileName = hi.messageID + "_" + fileName;
              string attachmentFilePath = _workingCopyRoot + selectedGroup + "\\" + attachmentFileName;
              FileStream outputStream = File.Create(attachmentFilePath);
              _articles.Add(new Colabo.ImportedArticle(attachmentFilePath,null));
              // convert string to stream
              byte[] bytes = Encoding.ASCII.GetBytes(uuEncodedFile);
              MemoryStream stream = new MemoryStream(bytes);
              Codecs.UUDecode(stream, outputStream);
              outputStream.Close();
              string mime = GetMimeType(attachmentFileName);
              string format = "[Attachment {0}: {1}]({2})\r\n\r\n";
              string attURL = _database.svnRepository + selectedGroup + "/" + attachmentFileName;
              if (mime.StartsWith("image")) format = "!" + format;
              attachments += string.Format(format, ++attachmentNo, fileName, attURL);
            }
            break;
        }
      }
      // save the article body
      // convert string to stream
      if (attachmentNo > 0)
      {
        markdownBody += "\r\n---------------------\r\n" + attachments;
      }
      // add possible avatar
      if (hi.icon!=null || hi.xwiki!=null)
      {
        string ngdivFormat = string.Format("<div class=\"ngdiv\" style=\"position:absolute; top:20px; right:20px\">{{0}}{{1}}</div>");
        string avatar = "";        
        if (hi.icon!=null)
          avatar = string.Format("<img class=\"ngavatar\" src=\"{0}\"/>", hi.icon);
        string xwiki = "";
        if (hi.xwiki != null)
          xwiki = string.Format("<a class=\"ngwiki\" href=\"{0}\" style=\"position:relative; top:{1}px; right:10px\">WIKI</a>", hi.xwiki, (hi.icon!=null ? -20 : 30) ); //when there is no icon, use 30px top indenting
        string ngdiv = string.Format(ngdivFormat, xwiki, avatar);
        markdownBody = ngdiv + markdownBody;
      }
      // generate a file stream with UTF8 characters
      if (!Directory.Exists(_workingCopyRoot + selectedGroup)) Directory.CreateDirectory(_workingCopyRoot + selectedGroup);      
      string fileFullPath = _workingCopyRoot + selectedGroup + "\\" + hi.messageID + ".txt";
      FileStream fw = File.Create(fileFullPath);
      StreamWriter sw = new StreamWriter(fw, System.Text.Encoding.UTF8);
      sw.Write(markdownBody);
      sw.Close();
      sw = null;
    }

    enum LNStates { LNText, LNFile, LNEmpty };
    private void saveBodyButton_Click(object sender, EventArgs e)
    {
      string body = txtBody.Text;
      SaveToFiles(_headerInfo, body);
    }
  }

  public class ArticleItemNG : Colabo.ArticleItem
  {
    public List<string> _tagsNG;   //tags created from NG properties (only the most current tags will be applied to the thread root article)
    public string _dateNG;
    public List<string> _parentNG;
    public string _idNG;
    public int _parentIX;
    public string _workingPathNG;
    public DateTime _tagsDate;
    public ArticleItemNG() { _tagsNG = null; _dateNG = null; _parentNG = null; _idNG = null; _parentIX = -1; _workingPathNG = null; }
  }

}
