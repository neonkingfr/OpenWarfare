using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.DirectoryServices.Protocols;

namespace Colabo
{
  // Dialog to edit database properties
  public partial class DialogDatabase : Form
  {
    ListBox.ObjectCollection _databases;
    int _index;
    bool _extended;
    string _ldapHost;
    int _ldapPort;
    string _ldapUser;
    string _ldapPassword;

    public DialogDatabase(ListBox.ObjectCollection databases, int index, string host, int port, string user, string password)
    {
      _databases = databases;
      _index = index;
      _ldapHost = host;
      _ldapPort = port;
      _ldapUser = user;
      _ldapPassword = password;
      _extended = false;

      InitializeComponent();
    }

    private void DialogDatabase_Load(object sender, EventArgs e)
    {
      ldapLoad.Enabled = !string.IsNullOrEmpty(_ldapHost);

      // initialize the list of server types
      valueServerType.Items.AddRange(DBServerTypeInfo.Values);

      DBInfo database = null;
      DBUserInfo userInfo = null;
      if (_index >= 0)
      {
        DBInfoExt info = (DBInfoExt)_databases[_index];
        database = info._dbInfo;
        userInfo = info._userInfo;
      }
      LoadDatabase(database, userInfo);
    }

    private void LoadDatabase(DBInfo database, DBUserInfo userInfo)
    {
      if (database != null)
      {
        if (!string.IsNullOrEmpty(_ldapHost) && !string.IsNullOrEmpty(database._ldapDN))
        {
          // reload values from LDAP
          if (LDAPLoadDatabase(database, userInfo))
          {
            ldapDN.Text = database._ldapDN;
            return;
          }
          // continue, use the structures
        }

        ldapDN.Text = "";
        ShowDbInfo(database);
        ShowUserInfo(userInfo);
      }
      else
      {
        ldapDN.Text = "";
        ShowDbInfo(null);
        ShowUserInfo(null);
      }
    }

    private void SelectServerType(DBServerType serverType)
    {
      foreach (DBServerTypeInfo item in valueServerType.Items)
      {
        if (item._value == serverType)
        {
          valueServerType.SelectedItem = item;
          return;
        }
      }
    }

    private bool LDAPLoadDatabase(DBInfo database, DBUserInfo userInfo)
    {
      LdapConnection connection = Configuration.OpenLDAPConnection(_ldapHost, _ldapPort, _ldapUser, _ldapPassword);
      if (connection == null) return false;

      try
      {
        // access the database properties
        SearchRequest request = new SearchRequest(database._ldapDN, "(objectClass=*)", SearchScope.Base,
          new String[]{"cn", "colaboDBServerType", "colaboDBServer", "colaboDBDatabase", "colaboSVNPath", "colaboTranslate"});
        SearchResponse response = (SearchResponse)connection.SendRequest(request);

        if (response.Entries.Count == 0) return false; // database not found in LDAP
        SearchResultEntry entry = response.Entries[0];

        DirectoryAttribute attributes = entry.Attributes["cn"];
        if (attributes != null && attributes.Count == 1) valueName.Text = attributes[0].ToString();

        DBServerType serverType = DBServerType.STMySQL;
        attributes = entry.Attributes["colaboDBServerType"];
        if (attributes != null && attributes.Count == 1)
        {
          string value = attributes[0].ToString();
          if (Enum.IsDefined(typeof(DBServerType), value))
            serverType = (DBServerType)Enum.Parse(typeof(DBServerType), value, true);
        }
        SelectServerType(serverType);

        attributes = entry.Attributes["colaboDBServer"];
        if (attributes != null && attributes.Count == 1) valueServer.Text = attributes[0].ToString();

        attributes = entry.Attributes["colaboDBDatabase"];
        if (attributes != null && attributes.Count == 1) valueDatabase.Text = attributes[0].ToString();

        attributes = entry.Attributes["colaboSVNPath"];
        if (attributes != null && attributes.Count == 1) valueSVN.Text = attributes[0].ToString();

        attributes = entry.Attributes["colaboTranslate"];
        if (attributes != null && attributes.Count == 1) valueTranslate.Text = attributes[0].ToString();

        // access the user properties
        request = new SearchRequest(_ldapUser, "(objectClass=*)", SearchScope.Base,
          new String[] { "uid", "displayName", "colaboUserPicture", "colaboUserColor"});
        response = (SearchResponse)connection.SendRequest(request);

        if (response.Entries.Count == 0) return false; // user not found in LDAP
        entry = response.Entries[0];

        attributes = entry.Attributes["uid"];
        if (attributes != null && attributes.Count == 1) valueUser.Text = attributes[0].ToString();

        valuePassword.Text = _ldapPassword;

        attributes = entry.Attributes["displayName"];
        if (attributes != null && attributes.Count == 1) valueFullName.Text = attributes[0].ToString();

        attributes = entry.Attributes["colaboUserPicture"];
        if (attributes != null && attributes.Count == 1) valuePicture.Text = attributes[0].ToString();

        valueColor.Visible = false;
        attributes = entry.Attributes["colaboUserColor"];
        if (attributes != null && attributes.Count == 1)
        {
          int color;
          if (int.TryParse(attributes[0].ToString(), out color))
          {
            valueColor.BackColor = Color.FromArgb(255, Color.FromArgb(color)); // set alpha = 255
            valueColor.Visible = true;
          }
        }
      }
      catch
      {
        return false;
      }

      // mark what cannot be modified
      valueName.Enabled = false;
      valueServerType.Enabled = false;
      valueServer.Enabled = false;
      valueDatabase.Enabled = false;
      valueUser.Enabled = false;
      valuePassword.Enabled = false;
      valueSVN.Enabled = false;
      valueTranslate.Enabled = false;

      valueFullName.Enabled = false;
      valuePicture.Enabled = false;
      buttonColor.Enabled = false;

      // limited handling of user info
      _extended = userInfo != null;
      buttonUserInfo.Enabled = !_extended;
      valueAliases.Enabled = _extended;

      return true;
    }

    private void ShowDbInfo(DBInfo database)
    {
      if (database == null)
      {
        valueName.Text = "";
        SelectServerType(DBServerType.STWebServices);
        valueServer.Text = "";
        valueDatabase.Text = "";
        valueUser.Text = "";
        valuePassword.Text = "";
        valueSVN.Text = "";
        valueTranslate.Text = "";
      }
      else
      {
        valueName.Text = database._name;
        SelectServerType(database._serverType);
        valueServer.Text = database._server;
        valueDatabase.Text = database._database;
        valueUser.Text = database._user;
        valuePassword.Text = database._password;
        valueSVN.Text = database._svnRepository;
        valueTranslate.Text = database._urlTranslate;
      }
      valueName.Enabled = true;
      valueServerType.Enabled = true;
      valueServer.Enabled = true;
      valueDatabase.Enabled = true;
      valueUser.Enabled = true;
      valuePassword.Enabled = true;
      valueSVN.Enabled = true;
      valueTranslate.Enabled = true;
    }

    private void ShowUserInfo(DBUserInfo userInfo)
    {
      _extended = userInfo != null;

      buttonUserInfo.Enabled = !_extended;
      valueFullName.Enabled = _extended;
      valuePicture.Enabled = _extended;
      valueColor.Visible = _extended;
      buttonColor.Enabled = _extended;
      valueAliases.Enabled = _extended;

      if (_extended)
      {
        valueFullName.Text = userInfo._fullName;
        valuePicture.Text = userInfo._picture;
        valueColor.BackColor = Color.FromArgb(255, Color.FromArgb(userInfo._color)); // set alpha = 255
        valueAliases.Text = userInfo._aliases;
      }
      else
      {
        valueFullName.Text = "";
        valuePicture.Text = "";
        valueColor.BackColor = Color.Black;
        valueAliases.Text = "";
      }
    }

    public DBInfoExt GetDBInfo()
    {
      DBInfo database = GetDatabase();

      DBUserInfo userInfo = null;
      if (_extended)
      {
        userInfo = new DBUserInfo();
        userInfo._fullName = valueFullName.Text;
        userInfo._picture = valuePicture.Text;
        userInfo._color = Color.FromArgb(0, valueColor.BackColor).ToArgb(); // remove alpha
        userInfo._aliases = valueAliases.Text;
      }

      DBInfoExt info = new DBInfoExt();
      info._dbInfo = database;
      info._userInfo = userInfo;
      return info;
    }

    private DBInfo GetDatabase()
    {
      DBInfo database = new DBInfo();
      database._ldapDN = ldapDN.Text;
      database._name = valueName.Text;
      database._serverType = ((DBServerTypeInfo)valueServerType.SelectedItem)._value;
      database._server = valueServer.Text;
      database._database = valueDatabase.Text;
      database._user = valueUser.Text;
      database._password = valuePassword.Text;
      database._svnRepository = valueSVN.Text;
      database._urlTranslate = valueTranslate.Text;

      return database;
    }

    private void DialogDatabase_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (DialogResult == DialogResult.OK)
      {
        string name = valueName.Text;
        if (name.Length == 0)
        {
          MessageBox.Show("The name should not be empty.", "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
          e.Cancel = true;
          return;
        }
        // check for names duplicity
        for (int i = 0; i < _databases.Count; i++)
        {
          if (i == _index) continue; // myself
          DBInfoExt info = (DBInfoExt)_databases[i];
          if (info._dbInfo._name.Equals(name))
          {
            MessageBox.Show(string.Format("The name {0} is already used.", name), "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            e.Cancel = true;
            return;
          }
        }
      }
    }
    private void buttonUserInfo_Click(object sender, EventArgs e)
    {
      DBInfo database = GetDatabase();
      DBUserInfo userInfo = SQLDatabase.LoadUserInfo(database);

      if (!string.IsNullOrEmpty(_ldapHost) && !string.IsNullOrEmpty(database._ldapDN))
      {
        // only userInfo._aliases will be used, other was already read from LDAP
        _extended = true;
        buttonUserInfo.Enabled = false;
        if (userInfo != null) valueAliases.Text = userInfo._aliases;
        valueAliases.Enabled = true;
      }
      else if (userInfo != null)
        ShowUserInfo(userInfo);
      else
        MessageBox.Show("Cannot connect to database using given data.", "Colabo", MessageBoxButtons.OK, MessageBoxIcon.Error);
    }

    private void buttonColor_Click(object sender, EventArgs e)
    {
      colorDialog.Color = valueColor.BackColor;
      DialogResult result = colorDialog.ShowDialog();
      if (result == DialogResult.OK)
        valueColor.BackColor = colorDialog.Color;
    }

    private void ldapLoad_Click(object sender, EventArgs e)
    {
      DialogDatabaseDN dialog = new DialogDatabaseDN();
      dialog.DBDN = ldapDN.Text;
      DialogResult result = dialog.ShowDialog();
      if (result == DialogResult.OK)
      {
        string dn = dialog.DBDN;
        if (dn != ldapDN.Text)
        {
          DBInfo database = new DBInfo();
          database._ldapDN = dn;
          DBUserInfo userInfo = new DBUserInfo();
          LoadDatabase(database, userInfo);
        }
      }
    }
  }
}