﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Colabo
{
  public partial class SslServerCertificateTrustDialog : Form
  {
    public SslServerCertificateTrustDialog()
    {
      InitializeComponent();
    }

    public void SetMessage(string msg) { text.Text = msg; }
  }
}
